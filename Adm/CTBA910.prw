#include "protheus.ch"
#include "ctba910.ch"
#include "apwizard.ch"
#INCLUDE 'FWBROWSE.CH'

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Funcao    � CTBWIZENT  � Autor � Microsiga                  � Data � 23/02/10 ���
��������������������������������������������������������������������������������Ĵ��
���Descri��o � User Function de mesmo nome criada com  fins de compatibilidade   ���
���          � entre os bin�rios                                                 ���
��������������������������������������������������������������������������������Ĵ��
���Sintaxe   � ENTWIZUPD  - Executada a partir da Main Fuction                   ���
��������������������������������������������������������������������������������Ĵ��
��� Uso      � ATUALIZACAO SIGACTB                                               ���
��������������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                            ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

User Function CTBWIZENT()
ENTWIZUPD()
Return

/*/
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
��������������������������������������������������������������������������������Ŀ��
���Funcao    � CTBWIZENT  � Autor � Microsiga                  � Data � 23/02/10 ���
��������������������������������������������������������������������������������Ĵ��
���Descri��o � User Function de mesmo nome criada com  fins de compatibilidade   ���
���          � entre os bin�rios                                                 ���
��������������������������������������������������������������������������������Ĵ��
���Sintaxe   � ENTWIZUPD  - Executada a partir da Main Fuction                   ���
��������������������������������������������������������������������������������Ĵ��
��� Uso      � ATUALIZACAO SIGACTB                                               ���
��������������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                            ���
���������������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������������
������������������������������������������������������������������������������������
/*/

Main Function CTBWIZENT()
ENTWIZUPD()
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWIZUPD �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � ENTWIZUPD  - Prepara��o para execu��o do Wizard.           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWIZUPD()
Local aSM0			:= {}

Private	cFirstEmp	:= ""
Private	cArqEmp		:= "SigaMat.Emp"
Private	nModulo		:= 34
Private	__cInterNet	:= Nil
Private nQtdEntid 	:= 1
Private nEntidIni 	:= 0
Private oMainWnd
Private cUserName 	:= ""
Private oGetDados

Private lChkRefaz	:= .F.
Private lChkATF		:= .T.
Private lChkCOM		:= .T.
Private lChkCTB		:= .T.
Private lChkEST		:= .T.
Private lChkFAT		:= .T.
Private lChkFIN		:= .T.
Private lChkGCT		:= .T.
Private lChkPCO		:= .T.
Private lChkVGE  	:= .T.
Private lMaxEnt		:= .F.

Private cMens		:=	STR0002 + CRLF +;	// "Esta rotina ira atualizar os dicionarios de dados"
						STR0003 + CRLF +;	// "para a utilizacao de novas entidades."
						STR0004 + CRLF +;	// "E importante realizar um backup completo dos dicionarios e base de dados, "
						STR0005 + CRLF +;	// "antes da execu��o desta rotina."
						STR0006				// "Nao deve existir usuarios utilizando o sistema durante a atualizacao!"

Private cMessage
Private aArqUpd		:= {}
Private aREOPEN		:= {}
Private __lPyme		:= .F.
Private oWizard

TCInternal(5,'*OFF') //-- Desliga Refresh no Lock do Top

Set Dele On
//Realiza a abertura do dicionario Exclusivo
OpenSM0Excl()
aSM0 := AdmAbreSM0()

RpcSetType(3)
RpcSetEnv( aSM0[1][1], aSM0[1][2] )

//Habilita mensagem
__cInterNet := ""

//���������������������������������������������������Ŀ
//� Verifica quantas entidades j� existem no ambiente �
//�����������������������������������������������������
IF (nEntidIni := GETMAXENT()) == 0 //Valida��o para caso n�o consiga obter acesso a tabela
	Return .F.
ElseIf nEntidIni > 9
	lMaxEnt := .T.
EndIf

dbSelectArea("SXB")
dbSetOrder(1)
If !MsSeek(Padr('CT0SX3', Len(SXB->XB_ALIAS)))
	MsgInfo(STR0065, STR0001)  // "Ambiente do Ativo Fixo desatualizado, executar o U_UPDCTB" ###, "Atencao !"
	Return .F.
EndIf

dbSelectArea("SX3")
dbSetOrder(2)
If !MsSeek("CT0_ENTIDA") .Or. Alltrim(SX3->X3_WHEN)=="!(M->CT0_ID $ '01|02|03|04')"
	MsgInfo(STR0065, STR0001)  // "Ambiente do CTB desatualizado, executar o U_UPDCTB" ###, "Atencao !"
	Return .F.
EndIf

//�����������������������������������������������������Ŀ
//� Painel 1 - Tela inicial do Wizard 		            �
//�������������������������������������������������������
oWizard := APWizard():New(STR0008/*<chTitle>*/,; // "Configura��o de Entidades"
STR0010/*<chMsg>*/, ""/*<cTitle>*/, ; // "Essa ferramenta ir� efetuar a manuten��o nos campos e par�metros para as novas configura��es"
cMens + CRLF + STR0013, ; // "Voc� dever� escolher o n�mero de entidades que ser�o inclu�das e a partir de qual ser� efetuada a inclus�o"
{||.T.} /*<bNext>*/ ,;
{||.T.}/*<bFinish>*/,;
.F./*<.lPanel.>*/, , , /*<.lNoFirst.>*/)
//{||.T.}/*<bNext>*/ ,;

//�����������������������������������������������������Ŀ
//� Painel 2 - Defini��o das Novas Entidades            �
//�������������������������������������������������������
oWizard:NewPanel(STR0008/*<chTitle>*/,; //"Configura��o de Entidades"
STR0014/*<chMsg>*/,; // "Assistente para configura��o de novas entidades no sistema"
{||.T.}/*<bBack>*/,;
{||ENTWZVLP2()} /*<bNext>*/ ,;
{||.T.}/*<bFinish>*/,;
.T./*<.lPanel.>*/ ,;
{|| EntGetNum()}/*<bExecute>*/) //Montagem da tela

//�����������������������������������������������������Ŀ
//� Painel 3 - Descri��o das Novas Entidades            �
//�������������������������������������������������������
oWizard:NewPanel(STR0008/*<chTitle>*/,; //"Configura��o de Entidades"
STR0014/*<chMsg>*/,; // "Assistente para configura��o de novas entidades no sistema"
{||.T.}/*<bBack>*/,;
{||ENTWZVLP3()} /*<bNext>*/ ,;
{||.T.}/*<bFinish>*/,;
.T./*<.lPanel.>*/ ,;
{|| EntGetDesc() }/*<bExecute>*/)

//�����������������������������������������������������Ŀ
//� Painel 4 - Acompanhamento do Processo               �
//�������������������������������������������������������
oWizard:NewPanel(STR0015/*<chTitle>*/,;  //"Processamento..."
""/*<chMsg>*/,;
{||.F.} /*<bBack>*/,;
{||.F.}/*<bNext>*/ ,;
{||.T.}/*<bFinish>*/,;
.T./*<.lPanel.>*/ ,;
{| lEnd| ENTWIZREGU(@lEnd)}/*<bExecute>*/)

oWizard:Activate( .T./*<.lCenter.>*/,;
{||.T.}/*<bValid>*/,;
{||.T.}/*<bInit>*/,;
{||.T.}/*<bWhen>*/)

Return(.F.)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWIZPROC�Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao  de processamento da gravacao dos arquivos          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWizProc(lEnd)
Local cTexto   := ''
Local cFile    := ""
Local cMask    := STR0009 //"Arquivos Texto (*.TXT) |*.txt|"
Local nRecno   := 0
Local nX       := 0
Local nRecAtu
Local aAreaSM0
Local lAbriu
Local oPanel    := oWizard:oMPanel[oWizard:nPanel]
Local nInc		:= 0
Local aSM0		:= AdmAbreSM0()

//�������������������������������
//�Abre os arquivos das empresas�
//�������������������������������
aSM0 := AdmAbreSM0()
oProcess:SetRegua1( Len( aSM0 ) )

RpcClearEnv()
OpenSm0Excl()

//����������������������������������������������Ŀ
//�Realiza as altera��es nos dicion�rios de dados�
//������������������������������������������������
For nInc := 1 To Len( aSM0 )
	RpcSetType(3)
	RpcSetEnv( aSM0[nInc][1], aSM0[nInc][2] )

	aArqUpd  := {}

	oProcess:IncRegua1( STR0011 + aSM0[nInc][1] + "/"+ STR0012 + aSM0[nInc][2] )  //"Empresa : "###"Filial : "

	cTexto += Replicate("-",128)+CRLF
	cTexto += STR0011 + aSM0[nInc][1] + "/" + STR0012 + aSM0[nInc][2] + "-" + aSM0[nInc][6] + CRLF //"Empresa : "###" Filial : "

	//�������������������������������Ŀ
	//�Atualiza o grupo de campos    .�
	//���������������������������������
	cTexto += CT0AtuSXG()

	//�������������������������������Ŀ
	//�Atualiza o dicionario de dados.�
	//���������������������������������
	cTexto += (ENTWizSX3())

	//�����������������������������Ŀ
	//�Atualiza o Indice           .�
	//�������������������������������
	cTexto += ENTAtuSIX()

	//������������������������������Ŀ
	//�Atualiza as consultas padroes.�
	//��������������������������������
	cTexto += EntAtuSXB()

	//������������������������������Ŀ
	//�Atualiza os Grupos de Perguntas.�
	//��������������������������������
	cTexto += EntAtuSX1()

	//�����������������������������Ŀ
	//�Inclui entidade             .�
	//�������������������������������
	cTexto += ENTAtuCT0()

	__SetX31Mode(.F.)
	For nX := 1 To Len(aArqUpd)
		lAbriu := .F.
		IncProc(STR0028+aArqUpd[nx]+"]") //"Atualizando estruturas. Aguarde... ["
		If Select(aArqUpd[nx])>0
			lAbriu := .T.
			dbSelecTArea(aArqUpd[nx])
			dbCloseArea()
		EndIf
		X31UpdTable(aArqUpd[nx])
		If __GetX31Error()
			Alert(__GetX31Trace())
			MsgInfo(STR0023+ aArqUpd[nx] + STR0024,STR0001) //"Ocorreu um erro desconhecido durante a atualizacao da tabela : "###". Verifique a integridade do dicionario e da tabela."###"Atencao!"
			cTexto += STR0026+aArqUpd[nx] +CRLF //"Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "

		ElseIf ! lAbriu
			dbSelectArea(aArqUpd[nx])
			dbCloseArea()
		EndIf
	Next nX

	RpcClearEnv()
	OpenSm0Excl()
Next

RpcSetEnv(aSM0[1][1],aSM0[1][2],,,,, { "AE1" })

cTexto     := STR0027+CRLF+cTexto	//	"Log da atualizacao "
__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)

DEFINE FONT oFont NAME "Mono AS" SIZE 5,12   //6,15
@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 280,130 OF oPanel PIXEL
oMemo:bRClicked := {||AllwaysTrue()}
oMemo:oFont:=oFont
DEFINE SBUTTON FROM 122,250 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oPanel PIXEL //Salva e Apaga //"Salvar Como..."

Return(.T.)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWizSX3 �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao  de processamento da gravacao do SX3                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWizSX3()

Local aSX3		:= {}
Local aEstrut	:= {}
Local lSX3		:= .F.
Local cTexto	:= ''
Local cAlias	:= ''
Local nContEstr	:= 1
Local nContItem	:= 1
Local nX		:= 0
Local nY		:= 0
Local nZ		:= 0
Local nNumEntid	 := nEntidIni
Local cGrpNum	:= ""
Local cEntidNum	:= ""
Local aArea		:= GetArea()
Local aAreaSX3	:= SX3->(GetArea())
Local aDisp		:= {}
Local cOrdem	:= ""
Local aTabATF	:= {{lChkATF},;															//Checklist ATF ou Refaz Campos
					{"SN3","SN5","SN6","SN7","SNA","SNC","SNG","SNV","SNX","SNW","SNY","FNE","FNF"},;	//Tabelas
					{"N3" ,"N5" ,"N6" ,"N7" ,"NA" ,"NC" ,"NG" ,"NV" ,"NX" ,"NW" ,"NY" ,"FNE","FNF"}}	//Inicial dos campos
Local aTabCOM	:= {{lChkCOM},;				//Checklist Compras ou Refaz Campos
					{"SC1","SC7","SCY","SD1","SDE","SCH","SCX","DBK","DBL"},;	//Tabelas
					{"C1" ,"C7" ,"CY" ,"D1" ,"DE" ,"CH","CX","DBK","DBL"}}	//Inicial dos campos
Local aTabCTB	:= {{lChkCTB},;													//Checklist Contabilidade ou Refaz Campos
					{"CT2","CT9","CTJ","CTK","CTZ","CV3","CV4","CV9","CVD","CW1","CW2","CW3"},;	//Tabelas
					{"CT2","CT9","CTJ","CTK","CTZ","CV3","CV4","CV9","CVD","CW1","CW2","CW3"}}	//Inicial dos campos
Local aTabEST	:= {{lChkEST},;			//Checklist Estoque ou Refaz Campos
					{"SB1","SCP","SCQ","SD3","SDG","SGS"},;	//Tabelas
					{"B1" ,"CP" ,"CQ" ,"D3" ,"DG" , "GS" }}	//Inicial dos campos
Local aTabFAT	:= {{lChkFAT},;	//Checklist Faturamento ou Refaz Campos
					{"SD2","SC6","AGG","AGH"},;		//Tabelas
					{"D2" ,"C6" ,"AGG","AGH" }}		//Inicial dos campos
Local aTabFIN	:= {{lChkFIN},;													//Checklist Financeiro ou Refaz Campos
					{"SE1","SE2","SE3","SE5","SE7","SEA","SED","SEF","SEH","SET","SEU","SEZ", "F46","FK8","FKK" },;	//Tabelas
					{"E1" ,"E2" ,"E3" ,"E5" ,"E7" ,"EA" ,"ED" ,"EF" ,"EH" ,"ET" ,"EU" ,"EZ" , "F46","FK8","FKK" }}	//Inicial dos campos
Local aTabGCT	:= {{lChkGCT},;	//Checklist Gestao de Contratos ou Refaz Campos
					Iif ( AliasIndic( "CXP" ), {"CNB","CNE","CNZ","CXP"}, {"CNB","CNE","CNZ"} ) ,;				//Tabelas
					Iif ( AliasIndic( "CXP" ), {"CNB","CNE","CNZ","CXP"}, {"CNB","CNE","CNZ"} ) } 				//Inicial dos campos
Local aTabVGE	:= {{lChkVGE},;													//Checklist Viagens ou Refaz Campos
					{"FLE","FLG"},;	//Tabelas
					{"FLE","FLG"}}	//Inicial
Local aTabGeral	:= {{.T.},;					//Geracao Padrao
					{"SA1","SA2","SA6"},;	//Tabelas
					{"A1" ,"A2" ,"A6" }}	//Inicial dos campos
Local aTabALL	:= {aTabATF,aTabCOM,aTabCTB,aTabEST,aTabFAT,aTabFIN,aTabGCT,aTabVGE,aTabGeral}
Local aColsGet	:= ACLONE(oGetDados:aCols)
Local aHeader	:= ACLONE(oGetDados:aHeader)
Local nPosPlano	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ID"})
Local nPosGrupo	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_GRPSXG"})
Local nPosF3	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_F3ENTI"})
Local nPosAlias	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ALIAS"})

Local cPlano
Local cF3
Local cAliasEnt
Local cFolder 	:= ""
Local cX3Usado	:= "���������������"
Local cX3Usado2	:= "���������������"
Local cX3NaoUso	:= "���������������"
Local xReserv	:= '��'
Local xReserv1	:= '?�'
Local xReserv2	:= '��'
Local xReserv3	:= '�+'
Local xReserv4	:= '�+'
Local xReserv5	:= '�+'
Local xReserv6	:= '��'
 
//Verifica se o dicion�rio esta no banco
Local lInDB		:= MPDicInDB()

aEstrut := {"X3_ARQUIVO"	,"X3_ORDEM"		,"X3_CAMPO"		,"X3_TIPO"		,"X3_TAMANHO"	,"X3_DECIMAL"	,"X3_TITULO"	,"X3_TITSPA"	,"X3_TITENG"	,;
			"X3_DESCRIC"	,"X3_DESCSPA"	,"X3_DESCENG"	,"X3_PICTURE"	,"X3_VALID"		,"X3_USADO"		,"X3_RELACAO"	,"X3_F3"		,"X3_NIVEL"		,;
			"X3_RESERV"		,"X3_CHECK"		,"X3_TRIGGER"	,"X3_PROPRI"	,"X3_BROWSE"	,"X3_VISUAL"	,"X3_CONTEXT"	,"X3_OBRIGAT"	,"X3_VLDUSER"	,;
			"X3_CBOX"		,"X3_CBOXSPA"	,"X3_CBOXENG"	,"X3_PICTVAR"	,"X3_WHEN"		,"X3_INIBRW"	,"X3_GRPSXG"	,"X3_FOLDER"	,"X3_PYME"		}

//---------------------------------------------------------------------------------
// Tratamento para converter o X3_USADO/X3_RESERV caso o dicion�rio esteja no banco de dados
//---------------------------------------------------------------------------------
if cPaisLoc == "PER"
	xReserv6	:= '��'
Endif
If lInDB
	cX3Usado	:= FWConvBin(cX3Usado)
	cX3Usado2	:= FWConvBin(cX3Usado2)
	cX3NaoUso	:= FWConvBin(cX3NaoUso)
	
	xReserv		:= FWConvRese(xReserv)
	xReserv1	:= FWConvRese(xReserv1)
	xReserv2	:= FWConvRese(xReserv2)
	xReserv3	:= FWConvRese(xReserv3)
	xReserv4	:= FWConvRese(xReserv4)
	xReserv5	:= FWConvRese(xReserv5)
	xReserv6	:= FWConvRese(xReserv6)
	
EndIf

For nX := 1 To Len(aColsGet) //La�o - Quantidade de entidades.

	cEntidNum	:= AllTrim(aColsGet[nX][nPosPlano]) //Numero corrente da entidade
	cGrpNum		:= aColsGet[nX][nPosGrupo]
	cF3			:= aColsGet[nX][nPosF3]
	cAliasEnt	:= aColsGet[nX][nPosAlias]

	//------------------------------------------
	// Campos padroes da contabilidade - INICIO
	//------------------------------------------
	    //"X3_ARQUIVO"	,"X3_ORDEM"	,"X3_CAMPO"					,"X3_TIPO"		,"X3_TAMANHO"	,"X3_DECIMAL"	,"X3_TITULO"				,"X3_TITSPA"				,"X3_TITENG"				, "X3_DESCRIC"							,"X3_DESCSPA"						,"X3_DESCENG"						,"X3_PICTURE"	,"X3_VALID"			,"X3_USADO"			,"X3_RELACAO"	,"X3_F3"	,"X3_NIVEL"	, "X3_RESERV"	,"X3_CHECK"	,"X3_TRIGGER"	,"X3_PROPRI"	,"X3_BROWSE"	,"X3_VISUAL"	,"X3_CONTEXT"	,"X3_OBRIGAT"	,"X3_VLDUSER"	, "X3_CBOX"		,"X3_CBOXSPA"	,"X3_CBOXENG"	,"X3_PICTVAR"	,"X3_WHEN"		,"X3_INIBRW"	,"X3_GRPSXG"	,"X3_FOLDER"	,"X3_PYME"	
	Aadd(aSX3,{"CT1"	,"00"		,"CT1_ACET"+cEntidNum		,"C"			,1				,0				,"Aceita Ent"+cEntidNum		,"Acepta Ent"+cEntidNum		,"Accept Ent"+cEntidNum		,"Aceita entidade "+cEntidNum+"?"		,"Acepta ente "+cEntidNum+"?"		,"Accept Entity "+cEntidNum+"?"		,"@!"			,"Pertence('12')"	,cX3Usado			,"'2'"			,""			,1			,xReserv		,""			,""				,"S"			,"N"			,"A"			,"R"			,""				,""				,"1=Sim;2=Nao","1=Si;2=No","1=Yes;2=No","","","","","3","S"})
	Aadd(aSX3,{"CT1","00","CT1_"+cEntidNum+"OBRG"	,"C",1,0,"Obrg.Ent."+cEntidNum+"?"	,"Oblig.Ent."+cEntidNum+"?"	,"Mand.Ent."+cEntidNum+"?"	,"Obrigat�ria entidade "+cEntidNum+"?"	,"Obligat�ria ente "+cEntidNum+"?"	,"Mandatory Entity "+cEntidNum+"?"	,"@!","Pertence('12')",cX3Usado,"'2'","",1,xReserv,"","","S","N","A","R","","","1=Sim;2=Nao","1=Si;2=No","1=Yes;2=No","","","","","3","S"})

	aPHelpPor := {"Informe Aceita entidade "+ cEntidNum}
	aPHelpSpa := {"Informe entidad acept� " + cEntidNum}
	aPHelpEng := {"Report accepted entity " + cEntidNum}
	PutHelp("PCT1_ACET"+cEntidNum, aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Obrigat�ria entidade " + cEntidNum}
	aPHelpSpa := {"Dile a entidad obligatoria " + cEntidNum}
	aPHelpEng := {"Inform Mandatory entity " + cEntidNum}
	PutHelp("PCT1_"+cEntidNum+"OBRG", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CT5","00","CT5_EC"+cEntidNum+"DB","C",200,0,"Ent.Deb. "+cEntidNum	,"Ent.Deb. "+cEntidNum	,"Ent.Deb. "+cEntidNum	,"Ent. Cont�bil Debito "+cEntidNum	,"Ent. Contable Debito "+cEntidNum	,"Acc. Entity Debit "+cEntidNum	,"@!","Vazio() .Or. Ctb080Form()",cX3Usado,"",cF3,1,xReserv,"","","S","","","","","","","","","","","","","2","S"})
	Aadd(aSX3,{"CT5","00","CT5_EC"+cEntidNum+"CR","C",200,0,"Ent.Cred. "+cEntidNum	,"Ent.Cred. "+cEntidNum	,"Cred.Ent. "+cEntidNum	,"Ent. Cont�bil Cr�dito "+cEntidNum	,"Ent. Contable Credito "+cEntidNum	,"Acc. Entity Credit "+cEntidNum,"@!","Vazio() .Or. Ctb080Form()",cX3Usado,"",cF3,1,xReserv,"","","S","","","","","","","","","","","","","2","S"})

	aPHelpPor := {"Informe Ent. Cont�bil D�bito " + cEntidNum}
	aPHelpSpa := {"Dile a Ent. Contabilidad de d�bito "	+ cEntidNum}
	aPHelpEng := {"Inform Ent. accounting Debit " + cEntidNum}
	PutHelp("PCT5_EC"+cEntidNum+"DB", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Ent. Cont�bil Cr�dito " + cEntidNum}
	aPHelpSpa := {"Dile a Ent. Contabilidad de d�bito "	+ cEntidNum}
	aPHelpEng := {"Inform Ent. Credit accounting " + cEntidNum}
	PutHelp("PCT5_EC"+cEntidNum+"CR", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CVX","00","CVX_NIV"+cEntidNum,"C",TamSXG(cGrpNum)[1],0,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"@!","",cX3Usado,"","",1,xReserv,"","","","","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe N�vel " + cEntidNum}
	aPHelpSpa := {"Dile a nivel " + cEntidNum}
	aPHelpEng := {"Inform Level " + cEntidNum}
	PutHelp("PCVX_NIV"+cEntidNum, aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CVY","00","CVY_NIV"+cEntidNum,"C",TamSXG(cGrpNum)[1],0,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"@!","",cX3Usado,"","",1,xReserv,"","","","","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe N�vel " + cEntidNum}
	aPHelpSpa := {"Dile a nivel " + cEntidNum}
	aPHelpEng := {"Inform Level " + cEntidNum}
	PutHelp("PCVY_NIV"+cEntidNum, aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CVZ","00","CVZ_NIV"+cEntidNum,"C",TamSXG(cGrpNum)[1],0,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"N�vel "+cEntidNum,"N�vel "+cEntidNum,"Level "+cEntidNum,"@!","",cX3Usado,"","",1,xReserv,"","","","","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe N�vel " + cEntidNum}
	aPHelpSpa := {"Dile a nivel " + cEntidNum}
	aPHelpEng := {"Inform Level " + cEntidNum}
	PutHelp("PCVZ_NIV"+cEntidNum, aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CTB","00","CTB_E"+cEntidNum+"DES","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Dest.","Ent."+cEntidNum+" Dest."	,"Ent."+cEntidNum+" Dest."	,"Entidade "+cEntidNum+" Destino"		,"Entidad "+cEntidNum+" Destino"		,"Entity "+cEntidNum+" Destiny"			,"@!",""							,cX3NaoUso,"",cF3,1,xReserv1	,"","","","S","","","","","","","","",""											,"",cGrpNum,"","N"})
	Aadd(aSX3,{"CTB","00","CTB_E"+cEntidNum+"INI","C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+" Ini"	,"Ent. "+cEntidNum+" Ini"	,"Ent. "+cEntidNum+" Ini"	,"Entid. "+cEntidNum+" Inicial Origem"	,"Entid. "+cEntidNum+" Inicio Origen"	,"Entity "+cEntidNum+" Initial Origin"	,"@!","Vazio() .Or. CtbEntExis()"	,cX3Usado2,"",cF3,1,xReserv2,"","","","S","","","","","","","","",'TrocaF3("'+cAliasEnt+'","'+cEntidNum+'")'	,"",cGrpNum,"","N"})
	Aadd(aSX3,{"CTB","00","CTB_E"+cEntidNum+"FIM","C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+" Fim"	,"Ent. "+cEntidNum+" Fin"	,"Ent. "+cEntidNum+" END"	,"Entidade "+cEntidNum+" Final Origem"	,"Entidad "+cEntidNum+" Final Origen"	,"Entity "+cEntidNum+" Final Origin"	,"@!","Vazio() .Or. CtbEntExis()"	,cX3Usado2,"",cF3,1,xReserv2,"","","","S","","","","","","","","",'TrocaF3("'+cAliasEnt+'", "'+cEntidNum+'")'	,"",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Destino."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Destino."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Destiny."}
	PutHelp("PCTB_E"+cEntidNum+"DES", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Inicial Origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Inicio Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " initial Source."}
	PutHelp("PCTB_E"+cEntidNum+"INI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Final Origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " final Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Origin final."}
	PutHelp("PCTB_E"+cEntidNum+"FIM", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CTQ","00","CTQ_E"+cEntidNum+"ORI"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+"Ori"	,"Ent. "+cEntidNum+"Ori"	,"Ent. "+cEntidNum+"Ori"	,"Entidade "+cEntidNum+" Origem"		,"Entidad "+cEntidNum+" Origen"			,"Entity "+cEntidNum+" Origin"		,"@!","CtbEntExis()",cX3Usado,"",cF3,1,xReserv3,"","","","N","","","","","","","","","","",cGrpNum,"","N"})
	Aadd(aSX3,{"CTQ","00","CTQ_E"+cEntidNum+"PAR"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+"Part"	,"Ent. "+cEntidNum+"Part"	,"Ent. "+cEntidNum+"Depar"	,"Entidade "+cEntidNum+" Partida"		,"Entidad "+cEntidNum+" Partida"		,"Entity "+cEntidNum+" Departure"	,"@!","CtbEntExis()",cX3Usado,"",cF3,1,xReserv3,"","","","N","","","","","","","","","","",cGrpNum,"","N"})
	Aadd(aSX3,{"CTQ","00","CTQ_E"+cEntidNum+"CP"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+" CPar"	,"Ent. "+cEntidNum+" CPar"	,"Ent. "+cEntidNum+" CPar"	,"Entid. "+cEntidNum+" Contra-Partida"	,"Entid. "+cEntidNum+" Contrapartida"	,"Entity "+cEntidNum+" Counterpart"	,"@!","CtbEntExis()",cX3Usado,"",cF3,1,xReserv4,"","","","N","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Source."}
	PutHelp("PCTQ_E"+cEntidNum+"ORI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aHelpPor := {"Digite a Entidade " + cEntidNum	,"Origem para obter o valor a"				,"ser rateado."}
	aHelpEsp := {"Digite la Entidad " + cEntidNum	,"origen para obtener el valor"				,"a prorratearse"}
	aHelpEng := {"Enter the source Entity "			,cEntidNum + "to obtain the value to be"	,"prorated."}
	PutHelp("PCCTQ_E"+cEntidNum+"ORI", aHelpPor,aHelpEng,aHelpEsp,.T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Partida."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Partida."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Departure."}
	PutHelp("PCTQ_E"+cEntidNum+"PAR", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aHelpPor := {"Neste campo dever� ser informado"	,"a Entidade " + cEntidNum + " a ser"	,"Debitada/Creditada na gera��o","dos lan�amentos de rateio."		,"Ser� Debitada/Creditada dependendo da"	,"Natureza do saldo resultante."		,"Se o Valor for devedor o Lan�amento"	,"ser� Credor e vice-versa.","Tecla <F3> disponivel para consulta"			,"do Cadastro de Entidade " + cEntidNum + "."}
	aHelpEsp := {"En este campo debera informarse"	,"la Entidad " + cEntidNum + " que se"	,"adeudara/acreditara en la"	,"generacion de los asientos de"	,"prorrateo. Se adeudara/acreditara segun"	,"la modalidad del saldo resultante."	,"Si el valor es deudor el asiento sera","acreedor y vice versa."	,"Pulse (F3) disponible para consultar"			,"el archivo de Entidad " + cEntidNum + "."}
	aHelpEng := {"You must inform in this field the","Entity " + cEntidNum + " to be"		,"debited/credited during the"	,"generaton of proration entries."	,"It will be credited/debited depending"	,"on the resulting balance nature."		,"If the value is in debt, the entries"	,"won�t be and vice versa."	,"<F3> available for the Entity " + cEntidNum	,"file look-up."}
	PutHelp("PCCTQ_E"+cEntidNum+"PAR", aHelpPor,aHelpEng,aHelpEsp,.T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Contra Partida."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Contrapartida."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Counterpart ."}
	PutHelp("PCTQ_E"+cEntidNum+"CP", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CV9","00","CV9_E"+cEntidNum+"ORI"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+"Ori"	,"Ent. "+cEntidNum+"Ori"	,"Ent. "+cEntidNum+"Ori"	,"Entidade "+cEntidNum+" Origem"		,"Entidad "+cEntidNum+" Origen"			,"Entity "+cEntidNum+" Origin"		,"@!","",cX3Usado,"",cF3,1,xReserv4,"","","","S","","","","","","","","","","",cGrpNum,"","N"})
	Aadd(aSX3,{"CV9","00","CV9_E"+cEntidNum+"PAR"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+"Part"	,"Ent. "+cEntidNum+"Part"	,"Ent. "+cEntidNum+"Depar"	,"Entidade "+cEntidNum+" Partida"		,"Entidad "+cEntidNum+" Partida"		,"Entity "+cEntidNum+" Departure"	,"@!","",cX3Usado,"",cF3,1,xReserv4,"","","","S","","","","","","","","","","",cGrpNum,"","N"})
	Aadd(aSX3,{"CV9","00","CV9_E"+cEntidNum+"CP"	,"C",TamSXG(cGrpNum)[1],0,"Ent. "+cEntidNum+" CPar"	,"Ent. "+cEntidNum+" CPar"	,"Ent. "+cEntidNum+" CPar"	,"Entid. "+cEntidNum+" Contra-Partida"	,"Entid. "+cEntidNum+" Contrapartida"	,"Entity "+cEntidNum+" Counterpart"	,"@!","",cX3Usado,"",cF3,1,xReserv4,"","","","S","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Origin ."}
	PutHelp("PCV9_E"+cEntidNum+"ORI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Partida."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Partida."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Departure ."}
	PutHelp("PCV9_E"+cEntidNum+"PAR", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Contra Partida."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Contrapartida."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Counterpart."}
	PutHelp("PCV9_E"+cEntidNum+"CP", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CV5","00","CV5_E"+cEntidNum+"ORI","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Orig.","Ent."+cEntidNum+" Orig.","Source Ent"+cEntidNum	,"Entidade "+cEntidNum+" Origem"		,"Entidad "+cEntidNum+" Origen"			,"Source Entity "+cEntidNum			,"@!"	,"CtbEntExis()"	,cX3Usado,"",cF3	,1,xReserv5,"","","","S","A","R","","","","","","",""										,"",cGrpNum	,"","N"})
	Aadd(aSX3,{"CV5","00","CV5_E"+cEntidNum+"FIM","C",TamSXG(cGrpNum)[1],0,"Entid."+cEntidNum+" Fim","Entid."+cEntidNum+" Fin","Fin. Ent."+cEntidNum	,"Entidade "+cEntidNum+" Orig. Fim"		,"Entidad "+cEntidNum+" Orig. Fin"		,"Final Entity "+cEntidNum+" Source","@!"	,""				,cX3Usado,"",cF3	,1,xReserv5,"","","","S","A","R","","","","","","",""										,"",cGrpNum	,"","N"})
	Aadd(aSX3,{"CV5","00","CV5_E"+cEntidNum+"DES","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Dest.","Ent."+cEntidNum+" Dest.","Dest. Ent."+cEntidNum	,"Entidade "+cEntidNum+" Destino"		,"Entidad "+cEntidNum+" Destino"		,"Destination Entity "+cEntidNum	,"@!"	,"CtbEntExis()"	,cX3Usado,"",cF3	,1,xReserv5,"","","","S","A","R","","","","","","","CtbOpCad(M->CV5_EMPDES,M->CV5_FILDES)"	,"",cGrpNum	,"","N"})
	Aadd(aSX3,{"CV5","00","CV5_E"+cEntidNum+"IGU","C",1					,0,"Ent."+cEntidNum+" igual","Ent."+cEntidNum+" igual","Ent."+cEntidNum+" equal","Entidade "+cEntidNum+" igual origem"	,"Entidad "+cEntidNum+" igual origen"	,"Entity "+cEntidNum+" equal origin",""		,""				,cX3Usado,"",""		,1,xReserv5,"","","",""	,""	,""	,"","","","","","",""										,"",""		,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Origin ."}
	PutHelp("PCV5_E"+cEntidNum+"ORI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Origem Fim."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " final Origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Origin final ."}
	PutHelp("PCV5_E"+cEntidNum+"FIM", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Destino."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Destino."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " Destiny ."}
	PutHelp("PCV5_E"+cEntidNum+"DES", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " igual origem."}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " igual origen."}
	aPHelpEng := {"Inform Entity " + cEntidNum + " equal origin ."}
	PutHelp("PCV5_E"+cEntidNum+"IGU", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CTA","00","CTA_ENTI"+cEntidNum,"C",TamSXG(cGrpNum)[1],0,"Entidade "+cEntidNum,"Entidad "+cEntidNum,"Entity "+cEntidNum,"Entidade "+cEntidNum,"Entidad "+cEntidNum,"Entity "+cEntidNum,"","",cX3Usado,"","",1,"","","","","","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum}
	aPHelpEng := {"Inform Entity " + cEntidNum}
	PutHelp("PCTA_ENTI"+cEntidNum, aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CTS","00","CTS_E"+cEntidNum+"INI","C",TamSXG(cGrpNum)[1],0,"Entid."+cEntidNum+" Ini","Entid."+cEntidNum+" Ini","Init. Ent."+cEntidNum,"Entidade "+cEntidNum+" Inicial"	,"Entidad "+cEntidNum+" Inicial","Initial Entity "+cEntidNum,"@!","Vazio() .Or. CtbEntExis()",cX3Usado,"",cF3,1,IIf(lInDB,FWConvRese("��"),"��"),"","","","S","","","","","","","","","","",cGrpNum,"","N"})
	Aadd(aSX3,{"CTS","00","CTS_E"+cEntidNum+"FIM","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Final","Ent."+cEntidNum+" Final","Final Ent."+cEntidNum,"Entidade "+cEntidNum+" Final"	,"Entidad "+cEntidNum+" Final"	,"Final Entity "+cEntidNum	,"@!","Vazio() .Or. CtbEntExis()",cX3Usado,"",cF3,1,IIf(lInDB,FWConvRese("��"),"��"),"","","","S","","","","","","","","","","",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Inicial "}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Inicial"}
	aPHelpEng := {"Inform Initial Entity  " + cEntidNum}
	PutHelp("PCTS_E"+cEntidNum+"INI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Final"}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Final"}
	aPHelpEng := {"Inform Final Entity  " + cEntidNum}
	PutHelp("PCTS_E"+cEntidNum+"FIM", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	Aadd(aSX3,{"CV1","00","CV1_E"+cEntidNum+"INI","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Ini"	,"Ent."+cEntidNum+" Inic.","Init. Ent."+cEntidNum,"Entidade "+cEntidNum+" Inicial"	,"Entidad "+cEntidNum+" Inicial","Initial Entity "+cEntidNum,"@!","Ctb390Vld()",cX3Usado,"",cF3,1,IIf(lInDB,FWConvRese("��"),"��"),"","","","","","","","","","","","","CtbMovSaldo('CT0',,'"+cEntidNum+"')","",cGrpNum,"","N"})
	Aadd(aSX3,{"CV1","00","CV1_E"+cEntidNum+"FIM","C",TamSXG(cGrpNum)[1],0,"Ent."+cEntidNum+" Final","Ent."+cEntidNum+" Final","Final Ent."+cEntidNum,"Entidade "+cEntidNum+" Final"	,"Entidad "+cEntidNum+" Final"	,"Final Entity "+cEntidNum	,"@!","Ctb390Vld()",cX3Usado,"",cF3,1,IIf(lInDB,FWConvRese("��"),"��"),"","","","","","","","","","","","","CtbMovSaldo('CT0',,'"+cEntidNum+"')","",cGrpNum,"","N"})

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Inicial"}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Inicial"}
	aPHelpEng := {"Inform Initial Entity  " + cEntidNum}
	PutHelp("PCV1_E"+cEntidNum+"INI", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

	aPHelpPor := {"Informe Entidade " + cEntidNum + " Final"}
	aPHelpSpa := {"Dile a la entidad " + cEntidNum + " Final"}
	aPHelpEng := {"Inform Final Entity  " + cEntidNum}
	PutHelp("PCV1_E"+cEntidNum+"FIM", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)
	//------------------------------------------
	// Campos padroes da contabilidade - FIM
	//------------------------------------------

	//--------------------------------------------------------------
	// Campos Debito e Credito padroes para os modulos selecionados
	//--------------------------------------------------------------
	For nY := 1 To Len(aTabALL) //La�o - M�dulos selecionaveis
		If aTabALL[nY][1][1] == .T. //Valida��o - M�dulo selecionado
			For nZ := 1 To Len(aTabAll[nY][2]) //La�o - Campos para gera��o
				If AliasInDic(aTabALL[nY][2][nZ])

					cFolder := Iif( aTabALL[nY][2][nZ] == "SED", "5", "" )
					Aadd(aSX3,{aTabALL[nY][2][nZ],"00",aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"DB","C",TamSXG(cGrpNum)[1],0,"Ent.Deb. "	+cEntidNum,"Ent.Deb. "	+cEntidNum,"Ent.Deb. "	+cEntidNum,"Ent. Cont�bil Debito "	+cEntidNum,"Ent. Contable Debito "	+cEntidNum,"Acc. Entity Debit "	+cEntidNum,"@!","CTB105EntC(,M->"+aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"DB,,'"+cEntidNum+"')",cX3Usado,"",cF3,1,xReserv6,"","","S","","","","","","","","","","","",cGrpNum,cFolder,"S"})
					Aadd(aSX3,{aTabALL[nY][2][nZ],"00",aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"CR","C",TamSXG(cGrpNum)[1],0,"Ent.Cred. "+cEntidNum,"Ent.Cred. "	+cEntidNum,"Cred.Ent. "	+cEntidNum,"Ent. Cont�bil Credito "	+cEntidNum,"Ent. Contable Credito "	+cEntidNum,"Acc. Entity Credit "+cEntidNum,"@!","CTB105EntC(,M->"+aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"CR,,'"+cEntidNum+"')",cX3Usado,"",cF3,1,xReserv6,"","","S","","","","","","","","","","","",cGrpNum,cFolder,"S"})

					aPHelpPor := {"Informe Ent. Cont�bil D�bito "		+ cEntidNum}
					aPHelpSpa := {"Dile a Ent. Contabilidad de d�bito "	+ cEntidNum}
					aPHelpEng := {"Inform Ent. accounting Debit "		+ cEntidNum}
					PutHelp("P"+aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"DB", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

					aPHelpPor := {"Informe Ent. Cont�bil Cr�dito "			+ cEntidNum}
					aPHelpSpa := {"Dile a Ent. Contabilidad de cr�dito "	+ cEntidNum}
					aPHelpEng := {"Inform Ent. accounting credit "			+ cEntidNum}
					PutHelp("P"+aTabALL[nY][3][nZ]+"_EC"+cEntidNum+"CR", aPHelpPor, aPHelpEng, aPHelpSpa, .T.)

				EndIf
			Next nZ
		EndIf
	Next nY

	//---------------------------
	// Campos especificos do PCO
	//---------------------------
	If lChkPCO
		Aadd(aSX3,{'AK2','00','AK2_ENT' + cEntidNum,'C',TamSXG(cGrpNum)[1],0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','Vazio() .Or. CTB105EntC(,M->AK2_ENT'+cEntidNum+',,"'+cEntidNum+'")',cX3Usado,'',cF3,1,IIf(lInDB,FWConvRese(Chr(150) + Chr(192)),Chr(150) + Chr(192)),'','S','S','S','A','R','N','','','','','','','',cGrpNum,'','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAK2_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AKC','00','AKC_ENT' + cEntidNum,'C',60,0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','PcoVldForm()',cX3Usado,'','CT0001',1,IIf(lInDB,FWConvRese(Chr(132) + Chr(128)),Chr(132) + Chr(128)),'','','S','S','A','R','N','','','','','','','','','','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAKC_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AKD','00','AKD_ENT' + cEntidNum,'C',TamSXG(cGrpNum)[1],0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','Vazio() .Or. CTB105EntC(,M->AKD_ENT'+cEntidNum+',,"'+cEntidNum+'")',cX3Usado,'',cF3,1,IIf(lInDB,FWConvRese(Chr(150) + Chr(192)),Chr(150) + Chr(192)),'','','S','S','A','R','N','','','','','','','',cGrpNum,'','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAKD_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'ALJ','00','ALJ_ENT' + cEntidNum,'C',TamSXG(cGrpNum)[1],0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','Vazio() .Or. CTB105EntC(,M->ALJ_ENT'+cEntidNum+',,"'+cEntidNum+'")',cX3Usado,'',cF3,1,IIf(lInDB,FWConvRese(Chr(150) + Chr(192)),Chr(150) + Chr(192)),'','','S','S','A','R','N','','','','','','','',cGrpNum,'','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PALJ_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AMJ','00','AMJ_ENT' + cEntidNum,'C',TamSXG(cGrpNum)[1],0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','Vazio() .Or. CTB105EntC(,M->AMJ_ENT'+cEntidNum+',,"'+cEntidNum+'")',cX3Usado,'',cF3,1,IIf(lInDB,FWConvRese(Chr(150) + Chr(192)),Chr(150) + Chr(192)),'','','S','S','A','R','N','','','','','','','',cGrpNum,'','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAMJ_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AMK','00','AMK_ENT' + cEntidNum,'C',60,0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','PcoVldForm()',cX3Usado,'','CT0001',1,IIf(lInDB,FWConvRese(Chr(132) + Chr(128)),Chr(132) + Chr(128)),'','','S','S','A','R','N','','','','','','','','','','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAMK_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AKI','00','AKI_ENT' + cEntidNum,'C',60,0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','PcoVldForm()',cX3Usado,'','CT0001',1,IIf(lInDB,FWConvRese(Chr(134) + Chr(128)),Chr(134) + Chr(128)),'','','S','S','A','R','N','','','','','','','','','','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAKI_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)

		Aadd(aSX3,{'AMZ','00','AMZ_ENT' + cEntidNum,'C',TamSXG(cGrpNum)[1],0,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'Entidade ' + cEntidNum,'Ente ' + cEntidNum,'Entity ' + cEntidNum,'@!','Vazio() .Or. CTB105EntC(,M->AMZ_ENT'+cEntidNum+',,"'+cEntidNum+'")',cX3Usado,'',cF3,1,IIf(lInDB,FWConvRese(Chr(254) + Chr(192)),Chr(254) + Chr(192)),'','','S','S','A','R','N','','','','','','','',cGrpNum,'','S'})
		aPHelpPor := {"Informe Ent. Cont�bil " + cEntidNum}
		aPHelpSpa := {"Dile a Ent. Contabilidad " + cEntidNum}
		aPHelpEng := {"Inform Ent. accounting " + cEntidNum}
		PutHelp("PAMZ_ENT"+cEntidNum,aPHelpPor,aPHelpEng,aPHelpSpa,.T.)
	EndIf

Next nX

//----------------------------------------------------------
// Identifica ordem correta para inclus�o dos campos no SX3
//----------------------------------------------------------
For nX := 1 to Len(aSX3)

	SX3->(DbSetOrder(2)) // X3_CAMPO
	If SX3->(MsSeek(aSX3[nX,3]))
		aSX3[nX,2] := SX3->X3_ORDEM
	Else
		If (nY := aScan(aDisp, {|x| x[1] == aSX3[nX,1]})) == 0
			AADD(aDisp, { aSX3[nX,1], {} } )
			nY := Len(aDisp)
			SX3->(dbSetOrder(1)) // X3_ARQUIVO+X3_ORDEM
			For nZ := 1 to Val(RetAsc("Z9",2,.F.))
				cOrdem := RetAsc(Alltrim(str(nZ)),2,.T.)
				If !SX3->(MsSeek( aSX3[nX,1]+cOrdem ))
					AADD( aDisp[nY][2], cOrdem )
				EndIf
			Next
		EndIf
		If Len(aDisp[nY][2])<1
			cOrdem := "ZZ"
		Else
			cOrdem := aDisp[nY][2][1]
			ADel(aDisp[nY][2],1)
			ASize(aDisp[nY][2], len(aDisp[nY][2])-1)
		EndIf
		aSX3[nX,2] := cOrdem
	EndIf

Next nX

oProcess:SetRegua2(Len(aSX3))

//------------------------------
// Atualiza dicion�rio de dados
//------------------------------
SX3->( DbSetOrder(2) )

For nContItem := 1 To Len(aSX3)

	cNomeCamp := AllTrim(aSX3[nContItem,3])

	lGrava := SX3->(!dbSeek(cNomeCamp))
	lSX3	:= .T.
	If ! (aSX3[nContItem,1] $ cAlias)
		If Len(cAlias) == 104
			cAlias += aSX3[nContItem,1]+"/"+CRLF+" "
		Else
			cAlias += aSX3[nContItem,1]+"/"
		EndIf
		If Ascan(aArqUpd,aSX3[nContItem,1]) == 0
			aAdd(aArqUpd,aSX3[nContItem,1])
		EndIf
	EndIf

	RecLock("SX3",lGrava)
	For nContEstr :=1 To Len(aSX3[nContEstr])
		If FieldPos(aEstrut[nContEstr])>0
			FieldPut(FieldPos(aEstrut[nContEstr]),aSX3[nContItem,nContEstr])
		EndIf
	Next nContEstr
	dbCommit()
	MsUnLock()
	oProcess:IncRegua2(STR0033)

Next nContItem

If lSX3
	cTexto := STR0007+cAlias+CRLF //'Tabelas atualizadas : '
EndIf

CTBUpdField("SX3", 2, "CT0_ENTIDA", "X3_WHEN", "CTB050WHEN()")
CTBUpdField("SX3", 2, "CT0_ALIAS ", "X3_WHEN", "!(M->CT0_ALIAS $ 'CT1|CTT|CTD|CTH')")
CTBUpdField("SX3", 2, "CTJ_ET05CR", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC05CR,,'05')")
CTBUpdField("SX3", 2, "CTJ_ET05DB", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC05DB,,'05')")
CTBUpdField("SX3", 2, "CTJ_ET06CR", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC06CR,,'06')")
CTBUpdField("SX3", 2, "CTJ_ET06DB", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC06DB,,'06')")
CTBUpdField("SX3", 2, "CTJ_ET07CR", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC07CR,,'07')")
CTBUpdField("SX3", 2, "CTJ_ET07DB", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC07DB,,'07')")
CTBUpdField("SX3", 2, "CTJ_ET08CR", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC08CR,,'08')")
CTBUpdField("SX3", 2, "CTJ_ET08DB", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC08DB,,'08')")
CTBUpdField("SX3", 2, "CTJ_ET09CR", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC09CR,,'09')")
CTBUpdField("SX3", 2, "CTJ_ET09DB", "X3_VALID", "CTB1XXEntC(,M->CTJ_EC09DB,,'09')")
CTBUpdField("SX3", 2, "CV0_LUCPER", "X3_VALID", "CTB105EntC(,M->CV0_LUCPER,,M->CV0_PLANO)")
CTBUpdField("SX3", 2, "CV0_PONTE ", "X3_VALID", "CTB105EntC(,M->CV0_PONTE,,M->CV0_PLANO)")

CTBUpdField("SX3", 2, "AK2_ENT05", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AK2_ENT05,,"05")')
CTBUpdField("SX3", 2, "AK2_ENT06", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AK2_ENT06,,"06")')
CTBUpdField("SX3", 2, "AK2_ENT07", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AK2_ENT07,,"07")')
CTBUpdField("SX3", 2, "AK2_ENT08", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AK2_ENT08,,"08")')
CTBUpdField("SX3", 2, "AK2_ENT09", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AK2_ENT09,,"09")')
CTBUpdField("SX3", 2, "AKD_ENT05", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AKD_ENT05,,"05")')
CTBUpdField("SX3", 2, "AKD_ENT06", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AKD_ENT06,,"06")')
CTBUpdField("SX3", 2, "AKD_ENT07", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AKD_ENT07,,"07")')
CTBUpdField("SX3", 2, "AKD_ENT08", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AKD_ENT08,,"08")')
CTBUpdField("SX3", 2, "AKD_ENT09", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AKD_ENT09,,"09")')
CTBUpdField("SX3", 2, "ALJ_ENT05", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->ALJ_ENT05,,"05")')
CTBUpdField("SX3", 2, "ALJ_ENT06", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->ALJ_ENT06,,"06")')
CTBUpdField("SX3", 2, "ALJ_ENT07", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->ALJ_ENT07,,"07")')
CTBUpdField("SX3", 2, "ALJ_ENT08", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->ALJ_ENT08,,"08")')
CTBUpdField("SX3", 2, "ALJ_ENT09", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->ALJ_ENT09,,"09")')
CTBUpdField("SX3", 2, "AMJ_ENT05", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMJ_ENT05,,"05")')
CTBUpdField("SX3", 2, "AMJ_ENT06", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMJ_ENT06,,"06")')
CTBUpdField("SX3", 2, "AMJ_ENT07", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMJ_ENT07,,"07")')
CTBUpdField("SX3", 2, "AMJ_ENT08", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMJ_ENT08,,"08")')
CTBUpdField("SX3", 2, "AMJ_ENT09", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMJ_ENT09,,"09")')
CTBUpdField("SX3", 2, "AMZ_ENT05", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMZ_ENT05,,"05")')
CTBUpdField("SX3", 2, "AMZ_ENT06", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMZ_ENT06,,"06")')
CTBUpdField("SX3", 2, "AMZ_ENT07", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMZ_ENT07,,"07")')
CTBUpdField("SX3", 2, "AMZ_ENT08", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMZ_ENT08,,"08")')
CTBUpdField("SX3", 2, "AMZ_ENT09", "X3_VALID", 'Vazio() .Or. CTB105EntC(,M->AMZ_ENT09,,"09")')

If FindFunction("MTPVLSOLEC")
	CTBUpdField("SX3", 2, "C1_EC05CR", "X3_VALID", "CTB105EntC(,M->C1_EC05CR,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC05DB", "X3_VALID", "CTB105EntC(,M->C1_EC05DB,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC06CR", "X3_VALID", "CTB105EntC(,M->C1_EC06CR,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC06DB", "X3_VALID", "CTB105EntC(,M->C1_EC06DB,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC07CR", "X3_VALID", "CTB105EntC(,M->C1_EC07CR,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC07DB", "X3_VALID", "CTB105EntC(,M->C1_EC07DB,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC08CR", "X3_VALID", "CTB105EntC(,M->C1_EC08CR,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC08DB", "X3_VALID", "CTB105EntC(,M->C1_EC08DB,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC09CR", "X3_VALID", "CTB105EntC(,M->C1_EC09CR,,'09') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "C1_EC09DB", "X3_VALID", "CTB105EntC(,M->C1_EC09DB,,'09') .And. MTPVLSOLEC()")

	CTBUpdField("SX3", 2, "CP_EC05CR", "X3_VALID", "CTB105EntC(,M->CP_EC05CR,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC05DB", "X3_VALID", "CTB105EntC(,M->CP_EC05DB,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC06CR", "X3_VALID", "CTB105EntC(,M->CP_EC06CR,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC06DB", "X3_VALID", "CTB105EntC(,M->CP_EC06DB,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC07CR", "X3_VALID", "CTB105EntC(,M->CP_EC07CR,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC07DB", "X3_VALID", "CTB105EntC(,M->CP_EC07DB,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC08CR", "X3_VALID", "CTB105EntC(,M->CP_EC08CR,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC08DB", "X3_VALID", "CTB105EntC(,M->CP_EC08DB,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC09CR", "X3_VALID", "CTB105EntC(,M->CP_EC09CR,,'09') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CP_EC09DB", "X3_VALID", "CTB105EntC(,M->CP_EC09DB,,'09') .And. MTPVLSOLEC()")

	CTBUpdField("SX3", 2, "CX_EC05CR", "X3_VALID", "CTB105EntC(,M->CX_EC05CR,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC05DB", "X3_VALID", "CTB105EntC(,M->CX_EC05DB,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC06CR", "X3_VALID", "CTB105EntC(,M->CX_EC06CR,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC06DB", "X3_VALID", "CTB105EntC(,M->CX_EC06DB,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC07CR", "X3_VALID", "CTB105EntC(,M->CX_EC07CR,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC07DB", "X3_VALID", "CTB105EntC(,M->CX_EC07DB,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC08CR", "X3_VALID", "CTB105EntC(,M->CX_EC08CR,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC08DB", "X3_VALID", "CTB105EntC(,M->CX_EC08DB,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC09CR", "X3_VALID", "CTB105EntC(,M->CX_EC09CR,,'09') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "CX_EC09DB", "X3_VALID", "CTB105EntC(,M->CX_EC09DB,,'09') .And. MTPVLSOLEC()")

	CTBUpdField("SX3", 2, "GS_EC05CR", "X3_VALID", "CTB105EntC(,M->GS_EC05CR,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC05DB", "X3_VALID", "CTB105EntC(,M->GS_EC05DB,,'05') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC06CR", "X3_VALID", "CTB105EntC(,M->GS_EC06CR,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC06DB", "X3_VALID", "CTB105EntC(,M->GS_EC06DB,,'06') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC07CR", "X3_VALID", "CTB105EntC(,M->GS_EC07CR,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC07DB", "X3_VALID", "CTB105EntC(,M->GS_EC07DB,,'07') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC08CR", "X3_VALID", "CTB105EntC(,M->GS_EC08CR,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC08DB", "X3_VALID", "CTB105EntC(,M->GS_EC08DB,,'08') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC09CR", "X3_VALID", "CTB105EntC(,M->GS_EC09CR,,'09') .And. MTPVLSOLEC()")
	CTBUpdField("SX3", 2, "GS_EC09DB", "X3_VALID", "CTB105EntC(,M->GS_EC09DB,,'09') .And. MTPVLSOLEC()")

	CTBUpdField("SX3", 2, "DBK_EC05CR", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC05CR'),,'05') .Or. FwFldGet('DBK_EC05CR')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC05DB", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC05DB'),,'05') .Or. FwFldGet('DBK_EC05DB')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC06CR", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC06CR'),,'06') .Or. FwFldGet('DBK_EC06CR')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC06DB", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC06DB'),,'06') .Or. FwFldGet('DBK_EC06DB')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC07CR", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC07CR'),,'07') .Or. FwFldGet('DBK_EC07CR')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC07DB", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC07DB'),,'07') .Or. FwFldGet('DBK_EC07DB')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC08CR", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC08CR'),,'08') .Or. FwFldGet('DBK_EC08CR')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC08DB", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC08DB'),,'08') .Or. FwFldGet('DBK_EC08DB')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC09CR", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC09CR'),,'09') .Or. FwFldGet('DBK_EC09CR')=='*'")
	CTBUpdField("SX3", 2, "DBK_EC09DB", "X3_VALID", "Vazio() .Or. CTB105EntC(FwFldGet('DBK_EC09DB'),,'09') .Or. FwFldGet('DBK_EC09DB')=='*'")
EndIf

RestArea(aAreaSX3)
RestArea(aArea)
Return cTexto

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTAtuSIX �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao de processamento da gravacao do SIX                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTAtuSIX()
Local cTexto  := ''
Local lSIX    := .F.
Local lNew    := .F.
Local aSIX    := {}
Local aEstrut := {}
Local i       := 0
Local j       := 0
Local cAlias  := ''
Local aColsGet	:= ACLONE(oGetDados:aCols)
Local aHeader	:= ACLONE(oGetDados:aHeader)
Local nPosPlano	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ID"})
Local cEntidNum	:= ""
Local nX		:= 0

aEstrut := {"INDICE","ORDEM","CHAVE","DESCRICAO","DESCSPA","DESCENG","PROPRI","F3","NICKNAME","SHOWPESQ"}

// Indice
Aadd(aSIX,{	"CV0","3","CV0_FILIAL+CV0_PLANO+CV0_ENTSUP",;
			"Plano Cont�b + Entidade Superior",;
			"Plan Contabl + Entidad Superior",;
			"Accoun. Plan + Superior Entity",;
			"S","","","S"})


If FindFunction("CtbSIXCTA") .And. CtbSIXCTA() //Se as existentes j� tem indice, adiciona as demais, sen�o precisa rodar UPD
	For nX := 1 To Len(aColsGet)
		cEntidNum	:= AllTrim(aColsGet[nX][nPosPlano]) //Numero corrente da entidade
		Aadd(aSIX,{"CTA",Soma1(AllTrim(STR(VAL(cEntidNum)))),"CTA_FILIAL+CTA_ENTI"+cEntidNum,"Entidade "+cEntidNum,"Entidad "+cEntidNum,"Entity "+cEntidNum,'S',"","CTA_ENTI"+cEntidNum,'S'})
	Next nX
EndIf


ProcRegua(Len(aSIX))

dbSelectArea("SIX")
dbSetOrder(1)

For i:= 1 To Len(aSIX)
	If !Empty(aSIX[i,1])
		If !dbSeek(aSIX[i,1]+aSIX[i,2])
			lNew:= .T.
		Else
			lNew:= .F.
		EndIf

		If  lNew .OR.;
			!(UPPER(AllTrim(CHAVE))==UPPER(Alltrim(aSIX[i,3]))) .OR.;
			!(UPPER(AllTrim(NICKNAME))==UPPER(Alltrim(aSIX[i,9])))

			If Ascan(aArqUpd,aSIX[i,1]) == 0
				aAdd(aArqUpd,aSIX[i,1])
			EndIf
			lSIX := .T.

			If !(aSIX[i,1]$cAlias)
				cAlias += aSIX[i,1]+"/"
			EndIf

			RecLock("SIX",lNew)

			For j:=1 To Len(aSIX[i])
				If FieldPos(aEstrut[j])>0
					FieldPut(FieldPos(aEstrut[j]),aSIX[i,j])
				EndIf
			Next j
			dbCommit()
			MsUnLock()
		EndIf

		IncProc(STR0029) //"Atualizando Indices..."

	EndIf
Next i

If lSIX
	cTexto += STR0049+cAlias+CRLF //"Indices atualizados  : "
EndIf

Return cTexto

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTAtuCT0 �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao  de processamento da gravacao da Entidade           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTAtuCT0()
Local aColsGet  := ACLONE(oGetDados:aCols)
Local cTexto 	:= ''
Local aSaveArea := GetArea()
Local nCont	:= 0
Local cAlias
Local cAux
Local cCpoSup
Local cCpoChv
Local cOrd1
Local cInd1
Local cOrd2
Local cInd2
Local lEnte05 := if(cPaisLoc == "PER" .AND. nEntidIni == 5,.T.,.F. )


dbSelectArea("CV0")
dbSetOrder(1)//CV0_FILIAL+CV0_PLANO+CV0_CODIGO

dbSelectArea("CT0")
dbSetOrder(1)// CT0_FILIAL+CT0_ID

oProcess:SetRegua1(Len(aColsGet))

For nCont := 1 To Len(aColsGet)
	If !dbSeek(xFilial("CT0")+aColsGet[nCont][1]) .OR. (lEnte05 .AND. dbSeek(xFilial("CT0")+aColsGet[nCont][1]) )
		dbSelectArea("CV0")
		If !Empty(aColsGet[nCont][11]) .And. !dbSeek(xFilial("CV0")+aColsGet[nCont][11]) .OR. lEnte05
			if lEnte05
				if dbSeek(xFilial("CV0")+"01")
					RecLock("CV0", .F.)
						CV0->CV0_PLANO  := aColsGet[nCont][11]
					MsUnlock("CV0")
				Endif
			Else
			RecLock("CV0", .T.)
			CV0->CV0_FILIAL := xFilial("CV0")
			CV0->CV0_PLANO  := aColsGet[nCont][11]
			CV0->CV0_DESC 	:= aColsGet[nCont][12]
			CV0->CV0_DTIBLQ := Ctod("")
			CV0->CV0_DTFBLQ := dDatabase
			CV0->CV0_DTIEXI := Ctod("")
			CV0->CV0_DTFEXI := Ctod("")
			MsUnlock("CV0")
			Endif
		EndIf
		dbSelectArea("CT0")
		if lEnte05
			RecLock("CT0", .F.)
				CT0->CT0_ENTIDA := aColsGet[nCont][11]
			MsUnlock("CT0")		
		Else
		RecLock("CT0",.T.)
				CT0->CT0_FILIAL := xFilial("CT0")
				CT0->CT0_ID	:= aColsGet[nCont][1]
				CT0->CT0_DESC   := aColsGet[nCont][2]
				CT0->CT0_DSCRES := aColsGet[nCont][3]
				CT0->CT0_CONTR  := aColsGet[nCont][4]
				CT0->CT0_ALIAS  := aColsGet[nCont][5]
				CT0->CT0_CPOCHV := aColsGet[nCont][6]
				CT0->CT0_CPODSC := aColsGet[nCont][7]
				CT0->CT0_ENTIDA := aColsGet[nCont][11]
				CT0->CT0_OBRIGA := "2"
				CT0->CT0_CPOSUP := aColsGet[nCont][8]
				CT0->CT0_GRPSXG := aColsGet[nCont][9]
				CT0->CT0_F3ENTI := aColsGet[nCont][10]
				MsUnlock("CT0")
			Endif
		cAlias  := Alltrim(aColsGet[nCont][5])

		If cAlias<>"CV0"
			cCpoSup := aColsGet[nCont][8]
			cCpoChv := aColsGet[nCont][6]
			If Left(cAlias,1)=='S'
				cAux:=substr(cAlias,2,2)
			Else
				cAux:=cAlias
			EndIf
			cOrd1 := ''
			cInd1 := Alltrim(cAux+"_FILIAL+"+cCpoChv)
			cOrd2 := ''
			cInd2 := Alltrim(cAux+"_FILIAL+"+cCpoSup)

			dbSelectArea("SIX")
			dbSetOrder(1)

			SIX->(dbSeek(cAlias))
			Do While !SIX->(Eof()) .And. SIX->INDICE==cAlias
				If Empty(cOrd1) .And. Left(SIX->CHAVE,len(cInd1))==cInd1
					cOrd1 := SIX->ORDEM
				EndIf
				If Empty(cOrd2) .And. Left(SIX->CHAVE,len(cInd2))==cInd2
					cOrd2 := SIX->ORDEM
				EndIf
				SIX->(dbSkip())
			EndDo

			If Empty(cOrd1)
				dbSelectArea("SX3")
				dbSetOrder(2)
				SX3->(dbSeek(cCpoChv))
				dbSelectArea("SIX")
				SIX->(dbSeek(cAlias))
				Do While !SIX->(Eof()) .And. SIX->INDICE==cAlias
					cOrd1 := SIX->ORDEM
					SIX->(dbSkip())
				EndDo
				cOrd1:=Soma1(cOrd1)
				RecLock("SIX",.T.)
				SIX->INDICE		:= cAlias
				SIX->ORDEM		:= cOrd1
				SIX->CHAVE		:= cInd1
				SIX->DESCRICAO	:= SX3->X3_TITULO
				SIX->DESCSPA	:= SX3->X3_TITSPA
				SIX->DESCENG	:= SX3->X3_TITENG
				SIX->PROPRI		:= 'U'
				SIX->NICKNAME	:= cAlias+"INDTMP1"
				SIX->SHOWPESQ	:= 'N'
				SIX->(dbCommit())
				SIX->(MsUnLock())
				If select(cAlias)>0
					dbSelectArea(cAlias)
					dbCloseArea()
				EndIf
				dbSelectArea(cAlias)
			EndIf

			If Empty(cOrd2)
				dbSelectArea("SX3")
				dbSetOrder(2)
				SX3->(dbSeek(cCpoSup))
				dbSelectArea("SIX")
				SIX->(dbSeek(cAlias))
				Do While !SIX->(Eof()) .And. SIX->INDICE==cAlias
					cOrd2 := SIX->ORDEM
					SIX->(dbSkip())
				EndDo
				cOrd2:=Soma1(cOrd2)
				RecLock("SIX",.T.)
				SIX->INDICE		:= cAlias
				SIX->ORDEM		:= cOrd2
				SIX->CHAVE		:= cInd2
				SIX->DESCRICAO	:= SX3->X3_TITULO
				SIX->DESCSPA	:= SX3->X3_TITSPA
				SIX->DESCENG	:= SX3->X3_TITENG
				SIX->PROPRI		:= 'U'
				SIX->NICKNAME	:= cAlias+"INDTMP2"
				SIX->SHOWPESQ	:= 'N'
				SIX->(dbCommit())
				SIX->(MsUnLock())
				If select(cAlias)>0
					dbSelectArea(cAlias)
					dbCloseArea()
				EndIf
				dbSelectArea(cAlias)
			EndIf

		EndIf

		dbSelectArea("CT0")

	EndIf
	oProcess:IncRegua2(STR0048)
Next

RestArea(aSaveArea)
Return cTexto

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTGetNum  �Autor  �Microsiga           � Data �  02/11/10  ���
�������������������������������������������������������������������������͹��
���Desc.     � Monta gets para informa��o do n�mero, entidade inicial e   ���
���          � m�dulos para gera��o.                                      ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function EntGetNum()
Local oPanel	:= Nil
Local nMaxEnt	:= GETMAXENT()
Local oSayQtdEnt, oGetQtdEnt, oSayEntIni, oGetEntIni, oCheckRef, oCheckCTB, oCheckATF, oCheckCOM,;
      oCheckEST, oCheckFAT, oCheckFIN, oCheckGCT, oCheckPCO,oCheckVGE

If lMaxEnt
	lChkRefaz := .T.
	nQtdEntid := 0
	nEntidIni := 0
EndIf

If nEntidIni == 5
	lChkRefaz := .F.
EndIf


//------------------------------------------------------------
// Campos para definicao da quantidade de entidades e modulos
//------------------------------------------------------------
oPanel   := oWizard:oMPanel[oWizard:nPanel]

oSayQtdEnt	:= TSay():New(005,008,{||STR0017},oPanel,,,,,,.T.,,,,,,,,,,) //"Total de Entidades a serem criadas:"
oGetQtdEnt	:= TGet():New(015,008,{|u| If(PCount() > 0,nQtdEntid := u,nQtdEntid)},oPanel,015,009,'@e 99',{|| ENTVldNum(nQtdEntid,nMaxEnt) }	,,,,,,.T.,,,{||!lMaxEnt},,,,,.F.,,"nQtdEntid",,,,,,,,,)

oSayEntIni	:= TSay():New(035,008,{||STR0018},oPanel,,,,,,.T.,,,,,,,,,,) //"Numera��o da primeira entidade a ser criada:"
oGetEntIni	:= TGet():New(045,008,{|u| If(PCount() > 0,nEntidIni := u,nEntidIni)},oPanel,015,009,'@ 99',{|| .T. },,,,,,.T.,,,{||.F.},,,,.F.,.F.,,"nEntidIni",,,,,,,,,)
oCheckRef	:= TCheckBox():New(065,008,STR0066,bSETGET(lChkRefaz)	,oPanel,150,009,,,,,,,,.T.,,,{||!lMaxEnt}) //"Cria campo para entidades j� existentes"

oSayRefaz	:= TSay():New(080,009,{||STR0019},oPanel,,,,,,.T.,,,,,,,,,,) //"Definir os m�dulos para cria��o:"
oCheckCTB   := TCheckBox():New(090,008,STR0043,bSETGET(lChkCTB)		,oPanel,050,009,,,,,,,,.T.,,,{|| .F. })	   //"Contabilidade"
oCheckATF	:= TCheckBox():New(100,008,STR0041,bSETGET(lChkATF)		,oPanel,050,009,,,,,,,,.T.,,,) //"Ativo Fixo"
oCheckCOM	:= TCheckBox():New(110,008,STR0042,bSETGET(lChkCOM)		,oPanel,050,009,,,,,,,,.T.,,,) //"Compras"
oCheckEST	:= TCheckBox():New(120,008,STR0044,bSETGET(lChkEST)		,oPanel,050,009,,,,,,,,.T.,,,) //"Estoque"
oCheckFAT	:= TCheckBox():New(130,008,STR0045,bSETGET(lChkFAT)		,oPanel,050,009,,,,,,,,.T.,,,) //"Faturamento"
oCheckFIN	:= TCheckBox():New(090,108,STR0046,bSETGET(lChkFIN)		,oPanel,050,009,,,,,,,,.T.,,,) //"Financeiro"
oCheckGCT	:= TCheckBox():New(100,108,STR0080,bSETGET(lChkGCT)		,oPanel,100,009,,,,,,,,.T.,,,) //"Gest�o de Contratos"
oCheckPCO	:= TCheckBox():New(110,108,STR0081,bSETGET(lChkPCO)		,oPanel,100,009,,,,,,,,.T.,,,) //"Controle Or�ament�rio"
oCheckVGE	:= TCheckBox():New(120,108,STR0084,bSETGET(lChkVGE)		,oPanel,100,009,,,,,,,,.T.,,,) //"Viagens"

If lMaxEnt
	oCheckRef:bWhen  := {|| .F. }
	oGetQtdEnt:bWhen := {|| .F. }
	oGetEntIni:bWhen  := {|| .F. }
EndIf

If nEntidIni == 5
	oCheckRef:bWhen  := {|| .F. }
EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTGetDesc �Autor  �Microsiga          � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Monta grid para informa��o das descri��es das entidades   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function EntGetDesc()
Local nContItem := 1
Local oPanel 	:= oWizard:oMPanel[oWizard:nPanel]
Local nEntNew   := nEntidIni
Local cLinOk	:= "CT910LOk()"

//��������������������������������������������������������������Ŀ
//� Monta grid para informativo das descri��es por entidade      �
//����������������������������������������������������������������
Local aHeader 	:= {}
Local aCols 	:= {}

//��������������������������������������������������������������Ŀ
//� Cria aHeader e aCols da GetDados                             �
//����������������������������������������������������������������
//				cTitulo	,cCampo			,cPicture	,nTamanho	,nDecimais	,cValida��o										,cUsado	,cTipo	,cF3		,cCntxt	,cCBox								,cRelacao
Aadd(aHeader,{	STR0030	,"CT0_ID"		,"!!"		,02			,0			,".F."											,"�"	,"C"	," "		,"R"	,""									,""		}) //Item
Aadd(aHeader,{	STR0031	,"CT0_DESC"		,"@!"		,30			,0			,"NaoVazio()"									,"�"	,"C"	," "		,"R"	,""									,""     }) //Descri��o
Aadd(aHeader,{	STR0052	,"CT0_DSCRES"	,"@!"		,10			,0			,"NaoVazio()"									,"�"	,"C"	," "		,"R"	,""									,""     }) //Descricao Resumida
Aadd(aHeader,{	STR0032	,"CT0_CONTR"	,"@!"		,1			,0			,"NaoVazio().And. Pertence('12')"				,"�"	,"C"	," "		,"R"	,""									,""     }) //Controla
Aadd(aHeader,{	STR0053	,"CT0_ALIAS"	,"@!"		,3			,0			,"NaoVazio().And. ValidX2Alias(M->CT0_ALIAS)"	,"�"	,"C"	," "		,"R"	,""									,""     }) //Alias
Aadd(aHeader,{	STR0054	,"CT0_CPOCHV"	,"@!"		,10			,0			,"NaoVazio().And. ValidX3Cpo(M->CT0_CPOCHV)"	,"�"	,"C"	,"CT0SX3"	,"R"	,""									,""     }) //Campo Chave
Aadd(aHeader,{	STR0055	,"CT0_CPODSC"	,"@!"		,10			,0			,"NaoVazio().And. ValidX3Cpo(M->CT0_CPODSC)"	,"�"	,"C"	,"CT0SX3"	,"R"	,""									,""     }) //Desc. Campo
Aadd(aHeader,{	STR0059	,"CT0_CPOSUP"	,"@!"		,10			,0			,"Vazio() .Or. ValidX3Cpo(M->CT0_CPOSUP)"		,"�"	,"C"	,"CT0SX3"	,"R"	,""									,""     }) //Cpo.Ent.Sup.
Aadd(aHeader,{	STR0060	,"CT0_GRPSXG"	,"@!"		,3			,0			,"Vazio() .Or. ValidSXG(M->CT0_GRPSXG)"			,"�"	,"C"	," "		,"R"	,IIF(!lChkRefaz,AdmCBGrupo(),"")	,""     }) //Grp.Campos
Aadd(aHeader,{	STR0061	,"CT0_F3ENTI"	,"@!"		,6			,0			,"Vazio() .Or. ValidSXB(M->CT0_F3ENTI)"			,"�"	,"C"	," "		,"R"	,IIF(!lChkRefaz,AdmCBCPad(),"")		,""     }) //Cons. Padrao
Aadd(aHeader,{	STR0062	,"CT0_ENTIDA"	,"@!"		,2			,0			,".F."											,"�"	,"C"	," "		,"R"	,""									,""     }) //Plano
Aadd(aHeader,{	STR0063	,"CV0_DESC"		,"@!"		,30			,0			,""												,"�"	,"C"	," "		,"R"	,""									,""     }) //Desc. Plano

If lChkRefaz
	aCols := CT910RACol(aHeader)
Else
	For nContItem := 1 to nQtdEntid
		Do Case //obten��o do nro do grupo
			Case nEntNew == 5  //Entidade 05
				cGrpNum := "040"
			Case nEntNew == 6  //Entidade 06
				cGrpNum := "042"
			Case nEntNew == 7  //Entidade 07
				cGrpNum := "043"
			Case nEntNew == 8  //Entidade 08
				cGrpNum := "044"
			Case nEntNew == 9  //Entidade 09
				cGrpNum := "045"
		EndCase
		aAdd(aCols,{StrZero(nEntNew,2,0), Space(TamSx3("CT0_DESC")[1]), Space(10), "1", "CV0", PADR("CV0_CODIGO",TamSx3("CT0_CPOCHV")[1]), PADR("CV0_DESC",TamSx3("CT0_CPODSC")[1]), PADR("CV0_ENTSUP",TamSx3("CT0_CPOSUP")[1]), cGrpNum, "CV0   ", StrZero(nEntNew,2,0), Upper(STR0062)+" "+StrZero(nEntNew,2,0), .F.}) //Plano
		nEntNew ++
	Next nContItem
EndIf

If lChkRefaz
	nOpcX := 0
Else
	nOpcX := GD_UPDATE
EndIf

//           MsNewGetDados():New(nSuperior	,nEsquerda	,nInferior	,nDireita	,nOpc	,cLinOk	,cTudoOk	,cIniCpos	,aAlterGDa					,nFreeze	,nMax	,cFieldOk	,cSuperDel	,cDelOk	,oDLG	,aHeader	,aCols)
oGetDados := MsNewGetDados():New(005		,008		,105		,255		,nOpcX	,cLinOk	,			,			,{"CT0_DESC","CT0_DSCRES"}	,			,5		,			,			,		,oPanel	,aHeader	,aCols)
oGetDados:SetEditLine(.F.)
SX3->(dbCloseArea())

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWIZREGU  �Autor  �Microsiga         � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Realiza o controle do obejto process da rotina             ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWIZREGU
Private oProcess
// Executa o processamento dos arquivos
dbSelectArea("SX2")
dbCloseArea()
dbSelectArea("SIX")
dbCloseArea()
dbSelectArea("SX3")
dbCloseArea()
oProcess:=	MsNewProcess():New( {|lEnd| ENTWIZPROC(oProcess) } )
oProcess:Activate()
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GETMAXENT  �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Retorna a partir de qual Entidade poder� ser realizada a   ���
���          � criacao                                                    ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function GETMAXENT()
Local nEntidIni := 0
Local nEntidMax := 0
Local aAreaSX2 	:= {}
Local lExistCpo := .T.

aAreaSX2 := SX2->(GetArea())

dbSelectArea("SX2")
dbSetOrder(1)
If !MsSeek("CT0")
	RestArea(aAreaSX2)
	MsgInfo(STR0057+" CT0 "+STR0058,STR0001) //"Tabela"###"n�o encontrada"###"Aten��o"
	Return(nEntidMax)
EndIf

DbSelectArea("CT0")
If !Empty(Select("CT0"))
	lOpen := .T.
EndIf

If !lOpen
	MsgInfo(STR0050,STR0001) //"Nao foi possivel a abertura da tabela de empresas de forma exclusiva!"###"Atencao!"
Else
	DbSelectArea("CT0")
	DbSetOrder(1)
	DbSeek(xFilial()+"01")
	While CT0->(!EOF())
		lExistCpo := .T.
		IF Val(CT0->CT0_ID) > 0

			If cPaisLoc $ "PER|COL" .and. Val(CT0->CT0_ID) >= 5 // Peru e Col�mbia possuem a quinta entidade em base padr�o (N.I.T.)
				lExistCpo := CtbEntIniVar(CT0->CT0_ID)
			EndIf
			nEntidIni := Val(CT0->CT0_ID)
			If nEntidIni > nEntidMax .and. IIF(cPaisLoc $ "PER|COL",lExistCpo, .T.)
				nEntidMax := nEntidIni
			EndIf
		Else
			Exit
		EndIf

		CT0->(DbSkip())
	EndDo

	If nEntidMax == 0
		MsgInfo(STR0036,STR0001) //"Nao foi possivel determinar a quantidade de entidades
	Else                         //parametrizadas no sistema!"###"Atencao!"
		nEntidMax++				 //N�mero da pr�xima entidade
	EndIf

Endif

RestArea(aAreaSX2)
Return nEntidMax
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTVldNum �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Validacao do numero de entidades a serem geradas ao sele-  ���
���          � cionar outro campo.                                        ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTVldNum(nNum, nEntidIni)
Local lRet := .T.
Local nEntidMax := 9 - (nEntidIni - 1)

If nNum > nEntidMax
	lRet := .F.
	MsgInfo(STR0038+AllTrim(Str(nEntidMax,0))+STR0039,STR0001) //'O n�mero m�ximo de entidades adicionais permitidas no momento � de: ' ### ' entidades. Ajuste o n�mero'###'Atencao'
ElseIf !Empty(nNum) .And. lChkRefaz
	lRet := .F.
	MSGINFO(STR0077,STR0021) //"A op��o refaz entidade n�o permite criar novas." ##"CTBWizard - Entidades"
ElseIf Empty(nNum) .And. !lChkRefaz
	lRet := .F.
	MsgInfo(STR0040,STR0001) //"� necess�rio informar pelo menos uma entidade!"###"Atencao"
EndIf

Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWZVLP2 �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Validacao dos dados inicias para parametrizacao de Entida- ���
���          � des na mudan�a de tela (next)                              ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWZVLP2()

Local aArea := GetArea()
Local lRet	:= .T.
Local nEntidMax := 9 - (nEntidIni - 1)

If lChkRefaz .And. !Empty(nQtdEntid)
	lRet := .F.
	MSGINFO(STR0067,STR0021) // "A op��o refaz entidade n�o permite criar novas."##"CTBWizard - Entidades"
ElseIf nEntidIni > 9 .Or. nQtdEntid > nEntidMax
	lRet := .F.
	MSGINFO(STR0035+AllTrim(Str(9,0))+STR0037,STR0021) //"A parametriza��o excede o limite de 09 entidades
	//configur�veis no sistema!" # "CTBWizard - Entidades"
Else
	If !lChkATF .And. !lChkCOM .And. !lChkCTB .And. !lChkEST .And. !lChkFAT .And. !lChkFIN .And. !lChkGCT .And. !lChkRefaz .And. !lChkPCO .And. !lChkVGE
		lRet := .F.
		MSGINFO(STR0047,STR0001) //"� necess�rio selecionar pelo menos um m�dulo!"###"Aten��o!"
	EndIf
ENDIF

RestArea(aArea)

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ENTWZVLP3 �Autor  �Microsiga           � Data �  02/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Validacao do grid de entidades a serem incluidas.          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � ENTWIZUPD                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ENTWZVLP3()
Local lRet 		:= .T.
Local aColsGet	:= ACLONE(oGetDados:aCols)
Local nX		:= 0

For nX := 1 to Len(aColsGet)
	lRet := CT910LOk(nX)
	If !lRet
		Exit
	ENdIf
Next nX

IF lRet
	lRet := MSGYESNO(STR0020,STR0021) //Confirma a parametriza��o das novas entidades # CTBWizard - Entidades
ENDIF

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValidX3Cpo   �Autor  �Microsiga        � Data �  05/03/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Valida a existencia do campo                               ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ValidX3Cpo(cCpo)
Local lRet      := .F.

If Empty(cCpo)
	lRet := .F.
	Help(" ",1,"NOMECPO")
Else
	dbSelectArea("SX3")
	dbSetOrder(2)
	dbSeek(cCpo)
	If !Found()
		Help(" ",1,"NOMECPO")
		lRet := .F.
	Else
		lRet := .T.
	EndIf
	dbSetOrder(1)
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValidX2Alias  �Autor  �Microsiga       � Data �  05/03/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Valida a existencia do Alias                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ValidX2Alias(cAlias)
Local lRet := .T.

dbSelectArea("SX2")
Set Filter to
dbSeek(cAlias)
if !Found()
	Help(" ",1,"X7_ALIAS")
	lRet := .F.
endif

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValidSXG      �Autor  �Marcelo Akama   � Data �  05/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Valida a existencia do grupo de campos                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ValidSXG(cGrupo)
Local lRet := .T.

DbSelectArea( "SXG" )
SXG->( DbSetOrder( 1 ) )
If !SXG->( DbSeek( cGrupo ) )
	lRet := .F.
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ValidSXB      �Autor  �Marcelo Akama   � Data �  05/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Valida a existencia de consulta padrao                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ValidSXB(cCod)
Local lRet := .T.

DbSelectArea( "SXB" )
SXB->( DbSetOrder( 1 ) )
If !SXB->( DbSeek( cCod ) )
	lRet := .F.
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AdmCBCPad     �Autor  �Marcelo Akama   � Data �  05/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Combo box de consulta padrao                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function AdmCBCPad()
Local cCBox := ""
Local aArea := GetArea()

DbSelectArea("SXB")
DbSetOrder(1)
DbGoTop()
cCBox := Space(31)
Do While !Eof()
	If XB_TIPO == "1" .And. XB_ALIAS != "SX5"
		cCBox += ';'+XB_ALIAS+"="+OemToAnsi(Substr(XBDESCRI(),1,25))
	EndIf
	DbSkip()
EndDo
DbSelectArea("SX5")
DbGoTop()
Do While X5_TABELA == "00"
	cCBox += ';'+SubStr(X5_CHAVE,1,3)+"="+Capital(OemToAnsi(Substr(X5DESCRI(),1,25)))
	DbSkip()
EndDo

RestArea(aArea)

Return cCBox

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AdmCBGrupo    �Autor  �Marcelo Akama   � Data �  05/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �  Combo box de grupo de campos                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function AdmCBGrupo()
Local cCBox := ""
Local aArea := GetArea()

DbSelectArea("SXG")
DbSetOrder(1)
DbGoTop()
cCBox := Space(31)
Do While !Eof()
	cCBox += ';'+XG_GRUPO+"="+OemToAnsi(Substr(XGDESCRI(),1,25))
	DbSkip()
EndDo

RestArea(aArea)

Return cCBox

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �AdmAbreSM0� Autor � Orizio                � Data � 22/01/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna um array com as informacoes das filias das empresas ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function AdmAbreSM0()
Local aArea			:= SM0->( GetArea() )
Local aAux			:= {}
Local aRetSM0		:= {}
Local lFWLoadSM0	:= FindFunction( "FWLoadSM0" )
Local lFWCodFilSM0 	:= FindFunction( "FWCodFil" )

If lFWLoadSM0
	aRetSM0	:= FWLoadSM0()
Else
	DbSelectArea( "SM0" )
	SM0->( DbGoTop() )
	While SM0->( !Eof() )
		aAux := { 	SM0->M0_CODIGO,;
		IIf( lFWCodFilSM0, FWGETCODFILIAL, SM0->M0_CODFIL ),;
		"",;
		"",;
		"",;
		SM0->M0_NOME,;
		SM0->M0_FILIAL }

		aAdd( aRetSM0, aClone( aAux ) )
		SM0->( DbSkip() )
	End
EndIf

RestArea( aArea )
Return aRetSM0

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CT0AtuSXG � Autor � --------------------- � Data � 18/06/07 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao do SXG                 ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ATUALIZACAO SIGACT0                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CT0AtuSXG()
Local aSXG   := {}
Local aEstrut:= {}
Local i      := 0
Local j      := 0
Local cTexto := ''
Local cAlias := ''
Local lSXG   := .F.

aEstrut:= { "XG_GRUPO","XG_DESCRI","XG_DESSPA","XG_DESENG","XG_SIZEMAX","XG_SIZEMIN","XG_SIZE","XG_PICTURE"}

AADD(aSXG,{"040","Entidade Contabil 05","Ente Contable 05","Accounting Entity 05",20,6,6,"@!"})
AADD(aSXG,{"042","Entidade Contabil 06","Ente Contable 06","Accounting Entity 06",20,6,6,"@!"})
AADD(aSXG,{"043","Entidade Contabil 07","Ente Contable 07","Accounting Entity 07",20,6,6,"@!"})
AADD(aSXG,{"044","Entidade Contabil 08","Ente Contable 08","Accounting Entity 08",20,6,6,"@!"})
AADD(aSXG,{"045","Entidade Contabil 09","Ente Contable 09","Accounting Entity 09",20,6,6,"@!"})


ProcRegua(Len(aSXG))

dbSelectArea("SXG")
dbSetOrder(1) // FUNCAO
For i:= 1 To Len(aSXG)
	If !Empty(aSXG[i][1])
		If !MsSeek(Padr(aSXG[i,1],Len(SXG->XG_GRUPO)))
			lSXG := .T.
			If !(aSXG[i,1]$cAlias)
				cAlias += aSXG[i,1]+"/"
			EndIf

			RecLock("SXG",.T.)

			For j:=1 To Len(aSXG[i])
				If !Empty(FieldName(FieldPos(aEstrut[j])))
					FieldPut(FieldPos(aEstrut[j]),aSXG[i,j])
				EndIf
			Next j

			dbCommit()
			MsUnLock()
			IncProc(STR0078)//"Atualizando grupo de campos"##"Atualizando grupo de campos"
		EndIf
	EndIf
Next i

If lSXG
	cTexto := STR0079+cAlias+CRLF //"Grupo de campos atualizados: "
EndIf

Return cTexto


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CTBA910   �Autor  �Microsiga           � Data �  04/02/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CT910RACol(aHeader)
Local aCols  := {}
Local nX     := 0
Local aArea  := GetArea()
Local cAlias := ""
Local nCols  := 0
Local aPlano := {}

CT0->(dbSetOrder(1)) //CT0_FILIAL+CT0_ID
CV0->(dbSetOrder(1)) //CV0_FILIAL+CV0_PLANO+CV0_CODIGO

CV0->(dbGoTop())
While CV0->(!Eof())

	If aSCan(aPlano,{|cPlano| Alltrim(cPlano) == CV0->CV0_PLANO }) <= 0
		aAdd(aPlano,CV0->CV0_PLANO)
	Else
		CV0->(dbSkip())
		Loop
	EndIf

	If CT0->(MsSeek( xFilial("CT0") + CV0->CV0_PLANO ))
		aAdd(aCols,Array(Len(aHeader)+1))
		nCols ++
		For nX := 1 To Len(aHeader)

			If "CT0" $ aHeader[nX][02]
				cAlias := "CT0"
			Else
				cAlias := "CV0"
			EndIf

			If ( aHeader[nX][10] != "V")
				aCols[nCols][nX] := (cAlias)->(FieldGet(FieldPos(aHeader[nX][2])))
			ElseIf (aHeader[nX][8] == "M") // Campo Memo
				aCols[nCols][nX] := MSMM((cAlias)->(&(cCPOMemo)), TamSX3(cMemo)[1] )
			Else
				aCols[nCols][nX] := CriaVar(aHeader[nX][2],.T.)
			Endif

		Next nX
		aCols[nCols][Len(aHeader)+1] := .F.
	EndIf
	CV0->(dbSkip())
EndDo

RestArea(aArea)
Return aCols

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CT910LOk   �Autor  �Microsiga           � Data �  04/03/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida��o LinOk da rotina                                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function CT910LOk(nLinha)
Local lRet      := .T.
Local aCols     := oGetDados:aCols
Local aHeader   := oGetDados:aHeader
Local nPosAlias := Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ALIAS"})
Local aPos		:= {}
Local nPosChav  := Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPOCHV"})
Local nPosDesc  := Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPODSC"})
Local nPosSup   := Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPOSUP"})
Local nY        := 0

Default nLinha := oGetDados:nAt

aAdd(aPos,Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPOCHV"}))
aAdd(aPos,Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPODSC"}))
aAdd(aPos,Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_CPOSUP"}))


If lRet
	For nY := 1 to Len(aHeader)
		IF Empty(aCols[nLinha,nY]) .And. nY<9 .And. nY>12
			MSGINFO(STR0022,STR0021) //Existem campos obrigat�rios n�o preenchidos # CTBWizard - Entidades
			lRet := .F.
			Exit
		ENDIF
	Next nY
EndIf

If lRet
	For nY := 1 to Len(aPos)
		If !Empty(aCols[nLinha][aPos[nY]])
			cPrefix := PrefixoCpo(aCols[nLinha][nPosAlias])
			If !(cPrefix $ aCols[nLinha][aPos[nY]])
				MSGINFO(STR0076,STR0021) //"O campo deve pertencer a tabela da nova entidade" # CTBWizard - Entidades
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next nY
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CT0F3SX3 �Autor  �Alvaro Camillo Neto � Data �  04/03/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Rotina para filtro de campos do SX3                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � CTBA910                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function CT0F3SX3()
Local aArea     := GetArea()
Local lRet      := .F.
Local cFiltro   := ""
Local aHeader   := oGetDados:aHeader
Local aCols     := oGetDados:aCols
Local nLinha    := oGetDados:nAt
Local nPosAlias := Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ALIAS"})
Local cEntidade := aCols[nLinha][nPosAlias]
Local oDlg
Local oBrowse
Local oMainPanel
Local oPanelBtn
Local oBtnOK
Local oBtnCan
Local oColumn1
Local oColumn2
Local oColumn3
Local oColumn4

If !Empty(cEntidade)
	cFiltro := " CT0SX3->X3_CONTEXT!='V' .And. CT0SX3->X3_TIPO=='C' .And. CT0SX3->X3_ARQUIVO == '" + Alltrim(cEntidade) + "' "
EndIf

If Select( 'CT0SX3' ) == 0
	OpenSxs(,,,,cEmpAnt,"CT0SX3","SX3",,.F.)
EndIf

Define MsDialog oDlg From 0, 0 To 390, 515 Title STR0070 Pixel Of oMainWnd		//"Consulta Padr�o - Campos do Sistema"

@00, 00 MsPanel oMainPanel Size 250, 80
oMainPanel:Align := CONTROL_ALIGN_ALLCLIENT

@00, 00 MsPanel oPanelBtn Size 250, 15
oPanelBtn:Align := CONTROL_ALIGN_BOTTOM

Define FWBrowse oBrowse DATA TABLE ALIAS 'CT0SX3'  NO CONFIG  NO REPORT ;
DOUBLECLICK { || lRet := .T.,  oDlg:End() } Of oMainPanel
ADD COLUMN oColumn1  DATA { || CT0SX3->X3_CAMPO   }  Title STR0071  Size Len( CT0SX3->X3_CAMPO   ) Of oBrowse // "Campo"
ADD COLUMN oColumn2  DATA { || If(__LANGUAGE=="SPANISH",CT0SX3->X3_TITSPA, If(__LANGUAGE=="ENGLISH",CT0SX3->X3_TITENG, CT0SX3->X3_TITULO)) }  Title STR0072 Size Len( CT0SX3->X3_TITULO ) Of oBrowse			//"Titulo"
ADD COLUMN oColumn3  DATA { || If(__LANGUAGE=="SPANISH",CT0SX3->X3_DESCSPA, If(__LANGUAGE=="ENGLISH",CT0SX3->X3_DESCENG, CT0SX3->X3_DESCRIC)) }  Title STR0073 Size Len( CT0SX3->X3_DESCRIC ) Of oBrowse		//"Descri��o"
If !Empty( cFiltro )
	oBrowse:SetFilterDefault( cFiltro )
EndIf
oBrowse:Activate()

Define SButton oBtnOK  From 02, 02 Type 1 Enable Of oPanelBtn ONSTOP STR0074 ;				//"Ok <Ctrl-O>"
Action ( lRet := .T., oDlg:End() )

Define SButton oBtnCan From 02, 32 Type 2 Enable Of oPanelBtn ONSTOP STR0075 ;				//"Cancelar <Ctrl-X>"
Action ( lRet := .F., oDlg:End() )

Activate MsDialog oDlg Centered

CT0SX3->( dbClearFilter() )

RestArea( aArea )

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  CTBUpdField �Autor �Marylly A. Silva	 � Data �  10/05/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Rotina para atualiza��o de campos do SXx de informa��es j�       ���
���          �existentes no dicion�rio de dados.                           ���
�������������������������������������������������������������������������͹��
���Uso       � CTBA910                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CTBUpdField(cAlias, nOrder, cIndexKey, cField, uNewValue, uTestValue, bBlockValue)
Local aArea       := (cAlias)->(GetArea())
Local lRet        := .F.
Local nFieldPos   := 0
Local aStruct     := {}
Local nPosField   := 0
Local uValueField := 0

dbSelectArea(cAlias)
(cAlias)->(dbSetOrder(nOrder))

// verifica se o registro existe no alias
If !(cAlias)->(dbSeek(cIndexKey))
	RestArea(aArea)
	Return lRet
EndIf

// verificar se o campo existe no alias
nFieldPos := (cAlias)->(FieldPos(cField))

If nFieldPos == 0
	RestArea(aArea)
	Return lRet
EndIf

aStruct := (cAlias)->(dbStruct())
nPosFIELD := aScan( aStruct ,{|aField|Alltrim(Upper(aField[1])) == Alltrim(Upper(cField)) } )
uValueField := (cAlias)->(FieldGet(nFieldPos))

If bBlockValue == Nil

	// teste por valor
	If uTestValue == Nil

		If nPosFIELD >0
			If aStruct[nPosFIELD][2] == "C"
				uValueField := AllTrim(uValueField)
				uTestValue  := AllTrim(uNewValue)
			EndIf
		EndIf

		// Somente atualiza se o valor gravado no campo (uValueField) for diferente do novo valor (uNewValue)
		lRet := !(uValueField == uTestValue)

		If lRet
			RecLock(cAlias, .F.)
			(cAlias)->(FieldPut(nFieldPos, uNewValue))
			MsUnlock()
		EndIf

		RestArea(aArea)

	Else

		If nPosFIELD >0
			// se for caracter deve retirar os brancos e maiusculas antes de comparar.
			If aStruct[nPosFIELD][2] == "C"
				uValueField := AllTrim(Upper(uValueField))
				uTestValue  := AllTrim(Upper(uTestValue))
			EndIf
		EndIf

		// se o teste existe, testa e altera o valor
		If uTestValue == uValueField
			RecLock(cAlias, .F.)
			(cAlias)->(FieldPut(nFieldPos, uNewValue))
			MsUnlock()

			RestArea(aArea)
			lRet := .T.
		EndIf
	EndIf
Else
	// teste por bloco - nao implementado
EndIf

RestArea(aArea)
Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CtbEntIniVar �Autor  �Microsiga        � Data �  08/02/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Analise da existencia dos campos das novas entidades       ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CtbEntIniVar(cIdEnt)
Local lExist := .F.
cIdEnt := StrZero(Val(cIdEnt),2)
lExist := CTJ->(FieldPos("CTJ_EC"+cIdEnt+"CR")>0 .And. FieldPos("CTJ_EC"+ cIdEnt + "DB")>0)
Return lExist

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �EntAtuSXB �Autor  �Microsiga           � Data �  21/02/13   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao de processamento da gravacao do SXB                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function EntAtuSXB()
Local aSXB   := {}
Local aAjSXB := {}
Local aEstrut:= {}
Local i      := 0
Local j      := 0
Local cTexto := ''
Local cAlias := ''
Local lSXB   := .F.

aEstrut:= {"XB_ALIAS","XB_TIPO","XB_SEQ","XB_COLUNA","XB_DESCRI","XB_DESCSPA","XB_DESCENG","XB_CONTEM"}

/*EXEMPLO AJUSTE SXB
Aadd(aSXB,{"AJ8","1","01","DB","Consulta Gerencial","Consulta Gerencial","Consulta Gerencial","AJ8"})
Aadd(aSXB,{"AJ8","2","01","03","Codigo+Entidade Superior","Codigo+Entidade Superior","Codigo+Entidade Superior",""})
Aadd(aSXB,{"AJ8","4","01","01","Codigo","Codigo","Codigo","AJ8_CODPLA"})
Aadd(aSXB,{"AJ8","4","01","02","Codigo da Entidade","Codigo da Entidade","Codigo da Entidade","AJ8_CONTAG"})
Aadd(aSXB,{"AJ8","5","","","","","","AJ8->AJ8_CONTAG"})
Aadd(aSXB,{"AJ8","6","01","01","","","","PmsAJ8F3()"})
*/
/*
ou assim quando for para ajustar somente um campo
	aAdd(aAjSXB, {"AL1","4","01","03",{"XB_CONTEM", "AL1_DESCRI"}})
*/

Aadd(aSXB,{"CT0001","1","01","RE"	,"Entidades Adicionais"	,"Entes Adicionales"	,"Additional Entities"	,"CT0"			})
Aadd(aSXB,{"CT0001","2","01","01"	,""						,""						,""						,".T."			})
Aadd(aSXB,{"CT0001","5","01",""		,""						,""						,""						,"PCOF3PtLan()"	})

ProcRegua(Len(aSXB))

dbSelectArea("SXB")
dbSetOrder(1)
For i:= 1 To Len(aSXB)
	If !Empty(aSXB[i][1])
		If !dbSeek(Padr(aSXB[i,1], Len(SXB->XB_ALIAS))+aSXB[i,2]+aSXB[i,3]+aSXB[i,4])
			lSXB := .T.
			If !(aSXB[i,1]$cAlias)
				cAlias += aSXB[i,1]+"/"
			EndIf

			RecLock("SXB",.T.)

			For j:=1 To Len(aSXB[i])
				If !Empty(FieldName(FieldPos(aEstrut[j])))
					FieldPut(FieldPos(aEstrut[j]),aSXB[i,j])
				EndIf
			Next j

			dbCommit()
			MsUnLock()
			IncProc(STR0082) //"Atualizando consultas padr�es..."
		EndIf
	EndIf
Next i

dbSelectArea("SXB")
dbSetOrder(1)
For i:= 1 To Len(aAjSXB)
	If !Empty(aAjSXB[i][1])
		If dbSeek(PadR(aAjSXB[i,1], Len(SXB->XB_ALIAS))+aAjSXB[i,2]+aAjSXB[i,3]+aAjSXB[i,4])
			If Upper(AllTrim(FieldGet(FieldPos(aAjSXB[i,5,1])))) # Upper(AllTrim(aAjSXB[i,5,2]))
				lSXB := .T.
				If !(aAjSXB[i,1]$cAlias)
					cAlias += aAjSXB[i,1]+"/"
				EndIf
				RecLock("SXB",.F.)
		   		If !Empty(FieldName(FieldPos(aAjSXB[i,5,1])))
					FieldPut(FieldPos(aAjSXB[i,5,1]),aAjSXB[i,5,2])
				EndIf
				dbCommit()
				MsUnLock()
				IncProc(STR0082) // //"Atualizando consultas padr�es..."
			EndIf
		EndIf
	EndIf
Next i

If lSXB
	cTexto := STR0083+cAlias+CRLF //'Consultas padr�es atualizadas: '
EndIf

Return cTexto

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  EntAtuSX1 �Autor  �Microsiga           � Data �  20/06/18   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao de processamento da gravacao do SX1                 ���
���          � Grupo de Perguntas                                         ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function EntAtuSX1()
Local cTexto    := '' //Texto de Retorno
Local lSX1      := .F. //Grupos de Perguntas atualizados?
Local nQtd      := 1 //Quantidade de Grupos
Local nQtdEnt   := 5 //Quantidade de Entidades Adicionais
Local aColsGet	:= ACLONE(oGetDados:aCols)
Local aHeader	:= ACLONE(oGetDados:aHeader)
Local nPosPlano	:= Ascan(aHeader,{|x|Alltrim(x[2]) == "CT0_ID"})
Local nPlano    := 0 //�ltimo Plano Incluso
Local nI        := 0 //Controle de FOR

Private aEntidades := {} //Entidades do Grupo

//Incrementando Regua de Processamento
ProcRegua(nQtd)

If (nPosPlano > 0 .AND. Len(aColsGet) > 0)
	//Incluindo Grupo de Perguntas "PCOM010E" - Exp. Mov. Or�ament�rios
	IncProc('Grupo de Perguntas PCOM010E')
	//Pegando o Ultimo Plano
	nPlano := Val(aColsGet[Len(aColsGet), nPosPlano])
	//Compondo array aEntidades
	For nI := 1 To nQtdEnt
		Aadd(aEntidades, {nPlano >= (nI + 4) , 'Ent Cont ' + cValToChar(nI + 4) + ' de:', 'Ent Cont ' + cValToChar(nI + 4) + ' at�:', '', ''})
	Next nI
	PCOM010X1E()
EndIf

If lSX1
	cTexto := "Grupos de Perguntas Atualizados." + CRLF //"Grupos de Perguntas Atualizados."
EndIf

Return cTexto