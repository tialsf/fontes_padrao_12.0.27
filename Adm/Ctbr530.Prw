#INCLUDE "CTBR530.CH"
#INCLUDE "PROTHEUS.CH"

//Tradu��o PTG 20080721

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 � Ctbr530	� Autor � Wagner Mobile Costa	� Data � 15.10.01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Demonstracao das mutacoes do patrimonio liquido     	    ���
�������������������������������������������������������������������������Ĵ��
���Retorno	 � Nenhum       											  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum													  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CtbR530()         

PRIVATE titulo	:= STR0003 			//"Demonstracoes das mutacoes do patrimonio liquido"
Private nomeprog	:= "CTBR530"
oFont06	:= TFont():New("Courier New",06,06,,.F.,,,,.T.,.F.)
oCouNew06N	:= TFont():New("Courier New",06,06,,.T.,,,,.T.,.F.)		// Negrito
oCouNew06S	:= TFont():New("Courier New",06,06,,.F.,,,,.T.,.T.)		// SubLinhado

CTBR530R4()

//Limpa os arquivos tempor�rios 
CTBGerClean()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 � CTBR530R4 � Autor� Daniel Sakavicius		� Data � 12/09/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Demonstracao das mutacoes do patrimonio liquido - R4  	  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 � CTBR530R4												  ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 � SIGACTB                                    				  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CTBR530R4() 
Local cPerg	  		:= "CTR530"			       

Private cPictVal 		:= PesqPict("CT2","CT2_VALOR")
Private cSayClVl		:= CtbSayApro("CTH")

If Pergunte(cPerg,.T.)
	//������������������������������������������������������������������������Ŀ
	//�Interface de impressao                                                  �
	//��������������������������������������������������������������������������
	oReport := ReportDef()      
	
	If ValType( oReport ) == "O"
		If ! Empty( oReport:uParam )
			Pergunte( oReport:uParam, .F. )
		EndIf	
		
		oReport :PrintDialog()      
	Endif
	
	oReport := nil
Endif

Return                                

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportDef � Autor � Daniel Sakavicius		� Data � 01/09/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Esta funcao tem como objetivo definir as secoes, celulas,   ���
���          �totalizadores do relatorio que poderao ser configurados     ���
���          �pelo relatorio.                                             ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 � SIGACTB                                    				  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportDef()   

Local aArea	  		:= GetArea()   
Local CREPORT		:= "CTBR530"
Local CDESC			:= STR0011+;		//"Este programa ir� imprimir as Demonstra��es das Muta��es do " +;
							STR0012			//"Patrim�nio L�quido, de acordo com os par�metros informados pelo usu�rio."
Local cPerg	  		:= "CTR530"			       
Local aTamVal		:= TAMSX3("CT2_VALOR")
Local nDecimais

Local aSetOfBook	:= CTBSetOf(mv_par02)

Titulo	:= If(! Empty(aSetOfBook[10]), aSetOfBook[10], Titulo)		// Titulo definido SetOfBook
If Valtype(mv_par08)=="N" .And. (mv_par08 == 1)
	Titulo := CTBNomeVis( aSetOfBook[5] )
EndIf

//������������������������������������������������������������������������Ŀ
//�Criacao do componente de impressao                                      �
//�                                                                        �
//�TReport():New                                                           �
//�ExpC1 : Nome do relatorio                                               �
//�ExpC2 : Titulo                                                          �
//�ExpC3 : Pergunte                                                        �
//�ExpB4 : Bloco de codigo que sera executado na confirmacao da impressao  �
//�ExpC5 : Descricao                                                       �
//��������������������������������������������������������������������������
oReport	:= TReport():New( cReport,Titulo,CPERG, { |oReport| If(!ReportPrint( oReport ),oReport:CancelPrint(),.T.) }, CDESC )
oReport:ParamReadOnly()

IF GETNEWPAR("MV_CTBPOFF",.T.)
	oReport:SetEdit(.F.)
ENDIF	                      
oReport:SetLandscape()				// Formato paisagem

//������������������������������������������������������������������������Ŀ
//�Criacao da secao utilizada pelo relatorio                               �
//�                                                                        �
//�TRSection():New                                                         �
//�ExpO1 : Objeto TReport que a secao pertence                             �
//�ExpC2 : Descricao da se�ao                                              �
//�ExpA3 : Array com as tabelas utilizadas pela secao. A primeira tabela   �
//�        sera considerada como principal para a se��o.                   �
//�ExpA4 : Array com as Ordens do relat�rio                                �
//�ExpL5 : Carrega campos do SX3 como celulas                              �
//�        Default : False                                                 �
//�ExpL6 : Carrega ordens do Sindex                                        �
//�        Default : False                                                 �
//�                                                                        �
//��������������������������������������������������������������������������
oSection0  := TRSection():New( oReport, STR0016, {"cArqTmp"},, .F., .F. )		//"Entidade Gerencial"        

TRCell():New( oSection0, "DESC"	  , ,STR0015/*Titulo*/,/*Picture*/,38/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,,.T.,,,,.F.)		//"Descri��o"
TRCell():New( oSection0, "VALOR1" , ,STR0014+"1"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 	//"Valor "
TRCell():New( oSection0, "VALOR2" , ,STR0014+"2"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR3" , ,STR0014+"3"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR4" , ,STR0014+"4"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR5" , ,STR0014+"5"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR6" , ,STR0014+"6"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR7" , ,STR0014+"7"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR8" , ,STR0014+"8"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 
TRCell():New( oSection0, "VALOR9" , ,STR0014+"9"/*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT")      
TRCell():New( oSection0, "VALORA" , ,STR0008    /*Titulo*/,/*Picture*/,aTamVal[1]/*Tamanho*/,/*lPixel*/,/*CodeBlock*/,"RIGHT",,"RIGHT") 	//"Total"     

oSection0:SetTotalInLine(.F.)          

/*
GESTAO - inicio */
/* Relacao das filiais selecionadas para compor o relatorio */
oSecFil := TRSection():New(oReport,"SECFIL")

TRCell():New(oSecFil,"CODFIL",,"C�digo",/*Picture*/,20,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSecFil,"EMPRESA",,"Empresa",/*Picture*/,60,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSecFil,"UNIDNEG",,"Unidade de neg�cio",/*Picture*/,60,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSecFil,"NOMEFIL",,"Filial",/*Picture*/,60,/*lPixel*/,/*{|| code-block de impressao }*/)
/* GESTAO - fim
*/             

Return(oReport)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportPrint� Autor � Daniel Sakavicius	� Data � 01/09/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime o relatorio definido pelo usuario de acordo com as  ���
���          �secoes/celulas criadas na funcao ReportDef definida acima.  ���
���          �Nesta funcao deve ser criada a query das secoes se SQL ou   ���
���          �definido o relacionamento e filtros das tabelas em CodeBase.���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ReportPrint(oReport)                                       ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �EXPO1: Objeto do relat�rio                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ReportPrint( oReport )  
Local oSection0 	:= oReport:Section(1)    
Local aSetOfBook	:= CTBSetOf(mv_par02)
Local aCtbMoeda	:= {}
Local aColunas0 	:= {}
Local aColunasA 	:= {}
Local aColunasF 	:= {}
Local aColunas	:= { }, aColunas2 := {}, aColunasT
Local aCabecalho	:= Array(9)
Local aPosCol		:= { 1610, 1830, 2045, 2260, 2475, 2690, 2905,3120,3335,3550 }
Local cArqTmp		:= ""
Local cPicture	:= ""
Local cDescMoeda	:= ""
Local cArqTrm		:= ""
Local cImpSaldo	:= ""
Local lFirstPage	:= .T.               
Local lImpTrmAux	:= Iif(mv_par05 == 1,.T.,.F.)
Local nColuna		:= 0
Local nlin 			:= 2301
Local nSubtrai		:= 25
Local nPosCol		:= 0
Local nLenPos		:= 0
Local nLenCol		:= 0   
Local nLenCab		:= 0
Local dFinal		:= Ctod("  /  /  ")
Local nContDesc 	:= 0 //Ajuste na impressao da descricao das contas
Local nTotCol       := 0
Local nContCol      := 0 
Local nColTot    	:= 0
Local lContinua 	:= .T.
Local nTamEmp		:= 0
Local nTamUnNeg	:= 0
Local nTamTit		:= 0
Local cFiLSel		:= ""
Local cTitulo		:= ""
Local oSecFil		:= oReport:Section("SECFIL") 
Local aSM0			:= {}    
Local nX  			:= 0
Local lSldAnt 	:= Iif( MV_PAR10 == 1, .T., .F. )
Local lSldZero 	:= Iif( MV_PAR11 == 1, .T., .F. )
Local lDescCtIni 	:= Iif( MV_PAR12 == 1, .T., .F. )
Local cDescEnt	:= ""
Private aSelFil	:= {}                     

If mv_par09 == 1 .And. Len( aSelFil ) <= 0  .And. !IsBlind()
	aSelFil := AdmGetFil()
	If Len( aSelFil ) <= 0      
		Return .F.
	EndIf 
EndIf  

/* imprime a lista de filiais selecionadas para o relatorio */
If Len(aSelFil) > 1
	aSM0 := FWLoadSM0()
	nTamEmp := Len(FWSM0LayOut(,1))
	nTamUnNeg := Len(FWSM0LayOut(,2))
	cTitulo := oReport:Title()
	oReport:SetTitle(cTitulo + " (" + "Filiais selecionadas para o relatorio" + ")")
	nTamTit := Len(oReport:Title())
	oSecFil:Init()  
	oSecFil:Cell("CODFIL"):SetBlock({||cFilSel})
	oSecFil:Cell("EMPRESA"):SetBlock({||aSM0[nLinha,SM0_DESCEMP]})
	oSecFil:Cell("UNIDNEG"):SetBlock({||aSM0[nLinha,SM0_DESCUN]})
	oSecFil:Cell("NOMEFIL"):SetBlock({||aSM0[nLinha,SM0_NOMRED]})

	For nX := 1 To Len(aSelFil)
		nLinha := Ascan(aSM0,{|sm0|,sm0[SM0_CODFIL] == aSelFil[nX]})
		If nLinha > 0
			cFilSel := Substr(aSM0[nLinha,SM0_CODFIL],1,nTamEmp)
			cFilSel += " "
			cFilSel += Substr(aSM0[nLinha,SM0_CODFIL],nTamEmp + 1,nTamUnNeg)
			cFilSel += " "
			cFilSel += Substr(aSM0[nLinha,SM0_CODFIL],nTamEmp + nTamUnNeg + 1)
			oSecFil:PrintLine()
		Endif
	Next
	oReport:SetTitle(cTitulo)
	oSecFil:Finish()
	oReport:EndPage()
Endif
/* GESTAO - fim
*/            

If ( !AMIIn(34) )		// Acesso somente pelo SIGACTB
	Return .F.
EndIf

// faz a valida��o do livro
if ! VdSetOfBook( mv_par02 , .T. )
   return .F.
endif

If Empty(aSetOfBook[5])
	//"Os demonstrativos contabeis obrigatoriamente devem ter um plano gerencial associado ao livro. Verifique a configuracao de livros escolhida !"
	ApMsgAlert(	STR0010) 
	Return .F.
Endif

CTG->(DbSeek(xFilial() + mv_par01))
While CTG->CTG_FILIAL = xFilial("CTG") .And. CTG->CTG_CALEND = mv_par01
	dFinal	:= CTG->CTG_DTFIM
	CTG->(DbSkip())
EndDo

lTRegCts	:= .T.		// Indica criar campo DESCORIG
dFinalA   	:= Ctod(Left(Dtoc(dFinal), 6) + Str(Year(dFinal) - 1, 4))
dPeriodo0 	:= Ctod(Left(Dtoc(dFinal), 6) + Str(Year(dFinal) - 2, 4)) + 1
mv_par01    := dFinal

If lSldAnt
	aColunas0 	:= { "", "", " ", 0, dPeriodo0 - 1, " ", 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00 }
Endif				
aColunasA 	:= { "", "", " ", 0, dFinalA, " ", 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00 }
aColunasF 	:= { "", "", " ", 0, dFinal, " ", 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00 }

Afill(aCabecalho, Space(0))

aCtbMoeda := CtbMoeda(mv_par03, aSetOfBook[9])
If Empty(aCtbMoeda[1])                       
	Help(" ",1,"NOMOEDA")
    Return .F.
Endif

cDescMoeda 	:= AllTrim(aCtbMoeda[3])
nDecimais 	:= DecimalCTB(aSetOfBook,mv_par03)

cPicture 	:= aSetOfBook[4]
If Empty(cPicture) .Or. Len(Trans(0, cPicture)) > 17
  	cPicture 	:= "@E 99,999,999,999.99"
Endif

oReport:SetPageNumber( mv_par04 )
oReport:SetCustomText( {|| CtCGCCabTR(,,,,,dDataBase,titulo,,,,,oReport) } )
//��������������������������������������������������������������Ŀ
//� Monta Arquivo Temporario para Impressao						 �
//����������������������������������������������������������������
MsgMeter({|	oMeter, oText, oDlg, lEnd | ;
			CTGerPlan(oMeter, oText, oDlg, @lEnd,@cArqTmp,;
			dFinalA+1,dFinal,"","","",Repl("Z", Len(CT1->CT1_CONTA)),;
			"",Repl("Z", Len(CTT->CTT_CUSTO)),"",Repl("Z", Len(CTD->CTD_ITEM)),;
			"",Repl("Z", Len(CTH->CTH_CLVL)),mv_par03,;
			MV_PAR07,aSetOfBook,Space(2),Space(20),Repl("Z", 20),Space(30),;     
				.F.,.F.,2,,.F.,"",,,,,,,,,,,,,,,,.F.,"",.F.,;
			"","",,,,,,,"",,aSelFil)},;
			STR0007, STR0003) //"Criando Arquivo Temporario..."

#DEFINE C_CONTA			1
#DEFINE C_DESC			2
#DEFINE C_NORMAL  		3
#DEFINE C_COLUNA  		4
#DEFINE C_PERIODO 		5
#DEFINE C_IDENTIFI		6
#DEFINE C_SALDOATU		6

dbSelectArea("cArqTmp")
oReport:SetMeter(RecCount())
dbGoTop()
While !Eof()
	If COLUNA > 0
		nContCol++  // WHILE PARA CONTAGEM DAS COLUNAS DO RELATORIO DE ACORDO COM A VISAO GERENCIAL
		If nContCol > Len(aCabecalho) .OR. COLUNA > Len(aCabecalho)
			lContinua := .F.		
			Aviso(STR0017, StrTran(STR0018,"8","9"),{"Ok"})  //"Atencao"##"Este relatorio somente suporta 8 colunas. Verifique a vis�o gerencial."
			Exit
		EndIf	
		If COLUNA > 6  .And. oReport:GetOrientation() == 1 //PORTRAIT
 			lContinua := .F.		
			Aviso(STR0017, STR0019,{"Ok"})  //"Atencao"##"Esse relat�rio s� pode ser impresso com mais de 6 colunas em modo paisagem"                                                                                                                                                                                                                                                                                                                                                                                                                                        
		    Exit
		Endif 			
	    aCabecalho[nContCol] := ALLTRIM(DESCCTA) 
	    //IF PARA TRATAR A CONTAGEM DAS COLUNAS VALIDAS PARA O RELATORIO.    
		IF !EMPTY(aCabecalho[nContCol])
			nColTot++
		ENDIF
	ENDIF
   	DBSKIP()
Enddo	

If lContinua
	dbSelectArea("cArqTmp")
	oReport:SetMeter(RecCount())
	dbGoTop()
	
	While !Eof()                
		oReport:IncMeter()
		If COLUNA = 0
			nContDesc++
	        
	       If lSldAnt
				If nContDesc == 1
					aColunas0[C_DESC]			:= DESCCTA
				EndIf
				aColunas0[C_NORMAL]			:= NORMAL
				aColunas0[C_IDENTIFI]		:= IDENTIFI
				aColunas0[Len(aColunas0)]	:= SALDOPER
			Endif
	
			If nContDesc == 1
				aColunasA[C_DESC]			:= DESCCTA
			EndIf
			aColunasA[C_NORMAL]			:= NORMAL
			aColunasA[C_IDENTIFI]		:= IDENTIFI
			aColunasA[Len(aColunasA)]	:= SALDOANT
	
			If nContDesc == 1
				aColunasF[C_DESC]			:= DESCCTA
			EndIf
			aColunasF[C_NORMAL]			:= NORMAL
			aColunasF[C_IDENTIFI]		:= IDENTIFI
			aColunasF[Len(aColunasA)]	:= SALDOATU
		Else
			If lSldAnt
				aColunas0[C_SALDOATU + COLUNA] += SALDOPER
			Endif
			aColunasA[C_SALDOATU + COLUNA] += SALDOANT
			aColunasF[C_SALDOATU + COLUNA] += SALDOATU
			
			aCabecalho[COLUNA] := ALLTRIM(DESCCTA)
			
			//Se for descri��o da conta inicial configurada
			If lDescCtIni
				cDescEnt := DESCORIG				
			Else //Se for descri��o configurada no campo "Cont. Descr." na vis�o gerencial
				cDescEnt := DESCCONT
			Endif 
			
			If (nColuna := Ascan(aColunas, { |x| 	x[C_DESC] = cDescEnt .And. x[C_COLUNA] = COLUNA })) = 0
				Aadd(	aColunas, { 	"", cDescEnt, NORMAL, COLUNA, Ctod(""), IDENTIFI,;
										0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, MOVIMENTO })
				nColuna := Len(aColunas)
			Endif
			aColunas[nColuna,C_SALDOATU + COLUNA] += MOVIMENTO
	
			If (nColuna := Ascan(aColunas2, { |x| 	x[C_DESC] = cDescEnt .And. x[C_COLUNA] = COLUNA })) = 0
				Aadd(	aColunas2, { 	"", cDescEnt, NORMAL, COLUNA, Ctod(""), IDENTIFI,;
										0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, MOVIMPER })
				nColuna := Len(aColunas2)
			Endif			
			aColunas2[nColuna,C_SALDOATU + COLUNA] += MOVIMPER
		Endif
		DbSkip()
	EndDo	
	
	// Define o cabecalho das celulas de valor
	nLenCab	:= Len(aCabecalho)
	For nColuna := 1 To nLenCab 
	    IF nColuna > nColTot .AND. ! nColuna == 10
			oSection0:Cell("VALOR"+AllTrim(Str(nColuna))):Disable()    
		Else
			oSection0:Cell("VALOR"+AllTrim(Str(nColuna))):SetTitle(StrTran(Alltrim(aCabecalho[nColuna])," ", CRLF))
		Endif	
	Next nColuna
	
	aColunasT := AClone(aColunas)
	aColunas  := {}
	
	If lSldAnt		
		// Adiciona o saldo anterior a 2 exercicios	
		If Len(aColunas0) > 0
			Aadd(aColunas, aColunas0)
		Endif
		
		// Adiciona a movimentacao anterior a 2 exercicios
		For nColuna := 1 To Len(aColunas2)		
			If lSldZero
				Aadd(aColunas, aColunas2[nColuna])
			Else
				//S� imprime se o total for maior que 0
				If aColunas2[nColuna][Len(aColunas2[nColuna])] > 0
					Aadd(aColunas, aColunas2[nColuna])
				Endif
			Endif			
		Next
	Endif
	
	// Adiciona o saldo anterior ao periodo solicitado	
	If Len(aColunasA) > 0
		Aadd(aColunas, aColunasA)
	Endif
	
	// Adiciona a movimentacao do periodo	
	For nColuna := 1 To Len(aColunasT)
		If lSldZero
			Aadd(aColunas, aColunasT[nColuna])			
		Else
			//S� imprime se o total for maior que 0
			If aColunasT[nColuna][Len(aColunasT[nColuna])] > 0
				Aadd(aColunas, aColunasT[nColuna])
			Endif
		Endif
	Next
	
	// Adiciona o saldo final do periodo	
	If Len(aColunasF) > 0
		Aadd(aColunas, aColunasF)
	Endif
	                                                 
	nLenPos	:= Len(aPosCol)
	nLenCol	:= Len(aColunas)
	
	oReport:SetMeter( Len(aColunas) )
	
	oSection0:Init()
	oReport:SkipLine()
	
	For nColuna := 1 To nLenCol
	
		nTotCol := 0
		
		oReport:IncMeter()
		
		If oReport:Cancel()
			Exit
		EndIf  
	
		oSection0:Cell("DESC"):SetTitle(STR0013 + cDescMoeda + ")") 	//"(Em " - Cabecalho da descricao
		oSection0:Cell("DESC"):SetBlock( { || AllTrim(aColunas[nColuna,C_DESC]) +;
					 	 If(aColunas[nColuna,C_COLUNA] = 0, " em " +	Dtoc(aColunas[nColuna,C_PERIODO]), "") } )
	
		If aColunas[nColuna,C_IDENTIFI] < "5"
			For nPosCol := 1 To nLenPos
				cImpSaldo := Transform(	aColunas[nColuna,C_SALDOATU + nPosCol],cPicture)

				If nPosCol < nLenPos   //ultima coluna refere-se a saldo final e nao deve ser somada no total
					nTotCol += aColunas[nColuna,C_SALDOATU + nPosCol]
				EndIf	

				If nPosCol == nLenPos
				 	If nPosCol == 10
				 		oSection0:Cell("VALORA"):SetValue(Transform(nTotCol,cPicture))
				 	Else
				 		oSection0:Cell("VALOR"+AllTrim(Str(nPosCol))):SetValue(Transform(nTotCol,cPicture))
				 	EndIf
			   	ELSE
				   	oSection0:Cell("VALOR"+AllTrim(Str(nPosCol))):SetValue(cImpSaldo)
			   	ENDIF	
			Next
		Endif
	
		oSection0:PrintLine()
		oReport:SkipLine()		                  
						
	Next                
	
	If lImpTrmAux
		cArqTRM 	:= mv_par06
		aVariaveis	:= {}
		
		If !File(cArqTRM)
			aSavSet:=__SetSets()
			cArqTRM := CFGX024(cArqTRM,STR0009) // "Respons�veis..."
			__SetSets(aSavSet)
			Set(24,Set(24),.t.)
		Endif
	
		If cArqTRM#NIL
			ImpTerm2(cArqTRM,aVariaveis,,,,oReport)
		Endif	 
	Endif
	
	oSection0:Finish()
EndIf  

Set Filter To
dbCloseArea() 
If Select("cArqTmp") == 0
	FErase(cArqTmp+GetDBExtension())
	FErase(cArqTmp+OrdBagExt())
EndIF	
dbselectArea("CT2") 

Return .T.

