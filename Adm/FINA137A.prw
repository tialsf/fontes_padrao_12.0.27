#Include 'PROTHEUS.CH'
#Include 'RESTFUL.CH'
#Include "FWMVCDEF.CH"
#Include "FINA137A.ch"

#Define Enter Chr(13)+Chr(10)

//-------------------------------------------------------------------
/*/{Protheus.doc} FINA137A
Descricao: Servi�o API Rest Evento Implanta��o - Totvs Supplier + 

@author Hermiro J�nior
@since 07/05/2020
@version 1.0

@Param:

/*/
//-------------------------------------------------------------------
Function FINA137A()
Return

// Servi�o.
WsRestFul FINA137A DESCRIPTION STR0001

	WsMethod POST Description STR0002  WsSyntax "FINA137A"

End WsRestFul
//-------------------------------------------------------------------
/*/{Protheus.doc} Metodo Post | Evento Implanta��o 
Descricao: 	Servi�o Rest contendo o Metodo POST do evento de 
				Implantacao do Projeto Totvs Supplier+

@author Hermiro J�nior
@since 07/05/2020
@version 1.0

@Param:

/*/
//-------------------------------------------------------------------
WsMethod POST WsReceive RECEIVE WsService FINA137A

	Local cMessage		As Character 
	Local cCode			As Character 
	Local cEmp			As Character 
	Local cFil			As Character 
	Local aTitulos		As Array
	Local aTitTrans		As Array
	Local aRetExec		As Array
	Local aRastro		As Array
	Local aErro			As Array
	Local lRet			As Logical 
	Local lRpcSetEnv	As Logical
	Local oBody			As J 

	cMessage			:= ""
	cCode				:= ''
	cEmp				:= ''
	cFil				:= ''
	aTitulos			:= {}
	aTitTrans			:= {}
	aRetExec			:= {}
	aRastro				:= {}
	aErro				:= {}
	lRet				:= .T.
	lRpcSetEnv			:= .T.

	// recupera o body da requisi��o
	oBody  := JsonObject():New()

	If ValType(oBody:fromJson( AllTrim( self:getContent() ) )) != 'U'	
		cCode 	:= STR0003 
		cMessage	:= STR0005
	Else
		cEmp 		:= oBody:getJsonText("EMPRESA")
		cFil 		:= oBody:getJsonText("FILIAL")
		
		// Verifica se os campos Obrigatorios para abertura da Empresa/Filial foram preenchidos.
		If (Empty(cEmp) .Or. AllTrim(cEmp)=='null')
			cCode 	:= STR0003
			cMessage	:= STR0006
		ElseIf (Empty(cFil) .Or. AllTrim(cFil)=='null')
			cCode 	:= STR0003
			cMessage	:= STR0007
		Else
			RPCSetType(3)  
			RpcSetEnv(cEmp,cFil,,,,GetEnvServer(),{ }) 
			lRpcSetEnv := .F.
		EndIf

		// Chama Rotina que ir� analisar os dados recebidos.
		If Empty(cCode) .And. Empty(cMessage)
			RetDados(oBody, @aTitulos, @aTitTrans, @aRastro, @aErro )
			
			// Se o array de Erro estiver vazio, segue com a ExecAuto
			If Empty(aErro)
				// Chama a ExecAuto
				aRetExec	:= FINA137B(aTitulos, aTitTrans, aRastro )
				cCode		:= aRetExec[2]
				cMessage	:= aRetExec[3]
			Else
				// Mostra o Erro no Retorno do Processamento.
				cCode		:= aErro[2]
				cMessage	:= aErro[3]
			EndIf 

		EndIf 

	EndIf

	//-> Mensagem de Retorno da Requisi��o
	self:setContentType("application/json")
	self:setResponse(fwJsonSerialize(StatReq(cCode,cMessage)))

	// Realiza o fechamento do Ambiente
	If !lRpcSetEnv
		RpcClearEnv()
	Endif 

	Freeobj(oBody)

Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} StatReq
Descricao: 	Realiza o Tratamento para envio de Mensagem de Retorno da API 
@author Hermiro J�nior
@since 12/05/2020
@version 1.0
@Param: 
	cCode 		= 	Codigo do Erro que sera informado no Retorno da API
	cMessage	= 	Mensagem de Retorno que sera informado no Retorno 
					da API
/*/
//-------------------------------------------------------------------
Static Function StatReq(cCode As Character, cMessage As Character) As J

	Local oJsonBody	As J
	Local oJsonRet 	As J

	Default cCode		:= ''
	Default cMessage	:= ''

	//Cria o Objeto de Retono da Requisi��o.
	oJsonRet  := JsonObject():New()
	oJsonRet["StatusRequest"] := {}

	oJsonBody := JsonObject():New()
	oJsonBody["code"]          := cCode
	oJsonBody["message"]       := cMessage

	AAdd( oJsonRet["StatusRequest"], oJsonBody )

Return oJsonRet
//-------------------------------------------------------------------
/*/{Protheus.doc} RetDados
Descricao: 	Realiza o tratamento dos dados vindos do Json e ja prepara
			o array de gera��o dos Titulos do Contas a Pagar.
@author Hermiro J�nior
@since 12/05/2020
@version 1.0
@Param:
	oBody		=	Objeto Json da Requisi��o da API
	aTitulos	=	Array que guardara os dados dos Titulos 
	aTitTrans	=	Array que guardara os dados da Transferencia de Cart.
	aRastro 	=	Array que guardara os dados do Rastreio dos titulos.
	aErro		=	Array que guardara possiveis erros de processamentos
					e/ou tratamento dos dados.
/*/
//-------------------------------------------------------------------
Static Function RetDados(oBody As J, aTitulos as Array, aTitTrans As Array, aRastro As Array, aErro As Array ) As Logical 

	Local aNames		As Array 
	Local aInd			As Array 
	Local aTemp			As Array 
	Local aTempTrf		As Array 
	Local aTmpRastro	As Array 
	Local aGetSX5		As Array
	Local cTipo 		As Character 
	Local cContent		As Character 
	Local cPrefixo		As Character
	Local cTpTit		As Character
	Local cNatureza		As Character
	Local nX			As Numeric
	Local nQtdTit		As Numeric
	Local nTam 			As Numeric
	Local nPos			As Numeric
	Local lRet			As Logical

	Default aTitulos	:= {}
	Default aTitTrans	:= {}
	Default aRastro		:= {}
	Default aErro		:= {}

	aNames				:= {}
	aInd				:= {"E1_FILIAL","E1_PREFIXO","E1_NUM","E1_PARCELA","E1_TIPO","E1_CLIENTE","E1_LOJA","E1_VALOR"}
	aTemp				:= {}
	aTempTrf			:= {}
	aTmpRastro			:= {}
	aGetSX5				:= {}
	cTipo 				:= ''
	cContent			:= ''
	cPrefixo			:= SuperGetMv("MV_SUPPREF",.F.," ")
	cTpTit				:= SuperGetMv("MV_SUPTIPO",.F.," ")
	cNatureza			:= SuperGetMv("MV_SUPNATR",.F.," ")
	nX					:= 0
	nQtdTit				:= 0
	nTam 				:= 0
	nPos				:= 0
	lRet				:= .T.
	
	// Verifica se Prefixo, Tipo e Natureza existem
	//Natureza
	If !FinVldNat( .F., cNatureza, 1 )        
		aErro	:= {.F.,STR0003, STR0008}
	EndIf 
	// Tipo
	
	aGetSX5	:= FWGetSX5( "05", cTpTit )

	If Len(aGetSX5) = 0
		aErro	:= {.F.,STR0003, STR0009}
	EndIf 

	If Empty(aErro)
		For nQtdTit	:= 1 to Len(oBody:TITULOS)
			//Busca os dados referentes ao (Nosso Numero e Linha Digitavel)
			GetInfoSE1(oBody, @aTemp, @aRastro, @aErro)

			If Empty(aErro)
				aNames	:= oBody:Titulos[nQtdTit]:GetNames()

				aAdd ( aTemp, {"E1_FILIAL"		, xFilial('SE1')		, Nil})
				aAdd ( aTemp, {"E1_PREFIXO"	, cPrefixo				, Nil})
				aAdd ( aTemp, {"E1_TIPO"		, cTpTit					, Nil})
				aAdd ( aTemp, {"E1_NATUREZ"	, cNatureza				, Nil})

				For nX:=1 to Len(aNames)
					nTam		:= TamSx3(AllTrim(aNames[nX]))[1]
					cTipo 	:= TamSx3(AllTrim(aNames[nX]))[3]
					cContent	:= oBody:Titulos[nQtdTit]:getJsonText(aNames[nX])

					// Realiza o tratamento pelo Tipo e Tamanho do Campo.
					If cTipo == 'N'
						aAdd ( aTemp, {AllTrim(aNames[nX]),Val(cContent),Nil})
					ElseIf cTipo == 'D'
						aAdd ( aTemp, {AllTrim(aNames[nX]), iIf(cContent $ '/',cToD( cContent ), sToD( cContent )), Nil })
					Else
						If AllTrim(aNames[nX]) == "E1_NUM"
							If Len(cContent) < nTam
								cContent := PADL(cContent,nTam,"0")
							EndIf
						EndIf
						aAdd ( aTemp, {AllTrim(aNames[nX]), PadR(AllTrim(cContent),nTam),Nil })
					EndIf 
				Next nX
				aAdd(aTitulos,aTemp)

				//Adicionar os dados da Array de Rastro.
				For nX:=1 to Len(aInd)
					nPos := Ascan(aTemp,{|x| x[1]==AllTrim(aInd[nX])})
					If nPos > 0
						aAdd(aTmpRastro,aTemp[nPos,2])
					Endif
				Next nX
				aAdd(aRastro,aTmpRastro)

				//Transferencia para Carteira Bloqueada.
				aAdd( aTempTrf,{"E1_FILIAL"	, aTemp[Ascan(aTemp,{|x| x[1]=="E1_FILIAL"})  ,2]	, Nil})
				aAdd( aTempTrf,{"E1_PREFIXO", aTemp[Ascan(aTemp,{|x| x[1]=="E1_PREFIXO"}) ,2]	, Nil})
				aAdd( aTempTrf,{"E1_NUM"	, aTemp[Ascan(aTemp,{|x| x[1]=="E1_NUM"}) 	  ,2]	, Nil})
				aAdd( aTempTrf,{"E1_PARCELA", aTemp[Ascan(aTemp,{|x| x[1]=="E1_PARCELA"}) ,2]	, Nil})
				aAdd( aTempTrf,{"E1_TIPO"	, aTemp[Ascan(aTemp,{|x| x[1]=="E1_TIPO"}) 	  ,2]	, Nil})
				aAdd( aTempTrf,{"E1_CLIENTE", aTemp[Ascan(aTemp,{|x| x[1]=="E1_CLIENTE"}) ,2]	, Nil})
				aAdd( aTempTrf,{"E1_LOJA"	, aTemp[Ascan(aTemp,{|x| x[1]=="E1_LOJA"}) 	  ,2]	, Nil})

				aAdd(aTitTrans,aTempTrf)
				aTemp			:= {}
				aTmpRastro	:= {}
				aTempTrf		:= {}
				aNames	:= {}
			EndIf 
		Next nQtdTit
	Else
		lRet := aErro[1]
	EndIf 

Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} GetInfoSE1
Descricao:  Busca os dados do Titulo Original para gerar os dados dos 	
			novos titulos.

@author Hermiro J�nior
@since 13/05/2020
@version 1.0

@Param:
	oBody		= Objeto Json com os dados da Requisi��o
	aTemp		= Array que receber� os dados retornados da Query
	aRastro	= Array que receber� os dados para realizar o Rastreio
	aErro		= Array que receber� um possivel erro de processamento
/*/
//-------------------------------------------------------------------
Static Function GetInfoSE1(oBody As J, aTemp As Array , aRastro As Array, aErro As Array) As Logical 

	Local cQuery	As Character
	Local LinDig	As Character
	Local NosNum	As Character
	Local cAlias	As Character
	Local lRet		As Logical 

	Default aBody	:= Nil
	Default aTemp	:= {}
	Default aRastro	:= {}
	Default aErro	:= {}

	lRet			:= .F.
	cQuery 			:= ''
	LinDig			:= ''
	NosNum			:= ''
	cAlias			:= getNextAlias()
	
	// Resgata o nosso numero e linha digitavel
	LinDig	:= oBody:BOLETOS_IMPLANTADOS[1]:getJsonText("LINHADIGITAVEL")
	NosNum	:= oBody:BOLETOS_IMPLANTADOS[1]:getJsonText("NOSSONUMERO")

	dbSelectArea('SE1')

	cQuery	:= "SELECT													   " + Enter 
	cQuery 	+= "	E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO,    " + Enter
	cQuery 	+= "	E1_CLIENTE, E1_LOJA, E1_NOMCLI, E1_MOEDA, E1_VALOR 	   " + Enter
	cQuery 	+= "FROM "+RetSqlName("SE1")+ " SE1 						   " + Enter
	cQuery 	+= "WHERE 													   " + Enter 
	cQuery   += " E1_FILIAL = '"+xFilial('SE1')+"' 						   " + Enter
	cQuery 	+= " AND (E1_CODDIG = '"+LinDig+"' OR E1_CODBAR ='"+LinDig+"') " + Enter 
	cQuery 	+= " AND E1_NUMBCO = '"+NosNum+"' 							   " + Enter 
	cQuery 	+= " AND SE1.D_E_L_E_T_ = ' ' 								   " + Enter 

	dbUseArea( .T., "TOPCONN", TcGenQry( , , cQuery ), cAlias, .F., .T. )

	dbSelectArea(cAlias)
	
	If  !(cAlias)->(Eof())
		// Adiciona os dados na Array que ser� enviada para a Array da SE1
		aAdd( aTemp,{"E1_CLIENTE"	, (cAlias)->E1_CLIENTE	, Nil})
		aAdd( aTemp,{"E1_LOJA"		, (cAlias)->E1_LOJA		, Nil})
		aAdd( aTemp,{"E1_NOMCLI"	, (cAlias)->E1_NOMCLI	, Nil})
		aAdd( aTemp,{"E1_MOEDA"		, (cAlias)->E1_MOEDA	, Nil})

		If Empty(aRastro)
			// Adiciona no Array para realizar o Rastro dos Titulos.
			aAdd( aRastro,{	(cAlias)->E1_FILIAL		,;
							(cAlias)->E1_PREFIXO	,;
							(cAlias)->E1_NUM		,;
							(cAlias)->E1_PARCELA	,;
							(cAlias)->E1_TIPO		,;
							(cAlias)->E1_CLIENTE	,;
							(cAlias)->E1_LOJA		,;
							(cAlias)->E1_VALOR	 	;					 
							})
		EndIf
		lRet	:= .T.
	EndIf 

	(cAlias)->(dbCloseArea())

	If !lRet
		aErro := {.F.,STR0003, STR0010}
	EndIf

Return lRet