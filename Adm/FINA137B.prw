#Include 'PROTHEUS.CH'
#Include 'RESTFUL.CH'
#Include "FWMVCDEF.CH"
#Include "FINA137B.ch"

#Define Enter Chr(13)+Chr(10)

//-------------------------------------------------------------------
/*/{Protheus.doc} FINA137B
Descricao: 	Rotina Responsavel pela Inclus�o de Titulos no C/R,
				Transferencia de Carteira (Carteira Bloqueada),
				Inclus�o do Rastreio dos Titulos.
				
@author Hermiro J�nior
@since 11/05/2020
@version 1.0

@Param: 
aTitulos 	-> Array contendo os dados dos Titulos que serao gerados
aTitTrans	-> Array contendo os dados dos Titulos para Transferencia
				de Carteira (Carteira Bloqueada).
aTitRastro	-> Array contendo os dados dos Titulos para Rastreio

/*/
//-------------------------------------------------------------------
Function FINA137B(aTitulos As Array, aTitTrans As Array, aRastro As Array) As Array

	Local aRetorno			As Array
	Local aVetor			As Array
	Local aErroAuto			As Array
	Local aRastroOri		As Array
	Local aRastroDest		As Array
	Local nX 				As Numeric
	Local nVlrProc			As Numeric
	Local oTitTRF			As J
	Local cMsg				As Character 
	Local cCode				As Character 
	Local cCarteira			As Character 
	Local cMensagem			As Character 
	Local cRetType			As Character 
	Local lRet				As Logical 

	Private  lMsErroAuto	As Logical 
	
	Default aTitulos 		:= {}
	Default aTitTrans 		:= {}
	Default aRastro 		:= {}

	aRetorno				:= {} 
	aVetor					:= {}
	aErroAuto				:= {}
	aRastroOri				:= {}
	aRastroDest				:= {}
	nX 						:= 0
	nVlrProc				:= 0
	oTitTRF					:= JsonObject():New()
	cMsg					:= ''
	cCode					:= ''
	cMensagem				:= " "
	cRetType				:= " "
	cCarteira				:= SuperGetMv("MV_SUPCART",.F.," ")
	lRet					:= .T.
	lMsErroAuto 			:= .F.	

	//Verifica se a Carteira foi Cadastrada.
	dbSelectArea('FRV')
	dbSetOrder(1)

	If !dbSeek(xFilial('FRV') + cCarteira)
		cCode	:= STR0001
		cMsg	:= STR0003
	Else
		// Ordena array conforme dicionario de dados
		dbSelectArea('SE1')
		
		Begin Transaction 

			For nX:=1 to Len(aTitulos)
				aVetor := FWVetByDic( aTitulos[nX], "SE1", .F. , )

				//Inicia o controle de transa��o
				//Chama a rotina autom�tica
				MSExecAuto({|x,y| FINA040(x,y)}, aVetor, 3)
				
				//Se houve erro, mostra o erro ao usu�rio e desarma a transa��o
				If lMsErroAuto
					aErroAuto := GetAutoGRLog()
					aEval( aErroAuto, {|x| cMsg += x } )
					cCode 	:= STR0001
					lRet		:= .F.
					cMsg		:= STR0004

					DisarmTransaction()
					Break
				Else
					cCode 	:= STR0002
					lRet		:= .T.
					cMsg		:= STR0005+Enter
				EndIf
				aVetor := {} 
			Next nX

			If lRet
				For nX:=2 to Len(aRastro)
					aAdd(aRastroDest,aRastro[nX])		
				Next nX
				aAdd(aRastroOri,aRastro[1])
				nVlrProc	:= aRastro[1,8]
				
				// Chama Rotina de Rastreio	
				CallRastro(aRastroOri,aRastroDest, nVlrProc )
				cCode	:= STR0002
				lRet	:= .T.
				cMsg	+= STR0006+Enter
			EndIf

			If lRet
				For nX:=1 to Len(aTitTrans)
					// Chama Rotina de Transferencia de Carteira.
					aVetor	:= FWVetByDic( aTitTrans[nX], "SE1", .F. , )
					
					dbSelectArea('SE1')
					SE1->(dbSetOrder(1))

					If MsSeek(SE1->(FWCodFil())+aVetor[2,2]+aVetor[3,2]+aVetor[4,2])
						oTitTRF["formattedErpId"]	:= &(SE1->(IndexKey(1)))
						oTitTRF["history"]			:= STR0009
						oTitTRF["date"]				:= SE1->E1_EMISSAO
						oTitTRF["localAmount"]		:= SE1->E1_VALOR
						oTitTRF["feeAmount"]		:= SE1->E1_DESCONT

						If F136Transf(oTitTRF , cCarteira, @cMensagem, @cRetType)
							cCode 	:= STR0002
							lRet	:= .T.
							cMsg	+= STR0007+Enter
						Else
							cCode 	:= STR0001
							lRet	:= .F.
							cMsg	:= STR0008+AllTrim(SE1->E1_NUM)+'/'+AllTrim(SE1->E1_PARCELA)+Enter
						EndIf 
					EndIf 
					aVetor	:= {}
				Next nX
			EndIf
		End Transaction 
	EndIf 

	aRetorno	:= {lRet,cCode,cMsg}

Return aRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} CallRastro
Descricao: 	Rotina Responsavel pela gera��o do Rastreio dos Titulos 
			(Original e dos Novos Titulos)

@author Hermiro J�nior
@since 11/05/2020
@version 1.0
@Param: 
	aRastroOri		=  Dados do Titulo de Origem do Rastreio
	aRastroDest		=	Dados do(s) Titulos(s) de Destino
	nVlrProc			=	Valor do Processo de Rastreio
/*/
//-------------------------------------------------------------------

Static Function CallRastro(aRastroOri As Array, aRastroDest As Array, nVlrProc As Numeric) As Logical

	Default aRastroOri	:= {}
	Default aRastroDest	:= {}
	Default nVlrProc	:= 0

	//->Tipos de Rastros
 	//->1 - Desdobramento
	//->2 - Fatura ou Liquidacao  
	FINRSTGRV(2,"SE1",aRastroOri,aRastroDest,nVlrProc)

Return .T.