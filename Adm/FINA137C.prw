#Include 'PROTHEUS.CH'
#Include 'RESTFUL.CH'
#Include "FWMVCDEF.CH"
#Include "FINA137C.ch"

#Define Enter Chr(13)+Chr(10)
//-------------------------------------------------------------------
/*/{Protheus.doc} FINA137C
Descricao: 	API REST dos Eventos de Coobriga��o e Baixa do Projeto 
			Totvs Supplier+
@author Hermiro J�nior
@since 22/05/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function FINA137C()
Return

// Servi�o - COOBRIGA��O
WsRestFul FINA137C DESCRIPTION STR0001
	
	WsMethod POST Description STR0002 WsSyntax "FINA137C"

End WsRestFul
//-------------------------------------------------------------------
/*/{Protheus.doc} Metodo Post | 
Descricao : Metodo POST da API de Eventos do Projeto Supplier+

@author Hermiro J�nior
@since 07/05/2020
@version 1.0
/*/
//-------------------------------------------------------------------
WsMethod POST WsReceive RECEIVE WsService FINA137C

	Local aRetAPI		As Array
	Local aRetAll		As Array
	Local aTitulos		As Array
	Local aTitCP		As Array
	Local oBody			As J 
	Local lRpcSetEnv	As Logical
	Local cEmp			As Character
	Local cFil 			As Character
	Local cIdEvento		As Character
	Local cMessage		As Character

	aRetAPI				:= {}
	aRetAll				:= {}
	aTitulos			:= {}
	aTitCP				:= {}
	lRpcSetEnv			:= .T.
	cEmp				:= ''
	cFil 				:= ''
	cIdEvento			:= ''
	cMessage			:= ''
	// recupera o body da requisi��o
	oBody  := JsonObject():New()

	If ValType(oBody:fromJson( AllTrim( self:getContent() ) )) != 'U'	
		cCode 		:= STR0003
		cMessage	:= STR0005
	Else
		cEmp 		:= oBody:getJsonText("EMPRESA")
		cFil 		:= oBody:getJsonText("FILIAL")
		cIdEvento	:= oBody:getJsonText("TIPO")
	
		// Verifica se os campos Obrigatorios para abertura da Empresa/Filial foram preenchidos.
		If (Empty(cEmp) .Or. AllTrim(cEmp)=='null')
			cCode 		:= STR0003
			cMessage	:= STR0006
		ElseIf (Empty(cFil) .Or. AllTrim(cFil)=='null')
			cCode 		:= STR0003
			cMessage	:= STR0007
		ElseIf (Empty(cIdEvento) .Or. AllTrim(cIdEvento)=='null')
			cCode 	:= STR0003
			cMessage	:= STR0008
		Else
			RPCSetType(3)  
			RpcSetEnv(cEmp,cFil,,,,GetEnvServer(),{ }) 
			// Resgata os dados do Post
			aRetAPI 	:= GetInfo(oBody, @cIdEvento, @aTitulos, @aTitCP)
			cMessage	+= aRetAPI[3]+Enter
			lRpcSetEnv	:= .F.
			
			If aRetAPI[1]
				aRetAPI		:= FINA137D(cIDEvento, aTitulos, aTitCP)
				cMessage	+= aRetAPI[3]+Enter
			EndIf 
				
			aRetAll	:= {.T., aRetAPI[2], iIf(!Empty(cMessage),cMessage,aRetAPI[3])}

			//-> Mensagem de Retorno da Requisi��o
			self:setContentType("application/json")
			self:setResponse(fwJsonSerialize(StatReq(aRetAll[2],aRetAll[3])))
		EndIf 
	EndIf

	// Realiza o fechamento do Ambiente
	If !lRpcSetEnv
		RpcClearEnv()
	Endif
	Freeobj(oBody)

	If Len(aRetAll) = 0
		aRetAll := {.F.}
	Endif
	
Return aRetAll[1]
//-------------------------------------------------------------------
/*/{Protheus.doc} GetInfo
Descricao: 	Realiza o tratamento dos dados recebidos pelo POSt da API
				e adiciona os dados nas variaveis que executar�o o 
				processamento.

@author Hermiro J�nior
@since 22/05/2020
@version 1.0

@Param:

cIDEvento	= ID do Evento que sera realizado (1-Baixa | 3-Coobrigacao)
aTitulos		= Array que guardara os dados dos Titulos 
oBody			= Json contendo os dados do POST
aTitCP		= Array que guardara os dados do Titulo CP

/*/
//-------------------------------------------------------------------
Static Function GetInfo(oBody As J, cIdEvento As Character , aTitulos As Array, aTitCP As Array) As Array

	Local aRetorno	As Array
	Local aTemp		As Array
	Local aGetSX5	As Array
	Local cTipo 	As Character 
	Local cContent	As Character 
	Local cPrefixo	As Character 
	Local cTpTit	As Character 
	Local cNatureza	As Character 
	Local cNatCP	As Character
	Local cFornece	As Character 
	Local cLoja		As Character 
	Local nTam 		As Numeric 
	Local nX		As Numeric 
	Local nQtdTit	As Numeric 
	Local nDiasVcto	As Numeric 

	aRetorno		:= {}
	aTemp			:= {}
	aGetSX5			:= {}
	cTipo 			:= ''
	cContent		:= ''
	cPrefixo		:= SuperGetMv("MV_SUPPREF",.F.," ")
	cTpTit			:= SuperGetMv("MV_SUPTIPO",.F.," ")
	cNatureza		:= SuperGetMv("MV_SUPNATR",.F.," ")
	cNatCP			:= SuperGetMv("MV_SUPNATP",.F.," ")
	cFornece		:= SuperGetMv("MV_SUPFORN",.F.," ")
	cLoja			:= SuperGetMv("MV_SUPLOJA",.F.," ")
	nTam 			:= 0
	nX				:= 0
	nQtdTit			:= 0
	nDiasVcto		:= SuperGetMv("MV_SUPVECP",.F.,0)

	// Verifica se Prefixo, Tipo e Natureza existem
	//Natureza
	If !FinVldNat( .F., cNatureza, 1 )        
		aRetorno	:= {.F.,STR0003, STR0009}
	ElseIf !FinVldNat( .F., cNatCP, 1 )     
		aRetorno	:= {.F.,STR0003, STR0018}
	EndIf 
	// Tipo
	aGetSX5	:= FWGetSX5( "05", cTpTit )

	If Len(aGetSX5) = 0
		aRetorno	:= {.F.,STR0003, STR0009}
	EndIf

	dbSelectArea('SA2')
	dbSetOrder(1)
	If !dbSeek(xFilial('SA2')+cFornece+cLoja)
		aRetorno	:= {.F.,STR0003, STR0011}
	EndIf

	If Empty(aRetorno)
		If cIdEvento == '1' // BAIXA
		
			//Extrai os dados da baixa
			If ValType(oBody:CR_BAIXA) == 'A'
				For nQtdTit:= 1 to Len(oBody:CR_BAIXA)
					aNames	:= oBody:CR_BAIXA[nQtdTit]:getNames()

					aAdd ( aTemp, {"E1_FILIAL"		, xFilial('SE1')		, Nil})
					aAdd ( aTemp, {"E1_PREFIXO"	, cPrefixo				, Nil})
					aAdd ( aTemp, {"E1_TIPO"		, cTpTit					, Nil})
					aAdd ( aTemp, {"E1_NATUREZ"	, cNatureza				, Nil})

					For nX:=1 to Len(aNames)
						nTam	:= TamSx3(AllTrim(aNames[nX]))[1]
						cTipo 	:= TamSx3(AllTrim(aNames[nX]))[3]
						cContent:= oBody:CR_BAIXA[nQtdTit]:getJsonText(aNames[nX])

						// Realiza o tratamento pelo Tipo e Tamanho do Campo.
						If cTipo == 'N'
							aAdd ( aTemp, {AllTrim(aNames[nX]),Val(cContent),Nil})
						ElseIf cTipo == 'D'
							aAdd ( aTemp, {AllTrim(aNames[nX]), iIf(cContent $ '/',cToD( cContent ), sToD( cContent )), Nil })
						Else
							If AllTrim(aNames[nX]) == "E1_NUM"
								If Len(cContent) < nTam
									cContent := PADL(cContent,nTam,"0")
								EndIf
							EndIf
							
							aAdd ( aTemp, {AllTrim(aNames[nX]), PadR(AllTrim(cContent),nTam),Nil })
						EndIf 
					Next nX
					aAdd(aTitulos,aTemp)
					aNames	:= {}
					aTemp	:= {}
				Next nQtdTit
				
				aRetorno	:= {.T.,STR0004,STR0012}  
			Else
				aRetorno	:= {.F.,STR0003,STR0013} 
			EndIf 
		ElseIf cIdEvento == '3' // COOBRIGA��O

			//Extrai os dados do Contas a Receber
			If ValType(oBody:CR_COOBRIGACAO) == 'A'
				For nQtdTit:= 1 to Len(oBody:CR_COOBRIGACAO)
					
					aNames	:= oBody:CR_COOBRIGACAO[nQtdTit]:getNames()
					
					aAdd ( aTemp, {"E1_FILIAL"	, xFilial('SE1')	, Nil})
					aAdd ( aTemp, {"E1_PREFIXO"	, cPrefixo			, Nil})
					aAdd ( aTemp, {"E1_TIPO"	, cTpTit			, Nil})
					aAdd ( aTemp, {"E1_NATUREZ"	, cNatureza			, Nil})
			
					For nX:=1 to Len(aNames)
						nTam	:= TamSx3(AllTrim(aNames[nX]))[1]
						cTipo 	:= TamSx3(AllTrim(aNames[nX]))[3]
						cContent:= oBody:CR_COOBRIGACAO[nQtdTit]:getJsonText(aNames[nX])

						// Realiza o tratamento pelo Tipo e Tamanho do Campo.
						If cTipo == 'N'
							aAdd ( aTemp, {AllTrim(aNames[nX]),Val(cContent),Nil})
						ElseIf cTipo == 'D'
							aAdd ( aTemp, {AllTrim(aNames[nX]), iIf(cContent $ '/',cToD( cContent ), sToD( cContent )), Nil })
						Else
							If AllTrim(aNames[nX]) == "E1_NUM"
								If Len(cContent) < nTam
									cContent := PADL(cContent,nTam,"0")
								EndIf
							EndIf
							
							aAdd ( aTemp, {AllTrim(aNames[nX]), PadR(AllTrim(cContent),nTam),Nil })
						EndIf 
					Next nX
					aAdd(aTitulos,aTemp)
					aNames	:= {}
					aTemp	:= {}
				Next nQtdTit
				aRetorno	:= {.T.,STR0004,STR0014} 
			Else
				aRetorno	:= {.F.,STR0003,STR0015} 
			EndIf 

			//Resgata os dados para gerar o Titulo do Contas a Pagar.
			If ValType(oBody:CP_COOBRIGACAO) == 'A'
				For nQtdTit:= 1 to Len(oBody:CP_COOBRIGACAO)
			
					dbSelectArea('SE2')
					aNames	:= oBody:CP_COOBRIGACAO[nQtdTit]:getNames()
					aAdd( aTemp, {"E2_FILIAL"	, xFilial('SE2')			, Nil})
					aAdd( aTemp, {"E2_PREFIXO"	, PadR(AllTrim(cPrefixo)	,TamSx3("E2_PREFIXO")[1])	, Nil})
					aAdd( aTemp, {"E2_TIPO"		, PadR(AllTrim(cTpTit)		,TamSx3("E2_TIPO")[1])		, Nil})
					aAdd( aTemp, {"E2_NATUREZ"	, PadR(AllTrim(cNatCP)		,TamSx3("E2_NATUREZ")[1])	, Nil})	
					aAdd( aTemp, {"E2_FORNECE"	, PadR(AllTrim(cFornece)	,TamSx3("E2_FORNECE")[1])	, Nil})	
					aAdd( aTemp, {"E2_LOJA"		, PadR(AllTrim(cLoja)		,TamSx3("E2_LOJA")[1])		, Nil})	

					For nX:=1 to Len(aNames)
						nTam		:= TamSx3(AllTrim(aNames[nX]))[1]
						cTipo 	:= TamSx3(AllTrim(aNames[nX]))[3]
						cContent	:= oBody:CP_COOBRIGACAO[nQtdTit]:getJsonText(aNames[nX])

						// Realiza o tratamento pelo Tipo e Tamanho do Campo.
						If cTipo == 'N'
							aAdd ( aTemp, {AllTrim(aNames[nX]),Val(cContent),Nil})
							
							If AllTrim(aNames[nX]) == "E2_VALOR"
								aAdd ( aTemp, {"E2_VLCRUZ",Val(cContent),Nil})	
							EndIf
						ElseIf cTipo == 'D'
							aAdd ( aTemp, {AllTrim(aNames[nX]), iIf(cContent $ '/',cToD( cContent ), sToD( cContent )), Nil })
							
							//Adiciona Vencimento
							If AllTrim(aNames[nX]) == "E2_EMISSAO"
								aAdd ( aTemp, {"E2_VENCTO" , iIf(cContent $ '/',cToD( cContent )+nDiasVcto, sToD( cContent )+nDiasVcto), Nil })	
								aAdd ( aTemp, {"E2_VENCREA", iIf(cContent $ '/',cToD( cContent )+nDiasVcto, sToD( cContent )+nDiasVcto), Nil })	
							EndIf 
						Else
							If AllTrim(aNames[nX]) == "E2_NUM"
								If Len(cContent) < nTam
									cContent := PADL(cContent,nTam,"0")
								EndIf
							EndIf

							aAdd ( aTemp, {AllTrim(aNames[nX]), PadR(AllTrim(cContent),nTam),Nil })
						EndIf 
					Next nX
					
					aAdd(aTitCP,aTemp)
					aNames	:= {}
					aTemp	:= {}
				Next nQtdTit
				aRetorno	:= {.T.,STR0004,STR0016} 
			Else
				aRetorno	:= {.F.,STR0003,STR0017} 
			EndIf 
		EndIf 
	EndIf 

Return aRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} StatReq
Descricao: Realiza o Tratamento para envio de Mensagem de Retorno da API 

@author Hermiro J�nior
@since 22/05/2020
@version 1.0

@Param:
	cCode		=  Codigo do retorno da API
	cMessage	=	Mensagem de Retorno da API
/*/
//-------------------------------------------------------------------
Static Function StatReq(cCode As Character, cMessage As Character) As J

	Local oJsonRet	As J
	Local oJsonBody	As J

	Default cCode		:= ''
	Default cMessage	:= ''

	//Cria o Objeto de Retono da Requisi��o.
	oJsonRet  := JsonObject():New()
	oJsonRet["StatusRequest"] := {}

	oJsonBody := JsonObject():New()
	oJsonBody["code"]          := cCode
	oJsonBody["message"]       := cMessage

	AAdd( oJsonRet["StatusRequest"], oJsonBody )

Return oJsonRet