#Include 'PROTHEUS.CH'
#Include 'RESTFUL.CH'
#Include "FWMVCDEF.CH"
#Include "FINA137D.ch"

#Define Enter Chr(13)+Chr(10)
//-------------------------------------------------------------------
/*/{Protheus.doc} FINA137D
Descricao: 	Funcao Responsavel pelo Processamento dos dados dos 
				Eventos do Projeto Totvs Supplier+
				* Evento: Coobriga��o
					-> Atualizacao de carteira, 
					-> Inclusao de Valor acessorio
					-> Inclusao do Contas a  Pagar
				* Evento: Baixa
					-> Baixar Titulos Contas a Receber 
@author Hermiro J�nior
@since 22/05/2020
@version 1.0
@Param:
	cIdEvento 	= 	Identificador do Evento
	aTitulos		= 	Array contendo os dados para gerar o Titulo C/R
	aTitCP		= 	Array contendo os dados para gerar o Titulo C/P
/*/
//-------------------------------------------------------------------
Function FINA137D(cIDEvento As Character, aTitulos As Array, aTitCP As Array ) As Array

	Local aRetorno	As Array
	Local aRetAll	As Array 
	Local cMensagem	As Character

	Default cIDEvento	:= ''
	Default aTitulos	:= {}
	Default aTitCP		:= {}

	aRetorno			:= {}
	aRetAll				:= {}
	cMensagem			:= ''

	//ID do Evento = COOBRIGACAO
	If cIDEvento == '3'
		
		// Realiza a Transferencia de carteira
		aRetorno 	:= TrfCartSup(aTitulos)
		cMensagem	+= aRetorno[3]+Enter 

		If aRetorno[1] 
			// Realiza a gera��o de Valores Acessorios 
			aRetorno 	:= IncTitVA(aTitulos)
			cMensagem	+= aRetorno[3]+Enter 
		EndIf 

		If aRetorno[1]
			//Chama Rotina de cria��o de Contas a Pagar
			aRetorno	:= IncTitCP(aTitCP)
			cMensagem	+= aRetorno[3]+Enter 
		EndIf
	ElseIf cIDEvento == '1' //Baixa do Titulo por Pagamento do Cliente
		aRetorno	:= fBaixa(aTitulos) 
		cMensagem	+= aRetorno[3]+Enter 
	Else
		aRetorno	:=	{.F., STR0001, STR0003}
		cMensagem	+= aRetorno[3]+Enter
	EndIf 

	aRetAll	:= {aRetorno[1],aRetorno[2],cMensagem}

Return aRetAll
//-------------------------------------------------------------------
/*/{Protheus.doc} fBaixa
Descricao:  Funcao responsavel pela Baixa dos Titulos do C/R
@author Hermiro J�nior
@since 25/05/2020
@version 1.0
@Param:
	aTitulos 	->	Array contendo os dados dos Titulos que serao
					baixados.
/*/
//-------------------------------------------------------------------
Static Function fBaixa(aTitulos As Array) As Array

	Local lRet			As Logical  
	Local nX			As Numeric 
	Local aTemp			As Array
	Local aRetorno		As Array
	Local cMsg			As Character
	Local cCode			As Character
	Local oTitBaixa		As J

	Default aTitulos	:= {}

	lRet				:= .F.
	nX					:= 0
	aTemp				:= {}
	aRetorno			:= {}
	oTitBaixa			:= JsonObject():New()
	cMsg				:= ''
	cCode				:= ''

	dbSelectArea('SE1')

	Begin Transaction 

		For nX:=1 to Len(aTitulos)

			aTemp	:= FWVetByDic( aTitulos[nX], "SE1", .F. , )

			dbSelectArea('SE1')
			SE1->(dbSetOrder(1))

			If SE1->(DBSeek(xFilial('SE1') + aTemp[2,2] + aTemp[3,2] +aTemp[4,2]))

				// Realiza Valida��es Pertinentes a Baixa de Titulo.
				If SE1->E1_SALDO == 0
					lRet	:= .F.
					cCode 	:= STR0001
					cMsg	:= STR0004
				ElseIf aTemp[Ascan(aTemp,{|x| x[1]=="E1_VALOR"}),2] > SE1->E1_SALDO
					lRet	:= .F.
					cCode 	:= STR0001
					cMsg	:= STR0005
				Else
					oTitBaixa["formattedErpId"]	:= &(SE1->(IndexKey(1)))
					oTitBaixa["history"]				:= STR0006
					oTitBaixa["date"]					:= SE1->E1_EMISSAO
					oTitBaixa["localAmount"]		:= SE1->E1_VALOR
					oTitBaixa["feeAmount"]			:= SE1->E1_DESCONT

					If F136Baixa(oTitBaixa)
						//F136Transf(oTitTRF , cCarteira, cMensagem, cRetType)
						cCode 	:= STR0002
						lRet	:= .T.
						cMsg	+= STR0007+AllTrim(SE1->E1_NUM)+'/'+AllTrim(SE1->E1_PARCELA)+Enter
					Else
						cCode 	:= STR0001
						lRet	:= .F.
						cMsg	+= STR0008+AllTrim(SE1->E1_NUM)+'/'+AllTrim(SE1->E1_PARCELA)+Enter
					EndIf 
				EndIf 
			EndIf 

			aTemp	:= {}
		Next nX
	End Transaction 

	aRetorno		:= {lRet,cCode,cMsg}

Return aRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} IncTitCP
Descricao: Funcao responsavel pela Icnlusao de Titulos do C/P

@author Hermiro J�nior
@since 22/05/2020
@version 1.0

@Param:
	aTitCP	=	Array contendo os dados para a geracao dos titulos 
				do Contas a Pagar.
/*/
//-------------------------------------------------------------------
Static Function IncTitCP(aTitCP As Array) As Array
	
	Local nX		As Numeric 
	Local aTemp		As Array 
	Local aRetorno	As Array
	Local cMsg		As Character
	Local cCode		As Character
	Local lRet		As Logical 

	Private  lMsErroAuto	As Logical 

	Default aTitCP	:= {}

	nX			:= 0
	aTemp		:= {}
	aRetorno	:= {}
	cMsg		:= ''
	cCode		:= ''
	lMsErroAuto	:= .F.
	lRet		:= .F.

	dbSelectArea('SE2')

	Begin Transaction 

		For nX:= 1 to Len(aTitCP)

			aTemp := FWVetByDic( aTitCP[nX], "SE2", .F. , )
			
			MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aTemp,, 3) 

			If lMsErroAuto
				aErroAuto := GetAutoGRLog()
				aEval( aErroAuto, {|x| cMsg += x } )

				cCode 	:= STR0001
				lRet	:= .F.
				cMsg	+= STR0009

				DisarmTransaction()
				Break
			Else
				cCode 	:= STR0002
				cMsg	:= STR0010
				lRet	:= .T.
			Endif
		Next nX
	End Transaction

	aRetorno		:= {lRet,cCode,cMsg}

Return aRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} TrfCartSup
Descricao: 	Funcao responsavel pela Transferencia de Carteira dos 
			titulos do Contas a Receber.

@author Hermiro J�nior
@since 22/05/2020
@version 1.0

@Param:
	aTitulos		= 	Titulos que ser�o transferidos para a Carteira 0
/*/
//-------------------------------------------------------------------
Static Function TrfCartSup(aTitulos As Array) As Array 
	
	Local nX			As Numeric
	Local cCode			As Character 
	Local cMsg			As Character 
	Local cCarteira		As Character 
	Local cMensagem		As Character 
	Local cRetType		As Character 
	Local	aRetorno	As Array
	Local aTemp			As Array
	Local lRet			As Logical 
	Local oTitTRF		As J
	
	cCode				:= ''
	cMsg				:= ''
	cCarteira			:= "0"
	cMensagem			:= " "
	cRetType			:= " "
	aTemp				:= {}
	aRetorno			:= {}
	lRet				:= .F.
	oTitTRF				:= JsonObject():New()
	nX					:= 0 
	
	For nX:=1 to Len(aTitulos)

		aTemp	:= FWVetByDic( aTitulos[nX], "SE1", .F. , )

		dbSelectArea('SE1')
		SE1->(dbSetOrder(1))

		If SE1->(DBSeek(xFilial('SE1')+aTemp[2,2]+aTemp[3,2]+aTemp[4,2]))

			oTitTRF["formattedErpId"]	:= &(SE1->(IndexKey(1)))
			oTitTRF["history"]			:= STR0011
			oTitTRF["date"]				:= SE1->E1_EMISSAO
			oTitTRF["localAmount"]		:= SE1->E1_VALOR
			oTitTRF["feeAmount"]		:= SE1->E1_DESCONT

			aTemp	:= {}

			If F136Transf(oTitTRF , cCarteira, cMensagem, cRetType)
				cCode 	:= STR0002
				lRet	:= .T.
				cMsg	:= STR0012+Enter
			Else
				cCode 	:= STR0001
				lRet	:= .F.
				cMsg	:= STR0013+AllTrim(SE1->E1_NUM)+'/'+AllTrim(SE1->E1_PARCELA)+Enter
			EndIf 
		Else
			cCode 	:= STR0001
			lRet	:= .F.
			cMsg	:= STR0014+Enter
		EndIf 	
	Next nX

	aRetorno		:= {lRet,cCode,cMsg}
	
Return aRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} IncTitVA
Descricao: 	Rotina responsavel em realizar a inclus�o dos Valores 
			Acessorios.
@author Hermiro J�nior
@since 22/05/2020
@version 1.0
@Param:
	aTitulos		= 	Array contendo os dados para Gerar o VA.

/*/
//-------------------------------------------------------------------
Static Function IncTitVA(aTitulos As Array) As Array 

	Local nX				As Numeric
	Local nOpc				As Numeric
	Local nVlrACE			As Numeric
	Local aTemp				As Array
	Local aValACE			As Array
	Local cCode				As Character 
	Local cMsg				As Character 
	Local cCodVA			As Character 
	Local lRet				As Logical 
	
	Private  lMsErroAuto	As Logical 

	nX						:= 0 
	nOpc					:= 4
	nVlrACE					:= SuperGetMv("MV_SUPVAPC",.F.,0)
	aValACE					:= {}
	aTemp					:= {}
	cCode					:= ''
	cMsg					:= ''
	cCodVA					:= SuperGetMv("MV_SUPCDVA",.F.," ")
	lRet					:= .F.
	lMsErroAuto 			:= .F.
	
	//Verificar se o codigo VA existe
	dbSelectArea('FKC')
	dbSetOrder(1)
	If !dbSeek(xFilial('FKC')+cCodVA)
		cCode 	:= STR0001
		lRet	:= .F.
		cMsg	:= STR0015
	Else
		dbSelectArea('SE1')
		
		Begin transaction 

			For nX:=1 to Len(aTitulos)
				//Chama a rotina autom�tica
				lMsErroAuto := .F.

				// Ordena o Array  
				aTemp := FWVetByDic( aTitulos[nX], "SE1", .F. , )

				//Adiciona valores acessorios 
				aAdd(aValACE, {cCodVA, nVlrACE})

				MSExecAuto({|a, b, c, d| FINA040(a, b,, c,,,, d)}, aTemp, nOpc,, aValACE)
				
				//Se houve erro, mostra o erro ao usu�rio e desarma a transa��o
				If lMsErroAuto
					aErroAuto := GetAutoGRLog()
					aEval( aErroAuto, {|x| cMsg += x } )
					
					cCode 	:= STR0001
					lRet	:= .F.
					cMsg	:= STR0016

					DisarmTransaction()
					Break
				Else
					cCode 	:= STR0002
					lRet	:= .T.
					cMsg	:= STR0017
				EndIf
			
				aValACE	:= {}
				aTemp	:= {}

			Next nX
		End Transaction 
	EndIf 

	aRetorno		:= {lRet,cCode,cMsg}

Return aRetorno
