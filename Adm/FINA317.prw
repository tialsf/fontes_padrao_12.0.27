#include "Protheus.ch"
#include "FILEIO.CH"
#include "FINA317.CH"


/*
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Oswaldo Leite         � Data �09/08/11  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados         ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function MenuDef()

Local aRotina := {  { "Pesquisar"     , "AxPesqui" , 0 , 1,,.F.} ,;
					{ "Visualizar"    ,	"AxVisual"  , 0 , 2} ,;
					{ "Compensar"	  ,	"CNI317PR"  , 0 , 4} ,;
 					{ "Estornar"	  ,	"Cni317RE"  , 0 , 4} ,;
					{ "Legenda"		  ,	"CNI317Leg"	, 0	, 7, ,.F.}}

Return(aRotina)

/*/
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 � CNI317RE   � Autor � Mauricio Pequim Jr.	 � Data � 20/12/98  ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Cancelamento de Liquida��o                 				    ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe	 � CNI317RE()													���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � Generico 												    ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CNI317RE(cAlias,cCampo,nOpcx,aCampos)

Local cIndex := ""
Local cTitulo := STR0001			/////"Cancelamento"
Local nOpct := 0
Private cCompCan := CriaVar("E1_NUM" , .F.)

cCompCan	:= SE2->E2_IDENTEE

DEFINE MSDIALOG oDlg4 FROM	20,1 TO 120,340 TITLE cTitulo PIXEL

	nEspLarg := 2
	nEspLin  := 2
	oDlg4:lMaximized := .F.
	oPanel4 := TPanel():New(0,0,'',oDlg4,, .T., .T.,, ,20,20)
	oPanel4:Align := CONTROL_ALIGN_ALLCLIENT

	@ nEspLin,  nEspLarg TO 36+nEspLin, 125+nEspLarg OF oPanel4 PIXEL
	@ 21+nEspLin, 14+nEspLarg MSGET cCompCan Valid If(nOpcT<>0,NaoVazio(cCompCan),.T.) SIZE 49, 11 OF oPanel4 PIXEL
	@ 11+nEspLin, 14+nEspLarg SAY STR0002 SIZE 49, 07 OF oPanel4 PIXEL                                 /////"Nro. Compensa��o"

	DEFINE SBUTTON FROM 10, 133 TYPE 1 ACTION (nOpct:=1,;
	IIF(CNI317CalCn(cCompCan,@cIndex),oDlg4:End(),nOpct:=0)) ENABLE OF oDlg4
	DEFINE SBUTTON FROM 23, 133 TYPE 2 ACTION (nOpcT:=0,oDlg4:End()) ENABLE OF oDlg4

ACTIVATE MSDIALOG oDlg4 CENTERED


if nOpct > 0//Inicia Regua de Andamento do Processamento
	Processa(  {|| Cni317Can(cAlias,cCampo,nOpcx,aCampos, nOpcT, cIndex) } , STR0003, STR0004, .F. )           ////"Aguarde"    "Executando processo"
else
	Cni317Can(cAlias,cCampo,nOpcx,aCampos, nOpcT, cIndex)
EndIf

Return

/*/
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 � CNI317CAN  � Autor � Mauricio Pequim Jr.	 � Data � 20/12/98  ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Cancelamento de Liquida��o                 				    ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe	 � CNI317CAN()													���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � Generico 												    ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CNI317CAN(cAlias,cCampo,nOpcx,aCampos, nOpct, cIndex)
//��������������������������������������������������������������Ŀ
//� Define Variaveis 														  �
//����������������������������������������������������������������
Local cArquivo,nTotal:=0,nHdlPrv:=0
Local lCabec := .F.
Local lPadrao:= VerPadrao("535")  // Cancelamento de Comp. Carteiras
Local cPadrao := "535"
Local cStrf
Local lDigita := .T.
Local nTotAbat := 0
Local nValRec := 0
Local nValPag := 0
Local lBxUnica := .F.
Local nRec450 := Recno()
Local nJuros := 0
Local nDescont := 0
Local nMulta	:= 0
Local cFornece
Local cLoja
local aBaixaSE3 := {}
Local nDecs	:=	2
Local nTaxa	:=	1
Local nValDifC	:=	0
Local lF450SE1C := ExistBlock("F450SE1C")
Local lF450SE2C := ExistBlock("F450SE2C")
Local aEstorno := {}
Local lEstorna := .F.
Local nX
Local aCancela := {}
Local aFiliais := {}
Local nRecAtu	:= SM0->(RECNO())
Local cEmpAtu	:= SM0->M0_CODIGO
Local cFilCan	:= cFilAnt
Local cFilAtu	:= cFilAnt
Local nRecPnl	:= SE2->(Recno())
Local aFlagCTB := {}
Local lUsaFlag	:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)
Local nInc		:= 0
Local aSM0
Local aFilCod	:= {}
Local cCompEfetuada := ""
Local cLayOut := SM0->M0_LEIAUTE

Private cLote
Private StrLctPad   := ""

aSM0	:= CniAbreSM0()
// Zerar variaveis para contabilizar os impostos da lei 10925.
VALOR5 := 0
VALOR6 := 0
VALOR7 := 0

// Estorna a operacao caso a matriz de rotina esteja com o conteudo 5 na posicao 4.
lEstorna := aRotina[nOpcx][4]==5
If lEstorna
	cTitulo := STR0102				////"Estornar"
Endif

//��������������������������������������������������������������Ŀ
//� Verifica o numero do Lote 											  �
//����������������������������������������������������������������
LoteCont("FIN")

//Abertura dos arquivos para utilizacao nas funcoes SumAbatPag e SumAbatRec
//Se faz necessario devido ao controle de transacao nao permitir abertura de arquivos
If Select("__SE1") == 0
	ChkFile("SE1",.F.,"__SE1")
Endif
If Select("__SE2") == 0
	ChkFile("SE2",.F.,"__SE2")
Endif

//��������������������������������������������������������������Ŀ
//� Carrega funcao Pergunte												  �
//����������������������������������������������������������������
SetKey (VK_F12,{|a,b| AcessaPerg("AFI450",.T.)})
pergunte("AFI450",.F.)

//���������������������������������������������������������������������������Ŀ
//� Ponto de entrada que controla se o titulo sera cancelado/extornado ou nao �
//�����������������������������������������������������������������������������
If ExistBlock("F450CAES")
	nOpct :=  ExecBlock("F450CAES",.F.,.F.,{cCompCan,nOpct})
EndIf

//�����������������������������������������������������Ŀ
//�Integracao com o SIGAPCO para lancamento via processo�
//�PcoIniLan("000018")                                  �
//�������������������������������������������������������
PcoIniLan("000018")

If nOpct == 1
	//-- Parametros da Funcao LockByName() :
	//   1o - Nome da Trava
	//   2o - usa informacoes da Empresa na chave
	//   3o - usa informacoes da Filial na chave


	If LockByName(cCompCan,.T.,.F.)

		SE5->(dbGoTop())

		ProcRegua( 0 )

		aEstorno := {}

		Begin Transaction

		While SE5->(!Eof())

			If SE5->E5_TIPODOC $ "DC/JR/MT/CM"

				SE5->(dbSkip())
				IncProc(STR0005)                        ////'Selecionando Registros'
				Loop
			Endif

			If (SE5->E5_RECPAG == "R" .And. !(SE5->E5_TIPO $ MV_CPNEG+"/"+MVPAGANT) ) .Or.( SE5->E5_RECPAG == "P" .And. ( SE5->E5_TIPO $ MV_CRNEG+"/"+MVRECANT ) )

				cCompEfetuada := cCompCan

				//����������������������������������������������������������Ŀ
				//� Cancela a compensacao de titulo a receber					 �  //0202000
				//������������������������������������������������������������
				dbSelectArea("SE1")
				dbSetOrder(2)

				If dbSeek(SE5->E5_FILIAL + SE5->(E5_CLIFOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO))

					nDecs	:=	MsDecimais(SE1->E1_MOEDA)
					nTaxa	:=	SE5->E5_VALOR/SE5->E5_VLMOED2
					nTotAbat 	:= SumAbatRec(SE1->E1_PREFIXO,SE1->E1_NUM,	SE1->E1_PARCELA,SE1->E1_MOEDA,"V",SE5->E5_DATA)
					If cPaisLoc == "BRA"
						If SE1->E1_MOEDA <= 1
							nValRec		:= xMoeda(SE5->E5_VALOR,1,SE1->E1_MOEDA,SE5->E5_DATA)
						Else
							nValRec		:= Round(NoRound(xMoeda(SE5->E5_VALOR,1,SE1->E1_MOEDA,SE5->E5_DATA,3),3),2)
						EndIf
						nJuros		:= xMoeda(SE5->E5_VLJUROS,1,SE1->E1_MOEDA,SE5->E5_DATA)
						nDescont		:= xMoeda(SE5->E5_VLDESCO,1,SE1->E1_MOEDA,SE5->E5_DATA)
						nMulta		:= xMoeda(SE5->E5_VLMULTA,1,SE1->E1_MOEDA,SE5->E5_DATA)
					Else
						nValRec		:= SE5->E5_VLMOED2
						nJuros		:= xMoeda(SE5->E5_VLJUROS,1,SE1->E1_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nDescont		:= xMoeda(SE5->E5_VLDESCO,1,SE1->E1_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nMulta		:= xMoeda(SE5->E5_VLMULTA,1,SE1->E1_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nValDifC		:= SE5->E5_VLCORRE
					Endif
					nValRec		+= nDescont - nJuros - nMulta
					nRec450 := SE1->(Recno())
					lBxUnica := IIf(STR(nValRec+nTotAbat+SE1->E1_SALDO,17,2) == STR(SE1->E1_VALOR,17,2),.T.,.F.)
					StrLctPad   := SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
					If nTotAbat > 0 .AND. SE1->E1_SALDO == 0
						nValRec += nTotAbat
						//������������������������������������������������������������������Ŀ
						//�Verifica se h� abatimentos para voltar a carteira                 �
						//��������������������������������������������������������������������
						SE1->(dbSetOrder(1))
						If SE1->(dbSeek(SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA))
							cTitAnt := (SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA)
							While SE1->(!Eof()) .and. cTitAnt == (SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA)
								If !SE1->E1_TIPO  $MVABATIM
									SE1->(dbSkip())
									Loop
								Endif
								//������������������������������������������������������������������Ŀ
								//�Volta t�tulo para carteira                                       �
								//��������������������������������������������������������������������
								Reclock("SE1")
								SE1->E1_BAIXA   := Ctod(" /  /  ")
								SE1->E1_SALDO   := E1_VALOR
								SE1->E1_DESCONT := 0
								SE1->E1_JUROS   := 0
								SE1->E1_MULTA   := 0
								SE1->E1_CORREC  := 0
								SE1->E1_VARURV  := 0
								SE1->E1_VALLIQ  := 0
								SE1->E1_LOTE    := Space(Len(E1_LOTE))
								SE1->E1_DATABOR := Ctod(" /  /  ")
								SE1->E1_STATUS  := "A"
								SE1->E1_IDENTEE := ""
								//����������������������������������������������Ŀ
								//� Carrega variaveis para contabilizacao dos    �
								//� abatimentos (impostos da lei 10925).         �
								//������������������������������������������������
								If SE1->E1_TIPO == MVPIABT
									VALOR5 := SE1->E1_VALOR
								ElseIf SE1->E1_TIPO == MVCFABT
									VALOR6 := SE1->E1_VALOR
								ElseIf SE1->E1_TIPO == MVCSABT
									VALOR7 := SE1->E1_VALOR
								Endif
								MsUnlock()
								SE1->(dbSkip())
							EndDo
						Endif
					Endif
					SE1->(dbGoto(nRec450))
					RecLock("SE1")
					SE1->E1_SALDO += nValRec
					SE1->E1_IDENTEE := ""
					If cPaisLoc == "CHI" .And. nValDifC <> 0
						E1_CAMBIO	-=	nValDifC
					Endif
					If lBxUnica
						SE1->E1_BAIXA   := Ctod(" /  /  ")
						SE1->E1_VALLIQ  := 0
						SE1->E1_SDACRES := SE1->E1_ACRESC
						SE1->E1_SDDECRE := SE1->E1_DECRESC
						SE1->E1_DESCONT := 0
						SE1->E1_JUROS   := 0
						SE1->E1_MULTA   := 0
						SE1->E1_CORREC  := 0
						SE1->E1_VARURV  := 0
						SE1->E1_VALLIQ  := 0
						SE1->E1_STATUS  := "A"
					Endif
					MsUnLock()
					//Ponto de entrada para gravacoes complementares
					If lF450SE1C
						ExecBlock("F450SE1C",.F.,.F.)
					Endif

					//����������������������������������������������������������Ŀ
					//� Inicializa variaveis para contabilizacao 					 �
					//������������������������������������������������������������
					VALOR := SE5->E5_VALOR
					VLRINSTR := VALOR
					If SE1->E1_MOEDA <= 5 .And. SE1->E1_MOEDA > 1
						cVal := Str(SE1->E1_MOEDA,1)
						If cPaisLoc == "BRA"
							VALOR&cVal := xMoeda(SE5->E5_VALOR,1,SE1->E1_MOEDA,SE5->E5_DATA)
						Else
							VALOR&cVal := SE5->E5_VLMOED2
						Endif
					EndIf
					//����������������������������������������������������������Ŀ
					//� Cancela compensacao						 							 �
					//������������������������������������������������������������
					dbSelectArea("SE5")
					nRecSE5 := SE5->(recno())
					SE5->(dbGotop())
					While SE5->(!Eof()) .and. SE5->E5_IDENTEE == cCompCan
						If SE5->(E5_FILIAL+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA) == ;
							xFilial("SE5")+SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA)
							RecLock("SE5")
							If ! lEstorna
//								Replace E5_SITUACA	With "C"
							    aadd(aCancela,SE5->(Recno()))
							Else
//								Replace E5_SITUACA	With "X"
								aadd(aEstorno,SE5->(Recno()))
							Endif
							MsUnLock()
							aadd(aBaixaSE3,{ SE5->E5_MOTBX , SE5->E5_SEQ , SE5->(Recno()) })
						Endif

						SE5->(dbSkip())
					Enddo
					SE5->(dbGoto(nRecSE5))
				EndIf
			Else
				//����������������������������������������������������������Ŀ
				//� Cancela a compensacao de titulo a receber				 �
				//������������������������������������������������������������
				dbSelectArea("SE2")
				dbSetOrder(6)

				cCompEfetuada := cCompCan

				If dbSeek(SE5->E5_FILIAL+SE5->(E5_CLIFOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO))
					nDecs	:=	MsDecimais(SE2->E2_MOEDA)
					nTaxa	:=	SE5->E5_VALOR/SE5->E5_VLMOED2

					nTotAbat 	:= SumAbatPag(SE2->E2_PREFIXO,SE2->E2_NUM,SE2->E2_PARCELA, 	SE2->E2_FORNECE,SE2->E2_MOEDA,"V",SE5->E5_DATA,SE2->E2_LOJA)
					If cPaisLoc == "BRA"
						If SE2->E2_MOEDA <= 1
							nValPag		:= xMoeda(SE5->E5_VALOR,1,SE2->E2_MOEDA,SE5->E5_DATA)
						Else
							nValPag		:= Round(NoRound(xMoeda(SE5->E5_VALOR,1,SE2->E2_MOEDA,SE5->E5_DATA,3),3),2)
						EndIf
						nJuros		:= xMoeda(SE5->E5_VLJUROS,1,SE2->E2_MOEDA,SE5->E5_DATA)
						nDescont		:= xMoeda(SE5->E5_VLDESCO,1,SE2->E2_MOEDA,SE5->E5_DATA)
						nMulta		:= xMoeda(SE5->E5_VLMULTA,1,SE2->E2_MOEDA,SE5->E5_DATA)
					Else
						nValPag		:= SE5->E5_VLMOED2
						nJuros		:= xMoeda(SE5->E5_VLJUROS,1,SE2->E2_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nDescont		:= xMoeda(SE5->E5_VLDESCO,1,SE2->E2_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nMulta		:= xMoeda(SE5->E5_VLMULTA,1,SE2->E2_MOEDA,SE5->E5_DATA,nDecs+1,Nil,nTaxa)
						nValDifC		:= SE5->E5_VLCORRE
					Endif
					nValPag		+= nDescont - nJuros - nMulta
					cFornece		:= SE2->E2_FORNECE
					cLoja			:= SE2->E2_LOJA
					StrLctPad   := SE2->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO)
					nRec450 := SE2->(Recno())
					//����������������������������������������������������������������Ŀ
					//� Caso esteja utilizando outras moedas, faz o acerto do valor do �
					//� titulo se a diferen�a for de 0.01				           	   �
					//������������������������������������������������������������������
					If SE2->E2_MOEDA > 1 .And. Round(((nValPag+nTotAbat+SE2->E2_SALDO)-SE2->E2_VALOR),2) = 0.01
						nValPag -= 0.01
					EndIf

					lBxUnica := IIf(STR(nValPag+nTotAbat+SE2->E2_SALDO,17,2) == STR(SE2->E2_VALOR,17,2),.T.,.F.)
					If lBxUnica .and. nTotAbat > 0
						nValPag += nTotAbat
						*������������������������������������������������������������������Ŀ
						*�Verifica se h� abatimentos para voltar a carteira					  �
						*��������������������������������������������������������������������
						SE2->(dbSetOrder(1))
						If SE2->(dbSeek(SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA))
							cTitAnt := (SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA)
							While !SE2->(Eof()) .and. cTitAnt == (SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA)
								IF !SE2->E2_TIPO $ MVABATIM
									SE2->(dbSkip())
									Loop
								EndIF
								IF SE2->E2_FORNECE+SE2->E2_LOJA != cFornece+cLoja
									SE2->(dbSkip())
									Loop
								EndIF
								//�����������������������������������������Ŀ
								//�Volta titulo para carteira 				  �
								//�������������������������������������������
								Reclock("SE2")
								SE2->E2_BAIXA	:= Ctod(" /  /  ")
								SE2->E2_SALDO	:= E2_VALOR
								SE2->E2_VALLIQ	:= 0
								SE2->E2_IDENTEE := ""
								SE2->E2_DESCONT := 0
								SE2->E2_JUROS   := 0
								SE2->E2_MULTA   := 0
								SE2->E2_CORREC  := 0
								MsUnlock()
								SE2->(dbSkip())
							Enddo
						Endif

					Endif

					SE2->(dbGoto(nRec450))
					RecLock("SE2")
					SE2->E2_SALDO += nValPag
					SE2->E2_IDENTEE := ""
					If cPaisLoc == "CHI" .And. nValDifC <> 0
						E2_CAMBIO	-=	nValDifC
					Endif
					If lBxUnica
						SE2->E2_BAIXA   := Ctod(" /  /  ")
						SE2->E2_VALLIQ  := 0
						SE2->E2_SDACRES := SE2->E2_ACRESC
						SE2->E2_SDDECRE := SE2->E2_DECRESC
						SE2->E2_DESCONT := 0
						SE2->E2_JUROS   := 0
						SE2->E2_MULTA   := 0
						SE2->E2_CORREC  := 0
					Endif
					MsUnLock()
					//Ponto de entrada para gravacoes complementares
					If lF450SE2C
						ExecBlock("F450SE2C",.F.,.F.)
					Endif

					//����������������������������������������������������������Ŀ
					//� Inicializa variaveis para contabilizacao 					 �
					//������������������������������������������������������������
					VALOR := SE5->E5_VALOR
					VLRINSTR := VALOR
					If SE2->E2_MOEDA <= 5 .And. SE2->E2_MOEDA > 1
						cVal := Str(SE2->E2_MOEDA,1)
						If cPaisLoc == "BRA"
							VALOR&cVal := xMoeda(SE5->E5_VALOR,1,SE2->E2_MOEDA,SE5->E5_DATA)
						Else
							VALOR&cVal := SE5->E5_VLMOED2
						Endif
					EndIf

					//����������������������������������������������������������Ŀ
					//� Cancela compensacao						 							 �
					//������������������������������������������������������������
					dbSelectArea("SE5")
					nRecSE5 := Recno()
					SE5->(dbGotop())
					While SE5->(!Eof()) .And. SE5->E5_IDENTEE == cCompCan
						If SE5->(!Eof()) .and. SE5->(E5_FILIAL+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA) == ;
							/*xFilial("SE5")*/SE2->E2_FILIAL+SE2->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA)
							RecLock("SE5")
							If ! lEstorna
								// Replace E5_SITUACA	With "C"
								Aadd(aCancela, SE5->(Recno()))
							Else
								//Replace E5_SITUACA	With "X"
								Aadd(aEstorno, SE5->(Recno()))
							Endif
							MsUnLock()
						Endif


						SE5->(dbSkip())
					Enddo

					SE5->(dbGoto(nRecSE5))
				ENdif
			Endif


			If (SE5->E5_RECPAG == "R" .And. !(SE5->E5_TIPO $ MV_CPNEG+"/"+MVPAGANT) ) .Or.( SE5->E5_RECPAG == "P" .And. (SE5->E5_TIPO $ MV_CRNEG+"/"+MVRECANT))
				//����������������������������������������������������������Ŀ
				//� Posiciona SA1 para contabiliza��o        					 �
				//������������������������������������������������������������
				dbSelectArea("SA1")

				dbSeek(  CNI317VrfFilial( xFilial("SA1"), SE1->E1_FILIAL, cLayOut)  +  SE1->E1_CLIENTE  +  SE1->E1_LOJA)

				IF SE5->E5_RECPAG == "R" .And. SE5->E5_TIPO $ MV_CPNEG+"/"+MVPAGANT  .Or.  SE5->E5_RECPAG == "P" .And. SE5->E5_TIPO $ MV_CRNEG+"/"+MVRECANT
					AtuSalDup("-",nValRec,SE1->E1_MOEDA,SE1->E1_TIPO,,SE1->E1_EMISSAO)
				Else
					AtuSalDup("+",nValRec,SE1->E1_MOEDA,SE1->E1_TIPO,,SE1->E1_EMISSAO)
				EndIf

				dbSelectArea("SE2")
				dbGobottom()
				dbSkip()
			Else

				//����������������������������������������������������������Ŀ
				//� Posiciona SA2 para contabiliza��o        					 �
				//������������������������������������������������������������
				SA2->(dbCLoseArea())//Corre��o referente a travamento ReckLock na SA2
				dbSelectArea("SA2")

				dbSeek(  CNI317VrfFilial ( xFilial("SA2"), SE2->E2_FILIAL, cLayOut)  +  SE2->E2_FORNECE  +  SE2->E2_LOJA)

				RecLock("SA2")
				IF SE5->E5_RECPAG == "R" .And. SE5->E5_TIPO $ MV_CPNEG+"/"+MVPAGANT  .Or.  SE5->E5_RECPAG == "P" .And. SE5->E5_TIPO $ MV_CRNEG+"/"+MVRECANT
					SA2->A2_SALDUP		-= nValPag
					SA2->A2_SALDUPM -= xMoeda(nValPag,1,Val(GetMv("MV_MCUSTO")),SE2->E2_EMISSAO,3)
				Else
					SA2->A2_SALDUP		+= nValPag
					SA2->A2_SALDUPM += xMoeda(nValPag,1,Val(GetMv("MV_MCUSTO")),SE2->E2_EMISSAO,3)
				EndIf
				MsUnLock()

				SA2->(dbCLoseArea())

				dbSelectArea("SE1")
				dbGobottom()
				dbSkip()
			Endif

			//����������������������������������������������������������Ŀ
			//� Contabiliza															 �
			//������������������������������������������������������������
			If !lCabec .and. lPadrao
				nHdlPrv := HeadProva(  cLote,;
				                      "FINA450",;
				                      Substr( cUsuario, 7, 6 ),;
				                      @cArquivo )

				lCabec := .t.
			Endif
			If lCabec .and. lPadrao .and. (VALOR+VALOR2+VALOR3+VALOR4+VALOR5) > 0
				nTotal += DetProva( nHdlPrv,;
				                    cPadrao,;
				                    "FINA450",;
				                    cLote,;
				                    /*nLinha*/,;
				                    /*lExecuta*/,;
				                    /*cCriterio*/,;
				                    /*lRateio*/,;
				                    /*cChaveBusca*/,;
				                    /*aCT5*/,;
				                    .F.,;
				                    @aFlagCTB,;
				                    /*aTabRecOri*/,;
				                    /*aDadosProva*/ )

			Endif


			dbSelectArea("SE5")
			IncProc(STR0005)                        ////'Selecionando Registros'IncProc("Selecionando Registros...")
			SE5-> (dbSkip())

		Enddo

		//��������������������������������������������������������������Ŀ
		//� Recupera a Integridade dos dados									  �
		//����������������������������������������������������������������
		dbSelectArea("SE5")
		RetIndex("SE5")
		Set Filter to
		If !Empty(cIndex)
			Ferase(cIndex+OrdBagExt())
		Endif

		For nx := 1 To Len(aCancela)
			SE5->(dbGoto(aCancela[nx]))
			RecLock("SE5",.F.)
			Replace E5_SITUACA	With "C"
			MsUnLock()
		Next

		For nx :=1 To Len(aEstorno)
			SE5->(dbGoto(aEstorno[nx]))
			RecLock("SE5",.F.)
			Replace E5_SITUACA	With "X"
			MsUnLock()

			If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
				aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
			EndIf

			CniGrvEst(aEstorno[nX], lUsaFlag ) // Gera registro de estorno da compensacao
		Next

		IF lPadrao .and. lCabec .and. nTotal > 0
			RodaProva(  nHdlPrv,;
						nTotal)
			//�����������������������������������������������������Ŀ
			//� Envia para Lancamento Contabil							  �
			//�������������������������������������������������������
			lDigita:=.T.
			lAglut :=IIF(mv_par02==1,.T.,.F.)
			cA100Incl( cArquivo,;
			           nHdlPrv,;
			           3,;
			           cLote,;
			           lDigita,;
			           lAglut,;
			           /*cOnLine*/,;
			           /*dData*/,;
			           /*dReproc*/,;
			           @aFlagCTB,;
			           /*aDadosProva*/,;
			           /*aDiario*/ )
			aFlagCTB := {}  // Limpa o coteudo apos a efetivacao do lancamento

		Endif

		if !Empty( cCompEfetuada )
		  	CNI317ExcTitsComp(cCompEfetuada)
		EndIf

		UnLockByName(cCompCan,.T.,.F.)
		End Transaction


		//�����������������������������������������������������Ŀ
		//�Integracao com o SIGAPCO para lancamento via processo�
		//�PCODetLan("000018","01","FINA450",.T.)               �
		//�������������������������������������������������������
		PCODetLan("000018","01","FINA450",.F.)

	Else
		MsgAlert( STR0006+STR0007 , STR0008 )      ///"Existe outro usu�rio cancelando esta compensa��o." "N�o � permitido o cancelamento da mesma compensa��o por dois usu�rios ao mesmo tempo."     "Aten��o"
	Endif
Endif

//��������������������������������������������������������������Ŀ
//� Recupera a Integridade dos dados									  �
//����������������������������������������������������������������
dbSelectArea("SE5")
RetIndex("SE5")
Set Filter to
If !Empty(cIndex)
	Ferase(cIndex+OrdBagExt())
Endif
//��������������������������������������������������������������Ŀ
//� Estorna Comissao                                             �
//����������������������������������������������������������������
Fa440DeleB(aBaixaSE3,.F.,.F.,"FINA070")

cFilAnt := cFilAtu

//�����������������������������������������������������Ŀ
//�Integracao com o SIGAPCO para lancamento via processo�
//�PcoFinLan("000018")                                  �
//�������������������������������������������������������
PcoFinLan("000018")

SE2->(MSGOTO(nRecPnl))

Return (.T.)

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o	 �ExcTitsComp� Autor � Oswaldo Leite         � Data � 10/08/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri��o � Excluir todos os titulos que participaram do estorno		   ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe	 � ExcTitsComp              								   ���
��������������������������������������������������������������������������Ĵ��
���Parametros� aExclSE1,consiste em array de titulos localizados na SE1	   ���
���Parametros� aExclSE2,consiste em array de titulos localizados na SE2	   ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/

Static Function CNI317ExcTitsComp(cCompEfetuada)

Local nCont
Local aVetor := {}
Local cString, cQuery, cERRO
Local p1, p2, p3
Local cFilBackup
Local aArea := GetArea()
Private cmsg		:= ''
Private lMsHelpAuto := .T.
Private lMsErroAuto := .F.

cQuery    := "SELECT E1_FILIAL, E1_NUM, E1_PREFIXO, E1_PARCELA, E1_TIPO, E1_CLIENTE, E1_LOJA  FROM "
cQuery    +=  RetSqlName("SE1") + " WHERE E1_NUM = '" + cCompEfetuada + "' and E1_PREFIXO = 'DME' AND D_E_L_E_T_ <> '*' "
cQuery    := ChangeQuery(cQuery)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),'TIT_DME',.T.,.T.)
dbSelectArea('TIT_DME')

while TIT_DME->(!Eof())

	aVetor := {	{"E1_FILIAL"      ,TIT_DME->E1_FILIAL          ,Nil},;
	            {"E1_NUM"         ,TIT_DME->E1_NUM             ,Nil},;
	   	        {"E1_PREFIXO"     ,TIT_DME->E1_PREFIXO			,Nil},;
	       	   	{"E1_PARCELA"     ,TIT_DME->E1_PARCELA			,Nil},;
	           	{"E1_TIPO"        ,TIT_DME->E1_TIPO				,Nil},;
	            {"E1_CLIENTE"     ,TIT_DME->E1_CLIENTE			,Nil},;
	            {"E1_LOJA"        ,TIT_DME->E1_LOJA				,Nil}}

	cFilBackup := cFilAnt
	cFilAnt := TIT_DME->E1_FILIAL
	MSExecAuto({|x,y| Fina040(x,y)},aVetor,5)  //5-EXCLUSAO
	cFilAnt := cFilBackup

	IncProc(STR0009) 				//////'Processando Registros'
	dbSelectArea('TIT_DME')
	dbSkip()
End

dbSelectArea('TIT_DME')
TIT_DME->(dbCloseArea())



cmsg		:= ''
lMsHelpAuto := .T.
lMsErroAuto := .F.
cQuery    := "SELECT E2_FILIAL, E2_NUM, E2_PREFIXO, E2_PARCELA, E2_TIPO, E2_FORNECE, E2_LOJA  FROM "
cQuery    += RetSqlName("SE2") + " WHERE E2_NUM = '" + cCompEfetuada + "' and E2_PREFIXO = 'DME'  AND D_E_L_E_T_ <> '*' "
cQuery    := ChangeQuery(cQuery)

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),'TIT_DME',.T.,.T.)
dbSelectArea('TIT_DME')

while TIT_DME->(!Eof())

	aVetor := {	{"E2_FILIAL"      ,TIT_DME->E2_FILIAL          ,Nil},;
	            {"E2_NUM"         ,TIT_DME->E2_NUM             ,Nil},;
    	        {"E2_PREFIXO"     ,TIT_DME->E2_PREFIXO			,Nil},;
        	   	{"E2_PARCELA"     ,TIT_DME->E2_PARCELA			,Nil},;
            	{"E2_TIPO"        ,TIT_DME->E2_TIPO				,Nil},;
	            {"E2_FORNECE"     ,TIT_DME->E2_FORNECE			,Nil},;
	            {"E2_LOJA"        ,TIT_DME->E2_LOJA				,Nil}}

	cFilBackup := cFilAnt
	cFilAnt := TIT_DME->E2_FILIAL
	MSExecAuto({|x, y, z| FINA050(x, y, z)}, aVetor, 5, 5)
	cFilAnt := cFilBackup

	IncProc(STR0009)			//////'Processando Registros')
	dbSelectArea('TIT_DME')
	dbSkip()
End


dbSelectArea('TIT_DME')
TIT_DME->(dbCloseArea())

RestArea(aArea)

Return

/*/
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o	 �CniGrvEst  � Autor � Oswaldo Leite         � Data �  9/08/11 ���
��������������������������������������������������������������������������Ĵ��
���Descri��o � Grava o movimento de estorno da baixa por CEC       		   ���
��������������������������������������������������������������������������Ĵ��
���Sintaxe	 � CniGrvEst(ExpN1)          										   ���
��������������������������������������������������������������������������Ĵ��
���Parametros� ExpN1 = Numero do registro do movimento a ser estornado	   ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
������������������������������������������������������������������������������
/*/
Static Function CNiGrvEst(nRecnoSe5,lUsaFlag)
Local aAreaSe5	:= SE5->(GetArea())
Local nX
Local aCampos	:= {}

// Posiciona no registro a ser estornado
SE5->(MsGoto(nRecnoSe5))
// Armazena os campos na memoria
For nX := 1 To fCount()
	Aadd(aCampos, {SE5->(FieldName(nX)), SE5->(FieldGet(nX))})
Next

// Grava registro de estorno baseado no registro que foi estornado, alterando os campos TIPODOC e RECPAG
Reclock("SE5",.T.)
// Descarrega aCampos no SE5 para que todos os campos do movimento principal sejam replicados ao movimento de estorno
For nX := 1 To fCount()
	If !Empty(aCampos[nX][2])
		SE5->(FieldPut(nX,aCampos[nX][2]))
	Endif
Next
SE5->E5_DATA      := dDatabase
SE5->E5_RECPAG    := Iif(SE5->E5_RECPAG=="P","R",Iif(SE5->E5_RECPAG=="R","P",""))
If !lUsaFlag
	SE5->E5_LA        := "S"
EndIf
SE5->E5_TIPODOC := "ES"
SE5->E5_DTDIGIT := dDataBase
SE5->E5_DTDISPO := dDataBase
SE5->E5_HISTOR	:= STR0010				/////"Estorno de CEC"
SE5->(RestArea(aAreaSe5))
MsUnlock()
Return Nil


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �AdmAbreSM0� Autor � Orizio                � Data � 22/01/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna um array com as informacoes das filias das empresas ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CniAbreSM0()
	Local aArea			:= SM0->( GetArea() )
	Local aAux			:= {}
	Local aRetSM0		:= {}
	Local lFWLoadSM0	:= .T.
	Local lFWCodFilSM0 	:= .T.

	If lFWLoadSM0
		aRetSM0	:= FWLoadSM0()
	Else
		DbSelectArea( "SM0" )
		SM0->( DbGoTop() )
		While SM0->( !Eof() )
			aAux := { 	SM0->M0_CODIGO,;
						IIf( lFWCodFilSM0, FWGETCODFILIAL, SM0->M0_CODFIL ),;
						"",;
						"",;
						"",;
						SM0->M0_NOME,;
						SM0->M0_FILIAL }

			aAdd( aRetSM0, aClone( aAux ) )
			SM0->( DbSkip() )
		End
	EndIf

	RestArea( aArea )
Return aRetSM0

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � CNI317   �Autor  �Oswaldo Leite       � Data �  09/08/11   ���
�������������������������������������������������������������������������͹��
���Desc.     � FUn��o respons�vel por listar tabela SE2 no grid           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function CNI317

Local aPergs := {}
Local nRegSE2 := SE2->(Recno())
Private aRotina := MenuDef()
//��������������������������������������������������������������Ŀ
//� Verifica o n�mero do Lote 											  �
//����������������������������������������������������������������
Private cLote
Private cMarca 		:= GetMark()
Private lInverte
Private cTipos 		:= ""
Private cCadastro 	:= STR0011			/////"Compensa��o Pagar/Receber entre Empresas"
Private cModSpb 	:= "1"
//���������������������������������������������������������������������������Ŀ
//�Usado no Chile, indica se serao compensados titulos de credito ou de debito�
//�����������������������������������������������������������������������������
Private nDebCred	:=	1
Private lMsErroAuto := .f.


CniMotBx("CEC","COMP CARTE","ASSS")

VALOR 		:= 0
VALOR2		:= 0
VALOR3		:= 0
VALOR4		:= 0
VALOR5		:= 0
VLRINSTR    := 0

SetKey (VK_F12,{|a,b| AcessaPerg("AFI450",.T.)})

pergunte("AFI450",.F.)

mBrowse(6,1,22,75,"SE2",,,,,,CNI317Leg())

dbSelectArea("SE5") //TABELA DE MOVTOS BANCARIOS
dbSetOrder(1)	&& devolve ordem principal
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �CniMotBX  �Autor  � Oswaldo Leite      � Data �  09/08/11   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao criar automaticamente o motivo de baixa CEC na      ���
���          � tabela Mot baixas                                          ���
�������������������������������������������������������������������������͹��
���Uso       � CNI317                                                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CniMotBx(cMot,cNomMot, cConfMot)
	Local lMotBxEsp	:= .F.
	Local aMotbx 	:= ReadMotBx(@lMotBxEsp)
	Local nHdlMot	:= 0
	Local I			:= 0
	Local cFile := "SIGAADV.MOT"
	Local nTamLn	:= 19

	If lMotBxEsp
		nTamLn	:= 20
		cConfMot	:= cConfMot + "N"
	EndIf
	If ExistBlock("FILEMOT")
		cFile := ExecBlock("FILEMOT",.F.,.F.,{cFile})
	Endif

	If Ascan(aMotbx, {|x| Substr(x,1,3) == Upper(cMot)}) < 1
		nHdlMot := FOPEN(cFile,FO_READWRITE)
		If nHdlMot <0
			HELP(" ",1,"SIGAADV.MOT")
			Final("SIGAADV.MOT")
		Endif

		nTamArq:=FSEEK(nHdlMot,0,2)	// VerIfica tamanho do arquivo
		FSEEK(nHdlMot,0,0)			// Volta para inicio do arquivo

		For I:= 0 to  nTamArq step nTamLn // Processo para ir para o final do arquivo
			xBuffer:=Space(nTamLn)
			FREAD(nHdlMot,@xBuffer,nTamLn)
	    Next

		fWrite(nHdlMot,cMot+cNomMot+cConfMot+chr(13)+chr(10))
		fClose(nHdlMot)
	EndIf
Return


/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �Cni317Leg �Autor  �Oswaldo Leite         � Data �  09/08/11   ���
���������������������������������������������������������������������������͹��
���Desc.     � Legendas                                                     ���
���          �                                                              ���
���������������������������������������������������������������������������͹��
���Uso       �CNI317                                                        ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/

Function Cni317Leg(nReg)

Local uRetorno	:= .T.
Local aLegen	:= {{"ENABLE",STR0012},{"DISABLE",STR0013},{"BR_AZUL",STR0014}}     /////STR0012"Titulo no compensado"},{"DISABLE",STR0013"Titulo totalmente compensado"},{"BR_AZUL",STR0014"Titulo parcialmente compensado"}}

If Empty(nReg)
	uRetorno := {}
	aAdd(uRetorno, {'E2_SALDO =  E2_VALOR .AND. E2_ACRESC = E2_SDACRES', aLegen[1][1]}) // Titulo nao Compensado
	aAdd(uRetorno, {'E2_SALDO =  0'									   , aLegen[2][1]}) // Titulo Compensado Totalmente
	aAdd(uRetorno, {'E2_SALDO <> 0'									   , aLegen[3][1]})	 // Titulo Compensado Parcialmente
Else
	BrwLegenda(cCadastro,STR0015,aLegen)                   ////"Legenda"
Endif

Return(uRetorno)



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � CNI317   �Autor  �Microsiga           � Data �  06/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     � Compensa��o entre carteiras simultanea em duas empresas    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function CNI317PR

Local nOpca 			:= 0
Local aSays				:={}
Local aButtons			:={}

Private  cEmpPri		:= " "

Private  cCliePri 		:= " "
Private  cLojCliPri		:= " "
Private  cFornPri		:= " "
Private  cLojForPri 	:= " "

Private  cEmpSec 		:= " "

Private  cClieSec 		:= " "
Private  cLojCliSec		:= " "
Private  cFornSec		:= " "
Private  cLojForSec		:= " "

Private  dDataIni		:= " "
Private  dDataFim		:= " "

Private  nMoeda			:= 1

Private	aRecPri			:= {}
Private	aPagPRi			:= {}
Private	aRecSec			:= {}
Private	aPagSec			:= {}
Private	nTotRecPri		:= 0
Private	nTotPagSec		:= 0
Private	nTotRecSec		:= 0
Private	nTotPagPri		:= 0
Private cMarca 			:= GetMark()
Private aTxMoedas	:=	{}


If !Pergunte("CNI317",.T.)
	Return
Endif

cEmpPri		:= mv_par01

cCliePri 		:= mv_par02
cLojCliPri		:= mv_par03
cFornPri		:= mv_par04
cLojForPri 	:= mv_par05

cEmpSec 		:= mv_par06

cClieSec 		:= mv_par07
cLojCliSec		:= mv_par08
cFornSec		:= mv_par09
cLojForSec		:= mv_par10

dDataIni		:= mv_par11
dDataFim		:= mv_par12

nMoeda			:= mv_par13

AADD(aSays,STR0016)						///////"Este programa tem por objetivo efetuar uma compensa��o entre  carteiras")
AADD(aSays,STR0017)						///////"simultaneamente em duas Empresas/Filiais e gerar uma fatura dos titulos")
AADD(aSays,STR0018)						///////"restantes da compensa��o" )

AADD(aButtons, { 5,.T.,{|| Pergunte("CNI317",.T.) } } )


AADD(aButtons, { 1,.T.,{|o| nOpca:= 1,o:oWnd:End()}} )
AADD(aButtons, { 2,.T.,{|o| o:oWnd:End() }} )
FormBatch( STR0019, aSays, aButtons )                     ////"Compensa��o entre Carteiras e Empresas/Filiais"

If nOpcA == 1
	Processa({|| u_C01Proc() })  // Chamada da funcao de processamento
Endif

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CNI317    �Autor  �Microsiga           � Data �  06/10/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �Processamento                                               ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function C01Proc()

Local cQuery 	:= ""
Local cSepNeg   := If("|"$MV_CPNEG,"|",",")
Local cSepProv  := If("|"$MVPROVIS,"|",",")
Local cSepRec   := If("|"$MVPAGANT,"|",",")
Local cFilOld	:= cFilAnt
Local na, ni, nx
Local cNumComp
Local lPadrao 	:= .F.
Local cPadrao 	:= "594"
Local lCabec 	:= .F.
Local aFlagCTB 	:= {}
Local lUsaFlag	:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)
Local aSE5Recs	:=	{}
Local lProcessou:= .T.
Local nValDif	:= 0
lPadrao:=VerPadrao(cPadrao)

cQuery := "SELECT SE1.R_E_C_N_O_ AS CRECNO, "
cQuery += "'SE1' AS ALIAS, "
cQuery += "E1_OK AS OK, "
cQuery += "E1_FILIAL AS FILIAL, "
cQuery += "E1_PREFIXO AS PREFIXO, "
cQuery += "E1_NUM AS NUM, "
cQuery += "E1_PARCELA AS PARCELA, "
cQuery += "E1_TIPO AS TIPO, "
cQuery += "E1_EMISSAO AS EMISSAO, "
cQuery += "E1_VENCREA AS VENCTO, "
cQuery += "E1_HIST AS HISTOR, "
cQuery += "E1_SALDO AS VALOR "
cQuery += "FROM "+RetSqlName("SE1")+" SE1 "
cQuery += "WHERE  E1_CLIENTE = '"+cCliePri+"'  "
cQuery += "AND E1_LOJA = '"+cLojCliPri+"' "
cQuery += "AND E1_FILIAL = '"+cEmpPri+"' "
cQuery += "AND E1_VENCREA >= '"+DtoS(dDataIni)+"' "
cQuery += "AND E1_VENCREA <= '"+DtoS(dDataFim)+"' "
cQuery += "AND E1_SALDO > 0 "
cQuery += "AND E1_MOEDA = "+AllTrim(Str(nMoeda,1))+" "
cQuery += "AND E1_EMISSAO <='" + DTOS(dDataBase) + "' "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVPAGANT,cSepRec)  + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MV_CPNEG,cSepNeg)  + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVABATIM,"|") + " "
cQuery += "AND E1_SITUACA IN ('0','F','G') "
cQuery += " AND D_E_L_E_T_ <> '*' "
cQuery += "UNION "
cQuery += "SELECT SE2.R_E_C_N_O_ AS CRECNO, "
cQuery += "'SE2' AS ALIAS, "
cQuery += "E2_OK AS OK, "
cQuery += "E2_FILIAL AS FILIAL, "
cQuery += "E2_PREFIXO AS PREFIXO, "
cQuery += "E2_NUM AS NUM, "
cQuery += "E2_PARCELA AS PARCELA, "
cQuery += "E2_TIPO AS TIPO, "
cQuery += "E2_EMISSAO AS EMISSAO, "
cQuery += "E2_VENCREA AS VENCTO, "
cQuery += "E2_HIST AS HISTOR, "
cQuery += "E2_SALDO AS VALOR  "
cQuery += "FROM "+RetSqlName("SE2")+" SE2 "
cQuery += "WHERE E2_FORNECE = '"+cFornPri+"'  "
cQuery += "AND E2_LOJA = '"+cLojForPri+"' "
cQuery += "AND E2_FILIAL = '"+cEmpPri+"' "
cQuery += "AND E2_VENCREA >= '"+DtoS(dDataIni)+"' "
cQuery += "AND E2_VENCREA <= '"+DtoS(dDataFim)+"' "
If GETMV("MV_CTLIPAG")
	cQuery += "AND E2_DATALIB <> '' "
EndIf
cQuery += "AND E2_SALDO > 0 "
cQuery += "AND E2_MOEDA = '"+AllTrim(Str(nMoeda,1))+"' "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVPAGANT,cSepRec)  + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MV_CPNEG,cSepNeg)  + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVABATIM,"|") + " "
cQuery += "AND E2_EMIS1 <='" + DTOS(dDataBase) + "' "
cQuery += " AND D_E_L_E_T_ <> '*' "
cQuery += "UNION "
cQuery += "SELECT SE1.R_E_C_N_O_ AS CRECNO, "
cQuery += "'SE1' AS ALIAS, "
cQuery += "E1_OK AS OK, "
cQuery += "E1_FILIAL AS FILIAL, "
cQuery += "E1_PREFIXO AS PREFIXO, "
cQuery += "E1_NUM AS NUM, "
cQuery += "E1_PARCELA AS PARCELA, "
cQuery += "E1_TIPO AS TIPO, "
cQuery += "E1_EMISSAO AS EMISSAO, "
cQuery += "E1_VENCREA AS VENCTO, "
cQuery += "E1_HIST AS HISTOR, "
cQuery += "E1_SALDO AS VALOR "
cQuery += "FROM "+RetSqlName("SE1")+" SE1 "
cQuery += "WHERE E1_CLIENTE = '"+cClieSec+"'  "
cQuery += "AND E1_LOJA = '"+cLojCliSec+"' "
cQuery += "AND E1_FILIAL = '"+cEmpSec+"' "
cQuery += "AND E1_VENCREA >= '"+DtoS(dDataIni)+"' "
cQuery += "AND E1_VENCREA <= '"+DtoS(dDataFim)+"' "
cQuery += "AND E1_SALDO > 0 "
cQuery += "AND E1_MOEDA = '"+AllTrim(Str(nMoeda,1))+"' "
cQuery += "AND E1_EMISSAO <='" + DTOS(dDataBase) + "' "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVPAGANT,cSepRec)  + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MV_CPNEG,cSepNeg)  + " "
cQuery += "AND E1_TIPO NOT IN " + FormatIn(MVABATIM,"|") + " "
cQuery += "AND E1_SITUACA IN ('0','F','G') "
cQuery += " AND D_E_L_E_T_ <> '*' "
cQuery += "UNION "
cQuery += "SELECT SE2.R_E_C_N_O_ AS CRECNO, "
cQuery += "'SE2' AS ALIAS, "
cQuery += "E2_OK AS OK, "
cQuery += "E2_FILIAL AS FILIAL, "
cQuery += "E2_PREFIXO AS PREFIXO, "
cQuery += "E2_NUM AS NUM, "
cQuery += "E2_PARCELA AS PARCELA, "
cQuery += "E2_TIPO AS TIPO, "
cQuery += "E2_EMISSAO AS EMISSAO, "
cQuery += "E2_VENCREA AS VENCTO, "
cQuery += "E2_HIST AS HISTOR, "
cQuery += "E2_SALDO AS VALOR "
cQuery += "FROM "+RetSqlName("SE2")+ " SE2 "
cQuery += "WHERE E2_FORNECE = '"+cFornSec+"' "
cQuery += "AND E2_LOJA = '"+cLojForSec+"' "
cQuery += "AND E2_FILIAL = '"+cEmpSec+"' "
cQuery += "AND E2_VENCREA >= '"+DtoS(dDataIni)+"' "
cQuery += "AND E2_VENCREA <= '"+DtoS(dDataFim)+"' "
If GETMV("MV_CTLIPAG")
	cQuery += "AND E2_DATALIB <> '' "
EndIf
cQuery += "AND E2_SALDO > 0 "
cQuery += "AND E2_MOEDA = '"+AllTrim(Str(nMoeda,1))+"' "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVPAGANT,cSepRec)  + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MV_CPNEG,cSepNeg)  + " "
cQuery += "AND E2_TIPO NOT IN " + FormatIn(MVABATIM,"|") + " "
cQuery += "AND E2_EMIS1 <='" + DTOS(dDataBase) + "' "
cQuery += " AND D_E_L_E_T_ <> '*' "
cQuery += "ORDER BY FILIAL,NUM "

cQuery := ChangeQuery( cQuery )
dbUseArea( .t., "TOPCONN", Tcgenqry( , , cQuery ), "TMPCMP", .F., .T. )
TMPCMP->(dbGoTop())

ProcRegua(5) //COMPENSA��O


While !eof()
	If TMPCMP->FILIAL == cEmpPri
		If TMPCMP->ALIAS == "SE1"
			aAdd(aRecPri,{cMarca, TMPCMP->CRECNO, TMPCMP->FILIAL, TMPCMP->PREFIXO, TMPCMP->NUM, TMPCMP->PARCELA, TMPCMP->TIPO, TMPCMP->VALOR})
			nTotRecPri += TMPCMP->VALOR
		Else
			aADD(aPagPRi,{cMarca, TMPCMP->CRECNO, TMPCMP->FILIAL, TMPCMP->PREFIXO, TMPCMP->NUM, TMPCMP->PARCELA, TMPCMP->TIPO, TMPCMP->VALOR})
			nTotPagPri += TMPCMP->VALOR
		EndIf
	Else
		If TMPCMP->ALIAS  == "SE1"
			aAdd(aRecSec,{cMarca, TMPCMP->CRECNO, TMPCMP->FILIAL, TMPCMP->PREFIXO, TMPCMP->NUM, TMPCMP->PARCELA, TMPCMP->TIPO, TMPCMP->VALOR})
			nTotRecSec += TMPCMP->VALOR
		Else
			aADD(aPagSec,{cMarca, TMPCMP->CRECNO, TMPCMP->FILIAL, TMPCMP->PREFIXO, TMPCMP->NUM, TMPCMP->PARCELA, TMPCMP->TIPO, TMPCMP->VALOR})
			nTotPagSec += TMPCMP->VALOR
		EndIf
	EndIf
	dbSkip()
EndDo
IncProc()//COMPENSA��O

If nTotRecPri <> nTotPagSec .or. nTotRecSec <> nTotPagPri
	MsgAlert(STR0020, STR0008)                          ////"Totais n�o conferem entre as empresas/filiais! "      "Aten��o"
EndIf

Aadd(aTxMoedas,{"",1,PesqPict("SM2","M2_MOEDA1")})
For nA	:=	2	To MoedFin()
	cMoedaTx	:=	Str(nA,IIf(nA <= 9,1,2))
	If !Empty(GetMv("MV_MOEDA"+cMoedaTx))
		Aadd(aTxMoedas,{GetMv("MV_MOEDA"+cMoedaTx),RecMoeda(dDataBase,nA),PesqPict("SM2","M2_MOEDA"+cMoedaTx) })
	Else
		Exit
	Endif
Next

cNumComp	:= Soma1(GetMv("MV_NUMCOMP"),6)

//Seta empresa e filial
cFilAnt := 	cEmpPri		//IIf( lFWCodFil, FWGETCODFILIAL, SM0->M0_CODFIL )


Begin Transaction

For ni:=1 to Len(aRecPri)
	lBaixou := .F.

	//�����������������������������������Ŀ
	//� Registro do Contas a Receber 	  �
	//�������������������������������������
	If aRecPri[ni][1] == cMarca
		nValRec	:= 0

		//����������������������������������������������������������Ŀ
		//� Procura registro no SE1											 �
		//������������������������������������������������������������
		dbSelectArea("SE1")
		dbSetOrder(1)
		dbGoTo(aRecPri[ni][2])
		//����������������������������������������������������������Ŀ
		//� Inicializa variaveis para baixa 								 �
		//������������������������������������������������������������
		dBaixa		:= dDataBase
		cBanco		:= cAgencia := cConta := cBenef :=	cCheque	:= " "
		cPortado 	:= cNumBor		:= " "
		cLoteFin 	:= " "
		nDescont 	:= nAbatim := nJuros := nMulta := nCm := 0
		cMotBX		:= "CEC"             // Compensacao Entre Carteiras
		lDesconto	:= .F.
		StrLctPad   := SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
		//����������������������������������������������������������Ŀ
		//� Calcula valor a ser baixado										 �
		//������������������������������������������������������������

		dbSelectArea("SE1")

		//TRB->CHAVE 	= SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO
		nTotAbat 	:= SumAbatRec(SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,nMoeda,"S",dDataBase)

		SE1->(dbGoto(aRecPri[ni][2]))

		nValRec		:= aRecPri[ni][8]/*TMPCMP->VALOR*/ - nTotAbat
		nValEstrang := nValRec
		nAcresc		:= SE1->E1_SDACRES
		nDecresc	:= SE1->E1_SDDECRE
		nDescont	:= xMoeda(SE1->E1_DESCONT,nMoeda,1,dDataBase)
		nJuros		:= xMoeda(SE1->E1_JUROS,nMoeda,1,dDataBase)
		nMulta		:= xMoeda(SE1->E1_MULTA,nMoeda,1,dDataBase)
		nTxMoeda	:= SE1->E1_TXMOEDA
		// Converte para poder baixar
		If cPaisLoc == "BRA"
		   nValRec		:= xMoeda(nValRec,nMoeda,1,dDataBase)
		   FA070CORR(nValEstrang,0)
		Else
		   nValDia		:= Round(xMoeda(nValRec,nMoeda,1,dDataBase,nDecs1+1,nTxMoeda),nDecs1)
		   nValRec		:= Round(xMoeda(nValRec,nMoeda,1,dDataBase,nDecs1+1,aTxMoedas[nMoeda][2]),nDecs1)
		   nDifCambio	:= nValRec - nValDia
		   nCM			:=	nDifCambio
		EndIf
		SE1->(dbGoto(aRecPri[ni][2]))
		cHist070 	:= STR0021				//////"Valor recebido por compensacao" //"Valor recebido por compensacao"
		//����������������������������������������������������������Ŀ
		//� Efetua a baixa														 �
		//������������������������������������������������������������
		If nValRec > 0

			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE1 							 �
			//������������������������������������������������������������
			RecLock("SE1")
				Replace E1_IDENTEE	With cNumComp
			MsUnLock()

			lBaixou		:= FA070Grv(lPadrao,lDesconto,.F.,;
								Nil,.F.,dDataBase,.F.,Nil)		//Nil=Arquivo Cnab
			If lBaixou
				nValDif := nValDif + nValRec
			Else
				Help( ,,'HELP', STR0022, STR0023+cEmpPri, 1, 0)	////STR0022"ERRO", STR0023"Problemas na Baixa por Compensa��o dos titulos da filial "+cEmpPri, 1, 0)
				DisarmTransaction()
				Return
			EndIf
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE5 						 �
			//������������������������������������������������������������
			RecLock("SE5")
				Replace E5_IDENTEE	With cNumComp
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
				Else
					Replace E5_LA			With "S"
				EndIf
			MsUnLock()

			AAdd(aSE5Recs,{"R",SE5->(Recno())})

			//Gravar o numero da compensacao nos titulos de juros, multa e desconto
			//para permitir que os mesmos sejam cancelados quando do cancelamento da
			//compensacao
			dbSelectArea("SE5")
			nRecAtu := SE5->(Recno())
			nOrdAtu := SE5->(IndexOrd())
			dbSetOrder(2)
			cChaveSe5 := SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(dDatabase)+E5_CLIFOR+E5_LOJA+E5_SEQ)
			aTipodoc := {"DC","MT","JR","CM"}
			For nX := 1 to Len(aTipodoc)
   				If dbSeek(xFilial("SE5")+aTipoDoc[nX]+cChaveSE5)
					RecLock("SE5")
						Replace E5_IDENTEE	With cNumComp

						If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
							aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
						Else
							Replace E5_LA			With "S"
						EndIf
					MsUnLock()
               	Endif
      		Next

			SE5->(dbSetOrder(nOrdAtu))
			SE5->(dbGoto(nRecAtu))

			//����������������������������������������������������������Ŀ
			//� Inicializa variaveis para contabilizacao 					 �
			//������������������������������������������������������������
			VALOR := nValRec
			VLRINSTR := VALOR
			If nMoeda <= 5 .And. nMoeda > 1
				cVal := Str(nMoeda,1)
				VALOR&cVal := nValEstrang
			EndIf

			//����������������������������������������������������������Ŀ
			//� Posiciona SA1 para contabiliza��o        					 �
			//������������������������������������������������������������
			dbSelectArea("SA1")
			dbSeek(xFilial("SA1")+SE1->E1_CLIENTE+SE1->E1_LOJA)

			dbSelectArea("SE2")
			dbGobottom()
			dbSkip()

			//����������������������������������������������������������Ŀ
			//� Contabiliza															 �
			//������������������������������������������������������������
			If !lCabec .and. lPadrao
				nHdlPrv := HeadProva( cLote,;
			                      "FINA450",;
			                      Substr( cUsuario, 7, 6 ),;
			                      @cArquivo )

				lCabec := .t.
			End
			If lCabec .and. lPadrao .and. lBaixou .and. (VALOR+VALOR2+VALOR3+VALOR4+VALOR5) > 0
				nTotal += DetProva( nHdlPrv,;
				                    cPadrao,;
				                    "FINA450",;
				                    cLote,;
				                    /*nLinha*/,;
				                    /*lExecuta*/,;
				                    /*cCriterio*/,;
				                    /*lRateio*/,;
				                    /*cChaveBusca*/,;
				                    /*aCT5*/,;
				                    /*lPosiciona*/,;
				                    @aFlagCTB,;
				                    /*aTabRecOri*/,;
				                    /*aDadosProva*/ )

			End
			//�����������������������������������������������������Ŀ
			//�Integracao com o SIGAPCO para lancamento via processo�
			//�PCODetLan("000018","01","FINA450",.T.)               �
			//�������������������������������������������������������
			PcoDetLan("000018","01","FINA450",.F.)
		End
	EndIf
Next

IncProc()//COMPENSA��O

For ni:=1 to Len(aPagPri)
	lBaixou := .F.

	//�����������������������������������Ŀ
	//� Registro do Contas a Receber 	  �
	//�������������������������������������
	If aPagPri[ni][1] == cMarca
		nValPgto := 0

		//����������������������������������������������������������Ŀ
		//� Procura registro no SE2											 �
		//������������������������������������������������������������
		dbSelectArea("SE2")
		dbSetOrder(1)
		dbGoTo(aPagPri[ni][2])

		//����������������������������������������������������������Ŀ
		//� Inicializa variaveis para baixa 								 �
		//������������������������������������������������������������
		dBaixa		:= dDataBase
		cBanco		:= cAgencia := cConta := cBenef := cLoteFin := " "
		cCheque		:= cPortado := cNumBor := " "
		nDespes		:= nDescont := nAbatim := nJuros := nMulta := 0
		nValCc		:= nCm := 0
		lDesconto	:= .F.
		cMotBx		:= "CEC"
		StrLctPad   := SE2->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA)
		//����������������������������������������������������������Ŀ
		//� Calcula Abatimentos 												 �
		//������������������������������������������������������������
		dbSelectArea("SE2")

		//TRB->CHAVE 	= SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+;
		//						SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA
		//SumAbatPag(cPrefixo,cNumero,cParcela,cFornece,nMoeda,cCpo,dData,cLoja,cTipoData,dDataIni,dDataFim)
		nTotAbat 	:= SumAbatPag(SE2->E2_PREFIXO,SE2->E2_NUM,SE2->E2_PARCELA,cFornPri,nMoeda,"S",dDataBase,SE2->E2_LOJA)

		cHist070 	:= STR0024				//////"Valor Pago por compensacao" //"Valor Pago por compensacao"
		SE2->(dbGoto(aPagPri[ni][2]))

		//����������������������������������������������������������Ŀ
		//� Calcula valor a ser baixado								 �
		//������������������������������������������������������������

		nValPgto 	:= aPagPri[ni][8] - nTotAbat
		nValEstrang := nValPgto
		nAcresc		:= SE2->E2_SDACRES
		nDecresc	:= SE2->E2_SDDECRE
		nDescont	:= xMoeda(SE2->E2_DESCONT,nMoeda,1,dDataBase)
		nJuros		:= xMoeda(SE2->E2_JUROS,nMoeda,1,dDataBase)
		nMulta		:= xMoeda(SE2->E2_MULTA,nMoeda,1,dDataBase)
		nTxMoeda	:= SE2->E2_TXMOEDA

		// Converte para poder baixar
		If cPaisLoc == "BRA"
		   nValPgto 	:= xMoeda(nValPgto,nMoeda,1,dDataBase)
		   FA080CORR(nValEstrang,0)
		Else
		   nValDia		:= Round(xMoeda(nValPgto,nMoeda,1,dDataBase,nDecs1+1,nTxMoeda),nDecs1)
           nValPgto 	:= Round(xMoeda(nValPgto,nMoeda,1,dDataBase,nDecs1+1,aTxMoedas[nMoeda][2]),nDecs1)
		   nDifCambio   := nValPgto - nValDia
		EndIf

		//����������������������������������������������������������Ŀ
		//� Efetua a baixa														 �
		//������������������������������������������������������������
		If nValPgto > 0
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE2 							 �
			//������������������������������������������������������������
			RecLock("SE2")
				Replace E2_IDENTEE	With cNumComp
			MsUnLock()

			// analisar a gravacao do SE2 - filial destino
			cLanca		:= "S"
			lBaixou		:= fA080Grv(lPadrao,.F.,.F.,cLanca,,IIf(cPaisLoc=="BRA",Nil,aTxMoedas[nMoeda][2]))
			If lBaixou
				nValDif := nValDif - nValPgto
			Else
				Help( ,,'HELP', STR0022, STR0023+cEmpPri, 1, 0)	               ////"ERRO"    "Problemas na Baixa por Compensa��o dos titulos da filial "
				DisarmTransaction()
				Return
			EndIf
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE5 							 �
			//������������������������������������������������������������
			RecLock("SE5")
				Replace E5_IDENTEE	With cNumComp

				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
				Else
					Replace E5_LA			With "S"
				EndIf
			MsUnLock()

			AAdd(aSE5Recs,{"P",SE5->(Recno())})

			//Gravar o numero da compensacao nos titulos de juros, multa e desconto
			//para permitir que os mesmos sejam cancelados quando do cancelamento da
			//compensacao
			dbSelectArea("SE5")
			nRecAtu := SE5->(Recno())
			nOrdAtu := SE5->(IndexOrd())
			dbSetOrder(2)
			cChaveSe5 := SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(dDatabase)+E5_CLIFOR+E5_LOJA+E5_SEQ)
			aTipodoc := {"DC","MT","JR","CM"}
			For nX := 1 to Len(aTipodoc)
				If dbSeek(xFilial("SE5")+aTipoDoc[nX]+cChaveSE5)
					RecLock("SE5")
						Replace E5_IDENTEE	With cNumComp
						If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
							aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
						Else
							Replace E5_LA			With "S"
						EndIf
					MsUnLock()
				Endif
			Next

			SE5->(dbSetOrder(nOrdAtu))
			SE5->(dbGoto(nRecAtu))

			//����������������������������������������������������������Ŀ
			//� Inicializa variaveis para contabilizacao 					 �
			//������������������������������������������������������������
			VALOR := nValPgto
			VLRINSTR := VALOR
			If nMoeda <= 5 .And. nMoeda > 1
				cVal := Str(nMoeda,1)
				VALOR&cVal := nValEstrang
			EndIf
			//����������������������������������������������������������Ŀ
			//� Posiciona SA2 para contabiliza��o        					 �
			//������������������������������������������������������������

			dbSelectArea("SA2")
			dbSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA)

			dbSelectArea("SE1")
			dbGobottom()
			dbSkip()

			//����������������������������������������������������������Ŀ
			//� Contabiliza															 �
			//������������������������������������������������������������
			If !lCabec .and. lPadrao
				nHdlPrv := HeadProva( cLote,;
			                      "FINA450",;
			                      Substr( cUsuario, 7, 6 ),;
			                      @cArquivo )

				lCabec := .t.
			End
			If lCabec .and. lPadrao .and. lBaixou .and. (VALOR+VALOR2+VALOR3+VALOR4+VALOR5) > 0
				nTotal += DetProva( nHdlPrv,;
				                    cPadrao,;
				                    "FINA450",;
				                    cLote,;
				                    /*nLinha*/,;
				                    /*lExecuta*/,;
				                    /*cCriterio*/,;
				                    /*lRateio*/,;
				                    /*cChaveBusca*/,;
				                    /*aCT5*/,;
				                    /*lPosiciona*/,;
				                    @aFlagCTB,;
				                    /*aTabRecOri*/,;
				                    /*aDadosProva*/ )

			End
			//�����������������������������������������������������Ŀ
			//�Integracao com o SIGAPCO para lancamento via processo�
			//�PCODetLan("000018","01","FINA450",.T.)               �
			//�������������������������������������������������������
			PcoDetLan("000018","01","FINA450",.F.)
		End
	EndIf
Next

IncProc()//COMPENSA��O

If nValDif == (nTotRecPri - nTotPagPri)
	IF !IncTit(nValDif,1,cNumComp)
		Help( ,,'HELP', STR0022, STR0026+cEmpPri, 1, 0)	  ///STR0022"ERRO", STR0026"Problemas na Inclusao Titulo Compensa��o dos titulos da filial "+cEmpPri, 1, 0)
		DisarmTransaction()
		Return
	EndIf
// Retirada valida��o a pedido dos consultores do DRG SP no Cliente CNI
//Else
//	Help( ,,'HELP', "ERRO", "Problemas com a compensa��o dos titulos da filial "+cEmpPri, 1, 0)
//	DisarmTransaction()
//	Return
EndIf

If	lProcessou

	IF lPadrao .and. lCabec
		RodaProva(  nHdlPrv,;
					nTotal)
		//�����������������������������������������������������Ŀ
		//� Envia para Lancamento Contabil							  �
		//�������������������������������������������������������
		lDigita:=IIF(mv_par01==1,.T.,.F.)
		lAglut :=IIF(mv_par02==1,.T.,.F.)
		cA100Incl( cArquivo,;
		           nHdlPrv,;
		           3,;
		           cLote,;
		           lDigita,;
		           lAglut,;
		           /*cOnLine*/,;
		           /*dData*/,;
		           /*dReproc*/,;
		           @aFlagCTB,;
		           /*aDadosProva*/,;
		           /*aDiario*/ )
		aFlagCTB := {}  // Limpa o coteudo apos a efetivacao do lancamento
	Endif

Endif

// Segunda empresa /filial

//Seta empresa e filial
cFilAnt := 	cEmpSec		//IIf( lFWCodFil, FWGETCODFILIAL, SM0->M0_CODFIL )
nValDif := 0

For ni:=1 to Len(aRecSec)
	lBaixou := .F.

	//�����������������������������������Ŀ
	//� Registro do Contas a Receber 	  �
	//�������������������������������������
	If aRecSec[ni][1] == cMarca
		nValRec	:= 0

		//����������������������������������������������������������Ŀ
		//� Procura registro no SE1											 �
		//������������������������������������������������������������
		dbSelectArea("SE1")
		dbSetOrder(1)
		dbGoTo(aRecSec[ni][2])
		//����������������������������������������������������������Ŀ
		//� Inicializa variaveis para baixa 								 �
		//������������������������������������������������������������
		dBaixa		:= dDataBase
		cBanco		:= cAgencia := cConta := cBenef :=	cCheque	:= " "
		cPortado 	:= cNumBor		:= " "
		cLoteFin 	:= " "
		nDescont 	:= nAbatim := nJuros := nMulta := nCm := 0
		cMotBX		:= "CEC"             // Compensacao Entre Carteiras
		lDesconto	:= .F.
		StrLctPad   := SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)
		//����������������������������������������������������������Ŀ
		//� Calcula valor a ser baixado										 �
		//������������������������������������������������������������

		dbSelectArea("SE1")

		//TRB->CHAVE 	= SE1->E1_FILIAL+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO
		nTotAbat 	:= SumAbatRec(SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,nMoeda,"S",dDataBase)

		SE1->(dbGoto(aRecSec[ni][2]))

		nValRec		:= aRecSec[ni][8] - nTotAbat
		nValEstrang := nValRec
		nAcresc		:= SE1->E1_SDACRES
		nDecresc	:= SE1->E1_SDDECRE
		nDescont	:= xMoeda(SE1->E1_DESCONT,nMoeda,1,dDataBase)
		nJuros		:= xMoeda(SE1->E1_JUROS,nMoeda,1,dDataBase)
		nMulta		:= xMoeda(SE1->E1_MULTA,nMoeda,1,dDataBase)
		nTxMoeda	:= SE1->E1_TXMOEDA
		// Converte para poder baixar
		If cPaisLoc == "BRA"
		   nValRec		:= xMoeda(nValRec,nMoeda,1,dDataBase)
		   FA070CORR(nValEstrang,0)
		Else
		   nValDia		:= Round(xMoeda(nValRec,nMoeda,1,dDataBase,nDecs1+1,nTxMoeda),nDecs1)
		   nValRec		:= Round(xMoeda(nValRec,nMoeda,1,dDataBase,nDecs1+1,aTxMoedas[nMoeda][2]),nDecs1)
		   nDifCambio	:= nValRec - nValDia
		   nCM			:=	nDifCambio
		EndIf
		SE1->(dbGoto(aRecSec[ni][2]))
		cHist070 	:= STR0021						/////////"Valor recebido por compensacao" //"Valor recebido por compensacao"
		//����������������������������������������������������������Ŀ
		//� Efetua a baixa														 �
		//������������������������������������������������������������
		If nValRec > 0

			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE1 							 �
			//������������������������������������������������������������
			RecLock("SE1")
				Replace E1_IDENTEE	With cNumComp
			MsUnLock()

			lBaixou		:= FA070Grv(lPadrao,lDesconto,.F.,;
								Nil,.F.,dDataBase,.F.,Nil)		//Nil=Arquivo Cnab
			If lBaixou
				nValDif := nValDif + nValRec
			Else
				Help( ,,'HELP', STR0022, STR0023+cEmpSec, 1, 0)	    /////"ERRO"    "Problemas na Baixa por Compensa��o dos titulos da filial "
				DisarmTransaction()
				Return
			EndIf
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE5 						 �
			//������������������������������������������������������������
			RecLock("SE5")
				Replace E5_IDENTEE	With cNumComp
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
				Else
					Replace E5_LA			With "S"
				EndIf
			MsUnLock()

			AAdd(aSE5Recs,{"R",SE5->(Recno())})

			//Gravar o numero da compensacao nos titulos de juros, multa e desconto
			//para permitir que os mesmos sejam cancelados quando do cancelamento da
			//compensacao
			dbSelectArea("SE5")
			nRecAtu := SE5->(Recno())
			nOrdAtu := SE5->(IndexOrd())
			dbSetOrder(2)
			cChaveSe5 := SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(dDatabase)+E5_CLIFOR+E5_LOJA+E5_SEQ)
			aTipodoc := {"DC","MT","JR","CM"}
			For nX := 1 to Len(aTipodoc)
				If dbSeek(xFilial("SE5")+aTipoDoc[nX]+cChaveSE5)
					RecLock("SE5")
						Replace E5_IDENTEE	With cNumComp

						If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
							aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
						Else
							Replace E5_LA			With "S"
						EndIf
					MsUnLock()
               	Endif
      		Next

			SE5->(dbSetOrder(nOrdAtu))
			SE5->(dbGoto(nRecAtu))

			//����������������������������������������������������������Ŀ
			//� Inicializa variaveis para contabilizacao 					 �
			//������������������������������������������������������������
			VALOR := nValRec
			VLRINSTR := VALOR
			If nMoeda <= 5 .And. nMoeda > 1
				cVal := Str(nMoeda,1)
				VALOR&cVal := nValEstrang
			EndIf

			//����������������������������������������������������������Ŀ
			//� Posiciona SA1 para contabiliza��o        					 �
			//������������������������������������������������������������
			dbSelectArea("SA1")
			dbSeek(xFilial("SA1")+SE1->E1_CLIENTE+SE1->E1_LOJA)

			dbSelectArea("SE2")
			dbGobottom()
			dbSkip()

			//����������������������������������������������������������Ŀ
			//� Contabiliza															 �
			//������������������������������������������������������������
			If !lCabec .and. lPadrao
				nHdlPrv := HeadProva( cLote,;
			                      "FINA450",;
			                      Substr( cUsuario, 7, 6 ),;
			                      @cArquivo )

				lCabec := .t.
			End
			If lCabec .and. lPadrao .and. lBaixou .and. (VALOR+VALOR2+VALOR3+VALOR4+VALOR5) > 0
				nTotal += DetProva( nHdlPrv,;
				                    cPadrao,;
				                    "FINA450",;
				                    cLote,;
				                    /*nLinha*/,;
				                    /*lExecuta*/,;
				                    /*cCriterio*/,;
				                    /*lRateio*/,;
				                    /*cChaveBusca*/,;
				                    /*aCT5*/,;
				                    /*lPosiciona*/,;
				                    @aFlagCTB,;
				                    /*aTabRecOri*/,;
				                    /*aDadosProva*/ )

			End
			//�����������������������������������������������������Ŀ
			//�Integracao com o SIGAPCO para lancamento via processo�
			//�PCODetLan("000018","01","FINA450",.T.)               �
			//�������������������������������������������������������
			PcoDetLan("000018","01","FINA450",.F.)
		End
	EndIf
Next

IncProc()//COMPENSA��O

For ni:=1 to Len(aPagSec)
	lBaixou := .F.

	//�����������������������������������Ŀ
	//� Registro do Contas a Receber 	  �
	//�������������������������������������
	If aPagSec[ni][1] == cMarca
		nValPgto := 0

		//����������������������������������������������������������Ŀ
		//� Procura registro no SE2											 �
		//������������������������������������������������������������
		dbSelectArea("SE2")
		dbSetOrder(1)
		dbGoTo(aPagSec[ni][2])

		//����������������������������������������������������������Ŀ
		//� Inicializa variaveis para baixa 								 �
		//������������������������������������������������������������
		dBaixa		:= dDataBase
		cBanco		:= cAgencia := cConta := cBenef := cLoteFin := " "
		cCheque		:= cPortado := cNumBor := " "
		nDespes		:= nDescont := nAbatim := nJuros := nMulta := 0
		nValCc		:= nCm := 0
		lDesconto	:= .F.
		cMotBx		:= "CEC"
		StrLctPad   := SE2->(E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA)
		//����������������������������������������������������������Ŀ
		//� Calcula Abatimentos 												 �
		//������������������������������������������������������������
		dbSelectArea("SE2")

		//TRB->CHAVE 	= SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+;
		//						SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA
		//SumAbatPag(cPrefixo,cNumero,cParcela,cFornece,nMoeda,cCpo,dData,cLoja,cTipoData,dDataIni,dDataFim)
		nTotAbat 	:= SumAbatPag(SE2->E2_PREFIXO,SE2->E2_NUM,SE2->E2_PARCELA,cFornSec,nMoeda,"S",dDataBase,SE2->E2_LOJA)

		cHist070 	:= STR0024				//////"Valor Pago por compensacao" //"Valor Pago por compensacao"
		SE2->(dbGoto(aPagSec[ni][2]))

		//����������������������������������������������������������Ŀ
		//� Calcula valor a ser baixado								 �
		//������������������������������������������������������������

		nValPgto 	:= aPagSec[ni][8] - nTotAbat
		nValEstrang := nValPgto
		nAcresc		:= SE2->E2_SDACRES
		nDecresc	:= SE2->E2_SDDECRE
		nDescont	:= xMoeda(SE2->E2_DESCONT,nMoeda,1,dDataBase)
		nJuros		:= xMoeda(SE2->E2_JUROS,nMoeda,1,dDataBase)
		nMulta		:= xMoeda(SE2->E2_MULTA,nMoeda,1,dDataBase)
		nTxMoeda	:= SE2->E2_TXMOEDA

		// Converte para poder baixar
		If cPaisLoc == "BRA"
		   nValPgto 	:= xMoeda(nValPgto,nMoeda,1,dDataBase)
		   FA080CORR(nValEstrang,0)
		Else
		   nValDia		:= Round(xMoeda(nValPgto,nMoeda,1,dDataBase,nDecs1+1,nTxMoeda),nDecs1)
           nValPgto 	:= Round(xMoeda(nValPgto,nMoeda,1,dDataBase,nDecs1+1,aTxMoedas[nMoeda][2]),nDecs1)
		   nDifCambio   := nValPgto - nValDia
		EndIf

		//����������������������������������������������������������Ŀ
		//� Efetua a baixa														 �
		//������������������������������������������������������������
		If nValPgto > 0
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE2 							 �
			//������������������������������������������������������������
			RecLock("SE2")
				Replace E2_IDENTEE	With cNumComp
			MsUnLock()

			// analisar a gravacao do SE2 - filial destino
			cLanca		:= "S"
			lBaixou		:= fA080Grv(lPadrao,.F.,.F.,cLanca,,IIf(cPaisLoc=="BRA",Nil,aTxMoedas[nMoeda][2]))
			If lBaixou
				nValDif := nValDif - nValPgto
			Else
				Help( ,,'HELP', STR0022, STR0023+cEmpSec, 1, 0)	    /////"ERRO"    "Problemas na Baixa por Compensa��o dos titulos da filial "
				DisarmTransaction()
				Return
			EndIf
			//����������������������������������������������������������Ŀ
			//� Grava numero da compensacao no SE5 							 �
			//������������������������������������������������������������
			RecLock("SE5")
				Replace E5_IDENTEE	With cNumComp

				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
				Else
					Replace E5_LA			With "S"
				EndIf
			MsUnLock()

			AAdd(aSE5Recs,{"P",SE5->(Recno())})

			//Gravar o numero da compensacao nos titulos de juros, multa e desconto
			//para permitir que os mesmos sejam cancelados quando do cancelamento da
			//compensacao
			dbSelectArea("SE5")
			nRecAtu := SE5->(Recno())
			nOrdAtu := SE5->(IndexOrd())
			dbSetOrder(2)
			cChaveSe5 := SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(dDatabase)+E5_CLIFOR+E5_LOJA+E5_SEQ)
			aTipodoc := {"DC","MT","JR","CM"}
			For nX := 1 to Len(aTipodoc)
				If dbSeek(xFilial("SE5")+aTipoDoc[nX]+cChaveSE5)
					RecLock("SE5")
						Replace E5_IDENTEE	With cNumComp
						If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
							aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ), 0, 0, 0} )
						Else
							Replace E5_LA			With "S"
						EndIf
					MsUnLock()
				Endif
			Next

			SE5->(dbSetOrder(nOrdAtu))
			SE5->(dbGoto(nRecAtu))

			//����������������������������������������������������������Ŀ
			//� Inicializa variaveis para contabilizacao 					 �
			//������������������������������������������������������������
			VALOR := nValPgto
			VLRINSTR := VALOR
			If nMoeda <= 5 .And. nMoeda > 1
				cVal := Str(nMoeda,1)
				VALOR&cVal := nValEstrang
			EndIf
			//����������������������������������������������������������Ŀ
			//� Posiciona SA2 para contabiliza��o        					 �
			//������������������������������������������������������������

			dbSelectArea("SA2")
			dbSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA)

			dbSelectArea("SE1")
			dbGobottom()
			dbSkip()

			//����������������������������������������������������������Ŀ
			//� Contabiliza															 �
			//������������������������������������������������������������
			If !lCabec .and. lPadrao
				nHdlPrv := HeadProva( cLote,;
			                      "FINA450",;
			                      Substr( cUsuario, 7, 6 ),;
			                      @cArquivo )

				lCabec := .t.
			End
			If lCabec .and. lPadrao .and. lBaixou .and. (VALOR+VALOR2+VALOR3+VALOR4+VALOR5) > 0
				nTotal += DetProva( nHdlPrv,;
				                    cPadrao,;
				                    "FINA450",;
				                    cLote,;
				                    /*nLinha*/,;
				                    /*lExecuta*/,;
				                    /*cCriterio*/,;
				                    /*lRateio*/,;
				                    /*cChaveBusca*/,;
				                    /*aCT5*/,;
				                    /*lPosiciona*/,;
				                    @aFlagCTB,;
				                    /*aTabRecOri*/,;
				                    /*aDadosProva*/ )

			End
			//�����������������������������������������������������Ŀ
			//�Integracao com o SIGAPCO para lancamento via processo�
			//�PCODetLan("000018","01","FINA450",.T.)               �
			//�������������������������������������������������������
			PcoDetLan("000018","01","FINA450",.F.)
		End
	EndIf
Next

IncProc()//COMPENSA��O

If nValDif == (nTotRecSec - nTotPagSec)
	If !IncTit(nValDif,2,cNumComp)
		////Help( ,,'HELP', "ERRO", "Problemas na Inclusao Titulo Compensa��o dos titulos da filial "+cEmpSec, 1, 0)
		Help( ,,'HELP', STR0022, STR0026+cEmpSec, 1, 0)
		DisarmTransaction()
		Return
	Endif
Else
	/////Help( ,,'HELP', "ERRO", "Problemas com a compensa��o dos titulos da filial "+cEmpSec, 1, 0)
	Help( ,,'HELP', STR0022, STR0027+cEmpSec, 1, 0)
	DisarmTransaction()
	Return
EndIf

If	lProcessou

	IF lPadrao .and. lCabec
		RodaProva(  nHdlPrv,;
					nTotal)
		//�����������������������������������������������������Ŀ
		//� Envia para Lancamento Contabil							  �
		//�������������������������������������������������������
		lDigita:=IIF(mv_par01==1,.T.,.F.)
		lAglut :=IIF(mv_par02==1,.T.,.F.)
		cA100Incl( cArquivo,;
		           nHdlPrv,;
		           3,;
		           cLote,;
		           lDigita,;
		           lAglut,;
		           /*cOnLine*/,;
		           /*dData*/,;
		           /*dReproc*/,;
		           @aFlagCTB,;
		           /*aDadosProva*/,;
		           /*aDiario*/ )
		aFlagCTB := {}  // Limpa o coteudo apos a efetivacao do lancamento
	Endif

Endif

End Transaction

cFilAnt := cFilOld


//�������������������������������������������������������Ŀ
//� Grava o numero da compensacao no.                     �
//���������������������������������������������������������
	PutMv("MV_NUMCOMP",cNumComp)
//Relatorio

DbSelectArea("TMPCMP")
DbCloseArea()

//���������������������������������������������������������������Ŀ
//�Rotina para apresentar os titulos compensados. Pagar e Receber.�
//�����������������������������������������������������������������

u_CNIRL317(cNumComp)

Return

Static Function IncTit(nValDif,nOper,cNumComp)

Local lRet := .T.

If nValDif > 0
	lMsErroAuto := .F. // variavel interna da rotina automatica
	aTit := {}
	AADD(aTit,  { 	"E1_FILIAL"		,xFilial("SE1")		, NIL } )
	AADD(aTit,  { 	"E1_PREFIXO"	,"DME"				, NIL } )
	AADD(aTit,  { 	"E1_NUM"		,cNumComp			, NIL } )
	AADD(aTit,  { 	"E1_PARCELA"	,' '				, NIL } )
	AADD(aTit,  { 	"E1_TIPO"		,MV_PAR14			, NIL } )
	AADD(aTit,  { 	"E1_NATUREZ"	,MV_PAR15			, NIL } )
	AADD(aTit,  { 	"E1_CLIENTE"	,If(nOper==1,cCliePri,cClieSec)		, NIL } )
	AADD(aTit,  { 	"E1_LOJA"		,If(nOper==1,cLojCliPri,cLojCliSec)	, NIL } )
	AADD(aTit,  { 	"E1_EMISSAO"	,dDataBase			, NIL } )
	AADD(aTit,  { 	"E1_VENCTO"		,dDataBase			, NIL } )
	AADD(aTit,  { 	"E1_VALOR"		,nValDif			, NIL } )

	MSExecAuto({|x, y| FINA040(x, y)}, aTit, 3)

	If lMsErroAuto
		MostraErro()
		DisarmTransaction()
		lREt := .f.
	EndIf
ElseIf nValDif < 0
	lMsErroAuto := .F. // variavel interna da rotina automatica
	aTit := {}
	AADD(aTit,  { 	"E2_FILIAL"		,xFilial("SE2")		, NIL } )
	AADD(aTit,  { 	"E2_PREFIXO"	,"DME"				, NIL } )
	AADD(aTit,  { 	"E2_NUM"		,cNumComp			, NIL } )
	AADD(aTit,  { 	"E2_PARCELA"	,' '				, NIL } )
	AADD(aTit,  { 	"E2_TIPO"		,MV_PAR16			, NIL } )
	AADD(aTit,  { 	"E2_NATUREZ"	,MV_PAR17			, NIL } )
	AADD(aTit,  { 	"E2_FORNECE"	,If(nOper==1,cFornPri,cFornSec)		, NIL } )
	AADD(aTit,  { 	"E2_LOJA"		,If(nOper==1,cLojForPri	,cLojForSec), NIL } )
	AADD(aTit,  { 	"E2_EMISSAO"	,dDataBase			, NIL } )
	AADD(aTit,  { 	"E2_VENCTO"		,dDataBase			, NIL } )
	AADD(aTit,  { 	"E2_VALOR"		,(nValDif*-1)		, NIL } )

	MSExecAuto({|x, y, z| FINA050(x, y, z)}, aTit, 3,3)

	If lMsErroAuto
		MostraErro()
		DisarmTransaction()
		lREt := .f.
	EndIf
EndIf

Return(lRet)


/*/
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 �CNI317CalCn � Autor � Oswaldo Leite         � Data � 09/08/11 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Calcula Parcelas, Nro.Titulos e valor da Liquida. a cancelar ���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � Cni317													    ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CNI317CalCn(cCompCan,cIndex)
Local cChave
Local nIndex

//�����������������������������������������������������������������Ŀ
//� Cria indice condicional separando os titulos que deram origem a �
//� liquidacao e os titulos que foram gerados							  �
//�������������������������������������������������������������������
dbSelectArea("SE5")
cIndex := CriaTrab(nil,.f.)
cChave := IndexKey()
IndRegua("SE5",cIndex,cChave,,CNI317FCAN(),STR0005)                        ////'Selecionando Registros'"Selecionando Registros...")

nIndex := RetIndex("SE5")
dbSelectArea("SE5")

#IFNDEF TOP
	dbSetIndex(cIndex+OrdBagExt())
#ENDIF
dbSetOrder(nIndex+1)
dbGoTop()

//���������������������������������������������������������������������Ŀ
//� Certifica se foram encontrados registros na condi��o selecionada		�
//�����������������������������������������������������������������������
If  BOF() .and. EOF()
	Help(" ",1,"RECNO")
	//������������������������������������������������������������������Ŀ
	//� Restaura os indices do SE5 e deleta o arquivo de trabalho			�
	//��������������������������������������������������������������������
	dbSelectArea("SE5")
	Set Filter to
	RetIndex("SE5")
	fErase(cIndex+OrdBagExt())
	cIndex := ""
	dbSetOrder(1)
	Return .F.
EndIf

dbSelectArea("SE5")

Return .T.


/*/
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 � CNI317FCan � Autor � Oswaldo Leite         � Data � 02/02/98 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Sele��o para a cria��o do indice condicional no CANCELAMENTO ���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � cni317														 ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CNI317FCan()
//���������������������������������������������������������������������������Ŀ
//� Devera selecionar todos os registros que atendam a seguinte condi��o : 	�
//���������������������������������������������������������������������������Ĵ
//� 2. Ou titulos que tenham originado a liquidacao selecionada 						�
//�����������������������������������������������������������������������������
Local cFiltro

cFiltro := 'E5_IDENTEE=="'+cCompCan+'".And.'
cFiltro += 'Dtos(E5_DATA)<="'+Dtos(dDataBase)+'".And.'
cFiltro += 'E5_TIPODOC<>"ES".And.'
cFiltro += 'E5_SITUACA<>"C".And.'
cFiltro += 'E5_SITUACA<>"X"'

If ExistBlock("F450FCA")
	cFiltro += '.And.'
	cFiltro += ExecBlock("F450FCA",.F.,.F.)
EndIf

Return cFiltro


/*/
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 � CNI317VrfFilial  Autor  Oswaldo            � Data � 10/08/11 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Identifica Formato da Empresa/Unidade/Filial usada na Base   ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe	 � CNI317VrfFilial												���
���          |	cParam1 - conteudo do campo filial da tabela-1				���
���          |	cParam2 - conteudo do campo filial da tabela-2				���
���          |	cLayOut - layout CFGX032									���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � Generico 												    ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function CNI317VrfFilial(cParam1, cParam2, cLayOut)
Local nE := 0, nU := 0, nF := 0
Local nCont, cRet := ""
Local nTamFixoDoCampo := 7

for nCont := 1 to Len(cLayOut)

	if substr(cLayout,nCont,1) == 'E'
		nE++
	Elseif substr(cLayout,nCont,1) == 'U'
		nU++
	Elseif substr(cLayout,nCont,1) == 'F'//o sistema exige ao menos 2 digitos de filial
		nF++
	Endif

Next nCont
nTamFixoDoCampo := Len(cParam1)

if Empty(AllTrim(cParam1))
    cRet := cParam1
else
    //Baseado totalmente do layout definido para o c�digo da empresa: EEUUFF, EUF ... etc
	if nE > 0
		cRet := cRet + substr(cParam2,01, nE)
	EndIf

	if nU > 0
		cRet := cRet + substr(cParam2,nE+1, nU)
	EndIf

	if nF > 0
		cRet := cRet + substr(cParam2,nE+nU+1, nF)
	EndIf

   cRet := cRet + space(nTamFixoDoCampo - Len(cRet))
EndIf

Return (cRet)
