#Include 'Protheus.ch'
#INCLUDE 'FWADAPTEREAI.CH'
#Include "FINA404.CH" 

/*/{Protheus.doc} FINA404
Funcao de integracao com o adapter EAI para envio de informa��es dos fornecedores autonomos.
@author	William Matos
@since		17/06/2015
/*/
Function FINA404()
Local oProcesso	:= Nil
Local oGrid		:= Nil

oGrid := FWGridProcess():New(	"FINA404",; //Nome da fun��o
								STR0001,; //"Integra�ao de Fornecedores Autonomos
								STR0002,; //Descri��o da rotina
								{|lEnd| lRet := FN404EnvioXML(oGrid,@lEnd)},; //Bloco de execu��o
								"FINA404",;// Pergunte
								Nil,; //cGrid
								.T.) //lSaveLog

oGrid:SetMeters(1)
oGrid:Activate()

Return

/*/{Protheus.doc} FN404EnvioXML
Envio do XML com os dados dos fornecedores autonomos.
@author	William Matos
@since		17/06/2015
/*/
Function FN404EnvioXML(oGrid,lEnd)
Local cQuery	:= ""
Local cAliasXML	:= ""
Local lIntFN404	:= FWHasEAI("FINA404",.T.,.F.,.T.)
Local aLog		:= {}
Local nX		:= 0
Local cSepAba	:= If("|"$MVABATIM,"|",",")
Local cSepAnt	:= If("|"$MVPAGANT,"|",",")
Local cSepNeg	:= If("|"$MV_CRNEG,"|",",")
Local cSepProv	:= If("|"$MVPROVIS,"|",",")
Local cSepRec	:= If("|"$MVRECANT,"|",",")
Local nRegSM0	:= SM0->(Recno())
Local aSM0		:= FWLoadSM0()
Local nFil		:= 0
Local lErro		:= .F.
Local lRunSched	:= FWGetRunSchedule()

If lIntFN404 //Integra��o configurada no EAI

	If !lRunSched //Nao carrega se for schedule pois as definicoes sao obtidas de la (botao parametros)
		Pergunte("FINA404", .F. )
		//MV_PAR01 - Data Inicial
		//MV_PAR02 - Data Final
		//MV_PAR03 - Fornecedor Inicial
		//MV_PAR04 - Loja Inicial
		//MV_PAR05 - Fornecedor Final
		//MV_PAR06 - Loja Final
	EndIf

	oGrid:SetMaxMeter(Len(aSM0), 1, STR0012) //"Processando as filiais"

	For nFil := 1 To Len( aSM0 )

		oGrid:SetIncMeter(1)

		If aSM0[nFil][1] == cEmpAnt

			cFilAnt := aSM0[nFil][2]

			cAliasXML 	:= GetNextAlias()

			//Monta query.
			cQuery := " SELECT E2_BASEINS, E2_INSS, E2_EMISSAO, E2_VENCTO, E2_FORNECE, E2_LOJA, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, SE2.R_E_C_N_O_, " + CRLF
			cQuery +=		" A2_NOME, A2_CGC, A2_NUMDEP, A2_FILIAL, A2_COD, A2_LOJA, A2_DTNASC, A2_INDRUR, A2_CODNIT, A2_CBO, A2_CATEG, A2_OCORREN " + CRLF
			cQuery += " FROM " + RetSQLName("SE2") + ' SE2 ' 													+ CRLF
			cQuery +=		" INNER JOIN " + RetSQLName("SA2") + " SA2 ON A2_COD = E2_FORNECE " 				+ CRLF
			cQuery +=		" AND A2_LOJA = E2_LOJA " 															+ CRLF
			cQuery += " WHERE " 																				+ CRLF
			cQuery +=		" A2_FILIAL = '" + XFilial("SA2") + "' AND "										+ CRLF
			cQuery +=		" A2_TIPO = 'F' AND " 																+ CRLF //Apenas registros de fornecedore fisicos.
			cQuery +=		" (A2_DTNASC	<> ' ' OR "															+ CRLF //Considera o fornecedor caso algum dado da pasta Autonomos esteja preenchido
			cQuery +=		" A2_CODNIT	<> ' ' OR "																+ CRLF
			cQuery +=		" A2_CATEG		<> ' ' OR "															+ CRLF
			cQuery +=		" A2_OCORREN	<> ' ') AND "														+ CRLF
			cQuery +=		" E2_FILORIG = '"			+ cFilAnt + "' AND "									+ CRLF
			cQuery +=		" E2_EMISSAO BETWEEN '"	+ DTOS(MV_PAR01) + "' AND '" + DTOS(MV_PAR02)	+ "' AND "	+ CRLF
			cQuery +=		" E2_FORNECE BETWEEN '"	+ MV_PAR03 + "' AND '"+ MV_PAR05 				+ "' AND "	+ CRLF
			cQuery +=		" E2_LOJA    BETWEEN '"	+ MV_PAR04 + "' AND '"+ MV_PAR06				+ "' AND "	+ CRLF
			cQuery +=		" E2_TIPO NOT IN "		+ FormatIn(MVABATIM,cSepAba)					+ " AND "	+ CRLF
			cQuery +=		" E2_TIPO NOT IN "		+ FormatIn(MVPAGANT,cSepAnt)					+ " AND "	+ CRLF
			cQuery +=		" E2_TIPO NOT IN "		+ FormatIn(MV_CRNEG,cSepNeg)					+ " AND "	+ CRLF
			cQuery +=		" E2_TIPO NOT IN "		+ FormatIn(MVPROVIS,cSepProv)					+ " AND "	+ CRLF
			cQuery +=		" E2_TIPO NOT IN "		+ FormatIn(MVRECANT,cSepRec)					+ " AND "	+ CRLF
			cQuery +=		" E2_SEFIP = ' ' AND "																+ CRLF //Apenas registros que n�o foram integrados.
			cQuery +=		" SA2.D_E_L_E_T_ = '' AND SE2.D_E_L_E_T_ = '' "										+ CRLF
			cQuery +=		"AND NOT EXISTS ("																	+ CRLF
			cQuery +=			" SELECT E5_NUMERO "															+ CRLF
			cQuery +=			" FROM " + RetSQLName("SE5") + " SE5 "											+ CRLF
			cQuery +=			" WHERE SE5.E5_FILIAL = '" + XFilial("SE5") + "' AND "							+ CRLF
			cQuery +=				" SE5.E5_PREFIXO	= SE2.E2_PREFIXO	AND "								+ CRLF
			cQuery +=				" SE5.E5_NUMERO	= SE2.E2_NUM	AND "										+ CRLF
			cQuery +=				" SE5.E5_PARCELA	= SE2.E2_PARCELA	AND "								+ CRLF
			cQuery +=				" SE5.E5_TIPO		= SE2.E2_TIPO		AND "								+ CRLF
			cQuery +=				" SE5.E5_CLIFOR	= SE2.E2_FORNECE	AND "									+ CRLF
			cQuery +=				" SE5.E5_LOJA		= SE2.E2_LOJA		AND "								+ CRLF
			cQuery +=				" SE5.E5_MOTBX	= 'DSD'			AND "										+ CRLF
			cQuery +=				" SE5.E5_RECPAG	= 'P'				AND "									+ CRLF
			cQuery +=				" SE5.D_E_L_E_T_	= ' ') "												+ CRLF
			cQuery += " ORDER BY A2_COD, A2_LOJA, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO "

			cQuery := ChangeQuery(cQuery)

			DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasXML,.T.,.T.)
			DbSelectArea(cAliasXML)

			//Processa o XML
			If (cAliasXML)->( !Eof() )
				FN040Struct( (cAliasXML), aLog,@lErro,oGrid)
			Else
				Aadd(aLog,STR0004 + " " + STR0007 + AllTrim(cFilAnt) + "." )//"N�o existem dados para serem processados."###"Filial: "
			EndIf	

			(cAliasXML)->(DBCloseArea())
			FErase((cAliasXML) + GetDbExtension())
			cAliasXML := ""

		EndIf

	Next nFil

	SM0->(DBGoTo(nRegSM0))
	cFilAnt := FWGETCODFILIAL

	//Salva log de processamento do XML.
	If Len(aLog) > 0

		For nX := 1 To Len(aLog)
			oGrid:SaveLog(aLog[nX])
		Next nX

		aSize(aLog,0)
		aLog := Nil

		If lErro
			Help(" ",1,"FN404EnvioXML",,STR0011,1,0) //"Houve ocorr�ncia na exporta��o dos dados, consulte o log."
		EndIf

	EndIf

Else

	oGrid:SaveLog(STR0003 ) //'Verifique se o atualizador de dicion�rio de dados foi aplicado no ambiente e se o Adapter EAI esta configurado corretamente no configurador.'

EndIf

aSize(aSM0,0)
aSM0 := Nil

Return 

/*/{Protheus.doc} FN404EnvioXML
Fun��o que preenche o xml para envio.
@author	William Matos
@since 17/06/2015
@param cAlias - Entidade temporaria com os dados que ser�o enviados pela mensagem unica
/*/
Function FN040Struct(cAliasXML,aLog,lErro,oGrid)
Local cEvent 		:= "UPSERT"
Local cXMLRet		:= ""
Local cXMLCab		:= ""
Local cInternalID	:= ""
Local cCEITomador	:= {}
Local cCGCTomador	:= ""
Local cTpTomador	:= ""
Local cNatService	:= "1" //Urbano
Local cLog			:= ""
Local aResult		:= {}
Local nX			:= 0
Local cMensErro		:= ""

Local cChvAux		:= ""

//Pesquisa dados na SM0
dbSelectArea("SM0")
If SM0->(dbSeek(cEmpAnt))
	cCEITomador	:= SM0->M0_CEI
	cCGCTomador	:= SM0->M0_CGC
	cTpTomador	:= '0' //Default como servi�o para o segmento.
EndIf

cXMLCab := '<BusinessEvent>'
cXMLCab += '	<Entity>ExternalAutonomousPayment</Entity>'
cXMLCab += '	<Event>' + cEvent + '</Event>'
cXMLCab += '</BusinessEvent>'
cXMLCab += '<BusinessContent>'
cXMLCab += '<ListOfAutonomous>'

While (cAliasXML)->( !Eof() ) .AND. nX <= 50

	//Caso um dos campos necessarios nao estejam preenchidos, nao envia os dados e gera log
	If	Empty((cAliasXML)->A2_NOME)		.Or.; 
		Empty((cAliasXML)->A2_CGC)		.Or.;
		Empty((cAliasXML)->A2_CBO)		.Or.;
		Empty((cAliasXML)->A2_DTNASC)	.Or.;
		Empty((cAliasXML)->A2_CODNIT)	.Or.;
		Empty((cAliasXML)->A2_CATEG)	.Or.;
		Empty((cAliasXML)->A2_OCORREN)

		cChvAux := (cAliasXML)->A2_FILIAL + (cAliasXML)->A2_COD + (cAliasXML)->A2_LOJA

		cMensErro := STR0005 + AllTrim((cAliasXML)->A2_COD) + STR0006 + AllTrim((cAliasXML)->A2_LOJA) + If(!Empty((cAliasXML)->A2_FILIAL)," (" + STR0007 + AllTrim(cFilAnt) + ")","" ) + STR0008 //"O Fornecedor: "###" Loja: "###"Filial: "###", est� com seu cadastro incompleto para envio dos dados."

		cMensErro += STR0009 //" Confira os campos: "

		cMensErro += AllTrim(RetTitle("A2_NOME"))		+ ", "
		cMensErro += AllTrim(RetTitle("A2_CGC"))		+ ", "
		cMensErro += AllTrim(RetTitle("A2_CBO"))		+ ", "
		cMensErro += AllTrim(RetTitle("A2_DTNASC"))		+ ", "
		cMensErro += AllTrim(RetTitle("A2_CODNIT"))		+ ", "
		cMensErro += AllTrim(RetTitle("A2_CATEG"))		+ STR0010 //" e "
		cMensErro += AllTrim(RetTitle("A2_OCORREN"))	+ "."

		Aadd(aLog,cMensErro)
		lErro := .T.

		//Faz la�o para pular todos os titulos do fornecedor com dados incompletos
		While (cAliasXML)->(!Eof()) .And. cChvAux == (cAliasXML)->A2_FILIAL + (cAliasXML)->A2_COD + (cAliasXML)->A2_LOJA  
		(cAliasXML)->(DBSkip())
		EndDo

		//Se nao acabou os dados, volto 1, pois o loop avancara para o proximo
		If (cAliasXML)->(!Eof())
			(cAliasXML)->(DBSkip(-1))
		EndIf

		Loop

	EndIf

	If (cAliasXML)->A2_INDRUR == '0' //N�o � Produtor rural.
		cNatService := '1' //Urbano 
	Else
		cNatService := '2' //Rural
	EndIf

	cInternalID :=	cEmpAnt							+ "|" + ;
					RTrim(XFilial("SE2"))			+ "|" + ;
					RTrim((cAliasXML)->E2_PREFIXO)	+ "|" + ;
					RTrim((cAliasXML)->E2_NUM)		+ "|" + ;
					RTrim((cAliasXML)->E2_PARCELA)	+ "|" + ;
					RTrim((cAliasXML)->E2_TIPO)		+ "|" + ;
					RTrim((cAliasXML)->E2_FORNECE)	+ "|" + ;
					RTrim((cAliasXML)->E2_LOJA)

	cXMLRet +=	'<Autonomous>'
	cXMLRet +=	'<CompanyId>'						+ AllTrim(cEmpAnt)  	 								+ '</CompanyId>'
	cXMLRet +=	'<BranchId>'						+ AllTrim(cFilAnt) 	 									+ '</BranchId>'
	cXMLRet +=	'<InternalId>'						+ cInternalID 		 									+ '</InternalId>'
	cXMLRet +='<CompanyInternalId>'					+ AllTrim(cEmpAnt + "|" + cFilAnt) 						+ '</CompanyInternalId>'
	cXMLRet +=	'<TakerId>'							+ cCGCTomador 		 									+ '</TakerId>'
	cXMLRet +=	'<TakerSpecificId>'					+ cCEITomador 											+ '</TakerSpecificId>'
	cXMLRet +=	'<TakerType>'						+ cTpTomador 			 								+ '</TakerType>'
	cXMLRet +=	'<AutonomousName>'					+ _NoTags((cAliasXML)->A2_NOME)							+ '</AutonomousName>'
	cXMLRet +=	'<DateOfBirth>'						+ Transform((cAliasXML)->A2_DTNASC,"@R 9999-99-99")		+ '</DateOfBirth>'
	cXMLRet +=	'<AutonomousId>'					+ AllTrim((cAliasXML)->A2_CGC)							+ '</AutonomousId>'
	cXMLRet +=	'<RegistrationNumber>'				+ (cAliasXML)->A2_CODNIT								+ '</RegistrationNumber>'
	cXMLRet +=	'<AutonomousOcupationNationalCode>'	+ (cAliasXML)->A2_CBO									+ '</AutonomousOcupationNationalCode>'
	cXMLRet +=	'<AutonomousCategory>'				+ (cAliasXML)->A2_CATEG									+ '</AutonomousCategory>'
	cXMLRet +=	'<SefipEventCode>'					+ (cAliasXML)->A2_OCORREN								+ '</SefipEventCode>'
	cXMLRet +=	'<IssueDate>'						+ Transform((cAliasXML)->E2_EMISSAO, "@R 9999-99-99")	+ '</IssueDate>'
	cXMLRet +=	'<DueDate>'							+ Transform((cAliasXML)->E2_VENCTO,"@R 9999-99-99" )	+ '</DueDate>'
	cXMLRet +=	'<InitiationDate>'					+ Transform((cAliasXML)->E2_EMISSAO,"@R 9999-99-99")	+ '</InitiationDate>'
	cXMLRet +=	'<ServiceNature>'					+ _NoTags(cNatService)									+ '</ServiceNature>'
	cXMLRet +=	'<DependentsNumber>'				+ cValToChar((cAliasXML)->A2_NUMDEP)					+ '</DependentsNumber>'
	cXMLRet +=	'<IRRFDependentsNumber>'			+ cValToChar((cAliasXML)->A2_NUMDEP)					+ '</IRRFDependentsNumber>'
	cXMLRet +=	'<PaymentValue>'					+ cValtoChar(ABS((cAliasXML)->E2_BASEINS))				+ '</PaymentValue>'
	cXMLRet +=	'<INSSValue>'						+ cValtoChar(ABS((cAliasXML)->E2_INSS))					+ '</INSSValue>'
	cXMLRet +=	'</Autonomous>'	
	nX++
	(cAliasXML)->(DbSkip())
	If nX == 50

		cXMLRet += 	'	</ListOfAutonomous>'
		cXMLRet += 	'</BusinessContent>'

		cXmlRet := cXMLCab + cXmlRet

		//Envia os dados.
		aResult := FwIntegDef( 'FINA404', EAI_MESSAGE_BUSINESS , TRANS_SEND , cXMLRet ) 
		If !aResult[1]
			Aadd(aLog,cValToChar(aResult[2]))
			lErro := .T.
		Else
			cXMLRet := ""
			nX 		 := 0
		EndIf

	EndIf

EndDo

If nX > 0

	cXMLRet += '	</ListOfAutonomous>'
	cXMLRet += '</BusinessContent>'

	cXmlRet := cXMLCab + cXmlRet

	//Envia os dados.
	aResult := FwIntegDef( 'FINA404', EAI_MESSAGE_BUSINESS , TRANS_SEND , cXMLRet ) 

	If !aResult[1]
		Aadd(aLog,cValToChar(aResult[2]))
		lErro := .T.
	EndIf

EndIf

aSize(aResult,0)
aResult := Nil

Return

/*/{Protheus.doc}IntegDef
Mensagem unica de integra��o com RM, envio de dados dos fornecedores autonomos.
@param cXml	  Xml passado para a rotina
@param nType 	  Determina se e uma mensagem a ser enviada/recebida ( TRANS_SEND ou TRANS_RECEIVE)
@param cTypeMessage Tipo de mensagem ( EAI_MESSAGE_WHOIS, EAI_MESSAGE_RESPONSE, EAI_MESSAGE_BUSINESS)
@author William Matos
@since  19/06/15
/*/
Static Function IntegDef( cXml, nType, cTypeMessage )
Local aRet := {}

aRet := FINI404( cXml, nType, cTypeMessage )

Return aRet

/*/{Protheus.doc} SchedDef
Utilizado somente se a rotina for executada via Schedule.
Permite usar o botao Parametros da nova rotina de Schedule
para definir os parametros(SX1) que serao passados a rotina agendada.
@author  TOTVS
@version 12.1.11
@since   04/03/2016
@return  aParam
/*/
Static Function SchedDef()
Local aParam := {}

aParam := {	"P"			,;	//Tipo R para relatorio P para processo
				"FINA404"	,;	//Nome do grupo de perguntas (SX1)
				Nil			,;	//cAlias (para Relatorio)
				Nil			,;	//aArray (para Relatorio)
				Nil			}	//Titulo (para Relatorio)

Return aParam