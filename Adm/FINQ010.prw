#include "protheus.ch"
#include "quicksearch.ch"
#include "finq010.ch"

QSSTRUCT FINQ010 DESCRIPTION STR0001 MODULE 6		//"Titulos em aberto - Clientes"

QSMETHOD INIT QSSTRUCT FINQ010

	//Relacionamento das tabelas	
	QSTABLE "SA1" JOIN "SE1" // baseado no SX9
	
	// campos do SX3 e indices do SIX		
	QSPARENTFIELD "E1_PREFIXO","E1_NUM" INDEX ORDER 1 LABEL STR0002 	//"Prefixo + Titulo"
	QSPARENTFIELD "A1_NOME" INDEX ORDER 2 SET RELATION TO "E1_CLIENTE","E1_LOJA" WITH "A1_COD","A1_LOJA" LABEL STR0004	//"Nome Cliente"
	
	// campos do SX3
	QSFIELD "E1_PREFIXO" LABEL STR0010	//"Pref."
	QSFIELD "E1_NUM"     LABEL STR0006	//"Titulo"
	QSFIELD "E1_PARCELA" LABEL STR0011	//"Parc."
	QSFIELD "E1_VENCREA" LABEL STR0007	//"Vencto."
	QSFIELD "E1_VLCRUZ"  LABEL STR0012	//"Valor"
	QSFIELD "E1_SALDO"   LABEL STR0008	//"Saldo"
	QSFIELD "A1_NOME"    LABEL STR0009	//"Nome"

	// acoes do menudef, MVC ou qualquer rotina
	QSACTION MENUDEF "MATA030" OPERATION 2 LABEL STR0016	//"Visualizar Cliente"
	QSACTION MENUDEF "FINA040" OPERATION 2 LABEL STR0017	//"Visualizar Titulo"

	//Filtros (o primeiro � o padr�o)
	QSFILTER STR0024																									//"Todos"        	
	QSFILTER STR0018 WHERE "E1_SALDO > 0"																				//"Em aberto"
	QSFILTER STR0019 WHERE "E1_SALDO > 0 AND E1_VENCREA < '"+DTOS(Date())+"' "											//"Atrasados"
	QSFILTER STR0020 WHERE "E1_SALDO > 0 AND E1_VENCREA = '"+DTOS(Date())+"' "											//"Vencendo hoje"
	QSFILTER STR0021 WHERE "E1_SALDO > 0 AND E1_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+ 7)+"' "			//"Proximos 7 dias"
	QSFILTER STR0022 WHERE "E1_SALDO > 0 AND E1_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+15)+"' "			//"Proximos 15 dias"
	QSFILTER STR0023 WHERE "E1_SALDO > 0 AND E1_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+30)+"' "			//"Proximos 30 dias"
	
Return
