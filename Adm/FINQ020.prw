#include "protheus.ch"
#include "quicksearch.ch"
#include "finq020.ch"

QSSTRUCT FINQ020 DESCRIPTION STR0001 MODULE 6		//"Titulos em aberto - Fornecedores"

QSMETHOD INIT QSSTRUCT FINQ020

	//Relacionamento das tabelas	
	QSTABLE "SA2" JOIN "SE2" ON "E2_FORNECE = A2_COD AND E2_LOJA = A2_LOJA"	
	
	// campos do SX3 e indices do SIX		
	QSPARENTFIELD "E2_PREFIXO","E2_NUM" INDEX ORDER 1 LABEL STR0002 	//"Prefixo + Titulo"
	QSPARENTFIELD "A2_NOME" INDEX ORDER 2 SET RELATION TO "E2_FORNECE","E2_LOJA" WITH "A2_COD","A2_LOJA" LABEL STR0004	//"Nome Fornecedor"

	// campos do SX3
	QSFIELD "E2_PREFIXO" LABEL STR0006		//"Pref."
	QSFIELD "E2_NUM"     LABEL STR0007		//"Titulo"
	QSFIELD "E2_PARCELA" LABEL STR0011		//"Parc."
	QSFIELD "E2_VENCREA" LABEL STR0008		//"Vencto."
	QSFIELD "E2_VLCRUZ"  LABEL STR0012		//"Valor"
	QSFIELD "E2_SALDO"   LABEL STR0009		//"Saldo"
	QSFIELD "A2_NOME"    LABEL STR0010		//"Nome"


	// acoes do menudef, MVC ou qualquer rotina
	QSACTION MENUDEF "MATA020" OPERATION 2 LABEL STR0016	//"Visualizar Fornecedor"
	QSACTION MENUDEF "FINA050" OPERATION 2 LABEL STR0017	//"Visualizar Titulo"

	//Filtros (o primeiro � o padr�o)
	QSFILTER STR0024																							//"Todos"
	QSFILTER STR0018 WHERE "E2_SALDO > 0"																		//"Em aberto"
	QSFILTER STR0019 WHERE "E2_SALDO > 0 AND E2_VENCREA < '"+DTOS(Date())+"' "									//"Atrasados"
	QSFILTER STR0020 WHERE "E2_SALDO > 0 AND E2_VENCREA = '"+DTOS(Date())+"' "									//"Vencendo hoje"
	QSFILTER STR0021 WHERE "E2_SALDO > 0 AND E2_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+ 7)+"' "	//"Proximos 7 dias"
	QSFILTER STR0022 WHERE "E2_SALDO > 0 AND E2_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+15)+"' "	//"Proximos 15 dias"
	QSFILTER STR0023 WHERE "E2_SALDO > 0 AND E2_VENCREA BETWEEN '"+DTOS(Date())+"' AND '"+DTOS(Date()+30)+"' "	//"Proximos 30 dias"
Return
