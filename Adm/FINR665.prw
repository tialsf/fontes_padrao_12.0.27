#include 'protheus.ch'
#include 'FINR665.ch'	


/*/{Protheus.doc} FINR665
Relat�rio - Mapa de Viagem

@author Alexandre Felicio
@since 04/09/2015
@version P12 R12.1.7

@return Nil
/*/
function FINR665()
Local lRet		:= .T.
Local cPerg		:= "FINR665"
Private aSelFil := {}

lRet := Pergunte( cPerg , .T. )

If mv_par21 == 1 .And. Len( aSelFil ) <= 0 .and. lRet
	aSelFil := AdmGetFil()
	If Len( aSelFil ) <= 0
		lRet := .F.
	EndIf 
EndIf    

If lRet
	oReport := ReportDef(cPerg)
	oReport:PrintDialog()
EndIf
	
return


/*/{Protheus.doc} ReportDef
Defini��o de layout do relat�rio  

@author Alexandre Felicio
@since 04/09/2015
@version P12 R12.1.7

@return Nil
/*/
Static Function ReportDef(cPerg)
Local cAlsFL5       := GetNextAlias()
Local oSection	    := Nil
Local oSessao1		:= Nil
Local oSessao2		:= Nil
Local oSessao3		:= Nil
Local oSessao4		:= Nil
Local oSessao5		:= Nil
Local oSessao6		:= Nil
Local oSessao7		:= Nil
Local oSessao8		:= Nil
Local oSessao9		:= Nil
Local oReport		:= Nil
Local oBreak	    := Nil

                                   ///"Mapa de Viagens"                                 ///"Mapa de Viagens" 
oReport := TReport():New("FINR665",STR0001,cPerg,{|oReport| PrintReport(oReport,cPerg)},STR0001	,.T.		,			,.F.			,			,				,				,			)
									
										///"Viagem"
oSection := TRSection():New( oReport	,STR0002	,"FL5"	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Viagem
                         ///"Viagem"
TRCell():New( oSection	,"VIAGEM" 		,"FL5"  ,STR0002,			,8		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New( oSection	,"FL5_FILIAL"	,"FL5"	,		,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)
TRCell():New( oSection	,"FL5_VIAGEM"	,"FL5"	,		,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)
TRCell():New( oSection	,"FL5_DESORI"	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		) 
TRCell():New( oSection	,"FL5_DESDES"	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		)
TRCell():New( oSection	,"FL5_DTINI"	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		) 
TRCell():New( oSection	,"FL5_DTFIM"	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		) 
TRCell():New( oSection	,"A1_NOME"   	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		)
TRCell():New( oSection	,"FL5_STATUS"	,"FL5"	,		,			,		,		,		,		,.T.		,				,			,			,.T.		,			,			,		)

									    ///"A�reo"
oSessao1 := TRSection():New( oReport,STR0003	,'FL7'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //A�reo
TRCell():New(oSessao1,"AEREO"		,"FL7",STR0003	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao1,"FL7_NOME"   	,"FL7",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao1,"FL7_ORIGEM" 	,"FL7",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao1,"FL7_DESTIN" 	,"FL7",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao1,"FL7_DSAIDA" 	,"FL7",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao1,"FL7_DCHEGA" 	,"FL7",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
									
										///"Hospedagem"
oSessao2 := TRSection():New( oReport,STR0004	,'FL9'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Hospedagem
TRCell():New(oSessao2,"HOSPEDAGEM"	,"FL9",STR0004	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao2,"FL9_NOME"   	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao2,"FL9_DIARIA" 	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao2,"FL9_DCHKIN" 	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao2,"FL9_HCHKIN" 	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao2,"FL9_DCHKOU" 	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
TRCell():New(oSessao2,"FL9_HCHKOU" 	,"FL9",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
									
										///"Rodovi�rio"
oSessao3 := TRSection():New( oReport	,STR0005	,'FL8'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Rodovi�rio
TRCell():New(oSessao3,"RODOVIARIO" ,"FL8",STR0005	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao3,"FL8_NOME"   ,"FL8",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao3,"FL8_ORIGEM" ,"FL8",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao3,"FL8_DESTIN" ,"FL8",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao3,"FL8_DSAIDA" ,"FL8",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao3,"FL8_DCHEGA" ,"FL8",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
									
										///"Loca��o Ve�culos"
oSessao4 := TRSection():New( oReport	,STR0006	,'FLB'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Loca��o Ve�culo
TRCell():New(oSessao4,"LOCACAO" 	,"FLB",STR0006	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao4,"FLB_NOME" 	,"FLB",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao4,"FLB_TIPVEI" 	,"FLB",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao4,"FLB_DIARIA" 	,"FLB",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao4,"FLB_DRETIR" 	,"FLB",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao4,"FLB_DDEVOL" 	,"FLB",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
										
										 //"Adiantamentos"
oSessao5 := TRSection():New( oReport	,STR0007	,'FLD'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Adiantamentos
TRCell():New(oSessao5,"ADIANTAMENTO","FLD",STR0007	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao5,"RD0_NOME" 	,"FLD",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao5,"FLD_VALOR" 	,"FLD",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao5,"FLD_DTSOLI" 	,"FLD",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao5,"FLD_DTPAGT" 	,"FLD",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao5,"FLD_STATUS" 	,"FLD",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  

										///"Presta��o de Contas"
oSessao6 := TRSection():New( oReport	,STR0008,'FLF'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Presta��o de Contas
TRCell():New(oSessao6,"PRESTCONTAS"	,"FLF",STR0008	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao6,"RD0_NOME" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao6,"FLF_TDESP1" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao6,"FLF_TDESP2" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao6,"FLF_TDESP3" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
TRCell():New(oSessao6,"FLF_TVLRE1" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao6,"FLF_TVLRE2" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao6,"FLF_TVLRE3" 	,"FLF",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)
TRCell():New(oSessao6,"FLF_STATUS" 	,"FLF",			,	    	,		,		, {|| FINR665STA(FLF_STATUS)}		,		,.T.			,				,			,			,.T.			,			,			,		)  
  													
 										///"Confer�ncia de Servi�os"
oSessao7 := TRSection():New( oReport	,STR0009	,'FLQ'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Confer�ncia de Servi�os
TRCell():New(oSessao7,"CONFSERV" 	,"FLQ", STR0009 ,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao7,"A2_NOME"    	,"FLQ",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao7,"FLQ_DATA"   	,"FLQ",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao7,"FLQ_TOTAL"  	,"FLQ",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao7,"FLQ_TPPGTO" 	,"FLQ",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao7,"DOCUMENTO"  	,"FLQ",	STR0012	,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		)  
										 ///"Documento"
										 
										 ///"Centro de Custo"
oSessao8 := TRSection():New( oReport	,STR0010,'FLH'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Centro de Custo
TRCell():New(oSessao8,"CCUSTO"    	,"FLH",STR0010	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao8,"FLH_CC"		,"FLH",			,			,		,		,		,"LEFT"	,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao8,"FLH_PORCEN"	,"FLH",			,			,		,		,		,		,.T.			,				,			,			,.T.			,			,			,		) 

										//"Aprovadores"
oSessao9 := TRSection():New( oReport	,STR0011	,'FLJ'	,		,			,			,			,.F.			,				,				,			,			,				,			,			,			,				,				,		,			,			,			) //Aprovadores
TRCell():New(oSessao9,"APROVADORES"	,"FLJ",STR0011	,			,		,		,		,"LEFT" ,.T.			,				,			,			,.T.			,			,	CLR_RED		,	.T.	)
TRCell():New(oSessao9,"FLJ_NOME"	,"FLJ",			,			,		,		,		,"LEFT"	,.T.			,				,			,			,.T.			,			,			,		) 
TRCell():New(oSessao9,"FLJ_EMAIL"	,"FLJ",			,			,		,		,		,"LEFT"	,.T.			,				,			,			,.T.			,			,			,		) 


Return  oReport

/*/{Protheus.doc} PrintReport
Realiza Rotina de impressao de dados  

@author Alexandre Felicio
@since 04/09/2015
@version P12 R12.1.7

@return Nil
/*/
Static Function PrintReport(oReport,cPerg) 
Local oFontC12S  := TFont():New("Arial      ",12,12,.T.,.T.,,,,,.F.) 	  //NEGRITO n�o SUBLINHADO   
Local cAlsFL5  := GetNextAlias()
Local cAlsFL7  := GetNextAlias()
Local cAlsFL9  := GetNextAlias()
Local cAlsFL8  := GetNextAlias()
Local cAlsFLB  := GetNextAlias()
Local cAlsFLD  := GetNextAlias()
Local cAlsFLF  := GetNextAlias()
Local cAlsFLQ  := GetNextAlias()
Local cAlsFLH  := GetNextAlias()
Local cAlsFLJ  := GetNextAlias()
Local oSection   := oReport:Section(1)
Local oSessao1	 := oReport:Section(2)
Local oSessao2	 := oReport:Section(3)
Local oSessao3	 := oReport:Section(4)
Local oSessao4	 := oReport:Section(5)
Local oSessao5	 := oReport:Section(6)
Local oSessao6	 := oReport:Section(7)
Local oSessao7	 := oReport:Section(8)
Local oSessao8	 := oReport:Section(9)
Local oSessao9	 := oReport:Section(10)
Local cFilFL5    := ''
Local cFilFL7    := ''
Local cFilFL9    := ''
Local cFilFL8    := ''
Local cFilFLB    := ''
Local cFilFLD    := ''
Local cFilFLF    := ''
Local cFilFLQ    := ''
Local cFilFLH    := ''
Local cFilFLJ    := ''
Local cTmpFL5Fil := ''
Local cTmpFL7Fil := ''
Local cTmpFL9Fil := ''
Local cTmpFL8Fil := ''
Local cTmpFLBFil := ''
Local cTmpFLDFil := ''
Local cTmpFLFFil := ''
Local cTmpFLQFil := ''
Local cTmpFLHFil := ''
Local cTmpFLJFil := ''
Local cRngFilFL5 := ''
Local cRngFilFL7 := ''
Local cRngFilFL9 := ''
Local cRngFilFL8 := ''
Local cRngFilFLB := ''
Local cRngFilFLD := ''
Local cRngFilFLF := ''
Local cRngFilFLQ := ''
Local cRngFilFLH := ''
Local cRngFilFLJ := ''
Local cWhere	 := ""
Local cViag 	 := ""
Local cNextV     := ''

If Empty(aSelFil)
	aSelFil := {cFilAnt}
	cFilFL5 := " FL5.FL5_FILIAL = '"+ xFilial("FL5") + "' AND "	
	cFilFL7 := " FL7.FL7_FILIAL = '"+ xFilial("FL7") + "' AND "
	cFilFL9 := " FL9.FL9_FILIAL = '"+ xFilial("FL9") + "' AND "
	cFilFL8 := " FL8.FL8_FILIAL = '"+ xFilial("FL8") + "' AND "	
	cFilFLB := " FLB.FLB_FILIAL = '"+ xFilial("FLB") + "' AND "
	cFilFLD := " FLD.FLD_FILIAL = '"+ xFilial("FLD") + "' AND "	
	cFilFLF := " FLF.FLF_FILIAL = '"+ xFilial("FLF") + "' AND "
	cFilFLQ := " FLQ.FLQ_FILIAL = '"+ xFilial("FLQ") + "' AND "
	cFilFLH := " FLH.FLH_FILIAL = '"+ xFilial("FLH") + "' AND "
	cFilFLJ := " FLJ.FLJ_FILIAL = '"+ xFilial("FLJ") + "' AND "
Else
	cRngFilFL5 := GetRngFil(aSelFil,"FL5",.T.,@cTmpFL5Fil)
	cRngFilFL7 := GetRngFil(aSelFil,"FL7",.T.,@cTmpFL7Fil)
	cRngFilFL9 := GetRngFil(aSelFil,"FL9",.T.,@cTmpFL9Fil)
	cRngFilFL8 := GetRngFil(aSelFil,"FL8",.T.,@cTmpFL8Fil)
	cRngFilFLB := GetRngFil(aSelFil,"FLB",.T.,@cTmpFLBFil)
	cRngFilFLD := GetRngFil(aSelFil,"FLD",.T.,@cTmpFLDFil)
	cRngFilFLF := GetRngFil(aSelFil,"FLF",.T.,@cTmpFLFFil)
	cRngFilFLQ := GetRngFil(aSelFil,"FLQ",.T.,@cTmpFLQFil)
	cRngFilFLH := GetRngFil(aSelFil,"FLH",.T.,@cTmpFLHFil)
	cRngFilFLJ := GetRngFil(aSelFil,"FLJ",.T.,@cTmpFLJFil)
	
	cFilFL5 := " FL5.FL5_FILIAL "+ cRngFilFL5 + " AND "
	cFilFL7 := " FL7.FL7_FILIAL "+ cRngFilFL7 + " AND "
	cFilFL9 := " FL9.FL9_FILIAL "+ cRngFilFL9 + " AND "
	cFilFL8 := " FL8.FL8_FILIAL "+ cRngFilFL8 + " AND "
	cFilFLB := " FLB.FLB_FILIAL "+ cRngFilFLB + " AND "
	cFilFLD := " FLD.FLD_FILIAL "+ cRngFilFLD + " AND "	
	cFilFLF := " FLF.FLF_FILIAL "+ cRngFilFLF + " AND "
	cFilFLQ := " FLQ.FLQ_FILIAL "+ cRngFilFLQ + " AND "
	cFilFLH := " FLH.FLH_FILIAL "+ cRngFilFLH + " AND "
	cFilFLJ := " FLJ.FLJ_FILIAL "+ cRngFilFLJ + " AND "
Endif

cFilFL5 := "%"+cFilFL5+"%"
cFilFL7 := "%"+cFilFL7+"%"
cFilFL9 := "%"+cFilFL9+"%"
cFilFL8 := "%"+cFilFL8+"%"
cFilFLB := "%"+cFilFLB+"%"
cFilFLD := "%"+cFilFLD+"%"
cFilFLF := "%"+cFilFLF+"%"
cFilFLQ := "%"+cFilFLQ+"%"
cFilFLH := "%"+cFilFLH+"%"
cFilFLJ := "%"+cFilFLJ+"%"

If MV_PAR07 == 2 //Nacional
	cWhere += "AND FL5.FL5_NACION = '1' "
ElseIf MV_PAR07 == 3 //Internacional
	cWhere += "AND FLF.FLF_NACION = '2' "
EndIf

If MV_PAR18 == 1 //Em Aberto
	cWhere += "AND FLF.FLF_STATUS not in ( '8', '9') "  ///  8 e 9  - Finalizados e Encerrados
ElseIf MV_PAR18 == 2 //Encerrado
	cWhere += "AND FLF.FLF_STATUS in ( '8', '9') "	 
EndIf
cWhere := "%" + cWhere + "%"

MakeSqlExp(cPerg)
 
///Query principal aonde sao considerados os Parametros do Relat�rio, exce��o a Filial que tamb�m � considerada nas demais queries abaixo.
BEGIN REPORT QUERY oSection //// Viagem

BeginSql alias cAlsFL5//// Viagem
	SELECT
		Distinct FL5.FL5_FILIAL,FL5.FL5_VIAGEM, FL5.FL5_DESORI, FL5.FL5_DESDES, FL5.FL5_DTINI, FL5.FL5_DTFIM , FL5.FL5_STATUS ,
		SA1.A1_NOME
	FROM
		%table:FL5% FL5 ,                    
		%table:SA1% SA1 ,
		%table:FLJ% FLJ ,
		%table:FLH% FLH ,
		%table:FLF% FLF ,
		%table:FL6% FL6 
	WHERE %exp:cFilFL5%	   		
        FL5.FL5_CLIENT  = SA1.A1_COD 
	and FL5.FL5_LOJA    = SA1.A1_LOJA
	and FLH.FLH_VIAGEM  = FL5.FL5_VIAGEM
	and FLF.FLF_VIAGEM  = FL5.FL5_VIAGEM
	
	and FL5.FL5_VIAGEM BETWEEN %exp:MV_PAR01% AND %exp:MV_PAR02%
	and FLF.FLF_PARTIC BETWEEN %exp:MV_PAR22% AND %exp:MV_PAR23%
	and FL5.FL5_DTINI  BETWEEN %exp:MV_PAR03% AND %exp:MV_PAR04%
	and FL5.FL5_DTFIM  BETWEEN %exp:MV_PAR05% AND %exp:MV_PAR06%
	and FLJ.FLJ_PARTIC BETWEEN %exp:MV_PAR08% AND %exp:MV_PAR09%
	and FL5.FL5_CLIENT BETWEEN %exp:MV_PAR10% AND %exp:MV_PAR12%
	and FL5.FL5_LOJA   BETWEEN %exp:MV_PAR11% AND %exp:MV_PAR13%
	and FLH.FLH_CC     BETWEEN %exp:MV_PAR14% AND %exp:MV_PAR15%
	and FL6.FL6_PARTSO BETWEEN %exp:MV_PAR16% AND %exp:MV_PAR17%
	and FL5.%notDel% 
	and SA1.%notDel%
	and FLJ.%notDel% 
	and FLH.%notDel% 
	and FLF.%notDel%
	and FL6.%notDel%
	%exp:cWhere%	
	

EndSql

END REPORT QUERY oSection//// Viagem
              
oReport:SetMeter((cAlsFL5)->(RecCount()))

/// a partir da query principal, aonde se encontram as viagens, as demais tabelas(se��es do Relat�rio) s�o filtradas a partir da VIAGEM posicionada, nas FILIAIS selecionadas ou logada.
(cAlsFL5)->(DbGoTop())
While (cAlsFL5)->(!EOF()) //// Viagem
    
    If !Empty(cNextV)
       oSection:Finish()
       oSection:SetPageBreak(.T.)  
    endif 
    
    ///armazena a Viagem para filtrar as queries abaixo
    cViag := ''
    cViag += "'" + (cAlsFL5)->FL5_VIAGEM + "'"
    cViag := "%" + cViag + "%"
   
    oSection:Init()
	oReport:IncMeter()
	oReport:SkipLine()
     
    oSection:PrintLine(.T.) 
    
    /////Seleciona registros(A�reo) para aquela viagem/Filial dentro do la�o(while)
    BEGIN REPORT QUERY oSessao1//// A�reo     

	BeginSql alias cAlsFL7//// A�reo
		SELECT
			FL7.FL7_VIAGEM, FL7.FL7_NOME, FL7.FL7_ORIGEM, FL7.FL7_DESTIN, FL7.FL7_DSAIDA, FL7.FL7_DCHEGA
		FROM
			%table:FL7% FL7
		WHERE  %exp:cFilFL7%
		 		FL7.%notDel%			
		AND FL7.FL7_VIAGEM = %exp:cViag%							
	EndSql
	
	END REPORT QUERY oSessao1//// A�reo
    
    oSessao1:Init()
    (cAlsFL7)->(DbGoTop())
    While (cAlsFL7)->(!EOF()) //// A�reo        
            oSessao1:PrintLine(.T.)
            cNextV := '*'   
	    (cAlsFL7)->(DBSKIP())        
	ENDDO  
    oSessao1:Finish()

    oReport:IncMeter()		
    oReport:ThinLine()	
    
    /////Seleciona registros(Hotel) para aquela viagem/Filial dentro do la�o(while)
    BEGIN REPORT QUERY oSessao2//// Hotel      

	BeginSql alias cAlsFL9//// Hotel
		SELECT
				FL9.FL9_VIAGEM,FL9.FL9_NOME,FL9.FL9_DIARIA, FL9.FL9_DCHKIN ,FL9.FL9_HCHKIN ,FL9.FL9_DCHKOU ,FL9.FL9_HCHKOU
		FROM
			%table:FL9% FL9  
		WHERE  %exp:cFilFL9%
		    	FL9.%notDel%
		AND     FL9.FL9_VIAGEM = %exp:cViag%
	EndSql
	
	END REPORT QUERY oSessao2//// Hotel    
    
    oSessao2:Init()
    (cAlsFL9)->(DbGoTop())
    While (cAlsFL9)->(!EOF()) //// Hotel        
            oSessao2:PrintLine(.T.) 
            cNextV := '*'
	    (cAlsFL9)->(DBSKIP())        
	ENDDO  
    oSessao2:Finish()
    
    /////Seleciona registros(Rodovi�rio) para aquela viagem/Filial dentro do la�o(while)
	BEGIN REPORT QUERY oSessao3//// Rodovi�rio      
	
	BeginSql alias cAlsFL8//// Rodovi�rio
		SELECT
			FL8.FL8_VIAGEM,FL8.FL8_NOME,FL8.FL8_ORIGEM,FL8.FL8_DESTIN,FL8.FL8_DSAIDA,FL8.FL8_DCHEGA
		FROM
			%table:FL8% FL8  
		WHERE  %exp:cFilFL8%
		 		FL8.%notDel%
		AND     FL8.FL8_VIAGEM = %exp:cViag%
	EndSql
	
	END REPORT QUERY oSessao3//// Rodovi�rio    
    
    oSessao3:Init()
    (cAlsFL8)->(DbGoTop())
    While (cAlsFL8)->(!EOF()) //// Rodovi�rio
            oSessao3:PrintLine(.T.)
            cNextV := '*' 
	    (cAlsFL8)->(DBSKIP())        
	ENDDO  
    oSessao3:Finish()
        
    /////Seleciona registros(Loca��o de Veiculos) para aquela viagem/Filial dentro do la�o(while)     
    BEGIN REPORT QUERY oSessao4//// Loca��o de Veiculos      

	BeginSql alias cAlsFLB////  Loca��o de Veiculos    
		SELECT
			FLB.FLB_VIAGEM,FLB.FLB_NOME,FLB.FLB_TIPVEI,FLB.FLB_DIARIA,FLB.FLB_DRETIR,FLB.FLB_DDEVOL
		FROM
			%table:FLB% FLB  
		WHERE  %exp:cFilFLB%
		 		FLB.%notDel%
		AND     FLB.FLB_VIAGEM = %exp:cViag%	
	EndSql
	
	END REPORT QUERY oSessao4////  Loca��o de Veiculos        
        
    oSessao4:Init()
    (cAlsFLB)->(DbGoTop())
    While (cAlsFLB)->(!EOF()) ////Loca��o de Ve�culos
            oSessao4:PrintLine(.T.)
            cNextV := '*' 
	    (cAlsFLB)->(DBSKIP())        
	ENDDO  
    oSessao4:Finish()

    /////Seleciona registros(Adiantamentos) para aquela viagem/Filial dentro do la�o(while) 
    BEGIN REPORT QUERY oSessao5//// Adiantamentos

	BeginSql alias cAlsFLD////  Adiantamentos
		SELECT
				FLD.FLD_VIAGEM,RD0.RD0_NOME, FLD.FLD_VALOR, FLD.FLD_DTSOLI,FLD.FLD_DTPAGT,FLD.FLD_STATUS
		FROM
			%table:FLD% FLD ,
			%Table:RD0% RD0  
		WHERE  %exp:cFilFLD%
			RD0.RD0_FILIAL = %xfilial:RD0%
		and FLD.%notDel%
		and	RD0.%notDel%
		and	FLD.%notDel%
		and FLD.FLD_PARTIC = RD0.RD0_CODIGO
		AND FLD.FLD_VIAGEM = %exp:cViag%	
	EndSql
	
	END REPORT QUERY oSessao5////  Adiantamentos   

    oSessao5:Init()
    (cAlsFLD)->(DbGoTop())
    While (cAlsFLD)->(!EOF()) ////Adiantamentos
            oSessao5:PrintLine(.T.)   
            cNextV := '*'                     
	    (cAlsFLD)->(DBSKIP())        
	ENDDO  
    oSessao5:Finish()
    
    /////Seleciona registros(Prestacao de Contas) para aquela viagem/Filial dentro do la�o(while)
    BEGIN REPORT QUERY oSessao6//// Prestacao de Contas 

	BeginSql alias cAlsFLF//// Prestacao de Contas 
		SELECT
				RD0.RD0_NOME,FLF.FLF_VIAGEM, FLF.FLF_TDESP1, FLF.FLF_TDESP2, FLF.FLF_TDESP3,
				FLF.FLF_TVLRE1, FLF.FLF_TVLRE2,  FLF.FLF_TVLRE3, FLF.FLF_STATUS
		FROM
			%table:FLF% FLF ,
			%Table:RD0% RD0  
		WHERE  %exp:cFilFLF%
			RD0.RD0_FILIAL = %xfilial:RD0%
		and	FLF.%notDel%
		and FLF.FLF_PARTIC = RD0.RD0_CODIGO
	    AND FLF.FLF_VIAGEM = %exp:cViag%	
	EndSql
	
	END REPORT QUERY oSessao6////  Prestacao de Contas  
    
     oSessao6:Init()
    (cAlsFLF)->(DbGoTop())
    While (cAlsFLF)->(!EOF()) ////Presta��o de Contas 
            oSessao6:PrintLine(.T.) 
            cNextV := '*'
	    (cAlsFLF)->(DBSKIP())        
	ENDDO  
    oSessao6:Finish()
    
    /////Seleciona registros(Conferencia de Servicos) para aquela viagem/Filial dentro do la�o(while)
    BEGIN REPORT QUERY oSessao7//// Conferencia de Servicos

	BeginSql alias cAlsFLQ//// Conferencia de Servicos
		SELECT
			   distinct SA2.A2_NOME,FLV.FLV_VIAGEM,FLQ.FLQ_CONFER,FLQ.FLQ_DATA,FLQ.FLQ_TOTAL,FLQ.FLQ_TPPGTO,
		       Case when FLQ.FLQ_TPPGTO in ( '1','3') then FLQ.FLQ_NUMTIT ELSE FLQ.FLQ_PEDIDO  END DOCUMENTO  
		FROM
			%table:FLQ% FLQ ,
			%table:FLV% FLV ,
			%Table:SA2% SA2  
		WHERE  %exp:cFilFLQ%
			SA2.A2_FILIAL = %xfilial:SA2%
		and	FLQ.%notDel%	
		and	FLV.%notDel%
		and	SA2.%notDel%
		and FLV.FLV_FILIAL =  FLQ.FLQ_FILIAL
		and FLV.FLV_CONFER  = FLQ.FLQ_CONFER
		and FLQ.FLQ_FORNEC  = SA2.A2_COD
		and FLQ.FLQ_LOJA    = SA2.A2_LOJA
		and FLQ.FLQ_DATA    BETWEEN %exp:MV_PAR19% AND %exp:MV_PAR20%
	    AND FLV.FLV_VIAGEM = %exp:cViag%			 
		
	EndSql
	
	END REPORT QUERY oSessao7////  Conferencia de Servicos
    
    oSessao7:Init()
    (cAlsFLQ)->(DbGoTop())
    While (cAlsFLQ)->(!EOF()) ////Conferencia de Servicos
            oSessao7:PrintLine(.T.) 
            cNextV := '*'
	    (cAlsFLQ)->(DBSKIP())        
	ENDDO  
    oSessao7:Finish()
            
    /////Seleciona registros(Centro de Custo) para aquela viagem/Filial dentro do la�o(while)        
    BEGIN REPORT QUERY oSessao8//// Centro de Custo

	BeginSql alias cAlsFLH//// Centro de Custo
		SELECT
			   FLH.FLH_VIAGEM,FLH.FLH_CC,FLH.FLH_PORCEN	        
		FROM
			%table:FLH% FLH 
		WHERE  %exp:cFilFLH%
		 	FLH.%notDel%
		AND FLH.FLH_VIAGEM = %exp:cViag%	
	EndSql
	
	END REPORT QUERY oSessao8//// Centro de Custo
          
    oSessao8:Init()
    (cAlsFLH)->(DbGoTop())
    While (cAlsFLH)->(!EOF()) ////Centro de Custo
            oSessao8:PrintLine(.T.) 
            cNextV := '*'
	    (cAlsFLH)->(DBSKIP())        
	ENDDO  
    oSessao8:Finish()
    
    /////Seleciona registros(Aprovadores) para aquela viagem/Filial dentro do la�o(while)
    BEGIN REPORT QUERY oSessao9//// Aprovadores

	BeginSql alias cAlsFLJ////Aprovadores
		SELECT
			 FLJ.FLJ_VIAGEM,FLJ.FLJ_NOME,FLJ.FLJ_EMAIL       
		FROM
			%table:FLJ% FLJ 
		WHERE  %exp:cFilFLJ%
		 	FLJ.%notDel%
		AND FLJ.FLJ_VIAGEM = %exp:cViag%	
	EndSql
	
	END REPORT QUERY oSessao9////Aprovadores
	
    oSessao9:Init()
    (cAlsFLJ)->(DbGoTop())
    While (cAlsFLJ)->(!EOF()) ////Aprovadores
            oSessao9:PrintLine(.T.) 
            cNextV := '*'
	    (cAlsFLJ)->(DBSKIP())        
	ENDDO  
    oSessao9:Finish()
    
            
   (cAlsFL5)->(DBSKIP())        
ENDDO  

oSection:Finish()
(cAlsFL5)->(DbCloseArea())

Return       

/*/{Protheus.doc} FINR665STA
Fun��o para identificar a descri��o do status da presta��o de contas 

@author Alexandre Felicio
@since 04/09/2015
@version P12 R12.1.7

@return Nil
/*/
Function FINR665STA(cStatus)
Local cDescStat := ""

Default cStatus := ""

Do Case
	Case cStatus == "1"
		cDescStat := STR0013 //"Em aberto"

	Case cStatus == "2"
		cDescStat := STR0014 //"Em confer�ncia"

	Case cStatus == "3"
		cDescStat := STR0015 //"Com bloqueio"

	Case cStatus == "4"
		cDescStat := STR0016 //"Em avalia��o"

	Case cStatus == "5"
		cDescStat := STR0017 //"Reprovada"

	Case cStatus == "6"
		cDescStat := STR0018 //"Aprovada"

	Case cStatus == "7"
		cDescStat := STR0019 //"Liberado pagto"

	Case cStatus == "8"
		cDescStat := STR0020 //"Finalizada"
	
	Case cStatus == "9"
		cDescStat := STR0021 //"Encerrados"	

EndCase

Return cDescStat