#INCLUDE "PROTHEUS.CH"

//-------------------------------------------------------------------------
/*/{Protheus.doc} ClearFlagE5

Fun��o para Limpeza de Flag das tabelas FK1,FK2,FK5 e SE5.
Utilizada pelo moduto SIGACTB.

@author Luiz Henrique
@since  06/01/2020
@param  cAliasOrig, char, Tabela de origem do rastreio cont�bil.
@version 12
/*/
//-------------------------------------------------------------------------  
Function ClearFlagE5(cAliasOrig)

	Local lLimpaFlag 	:= .F.
	Local cTamIdORI  	:= Space(TAMSX3("FKA_IDORIG")[1])
	Local cIdOrig 	 	:= cTamIdORI
	Local cKeySE5		:= ""

	DEFAULT cAliasOrig := ""
	
	// Limpa flags da tabelas nos cadastros de multiplas natureza (Rateio FIN)
	If cAliasOrig $ "SEZ|SEV|SE1|SE2|SE5"
		CTBApLAMN(cAliasOrig)						
	EndIf

	/// VERIFICA REGISTROS DO SE5 RELACIONADOS A BAIXA DE TITULOS (JUROS/MULTA/DESCONTO)
	If (cAliasOrig $ "SE5|SEZ" .and. !Empty(SE5->E5_NUMERO)) .OR.  ( cAliasOrig == "SE1" .AND. Alltrim(SE1->E1_TIPO) == 'RA')

		cKeySE5 := SE5->(E5_FILIAL+E5_TIPODOC+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(E5_DATA)+E5_CLIFOR+E5_LOJA+E5_SEQ)
					
		FK1->(dbSetOrder(1)) //FK1_FILIAL, FK1_IDFK1, R_E_C_N_O_, D_E_L_E_T_
		FK2->(dbSetOrder(1)) //FK2_FILIAL, FK2_IDFK2, R_E_C_N_O_, D_E_L_E_T_
		FKA->(dbSetOrder(3)) //FKA_FILIAL, FKA_TABORI, FKA_IDORIG, R_E_C_N_O_, D_E_L_E_T_
		FK5->(dbSetOrder(1)) //FK5_FILIAL, FK5_IDMOV, R_E_C_D_E_L_
		SE5->(dbSetOrder(7)) //E5_FILIAL, E5_PREFIXO, E5_NUMERO, E5_PARCELA, E5_TIPO, E5_CLIFOR, E5_LOJA, E5_SEQ, R_E_C_N_O_, D_E_L_E_T_
		if SE5->(MsSeek(cKeySE5,.T.))

			While SE5->(!Eof()) .and.cKeySE5 == SE5->(E5_FILIAL+E5_TIPODOC+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DTOS(E5_DATA)+E5_CLIFOR+E5_LOJA+E5_SEQ)

				lLimpaFlag := !Empty(SE5->E5_LA)

				If lLimpaFlag
					If (CV3->CV3_LP == '531') .And. (SE5->(RecNo()) != Int(Val(CV3->CV3_RECORI)))
						lLimpaFlag := .F.
					EndIf
				EndIf

				If lLimpaFlag

					//Baixas a Receber
					If SE5->E5_TABORI = 'FK1'
						If FK1->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))	
							If FK1->FK1_LA == "S"
								FK1->(RecLock("FK1",.F.))
								FK1->FK1_LA := ""
								FK1->(MsUnlock())
							EndIf
						Endif	
					//Baixas a Pagar
					ElseIf SE5->E5_TABORI = 'FK2'
						If FK2->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))
							If FK2->FK2_LA == "S"	
								FK2->(RecLock("FK2",.F.))
								FK2->FK2_LA := ""
								FK2->(MsUnlock())
							EndIf
						Endif
					//Movimenta��o bancaria.
					ElseIf SE5->E5_TABORI = 'FK5'
						//Posiciona a FKA com base no IDORIG da SE5 posicionada
						If FKA->(dbseek(SE5->E5_FILIAL+"FK5"+SE5->E5_IDORIG))	
							cIdOrig := FKA->FKA_IDORIG
						Endif

						If !Empty(cIdOrig) 
							If FK5->(dbseek(SE5->E5_FILIAL+cIdOrig))
								If FK5->FK5_LA == "S"	
									FK5->(RecLock("FK5",.F.))
									FK5->FK5_LA := ""
									FK5->(MsUnlock())
								EndIf
							Endif
						EndIf	
					EndIf
					//Atualiza a flag da SE5
					Reclock("SE5")
						REPLACE E5_LA With ''
					MsUnlock()

				EndIf

				CTBApLAMN(cAliasOrig)			// Limpa flags da tabelas nos cadastros de multiplas natureza (Rateio FIN)
				
				cIdOrig := cTamIdORI

				SE5->(dbSkip())
			EndDo
		EndIf
	EndIf	

Return


//-------------------------------------------------------------------------
/*/{Protheus.doc} CTBApLAMN

Apaga flag de contabilizacao nos movimentos com Rateio de 
Multiplas Naturezas ou Multiplas Naturezas por C.Custo 

@author Marcos S. Lobo
@since  01/28/06
@version 12
/*/
//-------------------------------------------------------------------------  

Function CTBApLAMN(cAliasPos)

	Local aAreaOri		:= GetArea()
	Local aAreaMN
	Local cFilSEV		:= xFilial("SEV")
	Local cFilSEZ		:= xFilial("SEZ")
	Local cAliasORI 	:= ""
	Local cTamIdORI 	:= Space(TAMSX3("FKA_IDORIG")[1])
	Local cTamIdPRO		:= Space(TAMSX3("FKA_IDPROC")[1])
	Local cIdOrig 		:= cTamIdORI 
	Local cIdProc		:= cTamIdPRO 
	Local cChaveSev 	:= ""
	Local cChaveSez 	:= ""

	DEFAULT cAliasPos := ""

	cAliasORI := cAliasPos

	If !Empty(cAliasPos)
		aAreaMN := (cAliasPos)->(GetArea())
	EndIf

	If cAliasPos == "SEZ"			/// SE RASTREAMENTO ESTIVER PELO SEZ LOCALIZA O T�TULO DE ORIGEM
		If SEZ->EZ_RECPAG == "R"	/// SE FOR REGISTRO DO CONTAS A RECEBER
			If SEZ->EZ_IDENT == "1" /// OPERACAO INCLUSAO
				dbSelectArea("SE1")
				dbSetorder(2)
				If MsSeek(xFilial("SE1")+SEZ->(EZ_CLIfOR+EZ_LOJA+EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO),.F.)
					If !Empty(SE1->E1_LA)
						RecLock("SE1",.F.)
						E1_LA := ""
						SE1->(MsUnlock())
					EndIf
					cAliasPos := "SE1"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
				EndIf

			ElseIf SEZ->EZ_IDENT == "2"	/// OPERACAO DE BAIXA
				dbSelectArea("SE5")
				dbSetorder(7)
				cChaveE5 := xFilial("SE5")+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA)
				If !Empty(SEZ->EZ_SEQ)
					cChaveE5+=SEZ->EZ_SEQ
				EndIf
				
				If MsSeek(cChaveE5,.F.)	// FALTA RECPAG NA CHAVE SE5 (P/ NAO MISTURAR BX CP. E CR.)	

					//Posiciona a FK1 com base no IDORIG da SE5 posicionada

					FK1->(dbSelectArea("FK1"))							
					FK1->(dbSetOrder(1))
					If FK1->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))
						If FK1->FK1_LA == "S"	
							FK1->(RecLock("FK1",.F.))
							FK1->FK1_LA := ""
							FK1->(MsUnlock())
						EndIf
					Endif	

					//Posiciona a FKA com base no IDORIG da SE5 posicionada
					FKA->(dbSelectArea("FKA"))							
					FKA->(dbSetOrder(3))
					If FKA->(dbseek(SE5->E5_FILIAL+"FK1"+SE5->E5_IDORIG))	
						cIdProc := FKA->FKA_IDPROC
					Endif	
					
					FKA->(dbSetOrder(2))
					If FKA->(dbseek(FKA->FKA_FILIAL+cIdProc))

						While !EOF() .AND. FKA->FKA_IDPROC == cIdProc

							If FKA->FKA_TABORI == "FK5"
								cIdOrig := FKA->FKA_IDORIG
							Endif
							FKA->(dbskip())

						Enddo 
					
					EndIf

						//Posiciona a FK5 com base no IDORIG da FKA posicionada
					If !Empty(cIdOrig) 
						FK5->(dbSelectArea("FK5"))							
						FK5->(dbSetOrder(1))
						If FK5->(dbseek(SE5->E5_FILIAL+cIdOrig))
							If FK5->FK5_LA == "S"		
								FK5->(RecLock("FK5",.F.))
								FK5->FK5_LA := ""
								FK5->(MsUnlock())
							EndIf
						Endif
					Endif	

					cAliasPos := "SE5"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
					cIdOrig := cTamIdORI 
					cIdProc	:= cTamIdPRO
				EndIf

			EndIf
		Else
			If SEZ->EZ_IDENT == "1" /// OPERACAO INCLUSAO
				dbSelectArea("SE2")
				dbSetorder(1)
				If MsSeek(xFilial("SE2")+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA),.F.)
					RecLock("SE2",.F.)
					E2_LA := ""
					SE2->(MsUnlock())
					cAliasPos := "SE2"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
				EndIf

			ElseIf SEZ->EZ_IDENT == "2"	/// OPERACAO DE BAIXA
				dbSelectArea("SE5")
				dbSetorder(7)
				cChaveE5 := xFilial("SE5")+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA)
				If !Empty(SEZ->EZ_SEQ)
					cChaveE5+=SEZ->EZ_SEQ
				EndIf
				If MsSeek(cChaveE5,.F.)	// FALTA RECPAG NA CHAVE SE5 (P/ NAO MISTURAR BX CP. E CR.)

					//Posiciona a FK2 com base no IDORIG da SE5 posicionada
					FK2->(dbSelectArea("FK2"))							
					FK2->(dbSetOrder(1))
					If FK2->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))
						If FK2->FK2_LA == "S"	
							FK2->(RecLock("FK2",.F.))
							FK2->FK2_LA := ""
							FK2->(MsUnlock())
						EndIf
					Endif	

					//Posiciona a FKA com base no IDORIG da SE5 posicionada
					FKA->(dbSelectArea("FKA"))							
					FKA->(dbSetOrder(3))
					If FKA->(dbseek(SE5->E5_FILIAL+"FK2"+SE5->E5_IDORIG))	
						cIdProc := FKA->FKA_IDPROC
					Endif	
					
					FKA->(dbSetOrder(2))
					If FKA->(dbseek(FKA->FKA_FILIAL+cIdProc))

						While !EOF() .AND. FKA->FKA_IDPROC == cIdProc

							If FKA->FKA_TABORI == "FK5"
								cIdOrig := FKA->FKA_IDORIG
							Endif
							FKA->(dbskip())

						Enddo 

					EndIf

					//Posiciona a FK5 com base no IDORIG da FKA posicionada
					If !Empty(cIdOrig) 
						FK5->(dbSelectArea("FK5"))							
						FK5->(dbSetOrder(1))
						If FK5->(dbseek(SE5->E5_FILIAL+cIdOrig))
							If FK5->FK5_LA == "S"		
								FK5->(RecLock("FK5",.F.))
								FK5->FK5_LA := ""
								FK5->(MsUnlock())
							EndIf
						Endif
					Endif	

					cAliasPos := "SE5"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
					cIdOrig := cTamIdORI 
					cIdProc	:= cTamIdPRO
				EndIf

			EndIf
		EndIf
	ElseIf cAliasPos == "SEV"			/// SE RASTREAMENTO ESTIVER PELO SEV LOCALIZA O T�TULO DE ORIGEM
		If SEV->EV_RECPAG == "R"	/// SE FOR REGISTRO DO CONTAS A RECEBER
			If SEV->EV_IDENT == "1" /// OPERACAO INCLUSAO
				dbSelectArea("SE1")
				dbSetorder(2)
				If MsSeek(xFilial("SE1")+SEV->(EV_CLIfOR+EV_LOJA+EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO),.F.)
					RecLock("SE1",.F.)
					E1_LA := ""
					SE1->(MsUnlock())
					cAliasPos := "SE1"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
				EndIf

			ElseIf SEV->EV_IDENT == "2"	/// OPERACAO DE BAIXA
				dbSelectArea("SE5")
				dbSetorder(7)
				cChaveE5 := xFilial("SE5")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA)
				If !Empty(SEV->EV_SEQ)
					cChaveE5+=SEV->EV_SEQ
				EndIf
				If MsSeek(cChaveE5,.F.)			/// FALTA RECPAG NA CHAVE SE5 (P/ NAO MISTURAR BX CP. E CR.)

					//Posiciona a FK1 com base no IDORIG da SE5 posicionada
					FK1->(dbSelectArea("FK1"))							
					FK1->(dbSetOrder(1))
					If FK1->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))
						If FK1->FK1_LA == "S"		
							FK1->(RecLock("FK1",.F.))
							FK1->FK1_LA := ""
							FK1->(MsUnlock())
						EndIf
					Endif	

					//Posiciona a FKA com base no IDORIG da SE5 posicionada
					FKA->(dbSelectArea("FKA"))							
					FKA->(dbSetOrder(3))
					If FKA->(dbseek(SE5->E5_FILIAL+"FK1"+SE5->E5_IDORIG))	
						cIdProc := FKA->FKA_IDPROC
					Endif	
					
					FKA->(dbSetOrder(2))
					If FKA->(dbseek(FKA->FKA_FILIAL+cIdProc))

						While !EOF() .AND. FKA->FKA_IDPROC == cIdProc

							If FKA->FKA_TABORI == "FK5"
								cIdOrig := FKA->FKA_IDORIG
							EndIf
							FKA->(dbskip())

						Enddo 	

					EndIf

					//Posiciona a FK5 com base no IDORIG da FKA posicionada

					If !Empty(cIdOrig) 
						FK5->(dbSelectArea("FK5"))							
						FK5->(dbSetOrder(1))
						If FK5->(dbseek(SE5->E5_FILIAL+cIdOrig))
							If FK5->FK5_LA == "S"		
								FK5->(RecLock("FK5",.F.))
								FK5->FK5_LA := ""
								FK5->(MsUnlock())
							EndIf
						EndIf
					EndIf	

					cAliasPos := "SE5"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
					cIdOrig := cTamIdORI 
					cIdProc	:= cTamIdPRO
				EndIf

			EndIf
		Else
			If SEV->EV_IDENT == "1" /// OPERACAO INCLUSAO
				dbSelectArea("SE2")
				dbSetorder(1)
				If MsSeek(xFilial("SE2")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA),.F.)
					RecLock("SE2",.F.)
					E2_LA := ""
					SE2->(MsUnlock())
					cAliasPos := "SE2"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
				EndIf

			ElseIf SEV->EV_IDENT == "2"	/// OPERACAO DE BAIXA
				dbSelectArea("SE5")
				dbSetorder(7)
				cChaveE5 := xFilial("SE5")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA)
				If !Empty(SEV->EV_SEQ)
					cChaveE5+=SEV->EV_SEQ
				EndIf
				If MsSeek(cChaveE5)			/// FALTA RECPAG NA CHAVE SE5 (P/ NAO MISTURAR BX CP. E CR.)

					//Posiciona a FK2 com base no IDORIG da SE5 posicionada
					FK2->(dbSelectArea("FK2"))							
					FK2->(dbSetOrder(1))
					If FK2->(dbseek(SE5->E5_FILIAL+SE5->E5_IDORIG))
						If FK2->FK2_LA == "S"	
							FK2->(RecLock("FK2",.F.))
							FK2->FK2_LA := ""
							FK2->(MsUnlock())
						EndIf
					Endif	

					//Posiciona a FKA com base no IDORIG da SE5 posicionada
					FKA->(dbSelectArea("FKA"))							
					FKA->(dbSetOrder(3))
					If FKA->(dbseek(SE5->E5_FILIAL+"FK2"+SE5->E5_IDORIG))	
						cIdProc := FKA->FKA_IDPROC
					Endif	
					
					FKA->(dbSetOrder(2))
					If FKA->(dbseek(FKA->FKA_FILIAL+cIdProc))

						While !EOF() .AND. FKA->FKA_IDPROC == cIdProc

							If FKA->FKA_TABORI == "FK5"
								cIdOrig := FKA->FKA_IDORIG
							Endif
							FKA->(dbskip())

						Enddo

					EndIF
					
					//Posiciona a FK5 com base no IDORIG da FKA posicionada
					If !Empty(cIdOrig) 
						FK5->(dbSelectArea("FK5"))							
						FK5->(dbSetOrder(1))
						If FK5->(dbseek(SE5->E5_FILIAL+cIdOrig))
							If FK5->FK5_LA == "S"		
								FK5->(RecLock("FK5",.F.))
								FK5->FK5_LA := ""
								FK5->(MsUnlock())
							EndIf
						Endif
					EndIf	

					cAliasPos := "SE5"	/// REMARCA TODOS REGISTROS DE MULT.NATUREZA RELACIONADOS AO T�TULO
					cIdOrig := cTamIdORI 
					cIdProc	:= cTamIdPRO
				EndIf		                                                                                   // |
			EndIf                                                                                              // |
		EndIf                                                                                                  // |
	EndIf		/// If DE MULTIPLA NATUREZA ESTA NA MESMA LINHA (CASO HABILITAR REMARCA��O DE TODOS SEV E SEZ)----|

	
	If cAliasPos == "SE1"				/// Rateio no Contas a Receber
	
		cChaveSev := RetChaveSev("SE1")
		cChaveSez := RetChaveSev("SE1",,"SEZ")

		DbSelectArea("SEV")
		// Se utiliza multiplas naturezas, contabiliza pelo SEV
		If SE1->E1_MULTNAT=="1" .And. SEV->(MsSeek(cChaveSev))
			DbSelectArea("SEV")
			dbSetOrder(2)
			While SEV->(!Eof()) .and.;
				cFilSEV+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA+EV_IDENT) == ;
				cChaveSev+"1"

				If SEV->EV_RATEICC == "1" // Rateou multinat por c.custo

					dbSelectArea("SEZ")
					dbSetOrder(4)
					MsSeek(cChaveSeZ+SEV->EV_NATUREZ) // Posiciona no arquivo de Rateio C.Custo da MultiNat

					While SEZ->(!Eof()) .and.;
						cFilSEZ+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT);
						== cChaveSeZ+SEV->EV_NATUREZ+"1"

						If !Empty(SEZ->EZ_LA)
							RecLock("SEZ")
							SEZ->EZ_LA := ""
							SEZ->(MsUnlock())
						EndIf

						dbSkip()
					Enddo

					DbSelectArea("SEV")

				EndIf

				If !Empty(SEV->EV_LA)
					RecLock("SEV")
					SEV->EV_LA := ""
					SEV->(MsUnlock())
				EndIf

				DbSelectArea("SEV")
				DbSkip()
			Enddo
		EndIf
	
	ElseIf cAliasPos == "SE2"			/// Rateio no Contas a Pagar
	
		If SE2->E2_RATEIO != "S"
			cChaveSev := RetChaveSev("SE2")
			cChaveSeZ := RetChaveSev("SE2",,"SEZ")
			DbSelectArea("SEV")
			// Se utiliza multiplas naturezas, contabiliza pelo SEV
			If SE2->E2_MULTNAT == "1" .And. MsSeek(cChaveSev)
				DbSelectArea("SEV")
				dbSetOrder(2)
				While SEV->(!Eof()) .and.;
						cFilSEV+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA+EV_IDENT);
						== cChaveSev+"1"

					If SEV->EV_RATEICC == "1" // Rateou multinat por c.custo

						dbSelectArea("SEZ")
						dbSetOrder(4)
						MsSeek(cChaveSeZ+SEV->EV_NATUREZ) // Posiciona no arquivo de Rateio C.Custo da MultiNat

						While SEZ->(!Eof()) .and.;
							cFilSEZ+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT);
							== cChaveSeZ+SEV->EV_NATUREZ+"1"

							If !Empty(SEZ->EZ_LA)
								RecLock("SEZ")
								SEZ->EZ_LA := ""
								SEZ->(MsUnlock())
							EndIf
							dbSkip()
						Enddo
					EndIf

					If !Empty(SEV->EV_LA)
						RecLock("SEV")
						SEV->EV_LA := ""
						SEV->(MsUnlock())
					EndIf

					dbSelectArea("SEV")
					DbSkip()
				Enddo
			EndIf
		EndIf
	
	ElseIf cAliasPos == "SE5"			/// Rateio nas Baixas e Mov.Bancario
	
		If SE5->E5_MULTNAT == "1"

			cSeqSE5	:= SE5->E5_SEQ

			/////////////////////////////////////////////////////////////////////////////////////////////
			If SE5->E5_RECPAG == "R" 		/// Movimentos de Baixas e Mov. Bancario Receber
			/////////////////////////////////////////////////////////////////////////////////////////////
				lAdiant 	:= .f.
				lEstorno 	:= .F.
				lEstRaNcc 	:= .F.
				lCompens 	:= .F.

				If SE5->E5_TIPODOC == "ES"
					lEstorno := .T.
				EndIf
				If SE5->E5_TIPODOC == "ES" .and. SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG
					lEstRaNcc := .T.
				EndIf
				If SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG
					lAdiant := .T.
				EndIf
				If  SE5->E5_TIPODOC == "BA" .and. SE5->E5_MOTBX == "CMP"
					lCompens := .T.
				EndIf

				If (lAdiant .or. lEstorno) .and. !lEstRaNcc
					dbSelectArea("SE2")
					dbSetOrder(1)
					MsSeek(xFilial("SE2")+SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIfOR+E5_LOJA),.T.)
				Else
					dbSelectArea( "SE1" )
					dbSetOrder(2)
					cFilorig := xFilial("SE1")
					If lCompens
						If !Empty(xFilial("SE5"))
							If !Empty(SE5->E5_FILORIG)
								cFilOrig := SE5->E5_FILORIG
							EndIf
						EndIf
					EndIf

					MsSeek(cFilOrig+SE5->(E5_CLIfOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO),.T.)
				EndIf

				If Found()
					If lEstorno
						cChaveSev := RetChaveSev("SE2")+"2"+cSeqSE5
						cChaveSez := RetChaveSev("SE2",,"SEZ")
					Else
						cChaveSev := RetChaveSev("SE1")+"2"+cSeqSE5
						cChaveSez := RetChaveSev("SE1",,"SEZ")
					EndIf

					DbSelectArea("SEV")
					dbSetOrder(2)
					// Se utiliza multiplas naturezas, contabiliza pelo SEV
					If MsSeek(cChaveSev)

						DbSelectArea("SEV")
						While SEV->(!Eof()) .and.;
							cFilSEV+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA+EV_IDENT+EV_SEQ);
							== cChaveSev

							//Se estou contabilizando um estorno, trata-se de um C. Pagar,
							//So contabiliza EV_SITUACA == E
							If (lEstorno .and. !(SEV->EV_SITUACA == "E")) .or. ;
								(!lEstorno .and. (SEV->EV_SITUACA == "E"))
								//Se nao for um estorno, nao devo contabilizar o registro se
								//EV_SITUACA == E
								dbSkip()
								Loop
							EndIf

							If SEV->EV_RATEICC == "1" // Rateou multinat por c.custo

								dbSelectArea("SEZ")
								dbSetOrder(4)
								MsSeek(cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5) // Posiciona no arquivo de Rateio C.Custo da MultiNat

								While SEZ->(!Eof()) .and.;
									cFilSEZ+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT+EZ_SEQ);
									== cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5

									//Se estou contabilizando um estorno, trata-se de um C. Pagar,
									//So vou contabilizar os EZ_SITUACA == E
									//Se nao for um estorno, nao devo contabilizar o registro se
									//EZ_SITUACA == E
									If (lEstorno .and. !(SEZ->EZ_SITUACA == "E")) .or. ;
										(!lEstorno .and. (SEZ->EZ_SITUACA == "E"))
										dbSkip()
										Loop
									EndIf

									If !Empty(SEZ->EZ_LA)
										RecLock("SEZ")
										SEZ->EZ_LA    := ""
										MsUnlock( )
									EndIf

									dbSkip()
								Enddo
							EndIf			/// Se Rateou Mult.Natur x C.Custo

							DbSelectArea("SEV")
							If !Empty(SEV->EV_LA)
								RecLock("SEV")
								SEV->EV_LA := ""
								MsUnlock( )
							EndIf

							DbSelectArea("SEV")
							DbSkip()
						Enddo
					EndIf		/// Encontrou no SEV
				EndIf

			/////////////////////////////////////////////////////////////////////////////////////////////
			ElseIf SE5->E5_RECPAG == "P" 	/// Movimentos de Baixas e Mov. Bancario Pagar
			/////////////////////////////////////////////////////////////////////////////////////////////
				lAdiant 	:= .F.
				lEstorno 	:= .F.
				lEstPaNdf 	:= .F.
				lEstCart2 	:= .F.
				lCompens  	:= .F.

				If SE5->E5_TIPODOC == "ES"
					lEstorno := .T.
				EndIf

				If SE5->E5_TIPODOC == "E2"
					lEstCart2 := .T.
				EndIf

				If SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG .and. SE5->E5_TIPODOC == "ES"
					lEstPaNdf := .T.
				EndIf

				If SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG
					lAdiant := .T.
				EndIf

				If SE5->E5_TIPODOC == "BA" .and. SE5->E5_MOTBX == "CMP"
					lCompens := .T.
				EndIf


				If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
					dbSelectArea("SE1")
					dbSetOrder(2)
					MsSeek(xFilial("SE1")+SE5->(E5_CLIfOR+E5_LOJA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO),.T.)
				Else
					dbSelectArea( "SE2" )
					dbSetOrder(1)
					cFilorig := xFilial("SE2")
					If lCompens
						If !Empty(xFilial("SE5"))
							If !Empty(SE5->E5_FILORIG)
								cFilOrig := SE5->E5_FILORIG
							EndIf
						EndIf
					EndIf

					MsSeek( cFilOrig +SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIfOR+E5_LOJA),.T.)
				EndIf

				If Found()

					//Contabilizando estorno de C.Receber
					If lEstorno
						cChaveSev := RetChaveSev("SE1")+"2"+cSeqSE5
						cChaveSez := RetChaveSev("SE1",,"SEZ")
					Else
						cChaveSev := RetChaveSev("SE2")+"2"+cSeqSE5
						cChaveSez := RetChaveSev("SE2",,"SEZ")
					EndIf

					DbSelectArea("SEV")
					dbSetOrder(2)
					If MsSeek(cChaveSev)

						DbSelectArea("SEV")
						While SEV->(!Eof()) .and.;
							cFilSEV+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIfOR+EV_LOJA+EV_IDENT+EV_SEQ);
							== cChaveSev

							//Se estou contabilizando um estorno, trata-se de um C. Pagar,
							//So vou contabilizar os EV_SITUACA == E
							//Se nao for um estorno, nao devo contabilizar o registro se
							//EV_SITUACA == E
							If (lEstorno .and. !(SEV->EV_SITUACA == "E")) .or. ;
								(!lEstorno .and. (SEV->EV_SITUACA == "E"))
								dbSkip()
								Loop
							EndIf

							dbSelectArea("SEV")

							If SEV->EV_RATEICC == "1"  // Rateou multinat por c.custo

								dbSelectArea("SEZ")
								dbSetOrder(4)
								MsSeek(cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5) // Posiciona no arquivo de Rateio C.Custo da MultiNat

								While SEZ->(!Eof()) .and.;
									cFilSEZ+SEZ->(EZ_PREFIXO+EZ_NUM+EZ_PARCELA+EZ_TIPO+EZ_CLIfOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT+EZ_SEQ);
									== cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5

									//Se estou contabilizando um estorno, trata-se de um C. Pagar,
									//So vou contabilizar os EZ_SITUACA == E
									//Se nao for um estorno, nao devo contabilizar o registro se
									//EZ_SITUACA == E
									If (lEstorno .and. !(SEZ->EZ_SITUACA == "E")) .or. ;
										(!lEstorno .and. (SEZ->EZ_SITUACA == "E"))
										dbSkip()
										Loop
									EndIf

									If !Empty(SEZ->EZ_LA)
										RecLock("SEZ")
										SEZ->EZ_LA    := ""
										MsUnlock( )
									EndIf

									dbSkip()
								Enddo

								DbSelectArea("SEV")
							EndIf

							If !Empty(SEV->EV_LA)
								RecLock("SEV")
								SEV->EV_LA := ""
								MsUnlock()
							EndIf

							DbSelectArea("SEV")
							DbSkip()
						Enddo
					EndIf	/// Se achou SEV
				EndIf
			/////////////////////////////////////////////////////////////////////////////////////////////
			EndIf
			/////////////////////////////////////////////////////////////////////////////////////////////
		EndIf	/// Fecha If do E5_MULTNAT == '1'
	EndIf

	If !Empty(cAliasPos)
		RestArea(aAreaMN)
	EndIf
	RestArea(aAreaOri)
Return

