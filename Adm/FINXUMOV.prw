#INCLUDE 'Protheus.ch'
#INCLUDE 'FINXUMOV.CH'

#DEFINE INSERIR		1
#DEFINE FILMOV		3
#DEFINE NMOVIMENTO	3
#DEFINE SECAOUMOV		20
#DEFINE NROCOMP		33
#DEFINE DTDIGIT		34
#DEFINE BENEF			35
#DEFINE NVALOR		36
#DEFINE CNPJ			37
#DEFINE IMAGEM		38
#DEFINE HISTORICO		39
#DEFINE SLDRESTANT	41

//------------------------------------------------------------------------------------------------
/*/{Protheus.doc} FINUMOV001
Fonte com as fun��es para integra��o de processos Protheus Financeiro com a plataforma uMov.me
para o processo do caixinha de presta��o de contas de movimentos de adiantamento.

@author marylly.araujo
@since 02/10/2013
/*/
//------------------------------------------------------------------------------------------------

Function FINUMOV001(cXml)

Local aArea		:= GetArea()
Local aAreaRD0	:= {}
Local aAreaSA1 	:= {}
Local aAreaSEU	:= {}
Local cArq			:= cXml
Local cLinha		:= ""
Local aDados		:= {}
Local aCampos		:= {}
Local aEstrTmp	:= {}
Local aEstruct	:= {}
Local cEntidade	:= ""
Local bExecAuto	:= {||}
Local cError		:= ""
Local cWarning	:= ""
Local nHandle		:= 0
Local cFilSEU		:= XFILIAL("SEU")
Local lRet			:= .T.
Local cRet			:= ""
Local cInfFilial	:= cFilSEU						// No ajuste de filial do uMov.me, esta vari�veil dever� ser removida
Local nTamFilArq	:= Len(cInfFilial)			// No ajuste de filial do uMov.me, esta vari�veil dever� ser removida
Local nTamFilial	:= FWSizeFilial(cEmpAnt)
Local nTamMov		:= TamSX3("EU_NUM")[1]		// Tamanho do campo de identifica��o do movimento do caixinha
Local cCpoFilial	:= ""
Local cNumMov		:= ""
Local nRecMov		:= 0
Local cCaixinha	:= ""
Local cCxMoeda	:= ""
Local cPartic		:= ""
Local cCliente	:= ""
Local cLojaCli	:= ""
Local cHistor		:= ""
Local cNroComp	:= ""
Local cBenef		:= ""
Local nX			:= 0
Local aChave		:= {}
Local lImguMov	:= .F.

Private lMsErroAuto	:= .F.

/*
 * Passando o parser no XML recebido
 */
cXML := XmlParser(cArq,"_",@cError,@cWarning)

/*
 * Verificando se existe o atributo do caminho do arquivo de texto para importar
 */
If XmlChildEx( cXML, "_UMOVIMPORT" ) <> Nil
	cCaminho := cXML:_UmovImport:_RelativeDirectory:Text+ "\" +cXML:_UmovImport:_FileName:Text
	/*
	 * Abrindo arquivo de texto
	 */
	nHandle := FT_FUSE(cCaminho)

	/*
	 * Sem arquivo para ser importado
	 */
	If nHandle == -1
		Conout(STR0001) //"Nenhum arquivo dispon�vel para importa��o."
	Endif

	DbSelectArea("SA1") // Clientes
	aAreaSA1	:= SA1->(GetArea())
	SA1->(DbSetOrder(1)) // Filial + C�digo + Loja

	DbSelectArea("RD0") // Participantes
	aAreaRD0	:= RD0->(GetArea())
	RD0->(DbSetOrder(1)) // Filial + C�digo do Participante

	DbSelectArea("SEU") // Movimentos do Caixinha
	aAreaSEU	:= SEU->(GetArea())
	SEU->(DbSetOrder(1)) // Filial + Identifica��o do Movimento
	lImguMov := cPaisLoc == "BRA"

	While !FT_FEof()

		cLinha 		:= FT_FReadLn()
		cLinha 		:= LimpaXML(cLinha)
		aDados 		:= Separa(cLinha,";")

		If aDados[INSERIR] == "INSERT"
			cCpoFilial	:= aDados[FILMOV] // Filial do Movimento de Adiantamento
			cCpoFilial	:= AllTrim(aDados[FILMOV])//),0,nTamFilArq) // Filial do Movimento de Adiantamento
			cCpoFilial	:= PADR(cCpoFilial,nTamFilial) // Filial do Movimento de Adiantamento

			cNumMov := AllTrim(SUBSTR(aDados[NMOVIMENTO],nTamFilial))

			If SEU->(DbSeek( cCpoFilial + cNumMov ) )
				/*
				 * Somente movimentos que n�o foram cancelados por presta��o de contas feita no ERP
				 */
				If SEU->EU_ENVUMOV != '3'
					cNumMov := AllTrim(SUBSTR(aDados[NMOVIMENTO],nTamFilial))

					nRecMov	:= SEU->(RECNO())

					cCaixinha	:= SEU->EU_CAIXA
					cCxMoeda	:= SEU->EU_MOEDA
					cPartic	:= SEU->EU_PROFISS
					cCliente	:= SEU->EU_CLIENTE
					cLojaCli	:= SEU->EU_LOJACLI
					cHistor	:= SEU->EU_HISTOR
					cBenef		:= SEU->EU_BENEF
					cNroComp	:= SEU->EU_NRCOMP

					SEU->(RecLock("SEU",.T.))
					SEU->EU_FILIAL 	:= cFilSEU
					SEU->EU_CAIXA		:= cCaixinha
					SEU->EU_NROADIA	:= cNumMov
					SEU->EU_MOEDA		:= cCxMoeda
					SEU->EU_PROFISS	:= cPartic
					SEU->EU_CLIENTE	:= cCliente
					SEU->EU_LOJACLI	:= cLojaCli
					SEU->EU_FILORI	:= cFilAnt

					If AllTrim(aDados[SECAOUMOV]) == "INFODOCUMENTOS"
						SEU->EU_TIPO		:= "00" // Tipo de Movimento de Despesa
						SEU->EU_CGC		:= aDados[CNPJ]
						SEU->EU_VALOR		:= VAL(aDados[NVALOR])
						SEU->EU_NRCOMP	:= aDados[NROCOMP]
						SEU->EU_HISTOR	:= aDados[HISTORICO]
						SEU->EU_BENEF		:= cBenef
						SEU->EU_NOME		:= aDados[BENEF]
						SEU->EU_EMISSAO	:= STOD(aDados[DTDIGIT])
						SEU->EU_DTDIGIT	:= STOD(aDados[DTDIGIT])
					ElseIf AllTrim(aDados[SECAOUMOV]) == "SALDO"
						SEU->EU_TIPO		:= "02" // Tipo de Movimento de Devolu��o
						SEU->EU_VALOR		:= VAL(aDados[SLDRESTANT])
						SEU->EU_HISTOR	:= cHistor
						SEU->EU_NRCOMP	:= cNroComp
						SEU->EU_BENEF		:= cBenef
						SEU->EU_NOME		:= cBenef
						SEU->EU_EMISSAO	:= DDATABASE
						SEU->EU_DTDIGIT	:= DDATABASE
					EndIf
					SEU->EU_ENVUMOV	:= "1" // Enviado ao uMov.me
					SEU->EU_STAUMOV	:= "3" // Movimento Pendente de Classifica��o (Mobile)
					SEU->EU_STATUS	:= "03"
					SEU->EU_SEQCXA	:= Fa570SeqAtu(cCaixinha)
					SEU->EU_NUM		:= GetSxENum("SEU","EU_NUM")
					If lImguMov
						SEU->EU_IMGUMOV	:= aDados[IMAGEM]
					EndIf
					SEU->(MsUnLock())

					ConfirmSX8() // Confirma��o do Identifica��o de Numera��o Autom�tica do Movimento do Caixinha
					SEU->(DbGoTo(nRecMov))
					SEU->(RecLock("SEU",.F.))
					SEU->EU_STAUMOV	:= "3" // Atualiza��o para pendente de classifica��o de movimentos de adiantamento.
					SEU->(MsUnLock())
				EndIf
			EndIf
		EndIf

		Asize( aDados,	0)
		aDados		:= Nil

		aEstrTmp	:= {}

		FT_FSkip()
	EndDo

	Ft_fuse()	//Fecha o arquivo

	RestArea(aAreaSEU)
	RestArea(aAreaSA1)
	RestArea(aAreaRD0)
	RestArea(aArea)
ElseIf XmlChildEx( cXML, "_UMOVEXPORT" ) <> Nil
	If XmlChildEx( cXML:_UMovExport, "_DATASETNAME" ) <> Nil
		If UPPER(AllTrim(cXML:_UMovExport:_DataSetName:Text)) == 'TAREFAS'
			If XmlChildEx( cXML:_UMovExport, "_LISTOFKEYS" ) <> Nil
				If ValType(cXML:_UMovExport:_ListOfKeys:_Key) <> "A"
					XmlNode2Arr(cXML:_UMovExport:_ListOfKeys:_Key,"_Key")
	   			EndIf

	   			DbSelectArea("SEU") // Movimentos do Caixinha
				aAreaSEU	:= SEU->(GetArea())
				SEU->(DbSetOrder(1)) // Filial + Identifica��o do Movimento Relacionado

	   			For nX := 1 To Len( cXML:_UMovExport:_ListOfKeys:_Key)
					cRet := cXML:_UMovExport:_ListOfKeys:_Key[nX]:Text
					aCampos := Separa(cRet,"|")
					If SEU->(DbSeek(PADR(aCampos[1],nTamFilial) + aCampos[2]))
						While SEU->(!Eof())
							If SEU->EU_TIPO == '01' .AND. SEU->EU_STAUMOV == '1'
								SEU->(RecLock("SEU",.F.))
								SEU->EU_STAUMOV := "2" // Em Campo
								SEU->(MsUnLock())
							EndIf
							SEU->(DbSkip())
						EndDo
					EndIf
				Next nX

				RestArea(aAreaSEU)
			EndIf
		EndIf
	EndIf
EndIf

Return {lRet,cRet}


//------------------------------------------------------------------------------------------------
/* {Protheus.doc} LimpaXML()

Remove a acentua��o e caracteres especiais no texto do XML.

@Return cRet - string DO XML sem acentua��o e caracteres especiais.
@author marylly.araujo
@since 02/10/2013
*/
//------------------------------------------------------------------------------------------------

Static Function LimpaXML(cTxt)

Local cRet	:= ""
Local nX	:= 0

For nX := 1 To Len(cTxt)
	If SubStr(cTxt,nX,1) $ "������������������������������������,'~"
		If SubStr(cTxt,nX,1) == ","
			cRet += " "
		ElseIf SubStr(cTxt,nX,1) == "'"
			cRet += " "
		ElseIf SubStr(cTxt,nX,1) == "~"
			cRet += " "
		ElseIf SubStr(cTxt,nX,1) == "-"
			cRet += " "
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "C"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "A"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "C"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "E"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "I"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "O"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		ElseIf SubStr(cTxt,nX,1) == "�"
			cRet += "U"
		EndIf
	Else
		cRet += SubStr(cTxt,nX,1)
	EndIf

Next nX

Return AllTrim(cRet)