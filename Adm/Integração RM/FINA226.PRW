#INCLUDE "PROTHEUS.CH"
#INCLUDE "FINA226.CH"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � FINA226  � Autor � Cesar Augusto Bianchi � Data � 18/12/08 ���
�������������������������������������������������������������������������Ĵ��
���Objetivos �     Cadastro de Descontos Condicionais x Plno Pgto 	 	  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � FINA226()                                                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �Integracao Protheus x Classis - Contas a Receber            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � FNC  �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fina226()
Private aRotina	:= MenuDef()
Private aTela 	:= {}
Private aGets	:= {}

//��������������Ŀ
//�Exibe a browse�
//����������������
mBrowse(6, 1,22,75,"FRH",,,,,,)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MenuDef   �Autor  �Cesar A. Bianchi    � Data �  02/10/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Monta os botoes da mBrowse                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MenuDef()
Local aButtons := {}
Local aButUsr  := {}
Local nI	   := 0

aButtons 	:= {{ STR0001,"AxPesqui", 0 , 1},;     	//Pesquisar
	     	 	{ STR0002,"Fn226Mnt", 0 , 2},;			//Visualizar
    	  		{ STR0003,"Fn226Mnt", 0 , 3},;			//Incluir
      			{ STR0004,"Fn226Mnt", 0 , 4}}			//Alterar

//�������������������������������������������������Ŀ
//�Ponto de entrada para exibir os botoes de usuario�
//���������������������������������������������������
if ExistBlock("Fn226Brw")
	aButUsr := ExecBlock("Fn226Brw", .F., .F.,{})
	For nI := 1 to len(aButUsr)
		aAdd(aButtons,aButUsr[nI])
	Next nI
endif

Return(aButtons)

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Mnt  �Autor  �Cesar A. Bianchi    � Data �  02/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Visualizacao, Inclusao, Alteracao e Exclusao dos Cadastros  ���
���          �de Desconto Condicional                                     ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Mnt(cAlias,nLin,nOpc)
Local aCordW 		:= {15,000,1024,1280}
Local aButtons  	:= {}
Local aButUsr		:= {}
Local nI			:= 0
Local aHeader		:= {}
Private oFn226Mnt	:= Nil
Private aCols		:= {}
Private c226CodPln	:= ""
Private c266IdPerl	:= ""
Private	c226Descri 	:= ""
Private	c266DtIni  	:= ""
Private	c266DtFim  	:= ""
Private nOpcA		:= nOpc

//��������������������������������������������������Ŀ
//�Ponto de entrada para definir os botoes de usuario�
//����������������������������������������������������
if ExistBlock("Fn226But")
	aButUsr := ExecBlock("Fn226But", .F., .F.,{nOpc})
	For nI := 1 to len(aButUsr)
		aAdd(aButtons,aButUsr[nI])
	Next nI
endif

//���������������Ŀ
//�Monta a aHeader�
//�����������������
SX3->(dbSetOrder(1))
SX3->(dbSeek("FRI"))
While SX3->(!Eof()) .And. SX3->X3_ARQUIVO == 'FRI'
	If cNivel >= SX3->X3_NIVEL .and. X3USO(SX3->X3_USADO)
		aAdd(aHeader,{TRIM(X3Titulo()),;
					SX3->X3_CAMPO,;
					SX3->X3_PICTURE,;
					SX3->X3_TAMANHO,;
					SX3->X3_DECIMAL,;
					SX3->X3_VALID,;
					SX3->X3_USADO,;
					SX3->X3_TIPO,;
					SX3->X3_F3,;
					SX3->X3_CONTEXT,;
					SX3->X3_CBOX,;
					SX3->X3_RELACAO,;
					SX3->X3_WHEN,;
					SX3->X3_VISUAL,;
					SX3->X3_VLDUSER,;
					SX3->X3_PICTVAR})

	EndIf
	SX3->(dbSkip())
EndDo

//�������������������������������Ŀ
//�Carrega as variaveis de memoria�
//���������������������������������
RegToMemory("FRH",.T.)

//��������������������������������������������������������Ŀ
//�Se visualizacao, inclusao ou alteracao, carrega os dados�
//����������������������������������������������������������
If nOpc == 2 .or. nOpc == 4
	aCols := Fn226Load()
EndIf

//�������������������������������������������������Ŀ
//�Monta a interface do tipo Modelo3 para integracao�
//���������������������������������������������������
oFn226Mnt:= ClsMod3():New()
oFn226Mnt:cTitle		:= STR0005 //"Cadastro Descontos Condicionais - Protheus x TOTVS Educacional"
oFn226Mnt:cAliasHead	:= "FRH"
oFn226Mnt:cAliasGrid	:= "FRI"
oFn226Mnt:nOpcHead		:= Iif(nOpc==4,2,nOpc)
oFn226Mnt:aHeader		:= aHeader
oFn226Mnt:aCols			:= aCols
oFn226Mnt:nOpcGetD		:= GD_UPDATE
oFn226Mnt:cLinOk		:= "AlwaysTrue()"
oFn226Mnt:cFieldOk		:= "AlwaysTrue()"
oFn226Mnt:cTudoOk		:= "AlwaysTrue()"
oFn226Mnt:aButtons		:= aButtons
oFn226Mnt:aAltGet		:= {'FRI_PERC1','FRI_DATE1','FRI_PERC2','FRI_DATE2','FRI_PERC3','FRI_DATE3'}
oFn226Mnt:lRMHead		:= .F.
oFn226Mnt:lRMGetD		:= .F.
oFn226Mnt:bAction		:= { || Fn226Grv(nOpc) }
oFn226Mnt:bEnd			:= { || Fn226Cncel() }
oFn226Mnt:nReg			:= nLin
oFn226Mnt:Show()

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Load �Autor  � Cesar A. Bianchi   � Data �  10/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega os dados (Visualizacao / Alteracao)				  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Load()
Local aArea 	:= getArea()
Local aRet	:= {}
Local aParcRM   := {}
Local aParcDif	:= {}
Local nI		:= 0
Local cMsg		:= ""


dbSelectArea('FRI')
FRI->(dbSetOrder(1))
If FRI->(dbSeek(xFilial('FRI')+FRH->FRH_CODCOL+FRH->FRH_IDPERL+FRH->FRH_PLNPGT))
	While FRI->FRI_FILIAL == FRH->FRH_FILIAL .and. ;
	    	FRI->FRI_CODCOL == FRH->FRH_CODCOL .and. ;
	    	FRI->FRI_IDPERL == FRH->FRH_IDPERL .and. ;
			FRI->FRI_PLNPGT == FRH->FRH_PLNPGT .and. ;
			FRI->(!Eof())

		aAdd(aRet,{FRI->FRI_PARC,FRI->FRI_VALOR,FRI->FRI_VENORI,FRI->FRI_PERC1,FRI->FRI_DATE1,FRI->FRI_PERC2,FRI->FRI_DATE2,FRI->FRI_PERC3,FRI->FRI_DATE3,.F.})
		FRI->(dbSkip())
	EndDo
EndIf

//�������������������������������������Ŀ
//�Ordena o vetor por Parcela (FRI_PARC)�
//���������������������������������������
If len(aRet) > 0
	aSort(aRet,,,{|x,y| val(x[1]) < val(y[1]) })
EndIf

//�����������������������������������������������������Ŀ
//�Carrega as parcelas existentes no plano de pagamento �
//�selecionado para identificar novas parcelas          �
//�������������������������������������������������������
aParcRM := ClsParcRM(FRH->FRH_IDPERL,FRH->FRH_PLNPGT)

//�������������������������������������������������������������������Ŀ
//�Compara os dois arrays, verificando se todas as parcelas           �
//� do array aParcRM existe em aRet. Se nao existir, entao adiciona�
//���������������������������������������������������������������������
If len(aParcRM) < len(aRet)
	For nI := 1 to len(aRet)
		If aScan(aParcRM,{ |x| Alltrim(x[1]) == alltrim(aRet[nI,1])}) <= 0
			cMsg := STR0006 + alltrim(aRet[nI,1]) + STR0007 + alltrim(FRH->FRH_PLNPGT) + STR0008 //'A parcela xxx n�o foi encontrada no Cadastro de Parcelas do Plano de Pagamento: xxx '. Portanto ser� desconsiderada'
			Aviso(STR0009,cMsg,{STR0010}) //'Parcela n�o encontrada no Plano de Pagamento'
		EndIf
	Next nI
Elseif len(aParcRM) > len(aRet)
	For nI := 1 to len(aParcRM)
		If aScan(aRet,{ |x| Alltrim(x[1]) == alltrim(aParcRM[nI,1])}) <= 0
        	//Adiciona no array de mensagem, exibido ao final
        	aAdd(aParcDif,aParcRM[nI])

        	//Adiciona como uma nova parcela no aRet
   			aAdd(aRet,{aParcRM[nI,1],aParcRM[nI,2],aParcRM[nI,3],0,cTod("  /  /    ") ,0,cTod("  /  /    ") ,0,cTod("  /  /    "),.F.})
		EndIf
	Next nI
EndIf

//���������������������������������������������������������������Ŀ
//�Exibe a mensagem de alerta, com as novas parcelas identificadas�
//�����������������������������������������������������������������
If len(aParcDif) > 0
	cMsg := STR0011 //"As parcelas abaixo listadas foram identificadas como novas - (incluidas ap�s o cadastro de descontos deste plano) "
	cMsg += STR0012 //"e ser�o apresentadas ao final da lista de parcelas. "
	If nOpcA == 2
		cMsg += STR0013 //"Preencha-as corretamente antes de gravar as altera��es. "
	Elseif nOpcA == 4
		cMsg += STR0014 //"Preencha-as corretamente atrav�s do recurso 'Alterar'. "
	Endif
	cMsg += chr(13) + chr(10) + chr(13) + chr(10)

	For nI := 1 to len(aParcDif)
		cMsg += STR0015 + aParcDif[nI,1] + STR0016 + alltrim(str(aParcDif[nI,2])) + STR0017 + dToc(aParcDif[nI,3]) //"Parcela nro: xxx  Valor: R$ xxx  Vencimento Original: xxxx"
		cMsg += chr(13) + chr(10)
	Next nI

	Aviso(STR0018,cMsg,{STR0010}) //'Novas parcelas identificadas'
EndIf


RestArea(aArea)
Return aRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Grv  �Autor  � Cesar A. Bianchi   � Data �  10/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava os dados (Inclusao / Alteracao )			          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Grv(nOpc)
Local aArea 	:= getArea()
Local lExist	:= .F.
Local nI 		:= 0
Local nJ		:= 0

If empty(FRH_IDPERL)
	Aviso(STR0029,STR0030,{STR0010}) //'Preencha estes campos corretamente antes de prosseguir com a inclus�o'
	Return
EndIf

BEGIN TRANSACTION

//��������������������Ŀ
//�Inclusao de registro�
//����������������������
If nOpc == 3
	dbSelectArea('FRH')
	FRH->(dbSetOrder(1))
	lExist := FRH->(dbSeek(xFilial('FRH')+M->FRH_CODCOL+M->FRH_IDPERL+M->FRH_PLNPGT))

	//Grava Header
	RecLock('FRH',!lExist)
	FRH->FRH_FILIAL := xFilial('FRH')
	FRH->FRH_CODCOL := alltrim(str(val(SM0->M0_CODIGO)))
	FRH->FRH_IDPERL := M->FRH_IDPERL
	FRH->FRH_PLNPGT := M->FRH_PLNPGT
	FRH->FRH_DTINI  := M->FRH_DTINI
	FRH->FRH_DTFIM  := M->FRH_DTFIM
	FRH->(msUnlock())

	//Grava Itens
	For nI := 1 to len(oFn226Mnt:oGetD:aCols)
		RecLock('FRI',!lExist)
		FRI->FRI_FILIAL := xFilial('FRH')
		FRI->FRI_CODCOL := alltrim(str(val(SM0->M0_CODIGO)))
		FRI->FRI_IDPERL := M->FRH_IDPERL
		FRI->FRI_PLNPGT := M->FRH_PLNPGT
		For nJ := 1 to len(oFn226Mnt:oGetD:aHeader)
			If FieldPos(oFn226Mnt:oGetD:aHeader[nJ,2]) > 0
				FieldPut(FieldPos(oFn226Mnt:oGetD:aHeader[nJ,2]), oFn226Mnt:oGetD:aCols[nI,nJ] )
			EndIf
		Next nJ
		FRI->(msUnlock())
	Next nI

//���������������������Ŀ
//�Alteracao de registro�
//�����������������������
ElseIF nOpc == 4

	//������������������������������������������������������������Ŀ
	//�Atualiza somente a getDados. (a Header nao � alterada nunca)�
	//��������������������������������������������������������������
 	dbSelectArea('FRI')
 	FRI->(dbSetOrder(1))

 	For nI := 1 to len(oFn226Mnt:oGetD:aCols)
		lExist := FRI->(dbSeek(xFilial('FRI')+FRH->FRH_CODCOL+FRH->FRH_IDPERL+FRH->FRH_PLNPGT+oFn226Mnt:oGetD:aCols[nI,1]))
		RecLock('FRI',!lExist)
		FRI->FRI_FILIAL := xFilial('FRH')
		FRI->FRI_CODCOL := FRH->FRH_CODCOL
		FRI->FRI_IDPERL := FRH->FRH_IDPERL
		FRI->FRI_PLNPGT := FRH->FRH_PLNPGT
		For nJ := 1 to len(oFn226Mnt:oGetD:aHeader)
			If FieldPos(oFn226Mnt:oGetD:aHeader[nJ,2]) > 0
				FieldPut(FieldPos(oFn226Mnt:oGetD:aHeader[nJ,2]), oFn226Mnt:oGetD:aCols[nI,nJ] )
			EndIf
		Next nJ
		FRI->(msUnlock())
	Next nI

EndIf

END TRANSACTION	//Commita transacao
Fn226Cncel()	//Fecha dialog
RestArea(aArea) //Restaura Area
Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Cncel�Autor  � Cesar A. Bianchi   � Data �  09/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Finaliza a tela de manutencao do cadastro de Descontos Condi���
���          �cionais                                                     ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Cncel()
	If ValType(oFn226Mnt) == "O"
		oFn226Mnt:Close()
	EndIf
Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Parcs�Autor  � Cesar A. Bianchi   � Data �  09/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega as parcelas do plano de pagamento+idperlet escolhido���
���          �na Enchoice (Header)                                        ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Parcs(cCodPln,cIdPerlet)
Local nAmbTop		:= 0
Local nAmbClassis   := 0
Local aParcelas		:= {}
Local aArea			:= {}
Local cQuery		:= ""
Local nI			:= 0
Local lMsSql		:= "MSSQL" $ Upper(TCGetDB())

//����������������������������Ŀ
//�Conecta na base do CorporeRM�
//������������������������������
If _IntRMTpCon(@nAmbTOP,@nAmbCLASSIS)
	aArea	:= getArea()
	TCSetConn(nAmbCLASSIS)
	If lMsSql
	    cQuery := " SELECT  PARCELA, convert(varchar(10),DTVENCIMENTO,103)  DTVENCIMENTO, VALOR, CODSERVICO FROM SPARCPLANO "
	    cQuery += " WHERE CODCOLIGADA  = " + alltrim(str(val(SM0->M0_CODIGO)))
	    cQuery += " 	AND IDPERLET = " + alltrim(cIdPerlet)
	    cQuery += " 	AND CODPLANOPGTO = '" + alltrim(cCodPln) + "'"
	    cQuery += " ORDER BY PARCELA ASC "
	else
	    cQuery := " SELECT  PARCELA, TO_CHAR(DTVENCIMENTO,'DD/MM/YYYY') DTVENCIMENTO, VALOR, CODSERVICO FROM SPARCPLANO "
	    cQuery += " WHERE CODCOLIGADA  = " + alltrim(str(val(SM0->M0_CODIGO)))
	    cQuery += " 	AND IDPERLET = " + alltrim(cIdPerlet)
	    cQuery += " 	AND CODPLANOPGTO = '" + alltrim(cCodPln) + "' "
	    cQuery += " ORDER BY PARCELA ASC "
	EndIf
	cQuery := ChangeQuery(cQuery)
	Iif(Select('TCQ')>0,TCQ->(dbCloseArea()),Nil)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "TCQ", .F., .T.)

	While TCQ->(!Eof())
		aAdd(aParcelas,{alltrim(str(TCQ->PARCELA)),TCQ->VALOR,CtoD(TCQ->DTVENCIMENTO)})
		TCQ->(dbSkip())
	EndDo

	//��������������������������Ŀ
	//�Adiciona os itens na aCols�
	//����������������������������
    oFn226Mnt:aCols := {}
    For nI := 1 to len(aParcelas)
		aAdd(oFn226Mnt:aCols,{aParcelas[nI,1],aParcelas[nI,2],aParcelas[nI,3] ,0,cTod("  /  /    ") ,0,cTod("  /  /    ") ,0,cTod("  /  /    "),.F.})
    Next nI
    oFn226Mnt:Refresh()

	//���������������������������������������������������������������Ŀ
	//�Atualiza a descricao, data inicial e final se estiver em branco�
	//�(consulta padrao nao foi executada)                            �
	//�����������������������������������������������������������������
    If Empty(M->FRH_DESCRI) .and. !Empty(M->FRH_PLNPGT)
	 	cQuery := "SELECT DESCRICAO, "
		cQuery += " convert(varchar(10),DTINICIO,103) DTINICIO, convert(varchar(10),DTFIM,103) DTFIM "
   		cQuery += " FROM SPLANOPGTO WHERE CODCOLIGADA = " + alltrim(str(VAL(SM0->M0_CODIGO)))
   		cQuery += " 	AND IDPERLET = " + M->FRH_IDPERL
   		cQuery += " 	AND CODPLANOPGTO = " + M->FRH_PLNPGT
		cQuery := ChangeQuery(cQuery)
		Iif(Select('TCQ')>0,TCQ->(dbCloseArea()),Nil)
	   	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "TCQ", .F., .T.)

   		M->FRH_DESCRI := alltrim(TCQ->DESCRICAO)
   		M->FRH_DTINI  := CtoD(TCQ->DTINICIO)
   		M->FRH_DTFIM  := CtoD(TCQ->DTFIM)
    Endif

   	//�������������������������������������Ŀ
	//�Fecha a conexao com a base do Corpore�
	//���������������������������������������
	TCQ->(dbCloseArea())
    TcSetConn(nAmbTop)
   	TcUnLink(nAmbCLASSIS)
   	RestArea(aArea)
   	cRet := M->FRH_IDPERL
EndIf

Return M->FRH_PLNPGT

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226Exist�Autor  � Cesar A. Bianchi   � Data �  11/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida se o CodColigada+IdPerlet+PlanoPagamento ja existe no���
���          �cadastro de Descontos Condicionais                          ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226Exist(cIdPerlet,cCodPln)
Local lRet := .T.
Local aArea:= getArea()
Local cQuery := ""

cQuery := " SELECT COUNT(*) TOTAL FROM " + RetSqlName('FRH') + " FRH "
cQuery += " WHERE FRH.FRH_FILIAL = '" + xFilial('FRH') + "'"
cQuery += " 	AND FRH.FRH_CODCOL = '" + alltrim(str(val(SM0->M0_CODIGO))) + "'"
cQuery += " 	AND FRH_IDPERL = '" + cIdPerlet + "'"
cQuery += " 	AND FRH.FRH_PLNPGT = '" + cCodPln + "'"
cQuery += " 	AND FRH.D_E_L_E_T_ = ' ' "
cQuery := ChangeQuery(cQuery)
iif(Select('TCQ')>0,TCQ->(dbCloseArea()),Nil)
dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "TCQ", .F., .T.)

lRet := TCQ->TOTAL > 0
TCQ->(dbCloseArea())

RestArea(aArea)
Return lRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226GetD �Autor  �Cesar A. Bianchi    � Data �  10/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega a descricao do plano de pagamento 				  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226GetD()
Local aArea 		:= getArea()
Local cRet			:= ""
Local nAmbTop		:= 0
Local nAmbClassis   := 0

//����������������������������Ŀ
//�Conecta na base do CorporeRM�
//������������������������������
If _IntRMTpCon(@nAmbTOP,@nAmbCLASSIS)
	TCSetConn(nAmbCLASSIS)
    cQuery := " SELECT  DESCRICAO FROM SPLANOPGTO "
    cQuery += " WHERE CODCOLIGADA  = " + alltrim(str(val(SM0->M0_CODIGO)))
	cQuery += " 	AND IDPERLET = " + FRH->FRH_IDPERL
	cQuery += " 	AND CODPLANOPGTO = '" + FRH->FRH_PLNPGT + "'"
	cQuery := ChangeQuery(cQuery)
	Iif(Select('TCQ')>0,TCQ->(dbCloseArea()),Nil)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "TCQ", .F., .T.)

	cRet := alltrim(TCQ->DESCRICAO)

   	//�������������������������������������Ŀ
	//�Fecha a conexao com a base do Corpore�
	//���������������������������������������
	TCQ->(dbCloseArea())
    TcSetConn(nAmbTop)
   	TcUnLink(nAmbCLASSIS)
EndIf


RestArea(aArea)
Return cRet


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226SXB  �Autor  �Cesar A. Bianchi    � Data �  03/08/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Executa a consulta padrao (especifica) "Planos de Pgto"     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226SXB()
Local cQuery 	:= ""
Local aIndices 	:= {}
Local aHeader 	:= {}
Local cEmp 		:= alltrim(str(val(SM0->M0_CODIGO))) // remove zero a esquerda
Local lMsSql	:= "MSSQL" $ Upper(TCGetDB())
Local lRet		:= .F.
Local oConsulta	:= Nil

//�����������������������������������������������������������������������������������������������������Ŀ
//�Monta a query da consulta padrao																		�
//�Converte primeiro campo para string pois o mesmo eh chave na consulta padrao e na pesquisa do mesmo	�
//�������������������������������������������������������������������������������������������������������
if lMsSql
	cQuery := "SELECT convert(varchar(4),IDPERLET) IDPERLET, CODPLANOPGTO, DESCRICAO, "
	cQuery += " convert(varchar(10),DTINICIO,103) DTINICIO, convert(varchar(10),DTFIM,103) DTFIM "
	cQuery += " FROM SPLANOPGTO WHERE CODCOLIGADA = " + cEmp
Else
 	cQuery := "SELECT cast(IDPERLET as varchar2(4)) IDPERLET, CODPLANOPGTO, DESCRICAO, "
 	cQuery += " TO_CHAR(DTINICIO,'DD/MM/YYYY') DTINICIO, "
 	cQuery += " TO_CHAR(DTFIM , 'DD/MM/YYYY') DTFIM "
 	cQuery += " FROM SPLANOPGTO WHERE CODCOLIGADA = " + cEmp
Endif

//����������������������������Ŀ
//�Monta os indices da consulta�
//������������������������������
aAdd(aIndices,{"IDPERLET + CODPLANOPGTO"	,STR0019}) //"Id. Perlet + Plano Pagamento"
aAdd(aIndices,{"CODPLANOPGTO + DESCRICAO"	,STR0020}) //"Plano Pagamento"
aAdd(aIndices,{"DESCRICAO"					,STR0021}) //"Descricao"

//�����������������������������
//�Monta a aHeader da consulta�
//�����������������������������
Aadd(aHeader,{STR0022	,"IDPERLET"		,"@!"		,4	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Id. Periodo"
Aadd(aHeader,{STR0023	,"CODPLANOPGTO"	,"@!"		,10	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Plano Pgto"
Aadd(aHeader,{STR0021	,"DESCRICAO"	,"@!"		,60	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Descricao"
Aadd(aHeader,{STR0024	,"DTINICIO"		,"@!"		,12	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Data Inicial"
Aadd(aHeader,{STR0025	,"DTFIM"		,"@!"		,12	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Data Final"

//�����������������������Ŀ
//�Monta a consulta padrao�
//�������������������������
oConsulta:=ClsIntSxb():New()
oConsulta:cTitle 		:= STR0026 					    								//Titulo da Consulta
oConsulta:cQuery 		:= cQuery														//Query dos dados a serem apresentados
oConsulta:aIndex 		:= aIndices														//Indices disponiveis (deve usar os mesmos campos da query)
oConsulta:aHeader 		:= aHeader                      								//Header da grid presente consulta (deve usar os mesmos campos da query)
oConsulta:aReturn		:= {"CODPLANOPGTO","IDPERLET","DESCRICAO","DTINICIO","DTFIM"}	//Campos de retorno da consulta (deve usar os mesmos campos da query)
oConsulta:lDataBaseRM 	:= .T.															//Indica se a query eh executada na base do protheus ou da RM
oConsulta:Show()

//����������������Ŀ
//�Coleta o retorno�
//������������������
If Len(oConsulta:aRetSXB) > 0
	c226CodPln := oConsulta:aRetSXB[1]
	c266IdPerl := oConsulta:aRetSXB[2]
	c226Descri := oConsulta:aRetSXB[3]
	c266DtIni  := CtOD(oConsulta:aRetSXB[4])
	c266DtFim  := CtOd(oConsulta:aRetSXB[5])

	//�����������������������������������Ŀ
	//�Verifica se ja foi cadastrado antes�
	//�������������������������������������
	If !Fn226Exist(c266IdPerl,c226CodPln)
		lRet := .T.

		//��������������������������������������������������
		//�Verifica se possui ao menos 1 parcela cadastrada�
		//��������������������������������������������������
		lRet := Fn226TemP(c226CodPln,c266IdPerl)

		//����������������������������Ŀ
		//�Carrega os itens da getDados�
		//������������������������������
		If nOpcA == 3 .and. lRet
			Fn226Parcs(c226CodPln,c266IdPerl)
		Endif
	else
	    lRet := .F.
	    MsgInfo(STR0027) //"Plano de Pagamento ja cadastrado. Utilize o recurso 'Alterar' para manuten��o"
	endif
EndIf

Return lRet


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Fn226TemP �Autor  � Cesar A. Bianchi   � Data �  08/11/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Verifica se o plano de pagamento escolhido possui ao menos 1���
���          �parcela cadastrada                                          ���
�������������������������������������������������������������������������͹��
���Uso       �Integracao Protheus x Classis                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function Fn226TemP(cCodPln,cIdPerlet)
Local nAmbTop		:= 0
Local nAmbClassis   := 0
Local aArea			:= {}
Local cQuery		:= ""

//����������������������������Ŀ
//�Conecta na base do CorporeRM�
//������������������������������
aArea	:= getArea()
If _IntRMTpCon(@nAmbTOP,@nAmbCLASSIS)
	TCSetConn(nAmbCLASSIS)
    cQuery := " SELECT COUNT(*) TOTAL FROM SPARCPLANO "
    cQuery += " WHERE CODCOLIGADA  = " + alltrim(str(val(SM0->M0_CODIGO)))
    cQuery += " 	AND IDPERLET = " + alltrim(cIdPerlet)
    cQuery += " 	AND CODPLANOPGTO = '" + alltrim(cCodPln) + "'"
	cQuery := ChangeQuery(cQuery)
	Iif(Select('TCQ')>0,TCQ->(dbCloseArea()),Nil)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery), "TCQ", .F., .T.)

	If TCQ->TOTAL > 0
		lRet := .T.
	Else
		lRet := .F.
		MsgStop('O Plano de pagamento escolhido nao possui parcelas cadastradas.')
	EndIf
	TCQ->(dbCloseArea())
    TcSetConn(nAmbTop)
   	TcUnLink(nAmbCLASSIS)
EndIf

RestArea(aArea)
Return lRet


//-------------------------------------------------------------------------------------
/*/{Protheus.doc}FINA226X
Retornar .T., fun��o somente para compatibilidade do X2_ROTINA

@author Francisco Oliveira
@since  27/07/2018
@version 12
/*/
//-------------------------------------------------------------------------------------

Function FINA226X()
Return .T.
