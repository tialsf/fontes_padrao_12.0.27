#Include "Protheus.ch"
#Include "FILEIO.ch"
#Include "UpdFin020.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������     
�������������������������������������������������������������������������Ŀ��
���			                                                              ���
���    PROJETO DE INTEGRACAO - Protheus x RM Biblios (RM SIstemas) 		  ���
���			                                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
���			                                                              ���
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �U_UpdF020   � Autor � Cesar A. Bianchi    � Data � 23/04/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Update para atualizacao de base referente a Integracao     ���
���          � Protheus x RM Bilios (RM Sistemas).                   	  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �U_UpdF020()   	                             			  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� 															  ���
���          � 															  ���
���          � 															  ���
���          �        													  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Integracao Protheus x Biblios (RM Sistemas)    		      ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se UpdIntBB	      			  		  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
User Function UpdF020()
Local cMsg := ""         

cArqEmp := "SigaMAT.Emp"
__cInterNet := Nil

Private cMessage
Private aArqUpd	 := {}
Private aREOPEN	 := {}
Private oMainWnd          
Private aVerCache := {}

Set Dele On

cMsg += STR0001 //"Este programa tem como objetivo criar as Tabelas "
cMsg += STR0002 //"relacionadas a Integra��o do Microsiga Protheus x RM Biblios. "
cMsg += STR0003 //"Esta rotina deve ser processada em modo exclusivo! "
cMsg += STR0004 //"Fa�a um backup dos dicion�rios e base de dados antes do processamento!" 

oMainWnd := MSDIALOG():Create()
oMainWnd:cName := "oMainWnd"
oMainWnd:cCaption := STR0005 //"Integra��o Microsiga Protheus x RM Biblios"
oMainWnd:nLeft := 0
oMainWnd:nTop := 0
oMainWnd:nWidth := 540
oMainWnd:nHeight := 250
oMainWnd:lShowHint := .F.
oMainWnd:lCentered := .T.
oMainWnd:bInit := {|| if( Aviso( STR0006, cMsg, {STR0007, STR0008}, 2 ) == 2 , (PxBUpdRun(), oMainWnd:End()), ( MsgAlert( STR0009 ), oMainWnd:End() ) ) }
 
//Controle das mensagens Aviso, MsgStop, MsgInfo e MsgAlert
If Type("lMsHelpAuto") <> Nil
	lMsHelpAuto := .F.
EndIf

oMainWnd:Activate()
                                
Return(.T.)      


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBUpdRun �Autor  �Denis D. Almeida    � Data �  18/06/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega as empresas existentes dentro do arquivo sigamat.emp���
���          �e executa o Wizard para atualizacao da Base.       		  ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios     (RM Sistemas)         ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se UpdIntRun 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/ 
Static Function PxBUpdRun()    
Local lOpen     	:= .F. 				//Retorna se conseguiu acesso exclusivo a base de dados    
Local nInd 			:= 0
Local nPos 			:= 0
Local lExecutou  	:= .F.

Private lMSSQL := "MSSQL"$Upper(TCGetDB())
Private aRecnoSM0 	:= {}
Private aEmpsFils 	:= {}
Private nI 			:= 0            //Contador para laco
Private cDBProtOld  := Space(50)   	//Variavel para armazenar o alias da base de dados do Protheus jah gravada na tabela INT_ALIASBD
Private cDBProtheus := Space(50)   	//Variavel para armazenar o alias da base de dados do Protheus
Private cDBRMOld	:= Space(50) 	//Variavel para armazenar o alias da base de dados do BIBLIOS jah gravada na tabela INT_ALIASBD
Private cDBRM		:= Space(50) 	//Variavel para armazenar o alias da base de dados do BIBLIOS

//Variaveis utilizadas para login e acesso as bases
Private cEndProt	:= Space(50)
Private cUsrProt	:= Space(50)
Private cSenProt	:= Space(50)
Private cEndCorp	:= Space(50)
Private cUsrCorp	:= Space(50)
Private cSenCorp	:= Space(50)

//Variavls utilizadas para controle de conexao das base (Protheus e RM) atraves do TOP
Private nAmbCLASSIS
Private nAmbTOP
Private cTopDataBase:= ""
Private cBaseRM 	:= ""
Private cTopServer 	:= ""
Private lTopOk   		:= .T.
Private cAlias 		:= GetNextAlias()
Private cQuery 		:= ""
Private cIniFile 	:= GetAdv97()
Private cLastConn	:= ""
Private cProtect
Private nPort
Private cSQLVersao  := ""

If ( lOpen := MyOpenSm0Ex() )
	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
		if SM0->M0_CODIGO == "99" //Desconsidera a empresa 99=Teste
			dbSkip()
			loop
		endif
  		If Ascan(aRecnoSM0,{ |x| x[2] == SM0->M0_CODIGO}) == 0 //--So adiciona no aRecnoSM0 se a empresa for diferente
			aAdd(aRecnoSM0,{Recno(),SM0->M0_CODIGO})
		EndIf
		aAdd(aEmpsFils,{.F.,SM0->M0_CODIGO,AllTrim(SM0->M0_NOME),SM0->M0_CODFIL,alltrim(SM0->M0_FILIAL),Recno()})
		dbSkip()
	EndDo
	
	ASort(aRecnoSM0,,,{|x,y| x[2] < y[2] })
	
	If lOpen
		SM0->(dbGoto(aRecnoSM0[1,1]))
		RpcSetType(2) 
		RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
		lMsFinalAuto := .F.

		//Seta a variavel para identificar o Banco de Dados Utilizado
		lMSSQL := "MSSQL"$Upper(TCGetDB())
		
		if lMSSQL //SQL Server
			//Busca a Versao do Banco de Dados SQL Server Utilizado
			dbUseArea( .T., "TOPCONN", TCGENQRY(,,"SELECT @@version as VERSAO"), "INFOSQL", .F., .T.)
			cSQLVersao := INFOSQL->VERSAO
			INFOSQL->( dbCloseArea() )
			
			for nInd:=1 to 3
				cSQLVersao := AllTrim(SubStr(cSQLVersao,At(" ",cSQLVersao)))
			next nInd
			cSQLVersao := AllTrim(SubStr(cSQLVersao,1,At(" ",cSQLVersao)))
		endif
		
		//Monta a tela do Wizard
		lExecutou := PxBTblWiz()
		
	    RpcClearEnv()
	endif 
	
endif  

if lTopOk .and. lExecutou
	MsgAlert(STR0010) //'Processamento Conclu�do.'
else
	MsgAlert(STR0011) //'Processamento Cancelado.'
endif

Return 

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBTblWiz �Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Monta a Tela do Wizard                                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM BIBLIOS (RM Sistemas)         	  ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdTblWiz 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBTblWiz()
Local cTxtPnl 	:= STR0012 //"O assistente ir� auxili�-lo no processo de instala��o da Integra��o Microsiga Protheus x RM Biblios."
Local cProcess	:= "CANCEL" //Processamento Cancelado
Local lRet 		:= .T.

Private oWizard
Private oListSM0
Private nMeter3	 	:= 0
Private oReguaTot
Private oReguaProc
Private nReguaTot	 := 0
Private oSay1
Private oSay2
Private oMemo
Private oMMProcess
Private oMMProcTot
Private oNoMarked  	:= LoadBitmap( GetResources(),'LBNO')
Private oMarked	  	:= LoadBitmap( GetResources(),'LBOK')
Private oFont 		:= TFont():New( "Arial Black"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .F./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private oFont1 		:= TFont():New( "Tahoma"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .T./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private oFontRed	:= TFont():New( "Tahoma"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .T./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private cCxText 	:= ""
Private cMMProcess 	:= ""
Private cMMProcTot 	:= ""


//���������������Ŀ
//�Primeiro painel�
//�����������������
oWizard := APWizard():New(STR0013/*<chTitle>*/, ""/*<chMsg>*/, STR0014/*<cTitle>*/, cTxtPnl/*<cText>*/, {||  .T.}/*<bNext>*/, {|| .T.}/*<bFinish>*/, /*<.lPanel.>*/,,,/*<.lNoFirst.>*/ )

//���������������Ŀ
//�Segundo painel �
//�����������������
oWizard:NewPanel( STR0015 /*<chTitle>*/, STR0016 /*<chMsg>*/,{ ||.T.}/*<bBack>*/, {|| PxBVlPn1()}/*<bNext>*/, {|| .T.}/*<bFinish>*/, /*<.lPanel.>*/, {|| .T.}/*<bExecute>*/ )
	
	//"Selecione as empresas a serem consideradas:"
	TSay():New( 02/*<nRow>*/, 007/*<nCol>*/, {|| STR0017 }	/*<{cText}>*/, oWizard:oMPanel[2]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, /*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
	
	
    //ListBox - Empresas/Filiais
    oListSM0 := TWBrowse():New( 15, 7, oWizard:oMPanel[2]:nClientHeight, oWizard:oMPanel[2]:nClientWidth-480, {|| }, {"", STR0018, STR0019, STR0020, STR0021}, {}, oWizard:oMPanel[2],,,,,,,,,,,,,,.T.,,,,,)
    oListSM0:bHeaderClick := {|x,y| if(y==1, PxBMkAll(),nil) }
    
	Asort(aEmpsFils,,, { |x,y| x[2]+x[4] < y[2]+y[4] } )
    
	oListSM0:SetArray( aEmpsFils )
	oListSM0:bLine := {||	{ if(aEmpsFils[oListSM0:nAt,1], oMarked, oNoMarked),;	//Flag de marcacao
								 aEmpsFils[oListSM0:nAt,2],;						//Codigo da Empresa
								 aEmpsFils[oListSM0:nAt,3],;						//Nome da Empresa
								 aEmpsFils[oListSM0:nAt,4],;						//Codigo da Filial
								 aEmpsFils[oListSM0:nAt,5]} }						//Nome da Filial
								 
    oListSM0:blDblClick := { || PxBMark() }
    

//���������������Ŀ
//�Terceiro painel�
//�����������������
oWizard:NewPanel( STR0022/*<chTitle>*/, STR0023/*<chMsg>*/, { || .F.}/*<bBack>*/, /*<bNext>*/, /*<bFinish>*/, /*<.lPanel.>*/, {|| cProcess:=PxBTbProc(), PxBCtrl(cProcess)}/*<bExecute>*/ )
				
	@ 05,05 GET oMemo  VAR cCxText MEMO SIZE 290,80 OF oWizard:oMPanel[3] PIXEL
	oMemo:bRClicked := {||AllwaysTrue()}
	oMemo:LREADONLY := .T.
//	oMemo:oFont:=oFont

	@ 89,65 GET oMMProcess  VAR cMMProcess MEMO SIZE 230,10 OF oWizard:oMPanel[3] PIXEL
	oMMProcess:LREADONLY := .T.
	
	@ 116,65 GET oMMProcTot  VAR cMMProcTot MEMO SIZE 230,10 OF oWizard:oMPanel[3] PIXEL
	oMMProcess:LREADONLY := .T.

oWizard:Activate( .T./*<.lCenter.>*/,/*<bValid>*/, /*<bInit>*/, /*<bWhen>*/ )

if cProcess == "CANCEL" //Processamento Cancelado
	lRet := .F.
endif

Return lRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBTbProc �Autor  � Cesar A. Bianchi   � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Executa o processamento de atualizacao da base.            ���
���          � Baseado no update da integracao Protheus x Biblios. 		  ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios (RM Sistemas)	          ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdTbProc 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBTbProc()
Local cRet 		:= "OK" 			//Processamento Concluido com Sucesso
Local nI 		:= 1
Local nX 		:= 0
Local nInd 		:= 0
Local lCriaTab 	:= .T. 				//Variavel de controle para executar apenas uma vez a funcao para criacao de tabelas apenas uma vez
Local lGetDB   	:= .F.
Local lProsseg 	:= .F.
Local cTxtLOG	:= ""  				//Grava o log do processo
Local aFiliais 	:= {}
Local lCountFil := 0
Local lAllFils 	:= .F.
Local cEmpAnt 	:= ""
Local nQtdEmp 	:= 0
Local nContador := 0
Local lExecuta 	:= .T.
Local aSucess 	:= {}
Local cNomeArq	:= __RelDir+"UpdIntPB.##R"
Local nCurrTot  := 0				//Controle do nivel da regua de processamento parcial
Private aArqUpd	 := {}

//����������������������������������������Ŀ
//�Coleta a quantidade de empresas marcadas�
//������������������������������������������
for nInd:=1 to len(aEmpsFils)
	if aEmpsFils[nInd][2] <> cEmpAnt
		cEmpAnt := aEmpsFils[nInd][2]
		nQtdEmp++
	endif
next nInd
cEmpAnt := ""

//������������������������������������Ŀ
//�Monta a regua do Processamento Total�
//��������������������������������������
oReguaTot:= tMeter():New(127,005,{|u|if(Pcount()>0,nReguaTot:=u,nReguaTot)},nQtdEmp,oWizard:oMPanel[3],290,10,   ,.T.)
oReguaTot:Set(0)

//��������������������������������������Ŀ
//�Monta a regua de Processamento Parcial�
//����������������������������������������
oReguaProc:= tMeter():New(100,005,{|u|if(Pcount()>0,nMeter3:=u,nMeter3)},1,oWizard:oMPanel[3],290,10,   ,.T.)
oReguaProc:NTOTAL := 9
oReguaProc:Set(0)

//�������������������������������������������������������Ŀ
//�Monta as Legendas "Processando" e "Processamento Total"�
//���������������������������������������������������������
oSay1:= TSay():New( 92/*<nRow>*/, 005/*<nCol>*/, {|| STR0024 } /*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, ((oWizard:ODLG:NCLIENTWIDTH/2)-30)/*<nWidth>*/, 8/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
oSay2:= TSay():New( 120/*<nRow>*/, 005/*<nCol>*/, {|| STR0025 } /*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, ((oWizard:ODLG:NCLIENTWIDTH/2)-30)/*<nWidth>*/, 8/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
cCxText := ""
oMemo:Refresh()

//��������������������������Ŀ
//�Grava LOG do Processamento�
//����������������������������
cTxtLOG := Replicate("=",100)+CHR(13)+CHR(10)+CHR(13)+CHR(10)
cTxtLOG += STR0026 +DToC(Date())+" - "+Time()+CHR(13)+CHR(10)
PxBArqLog(cNomeArq, cTxtLOG)
conout(cTxtLOG)
cCxText += cTxtLOG
oMemo:Refresh()

//��������������������������������������������Ŀ
//�Executa os updates para cada empresa marcada�
//����������������������������������������������
While nI <= Len(aEmpsFils)
	if !aEmpsFils[nI,1] .or. aEmpsFils[nI,2] == cEmpAnt
		nI++
		loop
	endif
	
	//���������������������������������������������������������Ŀ
	//�Atualiza as tabelas da empresa corrente no banco de dados�
	//�����������������������������������������������������������	
	cEmpAnt := aEmpsFils[nI,2]
	SM0->(dbGoto(aEmpsFils[nI,6]))
	RpcSetType(2) 
	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	lMsFinalAuto := .F.
	
	aadd(aArqUpd,"SA1")
	aadd(aArqUpd,"SE1")
	aadd(aArqUpd,"SE5")
	aadd(aArqUpd,"SX5")
	aadd(aArqUpd,"SA6")
	dbSelecTArea("SA1")
	dbSelecTArea("SE1")
	dbSelecTArea("SE5")
	dbSelecTArea("SX5")
	dbSelecTArea("SA5")
	
	__SetX31Mode(.F.)

	For nX := 1 To Len(aArqUpd)
		If Select(aArqUpd[nx])>0
			dbSelecTArea(aArqUpd[nx])
			dbCloseArea()
		EndIf
		X31UpdTable(aArqUpd[nx])				
		If __GetX31Error()
			Alert(__GetX31Trace())
			Aviso(STR0027,STR0028+ aArqUpd[nx] + STR0029 ,{STR0030},2)  //"Ocorreu um erro desconhecido durante a atualizacao da tabela : " 
		EndIf
	Next nX	
	
	aFiliais := {}
	lCountFil := 0
	lAllFils := .F.
	
	//���������������������������������������������������������������������Ŀ
	//�Verifica se foram selecionadas todas as filiais da empresa em questao�
	//�����������������������������������������������������������������������	
	for nInd:=1 to len(aEmpsFils)
		if aEmpsFils[nInd][2] == SM0->M0_CODIGO
			lCountFil++
			if aEmpsFils[nInd][1]
				aAdd(aFiliais, SM0->M0_CODFIL)
			endif
		endif
	next nInd
	
	lAllFils := len(aFiliais) == lCountFil
	
	cMMProcTot := STR0031 + SM0->M0_CODIGO
	oMMProcTot:Refresh()
	cCxTextAnt := cCxText
	
	//��������������������������������������������������������������������������������������Ŀ
	//�Monta Dialog para que o usuario informe o nome das bases de dados (Protheus e Biblios)�
	//����������������������������������������������������������������������������������������	
	if !lGetDB
		lGetDB := .T. //Seta a variavel para chamar a funcao apenas uma vez
		lProsseg := PxCGetDB()
	endif
	
	if lProsseg 
		//��������������������������Ŀ
		//�Grava LOG do Processamento�
		//����������������������������
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(10)+ STR0032 +SM0->M0_CODIGO+" - "+DToC(Date())+" - "+Time()		
		PxBArqLog(cNomeArq, cTxtLOG)
		conout(cTxtLOG)
		cCxText += cTxtLOG
		oMemo:Refresh()
	
		//���������������������������������������������������Ŀ
		//�Busca conexao com as bases de dados (Protheus e RM)�
		//�����������������������������������������������������
		lTopOk 	:= _IntRMTpCon(@nAmbTOP,@nAmbCLASSIS)
		
		if lTopOk 		
			// Alterna o TOP para o ambiente padrao
			TCSetConn( nAmbTOP )

			//�����������������������������������������������������������������������������Ŀ
			//�--> 1o.) Verifica se o ambiente possui a integracao com o CLASSIS INSTALADA. �
			//�		   Em caso negativo, instala, pois existem tabelas da integracao CLASSIS�
			//�		   que sao necessarias para a integracao BIBLIOS.						�
			//�������������������������������������������������������������������������������
			nContador := 1
			oReguaProc:Set(nContador)
			cMMProcess:= STR0033 //"Instalando Procedures, Trigers e Functions..."
			oMMProcess:Refresh()
						
			//���������������Ŀ
			//�Cria as tabelas�
			//�����������������				
			if lMSSQL 
				PxCTblSQL()
				//PRXBBT_SQL()
			else
				PxCTblORA()
				//PRXBBT_ORA()
			endif

			//���������������������Ŀ
			//�Atualizacao de campos�
			//�����������������������				
			if lMSSQL
				PxCNewCmp()
			Endif
			
			//����������������������������������������������Ŀ
			//�Cria as procedures, functions e trigers UNICAS�
			//������������������������������������������������				
			if lMSSQL
				PxCFnUnSQL() //::Functions
				PxCSPUnSQL() //::Procedures
				PxCTrUnSQL() //::Trigers
			else
				PxCFnUnORA() //::Functions
				PxCPkUnORA() //::Packages
				PxCSPUnORA() //::Procedures
				PxCTrUnORA() //::Trigers
			endif
			
			//��������������������������������������������������������Ŀ
			//�Cria as trigers por EMPRESA							   �
			//�OBS: Nao exisem mais procedures e functions por empresa �
			//����������������������������������������������������������
			if lMSSQL	
				PxCTrSQL(SM0->M0_CODIGO, SM0->M0_CODFIL)   //::Trigers
			else
				PxCTrORA(SM0->M0_CODIGO, SM0->M0_CODFIL)   //::Trigers
			endif										

			//���������������Ŀ
			//�Registra em log�
			//�����������������				
			cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(10)+ STR0034 //"***Instala��o de Procedures, trigers e funcions OK"
			PxBArqLog(cNomeArq, cTxtLOG)
			conout(cTxtLOG)
			cCxText += cTxtLOG
			oMemo:Refresh()


			//�����������������������������������������������������������������������������Ŀ
			//�--> 2o.) Efetua Atualizacao de Dicionarios                                   �
			//�������������������������������������������������������������������������������
			nContador := 2
			oReguaProc:Set(nContador)
			cMMProcess := STR0035 //"Atualizando Dicion�rios de Dados..."
			oMMProcess:Refresh()
			
			//������������������������Ŀ
			//�Atualiza o parametro SX6�
			//��������������������������
			PxBSx6(SM0->M0_CODIGO, SM0->M0_CODFIL)
			
			//�����������������������Ŀ
			//�Atualiza Consultas SXB �
			//�������������������������
			PxCAtuSxB()
			
			//������������������������������Ŀ
			//�Inclui Registro na tabela SX5 �
			//��������������������������������
			PxCAtuSx5()
			
			cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(15)+ STR0036 +DToC(Date())+" - "+Time() //" Atualiza��o de Dicion�rio e Dados - [Executado com Sucesso] - "
			
			//Grava LOG do Processamento
			PxBArqLog(cNomeArq, cTxtLOG)
			conout(cTxtLOG)
			cCxText += cTxtLOG
			oMemo:Refresh()

			
			//Atualiza as tabelas no Banco de Dados
			For nX := 1 To Len(aArqUpd)
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])				
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso(STR0037,STR0028+ aArqUpd[nx] + STR0029 ,{STR0030},2)
				EndIf
			Next nX

			//Efetua a carga inicial na tabela SA6 (BANCOS) do campo novo do Protheus.  
			//Efetua a cria�ao dos novos motivos de baixa do sigafin no arquivo SIGAADV.MOT 
			PxCAtuBase()
			
			nCurrTot+=1
			oReguaTot:Set(nCurrTot)
			
			cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(10)+STR0038+SM0->M0_CODIGO+" - "+DToC(Date())+" - "+Time() +CHR(13)+CHR(10)
			
			//Grava LOG do Processamento
			PxBArqLog(cNomeArq, cTxtLOG)
			conout(cTxtLOG)
			cCxText += cTxtLOG
			oMemo:Refresh()	
    	else
    		exit
    	endif
	else
    	cRet := "CANCEL" //Processamento Cancelado
    	
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(10)+ STR0038 +DToC(Date())+" - "+Time()+CHR(13)+CHR(10)+CHR(13)+CHR(10)
		
		//Grava LOG do Processamento
		PxBArqLog(cNomeArq, cTxtLOG)
		conout(cTxtLOG)
		cCxText += cTxtLOG
		oMemo:Refresh()
		
    	exit
    endif
         
    if lTopOk
		TCUNLINK(nAmbCLASSIS) // Finaliza a conexao do TOP com o ambiente Biblios [nAmbCLASSIS]
		TCSetConn(nAmbTOP)
	endif
    
    RpcClearEnv()
	If !( lOpen := MyOpenSm0Ex() )
		Exit 
	EndIf 
	nI++
    
EndDo

cTxtLOG := CHR(13)+CHR(10)+ STR0040 +DToC(Date())+" - "+Time()  //"Fim - Instala��o da Integra��o - "
cTxtLOG += CHR(13)+CHR(10)+CHR(13)+CHR(10)+Replicate("=",100)+CHR(13)+CHR(10)+CHR(13)+CHR(10)

//Grava LOG do Processamento
PxBArqLog(cNomeArq, cTxtLOG)
conout(cTxtLOG)
cCxText += cTxtLOG
oMemo:Refresh()

if lTopOk
	TCUNLINK(nAmbCLASSIS) // Finaliza a conexao do TOP com o ambiente Biblios [nAmbCLASSIS]
	TCSetConn(nAmbTOP)
endif

Return cRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � PxBMark  �Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Efetua a marcacao da Empresa/Filial selecionada.           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios (RM Sistemas)		      ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdMark 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBMark()

if !empty(aEmpsFils[oListSM0:nAt,2])

	//�����������������������������������������������������������������������������������������������������������Ŀ
	//�Verifica se nao eh uma empresa que esta utilizando o conceito de "Gestao de Empresas - Somente Vesrsao 11")�
	//�������������������������������������������������������������������������������������������������������������
	If len(alltrim(aEmpsFils[oListSM0:nAt,4])) <= 2	
		aEmpsFils[oListSM0:nAt,1] := !aEmpsFils[oListSM0:nAt,1]
		oListSM0:Refresh()
	Else
		Aviso(STR0037,STR0065,{STR0046}) //"A empresa/filial selecionada possui o conceito de Gest�o de Empresas e n�o pode ser utilizada na funcionalidade de integra��o Microsiga Protheus x RM Biblios"
	EndIf
Endif

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBVlPn1  �Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Valida se pelo menos uma empresa foi selecionada.          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios  (RM Sistemas)            ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdVlPn1 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBVlPn1()
Local lRet := .T.
Local nCount 	:= 0

aEval( aEmpsFils, { |x| nCount += iif( x[1], 1, 0 ) } )

if nCount == 0 
	lRet := .F.
	MsgStop(STR0041) //"Ao menos uma Empresa/Filial deve ser selecionada."
endif

Return lRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � PxBCtrl  �Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ativa ou desativa objetos na tela.                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios (RM Sistemas)     		  ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdCtrl 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBCtrl(cProcess)
oSay1:LVISIBLECONTROL := .F.
oSay2:LVISIBLECONTROL := .F.
oReguaTot:LVISIBLECONTROL:=.F.
oReguaProc:LVISIBLECONTROL:=.F.
oMMProcess:LVISIBLECONTROL := .F.
oMMProcTot:LVISIBLECONTROL := .F.
oWizard:OBACK:LVISIBLE:=.F.
oWizard:OCANCEL:LVISIBLECONTROL:=.F.
oMemo:nHeight := 250
oMemo:Refresh()

if cProcess == "OK" //Processamento Conclu�do com Sucesso
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0042 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
elseif cProcess == "ERRO" //Processamento Conclu�do com ERRO
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0043 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HRED/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
else //Processamento Cancelado
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0044 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HRED/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
endif

Return .T.


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBMkAll  �Autor  � Alberto Deviciente � Data � 07/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Efetua a marcacao de todas as Empresas/Filiais.            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios (RM Sistemas)    	      ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdMkAll 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBMkAll()
Local nCont := 0

aEval( aEmpsFils, { |x| nCont += iif( x[1] , 1, 0 ) } )

if nCont == len(aEmpsFils)
	aEval( aEmpsFils, {|x| x[1] := .F.} )
else
	aEval( aEmpsFils, {|x| x[1] := .T.} )
endif

oListSM0:Refresh()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PxBArqLog �Autor  � Alberto Deviciente � Data � 08/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Grava o arquivo de LOG do processamento.                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Biblios (RM Sistemas)         	  ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdArqLog 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBArqLog(cArquivo, cTexto)
Local nHdl 		:= 0

If !File(cArquivo)
	//Se nao encontrou o arquivo, cria
	nHdl := FCreate(cArquivo)
Else
	//Encontrou o arquivo
	nHdl := FOpen(cArquivo, FO_READWRITE)
Endif
FSeek(nHdl,0,FS_END)
//cTexto += Chr(13)+Chr(10)
FWrite(nHdl, cTexto, Len(cTexto))
FClose(nHdl)

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Cesar A. Bianchi      � Data �05/05/08  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Integracao Protheus x RM Biblios 					      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex() 

Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 To 20  
	dbUseArea(.T.,, "sigamat.emp", "SM0", .F., .F. ) 	
	If !Empty(Select("SM0" ) ) 
		lOpen := .T. 
		dbSetIndex("sigamat.ind") 
		Exit	
	EndIf
	Sleep(500) 
Next nLoop

If !lOpen
	Aviso(STR0037, STR0045, { STR0046 }, 2 ) 
EndIf

Return(lOpen)

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �PxBSx6	� Autor �Cesar A. Bianchi       � Data �23/Abr/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao para criacao/atualizacao de parametros.             ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Integracao Protheus x Biblios (RM Sistemas)                ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdSx6 							  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function PxBSx6(cEmp, cFil)
Local cTexto    := ''						//String para msg ao fim do processo
Local lSx6      := .F.                      //Verifica se houve atualizacao
Local aSx6      := {}						//Array que armazenara os indices
Local aEstrut   := {}				        //Array com a estrutura da tabela SX6
Local i         := 0 						//Contador para laco
Local j         := 0 						//Contador para laco
Local lInclui 	:= .F.

/*********************************************************************************************
Define estrutura do array
*********************************************************************************************/
aEstrut:= { "X6_FILIAL","X6_VAR","X6_TIPO","X6_DESCRIC","X6_DSCSPA","X6_DSCENG","X6_DESC1","X6_DSCSPA1",;
			"X6_DSCENG1","X6_DESC2","X6_DSCSPA2","X6_DSCENG2", "X6_CONTEUD","X6_CONTSPA", "X6_CONTENG",;
			"X6_PROPRI", "X6_PYME"}

/*********************************************************************************************
Define os dados do parametro
*********************************************************************************************/
aadd(aSx6,{"  ",;											//Filial
		"MV_RMBIBLI",;										//Var
		"L",;                 								//Tipo
		"Parametro de integracao Protheus x Biblios",; 		//Descric
		"Par�metro del integracion Protheus x Biblios",; 	//DscSpa
		"Integration parameter Protheus x Biblios",; 		//DscEng
		"",;												//Desc1
		"",;												//DscSpa1
		"",;												//DscEng1
		"",; 												//Desc2
		"",;												//DscSpa2
		"",;												//DscEng2
		".T.",;												//Conteud
		".T.",;												//ContSpa
		".T.",;												//ContEng
		"S",;												//Propri		
		"S"})												//Pyme


/*********************************************************************************************
Grava as informacoes do array na tabela six
*********************************************************************************************/
ProcRegua(Len(aSx6))

dbSelectArea("SX6")
SX6->(DbSetOrder(1))	

For i:= 1 To Len(aSx6)
	If DbSeek("  "+aSx6[i,2]) .or. DbSeek(cFil+aSx6[i,2])
		RecLock("SX6",.F.)
		lInclui := .F.
	Else
		RecLock("SX6",.T.)
		lInclui := .T.
	EndIf
	
	aAdd(aArqUpd,aSx6[i,1])
	lSx6 := .T.
	For j:=1 To Len(aSx6[i])
		if lInclui .or. !(aEstrut[j] $ "X6_FILIAL|X6_CONTEUD|X6_CONTSPA|X6_CONTENG") //Nao altera os campos jah configurados pelo usuario
			If FieldPos(aEstrut[j])>0
				FieldPut(FieldPos(aEstrut[j]),aSx6[i,j])
			EndIf
		endif
	Next j
	dbCommit()        
	MsUnLock()
	cTexto  += " "+aSx6[i][2]+" /"
Next i

If lSx6
	cTexto := SubStr(cTexto,1,len(cTexto)-1)
	cTexto := CHR(13)+CHR(10)+STR0064 +CHR(13)+CHR(10)+"  -"+cTexto+CHR(13)+CHR(10)
EndIf

Return cTexto