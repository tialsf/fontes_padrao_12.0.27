#INCLUDE "UPDFIN030.CH"    
#INCLUDE "RWMAKE.CH"
#INCLUDE "PROTHEUS.CH" 


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � UPDF030  � Autor � Cesar A. Bianchi      � Data � 07/10/08 ���
�������������������������������������������������������������������������Ĵ��
���Objetivos �Cria as tabelas, campos, parametros e grupos de pergunte  ne���
���          �cessarios para as rotinas que compoe o Caixa Tesouraria.    ���
���          �Fina220, Fina221, Fina222, Fina223, Fina224 e Fina225.      ���
���          �Fonte tambem contempla o U_UNPF030 						  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � U_UPDF030()                                                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAFIN e Integracao Protheus x Classis Net                ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
User Function UPDF030()
Local cMsg := ""         

Private oMainWnd         
Private aArqUpd	    := {}
Private aREOPEN	    := {}
Private cPulaLin    := CHR(13)+CHR(10)
Private cTexto      := ''
Private cMessage    := ''

Private	cReservKey  := ''
Private	cReservObr	:= ''
Private	cReservOpc  := ''
Private cReservNao  := ''

Private cUsadoKey   := ''
Private	cUsadoOpc   := ''
Private	cUsadoObr	:= ''
Private	cUsadoNao   := ''


Set Dele On        

//��������������������������������������������������������������������Ŀ
//�Monta o texto de orientacao ao usuario exibido na interface grafica.�
//����������������������������������������������������������������������
cMsg := STR0001 			//"Este programa tem como objetivo ajustar os dicion�rios e base de dados, " 
cMsg += STR0002 			//"para compatibilizacao com a melhoria de Sistema de Caixa. "
cMsg += STR0003 + cPulaLin	//"Esta rotina deve ser processada em modo exclusivo! " 
cMsg += STR0001 + cPulaLin 	//"Fa�a um backup dos dicion�rios e base de dados antes do processamento! "

//��������������������������������������Ŀ
//�Cria a interface grafica com o usuario�
//����������������������������������������
oMainWnd 		   := MSDIALOG():Create()
oMainWnd:cName     := "oMainWnd"
oMainWnd:cCaption  := STR0005 //"Atualizacao do Dicionario de Dados"
oMainWnd:nLeft     := 0
oMainWnd:nTop      := 0
oMainWnd:nWidth    := 540
oMainWnd:nHeight   := 250
oMainWnd:lShowHint := .F.
oMainWnd:lCentered := .T.
oMainWnd:bInit     := {|| if( Aviso( STR0006, cMsg, {STR0007, STR0008}, 2 ) == 2 , ( Processa({|lEnd| Ge41Prep(@lEnd)} , STR0006 ) , oMainWnd:End() ), ( MsgAlert( STR0009), oMainWnd:End() ) ) }
oMainWnd:Activate()

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41PREP  �Autor  �Cesar A. Bianchi    � Data �  07/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Prepara o ambiente para gravar os novos campos e tabelas no ���
���          �dicionario de dados carregando as empresas existentes dentro���
���          �do SIGAMAT.EMP                                              ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41PREP()
Local cMask       := STR0010						   //Mascara do arquivo de log a ser salvo
Local nRecno      := 0                                 //Nro empresa corrente
Local lOpen       := MyOpenSm0Ex() 				       //Retorna se conseguiu acesso exclusivo ao sigamat.emp
Local nI      	  := 0                                 //Contador para laco For/Next
Private aRecnoSM0 := {}                                //Array contendo as empresas presentes no sigamat.emp


If lOpen
	dbSelectArea("SM0")
	SM0->(dbGotop())
	
	//������������������������������������������������������������������Ŀ
	//�Adiciona o array aRecnoSMO todas as empresas existentes no sigamat�
	//��������������������������������������������������������������������	
	While SM0->(!Eof())
  		If Ascan(aRecnoSM0,{ |x| x[2] == SM0->M0_CODIGO}) == 0 
			aAdd(aRecnoSM0,{SM0->(Recno()),SM0->M0_CODIGO})
		EndIf			
		SM0->(dbSkip())
	EndDo	
		                                                
	//��������������������������������������������������������������������������Ŀ
	//�Executa a atualizacao dos SX's de cada empresa presente no array aRecnoSM0�
	//����������������������������������������������������������������������������	
	For nI := 1 to Len(aRecnoSM0)
		
		//������������������������������������Ŀ
		//�Define a empresa a manipular os SX's�
		//��������������������������������������
		SM0->(dbGoto(aRecnoSM0[nI,1]))
		RpcSetType(2) 
		RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
		lMsFinalAuto := .F.
	    
		//��������������������������������������������������Ŀ
		//�Registra no log a atualizacao da empresa corrente.�
		//����������������������������������������������������		
		cTexto += Replicate("-",128) + cPulaLin
		cTexto += STR0011 + SM0->M0_CODIGO+SM0->M0_NOME + cPulaLin //"Empresa : "
	 
		//������������������������������������������Ŀ
		//�Define o tamanho da regua de processamento�
		//��������������������������������������������		
		ProcRegua(nI)
		IncProc(STR0011 +SM0->M0_CODIGO+SM0->M0_NOME) //"Empresa : "
			
		//�������������������������������������������������������Ŀ
		//�Incrementa a regua de processamento e alerta no console�
		//���������������������������������������������������������		
		IncProc( dtoc( Date() )+" "+Time()+" "+STR0012) //"Inicio - Ajustando Dicion�rios"
		Conout( dtoc( Date() )+" "+Time()+" "+STR0012 ) //"Inicio - Ajustando Dicion�rios"
		cTexto += STR0013 + cPulaLin //"Ajustando Dicion�rios..."
		          
		//�������������������������������������������Ŀ
		//�Cria os campos no SX da empresa selecionada�
		//���������������������������������������������		
		GE41Grv(SM0->M0_CODIGO)
	
		//������������������������������������������������������Ŀ
		//�Executa a sincronizacao entre o SX3 e o banco de dados�
		//��������������������������������������������������������
		GE41Atu()

		//�������������������������������������������Ŀ
		//�Cria os parametros no SX6                  �
		//���������������������������������������������		
		GE41Sx6()

		//�������������������������������������������Ŀ
		//�Cria os gatilhos do campo no SX7           �
		//���������������������������������������������		
		GE41Sx7()

		//��������������������������
		//�Cria as consultas padrao�
		//��������������������������		
		GE41SXB()
		
		//������������������������Ŀ
		//�Cria as perguntas no SX1�
		//��������������������������		
		GE41SX1()
		
		//�����������������������������������������Ŀ
		//�Cria tabelas da integra��o (caso utilize)�
		//�������������������������������������������
		If GetNewPar('MV_RMCLASS',.F.)
			IntNewTbl()		
		Endif

		//�������������������������������������������������������Ŀ
		//�Incrementa a regua de processamento e alerta no console�
		//���������������������������������������������������������						
		IncProc( dtoc( Date() )+" "+Time()+" "+STR0014) //"Fim - Ajuste de Dicion�rios"
		Conout( dtoc( Date() )+" "+Time()+" "+STR0014)  //"Fim - Ajuste de Dicion�rios"
		cTexto += STR0015 + cPulaLin   					  //"Fim - Ajuste de Dicion�rios..."
	Next nI
endif

//���������������������������������������Ŀ
//�Exibe alerta do final do processamento.�
//�����������������������������������������
cTexto := STR0015  + cPulaLin + cTexto //"Log da Atualiza��o "
__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
DEFINE FONT oFont NAME "Mono AS" SIZE 5,12   //6,15
DEFINE MSDIALOG oDlg TITLE STR0016 From 3,0 to 340,417 PIXEL //"Atualizacao Conclu�da."
@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
oMemo:bRClicked := {||AllwaysTrue()}
oMemo:oFont:=oFont
DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
ACTIVATE MSDIALOG oDlg CENTER   

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41Grv   �Autor  �Cesar A. Bianchi    � Data �  07/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava os campos novos nos arquivos SX da empresa corrente.  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41Grv(cEmp)
Local lExist	:= .F.
Local aGravaSx  := {}
Local nJ, nI  
Local aEstrut	:= {"X3_ARQUIVO","X3_ORDEM"  ,"X3_CAMPO"  ,"X3_TIPO"   ,"X3_TAMANHO","X3_DECIMAL","X3_TITULO" ,"X3_TITSPA" ,"X3_TITENG" ,;
					"X3_DESCRIC","X3_DESCSPA","X3_DESCENG","X3_PICTURE","X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO","X3_F3"     ,"X3_NIVEL"  ,;
					"X3_RESERV" ,"X3_CHECK"  ,"X3_TRIGGER","X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT","X3_VLDUSER",;
					"X3_CBOX"   ,"X3_CBOXSPA","X3_CBOXENG","X3_PICTVAR","X3_WHEN"   ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER" , "X3_PYME"   }
		
//�����������������������������������������������������������Ŀ
//�Carrega as variaveis que definem os campos como obrigatorio�
//�������������������������������������������������������������
Ge41VarObr()
                    
//����������������������������������������������������������������������������������������������Ŀ
//�******************************************* FID **********************************************�
//������������������������������������������������������������������������������������������������

//������������������������Ŀ
//�Cria a tabela FID no SX2�
//��������������������������
dbSelectArea('SX2')
SX2->(dbSetOrder(1))
if SX2->(!dbSeek('FID'))
	RecLock('SX2',.T.)
	SX2->X2_CHAVE   := 'FID'
	SX2->X2_PATH    := ''
	SX2->X2_ARQUIVO := 'FID'+alltrim(cEmp)+'0'
	SX2->X2_NOME    := STR0017 //'Cadastro Operador'
	SX2->X2_NOMESPA := STR0017 //'Cadastro Operador'
	SX2->X2_NOMEENG := STR0017 //'Cadastro Operador'
	SX2->X2_MODO    := 'E'
	SX2->X2_DELET   := 0
	SX2->X2_TTS     := ''
	SX2->X2_UNICO   := 'FID_FILIAL+FID_USER'
	SX2->X2_PYME    := 'S'
	SX2->(msUnlock())  
	cTexto += STR0018 + cEmp  + cPulaLin //"Criada a tabela FID na empresa: "
endif 

//����������������������������Ŀ
//�Cria os campos da FID no SX3�
//������������������������������
dbSelectArea('SX3')
SX3->(dbSetOrder(2))
aGravaSx := {}			
aAdd(aGravaSX,{ 'FID'			   		,'01'      				,'FID_FILIAL'          ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,2         				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0019           		,STR0019				,STR0019               ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0020					,STR0020				,STR0020			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'02'      				,'FID_LEG' 		       ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,2         				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0021            		,STR0021				,STR0021               ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0022					,STR0022				,STR0022			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@BMP'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				'' 						,''						,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				'V'				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'V'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,'Fn221SitCx(FID->FID_USER,FID->FID_NCAIXA)' ,;	//PICTVAR,  //WHEN,  //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME
									
aAdd(aGravaSX,{ 'FID'			   		,'03'      				,'FID_USER'            ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,6        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0023       	   		,STR0023			    ,STR0023 		       ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0023		        	,STR0023	     		,STR0023         	   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoObr             ,;				//Picture   //Valid          //USADO
				''				   		,'US2'  				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,'S'				   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME
				
aAdd(aGravaSX,{ 'FID'			   		,'04'      				,'FID_NOME'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,20 					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0024  			    ,STR0024	            ,STR0024              	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0024  			    ,STR0024	            ,STR0024              	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'V'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'05'      				,'FID_BLOQ'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0025		  			,STR0025		        ,STR0025	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0026				    ,STR0026				,STR0026				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'06'      				,'FID_DESC'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0027		  			,STR0027	            ,STR0027	         	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0028					,STR0028				,STR0028				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'07'      				,'FID_MULTA'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1 					    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0029     			,STR0029	            ,STR0029            	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0030				    ,STR0030				,STR0030				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'08'      				,'FID_MAXDES'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,10 				    ,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0031    		   		,STR0031		        ,STR0031	         	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0032					,STR0032				,STR0032				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@E 999,999.99'			,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'09'      				,'FID_MAXMUL'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,10 				    ,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0033		   			,STR0033		        ,STR0033	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0034				    ,STR0034				,STR0034				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@E 999,999.99'			,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'10'      				,'FID_SUPER'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0035		   			,STR0035			    ,STR0035	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0036			 		,STR0036				,STR0036				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'11'      				,'FID_ABRECX'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0037		   			,STR0037		        ,STR0037	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0038		 			,STR0038				,STR0038				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'12'      				,'FID_FECHCX'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0039   				,STR0039		        ,STR0039	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0040		 			,STR0040				,STR0040				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'13'      				,'FID_NCAIXA'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,TamSx3('A6_COD ')[1]	,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0041		   			,STR0041			    ,STR0041	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0041		 			,STR0041				,STR0041				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''						,''					  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'14'      				,'FID_TPDES'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0042		   			,STR0042			    ,STR0042	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0042		 			,STR0042				,STR0042				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0180				 	,STR0180				,STR0180			  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'15'      				,'FID_TPMUL'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0043   				,STR0043			    ,STR0043	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0043		 			,STR0043				,STR0043				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0180	 				,STR0180				,STR0180			  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'16'      				,'FID_ESTOR'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0044 	 			,STR0044	            ,STR0044	         	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0045				 	,STR0045				,STR0045				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
				
aAdd(aGravaSX,{ 'FID'			   		,'17'      				,'FID_JUROS'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0046		  			,STR0046	            ,STR0046	         	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0047				 	,STR0047				,STR0047				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME					

aAdd(aGravaSX,{ 'FID'			   		,'18'      				,'FID_MAXJUR'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,10 				    ,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0048    				,STR0048	        	,STR0048	         	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0049				  	,STR0049				,STR0049				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@E 999,999.99'			,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'19'      				,'FID_TPJUR'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0050		   			,STR0050			    ,STR0050	          	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0050		 			,STR0050				,STR0050				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0180	 				,STR0180				,STR0180			  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME					

aAdd(aGravaSX,{ 'FID'			   		,'20'      				,'FID_TROCOP'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,11  				    ,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0051			   		,STR0051			    ,STR0051	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0051		 			,STR0051				,STR0051				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@E 999,999.99'         ,''		  				,cUsadoOpc 				,;	//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''					 	,''						,''					  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME					
   
aAdd(aGravaSX,{ 'FID'			   		,'21'      				,'FID_CHEQP'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0193		  			,STR0193		        ,STR0193	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0194				    ,STR0194				,STR0194				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FID'			   		,'22'      				,'FID_QTDCHQ'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,3  				    ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0195			   		,STR0195			    ,STR0195	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0196		 			,STR0196				,STR0196				,;				//Descricao	//Descricao SPA  //Descricao ENG					'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				'@E 999'		         ,''		  			,cUsadoOpc 				,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''					 	,''						,''					  	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME					
				
//��������������������������Ŀ
//�Cria o campo SE1->E1_LTCXA�
//����������������������������	
aAdd(aGravaSX,{ 'SE1'			   		,GetOrdem('SE1','E1_LTCXA'),'E1_LTCXA'         ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0052         		,STR0052				,STR0052	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0053					,STR0053				,STR0053			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

//��������������������������Ŀ
//�Cria o campo SE1->E1_NOPER�
//����������������������������	
aAdd(aGravaSX,{ 'SE1'			   		,GetOrdem('SE1','E1_NOPER'),'E1_NOPER'         ,;               //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,11        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0054	        		,STR0054				,STR0054	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0054					,STR0054				,STR0054			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999'		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME 
				
/*
//��������������������������Ŀ
//�Cria o campo SE5->E5_LTCXA�
//����������������������������	
aAdd(aGravaSX,{ 'SE5'			   		,GetOrdem('SE5','E5_LTCXA'),'E5_LTCXA'         ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0052         		,STR0052				,STR0052	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0053					,STR0053				,STR0053			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME 

//��������������������������Ŀ
//�Cria o campo SE5->E1_NOPER�
//����������������������������	
aAdd(aGravaSX,{ 'SE5'			   		,GetOrdem('SE5','E5_NOPER'),'E5_NOPER'         ,;               //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,11        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0054	        		,STR0054				,STR0054	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0054					,STR0054				,STR0054			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999'		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME
*/	

//��������������������������Ŀ
//�Cria o campo SEF->EF_LTCXA�
//����������������������������	
aAdd(aGravaSX,{ 'SEF'			   		,GetOrdem('SEF','EF_LTCXA'),'EF_LTCXA'         ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0052         		,STR0052				,STR0052	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0053					,STR0053				,STR0053			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

//��������������������������Ŀ
//�Cria o campo SEF->EF_NOPER�
//����������������������������	
aAdd(aGravaSX,{ 'SEF'			   		,GetOrdem('SEF','EF_NOPER'),'EF_NOPER'         ,;               //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,11        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0054	        		,STR0054				,STR0054	           ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0054					,STR0054				,STR0054			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999'		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME								

//���������������������������Ŀ
//�Cria o campo SEF->EF_IDOCPR�
//�����������������������������	
aAdd(aGravaSX,{ 'SEF'			   		,GetOrdem('SEF','EF_IDOCPR'),'EF_IDOCPR'    ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,12        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0220	    			,STR0220				,STR0220			    ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0220					,STR0220				,STR0220			   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME								

//����������������������������������������������Ŀ
//�Grava  cada campo existende no aGravaSx no SX3�
//������������������������������������������������      
dbSelectArea('SX3')
SX3->(dbSetOrder(2))
For nI:= 1 to len(aGravaSX)
	lExist := SX3->(dbSeek(aGravaSX[nI,3]))	
	RecLock("SX3",!lExist)
	For nJ:=1 To Len(aGravaSX[nI])
		If FieldPos(aEstrut[nJ])>0 .And. aGravaSX[nI,nJ] != NIL
			FieldPut(FieldPos(aEstrut[nJ]),aGravaSX[nI,nJ])
		EndIf
	Next nJ
	SX3->(msUnLock())
	
	cTexto += STR0221 + alltrim(SX3->X3_CAMPO) + cPulaLin	//"Criado o campo: "
Next nI
cTexto += STR0055 + cEmp  + cPulaLin //"Criado os campos da tabela FID na empresa: " 
       

//�����������������������������Ŀ
//�Cria os indices da FID no SIX�
//�������������������������������
dbSelectArea('SIX')
SIX->(dbSetOrder(1))  
If SIX->(!dbSeek('FID1'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FID'
	SIX->ORDEM  := '1'
	SIX->CHAVE  := 'FID_FILIAL+FID_USER'
	SIX->DESCRICAO := STR0056  //'Filial + Usuario'
	SIX->DESCSPA   := STR0056  //'Filial + Usuario'
	SIX->DESCENG   := STR0056  //'Filial + Usuario'
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0057 + cEmp  + cPulaLin  //"Criado o indice 01 da tabela FID na empresa: "
endif
If SIX->(!dbSeek('FID2'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FID'
	SIX->ORDEM  := '2'
	SIX->CHAVE  := 'FID_FILIAL+FID_NCAIXA'
	SIX->DESCRICAO := STR0058 //'Filial + Caixa'
	SIX->DESCSPA   := STR0058 //'Filial + Caixa'
	SIX->DESCENG   := STR0058 //'Filial + Caixa'
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0059 + cEmp  + cPulaLin //"Criado o indice 02 da tabela FID na empresa: "
endif


//����������������������������������������������������������������������������������������������Ŀ
//�******************************************* FIB **********************************************�
//������������������������������������������������������������������������������������������������

//������������������������Ŀ
//�Cria a tabela FIB no SX2�
//��������������������������
dbSelectArea('SX2')
SX2->(dbSetOrder(1))
if SX2->(!dbSeek('FIB'))
	RecLock('SX2',.T.)
	SX2->X2_CHAVE   := 'FIB'
	SX2->X2_PATH    := ''
	SX2->X2_ARQUIVO := 'FIB'+alltrim(cEmp)+'0'
	SX2->X2_NOME    := STR0060 //'Header Lote Caixa'
	SX2->X2_NOMESPA := STR0060 //'Header Lote Caixa'
	SX2->X2_NOMEENG := STR0060 //'Header Lote Caixa'
	SX2->X2_MODO    := 'E'
	SX2->X2_DELET   := 0
	SX2->X2_TTS     := ''
	SX2->X2_UNICO   := 'FIB_FILIAL+FIB_LOTE'
	SX2->X2_PYME    := 'S'
	SX2->(msUnlock())  
	cTexto += STR0061 + cEmp  + cPulaLin //"Criada a tabela FIB na empresa: "
endif

//����������������������������Ŀ
//�Cria os campos da FIB no SX3�
//������������������������������
dbSelectArea('SX3')
SX3->(dbSetOrder(2))

aGravaSx := {}	
aAdd(aGravaSX,{ 'FIB'			   		,'01'      				,'FIB_FILIAL'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,2         				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0019           		,STR0019				,STR0019              	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0020					,STR0020				,STR0020				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             	,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
   
aAdd(aGravaSX,{ 'FIB'			   		,'02'      				,'FIB_LOTE'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10						,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0062 				,STR0062                ,STR0062                ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0063			        ,STR0063		        ,STR0063		        ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoKey             	,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservKey		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'03'      				,'FIB_USER'            ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,6        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0064		       		,STR0064			    ,STR0064 		       ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0064		        	,STR0064	      		,STR0064         	   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoObr             ,;				//Picture   //Valid          //USADO
				''				   		,''      				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,'S'				   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME
				
aAdd(aGravaSX,{ 'FIB'			   		,'04'      				,'FIB_NOME'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,20 					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0024  			    ,STR0024	            ,STR0024              	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0024  			    ,STR0024	            ,STR0024             	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'V'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'V'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'05'      				,'FIB_DTABR'           ,;               //Arquivo,  //Ordem,         //Campo,
				'D'     		   		,8        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0065			       	,STR0065		    	,STR0065		       ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0066			        ,STR0066		   		,STR0066		   	   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoObr             ,;				//Picture   //Valid          //USADO
				''				   		,''      				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,'' 				   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'06'      				,'FIB_HRABR' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,5        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0067        			,STR0067				,STR0067		       	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0067			 		,STR0067				,STR0067			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'07'      				,'FIB_VLRABR' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0068			        ,STR0068				,STR0068		      	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0068			 		,STR0068				,STR0068			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'08'      				,'FIB_DTFCH'           ,;               //Arquivo,  //Ordem,         //Campo,
				'D'     		   		,8        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0069			       	,STR0069				,STR0069		       ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0069			        ,STR0069		  		,STR0069		  	   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoOpc             ,;				//Picture   //Valid          //USADO
				''				   		,''      				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''				       ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'09'      				,'FIB_HRFCH' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,5        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0070			     	,STR0070				,STR0070		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0070			 		,STR0070				,STR0070			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'10'      				,'FIB_DTCONC'          ,;               //Arquivo,  //Ordem,         //Campo,
				'D'     		   		,8        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0071			      	,STR0071				,STR0071			   ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0071			        ,STR0071		 		,STR0071		 	   ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoOpc             ,;				//Picture   //Valid          //USADO
				''				   		,''      				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''				       ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'11'      				,'FIB_SITCONC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0072	     	        ,STR0072		        ,STR0072	    	    ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0073			 		,STR0073				,STR0073			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'12'      				,'FIB_VLRDN' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0074			      	,STR0074				,STR0074		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0075			 		,STR0075				,STR0075			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'13'      				,'FIB_VLRCH' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0076			      	,STR0076				,STR0076		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0077			 		,STR0077				,STR0077			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'14'      				,'FIB_VLRCC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0078  		    	,STR0078				,STR0078		      	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0079					,STR0079				,STR0079				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'15'      				,'FIB_VLRCD' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0080			      	,STR0080				,STR0080		      	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0081					,STR0081				,STR0081			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'16'      				,'FIB_VLRNC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0082			      	,STR0082				,STR0082		      	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0082					,STR0082				,STR0082			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
				
aAdd(aGravaSX,{ 'FIB'			   		,'17'      				,'FIB_TPABRE' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0083			      	,STR0083				,STR0083		      	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0084					,STR0084				,STR0084			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0181					,STR0181		  		,STR0181			   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'18'      				,'FIB_TPFECH' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1        				,0                    	,;              //Tipo,     //Tamanho,       //Decimal
				STR0085			      	,STR0085				,STR0085		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0086					,STR0086				,STR0086			  	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0181					,STR0181		  		,STR0181			   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'19'      				,'FIB_USABRE' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0087			      	,STR0087				,STR0087		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0088					,STR0088				,STR0088			  	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''						,''		  		  		,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIB'			   		,'20'      				,'FIB_USFECH' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0089      			,STR0089				,STR0089			   	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0090					,STR0090				,STR0090			  	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,''			  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''						,''		  		  		,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
				
aAdd(aGravaSX,{ 'FIB'			   		,'21'      				,'FIB_ENCER'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0091		 	 		,STR0091		        ,STR0091		        ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0092					,STR0092				,STR0092				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
      
//����������������������������������������������Ŀ
//�Grava  cada campo existende no aGravaSx no SX3�
//������������������������������������������������
dbSelectArea('SX3')
SX3->(dbSetOrder(2))
For nI:= 1 to len(aGravaSX)

	lExist := SX3->(dbSeek(aGravaSX[nI,3]))

	RecLock("SX3",!lExist)
	For nJ:=1 To Len(aGravaSX[nI])
		If FieldPos(aEstrut[nJ])>0 .And. aGravaSX[nI,nJ] != NIL
			FieldPut(FieldPos(aEstrut[nJ]),aGravaSX[nI,nJ])
		EndIf
	Next nJ
	SX3->(msUnLock())                                               
	
	cTexto += "Criado o campo: " + alltrim(SX3->X3_CAMPO) + cPulaLin
Next nI
cTexto += STR0093 + cEmp  + cPulaLin //"Criado os campos da tabela FIB na empresa: "  

//�����������������������������Ŀ
//�Cria os indices da FIB no SIX�
//�������������������������������
dbSelectArea('SIX')
SIX->(dbSetOrder(1))  
If SIX->(!dbSeek('FIB1'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FIB'
	SIX->ORDEM  := '1'
	SIX->CHAVE  := 'FIB_FILIAL+FIB_LOTE'
	SIX->DESCRICAO := STR0094 //'Filial + Lote'
	SIX->DESCSPA   := STR0094 //'Filial + Lote'
	SIX->DESCENG   := STR0094 //'Filial + Lote'
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0095 + cEmp  + cPulaLin  //"Criado o indice 01 da tabela FIB na empresa: "
endif
If SIX->(!dbSeek('FIB2'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FIB'
	SIX->ORDEM  := '2'
	SIX->CHAVE  := 'FIB_FILIAL+DTOS(FIB_DTABR) + FIB_LOTE'
	SIX->DESCRICAO := STR0096 //'Filial + Abertura + Lote'
	SIX->DESCSPA   := STR0096 //'Filial + Abertura + Lote '
	SIX->DESCENG   := STR0096 //'Filial + Abertura + Lote'
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0097 + cEmp  + cPulaLin  //"Criado o indice 02 da tabela FIB na empresa: "
endif 
If SIX->(!dbSeek('FIB3'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FIB'
	SIX->ORDEM  := '3'
	SIX->CHAVE  := 'FIB_FILIAL+FIB_USER'
	SIX->DESCRICAO := STR0098 //'Filial + Operador '
	SIX->DESCSPA   := STR0098 //'Filial + Operador '
	SIX->DESCENG   := STR0098 //'Filial + Operador '
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0099 + cEmp  + cPulaLin  //"Criado o indice 03 da tabela FIB na empresa: "
endif

    
//����������������������������������������������������������������������������������������������Ŀ
//�******************************************* FIC **********************************************�
//������������������������������������������������������������������������������������������������

//������������������������Ŀ
//�Cria a tabela FIC no SX2�
//��������������������������
dbSelectArea('SX2')
SX2->(dbSetOrder(1))
if SX2->(!dbSeek('FIC'))
	RecLock('SX2',.T.)
	SX2->X2_CHAVE   := 'FIC'
	SX2->X2_PATH    := ''
	SX2->X2_ARQUIVO := 'FIC'+alltrim(cEmp)+'0'
	SX2->X2_NOME    := STR0100 //'Itens Lote Caixa'
	SX2->X2_NOMESPA := STR0100 //'Itens Lote Caixa'
	SX2->X2_NOMEENG := STR0100 //'Itens Lote Caixa'
	SX2->X2_MODO    := 'E'
	SX2->X2_DELET   := 0
	SX2->X2_TTS     := ''
	SX2->X2_UNICO   := 'FIC_FILIAL+FIC_LOTE+FIC_TRANS'
	SX2->X2_PYME    := 'S'
	SX2->(msUnlock())  
	cTexto += STR0101 + cEmp  + cPulaLin //"Criada a tabela FIC na empresa: "
endif
                                                                                                  
//����������������������������Ŀ
//�Cria os campos da FIC no SX3�
//������������������������������
dbSelectArea('SX3')
SX3->(dbSetOrder(2))

aGravaSx := {}	
aAdd(aGravaSX,{ 'FIC'			   		,'01'      				,'FIC_FILIAL'          ,;               //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,2         				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0019           		,STR0019				,STR0019               ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0020					,STR0020				,STR0020				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoNao             ,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   ,;				//RELACAO	//F3   			 //NIVEL
				cReservNao		   		,''		  				,''					   ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME
                                                                                                  
aAdd(aGravaSX,{ 'FIC'			   		,'02'      				,'FIC_FLGSIT'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,2						,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0102				    ,STR0102                ,STR0102	            ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0103		     	    ,STR0103        	  	,STR0103	  	        ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@BMP'					,''		  				,cUsadoObr             	,;				//Picture   //Valid          //USADO
				''						,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'V'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,'Fn223Leg()'		   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
									
aAdd(aGravaSX,{ 'FIC'			   		,'03'      				,'FIC_LOTE'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,10						,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0104 				,STR0104                ,STR0104                ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0105			        ,STR0105		        ,STR0105		        ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoObr             	,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME
									
aAdd(aGravaSX,{ 'FIC'			   		,'04'      				,'FIC_TRANS'         	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,11						,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0106		 			,STR0106	            ,STR0106	            ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0107				    ,STR0107 			    ,STR0107 			    ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999'		,''		  				,cUsadoObr             	,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'05'      				,'FIC_CODCXA'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,TamSx3('A6_COD ')[1]  ,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0108 				,STR0108                ,STR0108 	            ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0108	         	    ,STR0108        	  	,STR0108	   	        ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoObr             	,;				//Picture   //Valid          //USADO
				''						,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'V'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,'Fn223Cxa()'		   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME					
     
aAdd(aGravaSX,{ 'FIC'			   		,'06'      				,'FIC_VLRREC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0109			        ,STR0109				,STR0109		    	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0109			 		,STR0109				,STR0109			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'07'      				,'FIC_TPPAG'         	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1						,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0110 	 			,STR0110 		        ,STR0110 	            ,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0111			        ,STR0111  			    ,STR0111		        ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,'Pertence("123456")'	,cUsadoObr             	,;				//Picture   //Valid          //USADO
				''				   		,''		  				,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				STR0182 				,STR0182				,STR0182				,;				//CBOX,     //CBOX SPA,      //CBOX ENG  ->"1=Dinheiro;2=Cheque;3=C. Debito;4=C. Credito;5=NCC;6=Mix"
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'08'      				,'FIC_DTREC'           ,;               //Arquivo,  //Ordem,         //Campo,
				'D'     		   		,8        				,0                     ,;               //Tipo,     //Tamanho,       //Decimal
				STR0112			        ,STR0112				,STR0112			   ,;               //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0112			        ,STR0112		 	  	,STR0112  		       ,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''		  				,cUsadoObr             ,;				//Picture   //Valid          //USADO
				''				   		,''      				,1  				   ,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''				       ,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   ,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   ,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   ,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   ,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   })				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'09'      				,'FIC_HRREC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,5        				,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0113			     	,STR0113				,STR0113			   	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0113			 		,STR0113				,STR0113			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'			   		,''						,cUsadoObr 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservObr		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'S'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'10'      				,'FIC_ATIVO'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,1  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0114		  			,STR0114		        ,STR0114	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0114				    ,STR0114	  			,STR0114				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				'1=SIM;2=NAO'	   		,'1=SI;2=NO'			,'1=YES;2=NO'		   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'11'      				,'FIC_CARTR'           	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,40  					,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0115		  			,STR0115		        ,STR0115	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0115				    ,STR0115		  		,STR0115				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''						,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'12'      				,'FIC_CAROPE'          	,;              //Arquivo,  //Ordem,         //Campo,
				'C'     		   		,TamSx3('A1_COD')[1] 	,0                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0116			  		,STR0116		        ,STR0116	        	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0116				    ,STR0116		 		,STR0116				,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@!'					,''		  				,cUsadoOpc             	,;				//Picture   //Valid          //USADO
				''				   		,''                     ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	           	//CONTEXT,  //OBRIGAT,       //VLDUSER
				''				   		,''						,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME    

aAdd(aGravaSX,{ 'FIC'			   		,'13'      				,'FIC_MIXDIN' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0183        			,STR0183				,STR0183		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0184 				,STR0184				,STR0184			   	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'14'      				,'FIC_MIXCHQ' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0185        			,STR0185				,STR0185	       		,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0186 				,STR0186   				,STR0186			  	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME

aAdd(aGravaSX,{ 'FIC'			   		,'15'      				,'FIC_MIXDEB' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0187      			,STR0187				,STR0187		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0188 				,STR0188   				,STR0188			  	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME  

aAdd(aGravaSX,{ 'FIC'			   		,'16'      				,'FIC_MIXCRD' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0189      	   		,STR0189		  		,STR0189		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0190 				,STR0190				,STR0190 	 			,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME  

aAdd(aGravaSX,{ 'FIC'			   		,'17'      				,'FIC_MIXNCC' 	       	,;              //Arquivo,  //Ordem,         //Campo,
				'N'     		   		,12        				,2                     	,;              //Tipo,     //Tamanho,       //Decimal
				STR0191      			,STR0191				,STR0191		     	,;              //Titulo,   //Titulo SPA,    //Titulo ENG
				STR0192 				,STR0192				,STR0192			 	,;				//Descricao	//Descricao SPA  //Descricao ENG
				'@E 999,999,999.99'		,''						,cUsadoOpc 	           	,;				//Picture   //Valid          //USADO
				''				   		,''				        ,1					   	,;				//RELACAO	//F3   			 //NIVEL
				cReservOpc		   		,''		  				,''					   	,;				//RESERV    //CHECK,         //TRIGGER
				''				   		,'N'		  			,'A'				   	,;				//PROPRI,  	//BROWSE, 		 //VISUAL
				'R'				   		,''		  				,''					   	,;	            //CONTEXT,  //OBRIGAT,       //VLDUSER
				''				  		,''		  				,''					   	,;				//CBOX,     //CBOX SPA,      //CBOX ENG
				''				   		,''		  				,''					   	,;				//PICTVAR,  //WHEN, 		 //INIBRW 		
				''				   		,''		  				,''					   	})				//SXG, 	   	//FOLDER, 		 //PYME  
				
//����������������������������������������������Ŀ
//�Grava  cada campo existende no aGravaSx no SX3�
//������������������������������������������������
dbSelectArea('SX3')
SX3->(dbSetOrder(2))

For nI:= 1 to len(aGravaSX)
	lExist := SX3->(dbSeek(aGravaSX[nI,3]))
	
	RecLock("SX3",!lExist)
	For nJ:=1 To Len(aGravaSX[nI])
		If FieldPos(aEstrut[nJ])>0 .And. aGravaSX[nI,nJ] != NIL
			FieldPut(FieldPos(aEstrut[nJ]),aGravaSX[nI,nJ])
		EndIf
	Next nJ
	SX3->(msUnLock())
	cTexto += "Criado o campo: " + alltrim(SX3->X3_CAMPO) + cPulaLin
Next nI
cTexto += STR0117 + cEmp  + cPulaLin //"Criado os campos da tabela FIC na empresa: "                                                                         

//�����������������������������Ŀ
//�Cria os indices da FIC no SIX�
//�������������������������������
dbSelectArea('SIX')
SIX->(dbSetOrder(1))   
SIX->(dbSetOrder(1))  
If SIX->(!dbSeek('FIC1'))
	RecLock('SIX',.T.)
	SIX->INDICE := 'FIC'
	SIX->ORDEM  := '1'
	SIX->CHAVE  := 'FIC_FILIAL+FIC_LOTE+FIC_TRANS'
	SIX->DESCRICAO := STR0118 //'Filial + Lote + Transa��o'
	SIX->DESCSPA   := STR0118 //'Filial + Lote + Transa��o'
	SIX->DESCENG   := STR0118 //'Filial + Lote + Transa��o'
	SIX->PROPRI    := 'S'
	SIX->SHOWPESQ  := 'S'
	SIX->(msUnlock())
	cTexto += STR0119 + cEmp  + cPulaLin  //"Criado o indice 01 da tabela FIC na empresa: " 
endif

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41Atu   �Autor  �Cesar A. Bianchi 	 � Data �  09/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Realiza a sincronizacao entre o SX2/SX3 e o banco de dados  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41Atu()
Local aAlias := {'FIC','FID','FIB','SE1','SEF'}
Local nI     := 1
    
//�����������������������������������������������������������������Ŀ
//�Para cada alias presente no array aAlias, executa a sincronizacao�
//�������������������������������������������������������������������
For nI := 1 to len(aAlias)
	//Fecha a area caso esteja aberta
	if Select(aAlias[nI])>0
	    (aAlias[nI])->(dbCloseArea())
	endif
	
	//Atualiza o banco de dados
	X31UpdTable(aAlias[nI])
	TcRefresh(aAlias[nI])
	dbSelectArea(aAlias[nI])
	(aAlias[nI])->(dbSetOrder(1))
	(aAlias[nI])->(dbCloseArea())
	dbSelectArea(aAlias[nI])
Next nI

Return Nil
          
		
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41SX7   �Autor  �Cesar A. Bianchi    � Data �  31/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria os novos gatilhos para o campo FID_USER                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41Sx7()
Local aSX7      := {}						//Array que armazenara os indices
Local aEstrut   := {}				        //Array com a estrutura da tabela SX7
Local i         := 0 						//Contador para laco
Local j         := 0 						//Contador para laco   

//�������������������������Ŀ
//�Define a estrutura da SX7�
//���������������������������
aEstrut:= { "X7_CAMPO","X7_SEQUENC","X7_REGRA","X7_CDOMIN","X7_TIPO","X7_SEEK","X7_ALIAS","X7_ORDEM","X7_CHAVE","X7_CONDIC","X7_PROPRI"}

//�����������������������������������Ŀ
//�Define os gatilhos a serem gravados�
//�������������������������������������
aAdd(aSx7,{"FID_USER  ","001","ACDescUsu('FID_USER')","FID_NOME","P","N"," ",0," "," ","S"})
aAdd(aSx7,{"FIB_USER  ","001","ACDescUsu('FIB_USER')","FIB_NOME","P","N"," ",0," "," ","S"})

dbSelectArea("SX7")
SX7->(dbSetOrder(1)) //X7_CAMPO + X7_SEQUENC 
For i := 1 To Len(aSX7)
	If SX7->(dbSeek(aSx7[i][1] + aSx7[i][2]))
		RecLock("SX7",.F.)	
	Else
		RecLock("SX7",.T.)
		cTexto += STR0120+alltrim(aSx7[i][2])+STR0121 + aSx7[i][1] + cPulaLin //"Criado o gatilho "" para o campo "
	EndIf	
		
	For j:=1 To Len(aSX7[i])
		If FieldPos(aEstrut[j]) > 0
			FieldPut(FieldPos(aEstrut[j]),aSX7[i,j])
		EndIf
	Next j
	SX7->(MsUnLock())
Next i                                                            


Return

                   
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41SX6   �Autor  �Cesar A. Bianchi    � Data �  31/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria novo parametros no SX6                                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41SX6()
      
SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINFPGT")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINFPGT"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0122 //"Formas de pagamento disponiveis para a rotina"
	SX6->X6_DSCSPA  := STR0122 //"Formas de pagamento disponiveis para a rotina"
	SX6->X6_DSCENG  := STR0122 //"Formas de pagamento disponiveis para a rotina"
	SX6->X6_DESC1   := STR0123 //"FINA220, separados sempre por | (pipe)" 
	SX6->X6_DSCSPA1 := STR0123 //"FINA220, separados sempre por | (pipe)" 
	SX6->X6_DSCENG1 := STR0123 //"FINA220, separados sempre por | (pipe)" 
	SX6->X6_DESC2   := "1-Dinheiro,2-Cheque-3-C.Credito,4-C.Debito,5-NCC,6-MIX"
	SX6->X6_DSCSPA2 := "1-Dinheiro,2-Cheque-3-C.Credito,4-C.Debito,5-NCC,6-MIX"
	SX6->X6_DSCENG2 := "1-Dinheiro,2-Cheque-3-C.Credito,4-C.Debito,5-NCC,6-MIX"
	if ExistBlock("Fn220NCC")
		//So cria com o item 5-Ncc caso tenha PE, pois o controle eh por conta do usuario
		SX6->X6_CONTEUD := "|1|2|3|4|5|6|"
		SX6->X6_CONTSPA := "|1|2|3|4|5|6|"
		SX6->X6_CONTENG := "|1|2|3|4|5|6|"
	Else
		SX6->X6_CONTEUD := "|1|2|3|4|6|"
		SX6->X6_CONTSPA := "|1|2|3|4|6|"
		SX6->X6_CONTENG := "|1|2|3|4|6|"	
	endif
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0203 + cPulaLin //"Criado o parametro MV_FINFPGT"
EndIf

If SX6->( !DbSeek("  "+"MV_FINCHQC"))
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINCHQC"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0125 //"Nome do municipio padrao impresso no cheque FIN220"
	SX6->X6_DSCSPA  := STR0125 //"Nome do municipio padrao impresso no cheque FIN220"
	SX6->X6_DSCENG  := STR0125 //"Nome do municipio padrao impresso no cheque FIN220"
	SX6->X6_DESC1   := STR0126 //"rotina FINA220"
	SX6->X6_DSCSPA1 := STR0126 //"rotina FINA220"
	SX6->X6_DSCENG1 := STR0126 //"rotina FINA220"
	SX6->X6_CONTEUD := "S�O PAULO"
	SX6->X6_CONTSPA := "S�O PAULO"
	SX6->X6_CONTENG := "S�O PAULO"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )

	cTexto += STR0204 + cPulaLin  //"Criado o parametro MV_FINCHQC"
EndIf
     
SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINAUT")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINAUT"
	SX6->X6_TIPO	:= "L"
	SX6->X6_DESCRIC := STR0127 //"Realizar autentica��o de titulos na FINA220"
	SX6->X6_DSCSPA  := STR0127 //"Realizar autentica��o de titulos na FINA220"
	SX6->X6_DSCENG  := STR0127 //"Realizar autentica��o de titulos na FINA220"
	SX6->X6_DESC1   := STR0128 //".T. = SIM, .F. = N�O" 
	SX6->X6_DSCSPA1 := STR0128 //".T. = SIM, .F. = N�O" 
	SX6->X6_DSCENG1 := STR0128 //".T. = SIM, .F. = N�O" 
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := ".T."
	SX6->X6_CONTSPA := ".T."
	SX6->X6_CONTENG := ".T."
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0205 + cPulaLin //"Criado o parametro MV_FINAUT"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINPSQ")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINPSQ"
	SX6->X6_TIPO	:= "L"
	SX6->X6_DESCRIC := STR0129 //"Considerar intervalo venc. na pesquisa"
	SX6->X6_DSCSPA  := STR0129 //"Considerar intervalo venc. na pesquisa"
	SX6->X6_DSCENG  := STR0129 //"Considerar intervalo venc. na pesquisa"
	SX6->X6_DESC1   := STR0130 //"da Rotina FINA220" 
	SX6->X6_DSCSPA1 := STR0130 //"da Rotina FINA220" 
	SX6->X6_DSCENG1 := STR0130 //"da Rotina FINA220" 
	SX6->X6_DESC2   := STR0128 //".T. = SIM, .F. = N�O"
	SX6->X6_DSCSPA2 := STR0128 //".T. = SIM, .F. = N�O"
	SX6->X6_DSCENG2 := STR0128 //".T. = SIM, .F. = N�O"
	SX6->X6_CONTEUD := ".T."
	SX6->X6_CONTSPA := ".T."
	SX6->X6_CONTENG := ".T."
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )

	cTexto += STR0206 + cPulaLin //"Criado o parametro MV_FINPSQ"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINPRE")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINPRE"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0131 //"Prefixo do t�tulo de matricula para Analise de "
	SX6->X6_DSCSPA  := STR0131 //"Prefixo do t�tulo de matricula para Analise de "
	SX6->X6_DSCENG  := STR0131 //"Prefixo do t�tulo de matricula para Analise de "
	SX6->X6_DESC1   := STR0132 //"t�tulos de inadimplentes na rotina FINA220 " 
	SX6->X6_DSCSPA1 := STR0132 //"t�tulos de inadimplentes na rotina FINA220 " 
	SX6->X6_DSCENG1 := STR0132 //"t�tulos de inadimplentes na rotina FINA220 " 
	SX6->X6_DESC2   := STR0133 //"(vazio caso a verificacao nao � necessaria)"
	SX6->X6_DSCSPA2 := STR0133 //"(vazio caso a verificacao nao � necessaria)"
	SX6->X6_DSCENG2 := STR0133 //"(vazio caso a verificacao nao � necessaria)"
	SX6->X6_CONTEUD := "MAT"
	SX6->X6_CONTSPA := "MAT"
	SX6->X6_CONTENG := "MAT"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0207 + cPulaLin //"Criado o parametro MV_FINPRE"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINVERI")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINVERI"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0134 //"Prefixos dos titulos que nao podem ser baixados "
	SX6->X6_DSCSPA  := STR0134 //"Prefixos dos titulos que nao podem ser baixados"
	SX6->X6_DSCENG  := STR0134 //"Prefixos dos titulos que nao podem ser baixados"
	SX6->X6_DESC1   := STR0135 //"caso existam titulos de matricula em aberto" 
	SX6->X6_DSCSPA1 := STR0135 //"caso existam titulos de matricula em aberto" 
	SX6->X6_DSCENG1 := STR0135 //"caso existam titulos de matricula em aberto" 
	SX6->X6_DESC2   := STR0136 //"(MV_FINPRE) separados por | (vazio para todos)"
	SX6->X6_DSCSPA2 := STR0136 //"(MV_FINPRE) separados por | (vazio para todos)"
	SX6->X6_DSCENG2 := STR0136 //"(MV_FINPRE) separados por | (vazio para todos)"
	SX6->X6_CONTEUD := ""
	SX6->X6_CONTSPA := ""
	SX6->X6_CONTENG := ""
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0208 + cPulaLin //"Criado o parametro MV_FINVERI"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_ADMPRE")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_ADMPRE"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0137 //"Prefixo|Tipo|Natureza do t�t. gerado"
	SX6->X6_DSCSPA  := STR0137 //"Prefixo|Tipo|Natureza do t�t. gerado"
	SX6->X6_DSCENG  := STR0137 //"Prefixo|Tipo|Natureza do t�t. gerado"
	SX6->X6_DESC1   := STR0138 //"para a Administradora Financeira" 
	SX6->X6_DSCSPA1 := STR0138 //"para a Administradora Financeira" 
	SX6->X6_DSCENG1 := STR0138 //"para a Administradora Financeira" 
	SX6->X6_DESC2   := STR0139 //"FINA220 (separados por pipe | )"
	SX6->X6_DSCSPA2 := STR0139 //"FINA220 (separados por pipe | )"
	SX6->X6_DSCENG2 := STR0139 //"FINA220 (separados por pipe | )"
	SX6->X6_CONTEUD := "CAR|BOL|MENSALIDAD"
	SX6->X6_CONTSPA := "CAR|BOL|MENSALIDAD"
	SX6->X6_CONTENG := "CAR|BOL|MENSALIDAD"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0209 + cPulaLin //"Criado o parametro MV_ADMPRE"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_CHEPRE")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_CHEPRE"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0140 //"Prefixo|Tipo|Natureza do t�t. gerado "
	SX6->X6_DSCSPA  := STR0140 //"Prefixo|Tipo|Natureza do t�t. gerado "
	SX6->X6_DSCENG  := STR0140 //"Prefixo|Tipo|Natureza do t�t. gerado "
	SX6->X6_DESC1   := STR0141 //"quando baixa for por Cheques em " 
	SX6->X6_DSCSPA1 := STR0141 //"quando baixa for por Cheques em " 
	SX6->X6_DSCENG1 := STR0141 //"quando baixa for por Cheques em " 
	SX6->X6_DESC2   := STR0142 //"FINA220 (separados por pipe | )"
	SX6->X6_DSCSPA2 := STR0142 //"FINA220 (separados por pipe | )"
	SX6->X6_DSCENG2 := STR0142 //"FINA220 (separados por pipe | )"
	SX6->X6_CONTEUD := "CH |CH |MENSALIDAD"
	SX6->X6_CONTSPA := "CH |CH |MENSALIDAD"
	SX6->X6_CONTENG := "CH |CH |MENSALIDAD"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0210 + cPulaLin //"Criado o parametro MV_CHEPRE"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINCUST")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINCUST"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DSCSPA  := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DSCENG  := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DESC1   := STR0168 //"custodia,separados por pipe | (FINA220)" 
	SX6->X6_DSCSPA1 := STR0168 //"custodia separados por pipe | (FINA220)" 
	SX6->X6_DSCENG1 := STR0168 //"custodia separados por pipe | (FINA220)" 
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := "1|2|3|4|5|6|7|8|9"
	SX6->X6_CONTSPA := "1|2|3|4|5|6|7|8|9"
	SX6->X6_CONTENG := "1|2|3|4|5|6|7|8|9"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->(MsUnlock())
	
	cTexto += STR0211 + cPulaLin //"Criado o parametro MV_FINCUST"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINCOBR")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINCOBR"
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DSCSPA  := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DSCENG  := STR0167 //"Tipos de situacao que indicam um titulo como"
	SX6->X6_DESC1   := STR0169 //"cobranca,separados por pipe | (FINA220)" 
	SX6->X6_DSCSPA1 := STR0169 //"cobranca separados por pipe | (FINA220)" 
	SX6->X6_DSCENG1 := STR0169 //"cobranca separados por pipe | (FINA220)" 
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := "A|B|C|D|E|F"
	SX6->X6_CONTSPA := "A|B|C|D|E|F"
	SX6->X6_CONTENG := "A|B|C|D|E|F"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->(MsUnlock())
	
	cTexto += STR0212 + cPulaLin //"Criado o parametro MV_FINCOBR"
EndIf
     
SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+"MV_FINPSGR")) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := "MV_FINPSGR"
	SX6->X6_TIPO	:= "L"
	SX6->X6_DESCRIC := STR0170 //"Bloqueia a opcao de realizar a sangria durante o"
	SX6->X6_DSCSPA  := STR0170 //"Bloqueia a opcao de realizar a sangria durante o"
	SX6->X6_DSCENG  := STR0170 //"Bloqueia a opcao de realizar a sangria durante o"
	SX6->X6_DESC1   := STR0171 //"fechamento de caixa - Fina223"
	SX6->X6_DSCSPA1 := STR0171 //"fechamento de caixa - Fina223" 
	SX6->X6_DSCENG1 := STR0171 //"fechamento de caixa - Fina223" 
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := ".F."
	SX6->X6_CONTSPA := ".F."
	SX6->X6_CONTENG := ".F."
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0213 + cPulaLin //"Criado o parametro MV_FINPSGR"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+'MV_FINTRC')) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := 'MV_FINTRC'
	SX6->X6_TIPO	:= "L"
	SX6->X6_DESCRIC := STR0172 //"Verifica troco no fechamento de caixa FINA222"
	SX6->X6_DSCSPA  := STR0172 //"Verifica troco no fechamento de caixa FINA222"
	SX6->X6_DSCENG  := STR0172 //"Verifica troco no fechamento de caixa FINA222"
	SX6->X6_DESC1   := STR0173 //".T. = SIM / .F. = NAO"
	SX6->X6_DSCSPA1 := STR0173 //".T. = SIM / .F. = NAO"
	SX6->X6_DSCENG1 := STR0173 //".T. = SIM / .F. = NAO"
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := ".T."
	SX6->X6_CONTSPA := ".T."
	SX6->X6_CONTENG := ".T."
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0214 + cPulaLin //"Criado o parametro MV_FINTRC"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+'MV_FINQTDD')) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := 'MV_FINQTDD'
	SX6->X6_TIPO	:= "N"
	SX6->X6_DESCRIC := STR0174 //"Quantidade de dias que antecedem o vencto para"
	SX6->X6_DSCSPA  := STR0174 //"Quantidade de dias que antecedem o vencto para"
	SX6->X6_DSCENG  := STR0174 //"Quantidade de dias que antecedem o vencto para"
	SX6->X6_DESC1   := STR0175 //"utiliza��o no desconto condicional do FINA220"
	SX6->X6_DSCSPA1 := STR0175 //"utiliza��o no desconto condicional do FINA220"
	SX6->X6_DSCENG1 := STR0175 //"utiliza��o no desconto condicional do FINA220"
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := "0"
	SX6->X6_CONTSPA := "0"
	SX6->X6_CONTENG := "0"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0215 + cPulaLin //"Criado o parametro MV_FINQTDD"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+'MV_FINPRDS')) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := 'MV_FINPRDS'
	SX6->X6_TIPO	:= "N"
	SX6->X6_DESCRIC := STR0176 //"Percentual de desconto utilizado na regra de"
	SX6->X6_DSCSPA  := STR0176 //"Percentual de desconto utilizado na regra de"
	SX6->X6_DSCENG  := STR0176 //"Percentual de desconto utilizado na regra de"
	SX6->X6_DESC1   := STR0177 //"descontos para pagamento antecipado do FINA220"
	SX6->X6_DSCSPA1 := STR0177 //"descontos para pagamento antecipado do FINA220"
	SX6->X6_DSCENG1 := STR0177 //"descontos para pagamento antecipado do FINA220"
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := "0"
	SX6->X6_CONTSPA := "0"
	SX6->X6_CONTENG := "0"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0216 + cPulaLin //"Criado o parametro MV_FINPRDS"
EndIf

SX6->( DbSetorder(1) )
If SX6->( !DbSeek("  "+'MV_FINLOGO')) 
	RecLock("SX6", .T.)
	SX6->X6_FIL     := "  "
	SX6->X6_VAR     := 'MV_FINLOGO'
	SX6->X6_TIPO	:= "C"
	SX6->X6_DESCRIC := STR0178 //"Path com a imagem de fundo apresentada na "
	SX6->X6_DSCSPA  := STR0178 //"Path com a imagem de fundo apresentada na "
	SX6->X6_DSCENG  := STR0178 //"Path com a imagem de fundo apresentada na "
	SX6->X6_DESC1   := STR0179 //"rotina FINA220. (Resolucao deve ser 206 x 163)"
	SX6->X6_DSCSPA1 := STR0179 //"rotina FINA220. (Resolucao deve ser 206 x 163)"
	SX6->X6_DSCENG1 := STR0179 //"rotina FINA220. (Resolucao deve ser 206 x 163)"
	SX6->X6_DESC2   := ""
	SX6->X6_DSCSPA2 := ""
	SX6->X6_DSCENG2 := ""
	SX6->X6_CONTEUD := GetSrvProfString("StartPath","\system\") + "logoFina220.jpg"
	SX6->X6_CONTSPA := GetSrvProfString("StartPath","\system\") + "logoFina220.jpg"
	SX6->X6_CONTENG := GetSrvProfString("StartPath","\system\") + "logoFina220.jpg"
	SX6->X6_PROPRI	:= "S"
	SX6->X6_PYME    := "S"
	SX6->( MsUnlock() )
	
	cTexto += STR0217 + cPulaLin //"Criado o parametro MV_FINLOGO"
EndIf

//������������������������������������������������Ŀ
//�Cria os parametros utilizados em funcionalidades�
//�exclusivas da integracao Protheus x Classis     �
//��������������������������������������������������
If GetNewPar("MV_RMCLASS",.F.)
	SX6->( DbSetorder(1) )
	If SX6->( !DbSeek("  "+'MV_FINGRPO')) 
		RecLock("SX6", .T.)
		SX6->X6_FIL     := "  "
		SX6->X6_VAR     := 'MV_FINGRPO'
		SX6->X6_TIPO	:= "C"
		SX6->X6_DESCRIC := STR0197 //"Codigo do grupo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DSCSPA  := STR0197 //"Codigo do grupo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DSCENG  := STR0197 //"Codigo do grupo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DESC1   := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DSCSPA1 := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DSCENG1 := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DESC2   := STR0199 //"estiver ativa.)"
		SX6->X6_DSCSPA2 := STR0199 //"estiver ativa.)"
		SX6->X6_DSCENG2 := STR0199 //"estiver ativa.)"
		SX6->X6_CONTEUD := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_CONTENG := ""
		SX6->X6_PROPRI	:= "S"
		SX6->X6_PYME    := "N"
		SX6->( MsUnlock() )
		
		//cTexto += STR0218 + cPulaLin //"Criado o parametro MV_FINGRPO"
	EndIf
	
	SX6->( DbSetorder(1) )
	If SX6->( !DbSeek("  "+'MV_FINTPOC')) 
		RecLock("SX6", .T.)
		SX6->X6_FIL     := "  "
		SX6->X6_VAR     := 'MV_FINTPOC'
		SX6->X6_TIPO	:= "C"
		SX6->X6_DESCRIC := STR0200 //"Codigo do tipo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DSCSPA  := STR0200 //"Codigo do tipo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DSCENG  := STR0200 //"Codigo do tipo de ocorrencias utilizado na devolu��o de cheques "
		SX6->X6_DESC1   := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DSCSPA1 := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DSCENG1 := STR0198 //"da rotina LOJA190. (Somente se integra�ao Prothes x Classis "
		SX6->X6_DESC2   := STR0199 //"estiver ativa.)"
		SX6->X6_DSCSPA2 := STR0199 //"estiver ativa.)"
		SX6->X6_DSCENG2 := STR0199 //"estiver ativa.)"
		SX6->X6_CONTEUD := ""
		SX6->X6_CONTSPA := ""
		SX6->X6_CONTENG := ""
		SX6->X6_PROPRI	:= "S"
		SX6->X6_PYME    := "N"
		SX6->( MsUnlock() )
		
		//cTexto += STR0219 + cPulaLin //"Criado o parametro MV_FINTPOC"
	EndIf
	
EndIf
    
Return                        
      
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41SXB   �Autor  �Cesar A. Bianchi    � Data �  05/01/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria as novas consultas padrao da melhoria de Caixa-Tesoura-���
���          �ria.                                                        ���
�������������������������������������������������������������������������͹��
���Uso       � UPDGE41                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41SXB()
Local aSXB  := {}
Local aEstrut := {}
Local aArea   := getArea()
Local I,J              
//������������������������Ŀ
//�Monta a estrutura do SXB�
//��������������������������
aEstrut:= {"XB_ALIAS", "XB_TIPO", "XB_SEQ", "XB_COLUNA", "XB_DESCRI", "XB_DESCSPA", "XB_DESCENG", "XB_CONTEM", "XB_WCONTEM"}

//������������Ŀ
//�CONSULTA FID�
//��������������
aAdd(aSXB,{	"FID","1","01","DB",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0143,;				// DESCRI PORT
			STR0143,;				// DESCRI ESPA
			STR0143,;	   			// DESCRI INGL
			"FID", "" })			// CONTEM, WCONTEM

aAdd(aSXB,{	"FID","2","01","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0056,;				// DESCRI PORT
			STR0056,;				// DESCRI ESPA
			STR0056,;				// DESCRI INGL
			"", "" })				// CONTEM, WCONTEM

aAdd(aSXB,{	"FID","2","02","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0058,;				// DESCRI PORT
			STR0058,;				// DESCRI ESPA
			STR0058,;	   			// DESCRI INGL
			"", "" })				// CONTEM, WCONTEM

aAdd(aSXB,{	"FID","4","01","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0023,;				// DESCRI PORT
			STR0023,;				// DESCRI ESPA
			STR0023,;				// DESCRI INGL
			"FID_USER", "" })		// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FID","4","01","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0024,;				// DESCRI PORT
			STR0024,;				// DESCRI ESPA
			STR0024,;				// DESCRI INGL
			"FID_NOME", "" })		// CONTEM, WCONTEM

aAdd(aSXB,{	"FID","4","02","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0023,;				// DESCRI PORT
			STR0023,;				// DESCRI ESPA
			STR0023,;				// DESCRI INGL
			"FID_USER", "" })		// CONTEM, WCONTEM	
					
aAdd(aSXB,{	"FID","4","02","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0024,;				// DESCRI PORT
			STR0024,;				// DESCRI ESPA
			STR0024,;				// DESCRI INGL
			"FID_NOME", "" })		// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FID","5","01","",;		// ALIAS, TIPO, SEQ, COLUNA
			"",;					// DESCRI PORT
			"",;					// DESCRI ESPA
			"",;					// DESCRI INGL
			"FID->FID_USER", "" })	// CONTEM, WCONTEM

aAdd(aSXB,{	"FID","6","01","",;		// ALIAS, TIPO, SEQ, COLUNA
			"",;					// DESCRI PORT
			"",;					// DESCRI ESPA
			"",;					// DESCRI INGL
			"FID->FID_SUPER == '1'", "" })	// CONTEM, WCONTEM    

//�������������Ŀ
//�CONSULTA FIB2�
//���������������
aAdd(aSXB,{	"FID2","1","01","DB",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0143,;				// DESCRI PORT
			STR0143,;				// DESCRI ESPA
			STR0143,;				// DESCRI INGL
			"FID", "" })			// CONTEM, WCONTEM

aAdd(aSXB,{	"FID2","2","01","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0056,;				// DESCRI PORT
			STR0056,;				// DESCRI ESPA
			STR0056,;				// DESCRI INGL
			"", "" })				// CONTEM, WCONTEM

aAdd(aSXB,{	"FID2","2","02","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0058,;				// DESCRI PORT
			STR0058,;				// DESCRI ESPA
			STR0058,;				// DESCRI INGL
			"", "" })				// CONTEM, WCONTEM

aAdd(aSXB,{	"FID2","4","01","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0023,;				// DESCRI PORT
			STR0023,;				// DESCRI ESPA
			STR0023,;				// DESCRI INGL
			"FID_USER", "" })		// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FID2","4","01","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0024,;				// DESCRI PORT
			STR0024,;				// DESCRI ESPA
			STR0024,;				// DESCRI INGL
			"FID_NOME", "" })		// CONTEM, WCONTEM

aAdd(aSXB,{	"FID2","4","02","01",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0023,;				// DESCRI PORT
			STR0023,;				// DESCRI ESPA
			STR0023,;				// DESCRI INGL
			"FID_USER", "" })		// CONTEM, WCONTEM	
					
aAdd(aSXB,{	"FID2","4","02","02",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0024,;				// DESCRI PORT
			STR0024,;				// DESCRI ESPA
			STR0024,;				// DESCRI INGL
			"FID_NOME", "" })		// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FID2","5","01","",;	// ALIAS, TIPO, SEQ, COLUNA
			"",;					// DESCRI PORT
			"",;					// DESCRI ESPA
			"",;					// DESCRI INGL
			"FID->FID_USER", "" })	// CONTEM, WCONTEM

aAdd(aSXB,{	"FID2","6","01","",;	// ALIAS, TIPO, SEQ, COLUNA
			"",;					// DESCRI PORT
			"",;					// DESCRI ESPA
			"",;					// DESCRI INGL
			".T.", "" })				// CONTEM, WCONTEM

//������������Ŀ
//�CONSULTA FIB�
//��������������
aAdd(aSXB,{	"FIB","1","01","DB",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0144,;					// DESCRI PORT
			STR0144,;					// DESCRI ESPA
			STR0144,;	  				// DESCRI INGL
			"FIB", "" })   				// CONTEM, WCONTEM

aAdd(aSXB,{	"FIB","2","01","01",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0094,;	 				// DESCRI PORT
			STR0094,;	  				// DESCRI ESPA
			STR0094,;		   			// DESCRI INGL
			"", "" })					// CONTEM, WCONTEM

aAdd(aSXB,{	"FIB","2","02","03",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0098,;					// DESCRI PORT
			STR0098,;	  				// DESCRI ESPA
			STR0098,;	  	 			// DESCRI INGL
			"", "" })					// CONTEM, WCONTEM

aAdd(aSXB,{	"FIB","4","01","01",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0062,;	   	  			// DESCRI PORT
			STR0062,;	   	  			// DESCRI ESPA
			STR0062,;	   	  			// DESCRI INGL
			"FIB_LOTE", "" })			// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","4","01","02",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0065,;	   			  	// DESCRI PORT
			STR0065,;	   			  	// DESCRI ESPA
			STR0065,;	   	 		 	// DESCRI INGL
			"FIB_DTABR", "" })			// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","4","01","02",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0069,;	   	 		 	// DESCRI PORT
			STR0069,;	   	 		 	// DESCRI ESPA
			STR0069,;	   	   			// DESCRI INGL
			"FIB_DTFCH", "" })			// CONTEM, WCONTEM

aAdd(aSXB,{	"FIB","4","02","01",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0145,;	   	  			// DESCRI PORT
			STR0145,;	   	  			// DESCRI ESPA
			STR0145,;	   	  			// DESCRI INGL
			"FIB_LOTE", "" })			// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","4","02","02",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0065,;	   			  	// DESCRI PORT
			STR0065,;	   			  	// DESCRI ESPA
			STR0065,;	  		 	  	// DESCRI INGL
			"FIB_DTABR", "" })			// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","4","02","02",;		// ALIAS, TIPO, SEQ, COLUNA
			STR0069,;	   	  			// DESCRI PORT
			STR0069,;	   	  			// DESCRI ESPA
			STR0069,;	   	  			// DESCRI INGL
			"FIB_DTFCH", "" })			// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","5","01","",;			// ALIAS, TIPO, SEQ, COLUNA
			"",;	   				  	// DESCRI PORT
			"",;	   				  	// DESCRI ESPA
			"",;	   				  	// DESCRI INGL
			"FIB->FIB_LOTE", "" })		// CONTEM, WCONTEM
			
aAdd(aSXB,{	"FIB","6","01","",;			// ALIAS, TIPO, SEQ, COLUNA
			"",;	   	  				// DESCRI PORT
			"",;	   	  				// DESCRI ESPA
			"",;	   	  				// DESCRI INGL
			"FIB->FIB_USER == MV_PAR01", "" })		// CONTEM, WCONTEM
			
//���������������Ŀ
//�CONSULTA SA6_CX�
//�����������������
aAdd(aSXB,{	"SA6_CX","1","01","DB",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0146,;	   	 	   				// DESCRI PORT
			STR0146,;	   	 	 				// DESCRI ESPA
			STR0146,;	   	 	   				// DESCRI INGL
			"SA6", "" })   						// CONTEM, WCONTEM

aAdd(aSXB,{	"SA6_CX","2","01","01",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0147,;	   	 	 				// DESCRI PORT
			STR0147,;	   	 	 				// DESCRI ESPA
			STR0147,;	   	 	 				// DESCRI INGL
			"", "" })   						// CONTEM, WCONTEM

aAdd(aSXB,{	"SA6_CX","2","02","02",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0148,;	   	 	   				// DESCRI PORT
			STR0148,;	   	 					// DESCRI ESPA
			STR0148,;	   	 					// DESCRI INGL
			"", "" })   						// CONTEM, WCONTEM
			
aAdd(aSXB,{	"SA6_CX","4","01","01",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 			 		// DESCRI INGL
			"A6_COD", "" })   					// CONTEM, WCONTEM			

aAdd(aSXB,{	"SA6_CX","4","01","02",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0148,;	   	 	   				// DESCRI PORT
			STR0148,;	   	 		 			// DESCRI ESPA
			STR0148,;	   	 					// DESCRI INGL
			"A6_NOME", "" })   

aAdd(aSXB,{	"SA6_CX","4","02","01",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 			 		// DESCRI INGL
			"A6_COD", "" })   					// CONTEM, WCONTEM			

aAdd(aSXB,{	"SA6_CX","4","02","02",;	 		// ALIAS, TIPO, SEQ, COLUNA
			STR0148,;	   	 	   	   			// DESCRI PORT
			STR0148,;	   	 	 	   			// DESCRI ESPA
			STR0148,;	   	 		   			// DESCRI INGL
			"A6_NOME", "" })   
			                                	
aAdd(aSXB,{	"SA6_CX","5","01","",;	 	 		// ALIAS, TIPO, SEQ, COLUNA
			"",;	   	 	   		   			// DESCRI PORT
			"",;	   	 			   			// DESCRI ESPA
			"",;	   	 			   			// DESCRI INGL
			"SA6->A6_COD", "" })   				// CONTEM, WCONTEM			

aAdd(aSXB,{	"SA6_CX","6","01","",;	 	  		// ALIAS, TIPO, SEQ, COLUNA
			"",;	   	 	   	   				// DESCRI PORT
			"",;	   	 		  		   		// DESCRI ESPA
			"",;	   	 			 			// DESCRI INGL
			"Fn222ValCx(SA6->A6_COD)", "" })   			

//���������������Ŀ
//�Consulta SE4001�
//�����������������
aAdd(aSXB,{	"SE4001","1","01","DB",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0150,;	   	 	 		  	    // DESCRI PORT
			STR0150,;	   	   			     	// DESCRI ESPA
			STR0150,;	   	   				    // DESCRI INGL
			"SE4", "" })   						// CONTEM, WCONTEM			

aAdd(aSXB,{	"SE4001","2","01","01",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 					// DESCRI INGL
			"", "" })   						// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4001","4","01","01",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 					// DESCRI INGL
			"E4_CODIGO", "" })  				// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4001","4","01","02",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0151,;	   	 	  	 			// DESCRI PORT
			STR0151,;	   	 	   				// DESCRI ESPA
			STR0151,;	   	 	 				// DESCRI INGL
			"E4_COND", "" })   					// CONTEM, WCONTEM	
			
aAdd(aSXB,{	"SE4001","4","01","03",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0152,;	   	 	   	 			// DESCRI PORT
			STR0152,;	   	 	   				// DESCRI ESPA
			STR0152,;	   	 		 			// DESCRI INGL
			"E4_DESCRI", "" })   				// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4001","5","01","",;	 	 		// ALIAS, TIPO, SEQ, COLUNA
			"",;	   	 	   					// DESCRI PORT
			"",;	   	 						// DESCRI ESPA
			"",;	   	 			  			// DESCRI INGL
			"Fn220DesCo(SE4->E4_CODIGO)", "" }) // CONTEM, WCONTEM	

//���������������Ŀ
//�Consulta SE4002�
//�����������������
aAdd(aSXB,{	"SE4002","1","01","DB",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0150,;	   	 	 		  	    // DESCRI PORT
			STR0150,;	   	   			     	// DESCRI ESPA
			STR0150,;	   	   				    // DESCRI INGL
			"SE4", "" })   						// CONTEM, WCONTEM			

aAdd(aSXB,{	"SE4002","2","01","01",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 					// DESCRI INGL
			"", "" })   						// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4002","4","01","01",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0149,;	   	 	   				// DESCRI PORT
			STR0149,;	   	 					// DESCRI ESPA
			STR0149,;	   	 					// DESCRI INGL
			"E4_CODIGO", "" })  				// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4002","4","01","02",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0151,;	   	 	  	 			// DESCRI PORT
			STR0151,;	   	 	   				// DESCRI ESPA
			STR0151,;	   	 	 				// DESCRI INGL
			"E4_COND", "" })   					// CONTEM, WCONTEM	
			
aAdd(aSXB,{	"SE4002","4","01","03",;	 	 	// ALIAS, TIPO, SEQ, COLUNA
			STR0152,;	   	 	   	 			// DESCRI PORT
			STR0152,;	   	 	   				// DESCRI ESPA
			STR0152,;	   	 		 			// DESCRI INGL
			"E4_DESCRI", "" })   				// CONTEM, WCONTEM	

aAdd(aSXB,{	"SE4002","5","01","",;	 	 		// ALIAS, TIPO, SEQ, COLUNA
			"",;	   	 	   					// DESCRI PORT
			"",;	   	 						// DESCRI ESPA
			"",;	   	 			  			// DESCRI INGL
			"SE4->E4_CODIGO", "" }) 			// CONTEM, WCONTEM	

//�������������������������������Ŀ
//�Grava as novas consultas no SXB�
//���������������������������������
dbSelectArea("SXB")
SXB->(dbSetOrder(1)) //XB_ALIAS+XB_TIPO+XB_SEQ+XB_COLUNA	
For i := 1 To Len(aSXB)
	If !DbSeek(PadR(aSXB[i,1],6)+aSXB[i,2]+aSXB[i,3]+aSXB[i,4])
		RecLock("SXB",.T.)
	Else
		RecLock("SXB",.F.)
	EndIf
	lSxB := .T.
	For j:=1 To Len(aSXB[i])
		If FieldPos(aEstrut[j]) > 0
			FieldPut(FieldPos(aEstrut[j]),aSXB[i,j])
		EndIf
	Next j
	MsUnLock()
Next i        

cTexto += STR0153 + cPulaLin //"Atualizado as consultas padrao FID/FIB "

RestArea(aArea)
Return
      
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41SX1   �Autor  �Cesar A. Bianchi    � Data �  30/12/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria as perguntas no cad. Perguntas SX1                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � MP811/912 - Integracao Protheus x Classis .NET             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41SX1()
Local aEstrut := {}
Local aGrava  := {}
Local aArea	  := getArea()
Local nI,nJ

//����������������������������������������������������Ŀ
//�Grava as perguntas do relatorio Mapa Resumo de Caixa�
//������������������������������������������������������
dbSelectArea('SX1')
SX1->(dbSetOrder(1))
if SX1->(!dbSeek('FIR220'))
	//::Monta a Estrutura do SX1
	aEstrut := {"X1_GRUPO"	,"X1_ORDEM"		,"X1_PERGUNT"	,"X1_PERSPA"	,"X1_PERENG"	,"X1_VARIAVL"	,;
				 "X1_TIPO"	,"X1_TAMANHO"	,"X1_DECIMAL"	,"X1_PRESEL"	,"X1_GSC"		,"X1_VALID"		,;
				 "X1_VAR01"	,"X1_DEF01"		,"X1_DEFSPA1"	,"X1_DEFENG1"	,"X1_CNT01"		,"X1_F3"		,;
				 "X1_PYME"	,"X1_GRPSXG"	,"X1_HELP"		,"X1_PICTURE"									}
	//::Adiciona os dados a gravar
	aADD(aGrava,{"FIR220"	,"01"			,STR0154		,STR0154		,STR0154		,"mv_ch1"		,;
				 "C"		,6				,0				,0				,"G"			,""				,;
				 "MV_PAR01"	,""				,""				,""				,""				,"FID2"			,;
				 ""			,""				,""				,""												})
	aADD(aGrava,{"FIR220"	,"02"			,STR0155		,STR0155		,STR0155		,"mv_ch2"		,;
				 "C"		,10				,0				,0				,"G"			,""				,;
				 "MV_PAR02"	,""				,""				,""				,""				,"FIB"			,;
				 ""			,""				,""				,""												})
   
    //::Grava no SX1
	For nI:= 1 to len(aGrava)
		RecLock("SX1",.T.)
		For nJ:=1 To Len(aGrava[nI])
			If FieldPos(aEstrut[nJ])>0 .And. aGrava[nI,nJ] != NIL
				FieldPut(FieldPos(aEstrut[nJ]),aGrava[nI,nJ])
			EndIf
		Next nJ
		SX1->(msUnLock())
	Next nI
endif

//�����������������������Ŀ
//�Monta o Pergunte FIN222�
//�������������������������
dbSelectArea('SX1')
SX1->(dbSetOrder(1))
if SX1->(!dbSeek('FIN222'))
	RecLock('SX1',.T.)
	SX1->X1_GRUPO := "FIN222"
	SX1->X1_ORDEM := "01"
	SX1->X1_PERGUNT := STR0156 //"Imprimir via:"
	SX1->X1_VARIAVL := "mv_ch1"
	SX1->X1_TIPO := "N"
	SX1->X1_TAMANHO := 1
	SX1->X1_DECIMAL := 0
	SX1->X1_PRESEL := 2
	SX1->X1_GSC := "C"
	SX1->X1_VAR01 := "mv_par01"
	SX1->X1_DEF01 := STR0157 //"Equip. ECF"
	SX1->X1_CNT01 := '1'
	SX1->X1_VAR02 := ""
	SX1->X1_DEF02 := STR0158 //"Equip. Comum"
	SX1->X1_CNT02 := '2'
	SX1->(msUnlock())	
endif

RestArea(aArea)
Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE41VarObr�Autor  �Cesar A. Bianchi    � Data �  09/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Carrega as variaveis de campo obrigatorio, reservado, 	  ���
���          �nao-obrigatorio e nao-reservado    	                      ���
�������������������������������������������������������������������������͹��
���Uso       � UPDGE41                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE41VarObr()           

SX3->(dbSetOrder(2))
If SX3->( dbSeek("JA2_NUMRA ") ) //Este campo eh chave
	cUsadoKey	:= SX3->X3_USADO
	cReservKey	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("JA2_NOME  ") ) //Este campo eh obrigatorio e permite alterar
	cUsadoObr	:= SX3->X3_USADO
	cReservObr	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("JA2_CODFAM") ) //Este campo eh opcional e permite alterar
	cUsadoOpc	:= SX3->X3_USADO
	cReservOpc	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("JA2_FILIAL") ) //Este campo nao eh usado
	cUsadoNao	:= SX3->X3_USADO
	cReservNao	:= SX3->X3_RESERV
EndIf

Return Nil   

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GetOrdem  �Autor  �Cristina Souza      � Data � 26/Set/2006 ���
�������������������������������������������������������������������������͹��
���Desc.     �Busca a ordem que deve ser usada para atualizacao de campos ���
���          �no SX3.                                                     ���
�������������������������������������������������������������������������͹��
���Uso       �AtuSX3                                                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GetOrdem(cAlias, cCampo)
Local cRet

SX3->(dbSetOrder(2))
if SX3->(dbSeek(cCampo))
	cRet := SX3->X3_ORDEM
else
	SX3->(dbSetOrder(1))
	SX3->(dbSeek(cAlias))
	while SX3->(!Eof() .and. X3_ARQUIVO == cAlias)
		cRet := SX3->X3_ORDEM
		SX3->(dbSkip())
	end
	cRet := Soma1(cRet)
endif

Return cRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �IntNewTbl �Autor  �Cesar A. Bianchi    � Data �  13/07/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria as novas tabelas da integra��o Protheus x Classis que  ���
���          �so sao utilizada para as rotinas de Caixa.                  ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Sistema de Caixa                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function IntNewTbl()
Local lMsSql := "MSSQL" $ Upper(TCGetDB())
Local cQuery := ""

//������������������������������Ŀ
//�Cria a tabela INT_IDOCORRENCIA�
//��������������������������������
if !TcCanOpen('INT_IDOCORRENCIA')
	If lMsSql
		cQuery := "	CREATE TABLE [dbo].[INT_IDOCORRENCIA]( "
		cQuery += "		[OCO_COLIGADA] [float] NOT NULL, "
		cQuery += "		[OCO_FILIAL] [varchar](2) NOT NULL, "
		cQuery += "		[OCO_IDOCORPROTHEUS] [varchar](12) NOT NULL, "
		cQuery += "		[OCO_SEQ] [int] NOT NULL, "
		cQuery += "		[OCO_NUMRA] [varchar](20) NOT NULL, "
		cQuery += "		[OCO_OBSERVACOES] [varchar](255) NULL, "
		cQuery += "		[OCO_IDOCORCORPORE] [int] NOT NULL "
		cQuery += ") ON [PRIMARY] "
	Else
		cQuery := "create table INT_IDOCORRENCIA "
		cQuery += " ("
		cQuery += "		OCO_COLIGADA NUMBER not null, "
		cQuery += "		OCO_FILIAL   VARCHAR2(2) not null, "
		cQuery += "		OCO_IDOCORPROTHEUS VARCHAR2(12)  not null,"
		cQuery += "		OCO_SEQ NUMBER not null, "
		cQuery += "		OCO_NUMRA VARCHAR2(20) not null, "
		cQuery += "		OCO_OBSERVACOES VARCHAR2(255) null, "
		cQuery += "		OCO_IDOCORCORPORE NUMBER not null "
		cQuery += ")"
	endif
	
	//Executa a query	
	if TcSqlExec(cQuery) < 0
		conout(TcSqlError())
		cTexto += STR0201 + cPulaLin //" Falha ao tentar criar a tabela intermediaria INT_IDOCORRENCIA " + cPulaLin
		cTexto += TcSqlError() + cPulaLin
	else
		cTexto += STR0202  + cPulaLin //"Criada a tabela INT_IDOCORRENCIA "
	endif
endif

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()
Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 To 20  
	dbUseArea( .T.,, "sigamat.emp", "SM0", .F., .F. ) 	
	If !Empty( Select( "SM0" ) ) 
		lOpen := .T. 
		dbSetIndex("sigamat.ind") 
		Exit	
	EndIf
	Sleep( 500 ) 
Next nLoop

If !lOpen
	Aviso(STR0160,STR0159, {STR0161}, 2 ) 		//"Atencao !" //"Nao foi possivel a abertura da tabela de empresas de forma exclusiva !"//"Ok"
	cTexto += STR0162 + cPulaLin 				//"Nao foi possivel a abertura da tabela de empresas de forma exclusiva !"
EndIf

Return(lOpen)

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � U_UNPF030� Autor � Cesar A. Bianchi      � Data � 07/10/08 ���
�������������������������������������������������������������������������Ĵ��
���Objetivos �Desfaz as tabelas, campos e grupos de pergunte criados a par���
���          �tir do Update U_UPDF030									  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � U_UNPF030()                                                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAFIN e Integracao Protheus x Classis Net                ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
User Function UNPF030()
Local cMsg := ""         

Private oMainWnd         
Private aArqUpd	    := {}
Private aREOPEN	    := {}
Private cPulaLin    := CHR(13)+CHR(10)
Private cTexto      := ''
Private cMessage    := ''

Set Dele On        

//��������������������������������������������������������������������Ŀ
//�Monta o texto de orientacao ao usuario exibido na interface grafica.�
//����������������������������������������������������������������������
cMsg := STR0163 			//"Este programa tem como objetivo ajustar os dicion�rios e base de dados, " 
cMsg += STR0164 			//"para REMOVER a compatibilizacao com a melhoria de Caixa-Tesouraria. "
cMsg += STR0165 + cPulaLin //"Esta rotina deve ser processada em modo exclusivo! " 
cMsg += STR0166 + cPulaLin //"Fa�a um backup dos dicion�rios e base de dados antes do processamento! "

//��������������������������������������Ŀ
//�Cria a interface grafica com o usuario�
//����������������������������������������
oMainWnd 		   := MSDIALOG():Create()
oMainWnd:cName     := "oMainWnd"
oMainWnd:cCaption  := STR0005 //"Atualizacao do Dicionario de Dados"
oMainWnd:nLeft     := 0
oMainWnd:nTop      := 0
oMainWnd:nWidth    := 540
oMainWnd:nHeight   := 250
oMainWnd:lShowHint := .F.
oMainWnd:lCentered := .T.
oMainWnd:bInit     := {|| if( Aviso(STR0006, cMsg, {STR0007, STR0008}, 2 ) == 2 , ( Processa({|lEnd| Ge40Prep(@lEnd)} , STR0006 ) , oMainWnd:End() ), ( MsgAlert(STR0009), oMainWnd:End() ) ) }
oMainWnd:Activate()

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GE40PREP  �Autor  �Cesar A. Bianchi    � Data �  07/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     �Prepara o ambiente para gravar os novos campos e tabelas no ���
���          �dicionario de dados carregando as empresas existentes dentro���
���          �do SIGAMAT.EMP                                              ���
�������������������������������������������������������������������������͹��
���Uso       � Gestao Educacional - Atividades Complementares e Estagios  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function GE40PREP()
Local cMask       := STR0010 						  	//Mascara do arquivo de log a ser salvo
Local nRecno      := 0                                 	//Nro empresa corrente
Local lOpen       := MyOpenSm0Ex() 				       	//Retorna se conseguiu acesso exclusivo ao sigamat.emp
Local nI      	  := 0                                 	//Contador para laco For/Next
Private aRecnoSM0 := {}                                	//Array contendo as empresas presentes no sigamat.emp


If lOpen 
	dbSelectArea("SM0")
	SM0->(dbGotop())
	
	//������������������������������������������������������������������Ŀ
	//�Adiciona o array aRecnoSMO todas as empresas existentes no sigamat�
	//��������������������������������������������������������������������	
	While SM0->(!Eof())
  		If Ascan(aRecnoSM0,{ |x| x[2] == SM0->M0_CODIGO}) == 0 
			aAdd(aRecnoSM0,{SM0->(Recno()),SM0->M0_CODIGO})
		EndIf			
		SM0->(dbSkip())
	EndDo	
		                                                
	//��������������������������������������������������������������������������Ŀ
	//�Executa a atualizacao dos SX's de cada empresa presente no array aRecnoSM0�
	//����������������������������������������������������������������������������	
	For nI := 1 to Len(aRecnoSM0)
		
		//������������������������������������Ŀ
		//�Define a empresa a manipular os SX's�
		//��������������������������������������
		SM0->(dbGoto(aRecnoSM0[nI,1]))
		RpcSetType(2) 
		RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
		lMsFinalAuto := .F.
	    
		//��������������������������������������������������Ŀ
		//�Registra no log a atualizacao da empresa corrente.�
		//����������������������������������������������������		
		cTexto += Replicate("-",128) + cPulaLin
		cTexto += STR0011 + SM0->M0_CODIGO+SM0->M0_NOME + cPulaLin
	    
		//������������������������������������������Ŀ
		//�Define o tamanho da regua de processamento�
		//��������������������������������������������		
		ProcRegua(nI)
		IncProc(STR0011+SM0->M0_CODIGO+SM0->M0_NOME)			
			
		//�������������������������������������������������������Ŀ
		//�Incrementa a regua de processamento e alerta no console�
		//���������������������������������������������������������		
		IncProc( dtoc( Date() )+" "+Time()+" "+STR0012)
		Conout( dtoc( Date() )+" "+Time()+" "+STR0012)			
		cTexto += STR0013 + cPulaLin 
		
		DelBD()
			
		//�������������������������������������������������������Ŀ
		//�Incrementa a regua de processamento e alerta no console�
		//���������������������������������������������������������						
		IncProc( dtoc( Date() )+" "+Time()+" "+STR0014)						
		Conout( dtoc( Date() )+" "+Time()+" "+STR0014)
		cTexto += STR0014 + cPulaLin
	Next nI
endif

//���������������������������������������Ŀ
//�Exibe alerta do final do processamento.�
//�����������������������������������������
cTexto :=STR0015 + cPulaLin + cTexto
__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
DEFINE FONT oFont NAME "Mono AS" SIZE 5,12   //6,15
DEFINE MSDIALOG oDlg TITLE STR0016 From 3,0 to 340,417 PIXEL
@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
oMemo:bRClicked := {||AllwaysTrue()}
oMemo:oFont:=oFont
DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
ACTIVATE MSDIALOG oDlg CENTER   

Return         

//Defaz updge40
Static Function DelBD()
Local cQuery := ""

//���������������������Ŀ
//�Deleta a FIB dos SX's�
//�����������������������
dbSelectArea("SX2") 
SX2->(dbSetOrder(1))
If SX2->(dbSeek('FIB'))
	RecLock('SX2',.F.)
	SX2->(dbDelete())
	SX2->(msUnlock())
endif
dbSelectArea("SX3")
SX3->(dbSetOrder(1))
if SX3->(dbSeek('FIB'))
	While alltrim(SX3->X3_ARQUIVO) == "FIB"
		RecLock('SX3',.F.)
		SX3->(dbDelete())
		SX3->(msUnlock())
		SX3->(dbSkip())		
	EndDo
endif
dbSelectArea("SIX")
if SIX->(dbSeek('FIB'))
	While alltrim(SIX->INDICE)	== 'FIB' 
		RecLock('SIX',.F.)
		SIX->(dbDelete())	
		SIX->(msUnlock())
		SIX->(dbSkip())
	EndDo
endif

//���������������������Ŀ
//�Deleta a FIC dos SX's�
//�����������������������
dbSelectArea("SX2") 
SX2->(dbSetOrder(1))
If SX2->(dbSeek('FIC'))
	RecLock('SX2',.F.)
	SX2->(dbDelete())
	SX2->(msUnlock())
endif
dbSelectArea("SX3")
SX3->(dbSetOrder(1))
if SX3->(dbSeek('FIC'))
	While alltrim(SX3->X3_ARQUIVO) == "FIC"
		RecLock('SX3',.F.)
		SX3->(dbDelete())
		SX3->(msUnlock())
		SX3->(dbSkip())		
	EndDo
endif
dbSelectArea("SIX")
if SIX->(dbSeek('FIC'))
	While alltrim(SIX->INDICE)	== 'FIC' 
		RecLock('SIX',.F.)
		SIX->(dbDelete())	
		SIX->(msUnlock())
		SIX->(dbSkip())
	EndDo
endif

//���������������������Ŀ
//�Deleta a FID dos SX's�
//�����������������������
dbSelectArea("SX2") 
SX2->(dbSetOrder(1))
If SX2->(dbSeek('FID'))
	RecLock('SX2',.F.)
	SX2->(dbDelete())
	SX2->(msUnlock())
endif
dbSelectArea("SX3")
SX3->(dbSetOrder(1))
if SX3->(dbSeek('FID'))
	While alltrim(SX3->X3_ARQUIVO) == "FID"
		RecLock('SX3',.F.)
		SX3->(dbDelete())
		SX3->(msUnlock())
		SX3->(dbSkip())		
	EndDo
endif
dbSelectArea("SIX")
if SIX->(dbSeek('FID'))
	While alltrim(SIX->INDICE)	== 'FID' 
		RecLock('SIX',.F.)
		SIX->(dbDelete())	
		SIX->(msUnlock())
		SIX->(dbSkip())
	EndDo
endif

//���������������Ŀ
//�Deleta E1_LTCXA�
//�����������������
dbSelectArea("SX3")
SX3->(dbSetOrder(2))
if SX3->(dbSeek('E1_LTCXA'))
	RecLock('SX3',.F.)
	SX3->(dbDelete())
	SX3->(msUnlock())
endif
     
//���������������Ŀ
//�Deleta E1_NOPER�
//�����������������
dbSelectArea("SX3")
SX3->(dbSetOrder(2))
if SX3->(dbSeek('E1_NOPER'))
	RecLock('SX3',.F.)
	SX3->(dbDelete())
	SX3->(msUnlock())
endif

//���������������Ŀ
//�Deleta EF_LTCXA�
//�����������������
dbSelectArea("SX3")
SX3->(dbSetOrder(2))
if SX3->(dbSeek('EF_LTCXA'))
	RecLock('SX3',.F.)
	SX3->(dbDelete())
	SX3->(msUnlock())
endif
     
//���������������Ŀ
//�Deleta EF_NOPER�
//�����������������
dbSelectArea("SX3")
SX3->(dbSetOrder(2))
if SX3->(dbSeek('EF_NOPER'))
	RecLock('SX3',.F.)
	SX3->(dbDelete())
	SX3->(msUnlock())
endif

//����������������Ŀ
//�Deleta EF_IDOCPR�
//������������������
dbSelectArea("SX3")
SX3->(dbSetOrder(2))
if SX3->(dbSeek('EF_IDOCPR'))
	RecLock('SX3',.F.)
	SX3->(dbDelete())
	SX3->(msUnlock())
endif

dbSelectArea('SX6')
SX6->( DbSetorder(1) )
If SX6->(DbSeek("  "+"MV_FINFPGT")) 
	RecLock("SX6", .F.)
    SX6->(dbDelete())
    SX6->(msUnlock())
endif

dbSelectArea('SX6')
SX6->( DbSetorder(1) )
If SX6->(DbSeek("  "+"MV_CHEPRE")) 
	RecLock("SX6", .F.)
    SX6->(dbDelete())
    SX6->(msUnlock())
endif

dbSelectArea('SX1')
SX1->(dbSetOrder(1))
if SX1->(dbSeek('FIR220'))
	While alltrim(SX1->X1_GRUPO) == "FIR220"
		RecLock('SX1',.F.)
		SX1->(dbDelete())
		SX1->(msUnlock())
		SX1->(dbSkip())
	EndDo
endif

dbSelectArea('SX1')
SX1->(dbSetOrder(1))
if SX1->(dbSeek('FIN222'))
	While alltrim(SX1->X1_GRUPO) == "FIN222"
		RecLock('SX1',.F.)
		SX1->(dbDelete())
		SX1->(msUnlock())
		SX1->(dbSkip())
	EndDo
endif

//�����������������������Ŀ
//�Deleta tabelas do banco�
//�������������������������
cquery := "DROP TABLE " + RetSqlname("FIB") 
TCSQLExec(cQuery)
TCSQLExec("commit") 

cquery := "DROP TABLE " + RetSqlname("FIC") 
TCSQLExec(cQuery)
TCSQLExec("commit")                      

cquery := "DROP TABLE " + RetSqlname("FID") 
TCSQLExec(cQuery)
TCSQLExec("commit") 

cquery := "ALTER TABLE " + RetSqlname("SE1") + " drop column E1_LTCXA "       
TCSQLExec(cQuery)
TCSQLExec("commit")          
                
Aviso("Fim","Fim")
Return