#include "tbiconn.ch" 
#Include "Protheus.ch"
#Include "FILEIO.ch"
#Include "UPDFIN040.ch"


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���			                                                              ���
���    Update de Criacao de tabelas, campos, dicionarios de dados         ���
���    ROTINA: NF-e Educacional com Integracao Protheus x RM Classis Net  ���
���			                                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 � UpdNFeEduc � Autor � Alberto Deviciente  � Data �30/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Update para Criacao de tabelas, campos, dicionarios de     ���
���          � dados referente a rotina de NF-e Educacional com Integracao���
���          � Protheus x RM Classis Net.                                 ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � UpdNFeEduc()                                  			  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� 															  ���
���          � 															  ���
���          � 															  ���
���          �        													  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Rotina NF-e Educacional                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
User Function UpdF040()
Local cMsg := ""         

cArqEmp := "SigaMAT.Emp"
__cInterNet := Nil

Private cMessage
Private aREOPEN	 := {}
Private oMainWnd          
Private aEmpsFils:= {}

Set Dele On

cMsg += STR0001 //"Este programa tem como objetivo criar Tabelas, Campos, "
cMsg += STR0002 //"atualizar Dicion�rios relacionados a rotina de NF-e Educacional. "
cMsg += STR0003 //"Esta rotina deve ser processada em modo exclusivo! "
cMsg += STR0004 //"Fa�a um backup dos dicion�rios e base de dados antes do processamento!" 

oMainWnd := MSDIALOG():Create()
oMainWnd:cName := "oMainWnd"
oMainWnd:cCaption := STR0005 //"NF-e Educacional"
oMainWnd:nLeft := 0
oMainWnd:nTop := 0
oMainWnd:nWidth := 540
oMainWnd:nHeight := 250
oMainWnd:lShowHint := .F.
oMainWnd:lCentered := .T.
oMainWnd:bInit := {|| if( Aviso( STR0006, cMsg, {STR0007, STR0008}, 2 ) == 2 , (UpdNFeProc(), oMainWnd:End()), ( MsgAlert( STR0009 ), oMainWnd:End() ) ) }

//Controle das mensagens Aviso, MsgStop, MsgInfo e MsgAlert
If Type("lMsHelpAuto") <> Nil
	lMsHelpAuto := .F.
EndIf

oMainWnd:Activate()
                                
Return(.T.)      


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UpdNFeProc�Autor  �Alberto Deviciente  � Data � 30/Out/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Executa o Wizard para atualizacao da Base em todas as      ���
���          �empresas.                                         		  ���
�������������������������������������������������������������������������͹��
���Uso       � Rotina NF-e Educacional                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/ 
Static Function UpdNFeProc()    
Local lOpen     	:= .F.	//Retorna se conseguiu acesso exclusivo a base de dados    
Local nInd 			:= 0
Local lExecutou := 	.F.

Private aRecnoSM0 	:= {}
Private nI 			:= 0

If ( lOpen := MyOpenSm0Ex() )
	
	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0 //--So adiciona no aRecnoSM0 se a empresa for diferente
			aAdd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf
		aAdd(aEmpsFils,{.F.,SM0->M0_CODIGO,AllTrim(SM0->M0_NOME),SM0->M0_CODFIL,alltrim(SM0->M0_FILIAL),Recno()})
		dbSkip()
	EndDo
	
	ASort(aRecnoSM0,,,{|x,y| x[2] < y[2] })
	
	If lOpen
		SM0->(dbGoto(aRecnoSM0[1,1]))
		RpcSetType(2) 
		RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
		lMsFinalAuto := .F.
		
		//Verifica se existe tabela da Integracao para identificar a existencia da integracao instalada
		if !TCCanOpen("INT_ALUNO") //Verifica se a tabela INT_ALUNO existe na base.
			MsgStop(STR0010 + CHR(10)+CHR(10) + STR0011) //"A Integra��o entre os sistemas Protheus x TOTVS Educacional n�o est� instalada, pois a tabela INT_ALUNO n�o foi encontrada na base de dados do Protheus."
		else
			//Monta a tela do Wizard
			lExecutou := UpdNFeWiz()
		endif
		
	    RpcClearEnv()
	endif 
	
endif  

if lExecutou
	MsgAlert(STR0012) //"Processamento Conclu�do."
else
	MsgAlert(STR0013) //"Processamento Cancelado."
endif

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UpdNFeWiz �Autor  � Alberto Deviciente � Data � 30/Out/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Monta a Tela do Wizard                                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Rotina NF-e Educacional                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/ 
Static Function UpdNFeWiz()
Local cTxtPnl 	:= STR0014 //"O assistente ir� auxili�-lo no processo de instala��o da rotina NF-e Educacional"
Local cProcess	:= "CANCEL" //Processamento Cancelado
Local lRet 		:= .T.

Private oWizard
Private nReguaProc	 	:= 0
Private oReguaTot
Private oReguaProc
Private nReguaTot	 := 0
Private oSay1
Private oSay2
Private oMemo
Private oMMProcess
Private oMMProcTot
Private oFont 		:= TFont():New( "Arial Black"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .F./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private oFont1 		:= TFont():New( "Tahoma"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .T./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private oFontRed	:= TFont():New( "Tahoma"/*<cName>*/, /*<nWidth>*/, -10/*<nHeight>*/, /*<.from.>*/, .T./*[<.bold.>]*/, /*<nEscapement>*/, , /*<nWeight>*/, /*[<.italic.>]*/, /*[<.underline.>]*/,,,,,, /*[<oDevice>]*/ )
Private cCxText 	:= ""
Private cCxTextAnt	:= ""
Private cMMProcess 	:= ""
Private cMMProcTot 	:= ""
Private oNoMarked  	:= LoadBitmap( GetResources(),'LBNO')
Private oMarked	  	:= LoadBitmap( GetResources(),'LBOK')


//���������������Ŀ
//�Primeiro painel�
//�����������������
oWizard := APWizard():New( STR0015/*<chTitle>*/, ""/*<chMsg>*/, STR0016/*<cTitle>*/, cTxtPnl/*<cText>*/, {||  .T.}/*<bNext>*/, {|| .T.}/*<bFinish>*/, /*<.lPanel.>*/,,,/*<.lNoFirst.>*/ )

//��������������Ŀ
//�Segundo Painel�
//����������������
oWizard:NewPanel(STR0018/*<chTitle>*/,STR0136/*<chMsg>*/, { || .F.}/*<bBack>*/,{||  .T.}/*<bNext>*/,{|| .T.} /*<bFinish>*/, /*<.lPanel.>*/, {|| .T.}/*<bExecute>*/ )

	//���������������������������������������������������������Ŀ
	//�Monta o Say "Selecione as empresas a serem consideradas:"�
	//�����������������������������������������������������������
	TSay():New( 02/*<nRow>*/, 007/*<nCol>*/, {|| "Selecione as empresas a serem consideradas:"}	/*<{cText}>*/, oWizard:oMPanel[2]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, /*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ ) // "Selecione as Empresas/Filiais a serem consideradas:"
	
	//����������������������������Ŀ
	//�ListBox - "Empresas/Filiais"�
	//������������������������������
    oListSM0 := TWBrowse():New( 15, 7, oWizard:oMPanel[2]:nClientHeight, oWizard:oMPanel[2]:nClientWidth-480, {|| }, {"","C�d. Emp.","Nome Empresa","Cod. Fil.","Nome Filial"}, {}, oWizard:oMPanel[2],,,,,,,,,,,,,,.T.,,,,,)
    oListSM0:bHeaderClick := {|x,y| if(y==1, Upd40MkAll(),nil) }
	Asort(aEmpsFils,,, { |x,y| x[2]+x[4] < y[2]+y[4] } )   
	oListSM0:SetArray( aEmpsFils )
	oListSM0:bLine := {||	{ if(aEmpsFils[oListSM0:nAt,1], oMarked, oNoMarked),;	//Flag de marcacao
								 aEmpsFils[oListSM0:nAt,2],;						//Codigo da Empresa
								 aEmpsFils[oListSM0:nAt,3],;						//Nome da Empresa
								 aEmpsFils[oListSM0:nAt,4],;						//Codigo da Filial
								 aEmpsFils[oListSM0:nAt,5]} }						//Nome da Filial							 
    oListSM0:blDblClick := { || Upd40Mark() }


//����������������Ŀ
//�Terceiro painel �
//������������������
oWizard:NewPanel(STR0017 /*<chTitle>*/, STR0018/*<chMsg>*/, { || .F.}/*<bBack>*/, /*<bNext>*/, /*<bFinish>*/, /*<.lPanel.>*/, {|| cProcess:=UpdNFeExec(), UpdNFeCtrl(cProcess)}/*<bExecute>*/ ) //"Passo 01 - Execu��o da Instala��o."
				
	@ 05,05 GET oMemo  VAR cCxText MEMO SIZE 290,80 OF oWizard:oMPanel[3] PIXEL
	oMemo:bRClicked := {||AllwaysTrue()}
	oMemo:LREADONLY := .T.
//	oMemo:oFont:=oFont

	@ 89,65 GET oMMProcess  VAR cMMProcess MEMO SIZE 230,10 OF oWizard:oMPanel[3] PIXEL
	oMMProcess:LREADONLY := .T.
	
	@ 116,65 GET oMMProcTot  VAR cMMProcTot MEMO SIZE 230,10 OF oWizard:oMPanel[3] PIXEL
	oMMProcess:LREADONLY := .T.

//������������������������������������Ŀ
//�Exibe a Interface tipo wizard (show)�
//��������������������������������������
oWizard:Activate( .T./*<.lCenter.>*/,/*<bValid>*/, /*<bInit>*/, /*<bWhen>*/ )

if cProcess == "CANCEL" //Processamento Cancelado
	lRet := .F.
endif

Return lRet


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UpdNFeExec�Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Executa o processamento de atualizacao da base.            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Rotina NF-e Educacional                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UpdNFeExec()
Local cRet 		:= "OK" //Processamento Concluido com Sucesso
Local nI 		:= 1
Local nX 		:= 0
Local nInd 		:= 0
Local lCriaTab 	:= .T. //Variavel de controle para executar apenas uma vez a funcao para criacao de tabelas apenas uma vez
Local cTxtLOG	:= ""  //Grava o log do processo
Local aFiliais 	:= {}
Local lCountFil := 0
Local lAllFils 	:= .F.
Local nQtdEmp 	:= len(aRecnoSM0)
Local nContador := 0
Local lExecuta 	:= .T.
Local aSucess 	:= {}
Local cNomeArq		:= __RelDir+"UpdNFe.##R"
Local cEmp

Private aArqUpd	 := {}

//����������������������������������������Ŀ
//�Cria e inicia as reguas de processamento�
//������������������������������������������
oReguaTot:= tMeter():New(127,005,{|u|if(Pcount()>0,nReguaTot:=u,nReguaTot)},nQtdEmp,oWizard:oMPanel[3],290,10,,.T.)
oReguaProc:= tMeter():New(100,005,{|u|if(Pcount()>0,nReguaProc:=u,nReguaProc)},1,oWizard:oMPanel[3],290,10,,.T.)
oReguaProc:NTOTAL := 5
oReguaProc:Set(0)

//��������������������������������Ŀ
//�Say de "Status de Processamento"�
//����������������������������������
oSay1:= TSay():New(  92/*<nRow>*/, 005/*<nCol>*/, {|| STR0019 } /*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, ((oWizard:ODLG:NCLIENTWIDTH/2)-30)/*<nWidth>*/, 8/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ ) //"Processando:"
oSay2:= TSay():New( 120/*<nRow>*/, 005/*<nCol>*/, {|| STR0020 } /*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, ((oWizard:ODLG:NCLIENTWIDTH/2)-30)/*<nWidth>*/, 8/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ ) //"Processamento Total:"

//�����������������������������������Ŀ
//�Seta os alias que serao atualizados�
//�������������������������������������
aadd(aArqUpd,"SE1")
aadd(aArqUpd,"FRK")
aadd(aArqUpd,"FRL")
aadd(aArqUpd,"FRJ")

//��������������������������Ŀ
//�Grava LOG do Processamento�
//����������������������������
cCxText := ""
oMemo:Refresh()
nCurrTot := 0
cTxtLOG := Replicate("=",100)+CHR(13)+CHR(10)+CHR(13)+CHR(10)
cTxtLOG += STR0021 + DToC(Date())+" - "+Time()+CHR(13)+CHR(10) //"In�cio - Instala��o NF-e Educacional - "
UpdNFeLog(cNomeArq, cTxtLOG)
conout(cTxtLOG)
cCxText += cTxtLOG
oMemo:Refresh()

//��������������������������������������Ŀ
//�Define a empresa anterior como "vazio"�
//����������������������������������������
cEmpAnt := ""

//����������������������������������������������Ŀ
//�Roda o update apenas para as empresas marcadas�
//������������������������������������������������
While nI <= Len(aEmpsFils)
	if !aEmpsFils[nI,1] .or. aEmpsFils[nI,2] == cEmpAnt
		nI++
		loop
	endif
	
	cEmpAnt := aEmpsFils[nI,2]
	
	//Fecha a empresa anterior
	If Select('SM0') > 0
	    RpcClearEnv()
		If !( lOpen := MyOpenSm0Ex() )
			Exit 
		EndIf 
	EndIF
	
	SM0->(dbGoto(aEmpsFils[nI,6]))
	RpcSetType(2) 
 	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	lMsFinalAuto := .F.

	cTxtLOG := CHR(13)+CHR(10)+space(5)+ STR0022 +SM0->M0_CODIGO+" - "+DToC(Date())+" - "+Time() + CHR(13)+CHR(10) //"*** In�cio - Processamento Empresa: "
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
	
	cMMProcTot := STR0023 +SM0->M0_CODIGO //"Processando empresa: "
	oMMProcTot:Refresh()
	
	cCxTextAnt := cCxText
	
	//�����������������������������������������������Ŀ
	//�--> 1o.) Atualiza o dicionario de tabelas SX2. �
	//�������������������������������������������������
	cCxText += CHR(13)+CHR(10)+space(5)+ STR0024 +DToC(Date())+" - "+Time()+ STR0025  //" >> Cria��o de tabelas - " " - Em execu��o..."
	oMemo:Refresh()
	nContador := 1
	oReguaProc:Set(nContador)
	cMMProcess := STR0026 //"Criando tabelas..."
	oMMProcess:Refresh()
	
	aSucess := AtuSX2()
	
	cCxText := cCxTextAnt
	
	if aSucess[1] == "0"
		cTxtLOG := CHR(13)+CHR(10)+space(5)+ STR0027 +DToC(Date())+" - "+Time() + CHR(13)+CHR(10) + aSucess[2] //" >> Cria��o de tabelas - [Executado com Sucesso] - "
	else
		cRet := "ERRO" //Ocorreu Erro
		cTxtLOG := CHR(13)+CHR(10)+space(5)+ STR0028 +DToC(Date())+" - "+Time() //" >> Cria��o de tabelas - [* Ocorreu erro. Verifique o CONSOLE.LOG *] - "
	endif
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
	
	cCxTextAnt := cCxText
	
	//�����������������������������������������������Ŀ
	//�--> 2o.) Atualiza o dicionario de tabelas SX3. �
	//�������������������������������������������������
	cCxText += CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0029 +DToC(Date())+" - "+Time()+ STR0025 //" >> Cria��o de campos - "
	oMemo:Refresh()
	nContador := 2
	oReguaProc:Set(nContador)
	cMMProcess := STR0030 //"Cria��o de campos..."
	oMMProcess:Refresh()
	
	aSucess := AtuSX3()
	
	cCxText := cCxTextAnt
	
	if aSucess[1] == "0"
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0031 +DToC(Date())+" - "+Time() + CHR(13)+CHR(10) + aSucess[2] //" >> Cria��o de campos - [Executado com Sucesso] - "
	else
		cRet := "ERRO" //Ocorreu Erro
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0032 +DToC(Date())+" - "+Time() //" >> Cria��o de campos - [* Ocorreu erro. Verifique o CONSOLE.LOG *] - "
	endif
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
		
	cCxTextAnt := cCxText
	
	//�����������������������������������������������Ŀ
	//�--> 3o.) Atualiza Indices SIX.                 �
	//�������������������������������������������������
	cCxText += CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0033 +DToC(Date())+" - "+Time()+STR0025 //" >> Cria��o de Indices - "
	oMemo:Refresh()
	nContador := 3
	oReguaProc:Set(nContador)
	cMMProcess := STR0034 //"Cria��o de Indices..."
	oMMProcess:Refresh()
	
	aSucess := AtuSIX()
	
	cCxText := cCxTextAnt
	
	if aSucess[1] == "0"
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0035 +DToC(Date())+" - "+Time() + CHR(13)+CHR(10) + aSucess[2] //" >> Cria��o de Indices - [Executado com Sucesso] - "
	else
		cRet := "ERRO" //Ocorreu Erro
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0036 +DToC(Date())+" - "+Time() //" >> Cria��o de Indices - [* Ocorreu erro. Verifique o CONSOLE.LOG *] - "
	endif
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
		
	cCxTextAnt := cCxText
	
	//�����������������������������������������������Ŀ
	//�--> 4o.) Atualiza Consultas SXB.               �
	//�������������������������������������������������
	cCxText += CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0037+DToC(Date())+" - "+Time()+STR0025 //" >> Cria��o de Consultas - "
	oMemo:Refresh()
	nContador := 4
	oReguaProc:Set(nContador)
	cMMProcess := STR0038 //"Cria��o de Parametros..."
	oMMProcess:Refresh()
	
	aSucess := AtuSXB()
	
	cCxText := cCxTextAnt
	
	if aSucess[1] == "0"
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0039 +DToC(Date())+" - "+Time() + CHR(13)+CHR(10) + aSucess[2] //" >> Cria��o de Consultas - [Executado com Sucesso] - "
	else
		cRet := "ERRO" //Ocorreu Erro
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0040 +DToC(Date())+" - "+Time() //" >> Cria��o de Consultas - [* Ocorreu erro. Verifique o CONSOLE.LOG *] - "
	endif
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
		
	cCxTextAnt := cCxText
	
	//�����������������������������������������������Ŀ
	//�--> 5o.) Atualiza Parametros SX6.              �
	//�������������������������������������������������
	cCxText += CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0041 +DToC(Date())+" - "+Time()+STR0025 //" >> Cria��o de Parametros - "
	oMemo:Refresh()
	nContador := 5
	oReguaProc:Set(nContador)
	cMMProcess := STR0042 //"Cria��o de Parametros..."
	oMMProcess:Refresh()
	
	aSucess := AtuSX6(SM0->M0_CODFIL)
	
	cCxText := cCxTextAnt
	
	if aSucess[1] == "0"
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+ STR0043 +DToC(Date())+" - "+Time() + CHR(13)+CHR(10) + aSucess[2] //" >> Cria��o de Parametros - [Executado com Sucesso] - "
	else
		cRet := "ERRO" //Ocorreu Erro
		cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0044 +DToC(Date())+" - "+Time() //" >> Cria��o de Parametros - [* Ocorreu erro. Verifique o CONSOLE.LOG *] - "
	endif
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
		
	cCxTextAnt := cCxText
	
	oReguaTot:Set(nI) //Atualiza a Regua Total por Empresa
	
	//Atualiza as tabelas no Banco de Dados
	__SetX31Mode(.F.)
	For nX := 1 To Len(aArqUpd)
		dbSelecTArea(aArqUpd[nx])
		dbCloseArea()
		X31UpdTable(aArqUpd[nx])				
		If __GetX31Error()
			Alert(__GetX31Trace())
			Aviso(STR0045,STR0046+ aArqUpd[nx] + STR0047,{STR0048},2) //"Ocorreu um erro desconhecido durante a atualizacao da tabela : " //". Verifique a integridade do dicionario e da tabela."
		EndIf
	Next nX
	
	cTxtLOG := CHR(13)+CHR(10)+CHR(13)+CHR(10)+space(5)+STR0049 +SM0->M0_CODIGO+" - "+DToC(Date())+" - "+Time() + CHR(13)+CHR(10)
	
	//Grava LOG do Processamento
	UpdNFeLog(cNomeArq, cTxtLOG)
	conout(cTxtLOG)
	cCxText += cTxtLOG
	oMemo:Refresh()
	
    RpcClearEnv()
	If !( lOpen := MyOpenSm0Ex() )
		Exit 
	EndIf 
	
	nI++
EndDo

cTxtLOG := CHR(13)+CHR(10)+STR0050 +DToC(Date())+" - "+Time() //"Fim - Instala��o NF-e Educacional - "
cTxtLOG += CHR(13)+CHR(10)+CHR(13)+CHR(10)+Replicate("=",100)+CHR(13)+CHR(10)+CHR(13)+CHR(10)

//Grava LOG do Processamento
UpdNFeLog(cNomeArq, cTxtLOG)
conout(cTxtLOG)
cCxText += cTxtLOG
oMemo:Refresh()

Return cRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AtuSX2   � Autor �Alberto Deviciente    � Data � 31/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao para a grava��o do SX2.                             ��� 
���          �                                                            ��� 
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function AtuSX2()
Local aSX2   	:= {}							//Array que contem as informacoes das tabelas
Local aEstrut	:= {}							//Array que contem a estrutura da tabela SX2
Local i      	:= 0							//Contador para laco
Local j      	:= 0							//Contador para laco
Local cTexto 	:= ""							//Retorno da funcao, sera utilizado pela funcao chamadora
Local cTbsNew	:= ""
Local cTbsExist	:= ""
Local cPath	 	:= ""							//String para Caminho do arquivo 
Local cNome	 	:= ""							//String para compor o Nome do Arquivo
Local cModo  	:= ""							//String para Modo da Tabela
Local aRet 		:= {}

aEstrut:= {"X2_CHAVE","X2_PATH","X2_ARQUIVO","X2_NOME","X2_NOMESPA","X2_NOMEENG",;
			"X2_DELET","X2_MODO","X2_TTS","X2_ROTINA","X2_UNICO","X2_PYME"}

dbSelectArea("SX2")
SX2->(DbSetOrder(1))	
SX2->( dbSeek("SE1") ) //Seleciona a tabela que eh padrao do sistema para pegar algumas informacoes
cPath := SX2->X2_PATH
cNome := Substr(SX2->X2_ARQUIVO,4,5)  
cModo := SX2->X2_MODO //Modo - (C)Compartilhado ou (E)Exclusivo

/******************************************************************************************
* Adiciona as informacoes das tabelas num array, para trabalho posterior
*******************************************************************************************/
aAdd(aSX2,{	"FRJ",; 								//Chave
			cPath,;									//Path
			"FRJ"+cNome,;							//Arquivo
			STR0051 ,;								//Nome Port.	 //"Header NF-e Educacional"
			STR0051 ,;								//Nome Esp. 	//"Header NF-e Educacional"
			STR0051 ,;								//Nome Ing. 	//"Header NF-e Educacional"
			0,;										//Delete
			cModo,;									//Modo - (C)Compartilhado ou (E)Exclusivo
			"",;									//TTS
			"",;									//Rotina
			"FRJ_FILIAL+FRJ_NUMPRO",;				//Unico
			"S"})									//Pyme
		
aAdd(aSX2,{	"FRK",; 								//Chave
			cPath,;									//Path
			"FRK"+cNome,;							//Nome do Arquivo
			STR0052,;			   					//Nome Port.   //"Itens NF-e Educacional"
			STR0052,;								//Nome Esp.
			STR0052,;								//Nome Ing.
			0,;										//Delete
			cModo,;									//Modo - (C)Compartilhado ou (E)Exclusivo
			"",;									//TTS
			"",;									//Rotina
			"FRK_FILIAL+FRK_NUMPRO+FRK_ITEM+FRK_NUMRA+FRK_CLIENT+FRK_VENCTO+FRK_PRODUT+FRK_NUMTIT",; //Unico
			"S"})									//Pyme

aAdd(aSX2,{	"FRL",; 								//Chave
			cPath,;									//Path
			"FRL"+cNome,;							//Nome do Arquivo
			STR0053,;								//Nome Port. //"Filtros NF-e Educacional"
			STR0053,;								//Nome Esp.
			STR0053,;								//Nome Ing.
			0,;										//Delete
			cModo,;									//Modo - (C)Compartilhado ou (E)Exclusivo
			"",;									//TTS
			"",;									//Rotina
			"FRL_FILIAL+FRL_NUMPRO+FRL_TIPFIL+FRL_CONTEU+FRL_NUMRA",; //Unico
			"S"})									//Pyme
		
			
/*******************************************
Realiza a inclusao das tabelas
*******************************************/
For i:= 1 To Len(aSX2)
	If !Empty(aSX2[i][1])
		If SX2->( !dbSeek(aSX2[i,1]) )
			cTbsNew += " "+aSX2[i,1]+" /"
			RecLock("SX2",.T.) //Adiciona registro
			For j:=1 To Len(aSX2[i])
				If FieldPos(aEstrut[j]) > 0
					FieldPut(FieldPos(aEstrut[j]),aSX2[i,j])
				EndIf
			Next j
			MsUnLock()
		Else
			cTbsExist += " "+aSX2[i,1]+" /"
		EndIf
	EndIf
Next i

//Retira a ultima barra
if !empty(cTbsNew)
	cTbsNew := SubStr(cTbsNew,1,len(cTbsNew)-1)
	cTexto += space(10)+ STR0055 +cTbsNew +CHR(13)+CHR(10)
endif

if !empty(cTbsExist)
	cTbsExist := SubStr(cTbsExist,1,len(cTbsExist)-1)
	cTexto += space(10)+ STR0054 +cTbsExist +CHR(13)+CHR(10)
endif

aRet := {"0",cTexto}

Return aRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �  AtuSX3  � Autor �Alberto Deviciente    � Data � 31/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao do SX3 - Campos        ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function AtuSX3()
Local aSX3           := {}				//Array com os campos das tabelas
Local aEstrut        := {}              //Array com a estrutura da tabela SX3
Local i              := 0				//Laco para contador
Local j              := 0				//Laco para contador
Local cTexto         := ""				//String para msg ao fim do processo
Local cUsadoKey		 := ""				//String que servira para cadastrar um campo como "USADO" em campos chave
Local cReservKey	 := ""				//String que servira para cadastrar um campo como "Reservado" em campos chave
Local cUsadoObr		 := ""				//String que servira para cadastrar um campo como "USADO" em campos obrigatorios
Local cReservObr	 := ""				//String que servira para cadastrar um campo como "Reservado" em campos obrigatorios
Local cUsadoOpc		 := ""				//String que servira para cadastrar um campo como "USADO" em campos opcionais
Local cReservOpc	 := ""				//String que servira para cadastrar um campo como "Reservado" em campos opcionais
Local cUsadoNao		 := ""				//String que servira para cadastrar um campo como "USADO" em campos fora de uso
Local cReservNao	 := ""				//String que servira para cadastrar um campo como "Reservado" em campos fora de uso
Local cOrd			 := ""
Local aFieldsNew 	 := {}
Local aFieldsExist	 := {}
Local cFieldsNew 	 := ""
Local cFieldsExist	 := ""
Local aRet 			 := {}

/**************************
Define a estrutura do array
**************************/
aEstrut:= { "X3_ARQUIVO","X3_ORDEM"  ,"X3_CAMPO"  ,"X3_TIPO"   ,"X3_TAMANHO","X3_DECIMAL","X3_TITULO" ,"X3_TITSPA" ,"X3_TITENG" ,;
			"X3_DESCRIC","X3_DESCSPA","X3_DESCENG","X3_PICTURE","X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO","X3_F3"     ,"X3_NIVEL"  ,;
			"X3_RESERV" ,"X3_CHECK"  ,"X3_TRIGGER","X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT","X3_VLDUSER",;
			"X3_CBOX"   ,"X3_CBOXSPA","X3_CBOXENG","X3_PICTVAR","X3_WHEN"   ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER", "X3_PYME"}

dbSelectArea("SX3")
SX3->(DbSetOrder(2))

/***********************************************************
Seleciona as informacoes de alguns campos para uso posterior
***********************************************************/
If SX3->( dbSeek("A1_FILIAL") ) //Este campo nao eh usado
	cUsadoNao	:= SX3->X3_USADO
	cReservNao	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("A1_COD") ) //Este campo eh chave (obrigatorio e nao permite alterar)
	cUsadoKey	:= SX3->X3_USADO
	cReservKey	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("A6_NOME") ) //Este campo eh obrigatorio e permite alterar
	cUsadoObr	:= SX3->X3_USADO
	cReservObr	:= SX3->X3_RESERV
EndIf

If SX3->( dbSeek("E1_VLBOLSA") ) //Este campo eh opcional e permite alterar
	cUsadoOpc	:= SX3->X3_USADO
	cReservOpc	:= SX3->X3_RESERV
EndIf

//���������������������������������������Ŀ
//�Monta o array com os campos das tabelas�
//�����������������������������������������

//��������������������Ŀ
//�Campos da Tabela FRJ�
//����������������������
aAdd(aSX3,{	"FRJ",;								//Arquivo
			"01",;								//Ordem
			"FRJ_FILIAL",;						//Campo
			"C",;								//Tipo
			2,;									//Tamanho
			0,;									//Decimal
			STR0056 ,;					   	 	//Titulo  //"Filial"
			STR0056,;					    	//Titulo	
			STR0056,;					   		//Titulo
			STR0056,;					    	//Descricao
			STR0056,;					   	 	//Descricao
			STR0056,;					   	 	//Descricao
			"@!",;						   		//Picture
			'',;				   		   		//VALID
			cUsadoNao,;					   		//USADO
			'',;						   		//RELACAO
			"",;						   		//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"02",;								//Ordem
			"FRJ_NUMPRO",;						//Campo
			"C",;								//Tipo
			10,;								//Tamanho
			0,;									//Decimal
			STR0057,;						    //Titulo  //"Num Proc."
			STR0057,;						    //Titulo
			STR0057,;						    //Titulo
			STR0058,;		    				//Descricao	//"Num. Processo Simula��o"
			STR0058,;		   					//Descricao
			STR0058,;					    	//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoKey,;							//USADO
			'GetSXENum("FRJ","FRJ_NUMPRO")',;	//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservKey,;						//RESERV
			"","","","S","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 
			
			
aAdd(aSX3,{	"FRJ",;								//Arquivo
			"03",;								//Ordem
			"FRJ_LOTE",;						//Campo
			"C",;								//Tipo
			30,;								//Tamanho
			0,;									//Decimal
			STR0059,; 							//Titulo //"Lote do Proc"
			STR0059,; 							//Titulo
			STR0059,;		 					//Titulo
			STR0060,; 							//Descricao		//"Lote do processo"
			STR0060,; 			 				//Descricao
			STR0060,; 			   				//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","S","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"04",;								//Ordem
			"FRJ_STATUS",;						//Campo
			"C",;								//Tipo
			1,;									//Tamanho
			0,;									//Decimal
			STR0061,;					    	//Titulo		//Status
			STR0061,;					    	//Titulo
			STR0061,;					    	//Titulo
			STR0062,;		      				//Descricao		//"Status do Processo"
			STR0062,;		    				//Descricao
			STR0062,;		    				//Descricao
			"",;								//Picture
			''  ,;								//VALID
			cUsadoObr,;							//USADO
			"1",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservObr,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"1="+STR0063+";2="+STR0064,"1="+STR0063+";2="+STR0064,"1="+STR0063+";2="+STR0064,;//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"05",;								//Ordem
			"FRJ_DTSIMU",;						//Campo
			"D",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0065,;	  					    //Titulo
			STR0065,;						    //Titulo
			STR0065,;			   			    //Titulo
			STR0066,;		   				    //Descricao
			STR0066,;		   				    //Descricao
			STR0066,;			   				//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoObr,;							//USADO
			'dDataBase',;						//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservObr,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRJ",;								//Arquivo
			"06",;								//Ordem
			"FRJ_HRSIMU",;						//Campo
			"C",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0067,;						    //Titulo
			STR0067,;		   				    //Titulo
			STR0067,;					   		//Titulo
			STR0068,;			    			//Descricao
			STR0068,;					    	//Descricao
			STR0068,;		  			   		//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoObr,;							//USADO
			'Time()',;							//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservObr,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"07",;								//Ordem
			"FRJ_USSIMU",;						//Campo
			"C",;								//Tipo
			15,;								//Tamanho
			0,;									//Decimal
			STR0069,;			 			    //Titulo
			STR0069,;			 			    //Titulo
			STR0069,;						    //Titulo
			STR0070,;		 					//Descricao
			STR0070,;		 			   		//Descricao
			STR0070,;		   					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoObr,;							//USADO
			'Subs(cUsuario,7,15)',;				//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservObr,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"08",;								//Ordem
			"FRJ_DTGENF",;						//Campo
			"D",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0071,;			   			    //Titulo
			STR0071,;			   			    //Titulo
			STR0071,;						    //Titulo
			STR0072,;						    //Descricao
			STR0072,;		  	  			    //Descricao
			STR0072,;		 				   //Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME			

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"09",;								//Ordem
			"FRJ_HRGENF",;						//Campo
			"C",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0073,;			 			    //Titulo
			STR0073,;						    //Titulo
			STR0073,;						    //Titulo
			STR0074,;						    //Descricao
			STR0074,;		 				   //Descricao
			STR0074,;		    				//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRJ",;								//Arquivo
			"10",;								//Ordem
			"FRJ_USGENF",;						//Campo
			"C",;								//Tipo
			15,;								//Tamanho
			0,;									//Decimal
			STR0075,;						    //Titulo
			STR0075,;		   				    //Titulo
			STR0075,;			  			    //Titulo
			STR0076,;	   						//Descricao
			STR0076,;						    //Descricao
			STR0076,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			'',;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","S","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME


//��������������������Ŀ
//�Campos da Tabela FRK�
//����������������������
aAdd(aSX3,{	"FRK",;								//Arquivo
			"01",;								//Ordem
			"FRK_FILIAL",;						//Campo
			"C",;								//Tipo
			2,;									//Tamanho
			0,;									//Decimal
			STR0056,;					    	//Titulo
			STR0056,;					    	//Titulo
			STR0056,;					    	//Titulo
			STR0056,;					    	//Descricao
			STR0056,;					    	//Descricao
			STR0056,;					    	//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"02",;								//Ordem
			"FRK_NUMPRO",;						//Campo
			"C",;								//Tipo
			10,;								//Tamanho
			0,;									//Decimal
			STR0057,;						    //Titulo
			STR0057,;						    //Titulo
			STR0057,;						    //Titulo
			STR0058,;		    				//Descricao
			STR0058,;		    				//Descricao
			STR0058,;	   				   		//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoKey,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservKey,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"03",;								//Ordem
			"FRK_LOTE",;						//Campo
			"C",;								//Tipo
			30,;								//Tamanho
			0,;									//Decimal
			STR0059,; 	  			   			//Titulo
			STR0059,; 			  		 		//Titulo
			STR0059,; 			 	  			//Titulo
			STR0060,; 		  	   				//Descricao
			STR0060,; 		   	   				//Descricao
			STR0060,; 		 	  				//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"04",;								//Ordem
			"FRK_ITEM",;						//Campo
			"C",;								//Tipo
			3,;									//Tamanho
			0,;									//Decimal
			STR0135,; 							//Titulo
			STR0135,; 							//Titulo
			STR0135,; 							//Titulo
			STR0135,; 							//Descricao
			STR0135,; 							//Descricao
			STR0135,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME			
		
aAdd(aSX3,{	"FRK",;								//Arquivo
			"05",;								//Ordem
			"FRK_NUMRA",;						//Campo
			"C",;								//Tipo
			20,;								//Tamanho
			0,;									//Decimal
			STR0077,; 							//Titulo
			STR0077,; 							//Titulo
			STR0077,; 							//Titulo
			STR0077,; 							//Descricao
			STR0077,; 							//Descricao
			STR0077,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"06",;								//Ordem
			"FRK_CLIENT",;						//Campo
			"C",;								//Tipo
			TamSX3("E1_CLIENTE")[1],;		   	//Tamanho
			0,;									//Decimal
			STR0078,; 				  			//Titulo
			STR0078,;	 				   		//Titulo
			STR0078,; 				  			//Titulo
			STR0078,; 				  			//Descricao
			STR0078,; 				  			//Descricao
			STR0078,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","001","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"07",;								//Ordem
			"FRK_VENCTO",;						//Campo
			"D",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0079,;							//Titulo
			STR0079,;							//Titulo
			STR0079,;							//Titulo
			STR0080,;							//Descricao
			STR0080,;							//Descricao
			STR0080,;							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"08",;								//Ordem
			"FRK_NUMTIT",;						//Campo
			"C",;								//Tipo
			TamSX3("E1_NUM")[1],;			   	//Tamanho
			0,;									//Decimal
			STR0081,; 						 	//Titulo
			STR0081,; 						 	//Titulo
			STR0081,; 						 	//Titulo
			STR0082,; 							//Descricao
			STR0082,; 							//Descricao
			STR0082,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","018","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"09",;								//Ordem
			"FRK_PRODUT",;						//Campo
			"C",;								//Tipo
			TamSX3("B1_COD")[1],;			   	//Tamanho
			0,;									//Decimal
			STR0083,; 				  			//Titulo
			STR0083,; 				  			//Titulo
			STR0083,; 				 			//Titulo
			STR0084,; 							//Descricao
			STR0084,; 							//Descricao
			STR0084,; 		 					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"SB1",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","030","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"10",;								//Ordem
			"FRK_VALOR",;						//Campo
			"N",;								//Tipo
			14,;								//Tamanho
			2,;									//Decimal
			STR0085,;					    	//Titulo
			STR0085,;					    	//Titulo
			STR0085,;					    	//Titulo
			STR0085,;					    	//Descricao
			STR0085,;					    	//Descricao
			STR0085,;					    	//Descricao
			"@E 999,999,999.99",;				//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"11",;								//Ordem
			"FRK_DEDUC",;						//Campo
			"N",;								//Tipo
			14,;								//Tamanho
			2,;									//Decimal
			STR0086,; 							//Titulo
			STR0086,; 	  						//Titulo
			STR0086,; 	  						//Titulo
			STR0087,; 	  						//Descricao
			STR0087,; 	  						//Descricao
			STR0087,; 	  						//Descricao
			"@E 999,999,999.99",;				//Picture
			'',;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"12",;								//Ordem
			"FRK_AGRUP",;						//Campo
			"C",;								//Tipo
			1,;							    	//Tamanho
			0,;									//Decimal
			STR0088,; 	   	  					//Titulo
			STR0088,; 	   	  					//Titulo
			STR0088,; 		  					//Titulo
			STR0089,; 	  						//Descricao
			STR0089,; 	  						//Descricao
			STR0089,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"13",;								//Ordem
			"FRK_NOTA",;						//Campo
			"C",;								//Tipo
			TamSX3("F2_DOC")[1],;			   	//Tamanho
			0,;									//Decimal
			STR0090,; 					  		//Titulo
			STR0090,;	 						//Titulo
			STR0090,; 							//Titulo
			STR0091,; 							//Descricao
			STR0091,; 							//Descricao
			STR0091,;				 			//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","018","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

aAdd(aSX3,{	"FRK",;								//Arquivo
			"14",;								//Ordem
			"FRK_SERIE",;						//Campo
			"C",;								//Tipo
			3,;									//Tamanho
			0,;									//Decimal
			STR0092,; 							//Titulo
			STR0092,; 							//Titulo
			STR0092,; 							//Titulo
			STR0093,; 							//Descricao
			STR0093,; 							//Descricao
			STR0093,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"15",;								//Ordem
			"FRK_NFITEM",;						//Campo
			"C",;								//Tipo
			2,;									//Tamanho
			0,;									//Decimal
			STR0138,; 							//Titulo
			STR0138,; 							//Titulo
			STR0138,; 							//Titulo
			STR0138,; 							//Descricao
			STR0138,; 							//Descricao
			STR0138,; 							//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME  			

aAdd(aSX3,{	"FRK",;								//Arquivo
			"16",;								//Ordem
			"FRK_DTFAT",;						//Campo
			"D",;								//Tipo
			8,;									//Tamanho
			0,;									//Decimal
			STR0094,;							//Titulo
			STR0094,;							//Titulo
			STR0094,;							//Titulo
			STR0095,;		  		 			//Descricao
			STR0095,;		  					//Descricao
			STR0095,;		  	  				//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","V",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"17" ,;				   				//Ordem
			"FRK_IDBOLE",;						//Campo
			"N",;								//Tipo
			TamSX3("E1_IDBOLET")[1],;			//Tamanho
			0,;									//Decimal
			STR0096,;				    		//Titulo
			STR0096,;				    		//Titulo
			STR0096,;				    		//Titulo
			STR0096,;	    					//Descricao
			STR0096,;	    					//Descricao
			STR0096,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

aAdd(aSX3,{	"FRK",;								//Arquivo
			"18" ,;				   				//Ordem
			"FRK_TES",;					   		//Campo
			"C",;								//Tipo
			TamSX3("F4_CODIGO")[1],;			//Tamanho
			0,;									//Decimal
			STR0137,;				    		//Titulo
			STR0137,;				    		//Titulo
			STR0137,;				    		//Titulo
			STR0137,;	    					//Descricao
			STR0137,;	    					//Descricao
			STR0137,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

aAdd(aSX3,{ "FRK",;                             //Arquivo
			"19" ,;                             //Ordem
			"FRK_CCUSTO",;                      //Campo
			"C",;                               //Tipo
			TamSX3("D2_CCUSTO")[1],;            //Tamanho
			0,;                                 //Decimal
			STR0141,;                           //Titulo
			STR0141,;                           //Titulo
			STR0141,;                           //Titulo
			STR0141,;                           //Descricao
			STR0141,;                           //Descricao
			STR0141,;                           //Descricao
			"",;                                //Picture
			"",;                                //VALID
			cUsadoNao,;                         //USADO
			"",;                                //RELACAO
			"",;                                //F3
			1,;                                 //NIVEL
			cReservNao,;                        //RESERV
			"","","","N","",;                   //CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;                          //CONTEXT, OBRIGAT, VLDUSER
			"","","",;                          //CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})                //PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{ "FRK",;                             //Arquivo
			"20" ,;                             //Ordem
			"FRK_CLVL",;                        //Campo
			"C",;                               //Tipo
			TamSX3("D2_CLVL")[1],;              //Tamanho
			0,;                                 //Decimal
			STR0143,;                           //Titulo
			STR0143,;                           //Titulo
			STR0143,;                           //Titulo
			STR0143,;                           //Descricao
			STR0143,;                           //Descricao
			STR0143,;                           //Descricao
			"",;                                //Picture
			"",;                                //VALID
			cUsadoNao,;                         //USADO
			"",;                                //RELACAO
			"",;                                //F3
			1,;                                 //NIVEL
			cReservNao,;                        //RESERV
			"","","","N","",;                   //CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;                          //CONTEXT, OBRIGAT, VLDUSER
			"","","",;                          //CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})                //PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{ "FRK",;                             //Arquivo
			"21" ,;                             //Ordem
			"FRK_ITEMCC",;                      //Campo
			"C",;                               //Tipo
			TamSX3("D2_ITEMCC")[1],;            //Tamanho
			0,;                                 //Decimal
			STR0142,;                           //Titulo
			STR0142,;                           //Titulo
			STR0142,;                           //Titulo
			STR0142,;                           //Descricao
			STR0142,;                           //Descricao
			STR0142,;                           //Descricao
			"",;                                //Picture
			"",;                                //VALID
			cUsadoNao,;                         //USADO
			"",;                                //RELACAO
			"",;                                //F3
			1,;                                 //NIVEL
			cReservNao,;                        //RESERV
			"","","","N","",;                   //CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;                          //CONTEXT, OBRIGAT, VLDUSER
			"","","",;                          //CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})                //PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"19" ,;				   				//Ordem
			"FRK_CCUSTO",;					   	//Campo
			"C",;								//Tipo
			TamSX3("D2_CCUSTO")[1],;			//Tamanho
			0,;									//Decimal
			STR0141,;				    		//Titulo
			STR0141,;				    		//Titulo
			STR0141,;				    		//Titulo
			STR0141,;	    					//Descricao
			STR0141,;	    					//Descricao
			STR0141,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 			
			
aAdd(aSX3,{	"FRK",;								//Arquivo
			"20" ,;				   				//Ordem
			"FRK_CLVL",;					   	//Campo
			"C",;								//Tipo
			TamSX3("D2_CLVL")[1],;				//Tamanho
			0,;									//Decimal
			STR0143,;				    		//Titulo
			STR0143,;				    		//Titulo
			STR0143,;				    		//Titulo
			STR0143,;	    					//Descricao
			STR0143,;	    					//Descricao
			STR0143,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME			

aAdd(aSX3,{	"FRK",;								//Arquivo
			"21" ,;				   				//Ordem
			"FRK_ITEMCC",;					   	//Campo
			"C",;								//Tipo
			TamSX3("D2_ITEMCC")[1],;			//Tamanho
			0,;									//Decimal
			STR0142,;				    		//Titulo
			STR0142,;				    		//Titulo
			STR0142,;				    		//Titulo
			STR0142,;	    					//Descricao
			STR0142,;	    					//Descricao
			STR0142,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRK",;								//Arquivo
			"22" ,;				   				//Ordem
			"FRK_ITPROC",;					   	//Campo
			"C",;								//Tipo
			3,;			//Tamanho
			0,;									//Decimal
			STR0151,;				    		//Titulo
			STR0151,;				    		//Titulo
			STR0151,;				    		//Titulo
			STR0152,;	    					//Descricao
			STR0152,;	    					//Descricao
			STR0152,;	    					//Descricao
			"",;								//Picture
			"",;								//VALID
			cUsadoNao,;						//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
//��������������������Ŀ
//�Campos da Tabela FRL�
//����������������������
aAdd(aSX3,{	"FRL",;								//Arquivo
			"01",;								//Ordem
			"FRL_FILIAL",;						//Campo
			"C",;								//Tipo
			2,;									//Tamanho
			0,;									//Decimal
			STR0056,;					    	//Titulo
			STR0056,;						    //Titulo
			STR0056,;						    //Titulo
			STR0056,;						    //Descricao
			STR0056,;					    	//Descricao
			STR0056,;					    	//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoNao,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservNao,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME

aAdd(aSX3,{	"FRL",;								//Arquivo
			"02",;								//Ordem
			"FRL_NUMPRO",;						//Campo
			"C",;								//Tipo
			10,;								//Tamanho
			0,;									//Decimal
			STR0097,;						    //Titulo
			STR0097,;						    //Titulo
			STR0097,;						    //Titulo
			STR0098,;		    				//Descricao
			STR0098,;		    				//Descricao
			STR0098,;						    //Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

aAdd(aSX3,{	"FRL",;								//Arquivo
			"03",;								//Ordem
			"FRL_TIPFIL",;						//Campo
			"C",;								//Tipo
			02,;								//Tamanho
			0,;									//Decimal
			STR0099,;						    //Titulo
			STR0099,;				 		    //Titulo
			STR0099,;						    //Titulo
			STR0100,;				    		//Descricao
			STR0100,;						    //Descricao
			STR0100,;						    //Descricao
			"",;			   					//Picture
			"",;				   				//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

aAdd(aSX3,{	"FRL",;								//Arquivo
			"04",;								//Ordem
			"FRL_NUMRA",;						//Campo
			"C",;								//Tipo
			20,;								//Tamanho
			0,;									//Decimal
			STR0077,;				   			//Titulo
			STR0077,;				    		//Titulo
			STR0077,;				    		//Titulo
			STR0077,;			    			//Descricao
			STR0077,;			    			//Descricao
			STR0077,;			    			//Descricao
			"",;								//Picture
			"",;				   				//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 
			

aAdd(aSX3,{	"FRL",;								//Arquivo
			"05",;								//Ordem
			"FRL_CONTEU",;						//Campo
			"C",;								//Tipo
			30,;								//Tamanho
			0,;									//Decimal
			STR0101,;						    //Titulo
			STR0101,;						    //Titulo
			STR0101,;						    //Titulo
			STR0102,;			  				//Descricao
			STR0102,;						    //Descricao
			STR0102,;						    //Descricao
			"",;								//Picture
			"",;				   				//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME
			
//��������������������Ŀ
//�Campos da Tabela SE1�
//����������������������
SX3->( dbSetOrder(1) )

// Verifica qual eh a ultima ordem para incluir o registro na proxima ordem
cOrd := ""
SX3->( dbSeek( "SE1" ) )
While SX3->( !Eof() .And. Left( X3_ARQUIVO, 3 ) == "SE1" )
	cOrd := SX3->X3_ORDEM
	SX3->( dbSkip() )
Enddo                         

//Faz tratamento para ordenar utilizando letras caso ja exista mais de 99 campos na tabela
if cOrd >= "99"
	if SubStr(cOrd,2,1) == "9"
		cOrd := soma1(SubStr(cOrd,1,1))+"0"
	else
		cOrd := soma1(cOrd)
	endif
else
	cOrd := soma1(cOrd)
	cOrd := StrZero(val(cOrd), 2)
endif
			
aAdd(aSX3,{	"SE1",;								//Arquivo
			cOrd ,;				   				//Ordem
			"E1_NUMPRO",;						//Campo
			"C",;								//Tipo
			10,;								//Tamanho
			0,;									//Decimal
			STR0103,;						    //Titulo
			STR0103,;		   				    //Titulo
			STR0103,;						    //Titulo
			STR0104,;	    					//Descricao
			STR0104,;	   						//Descricao
			STR0104,;	    					//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 

//Faz tratamento para ordenar utilizando letras caso ja exista mais de 99 campos na tabela
if cOrd >= "99"
	if SubStr(cOrd,2,1) == "9"
		cOrd := soma1(SubStr(cOrd,1,1))+"0"
	else
		cOrd := soma1(cOrd)
	endif
else
	cOrd := soma1(cOrd)
	cOrd := StrZero(val(cOrd), 2)
endif

aAdd(aSX3,{	"SE1",;								//Arquivo
			cOrd ,;				   				//Ordem
			"E1_ITEPRO",;						//Campo
			"C",;								//Tipo
			3,;									//Tamanho
			0,;									//Decimal
			STR0139,;						    //Titulo
			STR0139,;		   				    //Titulo
			STR0139,;						    //Titulo
			STR0140,;	    					//Descricao
			STR0140,;	   						//Descricao
			STR0140,;	    					//Descricao
			"@!",;								//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 			

			
//Faz tratamento para ordenar utilizando letras caso ja exista mais de 99 campos na tabela
if cOrd >= "99"
	if SubStr(cOrd,2,1) == "9"
		cOrd := soma1(SubStr(cOrd,1,1))+"0"
	else
		cOrd := soma1(cOrd)
	endif
else
	cOrd := soma1(cOrd)
	cOrd := StrZero(val(cOrd), 2)
endif

aAdd(aSX3,{	"SE1",;								//Arquivo
			cOrd ,;				   				//Ordem
			"E1_PRODUTO",;						//Campo
			"C",;								//Tipo
			TamSX3("B1_COD")[1],;				//Tamanho
			0,;									//Decimal
			STR0084,;						    //Titulo
			STR0084,;						    //Titulo
			STR0084,;						    //Titulo
			STR0084,;	    					//Descricao
			STR0084,;	    					//Descricao
			STR0084,;	    					//Descricao
			"@!",;				  				//Picture
			"",;								//VALID
			cUsadoOpc,;							//USADO
			"",;								//RELACAO
			"SB1",;								//F3
			1,;									//NIVEL
			cReservOpc,;						//RESERV
			"","","","N","",;					//CHECK, TRIGGER, PROPRI, BROWSE, VISUAL
			"","","",;							//CONTEXT, OBRIGAT, VLDUSER
			"","","",;							//CBOX, CBOX SPA, CBOX ENG
			"","","","030","","S"})				//PICTVAR, WHEN, INIBRW, SXG, FOLDER, PYME 
			

/******************************************************************************************
Grava informacoes do array no banco de dados
******************************************************************************************/
SX3->(DbSetOrder(2))

For i:= 1 To Len(aSX3)
	If !Empty(aSX3[i][1])
		If !dbSeek(aSX3[i,3])
			nPos := aScan( aFieldsNew, { |x| x[1] == aSX3[i,1] } )
			if nPos > 0
				aAdd( aFieldsNew[nPos][2], aSX3[i,3] )
			else
				aAdd( aFieldsNew, {aSX3[i,1], {aSX3[i,3]} } )
			endif
			RecLock("SX3",.T.)
		Else
			aSX3[i,2] := SX3->X3_ORDEM //Se o campo ja existir, entao utiliza a ordem ja existente
			
			nPos := aScan( aFieldsExist, { |x| x[1] == aSX3[i,1] } )
			if nPos > 0
				aAdd( aFieldsExist[nPos][2], aSX3[i,3] )
			else
				aAdd( aFieldsExist, {aSX3[i,1], {aSX3[i,3]} } )
			endif
			RecLock("SX3",.F.)
		Endif
		If aScan( aArqUpd, aSX3[i,1] ) == 0
			aAdd( aArqUpd, aSX3[i,1] )
		EndIf
		
		For j:=1 To Len(aSX3[i])
			If FieldPos(aEstrut[j])>0 .And. aSX3[i,j] != NIL
				FieldPut(FieldPos(aEstrut[j]),aSX3[i,j])
			EndIf
		Next j
		MsUnLock()
	EndIf
Next i

for i:=1 to len(aFieldsNew)
	cFieldsNew += space(15)+ STR0105 +aFieldsNew[i][1]+":" +CHR(13)+CHR(10)
	for j:=1 to len(aFieldsNew[i][2])
		cFieldsNew += space(25)+aFieldsNew[i][2][j]+CHR(13)+CHR(10)
	next j
next i

for i:=1 to len(aFieldsExist)
	cFieldsExist += space(15)+ STR0105 +aFieldsExist[i][1]+":" +CHR(13)+CHR(10)
	for j:=1 to len(aFieldsExist[i][2])
		cFieldsExist += space(25)+aFieldsExist[i][2][j]+CHR(13)+CHR(10)
	next j
next i

if !empty(cFieldsNew)
	cTexto += space(10)+STR0106+ CHR(13)+CHR(10) + cFieldsNew
endif

if !empty(cFieldsExist)
	cTexto += space(10)+STR0107+ CHR(13)+CHR(10) + cFieldsExist
endif

aRet := {"0",cTexto}

Return aRet

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AtuSIX   � Autor � Alberto Deviciente   � Data � 31/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao do SIX - Indices       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function AtuSIX()
Local cTexto    := ""						//String para msg ao fim do processo
Local aSix      := {}						//Array que armazenara os indices
Local aEstrut   := {}				        //Array com a estrutura da tabela SiX
Local i         := 0 						//Contador para laco
Local j         := 0 						//Contador para laco
Local cIdxsNew  := ""
Local cIdxsAlter:= ""
Local cIdxsExist:= ""
Local lDelInd   := .F. 
Local aRet 		:= {}

/*********************************************************************************************
Define estrutura do array
*********************************************************************************************/
//INDICE ORDEM CHAVE DESCRICAO DESCSPA DESCENG PROPRI F3 NICKNAME SHOWPESQ
aEstrut:= {"INDICE","ORDEM","CHAVE","DESCRICAO","DESCSPA","DESCENG","PROPRI","F3","NICKNAME","SHOWPESQ"}

aAdd(aSIX,{"FRJ",;   																//Indice
		"1",;                 														//Ordem
		"FRJ_FILIAL+FRJ_NUMPRO",;  													//Chave
		STR0108,; 																	//Descicao Port.
		STR0108,;												   					//Descicao Spa.
		STR0108,;																	//Descicao Eng.
		"S",; 																		//Proprietario
		"",;  																		//F3
		"",;  																		//NickName
		"S"}) 																		//ShowPesq  

aAdd(aSIX,{"FRJ",;   																//Indice
		"2",;                 														//Ordem
		"FRJ_FILIAL+FRJ_STATUS+FRJ_NUMPRO",;										//Chave
		STR0109,;																	//Descicao Port.
		STR0109,;					   												//Descicao Port.
		STR0109,;																	//Descicao Port.
		"S",; 																		//Proprietario
		"",;  																		//F3
		"",;  																		//NickName
		"S"}) 																		//ShowPesq  

aAdd(aSIX,{"FRJ",;   																//Indice
		"3",;                 														//Ordem
		"FRJ_FILIAL+FRJ_LOTE",;	 													//Chave
		STR0110,;  																	//Descicao Port.
		STR0110,;					   	 											//Descicao Port.
		STR0110,;								  									//Descicao Port.
		"S",; 																		//Proprietario
		"",;  																		//F3
		"",;  																		//NickName
		"S"}) 																		//ShowPesq  		
 				    																
aAdd(aSIX,{"FRK",;                                                                  //Indice
		"1",;                                                                       //Ordem
		"FRK_FILIAL+FRK_NUMPRO+FRK_ITEM+FRK_NUMRA+FRK_CLIENT+FRK_VENCTO+FRK_PRODUT+FRK_NUMTIT",;	//Chave
		STR0111,;																	//Descicao Port.
		STR0111,;																	//Descicao Port.
		STR0111,;																	//Descicao Port.
		"S",;                                                                       //Proprietario
		"",;                                                                        //F3
		"",;                                                                        //NickName
		"S"})       																//ShowPesq 
		
aAdd(aSIX,{"FRK",;                                                                  //Indice
		"2",;                                                                       //Ordem
		"FRK_FILIAL+FRK_NUMPRO+FRK_ITEM+FRK_CLIENT+FRK_NUMRA+FRK_VENCTO+FRK_NUMTIT",;//Chave
		STR0112,;																	//Descicao Port.
		STR0112,;																	//Descicao Port.
		STR0112,;																	//Descicao Port.
		"S",;                                                                       //Proprietario
		"",;                                                                        //F3
		"",;                                                                        //NickName
		"S"})       																//ShowPesq


aAdd(aSIX,{"FRL",;   																//Indice
		"1",;                 														//Ordem
		"FRL_FILIAL+FRL_NUMPRO+FRL_TIPFIL+FRL_CONTEU",;								//Chave
		STR0113,;							 										//Descicao Port.
		STR0113,; 							 										//Descicao Port.
		STR0113,; 																	//Descicao Port.
		"S",; 																		//Proprietario
		"",;  																		//F3
		"",;  																		//NickName
		"S"}) 																		//ShowPesq
		
aAdd(aSIX,{"FRL",;   																//Indice
		"2",;                 														//Ordem
		"FRL_FILIAL+FRL_NUMPRO+FRL_TIPFIL+FRL_NUMRA",;								//Chave
		STR0114,; 			 														//Descicao Port.
		STR0114,; 																	//Descicao Port.
		STR0114,; 																	//Descicao Port.
		"S",; 																		//Proprietario
		"",;  																		//F3
		"",;  																		//NickName
		"S"}) 																		//ShowPesq
		
If cPaisLoc == "MEX"
	aAdd(aSIX,{"SE1",;   																//Indice
			"K",;                 														//Ordem
			"E1_FILIAL+E1_SERIE+E1_NUMNOTA",;											//Chave
			"S�rie Doc + Num Doc.",; 			 										//Descicao Port.
			"S�rie Doc + Num Doc.",; 													//Descicao Port.
			"S�rie Doc + Num Doc.",; 													//Descicao Port.
			"S",; 																		//Proprietario
			"",;  																		//F3
			"",;  																		//NickName
			"S"}) 																		//ShowPesq
EndIf
		                                                                          
/*********************************************************************************************
Grava as informacoes do array na tabela SIX
*********************************************************************************************/
dbSelectArea("SIX")
SIX->(DbSetOrder(1))	

For i:= 1 To Len(aSIX)
	If !Empty(aSIX[i,1])
		If !dbSeek(aSIX[i,1]+aSIX[i,2])
			RecLock("SIX",.T.)
			lDelInd := .F.
		Else
			RecLock("SIX",.F.)
			lDelInd := .T. //Se for alteracao precisa apagar o indice do banco
		EndIf
		
		If UPPER(AllTrim(CHAVE)) != UPPER(Alltrim(aSIX[i,3]))
			if lDelInd
				cIdxsAlter += " "+aSIX[i,1]+aSIX[i,2]+" /"
			else
				cIdxsNew += " "+aSIX[i,1]+aSIX[i,2]+" /"
			endif
			If aScan( aArqUpd, aSIX[i,1] ) == 0
				aAdd( aArqUpd, aSIX[i,1] )
			EndIf
			For j:=1 To Len(aSIX[i])
				If FieldPos(aEstrut[j])>0
					FieldPut(FieldPos(aEstrut[j]),aSIX[i,j])
				EndIf
			Next j
			MsUnLock()
			If lDelInd
				TcInternal(60,RetSqlName(aSix[i,1]) + "|" + RetSqlName(aSix[i,1]) + aSix[i,2]) //Exclui sem precisar baixar o TOP
			Endif	
		Else
			cIdxsExist += " "+aSIX[i,1]+aSIX[i,2]+" /"
		EndIf
	EndIf
Next i

If !empty(cIdxsNew)
	cIdxsNew := SubStr(cIdxsNew,1,len(cIdxsNew)-1)
	cTexto += space(10)+STR0115+ cIdxsNew +CHR(13)+CHR(10)
EndIf

If !empty(cIdxsAlter)
	cIdxsAlter := SubStr(cIdxsAlter,1,len(cIdxsAlter)-1)
	cTexto += space(10)+STR0116+ cIdxsAlter +CHR(13)+CHR(10)
EndIf

If !empty(cIdxsExist)
	cIdxsExist := SubStr(cIdxsExist,1,len(cIdxsExist)-1)
	cTexto += space(10)+STR0117+ cIdxsExist +CHR(13)+CHR(10)
EndIf

aRet := {"0",cTexto}

Return aRet


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AtuSX6   � Autor � Alberto Deviciente   � Data � 31/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao do SX6 - Parametros    ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function AtuSX6(cCodFilial)
Local aRet 			:= {}
Local cTexto    	:= ""						//String para msg ao fim do processo
Local aEstrut   	:= {}				        //Array com a estrutura da tabela SX6
Local aSx6      	:= {}						//Array que armazenara os indices
Local i         	:= 0 						//Contador para laco
Local j         	:= 0 						//Contador para laco
Local cParmsNew		:= ""
Local cParmsExist  	:= ""

Default cCodFilial := "  "


/*********************************************************************************************
Define estrutura do array
*********************************************************************************************/
aEstrut:= { "X6_Filial","X6_VAR","X6_TIPO","X6_DESCRIC","X6_DSCSPA","X6_DSCENG","X6_DESC1","X6_DSCSPA1",;
			"X6_DSCENG1","X6_DESC2","X6_DSCSPA2","X6_DSCENG2", "X6_CONTEUD","X6_CONTSPA", "X6_CONTENG",;
			"X6_PROPRI", "X6_PYME"}

/*********************************************************************************************
Define os dados do parametro
*********************************************************************************************/
aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISTS",;											//Var
		"C",;                 									//Tipo
		STR0118,; 												//Descric
		STR0118,; 												//Descric
		STR0118,; 												//Descric
		STR0119,;												//Desc1
		STR0119,;												//Desc1
		STR0119,;												//Desc1
		"",; 													//Desc2
		"",;													//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;													//ContSpa
		"",;													//ContEng
		"S",;													//Propri		
		"S"})													//Pyme

aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISPR",;											//Var
		"C",;                 									//Tipo
		STR0120,; 		 		 		 		 		 		//Descric
		STR0120,; 	 		 		 		 		 		 	//Descric
		STR0120,;  		  		 		 		 				//Descric
		STR0121,;							 		 			//Desc1
		STR0121,;									 		 	//Desc1
		STR0121,;										 		//Desc1
		"",; 													//Desc2
		"",;													//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;													//ContSpa
		"",;													//ContEng
		"S",;													//Propri		
		"S"})													//Pyme

aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISCP",;											//Var
		"C",;                 									//Tipo
		STR0122,; 												//Descric
		STR0122,; 												//Descric
		STR0122,; 												//Descric
		STR0123,; 												//Desc1
		STR0123,; 												//Desc1
		STR0123,;												//Desc1
		"",; 											   		//Desc2
		"",;											   		//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;											 		//ContSpa
		"",;											  		//ContEng
		"S",;													//Propri		
		"S"})													//Pyme   
		
		
aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISCC",;											//Var
		"C",;                 									//Tipo
		STR0144,; 												//Descric
		STR0144,; 												//Descric
		STR0144,; 												//Descric
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		"",; 													//Desc2
		"",;													//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;													//ContSpa
		"",;													//ContEng
		"S",;													//Propri		
		"S"})													//Pyme
		
aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISCV",;											//Var
		"C",;                 									//Tipo
		STR0146,; 												//Descric
		STR0146,; 												//Descric
		STR0146,; 												//Descric
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		"",; 													//Desc2
		"",;													//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;													//ContSpa
		"",;													//ContEng
		"S",;													//Propri		
		"S"})													//Pyme

aAdd(aSx6,{"  ",;												//Filial
		"MV_ACFISIC",;											//Var
		"C",;                 									//Tipo
		STR0147,; 												//Descric
		STR0147,; 												//Descric
		STR0147,; 												//Descric
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		STR0145,;												//Desc1
		"",; 													//Desc2
		"",;													//DscSpa2
		"",;													//DscEng2
		"",;													//Conteud
		"",;													//ContSpa
		"",;													//ContEng
		"S",;													//Propri		
		"S"})													//Pyme

		
/*********************************************************************************************
Grava as informacoes do array na tabela 
*********************************************************************************************/
dbSelectArea("SX6")
SX6->(DbSetOrder(1))	

For i:= 1 To Len(aSx6)
	If !DbSeek(cCodFilial+aSx6[i,2]) .and. !DbSeek(aSx6[i,1]+aSx6[i,2])
		RecLock("SX6",.T.)
		For j:=1 To Len(aSx6[i])
			If FieldPos(aEstrut[j])>0
				FieldPut(FieldPos(aEstrut[j]),aSx6[i,j])
			EndIf
		Next j 
		MsUnLock()
		
		cParmsNew += space(15)+ aSx6[i,2] +CHR(13)+CHR(10)
	Else
		cParmsExist += space(15)+ aSx6[i,2] +CHR(13)+CHR(10)
	Endif
Next i

If !empty(cParmsNew)
	cTexto += space(10)+ STR0124 + CHR(13)+CHR(10) + cParmsNew + CHR(13)+CHR(10)
EndIf

If !empty(cParmsExist)
	cTexto += space(10)+ STR0125 + CHR(13)+CHR(10) + cParmsExist + CHR(13)+CHR(10)
EndIf

aRet := {"0",cTexto}

Return aRet


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AtuSXB   � Autor � Alberto Deviciente   � Data � 14/Nov/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao do SXB - Consultas     ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function AtuSXB()
Local cTexto 	:= ""
Local aSXB      := {}						//Array que armazenara os indices
Local aEstrut   := {}				        //Array com a estrutura da tabela SXB
Local i         := 0 						//Contador para laco
Local j         := 0 						//Contador para laco
Local cSXBNew 	:= ""
Local cSXBExist := ""

//�������������������������Ŀ
//�Define estrutura do array�
//���������������������������
aEstrut:= {"XB_ALIAS", "XB_TIPO", "XB_SEQ", "XB_COLUNA", "XB_DESCRI", "XB_DESCSPA", "XB_DESCENG", "XB_CONTEM", "XB_WCONTEM"}

//���������������������������������������������������Ŀ
//�Define os novos conteudos dos filtros das consultas�
//�����������������������������������������������������
aAdd(aSXB,{	"SE1ALU","1","01","RE",;	// ALIAS, TIPO, SEQ, COLUNA
			STR0126,;					// DESCRI
			STR0126,;					// DESCRI
			STR0126,;					// DESCRI
			"SE1", "" })				// CONTEM, WCONTEM
aAdd(aSXB,{	"SE1ALU","2","01","01",;	// ALIAS, TIPO, SEQ, COLUNA
			"",;						// DESCRI
			"",;						// DESCRI
			"",;						// DESCRI
			"FI020SXBAl()", "" })		// CONTEM, WCONTEM
aAdd(aSXB,{	"SE1ALU","5","01","",;		// ALIAS, TIPO, SEQ, COLUNA
			"",;						// DESCRI
			"",;						// DESCRI
			"",;						// DESCRI
			"cRetRaAlu", "" })			// CONTEM, WCONTEM

dbSelectArea("SXB")
SXB->(dbSetOrder(1)) //XB_ALIAS+XB_TIPO+XB_SEQ+XB_COLUNA	
For i := 1 To Len(aSXB)
	If !DbSeek(PadR(aSXB[i,1],6)+aSXB[i,2]+aSXB[i,3]+aSXB[i,4])
		if !(aSXB[i,1] $ cSXBNew)
			cSXBNew += aSXB[i,1]+"/"
		endif
		RecLock("SXB",.T.)
	Else
		if !(aSXB[i,1] $ cSXBExist)
			cSXBExist += aSXB[i,1]+"/"
		endif
		RecLock("SXB",.F.)
	EndIf

	For j:=1 To Len(aSXB[i])
		If FieldPos(aEstrut[j]) > 0
			FieldPut(FieldPos(aEstrut[j]),aSXB[i,j])
		EndIf
	Next j
	MsUnLock()
Next i

If !empty(cSXBNew)
	cSXBNew := SubStr(cSXBNew,1,len(cSXBNew)-1)
	cTexto += space(10)+ STR0127 + cSXBNew + CHR(13)+CHR(10)
EndIf

If !empty(cSXBExist)
	cSXBExist := SubStr(cSXBExist,1,len(cSXBExist)-1)
	cTexto += space(10)+ STR0128 + cSXBExist + CHR(13)+CHR(10)
EndIf

aRet := {"0",cTexto}

Return aRet



/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �MyOpenSM0Ex� Autor � Alberto Deviciente   � Data �30/Out/09 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Rotina NF-e Educacional                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex() 
Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 To 20  
	dbUseArea(.T.,, "sigamat.emp", "SM0", .F., .F. ) 	
	If !Empty(Select("SM0" ) ) 
		lOpen := .T. 
		dbSetIndex("sigamat.ind") 
		Exit	
	EndIf
	//Sleep(500) 
Next nLoop

If !lOpen
	Aviso(STR0129, STR0130, { STR0131 }, 2 ) //"Nao foi possivel a abertura da tabela de empresas de forma exclusiva !"
EndIf

Return(lOpen)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UpdNFeCtrl�Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ativa ou desativa objetos na tela.                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Rotina NF-e Educacional                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UpdNFeCtrl(cProcess)
oSay1:LVISIBLECONTROL := .F.
oSay2:LVISIBLECONTROL := .F.
oReguaTot:LVISIBLECONTROL:=.F.
oReguaProc:LVISIBLECONTROL:=.F.
oMMProcess:LVISIBLECONTROL := .F.
oMMProcTot:LVISIBLECONTROL := .F.
oWizard:OBACK:LVISIBLE:=.F.
oWizard:OCANCEL:LVISIBLECONTROL:=.F.
oMemo:nHeight := 250
oMemo:Refresh()

if cProcess == "OK" //Processamento Conclu�do com Sucesso
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0132 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HBLUE/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
elseif cProcess == "ERRO" //Processamento Conclu�do com ERRO
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0133 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HRED/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
else //Processamento Cancelado
	TSay():New( 134/*<nRow>*/, 007/*<nCol>*/, {|| STR0134 }	/*<{cText}>*/, oWizard:oMPanel[3]/*[<oWnd>]*/, /*[<cPict>]*/, /*<oFont>*/, /*<.lCenter.>*/, /*<.lRight.>*/, /*<.lBorder.>*/, .T./*<.lPixel.>*/, CLR_HRED/*<nClrText>*/, /*<nClrBack>*/, 200/*<nWidth>*/, 08/*<nHeight>*/, /*<.design.>*/, /*<.update.>*/, /*<.lShaded.>*/, /*<.lBox.>*/, /*<.lRaised.>*/, /*<.lHtml.>*/ )
endif

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �UpdNFeLog �Autor  � Alberto Deviciente � Data � 08/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Grava o arquivo de LOG do processamento.                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Rotina NF-e Educacional                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function UpdNFeLog(cArquivo, cTexto)
Local nHdl 		:= 0

If !File(cArquivo)
	//Se nao encontrou o arquivo, cria
	nHdl := FCreate(cArquivo)
Else
	//Encontrou o arquivo
	nHdl := FOpen(cArquivo, FO_READWRITE)
Endif
FSeek(nHdl,0,FS_END)
//cTexto += Chr(13)+Chr(10)
FWrite(nHdl, cTexto, Len(cTexto))
FClose(nHdl)

Return


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Upd40MkAll�Autor  � Alberto Deviciente � Data � 07/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Efetua a marcacao de todas as Empresas/Filiais.            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Classis Net (RM Sistemas)         ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdMkAll 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function Upd40MkAll()
Local nCont := 0

aEval( aEmpsFils, { |x| nCont += iif( x[1] , 1, 0 ) } )

if nCont == len(aEmpsFils)
	aEval( aEmpsFils, {|x| x[1] := .F.} )
else
	aEval( aEmpsFils, {|x| x[1] := .T.} )
endif

oListSM0:Refresh()

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �Upd40Mark �Autor  � Alberto Deviciente � Data � 06/Abr/09   ���
�������������������������������������������������������������������������͹��
���Desc.     � Efetua a marcacao da Empresa/Filial selecionada.           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Integracao Protheus x RM Classis Net (RM Sistemas)         ���
�������������������������������������������������������������������������͹��
���OBS:	  	 �Funcao antiga chamava-se _UpdMark 						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function Upd40Mark()

if !empty(aEmpsFils[oListSM0:nAt,2])

	//�����������������������������������������������������������������������������������������������������������Ŀ
	//�Verifica se nao eh uma empresa que esta utilizando o conceito de "Gestao de Empresas - Somente Vesrsao 11")�
	//�������������������������������������������������������������������������������������������������������������
	If len(alltrim(aEmpsFils[oListSM0:nAt,4])) <= 2	
		aEmpsFils[oListSM0:nAt,1] := !aEmpsFils[oListSM0:nAt,1]
		oListSM0:Refresh()
	Else
		Aviso(STR0045,STR0148,{STR0131}) //"A empresa/filial selecionada possui o conceito de Gest�o de Empresas e n�o pode ser utilizada na funcionalidade de integra��o Microsiga Protheus x TOTVS Educacional"
	EndIf
Endif


Return