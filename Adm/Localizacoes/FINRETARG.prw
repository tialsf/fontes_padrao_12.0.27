#include "protheus.CH"
#include "FINA085A.CH"

Static lFWCodFil := ExistFunc("FWCodFil")
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FINRETARG � Autor �  Bruno Schmidt     � Data �  14/08/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � Calculo Retencoes                        				  ���
�������������������������������������������������������������������������ͺ��
��� ArgRetIVA   - Calculo de Ret de IVA para NF               			  ���
��� ARGRetIV2   - Calculo de Ret de IVA para NCP               		      ���
��� ARGRetIB    - Calculo de Ret de IIB para NF                		      ���
��� ARGRetIB2   - Calculo de Ret de IIB para NCP               		      ���
��� ARGRetSUSS  - Calculo de Ret de SUSS para NF               		      ���
��� ARGRetSU2   - Calculo de Ret de SUSS para NCP              		      ���
��� ARGRetSLI   - Calculo de Ret de SLI para NF                		      ���
��� ARGRetSL2   - Calculo de Ret de SLI para NCP               		      ���
��� ARGRetGN    - Calculo de Ret de Ganancias                  		      ���
��� ARGRetGNMnt - Calculo de Ganancia para Monotributista      		      ���
��� ARGCpr      - Calculo de Ret de CPR para NF                		      ���
��� ARGCpr2     - Calculo de Ret de CPR para NCP               		      ���
��� ARGRetCmr   - Calculo de Ret de CMR para NF                		      ���
��� ARGRetCmr2  - Calculo de Ret de CMR para NCP               		      ���
��� ARGSegF1    - Calculo de Ret de Seguridad e Hig para NF    		      ���
��� ARGSegF2    - Calculo de Ret de Seguridad e Hig para NCP   		      ���
��� ARGRetIM    - Calculo de Ret de Iva Monotributista para NCP  		  ���
��� ARGRetIM2   - Calculo de Ret de Iva Monotributista para NCP   		  ���
��������������������������������������������������������������������������̱�
���PROGRAMADOR � DATA   � BOPS   �  MOTIVO DA ALTERACAO                   ���
��������������������������������������������������������������������������̱�
���Laura Medina�15/02/17�MMI-121 �Se realiza la replica del llamado TUCD44���
���            �        �        �para considerar en la generacion de la  ���
���            �        �        �OP el pago a trav�s de CBU.             ���
���Laura Medina�27/02/17�MMI-4160�Adecuacion para que haga retencion IVA  ���
���            �        �        �a NF/NCr�dito de diferentes sucursales. ���
���            �        �        �Se incluyo la solucion de issue MMI-4147���
���            �        �        �para la validacion del BCO que impacta  ���
���            �        �        �en la generacion de la OP.              ���
���Raul Ortiz  �28/02/17�MMI-4162�Adecuacion para que haga retencion IVA  ���
���            �        �        �Sobre el valor del documento para       ���
���            �        �        �empresas de Limpieza                    ���
���Laura Medina�01/03/17�MMI-4184�Realizar correctamente el calculo IIBB  ���
���            �        �        �cuando existe mas de un registro en la  ���
���            �        �        �SFF para mismo CFO y diferente tipo de  ���
���            �        �        �Contribuyente.                          ���
���Laura Medina�03/03/17�MMI-4166�En la funcion de Ret. de IVA, se cambia ���
���            �        �        �la validacion cAcmIVA <> "" por         ���
���            �        �        �!Empty(cAcmIVA).                        ���
���Raul Ortiz  �03/03/17�MMI-4148�Se incorpora funcionalidad para la RG   ���
���            �        �        �2854-11                                 ���
���Laura Medina�06/03/17�MMI-4168�Adecuacion para que haga retencion IIBB ���
���            �        �        �a NF/NCr�dito de diferentes sucursales. ���
���Raul Ortiz  �09/03/17�MMI-238 �Se guardar correctamente el importe     ���
���            �        �        �de deducci�n para condominios ganancias ���
���Raul Ortiz  �15/03/17�MMI-4182�Adecuaci�n para calcular correctamente  ���
���            �        �        �SUSS al ser el calculo por cuotas       ���
���Laura Medina�30/03/17�MMI-4533�Adecuaciones para las r�plicas de los   ���
���            �        �        �llamados: TTXJZP,TUVZY4,TUSJW3 y TUXRWJ.���
���Raul Ortiz  �23/03/17�MMI-4417�Adecuaci�n para calcular correctamente  ���
���            �        �        �IIBB a no Inscriptos                    ���
���Raul Ortiz  �28/03/17|MMI-4546�Se considera el calculo de m�nimos IIBB ���
���            �        �        �por Ordend de Pago y no por comprobante ���
���Raul Ortiz  �30/03/17|MMI-4938�Se consideran las retenciones generadas ���
���            �        �        �por las ordenes de Pago Previas         ���
���Laura Medina�19/05/17�MMI-5084�RG 032/2016 para SF, Tipo de Mi. (CCO_  ���
���            �        �        �TPMINR)3-Base Imponible M�nima+Impuesto,���
���            �        �        �se modifica la regla para tomar los m�- ���
���            �        �        �nimos con la nueva opci�n (3)...        ���
���Roberto Glez�19/05/17�MMI-5717�Correcci�n en calculo de retenci�n de   ���
���            �        �        �ganancias en un PA.                     ���
���Roberto Glez�02/06/17�MMI-5806�Modificaci�n de validaci�n para prov si ���
���            �        �        �es agente de retenci�n de IVA y evitar  ���
���            �        �        �la exlusi�n del calculo de otro proceso.���
���Laura Medina�29/05/17�MMI-5197�Se da el tratamiento  si es un PA, obte-���
���            �        �        �niendo de manera correcta la Retencion y���
���            �        �        �calculando la Ret. Ganancias para alma- ���
���            �        �        �cenar en los arreglos.                  ���
���Roberto Glez�08/06/17�MMI-5901�Cuando una NCP cuenta con m�s de un �tem���
���            �        �        �con la misma TES, considerar la al�cuota���
���            �        �        �correcta para el c�lculo.               ���
���Raul Ortiz  �08/06/17�MMI-5667� Modificaciones para mostrar las Ret.   ���
���            �        �        �en pantalla de las Ordenes de Pago      ���
���Laura Medina�15/06/17�MMI-5343�Adecuacion para que haga retencion IVA  ���
���            �        �        �correctamente y para que en Ret. de NCP ���
���            �        �        �considere la configuracion del par�metro���
���            �        �        �MV_AGENTE.                              ���
���Roberto Glez�06/10/17�DMICNS  �Considerar el codigo de impuesto en el  ���
���            �        �-292    �calculo de retencion de ganancias.      ���
���Jose Glez   �13/10/17�TSSERMI01�Se agrega la variable aImpInf a la     ���
���            �        �-189     �funcion ARGRetSLI                      ���
���Roberto Glz �24/07/17�DMICNS- �Para calculo de SUSS para NF y NCP,     ���
���            �        �108     �considerar las especificaciones de la RG���
���            �        �        �3983 para tomar el valor de F1/F2_VALSUS���
���            �        �        �en la orden de pago del documento.      ���
���Raul Ortiz M�20/12/17�DMICNS- �Cambios en calculo de retenciones de IB ���
���            �        �673     �en las 2 funciones para tomar en cuenta ���
���            �        �        �los limites de calculo por orden de pago���
���            �        �        �Argentina                               ���
���Raul Ortiz  �12/03/18�DMICNS- �Cambios para ganancias en condominios   ���
���            �        �1060    �conceptos diferentes a 04- Argentina    ���
���Marcos A    �27/03/18�DMICNS- �Se toma en cuenta tipo de m�nimo para   ���
���            �        � 1062   �retenciones de IIBB en base a la opcion ���
���            �        �        �3-Base Imponible+Impuestos (CCO_TPMINR).���
���            �        �        �Pais: Argentina (BA y CO).              ���
���Marco A. Glz�03/12/18�DMICNS- �Replica de issue DMICNS-4532 (11.8), que���
���            �        � 4603   �soluciona el calculo correcto de reten- ���
���            �        �        �ciones, cuando se ha superado el monto  ���
���            �        �        �minimo definido en el pago.             ���
���GSantacruz  �15/01/19�DMINA-  �ARGSegF2() - Ret Seguridad e Higiene NCP���
���            �        �    5674�Proceso correcto de aSFF, aCF y signo   ���
���            �        �        �de % FE_PORCRET.                        ���
���GSA/ARL     �07/03/19�DMINA-  �ARGRetIB()/ARGRetIB2() - Retenciones IB ���
���            �        �    5667�Procesar si hay mas de un item y es     ���
���            �        �        �proveedor no inscripto.                 ���
���Alf. Medrano�28/03/19�DMINA-  �se modifica fun ARGRetGN() se utiliza la���
���            �        �    5675�variable lRegOP que indica si restar�   ���
���            �        �        �valor del importe para obtener el impues���
���            �        �        �to de retenci�n                         ���
���Oscar G.    �04/04/19�DMINA-  �Se modifica fun ARGRetIB() inicia var.  ���
���            �        �    5708�nPercTot, se validan vigencias de regi- ���
���            �        �        �tros en SFH.                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������

�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ArgRetIVA � Autor �   Bruno Schmidt    � Data �  14/08/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � ArgRetIVA   - Calculo de Ret de IVA para NF                ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ArgRetIVA(cAgente,nSigno,nSaldo,lPa,cCF,nValor,nProp,cSerieNF,nA,aPagAux,naPagar,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local oDlg4
Local aConIva  	 := {}
Local aSFEIVA  	 := {}
Local aDocAcm  	 := {}
Local aConfIva 	 := {}
Local aAreaSFE 	 := {}
Local aSalRet 	 := {}
Local aFIva	:= FilImpto({"3"},{"I"},{},{}) // Retorna todos los impuestos con IVA de SFB
Local lExisteSFF := .T.
Local lRetSerie	 :=	 .F.
Local lCalRet	 := .F.
Local lCalcImp	 := .T.
Local lCalRep	 := .F.
Local lCalcVen	 := .F.
Local lCalrtexp	 := .F.
Local lRetIVA	 := .F.
Local lAcmIva	 := .F.
Local lCalcAcm	 := .F.
Local lRetSit 	 := .T.
Local lExiste	 := .F.
Local lSento 	 := .F.
Local lExiteSFH	 := .F.
Local lReten100	 := .F.
Local lCalcIva 	 := .T.
Local lCalcNew	 := GetNewPar("MV_GRETIVA","N") == "S"
Local lReSaIV	 	:= GetNewPar("MV_RETIVA","N") == "S"
Local cChaveSFE	 := ""
Local cAcmIVA	 := ""
Local cConc		 := ""
Local nPorIva	 := 0 
Local nPosIva	 := 0
Local nCount	 := 0
Local nPercRet	 := 0
Local nBase		 := 0
Local nI 		 := 1
Local nValorBr	 := 0
Local nTotBase	 := 0
Local nTotImp	 := 0
Local nRetIva 	 := 0
Local nRetTotal	 := 0
Local nTotRet	 := 0
Local nTotRetSFE := 0
Local nVlrTot 	 := 0
Local nX		 := 1
Local nReduc 	 := 0
Local nValImps	 := 0
Local nValIVA 	 := 0
Local nPosDoc 	 := 0
Local nTotIva 	 := 0
Local nC		 := 0
Local nPorRet	 := 0
Local nImpRetIva := 0
Local nImporte 	 := 0
Local nVRetIVA := 0
Local nSRetIVA := 0
Local nSRIva	:= 0
Local nSVIva	:= 0
Local nSalVal	:= 0
Local nRecSF1	 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.
Local aRetPreO	:= {}
Local nPosPreO	:= 0
Local cCFOIVA 	:= ""
Local cCFOVIVA 	:= ""
Local nValBas	:= 0
Local nRedAliq	:= 0 
Local lCalcCbu:=.F.

If type("cFornece")=="U"
	 cFornece	:=SE2->E2_FORNECE
	 cLoja		:=SE2->E2_LOJA
EndIf

DEFAULT lPa		 :=	.F.
DEFAULT cCF 	 := ""
DEFAULT nValor 	 := 0
DEFAULT nSigno	 := 1
DEFAULT naPagar	 := 0
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lPa //.and. lReSaIV
	Return aSFEIVA
Endif

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIVA := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"I",nSaldo,dEmissao)
EndIf

//+---------------------------------------------------------------------+
//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten玟o.?
//+---------------------------------------------------------------------+
aArea:=GetArea()
dbSelectArea("SF1")
dbSetOrder(1)

If ValType(lMsFil) == "U"
	lMsFil :=	SE2->( ColumnPos('E2_MSFIL') > 0 ) .And. ; 
				SD1->( ColumnPos('D1_MSFIL') > 0 ) .And. ;
				SD2->( ColumnPos('D2_MSFIL') > 0 ) .And. ;
			  	SF1->( ColumnPos('F1_MSFIL') > 0 ) .And. ;
				SF2->( ColumnPos('F2_MSFIL') > 0 ) 
EndIf

If lMsFil
	nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
	SF1->(dbGoTo(nRecSF1))
Else
	nRecSF1 := FINBuscaNF(xFilial("SF1", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
	SF1->(dbGoTo(nRecSF1))
EndIf

nVRetIVA	:=	Iif(lReSaIV, SF1->F1_RETIVA, 0)
nSRetIVA	:=	Iif(lReSaIV, SF1->F1_SALIVA, 0)
lCalRet := Iif(Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "M" .or. (Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "A" .and. cPaisLoc=="ARG"),.T.,.F.)
lCalcAcm := Subs(cAgente,2,1) == "N"
If(SF1->(ColumnPos("F1_TPNFEXP"))>0 .And.  SFF->(ColumnPos("FF_VLRLEX"))>0 .And. SFF->(ColumnPos("FF_ALIQLEX"))>0  .And.  SF1->F1_TPNFEXP=="1")  //1 - Exportador 2- Normal
	lCalrtexp:=.T.
EndIf

If ExistBlock("F0851IMP")
	lCalcImp:=ExecBlock("F0851IMP",.F.,.F.,{"IVA"})
EndIf

SA2->( dbSetOrder(1) )
If !lPa
	SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
Else
	SA2->( MsSeek(xFilial("SA2")+cFornece+cLoja) )
EndIf    
If ((SA2->(ColumnPos("A2_CBUINI"))>0 .and. SA2->(ColumnPos("A2_CBUFIM"))>0) .and. SA2->A2_CBUINI <= SE2->E2_EMISSAO .and. SA2->A2_CBUFIM >= SE2->E2_EMISSAO .and.  Subs(SF1->F1_SERIE,1,1) =="A") .or. Subs(SF1->F1_SERIE,1,1) =="M"
	lCalcCbu:=.T.
EndIf   
If (Subs(cAgente,2,1) == "S"  .Or. lCalRet .Or. lCalcAcm .or. lCalcCbu) .And. lCalcImp  .And.  lRetSit
	SA2->( dbSetOrder(1) )
	If !lPa
		If !Empty(xFilial("SA2")) .And. lMsFil .And. xFilial("SF1") == xFilial("SA2")
			SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Else
			SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Endif
	Else
		SA2->( MsSeek(xFilial("SA2")+cFornece+cLoja))
	EndIf
	
	//Si el proveedor es Agente de Ret., independientemente si la emp es o no agente, no se debe retener
	If SA2->A2_AGENRET == "S" .And. Alltrim(PadR(SF1->F1_SERIE,nTamSer)) <> "M"
		Return aSFEIVA
	Endif

	lCalRep := Iif(SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("2/4"),.T.,.F.)
	If SA2->A2_AGENRET == "N"   .Or. lCalRet

		If !lPA //Se estou em um PA
			While Alltrim(SF1->F1_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
				SF1->(DbSkip())
				Loop
			Enddo
			
			If AllTrim(SF1->F1_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
				Iif(lMsFil,SF1->F1_MSFIL,xFilial("SF1", SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
				SF1->F1_FILIAL+SF1->F1_DOC+PadR(SF1->F1_SERIE,nTamSer)+SF1->F1_FORNECE+SF1->F1_LOJA

				//?Se esta definida uma retencao para uma serie especial, verifico?
				//?antes de procurar no SD1.(Serie 'M' foi o primeiro caso).      ?

				SFF->(DbSetOrder(3))

				If SFF->(MsSeek(xFilial("SFF")+"IVR"+PadR(SF1->F1_SERIE,nTamSer)+"SER")) .And.  !lCalrtexp .And. !lCalcAcm

					SD1->(DbSetOrder(1))
	              	If lMsFil
						SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
					Else
						SD1->(MsSeek(xFilial("SD1", SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
					EndIf
					If SD1->(Found())
						Do while Iif(lMsFil,SF1->F1_MSFIL,xFilial("SD1", SE2->E2_FILORIG))==SD1->D1_FILIAL.And.SF1->F1_DOC==SD1->D1_DOC.AND.;
							SF1->F1_SERIE==SD1->D1_SERIE.AND.SF1->F1_FORNECE==SD1->D1_FORNECE;
							.AND.SF1->F1_LOJA==SD1->D1_LOJA.AND.!SD1->(EOF())

							aImpInf := TesImpInf(SD1->D1_TES)
							lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
							If !lTESNoExen
								SD1->(DbSkip())
								Loop
							EndIf

							For nI := 1 To Len(aImpInf)
								If "IV"$Trim(aImpInf[nI][01]) .And.  Trim(aImpInf[nI][01])<>"IVP"
									nPosIva:=ASCAN(aConIva,{|x| x[1]==SF1->F1_SERIE})
									If nPosIva<>0
										aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD1->(FieldGet(ColumnPos(aImpInf[nI][02]))))
										aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD1->(FieldGet(ColumnPos(aImpInf[nI][07]))))
									Else										
									   	nVlrTot := SD1->(FieldGet(ColumnPos(aImpInf[nI][02])))
										Aadd(aConIva,{SD1->D1_CF,nVlrTot,SD1->(FieldGet(ColumnPos(aImpInf[nI][07]))), (SD1->(FieldGet(ColumnPos(aImpInf[nI][10]))) /100)})
									Endif

									If SD1->(FieldGet(ColumnPos(aImpInf[nI][07]))) < SFF->FF_IMPORTE
										aConIVA[1][2]:= ((SFF->FF_ALIQ/100 ) * (SD1->(FieldGet(ColumnPos(aImpInf[nI][07])))))
									Endif
								Endif
							Next
							SD1->(DbSkip())
						Enddo
					Endif

					lRetSerie	:=	.T.

				Else
				
					//?Obter o Valor do Imposto e Base baseando se no rateio do valor ?
					//?do titulo pelo total da Nota Fiscal.                           ?
					
					SD1->(DbSetOrder(1))
					If lMsFil
						SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
					Else
						SD1->(MsSeek(xFilial("SD1", SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
					EndIf
					If SD1->(Found())
						Do while Iif(lMsFil,SF1->F1_MSFIL,xFilial("SD1", SE2->E2_FILORIG))==SD1->D1_FILIAL.And.SF1->F1_DOC==SD1->D1_DOC.AND.;
							SF1->F1_SERIE==SD1->D1_SERIE.AND.SF1->F1_FORNECE==SD1->D1_FORNECE;
							.AND.SF1->F1_LOJA==SD1->D1_LOJA.AND.!SD1->(EOF())

							If AllTrim(SD1->D1_ESPECIE)<>AllTrim(SF1->F1_ESPECIE)
								SD1->(DbSkip())
								Loop
							Endif
							
							aImpInf := TesImpInf(SD1->D1_TES)
							lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
							If !lTESNoExen
								SD1->(DbSkip())
								Loop
							EndIf
							
							SB1->(DbSetOrder(1))
							If !Empty(SD1->D1_CF)
							
								nRateio := 1

								nPosIva:=ASCAN(aConIva,{|x| x[1]==SD1->D1_CF})
								If lCalcNew  .Or.  lCalrtexp // Calcula IVA metodo Novo
									If nPosIva<>0
										If Len(aConIVA[nPosIVA]) >= 6
											cAcmIVA := aConIva[nPosIva][6]
										EndIF 
										If (SB1->B1_RG2849 <> 0 .Or. cAcmIVA == "2")
											nValor := Round(xmoeda(SD1->D1_TOTAL-SD1->D1_VALDESC,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
											aConIva[nPosIva][2]+= ( (aConIva[nPosIva][4] * nValor )  - (SD1->D1_IVAFRET+SD1->D1_IVAGAST) ) * nRateio
											aConIva[nPosIva][3]+= nValor - (SD1->D1_BaIVAFR+SD1->D1_BaIVAGA) * nRateio
										Else
											nValor :=  ValImpto(aFIva,SD1->D1_TES)
											aConIva[nPosIva][2]:=aConIva[nPosIva][2] + ( (aConIva[nPosIva][4] * nValor )  - (SD1->D1_IVAFRET+SD1->D1_IVAGAST) ) * nRateio
											aConIva[nPosIva][3]:=aConIva[nPosIva][3] + nValor - (SD1->D1_BaIVAFR+SD1->D1_BaIVAGA) * nRateio
										EndIf
									Else
										cAcmIVA := ""
										If lCalRep .or. lCalcVen
											aImpInf := TesImpInf(SD1->D1_TES)
											For nI := 1 To Len(aImpInf)
												If  "IV"$Trim(aImpInf[nI][01]) .And.  Trim(aImpInf[nI][01])<>"IVP"

													Aadd(aConIva,{SD1->D1_CF,SD1->(FieldGet(ColumnPos(aImpInf[nI][02]))),SD1->(FieldGet(ColumnPos(aImpInf[nI][07]))), (SD1->(FieldGet(ColumnPos(aImpInf[nI][10]))) /100),cAcmIVA})
												EndIf
											Next
										Else
											SFF->(DbSetOrder(5))
											SFF->(DbGotop())
											If SFF->(MsSeek(xFilial("SFF")+"IVR"))
												nPerc:=0
												lAchou:=.F.
												While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
													If (Alltrim(SFF->FF_SERIENF)== Alltrim(PadR(SD1->D1_SERIE,nTamSer)) .And. SD1->D1_CF==  SFF->FF_CFO_C )
														lExistSFF:=.T.
														If GetNewPar("MV_SEGMEN","") $ "1|2|4"
															If   SA2->(ColumnPos("A2_TIPROVM"))>0 .AND.  ((SA2->A2_TIPROVM $ "1|2|3" .AND. SFF->FF_REGIST=="1") .OR. ( SA2->A2_TIPROVM $ "4" .AND. SFF->FF_REGIST=="2" ))
																lExistSFF:=.T.
															Else
																lExistSFF:=.F.
															EndIf
														Endif
														If lExistSFF
															lAchou:=.T.
															nPerc := SFF->FF_ALIQ
															cCFOIVA	:= SFF->FF_CFO_C
															cCFOVIVA := SFF->FF_CFO_V
															nRedAliq:= SFF->FF_REDALIQ 
															If SFF->(ColumnPos("FF_TPLIM")) > 0
																cAcmIVA := SFF->FF_TPLIM
																nLimite := SFF->FF_LIMITE
															EndIf  
															If SFF->(ColumnPos("FF_IMPORTE")) > 0
																nImporte := SFF->FF_IMPORTE
															EndIf															
														EndIf														
													EndIf
													SFF->(DbSkip())
												EndDO

												If cAcmIva != "2" .And. Subs(cAgente,2,1) == "N" .And. SA2->A2_AGENRET == "N" .And. Alltrim(PadR(SF1->F1_SERIE,nTamSer)) <> "M" .And. !lCalcCbu
													Return aSFEIVA
												EndIf

												/************************************************
												Verifica o acumulado para o c醠culo do imposto
												************************************************/

												//caso seja branco ou n鉶 foi selecionado nenhum m閠odo de acumulo
												If Type("aRetIvAcm") <> "A"
													aRetIvAcm := Array(3)
												EndIf

												If !Empty(cAcmIVA) .And. cAcmIVA <> "0" .And. lCalcAcm
													aDadosIVA 	:= F850AcmIVA(cAcmIVA,SF1->F1_EMISSAO,SF1->F1_FORNECE,SF1->F1_LOJA,Iif(lMsFil,SF2->F2_MSFIL,""),cCFOIVA,cCFOVIVA)


													nBase := aDadosIVA[1] - aDadosIVA[2]

													If aDadosIVA[4]
														If nBase >= nLimite .Or. aDadosIVA[2]  > 0
															lRetIva := .T.
															lAcmIva := .T.
																
															For nX := 1 to Len(aDadosIVA[3])
																If Empty(aDadosIVA[3][nX][2])
																	aAdd(aDocAcm, {aDadosIVA[3][nX][3], aDadosIVA[3][nX][4], Iif(aDadosIVA[3][nX][1],.F.,.T.),aDadosIVA[3][nX][6],aDadosIVA[3][nX][5]})
																EndIf
																
																//Propor��o do valor dos t�tulos parcelados
																If aDadosIVA[3][nX][5] = SE2->(Recno())
																	nRateio := aDadosIVA[3][nX][6]/100	
																EndIf
																
															Next nX
															
															If Len(aDocAcm) > 0
																aAdd(aConfIVA,nPerc)
																
																aRetIvAcm[1] :=  aConfIVA
																
																If aRetIvAcm[2] == Nil
																	aRetIvAcm[2] := {}
																EndIf
																
																For nX := 1 to Len(aDocAcm)
																	If aDocAcm[nX][2] == "SF1"
																		nPosDoc := aScan(aRetIvAcm[2],{|x| x[1]==aDocAcm[nX][1] .And. x[5]==aDocAcm[nX][5] .And. x[2]=="SF1"})
																	Else
																		nPosDoc := aScan(aRetIvAcm[2],{|x| x[1]==aDocAcm[nX][1] .And. x[5]==aDocAcm[nX][5] .And. x[2]=="SF2"})
																	EndIf
																	If nPosDoc == 0
																		Aadd(aRetIvAcm[2],aDocAcm[nX])
																	EndIf
																Next nX
																
															EndIf
															
														EndIf
													Else
														lRetIva := .T.
													EndIf
												Else
													//Se n鉶 usa cumulatividade de IVA, calcula no m閠odo antigo
													lRetIva := .T.
												EndIf

												If  lCalrtexp .And.	lAchou
													If Alltrim(PadR(SD1->D1_SERIE,nTamSer)) == "B" .and. SA2->A2_TIPO == "I" 
														nPerc := POSICIONE("SFB",1,xFilial("SFC")  + POSICIONE("SFC", 2, xFilial("SFC") + SD1->D1_TES + "IV", "FC_IMPOSTO" ),"FB_ALIQ")
														nValorBr:=Round(xmoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) / (1+ (nPerc/100))
													Else
														nValorBr:=Round(xmoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
														nPerc:=Iif(nValorBr>SFF->FF_VLRLEX,SFF->FF_ALIQ,SFF->FF_ALIQLEX)
													EndIf
												EndIf
							
												If lRetIva
													If cPaisLoc == "ARG" .And. ( (SB1->(ColumnPos("B1_RG2849"))>0 .AND. SB1->B1_RG2849 <> 0) .Or. cAcmIVA == "2")
														nValor := Round(xmoeda(SD1->D1_TOTAL-SD1->D1_VALDESC,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
														Aadd(aConIva,{SD1->D1_CF,(((nPerc/100 ) * nValor )  - (SD1->D1_IVAFRET+SD1->D1_IVAGAST)),(nValor - (SD1->D1_BaIVAFR +SD1->D1_BaIVAGA)),(nPerc /100),nImporte,cAcmIVA})
														
													Else
														nValor :=  ValImpto(aFIva,SD1->D1_TES)
														nValBas :=  ValBasImp(aFIva,SD1->D1_TES)
														If lCalcCbu 
															Aadd(aConIva,{SD1->D1_CF,(((nPerc/100*(1-nRedAliq/100) ) * nValBas )  - (SD1->D1_IVAFRET+SD1->D1_IVAGAST)),(nValBas - (SD1->D1_BaIVAFR +SD1->D1_BaIVAGA)),(nPerc /100*(1-nRedAliq/100)),nImporte,cAcmIVA})														
														Else    
															Aadd(aConIva,{SD1->D1_CF,(((nPerc/100 ) * nValor )  - (SD1->D1_IVAFRET+SD1->D1_IVAGAST)),(nValor - (SD1->D1_BaIVAFR +SD1->D1_BaIVAGA)),(nPerc /100),nImporte,cAcmIVA})
														EndIf	
													EndIf													    
												EndIf
											EndIf
										EndIf
									EndIF						
								Endif								
							ElseIf SB1->(MsSeek(Iif(lMsFil .And. !Empty(xFilial("SB1")),SD1->D1_MSFIL,xFilial("SB1")) + SD1->D1_COD))
								nPosIva:=ASCAN(aConIva,{|x| x[1]==SB1->B1_CONCIVA})
								If nPosIva<>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD1->D1_VALIMP1-SD1->D1_IVAFRET+SD1->D1_IVAGAST)
							 		aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD1->D1_BASIMP1-SD1->D1_BaIVAFR+SD1->D1_BaIVAGA)
								Else
									Aadd(aConIva,{SB1->B1_CONCIVA,(SD1->D1_VALIMP1-SD1->D1_IVAFRET+SD1->D1_IVAGAST),(SD1->D1_BASIMP1-SD1->D1_BaIVAFR +SD1->D1_BaIVAGA),0})
								Endif
							ElseIf !lCalcAcm
								nPosIva:=ASCAN(aConIva,{|x| x[1]==SA2->A2_ACTRET})
								If nPosIva<>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD1->D1_VALIMP1)
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD1->D1_BASIMP1)
								Else
									Aadd(aConIva,{SA2->A2_ACTRET,SD1->D1_VALIMP1,SD1->D1_BASIMP1,0})
								Endif	
							Endif
							If SD1->D1_IVAFRET > 0
								MV_FRETLOC  := GetNewPar("MV_FRETLOC","IVA162GAN06")
								cConc   := Alltrim(Subs( MV_FRETLOC, 4,At("G",MV_FRETLOC)-4))
								nPosIva := ASCAN(aConIva,{|x| x[1]==cConc})
								If nPosIva<>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+ SD1->D1_IVAFRET
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+ SD1->D1_BaIVAFR
								Else
									Aadd(aConIva,{cConc,SD1->D1_IVAFRET,SD1->D1_BaIVAFR,0})
								Endif
							EndIf
							If SD1->D1_IVAGAST > 0
								MV_GASTLOC  := GetNewPar("MV_GASTLOC","IVA112GAN06")
								cConc   := Alltrim(Subs( MV_GASTLOC, 4,At("G",MV_GASTLOC)-4))
								nPosIva := ASCAN(aConIva,{|x| x[1]==cConc})
								If nPosIva<>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+ (SD1->D1_IVAGAST)
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+ (SD1->D1_BaIVAGA)
								Else
									Aadd(aConIva,{cConc,SD1->D1_IVAGAST,SD1->D1_BaIVAGA,0})
								Endif
							EndIf
							SD1->(DbSkip())
						EndDo
					Endif
				Endif
				
				//Verificar a porcetagem correteado do Iva conforme informada nas datas

				If SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. SA2->(ColumnPos("A2_IVPCCOB")) > 0 .and. (dDataBase >= SA2->A2_IVPDCOB .and. dDataBase <= SA2->A2_IVPCCOB)
					nPorIva := SA2->A2_PORIVA
				ElseIf SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. SA2->(ColumnPos("A2_IVPCCOB")) > 0 .and. (Empty(SA2->A2_IVPDCOB) .and. dDataBase <= SA2->A2_IVPCCOB)
					nPorIva := SA2->A2_PORIVA
				ElseIf SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. SA2->(ColumnPos("A2_IVPCCOB")) > 0 .and. (Empty(SA2->A2_IVPCCOB) .and. dDataBase >= SA2->A2_IVPDCOB)
					nPorIva := SA2->A2_PORIVA
				ElseIf !SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. !SA2->(ColumnPos("A2_IVPCCOB")) > 0 
					nPorIva := SA2->A2_PORIVA
				Else
					nPorIva := 100
				EndIf
				
				// Exenciones 
				SFH->(dbSetOrder(1))
				SFH->(dbGoTop())
				If SFH->(MsSeek(xFilial()+SA2->A2_COD+SA2->A2_LOJA+"IVR")) .and. SFH->(ColumnPos("FH_PERCENT"))>0
					If  (SFH->FH_ISENTO == "S" .OR. SFH->FH_PERCENT == 100)
						nPorIva := 0
					ElseIf A085aVigSFH() 
						If SFH->FH_ISENTO == "S"   .OR. SFH->FH_PERCENT == 100
							nPorIva := 0
						Else
							nPorIva := SFH->FH_PERCENT
						EndIf
					Endif 
				Endif 
				
				//?Gravar Retenciones.
				
				For nCount:=1  to Len(aConIva)
					aConIva[nCount][2]   := Round(xMoeda(aConIva[nCount][2],SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
					aConIva[nCount][3]   := Round(xMoeda(aConIva[nCount][3],SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))

					AAdd(aSFEIVA,array(12))
					aSFEIVA[Len(aSFEIVA)][1] := SF1->F1_DOC         									//FE_NFISCAL
					aSFEIVA[Len(aSFEIVA)][2] := SF1->F1_SERIE       									//FE_SERIE
					If lReSaIV .and. !lCalcCbu 
						aSFEIVA[Len(aSFEIVA)][3] := IIf (Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "B" .and. SA2->A2_TIPO == "I",(aConIva[nCount][3]*nProp)*nSigno+(aConIva[nCount][2]*nProp)*nSigno,(aConIva[nCount][3])*nSigno)	//FE_VALBASE
						aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2])*nSigno	//FE_VALIMP					
					Else 
						aSFEIVA[Len(aSFEIVA)][3] := IIf (Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "B" .and. SA2->A2_TIPO == "I" .and. cPaisLoc == "ARG",(aConIva[nCount][3]*nProp)*nSigno + (aConIva[nCount][2]*nProp)*nSigno, (aConIva[nCount][3]*nProp)*nSigno	)						//FE_VALBASE
						aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2]*nProp)*nSigno						//FE_VALIMP
					Endif 

					aSFEIVA[Len(aSFEIVA)][5] := Iif(Alltrim(SF1->F1_SERIE)=="M", 100, nPorIva)		//FE_PORCRET
					aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]) *(Iif(Alltrim(SF1->F1_SERIE)=="M", 1, Iif(lCalRep .Or. lCalcVen, 1, nPorIva/100)))
					aSFEIVA[Len(aSFEIVA)][9] := aConIva[nCount][1] 										// Gravar CFOP da opera玢o
					aSFEIVA[Len(aSFEIVA)][10]:= aConIva[nCount][4]*100 									// Gravar A PORCENTAGEM DA ALIQUOTA
					aSFEIVA[Len(aSFEIVA)][11]:= " "
					aSFEIVA[Len(aSFEIVA)][12] := IIf(lReSaIV,aSFEIVA[Len(aSFEIVA)][4],0) 
					
					If LEN(aConIva[1])==4 	//pROVISORIO, PARA QUE NO DE ERROR EN BASES DESACTUALIZADAS						
						If !lRetSerie
							lExiste:=.F.
							SFF->(DbSetOrder(5))
							SFF->(MsSeek(xFilial("SFF")+"IVR"+aConIva[nCount][1]))
							While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lExiste
								If Alltrim(PadR(aSFEIVA[Len(aSFEIVA)][2],nTamSer)) == Alltrim(SFF->FF_SERIENF)
									lExiste:=.T.
								Else
									SFF->(DbSkip())
								EndIf
							EndDO
						EndIf
						
						If SFF->FF_IMPOSTO == "IVR"
							If !Empty(SFF->FF_CFO)
								aSFEIVA[Len(aSFEIVA)][11] := SFF->FF_CFO // Gravar CFOP da opera堙埽o de compra (para o caso de NCP)
							Else
								MsgAlert(STR0256)
								aSFEIVA[Len(aSFEIVA)][11]:= " "
							EndIf
						EndIf
						
						If lExiste
							If SA2->(ColumnPos("A2_IVPCCOB")) > 0

								nPercRet:=SFF->FF_ALIQ/100

								If	lCalRep .or. lCalcVen
									nPercRet:= 1
								EndIf
								If lCalcNew
									If lCalRep .or. lCalcVen
										aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4])
									Else

										//矯醠culo del % de Exenci髇 del IVA									   ?
										
										///Se verifica si la fecha de generaci髇 de la OP sea >= A2_IVPDCOB Y <= A2_IVPCCOB
										IF SA2->(ColumnPos("A2_IVPDCOB")) > 0 .AND. (ddatabase >= SA2->A2_IVPDCOB .and. ddatabase <= SA2->A2_IVPCCOB)
											// Se descuenta el porcentaje de A2_PORIVA
											aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][6]//aSFEIVA[Len(aSFEIVA)][4]- aSFEIVA[Len(aSFEIVA)][6]
										Else
											// Se realiza el c醠culo normal
											aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
										End IF																			
										
									EndIf
								ElseIf cPaisLoc<>"ARG" .And. Dtos(SA2->A2_IVRVCOB) < Dtos(dDataBase)
									MsgAlert(OemToAnsi(STR0146)+SA2->A2_COD+OemToAnsi(STR0147)) //	"Ha vencido el perido de observacion del proveedor  "+SA2->A2_COD+". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
									//Zera o array das retencoes de IVA...
									aSFEIVA := {}
									//Sai do loop...
									Exit
								Else									
									///Se verifica si la fecha de generaci髇 de la OP sea >= A2_IVPDCOB Y <= A2_IVPCCOB
									IF SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. (SA2-> A2_IVPDCOB <= ddatabase .and. SA2->A2_IVPCCOB >= ddatabase)
										// Se descuenta el porcentaje de A2_PORIVA
										aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][6]//aSFEIVA[Len(aSFEIVA)][4]- aSFEIVA[Len(aSFEIVA)][6]
									Else
										// Se realiza el c醠culo normal
										aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
									End IF
								EndIf
							Else
								nPercRet:=SFF->FF_ALIQ/100

								If  lCalRep .or. lCalcVen
									nPercRet:= 1
								EndIf
								If lCalcNew
									If lCalRep .or. lCalcVen
										aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4])
									Else
										aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]) *(SA2->A2_PORIVA/100)
									EndIf

								Else
									aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]*(nPercRet))*(Iif(Alltrim(PadR(SF1->F1_SERIE,nTamSer))=="M" .or. (Alltrim(PadR(SF1->F1_SERIE,nTamSer))=="A" .and. cPaisLoc == "ARG"),100,SA2->A2_PORIVA)/100)
								EndIf
							EndIf

							nTotRet += aSFEIVA[Len(aSFEIVA)][6]
						ELSE
							DEFINE MSDIALOG oDlg4 FROM 65,0 To 218,366 Title OemToAnsi(STR0074) Pixel //"Inconsistencia"
							@ 2,3 To 51,180 Pixel of oDlg4
							//"La actividad ", " de IVA no esta registrada en la Tabla SFF"
							//"por lo tanto no se generara retenci de IVA. Si desea Continuar con la "
							//"Orden de Pago acepte, sino cancele. "
							@ 10,004 SAY OemToAnsi(STR0050)+ Alltrim(aConIva[nCount][1]) +OemToAnsi(STR0051) PIXEL Of oDlg4
							@ 23,004 SAY OemToAnsi(STR0052)	PIXEL Of oDlg4
							@ 36,004 SAY OemToAnsi(STR0053) 	PIXEL	Of oDlg4
							DEFINE SBUTTON FROM 57,064  Type 1 Action (lRetOk	:=	.T.,oDlg4:End())    Pixel ENABLE Of oDlg4
							DEFINE SBUTTON FROM 57,104  Type 2 Action (lRetOk	:=	.F.,oDlg4:End())  Pixel ENABLE Of oDlg4
							Activate Dialog oDlg4 CENTERED
							aSFEIVA[Len(aSFEIVA)][6] := 0
						Endif
					Else
						lExiste := .F.
						If !lRetSerie
							SFF->(DbSetOrder(5))
							SFF->(MsSeek(xFilial("SFF")+"IVR"+aConIva[nCount][1]))
							While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lExiste
								If Alltrim(aSFEIVA[Len(aSFEIVA)][2]) == Alltrim(SFF->FF_SERIENF)
									lExiste:=.T.
								Else
									SFF->(DbSkip())
								EndIf
							EndDO
						EndIf
						lExiste := .F.

						If SFF->FF_IMPOSTO == "IVR"
							If !Empty(SFF->FF_CFO)
								aSFEIVA[Len(aSFEIVA)][11] := SFF->FF_CFO // Gravar CFOP da opera堙埽o de compra (para o caso de NCP)
							Else
								MsgAlert(STR0256)
								aSFEIVA[Len(aSFEIVA)][11]:= " " 
							EndIf
						EndIf
					
						nTotRet += aSFEIVA[Len(aSFEIVA)][6]
					Endif	
									
					//?Generar Titulo de Impuesto no Contas a Pagar. 					
					If  lCalcNew
						aSFEIVA[Len(aSFEIVA)][7] := nSaldo
					Else
						aSFEIVA[Len(aSFEIVA)][7] := SE2->E2_VALOR
					EndIf
					aSFEIVA[Len(aSFEIVA)][8] := SE2->E2_EMISSAO

					If lReSaIV .and. ((nSaldo < SE2->E2_VALOR) .or. (naPagar > 0 .or. nVRetIVA > 0))  //.and. naPagar <> nSaldo   
				
						aSFEIVA[Len(aSFEIVA)][3] := aSFEIVA[Len(aSFEIVA)][3] * Round((Int((100 * nSaldo) / SE2->E2_VALOR) / 100),1)
						aSFEIVA[Len(aSFEIVA)][4] := aSFEIVA[Len(aSFEIVA)][4] * Round((Int((100 * nSaldo) / SE2->E2_VALOR) / 100),1)
						aSFEIVA[Len(aSFEIVA)][5] := Int((100 * nSaldo) / SE2->E2_VALOR)
						aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
						
						nSRIva	+= Round(aSFEIVA[Len(aSFEIVA)][3],MsDecimais(1))	//FE_VALBASE
						nSVIva	+= Round(aSFEIVA[Len(aSFEIVA)][6],MsDecimais(1))	//FE_VALBASE

					Endif 
					
				Next
					If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
						DBSELECTAREA("FVC")
						FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
						If FVC->(MsSeek(xFilial("FVC")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC))
							While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF1->(F1_FORNECE+F1_LOJA+F1_DOC)
								If AllTrim(FVC->FVC_TIPO) == "I" .and. FVC->FVC_RETENC > 0 .and. FVC->FVC_SERIE == SF1->F1_SERIE
									nTotRetSFE += FVC->FVC_RETENC
									AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
								EndIf
								FVC->(dbSkip())
							Enddo
						EndIf	
					EndIf
					SFE->(dbSetOrder(4))
					If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE+"I"))
						cChaveSFE := xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE //+ SE2->E2_PARCELA
						While !SFE->(Eof()) .And. cChaveSFE == SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE //+ SFE->FE_PARCELA 
							If SFE->FE_TIPO =="I" .and. SFE->FE_RETENC > 0 .and. EMPTY(SFE->FE_DTESTOR)
								If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
									If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
										nTotRetSFE += SFE->FE_RETENC
									EndIf
								Else
									nTotRetSFE += SFE->FE_RETENC
								EndIf
							EndIf
							SFE->(dbSkip())
						End
					EndIf
					If nTotRetSFE > 0
						//Proporcionaliza o que ja foi retido e abate da retencao que
						//foi calculada.
						IF !(nSaldo < SE2->E2_VALOR) .or. !lReSaIV       
							IF !(nSaldo == SE2->E2_VALOR)
								For nCount:= 1 To Len(aSFEIVA)
									If !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
										aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
									ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
										aSFEIVA[nCount][6] -= nTotRetSFE
									EndIf
								Next nCount   
							ElseIf (nSaldo == SE2->E2_VALOR) //.And. (Funname() == "FINA850")
									For nCount:= 1 To Len(aSFEIVA)
										If		aSFEIVA[nCount][6] == nTotRetSFE
												aSFEIVA[nCount][6] := 0
										ElseIf !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
												aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
										ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
												aSFEIVA[nCount][6] -= nTotRetSFE
										EndIf
									Next nCount
							Endif
						ElseIf (nSaldo == SE2->E2_VALOR) .Or. lReSaIV
							For nCount:= 1 To Len(aSFEIVA)
								If !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
									aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
								ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
									aSFEIVA[nCount][6] -= nTotRetSFE
								EndIf
							Next nCount 
						Endif			
					EndIf
					//Verifica se o valor de retencao calculado supera o valor
					//minimo a ser retido, caso seja inferior nao eh realizada
					//a retencao.
					If !lCalcAcm
						If Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "M" .and. cPaisLoc <> "ARG"
							nTotBase := 0
							nTotRet := 0
							aEval(aSFEIVA,{|x| nTotRet += (x[4] * x[5]/100), nTotBase +=x[3]})
								If  (nTotBase < SFF->FF_IMPORTE) .Or. (nTotRet < SFF->FF_VALMIN)
									aEval(aSFEIVA,{|x| x[6] := 0})
								EndIf
						Else
							nTotRet := 0
							aEval(aSFEIVA,{|x| nTotRet += x[6]})
								If  nTotRet <= SFF->FF_IMPORTE
									aEval(aSFEIVA,{|x| x[6] := 0})
								EndIf
						EndIf
					EndIf				
			EndIf
		Elseif lRetPa

			SFF->(DbSetOrder(5))
			SFF->(MsSeek(xFilial("SFF")+"IVR"+cCF)) //cConceito pegar do TES (CFO)
			If SFF->(FOUND())
				While !SFF->(Eof()) .And. xFilial("SFF")+"IVR"+cCF == SFF->(FF_FILIAL+FF_IMPOSTO+FF_CFO_C)
					If !Empty(cSerieNF) .And. PadR(cSerieNF,Len(SFF->FF_SERIENF)) != SFF->FF_SERIENF
						SFF->(dbSkip())
						Loop
					EndIf
					nPercRet:=SFF->FF_ALIQ/100					
					AAdd(aSFEIVA,array(12))
					aSFEIVA[Len(aSFEIVA)][1] := ""         		//FE_NFISCAL
					aSFEIVA[Len(aSFEIVA)][2] := ""       		//FE_SERIE
					aSFEIVA[Len(aSFEIVA)][3] := Round(xMoeda((nSaldo * nSigno),nMoedaCor,1,,5,aTxMoedas[Max(nMoedaCor,1)][2]),MsDecimais(1))	//FE_VALBASE
					aSFEIVA[Len(aSFEIVA)][4] := 0	//FE_VALIMP
					aSFEIVA[Len(aSFEIVA)][5] := SA2->A2_PORIVA   		//FE_PORCRET
					aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][3]) *(Iif(lCalRep .or. lCalcVen ,1, SA2->A2_PORIVA/100))
					aSFEIVA[Len(aSFEIVA)][9] := cCF // Gravar CFOP da opera玢o
					aSFEIVA[Len(aSFEIVA)][10]:= nPercRet
					aSFEIVA[Len(aSFEIVA)][11]:= SFF->FF_CFO // Gravar CFOP da opera堙埽o de compra (para o caso de NCP)
					aSFEIVA[Len(aSFEIVA)][12] := 0 
					
			   		If SA2->A2_PORIVA < 100.00 .And. (Empty(SA2->A2_IVPCCOB) .Or. Dtos(SA2->A2_IVPCCOB) < Dtos(dDataBase))
						If SA2->A2_PORIVA < 100.00 .And. (Empty(SA2->A2_IVPCCOB) .Or. Dtos(SA2->A2_IVPCCOB) < Dtos(dDataBase))
							nPercRet:=SFF->FF_ALIQ/100							
						EndIf
	
						If	lCalRep .or. lCalcVen
							nPercRet:= 1
						EndIf
	
						If lCalRep .or. lCalcVen
							nRetIva:=(aSFEIVA[Len(aSFEIVA)][3])*nPercRet
						Else
							nRetIva:= ((aSFEIVA[Len(aSFEIVA)][3]) *(SA2->A2_PORIVA/100))*nPercRet
						EndIf	
					Else
						nPercRet:=SFF->FF_ALIQ/100
	
						If  lCalRep .or. lCalcVen
							nPercRet:= 1
						EndIf
						If lCalRep .or. lCalcVen
							nRetIva:= (aSFEIVA[Len(aSFEIVA)][3])*nPercRet
						Else
							nRetIva:= ((aSFEIVA[Len(aSFEIVA)][3]) *(SA2->A2_PORIVA/100))*nPercRet
						EndIf
					EndIf
	
					aArea:=GetArea()
					DbSelectArea("SFE")
					SFE->(dbSetOrder(4))
					If SFE->(MsSeek(xFilial("SFE")+cFornece+cLoja))
						cChaveSFE := xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
						While !SFE->(Eof()) .And. cChaveSFE == SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
							If SFE->FE_TIPO =="I" .And. !Empty(cNumOp) .And. SFE->FE_NUMOPER == cNumOp
								nRetTotal += SFE->FE_RETENC
							EndIf
							SFE->(dbSkip())
						End
					EndIf
	
					RestArea(aArea)
	
					aSFEIVA[Len(aSFEIVA)][6]:=Iif(nRetTotal>0,((nRetTotal-nRetIva)*-1),nRetIva)
	
					//Verifica se o valor a ser retido ?maior que o valor do PA
					If nValor < aSFEIVA[Len(aSFEIVA)][6]
						aSFEIVA[Len(aSFEIVA)][6]:= nValor
					EndIF
	
	
					//Verifica se o valor de retencao calculado supera o valor
					//minimo a ser retido, caso seja inferior nao eh realizada
					//a retencao.
					nTotRet := 0
					aEval(aSFEIVA,{|x| nTotRet += x[6]})

					If  nTotRet < SFF->FF_IMPORTE
						aEval(aSFEIVA,{|x| x[6] := 0})
					EndIf
						
					If !Empty(cSerieNF)
						SFF->(dbSkip())
					Else
						Exit
					EndIf
				EndDo
			EndIf
		EndIF

	EndIf
EndIf

If lReSaIV .and. (naPagar > 0 .or. nVRetIVA > 0) .and. (nSaldo < SE2->E2_VALOR) .and. nSaldo == SE2->E2_SALDO 

	aAreaSFE := GetArea()
	DbSelectArea("SFE")
	SFE->(dbSetOrder(4))

	If SFE->(MsSeek(xFilial("SFE") + SE2->E2_FORNECE + SE2->E2_LOJA + SF1->F1_doc + SF1->F1_SERIE + "I"))
		aSalRet := FilImpRet(xFilial("SFE"),SE2->E2_FORNECE,SE2->E2_LOJA,SF1->F1_doc,SF1->F1_SERIE,"I") // SALDO RETENIDO
	Endif 
	RestArea(aAreaSFE)
						
	IF Len(aSalRet)> 0 .and. ((nVRetIVA - (aSalRet[1] + nSVIva)) <> 0)
		For nX:=1  to Len(aSFEIVA)
			aSFEIVA[nX][4] := aSFEIVA[nX][4] - ((aSalRet[1] + nSVIva) - nVRetIVA ) / LEN(aSFEIVA) 
			aSFEIVA[nX][6] := aSFEIVA[nX][6] - ((aSalRet[1] + nSVIva) - nVRetIVA ) / LEN(aSFEIVA) 
		Next
	ElseIf Len(aSalRet)> 0 .and. nVRetIVA == aSalRet[1]
		For nX:=1  to Len(aSFEIVA)						  
			aSFEIVA[nX][4] := 0 
			aSFEIVA[nX][6] := 0
		Next 
	Endif
Endif

RestArea(aArea)
Return aSFEIVA

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetIV2 � Autor �	Bruno Schmidt     � Data �  14/08/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � ArgRetIVA   - Calculo de Ret de IVA para NCP               ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetIV2(cAgente,nSigno,nSaldo,nProp,nA,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local oDlg4
Local aConIva	:= {}
Local aSFEIVA	:= {}
Local aDadosIVA	:= {}
Local aDocAcm 	:= {}
Local aConfIVA 	:= {}
Local aFIva		:= FilImpto({"3"},{"I"},{},{}) // Retorna todos los impuestos con IVA de SFB 
Local lAcmIVA	:= .F.
Local lExisteSFF:= .T.
Local lRetSerie	:=	.F.
Local lCalRet	:= .F.
Local lCalcImp	:= .T.
Local lCalRep	:= .F.
Local lCalcVen	:= .F.
Local lCalrtexp	:= .F.
Local lCalcIva 	:= .T.
Local lRetIva  	:= .F.
Local lCalcAcm 	:= .F.
Local lRetSit	:= .T.
Local lCalcNew	:= GetNewPar("MV_GRETIVA","N") == "S"
Local cChaveSFE	:= ""
Local cAliasCabe:= ""
Local cAliasItem:= ""
Local cAcmIVA	:= ""
Local cConc		:= ""
Local nPorIva	:= 0
Local nPosIva	:= 0
Local nCount	:= 0
Local nTotRet	:= 0
Local nTotRetSFE:= 0
Local nI	 	:= 1
Local nX 		:= 0
Local nValorBr	:= 0
Local nTotBase	:= 0
Local nTotImp	:= 0
Local nValMin	:= 0
Local nRetIva	:= 0
Local nValIVA	:= 0
Local nAliqIva 	:= 0
Local nVlrTot := 0
Local nPerc	:= 0
Local nValor	:= 0  
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.
Local nValBas := 0
Local nRedAliq	:= 0
Local lCalcCbu:=.F.

If type("cFornece")=="U"
	 cFornece	:=SE2->E2_FORNECE
	 cLoja		:=SE2->E2_LOJA
EndIf

DEFAULT nSigno	:= -1
DEFAULT nProp 	:= 1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIVA := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"I",nSaldo,dEmissao)
EndIf

//+---------------------------------------------------------------------+
//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten玟o.?
//+---------------------------------------------------------------------+


dbSelectArea("SF2")
dbSetOrder(1)
If lMsfil
	nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
	SF2->(dbGoTo(nRecSF2))
Else
	nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
	SF2->(dbGoTo(nRecSF2))
EndIf

If cPaisLoc == "ARG" .and. Alltrim(SE2->E2_TIPO) == "PA"
	Return aSFEIVA
Endif 

lCalRet := Iif(SubStr(Alltrim(PadR(SF2->F2_SERIE,nTamSer)),1,1) == "M" .or. (Alltrim(PadR(SF2->F2_SERIE,nTamSer))=="A" .and. cPaisLoc == "ARG"),.T.,.F.)
lCalcAcm := Subs(cAgente,2,1) == "N"
If(SF2->(ColumnPos("F2_TPNFEXP"))>0 .And. SFF->(ColumnPos("FF_VLRLEX"))>0 .And. SFF->(ColumnPos("FF_ALIQLEX"))>0 .And.  SF2->F2_TPNFEXP=="1")  //1 - Exportador 2- Normal
	lCalrtexp:=.T.
EndIf
If ExistBlock("F0851IMP")
	lCalcImp:=ExecBlock("F0851IMP",.F.,.F.,{"IV2"})
EndIf
// Tratamento do Reproweb - Situa玢o igual a 0 n鉶 deve reter Resoluci髇 General ( AFIP) 2226/07

SA2->( dbSetOrder(1) )
If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
	SA2->(MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
Else
	SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
Endif
If ((SA2->(ColumnPos("A2_CBUINI"))>0 .and. SA2->(ColumnPos("A2_CBUFIM"))>0) .and. SA2->A2_CBUINI <= SE2->E2_EMISSAO .and. SA2->A2_CBUFIM >= SE2->E2_EMISSAO .and.  Subs(SF2->F2_SERIE,1,1) =="A").or. Subs(SF2->F2_SERIE,1,1) =="M"
	lCalcCbu:=.T.
EndIf
If (Subs(cAgente,2,1) == "S" .Or. lCalRet .Or. lCalcAcm .or.lCalcCbu )  .And.  lCalcImp  .And.  lRetSit
	SA2->( dbSetOrder(1) )
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
		SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Else
		SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Endif
	
	If  Subs(cAgente,2,1) == "S"       
		If  SA2->A2_AGENRET == "S" .And. SubStr(Alltrim(PadR(SF2->F2_SERIE,nTamSer)),1,1) <> "M"//Si el proveedor es Agente de Ret. al igual que al emp, no se debe retener   //Return aSFEIVA
			Return aSFEIVA
		Endif
	Endif
	
	If SA2->A2_AGENRET == "N" .Or. lCalRet

		lCalRep := Iif(SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("2/4"),.T.,.F.)

		While Alltrim(SF2->F2_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
			SF2->(DbSkip())
			Loop
		Enddo

		If AllTrim(SF2->F2_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
			Iif(lMsFil,SF2->F2_MSFIL,xFilial("SF2", SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
			F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA
			
			//?Obter o Valor do Imposto e Base baseando se no rateio do valor ?
			//?do titulo pelo total da Nota Fiscal.                           ?
			
			SD2->(DbSetOrder(3))
			If lMsFil
				SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			Else
				SD2->(MsSeek(xFilial("SD2", SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			EndIf
			If SD2->(Found())
				Do while Iif(lMsFil,SD2->D2_MSFIL,xFilial("SD2", SE2->E2_FILORIG))==SD2->D2_FILIAL.And.SF2->F2_DOC==SD2->D2_DOC.AND.;
					SF2->F2_SERIE==SD2->D2_SERIE.AND.SF2->F2_CLIENTE==SD2->D2_CLIENTE;
					.AND.SF2->F2_LOJA==SD2->D2_LOJA.AND.!SD2->(EOF())

					If AllTrim(SD2->D2_ESPECIE)<>AllTrim(SF2->F2_ESPECIE)
						SD2->(DbSkip())
						Loop
					Endif
					
					aImpInf := TesImpInf(SD2->D2_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD2->(DbSkip())
						Loop
					EndIf
					
					SB1->(DbSetOrder(1))
					If !Empty(SD2->D2_CF)
						If !lCalcAcm
							nRateio := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) )
						Else
							nRateio := 1
						EndIf
						nPosIva:=ASCAN(aConIva,{|x| x[1]==SD2->D2_CF})
						If lCalcNew    // Calcula IVA metodo Novo
							If nPosIva<>0
								If Len(aConIVA[nPosIVA]) >= 6
									cAcmIVA := aConIva[nPosIva][6]
								EndIF 
								If ( (SB1->(ColumnPos("B1_RG2849"))>0 .AND. SB1->B1_RG2849 <> 0) .Or. cAcmIVA == "2")
									nValor := Round(xmoeda(SD2->D2_TOTAL-SD2->D2_DESCON,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
									If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BAIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0
										aConIva[nPosIva][2]+= ((aConIva[nPosIva][4] * nValor )- ((SD2->D2_IVAFRET+SD2->D2_IVAGAST))) * nRateio
										aConIva[nPosIva][3]+= ( nValor - ((SD2->D2_BaIVAFR+SD2->D2_BaIVAGA)) )  * nRateio
									Else
										aConIva[nPosIva][2]+= (aConIva[nPosIva][4] * nValor )  * nRateio
										aConIva[nPosIva][3]+= nValor  * nRateio
									EndIf
								Else
									nValor :=  ValImpto(aFIva,SD2->D2_TES)		
									//ColumnPos adicionado pois n�o foi encontrado a origem da cria��o deste campo � N�O RETIRAR
									If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BAIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0
										aConIva[nPosIva][2]:=aConIva[nPosIva][2]+((aConIva[nPosIva][4] * nValor )- ((SD2->D2_IVAFRET+SD2->D2_IVAGAST))) * nRateio
										aConIva[nPosIva][3]:=aConIva[nPosIva][3]+( nValor - ((SD2->D2_BaIVAFR+SD2->D2_BaIVAGA)) )  * nRateio
									Else
										aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(aConIva[nPosIva][4] * nValor )  * nRateio
										aConIva[nPosIva][3]:=aConIva[nPosIva][3]+nValor  * nRateio
									EndIf
								EndIf
							
							Else
								cAcmIVA := ""
								If  lCalRep .Or. lCalcVen
									aImpInf := TesImpInf(SD2->D2_TES)
									For nI := 1 To Len(aImpInf)
										If  "IV"$Trim(aImpInf[nI][01]) .And.  Trim(aImpInf[nI][01])<>"IVP"
											Aadd(aConIva,{SD2->D2_CF,SD2->(FieldGet(ColumnPos(aImpInf[nI][02]))),SD2->(FieldGet(ColumnPos(aImpInf[nI][07]))), (SD2->(FieldGet(ColumnPos(aImpInf[nI][10]))) /100)})
										EndIf
									Next
								Else
									SFF->(DbSetOrder(5))
									If SFF->(MsSeek(xFilial("SFF")+"IVR"))
										nPerc:=0
										lAchou:=.F.
										While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
											If (Alltrim(SFF->FF_SERIENF)==SubStr(Alltrim(PadR(SD2->D2_SERIE,nTamSer)),1,1)  .And.  SD2->D2_CF==  SFF->FF_CFO_V)
												lExistSFF:=.T.
												If GetNewPar("MV_SEGMEN","") $ "1|2|4"
													If SA2->(ColumnPos("A2_TIPROVM"))>0 .AND. ((SA2->A2_TIPROVM $ "1|2|3" .AND. SFF->FF_REGIST=="1" ) .OR. ( SA2->A2_TIPROVM $ "4" .AND. SFF->FF_REGIST=="2" ))
														lExistSFF:=.T.
													Else
														lExistSFF:=.F.
													EndIf
												Endif
												
												
												If lExistSFF
													lAchou:=.T.
													nPerc:=SFF->FF_ALIQ
													cCFOIVA	:= SFF->FF_CFO_C
													cCFOVIVA := SFF->FF_CFO_V
													nRedAliq:= SFF->FF_REDALIQ 
													If SFF->(ColumnPos("FF_TPLIM")) > 0
														cAcmIVA := SFF->FF_TPLIM
														nLimite := SFF->FF_LIMITE
													EndIf													
												EndIf
											EndIf
											SFF->(DbSkip())
										EndDo
										
										If cAcmIva != "2" .And. Subs(cAgente,2,1) == "N" .And. SA2->A2_AGENRET == "N" .And. SubStr(Alltrim(PadR(SF1->F1_SERIE,nTamSer)),1,1) <> "M" .And. !lCalcCbu
											Return aSFEIVA
										EndIf
										
										/*************************************************
										//Verifica o acumulado para o c醠culo do imposto
										//************************************************/

										//caso seja branco ou n鉶 foi selecionado nenhum m閠odo de acumulo
										If Type("aRetIvAcm") <> "A"
											aRetIvAcm := Array(3)
										EndIf
										If !Empty(cAcmIVA) .And. cAcmIVA <> "0" .And. lCalcAcm
											aDadosIVA 	:= F850AcmIVA(cAcmIVA,SF2->F2_EMISSAO,SF2->F2_CLIENTE,SF2->F2_LOJA,,cCFOIVA,cCFOVIVA)
											If aDadosIVA[4]
												nBase := aDadosIVA[1] - aDadosIVA[2]

												If Abs(nBase) > nLimite .Or. aDadosIVA[2]  > 0
													lRetIva := .T.
													lAcmIVA := .T.

													For nX := 1 to Len(aDadosIVA[3])
														If Empty(aDadosIVA[3][nX][2])
															aAdd(aDocAcm, {aDadosIVA[3][nX][3], aDadosIVA[3][nX][4], Iif(aDadosIVA[3][nX][1],.F.,.T.),aDadosIVA[3][nX][6],aDadosIVA[3][nX][5]})
														EndIf
													
														//Propor��o do valor dos t�tulos parcelados
														If aDadosIVA[3][nX][5] = SE2->(Recno())
															nRateio := aDadosIVA[3][nX][6]/100	
														EndIf
													
													Next nX

													If Len(aDocAcm) > 0
														aAdd(aConfIVA,nPerc)

														aRetIvAcm[1] :=  aConfIVA

														If aRetIvAcm[2] == Nil
															aRetIvAcm[2] := {}
														EndIf

														For nX := 1 to Len(aDocAcm)
															If aDocAcm[nX][2] == "SF1"
																nPosDoc := aScan(aRetIvAcm[2],{|x| x[1]==aDocAcm[nX][1] .And. x[2]=="SF1"})
															Else
																nPosDoc := aScan(aRetIvAcm[2],{|x| x[1]==aDocAcm[nX][1] .And. x[2]=="SF2"})
															EndIf
															If nPosDoc == 0
																Aadd(aRetIvAcm[2],aDocAcm[nX])
															EndIf
														Next nX

													EndIf

												EndIf
											Else
												lRetIva := .T.
											EndIf

										Else
											//Se n鉶 usa cumulatividade de IVA, calcula no m閠odo antigo
											lRetIva := .T.
										EndIf
										If  lCalrtexp  .And. lAchou
											If Alltrim(PadR(SD2->D2_SERIE,nTamSer)) == "B" .and. SA2->A2_TIPO == "I" .and. cPaisLoc == "ARG"
												nPerc := POSICIONE("SFB",1,xFilial("SFC")  + POSICIONE("SFC", 2, xFilial("SFC") + SD2->D2_TES + "IV", "FC_IMPOSTO" ),"FB_ALIQ")
												nValorBr:=Round(xmoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) / (1+ (nPerc/100))
											Else
												nValorBr:=Round(xmoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
												nPerc:=Iif(nValorBr>SFF->FF_VLRLEX,SFF->FF_ALIQ,SFF->FF_ALIQLEX)
											EndIf
										EndIf

										If lRetIva
											nValor :=  ValImpto(aFIva,SD2->D2_TES)
											nValBas :=  ValBasImp(aFIva,SD2->D2_TES)
											If cPaisLoc == "ARG" .And. ( (SB1->(ColumnPos("B1_RG2849"))>0 .AND. SB1->B1_RG2849 <> 0 ) .Or. cAcmIVA == "2")
												nValor := Round(xmoeda(SD2->D2_TOTAL-SD2->D2_DESCON,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
											EndIf
											If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BAIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0 .and. !lCalcCbu
												Aadd(aConIva,{SD2->D2_CF,(((nPerc /100) * (nValor ))  - (SD2->D2_IVAFRET+SD2->D2_IVAGAST)),((nValor )- (SD2->D2_BaIVAFR +SD2->D2_BaIVAGA)), (nPerc /100),,cAcmIVA})
											Elseif lCalcCbu 
												Aadd(aConIva,{SD2->D2_CF, (nPerc/100*(1-nRedAliq/100) * nValBas ),nValBas,(nPerc /100*(1-nRedAliq/100)),,cAcmIVA})											
											Else
												Aadd(aConIva, {SD2->D2_CF, ((nPerc / 100) * (nValor)), nValor, (nPerc / 100), , cAcmIVA})
											EndIf
										EndIf

									EndIf
								EndIf
							EndIF
						Else
							If nPosIva<>0
								If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BaIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD2->D2_VALIMP1-SD2->D2_IVAFRET+SD2->D2_IVAGAST)
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD2->D2_BASIMP1-SD2->D2_BaIVAFR+SD2->D2_BaIVAGA)
								Else
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD2->D2_VALIMP1)
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD2->D2_BASIMP1)
								EndIf

							Else
								Aadd(aConIva,{SD2->D2_CF,SD2->D2_VALIMP1,SD2->D2_BASIMP1,0})
							Endif
						Endif
					ElseIf SB1->(MsSeek(Iif(lMsFil .And. !Empty(xFilial("SB1")),SD2->D2_MSFIL,xFilial("SB1")) + SD2->D2_COD)) .And. !lCalcAcm
						nPosIva:=ASCAN(aConIva,{|x| x[1]==SB1->B1_CONCIVA})
						If nPosIva<>0
							aConIva[nPosIva][2]:=aConIva[nPosIva][2]+SD2->D2_VALIMP1
							aConIva[nPosIva][3]:=aConIva[nPosIva][3]+SD2->D2_BASIMP1
						Else
							Aadd(aConIva,{SB1->B1_CONCIVA,SD2->D2_VALIMP1,SD2->D2_BASIMP1})
						Endif
					ElseIf !lCalcAcm
						nPosIva:=ASCAN(aConIva,{|x| x[1]==SA2->A2_ACTRET})
						If nPosIva<>0
							aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD2->D2_VALIMP1)
							aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD2->D2_BASIMP1)
						Else
							Aadd(aConIva,{SA2->A2_ACTRET,SD2->D2_VALIMP1,SD2->D2_BASIMP1})
						Endif
					Endif
					SD2->(DbSkip())
				EndDo
			Endif
		Endif
		
		//Verificar a porcetagem correteado do Iva conforme informada nas datas
		If SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. SA2->(ColumnPos("A2_IVPCCOB")) > 0
			If (dDataBase >= SA2->A2_IVPDCOB .and. dDataBase <= SA2->A2_IVPCCOB)
				nPorIva := SA2->A2_PORIVA
			ElseIf (Empty(SA2->A2_IVPDCOB) .and. dDataBase <= SA2->A2_IVPCCOB)
				nPorIva := SA2->A2_PORIVA
			ElseIf (Empty(SA2->A2_IVPCCOB) .and. dDataBase >= SA2->A2_IVPDCOB)
				nPorIva := SA2->A2_PORIVA
			EndIf
		ElseIf !SA2->(ColumnPos("A2_IVPDCOB")) > 0 .and. !SA2->(ColumnPos("A2_IVPCCOB")) > 0 
			nPorIva := SA2->A2_PORIVA
		Else
			nPorIva := 100
		EndIf

		// Exenciones 
		SFH->(dbSetOrder(1))
		SFH->(dbGoTop())
		If SFH->(MsSeek(xFilial()+SA2->A2_COD+SA2->A2_LOJA+"IVR")) .and. SFH->(ColumnPos("FH_PERCENT"))>0
			If  (SFH->FH_ISENTO == "S" .OR. SFH->FH_PERCENT == 100)
				nPorIva := 0
			ElseIf A085aVigSFH()     
				If SFH->FH_ISENTO == "S" .OR. SFH->FH_PERCENT == 100
					nPorIva := 0				
				Else
					nPorIva := SFH->FH_PERCENT
				EndIf
			Endif 
		Endif 

		//?Gravar Retenciones. 
		
		For nCount:=1  to Len(aConIva)
			aConIva[nCount][2]   := Round(xMoeda(aConIva[nCount][2],SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
			aConIva[nCount][3]   := Round(xMoeda(aConIva[nCount][3],SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))


			AAdd(aSFEIVA,array(12))
			aSFEIVA[Len(aSFEIVA)][1] := SF2->F2_DOC         								//FE_NFISCAL
			aSFEIVA[Len(aSFEIVA)][2] := SF2->F2_SERIE       								//FE_SERIE
			IF lCalcCbu 
				aSFEIVA[Len(aSFEIVA)][3] := nValBas*nSigno
				aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2]*nProp)*nSigno
			Else 
				aSFEIVA[Len(aSFEIVA)][3] := IIf (Alltrim(PadR(SF2->F2_SERIE,nTamSer)) == "B" .and. SA2->A2_TIPO == "I" .and. cPaisLoc == "ARG",(aConIva[nCount][3]*nProp)*nSigno+(aConIva[nCount][2]*nProp)*nSigno,(aConIva[nCount][3]*nProp)*nSigno)					//FE_VALBASE
				aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2]*nProp)*nSigno					//FE_VALIMP
			Endif	
			aSFEIVA[Len(aSFEIVA)][5] := Iif((lCalRet .and. Alltrim(SF2->F2_SERIE) <> "A") .or.  lCalcVen,100,nPorIva)	//FE_PORCRET
			aSFEIVA[Len(aSFEIVA)][6] := 0
			aSFEIVA[Len(aSFEIVA)][9] := aConIva[nCount][1]  									// Gravar CFOP da opera玢o
			aSFEIVA[Len(aSFEIVA)][10]:= aConIva[nCount][4]*100								// Gravar A PORCENTAGEM DA ALIQUOTA
			aSFEIVA[Len(aSFEIVA)][11]:= " "
			aSFEIVA[Len(aSFEIVA)][12]:= aConIva[nCount][2]  
						
			If LEN(aConIva[1])==3 //pROVISORIO, PARA QUE NO DE ERROR EN BASES DESACTUALIZADAS
				If SFF->(FOUND())
						If (cPaisLoc=="ARG" .or. SA2->(ColumnPos("A2_IVRVCOB")) > 0) .And. SA2->(ColumnPos("A2_IVPCCOB")) > 0
						If SA2->A2_PORIVA < 100.00 .And. (Empty(SA2->A2_IVPCCOB) .Or. Dtos(SA2->A2_IVPCCOB) < Dtos(dDataBase))
							MsgAlert(OemToAnsi(STR0154)+SA2->A2_COD+OemToAnsi(STR0146)) //"La fecha de validad para la reducci髇 del porcentaje de la retenci髇 del IVA del proveedor ya se ha vencido. ". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
							//Zera o array das retencoes de IVA...
							aSFEIVA := {}
							//Sai do loop...
							Exit
						EndIf

						If cPaisLoc=="ARG" .Or. Empty(SA2->A2_IVRVCOB)
							aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]*(SFF->FF_ALIQ/100))*(nPorIva/100)
						ElseIf Dtos(SA2->A2_IVRVCOB) < Dtos(dDataBase)
							MsgAlert(OemToAnsi(STR0145)+SA2->A2_COD+OemToAnsi(STR0146)) //	"Ha vencido el perido de observacion del proveedor  "+SA2->A2_COD+". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
							//Zera o array das retencoes de IVA...
							aSFEIVA := {}
							//Sai do loop...
							Exit
						Else
							aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
						EndIf
					Else
						aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]*(SFF->FF_ALIQ/100))*(Iif((lCalRet .and. Alltrim(SF2->F2_SERIE) <> "A"),100,nPorIva)/100)
					EndIf
						
						SFF->(DbSetOrder(5))
						If SFF->(MsSeek(xFilial("SFF")+"IVR"))
							lAchou:=.F.
							While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
								If (Alltrim(SFF->FF_SERIENF)==Alltrim(PadR(SD2->D2_SERIE,nTamSer))  .And.  aConIva[nCount][1] ==  SFF->FF_CFO_V)
									lAchou:=.T.
								Else
							   		SFF->(DbSkip())
								EndIf
								
							EndDO
                   EndiF
						
					If SFF->FF_IMPOSTO == "IVR"
						If !Empty(SFF->FF_CFO)
							aSFEIVA[Len(aSFEIVA)][11] := SFF->FF_CFO // Gravar CFOP da opera堙埽o de compra (para o caso de NCP)
						Else
							MsgAlert(STR0256)
							aSFEIVA[Len(aSFEIVA)][11] := " "
						EndIf
					EndIf
					
					
					nTotRet += aSFEIVA[Len(aSFEIVA)][6]
				ELSE
					DEFINE MSDIALOG oDlg4 FROM 65,0 To 218,312 Title OemToAnsi(STR0074) Pixel //"Inconsistencia"
					@ 2,3 To 51,150 Pixel of oDlg4
					//"La actividad ", " de IVA no esta registrada en la Tabla SFF"
					//"por lo tanto no se generara retenci de IVA. Si desea Continuar con la "
					//"Orden de Pago acepte, sino cancele. "
					@ 10,004 SAY OemToAnsi(STR0050)+ aConIva[nCount][1] +OemToAnsi(STR0051) PIXEL Of oDlg4
					@ 23,004 SAY OemToAnsi(STR0052)	PIXEL Of oDlg4
					@ 36,004 SAY OemToAnsi(STR0053) 	PIXEL	Of oDlg4
					DEFINE SBUTTON FROM 57,064  Type 1 Action (lRetOk	:=	.T.,oDlg4:End())    Pixel ENABLE Of oDlg4
					DEFINE SBUTTON FROM 57,104  Type 2 Action (lRetOk	:=	.F.,oDlg4:End())  Pixel ENABLE Of oDlg4
					Activate Dialog oDlg4 CENTERED
				Endif
			Else

				SFF->(DbSetOrder(5))
				If SFF->(MsSeek(xFilial("SFF")+"IVR"))
					lAchou:=.F.
					While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
						If (Alltrim(SFF->FF_SERIENF)==Alltrim(PadR(SF2->F2_SERIE,nTamSer))  .And.  aConIva[nCount][1]==  SFF->FF_CFO_V)
							lAchou:=.T.
						Else
							SFF->(DbSkip())
						EndIf
					EndDO
                End
			
				If SFF->FF_IMPOSTO == "IVR"
					If !Empty(SFF->FF_CFO)
						aSFEIVA[Len(aSFEIVA)][11] := SFF->FF_CFO // Gravar CFOP da opera堙埽o de compra (para o caso de NCP)
					Else
						MsgAlert(STR0256)
						aSFEIVA[Len(aSFEIVA)][11] := " "
					EndIf
				EndIf
				
				If  SA2->(ColumnPos("A2_IVPCCOB")) > 0 .And. SA2->(ColumnPos("A2_IVPDCOB")) > 0

					If lCalcNew
						If lCalRep .or. lCalcVen
							aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4])
						Else
							aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]) *(nPorIva/100)
						EndIf
					Else
						aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
					EndIf

				EndIf

				nTotRet += aSFEIVA[Len(aSFEIVA)][6]
			
			Endif
			
			//?Generar Titulo de Impuesto no Contas a Pagar.   
			
			aSFEIVA[Len(aSFEIVA)][7] := SE2->E2_VALOR
			aSFEIVA[Len(aSFEIVA)][8] := SE2->E2_EMISSAO
		Next
			If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
				DBSELECTAREA("FVC")
				FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
				If FVC->(MsSeek(xFilial("FVC")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC))
					While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF2->(F2_CLIENTE+F2_LOJA+F2_DOC)
						If AllTrim(FVC->FVC_TIPO) == "I" .and. ABS(FVC->FVC_RETENC) > 0 .and. FVC->FVC_SERIE == SF2->F2_SERIE
							nTotRetSFE += FVC->FVC_RETENC
						EndIf
						FVC->(dbSkip())
					Enddo
				EndIf	
			EndIf
			//Levanta quanto ja foi retido para a factura corrente, para em seguida
			//abater do total calculado para retencao.
			SFE->(dbSetOrder(4))
			If SFE->(MsSeek(xFilial("SFE")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC+SF2->F2_SERIE+"I"))
				cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+"I"
				While !SFE->(Eof())
					If cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_TIPO .and. EMPTY(SFE->FE_DTESTOR)  .And.;
					 (SFE->FE_RETENC<0 )
						nTotRetSFE += SFE->FE_RETENC
					EndIf
					SFE->(dbSkip())
				End
			EndIf
			
			If ABS(nTotRetSFE) > 0	
				//Proporcionaliza o que ja foi retido e abate da retencao que
				//foi calculada.
				IF !(nSaldo < SE2->E2_VALOR)
					IF !(nSaldo == SE2->E2_VALOR)
						For nCount:= 1 To Len(aSFEIVA)
							If !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
								aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
							ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
								aSFEIVA[nCount][6] -= nTotRetSFE
							EndIf
						Next nCount   
					ElseIf (nSaldo == SE2->E2_VALOR)
						For nCount:= 1 To Len(aSFEIVA)
							If		aSFEIVA[nCount][6] == nTotRetSFE
									aSFEIVA[nCount][6] := 0
							ElseIf !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
									aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
							ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
									aSFEIVA[nCount][6] -= nTotRetSFE
							EndIf
						Next nCount
					EndIf
				ElseIf (nSaldo == SE2->E2_VALOR)
					For nCount:= 1 To Len(aSFEIVA)
						If !(aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
							aSFEIVA[nCount][6] -= (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet))
						ElseIf (aSFEIVA[nCount][6] == (nTotRetSFE * (aSFEIVA[nCount][6] / nTotRet)) .and. Funname() <> "FINA850") 
							aSFEIVA[nCount][6] -= nTotRetSFE
						EndIf
					Next nCount 	
				Endif
			EndIf
		//Verifica se o valor de retencao calculado supera o valor
		//minimo a ser retido, caso seja inferior nao eh realizada
		//a retencao.
		
		If !lCalcAcm
			nTotRet := 0
			aEval(aSFEIVA,{|x| nTotRet += (x[6]*-1)})
			If  nTotRet <= SFF->FF_IMPORTE
				aEval(aSFEIVA,{|x| x[6] := 0})
			EndIf
		EndIf
			
	EndIf
EndIf

Return aSFEIVA

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGRetIB � Autor �	Bruno Schmidt     � Data �  14/08/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetIB    - Calculo de Ret de IIB para NF                ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

Function ARGRetIB(cAgente,nSigno,nSaldo,cCF,cProv,lPA,nPropImp,aConfProv,lSUSSPrim,lIIBBTotal,aImpCalc,aSUSS,nLinha,lLimNRet,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local aZonaIb 	:= {}
Local aSFEIB	:= {}
Local aPerIB	:= {}
Local aProvVerif:= {}
Local aAreaAnt	:= {}
Local aImpInf 	:= {}
Local aCF 		:= {}
Local aFil		:= {}
Local aAreaSFF 	:= {}
Local aRes      := {}
Local lCalcIb	:= .T.
Local lCalImpos	:= .F.
Local lCalcMon 	:= .F.
Local lRet 		:= .F.
Local lIsento	:= .F.
local lGravoPa 	:= .T.
Local lThdezb	:= .F.	
Local lPropIB	:= Iif(GetMV("MV_PROPIB",.F.,1)==2,.T.,.F.)
Local lExisCGF	:= TableInDic("CGF")
Local lNoIns    := .F.
Local lCalNoIns := .F.
Local lSmRMCR   := .F.
Local ltpLimVa  := .F.
Local cProvEnt 	:= ""
Local cChave	:= ""
Local cQuery 	:= ""
Local cTabTemp	:= ""
Local cItem		:= ""	
Local cEmpAct	:= ""
Local cZona		:= ""
Local cSituaca  := ""
Local nRatValImp:= 0
Local nRateio	:= 0
Local nValMerc	:= 0
Local nJ		:= 0
Local nPosIb	:= 0
Local nPosRet	:= 0
Local nRecSM0	:= 0
Local nMoeda	:= 1
Local nAliq     := 0
Local nVlrTotal	:= 0
Local nPos      := 0
Local nI, nA	:= 0
Local aI, aX		:= 0
Local nTotRetSFE:= 0
Local nTotBasSFE:= 0
Local nLimMInRet:= 0
Local nImposto	:= 0
Local nBaseAtual:= 0
Local nImpAtual	:= 0
Local nRetencao	:= 0
Local nTotBase	:= 0
Local nRecSFF 	:= 0
Local nDeduc	:= 0
Local nCofRet	:= 0
Local nMinUnit	:= 0
Local nMinimo	:= 0
Local nItem		:= 0
Local nNumRegs 	:= 0
Local nAliqAux	:= 0
Local nCoefmul	:= 0
Local nPercTot	:= 1   
Local nAliqRet 	:= 0 
Local nAliqAdc 	:= 0 
Local nObtMin 	:= 0
Local nLmRMCR		:= 0
Local cCond		:= 0
Local cZonaSFH := ""
Local cTipoSFH := ""
Local lAgRetCCO := .T.
Local nRecSF1	:= 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.
Local cTipoCalc 	:= ""
Local nValorRet 	:= 0
Local nImpCalc 	:= 0
Local nCRFRET   := 0
Local cOriMin := ""
Local aRetPreO 	:= {}
Local nCOTipMin 	:= 0
Local nTotCCO3 	:= nSaldo
Local lCCOLimRet := Iif(aConfProv[7] <> " ", .T., .F.)
Local cChaveSFE := ""
Local nLimiteSFF := 0
Local lLimPrim := .F.
Local nPrAliqIB := 1
Local lBxParcial := Iif(SE2->E2_SALDO < SE2->E2_VALOR,.T.,.F.)
Local lAjusta	:= .F.
Local lCalcPa	:= .F.
Local lCalPaIns	:= .F.

If	type("cFornece")=="U"
	cFornece	:=SE2->E2_FORNECE
	cLoja		:=SE2->E2_LOJA
EndIf

DEFAULT nSigno		:= 1
DEFAULT nPropImp	:= 1
DEFAULT aConfProv	:= {}
DEFAULT lSUSSPrim	:= .T.
DEFAULT lIIBBTotal:= .F.
DEFAULT aImpCalc	:= {}
DEFAULT aSUSS		:= {}
DEFAULT nLinha	:= 1
DEFAULT lLimNRet := .F.
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIB := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"B",nSaldo,dEmissao,aConfProv)
EndIf
//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten噭o.?

lEsRetAdc := .F.
aArea:=GetArea()
If !Empty(GetMV("MV_AGIIBB",,"CF|BA|SF|SE|TU|SA|JU|SL|MI|FO|ME|ER|SJ|LR|CO|CB|CA|NE|LP|TF|CH|RN|CR")) .And. nSigno > 0
	SA2->( dbSetOrder(1) )
	If !lPA
		If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SA2")
			SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Else
			SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Endif
	Else
		SA2->( MsSeek(xFilial("SA2")+CFORNECE+CLOJA) )
	Endif
	
	//?Generar las Retenci de Ingresos Brutos                       ?
	//?Reter Ingressos Brutos somente se valor total da Orden de Pago ?
	//?for igual ou maior que $400,00.                                ?
	

	If SA2->(ColumnPos("A2_DTICALB")) > 0 .And. SA2->(ColumnPos("A2_DTFCALB")) > 0 ;
	   .And. !Empty(SA2->A2_DTICALB) .And. !Empty(SA2->A2_DTFCALB)
	    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALB) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALB) )
	   		lCalcIb:=.F.
	    EndIf
	EndIf

	If ExistBlock("F0851IMP")
		lCalcIb:=ExecBlock("F0851IMP",.F.,.F.,{"IB"})
	EndIf

	If lCalcIb .And. SA2->A2_RETIB == "S"
    	If lRetPa .and. lPA
    		nMoeda := nMoedaCor
			nPosIb := Ascan(aZonaIb,{|X| X[1]==cCF .And. X[2]==cProv})

			//Por Entrega
			If aConfProv[2] == "1" .And. aConfProv[3] == "1" .And. aConfProv[1] == cProv
				cProvEnt := cProv
				lCalcPa := .T.
			//Por Pago
			ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "2"
				SX5->(MsSeek(xFilial()+"74"))
				cSucur := IIf( lFWCodFil, FWGETCODFILIAL, SM0->M0_CODFIL )

				nRecSM0 := SM0->(RecNo())
				cEmpAct := SM0->(M0_CODIGO)
				
				SM0->(MsSeek(cEmpAct+cSucur))
				If SM0->(FOUND()) .And. aConfProv[1] == SM0->M0_ESTENT
					cProvEnt := SM0->M0_ESTENT
					lCalcPa := .T.
				EndIf
				SM0->(DbGoto(nRecSM0))
			//Por Inscri玢o
			ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "3"
				cProvEnt := aConfProv[1]
				lCalPaIns	:= .T.
			Endif
			
			/* Por solicitud llamado (TSRHFF)
			If Empty(cProvEnt) .And. lPA .AND. !aConfProv[2] == "2" 
				cProvEnt := cProv
			EndIf
			
			If aConfProv[1] != cProv .And. lPA
				cProvEnt := ""
			EndIf */						
			
			If CCO->(ColumnPos("CCO_TPRET")) > 0 .and. CCO->(ColumnPos("CCO_TPRENI")) > 0  
				If NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", aConfProv[1]) //Identifica tratamiento Retencion sujeto No inscripto
					aRes:= NoInsCCORet( aConfProv[1],cProv)
					lCalNoIns := aRes[1][2]
					lNoIns := IIf(!Empty(aRes[1][1]),.T.,.F.)  
				EndIf 	
			EndIf
			
			AAdd(aZonaIb,{cProvEnt,cCF,nSaldo,aConfProv[4],.F.,""})

		Else

			dbSelectArea("SF1")
			SF1->(dbSetOrder(1))
			If lMsfil
				nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
				SF1->(dbGoTo(nRecSF1))
			Else
				nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
				SF1->(dbGoTo(nRecSF1))
			EndIf
			//If SF1->(Found())
			If nRecSF1 > 0
				cChave := SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA

				nRateio     := SF1->F1_VALMERC / SF1->F1_VALBRUT

				nRatValImp  := Iif(aConfProv[4] == "M" .And. !lPropIB,1,( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) ) )
				nValMerc    := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
			   	nMoeda   := Max(nMoeda,SF1->F1_MOEDA)

				SD1->(DbSetOrder(1))
				If lMsfil
					SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
				Else
					SD1->(MsSeek(xFilial("SD1",SF1->F1_FILIAL)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
				EndIf
				If SD1->(Found())
					nObtMin := 0
					Do While IIf(lMsfil,SD1->D1_MSFIL,xFilial("SD1",SF1->F1_FILIAL))==SD1->D1_FILIAL .And. SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == cChave
						If AllTrim(SD1->D1_ESPECIE) <> Alltrim(SF1->F1_ESPECIE)
							SD1->(DbSkip())
							Loop
						Endif

						If NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", aConfProv[1]) //Identifica tratamiento Retencion sujeto No inscripto : NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", SA2->A2_EST)
							aRes:= NoInsCCORet( aConfProv[1],SD1->D1_PROVENT)  //NoInsCCORet( SA2->A2_EST,SD1->D1_PROVENT)
							If (aRes[1][1] == "2" .and. SD1->D1_PROVENT <> aConfProv[1]) .Or. (aConfProv[1] == "TU" .and. SA2->A2_EST <> "TU")
								SD1->(DbSkip())
								Loop
							Endif
							aRes := {}
						Else 
							aRes := {}
						Endif 

						cProvEnt := ""

						//Por Entrega
						If aConfProv[2] == "1" .And. aConfProv[3] == "1"
							If SD1->(ColumnPos("D1_PROVENT")) >0 .And. !Empty(SD1->D1_PROVENT) .And. aConfProv[1] == SD1->D1_PROVENT
								cProvEnt := SD1->D1_PROVENT
							ElseIf  SF1->(ColumnPos("F1_PROVENT")) >0 .And.  !Empty(SF1->F1_PROVENT) .And. aConfProv[1] == SF1->F1_PROVENT .And. Empty(SD1->D1_PROVENT)
								cProvEnt := SF1->F1_PROVENT
							Endif
						//Por Pago
						ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "2"
							SX5->(MsSeek(xFilial()+"74"))
							cSucur := IIf( lFWCodFil, FWGETCODFILIAL, SM0->M0_CODFIL )
							While !SX5->(EOF())  .And. SX5->X5_TABELA=="74"
								If SD1->D1_LOCAL  $ SX5->(X5DESCRI())
									cSucur := SUBS(SX5->X5_CHAVE,3,2)
									Exit
								Endif
								SX5->(DbSkip())
							EndDo

							cEmpAct := SM0->(M0_CODIGO)

							If SM0->(MsSeek(cEmpAct+cSucur))
									If SM0->(FOUND()) .And. aConfProv[1] == SM0->M0_ESTENT
									cProvEnt := SM0->M0_ESTENT
								EndIf
							ElseIf SM0->(MsSeek(cEmpAct+cFilAnt))
								cSucur := cFilAnt
								If SM0->(FOUND()) .And. aConfProv[1] == SM0->M0_ESTENT
									cProvEnt := SM0->M0_ESTENT
								EndIf							
							EndIf
						//Por Inscri玢o
						ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "3"
							If aConfProv[1] =="MI"
								If CCO->(MsSeek(xFilial("CCO") + aConfProv[1]))
									If CCO->CCO_TPRENI == "2" .And. aConfProv[4] == "N" .And. aConfProv[1] != SD1->D1_PROVENT
										SD1->(DbSkip())
										Loop
									Else
										cProvEnt := aConfProv[1]	
									EndIf
								EndIf
							Else
								cProvEnt := aConfProv[1]
							EndIf
						Endif

						//homologado pelo depto. de localizacoes...
						If cProvEnt $ GetMv("MV_AGIIBB") .And. !(cProvEnt$"CF|BA|SJ|TU|SF|SE|SA|JU|SL|MI|FO|ME|ER|SC|LR|CO|CB|CA|NE|LP|TF|CH|RN|CR")
							If aScan(aProvVerif,cProvEnt) == 0
								MsgInfo(STR0159+cProvEnt+STR0160) //"Para la provincia o ciudad de "#" es necesario que el departamento de ubicaciones de Microsiga desarrolle la rutina de calculo. Por favor entre en contacto, con el administrador del sistema."
								AAdd(aProvVerif,cProvEnt)
							EndIf

							SD1->(DbSkip())
							Loop
						EndIf
						If !(cProvEnt $ GetMv("MV_AGIIBB"))
							SD1->(DbSkip())
							Loop
						EndIf	
						
						nVlrTotal := (SD1->D1_TOTAL+SD1->D1_BAIVAGA+SD1->D1_BAIVAFR)
			            nProp:=(nVlrTotal/SF1->F1_Valmerc)
			            nVlrTotal:=nVlrTotal-(SF1->F1_DESCONT*nProp)
						
						//Verifica as caracteristicas do TES para verificar se houve
						//a incidencia de Percepcao de IIBB...
						aImpInf := TesImpInf(SD1->D1_TES)
						lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
						If !lTESNoExen
							SD1->(DbSkip())
							Loop
						EndIf
						aArea:=GetArea()
						aReaSFF:=SFF->(GetArea())
						SFF->(dbSetOrder(10)) //10
						SFF->(MsSeek(xFilial("SFF")+"IBR" +SD1->D1_CF+cProvEnt+aConfProv[4]))
				  		lCalImpos:=.F.
				  		nTotBase:=0
						If SFF->(Found())
						    lCalImpos:=.F.
							aAreaAtu:=GetArea()
							If SFF->(ColumnPos("FF_INCIMP")) > 0
								For nI := 1 To Len(aImpInf)
									If(Trim(aImpInf[nI][01])$ SFF->FF_INCIMP)
										lCalImpos:=.T.
									 	nTotBase+=SD1->(FieldGet(ColumnPos(aImpInf[nI][02])))
									Endif
			      				Next
			        	 	EndIf
								//Se cambian validaciones para obtener el tipo de retenci�n, independientemente de si SFF tiene un importe diferente de 0.
							If (CCO->(ColumnPos("CCO_IMMINR")) > 0 .and. CCO->(ColumnPos("CCO_TPMINR")) > 0)
								If SFF->FF_IMPORTE == 0 .Or. Empty(SFF->FF_IMPORTE)
									nAliq  := SFF->FF_ALIQ
									cZona  := SFF->FF_ZONFIS
									If SFF->FF_PRALQIB <> 0
										nPrAliqIB := SFF->FF_PRALQIB
									EndIf
								EndIf
								CCO->(dbSetOrder(1))	//CCO_FILIAL+CCO_CODPRO
								If CCO->(MsSeek(xFilial("CCO") + cProvEnt))
									If SFF->FF_IMPORTE == 0 .Or. Empty(SFF->FF_IMPORTE)
										If CCO->CCO_IMMINR <> 0
											nObtMin := IIF(!Empty(CCO->CCO_TPMINR),Val(CCO->CCO_TPMINR),0)
											nLimMInRet := CCO->CCO_IMMINR
											cOriMin := "CCO"
										Endif
									Else
										nCOTipMin := IIF(!Empty(CCO->CCO_TPMINR),Val(CCO->CCO_TPMINR),0)
									EndIf
								Endif
							Else 
								nObtMin = 0
							Endif
							
							CCO->(dbSetOrder(1))	//CCO_FILIAL+CCO_CODPRO
							If CCO->(MsSeek(xFilial("CCO") + cProvEnt)) .And. CCO->(ColumnPos("CCO_TPCALC")) > 0 .And. cPaisLoc == "ARG"
								cTipoCalc := Iif(CCO->CCO_TPCALC == "2", "2", "1")
								If cTipoCalc == "2"
									nRatValImp := 1
									lIIBBTotal := .T.
								EndIf
							Endif
							
							RestArea(aAreaAtu)
						EndIf

						RestArea(aArea)
						SFF->(RestArea(aReaSFF))

						If !lCalImpos
							For nI:=1 to Len(aImpInf)
								If "IB"$Trim(aImpInf[nI][01]) .And. Trim(aImpInf[nI][01])<>"IBR"
									If !Empty(SD1->(FieldGet(ColumnPos(aImpInf[nI][10]))))
										nPos := aScan(aPerIB,{|x| x[1]==cProvent .And.	x[2]==SD1->D1_CF .And. x[3]==aImpInf[nI][01]})
										If (nPos) == 0
											AAdd(aPerIB,{cProvEnt,SD1->D1_CF,aImpInf[nI][01],aImpInf[nI][09]})
										Else
											If aImpInf[nI][09] > aPerIB[nPos][04]
												aPerIB[nPos][04] := aImpInf[nI][09]
											EndIf
										EndIf
									EndIf
								EndIf

							Next nI
						EndIf
						
						nPosIb := Ascan(aZonaIb,{|X| X[1]==cProvEnt .And. X[2]=SD1->D1_CF})
						If CCO->(ColumnPos("CCO_TPRET")) > 0 .and. CCO->(ColumnPos("CCO_TPRENI")) > 0  
							If NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", aConfProv[1]) //Identifica tratamiento Retencion sujeto No inscripto
								aRes:= NoInsCCORet( aConfProv[1],SD1->D1_PROVENT)
								lCalNoIns := aRes[1][2]
								lNoIns := IIf(!Empty(aRes[1][1]),.T.,.F.)
								nPosIb := Iif(lNoIns == .T.,Ascan(aZonaIb,{|X| X[1]==SD1->D1_PROVENT .And. X[2]=SD1->D1_CF}),nPosIb)  
								If nPosIb == 0 .and. lNoIns == .T. 
									If (lCalNoIns == .T.) .or. (Len(aRes)>0 .and. aRes[1][1] == "2")  
										AAdd(aZonaIb,{cProvEnt,SD1->D1_CF,((nVlrTotal+nTotBase )* nRatValImp),aConfProv[4],,SD1->D1_PROVENT})
									Endif
								ElseIf nPosIb <> 0 .and. lNoIns == .T. 
									If lCalNoIns == .T. .or. (Len(aRes)>0 .and. aRes[1][1] == "2")
										aZonaIb[nPosIb][3] := aZonaIb[nPosIb][3]+((nVlrTotal+nTotBase )* nRatValImp)
									Endif 
								EndIf
							Else
								If nPosIb == 0
									AAdd(aZonaIb,{cProvEnt,SD1->D1_CF,((nVlrTotal+nTotBase )* nRatValImp),aConfProv[4],.f.,SD1->D1_PROVENT})
								Else
									aZonaIb[nPosIb][3] := aZonaIb[nPosIb][3]+((nVlrTotal+nTotBase )* nRatValImp)
								Endif
							EndIf 	
						EndIf
						nVlrTotal := 0
						SD1->(DbSkip())
					Enddo
				Else
					nPosIb := Ascan(aZonaIb,{|X| X[1]==cProvEnt .And. X[2]==SD1->D1_CF})
					AAdd(aZonaIb,{cProvEnt,SD1->D1_CF,nValMerc,aConfProv[4],,SD1->D1_PROVENT})
				Endif
			Else
				nPosIb := Ascan(aZonaIb,{|X| X[1]==cProvEnt .And. X[2]==SD1->D1_CF})
				AAdd(aZonaIb,{cProvEnt,SD1->D1_CF,nSaldo/1.21,aConfProv[4],,SD1->D1_PROVENT})
			Endif
		EndIf
		For nJ := 1 To Len(aZonaIb)
			lRet := .F.
			nBaseAtual := 0
			//Converter a base para moeda 1
			aZonaIb[nJ][3] := Round(xMoeda(aZonaIb[nJ][3],nMoeda,1,,5,aTxMoedas[nMoeda][2]),MsDecimais(1))

			cZona := IIF(nObtMin == 0,"",cZona)
			SFF->(dbSetOrder(10))
			SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][2]+aZonaIb[nJ][1]+aZonaIb[nJ][4]))
			
			//Si CCO_TPLIMR == 5, valida si FF_TPLIM es diferente de 0 para no considerar el calculo de limite mensual
			If SFF->(Found()) .And. cPaisLoc == "ARG" .And. SFF->(ColumnPos("FF_TPLIM")) > 0 .And. lCCOLimRet
				If !Empty(SFF->FF_TPLIM) .And. SFF->FF_TPLIM != "0" .and. aZonaIb[nJ][1]<> "ER"  
					lCCOLimRet := .F.
				EndIf
			EndIf
			
			If aZonaIb[nJ][4] == "V" .and. SFF->(Eof())  
				SFF->(MsSeek( xFilial() + "IBR" + aZonaIb[nJ][2] + aZonaIb[nJ][1] + "*" ) )
			Endif 
            
			//Verificar si tiene retencion adicional
			If  SFF->(Found()) .And. SFF->(ColumnPos("FF_CFORA")) > 0  
				nAliqRet  := SFF->FF_ALIQ
				lEsRetAdc :=ObtDesgl("IBR",SFF->FF_CFO_C,SFF->FF_ZONFIS,SFF->FF_TIPO,SFF->FF_CFORA,@nAliqAdc) //Obtener registro adicional                   
			Endif

			If aZonaIb[nJ][4] <> "M" .Or. (aZonaIb[nJ][4] == "M" .And. aZonaIb[nJ][1] <> "CF")
				If SFF->(Found())
					cZona := SFF->FF_ZONFIS //aZonaIb[nJ][1]
					If aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][4] == "N" .and. aZonaIb[nJ][6] <> "TU"
						nAliq := SFF->FF_ALQNOINS
					Else
						nAliq := SFF->FF_ALIQ
					EndIf
					nLimMInRet:= IIF(nObtMin == 0,SFF->FF_IMPORTE,nLimMInRet)
					cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
					If lCCOLimRet
						nLimiteSFF := SFF->FF_LIMITE
					EndIf
					If SFF->FF_PRALQIB <> 0
						nPrAliqIB := SFF->FF_PRALQIB
					EndIf
				Else
					SFF->(dbSetOrder(11))
					If aZonaIb[nJ][1] == "CF" .And. aZonaIb[nJ][4] == "I"
						If SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]+aZonaIb[nJ][4]+Replicate("*",TamSX3("FF_CFO_C")[1]) ))
							cZona := aZonaIb[nJ][1]
							nAliq := SFF->FF_ALIQ
							nLimMInRet:= IIF(nObtMin == 0,SFF->FF_IMPORTE,nLimMInRet)
							cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
							If lCCOLimRet
								nLimiteSFF := SFF->FF_LIMITE
							EndIf
							If SFF->FF_PRALQIB <> 0
								nPrAliqIB := SFF->FF_PRALQIB
							EndIf
						EndIf
					Else
						If SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]+Replicate("*",TamSx3("FF_TIPO")[1])+Replicate("*",TamSX3("FF_CFO_C")[1]) ))
							cZona := aZonaIb[nJ][1]
							If aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][4] == "N" .and. aZonaIb[nJ][6] <> "TU"
								nAliq := SFF->FF_ALQNOINS
							Else
								nAliq := SFF->FF_ALIQ
							EndIf
							nLimMInRet:= IIF(nObtMin == 0,SFF->FF_IMPORTE,nLimMInRet)
							cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
							If lCCOLimRet
								nLimiteSFF := SFF->FF_LIMITE
							EndIf
							If SFF->FF_PRALQIB <> 0
								nPrAliqIB := SFF->FF_PRALQIB
							EndIf
						EndIf
					EndIf
				EndIf
			Elseif aZonaIb[nJ][1] =="CF" .And. aZonaIb[nJ][4] == "M"

				If SFF->FF_TIPO == 'M'

					cItem := SFF->FF_ITEM
					cZona := SFF->FF_ZONFIS
					nAliq := SFF->FF_ALIQ
					nLimMInRet:=  IIF(nObtMin == 0,SFF->FF_IMPORTE,nLimMInRet)
					cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
					If lCCOLimRet
						nLimiteSFF := SFF->FF_LIMITE
					EndIf
					If SFF->FF_PRALQIB <> 0
						nPrAliqIB := SFF->FF_PRALQIB
					EndIf

					nMinimo  	:= Iif(SFF->(ColumnPos("FF_LIMITE"))>0,SFF->FF_LIMITE,0)
					nMinUnit 	:= Iif(SFF->(ColumnPos("FF_MINUNIT"))>0,SFF->FF_MINUNIT,0)

					aAreaSFF := SFF->(GetArea())
					//nRecSFF := Recno()

					SFF->(dbSetOrder(11))
					SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]))

					//Array contendo todos os CFOs com a mesma classifica玢o
					While SFF->(!Eof()) .And. SFF->FF_IMPOSTO == "IBR"
						If SFF->FF_TIPO == 'M' .And. SFF->FF_ITEM == cItem .And. SFF->FF_ZONFIS == cZona
							If aScan(aCf,{|x| x[1] == SFF->FF_CFO_C}) == 0
								aAdd(aCf,{SFF->FF_CFO_C,SFF->FF_CFO_V})
							Endif
						Endif
						SFF->(dbSkip())
					Enddo

					//dbGoTo(nRecSFF)
			 		SFF->(RestArea(aAreaSFF))
					SFH->(DbSetOrder(1))
					SFH->(MsSeek(xFilial()+SA2->A2_COD+SA2->A2_LOJA+"IBR"+cZona))
					If SFH->(FOUND()) .And. (SFH->FH_ISENTO == "N" .And. A085aVigSFH())
				 		lCalcMon:= .T.
					Else
						//Verifica se deve calcular IB
						lCalcMon := F850CheckLim(cItem,aCf,SF1->F1_FORNECE,nMinimo,SF1->F1_DOC,PadR(SF1->F1_SERIE,nTamSer),nMinUnit,"IB",,1,Iif(lMsFil,SF1->F1_MSFIL,""))
					EndIf

				Endif
  			Endif
  			If aZonaIb[nJ][1] $ "CR|ME|"
				nTotRetSFE:=0
				nTotBasSFE:=0
				ltpLimVa := ( ( SFF->(ColumnPos("FF_TPLIM")) > 0 .and. SFF->FF_ZONFIS $ "ME") .and. ;
							   (EMPTY(SFF->FF_TPLIM) .OR. SFF->FF_TPLIM == " " .or. SFF->FF_TPLIM == "0") )

  				If aZonaIb[nJ][1] == "CR" .Or. ( SFF->(ColumnPos("FF_TPLIM")) > 0 .and. SFF->FF_TPLIM == "5" .and. SFF->FF_ZONFIS $ "ME" )
  					lSmRMCR := .T.
  					nLmRMCR := IIF(SFF->(ColumnPos("FF_LIMITE")) > 0,SFF->FF_LIMITE,0)
					SFE->(dbSetOrder(4))
					If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA))
						cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
						While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
							If  SFE->FE_EST <> aConfProv[1] .or. Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
								!(SFE->FE_TIPO $"B")
								SFE->(dbSkip())
								Loop
							EndIf
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							SFE->(dbSkip())
						End
					EndIf
				Endif 
				
				If aZonaIb[nJ][1] == "CR"				
					nBaseAtual:= (nTotBasSFE + (aZonaIb[nJ][3]*nPropImp)*nSigno)
				ElseIf aZonaIb[nJ][1] == "ME" .and. lExisCGF == .T.
					aAreaAnt := GetArea()										
					dbSelectArea("CGF")
					dbSetOrder(2) // CGF_FILIAL+CGF_COD+CGF_LOJA+CGF_ZONFIS+CGF_IMPOST+CGF_CFO+CGF_CODACT
					If CGF->(MsSeek(XFILIAL("CGF") + SA2->A2_COD + SA2->A2_LOJA + aZonaIb[nJ][1]+"IBR"+aZonaIb[nJ][2]))
						If (EMPTY(CGF->CGF_FIMVIGE) .or. CGF->CGF_FIMVIGE > SE2->E2_EMISSAO)
							aZonaIb[nJ][5] := .T. // Parametro para indicar que este excenta retencion 
						Endif
					Endif
					Restarea(aAreaAnt)						
				Endif 
			EndIf
			
			If lCCOLimRet
				SFE->(dbSetOrder(4))
				If SFE->(MsSeek(xFilial("SFE") + SF1->F1_FORNECE + SF1->F1_LOJA))
					cChaveSFE := SFE->FE_FILIAL + SFE->FE_FORNECE + SFE->FE_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE") + SFE->FE_FORNECE + SFE->FE_LOJA
						If SFE->FE_EST != aConfProv[1] .Or. Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or. Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. SFE->FE_TIPO != "B"
							SFE->(dbSkip())
							Loop
						EndIf
						nTotRetSFE += SFE->FE_RETENC
						nTotBasSFE += SFE->FE_VALBASE
						SFE->(dbSkip())
					End
				EndIf 
			EndIf

			nDeduc := 0
			nCofRet := 0

			If IIF(nObtMin = 0,!Empty(cZona),.T.) .And. (aZonaIb[nJ][4] <> "M" .Or. (aZonaIb[nJ][4] == "M" .And. lCalcMon) .Or. (aZonaIb[nJ][4] == "M" .And. aZonaIb[nJ][1] <> "CF") .or. (lCalNoIns))

				//****************************************************
				// SE obtienen los registros de la SFH para el proveedor
				cTabTemp := criatrab(nil,.F.)
				cQuery := "SELECT * "
				cQuery += "FROM " + RetSqlName("SFH")+ " SFH "
				cQuery += "WHERE FH_FORNECE='" + SA2->A2_COD + "' AND "
				cQuery += "FH_LOJA='"  + SA2->A2_LOJA + "' AND "
				cQuery += "FH_IMPOSTO='IBR' AND "
				cquery += "FH_ZONFIS='" + cZona +"' AND "
				If cPaisLoc == "ARG"
					cQuery += "FH_FILIAL='" +XFILIAL("SFH") + "' AND "
				Else
					If !Empty(xFilial("SFH"))  .and. !Empty(xFilial("SA2"))
						cQuery += "FH_FILIAL='" +XFILIAL("SA2") + "' AND "
					ElseIf !Empty(xFilial("SFH")) .and. !Empty(xFilial("SE2"))
						cQuery += "FH_FILIAL='" +XFILIAL("SE2") + "' AND "
					Endif
				EndIf                                                            
				
				cquery += "((FH_FIMVIGE>='"+Dtos(dDataBase)+"' AND "									      
				cquery += "FH_INIVIGE<='"+Dtos(dDataBase)+"') OR "                                            
				
				cquery += "(FH_FIMVIGE=' ' AND "									      
				cquery += "FH_INIVIGE=' ') OR "
				
				cquery += "(FH_FIMVIGE=' ' AND "									      
				cquery += "FH_INIVIGE<='"+Dtos(dDataBase)+"') OR "      
				
				cquery += "(FH_FIMVIGE>='"+Dtos(dDataBase)+"' AND "									      
				cquery += "FH_INIVIGE=' '))"      
				
				cQuery += " AND "
				cQuery += "D_E_L_E_T_<>'*'"
				cQuery += "ORDER BY FH_INIVIGE,FH_FIMVIGE"

				cQuery := ChangeQuery(cQuery)

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTabTemp,.T.,.T.)
				TCSetField(cTabTemp,"FH_INIVIGE","D")
 				TCSetField(cTabTemp,"FH_FIMVIGE","D")

				Count to nNumRegs

				If nNumRegs > 0 // Se verifica que existan registros para el proveedor en SFH
					(cTabTemp)->(dbGoTop())
					WHILE (cTabTemp)->(!eof())   .And. ;
						(cTabTemp)->FH_FORNECE==SA2->A2_COD .AND. (cTabTemp)->FH_LOJA == SA2->A2_LOJA .And. (cTabTemp)->FH_ZONFIS == cZona .And. !lRet

						If lNoIns .Or. !((dDatabase >=(cTabTemp)->FH_INIVIGE .Or. Empty((cTabTemp)->FH_INIVIGE) ) .And. ;
							(dDatabase <= (cTabTemp)->FH_FIMVIGE .Or. Empty((cTabTemp)->FH_FIMVIGE))) 
							(cTabTemp)->(dbskip())
							Loop
						Else
							nAliqAux := nAliq 
						EndIf

						If (cTabTemp)->FH_ZONFIS=="ME"
							lThdezb:=.f.
							If aZonaIb[nJ][5] == .T. // Verificamos si es excento
								nCoefmul := (cTabTemp)->FH_COEFMUL
								nAliqAux := 0
								nPercTot :=(100 - (cTabTemp)->FH_PERCENT) /100	 // % de Exencion del Impuesto	
								lIsento  := .t.
								lThdezb := .t.
								cSituaca := (cTabTemp)->FH_SITUACA
							ElseIf (cTabTemp)-> FH_SITUACA="2" .and. (EMPTY((cTabTemp)->FH_FIMVIGE) .or. SE2->E2_EMISSAO <= (cTabTemp)->FH_FIMVIGE)	
								nAliqAux  := ((cTabTemp)->FH_ALIQ ) // (SFF->FF_ALIQ * 2)
								nBaseAtual:= (nTotBasSFE + (aZonaIb[nJ][3]*nPropImp))*nSigno
								nCoefmul := (cTabTemp)->FH_COEFMUL
								nPercTot :=(100 - (cTabTemp)->FH_PERCENT) /100	 // % de Exencion del Impuesto	
								lIsento  := IIF((cTabTemp)->FH_ISENTO=="S" .OR. (cTabTemp)->FH_PERCENT == 100 ,.T.,.F.)
								lThdezb := .t.
								cSituaca := (cTabTemp)->FH_SITUACA
							Else
								nBaseAtual:= (nTotBasSFE + (aZonaIb[nJ][3]*nPropImp))*nSigno
							Endif
						Endif 
						
						If lThdezb == .f.
						If !Empty((cTabTemp)->FH_INIVIGE) .And. !Empty((cTabTemp)->FH_FIMVIGE)
							IF(dDatabase >=(cTabTemp)->FH_INIVIGE ) .AND. dDatabase <= (cTabTemp)->FH_FIMVIGE
								nCoefmul := (cTabTemp)->FH_COEFMUL
								nAliqaux := (cTabTemp)->FH_ALIQ
								lIsento  := IIF((cTabTemp)->FH_ISENTO=="S"  .OR. (cTabTemp)->FH_PERCENT == 100,.T.,.F.)
								nPercTot :=(100 - (cTabTemp)->FH_PERCENT) /100	 // % de Exencion del Impuesto								
								lRet := .T.
								cSituaca := (cTabTemp)->FH_SITUACA
								cZonaSFH := (cTabTemp)->FH_ZONFIS
								cTipoSFH := (cTabTemp)->FH_TIPO
							EndIF
						Else
							If Empty((cTabTemp)->FH_INIVIGE) .and. Empty((cTabTemp)->FH_FIMVIGE)
								nCoefmul := (cTabTemp)->FH_COEFMUL
								nAliqaux := (cTabTemp)->FH_ALIQ
								lIsento  := IIF((cTabTemp)->FH_ISENTO=="S" .OR. SFH->FH_PERCENT == 100,.T.,.F.)
								nPercTot :=(100 - (cTabTemp)->FH_PERCENT) /100	 // % de Exencion del Impuesto
								cSituaca := (cTabTemp)->FH_SITUACA
								cZonaSFH := (cTabTemp)->FH_ZONFIS
								cTipoSFH := (cTabTemp)->FH_TIPO								
							Else
								If (dDatabase >= (cTabTemp)->FH_INIVIGE .and. !Empty((cTabTemp)->FH_INIVIGE)) .or. (dDatabase <= (cTabTemp)->FH_FIMVIGE .and.  !Empty((cTabTemp)->FH_FIMVIGE))
									nAliqaux := (cTabTemp)->FH_ALIQ
									lIsento  := IIF((cTabTemp)->FH_ISENTO=="S"  .OR. (cTabTemp)->FH_PERCENT == 100,.T.,.F.)
									nPercTot :=(100 - (cTabTemp)->FH_PERCENT) /100	 // % de Exencion del Impuesto										
									nCoefmul := (cTabTemp)->FH_COEFMUL
									lRet := .T.
									cSituaca := (cTabTemp)->FH_SITUACA
									cZonaSFH := (cTabTemp)->FH_ZONFIS
									cTipoSFH := (cTabTemp)->FH_TIPO
								EndIf
							EndIf
						Endif
						Endif	
						
						If CCO->(ColumnPos("CCO_RPROAG")) > 0 .And. (cTabTemp)->FH_IMPOSTO ==  "IBR" .And. cPaisLoc == "ARG"
							If CCO->(MsSeek(xFilial("CCO")+(cTabTemp)->FH_ZONFIS))
 								If CCO->CCO_CODPRO == (cTabTemp)->FH_ZONFIS .And. CCO->CCO_AGRET == "1" .And. CCO->CCO_RPROAG == "N" .And. (cTabTemp)->FH_AGENTE == "S"
									lAgRetCCO := .F.
								EndIf
							EndIf
						EndIf
						
						(cTabTemp)->(dbskip())
					EndDo

					IF nAliqAux <> 0
						lRet := .T.
					EndIF

					If lRet .and. !lIsento .AND. nAliqAux <> 0	// Si se encontr?un registro y no es excento, se toma el valor de al韖uota.
						If cSituaca == "2" .and. CCO->(ColumnPos("CCO_CRFRET")) > 0
							nAliq := nAliqAux * (1+(POSICIONE("CCO",1,xFilial("CCO")+cZOna,"CCO_CRFRET"))/100)
						ElseIf cZonaSFH == "TU" .and. cTipoSFH == "V" .and. nAliqAux <> 0 
							If aZonaIb[nJ][6] <> "TU" .and. SA2->A2_EST <> "TU" .and. nCoefMul <> 0
								nAliq := (nAliqAux * Iif (nPrAliqIB<>1,1-(nPrAliqIB/100),nPrAliqIB) * nCoefmul)
							Else
								nAliq := nAliqAux
							EndIf
						ElseIf cZonaSFH == "TU" .and. cTipoSFH == "N"
							nAliq := 0
						Else
							nAliq := nAliqAux * Iif (nPrAliqIB<>1,1-(nPrAliqIB/100),nPrAliqIB)
						Endif 
					ElseIf lRet .and. !lIsento .AND. nAliqAux == 0
						If cZonaSFH == "TU" .and. aZonaIb[nJ][6] <> "TU" .and. cTipoSFH == "V" .and. SA2->A2_EST <> "TU"
							If CCO->(ColumnPos("CCO_CRFRET")) > 0
								nCRFRET := POSICIONE("CCO",1,xFilial("CCO")+cZOna,"CCO_CRFRET")
							EndIf
							If nCRFRET <> 0
								nAliq :=  (nAliq * nCRFRET) 
							EndIf
						ElseIf nPrAliqIB<>1
							nAliq := nAliq * (1-(nPrAliqIB/100))
						EndIf
					EndIf
					//Aplica % de Reducao para Convenio Multilateral...     
	  				If cZonaSFH <> "TU"
						nDeduc := aZonaIb[nJ][3]
						nCofRet := nCoefMul		
						aZonaIb[nJ][3] := aZonaIb[nJ][3] *nPercTot
						If nCoefmul <> 0
							aZonaIb[nJ][3] := aZonaIb[nJ][3] * (nCoefmul/100) 
						EndIf                                                           
	  					nDeduc -= aZonaIb[nJ][3]
					EndIf


				EndIf
				(cTabTemp)->(dbCloseArea())
                
				//Verificar si es necesario desglosar la aliquiota
                If  lEsRetAdc
                	nAliq := nAliqAdc + nAliqRet
                Endif

				If !Empty(lPA) .and. lPA
					lGravoPa:= Iif(lCalcPa .or. (lCalPaIns .and. (nNumRegs > 0 .or. lCalNoIns)),.T.,.F.)
				EndIf
				
				//****************************************************
				If !lIsento .and. lGravoPa
					IF TYPE ("aProvLim") == "A"
						nPsLmPv := Ascan(aProvLim, {|xT| xT[1] == aZonaIb[nJ][1]})
					Else
						nPsLmPv:=0
					EndIf
					If nPsLmPv > 0
						lLimPrim := aProvLim[nPsLmPv][2]
					EndIf

					If SFF->FF_REDBASE <> 0 .and. aZonaIb[nJ][3] <> 0
						aZonaIb[nJ][3] := (aZonaIb[nJ][3] * (( 100 - SFF->FF_REDBASE ) / 100 ) )
					Endif
					
					//Si el IIBB se controla por tipo 2 (total) 
					If cTipoCalc == "2" .And. cPaisLoc == "ARG"
						SFE->(dbSetOrder(4))
						nRetSFE:=0
						If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
							DBSELECTAREA("FVC")
							FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
							If FVC->(MsSeek(xFilial("FVC")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC))
								While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF1->(F1_FORNECE+F1_LOJA+F1_DOC)
									If AllTrim(FVC->FVC_TIPO) $ "B" .and. FVC->FVC_RETENC > 0 .and. FVC->FVC_SERIE == SF1->F1_SERIE .and. cZona == FVC->FVC_EST
										AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
										nRetSFE +=  FVC->FVC_RETENC
									EndIf
									FVC->(dbSkip())
								Enddo
							EndIf	
						EndIf
						//Cuenta el total de registros en SFE, correspondiente a �rdenes de pago anteriores.
						If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE+"B")) 
							cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SF1->F1_DOC+SF1->F1_SERIE+"B"
							While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_TIPO
								If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
									!(SFE->FE_TIPO $ "B") .Or. cZona != SFE->FE_EST .Or. Iif(!Empty(SFE->FE_DTRETOR), Month(SFE->FE_DTRETOR) != Month(dDataBase),.F.)
									SFE->(dbSkip())
									Loop
								EndIf
								If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
									If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
										nRetSFE += SFE->FE_RETENC
									EndIf
								Else
									nRetSFE += SFE->FE_RETENC
								EndIf
								SFE->(dbSkip())
							End
						EndIf
						
						//Impostos recalculados na tela - Modificar OP
						//Impostos calculados - IVA, IB...
						nImpCalc := 0
						If Len(aImpCalc) > 0
							For aX := 1 to Len(aImpCalc[nLinha])
								If ValType(aImpCalc[nLinha][aX]) == "A"
									nImpCalc += aImpCalc[nLinha][aX][2]
								EndIf
							Next
							
							For aX := 1 to Len(aSFEIb)
								If ValType(aSFEIb[aX]) == "A"
									nImpCalc += aSFEIb[aX][6]
								EndIf
							Next
							
							//Ganancia
							If Len(aImpCalc[Len(aImpCalc)]) > 0 .And. valtype(aImpCalc[Len(aImpCalc)][1]) <> "A"  .And. aImpCalc[Len(aImpCalc)][1] == "GAN"
								nImpCalc += aImpCalc[Len(aImpCalc)][2]
							Endif
						End
						
						//Se calcula si la diferencia entre los pagos y el total a pagar. Se considera limites con calculo en CCO
						If lCCOLimRet .And. nLimiteSFF > 0 .And. lLimPrim
							//FE_VALIMP
							nValorRet := Round(((nTotBasSFE + aZonaIb[nJ][3]) * (nAliq / 100)) - nTotRetSFE,TamSX3("FE_VALIMP")[2])
							aProvLim[nPsLmPv][2] := .F.
						Else
							nValorRet := Round((((aZonaIb[nJ][3] * nPropImp ) * nSigno) * (nAliq / 100)) - nRetSFE, TamSX3("FE_VALIMP")[2])
						EndIf

						If aZonaIb[nJ][1] $ "CR|ME|"
							nValorRet := Round((nBaseAtual  * (nAliq/100)) - nRetSFE,TamSX3("FE_VALIMP")[2])  //FE_VALIMP
						EndIf 
						
						//Si a�n existe un monto a pagar, se valida que el saldo sea suficiente para cubrir el monto, de lo contario se calcula solo lo que es posible cubrir o no se cubre en lo absoluto.
						If nValorRet != 0
							If (nSaldo-nImpCalc) < nValorRet
								nValorRet := (nSaldo-nImpCalc)
								lAjusta := .T. 
							EndIf 
						EndIf 
						
						//Se valida que no se envi� un monto menos que cero.
						If nValorRet < 0
							nValorRet	:= 0
						Endif
					EndIf
					
					If cTipoCalc == "2" .And. nValorRet != 0 .And. cPaisLoc == "ARG"
						nRetencao := nValorRet //FE_VALIMP
					ElseIf cTipoCalc == "2" .And. nValorRet == 0 .And. cPaisLoc == "ARG"
						lAgRetCCO := .F.
						nRetencao := 0
					Else
						nRetencao := Round((((aZonaIb[nJ][3]*nPropImp)*nSigno)*(nAliq/100)),TamSX3("FE_VALIMP")[2])  //FE_VALIMP
						If aZonaIb[nJ][1] $ "CR|ME|"
							nRetencao := Round((nBaseAtual * (nAliq/100)) - nTotRetSFE,TamSX3("FE_VALIMP")[2])  //FE_VALIMP
						EndIf
						If lCCOLimRet .And. nLimiteSFF > 0 .And. lLimPrim
							//FE_VALIMP
							nRetencao := Round((((nTotBasSFE) + aZonaIb[nJ][3]) * (nAliq / 100)) - nTotRetSFE,TamSX3("FE_VALIMP")[2])
							aProvLim[nPsLmPv][2] := .F.  
						EndIf
					EndIf
					
					If aZonaIb[nJ][1] $ "ER"
						nRetencao := Round((((nTotBasSFE) + aZonaIb[nJ][3]) * (nAliq / 100)) - nTotRetSFE,TamSX3("FE_VALIMP")[2])
					EndIf 

					If (SFF->FF_PRALQIB <> 0) .And. (Len(aPerIB) > 0) .and. !(aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][4] == "V")

						If aScan(aPerIB,{|x| X[1]==aZonaIb[nJ][1] .And. X[2]==aZonaIb[nJ][2] .And. x[4]<nAliq}) > 0
							nAliq := nAliq * (SFF->FF_PRALQIB/100)
						EndIf
					EndIf
					If !lAjusta
						nRetencao := Round((((aZonaIb[nJ][3]*nPropImp)*nSigno)*(nAliq/100)),TamSX3("FE_VALIMP")[2])//FE_VALIMP  
					Endif
					
					If nSaldo == 0
						lAgRetCCO := .F.
					EndIf
					
					Aadd(aSFEIb,Array(33))
					aSFEIb[Len(aSFEIb)][1] := SE2->E2_NUM                   //FE_NFISCAL
					aSFEIb[Len(aSFEIb)][2] := SE2->E2_PREFIXO               //FE_SERIE
					aSFEIb[Len(aSFEIb)][3] := (aZonaIb[nJ][3]*nPropImp)*nSigno        //FE_VALBASE
					aSFEIb[Len(aSFEIb)][4] := nAliq
					
					
					If aZonaIb[nJ][1] $ "CR|ME|"
						If aZonaIb[nJ][1] == "ME" .and. ltpLimVa
							aSFEIb[Len(aSFEIb)][5] := nRetencao
						ElseIf aZonaIb[nJ][1] == "CR" .or. lSmRMCR = .F.
							aSFEIb[Len(aSFEIb)][5] := nRetencao
						Else 
							aSFEIb[Len(aSFEIb)][5] := IIf((nLmRMCR > nTotBasSFE) .and. lSmRMCR,0,nRetencao)
						Endif
					ElseIf lLimNRet
						nRetencao := 0
						aSFEIb[Len(aSFEIb)][5] := nRetencao 
					Else
						aSFEIb[Len(aSFEIb)][5] := nRetencao
					Endif
					
					aSFEIb[Len(aSFEIb)][6] := aSFEIb[Len(aSFEIb)][5]        //FE_RETENC
					aSFEIb[Len(aSFEIb)][7] := nSaldo //SE2->E2_VALOR
					aSFEIb[Len(aSFEIb)][8] := SE2->E2_EMISSAO
					aSFEIb[Len(aSFEIb)][9] := cZona
					aSFEIb[Len(aSFEIb)][10]:= SE2->E2_MOEDA
					aSFEIb[Len(aSFEIb)][11]:= SFF->FF_CFO_C   //CFO - Compra
					aSFEIb[Len(aSFEIb)][12]:= SFF->FF_CFO_V   //CFO - Venda
					aSFEIb[Len(aSFEIb)][13]:= SE2->E2_TIPO
					aSFEIb[Len(aSFEIb)][14]:= SFF->FF_CONCEPT
					aSFEIb[Len(aSFEIb)][15]:= nDeduc
					aSFEIb[Len(aSFEIb)][16]:= nCofRet  
					aSFEIb[Len(aSFEIb)][17]:= lEsRetAdc
					aSFEIb[Len(aSFEIb)][18]:= Iif(lEsRetAdc,SFF->FF_CFORA,"")   
					aSFEIb[Len(aSFEIb)][19]:= nAliqRet    
					aSFEIb[Len(aSFEIb)][20]:= nAliqAdc
					aSFEIb[Len(aSFEIb)][21]:= nLimMInRet
					aSFEIb[Len(aSFEIb)][22]:= nObtMin // De donde se obtuvo el minimo
					aSFEIb[Len(aSFEIb)][23]:= Iif(aZonaIb[nJ][1] $ "CR|ME|",nBaseAtual,0) // Caso Mendoza
					aSFEIb[Len(aSFEIb)][24]:= 0
					aSFEIb[Len(aSFEIb)][25]:= cOriMin
					aSFEIb[Len(aSFEIb)][26]:= nLimMInRet // Monto del m�nimo, para calculo de m�nimos por OP
					aSFEIb[Len(aSFEIb)][27]:= Iif(cOriMin == "SFF", nCOTipMin, nObtMin) // De donde se obtuvo el m�nimo, para calculo de m�nimos por OP
					aSFEIb[Len(aSFEIb)][28]:= Iif(!(aConfProv[1] $ ("SF|BA|CO")), 0, nTotCCO3) //Base imponible + impuestos solamente cuando es para SF.
					aSFEIb[Len(aSFEIb)][29]:= aConfProv[6]
					aSFEIb[Len(aSFEIb)][30]:= lCCOLimRet .And. nLimiteSFF > 0 //Si se considerara el limite por CCO.
					aSFEIb[Len(aSFEIb)][31]:= nLimiteSFF //Monto del limite a considerarse
					aSFEIb[Len(aSFEIb)][32]:= nTotRetSFE //Monto retencion limite
					aSFEIb[Len(aSFEIb)][33]:= nTotBasSFE //Monto base imponible limite.
				Endif
			Endif	
		Next nJ
	EndIf
EndIf
RestArea(aArea)

If !lAgRetCCO .And. cPaisLoc == "ARG" .Or. (cTipoCalc == "2" .And. !lSUSSPrim .And. cPaisLoc == "ARG") 
	aSFEIB := {}
	If TYPE ("aProvLim") == "A" .And.  Len(aProvLim) > 0 .And. nPsLmPv > 0
		aProvLim[nPsLmPv][2] := lLimPrim
	EndIf
EndIf

Return aSFEIB

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetIB2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetIB2    - Calculo de Ret de IIB para NF               ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetIB2(cAgente,nSigno,nSaldo,nPropImp,aConfProv,lSUSSPrim,lIIBBTotal,aImpCalc,aSUSS,nLinha,lLimNRet,cChavePOP,cNFPOP,cSeriePOP,dEmissao)

Local nRatValImp,nRateio,nValMerc,cZona,nJ,nPosIb,cEmpAct
Local nPosRet,nRecSM0
Local aZonaIb  	:= {}
Local aSFEIB    := {}
Local aPerIB    := {}
Local aProvVerif:= {}
Local aImpInf 	:= {}
Local aCF 		:= {}
Local aAreaSFF 	:= {}
Local aFil		:= {}
Local aRes      := {}
Local lRet 		:= .F.
Local lIsento 	:= .F.
Local lFoundFh 	:= .F.
Local lCalcIb	:= .T.
Local lCalcImp 	:= .T.
Local lCalcMon	:= .F.
Local lCalImpos	:= .F.
Local lPropIB	:= Iif(GetMV("MV_PROPIB",.F.,1)==2,.T.,.F.)
Local lNoIns    := .F.
Local lCalNoIns := .F.
Local ltpLimVa  := .F.
Local cProvEnt  := ""
Local cChave    := ""
Local cCpoCFO   := ""
Local cItem		:= ""
Local cSituaca  := ""
Local nI		:= 0
Local aI, aX		:= 0
Local nTotRetSFE:= 0
Local nTotBasSFE:= 0
Local nLimMInRet:= 0
Local nImposto	:= 0
Local nBaseAtual:= 0
Local nImpAtual	:= 0
Local nRetencao	:= 0
Local nMoeda    := 1
Local nAliq     := 0
Local nVlrTotal := 0
Local nPos      := 0
Local nAliqaux	:= 0
Local nCoefmul	:= 0
Local nPercTot	:= 0
Local nTotBase	:= 0
Local nMinimo 	:= 0
Local nMinunit 	:= 0
Local nParcela	:= 0
Local nDeduc	:= 0
Local nCofRet	:= 0
Local nAliqRet 	:= 0 
Local nAliqAdc 	:= 0 
Local nObtMin 	:= 0
Local cZonaSFH := ""
Local cTipoSFH := ""
Local lAgRetCCO := .T.
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.
Local cTipoCalc 		:= ""
Local nValorRet 		:= 0
Local nImpCalc 		:= 0
Local nCRFRET   		:= 0
Local cOriMin 		:= ""
Local aRetPreO		:= {} 
Local nCOTipMin 		:= 0
Local nTotCCO3 		:= nSaldo
Local lIsPA 			:= .F.
Local nRecSM0 		:= 0
Local lCCOLimRet := Iif(aConfProv[7] <> " ", .T., .F.)
Local cChaveSFE := ""
Local nLimiteSFF := 0
Local lLimPrim := .F.
Local nPrAliqIB := 1
Local lBxParcial := Iif(SE2->E2_SALDO < SE2->E2_VALOR,.T.,.F.)
Local cTabTemp	:= ""
Local cQuery 		:= ""
Local nRecnoSFH	:= 0

If	type("cFornece")=="U"
	cFornece :=SE2->E2_FORNECE
	cLoja:=SE2->E2_LOJA
EndIf

DEFAULT nSigno		:= -1
DEFAULT nPropImp	:= 1
DEFAULT aConfProv	:= {}
DEFAULT lSUSSPrim		:= .T.
DEFAULT lIIBBTotal 	:= .F.
DEFAULT aImpCalc 		:= {}
DEFAULT aSUSS 		:= {}
DEFAULT nLinha		:= 0
DEFAULT lLimNRet := .F.     
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIB := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"B",nSaldo,dEmissao,aConfProv)
EndIf                                        


lEsRetAdc := .F.

//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten噭o.?

If !Empty(GetMV("MV_AGIIBB",,"CF|BA|SF|SE|TU|SA|JU|MI|FO|ME|ER|SJ|LR|CO|CB|CA|NE|LP|TF|CH|RN|CR")) //.And. nSigno > 0
	SA2->( dbSetOrder(1) )
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SA2")
		SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Else
		SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Endif
	
	//?Generar las Retenci de Ingresos Brutos                       ?
	//?Reter Ingressos Brutos somente se valor total da Orden de Pago ?
	//?for igual ou maior que $400,00.                                ?
	

	If SA2->(ColumnPos("A2_DTICALB")) > 0 .And. SA2->(ColumnPos("A2_DTFCALB")) > 0 ;
	   .And. !Empty(SA2->A2_DTICALB) .And. !Empty(SA2->A2_DTFCALB)
	    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALB) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALB) )
	   		lCalcIb:=.F.
	    EndIf
	EndIf

	If ExistBlock("F0851IMP")
		lCalcIb:=ExecBlock("F0851IMP",.F.,.F.,{"IB2"})
	EndIf
	If lCalcIb .And. SA2->A2_RETIB == "S"

		dbSelectArea("SF2")
		dbSetOrder(1)
		If lMsFil
			nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
			SF2->(dbGoTo(nRecSF2))
		Else
			nRecSF2 := FINBuscaNF(xFilial("SF2",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
			SF2->(dbGoTo(nRecSF2))
		EndIf
		If nRecSF2 > 0
			cChave := SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA
			If cPaisLoc<>"ARG"
				nRateio := 1
			else
				nRateio     := SF2->F2_VALMERC / SF2->F2_VALBRUT
			EndIf

			nRatValImp  := Iif(aConfProv[4] == "M" .And. !lPropIB,1,( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) ) )
			nValMerc    := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
			nMoeda   := Max(nMoeda,SF2->F2_MOEDA)

			SD2->(DbSetOrder(3))
			If lMsFil
				SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			Else
				SD2->(MsSeek(xFilial("SD2",SF2->F2_FILIAL)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			EndIf
			If SD2->(Found())
				nObtMin := 0
				Do while Iif(lMsFil,SF2->F2_MSFIL,xFilial("SD2",SF2->F2_FILIAL))==SD2->D2_FILIAL .And. SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA == cChave
				 
					If Alltrim(SD2->D2_ESPECIE) <> Alltrim(SF2->F2_ESPECIE) // .or. SD2->D2_PROVENT <> aConfProv[1]
						SD2->(DbSkip())
						Loop
					Endif

					If NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", aConfProv[1]) //Identifica tratamiento Retencion sujeto No inscripto
						aRes:= NoInsCCORet( aConfProv[1],SD2->D2_PROVENT)
						If (aRes[1][1] = "2" .and. SD2->D2_PROVENT <> aConfProv[1]) .Or. (aConfProv[1] == "TU" .and. SA2->A2_EST <> "TU")
							SD2->(DbSkip())
							Loop
						Endif
						aRes := {}
					Else 
						aRes := {}
					Endif 

					cProvEnt := ""

					//Por Entrega
					If aConfProv[2] == "1" .And. aConfProv[3] == "1"
						If SD2->(ColumnPos("D2_PROVENT")) >0 .And. !Empty(SD2->D2_PROVENT) .And. aConfProv[1] == SD2->D2_PROVENT
							cProvEnt := SD2->D2_PROVENT
						ElseIf  SF2->(ColumnPos("F2_PROVENT")) >0 .And.  !Empty(SF2->F2_PROVENT) .And. aConfProv[1] == SF2->F2_PROVENT .And. Empty(SD2->D2_PROVENT)
							cProvEnt := SF2->F2_PROVENT
						Endif
					//Por Pago
					ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "2"
						SX5->(MsSeek(xFilial()+"74"))
						cSucur := IIf( lFWCodFil, FWGETCODFILIAL, SM0->M0_CODFIL )
						While !SX5->(EOF())  .And. SX5->X5_TABELA=="74"
							If SD2->D2_LOCAL  $ SX5->(X5DESCRI())
								cSucur := SUBS(SX5->X5_CHAVE,3,2)
								Exit
							Endif
							SX5->(DbSkip())
						EndDo

						nRecSM0 := SM0->(RecNo())
						cEmpAct := SM0->(M0_CODIGO)

						If SM0->(MsSeek(cEmpAct+cSucur))
							If SM0->(FOUND()) .And. aConfProv[1] == SM0->M0_ESTENT
								cProvEnt := SM0->M0_ESTENT
							EndIf
						ElseIf SM0->(MsSeek(cEmpAct+cFilAnt))
							cSucur := cFilAnt
							If SM0->(FOUND()) .And. aConfProv[1] == SM0->M0_ESTENT
								cProvEnt := SM0->M0_ESTENT
							EndIf							
						EndIf
						SM0->(DbGoto(nRecSM0))
					//Por Inscri玢o
					ElseIf aConfProv[2] == "1" .And. aConfProv[3] == "3"
						If aConfProv[1] =="MI"
							If CCO->(MsSeek(xFilial("CCO") + aConfProv[1]))
								If CCO->CCO_TPRENI == "2" .And. aConfProv[4] == "N" .And. aConfProv[1] != SD2->D2_PROVENT
									SD2->(DbSkip())
									Loop
								Else
									cProvEnt := aConfProv[1]	
								EndIf
							EndIf
						Else
							cProvEnt := aConfProv[1]
						EndIf
					Endif

					//Verifica se a cidade ou provincia ja possui calculo de IIBB
					//homologado pelo depto. de localizacoes...
					If cProvEnt $ GetMv("MV_AGIIBB") .And. !(cProvEnt$"CF|BA|SF|SE|TU|SA|JU|SL|MI|FO|ME|ER|SJ|LR|CO|CB|CA|NE|LP|TF|CH|RN|CR|SC")
						If aScan(aProvVerif,cProvEnt) == 0
							MsgInfo(STR0159+cProvEnt+STR0160) //"Para la provincia o ciudad de "#" es necesario que el departamento de ubicaciones de Microsiga desarrolle la rutina de calculo. Por favor entre en contacto, con el administrador del sistema."
							AAdd(aProvVerif,cProvEnt)
						EndIf

						SD2->(DbSkip())
						Loop
					EndIf

					If !(cProvEnt $ GetMv("MV_AGIIBB"))
						SD2->(DbSkip())
						Loop
					EndIf
					
					nVlrTotal := SD2->D2_TOTAL
					//Verifica as caracteristicas do TES para verificar se houve
					//a incidencia de Percepcao de IIBB...
					aImpInf := TesImpInf(SD2->D2_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD2->(DbSkip())
						Loop
					EndIf
					aArea:=GetArea()
					aReaSFF:=SFF->(GetArea())
					SFF->(dbSetOrder(12)) //FF_FILIAL+FF_IMPOSTO+FF_CFO_V+FF_ZONFIS+FF_TIPO
					SFF->(MsSeek(xFilial("SFF")+"IBR" +SD2->D2_CF+cProvEnt+aConfProv[4]))
		  			nTotBase:=0
	  				lCalImpos:=.F.
					If SFF->(Found())
  						aAreaAtu:=GetArea()
  						If SFF->(ColumnPos("FF_INCIMP")) > 0
							For nI := 1 To Len(aImpInf)
								If(Trim(aImpInf[nI][01])$ SFF->FF_INCIMP)
									lCalImpos:=.T.
								   	nTotBase+=SD2->(FieldGet(ColumnPos(aImpInf[nI][02])))
								Endif
							Next
						EndIf
						//Se cambian validaciones para obtener el tipo de retenci�n, independientemente de si SFF tiene un importe diferente de 0.
						If (CCO->(ColumnPos("CCO_IMMINR")) > 0 .and. CCO->(ColumnPos("CCO_TPMINR")) > 0)
							If SFF->FF_IMPORTE == 0 .Or. Empty(SFF->FF_IMPORTE)
								nAliq  := SFF->FF_ALIQ
								cZona  := SFF->FF_ZONFIS
							EndIf
							CCO->(dbSetOrder(1))	//CCO_FILIAL+CCO_CODPRO
							If CCO->(MsSeek(xFilial("CCO") + cProvEnt))
								If SFF->FF_IMPORTE == 0 .Or. Empty(SFF->FF_IMPORTE)
									If CCO->CCO_IMMINR <> 0
										nObtMin := IIF(!Empty(CCO->CCO_TPMINR),Val(CCO->CCO_TPMINR),0)
										nLimMInRet := CCO->CCO_IMMINR
										cOriMin := "CCO"
									Endif
								Else
									nCOTipMin := IIF(!Empty(CCO->CCO_TPMINR),Val(CCO->CCO_TPMINR),0)
								EndIf
							Endif
						Else 
							nObtMin = 0
						Endif
						
						CCO->(dbSetOrder(1))	//CCO_FILIAL+CCO_CODPRO
						If CCO->(MsSeek(xFilial("CCO") + cProvEnt)) .And. CCO->(ColumnPos("CCO_TPCALC")) > 0 .And. cPaisLoc == "ARG"
							cTipoCalc := Iif(CCO->CCO_TPCALC == "2", "2", "1")
							If cTipoCalc == "2"
								nRatValImp := 1
								lIIBBTotal := .T.
							EndIf
						Endif
						
						If CCO->(MsSeek(xFilial("CCO") + cProvEnt)) .And. CCO->(ColumnPos("CCO_TPLIMR")) > 0 .And. cPaisLoc == "ARG" //.And. cProvEnt == "ER"
							If CCO->CCO_TPLIMR == "5"
								lCCOLimRet := .T.
							EndIf
						EndIf
						
					    RestArea(aAreaAtu)
					EndIf

						RestArea(aArea)
						SFF->(RestArea(aReaSFF))

						If !lCalImpos

							For nI := 1 To Len(aImpInf)
								If "IB"$Trim(aImpInf[nI][01]) .And. Trim(aImpInf[nI][01])<>"IBR"
									If !Empty(SD2->(FieldGet(ColumnPos(aImpInf[nI][10]))))
										nPos := aScan(aPerIB,{|x| x[1]==cProvent .And.	x[2]==SD2->D2_CF .And. x[3]==aImpInf[nI][01]})
										If (nPos) == 0
											AAdd(aPerIB,{cProvEnt,SD2->D2_CF,aImpInf[nI][01],aImpInf[nI][09]})
										Else
											If aImpInf[nI][09] > aPerIB[nPos][04]
												aPerIB[nPos][04] := aImpInf[nI][09]
											EndIf
										EndIf
									EndIf
								EndIf

							Next
						EndIf
						nPosIb := Ascan(aZonaIb,{|X| X[1]==cProvEnt .And. X[2]=SD2->D2_CF})
						If CCO->(ColumnPos("CCO_TPRET")) > 0 .and. CCO->(ColumnPos("CCO_TPRENI")) > 0  
							If NoInsSFH(SA2->A2_COD,SA2->A2_LOJA,"IBR", aConfProv[1]) // Identifica tratamiento Retencion sujeto No inscripto
								aRes:= NoInsCCORet( aConfProv[1],SD2->D2_PROVENT)
								lCalNoIns := aRes[1][2]
								lNoIns := IIf(!Empty(aRes[1][1]),.T.,.F.)
								nPosIb := Iif(lNoIns == .T.,Ascan(aZonaIb,{|X| X[1]==SD2->D2_PROVENT .And. X[2]=SD2->D2_CF}),nPosIb)  
								If nPosIb == 0 .and. lNoIns == .T. 
									If (lCalNoIns == .T. .and. aConfProv[1] = SD2->D2_PROVENT) .or. (Len(aRes)>0 .and. aRes[1][1] == "2")
										AAdd(aZonaIb,{SD2->D2_PROVENT,SD2->D2_CF,((nVlrTotal+nTotBase )* nRatValImp),0,aConfProv[4],SD2->D2_PROVENT})
									Endif 
								ElseIf nPosIb <> 0 .and. lNoIns == .T. 
									If lCalNoIns == .T. .or. (Len(aRes)>0 .and. aRes[1][1] == "2")
										aZonaIb[nPosIb][3] := aZonaIb[nPosIb][3]+((nVlrTotal+nTotBase )* nRatValImp)
									Endif 
								EndIf
							Else
								If nPosIb == 0 
									AAdd(aZonaIb,{cProvEnt,SD2->D2_CF,((nVlrTotal+nTotBase) * nRatValImp),0,aConfProv[4],SD2->D2_PROVENT})
								Else
									aZonaIb[nPosIb][3] := aZonaIb[nPosIb][3]+((nVlrTotal+nTotBase) * nRatValImp)
								Endif
							EndIf 	
						EndIf
						nVlrTotal := 0
						SD2->(DbSkip())
					Enddo
				Else
					nPosIb := Ascan(aZonaIb,{|X| X[1]==cProvEnt .And. X[2]==SD2->D2_CF})
					AAdd(aZonaIb,{cProvEnt,SD2->D2_CF,nValMerc,0,aConfProv[4],SD2->D2_PROVENT})
				Endif
			ElseIf SE2->E2_TIPO $ MVPAGANT .And. lRetPA .And. Alltrim(SE2->E2_ORIGEM)$"FINA085A|FINA850"
				lIsPA := .T.
				cTipoCalc := aConfProv[5]
				If cTipoCalc == "2"
					nRatPa := 1
					lIIBBTotal := .T.
				Else
					nRatPa := nSaldo/SE2->E2_VALOR
				EndIf 
				
				//Buscar el registro que se gener� en la creaci�n del PA en la SFE y carga la inforamci�n correspondiente.
				aArea:=GetArea()
				SFE->(DbSelectArea("SFE"))
	   			SFE->(DbSetOrder(2)) //2
				SFE->(MsSeek(xFilial("SFE")+ Substr(SE2->E2_NUM,1,Len(SFE->FE_ORDPAGO))+"B"))
				While SFE->(!EOF()) .And. Substr(SE2->E2_NUM,1,Len(SFE->FE_ORDPAGO))+"B" = SFE->FE_ORDPAGO + SFE->FE_TIPO
					If SFE->FE_EST == aConfProv[1] .and. AllTrim(SFE->FE_ESPECIE) == "PA"
						AAdd(aZonaIb, {aConfProv[1], PadR(SFE->FE_CFO, TamSX3("FF_CFO_C")[1], " "), SFE->FE_VALBASE * nRatPa, SFE->FE_VALIMP * nRatPa, aConfProv[4], SFE->FE_EST})
					EndIf
					If AllTrim(SE2->E2_TIPO) $ "PA" .And. lRetPA .and. SFE->FE_EST == aConfProv[1] .And. aConfProv[1] == "CO"
						AAdd(aZonaIb, {aConfProv[1], PadR(SFE->FE_CFO, TamSX3("FF_CFO_C")[1], " "), SFE->FE_VALBASE * nRatPa, SFE->FE_VALIMP * nRatPa, aConfProv[4], SFE->FE_EST})
					EndIf
					SFE->(DbSkip())
				EndDo
				RestArea(aArea)
			Endif

		For nJ   := 1  To Len(aZonaIb)
			lRet := .F.
			nBaseAtual := 0
			//Converter a base para moeda 1
			aZonaIb[nJ][3] := Round(xMoeda(aZonaIb[nJ][3],nMoeda,1,,5,aTxMoedas[nMoeda][2]),MsDecimais(1))

			cZona := ""
			SFF->( Iif(aZonaIb[nJ][2] < "500",dbSetOrder(10),dbSetOrder(12)) )
			
			SFF->(MsSeek( xFilial() + "IBR" + aZonaIb[nJ][2] + aZonaIb[nJ][1] +Iif(aZonaIb[nJ][5]<>Nil,aZonaIb[nJ][5],"") ))
			
			//Si CCO_TPLIMR == 5, valida si FF_TPLIM es diferente de 0 para no considerar el calculo de limite mensual
			If SFF->(Found()) .And. cPaisLoc == "ARG" .And. SFF->(ColumnPos("FF_TPLIM")) > 0 .And. lCCOLimRet
				If !Empty(SFF->FF_TPLIM) .And. SFF->FF_TPLIM != "0"
					lCCOLimRet := .F.
				EndIf
			EndIf
			
			If (aZonaIb[nJ][5]<>Nil .and. aZonaIb[nJ][5] == "V") .and. SFF->(Eof())
				SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][2]+aZonaIb[nJ][1]+Iif(aZonaIb[nJ][5]<>Nil,"*","")))
			Endif 
			
			If  SFF->(Found()) .And. SFF->(ColumnPos("FF_CFORA")) > 0  
				nAliqRet  := SFF->FF_ALIQ
				lEsRetAdc :=ObtDesgl("IBR",SFF->FF_CFO_V,SFF->FF_ZONFIS,SFF->FF_TIPO,SFF->FF_CFORA,@nAliqAdc) //Obtener registro adicional                   
			Endif
			

			If aZonaIb[nJ][5] <> "M" .Or. (aZonaIb[nJ][5] == "M" .And. aZonaIb[nJ][1] <> "CF")
				If SFF->(Found())
					cZona := SFF->FF_ZONFIS //aZonaIb[nJ][1]
					If aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][5] == "N" .and. aZonaIb[nJ][6] <> "TU"
						nAliq := SFF->FF_ALQNOINS
					Else
						nAliq := SFF->FF_ALIQ
					EndIf
					nLimMInRet := Iif(nObtMin == 0, SFF->FF_IMPORTE, nLimMInRet)
					cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
					If lCCOLimRet
						nLimiteSFF := SFF->FF_LIMITE
					EndIf
					If SFF->FF_PRALQIB <> 0
						nPrAliqIB := SFF->FF_PRALQIB
					EndIf
				Else
					cCpoCFO := Iif(aZonaIb[nJ][2] < "500","FF_CFO_C","FF_CFO_V")
					SFF->( Iif(aZonaIb[nJ][2] < "500",dbSetOrder(11),dbSetOrder(13)) )
					If aZonaIb[nJ][1] == "CF" .And. aZonaIb[nJ][5] == "I"
						If SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]+aZonaIb[nJ][5]+Replicate("*",TamSX3(cCpoCFO)[1]) ))
							cZona := aZonaIb[nJ][1]
							nAliq := SFF->FF_ALIQ
							nLimMInRet := Iif(nObtMin == 0, SFF->FF_IMPORTE, nLimMInRet)
							cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
							If lCCOLimRet
								nLimiteSFF := SFF->FF_LIMITE
							EndIf
							If SFF->FF_PRALQIB <> 0
								nPrAliqIB := SFF->FF_PRALQIB
							EndIf
						EndIf
					Else
						If SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]+Replicate("*",TamSx3("FF_TIPO")[1])+Replicate("*",TamSX3(cCpoCFO)[1]) ))
							cZona := aZonaIb[nJ][1]
							If aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][5] == "N" .and. aZonaIb[nJ][6] <> "TU"
								nAliq := SFF->FF_ALQNOINS
							Else
								nAliq := SFF->FF_ALIQ
							EndIf
							nLimMInRet := Iif(nObtMin == 0, SFF->FF_IMPORTE, nLimMInRet)
							cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
							If lCCOLimRet
								nLimiteSFF := SFF->FF_LIMITE
							EndIf
							If SFF->FF_PRALQIB <> 0
								nPrAliqIB := SFF->FF_PRALQIB
							EndIf
						EndIf
					EndIf
				EndIf
			Elseif aZonaIb[nJ][1] =="CF" .And. aZonaIb[nJ][5] == "M"

				If SFF->FF_TIPO == 'M'

					cItem := SFF->FF_ITEM
					cZona := SFF->FF_ZONFIS
					nAliq := SFF->FF_ALIQ
					nLimMInRet := Iif(nObtMin == 0, SFF->FF_IMPORTE, nLimMInRet)
					cOriMin := Iif(nObtMin == 0, "SFF", cOriMin)
					If lCCOLimRet
						nLimiteSFF := SFF->FF_LIMITE
					EndIf
					If SFF->FF_PRALQIB <> 0
						nPrAliqIB := SFF->FF_PRALQIB
					EndIf

					nMinimo  	:= Iif(SFF->(ColumnPos("FF_LIMITE"))>0,SFF->FF_LIMITE,0)
					nMinUnit 	:= Iif(SFF->(ColumnPos("FF_MINUNIT"))>0,SFF->FF_MINUNIT,0)

					aAreaSFF := SFF->(GetArea())

					SFF->(dbSetOrder(11))
					SFF->(MsSeek(xFilial()+"IBR"+aZonaIb[nJ][1]))

					//Array contendo todos os CFOs com a mesma classifica玢o
					While SFF->(!Eof()) .And. SFF->FF_IMPOSTO == "IBR"
						If SFF->FF_TIPO == 'M' .And. SFF->FF_ITEM == cItem .And. SFF->FF_ZONFIS == cZona
							If aScan(aCf,{|x| x[1] == SFF->FF_CFO_C}) == 0
								aAdd(aCf,{SFF->FF_CFO_C,SFF->FF_CFO_V})
							Endif
						Endif
						SFF->(dbSkip())
					Enddo

			 		SFF->(RestArea(aAreaSFF))

					//Verifica se deve calcular IB
					lCalcMon := F850CheckLim(cItem,aCf,SF2->F2_CLIENTE,nMinimo,SF2->F2_DOC,PadR(SF2->F2_SERIE,nTamSer),nMinUnit,"IB",,2,Iif(lMsFil,SF2->F2_MSFIL,""))
				Endif
  			Endif
			If aZonaIb[nJ][1] =="CR"
				SFE->(dbSetOrder(4))
				nTotRetSFE:=0
				nTotBasSFE:=0
				If SFE->(MsSeek(xFilial("SFE")+SF2->F2_CLIENTE+SF2->F2_LOJA))
					cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
						If SFE->FE_EST <> aConfProv[1] .or. Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
							!(SFE->FE_TIPO $"B")
							SFE->(dbSkip())
							Loop
						EndIf
						nTotRetSFE += SFE->FE_RETENC
						nTotBasSFE += SFE->FE_VALBASE
						SFE->(dbSkip())
					End
				EndIf
			 nBaseAtual:= (nTotBasSFE + (aZonaIb[nJ][3]*nPropImp)*nSigno)
			EndIf
			
			If lCCOLimRet
				SFE->(dbSetOrder(4))
				If SFE->(MsSeek(xFilial("SFE") + SF2->F2_CLIENTE + SF2->F2_LOJA))
					cChaveSFE := SFE->FE_FILIAL + SF2->F2_CLIENTE + SF2->F2_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE") + SFE->FE_FORNECE + SFE->FE_LOJA
						If SFE->FE_EST != aConfProv[1] .Or. Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or. Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. SFE->FE_TIPO != "B"
							SFE->(dbSkip())
							Loop
						EndIf
						nTotRetSFE += SFE->FE_RETENC
						nTotBasSFE += SFE->FE_VALBASE
						SFE->(dbSkip())
					End
				EndIf 
			EndIf
			
			If !Empty(cZona) .And. (aZonaIb[nJ][5] <> "M" .Or. (aZonaIb[nJ][5] == "M" .And. lCalcMon) .Or. (aZonaIb[nJ][5] == "M" .And.aZonaIb[nJ][1] <> "CF") .or. (lCalNoIns))
				
				// SE obtienen los registros de la SFH para el proveedor
				cTabTemp := criatrab(nil,.F.)
				cQuery := "SELECT * "
				cQuery += "FROM " + RetSqlName("SFH")+ " SFH "
				cQuery += "WHERE FH_FORNECE='" + SA2->A2_COD + "' AND "
				cQuery += "FH_LOJA='"  + SA2->A2_LOJA + "' AND "
				cQuery += "FH_IMPOSTO='IBR' AND "
				cquery += "FH_ZONFIS='" + cZona +"' AND "
				If cPaisLoc == "ARG"
					cQuery += "FH_FILIAL='" +XFILIAL("SFH") + "' AND "
				Else
					If !Empty(xFilial("SFH"))  .and. !Empty(xFilial("SA2"))
						cQuery += "FH_FILIAL='" +XFILIAL("SA2") + "' AND "
					ElseIf !Empty(xFilial("SFH")) .and. !Empty(xFilial("SE2"))
						cQuery += "FH_FILIAL='" +XFILIAL("SE2") + "' AND "
					Endif
				EndIf                                                            
				
				cquery += "((FH_FIMVIGE>='"+Dtos(dDataBase)+"' AND "									      
				cquery += "FH_INIVIGE<='"+Dtos(dDataBase)+"') OR "                                            
				
				cquery += "(FH_FIMVIGE=' ' AND "									      
				cquery += "FH_INIVIGE=' ') OR "
				
				cquery += "(FH_FIMVIGE=' ' AND "									      
				cquery += "FH_INIVIGE<='"+Dtos(dDataBase)+"') OR "      
				
				cquery += "(FH_FIMVIGE>='"+Dtos(dDataBase)+"' AND "									      
				cquery += "FH_INIVIGE=' '))"      
				
				cQuery += " AND "
				cQuery += "D_E_L_E_T_<>'*'" 
				cQuery += "ORDER BY FH_INIVIGE,FH_FIMVIGE"

				cQuery := ChangeQuery(cQuery)

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTabTemp,.T.,.T.)
				TCSetField(cTabTemp,"FH_INIVIGE","D")
 				TCSetField(cTabTemp,"FH_FIMVIGE","D")

				Count to nNumRegs
				
				If nNumRegs > 0 // Se verifica que existan registros para el proveedor en SFH
					(cTabTemp)->(dbGoTop())
					WHILE (cTabTemp)->(!eof())   .And. ;
						(cTabTemp)->FH_FORNECE==SA2->A2_COD .AND. (cTabTemp)->FH_LOJA == SA2->A2_LOJA .And. (cTabTemp)->FH_ZONFIS == cZona .And. !lRet

						If lNoIns .Or. !((dDatabase >=(cTabTemp)->FH_INIVIGE .Or. Empty((cTabTemp)->FH_INIVIGE) ) .And. ;
							(dDatabase <= (cTabTemp)->FH_FIMVIGE .Or. Empty((cTabTemp)->FH_FIMVIGE))) 
							(cTabTemp)->(dbskip())
							Loop
						Else
							nAliqAux := nAliq 
							nRecnoSFH := (cTabTemp)->R_E_C_N_O_
						EndIf
						
						(cTabTemp)->(dbskip())
					EndDo
				EndIf
				(cTabTemp)->(dbCloseArea())
				
				//?Validar en el archivos de exepciones fiscales si el Proveedor  ?
				//?es exento en la zona fiscal actual.                            ?
				
  				SFH->(DbSetOrder(1))
				SFH->(dbGoTo(nRecnoSFH))
				
				lIsento := SFH->FH_ISENTO == "S"
				
				If SFH->(Found()) .and. !lNoIns
					If SFH->FH_ISENTO=="S" .OR. SFH->FH_PERCENT == 100
						lIsento := .T.
					Else
						If !Empty(SFH->FH_DTINI) .And. !Empty(SFH->FH_DTFIM)
							If dDataBase>=(SFH->FH_DTINI) .And. dDataBase<=(SFH->FH_DTFIM)
								lIsento := .T.
							Else
								lIsento := .F.
							Endif
						Else
							lIsento := .F.
						Endif
					Endif

				EndIf
				While !lNoIns .and. !SFH->(EOF()).And. (SFH->FH_ISENTO <> "S" .And. (dDataBase < SFH->FH_DTINI .Or. dDataBase > SFH->FH_DTFIM)).And.;
										 SFH->FH_FORNECE==SE2->E2_FORNECE .AND. SFH->FH_LOJA == SE2->E2_LOJA .And. SFH->FH_ZONFIS == cZona .And. !lRet
					lFoundFh:= .T.										 		
					If !Empty(SFH->FH_INIVIGE) .And. !Empty(SFH->FH_FIMVIGE)
						IF(dDatabase >= SFH->FH_INIVIGE ) .AND. dDatabase <= SFH->FH_FIMVIGE					
							nCoefmul := SFH->FH_COEFMUL
							nAliqaux := SFH->FH_ALIQ
							lIsento  := IIF(SFH->FH_ISENTO=="S" .OR. SFH->FH_PERCENT == 100 ,.T.,.F.)
							nPercTot :=(100 - SFH->FH_PERCENT) /100	 // % de Exencion del Impuesto								
							lRet := .T.
							cSituaca := SFH->FH_SITUACA
							cZonaSFH := SFH->FH_ZONFIS
							cTipoSFH := SFH->FH_TIPO
						EndIF
					Else
						If Empty(SFH->FH_INIVIGE) .and. Empty(SFH->FH_FIMVIGE)
							nCoefmul := SFH->FH_COEFMUL
							nAliqaux := SFH->FH_ALIQ
							lIsento  := IIF(SFH->FH_ISENTO=="S"  .OR. SFH->FH_PERCENT == 100,.T.,.F.)
							nPercTot :=(100 - SFH->FH_PERCENT) /100	 // % de Exencion del Impuesto
							cSituaca := SFH->FH_SITUACA								
							cZonaSFH := SFH->FH_ZONFIS
							cTipoSFH := SFH->FH_TIPO								
						Else
							If (dDatabase >= SFH->FH_INIVIGE .and. !Empty(SFH->FH_INIVIGE)) .or. (dDatabase <= SFH->FH_FIMVIGE .and.  !Empty(SFH->FH_FIMVIGE))
								nAliqaux := SFH->FH_ALIQ
								lIsento  := IIF(SFH->FH_ISENTO=="S"  .OR. SFH->FH_PERCENT == 100,.T.,.F.)
								nPercTot :=(100 - SFH->FH_PERCENT) /100	 // % de Exencion del Impuesto										
								nCoefmul := SFH->FH_COEFMUL
								lRet := .T.
								cSituaca := SFH->FH_SITUACA
								cZonaSFH := SFH->FH_ZONFIS
								cTipoSFH := SFH->FH_TIPO
							EndIf
						EndIf
					Endif							
					
					If CCO->(ColumnPos("CCO_RPROAG")) > 0 .And. SFH->FH_IMPOSTO ==  "IBR" .And. cPaisLoc == "ARG"
						If CCO->(MsSeek(xFilial("CCO")+SFH->FH_ZONFIS))
							If CCO->CCO_CODPRO == SFH->FH_ZONFIS .And. CCO->CCO_AGRET == "1" .And. CCO->CCO_RPROAG == "N" .And. SFH->FH_AGENTE == "S"
								lAgRetCCO := .F.
							EndIf
						EndIf
					EndIf
						
					SFH->(DbSkip())
				EndDo          
				IF lFoundFh
					IF nAliqAux <> 0
						lRet := .T.
					EndIF
					If lRet .and. !lIsento .AND. nAliqAux <> 0	// Si se encontr?un registro y no es excento, se toma el valor de al韖uota.
						//Aplica % de Reducao para Convenio Multilateral...
						If cZonaSFH <> "TU"						     
							nDeduc := aZonaIb[nJ][3]
							nCofRet := nCoefMul		
							aZonaIb[nJ][3] := aZonaIb[nJ][3] *nPercTot
							If nCoefmul <> 0
								aZonaIb[nJ][3] := aZonaIb[nJ][3] * (nCoefmul/100) 
							EndIf                                                           
		  					nDeduc -= aZonaIb[nJ][3]
		  				EndIf
						If cSituaca == "2" 
							nAliq := nAliqAux * (1+(POSICIONE("CCO",1,xFilial("CCO")+cZOna,"CCO_CRFRET"))/100)
						ElseIf cZonaSFH == "TU" .and. cTipoSFH == "V" .and. nAliqAux <> 0 
							If aZonaIb[nJ][6] <> "TU" .and. SA2->A2_EST <> "TU" .and. nCoefMul <> 0
								nAliq := (nAliqAux * Iif (nPrAliqIB<>1,1-(nPrAliqIB/100),nPrAliqIB) * nCoefmul)
							Else
								nAliq := nAliqAux
							EndIf	
						ElseIf cZonaSFH == "TU" .and. cTipoSFH == "N"
							nAliq := 0
						Else
							nAliq := nAliqAux * Iif (nPrAliqIB<>1,1-(nPrAliqIB/100),nPrAliqIB)
						Endif
					ElseIf lRet .and. !lIsento .AND. nAliqAux == 0
						If cZonaSFH == "TU" .and. aZonaIb[nJ][6] <> "TU" .and. cTipoSFH == "V" .and. SA2->A2_EST <> "TU"
							If CCO->(ColumnPos("CCO_CRFRET")) > 0
								nCRFRET := POSICIONE("CCO",1,xFilial("CCO")+cZOna,"CCO_CRFRET")
							EndIf
							If nCRFRET <> 0
								nAliq :=  (nAliq * nCRFRET) 
							EndIf
						ElseIf nPrAliqIB<>1
							nAliq := nAliq * (1-(nPrAliqIB/100))
						EndIf
					EndIf			
				EndIF					
				If !lIsento

					If SFF->FF_REDBASE <> 0 .and. aZonaIb[nJ][3] <> 0
						aZonaIb[nJ][3] := (aZonaIb[nJ][3] * (( 100 - SFF->FF_REDBASE ) / 100 ) )
					Endif
					
					If TYPE ("aProvLim") == "A"  
						nPsLmPv := Ascan(aProvLim, {|xT| xT[1] == aZonaIb[nJ][1]})
					Else
						nPsLmPv := 0
					EndIf
					
					If nPsLmPv > 0
						lLimPrim := aProvLim[nPsLmPv][2]
					EndIf 
					
					//Si el IIBB se controla por tipo 2 (total) 
					If cTipoCalc == "2" .And. cPaisLoc == "ARG"
						SFE->(dbSetOrder(4))
						nRetSFE:=0
						If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
							DBSELECTAREA("FVC")
							FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
							If FVC->(MsSeek(xFilial("FVC")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC))
								While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF2->(F2_CLIENTE+F2_LOJA+F2_DOC)
									If AllTrim(FVC->FVC_TIPO) $ "B" .and. ABS(FVC->FVC_RETENC) > 0 .and. FVC->FVC_SERIE == SF2->F2_SERIE .and. cZona == FVC->FVC_EST
										AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
										nRetSFE +=  FVC->FVC_RETENC
									EndIf
									FVC->(dbSkip())
								Enddo
							EndIf	
						EndIf
						//Si es para un PA 
						If lIsPA
							SFE->(dbSetOrder(4)) //4
							//Cuenta el total de registros en SFE, correspondiente a �rdenes de pago anteriores.
							cChaveSFE := xFilial("SFE") + SE2->E2_FORNECE + SE2->E2_LOJA + SE2->E2_NUM + SE2->E2_PREFIXO + "B"
							If SFE->(MsSeek(cChaveSFE))
								While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE") + SFE->FE_FORNECE + SFE->FE_LOJA + SFE->FE_NFISCAL + SFE->FE_SERIE + SFE->FE_TIPO .And. cZona == SFE->FE_EST
									nRetSFE += SFE->FE_RETENC
									SFE->(dbSkip())
								Enddo
							EndIf
						Else 
							If SFE->(MsSeek(xFilial("SFE") + SF2->F2_CLIENTE + SF2->F2_LOJA + SF2->F2_DOC + SF2->F2_SERIE + "B"))  //Cuenta el total de registros en SFE, correspondiente a �rdenes de pago anteriores.
								cChaveSFE := SFE->FE_FILIAL + SFE->FE_FORNECE + SFE->FE_LOJA + SF2->F2_DOC + SF2->F2_SERIE + "B"
								While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE") + SFE->FE_FORNECE + SFE->FE_LOJA + SFE->FE_NFISCAL + SFE->FE_SERIE + SFE->FE_TIPO
									If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or. YEAR(SFE->FE_EMISSAO) != Year(dDataBase) .Or. !(SFE->FE_TIPO $ "B") .Or. cZona != SFE->FE_EST 
										SFE->(dbSkip())
										Loop
									EndIf
									If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
										If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE + SFE->FE_LOJA + SFE->FE_NFISCAL + SFE->FE_SERIE + SFE->FE_PARCELA)}) == 0
											nRetSFE += SFE->FE_RETENC
										EndIf
									Else
										nRetSFE += SFE->FE_RETENC
									EndIf
									SFE->(dbSkip())
								EndDo
							EndIf
						EndIf
						
						//Impostos recalculados na tela - Modificar OP
						//Impostos calculados - IVA, IB...
						nImpCalc := 0
						If Len(aImpCalc) > 0
							For aX := 1 to Len(aImpCalc[nLinha])
								If ValType(aImpCalc[nLinha][aX]) == "A"
									nImpCalc += aImpCalc[nLinha][aX][2]
								EndIf
							Next
							
							For aX := 1 to Len(aSFEIb)
								If ValType(aSFEIb[aX]) == "A"
									nImpCalc += aSFEIb[aX][6]
								EndIf
							Next
							
							//Ganancia
							If Len(aImpCalc[Len(aImpCalc)]) > 0 .And. valtype(aImpCalc[Len(aImpCalc)][1]) <> "A"  .And. aImpCalc[Len(aImpCalc)][1] == "GAN"
								nImpCalc += aImpCalc[Len(aImpCalc)][2]
							Endif
						End
						
						If lCCOLimRet .And. nLimiteSFF > 0 .And. lLimPrim
							//FE_VALIMP
							nValorRet := Round((((nTotBasSFE + aZonaIb[nJ][3]) * (nAliq/100)) - nTotRetSFE) * nSigno,TamSX3("FE_VALIMP")[2])
						Else
							//Se calcula si la diferencia entre los pagos y el total a pagar.
							nValorRet := Iif(aZonaIb[nJ][4]<>0,Round(aZonaIb[nJ][4]*nPropImp,TamSX3("FE_VALIMP")[2])*nSigno,Round((((aZonaIb[nJ][3]* nPropImp)*nSigno)*(nAliq/100)),TamSX3("FE_VALIMP")[2])) - nRetSFE ////FE_VALIMP
						EndIf
						
						If aZonaIb[nJ][1] $ "CR|ME|"
							nValorRet := Iif(aZonaIb[nJ][4]<>0,Round(aZonaIb[nJ][4]*nPropImp,TamSX3("FE_VALIMP")[2])*nSigno,Round(((nBaseAtual*(nAliq/100))-nRetSFE),TamSX3("FE_VALIMP")[2]))  //FE_VALIMP
						EndIf 
						
						//Si a�n existe un monto a pagar, se valida que el saldo sea suficiente para cubrir el monto, de lo contario se calcula solo lo que es posible cubrir o no se cubre en lo absoluto.
						If nValorRet != 0
							If (nSaldo - ABS(nImpCalc)) < ABS(nValorRet)
								nValorRet := (nSaldo - ABS(nImpCalc))
								If nValorRet > 0
									nValorRet := nValorRet * nSigno
								Else
									nValorRet := 0
								EndIf
							EndIf 
						EndIf 
						
					EndIf
					
					If cTipoCalc == "2" .And. nValorRet != 0 .And. cPaisLoc == "ARG"
						nRetencao := nValorRet //FE_VALIMP
					ElseIf cTipoCalc == "2" .And. nValorRet == 0 .And. cPaisLoc == "ARG"
						lAgRetCCO := .F.
						nRetencao := 0
					Else
						nRetencao := Iif(aZonaIb[nJ][4]<>0,Round(aZonaIb[nJ][4]*nPropImp,TamSX3("FE_VALIMP")[2])*nSigno,Round((((aZonaIb[nJ][3]* nPropImp)*nSigno)*(nAliq/100)),TamSX3("FE_VALIMP")[2]))  //FE_VALIMP
						If aZonaIb[nJ][1] $ "CR|ME|"
							nRetencao := Iif(aZonaIb[nJ][4]<>0,Round(aZonaIb[nJ][4]*nPropImp,TamSX3("FE_VALIMP")[2])*nSigno,Round(((nBaseAtual*(nAliq/100))-nTotRetSFE),TamSX3("FE_VALIMP")[2]))  //FE_VALIMP
						EndIf
						If lCCOLimRet .And. nLimiteSFF > 0 .And. lLimPrim
							//FE_VALIMP
							nRetencao := Round((((nTotBasSFE + aZonaIb[nJ][3]) * (nAliq/100)) - nTotRetSFE) * nSigno,TamSX3("FE_VALIMP")[2])
						EndIf
					EndIf 

					If (SFF->FF_PRALQIB <> 0) .And. (Len(aPerIB) > 0) .and. !(aZonaIb[nJ][1] == "TU" .and. aZonaIb[nJ][5] == "V")

						If aScan(aPerIB,{|x| X[1]==aZonaIb[nJ][1] .And. X[2]==aZonaIb[nJ][2] .And. x[4]<nAliq}) > 0
							nAliq := nAliq * (SFF->FF_PRALQIB/100)
						EndIf
					EndIf
					
					If nSaldo == 0
						lAgRetCCO := .F.
					EndIf
					
					Aadd(aSFEIb,Array(33))
					aSFEIb[Len(aSFEIb)][1] := SE2->E2_NUM                   //FE_NFISCAL
					aSFEIb[Len(aSFEIb)][2] := SE2->E2_PREFIXO               //FE_SERIE
					aSFEIb[Len(aSFEIb)][3] := (aZonaIb[nJ][3]* nPropImp)*nSigno        //FE_VALBASE
					aSFEIb[Len(aSFEIb)][4] := nAliq
					If aZonaIb[nJ][1] == "CR"
						aSFEIb[Len(aSFEIb)][5] := nRetencao
					ElseIf lLimNRet
						nRetencao := 0
						aSFEIb[Len(aSFEIb)][5] := nRetencao
					Else
						aSFEIb[Len(aSFEIb)][5] := nRetencao
					EndIf
					
					aSFEIb[Len(aSFEIb)][6] := aSFEIb[Len(aSFEIb)][5]        //FE_RETENC
					aSFEIb[Len(aSFEIb)][7] := nSaldo//SE2->E2_VALOR
					aSFEIb[Len(aSFEIb)][8] := SE2->E2_EMISSAO
					aSFEIb[Len(aSFEIb)][9] := cZona
					aSFEIb[Len(aSFEIb)][10]:= SE2->E2_MOEDA
					aSFEIb[Len(aSFEIb)][11]:= SFF->FF_CFO_C  //CFO - COMPRA
					aSFEIb[Len(aSFEIb)][12]:= SFF->FF_CFO_V  //CFO - VENDA
					aSFEIb[Len(aSFEIb)][13]:= SE2->E2_TIPO
					aSFEIb[Len(aSFEIb)][14]:= SFF->FF_CONCEPT
					aSFEIb[Len(aSFEIb)][15]:= nDeduc
					aSFEIb[Len(aSFEIb)][16]:= nCofRet
					aSFEIb[Len(aSFEIb)][17]:= lEsRetAdc
					aSFEIb[Len(aSFEIb)][18]:= Iif(lEsRetAdc,SFF->FF_CFORA,"")   
					aSFEIb[Len(aSFEIb)][19]:= nAliqRet    
					aSFEIb[Len(aSFEIb)][20]:= nAliqAdc	
					aSFEIb[Len(aSFEIb)][21]:= 1
					aSFEIb[Len(aSFEIb)][22]:= 0
					aSFEIb[Len(aSFEIb)][23]:= 0
					aSFEIb[Len(aSFEIb)][24]:= 0
					aSFEIb[Len(aSFEIb)][25]:= cOriMin
					aSFEIb[Len(aSFEIb)][26]:= nLimMInRet // Monto del m�nimo, para calculo de m�nimos por OP
					aSFEIb[Len(aSFEIb)][27]:= Iif(cOriMin == "SFF", nCOTipMin, nObtMin) // De donde se obtuvo el m�nimo, para calculo de m�nimos por OP
					aSFEIb[Len(aSFEIb)][28]:= Iif(!(aConfProv[1] $ ("SF|BA|CO")), 0, nTotCCO3) //Base imponible + impuestos solamente cuando es para SF.
					aSFEIb[Len(aSFEIb)][29]:= aConfProv[6]
					aSFEIb[Len(aSFEIb)][30]:= lCCOLimRet .And. nLimiteSFF > 0
					aSFEIb[Len(aSFEIb)][31]:= nLimiteSFF
					aSFEIb[Len(aSFEIb)][32]:= nTotRetSFE //Monto retencion limite
					aSFEIb[Len(aSFEIb)][33]:= nTotBasSFE //Monto base imponible limite.				
				EndIf

			Endif
		Next
	EndIf
EndIf

If !lAgRetCCO .And. cPaisLoc == "ARG" .Or. (cTipoCalc == "2" .And. !lSUSSPrim .And. cPaisLoc == "ARG")
	aSFEIB := {}
	If TYPE ("aProvLim") == "A" .And. Len(aProvLim) > 0 .And. nPsLmPv > 0
		aProvLim[nPsLmPv][2] := lLimPrim
	EndIf
EndIf

Return aSFEIB


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetSUSS� Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetSUSS  - Calculo de Ret de SUSS para NF               ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetSUSS(cAgente,nSigno,nSaldo,lRetPa,nProp,aSUSS,aImpCalc,nLinha,nControl,aSE2,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local aSFESUSS  := {}
Local aAreaOld	:= {}
Local aImposto	:= {}
Local lCalcula	:= .F.
Local lCalcSus	:= .T.
Local lReSaSus	:= GetNewPar("MV_RETSUSS","N") == "S" 
Local cCampoBase:= ""
Local cTipoCalc := ""
Local cTitAnt	:= ""
Local cTpObr	:= ""
Local cConceito	:= ""
Local nBaseImp	:= 0
Local nValBase	:= 0
Local nTotSuss 	:= 0
Local nBaseRet 	:= 0
Local nRateio   := 0
Local nValRet  	:= 0
Local nTotRetSFE:= 0
Local nTotBasSFE:= 0
Local nAliqRet 	:= 0
Local nLimMInRet:= 0
Local nI		:= 1
Local nPosSUSS  := 0
Local nImpCalc	:= 0
Local nS		:= 0
Local nAcmSuss	:= 0
Local nRecSF1	:= 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.
Local aRetPreO	:= {}
Local lSuss5 := .F.
            
If	type("cFornece")=="U"
	cFornece	:=SE2->E2_FORNECE
	cLoja		:=SE2->E2_LOJA
EndIf

DEFAULT nProp	:= 1
DEFAULT nSigno	:=	1
DEFAULT lRetPa 	:= .F.
DEFAULT aSUSS  	:= {}
DEFAULT aImpCalc:= {}
DEFAULT nLinha	:= 0 
DEFAULT nControl:= 0
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFESUSS := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"S",nSaldo,dEmissao,,lReSaSus)
EndIf

aArea:=GetArea()

If lRetPa //.and. lReSaSus
	Return aSFESUSS
Endif

//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Retenܧܤo.�
//+---------------------------------------------------------------------+
SA2->(DbSetOrder(1))
If !lRetPa
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
		SA2->(MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA))
	Else
		SA2->(MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA))
	Endif
	If SA2->(Found()) .And. SA2->(ColumnPos("A2_DTICALS")) > 0 ;
		.And. SA2->(ColumnPos("A2_DTFCALS")) > 0  .And. !Empty(SA2->A2_DTICALS) .And. !Empty(SA2->A2_DTFCALS)
   		If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS) )
   	 		If  SA2->(ColumnPos("A2_PORSUS"))>0
   	   			lCalcSus	:=.T.
		 	Else
				lCalcSus	:=.F.
			EndIf
 		EndIf
	EndIf
Else
	If SA2->(MsSeek(xFilial("SA2")+cFornece+cLoja)) .And. SA2->(ColumnPos("A2_DTICALS")) > 0 ;
			.And. SA2->(ColumnPos("A2_DTFCALS")) > 0  .And. !Empty(SA2->A2_DTICALS) .And. !Empty(SA2->A2_DTFCALS)
	   	If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS) )
	   	   	If  SA2->(ColumnPos("A2_PORSUS"))>0
	   	   		lCalcSus	:=.T.
	   	 	Else
	   			lCalcSus	:=.F.
	   		EndIf
	   	EndIf
	EndIf
EndIf


If ExistBlock("F0851IMP")
	lCalcSus:=ExecBlock("F0851IMP",.F.,.F.,{"SUSS"})
EndIf

If Subs(cAgente,6,1) == "S" .And. SF1->(ColumnPos("F1_VALSUSS")) > 0 .And.   SF1->F1_VALSUSS > 0 .And. lCalcSus .And. !lRetPa
	dbSelectArea("SF1")
	dbSetOrder(1)
	If lMsfil
		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	Else
		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	EndIf

	While Alltrim(SF1->F1_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
		SF1->(DbSkip())
		Loop
	Enddo

	If (AllTrim(SF1->F1_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
	   (Iif(lMsfil,SE2->E2_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
	   F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA) .And. (SF1->F1_VALSUSS > 0)
	   cTpObr	:=	 SF1->F1_CONCOBR
	   If SFF->(ColumnPos("FF_TPCALC")) > 0
			SA2->(DbSetOrder(1))
			If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1",SE2->E2_FILORIG) == xFilial("SE2")
				SA2->(MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA))
			Else
				SA2->(MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA))
			Endif
			If SA2->(Found())
				If  SA2->(ColumnPos("A2_CONCSUS")) > 0  .and. !Empty(SA2->A2_CONCSUS)// .And. (SA2->(ColumnPos("A2_PROVEMP")) > 0 .AND. SA2->A2_PROVEMP <> 1)
					dbSelectArea("SF1")
					dbSetOrder(1)
				  	If lMsFil
						nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
						SF1->(dbGoTo(nRecSF1))
					Else
						nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
						SF1->(dbGoTo(nRecSF1))
					Endif
					SFF->(DbSetOrder(9))
					If SFF->(MsSeek(xFilial("SFF")+"SUS"+SF1->F1_CONCOBR+SF1->F1_ZONGEO))
						While  !SFF->(EOF()) .AND.  xFilial("SFF")+SF1->F1_CONCOBR+SF1->F1_ZONGEO == SFF->FF_FILIAL+ SFF->FF_GRUPO
							If Alltrim(SA2->A2_CONCSUS) ==Alltrim(SFF->FF_ITEM)
								cTipoCalc := SFF->FF_TPCALC
								Exit
							EndIf
							SFF->(DbSkip())
						End
					EndIf
				//ColumnPos adicionado pois n�o foi encontrado a origem da cria��o deste campo � N�O RETIRAR
				ElseIf SA2->(ColumnPos("A2_PROVEMP")) > 0 .And.  SA2->A2_PROVEMP == 1
			 		SFF->(DbSetOrder(9))
					If SFF->(MsSeek(xFilial("SFF")+"SUS"+Space(TamSX3("FF_GRUPO")[1])))
						cTipoCalc := SFF->FF_TPCALC				   
					EndIf
	
				Endif
			Endif
		EndIf
		//Solamente si esta configurado para el concepto 5 de SUSS y con el tipo de calculo en 3 puede calcular.
		If cTipoCalc == "3" .And. AllTrim(SFF->FF_ITEM) == "5" .And. AllTrim(SA2->A2_CONCSUS) == "5"
			lSuss5 := .T.
			cConceito 	:= SFF->FF_ITEM
		EndIf
		If cTipoCalc == "2"
			nValRet := Round(xMoeda(SF1->F1_VALSUSS,SF1->F1_MOEDA,1,SF1->F1_DTDIGIT,5),MsDecimais(1))
			SFE->(dbSetOrder(4))
			If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE))
				cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
				While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
					If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
						!(SFE->FE_TIPO $"U|S")
						SFE->(dbSkip())
						Loop
					EndIf
					nValRet 	-= SFE->FE_RETENC
					SFE->(dbSkip())
				End
    		Endif
			IF SA2->A2_CONDO=='1'
				SFE->(dbSetOrder(5))
				If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE))
					cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORCOND+SFE->FE_LOJCOND+SFE->FE_NFISCAL+SFE->FE_SERIE
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORCOND+SFE->FE_LOJCOND+SFE->FE_NFISCAL+SFE->FE_SERIE
						If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
							!(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf
						nValRet 	-= SFE->FE_RETENC
						SFE->(dbSkip())
					End
	    		Endif
			Endif
		Else
			//����������������������������������������������������������������Ŀ
			//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
			//� do titulo pelo total da Nota Fiscal.                           �
			//������������������������������������������������������������������
			nRateio := Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,5),MsDecimais(1))/Round(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,SF1->F1_DTDIGIT,5),MsDecimais(1))
		 	nValRet := Round(xMoeda(SF1->F1_VALSUSS,SF1->F1_MOEDA,1,SF1->F1_DTDIGIT,5),MsDecimais(1))*nRateio
		Endif
		If nValRet != 0 .And. lSuss5
			//����������������������������������������������������������������Ŀ
			//� Gravar Retenciones.                                            �
			//������������������������������������������������������������������
			AAdd(aSFESUSS,array(14))
			aSFESUSS[Len(aSFESUSS)][1] := SF1->F1_DOC         		     //FE_NFISCAL
			aSFESUSS[Len(aSFESUSS)][2] := SF1->F1_SERIE       		     //FE_SERIE
			aSFESUSS[Len(aSFESUSS)][3] := nSaldo*nProp	                     //FE_VALBASE
			aSFESUSS[Len(aSFESUSS)][4] := nSaldo*nProp	                     //FE_VALIMP
			aSFESUSS[Len(aSFESUSS)][5] := Round((nValRet*100)/nSaldo,2)//FE_PORCRET
			aSFESUSS[Len(aSFESUSS)][6] := nValRet*nProp
			aSFESUSS[Len(aSFESUSS)][7] := 0     						// FE_ALIQ
			aSFESUSS[Len(aSFESUSS)][8] := cConceito                   	// FE_CONCEPT
			aSFESUSS[Len(aSFESUSS)][9] := cTipoCalc            		// Tipo Calculo - ajuste de retenܧܣo pot total
			aSFESUSS[Len(aSFESUSS)][10]:= cTpObr
			aSFESUSS[Len(aSFESUSS)][11]:= SE2->E2_FORNECE
			aSFESUSS[Len(aSFESUSS)][12]:= SE2->E2_LOJA
			aSFESUSS[Len(aSFESUSS)][13]:= IIf(lReSaSus ,aSFESUSS[Len(aSFESUSS)][4], 0)
			aSFESUSS[Len(aSFESUSS)][14] := SF1->F1_ESPECIE
		Endif
		
		If !lSuss5
			MSGALERT(OemToAnsi(STR0265) + SA2->A2_COD + OemToAnsi(STR0266))  
		EndIf
	EndIf
ElseIf  Subs(cAgente,6,1) == "S" .And. lCalcSus .And.( ( SA2->(ColumnPos("A2_PROVEMP")) > 0  .or.  SA2->(ColumnPos("A2_CONCSUS")) > 0)) ;
	.And. SIX->(MsSeek('SFF9'))
	If !lRetPa
		SA2->(DbSetOrder(1))
		If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
			SA2->(MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA))
		Else
			SA2->(MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA))
		Endif
		If SA2->(Found())
			If SA2->(ColumnPos("A2_CONCSUS")) > 0 .and. !Empty(SA2->A2_CONCSUS)
				dbSelectArea("SF1")
				dbSetOrder(1)
				If lMsfil
			 		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
					SF1->(dbGoTo(nRecSF1))
		  		Else
			  		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
					SF1->(dbGoTo(nRecSF1))
		  		EndIf
				nVRetSUSS	:= Iif( lReSaSus, SF1->F1_RETSUSS, 0 ) 
				nSRetSUSS	:= Iif( lReSaSus, SF1->F1_SALSUSS, 0 )
	   			cTpObr	:=	 SF1->F1_CONCOBR
	   			SFF->(DbSetOrder(9))
				If SFF->(MsSeek(xFilial("SFF")+"SUS"+SF1->F1_CONCOBR+SF1->F1_ZONGEO))
					While  !SFF->(EOF()) .AND.  xFilial("SFF")+SF1->F1_CONCOBR+SF1->F1_ZONGEO == SFF->FF_FILIAL+ SFF->FF_GRUPO
						If Alltrim(SA2->A2_CONCSUS) ==Alltrim(SFF->FF_ITEM)
							nAliqRet := SFF->FF_ALIQ   / 100
							nLimMInRet:=SFF->FF_IMPORTE
							lCalcula:=.T.
							cConceito 	:= SFF->FF_ITEM
							If SFF->(ColumnPos("FF_TPCALC")) > 0
								cTipoCalc := SFF->FF_TPCALC
							Endif
							Exit
						EndIf
						SFF->(DbSkip())
					End
				EndIf
		 	ElseIf SA2->(ColumnPos("A2_PROVEMP")) > 0 .And.  SA2->A2_PROVEMP == 1
		 		SFF->(DbSetOrder(9))
				If SFF->(MsSeek(xFilial("SFF")+"SUS"+Space(TamSX3("FF_GRUPO")[1])))
					nAliqRet := SFF->FF_ALIQ   / 100
					nLimMInRet:=SFF->FF_IMPORTE
					If SFF->(ColumnPos("FF_TPCALC")) > 0
						cTipoCalc := SFF->FF_TPCALC
					Endif
				Else
					aAreaOld:= GetArea()
					DbSelectArea("SFF")
					RecLock("SFF",.T.)
					Replace FF_FILIAL with xFilial("SFF")
					Replace FF_ALIQ 	with 2
					Replace FF_IMPORTE	with 39.99
					Replace FF_IMPOSTO	with "SUS"
					Replace FF_CONCEPT	with "SISTEMA UNICO DE LA SEGURIDAD SOCIAL (SUSS). RESOLUCION GENERAL 1784"
					MsUnlock()
					RestArea(aAreaOld)
					nAliqRet := SFF->FF_ALIQ   / 100
					nLimMInRet:=SFF->FF_IMPORTE
				EndIf
				lCalcula:=.T.
			EndIf
	        
	       //Si el concepto es 5, no debe de calcular SUSS.
			If AllTrim(SA2->A2_CONCSUS) == "5"
				lCalcula := .F.
			EndIf
		EndIf
		If lCalcula
   		cTpObr	:=	 SF1->F1_CONCOBR
			SD1->(DbSetOrder(1))
			If lMsfil
				SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			Else
				SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			EndIf
			If SD1->(Found())
				Do while Iif(lMsfil, SD1->D1_MSFIL, xFilial("SD1",SE2->E2_FILORIG)) == SD1->D1_FILIAL .And. SF1->F1_DOC == SD1->D1_DOC .AND. ;
					SF1->F1_SERIE == SD1->D1_SERIE .AND. SF1->F1_FORNECE == SD1->D1_FORNECE ;
					.AND. SF1->F1_LOJA == SD1->D1_LOJA .AND. !SD1->(EOF())
					If AllTrim(SD1->D1_ESPECIE)<>AllTrim(SF1->F1_ESPECIE)
						SD1->(DbSkip())
						Loop
					Endif
					aImposto:=DefImposto(SD1->D1_TES)
					lTESNoExen := aScan(aImposto,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD1->(DbSkip())
						Loop
					EndIf

					For nI:=1 to Len(aImposto)
						If Subs(aImposto[nI][1],1,2)="IV"
							aAreaSD1:=SD1->(GetArea())
							dbSelectArea("SFB")
							SFB->(DbSetOrder(1))
							If SFB->(MsSeek(xFilial("SFB")+aImposto[nI][1])) .And. SFB->FB_CLASSE=="I"
								RestArea(aAreaSD1)
								cCampoBase:="SD1->D1_"+ aImposto[nI][7]
								nBaseImp:=nBaseImp+ &cCampoBase
							EndIf

						EndIf
					Next
			            // Verificar se o Item tem IVA, caso contrario abate da base de calculo do SUSS

					SD1->(DbSkip())
			    EndDo
			EndIf
			 //Levanta quanto ja foi retido para a factura corrente, para em seguida
						//abater do total calculado para retencao.
			SFE->(dbSetOrder(4))

			If SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS =="1"
				If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA))
					cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
						If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
							!(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf
						If SFE->FE_NFISCAL+SFE->FE_SERIE == SF1->F1_DOC+SF1->F1_SERIE
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE					
							nBaseRet += SFE->FE_VALBASE
						Endif
						SFE->(dbSkip())
					End
				EndIf

				IF SA2->A2_CONDO=='1'
					If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
						DBSELECTAREA("FVC")
						FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
						If FVC->(MsSeek(xFilial("FVC")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC))
							While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF1->(F1_FORNECE+F1_LOJA+F1_DOC)
								If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. FVC->FVC_RETENC > 0 .and. FVC->FVC_SERIE == SF1->F1_SERIE
									AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
									nTotRetSFE += FVC->FVC_RETENC
									nTotBasSFE += FVC->FVC_VALBAS
									nBaseRet += FVC->FVC_VALBAS
								EndIf
								FVC->(dbSkip())
							Enddo
						EndIf	
					EndIf
					SFE->(dbSetOrder(5))
					If SFE->(MsSeek(xFilial("SFE")+SA2->A2_COD+SA2->A2_LOJA))
						While !SFE->(Eof()) .And. SFE->FE_FILIAL  == xFilial("SFE");
							.And. SFE->FE_FORCOND == SA2->A2_COD;
							.And. SFE->FE_LOJCOND == SA2->A2_LOJA
							If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
								!(SFE->FE_TIPO $"U|S")
								SFE->(dbSkip())
								Loop
							EndIf

							//Base retida para o tܭtulo
							If SFE->FE_NFISCAL+SFE->FE_SERIE == SF1->F1_DOC+SF1->F1_SERIE
								If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
									If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
										nTotRetSFE += SFE->FE_RETENC
										nTotBasSFE += SFE->FE_VALBASE
										nBaseRet += SFE->FE_VALBASE
									EndIf
								Else
									nTotRetSFE += SFE->FE_RETENC
									nTotBasSFE += SFE->FE_VALBASE					
									nBaseRet += SFE->FE_VALBASE
								EndIf
							Endif
							SFE->(dbSkip())
						End
		            Endif
				Endif

			ElseIf SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS == "4"
				If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
					DBSELECTAREA("FVC")
					FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
					If FVC->(MsSeek(xFilial("FVC")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC))
						While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF1->(F1_FORNECE+F1_LOJA+F1_DOC)
							If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. FVC->FVC_RETENC > 0 .and. FVC->FVC_SERIE == SF1->F1_SERIE
								AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
								nTotRetSFE += FVC->FVC_RETENC
								nTotBasSFE += FVC->FVC_VALBAS
								nBaseRet += FVC->FVC_VALBAS
							EndIf
							FVC->(dbSkip())
						Enddo
					EndIf	
				EndIf
				If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA))
					cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
						If Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. !(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf
						If SFE->FE_NFISCAL+SFE->FE_SERIE == SF1->F1_DOC+SF1->F1_SERIE
							If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
								If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
									nTotRetSFE += SFE->FE_RETENC
									nTotBasSFE += SFE->FE_VALBASE
									nBaseRet += SFE->FE_VALBASE
								EndIf
							Else
								nTotRetSFE += SFE->FE_RETENC
								nTotBasSFE += SFE->FE_VALBASE
								nBaseRet += SFE->FE_VALBASE
							EndIf
						Endif
						SFE->(dbSkip())
					End
				EndIf
				IF SA2->A2_CONDO=='1'

					SFE->(dbSetOrder(5))
					If SFE->(MsSeek(xFilial("SFE")+SA2->A2_COD+SA2->A2_LOJA))
						While !SFE->(Eof()) .And. SFE->FE_FILIAL  == xFilial("SFE");
							.And. SFE->FE_FORCOND == SA2->A2_COD;
							.And. SFE->FE_LOJCOND == SA2->A2_LOJA
							If Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. !(SFE->FE_TIPO $"U|S")
								SFE->(dbSkip())
								Loop
							EndIf
							If SFE->FE_NFISCAL+SFE->FE_SERIE == SF1->F1_DOC+SF1->F1_SERIE
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							nBaseRet = SFE->FE_VALBASE
							Endif
							
							SFE->(dbSkip())
						End
					Endif
	            Endif


			Else
				If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
					DBSELECTAREA("FVC")
					FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
					If FVC->(MsSeek(xFilial("FVC")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC))
						While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF1->(F1_FORNECE+F1_LOJA+F1_DOC)
							If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. FVC->FVC_RETENC > 0 .and. FVC->FVC_SERIE == SF1->F1_SERIE
								AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
								nTotRetSFE += FVC->FVC_RETENC
								nTotBasSFE += FVC->FVC_VALBAS
								nBaseRet := nTotBasSFE
							EndIf
							FVC->(dbSkip())
						Enddo
					EndIf	
				EndIf
				SFE->(dbSetOrder(4))
				If SFE->(MsSeek(xFilial("SFE")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE))
					cChaveSFE := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
					While !SFE->(Eof()) .And. cChaveSFE == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
						If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
							!(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf
						If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
							If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
								nTotRetSFE += SFE->FE_RETENC
								nTotBasSFE += SFE->FE_VALBASE
								nBaseRet := nTotBasSFE
							EndIf
						Else
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							nBaseRet := nTotBasSFE
						EndIf
						SFE->(dbSkip())
					End

				EndIf
			EndIf

		    If SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS $ "1|4"
		    	nTotBSUSS:= nTotBasSFE
		    	nLimiteSUSS:= nLimMInRet
		    	If SA2->A2_CONCSUS $ "1|4"
				 	For nI:= 1 to Len(aSUSS)
					   If len(aSuss[nI]) >10 .and. SE2->E2_FORNECE==aSuss[nI][11] .And. SE2->E2_LOJA==aSuss[nI][12]
						nTotRetSFE += Iif( aSUSS[nI][6] == 0,0,aSUSS[nI][4])
						nTotBasSFE += aSUSS[nI][3]
	
						If aSUSS[nI][1]+aSUSS[nI][2]+aSUSS[nI][14] == SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_ESPECIE
							nBaseRet += aSUSS[nI][3]
						Endif
	
						cTitAnt := aSUSS[nI][1]+aSUSS[nI][2]
					   EndIf
					Next
				EndIf
			Endif
			DbselectArea("SF1")
			While Alltrim(SF1->F1_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
				SF1->(DbSkip())
				Loop
			Enddo

			If (AllTrim(SF1->F1_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
				(Iif(lMsfil,SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
				F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA)
				//����������������������������������������������������������������Ŀ
				//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
				//� do titulo pelo total da Nota Fiscal.                           �
				//������������������������������������������������������������������
      			cTpObr	:=	 SF1->F1_CONCOBR

				nRateio := xMoeda(nSaldo,SE2->E2_MOEDA,1,SF1->F1_DTDIGIT,5)/xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,SF1->F1_DTDIGIT,5)

				If cTipoCalc == "2"
					nPosSUSS := aScan(aSUSS,{|x| x[1] == SF1->F1_DOC .And. x[2] == SF1->F1_SERIE})
				Endif

				nValBase 	:= xMoeda((nBaseImp*Iif(cTipoCalc == "2",1,nRateio)),SF1->F1_MOEDA,1,dDatabase,,aTxMoedas[Max(SF1->F1_MOEDA,1)][2])
				nValtotBas	:= nValBase
			    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) .and.  Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS) ) .Or. (Empty(SA2->A2_DTICALS).and.Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS)) .Or.(Empty(SA2->A2_DTFCALS) .and. Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) )
					nValRetCac	:= (nValtotBas * nAliqRet)*(SA2->A2_PORSUS/100)
					nValorRet	:= ABs(nValRetCac) 
				Else
					nValRetCac	:= nValtotBas * nAliqRet
					nValorRet	:= ABs(nValRetCac)
				EndIf
				
				If cTipoCalc == "2"
					nValorRet -= nTotRetSFE
				EndIf	
				//Impostos recalculados na tela - Modificar OP
				If nLinha > 0

					//Impostos calculados - IVA, IB...
					For nI := 1 to Len(aImpCalc[nLinha])
						If ValType(aImpCalc[nLinha][nI]) == "A"
							nImpCalc += aImpCalc[nLinha][nI][2]
						EndIf
					Next

					//Ganancia
					If Len(aImpCalc[Len(aImpCalc)]) > 0 .And. ValType(aImpCalc[Len(aImpCalc)][1]) =="C" .AND. aImpCalc[Len(aImpCalc)][1] == "GAN" .And. nPosSUSS == 0
						nImpCalc += aImpCalc[Len(aImpCalc)][2]
					Endif

				Endif

				If nValorRet+nImpCalc >= nSaldo .And. cTipoCalc == "2"

					nValorRet := nSaldo - nImpCalc

					//Zera o calculo caso a retencao seja negativa
					If nValorRet < 0
						nValBase 	:= 0
						nValtotBas 	:= 0
						nValRetCac	:= 0
						nValorRet	:= 0
					Endif

				Endif
				//����������������������������������������������������������������Ŀ
				//� Gravar Retenciones.                                            �
				//������������������������������������������������������������������

				If nValorRet > 0
					AAdd(aSFESUSS,array(14))
					aSFESUSS[Len(aSFESUSS)][1] := SF1->F1_DOC         		    //FE_NFISCAL
					aSFESUSS[Len(aSFESUSS)][2] := SF1->F1_SERIE       		    //FE_SERIE
					aSFESUSS[Len(aSFESUSS)][3] := nValBase	                    //FE_VALBASE
					aSFESUSS[Len(aSFESUSS)][4] := nValorRet 	                //FE_VALIMP
					aSFESUSS[Len(aSFESUSS)][5] := 100  						//FE_PORCRET
					If Alltrim(cConceito)=="1" 
						aSFESUSS[Len(aSFESUSS)][6] := + nValorRet
					Else
					 	If nValRetCac > nLimMInRet .Or. Alltrim(cConceito)=="4"
							aSFESUSS[Len(aSFESUSS)][6] := + nValorRet
						Else
							aSFESUSS[Len(aSFESUSS)][6] :=  0
						EndIf
					EndIf
					aSFESUSS[Len(aSFESUSS)][7] := nAliqRet * 100		//FE_ALIQ
					aSFESUSS[Len(aSFESUSS)][8] := cConceito            // FE_CONCEPT
					aSFESUSS[Len(aSFESUSS)][9] := cTipoCalc            // Tipo Calculo - ajuste de retenܧܣo pot total
					aSFESUSS[Len(aSFESUSS)][10] := cTpObr
					aSFESUSS[Len(aSFESUSS)][11] := SE2->E2_FORNECE
					aSFESUSS[Len(aSFESUSS)][12] := SE2->E2_LOJA
					aSFESUSS[Len(aSFESUSS)][13] := IIf(lReSaSus, aSFESUSS[Len(aSFESUSS)][4],0)  
					aSFESUSS[Len(aSFESUSS)][14] := SF1->F1_ESPECIE

				EndIf
			EndIf
	 	EndIf
	Elseif lRetPa 	// se for Pa  e calcular retencoes
		SA2->(DbSetOrder(1))
		If SA2->(MsSeek(xFilial("SA2")+cFornece+cLoja))
			If  SA2->(ColumnPos("A2_CONCSUS")) > 0 .and. !Empty(SA2->A2_CONCSUS)// .And. (SA2->(ColumnPos("A2_PROVEMP")) > 0 .AND. SA2->A2_PROVEMP <> 1)
	   			SFF->(DbSetOrder(9))
				If SFF->(MsSeek(xFilial("SFF")+"SUS"+Alltrim(nGrpSus)+Alltrim(cZnGeo)))
					While  !SFF->(EOF()) .AND.  xFilial("SFF")+Alltrim(nGrpSus)+Alltrim(cZnGeo) == SFF->FF_FILIAL+ SFF->FF_GRUPO
						If Alltrim(SA2->A2_CONCSUS) ==Alltrim(SFF->FF_ITEM)
							nAliqRet := SFF->FF_ALIQ   / 100
							nLimMInRet:=SFF->FF_IMPORTE
							lCalcula:=.T.
							cConceito := SFF->FF_ITEM
							cTpObr 		:= Substr(SFF->FF_GRUPO,1,1)
							Exit
						EndIf
						SFF->(DbSkip())
					End
			    EndIf
		 	ElseIf SA2->(ColumnPos("A2_PROVEMP")) > 0 .And.  SA2->A2_PROVEMP == 1
		 		SFF->(DbSetOrder(9))
				If SFF->(MsSeek(xFilial("SFF")+"SUS"+Space(TamSX3("FF_GRUPO")[1])))
					nAliqRet := SFF->FF_ALIQ   / 100
					nLimMInRet:=SFF->FF_IMPORTE
				Else
					aAreaOld:= GetArea()
					DbSelectArea("SFF")
					RecLock("SFF",.T.)
					Replace FF_FILIAL with xFilial("SFF")
					Replace FF_ALIQ 	with 2
					Replace FF_IMPORTE	with 39.99
					Replace FF_IMPOSTO	with "SUS"
					Replace FF_CONCEPT	with "SISTEMA UNICO DE LA SEGURIDAD SOCIAL (SUSS). RESOLUCION GENERAL 1784"
					MsUnlock()
					RestArea(aAreaOld)
					nAliqRet := SFF->FF_ALIQ   / 100
					nLimMInRet:=SFF->FF_IMPORTE
				EndIf
				lCalcula:=.T.
	        EndIf
		EndIf

		If lCalcula
		   //	nValtotBas:= (nTotBasSFE + nTotAnt )
			nValRetCac:= xMoeda(nTotAnt,nMoedaCor,1,dDatabase,,aTxMoedas[Max(nMoedaCor,1)][2]) * nAliqRet
			nValorRet:=  ABs(nValRetCac)

			aArea:=GetArea()
			DbSelectArea("SFE")
			SFE->(dbSetOrder(4))
			If SFE->(MsSeek(xFilial("SFE")+cFornece+cLoja))
				cChaveSFE := xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
					While !SFE->(Eof()) .And. cChaveSFE == SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
						If SFE->FE_TIPO $"U|S".And. !Empty(cNumOp) .And. SFE->FE_NUMOPER == cNumOp
							nTotSuss += SFE->FE_RETENC
						EndIf
						SFE->(dbSkip())
					End
			EndIf
			SA2->(dbSetOrder(1))
			SA2->(MsSeek(xFilial()+cFornece+cLoja))
			IF SA2->A2_CONDO=='1'
				SFE->(dbSetOrder(5))
				If SFE->(MsSeek(xFilial("SFE")+cFornece+cLoja))
					cChaveSFE := xFilial("SFE")+SFE->FE_FORCOND+SFE->FE_LOJCOND
					While !SFE->(Eof()) .And. cChaveSFE == SFE->FE_FILIAL+SFE->FE_FORCOND+SFE->FE_LOJCOND
						If SFE->FE_TIPO $"U|S".And. !Empty(cNumOp) .And. SFE->FE_NUMOPER == cNumOp
							nTotSuss += SFE->FE_RETENC
						EndIf
						SFE->(dbSkip())
					End
				EndIf
			Endif
			RestArea(aArea)

			nValorRet:=Iif(nTotSuss>0,((nTotSuss-nValorRet)*-1),nValorRet)

				//����������������������������������������������������������������Ŀ
				//� Gravar Retenciones.                                            �
				//������������������������������������������������������������������
   		AAdd(aSFESUSS,array(14))
			aSFESUSS[Len(aSFESUSS)][1] := SF1->F1_DOC         		     //FE_NFISCAL
			aSFESUSS[Len(aSFESUSS)][2] := SF1->F1_SERIE       		     //FE_SERIE
			aSFESUSS[Len(aSFESUSS)][3] := xMoeda(nTotAnt,nMoedaCor,1,dDatabase,,aTxMoedas[Max(nMoedaCor,1)][2])  //FE_VALBASE
			aSFESUSS[Len(aSFESUSS)][4] := Iif(nLiquido < nValorRet,nLiquido,nValorRet) 	                     //FE_VALIMP
			aSFESUSS[Len(aSFESUSS)][5] := 100  						//FE_PORCRET
			If (nValRetCac > nLimMInRet) .Or. ( SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS == "4")	
				aSFESUSS[Len(aSFESUSS)][6] := nValorRet //+ Iif(nValor<nValorRet,nValor,nValorRet)
			Else
				aSFESUSS[Len(aSFESUSS)][6] :=  0
			EndIf
		   	aSFESUSS[Len(aSFESUSS)][7] := nAliqRet * 100		//FE_ALIQ
		   	aSFESUSS[Len(aSFESUSS)][8] := cConceito            // FE_CONCEPT
		   	aSFESUSS[Len(aSFESUSS)][9] := cTipoCalc            // Tipo Calculo - ajuste de retenܧܣo pot total
		   	aSFESUSS[Len(aSFESUSS)][10] := cTpObr
		   	aSFESUSS[Len(aSFESUSS)][11] := SE2->E2_FORNECE
			aSFESUSS[Len(aSFESUSS)][12] := SE2->E2_LOJA
			aSFESUSS[Len(aSFESUSS)][13] := IIf(lReSaSus ,aSFESUSS[Len(aSFESUSS)][4], 0)
			aSFESUSS[Len(aSFESUSS)][14] := SF1->F1_ESPECIE
		EndIf
	EndIf
EndIf
RestArea(aArea)
Return aSFESUSS

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetSU2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetSU2   - Calculo de Ret de SUSS para NCP              ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetSU2(cAgente,nSigno,nSaldo,nProp,aSUSS,aImpCalc,nLinha,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
LOCAL aArea	:= {}
Local aAreaSEK	:= {}
Local aAreaSF4	:= {}
Local aAreaSFE	:= {}
Local aRetTotal	:= {}
Local aRetAplic	:= {}
Local aSFESUSS  := {}
Local aAreaOld	:= {}
Local aImposto	:= {}
Local lImposto 	:= .F.
Local lCalcSus	:= .T.
Local lCalcula	:= .F.
Local lReSaSU		:= GetNewPar("MV_RETSUSS","N") == "S"
Local cConceito	:= ""
Local cTpOBR	:= ""
Local cCampoBase:= ""
Local cTipoCalc	:= ""
Local cCF	:= ""
Local cCalPA	:= .F.
Local nRateio  	:= 0
Local nValRet   := 0
Local nTotRetSFE:= 0
Local nTotBasSFE:= 0
Local nAliqRet 	:= 0
Local nLimMInRet:= 0
Local nI		:= 1
Local nBaseImp	:= 0
Local nBaseRet	:= 0
Local nPosSUSS 	:= 0
Local nVRetSUSS	:= 0
Local nSRetSUSS	:= 0
Local nAlqIVA	:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.
Local nImpCalc 	:= 0
Local cChaveSFE2	:= ""
Local cTitAnt		:= ""
Local aRetPreO	:= {} 
Local lSuss5 := .F.
Local lReSaSus := GetNewPar("MV_RETSUSS","N") == "S" 
Local nValBase	:= 0

If type("cFornece")=="U"
	cFornece	:=SE2->E2_FORNECE
	cLoja		:=SE2->E2_LOJA
EndIf


DEFAULT nSigno	:=	-1
DEFAULT aSUSS  	:= {}
DEFAULT aImpCalc	:= {}
DEFAULT nLinha	:= 0
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFESUSS := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"S",nSaldo,dEmissao,,lReSaSus)
EndIf

SA2->(DbSetOrder(1))
If SA2->(MsSeek(If(lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2"),SE2->E2_MSFIL,xFilial("SA2"))+SE2->E2_FORNECE+SE2->E2_LOJA)) .And. SA2->(ColumnPos("A2_DTICALS")) > 0 ;
	.And. SA2->(ColumnPos("A2_DTFCALS")) > 0  .And. !Empty(SA2->A2_DTICALS) .And. !Empty(SA2->A2_DTFCALS)
    If  ( Dtos(dDataBase)<= Dtos(SA2->A2_DTICALS) ) .And. ( Dtos(Ddatabase) >= Dtos(SA2->A2_DTFCALS) )
   		lCalcSus	:=.F.
    EndIf
EndIf

If ExistBlock("F0851IMP")
	lCalcSus:=ExecBlock("F0851IMP",.F.,.F.,{"SU2"})
EndIf

If Alltrim(SE2->E2_TIPO) == "PA" //.and. lReSaSU
	Return aSFESUSS
Endif

//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Retenܧܤo.�
//+---------------------------------------------------------------------+
If Subs(cAgente,6,1) == "S" .And. SF2->(ColumnPos("F2_VALSUSS")) > 0 .And. SF2->F2_VALSUSS > 0   .And. lCalcSus
	dbSelectArea("SF2")
	dbSetOrder(1)
	If lMsFil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf

	nVRetSUSS	:= Iif( cPaisLoc == "ARG" .and. lReSaSU, SF2->F2_RETSUSS, 0 ) 
	nSRetSUSS	:= Iif( cPaisLoc == "ARG" .and. lReSaSU, SF2->F2_SALSUSS, 0 )

	While AllTrim(SF2->F2_ESPECIE) != AllTrim(SE2->E2_TIPO) .And. !EOF()
		SF2->(DbSkip())
		Loop
	EndDo

	If (AllTrim(SF2->F2_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
	   (Iif(lMsFil,SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
	   F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA) .And. (SF2->F2_VALSUSS > 0)
	   
	   If SFF->(ColumnPos("FF_TPCALC")) > 0
			SA2->(DbSetOrder(1))
			If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF2") == xFilial("SE2")
				SA2->(MsSeek(SE2->E2_MSFIL + SE2->E2_FORNECE + SE2->E2_LOJA))
			Else
				SA2->(MsSeek(xFilial("SA2") + SE2->E2_FORNECE + SE2->E2_LOJA))
			Endif
			
			If SA2->(Found())
				If  SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. !Empty(SA2->A2_CONCSUS)
					DbSelectArea("SF2")
					SF2->(DbSetOrder(1))
			  		If lMsFil
				  		SF2->(MsSeek(SE2->E2_MSFIL + SE2->E2_NUM + SE2->E2_PREFIXO + SE2->E2_FORNECE + SE2->E2_LOJA))
					Else
				  		SF2->(MsSeek(xFilial("SF2") + SE2->E2_NUM + SE2->E2_PREFIXO + SE2->E2_FORNECE + SE2->E2_LOJA))
				 	EndIf
					SFF->(DbSetOrder(9))
					If SFF->(MsSeek(xFilial("SFF") + "SUS" + SF2->F2_CONCOBR + SF2->F2_ZONGEO))
						While !SFF->(EOF()) .And. xFilial("SFF") + SF2->F2_CONCOBR + SF2->F2_ZONGEO == SFF->FF_FILIAL + SFF->FF_GRUPO
							If AllTrim(SA2->A2_CONCSUS) == AllTrim(SFF->FF_ITEM)
								cTipoCalc := SFF->FF_TPCALC
								Exit
							EndIf
							SFF->(DbSkip())
						End
				    EndIf
			 	ElseIf SA2->(ColumnPos("A2_PROVEMP")) > 0 .And. SA2->A2_PROVEMP == 1
			 		SFF->(DbSetOrder(9))
					If SFF->(MsSeek(xFilial("SFF") + "SUS" + Space(TamSX3("FF_GRUPO")[1])))
						cTipoCalc := SFF->FF_TPCALC
					EndIf
				EndIf
			EndIf
		EndIf
		
		//Solamente si esta configurado para el concepto 5 de SUSS y con el tipo de calculo en 3 puede calcular.
		If cTipoCalc == "3" .And. AllTrim(SFF->FF_ITEM) == "5" .And. AllTrim(SA2->A2_CONCSUS) == "5"
			lSuss5 := .T.
			cConceito := SFF->FF_ITEM
		EndIf

		//Obtener el valor del impuesto con base al prorateo del valor del titulo por el total de la nota fiscal.
		nRateio := Round(xMoeda(nSaldo, SE2->E2_MOEDA, 1, SE2->E2_EMISSAO, 5), MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT, SF2->F2_MOEDA, 1, SF2->F2_EMISSAO, 5), MsDecimais(1))
		nValRet := Round(xMoeda(SF2->F2_VALSUSS, SF2->F2_MOEDA, 1, SF2->F2_EMISSAO, 5), MsDecimais(1)) * nRateio * nSigno

		If nValRet != 0 .And. lSuss5
			//Gravar Retenciones.
			AAdd(aSFESUSS,array(14))
			aSFESUSS[Len(aSFESUSS)][1] := SF2->F2_DOC         		     //FE_NFISCAL
			aSFESUSS[Len(aSFESUSS)][2] := SF2->F2_SERIE       		     //FE_SERIE
			aSFESUSS[Len(aSFESUSS)][3] := nSaldo*nSigno	             //FE_VALBASE
			aSFESUSS[Len(aSFESUSS)][4] := nSaldo*nSigno	             //FE_VALIMP
			aSFESUSS[Len(aSFESUSS)][5] := Round((nValRet*100)/nSaldo,2)//FE_PORCRET
			aSFESUSS[Len(aSFESUSS)][6] := nValRet
			aSFESUSS[Len(aSFESUSS)][7] := 0  							//FE_ALIQ
			aSFESUSS[Len(aSFESUSS)][8] := cConceito                   	// FE_CONCEPT
			aSFESUSS[Len(aSFESUSS)][9] := cTipoCalc            // Tipo Calculo - ajuste de retenܧܣo pot total
			aSFESUSS[Len(aSFESUSS)][10] := SF2->F2_CONCOBR
			aSFESUSS[Len(aSFESUSS)][11] := SE2->E2_FORNECE
			aSFESUSS[Len(aSFESUSS)][12] := SE2->E2_LOJA
			aSFESUSS[Len(aSFESUSS)][13] := IIf(lReSaSus ,aSFESUSS[Len(aSFESUSS)][4], 0)
			aSFESUSS[Len(aSFESUSS)][14] := SF2->F2_ESPECIE
		EndIf
	EndIf
ElseIf  Subs(cAgente,6,1) == "S" .And. lCalcSus .And.( ( SA2->(ColumnPos("A2_PROVEMP")) > 0 .or. cPaisLoc == "ARG" )) ;
	.And. SIX->(MsSeek('SFF9'))
	SA2->(DbSetOrder(1))
	If SA2->(MsSeek(If(lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2"),SE2->E2_MSFIL,xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA)))
		If  SA2->(ColumnPos("A2_CONCSUS")) > 0 .and. !Empty(SA2->A2_CONCSUS)// .And. (SA2->(ColumnPos("A2_PROVEMP")) > 0 .AND. SA2->A2_PROVEMP <> 1)
			dbSelectArea("SF2")
			dbSetOrder(1)
	  		If lMsFil
		  		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
				SF2->(dbGoTo(nRecSF2))
	  		Else
				nRecSF2 := FINBuscaNF(xFilial("SF2",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
				SF2->(dbGoTo(nRecSF2))
	  		EndIf
			SFF->(DbSetOrder(9))
			If SFF->(MsSeek(xFilial("SFF")+"SUS"+SF2->F2_CONCOBR+SF2->F2_ZONGEO))
				While  !SFF->(EOF()) .AND.  xFilial("SFF")+SF2->F2_CONCOBR+SF2->F2_ZONGEO == SFF->FF_FILIAL+ SFF->FF_GRUPO
					If Alltrim(SA2->A2_CONCSUS) ==Alltrim(SFF->FF_ITEM)
						nAliqRet := SFF->FF_ALIQ   / 100
						nLimMInRet:=SFF->FF_IMPORTE
						lCalcula:=.T.
						cConceito := SFF->FF_ITEM
						cTpObr 		:= Substr(SFF->FF_GRUPO,1,1)
						If SFF->(ColumnPos("FF_TPCALC")) > 0
							cTipoCalc := SFF->FF_TPCALC
						Endif
						Exit
					EndIf
					SFF->(DbSkip())
				End
		    EndIf
	 	ElseIf SA2->(ColumnPos("A2_PROVEMP")) > 0 .And.  SA2->A2_PROVEMP == 1
	 		SFF->(DbSetOrder(9))
			If SFF->(MsSeek(xFilial("SFF")+"SUS"+Space(TamSX3("FF_GRUPO")[1])))
				nAliqRet := SFF->FF_ALIQ   / 100
				nLimMInRet:=SFF->FF_IMPORTE
				If SFF->(ColumnPos("FF_TPCALC")) > 0
					cTipoCalc := SFF->FF_TPCALC
				Endif
			Else
				aAreaOld:= GetArea()
				DbSelectArea("SFF")
				RecLock("SFF",.T.)
				Replace FF_FILIAL with xFilial("SFF")
				Replace FF_ALIQ 	with 2
				Replace FF_IMPORTE	with 39.99
				Replace FF_IMPOSTO	with "SUS"
				Replace FF_CONCEPT	with "SISTEMA UNICO DE LA SEGURIDAD SOCIAL (SUSS). RESOLUCION GENERAL 1784"
				MsUnlock()
				RestArea(aAreaOld)
				nAliqRet := SFF->FF_ALIQ   / 100
				nLimMInRet:=SFF->FF_IMPORTE
			EndIf
			lCalcula:=.T.
        EndIf
		
		//Si el concepto es 5, no debe de calcular SUSS.
		If AllTrim(SA2->A2_CONCSUS) == "5"
			lCalcula := .F.
		EndIf
	EndIf
	
	If lCalcula
		SD2->(DbSetOrder(3))
		If lMsFil
			SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		Else
			SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		EndIf
		If SD2->(Found())
			Do while Iif(lMsFil,SD2->D2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL.And.SF2->F2_DOC==SD2->D2_DOC.AND.;
				SF2->F2_SERIE==SD2->D2_SERIE.AND.SF2->F2_CLIENTE==SD2->D2_CLIENTE;
					.AND.SF2->F2_LOJA==SD2->D2_LOJA.AND.!SD2->(EOF())

				If AllTrim(SD2->D2_ESPECIE)<>AllTrim(SF2->F2_ESPECIE)
					SD2->(DbSkip())
					Loop
				Endif
		       aImposto:=DefImposto(SD2->D2_TES)
				lTESNoExen := aScan(aImposto,{|x| "IV" $ AllTrim(x[1])}) <> 0
				If !lTESNoExen
					SD2->(DbSkip())
					Loop
				EndIf
				 
				For nI:=1 to Len(aImposto)
					If Subs(aImposto[nI][1],1,2)="IV"
						aAreaSD2:=SD2->(GetArea())
						dbSelectArea("SFB")
						SFB->(DbSetOrder(1))
						If SFB->(MsSeek(xFilial("SFB")+aImposto[nI][1])) .And. SFB->FB_CLASSE=="I"
							RestArea(aAreaSD2)
							cCampoBase:="SD2->D2_"+ aImposto[nI][7]
							nBaseImp:=nBaseImp+ &cCampoBase
						EndIf
					EndIf
				Next
				SD2->(DbSkip())
		    EndDo
		EndIf
		
		SFE->(dbSetOrder(4))

		If SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS =="1"
			If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
				DBSELECTAREA("FVC")
				FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
				If FVC->(MsSeek(xFilial("FVC")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC))
					While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF2->(F2_CLIENTE+F2_LOJA+F2_DOC)
						If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. ABS(FVC->FVC_RETENC) > 0 .and. FVC->FVC_SERIE == SF2->F2_SERIE
							AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
							nTotRetSFE += FVC->FVC_RETENC
							nTotBasSFE += FVC->FVC_VALBAS
							nBaseRet += FVC->FVC_VALBAS
						EndIf
						FVC->(dbSkip())
					Enddo
				EndIf	
			EndIf
			If SFE->(MsSeek(xFilial("SFE")+SF2->F2_CLIENTE+SF2->F2_LOJA))
				cChaveSFE2 := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
				While !SFE->(Eof()) .And. cChaveSFE2 == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
					If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
						!(SFE->FE_TIPO $"U|S")
						SFE->(dbSkip())
						Loop
					EndIf
					If SFE->FE_NFISCAL+SFE->FE_SERIE == SF2->F2_DOC+SF2->F2_SERIE
						If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
							If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
								nTotRetSFE += SFE->FE_RETENC
								nTotBasSFE += SFE->FE_VALBASE
								nBaseRet += SFE->FE_VALBASE
							EndIf
						Else
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE					
							nBaseRet += SFE->FE_VALBASE
						EndIf
					Endif
					SFE->(dbSkip())
				End
			EndIf

			IF SA2->A2_CONDO=='1'
				SFE->(dbSetOrder(5))
				If SFE->(MsSeek(xFilial("SFE")+SA2->A2_COD+SA2->A2_LOJA))
					While !SFE->(Eof()) .And. SFE->FE_FILIAL  == xFilial("SFE");
						.And. SFE->FE_FORCOND == SA2->A2_COD;
						.And. SFE->FE_LOJCOND == SA2->A2_LOJA
						If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
							!(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf

						//Base retida para o tܭtulo
						If SFE->FE_NFISCAL+SFE->FE_SERIE == SF2->F2_DOC+SF2->F2_SERIE
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							nBaseRet += SFE->FE_VALBASE
						Endif
						SFE->(dbSkip())
					End
	            Endif
			Endif

		ElseIf SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS == "4"
			If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
				DBSELECTAREA("FVC")
				FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
				If FVC->(MsSeek(xFilial("FVC")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC))
					While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF2->(F2_CLIENTE+F2_LOJA+F2_DOC)
						If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. ABS(FVC->FVC_RETENC) > 0 .and. FVC->FVC_SERIE == SF2->F2_SERIE
							AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
							nTotRetSFE += FVC->FVC_RETENC
							nTotBasSFE += FVC->FVC_VALBAS
							nBaseRet += FVC->FVC_VALBAS
						EndIf
						FVC->(dbSkip())
					Enddo
				EndIf	
			EndIf
			If SFE->(MsSeek(xFilial("SFE")+SF2->F2_CLIENTE+SF2->F2_LOJA))
				cChaveSFE2 := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
				While !SFE->(Eof()) .And. cChaveSFE2 == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
					If Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. !(SFE->FE_TIPO $"U|S")
						SFE->(dbSkip())
						Loop
					EndIf
					If SFE->FE_NFISCAL+SFE->FE_SERIE == SF2->F2_DOC+SF2->F2_SERIE
						If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
							If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
								nTotRetSFE += SFE->FE_RETENC
								nTotBasSFE += SFE->FE_VALBASE
								nBaseRet += SFE->FE_VALBASE
							EndIf
						Else
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							nBaseRet += SFE->FE_VALBASE
						EndIf
					Endif
					SFE->(dbSkip())
				End
			EndIf
			IF SA2->A2_CONDO=='1'

				SFE->(dbSetOrder(5))
				If SFE->(MsSeek(xFilial("SFE")+SA2->A2_COD+SA2->A2_LOJA))
					While !SFE->(Eof()) .And. SFE->FE_FILIAL  == xFilial("SFE");
						.And. SFE->FE_FORCOND == SA2->A2_COD;
						.And. SFE->FE_LOJCOND == SA2->A2_LOJA
						If Year(SFE->FE_EMISSAO) != Year(dDataBase) .Or. !(SFE->FE_TIPO $"U|S")
							SFE->(dbSkip())
							Loop
						EndIf
						If SFE->FE_NFISCAL+SFE->FE_SERIE == SF2->F2_DOC+SF2->F2_SERIE
						nTotRetSFE += SFE->FE_RETENC
						nTotBasSFE += SFE->FE_VALBASE
						nBaseRet = SFE->FE_VALBASE
						Endif
						
						SFE->(dbSkip())
					End
				Endif
            Endif


		Else
			If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
				DBSELECTAREA("FVC")
				FVC->(DBSETORDER(3)) //FVC_FILIAL+FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC_SERIE+FVC_TIPO+FVC_CONCEP
				If FVC->(MsSeek(xFilial("FVC")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC))
					While FVC->(!Eof()) .and. FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC) == SF2->(F2_CLIENTE+F2_LOJA+F2_DOC)
						If AllTrim(FVC->FVC_TIPO) $ "U|S" .and. ABS(FVC->FVC_RETENC) > 0 .and. FVC->FVC_SERIE == SF2->F2_SERIE
							AADD(aRetPreO,FVC->(FVC_FORNEC+FVC_LOJA+FVC_NFISC+FVC->FVC_SERIE+FVC_PARCEL))
							nTotRetSFE += FVC->FVC_RETENC
							nTotBasSFE += FVC->FVC_VALBAS
							nBaseRet := nTotBasSFE
						EndIf
						FVC->(dbSkip())
					Enddo
				EndIf	
			EndIf
			SFE->(dbSetOrder(4))
			If SFE->(MsSeek(xFilial("SFE")+SF2->F2_CLIENTE+SF2->F2_LOJA+SF2->F2_DOC+SF2->F2_SERIE))
				cChaveSFE2 := SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
				While !SFE->(Eof()) .And. cChaveSFE2 == xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE
					If Month(SFE->FE_EMISSAO) != Month(dDataBase) .Or.YEAR(SFE->FE_EMISSAO)!=Year(dDataBase) .Or.;
						!(SFE->FE_TIPO $"U|S")
						SFE->(dbSkip())
						Loop
					EndIf
					If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
						If Ascan(aRetPreO,{|x| x == SFE->(SFE->FE_FORNECE+SFE->FE_LOJA+SFE->FE_NFISCAL+SFE->FE_SERIE+SFE->FE_PARCELA) }) == 0
							nTotRetSFE += SFE->FE_RETENC
							nTotBasSFE += SFE->FE_VALBASE
							nBaseRet := nTotBasSFE
						EndIf
					Else
						nTotRetSFE += SFE->FE_RETENC
						nTotBasSFE += SFE->FE_VALBASE
						nBaseRet := nTotBasSFE
					EndIf
					SFE->(dbSkip())
				End

			EndIf
		EndIf

	    If SA2->(ColumnPos("A2_CONCSUS")) > 0 .And. SA2->A2_CONCSUS $ "1|4"
	    	nTotBSUSS:= nTotBasSFE
	    	nLimiteSUSS:= nLimMInRet
	    	If SA2->A2_CONCSUS $ "1|4"
			 	For nI:= 1 to Len(aSUSS)
				   If len(aSuss[nI]) >10 .and. SE2->E2_FORNECE==aSuss[nI][11] .And. SE2->E2_LOJA==aSuss[nI][12]
					nTotRetSFE += Iif( aSUSS[nI][6] == 0,0,aSUSS[nI][4])
					nTotBasSFE += aSUSS[nI][3]
	
					If aSUSS[nI][1]+aSUSS[nI][2]+aSUSS[nI][14] == SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_ESPECIE
						nBaseRet += aSUSS[nI][3]
					Endif
	
					cTitAnt := aSUSS[nI][1]+aSUSS[nI][2]
				   EndIf
				Next
			EndIf
		Endif

		While Alltrim(SF2->F2_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
			SF2->(DbSkip())
			Loop
		Enddo

		If (AllTrim(SF2->F2_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
			(Iif(lMsFil,SE2->E2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
			SF2->F2_FILIAL+SF2->F2_DOC+PadR(SF2->F2_SERIE,nTamSer)+SF2->F2_CLIENTE+SF2->F2_LOJA)
			//����������������������������������������������������������������Ŀ
			//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
			//� do titulo pelo total da Nota Fiscal.                           �
			//������������������������������������������������������������������
			
			nRateio := Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,SF2->F2_DTDIGIT,5),MsDecimais(1))/Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,SF2->F2_DTDIGIT,5),MsDecimais(1))
			
			If cTipoCalc == "2"
				nPosSUSS := aScan(aSUSS,{|x| x[1] == SF2->F2_DOC .And. x[2] == SF2->F2_SERIE})
			Endif
			
	  		nValBase 	:= Round(xMoeda((nBaseImp*Iif(cTipoCalc == "2",1,nRateio)),SF2->F2_MOEDA,1,dDatabase,,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
	  		nValtotBas	:= nValBase 
	   	 	If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) .and.  Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS) ) .Or. (Empty(SA2->A2_DTICALS).and.Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALS)) .Or.(Empty(SA2->A2_DTFCALS) .and. Dtos(dDataBase)>= Dtos(SA2->A2_DTICALS) )	  			
				nValRetCac:= nValtotBas * nAliqRet *(SA2->A2_PORSUS/100)
				nValorRet	:= ABs(nValRetCac)    	 	
			Else
				nValRetCac:= nValtotBas * nAliqRet
				nValorRet := ABs(nValRetCac)
			Endif 			
			
			If cTipoCalc == "2"
				nValorRet -= nTotRetSFE
			EndIf
			//Impostos recalculados na tela - Modificar OP
			If nLinha > 0

				//Impostos calculados - IVA, IB...
				For nI := 1 to Len(aImpCalc[nLinha])
					If ValType(aImpCalc[nLinha][nI]) == "A"
						nImpCalc += aImpCalc[nLinha][nI][2]
					EndIf
				Next

				//Ganancia
				If Len(aImpCalc[Len(aImpCalc)]) > 0 .And. ValType(aImpCalc[Len(aImpCalc)][1]) =="C" .AND. aImpCalc[Len(aImpCalc)][1] == "GAN" .And. nPosSUSS == 0
					nImpCalc += aImpCalc[Len(aImpCalc)][2]
				Endif

			Endif
			
			If nImpCalc <> 0
				nImpCalc := nImpCalc * nSigno
			EndIf
			
			If nValorRet + nImpCalc >= nSaldo .And. cTipoCalc == "2"
				nValorRet := nSaldo - nImpCalc
				
				//Zera o calculo caso a retencao seja negativa
				If nValorRet < 0
					nValBase 	:= 0
					nValtotBas 	:= 0
					nValRetCac	:= 0
					nValorRet	:= 0
				Endif
				
			Endif

			//����������������������������������������������������������������Ŀ
			//� Gravar Retenciones.                                            �
			//������������������������������������������������������������������
			If nValorRet > 0
				AAdd(aSFESUSS,Array(14))
				aSFESUSS[Len(aSFESUSS)][1] := SF2->F2_DOC         		     //FE_NFISCAL
				aSFESUSS[Len(aSFESUSS)][2] := SF2->F2_SERIE       		     //FE_SERIE
				aSFESUSS[Len(aSFESUSS)][3] := (nValBase * nSigno)	//(nValtotBas *nSigno)	                     //FE_VALBASE
				aSFESUSS[Len(aSFESUSS)][4] := (nValorRet *nSigno )	                     //FE_VALIMP
				aSFESUSS[Len(aSFESUSS)][5] := 100  						//FE_PORCRET
			   	If Alltrim(cConceito)=="1"
			   		aSFESUSS[Len(aSFESUSS)][6] := + nValorRet *nSigno
		  		ElseIf  Alltrim(cConceito)=="4"
		  			aSFESUSS[Len(aSFESUSS)][6] := + nValorRet *nSigno
		  		Else
				   	If nValRetCac > nLimMInRet
						aSFESUSS[Len(aSFESUSS)][6] := (nValorRet*nSigno)
			    	 Else
			    	 	aSFESUSS[Len(aSFESUSS)][6] :=  0
			    	 EndIf
		    	EndIf
		    	aSFESUSS[Len(aSFESUSS)][7] := nAliqRet * 100		//FE_ALIQ
	
		    	aSFESUSS[Len(aSFESUSS)][8] := cConceito            // FE_CONCEPT
		    	aSFESUSS[Len(aSFESUSS)][9] := cTipoCalc            // Tipo Calculo - ajuste de retenܧܣo pot total
		    	aSFESUSS[Len(aSFESUSS)][10] := cTpObr
				aSFESUSS[Len(aSFESUSS)][11] := SE2->E2_FORNECE
				aSFESUSS[Len(aSFESUSS)][12] := SE2->E2_LOJA
	
				If	nVRetSUSS = 0
					aSFESUSS[Len(aSFESUSS)][13] := IIf(lReSaSU,nValRetCac,0)
				Endif 
				aSFESUSS[Len(aSFESUSS)][14] := SF2->F2_ESPECIE
			EndIf 
	    EndIf
	EndIf
EndIf

Return aSFESUSS 

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetSLI � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetSLI   - Calculo de Ret de SLI para NF                ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetSLI(cAgente,nSigno,nSaldo,nA,cChavePOP,cNFPOP,cSeriePOP)
Local aSFESLI	:= {}
Local lCalcLimp	:= .T.
Local cChave	:= ""
Local nRateio	:= 0
Local nValRet	:= 0
Local nVlrBase	:= 0
Local nVlrTotal	:= 0
Local nAliq	    := 0
Local nI		:= 0
Local nRecSF1 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.

If type("cFornece")=="U"
	cFornece	:=SE2->E2_FORNECE
	cLoja		:=SE2->E2_LOJA
EndIf      

DEFAULT nSigno	:=	1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFESLI := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"L")
EndIf

//+---------------------------------------------------------------------+
//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten玟o.?
//+---------------------------------------------------------------------+
SA2->( dbSetOrder(1) )
If SA2->(MsSeek(If(lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2"),SE2->E2_MSFIL,xFilial("SA2"))+SE2->E2_FORNECE+SE2->E2_LOJA)) .And. SA2->(ColumnPos("A2_DTICALL")) > 0 ;
	.And. SA2->(ColumnPos("A2_DTFCALL")) > 0  .And. !Empty(SA2->A2_DTICALL) .And. !Empty(SA2->A2_DTFCALL)
    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALL) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALL) )
   		lCalcLimp	:=.F.
    EndIf
EndIf

If ExistBlock("F0851IMP")
	lCalcLimp:=ExecBlock("F0851IMP",.F.,.F.,{"SLI"})
EndIf
If Subs(cAgente,7,1) == "S" .And. lCalcLimp


	dbSelectArea("SF1")
	dbSetOrder(1)
	If lMsfil
		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	Else
		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	EndIf
	While !Eof() .And. (Alltrim(SF1->F1_ESPECIE) <> AllTrim(SE2->E2_TIPO))
		dbSkip()
		Loop
	End

	If (AllTrim(SF1->F1_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
		(Iif(lMsFil,SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
		F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA)

		nRateio := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / ROund(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) )

		cChave := SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA
		SD1->(DbSetOrder(1))
		If lMsFil
			SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
		Else
			SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
		EndIf
		While !SD1->(Eof()) .And. Iif(lMsFil,SD1->D1_MSFIL,xFilial("SD1",SE2->E2_FILORIG))==SD1->D1_FILIAL .And. SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == cChave
			If AllTrim(SD1->D1_ESPECIE) <> Alltrim(SF1->F1_ESPECIE)
				SD1->(DbSkip())
				Loop
			Endif

			SFF->(dbSetOrder(10))
			SFF->(MsSeek(xFilial()+"SLI"+SD1->D1_CF))


			If SFF->(Found())

				//Verifica as caracteristicas do TES para que os impostos
				//possam ser somados ao valor da base de calculo da retencao...
				aImpInf := TesImpInf(SD1->D1_TES)
				lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
				If !lTESNoExen
					SD1->(DbSkip())
					Loop
				EndIf
				
				nAliq     := SFF->FF_ALIQ
				nVlrTotal += SD1->D1_TOTAL

				For nI := 1 To Len(aImpInf)
					//Caso o fornecedor seja "Responsable Inscripto" o IVA
					//nao eh considerado na soma dos impostos...
					If (SA2->A2_TIPO == "I")
						If !("IVA"$Trim(aImpInf[nI][01])) .And. (aImpInf[nI][03] <> "3")
							nVlrTotal += (SD1->(FieldGet(ColumnPos(aImpInf[nI][02]))) * Iif(aImpInf[nI][03]=="1",1,-1))
						ElseIf ("IVA"$Trim(aImpInf[nI][01])) .And. (aImpInf[nI][03] == "3")
							nVlrTotal -= SD1->(FieldGet(ColumnPos(aImpInf[nI][02])))
						EndIf
					Else
						If aImpInf[nI][03] <> "3"
							nVlrTotal += (SD1->(FieldGet(ColumnPos(aImpInf[nI][02]))) * Iif(aImpInf[nI][03]=="1",1,-1))
						EndIf
					EndIf
				Next
			EndIf
			SD1->(dbSkip())
		End

		If nVlrTotal > 0
			nVlrBase := (nVlrTotal * nRateio)
			nValRet  := Round((nVlrBase*(nAliq/100))*nSigno,TamSX3("FE_VALIMP")[2])

			
			//?Gravar Retenciones.   
			
			AAdd(aSFESLI,array(6))
			aSFESLI[Len(aSFESLI)][1] := SF1->F1_DOC   //FE_NFISCAL
			aSFESLI[Len(aSFESLI)][2] := SF1->F1_SERIE //FE_SERIE
			aSFESLI[Len(aSFESLI)][3] := nSaldo	       //FE_VALBASE
			aSFESLI[Len(aSFESLI)][4] := nSaldo	       //FE_VALIMP
			aSFESLI[Len(aSFESLI)][5] := Round((nValRet*100)/nSaldo,2)//FE_PORCRET
			aSFESLI[Len(aSFESLI)][6] := nValRet
		EndIf
	EndIf
EndIf

Return aSFESLI

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetSL2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetSL2   - Calculo de Ret de SLI para NF                ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetSL2(cAgente,nSigno,nSaldo,nA,cChavePOP,cNFPOP,cSeriePOP)

Local aSFESLI 	:= {}
Local lCalcLimp	:= .T.
Local cChave  	:= ""
Local nRateio 	:= 0
Local nValRet 	:= 0
Local nVlrBase	:= 0
Local nVlrTotal	:= 0
Local nAliq   	:= 0
Local nI	  	:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE') 
Local lTESNoExen := .F. 
       
If	type("cFornece")=="U"
	cFornece	:=SE2->E2_FORNECE
	cLoja		:=SE2->E2_LOJA
EndIf


DEFAULT nSigno	:= -1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFESLI := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"L")
EndIf

SA2->( dbSetOrder(1)) 
If SA2->(MsSeek(If(lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2"),SE2->E2_MSFIL,xFilial("SA2"))+SE2->E2_FORNECE+SE2->E2_LOJA)) .And. SA2->(ColumnPos("A2_DTICALL")) > 0 ;
	.And. SA2->(ColumnPos("A2_DTFCALL")) > 0  .And. !Empty(SA2->A2_DTICALL) .And. !Empty(SA2->A2_DTFCALL)
    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALL) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALL) )
   		lCalcLimp	:=.F.
    EndIf
EndIf

If ExistBlock("F0851IMP")
	lCalcLimp:=ExecBlock("F0851IMP",.F.,.F.,{"SL2"})
EndIf

//+---------------------------------------------------------------------+
//?Obter Impostos somente qdo a Empresa Usuario for Agente de Reten玟o.?
//+---------------------------------------------------------------------+
If Subs(cAgente,7,1) == "S"   .And. lCalcLimp

	dbSelectArea("SF2")
	dbSetOrder(1)
	If lMsFil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf
	While !Eof() .And. (Alltrim(SF2->F2_ESPECIE) <> AllTrim(SE2->E2_TIPO))
		DbSkip()
		Loop
	Enddo

	If (AllTrim(SF2->F2_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
		(Iif(lMsFil,SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
		F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA)

		nRateio := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) )

		cChave := SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA
		SD2->(DbSetOrder(3))
		If lMsFil
			SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		Else
			SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		EndIf

		While !SD2->(Eof()) .And. Iif(lMsfil,SD2->D2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL .And. SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA == cChave
			If AllTrim(SD2->D2_ESPECIE) <> Alltrim(SF2->F2_ESPECIE)
				SD2->(DbSkip())
				Loop
			Endif

			SFF->( Iif(SD2->D2_CF < "500",dbSetOrder(10),dbSetOrder(12)) )
			SFF->(MsSeek(xFilial()+"SLI"+SD2->D2_CF))

			Do Case
				Case IsInCallStack("Fa085Alt") .and. n == nA 
					nSaldo:= &(ReadVar())
				case IsInCallStack("Fa085Alt").and. !Empty(Acols) .and. Acols[nA][4] == SF2->F2_DOC .and. aCols [nA][1] <> SF2->F2_VALBRUT .and. n <> 0	
				   	nSaldo:= Acols[nA][1]
				Otherwise													
					nSaldo:= nSaldo
			EndCase

			If SFF->(Found())

				//Verifica ascaracteristicas do TES para que os impostos
				//possam ser somados ao valor da base de calculo da retencao...
				aImpInf := TesImpInf(SD2->D2_TES)
				lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
				If !lTESNoExen
					SD2->(DbSkip())
					Loop
				EndIf
				
				nAliq     := SFF->FF_ALIQ
				nVlrTotal += SD2->D2_TOTAL

				For nI := 1 To Len(aImpInf)
					//Caso o fornecedor seja "Responsable Inscripto" o IVA
					//nao eh considerado na soma dos impostos...
					If (SA2->A2_TIPO == "I")
						If !("IVA"$Trim(aImpInf[nI][01])) .And. (aImpInf[nI][03] <> "3")
							nVlrTotal += (SD2->(FieldGet(ColumnPos(aImpInf[nI][02]))) * Iif(aImpInf[nI][03]=="1",1,-1))
						ElseIf ("IVA"$Trim(aImpInf[nI][01])) .And. (aImpInf[nI][03] == "3")
							nVlrTotal -= SD2->(FieldGet(ColumnPos(aImpInf[nI][02])))
						EndIf
					Else
						If aImpInf[nI][03] <> "3"
							nVlrTotal += (SD2->(FieldGet(ColumnPos(aImpInf[nI][02]))) * Iif(aImpInf[nI][03]=="1",1,-1))
						EndIf
					EndIf
				Next
			EndIf
			SD2->(dbSkip())
		End

		If nVlrTotal > 0
			nVlrBase := (nVlrTotal * nRateio)
			nValRet  := Round((nVlrBase*(nAliq/100))*nSigno,TamSX3("FE_VALIMP")[2])

			
			//?Gravar Retenciones.
			
			AAdd(aSFESLI,array(6))
			aSFESLI[Len(aSFESLI)][1] := SF2->F2_DOC   				 //FE_NFISCAL
			aSFESLI[Len(aSFESLI)][2] := SF2->F2_SERIE 				 //FE_SERIE
			aSFESLI[Len(aSFESLI)][3] := nSaldo	      					 //FE_VALBASE
			aSFESLI[Len(aSFESLI)][4] := nSaldo	       				 //FE_VALIMP
			aSFESLI[Len(aSFESLI)][5] := Round((nValRet*100)/nSaldo,2)//FE_PORCRET
			aSFESLI[Len(aSFESLI)][6] := nValRet
		EndIf
	EndIf
EndIf

Return aSFESLI



/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGRetGN � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetGN    - Calculo de Ret de Ganancias                  ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetGN(cAgente,nSigno,aConGan,cFornece,cLoja,cChavePOP)
Local aBasTtlRet:= {}
Local aSFEGn	:= {}
Local aAreaAtu 	:= {}
Local aAreaSA2	:= {}
Local aGanComp	:= {}
Local aGanRet	:= {0,.F.,.F.}
Local aArea		:=	GetArea()
Local lReduzGan	:= .T.
Local cConcAux	:= ""
Local cConcepto	:= ""
Local nCount	:= 0
Local nRetencMes:= 0
Local nAliq		:= 0
Local nDeduc	:= 0
Local nImposto	:= 0
Local nImpAtual	:= 0
Local nBaseAtual:= 0
Local nRetMinima:= 0
Local nPos		:= 0
Local nBasTtlRet:= 0
Local nValImpor	:= 0
Local nFator	:= 1
Local lVisSFE   := .F.
Local lDeduc		:= .F. 
Local lFDeduc		:= .F. 
Local nDeducMes := 0 
Local lMinera	:=.F.
Local cConGan	:= "04"
Local nFEDeduc  := 0
Local lRegOP := .F.
Local lRG424518 :=SFF->(ColumnPos("FF_ALNOIPF")) > 0       // Novo conceito de aliquota - RG 4245-18 - Nuevos Montos No Sujetos a Retenci�n - Personas F�sicas 
Local lSerieM:=.f.
Local aAreaAtu:=GetArea()
Local nRecSF2:= 0
Local nRecSF1 := 0
Local lCbuVld:= .F.
DEFAULT nSigno	:= 1
DEFAULT cChavePOP	:= ""

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEGN := ObtReten(cChavePOP,"","","G")
EndIf

If GetNewPar("MV_SEGMEN","") $ "1|2|3" .And. SA2->(ColumnPos("A2_TIPROVM"))>0 .AND.  SA2->A2_TIPROVM $ "1|2|3|4"
	lMinera:=.T.
EndIf	

If type("inclui")=="U"
	 inclui:=NIL
EndIf

If (SE2->E2_TIPO $ MVPAGANT + "/"+ MV_CPNEG)
	dbSelectArea("SF2")
	dbSetOrder(1)
	If lMsfil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf
	If (SA2->(ColumnPos("A2_CBUINI"))>0 .and. SA2->(ColumnPos("A2_CBUFIM"))>0) .and. SA2->A2_CBUINI <= SF2->F2_EMISSAO .and. SA2->A2_CBUFIM >= SF2->F2_EMISSAO .and. Subs(SF2->F2_SERIE,1,1) =="A"
		lCbuVld:=.T.
	EndIf
	
	If nRecSF2 > 0 .And. ( Subs(SF2->F2_SERIE,1,1)=="M" .Or. lCbuVld)
		lSerieM:=.T.
	EndIf
Else
	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))
	If lMsfil
		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	Else
		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	EndIf
	
	If (SA2->(ColumnPos("A2_CBUINI"))>0 .and. SA2->(ColumnPos("A2_CBUFIM"))>0) .and. SA2->A2_CBUINI <= SF1->F1_EMISSAO .and. SA2->A2_CBUFIM >= SF1->F1_EMISSAO .and. Subs(SF1->F1_SERIE,1,1) == "A"
		lCbuVld:=.T.
	EndIf
	
	If nRecSF1 > 0 .And.( Subs(SF1->F1_SERIE,1,1)=="M" .Or. lCbuVld)
		lSerieM:=.T.
	EndIf
EndIf
RestArea(aAreaAtu)

If !lSerieM .and. lRetpa .and. type("cSerienf")=="C" .and. Subs(cSerienf,1,1)=="M"
	lSerieM:=.T.
EndIf

For nCount:=1  to Len(aConGan)
	If aConGan[nCount][2] < 0
		aAdd(aGanComp,-1)
	Else
		aAdd(aGanComp,1)
	Endif
	If aConGan[nCount,1] == '02' .And. !Empty(aConGan[nCount,3])
		cConcepto := aConGan[nCount,3]
	Else
		If lCbuVld
			aConGan[nCount,1] := "MA"
		EndIf
		cConcepto := aConGan[nCount,1]
	Endif
	nPos := Ascan(aBasTtlRet,{|conceito| conceito[1] == cConcepto})
	If nPos == 0
		Aadd(aBasTtlRet,{cConcepto,0})
		nPos := Len(aBasTtlRet)
	Endif
	aBasTtlRet[nPos,2] += aConGan[nCount][2] * aGanComp[nCount]
Next nCount

//���������������������������������������������������������������������Ŀ
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Reten��o �
//� o el concepto sea =="07" donde siempre se debe retener,             �
//� y el total dos titulos for maior que 0.00                           �
//�����������������������������������������������������������������������
For nCount:=1  to Len(aConGan)
	SA2->( dbSetOrder(1) )
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
		SA2->( MsSeek(aConGan[nCount][5]+aConGan[nCount][4]) )
	Else
		SA2->( MsSeek(xFilial("SA2")+aConGan[nCount][4]) )
	Endif
	If SA2->A2_AGREGAN   == "00"
		Loop
	Endif
	If SA2->(ColumnPos("A2_DTIREDG")) > 0 .And. SA2->(ColumnPos("A2_DTFREDG")) > 0 ;
	   .And. !Empty(SA2->A2_DTIREDG) .And. !Empty(SA2->A2_DTFREDG)
	    If  ( Dtos(dDataBase)< Dtos(SA2->A2_DTIREDG) ) .Or. ( Dtos(Ddatabase) > Dtos(SA2->A2_DTFREDG) )
	    	lReduzGan:= .F.
	    EndIf
	EndIf

	If SA2->(ColumnPos("A2_DTICALG")) > 0 .And. SA2->(ColumnPos("A2_DTFCALG")) > 0 ;
	   .And. !Empty(SA2->A2_DTICALG) .And. !Empty(SA2->A2_DTFCALG)
	    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALG) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALG) )
	    	If SA2->A2_PORGAN == 0
	    		IF Empty(SA2->A2_CODCOND)
	   				Return(aSFEGN)
	   			Else
	   				Loop
	   			EndIF	   			
	   		EndIf
	   	Else
	   		lReduzGan:= .F.	   		
	    EndIf
	EndIf


	cFornece	:=	SA2->A2_COD
	cLoja		:=	SA2->A2_LOJA

	nBasRetMes := 0.00
	nRetencMes := 0.00
	cConcepto  := 	aConGan[nCount][1]
	cConcepto2 :=	aConGan[nCount][3]

	If !Empty(cConcepto).And.((subs(cAgente,1,1) == "S").Or.cConcepto=="07");
		.And. aConGan[nCount][2] <> 0.00 ;
		.And. cConcepto <> '00'

		If ( cConcepto#"07".AND.(subs(cAgente,1,1) # "S") )
			Return(aSFEGN)
		EndIf

		//�������������������������������������������������������������������������Ŀ
		//� Varrer arquivo de Retencoes para obter acumulados do mes como condomino �
		//���������������������������������������������������������������������������
		If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855"
			ObtGan(@nRetencMes,@nFEDeduc,@nBasRetMes,cFORNECE,cLOJA,cConcepto,.T.)
			nBasRetMes := nBasRetMes
		EndIf
		
		dbSelectArea("SFE")
		dbSetOrder(5)
		MsSeek(xFilial("SFE")+cFORNECE+cLOJA)
		If Found()
			ProcRegua(RecCount())
			While !Eof() .And. FE_FILIAL  == xFilial("SFE");
				.And. FE_FORCOND == cFORNECE;
				.And. FE_LOJCOND == cLOJA

				IncProc()

				If Month(FE_EMISSAO) != Month(dDataBase) .Or.YEAR(FE_EMISSAO)!=Year(dDataBase) .Or.;
					FE_TIPO != "G" .Or. FE_CONCEPT != cConcepto
					dbSkip()
					Loop
				EndIf

				nBasRetMes := nBasRetMes + FE_VALBASE

				nRetencMes	:= nRetencMes + FE_RETENC
				dbSkip()
			Enddo
		EndIf
		
		If lShowPOrd .and. TableInDic("FVC") .and. Funname() == "FINA855" .and. !(SA2->A2_CONDO $ "1|2")
			ObtGan(@nRetencMes,@nDeducMes,@nBasRetMes,cFORNECE,cLOJA,cConcepto)
			If nDeducMes>0 .or. nBasRetMes>0
				nBasRetMes := nBasRetMes - nDeducMes
				lFDeduc := .T.
				lRegOP := .T. 
			EndIf
		EndIf
		
		dbSelectArea("SFE")
		dbSetOrder(3)
		MsSeek(xFilial("SFE")+cFORNECE+cLOJA)
		If Found()
			ProcRegua(RecCount())
			While !Eof() .And. FE_FILIAL  == xFilial("SFE");
				.And. FE_FORNECE == cFORNECE;
				.And. FE_LOJA    == cLOJA

				IncProc()

				If Month(FE_EMISSAO) != Month(dDataBase) .Or.YEAR(FE_EMISSAO)!=Year(dDataBase) .Or. Iif(!Empty(SFE->FE_DTRETOR), Month(SFE->FE_DTRETOR) != Month(dDataBase),.F.) .Or.;
					FE_TIPO != "G" .Or. FE_CONCEPT != cConcepto
					dbSkip()
					Loop
				EndIf
				lRegOP := .T. 
				nBasRetMes := nBasRetMes + FE_VALBASE - iif((FE_CONCEPT!="07"),FE_DEDUC,0)
				nDeducMes	:= nDeducMes + FE_DEDUC
				nRetencMes := nRetencMes + FE_RETENC
				If FJK->FJK_ORDPAG <> NIL
					If FJK->FJK_ORDPAG == SFE->FE_ORDPAGO    
						lDeduc := IIF(FE_DEDUC > 0, .T., .F.)
					Else
						lFDeduc := .T.
					EndIf
				EndIf
				dbSkip()
			Enddo
		EndIf

		//������������������������������������������������������������Ŀ
		//� Obter Base Atual para c�lculo da Gan�ncia.                 �
		//� Base Atual := ( Base Acumulada do mes + Total das NFs -    �
		//�                 Minimo disponivel mensal )                 �
		//��������������������������������������������������������������
		nAliq      := 0.00
		nDeduc     := 0.00
		nImposto   := 0.00
		nImpAtual  := 0.00

		nBaseAtual := nBasRetMes + aConGan[nCount][2]

		//������������������������������������������������������������������Ŀ
		//� Buscar el Valor de Retencion Minima.                             �
		//��������������������������������������������������������������������
		If cConcepto == '02' .And. !Empty(cConcepto2)
			cConcAux := cConcepto2+Space(TamSx3("FF_NUM")[1] - TamSx3("FF_ITEM")[1])
		Else
			cConcAux := cConcepto+Space(TamSx3("FF_NUM")[1] - TamSx3("FF_ITEM")[1])
		Endif
		aAreaAtu:=GetArea()
		aAreaSA2:= SA2->(GetArea())
		dbSelectArea("SA2")
		dbSetOrder(1)

		If MsSeek(xFilial("SA2")+cFORNECE+cLOJA) .AND. SA2->A2_INSCGAN == "N"
			nRetMinima :=0
		Else
			dbSelectArea("SFF")
			dbSetOrder(1)
			If !lMinera .And. MsSeek(xFilial("SFF")+cConcAux+"12")
				nRetMinima	:=	FF_IMPORTE
			Else
				// Caso ja tenha sido realizada manutencao no arquivo SFF, atraves da rotina MATA994.
				cConcAux := Space(TamSx3("FF_NUM")[1])
				If !lMinera .And. MsSeek(xFilial("SFF")+cConcAux+"12")
					nRetMinima	:=	FF_IMPORTE
				Else
					// Mantem a compatibilidade com o legado, caso nao tenha sido realizada manutencao
					// no arquivo SFF, atraves da rotina MATA994.
					cConcAux := StrZero(1,TamSx3("FF_NUM")[1])
					If !lMinera .And. MsSeek(xFilial("SFF")+cConcAux+"12")
						nRetMinima	:=	FF_IMPORTE
					Else
						nRetMinima	:=	0
					EndIf
				EndIf
			EndIf
        EndIf
		If lSerieM
			nRetMinima	:=	0
		EndIf
		RestArea(aAreaAtu)
		SA2->(RestArea(aAreaSA2))
		//������������������������������������������������������������������Ŀ
		//� Verificar que ACTIVIDAD desempe�a el proveedor para la retencion �
		//� de IG.                                                           �
		//��������������������������������������������������������������������
		dbSelectArea("SFF")
		dbSetOrder(2)
		If cConcepto == '02' .And. !Empty(cConcepto2)
			If "FF_IMPOSTO" $ IndexKey()
				MsSeek(xFilial("SFF")+cConcepto2+'GAN')
			Else
				MsSeek(xFilial("SFF")+cConcepto2)
			Endif
		Else
			If "FF_IMPOSTO" $ IndexKey()
				MsSeek(xFilial("SFF")+cConcepto+'GAN')
			Else
				MsSeek(xFilial("SFF")+cConcepto)
			Endif
		Endif
		If SFF->FF_ESCALA $ "D| "
			//���������������������������������������������������������������Ŀ
			//� Posicionar no Item "06" da Tabela de Ganancias para Obter     �
			//� os percentuais. (Item "06" Conceito):                         �
			//� Venta de Bienes de Cambio, Bienes Muebles; Locaciones de Obra �
			//� Y/O Servicios; Transferencia Definitiva de Llaves, Marcas,    �
			//� Patentes de Invencion, Regalias, Concesion y Similares.       �
			//�����������������������������������������������������������������
			dbSelectArea("SFF")
			dbSetOrder(2)
			If cConcepto == '02' .And. !Empty(cConcepto2)
				If "FF_IMPOSTO" $ IndexKey()
					MsSeek(xFilial("SFF")+cConcepto2+'GAN')
				Else
					MsSeek(xFilial("SFF")+cConcepto2)
				Endif
				nPos := Ascan(aBasTtlRet,{|conceito| conceito[1] == cConcepto2})
			Else
				If "FF_IMPOSTO" $ IndexKey()
					MsSeek(xFilial("SFF")+cConcepto+'GAN')
				Else
					MsSeek(xFilial("SFF")+cConcepto)
				Endif
				nPos := Ascan(aBasTtlRet,{|conceito| conceito[1] == cConcepto})
			Endif

			nBasTtlRet := nBaseAtual

			If ( nBasTtlRet + nDeducMes > FF_IMPORTE ) .or.  SA2->A2_INSCGAN == "N" .or. lMinera
				//������������������������������������������������������������Ŀ
				//� Calcular Gan�ncia baseando na Tabela de Gan�ncias.         �
				//��������������������������������������������������������������
				//�����������������������������������������������������������������������Ŀ
				//� Calculo da Gan�ncia:                                                  �
				//� Imposto := ( Retencao+Base de Calculo) * (Alquota Inscrito/100)       �
				//� Imposto := Imposto Atual - Impostos ja retidos no m�s.                �
				//�������������������������������������������������������������������������
				If SA2->A2_INSCGAN == "S"
					If lMinera
						If (SA2->A2_TIPROVM $ "1|2|3") 
							nAliq    := FF_ALSIMIN
						Elseif SA2->A2_TIPROVM $ "4" 
							nAliq    := FF_ALNOMIN
						EndIf
					Else	
						nAliq    := FF_ALQINSC
					EndIf
					nFator   := nBaseAtual/nBasTtlRet
					
					If ExistBlock("A850FTGN")
						nFator :=	ExecBlock("A850FTGN",.F.,.F.,{cConcepto,nFator})
					Endif
					
					If lMinera .And. SA2->A2_TIPROVM=="4"
						nDeduc   :=  0
					Else
						nDeduc   := FF_IMPORTE * nFator
					EndIf   					
					
					// Validacion de Importe > 0 
					IF ((!lSerieM .and. FF_IMPORTE > 0 ) .or.  lSerieM )  .And. SA2->A2_CONDO $ "1|2"
						nDeduc   := Iif(!lSerieM,FF_IMPORTE,0)
						If nRetencMes == 0 .and. nBasRetMes == 0
							nBaseAtual := nBaseAtual-IIf(!lSerieM,FF_IMPORTE,0)
						EndIf
						nImposto := ((FF_RETENC) + ((nBaseAtual*((100-FF_REDBASE)/100)) ) * (nAliq/100)) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
					Else
						If Inclui != Nil
							lVisSFE := Inclui
						Endif
						if cPaisloc == 'ARG' .and. FunName() == 'FINA855' .and. nRetencMes > 0 //if para calcular o imposto com dedu��o caso eu j� possua a reten��o e a base de reten��o registrados en base, em caso de modifica��o do registro existente
							nImposto := ((FF_RETENC) + ((nBaseAtual*((100-FF_REDBASE)/100)) - IIf(nRetencMes > 0 .and. nBasRetMes > 0,nDeduc,IIf(lFDeduc .and. nRetencMes > 0 .and. nDeducMes > 0,nDeduc,IIf(lVisSFE,0,IIF(lDeduc,nDeduc,0)))) ) * (nAliq/100)) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)  
						else
							nImposto := ((FF_RETENC) + ((nBaseAtual*((100-FF_REDBASE)/100)) - IIf(nRetencMes == 0 .and. nBasRetMes == 0,nDeduc,IIf(lFDeduc .and. nRetencMes == 0 .and. nDeducMes == 0,nDeduc,IIf(lVisSFE,0,IIF(lDeduc,nDeduc,0)))) ) * (nAliq/100)) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
						endif
					EndIF
					nImpAtual:= nImposto-nRetencMes
				Else
					If Alltrim(SA2->A2_TPESSOA) == "PF" .And.  lRG424518
						nAliq    := FF_ALNOIPF  
					Else
						nAliq    := FF_ALQNOIN
					EndIf
					nDeduc   := 0
					nImposto := ((FF_RETENC) + ((nBaseAtual*((100-FF_REDBASE)/100)) - nDeduc ) * (nAliq/100))* Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
					nImpAtual:= nImposto -  nRetencMes
				EndIf
			ElseIf SA2->A2_CONDO $ "1|2" .and. SA2->A2_INSCGAN == "S" .and. ((!lSerieM .and. FF_IMPORTE > 0 ).or.  lSerieM ).and. nRetencMes == 0 .and. nBasRetMes == 0
				nDeduc   := IIf(!lSerieM,FF_IMPORTE,0)
				nBaseAtual := nBaseAtual-IIf(!lSerieM,FF_IMPORTE,0)
				nAliq    := FF_ALQINSC
				nImpAtual := 0
			EndIf

		Else
            nValImpor:= IIf(!lSerieM,SFF->FF_IMPORTE,0)
			//���������������������������������������������������������������Ŀ
			//� Posicionar no Item "07" da Tabela de Ganancias para Obter     �
			//� os percentuais. (Item "07" Conceito):                         �
			//� Ejercicio de Profesiones Liberales u Oficios; Sindico; Manda- �
			//� tario; Director de Sociedades Anonimas; Corredor; Viajante de �
			//� Comercio y Despachante de Aduana.                             �
			//�����������������������������������������������������������������
			/*dbSelectArea("SFF")
			dbSetOrder(2)
			If "FF_IMPOSTO" $ IndexKey()
				MsSeek(xFilial("SFF")+"07"+"GAN")
			Else
				MsSeek(xFilial("SFF")+"07")
			Endif*/
			If SFF->FF_ITEM $ "DA"
			//Derechos de Autor
				aGanRet := F085TotGan(IIf(ColumnPos("FF_TPLIM") > 0,SFF->FF_TPLIM,"0"),SA2->A2_COD,SA2->A2_LOJA,SFF->FF_LIMITE,aConGan[1][2])
				If aGanRet[2]
					nBaseAtual := aGanRet[1]
					If SA2->A2_INSCGAN == "N"
						nImposto := ((SFF->FF_RETENC) + (nBaseAtual  * (SFF->FF_ALQNOIN/100))) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
						nImpAtual:= nImposto-nRetencMes
						If Alltrim(SA2->A2_TPESSOA) == "PF" .And.  lRG424518
							nAliq    := FF_ALNOIPF  
						Else
							nAliq    := SFF->FF_ALQNOIN
						EndIf
						nDeduc   := 0
					Else
						//������������������������������������������������������������Ŀ
						//� Adotar a Escala Aplicable.                                 �
						//��������������������������������������������������������������
						dbSelectArea("SFF")
						dbSetOrder(2)
						If "FF_IMPOSTO" $ IndexKey()
							MsSeek(xFilial("SFF")+"13"+"GAN")
						Else
							MsSeek(xFilial("SFF")+"13")
						Endif
						While !Eof()
							//�����������������������������������������������������������������������Ŀ
							//� Calculo da Gan�ncia:                                                  �
							//� Imposto := Retencao+(Base de Calculo-Faixa Tabela) * (Percentual/100) �
							//� Imposto := Imposto Atual - Impostos ja retidos no m�s.                �
							//�������������������������������������������������������������������������
							If (nBaseAtual - nValImpor) < FF_FXATE
								nImposto := (FF_RETENC + (nBaseAtual- nValImpor -FF_FXDE)* (FF_PERC/100))* Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
								nImpAtual := nImposto - nRetencMes
								If lMinera
									If (SA2->A2_TIPROVM $ "1|2|3") 
										nAliq    := FF_ALSIMIN
									Elseif SA2->A2_TIPROVM $ "4" 
										nAliq    := FF_ALNOMIN
									EndIf
								Else
									nAliq :=FF_PERC
								EndIf
								nDeduc:=nValImpor//+FF_FXDE
								
								Exit
							EndIf
				    		dbSkip()
						EndDo
					EndIf
				EndIf
			Else
				If SA2->A2_INSCGAN == "N"
					nImposto := ((FF_RETENC) + (nBaseAtual  * (FF_ALQNOIN/100))) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
					nImpAtual:= nImposto-nRetencMes
					If Alltrim(SA2->A2_TPESSOA) == "PF" .And.  lRG424518
						nAliq    := FF_ALNOIPF  
					Else	
						nAliq    := FF_ALQNOIN
					EndIf
					nDeduc   := 0
				Else
					//������������������������������������������������������������Ŀ
					//� Adotar a Escala Aplicable.                                 �
					//��������������������������������������������������������������
					If "FF_IMPOSTO" $ IndexKey()
						MsSeek(xFilial("SFF")+"13"+"GAN")
					Else
						MsSeek(xFilial("SFF")+"13")
					Endif
					While !Eof()
						//�����������������������������������������������������������������������Ŀ
						//� Calculo da Gan�ncia:                                                  �
						//� Imposto := Retencao+(Base de Calculo-Faixa Tabela) * (Percentual/100) �
						//� Imposto := Imposto Atual - Impostos ja retidos no m�s.                �
						//�������������������������������������������������������������������������

						If ((nBaseAtual - nValImpor) < FF_FXATE )//(lRegOP .and. nBaseAtual < FF_FXATE) .OR. (!lRegOP .and. (nBaseAtual - nValImpor) < FF_FXATE )
							If  Inclui!=Nil
								lVisSFE := Inclui
							Endif 
							If nRetencMes == 0 .and. nBasRetMes == 0//IIf(lVisSFE,0,IIF(lDeduc,nDeduc,0))
								nImposto := (FF_RETENC + (nBaseAtual- nValImpor -FF_FXDE)* (FF_PERC/100))* Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
								nDeduc:=nValImpor
							Else
								nImposto := (IIF( lVisSFE, FF_RETENC,IIF(lDeduc, FF_RETENC * 2, FF_RETENC)) + ( ( nBaseAtual - nValImpor - IIf(lVisSFE,0,IIF(lDeduc,nValImpor + FF_FXDE,0) )- FF_FXDE)* (FF_PERC/100) ) ) * Iif(lReduzGan,(SA2->A2_PORGAN/100),1)
							EndIf
							nImpAtual := nImposto - nRetencMes
							If lMinera
								If (SA2->A2_TIPROVM $ "1|2|3") 
									nAliq    := FF_ALSIMIN
								Elseif SA2->A2_TIPROVM $ "4" 
									nAliq    := FF_ALNOMIN
								EndIf
							Else
								nAliq :=FF_PERC
							EndIf
							If lMinera .And. SA2->A2_TIPROVM=="4"
								nDeduc   :=  0
							EndIf
							Exit
						EndIf
			    		dbSkip()
					End
				EndIf
			EndIf
		EndIf
		//����������������������������������������������������������������Ŀ
		//� Generar las Retenci�n de Gan�ncias                             �
		//������������������������������������������������������������������
		Aadd(aSFEGn,Array(12))
		aSFEGn[Len(aSFEGn)][1] := ""
		aSFEGn[Len(aSFEGn)][2] := IIf(aGanRet[3],(nBaseAtual),IIf(EMPTY(SA2->A2_CODCOND),aConGan[nCount][2] ,(nBaseAtual - nBasRetMes))) // FE_VALBASE
		aSFEGn[Len(aSFEGn)][3] := nAliq                                         // FE_ALIQ
		aSFEGn[Len(aSFEGn)][4] := Round(Iif(nImpAtual >= nRetMinima, nImpAtual, 0 ) * nSigno, TamSX3("FE_VALIMP")[2]) // FE_VALIMP
		aSFEGn[Len(aSFEGn)][5] := Round(Iif(nImpAtual >= nRetMinima, nImpAtual, 0 ) * nSigno, TamSX3("FE_RETENC")[2]) // FE_RETENC
		if cPaisloc == 'ARG' .and. FunName() == 'FINA855' .and. LFDeduc
			aSFEGn[Len(aSFEGn)][6] := IIf(nRetencMes > 0 .and. nBasRetMes > 0 .and. (nImpAtual + nRetencMes >= nRetMinima),nDeduc * aGanComp[nCount],IIf(lFDeduc .and. nRetencMes >= 0 .and. (nImpAtual + nRetencMes >= nRetMinima),nDeduc,0))    // FE_DEDUC
		else
			aSFEGn[Len(aSFEGn)][6] := IIf(nRetencMes == 0 .and. nBasRetMes == 0 .and. (nImpAtual + nRetencMes >= nRetMinima),nDeduc * aGanComp[nCount],IIf(lFDeduc .and. nRetencMes == 0 .and. (nImpAtual + nRetencMes >= nRetMinima),nDeduc,0))    // FE_DEDUC
		endif
		aSFEGn[Len(aSFEGn)][7] := cConcepto                                     // FE_CONCEPT
		aSFEGn[Len(aSFEGn)][8] := Iif(lReduzGan,SA2->A2_PORGAN,100)             // FE_PORCRET
		aSFEGn[Len(aSFEGn)][9] := cConcepto2                                    // FE_CONCEPT
   		If SA2->(ColumnPos("A2_CODCOND")*ColumnPos("A2_CONDO")*ColumnPos("A2_PERCCON")) > 0 .And.SA2->A2_CONDO == "2"
			aSFEGn[Len(aSFEGn)][10] := SA2->A2_COD                              // FE_PORCRET
			aSFEGn[Len(aSFEGn)][11] := SA2->A2_LOJA                                // FE_PORCRET
		Else
			aSFEGn[Len(aSFEGn)][10] := ''                              // FE_PORCRET
			aSFEGn[Len(aSFEGn)][11] := ''                              // FE_PORCRET
		Endif
		aSFEGn[Len(aSFEGn)][12] := Iif(aGanComp[nCount] < 0, .T., .F.) //NC - conceitos diferentes
	EndIf
Next
RestArea(aArea)
Return aSFEGN


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetGNMnt � Autor �  Bruno Schmidt   � Data �  14/08/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � Calculo de IVA para monotributista                         ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetGNMnt(cAgente,nSigno,aConGan,cFornece,cLoja,cDoc,cSerie,lPa,nTTit,cChavePOP)
Local aSFEGn   	:= {}
Local aGanComp	:= {}
Local aArea		:=	GetArea()
Local lReduzGan	:= .T.
Local lCalcMon 	:= .F. //Validacao do calculo de Ganancia e IVA para monotributista
Local nAliq		:= 0
Local nImposto	:= 0
Local nBaseAtual:= 0
Local nRetMinima:= 0
Local nMinimo 	:= 0
Local nMinUnit	:= 0
Local nI		:= 0
Local lMineira	:= .F.
Local aAreaAtu  :=GetArea()
Local lSerieM   :=.F.
DEFAULT nSigno	:=	1
DEFAULT lPa 	:= .F.
DEFAULT cChavePOP	:= ""

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEGN := ObtReten(cChavePOP,"","","G")
EndIf


If  SA2->(ColumnPos("A2_TIPROVM"))>0 .AND.  GetNewPar("MV_SEGMEN","") $ "1|2|4"  .And. SA2->A2_TIPROVM $ "1|2|3|4" 
	lMinera:=.T.
EndIf

If (SE2->E2_TIPO $ MVPAGANT + "/"+ MV_CPNEG)
	dbSelectArea("SF2")
	dbSetOrder(1)
	If lMsfil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf	
	
	If nRecSF2 > 0 .And.(  Subs(SF2->F2_SERIE,1,1)=="M")
		lSerieM:=.T.
	EndIf
Else
	dbSelectArea("SF1")
	SF1->(dbSetOrder(1))
	If lMsfil
		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	Else
		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	EndIf	
	
	If nRecSF1 > 0 .And. (Subs(SF1->F1_SERIE,1,1)=="M")
		lSerieM:=.T.
	EndIf
EndIf
RestArea(aAreaAtu)


If !lSerieM .and. lRetpa .and. type("cSerienf")=="C" .and. Subs(cSerienf,1,1)=="M"
	lSerieM:=.T.
EndIf

For nI:=1  to Len(aConGan)
	If aConGan[nI][2] < 0
		aAdd(aGanComp,-1)
	Else
		aAdd(aGanComp,1)
	Endif
Next nI

For nI := 1 to Len(aConGan)

	dbSelectArea("SFF")
	dbGoTop()
	dbSetOrder(2)	

	If MsSeek(xFilial("SFF") + aConGan[nI][1] + 'GAN')
		If aConGan[nI][1] $ "G1|G2"
			nMinimo  	:= Iif(SFF->(ColumnPos("FF_LIMITE"))>0,SFF->FF_LIMITE,0)
			nMinUnit 	:= Iif(SFF->(ColumnPos("FF_MINUNIT"))>0,SFF->FF_MINUNIT,0)
			nRetMinima 	:= IIf(!lSerieM,SFF->FF_IMPORTE,0)
		Endif

		If SFF->FF_TIPO == "M"
			//Verifica se deve calcular a Ganancia
			lCalcMon := F850CheckLim(aConGan[nI][1],,cFornece,nMinimo,cDoc,cSerie,nMinUnit,"GAN",lPa,nTTit,Iif(lMsFil,SE2->E2_MSFIL,""))
		Endif
	Endif

	If lCalcMon
		SA2->( dbSetOrder(1) )
		If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
			SA2->( MsSeek(aConGan[nCount][5]+aConGan[nCount][4]) )
		Else
			SA2->( MsSeek(xFilial("SA2")+aConGan[nI][4]) )
		Endif

		If SA2->(ColumnPos("A2_DTIREDG")) > 0 .And. SA2->(ColumnPos("A2_DTFREDG")) > 0 ;
		   .And. !Empty(SA2->A2_DTIREDG) .And. !Empty(SA2->A2_DTFREDG)
		    If  ( Dtos(dDataBase)< Dtos(SA2->A2_DTIREDG) ) .Or. ( Dtos(Ddatabase) > Dtos(SA2->A2_DTFREDG) )
		    	lReduzGan:= .F.
		    EndIf
		EndIf

		If SA2->(ColumnPos("A2_DTICALG")) > 0 .And. SA2->(ColumnPos("A2_DTFCALG")) > 0 ;
		   .And. !Empty(SA2->A2_DTICALG) .And. !Empty(SA2->A2_DTFCALG)
		    If  ( Dtos(dDataBase)>= Dtos(SA2->A2_DTICALG) ) .And. ( Dtos(Ddatabase) <= Dtos(SA2->A2_DTFCALG) )
		   		If SA2->A2_PORGAN == 0
	   				Return(aSFEGN)	   			
	   			EndIf
		    EndIf
		EndIf

		cFornece	:=	SA2->A2_COD
		cLoja		:=	SA2->A2_LOJA


		If !Empty(aConGan[nI][1]).And. aConGan[nI][2] <> 0.00

			nAliq      := 0.00
			nImposto   := 0.00

			nBaseAtual := aConGan[nI][2] * aGanComp[nI]

			dbSelectArea("SFF")
			dbSetOrder(2)
			MsSeek(xFilial("SFF") + aConGan[nI][1] + 'GAN')

			If ( nBaseAtual > SFF->FF_IMPORTE ) .or. lSerieM
				//������������������������������������������������������������Ŀ
				//� Calcular Gan�ncia baseando na Tabela de Gan�ncias.         �
				//��������������������������������������������������������������
				//�����������������������������������������������������������������������Ŀ
				//� Calculo da Gan�ncia:                                                  �
				//� Imposto := ( Retencao+Base de Calculo) * (Alquota Inscrito/100)       �
				//�������������������������������������������������������������������������

					nAliq    := SFF->FF_ALQNOIN
					nImposto := ((nBaseAtual - Iif(!lSerieM,SFF->FF_IMPORTE,0)) * (nAliq/100))* Iif(lReduzGan,(SA2->A2_PORGAN/100),1) * aGanComp[nI]

				//����������������������������������������������������������������Ŀ
				//� Generar las Retenci�n de Gan�ncias                             �
				//������������������������������������������������������������������
				Aadd(aSFEGn,Array(12))
				aSFEGn[Len(aSFEGn)][1] := ""
				aSFEGn[Len(aSFEGn)][2] := aConGan[nI][2]                            		// FE_VALBASE
				aSFEGn[Len(aSFEGn)][3] := nAliq                                         // FE_ALIQ
				aSFEGn[Len(aSFEGn)][4] := Round(nImposto * nSigno,TamSX3("FE_VALIMP")[2])  	// FE_VALIMP
				aSFEGn[Len(aSFEGn)][5] := Round(nImposto * nSigno,TamSX3("FE_RETENC")[2])  	// FE_RETENC
				aSFEGn[Len(aSFEGn)][6] := 0/*nDeduc*/                                   // FE_DEDUC
				aSFEGn[Len(aSFEGn)][7] := aConGan[nI][1]                                // FE_CONCEPT
				aSFEGn[Len(aSFEGn)][8] := Iif(lReduzGan,SA2->A2_PORGAN,100)             // FE_PORCRET
				aSFEGn[Len(aSFEGn)][9] := aConGan[nI][3]                               	// FE_CONCEPT
		   		If SA2->(ColumnPos("A2_CODCOND")*ColumnPos("A2_CONDO")*ColumnPos("A2_PERCCON")) > 0 .And.SA2->A2_CONDO == "2"
					aSFEGn[Len(aSFEGn)][10] := SA2->A2_COD                               // FE_PORCRET
					aSFEGn[Len(aSFEGn)][11] := SA2->A2_LOJA                              // FE_PORCRET
				Else
					aSFEGn[Len(aSFEGn)][10] := ''                              			// FE_PORCRET
					aSFEGn[Len(aSFEGn)][11] := ''                             				// FE_PORCRET
				Endif
				aSFEGn[Len(aSFEGn)][12] := Iif(aGanComp[nI] < 0,.T.,.F.)
			Endif
		EndIf
	Endif
Next nI
RestArea(aArea)
Return aSFEGN

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �  ARGCpr  � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGCpr      - Calculo de Ret de CPR para NF                ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGCpr(cAgente,nSigno,nSaldo)
Local aConCpr	:= {}
Local aConCprRat:= {}
Local aImpInf 	:= {}
Local lCalcGN 	:= .T.
Local lCalcula	:= .F.
Local cConc		:= ""
Local nRateio 	:= 0
Local nRatValImp:= 0
Local nValMerc	:= 0
Local nTotOrden	:= 0
Local nPosGan 	:= 0
Local nX 		:= 1
Local nY 		:= 1
Local nMoeda  	:= 1
Local nI		:= 1
Local nVlrTotal	:= 0
Local nAliqSFH	:= 0
Local nValRet	:= 0
Local nRecSF1 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.

DEFAULT nSigno	:= 1

If ExistBlock("F0851IMP")
	lCalcGN:=ExecBlock("F0851IMP",.F.,.F.,{"GN"})
EndIf

DbSelectArea("SFH")
DbSetOrder(1)
If MsSeek(xFilial("SFH")+SE2->E2_FORNECE+SE2->E2_LOJA+"CPR")
  	lCalcula:=.T.
	nAliqSFH:= SFH->FH_ALIQ
EndIf


//+----------------------------------------------------------------+
//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
//� do titulo pelo total da Nota Fiscal.                           �
//+----------------------------------------------------------------+
If lCalcula .and.  !(SE2->E2_TIPO $ MV_CPNEG) .and. A085aVigSFH()
	dbSelectArea("SF1")
	dbSetOrder(1)
		If lMsFil
			nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
			SF1->(dbGoTo(nRecSF1))
		Else
			nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
			SF1->(dbGoTo(nRecSF1))
		EndIf
	While Alltrim(SF1->F1_ESPECIE)<>Alltrim(SE2->E2_TIPO).And.!Eof()
		SF1->(DbSkip())
		Loop
	Enddo

	If Alltrim(SF1->F1_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
		Iif(lMsFil, SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
		F1_FILIAL+F1_DOC+PadR(SF1->F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA

		nMoeda      := Max(SF1->F1_MOEDA,1)

		If cPaisLoc<>"ARG"
			nRateio := 1
		else
			nRateio := ( SF1->F1_VALMERC - SF1->F1_DESCONT ) / SF1->F1_VALBRUT
		Endif

		nRatValImp	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) )
		nValMerc 	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
		If SF1->F1_FRETE > 0
			MV_FRETLOC := GetNewPar("MV_FRETLOC","IVA162GAN06")
			cConc   := Alltrim(Subs( MV_FRETLOC, At("N",MV_FRETLOC)+1))
			nPosGan := ASCAN(aConCpr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+(Round(xMoeda(SF1->F1_FRETE,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp)
			Else
				Aadd(aConCpr,{cConc,Round(xMoeda(SF1->F1_FRETE,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf
		If SF1->F1_DESPESA >  0
			MV_GASTLOC  := GetNewPar("MV_GASTLOC","IVA112GAN06")
			cConc   := Alltrim(Subs( MV_GASTLOC, At("N",MV_GASTLOC)+1))
			nPosGan := ASCAN(aConCpr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+Round(xMoeda(SF1->F1_DESPESA,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp
			Else
				Aadd(aConCpr,{cConc,Round(xMoeda(SF1->F1_DESPESA,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf

		SD1->(DbSetOrder(1))
			If lMsFil
				SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			Else
				SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			EndIf
		If SD1->(Found())
		    Do while Iif(lMsFil, SD1->D1_MSFIL,xFilial("SD1",SE2->E2_FILORIG))==SD1->D1_FILIAL.And.SF1->F1_DOC==SD1->D1_DOC.AND.;
				SF1->F1_SERIE==SD1->D1_SERIE.AND.SF1->F1_FORNECE==SD1->D1_FORNECE;
				.AND.SF1->F1_LOJA==SD1->D1_LOJA.And.!SD1->(EOF())

				IF AllTrim(SD1->D1_ESPECIE)<>AllTrim(SE2->E2_TIPO)
					SD1->(DbSkip())
					Loop
				Endif
					aImpInf := TesImpInf(SD1->D1_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD1->(DbSkip())
						Loop
					EndIf
					SFF->(DbSetOrder(5))

					 // Imposto +CFO
					If SFF->(MsSeek(xfilial("SFF")+"CPR"+Pad(SD1->D1_CF,len(SFF->FF_CFO)) )) .and. Round(xMoeda(( SF1->F1_VALMERC - SF1->F1_DESCONT ),SF1->F1_MOEDA,1,,,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) > SFF->FF_FXDE

			 			nAliq:= SFF-> FF_ALIQ

			 			If nAliqSFH>0
			 				nAliq:=nAliqSFH
			 			EndIf                            
							If ColumnPos("F1_CFO") > 0
				 			 	nPosGan:=ASCAN(aConCpr,{|x| x[1]==Pad(SF1->F1_CFO,len(SFF->FF_CFO_C)) })
							EndIf					 					 		
							If nPosGan <>0
								nValor:=(Round(xMoeda((SD1->D1_TOTAL-SD1->D1_VALDESC+nVlrTotal),SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp)
								nValRet:=nValor *  nAliq
								Aadd(aConCpr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CPR",nValor,nValRet,SFF->FF_ALIQ,SF1->F1_SERIE,SF1->F1_DOC,SF1->F1_FORNECE,SF1->F1_LOJA,,})
								aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+nValor
								aConCpr[nPosGan][3]:=aConCpr[nPosGan][3]+nValRet
							Else
								nValor:=Round(xMoeda((SD1->D1_TOTAL-SD1->D1_VALDESC+nVlrTotal),SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp
								nValRet:=Round(nValor *(nAliq/100),4)
								Aadd(aConCpr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CPR",nValor,nValRet,nAliq,SF1->F1_SERIE,SF1->F1_DOC,SF1->F1_FORNECE,SF1->F1_LOJA,,})

						EndIf

						// cfo , impostp, base, valor ret
						If (SFF->FF_IMPORTE <  SD1->D1_TOTAL*(SFF->FF_ALIQ   / 100))
					   		AAdd(aConCprRat,array(10))
							aConCprRat[Len(aConCprRat)][1] := aConCpr[1][1] //CFO
							aConCprRat[Len(aConCprRat)][2] := aConCpr[1][2] //IMPOSTO
 							aConCprRat[Len(aConCprRat)][3] := Round(xMoeda(nValor,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALBASE
							aConCprRat[Len(aConCprRat)][4] := Round(xMoeda(nValRet,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),4)*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALIMP
							aConCprRat[Len(aConCprRat)][5] := aConCpr[1][5]
							aConCprRat[Len(aConCprRat)][6] := aConCpr[1][6]
							aConCprRat[Len(aConCprRat)][7] := aConCpr[1][7]
							aConCprRat[Len(aConCprRat)][8] := aConCpr[1][8]
							aConCprRat[Len(aConCprRat)][9] := aConCpr[1][9]
							aConCprRat[Len(aConCprRat)][10] := 100
						Else
							AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})
						Endif
					EndIf
				SD1->(DbSkip())
			Enddo
		Endif
	Endif
Else

AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})

Return aConCprRat

EndIf

If Empty(aConCprRat)
	AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})
EndIf

Return aConCprRat


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �  ARGCpr2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGCpr      - Calculo de Ret de CPR para NCP               ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGCpr2(cAgente,nSigno,nSaldo)
Local aConCpr		:= {}
Local aConCprRat	:= {}
Local aImpInf 		:= {}
Local lCalcGN 		:= .T.
Local lCalcula		:= .F.
Local cConc			:= ""
Local nRateio		:= 0
Local nRatValImp	:= 0
Local nValMerc		:= 0
Local nTotOrden		:= 0
Local nPosGan 		:= 0
Local nX,nY,nMoeda	:= 1
Local nI			:= 1
Local nVlrTotal		:= 0
Local nAliqSFH		:= 0
Local nValRet		:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.   

DEFAULT nSigno		:=	1

If ExistBlock("F0851IMP")
	lCalcGN:=ExecBlock("F0851IMP",.F.,.F.,{"GN"})
EndIf

DbSelectArea("SFH")
DbSetOrder(1)
If MsSeek(xFilial("SFH")+SE2->E2_FORNECE+SE2->E2_LOJA+"CPR")
  	lCalcula:=.T.
	nAliqSFH:= SFH->FH_ALIQ
EndIf


//+----------------------------------------------------------------+
//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
//� do titulo pelo total da Nota Fiscal.                           �
//+----------------------------------------------------------------+
If lCalcula .and. (SE2->E2_TIPO $ MV_CPNEG).and. A085aVigSFH()
   	DbSelectArea("SF2")
   	DbSetOrder(1)
	If lMsFil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf
	While Alltrim(SF2->F2_ESPECIE)<>Alltrim(SE2->E2_TIPO).And.!Eof()
		SF2->(DbSkip())
		Loop
	Enddo

	If Alltrim(SF2->F2_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
		Iif(lMsFil, SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
		F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA

		nMoeda      := Max(SF2->F2_MOEDA,1)

		nRateio := ( SF2->F2_VALMERC - SF2->F2_DESCONT ) / SF2->F2_VALBRUT


		nRatValImp	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) )
		nValMerc 	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
		If SF2->F2_FRETE > 0
			MV_FRETLOC := GetNewPar("MV_FRETLOC","IVA162GAN06")
			cConc   := Alltrim(Subs( MV_FRETLOC, At("N",MV_FRETLOC)+1))
			nPosGan := ASCAN(aConCpr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+(Round(xMoeda(SF2->F2_FRETE,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp)
			Else
				Aadd(aConCpr,{cConc,Round(xMoeda(SF2->F2_FRETE,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf
		If SF2->F2_DESPESA >  0
			MV_GASTLOC  := GetNewPar("MV_GASTLOC","IVA112GAN06")
			cConc   := Alltrim(Subs( MV_GASTLOC, At("N",MV_GASTLOC)+1))
			nPosGan := ASCAN(aConCpr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+Round(xMoeda(SF2->F2_DESPESA,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp
			Else
				Aadd(aConCpr,{cConc,Round(xMoeda(SF2->F2_DESPESA,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf

		SD2->(DbSetOrder(3))
			If lMsFil
				SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			Else
				SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			EndIf
		If SD2->(Found())
		    Do while Iif(lMsFil, SD2->D2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL.And.SF2->F2_DOC==SD2->D2_DOC.AND.;
				SF2->F2_SERIE==SD2->D2_SERIE.AND.SF2->F2_CLIENTE==SD2->D2_CLIENTE;
				.AND.SF2->F2_LOJA==SD2->D2_LOJA.And.!SD2->(EOF())

				IF AllTrim(SD2->D2_ESPECIE)<>AllTrim(SE2->E2_TIPO)
					SD2->(DbSkip())
					Loop
				Endif
					aImpInf := TesImpInf(SD2->D2_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD2->(DbSkip())
						Loop
					EndIf
					SFF->(DbSetOrder(5))

					 // Imposto +CFO
					If SFF->(MsSeek(xfilial("SFF")+"CPR"+Pad(SD2->D2_CF,len(SFF->FF_CFO)) )) .and. Round(xMoeda(( SF2->F2_VALMERC - SF2->F2_DESCONT ),SF2->F2_MOEDA,1,,,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) > SFF->FF_FXDE

			 			nAliq:= SFF-> FF_ALIQ

			 			If nAliqSFH>0
			 				nAliq:=nAliqSFH
			 			EndIf

							If SF1->(ColumnPos("F2_CFO"))  > 0
				 				nPosGan:=ASCAN(aConCpr,{|x| x[1]==Pad(SF2->F2_CFO,len(SFF->FF_CFO_C)) })
					 		EndIf
							If nPosGan<>0
								nValor:=(Round(xMoeda((SD2->D2_TOTAL-SD2->D2_VALDESC+nVlrTotal),SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp)
								nValRet:=nValor *  nAliq
								aConCpr[nPosGan][2]:=aConCpr[nPosGan][2]+nValor
								aConCpr[nPosGan][3]:=aConCpr[nPosGan][3]+nValRet
							Else
								nValor:=Round(xMoeda((SD2->D2_TOTAL-SD2->D2_DESC+nVlrTotal),SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp
								nValRet:=nValor *(nAliq/100)

								Aadd(aConCpr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CPR",nValor,nValRet,SFF->FF_ALIQ,SF2->F2_SERIE,SF2->F2_DOC,SF2->F2_CLIENTE,SF2->F2_LOJA,,})
						Endif

						If (SFF->FF_IMPORTE <  SD2->D2_TOTAL*(SFF->FF_ALIQ   / 100))
							AAdd(aConCprRat,array(10))
							aConCprRat[Len(aConCpr)][1] := aConCpr[1][1] //CFO
							aConCprRat[Len(aConCpr)][2] := aConCpr[1][2] //IMPOSTO
 							aConCprRat[Len(aConCpr)][3] := Round(xMoeda(nValor,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALBASE
							aConCprRat[Len(aConCpr)][4] := Round(xMoeda(nValRet,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALIMP
							aConCprRat[Len(aConCpr)][5] := aConCpr[1][5]
							aConCprRat[Len(aConCpr)][6] := aConCpr[1][6]
							aConCprRat[Len(aConCpr)][7] := aConCpr[1][7]
							aConCprRat[Len(aConCpr)][8] := aConCpr[1][8]
							aConCprRat[Len(aConCpr)][9] := aConCpr[1][9]
							aConCprRat[Len(aConCpr)][10] := 100
						Else
							AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})
						Endif
					EndIf
				SD2->(DbSkip())
			Enddo
		Endif
	Endif
Else

AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})

Return aConCprRat

EndIf

If Empty(aConCprRat)
	AAdd(aConCprRat, {,0,0,0,0,0,0,0,0,0,0})
EndIf

Return aConCprRat


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetCmr � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetCmr   - Calculo de Ret de CMR para NF 		     	  ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetCmr(cAgente,nSigno,nSaldo)
Local aConCmr		:= {}
Local aConCmrRat	:= {}
Local aImpInf 		:= {}
Local lCalcula		:= .F.
Local lCalcGN 		:= .T.
Local cConc			:= ""
Local nRateio		:= 0
Local nRatValImp	:= 0
Local nValMerc		:= 0
Local nTotOrden		:= 0
Local nPosGan 		:= 0
Local nX,nY,nMoeda	:= 1
Local nI			:= 1
Local nVlrTotal		:= 0
Local nAliqSFH		:= 0
Local nValRet		:= 0
Local nRecSF1 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.

DEFAULT nSigno		:=	1

If ExistBlock("F0851IMP")
	lCalcGN:=ExecBlock("F0851IMP",.F.,.F.,{"GN"})
EndIf

DbSelectArea("SFH")
DbSetOrder(1)
If MsSeek(xFilial("SFH")+SE2->E2_FORNECE+SE2->E2_LOJA+"CMR")
  	lCalcula:=.T.
	nAliqSFH:= SFH->FH_ALIQ
EndIf


//+----------------------------------------------------------------+
//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
//� do titulo pelo total da Nota Fiscal.                           �
//+----------------------------------------------------------------+
If lCalcula .and.  !(SE2->E2_TIPO $ MV_CPNEG) .and. A085aVigSFH()
	dbSelectArea("SF1")
	dbSetOrder(1)
		If lMsFil
			nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
			SF1->(dbGoTo(nRecSF1))
		Else
			nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
			SF1->(dbGoTo(nRecSF1))
		EndIf
	While Alltrim(SF1->F1_ESPECIE)<>Alltrim(SE2->E2_TIPO).And.!Eof()
		SF1->(DbSkip())
		Loop
	Enddo

	If Alltrim(SF1->F1_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
		Iif(lMsFil, SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
		F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA

		nMoeda      := Max(SF1->F1_MOEDA,1)

		If cPaisLoc<>"ARG"
			nRateio := 1
		else
			nRateio := ( SF1->F1_VALMERC - SF1->F1_DESCONT ) / SF1->F1_VALBRUT
		Endif

		nRatValImp	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) )
		nValMerc 	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
		If SF1->F1_FRETE > 0
			MV_FRETLOC := GetNewPar("MV_FRETLOC","IVA162GAN06")
			cConc   := Alltrim(Subs( MV_FRETLOC, At("N",MV_FRETLOC)+1))
			nPosGan := ASCAN(aConCmr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+(Round(xMoeda(SF1->F1_FRETE,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp)
			Else
				Aadd(aConCmr,{cConc,Round(xMoeda(SF1->F1_FRETE,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf
		If SF1->F1_DESPESA >  0
			MV_GASTLOC  := GetNewPar("MV_GASTLOC","IVA112GAN06")
			cConc   := Alltrim(Subs( MV_GASTLOC, At("N",MV_GASTLOC)+1))
			nPosGan := ASCAN(aConCmr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+Round(xMoeda(SF1->F1_DESPESA,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp
			Else
				Aadd(aConCmr,{cConc,Round(xMoeda(SF1->F1_DESPESA,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf

		SD1->(DbSetOrder(1))
			If lMsFil
				SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			Else
				SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
			EndIf
		If SD1->(Found())
		    Do while Iif(lMsFil, SD1->D1_MSFIL,xFilial("SD1",SE2->E2_FILORIG))==SD1->D1_FILIAL.And.SF1->F1_DOC==SD1->D1_DOC.AND.;
				SF1->F1_SERIE==SD1->D1_SERIE.AND.SF1->F1_FORNECE==SD1->D1_FORNECE;
				.AND.SF1->F1_LOJA==SD1->D1_LOJA.And.!SD1->(EOF())

				IF AllTrim(SD1->D1_ESPECIE)<>AllTrim(SE2->E2_TIPO)
					SD1->(DbSkip())
					Loop
				Endif
					aImpInf := TesImpInf(SD1->D1_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD1->(DbSkip())
						Loop
					EndIf
					SFF->(DbSetOrder(5))

					 // Imposto +CFO
					If SFF->(MsSeek(xfilial("SFF")+"CMR"+Pad(SD1->D1_CF,len(SFF->FF_CFO)) )) .and. Round(xMoeda(( SF1->F1_VALMERC - SF1->F1_DESCONT ),SF1->F1_MOEDA,1,,,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) > SFF->FF_FXDE

			 			nAliq:= SFF-> FF_ALIQ

			 			If nAliqSFH>0
			 				nAliq:=nAliqSFH
			 			EndIf
							
							If SF1->( ColumnPos("F1_CFO") ) > 0 .And. SFF->( ColumnPos("FF_CFOC") ) > 0  
			 			 		nPosGan:=ASCAN(aConCmr,{|x| x[1]==Pad(SF1->F1_CFO,len(SFF->FF_CFOC)) })
			 			 	EndIF
							If nPosGan<>0
								nValor:=(Round(xMoeda((SD1->D1_TOTAL-SD1->D1_VALDESC+nVlrTotal),SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp)
								nValRet:=nValor *  nAliq
								Aadd(aConCmr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CMR",nValor,nValRet,SFF->FF_ALIQ,SF1->F1_SERIE,SF1->F1_DOC,SF1->F1_FORNECE,SF1->F1_LOJA,,})
								aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+nValor
								aConCmr[nPosGan][3]:=aConCmr[nPosGan][3]+nValRet
							Else
								nValor:=Round(xMoeda((SD1->D1_TOTAL-SD1->D1_VALDESC+nVlrTotal),SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp
								nValRet:=nValor *(nAliq/100)
								Aadd(aConCmr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CMR",nValor,nValRet,SFF->FF_ALIQ,SF1->F1_SERIE,SF1->F1_DOC,SF1->F1_FORNECE,SF1->F1_LOJA,,})

						EndIf

						// cfo , impostp, base, valor ret
						If (SFF->FF_IMPORTE <  SD1->D1_TOTAL*(SFF->FF_ALIQ   / 100))
					   		AAdd(aConCmrRat,array(10))
							aConCmrRat[Len(aConCmr)][1] := aConCmr[1][1] //CFO
							aConCmrRat[Len(aConCmr)][2] := aConCmr[1][2] //IMPOSTO
 							aConCmrRat[Len(aConCmr)][3] := Round(xMoeda(nValor,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALBASE
							aConCmrRat[Len(aConCmr)][4] := Round(xMoeda(nValRet,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALIMP
							aConCmrRat[Len(aConCmr)][5] := aConCmr[1][5]
							aConCmrRat[Len(aConCmr)][6] := aConCmr[1][6]
							aConCmrRat[Len(aConCmr)][7] := aConCmr[1][7]
							aConCmrRat[Len(aConCmr)][8] := aConCmr[1][8]
							aConCmrRat[Len(aConCmr)][9] := aConCmr[1][9]
							aConCmrRat[Len(aConCmr)][10] := 100
						Else
							AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})
						Endif
					EndIf
				SD1->(DbSkip())
			Enddo
		Endif
	Endif
Else

	AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})

EndIf

If Empty(aConCmrRat)
	AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})
Endif

Return aConCmrRat

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARGRetCmr2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetCmr2   - Calculo de Ret de CMR para NCP			  ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetCmr2(cAgente,nSigno,nSaldo)
Local aConCmr		:= {}
Local aConCmrRat	:= {}
Local aImpInf 		:= {}
Local lCalcula		:= .F.
Local lCalcGN 		:= .T.
Local cConc			:= ""
Local nRateio		:= 0
Local nRatValImp	:= 0
Local nValMerc		:= 0
Local nTotOrden		:= 0
Local nPosGan 		:= 0
Local nX,nY,nMoeda	:= 1
Local nI			:= 1
Local nVlrTotal		:= 0
Local nAliqSFH		:= 0
Local nValRet		:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F. 

DEFAULT nSigno		:=	1

//As retencoes sao calculadas com a taxa do d�a e nao com a taxa variavel....
//Bruno.
	If ExistBlock("F0851IMP")
		lCalcGN:=ExecBlock("F0851IMP",.F.,.F.,{"GN"})
	EndIf

DbSelectArea("SFH")
DbSetOrder(1)
If MsSeek(xFilial("SFH")+SE2->E2_FORNECE+SE2->E2_LOJA+"CMR")
  	lCalcula:=.T.
	nAliqSFH:= SFH->FH_ALIQ
EndIf


//+----------------------------------------------------------------+
//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
//� do titulo pelo total da Nota Fiscal.                           �
//+----------------------------------------------------------------+
If lCalcula .and. (SE2->E2_TIPO $ MV_CPNEG).and. A085aVigSFH()
   		DbSelectArea("SF2")
   		DbSetOrder(1)
		If lMsFil
			nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
			SF2->(dbGoTo(nRecSF2))
		Else
			nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
			SF2->(dbGoTo(nRecSF2))
		EndIf
	While Alltrim(SF2->F2_ESPECIE)<>Alltrim(SE2->E2_TIPO).And.!Eof()
		SF2->(DbSkip())
		Loop
	Enddo

	If Alltrim(SF2->F2_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
		Iif(lMsFil, SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
		F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA

		nMoeda      := Max(SF2->F2_MOEDA,1)

		nRateio := ( SF2->F2_VALMERC - SF2->F2_DESCONT ) / SF2->F2_VALBRUT


		nRatValImp	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) )
		nValMerc 	:= ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) * nRateio )
		If SF2->F2_FRETE > 0
			MV_FRETLOC := GetNewPar("MV_FRETLOC","IVA162GAN06")
			cConc   := Alltrim(Subs( MV_FRETLOC, At("N",MV_FRETLOC)+1))
			nPosGan := ASCAN(aConCmr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+(Round(xMoeda(SF2->F2_FRETE,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp)
			Else
				Aadd(aConCmr,{cConc,Round(xMoeda(SF2->F2_FRETE,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf
		If SF2->F2_DESPESA >  0
			MV_GASTLOC  := GetNewPar("MV_GASTLOC","IVA112GAN06")
			cConc   := Alltrim(Subs( MV_GASTLOC, At("N",MV_GASTLOC)+1))
			nPosGan := ASCAN(aConCmr,{|x| x[1]==cConc.And. x[3]==''})
			If nPosGan<>0
				aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+Round(xMoeda(SF2->F2_DESPESA,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp
			Else
				Aadd(aConCmr,{cConc,Round(xMoeda(SF2->F2_DESPESA,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))*nRatValimp,''})
			Endif
		EndIf

		SD2->(DbSetOrder(3))
			If lMsFil
				SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			Else
				SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
			EndIf
		If SD2->(Found())
		    Do while Iif(lMsFil, SD2->D2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL.And.SF2->F2_DOC==SD2->D2_DOC.AND.;
				SF2->F2_SERIE==SD2->D2_SERIE.AND.SF2->F2_CLIENTE==SD2->D2_CLIENTE;
				.AND.SF2->F2_LOJA==SD2->D2_LOJA.And.!SD2->(EOF())

				IF AllTrim(SD2->D2_ESPECIE)<>AllTrim(SE2->E2_TIPO)
					SD2->(DbSkip())
					Loop
				Endif
					aImpInf := TesImpInf(SD2->D2_TES)
					lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
					If !lTESNoExen
						SD2->(DbSkip())
						Loop
					EndIf
					SFF->(DbSetOrder(6))

					 // Imposto +CFO
					If SFF->(MsSeek(xfilial("SFF")+"CMR"+Pad(SD2->D2_CF,len(SFF->FF_CFO)) )) .and. Round(xMoeda(( SF2->F2_VALMERC - SF2->F2_DESCONT ),SF2->F2_MOEDA,1,,,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) > SFF->FF_FXDE

			 			nAliq:= SFF-> FF_ALIQ

			 			If nAliqSFH>0
			 				nAliq:=nAliqSFH
			 			EndIf

						If SF2->( ColumnPos("F2_CFO") ) > 0 .And. SFF->( ColumnPos("FF_CFOC") ) > 0 
			 				nPosGan:=ASCAN(aConCmr,{|x| x[1]==Pad(SF2->F2_CFO,len(SFF->FF_CFOC)) })
			 			EndIf
						If nPosGan<>0
							nValor:=(Round(xMoeda((SD2->D2_TOTAL-SD2->D2_VALDESC+nVlrTotal),SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp)
							nValRet:=nValor *  nAliq
							aConCmr[nPosGan][2]:=aConCmr[nPosGan][2]+nValor
							aConCmr[nPosGan][3]:=aConCmr[nPosGan][3]+nValRet
						Else
							nValor:=Round(xMoeda((SD2->D2_TOTAL-SD2->D2_DESC+nVlrTotal),SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) * nRatValImp
							nValRet:=nValor *(nAliq/100)

							Aadd(aConCmr,{Pad(SD1->D1_CF,len(SFF->FF_CFO)),"CMR",nValor,nValRet,SFF->FF_ALIQ,SF2->F2_SERIE,SF2->F2_DOC,SF2->F2_CLIENTE,SF2->F2_LOJA,,})

						Endif

						// cfo , impostp, base, valor ret
						If (SFF->FF_IMPORTE <  SD2->D2_TOTAL*(SFF->FF_ALIQ   / 100))
							AAdd(aConCmrRat,array(10))
							aConCmrRat[Len(aConCmr)][1] := aConCmr[1][1] //CFO
							aConCmrRat[Len(aConCmr)][2] := aConCmr[1][2] //IMPOSTO
 							aConCmrRat[Len(aConCmr)][3] := Round(xMoeda(nValor,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALBASE
							aConCmrRat[Len(aConCmr)][4] := Round(xMoeda(nValRet,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))*nSaldo/SE2->E2_VALOR * nSigno	//FE_VALIMP
							aConCmrRat[Len(aConCmr)][5] := aConCmr[1][5]
							aConCmrRat[Len(aConCmr)][6] := aConCmr[1][6]
							aConCmrRat[Len(aConCmr)][7] := aConCmr[1][7]
							aConCmrRat[Len(aConCmr)][8] := aConCmr[1][8]
							aConCmrRat[Len(aConCmr)][9] := aConCmr[1][9]
							aConCmrRat[Len(aConCmr)][10] := 100
						Else
							AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})
						Endif
					EndIf
				SD2->(DbSkip())
			Enddo
		Endif
	Endif
Else

	AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})

//Return aConCmrRat

EndIf

If Empty(aConCmrRat)
	AAdd(aConCmrRat, {,0,0,0,0,0,0,0,0,0,0})
Endif

Return aConCmrRat

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGSegF1 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGSegF1    - Calculo de Ret de Seguridad e Hig para NF    ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGSegF1(cAgente,nSigno,nSaldo,cChavePOP,cNFPOP,cSeriePOP,aSLIMIN)
Local aSFEISI  	:= {}
Local aSFF 		:= {}
Local aSFH 		:= {}
Local aCF	   	:= {}
Local aImps 	:= {}
Local lFin85Zar	:= .T.
Local lCalcLimp	:= .F.
Local cChave   	:= ""
Local nRateio  	:= 0
Local nValRet  	:= 0
Local nValTot  	:= 0
Local nVlrBase 	:= 0
Local nVlrTotal	:= 0
Local nAliq    	:= 0
Local nVlrFF   	:= 0
Local nRecFF   	:= 0
Local nPercTot 	:= 1
Local nValBasTot:= 0
Local nK 		:= 0
Local nL 		:= 0
Local nM		:= 0
Local nN 		:= 0
Local nI		:= 0
Local nJ		:= 0
Local nRecSF1 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.
Local nValCoef := SuperGetMv("MV_CRFRET",.F.,0)
Local lCalcCEI := .F.
Local nValMCO := 0
Local nPosMin	:= 0
Private lNoIsento := .T.
Private lSFH := .F.
Private lCero := .F.



DEFAULT nSigno	:=	1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT aSLIMIN := {}

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte .and. FVC->(ColumnPos("FVC_RET_MN")) > 0
	Return aSFEISI := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"M",nSaldo)
EndIf


//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Retenܧܤo.�
//+---------------------------------------------------------------------+
SA2->( dbSetOrder(1) )
If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
	SA2->(MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA))
Else
	SA2->(MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA))
Endif

dbSelectArea("SFF")
dbSetOrder(1)
If SA2->(Found()) .And. SA2->(ColumnPos("A2_RET_MUN")) >0 .And. SFF->(ColumnPos("FF_RET_MUN")) > 0 .And. SFE->(ColumnPos("FE_RET_MUN")) > 0 
	If !Empty(SA2->A2_RET_MUN)
		lCalcLimp	:=.T. 
	EndIf
EndIf

If  lCalcLimp
	If ExistBlock("F0851IMP")
		lCalcLimp:=ExecBlock("F0851IMP",.F.,.F.,{"M"})
	EndIf  
Endif
If lCalcLimp


	dbSelectArea("SF1")
	dbSetOrder(1)
	If lMsFil
		nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	Else
		nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
		SF1->(dbGoTo(nRecSF1))
	EndIf
	While !Eof() .And. (Alltrim(SF1->F1_ESPECIE) <> AllTrim(SE2->E2_TIPO))
		dbSkip()
		Loop
	End

	If (AllTrim(SF1->F1_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
		(Iif(lMsFil,SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
		F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA)

		nRateio := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / ROund(xMoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) )

		cChave := SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA
		SD1->(DbSetOrder(1))
		If lMsFil
			SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
		Else
			SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
		EndIf
		While !SD1->(Eof()) .And. Iif(lMsFil,SF1->F1_MSFIL,xFilial("SD1",SE2->E2_FILORIG))==SD1->D1_FILIAL .And. SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA == cChave
			If AllTrim(SD1->D1_ESPECIE) <> Alltrim(SF1->F1_ESPECIE)
				SD1->(DbSkip())
				Loop
			Endif
			
			//Verifica as caracteristicas do TES para que os impostos
			//possam ser somados ao valor da base de calculo da retencao..
			aImpInf := TesImpInf(SD1->D1_TES)
			lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
			If !lTESNoExen
				SD1->(DbSkip())
				Loop
			EndIf

			If SA2->(A2_RET_MUN) == "00004"// municipio retenci�n c�rdoba
					nValMCO += Round(xmoeda(SD1->D1_TOTAL,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
			Else
				If (nJ := aScan(aCF, {|x| x[1]==SD1->D1_CF}))==0
					aAdd( aCF , {SD1->D1_CF,0,{}} ) // CF , Total , Aliq
					nJ := Len(aCF)
				EndIf
			aCF[nJ,2]	+=  Round(xmoeda(SD1->D1_TOTAL,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1)) 
			Endif
		   //	aCF[nJ,2]	+= SD1->D1_TOTAL
		   If SA2->(A2_RET_MUN) == "00004"
		   		aSFF := ObtRegSF("CEI", "SFF") //Obtiene tabla generica SFF por codigo de municipalidad 
		   		If Len(aSFF) > 0 .AND. !lCalcCEI
		   			lCalcCEI := .T.
		   		Endif
		   	Else
		   		aSFF := ObtRegSF("", "SFF") //Obtiene tabla generica SFF por codigo de municipalidad
		   Endif
			
			SD1->(dbSkip())
		End
		If lCalcCEI
			aAdd( aCF , {"",nValMCO,{}} )
		Endif	
		// Loacaliza CF para totalizar
		For nJ:=1 To Len(aCF)	
			For nK := 1 To Len(aSFF)
				If (aSFF[nK][1] == aCF[nJ,1] .And. SA2->(A2_RET_MUN) == aSFF[nK][6]) .OR. (lCalcCEI .And. SA2->(A2_RET_MUN) == aSFF[nK][6])
					If nVlrFF < aSFF[nK][3]
						nVlrFF := aSFF[nK][3]
						If SA2->(A2_RET_MUN) == "00004"
							nPosMin := ascan(aSLIMIN,{|x|, x[2] == SA2->(A2_RET_MUN)})
							If nPosMin == 0
								AADD(aSLIMIN,{aSFF[nK][5],aSFF[nK][6],aSFF[nK][3],0})
							EndIf
						Endif              
						nRecFF := SFF->(Recno())
					EndIf
				EndIf
			Next
			// Verrica se encotrou SFF para o valor total e se o valor ܩ > 0
			If Iif(SA2->(A2_RET_MUN) == "00004",.T.,aCF[nJ,2] > nVlrFF) //nRecFF > 0 .and. aCF[nJ,2] > 0		
				For nK := 1 To Len(aSFF)
					If (aSFF[nK][1] == aCF[nJ,1] .And. SA2->(A2_RET_MUN) == aSFF[nK][6]) .OR. (lCalcCEI .And. SA2->(A2_RET_MUN) == aSFF[nK][6])
						aSFH := ObtRegSF(aSFF[nK][5], "SFH") //Obtiene tabla generica SFF por codigo de municipalidad
						If Len(aSFH) > 0 	
							For nL := 1 To Len(aSFH)
								If aSFH[nL][5] == "3"
									If aSFH[nL][3] == "CEI" .AND. aSFH[nL][6] == "CO"
										If aSFH[nL][1] == SA2->A2_COD .And. aSFH[nL][2] == SA2->A2_LOJA .And. aSFH[nL][3] == aSFF[nK][5] .and. lNoIsento .AND. lSFH .AND. !lCero
											aAdd(aCF[nJ,3], {aSFH[nL][4], aSFH[nL][3]} ) //aCF[nJ,3] := aSFH[nL][4]
										ElseIf (lNoIsento .AND. !lSFH) .OR. (!lNoIsento .AND. !lSFH) .OR. (lCero .AND. lSFH .AND. lNoIsento) .OR. (lNoIsento .AND. lCero)
											aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //				
										Endif
									Endif
								ElseIf aSFH[nL][5] <>  "3"	
									If aSFH[nL][1] == SA2->A2_COD .And. aSFH[nL][2] == SA2->A2_LOJA .And. aSFH[nL][3] == aSFF[nK][5] .and. lNoIsento .AND. lSFH .AND. !lCero
											aAdd(aCF[nJ,3], {aSFH[nL][4], aSFH[nL][3]} ) //aCF[nJ,3] := aSFH[nL][4]
										ElseIf (lNoIsento .AND. !lSFH) .OR. (!lNoIsento .AND. !lSFH) .OR. (lCero .AND. lSFH .AND. lNoIsento) .OR. (lNoIsento .AND. lCero)
											aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //				
									Endif
								Endif	
							Next
							Else
									aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //			
							EndIf
				    EndIf
				Next
			Else
				aCF[nJ,2]	:= 0 // Zera base de calculo quando nao tem CFOP no SFF
			EndIf
			nRecFF := 0
			nVlrFF := 0
			
		Next

		If lCalcLimp

			// Totaliza Base de calculo por CFOP
				For nJ:=1 To Len(aCF)
					For nM := 1 To Len(aCF[nJ,3])
						nVlrBase := (aCF[nJ,2] * nRateio)
						nAliq	 :=  aCF[nJ,3,nM,1] // Aliquota
						nValRet  := Round((nVlrBase*(nAliq/100))*nSigno,TamSX3("FE_VALIMP")[2])
		
						nValRet	 := (nValRet * nPercTot) // Reduܧܣo de imposto
						If (Len(aSFF) > 0 .And.aSFF[1][5] == "CEI") .And. (Len(aSFH)>0 .And. aSFH[1][5] == "2") .And. nValCoef <> 0
							nValRet := nValRet * nValCoef
						Endif
						nValTot	 += nValRet
						If (nN := aScan(aImps, {|x| x[2]== aCF[nJ,3,nM,2]}))==0
							aAdd( aImps , {aCF[nJ,3,nM,1], aCF[nJ,3,nM,2], nValRet, aCF[nJ,1]} ) // CF , Total , Aliq
						Else
							aImps[nN][3] += nValRet
						EndIf
					Next
					nValBasTot += nVlrBase
					nVlrBase := 0
				Next
			//����������������������������������������������������������������Ŀ
			//� Gravar Retenciones.                                            �
			//������������������������������������������������������������������
			AAdd(aSFEISI,array(10))
			aSFEISI[Len(aSFEISI)][1] := SF1->F1_DOC   				 //FE_NFISCAL
			aSFEISI[Len(aSFEISI)][2] := SF1->F1_SERIE 				 //FE_SERIE
			aSFEISI[Len(aSFEISI)][3] := nValBasTot	      				 //FE_VALBASE
			aSFEISI[Len(aSFEISI)][4] := nValTot	       			 //FE_VALIMP
			aSFEISI[Len(aSFEISI)][5] := Round((nValTot*100)/nSaldo,2)//FE_PORCRET
			aSFEISI[Len(aSFEISI)][6] := nValTot 						 // FE_RETENC
			aSFEISI[Len(aSFEISI)][7] := nPercTot 						 //FE_DEDUC
			aSFEISI[Len(aSFEISI)][8] := aImps 							 // {Aliq, Impuesto, Vlr Ret, Cod Fiscal}
			aSFEISI[Len(aSFEISI)][9] := SA2->A2_EST 					 // Cod Provincia
			aSFEISI[Len(aSFEISI)][10]:= SA2->A2_RET_MUN 				 // Cod Provincia

		EndIf
	EndIf
EndIf

Return aSFEISI

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGSegF2 � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGSegF2    - Calculo de Ret de Seguridad e Hig para NCP   ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGSegF2(cAgente,nSigno,nSaldo,cChavePOP,cNFPOP,cSeriePOP,aSLIMIN)
Local aSFEISI  	:= {}
Local aCF	   	:= {}
Local aImps 	:= {}
Local aSFF 		:= {}
Local aSFH 		:= {}
Local lCalcLimp := .F.
Local cChave   	:= ""
Local nRateio  	:= 0
Local nValRet  	:= 0
Local nValTot  	:= 0
Local nVlrBase 	:= 0
Local nVlrTotal	:= 0
Local nAliq    	:= 0
Local nVlrFF   	:= 0
Local nRecFF   	:= 0
Local nPercTot 	:= 1
Local nValBasTot:= 0
Local nI		:= 0
Local nJ		:= 0
Local nK 		:= 0
Local nL 		:= 0
Local nM 		:= 0
Local nN 		:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.
Local nValCoef := SuperGetMv("MV_CRFRET",.F.,0) 
Local lCalcCEI := .F.
Local nValMCO := 0
Local nPosMin	:= 0
Private lNoIsento := .T.
Private lSFH := .F.
Private lCero := .F.

DEFAULT nSigno:= -1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT aSLIMIN := {}

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte .and. FVC->(ColumnPos("FVC_RET_MN")) > 0
	Return aSFEISI := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"M",nSaldo)
EndIf


dbSelectArea("SFF")
dbSetOrder(1)
If SA2->(Found()) .And. SA2->(ColumnPos("A2_RET_MUN")) >0 .And. SFF->(ColumnPos("FF_RET_MUN")) > 0 .And. SFE->(ColumnPos("FE_RET_MUN")) > 0//.and.;
	If !Empty(SA2->A2_RET_MUN)
		lCalcLimp	:=.T.
	EndIf
EndIf


If  lCalcLimp
	If ExistBlock("F0851IMP")
		lCalcLimp:=ExecBlock("F0851IMP",.F.,.F.,{"M"})
	EndIf      
Endif

//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Retenܧܤo.�
//+---------------------------------------------------------------------+
If lCalcLimp

	dbSelectArea("SF2")
	dbSetOrder(1)
	If lMsFil
		nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	Else
		nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
		SF2->(dbGoTo(nRecSF2))
	EndIf
	While !Eof() .And. (Alltrim(SF2->F2_ESPECIE) <> AllTrim(SE2->E2_TIPO))
		DbSkip()
		Loop
	Enddo

	If (AllTrim(SF2->F2_ESPECIE) == Alltrim(SE2->E2_TIPO)) .And. ;
		(Iif(lMsFil,SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA == ;
		F2_FILIAL+F2_DOC+PadR(SF2->F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA)

		nRateio := ( Round(xMoeda(nSaldo,SE2->E2_MOEDA,1,,5,aTxMoedas[Max(SE2->E2_MOEDA,1)][2]),MsDecimais(1)) / Round(xMoeda(SF2->F2_VALBRUT,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) )

		cChave := SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA
		SD2->(DbSetOrder(3))
		If lMsFil
			SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		Else
			SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
		EndIf
		While !SD2->(Eof()) .And. Iif(lMsFil,SF2->F2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL .And. SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA == cChave
			If AllTrim(SD2->D2_ESPECIE) <> Alltrim(SF2->F2_ESPECIE)
				SD2->(DbSkip())
				Loop
			Endif
			
			//Verifica as caracteristicas do TES para que os impostos
			//possam ser somados ao valor da base de calculo da retencao...
			aImpInf := TesImpInf(SD2->D2_TES)
			lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
			If !lTESNoExen
				SD2->(DbSkip())
				Loop
			EndIf

			If SA2->(A2_RET_MUN) == "00004"
				nValMCO += Round(xmoeda(SD2->D2_TOTAL,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
			Else
				If (nJ := aScan(aCF, {|x| x[1]==SD2->D2_CF}))==0
					aAdd( aCF , {SD2->D2_CF,0,{}} ) // CF , Total , Aliq
					nJ := Len(aCF)
				EndIf
			aCF[nJ,2]	+=  Round(xmoeda(SD2->D2_TOTAL,SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1)) 
			Endif
			// aCF[nJ,2]	+= SD2->D2_TOTAL
			If SA2->(A2_RET_MUN) == "00004" //c�digo retencion munipio c�rdoba
				aSFF := ObtRegSF("CEI", "SFF") //Obtiene tabla generica SFF por codigo de municipalidad 
				If Len(aSFF) > 0 .AND. !lCalcCEI
					lCalcCEI := .T.
				Endif
		   Else
				aSFF := ObtRegSF("", "SFF") //Obtiene tabla generica SFF por codigo de municipalidad
		   Endif
		   
			SD2->(dbSkip())
		End
		If lCalcCEI
			aAdd( aCF , {"",nValMCO,{}} )
		Endif	
		// Loacaliza CF para totalizar
		For nJ:=1 To Len(aCF)
			For nK := 1 To Len(aSFF)
				If (aSFF[nK][2] == aCF[nJ,1] .And. aSFF[nK][6] == Alltrim(SA2->A2_RET_MUN)) .OR. (lCalcCEI .And. SA2->(A2_RET_MUN) == aSFF[nK][6])
					If nVlrFF < aSFF[nK][3]
						nVlrFF := aSFF[nK][3]
						If SA2->(A2_RET_MUN) == "00004"
							nPosMin := ascan(aSLIMIN,{|x|, x[2] == SA2->(A2_RET_MUN)})
							If nPosMin == 0
								AADD(aSLIMIN,{aSFF[nK][5],aSFF[nK][6],aSFF[nK][3],0})
							EndIf
						Endif             
					EndIf
				EndIf
			Next
			// Verrica se encotrou SFF para o valor total e se o valor ܩ > 0
			If Iif(SA2->(A2_RET_MUN) == "00004",.T.,aCF[nJ,2] > nVlrFF) //nRecFF > 0 .and. aCF[nJ,2] > 0		
				For nK := 1 To Len(aSFF)
					If (aSFF[nK][2] == aCF[nJ,1] .And. SA2->(A2_RET_MUN) == aSFF[nK][6]) .OR. (lCalcCEI .And. SA2->(A2_RET_MUN) == aSFF[nK][6])
						aSFH := ObtRegSF(aSFF[nK][5], "SFH") //Obtiene tabla generica SFF por codigo de municipalidad
						If Len(aSFH) > 0 	
							For nL := 1 To Len(aSFH)
								If aSFH[nL][5] == "3"
									If aSFH[nL][3] == "CEI" .AND. aSFH[nL][6] == "CO"
										If aSFH[nL][1] == SA2->A2_COD .And. aSFH[nL][2] == SA2->A2_LOJA .And. aSFH[nL][3] == aSFF[nK][5] .and. lNoIsento .AND. lSFH .AND. !lCero
											aAdd(aCF[nJ,3], {aSFH[nL][4], aSFH[nL][3]} ) //aCF[nJ,3] := aSFH[nL][4]
										ElseIf (lNoIsento .AND. !lSFH) .OR. (!lNoIsento .AND. !lSFH) .OR. (lCero .AND. lSFH .AND. lNoIsento) .OR. (lNoIsento .AND. lCero)
											aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //				
										Endif
									Endif
								ElseIf aSFH[nL][5] <>  "3"	
									If aSFH[nL][1] == SA2->A2_COD .And. aSFH[nL][2] == SA2->A2_LOJA .And. aSFH[nL][3] == aSFF[nK][5] .and. lNoIsento .AND. lSFH .AND. !lCero
											aAdd(aCF[nJ,3], {aSFH[nL][4], aSFH[nL][3]} ) //aCF[nJ,3] := aSFH[nL][4]
										ElseIf (lNoIsento .AND. !lSFH) .OR. (!lNoIsento .AND. !lSFH) .OR. (lCero .AND. lSFH .AND. lNoIsento) .OR. (lNoIsento .AND. lCero)
											aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //				
									Endif
								Endif	
							Next
							Else
								aAdd(aCF[nJ,3], {aSFF[nK][8], aSFF[nK][5]} ) //			
							EndIf
				    EndIf
				Next
			Else
				aCF[nJ,2]	:= 0 // Zera base de calculo quando nao tem CFOP no SFF
			EndIf
			nRecFF := 0
			nVlrFF := 0
		Next

		If lCalcLimp

			// Totaliza Base de calculo por CFOP
				For nJ:=1 To Len(aCF)
					For nM := 1 To Len(aCF[nJ,3])
						nVlrBase := (aCF[nJ,2] * nRateio) 
						nAliq	 :=  aCF[nJ,3,nM,1] // Aliquota
						nValRet  := Round((nVlrBase*(nAliq/100))*nSigno,TamSX3("FE_VALIMP")[2])
		
						nValRet	 := (nValRet * nPercTot) // Reduܧܣo de imposto
						If (Len(aSFF) > 0 .And.aSFF[1][5] == "CEI") .And. (Len(aSFH)>0 .And. aSFH[1][5] == "2") .And. nValCoef <> 0
							nValRet := nValRet * nValCoef
						Endif
						nValTot	 += nValRet
						If (nN := aScan(aImps, {|x| x[2]== aCF[nJ,3,nM,2]}))==0
							aAdd( aImps , {aCF[nJ,3,nM,1], aCF[nJ,3,nM,2], nValRet, aCF[nJ,1]} ) // CF , Total , Aliq
						Else
							aImps[nN][3] += nValRet
						EndIf
					Next
					nValBasTot += nVlrBase*nSigno
					nVlrBase := 0
				Next
			//����������������������������������������������������������������Ŀ
			//� Gravar Retenciones.                                            �
			//������������������������������������������������������������������
			AAdd(aSFEISI,array(10))
			aSFEISI[Len(aSFEISI)][1] := SF2->F2_DOC  					 //FE_NFISCAL
			aSFEISI[Len(aSFEISI)][2] := SF2->F2_SERIE 				 //FE_SERIE
			aSFEISI[Len(aSFEISI)][3] := nValBasTot	      				 //FE_VALBASE
			aSFEISI[Len(aSFEISI)][4] := nValTot		       		 //FE_VALIMP
			aSFEISI[Len(aSFEISI)][5] := Abs(Round((nValTot*100)/nSaldo,2))//FE_PORCRET
			aSFEISI[Len(aSFEISI)][6] := nValTot 						 // FE_RETENC
			aSFEISI[Len(aSFEISI)][7] := nPercTot 						 //FE_DEDUC
			aSFEISI[Len(aSFEISI)][8] := aImps							 // {Aliq, Impuesto, Vlr Ret, Cod Fiscal}
			aSFEISI[Len(aSFEISI)][9] := SA2->A2_EST					 // Cod Provincia
			aSFEISI[Len(aSFEISI)][10] := SA2->A2_RET_MUN 				 // Cod Provincia

		EndIf
	EndIf
EndIf

Return aSFEISI

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGRetIM � Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     � ARGRetIM    - Calculo de Ret de Iva Monotributista para NCP���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetIM(cAgente,nSigno,nSaldo,lPa,cCF,nValor,nProp,lNNF,cSerieNF,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local aConIva  	:= {}
Local aSFEIVA  	:= {}
Local aCF 		:= {}
Local lCalcMon 	:= .F.
Local lCalRet	:= .F.
Local lCalcImp	:= .T.
Local lCalRep	:= .F.
Local lRetSit	:= .T.
Local lPropIVA	:= Iif(GetMV("MV_PROPIVA",.F.,2)==2,.T.,.F.)
Local cChaveSFE	:= ""
Local cCodFis 	:= ""
Local cItem 	:= ""
Local nPosIva	:= 0
Local nCount	:= 0
Local nPercRet	:= 0
Local nI		:= 1
Local nRetIva	:= 0
Local nRetTotal	:= 0
Local nTotRet 	:= 0
Local nMinimo 	:= 0
Local nMinUnit 	:= 0
Local nParcela 	:= 0
Local nRecSF1 := 0 
Local nTamSer := SerieNfId('SF1',6,'F1_SERIE')
Local lTESNoExen := .F.

DEFAULT lNNF 	:= .F.
DEFAULT lPa		:= .F.
DEFAULT cCF 	:= ""
DEFAULT nValor 	:= 0
DEFAULT nSigno	:= 1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIVA := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"I",nSaldo,dEmissao)
EndIf

//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Reten��o.�
//+---------------------------------------------------------------------+
aArea:=GetArea()

dbSelectArea("SF1")
dbSetOrder(1)
If lMsFil
	nRecSF1 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
	SF1->(dbGoTo(nRecSF1))
Else
	nRecSF1 := FINBuscaNF(xFilial("SF1",SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF1",.T.)
	SF1->(dbGoTo(nRecSF1))
EndIf

If lNNF
	lCalRet := .T. //For�a calculo de IVA para monotributista sem nota (PA pela orden de pago)
Else
	lCalRet := Iif(Alltrim(PadR(SF1->F1_SERIE,nTamSer)) == "M",.T.,.F.)
EndIf

If ExistBlock("F0851IMP")
	lCalcImp:=ExecBlock("F0851IMP",.F.,.F.,{"IVA"})
EndIf

If lNNF
	SA2->(dbSetOrder(1))
	SA2->(MsSeek(xFilial("SA2")+cFornece+cLoja))
Else
	SA2->( dbSetOrder(1) )
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
		SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Else
		SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Endif
EndIf

// Tratamento do Reproweb - Situa��o igual a 0 n�o deve reter Resoluci�n General ( AFIP) 2226/07
If SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("0")
	lRetSit:=.F.
EndIf

If lCalcImp .And. lRetSit

	If !lNNF
		SA2->( dbSetOrder(1) )
		If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
			SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Else
			SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
		Endif
	EndIf

	lCalRep := Iif(SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("2/4"),.T.,.F.)

	If SA2->A2_AGENRET == "N" .Or. lCalRet

		If !lPA
			While Alltrim(SF1->F1_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
				SF1->(DbSkip())
				Loop
			Enddo

			//Caso o valor da fatura seja parcelado, atribui o valor da parcela.
			If !Empty(SE2->E2_PARCELA) .And. lPropIVA
				nParcela := SE2->E2_VALOR
			Endif

			If AllTrim(SF1->F1_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
				Iif(lMsFil,SF1->F1_MSFIL,xFilial("SF1",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
				F1_FILIAL+F1_DOC+PadR(F1_SERIE,nTamSer)+F1_FORNECE+F1_LOJA

					//����������������������������������������������������������������Ŀ
					//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
					//� do titulo pelo total da Nota Fiscal.                           �
					//������������������������������������������������������������������
				SD1->(DbSetOrder(1))
				If lMsFil
					SD1->(MsSeek(SF1->F1_MSFIL+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
				Else
					SD1->(MsSeek(xFilial("SD1",SE2->E2_FILORIG)+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA))
				EndIf
				If SD1->(Found())
					Do while Iif(lMsFil,SD1->D1_MSFIL,xFilial("SD1",SE2->E2_FILORIG))==SD1->D1_FILIAL.And.SF1->F1_DOC==SD1->D1_DOC.AND.;
						SF1->F1_SERIE==SD1->D1_SERIE.AND.SF1->F1_FORNECE==SD1->D1_FORNECE;
						.AND.SF1->F1_LOJA==SD1->D1_LOJA.AND.!SD1->(EOF())

						If AllTrim(SD1->D1_ESPECIE)<>AllTrim(SF1->F1_ESPECIE)
							SD1->(DbSkip())
							Loop
						Endif
						aImpInf := TesImpInf(SD1->D1_TES)
						lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
						If !lTESNoExen
							SD1->(DbSkip())
							Loop
						EndIf
						SB1->(DbSetOrder(1))
						If !Empty(SD1->D1_CF)
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SD1->D1_CF})
								If nPosIva<>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+((aConIva[nPosIva][4] * (SD1->D1_TOTAL ) )- ((SD1->D1_IVAFRET+SD1->D1_IVAGAST)))
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+((SD1->D1_TOTAL ) - ((SD1->D1_BaIVAFR+SD1->D1_BaIVAGA)) )
								Else

									If lCalRep //.or. lCalcVen
										aImpInf := TesImpInf(SD1->D1_TES)

										For nI := 1 To Len(aImpInf)
											If  "IV"$Trim(aImpInf[nI][01]) .And.  Trim(aImpInf[nI][01])<>"IVP"
												Aadd(aConIva,{SD1->D1_CF,SD1->(FieldGet(ColumnPos(aImpInf[nI][02]))),SD1->(FieldGet(ColumnPos(aImpInf[nI][07]))), (SD1->(FieldGet(ColumnPos(aImpInf[nI][10]))) /100)})
											 EndIf
										Next

									Else
										SFF->(DbSetOrder(5))
										SFF->(DbGotop())
										If SFF->(MsSeek(xFilial("SFF")+"IVR"))
											nPerc:=0
											lAchou:=.F.
											While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
												If (Alltrim(SFF->FF_SERIENF)== Alltrim(PadR(SD1->D1_SERIE,nTamSer)) .And. SD1->D1_CF==  SFF->FF_CFO_C )
													lAchou:=.T.
													nPerc:=SFF->FF_ALIQ
												Else
													SFF->(DbSkip())
												EndIf
											EndDo

											If lAchou
													nValorBr:=Round(xmoeda(SF1->F1_VALBRUT,SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
													nPerc:=SFF->FF_ALIQ
											EndIf

											Aadd(aConIva,{SD1->D1_CF,(((nPerc/100 ) * Iif(nParcela == 0,(SD1->D1_TOTAL),nParcela)) - (SD1->D1_IVAFRET+SD1->D1_IVAGAST)),(Iif(nParcela == 0,SD1->D1_TOTAL,nParcela)- (SD1->D1_BaIVAFR +SD1->D1_BaIVAGA)), (nPerc /100),SFF->FF_CFO})
										EndIf
									EndIf
								EndIF

						ElseIf SB1->(MsSeek(Iif(lMsFil .And. !Empty(xFilial("SB1")),SD1->D1_MSFIL,xFilial("SB1")) + SD1->D1_COD))
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SB1->B1_CONCIVA})
							If nPosIva<>0
								aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD1->D1_VALIMP1-SD1->D1_IVAFRET+SD1->D1_IVAGAST)
								aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD1->D1_BASIMP1-SD1->D1_BaIVAFR+SD1->D1_BaIVAGA)
							Else
								Aadd(aConIva,{SB1->B1_CONCIVA,(SD1->D1_VALIMP1-SD1->D1_IVAFRET+SD1->D1_IVAGAST),(SD1->D1_BASIMP1-SD1->D1_BaIVAFR +SD1->D1_BaIVAGA),0})
							Endif
						Else
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SA2->A2_ACTRET})
							If nPosIva<>0
								aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD1->D1_VALIMP1)
								aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD1->D1_BASIMP1)
							Else
								Aadd(aConIva,{SA2->A2_ACTRET,SD1->D1_VALIMP1,SD1->D1_BASIMP1,0})
							Endif
						Endif

						SD1->(DbSkip())
					EndDo
				Endif

				//����������������������������������������������������������������Ŀ
				//� Gravar Retenciones.                                            �
				//������������������������������������������������������������������
				For nCount:=1  to Len(aConIva)

	 				//Verifica se fara o calculo de IVA para fornecedores monotributistas
					dbSelectArea("SFF")
					dbGoTop()
					dbSetOrder(5)

					If MsSeek(xFilial("SFF")+"IVR"+aConIva[nCount][1])
						While SFF->(!Eof()) .And. SFF->FF_CFO_C == aConIva[nCount][1]

							If SFF->FF_ITEM $	"I1|I2" .And. SFF->FF_TIPO == 'M';
							   .And. AllTrim(SFF->FF_SERIENF) == AllTrim(PadR(SF1->F1_SERIE,nTamSer))

								cItem		:= SFF->FF_ITEM
								nMinimo  	:= Iif(SFF->(ColumnPos("FF_LIMITE"))>0,SFF->FF_LIMITE,0)
								nMinUnit 	:= Iif(SFF->(ColumnPos("FF_MINUNIT"))>0,SFF->FF_MINUNIT,0)

								nRecSFF := Recno()

								dbGoTop()
								MsSeek(xFilial("SFF")+"IVR")

								//Array contendo todos os CFOs com a mesma classifica��o
								While SFF->(!Eof())
									If SFF->FF_ITEM == cItem
										If aScan(aCf,{|x| x[1] == SFF->FF_CFO_C}) == 0
											aAdd(aCf,{SFF->FF_CFO_C,SFF->FF_CFO_V})
										Endif
									Endif
									SFF->(dbSkip())
								Enddo

								dbGoTo(nRecSFF)

								//Verifica se deve calcular IVA
								lCalcMon := F850CheckLim(cItem,aCf,cFornece,nMinimo,SF1->F1_DOC,SF1->F1_SERIE,nMinUnit,"IVA",,1,Iif(lMsFil,SF1->F1_MSFIL,""))
								Exit
							Endif
							SFF->(dbSkip())
						Enddo
					Endif

					If lCalcMon

						aConIva[nCount][2]   := Round(xMoeda(aConIva[nCount][2],SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))
						aConIva[nCount][3]   := Round(xMoeda(aConIva[nCount][3],SF1->F1_MOEDA,1,,5,aTxMoedas[Max(SF1->F1_MOEDA,1)][2]),MsDecimais(1))

						AAdd(aSFEIVA,array(11))

						aSFEIVA[Len(aSFEIVA)][1] := SF1->F1_DOC         		//FE_NFISCAL
						aSFEIVA[Len(aSFEIVA)][2] := SF1->F1_SERIE       		//FE_SERIE
						aSFEIVA[Len(aSFEIVA)][3] := (aConIva[nCount][3]*nProp)*nSigno	//FE_VALBASE
						aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2]*nProp)*nSigno	//FE_VALIMP
						aSFEIVA[Len(aSFEIVA)][5] := Iif(Alltrim(PadR(SF1->F1_SERIE,nTamSer))=="M",100,SA2->A2_PORIVA)      		//FE_PORCRET
						aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][4]) *(Iif(Alltrim(PadR(SF1->F1_SERIE,nTamSer))=="M",1,Iif(lCalRep /*.or. lCalcVen*/ ,1, SA2->A2_PORIVA/100) ))
						aSFEIVA[Len(aSFEIVA)][7] := nSaldo
						aSFEIVA[Len(aSFEIVA)][8] := SE2->E2_EMISSAO
						If Len(aConIva[nCount]) >4 .And. !Empty(aConIva[nCount][5])
							aSFEIVA[Len(aSFEIVA)][9] := aConIva[nCount][5] // Gravar CFOP da opera��o
						Else
						 	aSFEIVA[Len(aSFEIVA)][9] := aConIva[nCount][1] // Gravar CFOP da opera��o
						EndIf                
						aSFEIVA[Len(aSFEIVA)][10] := aConIva[nCount][4]*100 // Gravar A PORCENTAGEM DA ALIQUOTA
						aSFEIVA[Len(aSFEIVA)][11] := " "						

						If SA2->(ColumnPos("A2_IVRVCOB")) > 0 .And. SA2->(ColumnPos("A2_IVPCCOB")) > 0
							/*If SA2->A2_PORIVA < 100.00 .And. (Empty(SA2->A2_IVPCCOB) .Or. Dtos(SA2->A2_IVPCCOB) < Dtos(dDataBase))
								MsgAlert(OemToAnsi(STR0155)+SA2->A2_COD+OemToAnsi(STR0147)) //"La fecha de validad para la reducci�n del porcentaje de la retenci�n del IVA del proveedor ya se ha vencido. ". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
								//Zera o array das retencoes de IVA...
								aSFEIVA := {}
								//Sai do loop...
								Exit
							EndIf*/

							//nPercRet:=SFF->FF_ALIQ/100

							If Dtos(SA2->A2_IVRVCOB) < Dtos(dDataBase)
								MsgAlert(OemToAnsi(STR0146)+SA2->A2_COD+OemToAnsi(STR0147)) //	"Ha vencido el perido de observacion del proveedor  "+SA2->A2_COD+". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
								//Zera o array das retencoes de IVA...
								aSFEIVA := {}
								//Sai do loop...
								Exit
							EndIf
						Endif
					Endif
				Next nCount
			EndIf
		//Calculo do PA
		Elseif lRetPa

			lCalRep := Iif(SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("2/4"),.T.,.F.)
			SFF->(DbSetOrder(5))
			SFF->(MsSeek(xFilial("SFF")+"IVR"+cCF)) //cConceito pegar do TES (CFO)
			If SFF->(FOUND())
				While !SFF->(Eof()) .And. xFilial("SFF")+"IVR"+cCF == SFF->(FF_FILIAL+FF_IMPOSTO+FF_CFO_C)
					If !Empty(cSerieNF) .And. PadR(cSerieNF,Len(SFF->FF_SERIENF)) != SFF->FF_SERIENF
						SFF->(dbSkip())
						Loop
					EndIf

		            cCodFis:= SFF->FF_CFO
					If SFF->(ColumnPos("FF_VLRLEX"))>0 .And. SFF->(ColumnPos("FF_ALIQLEX"))>0
						nPercRet:=Iif(nSaldo>SFF->FF_VLRLEX,SFF->FF_ALIQ/100,SFF->FF_ALIQLEX/100)
		            Else
			            nPercRet:=SFF->FF_ALIQ/100
					EndIf

					AAdd(aSFEIVA,array(9))
					aSFEIVA[Len(aSFEIVA)][1] := ""         		//FE_NFISCAL
					aSFEIVA[Len(aSFEIVA)][2] := ""       		//FE_SERIE
					aSFEIVA[Len(aSFEIVA)][3] := nSaldo * nSigno	//FE_VALBASE
					aSFEIVA[Len(aSFEIVA)][4] := 0	//FE_VALIMP
					aSFEIVA[Len(aSFEIVA)][5] := SA2->A2_PORIVA   		//FE_PORCRET
					aSFEIVA[Len(aSFEIVA)][6] := (aSFEIVA[Len(aSFEIVA)][3]) *(Iif(lCalRep ,1, SA2->A2_PORIVA/100))
					aSFEIVA[Len(aSFEIVA)][9] := cCodFis // Gravar CFOP da opera��o

					If SA2->(ColumnPos("A2_IVRVCOB")) > 0 .And. SA2->(ColumnPos("A2_IVPCCOB")) > 0
					 	If lCalRep
					 		nRetIva:=(aSFEIVA[Len(aSFEIVA)][3])*nPercRet
						Else
							nRetIva:= ((aSFEIVA[Len(aSFEIVA)][3]) *(SA2->A2_PORIVA/100))*nPercRet
						EndIf
					Else
						If lCalRep
					 		nRetIva:= (aSFEIVA[Len(aSFEIVA)][3])*nPercRet
					   	Else
							nRetIva:= ((aSFEIVA[Len(aSFEIVA)][3]) *(SA2->A2_PORIVA/100))*nPercRet
					   	EndIf
					EndIf
	
					aArea:=GetArea()
					DbSelectArea("SFE")
					SFE->(dbSetOrder(4))
					If SFE->(MsSeek(xFilial("SFE")+cFornece+cLoja))
				  	   	cChaveSFE := xFilial("SFE")+SFE->FE_FORNECE+SFE->FE_LOJA
						While !SFE->(Eof()) .And. cChaveSFE == SFE->FE_FILIAL+SFE->FE_FORNECE+SFE->FE_LOJA
							If SFE->FE_TIPO =="I" .And. !Empty(cNumOp) .And. SFE->FE_NUMOPER == cNumOp
								nRetTotal += SFE->FE_RETENC
							EndIf
							SFE->(dbSkip())
						End
				    EndIf
	
					RestArea(aArea)
	
					aSFEIVA[Len(aSFEIVA)][6]:=Iif(nRetTotal>0,((nRetTotal-nRetIva)*-1),nRetIva)
	
	   				//Verifica se o valor a ser retido � maior que o valor do PA
					If nValor < aSFEIVA[Len(aSFEIVA)][6]
						aSFEIVA[Len(aSFEIVA)][6]:= nValor
					EndIF
	
	
					//Verifica se o valor de retencao calculado supera o valor
					//minimo a ser retido, caso seja inferior nao eh realizada
					//a retencao.
					nTotRet := 0
					aEval(aSFEIVA,{|x| nTotRet += x[6]})
					If SA2->(ColumnPos("A2_IVRVCOB")) > 0
						If  nTotRet < SFF->FF_IMPORTE .And. Empty(SA2->A2_IVRVCOB)
							  aEval(aSFEIVA,{|x| x[6] := 0})
						EndIf
					Else
						If  nTotRet < SFF->FF_IMPORTE
							  aEval(aSFEIVA,{|x| x[6] := 0})
						EndIf
					Endif
					
					If !Empty(cSerieNF)
						SFF->(dbSkip())
					Else
						Exit
					EndIf
				EndDo
            EndIf
		EndIF
	EndIf
EndIf
RestArea(aArea)

Return aSFEIVA
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ARGRetIM2� Autor �	Bruno Schmidt     � Data �  14/08/14  ���
�������������������������������������������������������������������������͹��
���Desc.     �ARGRetIM2 - Calculo de Ret de Iva Monotributista para NCP   ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ARGRetIM2(cAgente,nSigno,nSaldo,nProp,cChavePOP,cNFPOP,cSeriePOP,dEmissao)
Local aCf		:= {}
Local aConIva	:= {}
Local aSFEIVA	:= {}
Local lRetSerie	:= .F.
Local lCalRet	:= .F.
Local lCalcImp	:= .T.
Local lCalRep	:= .F.
Local lCalcMon	:= .F.
Local lRetSit	:= .T.
Local lPropIVA	:= Iif(GetMV("MV_PROPIVA",.F.,2)==2,.T.,.F.)
Local nMinimo	:= 0
Local nMinUnit	:= 0
Local nPosIva	:= 0
Local nCount	:= 0
Local nI		:= 1
Local nParcela	:= 0
Local nPosCF	:= 0
Local nRecSF2	:= 0 
Local nTamSer := SerieNfId('SF2',6,'F2_SERIE')
Local lTESNoExen := .F.  

DEFAULT nSigno	:= -1
DEFAULT nProp 	:= 1
DEFAULT cChavePOP:= ""
DEFAULT cNFPOP	 := ""
DEFAULT cSeriePOP:= ""
DEFAULT dEmissao := CTOD("//")
//+---------------------------------------------------------------------+
//� Obter Impostos somente qdo a Empresa Usuario for Agente de Reten��o.�
//+---------------------------------------------------------------------+

If lShowPOrd .and. Funname() == "FINA847" .and. Type("lCtaCte")<>"U" .and. lCtaCte
	Return aSFEIVA := ObtReten(cChavePOP,cNFPOP,cSeriePOP,"I",nSaldo,dEmissao)
EndIf

dbSelectArea("SF2")
dbSetOrder(1)
If lMsFil
	nRecSF2 := FINBuscaNF(SE2->E2_MSFIL,SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
	SF2->(dbGoTo(nRecSF2))
Else
	nRecSF2 := FINBuscaNF(xFilial("SF2", SE2->E2_FILORIG),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_FORNECE,SE2->E2_LOJA,"SF2",.T.)
	SF2->(dbGoTo(nRecSF2))
EndIf

lCalRet := Iif(Alltrim(PadR(SF2->F2_SERIE,nTamSer)) == "M",.T.,.F.)

If ExistBlock("F0851IMP")
	lCalcImp:=ExecBlock("F0851IMP",.F.,.F.,{"IV2"})
EndIf

// Tratamento do Reproweb - Situa��o igual a 0 n�o deve reter Resoluci�n General ( AFIP) 2226/07

SA2->( dbSetOrder(1) )
If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
	SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
Else
	SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
Endif


 If SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("0")
	 lRetSit:=.F.
 EndIf

If lCalcImp  .And.  lRetSit
	SA2->( dbSetOrder(1) )
	If lMsFil .And. !Empty(xFilial("SA2")) .And. xFilial("SF1") == xFilial("SE2")
		SA2->( MsSeek(SE2->E2_MSFIL+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Else
		SA2->( MsSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA) )
	Endif

	//���������������������������������������������������������������������Ŀ
	//� Fornecedor � Agente de Reten��o n�o Retem IVA.                      �
	//�����������������������������������������������������������������������
	If SA2->A2_AGENRET == "N" .Or. lCalRet

		lCalRep := Iif(SA2->(ColumnPos("A2_SITU")>0) .and. Alltrim(SA2->A2_SITU)$("2/4"),.T.,.F.)

		While Alltrim(SF2->F2_ESPECIE)<>AllTrim(SE2->E2_TIPO).And.!EOF()
			SF2->(DbSkip())
			Loop
		Enddo

		//Caso o valor da nota seja parcelado, atribui o valor da parcela.
		If !Empty(SE2->E2_PARCELA) .And. lPropIVA
			nParcela := SE2->E2_VALOR
		Endif

		If AllTrim(SF2->F2_ESPECIE)==Alltrim(SE2->E2_TIPO).And.;
			Iif(lMsFil,SF2->F2_MSFIL,xFilial("SF2",SE2->E2_FILORIG))+SE2->E2_NUM+SE2->E2_PREFIXO+SE2->E2_FORNECE+SE2->E2_LOJA==;
			F2_FILIAL+F2_DOC+PadR(F2_SERIE,nTamSer)+F2_CLIENTE+F2_LOJA

			//����������������������������������������������������������������Ŀ
			//� Se esta definida uma retencao para uma serie especial, verifico�
			//� antes de procurar no SD1.(Serie 'M' foi o primeiro caso).      �
			//������������������������������������������������������������������
			SFF->(DbSetOrder(3))

			If SFF->(MsSeek(xFilial("SFF")+"IVR"+PadR(SF2->F2_SERIE,nTamSer)+"SER"))
				nPosIva:=ASCAN(aConIva,{|x| x[1]==SF2->F2_SERIE})
				If nPosIva<>0
					aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SF2->F2_VALIMP1)
					aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SF2->F2_BASIMP1)
				Else
					Aadd(aConIva,{SF2->F2_SERIE,SF2->F2_VALIMP1,SF2->F2_BASIMP1})
				Endif
				lRetSerie	:=	.T.
			Else
				//��������������������������������������������������������������Ŀ
				//� Obter o Valor do Imposto e Base baseando se no rateio do valor �
				//� do titulo pelo total da Nota Fiscal.                           �
				//������������������������������������������������������������������
				SD2->(DbSetOrder(3))
				If lMsFil
					SD2->(MsSeek(SF2->F2_MSFIL+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
				Else
					SD2->(MsSeek(xFilial("SD2",SE2->E2_FILORIG)+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA))
				EndIf
				If SD2->(Found())
					Do while Iif(lMsFil,SD2->D2_MSFIL,xFilial("SD2",SE2->E2_FILORIG))==SD2->D2_FILIAL.And.SF2->F2_DOC==SD2->D2_DOC.AND.;
						SF2->F2_SERIE==SD2->D2_SERIE.AND.SF2->F2_CLIENTE==SD2->D2_CLIENTE;
						.AND.SF2->F2_LOJA==SD2->D2_LOJA.AND.!SD2->(EOF())

						If AllTrim(SD2->D2_ESPECIE)<>AllTrim(SF2->F2_ESPECIE)
							SD2->(DbSkip())
							Loop
						Endif
						aImpInf := TesImpInf(SD2->D2_TES)
						lTESNoExen := aScan(aImpInf,{|x| "IV" $ AllTrim(x[1])}) <> 0
						If !lTESNoExen
							SD2->(DbSkip())
							Loop
						EndIf
						SB1->(DbSetOrder(1))
						If !Empty(SD2->D2_CF)
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SD2->D2_CF})
							If nPosIva<>0
								If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BAIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+((aConIva[nPosIva][4] * (SD2->D2_TOTAL ) )- ((SD2->D2_IVAFRET+SD2->D2_IVAGAST)))
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+((SD2->D2_TOTAL ) - ((SD2->D2_BaIVAFR+SD2->D2_BaIVAGA)) )
								Else
									aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(aConIva[nPosIva][4] * (SD2->D2_TOTAL ) )
									aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD2->D2_TOTAL )
								EndIf
							Else
								If  lCalRep
									aImpInf := TesImpInf(SD2->D2_TES)
									For nI := 1 To Len(aImpInf)
										If  "IV"$Trim(aImpInf[nI][01]) .And.  Trim(aImpInf[nI][01])<>"IVP"
											Aadd(aConIva,{SD2->D2_CF,SD2->(FieldGet(ColumnPos(aImpInf[nI][02]))),SD2->(FieldGet(ColumnPos(aImpInf[nI][07]))), (SD2->(FieldGet(ColumnPos(aImpInf[nI][10]))) /100)})
										 EndIf
									Next
								Else
									SFF->(DbSetOrder(5))
									If SFF->(MsSeek(xFilial("SFF")+"IVR"))
										nPerc:=0
										lAchou:=.F.
										While SFF->(!EOF() ).and. (xFilial("SFF")+"IVR") ==(SFF->FF_FILIAL+SFF->FF_IMPOSTO) .And.  !lAchou
											If (Alltrim(SFF->FF_SERIENF)==Alltrim(PadR(SD2->D2_SERIE,nTamSer))  .And.  SD2->D2_CF==  SFF->FF_CFO_V)
												lAchou:=.T.
												nPerc:=SFF->FF_ALIQ
											Else
												SFF->(DbSkip())
											EndIf
										EndDo

										If SD2->(ColumnPos("D2_IVAFRET"))>0  .And.  SD2->(ColumnPos("D2_IVAGAST"))>0  .And. SD2->(ColumnPos("D2_BAIVAGA"))>0   .And.  SD2->(ColumnPos("D2_BAIVAFR"))>0
											Aadd(aConIva,{SD2->D2_CF,(((nPerc /100) * Iif(nParcela == 0,(SD2->D2_TOTAL ),nParcela))- (SD2->D2_IVAFRET+SD2->D2_IVAGAST)),((SD2->D2_TOTAL )- (SD2->D2_BaIVAFR +SD2->D2_BaIVAGA)), (nPerc /100)})
										Else
											Aadd(aConIva,{SD2->D2_CF,((nPerc /100) * Iif(nParcela == 0,(SD2->D2_TOTAL ),nParcela)) ,(SD2->D2_TOTAL ), (nPerc /100)})
										EndIf
									EndIf
								EndIf
							EndIF
						ElseIf SB1->(MsSeek(Iif(lMsFil .And. !Empty(xFilial("SB1")),SD2->D2_MSFIL,xFilial("SB1")) + SD2->D2_COD))
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SB1->B1_CONCIVA})
							If nPosIva<>0
								aConIva[nPosIva][2]:=aConIva[nPosIva][2]+SD2->D2_VALIMP1
								aConIva[nPosIva][3]:=aConIva[nPosIva][3]+SD2->D2_BASIMP1
							Else
								Aadd(aConIva,{SB1->B1_CONCIVA,SD2->D2_VALIMP1,SD2->D2_BASIMP1})
							Endif
						Else
							nPosIva:=ASCAN(aConIva,{|x| x[1]==SA2->A2_ACTRET})
							If nPosIva<>0
								aConIva[nPosIva][2]:=aConIva[nPosIva][2]+(SD2->D2_VALIMP1)
								aConIva[nPosIva][3]:=aConIva[nPosIva][3]+(SD2->D2_BASIMP1)
							Else
								Aadd(aConIva,{SA2->A2_ACTRET,SD2->D2_VALIMP1,SD2->D2_BASIMP1})
							Endif
						Endif
						SD2->(DbSkip())
					EndDo
				Endif
			Endif
			//����������������������������������������������������������������Ŀ
			//� Gravar Retenciones.                                            �
			//������������������������������������������������������������������
			For nCount:=1  to Len(aConIva)

				//Verifica se fara o calculo de IVA para fornecedores monotributistas
				dbSelectArea("SFF")
				dbGoTop()
				dbSetOrder(6)

				If MsSeek(xFilial("SFF")+"IVR"+aConIva[nCount][1])
					While SFF->(!Eof()) .And. SFF->FF_CFO_V == aConIva[nCount][1]

						If SFF->FF_ITEM $	"I1|I2" .And. SFF->FF_TIPO == 'M';
						   .And. AllTrim(SFF->FF_SERIENF) == AllTrim(PadR(SF2->F2_SERIE,nTamSer))

							cItem		:= SFF->FF_ITEM
							nMinimo  	:= Iif(SFF->(ColumnPos("FF_LIMITE"))>0,SFF->FF_LIMITE,0)
							nMinUnit 	:= Iif(SFF->(ColumnPos("FF_MINUNIT"))>0,SFF->FF_MINUNIT,0)

							nRecSFF := Recno()

							dbGoTop()
							MsSeek(xFilial("SFF")+"IVR")

							//Array contendo todos os CFOs com a mesma classifica��o
							While SFF->(!Eof())
								If SFF->FF_ITEM == cItem
									If aScan(aCf,{|x| x[2] == SFF->FF_CFO_V}) == 0
										aAdd(aCf,{SFF->FF_CFO_C,SFF->FF_CFO_V})
									Endif
								Endif
								SFF->(dbSkip())
							Enddo

							dbGoTo(nRecSFF)

							//Verifica se deve calcular IVA
							lCalcMon := F850CheckLim(cItem,aCf,cFornece,nMinimo,SF2->F2_DOC,SF2->F2_SERIE,nMinUnit,"IVA",,2,Iif(lMsFil,SF2->F2_MSFIL,""))
							Exit
						Endif
						SFF->(dbSkip())
					Enddo
				Endif

				If lCalcMon

					aConIva[nCount][2]   := Round(xMoeda(aConIva[nCount][2],SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))
					aConIva[nCount][3]   := Round(xMoeda(aConIva[nCount][3],SF2->F2_MOEDA,1,,5,aTxMoedas[Max(SF2->F2_MOEDA,1)][2]),MsDecimais(1))

					AAdd(aSFEIVA,array(11))
					aSFEIVA[Len(aSFEIVA)][1] := SF2->F2_DOC         		//FE_NFISCAL
					aSFEIVA[Len(aSFEIVA)][2] := SF2->F2_SERIE       		//FE_SERIE
					aSFEIVA[Len(aSFEIVA)][3] := (aConIva[nCount][3]*nProp)*nSigno	//FE_VALBASE
					aSFEIVA[Len(aSFEIVA)][4] := (aConIva[nCount][2]*nProp)*nSigno	//FE_VALIMP
					aSFEIVA[Len(aSFEIVA)][5] := Iif(lCalRet,100,SA2->A2_PORIVA)//FE_PORCRET
					aSFEIVA[Len(aSFEIVA)][6] := 0
					aSFEIVA[Len(aSFEIVA)][9] := aConIva[nCount][1]  // Gravar CFOP da opera��o
					If nPosCF	:=	aScan(aCf,{|x| x[2] == aConIva[nCount][1] }) == 0
						aSFEIVA[Len(aSFEIVA)][9] := aCF[nPosCF,1] //Gravar CFOP de compras
					Endif
					aSFEIVA[Len(aSFEIVA)][10] := aConIva[nCount][4]*100
					aSFEIVA[Len(aSFEIVA)][11] := " "
					If SA2->(ColumnPos("A2_IVRVCOB")) > 0 .And. SA2->(ColumnPos("A2_IVPCCOB")) > 0
						If SA2->A2_PORIVA < 100.00 .And. (Empty(SA2->A2_IVPCCOB) .Or. Dtos(SA2->A2_IVPCCOB) < Dtos(dDataBase))
							MsgAlert(OemToAnsi(STR0154)+SA2->A2_COD+OemToAnsi(STR0146)) //"La fecha de validad para la reducci�n del porcentaje de la retenci�n del IVA del proveedor ya se ha vencido. ". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
							//Zera o array das retencoes de IVA...
							aSFEIVA := {}
							//Sai do loop...
							Exit
						EndIf

						If Dtos(SA2->A2_IVRVCOB) < Dtos(dDataBase)
							MsgAlert(OemToAnsi(STR0146)+SA2->A2_COD+OemToAnsi(STR0147)) //	"Ha vencido el perido de observacion del proveedor  "+SA2->A2_COD+". Ingrese una fecha valida para el proveedor en el archivo de proveedores."
							//Zera o array das retencoes de IVA...
							aSFEIVA := {}
							//Sai do loop...
							Exit
						Else
							aSFEIVA[Len(aSFEIVA)][6] := aSFEIVA[Len(aSFEIVA)][4]
						EndIf

		      		EndIf
					//����������������������������������������������������������������Ŀ
					//� Generar Titulo de Impuesto no Contas a Pagar.                  �
					//������������������������������������������������������������������
					aSFEIVA[Len(aSFEIVA)][7] := SE2->E2_VALOR
					aSFEIVA[Len(aSFEIVA)][8] := SE2->E2_EMISSAO

				Endif
			Next
		EndIf
	EndIf
EndIf
Return aSFEIVA


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ObtRegSF � Autor �  Luis Samaniego    � Data �  19/05/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � Obtener informacion de SFF y SFH                           ���
�������������������������������������������������������������������������͹��
���Uso       � Funcao auxiliar                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
function ObtRegSF(cImpuesto, cTabla)
Local aDatosSFF	:= {}
Local cQuery	:= "" 
Local cAlias	:= CriaTrab(Nil,.F.)
Local lData := .F.

Private aTmpArea := {}

Default cImpuesto := ""
                     
If cTabla == "SFF" .And. cPaisLoc == "ARG"
	cQuery := " SELECT FF_CFO_C, FF_CFO_V, FF_IMPORTE, FF_CONCEPT, FF_IMPOSTO, FF_RET_MUN, FF_INCIMP, FF_ALIQ "
	cQuery += " FROM " + RetSQLName("SFF")
	cQuery += " WHERE "
	cQuery += " FF_FILIAL = '" + xFilial("SFF") + "' AND "
	cQuery += " FF_RET_MUN ='" + SA2->A2_RET_MUN + "' AND "
	If cImpuesto == "CEI"
		cQuery += "FF_CFO_C = '' AND "
		cQuery += "FF_CFO_V = '' AND "	
	Endif
	cQuery += " D_E_L_E_T_<>'*'" 

	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.) 
	
	(cAlias)->( dbGoTop() ) 
	IF  !(cAlias)->(Eof())  
		While !(cAlias)->(Eof())
		  If ( (cAlias)->FF_IMPOSTO == "CEI" .AND. (cAlias)->FF_RET_MUN == "00004") .AND. aScan(aDatosSFF,{|x|x[5]== "CEI"}) > 0
		    (cAlias)->(dbSkip())
		  Else 	
			aAdd( aDatosSFF, {(cAlias)->FF_CFO_C, (cAlias)->FF_CFO_V, (cAlias)->FF_IMPORTE, (cAlias)->FF_CONCEPT,; 
				(cAlias)->FF_IMPOSTO, (cAlias)->FF_RET_MUN, (cAlias)->FF_INCIMP,(cAlias)->FF_ALIQ } )	
			(cAlias)->(dbSkip())
		  Endif
		Enddo
	Endif
	
ElseIf cTabla == "SFH" .And. cPaisLoc $ "ANG|ARG|BOL|COL|COS|DOM|EQU|EUA|HAI|MEX|PAD|PAN|PAR|PER|POR|PTG|RUS|SAL|URU|VEN"
	cQuery := " SELECT FH_FORNECE, FH_LOJA, FH_IMPOSTO, FH_ALIQ, FH_SITUACA, FH_INIVIGE, FH_FIMVIGE, FH_ISENTO, FH_PERCENT, FH_ZONFIS "
	cQuery += " FROM " + RetSQLName("SFH")
	cQuery += " WHERE "
	cQuery += " FH_FILIAL = '" + xFilial("SFH") + "' AND "
	cQuery += " FH_FORNECE ='" + SA2->A2_COD + "' AND " 
	cQuery += " FH_LOJA ='" + SA2->A2_LOJA + "' AND " 
	If cPaisLoc <> "ARG"
		cQuery += " FH_IMPOSTO ='" + cImpuesto + "' AND "
	Endif
	cQuery += " FH_PERCIBI = 'S' AND "
	If cPaisLoc == "ARG" .And. cImpuesto == "CEI"
		cQuery += " ((FH_INIVIGE = '' AND FH_FIMVIGE = '')"
		cQuery += " OR "
		cQuery += " ('" + Dtos(dDatabase) + "' >= FH_INIVIGE  AND '" + Dtos(dDatabase) + "' <= FH_FIMVIGE)"    
		cQuery += " OR "
		cQuery += " ('" + Dtos(dDatabase) + "' >= FH_INIVIGE  AND  FH_FIMVIGE = '')) AND " + " FH_IMPOSTO ='" + cImpuesto + "' "
		cQuery += " AND "
	Endif
	If cPaisLoc == "ARG" .And. cImpuesto == "RMT"
		cQuery += " FH_ZONFIS = 'TU' AND "
	Endif
	cQuery += " D_E_L_E_T_<>'*'" 
	
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.T.,.T.) 
	
	(cAlias)->( dbGoTop() )
	
	While !(cAlias)->(Eof())  
		If ((cAlias)->FH_ISENTO = 'S') .OR. ((cAlias)->FH_PERCENT = 100) 
			lNoIsento := .F.
		Endif
		If 	(cAlias)->FH_ALIQ = 0
			lCero := .T.
		Endif  
		If ((Dtos(dDatabase) >= (cAlias)->FH_INIVIGE  .AND.  Dtos(dDatabase) <= (cAlias)->FH_FIMVIGE);
			.OR. (Dtos(dDatabase) >= (cAlias)->FH_INIVIGE  .AND.  (cAlias)->FH_FIMVIGE = ''); 
				.OR. (EMPTY((cAlias)->FH_INIVIGE)  .AND.  EMPTY((cAlias)->FH_FIMVIGE)) )
		lSFH := .T.
		Else
			(cAlias)->(dbSkip())
			LOOP
		Endif
			aAdd( aDatosSFF, {(cAlias)->FH_FORNECE, (cAlias)->FH_LOJA, (cAlias)->FH_IMPOSTO, (cAlias)->FH_ALIQ, (cAlias)->FH_SITUACA ,;
			FH_ZONFIS } )	
			(cAlias)->(dbSkip())
	Enddo
 
		
EndIf
(cAlias)->(dbCloseArea())  
 Return aDatosSFF
 
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ObtRegSF � Autor �  Luis Samaniego    � Data �  19/05/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � Obtener informacion de SFF y SFH                           ���
�������������������������������������������������������������������������͹��
���Uso       � Funcao auxiliar                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/ 
Function ObtDesgl(cImpsto,cCodC,cZona,cTipo,cCodAdc,nAliqAdc) 
Local lRet		:= .F.  
Local cTabTemp	:= criatrab(nil,.F.)   
Local cQuery  	:= ""  
Local nNumRegs	:= 0 

If cPaisLoc $ "ANG|ARG|BOL|COL|COS|DOM|EQU|EUA|HAI|MEX|PAD|PAN|PAR|PER|POR|PTG|RUS|SAL|URU|VEN"

	cQuery := "SELECT * "
	cQuery += "FROM " + RetSqlName("SFF")+ " SFF "
	cQuery += "WHERE FF_TIPO ='"+ cTipo +"' AND "
	cQuery += "FF_CFO_C   ='" + cCodAdc + "' AND "
	cQuery += "FF_IMPOSTO ='" + cImpsto +"' AND "
	cquery += "FF_ZONFIS  ='" + cZona +"' AND "
	cQuery += "FF_FILIAL='" +XFILIAL("SFF") + "' AND "
	cQuery += "D_E_L_E_T_<>'*'"
	
	cQuery := ChangeQuery(cQuery)
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cTabTemp,.T.,.T.)
	
	Count to nNumRegs
	
	If  nNumRegs > 0	//Verificar que exista un registro adicional
		(cTabTemp)->(dbGoTop())
		If (cTabTemp)->(!eof())
			nAliqAdc:= (cTabTemp)->FF_ALIQ  
			lRet    := .T.
		Endif   
	Endif 

EndIf

Return lRet     

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ObtRegSF � Autor �  Luis Samaniego    � Data �  19/05/14   ���
�������������������������������������������������������������������������͹��
���Desc.     � Obtener informacion de SFF y SFH                           ���
�������������������������������������������������������������������������͹��
���Uso       � Funcao auxiliar                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/ 
Function ARGBXIVA
Local cAreaSE2:=SE2->(GetArea())
Local cArea	  :=GetArea()
Local lCalc	  :=.T.  
         
cPesq:=xFilial("SE2")+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA
DbSelectArea("SE2")
DbSetOrder(1)
MsSeek(cPesq) 

While !EOF() .And. lCalc .and. (cPesq== SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA)
	lCalc:=SE2->E2_SALDO==SE2->E2_VALOR
	DbSkip()
EndDo 

Restarea(cArea)
SE2->(RestArea(cAreaSE2) )

Return(lCalc)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � FilImpto    � Autor �Emanuel Villicana � Data �  11/09/15  ���
�������������������������������������������������������������������������͹��
���Descricao � Extrae de SFB informacion filtrando                        ���
���          � FB_CLASSIF, FB_CLASSE,FB_TIPO , FB_ESTADO                  ���
�������������������������������������������������������������������������͹��
���Uso       � General                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function FilImpto(aClasif,aClase,aTipo,aEdo)
Local aRetorno	:= {}
Local cQryNLvro	:= ""
Local cTRBLVRO	:= "" 

Local cClasif	:= ""
Local cClase	:= ""
Local cEdo	:= ""
Local cTipo	:= ""
Local nI	:= 0

DEFAULT aClasif	:= {}
DEFAULT aClase	:= {}
DEFAULT aEdo	:= {}
DEFAULT aTipo	:= {}

If 	cPaisLoc == "ARG"
	//=============================================================================================
	//SFB - IMPUESTOS VARIABLES
	//=============================================================================================
	
	cQryNLvro := " SELECT DISTINCT FB_CLASSIF ,FB_CLASSE ,FB_CODIGO ,FB_DESCR , FB_CPOLVRO ,FB_TIPO TIPO, FB_ESTADO "
	cQryNLvro += " FROM "+RetsqlName("SFB")+" SFB "
	cQryNLvro += " WHERE D_E_L_E_T_='' "
	
	If Len(aClasif) > 0
		For nI := 1 to Len(aClasif)
			cClasif += Iif(Empty(cClasif),"",",") + "'" + aClasif[nI] + "'"
		Next nI
		cQryNLvro += " AND FB_CLASSIF IN ( " + cClasif +  " ) "
	Endif 
	
	If Len(aClase) > 0
		For nI := 1 to Len(aClase)
			cClase += Iif(Empty(cClase),"",",") + "'" + aClase[nI] + "'"
		Next nI
	
		cQryNLvro += " AND FB_CLASSE IN ( " + cClase +  " ) "
	Endif 
	
	If Len(aTipo) > 0
		For nI := 1 to Len(aTipo)
			cTipo += Iif(Empty(cTipo),"",",") + "'" + aTipo[nI] + "'"
		Next nI
	
		cQryNLvro += " AND FB_TIPO IN ( " + cTipo +  " ) "
	Endif 
	
	If Len(aEdo) > 0
		For nI := 1 to Len(aEdo)
			cEdo += Iif(Empty(cEdo),"",",") + "'" + aEdo[nI] + "'"
		Next nI
	
		cQryNLvro += " AND FB_ESTADO IN ( " + cEdo +  " ) "
	Endif 
	
	If Select("TRBLVRO")>0
		DbSelectArea("TRBLVRO")
		TRBLVRO->(DbCloseArea())
	Endif
	
	cTRBLVRO := ChangeQuery(cQryNLvro)
	dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cTRBLVRO ) ,"TRBLVRO", .T., .F.)
	
	DbSelectArea("TRBLVRO")
	TRBLVRO->(dbGoTop())
	If TRBLVRO->(!Eof())
		Do While TRBLVRO->(!Eof())
			aAdd(aRetorno,{TRBLVRO->FB_CODIGO,TRBLVRO->FB_DESCR,TRBLVRO->FB_CPOLVRO,TRBLVRO->FB_ESTADO,;
							 TRBLVRO->FB_CLASSIF ,TRBLVRO->FB_CLASSE })
			TRBLVRO->(DbSkip())
		End
	Endif
	
	TRBLVRO->(DbCloseArea())

EndIf


Return aRetorno

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ValImpto    � Autor �Emanuel Villicana � Data �  11/09/15  ���
�������������������������������������������������������������������������͹��
���Descricao � Extrae el valor del impuesto generado en la factura        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG - FINA085A                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function ValImpto(aNrLvIVA,cTes)
Local nRetorno	:= 0
Local cAliasSD	:= Iif(cTES > "500","SD2","SD1")
Local cAliasSF	:= Iif(cTES > "500","SF2","SF1")
Local aTes	:= {}
Local aPos	:= {} 
Local nbusca	:= 0
Local cLib	:= ""
Local nlI := 0

aTes := TesImpFin(cTes)

For nlI:=1 To Len(aNrLvIVA)
	If aScan(aTes,{|x| x[1] == aNrLvIVA[nlI][1]}) > 0 .and. ;
		&((cAliasSD) +"->" + SubStr(cAliasSD,2,2)+"_BASIMP" + aNrLvIVA[nlI][3]) > 0
		
		cLib := "0" + aNrLvIVA[nlI][3]
		nbusca := aScan(aPos,{|x| x[1] == cLib })
		If nbusca == 0
			
			IF 	&((cAliasSF) +"->" + SubStr(cAliasSF,2,2)+"_TCOMP") = '033'  //COMERCIALIZACI�N DE GRANOS
				nRetorno += &((cAliasSD) +"->" + SubStr(cAliasSD,2,2)+"_BASIMP" + aNrLvIVA[nlI][3])
			Else
				nRetorno += &((cAliasSD) +"->" + SubStr(cAliasSD,2,2)+"_VALIMP" + aNrLvIVA[nlI][3])
			EndIf
		
			aAdd(aPos,{cLib})
		Endif 
		
	Endif 
Next nlI

Return nRetorno

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TesImpFin � Autor � Bruno Sobieski        � Data � 17.11.98 ���
�������������������������������������������������������������������������Ĵ��
���Nota      � Se  hace copia de la funcion TesImpInf para uso modulo Fin.���
���          � llamado TUNJNS                                             ���
���Descri��o � Retorna um array com os dados dos impostos relacionados com���
���          � o tes que � pasado como parametro. Usado para Impostos nas |��
���          � Localizacoes no Exterior.                                  ���
���          � aImpFlag[n][1]-> Codigo do Imposto                         ���
���          �         [n][2]-> Campo no SD1 ou SD2 onde � gravado o valor|��
���          �                  imposto.                                  ���
���          �         [n][3]-> Se o valor do imposto incide na Nota      ���
���          �         [n][4]-> Se o valor do imposto incide na Duplicata ���
���          �         [n][5]-> Se o valor do imposto deve ser Creditado  ���
���          �         [n][6]-> Campo no SF1 ou SF2 onde � gravado o valor|��
���          �                  imposto.                                  ���
���          �         [n][7]-> Campo no SD1 ou SD2 onde � gravada a base |��
���          �                  do imposto.                               ���
���          �         [n][8]-> Campo no SF1 ou SF2 onde � gravada a base |��
���          �                  do imposto.                               ���
���          �         [n][9]-> Aliquota do imposto                       ���
�������������������������������������������������������������������������Ĵ��
��� Parametro� cTes -> Codigo de Entrada-Saida                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico Localizacoes.                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function TesImpFin(cTes)
Local aImpFlag :={}
Local aArea	:= GetArea()
Local aAreaSFB	:= SFB->(GetArea())
Local aAreaSFC	:= SFC->(GetArea())

SFB->(DbSetOrder(1))

DbSelectArea("SFC")
DbSetOrder(1)
MsSeek(xFilial("SFC") + cTes)

While SFC->(!eof()) .and. xFilial("SFC")== FC_FILIAL .and. cTes== FC_TES
	If SFB->(MsSeek(xFilial("SFB")+SFC->FC_IMPOSTO))
		If (cTes<="500" )
			If SD1->(ColumnPos("D1_ALQIMP"+SFB->FB_CPOLVRO))  > 0
				Aadd(aImpFlag,{SFB->FB_CODIGO,"D1_VALIMP"+SFB->FB_CPOLVRO,SFC->FC_INCNOTA,SFC->FC_INCDUPL,SFC->FC_CREDITA,"F1_VALIMP"+SFB->FB_CPOLVRO,"D1_BASIMP"+SFB->FB_CPOLVRO,"F1_BASIMP"+SFB->FB_CPOLVRO,SFB->FB_ALIQ,"D1_ALQIMP"+SFB->FB_CPOLVRO,SFC->FC_PROV,SFC->FC_INCIMP})
			EndIf
		Else
			Aadd(aImpFlag,{SFB->FB_CODIGO,"D2_VALIMP"+SFB->FB_CPOLVRO,SFC->FC_INCNOTA,SFC->FC_INCDUPL,SFC->FC_CREDITA,"F2_VALIMP"+SFB->FB_CPOLVRO,"D2_BASIMP"+SFB->FB_CPOLVRO,"F2_BASIMP"+SFB->FB_CPOLVRO,SFB->FB_ALIQ,"D2_ALQIMP"+SFB->FB_CPOLVRO,SFC->FC_PROV,SFC->FC_INCIMP})
			If cPaisLoc == "PTG" .And. SFC->(ColumnPos("FC_ABATCOM")) > 0
				Aadd(aImpFlag[Len(aImpFlag)],SFC->FC_ABATCOM)
			EndIf
		EndIf                                               
	EndIf
	SFC->(DbSkip())
End

RestArea(aAreaSFB)
RestArea(aAreaSFC)
RestArea(aArea)

Return(aImpFlag)

Function F085BXIVA
Local cAreaSE2:=SE2->(GetArea())
Local cArea:=GetArea()
Local cPref:= SE2->E2_PREFIXO
Local cNum:=	SE2->E2_NUM
Local cForn:=SE2->E2_FORNECE
Local cLoja:=SE2->E2_LOJA
Local ctipo:=SE2->E2_TIPO
Local lCalc:=.t.           
cPesq:=xFilial("SE2")+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA
DbSelectArea("SE2")
DbSetOrder(1)
MsSeek(cPesq) 

While !EOF() .And. lCalc .and. (cPesq== SE2->E2_FILIAL+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA)
	lCalc:=SE2->E2_SALDO==SE2->E2_VALOR
	DbSkip()
EndDo 
Restarea(cArea)
SE2->(RestArea(cAreaSE2) )
Return(lCalc)

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � NoInsSFH    � Autor �Emanuel Villica�a � Data �  21/08/14  ���
�������������������������������������������������������������������������͹��
���Descricao � Identifica No inscripto en SFH                             ���
�������������������������������������������������������������������������͹��
���Uso       � FINA085A, FINA850                                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function NoInsSFH(cProv,cLoja,cImposto,cZona)
Local aArea      := GetArea()
Local aAreaSFH   := SFH->(GetArea())
Local lCalNoIns  := .T. 
 	
If 	cPaisLoc $ "ANG|ARG|BOL|COL|COS|DOM|EQU|EUA|HAI|MEX|PAD|PAN|PAR|PER|POR|PTG|RUS|SAL|URU|VEN"	 	
	SFH->(DbSetOrder(1))
	If SFH->(MsSeek(xFilial()+cProv + cLoja + cImposto + cZona))
		While !SFH->(EOF()) .and. (xFilial("SFH")+cProv + cLoja + cImposto + cZona == SFH->FH_FILIAL + SFH->FH_FORNECE + SFH->FH_LOJA + SFH->FH_IMPOSTO + SFH->FH_ZONFIS)
			If (SFH->FH_TIPO == "N" .and. A085aVigSFH())
				lCalNoIns := .T.
				Exit
			ElseIf A085aVigSFH() .and. SFH->FH_TIPO <> "N"
				lCalNoIns := .F.
				Exit 
			Endif
			SFH->(DbSkip())
		EndDo 
	Else
		lCalNoIns := .T.
	Endif 
	
	RestArea(aAreaSFH)
	RestArea(aArea)
Endif

Return lCalNoIns

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � NoInsCCORet � Autor �Emanuel Villica�a � Data �  21/08/14  ���
�������������������������������������������������������������������������͹��
���Descricao � Identifica que proceso se realizara para prov. No inscripto���
���Descricao � en retencion de IB                                         ���
�������������������������������������������������������������������������͹��
���Uso       � FINA085A, FINA850                                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function NoInsCCORet(cZonCCO,cProvProc)
Local aArea      := GetArea()
Local aAreaCCO   := CCO->(GetArea())
Local cTppeni    := ""
Local lCalNoIns  := .F.
Local aRes       := {}
Local cRadica	   := SA2->A2_EST

Default cZonCCO   := ""
Default cProvProc := ""
	
If CCO->(ColumnPos("CCO_TPRET")) > 0 .and. CCO->(ColumnPos("CCO_TPRENI")) > 0;
		.and. cPaisLoc == "ARG"	.and. !Empty(cZonCCO)
	CCO->(DbSetOrder(1))
	If CCO->(MsSeek(xFilial("CCO") + cZonCCO))
		cTppeni	:=	CCO->CCO_TPRENI 
		If !Empty(cTppeni) .and. CCO->CCO_TPRET == "3"
			If	( cTppeni == "1" .and. (cZonCCO == cProvProc  .or. cZonCCO == cRadica) ) .or.;
				( cTppeni == "2" .and. cZonCCO == cProvProc ) .or.;
				( cTppeni == "3" .and. cZonCCO == cRadica )
				lCalNoIns := .T.
			Else 
				lCalNoIns := .F.
			Endif 
		Else
			lCalNoIns := .F.
		Endif
	Endif  
Endif 
RestArea(aAreaCCO)
RestArea(aArea)
AAdd(aRes,{cTppeni,lCalNoIns})
Return aRes

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � FilImpRet   � Autor �Emanuel Villicana � Data �  02/10/15  ���
�������������������������������������������������������������������������͹��
���Descricao � Extrae importe retenido                                    ���
�������������������������������������������������������������������������͹��
���Uso       � FINA085A, FINA850                                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function FilImpRet(cFil,cProv,cLoj,cNFis,cSerie,cTipo,cOrdPag)

Local aRetorno	:= {}
Local cQryNLvro	:= ""
Local cTRBLVRO	:= ""
 
Default cOrdPag	:= ""

//=============================================================================================
//SFE - RETENCIONES IMPUESTOS
//=============================================================================================
cQryNLvro := " SELECT SUM(FE_RETENC) RETEN,"
cQryNLvro += " SUM(FE_VALBASE) BASE"
//cQryNLvro := " SELECT FE_FILIAL,FE_FORNECE,FE_LOJA,FE_NFISCAL,FE_SERIE,FE_TIPO,SUM(FE_RETENC) RETEN"
cQryNLvro += " FROM "+RetsqlName("SFE")+" SFE "
cQryNLvro += " WHERE D_E_L_E_T_='' "
cQryNLvro += " AND FE_FILIAL =  '" + cFil + "'"
cQryNLvro += " AND FE_FORNECE = '" + cProv + "'" 
cQryNLvro += " AND FE_LOJA = '" + cLoj + "'"

If Empty(cOrdPag)
	cQryNLvro += " AND FE_NFISCAL = '" + cNFis + "'"
	cQryNLvro += " AND FE_SERIE = '" + cSerie + "'"
Else
	cQryNLvro += " AND FE_ORDPAGO = '" + cOrdPag + "'"
Endif 

cQryNLvro += " AND FE_TIPO = '" + cTipo + "'"

If Select("TRBLVRO")>0
	DbSelectArea("TRBLVRO")
	TRBLVRO->(DbCloseArea())
Endif

cTRBLVRO := ChangeQuery(cQryNLvro)
dbUseArea(.T., 'TOPCONN', TcGenQry( ,, cTRBLVRO ) ,"TRBLVRO", .T., .F.)

DbSelectArea("TRBLVRO")
TRBLVRO->(dbGoTop())
If TRBLVRO->(!Eof())
	aAdd(aRetorno,TRBLVRO->RETEN)
	aAdd(aRetorno,TRBLVRO->BASE)
Endif
TRBLVRO->(DbCloseArea())

Return aRetorno

/*
������������������������������������������������������������������������ͻ
�Programa  �ObtGan     �Autor  �Raul Ortiz Medina   � Data �  07/02/17   �
������������������������������������������������������������������������͹
�Desc.     � Realiza el acumulado de los valores de Retenciones de       �
�          � Ganancias de Orden de Pago Previa                           �
������������������������������������������������������������������������͹
�Uso       � FINA850                                                     �
������������������������������������������������������������������������ͼ
*/

Static Function ObtGan(nRetenc,nDeduc,nBasRet,cFornece,cLoja,cConcep,lCond)
Local cQuery	:= ""
Local cTempGan  := GetNextAlias()

Default lCond	:= .F.

	cQuery := "SELECT SUM(FVC_RETENC) RETENC,SUM(FVC_DEDUC) DEDUC, SUM(FVC_VALBAS) VALBASE " 
	cQuery += "FROM " + RetSqlName("FVC") + " FVC, " + RetSqlName("FJK") + " FJK "
	cQuery += "WHERE "
	cQuery += "FVC_FILIAL = '"  + xFilial("FVC") + "' AND "
	cQuery += "FJK_FILIAL = '" + xFilial("FJK") + "' AND "
	If !lCond
		cQuery	+= "FVC_FORNEC = '" + cFornece + "' AND "
		cQuery += "FVC_LOJA = '" + cLoja + "' AND "
		cQuery += "FVC_FORNEC = FJK_FORNEC AND "
		cQuery += "FVC_LOJA = FJK_LOJA  AND "
	Else
		cQuery	+= "FVC_FORCON = '" + cFornece + "' AND "
		cQuery += "FVC_LOJCON = '" + cLoja + "' AND "
	EndIf
	cQuery += "FVC_TIPO = 'G' AND "
	cQuery += "FVC_CONCEP = '" + cConcep + "' AND "
	cQuery += "FVC_PREOP = FJK_PREOP AND "
	cQuery += "FJK_ORDPAG = '' AND "
	cQuery += "Substring(FJK_DTDIG,5,2) = " + AllTrim(STR(Month(dDataBase))) + " AND "
	cQuery += "Substring(FJK_DTDIG,1,4) = " + AllTrim(STR(Year(dDataBase))) + " AND "
	cQuery += "FVC.D_E_L_E_T_ = '' AND "
	cQuery += "FJK.D_E_L_E_T_ = ''"
	
	cQuery	:= ChangeQuery(cQuery)
	DbUseArea(.T.,"TOPCONN",TCGenQry(,,cQuery),cTempGan,.F.,.T.)
	(cTempGan)->(dbGoTop())
	
	If !(cTempGan)->(EOF())
		nBasRet += (cTempGan)->VALBASE
		nRetenc += (cTempGan)->RETENC
		nDeduc  += (cTempGan)->DEDUC
	EndIf
	
	(cTempGan)->(dbCloseArea())
	
	If Select((cTempGan)) == 0
		FErase(cTempGan + GetDBExtension())
		FErase(cTempGan + OrdBagExt())
	EndIF
	

Return


/*
������������������������������������������������������������������������ͻ
�Programa  �ObtReten   �Autor  �Raul Ortiz Medina   � Data �  19/09/19   �
������������������������������������������������������������������������͹
�Desc.     � Realiza el acumulado de los valores de Retenciones de una   �
�          � Orden de Pago Previa                                        �
������������������������������������������������������������������������͹
�Uso       � FINA850                                                     �
������������������������������������������������������������������������ͼ
*/

Static Function ObtReten(cChave, cNF, cSerie, cTipo, nSaldo, dEmissao, aConfProv, lReSaSus)
Local aReten := {}
Local cFil	 := xFilial("FVC")

DEFAULT cChave 		:= ""
DEFAULT cNF 		:= ""
DEFAULT cSerie		:= ""
DEFAULT cTipo 		:= ""
DEFAULT nSaldo 		:= 0
DEFAULT dEmissao 	:= CTOD("//")
DEFAULT aConfProv 	:= {}
DEFAULT	lReSaSus	:= .F.
 
	DBSELECTAREA("FVC")
	FVC->(DBSETORDER(2)) //FVC_FILIAL+FVC_PREOP+FVC_FORNEC+FVC_LOJA
	If FVC->(MsSeek(cFil + cChave))
		While FVC->(!Eof()) .and. FVC->(FVC_FILIAL+FVC_PREOP+FVC_FORNEC+FVC_LOJA) == cFil + cChave
			If cTipo == "G" .and. AllTrim(FVC->FVC_TIPO) == "G"
				aAdd(aReten,{"",FVC_VALBAS,FVC_ALIQ,FVC_RETENC,FVC_RETENC,FVC_DEDUC,FVC_CONCEP,FVC_PORCR,"",FVC_FORCON,FVC_LOJCON})
			Else
				If FVC->FVC_NFISC == cNF .and. FVC->FVC_SERIE == cSerie
					If AllTrim(FVC->FVC_TIPO) == "I" .and. cTipo == "I" //.and. cTipo == "I" //nf +serie
						aAdd(aReten,{FVC->FVC_NFISC,FVC->FVC_SERIE,FVC->FVC_VALBAS,FVC->FVC_RETENC,FVC->FVC_PORCR,FVC->FVC_RETENC,nSaldo,dEmissao,FVC->FVC_CFO,FVC->FVC_ALIQ,FVC->FVC_CFO,0})
					ElseIf AllTrim(FVC->FVC_TIPO) == "B" .and. cTipo == "B" .and. AllTrim(FVC->FVC_EST) == aConfProv[1]
						aAdd(aReten,{FVC->FVC_NFISC,FVC->FVC_SERIE,FVC->FVC_VALBAS,FVC->FVC_ALIQ,FVC->FVC_RETENC,FVC->FVC_RETENC,nSaldo,dEmissao,FVC->FVC_EST,SE2->E2_MOEDA,FVC->FVC_CFO,;
						FVC->FVC_CFO,SE2->E2_TIPO,FVC->FVC_CONCEP,FVC_DEDUC,FVC_PORCR,.F.,"",FVC->FVC_ALIQ,0,0,0,;
						0,0,"",0,0,0,aConfProv[6],.F.,0,0,0})				
					ElseIf cTipo == "S" .and. (AllTrim(FVC->FVC_TIPO) == "U" .or. AllTrim(FVC->FVC_TIPO) == "S")
						aAdd(aReten,{FVC->FVC_NFISC,FVC->FVC_SERIE,FVC->FVC_VALBAS,FVC->FVC_RETENC,FVC->FVC_PORCR,FVC->FVC_RETENC,FVC->FVC_ALIQ,FVC->FVC_CONCEP,FVC->FVC_EST,"",FVC_FORCON,FVC_LOJCON,Iif(lReSaSus,FVC_RETENC,0)})
					ElseIf cTipo == "L" .and. AllTrim(FVC->FVC_TIPO) == "L" 
						aAdd(aReten,{FVC->FVC_NFISC,FVC->FVC_SERIE,FVC->FVC_VALBAS,FVC->FVC_VALBAS,FVC_PORCR,FVC->FVC_RETENC})					
					ElseIf cTipo == "M" .and. AllTrim(FVC->FVC_TIPO) == "M" 
						aAdd(aReten,{FVC->FVC_NFISC,FVC->FVC_SERIE,FVC->FVC_VALBAS,FVC->FVC_RETENC,Round((FVC->FVC_RETENC*100)/nSaldo,2),FVC->FVC_RETENC,FVC->FVC_DEDUC,{{FVC->FVC_ALIQ,"",FVC->FVC_RETENC,""}},FVC->FVC_EST,FVC->FVC_RET_MN})
					EndIf
				EndIF
			EndIf
			FVC->(DbSkip())
		Enddo
	EndIF

Return aReten

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ValBasImp    � Autor �Marivaldo        � Data �     02/20  ���
�������������������������������������������������������������������������͹��
���Descricao �Extrae el valor del base del impuesto generado en la factura���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � FINRETARG - FINA085A                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function ValBasImp(aNrLvIVA,cTes)
Local nRetorno	:= 0
Local cAliasSD	:= Iif(cTES > "500","SD2","SD1")
Local cAliasSF	:= Iif(cTES > "500","SF2","SF1")
Local aTes	:= {}
Local aPos	:= {} 
Local nbusca	:= 0
Local cLib	:= ""
Local nlI := 0

aTes := TesImpFin(cTes)

For nlI:=1 To Len(aNrLvIVA)
	If aScan(aTes,{|x| x[1] == aNrLvIVA[nlI][1]}) > 0 .and. ;
		&((cAliasSD) +"->" + SubStr(cAliasSD,2,2)+"_BASIMP" + aNrLvIVA[nlI][3]) > 0
		
		cLib := "0" + aNrLvIVA[nlI][3]
		nbusca := aScan(aPos,{|x| x[1] == cLib })
		If nbusca == 0		
			nRetorno += &((cAliasSD) +"->" + SubStr(cAliasSD,2,2)+"_BASIMP" + aNrLvIVA[nlI][3])		
			aAdd(aPos,{cLib})
		Endif 
		
	Endif 
Next nlI

Return nRetorno