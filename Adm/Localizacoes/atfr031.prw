#include "RwMake.ch"
#Include "Protheus.ch"
#Include "TopConn.ch"
#Include "ATFR031.ch"

#define PIX_DIF_COLUNA_VALORES		148		// Pixel inicial para impressao dos tracos das colunas dinamicas
#define PIX_INICIAL_VALORES			148		// Pixel para impressao do traco vertical
#define PIX_EQUIVALENTE				110		// Pixel inicial para impressao das colunas dinamicas
#define MASK_VALOR					"@E 99999,999.99"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ATFR032   � Autor � Totvs              � Data �  18/05/10   ���
�������������������������������������������������������������������������͹��
���Descricao �Detalhes do Ativo Fixo Reavaliado Formato 7.2               ���
�������������������������������������������������������������������������͹��
���Uso       �ATFR032                                                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ATFR031()

Local cPerg		:= "ATR031"
Local olReport

Private aFieldSM0 	:= {"M0_NOMECOM", "M0_CGC"}
Private aDatosEmp 	:= IIf (cVersao <> "11" ,FWSM0Util():GetSM0Data(cEmpAnt, cFilAnt, aFieldSM0),"")
Private cRUC	  	:= Trim(IIf (cVersao <> "11" ,aDatosEmp[2][2],SM0->M0_CGC))
Private cApellido	:= Trim(IIf (cVersao <> "11" ,aDatosEmp[1][2],SM0->M0_NOMECOM))

/*����������������������������������������������������������������Ŀ
� mv_par01 - Exercicio? - Ano do exercicio para emissao            �
� mv_par02 - Seleciona filiais? - Filiais para considerar no filtro�
������������������������������������������������������������������*/
If TRepInUse()
	Pergunte(cPerg,.F.)

	olReport := ATFRelat(cPerg)
	olReport:SetParam(cPerg)
	olReport:PrintDialog()
EndIf

Return

/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ATFRelat  � Autor � Totvs                 � Data | 14/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Cria��o do objeto TReport para a impress�o do relatorio.    ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �ATFRelat( cPerg )           				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpC1 = Perguntas dos parametros                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ATFRelat( cPerg )

Local clNomProg		:= FunName()
Local clTitulo 		:= STR0001  // "FORMATO 7.1:REGISTRO DE ACIVOS FIJOS- DETALLE DE ACTIVOS"
Local clDesc   		:= STR0001  // "FORMATO 7.1:REGISTRO DE ACIVOS FIJOS- DETALLE DE ACTIVOS"
Local olReport

olReport:=TReport():New(clNomProg,clTitulo,cPerg,{|olReport| ATFProc(olReport)},clDesc)
olReport:SetLandscape()					// Formato paisagem
olReport:oPage:nPaperSize	:= 8 		// Impress�o em papel A3
olReport:lHeaderVisible 	:= .F. 		// N�o imprime cabe�alho do protheus
olReport:lFooterVisible 	:= .F.		// N�o imprime rodap� do protheus
olReport:lParamPage			:= .F.		// N�o imprime pagina de parametros
olReport:SetEdit(.F.)                   // N�o permite personilizar o relat�rio, desabilitando o bot�o <Personalizar>

//+----------------+
//|Define as fontes|
//+----------------+

Return olReport

/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ATFProc   � Autor � Totvs                 � Data | 14/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Impress�o do relatorio.								      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ATFProc( ExpC1 )         				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpC1 = Objeto tReport                                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ATFProc( olReport )

Local nReg			:= 0 //Quantidade de registros impressos
Local nPag			:= 0 //Quantidade de paginas por pagina
Local nCol			:= 0
Local aColGrp1      := { STR0005,STR0008,""		,STR0012,STR0015,STR0018,STR0021,STR0023,""		,STR0026,STR0029,""		,STR0032,""			,STR0036,STR0039,STR0042,STR0045,STR0048,STR0051,STR0053,STR0056,STR0059,STR0062	,STR0064,STR0067,""		,STR0071,""			,STR0075,STR0078,STR0081 }
Local aColGrp12     := { STR0006,STR0009,STR0011,STR0013,STR0016,STR0019,STR0022,STR0024,STR0025,STR0027,STR0030,STR0031,STR0033,STR0035	,STR0037,STR0040,STR0043,STR0046,STR0049,STR0052,STR0054,STR0057,STR0060,STR0063	,STR0065,STR0068,STR0070,STR0072,STR0074	,STR0076,STR0079,STR0082 }
Local aColGrp13     := { STR0007,STR0010,""		,STR0014,STR0017,STR0020,""		,""		,""	,STR0028,""			,""	,STR0034,""			,STR0038,STR0041,STR0044,STR0047,STR0050,""		,STR0055,STR0058,STR0061,STR0090	,STR0066,STR0069,""		,STR0073,""			,STR0077,STR0080,STR0083 }
Local aInfo         := {}           
Local nInc			:= 0 
Local nPosEquiv		:= 0
Local nValor		:= 0
Local cStrFil		:= ""
Local oFont7 		:= TFont():New( "Courier New",, -06 )
Local oFont8 		:= TFont():New( "Courier New",, -08 )
Local aVert			:= { 100, 230, 350, PIX_INICIAL_VALORES }
Local nIniDin		:= PIX_INICIAL_VALORES					// Pixel redimencionado dinamicamente
Local nLimitrofe	:= 4850
Local aSelFil		:= {}
Local nRowStart		:= 0 
Local nPosIni       := 0
Local nPosFim       := 0
Local nCount        := 0
Local cDescActivo   := ""
Local cBaja 		:= ""
Private aTotais		:= {}    
Private aEquivale 	:= { "N1_CBASE","N3_CCONTAB","N1_DESCRIC","N1_MARCA","N1_MODELO","N1_CHAPA","NSLDINIC","NAQUIS","NAMPLIA","NBAIXAS","NAJUSTES","NVOLUN","NSOCIE","NOUTROS","NHIST","NAJUSINFLA","NAJUSTADO","N1_AQUISIC","N3_DINDEPR","CMETODO","N3_AUTDEPR","N3_TXDEPR1","NDEPRACM","NDEPRACM2","NDEPRBXS","NAJUSTES2","NVOLUL2","NSOCIE2","NOUTROS2","NDEPRHIST","NAJUDINFLA","NDEPRHIST"}
Private aMetodo     := {"",""}

// Se aFil nao foi enviada, exibe tela para selecao das filiais
If MV_PAR03 == 1
	aSelFil := AdmGetFil()

	If Len( aSelFil ) <= 0
		Return
	EndIf 
EndIf

aFill( aTotais, 0 )

If MV_PAR03 == 1
	For nInc := 1 To Len( aSelFil )
		cStrFil += "'" + aSelFil[nInc] + "'"
		If nInc < Len( aSelFil )
			cStrFil += ", "
		EndIf
	Next
Else
	cStrFil :=  xFilial('CN1')
EndIf
cStrFil	 := "%" + cStrFil+ "%"
If MV_PAR02 == 1   
	cBaja := "%" + Chr(39) + " " + Chr(39) + "," + Chr(39) + "0" + Chr(39) + "," + Chr(39) + "1" + Chr(39) + "%"
Else
	cBaja :=  "%" + Chr(39) + " " + Chr(39) + "," + Chr(39) + "0" + Chr(39) + "%"
Endif

BeginSql Alias "PER"

	SELECT DISTINCT N1_CBASE,N1_ITEM,N1_MARCA,N1_MODELO,N1_DESCRIC,N1_CHAPA,N1_AQUISIC, N3_VORIG1,N3_CCONTAB,
					N3_QUANTD,N3_DINDEPR,N3_TPDEPR,N3_AUTDEPR,N3_TXDEPR1,N3_NODIA,N1_PRODUTO, N1_AQUISIC,N3_BAIXA,N1_BAIXA 
					
	FROM  %table:SN1% SN1,%table:SN3% SN3

	WHERE SN1.N1_CBASE = SN3.N3_CBASE AND
		  SN3.N3_TIPO = '01' AND 
		  SN1.N1_FILIAL IN ( %Exp:cStrFil%  ) AND 
		  SN3.N3_BAIXA IN (%Exp:cBaja% ) AND   
		  SN3.%NotDel% AND SN1.%NotDel%
	ORDER BY N1_CBASE, N1_ITEM 
EndSql

TCSetField( "PER", "N1_AQUISIC",	"D", 08, 0 )
TCSetField( "PER", "N3_DINDEPR",	"D", 08, 0 )
TCSetField( "PER", "N3_VORIG1",		"N", TamSX3( "N3_VORIG1" )[1], TamSX3( "N3_VORIG1" )[2] )
TCSetField( "PER", "N3_VRDACM1",	"N", TamSX3( "N3_VRDACM1" )[1], TamSX3( "N3_VRDACM1" )[2] )
DbSelectArea( "PER" )                       
PER->( DbGoTop() )
If PER->( !Eof() )
	FCabR032( olReport, nCol, aColGrp1, aColGrp12, aColGrp13 ) //Impress�o do cabe�alho
	aTotais	:= FR032Array( aEquivale )
EndIf

nRowStart		:= olReport:Row()

PER->(dbGoTop())
olReport:SetMeter( RecCount() )

While PER->(!Eof())
	If olReport:Cancel()
		Exit
	EndIf         

	FR032PgMtd(PER->N3_TPDEPR)
		
	aVert := { 120 }                      
	nPosEquiv := 120	//PIX_EQUIVALENTE
	aInfo := FRInfoATF( PER->N1_CBASE, PER->N1_ITEM, MV_PAR01 )
	For nInc := 1 To Len( aEquivale )
                             
		If PER->( FieldPos( aEquivale[nInc] ) ) > 0
			If ValType( PER->&( aEquivale[nInc] ) ) == "C"

				If Upper( aEquivale[nInc] ) == "N1_DESCRIC"
					//For Next para tratamento de quebra de linha para impress�o da descri��o do Bem.
					//quando o mesmo ultrapassar 12 caracteres.					
					lExit := .F.
					For nCount := 1 To MlCount(RTrim(PER->N1_DESCRIC),13)           
						nPosIni := If(nCount==1,1,If(nCount==2,14,If(nCount==3,27,40)))
						nPosFim := If(nCount==1,13,nCount*13) 
                		cDescActivo := RTrim(Subs(PER->N1_DESCRIC,nPosIni,nPosFim))
  						If Empty(cDescActivo) 
					 		Exit
						Endif
 			 			If nCount > 1
							FR032PrnA( olReport,.F.)
							olReport:SkipLine( 1 )
						EndIf                
						olReport:Say( olReport:Row(), nPosEquiv, cDescActivo, oFont7 )	 
						If nCount == 2
							olReport:Say( olReport:Row(), 2932, aMetodo[nCount], oFont7)
						EndIf						
						If nCount == 1
							FR032PrtCol(olReport,aInfo,aEquivale,aTotais,nPosEquiv,aVert)
							lExit := .T.
						EndIf	
                    Next nCount
                    If lExit
                    	Exit
                    EndIf	
                Else    
					olReport:Say( olReport:Row(), nPosEquiv, PER->&( aEquivale[nInc] ), oFont7 )
                EndIf
			ElseIf ValType( PER->&( aEquivale[nInc] ) ) == "D"
				olReport:Say( olReport:Row(), nPosEquiv, DtoC( PER->&( aEquivale[nInc] ) ),  oFont7 )

			ElseIf ValType( PER->&( aEquivale[nInc] ) ) == "N"
				olReport:Say( olReport:Row(), nPosEquiv, Transform( PER->&( aEquivale[nInc] ), MASK_VALOR), oFont7 )

				// Ajusta os totalizadores
				aTotais[nInc] += PER->&( aEquivale[nInc] )
			EndIf

		Else
			If !Empty( aInfo )
				If Upper( aEquivale[nInc] ) == "NAMPLIA"
					nValor := aInfo[1]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NBAIXAS"
					nValor := aInfo[2]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM"
					nValor := aInfo[3]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM2"
					nValor := aInfo[4]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NDEPRBXS"
					nValor := aInfo[5]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NDEPRHIST"
					nValor := aInfo[3] + aInfo[4] + aInfo[5] + aInfo[13] + aInfo[14] + aInfo[15]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NSLDINIC"
					// Se o bem foi adquirido em exerc�cios anteriores, obter o valor do registro tipo 01. 
					// Caso contr�rio, preencher com 0 (zero).
					If Year( PER->N1_AQUISIC ) < MV_PAR01
						nValor := PER->N3_VORIG1
					EndIf
	
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor
	
				ElseIf Upper( aEquivale[nInc] ) == "NAQUIS"
					// Se o bem foi adquirido no exerc�cio do relat�rio, obter do registro tipo 01. 
					// Caso contr�rio, preencher com 0 (zero).
					If Year( PER->N1_AQUISIC ) == MV_PAR01
						nValor := PER->N3_VORIG1
					EndIf
	
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NVOLUN"
					nValor := aInfo[9]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NSOCIE"
					nValor := aInfo[10]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NOUTROS"
					nValor := aInfo[12]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NVOLUL2"
					nValor := aInfo[13]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor
                                           
				ElseIf Upper( aEquivale[nInc] ) == "NSOCIE2"
					nValor := aInfo[14]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NOUTROS2"
					nValor := aInfo[15]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NHIST"
					If MV_PAR02==1 .and. !Empty(PER->N3_BAIXA) .AND. !Empty(PER->N1_BAIXA)
						nValor := PER->N3_VORIG1 + aInfo[9] + aInfo[10] + aInfo[12]
					Else
						nValor := PER->N3_VORIG1 - aInfo[2] + aInfo[9] + aInfo[10] + aInfo[12]
					Endif
					aInfo[16] := nValor
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "CMETODO"
					If PER->N3_TPDEPR <> "1"
						olReport:Say( olReport:Row(), nPosEquiv, Subs(GetTPDesc( "20", PER->N3_TPDEPR ),1,13), oFont7 )
	               	EndIf   	     

				ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES"
					nValor := aInfo[11]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NAJUSINFLA"
					nValor := aInfo[16]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NAJUSTADO"
					nValor := aInfo[15] + aInfo[16]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aInfo[17] := nValor
					aTotais[nInc] += nValor
					
				ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES2"
					nValor := aInfo[20]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor

				ElseIf Upper( aEquivale[nInc] ) == "NAJUDINFLA"
					nValor := aInfo[21]
					olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
					aTotais[nInc] += nValor
				EndIf 
			EndIf
		EndIf
		nPosEquiv += PIX_DIF_COLUNA_VALORES
		aAdd( aVert, nPosEquiv )                     
	Next

	nLimitrofe := nIniDin
	FR032Traco( olReport, .F. )
	FR032PrnA( olReport,.F.)
	olReport:SkipLine( 1 )

	olReport:OnPageBreak( { || FCabR032( olReport, nCol, aColGrp1, aColGrp12, aColGrp13 ) } )
	If nPag > 80
		If nReg > 0
			FTotR032( olReport, nCol, aTotais )
		EndIf

		olReport:EndPage()
		nPag := 0
		olReport:setRow( nRowStart )
	EndIf

	DbSelectArea("PER")
	PER->( DbSkip() )
	
	olReport:IncMeter()

	nPag++				
	nReg++				
End

If MV_PAR04 == 1 
	IF MSGYESNO(STR0093) // "�Confirma la generaci�n del archivo TXT?"
	   Processa({|| GerArq(AllTrim(MV_PAR05))},,STR0094) // "Generando archivo TXT"
	Endif   
EndIF


If nReg > 0
	FTotR032( olReport, nCol, aTotais )
EndIf

PER->( DbCLoseArea() )

Return olReport

/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FCabR032  � Autor � Totvs                 � Data | 06/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Cabe�alho do relatorio.								      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FCabR032(Expo1,ExpN1,ExpA1)  				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpO1 = Objeto tReport                                      ���
���          �ExpN1 = Posi��o da coluna de impress�o                      ���
���          �ExpA1 = Array com as contas do ativo                        ���
���          �ExpA2 = Array com as contas do passivo                      ���
���          �ExpA3 = Array com as contas de patrimonio                   ���
���          �ExpA4 = Array com as contas de gasto                        ���
���          �ExpA5 = Array com as contas de receita                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FCabR032( olReport, nCol, aColGrp1, aColGrp12, aColGrp13 )
Local nInc			:= 0
Local aVert			:= { 100, 396, 988, 1728, 2172, 2912, 3208, 3504, 3948, 4392, 4836}
Local nIniDin		:= PIX_INICIAL_VALORES						// Pixel redimencionado dinamicamente
Local oFont7b 		:= TFont():New( "Courier New",, -06,,.T. )
Local oFont8 		:= TFont():New( "Courier New",, -08 )
Local oFont8b 		:= TFont():New( "Courier New",, -08,,.T. )
Local nLimitrofe	:= 4850
Local nCharPCol		:= 15  


//Cabe�alho    
olReport:Say( olReport:Row()+35  ,110, STR0001, oFont8b )		                // "FORMATO 7.1:REGISTRO DE ACIVOS FIJOS- DETALLE DE ACTIVOS"
olReport:Say( olReport:Row()+55  ,110, STR0002, oFont8b )	   			 		// Per�odo
olReport:Say( olReport:Row()+55  ,890, ":",oFont8) 
olReport:Say( olReport:Row()+55  ,910, MV_PAR01,oFont8) 
olReport:Say( olReport:Row()+85  ,110, STR0003, oFont8b )						// RUC
olReport:Say( olReport:Row()+85  ,890, ":",oFont8)
olReport:Say( olReport:Row()+85  ,910, cRUC,oFont8) 
olReport:Say( olReport:Row()+110 ,110, STR0004, oFont8b )						// "Apellidos y nombres, denominaci�n o raz�n social "
olReport:Say( olReport:Row()+110 ,890, ":",oFont8)
olReport:Say( olReport:Row()+110 ,910, AllTrim( Upper(Capital( cApellido) )), oFont8 )
olReport:SkipLine( 04 )

// Primeira linha
FR032Traco( olReport,.T. )
olReport:SkipLine( 1 )                             


FR032Prnt( olReport,aVert, .F. )
olReport:Say( olReport:Row()+20, 396, PadC( STR0084,	4 * nCharPCol ),	oFont7b )		// "Detalles del Act. Fijo"      
olReport:Say( olReport:Row()+20,1696, PadC( STR0085,	4 * nCharPCol ),	oFont7b )		// "Val. Reevaluac. Efect."      
olReport:Say( olReport:Row()+20,2912, PadC( STR0086,	2 * nCharPCol ),	oFont7b )		// "Depreciac."
olReport:Say( olReport:Row()+20,3504, PadC( STR0087,	3 * nCharPCol ),	oFont7b )		// "Val. de la Deprec."
olReport:Say( olReport:Row()+20,3948, PadC( STR0088,	3 * nCharPCol ),	oFont7b )		// "Val. Reevaluac. Efect."
olReport:SkipLine( 1 )
nIniDin *= Len( aColGrp1 )
nLimitrofe := nIniDin
FR032Traco( olReport, .F.)
FR032Prnt( olReport,aVert, .F. )													// Imprime a linhas verticais e passa para proxima linha
olReport:SkipLine( 1 )

// Segunda linha
aVert	:= { 100 }                                  �
nIniDin	:= 100
nLinPix := 32
For nInc := 1 To Len( aColGrp1 )
	olReport:Say( olReport:Row(), nIniDin, PadC( aColGrp1[nInc], 16 ), oFont7b )
	nIniDin += PIX_DIF_COLUNA_VALORES
	aAdd( aVert, nIniDin )
Next
nIniDin += PIX_DIF_COLUNA_VALORES
aAdd( aVert, nIniDin )
      
FR032Prnt( olReport, aVert,.F. )																	// Imprime a linhas verticais e passa para proxima linha
olReport:SkipLine( 1 )


// Terceira linha
// Imprime as contas
nIniDin	:= 100
For nInc := 1 To Len( aColGrp12 )
	olReport:Say( olReport:Row(), nIniDin, PadC( aColGrp12[nInc], 16 ), oFont7b )
	nIniDin += PIX_DIF_COLUNA_VALORES
Next

FR032Prnt( olReport, aVert, .F. )														// Imprime a linhas verticais e passa para proxima linha
olReport:SkipLine( 1 )

// Quarta linha
// Imprime as contas
nIniDin	:= 100
For nInc := 1 To Len( aColGrp13 )
	olReport:Say( olReport:Row(), nIniDin, PadC( aColGrp13[nInc], 16 ), oFont7b )
	nIniDin += PIX_DIF_COLUNA_VALORES
Next                             

FR032Traco( olReport, .F. )
FR032Prnt( olReport, aVert, .F. )														// Imprime a linhas verticais e passa para proxima linha
olReport:SkipLine( 1 )

Return

/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FTotR032  � Autor � Totvs                 � Data | 06/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Cabe�alho do relatorio.								      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FTotR032(Expo1,ExpN1,ExpA1)  				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpO1 = Objeto tReport                                      ���
���          �ExpN1 = Posi��o da coluna de impress�o                      ���
���          �ExpA1 = Array com as contas do ativo                        ���
���          �ExpA2 = Array com as contas do passivo                      ���
���          �ExpA3 = Array com as contas de patrimonio                   ���
���          �ExpA4 = Array com as contas de gasto                        ���
���          �ExpA5 = Array com as contas de receita                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FTotR032( olReport, nCol, aTotais )
Local nInc			:= 0
Local aVert			:= { 120 }
Local nIniDin		:= 120
Local oFont7b 		:= TFont():New( "Courier New",, -06,,.T. )
Local nLimitrofe	:= 4850

For nInc := 1 To Len( aTotais )
	If nInc < 6
		aTotais[nInc]:=""
	EndIf	
	If nInc == 6
		olReport:Say( olReport:Row()+10, aVert[nInc], "Totales", 			oFont7b )		// Totais
    Else
		If ValType( aTotais[nInc] ) == "N"
			olReport:Say( olReport:Row()+10, aVert[nInc], Transform(aTotais[nInc], MASK_VALOR ), oFont7b )
		EndIf
	EndIf	
	nIniDin += PIX_DIF_COLUNA_VALORES
	aAdd( aVert, nIniDin )
Next

nLimitrofe := nIniDin
//olReport:Box(olReport:Row()-004,olReport:Col()-004, olReport:Row()+031, nLimitrofe )

aVert   := {100}
nIniDin := 100
For nInc := 1 To 33
	nIniDin += PIX_DIF_COLUNA_VALORES
	aAdd( aVert, nIniDin)
Next nInc
	
FR032Prnt( olReport, aVert, .T. )	// Imprime a linhas verticais e passa para proxima linha
olReport:SkipLine( 1 )

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FR032Traco � Autor � Jose Lucas           � Data | 24/06/11 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime a linha vertical do relatorio.                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FR032Traco( ExpO1, ExpA1 )   				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpO1 = Objeto Report.                                      ���
���          �ExpA1 = Array com as colunas                                ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FR032Traco( olReport, lAvanca, cId )
Local nInc 		:= 1
Local nIniDin   := 100
Local nLinPix	:= 34
Local oFont8	:= TFont():New( "Courier New",, -08,,.F. ) 
Local aTraco    := { 100, PIX_INICIAL_VALORES } 
Local nIniCol   := 005
                   
For nInc := 1 To nLinPix
	If nInc < 34      
		nIniCol := If(nInc==1,010,nIniCol)
   	 	If lAvanca
			olReport:Say( olReport:Row()+015, aTraco[nInc]+nIniCol,If(nInc==33,"___________","______________"), oFont8 )     	   		//Tra�o
		Else
			olReport:Say( olReport:Row()    , aTraco[nInc]+nIniCol,If(nInc==33,"___________","______________"), oFont8 )     	   		//Tra�o
		EndIf
	EndIf			
	nIniDin += PIX_DIF_COLUNA_VALORES
	aAdd( aTraco, nIniDin )
Next nInc                                   

Return                   

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FR032Prnt  � Autor � Jose Lucas           � Data | 24/06/11 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime as linhas verticais e horizontais do relatorio.     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FR032Prnt( ExpO1, ExpA1 )   				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 = Array com as colunas                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/      
Static Function FR032Prnt( olReport, aCol, lJuncao, cId )
Local nInc 		:= 1
Local nIniDim   := 0
Local oFont8	:= TFont():New( "Courier New",, -08,,.F. )      
Local nIniCol   := 005

DEFAULT cId := ""

If Len(aCol) >= 33
	nIniDim := aCol[Len(aCol)] += PIX_DIF_COLUNA_VALORES  
	aAdd(aCol,nIniDim)
EndIf	
             
For nInc := 1 To Len(aCol)
	olReport:Say( olReport:Row(), aCol[nInc]+005, "|", oFont8 )
	olReport:Say( olReport:Row()+15, aCol[nInc]+005, "|", oFont8 )
	If lJuncao
		nIniCol := If(nInc==1,010,nIniCol)
		If nInc < 33                                                    
		   	If nInc <> 33
		   		olReport:Say( olReport:Row()+015, aCol[nInc]+nIniCol,If(nInc<32     ,"______________","___________"), oFont8 )     		//Tra�o
				olReport:Say( olReport:Row(), aCol[nInc]+005, "|", oFont8 )
			EndIf	
		EndIf	
	EndIf      
Next                                   

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FR032PrnA � Autor � Jose Lucas           � Data | 24/06/11 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Imprime as linhas verticais e horizontais do relatorio.     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FR032PrnA( ExpO1, ExpA1 )   				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 = Array com as colunas                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/    
Static Function FR032PrnA( olReport, lJuncao, cId )
Local nInc 		:= 1
Local oFont8	:= TFont():New( "Courier New",, -08,,.F. ) 
Local nLen      := 34
Local aCol      := {100}
Local nIniDin   := 100
             
For nInc := 1 To nLen
	olReport:Say( olReport:Row(), aCol[nInc]+005, "|", oFont8 )
	olReport:Say( olReport:Row()+15, aCol[nInc]+005, "|", oFont8 )
	If lJuncao
		If nInc < 34
			olReport:Say( olReport:Row()+015, aCol[nInc]+005,If(nInc==33,"___________","______________"), oFont8 )     		//Tra�o
			olReport:Say( olReport:Row(), aCol[nInc]+005, "|", oFont8 )
		EndIf	
	EndIf     
	nIniDin += PIX_DIF_COLUNA_VALORES          
	aAdd(aCol,nIniDin)
Next                                   

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FR032Array� Autor � Totvs                 � Data | 07/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Verifica quais colunas tem totalizadores e retorna array    ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FR032Array( ExpA1 )         				                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpA1 = Array com os campos                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FR032Array( aEquivale )
Local aRet	:= {}
Local nInc	:= 0

If Select( "PER" ) > 0
	For nInc := 1 To Len( aEquivale )
		If PER->( FieldPos( aEquivale[nInc] ) ) > 0
			If ValType( PER->&( aEquivale[nInc] ) ) == "N"
				aAdd( aRet, 0 )
			Else
				aAdd( aRet, NIL )
			EndIf
		Else
			If Upper( Left( aEquivale[nInc], 1 ) ) == "N"
				aAdd( aRet, 0 )
			Else
				aAdd( aRet, NIL )
			EndIf
		EndIf
	Next
EndIf

Return aRet

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FRInfoATF � Autor � Totvs                 � Data | 20/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna um array com historico/valores do ativo             ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �FRInfoATF( cBase, cItem, cExercicio, lConsBaixados )	      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FRInfoATF( cBase, cItem, nExercicio, lConsBaixados )
Local aRet		 := {}
Local aAreaSN3	 := SN3->( GetArea() )
Local aAreaSN4	 := SN4->( GetArea() )
Local nAmplia	 := 0
Local nAmplia2	 := 0
Local nReaval	 := 0
Local nReaval2	 := 0
Local nBaixas	 := 0
Local nDeprAcm	 := 0
Local nDeprAcm2	 := 0
Local nDeprBxs	 := 0
Local nVolul	 := 0
Local nSocie	 := 0
Local nOutros	 := 0
Local nVolul2	 := 0
Local nSocie2	 := 0
Local nOutros2	 := 0
Local nAjustes   := 0
Local nAjustado  := 0
Local nAjustes2  := 0
Local nAjusInfla := 0
Local nAjuDInfla := 0                   
Local nHistorico := 0

DEFAULT lConsBaixados := .F.

DbSelectArea( "SN4" )
SN4->( DbSetOrder(1) )
SN4->( MsSeek( xFilial( "SN4" ) + cBase + cItem ) )
While SN4->( !Eof() ) .AND. xFilial( "SN4" ) + cBase + cItem == SN4->( N4_FILIAL + N4_CBASE + N4_ITEM )

	If SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "09" .AND. SN4->N4_TIPOCNT == "1"		// Melhorias
		nAmplia += SN4->N4_VLROC1

	ElseIf SN4->N4_TIPO == "02" .AND. SN4->N4_OCORR == "05" .AND. SN4->N4_TIPOCNT == "1"	// Reavaliacoes
		nReaval += SN4->N4_VLROC1

		// Obs.: considerar somente os registros da SN4 cujo seu correspondente na SN3 tenha N3_TIPREAV = 1 
		// (para localizar o registro na SN3 use os campos N4_SEQREAV e N3_SEQREAV.
		If SN3->( FieldPos( "N3_TIPREAV" ) ) > 0
			DbSelectArea( "SN3" )
			SN3->( DbSetOrder( 1 ) )
			If SN3->(MsSeek(xFilial("SN3")+SN4->N4_CBASE+SN4->N4_ITEM+SN4->N4_TIPO+"0"+SN4->N4_SEQ))
				If 	   RTrim(SN3->N3_TIPREAV) == "1" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
					nVolul += SN4->N4_VLROC1    
				ElseIf RTrim(SN3->N3_TIPREAV) == "2" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
					nSocie += SN4->N4_VLROC1 
				ElseIf RTrim(SN3->N3_TIPREAV) == "3" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
					nOutros += SN4->N4_VLROC1
				EndIf
			EndIf
		EndIf
		
	ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "1"	// Baixas
		nBaixas += SN4->N4_VLROC1

	ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "4"	// Deprecia��o sobre as Baixas
		nDeprBxs += SN4->N4_VLROC1 

	ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "3"	// Outras depreciacoes                            
		nAjustes += SN4->N4_VLROC1

	ElseIf SN4->N4_TIPO == "05" .AND. SN4->N4_TIPOCNT == "1"								// Ampliacao
		nAmplia2 += SN4->N4_VLROC1

	ElseIf SN4->N4_TIPO == "05" .AND. SN4->N4_TIPOCNT == "3"								// Reavaliacoes
		nReaval2 += SN4->N4_VLROC1                     

	ElseIf SN4->N4_OCORR == "06" .AND. SN4->N4_TIPOCNT == "4"								// Depreciacao acumulado no exercicio anterior
	    // Valor da deprecia��o at� o final do exerc�cio anterior. 
		If Year( SN4->N4_DATA ) < nExercicio
			nDeprAcm += SN4->N4_VLROC1
		EndIf                               
				
	ElseIf SN4->N4_OCORR == "06" .AND. SN4->N4_TIPOCNT == "3"								
		// Total da deprecia��o calculada no exerc�cio. 
		If Year( SN4->N4_DATA ) == nExercicio
			If SN4->N4_TIPO == "02"			//reavaliacoes
				If SN3->( FieldPos( "N3_TIPREAV" ) ) > 0      	
					DbSelectArea( "SN3" )
					SN3->( DbSetOrder( 1 ) )
					If SN3->(MsSeek(xFilial("SN3")+SN4->N4_CBASE+SN4->N4_ITEM+SN4->N4_TIPO+"0"+SN4->N4_SEQ))
						If RTrim(SN3->N3_TIPREAV) == "1" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
							nVolul2 += SN4->N4_VLROC1
						ElseIf RTrim(SN3->N3_TIPREAV) == "2" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
							nSocie2 += SN4->N4_VLROC1
						ElseIf RTrim(SN3->N3_TIPREAV) == "3" .AND. SN3->N3_SEQREAV == SN4->N4_SEQREAV 
							nOutros2 += SN4->N4_VLROC1
						EndIf
					EndIf
				EndIf
			Else
				nDeprAcm2 += SN4->N4_VLROC1
			Endif
		EndIf
		DbSelectArea( "SN3" )
		SN3->( DbSetOrder( 1 ) )
		If SN3->(MsSeek(xFilial("SN3")+SN4->N4_CBASE+SN4->N4_ITEM+SN4->N4_TIPO+"1"+SN4->N4_SEQ))
			nDeprAcm2 := 0
		EndIf	
	EndIf

	SN4->( DbSkip() )
End

aAdd( aRet, nAmplia )					// 01- Melhorias = ampliacao
aAdd( aRet, nBaixas + nAmplia2)			// 02- Baixas + Ampliacao
aAdd( aRet, nDeprAcm )					// 03- Depreciacao Acumulada no exercicio anterior
aAdd( aRet, nDeprAcm2 )					// 04- Depreciacao Acumulada no exercicio atual
aAdd( aRet, nDeprBxs + nReaval2 )		// 05- Baixas no exercicio atual + reavaliacoes
aAdd( aRet, nReaval )					// 06- Reavaliacoes
aAdd( aRet, nAmplia )					// 07- Ampliacoes
aAdd( aRet, nBaixas+nAmplia2 )			// 08- Baixas e Ampliacoes
aAdd( aRet, nVolul )					// 09- Reavaliacao voluntaria
aAdd( aRet, nSocie )					// 10- Reavaliacao por reorganizacao de sociedade
aAdd( aRet, nAjustes )					// 11- Outros ajustes
aAdd( aRet, nOutros )					// 12- Reavaliacao outros
aAdd( aRet, nVolul2 )					// 13- Reavaliacao voluntaria - OCORRENCIA 6
aAdd( aRet, nSocie2 )					// 14- Reavaliacao por reorganizacao de sociedade - OCORRENCIA 6
aAdd( aRet, nOutros2 )			   		// 15- Reavaliacao outros - OCORRENCIA 6
aAdd( aRet, nHistorico )                // 16- Valor historico do ativo fixo em 31/12
aAdd( aRet, nAjusInfla ) 				// 17- Ajustes por Infla��o.
aAdd( aRet, nAjustado )                 // 18- Valor ajustado do ativo fixo em 31/12
aAdd( aRet, "CMETODO" )					// 19- Metodo Aplicado.
aAdd( aRet, nAjustes2 )					// 20- Deprecia��o relacionada outros Ajustes - OCORRENCIA 6
aAdd( aRet, nAjuDInfla )				// 21- Ajustes por Deprecia��o de infla��o

RestArea( aAreaSN3 )
RestArea( aAreaSN4 )

Return aRet

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �getTPDesc � Autor � Totvs                 � Data | 20/05/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Retorna a descricao do tipo de depreciacao.                 ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �getTPDesc( SN3->N3_TPDEPR )                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function getTPDesc( cChave, cTpDepr )
Local cDesc := ""

DbSelectArea( "SN0" )
SN0->( DbSetOrder( 1 ) )
If SN0->( MsSeek( xFilial( "SN0" ) + cChave + cTpDepr ) )
	cDesc := AllTrim(SN0->N0_DESC01)
EndIf

Return cDesc

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � FR032PrtCol � Autor � Jose Lucas         � Data | 28/06/11 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprimir demais colunas quando N1_CBASE impresso em 2 ou + ���
���          � linhas.                                                    ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FR032PrtCol(olReport,aInfo,aEquivale,aTotais,nPosEquiv,aVert)
Local nValor := 0.00
Local nInc   := 1
Local oFont7 		:= TFont():New( "Courier New",, -06 )

nPosEquiv += PIX_DIF_COLUNA_VALORES

For nInc := 4 To Len(aEquivale)							
	If !Empty( aInfo )
	If Upper( aEquivale[nInc] ) == "N1_MARCA"
		olReport:Say( olReport:Row(), nPosEquiv, PER->&( aEquivale[nInc] ),  oFont7 )
			
	ElseIf Upper( aEquivale[nInc] ) == "N1_MODELO"
		olReport:Say( olReport:Row(), nPosEquiv, PER->&( aEquivale[nInc] ),  oFont7 )
			
	ElseIf Upper( aEquivale[nInc] ) == "N1_CHAPA"
		olReport:Say( olReport:Row(), nPosEquiv, PER->&( aEquivale[nInc] ),  oFont7 )
		
	ElseIf Upper( aEquivale[nInc] ) == "NAMPLIA"
		nValor := aInfo[1]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NBAIXAS"
		nValor := aInfo[2]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM"
		nValor := aInfo[3]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM2"
		nValor := aInfo[4]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NDEPRBXS"
		nValor := aInfo[5]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NDEPRHIST"
		nValor := aInfo[3] + aInfo[4] + aInfo[5] + aInfo[13] + aInfo[14] + aInfo[15]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NSLDINIC"
		// Se o bem foi adquirido em exerc�cios anteriores, obter o valor do registro tipo 01. 
		// Caso contr�rio, preencher com 0 (zero).
		If Year( PER->N1_AQUISIC ) < VAL(MV_PAR01)
			nValor := PER->N3_VORIG1
		EndIf
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NAQUIS"
		// Se o bem foi adquirido no exerc�cio do relat�rio, obter do registro tipo 01. 
		// Caso contr�rio, preencher com 0 (zero).
		If Year( PER->N1_AQUISIC ) == VAL(MV_PAR01)
			nValor := PER->N3_VORIG1
		EndIf
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NVOLUN"
		nValor := aInfo[9]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NSOCIE"
		nValor := aInfo[10]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NOUTROS"
		nValor := aInfo[12]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NVOLUL2"
		nValor := aInfo[13]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor                                         
	ElseIf Upper( aEquivale[nInc] ) == "NSOCIE2"
		nValor := aInfo[14]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NOUTROS2"
		nValor := aInfo[15]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NHIST"
		If MV_PAR02==1 .and. !Empty(PER->N3_BAIXA) .AND. !Empty(PER->N1_BAIXA)
			nValor := PER->N3_VORIG1 + aInfo[9] + aInfo[10] + aInfo[12]
		Else
			nValor := PER->N3_VORIG1 - aInfo[2] + aInfo[9] + aInfo[10] + aInfo[12]
		Endif
		aInfo[16] := nValor
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "N1_AQUISIC"
		olReport:Say( olReport:Row(), nPosEquiv+20, DtoC( PER->&( aEquivale[nInc] ) ),  oFont7 )
		aTotais[nInc] := ""
	ElseIf Upper( aEquivale[nInc] ) == "N3_DINDEPR"
		olReport:Say( olReport:Row(), nPosEquiv+20, DtoC( PER->&( aEquivale[nInc] ) ),  oFont7 )
		aTotais[nInc] := ""
	ElseIf Upper( aEquivale[nInc] ) == "CMETODO"
		If PER->N3_TPDEPR <> "1"
			olReport:Say( olReport:Row(), nPosEquiv, Subs(GetTPDesc( "20", PER->N3_TPDEPR ),1,13), oFont7 )
       	EndIf   	     
	ElseIf Upper( aEquivale[nInc] ) == "N3_TXDEPR1"
		olReport:Say( olReport:Row(), nPosEquiv, Transform( PER->&( aEquivale[nInc] ), MASK_VALOR), oFont7 )
		aTotais[nInc] := ""
	ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES"
		nValor := aInfo[11]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NAJUSINFLA"
		nValor := aInfo[17]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NAJUSTADO"
		nValor := aInfo[16] + aInfo[17]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aInfo[17] := nValor
		aTotais[nInc] += nValor		
	ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES2"
		nValor := aInfo[20]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	ElseIf Upper( aEquivale[nInc] ) == "NAJUDINFLA"
		nValor := aInfo[21]
		olReport:Say( olReport:Row(), nPosEquiv, Transform( nValor, MASK_VALOR), oFont7 )
		aTotais[nInc] += nValor
	EndIf 
	EndIf
	nPosEquiv += PIX_DIF_COLUNA_VALORES
	aAdd( aVert, nPosEquiv )
Next nInc	
Return( aTotais )    

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � FR032PgMtd  � Autor � Jose Lucas         � Data | 28/06/11 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Quebrar descricao do metodo de calculo em 2 elementos para ���
���          � ser impresso em 2 linhas.                                  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FR032PgMtd()
Local nCount  := 0
Local nPosIni := 0
Local nPosFim := 0
Local cDscMetodo := ""       

If ! PER->N3_TPDEPR $ " |1"
	aMetodo := {}
	cMetodo := GetTPDesc( "20", PER->N3_TPDEPR )
	For nCount := 1 To MlCount(RTrim(cMetodo),13)           
		nPosIni := If(nCount==1,1,If(nCount==2,14,If(nCount==3,27,40)))
		nPosFim := If(nCount==1,13,nCount*13) 
   		cDscMetodo := RTrim(Subs(cMetodo,nPosIni,nPosFim))
		If Empty(cDscMetodo) 
		   Exit
		Endif
		AADD(aMetodo,cDscMetodo)
    Next nCount
EndIf 	     
Return


/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
��� Funcao     � GerArq                                 � Data � 19.03.2016 ���
���������������������������������������������������������������������������Ĵ��
��� Descricao  � Gera o arquivo magn�tico                                   ���
���������������������������������������������������������������������������Ĵ��
��� Parametros � cDir - Diretorio de criacao do arquivo.                    ���
���            � cArq - Nome do arquivo com extensao do arquivo.            ���
���������������������������������������������������������������������������Ĵ��
��� Retorno    � Nulo                                                       ���
���������������������������������������������������������������������������Ĵ��
��� Uso        � 7.1 - Estructura del Registro de Activos Fijos             ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Static Function GerArq(cDir)

Local nHdl       := 0
Local cLin       := ""
Local cSep       := "|"
Local cArq       := ""
Local nCont      := 0
Local nExercicio := AllTrim(MV_PAR01)
Local nContador	 := 0 
Local cCampo3 	 := ""
Local lUsaCbar 	 := GetMV( "MV_USACBAR") 
Local nInc 		 := 0
Local cDepraCM	 := ""
Local cDepraCM2	 := ""
Local cDeprbxs	 := ""
Local cAjustes2	 := ""
Local cVolul2	 := ""
Local cSocie2	 := ""
Local cOutros2	 := ""
Local cAjudinlfa := ""
Local nPLE		 := GetMv("MV_PLEPERU")
Local cCodBase 	 := ""
Local cCodBar	 := ""

cArq += "LE"                            // Fixo  'LE'
cArq +=  cRUC          					// Ruc
cArq +=  AllTrim(MV_PAR01)     		    // Ano
cArq +=  "00"                           // Mes Fixo '00'
cArq +=  "00"                           // Fixo '00'
cArq += "070100"                        // Fixo '070100'
cArq += "00"                            // Fixo '00'
cArq += "1"
cArq += "1"
cArq += "1"
cArq += "1"
cArq += ".TXT" // Extensao


FOR nCont:=LEN(ALLTRIM(cDir)) TO 1 STEP -1
   IF SUBSTR(cDir,nCont,1)=='\' 
      cDir:=Substr(cDir,1,nCont)
      EXIT
   ENDIF   
NEXT 

nHdl := fCreate(cDir+cArq)
If nHdl <= 0
	ApMsgStop(STR0095) // "Error al crear el archivo TXT"
Else
	
	dbSelectArea("PER")
	PER->(dbGoTop())
	lRet1 := .T.
	Do While PER->(!EOF())	
	
		If Val(MV_PAR01) < 2010
			Alert(STR0091) // "Para impresi�n del TXT, el per�odo debe ser igual o superior a 2010"
			fClose(nHdl)
			return nil
		EndIf
	
		If Year( PER->N1_AQUISIC ) > Val(MV_PAR01)
			PER->(dbSkip())
			Loop
		EndIf
		
		cLin  := ""
		cProd := PER->N1_CBASE		    		
						
		nAmplia	   := 0
		nAmplia2   := 0
		nReaval	   := 0
		nReaval2   := 0
		nBaixas	   := 0
		nDeprAcm   := 0
		nDeprAcm2  := 0
		nDeprBxs   := 0
		nAjustes   := 0
		nAjustado  := 0
		nAjustes2  := 0
		nAjusInfla := 0
		nAjuDInfla := 0                   
		nHistorico := 0
		nExercicio := Val(MV_PAR01)
		nRevTrib := 0
				
		DbSelectArea( "SN4" )
		SN4->( DbSetOrder(1) )
		SN4->( MsSeek( xFilial( "SN4" ) + PER->N1_CBASE + PER->N1_ITEM ) )
		While SN4->( !Eof() ) .AND. xFilial( "SN4" ) + PER->N1_CBASE + PER->N1_ITEM == SN4->( N4_FILIAL + N4_CBASE + N4_ITEM )	
		
			If SN4->N4_OCORR == "01" .And. SN4->N4_MOTIVO == "01" .And. SN4->N4_TIPOCNT == "1"	     // Reavalia��o tribut�ria por Venda
				nRevTrib += SN4->N4_VLROC1
			EndIf
				
			If SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "09" .AND. SN4->N4_TIPOCNT == "1"		// Melhorias
				nAmplia += SN4->N4_VLROC1
		
			ElseIf SN4->N4_TIPO == "02" .AND. SN4->N4_OCORR == "05" .AND. SN4->N4_TIPOCNT == "1"	// Reavaliacoes
				nReaval += SN4->N4_VLROC1
		
			ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "1"	// Baixas
				nBaixas += SN4->N4_VLROC1
		 
			ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "4"	// Deprecia��o sobre as Baixas
				nDeprBxs += SN4->N4_VLROC1 
		
			ElseIf SN4->N4_TIPO == "01" .AND. SN4->N4_OCORR == "01" .AND. SN4->N4_TIPOCNT == "3"	// Outras depreciacoes                            
				nAjustes += SN4->N4_VLROC1
			
			ElseIf SN4->N4_TIPO == "05" .AND. SN4->N4_TIPOCNT == "1"								// Ampliacao
				nAmplia2 += SN4->N4_VLROC1
		
			ElseIf SN4->N4_TIPO == "05" .AND. SN4->N4_TIPOCNT == "3"								// Reavaliacoes
				nReaval2 += SN4->N4_VLROC1
		
			ElseIf SN4->N4_OCORR == "06" .AND. SN4->N4_TIPOCNT == "4"								// Depreciacao acumulado no exercicio anterior
			    // Valor da deprecia��o at� o final do exerc�cio anterior. 
				If Year( SN4->N4_DATA ) < nExercicio
					nDeprAcm += SN4->N4_VLROC1
				EndIf
				
			ElseIf SN4->N4_OCORR == "06" .AND. SN4->N4_TIPOCNT == "3"								// Depreciacao acumulado no exercicio anterior
				// Total da deprecia��o calculada no exerc�cio. 
				If Year( SN4->N4_DATA ) == nExercicio
					nDeprAcm2 += SN4->N4_VLROC1
				EndIf   
				DbSelectArea( "SN3" )
				SN3->( DbSetOrder( 1 ) )
				If SN3->(MsSeek(xFilial("SN3")+SN4->N4_CBASE+SN4->N4_ITEM+SN4->N4_TIPO+"1"+SN4->N4_SEQ))
					nDeprAcm2 := 0
				EndIf	
			EndIf
		
			SN4->( DbSkip() )
		End
				
		nContador++	
		//01 - Periodo
		cLin += AllTrim(MV_PAR01)+"0000"	
		cLin += cSep
				
		//02 - C�digo �nico de la Operaci�n (CUO)	
		cLin += AllTrim(PER->N3_NODIA)			
        cLin += cSep
 												
        //03- N�mero correlativo  del asiento contable identificado  en el campo 2.       
        cCampo3 := Right(AllTrim(PER->N3_NODIA),9)
        cCampo3 := Strtran( PadL(cCampo3,9), Space(1), "0")
        cLin += "M" + cCampo3
        cLin += cSep
              
        //04- C�digo del cat�logo utilizado.
        DbSelectArea("SB1")
        SB1->(DbSetOrder(1))
		SB1->(MsSeek(xFilial("SB1")+PER->N1_PRODUTO))
		If lUsaCbar .And. AllTrim(SB1->B1_CODBAR) != ""
			cLin += "3"
		Else
			cLin += "9"		
		EndIf
		cLin += cSep	
				
		//05- C�digo propio del activo fijo correspondiente al cat�logo se�alado en el campo 4.
		If lUsaCbar .And. AllTrim(SB1->B1_CODBAR) != ""
			cLin += AllTrim(SB1->B1_CODBAR)
		Else
			cLin += AllTrim(PER->N1_CBASE)		
		EndIf
		cLin += cSep
				
		//06- C�digo del cat�logo utilizado.
		//07- C�digo propio de la existencia correspondiente al cat�logo se�alado en el campo 6.
		cCodBar  := StrTran(Trim(SB1->B1_CODBAR),"|","")
		cCodBar	 := StrTran(cCodBar,"/","")
		cCodBase := StrTran(Trim(PER->N1_CBASE),"|","")
		cCodBase := StrTran(cCodBase,"/","")
		
		If Trim(SB1->B1_PRODSAT)<> ""
		 	If nPLE > 5150
		 		cLin += "1"
		 		cLin += cSep
			EndIf
			cLin += Trim(SB1->B1_PRODSAT) + "00000000"
		ElseIf cCodBar <> "" .And. Trim(SB1->B1_PRODSAT) == ""
			If nPLE > 5150
				cLin += "3"
				cLin += cSep
			EndIf
			cLin += cCodBar
		Else
			If nPLE > 5150
				cLin += "2"
				cLin += cSep
			EndIf
			cLin += cCodBase + "00000000"
		Endif
		cLin += cSep
		
		//08 - C�digo de la Cuenta Contable del Activo Fijo, desagregada hasta el nivel m�ximo de d�gitos utilizado
		cLin += IIF(nRevTrib > 0, "2","1")
		cLin += cSep
				
		//09 - C�digo de la Cuenta Contable del Activo Fijo, desagregada hasta el nivel m�ximo de d�gitos utilizado 
		cLin += AllTrim(PER->N3_CCONTAB)
		cLin += cSep
		
		//10 - Estado del Activo Fijo
		cLin += IIF(PER->N3_BAIXA =="1","1","9")
		cLin += cSep
				
		//11 - Descripci�n del Activo Fijo		
		cLin += AllTrim(PER->N1_DESCRIC) 
		cLin += cSep
				
		//12 - Marca del Activo Fijo	
		cLin += If(AllTrim(PER->N1_MARCA) != "",AllTrim(PER->N1_MARCA),"-")
		cLin += cSep	
			
		//13 - Modelo del Activo Fijo	
		cLin += If(AllTrim(PER->N1_MODELO) != "",AllTrim(PER->N1_MODELO),"-")
		cLin += cSep		
		
		//14 - N�mero de serie y/o placa del Activo Fijo
		cLin += If(AllTrim(PER->N1_CHAPA) != "",AllTrim(PER->N1_CHAPA),"-")
        cLin += cSep
        		
        For nInc := 1 To Len( aEquivale )	
			If Upper( aEquivale[nInc] ) == "NSLDINIC"      //15 - Importe del saldo inicial del Activo Fijo
				cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
				cLin += cSep 
			ElseIf Upper( aEquivale[nInc] ) == "NAQUIS"    //16 - Importe de las adquisiciones o adiciones del Activo Fijo   
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
        	ElseIf Upper( aEquivale[nInc] ) == "NAMPLIA"   //17 - Importe de las mejoras del Activo Fijo   
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep     
			ElseIf Upper( aEquivale[nInc] ) == "NBAIXAS"   //18 - Importe de los retiros y/o bajas del Activo Fijo    
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
			ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES"  //19 - Importe por otros ajustes en el valor del Activo Fijo  
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
			ElseIf Upper( aEquivale[nInc] ) == "NVOLUN"    //20 - Valor de la revaluaci�n  voluntaria efectuada	   
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep  
			ElseIf Upper( aEquivale[nInc] ) == "NSOCIE"    //21 - Valor de la revaluaci�n  efectuada por reorganizaci�n de sociedades
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
        	ElseIf Upper( aEquivale[nInc] ) == "NOUTROS"   //22 - Valor de otras revaluaciones efectuada 
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
        	ElseIf Upper( aEquivale[nInc] ) == "NAJUSINFLA"//23 - Importe del valor del ajuste por inflaci�n del Fijo  
         		cLin += AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
        		cLin += cSep 
			ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM"  //29 - Depreciaci�n  acumulada al cierre del ejercicio anterior   
         		cDepraCM  := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
			ElseIf Upper( aEquivale[nInc] ) == "NDEPRACM2" //30 - Valor de la depreciaci�n del ejercicio sin considerar  la revaluaci�n  
         		cDepraCM2 := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
			ElseIf Upper( aEquivale[nInc] ) == "NDEPRBXS"  //31 - Valor de la depreciaci�n del ejercicio relacionada con los retiros y/o bajas 
         		cDeprbxs  := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
         	ElseIf Upper( aEquivale[nInc] ) == "NAJUSTES2" //32 - Valor de la depreciaci�n relacionada  con otros ajustes
         		cAjustes2 := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
         	ElseIf Upper( aEquivale[nInc] ) == "NVOLUL2"   //33 - Valor de la depreciaci�n de la revaluaci�n voluntaria efectuada  
         		cVolul2	  := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
         	ElseIf Upper( aEquivale[nInc] ) == "NSOCIE2"   //34 - Valor de la depreciaci�n de la revaluaci�n efectuada por reorganizaci�n de sociedades  
         		cSocie2	  := AllTrim(Transform(aTotais[nInc],"@E 99999999999"))
         	ElseIf Upper( aEquivale[nInc] ) == "NOUTROS2"  //35 - Valor de la depreciaci�n de otras revaluaciones efectuadas   
         		cOutros2  := AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))
         	ElseIf Upper( aEquivale[nInc] ) == "NAJUDINFLA"//36 - Valor del ajuste por inflaci�n de la depreciaci�n   
         		cAjudinlfa:= AllTrim(Transform(aTotais[nInc],"@E 999999999.99"))  //IF(AllTrim(STR(aTotais[nInc]))== "0","0.00",AllTrim(STR(aTotais[nInc])))
         	EndIf
		Next		   				
        
		//24 - Fecha de adquisici�n  del Activo Fijo    
		cLin +=SubStr(DTOC(PER->N1_AQUISIC),1,6)+SubStr(DTOS(PER->N1_AQUISIC),1,4)
        cLin += cSep       
        
		//25 - Fecha de inicio del Uso del Activo Fijo
		cLin +=  SubStr(DTOC(PER->N3_DINDEPR),1,6)+SubStr(DTOS(PER->N3_DINDEPR),1,4)  
        cLin += cSep     
        
		//26 - C�digo del M�todo aplicado en el c�lculo de la depreciaci�n  	
        cLin += AllTrim(PER->N3_TPDEPR)
        cLin += cSep      
                       
		//27 - N�mero de documento de autorizaci�n para cambiar el m�todo de la depreciaci�n		
		cLin += AllTrim(PER->N3_AUTDEPR)
        cLin += cSep      
        
		//28 - Porcentaje  de la depreciaci�n  
		
		cLin += AllTrim(Transform(PER->N3_TXDEPR1,"@E 999999999.99")) 
		cLin += cSep			
		               
		//29 - Depreciaci�n  acumulada al cierre del ejercicio anterior.   
		cLin += cDepraCM
		cLin += cSep		
		   
		//30 - Valor de la depreciaci�n del ejercicio sin considerar  la revaluaci�n
		cLin += cDepraCM2
        cLin += cSep
        
		//31 - Valor de la depreciaci�n del ejercicio relacionada con los retiros y/o bajas
		cLin += cDeprbxs
        cLin += cSep	
		      
		//32 - Valor de la depreciaci�n relacionada  con otros ajustes 
		cLin += cAjustes2
        cLin += cSep
        
		//33 - Valor de la depreciaci�n de la revaluaci�n voluntaria efectuada
		cLin += cVolul2
        cLin += cSep
                 
		//34 - Valor de la depreciaci�n de la revaluaci�n efectuada por reorganizaci�n de sociedades 
        cLin += cSocie2
        cLin += cSep      
         
		//35 - Valor de la depreciaci�n de otras revaluaciones efectuadas 
		cLin += cOutros2
        cLin += cSep 
        
        //36 - Valor del ajuste por inflaci�n de la depreciaci�n
        cLin += cAjudinlfa
        cLin += cSep
		               
		//37 - Indica el estado de la operaci�n
		cLin += "1" 
		cLin += cSep
					
		cLin += chr(13)+chr(10)
		fWrite(nHdl,cLin)
		PER->(dbSkip())
		
		If cProd == PER->N1_CBASE
			lRet1 := .T.	
			cProd := PER->N1_CBASE 
		Else	
			lRet1 := .F.		
		EndIf
		
	EndDo
	fClose(nHdl)
MsgAlert(STR0092) // "Archivo TXT generado con �xito"
EndIf

Return Nil