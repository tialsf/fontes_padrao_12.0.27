#Include "ctbr701A.ch"
#Include "protheus.ch"

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o	 � CTBR701	� Autor � Marcelo Akama			� Data � 17-09-2010 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Estado de Cambios en el Patrimonio Neto						���
���������������������������������������������������������������������������Ĵ��
��� Uso		 � Generico														���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador �Data    � BOPS     � Motivo da Alteracao                  ���
�������������������������������������������������������������������������Ĵ��
���Jonathan Glz�26/06/15�PCREQ-4256�Se elimina la funcion Ctr701Sx1() la  ���
���            �        �          �cual realiza modificacion a SX1 por   ���
���            �        �          �motivo de adecuacion a fuentes a nueva���
���            �        �          �estructura de SX para Version 12.     ���
���Jonathan Glz�09/10/15�PCREQ-4261�Merge v12.1.8                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Function Ctbr701A()

Private cProg     := "CTBR701A"
Private cPerg     := "CTR701"
Private aItems    := {}
Private aSetOfBook:= {}
Private oReport
Private oSection1
Private oSection2
Private aColumnas :={}
Private aRet := {}
Private aCab:={}
//Verifica si los informes personalizables est�n disponibles
If TRepInUse()
	If Pergunte(cPerg,.T.)
		oReport:=ReportDef()
		oReport:PrintDialog()
	EndIf
EndIf

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � ReportDef� Autor � Marcelo Akama			 � Data � 17/09/10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �															   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Nenhum													   ��
��������������������������������������������������������������������������Ĵ�
���Parametros� Nenhum													   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function ReportDef()

Local cTitulo := STR0001 //"ESTADO DE CAMBIOS EN EL PATRIMONIO NETO"
Local cDescri := STR0002+Chr(10)+Chr(13)+STR0003 //"Este programa imprimira el Estado de Cambios en el Patrimonio Neto"##"Se imprimira de acuerdo con los Param. solicitados por el usuario."
Local bReport := { |oReport|	ReportPrint( oReport ) }

aSetOfBook:= CTBSetOf(mv_par04)

If ValType(mv_par08)=="N" .And. (mv_par08 == 1)
	cTitulo := CTBNomeVis( aSetOfBook[5] )
EndIf

oReport  := TReport():New(cProg, cTitulo, cPerg , bReport, cDescri,.T.,,.F.)
oReport:SetCustomText( {|| CustHeader(oReport, cTitulo, mv_par02, mv_par05) } )
oReport:ParamReadOnly()
oSection1:= TRSection():New(oReport  ,OemToAnsi(""),{"",""})
oSection2:= TRSection():New(oReport  ,OemToAnsi(""),{"",""})
//                         (oParent  ,cTitle,uTable,aOrder,lLoadCells,lLoadOrder,uTotalText,lTotalInLine,lHeaderPage,lHeaderBreak,lPageBreak,lLineBreak,nLeftMargin,lLineStyle,nColSpace,lAutoSize,cCharSeparator,nLinesBefore,nCols,nClrBack,nClrFore,nPercentage)

oSection1:SetPageBreak(.F.)
oSection1:SetLineBreak(.F.)
oSection1:SetHeaderPage(.F.)
oSection1:SetTotalInLine(.F.)
oSection1:SetHeaderSection(.F.)
oSection2:SetHeaderSection(.F.)

Return oReport

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � ReportPrint� Autor � Marcelo Akama		 � Data � 17/09/10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �															   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Nenhum													   ��
��������������������������������������������������������������������������Ĵ�
���Parametros� Nenhum													   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportPrint(oReport)
Local x			:= 1
Local y			:= 1
Local cAlign
Local oFntReg
Local oFntBld

If (mv_par08 == 1)
	oReport:SetTitle( CTBNomeVis( aSetOfBook[5] ) )
EndIf  

If Empty(aSetOfBook[5])
	ApMsgAlert(STR0013) //"Plan de Gesti�n no asociado al libro. �Verifique la configuraci�n elegida del libro! "
	Return .F.
Endif

aItems := CriaArray()

oFntReg := TFont():New(oReport:cFontBody,0,(oReport:nFontBody+2)*(-1),,.F.,,,,.F. /*Italic*/,.F. /*Underline*/)
oFntBld := TFont():New(oReport:cFontBody,0,(oReport:nFontBody+2)*(-1),,.T.,,,,.F. /*Italic*/,.F. /*Underline*/)

//��������������������������������������������������������������Ŀ
//� Crea la secci�n, el encabezado y el totalizador              �
//����������������������������������������������������������������
If Len(aItems) > 0
	For x:=1 To Len(aItems[1])
		nTam := 0
		cAlign := "LEFT"
		For y := 1 to Len(aItems)
			If ValType(aItems[y][x])=='N'
				cAlign := "RIGHT"
				If nTam < 35
					nTam := 35
				EndIf
			Else
				If nTam < Len(alltrim(aItems[y][x]))
					nTam := Len(alltrim(aItems[y][x]))
				EndIf
			EndIf
		Next y
		TRCell():New(oSection1,aItems[1][x],"",aItems[1][x],,nTam,.F.,,cAlign,.T.,cAlign,,,x>1/*AutoSize*/,CLR_WHITE,CLR_BLACK) 
		oSection1:Cell(oSection1:aCell[x]:cName):SetValue(alltrim(aItems[1][x]))
		oSection1:Cell(oSection1:aCell[x]:cName):oFontBody := oFntBld
	Next x
	
	//��������������������������������������������������������������Ŀ
	//� Elimina la primera l�nea para no imprimir de nuevo como item �
	//����������������������������������������������������������������
	aDel(aItems,1)
	aSize(aItems,Len(aItems)-1)
EndIf

//          (oParent,cName,cAlias,cTitle,cPicture,nSize,lPixel,bBlock,cAlign,lLineBreak,cHeaderAlign,lCellBreak,nColSpace,lAutoSize,nClrBack,nClrFore) CLASS TRCell
TRCell():New(oSection2,"LEFT"    ,"","",,50,.F.,,"CENTER",,"CENTER",,,.T.)
TRCell():New(oSection2,"GERENTE" ,"","",,50,.F.,,"CENTER",,"CENTER")
TRCell():New(oSection2,"MID"     ,"","",,20,.F.,,"CENTER",,"CENTER")
TRCell():New(oSection2,"CONTADOR","","",,50,.F.,,"CENTER",,"CENTER")
TRCell():New(oSection2,"RIGHT"   ,"","",,50,.F.,,"CENTER",,"CENTER",,,.T.)

//��������������������������������������������������������������Ŀ
//� Inicializa la impresi�n                                      �
//����������������������������������������������������������������
oReport:SetMeter(Len(aItems))
oSection1:Init()

oSection1:PrintLine()

oReport:ThinLine()

For x:=1 To Len(aItems)-1
	If oReport:Cancel()
		x:=Len(aItems)
	EndIf

	For y:=1 To Len(aItems[x])
		If ValType(aItems[x][y])='N'
			oSection1:Cell(oSection1:aCell[y]:cName):SetValue( ValorCTB(aItems[x][y],,,14,02,.T.,"@E 999,999,999.99","1",,,,,,mv_par06==1,.F.) )
		Else
			oSection1:Cell(oSection1:aCell[y]:cName):SetValue(alltrim(aItems[x][y]))
		EndIf
		If x = Len(aItems) .Or. (mv_par03==2 .And. x = 1 .And. y = 1) 
			oSection1:Cell(oSection1:aCell[y]:cName):oFontBody := oFntBld
		Else
			oSection1:Cell(oSection1:aCell[y]:cName):oFontBody := oFntReg
		EndIf
	Next y
	oSection1:PrintLine()
	oReport:IncMeter()
Next x     

//��������������������������������������������������������������Ŀ
//� Imprimir mensaje si no hay registros para imprimir           �
//����������������������������������������������������������������
If Len(aItems) == 0
	oReport:PrintText(STR0004)//"No hay datos por mostrarse"
EndIf

//��������������������������������������������������������������Ŀ
//� Finaliza la secci�n de impresi�n                             �
//����������������������������������������������������������������
oSection1:Finish()

oSection2:Cell("GERENTE"):SetValue(STR0011) // "GERENTE"
oSection2:Cell("GERENTE"):SetBorder("TOP")
oSection2:Cell("CONTADOR"):SetValue(STR0012) // "CONTADOR"
oSection2:Cell("CONTADOR"):SetBorder("TOP")
oReport:SkipLine(5)
oSection2:Init()
oSection2:PrintLine()
oSection2:Finish()

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � CriaArray� Autor � Marcelo Akama			 � Data � 20/09/10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �															   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Nenhum                                                      ��
��������������������������������������������������������������������������Ĵ�
���Parametros� Nenhum                                                      ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CriaArray()        
Local aArea		:= GetArea()
Local aAreaCTS	:= CTS->(GetArea())
Local cQry		:= ''
Local cAlias
Local nX
Local nY
Local nI
Local cTrim := 'LTRIM'
Local cIsNull := 'COALESCE'
Local nA := 1
Local nPos :=0
Local aSubT :={}
Local nValor := 0
Local aTotal :={{}}
Local aTotalF :={{}}
Local nTotal := 0

If Alltrim(Upper(TcGetDB()))=='INFORMIX'
	cTrim := 'TRIM"
EndIf

If Alltrim(Upper(TcGetDB()))=='ORACLE'
	cTrim := 'NVL'
EndIf

dbSelectArea("CTS")
CTS->(DbSetOrder(1))
If CTS->(DbSeek(xFilial("CTS")+aSetOfBook[5]))
	Do While !CTS->(Eof())
		If  AScan(aCab, {|x| x[1] == CTS->CTS_CONTAG}) == 0 .And. CTS->CTS_CODPLA == aSetOfBook[5] 
			AADD(aCab, {CTS->CTS_CONTAG, CTS->CTS_DESCCG,CTS->CTS_CLASSE,CTS->CTS_IDENT,CTS_CTASUP})
		EndIf
		CTS->(DbSkip())
	Enddo
EndIf

cQry += "select CTS_CONTAG, CT2_LP, sum(VALOR) VALOR,CTS_ORDEM, CTS_FORMUL "+CRLF 
cQry += "from ( "+CRLF
cQry += "	select CTS.CTS_CONTAG, CT2.CT2_LP, CT2.CT2_VALOR*-1 VALOR ,CTS_ORDEM, CTS_FORMUL "+CRLF
cQry += "	from "+RetSqlName('CTS')+" CTS "+CRLF
cQry += "	inner join "+RetSqlName('CT2')+" CT2 "+CRLF
cQry += "	on CT2.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CT2.CT2_FILIAL = '"+xFilial('CT2')+"' "+CRLF
cQry += "	and CT2.CT2_DATA between '"+DTOS(mv_par01)+"' and '"+DTOS(mv_par02)+"' "+CRLF
cQry += "	and CT2.CT2_MOEDLC = '"+mv_par05+"' "+CRLF
cQry += "	and CT2.CT2_TPSALD = '"+mv_par07+"' "+CRLF
cQry += "	and ltrim(CT2.CT2_LP)<>'' "+CRLF
cQry += "	and CT2.CT2_DC in ('1','3') "+CRLF
cQry += "	and ( CT2.CT2_DEBITO between CTS.CTS_CT1INI and CTS.CTS_CT1FIM or "+cTrim+"(CTS.CTS_CT1FIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CCD    between CTS.CTS_CTTINI and CTS.CTS_CTTFIM or "+cTrim+"(CTS.CTS_CTTFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_ITEMD  between CTS.CTS_CTDINI and CTS.CTS_CTDFIM or "+cTrim+"(CTS.CTS_CTDFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CLVLDB between CTS.CTS_CTHINI and CTS.CTS_CTHFIM or "+cTrim+"(CTS.CTS_CTHFIM) = '' ) "+CRLF
cQry += "	where CTS.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CTS.CTS_FILIAL = '"+xFilial('CTS')+"' "+CRLF
cQry += "	and CTS.CTS_CODPLA = '"+aSetOfBook[5]+"' "+CRLF
cQry += "	and CTS.CTS_CLASSE='2' "+CRLF
cQry += "	and CTS_CONTAG Between '4D0201' AND '4D0299' "+CRLF
cQry += "	"+CRLF
cQry += "	union all "+CRLF
cQry += "	"+CRLF
cQry += "	select CTS.CTS_CONTAG, CT2.CT2_LP, CT2.CT2_VALOR VALOR,CTS_ORDEM, CTS_FORMUL "+CRLF
cQry += "	from "+RetSqlName('CTS')+" CTS "+CRLF
cQry += "	inner join "+RetSqlName('CT2')+" CT2 "+CRLF
cQry += "	on CT2.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CT2.CT2_FILIAL = '"+xFilial('CT2')+"' "+CRLF
cQry += "	and CT2.CT2_DATA between '"+DTOS(mv_par01)+"' and '"+DTOS(mv_par02)+"' "+CRLF
cQry += "	and CT2.CT2_MOEDLC = '"+mv_par05+"' "+CRLF
cQry += "	and CT2.CT2_TPSALD = '"+mv_par07+"' "+CRLF
cQry += "	and ltrim(CT2.CT2_LP)<>'' "+CRLF
cQry += "	and CT2.CT2_DC in ('2','3') "+CRLF
cQry += "	and ( CT2.CT2_CREDIT between CTS.CTS_CT1INI and CTS.CTS_CT1FIM or "+cTrim+"(CTS.CTS_CT1FIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CCC    between CTS.CTS_CTTINI and CTS.CTS_CTTFIM or "+cTrim+"(CTS.CTS_CTTFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_ITEMC  between CTS.CTS_CTDINI and CTS.CTS_CTDFIM or "+cTrim+"(CTS.CTS_CTDFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CLVLCR between CTS.CTS_CTHINI and CTS.CTS_CTHFIM or "+cTrim+"(CTS.CTS_CTHFIM) = '' ) "+CRLF
cQry += "	where CTS.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CTS.CTS_FILIAL = '"+xFilial('CTS')+"' "+CRLF
cQry += "	and CTS.CTS_CODPLA = '"+aSetOfBook[5]+"' "+CRLF
cQry += "	and CTS.CTS_CLASSE='2' "+CRLF
cQry += "	and CTS_CONTAG Between '4D0201' AND '4D0299' "+CRLF
cQry += "	"+CRLF
cQry += "	union all "+CRLF
cQry += "	"+CRLF
cQry += "	select CTS.CTS_CONTAG, '' CT2_LP, "+cIsNull+"(CT2.CT2_VALOR,0)*-1 VALOR,CTS_ORDEM, CTS_FORMUL "+CRLF
cQry += "	from "+RetSqlName('CTS')+" CTS "+CRLF
cQry += "	left outer join "+RetSqlName('CT2')+" CT2 "+CRLF
cQry += "	on CT2.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CT2.CT2_FILIAL = '"+xFilial('CT2')+"' "+CRLF
cQry += "	and CT2.CT2_DATA < '"+DTOS(mv_par01)+"' "+CRLF
cQry += "	and CT2.CT2_MOEDLC = '"+mv_par05+"' "+CRLF
cQry += "	and CT2.CT2_TPSALD = '"+mv_par07+"' "+CRLF
cQry += "	and ltrim(CT2.CT2_LP)<>'' "+CRLF
cQry += "	and CT2.CT2_DC in ('1','3') "+CRLF
cQry += "	and ( CT2.CT2_DEBITO between CTS.CTS_CT1INI and CTS.CTS_CT1FIM or "+cTrim+"(CTS.CTS_CT1FIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CCD    between CTS.CTS_CTTINI and CTS.CTS_CTTFIM or "+cTrim+"(CTS.CTS_CTTFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_ITEMD  between CTS.CTS_CTDINI and CTS.CTS_CTDFIM or "+cTrim+"(CTS.CTS_CTDFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CLVLDB between CTS.CTS_CTHINI and CTS.CTS_CTHFIM or "+cTrim+"(CTS.CTS_CTHFIM) = '' ) "+CRLF
cQry += "	where CTS.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CTS.CTS_FILIAL = '"+xFilial('CTS')+"' "+CRLF
cQry += "	and CTS.CTS_CODPLA = '"+aSetOfBook[5]+"' "+CRLF
cQry += "	and CTS.CTS_CLASSE='2' "+CRLF
cQry += "	and CTS_CONTAG Between '4D0101' AND '4D0199' "+CRLF
cQry += "	"+CRLF
cQry += "	union all "+CRLF
cQry += "	"+CRLF
cQry += "	select CTS.CTS_CONTAG, '' CT2_LP, "+cIsNull+"(CT2.CT2_VALOR,0) VALOR , CTS_ORDEM, CTS_FORMUL "+CRLF
cQry += "	from "+RetSqlName('CTS')+" CTS "+CRLF
cQry += "	left outer join "+RetSqlName('CT2')+" CT2 "+CRLF
cQry += "	on CT2.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CT2.CT2_FILIAL = '"+xFilial('CT2')+"' "+CRLF
cQry += "	and CT2.CT2_DATA < '"+DTOS(mv_par01)+"' "+CRLF
cQry += "	and CT2.CT2_MOEDLC = '"+mv_par05+"' "+CRLF
cQry += "	and CT2.CT2_TPSALD = '"+mv_par07+"' "+CRLF
cQry += "	and ltrim(CT2.CT2_LP)<>'' "+CRLF
cQry += "	and CT2.CT2_DC in ('2','3') "+CRLF
cQry += "	and ( CT2.CT2_CREDIT between CTS.CTS_CT1INI and CTS.CTS_CT1FIM or "+cTrim+"(CTS.CTS_CT1FIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CCC    between CTS.CTS_CTTINI and CTS.CTS_CTTFIM or "+cTrim+"(CTS.CTS_CTTFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_ITEMC  between CTS.CTS_CTDINI and CTS.CTS_CTDFIM or "+cTrim+"(CTS.CTS_CTDFIM) = '' ) "+CRLF
cQry += "	and ( CT2.CT2_CLVLCR between CTS.CTS_CTHINI and CTS.CTS_CTHFIM or "+cTrim+"(CTS.CTS_CTHFIM) = '' ) "+CRLF
cQry += "	where CTS.D_E_L_E_T_ = '' "+CRLF
cQry += "	and CTS.CTS_FILIAL = '"+xFilial('CTS')+"' "+CRLF
cQry += "	and CTS.CTS_CODPLA = '"+aSetOfBook[5]+"' "+CRLF
cQry += "	and CTS.CTS_CLASSE='2' "+CRLF
cQry += "	and CTS_CONTAG Between '4D0101' AND '4D0199' "+CRLF

cQry += ") a "+CRLF
cQry += "group by CT2_LP, CTS_ORDEM, CTS_CONTAG, CTS_FORMUL "+CRLF
cQry += "order by CT2_LP, CTS_ORDEM, CTS_CONTAG, CTS_FORMUL"+CRLF

cQry := ChangeQuery(cQry)
cAlias := GetNextAlias()
MsAguarde({|| dbUseArea(.T., "TOPCONN", TCGenQry(,,cQry), cAlias, .F., .T.)},STR0005,STR0006) // "Ejecutando consulta"###"Seleccionando asientos..."
DbSelectArea(cAlias)

If !(cAlias)->(Eof())

	For nA := 1 to 11
		AADD(aRet, Array( len(aCab) + 2 ))
		nY := Len(aRet)
		For nX := 2 to Len(aRet[nY])
			aRet[nY][nX] := 0
		Next
	Next
	
	Do While !(cAlias)->(Eof())
		If AllTrim((cAlias)->CTS_FORMUL)!= ""
			nY := Val(ALLTRIM((cAlias)->CTS_FORMUL))
			nX := AScan(aCab, {|x| x[1] == (cAlias)->CTS_CONTAG})
			If nX > 0 
				aRet[nY][nX+1] += (cAlias)->VALOR
				aRet[nY][len(aRet[nY])] += (cAlias)->VALOR
			EndIf
		Endif
	
	(cAlias)->(DbSkip())
	Enddo
	(cAlias)->(dbCloseArea())
	
	// Adiciona l�nea de Total
	AADD(aRet, Array( len(aCab) + 2 ))
	nY := Len(aRet)
	aRet[nY][1] := STR0024 //"Resultado Neto del Ejercicio"
	For nX := 2 to Len(aRet[nY])
		aRet[nY][nX] := 0
		For nI := 1 to nY-1
			aRet[nY][nX] += aRet[nI][nX]	
		Next
	Next


	// Adiciona encabezado
	AADD(aRet, nil)
	AINS(aRet, 1)
	aRet[1] := Array( len(aCab) + 2 )
	For nX := 1 to Len(aCab)
		aRet[1][nX+1] := aCab[nX][2]
		For nI := 2 to Len(aRet)
			If AllTrim(aCab[nX][3]) == "2" .And. AllTrim(aCab[nX][4]) == "5"
				aRet[nI][nX+1] := "" 
			EndIf
		Next 		
	Next
	
	For nI :=2 to nY
		aSubT:={}
		For nX:=2 to Len(aCab)
			If AllTrim(aCab[nX][3]) == "2" .And. AllTrim(aCab[nX][4]) != "5"
				//Sumarizaci�n a cuentas Superior
				nPos := AScan(aSubT, {|x| AllTrim(x[1]) == AllTrim(aCab[nX][5])})
				nValor := aRet[nI][nX+1]
				If nPos == 0 
					Aadd(aSubT,{AllTrim(aCab[nX][5]),nValor,AllTrim(aCab[nX][1])})
				Else
					aSubT[nPos][2] += nValor
				EndIf				
			Endif
		Next
		AADD(aTotal,aSubT)
	Next
	
	aColumnas := {STR0007,STR0014,STR0015,STR0016,STR0017,STR0018,STR0019,STR0020,STR0021,STR0022,STR0023,STR0024}
	For nY:=2 to Len(aColumnas)
		aRet[nY][1] := aColumnas[nY]
		//Acomoda Subtotales a Cuentas Superior
		For nPos :=1 to Len(aTotal[nY])
			nX := AScan(aCab, {|x| AllTrim(x[1]) == aTotal[nY][nPos][1]})
			If 	nX > 0
				aRet[nY][nX+1] := aTotal[nY][nPos][2]
			EndIf
		Next
	Next
	
	//Sumariza los Totales para cuentas tipo ident 3
	For nI :=2 to nY-1
		aSubT:={}
		For nX:=2 to Len(aCab)
			If AllTrim(aCab[nX][3]) == "1" .And. AllTrim(aCab[nX][4]) == "3"
				nPos := AScan(aSubT, {|x| AllTrim(x[1]) == AllTrim(aCab[nX][5])})
				nValor := aRet[nI][nX+1]
				If nPos == 0 
					Aadd(aSubT,{AllTrim(aCab[nX][5]),nValor,AllTrim(aCab[nX][1])})
				Else
				 	If AllTrim(aCab[nX][5])!= "" 
				 		aSubT[nPos][2] := aSubT[nPos][2] + nValor
					Else
						aSubT[nPos][2] += 0 
					EndIf
				EndIf	
			Endif
		Next
		AADD(aTotalF,aSubT)
	Next

   //Acomoda Totales a Cuentas Superior
	For nY:=2 to Len(aColumnas)
		For nPos :=1 to Len(aTotalF[nY])
			nX := AScan(aCab, {|x| AllTrim(x[1]) == aTotalF[nY][nPos][1]})
			If 	nX > 0
				aRet[nY][nX+1] := aTotalF[nY][nPos][2]
			EndIf
		Next
	Next
	//Actualiza columna 12 de las Cuentas Superior
	For nX:=2 to Len(aCab)
		If AllTrim(aCab[nX][3]) == "1" .And. AllTrim(aCab[nX][4]) == "3"
			nTotal:=0
			For nY:=2 to Len(aColumnas)
				If 	nY == Len(aColumnas)
					aRet[nY+1][nX+1] := nTotal
				Else
					nTotal := aRet[nY][nX+1] + nTotal
				EndIf
			Next
		Endif
	Next

	aRet[1][1] := aColumnas[1] // "Cuenta"
	aRet[1][len(aRet[1])] := STR0008 // "Total"

	If mv_par03==1
		aRet := PivotTable(aRet)
	EndIf
Else
	If Len(aCab) == 0
	ApMsgAlert(STR0014) //"Capital"
	EndIf
EndIf


RestArea(aAreaCTS)
RestArea(aArea)
If MV_PAR09 == 1           
	IF MSGYESNO(STR0025) //"�Deseea generar archivo txt?"
	   Processa({|| GerArq(AllTrim(MV_PAR10),aRet,aCab)},,STR0025) // Generando archivo...
	Endif   
EndIf
Return(aRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � CustHeader� Autor � Marcelo Akama		 � Data � 22/09/10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �															   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Nenhum                                                      ��
��������������������������������������������������������������������������Ĵ�
���Parametros� Nenhum                                                      ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CustHeader(oReport, cTitulo, dData, cMoeda)
Local cNmEmp   
Local cChar		:= chr(160)  // car�cter falso para la alineaci�n del encabezado
Local cTitMoeda := Alltrim(GETMV("MV_MOEDAP"+Alltrim(Str(val(cMoeda)))))

DEFAULT dData    := cTod('  /  /  ')

If SM0->(Eof())                                
	SM0->( MsSeek( cEmpAnt + cFilAnt , .T. ))
Endif

cNmEmp	:= AllTrim( SM0->M0_NOMECOM )

RptFolha := GetNewPar( "MV_CTBPAG" , RptFolha )

aCabec := {	"__LOGOEMP__" + "         " + cChar + "         " + cNmEmp + "         " + cChar + "         " + RptFolha+ TRANSFORM(oReport:Page(),'999999'),; 
			"SIGA /" + cProg + "/v." + cVersao + "         " + cChar + AllTrim(cTitulo) + "         " + cChar + RptDtRef + " " + DTOC(dDataBase),;
			RptHora + " " + time() + cChar + "         " + STR0009 + " " + DTOC(dData) + "         " + cChar + RptEmiss + " " + Dtoc(date()),;
			cChar + "         " + "(" + STR0010 + " " + cTitMoeda + ")" + "         " + cChar }

Return aCabec


/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
��� Funcao     � GerArq   � Autor � V�ronica Flores     � Data � 10.05.2016 ���
���������������������������������������������������������������������������Ĵ��
��� Descricao  � 3.19 - LIBRO DE INVENTARIOS Y BALANCES                     ���
���������������������������������������������������������������������������Ĵ��
��� Parametros � cDir - Diretorio de criacao do arquivo.                    ���
���            � cArq - Nome do arquivo com extensao do arquivo.            ���
���������������������������������������������������������������������������Ĵ��
��� Retorno    � Nulo                                                       ���
���������������������������������������������������������������������������Ĵ��
��� Uso        � Fiscal Peru                  - Arquivo Magnetico           ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Static Function GerArq(cDir,aItems,aCab)

Local nHdl  := 0
Local cLin  := ""
Local cSep  := "|"
Local nCont := 0
Local cArq  := ""
Local nMes 	:=  Month(MV_PAR02)
Local cDateFmt := SET(_SET_DATEFORMAT)
Local nx
Local nY

FOR nCont:=LEN(ALLTRIM(cDir)) TO 1 STEP -1
	IF SUBSTR(cDir,nCont,1)=='\'
		cDir:=Substr(cDir,1,nCont)
		EXIT
	ENDIF	
NEXT
 
cArq += "LE"                                  // Fixo  'LE'
cArq +=  AllTrim(SM0->M0_CGC)                 // Ruc
cArq +=  AllTrim(Str(Year(MV_PAR02)))         // Ano
cArq +=  AllTrim(Strzero(Month(MV_PAR02),2))  // Mes
cArq +=  AllTrim(Strzero(Day(MV_PAR02),2))    // Dia
cArq += "031900" 						     // Fixo '031900'
                           
//C�digo de Oportunidad
If nMes == 12
	cArq += "01"	
ElseIf nMes == 1
	cArq += "02"
ElseIf nMes == 6
	cArq += "04"
Else
	cArq += "07"
EndIf                         
cArq += "1"
cArq += "1"
cArq += "1"
cArq += "1"
cArq += ".TXT" // Extensao

nHdl := fCreate(cDir+cArq)
If nHdl <= 0
	ApMsgStop(STR0027) //"No se pudo generar el archivo"
Else

	For nX:=2 To Len(aRet)-1
	    If  aCab[nX-1][4] != '5' 
		cLin := ""
		//01 - Periodo
		
		cLin += SubStr(DTOS(mv_par02),1,8)
		cLin += cSep
		
		//02 - C�digo del c�talogo
			cLin += "09"
			cLin += cSep
		
		//03 - C�digo Rubro Financiero
		cLin += AllTrim(aCab[nX-1][1])
		cLin += cSep
		
		For nY:=1 To Len(aRet[nX])
			If ValType(aRet[nX][nY])='N'
			cLin += AllTrim(Transform(aRet[nx][ny],"@E 999999999.99"))
			cLin += cSep
			EndIf
		Next y
		
		//14- Indica el Estado de Operaci�n
			cLin += "1"	
			cLin += cSep
		cLin += chr(13)+chr(10)
		
		fWrite(nHdl,cLin)
		EndIf
	Next x    
	SET(_SET_DATEFORMAT,cDateFmt)
	fClose(nHdl)
	MsgAlert(STR0028) //"Archivo generado correctamente"
EndIf

Return Nil

