#INCLUDE "FINA095.CH"
#INCLUDE "PROTHEUS.CH"

Static lFWCodFil := .t.
STATIC lMod2     := .t.

/*���������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������Ŀ��
���Fun��o    � FINA095  � Autor � Wagner Montenegro           � Data � 30.09.10 ���
�������������������������������������������������������������������������������Ĵ��
���Descri��o � Controle de Cheques Emitidos e Cadastro de Talon�rio.            ���
�������������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                           ���
�������������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                           ���
�������������������������������������������������������������������������������Ĵ��
���                          ATUAIZACOES SOFRIDAS                               ���
�������������������������������������������������������������������������������Ĵ��
���Programador  � Data   �   BOPS   �           Motivo da Alteracao             ���
�������������������������������������������������������������������������������Ĵ��
���Jonathan Glez�14/12/16 �SERINN001-�Elimina funciones fa095ASIX con motivo    ���
���             �        �       486�de limpiza de SX                           ���
���Marco A. Glz �24/03/17�  MMI-41  �Se replica llamado (TWHERC - V11.8), Se    ���
���             �        �          �agrega validacion, para que solo se susti- ���
���             �        �          �tuyan cheques desde Anulacion de Ordenes de���
���             �        �          �Pago. (ARG)                                ���
���Laura Medina �24/03/17�  MMI-4145�Se replica llamado (THIMXG - V11.8), se    ���
���             �        �          �corrigi� la inclusi�n de mov a pagar, en la���
���             �        �          �rutina de Movimiento Bancario, para que per���
���             �        �          �mita incluir documentos con un item.       ���
���Roberto Glez �19/06/17� MMI-5711 �Se agregan asignaciones para la generaqci�n���
���             �        �          �de NF por movimientos bancarios seg�n los  ���
���             �        �          �datos informados en los par�metros.        ���
��������������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������������
���������������������������������������������������������������������������������*/
Function FINA095()

	Local lFiltraBco  := GetNewPar("MV_FILTBCO","2") == "1"
	Local cQuery := ""
	Local aIndFil := {}
	
	PRIVATE aButtons	:=	{}
	Private cCadastro := STR0001
	Private lInverte:=.F.
	Private lGeraLanc:=.T.
	Private aIndex		:= {}
	Private bFiltraBrw
	Private dIniDt380:= dDataBase
	Private dFimDt380:= dDataBase
	Private lIndice12 := .F.
	Private lCtrlCheq :=.T.
	Private cBco380	:=Criavar("EF_BANCO")
	Private cAge380	:=Criavar("EF_AGENCIA")
	Private cCta380	:=Criavar("EF_CONTA")
	Private cCheq380	:=Criavar("EF_NUM")
	Private cBcoDe 	:= cBco380
	Private cBcoAte	:= cBco380
	Private dDataDe 	:= dIniDt380
	Private dDataAte 	:= dFimDt380
	Private aUsoCH		:= {STR0002,STR0003}
	Private cUsoCH		:=	aUsoCH[1]
	Private cCondicao   := ""
	PRIVATE aIndices		:=	{} //Array necessario para a funcao FilBrowse
	PRIVATE bFilBrw := {|| }
	PRIVATE cFil090
	PRIVATE cFilter	:=	Nil
	PRIVATE nIndTemp:=1
	PRIVATE dDataLanc	:= dDataBase
	PRIVATE cPadrao	:= "530"
	PRIVATE cBordero	:= CriaVar("E2_NUMBOR")
	PRIVATE cPortado	:= CriaVar("E2_PORTADO")
	PRIVATE dVencIni	:= dDataBase
	PRIVATE dVencFim	:= dDataBase
	PRIVATE dBaixa		:= dDataBase
	PRIVATE nJuros		:= 0
	PRIVATE nCorrec	:= 0
	PRIVATE cCtBaixa	:= GetMv("MV_CTBAIXA")
	PRIVATE cMarca		:= GetMark()
	PRIVATE cMarcaE2	:= GetMark()
	PRIVATE cKey1, cIndexNew
	PRIVATE nAcresc     := 0
	PRIVATE nDecresc    := 0
	PRIVATE cCodDiario	:= ""
	PRIVATE cMoedBco	:= CriaVar( "E2_MOEDA" )
	PRIVATE aRecNoSE2   := {}
	Private cFilCxCtr :=Left(GetMv("MV_CXFIN"),3)+"/"+GetMv("MV_CARTEIR")+"/"+IsCxLoja()
	Private aDados050 :={}
	Private lAnular
	
	//�����������������������������Ŀ
	//� Verifica o n�mero do Lote   �
	//�������������������������������
	PRIVATE cLote := "",lAltera	:=.F.
	PRIVATE lFa380		:= ExistBlock("F380RECO",.F.,.F.)
	
	/*
	 * Verifica��o do processo que est� configurado para ser utilizado no M�dulo Financeiro (Argentina)
	 */
	If lMod2
		If !FinModProc()
			Return()
		EndIf
	EndIf
	
	Aadd(aButtons,{STR0004,{||FINC021(),STR0005,STR0004}})
	
	Private aCampos := 	{ {STR0006,"EF_BANCO"		,"",00,00,""} ,; //"Banco"
					           {STR0007,"EF_AGENCIA"		,"",00,00,""} ,; //"Agencia"
					           {STR0008,"EF_CONTA"		,"",00,00,""} ,; //"Conta"
					           {STR0009,"EF_NUM"			,"",00,00,""} ,; //"Numero"
					           {STR0010,"EF_VALOR"		,"",00,00,""} ,; //"Valor"
					           {STR0011,"EF_DATA"			,"",00,00,""} ,; //"Emissao"
					           {STR0012,"EF_VENCTO"		,"",00,00,""} ,; //"Vencimento"
					           {STR0013,"EF_PREFIXO"		,"",00,00,""} ,; //"Prefixo"
					           {STR0014,"EF_TITULO"		,"",00,00,""} ,; //"Titulo"
					           {STR0015,"EF_PARCELA"		,"",00,00,""} ,; //"Parcela"
					           {STR0016,"EF_TIPO"			,"",00,00,""} ,; //"Tipo"
					           {STR0017,"EF_BENEF"		,"",00,00,""} }  //"Beneficente"
	
	Private cDelFunc := ".T." // Validacao para a exclusao. Pode-se utilizar ExecBlock
	Private aRotina := MenuDef()
	
	//��������������������������������������������������������������Ŀ
	//� Carrega fun��o Pergunte                                      �
	//����������������������������������������������������������������
	SetKey (VK_F12,{|a,b| AcessaPerg("FIN090",.T.)})
	Pergunte("FIN090",.F.)
	
	//�����������������������������������������������Ŀ
	//� Variaveis utilizadas para par�metros          �
	//� mv_par01	Mostra Lan� Contabil              �
	//� mv_par02   Aglutina Lancamentos               �
	//� mv_par03   Contabiliza On-Line                �
	//� mv_par04   Gera Cheque automaticamente        �
	//� mv_par05   Ctb Bordero - Total/Por Bordero    �
	//�������������������������������������������������
	
	dbSelectArea("SEF")
	SEF->(dbSetOrder(1))
	SEF->(DbGoTop())
	
	cFilterBrw := "EF_FILIAL=='" +xFilial('SEF')+ "' .AND. EF_CART=='P'"  
	
	oBrowse := FWmBrowse():New()
	oBrowse:SetAlias("SEF")
	oBrowse:SetFields(aCampos)
	
	oBrowse:AddLegend('EF_STATUS == "00" .AND. EMPTY(EF_RECONC) .AND. EF_LIBER <> "N"'	,'BR_BRANCO'	,STR0022) //"N�o Usado"
	oBrowse:AddLegend('EF_STATUS == "01" .AND. EMPTY(EF_RECONC)'						,'BR_AZUL'		,STR0023) //"Em Carteira"		
	oBrowse:AddLegend('EF_STATUS == "02" .AND. EMPTY(EF_RECONC)'						,'BR_AMARELO'	,STR0024) //"Pagamento Vinculado"
	oBrowse:AddLegend('EF_STATUS == "03" .AND. EMPTY(EF_RECONC)'						,'BR_LARANJA'	,STR0025) //"Emitido"
	oBrowse:AddLegend('EF_STATUS == "04" .AND. EMPTY(EF_RECONC)'						,'BR_VERDE'		,STR0026) //"Liquidado"
	oBrowse:AddLegend('EF_STATUS == "05" .AND. EMPTY(EF_RECONC)'						,'BR_PRETO'		,STR0027) //"Anulado"		
	oBrowse:AddLegend('EF_STATUS == "06" .AND. EMPTY(EF_RECONC)'						,'BR_CINZA'		,STR0028) //"Substitu�do"
	oBrowse:AddLegend('EF_STATUS == "07" .AND. EMPTY(EF_RECONC)'						,'BR_VERMELHO'	,STR0029) //"Devolvido"
	oBrowse:AddLegend('EF_LIBER == "N"'													,'BR_VIOLETA'	,STR0038) //"Bloqueado"			
	oBrowse:AddLegend('EF_RECONC == "x"'												,'LIGHTBLU'		,STR0031) //"Conciliado"	
	
	If lFiltraBco
		SEF->(dbSetOrder(8))
		If A095Bco(@cFiltraBco)
			oBrowse:SetFilterDefault(cFilterBrw + " .And. " + cFiltraBco)
		EndIf
	Else
		oBrowse:SetFilterDefault(cFilterBrw)
	EndIf
	
	oBrowse:Activate()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �A095Compes� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Compensar Cheques, execuntado a fun��o fA090Aut().		   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � lExpL := A095Compes(cAlias,nReg,nOpcx)					   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � L�gico       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Compes(cAlias,nReg,nOpcx,lAutomato)
Local lRet			:= .T.
Local aAreaSFE		:= SFE->(GetArea())
Local aPergs		:= {}

Private nModo		:= 1
Private cMarca		:= GetMark()
Private cPadrao		:= "530"
Private cCtBaixa	:= GetMv("MV_CTBAIXA")
Private bFilBrw		:= {|| }

Default lAutomato 	:= .F. //para acceso por script autom�tico

If SEF->EF_LIBER == 'N'
  IF !lAutomato 
	MSGALERT(STR0094) //"O cheque n�o pode ser alterado pois pertence a um tal�o que est� bloqueado."
  Else 
	Help( " ", 1, "Help",,"El cheque no puede modificarse pues pertenece a un talonario que esta bloqueado.", 1 )
  Endif
Else
		SX5->(dbSetOrder(1))
		SX5->(dbSeek(xFilial("SX5")+"09FIN"))
		cLote := IIF(SX5->(Found()),AllTrim(SX5->(X5DESCRI())),"FIN")
EndIf


//��������������������������������������������������������������Ŀ
//� Carrega fun��o Pergunte                                      �
//����������������������������������������������������������������
SetKey (VK_F12,{|a,b| AcessaPerg("FIN090",.T.)})
Pergunte("FIN090",.F.)
lRet:=fA090Aut("SE2")

SetKey (VK_F12,NIL)
RestArea(aAreaSFE)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � A095Fluxo� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Consultar a rotina de Fluxo de Caixa.					   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � A095Fluxo(cAlias,nReg,nOpcx)								   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Null		       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Fluxo(cAlias,nReg,nOpcx)
Local lRet		:= .T.
Local aAreaSFE	:= SFE->(GetArea())

lRet := FINC021()                                  

RestArea(aAreaSFE)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �A095Emitir� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Imprimir cheques, execuntado o programa FINR480.			   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � A095Emitir(cAlias,nReg,nOpcx)							   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Null       											       ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Emitir(cAlias,nReg,nOpcx)
Local lRet		:= .T.
Local aAreaSFE	:=	SFE->(GetArea()) 

Private nModo	:= 1

lRet := FINR480()   
RestArea(aAreaSFE) 
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �A095Anular� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Anular cheques, execuntado a fun��o fA090Can().			   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � A095Anular(cAlias,nReg,nOpcx,lAnular)					   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Null		       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Anular(cAlias,nReg,nOpcx,lAnular,lAutomato)
	Local aAreaSFE		:= {}
	Local aAreaSE2		:= {}
	Local cKeySEF		:= ""
	Local aPergs     	:= {}
	Local aRegs			:= {}
	Local cNumChq		:= ""
	Local cCondAux		:= ""
	Local lRet		 	:= .F.
	Local aAreaSEF 		:= SEF->(GetARea())
	Local aArea			:= GetArea()
	Local lPeAltSEF 	:= ExistBlock("F095ALTSEF")
	
	Local aAcao := {}
	
	Private nModo	 	:=	2
	Private aMotivos 	:=	{}
	
	Default lAnular 	:= .T.
	Default lAutomato 	:= .F.
	
	aDados050 :={}
	
	If lAutomato
	   cCondicao			:= ""
	Endif
	
	If Type("cCondicao") <> "U"
		cCondAux := cCondicao
	EndIf
	
 If SEF->EF_LIBER == 'N'
	  IF !lAutomato 
		MSGALERT(STR0094) //"O cheque n�o pode ser alterado pois pertence a um tal�o que est� bloqueado.")
	  Else 
		Help( " ", 1, "Help",,"El cheque no puede modificarse pues pertenece a un talonario que esta bloqueado.", 1 )
	  Endif
	  Return()
  Endif
  
  If SEF->EF_RECONC == "x"
	  IF !lAutomato 
		MSGALERT(STR0119) //"O cheque n�o pode ser alterado pois pertence a um tal�o que est� bloqueado."
	  Else 
		Help( " ", 1, "Help",,"El cheque fue conciliado y por eso no puede anularse", 1 )
	  Endif
	  Return()
  Endif
		
	If SEF->EF_STATUS == '04'
		IF !lAnular
	   		IF !lAutomato 
				MSGALERT(STR0120) //"O cheque est� liquidado e por isso n�o pode ser substituido."
			Else 
				Help( " ", 1, "Help",,STR0120, 1 )
			Endif
	   		Return()
		Endif
	Endif
		
	SX5->(dbSetOrder(1))
	SX5->(dbSeek(xFilial("SX5")+"09FIN"))
	cLote := IIF(SX5->(Found()),AllTrim(SX5->(X5DESCRI())),"FIN")
	
	If SEF->EF_STATUS $ "00/01/02/03/04"
	
		//Controle de Concilia��o banc�ria
		If cPaisLoc $ "ARG"
	
			aAdd(aRegs,{SEF->EF_PREFIXO,SEF->EF_NUM,SEF->EF_PARCELA,SEF->EF_TIPO,SEF->EF_FORNECE,SEF->EF_LOJA,SEF->EF_BANCO,SEF->EF_AGENCIA,SEF->EF_CONTA})
	
			If F472VldConc(aRegs)
				MsgAlert(STR0107)
				If FunName() == "FINA095"
					SEF->(DbGoTop())
				EndIf
				Return
			EndIf
	
		EndIf
	
		If lAnular .And. !(SEF->EF_STATUS $ "04|00|01")  .And. FUNNAME() == "FINA095"
			If Empty(SEF->EF_ORDPAGO) .OR. ALLTRIM(SEF->EF_ORIGEM) == "FINA100"
				MsgAlert(STR0101) //"O cheque foi gerado pela movimenta��o banc�ria, e s� pode ser cancelado pela mesma rotina"
			Else
				MsgAlert(STR0082) //"Para anular um cheque que n�o tenha sido ainda compensado, voc� deve acessar a rotina de cancelamento da Ordem de Pago.
			EndIf
			SEF->(DbGoTop())
			Return		
		EndIf
	
		aAreaSFE	:=	SFE->(GetArea())
		aAreaSE2	:=	SE2->(GetArea())
		SX5->(DbSeek(xFilial("SX5")+"G0"))
		While !SX5->(Eof()) .and. SX5->X5_FILIAL==xFilial("SX5") .and. SX5->X5_TABELA=="G0"
			Aadd(aMotivos,Trim(SX5->X5_CHAVE)+" - "+Capital(Trim(SX5->(X5DESCRI()))))
			SX5->(DbSkip())
		Enddo
		//��������������������������������������������������������������Ŀ
		//� Carrega fun��o Pergunte                                      �
		//����������������������������������������������������������������
		SetKey (VK_F12,{|a,b| AcessaPerg("FIN090",.T.)})
		Pergunte("FIN090",.F.)
		SE2->(DbSetOrder(1))
		If !(cPaisLoc == "BRA")
			cNumChq	:= ""
			cNumChq	:= FA95NUMTIT(SEF->EF_NUM,SEF->EF_TITULO,SEF->EF_FORNECE, SEF->EF_LOJA,!Empty(SEF->EF_ORDPAGO),SEF->EF_BANCO,SEF->EF_CONTA,SEF->EF_AGENCIA)
			If SE2->(DbSeek(xFilial("SE2")+SEF->EF_PREFIXO+cNumChq))
				lRet:=fa090Can("SE2",SE2->(Recno()),nOpcx,aMotivos,lAnular,lAutomato)
			Else
				If SEF->EF_STATUS=="00" .AND. lAnular
					lRet:=fa090Can("SE2",SE2->(Recno()),nOpcx,aMotivos,lAnular,lAutomato)
				Endif
			Endif
			IF lPeAltSEF
				aAcao := {xFilial("SEF")+cNumChq,SEF->EF_FILIAL+SEF->EF_NUM} 
				ExecBlock( "F095ALTSEF",.F.,.F.,aAcao)
			EndIf
		Else
			If SE2->(DbSeek(xFilial("SE2")+SEF->EF_PREFIXO+SEF->EF_TITULO))
				cKeySEF:=SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA+SEF->EF_PREFIXO+SEF->EF_NUM+SEF->EF_TIPO+SUBSTR(SEF->EF_TITULO,1,TamSX3("E2_ORDPAGO")[1])
				While !SE2->(EOF()) .AND. (SE2->E2_ORDPAGO==SUBSTR(SEF->EF_TITULO,1,TamSX3("E2_ORDPAGO")[1]))
					If cKeySEF==(SE2->E2_BCOCHQ+SE2->E2_AGECHQ+SE2->E2_CTACHQ+SE2->E2_PREFIXO+SUBSTR(SE2->E2_NUMBCO,1,TamSX3("EF_NUM")[1])+SE2->E2_TIPO+SE2->E2_ORDPAGO)
						lRet:=.T.
						Exit
					Endif
					SE2->(DbSkip())
				Enddo
				If lRet
					lRet := fa090Can("SE2",SE2->(Recno()),nOpcx,aMotivos,lAnular)
				Endif
			Else
				If SEF->EF_STATUS=="00" .AND. lAnular
					lRet := fa090Can("SE2",SE2->(Recno()),nOpcx,aMotivos,lAnular)
				Endif
			Endif
		Endif
		SetKey (VK_F12,NIL)
		RestArea(aAreaSE2)
		RestArea(aAreaSFE)
	Elseif SEF->EF_STATUS=="05"
		MSGALERT(STR0020) //"Este cheque j� est� cancelado!"
	Elseif SEF->EF_STATUS=="06"
		MSGALERT(STR0021) //"Este cheque foi substituido!"
	ElseIf SEF->EF_STATUS == "07"
		MSGALERT(STR0089) //"Este cheque foi devolvido!"
	Endif
	If cPaisLoc $ "EQU|DOM|ARG" .AND. FUNNAME()=="FINA095"
		If !Empty(cCondAux)
			cCondicao := cCondAux
		EndIf
	EndIf
	RestArea(aAreaSEF)
	RestArea(aArea)
	
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �fA095Legen� Autor � Wagner Montenegro      � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Legenda do Controle de Cheques.							   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � fA095Legen(cAlias,nReg,nOpcx)							   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Null		       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function Fa095Legen(cAlias,nReg)

Local aLegenda := {	{"BR_BRANCO"	,STR0022},; //"N�o Usado" 			//01 
					{"BR_AZUL"		,STR0023},; //"Em Carteira" 		//02
					{"BR_AMARELO"	,STR0024},; //"Pagamento Vinculado"	//03
					{"BR_LARANJA"	,STR0025},; //"Emitido"   			//04 
					{"BR_VERDE"		,STR0026},; //"Liquidado"   		//05 
					{"BR_PRETO"		,STR0027},; //"Anulado"   	 		//06 
					{"BR_CINZA"		,STR0028},; //"Substitu�do"			//07 
					{"BR_VERMELHO"	,STR0029},; //"Devolvido" 	 		//08
					{"BR_VIOLETA"	,STR0038},; //"Bloqueado"			//09					 
					{"LIGHTBLU"		,STR0031}}  //"Conciliado"	  		//10 																												
        
BrwLegenda(STR0032,STR0033,aLegenda) //"Controle de Cheques", "Legenda"

Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � A095Bco	� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Compensar Cheques, execuntado a fun��o fA090Aut().		   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � A095Bco()												   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � Null		       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Bco()
Local nEspLarg	:= 0
Local nEspLin	:= 0
Local oDlg		:= Nil
Local oPanel	:= Nil
Local nOpca		:= 0
Local lRet		:= .F.
Local oBco380	:= Nil
Local oCBX		:= Nil

nEspLarg := 8
nEspLin  := 5
DEFINE MSDIALOG oDlg FROM 	143,145 TO 157,190 TITLE STR0006 //"Banco"
oDlg:lMaximized := .F.
oPanel := TPanel():New(00,00,'',oDlg,, .T., .T.,, ,00,00)
oPanel:Align := CONTROL_ALIGN_ALLCLIENT

@ 000+nEspLin,003+nEspLarg TO 073+nEspLin,163+nEspLarg OF oPanel  PIXEL
@ 011+nEspLin,010+nEspLarg SAY STR0006 SIZE 20, 7 OF oPanel PIXEL //"Banco:"
@ 009+nEspLin,045+nEspLarg MSGET cBco380 F3 "SA6" Picture "@!" ;
									Valid If((!Empty(cBco380)),CarregaSA6(@cBco380,@cAge380,@cCta380,.T.),Eval({||MsgAlert(STR0034),.F.})) ; //"Selecione o Banco."
									SIZE 17, 10 OF oPanel Hasbutton PIXEL

@ 026+nEspLin,010+nEspLarg SAY STR0007 SIZE 20, 7 OF oPanel PIXEL //"Agencia:"
@ 024+nEspLin,045+nEspLarg MSGET cAge380	Picture "@!" WHEN Empty(cBco380) .and. !Empty(cCta380) ;
									Valid If(nOpca<>0,CarregaSA6(@cBco380,@cAge380,,.T.),.T.) ;
									SIZE 32, 10 OF oPanel PIXEL
@ 026+nEspLin,085+nEspLarg SAY STR0008 SIZE 20, 7 OF oPanel PIXEL //"Conta:"
@ 024+nEspLin,105+nEspLarg MSGET cCta380	Picture PesqPict("SE8","E8_CONTA") WHEN Empty(cBco380) .and. !Empty(cAge380) ;
									Valid If(nOpca<>0,CarregaSA6(@cBco380,@cAge380,@cCta380,.T.),.T.) ;
									SIZE 47, 10 OF oPanel PIXEL

@ 042+nEspLin,010+nEspLarg SAY STR0035 SIZE 70, 07 OF oPanel PIXEL //"Selecionar Cheques : "

@ 040+nEspLin,065+nEspLarg COMBOBOX oCBX VAR cUsoCH ITEMS aUsoCH WHEN !Empty(cBco380) Valid AllwaysTrue() SIZE 50,50 OF oDlg PIXEL

@ 057+nEspLin,030+nEspLarg SAY STR0036 SIZE 20, 7 OF oPanel PIXEL //"De"
@ 056+nEspLin,045+nEspLarg MSGET dIniDt380 	Picture "99/99/99" When .T.;	//cUsoCH==aUsoCH[1] .and. !Empty(cBco380) ;
									VALID If(Empty(dFimDt380),If(Empty(dIniDt380), .F. , .T. ),If(!Empty(dIniDt380) .and. dIniDt380<=dFimDt380, .T. , .F. )) ;
									SIZE 45, 10 OF oPanel Hasbutton PIXEL

@ 058+nEspLin,095+nEspLarg SAY STR0037 SIZE 20, 7 OF oPanel PIXEL 	//"At�:"
@ 056+nEspLin,111+nEspLarg MSGET dFimDt380	Picture "99/99/99" When .T.;	//cUsoCH==aUsoCH[1] .and. !Empty(cBco380) ;
									VALID If(!Empty(dFimDt380) .and. dFimDt380 >= dIniDt380, .T. , .F.) ;
									SIZE 45, 10 OF oPanel Hasbutton PIXEL

DEFINE SBUTTON FROM 085, 120 TYPE 1 ENABLE ACTION (nOpca:=If(!Empty(cBco380),1,0),oDLg:End()) OF oPanel
DEFINE SBUTTON FROM 085, 150 TYPE 2 ENABLE ACTION oDlg:End() OF oPanel
ACTIVATE MSDIALOG oDlg CENTERED

If nOpca == 0 .Or. nOpca == 3
	oBrowse:DeleteFilter("FA095BCO")
	Return(lRet)
Endif

lRet		:=	.T.
cBcoDe		:= cBco380
cBcoAte		:= cBco380
dDataDe		:= dIniDt380
dDataAte	:= dFimDt380  

If StrZero(ASCAN(aUsoCH,cUsoCH),1) == "1"
	cCondicao := "(DTOS(EF_DATA) >= '" + DTOS(dIniDt380) + "' .AND. DTOS(EF_DATA) <= '" + DTOS(dFimDt380) + "' .AND. EF_STATUS <> '00') "
Else
	cCondicao := "(EF_STATUS == '00' .OR. (EF_STATUS == '05' .AND. EF_VALOR == 0)) "
Endif

cCondicao += ".AND. EF_BANCO == '" + cBco380 + "' "
cCondicao += ".AND. EF_AGENCIA == '" + cAge380 + "' "

cFiltroExt	:= cCondicao

If !Empty(oBrowse:cFilterDefault)
	oBrowse:DeleteFilter("FA095BCO")
	oBrowse:AddFilter(STR0058,cCondicao,.T.,.T.,"SEF",,,"FA095BCO")	
EndIf
SEF->(DbGoTop())
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � A095Talao� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Cadastra Novo Talon�rio de Cheques.						   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � lExpL := A095Talao(cAlias,nReg,nOpcx)					   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � L�gico       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095Talao(lAutomato)
Local aArea         := GetArea()
Local aCbxStatus	:= {STR0038,STR0039} //"Bloqueado","Desbloqueado"
Local aCbxTipo	:= {}
Local cCbxStatus 	:= "2"
Local cCbxTipo      := "1"
Local cChFin		:= Criavar("FRE_SEQFIM")
Local cChIni		:= Criavar("FRE_SEQINI")
Local nQtd			:= 0
Local cTalao		:= "" //:= GetSX8Num("SA6","A6_TALAO",(cBco380+cAge380+cCta380))
Local cObsTalao		:= Criavar("FRE_OBSERV")
Local cPrefixo		:=	Criavar("FRE_PREFIX")
Local oBco380
Local oAge380
Local oCta380
Local oChFin
Local oChIni
Local oQtd
Local oTalao
Local oPrefixo
Local oObsTalao
Local lRet			:=	.F.
Local nOpca			:=	0
Local nX
Local cTipo := " "
Local cBco380	:=Criavar("EF_BANCO")
Local cAge380	:=Criavar("EF_AGENCIA")
Local cCta380	:=Criavar("EF_CONTA")
Local bChave	:= Nil
Private _oDlg				// Dialog Principal
DEFAULT lAutomato   := .F.

If cPaisLoc $ "ARG"
	aCbxTipo := {STR0040,STR0109,STR0042,STR0108} //"Cheque Comum","Cheque Comum Eletr�nico","Cheque Diferido","Cheque Diferido Eletr�nico"
Else
	aCbxTipo := {STR0040,STR0041,STR0042} 			//"Cheque Comum","Cheque Eletr�nico","Cheque Diferido"
EndIf

bChave := {|| cPrefixo := Criavar("FRE_PREFIX"), cChIni := Criavar("FRE_SEQINI"), cChFin := Criavar("FRE_SEQFIM"), .T.}

If !lAutomato

	DEFINE MSDIALOG _oDlg TITLE STR0043 FROM 264,407 TO 513,858 PIXEL //"Cadastro de Talon�rio de Cheques"
	
	
	@ 004,064 Say	STR0007	Size	021,008 	PIXEL	OF	_oDlg //"Agencia"
	@ 004,129 Say	STR0008	Size	017,008  PIXEL	OF _oDlg //"Conta"
	@ 005,005 Say	STR0006	Size	017,008 	PIXEL	OF _oDlg //"Banco"
	@ 020,104 Say	STR0044	Size	040,008 	PIXEL OF _oDlg //"Tipo de Cheque"
	@ 021,005 Say	STR0045	Size	030,008 	PIXEL OF _oDlg //"Talon�rio n�"
	@ 036,005 Say	STR0046	Size	059,008  PIXEL OF _oDlg //"Quantidade de Cheques"
	@ 036,125 Say	STR0013	Size	018,008	PIXEL OF _oDlg //"Prefixo"
	@ 052,005 Say	STR0047	Size	028,008  PIXEL OF _oDlg //"Cheque de"
	@ 052,115 Say	STR0048	Size	029,008  PIXEL OF _oDlg //"Cheque at�"
	@ 067,007 Say	STR0049	Size	031,008	PIXEL OF _oDlg //"Observa��o"
	@ 084,006 Say	STR0050	Size	023,008  PIXEL OF _oDlg //"Situa��o"
	
	@ 004,028 MsGet	oBco380		Var	 cBco380	F3 "SA6" Picture "@S4"  Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380) When If(Empty(cTalao),.T.,Eval({||cTalao:="",RollBackSX8(),.T.}))	Size	026,009	 PIXEL	OF	_oDlg
	@ 004,092 MsGet	oAge380		Var	 cAge380	         Picture "@S5"	Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380) When .T.	Size	025,009	 PIXEL	OF	_oDlg
	@ 004,153 MsGet	oCta380		Var	 cCta380		     Picture "@S10" Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380) .and. fa095VTalao(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,@cTalao) When Eval({||cTalao:=GetSX8Num("SA6","A6_TALAO",(LTrim(cBco380)+LTrim(cAge380)+LTrim(cCta380))),.T.})   Size	    060,009	 PIXEL	OF	_oDlg
	@ 020,044 MsGet	oTalao		Var cTalao		  	     Picture "@!"   Valid fa095VTalao(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,cTalao) When .F.	Size	041,009  PIXEL  OF _oDlg
	
	@ 020,152 ComboBox	cCbxTipo		Items aCbxTipo		Size	071,010	PIXEL OF _oDlg;
			VALID fa095VTipo(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,cTalao)
	
	@ 036,073 MsGet		oQtd		Var nQtd		Picture	PesqPict("FRE","FRE_QTDCHE") When (nQtd == 0) Size	040,009 PIXEL OF _oDlg  
	@ 036,151 MsGet		oPrefixo	Var cPrefixo	Picture	PesqPict("FRE","FRE_PREFIX") When (nQtd > 0)	Valid A095VldPrx(cPrefixo)	Size	060,009	PIXEL OF _oDlg
	
	@ 052,041 MsGet		oChIni		Var cChIni		Picture	PesqPict("FRE","FRE_SEQINI") When (nQtd > 0);
									VALID lCkTalao(cBco380,cAge380,cCta380,cPrefixo,nQtd,cChIni) .and. If( !Empty(cChIni), Eval({||cChIni:=StrZero( Val(cChIni), Len(FRE->FRE_SEQINI) ),cChFin:=StrZero( (Val(cChIni)+nQtd-1), Len(FRE->FRE_SEQFIM) ),.T.}),Eval({||MsgAlert(STR0053),.F.}) ) .And. Iif(A095VldPrx(cPrefixo,cChIni,cChFin),.T.,Eval(bChave));
									SIZE 070,009 PIXEL OF _oDlg
	
	@ 052,152 MsGet		oChFin		Var cChFin		When Empty(cBco380)	Size 070,009  PIXEL OF _oDlg
	
	@ 067,041 MsGet		oObsTalao	Var	cObsTalao	Size	171,009	PIXEL OF _oDlg
	
	@ 084,041 ComboBox	cCbxStatus	Items	aCbxStatus	Size	051,010	PIXEL OF _oDlg
	
	DEFINE SBUTTON FROM 102, 127 TYPE 1 ENABLE ACTION (nOpca:=If(!Empty(cBco380).and.nQtd>0,1,0),If(nQtd>0,_oDLg:End(),MsgAlert(STR0051))) //"Informe a quantidade de cheques do talon�rio."
	DEFINE SBUTTON FROM 102, 175 TYPE 2 ENABLE ACTION _oDlg:End()
	
	ACTIVATE MSDIALOG _oDlg CENTERED

Else  //para ejecuci�n de script de teste
		If FindFunction("GetParAuto")
			aRetAuto 		:= GetParAuto("FINA095TESTCASE")
			cBco380 		:= aRetAuto[1]
			cAge380 		:= aRetAuto[2]
			cCta380 		:= aRetAuto[3]
			cTalao			:= aRetAuto[4]
			nQtd			:= aRetAuto[5]
			cPrefixo		:= aRetAuto[6]
			cChIni			:= aRetAuto[7]
			cChFin			:= aRetAuto[8]
			cObsTalao		:= aRetAuto[9]
			cCbxTipo		:= aRetAuto[10]
			cCbxStatus		:= aRetAuto[11]
		Endif
		fa095VTalao(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,@cTalao,lAutomato)
		nOpca:=1	
	Endif


If nOpca == 0 .Or. nOpca == 3
	RollBackSX8()
	Return(lRet)
Endif
If nOpca == 1
	If !Empty(cBco380) .And. !Empty(cAge380) .and.!Empty(cCta380) .And. nQtd > 0 .And. !Empty (cPrefixo) .And. !Empty (cChIni)
		cTipo := StrZero(ASCAN(aCbxTipo,cCbxTipo),1)
		FRE->(dbSetOrder(1))
		If ! FRE->(dbSeek(xFilial("FRE")+cBco380+cAge380+cCta380+cTipo+cTalao))
			BEGIN TRANSACTION
			RecLock("FRE",.T.)
			FRE->FRE_FILIAL	:=	xFilial("FRE")
			FRE->FRE_BANCO	:=	cBco380
			FRE->FRE_AGENCI :=	cAge380
			FRE->FRE_CONTA	:=	cCta380
			FRE->FRE_TIPO	:=	cTipo
			FRE->FRE_TALAO	:= cTalao
			FRE->FRE_PREFIX	:=	cPrefixo
			FRE->FRE_SEQINI	:=	cChIni
			FRE->FRE_SEQFIM	:=	cChFin
			FRE->FRE_DATA	:= dDataBase
			FRE->FRE_QTDCHE	:=	nQtd
			FRE->FRE_STATUS	:=	StrZero(ASCAN(aCbxStatus,cCbxStatus),1)
			FRE->FRE_OBSERV	:= cObsTalao
			FRE->(MSUnlock())
			SA6->(DbSetOrder(1))
			SA6->(DbSeek(xFilial("SA6")+cBco380+cAge380+cCta380))
			RecLock("SA6",.F.)
			SA6->A6_TALAO	:=	cTalao
			SA6->(MSUnlock())
			For nX := Val(cChIni) to Val(cChFin)
				RecLock("SEF",.T.)
				SEF->EF_FILIAL		:=	xFilial("SEF")
				SEF->EF_BANCO		:=	FRE->FRE_BANCO
				SEF->EF_AGENCIA	:=	FRE->FRE_AGENCI
				SEF->EF_CONTA		:=	FRE->FRE_CONTA
				SEF->EF_NUM			:=	StrZero(nX,TamSX3("FRE_SEQFIM")[1])
				SEF->EF_TALAO		:=	FRE->FRE_TALAO
				SEF->EF_CART		:=	"P"
				SEF->EF_PREFIXO	:=	FRE->FRE_PREFIX
				SEF->EF_TIPO		:=	"CH"
				SEF->EF_LIBER		:=	IF(FRE->FRE_STATUS=="1","N","S")
				SEF->EF_ORIGEM		:=	"FINA095"
				SEF->EF_STATUS		:=	"00"
				SEF->(MSUnlock())
			Next nX
			ConfirmSX8()
			END TRANSACTION
		EndIf
	Else
	  If !lAutomato
		Alert(STR0102 + CRLF + STR0103) //conta, quantidade de cheques ou prefixo digitado incorretamnte
	  Else
		Help(STR0102 + " " + STR0103)
	  Endif	
		RollBackSX8()
		lRet := .F.
	EndIf
EndIf
RestArea(aArea)
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � lCkTalao	� Autor � Wagner Montenegro      � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Consiste prefixo e numera��o de cheques existentes para o   ��
���          � Talon�rio.                                                  ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � lExpL:=A095Talao(cBco380,cCta380,cPrefixo, nSeq,nQtd,cChIni)��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � L�gico       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function lCkTalao(cBco380,cAge380,cCta380,cPrefixo,nQtd,cChIni)
Local aAreaFRE	:=	GetArea()
Local lRet		:=	.T.
Local nDif		:=	0
FRE->(DbSetOrder(2))
IF FRE->(Dbseek(xFilial("FRE")+cBco380+cAge380+cCta380+cPrefixo))
	nDif	:=	Val(cChIni)-Val(FRE->FRE_SEQFIM)
	While !FRE->(EOF()) .and. (FRE->FRE_BANCO==cBco380 .and. FRE->FRE_AGENCI==cAge380 .and. FRE->FRE_CONTA==cCta380 .and. FRE->FRE_PREFIX==cPrefixo) .and. lRet
	   If Val(FRE->FRE_SEQFIM) >= Val(cChIni).and. cPaisLoc <> "ARG"
  			MsgAlert(STR0055) //"O numero de cheque informado j� existe."
   			lRet	:=	.F.
   		Endif
   		nDif	:=	Val(cChIni)-Val(FRE->FRE_SEQFIM)
	   FRE->(DbSkip())
	Enddo
	If lRet .and. cPaisLoc <> "ARG"
		If nDif > 1
	   	MsgAlert(STR0056) //"O numero do cheque est� fora da sequencia."
   	 	lRet	:=	.F.
	   Endif
	Endif
Endif
FRE->(RestArea(aAreaFRE))
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �A095FilSEF� Autor � Wagner Montenegro	     � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Consistir o filtro de cheques usado e n�o usados.		   ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � lExpL:=A095FilSEF(cCbx)									   ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � L�gico       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador.										   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095FilSEF(cCbx)
Local lRet := .F.
IF EF_FILIAL==xFilial('SEF') .AND. EF_CART=='P' .AND. EF_BANCO==cBco380 .AND. EF_AGENCIA==cAge380 .AND. EF_CONTA==cCta380
	If cCbx=="1"
		If DTOS(EF_DATA) >= DTOS(dIniDt380) .AND. DTOS(EF_DATA) <= DTOS(dFimDt380) .AND. EF_STATUS<>"00"
			lRet:=.T.
		Endif
	Else
		If EF_STATUS=="00" .OR. (EF_STATUS == "05" .AND. EF_VALOR==0)
			lRet:=.T.
		Endif
	Endif
Endif
Return(lRet)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � MenuDef()� Autor � Wagner Montenegro     � Data �22/11/06  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �	  1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()

	Local aRotina := {}
	
	aRotina :=	{{STR0057, 'AxPesqui()'			, 0, 1 },;		//"Buscar"
				{ STR0059, 'A095Fluxo()'		, 0, 4 },; 		//"Fluxo de Caixa"
				{ STR0058, 'A095Bco()'			, 0, 3 },; 		//"Parametros"
				{ STR0064, 'A095Emitir()'		, 0, 6 },;		//"Emitir"
				{ STR0060, 'A095Compes()'		, 0, 4 },; 		//"Liquidar"
				{ STR0062, 'A095Anular(,,,.T.)'	, 0, 5 },;		//"Anular"
				{ STR0083, 'A095Anular(,,,.F.)'	, 0, 7 },;		//"Substituir"
				{ STR0100, 'fA095Devol'			, 0, 4 },;		//"Hist�rico do cheque"
				{ STR0080, 'A095Talao()'		, 0, 3 },;		//"Talon�rio"
				{ STR0090, 'fA095Bloq()'		, 0, 8, ,.F.},;	//"Bloquear/Desbloquear"
				{ STR0065, 'fA095Legen(,1)'		, 0, 6, ,.F.}}	//"Legenda"

Return (aRotina)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � fa095VTalao� Autor � Lucas               � Data � 15/12/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validar a existencia do Talonario de Cheques.              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � fa095VTalao(cBco380,cAge380,cCta380,cCbxTipo,cTalao)       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Gen�rico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function fa095VTalao(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,cTalao,lAutomato)
Local aArea := GetArea()
Local cTipo := StrZero(ASCAN(aCbxTipo,cCbxTipo),1)
Local lRet  := .T.
LOcal cTalaoAnt:=cTalao
Local lExiste:=.t.

DEFAULT lAutomato   := .F.

FRE->(dbSetOrder(1))
If FRE->(dbSeek(xFilial("FRE")+cBco380+cAge380+cCta380+cTipo+cTalao))
	cTalao:=GetSX8Num("SA6","A6_TALAO",(LTrim(cBco380)+LTrim(cAge380)+LTrim(cCta380)))
	While lExiste
		If FRE->(dbSeek(xFilial("FRE")+cBco380+cAge380+cCta380+cTipo+cTalao))
			cTalao:=GetSX8Num("SA6","A6_TALAO",(LTrim(cBco380)+LTrim(cAge380)+LTrim(cCta380)))
		Else
			lExiste:=.F.
		EndIf
	EndDo	
	
EndIf
If cTalaoAnt<>cTalao
  If !lAutomato
	MsgAlert(STR0115 +cTalaoAnt + STR0116 + cTalao,STR0080)
  Else
	Conout( STR0115 +cTalaoAnt + STR0116 + cTalao) 
  Endif
EndIf
RestArea(aArea)
Return lRet

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �fa095VTipo� Autor � Lucas                 � Data � 15/12/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validar a existencia do Talonario de Cheques com o mesmo   ���
���          � Tipo.                                                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �fa095VTipo(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,cTalao)���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Gen�rico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function fa095VTipo(cBco380,cAge380,cCta380,aCbxTipo,cCbxTipo,cTalao)
Local aArea := GetArea()
Local cTipo := StrZero(ASCAN(aCbxTipo,cCbxTipo),1)
Local lRet  := .T.

FRE->(dbSetOrder(1))
If FRE->(dbSeek(xFilial("FRE")+cBco380+cAge380+cCta380+cTipo+cTalao))
	MsgAlert(STR0087,STR0086)
	lRet := .F.
EndIf

RestArea(aArea)
Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �IsCxLoja  �Autor  �Totvs		         � Data �  02/12/10   ���
�������������������������������������������������������������������������͹��
���Desc.     � Verifica Caixas                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � FINA095                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function IsCxLoja()
LOCAL cStringCX :=""
LOCAL cAlias := Alias()

dbSelectArea("SX5")
dbSeek(xFilial("SX5") + "23")

While !Eof() .and. X5_TABELA == "23"
	cStringCX+=Substr(X5_CHAVE,1,3)
	dbSkip()
	cStringCX+="/"
Enddo

dbSelectArea(cAlias)
Return cStringCX

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 � FA095Bloq� Autor � Rodrigo Gimenes        � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �                                                             ��
���          �                                                             ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 �                                                             ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 �              											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Rep�blica Dominicana							   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function FA095Bloq()

Local aArea         := GetArea()
Local oBco380
Local oAge380
Local oCta380
Local oTalao
Local aCbxStatus	:= {STR0038,STR0039} //"Bloqueado","Desbloqueado"

Local lRet			:=	.F.
Local nOpca			:=	0
Local nX
Local cTipo 	:= Criavar("FRE_TIPO")
Local cBco380	:= Criavar("FRE_BANCO")
Local cAge380	:= Criavar("FRE_AGENCIA")
Local cCta380	:= Criavar("FRE_CONTA")
Local cTalao	:= Criavar("FRE_TALAO")
Local cFreStatus:= '1'

Private cStatus := ''
Private _oDlg				// Dialog Principal

DEFINE MSDIALOG _oDlg FROM	31,15 TO 240,300 TITLE  STR0092 PIXEL OF oMainWnd //  ""Bloquear/Desbloquear Tal�o"
@ 01, 002 TO 102, 142 LABEL  STR0091 OF _oDlg  PIXEL // ""Dados do Tal�o"
@ 012, 007 SAY  STR0045		SIZE 30,07 OF _oDlg PIXEL //"Fornec."
@ 010, 050 MSGET oTalao		Var	cTalao	F3 "FRE" Picture "@S8"  Size	041,009  PIXEL  OF _oDlg
@ 024, 007 SAY  STR0006		SIZE 40,07 OF _oDlg PIXEL //"Banco"
@ 022, 050 MSGET oBco380	Var	cBco380	Picture "@S4"  Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380)  When .F.SIZE 22,10 OF _oDlg PIXEL
@ 036, 007 SAY  STR0007		  	SIZE 39,07 OF _oDlg PIXEL //"Ag�ncia"
@ 034, 050 MSGET oAge380 Var	cAge380	         Picture "@S5"	Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380) When .F.	SIZE 35,10 OF _oDlg PIXEL
@ 048, 007 SAY  STR0008			SIZE 41,07 OF _oDlg PIXEL //"Conta"
@ 046, 050 MSGET oCta380 Var	cCta380 Picture "@S10" Valid CarregaSa6(@cBco380,@cAge380,@cCta380,.F.) .and. ExistCpo("SA6",cBco380+cAge380+cCta380) When .F.	SIZE 66,10 OF _oDlg PIXEL
@ 750, 750 MSGET oTipo Var	cTipo Picture "@!" SIZE 66,10 OF _oDlg PIXEL
@ 060, 007 SAY  STR0050		SIZE 41,07 OF _oDlg PIXEL //"Situa��o"
@ 058, 050 ComboBox	cStatus	Items	aCbxStatus	Size	051,010	PIXEL OF _oDlg


DEFINE SBUTTON FROM 85, 75 TYPE 1 ENABLE OF _oDlg ACTION (nOpca:=If(!Empty(cBco380),1,0),_oDLg:End())
DEFINE SBUTTON FROM 85, 105 TYPE 2 ENABLE OF _oDlg ACTION _oDlg:End()
ACTIVATE MSDIALOG _oDlg CENTERED

If nOpca == 0 .Or. nOpca == 3
	Return(lRet)
Endif
If nOpca == 1
	cFreStatus := StrZero(ASCAN(aCbxStatus,cStatus),1)
	FRE->(dbSetOrder(1))
	If FRE->(dbSeek(xFilial("FRE")+cBco380+cAge380+cCta380+cTipo+cTalao))
		If cFreStatus == '1' .And. fA095ChUsa(cBco380,cAge380,cCta380,cTalao,cFreStatus)
			MSGALERT(STR0093) // "O Tal�o n�o pode ser bloqueado pois existem cheques j� utilizados."  
		ElseIf cFreStatus == '2' .And. fA095ChUsa(cBco380,cAge380,cCta380,cTalao,cFreStatus)
			MSGALERT(STR0110) // "O Tal�o selecionado j� est� desbloqueado."
		Else
			BEGIN TRANSACTION
			RecLock("FRE",.F.)
			FRE->FRE_STATUS	:= 	cFreStatus
			FRE->(MSUnlock())
			DbSelectArea("SEF")
			SEF->(dbSetOrder(10))
			If SEF->(dbSeek(xFilial("SEF")+cBco380+cAge380+cCta380+cTalao))
				While !SEF->(EOF()) .AND. (SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA+SEF->EF_TALAO == cBco380+cAge380+cCta380+cTalao)
					RecLock("SEF",.F.)
					SEF->EF_LIBER :=	IF(cFreStatus == '1',"N","S")
					SEF->(MSUnlock())
					SEF->(DbSkip())
				EndDo
			EndIf
			MSGALERT(STR0045 + " " + cTalao + " " + IF(cFreStatus == '1',STR0038,STR0039)) // "Tal�o " + cTalao + Bloqueado\Desbloqueado
			END TRANSACTION
		EndIf
	EndIf
EndIf
RestArea(aArea)
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �fA095ChUsa� Autor � Rodrigo Gimenes 	     � Data � 05.07.11 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o � Verifica se existe algum cheque utilizado para o tal�o      ��
���          �                                                             ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 � lExpL:=fA095ChUsa(cBco,cAge,cCta,cTalao)                    ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 � L�gico       											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Equador\Rep�blica Dominicana.				   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fA095ChUsa(cBco,cAge,cCta,cTalao)
Local aArea	:= GetArea()
Local lRet	:= .F.
Local cChaveFRE     := "" 

SEF->(dbSetOrder(10))          
If SEF->(dbSeek(xFilial("SEF")+cBco+cAge+cCta+cTalao))
	cChaveFRE:=(xFilial("SEF")+cBco+cAge+cCta+cTalao)
	While !SEF->(EOF()) .and. cChaveFRE== xFilial("SEF")+SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA+SEF->EF_TALAO
		If Alltrim(cStatus) $ "Bloqueado"
			If SEF->EF_STATUS != "00"
				lRet := .T.
				Exit
			EndIf
		Else
			If SEF->EF_LIBER == "S" .And. SEF->EF_STATUS == "00" 
				lRet := .T.
				Exit
			EndIf		
		EndIf	
		SEF->(DbSkip())
	EndDo
EndIf
RestArea(aArea)

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ�
���Fun��o	 �fA095Devol| Autor � Rodrigo Gimenes        � Data � 30.09.10 ��
��������������������������������������������������������������������������Ĵ�
���Descri��o �                                                             ��
���          �                                                             ��
��������������������������������������������������������������������������Ĵ�
���Sintaxe	 �                                                             ��
��������������������������������������������������������������������������Ĵ�
���Retorno	 �              											   ��
��������������������������������������������������������������������������Ĵ�
���Uso		 � Localiza��o Rep�blica Dominicana							   ��
���������������������������������������������������������������������������ٱ
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function fA095Devol()

Local aArea         := GetArea()
Local aDevolu	:= {}
Local nPosicao	:= 1
Local oDlg				// Dialog Principal
Local oListBox
Local oPanel
Local nTamFRF:=TamSX3("FRF_NUM")[1]

	dbSelectArea("FRF")
	FRF->(dbSetOrder(1))
	FRF->(dbSeek(xFilial("FRF")+SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA+SEF->EF_PREFIXO+Subs(SEF->EF_NUM,1,nTamFRF)   ))

	While !FRF->(Eof()) .And. xFilial("FRF")  == FRF->FRF_FILIAL .And. FRF->FRF_BANCO == SEF->EF_BANCO .And. FRF->FRF_AGENCIA == SEF->EF_AGENCIA;
	  				.And. FRF->FRF_CONTA == SEF->EF_CONTA ;
	  				.And. FRF->FRF_NUM == Subs(SEF->EF_NUM,1,nTamFRF)

		aAdd(aDevolu,{FRF->FRF_NUM,FRF->FRF_DATDEV,FRF->FRF_DATPAG,FRF->FRF_MOTIVO,Lower(FRF->FRF_DESCRI),If(EMPTY(FRF->FRF_FORNEC),SEF->EF_FORNECE,FRF->FRF_FORNEC),If(EMPTY(FRF->FRF_LOJA),SEF->EF_LOJA,FRF->FRF_LOJA),FRF->FRF_ESPDOC,FRF->FRF_SERDOC,If(EMPTY(FRF->FRF_NUMDOC),SEF->EF_TITULO,FRF->FRF_NUMDOC),FRF->FRF_ITDOC,FRF->(Recno())})

		dbSelectArea("FRF")
		FRF->(dbSkip())
	EndDo

	If Len( aDevolu ) == 0
		Aviso( "",STR0099 , {"Ok"} ) //"N�o existem dados a consultar"
		Return
	Endif

	Asort(aDevolu,,,{|x,y| x[12]<y[12]})

	DEFINE MSDIALOG oDlg FROM 0, 0 TO 300,750 PIXEL TITLE OemToAnsi(STR0095) //"Hist�rico de Compensa��es e Devolu��es"
	oPanel := TPanel():New(0,0,'',oDlg,, .T., .T.,, ,370,80,.T.,.T. )
	oPanel:Align := CONTROL_ALIGN_BOTTOM
	oPanel:nHeight := 30
	oListBox := TCBrowse():New(0,0,10,10,,,,oDlg,,,,,,,,,,,,,,.T.,,,,.T.,)
	oListBox:AddColumn(TCColumn():New(STR0069,{||aDevolu[oListBox:nAt,1]},,,,,030,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(STR0096,{||If(!Empty(aDevolu[oListBox:nAt,2]),aDevolu[oListBox:nAt,2],"")},,,,,030,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(STR0097,{||If(!Empty(aDevolu[oListBox:nAt,3]),aDevolu[oListBox:nAt,3],"")},,,,,030,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(STR0068,{||aDevolu[oListBox:nAt,4]},,,,,015,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(STR0098,{||aDevolu[oListBox:nAt,5]},,,,,020,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(AllTrim(SF1->(RetTitle("F1_FORNECE"))),{||aDevolu[oListBox:nAt,6]},,,,,020,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(AllTrim(SF1->(RetTitle("F1_LOJA"))),{||aDevolu[oListBox:nAt,7]},,,,,020,.F.,.F.,,,,,))
	oListBox:AddColumn(TCColumn():New(AllTrim(SF1->(RetTitle("F1_DOC"))),{|| If(!Empty(aDevolu[oListBox:nAt,10]),AllTrim(aDevolu[oListBox:nAt,8]) + "  -  " + AllTrim(aDevolu[oListBox:nAt,9]) + " / " + AllTrim(aDevolu[oListBox:nAt,10]) + " - " + AllTrim(aDevolu[oListBox:nAt,11]),"")},,,,,030,.F.,.F.,,,,,))
	oListBox:SetArray( aDevolu)
	oListBox:Align := CONTROL_ALIGN_ALLCLIENT
	DEFINE SBUTTON FROM 02,330 PIXEL TYPE 1 ACTION oDLg:End() ENABLE OF oPanel
	ACTIVATE MSDIALOG oDlg CENTERED

	lRet := .T.

RestArea(aArea)
Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FINA095   �Autor  �Microsiga           �Fecha � 10/12/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Monta o cabecalho e o gride de itens da nota de debito com ���
���          � os dados do fornecedor e dos cheques.                      ���
���          � Esta funcao e chamada na montagem da tela pela LOCXNF,     ���
���          � montando-se um bloco de codigo como mostrado abaixo:       ���
���          � bFunAuto := {|| A095DadosND(SEF->EF_FORNECE,SEF->EF_LOJA,  ���
���          � ,{SEF->(Recno())},"SEF","EF_VALOR",.T.,.T.)}               ���
���          � Esta variavel deve ser inicializada antes da chamada a     ���
���          � Parametros:                                                ���
���          � cFornece		codigo do fornecedor                          ���
���          � cLoja    	filial do fornecedor                          ���
���          � aItens     	lista dos registros a serem considerados para ���
���          �            	a nd                                          ���
���          � cAlias      	tabela da lista de registros                  ���
���          � cCpoVal     	nome do campo que contem o valor do item      ���
���          � lLinhas		indica se poderao ser incluidos outros itens  ���
���          �             	na nd                                         ���
���          � lTitulo		indica se os TES para os itens deverao gerar  ���
���          �          	titulos no financeiro                         ���
�������������������������������������������������������������������������͹��
���Uso       � FINA095 - geracao de notas de debito para fornecedores     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095DadosND(cFornece,cLoja,aItens,cAlias,cCpoVal,lLinhas,lTitulo,lVldImpFor)
Local nItem			:= 0
Local nPosQtd		:= 0
Local nPosVlrUn		:= 0
Local nPosTotal		:= 0
Local nPosTES		:= 0
Local cValidacao	:= ""
Local bValid		:= {|| }
Local cParCOD := GETMV("MV_FINPDGB", , "RG498")
Local cParTES := GETMV("MV_FINTEGB", , "498")
Local nPosCOD := 0
Local nParUM := ""
Local nParLoc := ""
Local nParCFO := ""

Default cFornece	:= ""
Default cLoja		:= ""
Default aItens		:= {}
Default cAlias		:= "SEF"
Default cCpoVal		:= "EF_VALOR"
Default lTitulo		:= .T.
Default lLinhas		:= .T.
Default lVldImpFor	:= .F.

If !Empty(aItens)
	/* inicializa os dados do cebecalho da nota */
	M->F1_FORNECE := cFornece
	M->F1_LOJA := cLoja
	/* impede a edicao do fornecedor */
	If Type("__aoGets")!= "U" .And. ValType(__aoGets)=="A"
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_FORNECE"})
		If nItem > 0
			__aoGets[nItem]:bWhen := {|| .F.}
		Endif
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_LOJA"})
		If nItem > 0
			__aoGets[nItem]:bWhen := {|| .F.}
		Endif
	Endif
	/*
	inicializa os dados dos itens */
	nPosCOD   := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_COD"})
	nParUM    := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_UM"})
	nParLoc   := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_LOCAL"})
	nPosQtd   := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_QUANT"})
	nPosVlrUn := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_VUNIT"})
	nPosTotal := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_TOTAL"})
	nPosTES   := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_TES"})
	nParCFO   := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_CF"})
	aCols := {}
	For nItem := 1 To Len(aItens)
		If aItens[nItem] > 0
			(cAlias)->(DbGoTo(aItens[nItem]))
			oGetDados:AddLine()
			aCols[nItem,nPosQtd] := 1
			If FunName() == "FINA100"
				SB1->(DbSetOrder(RetOrder("SB1","B1_FILIAL+B1_COD")))
				SB1->(MsSeek(xFilial("SB1") + cParCOD))
				aCols[nItem,nPosCOD] := cParCOD
				aCols[nItem,nParUM] := SB1->B1_UM
				aCols[nItem,nParLoc] := SB1->B1_LOCPAD
				
				SF4->(DbSetOrder(RetOrder("SF4","F4_FILIAL+F4_CODIGO")))
				SF4->(MsSeek(xFilial("SF4") + cParTES))
				aCols[nItem,nPosTES] := cParTES
				aCols[nItem,nParCFO] := SF4->F4_CF
			EndIf
			aCols[nItem,nPosVlrUn] := (cAlias)->&cCpoVal
			aCols[nItem,nPosTotal] := (cAlias)->&cCpoVal
		Endif
	Next
	nItem--		//contem a quantidade de documentos devolvidos
	cValidacao := "(n>=" + AllTrim(Str(nItem)) + ")"
	/*
	altera a validacao da quantidade para nao permitir sua alteracao quando o item for um documento devolvido */
	If Empty(aHeader[nPosQtd,6])
		aHeader[nPosQtd,6] := cValidacao
	Else
		aHeader[nPosQtd,6] := cValidacao + " .And. " + aHeader[nPosQtd,6]
	Endif
	/*
	altera a validacao do valor para nao permitir sua alteracao quando o item for um documento devolvido */
	If Empty(aHeader[nPosVlrUn,6])
		aHeader[nPosVlrUn,6] := cValidacao
	Else
		aHeader[nPosVlrUn,6] := cValidacao + " .And. " + aHeader[nPosVlrUn,6]
	Endif
	/*
	altera a validacao do valor para nao permitir sua alteracao quando o item for um documento devolvido */
	If Empty(aHeader[nPosTotal,6])
		aHeader[nPosTotal,6] := cValidacao
	Else
		aHeader[nPosTotal,6] := cValidacao + " .And. " + aHeader[nPosTotal,6]
	Endif
	/*
	nao permite TES que atualizem estoque e/ou que nao gerem titulos*/
	If Empty(aHeader[nPosTES,6])
		aHeader[nPosTES,6] := "A095NDTES(M->D1_TES," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ")"
	Else
		aHeader[nPosTES,6] := "A095NDTES(M->D1_TES," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ") .And. " + aHeader[nPosTES,6]
	Endif
	/*-*/
	If Empty(oGetDados:cLinhaOK)
		If lLinhas
			oGetDados:cLinhaOK := "A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ")"
		Else
			oGetDados:cLinhaOK := "A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ") .And. " + cValidacao
		Endif
	Else
		If lLinhas
			oGetDados:cLinhaOK := "A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ") .And. " + oGetDados:cLinhaOK
		Else
			oGetDados:cLinhaOK := "(A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ") .And. " + cValidacao + ") .And. " + oGetDados:cLinhaOK
		Endif
	Endif
	If Empty(oGetDados:cTudoOK)
		oGetDados:cTudoOK := "A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ")"
	Else
		oGetDados:cTudoOK := "A095NDTES(," + If(lTitulo,".T.",".F.") + "," + If(lVldImpFor,".T.",".F.") + ") .And. " + oGetDados:cTudoOK
	Endif
	/*
	nao pemite a exclusao dos itens referentes aos documentos devolvidos */
	If Empty(oGetDados:cSuperDel)
		oGetDados:cSuperDel := cValidacao
	Else
		oGetDados:cSuperDel := cValidacao + ".And. " + oGetDados:cSuperDel
	Endif
	If Empty(oGetDados:cDelOk)
		oGetDados:cDelOk := cValidacao
	Else
		oGetDados:cDelOk := cValidacao + ".And. " + oGetDados:cDelOk
	Endif
	/*
	define uma funcao para ser executada ao final da edicao da nota de debito para capturar os dados na nd gerada */
	oGetDados:oWnd:bValid := {|| A095NDGer(),.T.}
	/*-*/
	oGetDados:lNewLine := .F.
	/*-*/
	MaFisClear()
	MaColsToFis(aHeader,aCols,,"MT100",.T.)
	If !lLinhas
		oGetDados:nMax := nItem
	Endif
	oGetDados:oBrowse:nAt := 1
	oGetDados:oBrowse:Refresh()
Endif
Return()

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FINA096   �Autor  �Microsiga           �Fecha � 12/12/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Recupera os dados da nota de debito gerada para serem      ���
���          � incluidos nos cheques devolvidos.                          ���
���          � Atribui os valores as variaveis declaradas como PRIVATE    ���
���          � pela rotina que executou a funcao da locxnf.               ���
�������������������������������������������������������������������������͹��
���Uso       � FINA096 - geracao de notas de debito para fornecedores     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095NDTES(cTes,lTitulo,lVldImpFor)
Local aAreaSFC	:= SFC->(GetArea())
Local lRet		:= .T.
Local nPosTES	:= 0

Default cTes		:= ""
Default lTitulo		:= .T.
Default lVldImpFor	:= .F.

If Empty(cTes)
	nPosTES := Ascan(aHeader,{|cpo| AllTrim(cpo[2]) == "D1_TES"})
	If nPosTES <> 0
		cTes := aCols[n,nPosTES]
	Endif
Endif

If !Empty(cTes)
	If SF4->(DbSeek(xFilial("SF4") + cTes))
		/* para despesas, o TES nao deve atualizar estoque e deve gerar titulos no financeiro */
		If SF4->F4_ESTOQUE == "S" .Or. If(lTitulo,SF4->F4_DUPLIC <> "S",SF4->F4_DUPLIC == "S")
			If lTitulo
				MsgAlert(STR0117) // "Para incluir Gastos utilice una TES que no actualice Stock y que genere t�tulos financieros."
			Else
				MsgAlert(STR0118) // "Para incluir Gastos utilice una TES que no actualice Stock y que no genere t�tulos financieros."
			Endif
			lRet := .F.
		Endif

		If lRet .And. cPaisLoc == "ARG" .And. lVldImpFor
			SFC->(DbSetOrder(1)) //FC_FILIAL+FC_TES+FC_SEQ+FC_IMPOSTO
			If SF4->F4_DUPLIC <> "S" .And. SFC->(DbSeek(XFilial("SFC")+SF4->F4_CODIGO))
				lRet := .F.
				MsgAlert(STR0106) //"Para notas de troca de valor, utilize TES que gere duplicata e n�o possua configura��o de impostos."
			EndIf
		EndIf

	Endif
Endif

RestArea(aAreaSFC)

Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FINA095   �Autor  �Microsiga           �Fecha � 11/12/2012  ���
�������������������������������������������������������������������������͹��
���Desc.     � Recupera os dados da nota de debito gerada para serem      ���
���          � incluidos nos cheques devolvidos.                          ���
���          � Atribui os valores as variaveis declaradas como PRIVATE    ���
���          � pela rotina que executou a funcao da locxnf.               ���
�������������������������������������������������������������������������͹��
���Uso       � FINA095 - geracao de notas de debito para fornecedores     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095NDGer()
Local nItem

If oGetDados:oWnd:nResult == 0
	If Type("__aoGets")!= "U" .And. ValType(__aoGets)=="A"
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_DOC"})
		If nItem > 0
			cNumNota := __aoGets[nItem]:cText
		Endif
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_SERIE"})
		If nItem > 0
			cSerNota := __aoGets[nItem]:cText
		Endif
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_ESPECIE"})
		If nItem > 0
			cEspNota := __aoGets[nItem]:cText
		Endif
		nItem := Ascan(__aoGets,{|aget| AllTrim(Upper(aget:cReadVar)) == "M->F1_VALBRUT"})
		If nItem > 0
			nValBrut := __aoGets[nItem]:cText
		Endif
	Endif
Endif
Return(.T.)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FINA095   �Autor  �Microsiga           � Data �  02/23/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function A095VldPrx(cPrefix,cChqDe,cChqAte)

Local lReturn 	:= .T.
Local aAreaFRE 	:= FRE->(GetArea())

DEFAULT cPrefix	:= ""
DEFAULT cChqDe	:= ""
DEFAULT cChqaTE	:= ""

If !Empty(cPrefix) .And. Empty(cChqDe) .And. Empty(cChqAte)
	dbSelectArea("FRE")
	If !Empty(IndexKey(4))
		FRE->(dbSetOrder(4))

		If FRE->(dbSeek(xFilial("FRE")+cPrefix))
			lReturn:=MsgYesNo(STR0104)
		EndIf
	EndIf
ElseIf !Empty(cPrefix) .And. !Empty(cChqDe) .And. !Empty(cChqAte)
	dbSelectArea("FRE")
	If !Empty(IndexKey(4))
		FRE->(dbSetOrder(4))

		If FRE->(dbSeek(xFilial("FRE")+cPrefix))
			While !FRE->(Eof()) .And. FRE->FRE_FILIAL == xFilial("FRE") .And. FRE->FRE_PREFIX == cPrefix
				If cChqDe >= FRE->FRE_SEQINI .And. cChqAte <= FRE->FRE_SEQFIM
					lReturn := .F.
					MsgAlert(STR0105)
					Exit
				EndIf
				FRE->(dbSkip())
			EndDo
		EndIf
	EndIf
Else
	lReturn := .F.
	MsgAlert(STR0052)
EndIf

FRE->(RestArea(aAreaFRE))

Return lReturn

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FA95NUMTIT�Autor  �Microsiga           � Data �  02/23/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function FA95NUMTIT(cNumChq,cTitulo,cFornec,cLoja,lOP,cBanco,cConta,cAgencia)
	
	Local aArea 	:= GetArea()
	Local aAreaSEF 	:= {}
	Local cQuery	:= ""
	Local cAlias	:= ""
	Local cChqOrg 	:= ""
	
	DbSelectArea("SEF")
	aAreaSEF 	:= SEF->(GetArea())
	
	cChqOrg := ""
	
	#IFDEF TOP
		cChqOrg := ""
		cQuery 	:=  "SELECT SE2.E2_NUM FROM "+RetSqlName("SEF")+" SEF "
		cQuery  +=  " INNER JOIN " + RetSqlName("SE2") + " SE2 ON(SE2.E2_NUMBCO = '" +  cNumChq + "' And SE2.E2_PREFIXO = SEF.EF_PREFIXO) "
		cQuery	+=	" WHERE EF_FILIAL = '"+xFilial("SEF")+"' AND "
		cQuery	+=	" EF_TITULO = '" + cTitulo +"' AND SE2.E2_NUMBCO = '" + cNumChq + "' AND "
		cQuery	+=	" EF_FORNECE  = '"+ cFornec +"' AND  EF_LOJA  = '"+ cLoja +"' AND "
		cQuery	+=	" E2_BCOCHQ  = '" + cBanco + "' AND  E2_CTACHQ  = '" + cConta + "' AND "
		cQuery	+=	" E2_AGECHQ  = '" + cAgencia + "' AND "
		cQuery	+=	" SEF.D_E_L_E_T_ = ''"
		If lOP
			cQuery	+= " AND SE2.E2_ORDPAGO = SEF.EF_TITULO"
		EndIf
		cQuery 	:= 	ChangeQuery(cQuery)
		cAlias	:=	GetNextAlias()
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAlias,.F.,.T.)
		If (cAlias)->(!Eof())
			cChqOrg := (cAlias)->(E2_NUM)
		EndIf
		DbCloseArea()
	#ELSE
		cChqOrg := ""
		dbSelectArea("SEF")
		SEF->(dbSetOrder(2)) //EF_FILIAL+EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO+EF_NUM+EF_SEQUENC
		SEF->(dbGoTop())
		If SEF->(dbSeek(xFilial("SEF")+" "+cTitulo))
			While !Eof() .And. Empty(cChqOrg)
				If SEF->EF_TITULO == cTitulo .And. SEF->(EF_FORNECE + EF_LOJA) == Fornec .And. SEF->EF_SUBCHE == cNumChq
					If Len(ALLTRIM(SEF->EF_TITULO)) == Len(ALLTRIM(SEF->EF_NUM))
						cChqOrg := SEF->EF_TITULO
					EndIf
				EndIf
			SEF->(dbSkip())
			Enddo
		EndIf
	#ENDIF
	
	If Empty(cChqOrg)
		cChqOrg := cNumChq
	EndIf
	
	RestArea(aAreaSEF)
	RestArea(aArea)

Return cChqOrg
