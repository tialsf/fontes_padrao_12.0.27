// Copyright (C) 2007, Microsiga

#include "protheus.ch"
#include "dbtree.ch"
#include "tbiconn.ch"
#include "msmgadd.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PCOA495   �Autor  �Acacio Egas/ Jo�o   � Data �  03/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina de Planejamento Evolu��o Patrimonial                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � SIGAPCO                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static oMenuALV,oMenuAM2

Function PCOA495()

Local nWidth  	:= GetScreenRes()[1] - 40
Local nHeight 	:= GetScreenRes()[2] - 200
Local bRight	:= {|x,y,z| RightTree(@oPlanej:GetTre("001"),, x, y, z)}
Local nContItem
Local aColsALX,aHeaderALX

If ALV->ALV_STATUS == "2"  // ORCAMENTO EM REVISAO 
	HELP("  ",1,"PCOA100REV")
	Return
EndIf

Private aDataPlnj := {}
Private _aListData := PcoRetPer(ALV->ALV_INIPER,ALV->ALV_FIMPER,ALV->ALV_TPPERI,.F.,aDataPlnj)

DbSelectArea("ALV")
Private oPlanej := PCOArea():New(0,0, nWidth, nHeight,"Planejamento Or�ament�rio ")
Private aCampos := {{'',''}}

dDataInic := CTOD(Substr(aListDtEv[1],1,8))
dSaldInic := dDataInic - 1
cSaldInic := DTOC(dSaldInic) + " - " + DTOC(dSaldInic)

aAdd(_aListData,cSaldInic)
For nContItem := 1 to Len(aListDtEv)
	aAdd(_aListData,aListDtEv[nContItem])
Next	

/*BEGINDOC
//����������������������Ŀ
//�Cria Layouts para Tela�
//������������������������
ENDDOC*/
oPlanej:AddLayout("Planej")
oPlanej:AddLayout("Conta")
oPlanej:AddLayout("Saldos")
oPlanej:AddLayout("Movime")


/*BEGINDOC
//���������������������������������Ŀ
//�Cria Janela Lateral e Objeto Tree�
//�����������������������������������
ENDDOC*/
oPlanej:addSide(28,"Cubos")
oPlanej:AddWindow(96,"WIN1","Estrutura",2,.F.,.F.,oPlanej:oArea:oSideBar)
oPlanej:AddTre("001","WIN1",nil)


/*BEGINDOC
//�����������������������Ŀ
//�Monta Estrutura do Tree�
//�������������������������
ENDDOC*/

oPlanej:No_Tree("Planejamento"			,"ALV","ALLTRIM(ALV_CODIGO)+'-'+ALV_DESCRI"	,"RPMCPO"	,{|| oPlanej:ShowLayout("Planej")}					, bRight ,,									)
oPlanej:No_Tree("Conta Or�ament�ria"	,"AK5","ALLTRIM(AK5_CODIGO)+'-'+AK5_DESCRI"	,"SIMULACA"	,{|| oPlanej:ShowLayout("Conta" )}					, bRight ,,{|x| PcoIncAM2(x,"007",,,.F.) }	)
oPlanej:No_Tree(""						,"XXX","Saldos"								,""			,{|| oPlanej:ShowLayout("Saldo" ),Find_Agreg()	}	, bRight ,,									)



aButs := { 	{ 1, "GPRIMG32", 	{|| PcoRatALX(oPlanej:Getobj('WIN2')) }, 		"Incluir Rateio"	, .F. }, ;
			{ 1, "RELATORIO", 	{|| PcoVisuRateioALX(oPlanej:Getobj('WIN2')) }, 	"Visualizar Rateio"	, .F. }, ;
			{ 1, "EXCLUIR", 	{|| PcoExclRatALX(oPlanej:Getobj('WIN2')) }, 	"Excluir Rateio"	, .F. }   }


// Adiciona Janela 2 com Bot�es - servira para conter o cadastro centro de custo /ou Funcao /ou GetDados


/*BEGINDOC
//����������������������������������Ŀ
//�Adiciona Janelas 2 e 3 com Bot�es.�
//������������������������������������
ENDDOC*/

oPlanej:AddWindow(48,"WIN2","Distribui��o de Entidades",3,.T.,,,aButs)

oPlanej:AddWindow(48,"WIN3","Distribui��o de Valores"	,2,.T.,"Saldos")


/*BEGINDOC
//��������������������������������������Ŀ
//�Cria Objetos do Layout de Planejamento�
//����������������������������������������
ENDDOC*/

// Cria variaveis de memoria para a MSMGet
RegToMemory("ALV", .F.,,, FunName())
oPlanej:AddMsm("001","Planejamento","ALV",ALV->(Recno()),"WIN2","Planej",/*{|x| Cancelamento }*/,/*{|x| Edicao }*/)

oPlanej:AddBrw("001","Conta Or�ament�ria",{"Codigo","Descri��o"},"WIN3","Planej",{|| AtuBrow()})

/*BEGINDOC
//����������������������������������Ŀ
//�Cria Objetos do Layout de Conta   �
//������������������������������������
ENDDOC*/


/*BEGINDOC
//�����������������������������������Ŀ
//�Cria Objetos do Layout de Saldos   �
//�������������������������������������
ENDDOC*/

	//������������������������������Ŀ
	//�GetDados de Saldos Iniciais   �
	//��������������������������������
	
	aCpos := {"ALX_TIPO","ALX_CLASSE","ALX_OPER","ALX_CC","ALX_ITCTB","ALX_CLVLR","ALX_SEQ","ALX_REGRA","ALX_QTDTOT","ALX_VLTOT"}
	If ExistBlock("PCOA4951")
		If VALTYPE(aCposUsr := ExecBlock("PCOA4951"))="A"
			aEval(aCposUsr , {|x| aAdd(aCpos , x ) } )
		EndIf
	EndIf
	aHeaderALX := GetAHeader("ALX",aCpos,)
	aColsALX 	:= nil
	oPlanej:AddGtD("001","Saldos Iniciais"		,"WIN2","Saldos",aHeaderALX,aColsALX,{|| PcoLoadALY(aDataPlnj,@oPlanej:GetGtd("001"))}	,{|x| Find_Agreg()},{|x| PcoPnjAtu(@oPlanej:GetGtd("001"),"007",.T.) })
	
	//�����������������������������Ŀ
	//�GetDados de Fluxo de Caixa   �
	//�������������������������������
	
	aCpos := {"ALX_TIPO","ALX_CLASSE","ALX_OPER","ALX_CC","ALX_ITCTB","ALX_CLVLR","ALX_SEQ","ALX_REGRA","ALX_QTDTOT","ALX_VLTOT"}
	If ExistBlock("PCOA4952")
		If VALTYPE(aCposUsr := ExecBlock("PCOA4952"))="A"
			aEval(aCposUsr , {|x| aAdd(aCpos , x ) } )
		EndIf
	EndIf
	aHeaderALX := GetAHeader("ALX",aCpos,)
	oPlanej:AddGtD("002","Fluxo de Caixa"		,"WIN2","Saldos",aHeaderALX,aColsALX,{|| PcoLoadALY(aDataPlnj,@oPlanej:GetGtd("002"))}	,{|x| Find_Agreg()},{|x| PcoPnjAtu(@oPlanej:GetGtd("002"),"008",.T.) })
	
	//����������������������������������Ŀ
	//�MsmGet dos valores e quantidades  �
	//������������������������������������
	
	_aGetValues := {}
	_aGetQuants := {}
	For nContItem := 1 to Len(_aListData)	
	
		_SetOwnerPrvt("VLR" + StrZero(nContItem,3),CriaVar(Trim("ALY_VALOR"),.F.))		
		_SetOwnerPrvt("QTD" + StrZero(nContItem,3),CriaVar(Trim("ALY_VALOR"),.F.))		
		SX3->(DbSetOrder(2))
		SX3->( MsSeek( PadR("ALY_VALOR", 10 ) ) )
		ADD FIELD _aGetValues TITULO _aListData[nContItem] CAMPO "VLR" + StrZero(nContItem,3) TIPO SX3->X3_TIPO 	TAMANHO SX3->X3_TAMANHO DECIMAL SX3->X3_DECIMAL PICTURE PesqPict(SX3->X3_ARQUIVO,SX3->X3_CAMPO) VALID (SX3->X3_VALID) OBRIGAT NIVEL SX3->X3_NIVEL F3 SX3->X3_F3 BOX SX3->X3_CBOX FOLDER 1
		ADD FIELD _aGetQuants TITULO _aListData[nContItem] CAMPO "QTD" + StrZero(nContItem,3) TIPO SX3->X3_TIPO 	TAMANHO SX3->X3_TAMANHO DECIMAL SX3->X3_DECIMAL PICTURE PesqPict(SX3->X3_ARQUIVO,SX3->X3_CAMPO) VALID (SX3->X3_VALID) OBRIGAT NIVEL SX3->X3_NIVEL F3 SX3->X3_F3 BOX SX3->X3_CBOX FOLDER 1
	
	Next	
	oPlanej:AddMsm("003","Quantidades"	     	,"ALY",ALY->(Recno())	,"WIN3"	,"Saldos"		,{|| PcoLoadALY(aDataPlnj,,)},{|x| PcoLoadALY(aDataPlnj,,,.T.) },_aGetQuants)
	oPlanej:AddMsm("002","Valores"           	,"ALY",ALY->(Recno())	,"WIN3"	,"Saldos"		,{|| PcoLoadALY(aDataPlnj,,)},{|x| PcoLoadALY(aDataPlnj,,,.T.) },_aGetValues)
	

/*BEGINDOC
//�������������������������������������Ŀ
//�Cria Objetos do Layout de Movimentos �
//���������������������������������������
ENDDOC*/

	//���������������������������Ŀ
	//�GetDados de Contra Partida �
	//�����������������������������
	
	aCpos := {"ALX_TIPO","ALX_CLASSE","ALX_OPER","ALX_CC","ALX_ITCTB","ALX_CLVLR","ALX_SEQ","ALX_REGRA","ALX_QTDTOT","ALX_VLTOT"}
	If ExistBlock("PCOA4951")
		If VALTYPE(aCposUsr := ExecBlock("PCOA4951"))="A"
			aEval(aCposUsr , {|x| aAdd(aCpos , x ) } )
		EndIf
	EndIf
	aHeaderALX := GetAHeader("ALX",aCpos,)
	aColsALX 	:= nil
	oPlanej:AddGtD("003","Contra Partida"		,"WIN2","Movime",aHeaderALX,aColsALX,{|| PcoLoadALY(aDataPlnj,@oPlanej:GetGtd("001"))}	,{|x| Find_Agreg()},{|x| PcoPnjAtu(@oPlanej:GetGtd("001"),"007",.T.) })
	
	//�����������������������������Ŀ
	//�GetDados de Fluxo de Caixa   �
	//�������������������������������
	
	aCpos := {"ALX_TIPO","ALX_CLASSE","ALX_OPER","ALX_CC","ALX_ITCTB","ALX_CLVLR","ALX_SEQ","ALX_REGRA","ALX_QTDTOT","ALX_VLTOT"}
	If ExistBlock("PCOA4952")
		If VALTYPE(aCposUsr := ExecBlock("PCOA4952"))="A"
			aEval(aCposUsr , {|x| aAdd(aCpos , x ) } )
		EndIf
	EndIf
	aHeaderALX := GetAHeader("ALX",aCpos,)
	oPlanej:AddGtD("004","Fluxo de Caixa"		,"WIN2","Movime",aHeaderALX,aColsALX,{|| PcoLoadALY(aDataPlnj,@oPlanej:GetGtd("002"))}	,{|x| Find_Agreg()},{|x| PcoPnjAtu(@oPlanej:GetGtd("002"),"008",.T.) })
	
	//����������������������������������Ŀ
	//�MsmGet dos valores e quantidades  �
	//������������������������������������
	
	_aGetValues := {}
	_aGetQuants := {}
	For nContItem := 1 to Len(_aListData)	
	
		_SetOwnerPrvt("VLR" + StrZero(nContItem,3),CriaVar(Trim("ALY_VALOR"),.F.))		
		_SetOwnerPrvt("QTD" + StrZero(nContItem,3),CriaVar(Trim("ALY_VALOR"),.F.))		
		SX3->(DbSetOrder(2))
		SX3->( MsSeek( PadR("ALY_VALOR", 10 ) ) )
		ADD FIELD _aGetValues TITULO _aListData[nContItem] CAMPO "VLR" + StrZero(nContItem,3) TIPO SX3->X3_TIPO 	TAMANHO SX3->X3_TAMANHO DECIMAL SX3->X3_DECIMAL PICTURE PesqPict(SX3->X3_ARQUIVO,SX3->X3_CAMPO) VALID (SX3->X3_VALID) OBRIGAT NIVEL SX3->X3_NIVEL F3 SX3->X3_F3 BOX SX3->X3_CBOX FOLDER 1
		ADD FIELD _aGetQuants TITULO _aListData[nContItem] CAMPO "QTD" + StrZero(nContItem,3) TIPO SX3->X3_TIPO 	TAMANHO SX3->X3_TAMANHO DECIMAL SX3->X3_DECIMAL PICTURE PesqPict(SX3->X3_ARQUIVO,SX3->X3_CAMPO) VALID (SX3->X3_VALID) OBRIGAT NIVEL SX3->X3_NIVEL F3 SX3->X3_F3 BOX SX3->X3_CBOX FOLDER 1
	
	Next	
	oPlanej:AddMsm("004","Quantidades"	     	,"ALY",ALY->(Recno())	,"WIN3"	,"Movime"		,{|| PcoLoadALY(aDataPlnj,,)},{|x| PcoLoadALY(aDataPlnj,,,.T.) },_aGetQuants)
	oPlanej:AddMsm("005","Valores"           	,"ALY",ALY->(Recno())	,"WIN3"	,"Movime"		,{|| PcoLoadALY(aDataPlnj,,)},{|x| PcoLoadALY(aDataPlnj,,,.T.) },_aGetValues)


oPlanej:ShowLayout("Planej")
// Inicializa o Tree
AtuAgreg(.t.)
oPlanej:Activate()

Processa( {|| PcoPnjSld() } , "Atualizando Saldos" )

Return




Static Function AtuBrow()

Local nX
Local cId,cCargo

	aCampos:= {}
	For Nx:=1 To Len(oPlanej:GetTre("001"):aCargo)

		DbSelectArea("AK5")
		DbSetOrder(1)
		cCargo := oPlanej:GetTre("001"):aCargo[Nx,1]
		If SubStr(cCargo,1,3)="AK5"
			
			cId := SubStr(cCargo,4,Len(cCargo)-3)
			DbSelectArea("AM2")
			DbSetOrder(3)
			DbSeek(xFilial("AM2") + ALV->ALV_CODIGO + ALV->ALV_VERSAO + "006" + cId)			
			
			DbSeek(xFilial("AK5")+AM2->AM2_AGREG)
			aAdd(aCampos,{AK5->AK5_CODIGO,AK5->AK5_DESCRI})

		EndIf

	Next
    If Len(aCampos)=0

    	aCampos:= {{'',''}}

    EndIf
	oPlanej:GetBrw("001"):SetArray(aCampos)
	oPlanej:GetBrw("001"):bLine := {|| aCampos[oPlanej:GetBrw("001"):nAt] }
	oPlanej:GetBrw("001"):Refresh()
ReTurn

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PopupMenu �Autor  �Acacio Egas         � Data �  03/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     � Fun��o executada ao clicar bot�o direita no xTree.         ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function RightTree(oTree,oObject1,oObject2, x, y, z)

Local oMenu := PopupMenu(@oTree)

If oMenu <> Nil
	oMenu:Activate(x - 24, y - 100, oPlanej:GetSidebar() )
EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PopupMenu �Autor  �Acacio Egas         � Data �  03/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     � Popup executado no xTree                                   ���
���          �                                                            ���
���Sintaxe   � LoadTree(ExpC1)                                            ���
�������������������������������������������������������������������������͹��
���          � ExpC1 - Objeto xTree para disparar popup.                  ���
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function PopupMenu(oTree)
Local oMenu := Nil
Local cCargo := oTree:GetCargo()
Local cAlias := SubStr(cCargo,1,3)
Local cId

Do Case	
	Case cAlias == "ALV"
		
		If oMenuALV<>nil
		
			oMenuALV:Free()
		
		EndIf
		
		Menu oMenuALV Popup
		MenuItem "Adicionar Conta Or�ament�ria " Block {|| InsertTipo(@oTree)}
		MenuItem "___________________" Disabled
		MenuItem "Excluir Saldos Iniciais" Block {|| PcoRunPlan(nil,5,"007") }		
		MenuItem "___________________" Disabled		
		EndMenu      
		
		oMenu := oMenuALV
		
	Case cAlias == "AK5"
	
		cId := SubStr(cCargo,4,Len(cCargo)-3)
	
		DbSelectArea("AM2")
		DbSetOrder(3)
		DbSeek(xFilial("AM2") + ALV->ALV_CODIGO + ALV->ALV_VERSAO + "006" + cId)
        
		If oMenuAM2<>nil
		
			oMenuAM2:Free()
			
		EndIf
		
		Menu oMenuAM2 Popup
		MenuItem "Excluir Conta Or�ament�ria "	Block {|| DelTipo(@oTree) }
		MenuItem "___________________" Disabled
		MenuItem "Gerar Saldos Iniciais" 	Block {|| PcoRunPlan(cId,"007"	,3,@oPlanej:GetGtd("001")) }
		MenuItem "Excluir Saldos Iniciais"	Block {|| PcoRunPlan(cId, "007" ,5,@oPlanej:GetGtd("001")) }
		MenuItem "___________________" Disabled
		MenuItem "Gerar Varia��o" 	        Block {|| PcoRunPlan(cId,"008",3,@oPlanej:GetGtd("002")) }
		MenuItem "_____________ ______" Disabled
		//MenuItem "Gerar Contra Partidas" 	Block {|| PcoRunPlan(cId,"507",3,@oPlanej:GetGtd("003"))}
		//MenuItem "Excluir Contra Partidas" 	Block {|| PcoRunPlan(cId,"507",5,@oPlanej:GetGtd("003"))}
		EndMenu
		
		oMenu := oMenuAM2
		
	Otherwise		
	
		// sem menu		
		
EndCase          

Return oMenu

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �InsertTipo      �Autor  �Acacio Egas   � Data �  03/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     � Distribui Conta Or�ament�ria no xTree.                     ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function InsertTipo(oTree)
Local aConfig
Local aLoad		:= {}
If ParamBox({ 	{1,"Conta de ",SPACE(Len(AK5->AK5_CODIGO)),PesqPict("AK5","AK5_CODIGO"),"ExistCpo('AK5')","AK5",".T.",60,.F.},; // "Tipos de Despesa"
	  			{1,"Conta ate",SPACE(Len(AK5->AK5_CODIGO)),PesqPict("AK5","AK5_CODIGO"),"ExistCpo('AK5')","AK5",".T.",60,.F.}},"Distribui��o do Planejamento",@aConfig,,,,,,,"PCOA495_01",,.T.)

			aAdd(aLoad,{"AK5X",' Saldos '		,{},"''"})
			aAdd(aLoad,{"AK5X",' Planejamento '	,{},"''"})

	oPlanej:LoadTree(@oTree,"AK5",aConfig[1],aConfig[2],,,aLoad,/*'AK5_TIPO =="2"'*/, "AM2->AM2_ID" )
	AtuAgreg()
EndIf                                                                                                                                          

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �DelTipo         �Autor  �Acacio Egas   � Data �  03/10/08   ���
�������������������������������������������������������������������������͹��
���Desc.     � Deleta Despesas Indiretas no xTree.                        ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function DelTipo(oTree)

Local cCargo 	:= oTree:GetCargo()
Local cId		:= SubStr(cCargo,4,Len(cCargo)-3)
Local lContinua := .T.

DbSelectArea("ALX")
DbSetOrder(3)

If DbSeek(xFilial("ALX")+ cId + "007")

	lContinua := .F.

EndIf
	
If lContinua

	DbSelectArea("AM2")
	DbSetOrder(3)
	If DbSeek(xFilial("AM2") + ALV->ALV_CODIGO + ALV->ALV_VERSAO + "007" + cId)

		RecLock("AM2",.F.)
		DbDelete()
		MsUnlock()
		oTree:DelItem()
		AtuAgreg()
	EndIf
	
Else

	Aviso("Aten��o","Existem lan�amentos para este agregador. Favor excluir os lan�amentos.",{"OK"})
	
EndIf

Return


// Atualiza aCols
Static Function Find_Agreg()
                                
Local cCargo	:= oPlanej:GetTre("001"):GetCargo()
Local cId 		:= SubStr(cCargo,4,Len(cCargo)-3)

DbSelectArea("AM2")
DbSetOrder(3)
DbSeek(xFilial("AM2")+ALV->ALV_CODIGO+ALV->ALV_VERSAO+"006"+cId)

PcoFindALX(cId,"007",@oPlanej:GetGtd("001"))
PcoFindALX(cId,"008",@oPlanej:GetGtd("002"))
//PcoFindALX(cId,"507",@oPlanej:GetGtd("003"))

Return

Static Function AtuAgreg(lIni)
// Monta Tree
Local nX
Local aLoad := {}

Default lIni := .F.
//Local nBrow := oPlanej:GetBrw("001")
DbSelectArea("AM2")
DbSetorder(3)
DbSeek(ALV->ALV_FILIAL+ALV->ALV_CODIGO+ALV->ALV_VERSAO+"007")
Do While !AM2->(Eof()) .and. ALV->ALV_FILIAL+ALV->ALV_CODIGO+ALV->ALV_VERSAO+"007"==AM2->AM2_FILIAL+AM2->AM2_PLANEJ+AM2->AM2_VERSAO+AM2->AM2_TIPOPL
	DbSelectArea("AK5")
	DbSetOrder(1)
	DbSeek(xFilial("AK5")+AM2->AM2_AGREG)
	aAdd(aLoad,{"AK5",AM2->AM2_AGREG,{},"'"+AM2->AM2_ID+"'",AM2->AM2_ID})
	AM2->(DbSkip())
EndDo

If lIni

	oPlanej:LoadTree(@oPlanej:GetTre("001"),"ALV",ALV->ALV_CODIGO,,1,.F.,aLoad)

EndIf

aCampos:= {}
For Nx:=1 To Len(oPlanej:GetTre("001"):aCargo)

	cCargo := oPlanej:GetTre("001"):aCargo[Nx,1]

	If SubStr(cCargo,1,3)="AK5"

	    cId := SubStr(cCargo,4,Len(cCargo)-3)
		DbSelectArea("AM2")
		DbSetOrder(3)
		DbSeek(xFilial("AM2")+ALV->ALV_CODIGO+ALV->ALV_VERSAO+"007"+cId)

		DbSelectArea("AK5")
		DbSetOrder(1)
		DbSeek(xFilial("AK5")+ AM2->AM2_AGREG )
		aAdd(aCampos,{AK5->AK5_CODIGO,AK5->AK5_DESCRI})

	EndIf

Next
If Len(aCampos)==0

	aCampos := {{'',''}}

EndIf
oPlanej:GetBrw("001"):SetArray(aCampos)
oPlanej:GetBrw("001"):bLine := {|| aCampos[oPlanej:GetBrw("001"):nAt] }
oPlanej:GetBrw("001"):Refresh()
Return