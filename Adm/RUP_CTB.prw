//-------------------------------------------------------------------
/*{Protheus.doc}  Rup_CTB( cVersion, cMode, cRelStart, cRelFinish, cLocaliz )
Filtro 

@author Simone Mie Sato Kakinoana
   
@version P12
@since   27/03/2015
@return  Nil
@obs

Fun��o exemplo de compatibiliza��o do release incremental. Esta fun��o � relativa ao m�dulo contabilidade gerencial 
Ser�o chamadas todas as fun��es compiladas referentes aos m�dulos cadastrados do Protheus 
Ser� sempre considerado prefixo "RUP_" acrescido do nome padr�o do m�dulo sem o prefixo SIGA. 
Ex: para o m�dulo SIGACTB criar a fun��o RUP_CTB   

@param  cVersion   - Vers�o do Protheus
@param  cMode      - Modo de execu��o. 1=Por grupo de empresas / 2=Por grupo de empresas + filial (filial completa)
@param  cRelStart  - Release de partida  Ex: 002  
@param  cRelFinish - Release de chegada Ex: 005 
@param  cLocaliz   - Localiza��o (pa�s). Ex: BRA 
*/
//-------------------------------------------------------------------
Function Rup_CTB( cVersion, cMode, cRelStart, cRelFinish, cLocaliz )

//Atualiza SX3	
CtbAtuSX3()      

//Atualiza SX7
CtbAtuSX7(cVersion, cMode, cRelStart, cRelFinish, cLocaliz )      

//Correcao da tabela CTB
CTBAtuCTB(cVersion, cMode, cRelStart, cRelFinish, cLocaliz)

//Ajusta Perguntas 
CTBATUSX1(cVersion)

//o semaforo s� ser� habilitado apartir da 12.1.31
//cMode- Execucao por grupo de empresas
If GetRPORelease() >= "12.1.031" .And. cMode == 1
	//Correcao da tabela CTF
	CTBAtuCTF(cVersion, cMode, cRelStart, cRelFinish, cLocaliz)
EndIf

Return

//-------------------------------------------------------------------
/*{Protheus.doc}  CtbAtuSX3()
Filtro 

@author Simone Mie Sato Kakinoana
   
@version P12
@since   27/03/2015
@return  Nil
@obs
*/
//-------------------------------------------------------------------
Static Function CtbAtuSx3()
Local cNaoUsado	:= "���������������"
Local cReserCSW	:= "��"

If FindFunction("MPDicInDB") .And. MPDicInDB() 
	cNaoUsado	:= "x       x       x       x       x       x       x       x       x       x       x       x       x       x       x       "	
EndIf

If FindFunction("MPDicInDB") .And. MPDicInDB() 
	cReserCSW	:= "  x  xx         "	
EndIf

RUPEngSX()

AjustaSx3("CVD_FILIAL",	 	{ { "X3_ORDEM", '01' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_CONTA",	 	{ { "X3_ORDEM", '02' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_ENTREF",	 	{ { "X3_ORDEM", '03' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_CODPLA",	 	{ { "X3_ORDEM", '04' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_VERSAO",	 	{ { "X3_ORDEM", '05' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_CTAREF",	 	{ { "X3_ORDEM", '06' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_CUSTO",	 	{ { "X3_ORDEM", '07' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_COLUNA",	 	{ { "X3_ORDEM", '08' } }, { }, { }, {|| .T.} )
AjustaSx3("CVD_TPUTIL",	 	{ { "X3_ORDEM", '09' } }, { }, { }, {|| .T.} )
//CVN
AjustaSx3("CVN_CODPLA",	 	{ { "X3_WHEN", 'Ctb025WCD()' } }, { }, { }, {|| .T.} )

// CQC
AjustaSx3("CQC_DATA",	 	{ { "X3_BROWSE", 'S' } }, { }, { }, {|| X3_BROWSE == 'N'} )
AjustaSx3("CQC_DESCR",	 	{ { "X3_BROWSE", 'S' } }, { }, { }, {|| X3_BROWSE == 'N'} )

//	CTQ
//Ajuste valida��o de conta Rateio Offline - CTBA270, removendo ExistCpo, passar� a utilizar Valida��o em fonte NOCONTAC
AjustaSx3("CTQ_CTCPAR",	 	{ { "X3_VALID" ,'Vazio().Or.Ctb105Cta().And.CtbAmarCTQ()' } }, { }, { }, {|| SX3->X3_VALID <> 'Vazio().Or.Ctb105Cta().And.CtbAmarCTQ()' } )

// ajusta o folder das entidades adicionais de 7 para 2
AjustaSx3("CT5_EC05DB",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC05CR",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC06DB",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC06CR",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC07DB",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC07CR",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC08DB",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC08CR",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC09DB",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )
AjustaSx3("CT5_EC09CR",	 	{ { "X3_FOLDER", '2' } }, { }, { }, {|| X3_FOLDER == '7'} )

// Ajusta o folder das entidades adicionais
AjustaSx3("CT1_ACET05",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_05OBRG",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_ACET06",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_06OBRG",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_ACET07",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_07OBRG",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_ACET08",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_08OBRG",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_ACET09",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )
AjustaSx3("CT1_09OBRG",	 	{ { "X3_FOLDER", '3' } }, { }, { }, {|| Empty(SX3->X3_FOLDER)} )

// ajuste do WHEN para o MVC do CTB080
AjustaSx3("CT5_SBLOTE",	 	{ { "X3_WHEN", 'Empty(SuperGetMv("MV_SUBLOTE"))' } }, { }, { }, {|| ALLTRIM(UPPER(X3_WHEN)) == 'CTBA080SB()'} )
AjustaSx3("CVK_SBLOTE",	 	{ { "X3_WHEN", 'Empty(SuperGetMv("MV_SUBLOTE"))' } }, { }, { }, {|| ALLTRIM(UPPER(X3_WHEN)) == 'CTBA080SB()'} )

AjustaSx3("CT1_AJ_INF",	 	{ { "X3_WHEN", 'FWFldGet("CT1_CLASSE")=="2"' } }, { }, { }, {|| ALLTRIM(X3_WHEN) == "M->CT1_CLASSE=='2'"} )
AjustaSx3("CT1_CTPART",	 	{ { "X3_WHEN", 'FWFldGet("CT1_LCCMPL") == "1"' } }, { }, { }, {|| ALLTRIM(X3_WHEN) == "(M->CT1_LCCMPL == '1')"} )
AjustaSx3("CT1_CTCPAR",	 	{ { "X3_WHEN", 'FWFldGet("CT1_LCCMPL") == "1")' } }, { }, { }, {|| ALLTRIM(X3_WHEN) == "(M->CT1_LCCMPL == '1')"} )

//Ajustes ECD leiaute 7
AjustaSx3("CSO_CODFAT",	 	{ { "X3_F3"		, " " 		} }, { }, { }, {|| !Empty(SX3->X3_F3)	   		} )
AjustaSx3("CSO_DESFAT",	 	{ { "X3_VISUAL"	, "A" 		} }, { }, { }, {|| SX3->X3_VISUAL <> "A" 		} )
AjustaSx3("CSE_NOTAEX",	 	{ { "X3_USADO"	, cNaoUsado	} }, { }, { }, {|| VldNaoUsd(SX3->X3_USADO) 	} )
AjustaSx3("CSW_TAM"	  ,	 	{ { "X3_RESERV" , cReserCSW	} }, { }, { }, {|| SX3->X3_RESERV <> cReserCSW 	} )
AjustaSx3("CS0_NUMLIV",	 	{ { "X3_RESERV" , cReserCSW	} }, { }, { }, {|| SX3->X3_RESERV <> cReserCSW 	} )

AjustaSx3("CT2_VALOR",	 	{ { "X3_OBRIGAT" , " "	} }, { }, { }, {|| SX3->X3_OBRIGAT == "  x     " 	} )
AjustaSx3("CT2_VALOR",	 	{ { "X3_RESERV" , "     x x        "	} }, { }, { }, {|| SX3->X3_RESERV == "    xxx         " 	} )

Return
//-------------------------------------------------------------------
/*{Protheus.doc}  AjustaSX3(cCampo, aCpoPor, aCpoSpa, aCpoEng, cCondicao)
Filtro 

@author Simone Mie Sato Kakinoana
   
@version P12
@since   27/03/2015
@return  Nil
@obs
*/
//-------------------------------------------------------------------
Static Function AjustaSx3(cCampo, aCpoPor, aCpoSpa, aCpoEng, cCondicao)

Local aArea := GetArea(), nIndice, lCondicao

If ValType(aCpoPor) = "C"		// A variavel pode ser passada tambem como string
	aCpoPor := { { "X3_CBOX", aCpoPor } }		// Pois eh convertida para matriz
	aCpoSpa := { { "X3_CBOXSPA", aCpoSpa } }
	aCpoEng := { { "X3_CBOXENG", aCpoEng } }
Endif

DbSelectArea("SX3")
DbSetOrder(2)
MsSeek(cCampo)

If ValType(cCondicao) = "B"
	lCondicao := Eval(cCondicao)
Else
	lCondicao := ! cCondicao $ AllTrim(&(aCpoPor[1][1]))
Endif
If Found() .And. lCondicao
	RecLock("SX3", .F.)
	For nIndice := 1 To Len(aCpoPor)
		Replace &(aCpoPor[nIndice][1]) With aCpoPor[nIndice][2]
	Next
	For nIndice := 1 To Len(aCpoSpa)
		Replace &(aCpoSpa[nIndice][1]) With aCpoSpa[nIndice][2]
	Next
	For nIndice := 1 To Len(aCpoEng)
		Replace &(aCpoEng[nIndice][1]) With aCpoEng[nIndice][2]
	Next
	MsUnLock()
Endif

RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*{Protheus.doc}  AjustaSX7(cCampo, aCpoPor, aCpoSpa, aCpoEng, cCondicao)
Filtro 

@author Totvs
   
@version P12
@since   30/09/2015
@return  .T.
@obs
*/
//-------------------------------------------------------------------
Static Function CtbAtuSX7(cVersion, cMode, cRelStart, cRelFinish, cLocaliz )

Local aArea    := GetArea()
Local aAreaSX7 := SX7->(GetArea())

/* Atualiza dicionario */
If cMode == "1"  /* Atualizo por Empresa */

	If cRelStart >= '006' .and. cRelFinish <= '007'  /* Release de partida menor ou igual '007'*/  
		dbSelectArea("SX7")		
		SX7->(dbSetOrder(1))                        /* X7_CAMPO + X7_SEQUENC */
		If SX7->(DbSeek("CTJ_PERCEN")) 
			RecLock("SX7",.F.)
			SX7->(dbDelete())
			SX7->(MsUnlock())
		EndIF
		If SX7->(DbSeek("CTJ_QTDDIS")) 
			RecLock("SX7", .F.)
			SX7->(dbDelete())
			SX7->(MsUnlock())
		EndIF
	EndIf
EndIf

RestArea(aAreaSX7)
RestArea(aArea)

Return

//--------------------------------------------------------------------------
/*{Protheus.doc} CTBAtuCTB
Ajuste da tabela CTB campo CTB_CT2FIL este campo foi criado posteriormente
e houve corre��o no preenchimento para gestao corporativa.

@author Totvs

@param  cVersion   - Vers�o do Protheus
@param  cMode      - Modo de execu��o. 1=Por grupo de empresas / 2=Por grupo de empresas + filial (filial completa)
@param  cRelStart  - Release de partida  Ex: 002  
@param  cRelFinish - Release de chegada Ex: 005 
@param  cLocaliz   - Localiza��o (pa�s). Ex: BRA 

@version P12.1.17
@since   25/05/2018
*/
//--------------------------------------------------------------------------
Static Function CTBAtuCTB(cVersion, cMode, cRelStart, cRelFinish, cLocaliz )
Local aSaveArea	:= {}
Local cEmpOri	:= ""
Local cFilOri	:= ""
Local cFilComp	:= ""
Local cModoEmp	:= ""
Local cModoUn	:= ""
Local cModoFil	:= ""
Local lTabExcl	:= .T.

//--------------------------------
// Execucao por grupo de empresas
//--------------------------------
If cMode == "1"

	aSaveArea := GetArea()

	DBSelectArea("CTB")
	CTB->(DBSetOrder(1))
	CTB->(DBGoTop())

	While CTB->(!EOF()) 

		//-------------------------------------------------------------------------
		// Obtem a estrutura de compartilhamento da CT2 no Grupo de Empresa origem
		//-------------------------------------------------------------------------
		If cEmpOri != CTB->CTB_EMPORI

			cEmpOri	:= CTB->CTB_EMPORI

			cModoEmp	:= FWModeAccess("CT2",1,cEmpOri) //Empresa
			cModoUn		:= FWModeAccess("CT2",2,cEmpOri) //Unidade de Negocio
			cModoFil	:= FWModeAccess("CT2",3,cEmpOri) //Filial

			cFilOri		:= ""
			cFilComp	:= ""

			//---------------------------------------------------------------------------------
			// Avalia se a tabela � total ou parcialmente exclusiva, para executar o FWXFilial
			//---------------------------------------------------------------------------------
			lTabExcl :=  "E" $ (cModoEmp+cModoUn+cModoFil)

		EndIf

		//-----------------------------------------------------------------------
		// Obtem a filial da empresa origem j� tratada conforme compartilhamento
		//-----------------------------------------------------------------------
		If lTabExcl .And. cFilOri != CTB->CTB_FILORI
			
			cFilOri := CTB->CTB_FILORI
			
			cFilComp := FWXFilial("CT2",cFilOri,cModoEmp,cModoUn,cModoFil)
		EndIf

		//-----------------------------
		// Atualiza o campo CTB_CT2FIL
		//-----------------------------
		If cFilComp != CTB->CTB_CT2FIL
			RecLock("CTB",.F.)
			CTB->CTB_CT2FIL := cFilComp
			CTB->(MSUnlock())
		EndIf

	CTB->(DBSkip())
	EndDo

	RestArea(aSaveArea)

EndIf

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  RUPEngSX	  		�TOTVS SA            � Data �  28/12/2018 ���
�������������������������������������������������������������������������͹��
���Desc.     � Chama as fun��es EngSX se o release da vers�o for 12.1.017 ���
�������������������������������������������������������������������������͹��
���Uso       � RUPEngSX		                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RUPEngSX()

Local aDados := {}

If ( GetRPORelease() == "12.1.017" )

	// Atualiza dicionario
	aAdd( aDados, { { 'CVC_CODREL' 	}, { { 'X3_WHEN'	, '', 'a016TabRel()' } } } )
	aAdd( aDados, { { 'CW1_CLVL' 	}, { { 'X3_VALID'	, '(Vazio(M->CW1_CLVL).or.ExistCpo("CTH",M->CW1_CLVL,1)).AND.Ctb276FPrc("CW1_CLVL").AND.CT276VLCl()', } } } )

	AAdd( aDados, { { 'CTS_NORMAL' 	}, { { 'X3_USADO'	, '���������������', '���������������' } } } )
	AAdd( aDados, { { 'CTS_CLASSE' 	}, { { 'X3_USADO'	, '���������������', '���������������' } } } )

	EngSX3117( aDados )

EndIf

Return()
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  VldUsdECD	  		�TOTVS SA            � Data �  21/03/2019 ���
�������������������������������������������������������������������������͹��
���Desc.     � Valida o X3_USADO de acordo com o dicion�rio de dados      ���
�������������������������������������������������������������������������͹��
���Uso       � VldUsdECD		                                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VldNaoUsd(cX3COMP)
Local cNaoUsado	:= "���������������"

If FindFunction("MPDicInDB") .And. MPDicInDB() 
	cNaoUsado	:= "x       x       x       x       x       x       x       x       x       x       x       x       x       x       x       "	
EndIf

Return (cX3COMP<>cNaoUsado)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  CTBATUSX1	  		�TOTVS SA            � Data �  21/03/2019 ���
�������������������������������������������������������������������������͹��
���Desc.     � Cria X1 de acordo com o dicion�rio de dados      ���
�������������������������������������������������������������������������͹��
���Uso       � VldUsdECD		                                          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function CTBATUSX1(cVersion)


Local aEstrutSX1 := {"X1_GRUPO"	, "X1_ORDEM"	, "X1_PERGUNT", "X1_VARIAVL", "X1_TIPO"	, "X1_TAMANHO", ;
				 "X1_DECIMAL"	, "X1_PRESEL"	, "X1_GSC"	   	, "X1_VALID"	, "X1_VAR01"	, "X1_DEF01"	, ;
				 "X1_CNT01"	, "X1_DEF02"	, "X1_DEF03"	, "X1_DEF04"	, "X1_DEF05"	,"X1_F3" 		,;
				 "X1_PYME"		, "X1_GRPSXG"	, "X1_PICTURE" }
Local aSX1:= {}
Local nj := 0
Local nI := 0 

DbSelectArea("SX1")
DbSetOrder(1)

aAdd( aSX1, {'CTB102', '15', 'Atualiza Saldos Online ? '	, 'MV_CHE', 'N', 1, 0, 1	, 'C', '', 'MV_PAR15', 'Sim', ''  , 'N�o', '', '', '', '', 'S', '', '' })
aAdd( aSX1, {'CTB102', '16', 'Reprocessa saldos ?', 'MV_CHF', 'N', 1, 0, 1	, 'C', '', 'MV_PAR16', 'Sim', '' , 'N�o', '', '', '', '', 'S', '', '' })
If Val(Substr(cVersion,7,2)) >= 23  //'12.1.023'
	For ni := 1 To Len(aSX1)
		If !Empty(aSX1[Ni,1])
			If !SX1->(dbseek( padr(aSX1[ni,1],10) + aSX1[ni,2]))
				RecLock("SX1",.T.)
			Else	
				RecLock("SX1",.F.)
			Endif	
			For nj :=1 To Len(aSX1[ni])
				If !Empty(SX1->(FieldName(FieldPos(aEstrutSX1[nj]))))
					SX1->(FieldPut(FieldPos(aEstrutSX1[nj]),aSX1[ni,nj]))
				EndIf
			Next nj
			SX1->(MsUnLock())
		EndIf
	Next ni	  

	PutHelp(".CTB10215.",{"Define se o sistema ir� atualizar o saldo no ato do estorno dos lan�amentos."},{""},{""}, .F., )
	PutHelp(".CTB10216.",{"Executa o reprocessamento de saldos ap�s o estorno dos lan�amentos. "},{""},{""}, .F., )

EndIf
return
//--------------------------------------------------------------------------
/*{Protheus.doc} CTBAtuCTF
Ajuste da tabela CTF no campo CTF_USADO este campo foi criado posteriormente
e dever� preenchido com 'S' 

@author Totvs

@param  cVersion   - Vers�o do Protheus
@param  cMode      - Modo de execu��o. 1=Por grupo de empresas / 2=Por grupo de empresas + filial (filial completa)
@param  cRelStart  - Release de partida  Ex: 002  
@param  cRelFinish - Release de chegada Ex: 005 
@param  cLocaliz   - Localiza��o (pa�s). Ex: BRA 

@version P12.1.30
@since   02/06/2020
*/
//--------------------------------------------------------------------------
Static Function CTBAtuCTF(cVersion, cMode, cRelStart, cRelFinish, cLocaliz )
Local aSaveArea	as array

Local cQuery     as Character
Local cAliasCTF  as Character
Local nUpDates   as Numeric
Local cQryUpdate as Character
Local cUpdate    as Character  
Local nMinRec    as Numeric
Local nMaxRec    as Numeric

aSaveArea := GetArea()

DbSelectArea("CTF")
DbSetOrder(1)

If CTF->(FieldPos("CTF_USADO")) > 0

	
	
	cAliasCTF := CriaTrab(,.F.)
	nUpdates  := 20000
	nMinRec := 0
	nMaxRec := 0
	
	cQuery := ""
	cQuery := "Select Isnull( Min(R_E_C_N_O_), 0 ) nMin , Isnull( Max(R_E_C_N_O_), 0 ) nMax FROM "+RetSqlName("CTF")
	cQuery += " Where CTF_USADO = ' ' "
	cQuery := ChangeQuery( cQuery )

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery), cAliasCTF)

	nMinRec := (caliasCTF)->(nMin)
	nMaxRec := (caliasCTF)->(nMax)

	/* QUERY DE ATUALIZA��O */ 
	cQryUpdate:= ""
	cQryUpdate := "UpDate "+RetSqlName("CTF")
	cQryUpdate += " SET CTF_USADO = 'S' Where CTF_USADO = ' ' "

	If (cAliasCTF)->( !Eof() .and. nMinRec > 0 )

		Do While nMinRec <= nMaxRec  

			cUpdate := cQryUpdate
			cUpdate += " AND R_E_C_N_O_ >= " + Str(nMinRec            , 10, 0) 
			cUpdate += " AND R_E_C_N_O_ <= " + Str( nMinRec + nUpDates, 10, 0)

			If TcSqlExec( cUpdate ) <> 0
				UserException( RetSqlName(cAliasCTF) + " "+ TCSqlError() )
			Endif
			TCRefresh(RetSqlName("CTF"))

			If nMaxRec > nMinRec + nUpDates 
				nMinRec := nMinRec + nUpDates 
				nMinRec++
			Else
				EXIT
			EndIf

		Enddo

	EndIf
	
	(caliasCTF)->( DBCloseArea() )
	
EndIf

RestArea(aSaveArea)

Return
