#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "WSFIN677.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE "TOPCONN.CH"

//-------------------------------------------------------------------
/*/ {REST Web Service} WSFin677
    App Minha Prestacao de Contas integrado com Protheus
    @version 12.1.17
    @since 23/08/2017
    @author Igor Sousa do Nascimento
/*/
//-------------------------------------------------------------------
WSRESTFUL WSFin677	DESCRIPTION "Servico de prestacao de contas"

    WSDATA order          As BOOLEAN Optional
    WSDATA default        As BOOLEAN Optional
    WSDATA page           As INTEGER Optional
    WSDATA pageSize       As INTEGER Optional
    WSDATA fields         As STRING  Optional
    WSDATA expenseID      As STRING  Optional
    WSDATA itemID         As STRING  Optional
    WSDATA isTravel       As STRING  Optional
    // Campos de filtro
    WSDATA international  As BOOLEAN Optional
    WSDATA departure_date As STRING  Optional
    WSDATA arrival_date   As STRING  Optional  
    WSDATA travelNumber   As STRING  Optional
    WSDATA travel         As BOOLEAN Optional
    WSDATA status         As STRING  Optional
    WSDATA open           As BOOLEAN Optional
    WSDATA searchKey      As STRING  Optional

    WSMETHOD GET Main ;
    DESCRIPTION "Carrega as prestacoes de contas na tela principal" ;
    WSSYNTAX "/expenses" ;
    PATH "/expenses"

    WSMETHOD GET Checked ;
    DESCRIPTION "Carrega as presta��es liberadas para aprova��o" ;
    WSSYNTAX "/checked" ;
    PATH "/checked"

    WSMETHOD GET CheckItems ;
    DESCRIPTION "Carrega os itens da prestacao de contas em aprova��o" ;
    WSSYNTAX "/checked/{expenseID}/{isTravel}/items" ;
    PATH "/checked/{expenseID}/{isTravel}/items"

    WSMETHOD GET CheckAttch ;
    DESCRIPTION "Carrega os anexos da despesa em aprova��o" ;
    WSSYNTAX "/checked/{expenseID}/{isTravel}/items/{itemID}/attachment" ;
    PATH "/checked/{expenseID}/{isTravel}/items/{itemID}/attachment"
 
    WSMETHOD GET Acc ;
    DESCRIPTION "Carrega uma prestacao de contas em especifico" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}" ;
    PATH "/expenses/{expenseID}/{isTravel}"

    WSMETHOD GET Items ;
    DESCRIPTION "Carrega os itens da prestacao de contas" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items" ;
    PATH "/expenses/{expenseID}/{isTravel}/items"

    WSMETHOD GET Attch ;
    DESCRIPTION "Carrega os anexos da despesa" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment" ;
    PATH "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment"

    WSMETHOD GET Clients ; 
    DESCRIPTION "Metodo para retornar a lista de clientes disponiveis" ;
    WSSYNTAX "/clients" ;
    PATH "/clients"

    WSMETHOD GET CostCenters ; 
    DESCRIPTION "Metodo para retornar a lista de centros de custo" ;
    WSSYNTAX "/cost_centers" ;
    PATH "/cost_centers"

    WSMETHOD GET Currencies ; 
    DESCRIPTION "Metodo para retornar a lista de moedas disponiveis" ;
    WSSYNTAX "/currencies" ;
    PATH "/currencies"

    WSMETHOD GET Destinations ; 
    DESCRIPTION "Metodo para retornar a lista de destinos de viagens" ; 
    WSSYNTAX "/destinations" ;
    PATH "/destinations"

    WSMETHOD GET XpenseType ; 
    DESCRIPTION "Metodo para retornar a lista dos tipos de despesas" ; 
    WSSYNTAX "/items/types" ;
    PATH "/items/types"

    WSMETHOD GET isApprover ; 
    DESCRIPTION "Metodo para retornar se um usuario e aprovador no cenario de despesas." ; 
    WSSYNTAX "/isapprover" ;
    PATH "/isapprover"

    WSMETHOD POST Acc ; 
    DESCRIPTION "Inclusao da prestacao de contas e item" ;
    WSSYNTAX "/expenses" ;
    PATH "/expenses"

    WSMETHOD POST Xpenses ; 
    DESCRIPTION "Inclusao de item na prestacao de contas" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items" ;
    PATH "/expenses/{expenseID}/{isTravel}/items"

    WSMETHOD POST Attch ; 
    DESCRIPTION "Inclusao de anexo na despesa" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment" ;
    PATH "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment"

    WSMETHOD PUT ToCheck ;
    DESCRIPTION "Envia prestacao de contas para conferencia" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}" ;
    PATH "/expenses/{expenseID}/{isTravel}"

    WSMETHOD PUT Update ; 
    DESCRIPTION "Atualiza item da prestacao de contas" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items/{itemID}" ;
    PATH "/expenses/{expenseID}/{isTravel}/items/{itemID}"

    WSMETHOD PUT Checked;
    DESCRIPTION "Aprova a presta��o de contas" ;
    WSSYNTAX "/checked/{expenseID}/{isTravel}" ;
    PATH "/checked/{expenseID}/{isTravel}"

    WSMETHOD DELETE Acc ; 
    DESCRIPTION "Exclui prestacao de contas e item" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}" ;
    PATH "/expenses/{expenseID}/{isTravel}"

    WSMETHOD DELETE Item ; 
    DESCRIPTION "Exclui item da prestacao de contas" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items/{itemID}" ;
    PATH "/expenses/{expenseID}/{isTravel}/items/{itemID}"

    WSMETHOD DELETE Attch ; 
    DESCRIPTION "Exclui anexo da despesa" ;
    WSSYNTAX "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment" ;
    PATH "/expenses/{expenseID}/{isTravel}/items/{itemID}/attachment"

END WSRESTFUL

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Main WSRECEIVE order,page,pageSize,fields,international,departure_date,arrival_date,travelNumber,travel,status,open,searchKey WSSERVICE WSFin677
    Local aFilter   As Array
    Local cJson     As Character
    Local cSign     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    Default Self:order          := .F.
    Default Self:page           := 1
    Default Self:pageSize       := 10
    Default Self:departure_date := ""
    Default Self:arrival_date   := ""
    Default Self:travelNumber   := ""
    Default Self:status         := ""
    Default Self:searchKey      := ""

    oExpenses     := JsonObject():New()

    // Converte parametro fields em array 
    If ValType(Self:fields) == "C"
        Self:fields   := StrToKarr(Self:fields,",")
    EndIf

    /* Estrutura do array para filtro:
       pos1 - Campo da tabela 
       pos2 - Valor a ser filtrado na query
       pos3 - Operador logico
       pos4 - Usada pelo searchKey, campos considerados na busca
       obs1: Todos os valores em String
    */
    aFilter       := {}
    If ValType(Self:international) == "L"
        cSign := If(Self:international," <> "," = ")
        Aadd(aFilter,{"FLF_NACION","'1'",cSign})
    EndIf
    If !Empty(Self:departure_date)
        Aadd(aFilter,{"FLF_DTINI", "'" + Self:departure_date + "'", " >= "})
    EndIf
    If !Empty(Self:arrival_date)
        Aadd(aFilter,{"FLF_DTFIM", "'" + Self:arrival_date + "'", " <= "})
    EndIf
    If !Empty(Self:travelNumber)
        Aadd(aFilter,{"FL5_VIAGEM", "'" + Self:travelNumber + "'", " = "})
    EndIf
    If ValType(Self:travel) == "L"
        cSign := If(Self:travel," <> "," = ")
        Aadd(aFilter,{"FLF_TIPO","'2'",cSign})
    EndIf
    If !Empty(Self:status)
        Aadd(aFilter,{"FLF_STATUS", "'" + Self:status + "'", " = "})
        
        If (Self:status == "2")
            aFilter[Len(aFilter)][2] := "('2', '3', '4')"
            aFilter[Len(aFilter)][3] := " IN"
        EndIf 

        If (Self:status == "8")
            aFilter[Len(aFilter)][2] := "('8', '9')"
            aFilter[Len(aFilter)][3] := " IN"
        EndIf

    EndIf
    If ValType(Self:open) == "L"
        If Self:open
            Aadd(aFilter,{"FLF_STATUS","('1','5')"," IN"})
        Else
            Aadd(aFilter,{"FLF_STATUS","('1','5')"," NOT IN"})
        EndIf
    EndIf

    If !Empty(Self:searchKey)
        Aadd(aFilter,{"SEARCHKEY", "'%" + Self:searchKey + "%' ", " LIKE ",{"FLF_PRESTA","FL5_DESDES","A1_NOME","FL5_VIAGEM"}})
    EndIf

    lRet := LoadXpense(Self:order,Self:page,Self:pageSize,Self:fields,aFilter,@oExpenses)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet


/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Checked WSRECEIVE page, pageSize, fields, searchKey WSSERVICE WSFin677
    Local aFilter   As Array
    Local cJson     As Character
    Local lRet       As Logical
    Local oExpenses  As Object

    Default Self:page     := 1
    Default Self:pageSize := 10
    Default Self:searchKey:= ""

    oExpenses     := JsonObject():New()
    
    If ValType(Self:fields) == "C"
        Self:fields   := StrToKarr(Self:fields, ",")
    EndIf

    aFilter       := {}
    If !Empty(Self:searchKey)
        Aadd(aFilter,{"SEARCHKEY", "'%" + Self:searchKey + "%' ", " LIKE ",{"FLF_PRESTA","FLF_PARTIC"}})
    EndIf

    lRet := LoadXpense( , Self:page, Self:pageSize, Self:fields, aFilter, @oExpenses, .T. )
    
    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Acc PATHPARAM expenseID,isTravel WSRECEIVE fields WSSERVICE WSFin677

    Local aFilter   As Array
    Local cJson     As Character
    Local cSign     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    Default Self:expenseID := ""
    Default Self:isTravel := ""

    oExpenses     := JsonObject():New()
    aFilter       := {}

    // Converte parametro fields em array 
    If ValType(Self:fields) == "C"
        Self:fields   := StrToKarr(Self:fields,",")
    EndIf

    If "travel" $ Self:isTravel
        cSign := " <> "
    ElseIf "detached" $ Self:isTravel
        cSign := " = "
    EndIf

    Aadd(aFilter,{"FLF_PRESTA", "'" + Self:expenseID + "'", " = "})
    Aadd(aFilter,{"FLF_TIPO","'2'",cSign})

    lRet := LoadXpense(,Self:page,Self:pageSize,Self:fields,aFilter,@oExpenses)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Items PATHPARAM expenseID,isTravel WSRECEIVE page,pageSize,fields WSSERVICE WSFin677

    Local cJson     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    Default Self:page           := 1
    Default Self:pageSize       := 10

    oExpenses     := JsonObject():New()

    // Converte parametro fields em array 
    If ValType(Self:fields) == "C"
        Self:fields   := StrToKarr(Self:fields,",")
    EndIf
    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet := LoadItems(Self:page,Self:pageSize,Self:fields,@oExpenses,Self:expenseID,Self:isTravel)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET CheckItems PATHPARAM expenseID, isTravel WSRECEIVE page, pageSize, fields WSSERVICE WSFin677
    Local cJson     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    Default Self:page           := 1
    Default Self:pageSize       := 10

    oExpenses     := JsonObject():New()

    // Converte parametro fields em array 
    If ValType( Self:fields ) == "C"
        Self:fields   := StrToKarr( Self:fields, "," )
    EndIf

    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet := LoadItems( Self:page, Self:pageSize, Self:fields, @oExpenses, Self:expenseID, Self:isTravel, .T. )

    cJson := FWJsonSerialize( oExpenses, .F., .F., .T. )
    ::SetResponse( cJson )
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Attch PATHPARAM expenseID, isTravel, itemID WSSERVICE WSFin677
    Local oJsonRet  := NIL
    Local lRet      := .T.
    Local cJson     := ''

    oJsonRet     := JsonObject():New()

    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet   := LoadAttach( Self:expenseID, Self:isTravel, Self:itemID, @oJsonRet )

    cJson := FWJsonSerialize( oJsonRet, .F., .F., .T. )
    ::SetResponse( cJson )
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET CheckAttch PATHPARAM expenseID, isTravel, itemID WSSERVICE WSFin677
    Local oJsonRet  := NIL
    Local lRet      := .T.
    Local cJson     := ''

    oJsonRet     := JsonObject():New()

    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet := LoadAttach( Self:expenseID, Self:isTravel, Self:itemID, @oJsonRet, .T. )

    cJson := FWJsonSerialize( oJsonRet, .F., .F., .T. )
    ::SetResponse( cJson )

    FreeObj( oJsonRet )
Return lRet


/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Clients WSRECEIVE page,pageSize,searchKey WSSERVICE WSFin677
    
    Local lRet      As Logical
    Local oClients  As Object

    // - Parametros enviados pela URL - QueryString
    Default Self:page     := 1
    Default Self:pageSize := 10
    Default Self:searchKey := ""

    oClients    := JsonObject():New()
    
    // Monta objeto do response
    lRet := LoadClients(Self:page,Self:pageSize,Self:searchKey,@oClients)
    
    cJson := FWJsonSerialize(oClients, .F., .F., .T.)
    ::SetResponse(cJson)
    
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET CostCenters WSRECEIVE page,pageSize,searchKey,default WSSERVICE WSFin677
    
    Local lRet      As Logical
    Local oCosts    As Object

    // - Parametros enviados pela URL - QueryString
    Default Self:page      := 1
    Default Self:pageSize  := 10
    Default Self:searchKey := ""
    Default Self:default   := .T.

    oCosts    := JsonObject():New()
    
    // Monta objeto do response
    lRet := LoadCC(Self:page,Self:pageSize,Self:searchKey,Self:default,@oCosts)
    
    cJson := FWJsonSerialize(oCosts, .F., .F., .T.)
    ::SetResponse(cJson)
    
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Currencies WSRECEIVE page,pageSize WSSERVICE WSFin677
    
    Local lRet      As Logical
    Local oCurrence As Object

    // - Parametros enviados pela URL - QueryString
    Default Self:page     := 1
    Default Self:pageSize := 10

    oCurrence := JsonObject():New()
    
    // Monta objeto do response
    lRet := Currencies(@oCurrence,Self:page,Self:pageSize)
    
    cJson := FWJsonSerialize(oCurrence, .F., .F., .T.)
    ::SetResponse(cJson)
    
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET Destinations WSRECEIVE page,pageSize,international,searchKey WSSERVICE WSFin677
    
    Local lRet           As Logical
    Local oDestinations  As Object

    // - Parametros enviados pela URL - QueryString
    Default Self:page          := 1
    Default Self:pageSize      := 10
    Default Self:international := .F.
    Default Self:searchKey     := ""

    oDestinations    := JsonObject():New()

    // Monta objeto do response
    lRet := Locations(Self:page,Self:pageSize,Self:international,Self:searchKey,@oDestinations)
    
    cJson := FWJsonSerialize(oDestinations, .F., .F., .T.)
    ::SetResponse(cJson)    
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET XpenseType WSRECEIVE page,pageSize,searchKey WSSERVICE WSFin677
    
    Local lRet         As Logical
    Local oItemsTypes  As Object
    
    Default Self:page       := 1
    Default Self:pageSize   := 10
    Default Self:searchKey := ""    
    
    oItemsTypes     := JsonObject():New()

    // Monta objeto do response
    lRet := XpenseType(Self:page,Self:pageSize,Self:searchKey,@oItemsTypes)
    
    cJson := FWJsonSerialize(oItemsTypes, .F., .F., .T.)
    ::SetResponse(cJson)    
Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD GET isApprover WSSERVICE WSFin677
    
    Local lRet         As Logical
    Local oJSONResp  As Object
     
    oJSONResp := JsonObject():New()

    // Monta objeto do response
    lRet := isApprover( @oJSONResp )
    
    cJson := FWJsonSerialize( oJSONResp, .F., .F., .T. )
    ::SetResponse(cJson) 

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD POST Acc WSSERVICE WSFin677

    Local cBody     As Character
    Local cJson  As Character
    Local lRet      As Logical
    Local oExpenses As Object

    cBody 	   := ::GetContent()
    oExpenses  := JsonObject():New()

    lRet := NewXpense(@oExpenses,cBody)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD POST Xpenses PATHPARAM expenseID,isTravel WSSERVICE WSFin677

    Local cBody     As Character
    Local cJson     As Character
    Local cTipo     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    cBody 	   := ::GetContent()
    oExpenses  := JsonObject():New()

    cTipo := ""
    If ("travel" $ Self:isTravel)
        cTipo := "1"
    ElseIf "detached" $ Self:isTravel
        cTipo := "2"
    EndIf
    lRet := NewItem(@oExpenses,cBody,Self:expenseID,cTipo)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD POST Attch PATHPARAM expenseID,isTravel,itemID WSSERVICE WSFin677

    Local aUser     As Array
    Local aSeek     As Array
    Local cJson     As Character
    Local cBody     As Character
    Local lRet      As Logical
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oJsonTmp  As Object
    Local oMessages As Object

    oJsonTmp     := JsonObject():New()
    oMessages    := JsonObject():New()
    cBody 	     := ::GetContent()

    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet   := .T.

    If FINXUser(__cUserID,@aUser,.F.)
        cCatch   := oJsonTmp:FromJSON(cBody)
        If cCatch == Nil
            dbSelectArea("FLE")
            dbSetOrder(1)
            dbSelectArea("FLF")
            dbSetOrder(1)
            If FLE->(dbSeek(xFilial("FLE") + Self:isTravel + Self:expenseID + aUser[1] + Self:itemID))
                FLF->(dbSeek(xFilial("FLF") + Self:isTravel + Self:expenseID + aUser[1]))
                oModel677:= FWLoadModel("FINA677")
                oModel677:SetOperation(MODEL_OPERATION_UPDATE)
                F677LoadMod(oModel677,MODEL_OPERATION_UPDATE)
                oModel677:Activate()

                oModelFLE:= oModel677:GetModel("FLEDETAIL")

                If (oModel677:GetValue("FLFMASTER","FLF_STATUS") == "1") .OR. oModel677:GetValue("FLFMASTER","FLF_STATUS") == "5"  
                    aSeek := { {"FLE_FILIAL", xFilial("FLE")},; 
                             {"FLE_TIPO", Self:isTravel},; 
                             {"FLE_PRESTA", Self:expenseID},; 
                             {"FLE_PARTIC", aUser[1]},;
                             {"FLE_ITEM", Self:itemID } }
                    oModelFLE:SeekLine( aSeek )
            
                    oModelFLE:LoadValue("FLE_FILE",oJsonTmp["name"])
                    
                    aImg := F677ImgApp(3,oModel677,oJsonTmp["content"])
                    If Len(aImg) > 0 .and. aImg[1] == "400"
                        oMessages["code"] 	:= aImg[1]
                        oMessages["message"]	:= "Bad Request"
                        oMessages["detailMessage"] := aImg[2]
                        lRet := .F.
                    Else
                        oJsonTmp := JsonObject():New()  // Retorna objeto vazio se deu tudo certo
                        oJsonTmp["name"] := aImg[2]
                        
                        If oModel677:GetValue("FLFMASTER","FLF_STATUS") == "5"
                            AltStatus()
                        EndIf
                        
                    EndIf
                Else
                    oMessages["code"] 	:= "403"
                    oMessages["message"]	:= "Forbidden"
                    oMessages["detailMessage"] := STR0009 //"Status da prestacao de contas nao permite alteracao."
                    lRet := .F.
                EndIf
                oModel677:DeActivate()
                oModel677:Destroy()
                oModel677 := NIL
            Else
                oMessages["code"] 	:= "404"
                oMessages["message"]	:= "Not Found"
                oMessages["detailMessage"] := "Nenhuma despesa encontrada para gravar o anexo enviado."   // "N?o foi encontrado anexo para essa despesa."
                lRet := .F.
            EndIf
        Else
            oMessages["code"] 	:= "400"
            oMessages["message"]	:= "Bad Request"
            oMessages["detailMessage"] := cCatch
            lRet := .F.
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oJsonTmp := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

    cJson := FWJsonSerialize(oJsonTmp, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD PUT ToCheck PATHPARAM expenseID,isTravel  WSSERVICE WSFin677

    Local aUser     As Array
    Local cTipo     As Character
    Local cIDFLF    As Character
    Local cLog      As Character
    Local lRet      As Logical
    Local oExpenses As Object
    Local oMessages As Object

    oExpenses  := JsonObject():New()
    oMessages  := JsonObject():New()

    cIDFLF := Self:expenseID
    cTipo  := ""
    lRet   := .T.

    If "travel" $ Self:isTravel
        cTipo := "1"
    ElseIf "detached" $ Self:isTravel
        cTipo := "2"
    EndIf

    If FINXUser(__cUserID,@aUser,.F.)
        oJsonTmp := JsonObject():New()
        
        dbSelectArea("FLE")
        FLE->(dbSetOrder(1))

        dbSelectArea("FLF")
        FLF->(dbSetOrder(1))

        If FLF->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1]))
            // Envia para conferencia
            F677ENVCON(@cLog)
            If !Empty(cLog)
                oMessages["code"] 	:= "400"
                oMessages["message"]	:= "Bad Request"
                oMessages["detailMessage"] := cLog
                lRet := .F.
            EndIf
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses["messages"] := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet


/*/--------------------------------------------------------------------------/*/
WSMETHOD PUT Checked PATHPARAM expenseID,isTravel  WSSERVICE WSFin677

    Local aUser     As Array
    Local cType     As Character
    Local cCatch    As Character
    Local cIDFLN    As Character
    Local cBody     As Character
    Local lRet      As Logical
    Local oExpenses As Object
    Local oMessages As Object
    Local oJsonTmp  As Object

    Local cAction As Character
    Local cReason as Character
    Local cTpAprov as Character
    
    cBody 	   := ::GetContent()

    oExpenses  := JsonObject():New()
    
    cIDFLN := Self:expenseID
    cType  := ""
    cAction := ""
    cReason := ""
    cTpAprov := ""
    lRet   := .T.

    If "travel" $ Self:isTravel
        cType := "1"
    ElseIf "detached" $ Self:isTravel
        cType := "2"
    EndIf

    If FINXUser(__cUserID, @aUser, .F.)
        oJsonTmp := JsonObject():New()
        cCatch   := oJsonTmp:FromJSON(cBody)
        If cCatch == Nil    

            If oJsonTmp["action"] == "approve"
                cAction := 'A'
            else
                cAction := 'R'
                cReason := DecodeUTF8( oJsonTmp["reason"] )
            endif
            
            dbSelectArea("FLN")
            FLN->(dbSetOrder(1)) //FLN_FILIAL+FLN_TIPO+FLN_PRESTA+FLN_PARTIC+FLN_SEQ+FLN_TPAPR

            If FLN->(dbSeek(xFilial("FLN") + cType + cIDFLN ))
                If FLN->FLN_APROV == aUser[1]
                    cTpAprov := 'O'
                EndIf
                
                If Empty(cTpAprov)
                    RD0->(DbSetOrder(1)) // Filial + Participante
                    If RD0->(DbSeek( xFilial("RD0") + FLN->FLN_PARTIC ))
                        If aUser[1] == RD0->RD0_APROPC
                            cTpAprov := 'O'
                        ElseIf aUser[1] == AllTrim(RD0->RD0_APSUBS)
                            cTpAprov := 'S'
                        EndIf
                    EndIf 
                EndIf

                F677APRGRV(cAction, cTpAprov, aUser, FLN->FLN_TIPO, FLN->FLN_PRESTA, FLN->FLN_PARTIC, FLN->FLN_SEQ, cReason, '1',)

                If FLN->(dbSeek(xFilial("FLN") + cType + cIDFLN ))
                    If FLN->FLN_STATUS == '1'
                        oMessages["code"] 	:= "400"
                        oMessages["message"]	:= "Bad Request"
                        oMessages["detailMessage"] := STR0010 //"A opera��o de aprova��o n�o foi conclu�da."
                        lRet := .F.
                    else
                        oExpenses["id"] := cIDFLN
                    EndIf
                EndIf
            EndIf
        Else
            oMessages["code"] 	:= "400"
            oMessages["message"]	:= "Bad Request"
            oMessages["detailMessage"] := cCatch
            lRet := .F.
        ENDIF
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses["messages"] := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD PUT Update PATHPARAM expenseID,isTravel,itemID  WSSERVICE WSFin677

    Local cBody     As Character
    Local cTipo     As Character
    Local lRet      As Logical
    Local oExpenses As Object

    cBody 	   := ::GetContent()
    oExpenses  := JsonObject():New()

    cTipo := ""
    If ("travel" $ Self:isTravel)
        cTipo := "1"
    ElseIf "detached" $ Self:isTravel
        cTipo := "2"
    EndIf
    lRet := UpdXpense(@oExpenses,cBody,Self:expenseID,cTipo,Self:itemID)

    cJson := FWJsonSerialize(oExpenses, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD DELETE Acc PATHPARAM expenseID,isTravel WSSERVICE WSFin677

    Local cTipo     As Character
    Local lRet      As Logical
    Local oResponse As Object

    oResponse  := JsonObject():New()

    cTipo := ""
    If "travel" $ Self:isTravel
        cTipo := "1"
    ElseIf "detached" $ Self:isTravel
        cTipo := "2"
    EndIf
    lRet := DelXpense(@oResponse,Self:expenseID,cTipo)    

    cJson := FWJsonSerialize(oResponse, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD DELETE Item PATHPARAM expenseID,isTravel,itemID WSSERVICE WSFin677

    Local cTipo     As Character
    Local lRet      As Logical
    Local oResponse As Object

    oResponse  := JsonObject():New()

    cTipo := ""
    If "travel" $ Self:isTravel
        cTipo := "1"
    ElseIf "detached" $ Self:isTravel
        cTipo := "2"
    EndIf
    lRet := DelItem(@oResponse,Self:expenseID,cTipo,Self:itemID)

    cJson := FWJsonSerialize(oResponse, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

/*/--------------------------------------------------------------------------/*/
WSMETHOD DELETE Attch PATHPARAM expenseID,isTravel,itemID WSSERVICE WSFin677

    Local aUser     As Array
    Local aImg      As Array
    Local aSeek     As Array
    Local cJson     As Character
    Local lRet      As Logical
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oJsonRet  As Object
    Local oMessages As Object

    oJsonRet     := JsonObject():New()
    oMessages    := JsonObject():New()

    If "travel" $ Self:isTravel
        Self:isTravel := "1"
    ElseIf "detached" $ Self:isTravel
        Self:isTravel := "2"
    EndIf

    lRet   := .T.

    If FINXUser(__cUserID,@aUser,.F.)
        dbSelectArea("FLE")
        dbSetOrder(1)
        dbSelectArea("FLF")
        dbSetOrder(1)
        If FLE->(dbSeek(xFilial("FLE") + Self:isTravel + Self:expenseID + aUser[1] + Self:itemID))
            FLF->(dbSeek(xFilial("FLF") + Self:isTravel + Self:expenseID + aUser[1]))
            oModel677:= FWLoadModel("FINA677")
            oModel677:SetOperation(MODEL_OPERATION_UPDATE)  // Update, pois nao estamos deletando o registro
            F677LoadMod(oModel677,MODEL_OPERATION_UPDATE)
            oModel677:Activate()
            oModelFLE:= oModel677:GetModel("FLEDETAIL")
           aSeek := { {"FLE_FILIAL", xFilial("FLE")},; 
                      {"FLE_TIPO", Self:isTravel},; 
                      {"FLE_PRESTA", Self:expenseID},; 
                      {"FLE_PARTIC", aUser[1]},;
                      {"FLE_ITEM", Self:itemID } }
            oModelFLE:SeekLine( aSeek )
            aImg := F677ImgApp(5,oModel677)
            If Len(aImg) > 0 .and. aImg[1] == "202"
                oJsonRet["message"]    := aImg[2]
            ElseIf aImg[1] <> Nil
                oMessages["code"] 	:= aImg[1]
                oMessages["message"]	:= "Bad Request"
                oMessages["detailMessage"] := aImg[2]
                lRet := .F.
            Else
                oMessages["code"] 	:= "404"
                oMessages["message"]	:= "Not Found"
                oMessages["detailMessage"] := STR0008 //"Nao foi encontrado anexo para essa despesa."   // "N?o foi encontrado anexo para essa despesa."
                lRet := .F.
            EndIf
            oModel677:DeActivate()
            oModel677:Destroy()
            oModel677 := NIL
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oJsonRet := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

    cJson := FWJsonSerialize(oJsonRet, .F., .F., .T.)
    ::SetResponse(cJson)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadXpense
    Carrega prestacao de contas na tela principal
    @author Igor Sousa do Nascimento
    @since 23/08/2017
/*/
//-------------------------------------------------------------------
Static Function LoadXpense(lAsc,nPage,nPageSize,aFields,aFilter,oExpenses, lChecked)

    Local aUser        As Array
    Local aDePara      As Array
    Local cTmp         As Character
    Local cQry         As Character
    Local cBranchFLF   As Character
    Local cBranchSA1   As Character
    Local cBranchCTT   As Character
    Local cOrderBy     As Character
    Local cFilter      As Character
    Local cFilterSub   As Character
    Local cSGBD        As Character
    Local lRet         As Logical
    Local nInitPage    As Numeric
    Local nI           As Numeric
    Local nAt  	       As Numeric
    Local nT           As Numeric
    Local oJsonTmp     As Object
    Local oMessages    As Object

    Default lAsc      := .F.
    Default nPage     := 1
    Default nPageSize := 10
    Default aFields   := { "id","client","reason","destination",;
                           "travel","travelNumber","departure_date","arrival_date",;
                           "status","balances","balances.advance_money","reason_for_refusal",;
                           "company_billing_percent","cc_code","cc_description" }
    Default aFilter   := {}
    Default oExpenses := JsonObject():New()
    Default lChecked  := .F.

    lRet       := .T.
    // Trata response negativo
    oMessages  := JsonObject():New()
    aDePara    := {}

    If FINXUser(__cUserID,@aUser,.F.)

        cTmp       := CriaTrab(,.F.)
        cQry       := ""
        cSGBD      := Alltrim(Upper(TCGetDB()))
        cBranchFLF := xFilial("FLF",cFilAnt)
        cBranchSA1 := xFilial("SA1",cBranchFLF)
        cBranchCTT := xFilial("CTT",cBranchFLF)

        If !lChecked
            cFilter    := "FLF.FLF_PARTIC = '" + aUser[1] + "'"
            cFilterSub := "SUBFLF.FLF_PARTIC = '" + aUser[1] + "'"
        else
            cFilter    := ''
            cFilterSub := ''
        EndIf

        If !Empty(aFilter)
            For nI := 1 to Len(aFilter)
                If !Empty( cFilter)
                    cFilter += " AND "
                    cFilterSub += " AND "
                ENDIF

                Do Case
                    Case aFilter[nI,1] == "SEARCHKEY"  // Filtro coringa em campos especificos
                        cFilter += "("
                        cFilterSub += "("
                        For nT := 1 to Len(aFilter[nI,4])   // Campos do searchKey
                            nAt := At("_",aFilter[nI,4,nT])
                            If nAt < 4
                                cFilter += "S"+SubStr(aFilter[nI,4,nT],1,nAt-1) +"."+ aFilter[nI,4,nT] + aFilter[nI,3] + AllTrim(aFilter[nI,2])
                                cFilterSub += "SUBS"+SubStr(aFilter[nI,4,nT],1,nAt-1) +"."+ aFilter[nI,4,nT] + aFilter[nI,3] + AllTrim(aFilter[nI,2])
                            Else
                                cFilter += SubStr(aFilter[nI,4,nT],1,nAt-1) +"."+ aFilter[nI,4,nT] + aFilter[nI,3] + AllTrim(aFilter[nI,2])
                                cFilterSub += "SUB"+SubStr(aFilter[nI,4,nT],1,nAt-1) +"."+ aFilter[nI,4,nT] + aFilter[nI,3] + AllTrim(aFilter[nI,2])
                            EndIf
                            cFilter += If(nT < Len(aFilter[nI,4]), " OR ", "")
                            cFilterSub += If(nT < Len(aFilter[nI,4]), " OR ", "")
                        Next nT
                        cFilter += ")"
                        cFilterSub += ")"
                    Otherwise
                        nAt := At("_",aFilter[nI,1])
                        cFilter += SubStr(aFilter[nI,1],1,nAt-1) +"."+ aFilter[nI,1] + aFilter[nI,3] + aFilter[nI,2]
                        cFilterSub += "SUB"+SubStr(aFilter[nI,1],1,nAt-1) +"."+ aFilter[nI,1] + aFilter[nI,3] + aFilter[nI,2]
                EndCase
            Next nI
        EndIf

        cOrderBy   := If(lAsc,"FLF_DTINI ASC","FLF_DTINI DESC")
        // Trata selecao de paginas
        nInitPage  := (nPage - 1) * (nPageSize)  

        /*--------------------------------------------------------------------------
         Query responsavel por trazer toda a amarracao das prestacoes x viagens 
        --------------------------------------------------------------------------*/
        cQry += "SELECT "
        // Accountability Header
        cQry +=     "FLF.FLF_FILIAL, FLF.FLF_PRESTA, FLF.FLF_DTINI, FLF.FLF_DTFIM, FLF.FLF_NACION, " 
        cQry +=     "FLF.FLF_CLIENT, FLF.FLF_LOJA, FLF.FLF_VIAGEM, FLF.FLF_TIPO, FLF.FLF_PARTIC, " 
        cQry +=     "FLF.FLF_STATUS, FLF.FLF_DTINI, FLF.FLF_DTFIM, FLF.FLF_NACION, FLF.FLF_FATEMP, "
        cQry +=     "FLF.FLF_TDESP1, FLF.FLF_TDESP2, FLF.FLF_TDESP3, " 
        cQry +=     "FLF.FLF_TVLRE1, FLF.FLF_TVLRE2, FLF.FLF_TVLRE3, " 
        cQry +=     "FLF.FLF_TDESC1, FLF.FLF_TDESC2, FLF.FLF_TDESC3, "
        cQry +=     "FLF.FLF_TADIA1, FLF.FLF_TADIA2, FLF.FLF_TADIA3, " 
        cQry +=     "FLF.FLF_MOTVFL, FLF.R_E_C_N_O_ AS IDFLF, "
        // Travels
        cQry +=     "COALESCE(FL5.FL5_FILIAL, ' ') AS FL5_FILIAL, COALESCE(FL5.FL5_VIAGEM, ' ') AS FL5_VIAGEM, "
        cQry +=     "COALESCE(FL5.FL5_NACION, ' ') AS FL5_NACION, COALESCE(FL5.FL5_STATUS, ' ') AS FL5_STATUS, "
        cQry +=     "COALESCE(FL5.FL5_CODORI, ' ') AS FL5_CODORI, COALESCE(FL5.FL5_DESORI, ' ') AS FL5_DESORI, "
        cQry +=     "COALESCE(FL5.FL5_CODDES, ' ') AS FL5_CODDES, COALESCE(FL5.FL5_DESDES, ' ') AS FL5_DESDES, "
        cQry +=     "COALESCE(FL5.FL5_DTINI , ' ') AS FL5_DTINI, COALESCE(FL5.FL5_DTFIM , ' ') AS FL5_DTFIM, "
        cQry +=     "FL5.D_E_L_E_T_, "
        // Client
        cQry +=     "COALESCE(SA1.A1_NOME, ' ') AS A1_NOME, "
        // Cost Centers
        cQry +=     "COALESCE(CTT.CTT_CUSTO, ' ') AS CTT_CUSTO, COALESCE(CTT.CTT_DESC01, ' ') AS CTT_DESC01 "
        // Checked
        If lChecked
            cQry += ", FLN.FLN_TIPO, FLN.FLN_PRESTA, FLN.FLN_PARTIC, FLN.FLN_SEQ, FLN.FLN_APROV "
        EndIf

        cQry += "FROM " +RetSQLName("FLF")+ " FLF "
        If lChecked
            cQry += "INNER JOIN " + RetSqlName("FLN") + " FLN "
            cQry += "ON FLN.FLN_FILIAL = FLF.FLF_FILIAL "
            cQry += "AND FLN.FLN_TIPO = FLF.FLF_TIPO "
            cQry += "AND FLN.FLN_PRESTA = FLF.FLF_PRESTA "
            cQry += "AND FLN.FLN_PARTIC = FLF.FLF_PARTIC "
            cQry += "AND FLN.FLN_STATUS = '1' "
            cQry += "AND FLN.FLN_TPAPR = '1' "
            cQry += "AND FLN.FLN_APROV = '" + aUser[1] + "' " 
            cQry += "AND FLN.D_E_L_E_T_ = ' ' "
        EndIf 

        cQry +=    "LEFT JOIN " +RetSQLName("FL5")+ " FL5 "
        cQry +=    "ON FL5.FL5_FILIAL = FLF.FLF_FILIAL "
        cQry +=    "AND FL5.FL5_VIAGEM = FLF.FLF_VIAGEM "
        cQry +=    "LEFT JOIN " +RetSQLName("SA1")+ " SA1 "
        cQry +=    "ON SA1.A1_FILIAL = '" +cBranchSA1+ "' "

        cQry +=    "AND SA1.A1_COD <> '"+Space(TamSx3("A1_COD")[1])+"' "        
        cQry +=    "AND SA1.A1_COD = FLF.FLF_CLIENT "
        cQry +=    "AND SA1.A1_LOJA = FLF.FLF_LOJA "
        cQry +=    "LEFT JOIN " +RetSQLName("CTT")+ " CTT "
        cQry +=    "ON CTT.CTT_FILIAL = '" +cBranchCTT+ "' "
        cQry +=    "AND CTT.CTT_CUSTO = FLF.FLF_CC "
        cQry +=    "AND CTT.D_E_L_E_T_  = ' ' "

        cQry += "WHERE "
        // Select Pages
        cQry +=     "FLF.R_E_C_N_O_ NOT IN "
        cQry +=     "("
        cQry +=         "SELECT "
        If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
            cQry +=         "TOP " +cValToChar(nInitPage)+ " "
        EndIf
        cQry +=             "SUBFLF.R_E_C_N_O_ "
        cQry +=         "FROM " +RetSQLName("FLF")+ " SUBFLF "
        If lChecked
            cQry += "INNER JOIN " + RetSqlName("FLN") + " SUBFLN "
            cQry += "ON SUBFLN.FLN_FILIAL = SUBFLF.FLF_FILIAL "
            cQry += "AND SUBFLN.FLN_TIPO = SUBFLF.FLF_TIPO "
            cQry += "AND SUBFLN.FLN_PRESTA = SUBFLF.FLF_PRESTA "
            cQry += "AND SUBFLN.FLN_PARTIC = SUBFLF.FLF_PARTIC "
            cQry += "AND SUBFLN.FLN_STATUS = '1' "
            cQry += "AND SUBFLN.FLN_TPAPR = '1' "
            cQry += "AND SUBFLN.FLN_APROV = '" + aUser[1] + "' " 
            cQry += "AND SUBFLN.D_E_L_E_T_ = ' ' "
        EndIf 
        cQry +=             "LEFT JOIN " +RetSQLName("FL5")+ " SUBFL5 "
        cQry +=             "ON SUBFL5.FL5_FILIAL = SUBFLF.FLF_FILIAL "
        cQry +=             "AND SUBFL5.FL5_VIAGEM = SUBFLF.FLF_VIAGEM "
        cQry +=             "LEFT JOIN " +RetSQLName("SA1")+ " SUBSA1 "
        cQry +=             "ON SUBSA1.A1_FILIAL = '" +cBranchSA1+ "' "
        cQry +=             "AND SUBSA1.A1_COD <> '"+Space(TamSx3("A1_COD")[1])+"' "
        cQry +=             "AND SUBSA1.A1_COD = SUBFLF.FLF_CLIENT "
        cQry +=             "AND SUBSA1.A1_LOJA = SUBFLF.FLF_LOJA "
        cQry +=         "WHERE "
        cQry +=             "SUBFLF.FLF_FILIAL = '" +cBranchFLF+ "' "
        IF !Empty( cFilterSub )
            cQry +=         "AND " + cFilterSub + " 
        Endif 
        cQry += " AND SUBFLF.D_E_L_E_T_ = ' ' " 
        cQry +=             "AND (SUBFL5.D_E_L_E_T_ = ' ' OR SUBFL5.D_E_L_E_T_ IS NULL) "
        If cSGBD $ "ORACLE"
            cQry +=         "AND ROWNUM < " +cValToChar(nInitPage)+ " "
        ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
            cQry +=     "LIMIT " +cValToChar(nInitPage)+ " "
        EndIf
        cQry +=     ") " // End Select Pages
        cQry +=     "AND FLF.FLF_FILIAL = '" +cBranchFLF+ "' "
        IF !Empty( cFilter )
            cQry +=     "AND " + cFilter + " 
        EndIf
        cQry +=     " AND FLF.D_E_L_E_T_ = ' ' "
        cQry +=     "AND (FL5.D_E_L_E_T_ = ' ' OR FL5.D_E_L_E_T_ IS NULL) "
        cQry += "ORDER BY "
        cQry +=     "FLF." +cOrderBy+ ", FLF.R_E_C_N_O_"
        cQry := ChangeQuery(cQry)

        MPSysOpenQuery(cQry, cTmp)
        dbSelectArea(cTmp)

        oExpenses["userName"]:= aUser[2]  // Nome do usu�rio logado
        oExpenses["hasNext"]:= .F.  // Propriedade para controle de paginas
        oExpenses["expenses"]:= {}  // Array para composi�ao das presta�oes de contas

        If !(cTmp)->(EoF())
            /*  aDePara = Array com campos do mobile e do protheus (De/Para)
                aDePara[n][1] = campo ou objeto do request
                aDePara[n][2] = se array 
                aDePara[n][3] = conteudo a ser atribuido
                aDePara[n][4] = se objeto JSON (.T. ou .F.)
                aDePara[n][5] = se propriedade de objeto JSON (.T. ou .F.)
                aDePara[n][6] = propriedades do array (olha para posicao 3)
            */
            Aadd(aDePara,{"id",,"FLF_PRESTA",.F.,.F.})
            Aadd(aDePara,{"client",,,.T.,.F.})
            Aadd(aDePara,{"client.name",,"EncodeUTF8(A1_NOME)",.F.,.T.})
            Aadd(aDePara,{"client.id",,"FLF_CLIENT",.F.,.T.})
            Aadd(aDePara,{"client.unit",,"FLF_LOJA",.F.,.T.})
            Aadd(aDePara,{"reason",,"",.F.,.F.})
            Aadd(aDePara,{"destination",,,.T.,.F.})
            Aadd(aDePara,{"destination.international",,"FLF_NACION == '2'",.F.,.T.})
            Aadd(aDePara,{"destination.name",,"EncodeUTF8(FL5_DESDES)",.F.,.T.})
            Aadd(aDePara,{"destination.id",,"FL5_CODDES",.F.,.T.})
            Aadd(aDePara,{"travel",,"AllTrim(FL5_VIAGEM) <> ''",.F.,.F.})
            Aadd(aDePara,{"travelNumber",,"If(AllTrim(FL5_VIAGEM) <> '',FL5_VIAGEM,Nil)",.F.,.F.})
            Aadd(aDePara,{"departure_date",,"FLF_DTINI",.F.,.F.})
            Aadd(aDePara,{"arrival_date",,"FLF_DTFIM",.F.,.F.})
            Aadd(aDePara,{"status",,"FLF_STATUS",.F.,.F.})
            Aadd(aDePara,{"balances",,,.T.,.F.})
            Aadd(aDePara,{"balances.advance_money",.T.,"Loadvance(FL5_FILIAL,FL5_VIAGEM)",.F.,.F.,{"current","dolar","euro","date", "status"}})
            Aadd(aDePara,{"balances.refundable_expenses",,,.T.,.F.})
            Aadd(aDePara,{"balances.refundable_expenses.current",,"RetTMobile()",.F.,.T.})
            Aadd(aDePara,{"balances.refundable_expenses.dolar",,"FLF_TVLRE2",.F.,.T.})
            Aadd(aDePara,{"balances.refundable_expenses.euro",,"FLF_TVLRE3",.F.,.T.})
            Aadd(aDePara,{"balances.non_refundable_expenses",,,.T.,.F.})
            Aadd(aDePara,{"balances.non_refundable_expenses.current",,"RetTMobile() - FLF_TVLRE1",.F.,.T.})
            Aadd(aDePara,{"balances.non_refundable_expenses.dolar",,"FLF_TDESP1 - FLF_TVLRE1",.F.,.T.})
            Aadd(aDePara,{"balances.non_refundable_expenses.euro",,"FLF_TDESP1 - FLF_TVLRE1",.F.,.T.})
            Aadd(aDePara,{"balances.refund",,,.T.,.F.})
            Aadd(aDePara,{"balances.refund.current",,"RetTMobile() - FLF_TDESC1 - nTotalAdv1",.F.,.T.})
            Aadd(aDePara,{"balances.refund.dolar",,"FLF_TVLRE2 - FLF_TDESC2 - nTotalAdv2",.F.,.T.})
            Aadd(aDePara,{"balances.refund.euro",,"FLF_TVLRE3 - FLF_TDESC3 - nTotalAdv3",.F.,.T.})
            Aadd(aDePara,{"reason_for_refusal",,"EncodeUTF8(FLF_MOTVFL)",.F.,.F.})
            Aadd(aDePara,{"company_billing_percent",,"FLF_FATEMP",.F.,.F.})
            Aadd(aDePara,{"cc_code",,"CTT_CUSTO",.F.,.F.})
            Aadd(aDePara,{"cc_description",,"EncodeUTF8(CTT_DESC01)",.F.,.F.})

            For nI := 1 to nPageSize
                If (cTmp)->(EoF())
                    Exit
                Else
                    // Carrega objeto com os valores da query
                    oJsonTmp := JSONFromTo(aFields,aDePara)

                    // Carrega as informa��es do memo
                    FLF->(DbGoto((cTmp)->IDFLF))
                    oJsonTmp["reason"] := AllTrim(EncodeUTF8(FLF->FLF_MOTIVO))
        
                    Aadd(oExpenses["expenses"],oJsonTmp)
                    (cTmp)->(dbSkip())
                EndIf
            Next nI
            If !(cTmp)->(EoF())
                oExpenses["hasNext"] := .T.
            EndIf
        EndIf
        (cTmp)->(dbCloseArea())
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadItems
    Carrega itens da prestacao de contas
    @author Igor Sousa do Nascimento
    @since 25/08/2017
/*/
//-------------------------------------------------------------------
Static Function LoadItems( nPage, nPageSize, aFields, oItems, cIDFLE, cTipo, lChecked )

    Local aUser      As Array
    Local aDePara    As Array
    Local cTmp       As Character
    Local cQry       As Character
    Local cSGBD      As Character
    Local cBranchFLE As Character
    Local cBranchFLG As Character
    Local cBranchFLS As Character
    Local cBranchSX5 As Character
    Local cFilter    As Character
    Local cSubFilter As Character
    Local lRet       As Logical
    Local nInitPage  As Numeric
    Local nI         As Numeric
    Local nY         As Numeric
    Local oJsonTmp   As Object
    Local oMessages  As Object
    Local lNote      As Logical

    Default nPage     := 1
    Default nPageSize := 10
    Default aFields   := {  "id","date","local","type","currency","quantity",;
                            "converstion_rate","total_value","attachment", "note"   }
    Default oItems    := JsonObject():New()
    Default cIDFLE    := ""
    Default cTipo     := ""
    Default lChecked  := .F.

    lRet       := .T.
    // Trata response negativo
    oMessages  := JsonObject():New()
    aDePara    := {}

    If FINXUser(__cUserID,@aUser,.F.)

        cTmp       := CriaTrab(,.F.)
        cQry       := ""
        cSGBD      := Alltrim(Upper(TCGetDB()))
        cBranchFLE := xFilial("FLE",cFilAnt)
        cBranchFLG := xFilial("FLG",cBranchFLE)
        cBranchFLS := xFilial("FLS",cBranchFLE)
        cBranchSX5 := xFilial("SX5",cBranchFLE)
        lNote      := FLE->(ColumnPos("FLE_OBS")) > 0

        If !lChecked
            cFilter    := "FLE.FLE_PARTIC = '" + aUser[1] + "' "
            cSubFilter := "SUBFLE.FLE_PARTIC = '" + aUser[1] + "' "
        else
            cFilter    := ''
            cSubFilter := ''
        EndIf

        // Trata selecao de paginas
        nInitPage  := (nPage - 1) * (nPageSize)  

        /*--------------------------------------------------------------------------
        Query responsavel por trazer toda a amarra�?o das presta�?es x viagens 
        --------------------------------------------------------------------------*/
        cQry += "SELECT "
        // Accountability Grid
        cQry +=     "FLE.FLE_PRESTA, FLE.FLE_ITEM, FLE.FLE_LOCAL, FLE.FLE_DESPES, FLE.FLE_QUANT, "
        cQry +=     "FLE.FLE_TOTAL, FLE.FLE_TXCONV, FLE.FLE_VALREE, FLE.FLE_VALNRE, FLE.FLE_DESCON, "
        cQry +=     "FLE.FLE_MOEDA, FLE.FLE_DETDES, FLE.FLE_VALUNI, FLE.FLE_DATA, FLE.FLE_PARTIC, "
        cQry +=     "FLE.FLE_TIPO, FLE.R_E_C_N_O_, "
        
        If lNote
            cQry += "FLE.FLE_OBS, "
        EndIf

        // Expense Types
        cQry +=     "FLG.FLG_DESCRI, "
        cQry +=     "FLS_VALUNI, FLS_DTINI, FLS_DTFIM, "
        // Location
        cQry +=     "SX5.X5_DESCRI "
        cQry += "FROM " +RetSQLName("FLE")+ " FLE "
        cQry +=     "INNER JOIN " +RetSQLName("FLG")+ " FLG "
        cQry +=     "ON FLG.FLG_FILIAL = '" +cBranchFLG+ "' "
        cQry +=     "AND FLG.FLG_CODIGO = FLE.FLE_DESPES "
        cQry +=     "INNER JOIN " +RetSQLName("SX5")+ " SX5 "
        cQry +=     "ON SX5.X5_FILIAL = '" + cBranchSX5 + "' "
        cQry +=     "AND SX5.X5_TABELA IN ('12','BH') " 
        cQry +=     "AND SX5.X5_CHAVE = FLE.FLE_LOCAL "
        
        If lChecked
            cQry += "INNER JOIN " + RetSqlName("FLN") + " FLN "
            cQry += "ON FLN.FLN_FILIAL = FLE.FLE_FILIAL "
            cQry += "AND FLN.FLN_TIPO = FLE.FLE_TIPO "
            cQry += "AND FLN.FLN_PRESTA = FLE.FLE_PRESTA "
            cQry += "AND FLN.FLN_PARTIC = FLE.FLE_PARTIC "
            cQry += "AND FLN.FLN_STATUS = '1' "
            cQry += "AND FLN.FLN_TPAPR = '1' "
            cQry += "AND FLN.FLN_APROV = '" + aUser[1] + "' " 
            cQry += "AND FLN.D_E_L_E_T_ = ' ' "
        EndIf 
        
        cQry +=     "LEFT JOIN " + RetSQLName("FLS") + " FLS " 
        cQry +=     "ON FLS.FLS_FILIAL = '" + cBranchFLS + "' "
        cQry +=     "AND FLS.FLS_CODIGO = FLG.FLG_CODIGO "
        cQry +=     "AND FLS.FLS_DTINI <= '" + dtos(ddatabase) + "' "
        cQry +=     "AND (FLS.FLS_DTFIM >= '" + dtos(ddatabase) + "' OR FLS.FLS_DTFIM = ' ') "
        cQry +=     "AND FLS.D_E_L_E_T_ = ' ' "
        cQry += "WHERE "
        // Select Pages
        cQry +=     "FLE.R_E_C_N_O_ NOT IN "
        cQry +=     "( "
        cQry +=         "SELECT "
        If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
            cQry +=         "TOP " +cValToChar(nInitPage)+ " " 
        EndIf
        cQry +=             "SUBFLE.R_E_C_N_O_ "
        cQry +=         "FROM " +RetSQLName("FLE")+ " SUBFLE "
        cQry +=             "INNER JOIN " +RetSQLName("FLG")+ " SUBFLG "
        cQry +=             "ON SUBFLG.FLG_FILIAL = '" +cBranchFLG+ "' "
        cQry +=             "AND SUBFLG.FLG_CODIGO = SUBFLE.FLE_DESPES "
        cQry +=             "INNER JOIN " +RetSQLName("SX5")+ " SUBSX5 "
        cQry +=             "ON SUBSX5.X5_FILIAL = '" + cBranchSX5 + "' "
        cQry +=             "AND SUBSX5.X5_TABELA IN ('12','BH') " 
        cQry +=             "AND SUBSX5.X5_CHAVE = SUBFLE.FLE_LOCAL "
        
        If lChecked
            cQry += "INNER JOIN " + RetSqlName("FLN") + " SUBFLN "
            cQry += "ON SUBFLN.FLN_FILIAL = SUBFLE.FLE_FILIAL "
            cQry += "AND SUBFLN.FLN_TIPO = SUBFLE.FLE_TIPO "
            cQry += "AND SUBFLN.FLN_PRESTA = SUBFLE.FLE_PRESTA "
            cQry += "AND SUBFLN.FLN_PARTIC = SUBFLE.FLE_PARTIC "
            cQry += "AND SUBFLN.FLN_STATUS = '1' "
            cQry += "AND SUBFLN.FLN_TPAPR = '1' "
            cQry += "AND SUBFLN.FLN_APROV = '" + aUser[1] + "' " 
            cQry += "AND SUBFLN.D_E_L_E_T_ = ' ' "
        EndIf 
        
        cQry +=             "LEFT JOIN " + RetSQLName("FLS") + " SUBFLS " 
        cQry +=             "ON SUBFLS.FLS_FILIAL = '" + cBranchFLS + "' "
        cQry +=             "AND SUBFLS.FLS_CODIGO = FLG_CODIGO "
        cQry +=             "AND SUBFLS.FLS_DTINI <= '" + dtos(ddatabase) + "' "
        cQry +=             "AND (SUBFLS.FLS_DTFIM >= '" + dtos(ddatabase) + "' OR SUBFLS.FLS_DTFIM = ' ') "
        cQry +=             "AND SUBFLS.D_E_L_E_T_ = ' ' "
        cQry +=         "WHERE "
        cQry +=             "SUBFLE.FLE_FILIAL = '" +cBranchFLE+ "' " 
        cQry +=             "AND SUBFLE.FLE_PRESTA = '" +cIDFLE+ "' "
        cQry +=             "AND SUBFLE.FLE_TIPO = '" +cTipo+ "' "
        
        IF !Empty( cSubFilter )
            cQry +=         "AND " + cSubFilter + " 
        EndIf

        cQry +=             " AND SUBFLE.D_E_L_E_T_ = ' ' AND SUBFLG.D_E_L_E_T_ = ' ' "
        cQry +=             "AND SUBSX5.D_E_L_E_T_ = ' ' "
        If cSGBD $ "ORACLE"
            cQry +=         "AND ROWNUM < " +cValToChar(nInitPage)+ " "
        ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
            cQry +=     "LIMIT " +cValToChar(nInitPage)+ " "
        EndIf
        cQry +=     ") "
        cQry +=     "AND FLE.FLE_FILIAL = '" +cBranchFLE+ "' " 
        cQry +=     "AND FLE.FLE_PRESTA = '" +cIDFLE+ "' "
        cQry +=     "AND FLE.FLE_TIPO = '" +cTipo+ "' "

        IF !Empty( cFilter )
            cQry +=     "AND " + cFilter + " 
        EndIf

        cQry +=     " AND FLE.D_E_L_E_T_ = ' ' AND FLG.D_E_L_E_T_ = ' ' "
        cQry +=     "AND SX5.D_E_L_E_T_ = ' ' "

        cQry += "ORDER BY "
        cQry +=     "FLE.FLE_DATA ASC, FLE.R_E_C_N_O_"
        cQry := ChangeQuery(cQry)

        MPSysOpenQuery(cQry, cTmp)
        dbSelectArea(cTmp)

        oItems["hasNext"]:= .F.  // Propriedade para controle de paginas
        oItems["items"]:= {}  // Array para composi�ao das presta�oes de contas

        If !(cTmp)->(EoF())
            /*  aDePara = Array com campos do mobile e do protheus (De/Para)
                aDePara[n][1] = campo ou objeto do request
                aDePara[n][2] = se array ou propriedade de array (criar propriedades na funcao JSONFromTo)
                aDePara[n][3] = array com campos (ou conteudo) Protheus
                aDePara[n][4] = se objeto JSON (.T. ou .F.)
                aDePara[n][5] = se propriedade de objeto JSON (.T. ou .F.)
            */
            Aadd(aDePara,{"id",,"FLE_ITEM",.F.,.F.})
            Aadd(aDePara,{"date",,"FLE_DATA",.F.,.F.})
            Aadd(aDePara,{"local",,,.T.,.F.})
            Aadd(aDePara,{"local.id",,"FLE_LOCAL",.F.,.T.})
            Aadd(aDePara,{"local.name",,"EncodeUTF8(X5_DESCRI)",.F.,.T.})
            Aadd(aDePara,{"type",,,.T.,.F.})
            Aadd(aDePara,{"type.id",,"FLE_DESPES",.F.,.T.})
            Aadd(aDePara,{"type.description",,"EncodeUTF8(FLG_DESCRI)",.F.,.T.})
            Aadd(aDePara,{"type.unit_value",,"FLS_VALUNI",.F.,.T.})
            Aadd(aDePara,{"currency",,,.T.,.F.})
            Aadd(aDePara,{"currency.id",,"FLE_MOEDA",.F.,.T.})
            Aadd(aDePara,{"currency.description",,"",.F.,.T.})
            Aadd(aDePara,{"currency.symbol",,"",.F.,.T.})
            Aadd(aDePara,{"quantity",,"FLE_QUANT",.F.,.F.})
            Aadd(aDePara,{"converstion_rate",,"FLE_TXCONV",.F.,.F.})
            Aadd(aDePara,{"total_value",,"FLE_TOTAL",.F.,.F.})
            Aadd(aDePara,{"attachment",,"HasAttch()",.F.,.F.})

            If lNote
                Aadd(aDePara,{"note",,"EncodeUTF8(AllTrim(FLE_OBS))",.F.,.F.})
            Endif

            For nI := 1 to nPageSize
                If (cTmp)->(EoF())
                    Exit
                Else
                    oJsonTmp := JsonObject():New()
                    // Texto descritivo da moeda e Simbolo
                    Do Case
                        Case (cTmp)->FLE_MOEDA == "1"   
                            nY := AScan(aDePara,{|x| x[1] == "currency.description" })
                            aDePara[nY][3]:= "'Real'"
                            nY := AScan(aDePara,{|x| x[1] == "currency.symbol" })
                            aDePara[nY][3]:= "'R$'"
                        Case (cTmp)->FLE_MOEDA == "2"   
                            nY := AScan(aDePara,{|x| x[1] == "currency.description" })
                            aDePara[nY][3]:= "'Dolar'"
                            nY := AScan(aDePara,{|x| x[1] == "currency.symbol" })
                            aDePara[nY][3]:= "'U$'"
                        Case (cTmp)->FLE_MOEDA == "3"   
                            nY := AScan(aDePara,{|x| x[1] == "currency.description" })
                            aDePara[nY][3]:= "'Euro'"
                            nY := AScan(aDePara,{|x| x[1] == "currency.symbol" })
                            aDePara[nY][3]:= "'?'"
                        Case (cTmp)->FLE_MOEDA == "9"   
                            nY := AScan(aDePara,{|x| x[1] == "currency.description" })
                            aDePara[nY][3]:= "'Outras'"
                            nY := AScan(aDePara,{|x| x[1] == "currency.symbol" })
                            aDePara[nY][3]:= "''"
                    EndCase
                    // Carrega objeto com campos do usuario
                    oJsonTmp := JSONFromTo(aFields,aDePara)
					Aadd(oItems["items"],oJsonTmp)
                    (cTmp)->(dbSkip())
                EndIf
            Next nI
            
            If !(cTmp)->(EoF())
                oItems["hasNext"] := .T.
            EndIf
        EndIf
        (cTmp)->(dbCloseArea())
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oItems["messages"] := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Loadvance
    Carrega informacoes dos adiantamentos da viagem
    @author Igor Sousa do Nascimento
    @since 23/08/2017
/*/
//-------------------------------------------------------------------
Function Loadvance(cBranchFL5,cIDFL5)

    Local aArea     As Array
    Local aAdvances As Array
    Local aUser     As Array
    Local cTmp      As Character

    Default cBranchFL5 := xFilial("FL5",cFilAnt)
    Default cIDFL5     := ""

    aAdvances  := {}
    aArea      := GetArea()
    cTmp       := CriaTrab(,.F.)
    cBranchFL5 := xFilial("FL5",cFilAnt)

    If FINXUser(__cUserID,@aUser,.F.)

        BEGINSQL ALIAS cTmp
            SELECT
                FLD.FLD_VIAGEM, FLD.FLD_ADIANT, 
                FLD.FLD_DTPREV, FLD_MOEDA, FLD_VALOR, FLD_STATUS

            FROM %Table:FLD% FLD

            WHERE
                FLD.FLD_FILIAL = %exp:cBranchFL5%
                AND FLD.FLD_VIAGEM = %exp:cIDFL5%
                AND FLD.FLD_PARTIC = %exp:aUser[1]%
                AND FLD.%NotDel%
        ENDSQL 

        dbSelectArea(cTmp)

        While !(cTmp)->(EoF())
            Aadd(aAdvances,{ If((cTmp)->FLD_MOEDA=="1",(cTmp)->FLD_VALOR,0), If((cTmp)->FLD_MOEDA=="2",(cTmp)->FLD_VALOR,0), If((cTmp)->FLD_MOEDA=="3", (cTmp)->FLD_VALOR,0), (cTmp)->FLD_DTPREV, (cTmp)->FLD_STATUS  })
            (cTmp)->(dbSkip())
        EndDo

        (cTmp)->(dbCloseArea())

        RestArea(aArea)

    EndIf

Return aAdvances

//-------------------------------------------------------------------
/*/{Protheus.doc} NewXpense
    Insere nova prestacao de contas Avulsa
    @author Igor Sousa do Nascimento
    @since 25/08/2017
/*/
//-------------------------------------------------------------------
Static Function NewXpense(oExpenses,cBody)
    
    Local aUser     As Array
    Local cIDFLF    As Character
    Local cCatch    As Character
    Local dDataIni  As Date
    Local dDataFim  As Date
    Local dEmissao  As Date
    Local dXpense   As Date
    Local lRet      As Logical
    Local lNote     As Logical
    Local nI        As Numeric
    Local oJsonTmp  As Object
    Local oMessages As Object
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oModelFLF As Object
    Local oModelTot As Object

    Default oExpenses := JsonObject():New()
    Default cBody     := ""

    lRet      := .T.
    oMessages := JsonObject():New()

    If FINXUser(__cUserID,@aUser,.F.)
        oJsonTmp := JsonObject():New()
        cCatch   := oJsonTmp:FromJSON(cBody)
        If cCatch == Nil    
            lNote := FLE->(ColumnPos("FLE_OBS")) > 0

            dbSelectArea("FLF")
            FLF->(dbSetOrder(1))  // FLF_FILIAL, FLF_TIPO, FLF_PRESTA, FLF_PARTIC
        
            oModel677:= FWLoadModel("FINA677")
            oModel677:SetOperation(MODEL_OPERATION_INSERT) 
            F677LoadMod(oModel677,MODEL_OPERATION_INSERT)
            oModelFLE:= oModel677:GetModel("FLEDETAIL")
            oModelFLF:= oModel677:GetModel("FLFMASTER")
            oModelTot:= oModel677:GetModel("TOTAL")
            cIDFLF   := oModelFLF:GetValue("FLF_PRESTA")

            dDataIni := StoD(oJsonTmp["departure_date"])
            dDataFim := StoD(oJsonTmp["arrival_date"])
            dEmissao := StoD(oJsonTmp["emission"])
            oModelFLF:SetValue("FLF_FILIAL", xFilial("FLF"))
            oModelFLF:SetValue("FLF_TIPO"  , "2")
            oModelFLF:SetValue("FLF_FATEMP", oJsonTmp["company_billing_percent"])
            oModelFLF:SetValue("FLF_FATCLI", 100-oJsonTmp["company_billing_percent"])
            oModelFLF:SetValue("FLF_STATUS", "1")   // Em Aberto
            oModelFLF:SetValue("FLF_EMISSA", dEmissao)
            oModelFLF:SetValue("FLF_DTINI" , dDataIni)
            oModelFLF:SetValue("FLF_DTFIM" , dDataFim)
            oModelFLF:SetValue("FLF_NACION", If(oJsonTmp["international"],"2","1"))        // 1 - Nacional | 2 - Internacional
            oModelFLF:SetValue("FLF_MOTIVO", DecodeUTF8( oJsonTmp["reason"] ) )
            oModelFLF:SetValue("FLF_CLIENT", oJsonTmp["client"])
            oModelFLF:SetValue("FLF_LOJA"  , oJsonTmp["unit"])
            oModelFLF:SetValue("FLF_CC"    , oJsonTmp["cc_code"])
            For nI := 1 to Len(oJsonTmp["items"])
                dXpense  := StoD(oJsonTmp["items"][nI]["date"])
                oModelFLE:SetValue("FLE_FILIAL", xFilial("FLE"))
                oModelFLE:SetValue("FLE_ITEM"  , StrZero(nI,TamSX3("FLE_ITEM")[1]))
                oModelFLE:SetValue("FLE_TIPO"  , "2")
                oModelFLE:SetValue("FLE_PRESTA", cIDFLF)
                oModelFLE:SetValue("FLE_PARTIC", aUser[1])
                oModelFLE:SetValue("FLE_DATA"  , dXpense)
                oModelFLE:SetValue("FLE_LOCAL" , oJsonTmp["items"][nI]["local"])
                oModelFLE:SetValue("FLE_DESPES", oJsonTmp["items"][nI]["type"])
                oModelFLE:SetValue("FLE_MOEDA" , oJsonTmp["items"][nI]["currency"])
                oModelFLE:SetValue("FLE_QUANT" , oJsonTmp["items"][nI]["quantity"])
                If oJsonTmp["items"][nI]["currency"] <> '1'
                    oModelFLE:SetValue("FLE_TXCONV", oJsonTmp["items"][nI]["conversion_rate"])
                EndIf
                
                If lNote .and. ValType(oJsonTmp["items"][nI]["note"]) <> 'U'
                    oModelFLE:SetValue("FLE_OBS" , DecodeUTF8(oJsonTmp["items"][nI]["note"]))
                EndIf

                oModelFLE:SetValue("FLE_TOTAL" , oJsonTmp["items"][nI]["total_value"])
                
                If nI < Len(oJsonTmp["items"])
                    oModelFLE:AddLine()
                EndIf
            Next nI

            If !oModel677:VldData()
                oMessages["code"] 	:= "400"
                oMessages["message"]	:= "Bad Request"
                oMessages["detailMessage"] := cValToChar(oModel677:GetErrorMessage()[6])
                lRet := .F.
            Else
                oModel677:CommitData()
                oExpenses["id"] := cIDFLF
            EndIf
            oModel677:DeActivate()
            oModel677:Destroy()
            oModel677 := NIL
        Else
            oMessages["code"] 	:= "400"
            oMessages["message"]	:= "Bad Request"
            oMessages["detailMessage"] := cCatch
            lRet := .F.
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} NewItem
    Insere nova despesa na prestacao de contas
    @author Igor Sousa do Nascimento
    @since 31/08/2017
/*/
//-------------------------------------------------------------------
Static Function NewItem(oExpenses,cBody,cIDFLF,cTipo)

    Local aUser     As Array
    Local cItem     As Character
    Local cCatch    As Character
    Local dData     As Date
    Local lRet      As Logical
    Local lNote     As Logical
    Local oJsonTmp  As Object
    Local oMessages As Object
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oModelFLF As Object
    Local oModelTot As Object

    Default oExpenses := JsonObject():New()
    Default cBody     := ""
    Default cIDFLF    := ""
    Default cTipo     := ""

    lRet      := .T.
    oMessages := JsonObject():New()

    If FINXUser(__cUserID,@aUser,.F.)
        oJsonTmp := JsonObject():New()
        cCatch   := oJsonTmp:FromJSON(cBody)

        If cCatch == Nil
            dbSelectArea("FLF")
            FLF->(dbSetOrder(1))  // FLF_FILIAL, FLF_TIPO, FLF_PRESTA, FLF_PARTIC

            If  dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1])
                If FLF->FLF_STATUS == "1" .or. FLF->FLF_STATUS == "5"   // Aberta ou Reprovada
                    lNote := FLE->(ColumnPos("FLE_OBS")) > 0

                    oModel677:= FWLoadModel("FINA677")
                    oModel677:SetOperation(MODEL_OPERATION_UPDATE)  
                    F677LoadMod(oModel677,MODEL_OPERATION_UPDATE)
                    oModel677:Activate()
                    oModelFLF:= oModel677:GetModel("FLFMASTER")
                    oModelFLE:= oModel677:GetModel("FLEDETAIL")
                    oModelTot:= oModel677:GetModel("TOTAL")

                    dData  := StoD(oJsonTmp["date"])
                    cItem  := StrZero(oModelFLE:Length(),TamSX3("FLE_ITEM")[1])
                    If oModelFLE:Length() > 1
                        oModelFLE:GoLine(oModelFLE:Length())
                        cItem := oModelFLE:GetValue("FLE_ITEM")
                        oModelFLE:AddLine()
                    Else
                        If oModelFLE:Length() > 0
                            oModelFLE:GoLine(oModelFLE:Length())
                            cItem := oModelFLE:GetValue("FLE_ITEM")
                            If oModelFLE:GetValue("FLE_TOTAL") > 0 
                                oModelFLE:AddLine()
                            EndIf
                        EndIf
                    EndIf
                    oModelFLE:SetValue("FLE_FILIAL", xFilial("FLE"))
                    oModelFLE:SetValue("FLE_ITEM"  , Soma1(cItem))
                    oModelFLE:SetValue("FLE_DATA"  , dData)
                    oModelFLE:SetValue("FLE_LOCAL" , oJsonTmp["local"])
                    oModelFLE:SetValue("FLE_DESPES", oJsonTmp["type"])
                    oModelFLE:SetValue("FLE_MOEDA" , oJsonTmp["currency"])
                    oModelFLE:SetValue("FLE_QUANT" , oJsonTmp["quantity"])
                    If oJsonTmp["currency"] <> '1' .and. ValType(oJsonTmp["conversion_rate"]) <> 'U'
                        oModelFLE:SetValue("FLE_TXCONV", oJsonTmp["conversion_rate"])
                    EndIf
                    
                    If lNote .and. ValType(oJsonTmp["note"]) <> 'U'
                        oModelFLE:SetValue("FLE_OBS" , DecodeUtf8(oJsonTmp["note"]))
                    EndIf

                    oModelFLE:SetValue("FLE_TOTAL" , oJsonTmp["total_value"])

                    If !oModel677:VldData()
                        oMessages["code"] 	:= "400"
                        oMessages["message"]	:= "Bad Request"
                        oMessages["detailMessage"] := cValToChar(oModel677:GetErrorMessage()[6])
                        lRet := .F.
                    Else
                        oModel677:CommitData()
                        oExpenses["id"]     := oModelFLE:GetValue("FLE_ITEM")
                    EndIf
                    oModel677:DeActivate()
                    oModel677:Destroy()
                    oModel677 := NIL
                Else
                    oMessages["code"] 	:= "403"
                    oMessages["message"]	:= "Forbidden"
                    oMessages["detailMessage"] :=  STR0006  // "Status do registro n?o permite altera�?o / exclus?o."
                    lRet := .F.
                EndIf
            EndIf
        Else
            oMessages["code"] 	:= "400"
            oMessages["message"]	:= "Bad Request"
            oMessages["detailMessage"] := cCatch
            lRet := .F.
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} UpdXpense
    Alteracao da prestacao de contas e item
    @author Igor Sousa do Nascimento
    @since 31/08/2017
/*/
//-------------------------------------------------------------------
Static Function UpdXpense(oExpenses,cBody,cIDFLF,cTipo,cItem,cAnexo)

    Local aUser     As Array
    Local aFields   As Array
    Local cCatch    As Character
    Local lRet      As Logical
    Local lNote     As Logical
    Local oJsonTmp  As Object
    Local oMessages As Object
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oModelFLF As Object
    Local nR := 0

    Default oExpenses := JsonObject():New()
    Default cBody     := ""
    Default cIDFLF    := ""
    Default cTipo     := ""
    Default cItem     := ""
    Default cAnexo    := .F.

    lRet      := .T.
    oMessages := JsonObject():New()

    If FINXUser(__cUserID,@aUser,.F.)
        oJsonTmp := JsonObject():New()
        cCatch   := oJsonTmp:FromJSON(cBody)

        If cCatch == Nil
            dbSelectArea("FLE")
            FLE->(dbSetOrder(1))

            dbSelectArea("FLF")
            FLF->(dbSetOrder(1))

            If FLF->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1]))
                aFields := oJsonTmp:GetProperties()
                lNote := FLE->(ColumnPos("FLE_OBS")) > 0

                If FLF->FLF_STATUS == "1" .or. FLF->FLF_STATUS == "5"   // Aberta ou Reprovada
                    If FLE->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1] + cItem))
                        cItemNew := cItem
                        Begin Transaction
                            oModel677:= FWLoadModel("FINA677")
                            oModel677:SetOperation(MODEL_OPERATION_UPDATE)   
                            F677LoadMod(oModel677,MODEL_OPERATION_UPDATE)
                            oModel677:Activate()

                            oModelFLF:= oModel677:GetModel("FLFMASTER")
                            oModelFLE:= oModel677:GetModel("FLEDETAIL")

                            For nR := 1 To oModelFLE:Length()
                                oModelFLE:GOLINE(nR)
                                If AllTrim(oModelFLE:GetValue("FLE_ITEM")) == AllTrim(cItem)
                                    Exit    
                                EndIf
                            Next nR

                            If ( AScan(aFields,{|x| x ='date'}) ) > 0
                                dXpense  := StoD(oJsonTmp["date"])
                                oModelFLE:SetValue("FLE_DATA"  , dXpense)
                            EndIf
                            If ( AScan(aFields,{|x| x ='type'}) ) > 0
                                oModelFLE:SetValue("FLE_DESPES", oJsonTmp["type"])
                            EndIf
                            If ( AScan(aFields,{|x| x ='currency'}) ) > 0
                                oModelFLE:SetValue("FLE_MOEDA" , oJsonTmp["currency"])
                            EndIf
                            If ( AScan(aFields,{|x| x ='quantity'}) ) > 0
                                oModelFLE:SetValue("FLE_QUANT" , oJsonTmp["quantity"])
                            EndIf
                            If ( AScan(aFields,{|x| x ='conversion_rate'}) ) > 0
                                oModelFLE:SetValue("FLE_TXCONV", oJsonTmp["conversion_rate"])
                            EndIf
                            If ( AScan(aFields,{|x| x ='local'}) ) > 0
                                oModelFLE:SetValue("FLE_LOCAL" , oJsonTmp["local"])
                            EndIf
                            If ( AScan(aFields,{|x| x ='total_value'}) ) > 0
                                oModelFLE:SetValue("FLE_TOTAL" , oJsonTmp["total_value"])
                            EndIf              
                            If lNote .and. ( AScan(aFields,{|x| x ='note'}) ) > 0 
                                oModelFLE:SetValue("FLE_OBS" , DecodeUtf8(oJsonTmp["note"]))
                            EndIf            

                            oModelFLF:SetValue("FLF_STATUS", "1")  // Retorna status para "em aberto"

                            If !oModel677:VldData()
                                oMessages["code"] 	:= "400"
                                oMessages["message"]	:= "Bad Request"
                                oMessages["detailMessage"] := cValToChar(oModel677:GetErrorMessage()[6])
                                lRet := .F.
                            Else
                                oModel677:CommitData()
                            EndIf
                            oModel677:DeActivate()
                            oModel677:Destroy()
                            oModel677 := NIL
                        End Transaction
                    EndIf
                Else
                    oMessages["code"] 	:= "403"
                    oMessages["message"]	:= "Forbidden"
                    oMessages["detailMessage"] := STR0006  // "Status do registro n?o permite altera�?o / exclus?o."
                    lRet := .F.
                EndIf
            EndIf
        Else
            oMessages["code"] 	:= "400"
            oMessages["message"]	:= "Bad Request"
            oMessages["detailMessage"] := cCatch
            lRet := .F.
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oExpenses := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DelXpense
    Deleta registro da FLF/FLE
    @author Igor Sousa do Nascimento
    @since 30/08/2017
/*/
//-------------------------------------------------------------------
Static Function DelXpense(oResponse,cIDFLF,cTipo)

    Local aUser     As Array
    Local lRet      As Logical
    Local oMessages As Object
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oModelFLF As Object

    Default oResponse := JsonObject():New()
    Default cIDFLF    := ""
    Default cTipo     := ""

    lRet      := .T.
    oMessages := JsonObject():New()

    If FINXUser(__cUserID,@aUser,.F.)
        // Deleta prestacao e item
        dbSelectArea("FLE")
        FLE->(dbSetOrder(1))

        dbSelectArea("FLF")
        FLF->(dbSetOrder(1))

        If FLF->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1]))
            Begin Transaction  
                oModel677:= FWLoadModel("FINA677")
                oModel677:SetOperation(MODEL_OPERATION_DELETE)  
                oModel677:Activate()
                oModelFLE:= oModel677:GetModel("FLEDETAIL")
                oModelFLF:= oModel677:GetModel("FLFMASTER")

                If !oModel677:VldData()
                    oMessages["code"] 	:= "400"
                    oMessages["message"]	:= "Bad Request"
                    oMessages["detailMessage"] := cValToChar(oModel677:GetErrorMessage()[6])
                    lRet := .F.
                Else
                    oModel677:CommitData()
                EndIf
                oModel677:DeActivate()
                oModel677:Destroy()
                oModel677 := NIL
            End Transaction
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oResponse := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DelItem
    Deleta registro da FLF/FLE
    @author Igor Sousa do Nascimento
    @since 30/08/2017
/*/
//-------------------------------------------------------------------
Static Function DelItem(oResponse,cIDFLF,cTipo,cItem)

    Local aUser     As Array
    Local aKeyFLE   As Array
    Local aDelImg   As Array
    Local lRet      As Logical
    Local oMessages As Object
    Local oModel677 As Object
    Local oModelFLE As Object
    Local oModelFLF As Object

    Default oResponse := JsonObject():New()
    Default cIDFLF    := ""
    Default cTipo     := ""
    Default cItem     := ""

    lRet      := .T.
    oMessages := JsonObject():New()

    If FINXUser(__cUserID,@aUser,.F.)
        // Deleta somente item
        dbSelectArea("FLE")
        FLE->(dbSetOrder(1))

        dbSelectArea("FLF")
        FLF->(dbSetOrder(1))

        If FLF->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1]))
            If FLE->(dbSeek(xFilial("FLF") + cTipo + cIDFLF + aUser[1] + cItem))  
                oModel677:= FWLoadModel("FINA677")
                oModel677:SetOperation(MODEL_OPERATION_UPDATE)  
                F677LoadMod(oModel677,MODEL_OPERATION_UPDATE)
                oModel677:Activate()
                oModelFLE:= oModel677:GetModel("FLEDETAIL")
                oModelFLF:= oModel677:GetModel("FLFMASTER")
                aKeyFLE := { {"FLE_FILIAL",xFilial("FLE")},{"FLE_TIPO",cTipo},{"FLE_PRESTA",cIDFLF},;
                                {"FLE_PARTIC",aUser[1]}, {"FLE_ITEM",cItem} }

                If oModelFLE:SeekLine(aKeyFLE)
                    // Exclui tambem o anexo, caso houver
                    aDelImg := F677ImgApp(5,oModel677)
                    If Len(aDelImg) > 0 .and. aDelImg[1] == "400"
                        lRet := .F.
                    EndIf
                    If lRet .and. oModelFLE:DeleteLine()
                        If !oModel677:VldData()
                            oMessages["code"]   := "400"
                            oMessages["message"]    := "Bad Request"
                            oMessages["detailMessage"] := cValToChar(oModel677:GetErrorMessage()[6])
                            lRet := .F.
                        Else
                            oModel677:CommitData()
                        EndIf
                    Else
                        oMessages["code"]   := "400"
                        oMessages["message"]    := "Bad Request"
                        oMessages["detailMessage"] := STR0004 + If(aDelImg[1] == "400", " " + aDelImg[2], " ") // A opera�?o n?o p�de ser conclu�da." 
                        lRet := .F.
                    EndIf
                EndIf
                oModel677:DeActivate()
                oModel677:Destroy()
                oModel677 := NIL
            EndIf
        EndIf
    Else
        oMessages["code"] 	:= "403"
        oMessages["message"]	:= "Forbidden"
        oMessages["detailMessage"] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oResponse := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadClients
    Carrega os clientes disponiveis no objeto do response
    @author Leonardo Castro
    @since 28/08/2017
    @version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function LoadClients(nPage,nPageSize,cSearchKey,oClients)

    Local cTmp       As Character
    Local cBranchSA1 As Character
    Local cCond      As Character
    Local cSGBD      As Character
    Local lRet       As Logical
    Local nInitPage  As Numeric
    Local nI         As Numeric
    Local oJsonTmp   As Object
    Local oMessages  As Object
    Local aFilter    As Array
    Local lSync      As Logical

    Default oClients := JsonObject():New()
    Default nPage     := 1
    Default nPageSize := 10
    Default cSearchKey := ""

    lSync := SuperGetMV( "MV_MPCSCLI", .F., .T. )

    If !lSync
        oClients["hasNext"] := .F. // Propriedade para controle de paginas
        oClients["clients"] := {}  // Array para composicao dos clientes
        Return .T.    
    EndIf

    aFilter  := {}
    cCond    := ""
    cSGBD    := Alltrim(Upper(TCGetDB()))
    lRet     := .T.
    oMessages:= JsonObject():New()

    nInitPage := (nPage - 1) * nPageSize // Define o comeco da pagina escolhida

    // Alias arquivo temporario
    cTmp := CriaTrab(,.F.)

    // Monta expressoes para query
    cBranchSA1  := xFilial("SA1",cFilAnt)

    If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
        cCond       := " SA1.R_E_C_N_O_ NOT IN ( SELECT TOP " + CValToChar(nInitPage)
        cCond       += " SA1SUB.R_E_C_N_O_ FROM "+ RetSqlName("SA1") +" SA1SUB WHERE SA1SUB.A1_FILIAL = '" + cBranchSA1 + "' AND "
    Else
        cCond       := " SA1.R_E_C_N_O_ NOT IN ( SELECT"
        cCond       += " SA1SUB.R_E_C_N_O_ FROM "+ RetSqlName("SA1") +" SA1SUB WHERE SA1SUB.A1_FILIAL = '" + cBranchSA1 + "' AND "
    EndIf
    //Filtro de busca via searchKey
    If !Empty(cSearchKey)
        cCond   += "(SA1SUB.A1_COD LIKE '%"+cSearchKey+"%' "
        cCond   +=      " OR SA1SUB.A1_NOME LIKE '%"+cSearchKey+"%') AND "
    EndIf

    If cSGBD $ "ORACLE"
        cCond   += " SA1SUB.D_E_L_E_T_ = ' ' AND ROWNUM < " +cValToChar(nInitPage)+ " ) AND "
    ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
        cCond   += " SA1SUB.D_E_L_E_T_ = ' ' "
        cCond   += " LIMIT " +cValToChar(nInitPage)
        cCond   += " ) AND "
    Else
        cCond   += " SA1SUB.D_E_L_E_T_ = ' ' "
        cCond   += " ORDER BY SA1SUB.A1_NOME) AND "
    EndIf

    If !Empty(cSearchKey)
        cCond   += "(SA1.A1_COD LIKE '%"+cSearchKey+"%' "
        cCond   += " OR SA1.A1_CGC LIKE '%"+cSearchKey+"%' "
        cCond   += " OR SA1.A1_NOME LIKE '%"+cSearchKey+"%') AND "
    EndIf

    If SA1->(ColumnPos("A1_MSBLQL")) > 0
        cCond   += "A1_MSBLQL <> '1' AND "
    EndIf

    cCond   += " A1_COD <> '"+Space(TamSx3("A1_COD")[1])+"' AND "


    cCond       := "%" + cCond + "%"

    BEGINSQL ALIAS cTmp
        /*--------------------------------------------------------------------------------------------------
          Query responsavel por trazer todos os Clientes possiveis que podem ser selecionados
        --------------------------------------------------------------------------------------------------*/
        SELECT
            // Client
            SA1.A1_COD, SA1.A1_LOJA, SA1.A1_NOME, SA1.A1_CGC, SA1.A1_PESSOA, SA1.R_E_C_N_O_

        FROM %Table:SA1% SA1

        WHERE
            %exp:cCond%
            SA1.A1_FILIAL = %exp:cBranchSA1% AND 
            SA1.%NotDel%

        ORDER BY
            SA1.A1_NOME
    ENDSQL
    
    dbSelectArea(cTmp)

    oClients["hasNext"] := .F. // Propriedade para controle de paginas
    oClients["clients"] := {}  // Array para composicao dos clientes
        
    If !(cTmp)->(EoF())
        For nI := 1 to nPageSize
            If (cTmp)->(EoF())
                Exit
            Else 
                oJsonTmp := JsonObject():New()
                oJsonTmp["id"]:= (cTmp)->A1_COD  // ID do Cliente
                oJsonTmp["unit"]:= (cTmp)->A1_LOJA  // Loja do cliente
                oJsonTmp["name"]:= EncodeUTF8((cTmp)->A1_NOME)  // Nome do cliente
                if !Empty((cTmp)->A1_CGC)
                    if (cTmp)->A1_PESSOA == 'F'
                        oJsonTmp["cgc"]:= TRANSFORM((cTmp)->A1_CGC, "@R 999.999.999-99")   // CPF
                    else 
                        oJsonTmp["cgc"]:= TRANSFORM((cTmp)->A1_CGC, "@R 99.999.999/9999-99")   // CNPJ
                    endif
                else
                    oJsonTmp["cgc"]:= ''
                endif
                Aadd(oClients["clients"],oJsonTmp)
                (cTmp)->(dbSkip())
            EndIf
        Next nI
        If !(cTmp)->(EoF())
            oClients["hasNext"] := .T.
        Endif
    EndIf

    (cTmp)->(dbCloseArea())

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oClients := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LoadCC
    Carrega os centros de custo disponiveis no objeto do response
    @author Igor Sousa do Nascimento
    @since 10/11/2019
    @version 12.1.25
/*/
//-------------------------------------------------------------------
Static Function LoadCC(nPage,nPageSize,cSearchKey,lDefault,oCosts)

    Local aUser      As Array
    Local cTmp       As Character
    Local cBranchCTT As Character
    Local cBranchRD0 As Character
    Local cCond      As Character
    Local cCondCTT   As Character
    Local cSGBD      As Character
    Local lRet       As Logical
    Local nInitPage  As Numeric
    Local nI         As Numeric
    Local oJsonTmp   As Object
    Local oMessages  As Object
    Local aFilter    As Array

    Default nPage      := 1
    Default nPageSize  := 10
    Default cSearchKey := ""
    Default lDefault   := .T.
    Default oCosts     := JsonObject():New()

    aUser    := {}
    aFilter  := {}
    cCond    := ""
    cCondCTT := ""
    cSGBD    := Upper(TCGetDB())
    lRet     := .T.
    oMessages:= JsonObject():New()

    nInitPage := (nPage - 1) * nPageSize // Define o comeco da pagina escolhida

    cTmp := CriaTrab(,.F.)

    cBranchCTT  := xFilial("CTT")
    cBranchRD0  := xFilial("RD0",cBranchCTT)

    oCosts["hasNext"] := .F. // Propriedade para controle de paginas
    oCosts["items"]   := {} 

    If Empty(oCosts["items"])
        If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
            cCond       := " CTT.R_E_C_N_O_ NOT IN ( SELECT TOP " + CValToChar(nInitPage)
            cCond       += " CTTSUB.R_E_C_N_O_ FROM "+ RetSqlName("CTT") +" CTTSUB WHERE CTTSUB.CTT_FILIAL = '" + cBranchCTT + "' AND "
        Else
            cCond       := " CTT.R_E_C_N_O_ NOT IN ( SELECT"
            cCond       += " CTTSUB.R_E_C_N_O_ FROM "+ RetSqlName("CTT") +" CTTSUB WHERE CTTSUB.CTT_FILIAL = '" + cBranchCTT + "' AND "
        EndIf
        //Filtro de busca via searchKey
        If !Empty(cSearchKey)
            cCond   += "(CTTSUB.CTT_CUSTO LIKE '%"+cSearchKey+"%' "
            cCond   +=      " OR CTTSUB.CTT_DESC01 LIKE '%"+cSearchKey+"%') AND "
        EndIf

        If cSGBD $ "ORACLE"
            cCond   += " CTTSUB.D_E_L_E_T_ = ' ' AND ROWNUM < " +cValToChar(nInitPage)+ " ) AND "
        ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
            cCond   += " CTTSUB.D_E_L_E_T_ = ' ' "
            cCond   += " LIMIT " +cValToChar(nInitPage)
            cCond   += " ) AND "
        Else
            cCond   += " CTTSUB.D_E_L_E_T_ = ' ' "
            cCond   += " ORDER BY CTTSUB.CTT_DESC01) AND "
        EndIf

        If !Empty(cSearchKey)
            cCond   += "(CTT.CTT_CUSTO LIKE '%"+cSearchKey+"%' "
            cCond   +=      " OR CTT.CTT_DESC01 LIKE '%"+cSearchKey+"%') AND "
        EndIf
        
        If CTT->(ColumnPos("CTT_MSBLQL")) > 0
            cCond   += "CTT_MSBLQL <> '1' AND "
        EndIf

        if lDefault
            cCondCTT += "AND RD0.RD0_USER = '"+__cUserID+"' "
            cCond += "RD0.RD0_USER = '"+__cUserID+"' AND "
        endif

        cCondCTT    := "%" + cCondCTT + "%"
        cCond       := "%" + cCond + "%"

        BEGINSQL ALIAS cTmp
            SELECT DISTINCT
                CTT.CTT_CUSTO, CTT.CTT_DESC01, CTT.R_E_C_N_O_, COALESCE(RD0.RD0_USER, '') RD0USER
            FROM %Table:CTT% CTT
                LEFT JOIN %Table:RD0% RD0 ON RD0.RD0_FILIAL = %exp:cBranchRD0% AND CTT.CTT_CUSTO = RD0.RD0_CC %exp:cCondCTT%
            WHERE
                %exp:cCond%
                CTT.CTT_FILIAL = %exp:cBranchCTT% AND 
                CTT.%NotDel%
            ORDER BY
                CTT.CTT_DESC01
        ENDSQL
        
        dbSelectArea(cTmp)
            
        If !(cTmp)->(EoF())
            For nI := 1 to nPageSize
                If (cTmp)->(EoF())
                    Exit
                Else
                    oJsonTmp := JsonObject():New()
                    oJsonTmp["cc_code"]:= (cTmp)->CTT_CUSTO
                    oJsonTmp["cc_description"]:= EncodeUTF8((cTmp)->CTT_DESC01) 

                    If AllTrim(__cUserId) == AllTrim((cTmp)->RD0USER)
                        oJsonTmp["cc_default"] := .T.
                    else
                        oJsonTmp["cc_default"] := .F.       
                    EndIf
                    
                    Aadd(oCosts["items"],oJsonTmp)
                    (cTmp)->(dbSkip())
                EndIf
            Next nI
            If !(cTmp)->(EoF())
                oCosts["hasNext"] := .T.
            Endif
        EndIf

        (cTmp)->(dbCloseArea())
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Currencies
    Carrega as moedas disponiveis no objeto do response
    @author Leonardo Castro
    @since 28/08/2017
    @version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function Currencies(oCurrence,nPage,nPageSize)

    Local aCurrence  As Array
    Local cBranchCTO As Character
    Local nI         As Numeric
    Local oJsonTmp   As Object

    Default nPage     := 1
    Default nPageSize := 10
    Default oCurrence := JsonObject():New()

    aCurrence := { {"1","Real","R$"},;
                   {"2","Dolar","U$"},;
                   {"3","Euro","?"},;
                   {"9","Outras",""} }
    // Monta expressoes para query
    cBranchCTO  := xFilial("CTO",cFilAnt)

    oCurrence["hasNext"] := .F. // Propriedade para controle de paginas
    oCurrence["types"] := {}  // Array para composicao das Moedas
        
    For nI := 1 to Len(aCurrence)
        oJsonTmp := JsonObject():New()
        oJsonTmp["id"]:= aCurrence[nI,1]
        oJsonTmp["description"]:= aCurrence[nI,2]
        oJsonTmp["symbol"]:= aCurrence[nI,3]
        Aadd(oCurrence["types"],oJsonTmp)
    Next nI

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} Locations
    Carrega os clientes disponiveis no objeto do response
    @author Totvs SA
    @since 28/08/2017
    @version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function Locations(nPage,nPageSize,lInter,cSearchKey,oDestinations)
    Local cTmp       As Character
	Local cBranchSX5 As Character
    Local lRet       As Logical
	Local nInitPage  As Numeric
	Local nI         As Numeric
	Local oJsonTmp   As Object
    Local oMessages  As Object
	Local cCond      As Character
	Local cCondPag   As Character
    Local cSGBD      As Character
	
	Default oDestinations := JsonObject():New()
	Default nPage      := 1
	Default nPageSize  := 10
    Default lInter    := .T.
    Default cSearchKey := ""

    lRet      := .T.
    oMessages := JsonObject():New()
    
    cCond     := ""
    cCondPag  := ""
    cSGBD     := Alltrim(Upper(TCGetDB()))
	nInitPage := ( (nPage - 1) * nPageSize ) // Define o comeco da pagina escolhida

	// Alias arquivo temporario
	cTmp := CriaTrab(,.F.)
	
	// Monta expressoes para query
	cBranchSX5  := xFilial("SX5",cFilAnt)

    If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
        cCondPag := " SX5.R_E_C_N_O_ NOT IN (SELECT TOP " + cValToChar(nInitPage) + " R_E_C_N_O_ FROM " + RetSqlName("SX5") + " SX51 "
        cCondPag += "WHERE SX51.X5_FILIAL = '" + xFilial("SX5") + "' AND "
    Else
        cCondPag := " SX5.R_E_C_N_O_ NOT IN (SELECT R_E_C_N_O_ FROM " + RetSqlName("SX5") + " SX51 "
        cCondPag += "WHERE SX51.X5_FILIAL = '" + xFilial("SX5") + "' AND "
    EndIf
    If lInter
        cCond := "SX5.X5_TABELA IN ('BH')  AND "
        cCondPag += "SX51.X5_TABELA    IN('BH') AND "
    Else
        cCond := "SX5.X5_TABELA IN ('12')  AND "
        cCondPag += "SX51.X5_TABELA IN('12') AND "
    EndIf
    If !Empty(cSearchKey)
        cCond += "(SX5.X5_CHAVE LIKE '%"+cSearchKey+"%' "
        cCond += " OR SX5.X5_DESCRI LIKE '%"+cSearchKey+"%') AND "
        cCondPag += "(SX51.X5_CHAVE LIKE '%"+cSearchKey+"%' "
        cCondPag += " OR SX51.X5_DESCRI LIKE '%"+cSearchKey+"%') AND "
    EndIf

    If cSGBD $ "ORACLE"
        cCondPag += "SX51.D_E_L_E_T_= ' ' AND ROWNUM < " +cValToChar(nInitPage)+ " ) AND "
    ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
        cCondPag += "SX51.D_E_L_E_T_= ' ' "
        cCondPag += "LIMIT " +cValToChar(nInitPage)
        cCondPag += " ) AND "
    Else
        cCondPag += "SX51.D_E_L_E_T_= ' ' ) AND "
    EndIf

    cCond := "%" + cCond  + "%"
    cCondPag := "%" + cCondPag + "%"
	
	BEGINSQL ALIAS cTmp
		SELECT
			SX5.X5_TABELA, SX5.X5_CHAVE, SX5.X5_DESCRI
		FROM %Table:SX5% SX5
		WHERE
            %exp:cCond%
            SX5.X5_FILIAL = %exp:cBranchSX5% AND 
            %exp:cCondPag%
			SX5.%NotDel%
		ORDER BY
			SX5.X5_TABELA, SX5.X5_CHAVE, SX5.X5_DESCRI
	ENDSQL
	
	oDestinations["hasNext"] := .F. 	 // Propriedade para controle de paginas
	oDestinations["destinations"] := {}  // Array para composicao dos clientes
	
	dbSelectArea(cTmp)

	If !(cTmp)->(EoF())
		For nI := 1 to nPageSize  
			If (cTmp)->(EoF())
				Exit
			Else
				oJsonTmp := JsonObject():New()
				oJsonTmp["id"]:= (cTmp)->X5_CHAVE  
				oJsonTmp["name"]:= EncodeUTF8(OemtoAnsi(AllTrim((cTmp)->X5_DESCRI)))
				oJsonTmp["international"]:= AllTrim((cTmp)->X5_TABELA) == "BH" // indica se � internacional
				
				Aadd(oDestinations["destinations"],oJsonTmp)
				(cTmp)->(dbSkip())
			EndIf		  
        Next nI
        If !(cTmp)->(EoF())
            oDestinations["hasNext"] := .T.
        Endif
    EndIf

    (cTmp)->(dbCloseArea())

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oDestinations := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} XpenseType
    Carrega os tipos de despesas
    @author Totvs SA
    @since 28/08/2017
    @version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function XpenseType(nPage,nPageSize,cSearchKey,oItemsTypes)
    Local cTmp       As Character
	Local cBranchFLG As Character
	Local cIniTPage  As Character
    Local cFilter    As Character
    Local cSGBD      As Character
    Local lRet       As Logical
	Local nInitPage  As Numeric
	Local nI         As Numeric
	Local oJsonTmp   As Object
    Local oMessages  As Object
	
	Default oItemsTypes := JsonObject():New()
	Default nPage      := 1
	Default nPageSize  := 10
	Default cSearchKey := ""

    lRet      := .T.
    oMessages := JsonObject():New()

	nInitPage := ( (nPage - 1) * nPageSize ) // Define o comeco da pagina escolhida
	aFilter   := {}
	cInitPage := ""
    cSGBD     := Alltrim(Upper(TCGetDB()))
	
	// Alias arquivo temporario
	cTmp := CriaTrab(,.F.)	
	
	// Monta expressoes para query
	cBranchFLG  := xFilial("FLG") 
	
    cFilter := "% FLG.FLG_FILIAL = '" + xFilial("FLG") + "' "
    If !(cSGBD $ "ORACLE|POSTGRES|DB2") .and. cSGBD <> "MYSQL" // SQL e demais bancos
        cIniTPage := "%FLG.R_E_C_N_O_ NOT IN (SELECT TOP " + cValToChar(nInitPage) + " FLG1.R_E_C_N_O_ FROM " + RetSqlName("FLG") + " FLG1 "
        cIniTPage += "WHERE FLG1.FLG_FILIAL = '" + cBranchFLG + "' "
    Else
        cIniTPage := "%FLG.R_E_C_N_O_ NOT IN (SELECT FLG1.R_E_C_N_O_ FROM " + RetSqlName("FLG") + " FLG1 "
        cIniTPage += "WHERE FLG1.FLG_FILIAL = '" + cBranchFLG + "' "
    EndIf
    If !Empty(cSearchKey)
        cFilter   += "AND (FLG.FLG_CODIGO LIKE '%"+cSearchKey+"%' "
        cFilter   +=      " OR FLG.FLG_DESCRI LIKE '%"+cSearchKey+"%') "
        cIniTPage += "AND (FLG1.FLG_CODIGO LIKE '%"+cSearchKey+"%' "
        cIniTPage +=      " OR FLG1.FLG_DESCRI LIKE '%"+cSearchKey+"%') "
    EndIf
    cIniTPage += "AND FLG1.D_E_L_E_T_ = ' ' " 
    If cSGBD $ "ORACLE"
        cIniTPage += "AND ROWNUM < " +cValToChar(nInitPage)+ " ) %"
    ElseIf cSGBD == "POSTGRES" .or. cSGBD == "MYSQL" .or. cSGBD == "DB2"
        cIniTPage += "LIMIT " +cValToChar(nInitPage)+ " ) %"
    Else
        cIniTPage += ") %"
    EndIf

    If FLG->(ColumnPos("FLG_MSBLQL")) > 0
        cFilter   += " AND FLG_MSBLQL <> '1' "
    EndIf

    cFilter   += " AND %"
	
	BEGINSQL ALIAS cTmp
		SELECT
			FLG.FLG_FILIAL, FLG.FLG_CODIGO, FLG.FLG_DESCRI, FLG.FLG_LIMITE, FLG.FLG_GRUPO, FLS_VALUNI, FLS_DTINI, FLS_DTFIM
		FROM %Table:FLG% FLG
        LEFT JOIN %Table:FLS% FLS 
            ON FLS_FILIAL = %xFilial:FLS%
            AND FLS_CODIGO = FLG_CODIGO
            AND FLS_DTINI <= %exp:dDatabase%
            AND (FLS_DTFIM >= %exp:dDatabase% OR FLS_DTFIM = ' ')
            AND FLS.%NotDel%
		WHERE
			%exp:cIniTPage% AND
            %exp:cFilter% 
			FLG.%NotDel% 
		ORDER BY
			FLG.FLG_FILIAL, FLG.FLG_DESCRI
	ENDSQL
	
    oItemsTypes["hasNext"] := .F. 		// Propriedade para controle de paginas
	oItemsTypes["types"] := {}  // Array para composicao dos clientes
	
	dbSelectArea(cTmp)

	If !(cTmp)->(EoF())
		For nI := 1 to nPageSize
			If (cTmp)->(EoF())
				Exit
			Else
                oJsonTmp := JsonObject():New()
                oJsonTmp["id"]:= (cTmp)->FLG_CODIGO
                oJsonTmp["name"]:= EncodeUTF8((cTmp)->FLG_DESCRI)
                oJsonTmp["unit_value"]:= (cTmp)->FLS_VALUNI
                
                Aadd(oItemsTypes["types"], oJsonTmp)
                (cTmp)->(dbSkip())
			EndIf			  
        Next nI
        If !(cTmp)->(EoF())
            oItemsTypes["hasNext"] := .T.
        Endif
    EndIf

    (cTmp)->(dbCloseArea())

    If !lRet
        oMessages["detailMessage"] := EncodeUTF8(oMessages["detailMessage"])
        oItemsTypes := oMessages
        SetRestFault(Val(oMessages["code"]),oMessages["detailMessage"])
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} JSONFromTo
    Carrega objeto com campos solicitados pelo usuario
    @author Igor Sousa do Nascimento
    @since 02/10/2017
    @param aFields = campos do request
    @param aDePara = Array com campos do mobile e do protheus (De/Para)
            aDePara[n][1] = campo ou objeto do request
            aDePara[n][2] = se array 
            aDePara[n][3] = conteudo a ser atribuido
            aDePara[n][4] = se objeto JSON (.T. ou .F.)
            aDePara[n][5] = se propriedade de objeto JSON (.T. ou .F.)
            aDePara[n][6] = propriedades do array (olha para posicao 3)
            obs1: Caso [n][3] for um campo, a tabela deve estar aberta
            obs2: Conteudo fora dessa estrutura devera ser tratado na funcao
/*/
//-------------------------------------------------------------------
Static Function JSONFromTo(aFields,aDePara)
    Local aJsonProp  As Array
    Local cCampoApp  As Character
    Local cJsonProp  As Character
    Local nT         As Numeric
    Local nX         As Numeric
    Local nY         As Numeric
    Local nZ         As Numeric
    Local nTotalAdv1 As Numeric
    Local nTotalAdv2 As Numeric
    Local nTotalAdv3 As Numeric
    Local oJsonObj   As Object

    Default aFields := {}
    Default aDePara := {}

    oJsonObj := JsonObject():New()

    If !Empty(aFields) .and. !Empty(aDePara)
        For nT := 1 to Len(aFields)
            If (nY := AScan(aDePara,{|x| x[1] == aFields[nT] }) ) > 0
                cCampoApp := aDePara[nY][1]
                Do Case
                    Case aDePara[nY][4]   // Se for campo de objeto
                        While aDePara[nY][1] = cCampoApp 
                            If aDePara[nY][3] <> Nil    
                                cJsonProp := '["' + StrTran(aDePara[nY][1],'.','"]["') + '"]'
                                If aDePara[nY][2]   // Trata array
                                    oJsonObj&(cJsonProp) := {}
                                    aAux := &(aDePara[nY][3])
                                    // Variaveis utilizadas no calculo do saldo totalizador
                                    nTotalAdv1 := 0
                                    nTotalAdv2 := 0
                                    nTotalAdv3 := 0
                                    // Monta array de adiantamentos
                                    For nX := 1 to Len(aAux)
                                        oAux := JsonObject():New()  // Objeto auxiliar onde serao criadas as propriedades
                                        For nZ := 1 to Len(aDePara[nY][6])
                                            cArrayProp := '["' + aDePara[nY][6][nZ] + '"]'
                                            oAux&(cArrayProp):= aAux[nX,nZ]
                                            If aAux[nX][5] == '4'
                                                If "current" $ cArrayProp
                                                    nTotalAdv1 += aAux[nX,nZ]
                                                ElseIf "dolar" $ cArrayProp
                                                    nTotalAdv2 += aAux[nX,nZ]
                                                ElseIf "euro" $ cArrayProp
                                                    nTotalAdv3 += aAux[nX,nZ]
                                                EndIf
                                            EndIf
                                        Next nZ
                                        Aadd(oJsonObj&(cJsonProp),oAux)
                                    Next nX
                                Else
                                    oJsonObj&(cJsonProp) := &(aDePara[nY][3])
                                EndIf
                            Else
                                cJsonProp := ""
                                aJsonProp := StrToKarr(aDePara[nY][1],".")
                                For nZ := 1 to Len(aJsonProp)
                                    cJsonProp += '["' + aJsonProp[nZ] + '"]'
                                    If ValType(oJsonObj&(cJsonProp)) == "U"  // Cria objeto dentro do objeto
                                        oJsonObj&(cJsonProp) := JsonObject():New()   // Cria objeto
                                    EndIf
                                Next nZ
                                ASize(aJsonProp,0)
                            EndIf
                            nY++    // Percorre para proxima propriedade
                        EndDo
                    Case aDePara[nY][5]   // Se for propriedade de objeto
                        cJsonProp := ""
                        aJsonProp := StrToKarr(aDePara[nY][1],".")
                        For nZ := 1 to Len(aJsonProp)
                            cJsonProp += '["' + aJsonProp[nZ] + '"]'
                            If nZ < Len(aJsonProp)     // Ultima propriedade nao e objeto
                                If ValType(oJsonObj&(cJsonProp)) == "U"  // Cria objeto dentro do objeto
                                    oJsonObj&(cJsonProp) := JsonObject():New()   // Cria objeto
                                EndIf
                            EndIf
                        Next nZ
                        oJsonObj&(cJsonProp) := &(aDePara[nY][3])   // Grava propriedade
                        cJsonProp := ""
                        ASize(aJsonProp,0)
                    Case aDePara[nY][2]     // Se array
                        cJsonProp := ""
                        cArrayProp:= ""
                        aJsonProp := StrToKarr(aDePara[nY][1],".")
                        For nZ := 1 to Len(aJsonProp)
                            cJsonProp += '["' + aJsonProp[nZ] + '"]'
                            If ValType(oJsonObj&(cJsonProp)) == "U" .and. nZ < Len(aJsonProp)  // Cria objeto dentro do objeto
                                oJsonObj&(cJsonProp) := JsonObject():New()   // Cria objeto
                            EndIf
                        Next nZ
                        oJsonObj&(cJsonProp) := {}
                        aAux := &(aDePara[nY][3])
                        // Variaveis utilizadas no calculo do saldo totalizador
                        nTotalAdv1 := 0
                        nTotalAdv2 := 0
                        nTotalAdv3 := 0
                        // Monta array de adiantamentos
                        For nX := 1 to Len(aAux)
                            oAux := JsonObject():New()  // Objeto auxiliar onde serao criadas as propriedades
                            For nZ := 1 to Len(aDePara[nY][6])
                                cArrayProp := '["' + aDePara[nY][6][nZ] + '"]'
                                oAux&(cArrayProp):= aAux[nX,nZ]
                                If "current" $ cArrayProp
                                    nTotalAdv1 += aAux[nX,nZ]
                                ElseIf "dolar" $ cArrayProp
                                    nTotalAdv2 += aAux[nX,nZ]
                                ElseIf "euro" $ cArrayProp
                                    nTotalAdv3 += aAux[nX,nZ]
                                EndIf
                            Next nZ
                            Aadd(oJsonObj&(cJsonProp),oAux)
                        Next nX
                    Otherwise
                        oJsonObj[cCampoApp] := &(aDePara[nY][3])
                EndCase
            EndIf
        Next nT
    EndIf

Return oJsonObj

//-------------------------------------------------------------------
/*/{Protheus.doc} HasAttch
    Verifica se a despesa posicionada possui anexo
    @author Igor Sousa do Nascimento
    @since 28/03/2019
    @version 12.1.23
/*/
//-------------------------------------------------------------------
Static Function HasAttch()
    Local aArea  As Array
    Local lRet   As Logical
    Local cKey   As Character
    Local cAlias As Character

    cAlias := Alias()

    If !Empty(cAlias)
        cKey := xFilial("FLE") +;
                (cAlias)->FLE_TIPO +;
                (cAlias)->FLE_PRESTA +;
                (cAlias)->FLE_PARTIC +;
                (cAlias)->FLE_ITEM

        aArea := GetArea()

        dbSelectArea("AC9")
        AC9->(dbSetOrder(2))    // AC9_FILIAL+AC9_ENTIDA+AC9_FILENT+AC9_CODENT+AC9_CODOBJ

        If AC9->(dbSeek(xFilial("AC9")+"FLE"+xFilial("FLE")+Alltrim(cKey)))
            dbSelectArea("ACB")
            ACB->(dbSetOrder(1))    // ACB_FILIAL+ACB_CODOBJ
            If ACB->(dbSeek(xFilial("ACB")+AC9->AC9_CODOBJ))
                lRet := .T.
            Else
                lRet := .F.
            EndIf
        Else
            lRet := .F.
        EndIf
        RestArea(aArea)
    Else
        lRet := .F.
    EndIf

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} RetTMobile
    Retorna o total de uma presta��o via query nos itens.
    @author Robson Santos
    @since 27/02/2020
    @version 12.1.27
/*/
//-------------------------------------------------------------------
Static Function RetTMobile()

Local aArea := GetArea()
Local nRet := 0
Local cQry := ""

cQry := " SELECT SUM(FLE_VALREE) VALREE FROM "+RetSqlName("FLE")+ " " 
cQry += " WHERE FLE_PRESTA = '"+AllTrim(FLF_PRESTA)+"' "
cQry += " AND D_E_L_E_T_ <> '*' "
cQry += " AND FLE_FILIAL = '"+XFilial("FLE")+"' "
cQry += " GROUP BY FLE_FILIAL, FLE_TIPO, FLE_PRESTA, FLE_PARTIC "

If Select("TMPXPENSE") > 0
    TMPXPENSE->(dbCloseArea())
EndIf

TCQUERY cQry NEW ALIAS "TMPXPENSE"

TMPXPENSE->(dbGoTop())

nRet := TMPXPENSE->VALREE

TMPXPENSE->(dbCloseArea())

RestArea(aArea)

Return nRet

//-------------------------------------------------------------------
/*/{Protheus.doc} AltStatus
    Altera o status de uma presta��o para aberto. 
    OBS: A presta��o tem que estar previamente posicionada.
    @author Robson Santos
    @since 27/02/2020
    @version 12.1.27
/*/
//-------------------------------------------------------------------
Static Function AltStatus()

If F677EXCAPR(FLF->FLF_TIPO, FLF->FLF_PRESTA, FLF->FLF_PARTIC)

    dbSelectArea("FLF")
    RecLock("FLF", .F.)
    FLF->FLF_STATUS := "1"
    FLF->(MsRUnLock())
    
EndIf

Return


//-------------------------------------------------------------------------------------
/*/{Protheus.doc} LoadAttach
Fun��o auxiliar para separar os registros do endpoint por filial

@param cExpenseID, string, c�digo da presta��o
@param cType, string, tipo da presta��o ( 1 = viagem e 2 = avulsa )
@param cItemID, string, c�digo do item da presta��o a verificar
@param @oAttach, object, objeto com o anexo
@param lChecked, boolean, Se .T. o servi�o est� executando uma aprova��o.

@return boolean, indica se conseguiu carregar o anexo.

@author  Marcia Junko
@since   02/07/2020
/*/
//-------------------------------------------------------------------------------------
Static Function LoadAttach( cExpenseID, cType, cItemID, oAttach, lChecked )
    Local aSvAlias  := GetArea()
    Local aSvFLF    := FLF->( GetArea() )
    Local aSvFLE    := FLE->( GetArea() )
    Local aUser     := {}
    Local aImg      := {}
    Local aSeek     := {}
    Local cUser     := ''
    Local lRet      := .T.
    Local oModel677 := NIL
    Local oModelFLE := NIL
    Local oMessages := NIL

    Default lChecked := .F.

    oMessages    := JsonObject():New()

    If FINXUser( __cUserID, @aUser, .F. )
        dbSelectArea( "FLE" )
        dbSetOrder( 1 )
        dbSelectArea( "FLF" )
        dbSetOrder( 1 )

        cUser := aUser[1]
        If lChecked .And. FLF->( MSSeek( xFilial( "FLF" ) + cType + cExpenseID ) )
            cUser := FLF->FLF_PARTIC
        EndIf

        If FLE->( dbSeek( xFilial( "FLE" ) + cType + cExpenseID + cUser + cItemID ) )
            FLF->( dbSeek( xFilial( "FLF" ) + cType + cExpenseID + cUser ) )
            
            oModel677:= FWLoadModel( "FINA677" )
            oModel677:SetOperation( MODEL_OPERATION_VIEW )
            F677LoadMod( oModel677, MODEL_OPERATION_VIEW )
            oModel677:Activate()
            
            oModelFLE:= oModel677:GetModel( "FLEDETAIL" )
            aSeek := { { "FLE_FILIAL", xFilial( "FLE" ) } ,; 
                       { "FLE_TIPO", cType } ,; 
                       { "FLE_PRESTA", cExpenseID } ,; 
                       { "FLE_PARTIC", cUser } ,;
                       { "FLE_ITEM", cItemID } }
            oModelFLE:SeekLine( aSeek )
            
            aImg := F677ImgApp( 2, oModel677 )
            If Len( aImg ) > 0 .and. ValType( aImg[2] ) == "C"
                oAttach[ "name" ]    := aImg[1]
                oAttach[ "content" ] := aImg[2]
            Else
                oMessages[ "code" ] 	:= "404"
                oMessages[ "message" ]	:= "Not Found"
                oMessages[ "detailMessage" ] := STR0008 //"Nao foi encontrado anexo para essa despesa."   // "N?o foi encontrado anexo para essa despesa."
                lRet := .F.
            EndIf

            oModel677:DeActivate()
            oModel677:Destroy()
            oModel677 := NIL
        EndIf
    Else
        oMessages[ "code" ] 	:= "403"
        oMessages[ "message" ]	:= "Forbidden"
        oMessages[ "detailMessage" ] := STR0003   // "Usu�rio inativo no sistema ou n?o autorizado para esta opera�?o."
        lRet := .F.
    EndIf

    If !lRet
        oAttach := oMessages
        SetRestFault( Val( oMessages[ "code" ] ), oMessages[ "detailMessage" ] )
    EndIf

    RestArea( aSvAlias )
    RestArea( aSvFLF )
    RestArea( aSvFLE )

    FWFreeArray( aSvAlias )
    FWFreeArray( aSvFLF )
    FWFreeArray( aSvFLE )
    FWFreeArray( aUser )
    FWFreeArray( aImg )
    FWFreeArray( aSeek )
    FreeObj( oModel677 )
    FreeObj( oModelFLE )
    FreeObj( oMessages )
Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} isApprover
Verifica se o usu�rio � aprovador ou substituto.

@param @oJSONResp, object, Objecto que armazena a informa��o do usu�rio.

@return boolean, indica se o usu�rio � um aprovador ou substituto
@author Totvs SA
@since 23/07/2020
@version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function isApprover( oJSONResp )
    Local aSvAlias   As Array
    Local cTmp       As Character
    Local cQuery     As Character
    Local cQryRD0    As Character
    
    aSvAlias := GetArea()
    cTmp := GetNextAlias()

    oJSONResp[ "isApprover" ] := .F. 

    cQryRD0 := " SELECT RD0_CODIGO FROM " + RetSqlName("RD0") + " RD0 " + ;
        "WHERE RD0.RD0_FILIAL = '" + xFilial("RD0") + "' AND RD0.RD0_USER = '" + __cUserId + "' AND RD0.D_E_L_E_T_ = ' ' "

    cQuery := "SELECT RD0_CODIGO FROM " + RetSqlName("RD0") + " RD0 " + ;
		"WHERE RD0.RD0_FILIAL = '" + xFilial("RD0") + "' " + ;
            "AND ( RD0_APROPC = ( " + cQryRD0 + " ) OR RD0_APSUBS = ( " + cQryRD0 + " ) ) " + ;
            "AND RD0.D_E_L_E_T_ = ' ' "

    cQuery := ChangeQuery( cQuery )
    MPSysOpenQuery( cQuery, cTmp )

	If ( cTmp )->( !EoF() )
        oJSONResp[ "isApprover" ] := .T.   
    EndIf

    (cTmp)->( DbCloseArea() )

    RestArea( aSvAlias )
    FWFreeArray( aSvAlias )
Return .T.
