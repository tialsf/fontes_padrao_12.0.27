#INCLUDE "FileIO.CH"
#INCLUDE "FINA370.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "DBINFO.CH"
#INCLUDE "FWMVCDEF.CH"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿛osicao do array para controle do processamento MultThread�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
#DEFINE ARQUIVO			1
#DEFINE MARCA			2
#DEFINE QTD_REGISTROS	3
#DEFINE VAR_STATUS		4
#DEFINE NRECNO			1
#DEFINE NMODEL			2
#DEFINE NALIAS			3

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿑lag de processamento escrito no arquivo de controle �
//쿭e threads                                           �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
#DEFINE MSG_OK			"OK"
#DEFINE MSG_ERRO		"ERRO"

// Flags de processamento das vari�veis globais --------------------------------
#DEFINE stThrError		'-1' // STATUS - Excecao: Erro de execu豫o
#DEFINE stThrReady		 '0' // STATUS - Etapa: Variavel pronta para execu豫o
#DEFINE stThrStart		 '1' // STATUS - Etapa: Iniciando execucao da Thread
#DEFINE stThrConnect	 '2' // STATUS - Etapa: Conex�o/Ambiente estabelecidos
#DEFINE stThrTbltmp		 '3' // STATUS - Etapa: Retorno da cria豫o da tabela tempor�ria
#DEFINE stThrFinish		 '4' // STATUS - Etapa: Encerramento da Thread
// -----------------------------------------------------------------------------
#DEFINE MVP03EMISSAO 1
#DEFINE MVP03DATABASE 2
#DEFINE MVP12PERIODO 1
#DEFINE MVP12DOCUMENTO 2
#DEFINE MVP12PROCESSO 3
#DEFINE MVP13TOTBOR 1
#DEFINE MVP13BORBOR 2
// -----------------------------------------------------------------------------

Static __lDefTop	:= IIF( FindFunction("IfDefTopCTB"), IfDefTopCTB(), .F.)
Static __lConoutR	:= FindFunction("CONOUTR")
Static __aFinAlias	:= {}
Static __lSchedule  := FWGetRunSchedule()
Static _oCTBAFIN	:= NIL
Static _cSGBD		:= Alltrim(Upper(TCGetDB()))
Static _MSSQL7		:= _cSGBD $ "MSSQL7"
Static _cOperador	:= If(_MSSQL7,"+","||")
Static _cSpaceMark	:= ''
Static _lMvPar18    := .F.
Static _lCtbIniLan	:= FindFunction("CtbIniLan")
Static _lBlind	    := IsBlind()
Static __oQryTroc	:= NIl

Static l370E5R		:= Existblock("F370E5R" )
Static l370E5P		:= Existblock("F370E5P" )
Static l370E5T		:= Existblock("F370E5T" )
Static l370E1FIL	:= Existblock("F370E1F" )   // Criado Ponto de Entrada
Static l370E1LGC	:= Existblock("F370E1L" )   // Criado Ponto de Entrada
Static l370E2FIL	:= Existblock("F370E2F" )   // Criado Ponto de Entrada
Static l370E5FIL	:= Existblock("F370E5F" )   // Criado Ponto de Entrada
Static l370EFFIL	:= Existblock("F370EFF" )   // Criado Ponto de Entrada
Static l370EFKEY	:= Existblock("F370EFK" )   // Criado Ponto de Entrada
Static l370EUFIL	:= Existblock("F370EUF" )   // Criado Ponto de Entrada
Static lF370NATP	:= Existblock("F370NATP")   // Criado Ponto de Entrada
Static lF370E1WH	:= Existblock("F370E1W" )   // Criado Ponto de Entrada para o While do SE1
Static l370E5KEY	:= Existblock("F370E5K" )   // Criado Ponto de Entrada
Static l370BORD 	:= Existblock("F370BORD")   // Criado Ponto de Entrada
Static l370CTBUSR 	:= Existblock("F370CTBUSR")
Static lCtbPFO7		:= Existblock("CtbPFO7")
Static l370E5CON	:= Existblock("F370E5CT")	 // Ponto de entrada para filtro do SE5 na contabiliza豫o
Static __CtbFPos 	:= Existblock("CtbFPos ")    // Ponto de Entrada, acionado ap? a contabiliza?o da filial

Static cCarteira
Static cCtBaixa
Static lUsaFlag
Static lPCCBaixa
Static lPosE1MsFil
Static lPosE2MsFil
Static lPosE5MsFil
Static lPosEfMsFil
Static lPosEuMsFil
Static lSeqCorr
Static lUsaFilOri

// Init Class Implementation -----------------------------------------------------------------------
CLASS oFINTMPTBL FROM LongNameClass
	DATA aIndexes
	DATA aFields
	DATA cChave
	DATA cClassName
	DATA cRealName
	DATA cAlias
	DATA oFWTMPTBL

	METHOD AddIndex(cOrd,aKey)
	METHOD Create()
	METHOD Delete()
	METHOD GetAlias()
	METHOD GetRealName()
	METHOD New(cTabName,aFields) CONSTRUCTOR
	METHOD SetFields(aFields)
ENDCLASS

METHOD New(cAlias,aFields) CLASS oFINTMPTBL
	::aFields		:= aFields
	::aIndexes		:= {}
	::cAlias		:= cAlias
	::cClassName	:= 'oFINTMPTBL'
	::cRealName		:= ''
RETURN NIL

METHOD AddIndex(cOrd,aKey) CLASS oFINTMPTBL
	AADD(::aIndexes, {cOrd, aKey})
RETURN NIL

METHOD Create() CLASS oFINTMPTBL
	LOCAL nI	as Numeric

	::Delete()	//Deleta a tabela temporaria no banco de dados, caso ja exista

	If !_MSSQL7
		MsCreate(::cAlias,::aFields,'TOPCONN')
		Sleep(1000)
		dbUseArea(.T.,'TOPCONN',::cAlias,::cAlias,.T.,.F.)
		//Cria o Indice
		For nI := 1 To LEN(::aIndexes)
			IndRegua(::cAlias,::cAlias,::aIndexes[nI,2],,)			
		Next nI
	Else
		::oFWTMPTBL := FWTemporaryTable():New(::cAlias,::aFields)
		For nI := 1 To LEN(::aIndexes)
			::oFWTMPTBL:AddIndex(::aIndexes[nI,1], StrToKarr(::aIndexes[nI,2],"+"))
		Next nI
		::oFWTMPTBL:Create()
		::cRealName := ::oFWTMPTBL:GetRealName()
	EndIf

RETURN NIL

METHOD Delete() CLASS oFINTMPTBL
	If !EMPTY(::oFWTMPTBL)
		::oFWTMPTBL:Delete()
		::oFWTMPTBL := NIL
	Else
		MsErase(::cAlias)
	EndIf
RETURN NIL

METHOD GetAlias() CLASS oFINTMPTBL
RETURN ::cAlias

METHOD GetRealName() CLASS oFINTMPTBL
	LOCAL cRet as Character
	cRet := If(!EMPTY(::cRealName),::cRealName,::cAlias)
RETURN cRet

METHOD SetFields(aFields) CLASS oFINTMPTBL
	::aFields := ACLONE(aFields)
RETURN NIL
// End Class Implementation ------------------------------------------------------------------------

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o	 � CTBAFIN	� Autor � Mauricio Pequim Jr	� Data � 06/07/06 낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o � Programa de Lan놹mentos Cont쟟eis Off-Line - TOP			  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿞intaxe	 � CTBFIN() 												  낢�     
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿢so		 � SIGAFIN													  낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function CTBAFIN(lDireto,lAutoMThrd)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define Vari쟶eis 											 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local lPanelFin	  := If (FindFunction("IsPanelFin"),IsPanelFin(),.F.)
Local nOpca		  := 0
Local aSays		  :={}
Local aButtons	  :={}
Local nNumProc	  := GetMv("MV_CFINTHR", .F., 1 )
Local cFilProcIni := cFilAnt
Local cFilProcFim := cFilAnt
Local nX		  := 0
Local lUsaLog	:= SuperGetMv("MV_FINLOG",.T.,.F.)
Local cPathLog := GetMv("MV_DIRDOC")
Local cLogArq 	:= "Fina370Log.TXT"
Local cCaminho := cPathLog + cLogArq
Local lProcCtb := SuperGetMv( "MV_CTBUPRC" , .T. , .F. ) .and. (_cSGBD $ "MSSQL7|ORACLE" ) 
Local cDescription	:= STR0010 + " " + STR0011 
Local bProcess		:= {|oSelf|nOpca:=1} //{|oSelf|F370Process(oSelf)}
Local aKeyProc		:= NIL

Private cCadastro := STR0009  //"Contabiliza뇙o Off Line"
Private LanceiCtb := .F.

SetVarNameLen(50)  //determina tamanho do nome das variaveis globais para status do progresso das threads

nNumProc := If((nNumProc > 30),30,nNumProc)

Default lDireto	  := .F.
DEFAULT lAutoMThrd := .F.

_cSpaceMark	:= "'"+SPACE(LEN(GetMark()))+"'"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Variaveis utilizadas para parametros                        �
//� mv_par01 - Mostra Lan놹mentos Contabeis ?  1- Sim, 2- N�o   �
//� mv_par02 - Aglutina Lan놹mentos Contabeis? 1- Sim, 2- N�o   �
//� mv_par03 - Contabiliza Emissoes? 1-Emissao / 2-Data Base    �
//� mv_par04 - Data Inicio                                      �
//� mv_par05 - Data Fim										        �
//� mv_par06 - Carteira ? 1-Receber/2-Pagar/3-Cheques/4-Todas   �
//� mv_par07 - Contab. Baixas ? 1-Dt Baixa/2-Dt Digit/3-DtDispo �
//� mv_par08 - Considera filiais abaixo? 1- Sim / 2 - Nao       �
//� mv_par09 - Da Filial                                        �
//� mv_par10 - Ate a Filial                                     �
//� mv_par11 - Atualiza Sinteticas                              �
//� mv_par12 - Separa por ? (Periodo,Documento,Processo)        �
//� mv_par13 - Ctb Bordero - Total/Por Bordero                  �
//� mv_par14 - Considera Filial Original?  1- Sim, 2 - N�o      �
//� mv_par15 - Filial de                                        �
//� mv_par16 - Filial At�                                       �
//� mv_par17 - Contab. Tit. Provisorio? 1-Sim/2-Nao             �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

// Obs: este array aRotina foi inserido apenas para permitir o
// funcionamento das rotinas internas do advanced.
Private aRotina:={ 	{ STR0012,"AxPesqui" , 0 , 1},;  //"Localizar"
					{ STR0013,"fA100Pag" , 0 , 3},;  //"Pagar"
					{ STR0014,"fA100Rec" , 0 , 3},;  //"Receber"
					{ STR0015,"fA100Can" , 0 , 5},;  //"Excluir"
					{ STR0016,"fA100Tran", 0 , 3},;  //"Transferir"
					{ STR0017,"fA100Clas", 0 , 5} }  //"Classificar"

Private lCabecalho
Private VALOR     	:= 0
Private VALOR6		:= 0
Private VALOR7		:= 0
Private nTpLog := SuperGetMv("MV_TIPOLOG",.T.,1)
Private FO1VADI := 0 

fErase(cCaminho)
/*   Se o parametro MV_CTBUPRC e for .T. N홒  executar com multi threads */
nNumProc := If( lProcCtb, 1, nNumProc)
// Inicializa as variaveis staticas da contabiliza豫o
If FindFunction("CLEARCX105")
	ClearCx105()
Endif

// Inicia as variaveis staticas do fonte.
FinIniVar()

// Variaveis utilizadas na contabilizacao do modulo SigaFin
// declarada neste ponto, caso o acesso seja feito via SigaAdv

Debito  	:= ""
Credito 	:= ""
CustoD		:= ""
CustoC		:= ""
ItemD 		:= ""
ItemC 		:= ""
CLVLD		:= ""
CLVLC		:= ""

Conta		:= ""
Custo 		:= ""
Historico 	:= ""
ITEM		:= ""
CLVL		:= ""

Abatimento  := 0
REGVALOR    := 0
STRLCTPAD 	:= ""		//para contabilizar o historico do cheque
NUMCHEQUE 	:= ""		//para contabilizar o numero do cheque
ORIGCHEQ  	:= ""		//para contabilizar o Origem do cheque
cHist190La 	:= ""
Variacao	:= 0
dDataUser	:= MsDate()
CODFORCP  	:= ""	//para contabilizar o Codigo do Fornecedor da Compensacao
LOJFORCP 	:= ""	//para contabilizar a Loja do Fornecedor da Compensacao

If (__lSchedule .or. lDireto) .AND. !lAutoMThrd
	BatchProcess(cCadastro, 	STR0010 + Chr(13) + Chr(10) +;
	STR0011, "FIN370",;
	{ || CTBFINProc(.T.) }, { || .F. })
	Return .T.
Endif

If !__lSchedule
	Pergunte("FIN370",.F.)
EndIf

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Inicializa o log de processamento                            �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
ProcLogIni( aButtons )

AADD(aSays,STR0010) //"  Este programa tem como objetivo gerar Lan놹mentos Cont쟟eis Off para t죜ulos"
AADD(aSays,STR0011) //"emitidos e/ou baixas efetuadas."
If lPanelFin  //Chamado pelo Painel Financeiro			
	aButtonTxt := {}			
	If Len(aButtons) > 0
		AADD(aButtonTxt,{STR0036,STR0036,aButtons[1][3]}) // Visualizar			
	Endif
	AADD(aButtonTxt,{STR0037,STR0037, {||Pergunte("FIN370",.T. )}}) // Parametros						
	FaMyFormBatch(aSays,aButtonTxt,{||nOpca :=1},{||nOpca:=0})	
ElseIf !lAutoMThrd           			
	tNewProcess():New( "CTBAFIN", cCadastro, bProcess, cDescription, "FIN370" )
Endif

//旼컴컴컴컴컴컴컴컴컴컴컴컴켁
//쿣alida豫o para multthread.�
//읕컴컴컴컴컴컴컴컴컴컴컴컴켁
nNumProc := IIF(__lSchedule .OR. nNumProc < 1 , 1, nNumProc)

If nNumProc > 1 .And. (nOpcA == 1 .OR. lAutoMThrd)
	If CtbValMult(.T.,MV_PAR02 == 1,MV_PAR01 == 1,MV_PAR12 )
		nOpcA := 1
	Else
		nOpcA := 0
	EndIf
EndIf

If nOpcA == 1

	If !CtbValiDt(,dDataBASE,,,,{"FIN001","FIN002"},)
		Return
	EndIf
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Atualiza o log de processamento   �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	ProcLogAtu("INICIO")
	
	lMulti := nNumProc > 1 .And. CtbValMult(.F.,MV_PAR02 == 1,MV_PAR01 == 1,MV_PAR12 )
	
	
	If mv_par08 == 1      //Considera filiais abaixo? 1- Sim / 2 - Nao 
		cFilProcIni	:= mv_par09
		cFilProcFim := mv_par10		
		
		If mv_par14 == 1  //Considera Filial Original?  1- Sim, 2 - N�o
			cFilProcIni	:= mv_par15
			cFilProcFim := mv_par16
		EndIf

		If lAutoMThrd .or. A370CanProc(mv_par06, mv_par04, mv_par05,cFilProcIni,cFilProcFim)
			aKeyProc := {{'CCART',STR(mv_par06,1)},{'DTDE',DTOS(mv_par04)},{'DTATE',DTOS(mv_par05)},{'FILDE',cFilProcIni},{'FILATE',cFilProcFim}}
			If lMulti
				MsgRun( STR0038,, {|| CTBMTFIN(mv_par06,nNumProc) } ) //"Contabilizando..."
			Else
				If _lCtbIniLan
					CtbIniLan()
				EndIf
				Processa({|lEnd| CTBFINProc()})  // Chamada da funcao de Contabilizacao Off-Line
				If FindFunction("CtbFinLan")
					CtbFinLan()
				EndIf
			EndIf
		EndIf
	Else
		If lAutoMThrd .or. A370CanProc(mv_par06, mv_par04, mv_par05)
			aKeyProc := {{'CCART',STR(mv_par06,1)},{'DTDE',DTOS(mv_par04)},{'DTATE',DTOS(mv_par05)}}
			If lMulti
				MsgRun( STR0038,, {|| CTBMTFIN(mv_par06,nNumProc) } ) //"Contabilizando..."
			Else
				If _lCtbIniLan
					CtbInilan()
				EndIf
				Processa({|lEnd| CTBFINProc()})  // Chamada da funcao de Contabilizacao Off-Line
				If FindFunction("CtbFinLan")
					CtbFinLan()
				EndIf
			EndIf
		EndIf
	EndIf
	
	//Libera o Processamento e envia mensagem no server (tempo)
	If !lAutoMThrd .and. !EMPTY(aKeyProc)
		A370FreeProc(aKeyProc)
	EndIf

	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Atualiza o log de processamento   �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	ProcLogAtu("FIM")

Endif

// Verifico se os alias foram encerrados, se n�o for�o o
For nX := 1 TO Len( __aFinAlias )
	If !Empty(__aFinAlias[nX]) .And. Select(__aFinAlias[nX]) > 0
		DbSelectArea(__aFinAlias[nX])	
		DbCloseArea()
		MsErase(__aFinAlias[nX])
	Endif
Next

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿚 codigo abaixo eh utilizado nesse ponto para garantir que tanto o alias�
//쿿uanto o browse serao recriados sem problemas na utilizacao do painel   �
//|financeiro quando a rotina nao eh chamada de forma semi-automatica pois | 
//|esse tratamento eh realizado na rotina T											|
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If lPanelFin //Chamado pelo Painel Financeiro			
	dbSelectArea(FinWindow:cAliasFile)			
	ReCreateBrow(FinWindow:cAliasFile,FinWindow)      	
Endif
	
If lUsaLog
	aIncons := FA370TXT(cCaminho)
	If Len(aIncons) > 0
		If nTpLog == 1
			Aviso(STR0031,STR0052+cCaminho+"'.",{'OK'},2) //"Foram gravados registros de inconsist�ncias na tabela SE5 nesta contabiliza豫o. Favor verificar os registros no arquivo 'Fina370Log.TXT', existente na pasta '"
		ElseIf nTpLog == 2	
			FA370Rel(aIncons) //Relat�rio de inconsist�ncias.
		EndIf
	EndIF
EndIf

//--------------------------------------------
// Ponto de Entrada ao final do processamento
//   para processos complementares do usuario
//--------------------------------------------
If l370CTBUSR
	Execblock("F370CTBUSR",.F.,.F.)
EndIf		

// Inicializa as variaveis staticas da contabiliza豫o
If FindFunction("CLEARCX105")
	ClearCx105()
Endif

Return
	
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o	 쿎TBFINProc� Autor � Wagner Xavier 	    � Data � 24/08/94 낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o � Programa de Lan놹mentos Cont쟟eis Off-Line				  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿞intaxe	 � FINA370()												  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿢so		 � SIGAFIN													  낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function CTBFINProc(lBat,lLerSE1,cAliasSE1,lLerSE2,cAliasSE2,lLerSEF,cAliasSEF,lLerSE5,cAliasSE5,lMultiThr,lLerSEU,cAliasSEU,lTrocoMT)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Define Vari쟶eis 											 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
Local lPadrao
Local nTotal		:= 0
Local nHdlPrv		:= 0
Local cArquivo		:= ""
Local cPadrao
Local nValLiq		:= 0
Local nDescont		:= 0
Local nJuros		:= 0
Local nMulta		:= 0
Local nCorrec		:= 0
Local nVl
Local nDc
Local nJr
Local nMt
Local lTitulo		:= .F.
Local dDataAnt		:= dDataIni := dDataBase
Local nPeriodo		:= 0
Local nLaco   		:= 0
Local cChave		:= ""
Local nRegSE5		:= 0
Local nRegOrigSE5	:= 0
Local lX			:= .f.
Local lAdiant 		:= .F.
Local nProxReg 		:= 0
Local lEstorno 		:= .F.
Local lEstRaNcc 	:= .F.
Local lEstPaNdf 	:= .F.
Local lEstCart2 	:= .F.
Local nValorTotal 	:= 0
Local cCondWhile	:= " "
Local nRegAnt
Local bCampo
Local nOrderSEU
Local cChaveSev
Local cChaveSeZ
Local nRecSe1
Local nRecSe2
Local cNumBor		:= ""
Local cProxBor		:= ""
Local cLstBor		:= ""
Local nBordero		:= 0
Local nTotBord		:= 0
Local nBordDc		:= 0
Local nBordJr  		:= 0
Local nBordMt		:= 0
Local nBordCm  		:= 0
Local nTotDoc		:= 0
Local nTotProc 		:= 0
Local lPadraoCC
Local lSkipLct 		:= .F.
Local cSitOri  		:= " "
Local cSitCob 		:= " "
Local cSeqSE5 		:= ""
Local lPadraoCCE
Local cPadraoCC
Local lMultNat 		:= .F.
Local nRecSev
Local nRecSez
Local aFlagCTB 		:= {}
Local nPis			:= 0
Local nCofins  		:= 0
Local nCsll	   		:= 0
Local nVretPis 		:= 0
Local nVretCof 		:= 0
Local nVretCsl 		:= 0
Local aRecsSE5 		:= {}
Local nX 			:= 0
Local nI			:= 0
Local nTamDoc		:= TamSX3("E5_PREFIXO")[1]+TamSX3("E5_NUMERO")[1]+TamSX3("E5_PARCELA")[1]+TamSX3("E5_TIPO")[1]
Local nTamBor		:= TamSX3("EA_NUMBOR")[1]
Local lMulNatSE2	:= .F.
Local cProxChq		:= ""
Local cChequeAtual	:= ""
Local aStru			:= SE5->(DbStruct())
Local cSepProv		:= If("|"$MVPROVIS,"|",",") 
Local lEstCompens	:= .F.
Local cSELSerie		:= ""
Local cSELRecibo	:= ""
Local lFindITF		:= FindFunction("FinProcITF")
Local lQrySE1		:= .F.
Local aRecsSEI	:= {}
Local nJ := 0
Local dDataSEF	:= dDataBase
Local cMVSLDBXCR 	:= SuperGetMv("MV_SLDBXCR",.F.,.F.)	//Indica como sera controlado o saldo da baixa do contas a receber, somente quando o cheque for compensado, ou no momento da baixa (B,C)
Local lCtMovPa		:= SuperGetMv("MV_CTMOVPA",.T.,"1") == "2" // Indica se a Contabiliza�?o Offline do LP513 ocorrer� pelo T죜ulo(SE2) ou Mov.Bancario(SE5) do Pagamento Antecipado. 1="SE2" / 2="SE5"
Local nRecMovPa		:= 0
Local lPAMov		:= .F.
Local lCtCheqLib    := SuperGetMv("MV_CTCHQBX",.T.,"1") == "2"
Local cAliasTroc	:= ""
Local lTroTemSE5	:= .F.

//Variaveis para grava豫o do c�digo de correlativo
Local aDiario		:= {}

Local nz			:= 0
Local lArqSEF		:= .F.
Local aLstSEF		:= {}  
Local cEF_T01		:= ""
Local bCond			:= Nil

// Quando processamento por MultiThread a fun豫o CTBMTFIN() � que controla as filiais e os registros
// Portanto, aqui assumimos o ambiente estabelecido pela Thread em execu豫o
Local aSM0			:= IF(lMultiThr,{nil},AdmAbreSM0())	

Local nContFil		:= 0
Local __cFilAnt		:= cFilAnt
Local nPosReg		:= 0
Local dDataCtb		:= Nil // variavel de controle da contabiliza豫o.
Local aAreaATU		:= {}
Local aRecsTRF		:= {}
Local aFils			:= {}
Local lPass			:= .F.
Local lAGP			:= .F.
Local lMvAGP		:= SuperGetMv("MV_CTBAGP",.F.,.F.)
Local lF370ChkAgp	:= FindFunction("F370ChkAgp")
Local cNatTroc		:= Alltrim(IIf( cPaisLoc <> "BRA", SuperGetMV("MV_NATTROC"), GetNewPar("MV_NATTROC",'"TROCO"') ))
LOCAL lGroupDoc		:= SUPERGETMV("MV_GPDOCTB",.F.,.F.)
Local lQuebraDoc	:= .T.
Local lBxCnab 		:= SuperGetMv("MV_BXCNAB",.T.,"N") == "S"
Local lAvancaReg	:= .T.
Local aCT5			:= {}
Local cUniao		:= GetMv( "MV_UNIAO" )
Local cMunic		:= GetMv( "MV_MUNIC" )
Local cOrdLCTB		:= Alltrim(GETMV("MV_ORDLCTB"))
//1 - Contabiliza por titulo ; 2 - contabiliza por venda (somente quando possuir integra豫o)
Local nCtbVenda		:= SuperGetMV("MV_CTBINTE",,1)
Local lRACont		:= .F.
Local lRAMov		:= .F.
Local aAreaAux		:= NIL
Local cLA			:= ""
Local cChE5Comp		:= ""

Local aAreaSE2		:= NIL
Local nVlDecresc	:= 0
Local nVlAcresc		:= 0

// Variaveis para contabiliza豫o dos juros informados na liquida豫o (FO1 e FO2)
Local cNumLiq       := ""
Local nRecFO2       := 0
Local aDadosFO1     := {}
Local lHasFilOrig	:= .F.
Local cMvSimb1		:= SuperGetMV("MV_SIMB1",,"R$")						// Simbolo da moeda
Local cTipoMov		:= ''
Local lLoop			:= .F.
Local cRecPag		:= ""
Local nSkip			:= 0
													
Local lExistFWI     := FwAliasInDic("FWI", .F.)

Private cCampo
Private Inclui		:= .T.
Private cLote		:= Space(4)
Private nSaveSx8Len := GetSx8Len()
Private cSeqCv4		:= ""

// Registro de LOG de inconsist�ncias
PRIVATE nTpLog		:= IF(lMultiThr,1,SuperGetMv("MV_TIPOLOG",.T.,1))
PRIVATE cPathLog	:= GetMv("MV_DIRDOC")
PRIVATE aIncons		:= {}

DEFAULT lBat    	:= .F.
DEFAULT lLerSE5 	:= .T.
DEFAULT lLerSE1 	:= .T.
DEFAULT lLerSE2 	:= .T.
DEFAULT lLerSEF 	:= .T.
DEFAULT lLerSEU  	:= .T.
DEFAULT cAliasSE5	:= "SE5"
DEFAULT cAliasSE2 	:= "SE2"
DEFAULT cAliasSEF	:= "SEF"
DEFAULT cAliasSE1	:= "SE1"
DEFAULT cAliasSEU	:= "TRBSEU"
DEFAULT lMultiThr	:= .F.
DEFAULT lTrocoMT	:= .F.

IF SUBSTR(cPathLog,LEN(cPathLog),1) <> '\'
	cPathLog := cPathLog + '\'
	PUTMV('MV_DIRDOC',cPathLog)
ENDIF

// Processa SEF se existir ao menos um LP configurado.
If lLerSEF
젨젨lLerSEF�:=쟗ldVerPad({'559','566','567','590'})�
EndIf

// Inicia as variaveis staticas do fonte.
FinIniVar()

If !__lSchedule
	Pergunte("FIN370",.F.)
EndIf

cSeqCv4 := GetSx8Num("CV4", "CV4_SEQUEN")

IF Len( aSM0 ) <= 0
	Help(" ",1,"NOFILIAL")
	Return .F.
Endif

//USO APENAS PARA TOP
If __lDefTop
	If TcSrvType() == "AS/400"
		HELP(" ",1,"USEFINA370")			
		Return .F.
	Endif	
Else
	HELP(" ",1,"USEFINA370")			
	Return .F.
Endif

cFilDe  := cFilAnt
cFilAte := cFilAnt

If mv_par08 == 1
	cFilDe := mv_par09
	cFilAte:= mv_par10
Endif

If FindFunction("F370Fil")
	aFils := F370Fil(mv_par09,mv_par10) // Retorna filiais que o usuario tem acesso
Endif

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Variaveis utilizadas para parametros						 �
//� mv_par01 // Mostra Lan놹mentos Cont쟟eis 					 �
//� mv_par02 // Aglutina Lan놹mentos Cont쟟eis					 �
//� mv_par03 // Emissao / Data Base 							 �
//� mv_par04 // Data Inicio										 �
//� mv_par05 // Data Fim										 �
//� mv_par06 // Carteira : Receber / Pagar /Cheque / Ambas 		 �
//� mv_par07 // Baixas por Data de Emiss꼘 ou Digita뇙o			 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

For nContFil := 1 to Len(aSM0)
	
	// ---------------------------------------------------------------------------------------------------------------------------
	// Quando processamento por MultiThread a fun豫o CTBMTFIN() � que controla as filiais e os registros.
	// Portanto, aqui assumimos o ambiente estabelecido pela Thread em execu豫o
	// ---------------------------------------------------------------------------------------------------------------------------
	IF !lMultiThr
		If aSM0[nContFil][SM0_CODFIL] < cFilDe .Or. aSM0[nContFil][SM0_CODFIL] > cFilAte .Or. aSM0[nContFil][SM0_GRPEMP] != cEmpAnt  
			Loop
		EndIf
		If MV_PAR14 == 1
			If aSM0[nContFil][SM0_CODFIL] < MV_PAR15 .Or. aSM0[nContFil][SM0_CODFIL] > MV_PAR16
				Loop
			EndIf
		Endif
		cFilAnt := aSM0[nContFil][SM0_CODFIL]
	ENDIF
	// ---------------------------------------------------------------------------------------------------------------------------
	
	If !Empty(aFils) 
	    If Ascan(aFils,{|x| Alltrim(x) == Alltrim(cFilAnt)}) == 0 
	    	Loop
	    Endif
    Endif

	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Chama a SumAbatRec para abrir alias auxiliar __SE1 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	If Select("__SE1") == 0
		SumAbatRec("","","",1,"")
	Endif	
	
	If lLerSe5
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Verifica qual a data a ser utilizada para baixa				 �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		cCampo := IIF(mv_par07 == 1,"E5_DATA",Iif(mv_par07 == 2,"E5_DTDIGIT","E5_DTDISPO"))
		aRecsSE5 := {}
		lHasFilOrig := .F.

		//Query para contabiliza豫o do troco pela FK5 - Se for multithread, a princ�pio s� chama na primeira thread - Rever quando o fonte estiver olhando apenas para as FKs
		If (mv_par06 == 2 .or. mv_par06 == 4) .And. (!lMultiThr .Or. lTrocoMT)
			cAliasTroc := QueryTroco( cFilAnt, mv_par04, mv_par05 )
		EndIf
		
		If lMultiThr
			(cAliasSE5)->(dbGoTop())
		Else
			DbSelectArea("SE5")
			aStru  := SE5->(DbStruct())
			cQuery := ""
			aEval(DbStruct(),{|e| cQuery += ","+AllTrim(e[1])})

			// Obtem os registros a serem processados
			cQuery := "SELECT " + SubStr(cQuery,2)
			cQuery += "     , SE5.R_E_C_N_O_ SE5RECNO "
			cQuery += "  FROM " + RetSqlName("SE5") + " SE5 "

			IF ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))
				If lPosE5MsFil .AND. !lUsaFilOri
					cQuery += "WHERE E5_MSFIL = '"  + cFilAnt + "' AND "
				Else
					cQuery += "WHERE E5_FILORIG = '"  + cFilAnt + "' AND "
					lHasFilOrig := .T.
				Endif
			Else
				cQuery += "WHERE E5_FILIAL = '" + xFilial("SE5") + "' AND "
			EndIf
			
			If mv_par07 == 1			//DATA
				cQuery += "E5_DATA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				cQuery += "E5_TIPODOC IN ('PA','RA','BA','VL','V2','AP','EP','PE','RF','IF','CP','TL','ES','TR','DB','OD','LJ','E2','TE','  ','IT')  AND "
				If mv_par12 <> MVP12DOCUMENTO .And. cOrdLCTB == "E"
					cChave := "E5_FILIAL+Dtos(E5_DATA)+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ+R_E_C_N_O_"
				Else
					cChave := "E5_FILIAL+Dtos(E5_DATA)+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
				EndIf
			ElseIf mv_par07 == 2		//DATA DE DIGITACAO
				cQuery += "((E5_DTDIGIT    between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				cQuery += "E5_TIPODOC IN ('PA','RA','BA','VL','V2','AP','EP','PE','RF','IF','CP','TL','ES','DB','OD','LJ','E2','  ','IT')) OR
				cQuery += "(E5_DATA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				cQuery += "E5_TIPODOC IN ('TR','TE')))  AND "
				If mv_par12 <> MVP12DOCUMENTO .And. cOrdLCTB == "E"
					cChave := "E5_FILIAL+Dtos(E5_DTDIGIT )+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ+R_E_C_N_O_"				
				Else
					cChave := "E5_FILIAL+Dtos(E5_DTDIGIT )+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
				EndIf
			Else 							//DATA DE DISPONIBILIDADE
				cQuery += "((E5_DTDISPO    between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				cQuery += "E5_TIPODOC IN ('PA','RA','BA','VL','V2','AP','EP','PE','RF','IF','CP','TL','ES','DB','OD','LJ','E2','  ')) OR
				cQuery += "(E5_DATA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				cQuery += " (E5_TIPODOC IN ('TR','TE') OR (E5_TIPODOC = ' ' AND E5_TIPO = ' ')))) AND "
				If mv_par12 <> MVP12DOCUMENTO .And. cOrdLCTB == "E"
					cChave := "E5_FILIAL+Dtos(E5_DTDISPO)+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ+R_E_C_N_O_"				
				Else
					cChave := "E5_FILIAL+Dtos(E5_DTDISPO)+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
				Endif
			Endif

			If lHasFilOrig .AND. !EMPTY(cChave)
				cChave := STRTRAN(cChave,'E5_FILIAL','E5_FILORIG')
			EndIf
			
			cQuery += "E5_SITUACA <> 'C' AND "
			cQuery += "(E5_LA <> 'S ' OR ((E5_ORDREC || E5_SERREC) <> '' AND E5_RECPAG = 'R' AND E5_TIPODOC = 'BA')) AND E5_MOTBX NOT IN ('DSD') AND "  // Filtra registros de Recebimentos Diversos p/Contabilizacao
			cQuery += "D_E_L_E_T_ = ' ' "
			
			// Ponto de Entrada para filtrar registros do SE5.
			If l370E5FIL
				cQuery := Execblock("F370E5F",.F.,.F.,cQuery)
			EndIf
			
			// Ponto de Entrada para alterar ordenacao do SE5
			If l370E5KEY
				cChave := Execblock("F370E5K",.F.,.F.,cChave)
			EndIf
			
			// seta a ordem de acordo com a opcao do usuario
			If MV_PAR12 == MVP12DOCUMENTO
				cQuery += " ORDER BY " + SqlOrder( StrTran( cChave , "E5_DOCUMEN+" , "E5_DOCUMEN+R_E_C_N_O_+" ) )
			Else
				cQuery += " ORDER BY " + SqlOrder( cChave )
			EndIf

			If __lConoutR
				ConoutR( cQuery )
			Endif
			
			cQuery := ChangeQuery(cQuery)

			cAliasSE5 := FINNextAlias()
			
			If Select(cAliasSE5) > 0
				DbSelectArea( cAliasSE5 )
				DbCloseArea()
			Endif

			dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSE5 , .F., .T.)

			For nI := 1 TO LEN(aStru)
				If aStru[nI][2] != "C"
					TCSetField(cAliasSE5, aStru[nI][1], aStru[nI][2], aStru[nI][3], aStru[nI][4])
				EndIf
			Next
			DbGoTop()
		EndIf
	Endif
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Contabiliza pelo E2_EMIS1 - CONTAS PAGAR   			�
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	If (mv_par06 = 2 .Or. mv_par06 = 4) .and. lLerSe2

		If lMultiThr
			(cAliasSE2)->(dbGoTop())
		Else
			dbSelectArea("SE2")
			cChave := "E2_FILIAL+DTOS(E2_EMIS1)+E2_NUMBOR+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA"

			aStru := SE2->(DbStruct())
			cQuery := ""
			aEval(DbStruct(),{|e| cQuery += ","+AllTrim(e[1])})

			// Obtem os registros a serem processados 
			cQuery := "SELECT " + SubStr(cQuery,2)
			cQuery += "     , SE2.R_E_C_N_O_ SE2RECNO "
			cQuery += "  FROM " + RetSqlName("SE2") + " SE2 "

			IF ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))							
				If lPosE2MsFil .AND. !lUsaFilOri
					cQuery += "WHERE E2_MSFIL = '"  + cFilAnt + "' AND "
				Else
					cQuery += "WHERE E2_FILORIG = '"  + cFilAnt + "' AND "
				Endif				
			Else
				cQuery += "WHERE E2_FILIAL = '" + xFilial("SE2") + "' AND "
			EndIf

			cQuery += "E2_EMIS1 BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
			
			// Soh adiciona filtro na query se nao contabiliza titulos provisorios
			If mv_par17 <> 1
				cQuery += "E2_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " AND "
			EndIf
			
			cQuery += "E2_STATUS <> 'D' AND "	// T�tulo base de desdobramento - Contabilizar pelas parcelas
			cQuery += "E2_LA <> 'S' AND E2_ORIGEM <> 'FINA677' AND "
			cQuery += "D_E_L_E_T_ = ' ' "
			
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			//� Ponto de Entrada para filtrar registros do SE2. �
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			If l370E2FIL
				cQuery := Execblock("F370E2F",.F.,.F.,cQuery)
			EndIf
			
			// seta a ordem de acordo com a opcao do usuario
			cQuery += " ORDER BY " + SqlOrder(cChave)

			cQuery := ChangeQuery(cQuery)

			If __lConoutR
				ConoutR( cQuery )
			Endif

			cAliasSE2 := FINNextAlias()
			
			If Select(cAliasSE2) > 0
				DbSelectArea( cAliasSE2 )
				DbCloseArea()
			Endif

			dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSE2, .F., .T.)
			For nI := 1 TO LEN(aStru)
				If aStru[nI][2] != "C"
					TCSetField(cAliasSE2, aStru[nI][1], aStru[nI][2], aStru[nI][3], aStru[nI][4])
				EndIf
			Next
			DbGoTop()
		EndIf
	Endif
	
	//SEF
	If (mv_par06 = 3 .Or. mv_par06 = 4 ) .and. lLerSef
		aStru := SEF->(DbStruct())
		cEF_T01 := aStru[SEF->(FieldPos("EF_DATA")),2]
		
		If lMultiThr
			// A Thread j� enviou a tabela filtrada.
			// Apenas posicionar no inicio.
			(cAliasSEF)->(dbGoTop())
		Else
			dbSelectArea("SEF")
			cChave := "EF_FILIAL+EF_BANCO+EF_AGENCIA+DTOS(EF_DATA)"
			aStru := SEF->(DbStruct())
			
			cQuery := ""
			aEval(DbStruct(),{|e| cQuery += ","+AllTrim(e[1])})
			// Obtem os registros a serem processados
			cQuery := "SELECT " +SubStr(cQuery,2)
			cQuery += "     , SEF.R_E_C_N_O_ SEFRECNO "
			cQuery += "  FROM " + RetSqlName("SEF")+" SEF "
			
			IF ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))
				If lPosEFMsFil .AND. !lUsaFilOri
				cQuery += "WHERE EF_MSFIL = '" + cFilAnt + "' AND "
				Else
					cQuery += "WHERE EF_FILORIG = '"  + cFilAnt + "' AND "
				Endif						
			Else
				cQuery += "WHERE EF_FILIAL = '" + xFilial("SEF") + "' AND "
			EndIf
			
			cQuery += "   EF_DATA between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
			cQuery += "   EF_LA <> 'S' AND "
			cQuery += "	EF_LIBER = 'S ' AND "
			cQuery += "   D_E_L_E_T_ = ' ' "
			
			// Ponto de Entrada para filtrar registros do SE2.
			If l370EFFIL
				cQuery += Execblock("F370EFF",.F.,.F.,cQuery)
			EndIf
			
			// Ponto de Entrada para alterar ordenacao do SEF
			If l370EFKEY
				cChave := Execblock("F370EFK",.F.,.F.,cChave)
			EndIf
			
			// seta a ordem de acordo com a opcao do usuario
			cQuery += " ORDER BY " + SqlOrder(cChave)
			cQuery := ChangeQuery(cQuery)

			If __lConoutR
				ConoutR( cQuery )
			Endif

			cAliasSEF := FINNextAlias()
			
			If Select(cAliasSEF) > 0
				DbSelectArea( cAliasSEF )
				DbCloseArea()
			Endif

			dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSEF, .F., .T.)

			For nI := 1 TO LEN(aStru)
				If aStru[nI][2] != "C"
					TCSetField(cAliasSEF, aStru[nI][1], aStru[nI][2], aStru[nI][3], aStru[nI][4])
				EndIf
			Next
			DbGoTop()
		EndIf
	EndIf
	
	// La�o da contabiliza豫o dia a dia - pela emiss�o (mv_par03 = 1) ou pela database (mv_par03 = 2)
	If MV_PAR03 == MVP03EMISSAO
		nPeriodo := mv_par05 - mv_par04 + 1 // Data Final - Data inicial
		nPeriodo := Iif( nPeriodo == 0, 1, nPeriodo )
	Else
		nPeriodo := 1
	Endif
	
	dDataIni := mv_par04
	
	If ! lBat
		ProcRegua(nPeriodo)
	Endif

	For nLaco := 1 to nPeriodo
		
		If ! lBat
			IncProc()
		Endif
		dbSelectArea( "SE1" )
		
		nTotal:=0
		nHdlPrv:=0
		lCabecalho:=.F.
		
		// Se a contabiliza豫o for pela data de emiss�o, altera o valor
		// da data-base e dos par�metros, para efetuar a contabiliza豫o
		// e a sele豫o dos registros respectivamente.
		If MV_PAR03 == MVP03EMISSAO
			dDataCtb := dDataIni + nLaco - 1

			mv_par04 := dDataCtb
			mv_par05 := dDataCtb

			// For�o a altera豫o do database para o periodo que estou contabilizando
			dDataBase := dDataCtb
		Endif
		
		// Verifica o Numero do Lote - SX5 Tabela 09 Chave FIN
		cLote := LoteCont("FIN")
		
		If (mv_par06 == 1 .or. mv_par06 == 4) .and. lLerSe1
			// Contas a Receber - SE1
			If lMultiThr
				dbSelectArea( cAliasSE1 )
				If MV_PAR03 <> MVP03EMISSAO
					cCondWhile:= "'"+xFilial("SE1")+"' == "+cAliasSE1+"->E1_FILIAL .And. ( "+cAliasSE1+"->E1_EMISSAO >= mv_par04	.And. "+cAliasSE1+"->E1_EMISSAO <= mv_par05 )"
				Else
					cCondWhile:= "'"+xFilial("SE1")+"' == "+cAliasSE1+"->E1_FILIAL .And. "+cAliasSE1+"->E1_EMISSAO == mv_par04"
				Endif
			Else
				If __lDefTop
					dbSelectArea("SE1")
					lQrySE1	:= .T.

					cChave := "E1_FILIAL+E1_EMISSAO+E1_NOMCLI+E1_PREFIXO+E1_NUM+E1_PARCELA"
					aStru := SE1->(DbStruct())
					
					cQuery := ""
					aEval(DbStruct(),{|e| cQuery += ","+AllTrim(e[1])})

					// Obtem os registros a serem processados
					cQuery := "SELECT " + SubStr(cQuery,2)
					cQuery += "     , SE1.R_E_C_N_O_ SE1RECNO "
					cQuery += "  FROM " + RetSqlName("SE1") + " SE1 "

					// Filtro das filiais
					IF ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))											
						If lPosE1MsFil .AND. !lUsaFilOri
						cQuery += " WHERE E1_MSFIL = '" + cFilAnt + "'"
					Else
							cQuery += " WHERE E1_FILORIG = '" + cFilAnt + "'"
						Endif												
					Else
						cQuery += " WHERE E1_FILIAL = '" + xFilial("SE1") + "'"
					EndIf
					
					cQuery += "  AND E1_EMISSAO BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "'"

					cQuery += "  AND E1_LA <> 'S' AND E1_ORIGEM <> 'FINA677' "

					If nCtbVenda == 2 //Contabiliza豫o por venda
						cQuery += " AND E1_ORIGEM <> 'FINI055'"
					Endif
					
					cQuery += "  AND D_E_L_E_T_ = ' ' "

					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Ponto de Entrada para filtrar registros do SE1. �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					If l370E1FIL
						cQuery := Execblock("F370E1F",.F.,.F.,cQuery)
					EndIf
					
					// seta a ordem de acordo com a opcao do usuario
					cQuery += " ORDER BY " + SqlOrder(cChave)
					
					cQuery := ChangeQuery(cQuery)
					
					If __lConoutR
						ConoutR( cQuery )
					Endif

					cAliasSE1 := FINNextAlias()
					
					If Select( cAliasSE1 ) > 0
						DbSelectArea(cAliasSE1)
						DbCloseArea()
					Endif
					
					dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSE1, .F., .T.)

					For nI := 1 TO LEN(aStru)
						If aStru[nI][2] != "C"
							TCSetField(cAliasSE1, aStru[nI][1], aStru[nI][2], aStru[nI][3], aStru[nI][4])
						EndIf
					Next
				Endif
					
				If lQrySE1
					If Select( cAliasSE1 ) > 0
						DbSelectArea(cAliasSE1)
						(cAliasSE1)->(DbGoTop())
					Else
						UserException( 'ERRO NA BUSCA DOS DADOS DA SE1' )
						Final()
					Endif
				Else
					cAliasSE1 := "SE1"
					
					dbSelectArea( cAliasSE1 )
					(cAliasSE1)->(dbSetOrder(6))
					(cAliasSE1)->(dbSeek(cFilial+Dtos(mv_par04),.T. ))
				Endif

				If lF370E1WH
					cCondWhile:= Execblock("F370E1W",.F.,.F.)
				Else
					If __lDefTop
						cCondWhile:= " .T. "
					Else
						If MV_PAR03 <> MVP03EMISSAO
							cCondWhile:= "'"+cFilial+"' == SE1->E1_FILIAL .And. ( SE1->E1_EMISSAO >= mv_par04	.And. SE1->E1_EMISSAO <= mv_par05 )"
						Else
							cCondWhile:= "'"+cFilial+"' == SE1->E1_FILIAL .And. SE1->E1_EMISSAO == mv_par04"
						Endif
					Endif
					
				EndIf
			EndIf
			
			DbSelectArea(cAliasSE1)
			While (cAliasSE1)->(!Eof()) .And. &cCondWhile

				If ( cAliasSE1 <> "SE1" )
					SE1->(dbGoto( (cAliasSE1)->SE1RECNO ) )
				EndIf
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//� Confirma a sele豫o do titulo para contabiliza豫o.  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				If l370E1LGC .And. ( cAliasSE1 == "SE1" )
					If !Execblock("F370E1L",.F.,.F.)
						(cAliasSE1)->(dbSkip())
						Loop
					EndIf
				EndIf
				
				cPadrao := "500"

				// Desdobramento
				If SE1->E1_DESDOBR == "1" // 1-Sim | 2-N�o
					cPadrao := "504"
				EndIf
				
				dDataCtb := IF(MV_PAR03 == MVP03EMISSAO, SE1->E1_EMIS1, dDataBase)
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Verifica se ser� gerado Lan놹mento Cont쟟il			  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				If SE1->E1_LA == "S" .Or. (SE1->E1_TIPO $ MVPROVIS .And. mv_par17 <> 1)
					(cAliasSE1)->( dbSkip())
					Loop
				Endif
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona no cliente.										  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				dbSelectArea( "SA1" )
				SA1->(dbSeek(xFilial("SA1")+SE1->E1_CLIENTE+SE1->E1_LOJA))
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona na natureza.										  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				dbSelectArea( "SED" )
				SED->(dbSeek(cFilial+SE1->E1_NATUREZ))
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona na SE5,se RA										  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				lRAMov := .F.
				If SE1->E1_TIPO $ MVRECANT
					dbSelectArea("SE5")
					SE5->(dbSetOrder(2))
					lRAMov := SE5->(dbSeek(xFilial()+"RA"+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+DtoS(SE1->E1_EMISSAO)+SE1->E1_CLIENTE+SE1->E1_LOJA))
					SE5->(dbSetOrder(1))
				Endif
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona no banco.											  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				dbSelectArea("SA6")
				SA6->(dbSetOrder(1))
				SA6->(dbSeek(cFilial+SE1->E1_PORTADO+SE1->E1_AGEDEP+SE1->E1_CONTA))
				// Se for um recebimento antecipado e nao encontrou o banco
				// pelo SE1, pesquisa pelo SE5.
				IF SE1->E1_TIPO $ MVRECANT
					If SA6->(Eof())
						dbSeek( cFilial+SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA )
					Endif
					cPadrao:="501"
				Endif
				
				dbSelectArea("SE1")
				
				lPadrao		:= VerPadrao(cPadrao)
				lPadraoCc 	:= VerPadrao("506") //Rateio por C.Custo de MultiNat C.Receber
				
				if lPadrao 
				
					If !lCabecalho
						a370Cabecalho(@nHdlPrv,@cArquivo)
					Endif
					cChaveSev := RetChaveSev("SE1")
					cChaveSez := RetChaveSev("SE1",,"SEZ")
					dbSelectArea("SE1")
					nRecSe1 := Recno()
					DbSelectArea("SEV")
					// Se utiliza multiplas naturezas, contabiliza pelo SEV
					If  SE1->E1_MULTNAT == "1" .And. MsSeek(cChaveSev)

						dbSelectArea("SE1")
						nRecSe1 := Recno()
						DbGoBottom()
						DbSkip()
						DbSelectArea("SEV")
						dbSetOrder(2)
						While xFilial("SEV")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIFOR+;
							EV_LOJA+EV_IDENT) == cChaveSev+"1" .And. SEV->(!Eof())

							If SEV->EV_LA != "S"
								dbSelectArea( "SED" )
								MsSeek( xFilial("SED")+SEV->EV_NATUREZ ) // Posiciona na natureza, pois a conta pode estar la.
								dbSelectArea("SEV")
								
								If SEV->EV_RATEICC == "1" .and. lPadraoCC .and. lPadrao// Rateou multinat por c.custo
									dbSelectArea("SEZ")
									dbSetOrder(4)
									MsSeek(cChaveSeZ+SEV->EV_NATUREZ) // Posiciona no arquivo de Rateio C.Custo da MultiNat
								
									While SEZ->(!Eof()) .and. xFilial("SEZ")+SEZ->(EZ_PREFIXO+EZ_NUM+;
										EZ_PARCELA+EZ_TIPO+EZ_CLIFOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT) == cChaveSeZ+SEV->EV_NATUREZ+"1"
										
										If SEZ->EZ_LA != "S"

											If lUsaFlag
												aAdd(aFlagCTB,{"EZ_LA","S","SEZ",SEZ->(Recno()),0,0,0})
											EndIf

											nTotDoc	+=	DetProva(nHdlPrv,"506","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

											If LanceiCtb // Vem do DetProva
												If !lUsaFlag
													RecLock("SEZ")
													SEZ->EZ_LA    := "S"
													MsUnlock( )
												EndIf
											ElseIf lUsaFlag
												If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEZ->(Recno()) }))>0
													aFlagCTB := Adel(aFlagCTB,nPosReg)
													aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
												Endif
											EndIf
										Endif
										SEZ->(dbSkip())
									Enddo
									DbSelectArea("SEV")
								Else
									If lUsaFlag
										aAdd(aFlagCTB,{"EV_LA","S","SEV",SEV->(Recno()),0,0,0})
									EndIf
									nTotDoc	:= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
								Endif
								
								If LanceiCtb // Vem do DetProva
									If !lUsaFlag	
										RecLock("SEV")
										SEV->EV_LA    := "S"
										MsUnlock( )
									EndIf
								ElseIf lUsaFlag
									If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEV->(Recno()) }))>0
										aFlagCTB := Adel(aFlagCTB,nPosReg)
										aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
									Endif
								Endif
								
							Endif

							DbSelectArea("SEV")
							DbSkip()
						Enddo
						nTotal  	+=	nTotDoc
						nTotProc	+=	nTotDoc // Totaliza por processo

						If !lCabecalho
							a370Cabecalho(@nHdlPrv,@cArquivo)
						Endif
						nRecSev := SEV->(Recno())
						nRecSez := SEZ->(Recno())

						dbSelectArea("SEV")
						dbGobottom()
						dbSkip()

						DbSelectArea("SEZ")
						dbGobottom()
						dbSkip()

						nTotDoc	+=	DetProva(nHdlPrv,"506","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

						If MV_PAR12 == MVP12DOCUMENTO 
							IF nTotDoc > 0 // Por documento
								Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
								nTotDoc := 0
							Endif
							aFlagCTB := {}
						Endif
						
						dbSelectArea("SE1")
						DbGoto(nRecSe1)
					Endif

					dbSelectArea("SEV")
					DbGoBottom()
					DbSkip()
					
					DbSelectArea("SE1")
					dbGoto(nRecSe1)
					cNumLiq := SE1->E1_NUMLIQ
					If lUsaFlag
						// Carrega em aFlagCTB os recnos ref. SE5 e FKs
						// Quando n�o lUsaFlag aRecsSE5 efetuar� a marca豫o
						If lRAMov
							CTBAddFlag(aFlagCTB)
						EndIf
						aAdd(aFlagCTB,{"E1_LA","S","SE1",SE1->(Recno()),0,0,0})
					EndIf
					If !lCabecalho	// Se n�o houver cabe�alho aberto, abre.
						a370Cabecalho(@nHdlPrv,@cArquivo)
					EndIf

					//Posiciono FO2 e alimento vari�vel referente ao juros informado na liquida豫o
					If !Empty(cNumLiq)
						dbSelectArea("FO2")
						nRecFO2 := F460PosFO2( nRecSe1 , cNumLiq )
						If nRecFO2 > 0
							FO2->(dbGoto(nRecFO2))
							JUROS3 := FO2->FO2_VLJUR
						EndIf
					EndIf					
					nTotDoc	:= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
					nTotal	+= nTotDoc
					nTotProc	+= nTotDoc //Totaliza por processo

					If MV_PAR12 == MVP12DOCUMENTO // Por documento
						IF nTotDoc > 0
							If lSeqCorr
								aDiario := {{"SE1",SE1->(recno()),SE1->E1_DIACTB,"E1_NODIA","E1_DIACTB"}}
							EndIf
							Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
						Endif
						aFlagCTB := {}
					Endif

					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
					//� Atualiza Flag de Lan놹mento Cont쟟il		  �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
					If LanceiCtb
						If !lUsaFlag
							Reclock("SE1")
							REPLACE E1_LA With "S"
							MsUnlock( )
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE1->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
				Endif
				DbSelectArea(cAliasSE1)
				(cAliasSE1)->(dbSkip())
			Enddo			
			
			If MV_PAR12 == MVP12PROCESSO 
				If nTotProc > 0 // Por processo
					Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
					nTotProc := 0
				Endif
				aFlagCTB := {}
			Endif
			
			// Encerro o temporario criado para a contabiliza豫o.
			If lQrySE1 .And. Select( cAliasSE1 ) > 0
				DbSelectArea(cAliasSE1)
				DbCloseArea()
				cAliasSE1 := "SE1"
			Endif
		Endif		

		If (mv_par06 == 2 .or. mv_par06 == 4) .and. lLerSE2

			IF Select(cAliasSE2)<=0
				If !__lSchedule
					MsAguarde( {|| Sleep( 2000 ) }, "Erro ao trazer os dados da SE2.", "CTBAFIN")
				EndIf
			
				cAliasSE2 := "SE2"
			Endif
	
			// Contas a Pagar - SE2
			// Contabiliza pelo E2_EMIS1
			dbSelectArea(cAliasSE2)
			While (cAliasSE2)->( !Eof() ) .And. (cAliasSE2)->E2_EMIS1 >= mv_par04 .And. (cAliasSE2)->E2_EMIS1 <= mv_par05
				
				If !lPass .And. lMvAGP
					While !Eof()
						If (cAliasSE2)->E2_PREFIXO == "AGP" .And. Empty((cAliasSE2)->E2_TITPAI) .And. Alltrim((cAliasSE2)->E2_ORIGEM) == "FINA378"
							lAGP := .T.
							Exit
						EndIf
						nSkip += 1
						(cAliasSE2)->(DbSkip())
					EndDo
					lPass := .T.
					If nSkip >= 1
						nSkip := 0
						(cAliasSE2)->(DbGotop())
					EndIf
				EndIf
				
				DBSelectArea("SE2")
				SE2->(dbGoto((cAliasSE2)->SE2RECNO))
				lMulNatSE2 := .F.
				lPAMov	:= .F.
				dDataCtb := IF(MV_PAR03 == MVP03EMISSAO, SE2->E2_EMIS1, dDataBase)
				                    
				// Nao contabiliza titulos de impostos aglutinados com origem na rotina FINA378
				// O par�metro MV_CTBAGP libera a contabiliza豫o dos t�tulos do PCC aglutinados.
				If !lAGP // Neste caso o Par�metro de contabiliza豫o dos impostos aglutinados inverte a opera豫o
					If	AllTrim( Upper( SE2->E2_ORIGEM ) ) == "FINA378" .And. SE2->E2_PREFIXO == "AGP"	// Aglutinacao Pis/Cofins/Csll
						(cAliasSE2)->(dbSkip())
						Loop
					Endif
				ElseIf AllTrim(SE2->E2_CODRET) == "5952" .And. ( (SE2->E2_PREFIXO != "AGP" .Or. !Empty(SE2->E2_TITPAI)) .And. AllTrim(SE2->E2_ORIGEM) != "FINA378") .And. (lF370ChkAgp .And. F370ChkAgp())
					RecLock("SE2",.F.)
					SE2->E2_LA := 'S'
					SE2->(MsUnLock())
					(cAliasSE2)->(dbSkip())
					Loop
				EndIf

				// Contabiliza movimentacao bancaria de adiantamento - MV_CTMOVPA
				If SE2->E2_TIPO $ MVPAGANT
					If lCtMovPa
						(cAliasSE2)->(dbSkip())
						Loop
					EndIf
					nRecMovPa := F080MovPA(.T.,SE2->E2_PREFIXO,SE2->E2_NUM,SE2->E2_PARCELA,SE2->E2_TIPO,SE2->E2_FORNECE,SE2->E2_LOJA)
					lPAMov := nRecMovPa > 0
				Endif

				If	SE2->E2_TIPO $ MVTAXA+"/"+MVISS+"/"+MVINSS .And.;
					( AllTrim(SE2->E2_FORNECE) $ cUniao .Or.;
					AllTrim(SE2->E2_FORNECE) $ cMunic)
					// Contabiliza rateio de impostos em multiplas naturezas e multiplos
					// centros de custos.
					lPadrao:=VerPadrao("510")
					lPadraoCC := VerPadrao("508")  // Rateio C.Custo de MultiNat Pagar
					If lPadrao
						If SE2->E2_RATEIO != "S"
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							Endif
							cChaveSev := RetChaveSev("SE2")
							cChaveSeZ := RetChaveSev("SE2",,"SEZ")
							DbSelectArea("SEV")
							// Se utiliza multiplas naturezas, contabiliza pelo SEV
							If SE2->E2_MULTNAT=="1" .And. MsSeek(cChaveSev)
								lMulNatSE2 := .F.
								dbSelectArea("SE2")
								nRecSe2 := Recno()
								DbGoBottom()
								DbSkip()

								DbSelectArea("SEV")
								dbSetOrder(2)
								While xFilial("SEV")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIFOR+;
									EV_LOJA+EV_IDENT) == cChaveSev+"1" .And. !Eof()
									If SEV->EV_LA != "S"
										dbSelectArea( "SED" )
										MsSeek( xFilial("SED")+SEV->EV_NATUREZ ) // Posiciona na natureza, pois a conta pode estar la.
										dbSelectArea("SEV")
										If SEV->EV_RATEICC == "1" .and. lPadrao .and. lPadraoCC // Rateou multinat por c.custo
											dbSelectArea("SEZ")
											dbSetOrder(4)
											MsSeek(cChaveSeZ+SEV->EV_NATUREZ) // Posiciona no arquivo de Rateio C.Custo da MultiNat
											While SEZ->(!Eof()) .and. xFilial("SEZ")+SEZ->(EZ_PREFIXO+EZ_NUM+;
												EZ_PARCELA+EZ_TIPO+EZ_CLIFOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT) == cChaveSeZ+SEV->EV_NATUREZ+"1"

												If SEZ->EZ_LA != "S"

													If lUsaFlag
														aAdd(aFlagCTB,{"EZ_LA","S","SEZ",SEZ->(Recno()),0,0,0})
													EndIf

													VALOR := 0
													VALOR2 := 0
													VALOR3 := 0
													VALOR4 := 0
													Do Case
														Case SEZ->EZ_TIPO $ MVTAXA
															VALOR2 := SEZ->EZ_VALOR
														Case SEZ->EZ_TIPO $ MVISS
															VALOR3 := SEZ->EZ_VALOR
														Case SEZ->EZ_TIPO $ MVINSS
															VALOR4 := SEZ->EZ_VALOR
													EndCase
													nTotDoc	+=	DetProva(nHdlPrv,"508","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

													If LanceiCtb // Vem do DetProva
														If !lUsaFlag
															RecLock("SEZ")
															SEZ->EZ_LA    := "S"
															MsUnlock( )
														EndIf
													ElseIf lUsaFlag
														If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEZ->(Recno()) }))>0
															aFlagCTB := Adel(aFlagCTB,nPosReg)
															aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
														Endif														
													Endif

												Endif
												SEZ->(dbSkip())
											Enddo
										Else
											VALOR := 0
											VALOR2 := 0
											VALOR3 := 0
											VALOR4 := 0
											Do Case
												Case SEV->EV_TIPO $ MVTAXA
													VALOR2 := SEV->EV_VALOR
												Case SEV->EV_TIPO $ MVISS
													VALOR3 := SEV->EV_VALOR
												Case SEV->EV_TIPO $ MVINSS
													VALOR4 := SEV->EV_VALOR
											EndCase

											If lUsaFlag
												aAdd(aFlagCTB,{"EV_LA","S","SEV",SEV->(Recno()),0,0,0})
											EndIf

											nTotDoc	+=	DetProva(nHdlPrv,"510","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

										Endif
										If LanceiCtb // Vem do DetProva
											If !lUsaFlag
												RecLock("SEV")
												SEV->EV_LA    := "S"
												MsUnlock( )
											EndIf
										ElseIf lUsaFlag
											If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEV->(Recno()) }))>0
												aFlagCTB := Adel(aFlagCTB,nPosReg)
												aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
											Endif
										Endif

									Endif
									dbSelectArea("SEV")
									DbSkip()
								Enddo
								nTotal  	+=	nTotDoc
								nTotProc	+=	nTotDoc // Totaliza por processo
								nRecSev := SEV->(Recno())
								nRecSez := SEZ->(Recno())
								dbSelectArea("SEV")
								dbGobottom()
								dbSkip()
								dbSelectArea("SEV")
								dbGobottom()
								dbSkip()
								dbSelectArea("SE2")			// permite contabilizar os impostos pelo SE2
								dbGoto(nRecSe2)
								If lUsaFlag
									aAdd(aFlagCTB,{"E2_LA","S","SE2",SE2->(Recno()),0,0,0})
								EndiF
								If !lCabecalho
									a370Cabecalho(@nHdlPrv,@cArquivo)
								Endif
								
								nTotDoc	+=	DetProva(nHdlPrv,"508","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
								SEV->(dbGoto(nRecSev))
								SEZ->(dbGoto(nRecSez))
								dbSelectArea("SE2")
								DbGoto(nRecSe2)
								If MV_PAR12 == MVP12DOCUMENTO 
									If nTotDoc > 0 // Por documento
										If lF370NatP
											ExecBlock("F370NATP",.F.,.F.,{nHdlPrv,cLote})
										Endif
										If lSeqCorr
											aDiario := {{"SE2",SE2->(recno()),SE2->E2_DIACTB,"E2_NODIA","E2_DIACTB"}}
										EndIf
										Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
										nTotDoc := 0
									Endif
									aFlagCTB := {}
								Endif
								
								//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
								//� Atualiza Flag de Lan놹mento Cont쟟il 	   �
								//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
								If LanceiCtb
									If !lUsaFlag
										Reclock("SE2")
										Replace E2_LA With "S"
										SE2->(MsUnlock())
									EndIF
								ElseIf lUsaFlag
									If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE2->(Recno()) }))>0
										aFlagCTB := Adel(aFlagCTB,nPosReg)
										aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
									Endif
								EndIf
							Endif
						Endif
					Endif
					// Fim da contabilizacao de titulos de impostos por multiplas natureza
					// e multiplos centros de custos
					dbSelectArea( cAliasSE2 )
					If lMulNatSE2
						dbSkip()
						Loop
					Endif
				Endif
				cPadrao := "510"
				
				IF	SE2->E2_TIPO $ MVPAGANT
					cPadrao:="513"
				EndIF
				
				If	SE2->E2_RATEIO == "S"
					cPadrao := "511"
				EndIf
				
				If	SE2->E2_DESDOBR == "S"
					cPadrao := "577"
				EndIf
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona no fornecedor.									  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				dbSelectArea( "SA2" )
				dbSeek( xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA )
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona na natureza.										  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				dbSelectArea( "SED" )
				dbSeek( xFilial("SED")+SE2->E2_NATUREZ )

				dbSelectArea("SE2")
				
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//� Posiciona na SE5 e no Banco,se PA e SEF para Cheque �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				If SE2->E2_TIPO $ MVPAGANT
					
					dbSelectArea("SE5")
					dbSetOrder(1)
					If lPaMov
						SE5->( DbGoTo(nRecMovPa) )
					EndIf
					
					dbSelectArea("SEF") // Busca CHEQUE
					dbSetOrder(3)
					dbSeek(xFilial()+SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_PARCELA+SE2->E2_TIPO+SE2->E2_NUMBCO)
					
					dbSelectArea( "SA6" )
					dbSetOrder(1)
					If SE5->(Found()) .or. lPaMov // Busca Movimento Bancario
						dbSeek( xFilial()+SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA)
					Else
						dbSeek( xFilial()+SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA)
					Endif

					SE5->( DbGoTo(0) ) // Desposiciona SE5
					dbSelectArea( "SE2" )
				Endif
				lPadrao		:= VerPadrao(cPadrao) 
				lPadraoCC 	:= VerPadrao("508")  // Rateio C.Custo de MultiNat Pagar
				
				If lPadrao  
				
					If SE2->E2_RATEIO != "S"
						If !lCabecalho
							a370Cabecalho(@nHdlPrv,@cArquivo)
						Endif
						cChaveSev := RetChaveSev("SE2")
						cChaveSeZ := RetChaveSev("SE2",,"SEZ")
						DbSelectArea("SEV")
						// Se utiliza multiplas naturezas, contabiliza pelo SEV
						If SE2->E2_MULTNAT == "1" .And. MsSeek(cChaveSev)
							dbSelectArea("SE2")
							nRecSe2 := Recno()
							DbSelectArea("SEV")
							dbSetOrder(2)
							While xFilial("SEV")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIFOR+;
								EV_LOJA+EV_IDENT) == cChaveSev+"1" .And. !Eof()

								If SEV->EV_LA != "S"

									dbSelectArea( "SED" )
									MsSeek( xFilial("SED")+SEV->EV_NATUREZ ) // Posiciona na natureza, pois a conta pode estar la.
									dbSelectArea("SEV")
									If SEV->EV_RATEICC == "1" .and. lPadrao .and. lPadraoCC // Rateou multinat por c.custo

										dbSelectArea("SEZ")
										dbSetOrder(4)
										MsSeek(cChaveSeZ+SEV->EV_NATUREZ) // Posiciona no arquivo de Rateio C.Custo da MultiNat
										While SEZ->(!Eof()) .and. xFilial("SEZ")+SEZ->(EZ_PREFIXO+EZ_NUM+;
												EZ_PARCELA+EZ_TIPO+EZ_CLIFOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT) == cChaveSeZ+SEV->EV_NATUREZ+"1"

											If SEZ->EZ_LA != "S"
												If lUsaFlag
													aAdd(aFlagCTB,{"EZ_LA","S","SEZ",SEZ->(Recno()),0,0,0})
												EndiF
	
												VALOR2	:= 0
												VALOR3	:= 0
												VALOR4	:= 0
												VALOR  	:= 0
												Do Case
													Case SEZ->EZ_TIPO $ MVTAXA
														VALOR2 := SEZ->EZ_VALOR
													Case SEZ->EZ_TIPO $ MVISS
														VALOR3 := SEZ->EZ_VALOR
													Case SEZ->EZ_TIPO $ MVINSS
														VALOR4 := SEZ->EZ_VALOR
													Otherwise
														VALOR  := SEZ->EZ_VALOR
												EndCase
												nTotDoc	+=	DetProva(nHdlPrv,"508","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

												If LanceiCtb // Vem do DetProva
													If !lUsaFlag
														RecLock("SEZ")
														SEZ->EZ_LA    := "S"
														MsUnlock( )
													EndIf
												ElseIf lUsaFlag
													If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEZ->(Recno()) }))>0
														aFlagCTB := Adel(aFlagCTB,nPosReg)
														aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
													Endif
												Endif
											Endif

											SEZ->(dbSkip())
										Enddo
										DbSelectArea("SEV")
									Else
										VALOR  := 0
										VALOR2 := 0
										VALOR3 := 0
										VALOR4 := 0
										Do Case
											Case SEV->EV_TIPO $ MVTAXA
												VALOR2 := SEV->EV_VALOR
											Case SEV->EV_TIPO $ MVISS
												VALOR3 := SEV->EV_VALOR
											Case SEV->EV_TIPO $ MVINSS
												VALOR4 := SEV->EV_VALOR
											Otherwise
												VALOR  := SEV->EV_VALOR
										EndCase
										If lUsaFlag
											aAdd(aFlagCTB,{"EV_LA","S","SEV",SEV->(Recno()),0,0,0})
										EndIf
										nTotDoc	+= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
									Endif
									If LanceiCtb // Vem do DetProva
										If !lUsaFlag
											RecLock("SEV")
											SEV->EV_LA    := "S"
											MsUnlock( )
										EndIf
									ElseIf lUsaFlag
										If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEV->(Recno()) }))>0
											aFlagCTB := Adel(aFlagCTB,nPosReg)
											aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
										Endif
									Endif

								Endif
								dbSelectArea("SEV")
								DbSkip()

								// Inicializa Vari�veis
								VALOR  := 0
								VALOR2 := 0
								VALOR3 := 0
								VALOR4 := 0
								
							Enddo
							nTotal  	+=	nTotDoc
							nTotProc	+=	nTotDoc // Totaliza por processo
							
							// Desposiciona SEV
							dbSelectArea("SEV")
							DbGoBottom()
							DbSkip()							
							
							// Posiciona T�tulo PPrincipal
							dbSelectArea("SE2")
							DbGoto(nRecSe2)
							If lUsaFlag
								aAdd(aFlagCTB,{"E2_LA","S","SE2",SE2->(Recno()),0,0,0})
							Endif
							nTotDoc	+= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
							If LanceiCtb .AND. !lUsaFlag// Vem do DetProva
								RecLock("SE2")
								Replace E2_LA With "S"
								SE2->(MsUnlock())
							ElseIf !LanceiCtb .and. lUsaFlag
								If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE2->(Recno()) }))>0
									aFlagCTB := Adel(aFlagCTB,nPosReg)
									aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
								Endif
							Endif

							If MV_PAR12 == MVP12DOCUMENTO .OR. MV_PAR12 == MVP12PROCESSO
								If lF370NatP
									ExecBlock("F370NATP",.F.,.F.,{nHdlPrv,cLote})
								Endif

								If MV_PAR12 == MVP12DOCUMENTO .AND. nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SE2",SE2->(recno()),SE2->E2_DIACTB,"E2_NODIA","E2_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
									nTotDoc := 0
									aFlagCTB := {}
								Endif
							Endif

 						Endif

						dbSelectArea( "SE2" )
						If !SE2->E2_LA == "S" .AND. SE2->E2_MULTNAT <> '1'
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							Endif
							
							If lUsaFlag
								aAdd(aFlagCTB,{"E2_LA","S","SE2",SE2->(Recno()),0,0,0})
							EndIf
							
							nTotDoc	+=	DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

							nTotProc+= nTotDoc
							nTotal	+=	nTotDoc
							If MV_PAR12 == MVP12DOCUMENTO 
								If nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SE2",SE2->(recno()),SE2->E2_DIACTB,"E2_NODIA","E2_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
									nTotDoc := 0
								Endif
								aFlagCTB := {}
							Endif

							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Atualiza Flag de Lan놹mento Cont쟟il 		  �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If LanceiCtb
								If !lUsaFlag
									Reclock("SE2")
									Replace E2_LA With "S"
									SE2->(MsUnlock())
								EndIf
							ElseIf lUsaFlag
								If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE2->(Recno()) }))>0
									aFlagCTB := Adel(aFlagCTB,nPosReg)
									aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
								Endif							
							EndIf
						EndIf
					Else
						
						// Devido a estrutura do programa, o rateio ja eh "quebrado"
						// por documento.
						If !lCabecalho							/// Se n�o houver cabe�alho aberto, abre.
							a370Cabecalho(@nHdlPrv,@cArquivo)
						EndIf

						RegToMemory("SE2",.F.,.F.)

						If lUsaFlag
							aAdd(aFlagCTB,{"E2_LA","S","SE2",SE2->(Recno()),0,0,0})
						EndIf

						F370RatFin(cPadrao,"FINA370",cLote,4," ",4,,,,@nHdlPrv,@nTotDoc,@aFlagCTB)

						lCabecalho := If(nHdlPrv <= 0, lCabecalho, .T.)
						nTotProc	+= nTotDoc
						nTotal	+=	nTotDoc

						If MV_PAR12 == MVP12DOCUMENTO 
							If nTotDoc > 0 // Por documento
								If lSeqCorr
									aDiario := {{"SE2",SE2->(recno()),SE2->E2_DIACTB,"E2_NODIA","E2_DIACTB"}}
								EndIf						
								Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
							Endif
							aFlagCTB := {}
						Endif
						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
						//� Atualiza Flag de Lan놹mento Cont쟟il		  �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
						If LanceiCtb
							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Atualiza Flag de Lan놹mento Cont쟟il		  �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							dbSelectArea("SE2")
							If !lUsaFlag
								Reclock("SE2")
								Replace E2_LA With "S"
								SE2->(MsUnlock())
							EndIf
						ElseIf lUsaFlag
							If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE2->(Recno()) }))>0
								aFlagCTB := Adel(aFlagCTB,nPosReg)
								aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
							Endif
						EndIf
						
					Endif
				Endif
				dbSelectArea(cAliasSE2)
				dbSkip()
				LanceiCtb := .F.
				
			Enddo

			If MV_PAR12 == MVP12PROCESSO 
				IF nTotProc > 0 // Por processo
					Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
					nTotProc := 0
				Endif
				aFlagCTB := {}
			Endif
			
			dbSelectArea( cAliasSE2 )
		Endif
			
		//Contabilizacao do SE5
 		If lLerSE5
			
			//Query para contabiliza豫o do troco pela FK5
			If !Empty(cAliasTroc)
				
				While (cAliasTroc)->( !EOF() ) .And. (cAliasTroc)->FK5_DATA >= mv_par04 .And. (cAliasTroc)->FK5_DATA <= mv_par05
					lTroTemSE5 := .F.
					nTotProc := 0
					FK5->( dbGoTo( (cAliasTroc)->R_E_C_N_O_  ) )

					SED->( dbSetOrder(1) ) //ED_FILIAL+ED_CODIGO
					SED->( dbSeek( FWxFilial("SED") + FK5->FK5_NATURE ) )
					SA6->( dbSetOrder(1) ) //A6_FILIAL+A6_COD+A6_AGENCIA+A6_NUMCON
					SA6->( dbSeek( FWxFilial("SA6") + FK5->FK5_BANCO + FK5->Fk5_AGENCI + FK5->FK5_CONTA ) )

					If FK5->FK5_TPDOC == "VL"
						cPadrao := "5C8" //Movimento Banc�rio - Movimento de Troco

						SE5->( dbSetOrder(21) ) //E5_FILIAL+E5_IDORIG+E5_TIPODOC
						If SE5->( msSeek( FWxFilial("SE5") + Fk5->FK5_IDMOV + FK5->FK5_TPDOC ) )
							lTroTemSE5 := .T.
						EndIf
					ElseIf FK5->FK5_TPDOC == "ES"
						cPadrao := "5C9" //Movimento Banc�rio - Estorno de Movimento de Troco
					EndIf
					
					lPadrao := VerPadrao(cPadrao)
					
					If lPadrao
						If !lCabecalho
							a370Cabecalho( @nHdlPrv, @cArquivo )
							lCabecalho := Iif( nHdlPrv > 0, .T., .F. )
						EndIf

						//Se atualiza as flags dos registros de troco pelo CTB
						If lUsaFlag
							aAdd( aFlagCTB, {"FK5_LA", "S", "FK5", (cAliasTroc)->R_E_C_N_O_, 0, 0, 0} )
							If lTroTemSE5
								aAdd( aFlagCTB, {"E5_LA", "S", "SE5", SE5->( Recno() ) , 0, 0, 0} )
							EndIf
						EndiF

						nTotDoc	:= DetProva( nHdlPrv, cPadrao, "FINA370", cLote,,,,,, aCT5,, @aFlagCTB ) //Total por documento
						nTotProc += nTotDoc //Total por processo
						nTotal += nTotDoc //Total por per�odo

						//Se n�o atualiza as flags de registros de troco pelo CTB, ent�o atualiza aqui com reclock
						If !lUsaFlag .And. LanceiCtb
							Reclock( "FK5", .F. )
							FK5->FK5_LA := "S"
							FK5->( msUnlock() )

							If lTroTemSE5
								Reclock( "SE5", .F. )
								SE5->E5_LA := "S"
								SE5->( msUnlock() )
							EndIf
						EndIf

						//Contabiliza o troco por documento
						If MV_PAR12 == MVP12DOCUMENTO
							If nTotDoc > 0
								Ca370Incl( cArquivo, @nHdlPrv, cLote, @aFlagCTB,, FK5->FK5_DATA )
							Endif
							
							aFlagCTB := {}
						Endif

					EndIf
					
					(cAliasTroc)->( dbSkip() )
				EndDo

			EndIf

			aRecsSE5 := {}
			aRecsTRF := {} //Registros de transferencia bancaria

			cCondFilSE5 := xFilial("SE5")
			
			cCondWhile:= "( (" + cAliasSE5 + "->" + cCampo + " >= mv_par04 .And. " + cAliasSE5 + "->" + cCampo + " <= mv_par05 ) .AND. "
			cCondWhile += " !(" + cAliasSE5 + "->E5_TIPODOC  $ 'TR#TE') ) .OR. "

			cCondWhile += "( ((" + cAliasSE5 + "->E5_DATA >= mv_par04 .AND. " + cAliasSE5 + "-> E5_DATA <= mv_par05) .OR. "
			cCondWhile += " (" + cAliasSE5 + "->" + cCampo + " >= mv_par04 .And. " + cAliasSE5 + "->" + cCampo + " <= mv_par05)) "
			cCondWhile += " .AND. " + cAliasSE5 + "->E5_TIPODOC $ 'TR#TE' .OR. Empty(" + cAliasSE5 + "->(E5_TIPODOC+E5_TIPO))) " // 쟂iltra somente as transferencias bancarias

			nValorTotal := 0 
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
			//쿣ariaveis para suporte a Recebimentos Diversos�
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
			cSELSerie	:= ""
			cSELRecibo	:= ""
			// 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴
			
			cRecPag := (cAliasSE5)->E5_RECPAG

			If MV_PAR12 == MVP12PROCESSO
				// Para cada per�odo processa SE5 em duas etapas:
				//   - Primeiro nas Aplica寤es/Emprestimos e Movimentos Banc�rios; e
				//   - Segundo nas Movimenta寤es relacionadas � SE1-CR e SE2-CP.
				cTipoMov := 'DIRETO'
			EndIf
			

			While VldDtE5(cAliasSe5,cCampo,mv_par05,ddataini)
								
				// garanto que estarei posicionado no primeiro registro da data --------- 
				If !(&cCondWhile)
					(cAliasSE5)->( dbSkip() )
					Loop
				Endif

				dbSelectArea( "SE5" ) // ------------------------------------------------
				If MV_PAR13 == MVP13TOTBOR	// Ctb Bordero - Total/Por Bordero
					SE5->(dbSetOrder(1))	// "E5_FILIAL+DTOS(E5_DATA)+E5_BANCO+E5_AGENCIA+E5_CONTA+E5_NUMCHEQ"
				Else
					SE5->(dbSetOrder(10))	// "E5_FILIAL+E5_DOCUMEN"
				Endif
				
				// Emparelha as tabelas Tempor�ria e Oficial ---------------------------- 
				SE5->(dbGoto((cAliasSE5)->(SE5RECNO)))
				dDataCtb := IF(SE5->E5_TIPODOC $ 'TR#TE',SE5->E5_DATA,(cAliasSE5)->(&cCampo))
				
				// Totaliza movimento banc�rio DIRETO e/ou de T�TULOS - Controlando por processo (MV_PAR12 == MVP12PROCESSO)
				If MV_PAR12 == MVP12PROCESSO
					If cTipoMov == "DIRETO" .and. !EMPTY((cAliasSE5)->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ))
						(cAliasSE5)->(dbSkip())
						If !VldDtE5(cAliasSe5,cCampo,mv_par05,ddataini)
							(cAliasSE5)->(dbGOTOP())
							cTipoMov := "TITULOS"
						EndIf	
						lLoop := .T.
					ElseIf	cTipoMov == "TITULOS" .and. EMPTY((cAliasSE5)->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ))
						(cAliasSE5)->(dbSkip())
						lLoop := .T.
					EndIf

					If !EMPTY(nTotProc) .and. cRecPag <> (cAliasSE5)->E5_RECPAG
						cRecPag := (cAliasSE5)->E5_RECPAG
						Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
						aFlagCTB := {}
						nTotProc := 0
					EndIf

					If lLoop
						lLoop := .F.
						LOOP
					EndIf
				EndIf

				// Posiciona SED Naturezas ----------------------------------------------
				SED->(dbSetOrder(1))	// ED_FILIAL+ED_CODIGO
				SED->(dbSeek(xFilial("SED")+SE5->E5_NATUREZ))
				// Posiciona SA6 Bancos -------------------------------------------------
				SA6->(dbSetOrder(1))	// A6_FILIAL+A6_COD+A6_AGENCIA+A6_NUMCON
				SA6->(dbSeek(xFilial("SA6")+SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA))
				
				// Posiciona SEA Borderos -----------------------------------------------
				SEA->(dbSetOrder(4)) // EA_FILORIG, EA_NUMBOR, EA_CART, EA_PREFIXO, EA_NUM
				SEA->(dbSeek(SE5->E5_FILORIG+PADR(SE5->E5_DOCUMEN,nTamBor)+"P"+SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA)))
				cNumBor :=  SEA->EA_NUMBOR	// Valida豫o de ultimo registro do border�
				SEA->(dbSkip())
				cProxBor := SEA->EA_NUMBOR

				dbSelectArea("SE5")		// Restaura a �rea da tabela Oficial 	

				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//� Movimentos Bancarios e Aplicacoes/Emprestimos - SE5  		  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				If (SE5->E5_TIPODOC $ "  /TR/TE/DB/PA/OD/AP/RF/PE/EP/IT" .OR. Empty(SE5->(E5_TIPODOC+E5_TIPO)))
					cPadrao := ""

					lRaCont := .F. //Limpa variavel de Contabiliza豫o de Baixa de RA - LP 520
					
					If SE5->E5_RECPAG == 'R' .and. SE5->E5_TIPODOC $ "  |DB" .and. ( mv_par06 == 1 .or. mv_par06 == 4 )
						cPadrao	:= Iif( SE5->E5_SITUACA <> "E", "563", "564" )
					ElseIf SE5->E5_RECPAG == 'P' .and. ( mv_par06 == 2 .or. mv_par06 == 4 )
						If !SE5->E5_TIPODOC $ "TR#TE" .AND. SE5->E5_SITUACA <> "E"
							cPadrao := "562"
						ElseIf !SE5->E5_TIPODOC $ "TR#TE" .AND. SE5->E5_RECPAG = "P"
							cPadrao := "565"
						EndIf
					Endif

					If SE5->E5_TIPO $ MVRECANT .AND. SE5->E5_RECPAG ="P" .AND. ALLTRIM(SE5->E5_LA) <> "S"
						lRACont := .T.
						cPadrao := '520'
					Endif 

					//se for ITF utiliza lancamento especifico
					If lFindITF .And. FinProcITF( SE5->( Recno() ),2 )
						If SE5->E5_SITUACA == 'E' .Or. SE5->E5_SITUACA == 'C'
							cPadrao := "56B"
						Else
							cPadrao := "56A"
						EndIf
					EndIf
					//Tranferencias

					//N�o contabiliza movimentos da carteira a receber caso esteja contabilizando apenas a carteira a pagar.
					//N�o contabiliza movimentos da carteira a pagar caso esteja contabilizando apenas a carteira a receber
					If !(SE5->E5_TIPODOC $ "TR#TE")
						If (mv_par06 != 1 .and. mv_par06 !=4) .And. SE5->E5_RECPAG == 'R'
							(cAliasSE5)->( dbSkip() )

							If MV_PAR12 == MVP12PROCESSO .AND. (cAliasSE5)->(EOF()) .and. cTipoMov == "DIRETO"
								(cAliasSE5)->(dbGOTOP())
								cTipoMov := "TITULOS"
							EndIf	

							Loop
						ElseIf (mv_par06 != 2 .and. mv_par06 !=4) .AND. SE5->E5_RECPAG == 'P'
							(cAliasSE5)->( dbSkip() )

							If MV_PAR12 == MVP12PROCESSO .AND. (cAliasSE5)->(EOF()) .and. cTipoMov == "DIRETO"
								(cAliasSE5)->(dbGOTOP())
								cTipoMov := "TITULOS"
							EndIf	

							Loop
						EndIf						
											
						// Nao contabiliza transferencia para carteira descontada
						// Este sera feito pela Baixa do titulo
						If SE5->E5_TIPODOC $ "TR#TE" .and. !Empty(SE5->E5_NUMERO)
							(cAliasSE5)->( dbSkip() )
							Loop
						Endif
	    	        Endif

					// Nao contabiliza movimento bancario totalizador da baixa CNAB ou automatica
					// Este sera feito pela Baixa do titulo (LP530 ou LP532)
					If Empty(SE5->E5_TIPODOC) .and. !Empty(SE5->E5_LOTE)
						(cAliasSE5)->( dbSkip() )
						Loop
					Endif
					
					If SE5->E5_RECPAG == "P" .and. SE5->E5_TIPODOC $ "TR#TE" .and. MV_PAR06 == 4
						cPadrao := "560"
						AADD(aRecsTRF,{SE5->(RECNO()),"560",IIF(!Empty(SE5->E5_NUMCHEQ),SE5->E5_NUMCHEQ,SE5->E5_DOCUMEN)})
						(cAliasSE5)->(dbSkip())

						If MV_PAR12 == MVP12PROCESSO .AND. (cAliasSE5)->(EOF()) .and. cTipoMov == "DIRETO"
							(cAliasSE5)->(dbGOTOP())
							cTipoMov := "TITULOS"
						EndIf	

						Loop
					Elseif SE5->E5_RECPAG == "R" .and. SE5->E5_TIPODOC $ "TR#TE" .and. MV_PAR06 == 4
						cPadrao := "561"
						AADD(aRecsTRF,{SE5->(RECNO()),"561",IIF(!Empty(SE5->E5_NUMCHEQ),SE5->E5_NUMCHEQ,SE5->E5_DOCUMEN)})
						(cAliasSE5)->(dbSkip())

						If MV_PAR12 == MVP12PROCESSO .AND. (cAliasSE5)->(EOF()) .and. cTipoMov == "DIRETO"
							(cAliasSE5)->(dbGOTOP())
							cTipoMov := "TITULOS"
						EndIf	

						Loop
					EndIf

					// Contabiliza movimentacao bancaria de adiantamento - MV_CTMOVPA
					If !lCtMovPa .And. SE5->E5_RECPAG == "P" .And. SE5->E5_TIPO	$ MVPAGANT
						(cAliasSE5)->(dbSkip())
						Loop
					Endif
					
					// Nao contabiliza movimentacao bancaria de adiantamento
					// Este sera feito pelo SE1
					If SE5->E5_RECPAG == "R" .And. SE5->E5_TIPO	$ MVRECANT
						(cAliasSE5)->(dbSkip())
						Loop
					Endif

					//Aplicacoes e emprestimos
					lX			:= .F.
					SEH->(dbSetOrder(1))
					
					If SE5->E5_TIPODOC $ "AP/RF/PE/EP" .And. SEH->(MsSeek(xFilial("SEH")+SubStr(SE5->E5_DOCUMEN,1,8)))
						lX		:= .T.
						If SE5->E5_TIPODOC $ "AP/EP"
							If ( SE5->E5_TIPODOC=="AP" .And. SE5->E5_RECPAG=="P" ) .Or.;
								( SE5->E5_TIPODOC=="EP" .And. SE5->E5_RECPAG=="R" )
								cPadrao := "580"
							Else
								cPadrao := "581"
							EndIf
						Else
							If ( SE5->E5_TIPODOC=="RF" .And. SE5->E5_RECPAG=="R" ) .Or.;
								( SE5->E5_TIPODOC=="PE" .And. SE5->E5_RECPAG=="P" )
								cPadrao := "585"
							Else
								cPadrao := "586"
							EndIf
						EndIf
						//旼컴컴컴컴컴컴컴컴컴컴컴컴커
						//� SEH ja esta posicionado  �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴켸
						RecLock("SEH")
						SEH->EH_VALREG := 0
						SEH->EH_VALREG2:= 0
						SEH->EH_VALIRF := 0
						// Soh zera valor do IOF para buscar o movimento de IOF (EI_TIPODOC igual a "I2") nos casos de aplicacoes.
						If SEH->EH_APLEMP == "APL"
							SEH->EH_VALIOF := 0
						EndIf	
						SEH->EH_VALSWAP:= 0
						SEH->EH_VALISWP:= 0
						SEH->EH_VALOUTR:= 0
						SEH->EH_VALGAP := 0
						SEH->EH_VALCRED:= 0
						SEH->EH_VALJUR := 0
						SEH->EH_VALJUR2:= 0
						SEH->EH_VALVCLP:= 0
						SEH->EH_VALVCCP:= 0
						SEH->EH_VALVCJR:= 0
						SEH->EH_VALREG := 0
						SEH->EH_VALREG2:= 0
						MsUnlock()
						
						dbSelectArea("SEI")
						dbSetOrder(1)
						dbSeek(xFilial("SEI")+SEH->EH_APLEMP+ Alltrim(SE5->E5_DOCUMEN))
						
						If ( !VerPadrao("581") .And. cPadrao$"581#580" .And. SEI->EI_STATUS=="C" )
							(cAliasSE5)->(dbSkip())
							Loop
						EndIf
						If ( !VerPadrao("586") .And. cPadrao$"586#585" .And. SEI->EI_STATUS=="C" )
							(cAliasSE5)->(dbSkip())
							Loop
						EndIf
						
						aRecsSEI := {}
						While ( SEI->(EI_FILIAL+EI_APLEMP+EI_NUMERO+EI_REVISAO+EI_SEQ)==;
							xFilial("SEI")+SEH->EH_APLEMP+ Alltrim(SE5->E5_DOCUMEN) )
							RecLock("SEH")
							If SEI->EI_MOTBX == "APR"
								If SEI->EI_TIPODOC == "I1"
									SEH->EH_VALIRF := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "I2"
									SEH->EH_VALIOF := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "I3"
									SEH->EH_VALISWP:= Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "I4" 
									SEH->EH_VALOUTR:= Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "I5"
									SEH->EH_VALGAP := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "JR"
									SEH->EH_VALJUR := Abs(SEI->EI_VALOR)
									SEH->EH_VALJUR2:= Abs(SEI->EI_VLMOED2)
								EndIf
								If SEI->EI_TIPODOC == "V1"
									SEH->EH_VALVCLP := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "V2"
									SEH->EH_VALVCCP := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC == "V3"
									SEH->EH_VALVCJR := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC $ "I1/I2/I3/I4/I5/JR/V1/V2/V3"
									If lUsaFlag
										AAdd( aFlagCTB, {"EI_LA","S","SEI",SEI->(Recno()),0,0,0} )
										CTBAddFlag(aFlagCTB)
									Else
										AAdd( aRecsSEI, SEI->(Recno()) )
										AAdd( aRecsSE5, {SE5->(Recno()),'FINM030','FK5'} )
									EndIf
								EndIf
							EndIf
							SEH->(MsUnLock())
							dbSelectArea("SEI")
							dbSkip()
						EndDo
						If ( VerPadrao("582") )
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							EndIf
							nTotDoc	:= DetProva(nHdlPrv,"582","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
							If LanceiCtb // Vem do DetProva
								For nJ := 1 To Len( aRecsSEI )
									SEI->( dbGoTo( aRecsSEI[nJ] ) )
									RecLock("SEI")
									SEI->EI_LA := "S"
									SEI->( MsUnlock() )
								Next nJ	
							EndIf

							nTotProc += nTotDoc
							nTotal	+=	nTotDoc

							If MV_PAR12 == MVP12DOCUMENTO 
								IF nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
								Endif
								aFlagCTB := {}
							Endif
						EndIf
						
						dbSelectArea("SEH")
						dbSetOrder(1)
						MsSeek(xFilial("SEH")+SubStr(SE5->E5_DOCUMEN,1,8))
						RecLock("SEH")
						SEH->EH_VALREG := 0
						SEH->EH_VALREG2:= 0
						SEH->EH_VALIRF := 0
						// Soh zera valor do IOF para buscar o movimento de IOF (EI_TIPODOC igual a "I2") nos casos de aplicacoes.
						If SEH->EH_APLEMP == "APL"
							SEH->EH_VALIOF := 0
						EndIf
						SEH->EH_VALSWAP:= 0
						SEH->EH_VALISWP:= 0
						SEH->EH_VALOUTR:= 0
						SEH->EH_VALGAP := 0
						SEH->EH_VALCRED:= 0
						SEH->EH_VALJUR := 0
						SEH->EH_VALJUR2:= 0
						SEH->EH_VALVCLP:= 0
						SEH->EH_VALVCCP:= 0
						SEH->EH_VALVCJR:= 0
						SEH->EH_VALREG := 0
						SEH->EH_VALREG2:= 0
						MsUnlock()
						
						dbSelectArea("SEI")
						dbSetOrder(1)
						dbSeek(xFilial("SEI")+SEH->EH_APLEMP+SubStr(SE5->E5_DOCUMEN,1,10))
						
						aRecsSEI := {}
						While ( SEI->EI_FILIAL+SEI->EI_APLEMP+SEI->EI_NUMERO+SEI->EI_REVISAO+SEI->EI_SEQ==;
							xFilial("SEI")+SEH->EH_APLEMP+SubStr(SE5->E5_DOCUMEN,1,10) )
							RecLock("SEH")
							If SEI->EI_MOTBX == "NOR"
								If ( SEI->EI_TIPODOC == "RG" )
									SEH->EH_VALREG := Abs(SEI->EI_VALOR)
									SEH->EH_VALREG2:= Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "I1" )
									SEH->EH_VALIRF := Abs(SEI->EI_VALOR)	
								EndIf
								If ( SEI->EI_TIPODOC == "I2" )
									SEH->EH_VALIOF := Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "SW" )
									SEH->EH_VALSWAP:= Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "I3" )
									SEH->EH_VALISWP:= Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "I4" )
									SEH->EH_VALOUTR:= Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "I5" )
									SEH->EH_VALGAP := Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "VL" )
									SEH->EH_VALCRED:= Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "JR" )
									SEH->EH_VALJUR := Abs(SEI->EI_VALOR)
									SEH->EH_VALJUR2:= Abs(SEI->EI_VLMOED2)
								EndIf
								If ( SEI->EI_TIPODOC == "V1" )
									SEH->EH_VALVCLP := Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "V2" )
									SEH->EH_VALVCCP := Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "V3" )
									SEH->EH_VALVCJR := Abs(SEI->EI_VALOR)
								EndIf
								If ( SEI->EI_TIPODOC == "BL" )
									SEH->EH_VALREG := Abs(SEI->EI_VALOR)
									SEH->EH_VALREG2:= Abs(SEI->EI_VLMOED2)
								EndIf
								If ( SEI->EI_TIPODOC == "BC" )
									SEH->EH_VALREG := Abs(SEI->EI_VALOR)
									SEH->EH_VALREG2:= Abs(SEI->EI_VLMOED2)
								EndIf
								If ( SEI->EI_TIPODOC == "BJ" )
									SEH->EH_VALJUR := Abs(SEI->EI_VALOR)
									SEH->EH_VALJUR2:= Abs(SEI->EI_VLMOED2)
								EndIf
								If ( SEI->EI_TIPODOC == "BP" )
									VALOR := Abs(SEI->EI_VALOR)
								EndIf
								If SEI->EI_TIPODOC $ "I1/I2/I3/I4/I5/JR/V1/V2/V3"
									If lUsaFlag
										AAdd( aFlagCTB, {"EI_LA","S","SEI",SEI->(Recno()),0,0,0} )
									Else
										AAdd( aRecsSEI, SEI->(Recno()) )
									EndIf
								EndIf
							EndIf
							SEH->(MsUnLock())
							dbSelectArea("SEI")
							dbSkip()
						EndDo
					EndIf
					
					dbSelectArea("SE5")
					
					//-------------------------------------------------------------
					// Caracteriza-se lancamento os registros com :
					// E5_TIPODOC = brancos
					// E5_TIPODOC = "DB" // Receita Bancaria - FINA200
					// E5_TIPODOC = "OD" // Outras Despesas  - FINA200
					// E5_TIPODOC = "TR" // Transferencia Banc - FINA100
					// E5_TIPODOC = "TE" // Est. Transf Bancar - FINA100
					// E5_TIPODOC = "IT" // Movimento de ITF (Bolivia) - FINA100
					//-------------------------------------------------------------
					If !lX .and. !(SE5->E5_TIPODOC $ "DB/OD/PA/  /TR/TE/IT")
						(cAliasSE5)->(dbSkip())
						Loop
					Endif
					
					If SE5->E5_RATEIO == "S"
						If SE5->E5_RECPAG = "R"
							If SE5->E5_SITUACA == "E"
								cPadrao := "557"
							Else
							cPadrao := "517"
							EndIf					    
						Else
							If SE5->E5_SITUACA == "E"
								cPadrao := "558"
							Else
							cPadrao := "516"
							EndIf					    
						Endif
					EndIf

					If SE5->E5_TIPO $ MVPAGANT
						// Desposiciona SE2
						SE2->( DbGoTo(0) )
						cPadrao := "513"
					EndIf

					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Posiciona na natureza.										  �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					dbSelectArea( "SED" )
					dbSeek(xFilial()+SE5->E5_NATUREZ)
					
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Posiciona no banco.											  �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					dbSelectArea("SA6")
					dbSetOrder(1)
					dbSeek(xFilial("SA6")+SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA)
					
					dbSelectArea("SE5")
					lPadrao:=VerPadrao(cPadrao)
					
					If l370E5R .and. SE5->E5_RECPAG == "R"
						Execblock("F370E5R",.F.,.F.)
					Endif

					If l370E5P .and. SE5->E5_RECPAG == "P"
						Execblock("F370E5P",.F.,.F.)
					Endif
					
					IF lPadrao
						If SE5->E5_RATEIO != "S"
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							Endif
							If lUsaFlag
								CTBAddFlag(aFlagCTB)
							EndiF
							nTotDoc	:= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

							If cPadrao == "585" .And. LanceiCtb // Vem do DetProva
								For nJ := 1 To Len( aRecsSEI )
									SEI->( dbGoTo( aRecsSEI[nJ] ) )
									RecLock("SEI")
									SEI->EI_LA := "S"
									SEI->( MsUnlock() )
								Next nJ
							EndIf

							nTotProc	+= nTotDoc
							nTotal		+=	nTotDoc
							
							If MV_PAR12 == MVP12DOCUMENTO 
								IF nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
								Endif
								aFlagCTB := {}
							Endif
							
							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Atualiza Flag de Lan놹mento Cont쟟il		  �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If LanceiCtb
								AAdd(aRecsSE5,{ SE5->(Recno()),'FINM030','FK5'}) // Mov.Bancaria
							EndIf
						Else
							
							If nHdlPrv <= 0
								a370Cabecalho(@nHdlPrv,@cArquivo)
							EndIf
							RegToMemory("SE5",.F.,.F.)
							If lUsaFlag
								CTBAddFlag(aFlagCTB)
							EndIf
							// Devido a estrutura do programa, o rateio ja eh "quebrado"
							// por documento.                                                          
							F370RatFin(cPadrao,"FINA370",cLote,4," ",4,,,,@nHdlPrv,@nTotDoc,@aFlagCTB)
							lCabecalho := If(nHdlPrv <= 0, lCabecalho, .T.)
							nTotProc	+= nTotDoc
							nTotal		+=	nTotDoc
							
							If MV_PAR12 == MVP12DOCUMENTO 
								If nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
								Endif
								aFlagCTB := {}
							Endif

							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Atualiza Flag de Lan놹mento Cont쟟il	   �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If LanceiCtb
								aAdd(aRecsSE5,{ SE5->(Recno()),'FINM030','FK5'}) // Mov.Bancaria
							EndIf
							
						Endif
					EndIf

				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//� Baixas a Receber 														  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				ElseIf SE5->E5_RECPAG == "R" .and. (mv_par06 == 1 .or. mv_par06 == 4)				    
		   			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
					//쿎ontabilizacao de Recebimentos Diversos - Tabela SEL�
					//쿐xecutada atraves das movimentacoes no SE5          �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
					If !Empty( SE5->(E5_ORDREC + E5_SERREC) )
							
						// Paises localizados executam a rotina FINA371
						// que contabiliza tambem Ordem de Pago.
						If cPaisLoc == 'BRA' .and.;
							( SE5->E5_SERREC + SE5->E5_ORDREC ) <> ( cSELSerie + cSELRecibo )

							cSELSerie	:= SE5->E5_SERREC
							cSELRecibo	:= SE5->E5_ORDREC
					
							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
							//쿎ontrole de contabilizacao.              �
							//쿛ercorre e contabiliza todos os registros�
							//쿭e um recibo.                            �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
							F370CTBSEL(	cSELSerie,;
										cSELRecibo,;
										@nTotDoc,;
										cLote,;
										@nHdlPrv,;
										@cArquivo,;
										lUsaFlag,;
										@aFlagCTB,;
										@aCT5;
									)

							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//쿞epara por ?                                      �
							//쿌cumuladores para a geracao do Documento Contabil.�
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If MV_PAR12 == MVP12PERIODO .and. nTotDoc > 0		// Por Periodo
								nTotal += nTotDoc
							ElseIf MV_PAR12 == MVP12DOCUMENTO 
								If nTotDoc > 0	// Por Documento
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
									nTotDoc := 0 // Inicializa Variavel
								Endif
								aFlagCTB := {}
							ElseIf MV_PAR12 == MVP12PROCESSO					// Por Processo
								nTotProc += nTotDoc
							Endif
						EndIf
				   		If AllTrim(SE5->E5_LA) == "S"
							(cAliasSE5)->(dbSkip()) // Nestes casos a movimentacao nao eh contabilizada pelo SE5
							Loop
						EndIf
					Endif
					// 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴					
				
					lAdiant := (SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG)
					lEstorno := (SE5->E5_TIPODOC == "ES")
					lEstRaNcc := (SE5->E5_TIPODOC == "ES" .and. SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG)
					lCompens := (SE5->E5_TIPODOC == "BA" .and. SE5->E5_MOTBX == "CMP")
					lEstCompens := (SE5->E5_TIPODOC == "ES" .and. SE5->E5_MOTBX == "CMP")

					VALOR := 0
					FO1VADI := 0
					aDadosFO1 := {}
   	
					// Registros gerados pelo SIGALOJA - Pgto Dinheiro 
					// Contabilizar apenas quando E5_TIPODOC = 'VL' e E5_MOTBX == 'NOR'
					If SE5->E5_TIPODOC == 'BA' .AND. SE5->E5_MOTBX == 'LOJ' .AND. (SE5->E5_TIPO == cMvSimb1 .OR. SE5->E5_MOEDA == cMvSimb1)
						AADD(aRecsSE5, {SE5->(RECNO()), 'FINM010' ,'FK1'} )
						(cAliasSE5)->(dbSkip())
						Loop
					EndIf
					
					// Despreza inclusao de RA que sera contabilizado pelo SE1
					If SE5->E5_TIPODOC == "RA" .and. SE5->E5_TIPO $ MVRECANT
						aAdd(aRecsSE5, {SE5->(RECNO()), 'FINM010' ,'FK1'} )
						(cAliasSE5)->(dbSkip())
						Loop
					Endif

					// Despreza baixas por compensacao do titulo principal, para nao duplicar.
					If SE5->E5_TIPODOC == "CP" .and. SE5->E5_MOTBX == "CMP"
					    lCompens := .T.
					    aAdd(aRecsSE5, {SE5->(RECNO()), 'FINM010', 'FK1'} ) //Baixas a Receber.
					    (cAliasSE5)->(dbSkip())
						 Loop
					Endif
										
  					// Despreza estorno de compensacao do titulo de antecipa豫o, para nao duplicar.
					If SE5->E5_TIPODOC == "ES" .and. SE5->E5_MOTBX == "CMP" .and. (SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG)
						aAdd(aRecsSE5, {SE5->(RECNO()), 'FINM020', 'FK2'} ) //Baixas a Pagar.
						(cAliasSE5)->(dbSkip())
						Loop
					Endif

					// Despreza movimento totalizador de Baixa Autom�tica por Lote - (MV_BXCNAB = 'S')
					If EMPTY(SE5->(E5_PREFIXO+E5_NUMERO+E5_TIPO)) .AND. !EMPTY(E5_LOTE) .AND. E5_TIPODOC == 'VL'
						(cAliasSE5)->(dbSkip())
						LOOP
					EndIF

					If (lAdiant .or. lEstorno) .and. !lEstRaNcc
						dbSelectArea("SE2")
						SE2->(dbSetOrder(1))
						If !SE2->(MsSeek(xFilial("SE2")+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIFOR+SE5->E5_LOJA))
							If SE5->E5_MOTBX == "CMP" .and. !SE2->(MsSeek(SE5->E5_FILORIG+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIFOR+SE5->E5_LOJA))
								// Localizada inconsist�ncia no arquivo SE5. A fun豫o fa370conc
								// pergunta se o usu�rio quer continuar ou abandonar.
								If !__lSchedule .And. !FA370CONC()  .And. !lMultiThr
									Return .F.
								Endif
								dbSelectArea(cAliasSE5)
								(cAliasSE5)->(dbSkip())
								Loop
							Endif
						EndIf
					Else
						dbSelectArea( "SE1" )
						SE1->(dbSetOrder(2))
						
						cFilorig := xFilial("SE1")
						If !SE1->(MsSeek(cFilOrig +SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
							If !Empty(SE5->E5_FILORIG)
								cFilOrig := SE5->E5_FILORIG
							Endif

							If !SE1->(MsSeek(cFilOrig +SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
								// Localizada inconsist�ncia no arquivo SE5. A fun豫o fa370conc
								// pergunta se o usu�rio quer continuar ou abandonar.
								If !lCompens .and. !(cChE5Comp == cFilOrig +SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO .and. SE5->E5_LA = cLa)	
									If !__lSchedule .And. !FA370CONC()  .And. !lMultiThr
										Return .F.
									Endif
								EndIf
								dbSelectArea(cAliasSE5)
								(cAliasSE5)->(dbSkip())
								Loop
							Endif
						Endif

						//Carregar vari�vel FO1VADI
						If Alltrim(SE5->E5_MOTBX) == "LIQ"
							cNumLiq := Alltrim(SE5->E5_DOCUMEN)
							If !Empty(SE5->E5_IDORIG)
								dbSelectArea("FO0")
								FO0->(dbSetOrder(2))
								FO0->(dbSeek(xFilial("FO0") + cNumLiq + SE5->E5_CLIFOR + SE5->E5_LOJA))
								aDadosFO1 := F460AbFO1(FO0->FO0_PROCES,FO0->FO0_VERSAO,SE5->E5_IDORIG)
								If Len(aDadosFO1) > 0
									FO1VADI := aDadosFO1[1,2]
								EndIf
							EndIf
						EndIf
						
						// Carrega variaveis para contabilizacao dos
						// abatimentos (impostos da lei 10925).
						dbSelectArea("__SE1")
						__SE1->(dbSetOrder(1))
						__SE1->(MsSeek(cFilOrig +SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA)))
						While __SE1->(!EOF()) .And.;
								__SE1->(E1_FILIAL + E1_PREFIXO + E1_NUM + E1_PARCELA) ==;
								(cFilOrig + SE5->(E5_PREFIXO + E5_NUMERO + E5_PARCELA))
							If __SE1->E1_TIPO == MVPIABT
								VALOR5 := __SE1->E1_VALOR			
							ElseIf __SE1->E1_TIPO == MVCFABT
								VALOR6 := __SE1->E1_VALOR
							ElseIf __SE1->E1_TIPO == MVCSABT
								VALOR7 := __SE1->E1_VALOR						
							Endif			
							__SE1->(dbSkip())
						Enddo

					Endif
					
					nPis:=0
					nCofins:=0
					nCsll:=0
					nVretPis:=0
					nVretCof:=0
					nVretCsl:=0
					VARIACAO := 0
	
					If (lAdiant .or. lEstorno) .and. !lEstRaNcc
						nValLiq	:= SE2->E2_VALLIQ
						nDescont := SE2->E2_DESCONT
						nJuros	:= SE2->E2_JUROS
						nMulta	:= SE2->E2_MULTA
						nCorrec	:= SE2->E2_CORREC
						If lPccBaixa
							nPis		:= SE2->E2_PIS
							nCofins	:= SE2->E2_COFINS
							nCsll		:= SE2->E2_CSLL
							nVretPis := SE2->E2_VRETPIS
							nVretCof := SE2->E2_VRETCOF
							nVretCsl := SE2->E2_VRETCSL
						Endif
					Else
						nValLiq	:= SE1->E1_VALLIQ
						nDescont := SE1->E1_DESCONT
						nJuros	:= SE1->E1_JUROS
						nMulta	:= SE1->E1_MULTA
						nCorrec	:= SE1->E1_CORREC
						cSitOri  := SE1->E1_SITUACA
					Endif
					
					dbSelectArea( "SE5" )
					nVl:=nDc:=nJr:=nMt:=VARIACAO:=0
					lTitulo := .F.
					cSeq	  :=	SE5->E5_SEQ
					cBanco  := " "
					nRegSE5 := 0
					nRegOrigSE5 := 0
					nPisBx := 0
					nCofBx := 0
					nCslBx := 0

					dbSelectArea("SE5")

					If lTitulo := (SE5->E5_TIPODOC $ "BA|VL|V2|ES|LJ|CP")
						
						cBanco	:= SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA
						nRegSE5	:= SE5->(Recno())
						lMultnat:= SE5->E5_MULTNAT == "1"
						cSeqSE5	:= SE5->E5_SEQ
						nVl		:= E5_VALOR
						nDc 	:= SE5->E5_VLDESCO
						nJr		:= SE5->E5_VLJUROS
						nMt		:= SE5->E5_VLMULTA
						VARIACAO := SE5->E5_VLCORRE
						cSitCob	:= " "
						
						If !Empty(SE5->E5_SITCOB)
							cSitCob := SE5->E5_SITCOB
						Endif

						// Carrega as variaveis VALOR da Compensacao CR
						If lCompens
							aAreaATU := SE5->(GetArea())
							SE5->(DbSetOrder(2)) //E5_FILIAL+E5_TIPODOC+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_DATA+E5_CLIFOR+E5_LOJA+E5_SEQ
							If SE5->(MsSeek(SE5->(E5_FILIAL+"CP"+PadR(E5_DOCUMEN,nTamDoc)+DTOS(E5_DATA)+E5_FORNADT+E5_LOJAADT+E5_SEQ))) .And. SE5->E5_SITUACA != 'C'
								
								// Disposi豫o dos valores nas vari�veis difere da contabiliza豫o online
								// http://tdn.totvs.com/display/PROT/FIN0003_LPAD_Variaveis_de_contabilizacao_da_compensacao_CR
								VALOR  := SE5->E5_VALOR
								VALOR2 := SE5->E5_VRETISS
								VALOR3 := SE5->E5_VRETINS
								VALOR4 := SE5->E5_VRETIRF
								VALOR5 := SE5->E5_VRETPIS
								VALOR6 := SE5->E5_VRETCOF
								VALOR7 := SE5->E5_VRETCSL
								VALOR8 := SE5->E5_VLACRES
								VALOR9 := SE5->E5_VLDESCO
							EndIf
							RestArea(aAreaATU)
						EndIf

						If lPccBaixa
							IF Empty(SE5->E5_PRETPIS)
								nPisBx := SE5->E5_VRETPIS
								nCofBx := SE5->E5_VRETCOF
								nCslBx := SE5->E5_VRETCSL
							Endif
						Endif

					Endif
					
					IF lTitulo
						If lAdiant .and. !lEstCompens
							cPadrao := "530"
						ElseIf lEstCompens  //Estorno Compensacao Pagar
							cPadrao := "589"	
						ElseIf lEstRaNcc
							cPadrao := '527'
						Elseif lEstorno
							cPadrao := "531"
						ElseIf lCompens
							cPadrao := "596"
							VARIACAO := __SE1->E1_CORREC
						Else
							dbSelectArea( "SE1" )
							If cPaisLoc == "CHI" .And. SE5->E5_MOTBX == "DEV"
								cPadrao := "574"
							Else
								cPadrao := fa070Pad()
							EndIf
						Endif

						lPadrao 	:= VerPadrao(cPadrao)
						lPadraoCc 	:= VerPadrao("536") //Rateio por C.Custo de MultiNat C.Receber
						lPadraoCcE 	:= VerPadrao("539") //Estorno do rateio C.Custo de MultiMat CR
						
						IF lPadrao 
							If SE5->E5_TABORI == "FK1"
								AAdd( aRecsSE5, {SE5->(Recno()),'FINM010','FK1'} ) //baixa a receber
							Else
								AAdd( aRecsSE5, {SE5->(Recno()),'FINM020','FK2'} ) //  estorno de Baixas a pagar
							Endif

							If lUsaFlag
								CTBAddFlag(aFlagCTB)
							EndIf

							IF (lAdiant .or. lEstorno) .and. !lEstRaNcc
								// Protecao provisoria: lTitulo .T. sem SE2
								IF SE2->(!EOF()) .AND. SE2->(!BOF())
									Reclock( "SE2" )
									Replace E2_VALLIQ  With nVl
									Replace E2_DESCONT With nDc
									Replace E2_JUROS	 With nJr
									Replace E2_MULTA	 With nMt
									Replace E2_CORREC  With VARIACAO
									IF lPccBaixa
										Replace E2_PIS		With nPisBx
										Replace E2_COFINS	With nCofBx
										Replace E2_CSLL	With nCslBx
										Replace E2_VRETPIS	With nPisBx
										Replace E2_VRETCOF	With nCofBx
										Replace E2_VRETCSL	With nCslBx
									ENDIF
									SE2->(MsUnlock())
								ENDIF
							ELSE
								// Protecao provisoria: lTitulo .T. sem SE1
								IF SE1->(!EOF()) .AND. SE1->(!BOF())
									Reclock( "SE1" )
									Replace E1_VALLIQ  With nVl
									Replace E1_DESCONT With nDc
									Replace E1_JUROS   With nJr
									Replace E1_MULTA   With nMt
									Replace E1_CORREC  With VARIACAO
									If !Empty(cSitCob)
										Replace E1_SITUACA With cSitCob
									Endif
									SE1->( MsUnlock())
								ENDIF
							ENDIF

							// Posiciona no cliente/fornecedor e Natureza
							If (lAdiant .or. lEstorno) .and. !lEstRaNcc
								SA2->(dbSeek(xFilial()+SE2->E2_FORNECE+SE2->E2_LOJA))
								SED->(dbSeek( xFilial()+SE2->E2_NATUREZ ))
							Else
								SA1->(dbSeek(cFilial+SE1->E1_CLIENTE+SE1->E1_LOJA))
								SED->(dbSeek(cFilial+SE1->E1_NATUREZ))
							Endif

							// Posiciona no banco
							SA6->(dbSetOrder(1))
							SA6->(dbSeek(xFilial("SA6")+cBanco))
						
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							Endif

							//Contabilizando estorno de C.Pagar
							If lEstorno
								cChaveSev := RetChaveSev("SE2")+"2"+cSeqSE5
								cChaveSez := RetChaveSev("SE2",,"SEZ")
							Else
								cChaveSev := RetChaveSev("SE1")+"2"+cSeqSE5
								cChaveSez := RetChaveSev("SE1",,"SEZ")
							Endif
							
							DbSelectArea("SEV")
							SEV->(dbSetOrder(2))
							// Se utiliza multiplas naturezas, contabiliza pelo SEV
							If  lMultNat .And. MsSeek(cChaveSev)
								dbSelectArea("SE1")
								nRecSe1 := SE1->(Recno())
								SE1->(DbGoBottom())
								SE1->(DbSkip())
								dbSelectArea("SE2")
								nRecSe2 := SE2->(Recno())
								SE2->(DbGoBottom())
								SE2->(DbSkip())
								
								DbSelectArea("SEV")
								
								While xFilial("SEV")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIFOR+;
									EV_LOJA+EV_IDENT+EV_SEQ) == cChaveSev .And. !SEV->(Eof())
									
									//Se estou contabilizando um estorno, trata-se de um C. Pagar,
									//So vou contabilizar os EV_SITUACA == E
									If (lEstorno .and. !(SEV->EV_SITUACA == "E")) .or. ;
										(!lEstorno .and. (SEV->EV_SITUACA == "E"))
										//Se nao for um estorno, nao devo contabilizar o registro se
										//EV_SITUACA == E
										SEV->(dbSkip())
										Loop
									ElseIf lEstorno
										//O lancamento a ser considerado passa a ser o do estorno
										lPadraoCC := lPadraoCCE
									Endif
									
									If SEV->EV_LA != "S"
										dbSelectArea( "SED" )
										SED->(MsSeek(xFilial("SED")+SEV->EV_NATUREZ)) // Posiciona na natureza, pois a conta pode estar la.
										dbSelectArea("SEV")
										If SEV->EV_RATEICC == "1" .and. lPadraoCC .and. lPadrao // Rateou multinat por c.custo
											dbSelectArea("SEZ")
											SEZ->(dbSetOrder(4))
											SEZ->(MsSeek(cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5)) // Posiciona no arquivo de Rateio C.Custo da MultiNat
											While SEZ->(!Eof()) .and. xFilial("SEZ")+SEZ->(EZ_PREFIXO+EZ_NUM+;
												EZ_PARCELA+EZ_TIPO+EZ_CLIFOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT+EZ_SEQ) == cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5
												
												//Se estou contabilizando um estorno, trata-se de um C. Pagar,
												//So vou contabilizar os EZ_SITUACA == E
												//Se nao for um estorno, nao devo contabilizar o registro se
												//EZ_SITUACA == E
												If (lEstorno .and. !(SEZ->EZ_SITUACA == "E")) .or. ;
													(!lEstorno .and. (SEZ->EZ_SITUACA == "E"))
													SEZ->(dbSkip())
													Loop
												Endif
												If SEZ->EZ_LA != "S"
		
													aAdd(aFlagCTB,{"EZ_LA","S","SEZ",SEZ->(Recno()),0,0,0})
													//O lacto padrao fica:
													//536 - Rateio multinat com c.custo C.Receber
													//539 - Estorno de Rat. Multinat C.Custo C.Pagar
													cPadraoCC := If(SEZ->EZ_SITUACA == "E","539","536")
													VALOR := SEZ->EZ_VALOR
													nTotDoc	+=	DetProva(nHdlPrv,cPadraoCC,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
	
													If LanceiCtb // Vem do DetProva
														If !lUsaFlag
															RecLock("SEZ")
															SEZ->EZ_LA := "S"
															SEZ->(MsUnlock())
														EndIf
													ElseIf lUsaFlag
														If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEZ->(Recno()) }))>0
															aFlagCTB := Adel(aFlagCTB,nPosReg)
															aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
														Endif
													Endif
	
												Endif
												SEZ->(dbSkip())
											Enddo
											DbSelectArea("SEV")
										Else
											If lUsaFlag
												aAdd(aFlagCTB,{"EV_LA","S","SEV",SEV->(Recno()),0,0,0})
											EndIf
				  						    nTotDoc	:=	DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
										Endif
										
										If LanceiCtb // Vem do DetProva
											If !lUsaFlag
												RecLock("SEV")
												SEV->EV_LA := "S"
												SEV->(MsUnlock())
											EndIf
										ElseIf lUsaFlag
											If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEV->(Recno()) }))>0
												aFlagCTB := Adel(aFlagCTB,nPosReg)
												aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
											Endif
										Endif
									Endif
									DbSelectArea("SEV")
									SEV->(DbSkip())
									VALOR := 0
								Enddo
								nTotProc +=	nTotDoc // Totaliza por processo
								nTotal +=	nTotDoc
								
								If MV_PAR12 == MVP12DOCUMENTO 
									If nTotDoc > 0 // Por documento
										Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
										nTotDoc := 0
									Endif
									aFlagCTB := {}
								Endif
						
								dbSelectArea("SE2")
								SE2->(DbGoto(nRecSe2))
								
								dbSelectArea("SE1")
								SE1->(DbGoto(nRecSe1))
							Else
								dbSelectArea("SEV")
								SEV->(DbGoBottom())
								SEV->(DbSkip())
								DbSelectArea("SE1")
								If SE1->E1_TIPO == MVPIABT
									VALOR5 := SE1->E1_VALOR			
								ElseIf SE1->E1_TIPO == MVCFABT
									VALOR6 := SE1->E1_VALOR
								ElseIf SE1->E1_TIPO == MVCSABT
									VALOR7 := SE1->E1_VALOR			
								ElseIf SE1->E1_SITUACA $ '1|H'	// Situa豫o de Cobran�a: Cobran�a Simples
									VALOR := SE1->E1_VALOR
								Endif	
								
								If lCompens
									STRLCTPAD := SE5->E5_DOCUMEN
								EndIf

								nTotDoc	:=	DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
								nTotProc	+= nTotDoc
								nTotal		+=	nTotDoc
								
								If MV_PAR12 == MVP12DOCUMENTO 
									If nTotDoc > 0 // Por documento
										Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
									Endif
									aFlagCTB := {}
								Endif
							Endif
						Endif

						VALOR2 := 0
						VALOR3 := 0
						VALOR4 := 0
						VALOR5 := 0
						VALOR6 := 0
						VALOR7 := 0
						VALOR8 := 0
						VALOR9 := 0
						REGVALOR := 0
						
						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
						//� Devolve a posi뇙o original do arquivo  �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
						If nRegOrigSE5 > 0
							SE5->(dbGoTo(nRegOrigSE5))
						Endif
						If !lAdiant .and. !lEstorno
							dbSelectArea("SE1")
							If !SE1->(Eof()) .And. !SE1->(Bof())
								Reclock( "SE1" )
								Replace SE1->E1_VALLIQ With nValliq
								Replace SE1->E1_DESCONT With nDescont
								Replace SE1->E1_JUROS With nJuros
								Replace SE1->E1_MULTA With nMulta
								Replace SE1->E1_CORREC With nCorrec
								Replace SE1->E1_SITUACA With cSitOri
								SE1->(MsUnlock())
							EndIF
						Else
							dbSelectArea("SE2")
							If !SE2->(Eof()) .And. !SE2->(Bof())
								Reclock( "SE2" )
								Replace SE2->E2_VALLIQ With nValliq
								Replace SE2->E2_DESCONT With nDescont
								Replace SE2->E2_JUROS With nJuros
								Replace SE2->E2_MULTA With nMulta
								Replace SE2->E2_CORREC With nCorrec
								If lPccBaixa
									Replace SE2->E2_PIS With nPis
									Replace SE2->E2_COFINS With nCofins
									Replace SE2->E2_CSLL With nCsll
									Replace SE2->E2_VRETPIS With nVretPis
									Replace SE2->E2_VRETCOF With nVretCof
									Replace SE2->E2_VRETCSL With nVretCsl
								Endif
								SE2->(MsUnlock())
							EndIf
						Endif
					Endif

				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//� Baixas a Pagar															  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				ElseIf SE5->E5_RECPAG == "P" .and.  (mv_par06 == 2 .or. mv_par06 == 4)

					dbSelectArea( "SE5" )
					VALOR 		:= 0
					VALOR2		:= 0
					VALOR3		:= 0
					VALOR4		:= 0
					VALOR5		:= 0
					
					lAdiant := (SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG)
					lEstorno := (SE5->E5_TIPODOC == "ES")
					lEstPaNdf := (SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG .and. SE5->E5_TIPODOC == "ES")
					lEstCart2 := (SE5->E5_TIPODOC == "E2")
					lCompens := (SE5->E5_TIPODOC == "CP" .and. SE5->E5_MOTBX == "CMP")
					lEstCompens := (SE5->E5_TIPODOC == "ES" .and. SE5->E5_MOTBX == "CMP")

					BEGIN SEQUENCE
						// Despreza baixas do titulo de antecipa豫o, para nao duplicar.
						If SE5->E5_TIPODOC == "BA" .and. SE5->E5_MOTBX == "CMP"
							AAdd( aRecsSE5, {SE5->(Recno()),'FINM020','FK2'} ) // Baixas a pagar.
							BREAK
						Endif
							
						// Despreza inclusao de PA que sera contabilizado pelo SE2
						If SE5->E5_TIPODOC == "PA" .and. SE5->E5_TIPO $ MVPAGANT
							BREAK
						Endif

						//Nao ser�o contabilizadas os movimentos de troco de vendas do sigaloja pela SE5. H� um trecho espec�fico para contabiliza豫o dos mesmos pela FK5.
						If SE5->E5_TIPODOC $ "VL#TR" .AND. !Empty(SE5->E5_NUMERO) .and. SE5->E5_MOEDA == "TC"
							If Upper(AllTrim(SE5->E5_NATUREZ)) $ Upper(cNatTroc) //"Troco"  
								BREAK
							EndIf
						EndIf

						// Despreza estorno de compensacao do titulo principal, para nao duplicar.
						If SE5->E5_TIPODOC == "ES" .and. SE5->E5_MOTBX == "CMP" .and. !(SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG)
							AAdd( aRecsSE5, {SE5->(Recno()),'FINM010','FK1'} ) // Baixas a receber.
							BREAK
						Endif

						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
						//� Apenas contabilza se :                                       �
						//� For realmente uma baixa de contas a PAGAR                    �
						//� mv_ctbaixa diferente de "C" - Cheque                         |
						//� Ou se for igual a C e for uma baixa no banco caixa			  |
						//�______________________________________________________________|

						If !lAdiant .and. !lEstorno .and. !lEstCart2
							If cCtBaixa == "C" .And. SE5->E5_MOTBX == "NOR" .And.;
								If(cPaisLoc == 'BRA', Empty(SE5->E5_AGLIMP), .T.) .And.;
								!(Substr(SE5->E5_BANCO,1,2)=="CX" .or. SE5->E5_BANCO$cCarteira)
								dbSelectArea(cAliasSE5)

								// Verificando a continua豫o do bordero
								If !Empty(cNumBor) 
									(cAliasSE5)->(dbSkip())
									lAvancaReg := .F.

									If (cAliasSE5)->(!Eof()) .AND. (&cCondWhile) 
										cProxBor := ALLTRIM((cAliasSE5)->E5_DOCUMEN)
									else
										cProxBor := " "
									EndIf
								EndIf
								BREAK
							Endif
						Endif
							
						// A baixa de adiantamento ou estorno de baixa a receber gera registro a pagar
							
						If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
							dbSelectArea("SE1")
							SE1->(dbSetOrder(2))
							If !SE1->(MsSeek(xFilial("SE1")+SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
								If SE5->E5_MOTBX == "CMP" .and. !SE1->(MsSeek(SE5->E5_FILORIG+SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
									//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
									//� Localizada inconsist�ncia no arquivo SE5. A fun豫o fa370conc	 �
									//� pergunta se o usu�rio quer continuar ou abandonar.				 �
									//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
									If (!lEstCompens ) // verifica se a compensa豫o do Adiantamento foi contabilizado RECPAG= R e TIPODOC= BA 
										If !__lSchedule .And. !FA370CONC()  .And. !lMultiThr
											Return .F.
										Endif
									Else
										cChE5Comp := SE5->E5_FILORIG+SE5->E5_CLIFOR+SE5->E5_LOJA+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO
										cLA := SE5->E5_LA
									EndIf
									dbSelectArea(cAliasSE5)
									BREAK
								EndIf
							Endif
						Else
							dbSelectArea("SE2")
							SE2->(dbSetOrder(1))
							cFilorig := xFilial("SE2")
							If !SE2->(MsSeek(cFilOrig +SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIFOR+SE5->E5_LOJA))
								If !Empty(SE5->E5_FILORIG)
									cFilOrig := SE5->E5_FILORIG   
								Else
									cFilOrig := SE5->E5_FILIAL		
								Endif

								If !SE2->(MsSeek(cFilOrig +SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIFOR+SE5->E5_LOJA))
									//Se for o totalizador da baixa automatica, nao pode ser gerada mensagem
									If lBxCnab .and. AllTrim(SE5->E5_ORIGEM) $ "FINA090|FINA091|FINA300|FINA430|FINA740|FINA750" .AND. SE5->E5_TIPODOC == "VL" .AND. !Empty(SE5->E5_LOTE) .AND. Empty(SE5->E5_TIPO + SE5->E5_DOCUMEN + SE5->E5_NUMERO + SE5->E5_PARCELA + SE5->E5_CLIFOR + SE5->E5_LOJA)
										dbSelectArea(cAliasSE5)
										BREAK
									EndIf
									
									//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
									//� Localizada inconsist�ncia no arquivo SE5. A fun豫o fa370conc	 �
									//� pergunta se o usu�rio quer continuar ou abandonar.				 �
									//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
									If !__lSchedule .And. !FA370CONC()  .And. !lMultiThr
										Return .F.
									Endif
									dbSelectArea(cAliasSE5)
									BREAK
								Endif
							Endif
							
							
							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Nao contabiliza titulos de impostos aglutinados com origem na rotina FINA378 �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If !lAGP .and. !lMvAGP  // Neste caso o Par�metro de contabiliza豫o dos impostos aglutinados inverte a opera豫o
								If AllTrim( Upper( SE2->E2_ORIGEM ) ) == "FINA378" .And. SE2->E2_PREFIXO == "AGP"	// Aglutinacao Pis/Cofins/Csll
									BREAK
								Endif
							ElseIf AllTrim(SE2->E2_CODRET) == "5952" .And.;
								((SE2->E2_PREFIXO != "AGP" .And. !Empty(SE2->E2_TITPAI)) .And. AllTrim(SE2->E2_ORIGEM) != "FINA378") .And. (lF370ChkAgp .And. F370ChkAgp())
								RecLock("SE5", .F.)
								SE5->E5_LA := 'S'
								SE5->(MsUnLock())
								BREAK
							EndIf					
						Endif
							
						nPis:=0
						nCofins:=0
						nCsll:=0
						nVretPis:=0
						nVretCof:=0
						nVretCsl:=0
						
						If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
							nValLiq := SE1->E1_VALLIQ
							nDescont := SE1->E1_DESCONT
							nJuros := SE1->E1_JUROS
							nMulta := SE1->E1_MULTA
							nCorrec := SE1->E1_CORREC
						Else
							nValLiq := SE2->E2_VALLIQ
							nDescont := SE2->E2_DESCONT
							nJuros := SE2->E2_JUROS
							nMulta := SE2->E2_MULTA
							nCorrec := SE2->E2_CORREC
							If lPccBaixa
								nPis := SE2->E2_PIS
								nCofins := SE2->E2_COFINS
								nCsll := SE2->E2_CSLL
								nVretPis := SE2->E2_VRETPIS
								nVretCof := SE2->E2_VRETCOF
								nVretCsl := SE2->E2_VRETCSL
							Endif
						Endif
						
						dbSelectArea( "SE5" )
						nVl:=nDc:=nJr:=nMt:=VARIACAO:=0
						lTitulo := .F.
						cSeq := SE5->E5_SEQ
						cBanco := " "
						nRegSE5 := 0
						nRegOrigSE5 := 0
						
						STRLCTPAD := SE5->E5_DOCUMEN
						If SE5->(FieldPos("E5_FORNADT")) > 0
							CODFORCP := SE5->E5_FORNADT
							LOJFORCP := SE5->E5_LOJAADT
						EndIf
			
						nPisBx := 0
						nCofBx := 0
						nCslBx := 0
						
						If lTitulo := (SE5->E5_TIPODOC $ "BA/VL/V2/ES/LJ/E2/CP")
							cBanco	 := SE5->E5_BANCO+SE5->E5_AGENCIA+SE5->E5_CONTA
							cCheque	 := SE5->E5_NUMCHEQ          
							nRegSE5	 := SE5->(Recno())
							lMultnat := SE5->E5_MULTNAT == "1"
							cSeqSE5	 := SE5->E5_SEQ
							nVl		 := SE5->E5_VALOR
							nDc 	 := SE5->E5_VLDESCO
							nJr		 := SE5->E5_VLJUROS
							nMt		 := SE5->E5_VLMULTA
							VARIACAO := SE5->E5_VLCORRE
							
							If lCompens
								aAreaATU := SE5->(GetArea())
								SE5->(DbSetOrder(2)) //E5_FILIAL+E5_TIPODOC+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_DATA+E5_CLIFOR+E5_LOJA+E5_SEQ
								If SE5->(MsSeek(SE5->(E5_FILIAL+"BA"+PadR(E5_DOCUMEN,nTamDoc)+DTOS(E5_DATA)+E5_FORNADT+E5_LOJAADT+E5_SEQ))) .And. SE5->E5_SITUACA != 'C'
										
									aAreaSE2 := SE2->(GetArea())
									SE2->(DBSetOrder(1))	// "E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA"
									SE2->(DBSEEK(XFILIAL('SE2',SE5->E5_FILORIG)+SE5->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA)))
									nVlDecresc := SE2->E2_DECRESC
									nVlAcresc  := SE2->E2_ACRESC
									REGVALOR := SE2->(Recno()) // Variavel para usuario reposicionar o registro do SE2
									RestArea(aAreaSE2)
		
									// Disposi豫o dos valores nas vari�veis difere da contabiliza豫o online
									// http://tdn.totvs.com/display/PROT/FIN0020_LPAD_Variaveis_de_contabilizacao_da_compensacao_CP
									VALOR  	:= SE5->E5_VALOR
									VALOR2 	:= nVlAcresc
									VALOR3 	:= nVlDecresc
									VALOR4 	:= SE5->E5_VLCORRE
									VARIACAO := SE5->E5_VLCORRE
								EndIf
								RestArea(aAreaATU)
							EndIf						

							If lPccBaixa
								IF Empty(SE5->E5_PRETPIS)
									nPisBx := SE5->E5_VRETPIS
									nCofBx := SE5->E5_VRETCOF
									nCslBx := SE5->E5_VRETCSL
								Endif
							Endif

							// Verificando a continua豫o do bordero
							If !Empty(cNumBor) 
								(cAliasSE5)->(dbSkip())
								lAvancaReg := .F.

								If (cAliasSE5)->(!Eof()) .AND. (&cCondWhile) 
									cProxBor := ALLTRIM((cAliasSE5)->E5_DOCUMEN)
								else
									cProxBor := " "
								EndIf
							EndIf
						EndIf
						
						If lTitulo
						
							If lEstorno .and. lEstPaNdf
								cPadrao := "531"
							ElseIf lEstCompens  //Estorno Compensacao Receber
								cPadrao := "588"									
							ElseIf lEstorno
								cPadrao := "527"
								If SE1->E1_SITUACA $ '1|H'	// Situa豫o de Cobran�a: Cobran�a Simples
									VALOR := SE1->E1_VALOR
								EndIf
							Elseif lEstCart2
								cPadrao := "540"
							ElseIf lAdiant
								dbSelectArea( "SE1" )
								cPadrao := fa070Pad()
							Elseif lCompens
								cPadrao := "597"
							Else
								cPadrao := Iif(Empty(SE5->E5_DOCUMEN) .Or. SE5->E5_MOTBX $ "PCC|LIQ|IRF" .Or. "FINA080" $ SE5->E5_ORIGEM,"530","532")
								
								//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
								//� Ponto de Entrada para validar lan�amento padr�o.�
								//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
								If l370BORD
									cPadrao := Execblock("F370BORD",.F.,.F.,{cNumBor})
								EndIf
							Endif

							lPadrao 	:= VerPadrao(cPadrao)
							lPadraoCc 	:= VerPadrao("537") //Rateio por C.Custo de MultiNat C.Pagar
							lPadraoCcE 	:= VerPadrao("538") //Estorno do rateio C.Custo de MultiMat CR
							
							IF  lPadrao 

								// Atualiza Flag de Lan�amento Cont�bil
								nRegAnt := SE5->(Recno())
								If (aScan(aRecsSE5,{|x| x[1]==nRegAnt}) == 0) 
									IF SE5->E5_TIPODOC $ "BA|VL|V2|ES|LJ|CP"
										If Alltrim(SE5->E5_TIPODOC) == "ES" .And. !Empty(SE5->E5_NUMCHEQ)
											AAdd( aRecsSE5, {SE5->(Recno()),'FINM030','FK5'} ) //  estorno de cheque
										Else 
											If SE5->E5_TABORI == "FK1" .OR. (SE5->E5_RECPAG == "R" .and. SE5->E5_TIPODOC <> "ES" .and. !SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG) .Or. (SE5->E5_RECPAG == "P" .and. SE5->E5_TIPODOC == "ES" .and. !SE5->E5_TIPO $ MVPAGANT+"/"+MV_CPNEG);
												.OR. (SE5->E5_RECPAG == "P" .and. SE5->E5_TIPODOC <> "ES" .and. SE5->E5_TIPO $ MVRECANT+"/"+MV_CRNEG)  

												AAdd( aRecsSE5, {SE5->(Recno()),'FINM010','FK1'} ) //baixa a receber
											Else
												AAdd( aRecsSE5, {SE5->(Recno()),'FINM020','FK2'} ) //  estorno de Baixas a pagar
											Endif
										EndIf	
									Else
										AAdd( aRecsSE5, {SE5->(Recno()),'',''} ) //Valores acess�rios
									EndIf
								Endif

								If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
									// Protecao provisoria: lTitulo .T. sem SE1
									IF SE1->(!EOF()) .AND. SE1->(!BOF())
										Reclock( "SE1" )
										Replace E1_VALLIQ  With nVl
										Replace E1_DESCONT With nDc
										Replace E1_JUROS	 With nJr
										Replace E1_MULTA	 With nMt
										Replace E1_CORREC  With VARIACAO
										SE1->( MsUnlock())
									ENDIF
								Else
									// Protecao provisoria: lTitulo .T. sem SE2
									IF SE2->(!EOF()) .AND. SE2->(!BOF())
										Reclock( "SE2" )
										Replace E2_VALLIQ  With nVl
										Replace E2_DESCONT With nDc
										Replace E2_JUROS	 With nJr
										Replace E2_MULTA	 With nMt
										Replace E2_CORREC  With VARIACAO
										If lPccBaixa
											Replace E2_PIS		With nPisBx
											Replace E2_COFINS	With nCofBx
											Replace E2_CSLL	With nCslBx
											Replace E2_VRETPIS	With nPisBx
											Replace E2_VRETCOF	With nCofBx
											Replace E2_VRETCSL	With nCslBx
										Endif
										SE2->(MsUnlock())
									ENDIF
								Endif

								// Posiciona no fornecedor
								If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
									dbSelectArea( "SA1" )
									dbSeek( cFilial+SE1->E1_CLIENTE+SE1->E1_LOJA )
									dbSelectArea( "SED" )
									dbSeek( cFilial+SE1->E1_NATUREZ )
								Else
									dbSelectArea("SA2")
									dbSeek(xFilial()+SE2->E2_FORNECE+SE2->E2_LOJA)
									dbSelectArea( "SED" )
									dbSeek( xFilial()+SE2->E2_NATUREZ )
								Endif
								
								// Posiciona no banco
								dbSelectArea("SA6")
								SA6->(dbSetOrder(1))
								SA6->(dbSeek(cFilial+cBanco))
								dbSelectArea("SE5")

								If lUsaFlag
									CTBAddFlag(aFlagCTB)
								EndIf

								// Totalizo por Bordero
								If cPadrao = "532"
									nValorTotal += SE2->E2_VALLIQ
									nBordero 	+= SE2->E2_VALLIQ
									nTotBord 	+= SE2->E2_VALLIQ
									nBordDc		+= SE2->E2_DESCONT
									nBordJr		+= SE2->E2_JUROS
									nBordMt		+= SE2->E2_MULTA
									nBordCm		+= SE2->E2_CORREC
									If !ALLTRIM(STRLCTPAD) $ cLstBor
										cLstBor += If(!EMPTY(cLstBor),':','') + ALLTRIM(STRLCTPAD)
									EndIf
								Endif

								If !lCabecalho
									a370Cabecalho(@nHdlPrv,@cArquivo)
								Endif
								// Se utiliza multiplas naturezas, contabiliza pelo SEV
								If  lMultNat 						
									//Contabilizando estorno de C.Receber
									If lEstorno
										cChaveSev := RetChaveSev("SE1")+"2"+cSeqSE5
										cChaveSez := RetChaveSev("SE1",,"SEZ")
									Else
										cChaveSev := RetChaveSev("SE2")+"2"+cSeqSE5
										cChaveSez := RetChaveSev("SE2",,"SEZ")
									Endif
							
									DbSelectArea("SEV")
									dbSetOrder(2)
									If MsSeek(cChaveSev)
			
										dbSelectArea("SE2")
										nValorTotal -= SE2->E2_VALLIQ							
			
										nRecSe2 := Recno()
										DbGoBottom()
										DbSkip()
										
										dbSelectArea("SE1")
										nRecSe1 := Recno()
										DbGoBottom()
										DbSkip()
										
										DbSelectArea("SEV")
				
										While xFilial("SEV")+SEV->(EV_PREFIXO+EV_NUM+EV_PARCELA+EV_TIPO+EV_CLIFOR+;
											EV_LOJA+EV_IDENT+EV_SEQ) == cChaveSev .And. !Eof()
												
											//Se estou contabilizando um estorno, trata-se de um C. Pagar,
											//So vou contabilizar os EV_SITUACA == E
											//Se nao for um estorno, nao devo contabilizar o registro se
											//EV_SITUACA == E
											If (lEstorno .and. !(SEV->EV_SITUACA == "E")) .or. ;
												(!lEstorno .and. (SEV->EV_SITUACA == "E"))
												dbSkip()
												Loop
											ElseIf lEstorno
												//O lancamento a ser considerado passa a ser o do estorno
												lPadraoCC := lPadraoCCE
											Endif
											
											If SEV->EV_LA != "S"
												dbSelectArea( "SED" )
												MsSeek( xFilial("SED")+SEV->EV_NATUREZ ) // Posiciona na natureza, pois a conta pode estar la.
												dbSelectArea("SEV")
												If SEV->EV_RATEICC == "1" .and. lPadraoCC .and. lPadrao // Rateou multinat por c.custo
													dbSelectArea("SEZ")
													dbSetOrder(4)
													MsSeek(cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5) // Posiciona no arquivo de Rateio C.Custo da MultiNat
													While SEZ->(!Eof()) .and. xFilial("SEZ")+SEZ->(EZ_PREFIXO+EZ_NUM+;
														EZ_PARCELA+EZ_TIPO+EZ_CLIFOR+EZ_LOJA+EZ_NATUREZ+EZ_IDENT+EZ_SEQ) == cChaveSeZ+SEV->EV_NATUREZ+"2"+cSeqSE5
														
														//Se estou contabilizando um estorno, trata-se de um C. Pagar,
														//So vou contabilizar os EZ_SITUACA == E
														//Se nao for um estorno, nao devo contabilizar o registro se
														//EZ_SITUACA == E
														If (lEstorno .and. !(SEZ->EZ_SITUACA == "E")) .or. ;
															(!lEstorno .and. (SEZ->EZ_SITUACA == "E"))
															SEZ->(dbSkip())
															Loop
														Endif
														If SEZ->EZ_LA != "S"
															If lUsaFlag
																aAdd(aFlagCTB,{"EZ_LA","S","SEZ",SEZ->(Recno()),0,0,0})
															EndIf
															//O lacto padrao fica:
															//537 - Rateio multinat com c.custo C.Pagar
															//538 - Estorno de Rat. Multinat C.Custo C.Receber
															cPadraoCC := If(SEZ->EZ_SITUACA == "E","538","537")
															VALOR := SEZ->EZ_VALOR
															nTotDoc	+=	DetProva(nHdlPrv,cPadraoCC,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
															If LanceiCtb // Vem do DetProva
																If !lUsaFlag
																	RecLock("SEZ")
																	SEZ->EZ_LA    := "S"
																	MsUnlock( )
																EndIf
															ElseIf lUsaFlag
																If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEZ->(Recno()) }))>0
																	aFlagCTB := Adel(aFlagCTB,nPosReg)
																	aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
																Endif
															Endif
														Endif
														SEZ->(dbSkip())
													Enddo
													DbSelectArea("SEV")
												Else
													If lUsaFlag
														aAdd(aFlagCTB,{"EV_LA","S","SEV",SEV->(Recno()),0,0,0})
													EndIf
													nTotDoc	:=	DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
												Endif

												If LanceiCtb // Vem do DetProva
													If !lUsaFlag
														RecLock("SEV")
														SEV->EV_LA    := "S"
														MsUnlock( )   
													EndIf
												ElseIf lUsaFlag
													If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEV->(Recno()) }))>0
														aFlagCTB := Adel(aFlagCTB,nPosReg)
														aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
													Endif			
												Endif
											Endif
											DbSelectArea("SEV")
											DbSkip()
											VALOR := 0
										Enddo
										nTotProc	+=	nTotDoc // Totaliza por processo
										nTotal  	+=	nTotDoc
										
										If MV_PAR12 == MVP12DOCUMENTO 
											IF nTotDoc > 0 // Por documento
												Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
												nTotDoc := 0
											Endif
											aFlagCTB := {}
										Endif

										VALOR  		:= 0									
										VALOR2 		:= 0
										VALOR3 		:= 0
										REGVALOR 	:= 0
										
										dbSelectArea("SE1")
										DbGoto(nRecSe1)
										dbSelectArea("SE2")
										DbGoto(nRecSe2)
									EndIf
								Else
									dbSelectArea("SEV")
									DbGoBottom()
									DbSkip()
									DbSelectArea("SE2")
									nTotDoc	:= DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
									nTotProc	+= nTotDoc
									nTotal		+=	nTotDoc
									
									If MV_PAR12 == MVP12DOCUMENTO 
										If nTotDoc > 0 // Por Documento
											If Empty( cProxBor ) .and. cPadrao <> "532"
												Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
												nTotDoc := 0
											Endif
										Endif
										LimpaArray(aFlagCTB)
									Endif
								Endif
							Endif
							//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
							//� Devolve a posi뇙o original do arquivo  �
							//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
							If (lAdiant .or. lEstorno .or. lEstCart2) .and. !lEstPaNdf
								dbSelectArea("SE1")
								If !Eof() .And. !Bof()
									Reclock( "SE1" )
									Replace E1_VALLIQ  With nValliq
									Replace E1_DESCONT With nDescont
									Replace E1_JUROS	 With nJuros
									Replace E1_MULTA	 With nMulta
									Replace E1_CORREC  With nCorrec
									SE1->( MsUnlock( ) )
								EndIF
								dbSelectArea( "SE5" )
							Else
								dbSelectArea("SE2")
								If !Eof() .And. !Bof()
									Reclock( "SE2" )
									Replace E2_VALLIQ  With nValliq
									Replace E2_DESCONT With nDescont
									Replace E2_JUROS	 With nJuros
									Replace E2_MULTA	 With nMulta
									Replace E2_CORREC  With nCorrec
									If lPccBaixa
										Replace E2_PIS		With nPis
										Replace E2_COFINS	With nCofins
										Replace E2_CSLL	With nCsll
										Replace E2_VRETPIS	With nVretPis
										Replace E2_VRETCOF	With nVretCof
										Replace E2_VRETCSL	With nVretCsl
									Endif
									SE2->( MsUnlock())
								EndIf
								dbSelectArea( "SE5" )
							Endif
						Endif
					END SEQUENCE
				Endif

				// Disponibilizo a Variavel VALOR com o total dos borderos aglutinados
				If lPadrao .and. nBordero > 0 .And. cProxBor <> cNumBor .and. (MV_PAR12 == MVP12DOCUMENTO .OR. MV_PAR13 == MVP13BORBOR)

					If !lCabecalho
						a370Cabecalho(@nHdlPrv,@cArquivo)
					EndIf

					nValorTotal := If(MV_PAR12 == MVP12DOCUMENTO,0,nValorTotal)
					VALOR 		:= nBordero
					VALOR2		:= nBordDc
					VALOR3		:= nBordJr
					VALOR4		:= nBordMt
					VALOR5		:= nBordCm
					STRLCTPAD	:= cLstBor
					nBordero 	:= 0.00
					nBordDc		:= 0
					nBordJr		:= 0
					nBordMt		:= 0
					nBordCm		:= 0
					cLstBor		:= ''

					// Desposicionamento de tabelas
					SE2->(dbGoTo(0))
					If mv_par06 == 2 .Or. mv_par06 == 4
						SE1->(dbGoTo(0))
						SE5->(dbGoTo(0))
						FK2->(dbGoTo(0))
						FK5->(dbGoTo(0))
					Endif

					nTotDoc := DetProva(nHdlPrv,"532","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
					nTotProc += nTotDoc
					nTotal += nTotDoc

					VALOR := 0
					VALOR2 := 0
					VALOR3 := 0
					VALOR4 := 0
					VALOR5 := 0

					If lSeqCorr
						aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
					EndIf

					If MV_PAR12 == MVP12DOCUMENTO
						Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
						LimpaArray(aFlagCTB)
					EndIf

				Endif
				
				dbSelectArea(cAliasSE5)
				If lAvancaReg
					(cAliasSE5)->(dbSkip())
				else
					lAvancaReg := .T.
				EndIf

				If (cAliasSE5)->(EOF()) .AND. MV_PAR12 == MVP12PROCESSO .AND. cTipoMov == "DIRETO"
					(cAliasSE5)->(dbGOTOP())
					cTipoMov := "TITULOS"
				EndIf
			Enddo

			//Contabiliza movimentos banc�rios por processo
			If EMPTY(nValorTotal) .and. !EMPTY(nTotProc) .and. MV_PAR12 == MVP12PROCESSO
				Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
				LimpaArray(aFlagCTB)
				nTotProc := 0
			EndIf

			//Contabiliza豫o de baixas de t�tulos por border� de pagamento
			If nValorTotal > 0 .And. MV_PAR13 == MVP13TOTBOR
				If !lCabecalho
					a370Cabecalho(@nHdlPrv,@cArquivo)
				EndIf
				VALOR       := nValorTotal
				STRLCTPAD   := cLstBor
				
				nValorTotal := 0.00
				nBordero    := 0.00
				nBordDc     := 0.00
				nBordJr     := 0.00
				nBordMt     := 0.00
				nBordCm     := 0.00
				cLstBor     := ''
				
				VALOR2      := 0.00
				VALOR3      := 0.00
				VALOR4      := 0.00
				VALOR5      := 0.00

				SE2->(dbGoTo(0))
				// Se estiver contabilizando carteira a Pagar apenas,
				// desposiciona E1 tambem, pois no LP podera conter
				// E1_VALLIQ e este campo retornara um valor, duplicando
				// o LP 527. Ex. Criar um LP 527 contabilizando pelo E1_VALLIQ
				// Fazer uma Baixa e um cancelamento, contabilizar off-line
				// escolhendo apenas a carteira a Pagar
				If mv_par06 == 2 .Or. mv_par06 == 4
					SE1->(DbGoTo(0))
					SE5->(DbGoTo(0))
					FK2->(DbGoTo(0))
					FK5->(DbGoTo(0))
				Endif

				nTotDoc := DetProva(nHdlPrv,"532","FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
				nTotProc += nTotDoc
				nTotal += nTotDoc

				If lSeqCorr
					aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
				EndIf
				Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
				LimpaArray(aFlagCTB)
			EndIF

			//Contabilizacao das transferencias
			aSort(aRecsTRF,,,{|x,y| x[3]+x[2] > y[3]+y[2]})
			For nX := 1 to Len(aRecsTRF)
				dbSelectArea("SE5") 
				dbSetOrder( 1 )
				dbGoto(aRecsTRF[nX,1])				
				SA6->(dbSetOrder( 1 ))	// A6_FILIAL + A6_COD + A6_AGENCIA + A6_NUMCON
				SA6->(dbSeek(xFilial("SA6") + SE5->(E5_BANCO + E5_AGENCIA + E5_CONTA)))
				lPadrao:=VerPadrao(aRecsTRF[nX,2])				
				If lPadrao	
					If !lCabecalho
						a370Cabecalho(@nHdlPrv,@cArquivo)
					EndIf
					If lUsaFlag
						CTBAddFlag(aFlagCTB)
					EndIf

					nTotDoc	:= DetProva(nHdlPrv,aRecsTRF[nX,2],"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)

					nTotal		+=	nTotDoc
					nTotProc	+= nTotDoc

					If SE5->E5_TIPODOC $ 'TR#TE'
						dDataCtb := SE5->E5_DATA
					EndIf				
					

					If MV_PAR12 == MVP12DOCUMENTO 
						If nTotDoc > 0 // Por documento
							If lSeqCorr
								aDiario := {{"SE5",SE5->(recno()),SE5->E5_DIACTB,"E5_NODIA","E5_DIACTB"}}
							EndIf

							IF lGroupDoc
								// Movimenta豫o Banc�ria - Controle por documento 
								IF LEN(aRecsTRF) == nX
									lQuebraDoc := .T.
								ELSE
									lQuebraDoc := ALLTRIM(aRecsTRF[nX,3]) <> ALLTRIM(aRecsTRF[nX+1,3])
								ENDIF
							ENDIF
							
							IF lQuebraDoc
								Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
								aFlagCTB := {}	// Limpa a lista ap�s a contabiliza豫o.
							Endif

						Endif
					Endif

					If LanceiCtb
						aAdd(aRecsSE5, {SE5->(RECNO()), 'FINM030', 'FK5'} ) // Movimenta豫o bancaria.
					EndIf
				Endif
			Next
			aRecsTRF := {}
		Endif

		If Len(aRecsSE5) > 0 .and. !lUsaFlag
			dbSelectArea("SE5")
			For nX := 1 to Len(aRecsSE5)
				dbGoto(aRecsSE5[nX][NRECNO])
				CTBGrvFlag(aRecsSE5[nX])
			Next
			aRecsSE5 := {} //Limpa variavel.
		Endif
		
		If MV_PAR12 == MVP12PROCESSO
			IF nTotProc > 0 // Por processo
				Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
				nTotProc := 0
			Endif
			aFlagCTB := {}
		Endif
		//Fim da contabiliza豫o do SE5				

		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Contabiliza놹o de Cheques  											  �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
   		If (mv_par06 = 3  .Or. mv_par06 = 4) .and. lLerSEF
		
			lArqSEF := .F.
			
   			If __lDefTop .and. !lMultiTHR
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//�       *** Alteracao para ganho de performance ***            �
				//쿎aso o banco seja relacional, pesquisar apenas os titulos     �
				//쿭a data que esta sendo processada. Alteracao faz com que      �
				//쿪 varredura de registros na SEF limite-se ao total de titulos �
				//쿮ncontrados na data e nao todos os registros da tabela.       �
				//�                                                              �
				//쿌PENAS a forma de varredura foi alterado, nada foi mudado     �
				//쿻as regras de gravacoes contabeis.                            �
				//�                                                              �
				//쿐STUDO DE CASO :                                              �								
				//쿎liente processa 7564 dias (nPeriodo) X 113056 registros SEF  �
				//쿟otalizando : 855.155.584 varreduras de registros no SEF      �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				lArqSEF := .T.
				aLstSEF := {}
				nz := 0
				nTotREGSEF := 0
				If Select(cAliasSEF) # 0
					dbSelectArea(cAliasSEF)
					dbCloseArea()
					fErase(cAliasSEF + OrdBagExt())
					fErase(cAliasSEF + GetDbExtension())
				Endif
				dbSelectArea("SEF")
				SEF->(dbSetOrder(1))
				cQuery := "SELECT EF_FILIAL, EF_BANCO, EF_AGENCIA, EF_CONTA, EF_NUM, EF_DATA, SEF.R_E_C_N_O_ SEFRECNO "
				cQuery += "FROM " + RetSqlName("SEF") + " SEF "
				
				IF ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))  					
	  				If lPosEfMsFil .AND. !lUsaFilOri
	  					cQuery += "WHERE EF_MSFIL = '" + cFilAnt + "' AND (("
  					Else
  						cQuery += "WHERE EF_FILORIG = '"  + cFilAnt + "' AND ((" 
					Endif				
				Else
					cQuery += "WHERE EF_FILIAL = '" + xFilial("SEF") + "' AND (("
				EndIf				

				If cMVSLDBXCR == "C"						
					cQuery += "EF_ORIGEM IN ('FINA040','FINA191','FINA070','FINA460','FINA740') AND EF_DTCOMP BETWEEN '"  + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "') OR "
					CQuery += "(EF_ORIGEM NOT IN ('FINA040','FINA191','FINA070','FINA460','FINA740') AND "
				Endif							

				cQuery += "EF_DATA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "')) AND " 
				cQuery += "EF_LA <> 'S' AND D_E_L_E_T_ = ' ' "

				If l370EFFIL
					cQuery += Execblock("F370EFF",.F.,.F.,cQuery)
				EndIf                           
				cQuery += "ORDER BY " + SqlOrder("EF_FILIAL+EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM+DTOS(EF_DATA)")
				ChangeQuery(cQuery)
				dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSEF, .F., .T.)
				(cAliasSEF)->(dbGoTop())
				Do While !(cAliasSEF)->(Eof())
					aAdd(aLstSEF,(cAliasSEF)->SEFRECNO)
					nTotREGSEF++
					(cAliasSEF)->(dbSkip())
				EndDo
   			ElseIf lMultiThr
   				// Utilizar a consulta enviada pela thread. 
				lArqSEF		:= .T.
				aLstSEF		:= {}
				nz			:= 0
				nTotREGSEF	:= 0
				
				Do While !(cAliasSEF)->(Eof())
				 	If (cAliasSEF)->EF_DATA >= MV_PAR04 .AND. (cAliasSEF)->EF_DATA <= MV_PAR05
				 		aAdd(aLstSEF,(cAliasSEF)->SEFRECNO)
				 		nTotREGSEF++
				 	ElseIf (cAliasSEF)->EF_DATA > MV_PAR04
				 		Exit
				 	EndIf
			 		(cAliasSEF)->(dbSkip())
				EndDo
   			Endif

			dbSelectArea("SEF")
			SEF->(dbSetOrder(1))
			SEF->(dbSeek(cFilial,.T.))
			
			If !lArqSEF
				bCond := {|| !SEF->(Eof()) .And. xFilial("SEF") == SEF->EF_FILIAL}
			Else
				bCond := {|| !Empty(nTotREGSEF) .AND. nz++ < nTotREGSEF}
			EndIf
			
			While Eval(bCond)

				LanceiCtb := .F. // Vem do DetProva
				
				If lArqSEF
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
					//� Guarda o pr쥅imo registro para IndRegua					  �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
					SEF->(dbGoTo(aLstSEF[nz]))
					nRegAnt := SEF->(Recno())
					cChqAnt := cChequeAtual
					dDataCTB := IF(MV_PAR03 == MVP03EMISSAO, SEF->EF_DATA, dDataBase)
					
					If nz < nTotREGSEF
						SEF->(dbGoTo(aLstSEF[nz + 1]))
						nProxReg := SEF->(Recno())
						cProxChq := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)  //Utilizado para quebra por documento
						SEF->(dbGoTo(aLstSEF[nz]))
					Else
						nProxReg := 0
						cProxChq := ""
					Endif
				Else
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
					//� Guarda o pr쥅imo registro para IndRegua				   �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
					nRegAnt := Recno()
					dbSkip()
					nProxReg := RecNo()
					cProxChq := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)  //Utilizado para quebra por documento
					dbGoto(nRegAnt)				
				Endif
				
				// Controle de Saldo Contas a Receber - par�metro MV_SLDBXCR
				If cMVSLDBXCR == "C" .AND. ALLTRIM(SEF->EF_ORIGEM) $ 'FINA040/FINA191/FINA070/FINA460/FINA740'
					dDtRefSEF := SEF->EF_DTCOMP
					dDataCTB := dDtRefSEF
				Else
					dDtRefSEF := If(cEF_T01="D",SEF->EF_DATA,StoD(SEF->EF_DATA))
				Endif							

				If dDtRefSEF >= mv_par04 .AND. ;
					dDtRefSEF <= mv_par05 .AND. ;
					!Empty(SEF->EF_NUM) .AND. ;
					SubStr(SEF->EF_LA,1,1) != "S" .AND. ;
					(Alltrim(SEF->EF_ORIGEM) $ "FINA050#FINA040#FINA080#FINA070#FINA190#FINA090#FINA091#FINA390TIT#FINA390AVU#FINA191#FINA460" .OR. ;
					Empty(SEF->EF_ORIGEM))
					
					cPadrao := "590"

					//Verifica se a contabilizacao do cheque ocorrera apenas quando ele estiver liberado
					If lCtCheqLib
						If SEF->EF_LIBER <> "S"
							cPadrao := ""
						EndIf
					EndIF

					lChqSTit := .F.
					IF SEF->EF_ORIGEM == "FINA390TIT"
						cPadrao := "566"
					Endif
					IF SEF->EF_ORIGEM == "FINA390AVU"
						cPadrao := "567"
					Endif
					If Alltrim(SEF->EF_ORIGEM) == "FINA191" .OR. Alltrim(SEF->EF_ORIGEM) == "FINA070" .Or. Alltrim(SEF->EF_ORIGEM) == "FINA460"
						cPadrao := "559"  
						If cMVSLDBXCR == "C"						
							dDataSEF := SEF->EF_DTCOMP
						Else
							dDataSEF := SEF->EF_DATA
						Endif							
					EndIf
					If Alltrim(SEF->EF_ORIGEM) == "FINA040" //Cheque gerados pelo SIGALOJA ou gerados na inclus�o de t�tulos da carteira CR.
						If cMVSLDBXCR == "C"	.and. !Empty(SEF->EF_DTCOMP)					
							dDataSEF := SEF->EF_DTCOMP
							cPadrao := "559"  
						Elseif SE1->E1_STATUS == "B" 
							dDataSEF := SEF->EF_DATA
							cPadrao := "559"  
						Else
							dbSelectArea("SEF")
							dbGoto(nProxReg)
							Loop
						Endif	
					Endif		
					If !cCtBaixa $ "AC" .and. Alltrim(SEF->EF_ORIGEM) $ "FINA050#FINA080#FINA190#FINA090#FINA091#FINA390TIT"
						dbSelectArea("SEF")
						dbGoto(nProxReg)
						Loop
					Endif
					
					// Contabilizo a emiss�o LP567 inclusive dos cancelados que n�o foram exclu�dos
					// A contabiliza豫o do cancelamento LP568 � online
					If SEF->EF_IMPRESS == "C" .AND. SEF->EF_LA == 'S'
						dbSelectArea("SEF")
						dbGoto(nProxReg)
						Loop
					Endif
					
					// Nao contabilizo cheques de PA nao aglutinados
					// Registro totalizador n?o t늤 preenchidos os campos EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO
					If SEF->EF_IMPRESS != "A" .and. Alltrim(SEF->EF_ORIGEM) == "FINA050" .AND. !EMPTY(SEF->(EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO)) 
						dbSelectArea("SEF")
						dbGoto(nProxReg)
						Loop
					Endif
					//N�o contabilizo cheque recebido que nao foram compensados
					If Empty(SEF->EF_DTCOMP) .and. Alltrim(SEF->EF_ORIGEM)=="FINA191"
						dbSelectArea("SEF")
						dbGoto(nProxReg)
						Loop
					Endif
					
					// Alimenta data de contabilizacao para cheques sem mov. bancario
					If Empty(dDataCtb)
						dDataCtb := IF(MV_PAR03 == MVP03EMISSAO, SEF->EF_DATA, dDataBase)
					EndIf

					If SEF->EF_IMPRESS $ "SNC "			// Cheque impresso ou n�o, ou Cancelado e n�o contabilizado na emiss�o
						VALOR     := SEF->EF_VALOR		// para lan놹mento padr꼘
						STRLCTPAD := SEF->EF_HIST
						NUMCHEQUE := SEF->EF_NUM
						ORIGCHEQ  := ALLTRIM(SEF->EF_ORIGEM)
						cChequeAtual := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)
						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						//� Desposiciona propositalmente o SEF para que APENAS a�
						//� variavel VALOR esteja com conteudo. O reposicionamen�
						//� to � feito na volta do Looping.                     �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						If ORIGCHEQ == "FINA190" .OR.; // Jun눯o de cheques
							(ORIGCHEQ == "FINA050" .AND. EMPTY(SEF->(EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO))) // Totalizador gerado na inclus?o de PA
							dbSelectArea("SEF")
							cChequeAtual := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)
							dbGoBottom()
							dbSkip()
							dbSelectArea("SE1")
							dbGoBottom()
							dbSkip()
							dbSelectArea("SE2")
							dbGoBottom()
							dbSkip()
							dbSelectArea("SE5")
							dbGoBottom()
							dbSkip()
						Endif
						If Alltrim(SEF->EF_ORIGEM) == "FINA080"  // Baixas a Pagar
							// Se o cheque nao foi impresso, desposiciona as tabelas para contabilizar somente com as variaveis
							If SEF->EF_IMPRESS $ "N "
								dbSelectArea("SEF")
								dbGoBottom()
								dbSkip()
								dbSelectArea("SE1")
								dbGoBottom()
								dbSkip()
								dbSelectArea("SE2")
								dbGoBottom()
								dbSkip()
								dbSelectArea("SE5")
								dbGoBottom()
								dbSkip()
							Else	// Cheque impresso
								// Posiciona SE5 na movimenta豫o de baixa do titulo do cheque e mantem as outras tabelas posicionadas
								SE5->(dbSetOrder(2))
								If ! SE5->( MsSeek( xFilial("SE5")+"VL"+SEF->(EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO+IIf(cEF_T01="D",DtoS(EF_DATA),EF_DATA)+EF_FORNECE+EF_LOJA+SEF->EF_SEQUENC)))
									SE5->( MsSeek( xFilial("SE5")+"BA"+SEF->(EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO+IIf(cEF_T01="D",DtoS(EF_DATA),EF_DATA)+EF_FORNECE+EF_LOJA+SEF->EF_SEQUENC)))
								EndIf
								SE5->(dbSetOrder(1))
								cChequeAtual := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)								
							EndIf
						Endif
						If Alltrim(SEF->EF_ORIGEM) == "FINA090"  // Baixa Autom�tica de T�tulos
							aAreaAux := {}
							AADD(aAreaAux,GETAREA())
							AADD(aAreaAux,SE5->(GetArea()))
							SE5->(dbSetOrder(11))	//E5_FILIAL, E5_BANCO, E5_AGENCIA, E5_CONTA, E5_NUMCHEQ, E5_DATA, R_E_C_N_O_, D_E_L_E_T_
							SE5->(dbSeek(xFilial('SE5')+SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)))
							While !SE5->(EOF()) .and. SE5->(E5_FILIAL+E5_BANCO+E5_AGENCIA+E5_CONTA+E5_NUMCHEQ) == xFilial('SE5')+SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)
								If SE5->E5_TIPODOC == 'CH'
									CTBAddFlag(aFlagCTB)
									EXIT
								EndIf
								SE5->(DbSkip())
							EndDo
							RestArea(aAreaAux[2])
							RestArea(aAreaAux[1])
						EndIf
						If Alltrim(SEF->EF_ORIGEM) == "FINA390TIT"  // Chq s/ Titulo
							dbSelectArea("SE1")
							dbGoBottom()
							dbSkip()
							dbSelectArea("SE2")
							dbGoBottom()
							dbSkip()
							VALOR     := 0
							lChqStit	:= .T.
						Endif
						If Alltrim(SEF->EF_ORIGEM) == "FINA390AVU"  // Cheque Avulso
							VALOR     := 0
							STRLCTPAD := ""
							NUMCHEQUE := ""
							ORIGCHEQ  := ""
							lChqStit	:= .T.
						Endif
					Elseif SEF->EF_IMPRESS == "A"	// Cheque Aglutinado
						VALOR     := 0
						STRLCTPAD := ""
						NUMCHEQUE := ""
						ORIGCHEQ  := ""
						cChequeAtual := SEF->(EF_BANCO+EF_AGENCIA+EF_CONTA+EF_NUM)
						dbSelectArea("SE5")
						dbSetOrder(2)		//posiciona no SE5
						If (dbSeek(xFilial()+"VL"+SEF->EF_PREFIXO+SEF->EF_TITULO+SEF->EF_PARCELA+SEF->EF_TIPO+IIf(cEF_T01="D",DtoS(SEF->EF_DATA),SEF->EF_DATA)+SEF->EF_FORNECE+SEF->EF_LOJA+SEF->EF_SEQUENC)) .OR.;
							dbSeek(xFilial()+"BA"+SEF->EF_PREFIXO+SEF->EF_TITULO+SEF->EF_PARCELA+SEF->EF_TIPO+IIf(cEF_T01="D",DtoS(SEF->EF_DATA),SEF->EF_DATA)+SEF->EF_FORNECE+SEF->EF_LOJA+SEF->EF_SEQUENC)
							If lUsaFlag
								CTBAddFlag(aFlagCTB)
							EndIf
						Endif
						dbSetOrder(1)
					Endif
					
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Posiciona Registros                                 �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					If !SEF->(Eof())
						dbSelectArea("SA6")
						dbSetOrder(1)
						dbSeek(cFilial+SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA)
						If !lChqSTit
							If SEF->EF_TIPO $ MVRECANT + "/" + MV_CRNEG
								//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
								//� Neste caso o titulo veio de um Contas�
								//� a Receber (SE1)                      �
								//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
								dbSelectArea("SE1")
								DbSetOrder(1)
								If dbSeek(xFilial()+SEF->EF_PREFIXO+SEF->EF_TITULO+SEF->EF_PARCELA+SEF->EF_TIPO+SEF->EF_FORNECE+SEF->EF_LOJA)
									dbSelectArea("SED")
									dbSeek(xFilial()+SE1->E1_NATUREZ)
									dbSelectArea("SA1")
									dbSeek(xFilial()+SEF->EF_FORNECE+SEF->EF_LOJA)
								Endif
							Else
								dbSelectArea( "SE2" )
								dbSetOrder(1)
								If dbSeek(xFilial("SE2")+SEF->EF_PREFIXO+SEF->EF_TITULO+;
									SEF->EF_PARCELA+SEF->EF_TIPO+;
									SEF->EF_FORNECE+SEF->EF_LOJA,.F.)
									dbSelectArea("SED")
									dbSetOrder(1)
									dbSeek(xFilial("SED")+SE2->E2_NATUREZ)
									dbSelectArea("SA2")
									dbSetOrder(1)
									dbSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA)
								EndIf
							Endif
							If EMPTY(SEF->(EF_PREFIXO+EF_TITULO+EF_PARCELA+EF_TIPO)) .OR. Alltrim(SEF->EF_ORIGEM) == "FINA390TIT"  // Chq s/ Titulo
								dbSelectArea("SEF")
								dbGoBottom()
								dbSkip()
							Endif
						Endif
					EndIf
					dbSelectArea( "SEF" )
					
					lPadrao:=VerPadrao(cPadrao)
					IF lPadrao
						If !lCabecalho
							a370Cabecalho(@nHdlPrv,@cArquivo)
						EndIF       
						If lUsaFlag
							aAdd(aFlagCTB,{"EF_LA","S","SEF",nRegAnt,0,0,0})
						EndIf                                                                                   

						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						//� Deve passar a tabela de cheques (SEF) e Recno posicionado para gravar na CTK/CV3    �
						//� Assim, no momento da exclusao do lancto. pelo CTB, limpa o flag da SEF corretamente �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						nTotDoc  += DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB,{"SEF",nRegAnt})

						nTotProc += nTotDoc
						nTotal   += nTotDoc
						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						//� Atualiza Flag de Lan놹mento Cont쟟il do cheque contabilizado  �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						dbSelectArea("SEF")
						dbGoto(nRegAnt)
						If LanceiCtb .And. !(SEF->(Eof()))
							If !lUsaFlag
								Reclock("SEF")
								SEF->EF_LA := "S"
								MsUnlock()
							EndIf
						EndIf
						// Por documento
						If MV_PAR12 == MVP12DOCUMENTO 
							If nTotDoc > 0 .and. cChequeAtual != cProxChq
								Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
								nTotDoc := 0							
							Endif
							If (SEF->(Eof()))
							aFlagCTB := {}
							EndIf
						Endif
					Endif
				Endif
				dbSelectArea("SEF")
				dbGoto(nProxReg)
			Enddo
			If MV_PAR12 == MVP12PROCESSO 
				If nTotProc > 0 // Por processo
					Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
				Endif
				aFlagCTB := {}
			Endif
		Endif
		
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//� Caixinha   SEU990             				     �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		If mv_par06 != 3 .and. lLerSEU
		
			If !lMultiTHR
			
				If Select(cAliasSEU) # 0
					dbSelectArea(cAliasSEU)
					dbCloseArea()
					fErase(cAliasSEU + OrdBagExt())
					fErase(cAliasSEU + GetDbExtension())
				Endif

				//Caso Seja Dt Baixa utilizar EU_BAIXA, se nao utilizar EU_DTDIGIT  
				If mv_par07 == 1
					cQuery := "SELECT DISTINCT EU_FILIAL, EU_CAIXA, EU_BAIXA AS DATASEU, 'OUTROS' AS TIPOMOV "
				Else
					cQuery := "SELECT DISTINCT EU_FILIAL, EU_CAIXA, EU_DTDIGIT AS DATASEU, 'OUTROS' AS TIPOMOV "
				EndIf
				
				cQuery += "FROM " + RetSqlName("SEU") + " "
				
				If ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))  					
					If lPosEuMsFil .AND. !lUsaFilOri
						cQuery += "WHERE EU_MSFIL = '" + cFilAnt + "' AND "
					Else
						cQuery += "WHERE EU_FILORI = '"  + cFilAnt + "' AND " 
					Endif				
				Else
					cQuery += "WHERE EU_FILIAL = '" + xFilial("SEU") + "' AND "
				EndIf				
				
				If mv_par07 == 1
					cQuery += "EU_BAIXA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND " 
				Else
					cQuery += "EU_DTDIGIT BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
				EndIf
				
				If _lMvPar18 .And. mv_par18 == 1
					cQuery += "EU_TIPO NOT IN ('01','03') AND  "
				EndIf
				
				cQuery += "EU_LA <> 'S' AND D_E_L_E_T_ = ' ' "

				If _lMvPar18 .And. mv_par18 == 1
					//Se for para considerar adiantamento de caixinha em aberto e solicitar pela baixa (mv_par07 = 1),  
					//ir� filtrar pela data de digita豫o
					cQuery += " UNION "
					cQuery += "SELECT DISTINCT EU_FILIAL, EU_CAIXA, EU_DTDIGIT AS DATASEU, 'ADTO' AS TIPOMOV "
					cQuery += "FROM " + RetSqlName("SEU") + " "
					
					If ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))  					
						If lPosEuMsFil .AND. !lUsaFilOri
							cQuery += "WHERE EU_MSFIL = '" + cFilAnt + "' AND "
						Else
							cQuery += "WHERE EU_FILORI = '"  + cFilAnt + "' AND " 
						Endif				
					Else
						cQuery += "WHERE EU_FILIAL = '" + xFilial("SEU") + "' AND "
					EndIf				
						
					cQuery += "EU_DTDIGIT BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
					cQuery += "EU_TIPO IN ('01','03') AND  "
					cQuery += "EU_LA <> 'S' AND D_E_L_E_T_ = ' ' "
				EndIf
				
				cQuery += "ORDER BY " + SqlOrder("EU_FILIAL+EU_CAIXA+DTOS(DATASEU)")

				ChangeQuery(cQuery)
				dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cAliasSEU, .F., .T.)

				TCSetField(cAliasSEU,'EU_BAIXA','D',TamSX3('EU_BAIXA')[1])
				TCSetField(cAliasSEU,'EU_DTDIGIT','D',TamSX3('EU_DTDIGIT')[1])
				TCSetField(cAliasSEU,'DATASEU','D',TamSX3('EU_DTDIGIT')[1])
			EndIf
				
			(cAliasSEU)->(dbGoTop())

			dbSelectArea( "SET" )
			SET->(dbSetOrder( 1 ))	//ET_FILIAL+ET_CODIGO 
	
			While !(cAliasSEU)->(Eof()) .And. xFilial("SEU") == (cAliasSEU)->EU_FILIAL
			
				// Localiza Caixinha 
				If SET->(dbSeek(xFilial("SET") + (cAliasSEU)->EU_CAIXA))

					nOrderSEU := IIf( mv_par07 == 1, 2, 4 ) // 2 - EU_FILIAL, EU_CAIXA, EU_BAIXA, EU_NUM // 4- EU_FILIAL+EU_CAIXA+DTOS(EU_DTDIGIT)+EU_NUM
					bCampo    := IIf( mv_par07 == 1, {|| SEU->EU_BAIXA }, {|| SEU->EU_DTDIGIT } )
					If ALLTRIM((cAliasSEU)->TIPOMOV) == 'ADTO'
						nOrderSEU := 4
						bCampo := {|| SEU->EU_DTDIGIT }
						bValAdt := {|| SEU->EU_TIPO $ '01;03'}
					ElseIf _lMvPar18 .and. mv_par18 == 1
						bValAdt := {|| !SEU->EU_TIPO $ '01;03'}	
					else
						bValAdt := {|| .T. }	
					EndIf
			
					dbSelectArea( "SEU" )
					SEU->(dbSetOrder( nOrderSEU ))
					SEU->(dbSeek( xFilial("SEU") + (cAliasSEU)->EU_CAIXA + DTOS((cAliasSEU)->DATASEU) , .F. ))
					
					While SEU->(!Eof()) .And. xFilial("SEU") == SEU->EU_FILIAL .And. SEU->EU_CAIXA == SET->ET_CODIGO .And. Eval(bCampo) <= mv_par05

						If !Eval(bValAdt)
							SEU->(DBSKIP())
							LOOP
						EndIf
						
						dDataCTB := Eval(bCampo)

						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						//� Ponto de Entrada para filtrar registros do SEU. �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						If l370EUFIL
							If !Execblock("F370EUF",.F.,.F.)
								SEU->(dbSkip())
								Loop
							EndIf
						EndIf
						
						//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						//� Verifica se ser� gerado Lan놹mento Cont쟟il			  �
						//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
						If SEU->EU_LA == "S"
							SEU->( dbSkip())
							Loop
						Endif
						
						// Tipo 00 sem Nro de adiantamento = Despesa (P)
						// Tipo 00 com Nro de adiantamento = Presta豫o de contas (R)
						// Tipo 01 - Adiantamento (P)
						// Tipo 02 - Devolucao de adiantamento (R)
						// Tipo 10 - Movimento Banco -> Caixinha  (R)
						// Tipo 11 - Movimento Caixinha -> Banco (P)
						
						lSkipLct := .F.
						
						//Receber
						//Verifico se eh Despesa. Se for, ignoro
						If mv_par06 == 1 .and. SEU->EU_TIPO $ "00" .AND. EMPTY(SEU->EU_NROADIA)
							lSkipLct := .T.
						Endif
						//Verifico se eh um Adiantamento ou Devolucao para o banco. Se for Ignoro
						If mv_par06 == 1 .and. SEU->EU_TIPO $ "01/03/11"
							lSkipLct := .T.
						Endif
						
						//Pagar
						//Verifico se eh Prestacao de contas de adiantamento para o caixinha.
						//Se for, ignoro pois eh movimento de entrada
						If mv_par06 == 2 .and. SEU->EU_TIPO $ "00" .and. !EMPTY(SEU->EU_NROADIA)
							lSkipLct := .T.
						Endif
						//Verifico se eh uma devolucao de dinheiro de adiantamento para o caixinha ou
						// se eh uma reposicao (Banco -> Caixinha).
						// Se for Ignoro pois eh movimento de entrada!!
						If mv_par06 == 2 .and. SEU->EU_TIPO $ "02/10"
							lSkipLct := .T.
						Endif
						
						If lSkipLct
							SEU->( dbSkip())
							Loop
						Endif
						
						//Reposicao = 10 - Devolucao de reposicao = 11
						If SEU->EU_TIPO $ "10/11"
							// Posiciona no banco
							SA6->(dbSetOrder(1))
							SA6->(dbSeek(xFilial("SA6")+SET->(ET_BANCO+ET_AGEBCO+ET_CTABCO)))
							cPadrao := IF(SEU->EU_TIPO == "10","573","57E")
						Else
							If SEU->EU_TIPO == '02'
								cPadrao:="579"
							Else
								cPadrao:="572"
							EndIf
						Endif
						
						dbSelectArea("SEU")
						lPadrao:=VerPadrao(cPadrao)
						IF lPadrao
							If !lCabecalho
								a370Cabecalho(@nHdlPrv,@cArquivo)
							Endif
							If lUsaFlag
								aAdd(aFlagCTB,{"EU_LA","S","SEU",SEU->(Recno()),0,0,0})
							EndIf
							nTotDoc	:=	DetProva(nHdlPrv,cPadrao,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB)
							nTotProc	+= nTotDoc
							nTotal	+=	nTotDoc
	
							If MV_PAR12 == MVP12DOCUMENTO 
								IF nTotDoc > 0 // Por documento
									If lSeqCorr
										aDiario := {{"SEU",SEU->(recno()),SEU->EU_DIACTB,"EU_NODIA","EU_DIACTB"}}
									EndIf
									Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,@aDiario,dDataCtb)
								Endif
								aFlagCTB := {}
							Endif
							// Atualiza Flag de Lancamento Contabil
							If LanceiCtb
								If !lUsaFlag
									Reclock("SEU")
									REPLACE SEU->EU_LA With "S"
									SEU->(MsUnlock( ))
								EndIf
							ElseIf lUsaFlag
								If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SEU->(Recno()) }))>0
									aFlagCTB := Adel(aFlagCTB,nPosReg)
									aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
								Endif
							EndIf
						Endif
						SEU->(dbSkip())
					Enddo
				EndIf
				
				(cAliasSEU)->(DbSkip())
			Enddo
			
			If MV_PAR12 == MVP12PROCESSO .and. nTotProc > 0	 // Por processo 
				Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
				aFlagCTB := {}
				nTotProc := 0
			Endif
			
		Endif
		
		/* contabiliza os itens de viagens - nao contabiliza se a carteira escolhida � "cheques" 
		em multi-thread a propria funcao da thread executa a fctba677 
		*/
		If !lMultiThr .And. !(MV_PAR06 == 3) 
			FCTBA677(@lCabecalho,@nHdlPrv,@cArquivo,@lUsaFlag,@aFlagCTB,@cLote,@nTotal,,)
		Endif
		
        If lExistFWI .And. !lMultiThr .And. (mv_par06 == 1 .or. mv_par06 == 4) 
            FCTBSITCOB(@nHdlPrv,@cArquivo,@aFlagCTB,@nTotal,"",.F.)
        EndIf
		
		If lCabecalho .And. nTotal > 0 .And. MV_PAR12 == MVP12PERIODO // Por periodo
			Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataCtb)
			aFlagCTB := {}
		Endif

		//Ponto de Entrada acionado ap�s a contabiliza豫o da Filial
		If (__CtbFPos)
			ExecBlock("CtbFPos", .F., .F.)
		EndIf

		If Len(aRecsSE5) > 0 
			dbSelectArea("SE5")
			For nX := 1 to Len(aRecsSE5)
				dbGoto(aRecsSE5[nX][NRECNO])
				If !"S" $ SE5->E5_LA
					CTBGrvFlag(aRecsSE5[nX])
				Endif
			Next
			aRecsSE5 := {} //Limpa variavel.
		Endif

	Next nLaco // final do la뇇 dos dias

	IF !lMultiThr
		If Select(cAliasSE5) > 0
			dbSelectArea(cAliasSE5)
			dbCloseArea()
		Endif
		IF Select(cAliasSE2) > 0
			dbSelectArea( cAliasSE2 )
			dbCloseArea()
		Endif
		IF Select(cAliasSEF) > 0
			dbSelectArea(cAliasSEF)
			dbCloseArea()
			fErase(cAliasSEF + OrdBagExt())
			fErase(cAliasSEF + GetDbExtension())
		Endif
		IF Select(cAliasSEU) > 0
			dbSelectArea( cAliasSEU )
			dbCloseArea()
		Endif
		If !Empty(cAliasTroc)
			(cAliasTroc)->( dbCloseArea() )
		EndIf
	EndIf

	IF MV_PAR14 == 2	// Considera Filial Original?  1- Sim, 2 - N�o
		If Empty(FWXFilial("SE1"))
			lLerSE1 := .F.
		Endif
		
		If Empty(FWXFilial("SE2"))
			lLerSE2 := .F.
		Endif
		
		If Empty(FWXFilial("SE5"))
			lLerSE5 := .F.
		Endif
		
		If Empty(FWXFilial("SEF"))
			lLerSEF := .F.
		Endif
		
		If Empty(FWXFilial("SEU"))
			lLerSEU := .F.
		Endif
	ENDIF

	If !lLerSE1 .and. !lLerSE2 .and. !lLerSE5 .and. !lLerSEF .And. !lLerSEU
		Exit
	Endif
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Data inicial precisa ser "resetada"                 �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	mv_par04 := mv_par04 - nLaco + 2
	
Next nContFil

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Recupera o valor real da data base por seguranca	�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
dDataBase := dDataAnt

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Recupera a filial original                      	  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
cFilAnt := __cFilAnt

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Recupera a Integridade dos Dados									  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
MsUnlockAll()

dbSelectArea( "SE1" )
dbSetOrder( 1 )
dbSeek(xFilial())
dbSelectArea( "SE2" )
dbSetOrder( 1 )
dbSeek(xFilial())
dbSelectArea("SEF")
dbSetOrder(1)
dbSeek(xFilial())
dbSelectArea( "SE5" )
Retindex("SE5")
dbClearFilter()

If FindFunction( "ClearCx105" )
	// Efetua a limpeza dos caches utilizadas no ctba105
	ClearCx105()
Endif

__oQryTroc := Nil

Return NIL

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o	 쿎a370Incl � Autor � Claudio D. de Souza   � Data � 12/08/02 낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o 쿐nvia lancamentos para contabilizade.                  	  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿞intaxe	 쿎a370Incl													  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿢so		 쿑INA370													  낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function Ca370Incl(cArquivo,nHdlPrv,cLote,aFlagCTB,aDiario,dDataCtb)
Local lDigita
Local lAglut

Default aDiario := {}

If nHdlPrv > 0
	lDigita := IIF(mv_par01 == 1, .T., .F.)
	lAglut  := IIF(mv_par02 == 1, .T., .F.)
	
	cA100Incl(cArquivo,nHdlPrv,3,cLote,lDigita,lAglut,,dDataCtb,,@aFlagCTB,,aDiario)
	lCabecalho := .F.
	nHdlPrv := 0
Endif

aFlagCTB := {}
aDiario	:= {}

Return Nil

//--------------------------------------------------------------------------
/*/{Protheus.doc} LimpaArray
Limpa a mem�ria e inicaliza o vetor
@Param aVetor 	Vetor de dados

@author Norberto M de Melo
@since 06/07/2020
@version 01
/*/
//---------------------------------------------------------------------------
STATIC FUNCTION LimpaArray(aVetor AS ARRAY)
	FWFReeArray(aVetor)
	aVetor := {}
RETURN NIL

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튔uncao    쿑370CTBSEL튍utor  쿘icrosiga           � Data �  03/05/09   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Funcao que pesquisa e contabiliza todos os registro de um  볍�
굇�          � recibo gerado pelo FINA087a                                볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � FINA370                                                    볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function F370CTBSEL( cSerie, cRecibo, nTotal, cLote, nHdlPrv, cArquivo, lUsaFlag, aFlagCTB, aCT5 )
Local lResult		:=	.T.
Local aListArea		:=	{ GetArea() } // Atencao: A primeira deve ser restaurada por ultimo. (UEPS)
Local i				:=	0
Local cKeyImp		:=	""
Local cAlias		:=	""
Local lAchou		:=	.F.
Local nLinha		:=	1 
Local aRecSEL		:= {}

DEFAULT aCT5 := {}

lResult := VerPadrao( "575" )

If lResult .and. !lCabecalho
	a370Cabecalho( @nHdlPrv, @cArquivo )
	
	If nHdlPrv <= 0
		Help( " ", 1, "A100NOPROV" )
		lResult := .F.
	EndIf
EndIf

If lResult
	GetDBArea( "SEL", @aListArea )
	SEL->( DBSetOrder( 8 ) )	// EL_FILIAL+EL_SERIE+EL_RECIBO+EL_TIPODOC+EL_PREFIXO+EL_NUMERO+EL_PARCELA+EL_TIPO
	SEL->( DBSeek( xFilial("SEL") + cSerie + cRecibo ) )
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//쿒era Lancamento Contab. para RECIBO.�
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	Do while	!SEL->( EOF() ) .and.;
		( SEL->EL_SERIE == cSerie ) .and.;
		( SEL->EL_RECIBO == cRecibo ) .and.;
		( SEL->EL_LA <> 'S' )
		
		//旼컴컴컴컴컴컴컴�
		//쿒uarda Registro�
		//읕컴컴컴컴컴컴컴�
		AADD( aRecSEL, SEL->(RECNO()) )
		
		//旼컴컴컴컴컴컴컴커
		//쿛osiciona Banco.�
		//읕컴컴컴컴컴컴컴켸
		GetDBArea( "SA6", @aListArea )
		SA6->( DbsetOrder( 1 ) )
		SA6->( DbSeek( xFilial("SA6") + SEL->EL_BANCO + SEL->EL_AGENCIA + SEL->EL_CONTA, .F.) )
		
		
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//쿣erifica se tem titulo vinculado.�
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		GetDBArea( "SE1", @aListArea )
		SE1->( DbsetOrder( 2 ) )
		SE1->( DbSeek(	xFilial("SE1") +;
		SEL->EL_CLIORIG +;
		SEL->EL_LOJORIG +;
		SEL->EL_PREFIXO +;
		SEL->EL_NUMERO +;
		SEL->EL_PARCELA +;
		SEL->EL_TIPO, .F.) )
		
		If !SE1->( EOF() )
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			//쿛osiciona na Natureza do Titulo .�
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			GetDBArea( "SED", @aListArea )
			SED->( DbsetOrder( 1 ) )
			SED->( DbSeek(	xFilial("SED") +;
			SE1->E1_NATUREZ, .F.) )
		Endif
		
		//旼컴컴컴컴컴컴컴컴커
		//쿛osiciona Cliente.�
		//읕컴컴컴컴컴컴컴컴켸
		GetDBArea( "SA1", @aListArea )
		SA1->( DbsetOrder( 1 ) )
		SA1->( DbSeek( xFilial("SA1") + SE1->E1_CLIENTE + SE1->E1_LOJA, .F.) )
		
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//쿛osiciona no cabecalho da NF vinculada.�
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		Do Case
			Case ( Alltrim( SEL->EL_TIPO ) == Alltrim( GetSESnew("NCC") ) )
				cAlias := "SF1"
			Case ( Alltrim( SEL->EL_TIPO ) == Alltrim( GetSESnew("NDE") ) )
				cAlias := "SF1"
			Otherwise
				cAlias := "SF2"
		EndCase
		cKeyImp := 	xFilial(cAlias)	+;
		SE1->E1_NUM		+;
		SE1->E1_PREFIXO	+;
		SE1->E1_CLIENTE	+;
		SE1->E1_LOJA
		
		If ( cAlias == "SF1" )
			cKeyImp += SE1->E1_TIPO
		Endif
		
		Posicione( cAlias, 1, cKeyImp, "F" + SubStr( cAlias, 3, 1 ) + "_VALIMP1" )
		lAchou := .F.
		
		GetDBArea( "SEL", @aListArea )
		If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
			aAdd( aFlagCTB, {"EL_LA", "S", "SEL", SEL->( Recno() ), 0, 0, 0} )
		Endif
		
		nTotal += DetProva( nHdlPrv,;
		"575" /*cPadrao*/,;
		"FINA087a" /*cPrograma*/,;
		cLote,;
		nLinha,;
		/*lExecuta*/,;
		/*cCriterio*/,;
		/*lRateio*/,;
		/*cChaveBusca*/,;
		aCT5,;
		/*lPosiciona*/,;
		@aFlagCTB,;
		/*aTabRecOri*/,;
		/*aDadosProva*/ )
		
		SEL->( DbSkip() )
	EndDo
	
	If !lUsaFlag .and. ( Len( aRecSEL ) > 0 )
		For i := 1 To Len( aRecSEL )
			SEL->( DBGoTo( aRecSEL[ i ] ) )
			RecLock( "SEL", .F. )
			Replace EL_LA With "S"
			MsUnLock()
		Next i
	EndIf
	
EndIf

// Restaura todas as areas.
// A ultima area a ser restaurada sera a area ativa no momento da chamada a esta funcao.
for i := Len( aListArea	 ) to 1 Step -1 // UEPS
	RestArea( aListArea[ i ] )
Next i

Return lResult

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿒etDBArea 튍utor  쿘icrosiga           � Data �  03/06/09   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Seleciona uma area de dados, armazena a area numa lista    볍�
굇�          � para permitir a restauracao porterior.                     볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � FINA370                                                    볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/

Static Function GetDBArea( cAlias, aListGetArea )
Default cAlias			:= Alias()
Default aListGetArea	:= {}

DBSelectArea( cAlias )
// Pesquisa para evitar duplicidade.
If ASCAN( aListGetArea, { | aVal | aVal[ 1 ] == cAlias } ) == 0
	AADD( aListGetArea, (cAlias)->( GetArea() ) )
Endif

Return NIL /*Function GetDBArea*/

//旼컴컴컴컴컴컴컴컴컴컴컴컴�
//쿛ROCESSAMENTO MULTITHREAD�
//읕컴컴컴컴컴컴컴컴컴컴컴컴�
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎TBMTFIN  튍utor  쿘arcos Justo        � Data �  12/05/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Contabiliza豫o do Financeiro - Multi Thread                볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CTBMTFIN(nCarteira,nNumProc)
Local aArea 	  := GetArea()
Local aSM0 		  := AdmAbreSM0()
Local nContFil	  := 0
Local cFilIni	  := cFilAnt
Local lRet		  := .T.
Local cFilDe	  := cFilAnt
Local cFilAte	  := cFilAnt
Local nX		  := 0
Local aSitPad     := {}

If mv_par08 == 1	//Considera filiais abaixo? 1- Sim / 2 - Nao 
	cFilDe := mv_par09
	cFilAte:= mv_par10
Endif

If mv_par14 == 1	// Considera Filial Original?  1- Sim, 2 - N�o
	cFilDe := MV_PAR15
	cFilAte:= MV_PAR16
Endif

For nContFil := 1 to Len(aSM0)
	If aSM0[nContFil][SM0_CODFIL] < cFilDe .Or. aSM0[nContFil][SM0_CODFIL] > cFilAte .Or. aSM0[nContFil][SM0_GRPEMP] != cEmpAnt
		Loop
	EndIf
	cFilAnt := aSM0[nContFil][SM0_CODFIL]
	
	If lRet .And. ( nCarteira == 1 .Or.  nCarteira == 4)
		// Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
		IF (MV_PAR14 == 2) .OR.;
			((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16)) 
			lRet := CtbMRec(nNumProc)
		EndIf
	EndIf
	
	If lRet .And. ( nCarteira == 2 .Or.  nCarteira == 4)
		// Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
		IF (MV_PAR14 == 2) .OR.;
			((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))
			lRet := CtbMPag(nNumProc)
		EndIf
	EndIf
	
	If lRet .And. ( nCarteira == 3 .Or.  nCarteira == 4)
		// Processa SEF se existir ao menos um LP configurado.
		If VldVerPad({'559','566','567','590'})
			// Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
			IF (MV_PAR14 == 2) .OR.;
				((MV_PAR14 == 1) .AND. lPosEfMsFil .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16)) .OR.;
				((MV_PAR14 == 1) .AND. !lPosEfMsFil)
				lRet := CtbMCheq(nNumProc)
			EndIf
		EndIf
	EndIf
	
	If lRet
		For nX := 1 to 2
			// Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
			IF (MV_PAR14 == 2) .OR.;
				((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))
				lRet := lRet .And. CtbMMov(nNumProc, nX == 1,'TRBMV'+CVALTOCHAR(nX))
			ENDIF
		Next
	EndIf
	
	If lRet
		// Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
		IF (MV_PAR14 == 2) .OR.;
			((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16))
			lRet := CtbMCaix(nNumProc)	// Caixinha: SET/SEU
		EndIf
	EndIf
	
	If lRet .And. !(nCarteira == 3)
		lRet := CtbMViag(nNumProc)
	Endif
	
    If lRet .And. ( nCarteira == 1 .Or.  nCarteira == 4)
        // Lps para transferencia de carteira - Receber 
        aSitPad := {'540','541','542','543','544','545','546','547','548','549','550','551','552','553','554','555','556'}
        If FwAliasInDic("FWI", .F.) .And. VldVerPad(aSitPad)
            // Verifica se vai usar filial origem e valida se cFilAnt est� dentro das op�?es informadas.
            IF (MV_PAR14 == 2) .OR.;
                ((MV_PAR14 == 1) .AND. (cFilAnt >= MV_PAR15) .AND. (cFilAnt <= MV_PAR16)) 
                lRet := CtbMSitCob(nNumProc)
            EndIf
        EndIf
    EndIf

	If !lRet
		EXIT
	EndIf
	
Next nContFil

cFilAnt := cFilIni
CTBClean()
RestArea(aArea)

Return lRet

/*/{Protheus.doc} CtbMTProc()
//Chama fun豫o de contabiliza豫o offline 
@author norbertom
@since 10/02/2019
@version undefined
@param cAlias,cTabCtb
@param cIdThread, char, N�mero da thread
@return return, return_description
CtbMTProc(cAlias,cTabCtb)
(examples)
@see (links_or_references)
/*/
Static Function CtbMTProc(cAlias,cTabCtb,cIdThread)
Local aArea		:= GetArea()
Local lRet		:= .T.

Local lBat		:= .T.
Local lMultiThr	:= .T.

Local cAliasSE1	:= NIL
Local cAliasSE2	:= NIL
Local cAliasSE5	:= NIL
Local cAliasSEF	:= NIL
Local cAliasSEU	:= NIL

Local lLerSE1	:= .F. 
Local lLerSE2	:= .F.
Local lLerSE5	:= .F.
Local lLerSEF	:= .F.
Local lLerSEU	:= .F.
Local lTrocoMT  := .F.

DEFAULT cAlias := ''
DEFAULT cTabCTB := ''
DEFAULT cIdThread := ""

If		lLerSE1 := cAlias == 'SE1'
	cAliasSE1 := cTabCTB
ElseIf	lLerSE2 := cAlias == 'SE2'
	cAliasSE2 := cTabCTB
ElseIf	lLerSE5 := cAlias == 'SE5'
	cAliasSE5 := cTabCTB
	lTrocoMT := ( cIdThread == "1" )
ElseIf	lLerSEF := cAlias == 'SEF'
	cAliasSEF := cTabCTB
ElseIf	lLerSEU := cAlias == 'SEU'
	cAliasSEU := cTabCTB
EndIf

If __lConoutR
	ConoutR( 'Thread: ' + alltrim(str(ThreadID())) + ' <> [' + cAlias + '][' + cTabCTB + ']')
Endif

If !EMPTY(cAlias) .AND. !EMPTY(cTabCTB)
	(cTabCtb)->(dbGoTop())
	CTBFINProc(lBat,lLerSE1,cAliasSE1,lLerSE2,cAliasSE2,lLerSEF,cAliasSEF,lLerSE5,cAliasSE5,lMultiThr,lLerSEU,cAliasSEU,lTrocoMT)
EndIf

RestArea(aArea)
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} CtbMViag()
Divide os registros selecionados de presta豫o de contas de viagens para
multi-processamento (multi-threads).

@author Marcello Gabriel
@since 18/08/2016
@version 12.1.7

@param nNumProc, n�mero de processos
/*/
Static Function CtbMViag(nNumProc)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cPadraoItem	:= "8B3"
Local cPadraoCabec	:= "8B5"
Local nTotalReg		:= 0
Local cRaizNome		:= 'CTBFINPROC'
Local aStruSQL		:= {}
Local cTabJob		:= "TRBFLF"
Local cPerg 		:= "FIN370"
Local cChave		:= ''

If VerPadrao(cPadraoItem) .Or. VerPadrao(cPadraoCabec)
	If !__lSchedule
		Pergunte(cPerg,.F.)
	EndIf
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//쿘onta o arquivo de trabalho�
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
	CtbGrvViag(.T.,,@cTabMult)
	
	(cTabMult)->(dbGoTop())
	(cTabMult)->(dbEval({|| nTotalReg++ }))
	(cTabMult)->(dbGoTop())
	
	If nTotalReg > 0
		
		aStruSQL := (cTabMult)->( DbStruct() )
		aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome,"")
		
		If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
			
			//Inicializa as Threads Transa豫o controlada nas Threads
			For nX := 1 to Len(aProcs)
				StartJob("JOBCTBVIAG", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],"",__cUserId,cUserName,cAcesso,cUsuario)
				Sleep(6000)
			Next nX
			
			//NAO RETIRAR A INSTRUCAO DO SLEEP
			//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
			Sleep(30000)
			//Realiza o controle das Threads
			lRet := FINMonitor(aProcs,4)
		ElseIf nNumProc == 1
			cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,cTabMult)
			lRet := FCTBA677(,,,,,,,cTabJob,.T.)
			If Select(cTabJob) > 0
				(cTabJob)->(dbCloseArea())
			EndIf
		EndIf
	EndIf
	
	If Select(cTabMult) > 0
		(cTabMult)->( dbCloseArea() )
	Endif

	MsErase(cTabMult)
	RestArea(aArea)

Endif
Return(lRet)


//-------------------------------------------------------------------
/*/{Protheus.doc} CtbGrvViag()
Seleciona os registros de presta豫o de contas de viagens e os divide
para multi-processamento (multi-threads).

@author Marcello Gabriel
@since 18/08/2016
@version 12.1.7

@param lTabTemp, indica a gera豫o da tabela tempor�ria 
@param cQuery, passado por refer�ncia, receber� o texto da consulta 
@param cTabTemp, passado por refer�ncia, receber� o nome da tabela tempor�ria (quando gerada)
/*/
Static Function CtbGrvViag(lTabTemp,cQuery,cTabTemp)
Local aStruSQL		:= {}
Local lMsFil		:= .F.
Local lFlfLA		:= .F.

Default lTabTemp	:= .F.
Default cTabTemp	:= ""
Default cQuery		:= ""

lMsFil := (FLF->(ColumnPos("FLF_MSFIL")) > 0)
lFlfLA	:= (FLF->(ColumnPos('FLF_LA')) > 0 )

cQuery := "select R_E_C_N_O_ REGFLF from " + RetSQLName("FLF") + " FLF " 

If MV_PAR14 == 1 .And. lMsFil
	cQuery += " where FLF.FLF_MSFIL BETWEEN '" + mv_par15 + "' and '" + mv_par16 + "'"
Else
	cQuery += " where FLF.FLF_FILIAL ='" + xFilial("FLF") + "'"
Endif

cQuery += " and FLF.FLF_STATUS IN ('7','8')"
If lFlfLa
	cQuery += " and FLF.FLF_LA = ' '"
Endif
cQuery += " and FLF.D_E_L_E_T_= ' '"


cQuery += " and  FLF.FLF_PRESTA IN ("
cQuery += " SELECT FO7_PRESTA FROM " + RetSQLName("FO7") + " FO7 "
	
	
If MV_PAR06 == 1 .OR. MV_PAR06 == 4
	cQuery += " JOIN " + RetSQLName("SE1") + " SE1 ON"
	cQuery += " FO7.FO7_FILIAL = SE1.E1_FILIAL AND "
	cQuery += " FO7.FO7_PREFIX = SE1.E1_PREFIXO AND "
	cQuery += " FO7.FO7_TITULO = SE1.E1_NUM AND "
	cQuery += " FO7.FO7_PARCEL = SE1.E1_PARCELA AND "
	cQuery += " FO7.FO7_TIPO = SE1.E1_TIPO AND "
	cQuery += " FO7.FO7_CLIFOR = SE1.E1_CLIENTE AND "
	cQuery += " FO7.FO7_LOJA = SE1.E1_LOJA AND "
	If MV_PAR12 == MVP12PERIODO
		cQuery += " SE1.E1_EMISSAO ='"+DTOS(mv_par04)+"' AND" 
	Else
		cQuery += " SE1.E1_EMISSAO BETWEEN '"+DTOS(mv_par04)+"' AND '"+DTOS(mv_par05)+"' AND "
	Endif
	cQuery += " SE1.D_E_L_E_T_= ' ' "
	cQuery += " WHERE FO7.FO7_FILIAL =FLF.FLF_FILIAL AND FO7.FO7_PRESTA = FLF.FLF_PRESTA AND FO7.D_E_L_E_T_= ' ' "
	cQuery += "AND FO7.FO7_RECPAG='R')"
Endif

If MV_PAR06 == 4
	cQuery += " UNION "
	cQuery += "select R_E_C_N_O_ REGFLF from " + RetSQLName("FLF") + " FLF " 

	If MV_PAR14 == 1 .And. lMsFil
		cQuery += " where FLF.FLF_MSFIL BETWEEN '" + mv_par15 + "' and '" + mv_par16 + "'"
	Else
		cQuery += " where FLF.FLF_FILIAL ='" + xFilial("FLF") + "'"
	Endif
	If MV_PAR06 == 1
		cQuery += " and FLF.FLF_RECPAG = 'R'"
	ElseIf MV_PAR06 == 2 
		cQuery += " and FLF.FLF_RECPAG = 'P'"
	Endif
	cQuery += " and FLF.FLF_STATUS IN ('7','8')"
	If lFlfLa
		cQuery += " and FLF.FLF_LA = ' '"
	Endif
	cQuery += " and FLF.D_E_L_E_T_= ' '"
	
	cQuery += " and  FLF.FLF_PRESTA IN ("
	cQuery += " SELECT FO7_PRESTA FROM " + RetSQLName("FO7") + " FO7 "
EndIf
	
If MV_PAR06 == 2 .OR. MV_PAR06 == 4
	cQuery += " JOIN " + RetSQLName("SE2") + " SE2 ON"
	cQuery += " FO7.FO7_FILIAL = SE2.E2_FILIAL AND "
	cQuery += " FO7.FO7_PREFIX = SE2.E2_PREFIXO AND "
	cQuery += " FO7.FO7_TITULO = SE2.E2_NUM AND "
	cQuery += " FO7.FO7_PARCEL = SE2.E2_PARCELA AND "
	cQuery += " FO7.FO7_TIPO = SE2.E2_TIPO AND "
	cQuery += " FO7.FO7_CLIFOR = SE2.E2_FORNECE AND "
	cQuery += " FO7.FO7_LOJA = SE2.E2_LOJA AND "
	If MV_PAR12 == MVP12PERIODO
		cQuery += " SE2.E2_EMIS1 = '"+DTOS(mv_par04)+"' AND"
	Else
		cQuery += " SE2.E2_EMIS1 BETWEEN '"+DTOS(mv_par04)+"' AND '"+DTOS(mv_par05)+"' AND "
	Endif
	cQuery += " SE2.D_E_L_E_T_= ' ' "
	cQuery += " WHERE FO7.FO7_FILIAL =FLF.FLF_FILIAL AND FO7.FO7_PRESTA = FLF.FLF_PRESTA AND FO7.D_E_L_E_T_= ' ' "
	cQuery += "AND FO7.FO7_RECPAG='P')"
Endif

If lTabTemp
	cTabTemp := FINNextAlias()
	aStruSQL := {}
	AADD(aStruSQL,{"REGFLF" ,"N",10,00})
	AADD(aStruSQL,{"CTBFLAG","C",02,00})

	MsErase(cTabTemp)
	MsCreate(cTabTemp, aStruSQL, 'TOPCONN' )
	Sleep(1000)
	DbUseArea( .T., 'TOPCONN', cTabTemp, cTabTemp, .T., .F. )
	SqlToTrb(cQuery, aStruSQL, cTabTemp)
Endif
Return()

//-------------------------------------------------------------------
/*/{Protheus.doc} JobCtbViag()
Executa o job para os registros de presta豫o de contas de viagens.

@author Marcello Gabriel
@since 18/08/2016
@version 12.1.7
/*/
Function JOBCTBViag(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario)
Local nHandle	 := 0
Local lRet		 := .T.

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

// STATUS 1 - Iniciando execucao do Job
PutGlbValue(cVarStatus,stThrStart)

//Seta job para nao consumir licensas
RpcSetType(3)
RpcClearEnv()
// Seta job para empresa filial desejada
RpcSetEnv( cEmpX,cFilX,,,,,)

// STATUS 2 - Conexao efetuada com sucesso
PutGlbValue(cVarStatus,stThrConnect)

//Set o usu�rio para buscar as perguntas do profile
lMsErroAuto := .F.
lMsHelpAuto := .T. 
lAutoErrNoFile := .T.

__cUserId := cXUserId 
cUserName := cXUserName
cAcesso   := cXAcesso
cUsuario  := cXUsuario

cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave)

// Realiza o processamento
lRet := FCTBA677(,,,,,,,cTabJob,.T.)

JOBFINEnd(cVarStatus,nHandle,lRet)

If Select(cTabJob) > 0
	(cTabJob)->(dbCloseArea())
EndIf

Return()


//MOVIMENTA플O BANCARIA
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbMMov   튍utor  쿌lvaro Camillo Neto � Data �  31/05/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿎ontabiliza豫o de movimentacao bancaria                                   볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbMMov(nNumProc,lMovNorm,cTabJOB)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cChave		:= ""
Local nTotalReg		:= 0
Local cRaizNome		:= 'CTBFINPROC'
Local aStruSQL		:= {}
Local cPerg			:= "FIN370"
Local nMaxReg		:= SuperGetMv("MV_CTBNMRB",.T.,0)	// Numero Maximo de registros a contabilizar. (Tratamento para evitar estouro na TEMPDB)
Local nSaldoReg		:= 0

If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf
If mv_par07 == 1 //DATA
	cChave := "E5_FILIAL+E5_DATA+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
ElseIf mv_par07 == 2		//DATA DE DIGITACAO
	cChave := "E5_FILIAL+E5_DTDIGIT+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
Else 							//DATA DE DISPONIBILIDADE
	cChave := "E5_FILIAL+E5_DTDISPO+E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA+E5_SEQ"
Endif

If !lMovNorm
	cChave := StrTran(cChave,'E5_RECPAG+E5_NUMCHEQ+E5_DOCUMEN','E5_PROCTRA+E5_RECPAG')
EndIf

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿘onta o arquivo de trabalho�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
cTabMult := CtbGrvMov(lMovNorm,cChave,@nTotalReg)
nMaxReg := If(EMPTY(nMaxReg),nTotalReg,nMaxReg)	// Se n�o informado pelo par�metro assume a quantidade total como padr�o

If !Empty(cTabMult) .And. (cTabMult)->(!EOF())
	
	While lRet .and. nTotalReg > 0

		If nTotalReg >= nMaxReg
			nSaldoReg := (nTotalReg - nMaxReg)	// Continua no La�o While
		Else
			nSaldoReg := 0
		EndIf
		
		aStruSQL := (cTabMult)->( DbStruct() )
		aProcs := CTBPrepFIN(@nNumProc,cTabMult,MIN(nMaxReg,nTotalReg),"CTBFLAG",cRaizNome, IIF(lMovNorm , "", "E5_DOCUMEN" ) )
	
		If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
			nTotalReg := nSaldoReg
			
			//Inicializa as Threads Transa豫o controlada nas Threads
			For nX := 1 to Len(aProcs)
				StartJob("JOBCTBMOV", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
				Sleep(6000)
			Next nX
			
			//NAO RETIRAR A INSTRUCAO DO SLEEP
			//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
			Sleep(30000)
			//Realiza o controle das Threads
			lRet := FINMonitor(aProcs,4)
		ElseIf nNumProc == 1
			cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
			lRet 	 := CtbMTProc('SE5',cTabJob)
			If Select(cTabJob) > 0
				(cTabJob)->(dbCloseArea())
			EndIf
				
			nTotalReg := 0	//  Deve sair do la�o While
		EndIf
		
	EndDo	
EndIf

If Select(cTabMult) > 0
	(cTabMult)->( dbCloseArea() )
Endif

CTBClean()
RestArea(aArea)

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbGrvMov  튍utor  쿌lvaro Camillo Neto� Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿒rava arquivo temporario das movimentacoes bancarias        볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                        볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbGrvMov(lMovNorm,cChave, nTotalReg)
Local cPerg 	 	:= "FIN370"
Local aStruSQL		:= {}
Local cTab			:= ""
Local cQuery		:= ""
Local cQuery2		:= ""
Local cCamposSel	:= ''
Local cCamposIns	:= ''
Local aStruSE5   	:= {}
Local cAliasCNT		:= ''
Local l370E5FIL := Existblock("F370E5F")   // Criado Ponto de Entrada

Default nTotalReg	:= 0

If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf

dbSelectArea("SE5")
dbSetOrder(1)

// Montagem da estrtura do arquivo
aStruSE5  := SE5->(DbStruct())
aStruSQL := aClone(aStruSE5)
AADD(aStruSQL,{"SE5RECNO"   	,"N",15,00})
AADD(aStruSQL,{"CTBFLAG" 		,"C",02,00})

// Cria tabela temporaria
cTab := CriaTMP('SE5',aStruSQL,cChave) 

// Montagem da Query
cCamposSel := ''
cCamposIns := ''
aEval(aStruSE5,{|e,i| cCamposIns += If(i==1,'',",")+AllTrim(e[1])})
cCamposSel := cCamposIns
If !_MSSQL7
	cCamposIns += ',SE5RECNO,CTBFLAG,R_E_C_N_O_'
Else
	cCamposIns += ',SE5RECNO,CTBFLAG'
EndIf 

cQuery := "SELECT " + cCamposSel
If !_MSSQL7
	cQuery += ",SE5.R_E_C_N_O_ SE5RECNO," + _cSpaceMark + " CTBFLAG,SE5.R_E_C_N_O_"
Else
	cQuery += ",SE5.R_E_C_N_O_ SE5RECNO," + _cSpaceMark + " CTBFLAG"
EndIf 
cQuery += "  FROM " + RetSqlName("SE5") + " SE5 "

If mv_par14 == 1
	If lPosE5MsFil .AND. !lUsaFilOri
	cQuery += "WHERE E5_MSFIL = '"  + cFilAnt + "' AND "
	Else
		cQuery += "WHERE E5_FILORIG = '"  + cFilAnt + "' AND "
	Endif
Else
	cQuery += "WHERE E5_FILIAL = '" + xFilial("SE5") + "' AND "
EndIf

If mv_par07 == 1			//DATA
	cQuery += "E5_DATA BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
ElseIf mv_par07 == 2		//DATA DE DIGITACAO
	cQuery += "E5_DTDIGIT    between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
Else 							//DATA DE DISPONIBILIDADE
	cQuery += "E5_DTDISPO    between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
Endif

cQuery += "E5_TIPODOC IN ('PA','RA','BA','VL','V2','AP','EP','PE','RF','IF','CP','TL','ES','TR','DB','OD','LJ','E2','TE','  ')  AND "
cQuery += "E5_SITUACA <> 'C' AND "
cQuery += "(E5_LA <> 'S ' OR ((E5_ORDREC " + _cOperador + " E5_SERREC) <> '' AND E5_RECPAG = 'R' AND E5_TIPODOC = 'BA'))  AND "  // Filtra registros de Recebimentos Diversos p/Contabilizacao

If lMovNorm
	cQuery += " (E5_DOCUMEN = ' ' AND E5_NUMCHEQ = ' ') AND "
Else
	cQuery += " (E5_DOCUMEN <> ' ' OR E5_NUMCHEQ <> ' ') AND "
EndIf

cQuery += "D_E_L_E_T_ = ' ' "

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Ponto de Entrada para filtrar registros do SE5. �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If l370E5FIL
	cQuery := Execblock("F370E5F",.F.,.F.,cQuery)
EndIf

// seta a ordem de acordo com a opcao do usuario
cQuery += " ORDER BY " + SqlOrder(cChave)

//Inicio do bloco que substitui SqlToTrb
cQuery2 := " INSERT "
If _cSGBD == "ORACLE"
	cQuery2 += " /*+ APPEND */ "
Endif
cQuery2 += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

If cQuery2 = ''
	MsgAlert(STR0060)  //Erro de Parse no Insert
Endif
If TcSqlExec(cQuery2) <> 0
	MsgAlert(STR0061) // "Erro no Insert"
EndIf

cQuery := "SELECT COUNT(*) QUANT "
cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
cQuery := ChangeQuery( cQuery )

cAliasCNT := GetNextAlias()
dbUseArea( .T. , "TOPCONN" , TCGenQry(,,cQuery) , cAliasCNT )
(cAliasCNT)->(DbGoTop())
nTotalReg := (cAliasCNT)->QUANT
(cAliasCNT)->(dbCloseArea())

//tiro o arquivo de eof
DbSelectArea(cTab)
(cTab)->(dbGoTop())
//Fim do bloco que substitui SqlToTrb

RETURN cTab
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿕OBCTBMov 튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Job da contabiliza豫o de movimentacoes bancarias           볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                        볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function JOBCTBMov(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
Local nHandle	 := 0
Local lRet		 := .T.

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 

DEFAULT cFWTMP	:= ''
DEFAULT cId	 	:= ""

cFilAnt := cFilX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

If  nHandle >= 0
	PutGlbValue(cVarStatus,stThrStart)

	//Seta job para nao consumir licensas
	RpcSetType(3)
	RpcClearEnv()
	// Seta job para empresa filial desejada
	RpcSetEnv( cEmpX,cFilX,,,,,)

	PutGlbValue(cVarStatus,stThrConnect)

	//Set o usu�rio para buscar as perguntas do profile
	lMsErroAuto := .F.
	lMsHelpAuto := .T. 
	lAutoErrNoFile := .T.

	__cUserId := cXUserId 
	cUserName := cXUserName
	cAcesso   := cXAcesso
	cUsuario  := cXUsuario
	
	//cria temporario de contabilizacao no banco de dados e otimiza validacao do lancamento
	If _lCtbIniLan
		CtbIniLan()
	EndIf
	
	cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

	// Realiza o processamento
	lRet := CtbmtProc('SE5',cTabJob,cId)

	JOBFINEnd(cVarStatus,nHandle,lRet)

	If Select(cTabJob) > 0
		(cTabJob)->(dbCloseArea())
	EndIf

	//exclui temporario de contabilizacao utilizado para otimizacao validacao do lancamento
	If FindFunction("CtbFinLan")
		//FINALIZA E APAGA ARQUIVO TMP NO BANCO
		CtbFinLan()
	EndIf
	
Else
	PutGlbValue(cVarStatus,stThrError)
EndIf

Return

// CAIXINHA

/*/{Protheus.doc} CtbMCaix
//Consulta, prepara e executa a contabiliza豫o do caixinha.
@author norbertom
@since 10/02/2019
@version P!@
@param nNumProc, numeric, description
@return return, return_description
@example
CtbMCaix(nNumProc)
@see (links_or_references)
/*/
Static Function CtbMCaix(nNumProc)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cChave        := ""

Local nTotalReg	:= 0
Local cRaizNome	:= 'CTBFINPROC'
Local aStruSQL	:= {}
Local cTabJob	:= "TRBSEU"

cChave := "EU_FILIAL+EU_CAIXA+DATASEU"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿘onta o arquivo de trabalho�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
cTabMult := CtbGrvCaix(cChave,@nTotalReg)

If !Empty(cTabMult) .And. (cTabMult)->(!EOF())
	
	aStruSQL := (cTabMult)->( DbStruct() )
	aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome)

	If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
		
		//Inicializa as Threads Transa豫o controlada nas Threads
		For nX := 1 to Len(aProcs)
			StartJob("JOBCTBCAIX", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
			Sleep(6000)
		Next nX
		
		//NAO RETIRAR A INSTRUCAO DO SLEEP
		//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
		Sleep(30000)  
		//Realiza o controle das Threads
		lRet := FINMonitor(aProcs,3)
	ElseIf nNumProc == 1
		cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
		lRet 	 := CtbMTProc('SEU',cTabJob)
		If Select(cTabJob) > 0
			(cTabJob)->(dbCloseArea())
		EndIf
	EndIf
EndIf

If Select(cTabMult) > 0
	(cTabMult)->( dbCloseArea() )
Endif

CTBClean()
RestArea(aArea)
Return lRet 

/*/{Protheus.doc} CtbMCheq
//Monta a consulta e cria a tabela tempor�ria.
@author norbertom
@since 10/02/2019
@version undefined
@param nNumProc, numeric, description
@return return, return_description
@example
CtbGrvCaix(cChave,nTotalReg)
@see (links_or_references)
/*/
Static Function CtbGrvCaix(cChave,nTotalReg)
Local cPerg 	 	:= "FIN370"
Local aStruSQL		:= {}
Local cTab			:= ""
Local cQuery		:= ""
Local cQuery2		:= ""
Local cCamposSel	:= ''
Local cCamposAux	:= ''
Local cCamposIns	:= ''
Local aStruAux		:= {}
Local aStruSEU   	:= {}
Local cAliasCNT		:= NIL
Local nI			:= NIL

Default cChave		:= ''
Default nTotalReg	:= 0

If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf

If mv_par07 == 1
	cCamposSel := "EU_FILIAL,EU_CAIXA,EU_BAIXA AS DATASEU, 'OUTROS' AS TIPOMOV "
Else
	cCamposSel := "EU_FILIAL,EU_CAIXA,EU_DTDIGIT AS DATASEU, 'OUTROS' AS TIPOMOV "
EndIf

// Montagem da estrtura do arquivo
dbSelectArea("SEU")
SEU->(dbSetOrder(1))
aStruAux := SEU->(dbStruct())
For nI := 1 TO LEN(aStruAux)
	If ALLTRIM(aStruAux[nI][1]) $ cCamposSel
		If ALLTRIM(aStruAux[nI][1])$"EU_BAIXA/EU_DTDIGIT"
			AADD(aStruSEU,{"DATASEU"   	,"D",8,00})	
		Else
			AADD(aStruSEU,aStruAux[nI])
		EndIf
	EndIf
Next nI
aStruSQL := aClone(aStruSEU)

AADD(aStruSQL,{"TIPOMOV"   	,"C",10,00})
AADD(aStruSQL,{"SEURECNO"   ,"N",15,00})
AADD(aStruSQL,{"CTBFLAG" 	,"C",02,00})

// Cria tabela temporaria
cTab := CriaTMP('SEU',aStruSQL,cChave) 

// Montagem da Query
aEval(aStruSEU,{|e,i| cCamposIns += If(i==1,'',",")+AllTrim(e[1])})
cCamposAux := cCamposIns

If !_MSSQL7
	cCamposIns += ", TIPOMOV,CTBFLAG,R_E_C_N_O_"
Else
	cCamposIns += ", TIPOMOV,CTBFLAG "
EndIf 

cQuery := "SELECT DISTINCT " + cCamposSel

If !_MSSQL7
	cQuery += "," + _cSpaceMark + " CTBFLAG,SEU.R_E_C_N_O_"
Else
	cQuery += "," + _cSpaceMark + " CTBFLAG"
EndIf 

cQuery += "  FROM " + RetSqlName("SEU")+" SEU "

If mv_par14 == 1
	If lPosEfMsFil .AND. !lUsaFilOri
		cQuery += "WHERE SEU.EU_MSFIL = '"  + cFilAnt + "' AND "
	Else
		cQuery += "WHERE SEU.EU_FILORI = '"  + cFilAnt + "' AND "
	Endif		
Else
	cQuery += "WHERE SEU.EU_FILIAL = '" + xFilial("SEU") + "' AND "
EndIf
If mv_par07 == 1
	cQuery += " SEU.EU_BAIXA   between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
Else
	cQuery += " SEU.EU_DTDIGIT between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
EndIf

If _lMvPar18 .And. mv_par18 == 1
	cQuery += "EU_TIPO NOT IN ('01','03') AND  "
EndIf

cQuery += " SEU.EU_LA <> 'S' AND "
cQuery += " SEU.D_E_L_E_T_ = ' ' "

If _lMvPar18 .And. mv_par18 == 1
	cQuery += " UNION "
						
	cQuery += "SELECT DISTINCT EU_FILIAL, EU_CAIXA, EU_DTDIGIT AS DATASEU, 'ADTO' AS TIPOMOV "
	If !_MSSQL7
		cQuery += "," + _cSpaceMark + " CTBFLAG,SEU.R_E_C_N_O_"
	Else
		cQuery += "," + _cSpaceMark + " CTBFLAG"
	EndIf 
	cQuery += "  FROM " + RetSqlName("SEU")+" SEU "

	If mv_par14 == 1
		If lPosEfMsFil .AND. !lUsaFilOri
			cQuery += "WHERE SEU.EU_MSFIL = '"  + cFilAnt + "' AND "
		Else
			cQuery += "WHERE SEU.EU_FILORI = '"  + cFilAnt + "' AND "
		Endif		
	Else
		cQuery += "WHERE SEU.EU_FILIAL = '" + xFilial("SEU") + "' AND "
	EndIf
	cQuery += " SEU.EU_DTDIGIT BETWEEN '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
	cQuery +=  "SEU.EU_TIPO IN ('01','03') AND "
	cQuery += " SEU.EU_LA <> 'S' AND "
	cQuery += " SEU.D_E_L_E_T_ = ' ' "
EndIf

// seta a ordem de acordo com a opcao do usuario
cQuery += " ORDER BY " + SqlOrder(cChave)

//Inicio do bloco que substitui SqlToTrb
cQuery2 := " INSERT "
If _cSGBD == "ORACLE"
	cQuery2 += " /*+ APPEND */ "
Endif
cQuery2 += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

if cQuery2 = ''
	MsgAlert(STR0060) //"Erro de Parse no Insert"
Endif
If TcSqlExec(cQuery2) <> 0
	MsgAlert(STR0061) //"Erro no Insert"
EndIf

cQuery := "SELECT COUNT(*) QUANT "
cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
cQuery := ChangeQuery( cQuery )

cAliasCNT := GetNextAlias()
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAliasCNT )
(cAliasCNT)->(DbGoTop())
nTotalReg := (cAliasCNT)->QUANT
(cAliasCNT)->(dbCloseArea())

//tiro o arquivo de eof
DbSelectArea(cTab)
(cTab)->(dbGoTop())
//Fim do bloco que substitui SqlToTrb

RETURN cTab

/*/{Protheus.doc} JOBCTBCaix
// Lan�ador das threads Caixinha.
@author norbertom
@since 10/02/2019
@version undefined
@param nNumProc, numeric, description
@return return, return_description
@example
JOBCTBCaix(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
@see (links_or_references)
/*/
Function JOBCTBCaix(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
Local nHandle	 := 0
Local lRet		 := .T.

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 

DEFAULT cFWTMP	:= ''

cFilAnt := cFilX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

If  nHandle >= 0
	PutGlbValue(cVarStatus,stThrStart)

	//Seta job para nao consumir licensas
	RpcSetType(3)
	RpcClearEnv()
	// Seta job para empresa filial desejada
	RpcSetEnv( cEmpX,cFilX,,,,,)

	PutGlbValue(cVarStatus,stThrConnect)

	//Set o usu�rio para buscar as perguntas do profile
	lMsErroAuto := .F.
	lMsHelpAuto := .T. 
	lAutoErrNoFile := .T.

	__cUserId := cXUserId 
	cUserName := cXUserName
	cAcesso   := cXAcesso
	cUsuario  := cXUsuario

	//cria temporario de contabilizacao no banco de dados e otimiza validacao do lancamento
	If _lCtbIniLan
		CtbIniLan()
	EndIf
	
	cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

	// Realiza o processamento
	lRet := CtbMTProc('SEU',cTabJob)

	JOBFINEnd(cVarStatus,nHandle,lRet)

	If Select(cTabJob) > 0
		(cTabJob)->(dbCloseArea())
	EndIf

	//exclui temporario de contabilizacao no banco de dados utilizado para otimizacao validacao do lancamento
	If FindFunction("CtbFinLan")
		//FINALIZA E APAGA ARQUIVO TMP NO BANCO
		CtbFinLan()
	EndIf
	
Else
	PutGlbValue(cVarStatus,stThrError)
EndIf

Return

//CHEQUES

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbMCheq   튍utor  쿌lvaro Camillo Neto � Data �  31/05/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿎ontabiliza豫o de cheques                                   볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbMCheq(nNumProc)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cChave        := "EF_FILIAL+EF_DATA+EF_BANCO+EF_AGENCIA+EF_CONTA"

Local nTotalReg	:= 0
Local cRaizNome	:= 'CTBFINPROC'
Local aStruSQL	:= {}
Local cTabJob	:= "TRBSEF"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿘onta o arquivo de trabalho�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
cTabMult := CtbGrvCheq(cChave,@nTotalReg)

If !Empty(cTabMult) .And. (cTabMult)->(!EOF())
	
	aStruSQL := (cTabMult)->( DbStruct() )
	aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome)

	If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
		
		//Inicializa as Threads Transa豫o controlada nas Threads
		For nX := 1 to Len(aProcs)
			StartJob("JOBCTBCHEQ", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
			Sleep(6000)
		Next nX
		
		//NAO RETIRAR A INSTRUCAO DO SLEEP
		//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
		Sleep(30000)  
		//Realiza o controle das Threads
		lRet := FINMonitor(aProcs,3)
	ElseIf nNumProc == 1
		cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
		lRet 	 := CtbMTProc('SEF',cTabJob)
		If Select(cTabJob) > 0
			(cTabJob)->(dbCloseArea())
		EndIf
	EndIf
EndIf

If Select(cTabMult) > 0
	(cTabMult)->( dbCloseArea() )
Endif

CTBClean()
RestArea(aArea)

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbGrvCheq  튍utor  쿌lvaro Camillo Neto� Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿒rava arquivo temporario dos cheques                       볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                        볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbGrvCheq(cChave,nTotalReg)
Local cPerg 	 	:= "FIN370"
Local aStruSQL		:= {}
Local cTab			:= ""
Local cQuery		:= ""
Local cQuery2		:= ""
Local cCamposSel	:= ''
Local cCamposIns	:= ''
Local aStruSEF   	:= {}
Local cAliasCNT
Local l370EFFIL 	:= Existblock("F370EFF")   // Criado Ponto de Entrada

Default nTotalReg	:= 0

If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf

dbSelectArea("SEF")
dbSetOrder(1)

// Montagem da estrtura do arquivo
aStruSEF  := SEF->(dbStruct())
aStruSQL := aClone(aStruSEF)
AADD(aStruSQL,{"SEFRECNO"   	,"N",15,00})
AADD(aStruSQL,{"CTBFLAG" 		,"C",02,00})

// Cria tabela temporaria
cTab := CriaTMP('SEF',aStruSQL,cChave) 

// Montagem da Query
cCamposSel := ''
cCamposIns := ''
aEval(aStruSEF,{|e,i| cCamposIns += If(i==1,'',",")+AllTrim(e[1])})
cCamposSel := cCamposIns
If !_MSSQL7
	cCamposIns += ',SEFRECNO,CTBFLAG,R_E_C_N_O_'
Else
	cCamposIns += ',SEFRECNO,CTBFLAG'
EndIf 

cQuery := "SELECT " + cCamposSel
If !_MSSQL7
	cQuery += ",SEF.R_E_C_N_O_ SEFRECNO," + _cSpaceMark + " CTBFLAG,SEF.R_E_C_N_O_"
Else
	cQuery += ",SEF.R_E_C_N_O_ SEFRECNO," + _cSpaceMark + " CTBFLAG"
EndIf 

cQuery += "  FROM " + RetSqlName("SEF")+" SEF "

If mv_par14 == 1
	If lPosEfMsFil .AND. !lUsaFilOri
		cQuery += "WHERE EF_MSFIL = '"  + cFilAnt + "' AND "
	Else
		cQuery += "WHERE EF_FILORIG = '"  + cFilAnt + "' AND "
	Endif		
Else
	cQuery += "WHERE EF_FILIAL = '" + xFilial("SEF") + "' AND "
EndIf
cQuery += " EF_DATA    between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "
cQuery += " EF_LA <> 'S' AND "
cQuery += " D_E_L_E_T_ = ' ' "

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Ponto de Entrada para filtrar registros do SE2. �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If l370EFFIL
	cQuery += Execblock("F370EFF",.F.,.F.,cQuery)
EndIf

// seta a ordem de acordo com a opcao do usuario
cQuery += " ORDER BY " + SqlOrder(cChave)

//Inicio do bloco que substitui SqlToTrb
cQuery2 := " INSERT "
If _cSGBD == "ORACLE"
	cQuery2 += " /*+ APPEND */ "
Endif
cQuery2 += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

if cQuery2 = ''
	MsgAlert(STR0060) //"Erro de Parse no Insert"
Endif
If TcSqlExec(cQuery2) <> 0
	MsgAlert(STR0061) //"Erro no Insert"
EndIf

cQuery := "SELECT COUNT(*) QUANT "
cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
cQuery := ChangeQuery( cQuery )

cAliasCNT := GetNextAlias()
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAliasCNT )
(cAliasCNT)->(DbGoTop())
nTotalReg := (cAliasCNT)->QUANT
(cAliasCNT)->(dbCloseArea())

//tiro o arquivo de eof
DbSelectArea(cTab)
(ctab)->(dbGoTop())
//Fim do bloco que substitui SqlToTrb

RETURN ctab
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿕OBCTBCheq  튍utor  쿌lvaro Camillo Neto� Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Job da contabiliza豫o de cheques                     볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                        볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function JOBCTBCheq(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
Local nHandle	 := 0
Local lRet		 := .T.

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 

DEFAULT cFWTMP	:= ''

cFilAnt := cFilX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

If  nHandle >= 0
	PutGlbValue(cVarStatus,stThrStart)

	//Seta job para nao consumir licensas
	RpcSetType(3)
	RpcClearEnv()
	// Seta job para empresa filial desejada
	RpcSetEnv( cEmpX,cFilX,,,,,)

	PutGlbValue(cVarStatus,stThrConnect)

	//Set o usu�rio para buscar as perguntas do profile
	lMsErroAuto := .F.
	lMsHelpAuto := .T. 
	lAutoErrNoFile := .T.

	__cUserId := cXUserId 
	cUserName := cXUserName
	cAcesso   := cXAcesso
	cUsuario  := cXUsuario

	//cria temporario de contabilizacao no banco de dados e otimiza validacao do lancamento
	If _lCtbIniLan
		CtbIniLan()
	EndIf
	
	cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

	// Realiza o processamento
	lRet := CtbMTProc('SEF',cTabJob)

	JOBFINEnd(cVarStatus,nHandle,lRet)

	If Select(cTabJob) > 0
		(cTabJob)->(dbCloseArea())
	EndIf

	//exclui temporario de contabilizacao no banco de dados utilizado para otimizacao validacao do lancamento
	If FindFunction("CtbFinLan")
		//FINALIZA E APAGA ARQUIVO TMP NO BANCO
		CtbFinLan()
	EndIf
	
Else
	PutGlbValue(cVarStatus,stThrError)
EndIf

Return

//CONTAS A PAGAR

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbMPag   튍utor  쿌lvaro Camillo Neto � Data �  31/05/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿎ontabiliza豫o do titulos a pagar                           볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbMPag(nNumProc)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cChave 		:= "E2_FILIAL+E2_EMIS1+E2_NUMBOR+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA"

Local nTotalReg	:= 0
Local cRaizNome	:= 'CTBFINPROC'
Local aStruSQL	:= {}
Local cTabJob	:= "TRBSE2"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿘onta o arquivo de trabalho�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
cTabMult := CtbGrvPag(cChave,@nTotalReg)

If !Empty(cTabMult) .And. (cTabMult)->(!EOF())
	
	aStruSQL := (cTabMult)->( DbStruct() )
	aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome)

	If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
		
		//Inicializa as Threads Transa豫o controlada nas Threads
		For nX := 1 to Len(aProcs)
			StartJob("JOBCTBPAG", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
			Sleep(6000)
		Next nX
		
		//NAO RETIRAR A INSTRUCAO DO SLEEP
		//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
		Sleep(30000)
		//Realiza o controle das Threads
		lRet := FINMonitor(aProcs,2)
	ElseIf nNumProc == 1
		cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
		lRet 	 := CtbMTProc('SE2',cTabJob)
		If Select(cTabJob) > 0
			(cTabJob)->(dbCloseArea())
		EndIf
	EndIf
EndIf

If Select(cTabMult) > 0
	(cTabMult)->( dbCloseArea() )
Endif
CTBClean()
RestArea(aArea)

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbGrvPag  튍utor  쿌lvaro Camillo Neto� Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿒rava arquivo temporario dos titulos a Pagar                볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbGrvPag(cChave,nTotalReg)
Local cPerg 	 := "FIN370"
Local aStruSQL		:= {}
Local cTab			:= ""
Local cQuery		:= ""
Local cQuery2		:= ""
Local cCamposSel	:= ''
Local cCamposIns	:= ''
Local aStruSE2		:= {}
Local cSepProv		:= If("|"$MVPROVIS,"|",",")
Local cAliasCNT		:= ''

Static l370E2FIL	:= Existblock("F370E2F")   // Criado Ponto de Entrada

Default nTotalReg	:= 0

If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf
dbSelectArea("SE2")
dbSetOrder(1)

// Montagem da estrtura do arquivo
aStruSE2  := SE2->(dbStruct())
aStruSQL := aClone(aStruSE2)
AADD(aStruSQL,{"SE2RECNO"    	,"N",15,00})
AADD(aStruSQL,{"CTBFLAG" 		,"C",02,00})

// Cria tabela temporaria
cTab := CriaTMP('SE2',aStruSQL,cChave) 

// Montagem da Query
cCamposSel := ''
cCamposIns := ''
aEval(aStruSE2,{|e,i| cCamposIns += If(i==1,'',",")+AllTrim(e[1])})
cCamposSel := cCamposIns
If !_MSSQL7
	cCamposIns += ',SE2RECNO,CTBFLAG,R_E_C_N_O_'
Else
	cCamposIns += ',SE2RECNO,CTBFLAG'
EndIf 

cQuery := "SELECT " + cCamposSel
If !_MSSQL7
	cQuery += ",SE2.R_E_C_N_O_ SE2RECNO," + _cSpaceMark + " CTBFLAG,SE2.R_E_C_N_O_"
Else
	cQuery += ",SE2.R_E_C_N_O_ SE2RECNO," + _cSpaceMark + " CTBFLAG"
EndIf 
cQuery += "  FROM " + RetSqlName("SE2")+" SE2 "

If mv_par14 == 1	
	If lPosE2MsFil .AND. !lUsaFilOri
	cQuery += "WHERE E2_MSFIL = '"  + cFilAnt + "' AND "
Else
		cQuery += "WHERE E2_FILORIG = '"  + cFilAnt + "' AND "
	Endif	
Else
	cQuery += "WHERE E2_FILIAL = '" + xFilial("SE2") + "' AND "
EndIf

cQuery += "E2_EMIS1 between '" + DTOS(mv_par04) + "' AND '" + DTOS(mv_par05) + "' AND "

// Soh adiciona filtro na query se nao contabiliza titulos provisorios
If mv_par17 <> 1
	cQuery += "E2_TIPO NOT IN " + FormatIn(MVPROVIS,cSepProv) + " AND "
EndIf

cQuery += " E2_LA <> 'S' AND E2_ORIGEM <> 'FINA677' AND "
cQuery += " D_E_L_E_T_ = ' ' "

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Ponto de Entrada para filtrar registros do SE2. �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If l370E2FIL
	cQuery := Execblock("F370E2F",.F.,.F.,cQuery)
EndIf

// seta a ordem de acordo com a opcao do usuario
cQuery += " ORDER BY " + SqlOrder(cChave)

//Inicio do bloco que substitui SqlToTrb
cQuery2 := " INSERT "
If _cSGBD == "ORACLE"
	cQuery2 += " /*+ APPEND */ "
Endif
cQuery2 += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

if cQuery2 = ''
	MsgAlert(STR0060) //"Erro de Parse no Insert"
Endif
If TcSqlExec(cQuery2) <> 0
	MsgAlert(STR0061) //"Erro no Insert"
EndIf

// Obtem a Contagem de Registros a processar.
cQuery := "SELECT COUNT(*) QUANT "
cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
cQuery := ChangeQuery( cQuery )

cAliasCNT := GetNextAlias()
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAliasCNT )
(cAliasCNT)->(DbGoTop())
nTotalReg := (cAliasCNT)->QUANT
(cAliasCNT)->(dbCloseArea())

//tiro o arquivo de eof
DbSelectArea(cTab)
(ctab)->(dbGoTop())
//Fim do bloco que substitui SqlToTrb

RETURN ctab
/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿕OBCTBPag 튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Job da contabiliza豫o do titulo a pagar                    볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function JOBCTBPag(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
Local nHandle	 := 0
Local lRet		 := .T.

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 

DEFAULT cFWTMP	:= ''

cFilAnt := cFilX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

If  nHandle >= 0
	PutGlbValue(cVarStatus,stThrStart)

	//Seta job para nao consumir licensas
	RpcSetType(3)
	RpcClearEnv()
	// Seta job para empresa filial desejada
	RpcSetEnv( cEmpX,cFilX,,,,,)

	PutGlbValue(cVarStatus,stThrConnect)

	//Set o usu�rio para buscar as perguntas do profile
	lMsErroAuto := .F.
	lMsHelpAuto := .T. 
	lAutoErrNoFile := .T.

	__cUserId := cXUserId 
	cUserName := cXUserName
	cAcesso   := cXAcesso
	cUsuario  := cXUsuario

	//cria temporario de contabilizacao no banco de dados e otimiza validacao do lancamento
	If _lCtbIniLan
		CtbIniLan()
	EndIf
	
	cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

	// Realiza o processamento
	lRet := CtbMTProc('SE2',cTabJob)

	JOBFINEnd(cVarStatus,nHandle,lRet)

	If Select(cTabJob) > 0
		(cTabJob)->(dbCloseArea())
	EndIf

	//exclui temporario de contabilizacao no banco de dados utilizado para otimizacao validacao do lancamento
	If FindFunction("CtbFinLan")
		//FINALIZA E APAGA ARQUIVO TMP NO BANCO
		CtbFinLan()
	EndIf
	
Else
	PutGlbValue(cVarStatus,stThrError)
EndIf

Return

// CONTAS A RECEBER

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbMRec   튍utor  쿌lvaro Camillo Neto � Data �  31/05/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿎ontabiliza豫o do titulos a receber                         볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbMRec(nNumProc)
Local aArea 		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local aProcs 		:= {}
Local cTabMult		:= ""// Tabela fisica para o processamento multi thread
Local cChave 		:= "E1_FILIAL+E1_EMISSAO+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA"

Local nTotalReg	:= 0
Local cRaizNome	:= 'CTBFINPROC'
Local aStruSQL	:= {}
Local cTabJob	:= "TRBSE1"

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿘onta o arquivo de trabalho�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴�
cTabMult := CtbGrvRec(cChave,@nTotalReg)

If !Empty(cTabMult) .And. (cTabMult)->(!EOF())
	
	aStruSQL := (cTabMult)->( DbStruct() )
	aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome)

	If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
		
		//Inicializa as Threads Transa豫o controlada nas Threads
		For nX := 1 to Len(aProcs)
			StartJob("JOBCTBREC", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,cValToChar(nX),aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
			Sleep(6000)
		Next nX
		
		//NAO RETIRAR A INSTRUCAO DO SLEEP
		//Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
		Sleep(30000)  
		//Realiza o controle das Threads
		lRet := FINMonitor(aProcs,1)
	ElseIf nNumProc == 1
		cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
		lRet 	 := CtbMTProc('SE1',cTabJob)
		If Select(cTabJob) > 0	
			(cTabJob)->(dbCloseArea())
		EndIf
	EndIf
EndIf

If Select(cTabMult) > 0
	(cTabMult)->( dbCloseArea() )
Endif

CTBClean()
RestArea(aArea)

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbGrvRec 튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿒rava arquivo temporario dos titulos a receber              볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbGrvRec(cChave, nTotalReg)
Local cPerg 	 := "FIN370"
Local aStruSQL		:= {}
Local cTab			:= ""
Local cQuery		:= ""
Local cQuery2		:= ""
Local cCamposSel	:= ''
Local cCamposIns	:= ''
Local aStruSE1   	:= {}
Local cAliQry       := ""
Local nCtbVenda		:= SuperGetMV("MV_CTBINTE",,1) // 1 - Contabiliza por titulo ; 2 - contabiliza por venda (somente quando possuir integra豫o)
Local cAliasCNT		:= ''

//Static lCFINE1FFIL 	:= Existblock("CFINE1F")   // Criado Ponto de Entrada

Default nTotalReg   := 0
cAliQry       := cTab + "_1" 
If !__lSchedule
	Pergunte(cPerg,.F.)
EndIf
dbSelectArea("SE1")
dbSetOrder(1)

// Montagem da estrtura do arquivo
aStruSE1  := SE1->(dbStruct())
aStruSQL := aClone(aStruSE1)
AADD(aStruSQL,{"SE1RECNO"    	,"N",15,00})
AADD(aStruSQL,{"CTBFLAG" 		,"C",02,00})

// Cria tabela temporaria
cTab := CriaTMP('SE1',aStruSQL,cChave) 

// Montagem da Query
cCamposSel := ''
cCamposIns := ''
aEval(aStruSE1,{|e,i| cCamposIns += IF(i==1,'',',')+AllTrim(e[1])})
cCamposSel := cCamposIns
If !_MSSQL7
	cCamposIns += ',SE1RECNO,CTBFLAG,R_E_C_N_O_'
Else
	cCamposIns += ',SE1RECNO,CTBFLAG'
EndIf 

cQuery := "SELECT " + cCamposSel
If !_MSSQL7
	cQuery += ",SE1.R_E_C_N_O_ SE1RECNO," + _cSpaceMark + " CTBFLAG,SE1.R_E_C_N_O_"
Else
	cQuery += ",SE1.R_E_C_N_O_ SE1RECNO," + _cSpaceMark + " CTBFLAG"
EndIf 
cQuery += "  FROM " + RetSqlName("SE1")+" SE1 "

If mv_par14 == 1
	If lPosE1MsFil .AND. !lUsaFilOri
		cQuery += "WHERE E1_MSFIL = '"  + cFilAnt + "'"
Else
		cQuery += "WHERE E1_FILORIG = '"  + cFilAnt + "'"
	Endif	
Else
	cQuery += "WHERE E1_FILIAL = '" + xFilial("SE1") + "'"
EndIf

cQuery += " AND SE1.E1_EMISSAO BETWEEN '"+DTOS(mv_par04)+"' AND '"+DTOS(mv_par05)+"'"
cQuery += " AND SE1.E1_LA <> 'S' AND E1_ORIGEM <> 'FINA677'  "       

If nCtbVenda == 2 //Contabiliza豫o por venda
	cQuery += " AND E1_ORIGEM <> 'FINI055'"
Endif

cQuery += " AND D_E_L_E_T_ = ' ' "

/*	Avaliar
// Ponto de Entrada para filtrar registros do SE1.
If lCFINE1FFIL
	cQuery := Execblock("CFINE1F",.F.,.F.,cQuery)

    //esta query somente para pegar estrutura pois em algum cliente os campos n�o estao na ordem de criacao da tabela temporaria
    //efetuado somente neste ponto de entrada pois tem que colocar na query o campo , SE1.R_E_C_N_O_ R_E_C_N_O_ no caso da tabela SE1
    //pode ser necessario fazer nos outros pontos de manipulacao da query do filtro ( sob demanda )
    cCampos := ''
	dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery+" AND R_E_C_N_O_ = 1 ") , cAliQry , .T. , .T. )
	aStruQry	:= (cAliQry)->(dbStruct())
	(cAliQry)->( dbCloseArea() ) 
	aEval(aStruQry,{|e,i| cCampos += If(i==1,'',',')+AllTrim(e[1])})
EndIf
*/

cQuery += " ORDER BY " + SqlOrder(cChave)

//Inicio do bloco que substitui SqlToTrb
cQuery2 := " INSERT "
If _cSGBD == "ORACLE"
	cQuery2 += " /*+ APPEND */ "
Endif
cQuery2 += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

If cQuery2 = ''
	MsgAlert(STR0060) //"Erro de Parse no Insert"
Endif
If TcSqlExec(cQuery2) <> 0
	MsgAlert(STR0061) //"Erro no Insert"
EndIf

// Obtem a Contagem de Registros a processar.
cQuery := "SELECT COUNT(*) QUANT "
cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
cQuery := ChangeQuery( cQuery )

cAliasCNT := GetNextAlias()
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAliasCNT )
(cAliasCNT)->(DbGoTop())
nTotalReg := (cAliasCNT)->QUANT
(cAliasCNT)->(dbCloseArea())

//tiro o arquivo de eof
DbSelectArea(cTab)
(ctab)->(dbGoTop())
//Fim do bloco que substitui SqlToTrb

RETURN ctab

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿕OBCTBREC 튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Job da contabiliza豫o do titulo a receber                  볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Function JOBCTBREC(cEmpX,cFilX,cMarca,cFileLck,cCpoFlag,cTabMaster,aStructTab,cTabJob,cId,cVarStatus,cChave,cXUserId,cXUserName,cXAcesso,cXUsuario,cFWTMP)
Local nHandle	 := 0
Local lRet		 := .T.   

Private lMsErroAuto 
Private lMsHelpAuto 
Private lAutoErrNoFile 

DEFAULT cFWTMP	:= ''

cFilAnt := cFilX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿌bre o arquivo de Lock parao controle externo das threads�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
nHandle := FINLock(cFileLck)

If  nHandle >= 0
	PutGlbValue(cVarStatus,stThrStart)

	//Seta job para nao consumir licensas
	RpcSetType(3)
	RpcClearEnv()
	// Seta job para empresa filial desejada
	RpcSetEnv( cEmpX,cFilX,,,,,)

	PutGlbValue(cVarStatus,stThrConnect)

	//Set o usu�rio para buscar as perguntas do profile
	lMsErroAuto := .F.
	lMsHelpAuto := .T. 
	lAutoErrNoFile := .T.

	__cUserId := cXUserId 
	cUserName := cXUserName
	cAcesso   := cXAcesso
	cUsuario  := cXUsuario

	//cria temporario de contabilizacao no banco de dados e otimiza validacao do lancamento
	If _lCtbIniLan
		CtbIniLan()
	EndIf
	
	cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

	// Realiza o processamento
	lRet := CtbMTProc('SE1',cTabJob)
		
	JOBFINEnd(cVarStatus,nHandle,lRet)

	If Select(cTabJob) > 0
		(cTabJob)->(dbCloseArea())
	EndIf

	//exclui temporario de contabilizacao no banco de dados utilizado para otimizacao validacao do lancamento
	If FindFunction("CtbFinLan")
		//FINALIZA E APAGA ARQUIVO TMP NO BANCO
		CtbFinLan()
	EndIf
	
Else
	PutGlbValue(cVarStatus,stThrError)
EndIf

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿑INMonitor튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Fun豫o responsavel por monitorar as threads de processamen 볍�
굇�          � to                                                         볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       �                                                            볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FINMonitor(aProcs,nTipo)
Local lRet			:= .T.
Local nX			:= 0
Local nHandle		:= 0
Local cMsg			:= ""
Local cRotErro		:= ""
Local cTipoErro		:= ""
Local cMsgErro		:= ""
Local aErros		:= {}
Local cMarkSem		:= "***"
Local nMarkSem		:= 0
Local cArqSem		:= ""
Local cOrigem		:= ""
Local nTMPGerado	:= 0
Local aTMPGerado	:= ARRAY(LEN(aProcs))

DEFAULT nTipo := 0

AFILL(aTMPGerado,.F.)

Do Case
	Case nTipo == 1 //Contas a receber
		cOrigem := 'CTBREC'
	Case nTipo == 2 //Contas a pagar
		cOrigem := 'CTBPAG'
	Case nTipo == 3 //Cheque
		cOrigem := 'CTBCHQ'
	Case nTipo == 4 //Movimentacao
		cOrigem := 'CTBMOV'
EndCase

While .T.
	For nX := 1 to Len(aProcs)
		//CONOUT('***** CONTROLE PARA LIBERACAO MONITOR: ['+ALLTRIM(STR(nX))+']: - Thread No ['+ALLTRIM(Str(ThreadID()))+'] *****')
		//CONOUT('***** STATUS: ['+aProcs[nX][VAR_STATUS]+']['+GETGlbValue(aProcs[nX][VAR_STATUS])+']-['+TIME()+'] *****')
		If nTMPGerado < Len(aProcs)
			If !aTMPGerado[nX] .and. GetGlbValue(aProcs[nX][VAR_STATUS]) >= '3'
				aTMPGerado[nX] := .T.
				nTMPGerado++
				//CONOUT('***** CONTROLE PARA LIBERACAO TEMPORARIA: ['+ALLTRIM(STR(nTMPGerado))+']: - Thread No ['+ALLTRIM(Str(ThreadID()))+'] *****')
			EndIf
			
			If nTMPGerado == Len(aProcs)
				CTBClean()
			EndIf
		EndIf
		
		If aScan(aProcs,{|aItem| aItem[1] == cMarkSem + aProcs[nX][1]} ) == 0
			//espera 15 segundos antes de tentar lockar o arquivo
			Sleep(15000) 
			nHandle := FINLock(aProcs[nX][1])
			If  nHandle >= 0
				FClose(nHandle)
				aProcs[nX][1] := cMarkSem + aProcs[nX][1]
				nMarkSem++
			EndIf
		EndIf
	Next
	If nMarkSem >= Len(aProcs)
		Exit
	EndIF

	Sleep(5000)  //espera +5 segundos antes de entrar novamente no laco FOR....NEXT

End

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿣erifica se todas a threads foram processadas corretamente�
//쿹ibera o recurso e apaga o arquivo                        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
For nX := 1 to Len(aProcs)
	cArqSem := STRTRAN(aProcs[nX][1],"*","")
	
	FT_FUse(cArqSem)
	FT_FGoTop()
	cMsg := FT_FReadLn()
	FT_FUse()
	fErase(cArqSem)
	
	If lRet .And. ALLTRIM(cMsg) != MSG_OK
		lRet := .F.
	EndIf
Next nX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿣erifica quais threads deram erro�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
If !lRet
	Do Case
		Case nTipo == 1 //Contas a receber
			cRotErro := STR0039	//" Erro no processamento Contas a receber: "
		Case nTipo == 2 //Contas a pagar
			cRotErro := STR0040	//" Erro no processamento Contas a pagar: "
		Case nTipo == 3 //Cheque
			cRotErro := STR0041	//" Erro no processamento Cheques: "
		Case nTipo == 4 //Movimentacao
			cRotErro := STR0042	//" Erro no processamento Movimenta豫o banc�ria: "
		Otherwise
			cRotErro := STR0046	//"Erro no Processamento"
	EndCase
	
	For nX := 1 to Len(aProcs)
		cStatus := GetGlbValue(aProcs[nX][VAR_STATUS])
		If cStatus != '3' // Concluido com sucesso
			Do Case
				Case cStatus == "1" // Erro na Conex�o
					cTipoErro := STR0043//" Erro na inicializa豫o do processo"
				Case cStatus == "2" // Erro no Processamento
					cTipoErro := STR0044 //" Erro no processo de contabiliza豫o"
			EndCase
			cMsgErro := cRotErro + cTipoErro + STR0045 + cValTochar(nX) //" processo numero "
			ProcLogAtu("ERRO",STR0046,cMsgErro)//"Erro no Processamento"
			aAdd(aErros,cMsgErro)
		EndIf
	Next nX
EndIf

// Limpa Vari�veis Globais
For nX := 1 TO LEN(aProcs)
	ClearGlbValue(aProcs[nX][VAR_STATUS])
Next nX

If !lRet
	If __lSchedule .or. MsgYesNo(STR0047)//"Ocorreram inconsistencia no processo, deseja imprimir o relatorio de erros?"
		CtRConOut(aErros)
	EndIf
EndIf

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿞elFINJob 튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Seleciona os registros para o processamento da Thread      볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function SelFINJob(cArqSem,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)
Local cQuery	:= ""
Local nX		:= 0

Default cArqSem	:= ''
Default cTabJob	:= FINNextAlias()
Default cFWTMP	:= cTabMaster
Default cChave	:= ""
 
cQuery += " SELECT "

For nX := 1 to Len(aStructTab)
	cQuery += " "+aStructTab[nX][1]+"  ,"
Next nX

cQuery := Left(cQuery,Len(cQuery)-1)
cQuery +=" FROM " + cFWTMP + " "
cQuery +=" WHERE " + cCpoFlag + " = '"+cMarca+"' "

If !Empty(cChave) 
	cQuery += " ORDER BY " + SqlOrder(cChave)
EndIf
 
cQuery := ChangeQuery(cQuery)

dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), cTabJob, .F., .T.)

IF !(cTabJob)->(EOF())
	For nX := 1 to Len(aStructTab)
		If aStructTab[nX][2] <> "C"
			TcSetField(cTabJob ,aStructTab[nX][1],aStructTab[nX][2],aStructTab[nX][3],aStructTab[nX][4])
		EndIf
	Next nX
	(cTabJob)->(dbGotop())
ENDIF

If !EMPTY(cArqSem)
	//CONOUT('***** STATUS PRE: ['+cArqSem+']['+GETGlbValue(cArqSem)+']-['+TIME()+'] - Thread No ['+ALLTRIM(Str(ThreadID()))+'] *****')
	PutGlbValue(cArqSem,stThrTbltmp)
	//CONOUT('***** CONSULTA CONCLUIDA COM SUCESSO: ['+cFWTMP+']: - Thread No ['+ALLTRIM(Str(ThreadID()))+'] *****')
	//CONOUT('***** STATUS POS: ['+cArqSem+']['+GETGlbValue(cArqSem)+']-['+TIME()+'] - Thread No ['+ALLTRIM(Str(ThreadID()))+'] *****')
EndIf

Return cTabJob

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o    쿑INUnLock � Autor 쿎ontroladoria          � Data � 15/04/10 낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o 쿐ncerra a trava                                             낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇� Uso      �                                                            낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FINUnLock(nHandle,lOk)
DEFAULT nHandle := -1
DEFAULT lOk := .T.

IF nHandle >= 0
	FWRITE(nHandle,IF(lOk,MSG_OK,MSG_ERRO))
	FCLOSE(nHandle)
ENDIF

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o    쿑INLock   |Autor  쿌lvaro Camillo Neto    | Data � 15/04/10 |굇
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o 쿎ria arquivo para travar processos e garantir que sao unicos낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇� Uso      �                                                            낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FINLock(cFile)
Local nHJob := -1
If File(cFile)
	nHJob := FOPEN(cFile,2)
Else
	nHJob := FCREATE(cFile)
EndIf
Return nHJob

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎TBPrepFIN튍utor  쿌lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿛repara as informacoes para o processamento multithread     볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CTBPrepFIN(nNumProc,cTabTemp,nTotalReg,cCpoFlag,cRaizNome,cCpoCond)
Local aProcs 		:= NIL
Local nX		 	:= 0
Local cDirSem  		:= "\Semaforo\"
Local cNomeArq		:= ""
Local cMarca  		:= ""
Local nRegAProc		:= 0 // Registros a processar
Local nRegJProc		:= 0 // Total de registros j� processados
Local cVarStatus	:= ""
Local nMxRcTHR		:= 200	// N� m�nimo de registros por thhread

Default cCpoCond := ""

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//쿎ria a pasta do semaforo caso n�o exista�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If !ExistDir(cDirSem)
	MontaDir(cDirSem)
EndIf

//Realizar calculo da quantidade de registros por thread.
//Est� definido que deve haver um m�nimo de 200 registros por thread
While nNumProc > 1
	IF (nTotalReg / nNumProc) >= nMxRcTHR
		EXIT
	ENDIF
	nNumProc := nNumProc - 1
EndDo

aProcs := Array(nNumProc)

For nX := 1 to Len(aProcs)
	cNomeArq 	:= cDirSem + cRaizNome +cEmpAnt + cFilAnt +cValtoChar(nX)+cValtoChar(INT(Seconds())) + '.lck'
	cNomeArq 	:= STRTRAN(cNomeArq,' ','_')
	cMarca		:= GetMark()
	nRegAProc	:= IIf( nX == Len(aProcs), nTotalReg-nRegJProc, Int(nTotalReg / nNumProc) )
	nRegJProc	+= nRegAProc
	cVarStatus  :="cFINP"+cEmpAnt+cFilAnt+StrZero(nX,2)+cMarca
	cVarStatus := STRTRAN(cVarStatus,' ','_')
	aProcs[nX]	:= {cNomeArq ,cMarca,nRegAProc,cVarStatus }
	PutGlbValue(cVarStatus,stThrReady)
	Sleep( nX+1000 ) //espera no minimo por 1 segundo para Seconds() retornar numero diferente
Next nX

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//쿝ealiza o Update dos campos de flag setando quais registros�
//쿬ada thread ir� processar.                                 �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
FINFlag(cTabTemp,aProcs,cCpoFlag,cCpoCond)

Return aProcs

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿑INFlag   튍utor  쿌Lvaro Camillo Neto � Data �  15/04/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿝ealiza o Update dos campos de flag setando quais registros 볍�
굇�          쿬ada thread ir� processar.                                  볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       �                                                            볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FINFlag(cTabMult,aProcs,cCpoFlag,cCpoCond)
Local nX		:= 1
Local nY		:= 0
Local cChave	:= ""
Local cCompar	:= ""
Local nQuant	:= 0
Local lOK		:= .T.
Local nCont		:= 0
Local cNumCMP	:= ''
Local cRecnoI	:= ''
Local cRecnoF	:= ''
Local cLista	:= ''

// Campos que ser�o utilizados para definir a condi豫o de divis�o dos registros
// Enquanto a express�o estiver na tabela esse conjunto ficara com a mesma marca.
Default cCpoCond := ''
//?럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫렓
//?Descobre o Recno M쟸imo e Minimo de cada ?
//?intervalo                                ?
//?럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫럫�?
nY := Len(aProcs)
IF (nY > 0)
	
(cTabMult)->(dbGoTop())

	// Utilizado aninhamento de WHILE em substitui�?o ao aninhamento FOR/WHILE pois 
	// o comando EXIT no nivel mais interno fazia sair dos dois niveis de uma vez.
		While (cTabMult)->(!EOF())
			
		nCont := aProcs[nX][QTD_REGISTROS]
		nQuant := 0
		lOK := .T.

		WHILE lOK

			If !EMPTY((cTabMult)->&(cCpoFlag)) .AND. ((cTabMult)->&(cCpoFlag) <> aProcs[nX][MARCA])
				(cTabMult)->(dbSkip())
				LOOP
			EndIf
		
			nQuant++	// Controle da quantidade de registros para cada Thread
			If nQuant == 1 
				cRecnoI := CVALTOCHAR((cTabMult)->(RECNO()))
			EndIf
			
			// Campo para avalia�?o de grupo de registros .
			If !Empty(cCpoCond)
				cCompar := ALLTRIM((cTabMult)->&(cCpoCond))
				
				// Tratamento para as transfer늧cias banc쟲ias onde o sistema relaciona 
				// a saida no campo E5_NUMCHEQ e a entrada no campo E5_DOCUMEN
				IF (cCpoCond == 'E5_DOCUMEN') .AND. EMPTY(cCompar)
					cCompar := ALLTRIM((cTabMult)->E5_NUMCHEQ)
				ENDIF
				
				// Tratamento para as transa�?es de Compensa�?o
				IF (cCpoCond == 'E5_DOCUMEN') .And. E5_MOTBX == 'CMP'
					cNumCMP := (cTabMult)->(E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+E5_CLIFOR+E5_LOJA)
				ENDIF
			EndIf
			
			// cCpoCond tem como objetivo n?o separar registros cujo conteudo seja identico.
			// Portanto este criterio pode inplicar um numero maior de registros marcados
			// do que o informado em aProcs.
			If (nQuant <= nCont) .OR. (!Empty( cCpoCond ) .AND. (cChave == ALLTRIM( cCompar ) .OR. cChave == cNumCMP))
				cRecnoF := CVALTOCHAR((cTabMult)->(RECNO()))
				If !_MSSQL7
					If EMPTY(cLista)
						cLista := '(' + ALLTRIM(STR((cTabMult)->(RECNO())))
					Else
						cLista += ',' + ALLTRIM(STR((cTabMult)->(RECNO())))
					EndIf
					
					If Len(cLista) >= 999
						cLista += ')'
						cUpdate := "UPDATE " + cTabMult + " SET " + cCpoFlag + " = '" + aProcs[nX][MARCA] + "' WHERE R_E_C_N_O_ IN " + cLista
						
						If TcSqlExec(cUpdate) < 0 
							If __lConoutR
								ConoutR( "ERRO AO ATUALIZAR:["+cUpdate+']' )
							Endif
						EndIf
						
						cLista := "" 
												
					Endif 
					
				EndIf
			Else
				lOK := .F.
			EndIf
			
			If !Empty(cCpoCond)
				cChave := ALLTRIM((cTabMult)->&(cCpoCond))
				
				// Tratamento para as transfer늧cias banc쟲ias onde o sistema relaciona 
				// a saida no campo E5_NUMCHEQ e a entrada no campo E5_DOCUMEN
				IF (cCpoCond == 'E5_DOCUMEN') .AND. EMPTY(cChave)
					cChave := (cTabMult)->E5_NUMCHEQ
			EndIf
			
			EndIf

			IF lOK			
			(cTabMult)->(dbSkip())
				IF (cTabMult)->(EOF())
					EXIT
				ENDIF
			ENDIF
		EndDo

		If !Empty(cLista)
			cLista += ')'
			cUpdate := "UPDATE " + cTabMult + " SET " + cCpoFlag + " = '" + aProcs[nX][MARCA] + "' WHERE R_E_C_N_O_ IN " + cLista
		ElseIf !Empty(cRecnoI+cRecnoF) .AND. _MSSQL7
			If FwIsInCallStack("CtbMViag")
				cUpdate := "UPDATE " + cTabMult + " SET " + cCpoFlag + " = '" + aProcs[nX][MARCA] + "' WHERE R_E_C_N_O_ BETWEEN '"+cRecnoI+"' AND '"+cRecnoF+"' "
			Else
				cUpdate := "UPDATE " + _oCTBAFIN:GETREALNAME() + " SET " + cCpoFlag + " = '" + aProcs[nX][MARCA] + "' WHERE R_E_C_N_O_ BETWEEN '"+cRecnoI+"' AND '"+cRecnoF+"' "
			EndIf
		EndIf

		If TcSqlExec(cUpdate) < 0 
			If __lConoutR
				ConoutR( "ERRO AO ATUALIZAR:["+cUpdate+']' )
			Endif
		EndIf	

		cLista := "" 
		
		// Permite incremento somente ate o tamanho de elementos de aProcs
		IF (nX < nY)
			nX++
		ELSE
			EXIT
		ENDIF

	EndDo

EndIf

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴敲굇
굇튡rograma  쿑INNextAlias튍utor  쿌lvaro Camillo Neto � Data �  15/04/10 볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴묽�
굇튒esc.     쿛rote豫o para retornar o pr�ximo alias disponivel no Banco  볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FINNextAlias()
Local cNextAlias := ""
Local aArea := GetArea()

While .T.
	cNextAlias := CriaTrab(NIL, .F.)
	If !TCCanOpen(cNextAlias) .And. Select(cNextAlias) == 0
		Exit
	EndIf
EndDo

// adiciono o alias para for�ar a limpeza no final.
Aadd( __aFinAlias , cNextAlias )

RestArea(aArea)

Return cNextAlias

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿎tbValMult튍utor  쿌lvaro Camillo Neto � Data �  05/19/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Valida se o processamento ser� feito MultiThread           볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function CtbValMult(lMostraHelp,lAglutina,lMostraLanc,nTipo)
Local lRet 		:= .T.
Local nHelp		:= 1
Default lMostraHelp := .F.
Default nTipo 		:= 1
Default lMostraLanc := .F.

If lMostraHelp .And. !__lSchedule
	nHelp := 1
Else
	nHelp := 0
EndIf


If lRet .And. nTipo != 2
	If lMostraHelp .And. !__lSchedule
		lRet := MsgYesNo(STR0048,STR0049)//"O processamento Multithread est� disponivel apenas para processamento por documento, o processamento ser� feito sem multithread. Concorda com opera豫o?"##"Aten豫o"
	Else
		lRet := .F.
	EndIf
EndIf

If lRet .And. lAglutina
	If lMostraHelp .And. !__lSchedule
		lRet := MsgYesNo(STR0050,STR0049)//"O processamento Multithread est� disponivel apenas para processamento sem aglutina豫o, o processamento ser� feito sem multithread. Concorda com opera豫o?" ##"Aten豫o"
	Else
		lRet := .F.
	EndIf
EndIf

If lRet
	If FindFunction("CTBINTRAN")
		lCtbInTran := CTBINTRAN(nHelp,lMostraLanc)
	Else
		lCtbInTran := .F.
	EndIf
	
	If !lCtbInTran
		If lMostraHelp .And. !_lBlind
			lRet := MsgYesNo(STR0051,STR0049)//"O processamento ser� feito sem multithread. Concorda com opera豫o?" ##"Aten豫o"
		Else
			lRet := .F.
		EndIf
	EndIf
EndIf

Return lRet


/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿕OBFINEnd 튍utor  쿘icrosiga           � Data �  06/21/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     �  Finaliza a Thread verificando se ocorrer help (erro)      볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function JOBFINEnd(cVarStatus,nHandle,lRet)
Local nX	:= 0
Local aLog  := {}
Local cMsg	:= ""

aLog  := GETAUTOGRLOG()

If !Empty(aLog)
	For nX := 1 to Len(aLog)
		cMsg := aLog[nX] 	
	Next
	If __lConoutR
		ConoutR( "Error CTBAFIN - " + cMsg )
	Endif
	UserException( "Error CTBAFIN- " + cMsg )
	PutGlbValue(cVarStatus,stThrError)
Else
	PutGlbValue(cVarStatus,stThrFinish)
	FINUnLock(nHandle,lRet)
Endif

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿑inIniVar 튍utor  쿘icrosiga           � Data �  06/21/10   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     � Inicializa as variaveis staticas da contabiliza豫o         볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP                                                         볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FinIniVar()
Local cPerg    := "FIN370"
Local aAreaSX1 := NIL

If TYPE("cCarteira") == 'U' .or. cCarteira == Nil
	cCarteira	:= GetMV("MV_CARTEIR")
EndIf

If TYPE("cCtBaixa") == 'U' .or. cCtBaixa == Nil
	cCtBaixa	:= Getmv("MV_CTBAIXA")
EndIf

If TYPE("lUsaFlag") == 'U' .or. lUsaFlag == Nil
	lUsaFlag	:= GetNewPar("MV_CTBFLAG",.F.)
EndIf

If TYPE("lPCCBaixa") == 'U' .or. lPCCBaixa == Nil 
	lPCCBaixa	:= SuperGetMv("MV_BX10925",.T.,"2") == "1"  .and. (!Empty( SE5->( FieldPos( "E5_VRETPIS" ) ) ) .And. !Empty( SE5->( FieldPos( "E5_VRETCOF" ) ) ) .And. ;
						!Empty( SE5->( FieldPos( "E5_VRETCSL" ) ) ) .And. !Empty( SE5->( FieldPos( "E5_PRETPIS" ) ) ) .And. ;
						!Empty( SE5->( FieldPos( "E5_PRETCOF" ) ) ) .And. !Empty( SE5->( FieldPos( "E5_PRETCSL" ) ) ) .And. ;
						!Empty( SE2->( FieldPos( "E2_SEQBX"   ) ) ) .And. !Empty( SFQ->( FieldPos( "FQ_SEQDES"  ) ) ) )
EndIf

If lPosE1MsFil == Nil
	lPosE1MsFil	:= !Empty( SE1->( FieldPos( "E1_MSFIL" ) ) )
EndIf

If lPosE2MsFil == Nil
	lPosE2MsFil	:= !Empty( SE2->( FieldPos( "E2_MSFIL" ) ) )
EndIf

If lPosE5MsFil == Nil
	lPosE5MsFil	:= !Empty( SE5->( FieldPos( "E5_MSFIL" ) ) )
EndIf

If lPosEfMsFil == Nil
	lPosEfMsFil	:= !Empty( SEF->( FieldPos( "EF_MSFIL" ) ) )
EndIf

If lPosEuMsFil == Nil
	lPosEuMsFil	:= !Empty( SEU->( FieldPos( "EU_MSFIL" ) ) )
EndIf

If lSeqCorr == Nil
	lSeqCorr	:= FindFunction( "UsaSeqCor" ) .And. UsaSeqCor()
EndIf

If lUsaFilOri == Nil
	lUsaFilOri	:= SuperGetMv("MV_CTMSFIL",.F.,.F.)
EndIf		

aAreaSX1 := SX1->(GetArea())
SX1->(DbSetOrder(1)) 
If SX1->(DBSEEK(PADR(cPerg, LEN(SX1->X1_GRUPO), ' ') + '18')) 
	_lMvPar18 := .T.
EndIf
RestArea(aAreaSX1)

Return



//-------------------------------------------------------------------
/*/{Protheus.doc} FCTBA677()
Gera Lan�amento contabil off line pela Presta豫o de Contas.

@author Antonio Flor�ncio Domingos Filho
@since 11/05/2015
@version 12.1.6
/*/

Function FCTBA677(lCabecalho,nHdlPrv,cArquivo,lUsaFlag,aFlagCTB,cLoteCtb,nTotal,cAliasFLF,lMultThr)
Local lRet			:= .T.
Local lPadraoItem	:= .F.
Local lPadraoCabec	:= .F.
Local cPadraoItem	:= "8B3"		/* prestacao por item */
Local cPadraoCabec	:= "8B5"		/* prestacao por cabecalho */
Local aGetArea  	:= GetArea()
Local nTotDoc		:= 0
Local nValLanc		:= 0
Local nPosReg		:= 0
Local cQuery		:= ""
Local cAliasFL		:= ""
Local cFilFLE		:= ""
Local lMsFil		:= .F.
Local aTabRecOri	:= {'',0}	// aTabRecOri[1]-> Tabela Origem ; aTabRecOri[2]-> RecNo
Local lFlfLa	:= .F.
Local aCT5       := {}

Default lCabecalho	:= .F.
Default nHdlPrv		:= 0
Default nTotal		:= 0
Default cArquivo	:= ""
Default lUsaFlag	:= SuperGetMV("MV_CTBFLAG",.T.,.F.)
Default aFlagCTB 	:= {}
Default cLoteCTB   	:= LoteCont("FIN")
Default cAliasFLF	:= ""
Default lMultThr	:= .F.

lMsFil := (FLF->(ColumnPos("FLF_MSFIL")) > 0)
lFlfLa := (FLF->(ColumnPos('FLF_LA')) > 0) 
cLote  := If(cLote=NIL,Space(TamSX3("CT2_LOTE")[1]),cLote)    

lPadraoItem  := VerPadrao(cPadraoItem)                                               
lPadraoCabec := VerPadrao(cPadraoCabec)                                               

FLE->(DbSetOrder(1))
FO7->(DbSetOrder(2))

If !lCabecalho
	a370Cabecalho(@nHdlPrv,@cArquivo)
Endif
		
if lPadraoItem .Or. lPadraoCabec
	If lMultThr .And. Select(cAliasFLF) > 0
	 	cAliasFL := cAliasFLF
	Else
		CtbGrvViag(.F.,@cQuery)	
		cAliasFL := FINNextAlias()
		DbUseArea(.T.,"TOPCONN",TCGenQry(,,cQuery),cAliasFL,.F.,.T.)
	Endif

	While !((cAliasFL)->(eof()))
		FLF->(DbGoTo((cAliasFL)->REGFLF))

		If !(MV_PAR12 == MVP12PERIODO) .And. nHdlPrv == 0
			a370Cabecalho(@nHdlPrv,@cArquivo)
		Endif
		
		If FLF->FLF_STATUS $ '7|8'
			If lCtbPFO7
				Execblock("CtbPFO7",.F.,.F.)
			Else
				If MV_PAR14 == 1 .And. lMsFil
					FO7->(DbSeek(xFilial("FO7",FLF->FLF_MSFIL) + FLF->FLF_TIPO + FLF->FLF_PRESTA))
				Else
					FO7->(DbSeek(xFilial("FO7") + FLF->FLF_TIPO + FLF->FLF_PRESTA))
				Endif
			Endif
			If MV_PAR03 == MVP03EMISSAO 
				
				If FO7->FO7_RECPAG == "R"
					dbSelectArea("SE1")
					SE1->(Dbsetorder(2))
					If SE1->(MsSeek(xFilial("SE1",FO7->FO7_FILIAL)+FO7->(FO7_CLIFOR+FO7_LOJA+FO7_PREFIX+FO7_TITULO+FO7_PARCEL+FO7_TIPO)))
						dDataBase := SE1->E1_EMISSAO
					EndIf
				Else
					DBSelectArea("SE2")
					SE2->(dbSetOrder(1))
					If SE2->(MsSeek(xFilial("SE2",FO7->FO7_FILIAL)+FO7->(FO7_PREFIX+FO7_TITULO+FO7_PARCEL+FO7_TIPO+FO7_CLIFOR+FO7_LOJA)))
						dDataBase := SE2->E2_EMISSAO
					EndIf	
				EndIf
			Endif
		Else
			DBSelectArea("FLN")
			FLN->(DbSetOrder(1))//FLN_FILIAL+FLN_TIPO+FLN_PRESTA+FLN_PARTIC+FLN_SEQ+FLN_TPAPR
			If MSSEEK(xFilial("FLN", FLF->FLF_FILIAL)+FLF->FLF_TIPO+ FLF->FLF_PRESTA+ FLF->FLF_PARTIC)
				While FLN->(!Eof()) .And. xFilial("FLN", FLF->FLF_FILIAL)+FLF->FLF_TIPO+ FLF->FLF_PRESTA+ FLF->FLF_PARTIC == FLN->(FLN_FILIAL+FLN_TIPO+FLN_PRESTA+FLN_PARTIC)
					If FLN->FLN_STATUS <> "2"
						FLN->(DbSkip())
					Else
						dDataBase := FLN->FLN_DTAPRO
						Exit
					EndIf		
				EndDo
			Endif
		EndIf

		If lPadraoCabec
			If lUsaFlag .and. lFlfLa
				aAdd(aFlagCTB,{"FLF_LA","S","FLF",FLF->(Recno()),0,0,0})
			EndIf
			
			aTabRecOri := { 'FLF', FLF->( RECNO() ) }
			
			nValLanc := DetProva(nHdlPrv,cPadraoCabec,"FINA370",cLoteCTB,,,,,,aCT5,,@aFlagCTB, aTabRecOri)
			nTotal += nValLanc
			nTotDoc += nValLanc
			If LanceiCtb // Vem do DetProva
				If !lUsaFlag .and. lFlfLa
					RecLock("FLF")
					FLF->FLF_LA := "S"
					MsUnlock( )
				EndIf
			ElseIf lUsaFlag
				If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == FLF->(Recno()) }))>0
					aFlagCTB := Adel(aFlagCTB,nPosReg)
					aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
				Endif
			EndIf
		Endif
		If lPadraoItem
			DbSelectArea("FLE")
			If MV_PAR14 == 1 .And. lMsFil
				cFilFLE := xFilial("FLE",FLF->FLF_MSFIL)
			Else
				cFilFLE := xFilial("FLE")
			Endif
			FLE->(DbSeek(cFilFLE + FLF->FLF_TIPO + FLF->FLF_PRESTA + FLF->FLF_PARTIC))
			While FLE->FLE_FILIAL == cFilFLE .And. FLE->FLE_TIPO == FLF->FLF_TIPO .And. FLE->FLE_PRESTA == FLF->FLF_PRESTA .And. FLE->FLE_PARTIC == FLF->FLF_PARTIC
				If !(FLE->FLE_LA == "S")
					If lUsaFlag
						aAdd(aFlagCTB,{"FLE_LA","S","FLE",FLE->(Recno()),0,0,0})
					EndIf
					aTabRecOri := { 'FLE', FLE->( RECNO() ) }
					
					nValLanc := DetProva(nHdlPrv,cPadraoItem,"FINA370",cLoteCTB,,,,,,aCT5,,@aFlagCTB, aTabRecOri)
					nTotal += nValLanc
					nTotDoc += nValLanc
					If LanceiCtb // Vem do DetProva
						If !lUsaFlag
							RecLock("FLE")
							FLE->FLE_LA := "S"
							MsUnlock( )
						EndIf
					ElseIf lUsaFlag
						If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == FLE->(Recno()) }))>0
							aFlagCTB := Adel(aFlagCTB,nPosReg)
							aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
						Endif
					EndIf
				Endif
				FLE->(DbSkip())
			Enddo
		Endif
		
		If !(MV_PAR12 == MVP12PERIODO) 
			If nTotDoc > 0
				Ca370Incl(cArquivo,@nHdlPrv,cLoteCtb,@aFlagCTB,,dDataBase)
			Endif
			nTotDoc := 0
			aFlagCTB := {}
		Endif
		
		DbSelectArea(cAliasFL)
		(cAliasFL)->(DbSkip())
	Enddo
	If Select(cAliasFL)>0
		DbSelectArea(cAliasFL)
		DbCloseArea()
		MsErase(cAliasFL)
	Endif
Endif


RestArea(aGetArea)
Return(lRet)

/*/{Protheus.doc} SchedDef
Uso - Execucao da rotina via Schedule.

Permite usar o botao Parametros da nova rotina de Schedule
para definir os parametros(SX1) que serao passados a rotina agendada.

@return  aParam
/*/
Static Function SchedDef(aEmp)
	Local aParam := {}

	aParam := {	"P"			,;	//Tipo R para relatorio P para processo
				"FIN370"	,;	//Nome do grupo de perguntas (SX1)
				Nil			,;	//cAlias (para Relatorio)
				Nil			,;	//aArray (para Relatorio)
				Nil			}	//Titulo (para Relatorio)

Return aParam

/*/{Protheus.doc} VldVerPad�
Valida a existencia de Lan놹mentos Padr?o.�
@author norbertom�
@since 13/09/2017�
@version 1.0�
@param aLstLP, array, Lista de Lan놹mentos Padr?o para validar na fun�?o VerPadrao().�
@return� lRet, logico, Verdadeiro se existir 1 ou mais Lps cadastrados e ativos, sen?o Falso.�
/*/�
Function VldVerPad(aLstLP)�
Local lRet := .T.�
Local nI := 0�
DEFAULT aLstLP := {}�
	
	For nI := 1 To Len(aLstLP)�
		lRet := VerPadrao(aLstLP[nI])�
		If lRet�
			Exit�
		EndIf�
	Next nI�

Return lRet

/*/{Protheus.doc} CTBClean

Limpa o objeto da temporarytable
 
@Author	Leonardo Castro
@since	08/02/2018
/*/
Static Function CTBClean()

If _oCTBAFIN <> Nil
	_oCTBAFIN:Delete()
	_oCTBAFIN := Nil
Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} CTBGrvFlag
Efetua valida寤es para a grava豫o do Flags 
@author  Norberto M de Melo
@since   23/02/201
@version P12
/*/
//-------------------------------------------------------------------
STATIC FUNCTION CTBGrvFlag(aIdFlag as Array)
	/**Declara豫o */
	LOCAL nI		as Numeric
	LOCAL aAreas	as Array
	LOCAL cTab		as Character
	LOCAL aFKRec	as Array

	/**Inicializa豫o */
	DEFAULT aIdFlag	:= {} 
	nI		:= 0
	aAreas	:= {}
	cTab	:= ''
	aFKRec	:= {}

	/**Implementa豫o */
	AADD(aAreas, GETAREA())

	IF !SE5->(EOF())
		CTBAddFlag(aFKRec)

		For nI := 1 To Len(aFKRec)
			(aFKRec[nI,3])->(dbGoTo(aFKRec[nI,4]))

			IF !(aFKRec[nI,3])->(EOF())
				Reclock(aFKRec[nI,3],.F.)
				&(aFKRec[nI,1]) := 'S'
				MsUnlock()
			EndIf
		Next nI
	ENDIF

	// restaura as areas utilizadas da �ltima para a primeira - Sistema UEPS ou LIFO
	FOR nI := LEN(aAreas) TO 1 Step -1
		RestArea(aAreas[nI])
	NEXT nI

RETURN NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} CTBAddFlag
Adiciona os recnos do relacionamento do registro SE5 atual e seus registros FKs
@author  Norberto M de Melo
@since   23/02/201
@version P12
/*/
//-------------------------------------------------------------------
STATIC FUNCTION CTBAddFlag(aCTBFlags as Array) as Array
/**Declara豫o */
	LOCAL aAreas		as Array
	LOCAL nI 			as Numeric
	LOCAL cKeyIDProc	as Character

/**Inicializa豫o */
	//DEFAULT aCTBFlags := {}
	aAreas		:= {}
	nI			:= 0
	cKeyIDProc	:= ''

/**Implementa豫o */
	AAdd(aAreas,GETAREA())

	// Adiciona o Recno do registro SE5 Atual 
	AAdd(aCTBFlags,{"E5_LA","S","SE5",SE5->(Recno()),0,0,0})

	// Inicia Pesquisa do relacionamento com as FKs
	If !EMPTY(SE5->(E5_TABORI+E5_IDORIG))
		AADD(aAreas,FKA->(GetArea()))
		dbSelectArea('FKA')
		FKA->(dbSetOrder(3))		//FKA_FILIAL+FKA_TABORI+FKA_IDORIG

		If FKA->(dbSeek(SE5->(E5_FILORIG+E5_TABORI+E5_IDORIG)))
			FKA->(dbSetOrder(2))	//FKA_FILIAL+FKA_IDPROC+FKA_IDORIG+FKA_TABORI
			cKeyIDProc := SE5->E5_FILORIG+FKA->FKA_IDPROC
			IF FKA->(DBSeek(cKeyIDProc))
				While !FKA->(EOF()) .AND. cKeyIDProc == FKA->(FKA_FILIAL+FKA_IDPROC)
					If SUBSTR(FKA->FKA_TABORI,1,2) <> 'FK'
						FKA->(DbSkip())
						Loop
					EndIf

					If ASCAN(aAreas,{|e|e[1]==FKA->FKA_TABORI}) == 0
						AADD(aAreas,(FKA->FKA_TABORI)->(GetArea()))
					EndIf

					dbSelectArea(FKA->FKA_TABORI)
					(FKA->FKA_TABORI)->(dbSetOrder(01))
					If (FKA->FKA_TABORI)->(dbSeek(SE5->E5_FILORIG+FKA->FKA_IDORIG))
						AAdd(aCTBFlags,{FKA->FKA_TABORI+"_LA","S",FKA->FKA_TABORI,(FKA->FKA_TABORI)->(Recno()),0,0,0})
					EndiF
					FKA->(DbSkip())
				EndDo
			EndIf
		Endif
	EndIf

	// restaura as areas utilizadas da �ltima para a primeira - Sistema UEPS ou LIFO
	For nI := LEN(aAreas) To 1 Step -1
		RestArea(aAreas[nI])
	Next nI
RETURN ACLONE(aCTBFlags)

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaTMP
Cria豫o de tabela Tempor�ria
@author  Norberto M de Melo
@since   06/03/2018
@version P12
/*/
//-------------------------------------------------------------------
STATIC FUNCTION CriaTMP(cAlias as Character, aStruSQL as Array, cChave as Character) as Character
	/**Declara豫o */
	LOCAL cTab as Character
	LOCAL aArea as Array

	/**Inicializa豫o */
	DEFAULT cAlias		:= ''
	DEFAULT aStruSQL	:= {}
	DEFAULT cChave		:= ''
	cTab	:= FINNextAlias()
	aArea	:= GetArea()

	/**Implementa豫o */
	If !EMPTY(cAlias) .AND. !EMPTY(aStruSQL)
		dbSelectArea(cAlias)
		(cAlias)->(dbSetOrder(1))
		If EMPTY(cChave)
			cChave := (cAlias)->(INDEXKEY())
		EndIf

		_oCTBAFIN := oFINTMPTBL():New(cTab)
		_oCTBAFIN:SetFields(aStruSQL)
		_oCTBAFIN:AddIndex('1',cChave)
		_oCTBAFIN:Create()
	EndIf

	RestArea(aArea)
Return cTab

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿌370FREEPR튍utor  쿘arcos S. Lobo      � Data �  06/26/06   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿗ibera registro alocado no semaforo de processamento.       볍�
굇�          �                                                            볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP Contabilizacao Off-Line Financeiro                      볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function A370FreeProc(aKeyProc)

Local cFile 	:= "CTB370"+AllTrim(cEmpAnt)
Local nER		:= 0
Local nI		:= NIL
Local lTRBEmpty := .F.

DEFAULT aKeyProc := NIL

If !( MsFile(cFile,,"TOPCONN") )
	Return
EndIf

If Select("SEM370") <= 0
	Return
EndIf

While !LockByName("FINA370LOCKPROC"+cEmpAnt,.T.,.T.,.T.)
    nER++
	If !_lBlind
		MsAguarde({|| Sleep(1000) }, STR0026+ALLTRIM(STR(nER)), STR0029)//"Semaforo de processamento... tentativa "#"Aguarde, arquivo sendo criado por outro usu�rio."
	Else
		Sleep(5000)		
	EndIf
	If nER > 5	/// A PARTIR DA QUINTA TENTATIVA
		If !_lBlind
			If Aviso(STR0028,STR0029,{STR0030,STR0034},2) == 2//"Gravacao de Semaforo de processamento."#"N�o foi possivel acesso exclusivo para gravar o semaforo de processamento."#"Repetir"#"Fechar"
				If Funname() == "FINA370" .and. !_lBlind
					oSelf:Savelog("ERRO",STR0031,STR0032+STR0033)	
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//쿢tilizacao da funcao ProcLogAtu para permitir a gravacao �
					//쿭o log no CV8 quando do uso da classe tNewProcess que    �
					//쿲rava o LOG no SXU (FNC 00000028259/2009)                �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
				Else	
					ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
				EndIf
				
				Return
			Else
				nER := 0
			EndIf		
		ElseIf nER >= 30
			If Funname() == "FINA370" .and. !_lBlind
				oSelf:Savelog("ERRO",STR0031,STR0032+STR0033)	
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//쿢tilizacao da funcao ProcLogAtu para permitir a gravacao �
				//쿭o log no CV8 quando do uso da classe tNewProcess que    �
				//쿲rava o LOG no SXU (FNC 00000028259/2009)                �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
			Else	
				ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
			EndIf
			Return
		EndIf
    EndIf
EndDo

If !EMPTY(aKeyProc)
	cEval := ''
	For nI := 1 To Len(aKeyProc)
		cEval += "SEM370->" + aKeyProc[nI][1] + " =='" + aKeyProc[nI][2] + "' "
		If nI < Len(aKeyProc)
			cEval += " .and. "
		EndIf
	Next nI
	bEval := {|| &cEval }
Else
	bEval := {|| .T.}
EndIf

dbSelectArea("SEM370")
SEM370->(dbGoTop())
While !SEM370->(Eof()) 
	If Eval(bEval) .and. SEM370->(RLock())
		Field->HORAF	:= Time()
		Field->DATAF	:= DTOS(Date())
		MsUnlock()
		RecLock("SEM370",.F.)
		SEM370->(dbDelete())
		MsUnlock()
	EndIf
	SEM370->(DBSKIP())
EndDo

SEM370->(DBCLOSEAREA())
dbUseArea(.T.,'TOPCONN',cFile,"SEM370",.T.,.F.)
lTRBEmpty := SEM370->(EOF())
SEM370->(DBCLOSEAREA())

If lTRBEmpty
	MSERASE(cFile,,"TOPCONN")
Endif

UnLockByName("FINA370LOCKPROC"+cEmpAnt,.T.,.T.,.T.)

Return

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇�袴袴袴袴袴佶袴袴袴袴藁袴袴袴錮袴袴袴袴袴袴袴袴袴袴箇袴袴錮袴袴袴袴袴袴敲굇
굇튡rograma  쿌370CanPro튍utor  쿘arcos S. Lobo      � Data �  06/26/06   볍�
굇勁袴袴袴袴曲袴袴袴袴袴姦袴袴袴鳩袴袴袴袴袴袴袴袴袴菰袴袴袴鳩袴袴袴袴袴袴묽�
굇튒esc.     쿎ria Semaforo de processamento e verIfica concorrencia com  볍�
굇�          쿫ase nos intervalos de parametros                           볍�
굇勁袴袴袴袴曲袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴묽�
굇튧so       � AP - Contabilizacao Off-Line Financeiro                    볍�
굇훤袴袴袴袴賈袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴袴선�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function A370CanProc(nCart, dDtVldDe, dDtVldAte, cFilDe, cFilAte,oSelf)
Local lRet		:= .F.
Local nEr		:= 0 
Local cFile		:= ""
Local cUserCTB	:= PADR('SCHED',15)

Default cFilDe := cFilAnt
Default cFilAte:= cFilAnt

If !IsBlind()
	cUserCTB := cUserName
EndIf

While !LockByName("FINA370LOCKPROC"+cEmpAnt,.T.,.T.,.T.)
    nER++
	If !_lBlind
		MsAguarde({|| Sleep(1000) }, STR0026+ALLTRIM(STR(nER)), STR0027) //"Semaforo de processamento... tentativa "#"Aguarde, arquivo sendo criado por outro usu�rio."
	Else
		Sleep(5000)		
	EndIf
	If nER > 5	/// A PARTIR DA QUINTA TENTATIVA
		If !_lBlind
			If Aviso(STR0028,STR0029,{STR0030,STR0034},2) == 2//"Cria豫o de Semaforo de processamento."#"N�o foi possivel acesso exclusivo para criar o semaforo de processamento."#"Repetir"#"Fechar"
				If Funname() == "FINA370" .and. !_lBlind
					oSelf:Savelog("ERRO",STR0031,STR0032+STR0033)	
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//쿢tilizacao da funcao ProcLogAtu para permitir a gravacao �
					//쿭o log no CV8 quando do uso da classe tNewProcess que    �
					//쿲rava o LOG no SXU (FNC 00000028259/2009)                �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					ProcLogAtu("ERRO",STR0031,STR0032+STR0033)						
				Else	
					ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
				EndIf	
				Return lRet
			Else
				nER := 0
			EndIf		
		ElseIf nER >= 30
			If Funname() == "FINA370" .and. !_lBlind
				oSelf:Savelog("ERRO",STR0031,STR0032+STR0033)	
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				//쿢tilizacao da funcao ProcLogAtu para permitir a gravacao �
				//쿭o log no CV8 quando do uso da classe tNewProcess que    �
				//쿲rava o LOG no SXU (FNC 00000028259/2009)                �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				ProcLogAtu("ERRO",STR0031,STR0032+STR0033)		
			Else	
				ProcLogAtu("ERRO",STR0031,STR0032+STR0033)	
			EndIf	
			Return lRet
		EndIf
    EndIf
EndDo

//MakeDir("\SEMAFORO\")
cFile := "CTB370"+AllTrim(cEmpAnt)

lCriaTrab := !(MsFile(cFile,,"TOPCONN"))

If !lCriaTrab
	If Select("SEM370") <= 0
		dbUseArea(.T.,'TOPCONN',cFile,"SEM370",.T.,.F.)
	EndIf
	If (lCriaTrab := VALTYPE(SEM370->DTDE) == 'D')
		SEM370->(DBCLOSEAREA())
		MSERASE(cFile,,"TOPCONN")
	Endif
EndIf

If lCriatrab
	aStruct  := {}
	AAdd( aStruct, { "FILDE"	, "C", Len( cFilAnt )	, 0 } )
	AAdd( aStruct, { "FILATE"	, "C", Len( cFilAnt )	, 0 } )
	AAdd( aStruct, { "DTDE"		, "C", 8 				, 0 } )
	AAdd( aStruct, { "DTATE"	, "C", 8 				, 0 } )
	AAdd( aStruct, { "CCART"	, "C", 1				, 0 } )
	AAdd( aStruct, { "CUSER"	, "C", Len( cUserCTB )	, 0 } )
	AAdd( aStruct, { "HORAI"	, "C", Len(Time())		, 0 } )
	AAdd( aStruct, { "DATAI"	, "C", 8				, 0 } )
	AAdd( aStruct, { "HORAF"	, "C", Len(Time())		, 0 } )
	AAdd( aStruct, { "DATAF"	, "C", 8				, 0 } )
	MsCreate( cFile , aStruct , 'TOPCONN' )
EndIf

If Select("SEM370") <= 0
	dbUseArea(.T.,'TOPCONN',cFile,"SEM370",.T.,.F.)
EndIf

dbSelectArea("SEM370")			
dbGoTop()

lSai		:= .F.
lRet1		:= .T.
lRet2		:= .T.
lRet3		:= .T.	

While !lSai .and. SEM370->(!Eof())
	        
	If cFilDe <= SEM370->FILDE .and. cFilAte >= SEM370->FILATE
		lRet1 := .F.
	ElseIf cFilDe >= SEM370->FILDE .and. cFilDe <= SEM370->FILATE
		lRet1 := .F.
	ElseIf cFilAte >= SEM370->FILDE .and. cFilAte <= SEM370->FILATE
		lRet1 := .F.
	ElseIf cFilDe > cFilAte
		lRet1 := .F.		
	EndIf	    

	If DTOS(dDtVldDe) <= SEM370->DTDE .and. DTOS(dDtVldAte) >= SEM370->DTATE
		lRet2 := .F.
	ElseIf DTOS(dDtVldDe) >= SEM370->DTDE .and. DTOS(dDtVldDe) <= SEM370->DTATE
		lRet2 := .F.
	ElseIf DTOS(dDtVldAte) >= SEM370->DTDE .and. DTOS(dDtVldAte) <= SEM370->DTATE
		lRet2 := .F.
	ElseIf DTOS(dDtVldDe) > DTOS(dDtVldAte)
		lRet2 := .F.		
	EndIf
	
	If nCart == 4 .or. SEM370->CCART == "4"
		lRet3 := .F.
	ElseIf Str(nCart,1) == SEM370->CCART	
		lRet3 := .F.	
	EndIf
	
	If !lRet1 .and. !lRet2 .and. !lRet3
		/// SE LOCALIZOU NO MESMO PERIODO E NAS MESMAS FILIAIS E MESMA CARTEIRA

		If SEM370->(RLock())			/// SE CONSEGUIR ALOCAR 	
			SEM370->(dbDelete())		/// NAO TEM CONCORRENCIA
			SEM370->(MsUnlock())
		Else		
			If !_lBlind
				Aviso(STR0031,STR0032+Alltrim(SEM370->CUSER)+" "+SEM370->HORAI+" "+STR0033,{STR0034},2) //"Aten豫o!"###"Este processo esta sendo utilizado com parametros conflitantes ( mesmo periodo ou carteiras ) por outro usu�rio ( "###" ) no momento. Verifique o per�odo e os parametros selecionados para o processamento ou tente novamente mais tarde."###"Fechar"
			EndIf
		
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
			//� Atualiza o log de processamento com o erro  �
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
				If Funname() == "FINA370" .and. !_lBlind
					If ValType(oSelf) == "O"
						oSelf:Savelog("ERRO",STR0031,STR0032+Alltrim(SEM370->CUSER)+STR0033)
					Endif
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//쿢tilizacao da funcao ProcLogAtu para permitir a gravacao �
					//쿭o log no CV8 quando do uso da classe tNewProcess que    �
					//쿲rava o LOG no SXU (FNC 00000028259/2009)                �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					ProcLogAtu("ERRO",STR0031,STR0032+AllTrim(SEM370->CUSER)+STR0033)	
				Else	
    				ProcLogAtu("ERRO",STR0031,STR0032+Alltrim(SEM370->CUSER)+STR0033)
    			EndIf	
			lSai		:= .T.
		EndIf
	EndIf
	SEM370->(dbSkip())
EndDo

If !lSai
	RecLock("SEM370",.T.)
	SEM370->FILDE	:= PADR(cFilDe,LEN(cFilAnt))
	SEM370->FILATE	:= PADR(cFilAte,LEN(cFilAnt))
	SEM370->DTDE	:= DTOS(dDtVldDe)
	SEM370->DTATE	:= DTOS(dDtVldAte)
	SEM370->CCART	:= Str(nCart,1)
	SEM370->CUSER	:= cUserCTB
	SEM370->HORAI	:= Time()
	SEM370->DATAI	:= DTOS(Date())
	MsUnlock()	
	RecLock("SEM370",.F.)	// DEIXA REGISTRO ALOCADO
	lRet := .T.				// PROCESSAMENTO PODE SER EFETUADO
EndIf

UnLockByName("FINA370LOCKPROC"+cEmpAnt,.T.,.T.,.T.)

Return lRet

/*
複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複複�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
굇旼컴컴컴컴컫컴컴컴컴컴쩡컴컴컴쩡컴컴컴컴컴컴컴컴컴컴컴쩡컴컴컫컴컴컴컴컴엽�
굇쿑un뇙o	 � FA370CONC� Autor � Vinicius Barreira	  � Data � 24/08/95	  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴좔컴컴컴좔컴컴컴컴컴컴컴컴컴컴컴좔컴컴컨컴컴컴컴컴눙�
굇쿏escri뇙o � Tela de Aviso de Falha na consist늧cia do SE5			  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿞intaxe	 � FA370CONC() 												  낢�
굇쳐컴컴컴컴컵컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴눙�
굇쿢so		 � SIGAFIN													  낢�
굇읕컴컴컴컴컨컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴袂�
굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇굇�
賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽賽�
*/
Static Function FA370CONC()
Local lRet		:= .F.
Local lUsaLog	:= SuperGetMv("MV_FINLOG",.T.,.F.)
Local cTexto	:= ""
Local cDate		:= DtoC(date())
Local cHour		:= substr(time(),1,5)
Local cPathLog	:= GetMv("MV_DIRDOC")
Local cLogArq	:= "Fina370Log.TXT"
Local cCaminho	:= cPathLog + cLogArq
Local lCtbafin	:= FwIsInCallStack("CTBAFIN")

If valtype("nTpLog") == "U"
	nTpLog := 1
EndIf

If !IsBlind()
	If lUsaLog
		cTexto += "*** "+cDate+" "+cHour+"--> "+ STR0025 + "PREF. + NUM + PARC + TIP"+chr(13)+chr(10) // "Dados do t�tulo:"
		cTexto += SE5->E5_PREFIXO+"-"+SE5->E5_NUMERO+"-"+SE5->E5_PARCELA+"-"+SE5->E5_TIPO + chr(13)+chr(10)
		cTexto += STR0035 + ": " + ALLTRIM(STR(SE5->(RECNO()))) +chr(13)+chr(10)    //"Registro SE5 ->"
		cTexto += "*** ---------------------- ***"

		If lCtbafin .Or. nTpLog == 1  
			FinLog( cCaminho, cTexto ) 
		ElseIf nTpLog == 2
			aAdd(aIncons, cTexto )
		EndIf

		lGerouTxt := .T.
		lRet := .T.

	Else
		lRet := MsgYesNo (STR0020+chr(13)+chr(10)+;
							chr(13)+chr(10)+ STR0024 + Iif(SE5->E5_RECPAG=="R",STR0014,STR0013)+;
							chr(13)+chr(10)+ STR0025 + SE5->E5_PREFIXO+"-"+SE5->E5_NUMERO+"-"+SE5->E5_PARCELA+"-"+SE5->E5_TIPO +;
							 chr(13)+chr(10)+ STR0035 + STR(SE5->(RECNO())),cCadastro)
	EndIf

Else 
	If lUsaLog
		cTexto += "*** "+cDate+" "+cHour+"--> "+ STR0025 + "PREF. + NUM + PARC + TIP"+chr(13)+chr(10) // "Dados do t�tulo:"
		cTexto += SE5->E5_PREFIXO+"-"+SE5->E5_NUMERO+"-"+SE5->E5_PARCELA+"-"+SE5->E5_TIPO + chr(13)+chr(10)
		cTexto += STR0035 + ": " + ALLTRIM(STR(SE5->(RECNO()))) +chr(13)+chr(10)    //"Registro SE5 ->"
		cTexto += "*** ---------------------- ***"

		If lCtbafin .Or. nTpLog == 1  
			FinLog( cCaminho, cTexto ) 
		ElseIf nTpLog == 2
			aAdd(aIncons, cTexto )
		EndIf
		
		lGerouTxt := .T.
		lRet := .T.
	EndIf
EndIf
Return lRet

//--------------------------------------------------------------------------
/*/{Protheus.doc} VldDtE5()
Condicional While para valida豫o se o a data do Movimento Bancario 
est� no range selecionado. 

@Param cAliasSe5 	Alias da tabela temporaria SE5
@Param cCampo 		Data Utilizada "E5_DATA","E5_DTDIGIT" ou "E5_DTDISPO"
@Param dPar05 		Pergunte F12 = ( mv_par05 - Data Fim)	
@Param dDataIni 	Data Inicial 

@author Luiz Henrique
@since 22/01/2020
@version 12.1.27
/*/
//---------------------------------------------------------------------------
Static Function VldDtE5(cAliasSe5,cCampo,dPar05,dDataIni)

	Local lRet 			:= .F.

	Default cAliasSe5 	:= "" 
	Default cCampo	  	:= ""
	Default dPar05		:= dDataBase
	Default dDataIni	:= dDataBase

	If !Empty(cAliasSe5) .AND. (cAliasSE5)->(!Eof()) 
		If (cAliasSE5)->E5_TIPODOC $ 'TR#TE' .OR. Empty((cAliasSE5)->(E5_TIPODOC+E5_TIPO)) 		
			If(cAliasSE5)->E5_DATA >= dDataIni .AND. (cAliasSE5)->E5_DATA <= dPar05
				lRet := .T.
			ElseIf (cAliasSE5)->(&cCampo) <= dPar05			
				lRet := .T.
			EndIf
		ElseIf (cAliasSE5)->(&cCampo) <= dPar05
			lRet := .T.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} CtbMSitCob()
Divide os registros selecionados de Transferencia de situa豫o de cobran�a 
multi-processamento (multi-threads).

@author Fernando Navarro 
@since 24/06/2020
@version 12.1.30

@param nNumProc, n�mero de processos
/*/
Static Function CtbMSitCob(nNumProc As Numeric) As Logical 

    Local aArea 		As Array
    Local aStruSQL		As Array
    Local aProcs 		As Array

    Local cChave		As Character
    Local cPerg 		As Character
    Local cRaizNome		As Character
    Local cTabJob		As Character
    Local cTabMult		As Character

    Local lRet			As Logical

    Local nTotalReg		As Numeric
    Local nX			As Numeric 

    aArea       := GetArea()
    lRet        := .T.
    nX          := 0
    aProcs      := {}
    cTabMult    := ""// Tabela fisica para o processamento multi thread
    nTotalReg   := 0
    cRaizNome   := 'CTBFINPROC'
    aStruSQL    := {}
    cTabJob     := "TRBFWI"
    cPerg       := "FIN370"
    cChave      := "FWI_FILIAL+DTOS(FWI_DTMOVI)+FWI_LANPAD+FWI_NUMBOR+FWI_SEQ"

    If !__lSchedule
        Pergunte(cPerg,.F.)
    EndIf
    
    //Monta o arquivo de trabalho
    cTabMult := CtbGrvSitC(@nTotalReg)
    
    If nTotalReg > 0
        
        aStruSQL := (cTabMult)->( DbStruct() )
        aProcs := CTBPrepFIN(@nNumProc,cTabMult,nTotalReg,"CTBFLAG",cRaizNome,cChave)
        
        If  nTotalReg >= nNumProc .And. nNumProc > 1 // MultiThread
            
            //Inicializa as Threads Transa豫o controlada nas Threads
            For nX := 1 to Len(aProcs)
                StartJob("JOBCTBSITC", GetEnvServer(), .F., cEmpAnt,cFilAnt,aProcs[nX][MARCA],aProcs[nX][ARQUIVO],"CTBFLAG",cTabMult,aStruSQL,cTabJob,aProcs[nX][VAR_STATUS],cChave,__cUserId,cUserName,cAcesso,cUsuario,_oCTBAFIN:GETREALNAME())
                Sleep(6000)
            Next nX
            
            //NAO RETIRAR A INSTRUCAO DO SLEEP
            //Esperar 30 segundos antes de monitorar para dar tempo das threads criar arquivo de semaforo
            Sleep(30000)
            //Realiza o controle das Threads
            lRet := FINMonitor(aProcs,4)
        ElseIf nNumProc == 1
            cTabJob	 := SelFINJob(NIL,cTabMult,aStruSQL,"CTBFLAG",aProcs[nNumProc][MARCA],cTabJob,cChave,_oCTBAFIN:GETREALNAME())
            lRet := FCTBSITCOB(,,,,cTabJob,.T.)
            If Select(cTabJob) > 0
                (cTabJob)->(dbCloseArea())
            EndIf
        EndIf
    EndIf
    
    If Select(cTabMult) > 0
        (cTabMult)->( dbCloseArea() )
    Endif

    MsErase(cTabMult)
    RestArea(aArea)

Return(lRet)


/*/{Protheus.doc} CtbGrvSitC()
Monta tempor�rio para processamento tanto multi-threado como individual com os registros de Transferencia de situa豫o de cobran�a para. 
Parametro s�o utilizados apenas no processamento multi-thread. 

@author Fernando Navarro 
@since 24/06/2020
@version 12.1.30

@param nTotalReg, Numero de registros a serem processados pelo processo multithread 
@param cChave,    Chave do indice que ser� criado na tabela temporaria / tamb�m ser� usado para segregrar os registro no processo multithread. 
/*/
Static Function CtbGrvSitC(nTotalReg As Numeric) As String 

    Local aStruFWI  As Array 
    Local aStruSE1  As Array 
    Local aStruSQL  As Array 

    Local cCampos    As Character
    Local cCamposIns As Character
    Local cChave     As Character 
    Local cInsert    As Character
    Local cQuery     As Character
    Local cTabela    As Character
    Local cUpdate    As Character

    Default nTotalReg  := 0 

    cChave     := "FWI_FILIAL+FWI_DTMOVI+FWI_LANPAD+FWI_NUMBOR+FWI_SEQ+FWI_PREFIX+FWI_NUMERO+FWI_PARCEL+FWI_TIPO+FWI_CLIENT+FWI_LOJA"

    aStruSE1 := SE1->(DbStruct())
    aStruFWI := FWI->(DbStruct())
    aStruSQL := {}
    cCampos  := ""

    aEval(aStruFWI,{|Campos| AAdd(aStruSQL, Campos)})
    //aEval(aStruSE1,{|Campos| AAdd(aStruSQL, Campos)})

    aEval(aStruSQL,{|Campos,i| cCampos += IF(i==1,'',',')+AllTrim(Campos[1])})

    AAdd(aStruSQL,{"SE1RECNO"    ,"N",15,00})
    AAdd(aStruSQL,{"FWIRECNO"    ,"N",15,00})
    AADD(aStruSQL,{"CTBFLAG"     ,"C",02,00})
    AADD(aStruSQL,{"SUMABATRE"   ,"C",01,00})

    cCamposIns := cCampos
    cCamposIns += ",SE1RECNO " 
    cCamposIns += ",FWIRECNO " 
    cCamposIns += ",CTBFLAG " 
    cCamposIns += ",SUMABATRE " 

    If !_MSSQL7
        cCamposIns += ", R_E_C_N_O_"
    EndIf 

    // Cria tabela temporaria
    cTabela := CriaTMP('FWI',aStruSQL,cChave) 

    cQuery := "SELECT " + cCampos 
    cQuery += ",SE1.R_E_C_N_O_ AS SE1RECNO "
    cQuery += ",FWI.R_E_C_N_O_ AS FWIRECNO "
    cQuery += "," + _cSpaceMark + " AS CTBFLAG " 
    cQuery += ", 'N' AS SUMABATRE "
    If !_MSSQL7
        cQuery += ", FWI.R_E_C_N_O_"
    EndIf 

    cQuery += " FROM " + RetSqlName("FWI") + " FWI "
    cQuery += " INNER JOIN " + RetSqlName("SE1") + " SE1 "
    cQuery += " ON E1_FILORIG = FWI_FILORI "
    cQuery += " AND E1_PREFIXO = FWI_PREFIX "
    cQuery += " AND E1_NUM = FWI_NUMERO "
    cQuery += " AND E1_PARCELA = FWI_PARCEL "
    cQuery += " AND E1_TIPO = FWI_TIPO "
    cQuery += " AND E1_CLIENTE = FWI_CLIENT "
    cQuery += " AND E1_LOJA = FWI_LOJA "
    cQuery += " AND SE1.D_E_L_E_T_ = ' ' "
    cQuery += " WHERE FWI.D_E_L_E_T_ = ' ' "
    cQuery += " AND   FWI_LA <> 'S' "
    If MV_PAR14 == 1
        cQuery += " AND   FWI_FILORI = '" + cFilAnt + "' "
    Else 
        cQuery += " AND   FWI_FILIAL = '" + FwXFilial("FWI") + "' "
    EndIf
    cQuery += " AND FWI_DTMOVI BETWEEN '"+DTOS(mv_par04)+"' AND '"+DTOS(mv_par05)+"'"
    // Avalia e buscas apenas os LPs Configurados
    cQuery += " AND EXISTS (SELECT 'LPADRAO' FROM " + RetSqlName("CT5") + " CT5 "
    cQuery += " WHERE CT5.D_E_L_E_T_ = ' ' "
    cQuery += " AND CT5_FILIAL = '" + FwXFilial("CT5") + "' "
    cQuery += " AND CT5_LANPAD = FWI_LANPAD "
    cQuery += " AND CT5_STATUS IN ( '1', ' ' ) ) "

    If !Empty(cChave) 
        cQuery += " ORDER BY " + SqlOrder(cChave)
    EndIf

    cInsert := " INSERT "
    If _cSGBD == "ORACLE"
        cInsert += " /*+ APPEND */ "
    EndIf
    cInsert += " INTO " + _oCTBAFIN:GETREALNAME() + " ("+cCamposIns+") " + cQuery

    If TcSqlExec(cInsert) <> 0
        If __lConoutR
            ConoutR(STR0061 + " Ref: CtbGrvSitC ") //"Erro no Insert"
        Endif
    EndIf

    // Identifica poss�veis candidatos a terem abatimentos, afim de melhorar a performance.
    cUpdate := " UPDATE " + _oCTBAFIN:GETREALNAME() + " SET SUMABATRE = 'S' "
    cUpdate += " WHERE R_E_C_N_O_ > 0 "
    cUpdate += " AND EXISTS (SELECT 'TITPAI' FROM " + RetSqlName("SE1") + " SE1 "
    cUpdate += " WHERE E1_FILIAL = FWI_FILIAL AND E1_PREFIXO = FWI_PREFIX "
    cUpdate += " AND E1_NUM = FWI_NUMERO AND E1_PARCELA = FWI_PARCEL "
    cUpdate += " AND E1_TITPAI <> ' ' AND SE1.D_E_L_E_T_ = ' ' ) "

    If TcSqlExec(cUpdate) <> 0
        If __lConoutR
            ConoutR(STR0062 + " Ref: CtbGrvSitC ") //"N�o Foi poss�vel efetuar o UPDATE na Tabela Tempor�ria"
        Endif
    EndIf

    // Obtem a Contagem de Registros a processar.
    cQuery := "SELECT COUNT(*) NREGS "
    cQuery += "  FROM " + _oCTBAFIN:GETREALNAME() + " TAB "
    cQuery := ChangeQuery( cQuery )

    nTotalReg := MpSysExecScalar(cQuery, "NREGS")

    (cTabela)->(DbGotop())

Return cTabela

/*/{Protheus.doc} JobCtbSitC()
Executa o job para os registros de Transferencia de situa豫o de cobran�a.

@author Fernando Navarro 
@since 24/06/2020
@version 12.1.30
/*/
Function JobCtbSitC(cEmpX As Character, cFilX As Character, cMarca As Character, cFileLck As Character,;
                    cCpoFlag As Character, cTabMaster As Character,aStructTab As Array, cTabJob As Character,;
                    cVarStatus As Character, cChave As Character,cXUserId  As Character,;
                    cXUserName As Character, cXAcesso As Character, cXUsuario As Character, cFWTMP As Character)

    Local lRet      As Logical 
    Local nHandle   As Numeric

    Private lMsErroAuto 
    Private lMsHelpAuto 
    Private lAutoErrNoFile 

    //Abre o arquivo de Lock parao controle externo das threads
    nHandle := FINLock(cFileLck)

    // STATUS 1 - Iniciando execucao do Job
    PutGlbValue(cVarStatus,stThrStart)

    //Seta job para nao consumir licensas
    RpcSetType(3)
    RpcClearEnv()
    // Seta job para empresa filial desejada
    RpcSetEnv( cEmpX,cFilX,,,,,)

    // STATUS 2 - Conexao efetuada com sucesso
    PutGlbValue(cVarStatus,stThrConnect)

    //Set o usu�rio para buscar as perguntas do profile
    lMsErroAuto := .F.
    lMsHelpAuto := .T. 
    lAutoErrNoFile := .T.

    __cUserId := cXUserId 
    cUserName := cXUserName
    cAcesso   := cXAcesso
    cUsuario  := cXUsuario

    cTabJob	 := SelFINJob(cVarStatus,cTabMaster,aStructTab,cCpoFlag,cMarca,cTabJob,cChave,cFWTMP)

    // Realiza o processamento
    lRet := FCTBSITCOB(,,,,cTabJob,.T.)

    JOBFINEnd(cVarStatus,nHandle,lRet)

    If Select(cTabJob) > 0
        (cTabJob)->(dbCloseArea())
    EndIf

Return

/*/{Protheus.doc} FCTBSITCOB()
Gera Lan�amento contabil off line Transferencia de situa豫o de cobran�a.

@author Fernando Navarro 
@since 24/06/2020
@version 12.1.30
/*/

Function FCTBSITCOB(nHdlPrv As Numeric, cArquivo As Character, aFlagCTB As Array,;
                    nTotal As Numeric, cAliasFWI As Character, lMultThr As Logical) As Logical

    Local aCT5          As Array
    Local aGetArea      As Array
    Local aTabRecOri    As Array   // aTabRecOri[1]-> Tabela Origem ; aTabRecOri[2]-> RecNo

    Local cAlias        As Character
    Local cChaveBor     As Character
    Local cQuery        As Character

    Local nDescEst      As Numeric 
    Local nPosReg       As Numeric
    Local nTotDoc       As Numeric
    Local nValCred      As Numeric
    Local nValIof       As Numeric
    Local nValLanc      As Numeric
    Local nTotAbat      As Numeric

    Private oEstFKAFK5  As Object

    Default nHdlPrv		:= 0
    Default nTotal		:= 0
    Default cArquivo	:= ""
    Default aFlagCTB 	:= {}
    Default cAliasFWI	:= ""
    Default lMultThr	:= .F.

    cLote       := If(cLote=NIL,LoteCont("FIN"),cLote)
    lCabecalho  := If(lCabecalho=NIL,.F.,lCabecalho)

    If !__lSchedule
        Pergunte("FIN370",.F.)
    EndIf

    STRLCTPAD   := ""     // Disponibiliza a situacao anterior para ser utilizada no LP
    VALOR       := 0      // para contabilizar o total descontado (Private)
    IOF         := 0      // Valor da taxa IOF calculada
    VALOR2      := 0      // Saldo dos titulo para contabilizacao da diferenca
    ABATIMENTO	:= 0      // Valor do abatimento dos titulos no bordero 
    VAR_IXB     := ""     // Dados do banco anterior a transferencia 
    PIS         := 0      // Valores dos impostos para FINA061
    COFINS      := 0      // Valores dos impostos para FINA061
    CSLL        := 0      // Valores dos impostos para FINA061

    aGetArea    := GetArea()
    nTotDoc     := 0
    nValLanc    := 0
    nValCred    := 0 
    nValIof     := 0 
    nTotAbat    := 0 
    nPosReg     := 0
    cQuery      := ""
    cAlias      := ""
    cChaveBor   := ""
    aTabRecOri  := {'',0}	// aTabRecOri[1]-> Tabela Origem ; aTabRecOri[2]-> RecNo
    aCT5        := {}

    FWI->(DbSetOrder(1))

    If !lCabecalho
        a370Cabecalho(@nHdlPrv,@cArquivo)
    Endif
    
    If lMultThr .And. Select(cAliasFWI) > 0
        cAlias := cAliasFWI
    Else
        cAlias := CtbGrvSitC()
    Endif

    While !((cAlias)->(Eof()))
        FWI->(DbGoTo((cAlias)->FWIRECNO))
        SE1->(DbGoTo((cAlias)->SE1RECNO))

        dDataBase := FWI->FWI_DTMOVI 

        // Banco vazio, contabiliza anterior
        If !Empty(FWI->FWI_BANCO)
            SA6->(DbSetOrder(1))
            SA6->(DbSeek(FwxFilial("SA6",FWI->FWI_FILORI)+FWI->(FWI_BANCO+FWI_AGENCI+FWI_CONTA)))
        Else
            SA6->(DbSetOrder(1))
            SA6->(DbSeek(FwxFilial("SA6",FWI->FWI_FILORI)+FWI->(FWI_BCOANT+FWI_AGEANT+FWI_CONANT)))
        EndIf

        SEA->(DbSetOrder(4)) // EA_FILORIG+EA_NUMBOR+EA_CART+EA_PREFIXO+EA_NUM+EA_PARCELA+EA_TIPO+EA_FORNECE+EA_LOJA
        SEA->(DbSeek(FWI->(FWI_FILORI+FWI_NUMBOR+"R"+FWI_PREFIX+FWI_NUMERO+FWI_PARCEL+FWI_TIPO+FWI_CLIENT+FWI_LOJA)))

        If cChaveBor <> FWI->FWI_FILIAL+FWI->FWI_SEQ+FWI->FWI_NUMBOR+FWI->FWI_IDMOV
            cChaveBor := FWI->FWI_FILIAL+FWI->FWI_SEQ+FWI->FWI_NUMBOR+FWI->FWI_IDMOV
            nValCred := 0
            nValIof  := 0
            nTotAbat := 0
        EndIf 

        IF Empty(FWI->FWI_NUMBOR)
            cChaveBor := ""
            PIS         := 0
            COFINS      := 0
            CSLL        := 0
        Else
            nValCred += FWI->FWI_VALOR
            nTotAbat += IIF((cAlias)->SUMABATRE == 'S', SumAbatRec(FWI->FWI_PREFIX,FWI->FWI_NUMERO,FWI->FWI_PARCEL, SE1->E1_MOEDA,"S",FWI->FWI_DTMOVI) , 0) 
            If lPCCBaixa 
                PIS      := SE1->E1_PIS
                COFINS   := SE1->E1_COFINS
                CSLL     := SE1->E1_CSLL 
            EndIf 
        EndIf 

        If !lCabecalho 
            a370Cabecalho(@nHdlPrv,@cArquivo)
        Endif

        If lUsaFlag 
            aAdd(aFlagCTB,{"FWI_LA","S","FWI",FWI->(Recno()),0,0,0})
        EndIf

        aTabRecOri := { 'FWI', FWI->( RECNO() ) }

        nValLanc := DetProva(nHdlPrv,FWI->FWI_LANPAD,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB, aTabRecOri)
        nTotal   += nValLanc
        nTotDoc  += nValLanc
        If LanceiCtb // Vem do DetProva
            If !lUsaFlag
                RecLock("FWI")
                FWI->FWI_LA := "S"
                MsUnlock( )
            EndIf
        ElseIf lUsaFlag
            If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == FWI->(Recno()) }))>0
                aFlagCTB := Adel(aFlagCTB,nPosReg)
                aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
            Endif
        EndIf

        If Empty(cChaveBor)
            // Movimento de tranferencia (Situacao cobranca - Descontada)
            If !Empty(FWI->FWI_IDMOV) 

                SE1->(DbGoBottom())
                SE1->(DbSkip())

                SE5->(DbSetOrder(21)) // E5_FILIAL+E5_IDORIG+E5_TIPODOC
                FK5->(DbSetOrder(1))  // FK5_FILIAL+FK5_IDMOV
                If SE5->(DbSeek(FWxFilial("SE5",FWI->FWI_FILORI)+FWI->FWI_IDMOV)) .And. ;
                    FK5->(DbSeek(FWxFilial("FK5",FWI->FWI_FILORI)+FWI->FWI_IDMOV))

                    STRLCTPAD   := FWI->FWI_SITUAC
                    VALOR       := FWI->FWI_VALOR - FWI->FWI_DESCON - FWI->FWI_IOF
                    IOF         := FWI->FWI_IOF
                    VALOR2      := FWI->FWI_VALOR
                    ABATIMENTO  := 0
                    VAR_IXB     := FWI->FWI_BCOANT

                    If lUsaFlag 
                        aAdd(aFlagCTB,{"E5_LA","S","SE5",SE5->(Recno()),0,0,0})
                        aAdd(aFlagCTB,{"FK5_LA","S","FK5",FK5->(Recno()),0,0,0})
                    EndIf

                    nValLanc := DetProva(nHdlPrv,FWI->FWI_LANPAD,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB, aTabRecOri)
                    nTotal   += nValLanc
                    nTotDoc  += nValLanc

                    If LanceiCtb // Vem do DetProva
                        If !lUsaFlag
                            RecLock("SE5")
                            SE5->E5_LA := "S"
                            MsUnlock()

                            RecLock("FK5")
                            FK5->FK5_LA := 'S'
                            MsUnlock()
                        EndIf
                    ElseIf lUsaFlag
                        If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE5->(Recno()) }))>0
                            aFlagCTB := Adel(aFlagCTB,nPosReg)
                            aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
                        Endif
                    EndIf

                    // Zera as variaveis cont�beis para o proximo lan�amento.
                    STRLCTPAD   := ""
                    VALOR       := 0 
                    IOF         := 0 
                    VALOR2      := 0 
                    ABATIMENTO	:= 0 

                    SE5->(DbGoBottom())
                    SE5->(DbSkip())

                    FK5->(DbGoBottom())
                    FK5->(DbSkip())

                EndIf 
            EndIf
            If (MV_PAR12 == MVP12DOCUMENTO) 
                If nTotDoc > 0
                    Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataBase)
                Endif
                nTotDoc := 0
                aFlagCTB := {}
            Endif
        Endif

        (cAlias)->(DbSkip())

        If !Empty(cChaveBor)
            //Movimento do bordero (Situacao cobranca - Descontada)
            If cChaveBor <> (cAlias)->(FWI_FILIAL+FWI_SEQ+FWI_NUMBOR+FWI_IDMOV)
                SE1->(DbGoBottom())
                SE1->(DbSkip())
                nDescEst := 0 
                If FWI->FWI_LANPAD == "554"
                    cEstFKAFK5 := EstFKAFK5(FWI->FWI_IDMOV)
                    FK6->(DbSetOrder(2)) // // FK6_FILIAL+FK6_IDMOV
                    If FK6->(DbSeek(FWxFilial("FK6",FWI->FWI_FILORI)+cEstFKAFK5))
                        nDescEst := FK6->FK6_VALMOV
                    EndIf 
                Else
                    cEstFKAFK5 := FWI->FWI_IDMOV
                    nDescEst   := FWI->FWI_DESCON
                EndIf

                SE5->(DbSetOrder(21)) // E5_FILIAL+E5_IDORIG+E5_TIPODOC
                FK5->(DbSetOrder(1))  // FK5_FILIAL+FK5_IDMOV
                If SE5->(DbSeek(FWxFilial("SE5",FWI->FWI_FILORI)+cEstFKAFK5)) .And. ;
                    FK5->(DbSeek(FWxFilial("FK5",FWI->FWI_FILORI)+cEstFKAFK5))

                    STRLCTPAD   := FWI->FWI_SITUAC
                    VALOR       := FK5->FK5_VALOR
                    IOF         := FWI->FWI_IOF
                    VALOR2      := nValCred // FK5->FK5_VALOR + nDescEst + FWI->FWI_IOF
                    ABATIMENTO  := nTotAbat // Total dos abatimentos dos titulos. 
                    VAR_IXB     := FWI->FWI_BCOANT

                    If lUsaFlag 
                        aAdd(aFlagCTB,{"E5_LA","S","SE5",SE5->(Recno()),0,0,0})
                        aAdd(aFlagCTB,{"FK5_LA","S","FK5",FK5->(Recno()),0,0,0})
                    EndIf

                    nValLanc := DetProva(nHdlPrv,FWI->FWI_LANPAD,"FINA370",cLote,,,,,,aCT5,,@aFlagCTB, aTabRecOri)
                    nTotal   += nValLanc
                    nTotDoc  += nValLanc

                    If LanceiCtb // Vem do DetProva
                        If !lUsaFlag
                            RecLock("SE5")
                            SE5->E5_LA := "S"
                            MsUnlock()

                            RecLock("FK5")
                            FK5->FK5_LA := 'S'
                            MsUnlock()
                        EndIf
                    ElseIf lUsaFlag
                        If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == SE5->(Recno()) }))>0
                            aFlagCTB := Adel(aFlagCTB,nPosReg)
                            aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
                        Endif
                    EndIf

                    // Zera as variaveis cont�beis para o proximo lan�amento.
                    STRLCTPAD   := ""
                    VALOR       := 0 
                    IOF         := 0 
                    VALOR2      := 0 
                    ABATIMENTO	:= 0 

                    SE5->(DbGoBottom())
                    SE5->(DbSkip())
                EndIf 

                If (MV_PAR12 == MVP12DOCUMENTO) 
                    If nTotDoc > 0
                        Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataBase)
                    Endif
                    nTotDoc := 0
                    aFlagCTB := {}
                Endif
            EndIf
        EndIf 

    Enddo

    If (MV_PAR12 == MVP12PROCESSO) 
        If nTotDoc > 0
            Ca370Incl(cArquivo,@nHdlPrv,cLote,@aFlagCTB,,dDataBase)
        Endif
        nTotDoc := 0
        aFlagCTB := {}
    Endif

    If !lMultThr .and. Select(cAlias)>0
        DbSelectArea(cAlias)
        DbCloseArea()
        MsErase(cAlias)
    Endif

    RestArea(aGetArea)

    If oEstFKAFK5 <> Nil 
        oEstFKAFK5:Destroy()
        oEstFKAFK5:= Nil 
    EndIf

Return .T. 

/*/{Protheus.doc} EstFKAFK5()
Posiciona a FK5 de estorno do movimento de cancelamento do Bordero Contas a Receber. 

@author Fernando Navarro 
@since 06/07/2020
@version 12.1.30
/*/

Static Function EstFKAFK5(cIdMov As Character) As Logical 

    Local cQuery    As Character 

    Local cIDEstFK5 As Character

    cIDEstFK5 := " " 

    If oEstFKAFK5 == Nil 

        cQuery := " SELECT FKA_IDORIG ESTFKAFK5 FROM " + RetSQLName("FKA") + " FKA " 
        cQuery += " INNER JOIN " + RetSQLName("FK5") + " FK5 ON FKA_IDORIG = FK5_IDMOV " 
        cQuery += " AND FKA_FILIAL = FK5_FILIAL "
        cQuery += " WHERE FKA_IDPROC IN ( "
        cQuery += " SELECT FKA_IDPROC FROM " + RetSQLName("FKA") + " AUX " 
        cQuery += " WHERE AUX.FKA_IDORIG = ? "
        cQuery += " AND   AUX.FKA_TABORI = 'FK5' " 
        cQuery += " AND   AUX.D_E_L_E_T_ = ' ' " 
        cQuery += " ) "
        cQuery += " AND  FKA.FKA_IDORIG <> ? " 
        cQuery += " AND  FKA.D_E_L_E_T_ = ' ' " 
        cQuery += " AND  FKA.FKA_TABORI = 'FK5' "

        cQuery := ChangeQuery(cQuery)

        oEstFKAFK5 := FWPreparedStatement():New(cQuery)

    Endif 

    oEstFKAFK5:SetString(1,cIdMov)
    oEstFKAFK5:SetString(2,cIdMov)

    cQuery     := oEstFKAFK5:GetFixQuery()
    cIDEstFK5  := MpSysExecScalar(cQuery,"ESTFKAFK5")

Return cIDEstFK5 

/*/{Protheus.doc} QueryTroco
Fun豫o para montar a query para buscar os registros de troco
que ainda n�o foram contabilizados 

@author pedro.alencar
@since 15/06/2020
@version 1.0
@type static function

@param dDataIni, date, Data inicial do per�odo a ser filtrado
@param dDataFim, date, Data final do per�odo a ser filtrado
@return cRet, Alias do resultset com os dados dos registros de troco n�o contabilizados
/*/
Static Function QueryTroco( cFilQry As Char, dDataIni As Date, dDataFim As Date ) As Char
	Local cRet As Char
	Local cQuery As Char
	Local cAliasTroc As Char
	Local aCamposQry As Array
	Local aAuxTamSX3 As Array
	Local nI As Numeric
	Default cFilQry := ""
	Default dDataIni := CTOD("//")
	Default dDataFim := CTOD("//")
	
	aCamposQry := {}
	aAuxTamSX3 := TAMSX3("FK5_DATA")
	AADD( aCamposQry, {"FK5_DATA", "D", aAuxTamSX3[1], aAuxTamSX3[2]} )
	
	cRet := ""
	If __oQryTroc == Nil
		cQuery := " SELECT R_E_C_N_O_, "
		
		For nI := 1 To Len(aCamposQry)
			cQuery += aCamposQry[nI,1]
			If nI < Len(aCamposQry)
				cQuery += ','
			EndIf
		Next nI

		cQuery += " FROM " + RetSQLName("FK5") + " FK5 "
		cQuery += " WHERE "
		cQuery +=		" FK5_FILORI = ? "
		cQuery +=		" AND FK5_DATA BETWEEN '" + DTOS(dDataIni) + "' AND '" + DTOS(dDataFim) + "' "
		cQuery +=		" AND FK5_MOEDA = 'TC' "
		cQuery +=		" AND FK5_TPDOC IN ('VL', 'ES') "
		cQuery +=		" AND FK5_LA = ' ' "
		cQuery +=		" AND FK5.D_E_L_E_T_ = ' ' "
		cQuery += " ORDER BY FK5_FILIAL, FK5_DATA"

		cQuery := ChangeQuery(cQuery)
		__oQryTroc := FWPreparedStatement():New(cQuery)
	EndIf
	__oQryTroc:SetString(1, cFilQry)

	cQuery := __oQryTroc:GetFixQuery()
	cAliasTroc := MpSysOpenQuery(cQuery,,aCamposQry)
	
	If (cAliasTroc)->( !EOF() )
		cRet := cAliasTroc
	Else
		(cAliasTroc)->( dbCloseArea() )
	Endif
	
	FwFreeArray(aAuxTamSX3)
	FwFreeArray(aCamposQry)
Return cRet
