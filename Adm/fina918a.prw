#Include "FINA918A.ch" 
#Include "PROTHEUS.CH"
#Include "FWMVCDEF.CH"
#Include "MSMGADD.CH"

#DEFINE TOT_DATA		1
#DEFINE TOT_CONC		2
#DEFINE TOT_QTDCONC		3
#DEFINE TOT_CPAR		4
#DEFINE TOT_QTDCPAR		5
#DEFINE TOT_CMAN		6
#DEFINE TOT_QTDCMAN		7
#DEFINE TOT_CNAO		8
#DEFINE TOT_QTDCNAO		9
#DEFINE TOT_DIV			10
#DEFINE TOT_QTDDIV		11
#DEFINE TOT_GERAL		12
#DEFINE _CRLF			Chr(13)+Chr(10)

#Define BMPCAMPO        "BMPCPO.PNG"
#Define BMPSAIR         "FINAL.PNG"
#Define BMPVISUAL       "BMPVISUAL.PNG"
#Define BMPCANCELAR 	"CANCEL.PNG"
#Define BMPLEGEND		"NG_ICO_LEGENDA.PNG"

Static nCodTimeOut		:= 90  

Static lPontoF			:= ExistBlock("FINA910F")		//Variavel que verifica se ponto de entrada esta' compilado no ambiente

Static __nThreads 
Static __nLoteThr 
Static __lProcDocTEF
Static __lDefTop		:= NIL
Static __lConoutR		:= FindFunction("CONOUTR")
Static __aBancos		:= {}
Static __lDocTef		:= FieldPos("FIF_NUCOMP") > 0
Static __nFldPar		:= 0
Static __aSelFil	    := {}
Static __cVarComb       := ""
Static __aVarComb       := {}

Static nTamBanco
Static nTamAgencia
Static nTamCC
Static nTamCheque
Static nTamNatureza

Static nTamParc
Static nTamParc2
Static nTamNSUTEF
Static nTamDOCTEF

Static lMEP
Static lTamParc
Static lA6MSBLQL

Static nSelFil			:= 2
Static cFilIni			:= ""
Static cFilFim			:= ""
Static dDataCredI		:= cTod("")	//Data de credito inicial que a administradora credita o valor para a empresa
Static dDataCredF		:= cTod("")	//Data de credito final que a administradora credita o valor para a empresa
Static cNSUIni			:= ""
Static cNSUFim			:= ""
Static cConcilia		:= ""		//Tipos de Baixa: 1- Baixa individual / 2-Baixa por lote
Static nTpProc			:= 1
Static nQtdDias			:= 0		//O numero de dias anteriores ao da data de cr�dito que sera' utilizada como referencia de pesquisa nos titulos
Static nMargem			:= 0		//Parametro utilizado para que titulos que estao com valores a menor no SITEF possam entrar na pasta de conciliados mediante tolerancia em percentual informada
Static cAdmFinanIni		:= ""		//Codigo Inicial da Administradora Financeira que esta' efetuando o pagamento para a empresa
Static cAdmFinanFim		:= ""
Static lUseFIFDtCred	:= .F.
Static nVldNSE			:= 1

Static lFifRecSE1
Static lUsaMep        	:= SuperGetMv("MV_USAMEP",.T.,.F.)
Static _oFINA910A					//Objeto para receber comandos da classe FwTemporaryTable
Static aRetThread		:= {}
Static oConc			:= Nil	
Static __oF918ADM		:= NIL		//Objeto para manipula��o da consulta especifica
Static __cAdmFin		:= ""
Static __lSOFEX			:= .T.
Static __cF14FilAnt 	:= ""

Static __lLJGERTX		:= SuperGetMv( "MV_LJGERTX" , .T. , .F. )
Static __aOrd           := {}
Static __cGuia			:= ""
Static __nCOL			:= 0
Static __lBxCnab		:= SuperGetMv( "MV_BXCNAB" , .T. , 'N' ) == 'S'
Static __cPicSALD       := PesqPict("SE1","E1_SALDO")
Static __cPicVL         := PesqPict("FIF",Iif(__lLJGERTX,"FIF_VLBRUT","FIF_VLLIQ"))

//---------------------------------------------------------------------------
/*/{Protheus.doc} FINA918A
Rotina que efetua a conciliacao entre os dados recebidos pelo arquivo do 
SITFEF e os dados do Contas a Receber

@type Function
@author Rafael Rosa da Silva
@since 05/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Function FINA918A()
Local aButtons			:= {}											//Variavel para a inclusao de botoes na EnchoiceBar
Local aHeadTot			:= {}											//Array que guarda os nomes dos campos que aparecerao no Folder Totais
Local aHeader    	 	:= {}											//Array que guarda os nomes dos campos que aparecerao nos Folders Conciliadas, Conc. Parcialmente e N�o Conciliadas
Local aHeadIndic 		:= {}											//Array que guarda os nomes dos campos que aparecerao no Rodape Conciliados Parcialmente
Local aHeadMan			:= {}											//Array que guarda os nomes dos campos que aparecerao no Folder Conc. Manualmente
Local aHeadSE1			:= {}											//Array que guarda os campos padroes da tabela SE1
Local aColsAux			:= {}											//Array auxiliar que guarda os valores padroes
Local aConc				:= {}											//Array que contem todos os registros que estao disponiveis no Folder Conciliar
Local aConcPar			:= {}											//Array que contem todos os registros que estao disponiveis no Folder Conc. Parcialmente
Local aConcMan			:= {}											//Array que contem todos os registros que estao disponiveis no Folder Conc. Manualmente
Local aNaoConc			:= {}											//Array que contem todos os registros que estao disponiveis no Folder Nao Conciliadas
Local aDiverg			:= {}											//Array que contem todos os registros que estao disponiveis no Folder Divergentes
Local aTitulos			:= {}											//Array que contem todos os titulos que se assemelham aos registros do Folder Nao Conciliadas
Local aTotais			:= {}											//Array que contem todos os registros que estao disponiveis no Folder Totais
Local aFolder			:= {}											//Nome dos Folders
Local aIndic			:= {}											//Array que armazena os itens parecidos para Conciliacao Parcial
Local aFolCop			:= {}											//Array que armazena os itens do Folder a partir da sele��o inicial da tela
Local oSelec			:= LoadBitmap(GetResources(), "BR_VERDE"	)	//Objeto de tela para mostrar como marcado um registro
Local oNSelec			:= LoadBitmap(GetResources(), "BR_BRANCO"	)	//Objeto de tela para mostrar como desmarcado um registro
Local oRedSelec			:= LoadBitmap(GetResources(), "BR_VERMELHO"	)	//Objeto de tela para mostrar como desmarcado um registro
Local oAmaSelec			:= LoadBitmap(GetResources(), "BR_AMARELO"	)	//Objeto de tela para mostrar como Divergente um registro
Local oBlcSelec			:= LoadBitmap(GetResources(), "BR_PRETO"	)	//Objeto de tela para mostrar registro com BANCO/CONTA bloqueado

Local oConcPar			:= Nil											//Objeto da funcao TWBrowse usado no Folder Conc. Parcialmente
Local oConcMan			:= Nil											//Objeto da funcao TWBrowse usado no Folder Conc. Manualmente
Local oNaoConc			:= Nil											//Objeto da funcao TWBrowse usado no Folder Nao Conciliadas (Registros a serem conciliados)
Local oDiverg			:= Nil											//Objeto da funcao TWBrowse usado no Folder Divergentes (Registros a serem conciliados)
Local oTitulos			:= Nil											//Objeto da funcao TWBrowse usado no Folder Nao Conciliadas (Registros do Contas a Receber)
Local oIndic			:= Nil											//Objeto da funcao TWBrowse usado no Folder Nao Conciliados
Local oTotais			:= Nil											//Objeto da funcao TWBrowse usado no Folder Totais
Local oDlg				:= Nil											//Objeto da tela principal
Local oFolder			:= Nil											//Objeto que cria os Folders
Local lRet				:= .T.		 									//Variavel para tratamento dos retornos de funcoes
Local cPerg				:= "FINA910A"									//Grupo de Perguntas para filtro de informacoes para a Tela de Conciliacao do SITEF
Local aCampos			:= {}
Local aChave			:= {}
Local lWhenConc			:= .T.	
Local cDescri 			:= ""
Local oDescri 			:= Nil
Local oFnt				:= Nil
Local nConc 			:= 0
Local lCancel			:= .F.
Local aHeadXls          := {}
Local aHeaderDiv        := {}

Private lMsErroAuto		:= .F.											//Variavel logica de retorno do MsExecAuto
Private nRandThread 	:= 0
Private cAliasTMP		:= GetNextAlias() 

// Define qual operadora para a concilia��o
nConc := Aviso(STR0176, STR0177, { STR0178, STR0179, STR0180 }, 2, ) //"Concilia��o"###"Por favor, definir operadora para a concilia��o: "###"Demais"###"Software Express"###"Cancelar"
__lSOFEX	:= ( nConc == 2 )
lCancel	:= ( nConc == 3 )

If !lCancel
	If __lSOFEX
		cPerg := "FINA910A" // Software Express
	Else
		cPerg := "FINA910A1"  // Demais operadoras
	EndIf
Else
	Return
EndIf

__cF14FilAnt := F918BxLote()

//Inicializa o log de processamento
ProcLogIni( aButtons )

//Apresenta a tela de parametros para o usuario para delimitar
//os dados que serao apresentados em tela
If !Pergunte(cPerg,.T.)
    Return
EndIf

//Verifica o compartilhamento das tabelas SE1 e MEP
If AliasInDic("MEP") .and. (xFilial("SE1") <> xFilial("MEP"))
	//"Compartilhamento incorreto entre as tabelas SE1 e MEP"
	Help(" ",1,"A918Compart",,STR0114 ,1,0,,,,,,)
	//"Para utilizar o Conciliador SiTEF m�ltiplos cart�es, faz-se necess�rio que as tabelas SE1 e MEP possuam mesma forma de compartilhamento."    
	Return .F.
EndIf

//Atualiza o log de processamento
ProcLogAtu(STR0137)

A918aIniVar()

ProcLogAtu(STR0138,STR0139)
AADD(aCampos, {"SOURCE","C",10,0})
AADD(aCampos, { "RECNO","C",10,0})

AADD(aChave, "SOURCE")
AADD(aChave, "RECNO")

CreateTMP(aCampos,cAliasTMP,aChave)

ProcLogAtu(STR0138,STR0140)

//Montagem do Header dos Folders Conciliadas, Conciliadas Parcialmente, Conciliadas Manualmente, Nao Conciliadas
aFolder		:= {STR0001,STR0002,STR0003,STR0004,STR0166,STR0005}	//"Conciliadas"	 ### "Conc. Parcialmente"### "Conc. Manualmente"### "Nao Conciliadas"	### Divergentes			### "Totais"
aHeadTot	:= {STR0006,STR0007,STR0008,STR0009,STR0010,STR0011,;	//"Data Credito" ### "Conciliados"		### "Qtd.Conciliados"	### "Conc. Parc."		### "Qtd.Conc.Parc."	### "Conc.Man."	
				STR0012,STR0013,STR0014,STR0166,STR0170,STR0015}	//"Qtd.Conc.Man."### "Nao Conc."		### "Qtd.Nao Conc." 	###	"Divergentes"		### "Qtd.Divergentes"	### "Total Geral"
	
aHeader		:= { "",STR0016, STR0017, STR0018, STR0019, STR0020, STR0021, STR0022,;		    //"Cod Estab"	### "Nome do Estab"	### "Adminis"		### "Prefixo"		### "Titulo"			### "Tipo"			### "Nro Parc"
				STR0023, STR0024, STR0025, STR0026, STR0027, STR0028, STR0029, STR0080,;	//"Nro Comp"	### "DT Emissao"	### "DT Credito"	### "Valor"			### "Valor Sitef"		### "NSU"			### "Parc Sitef"	### "DocTef E1"
				STR0081, STR0082, STR0079, STR0083, STR0084, STR0085, STR0086, STR0173, ;	//"NSU E1"		### "Dt Cr�dito E1" ### "Bco/Ag/Conta"	### "Status Conta"	### "Origem Trans"		### "RECNO SE1"		### "RECNO FIF"
				STR0174, STR0175, STR0039, STR0204, STR0167, STR0168, STR0169, STR0182}		//"Agencia" 	### "Conta"			### "Banco"		    ### "Filial Sitef "	### "Cod. Justificativa"	###"Desc. Justificativa"	###"Justtificativa 	##Motivo Ajuste"

aHeadManu	:= { "", STR0016, STR0017, STR0018, STR0019, STR0020, STR0021, STR0022,;		//"Cod Estab"	### "Nome do Estab"	### "Adminis"		### "Prefixo"		### "Titulo"			### "Tipo"			### "Nro Parc"
				STR0023, STR0024, STR0025, STR0026, STR0027, STR0028, STR0029, STR0080,;	//"Nro Comp"	### "DT Emissao"	### "DT Credito"	### "Valor"			### "Valor Sitef"		### "NSU"			### "Parc Sitef"	### "DocTef E1"
				STR0081, STR0082, STR0079, STR0083, STR0084, STR0085, STR0086,;				//"NSU E1"		### "Dt Cr�dito E1"	### "Bco/Ag/Conta"	### "Status Conta"	### "Origem Trans"		### "RECNO SE1"		### "RECNO FIF"
				STR0167, STR0168, STR0169}										//"Cod. Justificativa"	###"Desc. Justificativa"	###"Justificativa"

aHeadIndic	:= { STR0030,STR0031,STR0032,STR0033,STR0034,STR0035,STR0036,STR0029,;			//"Vl Protheus" ### "Vl Sitef"		### "NSU Protheus"	### "NSU Sitef"		### "Emiss�o Protheus"	### "Emiss�o Sitef"	### "Parc Protheus"	### "Parc Sitef"
				STR0019,STR0020,STR0021,STR0039, STR0087, STR0088}				            //"Prefixo"		### "Titulo"		### "Tipo"
	
aHeadSE1	:= { "", STR0024, STR0037, STR0030, STR0032, STR0038, STR0019, STR0020,;		//"DT Emissao"	### "Vcto Protheus"	### "Vl Protheus"	### "NSU Protheus"	### "Comp Protheus"	### "Prefixo"	### "Titulo"
				STR0036, STR0021, STR0018, STR0029, STR0039, STR0087, STR0088}				//"Parc Protheus"### "Tipo"			### "Adminis"		### "Parc Sitef"    ### "Loja"			### "Filial"	### "RECNO"

aHeadMan   := aClone(aHeader)

aHeaderDiv := { "",;
			STR0220,; // 02 # "Data Venda" 
		    STR0087,; // 03 # "Filial"	
            STR0206,; // 04 # "Vlr Venda"
            STR0207,; // 05 # "Vlr Liquido"
			STR0028,; // 06 # "NSU"
			STR0208,; // 07 # "Autoriza��o"  FIF_CODAUT
			STR0016,; // 08 # "Cod Estab"
			STR0209,; // 09 # "Tipo de Registro"  FIF_TPREG
	  		STR0210,; // 10 # "Loja Sitef" FIF_CODLOJ
	  		STR0211,; // 11 # "Nr Resumo" FIF_NURESU
			STR0023,; // 12 # "Nro Comp"
			STR0212,; // 13 # "Tipo Produto" FIF_TPPROD
			STR0175,; // 14 # "Banco"
			STR0173,; // 15 # "Agencia" 
			STR0174,; // 16 # "Conta"
			STR0213,; // 17 # "Vlr Comiss�o"  FIF_VLCOM
			STR0214,; // 18 # "Taxa Serv"  FIF_TXSERV
			STR0020,; // 19 # "Titulo"
			STR0022,; // 20 # "Nro Parc"		
			STR0215,; // 21 # "Parc.Alfanum" FIF_PARALF
			STR0216,; // 22 # "Rastro de Parc"
			STR0217,; // 23 # "Status Venda"		FIF_STVEND
			STR0218,; // 24 # "Cod bandeira" FIF_CODBAN
			STR0018,; // 25 # "Adminis"
			STR0219,; // 26 # "Seq. Tab FIF"  FIF_SEQFIF
			STR0182,; // 27 # "Motivo Ajuste" 
			STR0167,; // 28 # "Cod. Justificativa"
			STR0168,; // 29 # "Desc. Justificativa"
			STR0169}  // 30 # "Justtificativa"
	
aColsAux   := {{	"",;				//1  - Status oNSelec
					"", ;				//2  - Codigo do Estabelecimento Sitef
					"", ;				//3  - Codigo da Loja Sitef
					"", ;				//4  - Codigo do Cliente (Administradora)
					"", ;				//5  - Prefixo do titulo Protheus
					"", ;				//6  - Numero do titulo Protheus
					"", ;				//7  - Tipo do titulo Protheus
					"", ;				//8  - Numero da parcela Protheus
					"", ;				//9  - Numero do Comprovante Sitef
					cTod("  /  /  "),;  //10 - Data da Venda Sitef
					cTod("  /  /  "),;  //11 - Data de Credito Sitef
					0,  ;				//12 - Valor do titulo Protheus
					0,  ;				//13 - Valor liquido Sitef
					"", ;				//14 - Numero NSU Sitef
					"", ;				//15 - Numero da parcela Sitef
					"", ;				//16 - Documento TEF Protheus
					"", ;				//17 - NSU Sitef Protheus
					cTod("  /  /  "),;  //18 - Vencimento real do titulo
					"", ;				//19 - banco/agencia/conta
					"", ;				//20 - Informa��o de conta n�o cadastrada ou cadastrada
					"", ;				//21 - (0,2 ou 3 - Transa��o Sitef),(1 ou 4 - Outros/POS)
					0,  ;				//22 - RECNO do SE1
					0,  ;				//23 - RECNO do FIF
					"", ;				//24 - Agencia
					"", ;				//25 - Conta
					"", ;				//26 - Banco
					"", ;				//27 - Codigo da Loja
					"", ;				//28 - FILIAL => Filial Sitef 
					"", ;				//29 - Cod. Justificativa
					"", ;				//30 - Desc. Justificativa
					"", ;				//31 - (Justificativa) Desc. Justificativa 2
					"", ;				//32 - Codigo da Administradora
					"", ;				//33 - (Codigo do Motivo) Motivo Ajuste
					"", ;				//34 - NSU Sitef do Arquivo
					"", ;				//35 - Autoriza��o
					"", ;				//36 - Tp Registro
					"", ;				//37 - Nr Resumo
					"", ;				//38 - Tipo Produto
					 0, ;				//39 - Vlr Comiss�o
					 0, ;				//40 - Taxa Serv
					"", ;				//41 - Parc.Alfanum
					"", ;				//42 - Status Venda
					"", ;				//43 - C�digo Banco
					"", ;				//44 - Seq. Tab FIF
					 0, ;				//45 - Vlr Venda
					 0, ;				//46 - Vlr Liquido
					"", ;				//47 - Filial
					"" }}				//48 - Rastro Parcela SE1
					
aHeadXls		:= { "", STR0016, STR0017, STR0018, STR0019, STR0020, STR0021, STR0022,; //Header para importa��o das abas para EXCEL
				STR0023, STR0024, STR0025, STR0026, STR0027, STR0028, STR0029, STR0080,;			
				STR0081, STR0082, STR0079, STR0083, STR0084, STR0085, STR0086,	STR0087 ,;
				STR0167, STR0168, STR0169,STR0182}				

If __lSOFEX
	nSelFil 		:= 2 
	cFilIni			:= MV_PAR01  // 1	Da Filial ?
	cFilFim			:= MV_PAR02  // 2 	Ate Filial ?      
	dDataCredI		:= MV_PAR03  // 3	Data Cr�dito De ?             
	dDataCredF		:= If(Empty(MV_PAR04), MV_PAR03, MV_PAR04) //4	Data Credito At� ?            
	cNSUIni			:= MV_PAR05  // 5	Do Num NSU ?                  
	cNSUFim 		:= MV_PAR06  // 6	Ate Num NSU ?                 
	cConcilia		:= MV_PAR07  // 7	Gera��o ?                     
	nTpProc 		:= MV_PAR08  // 8	Tipo ?                        
	nQtdDias		:= MV_PAR09  // 9	Pesq Dias Ant ?               
	If Empty(MV_PAR10)
		//N�o foi inserido percentual de toler�ncia para valor de TEF menor que o valor do t�tulo
		Help(" ",1,"A910Margem",,STR0076 ,1,0,,,,,,/*{STR0094}*/)
		nMargem := 0
	Else
		nMargem = MV_PAR10  // 10	Toler�ncia em % ?             
	EndIf 
	cAdmFinanIni	:= Iif(!Empty(Alltrim(MV_PAR10)), FormatIn(Alltrim(MV_PAR10), ";"), "") // 11	De Financeira ?
	cAdmFinanFim 	:= Iif(!Empty(Alltrim(MV_PAR10)), FormatIn(Alltrim(MV_PAR10), ";"), "") // 12	Ate Financeira ?
	lUseFIFDtCred	:= ( MV_PAR13 == 2 ) // "Credito SITEF" // 13	Data Baixa ?                  
	nVldNSE 		:= MV_PAR14 		 //14	Valida NSU p/ n�o Conc. ?     
	//Processa todas as Abas
	__nFldPar := 0
Else 
	nSelFil 		:= MV_PAR01  //	1	Seleciona Filiais ?           
	dDataCredI		:= MV_PAR02  // 2	Data Cr�dito De ?             
	dDataCredF		:= If(Empty(MV_PAR03), MV_PAR02, MV_PAR03) //3	Data Credito At� ?            
	cNSUIni			:= MV_PAR04  // 4	Do Num NSU ?                  
	cNSUFim 		:= MV_PAR05  // 5	Ate Num NSU ?                 
	cConcilia		:= MV_PAR06  // 6	Gera��o ?                     
	nTpProc 		:= MV_PAR07  // 7	Tipo ?                        
	nQtdDias		:= MV_PAR08  // 8	Pesq Dias Ant ?               
	If Empty(MV_PAR09)
		nMargem := 0
	Else
		nMargem = MV_PAR09  // 9	Toler�ncia em % ?             
	EndIf 
	cAdmFinanIni	:= Iif(!Empty(Alltrim(MV_PAR10)), FormatIn(Alltrim(MV_PAR10), ";"), "") // 10	Administradora ?              
	lUseFIFDtCred	:= ( MV_PAR11 == 2 ) // "Credito SITEF" // 11	Data Baixa ?                  
	nVldNSE 		:= MV_PAR12 		 //12	Valida NSU p/ n�o Conc. ?     
		//Processamento por Aba
		If MV_PAR13 == 1  // 13	Tipo de Processamento ?        
			//Aba para Processamento
			__nFldPar := MV_PAR14  // 14	Aba para Processamento ?      
			
			//Adiciona no TFolder apenas o selecionado no parametro 
			aADD(aFolCop,aFolder[__nFldPar])
			aADD(aFolCop,STR0005)
		Else
			//Processa todas as Abas
			__nFldPar := 0
		Endif
EndIf 

__aSelFil := {}
If nSelFil == 1
		//Gest�o - Selecao das Filiais deve ser geral pois controle � feito atrav�s do MSFIL. 
	__aSelFil := AdmGetFil(.F., .T.,)
EndIf

If nSelFil == 2 .Or. Len(__aSelFil) > 0 

	//Atualiza os dados de itens de acordo com os filtros
	//mostrados inicialmente, onde caso nao exista dados,
	//o retorno ser� Falso e sai da rotina                   
	ProcLogAtu(STR0138,STR0053)
	LjMsgRun(	STR0041,,{||lRet := A918AtuDados( @aConc, @aConcPar, @aNaoConc, @aConcMan,@aIndic, @aDiverg, oNSelec, oRedSelec)}) //"Filtrando os Registros..."

	If !lRet
		Return()
	EndIf

	//Atualiza o array totalizador
	LjMsgRun(STR0042,,{|| A918Total( aConc, aConcPar, aNaoConc, aConcMan, aDiverg, @aTotais, aHeader, aHeadMan)}) //"Gerando os totalizadores..."

	ProcLogAtu(STR0138,STR0141)

	//Verifica se existem informacoes para cada um dos arrays
	If Len(aConc) == 0
		aColsAux[1][1] := oSelec	 // BR_VERDE
		aConc := aClone(aColsAux)
	EndIf
		
	If Len(aConcPar) == 0
		aColsAux[1][1] := oSelec	 // BR_VERDE
		aConcPar := aClone(aColsAux)
	EndIf
		
	If Len(aConcMan) == 0
		aColsAux[1][1] := oNSelec	 // BR_BRANCO
		aConcMan := aClone(aColsAux)
	EndIf
		
	If Len(aNaoConc) == 0
		aColsAux[1][1] := oNSelec	 // BR_BRANCO
		aNaoConc := aClone(aColsAux)
	EndIf

	If Len(aDiverg) == 0
		aColsAux[1][1] := oNSelec	 // BR_AMARELO
		aDiverg := aClone(aColsAux)
	EndIf
		
	DEFINE MSDIALOG oDlg TITLE STR0043 FROM FA918ARES(170),FA918ARES(150) TO FA918ARES(640),FA918ARES(977) PIXEL //"Conciliador SITEF"
	//DEFINE MSDIALOG oDlg TITLE STR0043 FROM FA918ARES(150),FA918ARES(170) TO FA918ARES(665),FA918ARES(997) PIXEL //"Conciliador SITEF"
		
	//Adiciona as barras dos bot�es
	DEFINE BUTTONBAR oBaroDlg SIZE 10,10 3D TOP OF oDlg
	//
	oButtTree := TButton():New( 002, 002, STR0180,oBaroDlg,,50,20,,,,,,,,,, )   
	oButtTree:bAction := {||Iif( MsgNoYes(STR0224) ,oDlg:End(),.F.)}
	oButtTree:Align := CONTROL_ALIGN_RIGHT 

	oButtTree := TButton():New( 002, 062, STR0143,oBaroDlg,,50,20,,,,,,,,,, )   
	oButtTree:bAction := {|| ProcLogView() }
	oButtTree:Align := CONTROL_ALIGN_RIGHT 

	oButtTree := TButton():New( 002, 122,STR0229,oBaroDlg,,50,20,,,,,,,,,, )   
	oButtTree:bAction := {|| FA918ALEG() }
	oButtTree:Align := CONTROL_ALIGN_RIGHT 

	If __nFldPar == 0
		// Cria as Folders do Sistema com todas abas
		oFolder	:= TFolder():New( FA918ARES(012), FA918ARES(002), aFolder,{},oDlg,,,,.T.,.F., FA918ARES(386), FA918ARES(230), )
		oFolder:Align := CONTROL_ALIGN_ALLCLIENT 
	Else
		// Cria as Folders do Sistema com a aba escolhida e Totais
		oFolder	:= TFolder():New( FA918ARES(012), FA918ARES(002), aFolCop,{},oDlg,,,,.T.,.F., FA918ARES(386), FA918ARES(230), )
		oFolder:Align := CONTROL_ALIGN_ALLCLIENT 
	EndIf
		
	//Tratamento do Folder -> Totais
	oTotais := TWBrowse():New( FA918ARES(000),FA918ARES(000),FA918ARES(415),FA918ARES(190),,aHeadTot,,Iif(__nFldPar > 0, oFolder:aDialogs[2], oFolder:aDialogs[6]),,,,,,,,,,,,,,,,,,, )
	oTotais:SetArray(aTotais)
	oTotais:bLine :=	{||{DtoC(aTotais[oTotais:nAt,01]),;
							Transform(aTotais[oTotais:nAt,02], __cPicSALD),;
							aTotais[oTotais:nAt,03],;
							Transform(aTotais[oTotais:nAt,04], __cPicSALD),;
							aTotais[oTotais:nAt,05],;
							Transform(aTotais[oTotais:nAt,06], __cPicSALD),;
							aTotais[oTotais:nAt,07],;
							Transform(aTotais[oTotais:nAt,08], __cPicSALD),;
							aTotais[oTotais:nAt,09],;
							Transform(aTotais[oTotais:nAt,10], __cPicSALD),;
							aTotais[oTotais:nAt,11],;
							Transform(aTotais[oTotais:nAt,12], __cPicSALD)}}

	oTotais:bHeaderClick := {|x,y,z|A918SelReg(@aTotais,@oTotais,oSelec,oNSelec,y,,.F.,,,,,STR0195)}

	@ C(195),C(340) Button STR0192 Size C(073),C(012) Action Processa({||Fina918D(aConc, aTotais, aHeadXls, aHeadTot, 3)},STR0047) PIXEL OF Iif( __nFldPar > 0, oFolder:aDialogs[2], oFolder:aDialogs[6] )

	If __nFldPar == 0 .OR. __nFldPar == 1	
		//Tratamento do Folder -> Conciliadas
		oConc := TWBrowse():New( FA918ARES(000), FA918ARES(000), FA918ARES(415), FA918ARES(190),,aHeader,,oFolder:aDialogs[1],,,,,,,,,,,,,,,,,,, )
		oConc:SetArray(aConc)
		oConc:bLine :=	{||{	aConc[oConc:nAt,01],aConc[oConc:nAt,02],;
								aConc[oConc:nAt,03],aConc[oConc:nAt,04],aConc[oConc:nAt,05],aConc[oConc:nAt,06],aConc[oConc:nAt,07],;
								aConc[oConc:nAt,08],aConc[oConc:nAt,09],aConc[oConc:nAt,10],aConc[oConc:nAt,11],;
								Transform(aConc[oConc:nAt,12], __cPicSALD),Transform(aConc[oConc:nAt,13], __cPicVL),aConc[oConc:nAt,34],aConc[oConc:nAt,15],;
								aConc[oConc:nAt,16],aConc[oConc:nAt,17],aConc[oConc:nAt,18],aConc[oConc:nAt,19],;
								aConc[oConc:nAt,20],Iif(aConc[oConc:nAt,21] == '1' .OR. aConc[oConc:nAt,21] == '4',STR0089,STR0090),;	//"OUTROS"	### "SITEF"
								aConc[oConc:nAt,22],aConc[oConc:nAt,23],,,,,aConc[oConc:nAt,28],,,,}}
									
		oConc:bHeaderClick := {|x,y,z|A918SelReg(@aConc,@oConc,oSelec,oNSelec,y,,.F.,,,,,STR0196)}
			
		//Verifica se existem informacoes Conciliadas
		If aConc[1][2] <> ""
			oConc:bLDblClick := {||A918SelReg(@aConc,@oConc,oSelec,oNSelec,1,oConc:nAt)}
		EndIf
			
		@ FA918ARES(195),FA918ARES(002) Button STR0044 Size FA918ARES(073),FA918ARES(012) Action A918Efetiva(@aConc, @oConc, oRedSelec, oAmaSelec,,,, @aTotais, @oTotais, 1,,oDlg) PIXEL OF oFolder:aDialogs[1]		//"&Efetiva Concilia��o"
		@ C(195),C(340) Button STR0193 Size C(073),C(012) Action Processa({||Fina918DX(aConc, aIndic, aHeadXls, aHeadIndic, 1)},STR0047) PIXEL OF oFolder:aDialogs[1] //"&Titulos Excel"###"Processando ..."
		
		oConc:Refresh()
	EndIF

	If __nFldPar == 0 .OR. __nFldPar == 2	
		//Tratamento do Folder -> Conciliadas Parcialmente
		oConcPar := TWBrowse():New( FA918ARES(000), FA918ARES(000), FA918ARES(415), FA918ARES(130),,aHeader,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[2]),,,,,,,,,,,,,,,,,,, )
		oConcPar:SetArray(aConcPar)
		oConcPar:bLine :=	{||{	aConcPar[oConcPar:nAt,01],aConcPar[oConcPar:nAt,02],;
									aConcPar[oConcPar:nAt,03],aConcPar[oConcPar:nAt,04], aConcPar[oConcPar:nAt,05],aConcPar[oConcPar:nAt,06],aConcPar[oConcPar:nAt,07],;
									aConcPar[oConcPar:nAt,08],aConcPar[oConcPar:nAt,09],aConcPar[oConcPar:nAt,10],aConcPar[oConcPar:nAt,11],;
									Transform(aConcPar[oConcPar:nAt,12], __cPicSALD),Transform(aConcPar[oConcPar:nAt,13], __cPicVL),aConcPar[oConcPar:nAt,34],aConcPar[oConcPar:nAt,15],;
									aConcPar[oConcPar:nAt,16],aConcPar[oConcPar:nAt,17],aConcPar[oConcPar:nAt,18],aConcPar[oConcPar:nAt,19],;
									aConcPar[oConcPar:nAt,20],Iif(aConcPar[oConcPar:nAt,21] == '1' .OR. aConcPar[oConcPar:nAt,21] == '4',STR0089,STR0090),;	//"OUTROS"	### "SITEF"
									aConcPar[oConcPar:nAt,22],aConcPar[oConcPar:nAt,23],aConcPar[oConcPar:nAt,24], aConcPar[oConcPar:nAt,25],;
									aConcPar[oConcPar:nAt,26], aConcPar[oConcPar:nAt,27], aConcPar[oConcPar:nAt,28],aConcPar[oConcPar:nAt,29],;
									aConcPar[oConcPar:nAt,30],aConcPar[oConcPar:nAt,31],,}}

		
		oConcPar:bHeaderClick := {|x,y,z|A918SelReg(@aConcPar,@oConcPar,oSelec,oNSelec,y,,.F.,,,,,STR0197)}
		
		//VoIndicerifica se existem informacoes Conciliadas Parcialmente
		If aConcPar[1][2] <> ""
			oConcPar:bLDblClick := {||A918SelReg(@aConcPar,@oConcPar,oSelec,oNSelec,1,oConcPar:nAt),FVXF3(@oConcPar,@aConcPar), IIf (oConcPar:ColPos() == 31,lEditCell(@aConcPar,oConcPar,"@!",31),Nil)}
		EndIf
		oConcPar:bChange := {||A918AtuDiv(aConcPar,@aIndic,oConcPar:nAt,oIndic)}
		
		@ C(135),C(340) Button STR0135 Size C(073),C(012) Action Processa({||Fina918DX(aConcPar, aIndic, aHeadXls, aHeadIndic, 1)},STR0047) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[2]) 	//"&Nao Conc Excel"###"Processando ..."
			
		//Tratamento Divergencias
		oIndic := TWBrowse():New(C(011),C(000),C(415),C(040),,aHeadIndic,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[2]),,,,,,,,,,,,,,,,,,,)
		
		oIndic:SetArray(aIndic)

		
		oIndic:bHeaderClick := {|x,y,z|A918SelReg(@aIndic,@oIndic,oSelec,oNSelec,y,,.F.,,,,,STR0198)}
							 
		@ C(195),C(002) Button STR0044 Size C(073),C(012) Action A918Efetiva(@aConcPar, @oConcPar,oRedSelec, oAmaSelec,,,, @aTotais, @oTotais, 2,,oDlg) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[2])		//"&Efetiva Concilia��o"
		
		@ C(195),C(340) Button STR0136 Size C(073),C(012) Action Processa({||Fina918DX(aConcPar, aIndic, aHeader, aHeadIndic, 3)},STR0047) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[2]) 	////"&Titulos Excel"###"Processando ..."        

		oConcPar:Refresh()
		
	EndIf

	If __nFldPar == 0 .OR. __nFldPar == 4	
		//Tratamento do Folder -> Nao Conciliadas
		oNaoConc := TWBrowse():New(C(000),C(000),C(415),C(090),,aHeader,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[4]),,,,,,,,,,,,,,,,,,,)
		
		oNaoConc:SetArray(aNaoConc)
		oNaoConc:bLine :=	{||{	aNaoConc[oNaoConc:nAt,01],aNaoConc[oNaoConc:nAt,02],;
									aNaoConc[oNaoConc:nAt,03],aNaoConc[oNaoConc:nAt,04], aNaoConc[oNaoConc:nAt,05],aNaoConc[oNaoConc:nAt,06],aNaoConc[oNaoConc:nAt,07],;
									aNaoConc[oNaoConc:nAt,08],aNaoConc[oNaoConc:nAt,09],aNaoConc[oNaoConc:nAt,10],aNaoConc[oNaoConc:nAt,11],;
									Transform(aNaoConc[oNaoConc:nAt,12], __cPicSALD),Transform(aNaoConc[oNaoConc:nAt,13], __cPicVL),aNaoConc[oNaoConc:nAt,34],aNaoConc[oNaoConc:nAt,15],;
									aNaoConc[oNaoConc:nAt,16],aNaoConc[oNaoConc:nAt,17],aNaoConc[oNaoConc:nAt,18],aNaoConc[oNaoConc:nAt,19],;
									aNaoConc[oNaoConc:nAt,20],Iif(aNaoConc[oNaoConc:nAt,21] == '1' .OR. aNaoConc[oNaoConc:nAt,21] == '4',STR0089,STR0090),;	//"OUTROS"	### "SITEF"
									aNaoConc[oNaoConc:nAt,22],aNaoConc[oNaoConc:nAt,23],,,,,aNaoConc[oNaoConc:nAt,28],aNaoConc[oNaoConc:nAt,29],;
									aNaoConc[oNaoConc:nAt,30],aNaoConc[oNaoConc:nAt,31]}}
								
		oNaoConc:bHeaderClick := {|x,y,z|A918SelReg(@aNaoConc,@oNaoConc,oSelec,oNSelec,y,,.F.,,,,,STR0199)}
		
		//Selecionar item correspondente no Rodap�
		oNaoConc:bLDblClick	  := {||FVXF3(@oNaoConc,@aNaoConc),IIf(oNaoConc:ColPos() == 31,lEditCell(@aNaoConc,oNaoConc,"@!",31),Nil),;
									IIf(oNaoConc:ColPos() != 29 .And. oNaoConc:ColPos() != 31,MsgInfo(STR0045,STR0004), Nil) }
			
		//Tratamento Rodape Nao Conciliadas
		oTitulos := TWBrowse():New(C(008),C(000),C(415),C(080),,aHeadSE1,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[4]),,,,,,,,,,,,,,,,,,,)
			
		LjMsgRun(STR0091,,{||A918Titulos( aNaoConc, oNaoConc:nAt, oSelec, @aTitulos, oTitulos, oNSelec, aConc, aConcPar, @lWhenConc )})			//"Carregando os T�tulos..."
		
		oTitulos:SetArray(aTitulos)
		oTitulos:bLine := {|| {	aTitulos[oTitulos:nAt,01],DtoC(aTitulos[oTitulos:nAt,02]),DtoC(aTitulos[oTitulos:nAt,03]),Transform(aTitulos[oTitulos:nAt,04], __cPicSALD),;
								aTitulos[oTitulos:nAt,05],aTitulos[oTitulos:nAt,06],aTitulos[oTitulos:nAt,07],aTitulos[oTitulos:nAt,08],aTitulos[oTitulos:nAt,09],;
								aTitulos[oTitulos:nAt,10],aTitulos[oTitulos:nAt,11],aTitulos[oTitulos:nAt,12],aTitulos[oTitulos:nAt,13],aTitulos[oTitulos:nAt,14],;
								aTitulos[oTitulos:nAt,15]}}

		oTitulos:bHeaderClick := {|x,y,z|A918SelReg(@aTitulos,@oTitulos,oSelec,oNSelec,y,,.F.,,,,,STR0200)}
			
		//Verifica se existem informacoes Nao Conciliadas
		If aTitulos[1][5] <> ""
			oTitulos:bLDblClick	:= {||A918SelReg(@aTitulos,@oTitulos,oSelec,oNSelec,1,oTitulos:nAt,.T.,@aNaoConc,oNaoConc:nAt,oNaoConc)}
		EndIf

		//"&Nao Conc Excel"###"Processando ..."	
		@ C(095),C(340) Button STR0046 Size C(073),C(012) Action Processa({||Fina918D(aNaoConc, aIndic, aHeadXls, aHeadIndic, 1)},STR0047) PIXEL OF Iif( __nFldPar > 0, oFolder:aDialogs[1], oFolder:aDialogs[4] )
			
		//"&Efetiva Concilia��o"
		@ C(195),C(002) Button STR0044 Size C(073),C(012) Action A918Efetiva( @aNaoConc, @oNaoConc, oRedSelec, oAmaSelec, aTitulos, @aConcMan, oConcMan, @aTotais, @oTotais, 3, @oTitulos, oDlg );
																				PIXEL OF Iif( __nFldPar > 0, oFolder:aDialogs[1], oFolder:aDialogs[4] ) WHEN lWhenConc
		//"&Titulos Excel"###"Processando ..."	
		@ C(195),C(340) Button STR0048 Size C(073),C(012) Action Processa( { ||Fina918D( aNaoConc, aTitulos, aHeader, aHeadSE1, 2) }, STR0047 ) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[4]) 	

		oNaoConc:Refresh()
		oTitulos:Refresh()
	EndIf

	If __nFldPar == 0 .OR. __nFldPar == 3	
		//Tratamento do Folder -> Conciliadas Manualmente
		oConcMan := TWBrowse():New(C(000),C(000),C(415),C(190),,aHeadManu,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[3]),,,,,,,,,,,,,,,,,,,)
		oConcMan:SetArray(aConcMan)
		oConcMan:bLine := {|| {	aConcMan[oConcMan:nAt,01],aConcMan[oConcMan:nAt,02],aConcMan[oConcMan:nAt,03],aConcMan[oConcMan:nAt,04],aConcMan[oConcMan:nAt,05],;
								aConcMan[oConcMan:nAt,06],aConcMan[oConcMan:nAt,07],aConcMan[oConcMan:nAt,08],aConcMan[oConcMan:nAt,09],dtoc(aConcMan[oConcMan:nAt,10]),;
								dtoc(aConcMan[oConcMan:nAt,11]),Transform(aConcMan[oConcMan:nAt,12],__cPicSALD),Transform(aConcMan[oConcMan:nAt,13], __cPicVL),aConcMan[oConcMan:nAt,34],; // FIF_NSUARQ
								aConcMan[oConcMan:nAt,15],aConcMan[oConcMan:nAt,16],aConcMan[oConcMan:nAt,17],dtoc(aConcMan[oConcMan:nAt,18]),aConcMan[oConcMan:nAt,19],aConcMan[oConcMan:nAt,20],;
								Iif(aConcMan[oConcMan:nAt,21] == '1' .OR. aConcMan[oConcMan:nAt,21] == '4',STR0089,STR0090),;	//"OUTROS"	### "SITEF"
								aConcMan[oConcMan:nAt,22],aConcMan[oConcMan:nAt,23],aConcMan[oConcMan:nAt,24],aConcMan[oConcMan:nAt,25],;
								aConcMan[oConcMan:nAt,26],,,,,,	}} 

		oConcMan:bHeaderClick := {|x,y,z|A918SelReg(@aConcMan,@oConcMan,oSelec,oNSelec,y,,.F.,,,,,STR0201)}

		@ C(195),C(340) Button STR0194 Size C(073),C(012) Action Processa({||Fina918DX(aConcMan, aIndic, aHeadXls, aHeadIndic, 1)},STR0047) PIXEL OF Iif( __nFldPar > 0, oFolder:aDialogs[1], oFolder:aDialogs[3] ) //"&Titulos Excel"###"Processando ..."
					
		oConcMan:Refresh()
	EndIf

	If __nFldPar == 0 .OR. __nFldPar == 5
		//Tratamento do Folder -> Divergentes
	    oDiverg := TWBrowse():New(C(000),C(000),C(415),C(130),,aHeaderDiv,,Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[5]),,,,,,,,,,,,,,,,,,,)
		oDiverg:SetArray(aDiverg)
		oDiverg:bLine := {||{aDiverg[oDiverg:nAt,01],; 
							aDiverg[oDiverg:nAt,10],;  
							aDiverg[oDiverg:nAt,47],;  
							TRANSFORM(aDiverg[oDiverg:nAt,45], __cPicSALD),; 
							TRANSFORM(aDiverg[oDiverg:nAt,46], __cPicVL),;   
							aDiverg[oDiverg:nAt,34],;  
							aDiverg[oDiverg:nAt,35],;  
							aDiverg[oDiverg:nAt,02],;  
							aDiverg[oDiverg:nAt,36],;  
							aDiverg[oDiverg:nAt,28],;  
							aDiverg[oDiverg:nAt,37],;  
							aDiverg[oDiverg:nAt,09],; 
							aDiverg[oDiverg:nAt,38],;  
							aDiverg[oDiverg:nAt,26],;  
							aDiverg[oDiverg:nAt,24],;  
							aDiverg[oDiverg:nAt,25],;  
							TRANSFORM(aDiverg[oDiverg:nAt,39], __cPicSALD),; 
							TRANSFORM(aDiverg[oDiverg:nAt,40], __cPicSALD),;
							aDiverg[oDiverg:nAt,06],;   
							aDiverg[oDiverg:nAt,08],;  
							aDiverg[oDiverg:nAt,41],;   
							aDiverg[oDiverg:nAt,48],;  
							aDiverg[oDiverg:nAt,42],;  
							aDiverg[oDiverg:nAt,43],;  
							aDiverg[oDiverg:nAt,04],; 
							aDiverg[oDiverg:nAt,44],; 
							aDiverg[oDiverg:nAt,33],; 
							aDiverg[oDiverg:nAt,29],;  
							aDiverg[oDiverg:nAt,31],; 
							aDiverg[oDiverg:nAt,30]}}  
							
		oDiverg:bHeaderClick := {|x,y,z|A918SelReg(@aDiverg,@oDiverg,oSelec,oNSelec,y,,.F.,,,,,STR0202)}

		
		DEFINE FONT oFnt NAME "Arial" SIZE 11,20 
		@ C(135), C(002) SAY   STR0172 SIZE 050, 020 OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[5]) PIXEL FONT oFnt COLOR CLR_HBLUE
		@ C(145), C(002) MSGET oDescri VAR cDescri 	SIZE 300, 40 OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[5]) PIXEL WHEN .F.
			
		oDiverg:bChange := {||A918Msg(@aDiverg,@oDiverg,@oDescri,@cDescri)}
		
		//Verifica se existem informacoes Conciliadas
		If aDiverg[1][2] <> ""
			oDiverg:bLDblClick := {|| A918SelReg(@aDiverg,@oDiverg,oSelec,oNSelec,1,oDiverg:nAt),FVXF3(@oDiverg,@aDiverg,.T.),IIf (oDiverg:ColPos() == 30,lEditCell(@aDiverg,oDiverg,"@!",30),Nil)}
		EndIf	
		
		@ FA918ARES(195),FA918ARES(002) Button STR0171 Size FA918ARES(073),FA918ARES(012) Action A918Efetiva(@aDiverg, @oDiverg, oRedSelec, oAmaSelec,,,, @aTotais, @oTotais, 4,,oDlg) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[5])		//"&Gravar"
		@ FA918ARES(195),FA918ARES(340) Button STR0136 Size FA918ARES(073),FA918ARES(012) Action Processa({||Fina918D(aDiverg, aIndic, aHeaderDiv, aHeadIndic, 4)},STR0047) PIXEL OF Iif(__nFldPar > 0, oFolder:aDialogs[1],oFolder:aDialogs[5]) //"&Titulos Excel"###"Processando ..."
		oDiverg:Refresh()
	EndIf
		
	ACTIVATE MSDIALOG oDlg CENTERED
								
	A918CLOSEAREA(cAliasTMP)
	dbSelectArea("SE1")
	SE1->(MSRUNLOCK())
	
	aConc		:= aSize(aConc,0)
	aConcPar	:= aSize(aConcPar,0)
	aNaoConc	:= aSize(aNaoConc,0)
	aConcMan	:= aSize(aConcMan,0)
	aIndic		:= aSize(aIndic,0)
	aDiverg		:= aSize(aDiverg,0)
		
	aConc		:= {}
	aConcPar	:= {}
	aNaoConc	:= {}
	aConcMan	:= {}
	aIndic		:= {}
	aDiverg		:= {}
	aFolCop		:= {}
		
	//Atualiza o log de processamento
	ProcLogAtu("FIM")

EndIf

Return(.T.)

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918AtuDados
Rotina que retorna os dados de conciliacao de acordo com o vinculo com o 
contas a receber                            

@type Function
@author Rafael Rosa da Silva
@since 05/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918AtuDados( aConc, aConcPar, aNaoConc, aConcMan, aIndic, aDiverg, oNSelec, oRedSelec ) 
Local cQry			:= ""						//Instrucao de query no banco
Local cAliasSitef	:= GetNextAlias()         	//Variavel que recebe o proximo Alias disponivel
Local cSubstring	:= ""						//Variavel para tratar a comando "SUBSTRING" no banco de dados
Local cValConta		:= ""						//Informacao de conta nao cadastrada ou cadastrada
Local cFilSitef		:= ""						//Filial informada no Sitef
	
Local aColsAux		:= {}						//Array auxiliar para carregar arrays de trabalho
Local aArea			:= GetArea()				//Array que armazena a ultima area utilizada
	
Local lRet			:= .T.						//Retorno da funcao
Local lExclusivo	:= !Empty(xFilial("SE1"))	//Verifica se SE1 esta' compartilhada ou exclusiva
Local lExclusFIF	:= !Empty(xFilial("FIF"))	//Verifica se FIF esta' compartilhada ou exclusiva
Local lExclusMEP	:= !Empty(xFilial("MEP"))
Local aDados		:= {}
Local cMSFIL		:= ""
Local cAdmAtu		:= ""
Local cCodBco		:= ""
Local cCodAge		:= ""
Local cNumCC		:= ""
Local lUsaMep		:= SuperGetMv("MV_USAMEP",.T.,.F.)
Local lMsgTef		:= .T.	

Local aParName		:= A918ParName() // resgata parametro x cod. administradora 
Local cCodFil	 	:= ""
Local cMVParTef 	:= ""
Local nPos 		:= 0
Local lOracle  	:= .F.
Local lPostGre	:= .F. 
Local cTamNSU		:= Alltrim(Str(nTamNSUTEF))
Local cVarAux       := ""

//Zero as variaveis antes de atualizar
aConc		:= {}
aConcPar	:= {}
aNaoConc	:= {}
aConcMan	:= {}
aDiverg		:= {}
aIndic		:= {}
	       
If ( AllTrim( Upper( TcGetDb() ) ) $ "ORACLE_INFORMIX" )
	cSubstring := "SUBSTR"
	lOracle := .T.
ElseIf ( AllTrim( Upper( TcGetDb() ) ) $ "DB2|DB2/400")
	cSubstring := "SUBSTR"
Else
	If ( AllTrim( Upper( TcGetDb() ) ) $ "POSTGRES" )
		lPostGre := .T.
	EndIf
	cSubstring := "SUBSTRING"
EndIf

If __lDefTop == Nil
	__lDefTop := FindFunction("IFDEFTOPCTB") .And. IfDefTopCTB() 
EndIf
	
If __lDefTop
    If !lMEP .or. !lUsaMep   //N�o Existe a tabela MEP ou n�o usa.
    	cQry := "SELECT FIF.FIF_CODEST, FIF.FIF_CODLOJ, FIF.FIF_NUCOMP, FIF.FIF_DTTEF, FIF.FIF_VLLIQ, FIF.FIF_NSUTEF, FIF.FIF_NSUARQ, FIF.FIF_PARCEL," 
    	cQry += " FIF.FIF_DTCRED, FIF.FIF_STATUS, FIF.FIF_PREFIX, FIF.FIF_NUM, FIF.FIF_CODRED, FIF.FIF_PARC, FIF.FIF_TIPO, FIF.R_E_C_N_O_ RECNO_FIF,"
    	cQry += " FIF_PARALF, FIF.FIF_CODBCO, FIF.FIF_CODAGE, FIF.FIF_NUMCC, FIF.FIF_CAPTUR, FIF.FIF_VLBRUT, FIF.FIF_PGJUST, FIF.FIF_PGDES1,"
    	cQry += " FIF.FIF_PGDES2, FIF.FIF_TPPROD, FIF.FIF_TPREG, FIF.FIF_CODADM, FIF.FIF_CODMAJ, SE1.E1_CLIENTE, SE1.E1_VALOR, SE1.E1_SALDO,"
    	cQry += " FIF.FIF_FILIAL, FIF.FIF_CODAUT, FIF.FIF_TPREG, FIF.FIF_NURESU, FIF.FIF_TPPROD, FIF.FIF_VLCOM, FIF.FIF_TXSERV, FIF.FIF_PARALF, FIF.FIF_STVEND, FIF.FIF_CODBAN, FIF.FIF_SEQFIF, "   	
    	cQry += " SE1.E1_PARCELA, SE1.E1_PREFIXO, SE1.E1_NUM, SE1.E1_TIPO, SE1.E1_MSFIL, SE1.E1_FILORIG, SE1.E1_DOCTEF, SE1.E1_NSUTEF, SE1.E1_VENCREA,"
    	cQry += " SE1.E1_EMISSAO, SE1.R_E_C_N_O_ RECNO_SE1, SE1.E1_LOJA,"
    	
        cQry += " '"+ Space(nTamParc) +"' AS MEP_PARTEF, FIF.FIF_CODFIL "
        cQry += "FROM " + AllTrim(RetSqlName("FIF")) + " FIF "
        cQry += "LEFT JOIN " + RetSqlName("SE1") + " SE1 "
        cQry += "ON ( SE1.E1_PARCELA = FIF.FIF_PARCEL OR SE1.E1_PARCELA = FIF_PARALF ) "
        cQry += "AND FIF.FIF_DTTEF = SE1.E1_EMISSAO "

        If !__lProcDocTEF
            If __lSOFEX
				cQry += "AND SE1.E1_NSUTEF = FIF.FIF_NSUTEF "
			Else
			 	If lOracle .OR. lPostGre
					cQry += "AND LPAD(TRIM(SE1.E1_NSUTEF), "+cTamNSU+", '0') = FIF.FIF_NSUTEF "
			 	Else
					cQry += "AND REPLICATE('0', "+cTamNSU+" - LEN(SE1.E1_NSUTEF)) + RTrim(SE1.E1_NSUTEF) =  FIF.FIF_NSUTEF "
				Endif	
			Endif                                                       
        ElseIf __lDocTef
            cQry += "AND CAST(E1_DOCTEF AS INT) = CAST(FIF_NUCOMP AS INT) "
        Endif

        cQry += "AND SE1.E1_SALDO > 0 "
        If nTpProc == 1
            cQry += "AND SE1.E1_TIPO = 'CD' "
        ElseIf	nTpProc == 2
            cQry += "AND SE1.E1_TIPO = 'CC' "
        Else
            cQry += "AND SE1.E1_TIPO IN ('CD','CC') "
        EndIf
        cQry += "AND SE1.E1_MSFIL = FIF.FIF_CODFIL "
		
		If __lSOFEX
			If lExclusivo
				cQry += "AND SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			Else
				cQry += "AND SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			EndIf
		Else
			If nSelFil == 1 .and. Len( __aSelFil ) > 0
				If lExclusivo
					cQry += " AND SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
				Else 
					cQry += " AND SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 	
					cQry += " AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )
				EndIf
			Else
				cQry += " AND SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "  
				cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf 
		cQry += "AND SE1.D_E_L_E_T_ = ' ' "

		If __lSOFEX
			cQry += "WHERE FIF.FIF_CODFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
		Else 
			If nSelFil == 1 .and. Len( __aSelFil ) > 0
				cQry += " WHERE FIF.FIF_CODFIL " + GetRngFil( __aSelFil, 'FIF', .T.,, 20, .T. )
			Else
				cQry += " WHERE FIF.FIF_CODFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf

		cQry += "AND FIF.FIF_DTCRED BETWEEN '" + dTos(dDataCredI) + "' AND '" + dTos(dDataCredF) + "'"
	
        If !__lProcDocTEF
            cQry += "AND FIF.FIF_NSUTEF BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' "
        ElseIf __lDocTef
            cQry += "AND FIF.FIF_NUCOMP BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' "
        Endif

        cQry += "AND FIF.FIF_STATUS IN ('1','4','6') "

        If nTpProc == 1
            cQry += "AND FIF.FIF_TPPROD IN ('D','V') "
        ElseiF	nTpProc == 2
            cQry += "AND FIF.FIF_TPPROD='C' "
        Else
            cQry += "AND FIF.FIF_TPPROD IN ('D','V','C') "
        EndIf
           
		If !Empty(cAdmFinanIni)
			If __lSOFEX
				cQry += "AND FIF.FIF_CODRED>='" + cAdmFinanIni + "' "
				If !Empty(cAdmFinanFim)
					cQry += "AND FIF.FIF_CODRED<='" + cAdmFinanFim + "' "
				EndIf
			Else
				cQry += "AND FIF.FIF_CODADM IN  " + cAdmFinanIni + " "
			EndIf
        EndIf
			
		cQry += "AND FIF.D_E_L_E_T_=' ' "
        cQry += "ORDER BY RECNO_SE1"
    Else //Caso exista a tabela MEP, rodo a seguinte query

        cQry := "SELECT FIF.FIF_CODEST,FIF.FIF_CODLOJ,FIF.FIF_NUCOMP,FIF.FIF_DTTEF,FIF.FIF_VLLIQ,FIF.FIF_NSUTEF,FIF.FIF_NSUARQ,"
        cQry += "FIF.FIF_PARCEL,FIF.FIF_DTCRED,FIF.FIF_STATUS,FIF.FIF_PREFIX,FIF.FIF_NUM,FIF.FIF_CODRED,FIF.FIF_PARC,"
        cQry += "FIF.FIF_TIPO,FIF.FIF_PARALF,FIF.FIF_CODBCO,FIF.FIF_CODAGE,FIF.FIF_NUMCC,FIF.FIF_CAPTUR,FIF.R_E_C_N_O_ RECNO_FIF,"
    	cQry += " FIF.FIF_FILIAL, FIF.FIF_CODAUT, FIF.FIF_TPREG, FIF.FIF_NURESU, FIF.FIF_TPPROD, FIF.FIF_VLCOM, FIF.FIF_TXSERV, FIF.FIF_PARALF, FIF.FIF_STVEND, FIF.FIF_CODBAN, FIF.FIF_SEQFIF, "   	        
        cQry += "SE1.E1_PREFIXO,SE1.E1_NUM,SE1.E1_PARCELA,SE1.E1_TIPO,SE1.E1_CLIENTE,SE1.E1_LOJA,SE1.E1_VALOR,SE1.E1_SALDO,"
        cQry += "SE1.E1_EMISSAO,SE1.E1_VENCREA,SE1.E1_DOCTEF,SE1.E1_NSUTEF,SE1.E1_MSFIL,SE1.E1_FILORIG,SE1.R_E_C_N_O_ RECNO_SE1,"
        cQry += "MEP.MEP_PARTEF, FIF.FIF_VLBRUT, FIF.FIF_PGJUST, FIF.FIF_PGDES1, FIF.FIF_PGDES2, FIF.FIF_TPPROD, FIF.FIF_TPREG, FIF.FIF_CODADM, FIF.FIF_CODMAJ, FIF.FIF_CODFIL "
    	
        cQry += " FROM " + RetSqlName("SE1") + " SE1 JOIN " + RetSqlName("FIF") + " FIF ON "

        If !__lProcDocTEF
            If __lSOFEX
				cQry += " SE1.E1_NSUTEF = FIF.FIF_NSUTEF AND "
			Else
			 	If lOracle .OR. lPostGre
					cQry += " LPAD(TRIM(SE1.E1_NSUTEF), "+cTamNSU+", '0') = FIF.FIF_NSUTEF AND "
			 	Else
					cQry += " REPLICATE('0', "+cTamNSU+" - LEN(SE1.E1_NSUTEF)) + RTrim(SE1.E1_NSUTEF) =  FIF.FIF_NSUTEF AND "
				Endif	
			Endif                                                       
        ElseIf __lDocTef
            cQry += " AND CAST(E1_DOCTEF AS INT) = CAST(FIF_NUCOMP AS INT) AND "
        Endif

        cQry += " SE1.E1_EMISSAO = FIF.FIF_DTTEF "
       	cQry += " AND  SE1.E1_MSFIL = FIF.FIF_CODFIL "
	
        cQry += " JOIN " + RetSqlName("MEP") + " MEP ON "
        cQry += " SE1.E1_FILIAL = MEP.MEP_FILIAL AND "
        cQry += " SE1.E1_PREFIXO = MEP.MEP_PREFIX AND "
        cQry += " SE1.E1_NUM = MEP.MEP_NUM AND "
        cQry += " SE1.E1_PARCELA = MEP.MEP_PARCEL AND "
        cQry += " SE1.E1_TIPO = MEP.MEP_TIPO AND "
        cQry += " SE1.E1_MSFIL = MEP.MEP_MSFIL "
			
        cQry += " WHERE "
        If !lExclusFif
            cQry += " FIF.FIF_FILIAL = '" + xFilial("FIF", __cF14FilAnt) + "' AND "
        EndIf

        If nTpProc == 1
            cQry += " SE1.E1_TIPO = 'CD' AND "
        ElseIf	nTpProc == 2
            cQry += " SE1.E1_TIPO = 'CC' AND "
        Else
            cQry += " SE1.E1_TIPO IN ('CD','CC') AND "
        EndIf

        cQry += " SE1.E1_SALDO > 0 AND "
		If __lSOFEX
			If lExclusivo
				cQry += " SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			Else
				cQry += " SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			EndIf
		Else
			If nSelFil == 1 .and. Len( __aSelFil ) > 0
				If  lExclusivo	
					cQry += "SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
				Else 
					cQry += "SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 	
					cQry += "AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )
				EndIf
			Else
				cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 
				cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf	
        cQry += " AND SE1.D_E_L_E_T_ = ' ' AND "

		If __lSOFEX
			If lExclusMEP
				cQry += " MEP.MEP_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' AND "
				cQry += " MEP.MEP_FILIAL = FIF.FIF_CODFIL "
			Else
				cQry += " MEP.MEP_FILIAL = '" + xFilial("MEP", __cF14FilAnt) + "'"
			EndIf
		Else
			If nSelFil == 1 .AND. Len( __aSelFil ) > 0
				cQry += " MEP.MEP_FILIAL " + GetRngFil( __aSelFil, 'MEP', .T.,, 20, .F. )
			Else
				cQry += " MEP.MEP_FILIAL = '" + xFilial("MEP", __cF14FilAnt) + "' "
			EndIf
		EndIf

		
		If __lSOFEX
			cQry += "AND FIF.FIF_CODFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
		Else
			If nSelFil == 1 .AND. Len( __aSelFil ) > 0
				cQry += " AND FIF.FIF_CODFIL " + GetRngFil( __aSelFil, 'FIF', .T.,, 20, .T. )
			Else
				cQry += " AND FIF.FIF_CODFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf
		cQry += " AND MEP.MEP_MSFIL = FIF.FIF_CODFIL "
		cQry += " AND FIF.FIF_PARCEL = MEP.MEP_PARTEF AND "
        cQry += " FIF.FIF_DTCRED BETWEEN '" + dTos(dDataCredI) + "' AND '" + dTos(dDataCredF) + "' AND "
        
        If !__lProcDocTEF
            cQry += " FIF.FIF_NSUTEF BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        ElseIf __lDocTef
            cQry += " FIF.FIF_NUCOMP BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        Endif
        
        cQry += " FIF.FIF_STATUS IN ('1','4','6') AND "
			
		If !Empty(cAdmFinanIni)
			If __lSOFEX
				cQry += " FIF.FIF_CODRED>='" + cAdmFinanIni + "' AND "
				If !Empty(cAdmFinanFim)
					cQry += " FIF.FIF_CODRED<='" + cAdmFinanFim + "' AND "
				EndIf
			Else
				cQry += " FIF.FIF_CODADM IN  " + cAdmFinanIni + " AND "
			EndIf
		EndIf
					
        If nTpProc == 1
            cQry += " FIF.FIF_TPPROD IN ('D','V') AND "
        ElseiF	nTpProc == 2
            cQry += " FIF.FIF_TPPROD = 'C' AND "
        Else
            cQry += " FIF.FIF_TPPROD IN ('D','V','C') AND "
        EndIf
		
        cQry += " MEP.D_E_L_E_T_ = ' ' AND "
        cQry += " FIF.D_E_L_E_T_ = ' ' "
			
        cQry += " UNION ALL "
			
        cQry += "SELECT  FIF.FIF_CODEST,FIF.FIF_CODLOJ,FIF.FIF_NUCOMP,FIF.FIF_DTTEF,FIF.FIF_VLLIQ,FIF.FIF_NSUTEF,FIF.FIF_NSUARQ,FIF.FIF_PARCEL,
        cQry += "FIF.FIF_DTCRED,FIF.FIF_STATUS,FIF.FIF_PREFIX,FIF.FIF_NUM,FIF.FIF_CODRED,FIF.FIF_PARC,FIF.FIF_TIPO,FIF.FIF_PARALF,"  	   	
        cQry += "FIF.FIF_CODBCO,FIF.FIF_CODAGE,FIF.FIF_NUMCC,FIF.FIF_CAPTUR,FIF.R_E_C_N_O_ RECNO_FIF, "
    	cQry += " FIF.FIF_FILIAL, FIF.FIF_CODAUT, FIF.FIF_TPREG, FIF.FIF_NURESU, FIF.FIF_TPPROD, FIF.FIF_VLCOM, FIF.FIF_TXSERV, FIF.FIF_PARALF, FIF.FIF_STVEND, FIF.FIF_CODBAN, FIF.FIF_SEQFIF, "
        cQry += " '' E1_PREFIXO,'' E1_NUM,"
        cQry += "'' E1_PARCELA,'' E1_TIPO,'' E1_CLIENTE,'' E1_LOJA,0 E1_VALOR,0 E1_SALDO,'' E1_EMISSAO,'' E1_VENCREA,'' E1_DOCTEF,"
        cQry += "'' E1_NSUTEF,'' E1_FILORIG,'' E1_MSFIL,0 RECNO_SE1,'' MEP_PARTEF, FIF.FIF_VLBRUT, FIF.FIF_PGJUST, FIF.FIF_PGDES1, "
        cQry += " FIF.FIF_PGDES2, FIF.FIF_TPPROD, FIF.FIF_TPREG, FIF.FIF_CODADM, FIF.FIF_CODMAJ, FIF.FIF_CODFIL "
    	
        cQry += " FROM " + RetSqlName("FIF") + " FIF WHERE "
			
        If !lExclusFif
            cQry += " FIF.FIF_FILIAL = '' AND "
		EndIf
		
		If __lSOFEX
			cQry += " FIF.FIF_CODFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "		
		Else	
			If nSelFil == 1 .AND. Len( __aSelFil ) > 0
				cQry += " FIF.FIF_CODFIL " + GetRngFil( __aSelFil, 'FIF', .T.,, 20, .T. )
			Else
				cQry += " FIF.FIF_CODFIL = '" + __cF14FilAnt + "' " 
			EndIf
		EndIf 
        cQry += " AND FIF.FIF_DTCRED BETWEEN '" + dTos(dDataCredI) + "' AND '" + dTos(dDataCredF) + "' AND "

        If !__lProcDocTEF
            cQry += " FIF.FIF_NSUTEF BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        ElseIf __lDocTef
            cQry += " FIF.FIF_NUCOMP BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        EndIf
        
        cQry += " FIF.FIF_STATUS IN ('1','4','6') AND "
			
		If !Empty(cAdmFinanIni)
			If __lSOFEX
				cQry += " FIF.FIF_CODRED>='" + cAdmFinanIni + "' AND "
				If !Empty(cAdmFinanFim)
					cQry += " FIF.FIF_CODRED<='" + cAdmFinanFim + "' AND "
				EndIf
			Else
				cQry += " FIF.FIF_CODADM IN  " + cAdmFinanIni + " AND "
			EndIf
		EndIf
			
        If nTpProc == 1
            cQry += " FIF.FIF_TPPROD IN ('D','V') AND "
        ElseiF	nTpProc == 2
            cQry += " FIF.FIF_TPPROD = 'C' AND "
        Else
            cQry += " FIF.FIF_TPPROD IN ('D','V','C') AND "
        EndIf

        cQry += " FIF.R_E_C_N_O_ NOT IN ( "
        cQry += " SELECT AUXFIF.R_E_C_N_O_ "
			
        cQry += " FROM " + RetSqlName("SE1") + " SE1 JOIN " + RetSqlName("FIF") + " AUXFIF ON "
        
        If !__lProcDocTEF
            If __lSOFEX
				cQry += " SE1.E1_NSUTEF = AUXFIF.FIF_NSUTEF AND "
			Else
			 	If lOracle .OR. lPostGre
					cQry += " LPAD(TRIM(SE1.E1_NSUTEF), "+cTamNSU+", '0') = AUXFIF.FIF_NSUTEF AND "
			 	Else
					cQry += " REPLICATE('0', "+cTamNSU+" - LEN(SE1.E1_NSUTEF)) + RTrim(SE1.E1_NSUTEF) =  AUXFIF.FIF_NSUTEF AND "
				Endif	
			Endif                                                       
        ElseIf __lDocTef
            cQry += " CAST(SE1.E1_DOCTEF AS INT) = CAST(AUXFIF.FIF_NUCOMP AS INT) AND "
        Endif

        cQry += " SE1.E1_EMISSAO = AUXFIF.FIF_DTTEF AND "
        cQry += " SE1.E1_MSFIL = AUXFIF.FIF_CODFIL "
			
        cQry += " JOIN " + RetSqlName("MEP") + " MEP ON "
        cQry += " SE1.E1_FILIAL = MEP.MEP_FILIAL AND "
        cQry += " SE1.E1_PREFIXO = MEP.MEP_PREFIX AND "
        cQry += " SE1.E1_NUM = MEP.MEP_NUM AND "
        cQry += " SE1.E1_PARCELA = MEP.MEP_PARCEL AND "
        cQry += " SE1.E1_TIPO = MEP.MEP_TIPO AND "
        cQry += " SE1.E1_MSFIL = MEP.MEP_MSFIL "

        cQry += " WHERE "
        If !lExclusFif
            cQry += " AUXFIF.FIF_FILIAL = '" + xFilial("FIF", __cF14FilAnt) + "' AND "
        EndIf
			
        If nTpProc == 1
            cQry += " SE1.E1_TIPO = 'CD' AND "
        ElseIf	nTpProc == 2
            cQry += " SE1.E1_TIPO = 'CC' AND "
        Else
            cQry += " SE1.E1_TIPO IN ('CD','CC') AND "
        EndIf

        cQry += " SE1.E1_SALDO > 0 AND "

		If __lSOFEX
			If lExclusivo
				cQry += "SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			Else
				cQry += "SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
			EndIf
		Else
			If nSelFil == 1 .AND. Len( __aSelFil ) > 0
				If lExclusivo
					cQry += "SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
				Else 
					cQry += "SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 	
					cQry += "AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )					
				EndIf
			Else
				cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 	
				cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf		
        cQry += " AND SE1.D_E_L_E_T_ = ' ' AND "
			        
		If __lSOFEX
			If lExclusMEP
				cQry += " MEP.MEP_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' AND "
				cQry += " MEP.MEP_FILIAL = AUXFIF.FIF_CODFIL "
			Else
				cQry += " MEP.MEP_FILIAL = '" + xFilial("MEP", __cF14FilAnt) + "' "
			EndIf
		Else
			If nSelFil == 1 .and. lExclusMEP
				If Len( __aSelFil ) <= 0
					cQry += "MEP.MEP_FILIAL = '" + xFilial("MEP", __cF14FilAnt) + "' " 
				Else
					cQry += "MEP.MEP_FILIAL " + GetRngFil( __aSelFil, 'MEP', .T.,, 20, .F. )
				EndIf	
				cQry += " AND MEP.MEP_FILIAL = AUXFIF.FIF_CODFIL "
			Else
				cQry += " MEP.MEP_FILIAL = '" + xFilial("MEP", __cF14FilAnt) + "' "
			EndIf
		EndIf

		If __lSOFEX
			cQry += "AND AUXFIF.FIF_CODFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "	
		Else 
			If nSelFil == 1 .AND. Len( __aSelFil ) > 0
				cQry += " AND AUXFIF.FIF_CODFIL " + GetRngFil( __aSelFil, 'FIF', .T.,, 20, .T. )
			Else
				cQry += " AND AUXFIF.FIF_CODFIL = '" + __cF14FilAnt + "' "
			EndIf
		EndIf
		cQry += " AND MEP.MEP_MSFIL  = AUXFIF.FIF_CODFIL "
        cQry += " AND AUXFIF.FIF_PARCEL = MEP.MEP_PARTEF AND "
        cQry += " AUXFIF.FIF_DTCRED BETWEEN '" + dTos(dDataCredI) + "' AND '" + dTos(dDataCredF) + "' AND "

        If !__lProcDocTEF
            cQry += " AUXFIF.FIF_NSUTEF BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        ElseIf __lDocTef
            cQry += " AUXFIF.FIF_NUCOMP BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
        Endif

        cQry += " AUXFIF.FIF_STATUS IN ('1','4','6') AND "
			
		If !Empty(cAdmFinanIni)
			If __lSOFEX
				cQry += " AUXFIF.FIF_CODRED>='" + cAdmFinanIni + "' AND "
				If !Empty(cAdmFinanFim)
					cQry += " AUXFIF.FIF_CODRED<='" + cAdmFinanFim + "' AND "
				EndIf
			Else
				cQry += " AUXFIF.FIF_CODADM IN  " + cAdmFinanIni + " AND "
			EndIf
		EndIf

        If nTpProc == 1
            cQry += " AUXFIF.FIF_TPPROD IN ('D','V') AND "
        ElseiF	nTpProc == 2
            cQry += " AUXFIF.FIF_TPPROD = 'C' AND "
        Else
            cQry += " AUXFIF.FIF_TPPROD IN ('D','V','C') AND "
        EndIf
        cQry += " MEP.D_E_L_E_T_ = ' ' AND "
        cQry += " AUXFIF.D_E_L_E_T_ = ' ' "
        cQry += " ) AND "
        cQry += " FIF.D_E_L_E_T_ = '' "
			
        cQry += " ORDER BY "
        cQry += " RECNO_SE1 "
    EndIf

    cQry := ChangeQuery(cQry)
		
    dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQry),cAliasSitef,.F.,.T.)
				
    dbSelectArea("SA6")
    SA6->(dbSetOrder(1))

    dbSelectArea("FIF")
    FIF->(dbSetOrder(5))
		
    If !(cAliasSitef)->(Eof())
			
        While !(cAliasSitef)->(Eof())
			//Limpa o array auxiliar
            aColsAux := {}
            
			// somente verifica novamente na SX6 se MSFil mudar
			If !Empty((cAliasSitef)->E1_MSFIL)
				cCodFil := (cAliasSitef)->E1_MSFIL
			Else
				cCodFil := (cAliasSitef)->FIF_CODFIL
			EndIf

            If cMSFIL <> cCodFil .Or. cAdmAtu <> Alltrim((cAliasSitef)->FIF_CODADM)
                cFilSitef := ""
                cMVParTef := ""

				cAdmAtu := Alltrim((cAliasSitef)->FIF_CODADM)
				nPos := aScan(aParName, {|x| x[1] == cAdmAtu})
				If nPos > 0
					cMVParTef := aParName[nPos][2]
				EndIf
				cFilSitef := SuperGetMv(cMVParTef,.F.,,cCodFil)
                cMSFIL := cCodFil				
                If Empty(cFilSitef) .and. lMsgTef

                		lMsgTef:= MsgYesNo(STR0162 + Alltrim((cAliasSitef)->FIF_CODFIL) + STR0163) // "N�o existe o par�metro MV_EMPTEF para a Empresa/Filial " ## ". O movimento n�o ser� conciliado. Continua exibindo alerta?" 
                EndIf
            EndIf
				
            cValConta := ''
            
            // Efetua a carga dos bancos 
            LoadBanco(cCodFil)

            If lPontoF
                aDados := ExecBlock('FINA910F', .F., .F., {(cAliasSitef)->FIF_CODBCO, (cAliasSitef)->FIF_CODAGE, (cAliasSitef)->FIF_NUMCC})
					
                If !(ValType(aDados) == 'A')
                    cValConta := STR0103      //"CONTA NAO CADASTRADA"
                    lPontoF := .f.
                Else
                    cValConta := STR0104	//"OK"
                EndIf
            Else
                If !Empty((cAliasSitef)->FIF_CODBCO) .And. !Empty((cAliasSitef)->FIF_CODAGE) .And. !Empty((cAliasSitef)->FIF_NUMCC)
                    If !A918VLDBCO(  Right(RTrim((cAliasSitef)->FIF_CODBCO), nTamBanco), Right(RTrim((cAliasSitef)->FIF_CODAGE), nTamAgencia), Right(RTrim(StrTran((cAliasSitef)->FIF_NUMCC,"-","")), nTamCC),cCodFil)
                        cValConta := STR0103     //'CONTA NAO CADASTRADA'
                    Else
                        cValConta := STR0104     //'OK'
                    EndIf
                EndIf
            EndIf
				
            aAdd(aColsAux,'')								//1-Status oNSelec
            aAdd(aColsAux,(cAliasSitef)->FIF_CODEST)		//2-Codigo do Estabelecimento Sitef
            aAdd(aColsAux,(cAliasSitef)->FIF_CODLOJ)		//3-Codigo da Loja Sitef
            aAdd(aColsAux,(cAliasSitef)->E1_CLIENTE)		//4-Codigo do Cliente (Administradora)
            aAdd(aColsAux,(cAliasSitef)->E1_PREFIXO)		//5-Prefixo do titulo Protheus
            aAdd(aColsAux,(cAliasSitef)->E1_NUM)			//6-Numero do titulo Protheus
            aAdd(aColsAux,(cAliasSitef)->E1_TIPO)			//7-Tipo do titulo Protheus
            aAdd(aColsAux,(cAliasSitef)->E1_PARCELA)		//8-Numero da parcela Protheus
            aAdd(aColsAux,(cAliasSitef)->FIF_NUCOMP)		//9-Numero do Comprovante Sitef
            aAdd(aColsAux,Stod((cAliasSitef)->FIF_DTTEF))	//10-Data da Venda Sitef
            aAdd(aColsAux,StoD((cAliasSitef)->FIF_DTCRED))	//11-Data de Credito Sitef
            aAdd(aColsAux,(cAliasSitef)->E1_SALDO)			//12-Valor do titulo Protheus
            If __lLJGERTX 
            	aAdd(aColsAux,(cAliasSitef)->FIF_VLBRUT)	//13-Valor Bruto Sitef
            Else
            	aAdd(aColsAux,(cAliasSitef)->FIF_VLLIQ)		//13-Valor liquido Sitef
            EndIf
            aAdd(aColsAux,(cAliasSitef)->FIF_NSUTEF)		//14-Numero NSU Sitef
            aAdd(aColsAux,(cAliasSitef)->FIF_PARCEL)		//15-Numero da parcela Sitef
            aAdd(aColsAux,(cAliasSitef)->E1_DOCTEF)			//16-Documento TEF Protheus
            aAdd(aColsAux,(cAliasSitef)->E1_NSUTEF)			//17-NSU Sitef Protheus
            aAdd(aColsAux,StoD((cAliasSitef)->E1_VENCREA))		//18-Vencimento real do titulo

            If !lPontoF
                aAdd(aColsAux,(cAliasSitef)->FIF_CODBCO +' / '+(cAliasSitef)->FIF_CODAGE+' / '+(cAliasSitef)->FIF_NUMCC)//19-banco/agencia/conta
                cCodBco := RTrim( (cAliasSitef)->FIF_CODBCO )
                cCodAge := RTrim( (cAliasSitef)->FIF_CODAGE )
                cNumCC  := RTrim( (cAliasSitef)->FIF_NUMCC )
            Else
                aAdd(aColsAux,aDados[1] +' / '+aDados[2]+' / '+aDados[3])//19-banco/agencia/conta
                cCodBco := RTrim( aDados[1] )
                cCodAge := RTrim( aDados[2] )
                cNumCC  := RTrim( aDados[3] )
            EndIF

            aAdd(aColsAux,cValConta)       //20-Informa��o de conta n�o cadastrada ou cadastrada
            aAdd(aColsAux,(cAliasSitef)->FIF_CAPTUR)       //21 - (0,2 ou 3 - Transa��o Sitef),(1 ou 4 - Outros/POS)
            aAdd(aColsAux,(cAliasSitef)->RECNO_SE1)        //22 - RECNO do SE1
            aAdd(aColsAux,(cAliasSitef)->RECNO_FIF)        //23 - RECNO do FIF
				
            If !lPontoF
                aAdd(aColsAux, Right(RTrim((cAliasSitef)->FIF_CODAGE), nTamAgencia) )      //24 - Agencia
                aAdd(aColsAux, Right(RTrim((cAliasSitef)->FIF_NUMCC), nTamCC)   )     //25 - Conta
                aAdd(aColsAux, Right(RTrim((cAliasSitef)->FIF_CODBCO), nTamBanco) )      //26 - Banco
            Else
                aAdd(aColsAux, aDados[2] )     //24 - Agencia
                aAdd(aColsAux, aDados[3] )     //25 - Conta
                aAdd(aColsAux, aDados[1] )     //26 - Banco 	aDados
            EndIf

            aAdd(aColsAux,(cAliasSitef)->E1_LOJA)           	//27 - Codigo da Loja
            aAdd(aColsAux,cCodFil)								//28 - FILIAL    
            aAdd(aColsAux,(cAliasSitef)->FIF_PGJUST)        	//29 - Codigo de Justificativa
            aAdd(aColsAux,(cAliasSitef)->FIF_PGDES1)        	//30 - Descri��o de Justificativa
            aAdd(aColsAux,(cAliasSitef)->FIF_PGDES2)        	//31 - Justificativa            
            aAdd(aColsAux,(cAliasSitef)->FIF_CODADM)        	//32 - Codigo da Administradora
            aAdd(aColsAux,(cAliasSitef)->FIF_CODMAJ)        	//33 - Codigo do Motivo
			aAdd(aColsAux,(cAliasSitef)->FIF_NSUARQ)		    //34-  Numero NSU Sitef com formata��o do arquivo
			aAdd(aColsAux,(cAliasSitef)->FIF_CODAUT)		    //35-  Autoriza��o
			aAdd(aColsAux,(cAliasSitef)->FIF_TPREG)		        //36-  Tp Registro
			aAdd(aColsAux,(cAliasSitef)->FIF_NURESU)		    //37-  Nr Resumo
			aAdd(aColsAux,(cAliasSitef)->FIF_TPPROD)		    //38-  Tipo Produto
			aAdd(aColsAux,(cAliasSitef)->FIF_VLCOM)		        //39-  Vlr Comiss�o
			aAdd(aColsAux,(cAliasSitef)->FIF_TXSERV)		    //40-  Taxa Serv
			aAdd(aColsAux,(cAliasSitef)->FIF_PARALF)		    //41-  Parc.Alfanum
			aAdd(aColsAux,f918GetComb('FIF_STVEND',(cAliasSitef)->FIF_STVEND) )		    //42-  Status Venda
			aAdd(aColsAux,(cAliasSitef)->FIF_CODBAN)		    //43-  C�digo Banco
			aAdd(aColsAux,(cAliasSitef)->FIF_SEQFIF)		    //44-  Seq. Tab FIF
			aAdd(aColsAux,(cAliasSitef)->FIF_VLBRUT)		    //45-  Vlr Venda
			aAdd(aColsAux,(cAliasSitef)->FIF_VLLIQ)		        //46-  Vlr Liquido
			aAdd(aColsAux,(cAliasSitef)->FIF_FILIAL)		    //47-  Filial
			aAdd(aColsAux,(cAliasSitef)->FIF_PARC)		        //48-  Rastro Parcela SE1
						
			//Tratamento para Conciliados Manualmente
            If (cAliasSitef)->FIF_STATUS == '4'
                If A918VLDBCO(cCodBco,cCodAge,cNumCC,cCodFil) .And.; //Valido se a conta est� bloqueada, caso esteja (.F.), marco o flag como preto e mudo o status da conta para BLOQUEADA
                    AllTrim(Upper(cValConta)) == "OK"
                    aColsAux[01] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
                Else
                    aColsAux[01] := LoadBitmap(GetResources(), "BR_PRETO") //"BR_PRETO"

                    If AllTrim(Upper(cValConta)) == "OK"
                        aColsAux[20] := STR0131
                    EndIf
                EndIf
				
                aColsAux[05] := (cAliasSitef)->FIF_PREFIX //Rastro Prefixo SE1
                aColsAux[06] := (cAliasSitef)->FIF_NUM    //Rastro Num SE1
                aColsAux[08] := (cAliasSitef)->FIF_PARC   //Rastro Parcela SE1
                aColsAux[07] := (cAliasSitef)->FIF_TIPO   //Rastro Tipo SE1

				aColsAux[24] := (cAliasSitef)->FIF_PGJUST
				aColsAux[25] := (cAliasSitef)->FIF_PGDES1
				aColsAux[26] := (cAliasSitef)->FIF_PGDES2

                aAdd(aConcMan,aColsAux)
                If RecLock(cAliasTMP,.T.)
					(cAliasTMP)->SOURCE	:= PadR('CONCMAN',10,' ')
					(cAliasTMP)->RECNO		:= StrZero((cAliasSitef)->RECNO_SE1, 10)
					(cAliasTMP)->(MSUNLOCK())
                EndIf
					
            Else
				//Tratamento para os Nao Conciliados
                If Empty((cAliasSitef)->E1_NSUTEF) .And. Alltrim((cAliasSitef)->FIF_TPREG) <> '3'
						
                    aColsAux[22] := 0   //limpa o recno do se1
						
                    If A918VLDBCO(cCodBco,cCodAge,cNumCC,cCodFil) .And.; //Valido se a conta est� bloqueada, caso esteja (.F.), marco o flag como preto e mudo o status da conta para BLOQUEADA
                        AllTrim(Upper(cValConta)) == "OK"
                        aColsAux[1] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
                    Else
                        aColsAux[1]	 := LoadBitmap(GetResources(), "BR_PRETO") //"BR_PRETO"
                        If AllTrim(Upper(cValConta)) == "OK"
                            aColsAux[20] := STR0131
                        EndIf
                    EndIf

                    aAdd(aNaoConc,aColsAux)
				
				//Tratamento para divergentes
				ElseIf (cAliasSitef)->FIF_TPREG == '3 ' .OR.;
						!Empty((cAliasSitef)->E1_TIPO) .AND. !Empty((cAliasSitef)->FIF_TPPROD) .AND.;
						AllTrim((cAliasSitef)->FIF_NSUTEF) ==  PADR("", nTamNSUTEF - len(Alltrim((cAliasSitef)->E1_NSUTEF)),"0") .AND.; //Alltrim(Str(Val((cAliasSitef)->E1_NSUTEF))) .AND.; //Tratamento para retirar zeros � esquerda dos arquivos NSU SITEF
						SUBSTR((cAliasSitef)->E1_TIPO,2,1) <> (cAliasSitef)->FIF_TPPROD  
							
					aColsAux[1] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
					aColsAux[20]:= "OK"
					
					cVarAux := aColsAux[30]
					aColsAux[30]:= aColsAux[31]
					aColsAux[31]:= cVarAux
					
					aAdd(aDiverg,aColsAux)
						
				//Tratamento para os Conciliados
                ElseIf (cAliasSitef)->FIF_VLLIQ >= (cAliasSitef)->E1_SALDO - ((cAliasSitef)->E1_SALDO * (nMargem/100)) .AND.;
						AllTrim((cAliasSitef)->FIF_NSUTEF) ==  PADR("", nTamNSUTEF - len(Alltrim((cAliasSitef)->E1_NSUTEF)),"0") + Alltrim((cAliasSitef)->E1_NSUTEF) .AND.; //Tratamento para retirar zeros � esquerda dos arquivos NSU SITEF
						AllTrim((cAliasSitef)->FIF_DTTEF)  ==  AllTrim((cAliasSitef)->E1_EMISSAO) .AND.; // Data emiss�o
						AllTrim(cFilSitef) == AllTrim((cAliasSitef)->FIF_CODLOJ) .AND.; //Inserido por Carlos Queiroz em 10/02/11
						( AllTrim((cAliasSitef)->MEP_PARTEF) == AllTrim((cAliasSitef)->FIF_PARCEL ) .Or. ; // Fabiana 29/06/11 - Incluida a validacao para mep_parctf
						( Empty((cAliasSitef)->MEP_PARTEF)  .AND. ( Val((cAliasSitef)->FIF_PARCEL) ==  Val((cAliasSitef)->E1_PARCELA) .OR. ;
                        Val((cAliasSitef)->FIF_PARALF) == Val((cAliasSitef)->E1_PARCELA) ) ) )

					//-------------------------------------------------------------------------------------
					// Existe a possibilidade de existir um registro na FIF com os dados iguais
					// FIF_FILIAL / FIF_DTTEF / FIF_NSUTEF / FIF_PARCEL E FIF_CODLOJ
					// Isto ocorre por causa das transacoes feitas em POS, com isso, teremos
					// dois ou mais registros na FIF com referencia ao mesmo registro da SE1.
					// Neste caso, so iremos conciliar o primeiro que foi encontrado e o outro(s)
					// ira(ao) para a pasta de n�o conciliados.
					// Procura no array de conciliados o recno do SE1
					//-------------------------------------------------------------------------------------
                    If Empty(aConc) .or. (aConc[Len(aConc)][22] <> aColsAux[22])
						//Valido se a conta est� bloqueada, caso esteja (.F.), marco o flag como preto e mudo o status da conta para BLOQUEADA
                        If A918VLDBCO(cCodBco,cCodAge,cNumCC,cCodFil) .And. AllTrim(Upper(cValConta)) == "OK"
							SE1->(DbGoTo((cAliasSitef)->RECNO_SE1))
							If SE1->(MsRLock())	
								aColsAux[1] := LoadBitmap(GetResources(), "BR_VERDE") //"BR_BRANCO"
							else
								aColsAux[1] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
							EndIf 
                        Else
                            aColsAux[1]	 := LoadBitmap(GetResources(), "BR_PRETO") //"BR_PRETO"
                            If AllTrim(Upper(cValConta)) == "OK"
                                aColsAux[20] := STR0131
                            EndIf
                        EndIf
						
                        aAdd(aConc,aColsAux)
						
                        If RecLock(cAliasTMP,.T.)
							(cAliasTMP)->SOURCE	:= PadR('CONC',10,' ')
                            (cAliasTMP)->RECNO		:= StrZero((cAliasSitef)->RECNO_SE1, 10)
                            (cAliasTMP)->(MSUNLOCK())
                        EndIf
                    Else
						//Valido se a conta est� bloqueada, caso esteja (.F.), marco o flag como preto e mudo o status da conta para BLOQUEADA
                        If A918VLDBCO(cCodBco,cCodAge,cNumCC,cCodFil) .And. AllTrim(Upper(cValConta)) == "OK"
                            aColsAux[1] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
                        Else
                            aColsAux[1]	 := LoadBitmap(GetResources(), "BR_PRETO") //"BR_PRETO"
							
                            If AllTrim(Upper(cValConta)) == "OK"
                                aColsAux[20] := STR0131
                            EndIf
                        EndIf
					
                        aAdd(aNaoConc,aColsAux)
                    EndIf
						
					//Tratamento para os Conciliados Parcialmente
                Else
					//Valido se a conta est� bloqueada, caso esteja (.F.), marco o flag como preto e mudo o status da conta para BLOQUEADA
                    If A918VLDBCO(cCodBco,cCodAge,cNumCC,cCodFil) .And. AllTrim(Upper(cValConta)) == "OK"
                        aColsAux[1] := LoadBitmap(GetResources(), "BR_BRANCO") //"BR_BRANCO"
                    Else
                        aColsAux[1] := LoadBitmap(GetResources(), "BR_PRETO") //"BR_PRETO"
						
                        If AllTrim(Upper(cValConta)) == "OK"
                            aColsAux[20] := STR0131
                        EndIf
                    EndIf
					
                    	aAdd(aConcPar,aColsAux)  
					
                    If RecLock(cAliasTMP,.T.)
						(cAliasTMP)->SOURCE	:= PadR('CONCPAR',10,' ')
                        (cAliasTMP)->RECNO		:= StrZero((cAliasSitef)->RECNO_SE1, 10)
                        (cAliasTMP)->(MSUNLOCK())
                    EndIf
						
					//Armazena os indicadores para exibir as divergencias (Rodape Conciliados Parcialmente)
                    aAdd(aIndic,{	(cAliasSitef)->E1_SALDO			,;//1
                        			(cAliasSitef)->FIF_VLLIQ		,;//2
                        			(cAliasSitef)->E1_NSUTEF		,;//3
                        			(cAliasSitef)->FIF_NSUTEF		,;//4
                        			StoD((cAliasSitef)->E1_EMISSAO)	,;//5
                        			StoD((cAliasSitef)->FIF_DTTEF)	,;//6
                        			(cAliasSitef)->E1_PARCELA		,;//7
                        			(cAliasSitef)->FIF_PARCEL		,;//8
                        			(cAliasSitef)->E1_PREFIXO		,;//9
                        			(cAliasSitef)->E1_NUM			,;//10
                        			(cAliasSitef)->E1_TIPO 			,;//11                    
                        			(cAliasSitef)->E1_LOJA			,;//12
                        			(cAliasSitef)->E1_FILORIG		,;//13
                        			(cAliasSitef)->RECNO_SE1 		,;//14
                        			(cAliasSitef)->FIF_NSUARQ		})//15
                EndIf
            EndIf
            (cAliasSitef)->(dbSkip())
        EndDo
    EndIf

    (cAliasSitef)->(dbCloseArea())

    RestArea(aArea)
EndIf
		
//Carrega o array de Divergencias
If Len(aIndic) == 0
	aAdd(aIndic,{	0,0,"","",cTod("  /  /  "),cTod("  /  /  "),"","","","","","","",0,""})
EndIf
	
If Len(aConc) == 0 .AND. Len(aConcPar) == 0 .AND. Len(aNaoConc) == 0 .AND. Len(aDiverg) == 0
	//"N�o foram encontradas informacoes com os parametros repassados, favor verificar novamente"
	Help(" ",1,"A918NoInfo",,STR0049 ,1,0,,,,,,/*{STR0094}*/)
	lRet := .F.
EndIf
	
Return(lRet)

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918Total
Atualiza o array com o valor total dos registros                            

@type Function
@author Rafael Rosa da Silva
@since 05/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918Total( aConc, aConcPar, aNaoConc, aConcMan, aDiverg, aTotais, aHeader, aHeadMan )
Local nI   	:= 0						//Variavel para incrementar intervalo selecionado
Local nPos 	:= 0						//Variavel para verificar se existe valor no array aTotais
Local dData	:= CTOD("  /  /    ")

//Percorre o array aConc somando os registros de acordo com a data
For nI := 1 to Len(aConc)
	
	//Verifica se ja existe um registro para a data em questao
	If nPos == 0 .or. dData <> aConc[nI][11]
		dData := aConc[nI][11]
		nPos := aScan(aTotais,{|x| x[1] == aConc[nI][11] })
	EndIf
					
	If nPos == 0
		aAdd(aTotais,{	aConc[nI][11]	,;	//Data de Credito
						aConc[nI][13]	,;	//Valor Total da Coluna Conciliados
						1				,;	//Quantidade de registros da coluna Conciliados
						0				,;	//Valor Total da Coluna Conciliados Parcialmente
						0				,;	//Quantidade de registros da coluna Conciliados Parcialmente
						0				,;	//Valor Total da Coluna Conciliados Manualmente
						0				,;	//Quantidade de registros da coluna Conciliados Manualmente
						0				,;	//Valor Total da Coluna Nao Conciliados
						0				,;	//Quantidade de registros da coluna Nao Conciliados
						0				,;	//Valor Total da Coluna Divergentes
						0				,;	//Quantidade de registros da coluna Divergentes
						0})					//Total Geral
	Else
		aTotais[nPos][TOT_CONC] += aConc[nI][13] //Somatorio Conciliados
		aTotais[nPos][TOT_CONC] := Round(aTotais[nPos][TOT_CONC],2)
		aTotais[nPos][TOT_QTDCONC] += 1
	EndIf
Next nI
	
//Percorre o array aConcPar somando os registros de acordo com a data
dData	:= CTOD("  /  /    ")
nPos 	:= 0

For nI := 1 to Len(aConcPar)
	//Verifica se ja existe um registro para a data em questao
	If nPos == 0 .or. dData <> aConcPar[nI][11]
		dData := aConcPar[nI][11]
		nPos := aScan(aTotais,{|x| x[1] == aConcPar[nI][11] })
	EndIf

	If nPos == 0
		aAdd(aTotais,{	aConcPar[nI][11],;	//Data de Credito
						0				,;	//Valor Total da Coluna Conciliados
						0				,;	//Quantidade de registros da Coluna Conciliados
						aConcPar[nI][13],;	//Valor Total da Coluna Conciliados Parcialmente
						1				,;	//Quantidade de registros da coluna Conciliados Parcialmente
						0				,;	//Valor Total da Coluna Conciliados Manualmente
						0				,;	//Quantidade de registros da coluna Conciliados Manualmente
						0				,;	//Valor Total da Coluna Nao Conciliados
						0				,;	//Quantidade de registros da coluna Nao Conciliados
						0				,;	//Valor Total da Coluna Divergentes
						0				,;	//Quantidade de registros da coluna Divergentes
						0})					//Total Geral
	Else
		aTotais[nPos][TOT_CPAR] += aConcPar[nI][13] //Somatorio Conciliados Parcialmente
		aTotais[nPos][TOT_CPAR] := Round(aTotais[nPos][TOT_CPAR],2)
		aTotais[nPos][TOT_QTDCPAR] += 1
	EndIf
Next nI
	
dData	:= CTOD("  /  /    ")
nPos	:= 0

For nI := 1 to Len(aConcMan)
	//Verifica se ja existe um registro para a data em questao
	If nPos == 0 .or. dData <> aConcMan[nI][11]
		dData := aConcMan[nI][11]
		nPos := aScan(aTotais,{|x| x[1] == aConcMan[nI][11]})
	EndIf

	If nPos == 0
		aAdd(aTotais,{	aConcMan[nI][11],;	//Data de Credito
						0				,;	//Valor Total da Coluna Conciliados
						0				,;	//Quantidade de registros da Coluna Conciliados
						0				,;	//Valor Total da Coluna Conciliados Parcialmente
						0				,;	//Quantidade de registros da coluna Conciliados Parcialmente
						aConcMan[nI][13],;	//Valor Total da Coluna Conciliados Manualmente
						1				,;	//Quantidade de registros da coluna Conciliados Manualmente
						0				,;	//Valor Total da Coluna Nao Conciliados
						0				,;	//Quantidade de registros da coluna Nao Conciliados
						0				,;	//Valor Total da Coluna Divergentes
						0				,;	//Quantidade de registros da coluna Divergentes
						0})					//Total Geral
	Else
		aTotais[nPos][TOT_CMAN] += aConcMan[nI][13] //Somatorio Conciliados Manualmente
		aTotais[nPos][TOT_CMAN] := Round(aTotais[nPos][TOT_CMAN],2)
		aTotais[nPos][TOT_QTDCMAN] += 1
	EndIf
Next nI

//Percorre o array aNaoConc somando os registros de acordo com a data
dData	:= CTOD("  /  /    ")
nPos	:= 0

For nI := 1 to Len(aNaoConc)
	//Verifica se ja existe um registro para a data em questao       
	If nPos == 0 .or. dData <> aNaoConc[nI][11]
		dData := aNaoConc[nI][11]
		nPos := aScan(aTotais,{|x| x[1] == aNaoConc[nI][11] })
	EndIf

	If nPos == 0
		aAdd(aTotais,{	aNaoConc[nI][11],;	//Data de Credito
						0				,;	//Valor Total da Coluna Conciliados
						0				,;	//Quantidade de registros da Coluna Conciliados
						0				,;	//Valor Total da Coluna Conciliados Parcialmente
						0				,;	//Quantidade de registros da coluna Conciliados Parcialmente
						0				,;	//Valor Total da Coluna Conciliados Manualmente
						0				,;	//Quantidade de registros da coluna Conciliados Manualmente
						aNaoConc[nI][13],;	//Valor Total da Coluna Nao Conciliados
						1				,;	//Quantidade de registros da coluna Nao Conciliados
						0				,;	//Valor Total da Coluna Divergentes
						0				,;	//Quantidade de registros da coluna Divergentes
						0})					//Total Geral
	Else
		aTotais[nPos][TOT_CNAO] += aNaoConc[nI][13] //Somatorio Nao Conciliados
		aTotais[nPos][TOT_CNAO] := Round(aTotais[nPos][TOT_CNAO],2)
		aTotais[nPos][TOT_QTDCNAO] += 1
	EndIf
Next nI

dData	:= CTOD("  /  /    ")
nPos	:= 0

For nI := 1 to Len(aDiverg)
	//Verifica se ja existe um registro para a data em questao
	If nPos == 0 .or. dData <> aDiverg[nI][11]
		dData := aDiverg[nI][11]
		nPos := aScan(aTotais,{|x| x[1] == aDiverg[nI][11]})
	EndIf

	If nPos == 0
		aAdd(aTotais,{	aDiverg[nI][11],;	//Data de Credito
						0				,;	//Valor Total da Coluna Conciliados
						0				,;	//Quantidade de registros da Coluna Conciliados
						0				,;	//Valor Total da Coluna Conciliados Parcialmente
						0				,;	//Quantidade de registros da coluna Conciliados Parcialmente
						0				,;	//Valor Total da Coluna Conciliados Manualmente
						0				,;	//Quantidade de registros da coluna Conciliados Manualmente
						0				,;	//Valor Total da Coluna Nao Conciliados
						0				,;	//Quantidade de registros da coluna Nao Conciliados
						aDiverg[nI][13]	,;	//Valor Total da Coluna Divergentes
						1				,;	//Quantidade de registros da coluna Divergentes
						0})					//Total Geral
	Else
		aTotais[nPos][TOT_DIV] += aDiverg[nI][13] //Somatorio Conciliados Manualmente
		aTotais[nPos][TOT_DIV] := Round(aTotais[nPos][TOT_DIV],2)
		aTotais[nPos][TOT_QTDDIV] += 1
	EndIf
Next nI
		
//Percorre o array aConcMan somando os registros de acordo com a data
For nI := 1 to Len(aTotais)
	aTotais[nI][TOT_GERAL] := aTotais[nI][TOT_CONC]+aTotais[nI][TOT_CPAR]+aTotais[nI][TOT_CMAN]+aTotais[nI][TOT_CNAO]+aTotais[nI][TOT_DIV]
	aTotais[nI][TOT_GERAL] := Round(aTotais[nI][TOT_GERAL],2)
Next nI
	
	//caso nao exista nenhum registro de somatoria, crio uma linha em branco para evitar erro
If Len(aTotais) == 0
	aAdd(aTotais,{cTod("  /  /  "),0,0,0,0,0,0,0,0,0,0,0})
EndIf
	
Return

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918Titulos
Funcao que retorna todos os titulos do contas a receber que possuam dados 
parecidos com os recebidos do arquivo de Conciliacao do SITEF                                       

@type Function
@author Rafael Rosa da Silva
@since 06/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918Titulos( aDados, nLinha, oSelec, aTitulos, oTitulos, oNSelec, aConc, aConcPar, lWhenConc )
Local cQry	    		:= ""						//Instrucao de query no banco
Local cAliasSitef    	:= GetNextAlias()         	//Variavel que recebe o proximo Alias disponivel
Local nNaoConc			:= 1						//Verificador se titulo possui vinculo com arquivos Sitef
Local nNaoConcPa		:= 1						//Verificador se titulo possui vinculo com arquivos Sitef
Local lExclusivo		:= !Empty(xFilial("SE1"))	//Verifica se SE1 esta' compartilhada ou exclusiva
Local lExclusFIF		:= !Empty(xFilial("FIF"))	//Verifica se FIF esta' compartilhada ou exclusiva
Local lExclusMEP		:= !Empty(xFilial("MEP"))	//Verifica se MEP esta' compartilhada ou exclusiva
Local cSubstring		:= ""
Local lUsaMep       	:= SuperGetMv("MV_USAMEP",.T.,.F.)
Local lOracle 		:= .F.
Local lPostGre			:= .F. 
Local cTamNSU			:= Alltrim(Str(nTamNSUTEF))

If __lDefTop == Nil
	__lDefTop := FindFunction("IFDEFTOPCTB") .And. IfDefTopCTB() 
EndIf
	
If !Empty(aDados[nLinha][2]) .AND. !Empty(aDados[nLinha][3])
    If __lDefTop
        If ( AllTrim( Upper( TcGetDb() ) ) $ "ORACLE_INFORMIX" )
            cSubstring := "SUBSTR"
            lOracle 	  := .T.
        ElseIf ( AllTrim( Upper( TcGetDb() ) ) $ "DB2|DB2/400")
            cSubstring := "SUBSTR"
        Else
			If ( AllTrim( Upper( TcGetDb() ) ) $ "POSTGRES")
				lPostGre := .T.
			EndIf
            cSubstring := "SUBSTRING"
        EndIf
			
		//Query para buscar os titulos no financeiro semelhantes aos titulos Sitef
        If !lMEP .or. !lUsaMep
            cQry := "SELECT SE1.E1_EMISSAO, SE1.E1_VENCREA, SE1.E1_NSUTEF, SE1.E1_DOCTEF ,SE1.E1_PREFIXO, SE1.E1_NUM, SE1.E1_FILORIG, "
            cQry += "SE1.E1_PARCELA, SE1.E1_TIPO, SE1.E1_CLIENTE, SE1.E1_LOJA, SE1.E1_VALOR, SE1.E1_SALDO, SE1.E1_MSFIL, SE1.E1_FILIAL, SE1.R_E_C_N_O_ RECNOSE1, "

            cQry += " '"+ Space(nTamParc) +"' AS MEP_PARTEF "
            cQry += "FROM " + RetSqlName("SE1") + " SE1 "

            cQry += "WHERE SE1.D_E_L_E_T_=' ' "
			If __lSOFEX	
				If lExclusivo
					cQry += "AND SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				Else
					cQry += "AND SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				EndIf
			Else
				If nSelFil == 1 .and. Len( __aSelFil ) > 0
					If lExclusivo
						cQry += " AND SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
					Else 
						cQry += " AND SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "	
						cQry += " AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )
					EndIf	
				Else
					cQry += " AND SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "
				EndIf	
			EndIf
			
            If	nTpProc == 1
                cQry += "AND SE1.E1_TIPO = 'CD' "
            ElseIf	nTpProc == 2
                cQry += "AND SE1.E1_TIPO = 'CC' "
            Else
                cQry += "AND SE1.E1_TIPO IN ('CC','CD') "
            EndIf
				
            cQry += "AND SE1.E1_VENCREA  BETWEEN '" + dTos(dDataCredI - nQtdDias) + "' AND '" + dTos(dDataCredF) + "' "
            cQry += "AND SE1.E1_SALDO > 0 "
            cQry += "ORDER BY SE1.E1_VALOR "
        Else
            cQry += " SELECT SE1.E1_EMISSAO, SE1.E1_VENCREA, SE1.E1_NSUTEF, SE1.E1_DOCTEF, SE1.E1_PREFIXO, SE1.E1_NUM, SE1.E1_PARCELA, SE1.E1_TIPO, SE1.E1_FILORIG, "
            cQry += " SE1.E1_CLIENTE, SE1.E1_LOJA, SE1.E1_VALOR, SE1.E1_SALDO, SE1.E1_MSFIL, SE1.E1_FILIAL,	SE1.R_E_C_N_O_ RECNOSE1, MEP.MEP_PARTEF "
            cQry += " FROM " + RetSqlName("SE1") + " SE1 LEFT JOIN " + RetSqlName("MEP") + " MEP ON "

			If __lSOFEX	
				If lExclusivo
					cQry += " SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				Else
					cQry += " SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				EndIf
			Else
				If nSelFil == 1 .and. Len( __aSelFil ) > 0
					If lExclusivo
						cQry += " SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
					Else 
						cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "	
						cQry += " AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )
					EndIf	
				Else
					cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "
					cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' "
				EndIf	
			EndIf

            cQry += " AND SE1.E1_FILIAL = MEP.MEP_FILIAL "
            cQry += " AND SE1.E1_PREFIXO = MEP.MEP_PREFIX "
            cQry += " AND SE1.E1_NUM = MEP.MEP_NUM "
            cQry += " AND SE1.E1_PARCELA = MEP.MEP_PARCEL "
            cQry += " AND SE1.E1_TIPO = MEP.MEP_TIPO "
			
            If !lExclusMEP
                cQry += " AND MEP.MEP_FILIAL = '' "
            EndIf
			
            cQry += " AND MEP.D_E_L_E_T_ = ''  "
            
			cQry += " WHERE "

			If __lSOFEX	
				If lExclusivo
					cQry += " SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				Else
					cQry += " SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' "
				EndIf
			Else
				If nSelFil == 1 .and. Len( __aSelFil ) > 0
					If lExclusivo
						cQry += " SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. )
					Else 
						cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "	
						cQry += " AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. )
					EndIf	
				Else
					cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "
					cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' "
				EndIf	
			EndIf			
		
            cQry += " AND SE1.E1_VENCREA BETWEEN '" + DtoS(dDataCredI - nQtdDias) + "' AND '" + DtoS(dDataCredF) + "' "
            cQry += " AND SE1.E1_TIPO IN ('CC','CD') "
            cQry += " AND SE1.E1_SALDO > 0 "

            If !__lProcDocTEF
                cQry += " AND SE1.E1_NSUTEF NOT IN ( "
                cQry += " SELECT AUXFIF.FIF_NSUTEF "
            ElseIf __lDocTef
                cQry += " AND SE1.E1_DOCTEF NOT IN ( "
                cQry += " SELECT AUXFIF.FIF_NUCOMP "
            Endif

            cQry += " FROM " + RetSqlName("SE1") + " SE1 "

            cQry += " JOIN " + RetSqlName("FIF") + " AUXFIF "
            cQry += " ON SE1.E1_MSFIL = AUXFIF.FIF_CODFIL "

            If !__lProcDocTEF
            	If __lSOFEX
					cQry += " AND SE1.E1_NSUTEF = AUXFIF.FIF_NSUTEF "
				Else
			 		If lOracle .OR. lPostGre
						cQry += "AND LPAD(TRIM(SE1.E1_NSUTEF), "+cTamNSU+", '0') = LPAD(TRIM(AUXFIF.FIF_NSUTEF), "+cTamNSU+", '0') "
			 		Else
						cQry += "AND REPLICATE('0', "+cTamNSU+" - LEN(SE1.E1_NSUTEF)) + RTrim(SE1.E1_NSUTEF) =  AUXFIF.FIF_NSUTEF "
					Endif	
				Endif                                                       
        	ElseIf __lDocTef
            	cQry += " AND CAST(SE1.E1_DOCTEF AS INT) = CAST(AUXFIF.FIF_NUCOMP AS INT) "
            Endif

            cQry += " AND SE1.E1_EMISSAO = AUXFIF.FIF_DTTEF "

            cQry += " JOIN " + RetSqlName("MEP") + " MEP "
            cQry += " ON SE1.E1_FILIAL = MEP.MEP_FILIAL "
            cQry += " AND SE1.E1_PREFIXO = MEP.MEP_PREFIX "
            cQry += " AND SE1.E1_NUM = MEP.MEP_NUM "
            cQry += " AND SE1.E1_PARCELA = MEP.MEP_PARCEL "
            cQry += " AND SE1.E1_TIPO = MEP.MEP_TIPO "
            cQry += " AND SE1.E1_MSFIL = MEP.MEP_MSFIL "

            cQry += " WHERE "
            If !lExclusFif
                cQry += " AUXFIF.FIF_FILIAL = '" + xFilial("FIF", __cF14FilAnt) + "' AND "
            EndIf

			If __lSOFEX	
				If lExclusivo
					cQry += " SE1.E1_FILIAL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' AND "
				Else
					cQry += " SE1.E1_MSFIL BETWEEN '" + cFilIni + "' AND '" + cFilFim + "' AND "
				EndIf
			Else
				If nSelFil == 1 .and. Len( __aSelFil ) > 0
					If lExclusivo
						cQry += " SE1.E1_FILIAL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .F. ) + " AND "
					Else 
						cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "	
						cQry += " AND SE1.E1_MSFIL " + GetRngFil( __aSelFil, 'SE1', .T.,, 20, .T. ) + " AND "
					EndIf	
				Else
					cQry += " SE1.E1_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' "
					cQry += " AND SE1.E1_MSFIL = '" + __cF14FilAnt + "' AND  "
				EndIf	
			EndIf

            If nTpProc == 1
                cQry += " SE1.E1_TIPO = 'CD' AND "
            ElseIf	nTpProc == 2
                cQry += " SE1.E1_TIPO = 'CC' AND "
            Else
                cQry += " SE1.E1_TIPO IN ('CD','CC') AND "
            EndIf
	
            cQry += " SE1.E1_SALDO > 0 AND "
							
            cQry += " SE1.D_E_L_E_T_ = ' ' AND "
            
            If nSelFil == 1 .and. lExclusMEP
				If Len( __aSelFil ) <= 0
					cQry += " MEP.MEP_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' " 
				Else
					cQry += " MEP.MEP_FILIAL " + GetRngFil( __aSelFil, 'MEP', .T.,, 20, .F. )
				EndIf	
				cQry += " AND MEP.MEP_FILIAL = AUXFIF.FIF_CODFIL AND "
			Else
				cQry += " MEP.MEP_FILIAL = '" + xFilial("SE1", __cF14FilAnt) + "' AND "
			EndIf
            
            If nSelFil == 1 .and. Len( __aSelFil ) > 0
				cQry += " AUXFIF.FIF_CODFIL " + GetRngFil( __aSelFil, 'FIF', .T.,, 20, .T. ) + " AND "
			Else
				cQry += " AUXFIF.FIF_CODFIL = '" + __cF14FilAnt + "' AND " 
			EndIf
				
            cQry += " AUXFIF.FIF_PARCEL = MEP.MEP_PARTEF AND "
            cQry += " AUXFIF.FIF_DTCRED BETWEEN '" + dTos(dDataCredI) + "' AND '" + dTos(dDataCredF) + "' AND "

            If !__lProcDocTEF
                cQry += " AUXFIF.FIF_NSUTEF BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
            ElseIf __lDocTef
                cQry += " AUXFIF.FIF_NUCOMP BETWEEN '" + cNSUIni + "' AND '" + cNSUFim + "' AND "
            Endif

            cQry += " AUXFIF.FIF_STATUS IN ('1','3','4','6') AND "
							
			If !Empty(cAdmFinanIni)
				If __lSOFEX
					cQry += " AUXFIF.FIF_CODRED IN  " + cAdmFinanIni + " AND "
				Else
					cQry += " AUXFIF.FIF_CODADM IN  " + cAdmFinanIni + " AND "
				EndIf
			EndIf

            If nTpProc == 1
                cQry += " AUXFIF.FIF_TPPROD IN ('D','V') AND "
            ElseiF	nTpProc == 2
                cQry += " AUXFIF.FIF_TPPROD = 'C' AND "
            Else
                cQry += " AUXFIF.FIF_TPPROD IN ('D','V','C') AND "
            EndIf
			
            cQry += " MEP.D_E_L_E_T_ = ' ' AND "
            cQry += " AUXFIF.D_E_L_E_T_ = ' ' ) "
            cQry += " AND SE1.D_E_L_E_T_ = ' ' "
                                      
            cQry += " ORDER BY SE1.E1_VALOR "
        EndIf

        cQry := ChangeQuery(cQry)
        dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQry),cAliasSitef,.F.,.T.)
			
        While !(cAliasSitef)->(Eof())
				
			//Verifica se o titulo retornado encontra-se na pasta de conciliados
			//Se existir, nao sera exibido na pasta de nao conciliados
            nNaoConc := If( (cAliasTMP)->(MSSeek(PadR('CONC',10)+STRZERO((cAliasSitef)->RECNOSE1,10))), 1, 0 )
				
			//Verifica se o titulo retornado encontra-se na pasta de conciliados
			//Se existir, nao sera exibido na pasta de conciliados parcialmente
            nNaoConcPa := If( (cAliasTMP)->(MSSeek(PadR('CONCPAR',10)+STRZERO((cAliasSitef)->RECNOSE1,10))), 1, 0 )
				
            If (nNaoConc + nNaoConcPa) = 0 //Se nao encontrar o item e Nao Conciliado
					
                aColsAux := {}
					
                aAdd(aColsAux,LoadBitmap(GetResources(), "BR_BRANCO"))	//1-Status
                aAdd(aColsAux,StoD((cAliasSitef)->E1_EMISSAO))				//2-Emissao do Titulo
                aAdd(aColsAux,StoD((cAliasSitef)->E1_VENCREA))				//3-Vencimento real do titulo
                aAdd(aColsAux,(cAliasSitef)->E1_SALDO)						//4-Valor do titulo
                aAdd(aColsAux,(cAliasSitef)->E1_NSUTEF)						//5-Numero NSU Sitef
                aAdd(aColsAux,(cAliasSitef)->E1_DOCTEF)						//6-Numero Documento Sitef
                aAdd(aColsAux,(cAliasSitef)->E1_PREFIXO)					//7-Prefixo do Titulo
                aAdd(aColsAux,(cAliasSitef)->E1_NUM)							//8-Numero do Titulo
                aAdd(aColsAux,(cAliasSitef)->E1_PARCELA)					//9-Parcela do titulo
                aAdd(aColsAux,(cAliasSitef)->E1_TIPO)						//10-Tipo do titulo
                aAdd(aColsAux,(cAliasSitef)->E1_CLIENTE)					//11-Cliente (Administradora)
                aAdd(aColsAux,(cAliasSitef)->MEP_PARTEF )         			//12 Parcela Sitef
                aAdd(aColsAux,(cAliasSitef)->E1_LOJA)						//13-Loja da venda
                aAdd(aColsAux,(cAliasSitef)->E1_MSFIL)						//14-Loja da venda
                aAdd(aColsAux,(cAliasSitef)->RECNOSE1)						//15-RECNO DO TITULO
                aAdd(aColsAux,(cAliasSitef)->E1_VALOR)						//16-vALOR DO TITULO
                aAdd(aColsAux,(cAliasSitef)->E1_FILORIG)	       		//17-FILIAL DA SE1
					
                If Len(aColsAux) > 1
                    aAdd(aTitulos,aColsAux)
                EndIf
            EndIf
				
            (cAliasSitef)->(dbSkip())
        EndDo

        (cAliasSitef)->(dbCloseArea())
    EndIf
EndIf
	
//Atualiza objeto oTitulos com as informacoes encontradas na consulta
If Len(aTitulos) > 0
    oTitulos:SetArray(aTitulos)
    oTitulos:bLine := {||aEval(aTitulos[oTitulos:nAt],{|z,w| aTitulos[oTitulos:nAt,w]})}
    oTitulos:Refresh()
    lWhenConc := .T.
Else //Se nao encontrar titulos carrega array para evitar erro no objeto
    aAdd(aTitulos,{"",cTod("  /  /  "),cTod("  /  /  "),0,"","","","","","","","","","",0})
		
    oTitulos:SetArray(aTitulos)
    oTitulos:bLine := {||aEval(aTitulos[oTitulos:nAt],{|z,w| aTitulos[oTitulos:nAt,w]})}
    oTitulos:Refresh()
    lWhenConc := .F.
EndIf
	
Return

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918Efetiva
Funcao que � executada a partir do botao de efetivar conciliacao dos Folders 
Conciliadas, Conc. Parcialmente e N�o Conciliadas, tendo como funcionalidade  
baixar os titulos do Contas a Receber e alterar o status do mesmo na tabela 
FIF 

@type Function
@author Unknown
@since 28/04/2011
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918Efetiva( aDados, oObj, oRedSelec, oAmaSelec, aTitulos, aConcMan, oConcMan, aTotais, oTotais, nTpConc, oTitulos, oDlg )
Local cMensagem := ''	//Mensagem utilizada na funcao processa
Local lContinua := .T.
	
If cConcilia == 2
    //Baixa por lote
	cMensagem := STR0093
	If !__lBxCnab
		MsgInfo(STR0203) //"A concilia��o n�o poder� ser efetuada. Para executar a baixa em lote o par�metro MV_BXCNAB deve estar configurado como 'S' ."
		lContinua := .F.
	End 
Else
	//"Baixa de T�tulos Individual..."
	cMensagem := STR0105
EndIf

If lContinua
	ProcLogAtu(STR0138,STR0054)
	
	Do Case
		//Efetivacao dos conciliados e conciliados parcial					
		Case nTpConc == 1 .OR. nTpConc == 2
	       ProcLogAtu(STR0138,STR0144)
			Processa(	{|| A918EfConc(aDados,oObj,oDlg,oRedSelec,oAmaSelec,aTotais,oTotais,nTpConc)},;
						STR0092,cMensagem)				//"Aguarde..."	### "Preparando Dados para Baixa..."
	
		//Efetiva��o dos n�o conciliados (Manualmente)										
		Case nTpConc == 3
	       ProcLogAtu(STR0138,STR0145)
			Processa(	{|| A918EfNaoConc(aDados,oObj,aTitulos,oDlg,aConcMan,oConcMan,oRedSelec,oTitulos,aTotais,oTotais)},;
						STR0092, cMensagem)				//"Aguarde..."	### "Preparando Dados para Baixa..."
		
		//Efetiva��o dos Divergentes
		Case nTpConc == 4
	       ProcLogAtu(STR0138,STR0144)
			Processa(	{|| fA918DvConc(aDados,oObj,aTotais,oTotais)},;
						STR0092,cMensagem)				//"Aguarde..."	### "Preparando Dados para Baixa..."
	EndCase
	
	ProcLogAtu(STR0138,STR0146)
EndIf

Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918EfNaoConc
Funcao que � executada a partir do botao de efetivar conciliacao dos Folders 
N�o Conciliadas, tendo como funcionalidade baixar os titulos do Contas a  
Receber e alterar o status do mesmo na tabela FIF 

@type Function
@author Unknown
@since 06/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918EfNaoConc( aNaoConc, oNaoConc, aTitulos, oDlg, aConcMan, oConcMan, oRedSelec, oTitulos, aTotais, oTotais )
Local nPos			:= 0		//Posicao do titulo selecionado
Local nCount		:= 0		//Contador utilizado para varrer o array da FIF
Local aLotes		:= {}		//Array com os lotes para baixa
Local lRet			:= .F.		//Retorno da fun��o de baixa individual
Local cFilOri		:= cFilAnt  
Local aDadoBanco	:= {}		//Array com os dados de banco, agencia e conta
Local nLote			:= 0		//Posicao do lote no array
Local aTitInd		:= {}		//Dados do titulo individual  
Local nPTot			:= 0		//Posicao da linha da aba totais, referente a data credito posicionada
	
//Verifica se algum item foi selecionado nos titulos
If (nPos := aScan(aTitulos,{|x| x[1]:cName == "BR_VERDE" })) == 0
	//"Nao ha' nenhum registro marcado"
	Help(" ",1,"A918NoSelec",,STR0051 ,1,0,,,,,,{STR0221})
	//"Selecione ao menos um registro para que seja conciliado"
	Return Nil
EndIf

ProcRegua(Len(aNaoConc))
	
DbSelectArea("SE1")
SE1->(DbSetOrder(1)) //E1_FILIAL + E1_PREFIXO + E1_NUM + E1_PARCELA + E1_TIPO
	                                                                                   
For nCount := 1 To Len(aNaoConc)

	//Verifica se o item esta selecionado
	If AllTrim(oNaoConc:aArray[nCount][1]:cName) == "BR_VERDE"
		
		//Procura no array de titulos n�o conciliados o recno correspondente a FIF
		If (nPos := aScan(aTitulos,{|x| x[15] == oNaoConc:aArray[nCount][22] })) <> 0
			cFilAnt := aTitulos[nPos][17]
		
			//Verifica se existe o registro no SE1
			If !SE1->(dbSeek(xFilial("SE1")+aTitulos[nPos][7]+aTitulos[nPos][8]+aTitulos[nPos][9]+aTitulos[nPos][10]))
				//"Arquivo n�o encontrado no Financeiro"
				Help(" ",1,"A918NoSelec",,STR0050 ,1,0,,,,,,{STR0222})
				//Selecione um t�tulo valido no Financeiro
			Else
				//Lote - prepara os dados para baixa dos titulos em lote
				//Baixa Individual - prepara e ja executa a baixa do titulo.
				lRet := .F.

				//Busca banco / agencia / conta. Efetua atualiza��o do SE1
				aDadoBanco := BuscarBanco(oNaoConc:aArray[nCount][26], oNaoConc:aArray[nCount][24],oNaoConc:aArray[nCount][25],oNaoConc:aArray[nCount][13])
					
				//Conciliacao por lote
				If cConcilia == 2
						
					//Verifica se ja existe um lote para este Banco, Agencia, Conta e Data do Credito
					If Len(aLotes) > 0 .AND. ;
						(nLote := AScan (aLotes, {|aX| aX[2] + aX[3] + aX[4] + DToS(aX[8]) == oNaoConc:aArray[nCount][26] + oNaoConc:aArray[nCount][24] + oNaoConc:aArray[nCount][25] + DToS(Iif (lUseFIFDtCred, oNaoConc:aArray[nCount][11],dDataBase))})) > 0							

						//Adiciona ao lote o recno de mais um titulo SE1
						AADD(aLotes[nLote ][9], oNaoConc:aArray[nCount][22])
					Else
						//Busca o numero do lote
						cLote := BuscarLote()
							
						//Cria um lote com o titulo SE1
						AADD(	aLotes, {cLote, oNaoConc:aArray[nCount][26], oNaoConc:aArray[nCount][24], oNaoConc:aArray[nCount][25], aDadoBanco[1], aDadoBanco[2], ;
								aDadoBanco[3], Iif (lUseFIFDtCred, oNaoConc:aArray[nCount][11],dDataBase), {oNaoConc:aArray[nCount][22]}})
					EndIf
						
					lRet := .T.
				Else
					//Array com os dados do titulo para baixa individual
					aTitInd	:= {{"E1_PREFIXO"	,oNaoConc:aArray[nCount][5]	 	 																				,NiL},;
								{"E1_NUM"		,oNaoConc:aArray[nCount][6]			 																			,NiL},;
								{"E1_PARCELA"	,oNaoConc:aArray[nCount][8]	     																				,NiL},;
								{"E1_TIPO"		,oNaoConc:aArray[nCount][7]       	 																			,NiL},;
								{"E1_CLIENTE"	,oNaoConc:aArray[nCount][4]     																				,NiL},;
								{"E1_LOJA"		,oNaoConc:aArray[nCount][27]       	 																			,NiL},;
								{"AUTMOTBX"		,"NOR"					     			 																		,Nil},;
								{"AUTBANCO"		,(PadR(aDadoBanco[1],Len(SE8->E8_BANCO)))   																	,Nil},;
								{"AUTAGENCIA"	,(PadR(aDadoBanco[2],Len(SE8->E8_AGENCIA)))																		,Nil},;
								{"AUTCONTA"		,If(lPontoF,(PadR(aDadoBanco[3],Len(SE8->E8_CONTA))),(PadR(StrTran(aDadoBanco[3],"-",""),Len(SE8->E8_CONTA))))	,Nil},;
								{"AUTDTBAIXA"	,Iif (lUseFIFDtCred, oNaoConc:aArray[nCount][11],dDataBase)														,Nil},;
								{"AUTDTCREDITO"	,Iif (lUseFIFDtCred, oNaoConc:aArray[nCount][11],dDataBase)														,Nil},;
								{"AUTHIST"		,STR0110			           			 																		,Nil},; //"Conciliador SITEF"
								{"AUTDESCONT"	,0					    	 			 																		,Nil},; //Valores de desconto
								{"AUTACRESC"	,0					 	    			 																		,Nil},; //Valores de acrescimo - deve estar cadastrado no titulo previamente
								{"AUTDECRESC"	,0					 		 			 																		,Nil},; //Valore de decrescimo - deve estar cadastrado no titulo previamente
								{"AUTMULTA"		,0					 		 			 																		,Nil},; //Valores de multa
								{"AUTJUROS"		,0					 				 	 																		,Nil},; //Valores de Juros
								{"AUTVALREC"	,oNaoConc:aArray[nCount][13]   	 																				,Nil}}  //Valor recebido
												
					//Efetua baixa individual
					A918EfetuaBX(aTitInd, oDlg, , @lRet)
				EndIf

				//TRECHO NOVO EXPERIMENTAL
				cFilAnt := cFilOri
				
				If lRet
					//Atualiza Folder Conciliados Manualmente Dinamicamente
					A918AtuMan(@aConcMan, @oConcMan, aTitulos, aNaoConc, nPos, nCount, oRedSelec, @oNaoConc, @oTitulos)
						
					//Atualizo folder de totais dinamicamente
					If (nPTot := aScan(aTotais,{|x| x[TOT_DATA] == oNaoConc:aArray[nCount][11] })) <> 0
	
						aTotais[nPTot][TOT_CNAO] := aTotais[nPTot][TOT_CNAO] - aNaoConc[nCount][13]
						aTotais[nPTot][TOT_CMAN] := aTotais[nPTot][TOT_CMAN] + aNaoConc[nCount][13]
						aTotais[nPTot][TOT_QTDCNAO]--
							
						If Len(aConcMan) > 0
								
							//Atualizacao do Objeto na Aba Nao Conciliados	
							If Empty(aConcMan[1][2])
								aDel(aConcMan,1)
								aSize(aConcMan,Len(aConcMan)-1)
								oConcMan:SetArray(aConcMan)
								oConcMan:bLine := {||aEval(aConcMan[oConcMan:nAt],{|z,w| aConcMan[oConcMan:nAt,w]})}
								oConcMan:Refresh()
							EndIf
								
							aTotais[nPTot][TOT_QTDCMAN] := Len(aConcMan)
						EndIf
		
						aTotais[nPTot][TOT_GERAL] := aTotais[nPTot][TOT_CONC]+aTotais[nPTot][TOT_CPAR]+aTotais[nPTot][TOT_CNAO]+aTotais[nPTot][TOT_CMAN]+aTotais[nPTot][TOT_DIV]
						aTotais[nPTot][TOT_GERAL] := Round(aTotais[nPTot][TOT_GERAL],2)
					
					EndIf
					//Inserir AtuTot
					aNaoConc[nCount][1]:cName := "BR_VERMELHO"
				Else
					aNaoConc[nCount][1]:cName := "BR_AMARELO"
				EndIf
					
			EndIf
		Else
			//"Nao ha' nenhum registro marcado"
			Help(" ",1,"A918NoSelec",,STR0051 ,1,0,,,,,,{STR0221})
			//"Selecione ao menos um registro para que seja conciliado"
		EndIf
	EndIf
		
	IncProc()
Next
	
oNaoConc:Refresh()
oTotais:Refresh()
	
	//Efetua a baixa por lote
If cConcilia == 2
	Processa({|| A918EfetuaBX (aLotes, oDlg, aNaoConc, @lRet)},STR0106,STR0107) //"Aguarde..."#"Efetuando Baixa de T�tulos Lote..."

	If !lRet
		Aeval(aNaoConc, {|x| x[1]:cName := IIF(x[1]:cName == "BR_VERMELHO", "BR_AMARELO", x[1]:cName)})
		oNaoConc:Refresh()
	EndIf
EndIf

Processa({|| F918AtuFIF (aNaoConc,"4")},STR0106,STR0108) //"Aguarde..."#"Atualizando Tabela FIF..."

If lRet
	MsgInfo(STR0109) //"Baixa efetuada com sucesso!!!"
EndIf
	
Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918EfConc
Funcao que � executada a partir do botao de efetivar conciliacao dos Folders  
Conciliadas e Conc. Parcialmente, tendo como funcionalidade baixar os titulos
do Contas a Receber e alterar o status do mesmo na tabela FIF 

@type Function
@author Unknown
@since 06/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918EfConc( aConc, oConc, oDlg, oRedSelec, oAmaSelec, aTotais, oTotais, nTpConc )
Local nCount		:= 0	//Contador utilizado para varrer o array da FIF
Local aLotes		:= {}	//Array com os lotes para baixa
Local lRet			:= .F.	//Retorno de Execu��o da fun��o de baixa individual
Local lMenorSitef	:= .F.	//Variavel para controlar a exibi��o de mensagem de valor menor sitef
Local lExist		:= .F.
Local aDadoBanco	:= {}	//Array com os dados de banco, agencia e conta
Local nLote			:= 0	//Posicao do lote no array
Local aTitInd		:= {}	//Dados do titulo individual
Local nPTot			:= 0		//Posicao da linha da aba totais, referente a data credito posicionada

If Len(aConc) <= 1 .And. Empty(aConc[1][6])
	//"N�o existem registros a conciliar"
	Help(" ",1,"A918NoConc",,STR0147 ,1,0,,,,,,{STR0221})
	//"Selecione ao menos um registro para que seja conciliado"
	Return Nil
EndIf
	
ProcRegua(Len(aConc))
	
DbSelectArea("SE1")
SE1->(DbSetOrder(1)) //E1_FILIAL + E1_PREFIXO + E1_NUM + E1_PARCELA + E1_TIPO
	
For nCount := 1 To Len(aConc)
    If cFilAnt<>aConc[nCount][28]
		cFilAnt := aConc[nCount][28]
	EndIf		
		//Verifica se o item esta selecionado
	If AllTrim(oConc:aArray[nCount][1]:cName) == "BR_VERDE"

		//Verifica se existe o registro no SE1
		If !SE1->(DbSeek(xFilial("SE1")+aConc[nCount][5]+aConc[nCount][6]+aConc[nCount][8]+aConc[nCount][7]))
			//"Arquivo n�o encontrado no Financeiro"
			Help(" ",1,"A918NoSelec",,STR0050 ,1,0,,,,,,{STR0222})
			//Selecione um t�tulo valido no Financeiro
		Else
			lExist := .T.
			//Tratamento Valor Sitef deve ser maior ou igual ao Valor no Financeiro
			If !(aConc[nCount][13] >= SE1->E1_SALDO - (SE1->E1_SALDO * (nMargem/100))) .and. nTpConc == 1
				lMenorSitef := .T.
				aConc[nCount][01] := oAmaSelec
				Loop
			Else
				//Lote - prepara os dados para baixa dos titulos em lote
				//Baixa Individual - prepara e ja executa a baixa do titulo.
				lRet := .F.
				
				//Busca banco / agencia / conta. Efetua atualiza��o do SE1
				aDadoBanco := BuscarBanco(oConc:aArray[nCount][26], oConc:aArray[nCount][24],oConc:aArray[nCount][25],oConc:aArray[nCount][13])
				
				//Conciliacao por lote
				If cConcilia == 2
						
					//Verifica se ja existe um lote para este Banco, Agencia, Conta e Data do Credito
						If Len(aLotes) > 0 .AND. (nLote := AScan (aLotes, {|aX| aX[2] + aX[3] + aX[4] + DToS(aX[8]) == oConc:aArray[nCount][26] + oConc:aArray[nCount][24] + oConc:aArray[nCount][25] + DToS(Iif (lUseFIFDtCred, oConc:aArray[nCount][11],dDataBase))})) > 0
						//Adiciona ao lote o recno de mais um titulo SE1
						AADD(aLotes[nLote ][9], oConc:aArray[nCount][22])
					Else
						//Busca o numero do lote
						cLote := BuscarLote()

						//Cria um lote com o titulo SE1
						AADD(aLotes, {cLote, oConc:aArray[nCount][26], oConc:aArray[nCount][24], oConc:aArray[nCount][25], aDadoBanco[1], aDadoBanco[2], ;
										aDadoBanco[3], Iif (lUseFIFDtCred, oConc:aArray[nCount][11],dDataBase), {oConc:aArray[nCount][22]}})
					EndIf
						
					lRet := .T.
				Else
						
					//Array com os dados do titulo para baixa individual
					aTitInd	:=	{	{"E1_PREFIXO"			,oConc:aArray[nCount][5]	 	 							,NiL},;
										{"E1_NUM"			,oConc:aArray[nCount][6]			 						,NiL},;
										{"E1_PARCELA"		,oConc:aArray[nCount][8]	     							,NiL},;
										{"E1_TIPO"			,oConc:aArray[nCount][7]       	 							,NiL},;
										{"E1_CLIENTE"		,oConc:aArray[nCount][4]     								,NiL},;
										{"E1_LOJA"			,oConc:aArray[nCount][27]       	 						,NiL},;
										{"AUTMOTBX"			,"NOR"					     			 					,Nil},;
										{"AUTBANCO"			,(PadR(aDadoBanco[1],Len(SE8->E8_BANCO)))   				,Nil},;
										{"AUTAGENCIA"		,(PadR(aDadoBanco[2],Len(SE8->E8_AGENCIA)))					,Nil},;
										{"AUTCONTA"			,If(lPontoF,(PadR(aDadoBanco[3],Len(SE8->E8_CONTA))),(PadR(StrTran(aDadoBanco[3],"-",""),Len(SE8->E8_CONTA)))),Nil},;
										{"AUTDTBAIXA"		,Iif (lUseFIFDtCred, oConc:aArray[nCount][11],dDataBase)	,Nil},;
										{"AUTDTCREDITO"		,Iif (lUseFIFDtCred, oConc:aArray[nCount][11],dDataBase)	,Nil},;
										{"AUTHIST"			,STR0110			           			 					,Nil},; //"Conciliador SITEF"
										{"AUTDESCONT"		,0					    	 			 					,Nil},; //Valores de desconto
										{"AUTACRESC"		,0					 	    			 					,Nil},; //Valores de acrescimo - deve estar cadastrado no titulo previamente
										{"AUTDECRESC"		,0					 		 			 					,Nil},; //Valore de decrescimo - deve estar cadastrado no titulo previamente
										{"AUTMULTA"			,0					 		 			 					,Nil},; //Valores de multa
										{"AUTJUROS"			,0					 				 	 					,Nil},; //Valores de Juros
										{"AUTVALREC"		,oConc:aArray[nCount][13]   	 							,Nil}}  //Valor recebido
								
						//Efetua baixa individual
					A918EfetuaBX(aTitInd, oDlg, , @lRet)

					If !lRet //Erro na Baixa do T�tulo
						aConc[nCount][1] := LoadBitmap(GetResources(), "BR_AMARELO") //"BR_AMARELO"
						Loop
					Else
						aConc[nCount][1] := LoadBitmap(GetResources(), "BR_VERMELHO") //"BR_VERMELHO"
					EndIf
				EndIf
									
				//Atualiza Folder Totais Dinamicamente
				If (nPTot := aScan(aTotais,{|x| x[TOT_DATA] == oConc:aArray[nCount][11] })) <> 0
					If nTpConc == 1
						aTotais[nPTot][TOT_CONC]	:= aTotais[nPTot][TOT_CONC] - aConc[nCount][13]
						aTotais[nPTot][TOT_QTDCONC]--
					ElseIf nTpConc == 2
						aTotais[nPTot][TOT_CPAR]	:= aTotais[nPTot][TOT_CPAR] - aConc[nCount][13]
						aTotais[nPTot][TOT_QTDCPAR]--
					EndIf
					aTotais[nPTot][TOT_GERAL]	:= aTotais[nPTot][TOT_CONC] + aTotais[nPTot][TOT_CPAR]+aTotais[nPTot][TOT_CNAO]+aTotais[nPTot][TOT_CMAN]+aTotais[nPTot][TOT_DIV]
					aTotais[nPTot][TOT_GERAL]	:= Round(aTotais[nPTot][TOT_GERAL],2)
				EndIf
			EndIf
		EndIf
	EndIf
		
	IncProc()
Next

//Verifica se algum item foi selecionado
If !lExist
	//"Nao ha' nenhum registro marcado"
	Help(" ",1,"A918NoSelec",,STR0051 ,1,0,,,,,,{STR0221})
	//"Selecione ao menos um registro para que seja conciliado"
	Return Nil
EndIf
	
If lMenorSitef
	Conout(STR0052)		//"Valor Sitef menor que o Valor Protheus, necess�rio corrigir no Financeiro"
EndIf

oConc:Refresh()
	
//Atualizacao do Objeto na Aba Totais
oTotais:SetArray(aTotais)
oTotais:bLine := {||aEval(aTotais[oTotais:nAt],{|z,w| aTotais[oTotais:nAt,w]})}
oTotais:Refresh()

//Efetua a baixa por lote
If cConcilia == 2
    ProcLogAtu(STR0138,STR0148)
    Processa({|| A918EfetuaBX(aLotes, oDlg, aConc, @lRet)},STR0106,STR0107) //"Aguarde..."#"Efetuando Baixa de T�tulos Lote..."

    If !lRet
        ProcLogAtu(STR0149,STR0150)
        Aeval(aConc, {|x| x[1]:cName := IIF(x[1]:cName == "BR_VERDE", "BR_AMARELO", x[1]:cName)})
    Else
        Aeval(aConc, {|x| x[1]:cName := IIF(x[1]:cName == "BR_VERDE", "BR_VERMELHO", x[1]:cName)})
    EndIf

    ProcLogAtu(STR0138,STR0151)
    oConc:Refresh()
EndIf

Processa({|| F918AtuFIF (aConc,"2")},STR0106,STR0108) //"Aguarde..."#"Atualizando Tabela FIF..."
	
If lRet
	MsgInfo(STR0109) //"Baixa efetuada com sucesso!!!"
Else
	MsgInfo(STR0191) //"Houve titulos n�o conciliados. Favor verificar"
EndIf
	
Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} fA918DvConc
Funcao que � executada a partir do botao de efetivar conciliacao dos Folders  
Divergentes, tendo como funcionalidade baixar os titulos
do Contas a Receber e alterar o status do mesmo na tabela FIF 

@Function fA918DvConc
@author Danilo Santos
@since 11/04/2018
@version 12.1.17   
/*/
//---------------------------------------------------------------------------
Static Function fA918DvConc( aDiverg As Array, oDiverg As Object, aTotais As Array, oTotais As Object) As Logical
Local nCount As Numeric
Local nPTot	 As Numeric
Local lRet	 As Logical

Default aDiverg := {}
Default oDiverg := Nil
Default aTotais := {}
Default oTotais := Nil

nCount		:= 0	//Contador utilizado para varrer o array da FIF
nPTot		:= 0	//Posicao da linha da aba totais, referente a data credito posicionada
lRet		:= .T.

If Len(aDiverg) == 0
	MsgInfo(STR0147)
	Return Nil
EndIf
	
//Verifica se algum item foi selecionado nos titulos
If (aScan(aDiverg,{|x| x[1]:cName == "BR_VERDE" })) == 0
	MsgInfo(STR0051) //"Nao ha' nenhum registro marcado"
	Return Nil
EndIf

ProcRegua(Len(aDiverg))

// Valida se campo de justificativa est� preenchido para todos os itens selecionados
For nCount := 1 To Len(aDiverg)
	If AllTrim(oDiverg:aArray[nCount][1]:cName) == "BR_VERDE"
		If Empty(oDiverg:aArray[nCount][29]) //29 - Codigo de Justificativa
			cMsg :=  CRLF + STR0025 + " - " + DtoC(oDiverg:aArray[nCount][11]) + CRLF + STR0028 + " - " + oDiverg:aArray[nCount][14] + CRLF + STR0029 + " - " + oDiverg:aArray[nCount][15] //"DT Credito"###"NSU"###"Parc Tef"
			lRet := .F.
			Help(NIL, NIL, "F918aJust", NIL, STR0190 + CRLF + cMsg, 1, 0) //"Informe a Justificativa para a Concilia��o."
			Exit
		EndIf
	EndIf
Next nCount

If lRet
	For nCount := 1 To Len(aDiverg)

			//Verifica se o item esta selecionado
		If AllTrim(oDiverg:aArray[nCount][1]:cName) == "BR_VERDE"

			aDiverg[nCount][1] := LoadBitmap(GetResources(), "BR_VERMELHO") //"BR_VERMELHO"
								
			//Atualiza Folder Totais Dinamicamente
			If (nPTot := aScan(aTotais,{|x| x[TOT_DATA] == oDiverg:aArray[nCount][11] })) <> 0
				aTotais[nPTot][TOT_DIV]		:= aTotais[nPTot][TOT_CONC] - aDiverg[nCount][13]
				aTotais[nPTot][TOT_QTDDIV]--
				aTotais[nPTot][TOT_GERAL]	:= aTotais[nPTot][TOT_CONC] + aTotais[nPTot][TOT_CPAR]+aTotais[nPTot][TOT_CNAO]+aTotais[nPTot][TOT_CMAN]+aTotais[nPTot][TOT_DIV]
				aTotais[nPTot][TOT_GERAL]	:= Round(aTotais[nPTot][TOT_GERAL],2)
			EndIf

		EndIf
			
		IncProc()
	Next
		
	oDiverg:Refresh()
		
	//Atualizacao do Objeto na Aba Totais
	oTotais:SetArray(aTotais)
	oTotais:bLine := {||aEval(aTotais[oTotais:nAt],{|z,w| aTotais[oTotais:nAt,w]})}
	oTotais:Refresh()

	Processa({|| F918AtuFIF (aDiverg,"3")},STR0106,STR0108) //"Aguarde..."#"Atualizando Tabela FIF..."
		
	MsgInfo(STR0109) //"Baixa efetuada com sucesso!!!"
EndIf

Return lRet


//---------------------------------------------------------------------------
/*/{Protheus.doc} BuscarBanco
Busca os dados de banco, agencia e conta (Ponto de entrada), se nao existir, 
matem os parametros que foram passados para a fun��o.                                                                                                          
Atualiza informacoes do SE1                                 

@type Function
@author Unknown
@since 06/08/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function BuscarBanco(cBancoFIF,cAgFIF,cContaFIF,nVlLiqFIF)
Local aDados	:= {}				//Dados do retorno banco/agencia/conta
Local aArea		:= GetArea()		//Salva area local
Local aAliasSE1	:= SE1->(GetArea()) //Salva area SE1
Local aAliasFIF	:= FIF->(GetArea()) //Salva area FIF
Local nACRESC	:= 0
Local nDECRESC	:= 0

If !lPontoF
	aDados := {cBancoFIF, cAgFIF, cContaFIF}
Else
	aDados := ExecBlock('FINA910F', .F., .F., {cBancoFIF, cAgFIF, cContaFIF})
	
	If !(ValType(aDados) == 'A' .AND. Len(aDados) == 3)
		aDados := {cBancoFIF, cAgFIF, cContaFIF}
	EndIf
EndIf

If nVlLiqFIF > SE1->E1_SALDO
	nACRESC 	:= nVlLiqFIF - SE1->E1_SALDO
EndIf

If nVlLiqFIF < SE1->E1_SALDO
	nDECRESC := (nVlLiqFIF - SE1->E1_SALDO) * (-1)
EndIf

RecLock("SE1",.F.)
		
SE1->E1_PORTADO	:= aDados[1]
SE1->E1_AGEDEP	:= aDados[2]
SE1->E1_CONTA	:= aDados[3]
	
If nAcresc <> 0
	SE1->E1_ACRESC	:= nAcresc
	SE1->E1_SDACRES	:= nAcresc
EndIf
	
If nDECRESC <> 0
	SE1->E1_DECRESC	:= nDECRESC
	SE1->E1_SDDECRE	:= nDECRESC
EndIf
	
SE1->(MsUnlock())

//Restaura areas
RestArea(aAliasSE1)
RestArea(aAliasFIF)
RestArea(aArea)

Return aDados

//---------------------------------------------------------------------------
/*/{Protheus.doc} BuscarLote
Busca o numero do proximo lote para baixa dos titulos

@type Function
@author Unknown
@since 28/04/2011
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function BuscarLote()
Local aArea		:= GetArea()		//Salva area local
Local aOrdSE5 	:= SE5->(GetArea())	//Salva area SE5
Local cLoteFin	:= ''				//Numero do lote
	
cLoteFin := GetSxENum("SE5","E5_LOTE","E5_LOTE"+cEmpAnt,5)
	
DbSelectArea("SE5")
DbSetOrder(5)
	
While SE5->(MsSeek(xFilial("SE5")+cLoteFin))
	If (__lSx8)
		ConfirmSX8()
	EndIf
		
	cLoteFin := GetSxENum("SE5","E5_LOTE","E5_LOTE"+cEmpAnt,5)
EndDo
	
ConfirmSX8()
	
//Restaura areas
RestArea(aArea)
RestArea(aOrdSE5)
	
Return cLoteFin

//---------------------------------------------------------------------------
/*/{Protheus.doc} F918AtuFIF
Atualiza os dados da FIF apos a baixa dos titulos

@type Function
@author Unknown
@since 28/04/2011
@version 12   
/*/
//---------------------------------------------------------------------------
Function F918AtuFIF(aRegConc As Array, cStatus As Character)

Local nCount	As Numeric
Local aArea		As Array
Local aAliasFIF	As Array

DEFAULT aRegConc	:= {}
DEFAULT cStatus		:= "2" //2-"Conciliado Normal"

nCount		:= 0				//Utilizada para ler todos os registros baixados
aArea		:= GetArea()		//Salva area local
aAliasFIF	:= FIF->(GetArea()) //Salva area FIF

ProcRegua(Len(aRegConc))
IncProc(STR0108) //"Atualizando tabela FIF..."
	
DbSelectArea("FIF")
DbSetOrder(5)
	
//FIF_FILIAL + FIF_DTTEF + FIF_NSUTEF + FIF_PARCEL
For nCount := 1 To Len( aRegConc )

	//Os registros que estiverem marcados como vermelho, s�o aqueles que foram conciliados e baixados
	If AllTrim(aRegConc[nCount][1]:cName) == "BR_VERMELHO"

		If !Empty(aRegConc[nCount][23])
			FIF->( dbGoTo(aRegConc[nCount][23]) ) // Faz a busca pelo FIF.R_E_C_N_O_
			If FIF->( !Eof() ) .And. FIF->( Recno() ) == aRegConc[nCount][23]

				IncProc()
			
				RecLock("FIF",.F.)
				FIF->FIF_STATUS := IIf( FIF->FIF_STATUS == '6', '7', cStatus )	//'6' - Ant. Nao Processada / '7' - Antecipado
				FIF->FIF_PREFIX := aRegConc[nCount][5]						//Rastro Prefixo SE1
				FIF->FIF_NUM    := aRegConc[nCount][6]						//Rastro Num SE1
				FIF->FIF_PARC   := aRegConc[nCount][8]						//Rastro Parcela SE1
				FIF->FIF_TIPO   := aRegConc[nCount][7]						//Rastro Tipo SE1
				FIF->FIF_PGJUST := aRegConc[nCount][29]						//Justificativa FVX
				If cStatus = "3"
					FIF->FIF_PGDES1 := aRegConc[nCount][31]						//Justificativa FVX
					FIF->FIF_PGDES2 := aRegConc[nCount][30]						//Justificativa Manual
				Else
					FIF->FIF_PGDES1 := aRegConc[nCount][30]						//Justificativa FVX
					FIF->FIF_PGDES2 := aRegConc[nCount][31]						//Justificativa Manual				
				EndIf
				FIF->FIF_USUPAG := RetCodUsr()								//C�digo do usu�rio
				FIF->FIF_DTPAG 	:= dDatabase								//database

				FIF->(MsUnlock())
			EndIf
		EndIf
	EndIf
	
	IncProc()

		
Next nCount

//Restaura areas
RestArea(aAliasFIF)
RestArea(aArea)
	
Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918SelReg
Funcao usada para atualizar a selecao do registro que esta posicionado ou de 
todos do array ou ordenar os registros  

@type Function
@author Rafael Rosa da Silva
@since 08/06/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918SelReg( aDados, oObj, oSelec, oNSelec, nColPos, nPos, lCheck, aNaoConc, nPosNConc, oNaoConc, oBlcSelec,cGuia )
Local nI		:= 0	//Variavel contadora
Local i         := 0
	
Default nPos	:= 0
Default nColPos	:= 1
Default lCheck	:= .T.
Default cGuia   := ""
	
oSelec		:= LoadBitmap(GetResources(), "BR_VERDE")	//Objeto de tela para mostrar como marcado um registro
oNSelec		:= LoadBitmap(GetResources(), "BR_BRANCO")	//Objeto de tela para mostrar como desmarcado um registro
oBlcSelec	:= LoadBitmap(GetResources(), "BR_PRETO")	//Objeto de tela para mostrar como desmarcado um registro

//Itens baixados
If nPos <> 0 .And. ValType(nPosNConc) <> "U"
	If Alltrim(oObj:aArray[nPos][5]) <> Alltrim(aNaoConc[nPosNConc][14]) .And. nVldNSE == 1
		MsgStop(STR0116)			//"NSU e N� Comprovante selecionados est�o divergentes"
		Return()
	ElseIf aNaoConc[nPosNConc][1]:cName == "BR_PRETO"
		MsgStop(STR0132)
		Return()
	Endif

	If Alltrim(oObj:aArray[nPos][1]:cName) == "BR_VERMELHO"
		Return()
	Else
		If !Empty(aNaoConc)
			If (Alltrim(oObj:aArray[nPos][1]:cName) == "BR_BRANCO" .AND. (aNaoConc[nPosNConc][1]:cname == "BR_VERDE" .OR. ;
					aNaoConc[nPosNConc][1]:cname == "BR_VERMELHO"))
				Return()
			EndIf
		EndIf
	EndIf
EndIf
	
If nColPos == 1 .AND. lCheck //Atualiza selecao do registro posicionado
	If nPos > 0
		If Alltrim(oObj:aArray[nPos][1]:cName) == "BR_BRANCO"
			If Empty(aNaoConc)
				If aDados[nPos][20] = STR0104 //"OK"
					SE1->(DbGoto(oObj:aArray[nPos][22]))
					If SE1->(MsRLock()) .AND. SE1->E1_SALDO > 0 
						oObj:aArray[nPos][1] := oSelec
					Else 
						//Registro bloqueado em outra opera��o de concilia��o
						Help(" ",1,"A918RegInUse",,STR0226 ,1,0,,,,,,{STR0228})
						//Aguarde a finaliza��o do processo de concilia��o feito por outro usu�rio.
					Endif 
				Else
					MsgStop(STR0101)			//"Este registro n�o pode ser selecionado, pois n�o existe a conta cadastrada."
					Return()
				EndIf
			Else
				If aNaoConc[nPosNConc][20] = STR0104 //"OK"
					oObj:aArray[nPos][1]:cName := "BR_VERDE"
				Else
					MsgStop(STR0101) 			//"Este registro n�o pode ser selecionado, pois n�o existe a conta cadastrada."
					Return()
				EndIf
			EndIf
		ElseIf Alltrim(oObj:aArray[nPos][1]:cName) == "BR_PRETO"
			MsgStop(STR0133)			//"Este registro n�o pode ser selecionado, pois n�o existe a conta cadastrada."
			Return()
		ElseIf Alltrim(oObj:aArray[nPos][1]:cName) == "BR_VERMELHO"
			MsgStop(STR0134)
			Return()
		ElseIf Alltrim(oObj:aArray[nPos][1]:cName) == "BR_AMARELO"
			MsgStop(STR0052)
			Return()
		Else
			If Empty(aNaoConc)
				//aDados[nPos][1] := oNSelec
				oObj:aArray[nPos][1] := oNSelec
				SE1->(DbGoto(oObj:aArray[nPos][22]))
				SE1->(MsUnlock())
			Else
				//Desmarca selecao dos nao conciliados
				//Verifica se o registro selecionado da FIF eh o mesmo do momento da selecao.
				If oNaoConc:aArray[nPosNConc][22] <> oObj:aArray[nPos][15] .and. (nVldNSE == 1) .OR. oNaoConc:aArray[nPosNConc][1]:cName <> "BR_VERDE" .and. (nVldNSE == 1)
					MsgStop(STR0113)  //"N�o � poss�vel desfazer a sele��o, porque o registro selecionado da FIF n�o corresponde ao da SE1."
					Return()
				Else
					oObj:aArray[nPos][1] := oNSelec
				EndIf
					
			EndIf
		EndIf
	Else
		If Len(aDados) == 1
			MsgStop(STR0102)					//"N�o existe registro para selecionar"
			Return()
		EndIf
		For nI := 1 to Len(aDados)
			If Alltrim(oObj:aArray[nI][1]:cName) <> "BR_VERMELHO"
				If Alltrim(oObj:aArray[nI][1]:cName) == "BR_BRANCO"
					SE1->(DbGoto(oObj:aArray[nI][22]))	
					If SE1->(MsRLock())	.AND. SE1->E1_SALDO > 0 
						//aDados[nI][1] := oSelec
						oObj:aArray[nI][1] := oSelec
					Else 
						lLockReg := .T. 		
					EndIf
				ElseIf Alltrim(oObj:aArray[nI][1]:cName) == "BR_PRETO"
					oObj:aArray[nI][1] := oBlcSelec
				Else
					//aDados[nI][1] := oNSelec
					oObj:aArray[nI][1] := oNSelec
					SE1->(DbGoto(oObj:aArray[nI][22]))
					SE1->(MsUnlock())
				EndIf
			EndIf
		Next nI
		If lLockReg
			//Um ou mais registros esta(�o) bloqueado(s) em outra opera��o de concilia��o
			Help(" ",1,"A918RegInUse",,STR0227,1,0,,,,,,{STR0228})
			//Aguarde a finaliza��o do processo de concilia��o feito por outro usu�rio.
		EndIf

	EndIf
Else //Ordena registros
	If !Empty(cGuia) 
		If cGuia <> __cGuia
			__cGuia := cGuia
			__nCol  := 0
			__aOrd := {}
			For i := 0 to 100
				aAdd(__aOrd,0)
			Next		
		Else
			If nColPos <> __nCol
				__nCol := nColPos
				__aOrd := {}
				For i := 0 to 100
					aAdd(__aOrd,0)
				Next
			End
		EndIf
		If __aOrd[nColPos] = 0 .or. __aOrd[nColPos] = 2
			aDados := aSort(aDados,,,{|x,y| x[nColPos] <= y[nColPos] })
			__aOrd[nColPos] := 1
		Else
			aDados := aSort(aDados,,,{|x,y| x[nColPos] >= y[nColPos] })
			__aOrd[nColPos] := 2
		EndIf
	Else
		aDados := aSort(aDados,,,{|x,y| x[nColPos] <= y[nColPos] })
	End
EndIf

	//Atualiza Objeto oNaoConc
If (ValType(aNaoConc) <> "U" .AND. Len(aNaoConc) > 0)

	If Alltrim(oObj:aArray[nPos][1]:cName) == "BR_BRANCO"
		If oNaoConc:aArray[nPosNConc][22] == oObj:aArray[nPos][15]	//recno se1
			oNaoConc:aArray[nPosNConc][1]:cName := "BR_BRANCO"
			SE1->(DbGoto(oNaoConc:aArray[nPosNConc][22]))
			SE1->(MsUnlock())
			//Limpa os dados quando registro for desmarcado
			oNaoConc:aArray[nPosNConc][22] 	:= 0 							//recno SE1
			oNaoConc:aArray[nPosNConc][5] 	:= ""							//prefixo SE1
			oNaoConc:aArray[nPosNConc][7] 	:= ""							//tipo SE1
			oNaoConc:aArray[nPosNConc][8] 	:= ""							//parcela SE1
			oNaoConc:aArray[nPosNConc][6] 	:= ""							//num SE1
			oNaoConc:aArray[nPosNConc][27] 	:= ""							//loja SE1
			oNaoConc:aArray[nPosNConc][12] 	:= 0							//valor SE1	
		Else
			//??? Avaliar
			oObj:aArray[nPos][1]:cName := "BR_VERDE"
		EndIf
	ElseIf Alltrim(oObj:aArray[nPos][1]:cName) == "BR_PRETO"
		oNaoConc:aArray[nPosNConc][1] := oBlcSelec
	Else
		SE1->(DbGoto(oObj:aArray[nPos][15]))
		If SE1->(MsRLock())	
			oNaoConc:aArray[nPosNConc][1]:cName := "BR_VERDE"
			//Atribui os valores do se1 no array da fif
			oNaoConc:aArray[nPosNConc][22] 	:= oObj:aArray[nPos][15]	//recno se1
			oNaoConc:aArray[nPosNConc][5] 	:= oObj:aArray[nPos][7]		//prefixo se1
			oNaoConc:aArray[nPosNConc][7] 	:= oObj:aArray[nPos][10]	//tipo se1
			oNaoConc:aArray[nPosNConc][8] 	:= oObj:aArray[nPos][9]		//parcela se1
			oNaoConc:aArray[nPosNConc][6] 	:= oObj:aArray[nPos][8]		//num se1
			oNaoConc:aArray[nPosNConc][27] 	:= oObj:aArray[nPos][12]	//loja se1
			oNaoConc:aArray[nPosNConc][12] 	:= oObj:aArray[nPos][04]	//valor SE1
		Else 
			//Registro bloqueado em outra opera��o de concilia��o
			Help(" ",1,"A918RegInUse",,STR0226 ,1,0,,,,,,{STR0228})
			//Aguarde a finaliza��o do processo de concilia��o feito por outro usu�rio.
			oObj:aArray[nPos][1]:cName := "BR_BRANCO"
		EndIf 




	EndIf

	oNaoConc:Refresh()
EndIf
	
oObj:Refresh()
	
Return

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918SelReg
Funcao usada para atualizar a selecao do registro que esta posicionado ou de 
todos do array ou ordenar os registros  

@type Function
@author Rafael Rosa da Silva
@since 08/06/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918Msg( aDados As Array, oObj As Object, oDescri As Object, cDescri As Character)
Local nI		As Numeric
Local nPos		As Numeric
Local lCheck	As Logical
Local cCodFil	As Character

nI		:= 0	//Variavel contadora
nPos	:= oObj:nAt
lCheck	:= .T.
cCodFil := ""

DbSelectArea("FVY")
FVY->(DbSetOrder(1))

//Itens baixados
If nPos <> 0 .and. (aDados[nPos][32] <> "")

	If F916ChkMod("FVY") // Verifica se modo da tabela � exclusivo
		cCodFil := aDados[nPos][28]
	Else
		cCodFil := xFilial("FVY")
	EndIf

	If FVY->(DbSeek(cCodFil + aDados[nPos][32] + aDados[nPos][33]))
		cDescri := FVY->FVY_DESCR
		oDescri:Refresh()
	Else
		cDescri := " "
		oDescri:Refresh()
	Endif

EndIf
	
Return

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FA918RES
Fun��o respons�vel por manter o layout da janela, indepedentemente da resolu��o
horizontal de tela onde o client est� sendo executado

@type Function
@author Norbert, Ernani e Mansano
@since 10/05/2005
@version 12
/*/
//-------------------------------------------------------------------------------------
Static Function FA918ARES( nTam )
Local nHRes := oMainWnd:nClientWidth // Resolucao horizontal do monitor
	
If nHRes == 640	// Resolucao 640x480 (soh o Ocean e o Classic aceitam 640)
	nTam *= 0.8
ElseIf (nHRes == 798) .OR. (nHRes == 800)		// Resolucao 800x600
	nTam *= 1
Else	// Resolucao 1024x768 e acima
	nTam *= 1.28
EndIf

Return Int(nTam)

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918AtuDiv
Localiza as divergencias em Conciliados Parcialmente para exibir no Rodape                                        

@type Function
@author Alessandro Santos
@since 17/11/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918AtuDiv( aConcPar, aIndic, oConcPar, oIndic )
Local nPos	:= oConcPar	//Posicao Titulo Conciliado Parcialmente
Local aAux	:= {}		//Array auxiliar
Local nI	:= 0		//Variavel para contador
	
//Verifica se arrays nao estao vazios
If aConcPar[1][14] <> ""
		
	//Compara item do Rodape com itens Sitef Conciliados Parcialmente para encontrar referencia
	For nI := 1 To Len(aIndic)
		If (aIndic[nI][9] == aConcPar[nPos][5] .AND. aIndic[nI][10] == aConcPar[nPos][6];
			.AND. aIndic[nI][11] == aConcPar[nPos][7] .AND. aIndic[nI][7] == aConcPar[nPos][8];
			.AND. aIndic[nI][8] == aConcPar[nPos][15])
			
			aAdd(aAux,aIndic[nI])
		EndIf
	Next nI
		
		//Atualiza Objeto oIndic
	If Len(aAux) > 0
		oIndic:SetArray(aAux)
		oIndic:bLine :=	{||{ Transform(aAux[oIndic:nAt,01], __cPicSALD) ,;	
							 Transform(aAux[oIndic:nAt,02], __cPicVL) ,;	
							 aAux[oIndic:nAt,03],;
							 aAux[oIndic:nAt,15],;
							 Dtoc(aAux[oIndic:nAt,05]),;
							 Dtoc(aAux[oIndic:nAt,06]),;
							 aAux[oIndic:nAt,07],;
							 aAux[oIndic:nAt,08],;
							 aAux[oIndic:nAt,09],;
							 aAux[oIndic:nAt,10],;
							 aAux[oIndic:nAt,11],;
							 aAux[oIndic:nAt,12],;
							 aAux[oIndic:nAt,13],;
							 aAux[oIndic:nAt,14]}}
		
		oIndic:Refresh()
	EndIf
		
EndIf

Return

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918AtuMan
Atualizacao Dinamica da Aba Conciliados Manualmente apos a Conciliacao de 
item Nao Conciliado                                                               

@type Function
@author Alessandro Santos
@since 11/12/2009
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918AtuMan( aConcMan, oConcMan, aTitulos, aDados, nPos, nPosCol, oRedSelec, oObj, oTitulos )
Local aArea		:= GetArea()		//Salva area atual
Local aAliasSE1	:= SE1->(GetArea())	//Salva area SE1
Local aAliasFIF	:= FIF->(GetArea())	//Salva area FIF
Local aColsAux	:= {}				//Array auxiliar na montagem do Array Conciliados manualmente
	
//Posiciona Titulo baixado no SE1
dbSelectArea("SE1")
SE1->(dbSetOrder(1))
SE1->(dbSeek(xFilial("SE1")+aTitulos[nPos][7]+aTitulos[nPos][8]+aTitulos[nPos][9]+aTitulos[nPos][10]))
	
//Posiciona arquivo SITEF Conciliado
dbSelectArea("FIF")
FIF->(dbSetOrder(5))
FIF->(dbSeek(xFilial("FIF")+DtoS(aDados[nPosCol][10])+aDados[nPosCol][14]+aDados[nPosCol][15]))
	
//Atualizacao do Array Conciliados Manualmente
aAdd(aColsAux, IIf(aDados[nPosCol][20] == 'OK',LoadBitmap(GetResources(),"BR_BRANCO"),LoadBitmap(GetResources(),"BR_PRETO")))	//01 Status
aAdd(aColsAux, aDados[nPosCol][2])	//02 Codigo do Estabelecimento Sitef
aAdd(aColsAux, aDados[nPosCol][3])	//03 Codigo da Loja Sitef
aAdd(aColsAux, aDados[nPosCol][4])	//04 Codigo do Cliente (Administradora)
aAdd(aColsAux, aTitulos[nPos][7])	//05 Prefixo do titulo Protheus
aAdd(aColsAux, aTitulos[nPos][8])	//06 Numero do titulo Protheus
aAdd(aColsAux, aTitulos[nPos][10])	//07 Tipo do titulo Protheus
aAdd(aColsAux, aTitulos[nPos][9])	//08 Numero da parcela Protheus
aAdd(aColsAux, aDados[nPosCol][9])	//09 Numero do Comprovante Sitef
aAdd(aColsAux, aDados[nPosCol][10])	//10 Data da Venda Sitef
aAdd(aColsAux, aDados[nPosCol][11])	//11 Data de Credito Sitef
aAdd(aColsAux, aDados[nPosCol][12])	//12 Valor do titulo Protheus
aAdd(aColsAux, aDados[nPosCol][13])	//13 Valor liquido Sitef
aAdd(aColsAux, aDados[nPosCol][14])	//14 Numero NSU Sitef
aAdd(aColsAux, aDados[nPosCol][15])	//15 Numero da parcela Sitef
aAdd(aColsAux, aDados[nPosCol][16])	//16 Documento TEF Protheus
aAdd(aColsAux, aDados[nPosCol][17])	//17 NSU Sitef Protheus
aAdd(aColsAux, aDados[nPosCol][18])	//18 Vencimento real do titulo
aAdd(aColsAux, aDados[nPosCol][19])	//19 
aAdd(aColsAux, aDados[nPosCol][20])	//20 
aAdd(aColsAux, '')                  //21  
aAdd(aColsAux, aDados[nPosCol][22]) //22
aAdd(aColsAux, aDados[nPosCol][23]) //23
aAdd(aColsAux, aDados[nPosCol][29]) //24
aAdd(aColsAux, aDados[nPosCol][30]) //25
aAdd(aColsAux, aDados[nPosCol][31]) //26
aAdd(aColsAux, aDados[nPosCol][27]) //27
aAdd(aColsAux, aDados[nPosCol][28]) //28
aAdd(aColsAux, aDados[nPosCol][29]) //29
aAdd(aColsAux, aDados[nPosCol][30]) //30
aAdd(aColsAux, aDados[nPosCol][31]) //31
aAdd(aColsAux, aDados[nPosCol][32]) //32
aAdd(aColsAux, aDados[nPosCol][33]) //33
aAdd(aColsAux, aDados[nPosCol][34]) //34
aAdd(aColsAux, aDados[nPosCol][35]) //35
aAdd(aColsAux, aDados[nPosCol][36]) //36
aAdd(aColsAux, aDados[nPosCol][37]) //37
aAdd(aColsAux, aDados[nPosCol][38]) //38
aAdd(aColsAux, aDados[nPosCol][39]) //39
aAdd(aColsAux, aDados[nPosCol][40]) //40
aAdd(aColsAux, aDados[nPosCol][41]) //41
aAdd(aColsAux, aDados[nPosCol][42]) //42
aAdd(aColsAux, aDados[nPosCol][43]) //43
aAdd(aColsAux, aDados[nPosCol][44]) //44
aAdd(aColsAux, aDados[nPosCol][45]) //45
aAdd(aColsAux, aDados[nPosCol][46]) //46
aAdd(aColsAux, aDados[nPosCol][47]) //47
aAdd(aColsAux, aDados[nPosCol][48]) //48

aAdd(aConcMan,aColsAux)
	
//Atualizacao do Objeto na Aba Conciliados Manualmente
oConcMan:SetArray(aConcMan)
oConcMan:bLine := {||aEval(aConcMan[oConcMan:nAt],{|z,w| aConcMan[oConcMan:nAt,w]})}
oConcMan:Refresh()
	
//Atualizacao do Objeto na Aba Nao Conciliados (Rodape)
aDel(aTitulos,nPos)
aSize(aTitulos,Len(aTitulos)-1)
oTitulos:SetArray(aTitulos)
oTitulos:bLine := {||aEval(aTitulos[oTitulos:nAt],{|z,w| aTitulos[oTitulos:nAt,w]})}
oTitulos:Refresh()
	
//Restaura Areas
RestArea(aAliasSE1)
RestArea(aAliasFIF)
RestArea(aArea)
	
Return(.T.)

//---------------------------------------------------------------------------
/*/{Protheus.doc} CreateTMP
Cria tabela temporaria para armazenar registro conciliados, n�o conciliadas  
evitando ascan em array                    

@type Function
@author Unknown
@since 07/08/2013
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function CreateTMP( aCampos, cAliasSitef, aChave )
Local aSaveArea	:= GetArea()

If Select(cAliasSitef) > 0
	A918CLOSEAREA(cAliasSitef)
EndIf

//Cria tabela tempor�ria no banco de dados  
_oFINA910A := FwTemporaryTable():New(cAliasSitef)
_oFINA910A:SetFields(aCampos)
_oFINA910A:AddIndex("1", aChave)
_oFINA910A:Create()

RestArea( aSaveArea )

Return nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918CLOSEAREA
Fechamento da tabela temporaria                    

@type Function
@author Unknown
@since 07/08/2013
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918CLOSEAREA( cAliasSitef )

If Select(cAliasSitef) > 0 
	(cAliasSitef)->(DbCloseArea())
Endif

//Deleta a tabela tempor�ria no banco, caso j� exista
If(_oFINA910A <> NIL)
	_oFINA910A:Delete()
	_oFINA910A := NIL
EndIf

Return Nil

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918VLDBCO
Valida��o do banco                              

@type Function
@author Pedro Pereira Lima
@since 07/08/2013
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918VLDBCO( cBanco, cAgencia, cConta,cCodFil )                                                   
Local lRet		:= .T.
Local nSubBco	:= IIf(Len(cBanco)   - nTamBanco   <= 0, 1, (Len(cBanco)   - nTamBanco  ) + 1)
Local nSubAge	:= IIf(Len(cAgencia) - nTamAgencia <= 0, 1, (Len(cAgencia) - nTamAgencia) + 1)
Local nSubCC	:= IIf(Len(cConta)   - nTamCC      <= 0, 1, (Len(cConta)   - nTamCC     ) + 1)
Local nPos		:= 0
Local __cFilSA6 := ""

lSA6Comp := F916ChkMod("SA6")

If __cFilSA6 == Nil .or. (xFilial( 'SA6' ) == cCodFil) .or. !lSA6Comp
	__cFilSA6	:= xFilial( 'SA6' )
Else
	__cFilSA6	:= cCodFil
Endif

cBanco		:= padr(SubStr(cBanco,nSubBco,nTamBanco),nTamBanco)
cAgencia	:= padr(SubStr(cAgencia,nSubAge,nTamAgencia),nTamAgencia)
cConta		:= padr(SubStr(cConta,nSubCC,nTamCC),nTamCC)

nPos := Ascan( __aBancos, {|x| x[1] == __cFilSA6 .And. x[2] == cBanco .And. x[3] == cAgencia .And. x[5] == cConta } )
lRet := ( nPos > 0 ) 

If lRet
	If __aBancos[nPos,7] == '1'
		lRet := .F.
	ElseIf !Empty( __aBancos[nPos,8] ) .And. __aBancos[nPos,8] == '1'
		lRet := .F.
	Endif 
Endif

Return lRet

//---------------------------------------------------------------------------
/*/{Protheus.doc} GenRandThread
Controle de numera��o das threads                              

@type Function
@author Pedro Pereira Lima
@since 30/10/2013
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function GenRandThread()
Local nThreadId := nRandThread

While nRandThread == nThreadId .Or. nRandThread == 0
	nRandThread := Randomize(10000,29999)
EndDo

nThreadId := nRandThread

Return nThreadId

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918aIniVar
Inicia as variaveis staticas utilizadas no fonte               

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918aIniVar()
 
If __nThreads == Nil 
	__nThreads	:= SuperGetMv( "MV_BLATHD" , .T. , 1 )	// Limite de 20 Threads permitidas
EndIf

If __nLoteThr == Nil
	__nLoteThr	:= SuperGetMv( "MV_BLALOT" , .T. , 50 )	// Quantidade de registros por lote
Endif

__nThreads := If( (__nThreads > 20) , 20 , __nThreads )

If __lProcDocTEF == Nil
    __lProcDocTEF  := SuperGetMv( "MV_BLADOC" , .T. , .F. ) // Verifica se ir� processar pelo DOCTEF ou pelo NSUTEF. Padr�o � pelo NSUTEF
Endif

If nTamBanco == Nil
	nTamBanco		:= TAMSX3("A6_COD")[1]
Endif

If nTamAgencia == Nil 
	nTamAgencia	:= TamSX3("A6_AGENCIA")[1]
Endif

If nTamCC == Nil
	nTamCC		:= TAMSX3("A6_NUMCON")[1]
Endif

If nTamCheque == Nil
	nTamCheque		:= TAMSX3("EF_NUM")[1]
Endif

If nTamNatureza == Nil
	nTamNatureza	:= TAMSX3("ED_CODIGO")[1]
Endif

If lMEP == Nil 
	lMEP		:= AliasInDic("MEP")
Endif

If nTamParc == Nil
	nTamParc		:= TamSX3("FIF_PARCEL")[1]
Endif

If nTamParc2 == Nil
	nTamParc2		:= TamSX3("FIF_PARALF")[1]
Endif

If lTamParc == Nil
	lTamParc	:= TamSX3("E1_PARCELA")[1] == TamSX3("FIF_PARCEL")[1]
Endif

If lA6MSBLQL == Nil
	lA6MSBLQL := ( SA6->(FieldPos( 'A6_MSBLQL') ) > 0 )
Endif

If lFifRecSE1 == Nil
    lFifRecSE1 := ( FIF->(FieldPos( 'FIF_RECSE1' ) ) > 0 )
Endif

If nTamNSUTEF == Nil 
    nTamNSUTEF := TamSX3("FIF_NSUTEF")[1]
Endif

If nTamDOCTEF == Nil .And. __lDocTef 
    nTamDOCTEF := TamSX3("FIF_NUCOMP")[1]
Endif

ProcLogAtu(STR0138,STR0152)

ProcLogAtu(STR0138,STR0153)

Return

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918EfetuaBX
Efetua a baixa do titulo por lote ou individual               

@type Function
@author Unknown
@since 29/04/2011
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918EfetuaBX( aLotes, oDlg, aRegConc, lRet )
Local aTitulosBx	:= {}
Local lMsErroAuto
Local nCount		:= 0									//Contador utilizado para varrer o array da FIF
Local nConcAux		:= 0 
Local nIx 			:= 0 
Local aConcAux 		:= {}
Default lRet		:= .F.

If cConcilia == 2 //Baixa por lote

	If __nThreads > 1
		lRet := A918ThrLote( aLotes, aRegConc, oDlg)
	Else

		For nCount := 1 To Len( aLotes )
			
			aTitulosBX := aLotes[nCount][9]
			
			IncProc(STR0106 + ", " + STR0111 + aLotes[nCount][1] + STR0112 + AllTrim( Str( Len( aTitulosBX ) ) ) + ")") //"Aguarde..."#(Lote: "#" / Qtde T�tulos: "
			
			If FBxLotAut("SE1", aTitulosBX, aLotes[nCount][5], aLotes[nCount][6], aLotes[nCount][7],,aLotes[nCount][1],, aLotes[nCount][8])
				aConcAux := oConc:aArray
				For nIx := 1 To Len(aTitulosBX)
					nConcAux := aScan(aConcAux, {|x| x[22] = aTitulosBX[nIx] })
					If nConcAux > 0
						oConc:aArray[nConcAux][1]:cName := "BR_VERMELHO" 
					Endif
				Next nIx
			Else 
				 //"Inconsistencia encontradas no processo de Baixas por Lote. esta interface ser� encerrada para garantir a integridade dos dados na situa��o de baixa por lote."###"Opera��o Cancelada"
				 Help(" ",1,"A910Incosit",,STR0056 ,1,0,,,,,,{STR0057})
				Exit
			EndIf
		Next nCount			
	EndIf
	
Else  
	//Baixa individual
	GETEMPR(cEmpAnt + cFilAnt)
	
	lMsErroAuto	:= .F.
	
	aTitulosBX := aLotes
	
	MSExecAuto({|x, y| FINA070(x, y)}, aTitulosBX, 3)
	
	//Verifica se ExecAuto deu erro
	lRet := !lMsErroAuto

	If !lRet
		MostraErro()
		DisarmTransaction()
	EndIf
EndIf

If !lRet
   ProcLogAtu(STR0149,STR0056)
   oDlg:End()
Endif

Return lRet

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918ThrLote
Efetua o controle das threads da concilia��o                              

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918ThrLote( aLotes, aRegConc, oDlg)
Local oModelBxR
Local oSubFKA
Local oSubFK5
Local aTitulosBX	:= {}
Local lOk			:= .T.
Local lFa110Tot		:= ExistBlock( 'FA110TOT' )
Local nIx			:= 0
Local nJx			:= 0 
Local cLog			:= ''
Local cCamposE5		:= ''
Local cBco110		:= '' 
Local cAge110		:= ''
Local cCta110		:= ''
Local cLoteFin		:= ''
Local cCheque		:= ''
Local cNatureza     := Nil
Local lBaixaVenc    := lUseFIFDtCred	//se deve gravar a data de credito na E1_BAIXA
Local cNatLote		:= FINNATMOV( 'R' )
Local cMyUId		:= 'F918_THREAD_ID' //Defini��o do nome da se��o para controle de vari�veis "globais"
Local cKeyId		:= 'F918_KEY_'		//Defini��o da chave para controle de vari�veis "globais"
Local aValor		:= {}				//Array que armazenar� os valores "globais"
Local aRet			:= {}
Local aRetAux		:= {}
Local nX			:= 0
Local nY			:= 0
Local lSpbInUse 	:= SpbInUse()
Local aRecnosAux	:= {}
Local aLoteAux		:= {}
Local nConcAux		:= 0
Local aConcAux		:= {}
Local aGrvTxt		:= {}
Local lGrvTxt		:= .F.
Local cEndGrvTx     := ""
Local cThreadId		:= ""

Private oThredSE1	:= Nil	//Objeto controlador de MultThreads
Private nValorTef	:= 0

ProcRegua(Len(aLotes))

For nIx := 1 TO Len( aLotes )

	lOk := A918VldLote( aLotes[nIx] )

	If !lOk
		Exit	
	EndIf
Next nIx

If Len(aLotes) == 1
	If Len(aLotes[1][9]) <= 50
		For nIx := 1 To Len( aLotes[1,9] )
			aAdd(aRecnosAux, aLotes[1,9,nIx]) 
		Next nIx
		Pergunte( "FIN110", .F. )
		aConcAux := oConc:aArray
		aLoteAux := {aRecnosAux,aLotes[1,2],aLotes[1,3],aLotes[1,4],cCheque,aLotes[1,1],cNatureza,aLotes[1,8],lBaixaVenc}
		lMsErroAuto := .F.
		FINA110( 3 , aLoteAux, .F., ,)
		lOk := !lMsErroAuto
		If lOk 
			For nIx := 1 To Len(aRecnosAux)
				nConcAux := aScan(aConcAux, {|x| x[22] = aRecnosAux[nIx] })
				If nConcAux > 0
					oConc:aArray[nConcAux][1]:cName := "BR_VERMELHO" 
				Endif
			Next nIx
		EndIf 
		Return lOk
	Endif
Endif

If lOk
	
For nJx := 1 TO Len( aLotes )
	aRetThread := {} 
	aRetAux := {} 
	cThreadId := SubStr( 'FA110_' + AllTrim(Str(GenRandThread())),1,15)

	//Defino a se��o, disponibilizando vari�veis globais que podem ser enxergadas nas threads que ser�o abertas
	VarSetUID( cMyUId+cThreadId )

	// Objeto controlador de Threads
	oThredSE1 := FWIPCWait():New(cThreadId, 10000 )
	
	oThredSE1:SetThreads(__nThreads)
	
	oThredSE1:StopProcessOnError(.T.)
	
	oThredSE1:SetEnvironment(cEmpAnt,cFilAnt)
	
	oThredSE1:Start( 'A918ATHRBX' )
	
	aTitulosBX := AClone( aLotes[nJx] )
	
	If __lConoutR
		ConoutR( STR0106 + ', '+ STR0111 + aTitulosBX[1] + STR0112 + AllTrim( Str( Len( aTitulosBX[9] ) ) ) + ')' )
	EndIf 

	IncProc( STR0106 + ', ' + STR0111 + aTitulosBX[1] + STR0112 + AllTrim( Str( Len( aTitulosBX[9] ) ) ) + ')') //"Aguarde..."#(Lote: "#" / Qtde T�tulos: "

	ProcLogAtu( STR0138, StrZero( ThreadId(),10) + ' ' + STR0111 + aTitulosBX[1] + STR0112 + AllTrim( Str( Len( aTitulosBX[9] ) ) ) + ')')

	lOk := A918APreBx( oThredSE1, aTitulosBX, aRegConc, cThreadId)
	
	If !lOk
		Exit
		
		oThredSE1:Stop() //Metodo aguarda o encerramento de todas as threads antes de retornar o controle.			
	Else

		oThredSE1:Stop() //Metodo aguarda o encerramento de todas as threads antes de retornar o controle.

		cErro := oThredSE1:GetError()

		aConcAux := oConc:aArray
		
		For nIx := 1 To Len(aRetThread)
			VarGetA(cMyUId+cThreadId, aRetThread[nIx][1], @aRet)
			If Len(aRet) > 0
				For nX := 1 To Len(aRet)
					nConcAux := aScan(aConcAux, {|x| x[22] = aRet[nX][1][2] })
					If aRet[nX][1][1]
						If nConcAux > 0
							oConc:aArray[nConcAux][1]:cName := "BR_VERMELHO" 
						Endif
					Else
						If nConcAux > 0
							oConc:aArray[nConcAux][1]:cName := "BR_AMARELO" 
						Endif
						lGrvTxt := .T.
						aAdd(aGrvTxt,{aRet[nX][1][2]})
					Endif
				Next nX
			Else
				lOk 	:= .F.
				lGrvTxt := .T.
				For nY := 1 To Len(aRetThread[nIx][2])
					aAdd(aGrvTxt,{aRetThread[nIx][2][nY]})
				Next nY
			Endif
			aAdd(aRetAux,aRet)
		Next nIx
		
		If Len(aRetAux) > 0
			For nIx := 1 To Len(aRetAux) 
				If Len(aRetAux[nIx]) > 0 .And. (Len(aRetAux[nIx]) - 1 ) == Len(aRetThread[nIx][2])
					Loop
				Else
					If Len(aRetAux[nIx]) > 0
						For nX := 1 To Len(aRetThread[nIx][2])
							If nX <= Len(aRetAux[nIx])
								Loop
							Else
								lOk 	:= .F.
								lGrvTxt := .T.
								aAdd(aGrvTxt,{aRetThread[nIx][2][nX]})
							Endif
						Next nX
					Endif
				Endif
			Next nIx
		Endif
	
		//Obtenho o array que foi alimentado pelas threads
		VarGetA( cMyUId+cThreadId, cKeyId, @aValor )
		
		//Varro o array atr�s dos valores totais gravados pelas threads que foram executadas
		For nX := 1 To Len(aRetAux) 
			If Len(aRetAux[nX]) > 0
				nValorTef += aRetAux[nX][Len(aRetAux[nX])][1][2]
			Endif
		Next nX
	
			//Gera registro totalizador no SE5, caso baixa seja
			//aglutinada (BX_CNAB=S)
			If nValorTef > 0 .And. __lBxCnab
				cBco110		:= PadR( aTitulosBX[5], nTamBanco	)
				cAge110		:= PadR( aTitulosBX[6], nTamAgencia	)
				cCta110		:= PadR( aTitulosBX[7], nTamCC		)
				cLoteFin	:= aTitulosBX[1]
				dBaixa		:= aTitulosBX[8]
	
				SE1->( DbSetOrder(1) )
				SE1->( DbSeek( xFilial('SE1') + aTitulosBX[1] + aTitulosBX[2] + aTitulosBX[3] + aTitulosBX[4] + aTitulosBX[5] + aTitulosBX[7] ) )
				
				//Define os campos que n�o existem nas FKs e que ser�o gravados apenas na E5, para que a grava��o da E5 continue igual
				//Estrutura para o E5_CAMPOS: "{{'SE5->CAMPO', Valor}, {'SE5->CAMPO', Valor}}|{{'SE5->CAMPO', Valor}, {'SE5->CAMPO', Valor}}"
				cCamposE5 := "{"
			
			   	oModelBxR := FWLoadModel( 'FINM030' )
				oModelBxR:SetOperation( MODEL_OPERATION_INSERT ) //Inclus�o
				oModelBxR:Activate()
				oModelBxR:SetValue( 'MASTER', 'E5_GRV'	, .T.	) //Informa se vai gravar SE5 ou n�o 
				oModelBxR:SetValue( 'MASTER', 'NOVOPROC', .T.	) //Informa que a inclus�o ser� feita com um novo n�mero de processo
				
				//Dados do Processo
				oSubFKA := oModelBxR:GetModel( 'FKADETAIL' )
				oSubFKA:SetValue( 'FKA_IDORIG', FWUUIDV4()	)
				oSubFKA:SetValue( 'FKA_TABORI', 'FK5'		)
				
				//Informacoes do movimento
				oSubFK5 := oModelBxR:GetModel( 'FK5DETAIL' )
				oSubFK5:SetValue( 'FK5_VALOR'	, nValorTef										)
				oSubFK5:SetValue( 'FK5_TPDOC'	, 'VL'											)
				oSubFK5:SetValue( 'FK5_BANCO'	, cBco110										)
				oSubFK5:SetValue( 'FK5_AGENCI'	, cAge110										)
				oSubFK5:SetValue( 'FK5_CONTA'	, cCta110										)
				oSubFK5:SetValue( 'FK5_RECPAG'	, 'R'											)
				oSubFK5:SetValue( 'FK5_HISTOR'	, STR0164 + " / " + STR0165 + ": " + cLoteFin	) // "Baixa Automatica / Lote: "	
				oSubFK5:SetValue( 'FK5_DTDISP'	, dBaixa										)
				oSubFK5:SetValue( 'FK5_FILORI'	, F918BxLote()									)
				oSubFK5:SetValue( 'FK5_ORIGEM'	, Substr(FunName(),1,8)							)				
				oSubFK5:SetValue( 'FK5_LOTE'	, cLoteFin										)
				oSubFK5:SetValue( 'FK5_NATURE'	, cNatLote										) 
				oSubFK5:SetValue( 'FK5_MOEDA'	, StrZero( SE1->E1_MOEDA, 2 )					)
				
				oSubFK5:SetValue( 'FK5_DATA', dBaixa )
				cCamposE5 += '{"E5_DTDIGIT",STOD("' + DtoS( dDataBase ) + '")}'
				cCamposE5 += ',{"E5_DTDISPO",STOD("' + DtoS( dBaixa ) + '")}'
				
				If lSpbInUse
					cCamposE5 += ',{"E5_MODSPB","1"}'
				Endif
				
				cCamposE5 += ',{"E5_LOTE","' + cLoteFin + '"}'
				cCamposE5 += '}'
				
				oModelBxR:SetValue( 'MASTER', 'E5_CAMPOS', cCamposE5 ) //Informa os campos da SE5 que ser�o gravados indepentes de FK5
			
				If oModelBxR:VldData()		
					oModelBxR:CommitData()
					SE5->( dbGoto( oModelBxR:GetValue( 'MASTER', 'E5_RECNO' ) ) )
				Else
					lOk := .F.
					cLog := cValToChar(oModelBxR:GetErrorMessage()[MODEL_MSGERR_IDFIELDERR]) + ' - '
				    cLog += cValToChar(oModelBxR:GetErrorMessage()[MODEL_MSGERR_ID]) + ' - '
				    cLog += cValToChar(oModelBxR:GetErrorMessage()[MODEL_MSGERR_MESSAGE])        	
				    
				    Help( ,,'M030VALID',,cLog, 1, 0 )	            
				EndIf
				
				oModelBxR:DeActivate()
				oModelBxR:Destroy()
				
				// PONTO DE ENTRADA FA110TOT
				// ExecBlock para gravar dados complementares ao registro totalizador
				If lFa110Tot
					Execblock( 'FA110TOT', .F., .F. )
				EndIf
			
				// Atualiza saldo bancario
				AtuSalBco( cBco110, cAge110, cCta110, SE5->E5_DATA, SE5->E5_VALOR, '+' )
			EndIf	

			aValor := {}

			//Zero o valor da vari�vel, evitando que ocorra somat�ria incorreta dos totais
			nValorTef := 0
					
		EndIf

		//Deleto a se��o ap�s sua utiliza��o
		VarClean( cMyUId+cThreadId )
		FreeObj( oThredSE1 )
	Next nJx 

	If lGrvTxt
		If Aviso(STR0183, STR0189, {STR0188,STR0187}, 3) = 1 //"Existem titulos que n�o foram conciliados. Deseja gerar arquivo LOG para conferencia?."
			cEndGrvTx := FA918GrvTx(aGrvTxt )	
			Aviso(STR0183, STR0184 + STR0185 + cEndGrvTx,{STR0104}, 3)  // "Aten��o" # "Os titulos que n�o foram conciliados est�o relacionados no arquivo, gravado em: "
		Endif
	Endif

	IncProc( STR0154 )

	oThredSE1 := Nil

	//Deleto a se��o ap�s sua utiliza��o
	VarClean( cMyUId )
EndIf

Return lOk

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918VldLote
Efetua a valia��o do lote de processamento dos lotes                              

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918VldLote( aLotes )
Local lOk		:= .T.
Local cBanco	:= PADR(aLotes[5],nTamBanco  )
Local cAgencia	:= PADR(aLotes[6],nTamAgencia)
Local cConta	:= PADR(aLotes[7],nTamCC     )

//Verifico informacoes para processo
If Empty(cBanco) .or. Empty(cAgencia) .or. Empty(cConta) 
	Help(" ",1,"BXLTAUT1",,STR0049, 1, 0 )		//"Informa��es incorretas n�o permitem a baixa autom�tica em lote. Verifique as informa��es passadas para a fun��o FBXLOTAUT()"
	lOk		:= .F.
ElseIf !CarregaSa6(@cBanco,@cAgencia,@cConta,.T.,,.F.)
	lOk		:= .F.
ElseIf Empty(aLotes[9])
	Help(" ",1,"RECNO")
	lOk		:= .F.
Endif

Return lOk

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918APreBx
Inicia a prepara��o das baixas dos cart�es                              

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function A918APreBx( oThread As Object , aTitulosBX As Array, aRegConc As Array, cThreadId As Character) As Logical

Local aRecnosAux    As Array
Local cChave        As Character
Local cMyUId		As Character
Local cKeyId		As Character
Local cBanco        As Character
Local cAgencia      As Character
Local cConta        As Character
Local cCheque       As Character
Local cLoteFin      As Array
Local cNatureza     As Character
Local aRecnos       As Array
Local lOk           As Logical
Local lBaixaVenc    As Logical
Local nIx           As Numeric 
Local nCont         As Numeric
Local nContAux		As Numeric
Local nQtLote		As Numeric
Local nCtrlLote		As Numeric
Local aValor		As Array
Local aRetTHD		As Array
Local nQtdeRec		As Numeric
Local nTHDAux		As Numeric

Private dBaixa      As Date 

aRecnosAux    := {}
cChave        := "FA110BXAUT_THRD"+cThreadId
cMyUId		  := 'F918_THREAD_ID' //Defini��o do nome da se��o para controle de vari�veis "globais"
cKeyId	      := ''		//Defini��o da chave para controle de vari�veis "globais"'
cBanco        := PADR(aTitulosBX[5],nTamBanco  )
cAgencia      := PADR(aTitulosBX[6],nTamAgencia)
cConta        := PADR(aTitulosBX[7],nTamCC     )
cCheque       := ''
cLoteFin      := aTitulosBX[1]
cNatureza     := Nil
aRecnos       := aTitulosBX[9]
lOk           := .T.
lBaixaVenc    := lUseFIFDtCred	//se deve gravar a data de credito na E1_BAIXA
nIx           := 0
nCont         := 0
nContAux		:= 0
nQtLote		:= 0
nCtrlLote		:= 0
aValor		:= {}
aRetTHD		:= {}
nQtdeRec		:= Int(Len(aRecnos) / __nLoteThr) + 1 
nTHDAux		:= __nThreads

dBaixa      := aTitulosBX[8]


//Priorizo quantidade de recno por lote 
If nQtdeRec <= nTHDAux
	nTHDAux := nQtdeRec
	nQtLote := Int(Len(aRecnos) / nTHDAux) + 1
Else
	//Priorizo o numero de Threads
	nQtLote := Int(Len(aRecnos) / __nThreads)
Endif

If !LockByName( cChave, .F. , .F. )
	Help( " " ,1, cChave ,,STR0155,1, 0 )
	lOk := .F.
Else
	// Abertura de Threads
	ProcRegua( Len( aRecnos ) )
					
	For nIx := 1 To Len( aRecnos )
		IncProc()
		nCont++
		nContAux++
		aAdd( aRecnosAux, aRecnos[nIx] )
	
		If nCont == nQtLote .Or. nContAux == Len( aRecnos )
			
			nCtrlLote++
			cKeyId	:= 'F918_KEY_' + cLoteFin + StrZero(nCtrlLote,3)

			aAdd(aRetThread, {cKeyId, aRecnosAux})
			
			VarSetA( cMyUId+cThreadId, cKeyId, aRetTHD )

			// Chamada da fun��o A918ATHRBX( aTitulos, aRegConc )
			oThread:Go( {aRecnosAux,cBanco,cAgencia,cConta,cCheque,cLoteFin,cNatureza,dBaixa,lBaixaVenc}, aRegConc, .T.,cKeyId,dDataBase,cMyUId+cThreadId)
	        Sleep(500)
		   aRecnosAux	:= {}
		   aValor		:= {}
           nCont		:= 0
		EndIf
	Next nIx

	// Fechamento das Threads   
	UnLockByName( cChave, .F. , .F. )
EndIf	

Return lOk

//---------------------------------------------------------------------------
/*/{Protheus.doc} A918ATHRBX
Rotina de controle das baixas. Inicializa uma trasa��o porpor thread. Caso 
caia, somente aquela thread � afetada.      

Incluido dData para que a Thread suba com a database da execu��o. 

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Function A918ATHRBX( aTitulos As Array, aRegConc As Array, lThread As Logical, cKeyId As Character, dData As Date, cMyUId As Character) As Logical

Local lOk As Logical

Default lThread	 := .T.
Default cKeyId   := ""
Default dData	 := Date()

lOk			:= .F.
dDatabase	:= dData
lMsErroAuto := .F.
Pergunte( "FIN110", .F. )

If __lConoutR
	ConoutR( StrZero(ThreadId(),10) + STR0156 )
Endif

FINA110(3,aTitulos,lThread,cKeyId,aRegConc,cMyUId)

//Verifica se ExecAuto deu erro
lOk := !lMsErroAuto

If !lOk .And. __lConoutR
   ConoutR( StrZero(ThreadId(),10) + STR0157 )
   /* Como aqui � um processo via Thread, n�o deveriamos ter processo de tela. 
	Outra solu��o � arrumar a passagem de parametro para gravar o arquivo do erro e posteriormente ler esse arquivo. 
   ConoutR( MostraErro() )*/
   DisarmTransaction()
Endif

If __lConoutR
	ConoutR( StrZero(ThreadId(),10) + STR0158 )
Endif
Return lOk
//---------------------------------------------------------------------------
/*/{Protheus.doc} LoadBanco
Carrega todos os bancos utiliados na concilia��o para a memoria. Evitando a  
busca repetida de dados no banco de dados

@type Function
@author Unknown
@since 21/01/2014
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function LoadBanco(cCodFil)
Local aArea			:= GetArea()
Local bCondWhile	:= {|| .T. }
Local cQuery 		:= ""
Local cAliasSA6	:= "SA6"
Local cFilSA6		:= ""
Local lSA6Comp 	:= .F.

lSA6Comp := F916ChkMod("SA6")

If (xFilial( 'SA6' ) == cCodFil) .Or. !lSA6Comp
	cFilSA6	:= xFilial( 'SA6' )
Else 
	cFilSA6	:= cCodFil
Endif


// garanto que a variavel est� limpa para o carregamento
__aBancos := {}

If __lDefTop == Nil
	__lDefTop := FindFunction("IFDEFTOPCTB") .And. IfDefTopCTB() 
EndIf

If __lDefTop
	cAliasSA6 := GetNextAlias()
	
	cQuery := "SELECT A6_FILIAL, A6_COD, A6_AGENCIA, A6_DVAGE, A6_NUMCON, A6_DVCTA, A6_BLOCKED"
	
	If lA6MSBLQL
		cQuery += ", A6_MSBLQL"
	Endif
	
	cQuery += "  FROM " + RetSqlName("SA6") + " SA6"
	cQuery += " WHERE SA6.A6_FILIAL = '" + cFilSA6 + "'"
	cQuery += "   AND SA6.D_E_L_E_T_ = ' '"
	
	cQuery := ChangeQuery( cQuery )

	// verifica se temporario est� aberto e tenta fechalo
	If Select( cAliasSA6 ) > 0
		DbSelectArea( cAliasSA6 )
		( cAliasSA6 )->( DbCloseArea() )
	Endif

	dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQuery) , cAliasSA6 )
Else
	bCondWhile := {|| A6_FILIAL == cFilSA6 }
	(cAliasSA6)->( DbSetOrder(1) )
Endif

If Select( cAliasSA6 ) > 0
	DbSelectArea( cAliasSA6 )
	(cAliasSA6)->(DbGoTop())

	nCont := 0
	While (cAliasSA6)->(!Eof()) .And. Eval( bCondWhile )
		// adiciono os bancos a serem utilizados na busca, otimiza��o do carregamento dos dados
		Aadd( __aBancos,	{	(cAliasSA6)->A6_FILIAL;
							,	(cAliasSA6)->A6_COD;
						 	,	(cAliasSA6)->A6_AGENCIA;
							,	(cAliasSA6)->A6_DVAGE;
							,	(cAliasSA6)->A6_NUMCON;
							,	(cAliasSA6)->A6_DVCTA;
							,	(cAliasSA6)->A6_BLOCKED;
							,	Iif( lA6MSBLQL, (cAliasSA6)->A6_MSBLQL, Nil );
							})
		(cAliasSA6)->( DbSkip() )
	EndDo
Endif

If __lDefTop
	// verifica se temporario est� aberto e tenta fechalo
	If Select( cAliasSA6 ) > 0
		DbSelectArea( cAliasSA6 )
		( cAliasSA6 )->( DbCloseArea() )
	Endif
Endif

RestArea( aArea )

Return


//---------------------------------------------------------------------------
/*/{Protheus.doc} FA918GrvTx
Grava arquivo texto com os daddos da SE1 que n�o foram conciliadas 

@type Function
@author Francisco Oliveira
@since 05/08/2018
@version 12   
/*/
//---------------------------------------------------------------------------

Static Function FA918GrvTx(aGrvTxt As Array) As Character 

Local aAreaSE1	As Array

Local nI        As Numeric
Local nHdl		As Numeric
Local cEOL    	As Character 
Local cLin		As Character 
Local cExtens	As Character 
Local cMainPath	As Character 

aAreaSE1	:= SE1->(GetArea())

cEOL    	:= "CHR(13)+CHR(10)"
cLin		:= ""
cExtens		:= "Arquivo TXT | *.TXT"
cMainPath	:= "C:\N_Conc_01.TXT"


cFileOpen 	:= cGetFile(cExtens,STR0225,,cMainPath,.T.)
nHdl    	:= fCreate(cFileOpen)
Incproc(Len(aGrvTxt))


cLin := "FILIAL; FILORIG; PREFIXO; NUMERO; PARCELA; TIPO; VENCIMENTO; EMISSAO; DT BAIXA;  SALDO"
cLin += &cEOL
cLin += &cEOL

If fWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
	Aviso(STR0183, STR0186,{STR0104}, 3 ) //"Atencao!" # "Ocorreu um erro na gravacao do arquivo. Favor verificar"
Endif
	
For nI := 1 To Len(aGrvTxt)

	SE1->(DbGoTo(aGrvTxt[nI][1]))

	cLin	:= ""
	
	cLin := SE1->E1_FILIAL  		+ ";"
	cLin += SE1->E1_FILORIG 		+ ";"
	cLin += SE1->E1_PREFIXO 		+ ";" 
	cLin += SE1->E1_NUM     		+ ";" 
	cLin += SE1->E1_PARCELA 		+ ";" 
	cLin += SE1->E1_TIPO 			+ ";" 
	cLin += DTOC(SE1->E1_VENCTO)	+ ";" 
	cLin += DTOC(SE1->E1_EMISSAO)	+ ";" 
	cLin += DTOC(SE1->E1_BAIXA)		+ ";" 
	cLin += cValToChar(SE1->E1_SALDO)
	
	cLin += &cEOL
	
	If fWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
		Aviso(STR0183, STR0186,{STR0104}, 3 ) //"Atencao!" # "Ocorreu um erro na gravacao do arquivo. Favor verificar"
		Exit
	Endif
	
Next nI

fClose(nHdl)

RestArea(aAreaSE1)

Return cFileOpen

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FA918ALEG
Rotina que monta a legenda dentro da tela de concila��o
@type Function
@author Fernando Navarro 
@since 07/05/2019
@version 12
/*/
//-------------------------------------------------------------------------------------
Function FA918ALEG()

BrwLegenda(STR0230 /*"Conciliacao TEF"*/,STR0229,{{"BR_VERDE"		,STR0231 /*"Selecionado para Conciliar"*/ },; //#"N�o Processado"
										{"BR_VERMELHO"	,STR0232 /*"Conciliado com Sucesso"*/ },; //#"Conciliado Normal"
										{"BR_AMARELO"	,STR0233 /*"Problemas na Concilia��o"*/ },; //#"Divergente"
										{"BR_BRANCO"	,STR0234 /*"N�o selecionado"*/ },; //#"Descartado"
										{"BR_PRETO"		,STR0235 /*"Conta n�o cadastrada"*/  }}) //#"Antecipado"			

Return 

//---------------------------------------------------------------------------
/*/{Protheus.doc} RefJus
Carrega todas as justificativas cadastradas para utiliza��o em tela, no caso  
de conciliado parcialmente 

@type Static Function
@author Danilo Santos - Servi�os Private
@since 04/04/2018
@version 12   
/*/
//---------------------------------------------------------------------------
Static Function RefJus(aJust,oJust)

Local nLinGd := oJust:nAt
Local cMotivo := oJust:aArray[nLinGd, 31] 
Local oTMultiget
Local oDlg
Local nOpc := 0

 DEFINE DIALOG oDlg TITLE STR0190 FROM 180, 180 TO 450, 700 PIXEL // "Informe a Justificativa"
   @ 00,00 MSPANEL oTMultiget PROMPT "" SIZE 20,20 of oDlg
   oTMultiget:Align := CONTROL_ALIGN_ALLCLIENT
   oTMultiget := tMultiget():New(01, 02, {| u| Iif(pCount() == 0, cMotivo, cMotivo := u)}, oDlg, 258, 92,,,,,, .T.,, .T., {|| .T.},,, .F., {|| .T.},, .T., .T.)
   @ 00,00 MSPANEL oPBotoes PROMPT "" SIZE 20,30 of oDlg
   oPBotoes:Align := CONTROL_ALIGN_BOTTOM
   @ 10, 160 Button STR0055 Size 040, 10 Pixel Of oPBotoes Action (nOpc := 0, oDlg:End()) // "Cancela"
   @ 10, 200 Button STR0058 Size 040, 10 Pixel Of oPBotoes Action (nOpc := 1, oDlg:End()) // "Confirma"
   ACTIVATE DIALOG oDlg CENTERED
   If nOpc == 1
    oJust:aArray[nLinGd, 31] := cMotivo
   Endif

oJust:Refresh()

Return 

//-------------------------------------------------------------------
/*/{Protheus.doc} F910ADM
Consulta Especifica das Administradoras Financeiras - Conciliador
Sitef
 ** FUN��O SE MANTEVE COMO F910ADM POIS � UTILIZADA NO DICIONARIO - SXB

@author Danilo Santos
@since 10/04/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function F910ADM()
	Local aColumns	:= {}
	Local aPesq		:= {}
	Local aStruct	:= SX5->(DBStruct())

	Local cMvPar	:= Alltrim(ReadVar())
	Local bOk		:= {|| lRetorno := F918Sel(cArqTrb, cMvPar), IIf(lRetorno, oDlg:End(),Nil)}
	Local bCancel	:= {|| oMrkBrw:Deactivate(), oDlg:End()}

	Local cArqTrb	:= GetNextAlias()
	Local cQuery	:= ""
	Local nCampo	:= 0

	Local lRetorno	:= .F.
	Local oDlg		:= NIL
	Local oMrkBrw	:= NIL

	//Limpa a Variavel de Retorno
	__cAdmFin := ""

	If __oF918ADM <> NIL
		__oF918ADM:Delete()
		__oF918ADM := NIL
	EndIf

	//Cria o Objeto do FwTemporaryTable
	__oF918ADM := FwTemporaryTable():New(cArqTrb)

	//Cria a estrutura do alias temporario
	Aadd(aStruct, {"X5_OK", "C", 1, 0})	 //Adiciono o campo de marca
	__oF918ADM:SetFields(aStruct)

	__oF918ADM:AddIndex("1", {"X5_TABELA"})
	__oF918ADM:AddIndex("2", {"X5_CHAVE"})

	//Criando a Tabela Temporaria
	__oF918ADM:Create()

	//Selecao dos Dados da SX5 - G3
	cQuery += "SELECT	SX5.X5_CHAVE" + CRLF 
	cQuery += ",		SX5.X5_DESCRI" + CRLF 
	cQuery += " FROM		" + RetSqlName("SX5") + " SX5" + CRLF
	cQuery += " WHERE	SX5.X5_FILIAL = '" + FWxFilial("SX5") + "'" + CRLF
	cQuery += " AND SX5.X5_TABELA = 'G3' "
	cQuery += " AND		SX5.D_E_L_E_T_ = ''" + CRLF
	cQuery += " ORDER BY " + SqlOrder(SX5->(IndexKey())) + CRLF

	cQuery := ChangeQuery(cQuery)

	//Cria arquivo temporario
	Processa({|| SqlToTrb(cQuery, aStruct, cArqTrb)})

	//Fica na ordem da query
 	DbSetOrder(0)

	//MarkBrowse
	For nCampo := 1 To Len(aStruct)
		If	aStruct[nCampo][1] $ "X5_CHAVE|X5_DESCRI"
			AAdd(aColumns, FWBrwColumn():New())
			aColumns[Len(aColumns)]:SetData( &("{||" + aStruct[nCampo][1] + "}") )
			aColumns[Len(aColumns)]:SetTitle(RetTitle(aStruct[nCampo][1])) 
			aColumns[Len(aColumns)]:SetSize(aStruct[nCampo][3]) 
			aColumns[Len(aColumns)]:SetDecimal(aStruct[nCampo][4])
			aColumns[Len(aColumns)]:SetPicture(PesqPict("X5", aStruct[nCampo][1])) 
		EndIf 	
	Next nX 
	
	//Regras para pesquisa na tela
	Aadd(aPesq, {AllTrim(RetTitle("X5_CHAVE")), {{"SX5", "C", TamSX3("X5_CHAVE")[1], 0, AllTrim(RetTitle("X5_CHAVE")), "@!"}}, 1})
	Aadd(aPesq, {AllTrim(RetTitle("X5_DESCRI")), {{"SX5", "C", TamSX3("X5_DESCRI")[1], 0, AllTrim(RetTitle("X5_DESCRI")), "@!"}}, 2})
	
	If !(cArqTrb)->(Eof())
		DEFINE MSDIALOG oDlg TITLE STR0059 From 300, 0 to 800,800 OF oMainWnd PIXEL //Administradoras
		oMrkBrw := FWMarkBrowse():New()
		oMrkBrw:oBrowse:SetEditCell(.T.)	
		oMrkBrw:SetFieldMark("X5_OK")
		oMrkBrw:SetOwner(oDlg)
		oMrkBrw:SetAlias(cArqTrb)
		oMrkBrw:SetSeek(.T., aPesq)
		oMrkBrw:SetMenuDef("FINA918A")
		oMrkBrw:AddButton(STR0058, bOk, NIL, 2) //Confirmar
		oMrkBrw:AddButton(STR0180, bCancel, NIL, 2) //Cancelar 
		oMrkBrw:bMark	:= {||}
		oMrkBrw:bAllMark	:= {|| F918MrkAll(oMrkBrw,cArqTrb)}
		oMrkBrw:SetMark( "X", cArqTrb, "X5_OK" )
		oMrkBrw:SetDescription("")
		oMrkBrw:SetColumns(aColumns)
		oMrkBrw:SetTemporary(.T.) 
		oMrkBrw:Activate()
		ACTIVATE MSDIALOG oDlg CENTERED
	EndIf

	If __oF918ADM <> NIL
		__oF918ADM:Delete()
		__oF918ADM := NIL
	EndIf

Return lRetorno
//-------------------------------------------------------------------
/*/{Protheus.doc} F916GetAdm
Retorna as Administradoras Financeiras Selecionadas

@author Guilherme Santos
@since 15/03/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Function F910GetAdm()
Return __cAdmFin
//-------------------------------------------------------------------
/*/{Protheus.doc} F916Sel
Grava em uma String as Administradoras Selecionadas

@author Guilherme Santos
@since 15/03/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Static Function F918Sel(cArqTrb, cMvPar)
	Local lRet		:= .T.
	Local nRecno	:= 0
	Local nX		:= 0
	Local aParLin	:= {}
	Local nVarTam	:= 0
	Local nTotTam	:= 0
	Local nMaxTam	:= 60

	Default cArqTrb := ""
	Default cMvPar 	:= Alltrim(ReadVar())

	dbSelectArea(cArqTrb)
	nRecno := (cArqTrb)->(RecNo())
	(cArqTrb)->(DbGoTop())
	
	__cAdmFin 	:= ""
	While !(cArqTrb)->(Eof())
		If !Empty((cArqTrb)->X5_OK)
			__cAdmFin += If(nX > 0, ";" + Alltrim((cArqTrb)->X5_CHAVE), Alltrim((cArqTrb)->X5_CHAVE) )
			nX++
		EndIf
		(cArqTrb)->(DbSkip())
	End

	// Efetua a valida��o da quantidade de empresas selecionadas para evitar estouro no par�metro e error.log na query
	aParLin  := StrToArray( Alltrim(__cAdmFin) ,";")
	For nX := 1 to Len(aParLin)
		nVarTam := Len(Alltrim(aParLin[nX])) + 1
		If ( nTotTam + nVarTam ) <= nMaxTam
			nTotTam += nVarTam
		Else
			lRet := .F.
			Help(NIL, NIL, "F918Sel", NIL, STR0181, 1, 0) //"Limite de sele��o de empresas excedido para o par�metro da rotina (Max 60 Caracteres)."
			Exit
		EndIf
	Next nX

	(cArqTrb)->(DbGoTo(nRecno))

	If lRet
		lRet := Iif(Len(__cAdmFin) > 0, .T., .F.)
		&(cMvPar) := __cAdmFin
	EndIf
	
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} F918MrkAll
Marca ou Desmarca todas as Administradoras

@author Guilherme Santos
@since 15/03/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Function F918MrkAll(oMrkBrw, cArqTrb)
	Local cMarca := oMrkBrw:Mark()
	
	DbSelectArea(cArqTrb)
	(cArqTrb)->(DbGoTop())
	
	While !(cArqTrb)->(Eof())
		
		RecLock(cArqTrb, .F.)
		
		If (cArqTrb)->X5_OK == cMarca
			(cArqTrb)->X5_OK := " "
		Else
			(cArqTrb)->X5_OK := cMarca
		EndIf
		
		MsUnlock()
		(cArqTrb)->(DbSkip())	
	End
	
	(cArqTrb)->(DbGoTop())
	oMrkBrw:oBrowse:Refresh(.T.)

Return .T.


//-------------------------------------------------------------------
/*/{Protheus.doc} FilFVXF3
Fun��o usada para criar uma Consulta Padr�o com SQL

@author Danilo Santos
@since 23/04/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Function FilFVXF3(cTitulo,cQuery,nTamCpo,cAlias,cCodigo,cCpoChave,cTitCampo,cMascara,cRetCpo,nColuna,oJustf,lDiverg)

	Local nLista  
	Local cCampos 	:= ""
	Local bCampo	:= {}
	Local nCont		:= 0
	Local aCampos 	:= {}
	Local cTabela 
	Local cCSSGet		:= "QLineEdit{ border: 1px solid gray;border-radius: 3px;background-color: #ffffff;selection-background-color: #3366cc;selection-color: #ffffff;padding-left:1px;}"
	Local cCSSButton 	:= "QPushButton{background-repeat: none; margin: 2px;background-color: #ffffff;border-style: outset;border-width: 2px;border: 1px solid #C0C0C0;border-radius: 5px;border-color: #C0C0C0;font: bold 12px Arial;padding: 6px;QPushButton:pressed {background-color: #ffffff;border-style: inset;}"
	Local cCSSButF3	:= "QPushButton {background-color: #ffffff;margin: 2px;border-style: outset;border-width: 2px;border: 1px solid #C0C0C0;border-radius: 3px; border-color: #C0C0C0;font: Normal 10px Arial;padding: 3px;} QPushButton:pressed {background-color: #e6e6f9;border-style: inset;}"
	Local nX		:= 0
	Local cColuna := ""

	Private _oLista	:= nil
	Private _oDlg 	:= nil
	Private _oCodigo
	Private _cCodigo 	
	Private _aDados 	:= {}
	Private _nColuna	:= 0
	
	Default cTitulo 	:= ""
	Default cCodigo 	:= ""
	Default nTamCpo 	:= 30
	Default _nColuna 	:= 1
	Default cTitCampo	:= RetTitle(cCpoChave)
	Default cMascara	:= PesqPict('"'+cAlias+'"',cCpoChave)
	Default lDiverg     := .F.
	
	_nColuna	:= nColuna

	If Empty(cAlias) .OR. Empty(cCpoChave) .OR. Empty(cRetCpo) .OR. Empty(cQuery)
		MsgStop(STR0060,STR0149)
		Return
	Endif

	_cCodigo := Space(nTamCpo)
	_cCodigo := cCodigo

	cTabela:= CriaTrab(Nil,.F.)
	DbUseArea(.T.,"TOPCONN", TCGENQRY(,,cQuery),cTabela, .F., .T.)
     
	(cTabela)->(DbGoTop())
	If (cTabela)->(Eof())
		MsgStop(STR0061,STR0183)
		Return
	Endif
   
	Do While (cTabela)->(!Eof())
		/*Cria o array conforme a quantidade de campos existentes na consulta SQL*/
		cCampos	:= ""
		aCampos 	:= {}
		For nX := 1 TO FCount()
			bCampo := {|nX| Field(nX) }
			If ValType((cTabela)->&(EVAL(bCampo,nX)) ) <> "M" .OR. ValType((cTabela)->&(EVAL(bCampo,nX)) ) <> "U"
				if ValType((cTabela)->&(EVAL(bCampo,nX)) )=="C"
					cCampos += "'" + (cTabela)->&(EVAL(bCampo,nX)) + "',"
				ElseIf ValType((cTabela)->&(EVAL(bCampo,nX)) )=="D"
					cCampos +=  DTOC((cTabela)->&(EVAL(bCampo,nX))) + ","
				Else
					cCampos +=  (cTabela)->&(EVAL(bCampo,nX)) + ","
				Endif
					
				aadd(aCampos,{EVAL(bCampo,nX),Alltrim(RetTitle(EVAL(bCampo,nX))),"LEFT",30})
			Endif
		Next
     
     	If !Empty(cCampos) 
     		cCampos 	:= Substr(cCampos,1,len(cCampos)-1)
     		aAdd( _aDados,&("{"+cCampos+"}"))
     	Endif
     	
		(cTabela)->(DbSkip())     
	Enddo
   
	(cTabela)->( DbCloseArea() )
	
	If Len(_aDados) == 0
		MsgInfo(STR0061,STR0062)
		Return
	Endif
   
	nLista := aScan(_aDados, {|x| alltrim(x[1]) == alltrim(_cCodigo)})
     
	iif(nLista = 0,nLista := 1,nLista)
     
	Define MsDialog _oDlg Title STR0063 + IIF(!Empty(cTitulo)," - " + cTitulo,"") From 0,0 To 280, 500 Of oMainWnd Pixel
	
	oCodigo:= TGet():New( 003, 005,{|u| if(PCount()>0,_cCodigo:=u,_cCodigo)},_oDlg,205, 010,cMascara,{|| Processa({|| FilFVXF3P(M->_cCodigo)},STR0092) },0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,"",_cCodigo,,,,,,,cTitCampo + ": ",1 )
	oCodigo:SetCss(cCSSGet)	
	oButton1 := TButton():New(010, 212,STR0063,_oDlg,{|| Processa({|| FilFVXF3P(M->_cCodigo) },STR0092) },037,013,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButton1:SetCss(cCSSButton)	
	    
	_oLista:= TCBrowse():New(26,05,245,90,,,,_oDlg,,,,,{|| _oLista:Refresh()},,,,,,,.F.,,.T.,,.F.,,,.f.)
	nCont := 1
        //Para ficar din�mico a cria��o das colunas, eu uso macro substitui��o "&"
	For nX := 1 to len(aCampos)
		cColuna := &('_oLista:AddColumn(TCColumn():New("'+aCampos[nX,2]+'", {|| _aDados[_oLista:nAt,'+StrZero(nCont,2)+']},PesqPict("'+cAlias+'","'+aCampos[nX,1]+'"),,,"'+aCampos[nX,3]+'", '+StrZero(aCampos[nX,4],3)+',.F.,.F.,,{|| .F. },,.F., ) )')
		nCont++
	Next
	_oLista:SetArray(_aDados)
	_oLista:bWhen 		 := { || Len(_aDados) > 0 }
	_oLista:bLDblClick  := { || FilFVXF3R(_oLista:nAt, _aDados, cRetCpo,@oJustf,lDiverg)  }
	_oLista:Refresh()

	oButton2 := TButton():New(122, 005," OK "			,_oDlg,{|| Processa({|| FilFVXF3R(_oLista:nAt, _aDados, cRetCpo,@oJustf,lDiverg) },STR0092) },037,012,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButton2:SetCss(cCSSButton)	
	oButton3 := TButton():New(122, 047,STR0180	,_oDlg,{|| _oDlg:End() },037,012,,,.F.,.T.,.F.,,.F.,,,.F. )
	oButton3:SetCss(cCSSButton)	

	Activate MSDialog _oDlg Centered	
Return(bRet)

//-------------------------------------------------------------------
/*/{Protheus.doc} FilFVXF3P
Fun��o para retornar o que foi selecionado para o campo que chamou a consulta

@author Danilo Santos
@since 23/04/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Static Function FilFVXF3P(cBusca)
	Local i := 0

	if !Empty(cBusca)
		For i := 1 to len(_aDados)
			//Aqui busco o texto exato, mas pode utilizar a fun��o AT() para pegar parte do texto
			if UPPER(Alltrim(_aDados[i,_nColuna]))==UPPER(Alltrim(cBusca))
				//Se encontrar me posiciono no grid e saio do "For"			
				_oLista:GoPosition(i)
				_oLista:Setfocus()
				exit
			Endif
		Next
	Endif
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FilFVXF3R
Fun��o para retornar o que foi selecionado para o campo que chamou a consulta

@author Danilo Santos
@since 23/04/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Static Function FilFVXF3R(nLinha,aDados,cRetCpo,oJustf,lDiverg)
Local nLinGd  := oJustf:nAt
Local nPosCpo := oJustf:ColPos()
Local cDescri := ""
Default lDiverg := .F.

	cCodigo := aDados[nLinha,_nColuna]
	cDescri := aDados[nLinha,2]
//	&(cRetCpo) := cCodigo //Uso desta forma para campos como tGet por exemplo.
	If lDiverg
		oJustf:aArray[nLinGd, 29] := cCodigo
		oJustf:aArray[nLinGd, 31] := cDescri	
	Else
		oJustf:aArray[nLinGd, 29] := cCodigo
		oJustf:aArray[nLinGd, 30] := cDescri
	EndIf
	oJustf:Refresh()
	bRet := .T.
	_oDlg:End()    
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FVWF3
Fun��o para fazer o select das justificativas possiveis

@author Danilo Santos
@since 23/04/2018
@version 12.1.19
/*/
//-------------------------------------------------------------------
Function FVXF3(oJustf,aJustf,lDiverg)
	Local cTitulo		:= STR0169
	Local cQuery		:= "" 								//obrigatorio
	Local cAlias		:= "FVX"							//obrigatorio
	Local cCpoChave		:= "FVX_CODIGO" 					//obrigatorio
	Local cTitCampo		:= RetTitle(cCpoChave)			//obrigatorio
	Local cMascara		:= PesqPict(cAlias,cCpoChave)	//obrigatorio
	Local nTamCpo		:= TamSx3(cCpoChave)[1]		
	Local cRetCpo		:= "M-&gt;cJustifica"					//obrigatorio
	Local nColuna		:= 1	
	Local cCodigo		:= ""		//pego o conteudo e levo para minha consulta padr�o
	Local nColpos 		:= oJustf:ColPos() 	
	Local nColGrid      := 0
					
 	Private bRet 		:= .F.
 	
 	Default lDiverg     := .F.
 	
 	If lDiverg
 		nColGrid := 28
 	Else
 		nColGrid := 29
 	EndIf
 	
If nColpos == nColGrid
   	//Monto minha consulta, neste caso quero retornar apenas uma coluna, mas poderia inserir outros campos para compor outras colunas no grid, lembrando que n�o posso utilizar um alias para o nome do campo, deixar o nome real.
   	//Posso fazer qualquer tipo de consulta, usando INNER, GROUPY BY, UNION's etc..., desde que mantenha o nome dos campos no SELECT.
   	cQuery := " SELECT DISTINCT FVX.FVX_CODIGO, FVX.FVX_DESCRI  "
	cQuery += " FROM "+RetSQLName("FVX") + " FVX " //WITH (NOLOCK)
	cQuery += " WHERE FVX.FVX_FILIAL  = '" + xFilial("FVX") + "' "
	cQuery += " AND FVX.D_E_L_E_T_= ' ' "
	cQuery += " ORDER BY FVX.FVX_CODIGO "

 	bRet := FilFVXF3(cTitulo,cQuery,nTamCpo,cAlias,cCodigo,cCpoChave,cTitCampo,cMascara,cRetCpo,nColuna,@oJustf,lDiverg)
 	Return(bRet)
Else
	Return
EndIf


//-------------------------------------------------------------------
/*/{Protheus.doc} A918ParName
Fun��o para fazer o vinculo entre o parametro x cod. administradora 
que ser� utilizado na concilia��o SITEF

@author Marcelo Ferreira
@since 24/05/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Static Function A918ParName()
	
	Local cAliasQry	:= GetNextAlias()
	Local aRet		:= {}
	
	BeginSql Alias cAliasQry
		SELECT  MDE.MDE_CODIGO, MDE.MDE_DESC
		
		FROM %Table:MDE% MDE
		
		WHERE MDE.MDE_TIPO = %Exp:'RD'%
			AND	MDE.MDE_ARQIMP <> %Exp:' '% 
			AND MDE.MDE_DESC IN (%Exp:'REDE'%,%Exp:'REDECARD'%,%Exp:'AMEX'%,%Exp:'AMERICAN EXPRESS'%,%Exp:'CIELO'%,%Exp:'SOFTWARE EXPRESS'%,%Exp:'SOFTWAREEXPRESS'%)
			AND MDE.%NotDel%
	EndSql
	
	(cAliasQry)->( dbGoTop() )
	If !(cAliasQry)->( Eof() )

		While !(cAliasQry)->( Eof() )
			If AllTrim( (cAliasQry)->MDE_DESC ) $ 'AMEX|AMERICAN EXPRESS'
				aAdd(aRet, {(cAliasQry)->MDE_CODIGO, "MV_EMPTAME"})
			
			ElseIf AllTrim( (cAliasQry)->MDE_DESC ) $ 'CIELO'
				aAdd(aRet, {(cAliasQry)->MDE_CODIGO, "MV_EMPTCIE"})
		
			ElseIf AllTrim( (cAliasQry)->MDE_DESC ) $ 'REDE|REDECARD'
				aAdd(aRet, {(cAliasQry)->MDE_CODIGO, "MV_EMPTRED"})
			
			ElseIf AllTrim( (cAliasQry)->MDE_DESC ) $ 'SOFTWAREEXPRESS|SOFTWARE EXPRESS'
				aAdd(aRet, {(cAliasQry)->MDE_CODIGO, "MV_EMPTEF"})
			EndIf

			(cAliasQry)->( dbSkip() )
		EndDo

	EndIf

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} F918aStVen
Fun��o para retornar o FIF_STVEND (Status de Venda) correto no momento
do Concilia��o e estorno do Pagamento

@author Marcelo Ferreira
@since 16/06/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function F918aStVen( cStVenb As Character, lEstorno As Logical) As Character
Local cRet	As Character
Local nLin	As Numeric
Local nP	As Numeric
Local nR	As Numeric
Local aDePara As Array

Default cStVenb	 := ""
Default lEstorno := .F.

cRet := ""
nLin := 0
nP 	 := 0
nP 	 := 0
aDePara := {}

aAdd(aDePara, {"1", "A"}) //"N�o Processado"
aAdd(aDePara, {"2", "B"}) //"Conciliado Normal"
aAdd(aDePara, {"3", "C"}) //"Venda Conciliada Parcialmente"
aAdd(aDePara, {"4", "D"}) //"Divergente"
aAdd(aDePara, {"5", "E"}) //"Venda Conciliada com Critica"
aAdd(aDePara, {"6", "F"}) //"Titulo sem Registro de Venda"
aAdd(aDePara, {"7", "G"}) //"Registro de Venda sem Titulo"

If !Empty(cStVenb)
	If lEstorno
		nP := 2
		nR := 1
	Else
		nP := 1
		nR := 2
	EndIf
	nLin := aScan(aDePara, {|x| x[nP] == Alltrim(cStVenb) })
	If nLin > 0
		cRet := aDePara[nLin][nR]
	EndIf
EndIf

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} f918GetComb
Fun��o para retornar o descritivo do combo

@author Igor Fricks
@since 26/04/2018
@version 12.1.17
/*/
//-------------------------------------------------------------------
Function f918GetComb(cCampo,cConteudo)
Local aCombo 	:= {}
Local cCombo 	:= {}
Local aArea	 	:= GetArea()
Local aAreaSX3 	:= SX3->(GetArea())
Local nX		:= 0
Local cRet      := ""
Local nPos      := 0

If !Empty(Alltrim(cConteudo))
	If __cVarComb <> cCampo
		__cVarComb := cCampo
		__aVarComb := {}
		SX3->(dbSetOrder(2))
		If SX3->(MsSeek(cCampo))
			cCombo := SX3->( X3CBox() )
			aCombo := StrTokArr ( cCombo , ';' )
			For nx := 1 To Len(aCombo)
				aAdd(__aVarComb, StrTokArr(aCombo[nX],'=')) 
			Next
		EndIf
		RestArea(aAreaSX3)
		RestArea(aArea)
	EndIf
	
	nPos := aScan(__aVarComb, {|x| x[1] == cConteudo})
	
	If nPos > 0
		cRet := __aVarComb[nPos][2]
	EndIf
EndIf

Return cRet