#INCLUDE "rwmake.ch"
#INCLUDE "SIGAWIN.CH"
#INCLUDE "FINR880.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � FINR880  � Autor � Rubens Joao Pante     � Data � 10/08/00 ���
�������������������������������������������������������������������������Ĵ��
���Descripcio� Informe para generacion del Comprobante de Egreso          ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � General localizacion Colombia                              ���
�������������������������������������������������������������������������Ĵ��
���  DATA    � BOPS �                  ALTERACAO                          ���
�������������������������������������������������������������������������Ĵ��
���21.09.00  �Melhor�Acrescimo da conta nas perguntas. Alteracao da ident.���
���          �Rubens�das perguntas de COMPEG para FIN880 e do nome do prog���
���          �      �para FINR880                                         ���
���04/10/00  �xxxxxx�Acerto das perguntas para a versao 5.08              ���
���          �Rubens�Verificacao da existencia do campo EF_COMPROB        ���
���06/10/00  �xxxxxx�Acerto dos parametros da PesqPict()                  ���
���          �Rubens�                                                     ���
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function FINR880()
Local cInd
Local aOrd         := {}
Local cDesc1       := OemToAnsi(STR0001) //"Este programa tiene como objetivo imprimir informe "
Local cDesc2       := OemToAnsi(STR0002) //"de acuerdo con los par�metros informados por el usuario."
Local cDesc3       := OemToAnsi(STR0003) //"Comprobante de Egreso"
Local cPict        := ""
Local titulo       := OemToAnsi(STR0004) //"Comprobante de Egreso"
Private lEnd       := .F.
Private lAbortPrint:= .f.
Private limite     := 79
Private tamanho    := "P"
Private nomeprog   := "FINR880" // Coloque aqu� el nombre del programa para impresi�n en el encabezamiento
Private nTipo      := 18
Private aReturn    := {OemToAnsi(STR0005) /* "A Rayas" */, 1, ;
                       OemToAnsi(STR0006) /* "Administracion" */, 1, 2, 1, "", 1}
Private nLastKey   := 0
Private wnrel      := "SEF" // Coloque aqu� el nombre del archivo usado para impresi�n en disco
Private cString    := "SEF"
Private cPerg      := "FIN880"

//���������������������������������Ŀ
//�Verifica existencia de EF_COMPROB�
//�����������������������������������
SX3->(dbSetOrder(2))
If ! SX3->(dbSeek("EF_COMPROB"))
    // "EF_COMPROB no existe. Crear igual campo EF_COMPROV"
    MsgStop(OemToAnsi(STR0033))
    Return
EndIf

/*
+----------------------------------------------------------+
|Definicion de Variables ambientes                         |
+----------------------------------------------------------+
| Variables utilizadas para parametros                     |
|   mv_par01		Numero del cheque                      |
|   mv_par02		Banco								   |
|   mv_par03        Agencia                                |
|   mv_par04        Cuenta                                 |
|   mv_par05        Concepto							   |
+----------------------------------------------------------+
*/

Pergunte(cPerg,.f.)

//���������������������������������������������������������������������Ŀ
//� Monta la interfase estandar con el usuario...                           �
//�����������������������������������������������������������������������

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.T.,Tamanho,,.T.)

If ! nLastKey == 27
	SetDefault(aReturn,cString)
	If ! nLastKey == 27
		nTipo := If(aReturn[4]==1,15,18)
		//Processa({|| LeeImpuesto()},OemToAnsi(STR0007) /*"Leyendo Impuestos......"*/)
		RptStatus({|lEnd| RunReport(@lEnd, wnrel, cString)},Titulo)
	Endif
Endif
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Function  �RunReport �Autor  �Rubens Joao Pante   � Data �  08/10/00   ���
�������������������������������������������������������������������������͹��
���Desc.     �Imprime el informe de Comprobante de Egreso                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico para FIERA.COM COLOMBIA LTDA                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������

         1         2         3         4         5         6         7         8         9        10
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

No.Factura       VLR.FACTURA     VLR.IVA  VLR.RET.IVA  VLR.RET.ICA  VLR.RET.FTE    VLR.TIMBRE
9999999999999 99.999.999.999 999.999.999  999.999.999  999.999.999  999.999.999   999.999.999
*/
Static Function RunReport
	Local 	i			:= 0
	Local   cConcepto   := "", ;
	        aImpuesto   := {}, ;
	        lChqSuelto  := .F.,;
	        cComprob    := "", ;
	        cNomeBanco	:= "", ;
	        cProveedor  := "", ;
	        cNit		:= "", ;
	        cCED		:= "", ;
	        nValor      := 0,  ;
	        nTamImpues  := 0,  ;
	        nLin		:= 80
 	Local   nTotVal1   := nTotVal2 := nTotVal3 := nTotVal4 := nTotVal5 := 0

	//�������������������������������������������������������������������������������Ŀ
	//�Lee todos as facturas asociadas el cheque y soma los impuestos de cada factura �
	//���������������������������������������������������������������������������������
    SEF->(dbsetorder(4))
    If SEF->(dbseek(xfilial("SEF")+mv_par01+mv_par02+mv_par03+mv_par04))
        lChqSuelto := EMPTY(SEF->EF_TITULO)
 		cComprob   := SEF->EF_Comprob
 		cHist      := SEF->EF_Hist
 		cBenef     := SEF->EF_Benef

		SA6->(dbsetorder(1))
		SA6->(dbseek(xfilial("SA6")+SEF->EF_BANCO+SEF->EF_AGENCIA+SEF->EF_CONTA))
   		cNomeBanco := SA6->A6_NOME

   		SA2->(dbsetorder(1))
		SA2->(dbseek(xfilial("SA2")+SEF->EF_Fornece+SEF->EF_Loja))
		cProveedor:= SA2->A2_NOME
		cNit      := SA2->A2_CGC
   		cCED      := SA2->A2_PFISICA

		SF1->(dbsetorder(1))
		While xfilial("SEF")  == SEF->EF_FILIAL .AND.;
	    	  SEF->EF_NUM     == mv_par01        .AND.;
		      SEF->EF_BANCO   == mv_par02        .AND.;
		      SEF->EF_AGENCIA == mv_par03        .AND.;
	    	  SEF->EF_IMPRESS == "A"             .AND.;
	    	  !EOF()
   			If SF1->(dbseek(xfilial("SF1")+SEF->EF_TITULO+SEF->EF_PREFIXO+SEF->EF_FORNECE+SEF->EF_LOJA))
  	     		AADD(aImpuesto,{SF1->F1_DOC,SEF->EF_Valor,SF1->F1_VALIMP1,SF1->F1_VALIMP2,SF1->F1_VALIMP3,SF1->F1_VALIMP4,SF1->F1_VALIMP5})
			Else
  	     		AADD(aImpuesto,{SEF->EF_TITULO,SEF->EF_Valor,0,0,0,0,0})
   			EndIf
	    	SEF->(dbSkip())
		Enddo
		nValor := SEF->EF_Valor
    EndIf

	//��������������������������������Ŀ
	//�Imprime el Comprobante de Egreso�
	//����������������������������������
	nTamImpues := (Len(aImpuesto) + IF(lChqSuelto, 1, 0))
	If nTamImpues > 0 .OR. lChqSuelto // Ha algo a imprimir
	    SetRegua(nTamImpuesto)
		For i:= 1 to nTamImpues
		    IncRegua()
			If lAbortPrint
				@nLin,00 PSAY OemToAnsi(STR0018) //"*** CANCELADO PELO OPERADOR ***"
 				Exit
			Endif

			If nLin > 55 // Salto de P�gina. En este caso el impreso tiene 55 l�neas...
				If mv_par05 == 1
         			cConcepto := OemToAnsi(STR0019) //"PAGO FACTURAS"
      			ElseIf mv_par05 == 2
    	     		cConcepto := OemToAnsi(STR0020) //"ABONO"
	      		ElseIf mv_par05 == 3
         			cConcepto := OemToAnsi(STR0021) //"CANCELACION FACTURA"
      			EndIf

				@ 03,01 PSAY OemToAnsi(SM0->M0_NomeCom)
	      		@ 03,53 PSAY OemToAnsi(STR0023) //"Emision   :"
      			@ 03,72 PSAY dtoc(ddatabase)
				@ 04,01 PSAY OemToAnsi(STR0004) //"Comprobante de Egreso"
				@ 04,53 PSAY OemToAnsi(STR0022) // "No.       :"+
				@ 04,67 PSAY cComprob
      			@ 05,01 PSAY OemToAnsi(STR0024) //"Concepto  :"
      			@ 05,15 PSAY cConcepto
    	  		@ 05,53 PSAY OemToAnsi(STR0025) //"Cheque No.:"
	      		@ 05,65 PSAY PADL(ALLTRIM(mv_par01),15)
      			@ 06,53 PSAY OemToAnsi(STR0026) //"Valor     :"
      			@ 06,65 PSAY nValor Picture PesqPict("SEF","EF_VALOR",15,1)
				nLin := 7
				If ! lChqSuelto
					@ 08,01 PSAY OemToAnsi(STR0027) + cProveedor //"Proveedor: "
					If Empty(cNit)
						@ 08,61 PSAY OemToAnsi(STR0028) + PADL(ALLTRIM(cCed),14) //"CED: "
					Else
						@ 08,61 PSAY OemToAnsi(STR0029) + PADL(ALLTRIM(cNit),14) //"NIT: "
					EndIf
					@ 10,01 PSAY Replicate("-",limite)
    	    	    // "No.FACTURA      VLR.FACTURA     VLR.IVA  VLR.RET.IVA  VLR.RET.ICA  VLR.RET.FTE"
					@ 11,02 PSAY OemToAnsi(STR0030)
					@ 12,01 PSAY Replicate("-",limite)
					nLin += 6
				EndIf
				nValor := 0
			Endif

			If lChqSuelto
				@ nLin+1,01 PSAY STR0031 + cHist
				@ nLin+3,01 PSAY STR0032 + cBenef
				nLin += 5
			Else
				nValor  += aImpuesto[i,2]
				@ nLin,01 PSAY aImpuesto[i,1]
				@ nLin,15 PSAY aImpuesto[i,2]  Picture PesqPict("SEF","EF_VALOR",14,1)
				@ nLin,30 PSAY aImpuesto[i,3]  Picture PesqPict("SF1","F1_VALIMP1",11,1)
                                @ nLin,43 PSAY aImpuesto[i,4]  Picture PesqPict("SF1","F1_VALIMP2",11,1)
                                @ nLin,56 PSAY aImpuesto[i,5]  Picture PesqPict("SF1","F1_VALIMP3",11,1)
                                @ nLin,69 PSAY aImpuesto[i,6]  Picture PesqPict("SF1","F1_VALIMP4",11,1)
				nLin++ // Avanza la l�nea de impresi�n

				nTotVal1:= nTotVal1 + aImpuesto[i,2]
				nTotVal2:= nTotVal2 + aImpuesto[i,3]
				nTotVal3:= nTotVal3 + aImpuesto[i,4]
				nTotVal4:= nTotVal4 + aImpuesto[i,5]
				nTotVal5:= nTotVal5 + aImpuesto[i,6]
			EndIf
		Next

		If ! lChqSuelto
			@ nLin   ,01 PSAY Replicate("-",limite)
			@ nLin+01,14 PSAY nTotVal1 Picture PesqPict("SEF","EF_VALOR",14,1)
			@ nLin+01,29 PSAY nTotVal2 Picture PesqPict("SF1","F1_VALIMP1",12,1)
			@ nLin+01,42 PSAY nTotVal3 Picture PesqPict("SF1","F1_VALIMP1",12,1)
			@ nLin+01,55 PSAY nTotVal4 Picture PesqPict("SF1","F1_VALIMP1",12,1)
			@ nLin+01,68 PSAY nTotVal5 Picture PesqPict("SF1","F1_VALIMP1",12,1)
			@ nLin+02,01 PSAY Replicate("-",limite)
			nLin += 3
		EndIf

		@ nlin   ,01 PSAY OemToAnsi(STR0016) + cNomeBanco // "BANCO : "
		@ nlin+30,50 PSAY Replicate("-",30)
		@ nlin+31,50 PSAY OemToAnsi(STR0017) // "FIRMA RECIBO / IDENTIFICACION"
		@ 00,00   PSAY ""
	Endif

	SET DEVICE TO SCREEN
	If aReturn[5]==1
		dbCommitAll()
		SET PRINTER TO
		OurSpool(wnrel)
	Endif
	MS_FLUSH()
Return
