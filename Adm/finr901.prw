#INCLUDE "PROTHEUS.CH"
#INCLUDE "FINR901.CH"

#DEFINE MAX_LINHA 	   		65 //N�mero m�ximo de linhas do relat�rio.
#DEFINE PRIMEIRA_LINHA 		06 //Posi��o da primeira linha do relat�rio. 
#DEFINE PRIMEIRA_COLUNA 	03 //Posi��o da primeira coluna do relat�rio. 

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Relat�rio Anal�tico de Recebimento.   

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//-------------------------------------------------------------------------------------
Function finr901() 
	Local aArea	  		:= getArea()  
	Local cAlias        := 'SE1'   
	                                   
	Private cTitulo 	:= STR0001 //"Relat�rio Anal�tico de Recebimento"
	Private m_pag   	:= 01  
	Private aReturn 	:= {"Zebrado", 2, "Administracao", 2, 2, 1,"",1}
	Private cPergunta  	:= "FINR901"   
	Private cTamanho 	:= "G"  	
	Private cFinr901Ra	:= "" 
	Private cFinr901Ser	:= ""    
	
	//�����������������������������������Ŀ
	//�Exibe interface do tipo "Pergunte".�
	//�������������������������������������
	Pergunte(cPergunta,.T.)
	wnrel := SetPrint(cAlias, cPergunta, cPergunta, @cTitulo, "", "", "", .F., {}, .F., cTamanho, ,.F. , .F., , , .F., 'COM1')
		
	If nLastKey == 27
		Set Filter To
		Return
	EndIf     
	
	//�����������������������������������Ŀ
	//�Inicia o processamento do relat�rio�
	//�������������������������������������
 	SetDefault(aReturn,cAlias)
	RptStatus({|lEnd|  finr901Regra()  },cTitulo)
	
	RestArea(aArea)
Return                    

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Defini��o da l�gica do relat�rio. 

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//------------------------------------------------------------------------------------- 
Static Function finr901Regra()  	
	Local nColuna  	 	:= PRIMEIRA_COLUNA
	Local nLinha  	 	:= PRIMEIRA_LINHA  
	Local lNewRA 		:= .T.
	Local cCodServico	:= "" 
	Local cServico		:= ""
	Local cNumRA 		:= ""  
	Local cQuery 		:= ""   	     
	Local cAlias 		:= GetNextAlias()	  
	Local dBaixa 		 
	Local nTxMoeda 		:= 0
	   
	Local nDesc         := 0
	Local nMultaPaga    := 0
	Local nJurosPago    := 0
	Local nVlPago       := 0   
		
  	Local nTotReal	 	:= 0
	Local nTotBolsa 	:= 0
	Local nTotDesc  	:= 0
	Local nTotMulta 	:= 0
	Local nTotJuros 	:= 0
	Local nTotVlPag 	:= 0 
	
	Local nSerReal 		:= 0	
	Local nSerBolsa 	:= 0
	Local nSerDesc  	:= 0
	Local nSerMulta 	:= 0
	Local nSerJuros 	:= 0
	Local nSerVlPag 	:= 0        
				
	Private cAlunoDe 	:= mv_par01
	Private cAlunoAte	:= mv_par02    
	Private cServDe 	:= cBIStr(mv_par03)
	Private cServAte 	:= cBIStr(mv_par04)	  
	Private cVencDe		:= DToS(mv_par05)
	Private cVencAte	:= DToS(mv_par06)	 
	Private cEmissaoDe	:= DToS(mv_par07)
	Private cEmissaoAte	:= DToS(mv_par08)	 
	
	//���������������������������������������Ŀ
	//�Monta a consulta dos dados do rela�rio.�
	//�����������������������������������������
	cQuery := " SELECT "   
	cQuery += " SE1.E1_NUMRA NUMRA, ALUNO.ALU_NOME NOME, SE1.E1_SERVICO SERVICO, SE1.E1_NUM NUMERO, SE1.E1_PARCELA PARCELA, "
	cQuery += " SE1.E1_PREFIXO PREFIXO, SE1.E1_EMISSAO EMISSAO, SE1.E1_VENCTO VENCIMENTO, SE1.E1_VENCREA VENCREA, "
	cQuery += " SE1.E1_CLIENTE CLIENTE, SE1.E1_BAIXA BAIXA, SE1.E1_VLMULTA VLMULTA, SE1.E1_VLBOLSA BOLSA, SE1.E1_DECRESC DECRESC, "
	cQuery += " SE1.E1_ACRESC ACRESC, SE1.E1_VALOR VALOR, SE1.E1_SALDO SALDO, SE1.E1_VALJUR JUROS, SE1.E1_PORCJUR PORCJUR, "
	cQuery += " SE1.E1_MOEDA MOEDA, SE1.E1_TXMOEDA TXMOEDA "
	//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio
	/*BEGINDOC
	//�������������������������������������������Ŀ
	//�Inserido a coluna E1_TIPO para             �
	//�realizar a chamada na funcao fajuros, para �
	//�adequar � regra de c�lculo de juros        �
	//�do controle de lojas                       �
	//���������������������������������������������
	ENDDOC*/    
	cQuery += ", SE1.E1_TIPO TIPO "
	//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final
	cQuery += " FROM "
	cQuery += retsqlname('SE1') + " SE1 "  
	cQuery += " LEFT JOIN "
	cQuery += " INT_ALUNO ALUNO " 
	cQuery += " ON "
	cQuery += " ALUNO.ALU_NUMRA = SE1.E1_NUMRA "
	cQuery += " WHERE "
	cQuery += "	SE1.E1_FILIAL = '" 	+ xFilial('SE1') + "'"
	cQuery += " AND "	               
	cQuery += " SE1.E1_NUMRA BETWEEN '" 	+ cAlunoDe 		+ "' AND '" + cAlunoAte 	+ "'" 
	cQuery += " AND "	               
	cQuery += " SE1.E1_SERVICO BETWEEN '" 	+ cServDe 		+ "' AND '" + cServAte 		+ "'"
	cQuery += " AND "	               
	cQuery += " SE1.E1_VENCTO BETWEEN '" 	+ cVencDe 		+ "' AND '" + cVencAte 		+ "'"
	cQuery += " AND "	               
	cQuery += " SE1.E1_EMISSAO BETWEEN '" 	+ cEmissaoDe 	+ "' AND '" + cEmissaoAte 	+ "'"
	cQuery += " AND "
	cQuery += " SE1.E1_STATUS = 'B' "
	cQuery += " AND "
	cQuery += " SE1.D_E_L_E_T_ = ' ' "
	cQuery += " ORDER BY "
	cQuery += " SE1.E1_NUMRA ASC, SE1.E1_SERVICO ASC, SE1.E1_PARCELA ASC, SE1.E1_VENCTO ASC "   

	//����������������������������������������������������������Ŀ
	//�Cria workarea tempor�ria baseada no resultset da consulta.�
	//������������������������������������������������������������
	cQuery := ChangeQuery(cQuery)    
	iif(Select(cAlias)>0,(cAlias)->(dbCloseArea()),Nil)
	dbUseArea( .T., "TopConn", TCGenQry(,,cQuery), cAlias, .F., .F. )  

	//��������������������������������������Ŀ
	//�Imprime o primeiro cabecalho.		 �
	//����������������������������������������
	finr901Cabec()    
	nLinha ++
	@ nLinha, nColuna	PSAY __PrtThinLine() 
	nLinha ++            
	    
	//��������������������������������Ŀ
	//�Imprime os t�tulos selecionados.�
	//����������������������������������
	While (cAlias)->(!Eof())         
             	
		//�������������������������������
		//�Controla a quebra de p�gina. �
		//�������������������������������
		If nLinha > MAX_LINHA  		
			nLinha  := PRIMEIRA_LINHA
			finr901Cabec()
			nLinha ++
			@ nLinha, nColuna	PSAY __PrtThinLine() 
			nLinha ++		
		EndIf  
		 
	  	//������������������������������������Ŀ
		//�Controla a totaliza��o dos valores. �
		//��������������������������������������    		
        cNumRA 			:= (cAlias)->NUMRA   
        cCodServico		:= (cAlias)->SERVICO
        cServico 		:=  allTrim(ClsServName(SERVICO))
  
  		//���������������������
		//�Calcula os valores.�
		//���������������������
		nDesc         := 0
		nMultaPaga    := 0
		nJurosPago    := 0
		nVlPago       := 0
		              
		DBSelectArea("SE5")
		SE5->( DBSetOrder(7) )
		SE5->( DBSeek(xFilial("SE5")+ (cAlias)->PREFIXO + (cAlias)->NUMERO + (cAlias)->PARCELA) )
		
		While !Eof() .and. xFilial() + (cAlias)->PREFIXO + (cAlias)->NUMERO + (cAlias)->PARCELA == SE5->E5_FILIAL + SE5->E5_PREFIXO + SE5->E5_NUMERO + SE5->E5_PARCELA
			
			If SE5->E5_CLIENTE == (cAlias)->CLIENTE .And. SE5->E5_SITUACA <> "C"  			
				
				If SE5->E5_TIPODOC == "VL" .or. SE5->E5_TIPODOC == "CP" .or. SE5->E5_TIPODOC == "BA"  
					nDesc            += SE5->E5_VLDESCO 
					nMultaPaga       += SE5->E5_VLMULTA
					nJurosPago       += SE5->E5_VLJUROS
					nVlPago          += SE5->E5_VALOR 
				
				ElseIf SE5->E5_TIPODOC == "ES"
					nDesc            := nDesc		-	SE5->E5_VLDESCO
					nMultaPaga       := nMultaPaga	-	SE5->E5_VLMULTA
					nJurosPago       := nJurosPago	-	SE5->E5_VLJUROS
					nVlPago          := nVlPago		-	SE5->E5_VALOR
				EndIf
			EndIf
			
			SE5->(dbskip())
		End
 
   		dBaixa 		:= If(Empty((cAlias)->BAIXA), dDataBase, SToD((cAlias)->BAIXA) )
   		nTxMoeda 	:= If(cPaisLoc=="BRA",(cAlias)->TXMOEDA,0) 
 	  	//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Inicio
 		nJuros 		:= faJuros( (cAlias)->VALOR, (cAlias)->SALDO, SToD((cAlias)->VENCIMENTO), (cAlias)->JUROS, (cAlias)->PORCJUR, (cAlias)->MOEDA, SToD((cAlias)->EMISSAO), dBaixa, nTxMoeda, SToD((cAlias)->BAIXA), , ,(cAlias)->PREFIXO, (cAlias)->NUMERO, (cAlias)->PARCELA, (cAlias)->TIPO)
		//Calculo de Juros e Multas: SIGALOJA x SIGAFIN  - Final
		nJuros 		:= iif(nJurosPago>0,nJurosPago,nJuros)
		
		If (dBaixa > SToD((cAlias)->VENCREA))
			nMulta := if( !Empty(( cAlias)->BAIXA ) .and. !Empty(nMultaPaga), nMultaPaga, (cAlias)->VLMULTA )
		else
			nMulta := nMultaPaga
		endif
		 
		//����������������������Ŀ
		//�Totaliza por servi�o. �
		//������������������������   
		nSerReal 	+= (cAlias)->VALOR 	  
		nSerBolsa 	+= (cAlias)->BOLSA 			
		nSerDesc  	+= nDesc 	+ (cAlias)->DECRESC       
		nSerMulta 	+= nMulta                        
		nSerJuros 	+= nJuros 	+ (cAlias)->ACRESC    
		nSerVlPag 	+= nVlPago   
		 
		//��������������������Ŀ
		//�Totaliza por aluno. �
		//����������������������  
		nTotReal 	+= (cAlias)->VALOR 	   
		nTotBolsa 	+= (cAlias)->BOLSA 			
		nTotDesc  	+= nDesc 	+ (cAlias)->DECRESC       
		nTotMulta 	+= nMulta                         
		nTotJuros 	+= nJuros 	+ (cAlias)->ACRESC    
		nTotVlPag 	+= nVlPago                       

        //��������������������������������Ŀ
		//�Insere os valores no relat�rio. �
		//����������������������������������		
		If ( lNewRA )                
	  		@ nLinha, nColuna    		PSAY allTrim((cAlias)->NUMRA)  
			@ nLinha, nColuna + 20  	PSAY SubStr(allTrim((cAlias)->NOME), 1, 40)  
			@ nLinha, nColuna + 60 		PSAY SubStr(cServico, 1, 25)  
			
			lNewRA := .F.   
		EndIf   
		  
		@ nLinha, nColuna + 85  	PSAY allTrim((cAlias)->NUMERO) 
		@ nLinha, nColuna + 100  	PSAY allTrim((cAlias)->PARCELA)     		
		@ nLinha, nColuna + 110    	PSAY DToC(SToD((cAlias)->EMISSAO))  		 
		@ nLinha, nColuna + 125   	PSAY DToC(SToD((cAlias)->VENCIMENTO))
		@ nLinha, nColuna + 140    	PSAY finr901Format((cAlias)->VALOR) 
		@ nLinha, nColuna + 154    	PSAY finr901Format((cAlias)->BOLSA) 
		@ nLinha, nColuna + 166    	PSAY finr901Format(nDesc)	
		@ nLinha, nColuna + 178    	PSAY finr901Format(nMulta) 			
		@ nLinha, nColuna + 190   	PSAY finr901Format(nJuros)  				
		@ nLinha, nColuna + 202		PSAY finr901Format(nVlPago) 
	                                                                    	
	    nLinha++    

		(cAlias)->(dbSkip())	
			 
		//���������������������������������������Ŀ
		//�Insere a linha de subtotal por servi�o.�
		//�����������������������������������������
		If !(cCodServico == (cAlias)->SERVICO) .Or. !(cNumRA == (cAlias)->NUMRA)				
			nLinha ++ 
			
			@ nLinha, nColuna + 60   	PSAY SubStr(STR0002 + cServico, 1, 25) //"Total"
			@ nLinha, nColuna + 140		PSAY finr901Format(nSerReal) 
			@ nLinha, nColuna + 154   	PSAY finr901Format(nSerBolsa)   					
			@ nLinha, nColuna + 166    	PSAY finr901Format(nSerDesc) 	
			@ nLinha, nColuna + 178    	PSAY finr901Format(nSerMulta) 				
			@ nLinha, nColuna + 190   	PSAY finr901Format(nSerJuros) 			
			@ nLinha, nColuna + 202		PSAY finr901Format(nSerVlPag) 
			
			nLinha ++      
			
			nSerReal	:= 0
			nSerBolsa 	:= 0
			nSerDesc  	:= 0
			nSerMulta 	:= 0
			nSerJuros 	:= 0
			nSerVlPag 	:= 0
		EndIf		 
  
		//������������������������������������������Ŀ
		//�Insere a linha de totaliza��o dos valores.�
		//��������������������������������������������
		If !(cNumRA == (cAlias)->NUMRA)				
			lNewRA := .T.
						
			nLinha ++ 
			
			@ nLinha, nColuna + 60   	PSAY allTrim(STR0003) //"Total do Aluno"
			@ nLinha, nColuna + 140		PSAY finr901Format(nTotReal) 
			@ nLinha, nColuna + 154    	PSAY finr901Format(nTotBolsa)   					
			@ nLinha, nColuna + 166    	PSAY finr901Format(nTotDesc)  	
			@ nLinha, nColuna + 178    	PSAY finr901Format(nTotMulta)  				
			@ nLinha, nColuna + 190   	PSAY finr901Format(nTotJuros) 				
			@ nLinha, nColuna + 202		PSAY finr901Format(nTotVlPag) 	
					
			nLinha ++
		   
			@ nLinha, nColuna	PSAY __PrtThinLine()   
				
			nLinha ++ 

			nTotReal 	:= 0			
			nTotBolsa 	:= 0
			nTotDesc  	:= 0
			nTotMulta 	:= 0
			nTotJuros 	:= 0
			nTotVlPag 	:= 0 
		EndIf	 
	EndDo      
	
	//���������������������������������������������������������Ŀ
	//�Exibe o relatorio em tela caso a impress�o seja em disco.�
	//�����������������������������������������������������������
	If aReturn[5] = 1
	   	Set Device to Screen
	   	Set Printer To 
	   	dbCommitAll()
	   	OurSpool(wnrel)
	Endif
	
	Ms_Flush() 	
Return 
     
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Defini��o do cabe�alho de colunas do relt�rio. 

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//-------------------------------------------------------------------------------------    
Static Function finr901Cabec()  
	Local nLinha 	:= PRIMEIRA_LINHA
	Local nColuna 	:= PRIMEIRA_COLUNA	 
	
	//��������������������������Ŀ
	//�Monta o cabe�alho padr�o. �
	//����������������������������
	Cabec(cTitulo, '', '', cPergunta, cTamanho, 15)  
	 
	//�����������������������������Ŀ
	//�Monta o cabe�alho de coluna. �
	//�������������������������������
	@ nLinha, nColuna    		PSAY alltrim("RA")   			
	@ nLinha, nColuna + 20  	PSAY alltrim(STR0004) //"Nome"     		40 Posi��es   
	@ nLinha, nColuna + 60  	PSAY alltrim(STR0005) //"Servi�o"   	25 Posi��es 
	@ nLinha, nColuna + 85 		PSAY alltrim(STR0006) //"T�tulo"    	15 Posi��es 
	@ nLinha, nColuna + 100 	PSAY alltrim(STR0007) //"Parcela"   	10 Posi��es 
	@ nLinha, nColuna + 110    	PSAY alltrim(STR0008) //"Emiss�o"		15 Posi��es
	@ nLinha, nColuna + 125    	PSAY alltrim(STR0009) //"Vencimento" 	15 Posi��es
	@ nLinha, nColuna + 143   	PSAY alltrim(STR0010) //"Valor"       	12 Posi��es
	@ nLinha, nColuna + 157   	PSAY alltrim(STR0011) //"Bolsa"       	12 Posi��es
	@ nLinha, nColuna + 169   	PSAY alltrim(STR0012) //"Desconto" 		12 Posi��es
	@ nLinha, nColuna + 181   	PSAY alltrim(STR0013) //"Multa"	    	12 Posi��es
	@ nLinha, nColuna + 193   	PSAY alltrim(STR0014) //"Juros" 	   	12 Posi��es 
	@ nLinha, nColuna + 205		PSAY alltrim(STR0015) //"Valor Pago" 	12 Posi��es
Return

//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Consulta padr�o de alunos (INT_ALUNO)

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//------------------------------------------------------------------------------------- 
Function finr901Alu()
	Local cQuery 	:= "" 
	Local aIndices 	:= {}
	Local aHeader 	:= {}

	//���������������������������������Ŀ
	//�Monta a query da consulta padrao	�
	//�����������������������������������
	cQuery := " SELECT " 
	cQuery += " ALUNO.ALU_NUMRA RA, ALUNO.ALU_NOME NOME "
	cQuery += " FROM "
	cQuery += " INT_ALUNO ALUNO "   
	    
	//����������������������������Ŀ
	//�Monta os indices da consulta�
	//������������������������������
	aAdd(aIndices,{"RA"		,"RA"}) 	//"RA"
	aAdd(aIndices,{"NOME"	,STR0004}) 	//"Nome"   
	
	//�����������������������������
	//�Monta a aHeader da consulta�
	//�����������������������������
	Aadd(aHeader,{"RA"		,"RA"		,"@!"		,20	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"RA"
	Aadd(aHeader,{STR0004	,"NOME"		,"@!"		,45	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Nome"   
	
	//�����������������������Ŀ
	//�Monta a consulta padrao�
	//�������������������������
	oConsulta:=ClsIntSxb():New()
	oConsulta:cTitle 		:= STR0017 		//"Alunos
	oConsulta:cQuery 		:= cQuery		//Query dos dados a serem apresentados
	oConsulta:aIndex 		:= aIndices		//Indices disponiveis (deve usar os mesmos campos da query)
	oConsulta:aHeader 		:= aHeader      //Header da grid presente consulta (deve usar os mesmos campos da query)
	oConsulta:aReturn		:= {"RA"}  		//Campos de retorno da consulta (deve usar os mesmos campos da query)
	oConsulta:lDataBaseRM 	:= .F.			//Indica se a query eh executada na base do protheus ou da RM
	oConsulta:Show()       
	                           
	//����������������Ŀ
	//�Coleta o retorno�
	//������������������
	If Len(oConsulta:aRetSXB) > 0
		cFinr901Ra := oConsulta:aRetSXB[1]
	EndIf	
Return .t. 
      
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Consulta padr�o de servi�os (SSERVICO)

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//------------------------------------------------------------------------------------- 
Function finr901Ser()
	Local cQuery 	:= "" 
	Local aIndices 	:= {}
	Local aHeader 	:= {}
	Local cEmp		:= substr(SM0->M0_CODIGO,2,1) 
	 
	//���������������������������������Ŀ
	//�Monta a query da consulta padrao	�
	//�����������������������������������
	cQuery := " SELECT " 
	cQuery += " SERVICO.CODSERVICO CODIGO, SERVICO.NOME NOME " 
	cQuery += " FROM " 
	cQuery += " SSERVICO SERVICO "
	cQuery += " WHERE "	
	cQuery += " SERVICO.CODCOLIGADA = '" + cEmp + "'"
		   
	//����������������������������Ŀ
	//�Monta os indices da consulta�
	//������������������������������
	aAdd(aIndices,{"CODIGO"		, STR0016}) //"C�digo"
	aAdd(aIndices,{"NOME"		, STR0004}) //"Nome"   
	
	//�����������������������������
	//�Monta a aHeader da consulta�
	//�����������������������������
	Aadd(aHeader,{STR0016		,"CODIGO"		,"@!"		,10	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"C�digo"
	Aadd(aHeader,{STR0004		,"NOME"			,"@!"		,45	,0	,"AlwaysTrue()"	,"���������������","C","","V"}) //"Nome"   
	
	//�����������������������Ŀ
	//�Monta a consulta padrao�
	//�������������������������
	oConsulta:=ClsIntSxb():New()
	oConsulta:cTitle 		:= STR0018		//"Servicos"
	oConsulta:cQuery 		:= cQuery		//Query dos dados a serem apresentados
	oConsulta:aIndex 		:= aIndices		//Indices disponiveis (deve usar os mesmos campos da query)
	oConsulta:aHeader 		:= aHeader      //Header da grid presente consulta (deve usar os mesmos campos da query)
	oConsulta:aReturn		:= {"CODIGO"}  	//Campos de retorno da consulta (deve usar os mesmos campos da query)
	oConsulta:lDataBaseRM 	:= .T.			//Indica se a query eh executada na base do protheus ou da RM
	oConsulta:Show()    
	                              
	//����������������Ŀ
	//�Coleta o retorno�
	//������������������
	If Len(oConsulta:aRetSXB) > 0
		cFinr901Ser := oConsulta:aRetSXB[1]
	EndIf	
Return .t. 
    
//-------------------------------------------------------------------------------------
/*/{Protheus.doc} FINR901
Formata e alinha os valores num�ricos.

@author    3174 - Valdiney V GOMES
@version   P10
@since     15/12/2010
/*/
//-------------------------------------------------------------------------------------           
Static Function finr901Format(cValor)     
	Local cRet 			:= ""
	Local cFormatado 	:= ""
	
	cFormatado 	:=  alltrim(Transform(cValor	,"@E 999,999.99"))                            
    cRet 		:= Replicate(" ",10 - len(cFormatado)) + cFormatado
Return cRet
