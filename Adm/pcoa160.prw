#INCLUDE "PCOA160.ch"
#INCLUDE "PROTHEUS.CH"

/*
_F_U_N_C_苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪哪勘�
北矲UNCAO    � PCOA160  � AUTOR � Paulo Carnelossi      � DATA � 12/11/2004 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪哪幢�
北矰ESCRICAO � Programa para cadastro configuracoes visao gerencial PCO     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� USO      � SIGAPCO                                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砡DOCUMEN_ � PCOA160                                                      潮�
北砡DESCRI_  � Programa para cadastro de configuracoes de visoes gerenciais 潮�
北砡DESCRI_  � utilizado no modulo SIGAPCO                                  潮�
北砡FUNC_    � Esta funcao podera ser utilizada com a sua chamada normal    潮�
北�          � partir do Menu ou a partir de uma funcao pulando assim o     潮�
北�          � browse principal e executando a chamada direta da rotina     潮�
北�          � selecionada.                                                 潮�
北�          � Exemplo: PCOA160(2) - Executa a chamada da funcao de visua-  潮�
北�          �                        zacao da rotina.                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砡PARAMETR_� ExpN1 : Chamada direta sem passar pela mBrowse               潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function PCOA160(nCallOpcx)
Private cCadastro	:= STR0001 //"Cadastro de Processos de Sistema"
Private aRotina := MenuDef()				

If AMIIn(57) // AMIIn do modulo SIGAPCO ( 57 )	
	A160Popula()

	If nCallOpcx <> Nil
		A160DLG("AKL",AKL->(RecNo()),nCallOpcx)
	Else
		mBrowse(6,1,22,75,"AKL")
	EndIf
EndIf

Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  矨160DLG   篈utor  砅aulo Carnelossi    � Data �  12/11/04  罕�
北掏屯屯屯屯拓屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     � Tratamento da tela de Inclusao/Alteracao/Exclusao/Visuali- 罕�
北�          � zacao                                                      罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP8                                                        罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function A160DLG(cAlias,nRecnoAKL,nCallOpcx)
Local oDlg
Local lCancel  := .F.
Local aButtons
Local aUsButtons := {}
Local oEnchAKL
Local oFolder

Local aHeadAKM
Local aColsAKM
Local nLenAKM   := 0 // Numero de campos em uso no AKM
Local nLinAKM   := 0 // Linha atual do acols
Local aRecAKM   := {} // Recnos dos registros
Local nGetD


Local oBarAKM
Local oBtnAKM

Private oGdAKM
Private INCLUI  := (nCallOpcx = 3)

If !AMIIn(57) // AMIIn do modulo SIGAPCO ( 57 )
	Return .F.
EndIf

//Bot鉶 habilitado somente na inclus鉶
If nCallOpcx = 3
	aButtons := { {"BMPCPO",{|| a160Suges(nCallOpcx, aHeadAKM, oGdAKM ) },"Preencher Campos Conforme Cubo","Cpo/Cubo"} }
EndIf

If nCallOpcx != 3 .And. ValType(nRecnoAKL) == "N" .And. nRecnoAKL > 0
	DbSelectArea(cAlias)
	DbGoto(nRecnoAKL)
	If EOF() .Or. BOF()
		HELP("  ",1,"PCOREGINV",,AllTrim(Str(nRecnoAKL)))
		Return .F.
	EndIf
EndIf


If (nCallOpcx == 4 .Or. nCallOpcx == 5) .And. AKL->AKL_SYSTEM == "1" //Tab.Sistema
	MsgAlert(STR0007) //"Configuracao de Sistema - Nao deve ser alterado ou excluido."
	Return .F.
EndIf


//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Adiciona botoes do usuario na EnchoiceBar                              �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If ExistBlock( "PCOA1602" )
	//P_E谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//P_E� Ponto de entrada utilizado para inclusao de botoes de usuarios         �
	//P_E� na tela de processos                                                   �
	//P_E� Parametros : Nenhum                                                    �
	//P_E� Retorno    : Array contendo as rotinas a serem adicionados na enchoice �
	//P_E�  Ex. :  User Function PCOA1602                                         �
	//P_E�         Return { 'PEDIDO', {|| MyFun() },"Exemplo de Botao" }          �
	//P_E滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁

	If ValType( aUsButtons := ExecBlock( "PCOA1602", .F., .F. ) ) == "A"
		AEval( aUsButtons, { |x| AAdd( aButtons, x ) } )
	EndIf
EndIf

DEFINE MSDIALOG oDlg TITLE STR0008 FROM 0,0 TO 480,640 PIXEL //"Cadastro de Configuracoes de Visao Gerencial - PCO"
oDlg:lMaximized := .T.

// Carrega dados do AKL para memoria
RegToMemory("AKL",INCLUI)

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Enchoice com os dados do Processo                                      �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
oEnchAKL := MSMGet():New('AKL',,nCallOpcx,,,,,{0,0,(oDlg:nClientHeight/6)-12,(oDlg:nClientWidth/2)},,,,,,oDlg,,,,,,.T.,,,)
oEnchAKL:oBox:Align := CONTROL_ALIGN_TOP

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Folder com os Pontos de Lancamento e Pontos de Bloqueio                �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
oFolder  := TFolder():New(oDlg:nHeight/6,0,{STR0009},{''},oDlg,1,,,.T.,,(oDlg:nWidth/2),oDlg:nHeight/3,) //"Parametros da Configuracao"
oFolder:Align := CONTROL_ALIGN_ALLCLIENT

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Montagem do aHeader do AKM                                             �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
aHeadAKM := GetaHeader("AKM")
nLenAKM  := Len(aHeadAKM) + 1

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Montagem do aCols do AKM                                               �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
aColsAKM := {}
DbSelectArea("AKM")
DbSetOrder(1)
DbSeek(xFilial()+AKL->AKL_CONFIG)
If nCallOpcx != 3
	While !Eof() .And. AKM->AKM_FILIAL + AKM->AKM_CONFIG == xFilial() + AKL->AKL_CONFIG
		AAdd(aColsAKM,Array( nLenAKM ))
		nLinAKM++
		// Varre o aHeader para preencher o acols
		AEval(aHeadAKM, {|x,y| aColsAKM[nLinAKM][y] := IIf(x[10] == "V", CriaVar(AllTrim(x[2])), FieldGet(FieldPos(x[2])) ) })
	
		// Deleted
		aColsAKM[nLinAKM][nLenAKM] := .F.
		
		// Adiciona o Recno no aRec
		AAdd( aRecAKM, AKM->( Recno() ) )
		
		AKM->(DbSkip())
	EndDo
EndIf

// Verifica se n鉶 foi criada nenhuma linha para o aCols
If Len(aColsAKM) = 0
	AAdd(aColsAKM,Array( nLenAKM ))
	nLinAKM++

	// Varre o aHeader para preencher o acols
	AEval(aHeadAKM, {|x,y| aColsAKM[nLinAKM][y] := IIf(Upper(AllTrim(x[2])) == "AKM_ITEM", StrZero(1,Len(AKM->AKM_ITEM)),CriaVar(AllTrim(x[2])) ) })

	// Deleted
	aColsAKM[nLinAKM][nLenAKM] := .F.
EndIf

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� GetDados com os Pontos de Lancamento          �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
If nCallOpcx = 3 .Or. nCallOpcx = 4
	nGetD:= GD_INSERT+GD_UPDATE+GD_DELETE
Else
	nGetD := 0
EndIf

oGdAKM:= MsNewGetDados():New(0,0,100,100,nGetd,,,"+AKM_ITEM",,,9999,,,,oFolder:aDialogs[1],aHeadAKM,aColsAKM)
oGdAKM:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
oGdAKM:CARGO := AClone(aRecAKM)

// Quando nao for MDI chama centralizada.
If SetMDIChild()
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| If(A160Ok(nCallOpcx,oEnchAKL,oGdAKM),(A160Grv(nCallOpcx,oEnchAKL,oGdAKM),oDlg:End()),) },{|| lCancel := .T., oDlg:End() },,aButtons)
Else
	ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg,{|| If(A160Ok(nCallOpcx,oEnchAKL,oGdAKM),(A160Grv(nCallOpcx,oEnchAKL,oGdAKM),oDlg:End()),) },{|| lCancel := .T., oDlg:End() },,aButtons)
EndIf

Return !lCancel

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  � A160Ok   篈utor  砅aulo Carnelossi    � Data �  12/11/04   罕�
北掏屯屯屯屯拓屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     � Funcao do botao OK da enchoice bar, valida e faz o         罕�
北�          � tratamento adequado das informacoes.                       罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP8                                                        罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function A160Ok(nCallOpcx,oEnchAKL,oGdAKM)
Local lRet := .F.

If nCallOpcx = 1 .Or. nCallOpcx = 2 // Pesquisar e Visualizar
	lRet := .T.
EndIf

If !A160Vld(nCallOpcx,oEnchAKL,oGdAKM)
	lRet := .F. 
Else
	lRet := .T. 
EndIf

Return(lRet)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  � A160Grv  篈utor  砅aulo Carnelossi    � Data �  12/11/04   罕�
北掏屯屯屯屯拓屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     � Funcao de gravacao das configuracoes de visao gerencial ao 罕�
北�          � pressionar o botao OK da enchoice bar.                     罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function A160Grv(nCallOpcx,oEnchAKL,oGdAKM)
Local nI
Local cCampo

If nCallOpcx = 3 // Inclusao

	// Grava Processo
	Reclock("AKL",.T.)
	For nI := 1 To FCount()
		cCampo := Upper(AllTrim(FieldName(nI)))
		If cCampo == "AKL_FILIAL"
			FieldPut(nI,xFilial())
		Else
			FieldPut(nI, &("M->" + cCampo))
		EndIf
	Next nI
	MsUnlock()
	
	// Grava Pontos de Lancamento
	For nI := 1 To Len(oGdAKM:aCols)
		If oGdAKM:aCols[nI][Len(oGdAKM:aCols[nI])] // Verifica se a linha esta deletada
			Loop
		Else
			Reclock("AKM",.T.)
		EndIf

		// Varre o aHeader e grava com base no acols
		AEval(oGdAKM:aHeader,{|x,y| FieldPut(FieldPos(x[2]), oGdAKM:aCols[nI][y] ) })

		Replace AKM_FILIAL With xFilial()
		Replace AKM_CONFIG With AKL->AKL_CONFIG

		MsUnlock()

	Next nI
	
ElseIf nCallOpcx = 4 // Alteracao

	// Grava Processo
	Reclock("AKL",.F.)
	For nI := 1 To FCount()
		cCampo := Upper(AllTrim(FieldName(nI)))
		If cCampo == "AKL_FILIAL"
			FieldPut(nI,xFilial())
		Else
			FieldPut(nI, &("M->" + cCampo))
		EndIf
	Next nI
	MsUnlock()

	// Grava Pontos de Lancamento
	For nI := 1 To Len(oGdAKM:aCols)
		If nI <= Len(oGdAKM:Cargo) .And. oGdAKM:Cargo[nI] > 0
			AKM->(DbGoto(oGdAKM:Cargo[nI]))
			Reclock("AKM",.F.)
		Else
			If oGdAKM:aCols[nI][Len(oGdAKM:aCols[nI])] // Verifica se a linha esta deletada
				Loop
			Else
				Reclock("AKM",.T.)
			EndIf
		EndIf
	
		If oGdAKM:aCols[nI][Len(oGdAKM:aCols[nI])] // Verifica se a linha esta deletada
			AKM->(DbDelete())
		Else
			// Varre o aHeader e grava com base no acols
			AEval(oGdAKM:aHeader,{|x,y| FieldPut( FieldPos(x[2]) , oGdAKM:aCols[nI][y] ) })
			Replace AKM_FILIAL With xFilial()
			Replace AKM_CONFIG With AKL->AKL_CONFIG

		EndIf

		MsUnlock()
	Next nI

ElseIf nCallOpcx = 5 // Exclusao

	// Exclui Pontos de Lancamento
	For nI := 1 To Len(oGdAKM:aCols)
		If nI <= Len(oGdAKM:Cargo) .And. oGdAKM:Cargo[nI] > 0
			AKM->(DbGoto(oGdAKM:Cargo[nI]))
			Reclock("AKM",.F.)
			AKM->(DbDelete())
			MsUnLock()
		EndIf		
	Next nI

	// Exclui Processo
	Reclock("AKL",.F.)
	AKL->(DbDelete())
	MsUnLock()

EndIf

Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  � A160Vld  篈utor  砅aulo Carnelossi    � Data �  12/11/04   罕�
北掏屯屯屯屯拓屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     � Funcao de validacao dos campos.                            罕�
北�          �                                                            罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP8                                                        罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function A160Vld(nCallOpcx,oEnchAKL,oGdAKM)
Local nI
If (nCallOpcx = 3 .Or. nCallOpcx = 4) .And. ;
	!Obrigatorio(oEnchAKL:aGets,oEnchAKL:aTela)
	Return .F.
EndIf

For nI := 1 To Len(oGdAKM:aCols)
	// Busca por campos obrigatorios que nao estjam preenchidos
	nPosField := AScanx(oGdAKM:aHeader,{|x,y| x[17] .And. Empty(oGdAKM:aCols[nI][y]) })
	If nPosField > 0
		SX2->(dbSetOrder(1))
		SX2->(MsSeek("AKD"))
		HELP("  ",1,"OBRIGAT2",,X2NOME()+CHR(10)+CHR(13)+STR0010 + AllTrim(oGdAKM:aHeader[nPosField][1]) + STR0011+Str(nI,3,0),3,1) //"Campo: "###"Linha: " //"Campo: "###"Linha: "
		Return .F.
	EndIf
Next nI

Return .T.


Function PcoVazio(cCampo, oGetDados, nLin)
Local nCampo
Local lRet := .F. 

DEFAULT oGetDados := oGdAKM
DEFAULT nLin := oGdAKM:nAt

nCampo := aScan(oGetDados:aHeader,{|x| AllTrim(x[2]) == cCampo})

If nCampo > 0
	lRet := Empty(oGdAKM:Acols[nLin, nCampo])
EndIf

Return(lRet)

Function PcoX3Filtro(oGetDados, nLin)
Local cCampo, cCpoAux := "", nCampo
Local cAliasRet := ""

DEFAULT oGetDados := oGdAKM
DEFAULT nLin := oGdAKM:nAt

cCampo := ReadVar()
cCampo := StrTran(cCampo, "M->","")

If cCampo == "AKM_CPOREF"
   cCpoAux := "AKM_ENTSIS"
ElseIf cCampo == "AKM_CPOFIL"
   cCpoAux := "AKM_ENTFIL"
EndIf

If !Empty(cCpoAux)   

	nCampo := aScan(oGetDados:aHeader,{|x| AllTrim(x[2]) == cCpoAux})

	If nCampo > 0
		cAliasRet := oGdAKM:Acols[nLin, nCampo]
	EndIf

EndIf

Return(Padr(cAliasRet,3))

Function A160ValCpo()
Local lRet := .F.
Local cContCpo

cContCpo := &(ReadVar())

SX3->(dbSetOrder(2))
lRet := SX3->(dbSeek(cContCpo))
SX3->(dbSetOrder(1))

Return(lRet)

Function A160Popula()
Local nX, nY, nQt, cItem
Local aAuxAKM := {}, aTabAKM := {} 
Static nQtdEntid  
             
// Verifica a quantidade de entidades contabeis
If nQtdEntid == NIL
	nQtdEntid := CtbQtdEntd() //sao 4 entidades padroes -> conta /centro custo /item contabil/ classe de valor
EndIf 

dbSelectArea("AKM")
dbSetOrder(1)

dbSelectArea("AKL")
dbSetOrder(1)


If !dbSeek(xFilial("AKL")+"001") .And. ;
	AKM->(! dbSeek(xFilial("AKM")+"001"))
	RecLock("AKL", .T.)
	AKL_FILIAL := xFilial("AKL")
	AKL_CONFIG := "001"
	AKL_DESCRI := STR0012 //"VISAO ORCAMENTARIA"
	AKL_SYSTEM := "1"
	AKL_UTILIZ := "1"
	MsUnLock()
	
	aAuxAKM := {}
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '01'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0013}) //'Planilha Orct.      '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK1  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK1_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK1  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK1_CODIGO'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK1  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '1'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"                            "'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {}
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '02'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0019 }) //'Versao'
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AKE  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AKE_REVISA'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_VERSAO'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AKE1'})
	aAdd(aAuxAKM, {'AKM_TIPO' , '1'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"                            "'})
	aAdd(aTabAKM, aAuxAKM)

	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '03'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0014}) //'Conta Orcamentaria  '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK5  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK5_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_CO    '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK5  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '04'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0015}) //'Classe Orcamentaria '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK6  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK6_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_CLASSE'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK6  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '05'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0020 })//'Operacao'
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AKF  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AKF_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_OPER'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AKF  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)

	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '06'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0016}) //'Centro Custo        '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTT  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTT_CUSTO '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_CC    '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTT  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '07'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0017}) //'Item Contabil(CTB)  '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTD  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTD_ITEM  '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_ITCTB '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTD  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '08'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0018}) //'Classe Valor(CTB)   '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTH  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTH_CLVL  '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_CLVLR '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTH  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	//INSERIR UNIDADE ORCAMENTARIA E TODAS AS NOVAS ENTIDADES CRIADAS PELO WIZARD  
	// Verifica a existencia da Unidade Orcamentaria na base do cliente
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '09'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0026}) //"Unidade Orcamentaria"
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AMF  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AMF_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_UNIORC'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AMF  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM) 
		
	If (nQtdEntid > 4)   
		cItem := '09'
		// Inclui novas entidades
		For nQt := 5 To nQtdEntid 
			dbSelectArea("CT0")
			dbSetOrder(1)
			
			dbSeek(xFilial("CT0")+STRZERO(nQt,2)) 
		
			cItem := Soma1(cItem)
			aAuxAKM := {} 
			aAdd(aAuxAKM, {'AKM_CONFIG' , '001'})
			aAdd(aAuxAKM, {'AKM_ITEM' , cItem})
			aAdd(aAuxAKM, {'AKM_TITULO' , CT0->CT0_DESC}) 
			aAdd(aAuxAKM, {'AKM_ENTSIS' , CT0->CT0_ALIAS})
			aAdd(aAuxAKM, {'AKM_CPOREF' , CT0->CT0_CPOCHV})
			aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AK2  '})
			aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AK2_ENT'+STRZERO(nQt,2)})
			aAdd(aAuxAKM, {'AKM_CONPAD' , CT0->CT0_F3ENTI})
			aAdd(aAuxAKM, {'AKM_TIPO' , '2'}) 
			aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
			aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
			
			aAdd(aTabAKM, aAuxAKM) 
		Next
	EndIf   
	
	dbSelectArea("AKM")
	
	For nX := 1 TO Len(aTabAKM)
		RecLock("AKM", .T.)
		AKM->AKM_FILIAL := xFilial("AKM")
		For nY := 1 TO Len(aTabAKM[nX])
			If (nPos := FieldPos(aTabAKM[nX][nY][1])) > 0
				FieldPut(nPos, aTabAKM[nX][nY][2])
			EndIf
		Next
		MsUnLock()	
	Next

EndIf

aAuxAKM := {}
aTabAKM := {} 

If !dbSeek(xFilial("AKL")+"002") .And. ;
	AKM->(! dbSeek(xFilial("AKM")+"002"))
	RecLock("AKL", .T.)
	AKL_FILIAL := xFilial("AKL")
	AKL_CONFIG := "002"
	AKL_DESCRI := STR0021//"CONF.RESERVADA P/ INCL.MOVTO-SIMULACAO"
	AKL_SYSTEM := "1"
	AKL_UTILIZ := "2"

	MsUnLock()
	
	aAuxAKM := {}
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '01'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0013}) //'Planilha Orct.      '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK1  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK1_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_CODPLA'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK1  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '1'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"                            "'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {}
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '02'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0019 }) //'Versao'
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AKE  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AKE_REVISA'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_VERSAO'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AKE1'})
	aAdd(aAuxAKM, {'AKM_TIPO' , '1'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"                            "'})
	aAdd(aTabAKM, aAuxAKM)

	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '03'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0014}) //'Conta Orcamentaria  '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK5  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK5_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_CO    '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK5  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '04'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0015}) //'Classe Orcamentaria '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AK6  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AK6_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_CLASSE'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AK6  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '05'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0020 })//'Operacao'
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AKF  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AKF_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_OPER'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AKF  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)

	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '06'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0016}) //'Centro Custo        '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTT  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTT_CUSTO '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_CC    '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTT  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '07'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0017}) //'Item Contabil(CTB)  '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTD  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTD_ITEM  '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_ITCTB '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTD  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '08'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0018}) //'Classe Valor(CTB)   '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'CTH  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'CTH_CLVL  '})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_CLVLR '})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'CTH  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM)
	
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '09'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0022 }) //'Periodo '
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AKD'})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AKD_DATA'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD'})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_DATA'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , ''})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '""'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"31/12/10"'})
	aAdd(aTabAKM, aAuxAKM)   
	

	//INCLUIR UNIDADE ORCAMENTARIA E AS NOVAS ENTIDADES CRIADAS PELO WIZARD  
	// Verifica a existencia da Unidade Orcamentaria na base do cliente
	aAuxAKM := {} 
	aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
	aAdd(aAuxAKM, {'AKM_ITEM' , '10'})
	aAdd(aAuxAKM, {'AKM_TITULO' , STR0026}) //"Unidade Orcamentaria"
	aAdd(aAuxAKM, {'AKM_ENTSIS' , 'AMF  '})
	aAdd(aAuxAKM, {'AKM_CPOREF' , 'AMF_CODIGO'})
	aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
	aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_UNIORC'})
	aAdd(aAuxAKM, {'AKM_CONPAD' , 'AMF  '})
	aAdd(aAuxAKM, {'AKM_TIPO' , '2'})
	aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
	aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
	aAdd(aTabAKM, aAuxAKM) 
		
	If (nQtdEntid > 4)   
		cItem := '10'
		// Inclui novas entidades
		For nQt := 5 To nQtdEntid
			    
			aAuxAKM := {}
			dbSelectArea("CT0")
			dbSetOrder(1)                                 
				
			dbSeek(xFilial("CT0")+STRZERO(nQt,2)) 
			
			cItem := Soma1(cItem)
			aAdd(aAuxAKM, {'AKM_CONFIG' , '002'})
			aAdd(aAuxAKM, {'AKM_ITEM' , cItem})
			aAdd(aAuxAKM, {'AKM_TITULO' , CT0->CT0_DESC}) 
			aAdd(aAuxAKM, {'AKM_ENTSIS' , CT0->CT0_ALIAS})
			aAdd(aAuxAKM, {'AKM_CPOREF' , CT0->CT0_CPOCHV})
			aAdd(aAuxAKM, {'AKM_ENTFIL' , 'AKD  '})
			aAdd(aAuxAKM, {'AKM_CPOFIL' , 'AKD_ENT'+STRZERO(nQt,2)})
			aAdd(aAuxAKM, {'AKM_CONPAD' , CT0->CT0_F3ENTI})
			aAdd(aAuxAKM, {'AKM_TIPO' , '2'})  
				
			aAdd(aAuxAKM, {'AKM_VALINI' , '"                            "'})
			aAdd(aAuxAKM, {'AKM_VALFIM' , '"zzzzzzzzzzzzzzzzzzzzzzzzzzzz"'})
				
			aAdd(aTabAKM, aAuxAKM) 
		Next                                       
	EndIf 
	
	dbSelectArea("AKM")
	
	For nX := 1 TO Len(aTabAKM)
		RecLock("AKM", .T.)
		AKM->AKM_FILIAL := xFilial("AKM")
		For nY := 1 TO Len(aTabAKM[nX])
			If (nPos := FieldPos(aTabAKM[nX][nY][1])) > 0
				FieldPut(nPos, aTabAKM[nX][nY][2])
			EndIf
		Next
		MsUnLock()	
	Next

EndIf

Return

Static Function a160Suges(nCallOpcx, aHeadAKM, oGdAKM )
Local nLin   := 1
Local aArea := GetArea()
Local aAreaAKW := AKW->(GetArea())
Local nPosItem		:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_ITEM"})
Local nPosTitle		:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_TITULO"})
Local nPosEntSis	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_ENTSIS"})
Local nPosCpoRef	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_CPOREF"})
Local nPosEntFil	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_ENTFIL"})
Local nPosCpoFil	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_CPOFIL"})
Local nPosConPad	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_CONPAD"})
Local nPosTipo		:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_TIPO"})
Local nPosValIni	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_VALINI"})
Local nPosValFim	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_VALFIM"})
Local nPosTipoCp	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_TIPOCP"})
Local nPosTamanh	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_TAMANH"})
Local nPosDecima	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_DECIMA"})
Local nPosPictur	:= aScan(aHeadAKM,{|x| AllTrim(x[2]) == "AKM_PICTUR"})

If nCallOpcx  == 3
	If Empty(M->AKL_CUBE)
		Aviso(STR0023, STR0025,{"Ok"})//"Atencao"###"Deve ser informado o codigo do cubo gerencial."
		Return
    EndIf
    
    dbSelectArea("AKW")
    dbSetOrder(1)
    If !dbSeek(xFilial("AKW")+M->AKL_CUBE)
		Aviso(STR0023, STR0024,{"Ok"})//"Atencao"###"Cubo nao existente. Verifique! "
    	Return
    EndIf
    
    While AKW->(!Eof() .AND. AKW_FILIAL+AKW_COD=xFilial("AKW")+M->AKL_CUBE)	
	    	
    	If nLin > 1 .And. Len(oGdAKM:aCols) < nLin
			AAdd(oGdAKM:aCols,Array( Len(aHeadAKM)+1 ))
			oGdAKM:aCols[nLin][Len(aHeadAKM)+1] := .F.
		EndIf
    	
    	//primeira linha do acols ja existe-somente substituir valores
		oGdAKM:aCols[nLin][nPosItem]	:= StrZero(Val(AKW->AKW_NIVEL),Len(AKM->AKM_ITEM))
		oGdAKM:aCols[nLin][nPosTitle]	:= AKW->AKW_DESCRI
		oGdAKM:aCols[nLin][nPosEntSis]	:= AKW->AKW_ALIAS
		oGdAKM:aCols[nLin][nPosCpoRef]	:= Subs(AKW->AKW_RELAC,6)
		oGdAKM:aCols[nLin][nPosEntFil]	:= LEFT(AKW->AKW_CHAVER,3)
		oGdAKM:aCols[nLin][nPosCpoFil]	:= SUBS(AKW->AKW_CHAVER,6)
		oGdAKM:aCols[nLin][nPosConPad]	:= AKW->AKW_F3
		oGdAKM:aCols[nLin][nPosTipo]	:= "2"
		oGdAKM:aCols[nLin][nPosValIni]	:= '"'+Space(AKW->AKW_TAMANH)+'"'
		oGdAKM:aCols[nLin][nPosValFim]	:= '"'+Repl("z", AKW->AKW_TAMANH)+'"'

		oGdAKM:aCols[nLin][nPosTipoCp]	:= "1"
		oGdAKM:aCols[nLin][nPosTamanh]	:= 0
		oGdAKM:aCols[nLin][nPosDecima]	:= 0
		oGdAKM:aCols[nLin][nPosPictur]	:= ""

        nLin++
		AKW->(dbSkip())
	
	End

EndIf

RestArea(aAreaAKW)
RestArea(aArea)

Return 




/*/
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北砅rograma  矼enuDef   � Autor � Ana Paula N. Silva     � Data �11/12/06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Utilizacao de menu Funcional                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   矨rray com opcoes da rotina.                                 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros砅arametros do array a Rotina:                               潮�
北�          �1. Nome a aparecer no cabecalho                             潮�
北�          �2. Nome da Rotina associada                                 潮�
北�          �3. Reservado                                                潮�
北�          �4. Tipo de Transa噭o a ser efetuada:                        潮�
北�          �		1 - Pesquisa e Posiciona em um Banco de Dados     潮�
北�          �    2 - Simplesmente Mostra os Campos                       潮�
北�          �    3 - Inclui registros no Bancos de Dados                 潮�
北�          �    4 - Altera o registro corrente                          潮�
北�          �    5 - Remove o registro corrente do Banco de Dados        潮�
北�          �5. Nivel de acesso                                          潮�
北�          �6. Habilita Menu Funcional                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�   DATA   � Programador   矼anutencao efetuada                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北�          �               �                                            潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
/*/
Static Function MenuDef()
Local aUsRotina := {}
Local aRotina 	:= {	{ STR0002,		"AxPesqui" , 0 , 1, ,.F.},;    //"Pesquisar"
							{ STR0003, 	"A160DLG" , 0 , 2},;    //"Visualizar"
							{ STR0004, 		"A160DLG" , 0 , 3},;	  //"Incluir"
							{ STR0005, 		"A160DLG" , 0 , 4},; //"Alterar"
							{ STR0006, 		"A160DLG" , 0 , 5}} //"Excluir"

If AMIIn(57) // AMIIn do modulo SIGAPCO ( 57 )
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Adiciona botoes do usuario no aRotina                                  �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
	If ExistBlock( "PCOA1601" )
		//P_E谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//P_E� Ponto de entrada utilizado para inclusao de funcoes de usuarios no     �
		//P_E� browse da tela de processos                                            �
		//P_E� Parametros : Nenhum                                                    �
		//P_E� Retorno    : Array contendo as rotinas a serem adicionados na enchoice �
		//P_E�               Ex. :  User Function PCOA1601                            �
		//P_E�                      Return {{"Titulo", {|| U_Teste() } }}             �
		//P_E滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		If ValType( aUsRotina := ExecBlock( "PCOA1601", .F., .F. ) ) == "A"
			AEval( aUsRotina, { |x| AAdd( aRotina, x ) } )
		EndIf
	EndIf
EndIf
Return(aRotina)