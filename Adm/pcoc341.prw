#INCLUDE "pcoc341.ch"
#include "protheus.ch"
#include "msgraphi.ch"

#DEFINE N_COL_VALOR		2

/*/
_F_U_N_C_苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪哪勘�
北矲UNCAO    � PCOC341  � AUTOR � Paulo Carnelossi      � DATA � 11/02/08   潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪哪幢�
北矰ESCRICAO � Programa de Consulta ao arquivo de saldos mensais dos Cubos  潮�
北�          � Por Periodo                                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� USO      � SIGAPCO                                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砡DOCUMEN_ � PCOC341                                                      潮�
北砡DESCRI_  � Programa de Consulta ao arquivo de saldos mensair dos Cubos  潮�
北砡FUNC_    � Esta funcao podera ser utilizada com a sua chamada normal    潮�
北�          � partir do Menu ou a partir de uma funcao pulando assim o     潮�
北�          � browse principal e executando a chamada direta da rotina     潮�
北�          � selecionada.                                                 潮�
北�          � Exemplo: PCO_C340(2) - Executa a chamada da funcao de visua- 潮�
北�          �                       zacao da rotina.                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砡PARAMETR_� ExpN1 : Chamada direta sem passar pela mBrowse               潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function PCOC341(nCallOpcx,dIni,dFim,nTpPer,nMoeda,cCodCube,cCfgCube,lZerado)

Local bBlock
Local nPos

SaveInter()

Private cCadastro	:= STR0001 //"Consulta Saldos por Periodos - Cubos"
Private aRotina := MenuDef()

If nCallOpcx <> Nil
	nPos := Ascan(aRotina,{|x| x[4]== nCallOpcx})
	If ( nPos # 0 )
		bBlock := &( "{ |x,y,z,k,w,a,b,c,d,e,f,g,h| " + aRotina[ nPos,2 ] + "(x,y,z,k,w,a,b,c,d,e,f,g,h) }" )
		Eval( bBlock,Alias(),AL4->(Recno()),nPos,,,,dIni,dFim,nTpPer,nMoeda,cCodCube,cCfgCube,lZerado)
	EndIf
Else
	mBrowse(6,1,22,75,"AL1")
EndIf

RestInter()

Return


/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  砅co_340View 篈utor  砅aulo Carnelossi    � Data �  11/02/08   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     硆otina que monta a grade e o grafico baseado nos parametros   罕�
北�          砳nformados                                                    罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                           罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function Pco_340View(cAlias,nRecno,nOpcx,xRes1,xRes2,xRes3,dIni,dFim,nTpPer,nMoeda,cCodCube,cCfgCube,lZerado)
Local nX, nZ, nY, lRet := .F.
Local nTpGraph

Local aCuboCfg 		:= {}

Local aCfgAuxCube 	:= {}
Local aSeriesCfg 	:= {}
Local aListArq 		:= {}
Local aAcessoCfg 
Local oStructCube
Local aConfig  := {}
Local lSintetica 	:= .F.
Local lTotaliza 	:= .T.
Local lMovimento 	:= .F.
Local aProcCube 	:= {}
Local aFilesErased 	:= {}
Local aDescrAnt
Local aFiltroAKD
Local aWhere
Local cWhereTpSld
Local lContinua 	:= .T.
Local aTpGrafico

//***********************************************
// Implemantacao do Grafico com FwChartFactory  *
//***********************************************
Private lNewGrafic := SuperGetMV("MV_PCOTPGR",.F.,2)==1 .and. FindFunction("__FWCBCOMP")
// parametro que informa qual objeto grafico sera utilizado 1= fwChart qquer outra informacao = msGraphic
Private oLayer,oChart

If lNewGrafic
                         
	aTpGrafico:= {STR0004,; //"1=Linha"
					"2="+SubStr(STR0007,3)}//"4=Barra"

Else
	aTpGrafico 	:= {STR0004,; //"1=Linha"
								STR0005,; //"2=Area"
								STR0006,; //"3=Pontos"
								STR0007,; //"4=Barra"
								STR0008,; //"5=Piramide"
								STR0009,; //"6=Cilindro"
								STR0010,; //"7=Barra Horizontal"
								STR0011,; //"8=Piramide Horizontal"
								STR0012,; //"9=Cilindro Horizontal"
								STR0013,; //"10=Pizza"
								STR0014,; //"11=Forma"
								STR0015,; //"12=Linha Rapida"
								STR0016,; //"13=Flechas"
								STR0017,; //"14=Gantt"
								STR0018 } //"15=Bolha"

EndIf	

Private aParamCons 	:= {}
Private aCfgCub 	:= {}
Private aCfgSel 	:= {}

Private aPeriodo
Private aPerAux		:= {}

Private aColAux
Private COD_CUBO  	:= AL1->AL1_CONFIG
Private nNivInic 	:= 1
Private cClasse
Private lClassView 	:= .F.
                
DEFAULT dIni 		:= dDataBase
DEFAULT dFim 		:= dDataBase+20
DEFAULT nTpPer		:= 3
DEFAULT cCodCube  	:= AL1->AL1_CONFIG
DEFAULT nMoeda 		:= 1
DEFAULT lZerado 	:= .F.

//***************************
// Valida Estrtura do Cubo  *
//***************************
If FindFunction("PcoVldCub") .and. !PcoVldCub(COD_CUBO)
	lContinua := .F.
EndIf

If lContinua
	//1a. tela de parametros
	lContinua := ParamBox({ 	{ 1 ,STR0019,dIni,"" 	 ,""  ,""    ,"" ,50 ,.F. },; //"Periodo de"
							{ 1 ,STR0020,dFim,"" 	 ,""  ,""    ,"" ,50 ,.F. },; //"Periodo Ate"
							{ 2 ,STR0021,nTpPer,{STR0022,STR0023,STR0024,STR0025,STR0026,STR0027,STR0070,STR0077},80,"",.F.},; //"Tipo Periodo"###"1=Semanal"###"2=Quinzenal"###"3=Mensal"###"4=Bimestral"###"5=Semestral"###"6=Anual"##'7=Diario'##'8=Trimestral'
							{ 2 ,STR0028,1,{STR0029,STR0030,STR0031,STR0032,STR0033},80,"",.F.},; //"Moeda"###"1=Moeda 1"###"2=Moeda 2"###"3=Moeda 3"###"4=Moeda 4"###"5=Moeda 5"
							{ 2 ,STR0034,4,aTpGrafico,80,"",.F.},; //"Tipo do Grafico"
							{ 1 ,STR0035,1,"" 	 ,""  ,""    ,"" ,50 ,.F. },;	//"Qtd. Series"
							{ 3 ,STR0069, 1,{STR0039,STR0040},40,,.F.} },STR0036,aParamCons,{||PCOC341TOk()},,,,,, "PCOC341_01",,.T.) //"Parametros" ### "Sim" ### "Nao"
EndIf

If lContinua							

	For nX := 1 TO aParamCons[6]
		&("MV_PAR"+AllTrim(STRZERO(nX+(1*(nX-1)),2,0))) := Space(LEN(AL4->AL4_CODIGO))
		&("MV_PAR"+AllTrim(STRZERO(nX+(1*(nX-1)+1),2,0))) := 1
		aAdd(aCuboCfg, { 1  ,STR0037+Str(nX, 2,0),Space(LEN(AL3->AL3_CODIGO))		  ,"@!" 	 ,''  ,"AL3" ,"" ,25 ,.F. }) //"Config.Cubo Serie"
		aAdd(aCuboCfg, { 3 ,STR0038,1,{STR0039,STR0040},40,,.F.}) //"Exibe Configura珲es"###"Sim"###"Nao"
		aAdd(aCuboCfg, { 1  ,STR0041,STR0042+Str(nx,2,0),"@!" 	 ,""  ,"" ,"" ,75 ,.F. })//"Descri玢o S閞ie"###"Serie "
		aAdd(aCuboCfg, { 3 ,STR0043,1,{STR0044,STR0045},95,,.F.}) //"Considerar "###"Saldo final do periodo"###"Movimento do periodo"
	Next
	
	//2a. tela de parametros (Solicita as configuracoes de cubos desejadas)
	lContinua := ParamBox(aCuboCfg, STR0046, aCfgCub,/*bOk*/,/*aButtons*/,/*lCentered*/,/*nPosx*/,/*nPosy*/, /*oDlgWizard*/, "PCOC341_02"/*cArqParam*/,,.T.) //"Configuracao de Cubos"

EndIf

If lContinua

	nTpGraph 	:= If(ValType(aParamCons[5])=="N", aParamCons[5], Val(aParamCons[5]))
	nTpPer 		:= If(ValType(aParamCons[3])=="N", aParamCons[3], Val(aParamCons[3]))
	aParamCons[4]	:= If(ValType(aParamCons[4])=="N", aParamCons[4], Val(aParamCons[4]))
	aPeriodo 	:= PcoRetPer(aParamCons[1]/*dIniPer*/, aParamCons[2]/*dFimPer*/, Str(nTpPer,1)/*cTipoPer*/, .F./*lAcumul*/, aPerAux)
	nNivInic 	:= 1
	
	If Len(aPerAux) > 180 //limitar em 180 no maximo
        Aviso(STR0047, STR0078, {"Ok"})  //"Atencao"##"Consulta limitada a 180 periodos no maximo. Verifique a periodicidade."
		lContinua := .F.
	EndIf

    If lContinua
		For nX := 1 TO aParamCons[6]
			aAdd(aSeriesCfg, Str(nX,1,0)+"="+aCfgCub[ (nX*4)-1 ])
		Next
	
		For nX := 1 TO Len(aCfgCub) STEP 4
			aAdd( aCfgSel, { aCfgCub[ nX ]/*Configuracao*/, aCfgCub[ nX + 1]/*se exibe Configuracao*/, aCfgCub[ nX + 2 ]/*descricao da serie*/, aCfgCub[ nX + 3 ]/*Final Periodo/Movimento*/ } )
		Next
	
	   	For nX := 1 TO Len(aCfgSel)
	   	
			//verificar se usuario tem acesso as configuracoes
			aAcessoCfg := PcoVer_Acesso( cCodCube, aCfgSel[nX,1] )  	//retorna posicao 1 (logico) .T. se tem acesso
	   											 						//        posicao 2 - Nivel acesso (0-Bloqueado 1-Visualiza 2-altera 
			If ! aAcessoCfg[1]
				lContinua := .F.
	   			Exit
	   		EndIf
	   		
	   		If lContinua
	   		
				lMovimento := ( aCfgSel[nX, 4] == 2 )
		   		oStructCube := PcoStructCube( cCodCube, aCfgSel[nX, 1] )
			
				If Empty(oStructCube:aAlias)  //se estiver vazio eh pq a estrutura nao esta correta
					lContinua := .F.
					Exit
				EndIf
	                
				If aWhere == NIL //logo na primeira configuracao do cubo define tamnho array aWhere
					aWhere := Array(oStructCube:nMaxNiveis)
				EndIf
	
				If aDescrAnt == NIL //logo na primeira configuracao do cubo define tamnho array aDescrAnt
					aDescrAnt := Array(oStructCube:nMaxNiveis)
				EndIf
	
				If aFiltroAKD == NIL //logo na primeira configuracao do cubo define tamnho array aFiltroAKD
					aFiltroAKD := Array(oStructCube:nMaxNiveis)
				EndIf
							
				//monta array aParametros para ParamBox
				aParametros := PcoParametro( oStructCube, .F./*lZerado*/, aAcessoCfg[1]/*lAcesso*/, aAcessoCfg[2]/*nDirAcesso*/ )
	
	            //exibe parambox para edicao ou visualizacao
				Pco_aConfig(aConfig, aParametros, oStructCube, (aCfgSel[nX, 2]==1)/*lViewCfg*/, @lContinua)
				
				If lContinua
					lZerado		:=	aConfig[Len(aConfig)-1]          	//penultimo informacao da parambox (check-box)
					lSintetica	:=	aConfig[Len(aConfig)]        		//ultimo informacao da parambox (check-box)
					lTotaliza 	:= ( aParamCons[7] == 1 )
					//veja se tipo de saldo inicial e final eh o mesmo e se nao ha filtro definido neste nivel
					cWhereTpSld := ""
					If oStructCube:nNivTpSld > 0 .And. ;
						oStructCube:aIni[oStructCube:nNivTpSld] == oStructCube:aFim[oStructCube:nNivTpSld] .And. ;
						Empty(oStructCube:aFiltros[oStructCube:nNivTpSld])
							cWhereTpSld := " AKT.AKT_TPSALD = '" + oStructCube:aIni[oStructCube:nNivTpSld] + "' AND "
					EndIf								
					
				    aAdd(aProcCube, { aPerAux, oStructCube, aCfgSel, aAcessoCfg, lZerado, lSintetica, lTotaliza, cWhereTpSld, lMovimento } )
				    
				EndIf
		   		
			EndIf
			
			If ! lContinua 
				Exit
			EndIf
	   		
	   	Next 
    EndIf
	//montagem da planilha e grafico
	If lContinua
		PCOC_340PFI(aProcCube,nNivInic,""/*cChave*/,nTpGraph,aDescrAnt,aFiltroAKD,aWhere,.T./*lShowGraph*/,aListArq,aFilesErased)
	Else
		Aviso(STR0047,STR0048,{STR0049},2) //"Aten玢o"###"N鉶 existem valores a serem visualizados na configura玢o selecionada. Verifique as configura珲es da consulta."###"Fechar"
	EndIf
	
EndIf

If ! Empty(aFilesErased)
	//apaga os arquivos temporarios criado no banco de dados
	For nZ := 1 TO Len(aFilesErased)
		If Select(Alltrim(aFilesErased[nZ])) > 0
			dbSelectArea(Alltrim(aFilesErased[nZ]))
			dbCloseArea()
		EndIf	
		MsErase(Alltrim(aFilesErased[nZ]))
	Next
EndIf

Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  砅COC_340PFI 篈utor  砅aulo Carnelossi  � Data �  11/02/08   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     硆otina que exibe a grade e o grafico                        罕�
北�          �                                                            罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/

Static Function PCOC_340PFI( aProcCube, nNivel, cChave, nTpGrafico, aDescrAnt, aFiltroAKD, aWhere , lShowGraph, aListArq, aFilesErased)

Local aArea 		:= GetArea()
Local cAlias

Local oDlg
Local oPanel
Local oPanel1
Local oPanel2
Local aSize      	:= {}
Local aPosObj    	:= {}
Local aObjects   	:= {}
Local aInfo      	:= {}

Local oView
Local aView      	:= {}

Local oGrafico

Local aTotProc
Local aProcessa
Local nx,ny,nZ
Local cDescri    	:= ""
Local aTabMail   	:= {}
Local aParam     	:= {"",.F.,.F.,.F.}

Local cFiltro
Local lPergNome  	:= ( SuperGetMV("MV_PCOGRAF",.F.,"2") == "1" )

Local aButtons   	:= {}

Local cWhere := ""
Local aDtSaldo := {}
Local aDtIni := {}
Local aTotSer := {}
Local aTmpSld, cQry, nLin, cChaveAux, aValorAux, aTotSerAux
Local nMaxNiv := aProcCube[1, 2]:nMaxNiveis
Local nNivClasse := aProcCube[1, 2]:nNivClasse

Local lTotaliza := ( aParamCons[7] == 1 )
Local oStructCube 
Local cDescrAux := ""
Local cDescrChv := ""
Local cCpoAux
Local lAuxSint 

Local bLinTotal 	:= {|| lTotaliza .And. Alltrim(Upper(aView[oView:nAT,1])) == "TOTAL" } 

Local bClasse 		:= {|| If(lClassView, cClasse := aView[oView:nAT,1], NIL) }

Local bAddDescr 	:= {|| aDescrAnt[nNivel] := Str(nNivel,2,0)+". "+Alltrim(cDescri)+" : "+AllTrim(aView[oView:nAT,1])+" - "+AllTrim(aView[oView:nAT,2]) }

Local bFiltro 		:= {|| aFiltroAKD[nNivel] := cCpoAux +" = '"+aView[oView:nAT,1]+"' "}

Local bDoubleClick 	:= {||	If(Eval(bLinTotal), ;
								NIL, ;						// se for ultima linha/total nao faz nada 
								If(nNivel < nMaxNiv, ; 		//se nao atingiu o ultimo nivel
									( 	Eval(bFiltro), ; 	//atribui o filtro
										Eval(bClasse), ;  	//se eh dimensao por classe atribui a varmem
										Eval(bAddDescr), ;  //adiciona a descricao e faz drilldown cubo
										PCOC_340PFI(aProcCube,nNivel+1,aView[oView:nAT,1]/*cChave*/,nTpGrafico,aDescrAnt,aFiltroAKD,aWhere,@lShowGraph,aListArq,aFilesErased) ;
									), ;//senao
									( 	Eval(bFiltro), ;		//atribui o filtro
										Pcoc_340lct(aFiltroAKD) ;  // no ultimo nivel o drilldown eh o lancto AKD
									);
								) ;//fecha If
							) ;//fecha If
						}  //fecha bloco de codigo

Local bEncerra := {|| 	If( nNivel == nNivClasse, (lClassView := .F., cClasse := NIL), NIL), ;  // se for nivel da classe inicializa var.
						If( nNivel > nNivInic, ;
								oDlg:End(), ; //se nivel > nivel inicial somente fecha
								If( Aviso(STR0050,STR0051, {STR0039, STR0040},2)==1, ;  //"Atencao"###"Deseja abandonar a consulta ?"###"Sim"###"Nao"
									( PcoArqSave(aListArq), oDlg:End() ), ;
							 		NIL;
						  		);//fecha o 3o. If
						 ) ; //fecha o 2o.If		
					} //fecha codeBlock

//Tratamento das entidades adicionais CV0
Local cPlanoCV0	:= ""
Local cAliasCubo	:= ""
Local cCodCube	:= AL1->AL1_CONFIG

DEFAULT aDescrAnt := {}
DEFAULT cChave := ""
DEFAULT lShowGraph := .T.
DEFAULT aListArq := {}

If nNivel+1 <= nMaxNiv
	aButtons := {	{"PMSZOOMIN"	,{|| Eval(oView:blDblClick) },STR0052 ,STR0053},; //"Drilldown do Cubo"###"Drilldown"
						{"BMPPOST"  ,{|| PmsGrafMail(oGrafico,Padr(cDescri,150),{cCadastro },aTabMail, NIL, 2, .T.) },STR0054,STR0055 },; //"Enviar Emial"###"Email"
						{"GRAF2D"   ,{|| HideShowGraph(oPanel2, oPanel1, @lShowGraph) },STR0071,STR0072 },; //"Exibir/Esconder Grafico"###"Grafico"
						{"SALVAR"	,{|| PcoSaveGraf(oGrafico, lPergNome, .T., .F., aListArq) },STR0074,STR0075 },; //"Imprimir/Gerar Grafico em formato BMP"##"Salva/BMP"
						{"PESQUISA" ,{|| PcoConsPsq(aView,.F.,@aParam,oView) },STR0002,STR0002 },; //Pesquisar
						{"E5"       ,{|| PcoConsPsq(aView,.T.,@aParam,oView) },STR0060 ,STR0060 }; //"Proximo"
					}
Else
	aButtons := {	{"PMSZOOMIN"	,{|| Eval(oView:blDblClick) },STR0052 ,STR0053},; //"Drilldown do Cubo"###"Drilldown"
						{"BMPPOST"  ,{|| PmsGrafMail(oGrafico,Padr(cDescri,150),{cCadastro },aTabMail, NIL, 2, .T.) },STR0054,STR0055 },; //"Enviar Emial"###"Email"
						{"GRAF2D"   ,{|| HideShowGraph(oPanel2, oPanel1, @lShowGraph) },STR0071,STR0072 },; //"Exibir/Esconder Grafico"###"Grafico"
						{"SALVAR"	,{|| PcoSaveGraf(oGrafico, lPergNome, .T., .F., aListArq) },STR0074,STR0075 },; //"Imprimir/Gerar Grafico em formato BMP"##"Salva/BMP"
						{"PESQUISA" ,{|| PcoConsPsq(aView,.F.,@aParam,@oView) },STR0002,STR0002 },; //Pesquisar
						{"E5"       ,{|| PcoConsPsq(aView,.T.,@aParam,@oView) },STR0060 ,STR0060 }; //"Proximo"
					}
EndIf					

For nX := 1 TO Len(aPerAux)
	aAdd(aDtIni, STOD(aPerAux[nX,1]) ) 
	aAdd(aDtSaldo, STOD(aPerAux[nX,2]) ) 
Next

//prepara a clausula where para qdo pressionar o drill-down ja passar para a a funcao
If nNivel > 1
	If Empty(Alltrim(cChave))
		aWhere[ nNivel-1 ] := "AKT.AKT_NIV" + StrZero(nNivel-1, 2) + " = ' ' "
	Else
		aWhere[ nNivel-1 ] := "AKT.AKT_NIV" + StrZero(nNivel-1, 2) + " = '" + Alltrim(cChave) +"' "
	EndIf
	cWhere := ""
	For nZ := 1 TO ( nNivel - 1 )
		cWhere += aWhere[nZ] + " AND "
	Next	
EndIf	

lClassView := ( nNivClasse == nNivel )

CursorWait()

//processa o cubo e o array aTmpSld contera as tabelas temporarias com os saldos para cada serie(cfg) da consulta
//cada elemento de aTmpSld equivale a uma serie (config. cubo)
aTmpSld := 	PcoProcCubo(aProcCube, nNivel, aParamCons[4]/*nMoeda*/, cWhere, aFilesErased, aDtSaldo, aDtIni)

aProcessa := {}
aView := {}
nLin := 0

For nX := 1 TO Len(aTmpSld)   // o Len(aTmpSld) tem q ser igual ao Len(aProcCube) 

	oStructCube := aProcCube[nX, 2]
	cDescri 	:= oStructCube:aDescri[nNivel]
	cCpoAux 	:= oStructCube:aChaveR[nNivel]

	If nX == 1 //somente na primeira serie deve montar o titulo
		aColAux := {}
		aAdd(aColAux, cDescri)
		aAdd(aColAux, STR0058)//"Descricao"
		For nY := 1 TO Len(aPeriodo)
			For nZ := 1 TO aParamCons[6]  //para cada periodo colocar colunas com as series
				aAdd(aColAux, aPeriodo[nY]+"["+AllTrim(aCfgCub[(nZ*4)-1])+"]")
			Next
		Next
		aAdd(aTabMail, aClone(aColAux) )
	EndIf	

	cQry := "SELECT * FROM " + aTmpSld[nX]
	cQry := ChangeQuery( cQry )
	dbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), "TMPAUX", .T., .T. )

    aTotSerAux := Array(Len(aPeriodo))
    aFill(aTotSerAux, 0)

	dbSelectArea("TMPAUX")
	dbGoTop()
	If !Eof()
		While ! Eof()
	
			//Ajustes para entidades adicionais CV0 - Renato Neves
			cPlanoCV0 := ""
			cAliasCubo:= oStructCube:aAlias[nNivel]
			If cAliasCubo == "CV0"
				cPlanoCV0 := GetAdvFVal("AKW","AKW_CHAVER",XFilial("AKW")+cCodCube+StrZero(nNivel,2),1,"")
				cPlanoCV0 := Right(AllTrim(cPlanoCV0),2)
				cPlanoCV0 := GetAdvFVal("CT0","CT0_ENTIDA",XFilial("CT0")+cPlanoCV0,1,"")
			EndIf
	
			cChaveAux := TMPAUX->(FieldGet(FieldPos("AKT_NIV"+StrZero(nNivel,2))))
	
			//descricao tem q macro executar a expressao contida em oStrucCube:aDescRel
			dbSelectArea(oStructCube:aAlias[nNivel])
			cDescrAux := ""
			lAuxSint := .F.	
			If DbSeek(XFilial()+cPlanoCV0+cChaveAux) //Ajustado para entidades adicionais CV0 - Renato Neves
				cDescrAux := &(oStructCube:aDescRel[nNivel])
				If ! Empty(oStructCube:aCondSint[nNivel])
					lAuxSint := &(oStructCube:aCondSint[nNivel])
				EndIf
			Else
				cDescrAux := STR0076 //"Outros"
			EndIf
	
			aValorAux := Array(Len(aPeriodo))
		    aFill(aValorAux, 0)
	
			For nY := 1 TO Len(aPeriodo)
				aValorAux[nY] := TMPAUX->(FieldGet(FieldPos("AKT_SLD"+StrZero(nY, 3))))
				If ! lAuxSint   //SOMENTE ACUMULA NO TOTAL SE NAO FOR SINTETICA
					aTotSerAux[nY] += aValorAux[nY]
				EndIf
			Next
			
			//pesquisa para ver se ja nao existe a linha no array para list box
			If ( nLin := aScan(aView, {|x| x[1] == cChaveAux} ) ) == 0
				//se nao existe
				Pcoc_AddLin(aView, aProcessa, aTabMail, @nLin)       								//adiciona nova linha
	
				Pcoc_AtribVal(aView, nLin, 1/*nCol*/, cChaveAux/*xValue*/)		//adiciona coluna 1 = codigo
	
				Pcoc_AtribVal(aView, nLin, 2/*nCol*/, cDescrAux/*xValue*/)				//adiciona coluna 2 = descricao
	
				//adiciona as colunas de valores de cada periodo
				For nZ := 1 TO Len(aPeriodo)
					Pcoc_AtribVal(aView, nLin, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2  /*nCol*/, aValorAux[nZ] /*xValue*/)
				Next	
				
	            //carrega array aProcessa
				Pcoc_AtribVal(aProcessa, nLin, 1/*nCol*/, cChaveAux /*xValue*/)
	
				For nZ := 1 TO Len(aPeriodo)
					Pcoc_AtribVal(aProcessa, nLin, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 1  /*nCol*/, aValorAux[nZ] /*xValue*/)
				Next	
				
				Pcoc_AtribVal(aTabMail, nLin+1, 1/*nCol*/, cChaveAux /*xValue*/)
				Pcoc_AtribVal(aTabMail, nLin+1, 2/*nCol*/, PadR(cDescrAux, 50) /*xValue*/)
	
				For nZ := 1 TO Len(aPeriodo)
					Pcoc_AtribVal(aTabMail, nLin+1, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2 /*nCol*/, Alltrim(Transform(aValorAux[nZ], "@E 999,999,999,999.99"))/*xValue*/)
				Next	
	
			Else
	
				//se ja existe a linha atribui a coluna de valor correspondente
				For nZ := 1 TO Len(aPeriodo)
					Pcoc_AtribVal(aView, 		nLin, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2/*nCol*/, aValorAux[nZ]/*xValue*/)
					Pcoc_AtribVal(aProcessa, 	nLin, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 1/*nCol*/, aValorAux[nZ]/*xValue*/)
					Pcoc_AtribVal(aTabMail, 	nLin+1, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2/*nCol*/, Alltrim(Transform(aValorAux[nZ], "@E 999,999,999,999.99"))/*xValue*/)
				Next	
	
			EndIf
			
			dbSelectArea("TMPAUX")
			dbSkip()
		
		EndDo
    Else
    	IW_MsgBox(STR0059,STR0050,"STOP")	//"N鉶 existem lan鏰mentos para compor o saldo deste cubo."###"Aten玢o"
    EndIf
    
	aAdd(aTotSer, aClone(aTotSerAux))
	
	dbSelectArea("TMPAUX")
	dbCloseArea()
		
Next
            
CursorArrow()

If !Empty(aView)
                
	aView := aSort( aView,,, { |x,y| x[1] < y[1] } )
	aTabMail := aSort( aTabMail,2,, { |x,y| x[1] < y[1] } )
	aProcessa := aSort( aProcessa,,, { |x,y| x[1] < y[1] } )
	
	If nNivel > 1
		cDescrChv := ""
		For nX := 1 TO ( nNivel - 1 )
			cDescrChv += aDescrAnt[nX]+CRLF
		Next	
	EndIf

	If lTotaliza	// Exibe totais das series
		AAdd( aView, { STR0068, "" } ) // TOTAL
		AAdd( aProcessa, { "" } )
		AAdd( aTabMail, { STR0068, Space(50) } ) // TOTAL
		          
		nLenView := Len(aView)
		
		For nx := 1 to Len(aTotSer)
			For nZ := 1 TO Len(aPeriodo)
				AAdd( aView[nLenView], 0 )
				AAdd( aProcessa[nLenView], 0 )
				AAdd( aTabMail[Len(aTabMail)], Alltrim(Transform(0, "@E 999,999,999,999.99")) )
			Next // nZ
		Next nx
		
		For nX := 1 to Len(aTotSer)
			//adiciona as colunas de valores de cada periodo
			aValorAux := aClone(aTotSer[nX])
			For nZ := 1 TO Len(aPeriodo)
				Pcoc_AtribVal(aView, nLenView, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2  /*nCol*/, aValorAux[nZ] /*xValue*/)
				Pcoc_AtribVal(aProcessa, nLenView, (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 1  /*nCol*/, aValorAux[nZ] /*xValue*/)
				Pcoc_AtribVal(aTabMail, Len(aTabMail), (nZ * aParamCons[6]) - (aParamCons[6]-nX) + 2  /*nCol*/, Alltrim(Transform(aValorAux[nZ], "@E 999,999,999,999.99")) /*xValue*/)
			Next // nZ
		Next nX
	EndIf

	aSize := MsAdvSize(,.F.,400)
	aObjects := {}
	
	AAdd( aObjects, { 100, 100 , .T., .T. } )
	
	aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 2, 2 }
	aPosObj := MsObjSize( aInfo, aObjects, .T. )
	
	DEFINE FONT oBold NAME "Arial" SIZE 0, -11 BOLD
	DEFINE FONT oFont NAME "Arial" SIZE 0, -10 
	DEFINE MSDIALOG oDlg TITLE cCadastro + " - "+cDescri From aSize[7],0 to aSize[6],aSize[5] of oMainWnd PIXEL
	oDlg:lMaximized := .T.
	
	oPanel := TPanel():New(0,0,'',oDlg, , .T., .T.,, ,10,If(Empty(cDescrChv),0,11+((nNivel-1)*11)),.T.,.T. )
	oPanel:Align := CONTROL_ALIGN_TOP

	oPanel1 := TPanel():New(0,0,'',oDlg, , .T., .T.,, ,40,40,.T.,.T. )
	oPanel1:Align := CONTROL_ALIGN_ALLCLIENT

	oPanel2 := TPanel():New(0,0,'',oDlg, , .T., .T.,, ,40,120,.T.,.T. )
	oPanel2:Align := CONTROL_ALIGN_BOTTOM
	If !lShowGraph
		oPanel2:Hide()
	EndIf	

	@ 2,4 SAY "Drilldown" of oPanel SIZE 120,9 PIXEL FONT oBold COLOR RGB(80,80,80)
	@ 3,3 BITMAP oBar RESNAME "MYBAR" Of oPanel SIZE BrwSize(oDlg,0)/2,8 NOBORDER When .F. PIXEL ADJUST

	@ 12,2   SAY cDescrChv Of oPanel PIXEL SIZE 640 ,79 FONT oBold
	

	oView	:= TWBrowse():New( 2,2,aPosObj[1,4]-6,aPosObj[1,3]-aPosObj[1,1]-16,,aColAux,,oPanel1,,,,,,,oFont,,,,,.F.,,.T.,,.F.,,,)
	oView:Align := CONTROL_ALIGN_ALLCLIENT

	For nX := 1 TO Len(aView)
		aView[nX] := PcoFrmDados(aView[nX], cClasse, lClassView)
 	Next

	oView:SetArray(aView)
	oView:blDblClick := bDoubleClick
	oView:bLine := { || aView[oView:nAT]}
	oView:bChange := { || 	oGrafico:= C340_Grafico(aPosObj, oPanel2, oFont, nTpGrafico, cChave, nNivel, aProcessa[oView:nAT],aParamCons,aPeriodo) }
	
	oGrafico:= C340_Grafico(aPosObj, oPanel2, oFont, nTpGrafico, cChave, nNivel, aProcessa[oView:nAT],aParamCons,aPeriodo)
	
	aButtons := aClone(AddToExcel(aButtons,{ {"ARRAY",cDescrChv,aColAux,aView} } ))

	dbSelectArea("AL1")
	
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| Eval(bEncerra)},{|| Eval(bEncerra)},,aButtons )

EndIf

RestArea(aArea)

Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  矯340_Grafico篈utor  砅aulo Carnelossi  � Data �  24/05/05   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     硆otina que monta o objeto grafico para exibicao no folder   罕�
北�          �                                                            罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function C340_Grafico(aPosObj, oPanel, oFont, nTpGrafico, cChave, nNivel, aValues, aParamCons)
Local nZ        := 0
Local ny        := 0
Local oGraphic  
Local nPeriodo	:= 1
Local aSeries	:= {}
Local aColors   := {CLR_BLUE,CLR_GREEN,CLR_RED,CLR_CYAN,CLR_MAGENTA,CLR_BROWN,CLR_HGRAY,CLR_YELLOW,CLR_HBLUE}
Local aSerie2	:= {}

	@ 2,2 MSGRAPHIC oGraphic SIZE aPosObj[1,4]-10,aPosObj[1,3]-aPosObj[1,1]-30 OF oPanel
	oGraphic:Align := CONTROL_ALIGN_ALLCLIENT
	oGraphic:oFont := oFont
	
	oGraphic:SetMargins( 0, 10, 10,10 )
	oGraphic:SetGradient( GDBOTTOMTOP, CLR_HGRAY, CLR_WHITE )
	oGraphic:SetTitle( "", "" , CLR_BLACK , A_LEFTJUST , GRP_TITLE )
	oGraphic:SetLegenProp( GRP_SCRRIGHT, CLR_WHITE, GRP_SERIES, .F. )
	
	For nZ := 1 TO aParamCons[6]
		aAdd(aSeries,Nil	)
		aSeries[nz] := oGraphic:CreateSerie( If(lNewGrafic,If(nTpGrafico==2,4,nTpGrafico),nTpGrafico) )	
	Next nz                
	
	oGraphic:l3D := .F.
	
	For nZ := 2 To Len(aValues) Step aParamCons[6]
		For ny := 1 TO aParamCons[6]
			oGraphic:Add(aSeries[ny], Round(aValues[nZ+ny-1],2), aPeriodo[nPeriodo], aColors[ny])	
		Next
		nPeriodo++
	Next	

	If lNewGrafic

		//oGraphic:Hide()
		//***********************************************
		// Implemantacao do Grafico com FwChartFactory  *
		//***********************************************
		// Monta Series
		aAdd(aSerie2, AllTrim(aCfgCub[3]))
		For ny := 2 To aParamCons[6]
			aAdd(aSerie2, AllTrim(aCfgCub[(4*(ny-1))+3]))
		Next
		
		PcoGrafPer(aValues,aPeriodo,nNivel,cChave,oPanel,nTpGrafico,aSerie2,@oLayer,@oChart,.T.)
	
	EndIf

Return(oGraphic)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯脱屯屯屯屯屯屯屯屯屯屯送屯屯脱屯屯屯屯屯屯槐�
北篜rograma  砅co_C340lct  篈utor 矱dson Maricate      � Data � 03/08/05   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯拖屯屯屯屯屯屯屯屯屯屯释屯屯拖屯屯屯屯屯屯贡�
北篋esc.     砎isualiza lancamento                                        罕�
北�          �                                                            罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function PcoC_340lct(aFiltroAKD, lExibeMsg)
Local aArea			:= GetArea()
Local aAreaAKD		:= AKD->(GetArea())
Local aSize			:= MsAdvSize(,.F.,430)
Local aIndexAKD	:= {}
Local cFiltroAKD
Local nX
PRIVATE bFiltraBrw:= {|| Nil }
Private aRotina 	:= {		{STR0002,"PesqBrw",0,2},;  //"Pesquisar"
								{STR0003,"_C340LctView",0,2}}  //"Visualizar"

Default lExibeMsg := .F.


cFiltroAKD := "AKD->AKD_FILIAL ='"+xFilial("AKD")+"' .And. "
cFiltroAKD += "DTOS(AKD->AKD_DATA) >= '" +DTOS(aParamCons[1])+"' .And. "
cFiltroAKD += "DTOS(AKD->AKD_DATA) <= '" +DTOS(aParamCons[2])+"' .And. "

For nX := 1 TO Len(aFiltroAKD)

	If aFiltroAKD[nX] != NIL
		cFiltroAKD += aFiltroAKD[nX]
		If nX < Len(aFiltroAKD)
			cFiltroAKD += " .And. "
		EndIf
	EndIf

Next	

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//砇ealiza a Filtragem                                                     �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
bFiltraBrw := {|| FilBrowse("AKD",@aIndexAKD,@cFiltroAKD) }
Eval(bFiltraBrw)

If AKD->( EoF() )
	Aviso(STR0050,STR0059,{"Ok"})	// Atencao ### N鉶 existem lan鏰mentos para compor o saldo deste cubo.
Else
	If ExistBlock("PCOC3412")
		aAddBtn := ExecBlock("PCOC3412", .F., .F.)
		aAdd(aRotina,aAddBtn)
	EndIf
	
	mBrowse(aSize[7],0,aSize[6],aSize[5],"AKD")
//	MaWndBrowse(aSize[7],0,aSize[6],aSize[5],cCadastro,"AKD",,aRotina,,,,.F.,,,,,,,,.F.)
EndIf	

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//砇estaura as condicoes de Entrada                                        �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
EndFilBrw("AKD",aIndexAKD)

RestArea(aAreaAKD)
RestArea(aArea)
Return 

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯脱屯屯屯屯屯屯屯屯屯屯送屯屯脱屯屯屯屯屯屯槐�
北篜rograma  砡C340LctView 篈utor 矱dson Maricate      � Data � 03/08/05   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯拖屯屯屯屯屯屯屯屯屯屯释屯屯拖屯屯屯屯屯屯贡�
北篋esc.     砎isualiza lancamento                                        罕�
北�          �                                                            罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function _C340LctView()
Local aArea	:= GetArea()
Local aAreaAKD	:= AKD->(GetArea())

PCOA050(2)

RestArea(aAreaAKD)
RestArea(aArea)
Return



Static Function MenuDef()
Local aRotina 	:= {	{ STR0002,		"AxPesqui" , 0 , 1},; //"Pesquisar"
							{ STR0003, 	"Pco_340View" , 0 , 2} }  //"Visualizar"
						
If AMIIn(57) // AMIIn do modulo SIGAPCO ( 57 )
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Adiciona botoes do usuario no Browse                                   �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
	If ExistBlock( "PCOC3411" )
		//P_E谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//P_E� Ponto de entrada utilizado para inclusao de funcoes de usuarios no     �
		//P_E� browse da tela de Centros Orcamentarios                                            �
		//P_E� Parametros : Nenhum                                                    �
		//P_E� Retorno    : Array contendo as rotinas a serem adicionados na enchoice �
		//P_E�               Ex. :  User Function PCOC3411                            �
		//P_E�                      Return {{"Titulo", {|| U_Teste() } }}             �
		//P_E滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		If ValType( aUsRotina := ExecBlock( "PCOC3411", .F., .F. ) ) == "A"
			AEval( aUsRotina, { |x| AAdd( aRotina, x ) } )
		EndIf
	EndIf      
EndIf
Return(aRotina)

//=========================================================================================================//
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯槐�
北篜rograma  砅coProcCubo 篈utor  砅aulo Carnelossi    � Data � 11/02/08  罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯贡�
北篋esc.     矼onta as querys baseados nos parametros e configuracoes de  罕�
北�          砪ubo e executa essas querys para gerar os arquivos tempora- 罕�
北�          硆ios cujos nomes sao devolvidos no array aTabResult         罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function PcoProcCubo(aProcCube, nNivel, nMoeda, cWhere, aFilesErased, aDtSaldo, aDtIni)
Local cCodCube
Local aTabResult := {}
Local cArquivo := "", cArqSld := ""
Local aQueryDim, cArqTmp
Local nX,nZ
Local cWhereTpSld := ""
Local aPeriodo, oStructCube, lZerado, lSintetica, lTotaliza, nQtdVal, lMovimento
Local cArqAS400 := ""
Local cSrvType := Alltrim(Upper(TCSrvType()))

//Processar todas as series(configuracoes) do cubo

For nX := 1 to Len(aProcCube)

    aPeriodo	:= aProcCube[nX, 1]
    nQtdVal		:= Len(aPeriodo)
	oStructCube := aProcCube[nX, 2]
	lZerado 	:= aProcCube[nX, 5]
	lSintetica 	:= aProcCube[nX, 6]
	lTotaliza 	:= aProcCube[nX, 7]
	lMovimento 	:= aProcCube[nX, 9]
	cWhereTpSld := aProcCube[nX, 8]
	cCodCube 	:= oStructCube:cCodeCube

	If cSrvType == "ISERIES" //outros bancos de dados que nao DB2 com ambiente AS/400
		//cria arquivo para popular
		PcoCriaTemp(oStructCube, @cArqAS400, nQtdVal)
		aAdd(aFilesErased, cArqAS400)
    EndIf

	//cria arquivo para popular
	PcoCriaTemp(oStructCube, @cArquivo, nQtdVal)
	aAdd(aFilesErased, cArquivo)
	aQryDim 	:= {}
    For nZ := 1 TO oStructCube:nMaxNiveis
		If lSintetica .And. nZ > nNivel
			aQueryDim := PcoCriaQueryDim(oStructCube, nZ, lSintetica, .T./*lForceNoSint*/)
    	Else
			aQueryDim := PcoCriaQueryDim(oStructCube, nZ, lSintetica)
		EndIf	
			
		//aqui fazer tratamento quando expressao de filtro e expressao sintetica nao for resolvida
		If (aQueryDim[2] .And. aQueryDim[3])  //neste caso foi resolvida
			
			If ! aQueryDim[4]
				aAdd( aQryDim, { aQueryDim[1], ""} )
			Else	
				aAdd( aQryDim, { aQueryDim[1], aQueryDim[5]} )
			EndIf
			
		Else  //se filtro ou condicao de sintetica nao foi resolvida pela query

			aQueryDim := PcoQueryDim(oStructCube, nZ, @cArqTmp, aQueryDim[1] )
			aAdd(aFilesErased, cArqTmp)

			If ! aQueryDim[4]
				aAdd( aQryDim, { aQueryDim[1], ""} )
			Else	
				aAdd( aQryDim, { aQueryDim[1], aQueryDim[5]} )
			EndIf
			
		EndIf	
    Next
    //criacao das querys para os diversos periodos ( e gerada uma query para cada periodo)
	aQuery := PcoCriaQry( cCodCube, nNivel, nMoeda, cArqAS400, nQtdVal, aDtSaldo, aQryDim, cWhere/*cWhere*/, cWhereTpSld, oStructCube:nNivTpSld, lMovimento, aDtIni )
	//execucao das querys criadas e popular arquivo temporario
	PcoPopulaTemp(oStructCube, cArquivo, aQuery, nQtdVal, lZerado, cArqAS400)
	
	//cria arquivo que contera o resultado da query agrupada 
	PcoCriaTemp(oStructCube, @cArqSld, nQtdVal)
	aAdd(aFilesErased, cArqSld)
    //execucao da query para agrupar os diversos periodos e popular arq temporario que sera usado na consulta
	PcoQryFinal( oStructCube, nNivel, cArqSld/*cAliasSld*/, nQtdVal, cArquivo)

	aAdd( aTabResult, cArqSld )

Next

Return( aTabResult )



/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯屯槐�
北篜rograma  砅coc_AddLin 篈utor  砅aulo Carnelossi  � Data �  23/01/08   罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯屯贡�
北篋esc.     砯uncao que adiciona linha no array aview e no array         罕�
北�          砤processa para a grade com o grafico                        罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function Pcoc_AddLin(aView, aProcessa, aTabMail, nLin)
Local nX, nZ

aAdd(aView, {})  //adiciona uma nova linha
aAdd(aView[Len(aView)], "")  //adiciona na linha a primeira coluna (Codigo)
aAdd(aView[Len(aView)], "")  //adiciona na linha a segunda coluna (Descricao)

For nZ := 1 TO Len(aPeriodo)
	For nX := 1 TO aParamCons[6]  //acrescenta uma nova coluna para cada serie
		aAdd(aView[Len(aView)], 0)  
	Next
Next

aAdd(aTabMail, {})  //adiciona uma nova linha
aAdd(aTabMail[Len(aTabMail)], "")  //adiciona na linha a primeira coluna (Codigo)
aAdd(aTabMail[Len(aTabMail)], "")  //adiciona na linha a segunda coluna (Descricao)

For nZ := 1 TO Len(aPeriodo)
	For nX := 1 TO aParamCons[6]  //acrescenta uma nova coluna para cada serie
		aAdd(aTabMail[Len(aTabMail)], Alltrim(Transform(0, "@E 999,999,999,999.99")) )  
	Next
Next

aAdd(aProcessa, {})
aAdd(aProcessa[Len(aProcessa)], "")  
For nZ := 1 TO Len(aPeriodo)
	For nX := 1 TO aParamCons[6]  //acrescenta uma nova coluna para cada serie
		aAdd(aProcessa[Len(aProcessa)], 0)  
	Next
Next

nLin := Len(aView)

Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯槐�
北篜rograma  砅coc_AtribVal 篈utor  砅aulo Carnelossi  � Data � 23/01/08  罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯贡�
北篋esc.     砯uncao que atribui um valor a linha e coluna informada para 罕�
北�          砤 grade com o grafico                                       罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � AP                                                         罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function Pcoc_AtribVal(aArray, nLin, nCol, xValue)

aArray[nLin, nCol] := xValue

Return


/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北赏屯屯屯屯脱屯屯屯屯屯屯送屯屯屯淹屯屯屯屯屯屯屯屯屯退屯屯屯淹屯屯屯屯屯槐�
北篜rograma  � PCOC341TOk 篈utor  � Gustavo Henrique   � Data �  17/04/08 罕�
北掏屯屯屯屯拓屯屯屯屯屯屯释屯屯屯贤屯屯屯屯屯屯屯屯屯褪屯屯屯贤屯屯屯屯屯贡�
北篋escricao � Validacoes gerais na confirmacao dos parametros informados 罕�
北�          � na Parambox inicial.                                       罕�
北掏屯屯屯屯拓屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯贡�
北篣so       � Consulta de Saldos por Periodo                             罕�
北韧屯屯屯屯拖屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯屯急�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function PCOC341TOk()
Local lRet := .T.
lRet := PCOCVldPer(mv_par01,mv_par02)
Return( lRet )
