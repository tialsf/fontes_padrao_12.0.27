#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'RetailSales.CH'

Static oMStruSL1 	:= Nil //Struct do Model
Static oMStruSL2 	:= Nil //Struct do Model
Static oMStruSL4 	:= Nil //Struct do Model
Static oVStruSL1 	:= Nil //Struct do View
Static oVStruSL2 	:= Nil //Struct do View
Static oVStruSL4 	:= Nil //Struct do View
Static oModelDef 	:= Nil				    //Modelo de dados constru�do

//--------------------------------------------------------
/*/{Protheus.doc} RetailSales
Realiza grava��o da venda
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@param 		aAutoCab  	, Array, Cabe�alho da venda - SL1
@param 		aAutoItens  , Array, Itens da Venda - SL2
@param 		aAutoPagtos , Array, Pagamentos da Venda - SL4
@return	lRet - Retorna se executou corretamente
/*/
//--------------------------------------------------------
Function RetailSales(aAutoCab, aAutoItens, aAutoPagtos, nOpcAuto)

    Local lRotAuto 	:= aAutoCab <> Nil	   	//Cadastro por rotina automatica
    Local lRet		:= .F.

    Private aRotina := MenuDef()       		//Array com os menus disponiveis

    Default aAutoCab 	:= {}
    Default aAutoItens	:= {}
    Default aAutoPagtos	:= {}
    Default nOpcAuto	:= 3

    If lRotAuto

        //Ordena os arrays da MsExecAuto com base no SX3, para n�o ter problemas nos gatilhos
        aAutoCab    := FwVetByDic(aAutoCab   , "SL1", .F., 1)

        aAutoItens  := FwVetByDic(aAutoItens , "SL2", .T., 1)

        aAutoPagtos := FwVetByDic(aAutoPagtos, "SL4", .T., 1)

        lRet := FWMVCRotAuto(ModelDef(), "SL1", nOpcAuto, { {"SL1MASTER", aAutoCab}, {"SL2DETAIL", aAutoItens} , {"SL4DETAIL", aAutoPagtos} })
    Else
        // N�o disponivel melhoria futura
        /*
        oBrowse := FWmBrowse():New()
        oBrowse:SetAlias( 'SL1' )
        oBrowse:SetDescription(STR0001) //"Venda Assistida"
        oBrowse:Activate()
        */
        
    EndIf	

Return lRet

//--------------------------------------------------------
/*/{Protheus.doc} MenuDef
MenuDef MVC
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    aRotina - Rotinas disponiveis
/*/
//--------------------------------------------------------
Static Function MenuDef()

    Local aRotina := {}

    aAdd( aRotina, { STR0002, 'VIEWDEF.RetailSales', 0, 2, 0, NIL } )   //"Visualizar"
    aAdd( aRotina, { STR0003, 'VIEWDEF.RetailSales', 0, 3, 0, NIL } )   //"Incluir"
    aAdd( aRotina, { STR0004, 'VIEWDEF.RetailSales', 0, 4, 0, NIL } )   //"Alterar"
    aAdd( aRotina, { STR0005, 'VIEWDEF.RetailSales', 0, 8, 0, NIL } )   //"Imprimir"

Return aRotina

//--------------------------------------------------------
/*/{Protheus.doc} ModelDef
ModelDef MVC
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    oModel - modelo de dados
/*/
//--------------------------------------------------------
Static Function ModelDef()

    If oModelDef == Nil

        LoadStrModel() //Carrega Struct do Model

        oModelDef := MPFormModel():New( 'RetailSales') // Cria o objeto do Modelo de Dados

        oModelDef:AddFields( 'SL1MASTER', /*cOwner*/ , oMStruSL1 )   // Adiciona ao modelo um componente de formul�rio
        oModelDef:AddGrid(   "SL2DETAIL", "SL1MASTER", oMStruSL2, /*bLinePre*/, {|oModelSL2, nLinhaAtu| ValidaSL2(oModelSL2, nLinhaAtu)}, /*bPre*/, /*bPost*/ )
        oModelDef:AddGrid(   'SL4DETAIL', 'SL1MASTER', oMStruSL4 )    // Adiciona ao modelo uma componente de grid

        oModelDef:SetRelation( 'SL2DETAIL', { { 'L2_FILIAL', 'xFilial( "SL2" )' }, { 'L2_NUM', 'L1_NUM' } }, SL2->( IndexKey( 1 ) ) )
        oModelDef:SetRelation( 'SL4DETAIL', { { 'L4_FILIAL', 'xFilial( "SL4" )' }, { 'L4_NUM', 'L1_NUM' } }, SL4->( IndexKey( 1 ) ) )

        oModelDef:SetDescription(STR0001)//"Venda Assistida"
        oModelDef:GetModel( 'SL1MASTER' ):SetDescription(STR0006)	//"Cabe�alho da Venda"
        oModelDef:GetModel( 'SL2DETAIL' ):SetDescription(STR0007)	//"Itens da Venda"
        oModelDef:GetModel( 'SL4DETAIL' ):SetDescription(STR0008)   //"Pagamentos da Venda"
    EndIf

Return oModelDef

//--------------------------------------------------------
/*/{Protheus.doc} ViewDef
ViewDef MVC
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    ViewDef - View do modelo
/*/
//--------------------------------------------------------
Static Function ViewDef()

    Local oModel    	:= FWLoadModel( 'RetailSales' )
    Local oView		    := Nil    

    LoadStrView() //Carrega Struct da View

    oView := FWFormView():New()
    oView:SetModel( oModel )

    oView:AddField( 'VIEW_SL1', oVStruSL1, 'SL1MASTER' )
    oView:AddGrid(  'VIEW_SL2', oVStruSL2, 'SL2DETAIL' )
    oView:AddGrid(  'VIEW_SL4', oVStruSL4, 'SL4DETAIL' )

    oView:CreateHorizontalBox( 'BOX_SL1', 15 )
    oView:CreateHorizontalBox( 'BOX_SL2', 65 )
    oView:CreateHorizontalBox( 'BOX_SL4', 20 )

    oView:SetOwnerView( 'VIEW_SL1', 'BOX_SL1' )
    oView:SetOwnerView( 'VIEW_SL2', 'BOX_SL2' )
    oView:SetOwnerView( 'VIEW_SL4', 'BOX_SL4' )

Return oView

//--------------------------------------------------------
/*/{Protheus.doc} LoadStrModel
Carrega struct do Model necessarios e adiciona demais propriedades
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    Nil 
/*/
//--------------------------------------------------------
Static Function LoadStrModel()

    oMStruSL1 	:= FWFormStruct( 1, 'SL1' ) // Cria as estruturas a serem usadas no Modelo de Dados
    oMStruSL2 	:= FWFormStruct( 1, 'SL2' )	// Cria as estruturas a serem usadas no Modelo de Dados
    oMStruSL4 	:= FWFormStruct( 1, 'SL4' )	// Cria as estruturas a serem usadas no Modelo de Dados

    //Deixa os campos como utilizados
    LjxAddFil('SL1', oMStruSL1, 1)
    LjxAddFil('SL2', oMStruSL2, 1)
    LjxAddFil('SL4', oMStruSL4, 1)

    //----------------------------------------------
    // SL1 | Cabe�alho da Venda 
    oMStruSL1:SetProperty("*",MODEL_FIELD_OBRIGAT,.F.)	
    oMStruSL1:SetProperty("*",MODEL_FIELD_VALID,{|| .T.} )
    oMStruSL1:SetProperty("*",MODEL_FIELD_INIT,{|| } )

    oMStruSL1:AddTrigger("L1_LOJA"      ,"L1_NOMCLI"    , {||.T.} , {|| Posicione("SA1",1,xFilial("SA1")+FwFldGet("L1_CLIENTE") + FwFldGet("L1_LOJA"),"A1_NOME")          })

    oMStruSL1:SetProperty("L1_FILIAL"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'xFilial("SL1")' ))
    oMStruSL1:SetProperty("L1_NUM"      ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'GetSxENum("SL1","L1_NUM")' ))
    oMStruSL1:SetProperty("L1_DTLIM"    ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'dDataBase + SuperGetMV("MV_DTLIMIT", .F., 0 )' ))
    oMStruSL1:SetProperty("L1_EMISSAO"  ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'dDataBase' ))
    oMStruSL1:SetProperty("L1_CONFVEN"  ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"SSSSSSSSNSSS"' ))
    oMStruSL1:SetProperty("L1_VEND"     ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'SuperGetMV( "MV_VENDPAD",.F.,"" )' ))
    oMStruSL1:SetProperty("L1_IMPRIME"  ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'RSImprime()' ))
    oMStruSL1:SetProperty("L1_CONDPG"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"CN"' ))
    oMStruSL1:SetProperty("L1_TABELA"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'AllTrim(SuperGetMv("MV_TABPAD"))' ))

    //----------------------------------------------
    // SL2 | Itens da Venda 
    oMStruSL2:SetProperty("*",MODEL_FIELD_OBRIGAT,.F.)
    oMStruSL2:SetProperty("*",MODEL_FIELD_VALID,{|| .T.} )
    oMStruSL2:SetProperty("*",MODEL_FIELD_INIT,{|| } )

    oMStruSL2:SetProperty("L2_PRODUTO" ,MODEL_FIELD_OBRIGAT, .T. ) 
    oMStruSL2:SetProperty("L2_ITEM"    ,MODEL_FIELD_OBRIGAT, .T. ) 
    oMStruSL2:SetProperty("L2_LOCAL"   ,MODEL_FIELD_OBRIGAT, .T. ) 
    oMStruSL2:SetProperty("L2_UM"      ,MODEL_FIELD_OBRIGAT, .T. ) 

    oMStruSL2:SetProperty("L2_ITEM"    ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'RSNumItem("SL2DETAIL")' ))
    oMStruSL2:SetProperty("L2_QUANT"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '1'           ))
    oMStruSL2:SetProperty("L2_VENDIDO" ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '""'          ))
    oMStruSL2:SetProperty("L2_EMISSAO" ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'dDataBase'   ))
    oMStruSL2:SetProperty("L2_GRADE"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"N"'         ))
    oMStruSL2:SetProperty("L2_ENTREGA" ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"2"'         ))
    oMStruSL2:SetProperty("L2_ITEMSD1" ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"000000"'    ))
    oMStruSL2:SetProperty("L2_TURNO"   ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'SuperGetMv("MV_LJTURNO",,"M")' ) )
    oMStruSL2:SetProperty("L2_VLTROCA" ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , '"2"'         ))
    oMStruSL2:SetProperty("L2_VEND"    ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'FwFldGet("L1_VEND")'         ))
    oMStruSL2:SetProperty("L2_PDV"     ,MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'FwFldGet("L1_PDV")'         ))

    oMStruSL2:AddTrigger("L2_PRODUTO"   ,"L2_DESCRI"    , {||.T.} , {|| Posicione('SB1',1,xFilial('SB1')+FwFldGet("L2_PRODUTO"),'B1_DESC')   })
    oMStruSL2:AddTrigger("L2_PRODUTO"   ,"L2_UM"        , {||.T.} , {|| Posicione('SB1',1,xFilial('SB1')+FwFldGet("L2_PRODUTO"),'B1_UM')     })
    oMStruSL2:AddTrigger("L2_PRODUTO"   ,"L2_LOCAL"     , {||.T.} , {|| Posicione('SB1',1,xFilial('SB1')+FwFldGet("L2_PRODUTO"),'B1_LOCPAD') })

    //----------------------------------------------
    // SL4 | Pagamentos 
    oMStruSL4:SetProperty("*",MODEL_FIELD_OBRIGAT,.F.)
    oMStruSL4:SetProperty("*",MODEL_FIELD_INIT,{|| } )
    oMStruSL4:SetProperty("*",MODEL_FIELD_VALID,{|| .T.} )

    oMStruSL4:SetProperty("L4_ITEM", MODEL_FIELD_INIT, FWBuildFeature( STRUCT_FEATURE_INIPAD , 'RSNumItem("SL4DETAIL")' ))

    oMStruSL4:SetProperty("L4_VALOR",MODEL_FIELD_VALID,{|| RSPgFields() } )
    oMStruSL4:SetProperty("L4_FORMA",MODEL_FIELD_VALID,{|| RSPgFields() } )

    oMStruSL4:AddTrigger("L4_VALOR"   ,"L1_PARCELA", {||.T.} , {|| RSParcel()  })
  
Return Nil

//--------------------------------------------------------
/*/{Protheus.doc} LoadStrView
Carrega struct do View e demais propriedades
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    Nil 
/*/
//--------------------------------------------------------
Static Function LoadStrView()

    oVStruSL1 	:= FWFormStruct( 2, 'SL1' )
    oVStruSL2 	:= FWFormStruct( 2, 'SL2' )
    oVStruSL4 	:= FWFormStruct( 2, 'SL4' )

    //Deixa os campos como utilizados
    LjxAddFil('SL1', oVStruSL1, 2)
    LjxAddFil('SL2', oVStruSL2, 2)
    LjxAddFil('SL4', oVStruSL4, 2)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidaSL2()
Valida a linha da SL2 e atualiza valores

@param 	oModelSL2 	- Model da SL2
@param 	nLinhaAtu	- Linha posicionada
@return lRetorno	- .T./.F. Determina se as informa��es foram alteradas corretamente
@author Rafael Tenorio da Costa
@since  15/01/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidaSL2(oModelSL2, nLinhaAtu)

    Local lRetorno   := .T.
    Local oModel     := Nil
    Local nValorUnit := 0

    If !oModelSL2:IsDeleted(nLinhaAtu)

        //Valida o valor unit�rio
        If FwFldGet("L2_VRUNIT") <= 0
            
            //Salva o modelo para ser restaurado, porque a fun��o STWFormPr esta mudando para o modelo da Tabela de Pre�o
            oModel := FwModelActive()

            nValorUnit := STWFormPr( FwFldGet("L2_PRODUTO"), FwFldGet("L1_CLIENTE"), Nil, FwFldGet("L1_LOJA"), 1)

            //Restaura modelo salvo
            FwModelActive(oModel, .T.)

            If nValorUnit > 0
                oModelSL2:LoadValue("L2_VRUNIT", nValorUnit)
            Else
                lRetorno := .F.
                oModel:SetErrorMessage("SL2DETAIL", "L2_VRUNIT", "SL2DETAIL", "L2_VRUNIT", "Valores", I18n("Valor (#1) atribuido ao campo (#2) inv�lido.", {cValToChar(nValorUnit)}), "")
            EndIf
        EndIf

        //Atualiza campos de pre�o da tabela e valor total do item
        If lRetorno
            If FwFldGet("L2_PRCTAB") <= 0
                oModelSL2:LoadValue("L2_PRCTAB", FwFldGet("L2_VRUNIT") )
            EndIf   

            If FwFldGet("L2_VLRITEM") <= 0
                oModelSL2:LoadValue("L2_VLRITEM", FwFldGet("L2_VRUNIT") * FwFldGet("L2_QUANT") )
            EndIf
        EndIf

    EndIf

Return lRetorno

//--------------------------------------------------------
/*/{Protheus.doc} RSNumItem
Retorna o numero do Item de acordo com as regras do Protheus
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    cRet - Numero do Item formatado 
/*/
//--------------------------------------------------------
Function RSNumItem(cModelo)

    Local cRet := ""

    cRet := STBPegaIT(oModelDef:GetModel(cModelo):Length() + 1)

Return cRet

//--------------------------------------------------------
/*/{Protheus.doc} RSImprime
Retorna valor para o campo IMPRIME de acordo com a regra
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    cRet - Valor do campo IMPRIME
/*/
//--------------------------------------------------------
Function RSImprime()

    Local cRet := "1N"

    If AllTrim(FwFldGet("L1_ESPECIE")) $ "NFM|SPED"
        cRet := "2N" 
    EndIf

Return cRet

//--------------------------------------------------------
/*/{Protheus.doc} RSPgFields
Grava valores complementares do cabe�alho da venda
apos ser preenchido nas linhas do pagamento.
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    lRet - Retorno da Execu��o
/*/
//--------------------------------------------------------
Function RSPgFields()

    Local cFormaPgto := AllTrim(FwFldGet("L4_FORMA"))
    Local lRet := .T.

    If FwFldGet("L4_VALOR") > 0 .And. !Empty(cFormaPgto)

        Do Case
            Case IsMoney(cFormaPgto) 
                FwFldPut( "L1_DINHEIR" , FwFldGet("L4_VALOR") - FwFldGet("L4_TROCO"))            
            Case cFormaPgto == 'CH'
                FwFldPut( "L1_CHEQUES" , FwFldGet("L1_CHEQUES") + FwFldGet("L4_VALOR"))          
            Case cFormaPgto $ 'CC|CD'
                FwFldPut("L1_VENDTEF","S")
                IF cFormaPgto == 'CC'  
                    FwFldPut( "L1_CARTAO" , FwFldGet("L1_CARTAO") + FwFldGet("L4_VALOR"))
                Else
                    FwFldPut( "L1_VLRDEBI", FwFldGet("L1_VLRDEBI") + FwFldGet("L4_VALOR"))
                EndIf
            Case cFormaPgto == 'CO'
                FwFldPut( "L1_CONVENI", FwFldGet("L1_CONVENI") + FwFldGet("L4_VALOR"))
            Case cFormaPgto == 'VA'
                FwFldPut( "L1_VALES", FwFldGet("L1_VALES") + FwFldGet("L4_VALOR"))
            Case cFormaPgto == 'FI'
                FwFldPut( "L1_FINANC", FwFldGet("L1_FINANC") + FwFldGet("L4_VALOR"))
            Case cFormaPgto == 'CR'
                FwFldPut( "L1_CREDITO", FwFldGet("L4_VALOR"))
            OtherWise
                FwFldPut( "L1_OUTROS", FwFldGet("L1_OUTROS") + FwFldGet("L4_VALOR"))
        EndCase 

        If !(cFormaPgto $ SuperGetMV("MV_ENTEXCE",.F.,""))	// Formas de pagamento nao consideradas como entrada
            If IsMoney(cFormaPgto) 
                FwFldPut( "L1_ENTRADA" , FwFldGet("L4_VALOR") - FwFldGet("L4_TROCO"))
            Else
                FwFldPut( "L1_ENTRADA" , FwFldGet("L1_ENTRADA") + FwFldGet("L4_VALOR"))
            EndIf
        EndIf

        FwFldPut( "L1_FORMPG" , cFormaPgto)//Armazena ultima forma de pagamento 
        FwFldPut( "L1_PARCELA" , oModelDef:GetModel("SL4DETAIL"):Length())

    EndIf

Return lRet

//--------------------------------------------------------
/*/{Protheus.doc} RSParcel
Retorna a quantidade de poarcelas da venda
@type function
@author  	rafael.pessoa
@since   	05/06/2019
@version 	P12
@return	    nParcel - Retorna o quantidade de parcelas
/*/
//--------------------------------------------------------
Function RSParcel()

    Local nParcel := FwFldGet("L1_PARCELA")

    If FwFldGet("L1_PARCELA") <= 0
        nParcel := oModelDef:GetModel("SL4DETAIL"):Length()
    EndIf    
 
Return nParcel

//--------------------------------------------------------
/*/{Protheus.doc} RsGrvVenda
Rotina para grava��o de venda\cancelamento utilizada pelo RMI

@author Rafael Tenorio da Costa
@since  11/12/19
@return { lRet, GetAutoGrLog() }
@uso    RetailSalesObj.prw e RmiEnvProtheusObj.prw
/*/
//--------------------------------------------------------
Function RsGrvVenda(aCab, aItens, aPagtos, nOpc)

    Local nPosSitua := 0
    Local lRet      := .T.
    Local aErroAuto := {}
    Local cErro     := ""
    Local nCont     := 0

    Private lMsHelpAuto     := .T. //Variavel de controle interno do ExecAuto
    Private lMsErroAuto 	:= .F. //Variavel que informa a ocorr�ncia de erros no ExecAuto
    Private lAutoErrNoFile  := .T. //For�a a grava��o das informa��es de erro em array

    //Carrega a situa��o da venda\cancelamento
    nPosSitua := Ascan(aCab, {|x| x[1] == "L1_SITUA"})

    //Para manter o Legado de cancelamento do RMI JOB de Cancelamento 
    If nPosSitua > 0 .And. aCab[nPosSitua][2] == "IC"

        lRet := RMISLXGRV(aCab)
    Else

        lRet := MsExecAuto( {|a,b,c,d| RetailSales(a,b,c,d)}, aCab, aItens, aPagtos, nOpc)
    EndIf

    If lMsErroAuto .Or. !lRet
        lRet      := .F.
        aErroAuto := GetAutoGrLog()

        For nCont := 1 To Len(aErroAuto)
            cErro += aErroAuto[nCont] + CRLF
        Next nCont
    EndIf

    Asize(aErroAuto, 0)

Return {lRet, cErro}