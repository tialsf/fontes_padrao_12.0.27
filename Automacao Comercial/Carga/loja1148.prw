#INCLUDE "PROTHEUS.CH"

// O protheus necessita ter ao menos uma fun��o p�blica para que o fonte seja exibido na inspe��o de fontes do RPO.
Function LOJA1148() ; Return


//--------------------------------------------------------------------------------
/*/{Protheus.doc} LJCInitialLoadMakerResult()

Classe que representa o resultado da gera��o de carga.  

@author Vendas CRM
@since 07/02/10
/*/
//-------------------------------------------------------------------------------- 
Class LJCInitialLoadMakerResult From FWSerialize
	Data aoGroups
	
	Method New()   
	Method AddGroup()
EndClass
   

//--------------------------------------------------------------------------------
/*/{Protheus.doc} New()

Construtor

@return Self

@author Vendas CRM
@since 16/10/10
/*/
//-------------------------------------------------------------------------------- 
Method New() Class LJCInitialLoadMakerResult
	Self:aoGroups := {}
Return
   

//--------------------------------------------------------------------------------
/*/{Protheus.doc} AddGroup()

Adiciona uma tabela na lista das tabelas transferidas. 

@param oTable: Objeto do tipo LJCInitialLoadTable.
@return Nenhum

@author Vendas CRM
@since 28/06/2012
/*/
//-------------------------------------------------------------------------------- 
Method AddGroup( oGroup ) Class LJCInitialLoadMakerResult
	aAdd( Self:aoGroups, oGroup )
Return
    