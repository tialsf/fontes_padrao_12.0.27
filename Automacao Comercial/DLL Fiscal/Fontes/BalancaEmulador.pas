unit BalancaEmulador;

interface

uses
  Dialogs, BalancaMain, ImpCheqMain, Windows, SysUtils, classes, LojxFun,
  IniFiles, Forms;

Type
  TBalEmul = Class( TBalanca )
  Public
    function Abrir( sPorta:String ):String; override;
    function Fechar( sPorta:String ):String; override;
    function PegaPeso( ):String; override;
  End;

Function OpenEmulador( sPorta:String ):String;
Function CloseToledo : String;

//------------------------------------------------------------------------------
implementation

Var
  iInteiros   : Integer; //Numero de Inteiros do Peso
  iDecimais   : Integer; //Numero de Decimais do Peso

Const
  {
  Se alterar esse nome deve alterar o Exe que gera o arquivo
  como se fosse a interface da balan�a 
  }
  sFile : String = 'emuladorpeso.txt';

//------------------------------------------------------------------------------
Function OpenEmulador( sPorta:String ) : String;
var
  bRet : Boolean;
  fArquivo : TIniFile;
  sMode,sPath : String;
begin
  GravaLog(' OpenEmulador -> Porta:' + sPorta);
  Result:= '0';
  bRet  := True;
  sPath := ExtractFilePath(Application.ExeName);

  // Capturo do SIGALOJA.INI os dados para abertura de porta
  // [BALANCA]
  // Mode = 0 ou 1 (desligado ou ligado)
  // Inteiro = 3
  // Decimais = 3
  try
    GravaLog(' Balanca -> Leitura do arquivo SIGALOJA.INI no caminho [' + sPath + ']');
    fArquivo:= TIniFile.Create(sPath+'SIGALOJA.INI');
    sMode   := fArquivo.ReadString('balanca', 'mode', '1');
    iInteiros := StrToInt(fArquivo.ReadString('balanca', 'Inteiro', '3'));
    iDecimais := StrToInt(fArquivo.ReadString('balanca', 'Decimais', '3'));
    fArquivo.Free;
  except
    MsgStop('N�o foi poss�vel ler o arquivo SIGALOJA.INI / Vide configura��o da Sess�o [BALANCA]');
    GravaLog('N�o foi poss�vel ler o arquivo SIGALOJA.INI / Vide configura��o da Sess�o [BALANCA]');
    bRet := False;
  end;

  If bRet AND (sPorta = '') then
  begin
    ShowMessage(' Porta n�o configurada no cadastro de Esta��o (LOJA121) - Verifique! ');
    GravaLog(' Porta n�o configurada no cadastro de Esta��o (LOJA121) - Verifique! ');
    Result := '1';
    bRet := False;
  end;

  If bRet AND (sMode = '0') then
  begin
    ShowMessage('a balanca esta setada como desligada - [ no arquivo SIGALOJA.INI - Sess�o Balanca ] ');
    GravaLog('a balanca esta setada como desligada - [ no arquivo SIGALOJA.INI - Sess�o Balanca ] ');
    Result := '1';
    bRet := False;
  end;

  If bRet then
  begin
    GravaLog('Emulador de Balan�a conectado com sucesso');
    Result := '0';
  end;
End;

//------------------------------------------------------------------------------
Function CloseToledo : String;
begin
  GravaLog(' Emulador Balanca Fechado ->');
  Result := '0';
end;

//------------------------------------------------------------------------------
Function TBalEmul.Abrir( sPorta:String ):String;
Begin
  Result := OpenEmulador(sPorta);
End;

//------------------------------------------------------------------------------
function TBalEmul.Fechar( sPorta:String ):String;
begin
  Result := CloseToledo;
end;

//------------------------------------------------------------------------------
Function TBalEmul.PegaPeso():String;
Var
  sPeso: String;
  iPos : Integer;
  bRet: Boolean;
  iPeso : currency;
  ListaPeso : TStringList;
Begin
  GravaLog(' Fun��o PegaPeso - Inicio ');
  bRet := True;
  ListaPeso := TStringList.Create();
  ListaPeso.Clear;

  {
  o arquivo ser� convencionado da seguinte forma:
  Respeitando as chaves Inteiro e Decimais do arquivo SIGALOJA.INI
  Usando o separador '.' (ponto)
  }
  If FileExists(sFile) then
  begin
    ListaPeso.LoadFromFile(sFile);
    sPeso := ListaPeso.Strings[0];
    iPos := Pos('.',sPeso);
    If iPos = 0
    then begin
           bRet := False;
           ShowMessage('Arquivo de Peso Fora da Configura��o Padr�o - '+
                        'Insira o peso utilizando o utilit�rio da Balan�a TOTVS');
           GravaLog('Arquivo de Peso Fora da Configura��o Padr�o - '+
                    'Insira o peso utilizando o utilit�rio da Balan�a TOTVS');
         end;

    If bRet then
    begin
      Try
        iPeso := StrToFloat(sPeso);
        GravaLog(' Emulador Balanca  -> Peso Capturado:' + sPeso);
      Except
        ShowMessage('Arquivo de Peso Fora da Configura��o Padr�o - '+
            'Insira o peso utilizando o utilit�rio da Balan�a TOTVS');
        GravaLog('Arquivo de Peso Fora da Configura��o Padr�o - '+
            'Insira o peso utilizando o utilit�rio da Balan�a TOTVS');
        bRet := False;
      End;
    end;
  end
  else
  begin
    bRet := False;
    ShowMessage('Peso n�o capturado - Arquivo de peso n�o encontrado : emuladorpeso.txt');
    GravaLog('Peso n�o capturado - Arquivo de peso n�o encontrado : emuladorpeso.txt');
  end;

  If bRet Then
  Begin
    Result := '0|'+ sPeso;
  End
  Else
  Begin
    Result := '1|';
  End;

  GravaLog(' Fun��o PegaPeso - Fim - Retorno: ' + Result);
end;

initialization
  RegistraBalanca( 'Balanca Emulador', TBalEmul, 'BRA' );

end.
