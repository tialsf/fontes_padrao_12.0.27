unit ImpFiscSchalter;

interface

uses
  Dialogs, ImpFiscMain, Windows, SysUtils, classes, LojxFun,
  IniFiles, Forms;

Type

////////////////////////////////////////////////////////////////////////////////
///  Impressora Fiscal Schalter ECF IF SCFI IE V3.03
///
  TImpSchalter = class(TImpressoraFiscal)
  private
  public
    function Abrir( sPorta:String; iHdlMain:Integer ):String; override;
    function Fechar( sPorta:String ):String; override;
    function LeituraX:String; override;
    function PegaCupom(Cancelamento:String):String; override; {VERIFICAR SE TA FUNCIONANDO LEGAL}
    function PegaPDV:String; override;
    function AbreEcf:String; override;

    function AbreCupom(Cliente:String; MensagemRodape:String):String; override;
    function CancelaCupom(Supervisor:String):String; override;
    function CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:String ):String; override;
    function RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:String; nTipoImp:Integer ): String; override;
    function DescontoTotal( vlrDesconto:String;nTipoImp:Integer ): String; override;
    function AcrescimoTotal( vlrAcrescimo:String ): String; override;
    function Pagamento( Pagamento,Vinculado,Percepcion:String ): String; override;
    function FechaCupom( Mensagem:String ):String; override;
    function FechaEcf:String; override;
    function Gaveta:String; override;
    function Suprimento( Tipo:Integer; Valor:String; Forma:String; Total:String; Modo:Integer; FormaSupr:String ):String; override;
    function AbreCupomNaoFiscal( Condicao,Valor,Totalizador, Texto:String ): String; override;
    function TextoNaoFiscal( Texto:String; Vias:Integer ):String; override;
    function FechaCupomNaoFiscal: String; override;
    function TotalizadorNaoFiscal( Numero,Descricao:String ):String; override;
    function ReducaoZ(MapaRes:String):String; override;
    function PegaSerie:String; override;
    function LeCondPag:String; override;
    function LeAliquotasISS:String; override;
    function LeAliquotas:String; override;
    function RelatorioGerencial( Texto:String ;Vias:Integer; ImgQrCode: String):String; override;
    function ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : String ; Vias : Integer ) : String; Override;
    function MemoriaFiscal( DataInicio,DataFim:TDateTime;ReducInicio,ReducFim,Tipo:String ): String; override;

    function ReImpCupomNaoFiscal( Texto:String ):String; override;
    function RecebNFis( Totalizador, Valor, Forma:String ): String; override;
    function AdicionaAliquota( Aliquota:String; Tipo:Integer ): String; override;
    function GravaCondPag( Condicao:String ):String; override;
    function StatusImp( Tipo:Integer ):String; override;
    procedure AlimentaProperties; override;
    function ImpostosCupom(Texto: String): String; override;  //s� compatibiliza��o
    function Autenticacao( Vezes:Integer; Valor,Texto:String ): String; override;
    function HorarioVerao( Tipo:String ):String; override;

    function DownloadMFD( sTipo, sInicio, sFinal : String ):String; override;
    function GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario  : String  ):String; override;
    function RelGerInd( cIndTotalizador , cTextoImp : String ; nVias : Integer; ImgQrCode: String) : String; override;
    function LeTotNFisc:String; override;
    function DownMF(sTipo, sInicio, sFinal : String):String; override;
    function RedZDado( MapaRes : String ):String;
    function IdCliente( cCPFCNPJ , cCliente , cEndereco : String ): String; Override;
    function EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : String ) : String; Override;
    {
        --  function Pedido( Totalizador, Tef, Texto, Valor, CondPagTef:String ): String; override;
    }
    function ImpTxtFis(Texto : String) : String; Override;
    function GrvQrCode(SavePath,QrCode: String): String; Override;
  end;

////////////////////////////////////////////////////////////////////////////////
///  Procedures e Functions
///
Function OpenSchalter( sPorta:String ):String;
Function CloseSchalter : String;
Function ImpCabec : String;
function CancelaCF( Supervisor:String ):String;
Function ImpCabecNF : String;
function AchaPagto( sForma:String;aFormas:Array of String ):String;
Function StatusSchalter( ) : LPSTR;
Function TrataTags( Mensagem : String ) : String;

//----------------------------------------------------------------------------
implementation

var
  fHandle : THandle;

  fFuncChangePort                 : function (choose  : Integer): Integer; StdCall;
  fFuncEcfCancDoc                 : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfFimTrans                : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfFimTransVinc            : function (operador: LPSTR; vincvias: LPSTR): integer;stdcall;
  fFuncEcfCancVenda               : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfLeituraX                : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfReducaoZ                : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfLXGerencial             : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfRZGerencial             : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfAutentica               : function (operador: LPSTR): Integer; StdCall;
  fFuncEcfImpLinha                : function (szLinha: LPSTR): Integer; StdCall;
  fFuncEcfLineFeed                : function (byEst: Integer; wLin: Integer): Integer; StdCall;
  fFuncEcfAbreGaveta              : function (): Integer; StdCall;

  fFuncEcfImpCab                  : function (byEst: Integer): Integer; StdCall;
  fFuncEcfInicCupomNFiscal        : function (tipo: Integer): Integer; StdCall;
  fFuncEcfInicCNFVinculado        : function (order: LPSTR; postab: LPSTR; valor: LPSTR): Integer; StdCall;
  fFuncEcfVendaItem               : function (szDescr: LPSTR; szValor: LPSTR; byTaxa: Integer): Integer; StdCall;
  fFuncEcfVendaItem78             : function (szDescr: LPSTR; szValor: LPSTR; byTaxa: Integer): Integer; StdCall;
  fFuncEcfVenda_Item              : function (szCodigo: LPSTR; szDescricao: LPSTR; szQInteira: LPSTR; szQFracionada: LPSTR; szValor: LPSTR; byTaxa: Integer): Integer; StdCall;
  fFuncEcfVendaItem3d             : function (szCodigo: LPSTR; szDescricao: LPSTR; szQuantidade: LPSTR; szValor: LPSTR; byTaxa: Integer; szUnidade: LPSTR; szDigitos: LPSTR ) : Integer; StdCall;
  fFuncEcfDescItem                : function (byTipo: Integer; szDescr: LPSTR; szValor: LPSTR) : Integer; StdCall;
  fFuncEcfCancItem                : function (szDescr: LPSTR) : Integer; StdCall;

  fFuncEcfPagamento               : function (byTipo: Integer; szPosTable: LPSTR; szValor: LPSTR; byLmens: Integer) : Integer; StdCall;
  fFuncEcfSubTotal                : function ( ) : Integer; StdCall;
  fFuncEcfCancAcresDescSubTotal   : function (byAcres: Integer; byTipo: Integer; szDescr: LPSTR; szValor: LPSTR) : Integer; StdCall;
  fFuncEcfPagTransfer             : function (pagOUT: LPSTR; pagIN: LPSTR; Value: LPSTR) : Integer; StdCall;

  fFuncEcfLeituraXSerial          : function ( ) : PChar; StdCall;
  fFuncEcfLeitMemFisc             : function (byTipo: Integer; szDi: LPSTR; szDf: LPSTR; wRi: Integer; wRf: Integer; archive: LPSTR ) : Integer; StdCall;

  fFuncEcfStatusImp               : function  ( ) : PChar; StdCall;
  fFuncEcfStatusCupom             : function  (flag_geral: Integer ) : PChar; StdCall;
  fFuncEcfStatusAliquotas         : function  (postab: Integer ) : PChar; StdCall;
  fFuncEcfStatusPayTypes          : function  (postab: Integer ) : PChar; StdCall;
  fFuncEcfStatusDocsNFs           : function  (postab: Integer ) : PChar; StdCall;
  fFuncEcfStatusEquipo            : function  ( ) : PChar; StdCall;
  fFuncEcfStatusUser              : function  (userposition: Integer) : PChar; StdCall;
  fFuncEcfStatusVincs             : function  (postab: Integer) : PChar; StdCall;
  fFuncEcfStatusTroco             : function  ( )  : PChar; StdCall;

  fFuncEcfAcertaData              : function  (dia: Integer; mes: Integer; ano: Integer; hor: Integer; min: Integer; seg: Integer ) : Integer; StdCall;
  fFuncEcfCargaAliqSelect         : function  (byPosTab: LPSTR; szTipo: LPSTR; szAliquota: LPSTR) : Integer; StdCall;
  fFuncEcfProgHeader30            : function  (szL1: LPSTR; szL2: LPSTR; szL3: LPSTR; wLj: Integer; wEq: Integer; szCGC: LPSTR; szIE: LPSTR; szIM: LPSTR): Integer; StdCall;
  fFuncEcfCargaCodCripto          : function  (parametric: Integer) : Integer; StdCall;
  fFuncEcfPrgCod                  : function  (byTAM: Integer) : Integer; StdCall;
  fFuncEcfPayPatterns             : function  (szPosTab: LPSTR; szValor: LPSTR): Integer; StdCall;
  fFuncEcfProgNFComprov           : function  (szPosTab: LPSTR; szTitulo: LPSTR; szDesconto: LPSTR; szAcres: LPSTR; szCancel: LPSTR; szComPag: LPSTR; szVinculado: LPSTR; szVinculo: LPSTR): Integer; StdCall;
  fFuncEcfCancItemDef             : function  (szItem : LPSTR; szDescr:LPSTR ): integer;stdcall;


//----------------------------------------------------------------------------
function TImpSchalter.Abrir(sPorta : String; iHdlMain:Integer) : String;
begin
    Result := OpenSchalter( sPorta );
    if Copy(Result,1,1) = '0' then
      AlimentaProperties;
end;

//----------------------------------------------------------------------------
Function OpenSchalter( sPorta:String ):String;
  function ValidPointer( aPointer: Pointer; sMSg :String ) : Boolean;
  begin
    if not Assigned(aPointer) Then
    begin
      LjMsgDlg('A fun��o "'+sMsg+'" n�o existe na Dll: ' + 'DLL32PHI.dll');
      Result := False;
    end
    else
      Result := True;
  end;
var
  bRet  : Boolean;
  aFunc : Pointer;
  iRet  : Integer;
  iPorta: Integer;
Begin
    Result := '0|';
    fHandle := LoadLibrary( 'DLL32PHI.dll' );

    if (fHandle <> 0) Then
    begin
      bRet := True;

      aFunc := GetProcAddress(fHandle,'ChangePort');
      if ValidPointer( aFunc, 'ChangePort' ) then
        fFuncChangePort := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCancDoc');
      if ValidPointer( aFunc, 'ecfCancDoc' ) then
        fFuncEcfCancDoc := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfFimTrans');
      if ValidPointer( aFunc, 'ecfFimTrans' ) then
        fFuncEcfFimTrans := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfFimTransVinc');
      if ValidPointer( aFunc, 'ecfFimTransVinc' ) then
        fFuncEcfFimTransVinc := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCancVenda');
      if ValidPointer( aFunc, 'ecfCancVenda' ) then
        fFuncEcfCancVenda := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfLeituraX');
      if ValidPointer( aFunc, 'ecfLeituraX' ) then
        fFuncEcfLeituraX := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfReducaoZ');
      if ValidPointer( aFunc, 'ecfReducaoZ' ) then
        fFuncEcfReducaoZ := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfLXGerencial');
      if ValidPointer( aFunc, 'ecfLXGerencial' ) then
        fFuncEcfLXGerencial := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfRZGerencial');
      if ValidPointer( aFunc, 'ecfRZGerencial' ) then
        fFuncEcfRZGerencial := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfAutentica');
      if ValidPointer( aFunc, 'ecfAutentica' ) then
        fFuncEcfAutentica := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfImpLinha');
      if ValidPointer( aFunc, 'ecfImpLinha' ) then
        fFuncEcfImpLinha := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfLineFeed');
      if ValidPointer( aFunc, 'ecfLineFeed' ) then
        fFuncEcfLineFeed := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfAbreGaveta');
      if ValidPointer( aFunc, 'ecfAbreGaveta' ) then
        fFuncEcfAbreGaveta := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfImpCab');
      if ValidPointer( aFunc, 'ecfImpCab' ) then
        fFuncEcfImpCab := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfInicCupomNFiscal');
      if ValidPointer( aFunc, 'ecfInicCupomNFiscal' ) then
        fFuncEcfInicCupomNFiscal := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfInicCNFVinculado');
      if ValidPointer( aFunc, 'ecfInicCNFVinculado' ) then
        fFuncEcfInicCNFVinculado := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfVendaItem');
      if ValidPointer( aFunc, 'ecfVendaItem' ) then
        fFuncEcfVendaItem := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfVendaItem78');
      if ValidPointer( aFunc, 'ecfVendaItem78' ) then
        fFuncEcfVendaItem78 := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfVenda_Item');
      if ValidPointer( aFunc, 'ecfVenda_Item' ) then
        fFuncEcfVenda_Item := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfVendaItem3d');
      if ValidPointer( aFunc, 'ecfVendaItem3d' ) then
        fFuncEcfVendaItem3d := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfDescItem');
      if ValidPointer( aFunc, 'ecfDescItem' ) then
        fFuncEcfDescItem := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCancItem');
      if ValidPointer( aFunc, 'ecfCancItem' ) then
        fFuncEcfCancItem := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfPagamento');
      if ValidPointer( aFunc, 'ecfPagamento' ) then
        fFuncEcfPagamento := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfSubTotal');
      if ValidPointer( aFunc, 'ecfSubTotal' ) then
        fFuncEcfSubTotal := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCancAcresDescSubTotal');
      if ValidPointer( aFunc, 'ecfCancAcresDescSubTotal' ) then
        fFuncEcfCancAcresDescSubTotal := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfPagTransfer');
      if ValidPointer( aFunc, 'ecfPagTransfer' ) then
        fFuncEcfPagTransfer := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfLeituraXSerial');
      if ValidPointer( aFunc, 'ecfLeituraXSerial' ) then
        fFuncEcfLeituraXSerial := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfLeitMemFisc');
      if ValidPointer( aFunc, 'ecfLeitMemFisc' ) then
        fFuncEcfLeitMemFisc := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusImp');
      if ValidPointer( aFunc, 'ecfStatusImp' ) then
        fFuncEcfStatusImp := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusCupom');
      if ValidPointer( aFunc, 'ecfStatusCupom' ) then
        fFuncEcfStatusCupom := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusAliquotas');
      if ValidPointer( aFunc, 'ecfStatusAliquotas' ) then
        fFuncEcfStatusAliquotas := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusPayTypes');
      if ValidPointer( aFunc, 'ecfStatusPayTypes' ) then
        fFuncEcfStatusPayTypes := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusDocsNFs');
      if ValidPointer( aFunc, 'ecfStatusDocsNFs' ) then
        fFuncEcfStatusDocsNFs := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusEquipo');
      if ValidPointer( aFunc, 'ecfStatusEquipo' ) then
        fFuncEcfStatusEquipo := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusUser');
      if ValidPointer( aFunc, 'ecfStatusUser' ) then
        fFuncEcfStatusUser := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusVincs');
      if ValidPointer( aFunc, 'ecfStatusVincs' ) then
        fFuncEcfStatusVincs := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfStatusTroco');
      if ValidPointer( aFunc, 'ecfStatusTroco' ) then
        fFuncEcfStatusTroco := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfAcertaData');
      if ValidPointer( aFunc, 'ecfAcertaData' ) then
        fFuncEcfAcertaData := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCargaAliqSelect');
      if ValidPointer( aFunc, 'ecfCargaAliqSelect' ) then
        fFuncEcfCargaAliqSelect := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfProgHeader30');
      if ValidPointer( aFunc, 'ecfProgHeader30' ) then
        fFuncEcfProgHeader30 := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCargaCodCripto');
      if ValidPointer( aFunc, 'ecfCargaCodCripto' ) then
        fFuncEcfCargaCodCripto := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfPrgCod');
      if ValidPointer( aFunc, 'ecfPrgCod' ) then
        fFuncEcfPrgCod := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfPayPatterns');
      if ValidPointer( aFunc, 'ecfPayPatterns' ) then
        fFuncEcfPayPatterns := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfProgNFComprov');
      if ValidPointer( aFunc, 'ecfProgNFComprov' ) then
        fFuncEcfProgNFComprov := aFunc
      else
        bRet := False;

      aFunc := GetProcAddress(fHandle,'ecfCancItemDef');
      if ValidPointer( aFunc, 'ecfCancItemDef' ) then
        fFuncEcfCancItemDef := aFunc
      else
        bRet := False;

    end
    else
    begin
      LjMsgDlg('O arquivo DLL32PHI.dll n�o foi encontrado.');
      bRet := False;
    end;

    if bRet then
    begin
        iPorta := StrToInt(Copy(sPorta,4,1))-1;
        //Abrir a porta
        If (iPorta = 0) or (iPorta = 1) then
        Begin
            iRet:= fFuncChangePort(iPorta);
            if ((iPorta = 0) and (iRet = 0)) or ((iPorta = 1) and (iRet = 1)) then
                Result := '0|'
            Else
                Result := '1|';
        End
        Else
        Begin
            LjMsgDlg('A Impressora Schalter funciona apenas se estiver conectada � COM1 ou COM2.');
            result := '1|';
        End;
    end
    else
    begin
      result := '1|';
    end;

End;


Function StatusSchalter() : LPSTR;
begin

    // Tabela com os c�digos do status da impressora. Estes correspondem a posi��o 3 da string de retorno
    //--------------------------------------------------------------------------
    //C�digo num�rico	Estado interno correspondente
    //--------------------------------------------------------------------------
    //      0	                livre
    //      65	                em venda
    //      83	                mem�ria n�o inicializada
    //      84	                mem�ria fiscal em erro
    //      85	                perda da mem�ria RAM
    //      90	                cupom aberto (somente o cabe�alho impresso)
    //      99	                em interven��o t�cnica
    //      100	                em per�odo de venda
    //      109	                erro irrecuper�vel
    //      113	                espera de fechamento
    //      115	                fechamento do dia j� realizado
    //      122	                relat�rio
    //      123	                em pagamento
    //      124	                em linha comercial
    //--------------------------------------------------------------------------

    Result := fFuncEcfStatusImp();

end;


function CancelaCF( Supervisor:String ):String;
var

  pAux : PChar;         // Utilizado para verificar se encontrou erro no status da impressora
  vret : LPSTR;         // Retorna string com status da impressora
  iRet : Integer;       // Retorno da execu��o do comando enviado a impressora

begin

    vRet := StatusSchalter();

    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin

        //Se na posi��o 3,3 do vret = 100 a impressora est� em modo de venda, onde conforme
        //manual, pode ser cancelado o cupom pois este j� foi emitido

        if copy(vRet,3,3) = '100' then
        begin
            iRet := fFuncEcfCancDoc('');

            if iRet <> 0 then
            begin
                //C�digo 91: N�o h� cupom a cancelar
                result := '1';
            end
            else
            begin
                iRet := fFuncEcfLineFeed(1,8);
                result := '0';
            end;
        end
        else
        begin
            //------------------------------
            // Faz o cancelamento da venda -
            //------------------------------
            iRet := fFuncEcfCancVenda('');

            if iRet <> 0 then
            begin
                result := '1';
	        end
            else
            begin
			    iRet := fFuncEcfLineFeed(1,8);
                result := '0';
            end;
        end;
    end
    else
        result := '1';

end;


function ImpCabec( ) : String;

var
  vret           : LPSTR;
  sRet           : String;
  aux_pchar      : PChar;
  estado_interno : string;
  situation      : integer;
  iGHeaderLayout : Integer;
  int66          : integer;

begin

    iGHeaderLayout := 0 ;

    vRet := StatusSchalter();

    aux_pchar := StrPos(vret, 'Erro');

    if aux_pchar = nil then
    begin
        estado_interno := copy(vret,3,3);
        situation := strtoint(estado_interno);

        // 0   = Livre
        // 65  = Em venda
        // 90  = Com cupom aberto
        // 99  = Em interven��o t�cnica
        // 100 = Em per�odo de venda
        // 113 = Esperando fechamento
        // 115 = Com o fechamento do dia j� feito
        // 122 = Em relat�rio
        // 123 = Em pagamento
        // 124 = Em linha comercial

        if situation = 113 then
        begin
            //LjMsgDlg('A impressora est� aguardando fechamento e n�o pode realizar vendas neste estado.');
            sRet := CancelaCF('');
            result := ('1');
        end;
        if situation = 115 then
        begin
           //LjMsgDlg('O fechamento do dia corrente j� foi realizado. A impressora n�o pode realizar vendas neste estado.');
           sRet := CancelaCF('');
           result := '1';
        end;
        if situation = 123 then
        begin
           //LjMsgDlg('A impressora est� em pagamento. Neste estado, n�o � poss�vel vender novos itens.');

           sRet := CancelaCF('');
           result := ('1');
        end;
        if situation = 124 then
        begin
           //LjMsgDlg('A impressora est� em linhas comerciais. Neste estado, n�o � poss�vel vender novos itens.');

           sRet := CancelaCF('');
           result := ('1');
        end;

        if (situation = 0) or (situation = 100) then
        begin
            int66 := fFuncEcfImpCab(iGHeaderLayout);
            if int66 <> 0 then
            begin
                result := ('1');
	            LjMsgDlg('Erro ao imprimir cabe�alho: '  + intToStr( int66) );
            end;
        end;

        result := ('0');
    end;

end;


function ImpCabecNF() : String;

var
  vret           : LPSTR;
  aux_pchar      : PChar;
  estado_interno : string;
  situation      : integer;
  iGHeaderLayout : Integer;
  int66          : integer;

begin

    iGHeaderLayout := 1 ;

    vRet := StatusSchalter();

    aux_pchar := StrPos(vret, 'Erro');

    if aux_pchar = nil then
    begin
        estado_interno := copy(vret,3,3);
        situation := strtoint(estado_interno);

        // 0   = Livre
        // 65  = Em venda
        // 90  = Com cupom aberto
        // 99  = Em interven��o t�cnica
        // 100 = Em per�odo de venda
        // 113 = Esperando fechamento
        // 115 = Com o fechamento do dia j� feito
        // 122 = Em relat�rio
        // 123 = Em pagamento
        // 124 = Em linha comercial

        if situation = 113 then
        begin
            LjMsgDlg('A impressora est� aguardando fechamento e n�o pode realizar vendas neste estado.');
            result := ('1');
        end;
        if situation = 115 then
        begin
           LjMsgDlg('O fechamento do dia corrente j� foi realizado. A impressora n�o pode realizar vendas neste estado.');
           result := ('1');
        end;
        if situation = 123 then
        begin
           LjMsgDlg('A impressora est� em pagamento. Neste estado, n�o � poss�vel vender novos itens.');
           result := ('1');
        end;
        if situation = 124 then
        begin
           LjMsgDlg('A impressora est� em linhas comerciais. Neste estado, n�o � poss�vel vender novos itens.');
           result := ('1');
        end;

        if (situation = 0) or (situation = 100) then
        begin
            int66 := fFuncEcfImpCab(iGHeaderLayout);
            if int66 <> 0 then
            begin
                result := ('1');
	            LjMsgDlg('Erro ao imprimir cabe�alho: '  + intToStr( int66) );
            end;
        end;

        result := ('0');
    end;

end;

//---------------------------------------------------------------------------

function AchaPagto( sForma:String;aFormas:Array of String ):String;
  var
    iPos, iTamanho : Integer;
begin
    iPos := 0;
    for iTamanho:=0 to Length(aFormas)-1 do
      if UpperCase(Trim( aFormas[iTamanho] ) ) = UpperCase(sForma) then
        iPos := iTamanho;
    if Length(IntToStr(iPos)) < 2 then
      result := '0' + IntToStr(iPos)
    else
      result := IntToStr(iPos);
end;

//---------------------------------------------------------------------------
function TImpSchalter.Fechar( sPorta:String ) : String;
begin
  Result := CloseSchalter;
end;

//----------------------------------------------------------------------------
Function CloseSchalter : String;
begin
  Result := '0|';
end;

//----------------------------------------------------------------------------
function TImpSchalter.LeituraX:String;
var
  iRet : Integer;
begin
  iRet :=fFuncEcfLeituraX('BLABLA');
  if iRet = 0 then
    Result := '0'
  Else
    Result := '1';
end;

//----------------------------------------------------------------------------
function TImpSchalter.PegaCupom(Cancelamento:String):String;
var
  sRet : String;
  sNumCupom : String;
begin

  //Se n�o tiver nenhum cupom aberto, � retornado os dados do pr�ximo cupom

  sRet := fFuncEcfStatusCupom(0);
  If Copy(sRet,1,5) <> 'Erro:' then
  Begin
    sNumCupom := Copy(sRet,6,6);
    If Cancelamento = 'T' then
        sNumCupom := IntToStr(StrToInt(sNumCupom)-1)
    else
        sNumCupom := IntToStr(StrToInt(sNumCupom));

    Result := '0|' +  sNumCupom;
  End
  Else
    Result := '1';
end;


//----------------------------------------------------------------------------
function TImpSchalter.PegaPDV:String;
var sRet    : String;
    sNumPDV : String;
begin

    sRet := fFuncEcfStatusCupom(0);
    If Copy(sRet,1,5) <> 'Erro:' then
    Begin
        sNumPDV := Copy(sRet,1,4);
        Result  := '0|' + sNumPDV;
    End
    Else
        Result  := '1|';

end;

//----------------------------------------------------------------------------
function TImpSchalter.PegaSerie:String;
var
    sRet    : String;
    pRet    : PChar;
    pAux    : PChar;

begin
    sRet:=Space(20);

    pRet   := fFuncEcfStatusEquipo( );
    pAux   := StrPos(pRet, 'Erro');

    if pAux = nil then
    begin
        sRet := copy(pRet,30,9);
        Result := '0|' + sRet;
    end else
    begin
        Result := '1';
    end;
end;

//----------------------------------------------------------------------------
function TImpSchalter.LeCondPag:String;
var
    i       : Integer;

    sErro   : String;
    sPagto  : String;
    sMsg    : String;
    sPay    : String;

    pRet    : PChar;
    pAux    : PChar;

begin

    sErro := '0';

    For i:=0 to 19 do
    begin
        //----------------------------------------------------------------
        //Faz a chamada da fun��o que retorna as condi��es de pagamento  -
        //----------------------------------------------------------------
        pRet := fFuncEcfStatusPayTypes( i );
        pAux := StrPos(pRet, 'Erro');

        if pAux = nil then
        begin
            //----------------------------------------------------------------
            // O segundo caracter da linha de retorno indica se a condi��o de-
            // pagamento est� Ativa("S") ou N�o ("N"). Deve-se considerar    -
            // somente as condi��es que est�o Ativas.                        -
            //----------------------------------------------------------------
            sPay := copy(pRet,2,1);
            sPagto := sPagto +  copy(pRet,4,20) + '|';
        end else
	    begin
            sMsg := ('Ocorreu o erro de n�mero ' + copy(pRet,7,3)) + chr(13)+ chr(13) ;

            if strtoint(copy(pRet,7,3)) = 4 then
            begin
                sMsg := sMsg + chr(13) + 'CAUSAS:'+ chr(13)+ chr(13) ;

                sMsg := sMsg + '- Impressora fora de linha ou desligada.' + chr(13);
                sMsg := sMsg + '- Verifique se o cabo est� conectado.' + chr(13);
                sMsg := sMsg + '- Verifique se a porta serial em uso est� correta.' + chr(13);
            end;
	        LjMsgDlg( sMsg );
            sErro := '1';
	        break;
        end;
    end;

    If sErro = '0' then
    begin
        Result := '0|' + sPagto;
    end else
        Result := '1';
end;


//--------------------------------------------------------------------------------------------------------------
function TImpSchalter.MemoriaFiscal( DataInicio,DataFim:TDateTime;ReducInicio,ReducFim,Tipo:String  ): String;
var
  iRet      : Integer;
  sDataIn,sDataFim: String;
  sFile     : String;
  fFile     : TextFile;
  sArqAux   : String;
Begin
    sArqAux := 'MEMFISC.TXT';
    //------------------------------------------------------------------
    // Par�metros da Fun��o fFuncEcfLeitMemFisc :                      -
    //                      1) Tipo                                    -
    //                      2) Per�odo Inicial                         -
    //                      3) Per�odo Final                           -
    //                      4) Redu��o Inicial                         -
    //                      5) Redu��o Final                           -
    //                      6) Nome do Arquivo a ser gravado           -
    //------------------------------------------------------------------

    if (Tipo='I') OR (Pos('I', UpperCase(Tipo)) > 0) then
    // Leitura da memoria para Impressora
    Begin
        //-----------------------------------
        // Faz a impress�o por Redu��o      -
        //-----------------------------------
        if ( Trim(ReducInicio)<>'') or ( Trim(ReducFim)<>'') then
        Begin
            ReducInicio:=FormataTexto(ReducInicio,4,0,2) ;
            ReducFim   :=FormataTexto(ReducFim,4,0,2);

            iRet       := fFuncEcfLeitMemFisc (2, '', '', strToInt(ReducInicio), strToInt(ReducFim), '' );
        End
        Else
        Begin
            //-----------------------------------
            // Faz a impress�o por data         -
            //-----------------------------------
            sDataIn    := FormataData(DataInicio,1);
            sDataFim   := FormataData(DataFim,1);
            iRet       := fFuncEcfLeitMemFisc (1, Pchar(sDataIn), Pchar(sdataFim), 0, 0, '' );
        End
    End
    Else
    Begin
        // Leitura da memoria para disco
        result:= '0';
        //----------------------------------------------------------------------------------------
        //Define o local de grava��o do arquivo, que ser� o mesmo onde se encontra a SIGALOJA.DLL-
        //----------------------------------------------------------------------------------------
        sFile := ExtractFilePath(Application.ExeName);

        // Deletar o Arquivo anterior
        If FileExists(sFile + sArqAux) then
        Begin
            AssignFile( fFile, sFile + sArqAux);
            Erase(fFile);
        End;

        If ( Trim(ReducInicio)<>'') or ( Trim(ReducFim)<>'') then
        Begin
            ReducInicio:=FormataTexto(ReducInicio,4,0,2);
            ReducFim   :=FormataTexto(ReducFim,4,0,2);

            iRet := fFuncEcfLeitMemFisc (4, '', '', strToInt(ReducInicio), strToInt(ReducFim), Pchar(sFile + sArqAux) );
        End
        Else
        Begin
            sDataIn    := FormataData(DataInicio,1);
            sDataFim   := FormataData(DataFim,1);

            iRet  := fFuncEcfLeitMemFisc (3, Pchar(sDataIn), Pchar(sdataFim), 0, 0, Pchar(sFile + sArqAux) );
        End;
    End;

    if iRet = 0 then
    begin
        result := '0';

        If (Tipo = 'D') OR (Pos('A', UpperCase(Tipo)) > 0) Then
        begin
            // Grava arquivo no local indicado
            Result := CopRenArquivo( sFile, sArqAux, PathArquivo, DEFAULT_ARQMEMCOM );
            If Result = '0' Then
                LjMsgDlg ('Arquivo gerado com sucesso : ' + sFile + sArqAux );
        end
        else
            iRet := fFuncEcfLineFeed(1,7);

    end
    else
        LjMsgDlg ('Ocorreu o erro de n�mero ' + intToStr(iRet) );

end;

//----------------------------------------------------------------------------
function TImpSchalter.AbreECF : String;
begin
  result := '0';
end;

//----------------------------------------------------------------------------




//----------------------------------------------------------------------------
function TImpSchalter.AbreCupom(Cliente:String; MensagemRodape:String) : String;
var
  vret           : LPSTR;       //Retorno da dll
  pAux           : PChar;       //Verifica se a dll retornou algum erro.
  sStatus        : string;      //Status da impressora
  situation      : integer;     //Situacao
  iGHeaderLayout : Integer;
  int66          : integer;
begin

    iGHeaderLayout := 0 ;

    vRet := StatusSchalter();

    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin
        sStatus    := copy(vret,3,3);
        situation := strtoint(sStatus);

        // 0   = Livre
        // 65  = Em venda
        // 90  = Com cupom aberto
        // 99  = Em interven��o t�cnica
        // 100 = Em per�odo de venda
        // 113 = Esperando fechamento
        // 115 = Com o fechamento do dia j� feito
        // 122 = Em relat�rio
        // 123 = Em pagamento
        // 124 = Em linha comercial

        Result := '0';

        if situation = 113 then
        begin
            LjMsgDlg('A impressora est� aguardando fechamento e n�o pode realizar vendas neste estado.');
            result := '1';
        end;
        if situation = 115 then
        begin
           LjMsgDlg('O fechamento do dia corrente j� foi realizado. A impressora n�o pode realizar vendas neste estado.');
           result := '1';
        end;
        if situation = 123 then
        begin
           LjMsgDlg('A impressora est� em pagamento. Neste estado, n�o � poss�vel vender novos itens.');
           result := '1';
        end;
        if situation = 124 then
        begin
           LjMsgDlg('A impressora est� em linhas comerciais. Neste estado, n�o � poss�vel vender novos itens.');
           result := '1';
        end;

        if (situation = 0) or (situation = 100) then
        begin
            int66 := fFuncEcfImpCab(iGHeaderLayout);
            if int66 <> 0 then
            begin
                result := '1';
	            LjMsgDlg('Erro ao imprimir cabe�alho: '  + intToStr( int66) );
            end;
        end;


    end;
end;


//----------------------------------------------------------------------------
function TImpSchalter.ReducaoZ(MapaRes:String):String;

var
  i           : Integer;            // Contador
  iRet        : Integer;            // Retorno da fu��o
  sRet        : String;             // Utilizada para armazenar dados do retorno das fun��es
  sGrandTot   : String;             // Grande Total
  sNroECF     : String;             // Numero do ECF
  sSeriePDV   : String;             // Armazena a s�rie do PDV
  sNumCupom   : String;             // Pega o pr�ximo nro de cupom
  sCupomIni   : String;             // Pega o n�mero do primeiro cupom impresso
  sCupomFin   : String;             // Pega o n�mero do �ltimo cupom impresso
  sNroReduz   : String;             // Numero de Redu��es Z realizadas pelo ECF
  sTotCanc    : String;             // Total de Cancelamentos do dia
  sTotDesc    : String;             // Total de descontos do dia
  sTotSubst   : String;             // Total de Substitui��o Tribut�ria
  sTotIsento  : String;             // Total de isentos
  sTotNaoTrib : String;             // Total de Nao tributado
  sDataReduz  : String;             // Data da Redu��o Z;
  sTotISS     : String;             //Total de ISS
  sAliq       : String;             // Aliquota cadastrada na impressora
  sNroInterv  : String;             // N�mero de interven��es t�cnicas

  pRet        : PChar;              // Retorno da fun��o LeituraXSerial
  aRetorno    : array of String;    //Retorno dos dados para o mapa resumo
  pPath       : PChar;
  sPath       : String;
  fArquivo    : TIniFile;

begin
    iRet := 0;

    MapaRes:= 'S';

    If Trim(MapaRes)='S' then
    begin

        // Prepara o array, aRetorno, com os dados do ECF...
        SetLength(aRetorno,21);

        sRet := fFuncEcfStatusCupom(0);
        If Copy(sRet,1,5) <> 'Erro:' then
        begin

            sNroECF     := Copy(sRet,1,4);

            sGrandTot   :=  StrTran(Copy(sRet,47,21),'.','');            // Grande Total (17 d�gitos)
            sGrandTot   := FormataTexto(sGrandTot, 18, 2, 1);

            //------------------------------------------------------------------
            // Pega a S�rie do PDV
            //------------------------------------------------------------------
            sRet := PegaSerie();
            if Copy( sRet,1,1) = '0' then
            begin
                sSeriePDV := Copy (sRet, 3, length(sRet) );

                //--------------------------------------------------------------
                // Pega o Numero do Ultimo
                //--------------------------------------------------------------
                sRet := PegaCupom('');
                if Copy( sRet,1,1) = '0' then
                begin
                    sNumCupom :=  intToStr((strToInt(Copy(sRet, 3,length(sRet)))));
                    sNumCupom := FormataTexto( sNumCupom,6,0,2 );

                    //----------------------------------------------------------
                    // Pega os dados da leituraXserial
                    //----------------------------------------------------------
                    pRet := fFuncEcfLeituraXSerial();
                    If Copy(pRet,1,5) <> 'Erro:' then
                    begin
                        sNroReduz   := Copy(pRet,1,4);
                        sDataReduz  := Copy(pRet,5,8);
                        sNroInterv  := FormataTexto(Copy(pRet,13,4),3,0,2);
                        sCupomIni   := Copy(pRet,17,6);
                        sCupomFin   := Copy(pRet,23,6);
                        sTotCanc    := FormataTexto( StrTran(Copy(pRet,35,21),'.',''),13, 2, 1);
                        sTotDesc    := FormataTexto( StrTran(Copy(pRet,56,21),'.',''),13, 2, 1);
                        sTotSubst   := FormataTexto( StrTran(Copy(pRet,119,18),'.',''),13, 2,1);
                        sTotIsento  := FormataTexto( StrTran(Copy(pRet,137,18),'.',''),13, 2,1);
                        sTotNaoTrib := FormataTexto( StrTran(Copy(pRet,155,18),'.',''),13, 2,1);
                        sTotISS     := FormataTexto( StrTran(copy(pRet,637,18),'.',''),13, 2,1);

                        aRetorno[ 0]:= sDataReduz;      //**** Data do Movimento                ****//
                        aRetorno[ 1]:= sNroECF;         //**** Numero do ECF                    ****//
                        aRetorno[ 2]:= sSeriePDV;       //**** Serie do ECF                     ****//
                        aRetorno[ 3]:= sNroReduz;       //**** Numero de reducoes               ****//
                        aRetorno[ 4]:= sGrandTot;       //**** Grande Total Final               ****//
                        aRetorno[ 5]:= sCupomIni;       //**** Numero Cupom inicial             ****//aRetorno[ 6];
                        aRetorno[ 6]:= sCupomFin;       //**** Numero Cupom Final               ****//
                        aRetorno[ 7]:= sTotCanc;        //**** Valor do Cancelamento            ****//

                        aRetorno[ 9]:= sTotDesc;        //**** Desconto                         ****//
                        aRetorno[10]:= sTotSubst;       //**** Nao tributado SUBSTITUIcao TRIB  ****//
                        aRetorno[11]:= sTotIsento;      //**** Nao tributado ISENTO             ****//
                        aRetorno[12]:= sTotNaoTrib;     //**** Nao tributado Nao Tributado      ****//
                        aRetorno[13]:= aRetorno[0];     //**** Data da Reducao  Z               ****//
                        aRetorno[14]:= sNumCupom;       //**** Sequencial de Opera��o(4 d�gitos)****//
                        aRetorno[15]:= FormataTexto('0',16, 0, 2);  // Outros recebimentos      ****//
                        aRetorno[16]:= sTotISS;         //**** Total de ISS                     ****//
                        aRetorno[17]:= sNroInterv;      //Numero de Interven��es                ****//
                        aRetorno[18]:= FormataTexto('0',16, 0, 2);  // Desconto de ISS      ****//
			aRetorno[19]:= FormataTexto('0',16, 0, 2);  // Cancelamento de ISS      ****//
                        aRetorno[20]:= '00';             // QTD DE Aliquotas

                        for i := 0 to 15 do
                        begin
                            pRet := fFuncEcfStatusAliquotas( i );
                            if Copy(pRet,1,5) <> 'Erro:' then
                            begin
                                sAliq := copy(pRet,3,5);
			                    if ( Copy(pRet, 2,1) = 'T' ) and (FormataTexto(sAliq,4,2,2) <> '0000')    then
                                begin
                                    aRetorno[20] :=  FormataTexto(IntToStr(StrToInt(aRetorno[20])+1),2,0,2);

                                    SetLength( aRetorno, Length(aRetorno)+1 );
                                    aRetorno[High(aRetorno)]:= 'T' + Copy(pRet,3,5)+' '+FormataTexto(StrTran(Copy(pRet,8,16),'.',''),14,2,1,'.')+' '+FormataTexto(StrTran(Copy(pRet,24,14),'.',''),14,2,1,'.');
                                end;
                            end;
                        end;
                    end;
                end
                else
                begin
                    iRet :=  1;
                end;
            end
            else
            begin
                iRet := 1;
            end;
        End
        Else
        begin
            iRet := 1;
        end;
    end;

    if iRet = 0 then
    begin
        iRet:=1;
        iRet := fFuncEcfReducaoZ('');
        if iRet <> 0 then
        begin
            LjMsgDlg('Erro na Redu��o Z: ' + intToStr(iRet)  );
            Result := '1|' + intToStr(iRet) ;
        end
        else
        begin
            Result := '0';

            // Zera o arquivo da Venda Bruta
            pPath       := Pchar(Replicate('0',100));
            GetSystemDirectory(pPath, 100);
            sPath := StrPas( pPath );
            fArquivo := TIniFile.Create( pPath + '\FILE.SCL');
            fArquivo.WriteString( 'SCHALTER','GT', '0' );

            If Trim(MapaRes) ='S' then
            begin
                Result := '0|';
                For i:= 0 to High(aRetorno) do
                    Result := Result + aRetorno[i]+'|';
            end;
            iRet := fFuncEcfLineFeed(1,8);
        end
    end
    else
    begin
        result := '1';
    end;

end;

//----------------------------------------------------------------------------
function TImpSchalter.LeAliquotas:String;
begin
  //Result := '0|' + ICMS;
  Result := '0|' + aliquotas;
end;
//----------------------------------------------------------------------------
function TImpSchalter.LeAliquotasISS:String;
begin
  result := '0|'+ISS;
end;

//----------------------------------------------------------------------------
Procedure TImpSchalter.AlimentaProperties;


var i       : Integer;
    pRet    : PChar;
    pAux    : PChar;
    sTipo   : String;
    sAliq   : String;

begin
    /// Inicaliza��o de variaveis
    ICMS    := '';
    ISS     := '';
    PDV     := '';
    Eprom   := '';
    Aliquotas := '';

    for i := 0 to 15 do
    begin
        pRet := fFuncEcfStatusAliquotas( i );
        pAux := StrPos(pRet, 'Erro');
        if pAux = nil then
        begin
            Aliquotas := Aliquotas + copy(pRet,2,6) + '|';
            sAliq := copy(pRet,3,5);
            if (FormataTexto(sAliq,4,2,2) <> '0000') then
            begin
                sTipo := copy(pRet,2,1);

                if sTipo = 'S' then
                begin
                    // Retorno de Aliquotas ( ISS )
                    ISS   := ISS + FormataTexto(sAliq,5,2,1) +'|';
                end
                else If sTipo = 'T' then
                begin
                    // Retorno de Aliquotas (ICMS)
                    ICMS   := ICMS + FormataTexto(sAliq,5,2,1) +'|';
                end;
            end;
        end;
    end;
end;


//----------------------------------------------------------------------------
function TImpSchalter.StatusImp( Tipo:Integer ):String;
var
  iRet       : Integer;
  i          : Integer;
  vret       : LPSTR;
  pAux       : PChar;
  pRet       : PChar;
  pPath      : PChar;
  sPath      : String;
  sVB        : String;
  sRet       : String;
  sForma     : String;
  Data       : String;
  Hora       : String;
  sSuprimento: String;
  fArquivo   : TIniFile;

begin
  //Tipo - Indica qual o status quer se obter da impressora
  //  1 - Obtem a Hora da Impressora
  //  2 - Obtem a Data da Impressora
  //  3 - Verifica o Papel
  //  4 - Verifica se � poss�vel cancelar um ou todos os itens.
  //  5 - Cupom Fechado ?
  //  6 - Ret. suprimento da impressora
  //  7 - ECF permite desconto por item
  //  8 - Verifica se o dia anterior foi fechado
  //  9 - Verifica o Status do ECF
  // 10 - Verifica se todos os itens foram impressos.
  // 11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
  // 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
  // 13 - Verifica se o ECF Arredonda o Valor do Item
  // 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
  // 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
  // 16 - Verifica se exige o extenso do cheque
  // 17 - Verifica venda bruta
  // 18 - Verifica Grande Total

  // 20 - Retorna o CNPJ cadastrado na impressora
  // 21 - Retorna o IE cadastrado na impressora
  // 22 - Retorna o CRZ - Contador de Redu��es Z
  // 23 - Retorna o CRO - Contador de Reinicio de Opera��es
  // 24 - Retorna a letra indicativa de MF adicional
  // 25 - Retorna o Tipo de ECF
  // 26 - Retorna a Marca do ECF
  // 27 - Retorna o Modelo do ECF
  // 28 - Retorna o Vers�o atual do Software B�sico do ECF gravada na MF
  // 29 - Retorna a Data de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
  // 30 - Retorna o Hor�rio de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
  // 31 - Retorna o N� de ordem seq�encial do ECF no estabelecimento usu�rio
  // 32 - Retorna o Grande Total Inicial
  // 33 - Retorna o Grande Total Final
  // 34 - Retorna a Venda Bruta Diaria
  // 35 - Retorna o Contador de Cupom Fiscal CCF
  // 36 - Retorna o Contador Geral de Opera��o N�o Fiscal
  // 37 - Retorna o Contador Geral de Relat�rio Gerencial
  // 38 - Retorna o Contador de Comprovante de Cr�dito ou D�bito
  // 39 - Retorna a Data e Hora do ultimo Documento Armazenado na MFD
  // 40 - Retorna o Codigo da Impressora Referente a TABELA NACIONAL DE C�DIGOS DE IDENTIFICA��O DE ECF

  pPath       := Pchar(Replicate('0',100));
  GetSystemDirectory(pPath, 100);
  sPath := StrPas( pPath );
  fArquivo := TIniFile.Create( pPath + '\FILE.SCL');
  //  1 - Obtem a Hora da Impressora
  If Tipo = 1 then
  begin
    Data:=Space(6);
    sRet := fFuncEcfStatusCupom(0);

    If Copy(sRet,1,5) <> 'Erro:' then
    Begin
      Hora   := Copy(sRet,12,6);
      Result := '0|'+Copy(Hora,1,2)+':'+Copy(Hora,3,2)+':'+Copy(Hora,5,2);
    end
    Else
      Result := '1';
  end
    //  2 - Obtem a Data da Impressora
  Else If Tipo = 2 then
  begin
    Data:=Space(6);
    sRet := fFuncEcfStatusCupom(0);

    If Copy(sRet,1,5) <> 'Erro:' then
    Begin
      Data   := Copy(sRet,18,8);
      Result := '0|'+ Data;
    end
    Else
      Result := '1';
  end
  //  3 - Verifica o Papel
  Else If Tipo = 3 then
  begin
    vRet := StatusSchalter();
    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin
        //Sexta posi��o da string: 0) Normal; 1) Pouco Papel
        sRet := copy(vret,6,1);
        If sRet = '1' then
            Result := '2'    // Pouco papel
        Else
            Result := '0';
    end
  end

  //  4 - Verifica se � poss�vel cancelar um ou todos os itens.
  Else if Tipo = 4 then
  begin
      Result := '0|TODOS';
  end
    Else If Tipo = 5 then
    Begin
        vRet := StatusSchalter();
        pAux := StrPos(vret, 'Erro');

        if pAux = nil then
        begin
            sRet := copy(vret,3,3);

            //065   - Em venda;
            //090   - Cupom Aberto (somente cabe�alho);
            //123   - Esperando pagamento;
            //100   - Em periodo de venda (cupom fechado)
            //000   - Impressora Livre

            If (sRet = '090') or (sRet='123') or (sRet= '065') then
            begin
                result := '7';
            end
            else
                result :='0';
        end
        else
            result := '1';     //

  end
  //  6 - Ret. suprimento da impressora
  Else If Tipo = 6 then
  begin

    for i:=1 to 19 do
    begin
        //----------------------------------------------------------------
        //Faz a chamada da fun��o que retorna as condi��es de pagamento  -
        //----------------------------------------------------------------
        pRet := fFuncEcfStatusPayTypes( i );
        pAux := StrPos(pRet, 'Erro');

        if pAux = nil then
        begin
            //----------------------------------------------------------------
            // O segundo caracter da linha de retorno indica se a condi��o de-
            // pagamento est� Ativa("S") ou N�o ("N"). Deve-se considerar    -
            // somente as condi��es que est�o Ativas.                        -
            //----------------------------------------------------------------
            sRet := copy(pRet,2,1);
            if sRet = 'S' then
            begin
                sForma := UpperCase( Trim( copy(pRet,4,20) ) );

                if sForma='DINHEIRO' then
                begin
                    // Retorna o valor acumulado efetuado no dia
                    sSuprimento := StrTran(copy(pRet,25,20),',','.');
                    sSuprimento := FloatToStrf(StrToFloat(sSuprimento),ffFixed,18,2);

                    Break;
                end
            end else
            begin
                Result:= '1';
            end
        end;
    end;
    Result := '0|' + sSuprimento;
  end

  //  7 - ECF permite desconto por item
  Else If Tipo = 7 then
    Result := '0'

  //  8 - Verifica se o dia anterior foi fechado
  Else If Tipo = 8 then
  begin

    vRet := StatusSchalter();

    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin
        sRet := copy(vret,3,3);
        iRet := strtoint(sRet);

        // 113 = Esperando fechamento
        if iRet = 113 then
        begin
            Result := '1';
        end
        else
        begin
            Result:= '0';
        end

    end;
  end

  //  9 - Verifica o Status do ECF
  Else If Tipo = 9 then
    Result := '0'

  // 10 - Verifica se todos os itens foram impressos.
  Else If Tipo = 10 then
    Result := '0'

  // 11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
  Else If Tipo = 11 then
    Result := '1'

  // 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
  Else If Tipo = 12 then
    Result := '1'

  // 13 - Verifica se o ECF Arredonda o Valor do Item
  Else If Tipo = 13 then
  begin

    // Para o FlagTruncamento, retorna 1 se a impressora estiver no modo truncamento e 0 se estiver no modo arredondamento.
    // Conforme conversao com o Renato ("suporte da schalter") a impressora trabalha em modo de truncamento.
    // N�o encontrei nehuma informa��o na documenta��o.
    Result := '1';

  end

  // 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
  else if Tipo = 14 then
  begin
    vRet := StatusSchalter();

    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin
        //Gaveta aberta retorna 0. Se estiver fechada retorna 1
        sRet := copy(vret,8,1);
        Result := sRet;
    end
  end

  // 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
  else if Tipo = 15 then
    Result := '0'
  // 16 - Verifica se exige o extenso do cheque
  else if Tipo = 16 then
    Result := '1'
  // 17 - Verifica a Venda Bruta, na Schalter nao existe esse retorno
  // assumiremos o GT como venda bruta
  else if Tipo = 17 then
     Begin
        sRet := fFuncEcfStatusCupom(0);
        If fArquivo.ReadString( 'SCHALTER', 'GT', '' ) = '0' Then
        Begin
          If Copy( sRet, 1, 4 ) = 'Erro' then
            Result := '1'
          Else
          Begin
            sRet := Copy( sRet, 47, 21 );
            sRet := StrTran( sRet, '.', '' );
            sRet := FormataTexto( sRet, 21, 2, 2);
            fArquivo.WriteString( 'SCHALTER','GT', sRet );
            sRet := '0';
            sRet := FormataTexto( sRet, 21, 2, 2);
            Result := '0|' + sRet;
          End;

        End

        Else
        begin
          sRet := Copy( sRet, 47, 21 );
          sRet := StrTran( sRet, '.', '' );
          sRet := FormataTexto( sRet, 21, 2, 2);
          sVB := fArquivo.ReadString( 'SCHALTER', 'GT', '' );
          sVB := FormataTexto( sVB, 21, 0, 2);
          sVB := IntToStr( StrToInt( sRet ) - StrToInt( sVB ) );
          sVB := FormataTexto( sVB, 21, 0, 2);
          Result := '0|' + sVB;
        end;

        fArquivo.Free;

     End
  // 18 - Verifica o Grande Total
  else if Tipo = 18 then
     Begin
       sRet := fFuncEcfStatusCupom(0);
       If Copy( sRet, 1, 4 ) = 'Erro' then
         Result := '1'
       Else
       Begin
         sRet := Copy( sRet, 47, 21 );
         sRet := StrTran( sRet, '.', '' );
         sRet := FormataTexto( sRet, 21, 2, 2);
         fArquivo.WriteString( 'SCHALTER','GT', sRet );
         Result := '0|' + sRet;
       End
     End
  // 20 ao 40 - Retorno criado para o PAF-ECF
  else if (Tipo >= 20) AND (Tipo <= 40) then
    Result := '0'
  else
    Result := '1';

end;

//----------------------------------------------------------------------------
function TImpSchalter.RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:String; nTipoImp:Integer ): String;
var
  sAliq     : String;
  sSituacao : String;
  sRet      : String;
  sCasas    : String;
  sTipoQtd  : String;
  iRet      : Integer;
  aAliq     : TaString;
  iAliq     : Integer;
  iPosTaxa  : Integer;
  iTamanho  : Integer;
  nPos      : Integer;

begin
    sCasas:='2';

    // Verifica a casa decimal dos par�metros
    qtde        := StrTran(qtde,',','.');
    vlrUnit     := StrTran(vlrUnit,',','.');
    vlrdesconto := StrTran(vlrdesconto,',','.');
    vlTotIt     := Trim(StrTran(vlTotIt,',','.'));

    //verifica se � para registra a venda do item ou s� o desconto
    if Trim(codigo+descricao+qtde+vlrUnit) = '' then
    begin
        if StrToFloat(vlrdesconto) <> 0 then
        begin
            iRet := fFuncEcfDescItem( 0, '', PChar( FormataTexto(vlrdesconto,9,2,2) ) );
        end
        else
        begin
            iRet := 0;
        end
    end
    else
    begin
        // Faz o tratamento da aliquota
        sSituacao := copy(aliquota,1,1);

        //aliquota := Trim(StrTran(copy(aliquota,2,5),',','.'));
        If Length( Trim( Copy( aliquota, 2, 5 ) ) ) = 4 then
          aliquota := Copy( aliquota, 1, 1 ) + '0' + Trim( Copy( aliquota, 2, 5 ) );
        aliquota := StrTran(copy(aliquota,1,6),'.',',');

        // Checa as aliquotas
        sRet := LeAliquotas;

        // Problemas na leitura de aliquota ?
        if copy( sRet, 1, 1 ) <> '0' then
        begin
            result := '1|';
            exit;
        end
        else
        begin
            sRet := copy( sRet, 3, length( sRet ) );
        end;

        // Verifica se a aliquota � ISENTA, c/SUBSTITUICAO TRIBUTARIA, NAO TRIBUTAVEL OU ISS
        If (sSituacao = 'T') or (sSituacao = 'S') then
        begin

            MontaArray( sRet, aAliq );

            //--------------------------------------------------------------------------------------
            // Procura a posicao da aliquota dentro do array. Esta posi��o define o tipo de imposto-
            //  0 - 15	taxa 0 at� taxa 15 (fiscais ou ISS)                                        -
            //  16	Substitui��o tribut�ria                                                        -
            //  17	Isento                                                                         -
            //  18	N�o incidente                                                                  -
            //--------------------------------------------------------------------------------------
            iPosTaxa := -1;
            for iTamanho:=0 to Length(aAliq)-1 do
            begin
                if aAliq[iTamanho] = aliquota then
                    //iPosTaxa := iTamanho + 1;
                  iPosTaxa := iTamanho;
            end;
            if (iPosTaxa = -1) then
            begin
                ShowMessage('Aliquota n�o cadastrada.');
                result := '1|';
                exit;
            end;
            {Else
            begin
                // Pega a posi��o exata da al�quota no array
                iPosTaxa := iPosTaxa - 1;
            end;}
        end
        //produto Isento de Tributa��o
        else if Trim(sSituacao) = 'I' then
            iPosTaxa:= 17
        //produto com Substitu��o Tribut�ria
        else if Trim(sSituacao) = 'F' then
            iPosTaxa:= 16
        //produto N�o Tributado
        else if Trim(sSituacao) = 'N' then
            iPosTaxa:= 18;

        nPos := Pos('.',vlTotIt);
        VlTotIt := FormataTexto(Copy(vlTotIt,1,nPos+2),12,2,2);

        if copy(vlrUnit,length(vlrUnit)-3,1) = '.' then
        begin
            VlrUnit	:= FormataTexto(vlrUnit,9,3,2);
            Descricao:= '~'+descricao;
        end
        else
        begin
            VlrUnit	:= FormataTexto(vlrUnit,9,2,2);
        end;

        //--------------------------------------------------------------------------------------
        // Tipo da quantidade 'I'-Inteiro  'F'-Fracionario
        //--------------------------------------------------------------------------------------
        sTipoQtd := 'F';

        //--------------------------------------------------------------------------------------
        // Formata a quantidade como XXXXZZZ onde XXXX = parte inteira e ZZZ = parte fracion�ria
        //--------------------------------------------------------------------------------------
        qtde := StrTran(FormataTexto( qtde, 7, 3, 1 ),'.',',');

        //--------------------------------------------------------------------------------------
        // Numero de cadas decimais para o pre�o unit�rio
        //--------------------------------------------------------------------------------------
        If Pos('.',vlrUnit) > 0 then
            If StrtoFloat(copy(vlrUnit,Pos('.',vlrUnit)+1,Length(vlrUnit))) > 99 then
                sCasas := '3'
            Else
                sCasas := '2';

        //////////////////////////////////////////////////////////////////////////////
        //  Fun��o ecfVendaItem3d - Faz o Registro do �tem
        //  Par�metros: 1) C�digo
        //              2) Descri��o
        //              3) Quantidade - ( M�x 7 Caracter )
        //              5) Valor - Valor unit�rio do �tem
        //              6) Taxa - Indica a posi��o da al�quota;
        //                        Taxa        Tipo de Al�quota
        //                        0-15    :   Taxa 0 at� taxa 15 (Ficais ou ISS)
        //                        16      :   Substitui��o tribut�ria
        //                        17      :   Isento
        //                        18      :   N�o incide
        //              7) Unidade
        //              8) Nro de casas decimais      Copy(codigo+Space(13),1,13)
        //////////////////////////////////////////////////////////////////////////////


        iRet :=fFuncEcfVendaItem3d (    PChar(codigo),
                                        PChar( Copy(descricao+Space(62),1,62) ),
                                        PChar( Qtde ),
                                        PChar( vlrUnit ),
                                        iPosTaxa,
                                        '',
                                        PChar(sCasas ) );

        if iRet = 0 then
        begin
            // Registra o desconto no �tem
            if StrToFloat(vlrdesconto) <> 0 then
                iRet := fFuncEcfDescItem( 0, '', PChar( FormataTexto(vlrdesconto,9,2,2) ) );
            end
    end;

    If iRet = 0 then
        Result := '0'
    Else
        Result := '1';

end;

//----------------------------------------------------------------------------


function TImpSchalter.CancelaCupom( Supervisor:String ):String;
var

  pAux : PChar;         // Utilizado para verificar se encontrou erro no status da impressora
  vret : LPSTR;         // Retorna string com status da impressora
  iRet : Integer;       // Retorno da execu��o do comando enviado a impressora

begin

    vRet := StatusSchalter();

    pAux := StrPos(vret, 'Erro');

    if pAux = nil then
    begin

        //Se na posi��o 3,3 do vret = 100 a impressora est� em modo de venda, onde conforme
        //manual, pode ser cancelado o cupom pois este j� foi emitido

        if copy(vRet,3,3) = '100' then
        begin
            iRet := fFuncEcfCancDoc('');

            if iRet <> 0 then
            begin
                //C�digo 91: N�o h� cupom a cancelar
                result := '1';
            end
            else
            begin
			    iRet := fFuncEcfLineFeed(1,8);
                result := '0';
            end;
        end
        else
        begin
            //------------------------------
            // Faz o cancelamento da venda -
            //------------------------------
            iRet := fFuncEcfCancVenda('');

            if iRet <> 0 then
            begin
                result := '1';
	        end
            else
            begin
			    iRet := fFuncEcfLineFeed(1,8);
                result := '0';
            end;
        end;
    end
    else
        result := '1';

end;

//----------------------------------------------------------------------------

function TImpSchalter.CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:String ):String;
var
  iRet : Integer;  // Retorno do comando enviado a impressora
begin

  // O Numero do �tem a ser cancelado tem que conter 4 digitos;
  // Ap�s o cancelamento do �tem a impressora n�o reordena a numera��o dos �tens que j� foram impressos
  iRet := fFuncEcfCancItemDef( Pchar(FormataTexto(numitem,4,0,2)), '' );

  if iRet = 0 then
    result := '0'
  else
    result := '1';

end;

//----------------------------------------------------------------------------
function TImpSchalter.DescontoTotal( vlrDesconto:String;nTipoImp:Integer ): String;
var
  iRet : Integer;
begin

  vlrDesconto := StrTran(vlrDesconto,',','.');

  iRet := fFuncEcfCancAcresDescSubTotal( 0, 0,'' , Pchar( FormataTexto(vlrDesconto,10,2,2) ) );

  if iRet = 0 then
    result := '0'
  else
    result := '1';

end;

//------------------------------------------------------------------------------

function TImpSchalter.AcrescimoTotal( vlrAcrescimo:String ): String;
var
  iRet : Integer;
begin

  vlrAcrescimo := FormataTexto(StrTran(vlrAcrescimo,',','.'),10,2,2)  ;

  iRet := fFuncEcfCancAcresDescSubTotal( 1, 0,'' , Pchar( vlrAcrescimo ) );

  if iRet = 0 then
    result := '0'
  else
    result := '1';

end;

//----------------------------------------------------------------------------

function TImpSchalter.Pagamento( Pagamento,Vinculado,Percepcion:String ): String;

var
  sRet      : String;
  iRet      : Integer;
  aFormas   : TaString;
  aAuxiliar : TaString;
  i         : Integer;
  sForma    : String;
  sValor    : String;
  nPos      : Integer;
begin

  result := '1|';
  iRet := 1;

  // Verifica o parametro
  Pagamento := StrTran(Pagamento,',','.');

  // Le as condicoes de pagamento
  sRet := LeCondPag;
  sRet := Copy(sRet, 3, Length(sRet));
  MontaArray(sRet,aFormas);

  // Monta um array auxiliar com os parametros
  MontaArray( Pagamento,aAuxiliar );

  i:=0;
  While i<Length(aAuxiliar) do
  begin
    if StrToInt( AchaPagto( aAuxiliar[i],aFormas ) ) >= 0 then
      Begin
        nPos := Pos('.',aAuxiliar[i+1]);
        If nPos > 0 then
          aAuxiliar[i+1] := copy(aAuxiliar[i+1],1,nPos+2);
        sForma := AchaPagto( aAuxiliar[i],aFormas );
        sValor := FormataTexto(aAuxiliar[i+1],10,2,2);

        // Parametros da fun��o EcfPagamento:
        // 1) Tipo da opera��o:  0 = Pagamento; 1 = Cancelamento do pagamento
        // 2) Forma de Pagamento;
        // 3) Valor do pagamento: String de 10 Caracteres (n�o deve-se usar v�rgula ou ponto)
        // 4) N�mero de linhas para observa��o: No m�ximo 3
        iRet := fFuncEcfPagamento( 0, Pchar( sForma ), Pchar( sValor) , 0);

       if iRet = 0 then
          result := '0' ;

      End;
    Inc(i,2);
  end;

  if iRet <> 0 then
  begin
    result :='1';
    sRet := CancelaCF('');
  end;


end;

//----------------------------------------------------------------------------

function TImpSchalter.FechaCupom( Mensagem:String ):String;
var

  sLinha    : String;
  sMsg , sMsgAux  : String;
  iLinha    : Integer;
  iRet      : Integer;
  nX        : Integer;
begin
    // La�o para imprimir toda a mensagem
    sMsg := '';
	sMsgAux := Mensagem;
	sMsgAux := TrataTags( sMsgAux );
    iLinha := 0;
    While ( Trim(sMsgAux)<>'' ) and ( iLinha < 8 ) do
      Begin
      sLinha := '';
      // La�o para pegar 40 caracter do Texto
      For nX:= 1 to 40 do
      Begin
        // Caso encontre um CHR(10) (Line Feed) imprime a linha
        If Copy(sMsgAux,nX,1) = #10 then
          Break;
        sLinha := sLinha + Copy(sMsgAux,nX,1);
      end;
      sLinha := Copy(sLinha+space(40),1,40);
      sMsg := sMsg + '0' + sLinha;
      sMsgAux := Copy(sMsgAux,nX+1,Length(sMsgAux));

      iRet := fFuncEcfImpLinha (Pchar( sLinha ) );

      inc(iLinha);
    End;

    iRet := fFuncEcfFimTrans('');

    if iRet = 0 then
    begin
        result := '0';
        iRet := fFuncEcfLineFeed(1,8);
    end
    else
    begin
        result := '0';
    end;

end;

//---------------------------------------------------------------------------

function TImpSchalter.AbreCupomNaoFiscal( Condicao,Valor,Totalizador, Texto:String ): String;

    function PegaRegistro( sCondicao:String):String;
    Var
        sRet    : String;
        aFormas : TaString;
        i       : Integer;
        pRet    : PChar;     // Retorna o Compravante n�o fiscal e suas propriedades
        pAux    : PChar;     // Verifica se o pRet retornou algum erro

    Begin
        result := '00';

        // Lendo todos as descri��es dos registradores.
        // Busca  em que posi��o est� cadastrado o documento n�o fiscal
        for i := 0 to 19 do
	    begin
            pRet := fFuncEcfStatusDocsNFs( i );

	        pAux := StrPos(pRet, 'Erro');
	        if pAux = nil then
	        begin

                //Verifica se o comprovante n�o fiscal cadastrado no Schalter.ini, est� cadastrado
                if UpperCase(  trim(copy(pRet,3,20)) ) = UpperCase ( trim(sCondicao) ) then
                begin
                    Result:= intToStr( i ) ;
                end;
            end;
        end;
   End;

var
  iRet      : Integer;          // Utilizado para obter o retorno da execucao dos comandos
  sRet      : String;
  sNumAnt   : String;           // �ltimo cupom impresso;
  i         : Integer;
  aFormas   : array of String;
  sFormas   : String;
  sPos      : String;           //Posi��o do totalizador n�o fiscal cadastrado na impressora;
  sPosTot   : String;
  iPos      : Integer;
  iTamanho  : Integer;
begin

    result := '1|';

    //Valor     := FormataTexto(Valor,9,2,2);
    Valor       := Copy( Valor, 1, Pos( '.', Valor) - 1 ) + Copy( Valor, Pos( '.', Valor) + 1, Length( Valor ) );
    Valor       := Copy( Valor, Length( Valor ) - 8, Length( Valor ) );

    If Trim(Totalizador)='' then
        Totalizador:='SIGALOJA';

    // Pegando o numero do ultimo cupom impresso
    //--------------------------------------------------------------
    // Pega o Numero do Ultimo Cupom impresso
    //--------------------------------------------------------------
    sRet := PegaCupom('');
    if Copy( sRet,1,1) = '0' then
    begin
        sNumAnt := Copy( sRet,3, length( sRet) );
        sNumAnt := FormataTexto( intToStr( strToInt(sNumAnt)-1 ) ,6,0,2 );

        // Monta o array aFormas com as condicoes de pagamento do cupom fiscal
        SetLength( aFormas,0 );

        sPos:= Totalizador;    // PegaRegistro(Totalizador);

        //------------------------------------------------------------------
        // Abrindo cupom nao fiscal vinculado
        //------------------------------------------------------------------
        sRet := ImpCabecNF();
        If sRet = '0' then
        begin
            iRet := fFuncEcfInicCNFVinculado( Pchar( sNumAnt ), Pchar( sPos ), Pchar( Valor ) );

            if iRet <> 0 then
            begin
                //----------------------------------------------------------------------------------
                // Abrindo cupom n�o fiscal n�o vinculado para que o pr�ximo seja vinculado a este
                //----------------------------------------------------------------------------------
                iRet := fFuncEcfInicCupomNFiscal(1);
                if iRet = 0 then
                begin
                    iRet := fFuncEcfVendaItem78 (Pchar(Texto),Pchar(valor), strToInt(sPos) );
                   
                    if iRet = 0 then
                    begin
                        // Faz o Fechamento do cupom
                        iRet := fFuncEcfFimTrans('');
                        if iRet = 0 then
                        begin
                            sNumAnt := intToStr( strToInt( sNumAnt) +1) ;
                            sNumAnt := FormataTexto( sNumAnt,4,2,2 );

                            sRet := ImpCabecNF();
                            If sRet = '0' then
                            begin
                                iRet := fFuncEcfInicCNFVinculado( Pchar( sNumAnt), Pchar(sPos), Pchar(Valor) );
                                if iRet = 0 then
                                begin
                                    Result := '0';
                                end
                                else
                                begin
                                    //------------------------------------------
                                    //Cancela o cupom
                                    //------------------------------------------
                                    iRet := fFuncEcfCancDoc('');
                                end;
                            end;
                        end;
                    end;
                end;
            end
            else
            begin
                Result := '0';
            end
        end;
    end;
end;

//----------------------------------------------------------------------------

function TImpSchalter.FechaEcf:String;
var
  iRet : Integer;

begin
    iRet := fFuncEcfReducaoZ('');

    if iRet <> 0 then
    begin
        Result := '1';
    end
    else
    begin
        Result := '0';
        iRet := fFuncEcfLineFeed(1,8);
    end;

end;

//----------------------------------------------------------------------------

function TImpSchalter.Suprimento( Tipo:Integer;Valor:String; Forma:String; Total:String; Modo:Integer; FormaSupr:String ):String;

var
  I         : Integer;
  iRet      : Integer;

  sRet      : String;
  sNomeCNF  : String;
  sTexto    : String;
  sForma    : String;
  sValor    : String;


  pRet : PChar;     // Retorna o Compravante n�o fiscal e suas propriedades
  pAux : PChar;     // Verifica se o pRet retornou algum erro


  aFormas : TaString;
  sFormas : String;


  sCondicao : String;

  sPosDep : String;
  sPosRet : String;


begin

    Result :='1|';

    If Tipo = 1 then
    begin

        sValor :=  FormataTexto(Valor,10,2,2);
        sFormas :='DINHEIRO';

        // Le as condicoes de pagamento cadastradas na impressora e guarda no array aFormas
        sRet := LeCondPag;
        sRet := Copy(sRet, 3, Length(sRet));
        MontaArray(sRet, aFormas);

        sPosRet := AchaPagto( sFormas,aFormas );

        //----------------------------------------------------------------------
        // Verifica o valor em dinheiro armazenado nas formas de pagamento
        //----------------------------------------------------------------------
        // Pesquisando antes de efetuar a transfer�ncia...

        pRet := fFuncEcfStatusPayTypes( strToInt(sPosRet) );
        pAux := StrPos(pRet, 'Erro');

        if pAux = nil then
        begin
            sRet := copy(pRet, 2, 1);
            if sRet = 'S' then
            begin
                sRet := copy(pRet, 24, 20);

                if StrToFloat(FormataTexto(sRet,10,2,2)) >= StrToFloat(FormataTexto(Valor,10,2,2)) then
                    result := '8'
                else
                    result := '9' ;
            end;
        end
    end
    else If Tipo = 2 then
    Begin
        sValor :=  FormataTexto(Valor,9,2,2);

        sNomeCNF := SigaLojaINI('SCHALTER.INI', 'Suprimento', 'NomeCNF', 'RECEBIMENTOS DIVERSO');
        sTexto   := SigaLojaINI('SCHALTER.INI', 'Suprimento', 'Texto',   'FUNDO DE CAIXA');  // Limitado a 70 caracteres

        // Busca  em que posi��o est� cadastrado o documento n�o fiscal de suprimento
        // Obrigatoriamente o parametro Permite pagamento = sim
        for i := 0 to 19 do
	    begin
            pRet := fFuncEcfStatusDocsNFs( i );

	        pAux := StrPos(pRet, 'Erro');
	        if pAux = nil then
	        begin

                //Verifica se o comprovante n�o fiscal cadastrado no Schalter.ini, est� cadastrado
                //na impressora.Posi��o 3, 20 = DESCRI��O DO COMPROVANTE N�O FISCAL
                if UpperCase(  trim(copy(pRet,3,20)) ) = UpperCase ( trim(sNomeCNF) ) then
                begin

	                if  ( copy(pRet, 2,1) = 'S' ) or     //Verifica na posi��o 2 se o documento n�o fiscal est� ativo na impressorathen
	                    ( copy(pRet,23,1) = 'N' ) or     //PERMITE DESCONTO = N
                        ( copy(pRet,24,1) = 'N' ) or     //PERMITE ACRESCIMO =N
                        ( copy(pRet,25,1) = 'N' ) or     //PERMITE CANCELAMENTO =N
                        ( copy(pRet,26,1) = 'S' ) or     //PERMITE PAGAMENTO = "S"
                        ( copy(pRet,27,1) = 'N' ) or     //� VINCULADO = N
                        ( copy(pRet,28,2) = '00') then  //FORMA AO QUAL � VINCULADO ='00'
                    begin
                        //imprime cabe�alho n�o fiscal

                        sRet := ImpCabecNF();
                        if sRet = '0' then
                        begin
                            //Imprime o In�cio do cupom n�o fiscal
                            iRet := fFuncEcfInicCupomNFiscal(1);

                            if iRet = 0 then
                            begin
                                //imprime o valor do suprimento
                                iRet := fFuncEcfVendaItem78(Pchar(sTexto),Pchar(sValor), i);

                                if iRet = 0 then
                                begin
                                    // Faz o Fechamento do cupom
                                    iRet := fFuncEcfFimTrans('');

                                    if iRet = 0 then
                                    begin
                                        result := '0';
                                        break;
                                    end;
                                end;
                            end;
                        end;
                    end;
                end;
            end;
        end;
    end
    else if Tipo = 3 then
    begin

        sValor :=  FormataTexto(Valor,21,2,2);

        sNomeCNF := SigaLojaINI('SCHALTER.INI', 'Sangria', 'NomeCNF', 'SANGRIA');
        sTexto   := SigaLojaINI('SCHALTER.INI', 'Sangria', 'Texto',   'SANGRIA');  // Limitado a 70 caracteres

        sFormas :='DINHEIRO';

        // Le as condicoes de pagamento cadastradas na impressora e guarda no array aFormas
        sRet := LeCondPag;
        sRet := Copy(sRet, 3, Length(sRet));
        MontaArray(sRet, aFormas);

        // Processo de Sangria: Na Schalter para fazer sangria � preciso criar uma forma de pagamento
        // com o nome de sangria ("ou outro qualquer") e fazer a transfer�ncia de valores da forma de
        // pagamento que deseja sangrar para a forma de pagamento Sangria.

        // Monta um array auxiliar com os parametros:
        //                                - Pagamento de Retirada;
        //                                - Pagamento de Dep�sito;


        // Verifica se a existe a condi��o de pagamento para sangria
        // conforme configura��o do arquivo schalter.ini na Se��o: Sangria.

        //AchaPagto( aAuxiliar[i],aFormas ) <> '00'
        //sValor := FormataTexto(aAuxiliar[i+1],10,2,2);
        sPosDep := AchaPagto( sNomeCNF,aFormas );
        sPosRet := AchaPagto( sFormas,aFormas );

        //----------------------------------------------------------------------
        // Verifica antes de executar o comando
        //----------------------------------------------------------------------
        // Pesquisando antes de efetuar a transfer�ncia...

        pRet := fFuncEcfStatusPayTypes( strToInt(sPosRet) );
        pAux := StrPos(pRet, 'Erro');

        if pAux = nil then
        begin
            sRet := copy(pRet, 2, 1);
            if sRet = 'N' then
            begin
                //showmessage('O pagamento de retirada n�o est� ativo.');
                result := '1';
            end
            else
            begin
                pRet := fFuncEcfStatusPayTypes( strtoint(sPosDep) );
                pAux := StrPos(pRet, 'Erro');

                if pAux = nil then
                begin
                    sRet := copy(pRet, 2, 1);
                    if sRet = 'N' then
                    begin
                        //showmessage('O pagamento de dep�sito n�o est� ativo.');
                        result := '1';
                    end
                    else
                    begin
                        iRet := fFuncEcfPagTransfer(Pchar(sPosRet), Pchar(sPosDep), Pchar(sValor) );

                        if iRet <> 0 then
                        begin
                            if (iRet = 70) then
                            begin
                                //showmessage('Verifique o valor do montante de transfer�ncia e ' +
                                //            'certifique-se de que h�, no m�nimo, este valor na ' +
                                //            'forma de pagamento de retirada');
                                result := '1|';
                            end
                            else
                            begin
                                result := '1';
                            end;
                        end
                        else
                        begin
                            // Faz o Fechamento do cupom
                            iRet := fFuncEcfFimTrans('');

                            if iRet = 0 then
                                result := '0'
                            else
                                Result := '1';
                        end;
                    end;
                end;
            end;
        end
        else
        begin
            //rets :=  strtoint( copy(retStatPay,7,3) );
            result := '1';
        end;
    end;
end;

//----------------------------------------------------------------------------

function TImpSchalter.ReImpCupomNaoFiscal( Texto:String ):String;
begin
    //para posterior implementacao
    result := '1';
end;

//----------------------------------------------------------------------------
function TImpSchalter.Gaveta:String;
var
  iRet : Integer;
begin
  iRet := fFuncEcfAbreGaveta();

  If iRet >= 0 then
    Result := '0'
  Else
    Result := '1';

end;

//----------------------------------------------------------------------------

function TImpSchalter.RecebNFis( Totalizador, Valor, Forma:String ): String;

    function PegaRegistro( sCondicao:String):String;
    Var
        sRet    : String;
        aFormas : TaString;
        i       : Integer;
        pRet : PChar;     // Retorna o Compravante n�o fiscal e suas propriedades
        pAux : PChar;     // Verifica se o pRet retornou algum erro

    Begin
        // Lendo todos as descri��es dos registradores.
        // Busca  em que posi��o est� cadastrado o documento n�o fiscal
        Result:= '';
        for i := 0 to 19 do
	    begin
            pRet := fFuncEcfStatusDocsNFs( i );

	        pAux := StrPos(pRet, 'Erro');
	        if pAux = nil then
	        begin

                //Verifica se o comprovante n�o fiscal cadastrado no Schalter.ini, est� cadastrado
                if UpperCase(  trim(copy(pRet,3,20)) ) = UpperCase ( trim(sCondicao) ) then
                begin
                    Result:= intToStr( i ) ;
                    break;
                end;
            end;
        end;
   End;
//**********************************************************************************************************
Var
  sRet      : String;
  iRet      : Integer;
  i         : Integer;
  aFormas   : TaString;
  sFormas   : String;
  sPos      : String;
  iTamanho  : Integer;
  sCondicao : String;
  aPgto     : TaString;
  sNomeCNF  : String;
  sTexto    : String;
begin

    result := '1|';

    Valor     := FormataTexto(Valor,9,2,2);
    sCondicao := Forma;

    // Le as condicoes de pagamento
    sRet := LeCondPag;
    sRet := Copy(sRet, 3, Length(sRet));
    MontaArray( sRet, aFormas );

    sNomeCNF:= SigaLojaINI('SCHALTER.INI', 'Recebimento', 'NomeCNFNV', 'RECEBIMENTO');
    sTexto    := SigaLojaINI('SCHALTER.INI', 'Recebimento', 'Texto',   'RECEBIMENTO');  // Limitado a 70 caracteres

    sPos:=PegaRegistro( sNomeCNF );

    if Trim(sPos) <> '' then
    begin
      //Imprime Cabe�alho N�o Fiscal
      sRet := ImpCabecNF();
      If sRet = '0' then
      begin
        //Inicializa cupom n�o fiscal
        iRet := fFuncEcfInicCupomNFiscal(1);
        if iRet = 0 then
        begin
          //Registra o �tem no cupom n�o fiscal n�o vinculado
          iRet := fFuncEcfVendaItem78(Pchar(sTexto),Pchar(valor), strToInt(sPos) );
          if iRet = 0 then
          Begin
             iRet := fFuncEcfPagamento( 0, Pchar( AchaPagto( Forma, aFormas ) ), Pchar( FormataTexto( Valor, 10, 2, 0 ) ), 0);
             If sRet = '0' Then
            Begin
              sRet := FechaCupomNaoFiscal;
              result := '0|';
            End;
          end;
        end;
      end;
    end;
end;

//------------------------------------------------------------------------------

function TImpSchalter.TextoNaoFiscal( Texto:String;Vias:Integer ):String;
var
  i: Integer;
  sTexto  : String;
  iRet    : Integer;
  sLinha  :String;
Begin
  Result := '0';
  if Vias > 1 then
  Begin
    sTexto := Texto;
    i:=1;
    While i < Vias do
    Begin
        Texto:= Texto+ sTexto;
        Inc(i);
    End;
  End;
  // La�o para imprimir toda a mensagem
  While ( Trim(Texto)<>'' ) do
      Begin
        sLinha := '';
         // La�o para pegar 40 caracter do Texto
         If Pos(#10,Copy(Texto,1,41))>1 then
         begin
            sLinha := Copy(Texto,1, Pos(#10,Texto)-1);
            Texto  := Copy(Texto,Pos(#10,Texto)+1, Length(Texto));
            iRet   := fFuncEcfImpLinha( Pchar( sLinha )  );

         End
         Else If Pos(#10,Copy(Texto,1,41))=1 then
         Begin
            Texto := Copy(Texto,2,Length(Texto));
            iRet := fFuncEcfImpLinha( Pchar( Space( 40 ) )  );
         End
         Else
         Begin
            sLinha := Copy(Texto,1, 40);
            Texto  := Copy(Texto,41, Length(Texto));
            iRet   := fFuncEcfImpLinha( Pchar( sLinha )  );
         End;

         // Ocorreu erro na impress�o do cupom
         if iRet <> 0 then
         Begin
            Result := '1';
            Break;
         End;
      End;
end;

//----------------------------------------------------------------------------
function TImpSchalter.RelGerInd( cIndTotalizador , cTextoImp : String ; nVias : Integer; ImgQrCode: String) : String;
begin
  Result := RelatorioGerencial(cTextoImp , nVias,ImgQrCode);
end;

//----------------------------------------------------------------------------
function TImpSchalter.RelatorioGerencial( Texto:String ;Vias:Integer; ImgQrCode: String):String;
var
  iRet, i : Integer;
  sTexto  : String;
  sLinha  :String;

begin

    Result := '0';

    // Fecha o cupom n�o fiscal
    iRet := fFuncEcfFimTrans('');

    //--------------------------------------------------------------------------
    // Para imprimir o Relat�rio gerencial, deve se antes emitir uma leitura X
    // ent�o logo ap�s o ECF deixa imprir  texto livre durante 10 minutos.
    //--------------------------------------------------------------------------
    iRet := fFuncEcfLXGerencial('');

    if iRet = 0 then
    begin

        if Vias > 1 then
        Begin
            sTexto := Texto;
            i:=1;
            While i < Vias do
            Begin
                Texto:= Texto+ sTexto;
                Inc(i);
            End;
        End;
        // La�o para imprimir toda a mensagem
        While ( Trim(Texto)<>'' ) do
        Begin
            sLinha := '';
            // La�o para pegar 40 caracter do Texto
            For i:= 1 to 40 do
            Begin
                // Caso encontre um CHR(10) (Line Feed) imprime a linha
                If Copy(Texto,i,1) = #10 then
                    Break;
                sLinha := sLinha + Copy(Texto,i,1);
            end;
            sLinha := Copy(sLinha+space(40),1,40);
            Texto  := Copy(Texto,i+1,Length(Texto));
            iRet   := fFuncEcfImpLinha( Pchar( sLinha )  );

            // Ocorreu erro na impress�o do cupom
            if iRet<>0 then
            Begin
                Result := '1';
                // Fecha o cupom n�o fiscal
                iRet := fFuncEcfFimTrans('');
                Exit;
            End;
        End;
    end
    else
    begin
        // Fecha o cupom n�o fiscal
        Result := '1';
        iRet := fFuncEcfFimTrans('');
    end;

    // Fecha o cupom n�o fiscal
    iRet := fFuncEcfFimTrans('');

    if iRet <> 0 then
        result := '1';

end;

//----------------------------------------------------------------------------
function TImpSchalter.ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : String ; Vias : Integer):String;

begin
  WriteLog('SIGALOJA.LOG','Comando n�o suportado para este modelo!');
  Result := '0';
end;

//----------------------------------------------------------------------------
function TImpSchalter.FechaCupomNaoFiscal: String;
var
  iRet : Integer;
begin
    // Faz o Fechamento do cupom
    iRet := fFuncEcfFimTrans('');

    If iRet = 0 then
        Result := '0'
    Else
        Result := '1';
end;

//----------------------------------------------------------------------------
function TImpSchalter.AdicionaAliquota( Aliquota:String; Tipo:Integer ): String;
// Tipo = 1 - ICMS
// Tipo = 2 - ISS
var
  iRet  : Integer;
  i     : Integer;
  pRet  : PChar;
  sAliq : String;
  sPos  : String;
  sTipo : String;

begin

    result := '1';

    If Tipo=1 then sTipo := 'T';    //T significa Aliquota de ICMS
    If Tipo=2 then sTipo := 'S';    //S significa Aliquota de ISS

    Aliquota := FormataTexto(Aliquota,4,2,2);

    //--------------------------------------------------------------------------
    // Procura uma posi��o livre na tabela para inserir a nova al�quota;
    //--------------------------------------------------------------------------
    for i := 1 to 15 do
    begin
        pRet := fFuncEcfStatusAliquotas( i );
        if Copy(pRet,1,5) <> 'Erro:' then
        begin
            sAliq := copy(pRet,3,5);
            if (FormataTexto(sAliq,4,2,2) = '0000') then
            begin
                sPos :=  FormataTexto(intToStr(i),2,0,2);
                iRet := fFuncEcfCargaAliqSelect ( Pchar(sPos) , Pchar(sTipo), Pchar( Aliquota) );
                If iRet = 0 then
                    Result := '0'
                Else
                    Result := '1';

                exit;
            end;
        end;
    end;
end;

//----------------------------------------------------------------------------
function TImpSchalter.GravaCondPag( condicao:string ) : String;
var
    iRet : Integer;
begin
    ShowMessage(' Para cadastrar Condi��o de pagamento, somente com interven��o t�cnica.');
    Result := '0';

end;

//------------------------------------------------------------------------------

function TImpSchalter.HorarioVerao( Tipo:String ):String;
var
iRet    : Word;
iDia    : Word;
iMes    : Word;
iAno    : Word;
iHor    : Word;
iMin    : Word;
iSeg    : Word;
tData   : TDateTime;
sRet    : String;

begin

    result :='1';

    //Armazena a Data
    sRet := StatusImp(2);
    if Copy(sRet,1,4) <> 'Erro' then;
    begin
        iDia  := strToInt( copy(sRet,3,2) );
        iMes  := strToInt( copy(sRet,6,2) );
        iAno  := strToInt( copy(sRet,9,2) );


        //Armazena a hora
        sRet := StatusImp(1);
        if Copy(sRet,1,4) <> 'Erro' then;
        begin
            iHor  := strToInt( copy(sRet,3,2) );
            iMin  := strToInt( copy(sRet,6,2) );
            iSeg  := strToInt( copy(sRet,9,2) );
        end;

        if Tipo = '+' then
            iHor := iHor + 1
        else
            iHor := iHor - 1;

        iRet := fFuncEcfAcertaData (iDia, iMes, iAno, iHor, iMin, iSeg );
        if iRet = 0 then
            Result := '0'
        else
            Result := '1';

    end;

end;



//------------------------------------------------------------------------------

function TImpSchalter.TotalizadorNaoFiscal( Numero,Descricao:String ):String;
begin

    ShowMessage(' Para cadastrar Totalizador n�o Fiscal, somente com interven��o t�cnica.');
    Result := '1';

end;

//------------------------------------------------------------------------------

function TImpSchalter.Autenticacao( Vezes:Integer; Valor,Texto:String ): String;
var
  iRet : Integer;
  i    : Integer;
begin
    result := '1';

    for i:=0 to 5 do
    begin
        iRet := fFuncEcfAutentica(PChar(Texto));
        if iRet = 0 then
        begin
            Result := '0';
            break;
        end
        Else
        begin
            ShowMessage('Posicione o papel para autentica��o e pressione OK.');
            Result := '1';
        end;
    end;

    if iRet <> 0 then
    begin
        //------------------------------
        // Faz o cancelamento da venda -
        //------------------------------
        iRet := fFuncEcfCancVenda('');
    end
    else
    begin
        // Faz o Fechamento do cupom
        iRet := fFuncEcfFimTrans('');
    end

end;


//---------------------------------------------------------------------------
function TImpSchalter.ImpostosCupom(Texto: String): String;
begin
  Result := '0';
end;

//----------------------------------------------------------------------------
function TImpSchalter.DownloadMFD( sTipo, sInicio, sFinal : String ):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpSchalter.GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario : String ):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;


//------------------------------------------------------------------------------
function TImpSchalter.LeTotNFisc:String;
begin
        Result := '0|-99';
end;

//------------------------------------------------------------------------------
function TImpSchalter.DownMF(sTipo, sInicio, sFinal : String):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpSchalter.RedZDado( MapaRes : String): String ;
Begin
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpSchalter.IdCliente( cCPFCNPJ , cCliente , cEndereco : String ): String;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - IdCliente : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpSchalter.EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : String ) : String;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - EstornNFiscVinc : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpSchalter.ImpTxtFis(Texto : String) : String;
begin
 GravaLog(' - ImpTxtFis : Comando N�o Implementado para este modelo');
 Result := '0';
end;

//------------------------------------------------------------------------------
Function TrataTags( Mensagem : String ) : String;
var
  cMsg : String;
begin
cMsg := Mensagem;
cMsg := RemoveTags( cMsg );
Result := cMsg;
end;

//------------------------------------------------------------------------------
function TImpSchalter.GrvQrCode(SavePath, QrCode: String): String;
begin
GravaLog(' GrvQrCode - n�o implementado para esse modelo ');
Result := '0';
end;

initialization
  //Vers�o de Eprom 3.03
  RegistraImpressora('SCHALTER SCFI IE - V. 03.03' , TImpSchalter, 'BRA', '340302');
  RegistraImpressora('SCHALTER SCFI IE - V. 03.06' , TImpSchalter, 'BRA', '340304');
end.

