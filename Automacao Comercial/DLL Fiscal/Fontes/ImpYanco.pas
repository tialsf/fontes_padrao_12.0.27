unit ImpYanco;
interface

Uses
  Dialogs,
  ImpFiscMain,
  Windows,
  SysUtils,
  classes,
  LojxFun,
  Forms,
  CommInt;

Type
  TYanco = class(TCustomComm)
  protected
      procedure Comm1RxChar(Sender: TObject; Count: Integer);
      procedure Comm1Error(Sender: TObject; Errors: Integer);
  public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
  end;

  TImpFiscalYanco = class(TImpressoraFiscal)
  public
    function Abrir( sPorta:String; iHdlMain:Integer ):String; override;
    function Fechar( sPorta:String ):String; override;
    function LeituraX:String; override;
    function PegaCupom(Cancelamento:String):String; override;
    function PegaPDV:String; override;
    function LeAliquotas:String; override;
    function LeAliquotasISS:String; override;
    function LeCondPag:String; override;
    function AdicionaAliquota( Aliquota:String; Tipo:Integer ): String; override;
    function GravaCondPag( condicao:String ):String; override;
    function AbreCupom(Cliente:String; MensagemRodape:String):String; override;
    function AbreEcf:String; override;
    function RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:String; nTipoImp:Integer ): String; override;
    function Pagamento( Pagamento,Vinculado,Percepcion:String ): String; override;
    function FechaCupom( Mensagem:String ):String; override;
    function DescontoTotal( vlrDesconto:String ;nTipoImp:Integer): String; override;
    function AcrescimoTotal( vlrAcrescimo:String ): String; override;
    function CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:String ):String; override;
    function CancelaCupom( Supervisor:String ):String; override;
    function StatusImp( Tipo:Integer ):String; override;
    function ReducaoZ( MapaRes:String ):String; override;
    function ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : String ; Vias : Integer ) : String; Override;
    function TotalizadorNaoFiscal( Numero,Descricao:String ):String; override;
    function AbreCupomNaoFiscal( Condicao,Valor,Totalizador,Texto:String ): String; override;
    function Autenticacao( Vezes:Integer; Valor,Texto:String ): String; override;
    function FechaCupomNaoFiscal: String; override;
    function TextoNaoFiscal( Texto:String;Vias:Integer ):String; override;
    function MemoriaFiscal( DataInicio,DataFim:TDateTime;ReducInicio,ReducFim,Tipo:String ): String; override;
    function Suprimento( Tipo:Integer;Valor:String;Forma:String; Total:String; Modo:Integer; FormaSupr:String ):String; override;
    procedure AlimentaProperties; override;
    function PegaSerie:String; override;
    function ImpostosCupom(Texto: String): String; override;
    function Pedido( Totalizador, Tef, Texto, Valor, CondPagTef:String ): String; override;        
    function RecebNFis( Totalizador, Valor, Forma:String ): String; override;
    function DownloadMFD( sTipo, sInicio, sFinal : String ):String; override;
    function GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario : String ):String; override;
    function RelGerInd( cIndTotalizador , cTextoImp : String ; nVias : Integer; ImgQrCode: String) : String; override;
    function LeTotNFisc:String; override;
    function DownMF(sTipo, sInicio, sFinal : String):String ; override;
    function RedZDado( MapaRes:String ):String; override;
    function IdCliente( cCPFCNPJ , cCliente , cEndereco : String ): String; Override;
    function EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : String ) : String; Override;
    function ImpTxtFis(Texto : String) : String; Override;
    function GrvQrCode(SavePath,QrCode: String): String; Override;
  end;

  Function FncSequencia:String;
  function EnviaComando(sCmd:String): String;
  Function TrataTags( Mensagem : String ) : String;

//----------------------------------------------------------------------------
implementation
//----------------------------------------------------------------------------
var
  Comm1 : TYanco;
  sRetorno : String;
  bRet : Boolean;

//------------------------------------------------------------------------------
constructor TYanco.Create(AOwner: TComponent);
begin
  inherited;
end;

//------------------------------------------------------------------------------
destructor TYanco.Destroy;
begin
  inherited;
end;
//------------------------------------------------------------------------------
function TImpFiscalYanco.Abrir(sPorta : String; iHdlMain:Integer) : String;
begin
  Comm1 := TYanco.Create(Application);
  Comm1.OnRxChar := Comm1.Comm1RxChar;
  Comm1.BaudRate := br9600;
  Comm1.Databits := da8;
  Comm1.Parity   := paNone;
  Comm1.StopBits := sb10;
  Comm1.DeviceName := sPorta;
  try
    //Abre a porta serial
    Comm1.Open;
    Comm1.SetRTSState(True);
    Comm1.SetDTRState(True);
    Comm1.SetBREAKState(False);
    Comm1.SetXONState(True);
    result := '0';
    AlimentaProperties;
  except
    result := '1';
  end;
end;

//---------------------------------------------------------------------------
function TImpFiscalYanco.Fechar( sPorta:String ) : String;
begin
 //Fecha porta serial
  Comm1.Close;
  Comm1.Free;
  result := '0|';
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.LeituraX: String;
var
  sRet : String;
begin
  sRet:=EnviaComando( '241             ');
  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.PegaCupom(Cancelamento:String):String;
var
  sRet,sCup : String;
begin
  sCup:='0000';
  sRet := EnviaComando( '2E');
  sRet:=copy(sRet,766,6);
  if StrToInt(sRet)>0 then
     Begin
     sCup:=sRet;
     sRet:=StatusImp( 5 );
     if Copy(sRet,1,1)='0' then
        sCup:=FormataTexto( IntToStr(StrToInt(sCup)-1),6,0,2);

     result :='0|'+sCup;
     end
  else
     result := '1|'
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.PegaPDV:String;
begin
  result :='0|'+PDV
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.LeAliquotas:String;
Begin
  result :='0|'+ICMS;
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.LeAliquotasISS:String;
Begin
  result :='0|'+ISS;
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.LeCondPag:String;
Begin
  Result := '0|'+FormasPgto;
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.ImpostosCupom(Texto: String): String;
begin
  Result := '0';
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.AdicionaAliquota( Aliquota:String; Tipo:Integer ): String;
var
 sAliq : String;
 sRet  : String;
 sAux  : String;
begin
  { Colocar a chamada da funcao de aliquota separadas por tipo, ou seja ICMS E TRATA-LA COMO SE FOSSO TODAS}
  Aliquota := FormataTexto(Aliquota,4,2,2);
  if Tipo=1 then
     Aliquota:='T'+Aliquota
  else
     Aliquota:='S'+Aliquota;
  sRet:='';

  sRet   := EnviaComando( '2E');
  if Trim(sRet)='' then
     Begin
     result := '1|';
     exit;
     end;

  sAux   :=copy(sRet,7,80);
  sAliq  :='';
  While Trim(sAux)<>'' do
     begin
     if copy(sAux,1,5)=Aliquota then
        begin
        result := '1';
        ShowMessage('Al�quota j� cadastrada.');
        exit;
        end;
     if ( StrToInt(copy(sAux,2,4))=0 ) and ( Trim(Aliquota)<>'' ) and ( Pos( copy(sAux,1,1),'FN' )=0 ) then
        Begin
        sAliq:=sAliq+Aliquota;
        Aliquota:='';
        end
     else
        sAliq:=sAliq+copy(sAux,1,5);

     sAux:=copy(sAux,6,length(sAux));
     end;

  sRet :='11';
  if Trim(Aliquota)<>'' then
     begin
     ShowMessage('N�o h� espa�o para cadastros de aliquotas');
     Result:='1';
     exit;
     end
  else
     sRet := EnviaComando( '04'+sAliq);

  if copy(sRet,7,2)='00' then
     Begin
     result := '0|';
     AlimentaProperties;
     end
  else
     result := '1|'
end;
//---------------------------------------------------------------------------
function TImpFiscalYanco.GravaCondPag( condicao:String ):String;
Var
  sRet     : String;
  sCond    : String;
  sAux     : String;
  sForma   : String;
  begin

  sRet   := EnviaComando( '2E');
  if Trim(sRet)='' then
     Begin
     result := '1|';
     exit;
     end;

  sAux     :=copy(sRet,104,450);
  sCond    :='';
  Condicao :=Trim(UpperCase(Condicao));
  While Trim(sAux)<>'' do
     begin
     sForma:=Trim(UpperCase(copy(sAux,1,15)));
     if sForma=Condicao then
        begin
        result := '1';
        ShowMessage('Forma de pagamento j� cadastrada.');
        exit;
        end
     else
       if ( sForma='---' ) and ( Trim(Condicao)<>'' )  then
          Begin
          sCond:=sCond+Copy(Condicao+space(16),1,15);
          Condicao:='';
          end
       else
          sCond:=sCond+Copy(sForma+space(16),1,15);

        sAux:=copy(sAux,16,length(sAux));
     end;
  if Trim(Condicao)<>'' then
     begin
     ShowMessage('N�o h� espa�o para cadastros de Formas de pagamentos');
     Result:='1';
     exit;
     end
  else
     sRet := EnviaComando( '0E'+sCond);

  if copy(sRet,7,2)='00' then
     Begin
     result := '0|';
     AlimentaProperties;
     end
  else
     result := '1|'

end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.AbreCupom(Cliente:String; MensagemRodape:String):String;
var
  sRet : String;
begin
  sRet := EnviaComando( '130');

  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//----------------------------------------------------------------------------

function TImpFiscalYanco.AbreEcf:String;
begin
  //Funcao Inicio do Dia
  EnviaComando( '121             ');
  result := '0|';
End;
//----------------------------------------------------------------------------
function TImpFiscalYanco.RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:String; nTipoImp:Integer ): String ;
var
  sRet : String;
  sTot : String;
  sTipo: String;
  sAux : String;
  iPos : Integer;
begin
  //verifica se � para registra a venda do item ou s� o desconto
  if ( Trim(codigo+descricao)='') and ( StrToFloat(vlrdesconto)>0.00 ) then
     begin
     vlrDesconto:=FormataTexto(vlrDesconto,12,3,2);
     sRet := EnviaComando( '18'+vlrDesconto+'00000');
     if copy(sRet,7,2)='00' then
        result := '0|'
     else
        result := '1|';
     exit;
     end;

  Codigo   :=Copy(Codigo+space(13),1,13);
  Descricao:=Copy(Descricao+space(30),1,30);
  sTot     := FloatToStr(StrToFloat(vlrUnit)*StrToFloat(qtde));
  if Pos('.',sTot)=0 then
     sTot:=sTot+'.00';

  vlrDesconto:=FormataTexto(vlrDesconto,12,3,2);
  vlrUnit  := FormataTexto(vlrUnit,9,3,2);

  qtde := FormataTexto(qtde,7,3,2);

  sTot := FormataTexto(sTot,12,3,2);

  sTipo:= Copy(Aliquota,1,1);
  Aliquota := Copy(Aliquota,2,5);
  if trim(aliquota)='' then
     aliquota :='0000'
  Else
     Aliquota := FormataTexto(Aliquota,4,2,2);

  Aliquota:=sTipo+Aliquota;
  sRet:='';
   //Pega variavel alimentada pela fun��o proprietes
  sAux   :=Aliquotas;
  sRet:='';
  if Trim(sAux)='' then
     Begin
     result := '1|';
     exit;
     end;
  sRet   := '11';
  iPos   := 0;
  Result := '0';
  While Trim(sAux)<>'' do
     begin
     Inc(iPos);
     if copy(sAux,1,5)=Aliquota then
        begin
        Result:= IntToStr(iPos);
        Break;
        end;

     sAux:=copy(sAux,6,length(sAux));
     end;

  if Result='0'  then
     begin
     ShowMessage('Aliquota n�o cadastrada');
     Result:='1';
     exit;
     end
  else
     Begin
     sTipo:=FormataTexto(Result,2,0,2);
     sRet := EnviaComando( '14'+Codigo+Descricao+vlrUnit+qtde+sTot+sTipo+'0');
     if ( copy(sRet,7,2)='00' ) and (  StrToInt(vlrDesconto)>0 ) then
        sRet := EnviaComando( '18'+vlrDesconto+'00000')

     end;

  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.Pagamento( Pagamento,Vinculado,Percepcion:String ): String;
var
  sRet,sAux,sVal,sForma : String;
  aAuxiliar      : TaString;
  iPos,i         : Integer;
begin
  // registra as formas de pagamento
  MontaArray( Pagamento,aAuxiliar );
  sForma := FormasPgto;
  sRet:='';
  if Trim(sForma)='' then
     Begin
     result := '1|';
     exit;
     end;

  for i:= 0 to high(aAuxiliar) do
      Begin
      iPos:=0;
      sAux:=sForma;
      While ( Trim(sAux)<>'' ) and (i mod 2 = 0) do
         begin
         Inc(iPos);
         if UpperCase(Trim(Copy(sAux,1,Pos('|',sAux)-1)))=UpperCase(Trim(aAuxiliar[i])) then
             Begin
             sRet:='11';
             sVal:=FormataTexto(aAuxiliar[i+1],13,2,1);
             sVal:=StrTran(sVal,'.','');
             sRet := EnviaComando( '15'+FormataTexto(IntToStr(iPos),2,0,2)+sVal);
             if copy(sRet,7,2)<>'00' then
                Begin
                result := '1|';
                exit;
                end;

             Break;
             end;
         sAux:=Copy(sAux,Pos('|',sAux)+1,Length(sAux))
         end;
      end;
  result :='0|';
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.FechaCupom( Mensagem:String ):String;
var
  sRet : String;
  cMsg : String ;
begin
  If Trim(Mensagem) <> '' then
  begin
    cMsg := Mensagem;
    cMsg := TrataTags( cMsg );
  end;

  cMsg:=Copy(cMsg+space(168),1,168);  
  sRet := EnviaComando( '1600'+cMsg);

  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.DescontoTotal( vlrDesconto:String ;nTipoImp:Integer): String;
var
  sRet : String;
begin
  vlrDesconto:=FormataTexto(vlrDesconto,13,3,2);
  if StrToInt(vlrDesconto)>0 then
     begin
     sRet := EnviaComando( '1B'+vlrDesconto+'00000');
     if copy(sRet,7,2)='00' then
        result := '0|'
     else
        result := '1|';
     end
  else
     result := '0|';

end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.AcrescimoTotal( vlrAcrescimo:String ): String;
var
  sRet : String;
begin
  vlrAcrescimo:=FormataTexto(vlrAcrescimo,13,3,2);
  if StrToInt(vlrAcrescimo)>0 then
     Begin
     sRet := EnviaComando( '1B'+vlrAcrescimo+'00002');
     if copy(sRet,7,2)='00' then
        result := '0|'
     else
        result := '1|';
     end
  else
     result := '0|';
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:String ):String;
var
  sRet : String;
begin
  NumItem:=FormataTexto(NumItem,3,0,2);
  Descricao:=Copy(Descricao+space(22),1,22);
  sRet := EnviaComando( '35'+NumItem+Descricao);
  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.CancelaCupom( Supervisor:String ):String;
var
  sRet : String;
begin
  sRet:=StatusImp( 5 );
  if Copy(sRet,1,1)='1' then
     Begin
     sRet := EnviaComando( '1D');
     if copy(sRet,7,2)='00' then
        result := '0|'
      else
         result := '1|'
      end
  Else
     Begin
     sRet := EnviaComando( '1E');
     if copy(sRet,7,2)='00' then
        result := '0|'
     else
        result := '1|'
     end;
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.ReducaoZ( MapaRes:String ):String;
var
  sRet : String;
begin
  //Redu��o Z
  sRet:=EnviaComando( '22             ');
  if copy(sRet,7,2)='00' then
     Begin
     // Inicio do Dia.
     sRet := EnviaComando( '121             ');
     if copy(sRet,7,2)='00' then
        result := '0|'
     else
        result := '1|'
     end
  else
     result := '1|';
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : String ; Vias : Integer):String;

begin
  WriteLog('SIGALOJA.LOG','Comando n�o suportado para este modelo!');
  Result := '0';
end;

//---------------------------------------------------------------------------
function TImpFiscalYanco.TotalizadorNaoFiscal( Numero,Descricao:String ):String;
var
  sRet : String;
begin
  if ( StrToInt(Numero) < 4 ) and ( StrToInt(Numero) > 8 ) then
     Begin
     ShowMessage('Utilize os registradores de 5 � 8');
     Result:='1|';
     exit;
     end;
  if ( Trim(Descricao)= '' ) then
     Begin
     ShowMessage('Defina uma descri��o para o totalizador');
     Result:='1|';
     exit;
     end;

  Numero   :=FormataTexto(Numero,1,0,2);
  Descricao:=Copy(Descricao+space(15),1,15);

  sRet:=EnviaComando( '10'+Numero+Descricao );
  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.AbreCupomNaoFiscal(Condicao,Valor,Totalizador,Texto:String ): String;
var
    sRet,sAux,sCondicoes : String;
    iCont : Integer;
    bOk : Boolean;
begin
    //Definicao de variaveis
    bOk        := True;
    iCont      := -1;
    sCondicoes := LeCondPag;

    while bOk do
    Begin

      //Verifico se existe condicao de pagamento cadastrada na impressora
      If Pos('|',sCondicoes) <= 0 Then
      Begin
        ShowMessage('Forma de Pagento n�o cadastrada na impressora fiscal.');
        result := '1|';
        bOk := False;
      End;

      //Verifico se a condicao no qual foi digitado, esta cadastrada na impressora
      //para que eu saiba sua sequencia e envie para a impressora.
      iCont := iCont +1 ;
      If Copy(sCondicoes,1,Pos('|',sCondicoes)-1) = UpperCase(Trim(Condicao)) Then
      Begin
        Totalizador:=FormataTexto(Totalizador,2,0,2);
        Valor:= StrTran(Valor,'.','');
        Valor:= FormataTexto(Valor,13,1,2);

        //Preencho com zero a esquerda (2 digitos).
        If iCont < 10 Then
          sAux := '0'+IntToStr(iCont)
        Else
          sAux := IntToStr(iCont);

        //Envio o comando para a impressora
        sRet:=EnviaComando( '29'+Totalizador+sAux+Valor );

        //Se eu obter um resultado positivo da impressora
        if copy(sRet,7,2)='00' then
            result := '0|'
        else
            result := '1|';

        //Apos a execucao, nao preciso mais realizar um loop
        bOk := False
      End
      Else
        //Caso nao seja a condicao no qual o usuario digitou, retiro a mesma
        //da string para realizar uma nova verificacao logo acima.
        sCondicoes := Copy(sCondicoes,Pos('|',sCondicoes)+1,Length(sCondicoes))
    End;
End;
//----------------------------------------------------------------------------
function TImpFiscalYanco.Autenticacao( Vezes:Integer; Valor,Texto:String ): String;
var
  sRet : String;
  i    : Integer;
begin
  if Vezes>4 then
     Begin
     ShowMessage('Maximo de Autentica��o � 4');
     Result:='1|';
     exit;
     end;

  For i:= 1 to Vezes do
     Begin
     ShowMessage('Posicione o Documento para '+IntToStr(i)+'a. Autentica��o');
     sRet:=EnviaComando( '30');
     if copy(sRet,7,2)<>'00' then
        Begin
        result := '1|';
        exit;
        end;
     end;
  result := '0|'
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.FechaCupomNaoFiscal: String;
Begin
  result := '0|';
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.TextoNaoFiscal(Texto:String;Vias:Integer  ):String;
var
  sRet   : String;
  i      : Integer;
  sLinha : String;
begin
  i:=1;
  sLinha := '';
  while i <= Length(Texto) do
  begin
    if (copy(Texto,i,1) = #10) or (Length(sLinha)>=46) then
      begin
      sRet:=EnviaComando( '2B0'+sLinha);
      if copy(sRet,7,2)<>'00' then
         Begin
         result := '1|';
         exit;
         end;
      sLinha := '';
      end
    else
      sLinha := sLinha + copy(Texto,i,1);
    Inc(i);
  end;
  result := '0|';
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.MemoriaFiscal( DataInicio,DataFim:TDateTime;ReducInicio,ReducFim,Tipo:String ): String;
Var
  sRet: String;
Begin
  sRet:=FormataData(DataInicio,1);
  sRet := EnviaComando('28'+FormataData(DataInicio,1)+FormataData(DataFim,1)+'1');
  if copy(sRet,7,2)='00' then
     result := '0|'
  else
     result := '1|'
end;
//----------------------------------------------------------------------------
function TImpFiscalYanco.StatusImp( Tipo:Integer ):String;
//Tipo - Indica qual o status quer se obter da impressora
//  1 - Obtem a Hora da Impressora
//  2 - Obtem a Data da Impressora
//  3 - Verifica o Papel
//  4 - Verifica se � poss�vel cancelar um ou todos os itens.
//  5 - Cupom Fechado ?
//  6 - Ret. suprimento da impressora
//  7 - ECF permite desconto por item
//  8 - Verica se o dia anterior foi fechado
//  9 - Verifica o Status do ECF
// 10 - Verifica se todos os itens foram impressos.
// 11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
// 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
// 13 - Verifica se o ECF Arredonda o Valor do Item
// 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
// 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
// 16 - Verifica se exige o extenso do cheque

// 20 - Retorna o CNPJ cadastrado na impressora
// 21 - Retorna o IE cadastrado na impressora
// 22 - Retorna o CRZ - Contador de Redu��es Z
// 23 - Retorna o CRO - Contador de Reinicio de Opera��es
// 24 - Retorna a letra indicativa de MF adicional
// 25 - Retorna o Tipo de ECF
// 26 - Retorna a Marca do ECF
// 27 - Retorna o Modelo do ECF
// 28 - Retorna o Vers�o atual do Software B�sico do ECF gravada na MF
// 29 - Retorna a Data de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
// 30 - Retorna o Hor�rio de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
// 31 - Retorna o N� de ordem seq�encial do ECF no estabelecimento usu�rio
// 32 - Retorna o Grande Total Inicial
// 33 - Retorna o Grande Total Final
// 34 - Retorna a Venda Bruta Diaria
// 35 - Retorna o Contador de Cupom Fiscal CCF
// 36 - Retorna o Contador Geral de Opera��o N�o Fiscal
// 37 - Retorna o Contador Geral de Relat�rio Gerencial
// 38 - Retorna o Contador de Comprovante de Cr�dito ou D�bito
// 39 - Retorna a Data e Hora do ultimo Documento Armazenado na MFD
// 40 - Retorna o Codigo da Impressora Referente a TABELA NACIONAL DE C�DIGOS DE IDENTIFICA��O DE ECF
// 43 e 44- Reservado Autocom
// 45  - Modelo Fiscal
// 46 - Marca, Modelo e Firmware

var
  sRet : String;
begin
  sRet:='1|';
  if Tipo = 1 then
     Begin
     sRet := EnviaComando( '2E');
     sRet:='0|'+Copy(sRet,740,5)+':00'
     end
  Else if Tipo = 2 then
     Begin
     sRet := EnviaComando( '2E');
     sRet:='0|'+Copy(sRet,747,8)
     end
  Else if Tipo = 3 then
     Begin
     sRet := EnviaComando('25');
     sRet:=Copy(sRet,15,1)+'|';
     end
  Else if Tipo = 4 then
     sRet:='0|Todos'
  Else if Tipo = 5 then
     Begin
     sRet := EnviaComando('25');
     sRet:=Copy(sRet,17,2);
     if sRet='1\' then
        sRet:='0|'
     else
        sRet:='1|';
     end
  //6 - Ret. suprimento da impressora
  Else if Tipo = 6 then
      sRet := '0.00'
  //7 - ECF permite desconto por item
  Else if Tipo = 7 then
     sRet := '0|'
  //8 - Verica se o dia anterior foi fechado
  Else if Tipo = 8 then
     sRet := '1'
  //9 - Verifica o Status do ECF
  Else if Tipo = 9 then
     sRet := '0'
  //10 - Verifica se todos os itens foram impressos.
  Else if Tipo = 10 then
     sRet := '0'
  //11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
  Else if Tipo = 11 then
     sRet := '1'
  // 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
  Else if Tipo = 12 then
     sRet := '1'
  // 13 - Verifica se o ECF Arredonda o Valor do Item
  Else if Tipo = 13 then
    result := '1'
  // 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
  else if Tipo = 14 then
    // 0 - Fechada
    Result := '0'
  // 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
  else if Tipo = 15 then
    Result := '1'
  // 16 - Verifica se exige o extenso do cheque
  else if Tipo = 16 then
    Result := '1'
  // 20 ao 40 - Retorno criado para o PAF-ECF
  else if (Tipo >= 20) AND (Tipo <= 40) then
    Result := '0'
  else If Tipo = 45 then
         Result := '0|'// 45 Codigo Modelo Fiscal
  else If Tipo = 46 then // 46 Identifica��o Protheus ECF (Marca, Modelo, firmware)
         Result := '0|'// 45 Codigo Modelo Fiscal
  else
    Result := '1';

  result :=sRet
end;
//----------------------------------------------------------------------------

procedure TYanco.Comm1Error(Sender: TObject; Errors: Integer);
//Mensagem de erro do Componente.
begin
  if (Errors and CE_BREAK > 0) then
    ShowMessage('The hardware detected a break condition.');
  if (Errors and CE_DNS > 0) then
    ShowMessage('Windows 95 only: A parallel device is not selected.');
  if (Errors and CE_FRAME > 0) then
    ShowMessage('The hardware detected a framing error.');
  if (Errors and CE_IOE > 0) then
    ShowMessage('An I/O error occurred during communications with the device.');
  if (Errors and CE_MODE > 0) then
  begin
    ShowMessage('The requested mode is not supported, or the hFile parameter'+
                 'is invalid. If this value is specified, it is the only valid error.');
  end;
  if (Errors and CE_OOP > 0) then
    ShowMessage('Windows 95 only: A parallel device signaled that it is out of paper.');
  if (Errors and CE_OVERRUN > 0) then
    ShowMessage('A character-buffer overrun has occurred. The next character is lost.');
  if (Errors and CE_PTO > 0) then
    ShowMessage('Windows 95 only: A time-out occurred on a parallel device.');
  if (Errors and CE_RXOVER > 0) then
  begin
    ShowMessage('An input buffer overflow has occurred. There is either no'+
                'room in the input buffer, or a character was received after'+
                'the end-of-file (EOF) character.');
  end;
  if (Errors and CE_RXPARITY > 0) then
    ShowMessage('The hardware detected a parity error.');
  if (Errors and CE_TXFULL > 0) then
  begin
    ShowMessage('The application tried to transmit a character, but the output'+
                 'buffer was full.');
  end;

end;
//------------------------------------------------------------------------------
procedure TYanco.Comm1RxChar(Sender: TObject; Count: Integer);
var
  Buffer  : array[0..1024] of Char;
  Bytes, P: Integer;
  bInic   : Boolean;

begin
  if trim(sRetorno)='' then
    bInic:= false
  else
    bInic:= True;

  Fillchar(Buffer, Sizeof(Buffer), 0);
  Bytes := Comm1.Read(Buffer, Count);
  if Bytes = -1 then
    ShowMessage('Erro de leitura da resposta do comando')
  else
  begin
    for P := 0 to Bytes do
      case Buffer[P] of
        #10: begin
               // Memo2.Lines.add('');
               // Inc(FCurrentLine);
             end;
        #03: begin
             bRet:= True;
             Break;
             end;
        #02: begin
             bInic:= True;
             end;
        #0: begin
            Break;
            end;

        #13:;
         else
           begin
              if bInic then
                sRetorno:=sRetorno+Buffer[P];
           end;
      end;
  end;
end;
//---------------------------------------------------------------------------
function EnviaComando(sCmd:String): String;
var
  soma,sSeq : string;
  checksum,i:integer;
  sMens     : String;
  iVz       : Integer;

begin
  sRetorno:='';
  sMens   :='';
  bRet := false;
  iVz:=0;
  sSeq:=FncSequencia;
  soma := chr(2)+ sSeq+sCmd+ chr(3);
   //Calcula o checksum do pacote
   checksum:=0;
   for i:=1 to length(soma) do
      checksum:=checksum+ord(soma[i]);

   soma := soma + chr(Checksum div 256)+chr(checkSum mod 256);
   Comm1.Write(Soma[1], Length(Soma));
   while not  bRet and ( iVz< 300 ) do
      begin
      Application.ProcessMessages;
      sleep(100);
      Inc(iVz);
      end;
   if Trim(sRetorno)='' then
      ShowMessage('Falha de comunica��o com a impressora');

   if ( sCmd<>'2E') and ( sCmd<>'25') and ( Copy(sRetorno,7,2)<>'00' )and ( sCmd<>'121             ') then
      Begin
      //Mensagem de erro da Impressora Fiscal
      Case StrToInt(copy(sRetorno,7,2)) of
          10 : sMens := 'Fim do papel';
          32 : sMens := 'Fun��o n�o permitida sem totaliza��o';
          34 : sMens := 'Situa��o tribut�ria inv�lida';
          35 : sMens := 'Campo num�rico inv�lido';
          36 : sMens := 'N�meros n�o permitido neste campo';
          41 : sMens := 'Fun��o n�o permitida sem o inicio de opera��o fiscal';
          42 : sMens := 'Fun��o n�o permitida sem o inicio de opera��o n�o fiscal';
          43 : sMens := 'Fun��o n�o permitida durante opera��o fiscal ou n�o fiscal';
          44 : sMens := 'Fun��o n�o permitida sem o inicio do Dia';
          45 : sMens := 'Fun��o n�o permitida sem status de interven��o ou durante opera��o fiscal ou n�o fiscal';
          46 : sMens := 'Fun��o n�o permitida com status de interven��o';
          47 : sMens := 'Fun��o n�o permitida ap�s inicio do dia';
          48 : sMens := 'Fun��o n�o permitida sem status de interven��o ou durante opera��o fiscal ou n�o fiscal';
          50 : sMens := 'Necessita Redu��o Z';
          53 : sMens := 'Ultima fun��o n�o permite a execu��o deste comando';
          54 : sMens := 'Cupom aberto mas n�o finalizado';
          55 : sMens := 'Fun��o n�o permitida sem abertura de relat�rio X ou Z';
      end;
      if Trim(sMens)<>'' then
         ShowMessage(sMens);

      end;
   result:=sRetorno;
end;
//----------------------------------------------------------------------------
procedure TImpFiscalYanco.AlimentaProperties;
Var
  sRet                      : String;
  sICMS, sISS, sTodas, sAux : String;
begin
  sICMS  := '';
  sISS   := '';
  sTodas := '';

  sRet   := EnviaComando( '2E');
  sAux   := copy(sRet,7,80);
  sTodas := sAux;
  if Trim(sAux)='' then
     Begin
     exit;
     end;

  While Trim(sAux)<>'' do
     begin
     if ( copy(sAux,1,1)='T' ) and (StrToFloat(copy(sAux,2,4))>0 ) then
        sICMS:=sICMS+copy(sAux,2,2)+'.'+copy(sAux,4,2)+'|';

     if ( copy(sAux,1,1)='S' ) and (StrToFloat(copy(sAux,2,4))>0 ) then
        sISS:=sISS+copy(sAux,2,2)+'.'+copy(sAux,4,2)+'|';

     sAux:=copy(sAux,6,length(sAux));
  end;

  PDV       := copy(sRet,100,3);
  ICMS      := sICMS;
  ISS       := sISS;
  Aliquotas := sTodas;

  sAux := copy(sRet,104,450);
  sRet := '';

  While Trim(sAux)<>'' do
  begin
     if Trim(copy(sAux,1,15))<>'---' then
        sRet:=sRet+Trim(copy(sAux,1,15))+'|';
        sAux:=copy(sAux,16,length(sAux));
  end;
  FormasPgto := sRet;
End;
//----------------------------------------------------------------------------
Function FncSequencia:String;
var
  fArq : TextFile;
  sArq:String;
  sCmd: String;
begin
  sCmd:='0000';
  sArq:='C:\YANCO.SEQ';
  if FileExists(sArq) Then
     Begin
     AssignFile( fArq,sArq );
     Reset( fArq );
     ReadLn( fArq,sCmd );
     CloseFile( fArq );
     Application.ProcessMessages;
     end;

  if (StrToInt(sCmd)+1)>9999 then
     sCmd:='0000';

  sCmd:=FormataTexto(IntToStr(StrToInt(sCmd)+1),4,0,2);
  AssignFile( fArq,sArq );
  ReWrite( fArq );
  WriteLn( fArq,sCmd );
  CloseFile( fArq );
  Application.ProcessMessages;
  Result:=sCmd;
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.PegaSerie : String;
begin
    result := '1|Funcao nao disponivel';
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.Suprimento( Tipo:Integer;Valor:String;Forma:String; Total:String; Modo:Integer; FormaSupr:String ):String;
var
    sTotaliz,sTipo,sRet : String;
begin
    //Utilizar o totalizador de "Recebimento"
    sTotaliz := '03';
    sTipo    := '  ';

    If Tipo = 1 Then //Caso a op��o for igual a checar
        Valor := '0.00';

    Valor:= StrTran(Valor,'.','');
    Valor:= FormataTexto(Valor,13,1,2);

    //Envio o comando para a impressora
    sRet:=EnviaComando( '29'+sTotaliz+sTipo+Valor );

    //Se eu obter um resultado positivo da impressora
    if copy(sRet,7,2)='00' then
        result := '0|'
    else
        result := '1|';
End;

//-----------------------------------------------------------
function TImpFiscalYanco.Pedido( Totalizador, Tef, Texto, Valor, CondPagTef:String ): String;
begin
  Result:='0';
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.RecebNFis( Totalizador, Valor, Forma:String ): String;
var iRet : Integer;
begin
  ShowMessage('Fun��o n�o dispon�vel para este equipamento' );
  result := '1';
end;
//-----------------------------------------------------------------------------
function TImpFiscalYanco.DownloadMFD( sTipo, sInicio, sFinal : String ):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpFiscalYanco.GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario : String):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpFiscalYanco.LeTotNFisc:String;
Begin
  Result := '0|-99' ;
End;

//------------------------------------------------------------------------------
function TImpFiscalYanco.IdCliente( cCPFCNPJ , cCliente , cEndereco : String ): String;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - IdCliente : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpFiscalYanco.EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : String ) : String;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - EstornNFiscVinc : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpFiscalYanco.ImpTxtFis(Texto : String) : String;
begin
 GravaLog(' - ImpTxtFis : Comando N�o Implementado para este modelo');
 Result := '0|';
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.RelGerInd( cIndTotalizador , cTextoImp : String ; nVias : Integer ; ImgQrCode: String) : String;
var iRet : Integer;
begin
  Result := '0|Comando n�o implementado';
end;

//-----------------------------------------------------------------------------
function TImpFiscalYanco.DownMF(sTipo, sInicio, sFinal : String):String;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//-----------------------------------------------------------------------------
function TImpFiscalYanco.RedZDado( MapaRes : String ):String;
Begin
  Result := '0';
End;

//----------------------------------------------------------------------------
Function TrataTags( Mensagem : String ) : String;
var
  cMsg : String;
begin
cMsg := Mensagem;
cMsg := RemoveTags( cMsg );
Result := cMsg;
end;

//----------------------------------------------------------------------------
function TImpFiscalYanco.GrvQrCode(SavePath, QrCode: String): String;
begin
GravaLog(' GrvQrCode - n�o implementado para esse modelo ');
Result := '0';
end;

initialization
  RegistraImpressora('YANCO 8000', TImpFiscalYanco, 'BRA', '470501');

//----------------------------------------------------------------------------
end.
