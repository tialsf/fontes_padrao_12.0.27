unit ImpFujitsu;

interface

uses
  Dialogs,
  ImpFiscMain,
  Windows,
  SysUtils,
  classes,
  LojxFun,
  Forms;

const
  pBuffSize = 200;

Type

  TImpFiscalGeneral = class(TImpressoraFiscal)
  private
    fHandle : THandle;
    fFuncAbrePorta      : function ( porta:Pointer ):integer; stdcall;    // OpenFujitsu
    fFuncEnviaComando   : function ( buffer:Pointer ):integer; stdcall;   // TxFujitsu
    fFuncLeRetorno      : function ( buffer:Pointer; Status1:Pointer; Status2:Pointer ):Integer; stdcall; // RxFujitsu
    fFuncFechaPorta     : procedure; stdcall;   // CloseFujitsu
    fFuncAnalisaByte    : procedure ( buffer:Pointer; ret:Pointer ); stdcall;    // AnalisaByte

  public
    function Abrir( sPorta:AnsiString; iHdlMain:Integer ):AnsiString; override;
    function Fechar( sPorta:AnsiString ):AnsiString; override;
    function AbreEcf:AnsiString; override;
    function FechaEcf:AnsiString; override;
    function LeituraX:AnsiString; override;
    function ReducaoZ( MapaRes:AnsiString ):AnsiString; override;
    function AbreCupom(Cliente:AnsiString; MensagemRodape:AnsiString):AnsiString; override;
    function PegaCupom(Cancelamento:AnsiString):AnsiString; override;
    function PegaPDV:AnsiString; override;
    function RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:AnsiString; nTipoImp:Integer ): AnsiString; override;
    function LeAliquotas:AnsiString; override;
    function LeAliquotasISS:AnsiString; override;
    function LeCondPag:AnsiString; override;
    function CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:AnsiString ):AnsiString; override;
    function CancelaCupom( Supervisor:AnsiString ):AnsiString; override;
    function FechaCupom( Mensagem:AnsiString ):AnsiString; override;
    function Pagamento( Pagamento,Vinculado,Percepcion:AnsiString ): AnsiString; override;
    function DescontoTotal( vlrDesconto:AnsiString ;nTipoImp:Integer): AnsiString; override;
    function AcrescimoTotal( vlrAcrescimo:AnsiString ): AnsiString; override;
    function MemoriaFiscal( DataInicio,DataFim:TDateTime ;ReducInicio,ReducFim,Tipo:AnsiString ): AnsiString; override;
    function AdicionaAliquota( Aliquota:AnsiString; Tipo:Integer ): AnsiString; override;
    function AbreCupomNaoFiscal( Condicao,Valor,Totalizador,Texto:AnsiString ): AnsiString; override;
    function TextoNaoFiscal( Texto:AnsiString;Vias:Integer ):AnsiString; override;
    function FechaCupomNaoFiscal: AnsiString; override;
    function TotalizadorNaoFiscal( Numero,Descricao:AnsiString ):AnsiString; override;
    function Autenticacao( Vezes:Integer; Valor,Texto:AnsiString ): AnsiString; override;
//    function Suprimento( Tipo:Integer;Valor:AnsiString ):AnsiString; override;
//    function Gaveta:AnsiString; override;
    function Status( Tipo:Integer; Texto:AnsiString ):AnsiString; override;
    function StatusImp( Tipo:Integer ):AnsiString; override;
    function EnviaComando( sComando:AnsiString;sArgumento:AnsiString='' ):AnsiString;
    Procedure PulaLinha( iNumero:Integer );
    function RelatorioGerencial( Texto:AnsiString;Vias:Integer ; ImgQrCode: AnsiString):AnsiString; override;
    function ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : AnsiString ; Vias : Integer ) : AnsiString; Override;
    function PegaSerie:AnsiString; override;
    function ImpostosCupom(Texto: AnsiString): AnsiString; override;
    function Pedido( Totalizador, Tef, Texto, Valor, CondPagTef:AnsiString ): AnsiString; override;
    function RecebNFis( Totalizador, Valor, Forma:AnsiString ): AnsiString; override;
    function DownloadMFD( sTipo, sInicio, sFinal : AnsiString ):AnsiString; override;
    function GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario : AnsiString ):AnsiString; override;
    function RelGerInd( cIndTotalizador , cTextoImp : AnsiString ; nVias : Integer ; ImgQrCode: AnsiString) : AnsiString; override;
    function LeTotNFisc:AnsiString; override;
    function DownMF(sTipo, sInicio, sFinal : AnsiString):AnsiString; override;
    function RedZDado( MapaRes:AnsiString ):AnsiString; override;
    function IdCliente( cCPFCNPJ , cCliente , cEndereco : AnsiString ): AnsiString; Override;
    function EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : AnsiString ) : AnsiString; Override;
    function ImpTxtFis(Texto : AnsiString) : AnsiString; Override;
    function GrvQrCode(SavePath,QrCode: AnsiString): AnsiString; Override;
end;

Function TrataTags( Mensagem : AnsiString ) : AnsiString;

implementation

//---------------------------------------------------------------------------
function TImpFiscalGeneral.Abrir(sPorta : AnsiString; iHdlMain:Integer) : AnsiString;

  function ValidPointer( aPointer: Pointer; sMSg :AnsiString ) : Boolean;
  begin
    if not Assigned(aPointer) Then
    begin
      ShowMessage('A fun��o "'+sMsg+'" n�o existe na Dll: GENERAL32.DLL');
      Result := False;
    end
    else
      Result := True;
  end;

var
  aFunc: Pointer;
  bRet : Boolean;
  iRet : Integer;
begin
  fHandle := LoadLibrary( 'GENERAL32.DLL' );
  if (fHandle <> 0) Then
  begin
    bRet := True;

    aFunc := GetProcAddress(fHandle,'CloseFujitsu');
    if ValidPointer( aFunc, 'CloseFujitsu' ) then
      fFuncFechaPorta := aFunc
    else
    begin
      bRet := False;
    end;

    aFunc := GetProcAddress(fHandle,'OpenFujitsu');
    if ValidPointer( aFunc, 'OpenFujitsu' ) then
      fFuncAbrePorta := aFunc
    else
    begin
      bRet := False;
    end;

    aFunc := GetProcAddress(fHandle,'TxFujitsu');
    if ValidPointer( aFunc, 'TxFujitsu' ) then
      fFuncEnviaComando := aFunc
    else
    begin
      bRet := False;
    end;

    aFunc := GetProcAddress(fHandle,'RxFujitsu');
    if ValidPointer( aFunc, 'RxFujitsu' ) then
      fFuncLeRetorno := aFunc
    else
    begin
      bRet := False;
    end;

    aFunc := GetProcAddress(fHandle,'AnalisaByte');
    if ValidPointer( aFunc, 'AnalisaByte' ) then
      fFuncAnalisaByte := aFunc
    else
    begin
      bRet := False;
    end;

  end
  else
  begin
    ShowMessage('O arquivo GENERAL32.DLL n�o foi encontrado em .' + ExtractFilePath(Application.ExeName) );
    bRet := False;
  end;

  if bRet then
  begin
    result := '0|';
    iRet := fFuncAbrePorta( PChar(sPorta) );
    if iRet <> 1 then
      bRet := False;
    if not bRet then
    begin
      ShowMessage('Erro na abertura da porta');
      result := '1|';
    end;
  end
  else
    result := '1|';

end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.Fechar( sPorta:AnsiString ) : AnsiString;
begin
  if (fHandle <> INVALID_HANDLE_VALUE) then
  begin
    fFuncFechaPorta;
    FreeLibrary(fHandle);
    fHandle := 0;
  end;
  Result := '0|';
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.LeituraX : AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '51','0' );
  result := Status( 1, sRet );
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.ReducaoZ ( MapaRes:AnsiString ): AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '52','0' );
  result := Status( 1, sRet );
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.LeAliquotas:AnsiString;
var
  sRet : AnsiString;
  i : Integer;
  sAliquota,sAux : AnsiString;
begin
  sRet := EnviaComando( '60' );
  For i:=0 to 15 do
  begin
    sAux := copy(sRet,(i*5)+15,5);
    if (copy(sAux,1,1) = 'T') and (copy(sAux,2,4) <> '0000')then
      sAliquota := sAliquota + FloatToStrf(StrToFloat(copy(sAux,2,4))/100,ffFixed,18,2) + '|';
  end;
  result := Status( 1,sRet );
  if result = '0' then
    result := result + '|' + sAliquota;
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.LeAliquotasISS:AnsiString;
var
  sRet : AnsiString;
  i : Integer;
  sAliquota,sAux : AnsiString;
begin
  sRet := EnviaComando( '60' );
  For i:=0 to 15 do
  begin
    sAux := copy(sRet,(i*5)+15,5);
    if (copy(sAux,1,1) = 'S') and (copy(sAux,2,4) <> '0000')then
      sAliquota := sAliquota + FloatToStrf(StrToFloat(copy(sAux,2,4))/100,ffFixed,18,2) + '|';
  end;
  result := Status( 1,sRet );
  if result = '0' then
    result := result + '|' + sAliquota;
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.LeCondPag:AnsiString;
var
  sRet1, sRet2, sRet : AnsiString;
  sPagto : AnsiString;
  sAux : AnsiString;
  i : Integer;
begin
  sRet1 := EnviaComando( '61','1' );
  sRet2 := EnviaComando( '62','2' );
  sRet := copy(sRet1,15,Length(sRet1)) + copy(sRet2,15,Length(sRet2));

  sPagto := '';
  For i:=0 to 29 do
  begin
    sAux := Trim(copy(copy(sRet,(i*16)+1,16),1,15));
    if sAux <> '' then
      sPagto := sPagto + sAux + '|';
  end;

  result := Status( 1,sRet1 );
  if copy(result,1,1) = '0' then
    result := result + '|' + sPagto;
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.AbreCupom(Cliente:AnsiString; MensagemRodape:AnsiString):AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '20','0' );
  result := Status( 1, sRet );

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.PegaCupom(Cancelamento:AnsiString):AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '65', '1' );
  result := Status( 1,sRet );
  if result = '0' then
    result := result + '|' + copy(sRet,15,6);
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.PegaPDV:AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
     result := result + '|' + copy(sRet,110,4);
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.ImpostosCupom(Texto: AnsiString): AnsiString;
begin
  Result := '0';
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.CancelaItem( numitem,codigo,descricao,qtde,vlrunit,vlrdesconto,aliquota:AnsiString ):AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '25',FormataTexto(numitem,3,0,2) );
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.CancelaCupom( Supervisor:AnsiString ):AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
  begin
    if copy(sRet,7,3) = '000' then  // cupom fechado
      sRet := EnviaComando( '2A' )
    else
      sRet := EnviaComando( '29' );
    result := Status( 1,sRet );

    // verifica se foi impresso apenas o cabecalho
    if copy(sRet,7,3) = '001' then
      result := '1';
  end;

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.RegistraItem( codigo,descricao,qtde,vlrUnit,vlrdesconto,aliquota,vlTotIt,UnidMed:AnsiString; nTipoImp:Integer ): AnsiString;
var
  sRet : AnsiString;
  sAliquota,sAux : AnsiString;
  sLinha : AnsiString;
  sPos : AnsiString;
  sSituacao : AnsiString;
  i : Integer;
begin
  //verifica se � para registrar a venda do item ou s� o desconto
  if Trim(codigo+descricao+qtde+vlrUnit) = '' then
  begin
    if StrToFloat(vlrdesconto) > 0 then
    begin
      sRet := EnviaComando( '26','20000'+FormataTexto(vlrdesconto,12,2,2) );
      result := Status( 1,sRet );
    end
    else
      result := '0';
    exit;
  end;

  //verifica a aliquota a ser utilizada
  sSituacao := copy(aliquota,1,1);

  sPos := '00';
  if Pos(sSituacao,'IFN') > 0 then      // Isento, nao tributado ou subst.trib.
    if sSituacao = 'I' then
      sPos := '17'
    else if sSituacao = 'F' then
      sPos := '18'
    else
      sPos := '19'
  else                                  // tributado ICMS ou ISS
  begin
    aliquota := FloatToStrf(StrToFloat(Trim(copy(aliquota,2,5))),ffFixed,18,2);
    sRet := EnviaComando( '60' );
    if status(1,sRet) = '0' then
    begin
      // le as aliquotas cadastradas na impressora
      For i:=0 to 15 do
      begin
        sAux := copy(sRet,(i*5)+15,5);
        if copy(sAux,2,4) <> '0000' then
          sAliquota := sAliquota + copy(sAux,1,1) + FloatToStrf(StrToFloat(copy(sAux,2,4))/100,ffFixed,18,2) + '|';
      end;
      if Pos(sSituacao+aliquota,sAliquota) > 0 then
      begin
        // verifica qual o totalizador que esta a aliquota
        if (Pos(sSituacao+aliquota,sAliquota) mod 5) > 0 then
          sPos := IntToStr((Pos(sSituacao+aliquota,sAliquota) div 5) + 1)
        else
          sPos := IntToStr(Pos(aliquota,sAliquota) div 5);
        sPos := FormataTexto(sPos,2,0,2);
      end;
    end;
  end;

  if sPos = '00' then
  begin
    result := '1';   // aliquota nao cadastrada;
    exit;
  end
  else
  begin
    // monta a linha para registrar o item
    sLinha := '';
    sLinha := sLinha + copy(codigo+space(13),1,13);
    sLinha := sLinha + copy(descricao+space(28),1,28);
    sLinha := sLinha + FormataTexto(qtde,7,3,2);
    sLinha := sLinha + FormataTexto(vlrUnit,8,2,2);
    sLinha := sLinha + '2'; // casas decimais
    sLinha := sLinha + FormataTexto(FloatToStr(StrToFloat(qtde)*StrToFloat(vlrUnit)),12,2,2); // valor total
    sLinha := sLinha + sPos; // aliquota
    sRet := EnviaComando( '21',sLinha );
  end;

  result := Status( 1,sRet);

  // verifica o desconto
  if result = '0' then
    if StrToFloat(vlrdesconto) > 0 then
    begin
      sRet := EnviaComando( '26','20000'+FormataTexto(vlrdesconto,12,2,2) );
      result := Status( 1,sRet );
    end;


end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.AbreECF:AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
  begin
    sRet := EnviaComando( '50' );
    result := Status( 1,sRet );
  end;
end;

//---------------------------------------------------------------------------
function TImpFiscalGeneral.FechaECF : AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '52','0' );
  result := Status( 1, sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.Pagamento( Pagamento,Vinculado,Percepcion:AnsiString ): AnsiString;
  function AchaPagto( sPagto:AnsiString; aPagtos: TaString ):AnsiString;
  var
    i : Integer;
    iPos : Integer;
  begin
    iPos := 0;
    for i:=0 to Length(aPagtos)-1 do
      if Trim(UpperCase(aPagtos[i])) = Trim(UpperCase(sPagto)) then
        iPos := i + 1;
    result := IntToStr(iPos);
    if Length(result) < 2 then
      result := '0' + result;
  end;
var
  sRet : AnsiString;
  sPagto : AnsiString;
  aPagto,aAuxiliar : TaString;
  i : Integer;
begin
  // Faz a checagem do Parametro
  Pagamento := StrTran(Pagamento,',','.');

  // Pega a condicao de pagamento
  sPagto := LeCondPag;
  MontaArray( copy(sPagto,2,Length(sPagto)),aPagto );

  // Monta array com as formas de pagto solicitadas
  MontaArray( Pagamento,aAuxiliar );

  // Verifica a forma de pagamento.
  i:=0;
  While i<Length(aAuxiliar) do
  begin
    if AchaPagto(aAuxiliar[i],aPagto) <> '00' then
    begin
      Sleep(500);
      sRet := EnviaComando( '22', AchaPagto(aAuxiliar[i],aPagto)+FormataTexto(aAuxiliar[i+1],13,2,2) );
    end;
    Inc(i,2);
  end;
  result := Status( 1,sRet );

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.FechaCupom( Mensagem:AnsiString ):AnsiString;
var
  sRet, sMsg : AnsiString;
begin
  // Encerra o cupom
  sMsg := Mensagem ;
  sMsg := TrataTags( sMsg );
  sMsg := sMsg + space(168);
  sRet := EnviaComando( '23','0'+Copy(sMsg,1,42)+
                             '0'+Copy(sMsg,43,42)+
                             '0'+Copy(sMsg,85,42)+
                             '0'+Copy(sMsg,127,42)+
                             '0');
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.DescontoTotal( vlrDesconto:AnsiString ;nTipoImp:Integer): AnsiString;
var
  sRet : AnsiString;
begin
  if StrToFloat(vlrDesconto) > 0 then
  begin
    // Registra o desconto.
    sRet := EnviaComando( '27', '20000' + FormataTexto(vlrDesconto,12,2,2) );
    result := Status( 1,sRet );
  end
  else
    result := '0';
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.AcrescimoTotal( vlrAcrescimo:AnsiString ): AnsiString;
var
  sRet : AnsiString;
begin
  // Registra o acrescimo.
  sRet := EnviaComando( '27', '40000' + FormataTexto(vlrAcrescimo,12,2,2) );
  result := Status( 1,sRet );
end;


//----------------------------------------------------------------------------
function TImpFiscalGeneral.MemoriaFiscal( DataInicio,DataFim:TDateTime;ReducInicio,ReducFim,Tipo:AnsiString  ): AnsiString;
var
  sRet : AnsiString;
  sArgumento : AnsiString;
begin
  sArgumento := FormataData( DataInicio,2 ) + FormataData( DataFim,2 );
  sRet := EnviaComando( '55',sArgumento );
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.AdicionaAliquota( Aliquota:AnsiString; Tipo:Integer ): AnsiString;
begin
  // esse comando s� poder� ser efetuado com interven��o tecnica, 'jumpeando' a placa
  result := '1';
end;
//----------------------------------------------------------------------------
function TImpFiscalGeneral.AbreCupomNaoFiscal( Condicao,Valor,Totalizador,Texto:AnsiString ): AnsiString;
var
  sRet : AnsiString;
  sPagto : AnsiString;
  aPagto : TaString;
  i : Integer;
  iPos : Integer;
  sPos : AnsiString;
begin
  sRet := EnviaComando( '20','1' );
  result := Status( 1,sRet );
  if Pos(copy(sRet,10,3),'047,064') > 0 then
  begin
    sPagto := LeCondPag;
    MontaArray( copy(sPagto,3,Length(sPagto)),aPagto );
    iPos := 0;
    For i:=0 to Length(aPagto)-1 do
    begin
      if (UpperCase(Trim(aPagto[i])) = UpperCase(Trim(Condicao))) then
        iPos := i + 1;
    end;
    sPos := IntToStr(iPos);
    sRet := EnviaComando( '41', Totalizador + FormataTexto(sPos,2,0,2) + FormataTexto(Valor,13,2,2) );
    result := Status( 1,sRet );
  end;

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.TextoNaoFiscal( Texto:AnsiString;Vias:Integer ):AnsiString;
var
  sRet : AnsiString;
  sLinha : AnsiString;
  i : Integer;
begin
  // faz a checagem do texto.
  i:=1;
  sLinha := '';
  while i <= Length(Texto) do
  begin
    if copy(Texto,i,1) = #10 then
    begin
      sRet := EnviaComando( '42', '0'+Copy(sLinha+Space(42),1,42) );
      sLinha := '';
    end
    else
      sLinha := sLinha + copy(Texto,i,1);
    Inc(i);
  end;
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.FechaCupomNaoFiscal: AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '43' );
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.Autenticacao( Vezes:Integer; Valor,Texto:AnsiString ): AnsiString;
var
  sRet : AnsiString;
begin
  sRet := EnviaComando( '2B' );
  result := Status( 1,sRet );
end;

//----------------------------------------------------------------------------
{function TImpFiscalGeneral.Suprimento( Tipo:Integer;Valor:AnsiString ):AnsiString;
begin
  if Tipo = 1 then
    result := '0'
  else
    result := '1';
end;
}
//----------------------------------------------------------------------------
{function TImpFiscalGeneral.Gaveta:AnsiString;
var
  iRet : Integer;
begin
  iRet := EnviaComando( 'T' );
  result := Status( 1,IntToStr(iRet) );
end;
}
//----------------------------------------------------------------------------
function TImpFiscalGeneral.Status( Tipo:Integer; Texto:AnsiString ):AnsiString;
begin
  case Tipo of
    1 : if copy(Texto,1,1) = '+' then
            result := '0'
        else
            if copy(Texto,5,1) = '1' then
                result := '2'    // sem papel
            else
                result := '1';
    else
      result := '0';
    end;

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.StatusImp( Tipo:Integer ):AnsiString;
var
  sRet : AnsiString;
  sData,sHora : AnsiString;
begin
//Tipo - Indica qual o status quer se obter da impressora
//  1 - Obtem a Hora da Impressora
//  2 - Obtem a Data da Impressora
//  3 - Verifica o Papel
//  4 - Verifica se � poss�vel cancelar um ou todos os itens.
//  5 - Cupom Fechado ?
//  6 - Ret. suprimento da impressora
//  7 - ECF permite desconto por item
//  8 - Verifica se o dia anterior foi fechado
//  9 - Verifica o Status do ECF
// 10 - Verifica se todos os itens foram impressos.
// 11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
// 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
// 13 - Verifica se o ECF Arredonda o Valor do Item
// 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
// 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
// 16 - Verifica se exige o extenso do cheque

// 20 - Retorna o CNPJ cadastrado na impressora
// 21 - Retorna o IE cadastrado na impressora
// 22 - Retorna o CRZ - Contador de Redu��es Z
// 23 - Retorna o CRO - Contador de Reinicio de Opera��es
// 24 - Retorna a letra indicativa de MF adicional
// 25 - Retorna o Tipo de ECF
// 26 - Retorna a Marca do ECF
// 27 - Retorna o Modelo do ECF
// 28 - Retorna o Vers�o atual do Software B�sico do ECF gravada na MF
// 29 - Retorna a Data de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
// 30 - Retorna o Hor�rio de instala��o da vers�o atual do Software B�sico gravada na Mem�ria Fiscal do ECF
// 31 - Retorna o N� de ordem seq�encial do ECF no estabelecimento usu�rio
// 32 - Retorna o Grande Total Inicial
// 33 - Retorna o Grande Total Final
// 34 - Retorna a Venda Bruta Diaria
// 35 - Retorna o Contador de Cupom Fiscal CCF
// 36 - Retorna o Contador Geral de Opera��o N�o Fiscal
// 37 - Retorna o Contador Geral de Relat�rio Gerencial
// 38 - Retorna o Contador de Comprovante de Cr�dito ou D�bito
// 39 - Retorna a Data e Hora do ultimo Documento Armazenado na MFD
// 40 - Retorna o Codigo da Impressora Referente a TABELA NACIONAL DE C�DIGOS DE IDENTIFICA��O DE ECF
// 43 e 44- Reservado Autocom
// 45  - Modelo Fiscal
// 46 - Marca, Modelo e Firmware

// Verifica a hora da impressora
If Tipo = 1 then
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
  begin
    sHora := copy(sRet,23,4);
    result := result + '|' + copy(sHora,1,2) + ':' + copy(sHora,3,2) + ':00';
  end;
end
// Verifica a data da Impressora
else if Tipo = 2 then
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
  begin
    sData := copy(sRet,15,8);
    result := result + '|' + copy(sData,1,2) + '/' + copy(sData,3,2) + '/' + copy(sData,7,2);
  end;
end
// Verifica o estado do papel
else if Tipo = 3 then
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
  begin
    if copy(sRet,6,1) = '1' then
      result := '2' // sem papel
    else
      result := '0' // com papel
  end;
end
//Verifica se � poss�vel cancelar um ou todos os itens.
else if Tipo = 4 then
  result := '0|TODOS'
//5 - Cupom Fechado ?
else if Tipo = 5 then
begin
  sRet := EnviaComando( '64' );
  result := Status( 1,sRet );
  if result = '0' then
    if copy(sRet,7,3) = '000' then  // cupom fechado
      result := '0'
    else
      result := '7';
end
//6 - Ret. suprimento da impressora
else if Tipo = 6 then
  result := '0|0.00'
//7 - ECF permite desconto por item
else if Tipo = 7 then
  result := '0'
//8 - Verica se o dia anterior foi fechado
else if Tipo = 8 then
begin
  sRet := EnviaComando( '64' );
  WriteLog( 'log.txt', sRet );
  result := Status( 1,sRet );
  if result = '0' then
    if Pos(copy(sRet,10,3),'097,093') > 0 then
      result := '10'
    else
      result := '0';
end
//  9 - Verifica o Status do ECF
else if Tipo = 9 then
  result := '0'
// 10 - Verifica se todos os itens foram impressos.
else if Tipo = 10 then
  result := '0'
// 11 - Retorna se eh um Emulador de ECF (0=Emulador / 1=ECF)
else if Tipo = 11 then
  result := '1'
// 12 - Verifica se o ECF possui as funcoes IFNumItem e IFSubTotal (1=Nao / 0=Sim)
else if Tipo = 12 then
  result := '1'
// 13 - Verifica se o ECF Arredonda o Valor do Item
else if Tipo = 13 then
  result := '1'
// 14 - Verifica se a Gaveta Acoplada ao ECF esta (0=Fechada / 1=Aberta)
else if Tipo = 14 then
  // 0 - Fechada
  Result := '0'
// 15 - Verifica se o ECF permite desconto apos registrar o item (0=Permite)
else if Tipo = 15 then
  Result := '1'
// 16 - Verifica se exige o extenso do cheque
else if Tipo = 16 then
  Result := '1'
// 20 ao 40 - Retorno criado para o PAF-ECF
else if (Tipo >= 20) AND (Tipo <= 40) then
  Result := '0'
else If Tipo = 45 then
       Result := '0|'// 45 Codigo Modelo Fiscal
else If Tipo = 46 then // 46 Identifica��o Protheus ECF (Marca, Modelo, firmware)
       Result := '0|'// 45 Codigo Modelo Fiscal
  //Retorno n�o encontrado
else
  Result := '1';

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.EnviaComando( sComando:AnsiString;sArgumento:AnsiString='' ):AnsiString;
var
  iRet2 : Integer;
  pRet : array[0..256] of char;
  pStatus1 : array[0..1] of char;
  pStatus2 : array[0..1] of char;
  pRetBit : array[0..8] of char;
begin
  fFuncEnviaComando( PChar(sComando+sArgumento) );
  iRet2 := fFuncLeRetorno( @pRet,@pStatus1,@pStatus2 );
  if iRet2 > 0 then
    result := '+'
  else
    result := '-';

  // retorno do status 1
  fFuncAnalisaByte(@pStatus1,@pRetBit);
  result := result + Trim(StrPas(pRetBit));

  // retorno do status 2
  result := result + FormataTexto(IntToStr(ord(pStatus2[0])),3,0,2) ;

  // retorno do comando
  result := result + strpas( pRet );

end;

//----------------------------------------------------------------------------
procedure TImpFiscalGeneral.PulaLinha( iNumero:Integer );
begin
  EnviaComando( '44', FormataTexto(IntToStr(iNumero),2,0,2));
end;


//----------------------------------------------------------------------------
function TImpFiscalGeneral.RelGerInd( cIndTotalizador , cTextoImp : AnsiString ; nVias : Integer ; ImgQrCode: AnsiString) : AnsiString;
begin
  Result := RelatorioGerencial(cTextoImp , nVias , ImgQrCode);
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.RelatorioGerencial( Texto:AnsiString;Vias:Integer ; ImgQrCode: AnsiString):AnsiString;
var
  sRet : AnsiString;
  sAux : AnsiString;
begin
  sRet := EnviaComando( '51','1' );
  result := Status( 1,sRet );
  If result = '0' then
  begin
    Texto := LimpaAcentuacao( Texto );
    While (Length(Texto) > 0) and (Texto <> #10) do
    begin
      sAux := copy(Texto,1,Pos(#10,Texto)-1);
      Texto := copy(Texto,Pos(#10,Texto)+1,Length(Texto));
      sRet := EnviaComando( '57', '0'+copy(sAux+Space(42),1,42) );
      if Status( 1,sRet ) <> '0' then
        exit;
    end;
    result := Status( 1,sRet );
    if result = '0' then
      EnviaComando( '56' );
  end;

end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.ImprimeCodBarrasITF( Cabecalho , Codigo, Rodape : AnsiString ; Vias : Integer):AnsiString;

begin
  WriteLog('SIGALOJA.LOG','Comando n�o suportado para este modelo!');
  Result := '0';
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.PegaSerie : AnsiString;
begin
    result := '1|Funcao nao disponivel';
end;

//-----------------------------------------------------------
function TImpFiscalGeneral.Pedido( Totalizador, Tef, Texto, Valor, CondPagTef:AnsiString ): AnsiString;
begin
  Result:='0';
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.RecebNFis( Totalizador, Valor, Forma:AnsiString ): AnsiString;
begin
  ShowMessage('Fun��o n�o dispon�vel para este equipamento' );
  result := '1';
end;
//------------------------------------------------------------------------------
function TImpFiscalGeneral.DownloadMFD( sTipo, sInicio, sFinal : AnsiString ):AnsiString;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.GeraRegTipoE( sTipo, sInicio, sFinal, sRazao, sEnd, sBinario : AnsiString):AnsiString;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.TotalizadorNaoFiscal( Numero,Descricao:AnsiString ) : AnsiString;
begin
  MessageDlg( MsgIndsImp, mtError,[mbOK],0);
  Result := '1';
end;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.LeTotNFisc:AnsiString;
Begin
  Result := '0|-99' ;
End;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.DownMF(sTipo, sInicio, sFinal : AnsiString):AnsiString;
Begin
  MessageDlg( MsgIndsMFD, mtError,[mbOK],0);
  Result := '1';
End;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.RedZDado(  MapaRes : AnsiString ):AnsiString;
begin
     Result := '0';
end;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.IdCliente( cCPFCNPJ , cCliente , cEndereco : AnsiString ): AnsiString;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - IdCliente : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.EstornNFiscVinc( CPFCNPJ , Cliente , Endereco , Mensagem , COOCDC : AnsiString ) : AnsiString;
begin
WriteLog('sigaloja.log', DateTimeToStr(Now)+' - EstornNFiscVinc : Comando N�o Implementado para este modelo');
Result := '0|';
end;

//------------------------------------------------------------------------------
function TImpFiscalGeneral.ImpTxtFis(Texto : AnsiString) : AnsiString;
begin
 GravaLog(' - ImpTxtFis : Comando N�o Implementado para este modelo');
 Result := '0';
end;

//------------------------------------------------------------------------------
Function TrataTags( Mensagem : AnsiString ) : AnsiString;
var
  cMsg : AnsiString;
begin
cMsg := Mensagem;
cMsg := RemoveTags( cMsg );
Result := cMsg;
end;

//----------------------------------------------------------------------------
function TImpFiscalGeneral.GrvQrCode(SavePath, QrCode: AnsiString): AnsiString;
begin
GravaLog(' GrvQrCode - n�o implementado para esse modelo ');
Result := '0';
end;

(* initialization
  RegistraImpressora('FUJITSU IF GP-2000 - V. 01.00', TImpFiscalGeneral, 'BRA', ' ');  *)

//----------------------------------------------------------------------------
end.
