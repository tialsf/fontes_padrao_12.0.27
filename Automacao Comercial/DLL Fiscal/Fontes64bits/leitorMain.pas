unit LeitorMain;

interface

Uses Classes, SysUtils, Dialogs;

function Leitor_Listar( var aBuff : AnsiString ) : Integer; StdCall;
function Leitor_Abrir( sModelo, sPorta, sFoco : PChar ) : Integer; StdCall;
function Leitor_Fechar( iHdl : Integer; sPorta : PChar ) : Integer; StdCall;
function Leitor_Foco( iHdl : Integer; Modo : Integer ) : Integer; StdCall;

////////////////////////////////////////////////////////////////////////////////
//
//  TLeitor - Classe
//
Type
  TLeitor = Class( TObject )
  private
    fModelo : String;
    fPorta  : String;

  public
    constructor Create( sModelo, sPorta : String ); virtual;

    function Abrir( sPorta, sFoco : String ) : String; virtual; abstract;
    function Fechar( sPorta : String ) : String; virtual; abstract;
    function LeitorFoco( Modo : Integer ) : String; virtual; abstract;

    property Modelo : String  read fModelo;
    property Porta  : String  read fPorta;
  end;

  TLeitorClass = class of TLeitor;

  procedure RegistraLeitor( sModelo : String; cClasse : TLeitorClass; sPaises : String );
//
////////////////////////////////////////////////////////////////////////////////

implementation

////////////////////////////////////////////////////////////////////////////////
//
//   TListaLeitor
//
Type
  TListaLeitor = Class( TStringList )
  public
    function RegistraLeitor( sModelo : String; cClasse : TLeitorClass; sPaises : String ) : Boolean;
    function CriaLeitor( sModelo, sPorta : String ) : TLeitor;
    function Acha( iHdl : integer ) : TLeitor;
  end;

var
  _z_ListaLeitor : TListaLeitor;
//
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------
constructor TLeitor.Create( sModelo, sPorta : String );
begin
  fModelo := sModelo;
  fPorta  := sPorta;
end;

//----------------------------------------------------------------------------
procedure RegistraLeitor( sModelo: String; cClasse : TLeitorClass; sPaises : String );
begin
  if not _z_ListaLeitor.RegistraLeitor( sModelo, cClasse, sPaises ) Then
    raise Exception.CreateFmt( 'Erro na cria��o do driver "%s"', [sModelo] );
end;
//----------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
//
//   Fun��es da TListaLeitor
//
function TListaLeitor.RegistraLeitor( sModelo : String; cClasse : TLeitorClass; sPaises : String ) : Boolean;
begin
  if IndexOf( sModelo ) < 0 then
  begin
    AddObject( sModelo, TObject( cClasse ) );
    AddObject( sPaises, TObject( cClasse ) );
    Result := True;
  end
  else
    Result := False;
end;

//----------------------------------------------------------------------------
function TListaLeitor.CriaLeitor( sModelo, sPorta : String ) : TLeitor;
var
  iPos  : Integer;
  p     : Pointer;
begin
  iPos := IndexOf( sModelo );

  if iPos < 0 then
    Result := nil
  else
  begin
    p := TLeitorClass( Objects[iPos] ).MethodAddress( 'TLeitor' );

    if not Assigned( p ) then
      Result := TLeitorClass( Objects[iPos] ).Create( sModelo, sPorta )
    else
      Result := nil;
  end;
end;

//----------------------------------------------------------------------------
function TListaLeitor.Acha( iHdl : Integer ) : TLeitor;
begin
  if ( iHdl >= 0 ) and ( iHdl < Count ) Then
    Result := TLeitor( Objects[iHdl] )
  else
    Result := nil;
end;
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
//   Fun��es Principais
//   
function Leitor_Listar( var aBuff : AnsiString ) : Integer;
begin
  aBuff := _z_ListaLeitor.CommaText;
  Result := 0;
end;

//----------------------------------------------------------------------------
function Leitor_Abrir( sModelo, sPorta, sFoco : PChar ) : Integer;
var
  aLeitor : TLeitor;
  sRet    : String;
  sChave  : String;
begin
  sChave := Format( '{{{%s}}}{{{%s}}}', [sModelo, sPorta] );

  if _z_ListaLeitor.IndexOf( sChave ) < 0 then
  begin
    aLeitor := _z_ListaLeitor.CriaLeitor( StrPas( sModelo ), StrPas( sPorta ) );

    if Assigned( aLeitor ) then
    begin
      sRet := aLeitor.Abrir( StrPas( sPorta ), StrPas( sFoco ) );

      if sRet = '0' then
        Result := _z_ListaLeitor.AddObject( sChave, aLeitor )
      else
      begin
        aLeitor.Free;
        Result := -1;
      end;
    end
    else
      Result := -1;
  end
  else
    Result := -1;
end;

//----------------------------------------------------------------------------
function Leitor_Fechar( iHdl : integer; sPorta : PChar ) : Integer;
var
  aLeitor : TLeitor;
begin
  aLeitor := _z_ListaLeitor.Acha( iHdl );

  if Assigned( aLeitor ) then
  begin
    aLeitor.Fechar( StrPas( sPorta ) );

    ////////////////////////////////////////////////////////////////////////////
    //                       *** Problema encontrado: ***                     //
    // A classe _z_ListaLeitor do tipo TStringList � criada e inicializada    //
    // quando o fonte LeitorMain � acessado pela primeira vez. Com isso esta  //
    // classe s� � criada na se��o implementation, ou seja, n�o ser� mais     //
    // criada nem inicializada. Logo, se for chamado a fun��o Free (destruir  //
    // o objeto e seus filhos) a classe _z_ListaLeitor n�o estar� mais        //
    // dispon�vel, at� que a aplica��o seja encerrada. Por isso quando �      //
    // chamado novamente a fun��o Leitor_Abrir e tenta-se encontrar o         //
    // equipamento e a porta de comunica��o no objeto, um erro � apresentado, //
    // pois j� foi destru�do.                                                 //
    //                                                                        //
    // _z_ListaLeitor.Objects[iHdl].Free;                                     //
    // _z_ListaLeitor.Objects[iHdl] := nil;                                   //
    //                                                                        //
    //                             *** Solu��o: ***                           //
    // Deletar o objeto que est� dentro da classe _z_ListaLeitor e n�o        //
    // destru�-lo. Assim a classe estar� sempre dispon�vel, criada e          //
    // inicializada.                                                          //
    //                                                                        //
    // _z_ListaLeitor.Delete( iHdl );                                         //
    ////////////////////////////////////////////////////////////////////////////

    _z_ListaLeitor.Delete( iHdl );

    Result := 0
  end
  else
    Result := 1;
end;

//----------------------------------------------------------------------------
function Leitor_Foco( iHdl : integer; Modo : Integer ) : Integer;
var
  aLeitor : TLeitor;
begin
  aLeitor := _z_ListaLeitor.Acha( iHdl );

  if Assigned( aLeitor ) then
  begin
    aLeitor.LeitorFoco( Modo );
    Result := 0;
  end
  else
    Result := 1;
end;
//
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------
initialization
  _z_ListaLeitor := TListaLeitor.Create;

  //----------------------------------------------------------------------------
finalization
  _z_ListaLeitor.Free;

end.
