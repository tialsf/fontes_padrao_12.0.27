Release TOTVSAPI.DLL (ID 1.0.6.0)
-----------------------------------

- 03/08/10:
	  
	Implementada as rotinas do Paf-Ecf para as Epson que foram desenvolvidas na TotvsApi.dll


Release TOTVSAPI.DLL (ID 1.0.6.0)
-----------------------------------

- 19/05/10:
  
	Homologada a clisitef para tef 2.0



Release TOTVSAPI.DLL (ID 1.0.5.0)
-----------------------------------

- 15/03/10:
  
	Homologada a impressora fiscal ITAUTEC QW PRINTER 6000 MT2 V01.00.05.


 Release TOTVSAPI.DLL (ID 1.0.4.0)
-----------------------------------

- 06/10/09:
  
	Homologada a impressora fiscal EPSON TM-T81FB V01.07.00.


 Release TOTVSAPI.DLL (ID 1.0.3.0)
-----------------------------------

- 24/07/09:
  
	Homologado concentrador de bomba de combust�vel APE da Ionics.


 Release TOTVSAPI.DLL (ID 1.0.2.0)
-----------------------------------

- 06/07/09:
  
	Homologada a impressora fiscal Bematech MP-3000 TH FI V01.01.01


 Release TOTVSAPI.DLL (ID 1.0.1.0)
-----------------------------------

- 10/12/08:
  
	Homologada a impressora fiscal EPSON TM-T88FB V01.06.00


 Release TOTVSAPI.DLL (ID 1.0.1.0)
-----------------------------------

- 17/04/09:
  
	Vers�o para manter compatibilidade com as dlls autocom, zlib e advplce.
