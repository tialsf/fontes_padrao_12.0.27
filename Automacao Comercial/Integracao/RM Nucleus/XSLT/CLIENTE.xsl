<?xml version='1.0' encoding='utf-8' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoFCFO.xsl"/>
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="normalize-space(TOTVSIntegrator/DocName)"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/@Operation = '4'">A</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/@Operation = '5'">E</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueDataServer>FinCFODataBR</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<FCFO>
					<!--Elements que representam a chave primária (CODCOLIGADA,CODCFO) precisam existir mesmo vazios-->
					<CODCOLIGADA>
						<xsl:choose>
							<xsl:when test="boolean(normalize-space(TOTVSIntegrator/DocCompany))">
								<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</CODCOLIGADA>
					<CODCFO>
						<xsl:if test="not(normalize-space(TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CODINT/value)='')">
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CODINT/value"/>
						</xsl:if>
					</CODCFO>
					<!--Element CODINTEGRACAO não existe na tabela FCFO mas será usado para relacionar os códigos-->
					<CODINTEGRACAO>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_COD/value"/>
					</CODINTEGRACAO>
					<!-- Demais elements são opcionais -->
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_NREDUZ/value">
						<NOMEFANTASIA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_NREDUZ/value"/>
						</NOMEFANTASIA>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_NOME/value">
						<NOME>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_NOME/value"/>
						</NOME>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CGC/value">
						<CGCCFO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CGC/value"/>
						</CGCCFO>
					</xsl:if>
					<PAGREC>1</PAGREC>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_END/value">
						<RUA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_END/value"/>
						</RUA>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_BAIRRO/value">
						<BAIRRO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_BAIRRO/value"/>
						</BAIRRO>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_MUN/value">
						<CIDADE>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_MUN/value"/>
						</CIDADE>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_EST/value">
						<CODETD>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_EST/value"/>
						</CODETD>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_COD_MUN/value">
						<CODMUNICIPIO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_COD_MUN/value"/>
						</CODMUNICIPIO>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CEP/value">
						<CEP>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CEP/value"/>
						</CEP>
					</xsl:if>
					<xsl:variable name="_ddi" select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_DDI/value"/>
					<xsl:variable name="_ddd" select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_DDD/value"/>
					<xsl:variable name="_tel" select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_TEL/value"/>
					<xsl:variable name="_tel_completo" select="concat(substring($_ddi, string-length($_ddi)-1, 2), substring($_ddd, string-length($_ddd)-1, 2), substring($_tel, string-length($_tel)-7, 8))"/>
					<xsl:variable name="_fax" select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_FAX/value"/>
					<xsl:variable name="_fax_completo" select="concat(substring($_ddi, string-length($_ddi)-1, 2), substring($_ddd, string-length($_ddd)-1, 2), substring($_fax, string-length($_fax)-7, 8))"/>
					<xsl:if test="$_tel != ''">
						<TELEFONE>
							<xsl:value-of select="$_tel_completo"/>
						</TELEFONE>
					</xsl:if>
					<xsl:if test="$_fax != ''">
						<FAX>
							<xsl:value-of select="$_fax_completo"/>
						</FAX>
					</xsl:if>
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_EMAIL/value)">
						<EMAIL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_EMAIL/value"/>
						</EMAIL>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CONTATO/value">
						<CONTATO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_CONTATO/value"/>
						</CONTATO>
					</xsl:if>
					<ATIVO>1</ATIVO>
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_PESSOA/value)">
						<PESSOAFISOUJUR>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CLIENTES/FormModel_SA1/A1_PESSOA/value"/>
						</PESSOAFISOUJUR>
					</xsl:if>
				</FCFO>
				<xsl:call-template name="ExtensaoFCFO"/>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2008. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="ClientePDV" userelativepaths="yes" externalpreview="no" url="..\..\..\Projetos\IntegracaoTOTVS\XML\Protheus\Cliente_inclusao.xml" htmlbaseurl="" outputurl="" processortype="internal" useresolver="no" profilemode="0"
		          profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no"
		          validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\..\XSD\RM\TipoMensagemTOTVS.xsd" destSchemaRoot="TOTVSMessage" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\..\Projetos\IntegracaoTOTVS\XSD\Protheus\header.xsd" srcSchemaRoot="TOTVSIntegrator" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="TOTVSMessage/DocCompany/xsl:value-of" x="252" y="144"/>
				<block path="TOTVSMessage/DocBranch/xsl:value-of" x="292" y="162"/>
				<block path="TOTVSMessage/DocName/xsl:value-of" x="252" y="180"/>
				<block path="TOTVSMessage/OperationType/xsl:choose" x="302" y="204"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[0]" x="256" y="198"/>
				<block path="" x="132" y="144"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[1]" x="256" y="226"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[2]" x="256" y="234"/>
				<block path="TOTVSMessage/Message/FCFO/CODCOLIGADA/xsl:choose" x="302" y="114"/>
				<block path="TOTVSMessage/Message/FCFO/CODCOLIGADA/xsl:choose/boolean[0]" x="256" y="108"/>
				<block path="TOTVSMessage/Message/FCFO/CODCOLIGADA/xsl:choose/boolean[0]/normalize-space[0]" x="210" y="106"/>
				<block path="TOTVSMessage/Message/FCFO/CODCOLIGADA/xsl:choose/xsl:when/xsl:value-of" x="172" y="144"/>
				<block path="TOTVSMessage/Message/FCFO/CODCFO/xsl:if" x="92" y="144"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if" x="52" y="144"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[1]" x="12" y="144"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[2]" x="252" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[3]" x="212" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[4]" x="172" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[5]" x="132" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[6]" x="92" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[7]" x="52" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[8]" x="12" y="104"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[9]/!=[0]" x="246" y="62"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[9]" x="292" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[9]/TELEFONE/xsl:value-of" x="252" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[10]/!=[0]" x="126" y="62"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[10]" x="172" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[10]/FAX/xsl:value-of" x="212" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[11]/boolean[0]" x="86" y="62"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[11]" x="132" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[12]" x="92" y="64"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[13]/boolean[0]" x="6" y="62"/>
				<block path="TOTVSMessage/Message/FCFO/xsl:if[13]" x="52" y="64"/>
				<block path="TOTVSMessage/Message/xsl:call-template" x="212" y="144"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->