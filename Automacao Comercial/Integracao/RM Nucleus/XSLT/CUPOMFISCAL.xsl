<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoTFMOV.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFITMMOV.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFLOGCAIXA.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFCHEQUE.xsl"/>
	<xsl:template match="/">
	<!-- NOTA: ao utilizar variaveis e operadores sempre deixar um "espaço" em branco antes da referência, caso contrário dará erro na rotina de transformação do TOTVS ESB.
	     Exemplo: <xsl:value-of select="( ( ( $VLIQITEM - $VDESPITEM ) - ( $QTDE * $PUNIT ) ) * ( -1 ) )"/> -->
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/@Operation = '4'">A</xsl:when>
					<!-- o cupom não pode ser excluído, tem que ser CANCELADO -->
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/@Operation = '5'">A</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueDataServer>ConVendaData</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<!-- (RM) VALORLIQUIDO = (soma(preco item x qtde item) - desconto total + acrescimo total), NÃO incluir itens CANCELADOS!
				     (RM) VALORLIQUIDO = (PROTHEUS) F2_VALBRUT, apesar do nome -->
				<xsl:variable name="VLIQ">
					<xsl:choose>
						<xsl:when test="string(number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VALBRUT/value)) != 'NaN'">
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VALBRUT/value"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- (RM) VALORDESC = soma dos descontos dos itens ou desconto aplicado ao cupom, NÃO incluir itens CANCELADOS!
				     (RM) VALORDESC = (PROTHEUS) F2_DESCONT
					 Obs: No Protheus o desconto pode ser por item, por cupom, ou ambos -->
				<xsl:variable name="VDESC">
					<xsl:choose>
						<xsl:when test="string(number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DESCONT/value)) != 'NaN'">
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DESCONT/value"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- (RM) VALORDESP = soma das despesas dos itens ou despesa aplicada ao cupom, NÃO incluir itens CANCELADOS!
				     (RM) VALORDESP = (PROTHEUS) F2_VALACRS
					 Obs: No Protheus o acrescimo é por cupom, com rateio para os itens, sempre virá já rateado para os itens -->
				<xsl:variable name="VACRESC">
					<xsl:choose>
						<xsl:when test="string(number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VALACRS/value)) != 'NaN'">
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VALACRS/value"/>
						</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<TFMOV>
					<!--Elements que representam a chave primária (IDTFMOV, CODCOLIGADA) precisam existir mesmo vazios-->
					<CODCOLIGADA>
						<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
					</CODCOLIGADA>
					<IDTFMOV>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DOC/value)"/>
					</IDTFMOV>
					<NUMEROMOV>
						<xsl:value-of select="format-number(number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DOC/value),'000000')"/>
					</NUMEROMOV>
					<NUMEROCUPOM>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DOC/value)"/>
					</NUMEROCUPOM>
					<COO>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DOC/value)"/>
					</COO>
					<CODFILIAL>
						<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
					</CODFILIAL>
					<!-- Obs: Existe um índice único para os campos: CODCOLIGADA, PONTOVENDA, TIPO, NUMEROMOV, CRO
					     o Adapter usará os campos CODCOLIGADA, PONTOVENDA e SERIE para descobrir o CRO na tabela HCCROINTEGRACAO -->
					<PONTOVENDA>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_PDV/value"/>
					</PONTOVENDA>
					<NUMEROCAIXA>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_PDV/value)"/>
					</NUMEROCAIXA>
					<SERIE>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_SERIE/value"/>
					</SERIE>
					<CRO/>
					<!-- Obs: Quando o cliente for cadastrado dos 2 lados haverá um código no RM e outro do outro lado; 
					     neste caso usar o campo CODINTEGRACAO e não o campo CODCFO. O campo CODINTEGRACAO não existe 
						 na tabela TFMOV, mas será usado pelo adapter para descobrir o CODCFO correspondente 
						 na tabela HCLIENTEINTEGRACAO -->
					<CODCOLCFO>
						<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
					</CODCOLCFO>
					<CODCFO>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_CLIENTE/value"/>
					</CODCFO>
					<TIPO>V</TIPO>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VEND1/value">
						<CODVEN1>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VEND1/value"/>
						</CODVEN1>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VEND2/value">
						<CODVEN2>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_VEND2/value"/>
						</CODVEN2>
					</xsl:if>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value">
						<DATAEMISSAO>
							<xsl:variable name="AAAA" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 1, 4)"/>
							<xsl:variable name="MM" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 5, 2)"/>
							<xsl:variable name="DD" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 7, 2)"/>
							<xsl:value-of select="concat( $AAAA, '-', $MM, '-', $DD)"/>
						</DATAEMISSAO>
					</xsl:if>
					<!-- Ler o valor líquido e as despesas do valor original enviado. Mas os descontos, recalcular para evitar erro no arredondamento. -->
					<VALORLIQUIDO>
						<xsl:value-of select="$VLIQ"/>
					</VALORLIQUIDO>
					<VALORRECEBIDO>
						<xsl:value-of select="$VLIQ"/>
					</VALORRECEBIDO>
					<VALORDESP>
						<xsl:value-of select="$VACRESC"/>
					</VALORDESP>
					<VALORDESC>
						<xsl:value-of select="$VDESC"/>
					</VALORDESC>
					<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value">
						<DATAGERACAO>
							<xsl:variable name="AAAA" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 1, 4)"/>
							<xsl:variable name="MM" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 5, 2)"/>
							<xsl:variable name="DD" select="substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_EMISSAO/value, 7, 2)"/>
							<xsl:value-of select="concat( $AAAA, '-', $MM, '-', $DD)"/>
						</DATAGERACAO>
					</xsl:if>
					<!-- O STATUSMOV deve ser sempre F para ser processado pelo modo server do RM Nucleus
					     Se o cupom estiver cancelado setar o element CANCELADO=1 -->
					<STATUSMOV>F</STATUSMOV>
					<CANCELADO>
						<xsl:choose>
							<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/@Operation = '5'">1</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</CANCELADO>
					<PRIORIDADE>2</PRIORIDADE>
					<CODCPG>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_COND/value"/>
					</CODCPG>
					<xsl:call-template name="ExtensaoTFMOV"/>
				</TFMOV>
				<xsl:for-each select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/ITEM_NOTA/items/item">
					<TFITMMOV>
						<!-- Variaveis locais (por item) -->
						<xsl:variable name="QTDE">
							<xsl:choose>
								<xsl:when test="string(number(D2_QUANT)) != 'NaN'">
									<xsl:value-of select="D2_QUANT"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="PUNIT">
							<xsl:choose>
								<xsl:when test="string(number(D2_PRUNIT)) != 'NaN'">
									<xsl:value-of select="D2_PRUNIT"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="VDESCITEM">
							<xsl:choose>
								<xsl:when test="string(number(D2_DESCON)) != 'NaN'">
									<xsl:value-of select="D2_DESCON"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="VDESPITEM">
							<xsl:choose>
								<xsl:when test="string(number(D2_VALACRS)) != 'NaN'">
									<xsl:value-of select="D2_VALACRS"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="VLIQITEM">
							<xsl:choose>
								<xsl:when test="string(number(D2_TOTAL)) != 'NaN'">
									<xsl:value-of select="D2_TOTAL"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<!--Elements que representam a chave primária (IDTFMOV, NSEQ, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDTFMOV>
							<xsl:value-of select="number(D2_DOC)"/>
						</IDTFMOV>
						<NSEQ>
							<xsl:value-of select="position()"/>
						</NSEQ>
						<CODIGOPRD>
							<xsl:value-of select="D2_COD"/>
						</CODIGOPRD>
						<QUANTIDADE>
							<xsl:value-of select="$QTDE"/>
						</QUANTIDADE>
						<PRECOUNITARIO>
							<xsl:value-of select="$PUNIT"/>
						</PRECOUNITARIO>
						<!-- 
						  Lembrando que no Protheus o desconto pode ser por item, por cupom, ou ambos.
						  E o acrescimo é por cupom, com rateio para os itens, sempre virá já rateado para os itens.
						  Não permite acrescimo no cupom e desconto no cupom ao mesmo tempo.

						  Para evitar erro na função do RM Nucleus que checa o Valor Líquido do Item (ocorre devido a arredondamento), faremos:
						  Se (DescontoCupom = Sim) então DescontoItem = ( $VLIQITEM -  VDESPITEM - ( $QTDE * $PUNIT ) ) * ( -1 ); mesmo se houver acrescimo, não recalcular.
						  Se (DescontoCupom = Não) e (AcrescimoCupomEItem) então AcrescimoItem = ( $VLIQITEM + $VDESCITEM - ( $QTDE * $PUNIT ) ); mesmo se houver desconto, não recalcular.
						-->
						<xsl:choose>
							<xsl:when test="$VDESC > 0">
								<VALORDESC>
									<xsl:value-of select="( ( ( $VLIQITEM - $VDESPITEM ) - ( $QTDE * $PUNIT ) ) * ( -1 ) )"/>
								</VALORDESC>
								<VALORDESP>
									<xsl:value-of select="$VDESPITEM"/>
								</VALORDESP>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="$VDESPITEM > 0">
										<VALORDESC>
											<xsl:value-of select="$VDESCITEM"/>
										</VALORDESC>
										<VALORDESP>
											<xsl:value-of select="( ( $VLIQITEM + $VDESCITEM ) - ( $QTDE * $PUNIT ) )"/>
										</VALORDESP>
									</xsl:when>
									<xsl:otherwise>
										<VALORDESC>
											<xsl:value-of select="$VDESCITEM"/>
										</VALORDESC>
										<VALORDESP>
											<xsl:value-of select="$VDESPITEM"/>
										</VALORDESP>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
						<VALORESCRITURACAO>
							<xsl:value-of select="$VLIQITEM"/>
						</VALORESCRITURACAO>
						<CODUND>
							<xsl:choose>
								<xsl:when test="D2_UM">
									<xsl:value-of select="D2_UM"/>
								</xsl:when>
								<xsl:otherwise>UN</xsl:otherwise>
							</xsl:choose>
						</CODUND>
						<SITUACAOITEM>F</SITUACAOITEM>
						<xsl:call-template name="ExtensaoTFITMMOV"/>
					</TFITMMOV>
				</xsl:for-each>
				<xsl:for-each select="TOTVSIntegrator/Message/Layouts/Content/FormModel_ITENS_CANCELADOS/FormModel_SLX">
					<TFITMMOV>
						<!-- Variaveis locais (por item) -->
						<xsl:variable name="QTDECANC">
							<xsl:choose>
								<xsl:when test="string(number(LX_QTDE)) != 'NaN'">
									<xsl:value-of select="LX_QTDE"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="VLIQCANC">
							<xsl:choose>
								<xsl:when test="string(number(LX_VALOR)) != 'NaN'">
									<xsl:value-of select="LX_VALOR"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="CONTAITENSOK">
							<xsl:value-of select="count(/TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/ITEM_NOTA/items/item)"/>
						</xsl:variable>
						<!--Elements que representam a chave primária (IDTFMOV, NSEQ, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDTFMOV>
							<xsl:value-of select="number(/TOTVSIntegrator/Message/Layouts/Content/FormModel_CUPOM/CAB_NOTA/F2_DOC/value)"/>
						</IDTFMOV>
						<NSEQ>
							<xsl:value-of select="position() + $CONTAITENSOK"/>
						</NSEQ>
						<CODIGOPRD>
							<xsl:value-of select="LX_PRODUTO"/>
						</CODIGOPRD>
						<QUANTIDADE>
							<xsl:value-of select="$QTDECANC"/>
						</QUANTIDADE>
						<PRECOUNITARIO>
							<xsl:choose>
								<xsl:when test="$QTDECANC != 0 and $VLIQCANC != 0">
									<xsl:value-of select="$VLIQCANC div $QTDECANC"/>
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</PRECOUNITARIO>
						<VALORESCRITURACAO>
							<xsl:value-of select="$VLIQCANC"/>
						</VALORESCRITURACAO>
						<CODUND>UN</CODUND>
						<SITUACAOITEM>C</SITUACAOITEM>
					</TFITMMOV>
				</xsl:for-each>
				<xsl:for-each select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CHEQUES/FormModel_SEF">
					<TFCHEQUE>
						<!--Elements que representam a chave primária (IDCHEQUE, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDCHEQUE>
							<!-- O IDCHEQUE precisa de um valor default qualquer, para não dar erro, mas este valor será sempre sobrescrito pelo autoinc -->
							<xsl:value-of select="position()"/>
						</IDCHEQUE>
						<!-- Demais elementos somente se informados na origem -->
						<xsl:if test="EF_VALOR/value">
							<VALOR>
								<xsl:value-of select="EF_VALOR/value"/>
							</VALOR>
						</xsl:if>
						<xsl:if test="EF_DATA/value">
							<DATAEMISSAO>
								<xsl:variable name="AAAA" select="substring(EF_DATA/value, 1, 4)"/>
								<xsl:variable name="MM" select="substring(EF_DATA/value, 5, 2)"/>
								<xsl:variable name="DD" select="substring(EF_DATA/value, 7, 2)"/>
								<xsl:value-of select="concat($AAAA, '-', $MM, '-', $DD)"/>
							</DATAEMISSAO>
						</xsl:if>
						<xsl:if test="EF_VENCTO/value">
							<DATAVENCIMENTO>
								<xsl:variable name="AAAA" select="substring(EF_VENCTO/value, 1, 4)"/>
								<xsl:variable name="MM" select="substring(EF_VENCTO/value, 5, 2)"/>
								<xsl:variable name="DD" select="substring(EF_VENCTO/value, 7, 2)"/>
								<xsl:value-of select="concat( $AAAA, '-', $MM, '-', $DD)"/>
							</DATAVENCIMENTO>
						</xsl:if>
						<xsl:if test="EF_BANCO/value">
							<BANCO>
								<xsl:value-of select="EF_BANCO/value"/>
							</BANCO>
						</xsl:if>
						<xsl:if test="EF_AGENCIA/value">
							<AGENCIA>
								<xsl:value-of select="EF_AGENCIA/value"/>
							</AGENCIA>
						</xsl:if>
						<xsl:if test="EF_CONTA/value">
							<CC>
								<xsl:value-of select="EF_CONTA/value"/>
							</CC>
						</xsl:if>
						<xsl:if test="EF_NUM/value">
							<CHEQUE>
								<xsl:value-of select="EF_NUM/value"/>
							</CHEQUE>
						</xsl:if>
						<xsl:if test="EF_EMITENT/value">
							<NOMEEMITENTE>
								<xsl:value-of select="EF_EMITENT/value"/>
							</NOMEEMITENTE>
						</xsl:if>
						<xsl:if test="EF_TEL/value">
							<TELEMITENTE>
								<xsl:value-of select="EF_TEL/value"/>
							</TELEMITENTE>
						</xsl:if>
						<xsl:call-template name="ExtensaoTFCHEQUE"/>
					</TFCHEQUE>
				</xsl:for-each>
				<xsl:for-each select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CONTASARECEBER/FormModel_SE1">
					<TFLOGCAIXA>
						<!--Elements que representam a chave primária (CODCOLIGADA, IDTFMOV, CODLANCAMENTO) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDTFMOV>
							<xsl:value-of select="number(E1_NUM/value)"/>
						</IDTFMOV>
						<CODLANCAMENTO>
							<xsl:value-of select="position()"/>
						</CODLANCAMENTO>
						<!-- Demais elementos somente se informados na origem -->
						<DEBITOCREDITO>C</DEBITOCREDITO>
						<xsl:if test="E1_VALOR/value">
							<VALOR>
								<xsl:value-of select="E1_VALOR/value"/>
							</VALOR>
						</xsl:if>
						<TIPOLANCAMENTO>1</TIPOLANCAMENTO>
						<xsl:if test="E1_EMISSAO/value">
							<DATAEMISSAO>
								<xsl:variable name="AAAA" select="substring(E1_EMISSAO/value, 1, 4)"/>
								<xsl:variable name="MM" select="substring(E1_EMISSAO/value, 5, 2)"/>
								<xsl:variable name="DD" select="substring(E1_EMISSAO/value, 7, 2)"/>
								<xsl:value-of select="concat( $AAAA, '-', $MM, '-', $DD)"/>
							</DATAEMISSAO>
						</xsl:if>
						<xsl:if test="E1_VENCTO/value">
							<DATAVENCIMENTO>
								<xsl:variable name="AAAA" select="substring(E1_VENCTO/value, 1, 4)"/>
								<xsl:variable name="MM" select="substring(E1_VENCTO/value, 5, 2)"/>
								<xsl:variable name="DD" select="substring(E1_VENCTO/value, 7, 2)"/>
								<xsl:value-of select="concat( $AAAA,'-', $MM, '-', $DD)"/>
							</DATAVENCIMENTO>
						</xsl:if>
						<xsl:if test="E1_TIPO/value">
							<CODFORMAPAGTO>
								<xsl:value-of select="E1_TIPO/value"/>
							</CODFORMAPAGTO>
						</xsl:if>
						<DOLAR>0</DOLAR>
						<TIPOPARCELAMENTO>
							<xsl:choose>
								<xsl:when test="E1_STATUS/value = 'B'">0</xsl:when>
								<xsl:otherwise>9</xsl:otherwise>
							</xsl:choose>
						</TIPOPARCELAMENTO>
						<QTDEPARCELAS>1</QTDEPARCELAS>
						<!-- Se usou cheque no pagamento tem que preencher os dados do cheque -->
						<BANCO>
							<xsl:value-of select="E1_BCOCHQ/value"/>
						</BANCO>
						<AGENCIA>
							<xsl:value-of select="E1_AGECHQ/value"/>
						</AGENCIA>
						<CC>
							<xsl:value-of select="E1_CTACHQ/value"/>
						</CC>
						<CHEQUE>
							<xsl:value-of select="E1_NUMCART/value"/>
						</CHEQUE>
						<TIPOTRANSACAO>1</TIPOTRANSACAO>
						<xsl:if test="E1_DOCTEF/value">
							<NSU>
								<xsl:value-of select="E1_DOCTEF/value"/>
							</NSU>
						</xsl:if>
						<xsl:if test="F2_COND/value">
							<FINALIZACAO>
								<xsl:value-of select="F2_COND/value"/>
							</FINALIZACAO>
						</xsl:if>
						<BAIXADO>
							<xsl:choose>
								<xsl:when test="E1_STATUS/value = 'B'">1</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</BAIXADO>
						<MULTIPLOPAGAMENTO>0</MULTIPLOPAGAMENTO>
						<xsl:call-template name="ExtensaoTFLOGCAIXA"/>
					</TFLOGCAIXA>
				</xsl:for-each>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\PUBLICO\xmls\ProtheusReceived-6672.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\..\..\..\Program Files\TOTVS-ESB\XSD\RM\TipoMensagemTOTVSTestes.xsd" destSchemaRoot="TOTVSMessage" destSchemaPathIsRelative="yes"
		            destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\..\Comum\XSD\Protheus\header.xsd" srcSchemaRoot="TOTVSIntegrator" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="TOTVSMessage/DocCompany/xsl:value-of" x="328" y="144"/>
				<block path="TOTVSMessage/DocBranch/xsl:value-of" x="368" y="162"/>
				<block path="TOTVSMessage/OperationType/xsl:choose" x="378" y="204"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[0]" x="332" y="198"/>
				<block path="" x="328" y="209"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[1]" x="332" y="226"/>
				<block path="TOTVSMessage/OperationType/xsl:choose/=[2]" x="332" y="234"/>
				<block path="TOTVSMessage/Message/TFMOV/CODCOLIGADA/xsl:value-of" x="208" y="169"/>
				<block path="TOTVSMessage/Message/TFMOV/IDTFMOV/xsl:value-of" x="168" y="169"/>
				<block path="TOTVSMessage/Message/TFMOV/NUMEROMOV/xsl:value-of" x="128" y="169"/>
				<block path="TOTVSMessage/Message/TFMOV/NUMEROMOV/xsl:value-of/number[0]" x="82" y="163"/>
				<block path="TOTVSMessage/Message/TFMOV/NUMEROCUPOM/xsl:value-of" x="328" y="89"/>
				<block path="TOTVSMessage/Message/TFMOV/COO/xsl:value-of" x="328" y="49"/>
				<block path="TOTVSMessage/Message/TFMOV/CODFILIAL/xsl:value-of" x="208" y="89"/>
				<block path="TOTVSMessage/Message/TFMOV/NUMEROCAIXA/xsl:value-of" x="48" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/CODCOLCFO/xsl:value-of" x="88" y="169"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if" x="48" y="169"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if[1]" x="368" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if[2]" x="248" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if[2]/DATAEMISSAO/xsl:value-of" x="288" y="89"/>
				<block path="TOTVSMessage/Message/TFMOV/VALORLIQUIDO/xsl:value-of" x="208" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/VALORRECEBIDO/xsl:value-of" x="88" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/VALORDESP/xsl:value-of" x="128" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/VALORDESC/xsl:value-of" x="168" y="129"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if[3]" x="248" y="89"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:if[3]/DATAGERACAO/xsl:value-of" x="368" y="49"/>
				<block path="TOTVSMessage/Message/TFMOV/CANCELADO/xsl:choose" x="98" y="59"/>
				<block path="TOTVSMessage/Message/TFMOV/CANCELADO/xsl:choose/=[0]" x="52" y="53"/>
				<block path="TOTVSMessage/Message/TFMOV/xsl:call-template" x="328" y="396"/>
				<block path="TOTVSMessage/Message/xsl:for-each" x="168" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/CODCOLIGADA/xsl:value-of" x="288" y="49"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/IDTFMOV/xsl:value-of" x="248" y="49"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/NSEQ/xsl:value-of" x="208" y="49"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/QUANTIDADE/xsl:value-of" x="168" y="49"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:value-of" x="48" y="49"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose" x="208" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/&gt;[0]" x="162" y="243"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:when/VALORDESC/xsl:value-of" x="368" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:when/VALORDESP/xsl:value-of" x="168" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose" x="88" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose/&gt;[0]" x="42" y="243"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose/xsl:when/VALORDESC/xsl:value-of" x="128" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose/xsl:when/VALORDESP/xsl:value-of" x="48" y="249"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose/xsl:otherwise/VALORDESC/xsl:value-of" x="328" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:choose/xsl:otherwise/xsl:choose/xsl:otherwise/VALORDESP/xsl:value-of" x="368" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORESCRITURACAO/xsl:value-of" x="208" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/CODUND/xsl:choose" x="288" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/xsl:call-template" x="208" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]" x="128" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/CODCOLIGADA/xsl:value-of" x="168" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/IDTFMOV/xsl:value-of" x="128" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/NSEQ/xsl:value-of" x="88" y="289"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/QUANTIDADE/xsl:value-of" x="328" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/PRECOUNITARIO/xsl:choose" x="98" y="19"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]" x="52" y="13"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]/!=[0]" x="6" y="7"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]/!=[1]" x="6" y="35"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/PRECOUNITARIO/xsl:choose/xsl:when/xsl:value-of" x="368" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[1]/TFITMMOV/VALORESCRITURACAO/xsl:value-of" x="248" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]" x="48" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/CODCOLIGADA/xsl:value-of" x="208" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/IDCHEQUE/xsl:value-of" x="168" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if" x="128" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[1]" x="48" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[1]/DATAEMISSAO/xsl:value-of" x="88" y="329"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[2]" x="288" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[2]/DATAVENCIMENTO/xsl:value-of" x="368" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[3]" x="248" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[4]" x="208" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[5]" x="168" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[6]" x="128" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[7]" x="48" y="369"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:if[8]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[2]/TFCHEQUE/xsl:call-template" x="88" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]" x="248" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/CODCOLIGADA/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/IDTFMOV/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/CODLANCAMENTO/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[1]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[1]/DATAEMISSAO/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[2]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[2]/DATAVENCIMENTO/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[3]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/TIPOPARCELAMENTO/xsl:choose" x="298" y="179"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/TIPOPARCELAMENTO/xsl:choose/=[0]" x="252" y="173"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[4]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[5]" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:if[5]/FINALIZACAO/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/BAIXADO/xsl:choose" x="298" y="179"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/BAIXADO/xsl:choose/=[0]" x="252" y="173"/>
				<block path="TOTVSMessage/Message/xsl:for-each[3]/TFLOGCAIXA/xsl:call-template" x="288" y="169"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->