<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/Extensao_SA1.xsl"/>
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<TOTVSIntegrator>
			<GlobalProduct>
				<xsl:value-of select="TOTVSMessage/GlobalProduct"/>
			</GlobalProduct>
			<GlobalFunctionCode>EAI</GlobalFunctionCode>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSMessage/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocumentFunctionDescription>
				<xsl:value-of select="TOTVSMessage/GlobalDocFunctionDescription"/>
			</GlobalDocumentFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSMessage/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSMessage/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSMessage/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>01</DocCompany>
			<DocBranch>01</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSMessage/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSMessage/DocFederalID"/>
			</DocFederalID>
			<!--Descontinuado no RM, mas necessario no Protheus: 1-Síncrono; 2-ASSíncrono-->			
			<DocType>2</DocType>
			<Message>
				<Layouts>
					<Version>1.01</Version>
					<Identifier>0000000001</Identifier>
					<FunctionCode>LJADPSA1</FunctionCode>
					<Content>
						<FormModel_CLIENTES>
							<xsl:attribute name="Operation">
								<xsl:choose>
									<xsl:when test="TOTVSMessage/OperationType = 'I'">3</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'A'">4</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'E'">5</xsl:when>
								</xsl:choose>
							</xsl:attribute>
							<xsl:attribute name="version">1.01</xsl:attribute>
							<FormModel_SA1>
								<xsl:attribute name="modeltype">GRID</xsl:attribute>
								<!--Não tem que enviar FILIAL porque a tabela é Compartilhada.-->
								<A1_COD>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/FCFO/CODCFO"/>
									</value>
								</A1_COD>
								<A1_LOJA>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>01</value>
								</A1_LOJA>
								<xsl:if test="TOTVSMessage/Message/FCFO/NOME">
									<A1_NOME>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/NOME,1,40)"/>
										</value>
									</A1_NOME>
								</xsl:if>
                                <xsl:if test="TOTVSMessage/Message/FCFO/PESSOAFISOUJUR">
                                    <A1_PESSOA>
                                        <xsl:attribute name="order">1</xsl:attribute>
                                        <value>
                                            <xsl:value-of select="TOTVSMessage/Message/FCFO/PESSOAFISOUJUR"/>
                                        </value>
                                    </A1_PESSOA>
                                </xsl:if>
								<xsl:if test="TOTVSMessage/Message/FCFO/NOMEFANTASIA">
									<A1_NREDUZ>
										<xsl:attribute name="order">1</xsl:attribute>
										<xsl:choose>
											<xsl:when test="boolean(TOTVSMessage/Message/FCFO/NOMEFANTASIA)">
												<value>
													<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/NOMEFANTASIA,1,20)"/>
												</value>
											</xsl:when>
											<xsl:otherwise>
												<value>
													<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/NOME,1,20)"/>
												</value>
											</xsl:otherwise>
										</xsl:choose>
									</A1_NREDUZ>
								</xsl:if>
                                <xsl:if test="TOTVSMessage/Message/FCFO/RUA">
                                    <A1_END>
                                        <xsl:variable name="END">
                                            <xsl:value-of select="substring(concat(TOTVSMessage/Message/FCFO/RUA, ' ', TOTVSMessage/Message/FCFO/NUMERO, ' ', TOTVSMessage/Message/FCFO/COMPLEMENTO), 1, 40)"/>
                                        </xsl:variable>
                                        <xsl:attribute name="order">1</xsl:attribute>
                                        <xsl:choose>
                                            <xsl:when test="$END != ''">
                                                <value>
                                                    <xsl:value-of select="$END"/>
                                                </value>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <value>RUA</value>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </A1_END>
                                </xsl:if>
								<xsl:if test="TOTVSMessage/Message/FCFO/CODETD">
									<A1_EST>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/CODETD"/>
										</value>
									</A1_EST>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CODMUNICIPIO)">
									<A1_COD_MUN>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/CODMUNICIPIO,1,5)"/>
										</value>
									</A1_COD_MUN>
								</xsl:if>
								<xsl:if test="TOTVSMessage/Message/FCFO/CIDADE">
									<A1_MUN>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/CIDADE,1,15)"/>
										</value>
									</A1_MUN>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/BAIRRO)">
									<A1_BAIRRO>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/BAIRRO"/>
										</value>
									</A1_BAIRRO>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CEP)">
									<A1_CEP>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="translate(translate(translate(translate(translate(translate(TOTVSMessage/Message/FCFO/CEP, '.', ''), '/', ''), '-', ''), '(', ''), ')', ''), ' ', '')"/>
										</value>
									</A1_CEP>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/TELEFONE)">
									<xsl:variable name="DDI">
										<xsl:value-of select="translate(translate(translate(translate(translate(translate(TOTVSMessage/Message/FCFO/TELEFONE, '.', ''), '/', ''), '-', ''), '(', ''), ')', ''), ' ', '')"/>
									</xsl:variable>
									<xsl:if test="string-length($DDI) &gt; 11">
										<A1_DDI>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>
												<xsl:value-of select="substring($DDI,string-length($DDI) - 11,2)"/>
											</value>
										</A1_DDI>
									</xsl:if>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/TELEFONE)">
									<xsl:variable name="DDD">
										<xsl:value-of select="translate(translate(translate(translate(translate(translate(TOTVSMessage/Message/FCFO/TELEFONE, '.', ''), '/', ''), '-', ''), '(', ''), ')', ''), ' ', '')"/>
									</xsl:variable>
									<xsl:if test="string-length($DDD) &gt; 9">
										<A1_DDD>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>
												<xsl:value-of select="substring($DDD,string-length($DDD) - 9,2)"/>
											</value>
										</A1_DDD>
									</xsl:if>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/TELEFONE)">
									<xsl:variable name="TEL">
										<xsl:value-of select="translate(translate(translate(translate(translate(translate(TOTVSMessage/Message/FCFO/TELEFONE, '.', ''), '/', ''), '-', ''), '(', ''), ')', ''), ' ', '')"/>
									</xsl:variable>
									<xsl:if test="string-length($TEL) &gt; 7">
										<A1_TEL>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>
												<xsl:value-of select="substring($TEL,string-length($TEL) - 7,8)"/>
											</value>
										</A1_TEL>
									</xsl:if>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/FAX)">
									<xsl:variable name="FAX">
										<xsl:value-of select="translate(translate(translate(translate(translate(translate(TOTVSMessage/Message/FCFO/FAX, '.', ''), '/', ''), '-', ''), '(', ''), ')', ''), ' ', '')"/>
									</xsl:variable>
									<xsl:if test="string-length($FAX) &gt; 7">
										<A1_FAX>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>
												<xsl:value-of select="substring($FAX,string-length($FAX) - 7,8)"/>
											</value>
										</A1_FAX>
									</xsl:if>
								</xsl:if>
								<xsl:variable name="ENDCOB">
									<xsl:value-of select="substring(concat(TOTVSMessage/Message/FCFO/RUAPGTO, TOTVSMessage/Message/FCFO/NUMEROPGTO, TOTVSMessage/Message/FCFO/COMPLEMENTOPGTO), 1, 40)"/>
								</xsl:variable>
								<xsl:if test="$ENDCOB != ''">
									<A1_ENDCOB>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(concat(TOTVSMessage/Message/FCFO/RUAPGTO,TOTVSMessage/Message/FCFO/NUMEROPGTO,TOTVSMessage/Message/FCFO/COMPLEMENTOPGTO),1,40)"/>
										</value>
									</A1_ENDCOB>
								</xsl:if>
								<xsl:variable name="ENDENT">
									<xsl:value-of select="substring(concat(TOTVSMessage/Message/FCFO/RUAENTREGA, TOTVSMessage/Message/FCFO/NUMEROENTREGA, TOTVSMessage/Message/FCFO/COMPLEMENTREGA), 1, 40)"/>
								</xsl:variable>
								<xsl:if test="$ENDENT != ''">
									<A1_ENDENT>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(concat(TOTVSMessage/Message/FCFO/RUAENTREGA,TOTVSMessage/Message/FCFO/NUMEROENTREGA,TOTVSMessage/Message/FCFO/COMPLEMENTREGA),1,40)"/>
										</value>
									</A1_ENDENT>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CONTATO)">
									<A1_CONTATO>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/CONTATO,1,15)"/>
										</value>
									</A1_CONTATO>
								</xsl:if>
                                <xsl:if test="boolean(TOTVSMessage/Message/FCFO/CGCCFO)">
                                    <A1_CGC>
                                        <xsl:attribute name="order">1</xsl:attribute>
                                        <value>
                                            <xsl:if test="TOTVSMessage/Message/FCFO/PESSOAFISOUJUR = 'F'">
                                                <xsl:value-of select="concat(substring(TOTVSMessage/Message/FCFO/CGCCFO,1,3),substring(TOTVSMessage/Message/FCFO/CGCCFO,5,3),substring(TOTVSMessage/Message/FCFO/CGCCFO,9,3),substring(TOTVSMessage/Message/FCFO/CGCCFO,13,2))"/>
                                            </xsl:if>
                                            <xsl:if test="TOTVSMessage/Message/FCFO/PESSOAFISOUJUR = 'J'">
                                                <xsl:value-of select="concat(substring(TOTVSMessage/Message/FCFO/CGCCFO,1,2),substring(TOTVSMessage/Message/FCFO/CGCCFO,4,3),substring(TOTVSMessage/Message/FCFO/CGCCFO,8,3),substring(TOTVSMessage/Message/FCFO/CGCCFO,12,4),substring(TOTVSMessage/Message/FCFO/CGCCFO,17,2))"/>
                                            </xsl:if>
                                        </value>
                                    </A1_CGC>
                                </xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/INSCRESTADUAL)">
									<A1_INSCR>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="translate(translate(translate(TOTVSMessage/Message/FCFO/INSCRESTADUAL,'.',''),'/',''),'-','')"/>
										</value>
									</A1_INSCR>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/BAIRROPGTO)">
									<A1_BAIRROC>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/BAIRROPGTO"/>
										</value>
									</A1_BAIRROC>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CEPPGTO)">
									<A1_CEPC>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="concat(substring(TOTVSMessage/Message/FCFO/CEPPGTO,1,5),substring(TOTVSMessage/Message/FCFO/CEPPGTO,7,3))"/>
										</value>
									</A1_CEPC>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CIDADEPGTO)">
									<A1_MUNC>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/CIDADEPGTO,1,15)"/>
										</value>
									</A1_MUNC>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CODETDPGTO)">
									<A1_ESTC>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/CODETDPGTO"/>
										</value>
									</A1_ESTC>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/BAIRROENTREGA)">
									<A1_BAIRROE>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/BAIRROENTREGA"/>
										</value>
									</A1_BAIRROE>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CEPENTREGA)">
									<A1_CEPE>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="concat(substring(TOTVSMessage/Message/FCFO/CEPENTREGA,1,5),substring(TOTVSMessage/Message/FCFO/CEPENTREGA,7,3))"/>
										</value>
									</A1_CEPE>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CIDADEENTREGA)">
									<A1_MUNE>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/FCFO/CIDADEENTREGA,1,15)"/>
										</value>
									</A1_MUNE>
								</xsl:if>
								<xsl:if test="boolean(TOTVSMessage/Message/FCFO/CODETDENTREGA)">
									<A1_ESTE>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="TOTVSMessage/Message/FCFO/CODETDENTREGA"/>
										</value>
									</A1_ESTE>
								</xsl:if>
								<!-- A1_MSBLQL (Bloqueado): 1=sim 2=nao -->
								<A1_MSBLQL>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:choose>
											<!-- ATIVO: 0=Inativo 1=Ativo -->
											<!-- CFOIMOB: 0=Ativo 1=bloqueado -->
											<xsl:when test="(TOTVSMessage/Message/FCFO/ATIVO = 0) or (TOTVSMessage/Message/FCFO/CFOIMOB = 1)">1</xsl:when>
											<xsl:otherwise>2</xsl:otherwise>
										</xsl:choose>
									</value>
								</A1_MSBLQL>
								<xsl:call-template name="Extensao_SA1"/>
							</FormModel_SA1>
						</FormModel_CLIENTES>
					</Content>
				</Layouts>
			</Message>
		</TOTVSIntegrator>
	</xsl:template>
	<xsl:template name="limpaString">
		<xsl:variable name="string_a">
			<!-- Pega antes do espaço -->
			<!--xsl:value-of select="substring-before( . , ' ' )"/-->
			<xsl:value-of select="TOTVSMessage/Message/FCFO/TELEFONE"/>
		</xsl:variable>
		<xsl:variable name="string_b">
			<!-- Tira o ponto -->
			<xsl:value-of select="translate( $string_a , '.' , '' ) "/>
		</xsl:variable>
		<xsl:variable name="string_c">
			<!-- Tira o traço -->
			<xsl:value-of select="translate( $string_b , '-' , '' ) "/>
		</xsl:variable>
		<xsl:variable name="string_d">
			<!-- Tira a barra -->
			<xsl:value-of select="translate( $string_c , '/' , '' ) "/>
		</xsl:variable>
		<xsl:variable name="string_e">
			<!-- Tira a barra -->
			<xsl:value-of select="translate( $string_d , '(' , '' ) "/>
		</xsl:variable>
		<xsl:variable name="string_f">
			<!-- Tira a barra -->
			<xsl:value-of select="translate( $string_e , ')' , '' ) "/>
		</xsl:variable>
		<xsl:variable name="string_g">
			<!-- Tira a barra -->
			<xsl:value-of select="translate( $string_f , ' ' , '' ) "/>
		</xsl:variable>
		<xsl:value-of select="$string_g"/>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2008. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="clientes_RM" userelativepaths="yes" externalpreview="no" url="..\..\..\..\Users\ericabrandao\AppData\Local\Microsoft\Windows\Temporary Internet Files\XML\Rm\FinCFODataBR1.xml" htmlbaseurl="" outputurl=""
		          processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline=""
		          postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\..\XSD\Protheus\header.xsd" destSchemaRoot="TOTVSIntegrator" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\XSD\Rm\TipoMensagemTOTVS.xsd" srcSchemaRoot="TOTVSMessage" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/xsl:attribute/xsl:choose" x="293" y="92"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/xsl:attribute/xsl:choose/=[0]" x="247" y="86"/>
				<block path="" x="243" y="4"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/xsl:attribute/xsl:choose/=[1]" x="247" y="114"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/xsl:attribute/xsl:choose/=[2]" x="247" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if" x="123" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_NOME/value/xsl:value-of" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[1]" x="83" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[1]/A1_NREDUZ/xsl:choose" x="83" y="202"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[1]/A1_NREDUZ/xsl:choose/boolean[0]" x="37" y="196"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[1]/A1_NREDUZ/xsl:choose/xsl:otherwise/value/xsl:value-of" x="43" y="202"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_END/xsl:choose" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_END/xsl:choose/!=[0]" x="197" y="45"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_END/xsl:choose/xsl:when/value/xsl:value-of" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[2]/boolean[0]" x="0" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[2]" x="43" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[2]/A1_COD_MUN/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MUN/xsl:choose" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MUN/xsl:choose/boolean[0]" x="197" y="45"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MUN/xsl:choose/xsl:when/value/xsl:value-of" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[3]/boolean[0]" x="247" y="59"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[3]" x="293" y="61"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[4]/boolean[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[4]" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[4]/A1_CEP/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[4]/A1_CEP/value/xsl:value-of/substring[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[4]/A1_CEP/value/xsl:value-of/substring[1]" x="197" y="144"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]/boolean[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]/xsl:if/&gt;[0]" x="117" y="9"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]/xsl:if/&gt;[0]/string-length[0]" x="71" y="3"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]/xsl:if" x="163" y="11"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[5]/xsl:if/A1_DDI/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]/boolean[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]/xsl:if/&gt;[0]" x="77" y="9"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]/xsl:if/&gt;[0]/string-length[0]" x="31" y="3"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]/xsl:if" x="123" y="11"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[6]/xsl:if/A1_DDD/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]/boolean[0]" x="207" y="20"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]" x="83" y="11"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]/xsl:if/&gt;[0]" x="117" y="50"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]/xsl:if/&gt;[0]/string-length[0]" x="71" y="44"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]/xsl:if" x="163" y="52"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[7]/xsl:if/A1_TEL/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]/boolean[0]" x="87" y="10"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]" x="43" y="11"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]/xsl:if/&gt;[0]" x="157" y="40"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]/xsl:if/&gt;[0]/string-length[0]" x="111" y="34"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]/xsl:if" x="203" y="42"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[8]/xsl:if/A1_FAX/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[9]/!=[0]" x="117" y="10"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[9]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[9]/A1_ENDCOB/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[9]/A1_ENDCOB/value/xsl:value-of/concat[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[10]/!=[0]" x="197" y="160"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[10]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[10]/A1_ENDENT/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[10]/A1_ENDENT/value/xsl:value-of/concat[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[11]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[11]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[11]/A1_CONTATO/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/=[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/xsl:value-of" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/xsl:value-of/substring[0]" x="197" y="45"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/xsl:value-of/substring[1]" x="197" y="73"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/xsl:value-of/substring[2]" x="197" y="81"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if/xsl:value-of/substring[3]" x="197" y="89"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/=[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of/substring[0]" x="197" y="45"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of/substring[1]" x="197" y="73"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of/substring[2]" x="197" y="81"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of/substring[3]" x="197" y="89"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_CGC/value/xsl:if[1]/xsl:value-of/substring[4]" x="197" y="97"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[12]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[12]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[12]/A1_INSCR/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[12]/A1_INSCR/value/xsl:value-of/translate[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[12]/A1_INSCR/value/xsl:value-of/translate[0]/translate[0]" x="151" y="110"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[13]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[13]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[14]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[14]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[14]/A1_CEPC/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[14]/A1_CEPC/value/xsl:value-of/substring[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[14]/A1_CEPC/value/xsl:value-of/substring[1]" x="197" y="144"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[15]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[15]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[15]/A1_MUNC/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[16]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[16]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[17]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[17]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[18]/boolean[0]" x="197" y="49"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[18]" x="243" y="51"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[18]/A1_CEPE/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[18]/A1_CEPE/value/xsl:value-of/substring[0]" x="197" y="116"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[18]/A1_CEPE/value/xsl:value-of/substring[1]" x="197" y="144"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[19]/boolean[0]" x="0" y="170"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[19]" x="43" y="172"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[19]/A1_MUNE/value/xsl:value-of" x="243" y="122"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[20]/boolean[0]" x="117" y="200"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:if[20]" x="163" y="202"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MSBLQL/value/xsl:if/=[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MSBLQL/value/xsl:if" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MSBLQL/value/xsl:if[1]/=[0]" x="167" y="19"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/A1_MSBLQL/value/xsl:if[1]" x="213" y="21"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SA1/CLIENTES_SA1/xsl:call-template" x="123" y="122"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->
