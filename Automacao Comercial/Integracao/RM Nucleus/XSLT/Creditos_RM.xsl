<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<TOTVSIntegrator>
			<GlobalProduct>
				<xsl:value-of select="TOTVSMessage/GlobalProduct"/>
			</GlobalProduct>
			<GlobalFunctionCode>EAI</GlobalFunctionCode>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSMessage/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocumentFunctionDescription>
				<xsl:value-of select="TOTVSMessage/GlobalDocFunctionDescription"/>
			</GlobalDocumentFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSMessage/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSMessage/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSMessage/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="format-number(TOTVSMessage/DocCompany,'00')"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="format-number(TOTVSMessage/DocBranch,'00')"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSMessage/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSMessage/DocFederalID"/>
			</DocFederalID>
			<!--Descontinuado no RM, mas necessario no Protheus: 1-S�ncrono; 2-ASS�ncrono-->			
			<DocType>2</DocType>
			<Message>
				<Layouts>
					<Version>1.01</Version>
					<Identifier>0000000001</Identifier>
					<FunctionCode>LJADPNCC</FunctionCode>
					<Content>
						<FormModel_CONTASARECEBER>
							<xsl:attribute name="Operation">
								<xsl:choose>
									<xsl:when test="TOTVSMessage/OperationType = 'I'">3</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'A'">4</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'E'">5</xsl:when>
								</xsl:choose>
							</xsl:attribute>
							<xsl:attribute name="version">1.01</xsl:attribute>
							<FormModel_SE1>
								<xsl:attribute name="modeltype">FIELDS</xsl:attribute>
								<E1_NUM>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="format-number(TOTVSMessage/Message/CREDITO/NUMEROCUPOM, '000000')"/>
									</value>
								</E1_NUM>
								<E1_PREFIXO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/SERIEINTEGRACAO"/>
									</value>
								</E1_PREFIXO>
								<E1_PARCELA>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/PARCELA"/>
									</value>
								</E1_PARCELA>
								<E1_TIPO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>NCC</value>
								</E1_TIPO>
								<E1_CLIENTE>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/CODCFO"/>
									</value>
								</E1_CLIENTE>
								<E1_LOJA>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>01</value>
								</E1_LOJA>
								<E1_EMISSAO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/DATAEMISSAO"/>
									</value>
								</E1_EMISSAO>
								<E1_VENCTO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/DATAEMISSAO"/>
									</value>
								</E1_VENCTO>
								<E1_VENCREA>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/DATAEMISSAO"/>
									</value>
								</E1_VENCREA>
								<E1_VENCORI>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/DATAEMISSAO"/>
									</value>
								</E1_VENCORI>
								<E1_VALOR>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/VALORLIQUIDO"/>
									</value>
								</E1_VALOR>
								<E1_SALDO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/VALORLIQUIDO"/>
									</value>
								</E1_SALDO>
								<E1_VLCRUZ>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/CREDITO/VALORLIQUIDO"/>
									</value>
								</E1_VLCRUZ>
								<E1_STATUS>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>A</value>
								</E1_STATUS>
							</FormModel_SE1>
						</FormModel_CONTASARECEBER>
					</Content>
				</Layouts>
			</Message>
		</TOTVSIntegrator>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2008. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Produtos_RM" userelativepaths="yes" externalpreview="no" url="..\..\..\..\temp\DEBUG1_452.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength=""
		          urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\..\XSD\Protheus\header.xsd" destSchemaRoot="TOTVSIntegrator" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\..\..\RM.NET\Source\Con-Conector\Projeto\Comum\XSD\TipoMensagemTOTVSALTERADO.xsd" srcSchemaRoot="TOTVSMessage" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="TOTVSIntegrator/DocCompany/xsl:value-of" x="151" y="162"/>
				<block path="TOTVSIntegrator/DocBranch/xsl:value-of" x="368" y="88"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/xsl:attribute[1]/xsl:choose" x="258" y="179"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/xsl:attribute[1]/xsl:choose/=[0]" x="212" y="173"/>
				<block path="" x="71" y="218"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/xsl:attribute[1]/xsl:choose/=[1]" x="212" y="201"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/xsl:attribute[1]/xsl:choose/=[2]" x="212" y="209"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_NUM/value/xsl:value-of" x="48" y="209"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_PREFIXO/value/xsl:value-of" x="88" y="88"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_PARCELA/value/xsl:value-of" x="328" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_CLIENTE/value/xsl:value-of" x="368" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_EMISSAO/value/xsl:value-of" x="288" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_VENCTO/value/xsl:value-of" x="168" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_VENCREA/value/xsl:value-of" x="128" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_VENCORI/value/xsl:value-of" x="88" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_VALOR/value/xsl:value-of" x="48" y="48"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_SALDO/value/xsl:value-of" x="328" y="128"/>
				<block path="TOTVSIntegrator/Message/Layouts/Content/FormModel_SE1/CONTASARECEBER_SE1/E1_VLCRUZ/value/xsl:value-of" x="368" y="128"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->
