<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoTPONTOVENDA.xsl"/>
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/@Operation = '4'">A</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/@Operation = '5'">E</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueType>2</QueueType>
			<QueueDataServer>ConPontoVendaData</QueueDataServer>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<TPONTOVENDA>
					<!--Elements que representam a chave primária (CODCOLIGADA,CODGRUPO,PONTOVENDA) precisam existir mesmo vazios-->
					<CODCOLIGADA>
						<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
					</CODCOLIGADA>
					<CODFILIAL>
						<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
					</CODFILIAL>
					<PONTOVENDA>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/FormModel_SLG/LG_PDV/value"/>
					</PONTOVENDA>
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/FormModel_SLG/LG_NOME/value)">
						<DESCRICAO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/FormModel_SLG/LG_NOME/value"/>
						</DESCRICAO>
					</xsl:if>
					<!-- Serie -->
					<SERIE>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_CADASTRODEESTACAO/FormModel_SLG/LG_SERIE/value"/>					
					</SERIE>					
					<!-- GRUPO sempre será preenchido no EAI -->
					<!-- CODTMV, CODTMVRECEB, CODTMVDEVOLUCAO, CODTMVPEDIDO, CODTMVTRANSF e CODLOC podem ter valores default definidos na HCTRATAMENTOESPECIFICO -->
					<!-- TMV para cupom fiscal -->
					<CODTMV/>
					<!-- TMV para recebimento -->
					<CODTMVRECEB/>
					<!-- TMV para devolução -->
					<CODTMVDEVOLUCAO/>
					<!-- TMV para sangria -->
					<CODTMVPEDIDO/>
					<!-- TMV para suprimento -->
					<CODTMVTRANSF/>
					<!-- Local de Estoque -->
					<CODLOC/>
					<xsl:call-template name="ExtensaoTPONTOVENDA"/>
				</TPONTOVENDA>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet>