<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/" name="Extensao_SA1">
		<!--<A1_EXEMPLO>
			<xsl:attribute name="order">1</xsl:attribute>
			<value>Exemplo Extensao</value>
		</A1_EXEMPLO>-->
		<!-- Colágua -->
		<A1_YTIPO>
			<value>
				<xsl:value-of select="TOTVSMessage/Message/FCFO/CODTCF"/>
			</value>
		</A1_YTIPO>
		<A1_YTANQUE>
			<value>
				<xsl:value-of select="TOTVSMessage/Message/FCFOCOMPL/TANQUE"/>
			</value>
		</A1_YTANQUE>
		<A1_YLATAO>
			<value>
				<xsl:value-of select="TOTVSMessage/Message/FCFOCOMPL/CODFOLHALEITE"/>
			</value>
		</A1_YLATAO>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Extensao_SA1" userelativepaths="yes" externalpreview="no" url="..\..\..\XSD\Protheus\clientes_sa1.xsd" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth=""
		          profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal"
		          customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->