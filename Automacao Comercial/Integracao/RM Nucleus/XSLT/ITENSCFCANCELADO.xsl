﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="ExtensaoTFMOV.xsl" />
	<xsl:import href="ExtensaoTFITMMOV.xsl" />
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct" />
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode" />
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription" />
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion" />
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="substring-before(TOTVSIntegrator/DocDateTime, 'Z')" />
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier" />
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)" />
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)" />
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName" />
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID" />
			</DocFederalID>
			<OperationType>I</OperationType>
			<QueueDataServer>MovCupomFiscalData</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<QueueRefs>
				<xsl:value-of select="concat('TMOV.NUMEROCUPOM=', TOTVSIntegrator/Message/Layouts/Content/FormModel_ITENS_CANCELADOS/FormModel_SLX/LX_CUPOM/value)" />
			</QueueRefs>
			<Message>
				<!-- variável para calcular o valor liquido do cupom -->
				<xsl:variable name="VLIQCUPOM">
					<xsl:value-of select="sum(/TOTVSIntegrator/Message/Layouts/Content/FormModel_ITENS_CANCELADOS/FormModel_SLX/LX_VALOR/value)" />
				</xsl:variable>
				<!-- outras variaveis, repetir por item -->
				<xsl:for-each select="/TOTVSIntegrator/Message/Layouts/Content/FormModel_ITENS_CANCELADOS/FormModel_SLX">
					<!-- Início variaveis -->
					<xsl:variable name="CUPOM">
						<xsl:value-of select="LX_CUPOM/value" />
					</xsl:variable>
					<xsl:variable name="COLIGADA">
						<xsl:value-of select="/TOTVSIntegrator/DocCompany" />
					</xsl:variable>
					<xsl:variable name="FILIAL">
						<xsl:value-of select="/TOTVSIntegrator/DocBranch" />
					</xsl:variable>
					<xsl:variable name="PONTOVENDA">
						<xsl:value-of select="LX_PDV/value" />
					</xsl:variable>
					<xsl:variable name="SERIE">
						<xsl:value-of select="LX_SERIE/value" />
					</xsl:variable>
					<!-- Fim variaveis -->
					<!-- montar o cabeçalho, pois só virão itens -->
					<xsl:if test="position() = 1">
						<TCUPOMFISCAL>
							<CODCOLIGADA>
								<xsl:value-of select="number($COLIGADA)" />
							</CODCOLIGADA>
							<CODFILIAL>
								<xsl:value-of select="number(/TOTVSIntegrator/DocBranch)" />
							</CODFILIAL>
							<SERIE>
								<xsl:value-of select="LX_SERIE/value" />
							</SERIE>
							<IDCUPOM>
								<xsl:value-of select="number($CUPOM)" />
							</IDCUPOM>
							<NUMERO>
								<xsl:value-of select="number($CUPOM)" />
							</NUMERO>
							<COO>
								<xsl:value-of select="number($CUPOM)" />
							</COO>
							<!-- Obs: Existe um índice único para os campos: CODCOLIGADA, PONTOVENDA, TIPO, NUMEROMOV, CRO
							     o Adapter usará os campos CODCOLIGADA, PONTOVENDA e SERIE para descobrir o CRO na tabela HCCROINTEGRACAO -->
							<TIPO>V</TIPO>
							<PONTOVENDA>
								<xsl:value-of select="$PONTOVENDA" />
							</PONTOVENDA>
							<CRO />
							<!-- CODCOLCFO e CODCFO são obrigatórios, porém o adapter de recebimento será capaz de usar um default definido nos parâmetros de integração-->
							<CODCOLCFO>0</CODCOLCFO>
							<CODCFO>04916</CODCFO>							
							<!-- VALORLIQUIDO e VALORRECEBIDO não incluem o valor de itens cancelados, 
							     logo se o cupom está todo cancelado, VALORLIQUIDO=0 e VALORRECEBIDO=0 -->
							<VALORLIQUIDO>
								<xsl:value-of select="$VLIQCUPOM" />
							</VALORLIQUIDO>
							<VALORRECEBIDO>
								<xsl:value-of select="$VLIQCUPOM" />
							</VALORRECEBIDO>
							<xsl:if test="LX_DTMOVTO/value">
								<DATAEMISSAO>
									<xsl:variable name="AAAA" select="substring(LX_DTMOVTO/value, 1, 4)" />
									<xsl:variable name="MM" select="substring(LX_DTMOVTO/value, 5, 2)" />
									<xsl:variable name="DD" select="substring(LX_DTMOVTO/value, 7, 2)" />
									<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)" />
								</DATAEMISSAO>
							</xsl:if>
							<!-- O STATUSMOV deve ser sempre F e CANCELADO=1 
							     O RM Nucleus não consegue criar um movimento no status cancelado. Com a configuração a cima irá primeiro incluir e depois cancelar. O lançamento financeiro por exemplo, será automatico -->
							<STATUSMOV>F</STATUSMOV>
							<CANCELADO>1</CANCELADO>
							<!--<PRIORIDADE>2</PRIORIDADE>-->
						</TCUPOMFISCAL>
						<TCUPOMFISCALFIN>
						<IDCUPOM>
							<xsl:value-of select="number($CUPOM)" />
						</IDCUPOM>
						<CODFORMAPAGTO>R$</CODFORMAPAGTO>
						<CODCPG>01</CODCPG>
						<NUMEROPARCELA>1</NUMEROPARCELA>
						<DATAVENCIMENTO>
							<xsl:variable name="AAAA" select="substring(LX_DTMOVTO/value, 1, 4)" />
							<xsl:variable name="MM" select="substring(LX_DTMOVTO/value, 5, 2)" />
							<xsl:variable name="DD" select="substring(LX_DTMOVTO/value, 7, 2)" />
							<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)" />
						</DATAVENCIMENTO>
						<VALOR>
							<xsl:value-of select="$VLIQCUPOM" />
						</VALOR>
						<DEBITOCREDITO>C</DEBITOCREDITO>
						<DATAEMISSAO>
							<xsl:variable name="AAAA" select="substring(LX_DTMOVTO/value, 1, 4)" />
							<xsl:variable name="MM" select="substring(LX_DTMOVTO/value, 5, 2)" />
							<xsl:variable name="DD" select="substring(LX_DTMOVTO/value, 7, 2)" />
							<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)" />
						</DATAEMISSAO>
						<TIPOTRANSACAO>1</TIPOTRANSACAO>
					</TCUPOMFISCALFIN>
					</xsl:if>
					<TCUPOMFISCALITM>
						<!-- Variaveis locais (por item) -->
						<!--Elements que representam a chave primária (IDTMOV, NSEQ, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number($COLIGADA)" />
						</CODCOLIGADA>
						<IDCUPOM>
							<xsl:value-of select="number($CUPOM)" />
						</IDCUPOM>
						<NUMEROITEM>
							<xsl:value-of select="position()" />
						</NUMEROITEM>
						<IDPRD>
							<xsl:value-of select="LX_PRODUTO/value" />
						</IDPRD>
			            <CODIGOPRD>
			              <xsl:value-of select="LX_PRODUTO/value" />
			            </CODIGOPRD>
			<TRIBUTACAOECF />
						<QUANTIDADE>
					       <xsl:choose>
					        <xsl:when test="string(number(LX_QTDE/value)) != 'NaN'">
					         <xsl:value-of select="LX_QTDE/value" />
					        </xsl:when>
					        <xsl:otherwise>0</xsl:otherwise>
					       </xsl:choose>
						</QUANTIDADE>
						<PRECOUNITARIO>
					       <xsl:choose>
					        <xsl:when test="string(number(LX_VRUNIT/value)) != 'NaN'">
					         <xsl:value-of select="LX_QTDE/value" />
					        </xsl:when>
					        <xsl:otherwise>0</xsl:otherwise>
					       </xsl:choose>
						</PRECOUNITARIO>
						<DESCONTO>
							<xsl:choose>
								<xsl:when test="string(number(LX_DESCON/value)) != 'NaN'">
									<xsl:value-of select="LX_DESCON/value" />
								</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</DESCONTO>
						<VALORLIQUIDO>
					       <xsl:choose>
					        <xsl:when test="string(number(LX_VALOR/value)) != 'NaN'">
					         <xsl:value-of select="LX_VALOR/value" />
					        </xsl:when>
					        <xsl:otherwise>0</xsl:otherwise>
					       </xsl:choose>
						</VALORLIQUIDO>
						<!-- o Protheus não envia a unidade utilizada na venda, qdo o cupom foi cancelado. 
						Mas utiliza a mesma informada no cadastro do produto, então vamos setar um valor default na HCTratamentoEspecifico -->
						<CODUND />
						<!-- SITUACAOITEM = F obrigatóriamente 
							 para evitar o erro "movimento sem itens" -->
						<!--<SITUACAOITEM>F</SITUACAOITEM>-->
					</TCUPOMFISCALITM>
				</xsl:for-each>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\temp\ENTRADA\13-11-09\cupomcanc151byErica.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0"
		          profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no"
		          validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\..\..\..\Program Files\TOTVS-ESB\XSD\RM\TipoMensagemTOTVSTestes.xsd" destSchemaRoot="TOTVSMessage" destSchemaPathIsRelative="yes"
		            destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\..\..\Program Files\TOTVS-ESB\XSD\Protheus\header.xsd" srcSchemaRoot="TOTVSIntegrator" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="TOTVSMessage/DocCompany/xsl:value-of" x="328" y="144"/>
				<block path="TOTVSMessage/DocBranch/xsl:value-of" x="368" y="162"/>
				<block path="TOTVSMessage/QueueRefs/xsl:value-of" x="203" y="132"/>
				<block path="TOTVSMessage/Message/xsl:for-each" x="258" y="366"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if" x="368" y="396"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/CODCOLIGADA/xsl:value-of" x="328" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/CODFILIAL/xsl:value-of" x="128" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/IDTMOV/xsl:value-of" x="368" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/NUMEROMOV/xsl:value-of" x="248" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/NUMEROMOV/xsl:value-of/number[0]" x="202" y="203"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/NUMEROCUPOM/xsl:value-of" x="288" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/COO/xsl:value-of" x="88" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/PONTOVENDA/xsl:value-of" x="288" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/NUMEROCAIXA/xsl:value-of" x="48" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/SERIE/xsl:value-of" x="168" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/VALORLIQUIDO/xsl:value-of" x="128" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/VALORRECEBIDO/xsl:value-of" x="88" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/xsl:if" x="208" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/xsl:if/DATAGERACAO/xsl:value-of" x="248" y="129"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/xsl:if[1]" x="168" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/xsl:if[1]/DATAEMISSAO/xsl:value-of" x="208" y="209"/>
				<block path="TOTVSMessage/Message/xsl:for-each/xsl:if/TMOV/xsl:call-template" x="328" y="396"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/CODCOLIGADA/xsl:value-of" x="48" y="169"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/IDTMOV/xsl:value-of" x="368" y="129"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/NSEQ/xsl:value-of" x="288" y="129"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/CODIGOPRD/xsl:value-of" x="208" y="129"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/QUANTIDADE/xsl:value-of" x="168" y="129"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:choose" x="58" y="99"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]" x="12" y="93"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]/!=[0]" x="0" y="87"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:choose/and[0]/!=[1]" x="0" y="115"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/PRECOUNITARIO/xsl:choose/xsl:when/xsl:value-of" x="128" y="129"/>
				<block path="" x="328" y="89"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORDESC/xsl:choose" x="13" y="182"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORDESC/xsl:choose/!=[0]" x="0" y="176"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORDESC/xsl:choose/!=[0]/string[0]" x="0" y="170"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORDESC/xsl:choose/!=[0]/string[0]/number[0]" x="0" y="168"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORDESC/xsl:choose/xsl:when/xsl:value-of" x="83" y="172"/>
				<block path="TOTVSMessage/Message/xsl:for-each/TFITMMOV/VALORESCRITURACAO/xsl:value-of" x="368" y="89"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->