﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="Extensao_SB1.xsl" />
	<xsl:import href="Extensao_SB0.xsl" />
	<xsl:import href="Extensao_SLK.xsl" />
	<xsl:output indent="yes" />
	<xsl:template match="/">
		<TOTVSIntegrator>
			<GlobalProduct>
				<xsl:value-of select="TOTVSMessage/MessageInformation/Product" />
			</GlobalProduct>
			<GlobalFunctionCode>EAI</GlobalFunctionCode>
			<GlobalDocumentFunctionCode>PRODUTO</GlobalDocumentFunctionCode>
			<GlobalDocumentFunctionDescription>Cadastro de Produto</GlobalDocumentFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSMessage/DocVersion" />
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSMessage/DocDateTime" />
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSMessage/DocIdentifier" />
			</DocIdentifier>
			<DocCompany>01</DocCompany>
			<DocBranch>01</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/GCOLIGADA/NOMEFANTASIA" />
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/GCOLIGADA/CGC" />
			</DocFederalID>
			<!--Descontinuado no RM, mas necessario no Protheus-->
			<DocType>1</DocType>
			<Message>
				<Layouts>
					<Version>1.01</Version>
					<Identifier>0000000001</Identifier>
					<FunctionCode>LJADPSB1</FunctionCode>
					<Content>
						<FormModel_PRODUTO>
							<xsl:attribute name="Operation">
								<xsl:choose>
									<xsl:when test="TOTVSMessage/OperationType = 'I'">3</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'A'">4</xsl:when>
									<xsl:when test="TOTVSMessage/OperationType = 'E'">5</xsl:when>
								</xsl:choose>
							</xsl:attribute>
							<xsl:attribute name="version">1.01</xsl:attribute>
							<PRODUTO>
								<xsl:attribute name="modeltype">GRID</xsl:attribute>
								<B1_FILIAL>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>01</value>
								</B1_FILIAL>
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF">
									<B1_TIPTRIB>
										<xsl:attribute name="order">1</xsl:attribute>
										<xsl:choose>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '0' and TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/CODTRB = 'ICMS'">
												<value>T</value>
											</xsl:when>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '0' and TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/CODTRB = 'ISS'">
												<value>S</value>
											</xsl:when>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '1'">
												<value>I</value>
											</xsl:when>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '2'">
												<value>F</value>
											</xsl:when>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '3'">
												<value>N</value>
											</xsl:when>
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = '4'">
												<value>R</value>
											</xsl:when>           
										</xsl:choose>
									</B1_TIPTRIB>
								</xsl:if>
								<!-- B1_MSBLQL (Bloqueado): 1=sim 2=nao -->
								<B1_MSBLQL>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:choose>
											<!-- INATIVO: 0=ativo 1=inativo -->
											<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/INATIVO = 1">1</xsl:when>
											<xsl:otherwise>2</xsl:otherwise>
										</xsl:choose>
									</value>
								</B1_MSBLQL>
								<B1_COD>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/CODIGOPRD" />
									</value>
								</B1_COD>
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/NOMEFANTASIA">
									<B1_DESC>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="substring(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/NOMEFANTASIA,1,30)" />
										</value>
									</B1_DESC>
								</xsl:if>
								<B1_TIPO>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>PA</value>
								</B1_TIPO>
								<B1_CODITE>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/CODIGOPRD" />
									</value>
								</B1_CODITE>
								<B1_UM>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>
										<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/CODUNDCONTROLE" />
									</value>
								</B1_UM>
								<B1_LOCPAD>
									<xsl:attribute name="order">1</xsl:attribute>
									<value>01</value>
								</B1_LOCPAD>
								<xsl:if test="(TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = 0) or (TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = 4)">
									<B1_PICM>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/ALIQUOTA,'00.00')" />
										</value>
									</B1_PICM>
								</xsl:if>
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO1">
									<B1_PRV1>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO1,'0000000.00')" />
										</value>
									</B1_PRV1>
								</xsl:if>
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PESAVEL">
									<B1_BALANCA>
										<xsl:attribute name="order">1</xsl:attribute>
										<value>
											<xsl:choose>
												<xsl:when test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PESAVEL = 1">2</xsl:when>
												<xsl:otherwise>0</xsl:otherwise>
											</xsl:choose>
										</value>
									</B1_BALANCA>
								</xsl:if>
								<xsl:call-template name="Extensao_SB1" />
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO1 or TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO2 or TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO3 or TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO4 or TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO5">
									<PRECO>
										<xsl:attribute name="modeltype">GRID</xsl:attribute>
										<B0_FILIAL>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>01</value>
										</B0_FILIAL>
										<B0_COD>
											<xsl:attribute name="order">1</xsl:attribute>
											<value>
												<xsl:value-of select="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/CODIGOPRD" />
											</value>
										</B0_COD>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO1">
											<B0_PRV1>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO1,'000000000.00')" />
												</value>
											</B0_PRV1>
										</xsl:if>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO2">
											<B0_PRV2>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO2,'000000000.00')" />
												</value>
											</B0_PRV2>
										</xsl:if>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO3">
											<B0_PRV3>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO3,'000000000.00')" />
												</value>
											</B0_PRV3>
										</xsl:if>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO4">
											<B0_PRV4>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO4,'000000000.00')" />
												</value>
											</B0_PRV4>
										</xsl:if>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO5">
											<B0_PRV5>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/PRECO5,'000000000.00')" />
												</value>
											</B0_PRV5>
										</xsl:if>
										<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/TIPO = 4">
											<B0_ALIQRED>
												<xsl:attribute name="order">1</xsl:attribute>
												<value>
													<xsl:value-of select="format-number(TOTVSMessage/Message/BusinessMessage/XMLMain/TTRIBUTACAOECF/ALIQUOTAREDUZIDA,'00.00')" />
												</value>
											</B0_ALIQRED>
										</xsl:if>
										<xsl:call-template name="Extensao_SB0" />
									</PRECO>
								</xsl:if>
								<xsl:if test="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTOCODIGO">
									<CODIGOBARRAS>
										<xsl:attribute name="modeltype">GRID</xsl:attribute>
										<items>
											<xsl:for-each select="TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTOCODIGO">
												<item>
													<xsl:attribute name="id">1</xsl:attribute>
													<xsl:attribute name="deleted">0</xsl:attribute>
													<LK_FILIAL>01</LK_FILIAL>
													<LK_CODBAR>
														<xsl:value-of select="CODIGO" />
													</LK_CODBAR>
													<LK_CODIGO>
														<xsl:value-of select="/TOTVSMessage/Message/BusinessMessage/XMLMain/TPRODUTO/CODIGOPRD" />
													</LK_CODIGO>
													<LK_QUANT>
														<xsl:value-of select="FATORCONVERSAO" />
													</LK_QUANT>
													<xsl:call-template name="Extensao_SLK" />
												</item>
											</xsl:for-each>
										</items>
									</CODIGOBARRAS>
								</xsl:if>
							</PRODUTO>
						</FormModel_PRODUTO>
					</Content>
				</Layouts>
			</Message>
		</TOTVSIntegrator>
	</xsl:template>
</xsl:stylesheet>
