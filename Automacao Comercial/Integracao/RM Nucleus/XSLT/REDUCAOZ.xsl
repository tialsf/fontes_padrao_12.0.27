<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoTFREDUCAOZ.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFREDUCAOZITEM.xsl"/>
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/@Operation = '4'">A</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/@Operation = '5'">E</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueDataServer>ConReducaoZData</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<TFREDUCAOZ>
					<CODCOLIGADA>
						<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
					</CODCOLIGADA>
					<PONTOVENDA>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
					</PONTOVENDA>
					<NUMEROREDUCAO>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
					</NUMEROREDUCAO>
					<DATAMOVIMENTO>
						<!--Converter de YYYYMMDD para YYYY-MM-DD-->
						<xsl:value-of select="concat(substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,1,4),'-',substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,5,2),'-',substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,7,2))"/>
					</DATAMOVIMENTO>
					<DATAREDUCAO>
						<!--Converter de YYYYMMDD para YYYY-MM-DD-->
						<xsl:value-of select="concat(substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,1,4),'-',substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,5,2),'-',substring(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DTMOVTO/value,7,2))"/>
					</DATAREDUCAO>
					<GRANDETOTAL>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_GTFINAL/value"/>
					</GRANDETOTAL>
					<CANCELAMENTOS>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_CANCEL/value"/>
					</CANCELAMENTOS>
					<DESCONTOS>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_DESC/value"/>
					</DESCONTOS>
					<ACRESCIMOS>0</ACRESCIMOS>
					<ACRESCIMOSFIN>0</ACRESCIMOSFIN>
					<!-- Campo COOANT na verdade é Cupom Inicial -->
					<COOANT>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMINI/value)"/>
					</COOANT>
					<!-- Campo COO na verdade é Cupom Final -->
					<COO>
						<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMFIM/value)"/>
					</COO>
					<CNSI>0</CNSI>
					<GRANDETOTALANT>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_GTINI/value"/>
					</GRANDETOTALANT>
					<STATUSREDUCAO>P</STATUSREDUCAO>
					<!-- Campo COORZ na é o COO (Contador de Ordem de Operação) -->
					<COORZ>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_COO/value"/>
					</COORZ>
					<xsl:call-template name="ExtensaoTFREDUCAOZ"/>
				</TFREDUCAOZ>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS7/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>T7</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS7/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS12/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>T12</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS12/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS18/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>T18</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS18/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS25/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>T25</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS25/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_ISENTO/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>I</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_ISENTO/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NTRIB/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>N</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NTRIB/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_SUBTRIB/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>F</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_SUBTRIB/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
				<xsl:if test="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS17/value">
					<TFREDUCAOZITEM>
						<CODCOLIGADA>
							<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
						</CODCOLIGADA>
						<PONTOVENDA>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_PDV/value"/>
						</PONTOVENDA>
						<NUMEROREDUCAO>
							<xsl:value-of select="number(TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_NUMREDZ/value)"/>
						</NUMEROREDUCAO>
						<TRIBUTACAOECF/>
						<CODINTEGRACAO>T17</CODINTEGRACAO>
						<TOTALPARCIAL>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_RESUMOREDUCAOZ/FormModel_SFI/FI_BAS17/value"/>
						</TOTALPARCIAL>
						<xsl:call-template name="ExtensaoTFREDUCAOZITEM"/>
					</TFREDUCAOZITEM>
				</xsl:if>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet>