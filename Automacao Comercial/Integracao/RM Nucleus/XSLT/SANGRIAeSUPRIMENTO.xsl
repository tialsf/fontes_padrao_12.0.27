<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoTFMOV.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFITMMOV.xsl"/>
	<xsl:import href="Extensoes/ExtensaoTFLOGCAIXA.xsl"/>
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_MOVIMENTOBANCARIO/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_MOVIMENTOBANCARIO/@Operation = '4'">A</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_MOVIMENTOBANCARIO/@Operation = '5'">E</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueDataServer>ConVendaData</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<!-- Apesar do for-each, Sangria e Suprimento possuem um único item, coloquei só para facilitar -->
				<xsl:for-each select="TOTVSIntegrator/Message/Layouts/Content/FormModel_MOVIMENTOBANCARIO/FormModel_SE5">
					<TFMOV>
						<!--Elements que representam a chave primária (IDTFMOV, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<!-- Para formar um número único temos que concatenar E5_NUMERO + E5_SEQ -->
						<IDTFMOV>
							<xsl:value-of select="number(concat(E5_NUMERO/value,E5_SEQ/value))"/>
						</IDTFMOV>
						<NUMEROMOV>
							<xsl:value-of select="concat(E5_NUMERO/value, E5_SEQ/value)"/>
						</NUMEROMOV>

						<CODFILIAL>
							<xsl:value-of select="number(/TOTVSIntegrator/DocBranch)"/>
						</CODFILIAL>
						<!-- Campos CODCOLCFO e CODCFO precisam existir, para que o RM Conector consiga setar um cliente default (HCTratamentoEspecifico) e não gere erro no modo server do RM Nucleus)-->
						<CODCOLCFO/>
						<CODCFO/>					
						<!-- Adapter deve buscar o campo PONTOVENDA na tabela HCCROINTEGRACAO passando os campos CODCOLIGADA e SERIE-->
						<SERIE>
							<xsl:value-of select="E5_PREFIXO/value"/>
						</SERIE>
						<PONTOVENDA/>
						<NUMEROCAIXA/>

						<NUMEROCUPOM>
							<xsl:value-of select="number(E5_NUMERO/value)"/>
						</NUMEROCUPOM>
						<COO>
							<xsl:value-of select="number(E5_NUMERO/value)"/>
						</COO>

						<!-- Obs: Existe um índice único para os campos: CODCOLIGADA, PONTOVENDA, TIPO, NUMEROMOV, CRO
					     o Adapter deverá buscar o CRO em uma tabela de integração. -->
						<TIPO>
							<xsl:choose>
								<xsl:when test="E5_NATUREZ/value = 'TROCO'">T</xsl:when>
								<xsl:when test="E5_NATUREZ/value = 'SANGRIA'">P</xsl:when>
							</xsl:choose>
						</TIPO>
						<CRO/>

						<CODCPG/>
						<VALORLIQUIDO>
							<xsl:value-of select="E5_VALOR/value"/>
						</VALORLIQUIDO>
						<VALORRECEBIDO>
							<xsl:value-of select="E5_VALOR/value"/>
						</VALORRECEBIDO>
						<STATUSMOV>F</STATUSMOV>
						<CANCELADO>0</CANCELADO>
						<PRIORIDADE>2</PRIORIDADE>

						<xsl:if test="E5_DATA/value">
							<xsl:variable name="AAAA" select="substring(E5_DATA/value, 1, 4)"/>
							<xsl:variable name="MM" select="substring(E5_DATA/value, 5, 2)"/>
							<xsl:variable name="DD" select="substring(E5_DATA/value, 7, 2)"/>
							<DATAEMISSAO>
								<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)"/>
							</DATAEMISSAO>
							<DATAGERACAO>
								<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)"/>
							</DATAGERACAO>
						</xsl:if>
					</TFMOV>


					<TFITMMOV>
						<!--Elements que representam a chave primária (IDTFMOV, NSEQ, CODCOLIGADA) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDTFMOV>
							<xsl:value-of select="number(concat(E5_NUMERO/value, E5_SEQ/value))"/>
						</IDTFMOV>
						<NSEQ>
							<xsl:value-of select="position()"/>
						</NSEQ>

						<!-- Deverá existir um produto específico 9999999991-Sangria e um 9999999992-Suprimento -->
						<CODIGOPRD>
							<xsl:choose>
								<xsl:when test="E5_NATUREZ/value = 'TROCO'">9999999992</xsl:when>
								<xsl:when test="E5_NATUREZ/value = 'SANGRIA'">9999999991</xsl:when>
							</xsl:choose>
						</CODIGOPRD>

						<QUANTIDADE>1</QUANTIDADE>
						<xsl:if test="E5_VALOR/value">
							<PRECOUNITARIO>
								<xsl:value-of select="E5_VALOR/value"/>
							</PRECOUNITARIO>
							<VALORESCRITURACAO>
								<xsl:value-of select="E5_VALOR/value"/>
							</VALORESCRITURACAO>
						</xsl:if>
						<CODUND>UN</CODUND>
						<SITUACAOITEM>F</SITUACAOITEM>
					</TFITMMOV>

					<TFLOGCAIXA>
						<!--Elements que representam a chave primária (CODCOLIGADA, IDTFMOV, CODLANCAMENTO) precisam existir mesmo vazios-->
						<CODCOLIGADA>
							<xsl:value-of select="number(/TOTVSIntegrator/DocCompany)"/>
						</CODCOLIGADA>
						<IDTFMOV>
							<xsl:value-of select="number(concat(E5_NUMERO/value, E5_SEQ/value))"/>
						</IDTFMOV>
						<CODLANCAMENTO>
							<xsl:value-of select="position()"/>
						</CODLANCAMENTO>
						<DEBITOCREDITO>C</DEBITOCREDITO>
						<VALOR>
							<xsl:value-of select="E5_VALOR/value"/>
						</VALOR>
						<TIPOLANCAMENTO>
							<xsl:choose>
								<xsl:when test="E5_NATUREZ/value = 'TROCO'">3</xsl:when>
								<xsl:when test="E5_NATUREZ/value = 'SANGRIA'">4</xsl:when>
							</xsl:choose>
						</TIPOLANCAMENTO>
						<xsl:if test="E5_DATA/value">
							<xsl:variable name="AAAA" select="substring(E5_DATA/value, 1, 4)"/>
							<xsl:variable name="MM" select="substring(E5_DATA/value, 5, 2)"/>
							<xsl:variable name="DD" select="substring(E5_DATA/value, 7, 2)"/>
							<DATAEMISSAO>
								<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)"/>
							</DATAEMISSAO>
							<DATAVENCIMENTO>
								<xsl:value-of select="concat($AAAA,'-',$MM,'-',$DD)"/>
							</DATAVENCIMENTO>
						</xsl:if>
						<CODFORMAPAGTO>R$</CODFORMAPAGTO>
						<DOLAR>0</DOLAR>
						<TIPOPARCELAMENTO>0</TIPOPARCELAMENTO>
						<QTDEPARCELAS>1</QTDEPARCELAS>
						<TIPOTRANSACAO>1</TIPOTRANSACAO>
						<BAIXADO>0</BAIXADO>
						<MULTIPLOPAGAMENTO>0</MULTIPLOPAGAMENTO>
						<xsl:call-template name="ExtensaoTFLOGCAIXA"/>
					</TFLOGCAIXA>
				</xsl:for-each>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet>