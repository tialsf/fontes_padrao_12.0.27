<?xml version='1.0' encoding='utf-8' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="Extensoes/ExtensaoTVEN.xsl"/>
	<xsl:template match="/">
		<TOTVSMessage>
			<GlobalProduct>
				<xsl:value-of select="TOTVSIntegrator/GlobalProduct"/>
			</GlobalProduct>
			<GlobalDocumentFunctionCode>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionCode"/>
			</GlobalDocumentFunctionCode>
			<GlobalDocFunctionDescription>
				<xsl:value-of select="TOTVSIntegrator/GlobalDocumentFunctionDescription"/>
			</GlobalDocFunctionDescription>
			<DocVersion>
				<xsl:value-of select="TOTVSIntegrator/DocVersion"/>
			</DocVersion>
			<DocDateTime>
				<xsl:value-of select="TOTVSIntegrator/DocDateTime"/>
			</DocDateTime>
			<DocIdentifier>
				<xsl:value-of select="TOTVSIntegrator/DocIdentifier"/>
			</DocIdentifier>
			<DocCompany>
				<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
			</DocCompany>
			<DocBranch>
				<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
			</DocBranch>
			<DocName>
				<xsl:value-of select="TOTVSIntegrator/DocName"/>
			</DocName>
			<DocFederalID>
				<xsl:value-of select="TOTVSIntegrator/DocFederalID"/>
			</DocFederalID>
			<DocType>
				<xsl:value-of select="TOTVSIntegrator/DocType"/>
			</DocType>
			<OperationType>
				<xsl:choose>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/@Operation = '3'">I</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/@Operation = '4'">A</xsl:when>
					<xsl:when test="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/@Operation = '5'">E</xsl:when>
				</xsl:choose>
			</OperationType>
			<QueueDataServer>ConVendedorData</QueueDataServer>
			<QueueType>2</QueueType>
			<QueueStatus>0</QueueStatus>
			<QueueAttempt>0</QueueAttempt>
			<Message>
				<TVEN>
					<!--Elements que representam a chave primária (CODCOLIGADA,CODVEN) precisam existir mesmo vazios-->
					<CODCOLIGADA>
						<xsl:value-of select="number(TOTVSIntegrator/DocCompany)"/>
					</CODCOLIGADA>
					<CODFILIAL>
						<xsl:value-of select="number(TOTVSIntegrator/DocBranch)"/>
					</CODFILIAL>
					<CODVEN>
						<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_COD/value"/>
					</CODVEN>
					<!-- IDFUNCIONARIO é gerado na GAUTOINC -->
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_NOME/value)">
						<NOME>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_NOME/value"/>
						</NOME>
					</xsl:if>
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_COMIS/value)">
						<COMISSAO1>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_COMIS/value"/>
						</COMISSAO1>
					</xsl:if>
					<VENDECOMPRA>2</VENDECOMPRA>
					<INATIVO>0</INATIVO>
					<xsl:if test="boolean(TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_PERDESC/value)">
						<DESCMAXIMO>
							<xsl:value-of select="TOTVSIntegrator/Message/Layouts/Content/FormModel_VENDEDORES/FormModel_SA3/A3_PERDESC/value"/>
						</DESCMAXIMO>
					</xsl:if>
					<xsl:call-template name="ExtensaoTVEN"/>
				</TVEN>
			</Message>
		</TOTVSMessage>
	</xsl:template>
</xsl:stylesheet>