<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="ng" version="1.0" extension-element-prefixes="ng" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ng="com.neogrid.integrator.XSLFunctions">

<xsl:import href="xml-to-string.xsl"/>

<xsl:template match="/">
    <SENDMESSAGE>
	<XML>
		<ng:encodeToBase64Direct>&lt;?xml version='1.0' encoding="UTF-8"?&gt;
<xsl:call-template name="xml-to-string"/>
		</ng:encodeToBase64Direct>
	</XML>
    </SENDMESSAGE>
	
</xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="9.xml" userelativepaths="yes" externalpreview="no" url="..\..\..\temp\SAIDA\9.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="no" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\..\..\temp\SAIDA\9.xml" srcSchemaRoot="TOTVSIntegrator" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/">
				<block path="SENDMESSAGE/XML/xsl:call-template" x="252" y="36"/>
			</template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->