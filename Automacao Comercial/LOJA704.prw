#Include "PROTHEUS.CH"
#Include "FWMVCDEF.CH"
#Include "RWMAKE.CH"
#INCLUDE "TBICONN.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � LOJA704  � Autor � Alan Oliveira         � Data � 11/04/18 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Geracao de Reservas pelo EAI                               ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � INTEGRACAO EAI                                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function LOJA704(aCabReser, aGridReser, nOper,cXMLRet,aProdRet)

Local aCliente 		:= {} 
Local aReservas		:= {}
Local aLin			:= {}
Local dDataRes		:= ctod("  /  /    ")
Local dDtLimite   	:= ctod("  /  /    ")
Local cReserv		:= ""
Local nx			:= 1
Local aSldEst		:= {}
Local cMvLjGerSC    := cValToChar( SuperGetMv("MV_LJGERSC",,1) )//Se utiliza o conceito de gera solicita��o de compras para reserva sem estoque. 
																//1 = Desabilitado o conceito; 2 = Habilita o processo; 3=Pergunta.      
Local lRet          := .T.

Default aCabReser	:= {}
Default aGridReser	:= {}
Default nOper		:= 1
Default cXMLRet		:= ""
Default aProdRet	:= {}

For nx:=1 To Len(aGridReser)

	if nx == 1
		nPosItem   := aScan(aGridReser[nX],{|x| x[1] == "C0_PRODUTO"})
		nPosQtd	   := aScan(aGridReser[nX],{|x| x[1] == "C0_QUANT"})
		nPosLoc    := aScan(aGridReser[nX],{|x| x[1] == "C0_LOCAL"})
		nPosLote   := aScan(aGridReser[nX],{|x| x[1] == "C0_LOTECTL"})
		nPosSublot := aScan(aGridReser[nX],{|x| x[1] == "C0_NUMLOTE"})
		nPosSerie  := aScan(aGridReser[nX],{|x| x[1] == "C0_NUMSERI"})
		nPosLocal  := aScan(aGridReser[nX],{|x| x[1] == "C0_LOCALIZ"})
		nPosFil	   := aScan(aGridReser[nX],{|x| x[1] == "C0_FIL"})
		nPosDtLim  := aScan(aGridReser[nX],{|x| x[1] == "C0_VALIDA"})
	Endif

	If nOper== 1 .OR. nOper == 2
		DbSelectArea("SB1")
		SB1->(DbSetOrder(1))
		If(SB1->(DbSeek(xFilial("SB1")+aGridReser[nx][nPosItem][2]+aGridReser[nx][nPosLoc][2])))
			
			DbSelectArea("SB2")
			SB2->(DbSetOrder(1))
			If(SB2->(DbSeek(xFilial("SB2")+aGridReser[nx][nPosItem][2]+aGridReser[nx][nPosLoc][2]))) 
				If SB1->B1_RASTRO $ ("L/S")
					aSldEst := SldPorLote(aGridReser[nx][nPosItem][2],;
										aGridReser[nx][nPosLoc][2],;
										aGridReser[nx][nPosQtd][2],;
										0,;
										aGridReser[nx][nPosLote][2],;
										aGridReser[nx][nPosSublot][2],;
										aGridReser[nx][nPosLocal][2],;
										aGridReser[nx][nPosSerie][2])
					If Len(aSldEst) > 0 .AND. Alltrim(cMvLjGerSC) == "1"
						If !aSldEst[1][5] >= aGridReser[nx][nPosQtd][2] 
							cXMLRet += "Saldo do Lote " + Alltrim(aGridReser[nx][nPosLote][2]) + " e menor que a quantidade Solicitada."
							lRet := .F.
							Aadd(aProdRet, {Alltrim(aGridReser[nx][nPosItem][2]) , aSldEst[1][5] } )							
						Endif
					Else
						If !Empty(Alltrim(aGridReser[nx][nPosLote][2])) .AND. Alltrim(cMvLjGerSC) == "1"
							cXMLRet += "N�o Existe o Lote Informado: " + Alltrim(aGridReser[nx][nPosLote][2])
							lRet := .F.
							Aadd(aProdRet, { Alltrim(aGridReser[nx][nPosItem][2]),0 })							
						Endif
					Endif
				ElseIf SaldoSb2() < aGridReser[nx][nPosQtd][2] .AND. Alltrim(cMvLjGerSC) == "1"
					cXMLRet += "Saldo do Produto" + Alltrim(aGridReser[nx][nPosLote][2]) + " e menor que a quantidade Solicitada."
					lRet := .F.
					Aadd(aProdRet, { Alltrim(aGridReser[nx][nPosItem][2]),SaldoSb2() })		
				EndIf	
			Else
				If 	!Alltrim(cMvLjGerSC) == "1"			
					cXMLRet += "N�o Existe o saldo para o Produto na SB2: "+aGridReser[nx][nPosItem][2]
					lRet := .F.
					Aadd(aProdRet, { Alltrim(aGridReser[nx][nPosItem][2]) , 0 })						
				Endif
			Endif

		Endif
	Endif

Next nx

If lRet 
	For nx:=1 To Len(aGridReser)

	//Armazenas os dados do cliente em um array para enviar via RPC para cadas
	//tra-lo na outra loja se necessario	
	aAdd(aCliente,{"EAILOJA704",.T.				 , Nil })
	AAdd(aCliente,{"C0_DOCRES" , aCabReser[aScan(aCabReser,{|x| x[1] == "C0_DOCRES"})][2]	 , Nil })
	AAdd(aCliente,{"C0_SOLICIT", aCabReser[aScan(aCabReser,{|x| x[1] == "C0_SOLICIT"})][2]	 , Nil })
	AAdd(aCliente,{"C0_FILRES", aCabReser[aScan(aCabReser,{|x| x[1] == "C0_FILRES"})][2]	 , Nil })

	if nx == 1
		nPosItem   := aScan(aGridReser[nX],{|x| x[1] == "C0_PRODUTO"})
		nPosQtd	   := aScan(aGridReser[nX],{|x| x[1] == "C0_QUANT"})
		nPosLoc    := aScan(aGridReser[nX],{|x| x[1] == "C0_LOCAL"})
		nPosLote   := aScan(aGridReser[nX],{|x| x[1] == "C0_LOTECTL"})
		nPosSublot := aScan(aGridReser[nX],{|x| x[1] == "C0_NUMLOTE"})
		nPosSerie  := aScan(aGridReser[nX],{|x| x[1] == "C0_NUMSERI"})
		nPosLocal  := aScan(aGridReser[nX],{|x| x[1] == "C0_LOCALIZ"})
		nPosFil	   := aScan(aGridReser[nX],{|x| x[1] == "C0_FIL"})
		nPosDtLim  := aScan(aGridReser[nX],{|x| x[1] == "C0_VALIDA"})
	Endif
	
	aadd(aLin ,StrZero(nx,2)) 		  			//N�MERO DO ITEM
	aadd(aLin ,aGridReser[nx][nPosItem][2])		//C�DIGO DO PRODUTO
	aadd(aLin ,aGridReser[nx][nPosQtd][2])		//QUANTIDADE
	aadd(aLin ,{{aGridReser[nx][nPosLoc][2],;	//LOCAL	
				0}} )				 			//QUANTIDADE EM ESTOQUE
	aadd(aLin ,aGridReser[nx][nPosLoc][2])		//LOCAL ONDE SERA FEITA A RESERVA
	aadd(aLin ,{aGridReser[nx][nPosSublot][2],;	//SUBLOTE
				aGridReser[nx][nPosLote][2],;	//LOTE
				aGridReser[nx][nPosLocal][2],;	//ENDERECO
				aGridReser[nx][nPosSerie][2]})	//SERIE

	aadd(aReservas,aLin)
	
	dDtLimite := aGridReser[nx][nPosDtLim][2]
	
	LjGrvLog(Nil, "Conteudo da variavel aReservas enviado como parametro na funcao Lj7GeraSC0()", aReservas)
	LjGrvLog(Nil, "Conteudo da variavel aCliente enviado como parametro na funcao Lj7GeraSC0()", aCliente)
	cReserv := Lj7GeraSC0( aReservas, dDtLimite, aCliente, cFilAnt,.F.,.F.,,@cXMLRet)
	LjGrvLog(Nil, "Retorno da funcao Lj7GeraSC0 (Geracao da Reserva)", cReserv)
	
	If Empty(cReserv) .and. nOper == 1
		Exit
	Endif		
	aLin	 := {}
	aReservas:= {}
	aSldEst  := {}
	aCliente := {}

	Next nx
EndIF

Return cReserv


/*
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������ͻ��
��� 	      � IntegDef � Autor � Alan Oliveira        � Data �  08/03/18   ���
����������������������������������������������������������������������������͹��
��� Descricao � Funcao de tratamento para o recebimento/envio de mensagem    ���
���           � unica de Reserva de produtos.                                ���
����������������������������������������������������������������������������͹��
��� Uso       � LOJA704                                                ���
����������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
*/
Static Function IntegDef( xEnt, nTypeTrans, cTypeMessage , cVersion, cTransaction, lJSon )

Local 	aRet := {}

Default xEnt 			:= Nil
Default cTypeMessage 	:= ""
Default cVersion		:= ""
Default cTransaction	:= ""
Default lJSon 			:= .F.

aRet := LOJI704(xEnt, nTypeTrans, cTypeMessage , lJSon)


Return aRet

