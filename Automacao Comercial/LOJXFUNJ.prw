#INCLUDE "PROTHEUS.CH"
#INCLUDE "LOJXFUNJ.CH"

#DEFINE CLR_HRED		255

/*���������������������������������������������������������������������������
���Funcao    �LJEAICOMPANY� Autor � Alan Oliveira � Data �26/11/10  		���
�������������������������������������������������������������������������Ĵ��
���Descricao � Funcao para identificar se deve gerar xml do contas a Rece-���
���          � ber. No processo de integracao Live Bematech x Protheus    ���
���          � apenas os t�tulos de um cliente (codigo Branch) no cadastro ��
���          � De/Para empresas XXD quando incluidos na Matriz devem gerar ��
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ljeaicompany(SE1->E1_CLIENTE)                      		  	���
�������������������������������������������������������������������������Ĵ��
���Parametros� cCliente - C�digo do cliente na geracao do contas a receber���
���          � 															  		���
�������������������������������������������������������������������������Ĵ��
���Retorno	 � ExpL1 = .T.             									  		���
���          �                                     						  	���
�������������������������������������������������������������������������Ĵ��
���Uso		 � Integracao EAI - Live Bematech x Protheus			  			���
���������������������������������������������������������������������������*/
Function LjEaiCompany(cCliente)

Local aCompany := SuperGetMv("MV_EAICOMP",,"")//Paramentro da Integracao EAI que deve ser preenchido para identificar a Matriz.
Local lRet     := .T.

If !Empty(aCompany)
	aCompany := StrTokArr(aCompany, "|")
	/*
	Refer�ncia
	Company
	Branch
	Empresa
	Filial
	*/

	If Len(aCompany) == 5
		   //APENAS TITULOS DE UMA BRANCH INCLU�DO NA MATRIZ DEVEM GERAR XML
		If !Len(FWEAIEMPFIL(Padr(aCompany[2],12), Padr(cCliente,12),  PADR("BEMATECH",15))) > 0 .AND. aCompany[5] == Alltrim(cFilant) .OR.;
		   !aCompany[5] == Alltrim(cFilant)
			lRet 	 := .F.
		Endif
	Else
		lRet 	 := .F.
	Endif
Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjCsCoPr
Condi��o da CiaShop que pode ser inserida no adapter do envio de Produto.

@since   09/10/18
@version 1.0
/*/
//-------------------------------------------------------------------
Function LjxjCsCoPr()

	Local lRetorno := .F.

	If INCLUI .Or. ALTERA
		lRetorno := !Empty( GetAdvFVal("ACV", "ACV_CATEGO", xFilial("ACV") + SB1->B1_COD, 5, "", .T.) )	.And.;
			   		!Empty( GetAdvFVal("SB5", "B5_ECFLAG" , xFilial("SB5") + SB1->B1_COD, 1, "", .T.) )
	Else
		lRetorno := .T.
	EndIf

Return lRetorno

//--------------------------------------------------------
/*/{Protheus.doc} LjxjPedBlq
Valida se deve gerar a credito bloqueado ou liberado para pedidos do varejo
@type function
@author  	rafael.pessoa
@since   	21/01/2019
@version 	P12
@param 		cOrcRes  , Caracter, Numero do orcamento da reserva
@param 		cBlCred  , Caracter, c�digo do bloqueio passar por referencia
@param 		lCredito , L�gico  , Determina se o credito deve ser bloqueado, passar por referencia
@return	lRet - Retorna se o pedido deve ser bloqueado
/*/
//--------------------------------------------------------
Function LjxjPedBlq(cOrcRes, cBlCred, lCredito)

    Local aArea      := GetArea()
    Local aAreaSL1   := SL1->( GetArea() )
    Local aAreaSL4   := SL4->( GetArea() )
    Local aAreaSC5   := SC5->( GetArea() )
    Local aAreaSC6   := SC6->( GetArea() )
    Local cFormPgLib := ""
    Local lExistBlq  := .F.                                                              //Controla se existe forma com bloqueio
    Local lRet		 := .F.
    Local lECommerce := SuperGetMV("MV_LJECOMM", , .F.)
    Local lECCia 	 := SuperGetMV("MV_LJECOMO", , .F.) .And. Val(SC5->C5_PEDECOM) > 0   //Integra��o EC CiaShop
    Local lGeraSE1 	 := SuperGetMV("MV_LJECOMS", , .F.)                                  //Gera Contas a Receber
    Local lLj901AGAE := ExistFunc("Lj901AGAE")                                           //Fun��o de Retorno da Administradora
    Local cOrcamto 	 := ""
    Local cStatusPed := ""

    Default cOrcRes  := ""
    Default cBlCred  := ""
    Default lCredito := .F.

    //Carrega informa��es do or�amento
    cOrcamto := Posicione("SL1", 1, xFilial("SL1") + cOrcRes, "L1_ORCRES")

    //E-commerce Rakuten
    //Para e-Commerce ira gravar com bloqueio de credito para Boleto(FI) e sem bloqueio para os demais.
    If lECommerce .And. !( Empty(cOrcRes) ) .And. ChkFile("MF5")

        MF5->( DbSetOrder(1) )  //MF5_FILIAL+MF5_ECALIA+MF5_ECVCHV

        If !( Empty(cOrcamto) ) .And. !( Empty(Posicione("MF5",1,xFilial("MF5")+"SL1"+xFilial("SL1")+cOrcamto,"MF5_ECPEDI")) )
            If (AllTrim(SL1->L1_FORMPG) == "FI") .And. (MF5->MF5_ECPAGO != "1")
                cBlCred  := "02"
                lCredito := .F.
            Else
                cBlCred  := "  "
                lCredito := .T.
            EndIf
        EndIf

    //E-commerce CiaShop Antiga
    ElseIf lECommerce .And. lECCia .And. lGeraSE1 .And. lLj901AGAE

        If Alltrim(Lj901AGAE()) == "FI"
            cBlCred  := "02"
            lCredito := .F.
        Else
            cBlCred  := "  "
            lCredito := .T.
        EndIf

    //Integra��o via Mensagem �nica
    ElseIf !lECommerce .And. !lECCia .And. !Empty(cOrcamto) .And. Posicione("SL1", 1, xFilial("SL1") + cOrcamto, "L1_ORIGEM") == "N"

        //Retorna as formas de pagamentos que geram pedido de venda Liberado para a integra��o via MSU - Ex: FI|BOL
        cFormPgLib := LjxjIFPLI()

        //Par�metro vazio, Bloqueia Pedidos
        If Empty(cFormPgLib)
            lExistBlq := .T.
        Else

            SL4->( DbSetOrder(1) )      //L4_FILIAL+L4_NUM+L4_ORIGEM
            If SL4->( DbSeek(xFilial("SL4") + cOrcamto) )
                While !SL4->( Eof() ) .And. SL4->L4_FILIAL == xFilial("SL4") .And. SL4->L4_NUM == cOrcamto

                    //Bloqueia o credito se n�o encontrar alguma forma do parametro
                    If !( AllTrim(SL4->L4_FORMA) $ cFormPgLib )
                        lExistBlq := .T.
                        Exit
                    EndIf

                    SL4->( DbSkip() )
                EndDo
            EndIf
        EndIf

        If lExistBlq
            cBlCred     := "02"     //C9_BLCRED
            cStatusPed  := "09"     //Pedido bloqueado
            lCredito    := .F.
            lRet 	    := .T.
        Else
            cBlCred     :=  "  "
            cStatusPed  := "01"     //Pedido liberado
            lCredito    := .T.
            lRet 	    := .F.
        EndIf

        //Atualiza status do Pedido
        If IsInCallStack("Lj7Pedido") .And. SC5->C5_STATUS <> cStatusPed
            RecLock("SC5", .F.)
                SC5->C5_STATUS := cStatusPed
            SC5->( MsUnLock() )
        EndIf

    EndIf

    RestArea(aAreaSC6)
    RestArea(aAreaSC5)
    RestArea(aAreaSL4)
    RestArea(aAreaSL1)
    RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjSaleId
Retornar o InternalId da Venda do Varejo.
Atualiza TAG RetailSalesInternalId do adapter MATA410B - DOCUMENTTRACEABILITYORDER.
Uso MATI410B e MATI410BO.

@param cFilPed	- Filial do Pedido
@param cNumOrc	- N�mero do Or�amento

@retorno cRetSaleId	- Retornar o InternalId da Venda do Varejo.

@author	 Rafael Tenorio da Costa
@since 	 19/12/18
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjSaleId(cFilPed, cNumOrc)

	Local aRetSale 	 := {}
	Local cRetSaleId := ""

	//Venda Varejo que originou Pedido de Venda
	If !Empty(cNumOrc)
		aRetSale := LjXjSalRet(cNumOrc, cFilPed)

		If Len(aRetSale) > 0
			cRetSaleId := IntVendExt( , aRetSale[1], aRetSale[2], aRetSale[3], aRetSale[4])[2]
		EndIf
	EndIf

Return cRetSaleId

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjSaleId
Retornar o InternalId da Venda do Varejo.
Atualiza TAG RetailSalesInternalId do adapter MATA410B - DOCUMENTTRACEABILITYORDER.
Uso MATI410B e MATI410BO.

@param cFilPed	- Filial do Pedido
@param cNumOrc	- N�mero do Or�amento

@retorno cRetSaleId	- Retornar o InternalId da Venda do Varejo.

@author	 Rafael Tenorio da Costa
@since 	 19/12/18
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjStaPed(cStatus)

	Local cSituacao := ""

	cStatus := AllTrim(cStatus)

	Do Case
		Case cStatus == "00"
			cSituacao := STR0001    //"Gerado"

		Case cStatus == "01"
			cSituacao := STR0002    //"Pedido liberado"

		Case cStatus == "05"
			cSituacao := STR0003    //"Em an�lise"

		Case cStatus == "09"
			cSituacao := STR0004    //"Pedido bloqueado"

		Case cStatus == "10"
			cSituacao := STR0005    //"Pagamento confirmado"

		Case cStatus == "11"
			cSituacao := STR0006    //"Faturado"

		Case cStatus == "15"
            cSituacao := STR0007    //"Empacotado"

		Case cStatus == "21"
			cSituacao := STR0008    //"Parcialmente enviado"

		Case cStatus == "30"
			cSituacao := STR0009    //"Enviado"

		Case cStatus == "31"
			cSituacao := STR0013    //"Entregue"

		Case cStatus == "90"
			cSituacao := STR0010    //"Cancelado"

		Case cStatus == "91"
			cSituacao := STR0011    //"Devolvido"

		OTherWise
			cSituacao := STR0012 + cStatus  //"Status indefinido: "
	End Case

Return cSituacao

//-------------------------------------------------------------------
/*/{Protheus.doc} AtuLoja
Faz atualiza��es necess�rias pelo Loja:
	- Envia mensagem unica de Rastreio de Pedido
	- Vincula Notas Fiscais de Presente com Nota Fiscal comum.
Uso MATA461

@param    aPedido    - matriz com os pedidos a serem vinculados

@author   Eduardo Vicente
@since    27.02.2013
@version  P11.5


ANTIGA FUN��O A460VNCNF NO MATA461

/*/
//-------------------------------------------------------------------
Function LjxjAtLoja(aPedido)

Local aArea	      	:= GetArea()			 //Grava a area Atual
Local aAreaSC5    	:= SC5->( GetArea() )
Local aAreaSF2    	:= SF2->( GetArea() )
Local aAreaSL1	  	:= SL1->( GetArea() )
Local nA            := 0
Local nLenAPedido   := Len(aPedido)
Local cOrcamto      := ""                    //Obtem o numero do Orcamento original para posicionar na MF5 e gravar o Status de Despachado.
Local cNFIndice	    := ""                    //Indice para posiciona a Nota Fiscal original
Local cNFVinculada  := ""                    //Indice para vincular a Nota Fiscal original
Local cNFPai	    := ""                    //Numero Nota de Vinculo da Nota Fiscal de Presente com a Nota Fiscal Original
Local cSerPai	    := ""                    //Serie Nota de Vinculo
Local lIntegraEC    := SF2->( (ColumnPos("F2_ECVINC1") > 0) .And. (ColumnPos("F2_ECVINC2") > 0) )
Local lECCia		:= SuperGetMV("MV_LJECOMO",,.F.)        //Indica se � e-commerce CiaShop
Local cFiltro		:= ""					                //Filtro da SL1
Local lGerSE1 		:= SuperGetMv("MV_LJECOMS",.T., .F.)    //Gera T�tulo de Contas a Receber
Local cStatusPed	:= AllTrim( SuperGetMv("MV_LJECST1",.F., "30") )
Local lECommerce 	:= SuperGetMV("MV_LJECOMM",,.F.)

Default aPedido   	:= {}

DbSelectArea("SF2")
DbSetOrder(1) //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO

DbSelectArea("SC5")
DbSetOrder(1) //C5_FILIAL+C5_PEDIDO

For nA := 1 To nLenAPedido

	//Verifica se o pedido foi gerado pelo loja
	If SC5->( DbSeek(xFilial("SC5") + aPedido[nA][01]) ) .And. ( !Empty(SC5->C5_ORCRES) .Or. !Empty(SC5->C5_PEDECOM) )

		If !lECommerce

			RecLock("SC5", .F.)
				SC5->C5_STATUS  := "11"	//Faturado
			SC5->( MsUnLock() )

			//Mensagem de rastreio de pedido
			If FwHasEai("MATA410B", .T., , .T.)
				FwIntegDef("MATA410B")
			EndIf

		Else

			cNFIndice := xFilial("SF2")+SC5->C5_NOTA+SC5->C5_SERIE+SC5->C5_CLIENTE+SC5->C5_LOJACLI  //Indice da NF Original

			If ChkFile("MF5")

			    cOrcamto := Posicione("SL1",1,xFilial("SL1")+SC5->C5_ORCRES,"L1_ORCRES")

			    If  !( Empty(cOrcamto) ) .And. !( Empty(Posicione("MF5",1,xFilial("MF5")+"SL1"+xFilial("SL1")+cOrcamto,"MF5_ECPEDI")) )
			    	If  MF5->( SoftLock("MF5") )
			    		MF5->MF5_ECSTAT := "17"  //Grava o Status de Despachado para enviar posteriormente ao e-Commerce.
			    		MF5->MF5_ECDTEX := " "   //Limpa a data para exportacao.
			    	    MF5->( MsUnLock() )
			    	EndIf
			    EndIf
			EndIf

			//Atualiza o Status EC Ciashop como 30  - Despachado
			If lECCia .AND. Val(SC5->C5_PEDECOM) > 0

				RecLock("SC5", .F.)
					SC5->C5_STATUS  := cStatusPed
					SC5->C5_VOLTAPS := ""
				SC5->( MsUnLock() )

		        If  SF2->( DbSeek(cNFIndice) )

					cFiltro := "L1_FILIAL == '" + xFilial("SL1") + "' .And. AllTrim(L1_PEDRES) == '" + Alltrim(SC5->C5_NUM) + "'"

					SL1->(DbSetOrder(1)) // L1_FILIAL+L1_NUM
					SL1->(DbSetFilter({ || &cFiltro }, cFiltro))

					If SL1->(!Eof())

						RecLock("SL1", .F.)
							SL1->L1_DOC 	:=  SF2->F2_DOC
							SL1->L1_SERIE 	:= SF2->F2_SERIE
							SL1->L1_EMISNF 	:= SF2->F2_EMISSAO
							If Empty(SL1->L1_PDV)
								SL1->L1_PDV := "."
							EndIf
						SL1->( MsUnLock() )

					   	If  lGerSE1 .AND. !Empty(SL1->L1_DOCPED) .AND. !Empty(SL1->L1_SERPED) .AND.  Empty( SF2->F2_PREFIXO) .AND. Empty(SF2->F2_DUPL)
							RecLock("SF2", .F.)
								SF2->F2_PREFIXO := SL1->L1_SERPED
								SF2->F2_DUPL := SL1->L1_DOCPED
							SF2->( MsUnLock() )
						EndIf

					EndIf

					SL1->(DbClearFilter())
		        EndIf

		        RestArea(aAreaSL1)
				RestArea(aAreaSF2)
			EndIf

			If  lIntegraEC
				If  !( Empty(SC5->C5_ECVINCU) )
					If  SC5->( dbSeek(SC5->C5_FILIAL+Alltrim(SC5->C5_ECVINCU)) )

						cNFVinculada := xFilial("SF2")+SC5->C5_NOTA+SC5->C5_SERIE+SC5->C5_CLIENTE+SC5->C5_LOJACLI

						If  SF2->( DbSeek(cNFIndice) )   //Posiciona a NF Origenal e Grava os dados da NF Vinculada (NF Presente)
							RecLock("SF2",.F.)
							SF2->F2_ECVINC1	:= SC5->C5_NOTA
							SF2->F2_ECVINC2	:= SC5->C5_SERIE
							SF2->(MsUnlock())

							cNFPai	  := SF2->F2_DOC
							cSerPai	  := SF2->F2_SERIE

							If  SF2->( DbSeek(cNFVinculada) )  //Posiciona na NF Vinculada e grava os dados da NF Original.
								RecLock("SF2",.F.)
								SF2->F2_ECVINC1	:= cNFPai
								SF2->F2_ECVINC2	:= cSerPai
								SF2->(MsUnlock())
							EndIf
						EndIf
					EndIf
				EndIf
			EndIf

		EndIf

	EndIf

Next nA

RestArea(aAreaSF2)
RestArea(aAreaSC5)
RestArea(aArea)

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjAtStTr
Atualiza status do pedido na transmiss�o da NFE.
J� esta posicionado no pedido.
Uso MATI461EAI

@author	 Rafael Tenorio da Costa
@since 	 17/01/19
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjAtStTr()

    Local lERakuten := SuperGetMV("MV_LJECOMM", , .F.)     //E-commerce Rakuten
    Local lECCia    := SuperGetMV("MV_LJECOMO", , .F.)     //E-commerce CiaShop 
	Local cStatus   := ""
    
    If lERakuten .Or. lECCia
        cStatus := AllTrim( SuperGetMv("MV_LJECST1", .F., "") )
    Else
        cStatus := "15" //Empacotado
    EndIf

	If !Empty(cStatus) .And. !Empty(SC5->C5_ORCRES)
		RecLock("SC5", .F.)
			SC5->C5_STATUS := cStatus
		SC5->( MsUnLock() )
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjIFPLI
Retorna o conteudo do par�metro MV_LJIFPLI, macro executando caso
seja necess�rio.

@author	 Rafael Tenorio da Costa
@since 	 27/03/19
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjIFPLI()

	Local cFormPgLib := AllTrim( SuperGetMv("MV_LJIFPLI", .F., "") )    //Define as formas de pagamentos que geram pedido de venda Liberado para a integra��o via MSU - Ex: FI|BOL
    Local cRetorno   := ""

    If SubStr(cFormPgLib, 1, 1) == "&"

        cFormPgLib := SubStr(cFormPgLib, 2)

        LjGrvLog("MV_LJIFPLI", "Executando fun��o contida no par�metro MV_LJIFPLI.", cFormPgLib, /*lCallStack*/)

        cRetorno := &(cFormPgLib)
    Else

        cRetorno := cFormPgLib
    EndIf

Return cRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjMsgErr
Apresenta mensagem de erro e grava os logs

@author	 Rafael Tenorio da Costa
@since 	 27/03/19
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjMsgErr(cErro, cSolucao)

    Local cDataHora := DtoC( Date() ) + " " + Time() + " "
    Local cRotina   := ProcName(1)

    Default cSolucao := ""

    Help("", 1, "HELP", cRotina, cErro, 1,,,,,,, {cSolucao})

    LjGrvLog(/*cNumControl*/, cErro, /*xVar*/, /*lCallStack*/)

    Conout( NoAcento( cDataHora + cRotina + " - " + cErro) )

Return Nil

//-------------------------------------------------------
/*/{Protheus.doc} LjxjAdmFin
Valida a Adm Financeira, tentando localizar atrav�s do
Array padr�o, se n�o houver a rotina cadastra automaticamente
na SX5 L9

@param   cNetworkDe - Conte�do da Tag NetworkDestination
@return  lRet - se a rotina fez com sucesso
@author  Fabricio Panhan Costa
@since   27/03/2019
@version 1.0
/*/
//-------------------------------------------------------
Function LjxjAdmFin(cNetworkDe)

	Local aArea    := GetArea()
	Local aAreaSX5 := SX5->(GetArea())
	Local aAdmFin  := {}
	Local nPos     := 0
	Local lRet     := .T.

	Default cNetworkDe := ""

	aAdd(aAdmFin, {"56","Leader"})
	aAdd(aAdmFin, {"58","Gazincred"})
	aAdd(aAdmFin, {"59","Telenet"})
	aAdd(aAdmFin, {"61","Brasil Card"})
	aAdd(aAdmFin, {"62","E-Pharma"})
	aAdd(aAdmFin, {"63","Rede Total"})
	aAdd(aAdmFin, {"65","GAX 65"})
	aAdd(aAdmFin, {"66","Peralta"})
	aAdd(aAdmFin, {"68","Banese"})
	aAdd(aAdmFin, {"69","Resomaq"})
	aAdd(aAdmFin, {"70","Sysdata"})
	aAdd(aAdmFin, {"72","Big Card"})
	aAdd(aAdmFin, {"73","Data Tranfer"})
	aAdd(aAdmFin, {"75","Check Express"})
	aAdd(aAdmFin, {"76","Givex"})
	aAdd(aAdmFin, {"77","Vale Card"})
	aAdd(aAdmFin, {"78","Portal Card"})
	aAdd(aAdmFin, {"79","Banpara"})
	aAdd(aAdmFin, {"80","Softnex"})
	aAdd(aAdmFin, {"81","Supercard"})
	aAdd(aAdmFin, {"82","Getnet"})
	aAdd(aAdmFin, {"82","Somar"})
	aAdd(aAdmFin, {"83","Prevsaude"})
	aAdd(aAdmFin, {"84","Banco Pottencial"})
	aAdd(aAdmFin, {"85","Sophus"})
	aAdd(aAdmFin, {"86","Marisa"})
	aAdd(aAdmFin, {"87","MaxiCred"})
	aAdd(aAdmFin, {"88","Black Hawk"})
	aAdd(aAdmFin, {"89","Expansiva"})
	aAdd(aAdmFin, {"90","Sas/NT"})
	aAdd(aAdmFin, {"91","Leader"})
	aAdd(aAdmFin, {"93","Aura � Cetelem"})
	aAdd(aAdmFin, {"94","Cabal"})
	aAdd(aAdmFin, {"95","Credsystem"})
	aAdd(aAdmFin, {"97","Cartesys"})
	aAdd(aAdmFin, {"98","Cisa"})
	aAdd(aAdmFin, {"99","TrnCentre"})
	aAdd(aAdmFin, {"101","Cardco"})
	aAdd(aAdmFin, {"102","CheckCheck"})
	aAdd(aAdmFin, {"103","DaCasa"})
	aAdd(aAdmFin, {"104","Private Bradesco"})
	aAdd(aAdmFin, {"105","Platinum"})
	aAdd(aAdmFin, {"106","GwCel"})
	aAdd(aAdmFin, {"107","Check Express"})
	aAdd(aAdmFin, {"109","Usecred"})
	aAdd(aAdmFin, {"110","Serv_Voucher"})
	aAdd(aAdmFin, {"111","Tredenexx"})
	aAdd(aAdmFin, {"112","Cart�o Presente"})
	aAdd(aAdmFin, {"113","Credishop"})
	aAdd(aAdmFin, {"114","Porto Seguro"})
	aAdd(aAdmFin, {"115","IBI"})
	aAdd(aAdmFin, {"116","WorkerCard"})
	aAdd(aAdmFin, {"118","Oboe"})
	aAdd(aAdmFin, {"119","Protege"})
	aAdd(aAdmFin, {"121","HotCard"})
	aAdd(aAdmFin, {"122","Banco Panamericano"})
	aAdd(aAdmFin, {"124","SigaCred"})
	aAdd(aAdmFin, {"125","Cielo"})
	aAdd(aAdmFin, {"127","Cartao Presente Marisa"})
	aAdd(aAdmFin, {"128","Cooplife"})
	aAdd(aAdmFin, {"130","Gcard"})
	aAdd(aAdmFin, {"131","Tcredit"})
	aAdd(aAdmFin, {"132","Siscred"})
	aAdd(aAdmFin, {"133","Foxwin Card"})
	aAdd(aAdmFin, {"134","Convcard"})
	aAdd(aAdmFin, {"135","SAV"})
	aAdd(aAdmFin, {"136","Expand Cards"})
	aAdd(aAdmFin, {"138","Qualicard"})
	aAdd(aAdmFin, {"140","Waapa"})
	aAdd(aAdmFin, {"141","SQCF"})
	aAdd(aAdmFin, {"142","Intellisys"})
	aAdd(aAdmFin, {"144","Accredito"})
	aAdd(aAdmFin, {"145","Comprocard"})
	aAdd(aAdmFin, {"146","Orgcard"})
	aAdd(aAdmFin, {"147","Minascred"})
	aAdd(aAdmFin, {"148","Farmacia Popular"})
	aAdd(aAdmFin, {"149","Fidelidade Mais"})
	aAdd(aAdmFin, {"152","Fortcard"})
	aAdd(aAdmFin, {"153","Paggo"})
	aAdd(aAdmFin, {"154","Smartnet"})
	aAdd(aAdmFin, {"155","Interfarmacia"})
	aAdd(aAdmFin, {"156","Valecon"})
	aAdd(aAdmFin, {"157","Cartao Evang�lico"})
	aAdd(aAdmFin, {"158","Vegas Card"})
	aAdd(aAdmFin, {"159","Sccard"})
	aAdd(aAdmFin, {"160","Orbitall"})
	aAdd(aAdmFin, {"161","Icards"})
	aAdd(aAdmFin, {"162","Facil Card"})
	aAdd(aAdmFin, {"163","Fidelize"})
	aAdd(aAdmFin, {"164","Finamax"})
	aAdd(aAdmFin, {"165","Banco GE"})
	aAdd(aAdmFin, {"166","Unik"})
	aAdd(aAdmFin, {"167","TIVIT"})
	aAdd(aAdmFin, {"168","Validata"})
	aAdd(aAdmFin, {"169","Banescard"})
	aAdd(aAdmFin, {"171","Valeshop"})
	aAdd(aAdmFin, {"172","Somar Card"})
	aAdd(aAdmFin, {"173","Omnion"})
	aAdd(aAdmFin, {"174","Condor"})
	aAdd(aAdmFin, {"175","StandbyDup"})
	aAdd(aAdmFin, {"177","Marisa Sax Sysin"})
	aAdd(aAdmFin, {"178","Starfiche"})
	aAdd(aAdmFin, {"179","Ace Seguros"})
	aAdd(aAdmFin, {"180","Top Card"})
	aAdd(aAdmFin, {"181","Getnet"})
	aAdd(aAdmFin, {"182","UpSight"})
	aAdd(aAdmFin, {"183","MAR"})
	aAdd(aAdmFin, {"184","Funcional PBM"})
	aAdd(aAdmFin, {"185","Pharma System"})
	aAdd(aAdmFin, {"186","Neus"})
	aAdd(aAdmFin, {"187","Sicredi"})
	aAdd(aAdmFin, {"189","Nservices"})
	aAdd(aAdmFin, {"190","CSF"})
	aAdd(aAdmFin, {"192","Avista"})
	aAdd(aAdmFin, {"193","Algorix"})
	aAdd(aAdmFin, {"194","Amex"})
	aAdd(aAdmFin, {"195","Compremax"})
	aAdd(aAdmFin, {"196","Libercard"})
	aAdd(aAdmFin, {"197","Seicon"})
	aAdd(aAdmFin, {"199","SmartN"})
	aAdd(aAdmFin, {"203","Peela"})
	aAdd(aAdmFin, {"204","Nutrik"})
	aAdd(aAdmFin, {"205","GoldenFarma PBM"})
	aAdd(aAdmFin, {"206","Global Payments"})
	aAdd(aAdmFin, {"207","Elavon"})
	aAdd(aAdmFin, {"208","CTF"})
	aAdd(aAdmFin, {"209","Banestik"})
	aAdd(aAdmFin, {"214","Eletrozema"})
	aAdd(aAdmFin, {"215","Barigui"})
	aAdd(aAdmFin, {"801","CB Banco do Brasil"})
	aAdd(aAdmFin, {"802","CB Interchange"})
	aAdd(aAdmFin, {"803","CB Bank Boston"})
	aAdd(aAdmFin, {"804","CB Cef"})
	aAdd(aAdmFin, {"805","CB Bradesco"})
	aAdd(aAdmFin, {"806","CB Safra"})
	aAdd(aAdmFin, {"807","CB Santander"})
	aAdd(aAdmFin, {"808","CB_Hsbc"})
	aAdd(aAdmFin, {"809","CB Bancoob"})
	aAdd(aAdmFin, {"810","CB CorbanSe"})
	aAdd(aAdmFin, {"811","CB GXS"})
	aAdd(aAdmFin, {"812","CB Itau"})

    nPos := AScan(aAdmFin, {|x| x[1] == cNetworkDe})

    If nPos > 0
        DbSelectArea("SX5")
        SX5->( DbSetOrder(1) )
        If !SX5->( DbSeek(xFilial("SX5") + "L9" + cNetworkDe) )
            RecLock("SX5", .T.)
                X5_FILIAL  := xFilial("SX5")
                X5_TABELA  := "L9"
                X5_CHAVE   := cNetworkDe
                X5_DESCRI  := aAdmFin[nPos][2]
                X5_DESCSPA := aAdmFin[nPos][2]
                X5_DESCENG := aAdmFin[nPos][2]
            SX5->( MsUnlock() )
        EndIf
    Else
        lRet := .F.
    EndIf

    RestArea(aAreaSX5)
    RestArea(aArea)

Return lRet


//--------------------------------------------------------
/*/{Protheus.doc} LjITesDev
Busca a tes de Devolu��o a partir da origem
@type function
@author  	rafael.pessoa
@since   	11/04/2019
@version 	P12
@param 		cDocOri    , Caracter, Numero do documento
@param 		cSerieOri  , Caracter, Numero da serie
@param 		cItOri     , Caracter, Item Origem
@param 		cCliente   , Caracter, Codigo do Cliente
@param 		cLoja      , Caracter, Loja do Cliente
@param 		cProduto   , Caracter, codigo do produto

@return	cTesDev - Retorna a TES de Devolu��o a partir da nota de origem
/*/
//--------------------------------------------------------
Function LjITesDev( cDocOri , cSerieOri , cItOri , cCliente , cLoja ,cProduto)

Local cTesOri 	    := ""
Local cTesDev 	    := ""
Local lRet 	        := .F.

Default cDocOri     := ""	
Default cSerieOri   := ""	
Default cItOri      := ""
Default cCliente    := ""	
Default cLoja       := ""
Default cProduto    := ""

DbSelectArea("SD1")
DbSetOrder(1)//D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM 
If ( MsSeek(xFilial("SD1") + cDocOri + cSerieOri + cCliente + cLoja + cProduto + cItOri) )
    lRet    := .T.
    cTesOri := D1_TES
EndIf

If lRet
    DbSelectArea("SF4")
    SF4->( DbSetOrder(1) ) //F4_FILIAL+F4_CODIGO
    If 	SF4->( DbSeek(xFilial("SF4") + cTesOri ) )
        If SF4->(DbSeek(xFilial("SF4")+SF4->F4_TESDV))
            cTesDev := SF4->F4_CODIGO
        EndIf
    EndIf    
EndIf

Return cTesDev

//--------------------------------------------------------
/*/{Protheus.doc} LjxAddFil
AddCampos todos os Campos Faltantes no model
@type function
@author  	rafael.ten�rio
@since   	05/06/2019
@version 	P12
@return	    ViewDef - View do modelo
/*/
//--------------------------------------------------------
Function LjxAddFil(cTable, oStruct, nType)

Local aArea		:= GetArea()
Local aAreaSX3	:= SX3->( GetArea() )
Local cField    := ""

Default oStruct := Nil
Default nType   := 0

If SX3->( DbSeek(cTable) )

    While SX3->X3_ARQUIVO == cTable
        cField := AllTrim(SX3->X3_CAMPO)
        If !oStruct:HasField(AllTrim(cField))

            If nType == 1 //Model 	
                oStruct:AddField(RetTitle(cField), "", cField, SX3->X3_TIPO, SX3->X3_TAMANHO, SX3->X3_DECIMAL, Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil)
            ElseIf nType == 2 //View
                oStruct:AddField(cField, SX3->X3_ORDEM, RetTitle(cField), "", {}, SX3->X3_TIPO, SX3->X3_PICTURE, Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil, .T.)
            EndIf
        EndIf
    
        SX3->( DbSkip() )
    EndDo

EndIf

RestArea(aAreaSX3)
RestArea(aArea)

Return Nil

//------------------------------------------------------------------
/*/{Protheus.doc} LJPesqCP()
Tela de pesquisa de condi��o de pagamento, disponibilizada
no Venda Assistida, chamada no X3_F3 do campo LQ_CONDPG
@type Function

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return L�gico, retorno .T.
/*/
//------------------------------------------------------------------
Function LJPesqCP()

Local oButConf
Local oVlrVend
Local oVlrDesc
Local oVlrAcrs
Local oVlrTot
Local oTcCond
Local oTcParc
Local oFont  		:= TFont():New("Courier New",,014,,.T.,,,,,.F.,.F.)
Local oFont2 		:= TFont():New("Courier New",,020,,.T.,,,,,.F.,.F.)
Local oFont3 		:= TFont():New("Courier New",,016,,.T.,,,,,.F.,.F.)
Local oFont4 		:= TFont():New("Courier New",,020,,.T.,,,,,.F.,.F.)
Local oVlrDescScr															// Objeto do valor de desconto na Tela
Local oVlrVendScr															// Objeto do valor da Venda na Tela
Local oVlrAcrsScr    														// Objeto do valor de Acr�scimo na Tela
Local oVlrTotScr     														// Objeto do valor de Total na Tela
Local oNegTela
Local aCabec 		:= {STR0014, STR0015, STR0016, STR0017, STR0018} 		// {"C�digo","Condi��o","Vlr. Parcela","Valor Total","Vlr.Minimo"} 
Local aItens    	:= {}
Local aParcelas 	:= {}
Local aParcAux  	:= {}  	// Armazena todas as parcelas para todas as condi��es de pagamento

Local cVlrVenda 	:= "" 	// vari�vel valor da Venda na Tela
Local cVlrDesc  	:= "" 	// vari�vel valor da desconto na Tela
Local cVlrAcres 	:= "" 	// vari�vel valor Acr�scimo na Tela
Local cVlrTotal 	:= "" 	// vari�vel do valor de Total na Tela
Local nAuxTot   	:= 0  	// Vari�vel que controla valor da venda vindo da Venda Assistida
Local nVlrParcela 	:= 0	// Vari�vel que armazena o Valor L�quido
Local nVlrSoma  	:= 0  	// Auxiliar do Frete
Local nValorAtu		:= 0

/*
Cadastro SXB para utiliza��o da funcionalidade

XB_ALIAS	XB_TIPO	XB_SEQ	XB_COLUNA	XB_DESCRI	XB_DESCSPA	XB_DESCENG	XB_CONTEM	XB_WCONTEM
LJCNDP		   1	01	       RE		Pesquisa 	Pesquisa	Pesquisa   	   SE4
LJCNDP		   2	01	       01										     LJPESQCP()
LJCNDP		   5	01					                                     LJRETSE4()

SX3 - Basta alterar o campo X3_F3 do LQ_CONDPG para LJCNDP .
*/

Lj7SetKeys(.F.) 				// Desabilita as teclas de atalho
Lj7ZeraPgtos() 					// Zera parcelas para evitar erros de montagem de parcelas
nAuxTot     := Lj7T_Total( 2 )
aAtuVlrTot  := AtuValorTot() 	// Alimenta a vari�vel que informar� o valor total antes da negocia��o
nVlrParcela := aAtuVlrTot[1]
nVlrSoma	:= aAtuVlrTot[2]

DEFINE MSDIALOG oNegTela TITLE STR0032 FROM 000, 000  TO 500, 890 COLORS 0, 16777215 PIXEL									//Negocia��o do Pagamento
oTcCond := TCBrowse():New (015,007,280,150,Nil,Nil,Nil,oNegTela,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,.T.)	//Cond. Pgto
oTcParc := TCBrowse():New (015,292,150,150,Nil,Nil,Nil,oNegTela,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,Nil,.T.)	// Parcelas
@ 225, 350 BUTTON oButConf PROMPT STR0020 	SIZE 045, 015 ACTION (ConfSrc(oTcCond, oNegTela, @cVlrTotal)) OF oNegTela PIXEL 			// Confirmar

@ 180, 010 SAY oVlrVend    PROMPT STR0021 	SIZE 100, 007 OF oNegTela FONT oFont2 COLORS 0, 16777215  PIXEL					//Valor da Venda:
@ 180, 110 SAY oVlrVendScr PROMPT cVlrVenda			 	SIZE 080, 007 OF oNegTela FONT oFont4 COLORS CLR_HRED 	 PIXEL

@ 205, 010 SAY oVlrDesc    PROMPT STR0022 	SIZE 100, 007 OF oNegTela FONT oFont2 COLORS 0, 16777215  PIXEL					//Valor do Desconto:
@ 205, 110 SAY oVlrDescScr PROMPT cVlrDesc			 	SIZE 080, 007 OF oNegTela FONT oFont4 COLORS CLR_HRED 	 PIXEL

@ 230, 010 SAY oVlrAcrs    PROMPT STR0023	SIZE 100, 007 OF oNegTela FONT oFont2 COLORS 0, 16777215  PIXEL 				//Valor do Acr�scimo:
@ 230, 110 SAY oVlrAcrsScr PROMPT cVlrAcres			 	SIZE 080, 007 OF oNegTela FONT oFont4 COLORS CLR_HRED     PIXEL

@ 180, 295 SAY oVlrTot     PROMPT STR0024 	SIZE 100, 007 OF oNegTela FONT oFont2 COLORS 0, 16777215 PIXEL					//Valor Total:
@ 180, 360 SAY oVlrTotScr  PROMPT cVlrTotal			 	SIZE 080, 007 OF oNegTela FONT oFont4 COLORS CLR_HRED     PIXEL

//Adiciona Cabe�alho da Condi��o de Pagamento
oTcCond:AddColumn(TCColumn():New(STR0014, {|| aItens[oTcCond:nAt,1]  },,,,"LEFT"   ,020,.F.,.F.,,,,.F.,))					//C�digo
oTcCond:AddColumn(TCColumn():New(STR0015, {|| aItens[oTcCond:nAt,2]  },,,,"LEFT",130,.F.,.F.,,,,.F.,))						//Condi��o
oTcCond:AddColumn(TCColumn():New(STR0016, {|| aItens[oTcCond:nAt,3] },,,,"LEFT",040,.F.,.F.,,,,.F.,))						//Vlr. Parcela
oTcCond:AddColumn(TCColumn():New(STR0017, {|| aItens[oTcCond:nAt,4] },,,,"LEFT",040,.F.,.F.,,,,.F.,))						//Vlr. Total
oTcCond:AddColumn(TCColumn():New(STR0018, {|| aItens[oTcCond:nAt,5] },,,,"LEFT",035,.F.,.F.,,,,.F.,))						//Vlr. Minimo

//Adiciona Cabe�alho das Parcelas
oTcParc:AddColumn(TCColumn():New(STR0025, {|| aParcelas[oTcParc:nAt,1] },,,,"LEFT"   ,025,.F.,.F.,,,,.F.,))					//Data
oTcParc:AddColumn(TCColumn():New(STR0026, {|| aParcelas[oTcParc:nAt,2] },,,,"LEFT"  ,060,.F.,.F.,,,,.F.,))					//Valor
oTcParc:AddColumn(TCColumn():New(STR0027, {|| aParcelas[oTcParc:nAt,3] },,,,"LEFT"  ,020,.F.,.F.,,,,.F.,))					//Pgto
oTcParc:AddColumn(TCColumn():New(STR0028, {|| aParcelas[oTcParc:nAt,4] },,,,"LEFT" ,010,.F.,.F.,,,,.F.,))					//Parc.

AtuCondScr(@oTcCond	, @aItens	, @oTcParc	, @aParcelas, @oVlrDescScr	, @oVlrVendScr	, @oVlrAcrsScr, @oVlrTotScr,;
		   nAuxTot	, @cVlrVenda, @cVlrDesc	, @cVlrAcres, @cVlrTotal	, nVlrSoma		, nVlrParcela , @aParcAux, nValorAtu) 																										// Alimenta itens da Condi��o de Pagamento e Parcelas
oTcCond:bLDblClick  := {|| ConfSrc(oTcCond, oNegTela, cVlrTotal)}  																	// A��o no Duplo Clique
oTcCond:bSeekChange := {|| AtuParcelas(oTcParc		, aParcelas	, Nil			, oTcCond	, oVlrDescScr	, oVlrVendScr,;
									   oVlrAcrsScr	, oVlrTotScr, nAuxTot		, cVlrVenda	, cVlrDesc		, cVlrAcres	 ,;
									   cVlrTotal	, nVlrSoma	, nVlrParcela	, aParcAux	, nValorAtu)} 							// A��o na movimenta��o com mouse
oTcCond:bDrawSelect := {|| AtuParcelas(oTcParc		, aParcelas	, Nil			, oTcCond	, oVlrDescScr	, oVlrVendScr,;
									   oVlrAcrsScr	, oVlrTotScr, nAuxTot		, cVlrVenda	, cVlrDesc		, cVlrAcres	 ,;
									   cVlrTotal	, nVlrSoma	, nVlrParcela	, aParcAux	, nValorAtu)}							// A��o na movimenta��o das linhas com o teclado

ACTIVATE MSDIALOG oNegTela CENTERED

Return .T.

//------------------------------------------------------------------
/*/{Protheus.doc} AtuVlrSrc()
Atualiza totalizadores da tela. 
@type Function

@param nValorAtu, num�rico, Valor atual da venda

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return nil, retorno nulo
/*/
//------------------------------------------------------------------
Static Function  AtuVlrSrc(nValorAtu, oVlrDescScr	, oVlrVendScr	, oVlrAcrsScr, oVlrTotScr, nAuxTot	  ,;
						   cVlrVenda, cVlrDesc		, cVlrAcres		, cVlrTotal	 , nVlrSoma	 , nVlrParcela,;
						   aParcAux)

Local aRet	:= {}

cVlrVenda := TransValScr(nAuxTot)
cVlrTotal := TransValScr(nValorAtu)

If nAuxTot = nValorAtu
	cVlrDesc  := TransValScr(0)
	cVlrAcres := TransValScr(0)
	
Elseif nValorAtu > nAuxTot
	cVlrDesc  := TransValScr(0)
	cVlrAcres := TransValScr(nValorAtu-nAuxTot)
Else
	cVlrDesc  := TransValScr(nAuxTot-nValorAtu)
	cVlrAcres := TransValScr(0)
Endif

oVlrDescScr:CCAPTION := cVlrDesc
oVlrVendScr:CCAPTION := cVlrVenda
oVlrAcrsScr:CCAPTION := cVlrAcres
oVlrTotScr:CCAPTION  := cVlrTotal

oVlrDescScr:ReFresh()
oVlrVendScr:ReFresh()
oVlrAcrsScr:ReFresh()
oVlrTotScr:ReFresh()

aRet := {cVlrVenda, cVlrDesc, cVlrAcres, cVlrTotal}

Return(aRet)

//------------------------------------------------------------------
/*/{Protheus.doc} AtuCondScr()
Alimenta Itens do box da Condi��o de Pagamento
@type Function

@param oTcCond	, objeto, Objeto Condi��es
@param aItens	, array, Itens
@param oTcParc	, objeto, Objeto Parcelas
@param aParcelas, array, Parcelas

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return nil, retorno nulo
/*/
//------------------------------------------------------------------
Static Function AtuCondScr(oTcCond		, aItens	, oTcParc	 , aParcelas, oVlrDescScr, oVlrVendScr	,;
						   oVlrAcrsScr	, oVlrTotScr, nAuxTot	 , cVlrVenda, cVlrDesc	 , cVlrAcres	,;
						   cVlrTotal	, nVlrSoma	, nVlrParcela, aParcAux , nValorAtu)
Local aSe4       := {}
Local aRet	     := {}
Local nx	     := 1
Local ny		 := 1
Local aAreaSE4   := SE4->(GetArea())
Local nVlrAcrsFi := 0
Local nTotParc	 := 0
Local nLinha	 := 0
Local lRecarga   := Alltrim(M->LR_PRODUTO) == GetMV("MV_LJPFID")

DbSelectArea("SE4")
DbSetOrder(1)
DbSeek(xFilial("SE4")) // Pociciona no primeiro registo indexado ( n�o se usa GoTop()! )

While !SE4->(EOF()) .And. SE4->E4_FILIAL == xFilial("SE4") // Se no futuro precisar filtrar alguma regra, � s� acrescentar a l�gica

/*/
Estamos liberando essa primeira vers�o sem a funcionalidade de verifica��o 
do tipo da condi��o de pagamento (Compras, Vendas Recebimento ou todas).
Isso porque estamos no aguardo da inclus�o de dois campos (SE4->E4_TOLVIN e SE4->E4_COMVEN)
pela equipe do financeiro. Assim que os campos forem incluidos, basta retirar os coment�rios.
/*/


/*	
	If !lRecebe .and. SE4->E4_COMVEN == "V" .and. VAL(SE4->E4_CODIGO) < 401 .or. !lRecebe .and. SE4->E4_COMVEN == "X" .and. VAL(SE4->E4_CODIGO) < 401;
	   .or. !lRecebe .and. SE4->E4_COMVEN == "R" .and. lRecarga .and. VAL(SE4->E4_CODIGO) < 401
	   
			Aadd(aSe4,{SE4->E4_CODIGO,SE4->E4_DESCRI,SE4->E4_INFER})

	Elseif lRecebe .and. SE4->E4_COMVEN == "R" .or. lRecebe .and. SE4->E4_COMVEN == "X"
*/
		Aadd(aSe4,{SE4->E4_CODIGO,SE4->E4_DESCRI,SE4->E4_INFER})

//	Endif
	SE4->(DBSkip())
End

//�����������������������������������������Ŀ
//�Array de retorno aRet   	  				�
//�[1][1] - Data Vencimento   				�
//�[1][2] - Valor da Parcela  				�
//�[1][3] - Forma de Pagamento				�
//�[1][4] - Array             				�
//�[1][5] - Vazio             				�
//�[1][6] - Moeda             				�
//�[1][7] - DataBase          				�
//�[1][8] - Vlr Acres.N�o Func				�
//�Para calcular acr�scimo usar nVlrAcrsFi  �
//�������������������������������������������

If Len(aSe4) > 0
	For nx := 1 To Len(aSe4)
		// A Fun��o Lj7CalcPgt retorna exatamente como as parcelas ficar�o de acordo com a condi��o de pagamento, sem alterar o apgtos
		aRet  := Lj7CalcPgt( nVlrParcela , aSe4[nx][1] , {} , nVlrSoma , NIL , NIL , NIL , 0 ,	NIL , NIL , NIL , @nVlrAcrsFi )
		If Len(aRet) > 0
			For ny := 1 To Len(aRet)
				nTotParc += aRet[ny][2] // soma o valor de todas as parcelas para uma �nica condi��o de pagamento
				Aadd(aParcAux,{aRet[ny][1],aRet[ny][2],aRet[ny][3],ny,nx})  // adiciona todas as parcelas para todas as condi��es
			Next ny
			//Adiciona a linha da condi��o de pagamento - BOX 1
			Aadd(aItens,{aSe4[nx][1],aSe4[nx][2],TransValScr(aRet[1][2]),TransValScr(nTotParc), TransValScr(aSe4[nx][3]), nx }) // Vencimento / Descr. Condi��o ,Vlr 1 Parcela , Vlr Total, Lim.Inferior
			nTotParc := 0
		Endif
		
	Next nx
Endif

If Len(aItens) == 0  // Tratamento para quando n�o h� condi��o cadastrada ou valor da parcela a ser pago � igual a 0
	Aadd(aParcAux,{Date(),nTotParc,"",1,1})  // adiciona todas as parcelas para todas as condi��es
	Aadd(aItens,{"","",TransValScr(nTotParc), TransValScr(nTotParc),"",1 }) // Vencimento / Descr. Condi��o ,Vlr 1 Parcela , Vlr Total, Lim. Inferior
Endif
// Monta no objeto os itens com base na pesquisa acima
If Len(aItens) > 0
	oTcCond:SetArray(aItens)
	oTcCond:bLine := {||{ aItens[oTcCond:nAt,01],;
	aItens[oTcCond:nAt,02],;
	aItens[oTcCond:nAt,03],;
	aItens[oTcCond:nAt,04],;
	aItens[oTcCond:nAt,05] } }
	
	oTcCond:Refresh()
	
	nLinha := oTcCond:NAT   // Linha Atual
	
	// Fun��o que atualiza o BOX 2 das parcelas
	AtuParcelas(@oTcParc	, @aParcelas, nLinha	, oTcCond	, @oVlrDescScr	, @oVlrVendScr	, @oVlrAcrsScr,;
	 			@oVlrTotScr	, @nAuxTot	, @cVlrVenda, @cVlrDesc	, @cVlrAcres	, @cVlrTotal	, @nVlrSoma   ,;
				@nVlrParcela, aParcAux	, @nValorAtu)
Endif

RestArea(aAreaSE4)

Return Nil

//------------------------------------------------------------------
/*/{Protheus.doc} TransValScr()
Fun��o que transforma os valores para R$ 0,00
@type Function

@param nValor, num�rico, Valor atual da venda

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return cNewVlr, retorno Novo valor
/*/
//------------------------------------------------------------------
Static Function TransValScr(nValor)
Local cNewVlr	:= ""

cNewVlr := SuperGetMV("MV_SIMB1") + " " + Alltrim(TransForm(nValor,PesqPict("SL1","L1_VLRTOT",,2)))

Return cNewVlr

//------------------------------------------------------------------
/*/{Protheus.doc} AtuParcelas()
Fun��o que atualiza as parcelas de acordo com o item 
posicionado no BOX 1 - condi��o de pagamento
@type Function

@param oTcParc, objeto, Objeto parcelas
@param aParcelas, array, Array que cont�m as parcelas
@param nLinha, num�rico, Linha
@param oTcCond, objeto, Objeto Condi��o de pagamento

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return cNewVlr, retorno Novo valor
/*/
//------------------------------------------------------------------
Static Function AtuParcelas(oTcParc		, aParcelas	, nLinha	 , oTcCond	, oVlrDescScr, oVlrVendScr	,;
						    oVlrAcrsScr	, oVlrTotScr, nAuxTot	 , cVlrVenda, cVlrDesc	 , cVlrAcres	,;
							cVlrTotal	, nVlrSoma  , nVlrParcela, aParcAux , nValorAtu)

Local aRet		:= {}
Local nPos 		:= 0
Local nx		:= 0
Local nVlrParc  := 0

If ValType(nLinha) != "N"
	nLinha := oTcCond:nAt
Endif

// Pesquisa necess�ria para garantir que foi encontrada a parcela correspondente. Apenas para prote��o
nPos := Ascan(aParcAux, { |x| x[5] == oTcCond:AARRAY[nLinha][6]})

If nPos > 0
	
	aParcelas := {} 					// Vari�vel respons�vel por demonstrar as parcelas para cada condi��o de pagamento
	
	For nx := 1 To Len(aParcAux) 		// Varre todas as parcelas para todas as condi��es de pagamento
		If aParcAux[nx][5] = nLinha 	// S� adiciona as parcelas vinculadas ao item da condi��o selecionado
			Aadd(aParcelas,{aParcAux[nx][1],TransValScr(aParcAux[nx][2]),aParcAux[nx][3],aParcAux[nx][4] } ) // Data ; Valor parcela ; Forma Pgto ; Numero Parcela
			nVlrParc += aParcAux[nx][2] // Soma o valor de cada parcela para futuro controle de acr�scimo e desconto
		Endif
	Next nx
	// Monta o objeto as parcelas
	If Len(aParcelas) > 0
		oTcParc:SetArray(aParcelas)
		oTcParc:bLine := {||{ aParcelas[oTcParc:nAt,01],;
		aParcelas[oTcParc:nAt,02],;
		aParcelas[oTcParc:nAt,03],;
		aParcelas[oTcParc:nAt,04] } }
		oTcParc:Refresh()
	Endif
	
Endif

aRet := AtuVlrSrc(nVlrParc		, @oVlrDescScr	, @oVlrVendScr	, @oVlrAcrsScr	, @oVlrTotScr, @nAuxTot		,;
				  @cVlrVenda	, @cVlrDesc	 	, @cVlrAcres	, @cVlrTotal	, @nVlrSoma	 , @nVlrParcela	,;
		  		  aParcAux		, @nValorAtu)																	// Atualiza os totalizadores da tela

Return(aRet)

//------------------------------------------------------------------
/*/{Protheus.doc} ConfSrc()
Atualiza a vari�vel de mem�ria na confirma��o da tela
@type Function

@param oTcCond	, objeto, Objeto Condi��es
@param aItens	, array, Itens
@param oTcParc	, objeto, Objeto Parcelas
@param aParcelas, array, Parcelas

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return nil, retorno nulo
/*/
//------------------------------------------------------------------
Static Function ConfSrc(oTcCond, oNegTela, cVlrTotal)

Local cCondE4	:= ""
Local nVlraux	:= 0
Local lOk		:= .F.
Local nValor	:= Val(Substr(cVlrTotal,3,Len(cVlrTotal)))
Local nTamUser 	:= 25                           // Tamanho do campo do usuario
Local cCaixaSup	:= Space(25)					// Caixa superior

cCaixaSup := PadR( cCaixaSup, nTamUser, " " )

/*/
Estamos liberando essa primeira vers�o sem a funcionalidade de verifica��o 
de tolerancia de limite inferior da condi��o de pagamento.
Isso porque estamos no aguardo da inclus�o de dois campos (SE4->E4_TOLVIN e SE4->E4_COMVEN)
pela equipe do financeiro. Assim que os campos forem incluidos, basta retirar os coment�rios.
/*/

	
If Len(oTcCond:AARRAY) > 0
	
	cCondE4 := oTcCond:AARRAY[oTcCond:NAT][1]

	SE4->(DbSetOrder(1))
	If SE4->(DbSeek(xFilial("SE4")+cCondE4))
		If SE4->E4_INFER <> 0 // Existe Valida��o para o Limite m�nimo
			If nValor <= SE4->E4_INFER
				nVlraux := SE4->E4_INFER - nValor
				/*
				If nVlraux <= SE4->E4_TOLVIN // Se a Diferen�a estiver dentro do limite estabelecido, chama o Superior do Caixa
					If FWAuthSuper(@cCaixaSup)    // Deixa o tratamento para obter qual o superior autorizou pronto para alguma necessidade no futuro.
						lOk := .T.
					Else
						Alert(STR0033) //Senha do Superior inv�lida
					Endif
				Else
				*/
					Alert(STR0029 + SuperGetMV("MV_SIMB1") + Alltrim(Str(SE4->E4_INFER)) + " " + STR0031) // Aten��o! Esta condi��o de pagamento s� poder� ser usada para vendas com um total maior que 
					
				/*
				Endif
				*/
			Else
				lOk := .T.
			Endif
		Else
			lOk := .T.
		Endif
	Endif
	
	If lOk
		M->LQ_CONDPG := cCondE4
		oNegTela:End()
	Endif

Endif
Return lOk

//------------------------------------------------------------------
/*/{Protheus.doc} LJRETSE4()
Fun��o que retorna da Pesquisa para o get da condi��o de pagamento
@type Function

@param cReadVar	, caracter, C�digo da condi��o de pagamento

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return Car�cter, Codigo da condi��o de pagamento
/*/
//------------------------------------------------------------------
Function LJRETSE4()

Local cReadVar := &(ReadVar())

oPgtos:SetFocus()
Lj7SetKeys(.T.)

Return cReadVar

//------------------------------------------------------------------
/*/{Protheus.doc} AtuValorTot()
Fun��o que o Valor das Parcelas considerando a NCC usada
@type Function

@author Marcos Iurato Junior
@since 16/09/2019
@version P12.1.25

@return Nil, Nulo
/*/
//------------------------------------------------------------------
Static Function AtuValorTot()

Local aRet			:= {}
Local nVlrPago  	:= 0
Local nValAbISS 	:= 0
Local nVlrSub		:= 0
Local nSobraNCC		:= 0
Local nValFrete		:= 0


nVlrPago := nNCCUsada + If( LJ220AbISS(), MaFisRet(,'NF_VALISS'), 0 ) // Considera a NCC selecionada

If LJ220AbISS()
	nValAbISS := MaFisRet(,'NF_VALISS')
EndIf

nVlrSub  := LJ7T_SubTotal(2) - nValAbISS

If nNCCUsada - nVlrSub > 0
	nSobraNCC := nVlrSub
Else
	nSobraNCC := nNCCUsada
EndIf

nVlrParcela := nVlrSub - LJ7T_DescV( 2 ) - nSobraNCC
nValFrete   := Lj7CalcFrete()
nVlrSoma    := nValFrete

If nNCCUsada >= (nVlrSub + nValFrete)
	nVlrSoma  := 0
EndIf

aRet := {nVlrParcela, nVlrSoma}

Return (aRet)

/*/{Protheus.doc} LjXjSalRet(cNumOrc)
Busca Venda Varejo que gerou o Pedido de Venda

@param	cNumPed	Numero do Orcamento Varejo

@author Everson S. P. Junior
@since 20/02/2020
@version 12
/*/

Function LjXjSalRet(cNumOrc, cFilorc)

Local cWhere 	:= ""
Local cAliasTmp := GetNextAlias() //Alias temporario
Local aRet   	:= {"","","",""}
Local aArea		:= GetArea()
Local aAreaSL1	:= SL1->(GetArea())
Local cNumFilho := ""

Default cNumOrc:= ""
Default cFilorc:= "" 

LjGrvLog("LjXjSalRet" , "Processo similar ao A410BRETAIL")
LjGrvLog("LjXjSalRet" , "Parametro cNumOrc "+cNumOrc)
LjGrvLog("LjXjSalRet" , "Parametro cFilorc "+cFilorc)
LjGrvLog("LjXjSalRet" , "Chamada QUERY")

//Busca informa��es do cumpom pai
//Condicional para a query		
if SL1->(DbSeek(xFilial("SL1")+cNumOrc)) .AND. !Empty(SL1->L1_SITUA)// cNumOrc for o numero do or�amento Pai deve achar os dados pela L1_ORCRES
	cWhere := "%"
	cWhere += " L1_FILRES = '"  + cFilorc + "'"
	cWhere += " AND L1_ORCRES = '" + cNumOrc + "'"
	cWhere += " AND D_E_L_E_T_ = ''"
	cWhere += "%"	
else //cNumOrc for o numero do or�amento Filha deve achar os dados pelo L1_NUM
		
	cWhere := "%"
	cWhere += " L1_FILIAL = '"  + Iif(Empty(cFilorc),xFilial("SL1"),cFilOrc) + "'"
	cWhere += " AND L1_NUM = '" + cNumOrc + "'"
	cWhere += " AND D_E_L_E_T_ = ''"
	cWhere += "%"
endIf


//Executa a query
BeginSql alias cAliasTmp
	SELECT
	L1_NUM,L1_FILRES, L1_ORCRES
	FROM %table:SL1%
	WHERE %exp:cWhere%
EndSql
		
(cAliasTmp)->(dbGoTop())
		
LjGrvLog("LjXjSalRet" , "QUERY executada "+GetLastQuery()[2])
//Busca informacoes da Venda Varejo
If (cAliasTmp)->(!EOF())

	LjGrvLog("LjXjSalRet" , "L1_FILRES:= " + (cAliasTmp)->L1_FILRES )
	LjGrvLog("LjXjSalRet" , "L1_ORCRES:= " + (cAliasTmp)->L1_ORCRES )
	
	SL1->(dbSetOrder(1)) //L1_FILIAL+L1_NUM
	If SL1->(dbSeek(xFilial("SL1",(cAliasTmp)->L1_FILRES) + (cAliasTmp)->L1_ORCRES))
		aRet[1] := SL1->L1_FILIAL
		aRet[2] := SL1->L1_SERPED
		aRet[3] := SL1->L1_DOCPED
		
		LjGrvLog("LjXjSalRet" , "aRet[1]:= " + SL1->L1_FILIAL )
		LjGrvLog("LjXjSalRet" , "aRet[2]:= " + SL1->L1_SERPED )
		LjGrvLog("LjXjSalRet" , "aRet[3]:= " + SL1->L1_DOCPED )
		                                			
		//Busca informacoes do pdv, pois ao criar o or�amento filho
		//o n�mero do pdv � apagado no or�amento pai.
		DbSelectArea("SL1")
		SL1->(DbSetOrder(1))
		If(SL1->(DbSeek(xFilial("SL1")+(cAliasTmp)->L1_NUM)))
			aRet[4] := SL1->L1_PDV
			LjGrvLog("LjXjSalRet" , "cNumOrc:= " + cNumOrc )
			LjGrvLog("LjXjSalRet" , "aRet[4]:= " + SL1->L1_PDV )
		Endif
	Endif		
EndIf

If Select(cAliasTmp) > 0
	(cAliasTmp)->(dbCloseArea())
EndIf


RestArea(aAreaSL1)
RestArea(aArea)
	
Return aRet 

/*/{Protheus.doc} LjJConPedP
	Para confirmar quando tenho pedido de venda pendente

	@type  Function
	@author Julio.Nery
	@since 14/08/2020
	@version P12
	@param param, param_type, param_descr
	@return lRet, l�gico, valida se o registro foi confirmado
/*/
Function LjJConPedP()
Local cCadastro := STR0034
Local lLOJI701A := ExistFunc("LOJI701A")
Local lisBlind	:= isblind()
Local lRet		:= .F.
Local oDlg		:= NIL
Local nOpca		:= 0

lRet := (AllTrim(SL1->L1_ECFLAG) == "P") .And. lLOJI701A

If lRet
	If lisBlind
		nOpca := 1
	Else 
		DEFINE MSDIALOG oDlg TITLE cCadastro From 3,0 TO 18,80 OF oMainWnd
			
			@ 35, 005 TO 100, 110 OF oDlg PIXEL
			@ 35, 112 TO 100, 225 OF oDlg PIXEL
			@ 35, 227 TO 100, 315 OF oDlg PIXEL

			//Quadro 1
			@ 40,008 Say STR0035 SIZE 30, 07	OF oDlg PIXEL	// "Or�amento:"
			@ 40,038 MSGet SL1->L1_NUM 	When .F. SIZE 30, 08	OF oDlg PIXEL

			@ 52,008 Say STR0036 SIZE 30, 07	OF oDlg PIXEL	// 'Documento:'
			@ 52,038 MSGet SL1->L1_DOC When .F.	 SIZE 30, 08	OF oDlg PIXEL
			
			@ 64, 008 SAY xPadL( STR0037, 36 )	SIZE 30, 07	OF oDlg PIXEL	// Pedido:
			@ 64, 038 MSGET SL1->L1_DOCPED When .F.	 SIZE 30, 08	OF oDlg PIXEL

			@ 76, 008 SAY xPadL( STR0038, 36 )	 SIZE 30, 07	OF oDlg PIXEL	// Serie Pedido:
			@ 76, 038 MSGET SL1->L1_SERPED When .F.	 SIZE 30, 08	OF oDlg PIXEL

			@ 88, 008 SAY xPadL( STR0039, 24 )		 SIZE 30, 07	OF oDlg PIXEL	// Cliente:
			@ 88, 038 MSGET SL1->L1_CLIENTE When .F.	 SIZE 30, 08	OF oDlg PIXEL

			//Quadro 2
			@ 40, 115 SAY xPadL( STR0040, 66 ) SIZE 66, 7 OF oDlg PIXEL // Total de Mercadorias:
			@ 40, 170 MSGET SL1->L1_VLRTOT When .F. Picture "@E 999,999.99"	SIZE 46, 08 OF oDlg PIXEL
			
			@ 52, 115 SAY xPadL( STR0041, 25 ) SIZE 25, 7 OF oDlg PIXEL	// "L�quido:"
			@ 52, 170 MSGET SL1->L1_VLRLIQ When .F. Picture "@E 999,999.99"	SIZE 46, 08 OF oDlg PIXEL

			@ 64, 115 SAY xPadL( STR0042, 36 ) SIZE 36, 7 OF oDlg PIXEL	// Desconto:
			@ 64, 170 MSGET SL1->L1_DESCONT When .F. Picture "@E 999,999.99"	SIZE 46, 08 OF oDlg PIXEL

			//Quadro 3
			@ 40, 230 SAY xPadL( STR0043, 28 ) SIZE 25, 7 OF oDlg PIXEL	// "Emiss�o:"
			@ 40, 260 MSGET SL1->L1_EMISSAO  When .F.							SIZE 46, 08 OF oDlg PIXEL

			@ 52, 230 SAY xPadL( STR0044, 28 ) SIZE 25, 7 OF oDlg PIXEL	// Validade:
			@ 52, 260 MSGET SL1->L1_DTLIM  When .F.							SIZE 46, 08 OF oDlg PIXEL

		ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{|| nOpca := 1,oDlg:End()},{|| nOpca := 2,oDlg:End()}) CENTERED
	EndIf

	If nOpca == 1
		If lLOJI701A
			//Chamada da fun��o de atualiza��o de status no fonte LOJI701A
			//LOJI701A()
		EndIf
		
		If lRet
			Reclock("SL1",.F.)
			REPLACE SL1->L1_ECFLAG WITH "O"
			SL1->(MsUnlock())
			LjGrvLog(SL1->L1_NUM,STR0045, {SL1->L1_NUM,SL1->(Recno())}) //#"Efetuado a libera��o do pedido pendente (proviniente da integra��o)"
			Conout(STR0045)
		Else
			If !lisBlind
				MsgStop(STR0046 + "[" + SL1->L1_NUM + "]") //#"Falha na tentativa de atualiza��o do pedido pendente - Or�amento (L1_NUM)"
			Endif
			LjGrvLog(SL1->L1_NUM,STR0047, { SL1->L1_NUM,SL1->(Recno()) }) //#"Falha na tentativa de atualiza��o do pedido pendente"
		EndIf
	Else
		lRet := .F.
	EndIf
Else
	If lLOJI701A
		//# "N�o � possivel confirmar 'Pedido Pendente' pois o Or�amento n�o est� Pendente (L1_ECFLAG diferente de 'P')"
		If !lisBlind
			MsgStop(STR0048)
		EndIf
		ConOut(STR0048)
		LjGrvLog( NIL, STR0048)
	Else
		//#"Fun��o LOJI701A n�o encontrada no RPO - atualize os fontes da integra��o "
		If !lisBlind
			MsgStop(STR0049)
		EndIf
		ConOut(STR0049)
		LjGrvLog( NIL, STR0049)
	EndIf
EndIf

Return lRet