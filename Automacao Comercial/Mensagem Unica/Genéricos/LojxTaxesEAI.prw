#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "MATXDEF.CH"
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} LjTaxEAI
Fun��o para c�lculo reverso de impostos, quando a Integra��o n�o envia Impostos.
C�lcula IPI

@param aCab      - Cabe�alho do or�amento
@param aItem     - Itens do or�amento

@since 	 04/06/19
@version 2.0
/*/
//------------------------------------------------------------------
Function LjTaxEAI(aCab, aItem)

    Local aArea         := GetArea()
    Local aAreaSB1      := SB1->( GetArea() )
    Local aAreaSF4      := SF4->( GetArea() )
    Local nItem			:= 0
    Local nVlrUnit		:= 0
    Local nVlrItem		:= 0
    Local nAliRedIpi	:= 0
    Local nPosProduto	:= 0
    Local nPosTes       := 0
    Local nPosQuant	    := 0
    Local nPosVrUnit    := 0
    Local nPosValDes	:= 0
    Local nPosVlrIte	:= 0
    Local nPosValIpi    := 0
    Local nPosIpi   	:= 0
    Local nPosBasIpi	:= 0
    Local nPosDesPro	:= 0
    Local aItemLoad		:= {}
    Local nRedBasIpi    := 1
    Local cCodCli       := ""
    Local cLojCli       := ""
    Local cTipoNF       := ""
    Local cCliFor       := ""
    Local cTipoCli      := ""
    Local nVlrIpi       := 0
    Local lAtualiza     := .F.

    If Len(aItem) > 0

        LjGrvLog("LjTaxEAI", "Inicia calculo de impostos - MaFisIni")

        nPosProduto := Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_PRODUTO"})
        nPosTes		:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_TES"    })
        nPosQuant	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_QUANT"  })
        nPosVrUnit	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_VRUNIT" })
        nPosPrcTab	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_PRCTAB" })
        nPosVlrIte	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_VLRITEM"})
        nPosValDes  := Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_VALDESC"})
        nPosDesPro	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_DESCPRO"})
        nPosValIpi	:= Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_VALIPI" })
        nPosBasIpi  := Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_BASEIPI"})
        nPosIpi	    := Ascan(aItem[1], {|x| Alltrim(x[1]) == "LR_IPI"    })

        //Carrega tipo do documento fiscal que dever� ser gerado
        cTipoNF  := aCab[Ascan(aCab,{|x| Alltrim(x[1]) == "LQ_TIPO"})][2]
        cTipoNF  := StrTran(cTipoNF, "V", "N")     //Venda para Normal
        cTipoNF  := StrTran(cTipoNF, "P", "N")     //Pedido para Normal

        cCodCli  := Padr(aCab[Ascan(aCab,{|x| Alltrim(x[1]) == "LQ_CLIENTE"})][2], TamSx3("LQ_CLIENTE")[1])
        cLojCli  := Padr(aCab[Ascan(aCab,{|x| Alltrim(x[1]) == "LQ_LOJA"})][2]   , TamSx3("LQ_LOJA")[1]   )
        cCliFor  := IIF(cTipoNF $ "B|D", "F", "C")
        cTipoCli := aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_TIPOCLI"})][2]

        //Inicializa o Calculo das operacoes Fiscais
        MaFisIni(	cCodCli			,;	// 01-Codigo Cliente/Fornecedor
                    cLojCli			,;	// 02-Loja do Cliente/Fornecedor
                    cCliFor	        ,;	// 03-C:Cliente , F:Fornecedor
                    cTipoNF         ,; 	// 04-Tipo da NF( "N","D","B","C","P","I","S" ) 
                    cTipoCli		,;	// 05-Tipo do Cliente/Fornecedor
                    Nil				,;	// 06-Relacao de Impostos que suportados no arquivo
                    Nil				,;	// 07-Tipo de complemento
                    .F.				,;	// 08-Permite Incluir Impostos no Rodape .T./.F.
                    "SB1"			,;	// 09-Alias do Cadastro de Produtos - ("SBI" P/ Front Loja)
                    "LOJA701"     	,;	// 10-Nome da rotina que esta utilizando a funcao
                    Nil				,;	// 11-Tipo de documento
                    Nil				,;	// 12-Especie do documento 
                    Nil				,;	// 13-Codigo e Loja do Prospect 
                    Nil				,;	// 14-Grupo Cliente
                    Nil				,;	// 15-Recolhe ISS
                    Nil				,;	// 16-Codigo do cliente de entrega na nota fiscal de saida
                    Nil				,;	// 17-Loja do cliente de entrega na nota fiscal de saida
                    Nil				,;	// 18-Informacoes do transportador [01]-UF,[02]-TPTRANS
                    .F.				,;	// 19- No momento o PDV nao emite NF , por isso sempre falso   
                    .T.				,;  // 20-Define se calcula IPI (SIGALOJA)
                    Nil				,;  // 21-Pedido de Venda
                    Nil             ,;	// 22	
                    Nil             ,;	// 23
                    Nil             ,;	// 24
                    Nil             ,;  // 25
                    /*cTpFrete*/)		// 26

        For nItem:=1 To Len(aItem)

            nRedBasIpi  := 1
            nVlrUnit    := aItem[nItem][nPosVrUnit][2]
            nVlrItem    := aItem[nItem][nPosVlrIte][2]

            //Posiciona nas tabelas
            SB1->( DbSetOrder(1) )  //B1_FILIAL+B1_COD
            SB1->( DbSeek(xFilial("SB1") + aItem[nItem][nPosProduto][2]) )

            SF4->( DbSetOrder(1) )  //F4_FILIAL+F4_CODIGO
            SF4->( DbSeek(xFilial("SF4") + aItem[nItem][nPosTes][2]) )
            
            //Inicializa o Calculo das operacoes Fiscais por item 
            aItemLoad := {}
            Aadd( aItemLoad , SB1->B1_COD               	    ) // IT_PRODUTO
            Aadd( aItemLoad , SF4->F4_CODIGO                    ) // IT_TES
            Aadd( aItemLoad , ""								) // IT_CODISS
            Aadd( aItemLoad , aItem[nItem][nPosQuant][2]		) // IT_QUANT
			Aadd( aItemLoad , ""                                ) // IT_NFORI
            Aadd( aItemLoad , ""								) // IT_SERIORI
            Aadd( aItemLoad , SB1->( Recno() )                  ) // IT_RECNOSB1
            Aadd( aItemLoad , SF4->( Recno() )                  ) // IT_RECNOSF4
            Aadd( aItemLoad , 0		 							) // IT_RECORI
            Aadd( aItemLoad , ""            					) // IT_LOTECTL
            Aadd( aItemLoad , ""								) // IT_NUMLOTE
            Aadd( aItemLoad , ""								) // IT_PRDFIS
            Aadd( aItemLoad , ""								) // IT_RECPRDF
            Aadd( aItemLoad , "" 				 				) // IT_TPOPER

            //Rotina inicializacao do item da funcao Fiscal
            LjGrvLog("LjTaxEAI", "Carrega informa��es do item - MaFisIniLoad", aItemLoad)
            MaFisIniLoad(nItem, aItemLoad, .F.)
            
            MaFisLoad( "IT_PRCUNI" 	, nVlrUnit                      , nItem)
            MaFisLoad( "IT_DESCONTO", aItem[nItem][nPosValDes][2]   , nItem)
            MaFisLoad( "IT_DESCTOT" , aItem[nItem][nPosDesPro][2] 	, nItem)
            MaFisLoad( "IT_VALMERC" , nVlrItem                      , nItem)
            
            //Fas o c�lculo do item
            LjGrvLog("LjTaxEAI", "C�lcula item - MaFisRecal", nItem)
            MaFisRecal("" , nItem) 

            //Calculo reverso do IPI
            nVlrIpi := MaFisRet(nItem, "IT_VALIPI")
            If nVlrIpi > 0

                LjGrvLog("LjTaxEAI", "Faz c�lculo reverso do IPI", nItem)            

                //Ipi de Pauta
                If SB1->B1_VLR_IPI > ( nVlrIpi / aItem[nItem][nPosQuant][2] )

                    nVlrUnit := nVlrUnit - SB1->B1_VLR_IPI

                //Ipi por aliquota com ou sem redu��o de base
                Else
            
                    //Reduz aliquota de IPI, caso tenha redu��o de base
                    nRedBasIpi  := IIF(SF4->F4_BASEIPI > 0, SF4->F4_BASEIPI / 100, 1) 
                    nAliRedIpi  := MaFisRet(nItem, "IT_ALIQIPI") * nRedBasIpi

                    nVlrUnit    := (nVlrItem / ( (nAliRedIpi / 100) + 1) ) / aItem[nItem][nPosQuant][2]
                EndIf

                //Atualiza valores
                nVlrItem := nVlrUnit * aItem[nItem][nPosQuant][2]

                aItem[nItem][nPosVrUnit][2] := nVlrUnit
                aItem[nItem][nPosVlrIte][2] := nVlrItem
                lAtualiza                   := .T.
            Endif                

            //Atualiza a MatxFis com os valores do Item
            MaFisLoad("IT_PRCUNI"   , aItem[nItem][nPosVrUnit][2] , nItem)
            MaFisLoad("IT_DESCONTO" , aItem[nItem][nPosValDes][2] , nItem)
            MaFisLoad("IT_DESCTOT"  , aItem[nItem][nPosDesPro][2] , nItem)
            MaFisLoad("IT_VALMERC"  , aItem[nItem][nPosVlrIte][2] , nItem)
            
            //Recalcula item
            LjGrvLog("LjTaxEAI", "Recalcula item - MaFisRecal", nItem)
            MaFisRecal("", nItem)

            //Atualiza item com valores calculados
            If lAtualiza
                aItem[nItem][nPosVrUnit][2] := MaFisRet(nItem, "IT_PRCUNI" )    //Pre�o Unit�rio
                aItem[nItem][nPosPrcTab][2] := MaFisRet(nItem, "IT_PRCUNI" )    //Pre�o de Tabela
                aItem[nItem][nPosVlrIte][2] := MaFisRet(nItem, "IT_VALMERC")    //Valor do Item
                aItem[nItem][nPosValIpi][2] := MaFisRet(nItem, "IT_VALIPI" )    //Valor IPI

                If nPosBasIpi > 0 
                    aItem[nItem][nPosBasIpi][2] := MaFisRet(nItem, "IT_BASEIPI") //Base IPI
                EndIf

                If nPosIpi > 0 
                    aItem[nItem][nPosIpi][2] := MaFisRet(nItem, "IT_ALIQIPI")    //Aliquota IPI
                EndIf

                LjGrvLog("LjTaxEAI", "Item atualizado", aItem[nItem])
            EndIf

            //Fecha o calculo do item e atualiza os totalizadores do cabe�alho
            MaFisEndLoad(nItem, 2) 
        Next nItem

        //Atualiza cabe�alho com valores calculados
        If lAtualiza
            aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_VLRTOT" })][2] := MaFisRet(,"NF_TOTAL")
            aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_VLRLIQ"})][2]  := MaFisRet(,"NF_TOTAL")
            aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_VALBRUT"})][2] := MaFisRet(,"NF_TOTAL")
            aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_VALMERC"})][2] := MaFisRet(,"NF_VALMERC")
            aCab[Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_VALIPI"})][2]  := MaFisRet(,"NF_VALIPI")

            If ( nPosBasIpi := Ascan(aCab, {|x| Alltrim(x[1]) == "LQ_BASEIPI"}) ) > 0
                aCab[nPosBasIpi][2] := MaFisRet(,"NF_BASEIPI")
            Endif

            LjGrvLog("LjTaxEAI", "Cabe�alho atualizado", aCab)
        EndIf

        LjGrvLog("LjTaxEAI", "Finaliza c�lculo de impostos - MaFisEnd")
        MaFisEnd()

        FwFreeObj(aItemLoad)

        RestArea(aAreaSF4)
        RestArea(aAreaSB1)
        RestArea(aArea)
    Endif
		
Return Nil