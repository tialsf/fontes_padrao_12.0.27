#INCLUDE "PROTHEUS.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} LjxjPvlSd2
Rotina para atualizar os valores de impostos a partir dos valores gerados pelo loja.
Utilizada na emis�o de documento de sa�da.
Uso MaPvl2Sd2 - Trecho retirado do fonte MATA461

@author	 Rafael Tenorio da Costa
@since 	 12/04/19
@version 1.0
/*/
//------------------------------------------------------------------
Function LjxjPvlSd2(nItem, aRateio)

    Local aArea         := GetArea()
    Local aAreaSL2      := SL2->( GetArea() )
    Local lERakuten     := SuperGetMV("MV_LJECOMM", , .F.)      //Integra��o E-commerce Rakuten Antiga
    Local lECCia        := SuperGetMv("MV_LJECOMO", , .F.)      //Integra��o E-commerce CiaShop Antiga
    Local nTamSL2Ite    := TamSx3("L2_ITEM")[1]                 //Tamanho do campo L2_ITEM
    Local nTamSL2Pro    := TamSx3("L2_PRODUTO")[1]              //Tamanho do campo L2_PRODUTO
    Local nTamSD2Des    := TamSx3("D2_DESCON")[2]               //Quantidade decimal D2_DESCON
    Local cChaveSL2     := ""
    Local cFiltroSL1    := ""   //Filtro da SL1
    Local nECBaseIPI    := 0    //Base do IPI
    Local nECVlrIPI     := 0    //Valor do IPI
    Local nECVlIPI2     := 0    //Valor do IPi alterado
    Local nECVlMerc     := 0    //Valor da Mercadoria
    Local nEcValor      := 0    //Valor do Item do e-commerce
    Local cAliSL2       := ""   //Alias da SL2

    //aRateio - Utilizado no MATA461
    #DEFINE RT_DESCONT  08
    #DEFINE RT_PRECOIT  09
    #DEFINE RT_PDESCON  10

    //Integra��es e-commerce Rakuten e CiaShop Antigas 
    If lERakuten .Or. lECCia

        //---Trecho retirado o fonte MATA461
        If !Empty(SC5->C5_PEDECOM)

            //Verifica se Existe altera��o no valor do IPI
            nECBaseIPI  := MaFisRet(nItem, "IT_BASEIPI")    //Base do IPI
            nECVlrIPI   := MaFisRet(nItem, "IT_VALIPI" )    //Valor do IPI
            nECVlMerc   := MaFisRet(nItem, "IT_VALMERC") -  MaFisRet(nItem,"IT_DESCONTO") + Round(aRateio[RT_DESCONT] + aRateio[RT_PDESCON], nTamSD2Des)    //Valor da mercadoria, exceto o desconto na condi��o

            If nECBaseIPI > 0 .And. nECVlrIPI > 0
            
                cAliSL2    := GetNextAlias()

                cFiltroSL1 := " SELECT (L2_ECVALOR / L2_QUANT) ECUNIT  "
                cFiltroSL1 +=   " FROM "  + RetSqlName("SL2") + " SL2,  "+ RetSqlName("SL1") + " SL1 "
                cFiltroSL1 += " WHERE 	SL2.L2_ITEM  = '" + PadR(SC6->C6_ITEM, nTamSL2Ite) +  "' AND SL1.L1_NUM = SL2.L2_NUM "
                cFiltroSL1 +=   " AND  SL2.D_E_L_E_T_ = ' ' AND SL2.L2_FILIAL = '" + xFilial("SL2") + "' "
                cFiltroSL1 +=   " AND  SL1.L1_PEDRES = '" + Alltrim(SC5->C5_NUM) + "' AND  SL1.D_E_L_E_T_  = ' ' AND SL1.L1_FILIAL = '" + xFilial("SL1") + "' "

                dbUseArea(.T., "TOPCONN", TCGENQRY(,,cFiltroSL1),cAliSl2, .F., .T.)

                nEcValor := (cAliSl2)->ECUNIT * SC9->C9_QTDLIB 

                If (cAliSl2)->(!Eof()) .AND. nEcValor > 0 
                    //O campo L2_ECVALOR Somente � gravado pela integra��o e-commerce CiaShop e caso a funcionalidade de ajuste de IPI esteja habilitada MV_LJECMMA
                    //e somente eh ajustado em itens tributados pelo IPI
                    If (nECVlMerc + nECVlrIPI) <> nEcValor 
                        
                        nECVlIPI2 := NoRound(nEcValor - nECVlMerc,2)

                        If nECVlIPI2 <> nECVlrIPI .AND. nECVlIPI2 > 0
                            MaFisAlt("IT_VALIPI", nECVlIPI2, nItem, .T.)    //For�a a atualiza��o da Base do IPI
                        EndIf
                    EndIf

                EndIf
                (cAliSl2)->(DbCloseArea())
            EndIf

        EndIf
        //---Trecho retirado o fonte MATA461

    //Integra��o via mensagem �nica
    Else

        If !Empty(SC5->C5_ORCRES)

            //Verifica se venda foi gerada pela integra��o
            DbSelectArea("SL1")
            SL1->( DbSetOrder(1) )     //L1_FILIAL+L1_NUM
            If SL1->( DbSeek(SC6->C6_FILIAL + SC5->C5_ORCRES) ) .And. Posicione("SL1", 1, SL1->L1_FILIAL + SL1->L1_ORCRES, "L1_ORIGEM") == "N"

                //Carrega a chave do SL2
                cChaveSL2 := SC6->C6_FILIAL + SC5->C5_ORCRES + PadR(SC6->C6_ITEM, nTamSL2Ite) + PadR(SC6->C6_PRODUTO, nTamSL2Pro)

                //Atualiza valores de impostos se for integra��o
                DbSelectArea("SL2")
                SL2->( DbSetOrder(1) )     //L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
                If SL2->( DbSeek(cChaveSL2) )

                    aRateio[RT_PRECOIT][1] := SL2->L2_PRCTAB
                    aRateio[RT_PRECOIT][2] := SL2->L2_VRUNIT
                    aRateio[RT_PRECOIT][3] := SL2->L2_VLRITEM

                    MaFisLoad("IT_PRCUNI" , SL2->L2_VRUNIT , nItem)
                    MaFisLoad("IT_VALMERC", SL2->L2_VLRITEM, nItem)

                    If MaFisRet(nItem, "IT_VALIPI") > 0

                        If SL2->( ColumnPos("L2_BASEIPI") ) > 0
                            MaFisLoad("IT_BASEIPI", SL2->L2_BASEIPI, nItem)
                        EndIF
                        
                        MaFisLoad("IT_VALIPI" , SL2->L2_VALIPI , nItem)
                    EndIf

                EndIf
            EndIf
        EndIf
        
    EndIf

    RestArea(aAreaSL2)
    RestArea(aArea)

Return Nil