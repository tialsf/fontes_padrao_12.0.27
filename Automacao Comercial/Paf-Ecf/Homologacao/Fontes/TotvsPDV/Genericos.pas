unit Genericos; 

interface

uses Dialogs, SysUtils, Windows, Forms, ExtCtrls, StdCtrls, Controls, Messages,
     Classes, Graphics, IniFiles, FileCtrl;

const
  CurrentDate: String = '17/10/2013';           // mudar a cada gera��o para alterar o MD5
  Valid      : String = '20131017TOTVSPDV';    // variavel com o conteudo que sera validado
  Executavel : String = 'SMARTCLIENT.EXE'; // programa padrao do protehus
  Funcao     : String = 'SIGALOJ';            // funcao a ser executado pelo WinExec
  Ini        : String = 'CLIENT.INI';      // arquivo com as configuracoes o programa - TESTE

type TaString = array of String;

  Procedure ExecClient(sCommand, sPrograma, sModulo, sValid, sParams, sAmbiente, sTcp : String);
  Procedure MontaArray( sTexto, sSeparador:String; Var aFormas: TaString );
  Function LeArqIni(sPath, sArquivo, sSecao, sCampo, sDefault: String ) : String;
  Function GrvArqIni(sPath, sArquivo, sModulo, sTcp, sAmbiente: String ) : Boolean;

implementation

//------------------------------------------------------------------------------
Procedure ExecClient(sCommand, sPrograma, sModulo, sValid, sParams, sAmbiente, sTcp : String);
Var nExec    , nCont : Integer;
    aParams  : TaString;
begin

  // Quebra a String em uma Array
  MontaArray(sParams, '|', aParams);

  // Monta Comando
  sCommand := sCommand + ' -E=' + sAmbiente +
                         ' -C=' + sTcp      +
                         ' -P=' + sPrograma +
                         ' -A=' + sModulo   +
                         ' -A=' + sValid    ;

  For nCont := 0 To (Length(aParams) -1) Do
  begin
    sCommand := sCommand + ' ' + aParams[nCont];
  end;

  Try
    nExec := WinExec(PChar( sCommand ), SW_SHOWNORMAL);
    If nExec <= 31 then
      halt;
  Except
    halt;
  end

end;

//----------------------------------------------------------------------------
procedure MontaArray( sTexto, sSeparador:String; Var aFormas: TaString );
var
  iTamanho : Integer;
  iPos : Integer;
  sFormas : String;
begin
  if Copy(sTexto,1,1) = sSeparador then
    sTexto := Copy(sTexto,2,Length(sTexto));

  iTamanho := 0;
  While (Pos(sSeparador, sTexto) > 0) do
  begin
    Inc(iTamanho);
    SetLength( aFormas, iTamanho );
    iPos := Pos(sSeparador, sTexto);
    if iPos = 1 then
      sFormas := ''
    else
      sFormas := Copy(sTexto, 1, iPos-1);
    aFormas[iTamanho-1] := sFormas ;
    sTexto := Copy(sTexto, iPos+1, Length(sTexto));
  end;
  if Length(sTexto)>0 then
  begin
    Inc(iTamanho);
    SetLength( aFormas, iTamanho );
    aFormas[iTamanho-1] := sTexto;
  end;
end;

//------------------------------------------------------------------------------
Function LeArqIni(sPath, sArquivo, sSecao, sCampo, sDefault: String ) : String;
Var
  fArquivo : TIniFile;
  sRetorno : String;
begin

  If not (Copy(sPath , Length(sPath), 1) = '\') then
    sPath := sPath + '\';

  try
    fArquivo := TInifile.Create( sPath + sArquivo );
    sRetorno := fArquivo.ReadString( sSecao, sCampo, sDefault );
  except
    sRetorno := sDefault;
  end;

  fArquivo.Free;
  Result := sRetorno;
end;

//------------------------------------------------------------------------------
Function GrvArqIni(sPath, sArquivo, sModulo, sTcp, sAmbiente: String ) : Boolean;
Var
  sIni : String;
  fArq : TIniFile;
  lRet : Boolean;
begin
  lRet := True;

  If not (Copy(sPath , Length(sPath), 1) = '\') then
    sPath := sPath + '\';

  sIni := sPath + sArquivo;

    Try
      fArq := TInifile.Create( sIni );

      fArq.WriteString( 'config', 'lastmainprog', sModulo );
      fArq.WriteString( 'config', 'envserver', sAmbiente );

      fArq.WriteString( 'drivers', 'active', sTcp );

      fArq.Free;
    Except
      lRet := False;
    End;

    Result := lRet;
end;



end.
