unit TelaParametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, jpeg, ExtCtrls, Genericos;

type
  TfrmParametros = class(TForm)
    imgTotvs: TImage;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btnOk: TButton;
    btnCancela: TButton;
    cbxModulo: TComboBox;
    cbxTcp: TComboBox;
    cbxAmbiente: TComboBox;
    Image1: TImage;
    Image2: TImage;
    procedure btnCancelaClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParametros: TfrmParametros;

implementation

{$R *.DFM}

procedure TfrmParametros.btnCancelaClick(Sender: TObject);
begin
  frmParametros.Close;
end;

procedure TfrmParametros.btnOkClick(Sender: TObject);
Var
  sModulo, sTcp, sAmbiente : String;
  nCont  : Integer;
  sParams, sParametro : String;
begin

    SModulo   := UpperCase( Trim(cbxModulo.Text) );
    sTcp      := UpperCase( Trim(cbxTcp.Text) );
    sAmbiente := UpperCase( Trim(cbxAmbiente.Text) );

    If (sModulo = '') or (sTcp = '') or (sAmbiente = '') then
        frmParametros.Close
    Else
    begin

        For nCont := 1 To ParamCount Do
        begin
            sParametro := UpperCase( ParamStr(nCont) );

            If (Pos('-P=', sParametro) = 0) and (Pos('-E=', sParametro) = 0) and (Pos('-C=', sParametro) = 0) Then
                sParams := sParams + '|' + sParametro;
        end;

        If (sModulo = 'SIGAFRT') or (sModulo = 'SIGALOJA') or (sModulo = 'SIGAPAF') then
            ExecClient(Executavel, Funcao, sModulo, Valid, sParams, sAmbiente, sTcp)
        Else
            ExecClient(Executavel, sModulo, '', '', sParams, sAmbiente, sTcp);

       // Grava Dados da Ultima configuração
       GrvArqIni(ExtractFilePath(Application.ExeName), Ini, sModulo, sTcp, sAmbiente);

       // Fecha Formulario     
       frmParametros.Close;

    end;

end;

procedure TfrmParametros.FormCreate(Sender: TObject);
begin

  // Le Arquivo Ini 
  cbxModulo.Text   := LeArqIni(ExtractFilePath(Application.ExeName), Ini, 'config', 'lastmainprog', 'SIGAFRT');
  cbxTcp.Text      := LeArqIni(ExtractFilePath(Application.ExeName), Ini, 'drivers', 'active', 'tcp');
  cbxAmbiente.Text := LeArqIni(ExtractFilePath(Application.ExeName), Ini, 'config', 'envserver', 'environment');

end;

end.
