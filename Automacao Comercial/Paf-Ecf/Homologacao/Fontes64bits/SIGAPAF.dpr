program SIGAPAF;

uses
  SysUtils,
  Windows,
  Dialogs,
  Forms,
  TelaParametros in 'TelaParametros.pas' {frmParametros},
  Genericos in 'Genericos.pas',
  Imagens in 'Imagens.pas' {Form1};

{$R *.RES}

var
  sCommand,sParametro,sParams,
  sModulo,sAmbiente,sTcp : String;
  nCont : Integer;
begin
  sCommand   := '';
  sParametro := '';
  sModulo    := '';
  sAmbiente  := '';
  sTcp       := '';
  sParams    := '';

  sCommand := Executavel;

  For nCont := 1 To ParamCount Do
  begin
     sParametro := UpperCase( ParamStr(nCont) );

     //Modulo
     If Pos('-P=SIGA', sParametro) > 0 Then
        sModulo := Copy( sParametro, 4, Length(sParametro) )

     //Ambiente
     Else If Pos('-E=', sParametro) > 0 Then
        sAmbiente := Copy( sParametro, 4, Length(sParametro) )

     //TCP
     Else If Pos('-C=', sParametro) > 0 Then
        sTcp := Copy( sParametro, 4, Length(sParametro) )

     //Outros Parametros
     Else
        sParams := sParams + '|' + sParametro
  end;

  //Executa Modulo
  If (sModulo <> '') and (sAmbiente <> '') and (sTcp <> '') Then
  begin
     DeleteFile(PChar(ExtractFilePath(Application.ExeName)+'\'+'PAFEMP.TXT'));
     ExecClient(sCommand, Funcao, sModulo, Valid, sParams, sAmbiente, sTcp);
  end
  //Abrir Tela para Sele��o dos Parametros
  Else
      ShowMessage('Para acesso ao PAF-ECF, configure os par�metros do atalho SIGAPAF com o conteudo: -P=SIGAFRT ou -P=SIGALOJA -E=<ambiente> -C=<TCP> -m -A= -A=<esta��o>.');

  Application.Terminate;
end.
