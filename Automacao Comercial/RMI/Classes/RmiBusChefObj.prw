#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIBUSCHEFOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiBusChefObj
Classe respons�vel pela busca de dados no Chef
    
/*/
//-------------------------------------------------------------------
Class RmiBusChefObj From RmiBuscaObj

    Data cLayoutFil     As Character    //Layout referente as filias que serao processadas pela API data de integra��o

    Data oLayoutFil     As Objetc       //Objeto jSonObject com layout das filias utilizadas pela API data de integra��o

    Data cDtIntegracao  As Character    //Data da Integra��o da Venda usada na API data de Integra��o

    Data cTime          As Character    //Hora para valida��o 

    Method New()	//Metodo construtor da Classe

    Method Busca()  //Metodo responsavel por buscar as informa��es no Assinante

    Method Venda()	//Metodo para carregar a publica��o de venda

    Method GravaFilial()	//Metodo para gravar a data e hora da venda da API - CapaVenda/DatadeIntegracao

    Method CheckTime(cTime)

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiBusChefObj
    
    _Super:New("CHEF")

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Busca
Metodo responsavel por enviar a mensagens ao Chef

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Busca() Class RmiBusChefObj

    Local nItem := 0
    Local lDtIntegra := .F. //Variavel para verificar se esta configurado o metodo ListPorDataIntegracaoChefWeb

    //Atualiza o token no body - para o Chef o token vale apenas para uma utiliza��o
    self:oBody["token"] := self:cToken
    self:cBody          := self:oBody:ToJson()

    self:cLayoutFil     := ""

    self:oLayoutFil     := Nil

    self:cDtIntegracao  := ""

    If ("MHP")->(FieldPos("MHP_LAYFIL")) > 0
    
        self:cLayoutFil := AllTrim(MHP->MHP_LAYFIL)
        lDtIntegra      := SubStr(self:oConfProce["url"],Rat("/",self:oConfProce["url"]) + 1) == "ListPorDataIntegracaoChefWeb"

        If lDtIntegra

            If Empty(self:cLayoutFil)
                self:cLayoutFil := GatMHP(AllTrim(MHP->MHP_FILPRO),"CHEF","VENDA",.F.)
                RecLock("MHP",.F.)
                MHP->MHP_LAYFIL := self:cLayoutFil
                MHP->( MsUnLock() )
            EndIf

            //Carrega layout de filial para 
            If !Empty(self:cLayoutFil) .And. SubStr(self:cLayoutFil, 1, 1) == "{"

                self:oLayoutFil := JsonObject():New()
                self:oLayoutFil:FromJson(self:cLayoutFil)

                If !(self:oLayoutFil == Nil) .and. Valtype(self:oLayoutFil["Filiais"]) == "A"
                    For nItem:=1 To Len( self:oLayoutFil["Filiais"] )
                        If Alltrim(self:oLayoutFil["Filiais"][nItem]["Filial"])  == Alltrim(self:aArrayFil[self:nFil][2])
                            If !Empty(self:oLayoutFil["Filiais"][nItem]["Data"])
                                self:cDtIntegracao := substr(FwTimeStamp(3,CtoD(self:oLayoutFil["Filiais"][nItem]["Data"])),1,rat("T",FwTimeStamp(3,CtoD(self:oLayoutFil["Filiais"][nItem]["Data"])))) + self:oLayoutFil["Filiais"][nItem]["Hora"]

                                If self:cDtIntegracao < self:oLayoutEnv["DataInicialIntegracaoChefweb"]  
                                    self:oBody["DataHoraUltimaIntegracaoChefweb"] := self:oLayoutEnv["DataInicialIntegracaoChefweb"]
                                Else 
                                    If  self:cDtIntegracao <  FwTimeStamp(3,dDataBase)    
                                        self:oBody["DataInicialIntegracaoChefweb"]     := substr(self:cDtIntegracao, 1,rat("T",self:cDtIntegracao)) +  self:oLayoutFil["Filiais"][nItem]["Hora"]
                                        self:oBody["DataHoraUltimaIntegracaoChefweb"]  := substr(self:cDtIntegracao, 1,rat("T",self:cDtIntegracao)) +  self:oLayoutFil["Filiais"][nItem]["Hora"]
                                        self:oBody["DataFinalIntegracaoChefweb"]       := substr(FwTimeStamp(3,CtoD(self:oLayoutFil["Filiais"][nItem]["Data"])+1),1,rat("T",FwTimeStamp(3,CtoD(self:oLayoutFil["Filiais"][nItem]["Data"])))) + self:oLayoutFil["Filiais"][nItem]["Hora"]
                                        self:cBody                                     := self:oBody:ToJson()
                                    EndIF
                                EndIf
                            EndIf
                        EndIf
                    Next nItem
                EndIf

            EndIf
        Else
            LjGrvLog( "RMIBUSCHEFOBJ", "N�o esta configurado para utilizar o metodo ListPorDataIntegracaoChefWeb", SubStr(self:oConfProce["url"],Rat("/",self:oConfProce["url"]) + 1) )
        EndIf

    EndIf

    _Super:Busca()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Venda
Metodo para carregar a publica��o de venda

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Venda() Class RmiBusChefObj

    Local nVenda     := 0
    Local nStatus    := 0                            //1=Aberto, 2=Finalizado, 3=Cancelado
    Local nItem      := 0
    Local aTags      := self:oLayoutPub:GetNames()   //Retorno o nome das tags do objeto publica    
    Local cHoraAux   := ""                           //Variavel auxiliar para uso de tempo
    Local cTimeAux   := ""                           //Variavel auxiliar para uso de tempo

    //Verifica se as vendas foram retornadas
    If !self:oRetorno["Sucesso"]

        self:lSucesso := .F.
        self:cRetorno := AllTrim(self:cProcesso) + " - " + self:oRetorno["Erros"][1]["DescricaoErro"]

    //Processa as vendas
    Else

        //Verifica se existem vendas
        If Len(self:oRetorno["Vendas"]) == 0

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0003, { ProcName(), self:cRetorno} )   //"[#1] N�o h� vendas a serem baixadas: #2"

            If !(self:oLayoutFil == Nil) 
                self:GravaFilial()
            EndIf
        Else

            For nVenda:=1 To Len(self:oRetorno["Vendas"])

                //Status da venda no Chef 1=Em Aberto, 2=Finalizada, 3=Cancelada
                nStatus := self:oRetorno["Vendas"][nVenda]["StatusVenda"]

                //S� carrega vendas Finalizadas ou Canceladas
                If nStatus > 1

                    //Carrega informa��es da publica��o que ser� gerada
                    self:oRegistro      := self:oRetorno["Vendas"][nVenda]  //Objeto que poder� ser utilizado para gerar o layout da publica��o. (MHP_LAYPUB)

                    self:cMsgOrigem := FWJsonSerialize(self:oRegistro) 

                    self:cChaveUnica    := self:oRegistro["ChaveVenda"]     //Chave �nica do registro publicado. (MHQ_CHVUNI)
                    self:cEvento        := IIF(nStatus == 2, "1", "2")      //Evento da publica��o 1=Upsert, 2=Delete (MHQ_EVENTO)

                    self:cDtIntegracao  := self:oRegistro["DataIntegracaoChefweb"]

                    //Verifica se a venda j� foi publicada
                    If self:AuxExistePub()
                        Loop
                    EndIf
                    
                    If !(self:oLayoutFil == Nil) .and. Valtype(self:oLayoutFil["Filiais"]) == "A"
                        For nItem:=1 To Len( self:oLayoutFil["Filiais"] )
                            If Alltrim(self:oLayoutFil["Filiais"][nItem]["Filial"])  == Alltrim(self:oPublica["L1_FILIAL"])
                                cTimeAux := self:CheckTime(substr(self:cDtIntegracao,rat("T",self:cDtIntegracao) +1 , 8))
                                self:cDtIntegracao :=  DtoC(Stod(StrTran( self:cDtIntegracao, "-", "" )))
                                If self:cDtIntegracao > self:oLayoutFil["Filiais"][nItem]["Data"]
                                    self:oLayoutFil["Filiais"][nItem]["Data"] := self:cDtIntegracao
                                    self:oLayoutFil["Filiais"][nItem]["Hora"] := cTimeAux
                                Else
                                    cHoraAux := ElapTime(self:oLayoutFil["Filiais"][nItem]["Hora"],cTimeAux)
                                    cTimeAux := Iif(self:oLayoutFil["Filiais"][nItem]["Hora"] < cHoraAux, self:oLayoutFil["Filiais"][nItem]["Hora"], cTimeAux)
                                    self:oLayoutFil["Filiais"][nItem]["Hora"] := cTimeAux
                                EndIf
                            EndIf
                        Next nItem
                    EndIf

                    //Grava a publica��o
                    self:Grava()

                    If self:lSucesso .and. !(self:oLayoutFil == Nil) 
                        self:GravaFilial()
                    EndIf

                    //Limpa objeto da publica��o
                    FwFreeObj(self:oRegistro)
                    FwFreeObj(self:oPublica)
                    self:oRegistro  := Nil
                    self:oPublica   := Nil
                    
                EndIf

            Next nVenda
        EndIf
    EndIf

    Asize(aTags, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GravaFilial
Metodo responsavel por gravar a data e hora da venda para 
a API - CapaVenda/DatadeIntegracao

@author  Danilo Rodrigues
@version 1.0
/*/
//-------------------------------------------------------------------
Method GravaFilial() Class RmiBusChefObj

Local nItem     := 0

    self:cDtIntegracao := DtoC(Stod(StrTran( self:cDtIntegracao, "-", "" )))

    If !self:lSucesso 
        If !(self:oLayoutFil == Nil) .and. Valtype(self:oLayoutFil["Filiais"]) == "A"
            For nItem:=1 To Len( self:oLayoutFil["Filiais"] )
                If Alltrim(self:oLayoutFil["Filiais"][nItem]["Filial"])  ==  Alltrim(self:aArrayFil[self:nFil][2])
                    If self:cDtIntegracao <= self:oLayoutFil["Filiais"][nItem]["Data"]
                        self:oLayoutFil["Filiais"][nItem]["Data"] := DtoC(Stod(StrTran( self:oBody["DataFinalIntegracaoChefweb"], "-", "" )))
                    EndIf
                EndIf
            Next nItem
        EndIf
    EndIF

    //Atualiza o token no body - para o Chef o token vale apenas para uma utiliza��o
    If !self:oLayoutFil == Nil
        self:cLayoutFil := self:oLayoutFil:ToJson()
    EndIf

    If !Empty(self:cLayoutFil)
        MHP->( DbSetOrder(1) )  //MHP_FILIAL, MHP_CASSIN, MHP_CPROCE, MHP_TIPO
        If MHP->( DbSeek(xFilial("MHP") + PadR(self:cOrigem, TamSx3("MHP_CASSIN")[1]) + PadR(self:cProcesso, TamSx3("MHP_CPROCE")[1])) )
            RecLock("MHP", .F.)
                MHP->MHP_LAYFIL := self:cLayoutFil
            MHP->( MsUnLock() )
        EndIf
    EndIf


Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CheckTime
Metodo responsavel por validar data e hora 

@author  Danilo Rodrigues
@version 1.0
/*/
//-------------------------------------------------------------------
Method CheckTime(cTime) Class RmiBusChefObj
   
	Local cH := SubStr(cTime,1,2)
	Local cM := SubStr(cTime,4,2)
    Local cS := SubStr(cTime,7,2)
    Local nSec := Val(cS) + 1
    Local nMin := Val(cM)
    Local nHor := Val(cH)

    Default cTime := ""
	
    If (nSec > 59)
        cS := "00"
        cM := StrZero( nMin + 1, 2)
    Else    
        cS := StrZero( nSec, 2)
    EndIf

    If (nMin > 59)
		cM := "00"
        cH := StrZero( nHor + 1, 2)
	EndIf

    If (nHor > 23)
		cH := "00"
	EndIf 

    If Len(AllTrim(cTime)) == 8
		cTime := cH + ":" + cM + ":" + cS
	EndIf
   
Return cTime
