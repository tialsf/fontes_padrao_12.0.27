#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIBUSLIVEOBJ.CH"
#INCLUDE "XMLXFUN.CH"
#INCLUDE "TRYEXCEPTION.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiBusLiveObj
Classe respons�vel pela busca de dados no Live
    
/*/
//-------------------------------------------------------------------
Class RmiBusLiveObj From RmiBuscaObj

    Method New()            //Metodo construtor da Classe

    Method PreExecucao()    //Metodo com as regras para efetuar conex�o com o sistema de destino

    Method Busca()          //Metodo responsavel por buscar as informa��es no sistema de destino

    Method Venda()          //Metodo para carregar a publica��o de venda    
    
    Method BusCad()         //Metodo para carregar a publica��o de Cadastros    
    
    Method Confirma()       //Metodo que efetua a confirma��o do recebimento

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiBusLiveObj
    
    _Super:New("LIVE")

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} PreExecucao
Metodo com as regras para efetuar conex�o com o sistema de destino
Exemplo obter um token

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method PreExecucao() Class RmiBusLiveObj
    
    Local cError    := ""
    Local cWarning  := ""
    Local cBody     := ""
    Local cAux      := ""

    //Token do Live eh valido por 1 dia
    If !Empty(self:cToken)
        Return Nil
    EndIf

    //Objeto oEnvia utilizado na PreExecu��o
    If self:oPreExecucao == Nil
        self:oPreExecucao := TWsdlManager():New()
        self:oPreExecucao:nSoapVersion       := 0
        self:oPreExecucao:nConnectionTimeout := 300
        self:oPreExecucao:nTimeout           := 300

        //Faz o parse de uma URL
        If !self:oPreExecucao:ParseUrl( self:oConfAssin["url_token"] )
            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, {ProcName(), "XmlParser", self:oPreExecucao:cError} )    //"[#1] Problema ao efetuar o #2: #3"
        EndIf
    EndIf

    If self:lSucesso

        //Seta a opera��o que ser� executada
        If !self:oPreExecucao:SetOperation( self:oConfAssin["operacao"] )   //"ObterChaveAcessoLC_Integracao"

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, {ProcName(), "SetOperation", self:oPreExecucao:cError} )     //"[#1] Problema ao efetuar o #2: #3"
        Else

            cBody := "<?xml version='1.0' encoding='UTF-8' standalone='no' ?>"
            cBody += "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:liv='http://LiveConnector/'>"
            cBody +=   "<SOAP-ENV:Body>"
            cBody +=       "<ObterChaveAcessoLC_Integracao xmlns='http://LiveConnector/'>"
            cBody +=           "<CodigoSistemaSatelite>" + self:oConfAssin["sistemasatelite"] + "</CodigoSistemaSatelite>"
            cBody +=           "<Usuario>" + self:oConfAssin["usuario"] + "</Usuario>"
            cBody +=           "<Senha>" + self:oConfAssin["senha"] + "</Senha>"
            cBody +=       "</ObterChaveAcessoLC_Integracao>"
            cBody +=   "</SOAP-ENV:Body>"
            cBody += "</SOAP-ENV:Envelope>"        

            //Envia a mensagem a ser processada
            If self:oPreExecucao:SendSoapMsg(cBody)

                self:cRetorno := self:oPreExecucao:GetSoapResponse()

                //Pesquisa a tag com o retorno do token
                cAux := RmiXGetTag(self:cRetorno, "<ObterChaveAcessoLC_IntegracaoResult>", .T.)

                If !Empty(cAux)

                    self:oRetorno := XmlParser(cAux, "_", @cError, @cWarning)

                    //Carrega token
                    If self:oRetorno <> Nil
                        self:cToken := self:oRetorno:_ObterChaveAcessoLC_IntegracaoResult:Text

                        //Atualiza o token no assinante
                        self:SalvaConfig()
                    EndIf
                EndIf

                If Empty(cAux) .Or. self:oRetorno == Nil
                    self:lSucesso := .F.
                    self:cRetorno := I18n(STR0001, { ProcName(), "XmlParser", cError + CRLF + cWarning + CRLF + self:cRetorno} )    //"[#1] Problema ao efetuar o #2: #3"
                EndIf
            Else

                self:lSucesso := .F.
                self:cRetorno := I18n(STR0001, { ProcName(), "SendSoapMsg", self:oPreExecucao:cError} )     //"[#1] Problema ao efetuar o #2: #3"
            EndIf

        EndIf

    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Busca
Metodo responsavel por buscar as informa��es no Live

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Busca() Class RmiBusLiveObj

    Local cAux      := ""
    Local cError    := ""
    Local cWarning  := ""
    Local cDocCad   := ""

    //Objeto oBusca utilizado na Busca
    If self:oBusca == Nil
        self:oBusca := TWsdlManager():New()
        self:oBusca:nSoapVersion       := 0
        self:oBusca:nConnectionTimeout := 300
        self:oBusca:nTimeout           := 300
    EndIf

    //Faz o parse de uma URL
    If !self:oBusca:ParseUrl( self:oConfProce["url"] )

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0001, { ProcName(), "ParseUrl", self:oBusca:cError} )      //"[#1] Problema ao efetuar o #2: #3"
    Else

        //Seta a opera��o que ser� executada
        If !self:oBusca:SetOperation( self:oConfProce["operacao"] )     //RecuperarCupomFiscalLC_Integracao_Xml

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, { ProcName(), "SetOperation", self:oBusca:cError} )      //"[#1] Problema ao efetuar o #2: #3"
        Else

            //Envia a mensagem a ser processada
            If self:oBusca:SendSoapMsg(self:cBody)

                self:cRetorno := self:oBusca:GetSoapResponse()
                self:cRetorno := StrTran(self:cRetorno, "&lt;", "<")
                self:cRetorno := StrTran(self:cRetorno, "&gt;", ">")

                //Pesquisa a tag com o retorno das vendas
                cAux := RmiXGetTag(self:cRetorno, self:oConfProce["tagretorno"], .T.)   //"<LC_TicketCupomFiscal>" OU "<LC_TicketClienteSaida>"
                //O xml (cAux) do cliente � retonado sem LC_ClienteSaida gerando erro caso continue. 
                cDocCad:= RmiXGetTag(self:cRetorno,"<Documentos>", .T.)//caso nao retorne documentos n�o deve continuar
                If Empty(cAux)

                    self:lSucesso := .F.
                    self:cRetorno := I18n(STR0002, { ProcName(), self:oConfProce["tagretorno"], self:cRetorno} )    //"[#1] N�o foi localizada a TAG #2: #3"
                ElseIf Empty(cDocCad) .AND. !Empty(cAux) // caso n�o exista mais integra��o para recuperar
                    self:lSucesso := .F.
                    self:cRetorno := ""
                    LjGrvLog(" RMIBusLiveObj ","N�o existe dados a serem importados segue XML Retorno -> "+self:cRetorno)
                Else

                    self:cRetorno := cAux                    
                    self:oRetorno := XmlParser(self:cRetorno, "_", @cError, @cWarning)
                    self:cRetorno := ""

                    If self:oRetorno == Nil 

                        self:lSucesso := .F.
                        self:cRetorno := I18n(STR0001, { ProcName(), "XmlParser", cError +"\"+ cWarning +"\"+ cAux} )      //"[#1] Problema ao efetuar o #2: #3"
                    Else
                        //Centraliza os retorno permitidos
                        self:TrataRetorno()
                    EndIf

                EndIf
            Else
                self:lSucesso := .F.
                self:cRetorno := I18n(STR0001, { ProcName(), "SendSoapMsg", self:oBusca:cError} )   //"[#1] Problema ao efetuar o #2: #3"
                
                //Verifica se erro � por causa do token
                //Limpa token para pegar um novo
                If "CHAVE DE ACESSO INV" $ Upper(self:cRetorno)
                    self:cToken := ""
                    self:SalvaConfig()
                EndIf

            EndIf

        EndIf

    EndIf

 Return Nil

 //-------------------------------------------------------------------
/*/{Protheus.doc} Venda
Metodo para carregar a publica��o de venda

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Venda() Class RmiBusLiveObj

    Local nVenda        := 0
    Local nAte          := 1

    //Chave utilizada para efetuar a confirma��o do recebimento das vendas
    self:cConfirma := self:oRetorno:_LC_TicketCupomFiscal:_Numero:Text

    If self:cConfirma == Nil .Or. Empty(self:cConfirma)

        self:lContinuaBusca := .F.
        self:lSucesso       := .F.
        self:cRetorno 		:= I18n(STR0003, { ProcName(), self:cRetorno} )   //"[#1] N�o h� vendas a serem baixadas: #2"
    Else

        //Ativa para efetuar busca novamente
        self:lContinuaBusca := .T.
        If ValType(self:oRetorno:_LC_TicketCupomFiscal:_Documentos:_LC_CupomFiscal) == 'A'
            nAte:= Len(self:oRetorno:_LC_TicketCupomFiscal:_Documentos:_LC_CupomFiscal)
        endIf
        //Processa as vendas
        For nVenda:=1 To nAte

            self:lSucesso := .T.
            self:cRetorno := ""

            //Carrega informa��es da publica��o que ser� gerada
            self:oRegistro      := IIF(nAte>1,self:oRetorno:_LC_TicketCupomFiscal:_Documentos:_LC_CupomFiscal[nVenda],self:oRetorno:_LC_TicketCupomFiscal:_Documentos:_LC_CupomFiscal)  //Objeto que poder� ser utilizado para gerar o layout da publica��o. (MHP_LAYPUB)
            self:cChaveUnica    := self:oRegistro:_Numero:Text                                              //Chave �nica do registro publicado. (MHQ_CHVUNI)
            self:cEvento        := IIF(self:oRegistro:_Situacao:Text == "A", "1", "2")                      //Evento da publica��o 1=Upsert, 2=Delete (MHQ_EVENTO) - Live: A = Nota normal, C= Nota cancelada

            //Carrega a mensagem original
            SAVE self:oRegistro XMLSTRING self:cMsgOrigem
            self:cMsgOrigem := StrTran(self:cMsgOrigem, ">>", ">")  //Ao salvar o XML como string estava deixando '>>' em alguns momentos

            //Verifica se a venda j� foi publicada
            If self:AuxExistePub()
                Loop
            EndIf
            
            //Grava a publica��o
            self:Grava()

            //Limpa objeto da publica��o
            FwFreeObj(self:oRegistro)
            self:oRegistro  := Nil
            
        Next nVenda
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Confirma
Metodo que efetua a confirma��o do recebimento

@author  Danilo Rodrigues
@version 1.0
/*/
//-------------------------------------------------------------------
Method Confirma() Class RmiBusLiveObj

    Local cBody     := ""
    Local cAux      := ""
    Local cError    := ""
    Local cWarning  := ""

    If self:oEnvia == Nil
        self:oEnvia := TWsdlManager():New()
        self:oEnvia:nSoapVersion       := 0
        self:oEnvia:nConnectionTimeout := 300
        self:oEnvia:nTimeout           := 300
    EndIf

    //Faz o parse de uma URL
    If !self:oEnvia:ParseUrl( self:oConfProce["url"] )

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0001, { ProcName(), "ParseUrl", self:oEnvia:cError} )      //"[#1] Problema ao efetuar o #2: #3"
    Else

        //Seta a opera��o que ser� executada
        If !self:oEnvia:SetOperation("ConfirmarTicketLC_Integracao")

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, { ProcName(), "SetOperation", self:oEnvia:cError} )      //"[#1] Problema ao efetuar o #2: #3"
        Else

            cBody := "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:liv='http://LiveConnector/' xmlns:ren='http://schemas.datacontract.org/2004/07/Rentech.Framework.Data' xmlns:ren1='http://schemas.datacontract.org/2004/07/Rentech.PracticoLive.Connector.Objects' xmlns:arr='http://schemas.microsoft.com/2003/10/Serialization/Arrays'>"
            cBody +=	"<soapenv:Header/>"
            cBody += 	"<soapenv:Body>"
            cBody += 		"<liv:ConfirmarTicketLC_Integracao>"
            cBody += 			"<liv:confirmacaoTicket>"
            cBody += 				"<ren1:Chave>" + self:cToken + "</ren1:Chave>"
            cBody += 				"<ren1:CodigoSistemaSatelite>" + self:oConfAssin["sistemasatelite"] + "</ren1:CodigoSistemaSatelite>"
            cBody += 				"<ren1:NumeroTicket>" + self:cConfirma + "</ren1:NumeroTicket>"
            cBody += 				"<ren1:SituacaoTicketSaida>EnvioConfirmado</ren1:SituacaoTicketSaida>"
            cBody += 			"</liv:confirmacaoTicket>"
            cBody += 		"</liv:ConfirmarTicketLC_Integracao>"
            cBody += 	"</soapenv:Body>"
            cBody += "</soapenv:Envelope>"

            //Envia a mensagem a ser processada
            If self:oEnvia:SendSoapMsg(cBody)

                self:cRetorno := self:oEnvia:GetSoapResponse()
                cAux := RmiXGetTag(self:cRetorno, "<s:Body>", .T.)

                self:oRetorno := XmlParser(cAux, "_", @cError, @cWarning)

                If self:oRetorno <> Nil
                    self:oRetorno := XmlChildEx(self:oRetorno, "_S_BODY")
                EndIf                    

                If self:oRetorno == Nil .Or. XmlChildEx(self:oRetorno, "_CONFIRMARTICKETLC_INTEGRACAORESPONSE") == Nil

                    self:lSucesso := .F.
                    self:cRetorno := I18n(STR0004, { ProcName(), self:cConfirma, self:cRetorno} )   //"[#1] Problema ao efetuar a confirma��o do ticket #2: #3"
                EndIf
            Else

                self:lSucesso := .F.
                self:cRetorno := I18n(STR0001, { ProcName(), "SendSoapMsg", self:oEnvia:cError} )   //"[#1] Problema ao efetuar o #2: #3"
            EndIf
        EndIf
    EndIf

Return Nil
//-------------------------------------------------------------------
/*/{Protheus.doc} BusCad
Metodo para carregar a publica��o de Cadastros

@author  Everson S P Junior
@version 1.0
/*/
//-------------------------------------------------------------------
Method BusCad() Class RmiBusLiveObj

    Local nCad      := 0
    Local nTag      := 1
    Local aTags     := self:oLayoutPub:GetNames()   //Retorno o nome das tags do objeto publica
    Local aTagsSec  := {}
    Local aTagsTri  := {}
    Local aCmpsTri  := {}    
    Local aNoFilho  := {}                           //Array do No Filho com os itens que ser�o procesasdos
    Local xNoFilho  := Nil                          //Objeto do No Filho com os itens que ser�o procesasdos
    Local nAte      := 1
    Local cDocId    := ""
    Local cTagRet   := ""
    Local nItem     := 0
    Local nTagSec   := 0
    Local lContNo   := .T. //Variavel para controlar se processa ou nao o n� do XML/JSON
    

    
    //Chave utilizada para efetuar a confirma��o do recebimento de cadastro//LC_TicketClienteSaida
    cTagRet :="self:oRetorno:_"+self:oConfProce["tagretorno"]
    cTagRet :=StrTran(cTagRet, "<", "")
    cTagRet :=StrTran(cTagRet, ">", "")
    self:cConfirma := &(cTagRet+":_Numero:Text")

    If self:cConfirma == Nil .Or. Empty(self:cConfirma)

        self:lContinuaBusca := .F.
        self:lSucesso       := .F.
        self:cRetorno 		:= I18n(STR0003, { ProcName(), self:cRetorno} )   //"[#1] N�o h� vendas a serem baixadas: #2"
    Else

        //Ativa para efetuar busca novamente
        self:lContinuaBusca := .T.
        cDocId := cTagRet+":_Documentos:_"+self:oConfProce["documentoId"]
        If ValType(&(cDocId)) == 'A'
            nAte:= Len(&(cDocId))// cDocId = self:oRetorno:_LC_TicketClienteSaida:_Documentos:_LC_ClienteSaida
        endIf

        //Processa as vendas
        For nCad:=1 To nAte

            self:lSucesso := .T.
            self:cRetorno := ""

            //Carrega informa��es da publica��o que ser� gerada 
            self:oRegistro      := IIF(nAte>1,&cDocId[nCad],&cDocId)                      //Objeto que poder� ser utilizado para gerar o layout da publica��o. (MHP_LAYPUB)
            self:cChaveUnica    := &("self:oRegistro:_"+self:oConfProce["ChaveUni"]+":Text")                  //Chave �nica do registro publicado. (MHQ_CHVUNI)
            self:cEvento        := "1"//IIF(self:oRegistro:_Situacao:Text == "A", "1", "2") //Evento da publica��o 1=Upsert, 2=Delete (MHQ_EVENTO) - Live: A = Nota normal, C= Nota cancelada

            //Carrega a mensagem original
            SAVE self:oRegistro XMLSTRING self:cMsgOrigem
            self:cMsgOrigem := StrTran(self:cMsgOrigem, ">>", ">")  //Ao salvar o XML como string estava deixando '>>' em alguns momentos

            //Verifica se a venda j� foi publicada
            If self:AuxExistePub() 
                /*No XML do Live, o registro de administradora vem separado em varios itens da tag LC_BandeiraCartao,
                por conta disso, foi necessario permitir inserir na tabela MHQ o mesmo c�digo no campo MHQ_CHVUNI
                e realizar o tratamento antes de enviar para o execauto do LOJA070, caso o registro j� exista apenas altera,
                caso contr�rio inclui uma nova adm financeira.*/
                If AllTrim(self:cProcesso) == 'ADMINISTRADORA'
                    Sleep(1000)
                Else
                    Loop
                EndIf
            EndIf
            
            If self:oPublica == Nil
                self:oPublica := JsonObject():New()
            EndIf            
            For nTag := 1 To Len(aTags)

                //Processa tags de um N� filho
                If ValType(self:oLayoutPub:GetJsonObject(aTags[nTag])) == "A"
                    self:oPublica[aTags[nTag]] := {}
                    aTagsSec := self:oLayoutPub[ aTags[nTag] ][1]:GetNames()
                    //Pega o caminho para o No Filho

                    ASize(aNoFilho, 0)

                    //Condi��o que pode dar erro
                    TRY EXCEPTION
                        xNoFilho := &( self:oConfProce[ aTags[nTag] ] )
                        lContNo := .T.
                    //Se ocorreu erro
                    CATCH EXCEPTION USING oError
                        LjGrvLog(" RMIBusLiveObj ","Erro ao macro executar o n� - " + self:oConfProce[ aTags[nTag] ])      
                        lContNo := .F.                  
                    ENDTRY

                    If lContNo
                        //Tratamento no layout para quando tiver apenas 1 item
                        If ValType(xNoFilho) == "O"
                            Aadd(aNoFilho, xNoFilho)
                        Else
                            aNoFilho := xNoFilho
                        EndIf

                        If ValType(aNoFilho) == "A"

                            For nItem:=1 To Len( aNoFilho )

                                Aadd(self:oPublica[ aTags[nTag] ], JsonObject():New())

                                //Carrega tags do No Filho
                                For nTagSec:=1 To Len(aTagsSec)                     

                                    cLayoutPub := self:oLayoutPub[ aTags[nTag] ][1][ aTagsSec[nTagSec] ]
                                    
                                    //Tratamento no layout para quando tiver apenas 1 item
                                    If ValType(xNoFilho) == "O"
                                        cLayoutPub := StrTran( cLayoutPub, "[nItem]", "")
                                    EndIf
                                    
                                    self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := self:AuxTrataTag( aTagsSec[nTagSec], cLayoutPub, nItem)

                                Next nTagSec
                                //Ajusto o array para default 
                                aTagsSec := self:oLayoutPub[ aTags[nTag] ][1]:GetNames()                  
                            Next nItem
                        EndIf
                    EndIf

                else
                    self:oPublica[ aTags[nTag] ] := self:AuxTrataTag( aTags[nTag], self:oLayoutPub[ aTags[nTag] ] )
                EndIf    
            next 
            //Grava a publica��o
            self:Grava()

            //Limpa objeto da publica��o
            FwFreeObj(xNoFilho)
            FwFreeObj(self:oRegistro)
            FwFreeObj(self:oPublica)
            xNoFilho        := Nil
            self:oRegistro  := Nil
            self:oPublica   := Nil
            
            If ValType(aNoFilho) == "A"
                Asize(aNoFilho, 0)
            EndIf
            Asize(aTagsSec, 0)
            Asize(aTagsTri, 0)
            Asize(aCmpsTri, 0)

        Next nCad
    EndIf

    Asize(aTags, 0)

Return Nil
