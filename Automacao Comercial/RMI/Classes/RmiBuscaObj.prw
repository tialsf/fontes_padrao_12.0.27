#INCLUDE "PROTHEUS.CH"
#INCLUDE "TRYEXCEPTION.CH"
#INCLUDE "RMIBUSCAOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiBuscaObj
Classe respons�vel pela busca de dados nos Assinantes
    
/*/
//-------------------------------------------------------------------
Class RmiBuscaObj From RmiEnviaObj

    Data cLayoutPub     As Character    //Layout referente a publica��o que ser� gerada quando for um processo de busca(MHP_LAYPUB)
    Data oLayoutPub     As Objetc       //Objeto JsonObject com layout de publica��o utilizado no processo de busca para gerar a publicada

    Data oBusca         As Objetc       //Objeto utilizado para efetuar a busca no Assinante ele conter� o retorno do Assinante

    Data oRegistro      As Objetc       //Objeto que conter� o retorno de 1 registro. (Exemplo: 1 venda)

    Data cConfirma      As Character    //C�digo de confirma��o utilizado para efetuar a confirma��o de busca

    Data lContinuaBusca As Logical      //Faz o controle do processamento continuo

    Data cMsgOrigem     As Character    //Mensagem sem nenhum tratamento retornada pelo Assinante onde foi feita a busca

    Method New(cAssinante)              //Metodo construtor da Classe

    Method Processa()                   //Metodo que ira controlar o processamento das buscas

    Method Busca()                      //Metodo responsavel por buscar as informa��es no Assinante

    Method Consulta()                   //Metodo com a consulta que ser� executada para saber quais os processos ser�o recebidos

    Method SetaProcesso(cProcesso)      //Metodo responsavel por carregar as informa��es referente ao processo que ser� recebido
    
    Method TrataRetorno()               //Metodo que centraliza os retorno permitidos

    Method Venda()                      //Metodo para carregar a publica��o de venda
    
    Method Grava()                      //Metodo que efetua a grava��o da publica��o

    Method Confirma()                   //Metodo que efetua a confirma��o do recebimento

    Method Destroi()                    //Metodo que ira tirar os objetos criados da memoria    

    //Metodos auxiliares para tratamento interno da classe
    Method AuxTrataTag(cTag, xConteudo, nItem)  //Metodo para efetuar o tratamento das Tags que ser�o gravadas na publica��o
    Method AuxExistePub()                       //Metodo que ira verificar se uma publica��o j� existe

    //Metodos responsaveis pelo reprocessamento da publica��o
    Method DelPublic()
    Method DelVenda(cUuid)
    Method DelDePara(cChvUni)
    Method DelLog(cUuid)
    Method DelDistrib(cUuid)

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New(cAssinante) Class RmiBuscaObj
    
    _Super:New(cAssinante)

    self:cOrigem        := self:cAssinante
    self:cTipo          := "2"   //2=Busca        
    self:cLayoutPub     := ""
    self:oLayoutPub     := Nil

    self:oBusca         := Nil
    
    self:oRegistro      := Nil

    self:cConfirma      := ""

    self:lContinuaBusca := .T.

    self:cMsgOrigem     := ""

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Processa
Metodo que ira controlar o processamento das buscas

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Processa() Class RmiBuscaObj

    Local nFil := 0
    
    //Carrega os processo que devem efetuar a busca
    self:Consulta()

    If !(self:cAliasQuery)->( Eof() )

        While !(self:cAliasQuery)->( Eof() )
            
            self:lSucesso := .T.
            self:cRetorno := ""

            //Atualiza informa��es do processo que ser� buscado
            self:SetaProcesso( (self:cAliasQuery)->MHP_CPROCE )

            //Gera log de erro
            If !self:lSucesso
                self:getRetorno()
            Else
                //Carrega o body de envio, solicitado pelo assinante para poder gerar o retorno
                For nFil:=1 To Len(self:aArrayFil)

                    //Tratamento para processamento por filial
                    self:nFil           := nFil 
                    self:lContinuaBusca := .T.

                    If self:lSucesso
                        self:PreExecucao() 
                    EndIf

                    If self:lSucesso
                        self:CarregaBody()
                    EndIf

                    If self:lSucesso  
                        //Controla a execu��o das buscas, alguns sistemas retornam paginado - Ex: Live 35 vendas por retorno
                        While self:lContinuaBusca

                            self:lContinuaBusca := .F.

                            //Efetua a busca no assinante
                            If self:lSucesso
                                self:Busca()
                            EndIf

                            //Efetua a confirma��o do recebimento
                            If self:lSucesso
                                self:Confirma()
                            EndIf

                            //Gera log de erro
                            If !self:lSucesso
                                self:getRetorno()
                            EndIf
                            
                            FwFreeObj(self:oRetorno)
                            self:lSucesso := .T.
                            self:cTipoRet := ""
                            self:cRetorno := ""
                            self:oRetorno := Nil
                        EndDo
                    EndIf
                Next nFil

            EndIf

            (self:cAliasQuery)->( DbSkip() )
        EndDo

        //Atualiza a configura��o do assinante, com o novo token gerado
        If self:lSucesso
            self:SalvaConfig()
        EndIf
    EndIf

    (self:cAliasQuery)->( DbCloseArea() )

    self:Destroi()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Consulta
Metodo com a consulta que ser� executada para saber quais os processos ser�o buscados

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Consulta() Class RmiBuscaObj

    self:cAliasQuery := GetNextAlias()
    self:cQuery      := " SELECT MHP_CPROCE"
    self:cQuery      += " FROM " + RetSqlName("MHP")
    self:cQuery      += " WHERE D_E_L_E_T_ = ' '"
    self:cQuery      +=     " AND MHP_CASSIN = '" + self:cAssinante + "'"
    self:cQuery      +=     " AND MHP_ATIVO = '1'"  //1=Sim
    self:cQuery      +=     " AND MHP_TIPO = '2'"   //2=Busca
    self:cQuery      += " ORDER BY MHP_CPROCE"
    
    DbUseArea(.T., "TOPCONN", TcGenQry( , , self:cQuery), self:cAliasQuery, .T., .F.)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} SetaProcesso
Metodo responsavel por carregar as informa��es referente ao processo que ser� buscado

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method SetaProcesso(cProcesso) Class RmiBuscaObj

    //Limpa configura��es anteriores
    self:cLayoutPub := ""

    //Chama metodo da classe pai para buscar informa��es comuns
    _Super:SetaProcesso(cProcesso)

    If self:lSucesso

        self:cLayoutPub := AllTrim(MHP->MHP_LAYPUB)
        
        //Carrega layout que ser� enviado
        If !Empty(self:cLayoutPub) .And. SubStr(self:cLayoutPub, 1, 1) == "{"

            If self:oLayoutPub == Nil
                self:oLayoutPub := JsonObject():New()
            EndIf
            self:oLayoutPub:FromJson(self:cLayoutPub)
        Else

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0003, {GetSX3Cache("MHP_LAYPUB", "X3_TITULO"), self:cAssinante})   //"#1 inv�lido, verifique o cadastro do Assinante #2"
        EndIf

    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Busca
Metodo responsavel por buscar as informa��es no Assinante

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Busca() Class RmiBuscaObj

    //Inteligencia poder� ser feita na classe filha - default em Rest com Json (com Post)
    If self:lSucesso

        If self:oBusca == Nil
            self:oBusca := FWRest():New("")
        EndIf

        self:oBusca:SetPath( self:oConfProce["url"] )

        self:oBusca:SetPostParams(self:cBody)

        If self:oBusca:Post( {"Content-Type:application/json"} )

            self:cRetorno := self:oBusca:GetResult()

            If self:oRetorno == Nil
                self:oRetorno := JsonObject():New()
            EndIf
            self:oRetorno:FromJson(self:cRetorno)
            self:cRetorno := ""

            //Centraliza os retorno permitidos
            self:TrataRetorno()
        Else

            self:lSucesso := .F.
            self:cRetorno := self:oBusca:GetLastError() + " - [" + self:oConfProce["url"] + "]" + CRLF
            //self:cRetorno += self:oBusca:GetResult()
        EndIf
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} TrataRetorno
Metodo que centraliza os retorno permitidos

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method TrataRetorno() Class RmiBuscaObj

    Do Case 

        Case AllTrim(self:cProcesso) == "VENDA"
            self:Venda()
        OTherWise
            If AllTrim(self:cOrigem) == "LIVE"
                self:BusCad()
            else
                self:lSucesso := .F.
                self:cRetorno := I18n(STR0001, {self:cProcesso})    //"Processo #1 sem tratamento para busca implementado"
            endIf    
    End Case

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Venda
Metodo para carregar a publica��o de venda com implementa��es genericas
para todos os assinantes.

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Venda() Class RmiBuscaObj

    Local nItem      := 0
    Local aTags      := self:oLayoutPub:GetNames()   //Retorno o nome das tags do objeto publica
    Local nTag       := 0
    Local aTagsSec   := {}
    Local nTagSec    := 0
    Local aNoFilho   := {}                           //Array do No Filho com os itens que ser�o procesasdos
    Local xNoFilho   := Nil                          //Objeto do No Filho com os itens que ser�o procesasdos
    Local cLayoutPub := ""                           //Propriedade que ser� macro executada para pegar o contedo retornado pelo assinante

    //Cria objeto que conter� a publica��o
    If self:oPublica == Nil
        self:oPublica := JsonObject():New()
    EndIf            

    //Percorre tags do layout de publica��o
    For nTag := 1 To Len(aTags)

        //Processa tags de um N� filho
        If aTags[nTag] $ "SL2|SL4"
            
            self:oPublica[ aTags[nTag] ] := {}

            //Pega o nome das tags filhas
            aTagsSec := self:oLayoutPub[ aTags[nTag] ][1]:GetNames()

            //Pega o caminho para o No Filho
            aNoFilho := {}
            ASize(aNoFilho, 0)
            xNoFilho := &( self:oConfProce[ aTags[nTag] ] )

            //Tratamento no layout para quando tiver apenas 1 item
            If ValType(xNoFilho) == "O"
                Aadd(aNoFilho, xNoFilho)
            Else
                aNoFilho := xNoFilho
            EndIf

            If ValType(aNoFilho) == "A"

                For nItem:=1 To Len( aNoFilho )

                    Aadd(self:oPublica[ aTags[nTag] ], JsonObject():New())

                    //Carrega tags do No Filho
                    For nTagSec:=1 To Len(aTagsSec)

                        cLayoutPub := self:oLayoutPub[ aTags[nTag] ][1][ aTagsSec[nTagSec] ]
                        
                        //Tratamento no layout para quando tiver apenas 1 item
                        If ValType(xNoFilho) == "O"
                            cLayoutPub := StrTran( cLayoutPub, "[nItem]", "")
                        EndIf

                        self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := self:AuxTrataTag( aTagsSec[nTagSec], cLayoutPub, nItem)

                    Next nTagSec
                Next nItem

            EndIf

        //Processa tags principais
        Else

            self:oPublica[ aTags[nTag] ] := self:AuxTrataTag( aTags[nTag], self:oLayoutPub[ aTags[nTag] ] )
        Endif

    Next nTag

    //Grava a publica��o
    self:Grava()

    //Limpa objeto da publica��o
    FwFreeObj(xNoFilho)
    FwFreeObj(self:oRegistro)
    FwFreeObj(self:oPublica)
    xNoFilho        := Nil
    self:oRegistro  := Nil
    self:oPublica   := Nil
    
    If ValType(aNoFilho) == "A"
        Asize(aNoFilho, 0)
    EndIf
    Asize(aTagsSec, 0)
    Asize(aTags   , 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Grava
Grava a publica��o recebida

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Grava() Class RmiBuscaObj

    Local cChave  := xFilial("MHQ") + PadR(self:cOrigem, TamSx3("MHQ_ORIGEM")[1]) + PadR(self:cProcesso, TamSx3("MHQ_CPROCE")[1]) + PadR(self:cChaveUnica, TamSx3("MHQ_CHVUNI")[1])
    Local cStatus := IIF(AllTrim(Self:cProcesso) = "VENDA","0","1")              //0=Fila;1=A Processar;2=Processada;3=Erro

    If !(AllTrim(Self:cProcesso) = "VENDA")
        //Carrega json com o publica��o que ser� gerada
        self:cPublica := self:oPublica:ToJson()
    EndIf

    Begin Transaction

        //Em caso de erro muda status
        If !self:lSucesso .AND. !(AllTrim(Self:cProcesso) = "VENDA")
            cStatus := "3" //1=A Processar;2=Processada;3=Erro           
        EndIf

        //Se for exclus�o mas ainda n�o existi uma publica��o de inclus�o, gera a inclus�o para depois gerar a exclus�o
        MHQ->( DbSetOrder(1) )  //MHQ_FILIAL + MHQ_ORIGEM + MHQ_CPROCE + MHQ_CHVUNI + MHQ_EVENTO + DTOS(MHQ_DATGER) + MHQ_HORGER
        If self:cEvento == "2" .And. !MHQ->( DbSeek(cChave) )
            RecLock("MHQ", .T.)

                MHQ->MHQ_FILIAL := xFilial("MHQ")
                MHQ->MHQ_ORIGEM := self:cOrigem
                MHQ->MHQ_CPROCE := self:cProcesso
                MHQ->MHQ_EVENTO := "1"              //1=Atualiza��o;2=Exclus�o
                MHQ->MHQ_CHVUNI := self:cChaveUnica
                MHQ->MHQ_MENSAG := self:cPublica
                MHQ->MHQ_DATGER := Date()
                MHQ->MHQ_HORGER := Time()
                MHQ->MHQ_STATUS := cStatus          //0=Fila;1=A Processar;2=Processada;3=Erro
                MHQ->MHQ_UUID   := FwUUID("MHQ")    //Gera chave unica
                MHQ->MHQ_MSGORI := self:cMsgOrigem

            MHQ->( MsUnLock() )
        EndIf

        RecLock("MHQ", .T.)

            MHQ->MHQ_FILIAL := xFilial("MHQ")
            MHQ->MHQ_ORIGEM := self:cOrigem
            MHQ->MHQ_CPROCE := self:cProcesso
            MHQ->MHQ_EVENTO := self:cEvento     //1=Atualiza��o;2=Exclus�o
            MHQ->MHQ_CHVUNI := self:cChaveUnica
            MHQ->MHQ_MENSAG := self:cPublica
            MHQ->MHQ_DATGER := Date()
            MHQ->MHQ_HORGER := Time()
            MHQ->MHQ_STATUS := cStatus          //0=Fila;1=A Processar;2=Processada;3=Erro
            If cStatus == '3'
                MHQ->MHQ_DATPRO := Date()
                MHQ->MHQ_HORPRO := Time()
            EndIf 
            MHQ->MHQ_UUID   := FwUUID("MHQ")    //Gera chave unica  
            MHQ->MHQ_MSGORI := self:cMsgOrigem      

        MHQ->( MsUnLock() )

        //Em caso de erro grava Log
        If !self:lSucesso .AND. !(AllTrim(Self:cProcesso) = "VENDA")
            RMIGRVLOG(  "IR"          , "MHQ"   , MHQ->(Recno())    , self:cTipoRet ,;
                        self:cRetorno ,         ,                   ,               ,;
                        .F.           , 1       , cChave,MHQ->MHQ_CPROCE, MHQ->MHQ_ORIGEM   )
        EndIf

    End Transaction

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Confirma
Metodo que efetua a confirma��o do recebimento

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Confirma() Class RmiBuscaObj
    //Implementa��o esta na classe filha
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Destroi
Metodo que ira tirar os objetos criados da memoria

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Destroi() Class RmiBuscaObj
    
    FwFreeObj(self:oLayoutPub)
    FwFreeObj(self:oBusca)
    FwFreeObj(self:oRegistro)

    _Super:Destroi()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} AuxTrataTag
Metodo para efetuar o tratamento das Tags que ser�o gravadas na publica��o

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method AuxTrataTag(cTag, xConteudo, nItem) Class RmiBuscaObj

    Local xTag      := xConteudo
    Local cTipo     := GetSX3Cache(cTag, "X3_TIPO")
    Local oError    := Nil
    Local cMensagem := ""

    Default nItem   := 0    //Variavel que pode ser utilizadada dentro do xConteudo

    //Retorna resultado de macro execu��o
    If ValType(xConteudo) == "C" .And. SubStr(xConteudo, 1, 1) == "&"

		//Macro executa
		TRY EXCEPTION
			//Condi��o que pode dar erro
			xTag := &( AllTrim( SubStr(xConteudo, 2) ) )

        //Se ocorreu erro
		CATCH EXCEPTION USING oError
            xTag      := CriaVar(cTag, .T.)
            cMensagem := I18n(STR0002, {cTag, AllTrim(self:cProcesso), AllTrim(self:cAssinante)})   //"N�o foi possivel macro executar a tag #1 do layout de retorno, ser� utilizado o valor default. Processo #2 e Assinante #3."
            LjxjMsgErr(cMensagem, /*cSolucao*/)

		ENDTRY
    EndIf

    //Formata com tipo esperado pelo Protheus
    Do Case

        Case cTipo == "C"
            xTag := NoAcento(xTag)
            xTag := DeCodeUtf8(xTag)

        Case cTipo == "D"
            xTag := StrTran(xTag, "-", "")
            xTag := SubStr(xTag, 1, 8)

        Case cTipo == "N" .And. ValType(xTag) == "C"
            xTag := Val(xTag)

    EndCase

Return xTag

//-------------------------------------------------------------------
/*/{Protheus.doc} ExistePub 
Metodo que ira verificar se uma publica��o j� existe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method AuxExistePub() Class RmiBuscaObj

    Local lExiste := .F. 
    Local cChave  := xFilial("MHQ") + PadR(self:cOrigem, TamSx3("MHQ_ORIGEM")[1]) + PadR(self:cProcesso, TamSx3("MHQ_CPROCE")[1]) + PadR(self:cChaveUnica, TamSx3("MHQ_CHVUNI")[1])

    MHQ->( DbSetOrder(1) )  //MHQ_FILIAL + MHQ_ORIGEM + MHQ_CPROCE + MHQ_CHVUNI + MHQ_EVENTO + DTOS(MHQ_DATGER) + MHQ_HORGER
    lExiste := MHQ->( DbSeek(cChave + self:cEvento) )

    /*Caso o registro exista na MHQ
    ent�o verifica se eh para reprocessar*/
    If lExiste
        lExiste := Self:DelPublic()
    EndIf

Return lExiste

//-------------------------------------------------------------------
/*/{Protheus.doc} DelPublic 
Esse metodo tem a responsabilidade em verificar se o registro da MHQ
esta marcado para reprocessamento e excluira todo o processo relacionado
a publica��o

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method DelPublic() Class RmiBuscaObj

Local cUuid := MHQ->MHQ_UUID
Local lRet  := .T.

If MHQ->MHQ_STATUS == "4" .AND. AllTrim(MHQ->MHQ_CPROCE) == "VENDA"
    LjGrvLog(" RmiBuscaObj ", "Iniciando o reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid))
    If Self:DelVenda(cUuid)         
        //Realiza a exclus�o do De/Para da venda
        Self:DelDePara(MHQ->MHQ_CHVUNI)

        //Realiza a exclus�o dos logs
        Self:DelLog(cUuid)

        //Realiza a exclus�o da distribui��o
        Self:DelDistrib(cUuid)

        //Exclui a publica��o
        RecLock("MHQ",.F.)
        MHQ->( DbDelete() )
        MHQ->( MsUnLock() )

        lRet := .F.

    EndIf
    LjGrvLog(" RmiBuscaObj ", "Fim do reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid))
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DelVenda 
Esse metodo tem o objetivo de localizar a venda que sera reprocessada
para antes, realizar a exclus�o da SL1, SL2 e SL4.

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method DelVenda(cUuid) Class RmiBuscaObj

Local lRet      := .T. //Variavel de retorno
Local cQuery    := "" //Armazena a query a ser executada
Local cAlias    := GetNextAlias() //Pega o proximo alias

cQuery := "SELECT R_E_C_N_O_ REC "
cQuery += "  FROM " + RetSqlName("SL1")
cQuery += " WHERE L1_UMOV = '" + cUuid + "'"
cQuery += "   AND D_E_L_E_T_ <> '*'"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

If !(cAlias)->( Eof() )
    SL1->(dbGoto((cAlias)->REC))
    If SL1->L1_SITUA $ "IR|ER|RP"

        LjGrvLog(" RmiBuscaObj ", "Reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid) + ". Excluindo a venda L1_FILIAL = " + AllTrim(SL1->L1_FILIAL) + " L1_NUM = " + AllTrim(SL1->L1_NUM)) 

        /*Trava a SL1 para n�o acontecer do usuario alterar o L1_SITUA
        pelo APSDU*/
        RecLock("SL1",.F.)

        //Inicia a exclusao da SL2
        dbSelectArea("SL2")
        SL2->(dbSetOrder(1)) //L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
        If SL2->(dbSeek(SL1->L1_FILIAL + SL1->L1_NUM))
            While SL2->(!Eof()) .AND. SL2->L2_FILIAL + SL2->L2_NUM == SL1->L1_FILIAL + SL1->L1_NUM
                RecLock("SL2",.F.)
                SL2->( DbDelete() )
                SL2->( MsUnLock() )
                SL2->( DbSkip() )
            EndDo
        EndIf

        //Inicia a exclusao da SL4
        dbSelectArea("SL4")
        SL4->(dbSetOrder(1)) //L4_FILIAL+L4_NUM+L4_ORIGEM
        If SL4->(dbSeek(SL1->L1_FILIAL + SL1->L1_NUM))
            While SL4->(!Eof()) .AND. SL4->L4_FILIAL + SL4->L4_NUM == SL1->L1_FILIAL + SL1->L1_NUM
                RecLock("SL4",.F.)
                SL4->( DbDelete() )
                SL4->( MsUnLock() )
                SL4->( DbSkip() )
            EndDo
        EndIf

        //Ap�s excluir a SL2 e SL4, terminar de excluir a SL1
        SL1->( DbDelete() )
        SL1->( MsUnLock() )       
        LjGrvLog(" RmiBuscaObj ", "Reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid) + ". Venda excluida com sucesso. L1_FILIAL = " + AllTrim(SL1->L1_FILIAL) + "L1_NUM = " + AllTrim(SL1->L1_NUM))  
    Else
        LjGrvLog(" RmiBuscaObj ", STR0004 + AllTrim(cUuid) + STR0005 + AllTrim(SL1->L1_SITUA) + STR0006) //"N�o � possivel reprocessar a publica��o (MHQ_UUID) " # " pois a venda esta com L1_SITUA igual a " # ". O reprocessamento s� � aceito para vendas com L1_SITUA igual a IR ou ER."
        lRet := .F.        
    EndIf
EndIf

(cAlias)->( DbCloseArea() )

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} DelDePara 
Metodo responsavel em realizar a exclus�o dos registros de de/para
antes de efetivar o reprocessamento da venda

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method DelDePara(cChvUni) Class RmiBuscaObj

Local cQuery    := "" //Armazena a query a ser executada
Local cAlias    := GetNextAlias() //Pega o proximo alias

cQuery := "SELECT R_E_C_N_O_ REC"
cQuery += "  FROM " + RetSqlName("MHM")
cQuery += " WHERE MHM_VLORIG = '" + cChvUni + "'"
cQuery += "   AND D_E_L_E_T_ <> '*'"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

LjGrvLog(" RmiBuscaObj ", "Reprocessamento da publica��o (MHQ_UUID) " + AllTrim(MHQ->MHQ_UUID) + ". Realizando a exclus�o dos de/para tabela (MHM)")

While !(cAlias)->( Eof() )
    MHM->(dbGoto((cAlias)->REC))
    RecLock("MHM",.F.)
    MHM->( DbDelete() )
    MHM->( MsUnLock() )    
    (cAlias)->( DbSkip() )
EndDo

(cAlias)->( DbCloseArea() )

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} DelLog 
Metodo responsavel em realizar a exclus�o dos registros de log da venda
que sera reprocessada

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method DelLog(cUuid) Class RmiBuscaObj

Local cQuery    := "" //Armazena a query a ser executada
Local cAlias    := GetNextAlias() //Pega o proximo alias

cQuery := "SELECT R_E_C_N_O_ REC"
cQuery += "  FROM " + RetSqlName("MHL")
cQuery += " WHERE MHL_CHAVE = '" + cUuid + "'"
cQuery += "   AND D_E_L_E_T_ <> '*'"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

LjGrvLog(" RmiBuscaObj ", "Reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid) + ". Realizando a exclus�o dos logs tabela (MHL)")

While !(cAlias)->( Eof() )
    MHL->(dbGoto((cAlias)->REC))
    RecLock("MHL",.F.)
    MHL->( DbDelete() )
    MHL->( MsUnLock() )    
    (cAlias)->( DbSkip() )
EndDo

(cAlias)->( DbCloseArea() )

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} DelDistrib 
Metodo responsavel em realizar a exclus�o dos registros de distribui��o
da venda antes de reprocessa-la 

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method DelDistrib(cUuid) Class RmiBuscaObj

Local cQuery    := "" //Armazena a query a ser executada
Local cAlias    := GetNextAlias() //Pega o proximo alias

cQuery := "SELECT R_E_C_N_O_ REC"
cQuery += "  FROM " + RetSqlName("MHR")
cQuery += " WHERE MHR_UIDMHQ = '" + cUuid + "'"
cQuery += "   AND D_E_L_E_T_ <> '*'"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

LjGrvLog(" RmiBuscaObj ", "Reprocessamento da publica��o (MHQ_UUID) " + AllTrim(cUuid) + ". Realizando a exclus�o da distribui��o tabela (MHR)")

While !(cAlias)->( Eof() )
    MHR->(dbGoto((cAlias)->REC))
    RecLock("MHR",.F.)
    MHR->( DbDelete() )
    MHR->( MsUnLock() )    
    (cAlias)->( DbSkip() )
EndDo

(cAlias)->( DbCloseArea() )

Return .T.
