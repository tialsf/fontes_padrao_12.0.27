#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIENVLIVEOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiEnvLiveObj
Classe respons�vel pelo envio de dados ao Live

/*/
//-------------------------------------------------------------------
Class RmiEnvLiveObj From RmiEnviaObj

    Method New()            //Metodo construtor da Classe

    Method PreExecucao()    //Metodo com as regras para efetuar conex�o com o sistema de destino

    Method Envia()          //Metodo responsavel por enviar a mensagens ao Live

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiEnvLiveObj
    
    _Super:New("LIVE")

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} PreExecucao
Metodo com as regras para efetuar conex�o com o sistema de destino
Exemplo obter um token

Mesma logica do metodo do fonte RmiBusLiveObj.prw

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method PreExecucao() Class RmiEnvLiveObj

    Local cError    := ""
    Local cWarning  := ""
    Local cBody     := ""
    Local cAux      := ""

    //Token do Live eh valido por 1 dia
    If !Empty(self:cToken)
        Return Nil
    EndIf

    //Objeto oEnvia utilizado na PreExecu��o
    If self:oPreExecucao == Nil
        self:oPreExecucao := TWsdlManager():New()
        self:oPreExecucao:nSoapVersion       := 0
        self:oPreExecucao:nConnectionTimeout := 300
        self:oPreExecucao:nTimeout           := 300

        //Faz o parse de uma URL
        If !self:oPreExecucao:ParseUrl( self:oConfAssin["url_token"] )
            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, {ProcName(), "XmlParser", self:oPreExecucao:cError} )    //"[#1] Problema ao efetuar o #2: #3"
        EndIf
    EndIf

    If self:lSucesso

        //Seta a opera��o que ser� executada
        If !self:oPreExecucao:SetOperation( self:oConfAssin["operacao"] )   //"ObterChaveAcessoLC_Integracao"

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, {ProcName(), "SetOperation", self:oPreExecucao:cError} )     //"[#1] Problema ao efetuar o #2: #3"
        Else

            cBody := "<?xml version='1.0' encoding='UTF-8' standalone='no' ?>"
            cBody += "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:liv='http://LiveConnector/'>"
            cBody +=   "<SOAP-ENV:Body>"
            cBody +=       "<ObterChaveAcessoLC_Integracao xmlns='http://LiveConnector/'>"
            cBody +=           "<CodigoSistemaSatelite>" + self:oConfAssin["sistemasatelite"] + "</CodigoSistemaSatelite>"
            cBody +=           "<Usuario>" + self:oConfAssin["usuario"] + "</Usuario>"
            cBody +=           "<Senha>" + self:oConfAssin["senha"] + "</Senha>"
            cBody +=       "</ObterChaveAcessoLC_Integracao>"
            cBody +=   "</SOAP-ENV:Body>"
            cBody += "</SOAP-ENV:Envelope>"        

            //Envia a mensagem a ser processada
            If self:oPreExecucao:SendSoapMsg(cBody)

                self:cRetorno := self:oPreExecucao:GetSoapResponse()

                //Pesquisa a tag com o retorno do token
                cAux := RmiXGetTag(self:cRetorno, "<ObterChaveAcessoLC_IntegracaoResult>", .T.)

                If !Empty(cAux)

                    self:oRetorno := XmlParser(cAux, "_", @cError, @cWarning)

                    //Carrega token
                    If self:oRetorno <> Nil
                        self:cToken := self:oRetorno:_ObterChaveAcessoLC_IntegracaoResult:Text

                        //Atualiza o token no assinante
                        self:SalvaConfig()
                    EndIf
                EndIf

                If Empty(cAux) .Or. self:oRetorno == Nil
                    self:lSucesso := .F.
                    self:cRetorno := I18n(STR0001, { ProcName(), "XmlParser", cError + CRLF + cWarning + CRLF + self:cRetorno} )    //"[#1] Problema ao efetuar o #2: #3"
                EndIf
            Else

                self:lSucesso := .F.
                self:cRetorno := I18n(STR0001, { ProcName(), "SendSoapMsg", self:oPreExecucao:cError} )     //"[#1] Problema ao efetuar o #2: #3"
            EndIf

        EndIf

    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Envia
Metodo responsavel por enviar a mensagens ao Live

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Envia() Class RmiEnvLiveObj

    Local cAux     := ""
    Local lReenvia := .T.
    Local nVez     := 0

    //Objeto oEnvia utilizado no Envio
    If self:oEnvia == Nil
        self:oEnvia := TWsdlManager():New()
        self:oEnvia:nSoapVersion       := 0
        self:oEnvia:nConnectionTimeout := 300
        self:oEnvia:nTimeout           := 300
    EndIf

    //Faz o parse de uma URL
    If !self:oEnvia:ParseUrl( self:oConfProce["url"] )

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0001, { ProcName(), "ParseUrl", self:oEnvia:cError} )      //"[#1] Problema ao efetuar o #2: #3"
    Else

        //Seta a opera��o que ser� executada
        If !self:oEnvia:SetOperation( self:oConfProce["operacao"] )     //ManterProdutoLC_Integracao_Xml

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0001, { ProcName(), "SetOperation", self:oEnvia:cError} )      //"[#1] Problema ao efetuar o #2: #3"
        Else

            While lReenvia

                //Controle de reenvio com gera��o de novo token
                lReenvia := .F.
                nVez++

                //Envia a mensagem a ser processada
                If self:oEnvia:SendSoapMsg(self:cBody)

                    self:cRetorno := self:oEnvia:GetSoapResponse()
                    self:cRetorno := StrTran(self:cRetorno, "&lt;", "<")
                    self:cRetorno := StrTran(self:cRetorno, "&gt;", ">")

                    //Pesquisa a tag com o retorno das vendas
                    cAux := RmiXGetTag(self:cRetorno, self:oConfProce["tagretorno"], .T.)   //"<LC_Retorno>"

                    If "PROCESSAMENTO REALIZADO COM SUCESSO" $ Upper(cAux)

                        //Por enquanto o processamento do Live � assincrono, ent�o n�o carrega a chave externa
                        self:lSucesso := .T.
                        self:cRetorno := cAux
                        //self:cChaveExterna := ""

                    ElseIf "CHAVE DE ACESSO INV" $ Upper(cAux)

                        If nVez > 1

                            self:lSucesso := .F.
                            self:cRetorno := I18n(STR0002, {ProcName(), self:cRetorno} )    //"[#1] Reenvio j� foi feito, mas erro persiste: #2"
                        Else
                            
                            //Gera novo token para tentar o reenvio
                            lReenvia    := .T.
                            self:cToken := ""
                            
                            self:PreExecucao() 

                            self:CarregaBody()
                        EndIf
                    Else

                        self:lSucesso := .F.
                        self:cRetorno := I18n(STR0003, { ProcName(), self:oConfProce["operacao"], self:cRetorno} )  //"[#1] Retorno desconhecido, na opera��o #2: #3"
                    EndIf
                    
                Else

                    self:lSucesso := .F.
                    self:cRetorno := I18n(STR0001, { ProcName(), "SendSoapMsg", self:oEnvia:cError} )   //"[#1] Problema ao efetuar o #2: #3"
                EndIf
            EndDo
        EndIf
    EndIf

Return Nil