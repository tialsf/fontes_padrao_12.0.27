#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIENVPROTHEUSOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiEnvProtheusObj
Classe que processa as distribui��es do Protheus
/*/
//-------------------------------------------------------------------
Class RmiEnvProtheusObj From RmiEnviaObj

    Data aExecAuto  As Array    //Array com o conteudo utilizado na manipula��o de dados MsExecAuto

    Method New()                //Metodo construtor da Classe

    Method PreExecucao()        //Metodo com as regras para efetuar conex�o com o sistema de destino

    Method CarregaBody()        //Metodo que carrega o corpo da mensagem que ser� enviada

    Method Envia()              //Metodo responsavel por enviar a mensagens ao sistema de destino

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiEnvProtheusObj
    
    _Super:New("PROTHEUS")

    self:aExecAuto := {}

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} PreExecucao
Metodo com as regras para efetuar conex�o com o sistema de destino
Exemplo obter um token

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method PreExecucao() Class RmiEnvProtheusObj
    //Como ja estamos no protheus n�o temos que efetuar nenhum procedimento
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CarregaBody
Metodo que carrega a propriedade self:aExecAuto com os dados que ser�o atualizado na base do Protheus

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method CarregaBody() Class RmiEnvProtheusObj

    Local aTags      := {}
    Local nTag       := 0
    Local aTagsSec   := {}
    Local nTagSec    := 0
    Local nTabelaSec := 0
    Local nItem      := 0   
    Local aItem      := {}
    Local xConteudo  := ""
    Local cSetFil    := ''

    //Inicializa o array que ira conter os dados de grava��o
    Asize(self:aExecAuto, 0)
    Aadd( self:aExecAuto, {})

    //Retorno o nome das tags do objeto publica
    aTags := self:oPublica:GetNames()
    If UPPER(Alltrim(self:cProcesso)) == "INVENTARIO" 
        Aadd(self:aSecundaria, "SB7")  //Para Ajustar a tabela cabe�alho e item de uma mesma tabela Modelo 2 
    EndIf
    
    For nTag := 1 To Len(aTags)

        //Carrega as tabelas secundarias do processo
        If Ascan(self:aSecundaria, aTags[nTag]) > 0

            //Verifica se veio algum item
            If self:oPublica[ aTags[nTag] ] == Nil .Or. Len( self:oPublica[ aTags[nTag] ] ) == 0
                Aadd(self:aExecAuto, {})
            Else

                //Pega o nome das tags da tabela secundaria
                aTagsSec := self:oPublica[ aTags[nTag] ][1]:GetNames()

                //Para cada tabela secundaria ser� criado um novo item no aExecAuto
                Aadd(self:aExecAuto, {})
                nTabelaSec := Len(self:aExecAuto)

                For nItem:=1 To Len(self:oPublica[ aTags[nTag] ])

                    //Carrega campos
                    For nTagSec:=1 To Len(aTagsSec)

                        xConteudo := self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ]

                        If GetSX3Cache(aTagsSec[nTagSec], "X3_TIPO") == "D"
                            xConteudo := StoD(xConteudo)
                        EndIf

                        Aadd(aItem, {aTagsSec[nTagSec], xConteudo, Nil} )
                    Next nTagSec

                    //Carrega item
                    Aadd(self:aExecAuto[nTabelaSec], aClone(aItem) )
                    aSize(aItem, 0)

                Next nItem

            EndIf

        //Carrega tabela principal do processo
        Else

            xConteudo := self:oPublica[ aTags[nTag] ]
            cSetFil := IIF('_FILIAL' $ Alltrim(aTags[nTag]),self:oPublica[ aTags[nTag] ],'')
            
            If !Empty(cSetFil)
                RmiFilInt(cSetFil,.T.)//Atuliza cfilAnt .T. 
            EndIf    
            
            If GetSX3Cache(aTags[nTag], "X3_TIPO") == "D"
                xConteudo := StoD(xConteudo)
            EndIf

            Aadd(self:aExecAuto[1], {aTags[nTag], xConteudo, Nil} )
        EndIf

    Next nTag

    //Atualiza o body para ser gravado no distribui��o MHR_ENVIO
    self:cBody := VarInfo("self:aExecAuto", self:aExecAuto, Nil, .F., .F.)
    
    Asize(aTags   , 0)
    Asize(aTagsSec, 0)
    aSize(aItem   , 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Envia
Grava as informa��es recebidas na distribui��o no protheus

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Envia() Class RmiEnvProtheusObj

    Local aAux := {}
    Local nPos := 0
    
    Do Case 

        Case AllTrim(self:cProcesso) == "VENDA"
            
            //Tratamento de Venda\Cancelamento para processamento do RmiRetailJob
            nPos := Ascan(self:aExecAuto[1], {|x| x[1] == "L1_SITUA"})
            Aadd(self:aExecAuto[1],{"L1_UMOV",MHQ->MHQ_UUID,NIL})//Adiciona o Rastreio UUID na venda.
            
            If nPos > 0
                self:aExecAuto[1][nPos][2] := IIF(self:cEvento == "1", "IP", "IC")                  //1=Upsert, 2=Delete
            Else
                Aadd(self:aExecAuto[1], {"L1_SITUA", IIF(self:cEvento == "1", "IP", "IC"), Nil} )   //1=Upsert, 2=Delete
            EndIf

            //Chama grava��o de venda\cancelamento utilizada pelo RMI
            aAux := RsGrvVenda(self:aExecAuto[1], self:aExecAuto[2], self:aExecAuto[3], 3)

            If aAux[1]

                self:lSucesso       := .T.
                self:cChaveExterna  := self:cChaveUnica
                self:cChaveUnica    := SL1->L1_FILIAL + "|" + SL1->L1_NUM
                self:cRetorno       := I18n(STR0001, {self:cChaveUnica})    //"Venda gerada #1"
            Else

                self:lSucesso := .F.
                self:cRetorno := aAux[2]
            EndIf

        Case AllTrim(self:cProcesso) == "CLIENTE"

            LjGrvLog("RMIENVPROTHEUSOBJ", "Vai gravar o cliente na base do Protheus (SA1)")

            //Dentro da funcao RmiGrvCli chama o ExecAuto do MATA030
            aAux := RmiGrvCli(self:aExecAuto)

            If aAux[1]
                LjGrvLog("RMIENVPROTHEUSOBJ", "Cliente gravado com sucesso na base do Protheus (SA1)",SA1->A1_FILIAL + "|" + SA1->A1_COD+"|" + SA1->A1_LOJA)
                self:lSucesso       := .T.
                self:cChaveExterna  := self:cChaveUnica
                self:cChaveUnica    := SA1->A1_FILIAL + "|" + SA1->A1_COD +"|" + SA1->A1_LOJA
                self:cRetorno       := I18n(STR0003, {aAux[1]})    //"Cliente gerado #1"
            Else
                self:lSucesso       := .F.
                self:cRetorno       := aAux[2]
                LjGrvLog("RMIENVPROTHEUSOBJ", "Erro ao Gravar o cliente na base do Protheus (SA1)",self:cRetorno)
            EndIf

        Case AllTrim(self:cProcesso) == "OPERADOR CAIXA"
            
            LjGrvLog("RMIENVPROTHEUSOBJ", "Vai gravar o operador de caixa")

            aAux := RmiGrvOpe(self:aExecAuto, self:cOrigem)

            If ValType(aAux) == 'A' .AND. Len(aAux) > 1
                If aAux[1]
                    self:lSucesso       := .T.
                    self:cChaveExterna  := self:cChaveUnica
                    self:cChaveUnica    := SA6->A6_FILIAL + "|" + SA6->A6_COD
                    self:cRetorno       := I18n("", {aAux[1]})
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Operador de caixa gravado com sucesso na base do Protheus (SA6)",SA6->A6_FILIAL + "|" + SA6->A6_COD)
                Else
                    self:lSucesso       := .F.
                    self:cRetorno       := aAux[2]
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Erro ao Gravar o operador de caixa na base do Protheus (SA6)",self:cRetorno)
                EndIf
            EndIf
        
        Case AllTrim(self:cProcesso) == "INVENTARIO"
            
            LjGrvLog("RMIENVPROTHEUSOBJ", "Vai gravar o INVENTARIO")

            aAux := RmiGrvInv(self:aExecAuto, self:cOrigem)

            If ValType(aAux) == 'A' .AND. Len(aAux) > 1
                If aAux[1]
                    self:lSucesso       := .T.
                    self:cChaveExterna  := self:cChaveUnica //B7_FILIAL+B7_DOC+B7_COD+B7_LOCAL                                                                                                                                
                    self:cChaveUnica    := SB7->B7_FILIAL+"|"+SB7->B7_DOC+"|"+SB7->B7_COD+"|"+SB7->B7_LOCAL
                    self:cRetorno       := I18n(STR0004, {aAux[1]})    //"Operador gerado #1"
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Inventario gravado com sucesso na base do Protheus (SB7)",SB7->B7_FILIAL+"|"+SB7->B7_DOC+"|"+SB7->B7_COD+"|"+SB7->B7_LOCAL)
                Else
                    self:lSucesso       := .F.
                    self:cRetorno       := aAux[2]
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Erro ao Gravar o Inventario na base do Protheus (SB7)",self:cRetorno)
                EndIf
            EndIf    
        
        Case AllTrim(self:cProcesso) == "ADMINISTRADORA"            
            LjGrvLog("RMIENVPROTHEUSOBJ", "Vai gravar a Adm Financeira na base do Protheus (SAE)")
            
            aAux := RmiGrvAdm(self:aExecAuto, self:cChaveUnica)

            If ValType(aAux) == 'A' .AND. Len(aAux) > 1            
                If aAux[1]
                    self:lSucesso       := .T.
                    self:cChaveExterna  := self:cChaveUnica 
                    self:cChaveUnica    := SAE->AE_FILIAL +"|"+SAE->AE_COD
                    self:cRetorno       := I18n(STR0005, {SAE->AE_FILIAL +"|"+SAE->AE_COD})    //"Adm Financeira gerada #1"
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Adm Financeira gravada com sucesso na base do Protheus (SAE)",SAE->AE_FILIAL +"|"+SAE->AE_COD)
                Else
                    self:lSucesso       := .F.
                    self:cRetorno       := aAux[2]
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Erro ao Gravar a Adm Financeira na base do Protheus (SAE)",self:cRetorno)
                EndIf
            EndIf
        
        Case AllTrim(self:cProcesso) == "CAIXA SANGRIA"            
            LjGrvLog("RMIENVPROTHEUSOBJ", "Vai gravar a CAIXA SANGRIA na base do Protheus (SE5)")
            
            aAux := GrvCXSan(self:aExecAuto)
            
            If ValType(aAux) == 'A' .AND. Len(aAux) > 1            
                If aAux[1]
                    self:lSucesso       := .T.
                    self:cChaveExterna  := self:cChaveUnica 
                    self:cChaveUnica    := SE5->E5_FILIAL+"|"+dTos(SE5->E5_DATA)+"|"+SE5->E5_BANCO+"|"+SE5->E5_AGENCIA+"|"+SE5->E5_CONTA+"|"+SE5->E5_IDMOVI
                    self:cRetorno       := I18n("CAIXA SANGRIA", {SE5->E5_FILIAL+"|"+dTos(SE5->E5_DATA)+"|"+SE5->E5_BANCO+"|"+SE5->E5_AGENCIA+"|"+SE5->E5_CONTA+"|"+SE5->E5_IDMOVI})    //"Adm Financeira gerada #1"
                    LjGrvLog("RMIENVPROTHEUSOBJ", "CAIXA SANGRIA gravada com sucesso na base do Protheus (SE5)",SE5->E5_FILIAL+"|"+dTos(SE5->E5_DATA)+"|"+SE5->E5_BANCO+"|"+SE5->E5_AGENCIA+"|"+SE5->E5_CONTA+"|"+SE5->E5_IDMOVI)
                Else
                    self:lSucesso       := .F.
                    self:cRetorno       := aAux[2]
                    LjGrvLog("RMIENVPROTHEUSOBJ", "Erro ao Gravar a CAIXA SANGRIA na base do Protheus (SE5)",self:cRetorno)
                EndIf
            EndIf
        OTherWise

            self:lSucesso := .F.
            self:cRetorno := I18n(STR0002, {self:cProcesso, self:cAssinante})   //"Envio de #1 n�o implmentado para o assinante #2"

    End Case

    Asize(aAux, 0)

Return Nil
