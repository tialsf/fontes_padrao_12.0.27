#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIENVIAOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiEnviaObj
Classe respons�vel pelo envio das distribui��es

/*/
//-------------------------------------------------------------------
Class RmiEnviaObj

    Data cAssinante     As Character    //C�digo do Assinante
    Data oConfAssin     As Objetc       //Objeto JsonObject de configura��o do assinante
    Data cToken         As Character    //C�digo do token que poder� ser atualizado - campo MHO_TOKEN
    
    Data cProcesso      As Character    //C�digo do Processo
    Data oConfProce     As Objetc       //Objeto JsonObject de configura��o do processo
    Data cTabela        As Character    //Principal tabela referente ao processo
    Data aSecundaria    As Character    //Tabelas secundarias relacionadas ao processo
    Data aArrayFil      As Array        //Contem as filias selecionada no Processo (MHP_FILPRO) 
    Data nFil           As Numeric      //Numero de controle para ArrayFil

    Data cTipo          As Character    //Tipo referente ao processo que ser� utilizado (MHP_TIPO)
    Data cLayoutEnv     As Character    //Layout referente ao processo que ser� utilizado para gera��o do body (MHP_LAYENV)
    Data oLayoutEnv     As Objetc       //Objeto JsonObject com layout do corpo da mensagem que ser� enviada

    Data cOrigem        As Character    //Nome do sistema origem que gerou a publica��o (MHQ_ORIGEM)
    Data cPublica       As Character    //Publica��o referente ao processo que ser� utilizado para gera��o do body (MHQ_MENSAG)
    Data oPublica       As Objetc       //Objeto JsonObject com a publica��o
    Data cChaveUnica    As Character    //C�digo da chave �nica referente ao registro publicado
    Data cEvento        As Character    //Define o evento do registro 1=Upsert, 2=Delete
    
    Data oPreExecucao   As Objetc       //Objeto utilizado para efetuar as regras de antes da execu��o Ex: Obter token

    Data cBody          As Character    //Corpo da mensagem que ser� enviada para o sistema de destino
    Data oBody          As Objetc       //Objeto Json com o corpo da mensagem que ser� enviada para o sistema de destino
    Data oEnvia         As Objetc       //Objeto utilizado para efetuar o envio

    Data lSucesso       As Logical      //Define o sucesso da execu��o
    Data cRetorno       As Character    //Mensagem de retorno
    Data cTipoRet       As Character    //Tipo do retorno que pode ser Erro, Alerta ou categorizado
    Data oRetorno       As Objetc       //Objeto que conter� o retorno do envio
    Data cChaveExterna  As Objetc       //C�digo da chave �nica retornado pelo sistema destino

    Data cQuery         As Character    //Query com a consulta das distribui��oes a enviar
    Data cAliasQuery    As Character    //Nome do alias temporario com o resultado da query acima
    
    Method New(cAssinante)              //Metodo construtor da Classe

    Method PreExecucao()                //Metodo com as regras para efetuar conex�o com o sistema de destino

    Method SetaProcesso(cProcesso)      //Metodo responsavel por carregar as informa��es referente ao processo que ser� enviado
    
    Method SetArrayFil()
    
    Method Processa()                   //Metodo que ira controlar o processamento dos envios

    Method Consulta()                   //Metodo que efetua consulta das distribui��es a enviar

    Method CarregaBody()                //Metodo que carrega o corpo da mensagem que ser� enviada

    Method Envia()                      //Metodo responsavel por enviar a mensagens ao sistema de destino

    Method Grava()                      //Metodo que ira atualizar a situa��o da distribui��o e gravar o de\para

    Method SalvaConfig()                //Metodo que ira atualizar a a configura�a� do assinante, com o novo token gerado

    Method Destroi()                    //Metodo que ira tirar os objetos criados da memoria

    Method DePara(cAlias , cValor , nRetIxdex , nIndice)                     //Metodo responsavel por buscar o de/para e validar dado na base.

    Method ProcessNode(oJson, cTag)     //Metodo responsavel em processar um n� do layout de publica��o

    Method AlteraConfig() //Metodo responsavel em ajustar o JSON de configura��o da tabela MHO

    //Metodos para controle de execu��o
    Method getSucesso()                 //Retorna a situa��o da execu��o - lSucesso
    Method getRetorno()                 //Retorna a mensagem de execu��o - cRetorno

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method New(cAssinante) Class RmiEnviaObj
    
    self:cAssinante     := ""
    self:oConfAssin     := Nil
    self:cToken         := ""

    self:cProcesso      := ""
    self:oConfProce     := Nil
    self:cTabela        := ""
    self:aSecundaria    := {}
    self:aArrayFil      := {}
    self:nFil           := 0
    self:cTipo          := "1"  //1=Envio
    self:cLayoutEnv     := ""
    self:oLayoutEnv     := Nil

    self:cOrigem        := ""
    self:cPublica       := ""
    self:oPublica       := Nil
    self:cChaveUnica    := ""
    self:cEvento        := "1"  //1=Upsert

    self:oPreExecucao   := Nil

    self:cBody          := ""
    self:oBody          := Nil
    self:oEnvia         := Nil

    self:lSucesso       := .T.
    self:cRetorno       := ""
    self:cTipoRet       := ""
    self:oRetorno       := Nil
    self:cChaveExterna  := ""

    self:cQuery         := ""
    self:cAliasQuery    := ""

    MHO->( DbSetOrder(1) )  //MHO_FILIAL + MHO_COD
    If MHO->( DbSeek( xFilial("MHO") + PadR(cAssinante, TamSx3("MHO_COD")[1]) ) )

        self:cAssinante := MHO->MHO_COD
        self:cToken     := AllTrim(MHO->MHO_TOKEN)
        self:oConfAssin := JsonObject():New()
        self:oConfAssin:FromJson( AllTrim(MHO->MHO_CONFIG) )
    Else

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0001, {cAssinante})    //"Assinante n�o encontrado #1"
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} PreExecucao
Metodo com as regras para efetuar conex�o com o sistema de destino
Exemplo obter um token

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method PreExecucao() Class RmiEnviaObj
    
    Local cJson := "" //Recebe o Json da configura��o do assinante MHO_CONFIG

    //Inteligencia poder� ser feita na classe filha - default em Rest com Json    
    If self:oPreExecucao == Nil
        self:oPreExecucao := FWRest():New("")

        //Seta a url para pegar o token
        self:oPreExecucao:SetPath( self:oConfAssin["url_token"] )
    EndIf
    
    cJson := self:oConfAssin:ToJson()
    If Self:oConfProce["chave"] <> Nil .AND. !Empty(Self:oConfProce["chave"])
        cJson := StrTran(cJson,self:oConfAssin["chave"],Self:oConfProce["chave"])
    EndIf

    //Seta o corpo do Post
    self:oPreExecucao:SetPostParams( cJson )

    //Busca o token
    If self:oPreExecucao:Post( {"Content-Type:application/json"} )

        self:cRetorno := self:oPreExecucao:GetResult()

        If self:oRetorno == Nil
            self:oRetorno := JsonObject():New()
        EndIf
        self:oRetorno:FromJson(self:cRetorno)

        self:lSucesso   := self:oRetorno[ self:oConfAssin["tagretorno"] ]
        self:cToken     := self:oRetorno[ self:oConfAssin["tagtoken"] ]
    Else

        self:lSucesso := .F.
        self:cRetorno := self:oPreExecucao:GetLastError() + " - [" + self:oConfAssin["url_token"] + "]"
        //self:cRetorno += self:oPreExecucao:GetResult()
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} SetaProcesso
Metodo responsavel por carregar as informa��es referente ao processo que ser� enviado

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method SetaProcesso(cProcesso) Class RmiEnviaObj
    
    //Limpa configura��es anteriores
    self:cProcesso  := ""
    self:cLayoutEnv := ""
    self:cTabela    := ""
    aSize(self:aSecundaria, 0)
    
    MHP->( DbSetOrder(1) )  //MHP_FILIAL + MHP_CASSIN + MHP_CPROCE + MHP_TIPO
    If MHP->( DbSeek( xFilial("MHP") + self:cAssinante + Padr(cProcesso, TamSx3("MHP_CPROCE")[1] ) + self:cTipo ) ) .And. MHP->MHP_ATIVO == "1"

        self:cProcesso  := MHP->MHP_CPROCE
        self:cLayoutEnv := AllTrim(MHP->MHP_LAYENV)
        self:cTabela    := Posicione("MHN", 1, xFilial("MHN") + self:cProcesso, "MHN_TABELA")     //MHN_FILIAL + MHN_COD

        //Gera aArrayFil com as filiais do De/Para
        self:SetArrayFil()

        //Caso o method SetArrayFil gere exception o self:lSucesso � Falso e para gerar log.
        If self:lSucesso

            //Carrega layout que ser� enviado
            If SubStr(self:cLayoutEnv, 1, 1) == "{"
                If self:oLayoutEnv == Nil
                    self:oLayoutEnv := JsonObject():New()
                EndIf
                self:oLayoutEnv:FromJson(self:cLayoutEnv)
            EndIf

            If !Empty(MHP->MHP_CONFIG)
                If self:oConfProce == Nil
                    self:oConfProce := JsonObject():New()
                EndIf
                self:oConfProce:FromJson( AllTrim(MHP->MHP_CONFIG) )
            EndIf

            //Carrega tabelas secundarias
            MHS->( DbSetOrder(1) )  //MHS_FILIAL + MHS_CPROCE + MHS_TABELA
            If MHS->( DbSeek(xFilial("MHS") + self:cProcesso) )
                While !MHS->( Eof() ) .And. MHS->MHS_FILIAL == xFilial("MHS") .And. MHS->MHS_CPROCE == self:cProcesso
                    Aadd(self:aSecundaria, MHS->MHS_TABELA)
                    MHS->( DbSkip() )
                EndDo
            EndIf
        endIf
    Else

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0002, {cProcesso, self:cAssinante})     //"Processo #1 n�o existe ou n�o esta ativo para o Assinante #2"
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Processa
Metodo que ira controlar o processamento dos envios

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Processa() Class RmiEnviaObj

    //Carrega a distribui��es que devem ser enviadas
    self:Consulta()
    
    If !(self:cAliasQuery)->( Eof() )

        While !(self:cAliasQuery)->( Eof() )

            self:lSucesso := .T.
            self:cRetorno := ""

            //Posiciona na publica��o
            MHQ->( DbSetOrder(1) )  //MHQ_FILIAL + MHQ_ORIGEM + MHQ_CPROCE
            MHQ->( DbGoTo( (self:cAliasQuery)->RECNO_PUB ) )
            If !MHQ->( Eof() )

                self:cOrigem     := MHQ->MHQ_ORIGEM
                self:cEvento     := MHQ->MHQ_EVENTO //1=Upsert, 2=Delete
                self:cChaveUnica := MHQ->MHQ_CHVUNI

                //Atualiza o processo que ser� distribuido
                self:SetaProcesso( (self:cAliasQuery)->MHQ_CPROCE )

                //Metodo com as regras para efetuar conex�o com o sistema de destino
                If self:lSucesso
                    self:PreExecucao() 
                EndIf

                //Carrega o layout com os dados da publica��o
                If self:lSucesso

                    //Carrega a publica��o que ser� distribuida
                    self:cPublica := AllTrim(MHQ->MHQ_MENSAG)
                    If self:oPublica == Nil
                        self:oPublica := JsonObject():New()
                    EndIf
                    self:oPublica:FromJson(self:cPublica)

                    self:CarregaBody()
                EndIf

                //Envia a distribui��o ao assinante
                If self:lSucesso
                    self:Envia()
                EndIf
            Else

                self:lSucesso := .F.
                self:cRetorno := I18n(STR0003, {"MHQ", cValToChar( (self:cAliasQuery)->RECNO_PUB)} )       //"Publica��o(#1), recno #2 n�o encontrado"
            EndIf

            //Posiciona na distribui��o
            MHR->( DbSetOrder(1) )  //MHR_FILIAL + MHR_CASSIN + MHR_CPROCE
            MHR->( DbGoTo( (self:cAliasQuery)->RECNO_DIS ) )
            If !MHR->( Eof() )

                self:Grava()
            EndIf

            (self:cAliasQuery)->( DbSkip() )
            FwFreeObj(self:oRetorno)
            self:oRetorno := Nil
        EndDo

        //Atualiza a configura��o do assinante, com o novo token gerado
        If self:lSucesso
            self:SalvaConfig()
        EndIf
    EndIf

    (self:cAliasQuery)->( DbCloseArea() )

    self:Destroi()

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Consulta
Metodo que efetua consulta das distribui��es a enviar

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Consulta() Class RmiEnviaObj

    self:cAliasQuery := GetNextAlias()
    self:cQuery      := " SELECT MHQ_CPROCE, MHQ.R_E_C_N_O_ AS RECNO_PUB, MHR.R_E_C_N_O_ AS RECNO_DIS "
    self:cQuery      += " FROM " + RetSqlName("MHQ") + " MHQ INNER JOIN " + RetSqlName("MHR") + " MHR "
    self:cQuery      += " ON MHQ.MHQ_UUID = MHR.MHR_UIDMHQ "
    self:cQuery      += " WHERE MHQ.D_E_L_E_T_ = ' ' AND MHR.D_E_L_E_T_ = ' '"
    self:cQuery      += " AND MHR_CASSIN = '" + self:cAssinante + "'"
    self:cQuery      += " AND MHR_STATUS = '1'"                         //1=A processar, 2=Processado, 3=Erro

    If !Empty(self:cProcesso)
        self:cQuery  += " AND MHQ_CPROCE = '" + self:cProcesso + "'"
    EndIf

    self:cQuery      += " ORDER BY MHQ_CPROCE, MHQ.R_E_C_N_O_, MHR.R_E_C_N_O_"
    
    DbUseArea(.T., "TOPCONN", TcGenQry( , , self:cQuery), self:cAliasQuery, .T., .F.)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} CarregaBody
Metodo que carrega o corpo da mensagem que ser� enviada

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method CarregaBody() Class RmiEnviaObj

    Local aTags     := {}
    Local nTag      := 0
    Local xLayout   := Nil
    Local nPosIni   := 0
    Local nPosFim   := 0
    Local cAux      := ""
    Local cCampo    := ""
    Local xPublica  := Nil

    Do Case

        //Layout n�o preenchido
        Case Empty(self:cLayoutEnv)

            self:cBody := ""

        //Macro executa fun��o para retornar body
        Case SubStr(self:cLayoutEnv, 1, 1) == "&"

            //Retira quebra de linha e tab para macro executar
            self:cBody := &( SubStr( StrTran( StrTran( StrTran(self:cLayoutEnv, Chr(9), ""), Chr(10), ""), Chr(13), ""), 2) )

        //Carrega body a partir do layout do processo
        OtherWise

            //Cria o corpo da mensagem a partir do layout
            If self:oBody == Nil
                self:oBody := JsonObject():New()
            EndIf            
            self:oBody:FromJson( self:oLayoutEnv:ToJson() )

            //Retorno o nome das propriedades do objeto layout
            aTags := self:oLayoutEnv:GetNames()

            For nTag := 1 To Len(aTags)

                //Carrega configura��o de uma tag do layout
                xLayout  := self:oLayoutEnv[ aTags[nTag] ]
                xPublica := ""

                Do Case

                    //Campo da publica��o
                    Case ValType(xLayout) == "C" .And. ( nPosIni := At("%", xLayout) ) > 0

                        //Carrega campo
                        cAux 	:= SubStr(xLayout, nPosIni + 1)
                        nPosFim := At("%", cAux)
                        cCampo  := AllTrim( SubStr(cAux, 1, nPosFim - 1) )

                        //Pega conteudo da publica��o
                        If At(cCampo, self:cLayoutEnv) > 0
                            xPublica := self:oPublica[cCampo]
                        EndIf

                    //Retorna resultado de macro execu��o
                    Case ValType(xLayout) == "C" .And. SubStr(xLayout, 1, 1) == "&"

                        cCampo  := AllTrim( SubStr(xLayout, 2) )

                        //Macro executa
                        If !Empty(cCampo)
                            xPublica := &(cCampo)
                        EndIf

                    Case ValType(xLayout) == "J"

                        Self:ProcessNode(xLayout, aTags[nTag])

                    //Retorna o proprio contudo do layout
                    OTherWise
                        xPublica := xLayout
                        
                End Case

                If ValType(xLayout) <> "J"

                    If ValType(xPublica) == "C" .AND. !Empty(xPublica)
                        xPublica := EncodeUtf8(xPublica)
                    elseIf (ValType(xPublica) $ "U")  
                        xPublica := I18n(STR0004, {xLayout})    //"Configura��o da TAG #1 inv�lida"
                    EndIf

                    //Atualiza o corpo da mensagem com os dados da publica��o
                    self:oBody[ aTags[nTag] ] := xPublica
                    
                EndIf

            Next nTag

            //Exportar o objeto JSON para uma string em formato JSON
            self:cBody := self:oBody:ToJson()

    End Case

    If ValType(self:cBody) <> "C" .Or. Empty(self:cBody) .Or. self:cBody =="{}"
        self:lSucesso := .F.
        self:cRetorno := STR0005    //"Corpo n�o preenchido ou inv�lido"
    EndIf

    self:cBody := AllTrim(self:cBody)

    Asize(aTags, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Envia
Metodo responsavel por enviar a mensagens ao sistema de destino

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Envia() Class RmiEnviaObj

    //Inteligencia poder� ser feita na classe filha - default em Rest com Json
    If self:lSucesso

        //Inteligencia poder� ser feita na classe filha - default em Rest com Json    
        If self:oEnvia == Nil
            self:oEnvia := FWRest():New("")
        EndIf

        self:oEnvia:SetPath( self:oConfProce["url"] )

        self:oEnvia:SetPostParams(self:cBody)

        If self:oEnvia:Post( {"Content-Type:application/json"} )

            self:cRetorno := self:oEnvia:GetResult()

            If self:oRetorno == Nil
                self:oRetorno := JsonObject():New()
            EndIf
            self:oRetorno:FromJson(self:cRetorno)

            self:lSucesso       := self:oRetorno[ self:oConfProce["tagretorno"] ]

            If self:oConfProce["tagprincipal"] <> Nil .AND. !Empty(self:oConfProce["tagprincipal"])

                //Quando ocorre algum erro no envio, a TAG principal � Nil, neste caso n�o deve pegar a chave externa
                If self:oRetorno[self:oConfProce["tagprincipal"]] <> Nil

                    self:cChaveExterna  := self:oRetorno[self:oConfProce["tagprincipal"]][self:oConfProce["tagcodigo"]]
                    If ValType(self:cChaveExterna) == "N"
                        self:cChaveExterna := cValToChar(self:cChaveExterna)
                    EndIf

                EndIf
            Else
                self:cChaveExterna  := self:oRetorno[ self:oConfProce["tagcodigo"] ]
            EndIf
        Else

            self:lSucesso := .F.
            self:cRetorno := self:oEnvia:GetLastError() + " - [" + self:oConfProce["url"] + "]" + CRLF
            //self:cRetorno += self:oEnvia:GetResult()
        EndIf
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Grava
Metodo que ira atualizar a situa��o da distribui��o e gravar o de\para

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Grava() Class RmiEnviaObj

    Local cStatus    := IIF(self:lSucesso, "2", "3")                                                //1=A processar, 2=Processado, 3=Erro
    Local cSisOrigem := IIF(AllTrim(self:cAssinante) == "PROTHEUS", self:cOrigem, self:cAssinante)  //Tratamento para publica��es que o Protheus assina

    Begin Transaction

        RecLock("MHR", .F.)
            MHR->MHR_STATUS := cStatus
            MHR->MHR_TENTAT := cValToChar( Val(MHR->MHR_TENTAT) + 1 )
            MHR->MHR_DATPRO := Date()
            MHR->MHR_HORPRO := Time()
            MHR->MHR_ENVIO  := self:cBody
            MHR->MHR_RETORN := self:cRetorno

        MHR->( MsUnLock() )

        //S� grava de\para quando tiver chave externa, tratamento para sistemas assincronos
        If self:lSucesso .And. !Empty(self:cChaveExterna)
            RmiDePaGrv( cSisOrigem    , self:cTabela, ""/*cCampo*/, self:cChaveExterna, self:cChaveUnica,;
                        (self:cEvento == "1") )
        EndIf

        If cStatus == "3"
            RMIGRVLOG("IR", "MHR" , MHR->(Recno()), "ENVIA",;
                      self:cRetorno  ,      ,      , 'MHR_STATUS',;
                        .F.          , 3      ,MHR->MHR_FILIAL + MHR->MHR_UIDMHQ + MHR->MHR_CASSIN + MHR->MHR_CPROCE,MHR->MHR_CPROCE,MHR->MHR_CASSIN)
        EndIf

    End Transaction
    
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} SalvaConfig
Metodo que ira atualizar a configura��o do assinante.
Por enquanto atualiza o token

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method SalvaConfig() Class RmiEnviaObj
    
    MHO->( DbSetOrder(1) )  //MHO_FILIAL + MHO_COD
    If MHO->( DbSeek( xFilial("MHO") + self:cAssinante ) )

        RecLock("MHO", .F.)
            MHO->MHO_TOKEN := RTrim(self:cToken)
        MHO->( MsUnLock() )
    EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Destroi
Metodo que ira tirar os objetos criados da memoria

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method Destroi() Class RmiEnviaObj
    
    FwFreeObj(self:oConfAssin)
    FwFreeObj(self:oConfProce)
    FwFreeObj(self:oLayoutEnv)
    FwFreeObj(self:oPublica)
    FwFreeObj(self:oBody)
    FwFreeObj(self:oPreExecucao)
    FwFreeObj(self:oEnvia)
    FwFreeObj(self:oRetorno)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} getSucesso
Retorna a situa��o da execu��o - lSucesso

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method getSucesso() Class RmiEnviaObj
Return self:lSucesso

//-------------------------------------------------------------------
/*/{Protheus.doc} getRetorno
Retorna a mensagem de execu��o - cRetorno

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Method getRetorno() Class RmiEnviaObj
    LjxjMsgErr( self:cRetorno, /*cSolucao*/ )
Return self:cRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} SetArrayFil
Alimenta o Atributo aArrayFil para controle de filial no processo.
@author  Everson S. P. Junior
@version 1.0
/*/
//-------------------------------------------------------------------
Method SetArrayFil() Class RmiEnviaObj

    Local aArrayCamp := {}
    Local nX         := 0
    Local cRetFil    := ""
    Local cSisOrigem := IIF(AllTrim(self:cAssinante) == "PROTHEUS", self:cOrigem, self:cAssinante)  //Tratamento para publica��es que o Protheus assina

    aSize(self:aArrayFil, 0)

    //Campo obrigatorio caso esteja em branco gerar exception
    If !Empty(MHP->MHP_FILPRO)

        aArrayCamp := StrToKarr( Alltrim(MHP->MHP_FILPRO), ";")

        For nX:=1 To Len(aArrayCamp)

            //Parametros-> cSisOri(CHEF ou LIVE...) ,Alias,cPesDePara,lOrigem) lOrigem-> .T. Retorna o campo MHM_VLORIG .F. Retorna MHM_VLINT
            cRetFil := RmiDePaRet(cSisOrigem, "SM0", aArrayCamp[nX], .T.)
            
            If !Empty(cRetFil)

                Aadd(self:aArrayFil, {cRetFil, aArrayCamp[nX]})
            Else

                self:lSucesso := .F.
                self:cRetorno := I18n(STR0006, {"cProcesso", self:cAssinante})  //De Para de Filiais n�o encontrado     
                Exit
            EndIf
        Next nX

    Else

        self:lSucesso := .F.
        self:cRetorno := I18n(STR0007, {"cProcesso", self:cAssinante})  //Campo Obrigatorio de Filiais n�o preenchido no cadastro de Assinantes
    EndIf

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} DePara
Metodo responsavel por buscar o de/para e validar seus dados na base.
Seta erro caso nao encontre de/para ou registro na base.

@param cAlias        Alias para Busca do De/Para | Obrigat�rio
@param cValor        Valor do De/Para para busca | Obrigat�rio
@param nRetIxdex     Indice a ser retornado quando De/para com mais de um retorno | Padr�o 2
@param nIndice       Indice para busca da informa��o na base se passar 0 n�o valida | Padr�o 1
@param lOrigem       indica se busca dado de origem | Padr�o .F. 

@author  Rafael Pessoa
@version 1.0
/*/
//-------------------------------------------------------------------
Method DePara(  cAlias  , cValor    , nRetIxdex , nIndice ,;
                lOrigem , cOrigem   ) Class RmiEnviaObj
    
    Local cRetDePara    := ""
    Local cRet          := ""
    Local aAux          := {}
    Local cChave        := ""

    Default cAlias	    := ""
    Default cValor	    := ""
    Default nRetIxdex   := 2
    Default nIndice	    := 1
    Default lOrigem	    := .F.
    Default cOrigem	    := ""

    LjGrvLog("DePara","ID_INICIO")

    cAlias      := AllTrim(cAlias)

    If Empty(cOrigem)    
        cOrigem     := IIf(AllTrim(self:cAssinante) == "PROTHEUS" , self:cOrigem, self:cAssinante)
    EndIf    
    
    LjGrvLog("DePara","cOrigem: " + cOrigem + " |cAlias: " + cAlias + " |cValor: " + cValor , lOrigem)
    
    cRetDePara := RmiDePaRet(cOrigem, cAlias, cValor , lOrigem)

    If Empty(cRetDePara) .And. ExistFunc("RmiDePaAut")
        //Verifica se a tabela tem tratamento para inclus�o autom�tica
        LjGrvLog("DePara","Antes de buscar Automatico " + cRetDePara)
        cRetDePara := RmiDePaAut(cOrigem, cAlias, cValor , lOrigem)
        LjGrvLog("DePara","Depois de buscar Automatico " + cRetDePara)
    EndIf

    If Empty(cRetDePara)
        self:lSucesso := .F.
        self:cTipoRet := "DEPARA"
        self:cRetorno   += I18n(STR0008, {AllTrim(cOrigem), cAlias, cValor }) + CHR(13)+CHR(10) //"De/Para(MHM) n�o encontrado para a busca realizada. Origem: #1 | Alias: #2 | Valor: #3"    
    Else

        aAux := Separa(cRetDePara,"|") 

        If Len(aAux) >= nRetIxdex

            If lOrigem
                cChave  := Replace(cValor,"|","")
            Else
                cChave  := Replace(cRetDePara,"|","")
            EndIf

            cRet    := aAux[nRetIxdex] //Pega o Retorno na posi��o do parametro da fun��o

            If nIndice > 0
                (cAlias)->( DbSetOrder(nIndice) )
                If !(cAlias)->( DbSeek(cChave) )
                    cRet := ""
                    self:lSucesso := .F.
                    self:cTipoRet := "DEPARA"
                    self:cRetorno   += I18n(STR0009, {cAlias, nIndice , cChave }) + CHR(13)+CHR(10) //"Registro n�o encontrado para a busca realizada no Alias: #1 | Indice: #2 | Chave: #3"   
                EndIf
            EndIf
        Else
            self:lSucesso := .F.
            self:cTipoRet := "DEPARA"
            self:cRetorno   += I18n(STR0010, { nRetIxdex , cRetDePara })  + CHR(13)+CHR(10) //"De/Para n�o localizado para posi��o: #1 . De/Para: #2 ."
        EndIf

    EndIf

    LjGrvLog("DePara","cRet: " + cRet)
    LjGrvLog("DePara","ID_FIM")

Return cRet


//-------------------------------------------------------------------
/*/{Protheus.doc} AlteraConfig
Metodo responsavel em ajustar o JSON de configura��o da tabela MHO

@param   oJson -> Recebe o objeto json na qual seria o n� a processar
@param   cTag  -> Nome da tag do n�
@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method AlteraConfig() Class RmiEnviaObj

Local cJson         := self:oConfAssin:ToJson() //Retorno do JSON
Local aTagsAssi     := self:oConfAssin:GetNames() //Retorna as tags do assinante
Local aTagsProce    := self:oConfProce:GetNames() //Retorna as tagas do processo
Local nI            := 0 //Variavel de loop
Local nPos          := 0 //Posi��o da tag

For nI := 1 To Len(aTagsAssi)
    nPos := aScan(aTagsProce,{|x| AllTrim(x) == AllTrim(aTagsAssi[nI])})
    If nPos > 0
        If self:oConfProce[aTagsAssi[nI]] <> Nil .AND. !Empty(self:oConfProce[aTagsAssi[nI]])
            cJson := StrTran(cJson,self:oConfAssin[aTagsAssi[nI]],Self:oConfProce[aTagsAssi[nI]])
        EndIf
    EndIf
Next nI

Return cJson


//-------------------------------------------------------------------
/*/{Protheus.doc} ProcessNode
Metodo responsavel por processar um n� no layout de envio do cadastro

@param   oJson -> Recebe o objeto json na qual seria o n� a processar
@param   cTag  -> Nome da tag do n�
@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method ProcessNode(oJson, cTag) Class RmiEnviaObj

Local aTags     := oJson:GetNames() //Armazena todas as tags para processar
Local nI        := 0                //Variavel de loop
Local xLayout   := Nil              //Recebe o layout a processar
Local xPublica  := Nil              //Recebe o conteudo para colocar no objeto oBody
Local nPosIni   := 0                //Verifica na string se existe um determinado caractere
Local nPosFim   := 0                //Verifica na string se existe um determinado caractere
Local cAux      := ""               //Conte�do auxilixar a ser macro-executado
Local cCampo    := ""               //Recebe o campo a ser macro-executado

For nI := 1 To Len(aTags)

    //Carrega configura��o de uma tag do layout
    xLayout  := oJson[ aTags[nI] ]
    xPublica := ""

    Do Case

        //Campo da publica��o
        Case ValType(xLayout) == "C" .And. ( nPosIni := At("%", xLayout) ) > 0

            //Carrega campo
            cAux 	:= SubStr(xLayout, nPosIni + 1)
            nPosFim := At("%", cAux)
            cCampo  := AllTrim( SubStr(cAux, 1, nPosFim - 1) )

            //Pega conteudo da publica��o
            If At(cCampo, self:cLayoutEnv) > 0
                xPublica := self:oPublica[cCampo]
            EndIf

        //Retorna resultado de macro execu��o
        Case ValType(xLayout) == "C" .And. SubStr(xLayout, 1, 1) == "&"

            cCampo  := AllTrim( SubStr(xLayout, 2) )

            //Macro executa
            If !Empty(cCampo)
                xPublica := &(cCampo)
            EndIf

        //Retorna o proprio contudo do layout
        OTherWise
            xPublica := xLayout
            
    End Case

    If ValType(xPublica) == "C" .AND. !Empty(xPublica)
        xPublica := EncodeUtf8(xPublica)
    elseIf (ValType(xPublica) $ "U")  
        xPublica := I18n(STR0004, {xLayout})    //"Configura��o da TAG #1 inv�lida"
    EndIf

    //Atualiza o corpo da mensagem com os dados da publica��o
    self:oBody[cTag][ aTags[nI] ] := xPublica
Next nI


Return Nil