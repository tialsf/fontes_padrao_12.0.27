#INCLUDE "PROTHEUS.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiGrvMsgPubChefObj
Classe respons�vel em gravar o Json de publica��o no campo MHQ_MENSAG
    
/*/
//-------------------------------------------------------------------
Class RmiGrvMsgPubChefObj From RmiGrvMsgPubObj

Method New()	//Metodo construtor da Classe
Method GeraMsg() //Metodo responsavel em gravar as mensagem no campo MHQ_MENSAG

EndClass


//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiGrvMsgPubChefObj

_Super:New()

Return Nil


//--------------------------------------------------------
/*/{Protheus.doc} GravaMsg
Metodo responsavel em gravar as mensagem no campo MHQ_MENSAG

@author  	Bruno Almeida
@version 	1.0
@since      25/05/2020
@return	    
/*/
//--------------------------------------------------------
Method GeraMsg() Class RmiGrvMsgPubChefObj

Local aTags         := {} //Guarda as tags do layout de publica��o
Local aTagsSec      := {} //Guarda as tags filhas do layout de publica��o
Local aNoFilho      := {} //Array do No Filho com os itens que ser�o procesasdos
Local xNoFilho      := Nil //Objeto do No Filho com os itens que ser�o procesasdos
Local nTagSec       := 0 //Variavel de loop
Local cLayoutPub    := "" //Propriedade que ser� macro executada para pegar o contedo retornado pelo assinante
Local nItem         := 0 //Variavel de loop
Local nTag          := 0 //Variavel de loop
Local oBuscaObj     := Nil //Inst�ncia da classe RmibuscaObj

//Realiza a consulta para saber se tem publica��o com status = 0
Self:Consulta()
dbSelectArea("MHQ")

While !(self:cAliasQuery)->( Eof() )
   
    //Move para o registro
    MHQ->(dbGoto((self:cAliasQuery)->R_E_C_N_O_))

    //Aqui estou instanciando a classe RmiBuscaObj para utilizar os objetos oLayoutPub, oConfProce, oRegistro e o metodo AuxTrataTag
    oBuscaObj := RmiBuscaObj():New(AllTrim(MHQ->MHQ_ORIGEM))
    oBuscaObj:SetaProcesso(AllTrim(MHQ->MHQ_CPROCE))

    //Posiciona na MHP
    Self:PosMHP(AllTrim(MHQ->MHQ_ORIGEM), AllTrim(MHQ->MHQ_CPROCE))

    If Self:lSucesso
    
        If oBuscaObj:oRegistro == Nil
            oBuscaObj:oRegistro := JsonObject():New()
        EndIf
        oBuscaObj:oRegistro:FromJson(MHQ->MHQ_MSGORI)
        
        //Guarda o array todas as tags do layout de publica��o
        aTags := oBuscaObj:oLayoutPub:GetNames()

        //Cria objeto que conter� a publica��o
        If self:oPublica == Nil
            self:oPublica := JsonObject():New()
        EndIf            

        //Percorre tags do layout de publica��o
        For nTag := 1 To Len(aTags)

            //Processa tags de um N� filho
            If aTags[nTag] $ "SL2|SL4"
                
                self:oPublica[ aTags[nTag] ] := {}

                //Pega o nome das tags filhas
                aTagsSec := oBuscaObj:oLayoutPub[ aTags[nTag] ][1]:GetNames()

                //Pega o caminho para o No Filho
                aNoFilho := {}
                ASize(aNoFilho, 0)
                xNoFilho := &( Self:TrataObj(oBuscaObj:oConfProce[ aTags[nTag] ]) )

                //Tratamento no layout para quando tiver apenas 1 item
                If ValType(xNoFilho) == "O"
                    Aadd(aNoFilho, xNoFilho)
                Else
                    aNoFilho := xNoFilho
                EndIf

                If ValType(aNoFilho) == "A"

                    For nItem:=1 To Len( aNoFilho )

                        Aadd(self:oPublica[ aTags[nTag] ], JsonObject():New())

                        //Carrega tags do No Filho
                        For nTagSec:=1 To Len(aTagsSec)

                            cLayoutPub := oBuscaObj:oLayoutPub[ aTags[nTag] ][1][ aTagsSec[nTagSec] ]
                            
                            //Tratamento no layout para quando tiver apenas 1 item
                            If ValType(xNoFilho) == "O"
                                cLayoutPub := StrTran( cLayoutPub, "[nItem]", "")
                            EndIf

                            If "_FILIAL" $ AllTrim(aTagsSec[nTagSec])
                                self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := Self:RetFil("CHEF","SM0",oBuscaObj:oRegistro["Loja"]["Codigo"],.F.)
                            Else
                                self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := oBuscaObj:AuxTrataTag( aTagsSec[nTagSec], cLayoutPub, nItem)
                            EndIf
                            
                            If !oBuscaObj:lSucesso
                                Self:lSucesso := .F.
                                Self:cErro := oBuscaObj:cRetorno
                                Exit
                            ElseIf !Self:lSucesso
                                Exit
                            EndIf

                        Next nTagSec

                        If !oBuscaObj:lSucesso .OR. !Self:lSucesso
                            Exit
                        EndIf

                    Next nItem

                Else

                    //Insere uma forma de pagamento, porque quando � cancelamento as vezes n�o vem pagamento
                    If ValType(aNoFilho) == "U" .And. AllTrim( Upper( aTags[nTag] ) ) == "SL4"

                        //Cancelamento
                        If oBuscaObj:oRegistro["StatusVenda"] == 3
                            Aadd(self:oPublica[ "SL4" ], JsonObject():New())

                            self:oPublica["SL4"][1]["L4_DATA"  ] := self:oPublica["L1_EMISSAO"]
                            self:oPublica["SL4"][1]["L4_FORMA" ] := "R$"
                            self:oPublica["SL4"][1]["L4_VALOR" ] := self:oPublica["L1_VLRTOT"]
                            self:oPublica["SL4"][1]["L4_FILIAL"] := self:oPublica["L1_FILIAL"]
                        EndIf
                    EndIf
                EndIf

            //Processa tags principais
            Else
                If "_FILIAL" $ AllTrim(aTags[nTag])
                    self:oPublica[ aTags[nTag] ] := Self:RetFil("CHEF","SM0",oBuscaObj:oRegistro["Loja"]["Codigo"],.F.)                
                Else            
                    self:oPublica[ aTags[nTag] ] := oBuscaObj:AuxTrataTag( aTags[nTag], oBuscaObj:oLayoutPub[ aTags[nTag] ] )
                EndIf

                If !oBuscaObj:lSucesso
                    Self:lSucesso := .F.
                    Self:cErro := oBuscaObj:cRetorno
                EndIf                
            Endif

            If !Self:lSucesso
                Exit
            EndIf

        Next nTag

    EndIf
    //Grava informa��o no campo MHQ_MENSAGE
    Self:Gravar(self:oPublica:toJson())

    //Limpa os objetos
    FwFreeObj(xNoFilho)
    FwFreeObj(oBuscaObj)
    FwFreeObj(self:oPublica)    
    xNoFilho        := Nil
    oBuscaObj       := Nil
    self:oPublica   := Nil

    If ValType(aNoFilho) == "A"
        Asize(aNoFilho, 0)
    EndIf
    Asize(aTagsSec, 0)

    (self:cAliasQuery)->( DbSkip() )
EndDo

(self:cAliasQuery)->( DbCloseArea() )

Return Nil