#INCLUDE "PROTHEUS.CH"
#INCLUDE "XMLXFUN.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiGrvMsgPubLiveObj
Classe respons�vel em gravar o Json de publica��o no campo MHQ_MENSAG
    
/*/
//-------------------------------------------------------------------
Class RmiGrvMsgPubLiveObj From RmiGrvMsgPubObj

Method New()	//Metodo construtor da Classe
Method GeraMsg() //Metodo responsavel em gravar as mensagem no campo MHQ_MENSAG

EndClass


//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiGrvMsgPubLiveObj

_Super:New()

Return Nil


//--------------------------------------------------------
/*/{Protheus.doc} GravaMsg
Metodo responsavel em gravar as mensagem no campo MHQ_MENSAG

@author  	Bruno Almeida
@version 	1.0
@since      28/05/2020
@return	    
/*/
//--------------------------------------------------------
Method GeraMsg() Class RmiGrvMsgPubLiveObj

Local aTags         := {} //Guarda as tags do layout de publica��o
Local aTagsSec      := {} //Guarda as tags filhas do layout de publica��o
Local aNoFilho      := {} //Array do No Filho com os itens que ser�o procesasdos
Local xNoFilho      := Nil //Objeto do No Filho com os itens que ser�o procesasdos
Local nTagSec       := 0 //Variavel de loop
Local cLayoutPub    := "" //Propriedade que ser� macro executada para pegar o contedo retornado pelo assinante
Local nItem         := 0 //Variavel de loop
Local nTag          := 0 //Variavel de loop
Local oBuscaObj     := Nil //Inst�ncia da classe RmibuscaObj
Local aTagsTri      := {} //Armazena os dados de tributa��o do Live
Local nTrib         := 0 //Variavel de loop
Local aCmpsTri      := {} //Campo de tributa��o

//Realiza a consulta para saber se tem publica��o com status = 0
Self:Consulta()
dbSelectArea("MHQ")

While !(self:cAliasQuery)->( Eof() )

    //Move para o registro
    MHQ->(dbGoto((self:cAliasQuery)->R_E_C_N_O_))

    //Aqui estou instanciando a classe RmiBuscaObj para utilizar os objetos oLayoutPub, oConfProce, oRegistro e o metodo AuxTrataTag
    oBuscaObj := RmiBuscaObj():New(AllTrim(MHQ->MHQ_ORIGEM))
    oBuscaObj:SetaProcesso(AllTrim(MHQ->MHQ_CPROCE))

    //Posiciona na MHP
    Self:PosMHP(AllTrim(MHQ->MHQ_ORIGEM), AllTrim(MHQ->MHQ_CPROCE))

    If Self:lSucesso

        oBuscaObj:oRegistro := Self:ExecParseXml(MHQ->MHQ_MSGORI)
        
        If Self:lSucesso
            //Guarda o array todas as tags do layout de publica��o
            aTags := oBuscaObj:oLayoutPub:GetNames()

            If self:oPublica == Nil
                self:oPublica := JsonObject():New()
            EndIf            

            //Percorre tags do layout de publica��o
            For nTag := 1 To Len(aTags)

                //Processa tags de um N� filho
                If aTags[nTag] $ "SL2|SL4"
                    
                    self:oPublica[ aTags[nTag] ] := {}

                    //Pega o nome das tags filhas
                    aTagsSec := oBuscaObj:oLayoutPub[ aTags[nTag] ][1]:GetNames()

                    //Pega o caminho para o No Filho
                    ASize(aNoFilho, 0)
                    xNoFilho := &( Self:TrataObj(oBuscaObj:oConfProce[ aTags[nTag] ]) )

                    //Tratamento no layout para quando tiver apenas 1 item
                    If ValType(xNoFilho) == "O"
                        Aadd(aNoFilho, xNoFilho)
                    Else
                        aNoFilho := xNoFilho
                    EndIf

                    If ValType(aNoFilho) == "A"

                        For nItem:=1 To Len( aNoFilho )

                            Aadd(self:oPublica[ aTags[nTag] ], JsonObject():New())

                            //Carrega tags do No Filho
                            For nTagSec:=1 To Len(aTagsSec)                     

                                cLayoutPub := oBuscaObj:oLayoutPub[ aTags[nTag] ][1][ aTagsSec[nTagSec] ]
                                
                                //Tratamento no layout para quando tiver apenas 1 item
                                If ValType(xNoFilho) == "O"
                                    cLayoutPub := StrTran( cLayoutPub, "[nItem]", "")
                                EndIf
                                
                                self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := oBuscaObj:AuxTrataTag( aTagsSec[nTagSec], cLayoutPub, nItem)

                                If !oBuscaObj:lSucesso
                                    Self:lSucesso := .F.
                                    Self:cErro := oBuscaObj:cRetorno
                                    Exit
                                EndIf

                            Next nTagSec

                            If !Self:lSucesso
                                Exit
                            EndIf

                            //Tratamento para impostos
                            If aTags[nTag] == "SL2"

                                If Valtype(oBuscaObj:oRegistro:_Itens:_Lc_ItemCupomfiscal) == "O"
                                    cLayoutPub := "oBuscaObj:oRegistro:_Itens:_Lc_ItemCupomfiscal:_Tributos:_Lc_TributoCupomFiscal"
                                Else
                                    cLayoutPub := "oBuscaObj:oRegistro:_Itens:_Lc_ItemCupomfiscal[nItem]:_Tributos:_Lc_TributoCupomFiscal"
                                EndIf

                                aTagsTri := &(cLayoutPub)
                            
                                For nTrib := 1 To Len (aTagsTri)

                                    ASize(aCmpsTri, 0)
                                    Do Case
                                        Case aTagsTri[nTrib]:_DescricaoTributo:Text == "ICMS"
                                            aCmpsTri := {"L2_VALICM" , "L2_BASEICM", "L2_PICM"   }

                                        Case aTagsTri[nTrib]:_DescricaoTributo:Text == "PIS"
                                            aCmpsTri := {"L2_VALPIS" , "L2_BASEPIS", "L2_ALIQPIS"}

                                        Case aTagsTri[nTrib]:_DescricaoTributo:Text == "COFINS"
                                            aCmpsTri := {"L2_VALCOFI", "L2_BASECOF", "L2_ALIQCOF"}
                                    End Case

                                    If Len(aCmpsTri) > 0

                                        Aadd(aTagsSec, aCmpsTri[1])
                                        nTagSec := Len(aTagsSec)
                                        self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := oBuscaObj:AuxTrataTag(aTagsSec[nTagSec], aTagsTri[nTrib]:_ValorTributo:Text, nItem)

                                        Aadd(aTagsSec, aCmpsTri[2])
                                        nTagSec := Len(aTagsSec)
                                        self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := oBuscaObj:AuxTrataTag(aTagsSec[nTagSec], aTagsTri[nTrib]:_BaseCalculo:Text , nItem)

                                        Aadd(aTagsSec, aCmpsTri[3])
                                        nTagSec := Len(aTagsSec)
                                        self:oPublica[ aTags[nTag] ][nItem][ aTagsSec[nTagSec] ] := oBuscaObj:AuxTrataTag(aTagsSec[nTagSec], aTagsTri[nTrib]:_Aliquota:Text    , nItem)
                                    EndIf

                                    If !oBuscaObj:lSucesso
                                        Self:lSucesso := .F.
                                        Self:cErro := oBuscaObj:cRetorno
                                        Exit
                                    EndIf                                    

                                Next nTrib

                            EndIf
                            
                            If !Self:lSucesso
                                Exit
                            EndIf

                            //Ajusto o array para default 
                            aTagsSec := oBuscaObj:oLayoutPub[ aTags[nTag] ][1]:GetNames()                  
                        Next nItem
                    EndIf

                //Processa tags principais
                Else
                    self:oPublica[ aTags[nTag] ] := oBuscaObj:AuxTrataTag( aTags[nTag], oBuscaObj:oLayoutPub[ aTags[nTag] ] )
                    If !oBuscaObj:lSucesso
                        Self:lSucesso := .F.
                        Self:cErro := oBuscaObj:cRetorno
                    EndIf
                Endif

                If !Self:lSucesso
                    Exit
                EndIf

            Next nTag

        EndIf

    EndIf

    //Grava informa��o no campo MHQ_MENSAGE
    Self:Gravar(self:oPublica:toJson())

    //Limpa os objetos
    FwFreeObj(xNoFilho)
    FwFreeObj(oBuscaObj)
    FwFreeObj(self:oPublica)    
    xNoFilho        := Nil
    oBuscaObj       := Nil
    self:oPublica   := Nil

    If ValType(aNoFilho) == "A"
        Asize(aNoFilho, 0)
    EndIf
    Asize(aTagsSec, 0)
    Asize(aTagsTri, 0)
    Asize(aCmpsTri, 0)

    (self:cAliasQuery)->( DbSkip() )
EndDo

Return Nil