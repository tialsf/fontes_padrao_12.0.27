#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIGRVMSGPUBOBJ.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} Classe RmiGrvMsgPubObj
Classe generica para o processamento da fila e grava��o do campo MHQ_MENSAG
    
/*/
//-------------------------------------------------------------------
Class RmiGrvMsgPubObj

Data cAliasQuery    As Character    //Nome do alias temporario com o resultado da query
Data cQuery         As Character    //Armazena a query que sera executada
Data oPublica       As Objetc       //Objeto JsonObject com a publica��o
Data lSucesso       As Logical      //Define o sucesso da execu��o
Data cErro          As Character    //Armazena a mensagem de erro

Method New()	//Metodo construtor da Classe
Method Consulta() //Metodo responsavel em consultar as mensagem disponiveis na fila para processamento
Method TrataObj(cTexto) //Metodo para realizar o tratamento do objeto que � instancia da classe RmiBuscaObj
Method Gravar(cMsg) //Grava o conteudo no campo MHQ_MENSAGE
Method PosMHP(cAssinante, cProcesso) //Posiciona no registro da MHP
Method RetFil(cCodFilial) //Retorna a filial do Protheus que esta cadastrado no de/para
Method Reprocessa(cChave) //Reprocessa uma mensagem que esta com status igual a 3 na MHQ
Method ExecParseXml(cMsgOriginal) //Executa o parse do XML e devolve um objeto

EndClass


//-------------------------------------------------------------------
/*/{Protheus.doc} New
M�todo construtor da Classe

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method New() Class RmiGrvMsgPubObj

Self:cAliasQuery    := ""
Self:cQuery         := ""
Self:oPublica       := Nil
Self:lSucesso       := .T.
Self:cErro          := ""

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} Consulta
Metodo responsavel em consultar as mensagem disponiveis na fila para processamento

@author  Bruno Almeida
@version 1.0
/*/
//-------------------------------------------------------------------
Method Consulta() Class RmiGrvMsgPubObj

Self:cAliasQuery := GetNextAlias()

Self:cQuery := "SELECT R_E_C_N_O_ "
Self:cQuery += "  FROM " + RetSqlName("MHQ")
Self:cQuery += " WHERE MHQ_STATUS = '0' "
Self:cQuery += "   AND MHQ_CPROCE = 'VENDA' "
Self:cQuery += "   AND D_E_L_E_T_ = ' ' "

DbUseArea(.T., "TOPCONN", TcGenQry( , , Self:cQuery), Self:cAliasQuery, .T., .F.)

If (self:cAliasQuery)->( Eof() )
    LjGrvLog("RMIGRVMSGPUBOBJ", "N�o foi encontrado nenhum registro (MHQ_STATUS = 0) na fila para processar a mensagem e gravar no campo MHQ_MENSAGE", "Query: " + Self:cQuery)
EndIf

Return Nil

//--------------------------------------------------------
/*/{Protheus.doc} TrataObj
Metodo para realizar o tratamento do objeto que � instancia da classe RmiBuscaObj

@author  	Bruno Almeida
@version 	1.0
@since      26/05/2020
@return	    
/*/
//--------------------------------------------------------
Method TrataObj(cTexto) Class RmiGrvMsgPubObj

cTexto := StrTran(cTexto,"self","oBuscaObj")

Return cTexto

//--------------------------------------------------------
/*/{Protheus.doc} Gravar
Grava o conteudo no campo MHQ_MENSAGE

@author  	Bruno Almeida
@version 	1.0
@since      26/05/2020
@return	    
/*/
//--------------------------------------------------------
Method Gravar(cMsg) Class RmiGrvMsgPubObj

If Self:lSucesso
    RecLock("MHQ", .F.)
    MHQ->MHQ_MENSAG := cMsg
    MHQ->MHQ_STATUS := "1"
    MHQ->( MsUnLock() )

    LjGrvLog("RMIGRVMSGPUBOBJ", "Mensagem gravada com sucesso!", "Venda (MHQ_CHVUNI): " + MHQ->MHQ_CHVUNI)
Else
    RmiGrvLog("3", "MHQ", MHQ->(Recno()), "PUBLICACAO", self:cErro,,, 'MHQ_STATUS', .T., 7, MHQ->MHQ_FILIAL + MHQ->MHQ_UUID, MHQ->MHQ_CPROCE, MHQ->MHQ_ORIGEM)
    LjGrvLog("RMIGRVMSGPUBOBJ", Self:cErro, "Venda (MHQ_CHVUNI): " + MHQ->MHQ_CHVUNI)

    Self:lSucesso := .T.
    Self:cErro := ""
EndIf

Return Nil

//--------------------------------------------------------
/*/{Protheus.doc} PosMHP
Posiciona no registro da MHP

@author  	Bruno Almeida
@version 	1.0
@since      27/05/2020
@return	    
/*/
//--------------------------------------------------------
Method PosMHP(cAssinante, cProcesso) Class RmiGrvMsgPubObj

Local cAlias := GetNextAlias()
Local cQuery := ""

Default cAssinante := ""
Default cProcesso := ""

cQuery := "SELECT HP.R_E_C_N_O_ "
cQuery += "  FROM " + RetSqlName("MHO") + " HO "
cQuery += "       INNER JOIN " + RetSqlName("MHP") + " HP ON HP.MHP_FILIAL = HO.MHO_FILIAL"
cQuery += "	                                            AND HP.MHP_CASSIN = HO.MHO_COD"
cQuery += " WHERE HP.MHP_CASSIN = '" + cAssinante + "'"
cQuery += "   AND HP.MHP_CPROCE = '" + cProcesso + "'"
cQuery += "   AND HP.MHP_ATIVO = '1'"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

If !(cAlias)->( Eof() )
    dbSelectArea("MHP")
    MHP->(dbGoto((cAlias)->R_E_C_N_O_))
    LjGrvLog("RMIGRVMSGPUBOBJ", "Posicionou com sucesso no registro da MHP", AllTrim(MHP->MHP_CASSIN) + "|" + AllTrim(MHP->MHP_CPROCE))
Else
    Self:lSucesso := .F.
    Self:cErro := STR0004 + cAssinante + STR0005 + cProcesso //"N�o foi encontrado o processo (MHP), o processo pode n�o estar cadastrado ou n�o esta ativo. Assinante: " # " - Processo: "
EndIf

(cAlias)->( DbCloseArea() )

Return Nil

//--------------------------------------------------------
/*/{Protheus.doc} RetFil
Retorna a filial do Protheus que esta cadastrado no de/para

@author  	Bruno Almeida
@version 	1.0
@since      27/05/2020
@return	    
/*/
//--------------------------------------------------------
Method RetFil(cSisOrigem, cAlias, nCodFilial, lOrigem) Class RmiGrvMsgPubObj

Local cRet := ""

Default cSisOrigem := ""
Default cAlias := "SM0"
Default nCodFilial := ""
Default lOrigem := .F.

cRet := RmiDePaRet(cSisOrigem, cAlias, nCodFilial, lOrigem)

If Empty(cRet)
    Self:lSucesso   := .F.
    Self:cErro      := STR0001 + IIF(ValType(nCodFilial) == "N", cValToChar(nCodFilial), nCodFilial)  //"De/para de filial (SM0) n�o encontrado - Cod. da Filial de Origem: "
Else
    LjGrvLog("RMIGRVMSGPUBOBJ", "Retorno do de/para da filial com sucesso!", "Venda (MHQ_CHVUNI): " + MHQ->MHQ_CHVUNI)    
EndIf

Return cRet

//--------------------------------------------------------
/*/{Protheus.doc} Reprocessa
Reprocessa uma mensagem que esta com status igual a 3 na MHQ

@author  	Bruno Almeida
@version 	1.0
@since      27/05/2020
@return	    
/*/
//--------------------------------------------------------
Method Reprocessa(cChave) Class RmiGrvMsgPubObj

Local cAlias := GetNextAlias()
Local cQuery := ""

Default cChave := ""

cQuery := "SELECT R_E_C_N_O_ "
cQuery += "  FROM MHQT10"
cQuery += " WHERE MHQ_STATUS = '3'"

If !Empty(cChave)
    cQuery += "   AND MHQ_CHVUNI = '" + cChave + "'"
EndIf

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cAlias, .T., .F.)

If !(cAlias)->( Eof() )

    While !(cAlias)->( Eof() )
        MHQ->(dbGoto((cAlias)->R_E_C_N_O_))

        RecLock("MHQ", .F.)
        MHQ->MHQ_STATUS := "0"
        MHQ->( MsUnLock() )        

        (cAlias)->( DbSkip() )
    EndDo
    (cAlias)->( DbCloseArea() )

Else
    LjGrvLog("RMIGRVMSGPUBOBJ", "N�o foi encontrada nenhuma publica��o para reprocessar!", "Query: " + cQuery)    
EndIf

Return Nil

//--------------------------------------------------------
/*/{Protheus.doc} ExecParseXml
Executa o parse do XML e devolve um objeto

@author  	Bruno Almeida
@version 	1.0
@since      28/05/2020
@return	    
/*/
//--------------------------------------------------------
Method ExecParseXml(cMsgOriginal) Class RmiGrvMsgPubObj

Local oRet      := Nil //Objeto de retorno
Local cError    := "" //Grava o erro do parse do XML
Local cWarning  := "" //Grava os warnings do parse do XML
Local oParse    := Nil //Objeto para realiza��o do parse do XML

Default cMsgOriginal := ""

If !Empty(cMsgOriginal)
    
    cMsgOriginal := '<LC_CupomFiscal>' + cMsgOriginal + '</LC_CupomFiscal>'
    oParse := XmlParser(cMsgOriginal, "_", @cError, @cWarning)
    
    If Empty(cError)
        oRet := oParse:_LC_CupomFiscal
    Else
        Self:lSucesso := .F.
        Self:cErro := STR0002 + cError //"Erro ao realizar o XMLParser - "
    EndIf
Else
    Self:lSucesso := .F.
    Self:cErro := STR0003 //"O par�metro cMsgOriginal esta vazio e n�o vai ser poss�vel executar o XMLParser"
EndIf

Return oRet