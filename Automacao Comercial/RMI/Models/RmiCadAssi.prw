#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "RMIASSINANTE.CH"

Static cBkpLayFil := ""

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiCadAssi
Cadastro de Assinantes

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiCadAssi()

	Local oBrowse := Nil

    If AmIIn(12)// Acesso apenas para modulo e licen�a do Varejo
        oBrowse := FWMBrowse():New()
        oBrowse:SetDescription(STR0001)    //"Assinantes"
        oBrowse:SetAlias("MHO")
        oBrowse:SetLocate()
        oBrowse:Activate()
    else
        MSGALERT(STR0011)// "Esta rotina deve ser executada somente pelo m�dulo 12 (Controle de Lojas)"
    EndIf
Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

	Local aRotina := {}
	
	aAdd(aRotina, { STR0002, "PesqBrw"             , 0, 1, 0, .T. } ) //"Pesquisar"
	aAdd(aRotina, { STR0003, "VIEWDEF.RMICADASSI", 0, 2, 0, NIL } ) //"Visualizar"
	aAdd(aRotina, { STR0004, "VIEWDEF.RMICADASSI", 0, 3, 0, NIL } ) //"Incluir"
	aAdd(aRotina, { STR0005, "VIEWDEF.RMICADASSI", 0, 4, 0, NIL } ) //"Alterar"
	aAdd(aRotina, { STR0006, "VIEWDEF.RMICADASSI", 0, 5, 0, NIL } ) //"Excluir"
	aAdd(aRotina, { STR0007, "VIEWDEF.RMICADASSI", 0, 8, 0, NIL } ) //"Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Base da Decis�o

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

	Local oView      := Nil
	Local oModel     := FWLoadModel("RMICADASSI")
	Local oStructMHO := FWFormStruct(2, "MHO")
    Local oStructMHP := FWFormStruct(2, "MHP")

    oStructMHP:RemoveField("MHP_CASSIN")
    oStructMHP:RemoveField("MHP_LAYFIL")
	
	oView := FWFormView():New()
	oView:SetModel(oModel)
	oView:SetDescription(STR0001)    //"Assinantes"

	oView:AddField("RMICADASSI_FIELD_MHO", oStructMHO, "MHOMASTER")
	oView:AddGrid("RMICADASSI_GRID_MHP"  , oStructMHP, "MHPDETAIL")

	oView:CreateHorizontalBox("FORMFIELD", 40)
    oView:CreateHorizontalBox("FORMGRID" , 60)

	oView:SetOwnerView("RMICADASSI_FIELD_MHO", "FORMFIELD")
    oView:SetOwnerView("RMICADASSI_GRID_MHP" , "FORMGRID" )

    oView:EnableTitleView("RMICADASSI_FIELD_MHO", STR0001)    //"Assinantes"
    oView:EnableTitleView("RMICADASSI_GRID_MHP" , STR0008)    //"Assinantes x Processos"

	oView:EnableControlBar(.T.)

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Base da Decis�o

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0

@obs MHOMASTER - Assinantes
/*/
//-------------------------------------------------------------------
Static Function Modeldef()

	Local oModel     := Nil
	Local oStructMHO := FWFormStruct(1, "MHO")
    Local oStructMHP := FWFormStruct(1, "MHP")
	
	//----------------------------------------- 
	//Monta o modelo do formul�rio
	//-----------------------------------------
	oModel:= MPFormModel():New("RMICADASSI", /*Pre-Validacao*/, /*Pos-Validacao*/, { |oModel| RmiCommit(oModel)}, { || RmiCancMdl()})
    oModel:SetDescription(STR0009)  //"Modelo de dados dos Assinantes"

	oModel:AddFields("MHOMASTER", Nil, oStructMHO, /*Pre-Validacao*/, /*Pos-Validacao*/)
    oModel:GetModel("MHOMASTER"):SetDescription(STR0001) 	//"Assinantes"

    oModel:AddGrid("MHPDETAIL", "MHOMASTER", oStructMHP, /*bLinePre*/, {|oModelMHP, nLinha| ValLinMHP(oModelMHP, nLinha)}, /*bPre*/, /*bPost*/)
	oModel:GetModel("MHPDETAIL"):SetDescription(STR0008)    //"Assinantes x Processos"
	oModel:SetRelation("MHPDETAIL", { { "MHP_FILIAL", "MHO_FILIAL" }, { "MHP_CASSIN", "MHO_COD" } }, MHP->( IndexKey(1) ))    //MHP_FILIAL+MHP_CASSIN+MHP_CPROCE
	oModel:GetModel("MHPDETAIL"):SetUniqueLine( {"MHP_CPROCE"} )

    oModel:SetActivate( {|oModel| Carga(oModel)} )

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} Carga
Rotina que ira efetuar a carga dos processos padr�es na inclus�o de um 
novo Assinante.

@author  Rafael Tenorio da Costa
@since   29/10/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Carga(oModel)

    Local aArea      := GetArea()
    Local aProcessos := {}
    Local nProc      := 1
    Local lRetorno   := .T.

    If oModel:GetOperation() == MODEL_OPERATION_INSERT

        oModelMHP  := oModel:GetModel("MHPDETAIL")

        aProcessos := RmiXSql("SELECT MHN_COD FROM " + RetSqlName("MHN") + " WHERE MHN_FILIAL = '" + xFilial("MHN") + "' AND D_E_L_E_T_ = ' '", "*", /*lCommit*/, /*aReplace*/)

        For nProc:=1 To Len(aProcessos)

            If nProc > 1
                lRetorno := oModelMHP:AddLine() >= nProc
            EndIf
            
            If lRetorno
                oModelMHP:LoadValue("MHP_CPROCE", aProcessos[nProc][1])
                oModelMHP:LoadValue("MHP_ATIVO" , "2")                      //1=Sim,2=N�o
            Else
                Exit
            EndIf
        Next nProc

        aSize(aProcessos, 0)
    EndIf

    RestArea(aArea)

Return lRetorno

//------------------------------------------------------------------
/*/{Protheus.doc} ValLinMHP
Valida linha do grid MHP - Assinantes x Processos

@author Rafael Tenorio da Costa
@since  09/06/2016
/*/
//-------------------------------------------------------------------
Static Function ValLinMHP(oModelMHP, nLinha)
	
    Local oModel    := FwModelActive()
	Local lRetorno  := .T.
    Local cFilPro   := ""
    Local aFilPro   := {}
    Local nCont     := 0

    //Valida se a linha n�o esta deletada
    If !oModelMHP:IsDeleted(nLinha)

        cFilPro := AllTrim( oModelMHP:GetValue("MHP_FILPRO", nLinha) )

        If SubStr(cFilPro, Len(cFilPro)) <> ";"
            cFilPro := cFilPro + ";"
        EndIf            

        aFilPro := StrToKarr(cFilPro, ";")

        For nCont:=1 To Len(aFilPro)

            If !FwFilExist(/*cGrpCompany*/, aFilPro[nCont])
                lRetorno := .F.
                oModel:SetErrorMessage("MHPDETAIL", "MHP_FILPRO", "MHPDETAIL", "MHP_FILPRO", AllTrim( GetSX3Cache("MHP_FILPRO", "X3_TITULO") ), I18n(STR0010, {aFilPro[nCont]}), "")    //"Filial (#1) inv�lida, verifique."
            EndIf

		Next nCont

        If lRetorno .And. Len(aFilPro) > 0
            oModelMHP:LoadValue("MHP_FILPRO", cFilPro)
        EndIf
    EndIf        
	
Return lRetorno


//------------------------------------------------------------------
/*/{Protheus.doc} GatMHP
Gatilho do objeto MHP

@author Danilo Rodrigues 
@since  27/04/2020
/*/
//-------------------------------------------------------------------
Function GatMHP(cFilVenda, cAssinante, cProcesso, lGatilho)

Local aFilPro   := {} //Guarda todas as filiais
Local cCRLF    	:= Chr(13) + Chr(10) //Pula linha 
Local cLayFil   := "" //Monta o Json
Local nCont     := 0 //Variavel de loop
Local oModel    := Nil //Recebe o model da tela
Local oJsonFil  := Nil //Json com as filiais
Local nI        := 0 //Variavel de loop
Local nPos      := 0 //Posicao da filial no Json

Default cAssinante := ""
Default cProcesso  := "" 
Default lGatilho   := .F.

If lGatilho
    oModel := FwModelActive()
    cFilVenda := oModel:GetValue('MHPDETAIL','MHP_FILPRO')
    cAssinante := AllTrim(oModel:GetValue('MHPDETAIL','MHP_CASSIN'))
    cProcesso := AllTrim(oModel:GetValue('MHPDETAIL','MHP_CPROCE'))
    If Empty(cBkpLayFil)
        cBkpLayFil := oModel:GetValue('MHPDETAIL','MHP_LAYFIL')
    EndIf
EndIf

If SubStr(Alltrim(cFilVenda), Len(Alltrim(cFilVenda))) == ";"
    cFilVenda := SubStr(Alltrim(cFilVenda), 1,Len(Alltrim(cFilVenda))-1)
EndIf
    
aFilPro := StrTokArr2(cFilVenda, ";")

If !Empty(cFilVenda) .AND. cAssinante == "CHEF" .AND. cProcesso == "VENDA"
    
    If Len(aFilPro) > 0
        cLayFil := '{"Filiais":['  + cCRLF 
    EndIf

    If !lGatilho

        For nCont := 1 To Len(aFilPro)

            cLayFil += '{'
            cLayFil += '"Filial":' + '"' + aFilPro[nCont] + '",' + cCRLF
            cLayFil += '"Data":' + ' " ",' + cCRLF
            cLayFil += '"Hora":' + ' " "' + cCRLF

            If nCont < Len(aFilPro) 
                cLayFil += '},' + cCRLF
            Else    
                cLayFil += '}' + cCRLF
            EndIf

        Next nCont

    Else

        oJsonFil := JsonObject():New()
        oJsonFil:FromJson(cBkpLayFil)

        For nCont := 1 To Len(aFilPro) 

            nPos := 0

            For nI := 1 To Len( oJsonFil["Filiais"] )
                If oJsonFil["Filiais"][nI]["Filial"] == aFilPro[nCont]
                    nPos := nI
                    Exit
                EndIF
            Next nI
            
            cLayFil += '{'
            cLayFil += '"Filial":' + '"' + aFilPro[nCont] + '",' + cCRLF
            cLayFil += '"Data":' + ' "' + IIF(nPos > 0, oJsonFil["Filiais"][nPos]["Data"]," ") + '",' + cCRLF
            cLayFil += '"Hora":' + ' "' + IIF(nPos > 0, oJsonFil["Filiais"][nPos]["Hora"]," ") + '"' + cCRLF

            If nCont < Len(aFilPro) 
                cLayFil += '},' + cCRLF
            Else    
                cLayFil += '}' + cCRLF
            EndIf

        Next nCont
        
    EndIf
    
    If Len(aFilPro) > 0
        cLayFil += ']}'
    EndIf

EndIf

Return cLayFil

//------------------------------------------------------------------
/*/{Protheus.doc} RmiCommit
Fun��o para Commit do modelo

@author Bruno Almeida
@since  08/05/2020
/*/
//-------------------------------------------------------------------
Static Function RmiCommit(oModel)

If FWFormCommit( oModel )
    cBkpLayFil := ""
EndIf

Return .T.


//------------------------------------------------------------------
/*/{Protheus.doc} RmiCancMdl
Fun��o para cancel do modelo

@author Bruno Almeida
@since  08/05/2020
/*/
//-------------------------------------------------------------------
Static Function RmiCancMdl()

cBkpLayFil := ""

Return .T.