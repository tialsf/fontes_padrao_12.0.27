#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "RMIPROCESSO.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiCadProc
Processos

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiCadProc()

	Local oBrowse := Nil
    If AmIIn(12)// Acesso apenas para modulo e licen�a do Varejo
        //Carrega registros padr�es
        Processa( {|| Carga()}, STR0010, STR0011 )   //"Carregando Processos Padr�es"   //"Aguarde. . ."
        
        oBrowse := FWMBrowse():New()
        
        oBrowse:SetDescription(STR0001)   //"Processos"
        oBrowse:SetAlias("MHN")
        oBrowse:SetLocate()
        oBrowse:Activate()
    else
        MSGALERT(STR0020)// "Esta rotina deve ser executada somente pelo m�dulo 12 (Controle de Lojas)"
    EndIf
Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menu Funcional

@return aRotina - Estrutura
[n,1] Nome a aparecer no cabecalho
[n,2] Nome da Rotina associada
[n,3] Reservado
[n,4] Tipo de Transa��o a ser efetuada:
1 - Pesquisa e Posiciona em um Banco de Dados
2 - Simplesmente Mostra os Campos
3 - Inclui registros no Bancos de Dados
4 - Altera o registro corrente
5 - Remove o registro corrente do Banco de Dados
6 - Altera��o sem inclus�o de registros
7 - C�pia
8 - Imprimir
[n,5] Nivel de acesso
[n,6] Habilita Menu Funcional

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

	Local aRotina := {}
	
	aAdd( aRotina, { STR0002, "PesqBrw"           , 0, 1, 0, .T. } ) //"Pesquisar"
	aAdd( aRotina, { STR0003, "VIEWDEF.RMICADPROC", 0, 2, 0, NIL } ) //"Visualizar"
	aAdd( aRotina, { STR0004, "VIEWDEF.RMICADPROC", 0, 3, 0, NIL } ) //"Incluir"
	aAdd( aRotina, { STR0005, "VIEWDEF.RMICADPROC", 0, 4, 0, NIL } ) //"Alterar"
	aAdd( aRotina, { STR0006, "VIEWDEF.RMICADPROC", 0, 5, 0, NIL } ) //"Excluir"
	aAdd( aRotina, { STR0007, "VIEWDEF.RMICADPROC", 0, 8, 0, NIL } ) //"Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de dados de Base da Decis�o

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

	Local oView      := Nil
	Local oModel     := FWLoadModel("RMICADPROC")
	Local oStructMHN := FWFormStruct(2, "MHN")
    Local oStructMHS := FWFormStruct(2, "MHS")

    oStructMHS:RemoveField("MHS_CPROCE")
	
	oView := FWFormView():New()
	oView:SetModel( oModel )
	oView:SetDescription(STR0001)   //"Processos"

	oView:AddField("RMIASSINANTE_FIELD_MHN", oStructMHN, "MHNMASTER")
	oView:AddGrid( "RMIASSINANTE_GRID_MHS" , oStructMHS, "MHSDETAIL")

	oView:CreateHorizontalBox("FORMFIELD", 40)
    oView:CreateHorizontalBox("FORMGRID" , 60)

	oView:SetOwnerView("RMIASSINANTE_FIELD_MHN", "FORMFIELD")
    oView:SetOwnerView("RMIASSINANTE_GRID_MHS" , "FORMGRID" )

    oView:EnableTitleView("RMIASSINANTE_FIELD_MHN", STR0001)    //"Processos"
    oView:EnableTitleView("RMIASSINANTE_GRID_MHS" , STR0008)    //"Tabelas Secund�rias"

	oView:EnableControlBar(.T.)

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados de Base da Decis�o

@author  Rafael Tenorio da Costa
@since   24/09/19
@version 1.0

@obs MHNMASTER - Processos
/*/
//-------------------------------------------------------------------
Static Function Modeldef()

	Local oModel     := Nil
	Local oStructMHN := FWFormStruct(1, "MHN")
    Local oStructMHS := FWFormStruct(1, "MHS")
	
	//-----------------------------------------
	//Monta o modelo do formul�rio
	//-----------------------------------------
	oModel:= MPFormModel():New( "RMICADPROC", /*Pre-Validacao*/, {|oModel| RmiVldCmp(oModel)}, /*Commit*/, /*Cancel*/)
	oModel:SetDescription( STR0009 )    //"Modelo de Processo"

	oModel:AddFields( "MHNMASTER", NIL, oStructMHN, /*Pre-Validacao*/, /*Pos-Validacao*/ )
	oModel:GetModel( "MHNMASTER" ):SetDescription( STR0001 )    //"Processos"

    oModel:AddGrid("MHSDETAIL", "MHNMASTER", oStructMHS, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bPost*/)
	oModel:GetModel("MHSDETAIL"):SetDescription(STR0008)  //"Tabelas Secund�rias"
    oModel:GetModel("MHSDETAIL"):SetUniqueLine( {"MHS_TABELA"} )    
	oModel:SetRelation("MHSDETAIL", { { "MHS_FILIAL", "MHN_FILIAL" }, { "MHS_CPROCE", "MHN_COD" } }, MHS->( IndexKey(1) ))  //MHS_FILIAL+MHS_CPROCE+MHS_TABELA
    oModel:SetOptional("MHSDETAIL", .T.)    

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} Carga
Rotina que ira efetuar a carga inicial caso n�o exist�o registros na tabela.

@author  Rafael Tenorio da Costa
@since   04/10/19
@version 1.0

/*/
//-------------------------------------------------------------------
Static Function Carga()

    Local aArea      := GetArea()
    Local aProcessos := {}
    Local aTabSecund := {}
    Local nProc      := 1
    Local nTabSec    := 1
  
    aTabSecund := {}
    Aadd(aProcessos, {"PRODUTO", "SB1", "B1_FILIAL+B1_COD", aTabSecund})

    aTabSecund := {}
    Aadd(aProcessos, {"CLIENTE", "SA1", "A1_FILIAL+A1_COD", aTabSecund})

    aTabSecund := {}
    Aadd(aTabSecund, {"DA1"    , "DA1_FILIAL+DA1_CODTAB"})
    Aadd(aProcessos, {"PRE�O"  , "DA0", "DA0_FILIAL+DA0_CODTAB" ,aClone(aTabSecund) } )

    aTabSecund := {}
    Aadd(aTabSecund, {"SL2", "L2_FILIAL+L2_NUM"})
    Aadd(aTabSecund, {"SL4", "L4_FILIAL+L4_NUM"})                      	
    Aadd(aProcessos, {"VENDA"  , "SL1", "L1_FILIAL+L1_NUM" ,aClone(aTabSecund)})
    
    aTabSecund := {}
    Aadd(aProcessos, {"NCM","SYD","YD_FILIAL+YD_TEC",aTabSecund})
    
    aTabSecund := {}
    Aadd(aProcessos, {"CATEGORIA","ACU","ACU_FILIAL+ACU_COD",aTabSecund})

    aTabSecund := {}
    Aadd(aProcessos, {"UN MEDIDA","SAH","AH_FILIAL+AH_UNIMED",aTabSecund})

    aTabSecund := {}
    Aadd(aProcessos, {"CEST","F0G","F0G_FILIAL+F0G_CEST",aTabSecund})
    
    aTabSecund := {}
    Aadd(aProcessos, {"OPERADOR CAIXA","SA6","A6_FILIAL+A6_COD",aTabSecund})

    aTabSecund := {}
    Aadd(aProcessos, {"INVENTARIO","SB7","B7_FILIAL+B7_DATA+B7_COD+B7_LOCAL+B7_LOCALIZ+B7_NUMSERI+B7_LOTECTL+B7_NUMLOTE+B7_CONTAGE",aTabSecund})
    
    aTabSecund := {}
    Aadd(aProcessos, {"IMPOSTO PROD","XXX","PROCESSO EXCLUSIVO DO SISTEMA",aTabSecund})

    Aadd(aTabSecund, {"MEN", "MEN_FILIAL+MEN_CODADM"})
    Aadd(aProcessos, {"ADMINISTRADORA","SAE","AE_FILIAL+AE_COD",aTabSecund})
    
    aTabSecund := {}
    Aadd(aProcessos, {"CAIXA SANGRIA","SE5","E5_FILIAL+E5_DATA+ E5_BANCO+E5_AGENCIA+E5_CONTA+E5_NUMCHEQ",aTabSecund})
    
    
    DbSelectArea("MHN")
    MHN->(DBSetOrder(2))
    ProcRegua(3)
    
    Begin Transaction

        For nProc:=1 To Len(aProcessos)

            IncProc()
            
            If !MHN->(Dbseek(xFilial("MHN")+PadR(aProcessos[nProc][2], TamSx3('MHN_TABELA')[1])))
                RecLock("MHN", .T.)
                    MHN->MHN_FILIAL := xFilial("MHN")
                    MHN->MHN_COD    := aProcessos[nProc][1]
                    MHN->MHN_TABELA := aProcessos[nProc][2]
                    MHN->MHN_CHAVE  := aProcessos[nProc][3]
                MHN->( MsUnLock() )
                //Inclui Tabelas Secund�rias
                aTabSecund := aProcessos[nProc][4]

                For nTabSec:=1 To Len(aTabSecund)

                    RecLock("MHS", .T.)
                        MHS->MHS_FILIAL := MHN->MHN_FILIAL
                        MHS->MHS_CPROCE := MHN->MHN_COD
                        MHS->MHS_TABELA := aTabSecund[nTabSec][1]
                        MHS->MHS_CHAVE  := aTabSecund[nTabSec][2]
                    MHS->( MsUnLock() )
                Next nTabSec
            EndIf
        Next nProc

    End Transaction
        


    aSize(aProcessos, 0)
    aSize(aTabSecund, 0)

    RestArea(aArea)

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} RmiVldCmp
Rotina que ira efetuar a valida��o de todos os campos chaves, tanto
do cabe�alho quanto das tabelas secundarias

@author  Bruno Almeida
@since   13/11/2019
@version 1.0

/*/
//-------------------------------------------------------------------
Function RmiVldCmp(oModel)

Local lRet      := .T. //Variavel de retorno
Local oCab      := oModel:GetModel('MHNMASTER') //Model do cabecalho
Local oItens    := oModel:GetModel('MHSDETAIL') //Model dos itens
Local nX        := 0 //Variavel de loop
Local nI        := 0 //Variavel de loop
Local aCab      := {} //Campos do cabecalho
Local aItens    := {} //Campos de itens
Local cTabela   := "" //Tabela
Local nOperation:= oModel:GetOperation() //Operacao executada no modelo de dados.

If nOperation == MODEL_OPERATION_DELETE .OR. nOperation == MODEL_OPERATION_UPDATE 
    If Alltrim(oCab:GetValue('MHN_TABELA')) == "XXX" .OR. Alltrim(oCab:GetValue('MHN_COD')) == "IMPOSTO PROD"
        lRet := .F.
        Help( ,, 'HELP',, oCab:GetValue('MHN_CHAVE'), 1, 0)//"� necessario preencher o campo Chave da tabela "
    EndIf
EndIf

If nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE .AND. lRet 

    //Valida os campos do cabecalho
    If !Empty(oCab:GetValue('MHN_CHAVE')) .AND. !Empty(oCab:GetValue('MHN_TABELA'))

        aCab    := Separa(oCab:GetValue('MHN_CHAVE'),'+')
        cTabela := oCab:GetValue('MHN_TABELA')

        For nX := 1 To Len(aCab)
            If (cTabela)->(FieldPos(aCab[nX])) == 0
                lRet := .F.
                MsgAlert(STR0012 + AllTrim(aCab[nX]) + STR0013 + cTabela) //"O campo " # " n�o existe na tabela "
                Exit
            EndIf

        Next nX

    EndIf

    //Valida os campos das tabelas secundarias
    If lRet

        For nX := 1 To oItens:Length()
            oItens:GoLine(nX)
            If !Empty(oItens:GetValue('MHS_TABELA'))
                If !Empty(oCab:GetValue('MHN_CHAVE'))
                    If !Empty(oItens:GetValue('MHS_CHAVE'))
                        aItens  := Separa(oItens:GetValue('MHS_CHAVE'),'+')
                        cTabela := oItens:GetValue('MHS_TABELA')

                        For nI := 1 To Len(aItens)
                            If (cTabela)->(FieldPos(aItens[nI])) == 0
                                lRet := .F.
                                Help( ,, 'HELP',, STR0012 + AllTrim(aItens[nI]) + STR0013 + cTabela, 1, 0)//"O campo " # " n�o existe na tabela "
                                Exit
                            EndIf

                        Next nX
                    Else
                        lRet := .F.
                        Help( ,, 'HELP',, STR0014 + oItens:GetValue('MHS_TABELA'), 1, 0)//"� necessario preencher o campo Chave da tabela "
                    EndIf
                Else
                    lRet := .F.
                    Help( ,, 'HELP',, STR0014 + oCab:GetValue('MHN_TABELA'), 1, 0)//"� necessario preencher o campo Chave da tabela "
                EndIf

                If !lRet
                    Exit
                EndIf

            EndIf

        Next nX

    EndIf

    lRet := RmiVldFilt(oCab, oItens)

EndIf

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} RmiVldFilt
Essa fun��o tem o objetivo de validar os campos que foram digitados no 
filtro.

@author  Bruno Almeida
@since   02/07/2020
@version 1.0

/*/
//-------------------------------------------------------------------
Function RmiVldFilt(oCab, oItens)

Local lRet      := .T.  //Variavel de retorno
Local aFiltro   := {}   //Recebe os campos que foram digitados no filtro
Local nI        := 0    //Variavel de loop
Local nX        := 0    //Variavel de loop
Local cTags     := "AND|OR|=|D_E_L_E_T_|R_E_C_N_O_|R_E_C_D_E_L_" //Tags que n�o fazem parte da compara��o

Default oCab    := Nil
Default oItens  := Nil

If ValType(oCab) == "O" .AND. ValType(oItens) == "O"
    If !Empty(oCab:GetValue('MHN_FILTRO'))
        //Transforma em um array o conteudo do filtro
        aFiltro := Separa(AllTrim(oCab:GetValue('MHN_FILTRO'))," ")

        //Caso o array aFiltro n�o seja no minimo um tamanho de 3 posi��es,
        //significa que o filtro n�o � valido.
        If Len(aFiltro) >= 3
            If !(AllTrim(aFiltro[1]) $ 'AND|OR')
                //Percorre cada uma das posi��es do array para validar todos os campos
                For nI := 1 To Len(aFiltro)

                    //Caso a posi��o do array seja um espa�o em branco ou algumas das palavras listados no cont�m, n�o � necessario validar
                    If !Empty(aFiltro[nI]) .AND. !(AllTrim(aFiltro[nI]) $ cTags)

                        //Pega o conteudo do array e tenta verificar se � um campo
                        If (AllTrim(SubStr(aFiltro[nI],4,1)) == "_" ) .OR. (AllTrim(SubStr(aFiltro[nI],3,1)) == "_")
                            
                            If (oCab:GetValue('MHN_TABELA'))->(ColumnPos(aFiltro[nI])) == 0
                                lRet := .F.
                                Help( ,, 'HELP',, STR0012 + AllTrim(aFiltro[nI]) + STR0013 + AllTrim(oCab:GetValue('MHN_TABELA')), 1, 0,,,,,,{STR0015 + AllTrim(oCab:GetValue('MHN_FILTRO'))})//"O campo " # " n�o existe na tabela " # "Por favor, corrija o filtro -> "
                                Exit
                            EndIf

                        EndIf
                    EndIf
                Next nI
            Else
                lRet := .F.
                Help( ,, 'HELP',, STR0018 + AllTrim(oCab:GetValue('MHN_TABELA')) + STR0019, 1, 0,,,,,,{STR0015 + AllTrim(oCab:GetValue('MHN_FILTRO'))})//"O filtro da tabela " # " n�o pode iniciar com as palavras AND ou OR." # "Por favor, corrija o filtro -> "
            EndIf
        Else
            lRet := .F.
            Help( ,, 'HELP',, STR0016 + AllTrim(oCab:GetValue('MHN_TABELA')) + STR0017, 1, 0,,,,,,{STR0015 + AllTrim(oCab:GetValue('MHN_FILTRO'))}) //"O filtro informado para a tabela " # " n�o � valido." # "Por favor, corrija o filtro -> "
        EndIf
    EndIf

    If lRet
        //L� cada linha do grid
        For nX := 1 To oItens:Length()
            oItens:GoLine(nX)
            
            If !Empty(oItens:GetValue('MHS_FILTRO'))

                //Transforma em um array o conteudo do filtro
                aFiltro := Separa(AllTrim(oItens:GetValue('MHS_FILTRO'))," ")
                
                //Para o filtro, deve-se haver pelo menos tr�s posi��es 
                //para considerar um filtro valido
                If Len(aFiltro) >= 3
                    If !(AllTrim(aFiltro[1]) $ 'AND|OR')

                        //Loop para percorrer cada uma das posi��es
                        For nI := 1 To Len(aFiltro)

                            //Caso n�o seja nenhuma das palavras abaixo, ent�o entra no IF
                            If !Empty(aFiltro[nI]) .AND. !(AllTrim(aFiltro[nI]) $ cTags)

                                If (AllTrim(SubStr(aFiltro[nI],4,1)) == "_") .OR. (AllTrim(SubStr(aFiltro[nI],3,1)) == "_")

                                    If (oItens:GetValue('MHS_TABELA'))->(ColumnPos(aFiltro[nI])) == 0
                                        lRet := .F.
                                        Help( ,, 'HELP',, STR0012 + AllTrim(aFiltro[nI]) + STR0013 + AllTrim(oItens:GetValue('MHS_TABELA')), 1, 0,,,,,,{STR0015 + AllTrim(oItens:GetValue('MHS_FILTRO'))})//"O campo " # " n�o existe na tabela " # "Por favor, corrija o filtro -> "
                                        Exit
                                    EndIf
                                EndIf
                            EndIf
                        Next nI
                    Else
                        lRet := .F.
                        Help( ,, 'HELP',, STR0018 + AllTrim(oItens:GetValue('MHS_TABELA')) + STR0019, 1, 0,,,,,,{STR0015 + AllTrim(oItens:GetValue('MHS_FILTRO'))})//"O filtro da tabela " # " n�o pode iniciar com as palavras AND ou OR." # "Por favor, corrija o filtro -> "
                    EndIf
                Else
                    lRet := .F.
                    Help( ,, 'HELP',, STR0016 + AllTrim(oItens:GetValue('MHS_TABELA')) + STR0017, 1, 0,,,,,,{STR0015 + AllTrim(oItens:GetValue('MHS_FILTRO'))}) //"O filtro informado para a tabela " # " n�o � valido." # "Por favor, corrija o filtro -> "
                EndIf
                If !lRet
                    Exit
                EndIf
            EndIf
        Next nX
    EndIf

EndIf

Return lRet
