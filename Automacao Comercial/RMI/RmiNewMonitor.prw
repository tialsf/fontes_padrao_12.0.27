#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMINEWMONI.CH"
#INCLUDE "FWBROWSE.CH"  

//Posicao do ListBox
#DEFINE PLEG		1			//Legenda - Vermelho = Erro | Amarelo = Erro por dependencia de outro cadastro | Azul = Erro de Integracacao com o ERP
#DEFINE PASSIN		2			//Assinantes
#DEFINE PETAPA		3			//Etapa
#DEFINE PROCES		4			//Processos
#DEFINE PDPROC		5			//Pendente de Processamento
#DEFINE PROCEOK		6			//Processados com sucesso
#DEFINE PROERRO		7			//Tabela Processada
#DEFINE PREPROC     8           //Pendente Reprocessamento

#DEFINE CETAPA      2
#DEFINE CALIAS		3			//Assinantes
#DEFINE CDATA		4			//Etapa
#DEFINE HORA		5			//Processos
#DEFINE ERROR		6			//Pendente de Processamento
#DEFINE INDICE		7			//Processados com sucesso
#DEFINE CHAVE		8			//Tabela Processada

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiNewMoni
Monitor de integra��o utilizado para apresenta��o das inconsistencias
apresentadas nas tabelas MHQ, MHR, SL1 e SLX

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiNewMoni()

    //Variaveis da Tela Main
    Local oLayer    := Nil
    Local aButtons 	:= {}
    Local oDlg      := Nil
    Local aCoors    := {} 
    Local oPnUp     := Nil
    Local oPnCenter := Nil
    Local oPnDown   := Nil
    Local oPnbotao  := Nil
    Local oPnColR   := Nil
    Local oPnColD   := Nil
    Local oPnColC   := Nil
    Local oPnColB   := Nil
    Local oColumn   := Nil
    Local oFont     := Nil
    Local oSay      := Nil

    //Variaveis dos GET
    Local oDtIni    := Nil
    Local oDtFim    := Nil
    Local dDtIni    := dDataBase
    Local dDtFim    := dDataBase

    //Variaveis do List dos Itens Consultados
    Local oLstMnt   := Nil
    Local aLstMnt	:= {}
    Local aCabMnt	:= {" ",STR0001, STR0002, STR0003, STR0004, STR0005, STR0006, STR0027} //"Assinantes", "Etapa", "Processos", "Pendentes de Processamento", "Processados", "Erro no Processsamento", "Aguardando Reprocessamento"

    //Variaveis do List das Integracoes
    Local aLstInt	:= {}
    
    //Variaveis do List dos Itens detalhes Consultados
    Local oLstMnt2   := Nil
    Local aLstMnt2	:= {}
    Local aCabMnt2	:= {" ",STR0002,STR0018, STR0019, STR0020, STR0021,STR0022, STR0023} //" ","Tabela", "Data Processamento", "Hora Processamento", "Descri��o do Erro", "Indice da Tabela", "Chave de Busca"
    If !AmIIn(12)// Acesso apenas para modulo e licen�a do Varejo
        MSGALERT(STR0026)// "Esta rotina deve ser executada somente pelo m�dulo 12 (Controle de Lojas)"
        Return
    EndIf    
    
    If ValTables()

        oLayer := FWLayer():new()
        aCoors := MsAdvSize() 

        DEFINE MSDIALOG oDlg TITLE STR0011 FROM aCoors[7],aCoors[1] TO aCoors[6],aCoors[5] PIXEL    //"Monitor de Integra��o"

        oLayer:Init(oDlg,.F.)
        oLayer:oPanel:Align := CONTROL_ALIGN_ALLCLIENT
        
        oLayer:AddLine('UP'    , 010, .F.)
        oLayer:AddLine('CENTER', 035, .F.)
        oLayer:AddLine('BOTAO', 05, .F.)
        oLayer:AddLine('DOWN', 043, .F.)
        
        oLayer:addCollumn('RIGHT' , 35, .F., 'UP')
        oLayer:addCollumn('CENTER', 40, .F., 'UP')
        oLayer:addCollumn('BOTAO', 05, .F., 'CENTER')
        oLayer:addCollumn('DOWN'  , 40, .F., 'BOTAO')
        
        oPnUp	    := oLayer:GetLinePanel('UP'	    )
        oPnCenter	:= oLayer:GetLinePanel('CENTER' )
        oPnbotao	:= oLayer:GetLinePanel('BOTAO' )
        oPnDown     := oLayer:GetLinePanel('DOWN' )
        
        oPnColR:= oLayer:GetColPanel('RIGHT' , 'UP')
        oPnColC:= oLayer:GetColPanel('CENTER', 'UP')
        oPnColB:= oLayer:GetColPanel('BOTAO' , 'CENTER')
        oPnColD:= oLayer:GetColPanel('DOWN', 'BOTAO')
        
        //Layer Superior
        TSay():New(010, 005,{|| STR0012 },oPnColR,,,.F.,.F.,.F.,.T.,,,200,008)  //"Per�odo"
        oDtIni	:= tGet():New(008,030,{|u| Iif( PCount() > 0, dDtIni := u, dDtIni)},oPnColR,40,,"@D",,,,,,,.T.,,,,,,,,,,"dDtIni")
                        
        TSay():New(010, 075,{|| STR0013 },oPnColR,,,.F.,.F.,.F.,.T.,,,200,008)  //"a"
        oDtFim	:= tGet():New(008,083,{|u| Iif( PCount() > 0, dDtFim := u, dDtFim)},oPnColR,40,,"@D",,,,,,,.T.,,,,,,,,,,"dDtFim")

        TButton():New( 008, 130, STR0014,oPnColR,{|| Processa( {|| oLstMnt2:SetArray(aLstMnt2:={}),oLstMnt2:Refresh(),aLstMnt := RmiConGeral(dDtIni, dDtFim), oLstMnt:SetArray(aLstMnt), oLstMnt:Refresh() } ) }, 40,12,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Pesquisar"
        
        
        aAdd(aButtons, {"USER", {|| Processa( {|| GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt := RmiConGeral(dDtIni, dDtFim), oLstMnt:SetArray(aLstMnt), oLstMnt:Refresh() } ) } , STR0016    } ) //"Atualiza"
        aAdd(aButtons, {"USER", {|| IIF(LEN(aLstMnt) > 0 ,Processa( {|| Reprocessa(oLstMnt,aLstMnt,dDtIni,dDtFim),aLstMnt := RmiConGeral(dDtIni, dDtFim), oLstMnt:SetArray(aLstMnt),oLstMnt:Refresh() } ),.T.) } , STR0028  } ) // "Reprocessar"
        aAdd(aButtons, {"USER", {|| BtLegenda()} , STR0015    } ) //"Legenda"

        TButton():New( 002, 005, STR0025,oPnbotao,{|| Processa( {||aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),; //bot�o da gride detalhe do erro
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()}) }, 60,16,,,.F.,.T.,.F.,,.F.,,,.F. ) //"Atualiza"   
        
        TButton():New( 002, 075, STR0024,oPnbotao,{|| Processa( {||DlgToExcel({{"ARRAY","MONITOR_" + DtoS( Date() ) + StrTran( Time(), ":", ""),aCabMnt2,aLstMnt2} })} ) },;
        60,16,,,.F.,.T.,.F.,,.F.,,,.F. ) // "Exportar Dados" para excel.

        oFont := TFont():New('Courier new',,-18,.T.,.T.,,,,,.T.)//define o tamanho do fonte no titulo do detalhe 
        oSay:= TSay():New(002, 275,{|| "" },oPnbotao,,oFont,.F.,.F.,.F.,.T.,,,200,008)//define a posi��o do texto no detalhe do erro

        //Layer Inferior//FWBrowse(): SetLineOk ( < bLineOk> ) -->
        DEFINE FWBROWSE oLstMnt DATA ARRAY ARRAY aLstMnt LINE BEGIN 1 NO LOCATE NO CONFIG NO SEEK NO REPORT OF oPnCenter 
            ADD STATUSCOLUMN    oColumn DATA { || aLstMnt[oLstMnt:At()][PLEG	] } DOUBLECLICK { |oLstMnt2|BtLegenda() } OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PASSIN ] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PASSIN ]	TYPE "C" SIZE 04 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PROCES ] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PROCES ]	TYPE "C" SIZE 04 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PETAPA ] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PETAPA ]	TYPE "C" SIZE 06 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PDPROC ] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PDPROC]	TYPE "N" SIZE 06 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PROCEOK] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PROCEOK]	TYPE "N" SIZE 06 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PROERRO] } DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PROERRO ] TYPE "N" SIZE 04 OF oLstMnt
            ADD COLUMN          oColumn DATA { || aLstMnt[oLstMnt:At()][PREPROC] }DOUBLECLICK { |oLstMnt| Processa( {||GeraTitulo(oLstMnt,aLstMnt,"",oSay),aLstMnt2:= RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt),;
            oLstMnt2:SetArray(aLstMnt2),oLstMnt2:Refresh()})} TITLE aCabMnt[PREPROC]	TYPE "N" SIZE 06 OF oLstMnt
            
            
        ACTIVATE FWBROWSE oLstMnt
        
        DEFINE FWBROWSE oLstMnt2 DATA ARRAY ARRAY aLstMnt2 LINE BEGIN 1 NO LOCATE NO CONFIG NO SEEK NO REPORT OF oPnDown 
            ADD STATUSCOLUMN    oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][PLEG	] }     DOUBLECLICK { |oLstMnt2|BtLegenda() } OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][CETAPA ] }   TITLE aCabMnt2[CETAPA ]	TYPE "C" SIZE 04 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][CALIAS ] }   TITLE aCabMnt2[CALIAS ]	TYPE "C" SIZE 06 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][CDATA ] }    TITLE aCabMnt2[CDATA ]	TYPE "D" SIZE 06 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][HORA	] }     TITLE aCabMnt2[HORA]	TYPE "C" SIZE 06 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][ERROR ] }    DOUBLECLICK { |oLstMnt2|/*CampoMemo(aLstMnt2[oLstMnt2:At()][ERROR ])*/ } TITLE aCabMnt2[ERROR ]	TYPE "C" SIZE 8000 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][INDICE] }    TITLE aCabMnt2[INDICE]	TYPE "N" SIZE 04 OF oLstMnt2
            ADD COLUMN          oColumn2 DATA { || aLstMnt2[oLstMnt2:At()][CHAVE] }     TITLE aCabMnt2[CHAVE]   TYPE "C" SIZE 20 OF oLstMnt2
        ACTIVATE FWBROWSE oLstMnt2

        ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg, {|| oDlg:End() }, {|| oDlg:End() }, .F., aButtons, /*nRecno*/, /*cAlias*/, .F., .F., .F., .F., .F., "RmiMonitor")

        FwFreeObj(aButtons)
        FwFreeObj(aCoors )
        FwFreeObj(aLstMnt)
        FwFreeObj(aCabMnt)
        FwFreeObj(aLstInt)
        FwFreeObj(aLstMnt2)
        FwFreeObj(aCabMnt2)        
        
        FwFreeObj(oDlg   )

    EndIf


Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} IntAtiva
Adiciona os botoes a outras a��es e habilita as integra��es

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValTables()

Local lRet := .F.

If FwAliasInDic( "MHQ" ) .AND. FwAliasInDic( "MHR" ) .AND. FwAliasInDic( "MHL" )
    lRet := .T.
Else
    MsgInfo(STR0017) //"N�o existem criadas as tabelas MHQ, MHR e MHL para a integra��o!"
    lRet := .F.
EndIf

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} BtLegenda
Add a legenda aos itens do grid consolidado das informa��es de
publica��o, distribui��o, gravabatch e cancelamento

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function BtLegenda()
    
    Local aLegs := {}

    AAdd(aLegs, {"BR_VERMELHO"	, STR0008	})  //"Com erro(s)"
    AAdd(aLegs, {"BR_VERDE"		, STR0009   })  //"Sem erro(s)"
    AAdd(aLegs, {"BR_AMARELO"   , STR0027   })  //"Aguardando Reprocessamento"
    

    BrwLegenda(STR0011, STR0015, aLegs)     //"Monitor de Integra��o"    //"Legenda"

Return NIL


//-------------------------------------------------------------------
/*/{Protheus.doc} RmiConGeral
Retorna o resultado com todos os registros pendentes e com erros nas tabelas
MHQ e MHR

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiConGeral(dDtIni,dDtFim)

Local cQuery    := "" //Armazena a query
Local cTabela   := "" //Proxima tabela temporaria
Local aRet      := {} //Array para retorno do resultado da query
Local aLstMnt   := {} //Array para retorno da fun��o
Local nY, nX    := 1  //Variavel de loop

Default dDtIni := Date()
Default dDtFim := Date()

cTabela := GetNextAlias()

cQuery := "SELECT ASSINANTE"
cQuery += "     , PROCESSO"
cQuery += "	    , STATUS"
cQuery += "	    , ETAPA"
cQuery += "     , SUM(CONTADOR) TOTAL"
cQuery += "  FROM (SELECT MHQ_ORIGEM ASSINANTE"
cQuery += "             , MHQ_CPROCE PROCESSO"
cQuery += "			    , CASE MHQ_STATUS "
cQuery += "     		       WHEN '1' THEN 'PENDENTE' "
cQuery += "					   WHEN '3' THEN 'COM ERRO' "
cQuery += "                    WHEN '4' THEN 'REPROCESSAR' "  
cQuery += "			      END STATUS"
cQuery += "             , 'PUBLICACAO' ETAPA"
cQuery += "			    , COUNT(MHQ_STATUS) CONTADOR"
cQuery += "          FROM " + RetSqlName("MHQ") + " HQ"
cQuery += "               LEFT JOIN " + RetSqlName("MHR") + " HR ON HR.MHR_FILIAL = HQ.MHQ_FILIAL"
cQuery += "	                                                    AND HR.MHR_UIDMHQ = HQ.MHQ_UUID"
cQuery += "         WHERE MHQ_STATUS <> '2'"
cQuery += "           AND MHQ_DATGER BETWEEN  '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
cQuery += "           AND HQ.D_E_L_E_T_ <> '*'"
cQuery += "         GROUP BY MHQ_STATUS"
cQuery += "		        , MHQ_CPROCE"
cQuery += "				, MHQ_ORIGEM"
cQuery += "         UNION ALL"
cQuery += "        SELECT MHR_CASSIN ASSINANTE"
cQuery += "		        , MHR_CPROCE PROCESSO"
cQuery += "			    , CASE MHR_STATUS "
cQuery += "			           WHEN '1' THEN 'PENDENTE' "
cQuery += "			           WHEN '2' THEN 'PROCESSADOS' "
cQuery += "			           WHEN '3' THEN 'COM ERRO' "      
cQuery += "				  END STATUS"
cQuery += "             , 'DISTRIBUICAO' ETAPA"
cQuery += "			    , COUNT(MHR_STATUS) CONTADOR"
cQuery += "          FROM " + RetSqlName("MHR") + " HR"
cQuery += "         WHERE MHR_STATUS <> '0'"
cQuery += "           AND (MHR_DATPRO BETWEEN '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "' OR MHR_STATUS = '1') "
cQuery += "           AND HR.D_E_L_E_T_ <> '*'"
cQuery += "         GROUP BY MHR_STATUS"
cQuery += "		        , MHR_CPROCE"
cQuery += "				, MHR_CASSIN"
cQuery += "         UNION ALL"
cQuery += "		   SELECT 'PROTHEUS' ASSINANTE"
cQuery += "			    , 'VENDA' PROCESSO"
cQuery += "   			, CASE "
cQuery += "					   WHEN L1_SITUA IN ('RX','IP') THEN 'PENDENTE'"
cQuery += "					   WHEN L1_SITUA IN ('OK') THEN 'PROCESSADOS'"
cQuery += "  				   ELSE 'COM ERRO'"
cQuery += "			      END STATUS"
cQuery += "			    , 'GRVBATCH' ETAPA"
cQuery += "			    , COUNT(L1_SITUA) CONTADOR"
cQuery += "		     FROM " + RetSqlName("SL1") + " L1"
cQuery += "		    WHERE L1_SITUA IN ('RX','ER','OK', 'IP', 'IR')"
cQuery += "		      AND L1_EMISSAO BETWEEN '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
cQuery += "		      AND L1.D_E_L_E_T_ <> '*'"
cQuery += "		    GROUP BY L1_SITUA) TAB"
cQuery += " GROUP BY ASSINANTE"
cQuery += "        , PROCESSO"
cQuery += "  	   , STATUS"
cQuery += "  	   , ETAPA"
cQuery += " ORDER BY ASSINANTE DESC"

DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabela, .T., .F.)

While !(cTabela)->( Eof() )     
    Aadd(aRet,{" ",(cTabela)->STATUS,(cTabela)->ASSINANTE,(cTabela)->PROCESSO,(cTabela)->TOTAL,(cTabela)->ETAPA})
    (cTabela)->( DbSkip() )
EndDo
(cTabela)->( DbCloseArea() )
 
For nX :=1 To Len(aRet)
    For nY:= 1 To Len(aRet)
        If aRet[nX][3]+aRet[nX][4]+aRet[nX][6] == aRet[nY][3]+aRet[nY][4]+aRet[nY][6]  
            If EMPTY(aLstMnt) 
                If aRet[nY][2] = "PENDENTE"   
                    AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],aRet[nY][5],0,0,0}  )     
                elseIf aRet[nY][2] = "PROCESSADOS" 
                    AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,aRet[nY][5],0,0}  )
                ElseIf aRet[nY][2] = "COM ERRO"
                    AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,0,aRet[nY][5],0}  )
                ElseIf aRet[nY][2] = "REPROCESSAR"
                    AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,0,0,aRet[nY][5]}  )
                EndIf    
            else
                If aRet[nX][3]+aRet[nX][4]+aRet[nX][6] == aLstMnt[Len(aLstMnt)][2]+aLstMnt[Len(aLstMnt)][4]+aLstMnt[Len(aLstMnt)][3]
                    If aRet[nY][2] = "PENDENTE"   
                        aLstMnt[Len(aLstMnt)][5] := aRet[nY][5]
                    elseIf aRet[nY][2] = "PROCESSADOS" 
                        aLstMnt[Len(aLstMnt)][6] := aRet[nY][5]
                    ElseIf aRet[nY][2] = "COM ERRO"
                        aLstMnt[Len(aLstMnt)][7] := aRet[nY][5]
                    ElseIf aRet[nY][2] = "REPROCESSAR"
                        aLstMnt[Len(aLstMnt)][8] := aRet[nY][5]
                    EndIf
                else
                    If aRet[nY][2] = "PENDENTE"   
                        AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],aRet[nY][5],0,0,0}  )     
                    elseIf aRet[nY][2] = "PROCESSADOS" 
                        AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,aRet[nY][5],0,0}  )
                    ElseIf aRet[nY][2] = "COM ERRO"
                        AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,0,aRet[nY][5],0}  )
                    ElseIf aRet[nY][2] = "REPROCESSAR"
                        AAdd(aLstMnt,{aRet[nY][1],aRet[nY][3],aRet[nY][6],aRet[nY][4],0,0,0,aRet[nY][5]}  )
                    EndIf       
                EndIf        
            endIf    
        EndIf    
    Next nY
Next nX

//Add legenda, quando n�o tem erro fica azul e quando tem fica vermelho.
For nX := 1 To Len(aLstMnt)

    If aLstMnt[nX][PROERRO] > 0
        aLstMnt[nX][PLEG] := "BR_VERMELHO"
    ElseIf aLstMnt[nX][PREPROC] > 0
        aLstMnt[nX][PLEG] := "BR_AMARELO"
    Else    
        aLstMnt[nX][PLEG] := "BR_VERDE"    
    EndIf

Next nX

Return aLstMnt
//-------------------------------------------------------------------
/*/{Protheus.doc} RmiAtuErro
Retorna o resultado com todos os registros pendentes e com erros nas tabelas
MHQ e MHR

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiAtuErro(dDtIni, dDtFim,aLstMnt,oLstMnt,cClick)
Local cQuery    := "" //Armazena a query
Local cTabela   := "" //Proxima tabela temporaria
Local aRet      := {} //Array para retorno do resultado da query
Local cTaLias   := ""

Default dDtIni := Date()
Default dDtFim := Date()
Default aLstMnt := {}

If !Empty(aLstMnt) 
    cTabela := GetNextAlias()

    If Alltrim(aLstMnt[oLstMnt:nAt][PETAPA]) == "DISTRIBUICAO"  
        cTAlias := 'MHR'
    ElseIf Alltrim(aLstMnt[oLstMnt:nAt][PETAPA]) == "GRVBATCH"
        cTAlias := 'SL1'    
    Else 
        cTAlias := 'MHQ' 
    EndIf
    
    cQuery := " SELECT  R_E_C_N_O_ RECMHL, MHL_FILIAL,MHL_ALIAS,MHL_DATA,MHL_HORA,MHL_INDICE,MHL_CHAVE, "
    If "MSSQL" $ Upper(Alltrim(TcGetDB()))
        cQuery += " ISNULL(CONVERT(VARCHAR(8000), CONVERT(VARBINARY(8000), MHL_ERROR)),'') AS MHL_ERROR"
    Else
        cQuery += " '' MHL_ERROR"
    EndIf
    If "MSSQL" $ Upper(Alltrim(TcGetDB()))
        cQuery += "  FROM " + RetSqlName("MHL") + " WITH (NOLOCK)"
    Else
        cQuery += "  FROM " + RetSqlName("MHL")
    EndIf
    cQuery += " Where " 
    cQuery += " MHL_DATA BETWEEN '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
    cQuery += " AND  MHL_ALIAS ='"+cTAlias+"'"
    cQuery += " AND D_E_L_E_T_ = ' '"
    If Alltrim(aLstMnt[oLstMnt:nAt][PETAPA]) == "GRVBATCH"
        cQuery += " AND MHL_CPROCE = '"+Alltrim(aLstMnt[oLstMnt:nAt][PETAPA])+"'"
    else
        cQuery += " AND MHL_CPROCE = '"+Alltrim(aLstMnt[oLstMnt:nAt][PROCES])+"'"
    EndIf    
    cQuery += " AND MHL_CASSIN = '"+Alltrim(aLstMnt[oLstMnt:nAt][PASSIN])+"'"
    cQuery += " AND  MHL_STATUS ='IR' "

    DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabela, .T., .F.)

    While !(cTabela)->( Eof() )     
        
        MHL->( dbGoTo( (cTabela)->RECMHL ) )
        //Caso seja base SQL ja tem o tratamento para a leitura do erro e fica mais rapido
        If Empty((cTabela)->MHL_ERROR)
            cDescErro := AllTrim( Memoline( MHL->MHL_ERROR ) )
        Else
            cDescErro := AllTrim( (cTabela)->MHL_ERROR )
        EndIf    
        
        Aadd(aRet,{"BR_VERMELHO",aLstMnt[oLstMnt:nAt][PETAPA],(cTabela)->MHL_ALIAS,(cTabela)->MHL_DATA,(cTabela)->MHL_HORA,;
        (cTabela)->MHL_ERROR,(cTabela)->MHL_INDICE,(cTabela)->MHL_CHAVE})
        
        (cTabela)->( DbSkip() )
    EndDo
    (cTabela)->( DbCloseArea() )
EndIf

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Mostra erro
Retorna o resultado registros com erros nas tabelas

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------

Static Function CampoMemo(cTexto)
    

 DEFINE DIALOG oDlg TITLE STR0025 FROM 180,180 TO 550,700 PIXEL //"Detalhe do Erro"
  
 // Cria Fonte para visualiza��o
 oFont := TFont():New('Courier new',,-18,.T.)
  
 // Usando o m�todo New
 oSay1:= TSay():New(01,01,{||},oDlg,,oFont,,,,.T.,CLR_RED,CLR_WHITE,550,700)
 oSay1:SetText( cTexto )
  
 // M�todos
 oSay1:CtrlRefresh()
 
 //oSay:SetText( cTexto )
  
 oSay1:SetTextAlign( 0, 0 )
 
 // Propriedades
 oSay1:lTransparent = .T.
 
 oSay1:lWordWrap = .T.
 
ACTIVATE DIALOG oDlg CENTERED

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraTitulo
Atualiza o titulo do grid detalhe conforme linha selecionada

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GeraTitulo(oLstMnt,aLstMnt,cTexto,oSay) 

If Len(aLstMnt) > 0
    cTexto := Alltrim(aLstMnt[oLstMnt:At()][PASSIN ])+" | "+Alltrim(aLstMnt[oLstMnt:At()][PROCES ])+" | "+Alltrim(aLstMnt[oLstMnt:At()][PETAPA ])
    oSay:SetText(cTexto)
    oSay:CtrlRefresh()
endif

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} Reprocessa
Prepara o Registro para Reprocessamento.

@author  Varejo
@since   16/04/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function Reprocessa(oLstMnt,aLstMnt,dDtIni,dDtFim) 
Local cQuery    := ""
Local aArea     := GetArea()
Local cTabTemp  := GetNextAlias()
Local lMHR,lSL1 := .F.


If Alltrim(aLstMnt[oLstMnt:At()][PROCES]) == "VENDA" 
    If MSGYESNO( STR0032+Alltrim(Str(aLstMnt[oLstMnt:At()][PROERRO])))    //"Confirma o Reprocessamento de VENDA - Total - "
        If Alltrim(aLstMnt[oLstMnt:At()][PETAPA]) == "PUBLICACAO"
            
            cQuery += " SELECT MHQ_UUID  UUID  "
            cQuery += " FROM " + RetSqlName("MHQ") + " HQ"
            cQuery += " WHERE MHQ_STATUS = '3' "
            cQuery += " AND MHQ_DATGER BETWEEN  '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
            cQuery += " AND MHQ_ORIGEM = 'CHEF' "
            cQuery += " AND MHQ_CPROCE ='VENDA' "
            cQuery += " AND HQ.D_E_L_E_T_ <> '*'"
            DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabTemp, .T., .F.)
        elseif Alltrim(aLstMnt[oLstMnt:At()][PETAPA]) == "DISTRIBUICAO"
            lMHR   := .T.
            cQuery += " SELECT MHR_UIDMHQ  UUID  "
            cQuery += " FROM " + RetSqlName("MHR") + " HR"
            cQuery += " INNER JOIN " + RetSqlName("MHQ") + " MHQ ON MHQ.MHQ_UUID = HR.MHR_UIDMHQ"
            cQuery += " AND MHQ_ORIGEM = 'CHEF' "
            cQuery += " AND MHQ.D_E_L_E_T_ <> '*'"
            cQuery += " WHERE MHR_STATUS = '3' "
            cQuery += " AND MHR_CPROCE ='VENDA' "
            cQuery += " AND MHR_DATPRO BETWEEN  '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
            cQuery += " AND HR.D_E_L_E_T_ <> '*'"
            DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabTemp, .T., .F.)
        elseIf Alltrim(aLstMnt[oLstMnt:At()][PETAPA]) == "GRVBATCH"
            lSL1   := .T.
            cQuery += " SELECT L1_UMOV UUID,SL1.R_E_C_N_O_  Recno  "
            cQuery += " FROM " + RetSqlName("SL1") + " SL1"
            cQuery += " INNER JOIN " + RetSqlName("MHQ") + " MHQ ON MHQ.MHQ_UUID = SL1.L1_UMOV"
            cQuery += " AND MHQ_ORIGEM = 'CHEF' "
            cQuery += " AND MHQ.D_E_L_E_T_ <> '*'"
            cQuery += " WHERE L1_SITUA IN ('ER','IR')"
            cQuery += " AND SL1.L1_UMOV <> ' ' "
            cQuery += " AND SL1.L1_EMISSAO BETWEEN '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim) + "'"
            cQuery += " AND SL1.D_E_L_E_T_ <> '*'"
            DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabTemp, .T., .F.)
        EndIf


        If (cTabTemp)->(!EOF())
            
            dbSelectArea("MHQ")
            MHQ->(dbSetOrder(7)) 
            
            dbSelectArea("MHR")
            MHR->(dbSetOrder(3))
            
            dbSelectArea("SL1")
            Begin Transaction
                While (cTabTemp)->(!EOF())
                    
                    If lSL1 
                        SL1->(dbGoto((cTabTemp)->Recno))
                        If SL1->L1_SITUA  $ "ER|IR"
                            SL1->(RecLock("SL1",.F.))
                            SL1->L1_SITUA := 'RP'
                            SL1->(MsUnLock())
                        EndIf     
                    EndIf  
                    
                    If lMHR .AND. MHR->( DbSeek(xFilial("MHR")+(cTabTemp)->UUID))
                        MHR->(RecLock("MHR",.F.))
                        MHR->MHR_STATUS := '4'
                        MHR->(MsUnLock()) 
                    EndIf        
                    
                    If  MHQ->( DbSeek(xFilial("MHQ")+(cTabTemp)->UUID))
                        MHQ->(RecLock("MHQ",.F.))
                        MHQ->MHQ_STATUS := '4'
                        MHQ->(MsUnLock()) 
                    EndIf

                    (cTabTemp)->( DbSkip() )
                EndDo
            End Transaction
        else
            MSGINFO( STR0029, STR0028 )
            MSGINFO( STR0030, STR0028 )
        EndIf
        (cTabTemp)->(DbCloseArea())
    EndIf
else
    MSGINFO(STR0031, "Reprocessar" )    
endIf

RestArea(aArea)
Return
