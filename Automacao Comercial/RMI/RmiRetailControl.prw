#include 'protheus.ch' 
#include 'parmtype.ch'
#INCLUDE "TOPCONN.CH"
#INCLUDE "RMIMONITOR.CH"
#INCLUDE "TBICONN.CH"

Static cACAOINT         := ""           //Acao de Integracao para evitar o carregamento do _aTabPad e _aTabInt a cada registro 

//Salva dados do Ambiente
Static dBkpdData        := CtoD("  /  /    ")
Static cBkpFilAnt       := ""
Static cBkpEmpAnt       := ""
Static aAreaSM0         := {}    

//-------------------------------------------------------------------
/*/{Protheus.doc} TotalInputDocument
Rotina para preparar o ambiente para a importacao de
registro do Legado para o Protheus via Schedule.
esta funcionalidade funciona da mesma forma que o INT300K
somente tendo como diferen�a a forma de tratamento parametro
@param aParam, cEmpAux,cFilAux,cTotFil

@return Vazio
@Obs	INT300KJ para RmiControl 
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiControl(aParam,cEmpAux,cFilAux,cTotFil)//VAI
Local cJob              := "RMICONTROL"
Local cEmpJob           := ""                                   //Empresa
Local cEmpPar           := ""                                     //Empresa par�metros 
Local cFilJob           := ""                                   //Guarda a filial anterior
Local cFilPar           := ""                                   //Guarda a filial vinda como parametro
Local cFuncao           := ""                                   //Codigo da Rotina que sera executada
Local oLocker           := Nil                                  //Objeto do Loja para Lock de registros
Local lTodos           	
Local aEmpFil			:= {}
Local nPos				:= 0
Local nX				:= 0        
Local aFilVld			:= {}

Default cEmpAux			:= ""
Default cFilAux			:= ""
Default cTotFil			:= "0"

Begin Sequence
  	//������������������������������Ŀ
 	//�Analisa Parametros recebidos. �
  	//��������������������������������    
  	If (aParam == Nil .OR. ValType(aParam) == "U")
    	LjGrvLog("RmiControl","RmiControl - * * " + DtoC(dDataBase) + " " + Time() + " - Parametros nao recebidos => RMICONTROL(cEmpJob, cFilJob, cFuncao)")
    	Return(Nil) 
  	Endif
  	
  	//Caso esteja como tipo Caracter, significa que esta sendo executado pelo JOB
  	If Valtype(aParam) == "C"
  		LjGrvLog("RmiControl", " Configurado como JOB  - Funcao: " + aParam + " Empresa: " + cEmpAux + " Filial: " + cFilAux)
  		cFuncao := aParam
  		aParam	:= {}
  		
  		aAdd(aParam, cFuncao)
  		aAdd(aParam, cEmpAux)
  		aAdd(aParam, cFilAux)
  		aAdd(aParam, cTotFil)
	 	
  	EndIf
        
  	LjGrvLog("RmiControl","RmiControl - Executando funcionalidade JOB RmiControl")
 	LjGrvLog("RmiControl","RmiControl - Funcao  : "+If( Valtype(aParam)=="A" .and. Len(aParam) > 0,aParam[1],"NAO"  ))
  	LjGrvLog("RmiControl","RmiControl - Empresa : "+If( Valtype(aParam)=="A" .and. Len(aParam) > 1,aParam[2],"NAO"  ))
  	LjGrvLog("RmiControl","RmiControl - Filial  : "+If( Valtype(aParam)=="A" .and. Len(aParam) > 2,aParam[3],"NAO"  ))

    cFuncao := aParam[1]
    cEmpJob := If( ValType(aParam[2]) == "C" , aParam[2]    , cEmpAnt)   
    cEmpPar := If( ValType(aParam[2]) == "C" , aParam[2]    , "")   
    cFilJob := If( ValType(aParam[3]) == "C" , aParam[3]    , cFilAnt )
    cFilPar := If( ValType(aParam[3]) == "C" , aParam[3]    , "")
    
	//processamento de todas as empresas
	lTodos  :=  If( aParam[4] == "1", .T., .F. ) 
    //Conout(ENTER)
    Conout( Upper("--- Inicio do Processamento de Importacao. - RMICONTROL - " + cFuncao + " Empresa "+cEmpJob+" Filial "+cFilJob ) )
    LjGrvLog("RmiControl", Upper("--- Inicio do Processamento de Importacao. - RMICONTROL - " + cFuncao + " Empresa "+cEmpJob+" Filial "+cFilJob ) )

  	//�Prepara Ambiente. �
  	RpcSetEnv(cEmpJob, cFilJob)          
  	Conout( "Aberto "+" Empresa "+cEmpJob+" Filial "+cFilJob)
    LjGrvLog("RmiControl", "RmiControl - Aberto "+" Empresa "+cEmpJob+" Filial "+cFilJob)                               

  	//�Verifica se o JOB ja esta executando.�
  	cJob += cEmpJob + "_" + cFuncao
  	oLocker := LJCGlobalLocker():New()
  	If !oLocker:GetLock( cJob )
    	Conout(" * * " + DtoC(dDataBase) + " " + Time() + " - <<< " + cJob + " >>> Processo ja esta em execucao.")
    	LjGrvLog("RmiControl","RmiControl - * * " + DtoC(dDataBase) + " " + Time() + " - <<< " + cJob + " >>> Processo ja esta em execucao.")
    	RpcClearEnv()           //Reset Environment
    	Return(Nil)
  	EndIf

  	//�Carrega Chave e Busca. �
    LjGrvLog("RmiControl" ,"RmiControl - Valida��o de processamento "+" Empresa "+cEmpJob+" Filial "+cFilJob)
   	//Executa para todas as Empresas
	_cChave := ""
  	If !Empty(cEmpPar)           
  		_cChave  += cEmpPar
  	EndIf

	//Executa para todas as Filiais da Empresa
  	If !Empty(cFilPar)           
   		_cChave  += cFilPar 
  	EndIf
    
  	aFilVld := FWLoadSM0(.T.)//Busca todas as filiais e empresa do sistema
	LjGrvLog( "RmiControl","RmiControl - Abertura de Arquivos de Empresa - Empresa: " + cEmpPar + " Filial: " + cFilPar)                           	  

 	aEmpFil := {}
 	LjGrvLog("RmiControl" ,"Array de Filial Carregado na FWLoadSM0 ",aFilVld)
 	For nX:=1 To Len(aFilVld)//Busca todas as filias
 		Aadd(aEmpFil, {aFilVld[nX][1],aFilVld[nX][2]} )
 	Next	
	LjGrvLog("RmiControl" ,"Depois do For Total de Filiais adicionado no aEmpFil ",aEmpFil)
	
	If Len(aEmpFil) == 0
		LjGrvLog("RmiControl","RmiControl - N�o localizado Empresa/Filial")
		Break	
	Endif
	//processar empresa/filial encontradas
	For nPos := 1 To Len(aEmpFil)
		LjGrvLog("RmiControl","RmiControl - Verificando Empresa: " + aEmpFil[nPos,1] + " - Filial: " + aEmpFil[nPos,2])
   		RpcClearEnv()
   		LjGrvLog("RmiControl","RmiControl - Processando Empresa: " + aEmpFil[nPos,1] + " - Filial: " + aEmpFil[nPos,2]) 
   		RpcSetEnv(aEmpFil[nPos,1], aEmpFil[nPos,2])                              
   		ChkFile("SM0")
    	//�Executa a funcao de Importacao passada no Parametro. �
    	RMIExec(cFuncao, aEmpFil[nPos,1], aEmpFil[nPos,2])  
     Next             
  	//�Libera JOB que estava em execucao.�
  	oLocker:ReleaseLock( cJob )
    //�Reset Environment. �
  	DbCloseAll()
  	RpcClearEnv()  
    Conout( Upper("--- Finalizacao do Processamento de Importacao. - RmiControl - " + cFuncao + " ---") )
    LjGrvLog( "RmiControl",Upper("--- Finalizacao do Processamento de Importacao. - RmiControl - " + cFuncao + " ---") )
  	//Conout(ENTER)
End Sequence        
Return(Nil)                            
//-------------------------------------------------------------------
/*/{Protheus.doc} RmiFilInt
Trata Filial
@param cFilInt, lAtualiza, cCnpj

@return Vazio
@Obs	INTM010U para RmiFilInt 
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiFilInt(cFilInt, lAtualiza, cCnpj)
Local lFilCnpj	:= SuperGetMv("IN_FILCNPJ", , .F.)		//Parametro para indicar se a busca � por CNPJ ou Filial (.T. - CNPJ .F. - Filial)
  
Default cFilInt	:= cFilAnt
Default lAtualiza	:= .T.
Default cCnpj		:= ""
                               
If lAtualiza
	// Caso o parametro seja por CNPJ preenche o campos cFilInt
    If lFilCnpj .And. !Empty(cCnpj)
    	cFilInt := 	RMIFILCNP(cCnpj)
    EndIf
	  
    If !Empty(cFilInt) 
	    cBkpEmpAnt      := SM0->M0_CODIGO
	    cBkpFilAnt      := cFilAnt
	    aAreaSM0        := SM0->(GetArea())
	    SM0->( DbSetOrder(1) )			//M0_CODIGO + M0_CODFIL
	    SM0->( MsSeek(cBkpEmpAnt + PadR(cFilInt, 12)) )
	    cFilAnt := cFilInt
    EndIf              
Else      
	If Len(aAreaSM0) > 0
		RestArea(aAreaSM0)      
      	cFilAnt := cBkpFilAnt  
    EndIf   
	
EndIf
        
Return(Nil)
     
//-------------------------------------------------------------------
/*/{Protheus.doc} RmiThread
Calcula a quantidade de registros por Thread
@param nQtdRegis, cParametro

@return Vazio
@Obs	INTM010W para RmiThread
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiThread(nQtdRegis, cParametro)//Vai
Local nQtdThread        := 1            //Quantidade de Threads de Processamento 
Local nRegThread        := 0     

Default cParametro      := "IN_TRHEAMI"
        
nQtdThread := SuperGetMv(cParametro, Nil, 4)
nRegThread := Int( nQtdRegis / nQtdThread )

If Mod( nQtdRegis, nQtdThread ) > 0 
	nRegThread++                                    
EndIf  

Return(nRegThread)

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiCargaIn
Executa a funcao que foi chamada pelo StartJob para as cargas iniciais
@param cEmpJob, cFilJob, aFuncExec, cThread, cStaimp

@return Vazio
@Obs	INTM010X para RmiCargaIn
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
 Function RmiCargaIn(cEmpJob, cFilJob, aFuncExec, cThread, cStaimp)//Vai
  Local cFuncao   := ""                   //Nome da Funcao que sera Executada
  Local nRecIni   := 0                    //Recno Inicial
  Local nRecFim   := 0                    //Recno Final
  Local nCont     := 0                    //Contador
//  Local cJob      := "INTM010X"            //Nome do Semaforo
  Local cJob      := "RmiCargaIn"           //Nome do Semaforo
  Local oLocker   := Nil                  //Objeto utilizado para Semaforo
  Local nLockByOld:= 0                    //Numero maximo de nomes reservados anteriormente
        
  Default cStaimp := "' ','4'"    //Status que sera importado _STAIMP
        
  //����������������������Ŀ
  //�aFuncExec - Composicao�
  //������������������������
  //--Nome da Funcao
  //--Descricao
  //--Recno Inicial
  //--Recno Final                 
        
  //���������������������
  //�Prepara o Ambiente �
  //���������������������
  //RpcSetType(3)                   //Nao utilizar licenca
  //RpcSetType(2)                   //Nao utilizar licenca
  LjGrvLog("RmiCargaIn","RmiControl - Chamada funcionalidade " + cJob + " - Tread Filial: " + cFilJob + " - Empresa : " + cEmpJob)
  RpcSetEnv(cEmpJob, cFilJob)            
        
  //�������������������������������������Ŀ
  //�Verifica se o JOB ja esta executando.�
  //���������������������������������������    
  cJob += AllTrim(cEmpJob) + "_" + AllTrim( Upper(aFuncExec[1][1]) ) + "_" + cThread
  oLocker := LJCGlobalLocker():New()
  If !oLocker:GetLock( cJob )
  	Conout(" * * " + DtoC(dDataBase) + " " + Time() + " - <<< " + cJob + " >>> Processo ja esta em execucao.")
    LjGrvLog("RmiCargaIn","RmiControl - * * " + DtoC(dDataBase) + " " + Time() + " - <<< " + cJob + " >>> Processo ja esta em execucao.")
    RpcClearEnv()           //Reset Environment
    Return(Nil)
  EndIf
        
  //��������������������������������������������������������������Ŀ
  //�Atualiza o numero m�ximo de nomes reservados para MayIUseCode.�
  //����������������������������������������������������������������
  nLockByOld := SetMaxCodes(50)
        
  For nCont:=1 To Len(aFuncExec)
  	cFuncao := AllTrim( Upper(aFuncExec[nCont][1]) )  
    nRecIni := aFuncExec[nCont][3]
    nRecFim := aFuncExec[nCont][4]
                
    LjGrvLog("RmiCargaIn","RmiControl - THREAD: " + cThread + " FUNCAO: " + cFuncao + " REGISTROS: " + cValToChar(nRecIni) + " ao " + cValToChar(nRecFim) )
        
    //��������������������������������������Ŀ
    //�Verifica a funcao que sera executada. �
    //����������������������������������������
    Do Case                                                  
    	Case cFuncao == "RMIRETAILJ"  
    		RMIRetailJ( Nil, nRecIni, nRecFim, cStaimp)
    	Case cFuncao == "RMICANCELLATION"  
    		RmiCancellation(Nil, nRecIni, nRecFim, cStaimp)
    	OtherWise
    	//Nada Faz
    EndCase 
    //Aguarda 5 segundo para processar a proxima funcao
    LjGrvLog("RmiCargaIn","RmiControl - THREAD: " + cThread + " FUNCAO: " + cFuncao + " REGISTROS: " + cValToChar(nRecIni) + " ao " + cValToChar(nRecFim) + " - FINALIZADA. " )
    Sleep(5000)        
  Next nCont
        
  //��������������������������������������������������������������Ŀ
  //�Restaura o numero maximo de nomes reservados para MayIUseCode.�
  //����������������������������������������������������������������
  SetMaxCodes(nLockByOld)
        
  //����������������������������������Ŀ
  //�Libera JOB que estava em execucao.�
  //������������������������������������ 
  oLocker:ReleaseLock( cJob )

  Return(Nil)     

//-------------------------------------------------------------------
/*/{Protheus.doc} RMIPREPM
Prepara para chamar as rotinas via Multi Thread
@param cTabTemp, cQuery, cFuncao, cParametro, cStaimp

@return Vazio
@Obs	�INTM010Y para RMIPREPM
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RMIPREPM(cTabTemp, cQuery, cFuncao, cParametro, cStaimp)//Vai  
  Local nQtdThread        := 1                    //Quantidade de Threds de Processamento 
  Local nQtdRegis         := 0
  Local nRegThread        := 0
  Local aRecnos           := {}  
  Local nThread           := 1
  Local nIni              := 1
  Local nFim              := 0
  Local lEspera           := .F.
  Local aDados            := {}
        
//  Default cParametro      := "ES_0000027"           
  Default cParametro      := "IN_TRHEAMI"           
  Default cStaimp         := "' ', '4'"
        
  nQtdThread := SuperGetMv(cParametro, Nil, 4)
        
  If Select(cTabTemp) <> 0
    (cTabTemp)->(DbCloseArea())
  Endif  

  DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cTabTemp, .F., .T.)
        
  //���������������������������������������Ŀ
  //�Carrega os registros de notas fiscais. �
  //�����������������������������������������
  If !(cTabTemp)->( Eof() ) 
  	While !(cTabTemp)->( Eof() )
    	Aadd(aRecnos, (cTabTemp)->REGISTRO)
    	(cTabTemp)->( DbSkip() )                
    EndDo
  //Caso seja EOF n�o Processar e retornar DAC 18/10/2013
  Else
  	(cTabTemp)->( DbCloseArea() )  //DAC 20160927
  	Return(Nil)
  EndIf 
  (cTabTemp)->( DbCloseArea() ) 
        
  //����������������������������������
  //�Carrega os registros por Thread.�
  //����������������������������������
  nQtdRegis := Len(aRecnos)  
  If nQtdRegis > 0

  	//�������������������������������������������Ŀ
    //�Pega a quantidade de Registros por Thread. �
    //���������������������������������������������
    //nRegThread := U_INTM010W(nQtdRegis, cParametro) 
    nRegThread := RmiThread(nQtdRegis, cParametro) 
                
    For nThread:=1 To nQtdThread 
    	If nThread == nQtdThread
    		lEspera := .T.
    		nFim    := nQtdRegis
    	Else    
    		nFim := (nIni + nRegThread) - 1                 
    	EndIf

    	//��������������������������������������������������������������������������������Ŀ
    	//�Verifica se a quantidade total de registro eh maior ou igual a inicial e final. �
    	//����������������������������������������������������������������������������������
    	If nQtdRegis >= nIni .AND. nQtdRegis >= nFim

			If nQtdThread == 1
				aDados  := {cFuncao, "", 0, 0}
			Else
    			aDados  := {cFuncao, "", aRecnos[nIni], aRecnos[nFim]}
			EndIf
	        //�������������������
	        //�Executa Threads. �
	        //�������������������
	        StartJob("RmiCargaIn", GetEnvServer(), lEspera, cEmpAnt, cFilAnt, {aDados}, cValToChar(nThread), cStaimp)
	        Sleep(5000)      
	    EndIf   
	    nIni += nRegThread
	Next nThread
  EndIf           
  Sleep(10000)      
  
  Return(Nil)
//-------------------------------------------------------------------
/*/{Protheus.doc} RMIExec
Executa funcao de Importacao ou Carga Inicial que sera
executada via schedule
@param cFuncao, cEmpJob, cFilJob

@return Vazio
@Obs	� RMIExec
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RMIExec(cFuncao, cEmpJob, cFilJob)//Vai
  Local cDescricao        := ""           //Descricao da Rotina que esta sendo executada
  Local aQuery            := {}           //Select que seram utilizado para multi Thread
  Local aThreads          := {}           //Divisao de registro que seram processados por cadas Thread
  Local nCont             := 1            //Contador
  Local aFuncoes          := {}           //Funcoes que seram executadas

  Local lEspera           := .F.          //Define se aguarda a rotina startada via Multi Thread terminar para proceguir
  Local nQtdThread        := 0
        
  Default cEmpJob := ""
  Default cFilJob := "" 
        
  cFuncao := AllTrim( Upper(cFuncao) )

  Do Case
  	Case "CUPOM"       == cFuncao 
  		cFuncao          := "RMICUPIMP"
  		cDescricao       := "Importacao de Cupons - MULTI THREAD"
  		Aadd(aFuncoes, {cFuncao, cDescricao})
  		//Startar o Cancelamento na mesma configura��o do CUPOM 
  		cFuncao          := "RMICANCEXE"
  		cDescricao       := "Cancelamento de Cupons - MULTI THREAD"
  		Aadd(aFuncoes, {cFuncao, cDescricao})
    OtherWise       
    	cFuncao := ""                   
  EndCase
        
  //�����������������������������Ŀ
  //�Valida Execucao das Rotinas. �
  //�������������������������������
  If Empty(cFuncao)
  	LjGrvLog("RMIExec","RmiControl - " + DtoC(dDataBase) + " " + Time() + " (RMIExec) - Funcao (" + cFuncao + ") nao encontrada. Nao sera Executada." )        
  Else   
  	//��������������������������������������������������������������������������������������Ŀ
  	//�Calcula range de processamente por Thread, caso seja utilizado processo Multi Thread. �
  	//����������������������������������������������������������������������������������������
  	aThreads := RMICalcThd(aQuery)   
  	//���������������������������������������������
	//�Carga Inicial - Executa as Carga Iniciais. �
	//���������������������������������������������
	nQtdThread := Len(aThreads)
	If nQtdThread > 0 
		//�������������������
		//�Executa Threads. �
		//�������������������
		For nCont:=1 To nQtdThread
			If nCont == nQtdThread
				lEspera := .T.
	    	EndIf
	    	StartJob("RmiCargaIn", GetEnvServer(), lEspera, cEmpJob, cFilJob, aThreads[nCont], cValToChar(nCont))
	    	Sleep(5000)        
	    Next nCont     
	Else
		//�Importacao - Executas as Importacao. �
		For nCont:=1 To Len(aFuncoes)
			If ExistFunc(aFuncoes[nCont][1])
				LjGrvLog("RMIExec","RmiControl - " + DtoC(dDataBase) + " " + Time() + " (RMIExec) - Executando Importacao referente a Rotina: " + aFuncoes[nCont][1] + " - " + Upper(aFuncoes[nCont][2]) )			
				&(aFuncoes[nCont][1] + "()")
			EndIf
		Next nCont      
    EndIf                 
  EndIf

  Return(Nil)


//-------------------------------------------------------------------
/*/{Protheus.doc} RMICalcThd
Retorna o Range que cada thread ira processar
@param aQuery

@return Vazio
@Obs	� RMICalcThd
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RMICalcThd(aQuery)   
  Local nQtdThred := SuperGetMv("IN_TRHEAMI", , 4)             //Quantidade de Threds de Processamento
  Local cTabTemp  := "QRY"
  Local cQuery    := ""
  Local aAuxQtd   := {}                                                                   //Array com a Quantidade Inicial de Registro e Final de Registros
  Local nCont     := 0                                                          
  Local aTab      := {}                                                                   //Informacoes sobres as tabelas de Integracao (Funcao, Descricao, RecIni, RecFim)
  Local nTab      := 0                                                                    //Quantidade de Tabelas
  Local nIni      := 0
  Local nRegPThre := 0
  Local aThreads  := {}                                                                   //Array com o range que sera executado por cada Thread
              
  Default aQuery  := {} 
        
  //�������������������������������������������������������
  //�Verifica se processo sera executado em Multi Thread. �
  //�������������������������������������������������������
  If Len(aQuery) > 0 .AND. nQtdThred > 1 
  	//���������������������������������������������������������������������������������Ŀ
  	//�Pega primeiro e ultimo Recno das tabelas de integra��o que nao foram processados.�
    //�����������������������������������������������������������������������������������
    For nTab:=1 To Len(aQuery)   
    	aAuxQtd         := {}  
    	nRegPThre       := 0  
    	For nCont:=1 To Len(aQuery[nTab])
    		If Select(cTabTemp) > 0
    			(cTabTemp)->( DbCloseArea() )  
    		EndIf    
    		cQuery := ChangeQuery( aQuery[nTab][nCont][3] )
    		DbUseArea(.T., "TOPCONN", TcGenQry(Nil, Nil, cQuery), cTabTemp, .F., .T.)
    		If !(cTabTemp)->(Eof())   
    			Aadd(aAuxQtd, (cTabTemp)->R_E_C_N_O_ ) 
    		EndIf
    		(cTabTemp)->( DbCloseArea() )   
    	Next nCont  
                
    	If Len(aAuxQtd) > 0         
    		If (aAuxQtd[1] == aAuxQtd[2]) .OR. ( (aAuxQtd[2] - aAuxQtd[1]) <= nQtdThred )
    			nRegPThre := 1
    	Else                                                             
    		nRegPThre := RmiThread( aAuxQtd[2] - aAuxQtd[1], "IN_TRHEAMI")
    	EndIf
    		Aadd(aTab, {aAuxQtd[1], aAuxQtd[2], nRegPThre, aAuxQtd[1], aQuery[nTab][1][1] ,aQuery[nTab][1][2]} )
    	EndIf
    Next nTab  
            
    //������������������������������������������������������������Ŀ
    //�Calcula range de Recnos que sera executado por cada Thread. �
    //��������������������������������������������������������������
    If Len(aTab) > 0     
    	For nCont:=1 To nQtdThred 
    		aAuxQtd := {}
    		For nTab:=1 To Len(aTab)  
    			If nCont == 1
    				nIni := aTab[nTab][1]
    			Else
    				nIni := aTab[nTab][4] + 1       
    			EndIf   
    			aTab[nTab][4] += aTab[nTab][3]
    			//Nome da Funcao        //Descricao                     //Recno Inicial         //Recno Final
    			Aadd(aAuxQtd, { aTab[nTab][5]           , aTab[nTab][6]         , nIni                          , aTab[nTab][4]} )
    		Next nTab                                              
    		Aadd(aThreads, aAuxQtd)
    	Next nCont                       
    Endif           
  EndIf   

  Return(aThreads)    


//-------------------------------------------------------------------
/*/{Protheus.doc} RMIGRVSTAT
Grava o Status na tabela q foi feita a integracao
@param aQuery

@return Vazio
@Obs	� �INTM010J para RMIGRVSTAT
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RMIGRVSTAT(cAlias, nRec, cStatus)
  
  (cAlias)->( DbGoTo(nRec) )
  (cAlias)->( RecLock(cAlias, .F.) )
  &(cAlias + "->L1_SITUA") := cStatus
  (cAlias)->( MsUnLock() )

Return(Nil)

//-------------------------------------------------------------------
/*/{Protheus.doc} RMIFILCNP
Busca a Empresa e Filial pelo CNPJ
@param cCnpj

@return Vazio
@Obs	� �INTM010N para RMIFILCNP
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RMIFILCNP(cCnpj)
  Local aAreaSM0	:= SM0->( GetArea() )   
  Local nRecSM0		:= SM0->( Recno() )
  Local lAchouCNPJ	:= .F.

  SM0->( DbGotop() )
  
  While !SM0->( Eof() ) .And. !lAchouCNPJ
  	If AllTrim( cCnpj ) == AllTrim( SM0->M0_CGC )
		cFilialInt	:= SM0->M0_CODFIL
		lAchouCNPJ	:= .T.
	EndIf
	
    SM0->( DbSkip() )
  EndDo
  SM0->( DbGoTo(nRecSM0) )
  RestArea( aAreaSM0 )

Return(cFilialInt)


//-------------------------------------------------------------------
/*/{Protheus.doc} RMICUPIMP
Efetua importacao dos cupons fiscais em Multi - Thread
@param cCnpj

@return Vazio
@Obs	� �INTM210E para RMICUPIMP
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RMICUPIMP()

Local cTabTemp	:= 	"TMPSL1"
Local cFuncao	:= 	"RMIRetailJ"   //DAC 18/10/2013             
Local cQuery 	:=	""
Local cStaImp	:= "' '"


//Efetua o processamento
cQuery := " SELECT SL1.*, SL1.R_E_C_N_O_ as REGISTRO FROM " + RetSQLName("SL1") + " SL1" +;
		  " WHERE SL1.L1_SITUA = 'IP' AND D_E_L_E_T_ = ' ' "                             +;
          " ORDER BY SL1.R_E_C_N_O_"
				
//Chama funcao para executar as Multis Threads.?
RMIPREPM(cTabTemp, cQuery, cFuncao, "IN_TRHECUP", cStaimp)

Return(Nil)

