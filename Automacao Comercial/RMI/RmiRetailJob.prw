#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "RMIMONITOR.CH"

#DEFINE ORCAMENTO			"1"					//Orcamento
#DEFINE CUPNAOFISCAL		"2"					//Cupom nao fiscal
#DEFINE CUPOMFISCAL			"3"					//Cupom fiscal
#DEFINE ENTREGANAOFISCAL	"4"					//Cupom nao fiscal de entrega	-	Gravabat gera pedido de venda para rotina de importacao de NFS gerar na nota em cima desse pedido.
#DEFINE AUTOSERVICO			"5"					//Auto Servico - Cupom direto

#DEFINE EXCLUI_REPROCESSA	"4"					//Exclusao para reprocessamento, alterado para diferenciar que vai ser um reprocessamento

Static lFilCnpj	:= SuperGetMv("IN_FILCNPJ", , .F.)
Static cMvSisOri:= SuperGetMv("MV_SISORI", , "") //Esse parametro informa qual eh o sistema de origem da informacao que esta sendo processada. Esse parametro eh temporario, pois a informacao do sistema de origem vai estar disponivel no JSON da venda
Static lDePara	:= ExistFunc('RmiPsqDePa') //Verifica se existe a funcao para realizar o DePara
Static cChaveSL1 := "" //Guarda o conteudo do campo L1_UMOV

//-------------------------------------------------------------------
/*/{Protheus.doc} RMIRetailJ
Integracao Legado X PROTHEUS Importacao de Arquivos do Orcamento SL1 SL2 SL4
@param cQuery, nRecIni, nRecFim, cStaimp

@return Vazio
@Obs	INTM210 para RMIRetailJ
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function RMIRetailJ(cQuery, nRecIni, nRecFim, cStaimp)

Local lMultThread	:= .F.						//Define se importacao sera processada via Multi Thread
Local dDataI
Local dDataF
Local cHoraI
Local cHoraF
Local lJob 		:= IsBlind()
//Variaveis Private da Funcao
Private aSL1		:= {}						//Cabecalho do orcamento
Private aSL2 		:= {}						//Itens do orcamento
Private aSL4 		:= {} 						//Parcelas do pagamento
Private aInfoLog1	:= {}						//Informacoes necessario para a gravacao do Log
Private aInfoLog2	:= {}						//Informacoes necessario para a gravacao do Log
Private lAppend		:= .F.							//Variavel indica se vai gravar novo registro(.T.) ou replace (.F.)
Private cErro		:= ""						//Mensagem de erro					
                                                                
Default nRecIni	 	:= 0
Default nRecFim		:= 0    
Default cStaimp		:= "' ', '4'"
Default cQuery 		:=	"SELECT SL1.*,SL1.R_E_C_N_O_ as REGISTRO FROM " + RetSQLName("SL1") + " SL1"+;
						" WHERE SL1.L1_SITUA = 'IP' AND D_E_L_E_T_ = ' ' " //L1_SITUA = 'IP' Aguardando processamento


lMultThread := (nRecIni <> 0 .AND. nRecFim <> 0)

//Caso esteja executando pelo menu, filtra somente registros da propria filial
If !lJob .And. SuperGetMV("IN_210FFIL",,.T.)
	cQuery += " AND (L1_FILIAL = '" + cFilAnt + "')"
EndIf

If lMultThread
	cQuery += " AND (SL1.R_E_C_N_O_ >= " + cValToChar(nRecIni) + " AND SL1.R_E_C_N_O_ <= " + cValToChar(nRecFim) + ") "
	cQuery += " ORDER BY SL1.R_E_C_N_O_"
Else
	cQuery += " ORDER BY SL1.L1_FILIAL, SL1.L1_NUM "
EndIf
                            
dDataI := Date()
cHoraI := Time()

DbSelectArea("SL1")
If !lJob
	Processa({|lEnd|ValidSL1(cQuery, lMultThread,cStaimp,lJob)})
Else
	ValidSL1(cQuery, lMultThread,cStaimp,lJob)
Endif	
            
dDataF := Date()
cHoraF := Time()

ConOut("Registros processados data inicial " + DtoC(dDataI) + " hora inicio " + cHoraI + " data final " + DtoC(dDataF) + " hora fim " + cHoraF )
LjGrvLog("RMIRetailJ","Registros processados data inicial " + DtoC(dDataI) + " hora inicio " + cHoraI + " data final " + DtoC(dDataF) + " hora fim " + cHoraF )

Return(.T.)


//-------------------------------------------------------------------
/*/{Protheus.doc} ValidSL1
Gera arquivo temporario TMPSL1 atraves do arquivo Legado importado 
atraves do comando Append
@param cQuery, lMultThread,cStaimp,lJob

@return Vazio
@Obs	ValidSL1 para ValidSL1
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidSL1(cQuery, lMultThread,cStaimp,lJob)

Local nRecSL1		:= 	0			//nRecSL1 Numero do Registro atual da tabela PIN
Local aSL1Vazio		:= {}			//Relacao de campos que nao podem estar vazios
Local cCliente		:= ""			//Codigo do Cliente
Local cLoja   		:= ""			//Loja do Cliente
Local cTipoCli		:= ""			//Tipo do Cliente
Local cVendedor		:= ""			//Codigo do Vendedor
Local nMoedaCor		:= 1 	     	//Moeda da venda
Local cOperador		:= ""			//Operador
Local cSituacao		:= ""			//Situacao do orcametno
Local nVlrTot		:= 0			//Valor Total do Orcamento
Local nCasaDecimal	:= TamSX3("L1_VLRTOT")[2]//Quantidade de digitos para a casa decimal
Local cSerie		:= ""			//Serie do PDV
Local cEstacao		:= ""			//Codigo da Estacao
Local cTipoOrc		:= ""			//Tipo do Orcamento
Local aAux			:= {}			//Pega o retorno da funcao ValCampo
Local lErro 		:= .F.	         
Local lContinua		:= .T.
//Local cXTPREGOld	:= ""			//Quarda o XTPREG original vindo registro
Local cXTPREG		:= ""
Local cDOCPED		:= ""   
Local lSemReserva	:= .F.			//Define se Cupom Fiscal teve reserva
Local lHistorico	:= .F.
Local lExistSL1		:= .F.			//Verifica se existe cabecalho
Local cLjCodigo		:= ""			//Codigo da Loja na Tabela SLJ
Local l210INOK    	:= ExistBlock("I210INCOK")  // PE para valida��o do usu?io na inclus?
Local l210CLIOK    	:= ExistBlock("I210CLIOK")  // PE para mudan?/validacao do cliente na inclusao da venda.
//Local cLojaPad     := Alltrim(SuperGetMV( "MV_LOJAPAD"  ,.F.,"" ))
Local cVendPad		:= Alltrim(SuperGetMV( "MV_VENDPAD"  ,.F.,"" ))
Local cMens, lRet, cChaveSA1, nOrdem
Local lRet210I      := .T.
Local nLidos       := 0
Local nProcessados := 0 
Local nItemPrc     := 0
Local lSAT			:= .F.
Local lNFCe		:= .F.
Local lImpFis		:= .F.
Local cEspecie		:= ""
Local _aL1Mat
Local nPos, nI                                       
Local nTamSA6		:= len(Alltrim(xFilial("SA6")) )
Local nTamSA1		:= len(Alltrim(xFilial("SA1")) )
Local aCodCli		:= "" //Codigo do cliente da venda

Private lAtuSitua 	:= .F.			//Indica se atualiza o L1_SITUA - Por causa do GravaBat
DbSelectArea("SL4")//
DbSelectArea("SL2")//PIO
DbSelectArea("SL1") //PIN Tabela clone da SL1

//��������������������������Ŀ
//?echas Alias Temporarios. ?
//����������������������������
FechaAlias(.T.)

//Executa a query
//cQuery := ChangeQuery( cQuery )					
DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), "TMPSL1", .T., .F. )
TcSetField("TMPSL1", "L1_EMISSAO", "D")

//Conout( "INTM210","INICIO      : "+DtoC(Date())+" Hora: "+Time() )
//Conout( "INTM210","PROCESSANDO : "+If(TMPSL1->PIN_ACAO == "1","Inclus?", ;
//								   If(TMPSL1->PIN_ACAO == "2","Altera��o","Exclus?")) )
If !lJob
	ProcRegua(TMPSL1->(RecCount()))
Endif

While TMPSL1->(!EOF())
	If !lJob
		IncProc()
    Endif
	//Limpa Variaveis  
	lAppend	:= .F.
	cErro		:= ""    
	lErro 		:= .F.	
	aInfoLog1	:= {}  
	aInfoLog2	:= {}
	aSL1		:= {}
	aSL2		:= {}                            
	aSL4		:= {}
	aSL1Vazio	:= {}               
	cDOCPED 	:= "" 
	cLjCodigo	:= ""
	lContinua 	:= .T.  
	lSemReserva	:= .F.
	lHistorico	:= .F.
	lExistSL1 	:= .F.
	lAtuSitua 	:= .F.
	cEstacao 	:= ""
	cSerie   	:= ""
	lSAT		:= .F.
	lNFCe		:= .F.
	lImpFis	:= .F.
	cEspecie	:= ""
	
    nLidos     ++
    	
	
	//Carrega informacoes para gravacao de Log
	nRecSL1 := TMPSL1->REGISTRO
	cChaveSL1 := TMPSL1->L1_UMOV

	Aadd(aInfoLog1, "SL1"	)
	Aadd(aInfoLog1, nRecSL1)      

	//Verifica se o Registro ja foi processado, tratamento para Multi Thread.?
	If lMultThread
		SL1->(DbGoTo(nRecSL1))
		//!Empty(PIN->PIN_STAIMP) .AND. PIN->PIN_STAIMP <> "4"
		If SL1->L1_SITUA <> "IP"
			//Passa ao proximo registro
			TMPSL1->(DbSkip())
			Loop
		Else
			If !SL1->L1_ORIGEM == 'N' 	
				AtuaOrigem(nRecSL1)
			EndIf
		EndIf
	Else
		If !SL1->L1_ORIGEM == 'N' 	
			AtuaOrigem(nRecSL1)
		EndIf
	EndIf
	
	//��������������������������Ŀ
	//?echas Alias Temporarios. ?
	//����������������������������
	FechaAlias()
	
	RmiFilInt( TMPSL1->L1_FILIAL, .T., TMPSL1->L1_CGCCLI)
	//��������������������������������?
	//?arrega Variaveis de Controle. ?
	//��������������������������������?
	cTipoOrc	:= AllTrim( TMPSL1->L1_TIPO 	)    
	//Para integra��o RMI o Campo L1_ORIGEM deve estar igual a 'N'
	//Posiciona na SL1.
	SL1->(dbGoTo(TMPSL1->REGISTRO))
	//Valida se Doc e Serie j� existe na Filial.
	If VldSL1Dup(SL1->L1_FILIAL,SL1->L1_DOC,SL1->L1_SERIE)
		cErro := STR0129 + " - "+SL1->L1_DOC + " - " + SL1->L1_SERIE
		RMIGRVLOG("IR", "SL1" , nRecSL1, "STR0129" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")//[DUPL] Identificado um registro com o mesmo Documento/Serie: 
		cErro := ""
		lErro := .T.
	EndIf
	
	If !RMIEstacao(SL1->L1_FILIAL	,SL1->L1_PDV	,SL1->L1_SERIE	,SL1->L1_ESPECIE,;
					@cEstacao		,@cSerie		,@lSAT			,@lNFCe			,;
					@lImpFis		,@cEspecie		,nRecSL1		,SL1->L1_SERSAT	)
		lErro	:= .T.	
	EndIf
	
	//Verifica se existem vendas duplicadas na base em relacao a venda que esta sendo processada no momento.
	//Criar uma tarefa para validar venda duplicada.
	/*If !M210VndDup(	PIN->PIN_FILIAL	,PIN->PIN_XCODEX,nRecSL1		,PIN->PIN_DOC	,;
					cSerie			,PIN->PIN_PDV	,PIN->L1_XTPREG,lSAT			,;
					lNFCe			,lImpFis		,cEstacao		)
		TMPSL1->( DbSkip() )
		Loop
	EndIf*/
	//Quando for uma venda do tipo SAT ou NFC-e Mater baseado na SM0
	If (lSAT .Or. lNFCe)
		If !(Alltrim(SM0->M0_CGC) == SubStr(Alltrim(SL1->L1_KEYNFCE),7,14))
			//"CNPJ da Filial de origem diferente do contido na Chave da NFC-e/SAT: "
			cErro := STR0139 + Alltrim(SM0->M0_CGC) + " " + Alltrim(SL1->L1_KEYNFCE)
			RMIGRVLOG("IR", "SL1" , nRecSL1, "STR0139" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			TMPSL1->( DbSkip() )
			Loop
		EndIf
	EndIf
	//Retirado Valida se a Loja esta corretamente Cadastrada. Informacoes necessarias para o GRAVABAT.
	//Retirado Verifica se he Venda de Historico.
	//Retirado Verifica se eh Cupom Nao Fiscal\Cupom Fiscal\Entrega Nao Fiscal e Nao eh Historico. ?
	//Retirado Caso para validar venda com Entrega, RMI ainda n�o processa pedido de venda.
	
	//Analisa se campos obrigatorios estao em vazios.
	Aadd(aSL1Vazio, "L1_DOC")    
	Aadd(aSL1Vazio, "L1_PDV")      
	Aadd(aSL1Vazio,	"L1_OPERADO")
	//Analisa se campos obrigatorios estao em vazios.
	For nI:= 1 to Len(aSL1Vazio) 
		If Empty( &("TMPSL1->"+aSL1Vazio[nI])) 
			cErro += STR0002 + " " + aSL1Vazio[nI] //+ ENTER
		EndIf
	Next nI
	
	//Trava o Log dos campos obrigatorios. ? 
	If !Empty(cErro)
		RMIGRVLOG("IR", "SL1" , nRecSL1, "STR0002" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		TMPSL1->(DbSkip())
		Loop      
	EndIf
	
	//Valida o Cliente. ?
	//Caso o tipo de Registro seja 5 pegar o cliente padr�o? DAC 09/01/2015
	If l210CLIOK
		If U_I210CLIOK(@cChaveSA1,@nOrdem,nRecSL1)
			If ValType(cChaveSA1) <> "C" .Or. ValType(nOrdem) <> "N" .Or. Empty(cChaveSA1) .Or. nOrdem <= 0
				lErro := .T.
				RMIGRVLOG("IR", "SL1" , nRecSL1, "STR0021" , "Retorno do PE I210CLIOK incorreto",,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			EndIf
		Else
			lErro := .T.
			RMIGRVLOG("IR", "SL1" , nRecSL1, "STR0021" , "Retorno do PE I210CLIOK sem cliente informado",,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		EndIf
	Else
		If !Empty(TMPSL1->L1_CLIENTE)//PIN_CLIENT
		  	cChaveSA1 := SUBSTR(TMPSL1->L1_FILIAL,1,nTamSA1) + SPACE (TamSX3("A1_FILIAL")[1] - nTamSA1) + TMPSL1->L1_CLIENTE + TMPSL1->L1_LOJA //TMPSL1->PIN_CLIENT + TMPSL1->PIN_LOJA
		  	nOrdem := 1
		ElseIf !Empty(TMPSL1->L1_CGCCLI) //PIN_XCPFC
		  	cChaveSA1 := SUBSTR(TMPSL1->L1_FILIAL,1,nTamSA1) + SPACE (TamSX3("A1_FILIAL")[1] - nTamSA1) + TMPSL1->L1_CGCCLI
		  	nOrdem := 3
		/*ElseIf TMPSL1->PIN_XTPREG == '5' .and. !Empty(cCliPad)//PIN_XTPREG
		  	cChaveSA1 := xFilial("SA1") + cCliPad + cLojaPad
		  	nOrdem := 1*/
		Else
			lErro := .T.
			RMIGRVLOG("IR", "SL1", nRecSL1, STR0021,"N? Informado CPF no Cupom e n? preenchido parametro MV_CLIPAD",,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")// Validacao usuario PE l210INCOK
		EndIf
	EndIf

	/*If !lErro 
		cErro 	:= STR0021 + " " + cChaveSA1  //TMPSL1->PIN_XCPFC	
		aAux 	:= ValCampo("SA1", {"A1_COD", "A1_LOJA", "A1_TIPO"}, cChaveSA1, nOrdem, @cErro, "STR0021")

		If Len(aAux) > 0
			cCliente	:= aAux[1]
			cLoja		:= aAux[2]
			cTipoCli	:= aAux[3]
		Else
			lErro := .T.	
		EndIf
	Endif
	//Nao eh necessario esse trecho pois a rotina de de/para faz a mesma funcao.
	*/
	//��������������������?
	//?alida o Vendedor. ?
	//��������������������?    
	lRet := .T.         
		//Ponto de Entrada para validacao do registro
	If l210INOK
		lRet210I := ExecBlock("I210INCOK",.F.,.F., { nRecSL1 } )
		If Valtype(lRet210I) <> "L"
			lRet210I := .T.
		EndIf
		
		If !lRet210I
			lRet := .F.
			RMIGRVLOG("IR", "SL1", nRecSL1, "STR210I","STR210I",,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		Endif
	EndIf
	//Acrescentado valida��o conforme solicita��o Gendra 21/08/2014 esta valida��oser?utilizada emv?ias empresas
	Begin Sequence
		
		//Valida a Data Fiscal?

			_cDataFis := DtoS(SuperGetMV( "MV_DATAFIS",,.F.,SL1->L1_FILIAL))
			If DtoS(TMPSL1->L1_EMISSAO) <= _cDataFis
				cMens := "Data do Cupom Fiscal (L1_EMISSAO) "+DtoC(TMPSL1->L1_EMISSAO)+" menor/igual que Data de Fechamento (MV_DATAFIS) "+Subs(_cDataFis,7,2)+"/"+Subs(_cDataFis,5,2)+"/"+Subs(_cDataFis,3,2)
				RMIGRVLOG("IR", "SL1", nRecSL1, "STR0200",cMens,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
				lRet := .F.
			Endif

    End Sequence
	//passar referencia para erro
	If !lRet
		lErro	:= .T.	
	Endif
		
	
	//Verifica se tem erro no regsitro corrente. ?
	If !Empty(cErro) .OR. 	lErro
		TMPSL1->(DbSkip())
		Loop      
	EndIf	
	
	//Carega array com as informacoes do cabecario. ?
	nVlrTot := Round( TMPSL1->L1_VLRTOT, nCasaDecimal )
 	//Caso o mesmo venha vazio utilizar a data do cupom
	If Empty(TMPSL1->L1_DTLIM)  //Caso esteja vazio utilizar data do Cupom
		_dDtLim := TMPSL1->L1_EMISNF 
	Else
		_dDtLim := TMPSL1->L1_DTLIM 
	Endif
	
	If SL1->(FieldPos('L1_VEND')) > 0  
		cVendedor := IIF(Empty(TMPSL1->L1_VEND),cVendPad,TMPSL1->L1_VEND)
	Else
		cVendedor := cVendPad 
	EndIf

	aSL1 := {}
	//Aadd( aSL1, {"L1_FILIAL"	, PIN->PIN_FILIAL	} )
	//Aadd( aSL1, {"L1_NUM"	 	, ""					} )		//Numero do Orcamento sera incluido no final
	//Aadd( aSL1, {"L1_PDV"    	, TMPSL1->PIN_PDV		} )

	If lDePara
		cVendedor := RmiPsqDePa(cMvSisOri, "SA3", "A3_COD", cVendedor, 1, xFilial("SA3")+cVendedor)
		If Empty(cVendedor)
			RmiGrvLog("IR"	, "SL1"	, nRecSL1 , "STR1001" , STR1001,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS") //"O vendedor do R_E_C_N_O_ informado, n�o existe na tabela SA3 (Vendedores) e tamb�m no cadastro de De/para (MHM)"
			lErro := .T.
		Else
			Aadd( aSL1, {"L1_VEND"   	, cVendedor				} )
		EndIf			
	EndIf

	Aadd( aSL1, {"L1_NUMCFIS"	, TMPSL1->L1_DOC		} )
	//Aadd( aSL1, {"L1_RESERVA"  ,IIF(Empty(TMPSL1->L1_RESERVA),"S",TMPSL1->L1_RESERVA)} )	
	Aadd( aSL1, {"L1_EMISNF" 	, TMPSL1->L1_EMISSAO	} )
	//Aadd( aSL1, {"L1_EMISSAO"	, TMPSL1->PIN_EMISSA	} )
	Aadd( aSL1, {"L1_DTLIM"  	,IIF(Empty(TMPSL1->L1_DTLIM),_dDtLim,TMPSL1->L1_DTLIM)	} )

	If lDePara
		cOperador := RmiPsqDePa(cMvSisOri, "SA6", "A6_COD", IIF(Empty(TMPSL1->L1_OPERADO),cOperador,TMPSL1->L1_OPERADO), 1, xFilial("SA6")+IIF(Empty(TMPSL1->L1_OPERADO),cOperador,TMPSL1->L1_OPERADO))
		If Empty(cOperador)
			RmiGrvLog("IR"	, "SL1"	, nRecSL1 , "STR1003" , STR1003,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS") //"O operador do R_E_C_N_O_ informado, n�o existe na tabela SA6 (Bancos) e tamb�m no cadastro de De/para (MHM)"
			lErro := .T.
		Else
			Aadd( aSL1, {"L1_OPERADO"	, cOperador} )
		EndIf			
	EndIf

	//Aadd( aSL1, {"L1_COMIS" 	, TMPSL1->PIN_COMIS		} )
	//Aadd( aSL1, {"L1_VEND2"  	, ""					} )
	//Aadd( aSL1, {"L1_VEND3"  	, ""					} )	

	If lDePara
		aCodCli := RmiPsqDePa(cMvSisOri, "SA1", "A1_COD", IIF(Empty(TMPSL1->L1_CLIENTE),cCliente,TMPSL1->L1_CLIENTE), 1, xFilial("SA1")+IIF(Empty(TMPSL1->L1_CLIENTE),cCliente,TMPSL1->L1_CLIENTE)+IIF(Empty(TMPSL1->L1_LOJA), cLoja,TMPSL1->L1_LOJA))
		If Len(aCodCli) < 2
			RmiGrvLog("IR"	, "SL1"	, nRecSL1 , "STR1002" , STR1002,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS") //"O cliente do R_E_C_N_O_ informado, n�o existe na tabela SA1 (Clientes) e tamb�m no cadastro de De/para (MHM)"			
			lErro := .T.
		Else
			Aadd( aSL1, {"L1_CLIENTE"	, aCodCli[1]} )
			Aadd( aSL1, {"L1_LOJA"   	, aCodCli[2]} )
		EndIf			
	EndIf

	Aadd( aSL1, {"L1_TIPOCLI"	,IIF(Empty(TMPSL1->L1_TIPOCLI), cTipoCli,TMPSL1->L1_TIPOCLI)} )
	//Aadd( aSL1, {"L1_VLRTOT" 	, TMPSL1->PIN_VLRTOT	} )
	//Aadd( aSL1, {"L1_DESCONT"	, TMPSL1->PIN_DESCON	} )		//{"L1_DESCONT"	, ABS(TMPSL1->PIN_DESCON)	},;	- RETIRADO O DESCONTO PORQUE ESTAVA DESCONTANDO 2 VEZES NO SF2
	//Aadd( aSL1, {"L1_VLRLIQ" 	, TMPSL1->PIN_VLRTOT	} )
	//Aadd( aSL1, {"L1_VALBRUT"	, TMPSL1->PIN_VALBRU	} )
	//Aadd( aSL1, {"L1_VALMERC"	, TMPSL1->PIN_VALMER	} )		//{"L1_VALMERC"	, nVlrTot				},;
	Aadd( aSL1, {"L1_DINHEIR"	, 0	                    } )
	Aadd( aSL1, {"L1_CHEQUES"	, 0	                    } )
	Aadd( aSL1, {"L1_CARTAO" 	, 0	                    } )
	Aadd( aSL1, {"L1_CONVENI"	, 0	                    } )
	Aadd( aSL1, {"L1_VALES"  	, 0		                } )
	Aadd( aSL1, {"L1_FINANC" 	, 0	                    } )	
	Aadd( aSL1, {"L1_VLRDEBI"	, 0	                    } )
	Aadd( aSL1, {"L1_OUTROS" 	, 0	                    } )
	Aadd( aSL1, {"L1_ENTRADA"	, 0	                    } )
	Aadd( aSL1, {"L1_CREDITO"	, 0                 	} )    
	//Aadd( aSL1, {"L1_JUROS"  	, TMPSL1->PIN_JUROS		} )
	//Aadd( aSL1, {"L1_PARCELA"	, TMPSL1->PIN_PARCEL	} )
	//Aadd( aSL1, {"L1_COND" 	, TMPSL1->PIN_COND		} )	
	//Aadd( aSL1, {"L1_VENDTEF"	, TMPSL1->PIN_VENDTE	} )
	//Aadd( aSL1, {"L1_DATATEF"	, TMPSL1->PIN_DATATE	} ) 
	//Aadd( aSL1, {"L1_HORATEF"	, TMPSL1->PIN_HORATE	} ) 
	//Aadd( aSL1, {"L1_AUTORIZ"	, TMPSL1->PIN_AUTORI	} ) 
	//Aadd( aSL1, {"L1_NSUTEF"	, TMPSL1->PIN_NSUTEF	} ) 
	//Aadd( aSL1, {"L1_CONDPG" 	, "CN"					} )
	//Aadd( aSL1, {"L1_DESPESA"	, TMPSL1->PIN_DESPES	} )
	//Aadd( aSL1, {"L1_CONFVEN"	, "SSSSSSSSNSSS"		} )
	//Aadd( aSL1, {"L1_ESTACAO"	, cEstacao				} )	
	Aadd( aSL1, {"L1_MOEDA"	,IIF(Empty(TMPSL1->L1_MOEDA),nMoedaCor,TMPSL1->L1_MOEDA)} )
	//Aadd( aSL1, {"L1_TPORC"	, TMPSL1->PIN_TPORC		} )
	//Aadd( aSL1, {"L1_PLIQUI"	, TMPSL1->PIN_PLIQUI	} )
	//Aadd( aSL1, {"L1_PBRUTO"	, TMPSL1->PIN_PBRUTO	} )
	//Aadd( aSL1, {"L1_NUMORC"	, TMPSL1->PIN_NUMORC	} )
	//Aadd( aSL1, {"L1_TPFRET"	, TMPSL1->PIN_TPFRET	} )
	//Aadd( aSL1, {"L1_TRANSP"	, TMPSL1->PIN_TRANSP	} )
	//Aadd( aSL1, {"L1_FRETE"	, TMPSL1->PIN_FRETE		} )
	//Aadd( aSL1, {"L1_CGCCLI"	, TMPSL1->PIN_CGCCLI	} )
	//Aadd( aSL1, {"L1_BRICMS"	, TMPSL1->PIN_BRICMS	} )  
	//Aadd( aSL1, {"L1_ICMSRET"	, TMPSL1->PIN_ICMSRE	} )  
	//Aadd( aSL1, {"L1_IMPRIME"	, IIF(Empty(TMPSL1->PIN_IMPRIM),"1N",TMPSL1->PIN_IMPRIM)	} )  
	//Aadd( aSL1, {"L1_SERSAT"	, TMPSL1->PIN_SERSAT	} )
	//Aadd( aSL1, {"L1_KEYNFCE"	, TMPSL1->PIN_KEYNFC	} )
	//Aadd( aSL1, {"L1_DOCCANC"	, TMPSL1->PIN_DOCCAN	} )
	//Aadd( aSL1, {"L1_DATCANC"	, TMPSL1->PIN_DATCAN	} )
	//Aadd( aSL1, {"L1_XTPREG" 	, TMPSL1->PIN_XTPREG	} )
	//Aadd( aSL1, {"L1_XCODEX"	, TMPSL1->PIN_XCODEX	} )
	Aadd( aSL1, {"L1_ESPECIE"	,IIF(Empty(TMPSL1->L1_ESPECIE),cEspecie,TMPSL1->L1_ESPECIE)} )
				
	//If PIN->(FieldPos('PIN_DESCNF')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_DESCNF"	, TMPSL1->PIN_DESCNF	} )
	//Endif
				
	//If PIN->(FieldPos('PIN_INTERV')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_INTERV"	, TMPSL1->PIN_INTERV	} )
	//Endif

	//If PIN->(FieldPos('PIN_TXDESC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_TXDESC"	, TMPSL1->PIN_TXDESC	} )
	//Endif

	//If PIN->(FieldPos('PIN_TXMOED')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_TXMOEDA"	, TMPSL1->PIN_TXMOED} )
	//Endif

	//If PIN->(FieldPos('PIN_FORMA')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_FORMA"	, TMPSL1->PIN_FORMA} )
	//Endif

	//If PIN->(FieldPos('PIN_VALICM')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALICM"	, TMPSL1->PIN_VALICM} )
	//Endif

	//If PIN->(FieldPos('PIN_VALIPI')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALIPI"	, TMPSL1->PIN_VALIPI} )
	//Endif

	//If PIN->(FieldPos('PIN_VALISS')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALISS"	, TMPSL1->PIN_VALISS} )
	//Endif

	//If PIN->(FieldPos('PIN_FORMPG')) > 0  //Incluido DAC 17/01/2017
	Aadd( aSL1, {"L1_FORMPG", ""} )
	//Endif

	//If PIN->(FieldPos('PIN_VEND2')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VEND2"	, TMPSL1->PIN_VEND2} )
	//Endif

	//If PIN->(FieldPos('PIN_VEND3')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VEND3"	, TMPSL1->PIN_VEND3} )
	//Endif
 
	//If PIN->(FieldPos('PIN_MULTNO')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_MULTNOT"	, TMPSL1->PIN_MULTNO} )
	//Endif

	//If PIN->(FieldPos('PIN_NUMCFI')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_NUMCFIS"	, TMPSL1->PIN_NUMCFI} )
	//Endif

	//If PIN->(FieldPos('PIN_FATOR')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_FATOR"	, TMPSL1->PIN_FATOR} )
	//Endif

	//If PIN->(FieldPos('PIN_DOCTEF')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_DOCTEF"	, TMPSL1->PIN_DOCTEF} )
	//Endif

	//If PIN->(FieldPos('PIN_INSTIT')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_INSTITU"	, TMPSL1->PIN_INSTIT} )
	//Endif
                
	//If PIN->(FieldPos('PIN_TIPCAR')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_TIPCART"	, TMPSL1->PIN_TIPCAR} )
	//Endif

	//If PIN->(FieldPos('PIN_TEFSTA')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_TEFSTAT"	, TMPSL1->PIN_TEFSTA} )
	//Endif
   
	//If PIN->(FieldPos('PIN_ADMFIN')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_ADMFIN"	, TMPSL1->PIN_ADMFIN} )
	//Endif

	//If PIN->(FieldPos('PIN_STATUS')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_STATUS"	, TMPSL1->PIN_STATUS} )
	//Endif

	//If PIN->(FieldPos('PIN_HORA')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_HORA"	, TMPSL1->PIN_HORA} )
	//Endif

	//If PIN->(FieldPos('PIN_OPERAC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_OPERACA"	, TMPSL1->PIN_OPERAC} )
	//Endif

	//If PIN->(FieldPos('PIN_NUMORI')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_NUMORIG"	, TMPSL1->PIN_NUMORI} )
	//Endif

	//If PIN->(FieldPos('PIN_ENDCOB')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_ENDCOB"	, TMPSL1->PIN_ENDCOB} )
	//Endif

	//If PIN->(FieldPos('PIN_ENDENT')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_ENDENT"	, TMPSL1->PIN_ENDENT} )
	//Endif

	//If PIN->(FieldPos('PIN_BAIRRC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_BAIRROC"	, TMPSL1->PIN_BAIRRC} )
	//Endif
                       
	//If PIN->(FieldPos('PIN_CEPC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_CEPC"	, TMPSL1->PIN_CEPC} )
	//Endif

	//If PIN->(FieldPos('PIN_MUNC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_MUNC"	, TMPSL1->PIN_MUNC} )
	//Endif

	//If PIN->(FieldPos('PIN_ESTC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_ESTC"	, TMPSL1->PIN_ESTC} )
	//Endif

	//If PIN->(FieldPos('PIN_BAIRRE')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_BAIRROE"	, TMPSL1->PIN_BAIRRE} )
	//Endif

	//If PIN->(FieldPos('PIN_CEPE')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_CEPE"	, TMPSL1->PIN_CEPE} )
	//Endif

	//If PIN->(FieldPos('PIN_MUNE')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_MUNE"	, TMPSL1->PIN_MUNE} )
	//Endif

	//If PIN->(FieldPos('PIN_ESTE')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_ESTE"	, TMPSL1->PIN_ESTE} )
	//Endif

	//If PIN->(FieldPos('PIN_SEGURO')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_SEGURO"	, TMPSL1->PIN_SEGURO} )
	//Endif

	//If PIN->(FieldPos('PIN_VOLUME')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VOLUME"	, TMPSL1->PIN_VOLUME} )
	//Endif

	//If PIN->(FieldPos('PIN_NRDOC')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_NRDOC"	, TMPSL1->PIN_NRDOC} )
	//Endif

	//If PIN->(FieldPos('PIN_TROCO1')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_TROCO1"	, TMPSL1->PIN_TROCO1} )
	//Endif

	//If PIN->(FieldPos('PIN_CONTRA')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_CONTRA"	, TMPSL1->PIN_CONTRA} )
	//Endif

	//If PIN->(FieldPos('PIN_VALPIS')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALPIS"	, TMPSL1->PIN_VALPIS} )
	//Endif

	//If PIN->(FieldPos('PIN_VALCOF')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALCOFI"	, TMPSL1->PIN_VALCOF} )
	//Endif

	//If PIN->(FieldPos('PIN_VALCSL')) > 0  //Incluido DAC 17/01/2017
	//	Aadd( aSL1, {"L1_VALCSLL"	, TMPSL1->PIN_VALCSL} )
	//Endif

	/*If cXTPREG  == ORCAMENTO
		Aadd(aSL1, {"L1_TIPO"   	, ""				})
		Aadd(aSL1, {"L1_SITUA"  	, ""				})
		Aadd(aSL1, {"L1_NUMMOV"  	, ""				})    
		
	ElseIf cXTPREG  == CUPNAOFISCAL
		Aadd(aSL1, {"L1_DOCPED"  	, TMPSL1->PIN_DOCPED	})
		Aadd(aSL1, {"L1_SERPED"  	, cSerie				})	                       		
		Aadd(aSL1, {"L1_TIPO"   	, "P"					})
		Aadd(aSL1, {"L1_SITUA"  	, ""					})		//Sera atualizado no final na funcao GeraOrc 
		Aadd(aSL1, {"L1_NUMMOV"  	, ""					})
		lAtuSitua := .T.
		
	ElseIf cXTPREG  == CUPOMFISCAL
		Aadd(aSL1, {"L1_DOC"    	, TMPSL1->PIN_DOC	})
		Aadd(aSL1, {"L1_SERIE"  	, cSerie			})
		Aadd(aSL1, {"L1_DOCPED"  	, ""				})
		Aadd(aSL1, {"L1_SERPED"  	, ""				})	                       		
		Aadd(aSL1, {"L1_TIPO"   	, "V"				}) 
		Aadd(aSL1, {"L1_SITUA"  	, ""				})			//Sera atualizado no final na funcao GeraOrc 
		Aadd(aSL1, {"L1_NUMMOV"  	, "1"				})
		lAtuSitua := .T.
  	
  	EndIf*/

	Aadd(aSL1, {"L1_SERIE"  	, cSerie			})

	//If PIN->(FieldPos('PIN_LJORI')) > 0 .AND. SL1->(FieldPos('L1_LJORI'))	> 0  //Incluido  16/02/2018
	//	Aadd( aSL1, {"L1_LJORI"	, TMPSL1->PIN_LJORI} )
	//Endif

	//If PIN->(FieldPos('PIN_CNPJOR')) > 0 .AND. SL1->(FieldPos('L1_CNPJOR'))> 0   //Incluido  16/02/2018
	//	Aadd( aSL1, {"L1_CNPJOR"	, TMPSL1->PIN_CNPJOR} )
	//Endif
          
	//If PIN->(FieldPos('PIN_SERPDV')) > 0 .AND. SL1->(FieldPos('L1_SERPDV'))> 0   //Incluido  16/02/2018
	//	Aadd( aSL1, {"L1_SERPDV"	, TMPSL1->PIN_SERPDV} )
	//Endif

	//If PIN->(FieldPos('PIN_CHVORI')) > 0 .AND. SL1->(FieldPos('L1_CHVORI'))> 0   //Incluido  16/02/2018
	//	Aadd( aSL1, {"L1_CHVORI"	, TMPSL1->PIN_CHVORI} )
	//Endif
            
   	//Ponto de Entrada criado para retornar Campo e conte?o para execu��o no Execauto, somente duas posi��es por Array DAC 17/01/2017
	If ExistBlock("IN210AL1")        
		SL1->(DbGoto(nRecSL1))  //Garanto posicionamento na tabela da Camada de Integra��o 
		_aL1Mat := {}
   		_aL1Mat := ExecBlock("IN210AL1",.F.,.F., { nRecSL1 })
   		If ValType(_aL1Mat) == "A" .and. Len(_aL1Mat) > 0
			For nPos := 1 To Len(_aL1Mat) 
				If Len(_aL1Mat[nPos]) <> 2   //somente duas posu��es nome do campo e conteudo     
					lErro 	:= .T.	
					cErro 	:= STR0124   //"Erro no retorno do Ponto de Entrada IN210AL1, retornar Array com 2 Posi��es [nome do campo | conteudo do campo] "
					RMIGRVLOG("IR", "SL1", nRecSL1, "STR0124", "STR0124",,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
					Exit
				EndIf
				//Verificar se o campo esta incluindo na matriz principal caso esteja o mesmo ser?alterado conforme o retorno do P,E.            
				nPosCpo := Ascan( aSL1, { |x| AllTrim(x[1]) == AllTrim(_aL1Mat[nPos,1]) } )
				If nPosCpo > 0
					aSL1[nPosCpo,2]	:= _aL1Mat[nPos,2]
				Else 
					Aadd(aSL1 ,{	AllTrim(_aL1Mat[nPos,1]),;  	//Nome do Campo
									_aL1Mat[nPos,2],; 				//Conte?o do Campo
									Nil})      	
				Endif					
			Next nPos
      	EndIf
	EndIf

	If !Empty(cErro) .OR. 	lErro
		TMPSL1->(DbSkip())
		Loop      
	EndIf	

	//����������������Ŀ
	//?era Itens SL2. ?
	//������������������
 	ValidSL2(	SL1->L1_FILIAL	,TMPSL1->L1_NUM , TMPSL1->L1_PDV	, TMPSL1->L1_DOC		,;
				cSerie				, nRecSL1			, aCodCli[1]			, aCodCli[2]					,;
				cVendedor			, cSituacao			, nVlrTot			, TMPSL1->L1_DOCPED	,;
				cXTPREG			, cDOCPED			, lSemReserva		, "",;
				TMPSL1->L1_SITUA	, lHistorico 		, cLjCodigo,""	)

	If Len(aSL2) > 0
	    nProcessados ++  	
		nItemPrc     += Len(aSL2)
	Endif		
	
	TMPSL1->( DbSkip() )
End

Conout("RmiRetailJob","FINAL                : "+DtoC(Date())+" Hora: "+Time())
Conout("RmiRetailJob","REGISTROS LIDOS      : "+StrZero(nLidos,6))
Conout("RmiRetailJob","REGISTROS PROCESSADOS: "+StrZero(nProcessados,6))
Conout("RmiRetailJob","REGISTROS PROC. ITENS: "+StrZero(nItemPrc,6))

LjGrvLog("ValidSL1","RmiRetailJob","FINAL                : "+DtoC(Date())+" Hora: "+Time())
LjGrvLog("ValidSL1","RmiRetailJob","REGISTROS LIDOS      : "+StrZero(nLidos,6))
LjGrvLog("ValidSL1","RmiRetailJob","REGISTROS PROCESSADOS: "+StrZero(nProcessados,6))
LjGrvLog("ValidSL1","RmiRetailJob","REGISTROS PROC. ITENS: "+StrZero(nItemPrc,6))


TMPSL1->( DbCloseArea() )

//������������������������������?
//?estaura a Empresa e Filial. ?
//������������������������������?
RmiFilInt(cFilAnt, .F.)

Return(Nil)      


//-------------------------------------------------------------------
/*/{Protheus.doc} ValidSL2
Gera arquivo temporario TMPSL2 atraves do arquivo Legado 
@param 
@return Vazio
@Obs	ValidSL2 para ValidSL2
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidSL2(	cFilSL1	, cNum		, cPdv			, cDoc			,;
							cSerie		, nRecSL1	, _cCliente		, _cLoja		,;
							cVendedor	, cSituacao	, nVlrTotL1		, cDocPed		,;
							cXTPREG	, cDOCPED 	, lSemReserva	, cXTPREGOld	,;
							cAcao		, lHistorico, cLjCodigo     , cCNPJ		)

Local cQuery        := ""
Local nVlTotItem	 := 0									//Valor total dos itens L2_VLRITEM
Local nTolerancia	 := SuperGetMv("IN_TOLPIO"	,, 0.05)	//Valor de tolerancia
//Local nMV_TXPIS		 := SuperGetMV("MV_TXPIS"	,, 0.05)
//Local nMV_TXCOFIN 	 := SuperGetMV("MV_TXCOFIN"	,, 0.05) 
//Local _lItemLJCli    := SuperGetMv("IN_ITLJCLI"	,, .F.)	//indica se ser?utilizado o item enviado por esta cusomiza��o e ou se utilizara o padr? que montara utilizando o SOMA1 DAC 10/03/2016
Local nALIQPS2		:= 0
Local nALIQCF2		:= 0
Local nBASEPS2		:= 0
Local nBASECF2		:= 0
Local nVALPS2   	:= 0
Local nVALCF2		:= 0
Local nDiferenca	:= 0													//Diferenca do total da L1 para L2
Local nCasaDecimal	:= TamSX3("L1_VLRTOT")[2] 								//Quantidade de digitos para a casa decimal
Local aSL2Vazio	:= {}													//Relacao de campos que nao podem estar vazios
Local nTamIt	 	:= TamSx3("L2_ITEM")[1]
Local cItem		 	:= Repl("0", nTamIt)
Local aAuxSL2		:= {}													//Contem o Item do orcamento
Local cProduto		:= ""													//Codigo do produto
Local cTES			:= ""													//Retorno via tabela PJA Tes
Local cTesPed 		:= ""													//Tes que sera gerado o Pedido de Venda F4_XTESPED
Local cCF			:= ""													//Retorno via tabela PJA Classificacao Fiscal
Local cSitTri		:= ""													//Retorno via tabela PJA Situacao Tributaria
Local cConta		:= ""
Local aAux			:= {}													//Pega o retorno da funcao ValCampo
Local aPIORecnos	:= {}													//Recnos dos registros da tabela PIO
Local lErro 		:= .F.
Local lRet			:= .T.
Local cLocal 		:= ""  
Local cUnidade		:= ""													//Unidade de medida do produto
Local cGrpTrib		:= ""													//Grupo Tributario do Produto
Local cLocal2 		:= SuperGetMv("ES_0000037",, "01")						//Armazem trocar
Local nI			:= 0
Local cVendItem		:= ""			//Codigo do Vendedor por Item  
// Local _cTpOper		:= SuperGetMV("IN_TPOPPAD",,"01")						//Tipo de Operacao padrao
Local cTpOper		:= SuperGetMV("MV_LJOPTES",,"01")						//Tipo de Operacao padrao
Local nPosCpo
Local _aL2Mat                           
Local nPos
Local cCodProd		:= "" //Codigo do produto a ser gravado na SL2

//filtrar apenas por Filial e cNUm
cQuery := "SELECT SL2.*, SL2.R_E_C_N_O_ as REGISTRO FROM " + RetSQLName("SL2") + " SL2 "
cQuery += " WHERE 	SL2.L2_FILIAL 	= '" + cFilSL1 		+ "' 	AND "
cQuery += " 	 	SL2.L2_NUM 		= '" + cNum			+ "' 	AND "
cQuery += " 		SL2.D_E_L_E_T_  <> '*'"
cQuery += "ORDER BY SL2.L2_FILIAL, SL2.L2_ITEM"

//Execulta a query
cQuery := ChangeQuery( cQuery )
DbUseArea( .T., "TOPCONN", TcGenQry( , , cQuery), "TMPSL2", .T., .F. )

If lFilCnpj//remover
	//Aadd(aSL2Vazio, "L2_CNPJOR"	)
Else
	Aadd(aSL2Vazio, "L2_FILIAL"	)
EndIf
Aadd(aSL2Vazio, "L2_NUM"	)
Aadd(aSL2Vazio, "L2_ITEM"		)

//Verifica se nao eh orcamento para adicionar outros campos para validacao.
If !Empty(cSituacao)
	Aadd(aSL2Vazio, "L2_PDV"	)    
EndIf	


//Verifica se encontrou os Itens
If TMPSL2->(Eof())
	cErro := STR0064		//"Nao foram encontrados itens para o or?mento - PIO (Itens do cupom fiscal)."
	RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0064" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
Else            

	While !TMPSL2->( Eof() )

		//Limpa variaveis
		cProduto 	:= ""
		cDescricao 	:= ""
		cUnidade 	:= ""
		cTES 		:= ""
		cTesPed		:= ""
		cCF	 		:= ""
		cSitTri		:= ""
		cLocal		:= ""
		cGrpTrib	:= ""
		cConta		:= ""
		cErro		:= ""
		lErro 		:= .F.
		
		aInfoLog2	:= {}  		
		nALIQPS2	:= 0
		nALIQCF2	:= 0
		nBASEPS2	:= 0
		nBASECF2	:= 0
		nVALPS2     := 0
		nVALCF2		:= 0
	 	lRet		:= .T.
	 	
		//Carrega informacoes para gravacao de Log
		Aadd(aInfoLog2	, "SL2"					)
		Aadd(aInfoLog2	, TMPSL2->R_E_C_N_O_	)
		Aadd( aPIORecnos, TMPSL2->R_E_C_N_O_	)
		
		//Grava Filial e Posiciona a PIO
		
		//Analisa se campos obrigatorios estao em vazios. ?
		For nI:= 1 to Len(aSL2Vazio)
			If Empty( &("TMPSL2->"+aSL2Vazio[nI]))
				cErro += STR0002 + " " + aSL2Vazio[nI] //+ ENTER
			EndIf
		Next nI
		
		//��������������������������������������?
		//?rava o Log dos campos obrigatorios. ?
		//��������������������������������������?
		If !Empty(cErro)
			aSL2 := {}
			RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0002" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			Return(Nil) 
		EndIf
		//Verifica se o campo ITEM ESTA PREENCHIDO
		//Valida se Cupom Fiscal eh Historico. ?
		/*If (cXTPREG <> CUPOMFISCAL) .AND. lHistorico
			aSL2 	:= {}		
 			//cErro	:= STR0092 + " (" + cXTPREG + "\" + TMPSL2->PIO_TES + ")."
 			cErro	:= STR0092 + " (" + cXTPREG + ")."  //N? ESTA UTILIZANDO TPREG

			U_INTM010c("3", "PIN", nRecSL1			, "STR0092"	, cErro	)		//"Cupom de Historico, PIN_XTPREG diferente de '3', inv?ido: PIN_XTPREG\PIO_TES "
			Return(Nil)
		EndIf*/	
		//Valida se Cupom Fiscal teve reservas. ?
		/*If lSemReserva .AND. !lHistorico
			If TMPSL2->L2_ENTREGA == "3" //!Empty(TMPSL2->PIO_ENTREG)  //Alterado por DAC  04/12/2015    1=retira posteriormente 2=retira 3=entrega
				aSL2 	:= {}
	 			cErro	:= STR0074 + " - L2_FILIAL\L2_NUM\L2_PDV " + " (" + cFilSL1 + "\" + cNum + "\" + cPdv + ")."
				RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0074" , cErro)
				//U_INTM010c("4", "PIN", nRecSL1			, "STR0074"	, cErro	)		//"Reserva n? efetuada. N? foi encontrado o or?mento."
				Return(Nil)
			EndIf         
		EndIf*/
		//Valida codigo do produto. ?
		//DbSelectArea("SB1")
    	//SB1->(DbOrderNickName("B1XCODEX"))		
    	/*SB1->(DbSetOrder(1))   //B1_FILIAL+B1_COD
		cErro 	:= STR0008 + " " + TMPSL2->L2_PRODUTO
		aAux	:= ValCampo("SB1", {"B1_COD", "B1_DESC", "B1_LOCPAD", "B1_UM", "B1_GRTRIB", "B1_CONTA"},  xFilial("SB1") + PadR(TMPSL2->L2_PRODUTO,TamSx3("L2_PRODUTO")[1] ), SB1->(IndexOrd()), @cErro, "STR0008")
		If Len(aAux) > 0 
		
			cProduto 	:= aAux[1]
			cDescricao 	:= SubStr(aAux[2], 1, 30)
			cLocal 		:= cLocal2
			cUnidade 	:= aAux[4]
			cGrpTrib	:= aAux[5]
			cConta		:= aAux[6]

			DbSelectArea("SBZ")
			DbSetOrder(1)
			If DbSeek(xFilial("SBZ") + cProduto)
				If Empty(BZ_LOCPAD)
					cLocal := cLocal2
				Else
					cLocal := SBZ->BZ_LOCPAD
				Endif
			Endif			
			CriaSB2(cProduto, cLocal)			

		Else
		
			lErro := .T.
			Return Nil
		EndIf                      
		//Esse trecho nao precisa pois a rotina de De/para faz a mesma fun��o
		*/
		
		//��������������������?
		//?alida Quantidade. ?
		//��������������������?
		If TMPSL2->L2_QUANT <= 0
			lErro 	:= .T.	
			cErro 	:= STR0038 + " " + cValToChar(TMPSL2->L2_QUANT)
			RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0038" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		EndIf
				
		cSitTri	:= TMPSL2->L2_SITTRIB

		//Valida o Vendedor. ?
		//Retirado n�o vai utilizar pedido -> Valida a Tes de Pedido se for uma Entrega nao Fiscal, para gerar o Pedido corretamente pelo GRAVABAT. ?
		cItem := Soma1(cItem, nTamIt)
		//Verifica se tem erro no regsitro corrente. ?
		If !Empty(cErro) .OR. lErro
			aSL2 := {}
			Return(Nil)
		EndIf	       
		//Carrega array com as informacoes do item. ?
		//				{"L2_XJURFI"	, TMPSL2->PIO_XJURFI		},;  //retirado 06/09/2016 DAC
        //				{"L2_GRADE"		, "N"						},;  //retirado 14/09/2016 DAC
		aAuxSL2 := {}
	    //Aadd( aAuxSL2, {"L2_FILIAL"		, cFilSL1				} )
		//Aadd( aAuxSL2, {"L2_NUM"		, ""					} )		//Numero do Orcamento sera incluido no final
		Aadd( aAuxSL2, {"L2_PDV"		, IIF(Empty(TMPSL2->L2_PDV),cPdv,TMPSL2->L2_PDV)} )

		If lDePara
			cCodProd := RmiPsqDePa(cMvSisOri, "SB1", "B1_COD", IIF(Empty(TMPSL2->L2_PRODUTO),cProduto,TMPSL2->L2_PRODUTO), 1, xFilial("SB1")+IIF(Empty(TMPSL2->L2_PRODUTO),cProduto,TMPSL2->L2_PRODUTO))
			If Empty(cCodProd)
				RmiGrvLog("IR"	, "SL1"	, nRecSL1 , "STR1000" , STR1000,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS") //"O produto do R_E_C_N_O_ informado, n�o existe na tabela SB1 (Produtos) e tamb�m no cadastro de De/para (MHM)."
				Return(Nil)
			Else
				Aadd( aAuxSL2, {"L2_PRODUTO"	, cCodProd} )
			EndIf			
		EndIf

		//Busca a TES - Inicio
		cTES := LjRetTes(cTpOper, _cCliente, _cLoja, cCodProd)

		If Empty(cTES)
			lRet := .F.
		EndIf

		If lRet        
			If !SF4->( DbSeek(xFilial("SF4") + cTES ) )			
				lErro 	:= .T.	
				cErro 	:= STR0007 + cTES                                             		
				RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0007" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
				Return(Nil)
			Else
				 
				If Empty(cCF)
					cCF	:= SF4->F4_CF
				EndIf
								
				//Calcula PIS\COFINS
				If SF4->F4_PISCOF <> "4" 	

					nBASEPS2	:= TMPSL2->L2_BASEPS2  
					nBASECF2	:= TMPSL2->L2_BASECF2  
					
					If SF4->F4_PISCRED <> "4"			
						nALIQPS2	:= TMPSL2->L2_ALIQPS2 
						nALIQCF2	:= TMPSL2->L2_ALIQCF2 

						//Vai deixar o GrvBatch recalcular sempre ser� preciso documentar no TDN. Informando que esse campos s�o calculados mesmo que tenha vindo na integra��o
						nVALPS2     := IIF(Empty(TMPSL2->L2_VALPS2),Round((TMPSL2->L2_VLRITEM *  nALIQPS2)/100	, 2),TMPSL2->L2_VALPS2)
						nVALCF2		:= IIF(Empty(TMPSL2->L2_VALCF2),Round((TMPSL2->L2_VLRITEM *  nALIQCF2)/100	, 2),TMPSL2->L2_VALCF2)
					EndIf
				EndIf	
			EndIf
		Else
			lErro 	:= .T.	
			cErro 	:= 	STR0128 + " L2_PRODUTO: " + TMPSL2->L2_PRODUTO + " DESC: " +  Alltrim(Posicione("SB1", 1, TMPSL2->L2_FILIAL + Alltrim(TMPSL2->L2_PRODUTO),"B1_DESC"))
			cErro 	+= " L2_SITTRIB: " + TMPSL2->L2_SITTRIB + " L2_VLRITEM: " + cValToChar(TMPSL2->L2_VLRITEM) + " L2_BASEICM: " + cValToChar(TMPSL2->L2_BASEICM)  
			cErro 	+= " B1_GRTRIB: " + cGrpTrib + " L2_TES: " + TMPSL2->L2_TES
			RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0128" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			Return(Nil)
		EndIf
		//Busca a TES - Fim

		Aadd( aAuxSL2, {"L2_DESCRI" 	, IIF(Empty(TMPSL2->L2_DESCRI),cDescricao,TMPSL2->L2_DESCRI)} )
		Aadd( aAuxSL2, {"L2_LOCAL"		, IIF(Empty(TMPSL2->L2_LOCAL),cLocal,TMPSL2->L2_LOCAL)} )	
		Aadd( aAuxSL2, {"L2_UM"			, IIF(Empty(TMPSL2->L2_UM),cUnidade,TMPSL2->L2_UM)} )
		Aadd( aAuxSL2, {"L2_ITEM"		, IIF(Empty(TMPSL2->L2_ITEM),cItem,TMPSL2->L2_ITEM)} )
		//Aadd( aAuxSL2, {"L2_QUANT"		, TMPSL2->PIO_QUANT		} )
		//Aadd( aAuxSL2, {"L2_VRUNIT"		, TMPSL2->PIO_VRUNIT	} )
		//Aadd( aAuxSL2, {"L2_VLRITEM"	, TMPSL2->PIO_VLRITE	} )
		//Aadd( aAuxSL2, {"L2_DESC"		, TMPSL2->PIO_DESC		} )
		//Aadd( aAuxSL2, {"L2_VALDESC"	, ABS(TMPSL2->PIO_VALDES)} )
		//Aadd( aAuxSL2, {"L2_DESPESA"	, TMPSL2->PIO_DESPES	} )
		//Aadd( aAuxSL2, {"L2_VALIPI"		, 0						} )
		//Aadd( aAuxSL2, {"L2_VALICM"		, TMPSL2->PIO_VALICM	} )
		//Aadd( aAuxSL2, {"L2_VALPIS"		, TMPSL2->PIO_VALPIS	} )
		//Aadd( aAuxSL2, {"L2_VALCOFI"	, TMPSL2->PIO_VALCOF	} )
		//Aadd( aAuxSL2, {"L2_VALCSLL"	, TMPSL2->PIO_VALCSL	} )
		//Aadd( aAuxSL2, {"L2_BASEICM"	, TMPSL2->PIO_BASEIC	} )
		//Aadd( aAuxSL2, {"L2_VENDIDO"	, TMPSL2->PIO_VENDID	} )
		Aadd( aAuxSL2, {"L2_TES"		, cTES} )
		Aadd( aAuxSL2, {"L2_CF"			, IIF(Empty(TMPSL2->L2_CF),cCF,TMPSL2->L2_CF)} )		//{"L2_CF"		, Posicione("SF4", 1, xFilial("SF4") + cTES, "F4_CF")},;
		//Aadd( aAuxSL2, {"L2_ENTREGA"	, TMPSL2->PIO_ENTREG	} )     
		Aadd( aAuxSL2, {"L2_VEND"		, IIF(Empty(TMPSL2->L2_VEND),cVendItem,TMPSL2->L2_VEND)} )//
		Aadd( aAuxSL2, {"L2_ALIQPS2"	, nALIQPS2				} )//J� faz a valida��o quando cria a variavel.
		Aadd( aAuxSL2, {"L2_ALIQCF2"	, nALIQCF2				} )//J� faz a valida��o quando cria a variavel.
		Aadd( aAuxSL2, {"L2_BASEPS2"	, nBASEPS2				} )//J� faz a valida��o quando cria a variavel.
		Aadd( aAuxSL2, {"L2_BASECF2"	, nBASECF2				} )//J� faz a valida��o quando cria a variavel.
		Aadd( aAuxSL2, {"L2_VALPS2"		, nVALPS2     			} )//J� faz a valida��o quando cria a variavel.
		Aadd( aAuxSL2, {"L2_VALCF2"		, nVALCF2				} )//J� faz a valida��o quando cria a variavel.
		//Aadd( aAuxSL2, {"L2_PRCTAB"		, TMPSL2->PIO_PRCTAB	} )
		//Aadd( aAuxSL2, {"L2_EMISSAO"	, TMPSL2->PIO_EMISSA	} )
		//Aadd( aAuxSL2, {"L2_BCONTA"		, ""					} )		//{"L2_MSIMP"	, Dtos(Date())	},;
		Aadd( aAuxSL2, {"L2_SITTRIB"	, cSitTri				} )
		//Aadd( aAuxSL2, {"L2_BRICMS"		, TMPSL2->PIO_BRICMS	} )  
		//Aadd( aAuxSL2, {"L2_ICMSRET"	, TMPSL2->PIO_ICMSRE	} )  
		//Aadd( aAuxSL2, {"L2_XCODEX"		, TMPSL2->PIO_XCODEX	} )
		//Aadd( aAuxSL2, {"L2_GARANT"		, TMPSL2->PIO_GARANT	} )     

		
		//If PIO->( FieldPos("PIO_SEGURO") ) > 0			
			//Aadd(aAuxSL2, {"L2_SEGURO"	, TMPSL2->PIO_SEGURO		}) //Implementado DAC 04/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_LOCAL") ) > 0			
			//Aadd(aAuxSL2, {"L2_LOCAL"	, TMPSL2->PIO_LOCAL		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_DOC") ) > 0			
			//Aadd(aAuxSL2, {"L2_DOC"	, TMPSL2->PIO_DOC		}) //Implementado DAC 17/01/2017
		//Endif
 
		//If PIO->( FieldPos("PIO_SERIE") ) > 0			
			//Aadd(aAuxSL2, {"L2_SERIE"	, TMPSL2->PIO_SERIE		}) //Implementado DAC 17/01/2017
		//Endif
  
		//If PIO->( FieldPos("PIO_VALISS") ) > 0			
			//Aadd(aAuxSL2, {"L2_VALISS"	, TMPSL2->PIO_VALISS		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_TABELA") ) > 0			
			//Aadd(aAuxSL2, {"L2_TABELA"	, TMPSL2->PIO_TABELA		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_STATUS") ) > 0			
			//Aadd(aAuxSL2, {"L2_STATUS"	, TMPSL2->PIO_STATUS		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_DESCPR") ) > 0			
			//Aadd(aAuxSL2, {"L2_DESCPRO"	, TMPSL2->PIO_DESCPR		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_CUSTO1") ) > 0			
			//Aadd(aAuxSL2, {"L2_CUSTO1"	, TMPSL2->PIO_CUSTO1		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_CUSTO2") ) > 0			
			//Aadd(aAuxSL2, {"L2_CUSTO2"	, TMPSL2->PIO_CUSTO2		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_GRADE") ) > 0			
			//Aadd(aAuxSL2, {"L2_GRADE"	, TMPSL2->PIO_GRADE		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_PREMIO") ) > 0			
			//Aadd(aAuxSL2, {"L2_PREMIO"	, TMPSL2->PIO_PREMIO		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_LOTECT") ) > 0			
			//Aadd(aAuxSL2, {"L2_LOTECTL"	, TMPSL2->PIO_LOTECT		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_VALFRE") ) > 0			
			//Aadd(aAuxSL2, {"L2_VALFRE"	, TMPSL2->PIO_VALFRE		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_EMPRES") ) > 0			
			//Aadd(aAuxSL2, {"L2_EMPRES"	, TMPSL2->PIO_EMPRES		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_FILRES") ) > 0			
			//Aadd(aAuxSL2, {"L2_FILRES"	, TMPSL2->PIO_FILRES		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_BASEPS") ) > 0			
			//Aadd(aAuxSL2, {"L2_BASEPS2"	, TMPSL2->PIO_BASEPS		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_BASECF") ) > 0			
			//Aadd(aAuxSL2, {"L2_BASECF2"	, TMPSL2->PIO_BASECF		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_ALIQPS") ) > 0			
			//Aadd(aAuxSL2, {"L2_ALIQPS2"	, TMPSL2->PIO_ALIQPS		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_ALIQCF") ) > 0			
			//Aadd(aAuxSL2, {"L2_ALIQCF2"	, TMPSL2->PIO_ALIQCF		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_ITEMSD") ) > 0			
			//Aadd(aAuxSL2, {"L2_ITEMSD1"	, TMPSL2->PIO_ITEMSD		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_DTVALI") ) > 0			
			//Aadd(aAuxSL2, {"L2_DTVALID"	, TMPSL2->PIO_DTVALI		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_SEGUM") ) > 0			
			//Aadd(aAuxSL2, {"L2_SEGUM"	, TMPSL2->PIO_SEGUM		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_PEDRES") ) > 0			
			//Aadd(aAuxSL2, {"L2_PEDRES"	, TMPSL2->PIO_PEDRES		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_FDTENT") ) > 0			
			//Aadd(aAuxSL2, {"L2_FDTENTR"	, TMPSL2->PIO_FDTENT		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_CODCON") ) > 0			
			//Aadd(aAuxSL2, {"L2_CODCONT"	, TMPSL2->PIO_CODCON		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_FDTMON") ) > 0			
			//Aadd(aAuxSL2, {"L2_FDTMONT"	, TMPSL2->PIO_FDTMON		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_CODREG") ) > 0			
			//Aadd(aAuxSL2, {"L2_CODREG"	, TMPSL2->PIO_CODREG		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_VLDESR") ) > 0			
			//Aadd(aAuxSL2, {"L2_VLDESRE"	, TMPSL2->PIO_VLDESR		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_TURNO") ) > 0			
			//Aadd(aAuxSL2, {"L2_TURNO"	, TMPSL2->PIO_TURNO		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_NUMORI") ) > 0			
			//Aadd(aAuxSL2, {"L2_NUMORIG"	, TMPSL2->PIO_NUMORI		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_VALEPR") ) > 0			
			//Aadd(aAuxSL2, {"L2_VALEPRE"	, TMPSL2->PIO_VALEPR		}) //Implementado DAC 17/01/2017
		//Endif

		//If PIO->( FieldPos("PIO_LJORI") ) > 0	.AND. SL2->(FieldPos("L2_LJORI"))  > 0			
			//Aadd(aAuxSL2, {"L2_LJORI"	, TMPSL2->PIO_LJORI			}) //Implementado  16/02/2018
		//Endif

		//If PIO->( FieldPos("PIO_CNPJOR") ) > 0	.AND. SL2->(FieldPos("L2_CNPJOR"))	> 0			
			//Aadd(aAuxSL2, {"L2_CNPJOR"	, TMPSL2->PIO_CNPJOR			}) //Implementado  16/02/2018
		//Endif  
		
		//If PIO->( FieldPos("PIO_CHVORI") ) > 0	.AND. SL2->(FieldPos("L2_CHVORI"))	> 0			
			//Aadd(aAuxSL2, {"L2_CHVORI"	, TMPSL2->PIO_CHVORI			}) //Implementado  16/02/2018
		//Endif  
		
		/*If cXTPREG  == ORCAMENTO
			Aadd(aAuxSL2, {"L2_LOJARES"  	, cLjCodigo	})
			Aadd(aAuxSL2, {"L2_RESERVA"  	, ""		})		                              
			Aadd(aAuxSL2, {"L2_SITUA"		, ""		})			

		ElseIf cXTPREG  == CUPNAOFISCAL  
			Aadd(aAuxSL2, {"L2_DOCPED"  	, cDocPed	})
			Aadd(aAuxSL2, {"L2_SERPED"  	, cSerie	})	                       		
			Aadd(aAuxSL2, {"L2_RESERVA"  	, ""		})		                              
			Aadd(aAuxSL2, {"L2_LOJARES"  	, cLjCodigo	})
			Aadd(aAuxSL2, {"L2_SITUA"		, "RX"		})

		ElseIf cXTPREG  == CUPOMFISCAL
			Aadd(aAuxSL2, {"L2_DOC"			, cDoc		})
			Aadd(aAuxSL2, {"L2_SERIE"		, cSerie	})
			Aadd(aAuxSL2, {"L2_RESERVA"  	, ""		})			
			Aadd(aAuxSL2, {"L2_SITUA"		, "RX"		})
						
			//Retira
			If Alltrim(TMPSL2->PIO_ENTREG) == "2"		
				Aadd(aAuxSL2, {"L2_LOJARES"  	, cLjCodigo			})			
			Else                                                      
			
			   //Aadd(aAuxSL2, {"L2_FILRES"  	, ""				})   - Retirado esse campo porque eh preenchido pelo Lj7Pedido se for necessario
				Aadd(aAuxSL2, {"L2_DOCPED"		, ""				})
				Aadd(aAuxSL2, {"L2_SERPED"  	, ""				})						
				Aadd(aAuxSL2, {"L2_LOJARES"  	, ""				})		                              					
			Endif	
		Endif*/

    	//Ponto de Entrada criado para retornar Campo e conte?o para execu��o no Execauto, somente duas posi��es por Array DAC 17/01/2017
  		If FindFunction("U_IN210AL2")        
			PIO->(DbGoto(TMPSL2->REGISTRO))  //Garanto posicionamento na tabela da Camada de Integra��o 
   			_aL2Mat := {}
      		_aL2Mat := U_IN210AL2(TMPSL2->REGISTRO,@aAuxSL2)
      		If ValType(_aL2Mat) == "A" .and. Len(_aL2Mat) > 0
				For nPos := 1 To Len(_aL2Mat) 
					If Len(_aL2Mat[nPos]) <> 2   //somente duas posu��es nome do campo e conteudo     
						lErro 	:= .T.	
						cErro 	:= STR0125   //"Erro no retorno do Ponto de Entrada IN210AL2, retornar Array com 2 Posi��es [nome do campo | conteudo do campo] "                                          		
						RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0125" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
						//U_INTM010c("4"	, "PIN"	, nRecSL1			, "STR0125" , cErro	)
						Return(Nil)
					Endif
					//Verificar se o campo esta incluindo na matriz principal caso esteja o mesmo ser?alterado conforme o retorno do P,E.            
					nPosCpo := Ascan( aAuxSL2, { |x| AllTrim(x[1]) == AllTrim(_aL2Mat[nPos,1]) } )
					If nPosCpo > 0
						aAuxSL2[nPosCpo,2]	:= _aL2Mat[nPos,2]
					Else 
						Aadd(aAuxSL2,{	AllTrim(_aL2Mat[nPos,1]),;  	//Nome do Campo
										_aL2Mat[nPos,2],; 				//Conte?o do Campo
										Nil})      	
					Endif					
    			Next
      		Endif
		Endif      	    


		Aadd(aSL2, aAuxSL2) 
		
		nVlTotItem 	+= TMPSL2->L2_VLRITEM               //DAC 23/02/2015 JA CONSTA O DESCONTO E DEVER?SER VALIDADO PELO VALOR TOTAL SL1 QUE DEVE SER IGUAL AO VALOR LIQUIDO

		TMPSL2->(DbSkip())
	EndDo 
	                                        
	//������������������������������������?
	//?alida valor total do L1 com a L2. ?
	//������������������������������������?
	nVlTotItem := Round(nVlTotItem, nCasaDecimal) 
	nDiferenca := Abs(nVlrTotL1 - nVlTotItem)
	If nDiferenca > nTolerancia

		cErro := STR0017 + " - (" + cValtoChar(nVlrTotL1) + "/" + cValtoChar(nVlTotItem) + ")"
		RMIGRVLOG("IR"	, "SL1"	, nRecSL1 , "STR0017" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		aSL2 := {}
		Return(Nil)
	EndIF

	//������������������������������Ŀ
	//?era Formas de pagamento SL4. ?
	//��������������������������������
	ValidSL4(	cFilSL1	, cNum		, cPdv			, nRecSL1	,;
				aPIORecnos	, cSituacao	, nVlrTotL1		, cXTPREG	,;
				cDOCPED	, _cCliente	, ""	, cAcao		,;
				lHistorico	, cConta)
	
EndIf   

TMPSL2->(DbCloseArea())

Return(Nil)


//-------------------------------------------------------------------
/*/{Protheus.doc} ValidSL4
Gera arquivo temporario TMPSL4 atraves do arquivo Legado 
@param 

@return Vazio
@Obs	ValidSL4 para ValidSL4
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidSL4(	cFilSL1	    , cNum		, cPdv			, nRecSL1	,;
						 	aPIORecnos	, cSituacao	, nVlrTotL1		, cXTPREG	,;
						 	cDOCPED	    , cCliente  , cXTPREGOld	, cAcao		,;
						 	lHistorico	, cConta)

Local cQuery 		:= ""								//Variavel da consulta
Local nVlTotPag		:= 0								//Valor total da soma dos pagamento
Local nTolerancia	:= SuperGetMv("ES_0000023", , 0.00)	//Valor de tolerancia de diferenca
Local nDiferenca	:= 0                   				//Valor de diferenca
Local nCasaDecimal	:= TamSX3("L1_VLRTOT")[2]			//Quanidade de casas decimais
Local lValTotSL4	:= SuperGetMv("ES_0000022", , .T.)  //Define se ira efetuar a validacao da SL4
Local aSL4Vazio	:= {}								    //Relacao de campos que nao podem estar vazios
Local nI			:= 0
Local cErro		:= ""
Local lRetorno		:= .F.								//Controla a gravacao dos registro
Local aRetorno		:= {}								//Retorno da Funcao GeraOrc
Local aAuxSL4		:= {}								//Contem as parcelas da forma de pagamento
Local aSL4Recnos	:= {}								//Recnos da tabela PIP
Local nTamIt	 	:= TamSx3("L4_ITEM")[1]				//Tamanho do campo de itens
Local nTamFormID 	:= TamSx3("L4_FORMAID")[1]			//Tamanho do campo de FormaID
Local cItem		 	:= Repl("0", nTamIt)				//Numeracao dos Itens
Local cFormID		:= Replicate("0",nTamFormID)		//Sequencia para o campo L4_FORMAID
Local nCont			:= 0
Local cAdm			:= "" 
Local lVendTEF		:= .F.
Local aL4Mat		:= {}
Local nPos			:= 0
Local cFormaPgto	:= ""                               //Armazena a forma de pagamento
Local cCampoL1      := ""
Local nPosCmpL1     := 0

cQuery := "SELECT SL4.*, SL4.R_E_C_N_O_ as REGISTRO FROM " + RetSQLName("SL4") + " SL4"
cQuery += " WHERE 	SL4.L4_FILIAL 	= '" + cFilSL1		+ "'	AND "	
cQuery += " 	 	SL4.L4_NUM 		= '" + cNum 		+ "' 	AND "                               
cQuery += " 		SL4.D_E_L_E_T_  <> '*' 									"
cQuery += " ORDER BY SL4.L4_FILIAL, SL4.L4_NUM"

cQuery := ChangeQuery( cQuery )

//Execulta a query
DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), "TMPSL4", .T., .F. )

//Campos Obrigatorios do SL4
If lFilCnpj
	Aadd(aSL4Vazio, "L4_CNPJOR"	)
Else
	Aadd(aSL4Vazio, "L4_FILIAL"	)
EndIf
Aadd(aSL4Vazio, "L4_NUM"	)    
Aadd(aSL4Vazio, "L4_FORMA"	)
Aadd(aSL4Vazio, "L4_VALOR"	)
                                
//Verifica se nao eh orcamento para validar outros campos
If !Empty(cSituacao)
	Aadd(aSL4Vazio, "L4_PDV"	)    
EndIf	

//Verifica se encontrou a Condicao Negociada
If TMPSL4->(Eof())

	cErro := STR0065    //"Nao foram encontradas formas de pagamento para o or?mento - PIP (Condicao negociada)."
	RMIGRVLOG("IR", "SL1", nRecSL1, "STR0065", cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
Else

	While !TMPSL4->( Eof() )
                    
		//Limpa variaveis
		cErro 	 := ""      
		cAdm     := ""
		lVendTEF := .F.  
		
		LjGrvLog("ValidSL4","RmiRetailJob - Processando Orcamento: " +  TMPSL4->L4_NUM)

		//Carrega informacoes para gravacao de Log
		Aadd( aSL4Recnos, TMPSL4->REGISTRO)
		
		//Analisa se campos obrigatorios estao em vazios
		For nI:= 1 to Len(aSL4Vazio)
			If Empty( &("TMPSL4->"+aSL4Vazio[nI]))
				cErro += STR0002 + " " + aSL4Vazio[nI]
			EndIf
		Next nI
		
		//Grava o Log dos campos obrigatorios
		If !Empty(cErro)
			RMIGRVLOG("IR", "SL1", nRecSL1, "STR0002" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			aSL4 := {}
			Return(Nil)
		EndIf
		
        //Forma de pagamento
        cFormaPgto := TMPSL4->L4_FORMA
		If lDePara

			cFormaPgto := AllTrim( RmiPsqDePa(cMvSisOri, "SX5", "X5_TABELA", cFormaPgto, 1, xFilial("SX5") + '24') )

			If Empty(cFormaPgto)
				RmiGrvLog("IR", "SL1", nRecSL1, "STR1004", STR1004,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")     //"A forma de pagamento do R_E_C_N_O_ informado, n�o existe na tabela SX5 (Genericas) e tamb�m no cadastro de De/para (MHM)"
				Return(Nil)
			EndIf			
		EndIf

        //Administradora
		cAdm     := TMPSL4->L4_ADMINIS		
		lVendTEF := AllTrim(cFormaPgto) $ "CC|CD|VA|CO|CP|FI|BO"
		If lDePara .And. lVendTEF

            cAdm := AllTrim( RmiPsqDePa(cMvSisOri, "SAE", "AE_COD", cAdm, 1, xFilial("SAE") + cAdm) )

			If Empty(cAdm)
				RmiGrvLog("IR", "SL1", nRecSL1, "STR0039", STR0039 + " " + cAdm,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")  //"Codigo da Administradora sem referencia De/Para no Protheus (SAE)"
				Return(Nil)
			EndIf
		Endif	
		LjGrvLog("ValidSL4", "RmiRetailJob Orcamento: " +  TMPSL4->L4_NUM + " - L4_FORMA : " + AllTrim(cFormaPgto) + " L4_ADMINIS: " + AllTrim(cAdm))
		
		//Verifica se tem erro no registro corrente
		If !Empty(cErro)
			aSL4 := {}
			Return(Nil)
		EndIf	
		   
		//Atualiza codigo do item da conficao de pagamento
		cItem := Soma1(cItem, nTamIt)
	    
		//Carrega array com as informacoes do pagamento
		aAuxSL4 := {}
		Aadd( aAuxSL4, {"L4_FILIAL"	, cFilSL1									} )
		Aadd( aAuxSL4, {"L4_ITEM"	, cItem		    							} )
		Aadd( aAuxSL4, {"L4_SITUA"  , cSituacao 								} )						
		Aadd( aAuxSL4, {"L4_FORMA"  , cFormaPgto                                } )
		Aadd( aAuxSL4, {"L4_ADMINIS", cAdm			                            } )
		Aadd( aAuxSL4, {"L4_TERCEIR", IIF(TMPSL4->L4_TERCEIR == "S", .T., .F.)  } )
						
		//Tratamento para o campo L4_FORMAID, caso seja uma venda TEF
		//Verificar a forma correta de validar abaixo
		If lVendTEF
			If SL4->(FieldPos("L4_FORMAID")) > 0
				If !Empty(TMPSL4->L4_FORMAID)
					Aadd(aAuxSL4, {"L4_FORMAID"	, TMPSL4->L4_FORMAID	})
				Else
					cFormID := Soma1(cFormID,nTamFormID)
					Aadd(aAuxSL4, {"L4_FORMAID"	, cFormID		})
				EndIf
			Else
				cFormID := Soma1(cFormID,nTamFormID)
				Aadd(aAuxSL4, {"L4_FORMAID"	, cFormID		})
			EndIf
		EndIf

    	//Ponto de Entrada criado para retornar Campo e conte?o para execu��o no Execauto, somente duas posi��es por Array DAC 17/01/2017
  		If ExistBlock("IN210AL4")        
			SL4->(DbGoto(TMPSL4->REGISTRO))  //Garanto posicionamento na tabela da Camada de Integra��o 
   			aL4Mat := {}
      		aL4Mat := ExecBlock("IN210AL4",.F.,.F., { TMPSL2->REGISTRO })
      		If ValType(aL4Mat) == "A" .and. Len(aL4Mat) > 0
				For nPos := 1 To Len(aL4Mat) 
					If Len(aL4Mat[nPos]) <> 2   //somente duas posu��es nome do campo e conteudo     
						lErro 	:= .T.	
						cErro 	:= STR0126   //"Erro no retorno do Ponto de Entrada IN210AL4, retornar Array com 2 Posi��es [nome do campo | conteudo do campo] "
						RMIGRVLOG("IR", "SL1", nRecSL1, "STR0126" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
						Return(Nil)
					Endif
					//Verificar se o campo esta incluindo na matriz principal caso esteja o mesmo ser?alterado conforme o retorno do P,E.            
					nPosCpo := Ascan( aAuxSL4, { |x| AllTrim(x[1]) == AllTrim(aL4Mat[nPos,1]) } )
					If nPosCpo > 0
						aAuxSL4[nPosCpo,2]	:= aL4Mat[nPos,2]
					Else 
						Aadd(aAuxSL4,{	AllTrim(aL4Mat[nPos,1]),;   //Nome do Campo
										aL4Mat[nPos,2],; 			//Conte?o do Campo
										Nil})      	
					Endif					
    			Next
      		Endif
		Endif

        //Carrega itens do pagamento
		Aadd(aSL4, aAuxSL4)

        //Atualiza pagamentos no cabe�alho SL1
        If TMPSL4->L4_VALOR > 0 .And. !Empty(cFormaPgto)

            Do Case
                Case IsMoney(cFormaPgto) 
                    cCampoL1 := "L1_DINHEIR"
                
                Case cFormaPgto == 'CH'
                    cCampoL1 := "L1_CHEQUES"
                
                Case cFormaPgto $ 'CC|CD'

                    If ( nPosCmpL1 := Ascan(aSL1, {|x| x[1] == "L1_VENDTEF"}) ) > 0 
                        aSL1[nPosCmpL1][2] := "S"
                    Else
                        Aadd(aSL1, {"L1_VENDTEF", "S"})
                    EndIf

                    cCampoL1 := IIF (cFormaPgto == 'CC', "L1_CARTAO", "L1_VLRDEBI")
                
                Case cFormaPgto == 'CO'
                    cCampoL1 := "L1_CONVENI"
                
                Case cFormaPgto == 'VA'
                    cCampoL1 := "L1_VALES"
                
                Case cFormaPgto == 'FI'
                    cCampoL1 := "L1_FINANC"
                
                Case cFormaPgto == 'CR'
                    cCampoL1 := "L1_CREDITO"
                
                OtherWise
                    cCampoL1 := "L1_OUTROS"
            EndCase 

            //Atualiza um dos campos acima
            If ( nPosCmpL1 := Ascan(aSL1, {|x| x[1] == cCampoL1}) ) > 0 
                aSL1[nPosCmpL1][2] += TMPSL4->L4_VALOR
            EndIf

            //Carrega o valor de entradas exceto as contidas no par�metro
            If !( cFormaPgto $ SuperGetMV("MV_ENTEXCE", .F., "") )

                If ( nPosCmpL1 := Ascan(aSL1, {|x| x[1] == "L1_ENTRADA"}) ) > 0 
                    aSL1[nPosCmpL1][2] += TMPSL4->L4_VALOR
                EndIf
            EndIf

            //Armazena ultima forma de pagamento 
            If ( nPosCmpL1 := Ascan(aSL1, {|x| x[1] == "L1_FORMPG"}) ) > 0 
                aSL1[nPosCmpL1][2] := cFormaPgto
            EndIf
        EndIf
        //TENORIO        
		
		//Soma Valor dos Pagamentos
		nVlTotPag += TMPSL4->L4_VALOR

		TMPSL4->( DbSkip() )
	EndDo   
	
	//Valida valor total do L1 com o L4
	If lValTotSL4
	    
		nVlTotPag	:= Round(nVlTotPag, nCasaDecimal)
		nDiferenca 	:= Abs(nVlrTotL1 - nVlTotPag)
		If nDiferenca > nTolerancia
	
			cErro := STR0066 + " - (" + cValtoChar(nVlrTotL1) + "/" + cValtoChar(nVlTotPag) + ")"
			RMIGRVLOG("IR", "SL1", nRecSL1, "STR0066" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
			aSL4 := {}
			Return(Nil)
		EndIF        
		
	EndIf
	
	//Abre transacao para gravar Orcamento
	Begin Transaction
	
		//Grava o Orcamento SL1, SL2 e SL4
		aRetorno := GeraOrc(	nRecSL1		, aPIORecnos	, aSL4Recnos	, cFilSL1	,; 
								cNum		, cPdv			,  cXTPREG		, cCliente	,;
								lHistorico	, cConta		)
																
		If Len(aRetorno) > 0
			lRetorno := aRetorno[1][1] 
		Else
			lRetorno := .T.         					
		EndIf
		
		If !lRetorno         
		
			RollBackSX8()	
			DisarmTransaction()
		Else
			ConfirmSX8()
			//Atualiza Status nas tabela SL1
			RMIGRVSTAT("SL1", nRecSL1, "RX")
		EndIf
	
	End Transaction 
	
	//Trava Logs de Erro - Depois de Fechar a Transacao. ?
	If !lRetorno  
		For nCont:=1 To Len(aRetorno)
			RMIGRVLOG(aRetorno[nCont][2], aRetorno[nCont][3], aRetorno[nCont][4], aRetorno[nCont][5], aRetorno[nCont][6],,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		Next nCont
	EndIf				
	
Endif

TMPSL4->( DbCloseArea() )
SL1->( DbCloseArea() )
SL2->( DbCloseArea() )
SL4->( DbCloseArea() )

Return(Nil)


//-------------------------------------------------------------------
/*/{Protheus.doc} GeraOrc
Travacao das tabelas de orcamento SL1, SL2 e SL4 
@param 

@return Vazio
@Obs	GeraOrc para GeraOrc
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GeraOrc(	nRecSL1		, aPIORecnos	, aSL4Recnos	, cFilSL1	,; 
							_cNumExter	, _cPdv			,  cXTPREG		, cCliente	,;
							lHistorico	, cConta		)
							
Local aRetorno		:= {}							//Retorno da Funcao
Local lGravou		:= .F.             		       	//Indica se foi feita a correga gravacao do registro
Local nPosNSL4		:= 0							//Posicao no array aSL4 do L4_NUM
Local nPosItSL2		:= 0							//Posicao no Array aSL2 do L2_ITEM
Local nPosProd		:= 0							//Posicao no array aSL2 do L2_PRODUTO
Local nPosItSL4		:= 0							//Posicao no Array aSL4 do L4_ITEM
Local nPosLocal		:= 0
Local nPosPdv 		:= 0
Local cNumero		:= ""							//Numero do proximo orcamento
Local cItem			:= ""							//Codigo do Item
Local cProduto		:= ""							//Codigo do Produto
Local nCont			:= 0							
Local lAtuL4		:= .T.
Local cLocal		:= ""
Local _cStaImp		:= ""							//Variavel para determinar o codigo do que sera preenchido no campo PIN_STAIMP

DbSelectArea("SL1")  
SL1->(DbSetOrder(1))	

DbSelectArea("SL2")
SL2->( DbSetOrder(1) )					//L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO

DbSelectArea("SL4")
SL4->( DbSetOrder(4) )					//L4_FILIAL+L4_NUM+L4_ITEM

//Ponto de Entrada para Validar se os dados estao corretos
If FindFunction("U_IN210VLD")
	If !U_IN210VLD(@_cStaImp,nRecSL1,@cErro,aSL1,aSL2,aSL4)
		If Empty(_cStaImp)
			_cStaImp := "3"
		EndIf
		
		cErro := "[Ponto de Entrada - IN210VLD]" + cErro
		
		Aadd(aRetorno, {.F., _cStaImp, "SL1", nRecSL1, "STR9999" ,cErro} )
		
		Return aRetorno
	EndIf
EndIf


//������������������������������������??
//?tualiza numero do orcamento na SL1 ?
//������������������������������?������?
//nPosNum := Ascan(aSL1, {|x| x[1] == "L1_NUM"	} )
//aSL1[nPosNum][2] := cNumero

//��������������������������������������?
//Pega posicao de campos no array aSL2 ?
//��������������������������������������?
//nPosNSL2 	:= Ascan(aSL2[1], {|x| x[1] == "L2_NUM"		} )     
nPosItSL2 	:= Ascan(aSL2[1], {|x| x[1] == "L2_ITEM"		} )     
nPosProd	:= Ascan(aSL2[1], {|x| x[1] == "L2_PRODUTO"	} )     
//nPosQtd		:= Ascan(aSL2[1], {|x| x[1] == "L2_QUANT"		} )     
//nPosReserva	:= Ascan(aSL2[1], {|x| x[1] == "L2_RESERVA"	} )     
//nPosGarant	:= Ascan(aSL2[1], {|x| x[1] == "L2_GARANT"	} )     
nPosLocal	:= Ascan(aSL2[1], {|x| x[1] == "L2_LOCAL"		} )  
//nPosEntrega := Ascan(aSL2[1], {|x| x[1] == "L2_ENTREGA"	} )     
//nPosDtEmis 	:= Ascan(aSL2[1], {|x| x[1] == "L2_EMISSAO"	} )  
//nPosDocPed 	:= Ascan(aSL2[1], {|x| x[1] == "L2_DOCPED"	} )  
//nPosSerPed 	:= Ascan(aSL2[1], {|x| x[1] == "L2_SERPED"	} )  
nPosPdv 	:= Ascan(aSL2[1], {|x| x[1] == "L2_PDV"		} )  
//nPosDoc 	:= Ascan(aSL2[1], {|x| x[1] == "L2_DOC"		} )  
//nPosSerie 	:= Ascan(aSL2[1], {|x| x[1] == "L2_SERIE"		} )  
//nPosVlItem 	:= Ascan(aSL2[1], {|x| x[1] == "L2_VLRITEM"	} )  

//��������������������������������������?
//?ega posicao de campos no array aSL4 ?
//��������������������������������������?
nPosNSL4	:= Ascan(aSL4[1], {|x| x[1] == "L4_NUM"} )
nPosItSL4	:= Ascan(aSL4[1], {|x| x[1] == "L4_ITEM"} )
	
//Incluido transaction DAC 17/01/2017
Begin Sequence
	//������������������������?
	//?az a gravacao do SL1. ?
	//������������������������?
	SL1->( DbGoTo(nRecSL1) )
	If !SL1->( Eof() )
		cFilSL1 := SL1->L1_FILIAL
		cNumero := SL1->L1_NUM
		
		lGravou := FRTGeraSL("SL1", aSL1, .F.)
	EndIf
	
	If !lGravou
		cErro 	:= STR0081
		Aadd(aRetorno, {lGravou, "IR", "SL1", nRecSL1, "STR0123" , cErro} )
        Break
	Endif            

	//����������������������������������������������������Ŀ
	//?eleta Reservas e Itens dos Orcamento caso encontre.?
	//������������������������������������������������������
	//lGravou	:= DelReserva( cFilSL1, cCliente, cNumero, cXTPREG)
	/*If !lGravou
		cErro 	:= STR0081
		Aadd(aRetorno, {lGravou, "4", "PIN", nRecSL1, "STR0081" , cErro} )
	Endif*/			

	//Ponto de Entrada para permitir altera��o no SL1 ap? a Grava��o do Registro DAC 08/11/2016
	If ExistBlock("IN210GL1")
		ExecBlock("IN210GL1",.F.,.F., { aSL1 } )
	Endif

	//����������������������������������������������?
	//?rava tabela SL2 com os dados da importacao. ?
	//����������������������������������������������?
	For nCont := 1 To Len(aSL2)
	
		lGravou := .F.
		
		//Atualiza Numero do Orcamento
		//aSL2[nCont][nPosNSL2][2] := cNumero
		//Pega dados dos Itens
		cItem 		:= aSL2[nCont][nPosItSL2][2]
		cProduto	:= aSL2[nCont][nPosProd][2]
       	//nQuant		:= aSL2[nCont][nPosQtd][2]         
       	cLocal 		:= aSL2[nCont][nPosLocal][2] 
        //cReserva	:= "" 
        //cGarantia	:= AllTrim( Upper(aSL2[nCont][nPosGarant][2]) )
        //dDtEmissa	:= StoD( aSL2[nCont][nPosDtEmis][2] )
        //cEntrega	:= Alltrim(aSL2[nCont][nPosEntrega][2]) 
       	//cDocPed		:= IIF( nPosDocPed 	> 0, aSL2[nCont][nPosDocPed][2]	, "")
		//cSerPed		:= IIF( nPosSerPed 	> 0, aSL2[nCont][nPosSerPed][2]	, "")
		//cPdv		:= IIF( nPosPdv 	> 0, aSL2[nCont][nPosPdv][2]		, "")
		//cDoc		:= IIF( nPosDoc 	> 0, aSL2[nCont][nPosDoc][2]		, "")
		//cSerie		:= IIF( nPosSerie 	> 0, aSL2[nCont][nPosSerie][2]	, "")
		//nVlrItem	:= aSL2[nCont][nPosVlItem][2] 
             
		//������������������������������
		//?era reseva de Produtos que nao sejam Garantia (GE) e nao sejam Cupons de Historico. ?
		//������������������������������
        //N�o vai fazer reserva pois n�o existe pedido no momento.
        /*If !(SL2->L2_GARANT == "GE") .AND. !lHistorico
			If cEntrega == "" .OR. cEntrega == "2"
				lGravou	:= .T.
			ElseIf !FazReserva( 	cFilSL1	, cCliente	, cNumero	, cProduto	,;
									nQuant		, @cReserva	, cLocal	, cXTPREG	,;
									cEntrega	)                                     
				lGravou	:= .F.
				cErro 	:= STR0080 + ENTER + "Tabela SL2"
				Aadd(aRetorno, {lGravou, "IR", "SL1", aPIORecnos[nCont], "STR0080" , cErro} )
				Exit				                                          
			Else
				aSL2[nCont][nPosReserva][2] := cReserva
			EndIf
		Else
			aSL2[nCont][nPosReserva][2] := cReserva					
		EndIf*/
										
		//������������������������?
		//?az a gravacao do SL2. ?
		//������������������������?
		SL2->(DbSetOrder(1))		//L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
		If SL2->( DbSeek(cFilSL1 + cNumero + cItem) )
			lGravou	:= FRTGeraSL("SL2", aSL2[nCont], .F.)
		EndIf
			
		If !lGravou 
			cErro 	:= STR0001  + "Erro na Inclus? dos Itens: L2_FILIAL+L2_NUM+L2_ITEM (" + cFilSL1 + cNumero + cItem + ") - SL2"
			Aadd(aRetorno, {lGravou, "IR", "SL1", nRecSL1 , "STR0001" , cErro} )
			//Exit
			Break				
		EndIf  
			  
		//Ponto de Entrada para permitir altera��o no SL2 ap? a Grava��o do Registro  DAC 08/11/2016
		If ExistBlock("IN210GL2")
			ExecBlock("IN210GL2",.F.,.F., { aSL2[nCont] } )
		Endif				
	Next nCont	

	//����������������������������������������������?
	//?rava tabela SL4 com os dados da importacao. ?
	//����������������������������������������������?
	If lAtuL4   
		For nCont:=1 To Len(aSL4)  
			
			lGravou := .F.
				
			//Atualiza Numero do Orcamento
			//aSL4[nCont][nPosNSL4][2] := cNumero
					
			cItem := aSL4[nCont][nPosItSL4][2]
		             
			//������������������������?
			//?az a gravacao do SL4. ?
			//������������������������?
			SL4->( DbSetOrder(4) )		//L4_FILIAL+L4_NUM+L4_ITEM
			If SL4->( DbSeek(cFilSL1 + cNumero + cItem) )
				lGravou := FRTGeraSL("SL4", aSL4[nCont], .F.)			
			EndIf
				
			If !lGravou 
				cErro 	:= STR0001  + "Erro na Inclus? dos Pagamento: L4_FILIAL+L4_NUM+L4_ITEM (" + cFilSL1 + cNumero + cItem + ") - PIP"
				Aadd(aRetorno, {lGravou, "IR", "SL1", nRecSL1, "STR0001" , cErro} )
				//Exit
				Break				
			EndIf
			 
			//Ponto de Entrada para permitir altera��o no SL4 ap? a Grava��o do Registro  DAC 08/11/2016
			If ExistBlock("IN210GL4")
				ExecBlock("IN210GL4",.F.,.F., { aSL4[nCont] } )
			Endif				

		Next nCont	
	EndIf 
	//������������������������������������������������������������������������������������������������������������?
	//?tualiza o L1_SITUA apenas depois de gravar todo o orcamento, para o GravaBat pode Processar Corretamente. ?
	//������������������������������������������������������������������������������������������������������������?
	If lAtuSitua
		RecLock("SL1", .F.)
		SL1->L1_SITUA := "RX"
		SL1->( MsUnLock() )
	EndIf
	
End Sequence

Return(aRetorno)


//-------------------------------------------------------------------
/*/{Protheus.doc} ValCampo
Retorna dados das tabelas do padrao pelos campos que foram
criados para referencia
@param 

@return Vazio
@Obs	ValCampo para ValCampo
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValCampo(cTabela, aCmpBus, cChave, nIndex, cErro, cStr)

Local aArea	:= GetArea()
Local nI		:= 0
Local	aRetorno:= {}
Local aAreaTab	:= (cTabela)->( GetArea() )

//Retorno o valor dos campos 
DbSelectArea(cTabela)
dbSetOrder(nIndex)
If (cTabela)->( dbSeek( cChave ) )
	cErro := ""
	If Len(aCmpBus) > 0
		For nI := 1 to Len(aCmpBus)
			aAdd(aRetorno,(cTabela)->&(aCmpBus[nI]))
		Next nI
	EndIf
//Grava os devidos Logs na PI0 caso nao ache o registro	
Else
	RMIGRVLOG("IR", aInfoLog1[1], aInfoLog1[2], cStr, cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS"	)
	//U_INTM010c("4", aInfoLog1[1], aInfoLog1[2], cStr, cErro, .T.) 
	
	If Len(aInfoLog2) > 0
		RMIGRVLOG("IR", aInfoLog1[1], aInfoLog1[2], cStr	, cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS")
		//U_INTM010c( "4", aInfoLog1[1], aInfoLog1[2], cStr	, cErro	)
	EndIf	      
	
	aRetorno := {}
EndIf

RestArea(aArea)
RestArea(aAreaTab)
	
Return(aRetorno)     


//-------------------------------------------------------------------
/*/{Protheus.doc} FechaAlias
Fechas os Alias Temporarios
@param 

@return Vazio
@Obs	FechaAlias para FechaAlias
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function FechaAlias(lTodos)

Default lTodos := .F.

If lTodos
	If Select("TMPSL1") > 0
		TMPSL1->(DbCloseArea())
	EndIf    
EndIf

If Select("TMPSL2") > 0					
	TMPSL2->(DbCloseArea())
EndIf

If Select("TMPSL4") > 0					
	TMPSL4->(DbCloseArea())
EndIf

Return(Nil)

//-------------------------------------------------------------------
/*/{Protheus.doc} RMIEstacao
Rotina que analisa qual serie e especie devem ser usadas
@param 

@return Vazio
@Obs	RMIEstacao para RMIEstacao
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RMIEstacao(cFilSL1	,cPDVSL1	,cSerieSL1	,cEspecSL1	,;
							cEstacao	, cSerie	,lSAT		,lNFCe		,;
							lImpFis	,cEspecie	,nRecSL1	,cSetSatSL1)
							
Local lRet		:= .T.						//Variavel de Controle
Local _lTemEst	:= .T.						//Variavel que controla se encontrou a estacao
Local _cPDV		:= Alltrim(SL1->L1_PDV)	//Concatena a chave do PDV
Local cQry		:= ""						//Variavel auxiliar para QUERY
Local cAlias	:= GetNextAlias()			//Busca o proximo Alias disponivel
Local _aAliasAux:= {}						//Alias Auxiliar
Local aArea	:= GetArea()				//Salva a Area Atual
Local aAreaSLG	:= SLG->( GetArea() )		//Salva a Area da tabela SLG
Local cNome	:= ""


If SuperGetMV( "IN_VICUPCL"  , ,.F. )  .And. !Empty(SL1->L1_SERIE)	//PARAMETRO PARA INDICAR SE OBEDECERA  OS DADOS REFERENTE A CAMADA DE IMPORTA��O DO CLIENTE E N? A DO SLG
	cEstacao := SL1->L1_ESTACAO
	cSerie   := SL1->L1_SERIE
	
	DbSelectArea("SLG")
	SLG->( dbSetorder(1) )
	If SLG->( DbSeek(  xFilial("SLG") + cEstacao ) )
		lSAT		:= SLG->LG_USESAT
		lNFCe		:= SLG->LG_NFCE
		lImpFis	:= (!SLG->LG_NFCE .And. !SLG->LG_USESAT)
	Else
		lRet		:= .F.
	EndIf
Else
	//Atualiza a variavel que define o tipo de estacao que esta sendo criada
	
	If !Empty(SL1->L1_KEYNFCE) .AND. !Empty(SL1->L1_SERSAT)
		lSAT := .T.
	ElseIf !Empty(SL1->L1_KEYNFCE)
		lNFCe := .T.
	ElseIf !Empty(SL1->L1_SERPDV)
		lImpFis := .T.
	Else
		cErro:= "Venda sem um tipo definido (SAT/NFC-e/ECF)"
		lRet := .F.		
	Endif
	
	//Caso seja uma venda de ECF, � necessario o envio do campo PIN_PDV
	If lImpFis .And. Empty(SL1->L1_PDV)
		cErro:= "PDV do Tipo ECF deve receber o Numero do PDV"
		lRet := .F.
	EndIf

	//Verifica se eh o novo formato (PIN->PIN_LJORI + PIN->PIN_CXOR) ou o antigo (PIN_PDV)
	/*If (lSAT .Or. lNFCe) .And. SL1->( FieldPos("PIN_LJORI")) > 0 .And. PIN->( FieldPos("PIN_CXOR")) > 0		//Verifica se o PDV veio preenchido
		If !Empty(Alltrim(PIN->PIN_LJORI) + Alltrim(PIN->PIN_CXOR)) 
			_cPDV	:= Alltrim(PIN->PIN_LJORI) + Alltrim(PIN->PIN_CXOR)
		EndIf
	EndIf*/

	If lRet
		//Verifica se existe a Estacao cadastrada na filial atual com o codigo do PDV e com o campo correspondente ao tipo da venda
		_aAliasAux := GetArea()		//Salva a Area Atual
		
		cQry := "SELECT LG_CODIGO,LG_SERIE, " + RetSqlName("SLG") + ".R_E_C_N_O_ RECSLG FROM " + RetSqlName("SLG")
		cQry += " WHERE LG_FILIAL  = '" + xFilial("SLG")	+ "'"
		
		If lImpFis
			cQry += "   AND LG_PDV     = '" + _cPDV + "'"
		EndIf
		
		cQry += "   AND D_E_L_E_T_ = ' '"

		If !Empty(SL1->L1_SERIE)
			cQry += " AND LG_SERIE = '" + Alltrim(SL1->L1_SERIE)		+ "'"
   		EndIf

   		If !Empty(SL1->L1_SERSAT)
   			cQry += " AND LG_SERSAT = '" + Alltrim(SL1->L1_SERSAT)	+ "'"
   		EndIf

   		If !Empty(SL1->L1_SERPDV)
   			cQry += " AND LG_SERPDV = '" + Alltrim(SL1->L1_SERPDV)	+ "'"
   		EndIf

		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), cAlias, .T., .F. )
		If (cAlias)->( Eof() )
			_lTemEst	:= .F.
		Else
			cEstacao	:= (cAlias)->LG_CODIGO
			cSerie		:= (cAlias)->LG_SERIE
			
			/*If SLG->( FieldPos("PIN_LJORI") ) > 0 .And. SLG->( FieldPos("PIN_CXOR") ) > 0		//Concatena os campos conforme regra de campos do Bematech Chef
				//Verifico se o tamanho da filial eh maior que o tamanho da loja do PDV
				_nRepli	:= Len(cFilAnt) - Len(Alltrim(PIN->PIN_LJORI))
				
		        //Caso seja maior que zero, mantem o calculo, caso contrario zera
				_nRepli	:= IIF(_nRepli > 0,_nRepli,0)
				
				cNome	:= "LJ " + Replicate("0",_nRepli) + Alltrim(PIN->PIN_LJORI) + " PDV " + Alltrim(PIN->PIN_CXOR)
			Else*/																				//Concatena os campos conforme regra padrao
				cNome	:= "LJ " + Alltrim(cFilAnt) + " PDV " + Alltrim(SL1->L1_PDV)
			//EndIf
			
			dbSelectArea("SLG")
			SLG->( dbGoTo( (cAlias)->RECSLG ) )
			RecLock("SLG",.F.)
			SLG->LG_NOME	:= cNome			
			SLG->LG_PDV 	:= _cPDV
			SLG->( MsUnLock() )			
		EndIf

		(cAlias)->( dbCloseArea() )

		RestArea(_aAliasAux)
	EndIf
EndIf

//caso nao encontre a Estacao, chama a funcao para criar.
If lRet .And. !_lTemEst
	If !RMICriaEst(lSAT, lNFCe, lImpFis, @cEstacao, @cSerie, @cErro, _cPDV)
		cErro 	:= STR0036 + " " + SLG->( IndexKey() ) + " (" + SL1->L1_FILIAL + SL1->L1_PDV + ")"
		RMIGRVLOG("IR", "SL1"	, nRecSL1 , "STR0036" , cErro,,,,,,cChaveSL1,"GRVBATCH","PROTHEUS" )
		//U_INTM010c("4", "PIN", nRecSL1, "STR0036", cErro, .T.)
		lRet		:= .F.
	EndIf
EndIf

//Preenchimento do campo Especie conforme tipo da Venda
If !Empty(cEspecSL1)
	cEspecie:= cEspecSL1
ElseIf lSAT
	cEspecie:= "SATCE"
ElseIf lNFCe
	cEspecie:= "NFCE"
ElseIf lImpFis
	cEspecie:= "CF"
Else
	cEspecie:= ""
EndIf

RestArea(aArea)
RestArea(aAreaSLG)

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} RMICriaEst
Funcao que cria a estacao caso nao exista
@param 

@return Vazio
@Obs	RMICriaEst para RMICriaEst
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RMICriaEst(lSAT	,lNFCe	,lImpFis	,cEstacao,;
							cSerie	,cErro	,_cPDV		)

Local lRet		:= .T.								//Variavel de Controle para criacao da estacao
Local cCodigo	:= ""								//Codigo da Esntacao
Local cNome	:= ""								//Nome da Estacao (campos concatenados)
Local cSerSL1	:= SL1->L1_SERIE					//Serie da Estacao
Local aArea	:= GetArea()						//Salva a area atual
Local cAlias	:= GetNextAlias()					//Busca o proximo alias disponivel
Local cQry		:= ""								//Auxiliar para montagem da QUERY


//Caso seja uma venda do tipo SAT, verifico se o numero do SERSAT ja esta sendo usado para a mesma filial para manter.
//Caso seja o mesmo codigo, mas com outra Filial sempre cria uma nova estacao.
If lSAT
	cQry := "SELECT LG_SERIE FROM " + RetSqlName("SLG")
	cQry += " WHERE LG_FILIAL  = '" + xFilial("SLG")	+ "'"
	cQry += "   AND LG_PDV    <> '" + _cPDV + "'"
	cQry += "   AND D_E_L_E_T_ = ' '"
	cQry += "   AND LG_SERSAT  = '" + Alltrim(SL1->L1_SERSAT)	+ "'"
	DbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), cAlias, .T., .F. )
	If !(cAlias)->( Eof() )
		cSerSL1	:= (cAlias)->LG_SERIE
	EndIf

	(cAlias)->( dbCloseArea() )
EndIf

//Tratamento da Serie
If Empty(cSerSL1)	//Verifica se a Serie veio preenchido  
	cSerSL1 := RMISerie(lSAT,lNFCe,lImpFis)
EndIf

If lRet
	//Busca o proximo codigo disponivel para a filial
	cQry := "SELECT LG_CODIGO FROM " + RetSqlName("SLG")
	cQry += " WHERE LG_FILIAL = '" + xFilial("SLG") + "'"
	cQry += "   AND D_E_L_E_T_ = ' '"
	cQry += " ORDER BY LG_CODIGO DESC"
	DbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), cAlias, .T., .F. )
	If (cAlias)->( Eof() )	//Caso nao exista nenhuma estacao para a filial define como a primeira o codigo 002, pois a 001 eh a estacao padrao
		cCodigo:= "002"
	Else					//Caso exista, soma 1
		cCodigo:= Soma1((cAlias)->LG_CODIGO,TamSX3("LG_CODIGO")[1])
	EndIf
	
	(cAlias)->( dbCloseArea() )

	//Tratamento do Nome da Estacao
	/*If SLG->( FieldPos("PIN_LJORI") ) > 0 .And. SLG->( FieldPos("PIN_CXOR") ) > 0		//Concatena os campos conforme regra de campos do Bematech Chef
		//Verifico se o tamanho da filial eh maior que o tamanho da loja do PDV
		_nRepli	:= Len(cFilAnt) - Len(Alltrim(PIN->PIN_LJORI))
		
        //Caso seja maior que zero, mantem o calculo, caso contrario zera
		_nRepli	:= IIF(_nRepli > 0,_nRepli,0)
		
		cNome	:= "LJ " + Replicate("0",_nRepli) + Alltrim(PIN->PIN_LJORI) + " PDV " + Alltrim(PIN->PIN_CXOR)
	Else*/																				//Concatena os campos conforme regra padrao
		cNome	:= "LJ " + Alltrim(cFilAnt) + " PDV " + Alltrim(SL1->L1_PDV)
	//EndIf
	
	RecLock("SLG",.T.)
	SLG->LG_FILIAL	:= xFilial("SLG")
	SLG->LG_CODIGO	:= cCodigo
	SLG->LG_NOME	:= cNome
	SLG->LG_SERIE	:= cSerSL1
	SLG->LG_PDV		:= _cPDV
	SLG->LG_SERPDV	:= SL1->L1_SERPDV
	SLG->LG_LARGCOL	:= 40
	SLG->LG_NFCE	:= lNFCe
	SLG->LG_SERSAT	:= SL1->L1_SERSAT
	SLG->LG_USESAT	:= lSAT
	SLG->LG_IMPFISC	:= IIF(lImpFis,"BEMATECH MP4000 THFI - V. 01.00.01","BEMATECH MP-4200 TH 01.00.00(S)")
	SLG->LG_PORTIF	:= "COM1"
	SLG->( MsUnLock() )
	
	//Atualiza as variaveis 
	cEstacao:= SLG->LG_CODIGO
	cSerie	:= SLG->LG_SERIE
EndIf

RestArea(aArea)

Return lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} RMISerie
Funcao que retorna a proxima serie disponivel
@param 

@return Vazio
@Obs	RMISerie para RMISerie
@author Everson S P Junior
@since 03/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RMISerie(lSAT,lNFCe,lImpFis)

Local cSerie	:= ""
Local cQry		:= ""
Local cAlias	:= GetNextAlias()					//Busca o proximo alias disponivel
Local nTamSerie:= TamSX3("L1_SERIE")[1]			//Tamanho da Serie
Local cBlckLst	:= SuperGetMV("IN_BLCKSER",,"001|002")
Local aArea	:= GetArea()

cQry := "SELECT MAX(L1_SERIE) L1_SERIE FROM " + RetSqlName("SL1")
cQry += " WHERE L1_FILIAL = '" + xFilial("SL1") + "'"
cQry += "   AND L1_SERIE <> '" + Space(nTamSerie) + "'"
cQry += "   AND LEN(LTRIM(RTRIM(L1_SERIE))) = " + cValToChar(nTamSerie)
cQry += "   AND D_E_L_E_T_ = ' '"
DbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), cAlias, .T., .F. )
If (cAlias)->( Eof() )
	cSerie := "001"
Else
	cSerie := Soma1((cAlias)->L1_SERIE,nTamSerie)
EndIf

If !lNFCe
	While .T.
		If Alltrim(cSerie) $	Alltrim(cBlckLst)
			cSerie := Soma1(cSerie,nTamSerie)
		Else
			Exit
		EndIf
	End
EndIf

(cAlias)->( dbCloseArea() )

RestArea(aArea)

Return cSerie
//-------------------------------------------------------------------
/*/{Protheus.doc} AtuaOrigem
Atualiza o campo L1_ORIGEM para 'N' configurando que o registro 
veio de uma integra��o
@param 

@return Nil
@Obs	Para seguir o Legado o campo L1_ORIGEM deve ser igual 'N'
@author Everson S P Junior
@since 18/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function AtuaOrigem(nRecSL1)
Local aArea	:= GetArea()
	
SL1->(dbGoTo(nRecSL1))
RecLock("SL1", .F.)
SL1->L1_ORIGEM := 'N'
SL1->(MsUnLock())

RestArea(aArea)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} VldSL1Dup
Verifica se DOC e Serie j� existem na SL1
@param 

@return lRet
@author Everson S P Junior
@since 24/09/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function VldSL1Dup(cFILSL1,cDocSL1,cSERSL1)
Local aArea		:= GetArea()
Local lRet		:= .F.	
Local cQuery	:= ""

cQuery := "SELECT COUNT(L1_DOC) TOTAL FROM " + RetSQLName("SL1") + " SL1"
cQuery += " WHERE 	SL1.L1_FILIAL 	= '" + cFILSL1		+ "'	AND "	
cQuery += " 	 	SL1.L1_DOC 		= '" + cDocSL1 		+ "' 	AND "                               
cQuery += " 	 	SL1.L1_SERIE	= '" + cSERSL1 		+ "' 	AND "
cQuery += " 	 	SL1.L1_SITUA   IN ('RX','OK') 				AND "
cQuery += " 		SL1.D_E_L_E_T_  <> '*'"
//Execulta a query
DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), "VLDSL1", .T., .F. )

If VLDSL1->TOTAL > 0
	lRet:= .T.
EndIf

VLDSL1->(DbCloseArea())
RestArea(aArea)
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} LjRetTes
Retorna a TES para atualizar na venda

@param 
@return 
@author  Bruno Almeida
@since	 07/02/2020
@version 1.0
/*/
//-------------------------------------------------------------------

Function LjRetTes(cTpOper, cCliente, cLoja, cCodProd)

Local cTES 	:= "" //Variavel que retorna a TES
Local aArea	:= GetArea() //Guarda a area

Default cTpOper := ""
Default cCliente := ""
Default cLoja := ""
Default cCodProd := ""

/*
Prioridades da utilizacao da TES:	
1 - TES que esta gravada na SL2
2 - TES Inteligente
3 - TES do Item (SB1) ou Indicadores de produto (SBZ)
4 - TES Padrao MV_TESSAI
*/

//Retorna a TES - Pega a TES ja gravada na SL2
If !Empty(TMPSL2->L2_TES) 
	cTES	:= TMPSL2->L2_TES  
								
Else

	//Retorna a TES - Retorna a TES Inteligente
	cTES := MaTesInt(2, cTpOper, cCliente	, cLoja, "C", cCodProd )

	If Empty(cTES)

		//Foi necessario posicionar no produto  da venda, pois se o parametro MV_ARQPROD for igual a SB1,
		//so retorna a informacao da TES na funcao RetFldProd se estiver posicionado no registro do produto.
		dbSelectArea("SB1")
		SB1->(dbSetOrder(1)) //B1_FILIAL+B1_COD
		If SB1->(dbSeek(xFilial("SB1")+cCodProd))

			cTES := RetFldProd(SB1->B1_COD,"B1_TS")

			If Empty(cTES)
				cTES := SuperGetMV("MV_TESSAI",,"")
			EndIf

		EndIf

	EndIf
EndIf

RestArea(aArea)

Return cTES
