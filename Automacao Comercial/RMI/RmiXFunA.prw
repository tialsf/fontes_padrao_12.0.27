#INCLUDE "PROTHEUS.CH"
#INCLUDE "DBSTRUCT.CH"

Static cRetF3Fil := ""      //Filiais selecionadas utilizado nas fun��es RmiXF3Fil e RmiXF3FilR

//--------------------------------------------------------
/*/{Protheus.doc} RmiGrvLog
Grava Log da Integra��o RMI para posterior an�lise no Monitor

@param 		cStatus    -> Status que sera gravado no registro da tebela de vendas
@param 		cAlias     -> Alias da tabela de vendas
@param 		nRecno     -> Recno do registro que sera atualizado o status
@param 		cCodMen    -> Codigo da mensagem
@param 		cErro      -> Erro que foi encontrado na verificacao
@param 		lRegNew    -> Parametro que identifica se inclui um novo registro ou
                        atualiza o registro ja existente na tabela
@param 		cFilStatus -> Campo que sera atualizado o status na tabela
@param 		lUpdStatus -> Identifica se faz atualiza��o de status 
@param 		nIndice    -> Numero do �ndice da tabela origem para busca
@param 		cChave     -> Chave da tabela origem para busca
@author  	Varejo
@version 	1.0
@since    03/09/2019
@return	  Nao ha
/*/
//--------------------------------------------------------
Function RmiGrvLog( cStatus     , cAlias    , nRecno    , cCodMen   ,;
                    cErro       , lRegNew   , lTxt      , cFilStatus,;
                    lUpdStatus  , nIndice   , cChave ,cProcesso,cAssinante )
   
    Local aArea         := GetArea() //Guarda a area
    Local nSeq          := 0         //Sequencia dos registros
    Local lGrvIndice    := MHL->(ColumnPos("MHL_INDICE")) > 0 //Valida se grava Indice para busca
    Local lGrvChave     := MHL->(ColumnPos("MHL_CHAVE" )) > 0 //Valida se grava Chave para busca
    Local lGrvProce     := MHL->(ColumnPos("MHL_CPROCE" )) > 0 
    Local lGrvAssin     := MHL->(ColumnPos("MHL_CASSIN" )) > 0 
   
    Default lRegNew     := .T.
    Default lTxt        := .F.
    Default cStatus     := ""
    Default cAlias      := ""
    Default nRecno      := 0
    Default cCodMen     := ""
    Default cErro       := ""
    Default cFilStatus  := "L1_SITUA"
    Default lUpdStatus  := .T.
    Default nIndice     := 0
    Default cChave      := ""
    Default cProcesso   := ""
    Default cAssinante  := ""

    DbSelectArea("MHL")
    MHL->( DbSetOrder(1) )  //MHL_FILIAL+MHL_ALIAS+STR(MHL_RECNO,12,0)+MHL_SEQ

    //Se j� existe o registro de log, o sistema pode apenas alterar o registro que ja
    //existe, como pode incluir um novo registro, tudo vai depender de como vai vir o 
    //parametro lRegNew
    If MHL->( DbSeek(xFilial("MHL") + cAlias + Str(nRecno, 12, 0)) )

        If lRegNew
            //Loop para buscar a sequencia do ultimo registro e acrescentar mais um
            While MHL->(!Eof()) .AND. AllTrim(MHL->MHL_FILIAL+MHL->MHL_ALIAS+Str(MHL->MHL_RECNO, 12, 0)) == AllTrim(xFilial("MHL") + cAlias + Str(nRecno, 12, 0))
                nSeq := MHL->MHL_SEQ
                MHL->( DbSkip() )
            End
            nSeq++
        EndIf

    //Se n�o existe o registro de log incluo as informa��es            
    Else

        lRegNew := .T.
        nSeq    := 1
    EndIf

    Begin Transaction

        RecLock("MHL", lRegNew)
            MHL->MHL_FILIAL := xFilial("MHL")
            MHL->MHL_STATUS := cStatus
            MHL->MHL_DATA   := Date()
            MHL->MHL_HORA   := Time()
            MHL->MHL_ALIAS  := cAlias
            MHL->MHL_RECNO  := nRecno
            MHL->MHL_CODMEN := cCodMen
            MHL->MHL_ERROR  := cErro
            
            If lRegNew
                MHL->MHL_SEQ := nSeq
            EndIf 

            IIF(lGrvIndice, MHL->MHL_INDICE  := nIndice,)
            IIF(lGrvChave , MHL->MHL_CHAVE   := cChave,)
            IIF(lGrvProce , MHL->MHL_CPROCE  := cProcesso,)
            IIF(lGrvAssin , MHL->MHL_CASSIN  := cAssinante,)

        MHL->( MsUnLock() )
        
        //Atualiza o status conforme o registro que foi passado no parametro nRecno
        If lUpdStatus .And. !Empty(cStatus)
            DbSelectArea(cAlias)
            (cAlias)->( DbGoTo(nRecno) )

            If !(cAlias)->( Eof() )
                RecLock(cAlias, .F.)
                    &(cAlias + "->" + cFilStatus) := cStatus
                (cAlias)->( MsUnLock() )
            EndIf
        EndIf

    End Transaction

    RestArea(aArea)

Return .T.

//--------------------------------------------------------
/*/{Protheus.doc} RmiPsqDePa
O objetivo desta funcao eh pesquisar o conteudo gravado 

@param 		cSisOri     -> Nome do sistema que enviou a informacao para o Protheus
@param 		cAlias      -> Tabela padrao no Protheus
@param 		cCampo      -> Campo da tabela padrao do Protheus
@param 		cContOri    -> Conteudo do campo que veio do sistema de origem
@param 		nIndex      -> Indece para realizar a pesquisa antes de pesquisar na tabela De/Para
@param 		cChave      -> Chave de pesquisa que sera utilizado antes de pesquisar na tabela De/Para
@author  	Varejo
@version 	1.0
@since      24/09/2019
@return	    cRet        -> Retorno do conteudo pesquisado na tabela padrao ou na tabela De/Para
/*/
//--------------------------------------------------------
Function RmiPsqDePa(cSisOri, cAlias, cCampo, cContOri, nIndex, cChave)

    Local uRet  := "" 			//Variavel de retorno
    Local aArea := GetArea() 	//Guarda a area

    Default cSisOri     := ""
    Default cAlias      := ""
    Default cCampo      := ""
    Default cContOri    := ""
    Default nIndex      := 0
    Default cChave      := ""
    
    If !Empty(cSisOri) .AND. !Empty(cAlias) .AND. !Empty(cCampo) .AND.;
       !Empty(cContOri) .AND. nIndex > 0 .AND. !Empty(cChave)

        If cAlias == "SX5"
            uRet := FWGetSX5 ("24",cContOri)
            If ValType(uRet) == "A" .AND. Len(uRet) >= 1
                uRet := uRet[1][3]
            Else
                uRet := ""
            EndIf
        EndIf

        If !(cAlias $ "SX5|SB1")
            dbSelectArea(cAlias)
            (cAlias)->(DbSetOrder(nIndex))
        EndIf

        If !(cAlias $ "SX5|SB1") .AND. (cAlias)->(dbSeek(cChave))
            If !(cAlias $ "SA1|SAE")
                uRet := &(cAlias + "->" + cCampo)
            ElseIf cAlias == "SA1"
                uRet := {}
                Aadd(uRet,SA1->A1_COD)
                Aadd(uRet,SA1->A1_LOJA)
            ElseIf cAlias == "SAE"
                uRet := AllTrim(SAE->AE_COD) + "-" + AllTrim(SAE->AE_DESC)
            EndIf
        Else
            
            If (cAlias == "SX5" .AND. Empty(uRet)) .OR. cAlias <> "SX5"

                cSisOri := PadR(cSisOri,TamSx3('MHM_SISORI')[1])
                cAlias  := PadR(cAlias,TamSx3('MHM_TABELA')[1])
                cCampo  := PadR(cCampo,TamSx3('MHM_CAMPO')[1])
                cContOri:= PadR(cContOri,TamSx3('MHM_VLORIG')[1])

                dbSelectArea("MHM")
                MHM->(dbSetOrder(1)) //MHM_FILIAL+MHM_SISORI+MHM_TABELA+MHM_CAMPO+MHM_VLORIG+MHM_FILINT+MHM_VLINT
                If MHM->(dbSeek(xFilial("MHM")+cSisOri+cAlias+cCampo+cContOri+xFilial(cAlias)))
                    If cAlias <> "SA1"
                        uRet := MHM->MHM_VLINT
                    Else
                        uRet := {}
                        uRet := Separa(MHM->MHM_VLINT, "|")
                    EndIf
                Else
                    If cAlias <> "SA1"
                        uRet := ""
                    Else
                        uRet := {}
                        Aadd(uRet,"")
                        Aadd(uRet,"")
                    EndIf
                EndIf

            EndIf

            //Pesquisa primeiro pelo codigo de barras
            If cAlias == "SB1" .AND. Empty(uRet)

                dbSelectArea("SLK")
                SB1->(DbSetOrder(1)) //LK_FILIAL+LK_CODBAR

                cContOri := PadR(cContOri,TamSx3('LK_CODBAR')[1])            

                If SLK->(dbSeek(xFilial('SLK')+cContOri))
                    uRet := SLK->LK_CODIGO
                Else
                    dbSelectArea("SB1")
                    SB1->(DbSetOrder(5)) //B1_FILIAL+B1_CODBAR

                    cContOri := PadR(cContOri,TamSx3('B1_CODBAR')[1])            

                    If SB1->(dbSeek(xFilial('SB1')+cContOri))
                        uRet := SB1->B1_COD
                    Else
                        dbSelectArea("SB1")
                        (cAlias)->(DbSetOrder(nIndex))

                        If (cAlias)->(dbSeek(cChave))
                            uRet := &(cAlias + "->" + cCampo)
                        EndIf

                    EndIf
                EndIf
            EndIf

        EndIf
    EndIf

    RestArea(aArea)

Return uRet

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiXSql
Fun��o utilizada para rodar uma query e trazer o result em um array.

@Param aReplace - Array com os replaces para serem feitos ap�s o changeQuery
				  [1] - Primeira parte do Replace
				  [2] - Segunda parte do Replace

@author  Rafael Tenorio da Costa
@since 	 31/10/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiXSql(cSQL, xCamposRet, lCommit, aReplace)

    Local aValor  := {}
    Local aArea   := GetArea()
    Local cQry	  := ""
    Local cTmp    := ""
    Local cAux    := ""
    Local nI      := 1
    Local nY      := 1
    Local aFldPos := {}
    Local aStru   := {}
    Local cType   := ""
    Local nJ      := 0

    Default cSQL       := ""
    Default xCamposRet := {}
    Default lCommit    := .T.
    Default aReplace   := {}

	If !Empty(cSQL) .And. !Empty(xCamposRet)

		If lCommit
			DbCommitAll() //Para efetivar a altera��o no banco de dados (n�o impacta no rollback da transa��o)
		EndIf

		cQry  := ChangeQuery(cSQL)

		If Len(aReplace) > 0
			For nJ := 1 to Len(aReplace)
				cQry := StrTran(cQry, aReplace[nJ][1], aReplace[nJ][2])
			Next
		EndIf

		cTmp  := GetNextAlias()
		dbUseArea( .T., "TOPCONN", TcGenQry( ,, cQry ), cTmp, .T., .F. )

		cType := ValType(xCamposRet)

		If cType == "A" .And. LEN(xCamposRet) == 1 .And. xCamposRet[1] == "*" // Tratamento para aceitar tanto "*" como {"*"}
			xCamposRet := "*"
			cType      := "C"
		EndIf

		If cType == "C"
			If xCamposRet == "*"
				xCamposRet := {}
				aStru := (cTmp)->(dbStruct())
				For nI := 1 to Len(aStru)
					aAdd(xCamposRet, aStru[nI][DBS_NAME])
				Next
			else
				cAux := xCamposRet
				xCamposRet := {}
				aAdd(xCamposRet, cAux)
			EndIf
		EndIF
		aFldPos := Array(Len(xCamposRet))

		While !(cTmp)->(EOF())

			aAdd(aValor, {})
			For nI := 1 to Len(xCamposRet)
				If aFldPos[nI] == Nil
					aFldPos[nI] := (cTmp)->(FieldPos(xCamposRet[nI]))
				EndIf
				aAdd(aValor[nY], (cTmp)->(FieldGet(aFldPos[nI])))
			Next
			nY += 1
			(cTmp)->(dbSkip())

		EndDo

		(cTmp)->( dbCloseArea() )

	EndIf

	RestArea( aArea )

Return aValor

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiXSelFil
Apresenta um tela com as filiais da empresa logada para sele��o.

@author  Rafael Tenorio da Costa
@since 	 28/01/20
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiXSelFil()

    Local aSelFilial := {}
    Local cSelFilial := ""    
    Local nCont      := 0

    //Campos da SM0 retornados no array: { 'FLAG', 'SM0_CODFIL', 'SM0_NOMRED', 'SM0_CGC', 'SM0_INSC', 'SM0_INSCM' }
    aSelFilial := FwListBranches( .F./*lCheckUser*/, .F./*lAllEmp*/, /*lOnlySelect*/ , /*aRetInfo*/);
    
    If Len(aSelFilial) > 0 

        For nCont:=1 To Len(aSelFilial)
            cSelFilial += aSelFilial[nCont][2] + ";"
        Next nCont

        //cSelFilial := SubStr(cSelFilial, 1, Len(cSelFilial) - 1)
    EndIf

    Asize(aSelFilial, 0)

Return cSelFilial

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiXF3Fil
Utilizado em consultas padr�es F3 para selecionar multiplas filiais.
 
@author  Rafael Tenorio da Costa
@since 	 29/01/20
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiXF3Fil()
    cRetF3Fil := RmiXSelFil()
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiXF3FilR
Utilizado em consultas padr�es F3 para retornar as filiais selecionadas.

@author  Rafael Tenorio da Costa
@since 	 29/01/20
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiXF3FilR()
Return cRetF3Fil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiXGetTag
Retonar o conteudo de uma tag do xml

@param cXml		- Xml de resposta do WebService
@param cTagIni	- Tag que tera o conteudo retornado
@param lTag		- Indica se deve retornar a tag tb

@author  Rafael Tenorio da Costa
@since 	 17/02/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiXGetTag(cXml, cTagIni, lTag)

	Local cRet 	  := ""
	Local cTagFim := ""
	Local nAtIni  := 0
	Local nAtFim  := 0
	Local nTamTag := 0

	Default lTag  := .T.

	cTagFim := StrTran(cTagIni, "<", "</")

	//Localiza��o das tags na string do XML
	nAtIni := At( Lower(cTagIni), Lower(cXml) )
	nAtFim := At( Lower(cTagFim), Lower(cXml) )

	//Pega o valor entre a tag inicial e final
	If nAtIni > 0 .And. nAtFim > 0
		nTamTag := Len(cTagIni)
		cRet	:= SubStr(cXml, nAtIni + nTamTag, nAtFim - nAtIni - nTamTag)
	Endif

	//Retorna a tag com o conteudo
	If !Empty(cRet) .And. lTag
		cRet := cTagIni + cRet + cTagFim
	EndIf

Return cRet
//--------------------------------------------------------
/*/{Protheus.doc} PagMovCx
Fun��o para buscar o movimento do caixa de Sangria

@param 		aMoviPag  -> Array com os dados do movimento
@author  	Varejo
@version 	1.0
@since      08/07/2020
@return	    xRet       -> Retorna o VALORCONFERIDO
/*/
//--------------------------------------------------------
function PagMovCx(aMoviPag)
Local xRet  := ""
Local nX    := 0  

For nX := 1 To Len(aMoviPag)
    If Alltrim(aMoviPag[nX]:_FORMAPAGAMENTO:TEXT) = "DINHEIRO"
        xRet:= aMoviPag[nX]:_VALORCONFERIDO:TEXT
        exit
    EndIf
Next

Return xRet  