#INCLUDE "PROTHEUS.CH"
#INCLUDE "TRYEXCEPTION.CH"
#INCLUDE "DBSTRUCT.CH"
#INCLUDE "ArrayfunC.CH"


//--------------------------------------------------------
/*/{Protheus.doc} GetImpPrd
Fun��o para retorna conforme parametro usando MATXFIS

@param 		aItens     -> Array com produtos e Filiais a serem consultados (Obrigatorio)
@param 		aCampos    -> Array de campos retorno da MATXFIS exemplo: "IT_VALICM" ou "NF_" (Obrigatorio)
@param 		cCliente   -> Codigo do cliente (opcional)
@param 		cLojaCli   -> Loja do Cliente (opcional)
@author  	Varejo
@version 	1.0
@since      23/07/2020
@return	    aRet    -> Retorna com a informa��o
/*/
//--------------------------------------------------------
Function GetImpPrd(aItens,aCampos,cCliente,cLojaCli)
Local aArea 	:= GetArea()
Local nInd,nY   := 0
Local nTotItens := 0
Local aRet      := {}
Local nPreco    := 0
Local cTesProd  := ""
Local cFilbkp   := cFilAnt
Local xCampo1   := 0
Local nItem     := 0

Default cCliente:= GetMv( "MV_CLIPAD" )		// Cliente padrao 
Default cLojaCli:= GetMv( "MV_LOJAPAD" )   // Loja padrao
Default aItens  := {}
Default aCampos := {}


SB1->(DbSetOrder(1))
nTotItens 	:= Len(aItens)
If Len(aItens) > 0 .AND. Len(aItens[1]) > 1 .AND. Len(aCampos) > 0
    MaFisIni(cCliente ,cLojaCli	, "C"	,"S"	,;
                                'F'	,NIL		, NIL	,.F.	,;
                                "SB1"		,"LOJA701", "01"	,NIL	,;
                                NIL			,NIL		, NIL	,NIL	,;
                                NIL			,NIL		, .F.,.T.	)

    For nInd:=1 To nTotItens
        
        If aItens[nInd][2] != cFilAnt .AND. !Empty(aItens[nInd][2])
            RmiFilInt(aItens[nInd][2],.T.)//Atuliza cfilAnt .T. 
        EndIf
        
        If !SB1->(DBSeek(xFilial("SB1")+PadR(aItens[nInd][1],TamSx3("B1_COD")[1])))
            LjGrvLog("GetImpPrd", "GetImpPrd -> Produto n�o encontrado FILIAL|B1_COD  ", cFilAnt+"|"+aItens[nInd][1])
            Exit
        EndIf

        nPreco   := STWFormPr( SB1->B1_COD, cCliente, Nil, cLojaCli, 1)
        cTesProd := IIf(Empty(cTesProd := RetFldProd(SB1->B1_COD,"B1_TS")), GetMv("MV_TESSAI"), cTesProd)
        
        If Empty(cTesProd)
            LjGrvLog("GetImpPrd", "RetFldProd -> TES do Produto n�o encontrado e MV_TESSAI esta Vazio FILIAL|B1_COD  ", cFilAnt+"|"+aItens[nInd][1])
            Exit
        EndIf
        If !(nPreco > 0)
            LjGrvLog("GetImpPrd", "STWFormPr -> Pre�o do Produto n�o encontrado FILIAL|B1_COD  ", cFilAnt+"|"+aItens[nInd][1])
            Exit
        EndIf
        
        nItem := MaFisAdd(SB1->B1_COD, cTesProd, 1, nPreco,;
                                    0, ""	 		, ""    		,				,;
                                    0 /*Frete*/   								, 0 /*Despesa*/	, 0 /*Seguro*/	,0 ,;
                                    nPreco	, 0	 	)
        
        For nY := 1 To Len(aCampos)
            xCampo1   	:= IIF("IT" $ aCampos[nY][1] , MaFisRet(nItem,aCampos[nY][1] ),MaFisRet(,aCampos[nY][1]))
            Aadd(aRet,{cFilAnt,SB1->B1_COD,aCampos[nY][1],xCampo1})    
        Next    
       
        
    Next nInd
    MafisEnd()
    If !Empty(cFilbkp)
        RmiFilInt(cFilbkp,.T.)//Atuliza cfilAnt .T. 
    EndIf
    
EndIf

RestArea(aArea)
Return aRet 
//-------------------------------------------------------------------
/*/{Protheus.doc} JsonImp
Fun��o que gera o Json com os campos da tabela passada, 
no registro da SB1 que esta posicionado

@author  Everson S P Junior
@since   07/08/20
@version 1.0
/*/
//-------------------------------------------------------------------
Function JsonImp(cJson,cCliente,cLojaCli)
Local aArea 	:= GetArea()
Local nInd,nY   := 0
Local nPreco    := 0
Local cTesProd  := ""
Local cFilbkp   := cFilAnt
Local xCampo1   := 0
Local nItem     := 0
Local aCampos := ACLONE( ArrayFis )//Campos da MatXfis definidos no include ArrayfunC.CH.

Default cCliente:= GetMv( "MV_CLIPAD" )		// Cliente padrao 
Default cLojaCli:= GetMv( "MV_LOJAPAD" )   // Loja padrao
Default cJoson  := "" 

cJson      := "{"

GeraJson(@cJson,"B1_FILIAL", SB1->B1_FILIAL) //Gera campo da B1 no Json
GeraJson(@cJson,"B1_COD", SB1->B1_COD)

If SB1->B1_FILIAL != cFilAnt
    RmiFilInt(SB1->B1_FILIAL,.T.)//Atuliza cfilAnt .T. 
EndIf

MaFisIni(cCliente ,cLojaCli	, "C"	,"S"	,;
                            'F'	,NIL		, NIL	,.F.	,;
                            "SB1"		,"LOJA701", "01"	,NIL	,;
                            NIL			,NIL		, NIL	,NIL	,;
                            NIL			,NIL		, .F.,.T.	)    
nPreco   := STWFormPr( SB1->B1_COD, cCliente, Nil, cLojaCli, 1)
cTesProd := IIf(Empty(cTesProd := RetFldProd(SB1->B1_COD,"B1_TS")), GetMv("MV_TESSAI"), cTesProd)
    
If Empty(cTesProd)
    LjGrvLog("GetImpPrd", "RetFldProd -> TES do Produto n�o encontrado e MV_TESSAI esta Vazio FILIAL|B1_COD  ", cFilAnt+"|"+aItens[nInd][1])
EndIf

If !(nPreco > 0)
    LjGrvLog("GetImpPrd", "STWFormPr -> Pre�o do Produto n�o encontrado FILIAL|B1_COD  ", cFilAnt+"|"+aItens[nInd][1])
EndIf
    
nItem := MaFisAdd(SB1->B1_COD, cTesProd, 1, nPreco,;
                    0, ""	 		, ""    		,				,;
                    0 /*Frete*/   								, 0 /*Despesa*/	, 0 /*Seguro*/	,0 ,;
                    nPreco	, 0	 	)
    
For nY := 1 To Len(aCampos)
    xCampo1   	:= IIF("IT" $ aCampos[nY] , MaFisRet(nItem,aCampos[nY] ),MaFisRet(,aCampos[nY]))
    GeraJson(@cJson,aCampos[nY], xCampo1)
Next    
cJson := SubStr(cJson, 1, Len(cJson)-1)
cJson += "}"

MafisEnd()
If !Empty(cFilbkp)
    RmiFilInt(cFilbkp,.T.)//Atuliza cfilAnt .T. 
EndIf

RestArea(aArea)
Return cJson

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraJson
Fun��o que gera o Json com os campos da MATAXFIS, 
no registro que esta posicionado

@author  Rafael Tenorio da Costa
@since   30/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GeraJson(cJson,cCampo, xType)
 
    Local cTipo      := ""
    Local xConteudo  := ""
    
    Default cCampo := ""
    Default xType  := ""
    
    LjGrvLog(" GeraJsonImp "," Function GeraJson()")
    
    cTipo     := Valtype(xType)
    xConteudo := xType    
    
    Do Case
        Case cTipo $ "C|M"

            //Retira as "" ou '', pois ocorre erro ao realizar o Parse do Json
            xConteudo := StrTran(xConteudo,'"','')
            xConteudo := StrTran(xConteudo,"'","")
            
            xConteudo := '"' + AllTrim(xConteudo) + '"'

        Case cTipo == "N"
            xConteudo := cValToChar(xConteudo)

        Case cTipo == "D"
            xConteudo := '"' + DtoS(xConteudo) + '"'

        Case cTipo == "L"
            xConteudo := IIF(xConteudo, "true", "false")
        
        OTherWise
            xConteudo := '"Tipo do campo inv�lido"'
    End Case
    
    cJson += '"' + AllTrim( cCampo ) + '":' + xConteudo + ","

Return cJson