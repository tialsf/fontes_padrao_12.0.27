#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIBUSCA.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiBusca
Servi�o que busca informa��es nos Assinantes

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiBusca(cEmpAmb, cFilAmb)

	Local lManual   := (cEmpAmb == Nil .Or. cFilAmb == Nil)
	Local lContinua := .T.
	Local cUser     := '000000'
    Local cSelect   := ""
    Local cTabela   := ""
	
    Default cEmpAmb := ""
	Default cFilAmb := ""

	If !lManual
		lContinua := .F.

		If !Empty(cEmpAmb) .And. !Empty(cFilAmb)
			lContinua := .T.

			//Para informar ao Server que consumir� licen�as
			RpcSetEnv(cEmpAmb, cFilAmb, , ,'LOJ', "RMIBUSCA")
            LjGrvLog(" RpcSetEnv ", "Iniciou ambiente: ", {cEmpAmb,cFilAmb,cModulo})    
			__CUSERID := cUser
		Else

            LjxjMsgErr( I18n(STR0001, {"RMIBUSCA"}), /*cSolucao*/)   //"Par�metros incorretos no servi�o #1."
		EndIf	
	EndIf
	
	If lContinua
	
		//Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
		If !LockByName("RMIBUSCA", .T., .T.)
            LjxjMsgErr( I18N(STR0002, {"RMIBUSCA"}), /*cSolucao*/)    //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
			Return Nil
		EndIf

        //Seleciona os assinantes com a busca ativa
        cTabela := GetNextAlias()
        cSelect := " SELECT MHO_COD"
        cSelect += " FROM " + RetSqlName("MHO") + " MHO INNER JOIN " + RetSqlName("MHP") + " MHP"
        cSelect +=      " ON MHO_FILIAL = MHP_FILIAL AND MHO_COD = MHP_CASSIN AND MHP_ATIVO = '1' AND MHP_TIPO = '2' AND MHO.D_E_L_E_T_ = MHP.D_E_L_E_T_"   //1=Ativo, 2=Busca
        cSelect += " WHERE MHO_FILIAL = '" + xFilial("MHO") + "' AND MHO.D_E_L_E_T_ = ' '"

        DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)

        While !(cTabela)->( Eof() )

	        StartJob("RmiBusExec", GetEnvServer(), .F./*lEspera*/, cEmpAnt, cFilAnt, (cTabela)->MHO_COD)
            Sleep(5000)
            
            (cTabela)->( DbSkip() )
        EndDo
        (cTabela)->( DbCloseArea() )

        //Libera a execu��o do login
        UnLockByName("RMIBUSCA", .T., .T.)
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiBusExec
Executa a busca

@author  Rafael Tenorio da Costa
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiBusExec(cEmpEnv, cFilEnv, cAssinante)

    Local cSemaforo := "RMIBUSEXEC" +"_"+ cEmpEnv +"_"+ cAssinante
    Local oBusca    := Nil
    Local lContinua := .T.
    Local oGrvMsg   := Nil //Inst�ncia da classe RmiGrvMsgPubChefObj

    // Protege rotina para que seja usada apenas no SIGALOJA / Front Loja
    RpcSetType(3) // Para nao consumir licen�as na Threads
    RpcSetEnv(cEmpEnv, cFilEnv, , ,"LOJ" , "RmiBusExec")
    nModulo := 12 //RpcSetEnv incia o modulo 5 por padr�o, para validar AmIIn(12) foi preciso mudar nModulo 
    If !AmIIn(12)
	    LjGrvLog(" RmiBusca ", "N�o foi encontrado Licen�a para o Varejo-SIGALOJA: ")    
        ConOut("RmiBusca - N�o foi encontrado Licen�a para o Varejo-SIGALOJA:" )
        Return(.F.)
    EndIf  
    //Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
    If !LockByName(cSemaforo, .T., .T.)
        LjxjMsgErr( I18n(STR0002, {cSemaforo}), /*cSolucao*/)   //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
        Return Nil
    EndIf

    cAssinante := AllTrim(cAssinante)

    Do Case

        Case cAssinante == "CHEF"
            oBusca := RmiBusChefObj():New()

        Case cAssinante == "LIVE"
            oBusca := RmiBusLiveObj():New()

        OTherWise
            LjxjMsgErr( I18n(STR0003, {cAssinante}), /*cSolucao*/)   //"Assinante #1 sem busca implementada."
            lContinua := .F.

    End Case

    //Envia as distribui��es ao assinante
    If lContinua 
    
        If oBusca:GetSucesso()
            oBusca:Processa()
        EndIf

        //Gera log de erro
        If !oBusca:getSucesso()
            oBusca:getRetorno()
        EndIf
    EndIf

    FwFreeObj(oBusca)
    Sleep(2000)

    Do Case

        Case cAssinante == "CHEF"
            //Inst�ncia da classe responsavel pela gera��o da mensagem
            oGrvMsg := RmiGrvMsgPubChefObj():New()

            //Gera a mensagem de publica��o no campo MHQ_MENSAG
            oGrvMsg:GeraMsg()

        Case cAssinante == "LIVE"
            //Inst�ncia da classe responsavel pela gera��o da mensagem
            oGrvMsg := RmiGrvMsgPubLiveObj():New()

            //Gera a mensagem de publica��o no campo MHQ_MENSAG
            oGrvMsg:GeraMsg()

    End Case

    //Limpa o objeto
    FwFreeObj(oGrvMsg)

    //Libera a execu��o do login
	UnLockByName(cSemaforo, .T., .T.)

Return Nil