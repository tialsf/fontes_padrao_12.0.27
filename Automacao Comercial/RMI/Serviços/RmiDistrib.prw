#INCLUDE "PROTHEUS.CH"
#INCLUDE "DBSTRUCT.CH"
#INCLUDE "RMIDISTRIB.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiDistrib
Servi�os que gera as Distribui��es

@author  Rafael Tenorio da Costa
@since   08/11/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiDistrib(cEmpAmb, cFilAmb)

	Local lManual   := (cEmpAmb == Nil .Or. cFilAmb == Nil)
	Local lContinua := .T.
	Local cUser     := '000000'
    Local cSelect   := ""
    Local cTabela   := ""
	
    Default cEmpAmb := ""
	Default cFilAmb := ""

	If !lManual
		lContinua := .F.

		If !Empty(cEmpAmb) .And. !Empty(cFilAmb)
			lContinua := .T.

			//Normalmente utiliza-se RPCSetType(3) para informar ao Server que a RPC n�o consumir� licen�as
			RpcSetEnv(cEmpAmb, cFilAmb, , , "LOJ", "RMIDISTRIB")
            LjGrvLog(" RmiDistrib ", "Iniciou ambiente: ", {cEmpAmb,cFilAmb,cModulo})    
			__CUSERID := cUser
		Else

            LjxjMsgErr( I18n(STR0001, {"RmiDistrib"}), /*cSolucao*/)   //"Par�metros incorretos no servi�o #1."
		EndIf	
	EndIf
	
	If lContinua
	
		//Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
		If !LockByName("RMIDISTRIB", .T., .T.)
            LjxjMsgErr( I18N(STR0002, {"RmiDistrib"}), /*cSolucao*/)    //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
			Return Nil
		EndIf

        //Seleciona os processos assinados
        cTabela := GetNextAlias()
        cSelect := " SELECT MHP_CPROCE"
        cSelect += " FROM " + RetSqlName("MHP")
        cSelect += " WHERE MHP_FILIAL = '" + xFilial("MHP") + "'"
        cSelect +=      " AND MHP_ATIVO = '1'"      //1=Sim
        cSelect +=      " AND MHP_TIPO = '1'"       //1=Envia
        cSelect +=      " AND D_E_L_E_T_ = ' '"
        cSelect += " GROUP BY MHP_CPROCE"

        DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)

        While !(cTabela)->( Eof() )

	        StartJob("RmiDistSel", GetEnvServer(), .F./*lEspera*/, cEmpAnt, cFilAnt, (cTabela)->MHP_CPROCE)
            Sleep(5000)
            
            (cTabela)->( DbSkip() )
        EndDo
        (cTabela)->( DbCloseArea() )

        //Libera a execu��o do login
        UnLockByName("RMIDISTRIB", .T., .T.)
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiDistSel
Seleciona os registros que ser�o distribuidos

@author  Rafael Tenorio da Costa
@since   08/11/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiDistSel(cEmpDist, cFilDist, cProcesso)

    Local cSemaforo  := "RMIDISTSEL" +"_"+ cEmpDist +"_"+ AllTrim(cProcesso)
    Local cSelect    := ""
    Local cTabela    := ""
    Local aAssinante := {}
    Local nAssi      := 0

    RpcSetType(3) // Para nao consumir licen�as na Threads
    RpcSetEnv(cEmpDist, cFilDist, , ,"LOJ" , "RmiDistSel")
    nModulo := 12 //RpcSetEnv incia o modulo 5 por padr�o, para validar AmIIn(12) foi preciso mudar nModulo 
    If !AmIIn(12)
	    LjGrvLog(" RmiDistrib ", "N�o foi encontrado Licen�a para o Varejo-SIGALOJA: ")    
        ConOut("RmiDistrib - N�o foi encontrado Licen�a para o Varejo-SIGALOJA:" )
        Return(.F.)
    EndIf 
    //Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
    If !LockByName(cSemaforo, .T., .T.)
        LjxjMsgErr( I18n(STR0002, {cSemaforo}), /*cSolucao*/)   //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
        Return Nil
    EndIf

    //Carrega Assinantes do Processo
    aAssinante := RmiXSql("SELECT MHP_CASSIN FROM " + RetSqlName("MHP") +;
                         " WHERE MHP_FILIAL = '" + xFilial("MHP") + "' AND MHP_CPROCE = '" + cProcesso +  "' AND MHP_ATIVO = '1' AND MHP_TIPO = '1' AND D_E_L_E_T_ = ' '", "*", /*lCommit*/, /*aReplace*/)

    //Seleciona as publica��es de um determinado processo, para serem distribuidas
    cTabela := GetNextAlias()
    cSelect := " SELECT R_E_C_N_O_ AS REGISTRO"
    cSelect += " FROM " + RetSqlName("MHQ") 
    cSelect += " WHERE MHQ_FILIAL = '" + xFilial("MHQ") + "' AND MHQ_CPROCE = '" + cProcesso + "' AND MHQ_STATUS = '1' AND D_E_L_E_T_ = ' '"
    
    DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)

    While !(cTabela)->( Eof() )

        For nAssi := 1 To Len(aAssinante)

            //Grava distribui��o
            RmiDistGrv(aAssinante[nAssi][1], cProcesso, (cTabela)->REGISTRO)

        Next nAssi

        (cTabela)->( DbSkip() )
    EndDo
    (cTabela)->( DbCloseArea() )

    aSize(aAssinante, 0)

    //Libera a execu��o do login
	UnLockByName(cSemaforo, .T., .T.)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiDistGrv
Grava a distribui��o na tabela MHR - Mensagens Distribuidas
Feito em Function para possibilitar a chamada de outros fontes.

@author  Rafael Tenorio da Costa
@since   08/11/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiDistGrv(cAssinante, cProcesso, nRecnoPub)

    Local aArea := GetArea()

    MHQ->( DbGoTo(nRecnoPub) )

    //N�o distribui publica��o onde a origem � igual ao destino
    If !MHQ->( Eof() ) .And. AllTrim(MHQ->MHQ_ORIGEM) <> AllTrim(cAssinante)

        Begin Transaction

            //Gera distribui��o
            RecLock("MHR", .T.)

                MHR->MHR_FILIAL := xFilial("MHR")
                MHR->MHR_CPROCE := cProcesso
                MHR->MHR_CASSIN := cAssinante
                MHR->MHR_RECPUB := nRecnoPub        //Antigo relacionamento entre Publica��o x Distribui��o, foi mantido para n�o dar erro na chave �nica.
                MHR->MHR_TENTAT := "0"
                MHR->MHR_STATUS := "1"              //1=A Processar;2=Processada;3=Erro
                MHR->MHR_UIDMHQ := MHQ->MHQ_UUID    //Id unico da MHQ para relacionamento com MHR.

            MHR->( MsUnLock() )

            //Atualiza publica��o
            RecLock("MHQ", .F.)

                MHQ->MHQ_STATUS := "2"              //1=A Processar;2=Processada;3=Erro
                MHQ->MHQ_DATPRO := Date()
                MHQ->MHQ_HORPRO := Time()

            MHQ->( MsUnLock() )

        End Transaction
    EndIf

    RestArea(aArea)

Return Nil