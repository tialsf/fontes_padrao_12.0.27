#INCLUDE "PROTHEUS.CH"
#INCLUDE "RMIENVIA.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiEnvia
Servi�os que ira enviar as distribui��es para os destinos

@author  Rafael Tenorio da Costa
@since   
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiEnvia(cEmpAmb, cFilAmb)

	Local lManual   := (cEmpAmb == Nil .Or. cFilAmb == Nil)
	Local lContinua := .T.
	Local cUser     := '000000'
    Local cSelect   := ""
    Local cTabela   := ""
	
    Default cEmpAmb := ""
	Default cFilAmb := ""

	If !lManual 
		lContinua := .F.

		If !Empty(cEmpAmb) .And. !Empty(cFilAmb)
			lContinua := .T.

			//Normalmente utiliza-se RPCSetType(3) para informar ao Server que a RPC n�o consumir� licen�as
			RpcSetEnv(cEmpAmb, cFilAmb, , ,"LOJ", "RMIENVIA")
            LjGrvLog(" RmiEnvia ", "Iniciou ambiente: ", {cEmpAmb,cFilAmb,cModulo})
			__CUSERID := cUser
		Else

            LjxjMsgErr( I18n(STR0001, {"RMIENVIA"}), /*cSolucao*/)   //"Par�metros incorretos no servi�o #1."
		EndIf	
	EndIf

	LjGrvLog(" RmiEnvia ","Iniciando: "+" cEmpImp = " + cEmpAmb+" cFilImp = "+cFilAmb )

	If lContinua
        LjGrvLog(" RmiEnvia ","lContinua: .T. ")
		//Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
		If !LockByName("RMIENVIA", .T., .T.)
            LjGrvLog(" RMIENVIA ",I18N(STR0002, {"RMIENVIA"}) )
            LjxjMsgErr( I18N(STR0002, {"RMIENVIA"}), /*cSolucao*/)    //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
			Return Nil
		EndIf

        //Seleciona os assinantes
        cTabela := GetNextAlias()
        cSelect := " SELECT MHO_COD"
        cSelect += " FROM " + RetSqlName("MHO") + " MHO INNER JOIN " + RetSqlName("MHP") + " MHP"
        cSelect +=      " ON MHO_FILIAL = MHP_FILIAL AND MHO_COD = MHP_CASSIN AND MHP_ATIVO = '1' AND MHP_TIPO = '1' AND MHO.D_E_L_E_T_ = MHP.D_E_L_E_T_"   //1=Ativo, 1=Envia
        cSelect += " WHERE MHO_FILIAL = '" + xFilial("MHO") + "' AND MHO.D_E_L_E_T_ = ' '"
        
        LjGrvLog(" RMIENVIA ", "Antes do While QUERY Seleciona os assinantes: "+cSelect )    
        DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)
        
        If !(cTabela)->( Eof() )
            LjGrvLog(" RmiPublica ", "Entrou no While e vai executar (RmiEnvExec) : ")
        else
            LjGrvLog(" RmiPublica ", "N�o entro no While: ")        
        EndIf 
        
        While !(cTabela)->( Eof() )

	        StartJob("RmiEnvExec", GetEnvServer(), .F./*lEspera*/, cEmpAnt, cFilAnt, (cTabela)->MHO_COD)
            Sleep(5000)
            
            (cTabela)->( DbSkip() )
        EndDo
   
        (cTabela)->( DbCloseArea() )

        //Libera a execu��o do login
        UnLockByName("RMIENVIA", .T., .T.)
	EndIf

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiEnvExec
Executa o envio

@author  Rafael Tenorio da Costa
@since   
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiEnvExec(cEmpEnv, cFilEnv, cAssinante)

    Local cSemaforo := "RMIENVEXEC" +"_"+ cEmpEnv +"_"+ cAssinante
    Local oEnvio    := Nil
    Local lContinua := .T.

    RpcSetType(3) // Para nao consumir licen�as na Threads
    RpcSetEnv(cEmpEnv, cFilEnv, , ,"LOJ", "FRTA271D")// Nome FRTA271D para manter o padrao do loja na grava��o de movimento de caixa FK5_ORIGEM 
    nModulo := 12 //RpcSetEnv inciar o modulo 5 por padr�o, para validar AmIIn(12) foi preciso mudar nModulo 
    If !AmIIn(12)
	    LjGrvLog(" RmiEnvExec ", "N�o foi encontrado Licen�a para o Varejo-SIGALOJA: ")    
        ConOut("RmiEnvExec - N�o foi encontrado Licen�a para o Varejo-SIGALOJA:" )
        Return(.F.)
    EndIf 
    
    LjGrvLog(" RmiEnvExec ", "Iniciou ambiente para Empresa, filial e Assinante : ", {cEmpEnv,cFilEnv,cAssinante})
    //Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
    If !LockByName(cSemaforo, .T., .T.)
        LjGrvLog(" RmiEnvExec ", I18n(STR0002, {cSemaforo}))
        LjxjMsgErr( I18n(STR0002, {cSemaforo}), /*cSolucao*/)   //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
        Return Nil
    EndIf

    cAssinante := AllTrim(cAssinante)

    Do Case

        Case cAssinante == "CHEF"
            LjGrvLog(" RmiEnvExec ", "Assinante igual a CHEF cria objeto-> oEnvio : ")
            oEnvio := RmiEnvChefObj():New()

        Case cAssinante == "LIVE"
            LjGrvLog(" RmiEnvExec ", "Assinante igual a LIVE cria objeto-> oEnvio : ")
            oEnvio := RmiEnvLiveObj():New()

        Case cAssinante == "PROTHEUS"
            LjGrvLog(" RmiEnvExec ", "Assinante igual a PROTHEUS cria objeto-> oEnvio : ")
            oEnvio := RmiEnvProtheusObj():New()

        OTherWise
            LjGrvLog(" RmiEnvExec ", I18n(STR0003, {cAssinante}))//"Assinante #1 sem envio implementado."
            LjxjMsgErr( I18n(STR0003, {cAssinante}), /*cSolucao*/)   //"Assinante #1 sem envio implementado."
            lContinua := .F.

    End Case

    //Envia as distribui��es ao assinante
    If lContinua 
    
        If oEnvio:GetSucesso()
            LjGrvLog(" RmiEnvExec ", " executado o oEnvio:GetSucesso() = .T. e vai executar a rotina oEnvio:Processa() ")
            oEnvio:Processa()
        EndIf

        //Gera log de erro
        If !oEnvio:getSucesso()
            LjGrvLog(" RmiEnvExec ", " executado o oEnvio:GetSucesso() = .F. e Vai recuperar o erro ")
            oEnvio:getRetorno()
            LjGrvLog(" RmiEnvExec ",oEnvio:getRetorno())
        EndIf
    EndIf

    FwFreeObj(oEnvio)

    //Libera a execu��o do login
	UnLockByName(cSemaforo, .T., .T.)

Return Nil