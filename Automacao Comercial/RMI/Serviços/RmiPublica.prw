#INCLUDE "PROTHEUS.CH"
#INCLUDE "DBSTRUCT.CH"
#INCLUDE "RMIPUBLICA.CH"
#INCLUDE "TRYEXCEPTION.CH"

Static lFields := .F.  //Verifica se existem os campo MHN_FILTRO e MHS_FILTRO
Static oError  := Nil //Objeto que recebe a exce��o que ocorreu na execu��o da query

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiPublica
Servi�os que gera as Publica��es

@author  Rafael Tenorio da Costa
@since   26/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiPublica(cEmpImp, cFilImp)

	Local lManual   := (cEmpImp == Nil .Or. cFilImp == Nil)
	Local lContinua := .T.
	Local cUser     := '000000'
    Local cSelect   := "" 
    Local cTabela   := ""
    Local aTable    := {}   //Array que ira receber a tabela principal (MHN) e os filhos da tabela (MHS)
    Local nPos      := 0    //Recebe a posicao de uma determinada tabela no array aTable
    Local nX        := 0    //Variavel de loop
	
    Default cEmpImp := ""
	Default cFilImp := ""

    If !lManual
		lContinua := .F.

		If !Empty(cEmpImp) .And. !Empty(cFilImp)
			lContinua := .T.

			//Normalmente utiliza-se RPCSetType(3) para informar ao Server que a RPC n�o consumir� licen�as
			RpcSetEnv(cEmpImp, cFilImp, , ,"LOJ", "RMIPUBLICA")
			__CUSERID := cUser
            LjGrvLog(" RmiPublica ", "Iniciou ambiente: ", {cEmpImp,cFilImp,cModulo})
		Else

            LjxjMsgErr(STR0001, /*cSolucao*/)   //"Par�metros incorretos no servi�o."
		EndIf	
	EndIf

    LjGrvLog(" RmiPublica ","Iniciando: "+" cEmpImp = " + cEmpImp+" cFilImp = "+cFilImp )

	If lContinua
        LjGrvLog(" RmiPublica ","lContinua: .T. ")
		//Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
		If !LockByName("RMIPUBLICA", .T., .T.)
            LjGrvLog(" RmiPublica ",I18N(STR0002, {"RmiPublica"}) )
            LjxjMsgErr( I18N(STR0002, {"RmiPublica"}), /*cSolucao*/)    //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
			Return Nil
		EndIf

        lFields := MHN->(ColumnPos("MHN_FILTRO")) > 0 .AND. MHS->(ColumnPos("MHS_FILTRO")) > 0

        //Seleciona os processos assinados
        cTabela := GetNextAlias()
        cSelect := " SELECT MHN_COD, MHN_TABELA, MHN_CHAVE, MHS_TABELA, MHS_CHAVE" + IIF(lFields, ", MHN_FILTRO, MHS_FILTRO ", "")
        cSelect += " FROM " + RetSqlName("MHN") + " MHN INNER JOIN " + RetSqlName("MHP") + " MHP"
        cSelect +=      " ON MHN_FILIAL = MHP_FILIAL AND MHN_COD = MHP_CPROCE AND MHN.D_E_L_E_T_ = MHP.D_E_L_E_T_ AND MHN_TABELA <> 'XXX' "
        cSelect += " LEFT JOIN " + RetSqlName("MHS") + " MHS "
        cSelect +=      " ON MHS_FILIAL = MHN_FILIAL AND MHS_CPROCE = MHN_COD AND MHS.D_E_L_E_T_ = ' '"
        cSelect += " WHERE MHN.D_E_L_E_T_ = ' ' "
        cSelect += " AND MHP_ATIVO = '1'"      //1=Sim
        cSelect += " AND MHP_TIPO = '1'"       //1=Envia
        cSelect += " AND MHP_CASSIN <> 'PROTHEUS' "//Se n�o existir assinante diferente do Protheus para o envio n�o gera MHQ.
        cSelect += " GROUP BY MHN_COD, MHN_TABELA, MHN_CHAVE, MHS_TABELA, MHS_CHAVE" + IIF(lFields, ", MHN_FILTRO, MHS_FILTRO ", "")
        
        LjGrvLog(" RmiPublica ", "Antes do While QUERY Seleciona os processos assinados: "+cSelect )
        DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)

        While !(cTabela)->( Eof() )
            
            //Verifico no array aTable se determinada tabela do cabe�alho ja existe no array
            nPos := aScan(aTable,{|x| AllTrim(x[1]) == AllTrim( (cTabela)->MHN_COD ) })

            If nPos == 0
                //Caso a tabela ainda nao existe no array, entao � add no array a tabela principal mais as filhas
                Aadd(aTable,{AllTrim( (cTabela)->MHN_COD ),AllTrim( (cTabela)->MHN_TABELA ),{},IIF(lFields,(cTabela)->MHN_FILTRO,"")})
                If !Empty( (cTabela)->MHS_TABELA )
                    Aadd(aTable[Len(aTable)][3],{AllTrim( (cTabela)->MHS_TABELA ),AllTrim( (cTabela)->MHS_CHAVE ), (cTabela)->MHN_CHAVE, IIF(lFields,(cTabela)->MHS_FILTRO,"") })
                EndIf
            Else
                //Se a tabela ja existe no array, entao add apenas mais uma posi��o para incluir a tabela filha
                If !Empty( (cTabela)->MHS_TABELA )
                    Aadd(aTable[nPos][3],{AllTrim( (cTabela)->MHS_TABELA ),AllTrim( (cTabela)->MHS_CHAVE ), (cTabela)->MHN_CHAVE, IIF(lFields,(cTabela)->MHS_FILTRO,"") })
                EndIf
            EndIf

            (cTabela)->( DbSkip() )
        EndDo
        If !Empty(aTable)
            LjGrvLog(" RmiPublica ", "Entrou no While MHN e MHS resultado: ", aTable)
        else
            LjGrvLog(" RmiPublica ", "N�o entro no While da Tabela MHN e MHS: ")        
        EndIf    
        (cTabela)->( DbCloseArea() )

        For nX := 1 To Len(aTable)
	        StartJob("RmiPubSel", GetEnvServer(), .F./*lEspera*/, cEmpAnt, cFilAnt, aTable[nX] )
            Sleep(5000)
        Next nX

        //Libera a execu��o do login
        UnLockByName("RMIPUBLICA", .T., .T.)
	EndIf
    LjGrvLog(" RmiPublica ","Fim do Processamento" )
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiPubSel
Seleciona os registros que ser�o publicados

@author  Rafael Tenorio da Costa
@since   26/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiPubSel(cEmpPub, cFilPub, aProcesso)

    Local cSemaforo := "RMIPUBSEL" +"_"+ cEmpPub +"_"+ aProcesso[1]
    Local cSelect   := ""
    Local cTabela   := ""
    Local cPrefixo  := IIF( SubStr( aProcesso[2], 1, 1) == "S", SubStr( aProcesso[2], 2), aProcesso[2] )
    Local cCmpExp   := cPrefixo + "_MSEXP"

    RpcSetType(3) // Para nao consumir licen�as na Threads
    RpcSetEnv(cEmpPub, cFilPub, , ,"LOJ", "RMIPUBSEL") 
    nModulo := 12 //RpcSetEnv incia o modulo 5 por padr�o, para validar AmIIn(12) foi preciso mudar nModulo 
    If !AmIIn(12)
	    LjGrvLog(" RmiPubSel ", "N�o foi encontrado Licen�a para o Varejo-SIGALOJA: ")    
        ConOut("RmiPubSel - N�o foi encontrado Licen�a para o Varejo-SIGALOJA:" )
        Return(.F.)
    EndIf

    LjGrvLog(" RmiPublica ", "Job RMIPUBSEL iniciado  ")  
    LjGrvLog(" RMIPUBSEL ", "Processo:   ",aProcesso)  
    //Trava a execu��o para evitar que mais de uma sess�o fa�a a execu��o.
    If !LockByName(cSemaforo, .T., .T.)
        LjGrvLog(" RMIPUBSEL ",I18n(STR0002, {cSemaforo}))
        LjxjMsgErr( I18n(STR0002, {cSemaforo}), /*cSolucao*/)   //"Servi�o #1 j� esta sendo utilizado por outra inst�ncia."
        Return Nil
    EndIf

    //Valida existencia do campo de controle de exporta��o
    If (aProcesso[2])->( ColumnPos(cCmpExp) ) == 0
        LjGrvLog(" RMIPUBSEL ",I18n(STR0003, {cCmpExp}))
        LjxjMsgErr( I18n(STR0003, {cCmpExp}), /*cSolucao*/)     //"Campo #1 n�o existe, o servi�o de publica��o n�o ser� executado."
    Else

        //Seleciona os registros que ser�o publicados inclui tambem os deletados
        cTabela := GetNextAlias()

        TRY EXCEPTION
            cSelect := "SELECT R_E_C_N_O_ AS REGISTRO FROM " + RetSqlName(aProcesso[2]) + " WHERE " + cCmpExp + " = '" + Space( TamSx3(cCmpExp)[1] ) + "'" + IIF(!Empty(aProcesso[4]), ' AND ' + AllTrim(aProcesso[4]), "")
            DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)
            LjGrvLog(" RMIPUBSEL ","QUERY Seleciona os registros que ser�o publicados "+cSelect)
            
        //Se ocorreu erro
		CATCH EXCEPTION USING oError        
            LjGrvLog(" RMIPUBSEL ","Erro ao executar a query " + AllTrim(cSelect) + ". O erro pode estar ocorrendo pois o filtro cadastrado para a tabela " + aProcesso[2] + " pode estar errado, por favor, verifique no cadastro de processo se o filtro esta cadastrado corretamente. Filtro -> " +  ' AND ' + AllTrim(aProcesso[4]))
        ENDTRY

        //Se for igual a Nil, eh porque n�o deu erro na execu��o da query, segue o processo normal
        If oError == Nil

            While !(cTabela)->( Eof() )

                //Grava publica��o
                RmiPubGrv(aProcesso[1], aProcesso[2], (cTabela)->REGISTRO, cCmpExp, aProcesso[3])

                (cTabela)->( DbSkip() )
            EndDo
            (cTabela)->( DbCloseArea() )        

        Else
            //Se for diferente de Nil, deu erro na query e neste caso apenas limpo a variavel
            //Essa variavel oError eh apenas uma variavel de controle
            oError := Nil
        EndIf

    EndIf

    //Libera a execu��o do login
	UnLockByName(cSemaforo, .T., .T.)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} RmiPubGrv
Grava a publica��o na tabela MHQ - Mensagens Publicadas

@author  Rafael Tenorio da Costa
@since   26/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Function RmiPubGrv(cProcesso, cTabela, nRegistro, cCmpExp, aTableFil) 

    Local aArea     := GetArea()
    Local cJson     := ""
    Local cCmpHrExp := StrTran(cCmpExp, "_MSEXP", "_HREXP")
    Local cChave    := StrTran( Posicione("MHN", 1, xFilial("MHN") + cProcesso, "MHN_CHAVE"), "+", "+ '|' +" )      //Retorna campos que compoem a chave da publica��o
    Local nX        := 0	//Variavel de loop 
    Local cTabTemp  := "" 	//Variavel que recebera a tabela temporaria 
    Local cJsonFilho:= "" 	//Variavel que recebera o Json das tabelas filhas 
    Local lControle := .T. 	//Variavel de controle do Json das tabelas filhas 

    Default aTableFil := {} //Tabelas filhas da tabela que esta no parametro cTabela 
    LjGrvLog(" RmiPubGrv ","Grava a publica��o na tabela MHQ Function RmiPubGrv(",{cProcesso, cTabela, nRegistro, cCmpExp, aTableFil})
    //Considera os registros deletados
    SET DELETED OFF

    (cTabela)->( DbGoTo(nRegistro) )

    If !(cTabela)->( Eof() )
        LjGrvLog(" RmiPubGrv ","chamada da funcao GeraJson("+cTabela+", 0)")
        cJson := GeraJson(cTabela, 0)
        LjGrvLog(" RmiPubGrv ","Resultado cJson GeraJson "+cJson)
        //Complementa o cJson com as tabelas filhas
        For nX := 1 To Len(aTableFil) 
            
            cTabTemp  := GeraQuery(cTabela, aTableFil[nX])

            //Se for igual a Nil, eh porque n�o deu erro na execu��o da query, segue o processo normal
            If oError == Nil

                lControle := .T.

                While !(cTabTemp)->( Eof() )

                    (aTableFil[nX][1])->( DbGoTo( (cTabTemp)->REGISTRO_FILHO ) )
                    LjGrvLog(" RmiPubGrv ","chamada da funcao GeraJson(",{aTableFil[nX][1],",1,",lControle})
                    cJsonFilho  += GeraJson(aTableFil[nX][1], 1, lControle)
                    LjGrvLog(" RmiPubGrv ","Resultado cJsonFilho GeraJson "+cJsonFilho)
                    lControle   := .F.
                    (cTabTemp)->( DbSkip() )
                EndDo

                If !Empty(cJsonFilho)
                    cJson := SubStr(cJson,1,Len(cJson) - 1) + ',' + SubStr(cJsonFilho,1,Len(cJsonFilho) - 1) + CRLF + "],"
                EndIf
                cJsonFilho := ""

                (cTabTemp)->( DbCloseArea() )   

            Else
                //Caso tenha erro, interrompe o loop e n�o grava a MHQ
                Exit
            EndIf        

        Next nX

        If oError == Nil

            cJson := SubStr(cJson,1,Len(cJson) - 1) + CRLF + '}' 
            LjGrvLog(" RmiPubGrv ","Resultado Json que ser� gravado -> "+cJson)
            Begin Transaction

                RecLock("MHQ", .T.)

                    MHQ->MHQ_FILIAL := xFilial("MHQ")
                    MHQ->MHQ_ORIGEM := "PROTHEUS"
                    MHQ->MHQ_CPROCE := cProcesso
                    MHQ->MHQ_EVENTO := IIF( (cTabela)->( Deleted() ), "2", "1")     //1=Atualiza��o;2=Exclus�o
                    MHQ->MHQ_CHVUNI := (cTabela)->&(cChave)
                    MHQ->MHQ_MENSAG := cJson
                    MHQ->MHQ_DATGER := Date()
                    MHQ->MHQ_HORGER := Time()
                    MHQ->MHQ_STATUS := "1"                                          //1=A Processar;2=Processada;3=Erro
                    MHQ->MHQ_UUID   := FWUUID("MHQ") //Gera chave unica 

                MHQ->( MsUnLock() )

                RecLock(cTabela, .F.)
                    If (cTabela)->( ColumnPos(cCmpExp) ) > 0
                        (cTabela)->&( cCmpExp ) := DtoS( Date() )
                    EndIf

                    If (cTabela)->( ColumnPos(cCmpHrExp) ) > 0
                        (cTabela)->&( cCmpHrExp ) := Time()
                    EndIf
                (cTabela)->( MsUnLock() )
                //Caso exista imposto assinado para o produto
                If VLDIMPB1(@cJson)
                    RecLock("MHQ", .T.)
                    MHQ->MHQ_FILIAL := xFilial("MHQ")
                    MHQ->MHQ_ORIGEM := "PROTHEUS"
                    MHQ->MHQ_CPROCE := "IMPOSTO PROD"
                    MHQ->MHQ_EVENTO := IIF( (cTabela)->( Deleted() ), "2", "1")     //1=Atualiza��o;2=Exclus�o
                    MHQ->MHQ_CHVUNI := (cTabela)->&(cChave)
                    MHQ->MHQ_MENSAG := cJson
                    MHQ->MHQ_DATGER := Date()
                    MHQ->MHQ_HORGER := Time()
                    MHQ->MHQ_STATUS := "1"                                          //1=A Processar;2=Processada;3=Erro
                    MHQ->MHQ_UUID   := FWUUID("MHQ") //Gera chave unica 
                    MHQ->( MsUnLock() )
                EndIf

            End Transaction
        Else
            //Se for diferente de Nil, deu erro na query e neste caso apenas limpo a variavel
            //Essa variavel oError eh apenas uma variavel de controle
            oError := Nil
        EndIf
    EndIf

    //Desconsidera os registros deletados
	SET DELETED ON
    LjGrvLog(" RmiPubGrv ","Fim da Grava��o ")
    RestArea(aArea)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraJson
Fun��o que gera o Json com os campos da tabela passada, 
no registro que esta posicionado

@author  Rafael Tenorio da Costa
@since   30/09/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GeraJson(cTabela, nTypeJson, lControle)

    Local cJson      := "{"
    Local cTipo      := ""
    Local cCampo     := ""
    Local xConteudo  := ""
    Local nCont      := 1
    Local aStructExp := (cTabela)->( DbStruct() )
    Local cPrefixo   := ""
    
    Default nTypeJson := 0 
    Default lControle := .F.
    LjGrvLog(" GeraJson "," Function GeraJson(",{cTabela, nTypeJson, lControle})
    If lControle 
        cJson := '"' + cTabela + '": ['
        cJson += CRLF + ' { ' 
    EndIf
    If !Empty(aStructExp)
        cCampo    := AllTrim(aStructExp[1][DBS_NAME] )
        cPrefixo  := SubStr(cCampo, 1, at('_',cCampo)-1)
    EndIf    
    LjGrvLog(" GeraJson "," aStructExp estrutura da tabela -> ",aStructExp)
    For nCont:=1 To Len(aStructExp)
        
        cTipo     := AllTrim( aStructExp[nCont][DBS_TYPE] )
        cCampo    := AllTrim( aStructExp[nCont][DBS_NAME] )
        
        If cCampo $  ("'"+cPrefixo+"_USERLGA'|"+ "'"+ cPrefixo+"_USERLGI'|")
           Loop 
        EndIf
        
        xConteudo := (cTabela)->&( cCampo )

            Do Case
                Case cTipo $ "C|M"

                    //Retira as "" ou '', pois ocorre erro ao realizar o Parse do Json
                    xConteudo := StrTran(xConteudo,'"','')
                    xConteudo := StrTran(xConteudo,"'","")
                    
                    xConteudo := '"' + AllTrim(xConteudo) + '"'

                Case cTipo == "N"
                    xConteudo := cValToChar(xConteudo)

                Case cTipo == "D"
                    xConteudo := '"' + DtoS(xConteudo) + '"'

                Case cTipo == "L"
                    xConteudo := IIF(xConteudo, "true", "false")
                
                OTherWise
                    xConteudo := '"' + STR0004 + '"'    //"Tipo do campo inv�lido"
            End Case

            cJson += '"' + AllTrim( cCampo ) + '":' + xConteudo + ","
    Next nCont

    cJson := SubStr(cJson, 1, Len(cJson)-1)
    If nTypeJson == 1 
        cJson += "},"
    Else
        cJson += "}"
    EndIf

    aSize(aStructExp, 0)

Return cJson

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraQuery
Gera a query da tabela pai com a(s) tabela(s) filha(s)

@author  Bruno Almeida
@since   31/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function GeraQuery(cTb, aTb)

    Local cQuery    := ""                   //Recebe a query
    Local cCont     := GetNextAlias()       //Recebe o nome do proximo alias
    Local aCab      := Separa(aTb[3],'+')   //Variavel que ira receber a chave do cabecalho
    Local aSec      := Separa(aTb[2],'+')   //Variavel que ira receber a chave das tabelas secundarias
    Local nX        := 0                    //Variavel de loop

    cQuery := "SELECT B.R_E_C_N_O_ AS REGISTRO_FILHO "
    cQuery += "  FROM " + RetSqlName(cTb) + " A "
    cQuery += "           LEFT JOIN " + RetSqlName(aTb[1]) + " B " + " ON " + RetJoin(aTb) + " AND B.D_E_L_E_T_ = ' ' "
    cQuery += " WHERE "

    For nX := 1 To Len(aSec)
        cQuery += 'B.' + AllTrim(aSec[nX]) + " = " + RetCont(cTb,AllTrim(aCab[nX])) + ' AND '
    Next nX

    //Concatena na query o filtro da tabela filha
    If Len(aTb) >= 4 .AND. !Empty(aTb[4])
        cQuery += aTb[4] + ' AND '
    EndIf

    cQuery := SubStr(cQuery,1,Len(cQuery) - 4)
    LjGrvLog(" GeraQuery "," aquery da tabela pai com a(s) tabela(s) filha(s) -> ",cQuery)
    
    TRY EXCEPTION
        DbUseArea(.T., "TOPCONN", TcGenQry( , , cQuery), cCont, .T., .F.)
        
    //Se ocorreu erro
    CATCH EXCEPTION USING oError        
        LjGrvLog(" RMIPUBSEL ","Erro ao executar a query " + AllTrim(cQuery) + ". O erro pode estar ocorrendo pois o filtro cadastrado para a tabela " + aTb[1] + " pode estar errado, por favor, verifique no cadastro de processo se o filtro esta cadastrado corretamente. Filtro -> " +  " AND " + AllTrim(aTb[4]))
    ENDTRY

Return cCont

//-------------------------------------------------------------------
/*/{Protheus.doc} RetJoin
Retorna o relacionamento entre as tabelas

@author  Bruno Almeida
@since   01/11/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RetJoin(aTb)

    Local cRet  := "" //Variavel de retorno
    Local aCab  := Separa(aTb[3],'+') //Variavel que ira receber a chave do cabecalho
    Local aSec  := Separa(aTb[2],'+') //Variavel que ira receber a chave das tabelas secundarias
    Local nX    := 0 //Variavel de loop

    For nX := 1 To Len(aCab)

        cRet += 'B.' + AllTrim(aSec[nX]) + ' = ' + 'A.' + AllTrim(aCab[nX]) + ' AND '

    Next nX 

    cRet := SubStr(cRet,1,Len(cRet) - 4)

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} RetValor
Retorna o conteudo do campo que � passado como parametro

@author  Bruno Almeida
@since   01/11/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function RetCont(cTb,cCampo)

    Local cTipo     := TamSx3(cCampo)[3] //Pega o tipo do campo
    Local xConteudo := (cTb)->&( cCampo ) //Pega o conteudo do campo

    Do Case
        Case cTipo $ "C|M|L"
            xConteudo := "'" + xConteudo + "'"

        Case cTipo $ "N"
            xConteudo := cValToChar(xConteudo)

        Case cTipo == "D"
            xConteudo := "'" + DtoS(xConteudo) + "'"
        
    End Case

Return xConteudo


//-------------------------------------------------------------------
/*/{Protheus.doc} VLDIMPB1
Retorna se existe assinante para imposto do produto

@author  Everson S P Junior
@since   06/01/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static function VLDIMPB1(cJson)
Local lRet      := .F.
Local cTabela   := ""

LjGrvLog(" RMIPUBLICA "," Static VLDIMPB1 verifica se existe assinante para imposto do produto ")

cTabela := GetNextAlias()
cSelect := " SELECT MHN_COD, MHN_TABELA, MHN_CHAVE, MHS_TABELA, MHS_CHAVE"
cSelect += " FROM " + RetSqlName("MHN") + " MHN INNER JOIN " + RetSqlName("MHP") + " MHP"
cSelect +=      " ON MHN_FILIAL = MHP_FILIAL AND MHN_COD = MHP_CPROCE AND MHN.D_E_L_E_T_ = MHP.D_E_L_E_T_ AND MHN_TABELA = 'XXX' "
cSelect += " LEFT JOIN " + RetSqlName("MHS") + " MHS "
cSelect +=      " ON MHS_FILIAL = MHN_FILIAL AND MHS_CPROCE = MHN_COD AND MHS.D_E_L_E_T_ = ' '"
cSelect += " WHERE MHN.D_E_L_E_T_ = ' ' "
cSelect += " AND MHP_ATIVO = '1'"      //1=Sim
cSelect += " AND MHP_TIPO = '1'"       //1=Envia
cSelect += " GROUP BY MHN_COD, MHN_TABELA, MHN_CHAVE, MHS_TABELA, MHS_CHAVE"
DbUseArea(.T., "TOPCONN", TcGenQry( , , cSelect), cTabela, .T., .F.)

LjGrvLog(" RMIPUBLICA "," VLDIMPB1 Query para buscar assinante de Imposto Prod -> ",cSelect)
If !(cTabela)->(Eof())
    lRet := .T.
    LjGrvLog(" RMIPUBLICA ","VLDIMPB1 Assinante encontrado para Imposto Prod lRet = .T. ")
    JsonImp(@cJson)//Fun��o para gera��o de impostos no forma Json
EndIf

(cTabela)->( DbCloseArea()) 
Return lRet
