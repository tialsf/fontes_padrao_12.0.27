#INCLUDE "RWMAKE.CH"
#INCLUDE "CRD010C.CH"

//Pula Linha
#Define CTRL Chr(10)+Chr(13)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CRD010C   �Autor  �Danilo Calil        � Data �  06/24/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Ponto de entrada para adicionar ou mudar o conteudo da 	  ���
���          �impressao do comprovante de financiamento.                  ���
���          �E executado para os dois tipos de comprovantes OFF-LINE  e  ���
���          �ON-LINE.                                                    ���
�������������������������������������������������������������������������͹��
���Parametro � ExpA1 - Array com o texto que sera impresso                ���
���          � ExpN2 - Valor total do SL1                                 ���
���          � ExpN3 - Dias informados no SE4                             ���
���          � ExpN4 - Parcelas                                           ���
���          � ExpN5 - Valor da parcela                                   ���
�������������������������������������������������������������������������͹��
���Retorno   � ExpA1 - Array com o texto que sera impresso                ���
�������������������������������������������������������������������������͹��
���Uso       � SigaCRD                                                    ���
�������������������������������������������������������������������������͹��
���                         ATUALIZACOES SOFRIDAS                         ���
�������������������������������������������������������������������������Ĺ��
���Programador � Data   � BOPS �           Motivo da Alteracao            ���
�������������������������������������������������������������������������Ĺ��
���Danilo Calil�12.07.06�103046�Alterado a String para impressao          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function CRD010C()

Local aComp			:= ParamIxb[1] 							//Texto que sera impresso no comprovante
Local nVlrTot		:= ParamIxb[2] 							//Valor Total do SL1
Local nDiasSe4		:= ParamIxb[3] 							//Dias para pagto do SE4
Local nQtdFin		:= ParamIxb[4] 							//Parcelas             
Local nParcFin		:= ParamIxb[5] 							//Valor da Parcela
Local nAcrsFin		:= ParamIxb[6] 							//Porcentagem do Acrescimo
Local nPosParc 		:= 0 									//Posicao das parcelas no array de texto
Local nPosVlrTot	:= 0 									//Posicao do valor total no array de texto
Local cMvFiliais	:= SuperGetMV("MV_FILACRS",,"01")   	//Filiais que trabalham com o conceito de acrescimo.

If nAcrsFin > 0
	nParcFin := nParcFin + nAcrsFin
EndIf

If cFilAnt $ cMvFiliais
	
	//���������������������������������������������������������������������������Ŀ
	//�Atualiza o comprovante nas linhas de valor total e parcelas com outro texto�
	//�����������������������������������������������������������������������������
	//Parcela
	nPosVlrTot	:= Ascan(aComp,STR0001) 	//Linha do comprovante onde e informado o valor da compra numero de parcelas
	//Valor
	nPosParc 	:= Ascan(aComp,STR0002)		//Linha do comprovante onde e informado o numero de parcelas

	If nPosVlrTot > 0
		//"Para pagamento a vista em ate " + "dias R$"
		aComp[nPosVlrTot]:= 	" "+STR0003 + Alltrim(STR(nDiasSe4,0)) + STR0004 + STR0007 + ;
								Alltrim(Transform(nVlrTot,PesqPict("SL1","L1_VLRTOT")))
	Endif
	If nPosParc > 0
		//"Ou em" + "parcelas de R$ "
		aComp[nPosParc]  :=  	"    "+STR0005 + Alltrim(STR(nQtdFin,0)) + STR0006 + STR0007 + ;
								Alltrim(Transform(nParcFin,PesqPict("SL2","L2_VLRITEM")))
	Endif


EndIf

Return(aComp)