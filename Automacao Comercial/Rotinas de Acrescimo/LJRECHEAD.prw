#include "RWMAKE.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LJRECHEAD �Autor  �Andrea Farias       � Data �  02/15/06   ���
�������������������������������������������������������������������������͹��
���Desc.     �Permite ordenar os campos exibidos na tela de recebimentos  ���
���          �de acordo com o modelo do cliente.                          ���
�������������������������������������������������������������������������͹��
���Uso       � FRONT LOJA - RECEBIMENTOS                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function LJRECHEAD()

Local aRet		:= {}

//��������������������������������������������������������������������
//�Retornar a descricao, posicao e picture dos campos no aheader.    �
//��������������������������������������������������������������������
AAdd(aRet,{" "					, 1,	"@!" })
AAdd(aRet,{"Contrato"			, 12,	"@!" })
AAdd(aRet,{"T�tulo"	   			, 3, 	"@!" })
AAdd(aRet,{"Parcela"			, 4, 	"@!" })
AAdd(aRet,{"Tipo"				, 11, 	"@!" })
AAdd(aRet,{"Vencimento"			, 5, 	"@!" })
AAdd(aRet,{"Valor a Receber"	, 10, 	PesqPict("SE1", "E1_VALOR"	, 13)})
AAdd(aRet,{"Valor sem Acres"	, 6, 	PesqPict("SE1", "E1_VALOR"	, 13)})
AAdd(aRet,{"Multa"				, 7, 	PesqPict("SE1", "E1_MULTA"	, 13)})
AAdd(aRet,{"Juros"				, 8, 	PesqPict("SE1", "E1_JUROS"	, 13)})
AAdd(aRet,{"Desconto"  			, 9, 	PesqPict("SE1", "E1_DESCONT", 13)})
AAdd(aRet,{"Prefixo"			, 2,	"@!" })

Return(aRet)