#INCLUDE "RWMAKE.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "LJRECSELE.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LJRECSELE �Autor  �Danilo Calil        � Data �  02/16/06   ���
�������������������������������������������������������������������������͹��
���Desc.     � Marca todos os item do aTitulos, se o cliente quiser quitar���
���          � o contrato. Este controle eh feito pela posicao [18] do    ���
���          � aTitulos.                                                  ���
�������������������������������������������������������������������������͹��
���Uso       � FRONT LOJA - RECEBIMENTOS                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function LJRECSELE()

Local aTitulo		:= Paramixb[1]		//Array com os titulos a receber do cliente
Local oTit			:= Paramixb[2]    	//Objeto do ListBox
Local lNaoAcresc	:= .F.				//Tem direito a desconto do acrescimo.
Local nX       		:= 0
Local cTitulo  		:= ""
Local cParcela 		:= ""
Local lRet			:= .T.

cTitulo  := aTitulo[oTit:nAt][2]+aTitulo[oTit:nAt][3]
cParcela := aTitulo[oTit:nAt][4]

//Estrutura do array aTitulo:
//����������������Ŀ
//�2-  Prefixo     �
//�3-  NumTitulo   �
//�4-  Parcela     �
//�5-  Vencimento  �
//�6-  Valor       �
//�7-  VlrMulta    �
//�8-  VlrJuros    �
//�9-  VlrDesconto �
//�10- VlrRecebido �
//�11- Tipo        �
//�12- Contrato    �
//�13- Cliente     �
//�14- Loja        �
//�15- FilTit      �
//�16- NumRecno    �
//������������������

For nX := 1 To Len( aTitulo )
	If cTitulo == aTitulo[nX][2] + aTitulo[nX][3]
		If aTitulo[nX][4] < cParcela
			If !aTitulo[nX][1]
				lRet := .F.
			EndIf
		ElseIf aTitulo[nX][4] > cParcela .And. lRet
			If aTitulo[nX][1]
				lRet := .F.
			EndIf
		EndIf
	EndIF
Next nX

lNaoAcresc 	:= aTitulo[oTit:nAt][18]

If lRet
	If lNaoAcresc
		cContrato	:= aTitulo[oTit:nAt][12]	
		If !Empty(cContrato)
			If !aTitulo[oTit:nAt][1]
				//"Deseja quitar todos os titulos do contrato: "+"Atencao"
				If MsgNoYes(STR0001 + cContrato + "?" , STR0002)		
					aEval( aTitulo, {|x| IIf( x[12] == cContrato, x[1] := .T.,"") } )
				Else
					aTitulo[oTit:nAt][1] := .T.	    				
				EndIf
		    Else
				aTitulo[oTit:nAt][1] := .F.	    	
			EndIf	    
		EndIf
	Else
		If !aTitulo[oTit:nAt][1]
			aTitulo[oTit:nAt][1] := .T.	    				
		Else
			aTitulo[oTit:nAt][1] := .F.	    					
		EndIf
	EndIf
Else
	aTitulo[oTit:nAt][1] := .F.
EndIf


Return(aTitulo)