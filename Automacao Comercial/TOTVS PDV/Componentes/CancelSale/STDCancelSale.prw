#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

Static cDocPed := "" //Armazena o L1_DOCPED da venda

//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSChkRes
Verifica se existe reserva para todos itens da Venda

@param   cNumSale				Numero da Venda
@author  Varejo
@version P11.8
@since   29/03/2012
@return  lRet					Retorna se a venda est� em andamento ou n�o
/*/
//-------------------------------------------------------------------
Function STDCSChkRes( cNumSale )
Local lRet		:=	.T.					// Retorna se todos os itens s�o de reserva
Local aArea		:= GetArea()			// Guarda Area
Local aAreaSL2	:= {}
Local cFilSL2	:= ""

Default cNumSale:= ""

LjGrvLog(cNumSale,"Valida��o se venda possui reserva para permitir cancelamento")

DbSelectArea("SL2")
aAreaSL2 := SL2->(GetArea())	// Guarda area do SL2
SL2->( DbSetOrder(1) )//L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
cFilSL2 := xFilial("SL2")
LjGrvLog(cNumSale," Itens a serem procurados - Dados: L2_FILIAL [" + cFilSL2 + "] || L2_NUM [" + cNumSale + "]")

If SL2->( DbSeek( cFilSL2 + cNumSale ) )
	LjGrvLog(cNumSale,"Achou os itens da venda",cFilSL2 + cNumSale)
	While SL2->( !EOF() ) .AND. (SL2->L2_FILIAL + SL2->L2_NUM == cFilSL2 + cNumSale)
		LjGrvLog(cNumSale,"SL2->L2_ENTREGA",SL2->L2_ENTREGA)
		LjGrvLog(cNumSale,"SL2->L2_RESERVA",SL2->L2_RESERVA)
		If Empty(AllTrim(SL2->L2_ENTREGA)) .OR. (SL2->L2_ENTREGA == "2") .OR. Empty(AllTrim(SL2->L2_RESERVA))
			lRet := .F.
			LjGrvLog(cNumSale,"N�o tem reserva, permite cancelar a venda - lRet = .F.",lRet)
			Exit
		EndIf
		SL2->( DbSkip() )
	EndDo              
Else
	LjGrvLog(cNumSale,"Numero de or�amento n�o encontrado - L1_NUM [" + cNumSale + "]")
	lRet := .F.
EndIf	

RestArea(aAreaSL2)
RestArea(aArea)               

Iif(lRet,LjGrvLog(cNumSale,"Existe(m) produto(s) de reserva - venda n�o pode ser cancelada"),NIL)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSLastSale
Busca Numero do Ultimo Or�amento

@param   cRetField - Nome do campo no dicionario de dados da tabela SL1
@author  Varejo
@version P11.8
@since   29/03/2012
@return  cNumSale - Retorna o Numero ou outro campo da tabela SL1 da Ultima Venda
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSLastSale(cRetField)

Local cNumSale	:= ""			// Retorna o Numero da Ultima Venda ou o campo do parametro
Local nRecnoL1	:= 0			// Armazena Recno do ultimo registro f�sico da SL1
Local aArea		:= {}			// Armazena Area corrente

Default cRetField := "L1_NUM"

aArea 		:= SL1->(GetArea())

//Posiciona no ultimo registro da SL1
DbSelectArea("SL1")
SL1->(DbSetOrder(1))
nRecnoL1 := SL1->(LASTREC())
SL1->(DbGoto(nRecnoL1))
While !BOF() .And. SL1->( Deleted() )
	SL1->(DbSkip(-1))
EndDo 
cNumSale := &("SL1->("+cRetField+")")

LjGrvLog( "STDCSLastSale", "Ultimo registro da SL1 posicionado:"+cRetField,cNumSale)

RestArea(aArea)

Return cNumSale


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSDoc
Busca o DOC da venda

@param   cNumSale				Numero da Venda
@author  Varejo
@version P11.8
@since   29/03/2012
@return  cDoc					Retorna o L1_DOC da Venda
@obs     
@sample
/*/
//-------------------------------------------------------------------
// 
Function STDCSDoc( cNumSale )

Local cDoc 		:= ""				// Retorna o L1_DOC da Venda
Local aArea		:= {}				// Armazena Area corrente
Local aAreaSL1	:= {}				// Armazena Area SL1

Default cNumSale:= ""

aArea 		:= GetArea()
aAreaSL1 	:= SL1->(GetArea())

DbSelectArea("SL1")
DbSetOrder(1)//L1_FILIAL+L1_NUM
		
If (xFilial("SL1") == SL1->L1_FILIAL) .AND. (SL1->L1_NUM == cNumSale) // Ja posicionado

	cDocPed := SL1->L1_DOCPED
	If Empty(cDocPed)
		cDoc := SL1->L1_DOC	
	EndIf 

	STDSPBasket("SL1","L1_CLIENTE"	,SL1->L1_CLIENTE)
	STDSPBasket("SL1","L1_LOJA"	,SL1->L1_LOJA)
	STDSPBasket("SL1","L1_CGCCLI"	,SL1->L1_CGCCLI)

Else 

	If DbSeek( xFilial("SL1") + cNumSale )

		cDocPed := SL1->L1_DOCPED
		If Empty(cDocPed)
			cDoc := SL1->L1_DOC	
		EndIf 
		
		//Alimenta o codigo e o nome do cliente na cesta
		STDSPBasket("SL1","L1_CLIENTE"	,SL1->L1_CLIENTE)
		STDSPBasket("SL1","L1_LOJA"	,SL1->L1_LOJA)
		STDSPBasket("SL1","L1_CGCCLI"	,SL1->L1_CGCCLI)
	EndIf
		
EndIf

RestArea(aAreaSL1)
RestArea(aArea)
	
Return cDoc


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSNum
Busca L1_NUM a partir do DOC/PDV

@param   cNumDoc				Numero do documento (L1_DOC)
@param   cPDV					Numero do PDV
@author  Varejo
@version P11.8
@since   29/03/2012
@return  cNum					Retorna o numero da vendaa L1_NUM
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSNum( cNumDoc , cPDV, lDocNf, cSerie )

Local cNum 			:= ""						// Retorna L1_NUM
Local aArea 			:= GetArea()				// Armazena Area corrente
Local aAreaSL1 		:= SL1->(GetArea())		// Armazena Area SL1
Local cSerNf		:= "" //Serie nao fiscal
Local cDocNf		:= "" //Numero do documento nao fiscal

Default cNumDoc  	:= ""
Default cPDV  		:= ""
Default lDocNf		:= .F.
Default cSerie		:= STFGetStation("SERIE")	// Serie da esta�ao, acrescentado para cancelamento NF-e

cSerie	:= PadR( cSerie, SL1->(TamSx3("L1_SERIE"))[1] )
cNumDoc := PadR( cNumDoc, SL1->(TamSx3("L1_DOC"))[1] )
cPDV	:= Padr( cPDV, Len(CriaVar("L1_PDV",.F.)),'' )

DbSelectArea("SL1")

If !lDocNf
	SL1->( DbSetOrder(2) )	//L1_FILIAL+L1_SERIE+L1_DOC+L1_PDV
	If SL1->( DbSeek(xFilial("SL1") + cSerie + cNumDoc + cPDV) )
		cNum := SL1->L1_NUM
	Else
		SL1->(DbSetOrder(11)) //L1_FILIAL+L1_SERPED+L1_DOCPED
		If SL1->( DbSeek(xFilial("SL1")+cSerie+cNumDoc))  .AND. AllTrim(SL1->L1_PDV) == Alltrim(cPDV)
				cNum := SL1->L1_NUM
		EndIf                                                                                                                                 
	EndIf
Else
	cSerNf	:= PadR( STFGetStation("SERNFIS"), SL1->(TamSx3("L1_SERPED"))[1] )
	cDocNf	:= PadR( cNumDoc, SL1->(TamSx3("L1_DOCPED"))[1] )

	SL1->(DbSetOrder(11)) //L1_FILIAL+L1_SERPED+L1_DOCPED
	If SL1->( DbSeek(xFilial("SL1")+cSerNf+cDocNf))  .AND. AllTrim(SL1->L1_PDV) == Alltrim(cPDV)
		cNum := SL1->L1_NUM
	EndIf
EndIf


RestArea(aAreaSL1)
RestArea(aArea)

Return cNum


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSFactura
Busca Dados da venda para an�lide se Factura Global

@param   cNumDoc				Numero do documento (L1_DOC)
@param   cPDV					Numero do PDV
@author  Varejo
@version P11.8
@since   29/03/2012
@return  aRet[1]				L1_DOC
@return  aRet[2]				L1_SERIE
@return  aRet[3]				L1_CLIENTE
@return  aRet[4]				L1_LOJA
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSFactura( cNumDoc , cPDV )

Local aRet 		:= {}					// Array de retorno
Local aArea 	:= GetArea()			// Armazena Area corrente
Local aAreaSL1 	:= SL1->(GetArea())	// Armazena Area SL1

Default cNumDoc := ""
Default cPDV  	:= ""

SL1->(DbSetOrder(8))//L1_FILIAL+L1_PDV+L1_DOC
If SL1->(DbSeek(xFilial("SL1") + cPDV + cNumDoc))
	aRet := { SL1->L1_DOC, SL1->L1_SERIE, SL1->L1_CLIENTE, SL1->L1_LOJA }
EndIf

RestArea(aAreaSL1)
RestArea(aArea)
	
Return aRet

 
//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSRefsh
Desposiciona e reposiciona no mesmo registro. Utilizado em caso de registro alterado por outra thread

@param   cTab				Tabela do registro a ser atualizado
@author  Varejo
@version P11.8
@since   29/03/2012
@return  Nil
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSRefsh(cTab)

Local nRecno := 0			// Numero do recno

Default cTab := ""

If !Empty(cTab)
	nRecno := (cTab)->(RecNo())
	(cTab)->(DbSkip())
	(cTab)->(DbGoto(nRecno))
EndIf

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSDeleteSale
Realiza a dele��o do SL1, SL2 e SL4 e SE5

@param   cNumSale				Numero da Venda
@param   lDelChange			Define se deleta SE5 (Troco para localiza��es)
@author  Varejo
@version P11.8
@since   29/03/2012
@return  lRet					Retorna se deletou venda	
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSDeleteSale( cNumSale , lDelChange )

Local lRet			:= .T.						// Retorna se deletou venda
Local aArea 			:= GetArea()				// Armazena Area corrente
Local aAreaSL1 		:= SL1->(GetArea())		// Armazena Area SL1
Local lCancVenda	:= .T.
Local lUseSat	:= STFGetCfg("lUseSAT", .F.) //Utiliza SAT
Local lMobile	:= STFGetCfg("lMobile", .F.) //Utiliza SAT

Default cNumSale  	:= ""
Default lDelChange	:= .F.

If ValType(lMobile) <> "L"
	lMobile := .F.
EndIf

/*
Indica que a venda dever� ser cancelada, por�m no caso de uma NFC-e rejeitada,
ela deve ser subir para a retaguarda como uma venda normal, por isso n�o a cancelamos.
O flag (_STORC = "A") indica que ela dever� ser inutilizada pelo LjGrvBatch, 
por esse motivo ela n�o gera SLI e os campos ficarao dessa forma: L1_SITUA = 00 e L1_STORC = "A"	 
*/
If !lMobile
	lCancVenda := !( STBGetNFCE() .AND. STBCSIsProgressSale() .AND. !lUseSAT .AND. STDGPBasket("SL1","L1_SITUA") == '65' )
ElseIf STBGetNFCE() .AND. !STBCSIsProgressSale() .And. !lUseSat
	//nao cancela para a venda subir e cancelar no gravabatch (piloto PDV Mobile)
	lCancVenda := .F.
EndIf

DbSelectArea("SL1")
DbSetOrder(1) //L1_FILIAL+L1_NUM
If DbSeek( xFilial("SL1") + cNumSale )
	If !RecLock( "SL1", .F. )
		lRet := .F.
	EndIf
Else
	lRet := .F.
EndIf

If lRet
	DbSelectArea("SL4")
	DbSetOrder(1)//L4_FILIAL+L4_NUM+L4_ORIGEM
	DbSeek(xFilial("SL4")+cNumSale)
	While ( ( SL4->L4_FILIAL + SL4->L4_NUM ) == ( xFilial("SL1") + cNumSale ) ) .AND. !EOF()
		If lCancVenda
			RecLock("SL4",.F.)
			SL4->( DbDelete() )
			MsUnlock()
		EndIf
		SL4->( DbSkip() )
	EndDo
	
	DbSelectArea("SL2")
	DbSetOrder(1)//L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
	DbSeek(xFilial("SL2") + cNumSale)
	While ( (SL2->L2_FILIAL + SL2->L2_NUM) == (xFilial("SL1") + cNumSale) ) .AND. !EOF()
		If lCancVenda
			RecLock("SL2", .F.)
			SL2->( DbDelete() )
			MsUnLock()
		EndIf			
		SL2->( DbSkip() )
	EndDo

	If lDelChange
		DbSelectArea("SE5")
		DbSetOrder(2)//E5_FILIAL+E5_TIPODOC+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DtoS(E5_DATA)+E5_CLIFOR+E5_LOJA+E5_SEQ
		If DbSeek(xFilial("SE5") + "VL" + SL1->L1_SERIE + SL1->L1_DOC)	    	
			While !EOF() .AND.  ( xFilial("SE5") + "VL" + SE5->E5_PREFIXO + SE5->E5_NUMERO ) == ( xFilial("SE5") + "VL" + SL1->L1_SERIE + SL1->L1_DOC )
				If (Empty(SE5->E5_TIPO) 					.AND. ;
					SE5->E5_MOEDA == "TC" 				.AND. ;
					SE5->E5_RECPAG == "P" 				.AND. ;
				   (SE5->E5_CLIFOR == SL1->L1_CLIENTE)	.AND. ;
				   (SE5->E5_LOJA == SL1->L1_LOJA)		)
				   				   
					RecLock("SE5",.F.)
					DbDelete()
					MsUnLock()					
				EndIf
				DbSkip()
			EndDo
		EndIf
	EndIf

	DbSelectArea("SL1")
	RecLock("SL1", .F.)
	If lCancVenda
		Replace SL1->L1_SITUA with "07"
		SL1->( DbDelete() )
	Else			
		Replace SL1->L1_SITUA with "00"
		Replace SL1->L1_STORC with "A"			
	EndIf
	SL1->( MsUnLock() )

EndIf

RestArea(aAreaSL1)
RestArea(aArea)

Return  lRet


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSGiftVoucher
Busca Vale Presentes a serem estornados

@param   cNumSale				Numero da Venda
@author  Varejo
@version P11.8
@since   29/03/2012
@return  aRet				Retorna info do vale presente utilizado

@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSGiftVoucher( cNumsale )

Local aRet			:= {}					// Retorna VPs a serem estornados
Local aArea 			:= GetArea()			// Armazena Area corrente

Default cNumSale  	:= ""

ParamType 0 Var  cNumSale 	As Character	 Default ""

// Busca se foi utilizado VP na venda como forma de pagamento
DbSelectArea("SL4")
DbSetOrder(1)//L4_FILIAL+L4_NUM+L4_ORIGEM
If DbSeek(xFilial("SL4") + cNumsale) 
	While (!SL4->(EOF())) .AND. (SL4->L4_FILIAL + SL4->L4_NUM == xFilial("SL4") + cNumsale)
		If AllTrim(SL4->L4_FORMA) == "VP"	
   			AADD(aRet,{AllTrim(SL4->L4_CODVP),"2"})
		EndIf
		DbSkip()
	EndDo
EndIf

// Busca se foi vendido algum VP na venda
DbSelectArea("SL2")
DbSetOrder(1)//L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
If DbSeek(xFilial("SL2") + cNumsale) 
	While !SL2->(Eof()) .AND. (SL2->L2_FILIAL + SL2->L2_NUM == xFilial("SL2") + cNumsale)
		If SL2->(FieldPos("L2_VALEPRE")) > 0 .AND. !Empty(SL2->L2_VALEPRE)
   			AADD(aRet,{AllTrim(SL2->L2_VALEPRE),"1"})
		EndIf
		DbSkip()
	EndDo
EndIf

RestArea(aArea)

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSTotalSale
Busca o total da venda a ser cancelada

@param   cNumSale			Numero da Venda(L1_NUM)
@author  Varejo
@version P11.8
@since   29/03/2012
@return  nTotalSale			Retorna o valor total da venda

@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSTotalSale(cNumSale)

Local nTotalSale		:= 0					// Retorna o valor total da venda
Local aArea 			:= GetArea()			// Armazena Area corrente
Local aAreaSL1 			:= SL1->(GetArea())		// Armazena Area SL1

Default cNumSale  	:= ""

ParamType 0 Var  cNumSale 		As Character	 Default ""

DbSelectArea("SL1")
DbSetOrder(1) //L1_FILIAL+L1_NUM
If SL1->(DbSeek(xFilial("SL1") + cNumSale))
	nTotalSale := SL1->L1_VLRTOT
EndIf

RestArea(aAreaSL1)
RestArea(aArea)

Return nTotalSale


//-------------------------------------------------------------------
/*/{Protheus.doc} STDCSRequestCancel
Busca informa��es para solicita��o de cancelamento

@param   cNumSale		Numero da Venda(L1_NUM)
@param 	  lCancVenc 	Cancela vendas vencidas 
@author  Varejo
@version P11.8
@since   29/03/2012
@return  aRequest			Retorna se realizou a solicita��o de cancelamento
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCSRequestCancel( cNumSale , lCancVenc, cNFisCanc )

Local lRet				:= .T.				// Retorna se realizou a solicita��o de cancelamento
Local aArea				:= GetArea()		// Armazena area corrente
Local aAreaSL1			:= SL1->(GetArea())	// Armazena Area SL
Local aSL1				:= {}				// Array Grava��o SL1
Local cLiMsg 			:= ""				// Msg SLI para cancelamento
Local cStation			:= ""				// SLI_Estacao
Local cPAFMD5				:= ""
Local lFirst			:= .F. 				// Integra com First?
Local cIntegration		:= "DEFAULT" 		// Tipo de Integra��o
Local lConfStatus		:= .F.				// Valida se populou SL1 atualizando STORC
Local cAliasAtu			:= ""				

Default cNumSale  		:= ""
Default lCancVenc   	:= .F.
Default cNFisCanc	:= ""

If Findfunction("STFCfgIntegration")
	cIntegration := STFCfgIntegration()
ElseIf SuperGetMv("MV_LJRMS",,.F.)
	cIntegration := "RMS"	
EndIf

lFirst := cIntegration == "FIRST"

//Por seguranca, valida se SL1 est� posicionado na venda que sera cancelada
If xFilial("SL1") + cNumSale <> SL1->L1_FILIAL + SL1->L1_NUM 
	LjGrvLog( cNumSale,"Venda nao estava posicionada, sera realizado Seek para posicionar. L1_NUM atual:",SL1->L1_NUM )
	
	//armazena o Alias corrente
	cAliasAtu := Alias()

	DbSelectArea("SL1")
	SL1->( DbSetOrder(1) )
	lRet := SL1->( DbSeek(xFilial("SL1") + cNumSale) )
	aAreaSL1 := SL1->( GetArea() )

	//se o Alias for SL1, entao pegamos a Area novamente, pois a restauracao posicionaria ele no registro errado
	If cAliasAtu == "SL1"
		aArea := aClone(aAreaSL1)
	EndIf 

	LjGrvLog( cNumSale,"Apos Seek para posicionar. L1_NUM atual:",SL1->L1_NUM )
EndIf
                 
If lRet
	
	LjGrvLog( cNumSale,"Situacao atual da venda, no momento do cancelamento :",SL1->L1_SITUA )  //Gera LOG
		
	If !lFirst	
		If !Empty(cNFisCanc) //Acrescentado a serie para que o cancelamento ocorra na nota certa
			cLiMsg := SL1->L1_NUMORIG + "|" + SL1->L1_DOC + "|" + SL1->L1_PDV + "|" + cNFisCanc 	
		Else				
			cLiMsg := SL1->L1_NUMORIG + "|" + SL1->L1_DOC + "|" + SL1->L1_PDV + "| |" + SL1->L1_SERIE 	
		EndIf			
	
		LjGrvLog( cNumSale,"Dados armazenados para cancelamento via SLI: ",cLiMsg )  //Gera LOG
		
		cStation := Space(TamSX3("LI_ESTACAO")[1])
		lRet := STFSLICreate(	cStation	, "CAN" , cLiMsg	, "NOVO" )
		
		//Se registro em uso, rotina STFSaveTab tem controle padrao para aguardar liberacao. Nao compromete o processo, quando cancelamento sobe, tambem atualiza o STORC
		//Cenario pouco provavel: Registro em uso nesse momento + retaguarda fora + fechamento de caixa. Impacto: Venda ser� considerada no movimento do caixa.
		aSL1 := {{"L1_STORC" , "C"}} //Atualiza STORC, Situa sera atualizado no STDUPDATA quando subir o cancelamento
		lConfStatus := STFSaveTab( "SL1" , aSL1 )	  
		LjGrvLog( cNumSale,"Atualiza��o Statua(L1_STORC):",lConfStatus )  //Gera LOG
					
	Else
		LjGrvLog( cNumSale,"Habilitado Integracao com FIRST(MV_LJRETIN)",cLiMsg )  //Gera LOG
	
		aSL1 := {{"L1_SITUA" , "C0"}}	//C0 - Solicitado o cancelamento de cupom enviado
		STFSaveTab( "SL1" , aSL1 )
	
		//Atualiza MD5(hash) utilizado no PAF-ECF para identificar alteracao de registro 
		If STBIsPaf() .AND. SL1->(FieldPos("L1_PAFMD5")) > 0
			cPAFMD5 := STBPAFMD5("SL1")
			aSL1 := {{"L1_PAFMD5",	cPAFMD5}}
			lRet := STFSaveTab( "SL1" , aSL1 )
		EndIf
		EndIf
		
	EndIf

LjGrvLog( cNumSale,"Retorno do cancelamento ",lRet )  //Gera LOG

RestArea(aAreaSL1)
RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*{Protheus.doc} STDCSShopCard
Tratamento para Cancelamento Shop Card

@param   cNumSale					Numero da Venda
@author  Varejo
@version P11.8
@since   29/03/2012
@return
@obs     
@sample
/*/
//------------------------------------------------------------------- 
Function STDCSShopCard( cNumSale )
Local aArea				:= GetArea()
Local aParam			:= {}			// 

Default cNumSale  		:= ""

ParamType 0 Var  cNumSale 	As Character	 Default ""  

If lLjcFid .AND. !Empty(cNumSale)

	//��������������������������������Ŀ
	//�Release 11.5 - Cart�o Fidelidade�
	//����������������������������������
	If lLjcFid			
		DbSelectArea("SL1")
		SL1->(DbSetOrder())
		SL1->(DbSeek(xFilial("SL1")+cNumSale))
				
		DbSelectArea("SL2")
		SL2->(DbSetOrder(1))
		If DbSeek(xFilial("SL2")+cNumSale) 
			While !SL2->(Eof()) .AND. (SL2->L2_FILIAL+SL2->L2_NUM == xFilial("SL2")+cNumSale)				
				If STBAvalShopCard(SL2->L2_PRODUTO) .AND. !Empty(SL2->L2_NUMCFID) .AND. !Empty(SL2->L2_DTSDFID) .AND. (SL2->L2_VLRCFID > 0)	
					aParam := {SL2->L2_NUMCFID,SL1->L1_DOC,SL1->L1_SERIE,SL1->L1_LOJA,"4"}			 	
		 	   		//Cancelamento de venda de recarga de cartao fidelidade
		 	   		If STBRemoteExecute("CA280ESLD" ,aParam, NIL,.T.	,@uResult)
		 	   			If ValType(uResult) == "L" .AND. !uResult
		 	   				aSL1 := {{"L1_SITUA",	"00"}}				// Retira a solicita��o de cancelamento
							STFSaveTab( "SL1" , aSL1 )					// Salva na tabela a alteracao
		 	   			EndIf
		 	   		Else
		 	   			// TO DO: Gerar contingencia
		 	   		EndIf
		 			Exit
		 		EndIf
		 		SL2->(DbSkip())
		 	End
		 EndIf
		 
 		//Cancelamento de venda com forma de pagamamento FID - Cartao fidelidade
		DbSelectArea("SL4")				 		
		DbSetOrder (1)
		If DbSeek (xFilial("SL4")+cNumSale)
			While SL4->(!Eof()) .AND. SL4->L4_NUM == cNumSale
				If Alltrim(SL4->L4_FORMA) == "FID"							
					//���������������������������������������������������� �
					//�Se a venda foi paga com cartao fidelidade, entao o �
					//�movimento de saida sera estornado da tabela MBN    �
					//�e o valor devolvido ao respectivo saldo.           �
					//�����������������������������������������������������
					aParam := {SL2->L2_NUMCFID,SL1->L1_DOC,SL1->L1_SERIE,SL1->L1_LOJA,"3"}
					If STBRemoteExecute("CA280ESLD" ,aParam, NIL,.T.	,@uResult)
						If ValType(uResult) == "L" .AND. !uResult
							aSL1 := {{"L1_SITUA",	"00"}}				// Retira a solicita��o de cancelamento
							STFSaveTab( "SL1" , aSL1 )					// Salva na tabela a alteracao
						EndIf
					Else
						// TO DO: Gerar contingencia
					EndIf					
 					Exit
 				EndIf 
 				SL4->(DbSkip())							
			End						
		EndIf					
	EndIf
EndIf	

RestArea(aArea)		

Return 

//-------------------------------------------------------------------
/*{Protheus.doc} STDLogCanc
Apos cancelamento do cupom grava SLX
A gravacao da SLX no Totvs Pdv foi feita devido a integracao com a RMS

@param   
@author  Varejo
@version P11.8
@since   29/07/2014
@return
@obs     
@sample
/*/
//------------------------------------------------------------------- 
Function STDLogCanc(lIsItem	,	nItem	, cSuper	, lImportSale,;
 					lDocNf	,	lCancCF	)
Local lRet			:= .T. 						//Variavel de retorno
Local lLX_SITTRI	:= SLX->(ColumnPos("LX_SITTRIB")) > 0
Local lSTBIsPAF		:= STBIsPAF()
Local lVendaRecup	:= ExistFunc("STWRSVerRe") .And. STWRSVerRe()
Local cPdv			:= STFGetStat( "PDV" )		//Codigo da estacao
Local cCaixa		:= xNumCaixa()				//Codigo do caixa
Local cSerie		:= STFGetStat( "Serie" )	//Serie
Local aInfCan		:= STIGetCancel()			//Informacoes do cancelamento
Local aArea			:= {}
Local oModelCesta	:= STDGPBModel()			//Model da cesta
Local cSupervisor	:= "" 						// Supervisor
Local nI			:= 0
Local cSerNf		:= ""						//Armazena a serie nao fiscal
Local cAux			:= ""
Local cItem			:= ""
Local cDoc			:= ""
Local cFilSLX		:= ""
Local cLXItem		:= ""
Local lFoundSLX		:= .F.
Local cSerNFe		:= PadR( GetNewPar("MV_LOJANF",""), len(cSerie) ) //Ajuste para NFe

Default lIsItem 	:= .F.
Default nItem		:= 0 
Default cSuper		:= ""
Default lImportSale := .F.						//Controla se houve importa��o de or�amento.
Default lDocNf		:= .F.
Default lCancCF		:= .F.

If !(lImportSale .AND. lSTBIsPAF)				//Se for or�amento importado e ambiente PAF, n�o pede a autoriza��o novamente.
	If Empty(cSuper)
		cSupervisor 	:= STFProFile(7)[2] 	// Supervisor, aqui chamara a tela de supervisor
	Else
		cSupervisor 	:= cSuper 				// Supervisor vindo da rotina STWItemCancel.Protecao para nao chamar a tela supervisor duas vezes
	EndIf	
Endif

Aadd(aArea,GetArea())
DbSelectArea("SLX")
Aadd(aArea,SLX->(GetArea()))
Aadd(aArea,SL2->(GetArea()))
Aadd(aArea,SL1->(GetArea()))

//Cancelamento Item
If lIsItem
	oModelCesta := oModelCesta:GetModel("SL2DETAIL")
	oModelCesta:GoLine(nItem)
	
	SLX->(RecLock("SLX", .T.))
	REPLACE SLX->LX_FILIAL  WITH xFilial("SLX")
	REPLACE SLX->LX_PDV     WITH cPDV
	REPLACE SLX->LX_OPERADO WITH cCaixa 
	REPLACE SLX->LX_DTMOVTO WITH dDatabase
	REPLACE SLX->LX_HORA    WITH Time()
		
	//Tratamento para Totvs PDV com NFe					 			
	REPLACE SLX->LX_SERIE   WITH IIF(!lDocNf,;
									iif( cSerie <> cSerNFe,cSerie, cSerNFe ),;
									cSerNf)
							 			
	REPLACE SLX->LX_CUPOM   WITH PadL( AllTrim(SL1->L1_DOC) , LjxECFCupT() , "0" )
	REPLACE SLX->LX_ITEM    WITH oModelCesta:GetValue("L2_ITEM")
	REPLACE SLX->LX_TPCANC  WITH "I"
	
	If SLX->(ColumnPos("LX_VRUNIT")) > 0
		REPLACE SLX->LX_VRUNIT  WITH oModelCesta:GetValue("L2_VRUNIT")
	EndIf
	
	REPLACE SLX->LX_VALOR   WITH oModelCesta:GetValue("L2_VLRITEM") + oModelCesta:GetValue("L2_VALDESC")
	REPLACE SLX->LX_PRODUTO WITH oModelCesta:GetValue("L2_PRODUTO")
	REPLACE SLX->LX_QTDE    WITH oModelCesta:GetValue("L2_QUANT")
	REPLACE SLX->LX_SUPERVI WITH cSupervisor
	REPLACE SLX->LX_SITUA 	WITH "00"

	If SLX->(ColumnPos("LX_PRCTAB")) > 0
		REPLACE SLX->LX_PRCTAB 	WITH oModelCesta:GetValue("L2_PRCTAB")
	EndIf
	
	If lSTBIsPAF
		REPLACE SLX->LX_CONTDOC WITH STBRetCoD()
	EndIf
	    		
	cAux := AllTrim(oModelCesta:GetValue("L2_SITTRIB"))
	If !Empty(cAux) .And. Substr(cAux,1,1) == "T" //somente grava se for aliquota de ICMS
		REPLACE SLX->LX_ALIQICM WITH Val(Substr(cAux,2,Len(cAux)))/100
	EndIf
	
	If lLX_SITTRI
		REPLACE SLX->LX_SITTRIB WITH cAux
	EndIf
	
	REPLACE SLX->LX_DESCON 	WITH oModelCesta:GetValue("L2_VALDESC")

	SLX->(MsUnlock())

	// Seto o numero do registro da SLX no item da SL2
	If oModelCesta:HasField("L2_REGSLX")
		STDSPBasket( "SL2" , "L2_REGSLX" ,  SLX->(Recno()))
	Endif	
Else //Cancelamento Cupom
	
	oModelCesta := oModelCesta:GetModel("SL2DETAIL")
	//Se houverem itens na cesta, cancelamento de cupom aberto
	If oModelCesta:Length() > 0 .And. !Empty(oModelCesta:GetValue("L2_PRODUTO"))
		
		//Quando PAF-ECF e cancelamento de venda no meio da venda
		//esta duplicando caso o registro ja tenha sido cancelado
		If lCancCF
			//Como abaixo ser�o criados todos os itens de novo, eu apago caso j� exista algum
			SLX->(DbSetOrder( 1 )) 	//LX_FILIAL+LX_PDV+LX_CUPOM+LX_SERIE+LX_ITEM
			
			cAux := xFilial("SLX") + PadR(cPDV, TamSX3("LX_PDV")[1]) + ;
						PadR(AllTrim(SL1->L1_DOC), TamSX3("LX_CUPOM")[1]) + PadR(cSerie, TamSX3("LX_SERIE")[1])
	
			If SLX->(DbSeek(cAux))
				While !SLX->(Eof()) .And. SLX->(LX_FILIAL+LX_PDV+LX_CUPOM+LX_SERIE) == cAux
				 	IF !(AllTrim(SLX->LX_ITEM) == "00")
				 		RecLock("SLX",.F.)
						SLX->(DbDelete())
						SLX->(MsUnlocK())
				 	EndIf
				 	SLX->(DbSkip())
				End
			EndIf
		EndIf
		
		For nI := 1 To oModelCesta:Length()
			oModelCesta:GoLine(nI)																 									
			
			cItem := oModelCesta:GetValue("L2_ITEM")
			cDoc  := Right(SL1->L1_DOC, TamSX3("LX_CUPOM")[1])
			
			SLX->(DbSetOrder(1)) //LX_FILIAL, LX_PDV, LX_CUPOM, LX_SERIE, LX_ITEM, LX_HORA
			
			/*
			Valido se � uma venda recuperada pois se efetuo um cancelamento do ultimo
			cupom e houve itens cancelados eu devo considerar todos os itens novos pois
			h� um reordenamento do numero do item
			*/
			If lVendaRecup .And. SLX->(DbSeek(xFilial("SLX") + cPDV + cDoc + cSerie + cItem))
				//-- Essa situa��o � um item cancelado de um cupom cancelado que existe na SL2 e SLX
				If SLX->LX_TPCANC == "I"
					LjGrvLog( NIL, "Item [" + AllTrim(cItem) + "] j� foi gravado na SLX [Num. Doc:" + AllTrim(cDoc) + ;
							"] com cancelamento de item - n�o sera gravado no cancelamento total")
				Else
					SLX->(RecLock("SLX", .F.))
					REPLACE SLX->LX_TPCANC  WITH "A" //Insiro "A" para que n�o seja acumulado duas vezes no XML do PAF
					SLX->(MsUnlock())
				EndIf
			Else
				SLX->(RecLock("SLX", .T.))
				REPLACE SLX->LX_FILIAL  WITH xFilial("SLX")
				REPLACE SLX->LX_PDV     WITH cPDV
				REPLACE SLX->LX_OPERADO WITH cCaixa 
				REPLACE SLX->LX_DTMOVTO WITH dDatabase
				REPLACE SLX->LX_HORA    WITH Time()
				
				//Tratamento para Totvs PDV com NFe
				REPLACE SLX->LX_SERIE   WITH IIF(!lDocNf,;
												iif( cSerie <> cSerNFe,cSerie, GetMV("MV_LOJANF") ),;
												cSerNf)
	
				REPLACE SLX->LX_CUPOM   WITH cDoc	
				REPLACE SLX->LX_ITEM    WITH cItem
				REPLACE SLX->LX_TPCANC  WITH "A"	
				REPLACE SLX->LX_VRUNIT  WITH oModelCesta:GetValue("L2_VRUNIT")
				REPLACE SLX->LX_VALOR   WITH oModelCesta:GetValue("L2_VLRITEM") + oModelCesta:GetValue("L2_VALDESC")
				REPLACE SLX->LX_PRODUTO WITH oModelCesta:GetValue("L2_PRODUTO")
				REPLACE SLX->LX_QTDE    WITH oModelCesta:GetValue("L2_QUANT")
				REPLACE SLX->LX_SUPERVI WITH cSupervisor
				REPLACE SLX->LX_SITUA WITH "00"
				If SLX->(ColumnPos("LX_PRCTAB")) > 0
					REPLACE SLX->LX_PRCTAB 	WITH oModelCesta:GetValue("L2_PRCTAB")
				EndIf
				
				If lSTBIsPAF
		    		REPLACE SLX->LX_CONTDOC WITH STBRetCoD()
		    	EndIf
				
				cAux := AllTrim(oModelCesta:GetValue("L2_SITTRIB"))
				If !Empty(cAux) .And. Substr(cAux,1,1) == "T" //somente grava se for aliquota de ICMS
					REPLACE SLX->LX_ALIQICM WITH Val(Substr(cAux,2,Len(cAux)))/100
				EndIf
				
				If lLX_SITTRI
					REPLACE SLX->LX_SITTRIB WITH cAux
				EndIf
				
				REPLACE SLX->LX_DESCON 	WITH oModelCesta:GetValue("L2_VALDESC")
	
				SLX->(MsUnlock())
			EndIf
		Next nI
	Else
		//Cancelamento de cupom encerrado
		SL2->(dbSetOrder(1)) //L2_FILIAL+L2_NUM+L2_ITEM+L2_PRODUTO
		SL2->(dbSeek(SL1->L1_FILIAL + SL1->L1_NUM))

		If lDocNf
			cSerNf := PadR( STFGetStation("SERNFIS"), SL1->(TamSx3("L1_SERPED"))[1] )
		EndIf

		cFilSLX := xFilial("SLX")
		While SL2->(!EOF()) .And. SL2->L2_FILIAL == SL1->L1_FILIAL .And. SL2->L2_NUM == SL1->L1_NUM

			SLX->(DbSetOrder(1)) //LX_FILIAL, LX_PDV, LX_CUPOM, LX_SERIE, LX_ITEM, LX_HORA
			/*
			Valido se � uma venda recuperada pois se efetuo um cancelamento do ultimo
			cupom e houve itens cancelados eu devo considerar todos os itens novos pois
			h� um reordenamento do numero do item
			*/
			If lVendaRecup .And. SLX->(DbSeek(cFilSLX + SL2->L2_PDV + SL2->L2_DOC + SL2->L2_SERIE + SL2->L2_ITEM))
				//-- Essa situa��o � um item cancelado de um cupom cancelado que existe na SL2 e SLX
				If SLX->LX_TPCANC == "I"
					LjGrvLog( NIL, "Item [" + AllTrim(SL2->L2_ITEM) + "] j� foi gravado na SLX [Num. Doc:" + AllTrim(SL2->L2_DOC) +;
					 			"] com cancelamento de item - n�o sera gravado no cancelamento total")
				Else
					SLX->(RecLock("SLX", .F.))
					REPLACE SLX->LX_TPCANC  WITH "A" //Insiro "A" para que n�o seja acumulado duas vezes no XML do PAF
					SLX->(MsUnlock())
				EndIf
			Else
				
				/*Ajuste para o SQLite:
				- quando se tem um deletado e um item vendido o L2_ITEM muda e
				com isso ao cancelar a venda em cupom finalizado deve-se alterar
				o LX_ITEM para evitar erro de chave duplicada aqui na SLX*/
				nI := 0
				cLXItem := ""
				lFoundSLX := SLX->(DbSeek(cFilSLX + SL2->L2_PDV + SL2->L2_DOC + SL2->L2_SERIE + SL2->L2_ITEM)) 
				While lFoundSLX
					nI += 1
			
					If Len(AllTrim(Str(nI))) == 1
						cLXItem := '0' + AllTrim(Str(nI))
					Else
						cLXItem := AllTrim(Str(nI))
					EndIf			
					
					If Val(cLXItem) > 99
						cLXItem := STBPegaIT(Val(cLXItem))
					EndIf
					
					cLXItem := AllTrim(cLXItem)
					lFoundSLX := SLX->(DbSeek(cFilSLX + SL2->L2_PDV + SL2->L2_DOC + SL2->L2_SERIE + PadR(cLXItem,TamSX3("L2_ITEM")[1])))
				End
				
				SLX->(RecLock("SLX", .T.))
				REPLACE SLX->LX_FILIAL  WITH xFilial("SLX")
				REPLACE SLX->LX_PDV     WITH cPDV
				REPLACE SLX->LX_OPERADO WITH cCaixa 
				REPLACE SLX->LX_DTMOVTO WITH dDatabase
				REPLACE SLX->LX_HORA    WITH Time()
				REPLACE SLX->LX_SERIE   WITH IIF(!lDocNf, cSerie, cSerNf)
				REPLACE SLX->LX_CUPOM   WITH IIF(!lDocNf, Right(SL1->L1_DOC, TamSX3("LX_CUPOM")[1]), Right(STDGetDocP(), TamSX3("LX_CUPOM")[1]) ) 	
				REPLACE SLX->LX_ITEM    WITH IIF(Empty(cLXItem),SL2->L2_ITEM,cLXItem)
				REPLACE SLX->LX_TPCANC  WITH "A"	
				REPLACE SLX->LX_VRUNIT  WITH SL2->L2_VRUNIT
				REPLACE SLX->LX_VALOR   WITH SL2->L2_VLRITEM + SL2->L2_VALDESC
				REPLACE SLX->LX_PRODUTO WITH SL2->L2_PRODUTO
				REPLACE SLX->LX_QTDE    WITH SL2->L2_QUANT
				
				If ValType(aInfCan) == "A" .AND. Len(aInfCan) > 0
					REPLACE SLX->LX_SUPERVI WITH aInfCan[3]
				EndIf
				
				REPLACE SLX->LX_SITUA WITH "00"
				
				If SLX->(ColumnPos("LX_PRCTAB")) > 0
					REPLACE SLX->LX_PRCTAB 	WITH SL2->L2_PRCTAB
				EndIf
				
				If lSTBIsPAF
		    		REPLACE SLX->LX_CONTDOC WITH STBRetCoD()
		    	EndIf
		    	
				cAux := AllTrim(SL2->L2_SITTRIB)
				If !Empty(cAux) .And. Substr(cAux,1,1) == "T" //somente grava se for aliquota de ICMS
					REPLACE SLX->LX_ALIQICM WITH Val(Substr(cAux,2,Len(cAux)))/100
				EndIf
				
				If lLX_SITTRI
					REPLACE SLX->LX_SITTRIB WITH cAux
				EndIf
				
				REPLACE SLX->LX_DESCON 	WITH SL2->L2_VALDESC
				
				SLX->(MsUnlock())
			EndIf
			
			SL2->(dbSkip())
		EndDo
	EndIf		
EndIf

For nI:=1 to Len(aArea)
	RestArea(aArea[nI])
Next nI

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STDSitDoc
Retorna o L1_SITUA a partir do L1_SERIE + L1_DOC + L1_PDV

@param   cSerie			Numero de S�rie
@param   cNumDoc		Numero do Documento Fiscal
@param   cPDV			Numero do PDV
@author  Varejo
@version P11.8
@since   29/03/2012
@return  cSitua			Campo L1_SITUA da venda
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDSitDoc( cNumSerie, cNumDoc, cPDV, lDocNf )

Local aArea 		:= GetArea()			// Armazena Area corrente
Local aAreaSL1 		:= SL1->(GetArea())		// Armazena Area SL1
Local aRet			:= {}
Local cDocNf		:= "" //Documento nao fiscal
Local cSerNf		:= "" //Serie nao fiscal

Default cNumSerie := ""
Default cNumDoc := ""
Default cPDV := ""
Default lDocNf := .F.

If !lDocNf
	cNumSerie	:= PadR( cNumSerie	, SL1->(TamSx3("L1_SERIE"))[1] )
	cNumDoc	  	:= PadR( cNumDoc	, SL1->(TamSx3("L1_DOC")  )[1] )
	cPDV		:= Padr( cPDV		, SL1->(TamSx3("L1_PDV")  )[1] )
	//o ultimo parametro indica se deve usar o valor default (penultimo parametro) caso nao encontre o registro (nao documento no TDN)
	aRet := GetAdvFVal(	"SL1", {"L1_SITUA", "L1_STORC"}, (xFilial("SL1")+cNumSerie+cNumDoc+cPDV), 2, {"404", ""}, .T. )
Else
	cDocNf := PadR( cNumDoc, SL1->(TamSx3("L1_DOCPED"))[1] )
	cSerNf := PadR( STFGetStation("SERNFIS"), SL1->(TamSx3("L1_SERPED"))[1] )
	aRet := GetAdvFVal(	"SL1", {"L1_SITUA", "L1_STORC"}, (xFilial("SL1")+cSerNf+cDocNf), 11, {"404", ""}, .T. )
EndIf

RestArea(aAreaSL1)
RestArea(aArea)

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STDCancTef
Verifica se existe transa��o TEF ou em POS para cancelar a venda

@param   cLastNum		Numero do or�amento
@author  Varejo
@version P11.8
@since   22/10/2015
@return  lRet			.T.->Venda com TEF/POS/ .F.->Venda sem TEF/POS
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDCancTef(cLastNum)
Local lRet		:= .F.
Local aSl4Ord	:= {}

If AllTrim(STDCSLastSale("L1_SITUA")) <> "07" 
	
	aSl4Ord	:= SL4->(GetArea())
	
	SL4->(DbSetOrder(1))
	If SL4->(DbSeek(xFilial("SL4") + cLastNum))			
		While !SL4->(Eof()) .AND. AllTrim(SL4->L4_FILIAL)+AllTrim(SL4->L4_NUM) == AllTrim(xFilial("SL4"))+AllTrim(cLastNum)
			If (Alltrim(SL4->L4_FORMA) == "CC" .OR. Alltrim(SL4->L4_FORMA) == "CD")
				lRet := .T.
				Exit
			Endif
			SL4->(DbSkip())
		Enddo
	EndIf

	RestArea(aSl4Ord)
	
EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STDLastSat
Busca Numero do Ultimo Or�amento SAT

@param   cRetField - Nome do campo no dicionario de dados da tabela SL1
@author  Varejo
@version P11.8
@since   02/12/2015
@return  cNumSale - Retorna o Numero ou outro campo da tabela SL1 da Ultima Venda
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDLastSat(cRetField, cRetSerie)

Local cNumSale		:= ""			// Retorna o Numero da Ultima Venda ou o campo do parametro
Default cRetField	:= "L1_NUM"
Default cRetSerie	:= Nil 			//Tratamento por conta da NFe

// Posiciona o SL1 na ultima venda
DbSelectArea("SL1")
SL1->( DbSetOrder(1) ) //L1_FILIAL+L1_NUM

If SL1->( DbSeek(xFilial("SL1")) ) 
	SL1->(DbGoBottom()) 

	//Macrosubstituicao consome muito recurso
	cNumSale := SL1->( FieldGet( ColumnPos(cRetField) ) )

	//Tratamento por conta da NFe
	if cRetSerie <> Nil
		cRetSerie := SL1->L1_SERIE
	endif
EndIf

Return cNumSale

/*/{Protheus.doc} STDDelPay
Reseta e/ou apaga os pagamentos ja realizados no BD
@type		function
@param		cNumSale, caracter, numero do orcamento
@author  	Varejo
@version 	P12
@since   	14/10/2016
@return  	logico, os pagamentos foram resetados/excluidos da BD
/*/
Function STDDelPay(cNumSale)

Local cL4Origem	:= Space( TamSX3("L4_ORIGEM")[1] )
Local lRet		:= .F.				//indica se excluiu algum pagamento
Local lExcNCC	:= .F.				//indica se zerou o campo L1_CREDITO (NCC)
Local lExcSL4	:= .F.				//indica se excluiu algum pagamento presente na SL4
Local aSL1Area	:= SL1->(GetArea())	//armazena a area da SL1
Local aSL4Area	:= SL4->(GetArea())	//armazena a area da SL4
Local aArea		:= GetArea()		//armazena a area corrente

Default cNumSale := "" 

DbSelectArea("SL1")
SL1->( DbSetOrder(1) )	//L4_FILIAL + L4_NUM
If SL1->( DbSeek(xFilial("SL1") + cNumSale) )
	If SL1->L1_CREDITO > 0
		RecLock("SL1",.F.)
		Replace SL1->L1_CREDITO with 0
		SL1->( MsUnlock() )
		
		lExcNCC := .T.
	EndIf
EndIf

DbSelectArea("SL4")
SL4->( DbSetOrder(1) )	//L4_FILIAL + L4_NUM + L4_ORIGEM
lExcSL4 := SL4->( DbSeek(xFilial("SL4") + cNumSale + cL4Origem) ) 
If lExcSL4
	While SL4->( !EoF() ) .AND. xFilial("SL4") + cNumSale + cL4Origem == SL4->L4_FILIAL + SL4->L4_NUM + SL4->L4_ORIGEM
		RecLock("SL4",.F.)
		SL4->( DbDelete() )
		SL4->( MsUnlock() )

		SL4->( DbSkip() )
	End
EndIf

RestArea(aSL1Area)
RestArea(aSL4Area)
RestArea(aArea)

//se excluiu a NCC ou os pagamentos da SL4
If lExcNCC .OR. lExcSL4
	lRet := .T.
Else
	LJGrvLog(cNumSale, "Nenhum pagamento (L1_CREDITO ou SL4) foi resetado")
EndIf

Return lRet

//-------------------------------------------------------------------
/*{Protheus.doc} STDAjstSLX
Fun��o responsavel por ajustar o campo LX_CUPOM para os casos de
item cancelado ou cupom cancelado em ambiente que utilize impressora
nao-fiscal, no caso de NFC-e.

@param   
@author  Varejo
@version P12.17
@since   05/03/2018
@return
@obs     
@sample
/*/
//------------------------------------------------------------------- 
Function STDAjstSLX(cNumDOC)
Local cPdv		:= STFGetStat( "PDV" )		//Codigo da estacao
Local cSerie	:= STFGetStat( "Serie" )	//Serie
Local nX 		:= 0
Local nTamCupom := TamSX3("LX_CUPOM")[1]
Local nTamPdv 	:= TamSX3("LX_PDV")[1]
Local aSlxArea  := SLX->(GetArea()) //armazena a area da tabela SLX
Local aArea		:= GetArea()		//armazena a area corrente
Local oModelCesta	:= STDGPBModel()			//Model da cesta

Default cNumDOC := ""

If !Empty(cNumDOC) .And. AllTrim(Upper(cNumDOC)) != "NFCE"

	oModelCesta := oModelCesta:GetModel("SL2DETAIL")
		
	For nX := 1 To oModelCesta:Length() 
		oModelCesta:GoLine(nX)																 									

		If oModelCesta:HasField("L2_REGSLX")
			If oModelCesta:GetValue('L2_REGSLX') > 0
				SLX->(DbGoTo(oModelCesta:GetValue('L2_REGSLX'))) //Posiciona no Registro a ser atualizado
		
				If AllTrim(Upper(SLX->LX_CUPOM)) == "NFCE"
					RecLock("SLX", .F.)
					SLX->LX_CUPOM := cNumDOC
					SLX->(MsUnLock())
				EndIf
			Endif	
		Endif
	Next nX

Else
	//Se nao foi passado o numero do DOC, entao os registros temporarios na SLX devem ser excluidos (Cujo DOC = "NFCe")
	DbSelectArea("SLX")
	SLX->( DbSetOrder(1) ) //LX_FILIAL+LX_PDV+LX_CUPOM+LX_SERIE+LX_ITEM+LX_HORA
	While SLX->( DbSeek( xFilial("SLX")+PadR(cPdv,nTamPdv)+PadR("NFCE",nTamCupom)+cSerie ) )
		RecLock("SLX", .F.)
		SLX->(DbDelete())
		SLX->(MsUnLock())
	End
EndIf

RestArea(aSLxArea)
RestArea(aArea)

Return

//-------------------------------------------------------------------
/*{Protheus.doc} STDGetDocP()
Retorna o numero do Doc Ped caso seja uma venda de cupom nao fiscal (venda entrega/vale presente)

@param   
@author  Bruno Almeida
@version P12
@since   17/06/2019
@return  cDocPed -> Retorna o Doc Ped de uma venda nao fiscal
/*/
//------------------------------------------------------------------- 
Function STDGetDocP()
Return cDocPed
