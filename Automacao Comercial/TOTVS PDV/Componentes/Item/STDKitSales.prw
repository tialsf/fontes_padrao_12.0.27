#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"

//--------------------------------------------------------
/*/{Protheus.doc} STDKitSales
Gera a lista de c�digos de produtos que fazem parte do Produto Kit passado por parametro.
@param	 cCodProdKit	  Codigo do produto que sera analisado para ofertar outros
@author  Varejo
@version P11.8
@since   25/07/2012
@return	 Vetor de codigos de produtos a serem sugeridos
@obs     
@sample
/*/
//--------------------------------------------------------
Function STDKitSales(cCodProdKit)

Local aArea			:= GetArea() 	// Gurada area
Local aRetorno		:= {}			// Retorno da funcao, vetor de codigos de produtos a serem sugeridos
Local nDesconto		:= 0			// Caso o desconto esteja preenchida no cabecalho, esta variavel sera alimentada.

Default cCodProdKit	:= ""

ParamType 0 Var cCodProdKit As Character Default ""  
                    

DbSelectArea("MEU")
MEU->(DbSetOrder(1)) // MEU_FILIAL+MEU_CODIGO 
If MEU->(DbSeek(xFilial("MEU") + PadR(cCodProdKit,TamSX3("MEU_CODIGO")[1])))
	If !Empty(MEU->MEU_DESCNT) // Caso o desconto esteja preenchido no cabecalho, esse desconto sera utilizado.
		nDesconto := MEU->MEU_DESCNT
	EndIf
EndIf

DbSelectArea("MEV")
MEV->(DbSetOrder(1)) // MEV_FILIAL+MEV_CODKIT+MEV_PRODUT 

If MEV->(DbSeek(xFilial("MEV") + PadR(cCodProdKit,TamSX3("MEV_CODKIT")[1])))
	While !MEV->(Eof()) .AND. (AllTrim(MEV->MEV_CODKIT) == AllTrim(cCodProdKit))
		AAdd(aRetorno, {MEV->MEV_PRODUT,IIF(!Empty(nDesconto),nDesconto,MEV->MEV_DESCNT),MEV->MEV_QTD})
		MEV->(DbSkip())
	End Do
EndIf

RestArea(aArea)

Return aRetorno
