#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"


//--------------------------------------------------------------------
/*/{Protheus.doc} STWChkTef
Verifica em qual qual o tipo de transa��o deve ser executada

@param   	cType - Tipo de transacao
@author  Varejo
@version P11.8
@since   	20/02/2013
@return  	lRet - Retorna se o Tipo esta configurado
@obs     
@sample
/*/
//--------------------------------------------------------------------
Function STWChkTef( cType )

Local lRet		:= .F.				//Variavel de retorno
Local oTEF20 	:= STBGetTEF()		//Objeto do TEF j� instanciado

Default cType := ""

ParamType 0 Var 	cType 	As Character	Default 	""

If oTEF20 <> Nil

	Do Case
	
		Case cType $ "CC|CD"
			If 	oTEF20:oConfig:ISCCCD()
				lRet := .T.
			Else
				lRet := .F.
			EndIf	
		Case cType == "CH"
			If oTEF20:oConfig:ISCheque()
				lRet := .T.
			Else
				lRet := .F.
			EndIf
		Case cType == "RC"
			If oTEF20:oConfig:ISRecCel()
				lRet := .T.
			Else
				lRet := .F.
			EndIf			
		Case cType == "CB"
			If oTEF20:oConfig:ISCB()
				lRet := .T.
			Else
				lRet := .F.
			EndIf
			
	EndCase 
	
EndIf

Return lRet

