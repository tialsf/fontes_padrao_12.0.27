#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "STFCRMRMS.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} STFCrmRms
Funcao para pesquisar o credito do cliente no ws da RMS

@author	Varejo
@since		19/08/2014
@version	11
/*/
//-------------------------------------------------------------------
Function STFCrmRms(cType,nVal,cCardConve,cCpfCheck)

Local oDados := Nil //Classe do ws de credito
Local aRet := Array(4) //Array de retorno

Default cType := ''
Default nVal	:= 0
Default cCardConve := ''
Default cCpfCheck := ''

If (cType $ 'CH|CO') .AND. (nVal > 0) .AND. (FindFunction('U__NXTWPAR'))
	oDados := WSCreditoService():New()
	oDados:_URL := STFRmsWs()	
	oDados:TestConnectionService()
	If oDados:lTestConnectionServiceResult	                 						
		If cType == 'CH'
			If !Empty(cCpfCheck)
				oDados:GetCheckAuthorization( Val(STFDePFili(xFilial("SL1"))), AllTrim(cCpfCheck), nVal )
			Else
				oDados:GetCheckAuthorization( Val(STFDePFili(xFilial("SL1"))), STFCpfCli(), nVal )
			EndIf			
			If oDados:oWSGetCheckAuthorizationResult:nStatus == 1
				aRet[1] := .T. 
				aRet[2] := oDados:oWSGetCheckAuthorizationResult:nAmount
				aRet[3] := oDados:oWSGetCheckAuthorizationResult:nAuthorizationID
				aRet[4] := STR0001 //"Limite para pagamento autorizado!"
			Else			
				aRet[1] := .F.							
				aRet[2] := oDados:oWSGetCheckAuthorizationResult:nAmount
				aRet[3] := ''
				aRet[4] := oDados:oWSGetCheckAuthorizationResult:cErrorMessage
			EndIf		
		ElseIf cType == 'CO'
			If !Empty(cCardConve)
				oDados:GetAgreementCardAuthorization(Val(STFDePFili(xFilial("SL1"))),2,AllTrim(cCardConve), nVal)
			Else
				oDados:GetAgreementCardAuthorization(Val(STFDePFili(xFilial("SL1"))),1,STFCpfCli(), nVal)
			EndIf
			If oDados:oWSGetAgreementCardAuthorizationResult:nStatus == 1
				aRet[1] := .T.
				aRet[2] := oDados:oWSGetAgreementCardAuthorizationResult:nAmount
				aRet[3] := oDados:oWSGetAgreementCardAuthorizationResult:nAuthorizationID
				aRet[4] := STR0001 //"Limite para pagamento autorizado!"
			Else			
				aRet[1] := .F.
				aRet[2] := oDados:oWSGetCheckAuthorizationResult:nAmount
				aRet[3] := ''
				aRet[4] := oDados:oWSGetAgreementCardAuthorizationResult:cErrorMessage
			EndIf			
		EndIf
	Else
		aRet[1] := .F.
		aRet[2] := 0
		aRet[3] := ''
		aRet[4] := STR0002 //"Falha de conex�o CRM RMS"
	EndIf	
Else
	aRet[1] := .T.
	aRet[2] := 0
	aRet[3] := ''
	aRet[4] := STR0003 //"Forma de pagamento n�o contempla o CRM!"	
EndIf

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} STFRmsWs
Retorna o caminho do web service de credito

@author	Varejo
@since		19/08/2014
@version	11
/*/
//-------------------------------------------------------------------
Static Function STFRmsWs()

Local cPatchWs := '' //Caminho do webservice da rsm

If !Empty(AllTrim(SuperGetMv("MV_LJWSCPO",,"")))
	cPatchWs := 'http://' + AllTrim(SuperGetMv("MV_LJWSCRM",,"")) + ':' + AllTrim(SuperGetMv("MV_LJWSCPO",,"")) + '/RMS.WS.Credito/Services/CreditoService.svc'
Else
	cPatchWs := 'http://' + AllTrim(SuperGetMv("MV_LJWSCRM",,"")) + '/RMS.WS.Credito/Services/CreditoService.svc'
EndIf

Return cPatchWs


//-------------------------------------------------------------------
/*/{Protheus.doc} STFDePFili
Funcao para pesquisar a filial de/para

@author	Varejo
@since		16/08/2014
@version	11
/*/
//-------------------------------------------------------------------
Function STFDePFili(cFilPrt)

Local cFilialRms := '' //Variavel de retorno

Default cFilPrt := ''

If !Empty(cFilPrt)

	dbSelectArea('MGL')
	MGL->(dbSetOrder(2)) //MGL_FILIAL+MGL_FILPRT
	If MGL->(dbSeek(xFilial('MGL')+cFilPrt))
		cFilialRms := MGL->MGL_FILRMS
	EndIf

EndIf

Return cFilialRms

//-------------------------------------------------------------------
/*/{Protheus.doc} STFDePFili
Funcao para pesquisar a filial de/para

@author	Varejo
@since		16/08/2014
@version	11
/*/
//-------------------------------------------------------------------
Function STFCpfCli()

Local cRet := '' //Retorno do documento do cliente
Local aArea := GetArea()	//Guarda area
Local cCodCli := STDGPBasket( "SL1" , "L1_CLIENTE" ) //Retorna o cliente que esta na cesta de vendas
Local cCodLoj := STDGPBasket( "SL1" , "L1_LOJA" ) //Retorna a loja do cliente que esta na cesta de vendas

dbSelectArea("SA1")
SA1->(dbSetOrder(1)) //A1_FILIAL+A1_COD+A1_LOJA
If SA1->(dbSeek(xFilial("SA1")+cCodCli+cCodLoj))
	cRet := SA1->A1_CGC
EndIf

RestArea(aArea)

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STFSetAuto
Set da autorizacao para o CRM da RMS

@author	Varejo
@since		16/08/2014
@version	11
/*/
//-------------------------------------------------------------------
Function STFSetAuto()

Local oDados := Nil //Classe do ws de credito
Local nX := 0 //Variavel de loop
Local aCheck := IIF(FindFunction("STIGetChRms"),STIGetChRms()[1],{}) //Retorna array com os cheques
Local aConv := IIF(FindFunction("STIGetChRms"),STIGetChRms()[2],{}) //Retorna array com os convenios

If (Len(aCheck) > 0) .OR. (Len(aConv) > 0) 

	oDados := WSCreditoService():New()
	oDados:_URL := STFRmsWs()	
	oDados:TestConnectionService()
	If oDados:lTestConnectionServiceResult
	
		//Confirma o cheque
		For nX := 1 To Len(aCheck)
			oDados:SetCheckPayment(	aCheck[nX][1],; //Codigo da loja
										aCheck[nX][2],; //Numero do pdv
										aCheck[nX][3],; //Numero do cupom COO
										aCheck[nX][4],; //Codigo do operador
										aCheck[nX][5],; //Data do cheque
										aCheck[nX][6],; //Numero do banco
										aCheck[nX][7],; //Agencia
										aCheck[nX][8],; //Conta
										aCheck[nX][9],; //Numero do cheque
										aCheck[nX][10],; //Codigo da autorizacao
										aCheck[nX][11],; //CNPJ-CPF do cliente
										aCheck[nX][12],; //Valor da venda a ser feita naquele cheque
										aCheck[nX][13],; //CPF-CNPJ do dono do cheque
										aCheck[nX][14]) //Codigo do superior
																			
			If oDados:oWSSetCheckPaymentResult:nStatus == 1
				CoNout(STR0004) //"SetCheckPayment efetuado com sucesso!"
			Else
				CoNout(oDados:oWSSetCheckPaymentResult:cErrorMessage)
			EndIf
		Next nX
		
		//Confirma o convenio
		For nX := 1 To Len(aConv)
			oDados:SetAgreementCardPayment(	aConv[nX][1],; //Codigo da loja
												aConv[nX][2],; //Numero do pdv
												aConv[nX][3],; //Numero do cupom COO
												aConv[nX][4],; //Codigo do operador
												aConv[nX][5],; //Codigo da autorizacao
												aConv[nX][6],; //1-para cfp ou 2-numero do cartao do convenio
												aConv[nX][7],; //Numero do cartao do convenio ou cpf do cliente
												aConv[nX][8],; //Valor da venda em convenio
												aConv[nX][9]) //Codigo do superior
																			
			If oDados:oWSSetAgreementCardPaymentResult:nStatus == 1
				CoNout(STR0005) //"SetAgreementCardPayment efetuado com sucesso!"
			Else
				CoNout(oDados:oWSSetAgreementCardPaymentResult:cErrorMessage)
			EndIf
		Next nX		
			
	EndIf
EndIf
STISetChRms()

Return .T.
