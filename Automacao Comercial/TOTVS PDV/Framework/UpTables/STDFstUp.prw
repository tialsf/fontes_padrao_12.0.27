#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH" 
#INCLUDE "STDUPDATA.CH"
#INCLUDE "XMLXFUN.CH"


Static oWS := NIL
Static oXML := NIL


//===================================================
// 				Situacoes do _SITUA                             
//
// "  " - Base Errada, Registro Ignorado.          
// "00" - Venda Efetuada com Sucesso               
// "01" - Abertura do Cupom Nao Impressa           
// "02" - Impresso a Abertura do Cupom             
// "03" - Item Nao Impresso                        
// "04" - Impresso o Item                          
// "05" - Solicitado o Cancelamento do Item        
// "06" - Item Cancelado                           
// "07" - Solicitado o Cancelamento do Cupom       
// "08" - Cupom Cancelado                          
// "09" - Encerrado SL1 (Nao gerado SL4)           
// "10" - Encerrado a Venda                        
//        Pode nao ter sido impresso o cupom       
// "TX" - Foi Enviado ao Server (Pdv)
// "ER" - Erro ao envia ao server (Pdv)  
// "EP" - Erro de processamento da venda (Pdv)
// "RE" - Ja foi feita nova tentativa de subir  (Pdv)                      
// "RX" - Foi Recebido Pelo Server (Server)                 
// "OK" - Foi Processado no Server (Serevr)               
//        Enviar um OK ao Client que foi Processado 
// "CP" - Recebido pela Central de PDV
// "RY" - Cancelamento Nfc-e
// "CX" - Cancelamento enviado first
// "C0" - Cancelamento a enviar first
//===================================================

Function STDFrstUp()

Return NIL

//-------------------------------------------------------------------
/*/{Protheus.doc} STDLogCons
Enviado o Log para o Console

@param cMessage		Codigo da estacao

@author  Varejo
@version P11.8
@since   05/05/2015
@return  Nil
@obs     
@sample
/*/
//-------------------------------------------------------------------
Static Function STDLogCons( cMessage )  

Default cMessage := ""

ParamType 0 Var 	cMessage 	As Character	Default 	""

Conout(cMessage)  

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} STDUpFirstSale
Envia as Vendas para o First

@param aSL1			Array da Venda
@param aSL2			Array dos Itens
@param aSL4			Array dos Pagamentos
@param cEstacaoIni 	Esta��o
@param cStatusNum		Retorno da Subida 

@author  Varejo
@version P11.8
@since   05/05/2015
@return  lRet 			Execu��o com sucesso
@obs     
@sample
/*/
//-------------------------------------------------------------------

Function STDUpFirstSale( aSL1, aSL2, aSL4, cEstacaoIni, ;
							cStatusNum)
Local aArea  		:= GetArea() 	//WorkArea Anterior
Local lRet 		:= .T.			//Retorno da Rotina
Local cXML 		:= ""			//Dados da Venda
Local nSL2 		:= 0			//Contador SL2
Local nSL4 		:= 0			//Contador SL4
Local cError 		:= ""			//Erro Parseamento
Local cWarnning 	:= ""			//Alerta Parseamento
Local cPai 		:= ""			//Node Pai
Local cPaiAnt		:= ""			//Node Pai anterior
Local cNode 		:= ""			//Node
Local cTipo 		:= ""			//Tipo do Node
Local cValor 		:= ""			//Valor do Node
Local nParc		:= 0			//Contador de Parcelas
Local cEntidade 	:= ""			//Codigo da Entidade
Local cSitua  	:= STDUpArrVal(aSL1, "L1_SITUA") //Situa��o da Venda
Local aAreaSE4 	:= SE4->(GetArea())	//WorkArea SL4
Local cFilSE4 	:= xFilial("SE4")	//Filial SF4
Local cCond 		:= ""			//Condi��o de Pagamento
Local cMvVendPad := SuperGetMV("MV_VENDPAD",.F., "") //vendedor Padr�o
Local cVendedor 	:= ""			//Vendedor
Local nValDesc 	:= 0			//Valor do Desconto
Local nPrcTot		:= 0			//Pre�o de Venda
Local cTES			:= ""			//TES de Venda
Local cFilSF4 	:= xFilial("SF4") //Filial SF4
Local aICMS  		:= {} 			//Array do ICMS, onde, 1 - Aliquota, 2- Base , 3 - Valor
Local cSitTrib 	:= ""			//Situa��o Tribut�ria

SF4->(DbSetOrder(1)) //F4_FILIAL + F4_CODIGO


If oWS = NIL
	 oWS := WSINTEGRACAOPDV():New()
	 oWS:_URL := "http://" + STFGetStat("WSSRV")+ "/INTEGRACAOPDV.apw"
EndIf

cXML := cXML + "<vendas></vendas>"

oXML := NIL

oXML := XMLParser( cXML, "" , @cError, @cWarnning)
//fonte LjNfceXDet ljnfce

cPai := "oXML:_vendas"

	 	cTipo := "NOD"  	
	 	cNode := "cabecalho"
	 	cValor := nil
		STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)

		//filhos do cabe�alho

		
		 	cTipo := "NOD"  	
		 	cNode := "cliente"
		 	cValor :=  STDGtEtChv("SA1", STDUpArrVal(aSL1, "L1_CLIENTE")) 
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
			
			cCond := STDUpArrVal(aSL1, "L1_CONDPG")		
			If Empty(cCond)
				SE4->(DbSetOrder(1)) //E4_FILIAL + E4_COD
				If SE4->(DbSeek(cFilSE4))
					cCond := SE4->E4_CODIGO
				EndIf
			EndIf
		 	cTipo := "NOD"  	
		 	cNode := "condicao"
		 	cValor := STDGtEtChv("SE4", cCond )
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)			
												
		 	cTipo := "NOD"  	
		 	cNode := "emissao"
		 	cValor := TRANSFORM(DTOS(STDUpArrVal(aSL1, "L1_EMISNF")), "@r 9999-99-99") //YYYY-MM-DD
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	 			
	
			cChaveSL1 := STDUpArrVal(aSL1, "L1_PDV")+STDUpArrVal(aSL1, "L1_DOC")
		 	cTipo := "NOD"  	
		 	cNode := "cupom"
		 	cValor := cChaveSL1
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)

			cVendedor := STDUpArrVal(aSL1, "L1_VEND")
		 	cTipo := "NOD"  	
		 	cNode := "vendedor" 	
		 	//se for vendedor padr�o n�o envia pois o first exige % de comiss�o
			If cMvVendPad <> cVendedor	
		 		cValor := STDGtEtChv("SA3", cVendedor) 
		 	Else
		 		cValor := ""
		 	EndIf
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)


		 	cTipo := "NOD"  	
		 	cNode := "cancelado"
		 	cValor := IIF(STDUpArrVal(aSL1, "L1_STORC") == "C" .OR. cSitua == "C0", "S", "N") 
			STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)


			//oXML:_vendas:_Itens:_Item //array
			//Filhos do sl2

			If  Len(aSL2) > 0
				cPai := Left(cPai, Rat(":", cPai)-1) //volta o xml
				
				cTipo := "NOD"  	
			 	cNode := "Itens"
			 	cValor := Nil
				STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)
					
			EndIF
			
			cPaiAnt := cPai
			
			nSL2 := 1
			nSL2++
			
			Do While nSL2 <= Len(aSL2) 
				cPai:= cPaiAnt
				
				cSitTrib := SubStr( STDUpArrVal(aSL2[nSL2], "L2_SITTRIB", aSL2[1], .T.), 1, 1 )
			
				nParc ++
				cTipo := "NOD"  			 	
			 	cValor := Nil
			 	cNode := "Item" + LTrim(Str(nParc))
			 	
				STDUpGTag( @cPai, cNode, "Item", cTipo, cValor)   

				//CST utilizadas na varejo
				//00,10,20,40,41 e 60, 90
				//salvo exce�oes o 90, que � meio gen�rico

				cSitTrib := STDUpArrVal(aSL2[nSL2], "L2_SITTRIB", aSL2[1], .T.)
				cTES := STDUpArrVal(aSL2[nSL2], "L2_TES", aSL2[1], .T.)
				aICMS  := Array(6)
				aFill(aICMS, 0)				
				If SF4->(DbSeek(cFilSF4 + cTES))
					Do Case
							 Case SF4->F4_SITTRIB == "00"	//CST = 00
								
								aICMS[1] := Val(SubStr(cSitTrib, 2, 2) + "." + SubStr(cSitTrib, 4, 2))
								aICMS[2] := STDUpArrVal(aSL2[nSL2], "L2_BASEICM", aSL2[1], .T.)
								aICMS[3] := STDUpArrVal(aSL2[nSL2], "L2_VALICM", aSL2[1], .T.)
								
				
							 Case SF4->F4_SITTRIB == "10"	//CST = 10
	
								aICMS[1] := Val(SubStr(cSitTrib, 2, 2) + "." + SubStr(cSitTrib, 4, 2))
								aICMS[2] := STDUpArrVal(aSL2[nSL2], "L2_BASEICM", aSL2[1], .T.)
								aICMS[3] := STDUpArrVal(aSL2[nSL2], "L2_VALICM", aSL2[1], .T.)
								
								//N�o trataremos esse cen�rio, pois temos essaa informa��o no SL2 e ST para varejo na maioria das opera��es � CST 60
				
							//Legenda ECF: T - Reducao BC
							Case SF4->F4_SITTRIB == "20"	//CST = 20
	
								//tamb�m n�o temos essa informa��o
								//cXML +=		"<pICMS>" + LjConvType( MaFisRet(nPosIt, "IT_ALIQICM"), 5, 2 ) + "</pICMS>"
								aICMS[1] := Val(SubStr(cSitTrib, 2, 2) + "." + SubStr(cSitTrib, 4, 2))
								
								aICMS[2] := STDUpArrVal(aSL2[nSL2], "L2_BASEICM", aSL2[1], .T.)
								aICMS[3] := STDUpArrVal(aSL2[nSL2], "L2_VALICM", aSL2[1], .T.)
	
							Case SF4->F4_SITTRIB == "30"	//CST = 30
								//N�o trataremos esse cen�rio, pois temos essaa informa��o no SL2 e ST para varejo na maioria das opera��es � CST 60
				
							//Legenda ECF: I|N - Isento e Nao Tributado
							Case SF4->F4_SITTRIB $ "40|41"
	
							//CST = 50 nao pode constar na NFC-e
							//Rejeicao N12-30: NFC-e com CST 50-Suspensao
	
							//CST = 51 nao pode constar na NFC-e
							//Rejeicao N12-40: NFC-e com CST 51-Diferimento
							
							//Legenda ECF: F - ICMS cobrado anteriormente por Substitui��o Tribut�ria
							Case SF4->F4_SITTRIB == "60"	//CST = 60
	
								aICMS[5] := STDUpArrVal(aSL2[nSL2], "L2_BRICMS", aSL2[1], .T.)
								aICMS[6] := STDUpArrVal(aSL2[nSL2], "L2_ICMSRET", aSL2[1], .T.)
	
							Case SF4->F4_SITTRIB == "70"	//CST = 70
	
								aICMS[1] := Val(SubStr(cSitTrib, 2, 2) + "." + SubStr(cSitTrib, 4, 2))
								
								aICMS[2] := STDUpArrVal(aSL2[nSL2], "L2_BASEICM", aSL2[1], .T.)
								aICMS[3] := STDUpArrVal(aSL2[nSL2], "L2_VALICM", aSL2[1], .T.)
	
							//N�o trataremos esse cen�rio, pois temos essaa informa��o no SL2 e ST para varejo na maioria das opera��es � CST 60
						
							Case SF4->F4_SITTRIB == "90"	//CST = 90
	
								aICMS[1] := Val(SubStr(cSitTrib, 2, 2) + "." + SubStr(cSitTrib, 4, 2))
								
								aICMS[2] := STDUpArrVal(aSL2[nSL2], "L2_BASEICM", aSL2[1], .T.)
								aICMS[3] := STDUpArrVal(aSL2[nSL2], "L2_VALICM", aSL2[1],.T.)			
	
					End Case				
				EndIf
				


				cTipo := "NOD"  	
			 	cNode := "Produto"
			 	cValor :=  STDGtEtChv("SB1", STDUpArrVal(aSL2[nSL2], "L2_PRODUTO", aSL2[1], .T.))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)    
				
				cTipo := "NOD"  	
			 	cNode := "Quantidade"
			 	cValor :=  Str(STDUpArrVal(aSL2[nSL2], "L2_QUANT", aSL2[1], .T.), ,SL2->(TamSx3("L2_QUANT")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  

				cTipo := "NOD"  	
			 	cNode := "precounitario"
			 	nPrcTot := STDUpArrVal(aSL2[nSL2], "L2_VLRITEM", aSL2[1], .T.) + STDUpArrVal(aSL2[nSL2], "L2_DESCPRO", aSL2[1], .T.) + STDUpArrVal(aSL2[nSL2], "L2_VALDESC", aSL2[1], .T.)
			 	cValor :=  Str( nPrcTot / STDUpArrVal(aSL2[nSL2], "L2_QUANT", aSL2[1], .T.), ,SL2->(TamSx3("L2_VLRITEM")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "precototal"
			 	cValor :=  Str( nPrcTot, ,SL2->(TamSx3("L2_VLRITEM")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "ipivalor"
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS e Confins 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS e Confins 14/04/2015
			 	cNode := "ipialiquota" //todo
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "ipibase" //todo
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS e Confins 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  

				cTipo := "NOD"  	
			 	cNode := "icmsaliquota" 
				cValor := Str(aICMS[1],,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "icmsbase" 
			 	cValor := Str( aICMS[2], ,SL2->(TamSx3("L2_BASEICM")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  

				cTipo := "NOD"  	
			 	cNode := "icmsvalor" 
			 	cValor :=  Str(aICMS[3], ,SL2->(TamSx3("L2_VALICM")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "icmssolidaliquota"  //n�o enviar porque o varejo utiliza na maioria dos casos CST 60 que � ST retida
			 	cValor := Str(aICMS[4],,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  

				cTipo := "NOD"  	
			 	cNode := "icmssolidbase" 
			 	cValor := Str(aICMS[5], ,SL2->(TamSx3("L2_ICMSRET")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor) 
				
				cTipo := "NOD"  	
			 	cNode := "icmssolidvalor" 
			 	cValor := Str(aICMS[6], ,SL2->(TamSx3("L2_ICMSRET")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  

				cTipo := "NOD"  	
			 	cNode := "descontovalor" 
			 	
			 	nValDesc := STDUpArrVal(aSL2[nSL2], "L2_DESCPRO", aSL2[1], .T.) + STDUpArrVal(aSL2[nSL2], "L2_VALDESC", aSL2[1], .T.)
			 	cValor :=  Str(nValDesc, ,SL2->(TamSx3("L2_VALDESC")[2]))
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)  
				
				cTipo := "NOD"  	
			 	cNode := "descontoporc" 
			 	nValDesc := (nValDesc / nPrcTot ) * 100
			 	cValor :=  Str( nValDesc,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
												 
				cTipo := "NOD"  	
			 	cNode := "issvalor" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)

				cTipo := "NOD"  	
			 	cNode := "issaliquota" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				cTipo := "NOD"  	
			 	cNode := "issbase" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
					
				cTipo := "NOD"  	
			 	cNode := "cofinsvalor" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				cTipo := "NOD"  	
			 	cNode := "cofinsaliquota" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				cTipo := "NOD"  	
			 	cNode := "cofinsbase" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
																		
				cTipo := "NOD"  	
			 	cNode := "pisvalor" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
								
				cTipo := "NOD"  	
			 	cNode := "pisaliquota" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				cTipo := "NOD"  	
			 	cNode := "pisbase" 
			 	//A Integra��o First utiliza NFC-e como envio, que, nesse caso n�o envia valores de  IPI, PIS, Confins e ISSQN 14/04/2015
			 	cValor :=  Str( 0,,2)
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				cTipo := "NOD"  	
		 		cNode := 	"cancelado" 
			 	cValor := "N"
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
				nSL2++
			EndDo			
			cPai := Left(cPai, Rat(":", cPai)-1) //volta o xml Itenm
			
			cPai := Left(cPai, Rat(":", cPai)-1) //volta o xml Itens
			
			
			If  Len(aSL4) > 0
				 cTipo := "NOD"  	
			 	cNode := "Parcelas"
			 	cValor := Nil
				STDUpGTag( @cPai, cNode, cNode , cTipo, cValor)
				
			EndIf
			
			cPaiAnt := cPai
			nParc := 0
			nSL4 := 1
			nSL4++
			Do While nSL4 <= Len(aSL4)
					cPai:= cPaiAnt
							
					//Filhos do sl4
					nParc++
	
				 	cNode := "Parcela" + LTrim(Str(nParc))
				 	cValor := NIL
				 	
					STDUpGTag( @cPai, cNode, "Parcela" , cTipo, cValor)	
	
					cTipo := "NOD"  	
			 		cNode := 	"vencimento" 
				 	cValor := TRANSFORM(DTOS(STDUpArrVal(aSL4[nSL4], "L4_DATA", aSL4[1], .T.)), "@r 9999-99-99") 
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)
					
					cTipo := "NOD"  	
			 		cNode := 	"valor" 
				 	cValor := Str(STDUpArrVal(aSL4[nSL4], "L4_VALOR", aSL4[1], .T.), ,SL4->(TamSx3("L4_VALOR")[2]))
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)		
					
					cTipo := "NOD"  	
			 		cNode := 	"condicao" 
				 	cValor :=  STDGtEtChv("SE4",STDUpArrVal(aSL1, "L1_CONDPG")) //TODO:
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)

					cTipo := "NOD"  	
			 		cNode := 	"sequencia" 
				 	cValor :=  Alltrim(Str(nParc))
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)


				nSL4++
			EndDo	
	
			SAVE oXml XMLSTRING cXML  
			
			cXML := Encode64(AllTrim(cXML))
			//TODO: TESTAR REENVIO
			cChaveSL1 := STDUpArrVal(aSL1, "L1_PDV")+STDUpArrVal(aSL1, "L1_DOC")
			cEntidade := STDDwnGtEnt("PDV", cEstacaoIni, oWS)
			lRet := oWS:EnviarDados(cEntidade/*codigo*/, STDwnGtTok(oWS, cEstacaoIni),"Vendas",STDGtEtChv("SL1",cChaveSL1, cEntidade) ,iif(cSitua == "00", "1", "2"),cXML) //1 - INCLUSAO;2-ALTERACAO;3-EXCLUS
			If ValType(lRet) <> "L" .OR. !lRet
				STDLogCons(IIf( Empty(GetWscError(3)), GetWscError(1), GetWscError(3) ))
				lRet := .F.
			EndIf

RestArea(aAreaSE4)
RestArea(aArea)
If lRet
	cStatusNum := "OK|"+STDUpArrVal(aSL1, "L1_NUM")
Else
	cStatusNum := "ERRO"
EndIf


oXML := NIL 
Return lRet



//-------------------------------------------------------------------
/*/{Protheus.doc} STDUpDtFirst
Envia a movimenta��o  para o First

@Param   cAlias 	- Alias que sera usado
@Param   nOrder 	- Codigo de ordenacao
@Param   cChave 	- Campos de busca
@Param   cBusca 	- Conteudo de busca
@Param 	 cConfLocal 	- String de confirmacao de gravacao do campo Local	
@Param 	 cConfServer 	- String de confirmacao de gravacao do campo no server
@Param 	 cEstacaoIni 	- Estacao
@Param 	 cFunc		 	- Funcao a ser executado no server

@author  Varejo
@version P11.8
@since   16/01/2013
@return  Nil
@obs     
@sample
/*/
//-------------------------------------------------------------------
Function STDUpDtFirst(	cAlias 		, nOrder 		, cChave		, cBusca	,; 
								cConfLocal		, cEstacaoIni, cFunc )   



Local aRecnos    		:= {}		//Registros
Local nX      		:= 0    	// Contador
Local cNameField 	:= ""      	// Nome do campo situa para o Alias atual
Local aData 	 		:= {}     	// Array com dados a subir para o server
Local lContinua  		:= .F.		// Controle de execucao
Local aArea			:= GetArea()	// Guarda area atual
Local nCont			:= 0 		//Contador
Local lOk				:= .T. 	//Dados ok
Local aCposOri		:= {"E5_DATA","E5_HISTOR", "E5_VALOR", "E5_PREFIXO",;
							"E5_NUMERO", "E5_BANCO", "E5_AGENCIA", "E5_CONTA"} 
Local aCpoComp		:= {} 		//Campos compara��o
Local aCpoEqua	:= {"E5_DATA", "E5_MOEDA", "E5_VALOR", "E5_NATUREZ", ;
						"E5_HISTOR", "E5_TIPODOC", "E5_PREFIXO","E5_NUMERO", ;
						"E5_DTDIGIT", "E5_NUMMOV"}
Local cSeqAnt			:= ""		//Sequencia Anterior
Local nC				:= 0 		//Variavel contadora
Local nC2				:= 0 		//Contador
Local nC3				:= 0		///Contador
Local lMovPar			:= .F. 	//movimentacao Par
Local lRet 			:= .T.		//Retorno da Rotina
Local cXML 			:= ""		//XML envio
Local cError 			:= ""		//Erro Parseamento
Local cWarnning 		:= ""		//Alerta Parseamento
Local cPai 			:= ""		//Node pai
Local cPaiAnt 		:= ""		//Node Pai anterior
Local cNode 			:= ""		//Node
Local cTipo 			:= ""		//Tipo do Node
Local cValor 			:= ""		//Valor do XML
Local cEntidade 		:= ""		//Codigo da Entidade
Local cChaveSE5 		:= ""		//Chave SE5
Local cSitua 			:= ""		//Situa��o

If oWS = NIL
	 oWS := WSINTEGRACAOPDV():New()
	 oWS:_URL := "http://" + STFGetStat("WSSRV")+ "/INTEGRACAOPDV.apw"
EndIf


Default 	cAlias 	 		:= ""
Default 	nOrder 	 		:= 0
Default 	cChave 	 		:= ""
Default 	cBusca 	 		:= ""
Default 	cConfLocal 	 	:= ""
Default 	cFunc 	 			:= ""

ParamType 0 Var 	cAlias 		As Character	Default 	""
ParamType 1 Var 	nOrder 		As Numeric		Default 	0
ParamType 2 Var 	cChave 		As Character	Default 	""
ParamType 3 Var 	cBusca 		As Character	Default 	""
ParamType 4 Var 	cConfLocal 	As Character	Default 	""
ParamType 7 Var 	cFunc 			As Character	Default 	""

// Formata campo NOMETABELA_SITUA
// Exemplo: Iniciando com "S": SL1->L1_SITUA, caso contr�rio MDZ->MDZ_SITUA

If Substr(Upper(cAlias), 1, 1) == "S"
	cNameField := Substr(cAlias, 2, 2) + "_SITUA"        
Else
	cNameField := cAlias + "_SITUA"        
EndIf

DbSelectArea(cAlias)
DbSetOrder(nOrder)  

If DbSeek(cBusca)

	nCampos := FCount() 

	While (cAlias)->(!Eof()) .AND. &(cChave) == cBusca .AND. nCont <= 20 
		aRecnos := {}
		nCont := nCont + 1
		nC := 1
		
		lOk := .T.
		lMovPar := .T.
		lContinua := .T.
		aData := {}
		aCpoComp := {}
		If cAlias == "SE5"
		
			
			Do While (cAlias)->(!Eof()) .AND. &(cChave) == cBusca .AND. nC <= 2 

				
				If nC == 1 .AND. E5_RECPAG == "P"
					//Primeiro registro � pagamento
					cSeqAnt := E5_SEQ
					cChaveSE5 := E5_PREFIXO + E5_NUMERO + Dtos(E5_DATA) + E5_NUMMOV+E5_HISTOR
					cSitua := FieldGet(FieldPos(cNameField))
				ElseIf nC == 1 .AND. E5_RECPAG <> "P"
					//N�o atualiza os registros
					aRecnos := {}
					Loop
				EndIf
				aAdd(aData, {} )
				aAdd(aCpoComp, {})
				aAdd(aRecnos, Recno())
				
				nC3 := 1
	
				Do While lMovPar .AND. nC3  <=  Len( aCpoEqua)
						aAdd(aCpoComp[nC], { aCpoEqua[nC3], FieldGet(FieldPos( aCpoEqua[nC3])), NIL})
						If nC > 1						
							lMovPar := aCpoComp[nC-1][nC3][2] == aCpoComp[nC][nC3][2]
						EndIf
						
					nC3++
				EndDo
				
				
				If nC == 2
					lMovPar := lMovPar  .AND.  E5_RECPAG == "R" .AND. Val(cSeqAnt)+1 == Val(E5_SEQ) //Movimenta��o em pares
				EndIf
				
				If lMovPar
					nC3 := 1
					Do While  nC3  <=  len( aCposOri)
							aAdd(aData[nC], { aCposOri[nC3], FieldGet(FieldPos( aCposOri[nC3])), NIL})						
							nC3++
					EndDo					
				
				Else //!lMovPar
					aRecnos := {}
					dbskip(1)
					Loop
				EndIf

				If nC == 1
					dbskip(1)
				EndIf
				nC++
			EndDo
			
			If Len(aRecnos) == 2 .AND. Len(aData) <= 2
			
				nC := 1
				//Prepar o xml para envio
				
				oXML := NIL
				cXML :=  "<movbancaria></movbancaria>"
				
				oXML := XMLParser( cXML, "" , @cError, @cWarnning)


				cPai := "oXML:_movbancaria"
		
			 	//pai cabeclaho
			 	cTipo := "NOD"  	
			 	cNode := "cabecalho"
			 	cValor := nil			 			
				STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)		

				//filhos do cabe�alho				
				 	cTipo := "NOD"  	
				 	cNode := "tipo"
				 	cValor :=  "4" 			 	
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
					
				 	cTipo := "NOD"  	
				 	cNode := "historico"
				 	cValor :=  STDUpArrVal(aData[nC], "E5_HISTOR") 		 	
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
			
				 	cTipo := "NOD"  	
				 	cNode := "valor"
				 	cValor :=  Str(STDUpArrVal(aData[nC], "E5_VALOR"), ,SE5->(TamSx3("E5_VALOR")[2])) 
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
					
				 	cTipo := "NOD"  	
				 	cNode := "documento"
				 	cValor :=  STDUpArrVal(aData[nC], "E5_PREFIXO")+STDUpArrVal(aData[nC], "E5_NUMERO")
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	

				 	cTipo := "NOD"  	
				 	cNode := "data"
				 	cValor :=   TRANSFORM(DTOS(STDUpArrVal(aData[nC], "E5_DATA")), "@r 9999-99-99") //YYYY-MM-DD	 
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)				
				
			//fim filhos cabe�alho											
			cPai := Left(cPai, Rat(":", cPai)-1) //volta o pai
				
				Do While (nC <= 2)	
				
					//pai origem/destino
				 	cTipo := "NOD"  	
				 	cNode := If(nC == 1, "origem", "destino")
				 	cValor := nil
					STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
				
						//filhos		
						
						 	cTipo := "NOD"  	
						 	cNode := "banco"
						 	cValor :=  STDUpArrVal(aData[nC], "E5_BANCO")
						 	STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
						
						 	cTipo := "NOD"  	
						 	cNode := "agencia"
						 	cValor :=  STDUpArrVal(aData[nC], "E5_AGENCIA")
						 	STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
		
						 	cTipo := "NOD"  	
						 	cNode := "conta"
						 	cValor :=  STDUpArrVal(aData[nC], "E5_CONTA")
						 	STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
						 	
						 	cTipo := "NOD"  	
						 	cNode := "categoria"
						 	cValor :=  ""
							STDUpGTag( @cPai, cNode, cNode, cTipo, cValor)	
		
					cPai := Left(cPai, Rat(":", cPai)-1) //volta o pai		
					//fim filhos origem
					nC++
				EndDo	

			
				SAVE oXml XMLSTRING cXML  
				
				cXML := Encode64(AllTrim(cXML))
				//reenvio
				cEntidade := STDDwnGtEnt("PDV", cEstacaoIni, oWS)
				lContinua := oWS:EnviarDados(cEntidade/*codigo*/, STDwnGtTok(oWS, cEstacaoIni),"Movimentacao Bancaria",STDGtEtChv("SE5",cChaveSE5, cEntidade) ,iif(cSitua == "00", "1", "2"),cXML) //1 - INCLUSAO;2-ALTERACAO;3-EXCLUS
				If ValType(lContinua) <> "L" .OR. !lContinua
					STDLogCons(IIf( Empty(GetWscError(3)), GetWscError(1), GetWscError(3) ))
					lContinua := .F.
				EndIf			

			Endif
	

		EndIf
		
		If lContinua
		
			//array de dados
			For nX := 1 to len(aRecnos)
				dbGoto(aRecnos[nX])
				If RecLock( cAlias, .F. )            		
		         	REPLACE &((cAlias)->(cNameField))	WITH cConfLocal
	   				&(cAlias)->(dbCommit())
		         	&(cAlias)->(MsUnlock()) 
		            
	
	        	EndIf 
        	Next nX
		Else
			STDLogCons(STR0014 ) //"Erro de comunicacao com servidor"
		EndIf 
			
		/*
			Por garantia posiciona no registro anterior para depois
			ir para o primeiro, pois ao alterar um campo do indice
			os registros serao reordenados.
		*/        
		&(cAlias)->(DbSkip(-1))
		&(cAlias)->(DbGoTop()) 

	EndDo

EndIf
oXML := NIL

cSeqAnt := NIL

RestArea(aArea)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} STDUpGTag
Cria e alimenta o valor da Tag

@Param   cPai 	- Node Pai
@Param   cNode 	- Nome Logico do Node
@Param   cRealNode 	- Nome real do Node
@Param   cTipo 	- Tipo do Node
@Param 	 cValor 	- Valor do Node	
@author  Varejo
@version P11.8
@since   05/05/2015
@return  Nil
@obs     
@sample
/*/
//-------------------------------------------------------------------

Static Function STDUpGTag( cPai, cNode, cRealNode, cTipo, ;
								cValor)

Default cRealNode := cNode

XmlNewNode(&cPai, cNode, cNode, cTipo )    
&(cPai+":" + cNode + ":RealName") := cRealNode
If cValor <> NIL
	&(cPai+":" + cNode + ":Text") := AllTrim(cValor)
Else
	cPai += ":" + cNode
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} STDUpFrDsty
Destroi o Objeto WS

@author  Varejo
@version P11.8
@since   05/05/2015
@return  Nil
@obs     
@sample
/*/
//-------------------------------------------------------------------

Function STDUpFrDsty()

If oWS <> NIL
	oWS := FreeObj(oWS)
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} STDUpArrVal
Retorna o valor dos arrays da Venda

@Param   aArray 	- Array de Dados
@Param   cCampo 	- Nome do Campo
@Param   aHeader 	- Header de Campo (SL2/SL4)
@Param   lL2L4 	- Array SL2/SL4
@Param 	 cValor 	- Valor do Node	
@author  Varejo
@version P11.8
@since   05/05/2015
@return  uRet  - Valor
@obs     
@sample
/*/
//-------------------------------------------------------------------

Static Function STDUpArrVal(aArray, cCampo, aHeader, lL2L4)
Local nPos := 0
local uRet := NIL


If lL2l4
	nPos := aScan(aHeader, { |l| l == AllTrim(cCampo)})
	
	If nPos > 0
		uRet := aArray[nPos]
	EndIf
Else
	nPos := aScan(aArray, { |l| l[1] == AllTrim(cCampo)})
	
	
	If nPos > 0
		uRet := aArray[nPos, 02]
	EndIf

EndIf

Return uRet

//-------------------------------------------------------------------
/*/{Protheus.doc} STDGtEtChv
Retorna a Entidade First + Chave

@Param   cAliasEnt 	- Alias da Entidade
@Param   cChave 	- Chave
@Param   cEntidade 	- Codigo da Entidade
@author  Varejo
@version P11.8
@since   05/05/2015
@return  cChaveRet  - Retorno da rotina 
@obs     
@sample
/*/
//-------------------------------------------------------------------


Function STDGtEtChv(cAliasEnt, cChave, cEntidade)
Local cChaveRet 		:= "" //Chave de Retorno
Local nTamFil 		:= RetFilFirst() //Tamanho da Filial First
Local cFilFirst := SuperGetMv("MV_LJFILIN",.F.,"") //De/para filial do First

Default cEntidade := ""

cEntidade := STDDwnGtEnt("FIRST",cEntidade)


Do Case 
Case cAliasEnt == "SA1" //Cliente
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave //ALTERADO - RECEBEU PELO PDV
Case cAliasEnt == "SE4" //Condicao
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave
Case cAliasEnt == "SA3" //Vendedor
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave
Case cAliasEnt == "SB1" //Produto
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave
	
Case cAliasEnt == "SL1" //VENDAS
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave

Otherwise
	cChaveRet := cEntidade +PadR(cFilFirst, nTamFil)+cChave
EndCase

Return cChaveRet
