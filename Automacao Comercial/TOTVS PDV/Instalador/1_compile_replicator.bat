@echo off
@setlocal
set JAVA_HOME=%CD%\jre
set CLASSPATH=%JAVA_HOME%/bin;%CLASSPATH%
set PATH=%JAVA_HOME%/bin;%PATH%

msxsl\msxsl config\config2.por.xml xsl\custom.por.xsl -o i18n\custom.por.xml
msxsl\msxsl config\config2.eng.xml xsl\custom.eng.xsl -o i18n\custom.eng.xml
msxsl\msxsl config\config2.spa.xml xsl\custom.spa.xsl -o i18n\custom.spa.xml
msxsl\msxsl config\config2.xml xsl\installInput.xsl -o specs\installInput.xml

call IzPack\bin\compile replicator.xml -b . -o launchers\replicator\replicator.jar -k standard.
@endlocal