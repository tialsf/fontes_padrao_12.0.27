@echo off
echo [Begin]

set OLD_DIR=%CD%
cd launchers\replicator
del files.7z /Q
..\..\7zip\7za.exe a files.7z @files.txt -mx0
copy /B ..\..\7zip\totvs.sfx + config.txt + files.7z ..\..\output\replicator\setup.exe
cd %OLD_DIR%

echo.
echo Output file launchers\replicator\setup.exe
echo [End]