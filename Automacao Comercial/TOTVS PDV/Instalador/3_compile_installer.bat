@echo off
@setlocal
set JAVA_HOME=%CD%\jre
set CLASSPATH=%JAVA_HOME%/bin;%CLASSPATH%
set PATH=%JAVA_HOME%/bin;%PATH%

if not exist i18n\custom.por.xml msxsl\msxsl config\config2.por.xml xsl\custom.por.xsl -o i18n\custom.por.xml
if not exist i18n\custom.eng.xml msxsl\msxsl config\config2.eng.xml xsl\custom.eng.xsl -o i18n\custom.eng.xml
if not exist i18n\custom.spa.xml msxsl\msxsl config\config2.spa.xml xsl\custom.spa.xsl -o i18n\custom.spa.xml
if not exist specs\installInput.xml msxsl\msxsl config\config2.xml xsl\installInput.xsl -o specs\installInput.xml

call IzPack\bin\compile installer.xml -b . -o launchers\installer\installer.jar -k standard
@endlocal