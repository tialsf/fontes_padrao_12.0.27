@echo off
echo [Begin]

set OLD_DIR=%CD%
cd launchers\installer
del files.7z /Q
..\..\7zip\7za.exe a files.7z @files.txt -mx0
copy /B ..\..\7zip\totvs.sfx + config.txt + files.7z ..\..\output\installer\setup.exe
cd %OLD_DIR%

echo.
echo Output file output\installer\setup.exe
echo [End]