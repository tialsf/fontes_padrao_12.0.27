cd $INSTALL_PATH${REPLICATOR_PATH}
call IzPack\bin\compile files\replica.xml -b . -o launcher\replica.jar -k standard.
if %errorlevel% neq 0 goto end

set OLD_DIR=%CD%
cd launcher
del ${BUILD_PATH}\setup.exe /Q
del files.7z /Q
..\7zip\7za.exe a files.7z @files.txt -mx0
if %errorlevel% neq 0 goto end
copy /B ..\7zip\totvs.sfx + config.txt + files.7z ${BUILD_PATH}\setup.exe
cd %OLD_DIR%

:end
del 7zip\*.* /Q /S /F
del files\*.* /Q /S /F
del izpack\*.* /Q /S /F
del launcher\*.* /Q /S /F

rmdir 7zip /Q /S
rmdir files /Q /S
rmdir izpack /Q /S
rmdir launcher /Q /S
