@echo off
setlocal ENABLEDELAYEDEXPANSION

set companydir="${PDV_COMPANY_DIR}"
if %companydir% NEQ "" if exist %companydir%\sigamat.emp copy %companydir%\sigamat.emp "$INSTALL_PATH\Protheus_data\${INSTALL_SYSTEM}" /Y

REM Set a string with an arbitrary number of substrings separated by semi colons
set string=${PDV_FILES_SITEF}${PDV_FILES_DEVICES}${PDV_FILES_USER}

:stringLOOP
    REM Stop when the string is empty
    if "!string!" EQU "" goto END

    for /f "delims=;" %%a in ("!string!") do set substring=%%a

		copy "!substring!" "$INSTALL_PATH\Protheus\bin\smartclient"

REM Now strip off the leading substring
:striploop
    set stripchar=!string:~0,1!
    set string=!string:~1!

    if "!string!" EQU "" goto stringloop

    if "!stripchar!" NEQ ";" goto striploop

    goto stringloop
)

:END
endlocal