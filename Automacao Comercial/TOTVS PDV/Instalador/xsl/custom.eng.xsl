<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:template match="/">
<langpack>
	<str id="MyCheckedHelloPanel.headline" txt="Welcome" />
	<str id="ProcessPanel.headline" txt="Updating" />
	<str id="installer.reversetitle" txt="${{APP_NAME}} ${{APP_VER}} "/>

	<str id="LicencePanel.headline.licence" txt="Terms de Use"/>
	<str id="TargetPanel.headline" txt="Installation Directory"/>
    <str id="UserInputPanel.headline.operation" txt="Installation"/>	
	<str id="UserInputPanel.headline.rpo" txt="${{APP_NAME}} Configuration" />
	<str id="UserInputPanel.headline.config" txt="${{APP_NAME}} Configuration" />
	<str id="UserInputPanel.headline.packs" txt="Components" />
	<str id="UserInputPanel.headline.userfiles" txt="Other Files" />
	<str id="UserInputPanel.headline.sitef" txt="SiTef" />
	<str id="UserInputPanel.headline.devices" txt="Devices" />
	<str id="UserInputPanel.headline.replica" txt="Replica Generator" />
	<str id="UserInputPanel.headline.source" txt="Source Directory" />
	<str id="UserInputPanel.headline.pdvconfig" txt="Station Configuration" />

    <!-- Install or Update choice -->
    <str id="installOrUpdate.inform" txt="Welcome to ${{APP_NAME}}. Click the 'Next' button to install"/>
    <str id="radio.install" txt="Install ${{APP_NAME}}"/>
    <str id="radio.update" txt="Update my installation"/>

	<!-- Install path selection -->
	<str id="install.dir.label" txt="Directory:" />
	<str id="install.dir.inform" txt="Enter installation directory for ${{APP_NAME}} below." />
	<str id="install.folder.empty.validator" txt="Field Directory is mandatory!" />
	<str id="validator.install.folder.blankField" txt="One directory should be selected for installation." />
	<str id="validator.install.folder.freeSpace" txt="Space insufficient! Minimum space required is 1 GB." />
	<str id="validator.install.folder.empty" txt="Entered directory is not empty! Choose another directory." />
	<str id="validator.install.folder.create" txt="Entered directory does not exist but it will be created." />
	<str id="data.validation.error.title" txt="Error" />
	<str id="data.validation.warning.title" txt="Warning" />

	<str id="appserver.config.description" txt="Enter the connection parameters to TOTVS | Application Server." />
	<str id="appserver.config.server" txt="Server:" />
	<str id="appserver.config.port" txt="Port:" />	
	<str id="appserver.config.servervalidator" txt="You must type the environment used by TOTVS Application Server" />
	<str id="appserver.config.portvalidator" txt="You must type the network type used by TOTVS Application Server" />
	<str id="updserver.config.port" txt="Update server port:" />
	<str id="appserver.rpo.description" txt="Enter the base country for the data dictionary e RPO information." />
	<str id="appserver.rpo.db" txt="Database" />
	<str id="appserver.rpo.db.1" txt="DBF Standard ISAM" />
	<str id="appserver.rpo.db.2" txt="CTree" />
	<str id="appserver.rpo.language" txt="RPO Language" />
	<str id="appserver.rpo.language.1" txt="Portuguese" />
	<str id="appserver.rpo.language.2" txt="English" />
	<str id="appserver.rpo.language.3" txt="Spanish" />
	<str id="appserver.regional.language" txt="Available countries" />

	<xsl:for-each select="xfragment/str">
		<str id="{@id}" txt="{@txt}"/>
	</xsl:for-each>
</langpack>
</xsl:template>
</xsl:stylesheet>
