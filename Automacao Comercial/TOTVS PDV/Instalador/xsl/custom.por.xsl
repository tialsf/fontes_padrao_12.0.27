﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:template match="/">
<langpack>
	<str id="MyCheckedHelloPanel.headline" txt="Bem vindo" />
	<str id="ProcessPanel.headline" txt="Atualizando" />	
	<str id="installer.reversetitle" txt="${{APP_NAME}} ${{APP_VER}}" />

	<str id="LicencePanel.headline.licence" txt="Termo de Uso" />
  	<str id="TargetPanel.headline" txt="Diretório de Instalação"/>
    <str id="UserInputPanel.headline.operation" txt="Instalação"/>
	<str id="UserInputPanel.headline.rpo" txt="Configuração do ${{APP_NAME}}" />
	<str id="UserInputPanel.headline.config" txt="Configuração do ${{APP_NAME}}" />
	<str id="UserInputPanel.headline.packs" txt="Componentes" />
	<str id="UserInputPanel.headline.userfiles" txt="Outros Arquivos" />
	<str id="UserInputPanel.headline.sitef" txt="SiTef" />
	<str id="UserInputPanel.headline.devices" txt="Periféricos" />
	<str id="UserInputPanel.headline.replica" txt="Gerador de Réplica" />
	<str id="UserInputPanel.headline.source" txt="Diretório de Origem" />
	<str id="UserInputPanel.headline.pdvconfig" txt="Configuração da Estação" />

	<str id="UserInputPanel.dir.notdirectory.message" txt="Diretório não encontrado"/>
	<str id="UserInputPanel.button.browse" txt="Procurar..."/>
	<str id="UserInputPanel.button.delete" txt="Remover"/>

	<!-- Install or Update choice -->
	<str id="installOrUpdate.inform" txt="Seja bem vindo ao ${{APP_NAME}}. Clique no botão 'Próximo' para instalar."/>
	<str id="radio.install" txt="Instalar o ${{APP_NAME}}"/>
	<str id="radio.update" txt="Atualizar a minha instalação"/>

	<!-- Install path selection -->
    <str id="install.dir.label" txt="Diretório:"/>
  	<str id="install.dir.inform" txt="Informe abaixo o diretório de instalação do ${{APP_NAME}}."/>
  	<str id="install.folder.empty.validator" txt="O campo Diretório é obrigatório!"/>
  	<str id="validator.install.folder.blankField" txt="Deve ser selecionado um diretório para instalação."/>
  	<str id="validator.install.folder.freeSpace" txt="Espaço insuficiente! O espaço mínimo requerido é de 1 GB."/>
  	<str id="validator.install.folder.empty" txt="O diretório informado não está vazio! Selecione outro diretório."/>
  	<str id="validator.install.folder.create" txt="O diretório informado não existe e será criado."/>
    <str id="data.validation.error.title" txt="Erro"/>
	<str id="data.validation.warning.title" txt="Aviso"/>

	<str id="appserver.config.description" txt="Entre com os parâmetros de conexão do TOTVS Application Server." />
	<str id="appserver.config.env" txt="Ambiente:" />
	<str id="appserver.config.port" txt="Porta:" />
	<str id="appserver.config.servervalidator" txt="Você deve fornecer o ambiente que será utilizado pelo TOTVS Application Server" />
	<str id="appserver.config.portvalidator" txt="Você deve fornecer a porta de rede que será utilizada pelo TOTVS Application Server" />
	<str id="updserver.config.port" txt="Porta servidor de atualização:" />
	<str id="appserver.rpo.description" txt="Informe abaixo qual o país base para os dicionários de dados e informações do RPO." />
	<str id="appserver.rpo.db" txt="Banco de Dados" />
	<str id="appserver.rpo.db.1" txt="ISAM Padrão DBF" />
	<str id="appserver.rpo.db.2" txt="CTree" />
	<str id="appserver.rpo.language" txt="Idioma do RPO" />
	<str id="appserver.rpo.language.1" txt="Portugues" />
	<str id="appserver.rpo.language.2" txt="Ingles" />
	<str id="appserver.rpo.language.3" txt="Espanhol" />
	<str id="appserver.regional.language" txt="Países disponíveis" />

	<str id="pdv.id.description" txt="Informe abaixo o código da estação" />
	<str id="pdv.id.shortdescription" txt="Código da estação" />
	<str id="pdv.id.validator" txt="O código da estação é obrigatório" />

	<str id="pdv.system.path" txt="Informe o nome do diretório system" />
	<str id="pdv.system.validator" txt="O caminho do diretório system é obrigatório" />

	<str id="pdv.company.description" txt="Informe abaixo o diretório para cópia do arquivo de empresas (sigamat.emp)" />
	<str id="pdv.userfiles.description" txt="Inclua abaixo outros arquivos (DLLs, executáveis, arquivos .ini, etc):" />
	<str id="pdv.sitef.description" txt="Informe os arquivos do SiTef (DLL e .ini):" />
	<str id="pdv.devices.description" txt="Informe os arquivos de configuração dos periféricos:" />
	<str id="pdv.build.dir" txt="Informe o diretório onde o instalador da réplica será salvo:"/>
	<str id="pdv.source.dir" txt="Informe o diretório onde o TOTVS Microsiga Protheus está instalado:"/>

	<xsl:for-each select="xfragment/str">
		<str id="{@id}" txt="{@txt}"/>
	</xsl:for-each>
</langpack>
</xsl:template>
</xsl:stylesheet>
