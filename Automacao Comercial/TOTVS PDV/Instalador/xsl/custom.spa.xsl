<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:template match="/">
<langpack>
	<str id="MyCheckedHelloPanel.headline" txt="Bienvenido" />
	<str id="ProcessPanel.headline" txt="Actualizando" />
	<str id="installer.reversetitle" txt="${{APP_NAME}} ${{APP_VER}}" />

	<str id="LicencePanel.headline.licence" txt="Términos de uso" />
   	<str id="TargetPanel.headline" txt="Directorio de Instalación"/>
	<str id="UserInputPanel.headline.operation" txt="Instalación"/>	
	<str id="UserInputPanel.headline.rpo" txt="Configuración del ${{APP_NAME}}" />
	<str id="UserInputPanel.headline.config" txt="Configuración del ${{APP_NAME}}" />
	<str id="UserInputPanel.headline.packs" txt="Componentes" />
	<str id="UserInputPanel.headline.userfiles" txt="Otros Archivos" />
	<str id="UserInputPanel.headline.sitef" txt="SiTef" />
	<str id="UserInputPanel.headline.devices" txt="Periféricos" />
	<str id="UserInputPanel.headline.replica" txt="Replica Generador" />
	<str id="UserInputPanel.headline.source" txt="Directorio de Origen" />
	<str id="UserInputPanel.headline.pdvconfig" txt="Configuración del Estacion" />

	<str id="UserInputPanel.dir.notdirectory.message" txt="Directorio no encontrado"/>
	<str id="UserInputPanel.button.browse" txt="Examinar..."/>
	<str id="UserInputPanel.button.delete" txt="Remover"/>

    <!-- Install or Update choice -->
    <str id="installOrUpdate.inform" txt="Bienvenido al ${{APP_NAME}} Presione el botón 'Siguiente' para instalar."/>
    <str id="radio.install" txt="Instalar el ${{APP_NAME}}"/>
    <str id="radio.update" txt="Actualizar mi instalación"/>

	<!-- Install path selection -->
	<str id="install.dir.label" txt="Directorio:" />
	<str id="install.dir.inform" txt="Informe abajo el directorio de instalación de ${{APP_NAME}}." />
	<str id="install.folder.empty.validator" txt="El campo Directorio es obligatorio!" />
	<str id="validator.install.folder.blankField" txt="Debe ser seleccionado un directorio para instalación." />
	<str id="validator.install.folder.freeSpace" txt="¡Espacio no es suficiente! El espacio mínimo requerido es de 1GB." />
	<str id="validator.install.folder.empty" txt="¡El directorio informado no está vacío! Seleccione otro directorio." />
	<str id="validator.install.folder.create" txt="El directorio informado no existe pero será creado." />
	<str id="data.validation.error.title" txt="Error" />
	<str id="data.validation.warning.title" txt="Advertencia" />

	<str id="appserver.config.description" txt="Informe los parámetros de configuración de conexión del TOTVS | Application Server." />
	<str id="appserver.config.server" txt="Servidor:" />
	<str id="appserver.config.port" txt="Puerta:" />
	<str id="appserver.config.servervalidator" txt="Usted debe proporcionar el ambiente que será utilizado por TOTVS Application Server" />
	<str id="appserver.config.portvalidator" txt="Usted debe proporcionar el puerta de red que se utilizado por TOTVS Application Server" />
	<str id="updserver.config.port" txt="Puerta servidor de actualización:" />
	<str id="appserver.rpo.description" txt="Informe a continuación el país para los diccionarios de datos e información de RPO." />
	<str id="appserver.rpo.db" txt="Base de Dados" />
	<str id="appserver.rpo.db.1" txt="ISAM Estándar DBF" />
	<str id="appserver.rpo.db.2" txt="CTree" />
	<str id="appserver.rpo.language" txt="Idioma del RPO" />
	<str id="appserver.rpo.language.1" txt="Portugués" />
	<str id="appserver.rpo.language.2" txt="Inglés" />
	<str id="appserver.rpo.language.3" txt="Español" />
	<str id="appserver.regional.language" txt="Países disponibles" />

	<str id="pdv.id.description" txt="Informe a continuación el código de estación" />
	<str id="pdv.id.shortdescription" txt="Código de estación" />
	<str id="pdv.id.validator" txt="Código de estación es obligatorio" />
	<str id="pdv.company.description" txt="Informe a continuación el directorio a copiar los archivos de las empresas (sigamat.emp)" />
	<str id="pdv.userfiles.description" txt="Informe a continuación otros archivos (DLLs, ejecutables, archivos .ini, etc):" />
	<str id="pdv.sitef.description" txt="Informe a continuación archivos de SiTef (DLL e .ini):" />
	<str id="pdv.devices.description" txt="Informe el archivos de configuración de periféricos:" />
	<str id="pdv.build.dir" txt="Informe el directorio donde se guarda la réplica instalador:"/>
	<str id="pdv.source.dir" txt="Informe o diretório donde está instalado TOTVS Microsiga Protheus:"/>
	
	<xsl:for-each select="xfragment/str">
		<str id="{@id}" txt="{@txt}"/>
	</xsl:for-each>
</langpack>
</xsl:template>
</xsl:stylesheet>
