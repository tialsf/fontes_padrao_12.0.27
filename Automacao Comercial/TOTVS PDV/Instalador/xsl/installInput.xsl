<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" indent="yes"/>
<xsl:template match="/">
<userInput>
    <panel order="0" border="false" id="rpo">
        <field id="appserver.rpo.description" type="staticText" align="left" />
        <field type="divider" />
        <field type="space" />
        <field type="space" />
        <field type="combo" variable="RPO_DB">
			<spec align="left" id="appserver.rpo.db">
				<choice id="appserver.rpo.db.1" value="dbf"/>
				<choice id="appserver.rpo.db.2" value="ctree"/>
			</spec>
        </field>
        <field type="space" />
        <field type="combo" variable="RPO_LANGUAGE">
			<spec align="left" id="appserver.rpo.language">
				<choice id="appserver.rpo.language.1" value="Portuguese"/>
				<choice id="appserver.rpo.language.2" value="English"/>
				<choice id="appserver.rpo.language.3" value="Spanish"/>
			</spec>
        </field>
        <field type="space" />
        <field type="combo" variable="REGIONAL_LANGUAGE">
			<spec align="left" id="appserver.regional.language">
				<xsl:for-each select="xfragment/choice">
				<xsl:choose>
				<xsl:when test="@set">
				<choice id="{@id}" value="{@value}" set="{@set}"/>
				</xsl:when>
				<xsl:otherwise>
				<choice id="{@id}" value="{@value}"/>
				</xsl:otherwise>
				</xsl:choose>
				</xsl:for-each>
			</spec>
        </field>
    </panel>

	<panel order="1" border="false">
        <field id="appserver.config.description" type="staticText" align="left" />
        <field type="divider" />
        <field type="space" />
        <field type="space" />
        <field type="text" variable="SERVER_ENV">
            <spec id="appserver.config.env" set="environment" size="20" />
            <validator class="com.izforge.izpack.util.NotEmptyValidator" id="appserver.config.servervalidator">
            </validator>
        </field>
        <field type="space" />
        <field type="text" variable="SERVER_PORT">
            <spec id="appserver.config.port" set="1234" size="20" />
            <validator class="com.izforge.izpack.util.NotEmptyValidator" id="appserver.config.portvalidator">
            </validator>
        </field>
		<field type="space" />
        <field type="text" variable="UPDATE_PORT">
            <spec id="updserver.config.port" set="1235" size="20" />
            <validator class="com.izforge.izpack.util.NotEmptyValidator" id="appserver.config.portvalidator">
            </validator>
        </field>
    </panel>

	<panel order="2" border="false">
        <field type="space" />
        <field type="space" />
		<field id="pdv.id.description" type="staticText" align="left" />
        <field type="text" variable="PDV_ID">
            <spec size="20" />
            <validator class="com.izforge.izpack.util.NotEmptyValidator" id="pdv.id.validator">
            </validator>
        </field>
        <field type="space" />
		<field id="pdv.system.path" type="staticText" align="left" />
        <field type="text" variable="INSTALL_SYSTEM">
            <spec size="35" />
            <validator class="com.izforge.izpack.util.NotEmptyValidator" id="pdv.system.validator">
            </validator>
        </field>
		<field type="space" />
		<field id="pdv.company.description" type="staticText" align="left" />
        <field type="dir" variable="PDV_COMPANY_DIR">
            <spec size="25" mustExist="true" />
        </field>
    </panel>

	<panel order="3" border="false">
        <field id="pdv.sitef.description" type="staticText" align="left" />
		<field type="space" />
		<field type="space" />
        <field type="multiFile" variable="PDV_FILES_SITEF" align="left">
            <spec allowEmptyValue="true" prefY="150" prefX="250" />
        </field>
    </panel>

	<panel order="4" border="false">
        <field id="pdv.devices.description" type="staticText" align="left" />
		<field type="space" />
		<field type="space" />
        <field type="multiFile" variable="PDV_FILES_DEVICES" align="left">
            <spec allowEmptyValue="true" prefY="150" prefX="250" />
        </field>
    </panel>

	<panel order="5" border="false">
        <field id="pdv.userfiles.description" type="staticText" align="left" />
		<field type="space" />
		<field type="space" />
        <field type="multiFile" variable="PDV_FILES_USER" align="left">
            <spec allowEmptyValue="true" prefY="150" prefX="250"/>
        </field>
    </panel>

	<panel order="6" border="false">
		<createForPack name="izpack" />
		<field type="staticText" align="left" id="pdv.build.dir" />
		<field type="divider" />
		<field type="space" />
		<field type="space" />
		<field type="dir" align="left" variable="BUILD_PATH">
			<spec id="install.dir.label" size="25" mustExist="true" set="${{SOURCE_PATH}}" />
			<validator class="com.izforge.izpack.util.NotEmptyValidator" id="install.folder.empty.validator" />
		</field>
	</panel>
</userInput>

<!-- 
<validator classname="com.izforge.izpack.panels.userinput.validator.CompareValidator" />
<validator classname="com.totvs.remotehtml.panels.CompareValidator" /> 
<validator class="com.izforge.izpack.util.RegularExpressionValidator" id="smartclient.config.maxheaperror">
<param name="pattern" value="(51[2-9])|(5[2-9][0-9])|([6-9][0-9][0-9])|([1-9]([0-9]{3,}))" /> 
<param name="pattern" value="(12[8-9])|(1[3-9][0-9])|([2-9][0-9][0-9])|([1-9]([0-9]{3,}))" />
</validator>
-->
</xsl:template>
</xsl:stylesheet>
