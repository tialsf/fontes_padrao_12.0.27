#INCLUDE "LOJA075.ch"
#INCLUDE "Protheus.ch"
#INCLUDE "FWEVENTVIEWCONSTS.CH"                            
#INCLUDE "FWADAPTEREAI.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
���Programa  � LOJA075  � Autor �Vendas Clientes     � Data �  08/10/10   ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de codigos de retorno do SITEF para cada bandeira ���
���          � de cartao de credito ou debito                             ���
�����������������������������������������������������������������������������
/*/
Function LOJA075()
Private cCadastro := OemToAnsi(STR0001) //"Codigos Retorno SITEF"
Private aRotina := {}       
 
DbSelectArea("MDE")
MDE->(DbSeek(xFilial("MDE")))
If MDE->(Eof())
	LJIncMDE()
	
    //Verifica se existe o campo REDE AUTORIZADORA e Inclui os codigos de retorno das Redes Autorizadoras do SiTEF
	DbSelectArea("SAE")
	If SAE->(ColumnPos("AE_REDEAUT")) > 0
		LjAjustMDE()
	EndIf
EndIf

DbSelectArea("MDE")
aRotina := MenuDef()
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('MDE')
oBrowse:SetDescription(OemToAnsi(STR0001))
oBrowse:Activate()	

Return Nil

//-------------------------------------------------------------------
/* {Protheus.doc} MenuDef
Menu Funcional

@author Vendas & CRM
@since 02/08/2012
@version 11
@return  aRotina - Array com as opcoes de menu

*/
//-------------------------------------------------------------------
Static Function MenuDef()     
Local aRotina        := {}

ADD OPTION aRotina TITLE STR0002 ACTION "PesqBrw"                                          OPERATION 0                                                                                                     ACCESS 0 //"Pesquisar"
ADD OPTION aRotina TITLE STR0003 ACTION "VIEWDEF.LOJA075"     OPERATION MODEL_OPERATION_VIEW         ACCESS 0 //"Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION "VIEWDEF.LOJA075"     OPERATION MODEL_OPERATION_INSERT      ACCESS 0 //"Incluir"
ADD OPTION aRotina TITLE STR0005 ACTION "VIEWDEF.LOJA075"     OPERATION MODEL_OPERATION_UPDATE    ACCESS 0 //"Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION "VIEWDEF.LOJA075"     OPERATION MODEL_OPERATION_DELETE     ACCESS 0 //"Excluir"

Return aRotina

//-------------------------------------------------------------------
/* {Protheus.doc} ModelDef
Definicao do Modelo de dados

@author Vendas & CRM
@since 02/08/2012
@version 11
@return  oModel - Retorna o model com todo o conteudo dos campos preenchido

*/
//-------------------------------------------------------------------
Static Function ModelDef()
Local oStructMDE 	:= FWFormStruct(1,"MDE") 	// Estrutura da tabela MDE
Local oModel 		:= Nil						// Objeto com o modelo de dados

//-----------------------------------------
//Monta o modelo do formul�rio 
//-----------------------------------------
oModel:= MPFormModel():New("LOJA075",/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
oModel:AddFields("LOJA075_MDE", Nil/*cOwner*/, oStructMDE ,/*Pre-Validacao*/,/*Pos-Validacao*/,/*Carga*/)
oModel:GetModel("LOJA075_MDE"):SetDescription(STR0001)

Return oModel

//-------------------------------------------------------------------
/* {Protheus.doc} ViewDef
Definicao da Interface do programa.

@author		Vendas & CRM
@version	11
@since 		02/08/2012
@return		oView - Retorna o objeto que representa a interface do programa

*/
//-------------------------------------------------------------------
Static Function ViewDef()
Local oView  		:= Nil						// Objeto da interface
Local oModel  		:= FWLoadModel("LOJA075")	// Objeto com o modelo de dados
Local oStructMDE 	:= FWFormStruct(2,"MDE")	// Estrutura da tabela SBA

//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   
oView:EnableControlBar(.T.)  
oView:AddField( "LOJA075_MDE" , oStructMDE )
oView:CreateHorizontalBox( "HEADER" , 100 )
oView:SetOwnerView( "LOJA075_MDE" , "HEADER" )
                
Return oView

            
/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �Loja075Valid� Autor �Vendas Clientes     � Data �  08/10/10   ���
���������������������������������������������������������������������������͹��
���Descricao � Validacao da amarracao entre Administradora Financeira x Cod.���
���          � Sitef                                                        ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Function Loja075Valid()
                 
dbSelectArea("MDE")
MDE->(dbSetOrder(1))
If MDE->(dbSeek(xFilial("MDE")+M->AE_ADMCART )) .And. M->AE_TIPO <> MDE->MDE_TIPO
	MsgAlert(STR0007) //"Esse c�digo de retorno do SITEF n�o � do mesmo tipo dessa administradora financeira."
	Return(.F.)
EndIf

Return .T.    

  
/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �LJIncMDE� Autor �Vendas Clientes     � Data �  15/04/13   	���
���������������������������������������������������������������������������͹��
���Descricao �Popula MDE											   		���
���          �                                                       		���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/

Static Function LJIncMDE() 

Local aCodsSITEF := {} 
Local nCont		 := 0  
Local cTexto     := STR0011 //'Inicio da cria��o dos registros na tabela MDE'      
Local nSaveSx8 	 := GetSx8Len()
Local aRet 		 := {}

INCLUI := .T.

//Carga inicial na tabela MDE

//---------------------
//Bandeiras do Tipo CC
//---------------------
//             Codigo SiTef, "CC",  Descricao
aAdd( aCodsSITEF, { '00000', 'CC', 'OUTRO'                                    } )
aAdd( aCodsSITEF, { '00001', 'CC', 'VISA'                                     } )
aAdd( aCodsSITEF, { '00002', 'CC', 'MASTERCARD'                               } )
aAdd( aCodsSITEF, { '00003', 'CC', 'DINERS'                                   } )
aAdd( aCodsSITEF, { '00004', 'CC', 'AMEX'                                     } )
aAdd( aCodsSITEF, { '00005', 'CC', 'SOLLO'                                    } )
aAdd( aCodsSITEF, { '00006', 'CC', 'SIDECARD'                                 } )
aAdd( aCodsSITEF, { '00007', 'CC', 'PRIVATE LABEL'                            } )
aAdd( aCodsSITEF, { '00008', 'CC', 'REDESHOP'                                 } )
aAdd( aCodsSITEF, { '00009', 'CC', 'PAO DE ACUCAR'                            } )
aAdd( aCodsSITEF, { '00010', 'CC', 'FININVEST'                                } )
aAdd( aCodsSITEF, { '00011', 'CC', 'JCB'                                      } )
aAdd( aCodsSITEF, { '00012', 'CC', 'HIPERCARD'                                } )
aAdd( aCodsSITEF, { '00013', 'CC', 'AURA'                                     } )
aAdd( aCodsSITEF, { '00014', 'CC', 'LOSANGO'                                  } )
aAdd( aCodsSITEF, { '00015', 'CC', 'SOROCRED'                                 } )
aAdd( aCodsSITEF, { '00030', 'CC', 'CABAL'                                    } )
aAdd( aCodsSITEF, { '00031', 'CC', 'ELO'                                      } )
aAdd( aCodsSITEF, { '00033', 'CC', 'POLICARD'                                 } )
aAdd( aCodsSITEF, { '00035', 'CC', 'BANESCARD'                                } )
aAdd( aCodsSITEF, { '00038', 'CC', 'CETELEM'                                  } )
aAdd( aCodsSITEF, { '00041', 'CC', 'SICREDI'                                  } )
aAdd( aCodsSITEF, { '00043', 'CC', 'COOPERCRED'                               } )
aAdd( aCodsSITEF, { '00045', 'CC', 'A VISTA'                                  } )
aAdd( aCodsSITEF, { '00057', 'CC', 'CREDISYSTEM'                              } )
aAdd( aCodsSITEF, { '00058', 'CC', 'BANPARA'                                  } )
aAdd( aCodsSITEF, { '00060', 'CC', 'AMAZONCARD'                               } )
aAdd( aCodsSITEF, { '00061', 'CC', 'YAMADA'                                   } )
aAdd( aCodsSITEF, { '00062', 'CC', 'GOIASCARD'                                } )
aAdd( aCodsSITEF, { '00063', 'CC', 'CREDPAR'                                  } )
aAdd( aCodsSITEF, { '00064', 'CC', 'BOTICARIO'                                } )
aAdd( aCodsSITEF, { '00065', 'CC', 'ASCARD'                                   } )
aAdd( aCodsSITEF, { '00066', 'CC', 'JETPAR'                                   } )
aAdd( aCodsSITEF, { '00067', 'CC', 'MAXXCARD'                                 } )
aAdd( aCodsSITEF, { '00068', 'CC', 'GARANTIDO'                                } )
aAdd( aCodsSITEF, { '00069', 'CC', 'AMAZON PRIME'                             } )
aAdd( aCodsSITEF, { '00070', 'CC', 'CREDZ'                                    } )
aAdd( aCodsSITEF, { '00071', 'CC', 'CREDISHOP'                                } )

//---------------------
//Bandeiras do Tipo CD
//---------------------
//             Codigo SiTef, "CD", Descricao
aAdd( aCodsSITEF, { '00000', 'CD', 'DEBITO GENERICO'                          } )
aAdd( aCodsSITEF, { '00001', 'CD', 'VOUCHER GENERICO'                         } )
aAdd( aCodsSITEF, { '00003', 'CD', 'GIFT PRE-PAGO'                            } )
aAdd( aCodsSITEF, { '10001', 'CD', 'TICKET'                                   } )
aAdd( aCodsSITEF, { '10002', 'CD', 'VISAVALE'                                 } )
aAdd( aCodsSITEF, { '10003', 'CD', 'SODEXO'                                   } )
aAdd( aCodsSITEF, { '10004', 'CD', 'NUTRICASH'                                } )
aAdd( aCodsSITEF, { '10005', 'CD', 'GREENCARD'                                } )
aAdd( aCodsSITEF, { '10006', 'CD', 'PLANVALE'                                 } )
aAdd( aCodsSITEF, { '10007', 'CD', 'BANQUET'                                  } )
aAdd( aCodsSITEF, { '10008', 'CD', 'VEROCHEQUE'                               } )
aAdd( aCodsSITEF, { '10009', 'CD', 'SAPORE'                                   } )
aAdd( aCodsSITEF, { '10010', 'CD', 'BNB CLUBE'                                } )
aAdd( aCodsSITEF, { '10011', 'CD', 'VALECARD'                                 } )
aAdd( aCodsSITEF, { '10012', 'CD', 'CABAL'                                    } )
aAdd( aCodsSITEF, { '10013', 'CD', 'ELO'                                      } )
aAdd( aCodsSITEF, { '10014', 'CD', 'DISCOVERY'                                } )
aAdd( aCodsSITEF, { '10015', 'CD', 'GOODCARD'                                 } )
aAdd( aCodsSITEF, { '10016', 'CD', 'POLICARD'                                 } )
aAdd( aCodsSITEF, { '10017', 'CD', 'CARDSYSTEM'                               } )
aAdd( aCodsSITEF, { '10018', 'CD', 'BONUS CBA'                                } )
aAdd( aCodsSITEF, { '10019', 'CD', 'ALELO'                                    } )
aAdd( aCodsSITEF, { '10020', 'CD', 'BANESCARD'                                } )
aAdd( aCodsSITEF, { '10021', 'CD', 'ALELO (REFEICAO)'                         } )
aAdd( aCodsSITEF, { '10022', 'CD', 'ALELO (ALIMENTACAO)'                      } )
aAdd( aCodsSITEF, { '10023', 'CD', 'ALELO (CULTURA)'                          } )
aAdd( aCodsSITEF, { '10024', 'CD', 'TICKET (REFEICAO)'                        } )
aAdd( aCodsSITEF, { '10025', 'CD', 'TICKET (ALIMENTACAO)'                     } )
aAdd( aCodsSITEF, { '10026', 'CD', 'TICKET (PARCEIRO)'                        } )
aAdd( aCodsSITEF, { '10027', 'CD', 'TICKET (CULTURA)'                         } )
aAdd( aCodsSITEF, { '10028', 'CD', 'SODEXO (REFEICAO)'                        } )
aAdd( aCodsSITEF, { '10029', 'CD', 'SODEXO (ALIMENTACAO)'                     } )
aAdd( aCodsSITEF, { '10030', 'CD', 'SODEXO (GIFT)'                            } )
aAdd( aCodsSITEF, { '10031', 'CD', 'SODEXO (PREMIUM)'                         } )
aAdd( aCodsSITEF, { '10032', 'CD', 'SODEXO (CULTURA)'                         } )
aAdd( aCodsSITEF, { '10033', 'CD', 'SODEXO (COMBUSTIVEL)'                     } )
aAdd( aCodsSITEF, { '10051', 'CD', 'PLANVALE (CULTURA)'                       } )
aAdd( aCodsSITEF, { '10053', 'CD', 'NUTRICASH (CULTURA)'                      } )
aAdd( aCodsSITEF, { '10054', 'CD', 'TICKET (COMBUSTIVEL)'                     } )
aAdd( aCodsSITEF, { '10055', 'CD', 'VALECARD (CULTURA)'                       } )
aAdd( aCodsSITEF, { '20001', 'CD', 'MAESTRO'                                  } )
aAdd( aCodsSITEF, { '20002', 'CD', 'VISA ELECTRON'                            } )
aAdd( aCodsSITEF, { '20003', 'CD', 'CABAL'                                    } )
aAdd( aCodsSITEF, { '20012', 'CD', 'CABAL'                                    } )
aAdd( aCodsSITEF, { '20013', 'CD', 'ELO'                                      } )
aAdd( aCodsSITEF, { '20032', 'CD', 'ELO'                                      } )
aAdd( aCodsSITEF, { '20034', 'CD', 'POLICARD'                                 } )
aAdd( aCodsSITEF, { '20036', 'CD', 'BANESCARD'                                } )
aAdd( aCodsSITEF, { '10037', 'CD', 'SOROCRED'                                 } )
aAdd( aCodsSITEF, { '20037', 'CD', 'HIPERCARD'                                } )
aAdd( aCodsSITEF, { '10039', 'CD', 'VALEMULTI'                                } )
aAdd( aCodsSITEF, { '10040', 'CD', 'VALEFROTA'                                } )
aAdd( aCodsSITEF, { '20042', 'CD', 'SICREDI'                                  } )
aAdd( aCodsSITEF, { '10044', 'CD', 'COOPERCRED'                               } )
aAdd( aCodsSITEF, { '10046', 'CD', 'VALE FACIL'                               } )
aAdd( aCodsSITEF, { '10047', 'CD', 'VR (REFEICAO)'                            } )
aAdd( aCodsSITEF, { '10048', 'CD', 'VR (ALIMENTACAO)'                         } )
aAdd( aCodsSITEF, { '10049', 'CD', 'VR (COMBUSTIVEL)'                         } )
aAdd( aCodsSITEF, { '10050', 'CD', 'VR (CULTURA)'                             } )
aAdd( aCodsSITEF, { '10052', 'CD', 'BANRISUL (CULTURA)'                       } )
aAdd( aCodsSITEF, { '20059', 'CD', 'BANPARA'                                  } )
aAdd( aCodsSITEF, { '10070', 'CD', 'VR (BENEFICIO)'                           } )
aAdd( aCodsSITEF, { '10071', 'CD', 'PLANVALE (BENEFICIO)'                     } )
aAdd( aCodsSITEF, { '20071', 'CD', 'SOROCRED'                                 } )
aAdd( aCodsSITEF, { '10072', 'CD', 'PLANVALE (ALIMENTACAO)'                   } )
aAdd( aCodsSITEF, { '10073', 'CD', 'PLANVALE (REFEICAO)'                      } )
aAdd( aCodsSITEF, { '10074', 'CD', 'PLANVALE (COMBUSTIVEL)'                   } )
aAdd( aCodsSITEF, { '10075', 'CD', 'PLANVALE (FARMACIA)'                      } )

aAdd( aRet, cTexto )

DbSelectArea("MDE")
MDE->(dbSetOrder(1))
For nCont := 1 To Len(aCodsSITEF)
	MDE->(RecLock("MDE",.T.))
	MDE->MDE_FILIAL	:= xFilial("MDE")
	MDE->MDE_CODIGO	:= CriaVar("MDE_CODIGO")	
	MDE->MDE_DESC	:= aCodsSITEF[nCont,3]
	MDE->MDE_TIPO	:= aCodsSITEF[nCont,2]
	MDE->MDE_CODSIT	:= aCodsSITEF[nCont,1]
	MDE->(MsUnlock())
		
	cTexto := STR0008 + ": " + MDE->MDE_CODIGO + " - " + STR0009 + ": " + AllTrim(MDE->MDE_DESC) //"Incluido codigo"###": "###" - "###"Administradora"###": "
	
	aAdd( aRet, cTexto )

Next nCont

While (GetSX8Len() > nSaveSx8)
	ConfirmSx8()
End

cTexto := STR0010 //"Fim da cria��o dos registros na tabela MDE"
aAdd( aRet, cTexto )
                 
Return aRet

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���Programa  �LjAjustMDE� Autor �   Varejo           � Data �  02/02/2016   ���
���������������������������������������������������������������������������͹��
���Descricao � Ajusta dados da tabela MDE.			 						���
���          � Inclui os codigos de retorno das Redes Autorizadoras do SiTEF���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Function LjAjustMDE()
Local aArea 	 := GetArea()
Local aAreaMDE 	 := MDE->(GetArea())
Local aCodsSITEF := {} 
Local nCont		 := 0  
Local cTexto     := STR0011 //'Inicio da cria��o dos registros na tabela MDE'      
Local lTemRede 	 := .F. 					//Indica se ja existe codigo de Rede Cadastrado
Local nSaveSx8 	 := GetSx8Len()
Local aRet 		 := {}

INCLUI := .T.

//Somente prossegue com a execucao dessa funcao se ainda nao existir nenhum codigo do Tipo = "RD=Rede"
DbSelectArea("MDE")
MDE->(dbSetOrder(3)) //MDE_FILIAL+MDE_CODSIT+MDE_TIPO
While MDE->(!EoF()) .And. MDE->MDE_FILIAL == xFilial("MDE")
	If MDE->MDE_TIPO == "RD"
		lTemRede := .T.
		Exit
	EndIf
	MDE->(DbSkip())
End

If !lTemRede
	aAdd( aRet, cTexto )
	
	//Manutencao na tabela MDE (Inclusao de Codigos de REDE). Tipo = "RD=Rede"
	//             Codigo SiTef, "RD", Descricao
	aAdd( aCodsSITEF, { '00000', 'RD', 'OUTRA'                                    } )
	aAdd( aCodsSITEF, { '00001', 'RD', 'TECBAN'                                   } )
	aAdd( aCodsSITEF, { '00002', 'RD', 'ITAU'                                     } )
	aAdd( aCodsSITEF, { '00003', 'RD', 'BRADESCO'                                 } )
	aAdd( aCodsSITEF, { '00004', 'RD', 'VISANET - ESPECIFICACAO 200001'           } )
	aAdd( aCodsSITEF, { '00005', 'RD', 'REDECARD'                                 } )
	aAdd( aCodsSITEF, { '00006', 'RD', 'AMEX'                                     } )
	aAdd( aCodsSITEF, { '00007', 'RD', 'SOLLO'                                    } )
	aAdd( aCodsSITEF, { '00008', 'RD', 'E CAPTURE'                                } )
	aAdd( aCodsSITEF, { '00009', 'RD', 'SERASA'                                   } )
	aAdd( aCodsSITEF, { '00010', 'RD', 'SPC BRASIL'                               } )
	aAdd( aCodsSITEF, { '00011', 'RD', 'SERASA DETALHADO'                         } )
	aAdd( aCodsSITEF, { '00012', 'RD', 'TELEDATA'                                 } )
	aAdd( aCodsSITEF, { '00013', 'RD', 'ACSP'                                     } )
	aAdd( aCodsSITEF, { '00014', 'RD', 'ACSP DETALHADO'                           } )
	aAdd( aCodsSITEF, { '00015', 'RD', 'TECBIZ'                                   } )
	aAdd( aCodsSITEF, { '00016', 'RD', 'CDL DF'                                   } )
	aAdd( aCodsSITEF, { '00017', 'RD', 'REPOM'                                    } )
	aAdd( aCodsSITEF, { '00018', 'RD', 'STANDBY'                                  } )
	aAdd( aCodsSITEF, { '00019', 'RD', 'EDMCARD'                                  } )
	aAdd( aCodsSITEF, { '00020', 'RD', 'CREDICESTA'                               } )
	aAdd( aCodsSITEF, { '00021', 'RD', 'BANRISUL'                                 } )
	aAdd( aCodsSITEF, { '00022', 'RD', 'ACC CARD'                                 } )
	aAdd( aCodsSITEF, { '00023', 'RD', 'CLUBCARD'                                 } )
	aAdd( aCodsSITEF, { '00024', 'RD', 'ACPR'                                     } )
	aAdd( aCodsSITEF, { '00025', 'RD', 'VIDALINK'                                 } )
	aAdd( aCodsSITEF, { '00026', 'RD', 'CCC_WEB'                                  } )
	aAdd( aCodsSITEF, { '00027', 'RD', 'EDIGUAY'                                  } )
	aAdd( aCodsSITEF, { '00028', 'RD', 'CARREFOUR'                                } )
	aAdd( aCodsSITEF, { '00029', 'RD', 'SOFTWAY'                                  } )
	aAdd( aCodsSITEF, { '00030', 'RD', 'MULTICHEQUE'                              } )
	aAdd( aCodsSITEF, { '00031', 'RD', 'TICKET COMBUSTIVEL'                       } )
	aAdd( aCodsSITEF, { '00032', 'RD', 'YAMADA'                                   } )
	aAdd( aCodsSITEF, { '00033', 'RD', 'CITIBANK'                                 } )
	aAdd( aCodsSITEF, { '00034', 'RD', 'INFOCARD'                                 } )
	aAdd( aCodsSITEF, { '00035', 'RD', 'BESC'                                     } )
	aAdd( aCodsSITEF, { '00036', 'RD', 'EMS'                                      } )
	aAdd( aCodsSITEF, { '00037', 'RD', 'CHEQUE CASH'                              } )
	aAdd( aCodsSITEF, { '00038', 'RD', 'CENTRAL CARD'                             } )
	aAdd( aCodsSITEF, { '00039', 'RD', 'DROGARAIA'                                } )
	aAdd( aCodsSITEF, { '00040', 'RD', 'OUTRO SERVICO'                            } )
	aAdd( aCodsSITEF, { '00041', 'RD', 'ACCOR'                                    } )
	aAdd( aCodsSITEF, { '00042', 'RD', 'EPAY GIFT'                                } )
	aAdd( aCodsSITEF, { '00043', 'RD', 'PARATI'                                   } )
	aAdd( aCodsSITEF, { '00044', 'RD', 'TOKORO'                                   } )
	aAdd( aCodsSITEF, { '00045', 'RD', 'COOPERCRED'                               } )
	aAdd( aCodsSITEF, { '00046', 'RD', 'SERVCEL'                                  } )
	aAdd( aCodsSITEF, { '00047', 'RD', 'SOROCRED'                                 } )
	aAdd( aCodsSITEF, { '00048', 'RD', 'VITAL'                                    } )
	aAdd( aCodsSITEF, { '00049', 'RD', 'SAX FINANCEIRA'                           } )
	aAdd( aCodsSITEF, { '00050', 'RD', 'FORMOSA'                                  } )
	aAdd( aCodsSITEF, { '00051', 'RD', 'HIPERCARD'                                } )
	aAdd( aCodsSITEF, { '00052', 'RD', 'TRICARD'                                  } )
	aAdd( aCodsSITEF, { '00053', 'RD', 'CHECK OK'                                 } )
	aAdd( aCodsSITEF, { '00054', 'RD', 'POLICARD'                                 } )
	aAdd( aCodsSITEF, { '00055', 'RD', 'CETELEM CARREFOUR'                        } )
	aAdd( aCodsSITEF, { '00056', 'RD', 'LEADER'                                   } )
	aAdd( aCodsSITEF, { '00057', 'RD', 'CONSORCIO CREDICARD VENEZUELA'            } )
	aAdd( aCodsSITEF, { '00058', 'RD', 'GAZINCRED'                                } )
	aAdd( aCodsSITEF, { '00059', 'RD', 'TELENET'                                  } )
	aAdd( aCodsSITEF, { '00060', 'RD', 'CHEQUE PRE'                               } )
	aAdd( aCodsSITEF, { '00061', 'RD', 'BRASIL CARD'                              } )
	aAdd( aCodsSITEF, { '00062', 'RD', 'EPHARMA'                                  } )
	aAdd( aCodsSITEF, { '00063', 'RD', 'TOTAL'                                    } )
	aAdd( aCodsSITEF, { '00064', 'RD', 'CONSORCIO AMEX VENEZUELA'                 } )
	aAdd( aCodsSITEF, { '00065', 'RD', 'GAX'                                      } )
	aAdd( aCodsSITEF, { '00066', 'RD', 'PERALTA'                                  } )
	aAdd( aCodsSITEF, { '00067', 'RD', 'SERVIDOR PAGAMENTO'                       } )
	aAdd( aCodsSITEF, { '00068', 'RD', 'BANESE'                                   } )
	aAdd( aCodsSITEF, { '00069', 'RD', 'RESOMAQ'                                  } )
	aAdd( aCodsSITEF, { '00070', 'RD', 'SYSDATA'                                  } )
	aAdd( aCodsSITEF, { '00071', 'RD', 'CDL POA'                                  } )
	aAdd( aCodsSITEF, { '00072', 'RD', 'BIGCARD'                                  } )
	aAdd( aCodsSITEF, { '00073', 'RD', 'DTRANSFER'                                } )
	aAdd( aCodsSITEF, { '00074', 'RD', 'VIAVAREJO'                                } )
	aAdd( aCodsSITEF, { '00075', 'RD', 'CHECK EXPRESS'                            } )
	aAdd( aCodsSITEF, { '00076', 'RD', 'GIVEX'                                    } )
	aAdd( aCodsSITEF, { '00077', 'RD', 'VALECARD'                                 } )
	aAdd( aCodsSITEF, { '00078', 'RD', 'PORTAL CARD'                              } )
	aAdd( aCodsSITEF, { '00079', 'RD', 'BANPARA'                                  } )
	aAdd( aCodsSITEF, { '00080', 'RD', 'SOFTNEX'                                  } )
	aAdd( aCodsSITEF, { '00081', 'RD', 'SUPERCARD'                                } )
	aAdd( aCodsSITEF, { '00082', 'RD', 'GETNET'                                   } )
	aAdd( aCodsSITEF, { '00083', 'RD', 'PREVSAUDE'                                } )
	aAdd( aCodsSITEF, { '00084', 'RD', 'BANCO POTTENCIAL'                         } )
	aAdd( aCodsSITEF, { '00085', 'RD', 'SOPHUS'                                   } )
	aAdd( aCodsSITEF, { '00086', 'RD', 'MARISA 2'                                 } )
	aAdd( aCodsSITEF, { '00087', 'RD', 'MAXICRED'                                 } )
	aAdd( aCodsSITEF, { '00088', 'RD', 'BLACKHAWK'                                } )
	aAdd( aCodsSITEF, { '00089', 'RD', 'EXPANSIVA'                                } )
	aAdd( aCodsSITEF, { '00090', 'RD', 'SAS NT'                                   } )
	aAdd( aCodsSITEF, { '00091', 'RD', 'LEADER 2'                                 } )
	aAdd( aCodsSITEF, { '00092', 'RD', 'SOMAR'                                    } )
	aAdd( aCodsSITEF, { '00093', 'RD', 'CETELEM AURA'                             } )
	aAdd( aCodsSITEF, { '00094', 'RD', 'CABAL'                                    } )
	aAdd( aCodsSITEF, { '00095', 'RD', 'CREDSYSTEM'                               } )
	aAdd( aCodsSITEF, { '00096', 'RD', 'BANCO PROVINCIAL'                         } )
	aAdd( aCodsSITEF, { '00097', 'RD', 'CARTESYS'                                 } )
	aAdd( aCodsSITEF, { '00098', 'RD', 'CISA'                                     } )
	aAdd( aCodsSITEF, { '00099', 'RD', 'TRNCENTRE'                                } )
	aAdd( aCodsSITEF, { '00100', 'RD', 'ACPR D'                                   } )
	aAdd( aCodsSITEF, { '00101', 'RD', 'CARDCO'                                   } )
	aAdd( aCodsSITEF, { '00102', 'RD', 'CHECK CHECK'                              } )
	aAdd( aCodsSITEF, { '00103', 'RD', 'CADASA'                                   } )
	aAdd( aCodsSITEF, { '00104', 'RD', 'PRIVATE BRADESCO'                         } )
	aAdd( aCodsSITEF, { '00105', 'RD', 'CREDMAIS'                                 } )
	aAdd( aCodsSITEF, { '00106', 'RD', 'GWCEL'                                    } )
	aAdd( aCodsSITEF, { '00107', 'RD', 'CHECK EXPRESS 2'                          } )
	aAdd( aCodsSITEF, { '00108', 'RD', 'GETNET PBM'                               } )
	aAdd( aCodsSITEF, { '00109', 'RD', 'USECRED'                                  } )
	aAdd( aCodsSITEF, { '00110', 'RD', 'SERV VOUCHER'                             } )
	aAdd( aCodsSITEF, { '00111', 'RD', 'TREDENEXX'                                } )
	aAdd( aCodsSITEF, { '00112', 'RD', 'BONUS PRESENTE CARREFOUR'                 } )
	aAdd( aCodsSITEF, { '00113', 'RD', 'CREDISHOP'                                } )
	aAdd( aCodsSITEF, { '00114', 'RD', 'ESTAPAR'                                  } )
	aAdd( aCodsSITEF, { '00115', 'RD', 'BANCO IBI'                                } )
	aAdd( aCodsSITEF, { '00116', 'RD', 'WORKERCARD'                               } )
	aAdd( aCodsSITEF, { '00117', 'RD', 'TELECHEQUE'                               } )
	aAdd( aCodsSITEF, { '00118', 'RD', 'OBOE'                                     } )
	aAdd( aCodsSITEF, { '00119', 'RD', 'PROTEGE'                                  } )
	aAdd( aCodsSITEF, { '00120', 'RD', 'SERASA CARDS'                             } )
	aAdd( aCodsSITEF, { '00121', 'RD', 'HOTCARD'                                  } )
	aAdd( aCodsSITEF, { '00122', 'RD', 'BANCO PANAMERICANO'                       } )
	aAdd( aCodsSITEF, { '00123', 'RD', 'BANCO MERCANTIL'                          } )
	aAdd( aCodsSITEF, { '00124', 'RD', 'SIGACRED'                                 } )
	aAdd( aCodsSITEF, { '00125', 'RD', 'VISANET - ESPECIFICACAO 4.1'              } )
	aAdd( aCodsSITEF, { '00126', 'RD', 'SPTRANS'                                  } )
	aAdd( aCodsSITEF, { '00127', 'RD', 'PRESENTE MARISA'                          } )
	aAdd( aCodsSITEF, { '00128', 'RD', 'COOPLIFE'                                 } )
	aAdd( aCodsSITEF, { '00129', 'RD', 'BOD'                                      } )
	aAdd( aCodsSITEF, { '00130', 'RD', 'G CARD'                                   } )
	aAdd( aCodsSITEF, { '00131', 'RD', 'TCREDIT'                                  } )
	aAdd( aCodsSITEF, { '00132', 'RD', 'SISCRED'                                  } )
	aAdd( aCodsSITEF, { '00133', 'RD', 'FOXWINCARDS'                              } )
	aAdd( aCodsSITEF, { '00134', 'RD', 'CONVCARD'                                 } )
	aAdd( aCodsSITEF, { '00135', 'RD', 'VOUCHER'                                  } )
	aAdd( aCodsSITEF, { '00136', 'RD', 'EXPAND CARDS'                             } )
	aAdd( aCodsSITEF, { '00137', 'RD', 'ULTRAGAZ'                                 } )
	aAdd( aCodsSITEF, { '00138', 'RD', 'QUALICARD'                                } )
	aAdd( aCodsSITEF, { '00139', 'RD', 'HSBC UK'                                  } )
	aAdd( aCodsSITEF, { '00140', 'RD', 'WAPPA'                                    } )
	aAdd( aCodsSITEF, { '00141', 'RD', 'SQCF'                                     } )
	aAdd( aCodsSITEF, { '00142', 'RD', 'INTELLISYS'                               } )
	aAdd( aCodsSITEF, { '00143', 'RD', 'BOD DEBITO'                               } )
	aAdd( aCodsSITEF, { '00144', 'RD', 'ACCREDITO'                                } )
	aAdd( aCodsSITEF, { '00145', 'RD', 'COMPROCARD'                               } )
	aAdd( aCodsSITEF, { '00146', 'RD', 'ORGCARD'                                  } )
	aAdd( aCodsSITEF, { '00147', 'RD', 'MINASCRED'                                } )
	aAdd( aCodsSITEF, { '00148', 'RD', 'FARMACIA POPULAR'                         } )
	aAdd( aCodsSITEF, { '00149', 'RD', 'FIDELIDADE MAIS'                          } )
	aAdd( aCodsSITEF, { '00150', 'RD', 'ITAU SHOPLINE'                            } )
	aAdd( aCodsSITEF, { '00151', 'RD', 'CDL RIO'                                  } )
	aAdd( aCodsSITEF, { '00152', 'RD', 'FORTCARD'                                 } )
	aAdd( aCodsSITEF, { '00153', 'RD', 'PAGGO'                                    } )
	aAdd( aCodsSITEF, { '00154', 'RD', 'SMARTNET'                                 } )
	aAdd( aCodsSITEF, { '00155', 'RD', 'INTERFARMACIA'                            } )
	aAdd( aCodsSITEF, { '00156', 'RD', 'VALECON'                                  } )
	aAdd( aCodsSITEF, { '00157', 'RD', 'CARTAO EVANGELICO'                        } )
	aAdd( aCodsSITEF, { '00158', 'RD', 'VEGASCARD'                                } )
	aAdd( aCodsSITEF, { '00159', 'RD', 'SCCARD'                                   } )
	aAdd( aCodsSITEF, { '00160', 'RD', 'ORBITALL'                                 } )
	aAdd( aCodsSITEF, { '00161', 'RD', 'ICARDS'                                   } )
	aAdd( aCodsSITEF, { '00162', 'RD', 'FACILCARD'                                } )
	aAdd( aCodsSITEF, { '00163', 'RD', 'FIDELIZE'                                 } )
	aAdd( aCodsSITEF, { '00164', 'RD', 'FINAMAX'                                  } )
	aAdd( aCodsSITEF, { '00165', 'RD', 'BANCO GE'                                 } )
	aAdd( aCodsSITEF, { '00166', 'RD', 'UNIK'                                     } )
	aAdd( aCodsSITEF, { '00167', 'RD', 'TIVIT'                                    } )
	aAdd( aCodsSITEF, { '00168', 'RD', 'VALIDATA'                                 } )
	aAdd( aCodsSITEF, { '00169', 'RD', 'BANESCARD'                                } )
	aAdd( aCodsSITEF, { '00170', 'RD', 'CSU CARREFOUR'                            } )
	aAdd( aCodsSITEF, { '00171', 'RD', 'VALESHOP'                                 } )
	aAdd( aCodsSITEF, { '00172', 'RD', 'SOMAR CARD'                               } )
	aAdd( aCodsSITEF, { '00173', 'RD', 'OMNION'                                   } )
	aAdd( aCodsSITEF, { '00174', 'RD', 'CONDOR'                                   } )
	aAdd( aCodsSITEF, { '00175', 'RD', 'STANDBYDUP'                               } )
	aAdd( aCodsSITEF, { '00176', 'RD', 'BPAG BOLDCRON'                            } )
	aAdd( aCodsSITEF, { '00177', 'RD', 'MARISA SAX SYSIN'                         } )
	aAdd( aCodsSITEF, { '00178', 'RD', 'STARFICHE'                                } )
	aAdd( aCodsSITEF, { '00179', 'RD', 'ACE SEGUROS'                              } )
	aAdd( aCodsSITEF, { '00180', 'RD', 'TOP CARD'                                 } )
	aAdd( aCodsSITEF, { '00181', 'RD', 'GETNET LAC'                               } )
	aAdd( aCodsSITEF, { '00182', 'RD', 'UP SIGHT'                                 } )
	aAdd( aCodsSITEF, { '00183', 'RD', 'MAR'                                      } )
	aAdd( aCodsSITEF, { '00184', 'RD', 'FUNCIONAL CARD'                           } )
	aAdd( aCodsSITEF, { '00185', 'RD', 'PHARMA SYSTEM'                            } )
	aAdd( aCodsSITEF, { '00186', 'RD', 'NEUS'                                     } )
	aAdd( aCodsSITEF, { '00187', 'RD', 'SICREDI'                                  } )
	aAdd( aCodsSITEF, { '00188', 'RD', 'ESCALENA'                                 } )
	aAdd( aCodsSITEF, { '00189', 'RD', 'N SERVICOS'                               } )
	aAdd( aCodsSITEF, { '00190', 'RD', 'CSF CARREFOUR'                            } )
	aAdd( aCodsSITEF, { '00191', 'RD', 'ATP'                                      } )
	aAdd( aCodsSITEF, { '00192', 'RD', 'AVST'                                     } )
	aAdd( aCodsSITEF, { '00193', 'RD', 'ALGORIX'                                  } )
	aAdd( aCodsSITEF, { '00194', 'RD', 'AMEX EMV'                                 } )
	aAdd( aCodsSITEF, { '00195', 'RD', 'COMPREMAX'                                } )
	aAdd( aCodsSITEF, { '00196', 'RD', 'LIBERCARD'                                } )
	aAdd( aCodsSITEF, { '00197', 'RD', 'SEICON'                                   } )
	aAdd( aCodsSITEF, { '00198', 'RD', 'SERASA AUTORIZ CREDITO'                   } )
	aAdd( aCodsSITEF, { '00199', 'RD', 'SMARTN'                                   } )
	aAdd( aCodsSITEF, { '00200', 'RD', 'PLATCO'                                   } )
	aAdd( aCodsSITEF, { '00201', 'RD', 'SMARTNET EMV'                             } )
	aAdd( aCodsSITEF, { '00202', 'RD', 'PROSA MEXICO'                             } )
	aAdd( aCodsSITEF, { '00203', 'RD', 'PEELA'                                    } )
	aAdd( aCodsSITEF, { '00204', 'RD', 'NUTRIK'                                   } )
	aAdd( aCodsSITEF, { '00205', 'RD', 'GOLDENFARMA PBM'                          } )
	aAdd( aCodsSITEF, { '00206', 'RD', 'GLOBAL PAYMENTS'                          } )
	aAdd( aCodsSITEF, { '00207', 'RD', 'ELAVON'                                   } )
	aAdd( aCodsSITEF, { '00208', 'RD', 'CTF'                                      } )
	aAdd( aCodsSITEF, { '00209', 'RD', 'BANESTIK'                                 } )
	aAdd( aCodsSITEF, { '00210', 'RD', 'VISA ARG'                                 } )
	aAdd( aCodsSITEF, { '00211', 'RD', 'AMEX ARG'                                 } )
	aAdd( aCodsSITEF, { '00212', 'RD', 'POSNET ARG'                               } )
	aAdd( aCodsSITEF, { '00213', 'RD', 'AMEX MEXICO'                              } )
	aAdd( aCodsSITEF, { '00214', 'RD', 'ELETROZEMA'                               } )
	aAdd( aCodsSITEF, { '00215', 'RD', 'BARIGUI'                                  } )
	aAdd( aCodsSITEF, { '00216', 'RD', 'SIMEC'                                    } )
	aAdd( aCodsSITEF, { '00217', 'RD', 'SGF'                                      } )
	aAdd( aCodsSITEF, { '00218', 'RD', 'HUG'                                      } )
	aAdd( aCodsSITEF, { '00219', 'RD', 'CARTAO METTACARD'                         } )
	aAdd( aCodsSITEF, { '00220', 'RD', 'DDTOTAL'                                  } )
	aAdd( aCodsSITEF, { '00221', 'RD', 'CARTAO QUALIDADE'                         } )
	aAdd( aCodsSITEF, { '00222', 'RD', 'REDECONV'                                 } )
	aAdd( aCodsSITEF, { '00223', 'RD', 'NUTRICARD'                                } )
	aAdd( aCodsSITEF, { '00224', 'RD', 'DOTZ'                                     } )
	aAdd( aCodsSITEF, { '00225', 'RD', 'PREMIACOES RAIZEN'                        } )
	aAdd( aCodsSITEF, { '00226', 'RD', 'TROCO SOLIDARIO'                          } )
	aAdd( aCodsSITEF, { '00227', 'RD', 'AMBEV SOCIO TORCEDOR'                     } )
	aAdd( aCodsSITEF, { '00228', 'RD', 'SEMPRE'                                   } )
	aAdd( aCodsSITEF, { '00229', 'RD', 'FIRST DATA'                               } )
	aAdd( aCodsSITEF, { '00230', 'RD', 'COCIPA'                                   } )
	aAdd( aCodsSITEF, { '00231', 'RD', 'IBI MEXICO'                               } )
	aAdd( aCodsSITEF, { '00232', 'RD', 'SIANET'                                   } )
	aAdd( aCodsSITEF, { '00233', 'RD', 'SGCARDS'                                  } )
	aAdd( aCodsSITEF, { '00234', 'RD', 'CIAGROUP'                                 } )
	aAdd( aCodsSITEF, { '00235', 'RD', 'FILLIP'                                   } )
	aAdd( aCodsSITEF, { '00236', 'RD', 'CONDUCTOR'                                } )
	aAdd( aCodsSITEF, { '00237', 'RD', 'LTM RAIZEN'                               } )
	aAdd( aCodsSITEF, { '00238', 'RD', 'INCOMM'                                   } )
	aAdd( aCodsSITEF, { '00239', 'RD', 'VISA PASS FIRST'                          } )
	aAdd( aCodsSITEF, { '00240', 'RD', 'CENCOSUD'                                 } )
	aAdd( aCodsSITEF, { '00241', 'RD', 'HIPERLIFE'                                } )
	aAdd( aCodsSITEF, { '00242', 'RD', 'SITPOS'                                   } )
	aAdd( aCodsSITEF, { '00243', 'RD', 'AGT'                                      } )
	aAdd( aCodsSITEF, { '00244', 'RD', 'MIRA'                                     } )
	aAdd( aCodsSITEF, { '00245', 'RD', 'AMBEV 2 SOCIO TORCEDOR'                   } )
	aAdd( aCodsSITEF, { '00246', 'RD', 'JGV'                                      } )
	aAdd( aCodsSITEF, { '00247', 'RD', 'CREDSAT'                                  } )
	aAdd( aCodsSITEF, { '00248', 'RD', 'BRAZILIAN CARD'                           } )
	aAdd( aCodsSITEF, { '00249', 'RD', 'RIACHUELO'                                } )
	aAdd( aCodsSITEF, { '00250', 'RD', 'ITS RAIZEN'                               } )
	aAdd( aCodsSITEF, { '00251', 'RD', 'SIMCRED'                                  } )
	aAdd( aCodsSITEF, { '00252', 'RD', 'BANCRED CARD'                             } )
	aAdd( aCodsSITEF, { '00253', 'RD', 'CONEKTA'                                  } )
	aAdd( aCodsSITEF, { '00254', 'RD', 'SOFTCARD'                                 } )
	aAdd( aCodsSITEF, { '00255', 'RD', 'ECOPAG'                                   } )
	aAdd( aCodsSITEF, { '00256', 'RD', 'C&A AUTOMACAO IBI'                        } )
	aAdd( aCodsSITEF, { '00257', 'RD', 'C&A PARCERIAS BRADESCARD'                 } )
	aAdd( aCodsSITEF, { '00258', 'RD', 'OGLOBA'                                   } )
	aAdd( aCodsSITEF, { '00259', 'RD', 'BANESE VOUCHER'                           } )
	aAdd( aCodsSITEF, { '00260', 'RD', 'RAPP'                                     } )
	aAdd( aCodsSITEF, { '00261', 'RD', 'MONITORA POS'                             } )
	aAdd( aCodsSITEF, { '00262', 'RD', 'SOLLUS'                                   } )
	aAdd( aCodsSITEF, { '00263', 'RD', 'FITCARD'                                  } )
	aAdd( aCodsSITEF, { '00264', 'RD', 'ADIANTI'                                  } )
	aAdd( aCodsSITEF, { '00265', 'RD', 'STONE'                                    } )
	aAdd( aCodsSITEF, { '00266', 'RD', 'DMCARD'                                   } )
	aAdd( aCodsSITEF, { '00267', 'RD', 'ICATU 2'                                  } )
	aAdd( aCodsSITEF, { '00268', 'RD', 'FARMASEG'                                 } )
	aAdd( aCodsSITEF, { '00269', 'RD', 'BIZ'                                      } )
	aAdd( aCodsSITEF, { '00270', 'RD', 'SEMPARAR RAIZEN'                          } )
	aAdd( aCodsSITEF, { '00272', 'RD', 'PBM GLOBAL'                               } )
	aAdd( aCodsSITEF, { '00273', 'RD', 'PAYSMART'                                 } )
	aAdd( aCodsSITEF, { '00275', 'RD', 'ONEBOX'                                   } )
	aAdd( aCodsSITEF, { '00276', 'RD', 'CARTO'                                    } )
	aAdd( aCodsSITEF, { '00277', 'RD', 'WAYUP'                                    } )
	
	DbSelectArea("MDE")
	MDE->(dbSetOrder(3)) //MDE_FILIAL+MDE_CODSIT+MDE_TIPO
	For nCont := 1 To Len(aCodsSITEF)
		
		If !MDE->(dbSeek(xFilial("MDE")+aCodsSITEF[nCont,1]+aCodsSITEF[nCont,2]))
			MDE->(RecLock("MDE",.T.))
			MDE->MDE_FILIAL	:= xFilial("MDE")
			MDE->MDE_CODIGO	:= CriaVar("MDE_CODIGO")
			MDE->MDE_DESC	:= aCodsSITEF[nCont,3]
			MDE->MDE_TIPO	:= aCodsSITEF[nCont,2]
			MDE->MDE_CODSIT	:= aCodsSITEF[nCont,1]
			MDE->(MsUnlock())
			
			cTexto := STR0008 + ": " + MDE->MDE_CODIGO + " - " + STR0012 + ": " + AllTrim(MDE->MDE_DESC) //"Incluido codigo"###": "###" - "###"Rede"###": "
			aAdd( aRet, cTexto )
		EndIf
		
	Next nCont
	
	cTexto := STR0010 //"Fim da cria��o dos registros na tabela MDE"
	aAdd( aRet, cTexto )
	
	While (GetSX8Len() > nSaveSx8)
		ConfirmSx8()
	End
	
EndIf

RestArea(aAreaMDE)
RestArea(aArea)

Return aRet

