#include "Protheus.ch" 


/*
���������������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������������Ŀ��
���Fun��o    � CBMSD2520  � Autor � Anderson Rodrigues Pereira          � Data � 17/10/03 ���
�����������������������������������������������������������������������������������������Ĵ��
���Descri��o � Estorno das informacoes da nota na Ordem de Separacao					  ���
�����������������������������������������������������������������������������������������Ĵ��
���Uso       � SIGAACD - MATA520														  ���
������������������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Descri��o � PLANO DE MELHORIA CONTINUA                                 ���
�������������������������������������������������������������������������Ĵ��
���ITEM PMC  � Responsavel              � Data         |BOPS:             ���
�������������������������������������������������������������������������Ĵ��
���      01  �Flavio Luiz Vicco         �25/05/2006    |00000098409       ���
���      02  �Erike Yuri da Silva       �17/03/2006    |00000094644       ���
���      03  �                          �              |                  ���
���      04  �                          �              |                  ���
���      05  �                          �              |                  ���
���      06  �                          �              |                  ���
���      07  �Flavio Luiz Vicco         �25/05/2006    |00000098409       ���
���      08  �                          �              |                  ���
���      09  �                          �              |                  ���
���      10  �Erike Yuri da Silva       �17/03/2006    |00000094644       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/   
Function CBMSD2520( lRmOrdSep )
Local aArea   := GetArea()
Local aCB7    := CB7->(GetArea())
Local aCB6    := CB6->(GetArea())
Local aCB0    := CB0->(GetArea())
Local aSF2    := SF2->(GetArea())
Local aRetCB0 := {}
Local cRet    := ""
Local lEtqVazia := Empty( SuperGetMv( "MV_ACDCB0" ) )

Default lRmOrdSep := .F.

If !SuperGetMV("MV_CBPE008",.F.,.F.)
	Return
EndIf

If Type("l520AUTO") =="L" .and. l520AUTO
	Return
EndIf

If (AllTrim(FunName()) $ "ACDV168|ACDV170|ACDV177|")// --> Nao executa se chamado pela rotina de geracao de Nota da expedicao
	Return
EndIf

dbSelectArea("CB7")
CB7->(DbSetOrder(1))
If !CB7->(dbSeek(xFilial("CB7")+SD2->D2_ORDSEP))
	Return
EndIf

dbSelectArea("CB6")
CB6->(DBSetOrder(1))

dbSelectArea("CB9")
CB9->(DbSetOrder(1))
If !CB9->(DBSeek(xFilial("CB9")+CB7->CB7_ORDSEP))
	Return
EndIf

While CB9->(!Eof() .and. xFilial("CB9")+CB7->CB7_ORDSEP == CB9_FILIAL+CB9_ORDSEP)
	If CB6->(DbSeek(xFilial("CB6")+CB9->CB9_VOLUME))
		RecLock('CB6',.f.)
		CB6->CB6_NOTA := ''
		CB6->CB6_SERIE:= ''
		CB6->(MsUnlock())
	EndIf
	aRetCB0 := CBRetEti(CB9->CB9_CODETI,'01')
	If Len(aRetCB0) > 0
		aRetCB0[13] := ''
		aRetCB0[14] := ''
		CBGrvEti("01",aRetCB0,CB9->CB9_CODETI)
	EndIf
	RecLock('CB9',.f.)
	CB9->CB9_QTEEBQ := 0.00
	
	If "01*" $ CB7->CB7_TIPEXP .Or. "02*" $ CB7->CB7_TIPEXP // Separacao com Embalagem
		CB9->CB9_STATUS := "2"  // EMBALAGEM FINALIZADA	
	Else
		CB9->CB9_STATUS := "1"  // EM ABERTO
	EndIf
	
	CB9->(MsUnlock())
	CB9->(DbSkip())
EndDo

RecLock('CB7',.F.)
If "03*" $ CB7->CB7_TIPEXP 
	CB7->CB7_STATUS := CBAntProc(CB7->CB7_TIPEXP,"03*")
	CB7->CB7_STATPA := "1"
EndIf	
CB7->CB7_NOTA   := " "
CB7->CB7_SERIE  := " "
CB7->CB7_VOLEMI := " "
CB7->CB7_NFEMIT := " "
CB7->(MsUnlock())

cRet := IIf( ( lRmOrdSep .And. lEtqVazia ), ",SC9->C9_ORDSEP := CriaVar( 'C9_ORDSEP', .F. ) ", ", SC9->C9_ORDSEP := SD2->D2_ORDSEP " )

RestArea(aCB7)
RestArea(aSF2)
RestArea(aCB0)
RestArea(aCB6)
RestArea(aArea)
Return cRet