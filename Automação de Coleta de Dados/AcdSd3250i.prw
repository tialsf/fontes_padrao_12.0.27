#include "rwmake.ch"    


/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������ͻ��
���  Funcao  � CBSD3250I  � Autor � Anderson Rodrigues � Data �Tue  08/11/02���
���������������������������������������������������������������������������͹��
���Descri��o � Impressao das Etiquetas dos PA's no						 	���
���          � apontamento da producao e baixa da requisicao do D4_EMPROC 	���
���������������������������������������������������������������������������͹��
���Uso       � SIGAACD                                                      ���
���������������������������������������������������������������������������ͼ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/
Function CBSD3250I()
Local lRet:= .t. 
Local cImpOP :=  AllTrim(GetNewPar("MV_IMPIPOP","0"))
Local aSD3
Local nRecnoSD3
Local cOP
Local cNumSeq
Local cIdent
Local cArmProc   := GetMvNNR('MV_LOCPROC','99')
Local lAuto      := IIF(Type("aRotAuto") =="U",.F.,.T.)


If !SuperGetMV("MV_CBPE018",.F.,.F.)
	Return
EndIf

If !lAuto
	If Type("l250Auto") != "U"
		lAuto := l250Auto 
	EndIf

	If !lAuto .AND. Type("l681Auto") != "U"
		lAuto := l681Auto 
	EndIf

	If !lAuto .AND. Type("l680Auto") != "U"
		lAuto := l680Auto 
	EndIf
EndIf 

/* Verifica se deve imprimir etiqueta do PA ou do PI no fim do apontamento da producao*/
If  cImpOp $ "1|2" // 1- Pergunta se imprime; 2- Imprime automaticamente sem perguntar		
	If CBImpEti(SD3->D3_COD)
	   ACDI10OP(lAuto)
	EndIf
EndIf 

// Baixa os empenhos do campo SD4->D4_EMPROC -> Saldo Requisitado para armazem de processos

If ExistBlock('ACD250I')
   lRet:= ExecBlock("ACD250I",.F.,.F.)
EndIf

If !lRet
   Return
Endif

SD4->(DbSetOrder(2))
aSD3      := SD3->(GetArea())
nRecnoSD3 := SD3->(Recno())
cOP       := SD3->D3_OP
cNumSeq   := SD3->D3_NUMSEQ
cIdent    := SD3->D3_IDENT
SD3->(DbSetOrder(1))
If SD3->(DbSeek(xFilial("SD3")+cOP))
	While SD3->(!Eof() .AND. D3_FILIAL+D3_OP == xFilial("SD3")+cOP)
		If (SD3->D3_CF == "RE2") .AND. (SD3->D3_NUMSEQ == cNumSeq) .AND. (SD3->D3_IDENT == cIdent) .AND. (Left(SD3->D3_COD,3) != "MOD") .AND. (SD3->D3_LOCAL == cArmProc)
			If SD4->(DbSeek(xFilial("SD4")+SD3->(D3_OP+D3_COD+D3_LOCAL)))
	   		RecLock("SD4",.F.)
			   SD4->D4_EMPROC:= SD4->D4_EMPROC - SD3->D3_QUANT
			   SD4->(MsUnlock())	
			Endif
		Endif
		SD3->(DbSkip())
	Enddo
Endif
RestArea(aSD3)
SD3->(DbGoto(nRecnoSD3))
Return
