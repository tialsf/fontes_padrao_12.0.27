/*
 * BscApplet.java
 *
 * Created on 25 de Novembro de 2003, 15:12
 */
package bsc.applet;

import bsc.core.BscStaticReferences;

/**
 *
 * @author  siga1728
 */
public class BscApplet extends javax.swing.JApplet {

    // Vers�o deseute Client Java
    String BSCClientVersion = "1.06.100224";
    // Espa�o para elementos essenciais
    String language;
    String sessionID;
    String BSCServerVersion;
    bsc.applet.BscLoginPanel loginPanel;
    bsc.applet.BscMainPanel mainPanel;
    bsc.applet.BscMainFrame mainFrame;
    public static final java.awt.Font bscFontPlain = new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11);
    public static final java.awt.Font bscFontBold = new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11);
    public static final java.awt.Color bscColorDisabled = new java.awt.Color(110, 110, 110);
    public static boolean bscPaineis;
    public static String bscEntidade;
    public static String bscId;
    public static String bscComando;

    /** Creates new form BSCApplet */
    public BscApplet() {
    }

    @Override
    public void init() {
        language = String.valueOf(getParameter("LANGUAGE"));
        sessionID = String.valueOf(getParameter("SESSIONID"));
        bscPaineis = (String.valueOf(getParameter("PAINEL")).toLowerCase().equals("false") ? false : true);
        bscEntidade = String.valueOf(getParameter("ENTIDADE"));
        bscId = String.valueOf(getParameter("ID"));
        bscComando = String.valueOf(getParameter("COMANDO"));

        if ("TRUE".equals(String.valueOf(getParameter("DEBUG")))) {
            bsc.core.BscDebug.setDebug(true);
        } else {
            bsc.core.BscDebug.setDebug(false);
        }

        // Seta a linguagem do RPO do Protheus
        java.util.Locale loc;
        if ("SPANISH".equals(language)) {
            loc = new java.util.Locale("es");
        } else if ("ENGLISH".equals(language)) {
            loc = new java.util.Locale("en");
        } else {
            loc = new java.util.Locale("pt", "BR");
        }
        BscStaticReferences.setBscDefaultLocale(loc);
        BscStaticReferences.setBscURL(getCodeBase().toString());

        // BSC SERVER VERSION
        // Cont�m a vers�o do servidor
        BscStaticReferences.setBscServerVersion(String.valueOf(getParameter("BSCVERSION")));
        BSCServerVersion = BscStaticReferences.getBscServerVersion();

        System.out.println("BSCServerVersion" + BSCServerVersion);
        if (!BSCClientVersion.trim().equals(BSCServerVersion.trim())) {
            javax.swing.JOptionPane.showMessageDialog(this,
                    "Cliente e servidor incompat�veis.\nVers�o do Servidor - " + BSCServerVersion + "\nVers�o do Cliente - " + BSCClientVersion,
                    java.util.ResourceBundle.getBundle("international", BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"),
                    javax.swing.JOptionPane.OK_OPTION);
            this.resetByVersion();
            return;
        }

        // BSC APPLET
        // Cont�m o applet chave do BSC
        BscStaticReferences.setBscApplet(this);

        // DATA CONTROLLER
        // Cont�m todo o protocolo de comunica��o do BSC
        BscStaticReferences.setBscDataController(new bsc.core.BscDataController());

        // THEME default
        try {
            javax.swing.plaf.metal.MetalLookAndFeel.setCurrentTheme(new bsc.applet.themes.BscDefaultTheme());
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        //javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", BscStaticReferences.getBscDefaultLocale()).getString("BscApplet_00001"));
        }

        // Fontes
        javax.swing.UIDefaults uiDefaults = javax.swing.UIManager.getDefaults();
        uiDefaults.put("Label.font", bscFontPlain);

        uiDefaults.put("CheckBox.font", bscFontPlain);
        uiDefaults.put("CheckBoxMenuItem.font", bscFontPlain);

        uiDefaults.put("RadioButton.font", bscFontPlain);
        uiDefaults.put("RadioButtonMenuItem.font", bscFontPlain);

        uiDefaults.put("Tree.font", bscFontPlain);
        uiDefaults.put("FormattedTextField.font", bscFontPlain);
        uiDefaults.put("ColorChooser.font", bscFontPlain);
        uiDefaults.put("ToggleButton.font", bscFontPlain);
        uiDefaults.put("TextArea.font", bscFontPlain);
        uiDefaults.put("Menu.font", bscFontPlain);
        uiDefaults.put("Spinner.font", bscFontPlain);
        uiDefaults.put("TextField.font", bscFontPlain);
        uiDefaults.put("OptionPane.font", bscFontPlain);
        uiDefaults.put("MenuBar.font", bscFontPlain);
        uiDefaults.put("Button.font", bscFontPlain);
        uiDefaults.put("PasswordField.font", bscFontPlain);
        uiDefaults.put("InternalFrame.titleFont", bscFontBold);
        uiDefaults.put("MenuItem.font", bscFontPlain);
        uiDefaults.put("ToolTip.font", bscFontPlain);
        uiDefaults.put("List.font", bscFontPlain);
        uiDefaults.put("EditorPane.font", bscFontPlain);
        uiDefaults.put("Table.font", bscFontPlain);
        uiDefaults.put("TableHeader.font", bscFontPlain);
        uiDefaults.put("TabbedPane.font", bscFontBold);
        uiDefaults.put("TextPane.font", bscFontPlain);
        uiDefaults.put("PopupMenu.font", bscFontPlain);
        uiDefaults.put("TitledBorder.font", bscFontPlain);

        uiDefaults.put("ComboBox.font", bscFontPlain);
        uiDefaults.put("ComboBox.disabledForeground", bscColorDisabled);

        //uiDefaults.put("TextArea.background", uiDefaults.get("Label.background"));
        //uiDefaults.put("TextArea.foreground", uiDefaults.get("TextPane.foreground"));
        uiDefaults.put("FormattedTextField.inactiveForeground", bscColorDisabled);
        uiDefaults.put("EditorPane.inactiveForeground", bscColorDisabled);
        uiDefaults.put("TextField.inactiveForeground", bscColorDisabled);
        uiDefaults.put("CheckBoxMenuItem.inactiveForeground", bscColorDisabled);
        uiDefaults.put("CheckBox.disabledText", bscColorDisabled);
        uiDefaults.put("TextArea.inactiveForeground", bscColorDisabled);
        uiDefaults.put("Label.disabledForeground", bscColorDisabled);
        uiDefaults.put("ComboBox.disabledForeground", bscColorDisabled);
        uiDefaults.put("RadioButton.disabledText", bscColorDisabled);
        //uiDefaults.put("Label.inactiveForeground", new java.awt.Color(95, 95, 95));
        //uiDefaults.put("TextPane.inacti0veForeground", new java.awt.Color(0, 0, 0));

        javax.swing.JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.JDialog.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.updateComponentTreeUI(this);
    }

    // Forma padr�o de inicar o mainPanel
    void initMainPanel() {
        try {
            mainPanel = new bsc.applet.BscMainPanel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Forma padr�o de inicar o bsc em frame
    void initMainFrame() {
        try {
            mainFrame = new bsc.applet.BscMainFrame(mainPanel);
            mainFrame.setBounds(16, 16, 680, 540);
            mainFrame.setExtendedState(BscMainFrame.NORMAL);
            mainFrame.validate();
            mainFrame.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Painel principal
        mainPanel.setBscMainFrame(mainFrame);
    }

    // Forma padr�o de reestartar o bsc
    public void resetByVersion() {
        try {
            this.getAppletContext().showDocument(new java.net.URL(this.getCodeBase().toString()));
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // Forma padr�o de reestartar o bsc
    public void resetApplet() {
        try {
            BscStaticReferences.getBscFormController().closeAll();
            if (mainFrame != null) {
                mainFrame.hide();
            }
            this.getAppletContext().showDocument(new java.net.URL(this.getCodeBase().toString()));
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // Mostrar a tela de "sobre"
    public void showAbout() {
        try {
            this.getAppletContext().showDocument(new java.net.URL(this.getCodeBase().toString() + "/bscsobre.apw"), "_blank");
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void initLogin() {
        if (loginPanel == null) {
            loginPanel = new bsc.applet.BscLoginPanel(this, String.valueOf(getParameter("USUARIO")), String.valueOf(getParameter("SENHA")));
        }
        getContentPane().removeAll();
        getContentPane().add(loginPanel, java.awt.BorderLayout.CENTER);
    }

    /** Getter for property sessionID.
     * @return Value of property sessionID.
     *
     */
    public java.lang.String getSessionID() {
        return sessionID;
    }

    /** Setter for property sessionID.
     * @param sessionID New value of property sessionID.
     *
     */
    public void setSessionID(java.lang.String sessionID) {
        this.sessionID = sessionID;
    }
}
