/*
 * JPanel.java
 *
 * Created on 11 de Novembro de 2003, 14:51
 */
package bsc.applet;

import bsc.core.BscFormController;
import bsc.xml.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import java.awt.event.ItemEvent;
import javax.swing.tree.*;
import java.util.*;
import javax.swing.BorderFactory;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 *
 * @author  siga1728
 */
public class BscMainPanel extends javax.swing.JPanel {

    // temas
    public static final int THEME_DEFAULT = 0;
    // constantes de condi��es da status bar
    public static final int ST_IDLE = 0;
    public static final int ST_CONNECTING = 1;
    //constantes que identificam o tipo da arvore
    public static final String ST_ORGANIZACAO = "ORGANIZACAO";
    public static final String ST_AREATRABALHO = "DESKTOP";
    public static final String ST_USUARIO = "USUARIO";
    // fields
    int theme = THEME_DEFAULT;
    public bsc.swing.BscTree bscTree;
    bsc.applet.BscApplet bscApplet;
    bsc.applet.BscMainFrame bscMainFrame;
    bsc.core.BscImageResources bscImageResources;
    bsc.swing.BscDefaultDialogSystem bscDialogSystem;
    bsc.core.BscDataController bscDataController;
    public bsc.core.BscFormController bscFormControllerUsuarios;
    public bsc.core.BscFormController bscFormControllerOrganizacao;
    public bsc.core.BscFormController bscFormControllerAreaTrabalho;
    private bsc.xml.BIXMLRecord bscXmlTreeSelected; //Armazena o valor quando requisito a �rvore de desktop.
    private bsc.util.BscToolKit oBscToolKit = new bsc.util.BscToolKit();
    private java.util.Hashtable hstDeskEst = new java.util.Hashtable();
    private java.util.Hashtable hstOrganizacao = new java.util.Hashtable();
    private String treeNodeID = new String();
    private String userId = new String();

    /** Creates new form JPanel */
    public BscMainPanel() {
        initComponents();

        setStatusBarCondition(ST_IDLE);
        setSplitPaneDividerColor(jSplitPaneAreaTrabalho, new Color(200, 217, 223));
        setSplitPaneDividerColor(jSplitPaneOrganizacao, new Color(200, 217, 223));
        setSplitPaneDividerColor(jSplitPaneUsuarios, new Color(200, 217, 223));
        
        // se for modo de exibi��o somente em paineis
        if (BscApplet.bscPaineis) {
            jTexturePanel2.setVisible(false);
            jScrollPane4.setVisible(false);
            jScrollPane2.setVisible(false);
            jSplitPaneOrganizacao.setDividerSize(-1);
        }

        // Iniciando recursos principais
        bscImageResources = new bsc.core.BscImageResources();
        bscDialogSystem = new bsc.swing.BscDefaultDialogSystem(this);

        // Form Controller
        bscFormControllerAreaTrabalho = new bsc.core.BscFormController(this, jDesktopAreaTrabalho);
        bscFormControllerOrganizacao = new bsc.core.BscFormController(this, jDesktopOrganizacao);
        bscFormControllerUsuarios = new bsc.core.BscFormController(this, jDesktopUsuarios);

        // Referencia estatica dos objetos
        bscApplet = bsc.core.BscStaticReferences.getBscApplet();
        bscDataController = bsc.core.BscStaticReferences.getBscDataController();
        bsc.core.BscStaticReferences.setBscImageResources(bscImageResources);
        bsc.core.BscStaticReferences.setBscDialogSystem(bscDialogSystem);
        bsc.core.BscStaticReferences.setBscFormController(bscFormControllerAreaTrabalho);
        bsc.core.BscStaticReferences.setBscMainPanel(this);

        // Init
        bscTreeUsuarios.removeAll();
        bscTreeUsuarios.setCellRenderer(new bsc.swing.BscTreeCellRenderer(bscImageResources));
        bscTreeAreaTrabalho.removeAll();
        bscTreeAreaTrabalho.setCellRenderer(new bsc.swing.BscTreeCellRenderer(bscImageResources));
        bscTreeOrganizacao.removeAll();
        bscTreeOrganizacao.setCellRenderer(new bsc.swing.BscTreeCellRenderer(bscImageResources));

        // Area Trabalho
        //Seta as opcoes escolhidas pelo usu�rio
        bsc.swing.desktop.BscDesktopFunctions oDeskProperties = new bsc.swing.desktop.BscDesktopFunctions();
        bscFormControllerAreaTrabalho.lastView = oDeskProperties.recDesktop.getInt("DESKVIEWTYPE");
        bscFormControllerOrganizacao.lastView = oDeskProperties.recDesktop.getInt("ORGVIEWTYPE");
        bscFormControllerUsuarios.lastView = oDeskProperties.recDesktop.getInt("USERVIEWTYPE");

        showAreaTrabalho();
        //showOrganizacao();
        if (BscApplet.bscPaineis) {
            try {
                bsc.swing.BscDefaultFrameFunctions frame =
                        bsc.core.BscStaticReferences.getBscFormController().getForm(BscApplet.bscEntidade, BscApplet.bscId, "");
                frame.asJInternalFrame().setMaximizable(true);
            } catch (bsc.core.BscFormControllerException e) {
                bsc.core.BscDebug.println(e.getMessage());
            }
        }

    }

    public bsc.core.BscFormController getBscFormController() {
        return bsc.core.BscStaticReferences.getBscFormController();
    }

    public bsc.applet.BscApplet getBscApplet() {
        return bscApplet;
    }

    public void setBscMainFrame(BscMainFrame bscMainFrame) {
        this.bscMainFrame = bscMainFrame;
    }

    public BscMainFrame getBscMainFrame() {
        return bscMainFrame;
    }

    public void showUsuarios() {
        java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelCentral.getLayout());
        cl.show(jPanelCentral, "USUARIOS");
        bsc.core.BscStaticReferences.setBscFormController(bscFormControllerUsuarios);
        bscTree = (bsc.swing.BscTree) bscTreeUsuarios;
        if (bscTree.getAnchorSelectionPath() == null) {
            loadTree(ST_USUARIO, "0");
            bscTree.setBscTreeType(ST_USUARIO);
            bscTree.expandRow(0);
        }
        alterDesktop(bscFormControllerUsuarios);
    }

    public void showAreaTrabalho() {
        java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelCentral.getLayout());
        cl.show(jPanelCentral, "AREATRABALHO");
        bsc.core.BscStaticReferences.setBscFormController(bscFormControllerAreaTrabalho);
        bscTree = (bsc.swing.BscTree) bscTreeAreaTrabalho;
        if (bscTree.getAnchorSelectionPath() == null) {
            loadTree(ST_AREATRABALHO, "0");
            bscTree.setBscTreeType(ST_AREATRABALHO);
            bscTree.expandRow(0);
        }
        alterDesktop(bscFormControllerAreaTrabalho);
    }

    public String getMenuSelecionado() {
        return bscTree.getBscTreeType();
    }

    public void showOrganizacao(Boolean forceUpdate) {
        java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelCentral.getLayout());
        cl.show(jPanelCentral, ST_ORGANIZACAO);
        bsc.core.BscStaticReferences.setBscFormController(bscFormControllerOrganizacao);
        bscTree = (bsc.swing.BscTree) bscTreeOrganizacao;
        if (bscTree.getAnchorSelectionPath() == null) {
            loadTree(ST_ORGANIZACAO, "0");
            bscTree.setBscTreeType(ST_ORGANIZACAO);
            bscTree.expandRow(0);
        }
        alterDesktop(bscFormControllerOrganizacao);
    }

    public void showMenuPrincipal() {
        java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelMultiMenu.getLayout());
        cl.show(jPanelMultiMenu, "PRINCIPAL");
    }

    public void loadTreeOrganizacao() {
        String organizacaoID = oBscToolKit.getComboValue(hstOrganizacao, cbbFiltroOrganizacao);
        bscTree = (bsc.swing.BscTree) bscTreeOrganizacao;

        loadTree(ST_ORGANIZACAO, organizacaoID);
        bscTree.setBscTreeType(ST_ORGANIZACAO);
        bscTree.setBscTreeItemSelected(organizacaoID);
        bscTree.expandRow(0);
    }

    public void updateTree(BIXMLRecord treeXML, javax.swing.JTree actualBscTree) {
        // Vetores que armazenar�o os n�s vis�veis e selecionados.

        java.util.HashMap visible = new java.util.HashMap();
        java.util.HashMap selected = new java.util.HashMap();

        // Verifica quais n�s s�o atualmente vis�veis.
        TreePath path = null;
        DefaultMutableTreeNode node = null;
        bsc.swing.BscTreeNode myNode = null;

        for (int i = 0; i < actualBscTree.getRowCount(); i++) {
            path = actualBscTree.getPathForRow(i);
            node = (DefaultMutableTreeNode) path.getLastPathComponent();
            myNode = (bsc.swing.BscTreeNode) node.getUserObject();
            visible.put(myNode.getTreeID(), myNode);
        }

        TreePath[] paths = actualBscTree.getSelectionPaths();

        if (paths != null) {
            for (int i = 0; i < paths.length; i++) {
                node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
                myNode = (bsc.swing.BscTreeNode) node.getUserObject();
                selected.put(myNode.getTreeID(), myNode);
            }
        }

        DefaultMutableTreeNode root = createTree(treeXML);

        try {
            actualBscTree.setModel(new DefaultTreeModel(root));
        } catch (Exception e) {
            actualBscTree.setModel(new DefaultTreeModel(
                    new DefaultMutableTreeNode(new String(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00001")))));
            throw new RuntimeException(e.getMessage());
        }

        actualBscTree.collapseRow(0);

        updateTreeStatus(visible, selected, root, actualBscTree);

    }

    public void updateTreeStatus(java.util.HashMap visible, java.util.HashMap selected,
            DefaultMutableTreeNode root, javax.swing.JTree actualBscTree) {
        bsc.swing.BscTreeNode node = (bsc.swing.BscTreeNode) root.getUserObject();
        bsc.swing.BscTreeNode compNode;

        TreeNode[] path = root.getPath();

        if (visible.containsKey(node.getTreeID())) {
            compNode = (bsc.swing.BscTreeNode) visible.get(node.getTreeID());
            if (compNode.equals(node)) {
                actualBscTree.makeVisible(new TreePath(path));
            }
        }

        if (selected.containsKey(node.getTreeID())) {
            compNode = (bsc.swing.BscTreeNode) selected.get(node.getTreeID());
            if (compNode.equals(node)) {
                actualBscTree.addSelectionPath(new TreePath(path));
            }
        }

        for (int i = 0; i < root.getChildCount(); i++) {
            updateTreeStatus(visible, selected,
                    (DefaultMutableTreeNode) root.getChildAt(i), actualBscTree);
        }
    }

    public void updateTreeStatus(java.util.HashMap visible, java.util.HashMap selected,
            DefaultMutableTreeNode root) {
        bsc.swing.BscTreeNode node = (bsc.swing.BscTreeNode) root.getUserObject();

        TreeNode[] path = root.getPath();

        if (visible.containsKey(node.getTreeID())) {
            bscTree.makeVisible(new TreePath(path));
        }

        if (selected.containsKey(node.getTreeID())) {
            bscTree.addSelectionPath(new TreePath(path));
        }

        for (int i = 0; i < root.getChildCount(); i++) {
            updateTreeStatus(visible, selected,
                    (DefaultMutableTreeNode) root.getChildAt(i));
        }
    }

    public void loadTree(String type, String id) {
        BIXMLRecord treeXML = null;
        bscTree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode(
                new String(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00002")))));
        final String localType = String.valueOf(type);
        final String entity = String.valueOf(id);
        javax.swing.JTree threadTree = null;

        if (type.equals(ST_AREATRABALHO)) {
            threadTree = bscTreeAreaTrabalho;
        } else if (type.equals(ST_ORGANIZACAO)) {
            threadTree = bscTreeOrganizacao;
        } else {
            threadTree = bscTreeUsuarios;
        }

        final bsc.core.BscSwingWorker worker = new bsc.core.BscSwingWorker((bsc.swing.BscTree) threadTree) {

            //long inicio = System.currentTimeMillis();
            @Override
            public Object construct() {
                setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                return bscDataController.loadTree(localType, entity);
            }

            @Override
            public void finished() {
                DefaultMutableTreeNode root = null;
                if (bscDataController.getStatus() == bsc.core.BscDataController.BSC_ST_OK) {
                    try {
                        if (localType.equals(ST_AREATRABALHO)) {
                            bscXmlTreeSelected = (bsc.xml.BIXMLRecord) get();
                            oBscToolKit.populateCombo(bscXmlTreeSelected.getBIXMLVector("CMBFILTROS"), "FIL_ID", hstDeskEst, cbbFiltroTree, null, null);
                            cbbFiltroTree.removeItemAt(0);
                            bsc.xml.BIXMLRecord bscXmlNodeActual = getOrgEstTree(bscXmlTreeSelected);
                            jLabelUsuario.setText(bscXmlTreeSelected.getAttributes().getString("USUARIO") + "  ");

                            userId = bscXmlTreeSelected.getAttributes().getString("COD");

                            if (bscXmlNodeActual != null) {
                                getThreadTree().setModel(new DefaultTreeModel(createTree(bscXmlNodeActual)));
                            }
                        } else {
                            bscXmlTreeSelected = (bsc.xml.BIXMLRecord) get();
                            jLabelUsuario.setText(bscXmlTreeSelected.getAttributes().getString("USUARIO") + "  ");
                            getThreadTree().setModel(new DefaultTreeModel(createTree(bscXmlTreeSelected)));
                        }
                    } catch (Exception e) {
                        bscTree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode(new String(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00003")))));
                        throw new RuntimeException(e.getMessage());
                    }
                } else {
                    if (bscDataController.getStatus() == bsc.core.BscDataController.BSC_ST_GENERALERROR) {
                        bscDialogSystem.errorMessage(bscDataController.getErrorMessage());
                    } else {
                        bscDialogSystem.errorMessage(bscDataController.getStatus());
                        if (/*localType.equals("USUARIO") &&*/bscDataController.getStatus() == bsc.core.BscDataController.BSC_ST_NORIGHTS) {
                            showAreaTrabalho();
                        }
                    }
                }
                //System.out.println("BscMainPanel LoadTree finalizada em:  "+ (System.currentTimeMillis()-inicio) + "ms");
                setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
            }
        };
        worker.start();

    }

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        String key = new String((String) treeXML.getKeyNames().next());
        treeNodeID = "0";
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector(key), null);

        return root;
    }

    //Novo este
    public DefaultMutableTreeNode insertRecord(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new bsc.swing.BscTreeNode(vector.getAttributes().getString("NOME"),
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"), treeNodeID));


        for (int i = 0; i < vector.size(); i++) {
            treeNodeID = vector.getMainTag().concat(vector.get(i).getAttributes().getString("ID"));
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree)
            throws BIXMLException {
        DefaultMutableTreeNode child;
        if (record.getTagName().trim().equals("INDICADOR")) {
            child = new DefaultMutableTreeNode(
                    new bsc.swing.BscTreeNode(record.getTagName(),
                    record.getAttributes().getString("ID"),
                    record.getAttributes().getString("NOME"), treeNodeID,
                    record.getAttributes().getString("TIPOIND")));
        } else {
            child = new DefaultMutableTreeNode(
                    new bsc.swing.BscTreeNode(record.getTagName(),
                    record.getAttributes().getString("ID"),
                    record.getAttributes().getString("NOME"), treeNodeID, ""));
        }
        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            String strChave = (String) e.next();

            treeNodeID = record.getTagName().concat(record.getAttributes().getString("ID"));
            if (record.getBIXMLVector(strChave).getAttributes().contains("TIPO")) {
                child = insertGroup(record.getBIXMLVector(strChave), child);
            } else {
                child = insertRecord(record.getBIXMLVector(strChave), child);
            }
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new bsc.swing.BscTreeNode(vector.getAttributes().getString("TIPO"),
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME"), treeNodeID));

        for (int i = 0; i < vector.size(); i++) {
            treeNodeID = vector.getMainTag().concat(vector.get(i).getAttributes().getString("ID"));
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanelFirst = new javax.swing.JPanel();
        jPanelCentral = new javax.swing.JPanel();
        jSplitPaneAreaTrabalho = new javax.swing.JSplitPane();
        jDesktopAreaTrabalho = new javax.swing.JDesktopPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTexturePanel2 = new bsc.beans.JTexturePanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        TOTVSBSC = new javax.swing.JButton();
        cbbFiltroTree = new javax.swing.JComboBox();
        bscTreeAreaTrabalho =  new bsc.swing.BscTree();
        jSplitPaneOrganizacao = new javax.swing.JSplitPane();
        jDesktopOrganizacao = new javax.swing.JDesktopPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        TOTVSBSC1 = new javax.swing.JButton();
        jTexturePanel1 = new bsc.beans.JTexturePanel();
        pnlAtualiza = new javax.swing.JPanel();
        lblAtualiza = new javax.swing.JLabel();
        bscTreeOrganizacao =  new bsc.swing.BscTree();
        cbbFiltroOrganizacao = new javax.swing.JComboBox();
        jSplitPaneUsuarios = new javax.swing.JSplitPane();
        jDesktopUsuarios = new javax.swing.JDesktopPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        TOTVSBSC2 = new javax.swing.JButton();
        jTexturePanel3 = new bsc.beans.JTexturePanel();
        bscTreeUsuarios =  new bsc.swing.BscTree();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        TOTVSLog = new javax.swing.JButton();
        jTexturePanel6 = new bsc.beans.JTexturePanel();
        jPanel4 = new javax.swing.JPanel();
        jPanelStatus = new javax.swing.JPanel();
        jLabelStatusIcone = new javax.swing.JLabel();
        jLabelStatusGauge = new javax.swing.JLabel();
        jLabelStatusMessage = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabelUsuario = new javax.swing.JLabel();
        jButtonUsuarios = new javax.swing.JButton();
        jTexturePanel5 = new bsc.beans.JTexturePanel();
        jPanel1 = new javax.swing.JPanel();
        jPanelMultiMenu = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButtonAreaTrabalho = new javax.swing.JButton();
        jButtonOrganizacao = new javax.swing.JButton();
        jButtonExibir = new javax.swing.JButton();
        jButtonAjuda = new javax.swing.JButton();
        jButtonLogout = new javax.swing.JButton();
        jToolBar1Janelas = new javax.swing.JToolBar();
        jLabelJanelas = new javax.swing.JLabel();
        jButtonCascata = new javax.swing.JButton();
        jButtonLado = new javax.swing.JButton();
        jButtonIconizar = new javax.swing.JButton();
        jToolBar1Ajuda = new javax.swing.JToolBar();
        jLabelAjuda = new javax.swing.JLabel();
        jButtonConteudo = new javax.swing.JButton();
        jButtonPolitica = new javax.swing.JButton();
        jButtonSobre = new javax.swing.JButton();
        jToolBar1View = new javax.swing.JToolBar();
        jLabelExibir = new javax.swing.JLabel();
        radJanela = new javax.swing.JRadioButton();
        radPasta = new javax.swing.JRadioButton();
        jTexturePanel7 = new bsc.beans.JTexturePanel();
        jTexturePanel8 = new bsc.beans.JTexturePanel();
        jTexturePanel9 = new bsc.beans.JTexturePanel();

        setLayout(new java.awt.BorderLayout());

        jPanelFirst.setFocusable(false);
        jPanelFirst.setMinimumSize(new java.awt.Dimension(260, 180));
        jPanelFirst.setPreferredSize(new java.awt.Dimension(760, 580));
        jPanelFirst.setLayout(new java.awt.BorderLayout());

        jPanelCentral.setLayout(new java.awt.CardLayout());

        jSplitPaneAreaTrabalho.setBackground(new java.awt.Color(200, 217, 223));
        jSplitPaneAreaTrabalho.setDividerLocation(200);
        jSplitPaneAreaTrabalho.setDividerSize(6);
        jSplitPaneAreaTrabalho.setForeground(new java.awt.Color(0, 0, 0));
        jSplitPaneAreaTrabalho.setDoubleBuffered(true);
        jSplitPaneAreaTrabalho.setOneTouchExpandable(true);
        jSplitPaneAreaTrabalho.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jSplitPaneAreaTrabalhoMouseEntered(evt);
            }
        });

        jDesktopAreaTrabalho.setBackground(new java.awt.Color(255, 255, 255));
        jDesktopAreaTrabalho.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jDesktopAreaTrabalho.setForeground(java.awt.Color.black);
        jSplitPaneAreaTrabalho.setRightComponent(jDesktopAreaTrabalho);

        jScrollPane3.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N

        jTexturePanel2.setBackground(new java.awt.Color(255, 255, 255));
        jTexturePanel2.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jTexturePanel2.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setForeground(new java.awt.Color(255, 255, 255));
        jPanel8.setAlignmentX(0.0F);
        jPanel8.setAlignmentY(0.0F);
        jPanel8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel8.setEnabled(false);
        jPanel8.setFocusable(false);
        jPanel8.setRequestFocusEnabled(false);
        jPanel8.setLayout(new java.awt.BorderLayout());

        TOTVSBSC.setBackground(new java.awt.Color(0, 0, 0));
        TOTVSBSC.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        TOTVSBSC.setForeground(new java.awt.Color(255, 255, 255));
        TOTVSBSC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo_totvs_bsc.png"))); // NOI18N
        TOTVSBSC.setMnemonic('U');
        TOTVSBSC.setAlignmentX(0.5F);
        TOTVSBSC.setBorder(null);
        TOTVSBSC.setBorderPainted(false);
        TOTVSBSC.setContentAreaFilled(false);
        TOTVSBSC.setFocusPainted(false);
        TOTVSBSC.setFocusable(false);
        TOTVSBSC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TOTVSBSC.setMargin(new java.awt.Insets(0, 0, 0, 0));
        TOTVSBSC.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        TOTVSBSC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TOTVSBSCActionPerformed(evt);
            }
        });
        jPanel8.add(TOTVSBSC, java.awt.BorderLayout.CENTER);

        jPanel2.add(jPanel8, java.awt.BorderLayout.NORTH);

        cbbFiltroTree.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        cbbFiltroTree.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroTreeItemStateChanged(evt);
            }
        });
        cbbFiltroTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbFiltroTreeActionPerformed(evt);
            }
        });
        jPanel2.add(cbbFiltroTree, java.awt.BorderLayout.SOUTH);

        jTexturePanel2.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        bscTreeAreaTrabalho.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Aguarde...");
        bscTreeAreaTrabalho.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        bscTreeAreaTrabalho.setAutoscrolls(true);
        bscTreeAreaTrabalho.setDoubleBuffered(true);
        bscTreeAreaTrabalho.setOpaque(false);
        bscTreeAreaTrabalho.setToggleClickCount(0);
        bscTreeAreaTrabalho.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bscTreeOrganizacaoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                bscTreeAreaTrabalhoMouseEntered(evt);
            }
        });
        jTexturePanel2.add(bscTreeAreaTrabalho, java.awt.BorderLayout.CENTER);

        jScrollPane3.setViewportView(jTexturePanel2);

        jSplitPaneAreaTrabalho.setLeftComponent(jScrollPane3);

        jPanelCentral.add(jSplitPaneAreaTrabalho, "AREATRABALHO");

        jSplitPaneOrganizacao.setBackground(new java.awt.Color(200, 217, 223));
        jSplitPaneOrganizacao.setDividerLocation(200);
        jSplitPaneOrganizacao.setDividerSize(6);
        jSplitPaneOrganizacao.setDoubleBuffered(true);
        jSplitPaneOrganizacao.setOneTouchExpandable(true);
        jSplitPaneOrganizacao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jSplitPaneAreaTrabalhoMouseEntered(evt);
            }
        });

        jDesktopOrganizacao.setBackground(new java.awt.Color(255, 255, 255));
        jDesktopOrganizacao.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jSplitPaneOrganizacao.setRightComponent(jDesktopOrganizacao);

        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel9.setForeground(new java.awt.Color(255, 255, 255));
        jPanel9.setAlignmentX(0.0F);
        jPanel9.setAlignmentY(0.0F);
        jPanel9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel9.setEnabled(false);
        jPanel9.setFocusable(false);
        jPanel9.setRequestFocusEnabled(false);
        jPanel9.setLayout(new java.awt.BorderLayout());

        TOTVSBSC1.setBackground(new java.awt.Color(0, 0, 0));
        TOTVSBSC1.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        TOTVSBSC1.setForeground(new java.awt.Color(255, 255, 255));
        TOTVSBSC1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo_totvs_bsc.png"))); // NOI18N
        TOTVSBSC1.setMnemonic('U');
        TOTVSBSC1.setAlignmentX(0.5F);
        TOTVSBSC1.setBorder(null);
        TOTVSBSC1.setBorderPainted(false);
        TOTVSBSC1.setContentAreaFilled(false);
        TOTVSBSC1.setFocusPainted(false);
        TOTVSBSC1.setFocusable(false);
        TOTVSBSC1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TOTVSBSC1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        TOTVSBSC1.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        TOTVSBSC1.setMinimumSize(null);
        TOTVSBSC1.setPreferredSize(null);
        TOTVSBSC1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TOTVSBSC1ActionPerformed(evt);
            }
        });
        jPanel9.add(TOTVSBSC1, java.awt.BorderLayout.CENTER);

        jPanel5.add(jPanel9, java.awt.BorderLayout.NORTH);

        jTexturePanel1.setBackground(new java.awt.Color(255, 255, 255));
        jTexturePanel1.setLayout(new java.awt.BorderLayout());

        pnlAtualiza.setBackground(new java.awt.Color(255, 255, 255));
        pnlAtualiza.setPreferredSize(new java.awt.Dimension(25, 25));

        lblAtualiza.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblAtualiza.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        lblAtualiza.setToolTipText(bundle.getString("BscAgendadorFrame_00009")); // NOI18N
        lblAtualiza.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblAtualiza.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblAtualiza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAtualizaMouseClicked(evt);
            }
        });
        pnlAtualiza.add(lblAtualiza);

        jTexturePanel1.add(pnlAtualiza, java.awt.BorderLayout.LINE_END);

        bscTreeOrganizacao.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        bscTreeOrganizacao.setAutoscrolls(true);
        bscTreeOrganizacao.setDoubleBuffered(true);
        bscTreeOrganizacao.setOpaque(false);
        bscTreeOrganizacao.setToggleClickCount(0);
        bscTreeOrganizacao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bscTreeOrganizacaoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                bscTreeOrganizacaoMouseEntered(evt);
            }
        });
        jTexturePanel1.add(bscTreeOrganizacao, java.awt.BorderLayout.CENTER);

        cbbFiltroOrganizacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbFiltroOrganizacao.setPreferredSize(new java.awt.Dimension(22, 22));
        cbbFiltroOrganizacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroOrganizacaoItemStateChanged(evt);
            }
        });
        jTexturePanel1.add(cbbFiltroOrganizacao, java.awt.BorderLayout.PAGE_START);

        jPanel5.add(jTexturePanel1, java.awt.BorderLayout.CENTER);

        jScrollPane2.setViewportView(jPanel5);

        jSplitPaneOrganizacao.setLeftComponent(jScrollPane2);

        jPanelCentral.add(jSplitPaneOrganizacao, "ORGANIZACAO");

        jSplitPaneUsuarios.setBackground(new java.awt.Color(200, 217, 223));
        jSplitPaneUsuarios.setDividerLocation(200);
        jSplitPaneUsuarios.setDividerSize(6);
        jSplitPaneUsuarios.setForeground(new java.awt.Color(0, 0, 0));
        jSplitPaneUsuarios.setDoubleBuffered(true);
        jSplitPaneUsuarios.setOneTouchExpandable(true);
        jSplitPaneUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jSplitPaneAreaTrabalhoMouseEntered(evt);
            }
        });

        jDesktopUsuarios.setBackground(new java.awt.Color(255, 255, 255));
        jSplitPaneUsuarios.setRightComponent(jDesktopUsuarios);

        jPanel10.setLayout(new java.awt.BorderLayout());

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel11.setForeground(new java.awt.Color(255, 255, 255));
        jPanel11.setAlignmentX(0.0F);
        jPanel11.setAlignmentY(0.0F);
        jPanel11.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel11.setEnabled(false);
        jPanel11.setFocusable(false);
        jPanel11.setRequestFocusEnabled(false);
        jPanel11.setLayout(new java.awt.BorderLayout());

        TOTVSBSC2.setBackground(new java.awt.Color(0, 0, 0));
        TOTVSBSC2.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        TOTVSBSC2.setForeground(new java.awt.Color(255, 255, 255));
        TOTVSBSC2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo_totvs_bsc.png"))); // NOI18N
        TOTVSBSC2.setMnemonic('U');
        TOTVSBSC2.setAlignmentX(0.5F);
        TOTVSBSC2.setBorder(null);
        TOTVSBSC2.setBorderPainted(false);
        TOTVSBSC2.setContentAreaFilled(false);
        TOTVSBSC2.setFocusPainted(false);
        TOTVSBSC2.setFocusable(false);
        TOTVSBSC2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TOTVSBSC2.setMargin(new java.awt.Insets(0, 0, 0, 0));
        TOTVSBSC2.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        TOTVSBSC2.setMinimumSize(null);
        TOTVSBSC2.setPreferredSize(null);
        TOTVSBSC2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TOTVSBSC2ActionPerformed(evt);
            }
        });
        jPanel11.add(TOTVSBSC2, java.awt.BorderLayout.CENTER);

        jPanel10.add(jPanel11, java.awt.BorderLayout.NORTH);

        jTexturePanel3.setBackground(new java.awt.Color(255, 255, 255));
        jTexturePanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTexturePanel3.setLayout(new java.awt.BorderLayout());

        bscTreeUsuarios.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        bscTreeUsuarios.setAutoscrolls(true);
        bscTreeUsuarios.setDoubleBuffered(true);
        bscTreeUsuarios.setOpaque(false);
        bscTreeUsuarios.setToggleClickCount(0);
        bscTreeUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bscTreeUsuariosMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                bscTreeUsuariosMouseEntered(evt);
            }
        });
        jTexturePanel3.add(bscTreeUsuarios, java.awt.BorderLayout.CENTER);

        jPanel10.add(jTexturePanel3, java.awt.BorderLayout.CENTER);

        jScrollPane4.setViewportView(jPanel10);

        jSplitPaneUsuarios.setLeftComponent(jScrollPane4);

        jPanelCentral.add(jSplitPaneUsuarios, "USUARIOS");

        jPanelFirst.add(jPanelCentral, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(0, 0, 0));
        jPanel6.setLayout(new java.awt.BorderLayout());

        jPanel7.setBackground(new java.awt.Color(0, 0, 0));
        jPanel7.setAlignmentX(0.0F);
        jPanel7.setAlignmentY(0.0F);
        jPanel7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel7.setEnabled(false);
        jPanel7.setFocusable(false);
        jPanel7.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel7.setMinimumSize(new java.awt.Dimension(87, 43));
        jPanel7.setPreferredSize(new java.awt.Dimension(87, 43));
        jPanel7.setRequestFocusEnabled(false);
        jPanel7.setLayout(new java.awt.BorderLayout());

        TOTVSLog.setBackground(new java.awt.Color(0, 0, 0));
        TOTVSLog.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        TOTVSLog.setForeground(new java.awt.Color(255, 255, 255));
        TOTVSLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/form_footer_Left.gif"))); // NOI18N
        TOTVSLog.setMnemonic('U');
        TOTVSLog.setAlignmentX(0.5F);
        TOTVSLog.setBorder(null);
        TOTVSLog.setBorderPainted(false);
        TOTVSLog.setContentAreaFilled(false);
        TOTVSLog.setFocusPainted(false);
        TOTVSLog.setFocusable(false);
        TOTVSLog.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        TOTVSLog.setMargin(new java.awt.Insets(0, 0, 0, 0));
        TOTVSLog.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        TOTVSLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TOTVSLogActionPerformed(evt);
            }
        });
        jPanel7.add(TOTVSLog, java.awt.BorderLayout.CENTER);

        jPanel6.add(jPanel7, java.awt.BorderLayout.WEST);

        jTexturePanel6.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/images/art_menu.gif"))); // NOI18N
        jTexturePanel6.setMinimumSize(new java.awt.Dimension(500, 40));
        jTexturePanel6.setPreferredSize(new java.awt.Dimension(500, 36));
        jTexturePanel6.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBackground(new java.awt.Color(20, 65, 84));
        jPanel4.setAlignmentX(1.0F);
        jPanel4.setAlignmentY(1.0F);
        jPanel4.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel4.setMinimumSize(new java.awt.Dimension(400, 16));
        jPanel4.setPreferredSize(new java.awt.Dimension(200, 70));
        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanelStatus.setBackground(new java.awt.Color(20, 65, 84));
        jPanelStatus.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanelStatus.setLayout(new javax.swing.BoxLayout(jPanelStatus, javax.swing.BoxLayout.LINE_AXIS));

        jLabelStatusIcone.setBackground(new java.awt.Color(20, 65, 84));
        jLabelStatusIcone.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_desconectado.gif"))); // NOI18N
        jLabelStatusIcone.setPreferredSize(new java.awt.Dimension(20, 18));
        jPanelStatus.add(jLabelStatusIcone);

        jLabelStatusGauge.setBackground(new java.awt.Color(255, 255, 255));
        jLabelStatusGauge.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/loading.gif"))); // NOI18N
        jLabelStatusGauge.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jLabelStatusGauge.setPreferredSize(new java.awt.Dimension(53, 16));
        jPanelStatus.add(jLabelStatusGauge);

        jLabelStatusMessage.setBackground(new java.awt.Color(20, 65, 84));
        jLabelStatusMessage.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabelStatusMessage.setForeground(new java.awt.Color(255, 255, 255));
        jLabelStatusMessage.setText(bundle.getString("BscMainPanel_00033")); // NOI18N
        jLabelStatusMessage.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelStatusMessage.setAlignmentX(0.5F);
        jPanelStatus.add(jLabelStatusMessage);

        jPanel4.add(jPanelStatus, java.awt.BorderLayout.WEST);

        jPanel3.setBackground(new java.awt.Color(20, 65, 84));
        jPanel3.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel3.setPreferredSize(new java.awt.Dimension(400, 28));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jLabelUsuario.setBackground(new java.awt.Color(20, 65, 84));
        jLabelUsuario.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jLabelUsuario.setForeground(new java.awt.Color(255, 255, 255));
        jLabelUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_usuario.gif"))); // NOI18N
        jLabelUsuario.setMaximumSize(null);
        jLabelUsuario.setMinimumSize(null);
        jLabelUsuario.setPreferredSize(new java.awt.Dimension(100, 20));
        jLabelUsuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelUsuarioMouseClicked(evt);
            }
        });
        jPanel3.add(jLabelUsuario, java.awt.BorderLayout.CENTER);

        jButtonUsuarios.setBackground(new java.awt.Color(20, 65, 84));
        jButtonUsuarios.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonUsuarios.setForeground(new java.awt.Color(255, 255, 255));
        jButtonUsuarios.setMnemonic('U');
        jButtonUsuarios.setText(bundle.getString("BscMainPanel_00040")); // NOI18N
        jButtonUsuarios.setAlignmentX(0.5F);
        jButtonUsuarios.setBorder(null);
        jButtonUsuarios.setBorderPainted(false);
        jButtonUsuarios.setContentAreaFilled(false);
        jButtonUsuarios.setFocusPainted(false);
        jButtonUsuarios.setFocusable(false);
        jButtonUsuarios.setPreferredSize(new java.awt.Dimension(100, 28));
        jButtonUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUsuariosActionPerformed(evt);
            }
        });
        jPanel3.add(jButtonUsuarios, java.awt.BorderLayout.EAST);

        jPanel4.add(jPanel3, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jTexturePanel6.add(jPanel4, gridBagConstraints);

        jPanel6.add(jTexturePanel6, java.awt.BorderLayout.CENTER);

        jTexturePanel5.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/images/art_menu.gif"))); // NOI18N
        jTexturePanel5.setMinimumSize(new java.awt.Dimension(300, 40));
        jTexturePanel5.setPreferredSize(new java.awt.Dimension(250, 36));
        jTexturePanel5.setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(20, 65, 84));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setAlignmentY(1.0F);
        jPanel1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jPanel1.setMinimumSize(new java.awt.Dimension(300, 25));
        jPanel1.setPreferredSize(new java.awt.Dimension(330, 25));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanelMultiMenu.setBackground(new java.awt.Color(20, 65, 84));
        jPanelMultiMenu.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanelMultiMenu.setPreferredSize(new java.awt.Dimension(250, 22));
        jPanelMultiMenu.setLayout(new java.awt.CardLayout());

        jPanel12.setBackground(new java.awt.Color(20, 65, 84));
        jPanel12.setLayout(new java.awt.BorderLayout());

        jPanel13.setBackground(new java.awt.Color(20, 65, 84));
        jPanel13.setPreferredSize(new java.awt.Dimension(20, 10));
        jPanel12.add(jPanel13, java.awt.BorderLayout.WEST);

        jToolBar1.setBackground(new java.awt.Color(20, 65, 84));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMaximumSize(new java.awt.Dimension(16000, 22));
        jToolBar1.setMinimumSize(new java.awt.Dimension(200, 22));
        jToolBar1.setPreferredSize(new java.awt.Dimension(205, 28));

        jButtonAreaTrabalho.setBackground(new java.awt.Color(0, 0, 0));
        jButtonAreaTrabalho.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonAreaTrabalho.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_ambiente.png"))); // NOI18N
        jButtonAreaTrabalho.setMnemonic('a');
        jButtonAreaTrabalho.setToolTipText(bundle.getString("BscMainPanel_00034")); // NOI18N
        jButtonAreaTrabalho.setAlignmentX(0.5F);
        jButtonAreaTrabalho.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jButtonAreaTrabalho.setBorderPainted(false);
        jButtonAreaTrabalho.setContentAreaFilled(false);
        jButtonAreaTrabalho.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonAreaTrabalho.setFocusPainted(false);
        jButtonAreaTrabalho.setMaximumSize(new java.awt.Dimension(35, 19));
        jButtonAreaTrabalho.setMinimumSize(new java.awt.Dimension(35, 19));
        jButtonAreaTrabalho.setPreferredSize(new java.awt.Dimension(35, 19));
        jButtonAreaTrabalho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAreaTrabalhoActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonAreaTrabalho);

        jButtonOrganizacao.setBackground(new java.awt.Color(0, 0, 0));
        jButtonOrganizacao.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonOrganizacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_organizacao.gif"))); // NOI18N
        jButtonOrganizacao.setMnemonic('O');
        jButtonOrganizacao.setToolTipText(bundle.getString("BscMainPanel_00004")); // NOI18N
        jButtonOrganizacao.setAlignmentX(0.5F);
        jButtonOrganizacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jButtonOrganizacao.setContentAreaFilled(false);
        jButtonOrganizacao.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonOrganizacao.setFocusPainted(false);
        jButtonOrganizacao.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonOrganizacao.setMaximumSize(new java.awt.Dimension(35, 19));
        jButtonOrganizacao.setMinimumSize(new java.awt.Dimension(35, 19));
        jButtonOrganizacao.setPreferredSize(new java.awt.Dimension(35, 19));
        jButtonOrganizacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOrganizacaoActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonOrganizacao);

        jButtonExibir.setBackground(new java.awt.Color(0, 0, 0));
        jButtonExibir.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonExibir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_visao.png"))); // NOI18N
        jButtonExibir.setMnemonic('E');
        jButtonExibir.setToolTipText(bundle.getString("BscMainPanel_00036")); // NOI18N
        jButtonExibir.setAlignmentX(0.5F);
        jButtonExibir.setContentAreaFilled(false);
        jButtonExibir.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonExibir.setFocusPainted(false);
        jButtonExibir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonExibir.setMaximumSize(new java.awt.Dimension(35, 19));
        jButtonExibir.setMinimumSize(new java.awt.Dimension(35, 19));
        jButtonExibir.setPreferredSize(new java.awt.Dimension(35, 19));
        jButtonExibir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExibirActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonExibir);
        jButtonExibir.getAccessibleContext().setAccessibleName("VIEW");

        jButtonAjuda.setBackground(new java.awt.Color(0, 0, 0));
        jButtonAjuda.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_ajuda.png"))); // NOI18N
        jButtonAjuda.setToolTipText(bundle.getString("BscMainPanel_00018")); // NOI18N
        jButtonAjuda.setAlignmentX(0.5F);
        jButtonAjuda.setContentAreaFilled(false);
        jButtonAjuda.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonAjuda.setFocusPainted(false);
        jButtonAjuda.setFocusable(false);
        jButtonAjuda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonAjuda.setMargin(null);
        jButtonAjuda.setMaximumSize(new java.awt.Dimension(35, 19));
        jButtonAjuda.setMinimumSize(new java.awt.Dimension(35, 19));
        jButtonAjuda.setPreferredSize(new java.awt.Dimension(35, 19));
        jButtonAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAjudaActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonAjuda);

        jButtonLogout.setBackground(new java.awt.Color(0, 0, 0));
        jButtonLogout.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        jButtonLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_seguranca.gif"))); // NOI18N
        jButtonLogout.setMnemonic('S');
        jButtonLogout.setToolTipText(bundle.getString("BscMainPanel_00007")); // NOI18N
        jButtonLogout.setAlignmentX(0.5F);
        jButtonLogout.setContentAreaFilled(false);
        jButtonLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonLogout.setFocusable(false);
        jButtonLogout.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonLogout.setMaximumSize(new java.awt.Dimension(35, 19));
        jButtonLogout.setMinimumSize(new java.awt.Dimension(35, 19));
        jButtonLogout.setPreferredSize(new java.awt.Dimension(35, 19));
        jButtonLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLogoutActionPerformed(evt);
            }
        });
        jToolBar1.add(jButtonLogout);

        jPanel12.add(jToolBar1, java.awt.BorderLayout.EAST);

        jPanelMultiMenu.add(jPanel12, "PRINCIPAL");

        jToolBar1Janelas.setBackground(new java.awt.Color(20, 65, 84));
        jToolBar1Janelas.setFloatable(false);
        jToolBar1Janelas.setRollover(true);
        jToolBar1Janelas.setMaximumSize(new java.awt.Dimension(1600, 22));
        jToolBar1Janelas.setMinimumSize(new java.awt.Dimension(1600, 22));
        jToolBar1Janelas.setPreferredSize(new java.awt.Dimension(1600, 22));

        jLabelJanelas.setBackground(new java.awt.Color(0, 0, 0));
        jLabelJanelas.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jLabelJanelas.setForeground(new java.awt.Color(255, 255, 255));
        jLabelJanelas.setText(bundle.getString("BscMainPanel_00029")); // NOI18N
        jToolBar1Janelas.add(jLabelJanelas);

        jButtonCascata.setBackground(new java.awt.Color(0, 0, 0));
        jButtonCascata.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonCascata.setForeground(new java.awt.Color(255, 255, 255));
        jButtonCascata.setMnemonic('C');
        jButtonCascata.setText(bundle.getString("BscMainPanel_00019")); // NOI18N
        jButtonCascata.setContentAreaFilled(false);
        jButtonCascata.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonCascata.setFocusPainted(false);
        jButtonCascata.setFocusable(false);
        jButtonCascata.setPreferredSize(new java.awt.Dimension(100, 28));
        jToolBar1Janelas.add(jButtonCascata);

        jButtonLado.setBackground(new java.awt.Color(0, 0, 0));
        jButtonLado.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonLado.setForeground(new java.awt.Color(255, 255, 255));
        jButtonLado.setMnemonic('L');
        jButtonLado.setText(bundle.getString("BscMainPanel_00020")); // NOI18N
        jButtonLado.setContentAreaFilled(false);
        jButtonLado.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonLado.setFocusPainted(false);
        jButtonLado.setFocusable(false);
        jButtonLado.setPreferredSize(new java.awt.Dimension(100, 28));
        jToolBar1Janelas.add(jButtonLado);

        jButtonIconizar.setBackground(new java.awt.Color(0, 0, 0));
        jButtonIconizar.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonIconizar.setForeground(new java.awt.Color(255, 255, 255));
        jButtonIconizar.setMnemonic('I');
        jButtonIconizar.setText(bundle.getString("BscMainPanel_00021")); // NOI18N
        jButtonIconizar.setContentAreaFilled(false);
        jButtonIconizar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonIconizar.setFocusable(false);
        jButtonIconizar.setPreferredSize(new java.awt.Dimension(100, 28));
        jToolBar1Janelas.add(jButtonIconizar);

        jPanelMultiMenu.add(jToolBar1Janelas, "JANELAS");

        jToolBar1Ajuda.setBackground(new java.awt.Color(20, 65, 84));
        jToolBar1Ajuda.setFloatable(false);
        jToolBar1Ajuda.setRollover(true);
        jToolBar1Ajuda.setMaximumSize(new java.awt.Dimension(1600, 22));
        jToolBar1Ajuda.setMinimumSize(new java.awt.Dimension(1600, 22));
        jToolBar1Ajuda.setPreferredSize(new java.awt.Dimension(1600, 22));

        jLabelAjuda.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jLabelAjuda.setForeground(new java.awt.Color(255, 255, 255));
        jLabelAjuda.setText(bundle.getString("BscMainPanel_00026")); // NOI18N
        jLabelAjuda.setPreferredSize(new java.awt.Dimension(50, 15));
        jToolBar1Ajuda.add(jLabelAjuda);

        jButtonConteudo.setBackground(new java.awt.Color(0, 0, 0));
        jButtonConteudo.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonConteudo.setForeground(new java.awt.Color(255, 255, 255));
        jButtonConteudo.setMnemonic('C');
        jButtonConteudo.setText(bundle.getString("BscMainPanel_00027")); // NOI18N
        jButtonConteudo.setContentAreaFilled(false);
        jButtonConteudo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonConteudo.setFocusPainted(false);
        jButtonConteudo.setFocusable(false);
        jButtonConteudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConteudoActionPerformed(evt);
            }
        });
        jToolBar1Ajuda.add(jButtonConteudo);

        jButtonPolitica.setBackground(new java.awt.Color(0, 0, 0));
        jButtonPolitica.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonPolitica.setForeground(new java.awt.Color(255, 255, 255));
        jButtonPolitica.setMnemonic('P');
        jButtonPolitica.setText(bundle.getString("BscMainPanel_00035")); // NOI18N
        jButtonPolitica.setContentAreaFilled(false);
        jButtonPolitica.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonPolitica.setFocusPainted(false);
        jButtonPolitica.setFocusable(false);
        jButtonPolitica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPoliticaActionPerformed(evt);
            }
        });
        jToolBar1Ajuda.add(jButtonPolitica);
        jButtonPolitica.getAccessibleContext().setAccessibleName("politica");

        jButtonSobre.setBackground(new java.awt.Color(0, 0, 0));
        jButtonSobre.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jButtonSobre.setForeground(new java.awt.Color(255, 255, 255));
        jButtonSobre.setMnemonic('S');
        jButtonSobre.setText(bundle.getString("BscMainPanel_00030")); // NOI18N
        jButtonSobre.setContentAreaFilled(false);
        jButtonSobre.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButtonSobre.setFocusPainted(false);
        jButtonSobre.setFocusable(false);
        jButtonSobre.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSobre.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSobreActionPerformed(evt);
            }
        });
        jToolBar1Ajuda.add(jButtonSobre);

        jPanelMultiMenu.add(jToolBar1Ajuda, "AJUDA");

        jToolBar1View.setBackground(new java.awt.Color(20, 65, 84));
        jToolBar1View.setFloatable(false);
        jToolBar1View.setForeground(new java.awt.Color(255, 255, 255));
        jToolBar1View.setRollover(true);
        jToolBar1View.setMaximumSize(new java.awt.Dimension(1600, 22));
        jToolBar1View.setMinimumSize(new java.awt.Dimension(1600, 22));
        jToolBar1View.setPreferredSize(new java.awt.Dimension(1600, 22));

        jLabelExibir.setBackground(new java.awt.Color(236, 236, 236));
        jLabelExibir.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        jLabelExibir.setForeground(new java.awt.Color(255, 255, 255));
        jLabelExibir.setText(bundle.getString("BscMainPanel_00037")); // NOI18N
        jLabelExibir.setToolTipText(bundle.getString("BscMainPanel_00037")); // NOI18N
        jToolBar1View.add(jLabelExibir);
        jLabelExibir.getAccessibleContext().setAccessibleDescription(bundle.getString("BscMainPanel_00037")); // NOI18N

        radJanela.setBackground(new java.awt.Color(0, 0, 0));
        buttonGroup1.add(radJanela);
        radJanela.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        radJanela.setForeground(new java.awt.Color(255, 255, 255));
        radJanela.setText(bundle.getString("BscMainPanel_00038")); // NOI18N
        radJanela.setToolTipText(bundle.getString("BscMainPanel_00038")); // NOI18N
        radJanela.setContentAreaFilled(false);
        radJanela.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        radJanela.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radJanelaItemStateChanged(evt);
            }
        });
        jToolBar1View.add(radJanela);

        radPasta.setBackground(new java.awt.Color(0, 0, 0));
        buttonGroup1.add(radPasta);
        radPasta.setFont(new java.awt.Font("MS Sans Serif", 0, 11)); // NOI18N
        radPasta.setForeground(new java.awt.Color(255, 255, 255));
        radPasta.setText(bundle.getString("BscMainPanel_00039")); // NOI18N
        radPasta.setToolTipText(bundle.getString("BscMainPanel_00039")); // NOI18N
        radPasta.setContentAreaFilled(false);
        radPasta.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jToolBar1View.add(radPasta);

        jPanelMultiMenu.add(jToolBar1View, "VIEW");

        jPanel1.add(jPanelMultiMenu, java.awt.BorderLayout.EAST);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jTexturePanel5.add(jPanel1, gridBagConstraints);

        jPanel6.add(jTexturePanel5, java.awt.BorderLayout.EAST);

        jPanelFirst.add(jPanel6, java.awt.BorderLayout.PAGE_END);

        jTexturePanel7.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/images/art_menu_top.gif"))); // NOI18N
        jTexturePanel7.setMinimumSize(new java.awt.Dimension(300, 8));
        jTexturePanel7.setPreferredSize(new java.awt.Dimension(200, 8));
        jTexturePanel7.setLayout(new java.awt.GridBagLayout());
        jPanelFirst.add(jTexturePanel7, java.awt.BorderLayout.NORTH);

        jTexturePanel8.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/images/art_menu_sides.gif"))); // NOI18N
        jTexturePanel8.setMinimumSize(new java.awt.Dimension(5, 300));
        jTexturePanel8.setPreferredSize(new java.awt.Dimension(5, 200));
        jTexturePanel8.setLayout(new java.awt.GridBagLayout());
        jPanelFirst.add(jTexturePanel8, java.awt.BorderLayout.WEST);

        jTexturePanel9.setImageTexture(new javax.swing.ImageIcon(getClass().getResource("/images/art_menu_sides.gif"))); // NOI18N
        jTexturePanel9.setMinimumSize(new java.awt.Dimension(5, 200));
        jTexturePanel9.setPreferredSize(new java.awt.Dimension(5, 200));
        jTexturePanel9.setLayout(new java.awt.GridBagLayout());
        jPanelFirst.add(jTexturePanel9, java.awt.BorderLayout.EAST);

        add(jPanelFirst, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

	private void cbbFiltroTreeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroTreeItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                bsc.xml.BIXMLRecord bscXmlNodeActual = getOrgEstTree(bscXmlTreeSelected);
                if (bscXmlNodeActual != null) {
                    bscTree.setModel(new DefaultTreeModel(createTree(bscXmlNodeActual)));
                }
            }
	}//GEN-LAST:event_cbbFiltroTreeItemStateChanged

	private void bscTreeAreaTrabalhoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeAreaTrabalhoMouseEntered
            showMenuPrincipal();
	}//GEN-LAST:event_bscTreeAreaTrabalhoMouseEntered

	private void bscTreeUsuariosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeUsuariosMouseEntered
            showMenuPrincipal();
	}//GEN-LAST:event_bscTreeUsuariosMouseEntered

	private void bscTreeOrganizacaoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeOrganizacaoMouseEntered
            showMenuPrincipal();
	}//GEN-LAST:event_bscTreeOrganizacaoMouseEntered

	private void radJanelaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radJanelaItemStateChanged
            //Muda o tipo de vizualiza��o de pastas para janelas.
            bsc.core.BscFormController formController = bsc.core.BscStaticReferences.getBscFormController();
            setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
            formController.changeView();
            setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
	}//GEN-LAST:event_radJanelaItemStateChanged

	private void jButtonExibirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExibirActionPerformed
            java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelMultiMenu.getLayout());
            cl.show(jPanelMultiMenu, "VIEW");
	}//GEN-LAST:event_jButtonExibirActionPerformed

	private void jButtonPoliticaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPoliticaActionPerformed
            BIXMLRecord record = bsc.core.BscStaticReferences.getBscDataController().loadHelp("BSC", "POLITICA");
            if (bsc.core.BscStaticReferences.getBscDataController().getStatus() == bsc.core.BscDataController.BSC_ST_OK) {
                try {
                    int index;
                    String url = record.getString("URL");
                    java.net.URL helpURL = new java.net.URL(url);
                    bsc.core.BscStaticReferences.getBscApplet().getAppletContext().showDocument(helpURL, "_blank");
                } catch (java.net.MalformedURLException e) {
                    e.printStackTrace();
                }
            }
	}//GEN-LAST:event_jButtonPoliticaActionPerformed

	private void bscTreeUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeUsuariosMouseClicked
            if (evt.getClickCount() == 2) {
                // Continuar daqui.
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTree.getLastSelectedPathComponent();
                bsc.swing.BscTreeNode data = (bsc.swing.BscTreeNode) node.getUserObject();
                try {
                    bsc.swing.BscDefaultFrameFunctions frame =
                            bsc.core.BscStaticReferences.getBscFormController().getForm(data.getType(), data.getID(), data.getName());
                } catch (bsc.core.BscFormControllerException e) {
                    bsc.core.BscDebug.println(e.getMessage());
                }
            }
	}//GEN-LAST:event_bscTreeUsuariosMouseClicked

	private void jSplitPaneAreaTrabalhoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSplitPaneAreaTrabalhoMouseEntered
            showMenuPrincipal();
	}//GEN-LAST:event_jSplitPaneAreaTrabalhoMouseEntered

	private void jButtonAreaTrabalhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAreaTrabalhoActionPerformed
            showAreaTrabalho();
	}//GEN-LAST:event_jButtonAreaTrabalhoActionPerformed

	private void jButtonConteudoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConteudoActionPerformed
            BIXMLRecord record = bsc.core.BscStaticReferences.getBscDataController().loadHelp("BSC", "SIGABSC");
            if (bsc.core.BscStaticReferences.getBscDataController().getStatus() == bsc.core.BscDataController.BSC_ST_OK) {
                try {
                    int index;
                    String url = record.getString("URL");
                    java.net.URL helpURL = new java.net.URL(url);
                    bsc.core.BscStaticReferences.getBscApplet().getAppletContext().showDocument(helpURL, "_blank");
                } catch (java.net.MalformedURLException e) {
                    e.printStackTrace();
                }
            }
	}//GEN-LAST:event_jButtonConteudoActionPerformed

	private void bscTreeOrganizacaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeOrganizacaoMouseClicked
            if (evt.getClickCount() == 2) {
                // Continuar daqui.
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTree.getLastSelectedPathComponent();
                bsc.swing.BscTreeNode data = (bsc.swing.BscTreeNode) node.getUserObject();
                try {
                    bsc.swing.BscDefaultFrameFunctions frame =
                            bsc.core.BscStaticReferences.getBscFormController().getForm(data.getType(), data.getID(), data.getName());
                } catch (bsc.core.BscFormControllerException e) {
                    bsc.core.BscDebug.println(e.getMessage());
                }
            }
	}//GEN-LAST:event_bscTreeOrganizacaoMouseClicked

	private void jButtonAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAjudaActionPerformed
            java.awt.CardLayout cl = (java.awt.CardLayout) (jPanelMultiMenu.getLayout());
            cl.show(jPanelMultiMenu, "AJUDA");
	}//GEN-LAST:event_jButtonAjudaActionPerformed

	private void jButtonOrganizacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOrganizacaoActionPerformed
            showOrganizacao(false);
            loadComboOrg();
	}//GEN-LAST:event_jButtonOrganizacaoActionPerformed

    //Carrega a combo de Organiza��es
    public void loadComboOrg() {
        BIXMLRecord record = bscDataController.loadRecord("0", "LISTA_ORGANIZACAO");
        oBscToolKit.populateCombo(record.getBIXMLVector("ORGANIZACOES"), "ID", hstOrganizacao, cbbFiltroOrganizacao, null, null);
        cbbFiltroOrganizacao.removeItemAt(0);
    }

    public int getComboLine() {
        return cbbFiltroOrganizacao.getSelectedIndex();
    }

    public void setComboLine(int index) {
        cbbFiltroOrganizacao.setSelectedIndex(index);
    }

    public void selectLastLineCombo() {
        cbbFiltroOrganizacao.setSelectedIndex(cbbFiltroOrganizacao.getItemCount() - 1);
    }

	private void jButtonUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUsuariosActionPerformed
            showUsuarios();
	}//GEN-LAST:event_jButtonUsuariosActionPerformed

	private void jButtonSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSobreActionPerformed
            bsc.core.BscStaticReferences.getBscApplet().showAbout();
	}//GEN-LAST:event_jButtonSobreActionPerformed

    private void lblAtualizaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAtualizaMouseClicked
        loadTreeOrganizacao();
}//GEN-LAST:event_lblAtualizaMouseClicked

    private void cbbFiltroOrganizacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroOrganizacaoItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            if (cbbFiltroOrganizacao.getSelectedIndex() > -1) {
                loadTreeOrganizacao();
            }
        }
    }//GEN-LAST:event_cbbFiltroOrganizacaoItemStateChanged

    private void cbbFiltroTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbFiltroTreeActionPerformed
    }//GEN-LAST:event_cbbFiltroTreeActionPerformed

    private void jButtonLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLogoutActionPerformed
        bsc.swing.BscDefaultDialogSystem oQuestion;
        oQuestion = bsc.core.BscStaticReferences.getBscDialogSystem();
        if (oQuestion.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00009")) == javax.swing.JOptionPane.YES_NO_OPTION) {
            getBscApplet().resetApplet();
        }
}//GEN-LAST:event_jButtonLogoutActionPerformed

    private void jLabelUsuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelUsuarioMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabelUsuarioMouseClicked

    private void TOTVSLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TOTVSLogActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_TOTVSLogActionPerformed

    private void TOTVSBSCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TOTVSBSCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TOTVSBSCActionPerformed

    private void TOTVSBSC1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TOTVSBSC1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TOTVSBSC1ActionPerformed

    private void TOTVSBSC2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TOTVSBSC2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TOTVSBSC2ActionPerformed

    public bsc.xml.BIXMLRecord getOrgEstTree(bsc.xml.BIXMLRecord bscXmlTreeSelected) {
        bsc.xml.BIXMLRecord recArvore = null;
        boolean lProxOrg = true;
        String tmpCod;
        int qtdCod = 0,
                orgSel = 0,
                estSel = 0,
                qtdEst = 0,
                qtdOrg = bscXmlTreeSelected.getBIXMLVector("ORGANIZACOES").size();

        //Carregando os c�digos a partir da "combo box"
        StringTokenizer toke = new StringTokenizer(oBscToolKit.getComboValue(hstDeskEst, cbbFiltroTree), "|");
        while (toke.hasMoreElements()) {
            tmpCod = (String) toke.nextElement();
            if (qtdCod == 0) {
                orgSel = Integer.parseInt(tmpCod);
            } else {
                estSel = Integer.parseInt(tmpCod);
            }
            qtdCod++;
        }
        if (qtdOrg > 0) {
            //Faz a busca no XML para localizar os itens que ser�o carregados na "tree".
            for (int indOrg = 0; indOrg <= qtdOrg; indOrg++) {
                if (indOrg == orgSel) {
                    if (bscXmlTreeSelected.getBIXMLVector("ORGANIZACOES").get(indOrg).contains("ESTRATEGIAS")) {
                        qtdEst = bscXmlTreeSelected.getBIXMLVector("ORGANIZACOES").get(indOrg).getBIXMLVector("ESTRATEGIAS").size();
                        for (int indEst = 0; qtdEst >= indEst; indEst++) {
                            if (indEst == estSel) {
                                recArvore = bscXmlTreeSelected.getBIXMLVector("ORGANIZACOES").get(indOrg).getBIXMLVector("ESTRATEGIAS").get(indEst);
                                lProxOrg = false;
                                break;
                            }

                        }
                    }
                    //Controla se devo ler a pr�xima organiza��o.
                    if (!lProxOrg) {
                        break;
                    }
                }
            }
        } else {
            //
            bsc.xml.BIXMLAttributes recAttr = new bsc.xml.BIXMLAttributes();
            recAttr.set("ID", "0");
            recAttr.set("NOME", "Sem dados");
            recArvore = new bsc.xml.BIXMLRecord("ESTRATEGIA", recAttr);

            bsc.xml.BIXMLAttributes vctAttr = new bsc.xml.BIXMLAttributes();
            vctAttr.set("ID", bscXmlTreeSelected.getAttributes().getString("COD"));
            vctAttr.set("NOME", java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00034"));
            vctAttr.set("TIPO", "DESKTOP");

            bsc.xml.BIXMLVector treeVector = new bsc.xml.BIXMLVector("DESKTOPS", "DESKTOP", vctAttr);
            recArvore.set(treeVector);
        }
        return recArvore;
    }

    public void changeTheme(int theme) {
        try {
            this.theme = theme;
            javax.swing.plaf.metal.MetalLookAndFeel.setCurrentTheme(new bsc.applet.themes.BscDefaultTheme());
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getCrossPlatformLookAndFeelClassName());
            javax.swing.SwingUtilities.updateComponentTreeUI(bscApplet);
            if (bscMainFrame != null) {
                javax.swing.SwingUtilities.updateComponentTreeUI(bscMainFrame);
            }
        } catch (Exception e) {
            bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00013"));
        }
    }

    public bsc.core.BscImageResources getImageResources() {
        return bscImageResources;
    }

    public void setStatusBarCondition(int condition) {
        if (condition == ST_IDLE) {
            jLabelStatusMessage.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00014"));
            jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_desconectado.gif")));
            jLabelStatusGauge.setVisible(false);
            jPanelStatus.repaint();
        } else if (condition == ST_CONNECTING) {
            jLabelStatusMessage.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00015"));
            jLabelStatusIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif")));
            jLabelStatusGauge.setVisible(true);
            jPanelStatus.repaint();
        }
    }

    //Usado quando o usu�rio alterna entre os s.
    public void alterDesktop(bsc.core.BscFormController formController) {
        //Desativa os listeners quando muda o desktop.
        java.awt.event.ItemListener[] eventJanelas = radJanela.getItemListeners();
        java.awt.event.ItemListener[] eventPastas = radPasta.getItemListeners();
        for (int iEvento = 0; iEvento < eventJanelas.length; iEvento++) {
            radJanela.removeItemListener(eventJanelas[iEvento]);
        }
        for (int iEvento = 0; iEvento < eventPastas.length; iEvento++) {
            radPasta.removeItemListener(eventPastas[iEvento]);
        }

        //Seta a op��o default do menu.
        if (formController.lastView == BscFormController.ST_PASTA) {
            radJanela.setSelected(false);
            radPasta.setSelected(true);
        } else {
            radJanela.setSelected(true);
            radPasta.setSelected(false);
        }

        //Registra os eventos.
        for (int iEvento = 0; iEvento < eventJanelas.length; iEvento++) {
            radJanela.addItemListener(eventJanelas[iEvento]);
        }
        for (int iEvento = 0; iEvento < eventPastas.length; iEvento++) {
            radPasta.addItemListener(eventPastas[iEvento]);
        }
    }

    private void setSplitPaneDividerColor(JSplitPane splitPane, Color newDividerColor) {
      SplitPaneUI splitUI = splitPane.getUI();
      if (splitUI instanceof BasicSplitPaneUI) { // obviously this will not work if the ui doen't extend Basic...
         BasicSplitPaneDivider div = ((BasicSplitPaneUI) splitUI).getDivider();
         assert div != null;
         Border divBorder = div.getBorder();
         Border newBorder = null;
         Border colorBorder = null;

         class BGBorder implements Border {
            private Color color;
            private final Insets NO_INSETS = new Insets(0, 0, 0, 0);

            private BGBorder(Color color) {
               this.color = color;
            }
            Rectangle r = new Rectangle();
            public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
               g.setColor(color);
               g.fillRect(x, y, width, height);
               if (c instanceof Container) {
                  Container cont = (Container) c;
                  for (int i = 0, n = cont.getComponentCount(); i < n; i++) {
                     Component comp = cont.getComponent(i);
                     comp.getBounds(r);
                     Graphics tmpg = g.create(r.x, r.y, r.width, r.height);
                     comp.paint(tmpg);
                     tmpg.dispose();
                  }
               }
            }

            public Insets getBorderInsets(Component c) {
               return NO_INSETS;
            }

            public boolean isBorderOpaque() {
               return true;
            }
         }

         colorBorder = new BGBorder(newDividerColor);

         if (divBorder == null) {
            newBorder = colorBorder;
         } else {
            newBorder = BorderFactory.createCompoundBorder(divBorder, colorBorder);
         }
         div.setBorder(newBorder);
      }
   }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton TOTVSBSC;
    private javax.swing.JButton TOTVSBSC1;
    private javax.swing.JButton TOTVSBSC2;
    private javax.swing.JButton TOTVSLog;
    public javax.swing.JTree bscTreeAreaTrabalho;
    public javax.swing.JTree bscTreeOrganizacao;
    public javax.swing.JTree bscTreeUsuarios;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbbFiltroOrganizacao;
    private javax.swing.JComboBox cbbFiltroTree;
    private javax.swing.JButton jButtonAjuda;
    private javax.swing.JButton jButtonAreaTrabalho;
    private javax.swing.JButton jButtonCascata;
    private javax.swing.JButton jButtonConteudo;
    private javax.swing.JButton jButtonExibir;
    private javax.swing.JButton jButtonIconizar;
    private javax.swing.JButton jButtonLado;
    private javax.swing.JButton jButtonLogout;
    private javax.swing.JButton jButtonOrganizacao;
    private javax.swing.JButton jButtonPolitica;
    private javax.swing.JButton jButtonSobre;
    private javax.swing.JButton jButtonUsuarios;
    private javax.swing.JDesktopPane jDesktopAreaTrabalho;
    private javax.swing.JDesktopPane jDesktopOrganizacao;
    private javax.swing.JDesktopPane jDesktopUsuarios;
    private javax.swing.JLabel jLabelAjuda;
    private javax.swing.JLabel jLabelExibir;
    private javax.swing.JLabel jLabelJanelas;
    private javax.swing.JLabel jLabelStatusGauge;
    private javax.swing.JLabel jLabelStatusIcone;
    private javax.swing.JLabel jLabelStatusMessage;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelCentral;
    private javax.swing.JPanel jPanelFirst;
    private javax.swing.JPanel jPanelMultiMenu;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    public javax.swing.JSplitPane jSplitPaneAreaTrabalho;
    public javax.swing.JSplitPane jSplitPaneOrganizacao;
    private javax.swing.JSplitPane jSplitPaneUsuarios;
    private bsc.beans.JTexturePanel jTexturePanel1;
    private bsc.beans.JTexturePanel jTexturePanel2;
    private bsc.beans.JTexturePanel jTexturePanel3;
    private bsc.beans.JTexturePanel jTexturePanel5;
    private bsc.beans.JTexturePanel jTexturePanel6;
    private bsc.beans.JTexturePanel jTexturePanel7;
    private bsc.beans.JTexturePanel jTexturePanel8;
    private bsc.beans.JTexturePanel jTexturePanel9;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar1Ajuda;
    private javax.swing.JToolBar jToolBar1Janelas;
    private javax.swing.JToolBar jToolBar1View;
    private javax.swing.JLabel lblAtualiza;
    private javax.swing.JPanel pnlAtualiza;
    public javax.swing.JRadioButton radJanela;
    private javax.swing.JRadioButton radPasta;
    // End of variables declaration//GEN-END:variables
}
