/*
 * BscNavApplet.java
 *
 * Created on 26 de Novembro de 2003, 10:00
 */

package bsc.applet;

/**
 *
 * @author  siga1728
 */
public class BscNavApplet extends bsc.applet.BscApplet {
	
	/** Creates a new instance of BscNavApplet */
	public BscNavApplet() {
	}

	public void init() {
		super.init();
		initMainPanel();
		getContentPane().add(mainPanel, java.awt.BorderLayout.CENTER);
	}
}