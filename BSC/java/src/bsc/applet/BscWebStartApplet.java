/*
 * BscWebstartApplet.java
 *
 * Created on 11 de Novembro de 2003, 16:29
 */

package bsc.applet;

/**
 *
 * @author  siga1728
 */
public class BscWebStartApplet extends BscApplet {
	
	BscLoginPanel loginPanel;
	
	/** Initializes the applet BscWebstartApplet */
	public void init() {
		super.init();
		initLogin();
	}
	
	/** This method is called from within the init() method to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    private void initComponents() {//GEN-BEGIN:initComponents

    }//GEN-END:initComponents

	// Forma padr�o de reestartar o bsc
	public void resetApplet() {
		bsc.core.BscStaticReferences.getBscFormController().closeAll();	
		if(mainFrame!=null) mainFrame.hide();
	}
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
	
}
