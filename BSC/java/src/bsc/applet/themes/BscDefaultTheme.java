package bsc.applet.themes;

import javax.swing.plaf.*;
import javax.swing.plaf.metal.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class BscDefaultTheme extends DefaultMetalTheme {

	public String getName() { return "Blue"; }

	private final ColorUIResource primary1 = new ColorUIResource(56, 64, 60);
	private final ColorUIResource primary2 = new ColorUIResource(66, 98, 133);
	private final ColorUIResource primary3 = new ColorUIResource(236, 236, 236);

//	private final ColorUIResource secondary1 = new ColorUIResource(255, 0, 0);
//	private final ColorUIResource secondary2 = new ColorUIResource(0, 255, 0);
	private final ColorUIResource secondary3 = new ColorUIResource(226, 226, 226);
	
	protected ColorUIResource getPrimary1() { return primary1; }
    protected ColorUIResource getPrimary2() { return primary2; }
    protected ColorUIResource getPrimary3() { return primary3; }
	
//	protected ColorUIResource getSecondary1() { return secondary1; }
//	protected ColorUIResource getSecondary2() { return secondary2; }
	protected ColorUIResource getSecondary3() { return secondary3; }

}
