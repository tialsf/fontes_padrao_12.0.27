/*
 * BIDoubleField.java
 *
 * Created on October 21, 2003, 11:52 AM
 */

package bsc.beans;

import javax.swing.*; 
import javax.swing.text.*; 
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 *
 * @author  siga1996
 */
public class BIFloatField extends javax.swing.JTextField {
    private DecimalFormat decimalFormatter;
	private int decimal = 0;

    public BIFloatField(float value, int columns, int decimalAux) {
       super(columns);
			decimal = decimalAux;
			String pattern = new String( "###################################." );
			for ( int i = 0; i < decimal; i++ )
				pattern = pattern + "0";
			decimalFormatter = new DecimalFormat(pattern);
			setValue(value);
    }

    public float getValue() {
        float retVal = 0;
        try {
					retVal = decimalFormatter.parse(getText()).floatValue();
        } catch (ParseException e) {
        }
        return retVal;
    }

    public void setValue(float value) {
        setText(decimalFormatter.format(value));
    }

    protected Document createDefaultModel() {
        return new BIDoubleDocument();
    }

		protected class BIDoubleDocument extends PlainDocument {
			public void insertString(int offs, String str, AttributeSet a)
					throws BadLocationException {
				char[] source = str.toCharArray();
				char[] result = new char[source.length];
				int j = 0;
				for (int i = 0; i < result.length; i++) {
					if ( Character.isDigit(source[i]) ||
							(decimalFormatter.getDecimalFormatSymbols().getDecimalSeparator() == source[i] ) )
						result[j++] = source[i];
				}
				super.insertString(offs, new String(result, 0, j), a);
			}
		}
}
