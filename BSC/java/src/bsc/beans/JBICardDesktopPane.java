/*
 * JBICardDesktopPane.java
 *
 * Created on September 23, 2003, 10:39 AM
 */

package bsc.beans;

import javax.swing.*;
import java.awt.*;
import bsc.swing.analisys.*;

/**
 *
 * @author  siga1996
 */
public class JBICardDesktopPane extends javax.swing.JDesktopPane
		implements java.awt.print.Printable{
	private java.util.Vector vctFrames = new java.util.Vector();
	private static int FRAME_OFFSET=10;
	private JBICardDesktopManager manager;
	private boolean autoOrganize = false;
	
	public void setAutoOrganize( boolean ao ) {
		autoOrganize = ao;
		//autoOrganizeFrames();
	}
	
	public boolean getAutoOrganize( ) {
		return autoOrganize;
	}
	
	public JBICardDesktopPane() {
		manager=new JBICardDesktopManager(this);
		setDesktopManager(manager);
		setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
	}
	
	public void setBounds(int x, int y, int w, int h) {
		super.setBounds(x,y,w,h);
		checkDesktopSize();
	}
	
	public void removeAll() {
		vctFrames.removeAllElements();
		super.removeAll();
	}
	
	public void autoOrganizeFrames() {
		if ( autoOrganize ) {
			BscCardFrame frame = null;
			if ( vctFrames.size() > 0 ) {
				frame = (BscCardFrame) vctFrames.get(0);
				int width = frame.getWidth();
				int height = frame.getHeight();
				int numCols = (int) ( getScrollPane().getWidth() / ( width + FRAME_OFFSET ) );
				int numLines = (int) Math.round( Math.floor( vctFrames.size() / numCols ) );
				int line = 0,
						column = 0,
						realLine = 10,
						realColumn = 10;
				
				for ( int i = 0; i < vctFrames.size(); i++ ) {
					frame = (BscCardFrame) vctFrames.get(i);
					frame.setOrder( i );
					column = i % numCols;
					line = (int) i / numCols;
					realLine = (line * ( height + FRAME_OFFSET )) + FRAME_OFFSET;
					realColumn = (column * (width + FRAME_OFFSET)) + FRAME_OFFSET;
					frame.setLocation( realColumn, realLine );
				}
				this.repaint();
			}
		}
	}
	
	public Component add(JInternalFrame frame) {
		BscCardFrame cardFrame = (BscCardFrame) frame;
		boolean incluiu = false;
		for ( int i = 0; (i < vctFrames.size()) && (!incluiu); i++ )
			if ( ((BscCardFrame)vctFrames.get(i) ).getOrder() > cardFrame.getOrder() ) {
			vctFrames.add(i, cardFrame );
			incluiu = true;
			}
		
		if (!incluiu)
			vctFrames.add(vctFrames.size(), cardFrame);
		
		Point p;
		int w;
		int h;
		
		Component retval = super.add(frame);
		if ( !autoOrganize ) {
			if ( ( cardFrame.getCardFramePosition().getX() == 0 ) && (cardFrame.getCardFramePosition().getY() ==0 ) ) {
				if (vctFrames.size() != 0) {
					p = ((JInternalFrame)vctFrames.get(0)).getLocation();
					p.x = p.x + FRAME_OFFSET;
					p.y = p.y + FRAME_OFFSET;
				} else {
					p = new Point(0, 0);
				}
			} else
				p = new Point( cardFrame.getCardFramePosition().x,
						cardFrame.getCardFramePosition().y );
			frame.setLocation(p.x, p.y);
		}
		
		this.moveToFront(frame);
		frame.setVisible(true);
		try {
			frame.setSelected(true);
		} catch (java.beans.PropertyVetoException e) {
			frame.toBack();
		}
		
		checkDesktopSize();
		return retval;
	}
	
	public void remove(Component c) {
		super.remove(c);
		checkDesktopSize();
	}
	
	public void moveFrame(JComponent component) {
		BscCardFrame frameMoved = (BscCardFrame) component;
		Point p = frameMoved.getLocation();
		p.x = p.x + ( frameMoved.getWidth() / 2 );
		p.y = p.y + ( frameMoved.getHeight() / 2 );
		vctFrames.remove(component);
		
		int numCols = (int) ( getScrollPane().getWidth() /
				( frameMoved.getWidth() + FRAME_OFFSET ) );
		int line = 0,
				column = 0,
				realLine = frameMoved.getHeight() + FRAME_OFFSET,
				realColumn = frameMoved.getWidth() + FRAME_OFFSET;
		while ( ( p.x > realColumn ) || ( p.y > realLine ) ) {
			if ( p.x > realColumn ) {
				realColumn = realColumn + frameMoved.getWidth() + FRAME_OFFSET;
				column++;
			}
			if ( p.y > realLine ) {
				realLine = realLine + frameMoved.getHeight() + FRAME_OFFSET;
				line++;
			}
		}
		
		if ( column >= numCols ) {
			column = 0;
			line++;
		}
		
		int posVector = numCols * line + column;
		
		if ( posVector >= vctFrames.size() )
			vctFrames.addElement( frameMoved );
		else
			vctFrames.insertElementAt( frameMoved, posVector );
		
		this.autoOrganizeFrames();
	}
	
	/**
	 * Sets all component size properties ( maximum, minimum, preferred)
	 * to the given dimension.
	 */
	public void setAllSize(Dimension d){
		setMinimumSize(d);
		setMaximumSize(d);
		setPreferredSize(d);
	}
	
	/**
	 * Sets all component size properties ( maximum, minimum, preferred)
	 * to the given width and height.
	 */
	public void setAllSize(int width, int height){
		setAllSize(new Dimension(width,height));
	}
	
	public void removeFrame( JInternalFrame jif ) {
		vctFrames.remove( jif );
	}
	
	private void checkDesktopSize() {
		if ( getParent() != null && isVisible() )
			manager.resizeDesktop();
	}
	
	private JScrollPane getScrollPane() {
		if (this.getParent() instanceof JViewport) {
			JViewport viewPort = (JViewport)this.getParent();
			if (viewPort.getParent() instanceof JScrollPane)
				return (JScrollPane)viewPort.getParent();
		}
		return null;
	}
	
	public int print(java.awt.Graphics graphics,java.awt.print.PageFormat pageFormat,int pi) throws java.awt.print.PrinterException {
		if (pi > 0) {
			return(NO_SUCH_PAGE);
		} else {
			java.awt.Graphics2D g2d = (java.awt.Graphics2D)graphics;
			g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY()+50);
			g2d.scale( 0.5, 0.5 );
			disableDoubleBuffering(this);
			this.paint(g2d);
			enableDoubleBuffering(this);
			return(PAGE_EXISTS);
		}
	}	
	
	/**
	 *Desabilita o buffer para as figuras.
	 **/
	private void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
	
	private void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
}

/**
 * Private class used to replace the standard DesktopManager for JDesktopPane.
 * Used to provide scrollbar functionality.
 */
class JBICardDesktopManager extends DefaultDesktopManager {
	private JBICardDesktopPane desktop;
	
	public void closeFrame(JInternalFrame f) {
		desktop.removeFrame( f );
		super.closeFrame(f);
		desktop.autoOrganizeFrames();
	}
	
	public JBICardDesktopManager(JBICardDesktopPane desktop) {
		this.desktop = (JBICardDesktopPane) desktop;
	}
	
	public void endResizingFrame(JComponent f) {
		super.endResizingFrame(f);
		desktop.autoOrganizeFrames();
		resizeDesktop();
	}
	
	public void endDraggingFrame(JComponent f) {
		super.endDraggingFrame(f);
		desktop.moveFrame(f);
		resizeDesktop();
	}
	
	public void setNormalSize() {
		JScrollPane scrollPane=getScrollPane();
		int x = 0;
		int y = 0;
		Insets scrollInsets = getScrollPaneInsets();
		
		if (scrollPane != null) {
			Dimension d = scrollPane.getVisibleRect().getSize();
			if (scrollPane.getBorder() != null) {
				d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right,
						d.getHeight() - scrollInsets.top - scrollInsets.bottom);
			}
			
			d.setSize(d.getWidth() - 20, d.getHeight() - 20);
			desktop.setAllSize(x,y);
			scrollPane.invalidate();
			scrollPane.validate();
		}
	}
	
	private Insets getScrollPaneInsets() {
		JScrollPane scrollPane=getScrollPane();
		if (scrollPane==null)
			return new Insets(0,0,0,0);
		else
			return getScrollPane().getBorder().getBorderInsets(scrollPane);
	}
	
	private JScrollPane getScrollPane() {
		if (desktop.getParent() instanceof JViewport) {
			JViewport viewPort = (JViewport)desktop.getParent();
			if (viewPort.getParent() instanceof JScrollPane)
				return (JScrollPane)viewPort.getParent();
		}
		return null;
	}
	
	protected void resizeDesktop() {
		int x = 0;
		int y = 0;
		JScrollPane scrollPane = getScrollPane();
		Insets scrollInsets = getScrollPaneInsets();
		
		if (scrollPane != null) {
			JInternalFrame allFrames[] = desktop.getAllFrames();
			for (int i = 0; i < allFrames.length; i++) {
				if (allFrames[i].getX()+allFrames[i].getWidth()>x) {
					x = allFrames[i].getX() + allFrames[i].getWidth();
				}
				if (allFrames[i].getY()+allFrames[i].getHeight()>y) {
					y = allFrames[i].getY() + allFrames[i].getHeight();
				}
			}
			Dimension d=scrollPane.getVisibleRect().getSize();
			if (scrollPane.getBorder() != null) {
				d.setSize(d.getWidth() - scrollInsets.left - scrollInsets.right,
						d.getHeight() - scrollInsets.top - scrollInsets.bottom);
			}
			
			if (x <= d.getWidth())
				x = ((int)d.getWidth()) - 20;
			if (y <= d.getHeight())
				y = ((int)d.getHeight()) - 20;
			desktop.setAllSize(x,y);
			scrollPane.invalidate();
			scrollPane.validate();
		}
	}
}