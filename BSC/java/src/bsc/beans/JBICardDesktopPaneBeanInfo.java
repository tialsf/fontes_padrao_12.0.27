package bsc.beans;

import java.beans.*;

public class JBICardDesktopPaneBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBICardDesktopPane.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_focusTraversalPolicy = 6;
    private static final int PROPERTY_registeredKeyStrokes = 7;
    private static final int PROPERTY_alignmentX = 8;
    private static final int PROPERTY_paintingTile = 9;
    private static final int PROPERTY_alignmentY = 10;
    private static final int PROPERTY_hierarchyListeners = 11;
    private static final int PROPERTY_accessibleContext = 12;
    private static final int PROPERTY_preferredSize = 13;
    private static final int PROPERTY_managingFocus = 14;
    private static final int PROPERTY_minimumSizeSet = 15;
    private static final int PROPERTY_focusTraversalPolicySet = 16;
    private static final int PROPERTY_y = 17;
    private static final int PROPERTY_x = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_inputMethodRequests = 20;
    private static final int PROPERTY_containerListeners = 21;
    private static final int PROPERTY_insets = 22;
    private static final int PROPERTY_componentCount = 23;
    private static final int PROPERTY_components = 24;
    private static final int PROPERTY_inputVerifier = 25;
    private static final int PROPERTY_hierarchyBoundsListeners = 26;
    private static final int PROPERTY_border = 27;
    private static final int PROPERTY_name = 28;
    private static final int PROPERTY_optimizedDrawingEnabled = 29;
    private static final int PROPERTY_graphics = 30;
    private static final int PROPERTY_minimumSize = 31;
    private static final int PROPERTY_toolTipText = 32;
    private static final int PROPERTY_focusTraversalKeysEnabled = 33;
    private static final int PROPERTY_foreground = 34;
    private static final int PROPERTY_selectedFrame = 35;
    private static final int PROPERTY_ignoreRepaint = 36;
    private static final int PROPERTY_focusable = 37;
    private static final int PROPERTY_preferredSizeSet = 38;
    private static final int PROPERTY_visible = 39;
    private static final int PROPERTY_focusCycleRootAncestor = 40;
    private static final int PROPERTY_parent = 41;
    private static final int PROPERTY_rootPane = 42;
    private static final int PROPERTY_autoOrganize = 43;
    private static final int PROPERTY_lightweight = 44;
    private static final int PROPERTY_width = 45;
    private static final int PROPERTY_keyListeners = 46;
    private static final int PROPERTY_toolkit = 47;
    private static final int PROPERTY_inputContext = 48;
    private static final int PROPERTY_layout = 49;
    private static final int PROPERTY_opaque = 50;
    private static final int PROPERTY_desktopManager = 51;
    private static final int PROPERTY_font = 52;
    private static final int PROPERTY_locale = 53;
    private static final int PROPERTY_cursor = 54;
    private static final int PROPERTY_dragMode = 55;
    private static final int PROPERTY_inputMethodListeners = 56;
    private static final int PROPERTY_transferHandler = 57;
    private static final int PROPERTY_vetoableChangeListeners = 58;
    private static final int PROPERTY_doubleBuffered = 59;
    private static final int PROPERTY_visibleRect = 60;
    private static final int PROPERTY_maximumSizeSet = 61;
    private static final int PROPERTY_valid = 62;
    private static final int PROPERTY_focusCycleRoot = 63;
    private static final int PROPERTY_maximumSize = 64;
    private static final int PROPERTY_mouseMotionListeners = 65;
    private static final int PROPERTY_treeLock = 66;
    private static final int PROPERTY_bounds = 67;
    private static final int PROPERTY_focusTraversable = 68;
    private static final int PROPERTY_propertyChangeListeners = 69;
    private static final int PROPERTY_autoscrolls = 70;
    private static final int PROPERTY_componentListeners = 71;
    private static final int PROPERTY_showing = 72;
    private static final int PROPERTY_allFrames = 73;
    private static final int PROPERTY_dropTarget = 74;
    private static final int PROPERTY_focusListeners = 75;
    private static final int PROPERTY_nextFocusableComponent = 76;
    private static final int PROPERTY_peer = 77;
    private static final int PROPERTY_height = 78;
    private static final int PROPERTY_topLevelAncestor = 79;
    private static final int PROPERTY_displayable = 80;
    private static final int PROPERTY_background = 81;
    private static final int PROPERTY_graphicsConfiguration = 82;
    private static final int PROPERTY_focusOwner = 83;
    private static final int PROPERTY_ancestorListeners = 84;
    private static final int PROPERTY_requestFocusEnabled = 85;
    private static final int PROPERTY_debugGraphicsOptions = 86;
    private static final int PROPERTY_backgroundSet = 87;
    private static final int PROPERTY_actionMap = 88;
    private static final int PROPERTY_mouseListeners = 89;
    private static final int PROPERTY_enabled = 90;
    private static final int PROPERTY_foregroundSet = 91;
    private static final int PROPERTY_validateRoot = 92;
    private static final int PROPERTY_UI = 93;
    private static final int PROPERTY_UIClassID = 94;
    private static final int PROPERTY_component = 95;
    private static final int PROPERTY_componentsInLayer = 96;
    private static final int PROPERTY_allFramesInLayer = 97;
    private static final int PROPERTY_componentCountInLayer = 98;
    private static final int PROPERTY_focusTraversalKeys = 99;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[100];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBICardDesktopPane.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBICardDesktopPane.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBICardDesktopPane.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBICardDesktopPane.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBICardDesktopPane.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBICardDesktopPane.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBICardDesktopPane.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBICardDesktopPane.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBICardDesktopPane.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBICardDesktopPane.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBICardDesktopPane.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBICardDesktopPane.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBICardDesktopPane.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBICardDesktopPane.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBICardDesktopPane.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBICardDesktopPane.class, "isMinimumSizeSet", null );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBICardDesktopPane.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBICardDesktopPane.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBICardDesktopPane.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBICardDesktopPane.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBICardDesktopPane.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBICardDesktopPane.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBICardDesktopPane.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBICardDesktopPane.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBICardDesktopPane.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBICardDesktopPane.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBICardDesktopPane.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBICardDesktopPane.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBICardDesktopPane.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBICardDesktopPane.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBICardDesktopPane.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBICardDesktopPane.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBICardDesktopPane.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBICardDesktopPane.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBICardDesktopPane.class, "getForeground", "setForeground" );
            properties[PROPERTY_selectedFrame] = new PropertyDescriptor ( "selectedFrame", JBICardDesktopPane.class, "getSelectedFrame", "setSelectedFrame" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBICardDesktopPane.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBICardDesktopPane.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBICardDesktopPane.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBICardDesktopPane.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBICardDesktopPane.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBICardDesktopPane.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBICardDesktopPane.class, "getRootPane", null );
            properties[PROPERTY_autoOrganize] = new PropertyDescriptor ( "autoOrganize", JBICardDesktopPane.class, "getAutoOrganize", "setAutoOrganize" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBICardDesktopPane.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBICardDesktopPane.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBICardDesktopPane.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBICardDesktopPane.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBICardDesktopPane.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBICardDesktopPane.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBICardDesktopPane.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_desktopManager] = new PropertyDescriptor ( "desktopManager", JBICardDesktopPane.class, "getDesktopManager", "setDesktopManager" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBICardDesktopPane.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBICardDesktopPane.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBICardDesktopPane.class, "getCursor", "setCursor" );
            properties[PROPERTY_dragMode] = new PropertyDescriptor ( "dragMode", JBICardDesktopPane.class, "getDragMode", "setDragMode" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBICardDesktopPane.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBICardDesktopPane.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBICardDesktopPane.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBICardDesktopPane.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBICardDesktopPane.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBICardDesktopPane.class, "isMaximumSizeSet", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBICardDesktopPane.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBICardDesktopPane.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBICardDesktopPane.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBICardDesktopPane.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBICardDesktopPane.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBICardDesktopPane.class, "getBounds", "setBounds" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBICardDesktopPane.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBICardDesktopPane.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBICardDesktopPane.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBICardDesktopPane.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBICardDesktopPane.class, "isShowing", null );
            properties[PROPERTY_allFrames] = new PropertyDescriptor ( "allFrames", JBICardDesktopPane.class, "getAllFrames", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBICardDesktopPane.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBICardDesktopPane.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBICardDesktopPane.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBICardDesktopPane.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBICardDesktopPane.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBICardDesktopPane.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBICardDesktopPane.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBICardDesktopPane.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBICardDesktopPane.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBICardDesktopPane.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBICardDesktopPane.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBICardDesktopPane.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBICardDesktopPane.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBICardDesktopPane.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBICardDesktopPane.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBICardDesktopPane.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBICardDesktopPane.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBICardDesktopPane.class, "isForegroundSet", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBICardDesktopPane.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBICardDesktopPane.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBICardDesktopPane.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBICardDesktopPane.class, null, null, "getComponent", null );
            properties[PROPERTY_componentsInLayer] = new IndexedPropertyDescriptor ( "componentsInLayer", JBICardDesktopPane.class, null, null, "getComponentsInLayer", null );
            properties[PROPERTY_allFramesInLayer] = new IndexedPropertyDescriptor ( "allFramesInLayer", JBICardDesktopPane.class, null, null, "getAllFramesInLayer", null );
            properties[PROPERTY_componentCountInLayer] = new IndexedPropertyDescriptor ( "componentCountInLayer", JBICardDesktopPane.class, null, null, "getComponentCountInLayer", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBICardDesktopPane.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_containerListener = 0;
    private static final int EVENT_hierarchyListener = 1;
    private static final int EVENT_mouseMotionListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_inputMethodListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_keyListener = 6;
    private static final int EVENT_mouseWheelListener = 7;
    private static final int EVENT_ancestorListener = 8;
    private static final int EVENT_propertyChangeListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyTyped", "keyReleased"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseExited", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBICardDesktopPane.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_setBounds0 = 0;
    private static final int METHOD_removeAll1 = 1;
    private static final int METHOD_autoOrganizeFrames2 = 2;
    private static final int METHOD_add3 = 3;
    private static final int METHOD_remove4 = 4;
    private static final int METHOD_moveFrame5 = 5;
    private static final int METHOD_setAllSize6 = 6;
    private static final int METHOD_setAllSize7 = 7;
    private static final int METHOD_removeFrame8 = 8;
    private static final int METHOD_updateUI9 = 9;
    private static final int METHOD_getLayer10 = 10;
    private static final int METHOD_getIndexOf11 = 11;
    private static final int METHOD_getPosition12 = 12;
    private static final int METHOD_setLayer13 = 13;
    private static final int METHOD_setLayer14 = 14;
    private static final int METHOD_highestLayer15 = 15;
    private static final int METHOD_moveToBack16 = 16;
    private static final int METHOD_getLayer17 = 17;
    private static final int METHOD_setPosition18 = 18;
    private static final int METHOD_paint19 = 19;
    private static final int METHOD_lowestLayer20 = 20;
    private static final int METHOD_remove21 = 21;
    private static final int METHOD_getLayeredPaneAbove22 = 22;
    private static final int METHOD_moveToFront23 = 23;
    private static final int METHOD_putLayer24 = 24;
    private static final int METHOD_getPropertyChangeListeners25 = 25;
    private static final int METHOD_scrollRectToVisible26 = 26;
    private static final int METHOD_getClientProperty27 = 27;
    private static final int METHOD_print28 = 28;
    private static final int METHOD_unregisterKeyboardAction29 = 29;
    private static final int METHOD_removePropertyChangeListener30 = 30;
    private static final int METHOD_getConditionForKeyStroke31 = 31;
    private static final int METHOD_requestFocus32 = 32;
    private static final int METHOD_paintImmediately33 = 33;
    private static final int METHOD_requestDefaultFocus34 = 34;
    private static final int METHOD_getToolTipText35 = 35;
    private static final int METHOD_requestFocusInWindow36 = 36;
    private static final int METHOD_repaint37 = 37;
    private static final int METHOD_getInputMap38 = 38;
    private static final int METHOD_getSize39 = 39;
    private static final int METHOD_requestFocus40 = 40;
    private static final int METHOD_firePropertyChange41 = 41;
    private static final int METHOD_isLightweightComponent42 = 42;
    private static final int METHOD_getInputMap43 = 43;
    private static final int METHOD_firePropertyChange44 = 44;
    private static final int METHOD_printAll45 = 45;
    private static final int METHOD_computeVisibleRect46 = 46;
    private static final int METHOD_getLocation47 = 47;
    private static final int METHOD_disable48 = 48;
    private static final int METHOD_paintImmediately49 = 49;
    private static final int METHOD_firePropertyChange50 = 50;
    private static final int METHOD_addNotify51 = 51;
    private static final int METHOD_getToolTipLocation52 = 52;
    private static final int METHOD_getListeners53 = 53;
    private static final int METHOD_firePropertyChange54 = 54;
    private static final int METHOD_reshape55 = 55;
    private static final int METHOD_grabFocus56 = 56;
    private static final int METHOD_getBounds57 = 57;
    private static final int METHOD_registerKeyboardAction58 = 58;
    private static final int METHOD_registerKeyboardAction59 = 59;
    private static final int METHOD_enable60 = 60;
    private static final int METHOD_addPropertyChangeListener61 = 61;
    private static final int METHOD_update62 = 62;
    private static final int METHOD_getActionForKeyStroke63 = 63;
    private static final int METHOD_removeNotify64 = 64;
    private static final int METHOD_firePropertyChange65 = 65;
    private static final int METHOD_firePropertyChange66 = 66;
    private static final int METHOD_getInsets67 = 67;
    private static final int METHOD_getDefaultLocale68 = 68;
    private static final int METHOD_putClientProperty69 = 69;
    private static final int METHOD_createToolTip70 = 70;
    private static final int METHOD_contains71 = 71;
    private static final int METHOD_revalidate72 = 72;
    private static final int METHOD_repaint73 = 73;
    private static final int METHOD_resetKeyboardActions74 = 74;
    private static final int METHOD_setDefaultLocale75 = 75;
    private static final int METHOD_firePropertyChange76 = 76;
    private static final int METHOD_setInputMap77 = 77;
    private static final int METHOD_firePropertyChange78 = 78;
    private static final int METHOD_applyComponentOrientation79 = 79;
    private static final int METHOD_add80 = 80;
    private static final int METHOD_validate81 = 81;
    private static final int METHOD_getComponentAt82 = 82;
    private static final int METHOD_preferredSize83 = 83;
    private static final int METHOD_areFocusTraversalKeysSet84 = 84;
    private static final int METHOD_list85 = 85;
    private static final int METHOD_printComponents86 = 86;
    private static final int METHOD_paintComponents87 = 87;
    private static final int METHOD_insets88 = 88;
    private static final int METHOD_findComponentAt89 = 89;
    private static final int METHOD_deliverEvent90 = 90;
    private static final int METHOD_locate91 = 91;
    private static final int METHOD_getComponentAt92 = 92;
    private static final int METHOD_transferFocusBackward93 = 93;
    private static final int METHOD_minimumSize94 = 94;
    private static final int METHOD_add95 = 95;
    private static final int METHOD_add96 = 96;
    private static final int METHOD_add97 = 97;
    private static final int METHOD_layout98 = 98;
    private static final int METHOD_transferFocusDownCycle99 = 99;
    private static final int METHOD_isFocusCycleRoot100 = 100;
    private static final int METHOD_add101 = 101;
    private static final int METHOD_invalidate102 = 102;
    private static final int METHOD_list103 = 103;
    private static final int METHOD_countComponents104 = 104;
    private static final int METHOD_isAncestorOf105 = 105;
    private static final int METHOD_doLayout106 = 106;
    private static final int METHOD_findComponentAt107 = 107;
    private static final int METHOD_getFontMetrics108 = 108;
    private static final int METHOD_location109 = 109;
    private static final int METHOD_transferFocusUpCycle110 = 110;
    private static final int METHOD_postEvent111 = 111;
    private static final int METHOD_contains112 = 112;
    private static final int METHOD_gotFocus113 = 113;
    private static final int METHOD_setSize114 = 114;
    private static final int METHOD_list115 = 115;
    private static final int METHOD_hide116 = 116;
    private static final int METHOD_checkImage117 = 117;
    private static final int METHOD_checkImage118 = 118;
    private static final int METHOD_hasFocus119 = 119;
    private static final int METHOD_setLocation120 = 120;
    private static final int METHOD_mouseEnter121 = 121;
    private static final int METHOD_size122 = 122;
    private static final int METHOD_keyDown123 = 123;
    private static final int METHOD_move124 = 124;
    private static final int METHOD_inside125 = 125;
    private static final int METHOD_list126 = 126;
    private static final int METHOD_toString127 = 127;
    private static final int METHOD_keyUp128 = 128;
    private static final int METHOD_dispatchEvent129 = 129;
    private static final int METHOD_mouseDrag130 = 130;
    private static final int METHOD_setLocation131 = 131;
    private static final int METHOD_createImage132 = 132;
    private static final int METHOD_show133 = 133;
    private static final int METHOD_add134 = 134;
    private static final int METHOD_show135 = 135;
    private static final int METHOD_mouseUp136 = 136;
    private static final int METHOD_lostFocus137 = 137;
    private static final int METHOD_imageUpdate138 = 138;
    private static final int METHOD_resize139 = 139;
    private static final int METHOD_mouseDown140 = 140;
    private static final int METHOD_action141 = 141;
    private static final int METHOD_createVolatileImage142 = 142;
    private static final int METHOD_nextFocus143 = 143;
    private static final int METHOD_getLocation144 = 144;
    private static final int METHOD_paintAll145 = 145;
    private static final int METHOD_createVolatileImage146 = 146;
    private static final int METHOD_createImage147 = 147;
    private static final int METHOD_handleEvent148 = 148;
    private static final int METHOD_repaint149 = 149;
    private static final int METHOD_repaint150 = 150;
    private static final int METHOD_mouseExit151 = 151;
    private static final int METHOD_prepareImage152 = 152;
    private static final int METHOD_prepareImage153 = 153;
    private static final int METHOD_remove154 = 154;
    private static final int METHOD_setSize155 = 155;
    private static final int METHOD_enable156 = 156;
    private static final int METHOD_transferFocus157 = 157;
    private static final int METHOD_mouseMove158 = 158;
    private static final int METHOD_getSize159 = 159;
    private static final int METHOD_repaint160 = 160;
    private static final int METHOD_enableInputMethods161 = 161;
    private static final int METHOD_bounds162 = 162;
    private static final int METHOD_resize163 = 163;
    private static final int METHOD_list164 = 164;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[165];
    
        try {
            methods[METHOD_setBounds0] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds0].setDisplayName ( "" );
            methods[METHOD_removeAll1] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll1].setDisplayName ( "" );
            methods[METHOD_autoOrganizeFrames2] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("autoOrganizeFrames", new Class[] {}));
            methods[METHOD_autoOrganizeFrames2].setDisplayName ( "" );
            methods[METHOD_add3] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {javax.swing.JInternalFrame.class}));
            methods[METHOD_add3].setDisplayName ( "" );
            methods[METHOD_remove4] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove4].setDisplayName ( "" );
            methods[METHOD_moveFrame5] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("moveFrame", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_moveFrame5].setDisplayName ( "" );
            methods[METHOD_setAllSize6] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setAllSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setAllSize6].setDisplayName ( "" );
            methods[METHOD_setAllSize7] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setAllSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setAllSize7].setDisplayName ( "" );
            methods[METHOD_removeFrame8] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("removeFrame", new Class[] {javax.swing.JInternalFrame.class}));
            methods[METHOD_removeFrame8].setDisplayName ( "" );
            methods[METHOD_updateUI9] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI9].setDisplayName ( "" );
            methods[METHOD_getLayer10] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getLayer", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayer10].setDisplayName ( "" );
            methods[METHOD_getIndexOf11] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getIndexOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_getIndexOf11].setDisplayName ( "" );
            methods[METHOD_getPosition12] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getPosition", new Class[] {java.awt.Component.class}));
            methods[METHOD_getPosition12].setDisplayName ( "" );
            methods[METHOD_setLayer13] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setLayer13].setDisplayName ( "" );
            methods[METHOD_setLayer14] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLayer14].setDisplayName ( "" );
            methods[METHOD_highestLayer15] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("highestLayer", new Class[] {}));
            methods[METHOD_highestLayer15].setDisplayName ( "" );
            methods[METHOD_moveToBack16] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("moveToBack", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToBack16].setDisplayName ( "" );
            methods[METHOD_getLayer17] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getLayer", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_getLayer17].setDisplayName ( "" );
            methods[METHOD_setPosition18] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setPosition", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setPosition18].setDisplayName ( "" );
            methods[METHOD_paint19] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint19].setDisplayName ( "" );
            methods[METHOD_lowestLayer20] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("lowestLayer", new Class[] {}));
            methods[METHOD_lowestLayer20].setDisplayName ( "" );
            methods[METHOD_remove21] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove21].setDisplayName ( "" );
            methods[METHOD_getLayeredPaneAbove22] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getLayeredPaneAbove", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayeredPaneAbove22].setDisplayName ( "" );
            methods[METHOD_moveToFront23] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("moveToFront", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToFront23].setDisplayName ( "" );
            methods[METHOD_putLayer24] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("putLayer", new Class[] {javax.swing.JComponent.class, Integer.TYPE}));
            methods[METHOD_putLayer24].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners25] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners25].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible26] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible26].setDisplayName ( "" );
            methods[METHOD_getClientProperty27] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty27].setDisplayName ( "" );
            methods[METHOD_print28] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print28].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction29] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction29].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener30] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener30].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke31] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke31].setDisplayName ( "" );
            methods[METHOD_requestFocus32] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus32].setDisplayName ( "" );
            methods[METHOD_paintImmediately33] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately33].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus34] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus34].setDisplayName ( "" );
            methods[METHOD_getToolTipText35] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText35].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow36] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow36].setDisplayName ( "" );
            methods[METHOD_repaint37] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint37].setDisplayName ( "" );
            methods[METHOD_getInputMap38] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap38].setDisplayName ( "" );
            methods[METHOD_getSize39] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize39].setDisplayName ( "" );
            methods[METHOD_requestFocus40] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus40].setDisplayName ( "" );
            methods[METHOD_firePropertyChange41] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange41].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent42] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent42].setDisplayName ( "" );
            methods[METHOD_getInputMap43] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap43].setDisplayName ( "" );
            methods[METHOD_firePropertyChange44] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange44].setDisplayName ( "" );
            methods[METHOD_printAll45] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll45].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect46] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect46].setDisplayName ( "" );
            methods[METHOD_getLocation47] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation47].setDisplayName ( "" );
            methods[METHOD_disable48] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable48].setDisplayName ( "" );
            methods[METHOD_paintImmediately49] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately49].setDisplayName ( "" );
            methods[METHOD_firePropertyChange50] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange50].setDisplayName ( "" );
            methods[METHOD_addNotify51] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify51].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation52] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation52].setDisplayName ( "" );
            methods[METHOD_getListeners53] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners53].setDisplayName ( "" );
            methods[METHOD_firePropertyChange54] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange54].setDisplayName ( "" );
            methods[METHOD_reshape55] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape55].setDisplayName ( "" );
            methods[METHOD_grabFocus56] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus56].setDisplayName ( "" );
            methods[METHOD_getBounds57] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds57].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction58] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction58].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction59] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction59].setDisplayName ( "" );
            methods[METHOD_enable60] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable60].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener61] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener61].setDisplayName ( "" );
            methods[METHOD_update62] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update62].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke63] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke63].setDisplayName ( "" );
            methods[METHOD_removeNotify64] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify64].setDisplayName ( "" );
            methods[METHOD_firePropertyChange65] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange65].setDisplayName ( "" );
            methods[METHOD_firePropertyChange66] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange66].setDisplayName ( "" );
            methods[METHOD_getInsets67] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets67].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale68] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale68].setDisplayName ( "" );
            methods[METHOD_putClientProperty69] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty69].setDisplayName ( "" );
            methods[METHOD_createToolTip70] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip70].setDisplayName ( "" );
            methods[METHOD_contains71] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains71].setDisplayName ( "" );
            methods[METHOD_revalidate72] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate72].setDisplayName ( "" );
            methods[METHOD_repaint73] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint73].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions74] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions74].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale75] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale75].setDisplayName ( "" );
            methods[METHOD_firePropertyChange76] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange76].setDisplayName ( "" );
            methods[METHOD_setInputMap77] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap77].setDisplayName ( "" );
            methods[METHOD_firePropertyChange78] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange78].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation79] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation79].setDisplayName ( "" );
            methods[METHOD_add80] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add80].setDisplayName ( "" );
            methods[METHOD_validate81] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate81].setDisplayName ( "" );
            methods[METHOD_getComponentAt82] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt82].setDisplayName ( "" );
            methods[METHOD_preferredSize83] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize83].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet84] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet84].setDisplayName ( "" );
            methods[METHOD_list85] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list85].setDisplayName ( "" );
            methods[METHOD_printComponents86] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents86].setDisplayName ( "" );
            methods[METHOD_paintComponents87] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents87].setDisplayName ( "" );
            methods[METHOD_insets88] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets88].setDisplayName ( "" );
            methods[METHOD_findComponentAt89] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt89].setDisplayName ( "" );
            methods[METHOD_deliverEvent90] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent90].setDisplayName ( "" );
            methods[METHOD_locate91] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate91].setDisplayName ( "" );
            methods[METHOD_getComponentAt92] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt92].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward93] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward93].setDisplayName ( "" );
            methods[METHOD_minimumSize94] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize94].setDisplayName ( "" );
            methods[METHOD_add95] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add95].setDisplayName ( "" );
            methods[METHOD_add96] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add96].setDisplayName ( "" );
            methods[METHOD_add97] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add97].setDisplayName ( "" );
            methods[METHOD_layout98] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout98].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle99] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle99].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot100] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot100].setDisplayName ( "" );
            methods[METHOD_add101] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add101].setDisplayName ( "" );
            methods[METHOD_invalidate102] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate102].setDisplayName ( "" );
            methods[METHOD_list103] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list103].setDisplayName ( "" );
            methods[METHOD_countComponents104] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents104].setDisplayName ( "" );
            methods[METHOD_isAncestorOf105] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf105].setDisplayName ( "" );
            methods[METHOD_doLayout106] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout106].setDisplayName ( "" );
            methods[METHOD_findComponentAt107] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt107].setDisplayName ( "" );
            methods[METHOD_getFontMetrics108] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics108].setDisplayName ( "" );
            methods[METHOD_location109] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("location", new Class[] {}));
            methods[METHOD_location109].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle110] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle110].setDisplayName ( "" );
            methods[METHOD_postEvent111] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent111].setDisplayName ( "" );
            methods[METHOD_contains112] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains112].setDisplayName ( "" );
            methods[METHOD_gotFocus113] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus113].setDisplayName ( "" );
            methods[METHOD_setSize114] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize114].setDisplayName ( "" );
            methods[METHOD_list115] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list115].setDisplayName ( "" );
            methods[METHOD_hide116] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide116].setDisplayName ( "" );
            methods[METHOD_checkImage117] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage117].setDisplayName ( "" );
            methods[METHOD_checkImage118] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage118].setDisplayName ( "" );
            methods[METHOD_hasFocus119] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus119].setDisplayName ( "" );
            methods[METHOD_setLocation120] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation120].setDisplayName ( "" );
            methods[METHOD_mouseEnter121] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter121].setDisplayName ( "" );
            methods[METHOD_size122] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("size", new Class[] {}));
            methods[METHOD_size122].setDisplayName ( "" );
            methods[METHOD_keyDown123] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown123].setDisplayName ( "" );
            methods[METHOD_move124] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move124].setDisplayName ( "" );
            methods[METHOD_inside125] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside125].setDisplayName ( "" );
            methods[METHOD_list126] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list126].setDisplayName ( "" );
            methods[METHOD_toString127] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString127].setDisplayName ( "" );
            methods[METHOD_keyUp128] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp128].setDisplayName ( "" );
            methods[METHOD_dispatchEvent129] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent129].setDisplayName ( "" );
            methods[METHOD_mouseDrag130] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag130].setDisplayName ( "" );
            methods[METHOD_setLocation131] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation131].setDisplayName ( "" );
            methods[METHOD_createImage132] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage132].setDisplayName ( "" );
            methods[METHOD_show133] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show133].setDisplayName ( "" );
            methods[METHOD_add134] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add134].setDisplayName ( "" );
            methods[METHOD_show135] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("show", new Class[] {}));
            methods[METHOD_show135].setDisplayName ( "" );
            methods[METHOD_mouseUp136] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp136].setDisplayName ( "" );
            methods[METHOD_lostFocus137] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus137].setDisplayName ( "" );
            methods[METHOD_imageUpdate138] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate138].setDisplayName ( "" );
            methods[METHOD_resize139] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize139].setDisplayName ( "" );
            methods[METHOD_mouseDown140] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown140].setDisplayName ( "" );
            methods[METHOD_action141] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action141].setDisplayName ( "" );
            methods[METHOD_createVolatileImage142] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage142].setDisplayName ( "" );
            methods[METHOD_nextFocus143] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus143].setDisplayName ( "" );
            methods[METHOD_getLocation144] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation144].setDisplayName ( "" );
            methods[METHOD_paintAll145] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll145].setDisplayName ( "" );
            methods[METHOD_createVolatileImage146] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage146].setDisplayName ( "" );
            methods[METHOD_createImage147] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage147].setDisplayName ( "" );
            methods[METHOD_handleEvent148] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent148].setDisplayName ( "" );
            methods[METHOD_repaint149] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint149].setDisplayName ( "" );
            methods[METHOD_repaint150] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint150].setDisplayName ( "" );
            methods[METHOD_mouseExit151] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit151].setDisplayName ( "" );
            methods[METHOD_prepareImage152] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage152].setDisplayName ( "" );
            methods[METHOD_prepareImage153] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage153].setDisplayName ( "" );
            methods[METHOD_remove154] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove154].setDisplayName ( "" );
            methods[METHOD_setSize155] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize155].setDisplayName ( "" );
            methods[METHOD_enable156] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable156].setDisplayName ( "" );
            methods[METHOD_transferFocus157] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus157].setDisplayName ( "" );
            methods[METHOD_mouseMove158] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove158].setDisplayName ( "" );
            methods[METHOD_getSize159] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize159].setDisplayName ( "" );
            methods[METHOD_repaint160] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint160].setDisplayName ( "" );
            methods[METHOD_enableInputMethods161] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods161].setDisplayName ( "" );
            methods[METHOD_bounds162] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds162].setDisplayName ( "" );
            methods[METHOD_resize163] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize163].setDisplayName ( "" );
            methods[METHOD_list164] = new MethodDescriptor ( bsc.beans.JBICardDesktopPane.class.getMethod("list", new Class[] {}));
            methods[METHOD_list164].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

