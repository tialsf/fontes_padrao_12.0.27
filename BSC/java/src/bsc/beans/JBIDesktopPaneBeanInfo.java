package bsc.beans;

import java.beans.*;

public class JBIDesktopPaneBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIDesktopPane.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_focusTraversalPolicy = 6;
    private static final int PROPERTY_registeredKeyStrokes = 7;
    private static final int PROPERTY_alignmentX = 8;
    private static final int PROPERTY_paintingTile = 9;
    private static final int PROPERTY_alignmentY = 10;
    private static final int PROPERTY_hierarchyListeners = 11;
    private static final int PROPERTY_accessibleContext = 12;
    private static final int PROPERTY_preferredSize = 13;
    private static final int PROPERTY_managingFocus = 14;
    private static final int PROPERTY_minimumSizeSet = 15;
    private static final int PROPERTY_focusTraversalPolicySet = 16;
    private static final int PROPERTY_y = 17;
    private static final int PROPERTY_x = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_inputMethodRequests = 20;
    private static final int PROPERTY_containerListeners = 21;
    private static final int PROPERTY_insets = 22;
    private static final int PROPERTY_componentCount = 23;
    private static final int PROPERTY_components = 24;
    private static final int PROPERTY_inputVerifier = 25;
    private static final int PROPERTY_hierarchyBoundsListeners = 26;
    private static final int PROPERTY_border = 27;
    private static final int PROPERTY_name = 28;
    private static final int PROPERTY_optimizedDrawingEnabled = 29;
    private static final int PROPERTY_graphics = 30;
    private static final int PROPERTY_minimumSize = 31;
    private static final int PROPERTY_toolTipText = 32;
    private static final int PROPERTY_focusTraversalKeysEnabled = 33;
    private static final int PROPERTY_foreground = 34;
    private static final int PROPERTY_selectedFrame = 35;
    private static final int PROPERTY_ignoreRepaint = 36;
    private static final int PROPERTY_focusable = 37;
    private static final int PROPERTY_preferredSizeSet = 38;
    private static final int PROPERTY_visible = 39;
    private static final int PROPERTY_focusCycleRootAncestor = 40;
    private static final int PROPERTY_parent = 41;
    private static final int PROPERTY_rootPane = 42;
    private static final int PROPERTY_lightweight = 43;
    private static final int PROPERTY_width = 44;
    private static final int PROPERTY_keyListeners = 45;
    private static final int PROPERTY_toolkit = 46;
    private static final int PROPERTY_inputContext = 47;
    private static final int PROPERTY_layout = 48;
    private static final int PROPERTY_opaque = 49;
    private static final int PROPERTY_desktopManager = 50;
    private static final int PROPERTY_font = 51;
    private static final int PROPERTY_locale = 52;
    private static final int PROPERTY_cursor = 53;
    private static final int PROPERTY_dragMode = 54;
    private static final int PROPERTY_inputMethodListeners = 55;
    private static final int PROPERTY_transferHandler = 56;
    private static final int PROPERTY_vetoableChangeListeners = 57;
    private static final int PROPERTY_doubleBuffered = 58;
    private static final int PROPERTY_visibleRect = 59;
    private static final int PROPERTY_maximumSizeSet = 60;
    private static final int PROPERTY_valid = 61;
    private static final int PROPERTY_focusCycleRoot = 62;
    private static final int PROPERTY_maximumSize = 63;
    private static final int PROPERTY_mouseMotionListeners = 64;
    private static final int PROPERTY_treeLock = 65;
    private static final int PROPERTY_bounds = 66;
    private static final int PROPERTY_focusTraversable = 67;
    private static final int PROPERTY_propertyChangeListeners = 68;
    private static final int PROPERTY_autoscrolls = 69;
    private static final int PROPERTY_componentListeners = 70;
    private static final int PROPERTY_showing = 71;
    private static final int PROPERTY_allFrames = 72;
    private static final int PROPERTY_dropTarget = 73;
    private static final int PROPERTY_focusListeners = 74;
    private static final int PROPERTY_nextFocusableComponent = 75;
    private static final int PROPERTY_peer = 76;
    private static final int PROPERTY_height = 77;
    private static final int PROPERTY_topLevelAncestor = 78;
    private static final int PROPERTY_displayable = 79;
    private static final int PROPERTY_background = 80;
    private static final int PROPERTY_graphicsConfiguration = 81;
    private static final int PROPERTY_focusOwner = 82;
    private static final int PROPERTY_ancestorListeners = 83;
    private static final int PROPERTY_requestFocusEnabled = 84;
    private static final int PROPERTY_debugGraphicsOptions = 85;
    private static final int PROPERTY_backgroundSet = 86;
    private static final int PROPERTY_actionMap = 87;
    private static final int PROPERTY_mouseListeners = 88;
    private static final int PROPERTY_enabled = 89;
    private static final int PROPERTY_foregroundSet = 90;
    private static final int PROPERTY_validateRoot = 91;
    private static final int PROPERTY_UI = 92;
    private static final int PROPERTY_UIClassID = 93;
    private static final int PROPERTY_component = 94;
    private static final int PROPERTY_componentsInLayer = 95;
    private static final int PROPERTY_allFramesInLayer = 96;
    private static final int PROPERTY_componentCountInLayer = 97;
    private static final int PROPERTY_focusTraversalKeys = 98;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[99];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIDesktopPane.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIDesktopPane.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIDesktopPane.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIDesktopPane.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIDesktopPane.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIDesktopPane.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIDesktopPane.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIDesktopPane.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIDesktopPane.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIDesktopPane.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIDesktopPane.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIDesktopPane.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIDesktopPane.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIDesktopPane.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIDesktopPane.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIDesktopPane.class, "isMinimumSizeSet", null );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIDesktopPane.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIDesktopPane.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIDesktopPane.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIDesktopPane.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIDesktopPane.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIDesktopPane.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIDesktopPane.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIDesktopPane.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIDesktopPane.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIDesktopPane.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIDesktopPane.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIDesktopPane.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIDesktopPane.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIDesktopPane.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIDesktopPane.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIDesktopPane.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIDesktopPane.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIDesktopPane.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIDesktopPane.class, "getForeground", "setForeground" );
            properties[PROPERTY_selectedFrame] = new PropertyDescriptor ( "selectedFrame", JBIDesktopPane.class, "getSelectedFrame", "setSelectedFrame" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIDesktopPane.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIDesktopPane.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIDesktopPane.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIDesktopPane.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIDesktopPane.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIDesktopPane.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIDesktopPane.class, "getRootPane", null );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIDesktopPane.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIDesktopPane.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIDesktopPane.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIDesktopPane.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIDesktopPane.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIDesktopPane.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIDesktopPane.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_desktopManager] = new PropertyDescriptor ( "desktopManager", JBIDesktopPane.class, "getDesktopManager", "setDesktopManager" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIDesktopPane.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIDesktopPane.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIDesktopPane.class, "getCursor", "setCursor" );
            properties[PROPERTY_dragMode] = new PropertyDescriptor ( "dragMode", JBIDesktopPane.class, "getDragMode", "setDragMode" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIDesktopPane.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIDesktopPane.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIDesktopPane.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIDesktopPane.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIDesktopPane.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIDesktopPane.class, "isMaximumSizeSet", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIDesktopPane.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIDesktopPane.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIDesktopPane.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIDesktopPane.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIDesktopPane.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIDesktopPane.class, "getBounds", "setBounds" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIDesktopPane.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIDesktopPane.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIDesktopPane.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIDesktopPane.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIDesktopPane.class, "isShowing", null );
            properties[PROPERTY_allFrames] = new PropertyDescriptor ( "allFrames", JBIDesktopPane.class, "getAllFrames", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIDesktopPane.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIDesktopPane.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIDesktopPane.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIDesktopPane.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIDesktopPane.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIDesktopPane.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIDesktopPane.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIDesktopPane.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIDesktopPane.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIDesktopPane.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIDesktopPane.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIDesktopPane.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIDesktopPane.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIDesktopPane.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIDesktopPane.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIDesktopPane.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIDesktopPane.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIDesktopPane.class, "isForegroundSet", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIDesktopPane.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIDesktopPane.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIDesktopPane.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIDesktopPane.class, null, null, "getComponent", null );
            properties[PROPERTY_componentsInLayer] = new IndexedPropertyDescriptor ( "componentsInLayer", JBIDesktopPane.class, null, null, "getComponentsInLayer", null );
            properties[PROPERTY_allFramesInLayer] = new IndexedPropertyDescriptor ( "allFramesInLayer", JBIDesktopPane.class, null, null, "getAllFramesInLayer", null );
            properties[PROPERTY_componentCountInLayer] = new IndexedPropertyDescriptor ( "componentCountInLayer", JBIDesktopPane.class, null, null, "getComponentCountInLayer", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIDesktopPane.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_containerListener = 0;
    private static final int EVENT_hierarchyListener = 1;
    private static final int EVENT_mouseMotionListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_inputMethodListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_keyListener = 6;
    private static final int EVENT_mouseWheelListener = 7;
    private static final int EVENT_ancestorListener = 8;
    private static final int EVENT_propertyChangeListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyTyped", "keyReleased"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseExited", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIDesktopPane.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_setBounds0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_remove2 = 2;
    private static final int METHOD_cascadeFrames3 = 3;
    private static final int METHOD_tileFrames4 = 4;
    private static final int METHOD_setAllSize5 = 5;
    private static final int METHOD_setAllSize6 = 6;
    private static final int METHOD_updateUI7 = 7;
    private static final int METHOD_getLayer8 = 8;
    private static final int METHOD_getIndexOf9 = 9;
    private static final int METHOD_getPosition10 = 10;
    private static final int METHOD_setLayer11 = 11;
    private static final int METHOD_setLayer12 = 12;
    private static final int METHOD_highestLayer13 = 13;
    private static final int METHOD_moveToBack14 = 14;
    private static final int METHOD_getLayer15 = 15;
    private static final int METHOD_setPosition16 = 16;
    private static final int METHOD_paint17 = 17;
    private static final int METHOD_lowestLayer18 = 18;
    private static final int METHOD_remove19 = 19;
    private static final int METHOD_getLayeredPaneAbove20 = 20;
    private static final int METHOD_moveToFront21 = 21;
    private static final int METHOD_putLayer22 = 22;
    private static final int METHOD_getPropertyChangeListeners23 = 23;
    private static final int METHOD_scrollRectToVisible24 = 24;
    private static final int METHOD_getClientProperty25 = 25;
    private static final int METHOD_print26 = 26;
    private static final int METHOD_unregisterKeyboardAction27 = 27;
    private static final int METHOD_removePropertyChangeListener28 = 28;
    private static final int METHOD_getConditionForKeyStroke29 = 29;
    private static final int METHOD_requestFocus30 = 30;
    private static final int METHOD_paintImmediately31 = 31;
    private static final int METHOD_requestDefaultFocus32 = 32;
    private static final int METHOD_getToolTipText33 = 33;
    private static final int METHOD_requestFocusInWindow34 = 34;
    private static final int METHOD_repaint35 = 35;
    private static final int METHOD_getInputMap36 = 36;
    private static final int METHOD_getSize37 = 37;
    private static final int METHOD_requestFocus38 = 38;
    private static final int METHOD_firePropertyChange39 = 39;
    private static final int METHOD_isLightweightComponent40 = 40;
    private static final int METHOD_getInputMap41 = 41;
    private static final int METHOD_firePropertyChange42 = 42;
    private static final int METHOD_printAll43 = 43;
    private static final int METHOD_computeVisibleRect44 = 44;
    private static final int METHOD_getLocation45 = 45;
    private static final int METHOD_disable46 = 46;
    private static final int METHOD_paintImmediately47 = 47;
    private static final int METHOD_firePropertyChange48 = 48;
    private static final int METHOD_addNotify49 = 49;
    private static final int METHOD_getToolTipLocation50 = 50;
    private static final int METHOD_getListeners51 = 51;
    private static final int METHOD_firePropertyChange52 = 52;
    private static final int METHOD_reshape53 = 53;
    private static final int METHOD_grabFocus54 = 54;
    private static final int METHOD_getBounds55 = 55;
    private static final int METHOD_registerKeyboardAction56 = 56;
    private static final int METHOD_registerKeyboardAction57 = 57;
    private static final int METHOD_enable58 = 58;
    private static final int METHOD_addPropertyChangeListener59 = 59;
    private static final int METHOD_update60 = 60;
    private static final int METHOD_getActionForKeyStroke61 = 61;
    private static final int METHOD_removeNotify62 = 62;
    private static final int METHOD_firePropertyChange63 = 63;
    private static final int METHOD_firePropertyChange64 = 64;
    private static final int METHOD_getInsets65 = 65;
    private static final int METHOD_getDefaultLocale66 = 66;
    private static final int METHOD_putClientProperty67 = 67;
    private static final int METHOD_createToolTip68 = 68;
    private static final int METHOD_contains69 = 69;
    private static final int METHOD_revalidate70 = 70;
    private static final int METHOD_repaint71 = 71;
    private static final int METHOD_resetKeyboardActions72 = 72;
    private static final int METHOD_setDefaultLocale73 = 73;
    private static final int METHOD_firePropertyChange74 = 74;
    private static final int METHOD_setInputMap75 = 75;
    private static final int METHOD_firePropertyChange76 = 76;
    private static final int METHOD_applyComponentOrientation77 = 77;
    private static final int METHOD_add78 = 78;
    private static final int METHOD_validate79 = 79;
    private static final int METHOD_getComponentAt80 = 80;
    private static final int METHOD_removeAll81 = 81;
    private static final int METHOD_preferredSize82 = 82;
    private static final int METHOD_areFocusTraversalKeysSet83 = 83;
    private static final int METHOD_list84 = 84;
    private static final int METHOD_printComponents85 = 85;
    private static final int METHOD_paintComponents86 = 86;
    private static final int METHOD_insets87 = 87;
    private static final int METHOD_findComponentAt88 = 88;
    private static final int METHOD_deliverEvent89 = 89;
    private static final int METHOD_locate90 = 90;
    private static final int METHOD_getComponentAt91 = 91;
    private static final int METHOD_transferFocusBackward92 = 92;
    private static final int METHOD_minimumSize93 = 93;
    private static final int METHOD_add94 = 94;
    private static final int METHOD_add95 = 95;
    private static final int METHOD_add96 = 96;
    private static final int METHOD_layout97 = 97;
    private static final int METHOD_transferFocusDownCycle98 = 98;
    private static final int METHOD_isFocusCycleRoot99 = 99;
    private static final int METHOD_add100 = 100;
    private static final int METHOD_invalidate101 = 101;
    private static final int METHOD_list102 = 102;
    private static final int METHOD_countComponents103 = 103;
    private static final int METHOD_isAncestorOf104 = 104;
    private static final int METHOD_doLayout105 = 105;
    private static final int METHOD_findComponentAt106 = 106;
    private static final int METHOD_getFontMetrics107 = 107;
    private static final int METHOD_location108 = 108;
    private static final int METHOD_transferFocusUpCycle109 = 109;
    private static final int METHOD_postEvent110 = 110;
    private static final int METHOD_contains111 = 111;
    private static final int METHOD_gotFocus112 = 112;
    private static final int METHOD_setSize113 = 113;
    private static final int METHOD_list114 = 114;
    private static final int METHOD_hide115 = 115;
    private static final int METHOD_checkImage116 = 116;
    private static final int METHOD_checkImage117 = 117;
    private static final int METHOD_hasFocus118 = 118;
    private static final int METHOD_setLocation119 = 119;
    private static final int METHOD_mouseEnter120 = 120;
    private static final int METHOD_size121 = 121;
    private static final int METHOD_keyDown122 = 122;
    private static final int METHOD_move123 = 123;
    private static final int METHOD_inside124 = 124;
    private static final int METHOD_list125 = 125;
    private static final int METHOD_toString126 = 126;
    private static final int METHOD_keyUp127 = 127;
    private static final int METHOD_dispatchEvent128 = 128;
    private static final int METHOD_mouseDrag129 = 129;
    private static final int METHOD_setLocation130 = 130;
    private static final int METHOD_createImage131 = 131;
    private static final int METHOD_show132 = 132;
    private static final int METHOD_add133 = 133;
    private static final int METHOD_show134 = 134;
    private static final int METHOD_mouseUp135 = 135;
    private static final int METHOD_lostFocus136 = 136;
    private static final int METHOD_imageUpdate137 = 137;
    private static final int METHOD_resize138 = 138;
    private static final int METHOD_mouseDown139 = 139;
    private static final int METHOD_action140 = 140;
    private static final int METHOD_createVolatileImage141 = 141;
    private static final int METHOD_nextFocus142 = 142;
    private static final int METHOD_getLocation143 = 143;
    private static final int METHOD_paintAll144 = 144;
    private static final int METHOD_createVolatileImage145 = 145;
    private static final int METHOD_createImage146 = 146;
    private static final int METHOD_handleEvent147 = 147;
    private static final int METHOD_repaint148 = 148;
    private static final int METHOD_repaint149 = 149;
    private static final int METHOD_mouseExit150 = 150;
    private static final int METHOD_prepareImage151 = 151;
    private static final int METHOD_prepareImage152 = 152;
    private static final int METHOD_remove153 = 153;
    private static final int METHOD_setSize154 = 154;
    private static final int METHOD_enable155 = 155;
    private static final int METHOD_transferFocus156 = 156;
    private static final int METHOD_mouseMove157 = 157;
    private static final int METHOD_getSize158 = 158;
    private static final int METHOD_repaint159 = 159;
    private static final int METHOD_enableInputMethods160 = 160;
    private static final int METHOD_bounds161 = 161;
    private static final int METHOD_resize162 = 162;
    private static final int METHOD_list163 = 163;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[164];
    
        try {
            methods[METHOD_setBounds0] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {javax.swing.JInternalFrame.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_remove2] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove2].setDisplayName ( "" );
            methods[METHOD_cascadeFrames3] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("cascadeFrames", new Class[] {}));
            methods[METHOD_cascadeFrames3].setDisplayName ( "" );
            methods[METHOD_tileFrames4] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("tileFrames", new Class[] {}));
            methods[METHOD_tileFrames4].setDisplayName ( "" );
            methods[METHOD_setAllSize5] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setAllSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setAllSize5].setDisplayName ( "" );
            methods[METHOD_setAllSize6] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setAllSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setAllSize6].setDisplayName ( "" );
            methods[METHOD_updateUI7] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI7].setDisplayName ( "" );
            methods[METHOD_getLayer8] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getLayer", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayer8].setDisplayName ( "" );
            methods[METHOD_getIndexOf9] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getIndexOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_getIndexOf9].setDisplayName ( "" );
            methods[METHOD_getPosition10] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getPosition", new Class[] {java.awt.Component.class}));
            methods[METHOD_getPosition10].setDisplayName ( "" );
            methods[METHOD_setLayer11] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setLayer11].setDisplayName ( "" );
            methods[METHOD_setLayer12] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLayer12].setDisplayName ( "" );
            methods[METHOD_highestLayer13] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("highestLayer", new Class[] {}));
            methods[METHOD_highestLayer13].setDisplayName ( "" );
            methods[METHOD_moveToBack14] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("moveToBack", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToBack14].setDisplayName ( "" );
            methods[METHOD_getLayer15] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getLayer", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_getLayer15].setDisplayName ( "" );
            methods[METHOD_setPosition16] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setPosition", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setPosition16].setDisplayName ( "" );
            methods[METHOD_paint17] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint17].setDisplayName ( "" );
            methods[METHOD_lowestLayer18] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("lowestLayer", new Class[] {}));
            methods[METHOD_lowestLayer18].setDisplayName ( "" );
            methods[METHOD_remove19] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove19].setDisplayName ( "" );
            methods[METHOD_getLayeredPaneAbove20] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getLayeredPaneAbove", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayeredPaneAbove20].setDisplayName ( "" );
            methods[METHOD_moveToFront21] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("moveToFront", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToFront21].setDisplayName ( "" );
            methods[METHOD_putLayer22] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("putLayer", new Class[] {javax.swing.JComponent.class, Integer.TYPE}));
            methods[METHOD_putLayer22].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners23] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners23].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible24] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible24].setDisplayName ( "" );
            methods[METHOD_getClientProperty25] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty25].setDisplayName ( "" );
            methods[METHOD_print26] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print26].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction27] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction27].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener28] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener28].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke29] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke29].setDisplayName ( "" );
            methods[METHOD_requestFocus30] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus30].setDisplayName ( "" );
            methods[METHOD_paintImmediately31] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately31].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus32] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus32].setDisplayName ( "" );
            methods[METHOD_getToolTipText33] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText33].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow34] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow34].setDisplayName ( "" );
            methods[METHOD_repaint35] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint35].setDisplayName ( "" );
            methods[METHOD_getInputMap36] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap36].setDisplayName ( "" );
            methods[METHOD_getSize37] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize37].setDisplayName ( "" );
            methods[METHOD_requestFocus38] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus38].setDisplayName ( "" );
            methods[METHOD_firePropertyChange39] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange39].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent40] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent40].setDisplayName ( "" );
            methods[METHOD_getInputMap41] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap41].setDisplayName ( "" );
            methods[METHOD_firePropertyChange42] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange42].setDisplayName ( "" );
            methods[METHOD_printAll43] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll43].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect44] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect44].setDisplayName ( "" );
            methods[METHOD_getLocation45] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation45].setDisplayName ( "" );
            methods[METHOD_disable46] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable46].setDisplayName ( "" );
            methods[METHOD_paintImmediately47] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately47].setDisplayName ( "" );
            methods[METHOD_firePropertyChange48] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange48].setDisplayName ( "" );
            methods[METHOD_addNotify49] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify49].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation50] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation50].setDisplayName ( "" );
            methods[METHOD_getListeners51] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners51].setDisplayName ( "" );
            methods[METHOD_firePropertyChange52] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange52].setDisplayName ( "" );
            methods[METHOD_reshape53] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape53].setDisplayName ( "" );
            methods[METHOD_grabFocus54] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus54].setDisplayName ( "" );
            methods[METHOD_getBounds55] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds55].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction56] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction56].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction57] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction57].setDisplayName ( "" );
            methods[METHOD_enable58] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable58].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener59] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener59].setDisplayName ( "" );
            methods[METHOD_update60] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update60].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke61] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke61].setDisplayName ( "" );
            methods[METHOD_removeNotify62] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify62].setDisplayName ( "" );
            methods[METHOD_firePropertyChange63] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange63].setDisplayName ( "" );
            methods[METHOD_firePropertyChange64] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange64].setDisplayName ( "" );
            methods[METHOD_getInsets65] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets65].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale66] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale66].setDisplayName ( "" );
            methods[METHOD_putClientProperty67] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty67].setDisplayName ( "" );
            methods[METHOD_createToolTip68] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip68].setDisplayName ( "" );
            methods[METHOD_contains69] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains69].setDisplayName ( "" );
            methods[METHOD_revalidate70] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate70].setDisplayName ( "" );
            methods[METHOD_repaint71] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint71].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions72] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions72].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale73] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale73].setDisplayName ( "" );
            methods[METHOD_firePropertyChange74] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange74].setDisplayName ( "" );
            methods[METHOD_setInputMap75] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap75].setDisplayName ( "" );
            methods[METHOD_firePropertyChange76] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange76].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation77] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation77].setDisplayName ( "" );
            methods[METHOD_add78] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add78].setDisplayName ( "" );
            methods[METHOD_validate79] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate79].setDisplayName ( "" );
            methods[METHOD_getComponentAt80] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt80].setDisplayName ( "" );
            methods[METHOD_removeAll81] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll81].setDisplayName ( "" );
            methods[METHOD_preferredSize82] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize82].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet83] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet83].setDisplayName ( "" );
            methods[METHOD_list84] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list84].setDisplayName ( "" );
            methods[METHOD_printComponents85] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents85].setDisplayName ( "" );
            methods[METHOD_paintComponents86] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents86].setDisplayName ( "" );
            methods[METHOD_insets87] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets87].setDisplayName ( "" );
            methods[METHOD_findComponentAt88] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt88].setDisplayName ( "" );
            methods[METHOD_deliverEvent89] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent89].setDisplayName ( "" );
            methods[METHOD_locate90] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate90].setDisplayName ( "" );
            methods[METHOD_getComponentAt91] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt91].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward92] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward92].setDisplayName ( "" );
            methods[METHOD_minimumSize93] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize93].setDisplayName ( "" );
            methods[METHOD_add94] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add94].setDisplayName ( "" );
            methods[METHOD_add95] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add95].setDisplayName ( "" );
            methods[METHOD_add96] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add96].setDisplayName ( "" );
            methods[METHOD_layout97] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout97].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle98] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle98].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot99] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot99].setDisplayName ( "" );
            methods[METHOD_add100] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add100].setDisplayName ( "" );
            methods[METHOD_invalidate101] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate101].setDisplayName ( "" );
            methods[METHOD_list102] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list102].setDisplayName ( "" );
            methods[METHOD_countComponents103] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents103].setDisplayName ( "" );
            methods[METHOD_isAncestorOf104] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf104].setDisplayName ( "" );
            methods[METHOD_doLayout105] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout105].setDisplayName ( "" );
            methods[METHOD_findComponentAt106] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt106].setDisplayName ( "" );
            methods[METHOD_getFontMetrics107] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics107].setDisplayName ( "" );
            methods[METHOD_location108] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("location", new Class[] {}));
            methods[METHOD_location108].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle109] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle109].setDisplayName ( "" );
            methods[METHOD_postEvent110] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent110].setDisplayName ( "" );
            methods[METHOD_contains111] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains111].setDisplayName ( "" );
            methods[METHOD_gotFocus112] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus112].setDisplayName ( "" );
            methods[METHOD_setSize113] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize113].setDisplayName ( "" );
            methods[METHOD_list114] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list114].setDisplayName ( "" );
            methods[METHOD_hide115] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide115].setDisplayName ( "" );
            methods[METHOD_checkImage116] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage116].setDisplayName ( "" );
            methods[METHOD_checkImage117] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage117].setDisplayName ( "" );
            methods[METHOD_hasFocus118] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus118].setDisplayName ( "" );
            methods[METHOD_setLocation119] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation119].setDisplayName ( "" );
            methods[METHOD_mouseEnter120] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter120].setDisplayName ( "" );
            methods[METHOD_size121] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("size", new Class[] {}));
            methods[METHOD_size121].setDisplayName ( "" );
            methods[METHOD_keyDown122] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown122].setDisplayName ( "" );
            methods[METHOD_move123] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move123].setDisplayName ( "" );
            methods[METHOD_inside124] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside124].setDisplayName ( "" );
            methods[METHOD_list125] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list125].setDisplayName ( "" );
            methods[METHOD_toString126] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString126].setDisplayName ( "" );
            methods[METHOD_keyUp127] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp127].setDisplayName ( "" );
            methods[METHOD_dispatchEvent128] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent128].setDisplayName ( "" );
            methods[METHOD_mouseDrag129] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag129].setDisplayName ( "" );
            methods[METHOD_setLocation130] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation130].setDisplayName ( "" );
            methods[METHOD_createImage131] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage131].setDisplayName ( "" );
            methods[METHOD_show132] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show132].setDisplayName ( "" );
            methods[METHOD_add133] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add133].setDisplayName ( "" );
            methods[METHOD_show134] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("show", new Class[] {}));
            methods[METHOD_show134].setDisplayName ( "" );
            methods[METHOD_mouseUp135] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp135].setDisplayName ( "" );
            methods[METHOD_lostFocus136] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus136].setDisplayName ( "" );
            methods[METHOD_imageUpdate137] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate137].setDisplayName ( "" );
            methods[METHOD_resize138] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize138].setDisplayName ( "" );
            methods[METHOD_mouseDown139] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown139].setDisplayName ( "" );
            methods[METHOD_action140] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action140].setDisplayName ( "" );
            methods[METHOD_createVolatileImage141] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage141].setDisplayName ( "" );
            methods[METHOD_nextFocus142] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus142].setDisplayName ( "" );
            methods[METHOD_getLocation143] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation143].setDisplayName ( "" );
            methods[METHOD_paintAll144] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll144].setDisplayName ( "" );
            methods[METHOD_createVolatileImage145] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage145].setDisplayName ( "" );
            methods[METHOD_createImage146] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage146].setDisplayName ( "" );
            methods[METHOD_handleEvent147] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent147].setDisplayName ( "" );
            methods[METHOD_repaint148] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint148].setDisplayName ( "" );
            methods[METHOD_repaint149] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint149].setDisplayName ( "" );
            methods[METHOD_mouseExit150] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit150].setDisplayName ( "" );
            methods[METHOD_prepareImage151] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage151].setDisplayName ( "" );
            methods[METHOD_prepareImage152] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage152].setDisplayName ( "" );
            methods[METHOD_remove153] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove153].setDisplayName ( "" );
            methods[METHOD_setSize154] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize154].setDisplayName ( "" );
            methods[METHOD_enable155] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable155].setDisplayName ( "" );
            methods[METHOD_transferFocus156] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus156].setDisplayName ( "" );
            methods[METHOD_mouseMove157] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove157].setDisplayName ( "" );
            methods[METHOD_getSize158] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize158].setDisplayName ( "" );
            methods[METHOD_repaint159] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint159].setDisplayName ( "" );
            methods[METHOD_enableInputMethods160] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods160].setDisplayName ( "" );
            methods[METHOD_bounds161] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds161].setDisplayName ( "" );
            methods[METHOD_resize162] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize162].setDisplayName ( "" );
            methods[METHOD_list163] = new MethodDescriptor ( bsc.beans.JBIDesktopPane.class.getMethod("list", new Class[] {}));
            methods[METHOD_list163].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

