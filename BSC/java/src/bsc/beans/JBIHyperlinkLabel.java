/*
 * JBIHyperlinkLabel.java
 *
 * Created on October 28, 2003, 4:10 PM
 */
package bsc.beans;

import bsc.applet.BscMainPanel;

/**
 * @author  siga1996
 */
public class JBIHyperlinkLabel extends javax.swing.JLabel {

    String type = null,
            id = null,
            title = null;
    boolean setFoco = true;

    public void setFoco(boolean focus) {
        setFoco = focus;
    }

    public void setType(String typeAux) {
        type = String.valueOf(typeAux);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String idAux) {
        id = String.valueOf(idAux);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setTitle(String titleAux) {
        title = String.valueOf(titleAux);
        setText(title);
    }

    public String getTitle() {
        return String.valueOf(title);
    }

    /** Creates a new instance of JBIHyperlinkLabel */
    public JBIHyperlinkLabel() {
        super();
        BIHyperlinkListener listener = new BIHyperlinkListener(this);
        this.addMouseListener(listener);
    }

    public JBIHyperlinkLabel(boolean focus) {
        super();
        BIHyperlinkListener listener = new BIHyperlinkListener(this);
        this.addMouseListener(listener);
        setFoco(focus);
    }

    public JBIHyperlinkLabel(String typeAux, String idAux, String titleAux) {
        super();
        type = String.valueOf(typeAux);
        id = String.valueOf(idAux);
        title = String.valueOf(titleAux);
        this.setText(title);
        BIHyperlinkListener listener = new BIHyperlinkListener(this);
        this.addMouseListener(listener);
        setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 11));
    }
}

class BIHyperlinkListener extends javax.swing.event.MouseInputAdapter {

    JBIHyperlinkLabel label = null;
    bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();

    public BIHyperlinkListener(JBIHyperlinkLabel lbl) {
        label = lbl;
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e) {
        if (e.getClickCount() == 1 && label.isEnabled()) {
            label.setCursor(null);
            label.setText(this.label.getTitle());
            
            if (!(mainPanel.getMenuSelecionado().equals(BscMainPanel.ST_AREATRABALHO))) {
                try {
                    bsc.swing.BscDefaultFrameFunctions frame =
                            bsc.core.BscStaticReferences.getBscFormController().getForm(label.getType(), label.getID(), label.getText());
                } catch (bsc.core.BscFormControllerException exc) {
                    bsc.core.BscDebug.println(exc.getMessage());
                }
            }
        }
    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e) {
        if (label.isEnabled()) {
            
            if (!(mainPanel.getMenuSelecionado().equals(BscMainPanel.ST_AREATRABALHO))) {
                try {
                    // desvia o foco para o JBIHyperLink que o mouse esta apontando
                    if (label.setFoco) {
                        ((javax.swing.JInternalFrame) label.getRootPane().getParent()).setSelected(true);
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
                label.setText("<html><u>" + this.label.getTitle() + "</u></html>");
                label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            }
        }
    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e) {
        if (label.isEnabled()) {
            label.setCursor(null);
            label.setText(this.label.getTitle());
        }
    }
}