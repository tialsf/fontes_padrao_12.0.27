package bsc.beans;

import java.beans.*;

public class JBIHyperlinkLabelBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIHyperlinkLabel.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_displayedMnemonic = 5;
    private static final int PROPERTY_colorModel = 6;
    private static final int PROPERTY_focusTraversalPolicy = 7;
    private static final int PROPERTY_registeredKeyStrokes = 8;
    private static final int PROPERTY_alignmentX = 9;
    private static final int PROPERTY_paintingTile = 10;
    private static final int PROPERTY_alignmentY = 11;
    private static final int PROPERTY_hierarchyListeners = 12;
    private static final int PROPERTY_accessibleContext = 13;
    private static final int PROPERTY_preferredSize = 14;
    private static final int PROPERTY_managingFocus = 15;
    private static final int PROPERTY_minimumSizeSet = 16;
    private static final int PROPERTY_disabledIcon = 17;
    private static final int PROPERTY_focusTraversalPolicySet = 18;
    private static final int PROPERTY_y = 19;
    private static final int PROPERTY_labelFor = 20;
    private static final int PROPERTY_x = 21;
    private static final int PROPERTY_cursorSet = 22;
    private static final int PROPERTY_inputMethodRequests = 23;
    private static final int PROPERTY_containerListeners = 24;
    private static final int PROPERTY_insets = 25;
    private static final int PROPERTY_componentCount = 26;
    private static final int PROPERTY_components = 27;
    private static final int PROPERTY_inputVerifier = 28;
    private static final int PROPERTY_icon = 29;
    private static final int PROPERTY_hierarchyBoundsListeners = 30;
    private static final int PROPERTY_border = 31;
    private static final int PROPERTY_name = 32;
    private static final int PROPERTY_optimizedDrawingEnabled = 33;
    private static final int PROPERTY_graphics = 34;
    private static final int PROPERTY_minimumSize = 35;
    private static final int PROPERTY_toolTipText = 36;
    private static final int PROPERTY_focusTraversalKeysEnabled = 37;
    private static final int PROPERTY_foreground = 38;
    private static final int PROPERTY_horizontalTextPosition = 39;
    private static final int PROPERTY_ignoreRepaint = 40;
    private static final int PROPERTY_focusable = 41;
    private static final int PROPERTY_preferredSizeSet = 42;
    private static final int PROPERTY_visible = 43;
    private static final int PROPERTY_focusCycleRootAncestor = 44;
    private static final int PROPERTY_parent = 45;
    private static final int PROPERTY_rootPane = 46;
    private static final int PROPERTY_displayedMnemonicIndex = 47;
    private static final int PROPERTY_lightweight = 48;
    private static final int PROPERTY_width = 49;
    private static final int PROPERTY_keyListeners = 50;
    private static final int PROPERTY_toolkit = 51;
    private static final int PROPERTY_inputContext = 52;
    private static final int PROPERTY_layout = 53;
    private static final int PROPERTY_opaque = 54;
    private static final int PROPERTY_font = 55;
    private static final int PROPERTY_locale = 56;
    private static final int PROPERTY_cursor = 57;
    private static final int PROPERTY_inputMethodListeners = 58;
    private static final int PROPERTY_transferHandler = 59;
    private static final int PROPERTY_vetoableChangeListeners = 60;
    private static final int PROPERTY_iconTextGap = 61;
    private static final int PROPERTY_doubleBuffered = 62;
    private static final int PROPERTY_visibleRect = 63;
    private static final int PROPERTY_maximumSizeSet = 64;
    private static final int PROPERTY_horizontalAlignment = 65;
    private static final int PROPERTY_valid = 66;
    private static final int PROPERTY_focusCycleRoot = 67;
    private static final int PROPERTY_maximumSize = 68;
    private static final int PROPERTY_mouseMotionListeners = 69;
    private static final int PROPERTY_verticalTextPosition = 70;
    private static final int PROPERTY_treeLock = 71;
    private static final int PROPERTY_bounds = 72;
    private static final int PROPERTY_text = 73;
    private static final int PROPERTY_focusTraversable = 74;
    private static final int PROPERTY_propertyChangeListeners = 75;
    private static final int PROPERTY_autoscrolls = 76;
    private static final int PROPERTY_ID = 77;
    private static final int PROPERTY_componentListeners = 78;
    private static final int PROPERTY_showing = 79;
    private static final int PROPERTY_verticalAlignment = 80;
    private static final int PROPERTY_title = 81;
    private static final int PROPERTY_dropTarget = 82;
    private static final int PROPERTY_focusListeners = 83;
    private static final int PROPERTY_nextFocusableComponent = 84;
    private static final int PROPERTY_peer = 85;
    private static final int PROPERTY_height = 86;
    private static final int PROPERTY_topLevelAncestor = 87;
    private static final int PROPERTY_displayable = 88;
    private static final int PROPERTY_background = 89;
    private static final int PROPERTY_graphicsConfiguration = 90;
    private static final int PROPERTY_type = 91;
    private static final int PROPERTY_focusOwner = 92;
    private static final int PROPERTY_ancestorListeners = 93;
    private static final int PROPERTY_requestFocusEnabled = 94;
    private static final int PROPERTY_debugGraphicsOptions = 95;
    private static final int PROPERTY_backgroundSet = 96;
    private static final int PROPERTY_actionMap = 97;
    private static final int PROPERTY_mouseListeners = 98;
    private static final int PROPERTY_enabled = 99;
    private static final int PROPERTY_foregroundSet = 100;
    private static final int PROPERTY_validateRoot = 101;
    private static final int PROPERTY_UI = 102;
    private static final int PROPERTY_UIClassID = 103;
    private static final int PROPERTY_component = 104;
    private static final int PROPERTY_focusTraversalKeys = 105;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[106];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIHyperlinkLabel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIHyperlinkLabel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIHyperlinkLabel.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIHyperlinkLabel.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIHyperlinkLabel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_displayedMnemonic] = new PropertyDescriptor ( "displayedMnemonic", JBIHyperlinkLabel.class, null, "setDisplayedMnemonic" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIHyperlinkLabel.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIHyperlinkLabel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIHyperlinkLabel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIHyperlinkLabel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIHyperlinkLabel.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIHyperlinkLabel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIHyperlinkLabel.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIHyperlinkLabel.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIHyperlinkLabel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIHyperlinkLabel.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIHyperlinkLabel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_disabledIcon] = new PropertyDescriptor ( "disabledIcon", JBIHyperlinkLabel.class, "getDisabledIcon", "setDisabledIcon" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIHyperlinkLabel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIHyperlinkLabel.class, "getY", null );
            properties[PROPERTY_labelFor] = new PropertyDescriptor ( "labelFor", JBIHyperlinkLabel.class, "getLabelFor", "setLabelFor" );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIHyperlinkLabel.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIHyperlinkLabel.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIHyperlinkLabel.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIHyperlinkLabel.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIHyperlinkLabel.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIHyperlinkLabel.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIHyperlinkLabel.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIHyperlinkLabel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_icon] = new PropertyDescriptor ( "icon", JBIHyperlinkLabel.class, "getIcon", "setIcon" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIHyperlinkLabel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIHyperlinkLabel.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIHyperlinkLabel.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIHyperlinkLabel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIHyperlinkLabel.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIHyperlinkLabel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIHyperlinkLabel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIHyperlinkLabel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIHyperlinkLabel.class, "getForeground", "setForeground" );
            properties[PROPERTY_horizontalTextPosition] = new PropertyDescriptor ( "horizontalTextPosition", JBIHyperlinkLabel.class, "getHorizontalTextPosition", "setHorizontalTextPosition" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIHyperlinkLabel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIHyperlinkLabel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIHyperlinkLabel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIHyperlinkLabel.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIHyperlinkLabel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIHyperlinkLabel.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIHyperlinkLabel.class, "getRootPane", null );
            properties[PROPERTY_displayedMnemonicIndex] = new PropertyDescriptor ( "displayedMnemonicIndex", JBIHyperlinkLabel.class, "getDisplayedMnemonicIndex", "setDisplayedMnemonicIndex" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIHyperlinkLabel.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIHyperlinkLabel.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIHyperlinkLabel.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIHyperlinkLabel.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIHyperlinkLabel.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIHyperlinkLabel.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIHyperlinkLabel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIHyperlinkLabel.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIHyperlinkLabel.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIHyperlinkLabel.class, "getCursor", "setCursor" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIHyperlinkLabel.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIHyperlinkLabel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIHyperlinkLabel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_iconTextGap] = new PropertyDescriptor ( "iconTextGap", JBIHyperlinkLabel.class, "getIconTextGap", "setIconTextGap" );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIHyperlinkLabel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIHyperlinkLabel.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIHyperlinkLabel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_horizontalAlignment] = new PropertyDescriptor ( "horizontalAlignment", JBIHyperlinkLabel.class, "getHorizontalAlignment", "setHorizontalAlignment" );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIHyperlinkLabel.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIHyperlinkLabel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIHyperlinkLabel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIHyperlinkLabel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_verticalTextPosition] = new PropertyDescriptor ( "verticalTextPosition", JBIHyperlinkLabel.class, "getVerticalTextPosition", "setVerticalTextPosition" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIHyperlinkLabel.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIHyperlinkLabel.class, "getBounds", "setBounds" );
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", JBIHyperlinkLabel.class, "getText", "setText" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIHyperlinkLabel.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIHyperlinkLabel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIHyperlinkLabel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_ID] = new PropertyDescriptor ( "ID", JBIHyperlinkLabel.class, "getID", "setID" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIHyperlinkLabel.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIHyperlinkLabel.class, "isShowing", null );
            properties[PROPERTY_verticalAlignment] = new PropertyDescriptor ( "verticalAlignment", JBIHyperlinkLabel.class, "getVerticalAlignment", "setVerticalAlignment" );
            properties[PROPERTY_title] = new PropertyDescriptor ( "title", JBIHyperlinkLabel.class, "getTitle", "setTitle" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIHyperlinkLabel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIHyperlinkLabel.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIHyperlinkLabel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIHyperlinkLabel.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIHyperlinkLabel.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIHyperlinkLabel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIHyperlinkLabel.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIHyperlinkLabel.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIHyperlinkLabel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_type] = new PropertyDescriptor ( "type", JBIHyperlinkLabel.class, "getType", "setType" );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIHyperlinkLabel.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIHyperlinkLabel.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIHyperlinkLabel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIHyperlinkLabel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIHyperlinkLabel.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIHyperlinkLabel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIHyperlinkLabel.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIHyperlinkLabel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIHyperlinkLabel.class, "isForegroundSet", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIHyperlinkLabel.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIHyperlinkLabel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIHyperlinkLabel.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIHyperlinkLabel.class, null, null, "getComponent", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIHyperlinkLabel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_containerListener = 0;
    private static final int EVENT_hierarchyListener = 1;
    private static final int EVENT_mouseMotionListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_inputMethodListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_keyListener = 6;
    private static final int EVENT_mouseWheelListener = 7;
    private static final int EVENT_ancestorListener = 8;
    private static final int EVENT_propertyChangeListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyTyped", "keyReleased"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseExited", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIHyperlinkLabel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_imageUpdate0 = 0;
    private static final int METHOD_getDisplayedMnemonic1 = 1;
    private static final int METHOD_setDisplayedMnemonic2 = 2;
    private static final int METHOD_updateUI3 = 3;
    private static final int METHOD_getPropertyChangeListeners4 = 4;
    private static final int METHOD_scrollRectToVisible5 = 5;
    private static final int METHOD_getClientProperty6 = 6;
    private static final int METHOD_print7 = 7;
    private static final int METHOD_unregisterKeyboardAction8 = 8;
    private static final int METHOD_removePropertyChangeListener9 = 9;
    private static final int METHOD_getConditionForKeyStroke10 = 10;
    private static final int METHOD_requestFocus11 = 11;
    private static final int METHOD_paint12 = 12;
    private static final int METHOD_paintImmediately13 = 13;
    private static final int METHOD_requestDefaultFocus14 = 14;
    private static final int METHOD_getToolTipText15 = 15;
    private static final int METHOD_requestFocusInWindow16 = 16;
    private static final int METHOD_repaint17 = 17;
    private static final int METHOD_getInputMap18 = 18;
    private static final int METHOD_getSize19 = 19;
    private static final int METHOD_requestFocus20 = 20;
    private static final int METHOD_firePropertyChange21 = 21;
    private static final int METHOD_isLightweightComponent22 = 22;
    private static final int METHOD_getInputMap23 = 23;
    private static final int METHOD_firePropertyChange24 = 24;
    private static final int METHOD_printAll25 = 25;
    private static final int METHOD_computeVisibleRect26 = 26;
    private static final int METHOD_getLocation27 = 27;
    private static final int METHOD_disable28 = 28;
    private static final int METHOD_paintImmediately29 = 29;
    private static final int METHOD_firePropertyChange30 = 30;
    private static final int METHOD_addNotify31 = 31;
    private static final int METHOD_getToolTipLocation32 = 32;
    private static final int METHOD_getListeners33 = 33;
    private static final int METHOD_firePropertyChange34 = 34;
    private static final int METHOD_reshape35 = 35;
    private static final int METHOD_grabFocus36 = 36;
    private static final int METHOD_getBounds37 = 37;
    private static final int METHOD_registerKeyboardAction38 = 38;
    private static final int METHOD_registerKeyboardAction39 = 39;
    private static final int METHOD_enable40 = 40;
    private static final int METHOD_addPropertyChangeListener41 = 41;
    private static final int METHOD_update42 = 42;
    private static final int METHOD_getActionForKeyStroke43 = 43;
    private static final int METHOD_removeNotify44 = 44;
    private static final int METHOD_firePropertyChange45 = 45;
    private static final int METHOD_firePropertyChange46 = 46;
    private static final int METHOD_getInsets47 = 47;
    private static final int METHOD_getDefaultLocale48 = 48;
    private static final int METHOD_putClientProperty49 = 49;
    private static final int METHOD_createToolTip50 = 50;
    private static final int METHOD_contains51 = 51;
    private static final int METHOD_revalidate52 = 52;
    private static final int METHOD_repaint53 = 53;
    private static final int METHOD_resetKeyboardActions54 = 54;
    private static final int METHOD_setDefaultLocale55 = 55;
    private static final int METHOD_firePropertyChange56 = 56;
    private static final int METHOD_setInputMap57 = 57;
    private static final int METHOD_firePropertyChange58 = 58;
    private static final int METHOD_applyComponentOrientation59 = 59;
    private static final int METHOD_add60 = 60;
    private static final int METHOD_validate61 = 61;
    private static final int METHOD_getComponentAt62 = 62;
    private static final int METHOD_removeAll63 = 63;
    private static final int METHOD_remove64 = 64;
    private static final int METHOD_preferredSize65 = 65;
    private static final int METHOD_remove66 = 66;
    private static final int METHOD_areFocusTraversalKeysSet67 = 67;
    private static final int METHOD_list68 = 68;
    private static final int METHOD_printComponents69 = 69;
    private static final int METHOD_paintComponents70 = 70;
    private static final int METHOD_insets71 = 71;
    private static final int METHOD_findComponentAt72 = 72;
    private static final int METHOD_deliverEvent73 = 73;
    private static final int METHOD_locate74 = 74;
    private static final int METHOD_getComponentAt75 = 75;
    private static final int METHOD_transferFocusBackward76 = 76;
    private static final int METHOD_minimumSize77 = 77;
    private static final int METHOD_add78 = 78;
    private static final int METHOD_add79 = 79;
    private static final int METHOD_add80 = 80;
    private static final int METHOD_layout81 = 81;
    private static final int METHOD_transferFocusDownCycle82 = 82;
    private static final int METHOD_isFocusCycleRoot83 = 83;
    private static final int METHOD_add84 = 84;
    private static final int METHOD_invalidate85 = 85;
    private static final int METHOD_list86 = 86;
    private static final int METHOD_countComponents87 = 87;
    private static final int METHOD_isAncestorOf88 = 88;
    private static final int METHOD_doLayout89 = 89;
    private static final int METHOD_findComponentAt90 = 90;
    private static final int METHOD_getFontMetrics91 = 91;
    private static final int METHOD_location92 = 92;
    private static final int METHOD_transferFocusUpCycle93 = 93;
    private static final int METHOD_postEvent94 = 94;
    private static final int METHOD_contains95 = 95;
    private static final int METHOD_gotFocus96 = 96;
    private static final int METHOD_setSize97 = 97;
    private static final int METHOD_list98 = 98;
    private static final int METHOD_hide99 = 99;
    private static final int METHOD_checkImage100 = 100;
    private static final int METHOD_checkImage101 = 101;
    private static final int METHOD_hasFocus102 = 102;
    private static final int METHOD_setLocation103 = 103;
    private static final int METHOD_mouseEnter104 = 104;
    private static final int METHOD_size105 = 105;
    private static final int METHOD_keyDown106 = 106;
    private static final int METHOD_move107 = 107;
    private static final int METHOD_inside108 = 108;
    private static final int METHOD_list109 = 109;
    private static final int METHOD_toString110 = 110;
    private static final int METHOD_keyUp111 = 111;
    private static final int METHOD_dispatchEvent112 = 112;
    private static final int METHOD_mouseDrag113 = 113;
    private static final int METHOD_setLocation114 = 114;
    private static final int METHOD_createImage115 = 115;
    private static final int METHOD_show116 = 116;
    private static final int METHOD_add117 = 117;
    private static final int METHOD_show118 = 118;
    private static final int METHOD_mouseUp119 = 119;
    private static final int METHOD_lostFocus120 = 120;
    private static final int METHOD_resize121 = 121;
    private static final int METHOD_mouseDown122 = 122;
    private static final int METHOD_action123 = 123;
    private static final int METHOD_createVolatileImage124 = 124;
    private static final int METHOD_nextFocus125 = 125;
    private static final int METHOD_getLocation126 = 126;
    private static final int METHOD_paintAll127 = 127;
    private static final int METHOD_createVolatileImage128 = 128;
    private static final int METHOD_createImage129 = 129;
    private static final int METHOD_handleEvent130 = 130;
    private static final int METHOD_repaint131 = 131;
    private static final int METHOD_repaint132 = 132;
    private static final int METHOD_mouseExit133 = 133;
    private static final int METHOD_prepareImage134 = 134;
    private static final int METHOD_prepareImage135 = 135;
    private static final int METHOD_remove136 = 136;
    private static final int METHOD_setSize137 = 137;
    private static final int METHOD_enable138 = 138;
    private static final int METHOD_transferFocus139 = 139;
    private static final int METHOD_mouseMove140 = 140;
    private static final int METHOD_getSize141 = 141;
    private static final int METHOD_repaint142 = 142;
    private static final int METHOD_enableInputMethods143 = 143;
    private static final int METHOD_bounds144 = 144;
    private static final int METHOD_setBounds145 = 145;
    private static final int METHOD_resize146 = 146;
    private static final int METHOD_list147 = 147;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[148];
    
        try {
            methods[METHOD_imageUpdate0] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate0].setDisplayName ( "" );
            methods[METHOD_getDisplayedMnemonic1] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getDisplayedMnemonic", new Class[] {}));
            methods[METHOD_getDisplayedMnemonic1].setDisplayName ( "" );
            methods[METHOD_setDisplayedMnemonic2] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setDisplayedMnemonic", new Class[] {Integer.TYPE}));
            methods[METHOD_setDisplayedMnemonic2].setDisplayName ( "" );
            methods[METHOD_updateUI3] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI3].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners4] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners4].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible5] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible5].setDisplayName ( "" );
            methods[METHOD_getClientProperty6] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty6].setDisplayName ( "" );
            methods[METHOD_print7] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print7].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction8] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction8].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener9] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener9].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke10] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke10].setDisplayName ( "" );
            methods[METHOD_requestFocus11] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus11].setDisplayName ( "" );
            methods[METHOD_paint12] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint12].setDisplayName ( "" );
            methods[METHOD_paintImmediately13] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately13].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus14] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus14].setDisplayName ( "" );
            methods[METHOD_getToolTipText15] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText15].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow16] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow16].setDisplayName ( "" );
            methods[METHOD_repaint17] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint17].setDisplayName ( "" );
            methods[METHOD_getInputMap18] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap18].setDisplayName ( "" );
            methods[METHOD_getSize19] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize19].setDisplayName ( "" );
            methods[METHOD_requestFocus20] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus20].setDisplayName ( "" );
            methods[METHOD_firePropertyChange21] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange21].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent22] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent22].setDisplayName ( "" );
            methods[METHOD_getInputMap23] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap23].setDisplayName ( "" );
            methods[METHOD_firePropertyChange24] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange24].setDisplayName ( "" );
            methods[METHOD_printAll25] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll25].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect26] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect26].setDisplayName ( "" );
            methods[METHOD_getLocation27] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation27].setDisplayName ( "" );
            methods[METHOD_disable28] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable28].setDisplayName ( "" );
            methods[METHOD_paintImmediately29] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately29].setDisplayName ( "" );
            methods[METHOD_firePropertyChange30] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange30].setDisplayName ( "" );
            methods[METHOD_addNotify31] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify31].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation32] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation32].setDisplayName ( "" );
            methods[METHOD_getListeners33] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners33].setDisplayName ( "" );
            methods[METHOD_firePropertyChange34] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange34].setDisplayName ( "" );
            methods[METHOD_reshape35] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape35].setDisplayName ( "" );
            methods[METHOD_grabFocus36] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus36].setDisplayName ( "" );
            methods[METHOD_getBounds37] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds37].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction38] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction38].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction39] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction39].setDisplayName ( "" );
            methods[METHOD_enable40] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable40].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener41] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener41].setDisplayName ( "" );
            methods[METHOD_update42] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update42].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke43] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke43].setDisplayName ( "" );
            methods[METHOD_removeNotify44] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify44].setDisplayName ( "" );
            methods[METHOD_firePropertyChange45] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange45].setDisplayName ( "" );
            methods[METHOD_firePropertyChange46] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange46].setDisplayName ( "" );
            methods[METHOD_getInsets47] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets47].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale48] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale48].setDisplayName ( "" );
            methods[METHOD_putClientProperty49] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty49].setDisplayName ( "" );
            methods[METHOD_createToolTip50] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip50].setDisplayName ( "" );
            methods[METHOD_contains51] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains51].setDisplayName ( "" );
            methods[METHOD_revalidate52] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate52].setDisplayName ( "" );
            methods[METHOD_repaint53] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint53].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions54] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions54].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale55] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale55].setDisplayName ( "" );
            methods[METHOD_firePropertyChange56] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange56].setDisplayName ( "" );
            methods[METHOD_setInputMap57] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap57].setDisplayName ( "" );
            methods[METHOD_firePropertyChange58] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange58].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation59] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation59].setDisplayName ( "" );
            methods[METHOD_add60] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add60].setDisplayName ( "" );
            methods[METHOD_validate61] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate61].setDisplayName ( "" );
            methods[METHOD_getComponentAt62] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt62].setDisplayName ( "" );
            methods[METHOD_removeAll63] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll63].setDisplayName ( "" );
            methods[METHOD_remove64] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove64].setDisplayName ( "" );
            methods[METHOD_preferredSize65] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize65].setDisplayName ( "" );
            methods[METHOD_remove66] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove66].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet67] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet67].setDisplayName ( "" );
            methods[METHOD_list68] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list68].setDisplayName ( "" );
            methods[METHOD_printComponents69] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents69].setDisplayName ( "" );
            methods[METHOD_paintComponents70] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents70].setDisplayName ( "" );
            methods[METHOD_insets71] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets71].setDisplayName ( "" );
            methods[METHOD_findComponentAt72] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt72].setDisplayName ( "" );
            methods[METHOD_deliverEvent73] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent73].setDisplayName ( "" );
            methods[METHOD_locate74] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate74].setDisplayName ( "" );
            methods[METHOD_getComponentAt75] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt75].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward76] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward76].setDisplayName ( "" );
            methods[METHOD_minimumSize77] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize77].setDisplayName ( "" );
            methods[METHOD_add78] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add78].setDisplayName ( "" );
            methods[METHOD_add79] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add79].setDisplayName ( "" );
            methods[METHOD_add80] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add80].setDisplayName ( "" );
            methods[METHOD_layout81] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout81].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle82] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle82].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot83] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot83].setDisplayName ( "" );
            methods[METHOD_add84] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add84].setDisplayName ( "" );
            methods[METHOD_invalidate85] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate85].setDisplayName ( "" );
            methods[METHOD_list86] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list86].setDisplayName ( "" );
            methods[METHOD_countComponents87] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents87].setDisplayName ( "" );
            methods[METHOD_isAncestorOf88] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf88].setDisplayName ( "" );
            methods[METHOD_doLayout89] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout89].setDisplayName ( "" );
            methods[METHOD_findComponentAt90] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt90].setDisplayName ( "" );
            methods[METHOD_getFontMetrics91] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics91].setDisplayName ( "" );
            methods[METHOD_location92] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location92].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle93] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle93].setDisplayName ( "" );
            methods[METHOD_postEvent94] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent94].setDisplayName ( "" );
            methods[METHOD_contains95] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains95].setDisplayName ( "" );
            methods[METHOD_gotFocus96] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus96].setDisplayName ( "" );
            methods[METHOD_setSize97] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize97].setDisplayName ( "" );
            methods[METHOD_list98] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list98].setDisplayName ( "" );
            methods[METHOD_hide99] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide99].setDisplayName ( "" );
            methods[METHOD_checkImage100] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage100].setDisplayName ( "" );
            methods[METHOD_checkImage101] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage101].setDisplayName ( "" );
            methods[METHOD_hasFocus102] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus102].setDisplayName ( "" );
            methods[METHOD_setLocation103] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation103].setDisplayName ( "" );
            methods[METHOD_mouseEnter104] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter104].setDisplayName ( "" );
            methods[METHOD_size105] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size105].setDisplayName ( "" );
            methods[METHOD_keyDown106] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown106].setDisplayName ( "" );
            methods[METHOD_move107] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move107].setDisplayName ( "" );
            methods[METHOD_inside108] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside108].setDisplayName ( "" );
            methods[METHOD_list109] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list109].setDisplayName ( "" );
            methods[METHOD_toString110] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString110].setDisplayName ( "" );
            methods[METHOD_keyUp111] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp111].setDisplayName ( "" );
            methods[METHOD_dispatchEvent112] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent112].setDisplayName ( "" );
            methods[METHOD_mouseDrag113] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag113].setDisplayName ( "" );
            methods[METHOD_setLocation114] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation114].setDisplayName ( "" );
            methods[METHOD_createImage115] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage115].setDisplayName ( "" );
            methods[METHOD_show116] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show116].setDisplayName ( "" );
            methods[METHOD_add117] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add117].setDisplayName ( "" );
            methods[METHOD_show118] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show118].setDisplayName ( "" );
            methods[METHOD_mouseUp119] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp119].setDisplayName ( "" );
            methods[METHOD_lostFocus120] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus120].setDisplayName ( "" );
            methods[METHOD_resize121] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize121].setDisplayName ( "" );
            methods[METHOD_mouseDown122] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown122].setDisplayName ( "" );
            methods[METHOD_action123] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action123].setDisplayName ( "" );
            methods[METHOD_createVolatileImage124] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage124].setDisplayName ( "" );
            methods[METHOD_nextFocus125] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus125].setDisplayName ( "" );
            methods[METHOD_getLocation126] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation126].setDisplayName ( "" );
            methods[METHOD_paintAll127] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll127].setDisplayName ( "" );
            methods[METHOD_createVolatileImage128] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage128].setDisplayName ( "" );
            methods[METHOD_createImage129] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage129].setDisplayName ( "" );
            methods[METHOD_handleEvent130] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent130].setDisplayName ( "" );
            methods[METHOD_repaint131] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint131].setDisplayName ( "" );
            methods[METHOD_repaint132] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint132].setDisplayName ( "" );
            methods[METHOD_mouseExit133] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit133].setDisplayName ( "" );
            methods[METHOD_prepareImage134] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage134].setDisplayName ( "" );
            methods[METHOD_prepareImage135] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage135].setDisplayName ( "" );
            methods[METHOD_remove136] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove136].setDisplayName ( "" );
            methods[METHOD_setSize137] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize137].setDisplayName ( "" );
            methods[METHOD_enable138] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable138].setDisplayName ( "" );
            methods[METHOD_transferFocus139] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus139].setDisplayName ( "" );
            methods[METHOD_mouseMove140] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove140].setDisplayName ( "" );
            methods[METHOD_getSize141] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize141].setDisplayName ( "" );
            methods[METHOD_repaint142] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint142].setDisplayName ( "" );
            methods[METHOD_enableInputMethods143] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods143].setDisplayName ( "" );
            methods[METHOD_bounds144] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds144].setDisplayName ( "" );
            methods[METHOD_setBounds145] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds145].setDisplayName ( "" );
            methods[METHOD_resize146] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize146].setDisplayName ( "" );
            methods[METHOD_list147] = new MethodDescriptor ( bsc.beans.JBIHyperlinkLabel.class.getMethod("list", new Class[] {}));
            methods[METHOD_list147].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

