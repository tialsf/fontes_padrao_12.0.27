package bsc.beans;

import java.beans.*;

public class JBILayeredPaneBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBILayeredPane.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_focusTraversalPolicy = 6;
    private static final int PROPERTY_registeredKeyStrokes = 7;
    private static final int PROPERTY_alignmentX = 8;
    private static final int PROPERTY_paintingTile = 9;
    private static final int PROPERTY_alignmentY = 10;
    private static final int PROPERTY_hierarchyListeners = 11;
    private static final int PROPERTY_accessibleContext = 12;
    private static final int PROPERTY_preferredSize = 13;
    private static final int PROPERTY_managingFocus = 14;
    private static final int PROPERTY_minimumSizeSet = 15;
    private static final int PROPERTY_focusTraversalPolicySet = 16;
    private static final int PROPERTY_y = 17;
    private static final int PROPERTY_x = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_inputMethodRequests = 20;
    private static final int PROPERTY_containerListeners = 21;
    private static final int PROPERTY_insets = 22;
    private static final int PROPERTY_componentCount = 23;
    private static final int PROPERTY_components = 24;
    private static final int PROPERTY_inputVerifier = 25;
    private static final int PROPERTY_hierarchyBoundsListeners = 26;
    private static final int PROPERTY_border = 27;
    private static final int PROPERTY_name = 28;
    private static final int PROPERTY_optimizedDrawingEnabled = 29;
    private static final int PROPERTY_graphics = 30;
    private static final int PROPERTY_minimumSize = 31;
    private static final int PROPERTY_toolTipText = 32;
    private static final int PROPERTY_focusTraversalKeysEnabled = 33;
    private static final int PROPERTY_foreground = 34;
    private static final int PROPERTY_ignoreRepaint = 35;
    private static final int PROPERTY_focusable = 36;
    private static final int PROPERTY_preferredSizeSet = 37;
    private static final int PROPERTY_visible = 38;
    private static final int PROPERTY_focusCycleRootAncestor = 39;
    private static final int PROPERTY_parent = 40;
    private static final int PROPERTY_rootPane = 41;
    private static final int PROPERTY_lightweight = 42;
    private static final int PROPERTY_width = 43;
    private static final int PROPERTY_keyListeners = 44;
    private static final int PROPERTY_toolkit = 45;
    private static final int PROPERTY_inputContext = 46;
    private static final int PROPERTY_layout = 47;
    private static final int PROPERTY_opaque = 48;
    private static final int PROPERTY_font = 49;
    private static final int PROPERTY_locale = 50;
    private static final int PROPERTY_cursor = 51;
    private static final int PROPERTY_inputMethodListeners = 52;
    private static final int PROPERTY_transferHandler = 53;
    private static final int PROPERTY_vetoableChangeListeners = 54;
    private static final int PROPERTY_doubleBuffered = 55;
    private static final int PROPERTY_visibleRect = 56;
    private static final int PROPERTY_maximumSizeSet = 57;
    private static final int PROPERTY_valid = 58;
    private static final int PROPERTY_focusCycleRoot = 59;
    private static final int PROPERTY_maximumSize = 60;
    private static final int PROPERTY_mouseMotionListeners = 61;
    private static final int PROPERTY_treeLock = 62;
    private static final int PROPERTY_bounds = 63;
    private static final int PROPERTY_focusTraversable = 64;
    private static final int PROPERTY_propertyChangeListeners = 65;
    private static final int PROPERTY_autoscrolls = 66;
    private static final int PROPERTY_componentListeners = 67;
    private static final int PROPERTY_showing = 68;
    private static final int PROPERTY_dropTarget = 69;
    private static final int PROPERTY_focusListeners = 70;
    private static final int PROPERTY_nextFocusableComponent = 71;
    private static final int PROPERTY_peer = 72;
    private static final int PROPERTY_height = 73;
    private static final int PROPERTY_topLevelAncestor = 74;
    private static final int PROPERTY_displayable = 75;
    private static final int PROPERTY_background = 76;
    private static final int PROPERTY_graphicsConfiguration = 77;
    private static final int PROPERTY_focusOwner = 78;
    private static final int PROPERTY_ancestorListeners = 79;
    private static final int PROPERTY_requestFocusEnabled = 80;
    private static final int PROPERTY_debugGraphicsOptions = 81;
    private static final int PROPERTY_backgroundSet = 82;
    private static final int PROPERTY_actionMap = 83;
    private static final int PROPERTY_mouseListeners = 84;
    private static final int PROPERTY_enabled = 85;
    private static final int PROPERTY_foregroundSet = 86;
    private static final int PROPERTY_validateRoot = 87;
    private static final int PROPERTY_UIClassID = 88;
    private static final int PROPERTY_component = 89;
    private static final int PROPERTY_componentsInLayer = 90;
    private static final int PROPERTY_componentCountInLayer = 91;
    private static final int PROPERTY_focusTraversalKeys = 92;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[93];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBILayeredPane.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBILayeredPane.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBILayeredPane.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBILayeredPane.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBILayeredPane.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBILayeredPane.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBILayeredPane.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBILayeredPane.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBILayeredPane.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBILayeredPane.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBILayeredPane.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBILayeredPane.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBILayeredPane.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBILayeredPane.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBILayeredPane.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBILayeredPane.class, "isMinimumSizeSet", null );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBILayeredPane.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBILayeredPane.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBILayeredPane.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBILayeredPane.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBILayeredPane.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBILayeredPane.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBILayeredPane.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBILayeredPane.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBILayeredPane.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBILayeredPane.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBILayeredPane.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBILayeredPane.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBILayeredPane.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBILayeredPane.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBILayeredPane.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBILayeredPane.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBILayeredPane.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBILayeredPane.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBILayeredPane.class, "getForeground", "setForeground" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBILayeredPane.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBILayeredPane.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBILayeredPane.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBILayeredPane.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBILayeredPane.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBILayeredPane.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBILayeredPane.class, "getRootPane", null );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBILayeredPane.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBILayeredPane.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBILayeredPane.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBILayeredPane.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBILayeredPane.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBILayeredPane.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBILayeredPane.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBILayeredPane.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBILayeredPane.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBILayeredPane.class, "getCursor", "setCursor" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBILayeredPane.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBILayeredPane.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBILayeredPane.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBILayeredPane.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBILayeredPane.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBILayeredPane.class, "isMaximumSizeSet", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBILayeredPane.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBILayeredPane.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBILayeredPane.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBILayeredPane.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBILayeredPane.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBILayeredPane.class, "getBounds", "setBounds" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBILayeredPane.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBILayeredPane.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBILayeredPane.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBILayeredPane.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBILayeredPane.class, "isShowing", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBILayeredPane.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBILayeredPane.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBILayeredPane.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBILayeredPane.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBILayeredPane.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBILayeredPane.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBILayeredPane.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBILayeredPane.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBILayeredPane.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBILayeredPane.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBILayeredPane.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBILayeredPane.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBILayeredPane.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBILayeredPane.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBILayeredPane.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBILayeredPane.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBILayeredPane.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBILayeredPane.class, "isForegroundSet", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBILayeredPane.class, "isValidateRoot", null );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBILayeredPane.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBILayeredPane.class, null, null, "getComponent", null );
            properties[PROPERTY_componentsInLayer] = new IndexedPropertyDescriptor ( "componentsInLayer", JBILayeredPane.class, null, null, "getComponentsInLayer", null );
            properties[PROPERTY_componentCountInLayer] = new IndexedPropertyDescriptor ( "componentCountInLayer", JBILayeredPane.class, null, null, "getComponentCountInLayer", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBILayeredPane.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_containerListener = 0;
    private static final int EVENT_hierarchyListener = 1;
    private static final int EVENT_mouseMotionListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_inputMethodListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_keyListener = 6;
    private static final int EVENT_mouseWheelListener = 7;
    private static final int EVENT_ancestorListener = 8;
    private static final int EVENT_propertyChangeListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyTyped", "keyReleased"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseExited", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBILayeredPane.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_printLayeredPane0 = 0;
    private static final int METHOD_print1 = 1;
    private static final int METHOD_disableDoubleBuffering2 = 2;
    private static final int METHOD_enableDoubleBuffering3 = 3;
    private static final int METHOD_getLayer4 = 4;
    private static final int METHOD_getIndexOf5 = 5;
    private static final int METHOD_getPosition6 = 6;
    private static final int METHOD_setLayer7 = 7;
    private static final int METHOD_setLayer8 = 8;
    private static final int METHOD_highestLayer9 = 9;
    private static final int METHOD_moveToBack10 = 10;
    private static final int METHOD_getLayer11 = 11;
    private static final int METHOD_setPosition12 = 12;
    private static final int METHOD_paint13 = 13;
    private static final int METHOD_lowestLayer14 = 14;
    private static final int METHOD_remove15 = 15;
    private static final int METHOD_getLayeredPaneAbove16 = 16;
    private static final int METHOD_moveToFront17 = 17;
    private static final int METHOD_putLayer18 = 18;
    private static final int METHOD_getPropertyChangeListeners19 = 19;
    private static final int METHOD_scrollRectToVisible20 = 20;
    private static final int METHOD_getClientProperty21 = 21;
    private static final int METHOD_print22 = 22;
    private static final int METHOD_unregisterKeyboardAction23 = 23;
    private static final int METHOD_removePropertyChangeListener24 = 24;
    private static final int METHOD_getConditionForKeyStroke25 = 25;
    private static final int METHOD_updateUI26 = 26;
    private static final int METHOD_requestFocus27 = 27;
    private static final int METHOD_paintImmediately28 = 28;
    private static final int METHOD_requestDefaultFocus29 = 29;
    private static final int METHOD_getToolTipText30 = 30;
    private static final int METHOD_requestFocusInWindow31 = 31;
    private static final int METHOD_repaint32 = 32;
    private static final int METHOD_getInputMap33 = 33;
    private static final int METHOD_getSize34 = 34;
    private static final int METHOD_requestFocus35 = 35;
    private static final int METHOD_firePropertyChange36 = 36;
    private static final int METHOD_isLightweightComponent37 = 37;
    private static final int METHOD_getInputMap38 = 38;
    private static final int METHOD_firePropertyChange39 = 39;
    private static final int METHOD_printAll40 = 40;
    private static final int METHOD_computeVisibleRect41 = 41;
    private static final int METHOD_getLocation42 = 42;
    private static final int METHOD_disable43 = 43;
    private static final int METHOD_paintImmediately44 = 44;
    private static final int METHOD_firePropertyChange45 = 45;
    private static final int METHOD_addNotify46 = 46;
    private static final int METHOD_getToolTipLocation47 = 47;
    private static final int METHOD_getListeners48 = 48;
    private static final int METHOD_firePropertyChange49 = 49;
    private static final int METHOD_reshape50 = 50;
    private static final int METHOD_grabFocus51 = 51;
    private static final int METHOD_getBounds52 = 52;
    private static final int METHOD_registerKeyboardAction53 = 53;
    private static final int METHOD_registerKeyboardAction54 = 54;
    private static final int METHOD_enable55 = 55;
    private static final int METHOD_addPropertyChangeListener56 = 56;
    private static final int METHOD_update57 = 57;
    private static final int METHOD_getActionForKeyStroke58 = 58;
    private static final int METHOD_removeNotify59 = 59;
    private static final int METHOD_firePropertyChange60 = 60;
    private static final int METHOD_firePropertyChange61 = 61;
    private static final int METHOD_getInsets62 = 62;
    private static final int METHOD_getDefaultLocale63 = 63;
    private static final int METHOD_putClientProperty64 = 64;
    private static final int METHOD_createToolTip65 = 65;
    private static final int METHOD_contains66 = 66;
    private static final int METHOD_revalidate67 = 67;
    private static final int METHOD_repaint68 = 68;
    private static final int METHOD_resetKeyboardActions69 = 69;
    private static final int METHOD_setDefaultLocale70 = 70;
    private static final int METHOD_firePropertyChange71 = 71;
    private static final int METHOD_setInputMap72 = 72;
    private static final int METHOD_firePropertyChange73 = 73;
    private static final int METHOD_applyComponentOrientation74 = 74;
    private static final int METHOD_add75 = 75;
    private static final int METHOD_validate76 = 76;
    private static final int METHOD_getComponentAt77 = 77;
    private static final int METHOD_removeAll78 = 78;
    private static final int METHOD_remove79 = 79;
    private static final int METHOD_preferredSize80 = 80;
    private static final int METHOD_areFocusTraversalKeysSet81 = 81;
    private static final int METHOD_list82 = 82;
    private static final int METHOD_printComponents83 = 83;
    private static final int METHOD_paintComponents84 = 84;
    private static final int METHOD_insets85 = 85;
    private static final int METHOD_findComponentAt86 = 86;
    private static final int METHOD_deliverEvent87 = 87;
    private static final int METHOD_locate88 = 88;
    private static final int METHOD_getComponentAt89 = 89;
    private static final int METHOD_transferFocusBackward90 = 90;
    private static final int METHOD_minimumSize91 = 91;
    private static final int METHOD_add92 = 92;
    private static final int METHOD_add93 = 93;
    private static final int METHOD_add94 = 94;
    private static final int METHOD_layout95 = 95;
    private static final int METHOD_transferFocusDownCycle96 = 96;
    private static final int METHOD_isFocusCycleRoot97 = 97;
    private static final int METHOD_add98 = 98;
    private static final int METHOD_invalidate99 = 99;
    private static final int METHOD_list100 = 100;
    private static final int METHOD_countComponents101 = 101;
    private static final int METHOD_isAncestorOf102 = 102;
    private static final int METHOD_doLayout103 = 103;
    private static final int METHOD_findComponentAt104 = 104;
    private static final int METHOD_getFontMetrics105 = 105;
    private static final int METHOD_location106 = 106;
    private static final int METHOD_transferFocusUpCycle107 = 107;
    private static final int METHOD_postEvent108 = 108;
    private static final int METHOD_contains109 = 109;
    private static final int METHOD_gotFocus110 = 110;
    private static final int METHOD_setSize111 = 111;
    private static final int METHOD_list112 = 112;
    private static final int METHOD_hide113 = 113;
    private static final int METHOD_checkImage114 = 114;
    private static final int METHOD_checkImage115 = 115;
    private static final int METHOD_hasFocus116 = 116;
    private static final int METHOD_setLocation117 = 117;
    private static final int METHOD_mouseEnter118 = 118;
    private static final int METHOD_size119 = 119;
    private static final int METHOD_keyDown120 = 120;
    private static final int METHOD_move121 = 121;
    private static final int METHOD_inside122 = 122;
    private static final int METHOD_list123 = 123;
    private static final int METHOD_toString124 = 124;
    private static final int METHOD_keyUp125 = 125;
    private static final int METHOD_dispatchEvent126 = 126;
    private static final int METHOD_mouseDrag127 = 127;
    private static final int METHOD_setLocation128 = 128;
    private static final int METHOD_createImage129 = 129;
    private static final int METHOD_show130 = 130;
    private static final int METHOD_add131 = 131;
    private static final int METHOD_show132 = 132;
    private static final int METHOD_mouseUp133 = 133;
    private static final int METHOD_lostFocus134 = 134;
    private static final int METHOD_imageUpdate135 = 135;
    private static final int METHOD_resize136 = 136;
    private static final int METHOD_mouseDown137 = 137;
    private static final int METHOD_action138 = 138;
    private static final int METHOD_createVolatileImage139 = 139;
    private static final int METHOD_nextFocus140 = 140;
    private static final int METHOD_getLocation141 = 141;
    private static final int METHOD_paintAll142 = 142;
    private static final int METHOD_createVolatileImage143 = 143;
    private static final int METHOD_createImage144 = 144;
    private static final int METHOD_handleEvent145 = 145;
    private static final int METHOD_repaint146 = 146;
    private static final int METHOD_repaint147 = 147;
    private static final int METHOD_mouseExit148 = 148;
    private static final int METHOD_prepareImage149 = 149;
    private static final int METHOD_prepareImage150 = 150;
    private static final int METHOD_remove151 = 151;
    private static final int METHOD_setSize152 = 152;
    private static final int METHOD_enable153 = 153;
    private static final int METHOD_transferFocus154 = 154;
    private static final int METHOD_mouseMove155 = 155;
    private static final int METHOD_getSize156 = 156;
    private static final int METHOD_repaint157 = 157;
    private static final int METHOD_enableInputMethods158 = 158;
    private static final int METHOD_bounds159 = 159;
    private static final int METHOD_setBounds160 = 160;
    private static final int METHOD_resize161 = 161;
    private static final int METHOD_list162 = 162;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[163];
    
        try {
            methods[METHOD_printLayeredPane0] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("printLayeredPane", new Class[] {}));
            methods[METHOD_printLayeredPane0].setDisplayName ( "" );
            methods[METHOD_print1] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("print", new Class[] {java.awt.Graphics.class, java.awt.print.PageFormat.class, Integer.TYPE}));
            methods[METHOD_print1].setDisplayName ( "" );
            methods[METHOD_disableDoubleBuffering2] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("disableDoubleBuffering", new Class[] {java.awt.Component.class}));
            methods[METHOD_disableDoubleBuffering2].setDisplayName ( "" );
            methods[METHOD_enableDoubleBuffering3] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("enableDoubleBuffering", new Class[] {java.awt.Component.class}));
            methods[METHOD_enableDoubleBuffering3].setDisplayName ( "" );
            methods[METHOD_getLayer4] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getLayer", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayer4].setDisplayName ( "" );
            methods[METHOD_getIndexOf5] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getIndexOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_getIndexOf5].setDisplayName ( "" );
            methods[METHOD_getPosition6] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getPosition", new Class[] {java.awt.Component.class}));
            methods[METHOD_getPosition6].setDisplayName ( "" );
            methods[METHOD_setLayer7] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setLayer7].setDisplayName ( "" );
            methods[METHOD_setLayer8] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setLayer", new Class[] {java.awt.Component.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLayer8].setDisplayName ( "" );
            methods[METHOD_highestLayer9] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("highestLayer", new Class[] {}));
            methods[METHOD_highestLayer9].setDisplayName ( "" );
            methods[METHOD_moveToBack10] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("moveToBack", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToBack10].setDisplayName ( "" );
            methods[METHOD_getLayer11] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getLayer", new Class[] {javax.swing.JComponent.class}));
            methods[METHOD_getLayer11].setDisplayName ( "" );
            methods[METHOD_setPosition12] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setPosition", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_setPosition12].setDisplayName ( "" );
            methods[METHOD_paint13] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint13].setDisplayName ( "" );
            methods[METHOD_lowestLayer14] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("lowestLayer", new Class[] {}));
            methods[METHOD_lowestLayer14].setDisplayName ( "" );
            methods[METHOD_remove15] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove15].setDisplayName ( "" );
            methods[METHOD_getLayeredPaneAbove16] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getLayeredPaneAbove", new Class[] {java.awt.Component.class}));
            methods[METHOD_getLayeredPaneAbove16].setDisplayName ( "" );
            methods[METHOD_moveToFront17] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("moveToFront", new Class[] {java.awt.Component.class}));
            methods[METHOD_moveToFront17].setDisplayName ( "" );
            methods[METHOD_putLayer18] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("putLayer", new Class[] {javax.swing.JComponent.class, Integer.TYPE}));
            methods[METHOD_putLayer18].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners19] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners19].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible20] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible20].setDisplayName ( "" );
            methods[METHOD_getClientProperty21] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty21].setDisplayName ( "" );
            methods[METHOD_print22] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print22].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction23] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction23].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener24] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener24].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke25] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke25].setDisplayName ( "" );
            methods[METHOD_updateUI26] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI26].setDisplayName ( "" );
            methods[METHOD_requestFocus27] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus27].setDisplayName ( "" );
            methods[METHOD_paintImmediately28] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately28].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus29] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus29].setDisplayName ( "" );
            methods[METHOD_getToolTipText30] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText30].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow31] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow31].setDisplayName ( "" );
            methods[METHOD_repaint32] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint32].setDisplayName ( "" );
            methods[METHOD_getInputMap33] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap33].setDisplayName ( "" );
            methods[METHOD_getSize34] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize34].setDisplayName ( "" );
            methods[METHOD_requestFocus35] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus35].setDisplayName ( "" );
            methods[METHOD_firePropertyChange36] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange36].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent37] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent37].setDisplayName ( "" );
            methods[METHOD_getInputMap38] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap38].setDisplayName ( "" );
            methods[METHOD_firePropertyChange39] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange39].setDisplayName ( "" );
            methods[METHOD_printAll40] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll40].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect41] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect41].setDisplayName ( "" );
            methods[METHOD_getLocation42] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation42].setDisplayName ( "" );
            methods[METHOD_disable43] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable43].setDisplayName ( "" );
            methods[METHOD_paintImmediately44] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately44].setDisplayName ( "" );
            methods[METHOD_firePropertyChange45] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange45].setDisplayName ( "" );
            methods[METHOD_addNotify46] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify46].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation47] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation47].setDisplayName ( "" );
            methods[METHOD_getListeners48] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners48].setDisplayName ( "" );
            methods[METHOD_firePropertyChange49] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange49].setDisplayName ( "" );
            methods[METHOD_reshape50] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape50].setDisplayName ( "" );
            methods[METHOD_grabFocus51] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus51].setDisplayName ( "" );
            methods[METHOD_getBounds52] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds52].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction53] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction53].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction54] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction54].setDisplayName ( "" );
            methods[METHOD_enable55] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable55].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener56] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener56].setDisplayName ( "" );
            methods[METHOD_update57] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update57].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke58] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke58].setDisplayName ( "" );
            methods[METHOD_removeNotify59] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify59].setDisplayName ( "" );
            methods[METHOD_firePropertyChange60] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange60].setDisplayName ( "" );
            methods[METHOD_firePropertyChange61] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange61].setDisplayName ( "" );
            methods[METHOD_getInsets62] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets62].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale63] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale63].setDisplayName ( "" );
            methods[METHOD_putClientProperty64] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty64].setDisplayName ( "" );
            methods[METHOD_createToolTip65] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip65].setDisplayName ( "" );
            methods[METHOD_contains66] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains66].setDisplayName ( "" );
            methods[METHOD_revalidate67] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate67].setDisplayName ( "" );
            methods[METHOD_repaint68] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint68].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions69] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions69].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale70] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale70].setDisplayName ( "" );
            methods[METHOD_firePropertyChange71] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange71].setDisplayName ( "" );
            methods[METHOD_setInputMap72] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap72].setDisplayName ( "" );
            methods[METHOD_firePropertyChange73] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange73].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation74] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation74].setDisplayName ( "" );
            methods[METHOD_add75] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add75].setDisplayName ( "" );
            methods[METHOD_validate76] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate76].setDisplayName ( "" );
            methods[METHOD_getComponentAt77] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt77].setDisplayName ( "" );
            methods[METHOD_removeAll78] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll78].setDisplayName ( "" );
            methods[METHOD_remove79] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove79].setDisplayName ( "" );
            methods[METHOD_preferredSize80] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize80].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet81] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet81].setDisplayName ( "" );
            methods[METHOD_list82] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list82].setDisplayName ( "" );
            methods[METHOD_printComponents83] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents83].setDisplayName ( "" );
            methods[METHOD_paintComponents84] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents84].setDisplayName ( "" );
            methods[METHOD_insets85] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets85].setDisplayName ( "" );
            methods[METHOD_findComponentAt86] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt86].setDisplayName ( "" );
            methods[METHOD_deliverEvent87] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent87].setDisplayName ( "" );
            methods[METHOD_locate88] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate88].setDisplayName ( "" );
            methods[METHOD_getComponentAt89] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt89].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward90] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward90].setDisplayName ( "" );
            methods[METHOD_minimumSize91] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize91].setDisplayName ( "" );
            methods[METHOD_add92] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add92].setDisplayName ( "" );
            methods[METHOD_add93] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add93].setDisplayName ( "" );
            methods[METHOD_add94] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add94].setDisplayName ( "" );
            methods[METHOD_layout95] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout95].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle96] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle96].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot97] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot97].setDisplayName ( "" );
            methods[METHOD_add98] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add98].setDisplayName ( "" );
            methods[METHOD_invalidate99] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate99].setDisplayName ( "" );
            methods[METHOD_list100] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list100].setDisplayName ( "" );
            methods[METHOD_countComponents101] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents101].setDisplayName ( "" );
            methods[METHOD_isAncestorOf102] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf102].setDisplayName ( "" );
            methods[METHOD_doLayout103] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout103].setDisplayName ( "" );
            methods[METHOD_findComponentAt104] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt104].setDisplayName ( "" );
            methods[METHOD_getFontMetrics105] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics105].setDisplayName ( "" );
            methods[METHOD_location106] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("location", new Class[] {}));
            methods[METHOD_location106].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle107] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle107].setDisplayName ( "" );
            methods[METHOD_postEvent108] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent108].setDisplayName ( "" );
            methods[METHOD_contains109] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains109].setDisplayName ( "" );
            methods[METHOD_gotFocus110] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus110].setDisplayName ( "" );
            methods[METHOD_setSize111] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize111].setDisplayName ( "" );
            methods[METHOD_list112] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list112].setDisplayName ( "" );
            methods[METHOD_hide113] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide113].setDisplayName ( "" );
            methods[METHOD_checkImage114] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage114].setDisplayName ( "" );
            methods[METHOD_checkImage115] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage115].setDisplayName ( "" );
            methods[METHOD_hasFocus116] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus116].setDisplayName ( "" );
            methods[METHOD_setLocation117] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation117].setDisplayName ( "" );
            methods[METHOD_mouseEnter118] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter118].setDisplayName ( "" );
            methods[METHOD_size119] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("size", new Class[] {}));
            methods[METHOD_size119].setDisplayName ( "" );
            methods[METHOD_keyDown120] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown120].setDisplayName ( "" );
            methods[METHOD_move121] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move121].setDisplayName ( "" );
            methods[METHOD_inside122] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside122].setDisplayName ( "" );
            methods[METHOD_list123] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list123].setDisplayName ( "" );
            methods[METHOD_toString124] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString124].setDisplayName ( "" );
            methods[METHOD_keyUp125] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp125].setDisplayName ( "" );
            methods[METHOD_dispatchEvent126] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent126].setDisplayName ( "" );
            methods[METHOD_mouseDrag127] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag127].setDisplayName ( "" );
            methods[METHOD_setLocation128] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation128].setDisplayName ( "" );
            methods[METHOD_createImage129] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage129].setDisplayName ( "" );
            methods[METHOD_show130] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show130].setDisplayName ( "" );
            methods[METHOD_add131] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add131].setDisplayName ( "" );
            methods[METHOD_show132] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("show", new Class[] {}));
            methods[METHOD_show132].setDisplayName ( "" );
            methods[METHOD_mouseUp133] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp133].setDisplayName ( "" );
            methods[METHOD_lostFocus134] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus134].setDisplayName ( "" );
            methods[METHOD_imageUpdate135] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate135].setDisplayName ( "" );
            methods[METHOD_resize136] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize136].setDisplayName ( "" );
            methods[METHOD_mouseDown137] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown137].setDisplayName ( "" );
            methods[METHOD_action138] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action138].setDisplayName ( "" );
            methods[METHOD_createVolatileImage139] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage139].setDisplayName ( "" );
            methods[METHOD_nextFocus140] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus140].setDisplayName ( "" );
            methods[METHOD_getLocation141] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation141].setDisplayName ( "" );
            methods[METHOD_paintAll142] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll142].setDisplayName ( "" );
            methods[METHOD_createVolatileImage143] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage143].setDisplayName ( "" );
            methods[METHOD_createImage144] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage144].setDisplayName ( "" );
            methods[METHOD_handleEvent145] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent145].setDisplayName ( "" );
            methods[METHOD_repaint146] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint146].setDisplayName ( "" );
            methods[METHOD_repaint147] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint147].setDisplayName ( "" );
            methods[METHOD_mouseExit148] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit148].setDisplayName ( "" );
            methods[METHOD_prepareImage149] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage149].setDisplayName ( "" );
            methods[METHOD_prepareImage150] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage150].setDisplayName ( "" );
            methods[METHOD_remove151] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove151].setDisplayName ( "" );
            methods[METHOD_setSize152] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize152].setDisplayName ( "" );
            methods[METHOD_enable153] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable153].setDisplayName ( "" );
            methods[METHOD_transferFocus154] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus154].setDisplayName ( "" );
            methods[METHOD_mouseMove155] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove155].setDisplayName ( "" );
            methods[METHOD_getSize156] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize156].setDisplayName ( "" );
            methods[METHOD_repaint157] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint157].setDisplayName ( "" );
            methods[METHOD_enableInputMethods158] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods158].setDisplayName ( "" );
            methods[METHOD_bounds159] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds159].setDisplayName ( "" );
            methods[METHOD_setBounds160] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds160].setDisplayName ( "" );
            methods[METHOD_resize161] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize161].setDisplayName ( "" );
            methods[METHOD_list162] = new MethodDescriptor ( bsc.beans.JBILayeredPane.class.getMethod("list", new Class[] {}));
            methods[METHOD_list162].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

