/*
 * JBIListPanelBeanInfo.java
 *
 * Created on 22 de Julho de 2004, 14:37
 */

package bsc.beans;

import java.beans.*;

/**
 * @author Siga1776
 */
public class JBIListPanelBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIListPanel.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_components = 15;
    private static final int PROPERTY_containerListeners = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_debugGraphicsOptions = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_doubleBuffered = 21;
    private static final int PROPERTY_dropTarget = 22;
    private static final int PROPERTY_enabled = 23;
    private static final int PROPERTY_focusable = 24;
    private static final int PROPERTY_focusCycleRoot = 25;
    private static final int PROPERTY_focusCycleRootAncestor = 26;
    private static final int PROPERTY_focusListeners = 27;
    private static final int PROPERTY_focusOwner = 28;
    private static final int PROPERTY_focusTraversable = 29;
    private static final int PROPERTY_focusTraversalKeys = 30;
    private static final int PROPERTY_focusTraversalKeysEnabled = 31;
    private static final int PROPERTY_focusTraversalPolicy = 32;
    private static final int PROPERTY_focusTraversalPolicySet = 33;
    private static final int PROPERTY_font = 34;
    private static final int PROPERTY_fontSet = 35;
    private static final int PROPERTY_foreground = 36;
    private static final int PROPERTY_foregroundSet = 37;
    private static final int PROPERTY_graphics = 38;
    private static final int PROPERTY_graphicsConfiguration = 39;
    private static final int PROPERTY_height = 40;
    private static final int PROPERTY_hierarchyBoundsListeners = 41;
    private static final int PROPERTY_hierarchyListeners = 42;
    private static final int PROPERTY_ignoreRepaint = 43;
    private static final int PROPERTY_inputContext = 44;
    private static final int PROPERTY_inputMethodListeners = 45;
    private static final int PROPERTY_inputMethodRequests = 46;
    private static final int PROPERTY_inputVerifier = 47;
    private static final int PROPERTY_insets = 48;
    private static final int PROPERTY_keyListeners = 49;
    private static final int PROPERTY_layout = 50;
    private static final int PROPERTY_lightweight = 51;
    private static final int PROPERTY_locale = 52;
    private static final int PROPERTY_locationOnScreen = 53;
    private static final int PROPERTY_managingFocus = 54;
    private static final int PROPERTY_maximumSize = 55;
    private static final int PROPERTY_maximumSizeSet = 56;
    private static final int PROPERTY_minimumSize = 57;
    private static final int PROPERTY_minimumSizeSet = 58;
    private static final int PROPERTY_mouseListeners = 59;
    private static final int PROPERTY_mouseMotionListeners = 60;
    private static final int PROPERTY_mouseWheelListeners = 61;
    private static final int PROPERTY_name = 62;
    private static final int PROPERTY_nextFocusableComponent = 63;
    private static final int PROPERTY_novoEnabled = 64;
    private static final int PROPERTY_opaque = 65;
    private static final int PROPERTY_optimizedDrawingEnabled = 66;
    private static final int PROPERTY_paintingTile = 67;
    private static final int PROPERTY_parent = 68;
    private static final int PROPERTY_peer = 69;
    private static final int PROPERTY_preferredSize = 70;
    private static final int PROPERTY_preferredSizeSet = 71;
    private static final int PROPERTY_propertyChangeListeners = 72;
    private static final int PROPERTY_registeredKeyStrokes = 73;
    private static final int PROPERTY_requestFocusEnabled = 74;
    private static final int PROPERTY_rootPane = 75;
    private static final int PROPERTY_showing = 76;
    private static final int PROPERTY_toolkit = 77;
    private static final int PROPERTY_toolTipText = 78;
    private static final int PROPERTY_topLevelAncestor = 79;
    private static final int PROPERTY_transferHandler = 80;
    private static final int PROPERTY_treeLock = 81;
    private static final int PROPERTY_UI = 82;
    private static final int PROPERTY_UIClassID = 83;
    private static final int PROPERTY_valid = 84;
    private static final int PROPERTY_validateRoot = 85;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 86;
    private static final int PROPERTY_vetoableChangeListeners = 87;
    private static final int PROPERTY_visible = 88;
    private static final int PROPERTY_visibleRect = 89;
    private static final int PROPERTY_width = 90;
    private static final int PROPERTY_x = 91;
    private static final int PROPERTY_y = 92;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[93];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIListPanel.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIListPanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIListPanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIListPanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIListPanel.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIListPanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIListPanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIListPanel.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIListPanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIListPanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIListPanel.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIListPanel.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIListPanel.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIListPanel.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIListPanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIListPanel.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIListPanel.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIListPanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIListPanel.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIListPanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIListPanel.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIListPanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIListPanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIListPanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIListPanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIListPanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIListPanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIListPanel.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIListPanel.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIListPanel.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIListPanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIListPanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIListPanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIListPanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIListPanel.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIListPanel.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIListPanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIListPanel.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIListPanel.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIListPanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIListPanel.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIListPanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIListPanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIListPanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIListPanel.class, "getInputContext", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIListPanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIListPanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIListPanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIListPanel.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIListPanel.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIListPanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIListPanel.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIListPanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIListPanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIListPanel.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIListPanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIListPanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIListPanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIListPanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIListPanel.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIListPanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIListPanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIListPanel.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIListPanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_novoEnabled] = new PropertyDescriptor ( "novoEnabled", JBIListPanel.class, "getNovoEnabled", "setNovoEnabled" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIListPanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIListPanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIListPanel.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIListPanel.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIListPanel.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIListPanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIListPanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIListPanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIListPanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIListPanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIListPanel.class, "getRootPane", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIListPanel.class, "isShowing", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIListPanel.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIListPanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIListPanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIListPanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIListPanel.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIListPanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIListPanel.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIListPanel.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIListPanel.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIListPanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIListPanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIListPanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIListPanel.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIListPanel.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIListPanel.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIListPanel.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIListPanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enable18 = 18;
    private static final int METHOD_enableInputMethods19 = 19;
    private static final int METHOD_findComponentAt20 = 20;
    private static final int METHOD_firePropertyChange21 = 21;
    private static final int METHOD_getActionForKeyStroke22 = 22;
    private static final int METHOD_getBounds23 = 23;
    private static final int METHOD_getClientProperty24 = 24;
    private static final int METHOD_getComponentAt25 = 25;
    private static final int METHOD_getConditionForKeyStroke26 = 26;
    private static final int METHOD_getDefaultLocale27 = 27;
    private static final int METHOD_getFontMetrics28 = 28;
    private static final int METHOD_getInputMap29 = 29;
    private static final int METHOD_getInsets30 = 30;
    private static final int METHOD_getListeners31 = 31;
    private static final int METHOD_getLocation32 = 32;
    private static final int METHOD_getPropertyChangeListeners33 = 33;
    private static final int METHOD_getSize34 = 34;
    private static final int METHOD_getToolTipLocation35 = 35;
    private static final int METHOD_getToolTipText36 = 36;
    private static final int METHOD_gotFocus37 = 37;
    private static final int METHOD_grabFocus38 = 38;
    private static final int METHOD_handleEvent39 = 39;
    private static final int METHOD_hasFocus40 = 40;
    private static final int METHOD_hide41 = 41;
    private static final int METHOD_imageUpdate42 = 42;
    private static final int METHOD_insets43 = 43;
    private static final int METHOD_inside44 = 44;
    private static final int METHOD_invalidate45 = 45;
    private static final int METHOD_isAncestorOf46 = 46;
    private static final int METHOD_isFocusCycleRoot47 = 47;
    private static final int METHOD_isLightweightComponent48 = 48;
    private static final int METHOD_keyDown49 = 49;
    private static final int METHOD_keyUp50 = 50;
    private static final int METHOD_layout51 = 51;
    private static final int METHOD_list52 = 52;
    private static final int METHOD_locate53 = 53;
    private static final int METHOD_location54 = 54;
    private static final int METHOD_lostFocus55 = 55;
    private static final int METHOD_minimumSize56 = 56;
    private static final int METHOD_mouseDown57 = 57;
    private static final int METHOD_mouseDrag58 = 58;
    private static final int METHOD_mouseEnter59 = 59;
    private static final int METHOD_mouseExit60 = 60;
    private static final int METHOD_mouseMove61 = 61;
    private static final int METHOD_mouseUp62 = 62;
    private static final int METHOD_move63 = 63;
    private static final int METHOD_nextFocus64 = 64;
    private static final int METHOD_paint65 = 65;
    private static final int METHOD_paintAll66 = 66;
    private static final int METHOD_paintComponents67 = 67;
    private static final int METHOD_paintImmediately68 = 68;
    private static final int METHOD_postEvent69 = 69;
    private static final int METHOD_preferredSize70 = 70;
    private static final int METHOD_prepareImage71 = 71;
    private static final int METHOD_print72 = 72;
    private static final int METHOD_printAll73 = 73;
    private static final int METHOD_printComponents74 = 74;
    private static final int METHOD_putClientProperty75 = 75;
    private static final int METHOD_registerKeyboardAction76 = 76;
    private static final int METHOD_remove77 = 77;
    private static final int METHOD_removeAll78 = 78;
    private static final int METHOD_removeNotify79 = 79;
    private static final int METHOD_removePropertyChangeListener80 = 80;
    private static final int METHOD_repaint81 = 81;
    private static final int METHOD_requestDefaultFocus82 = 82;
    private static final int METHOD_requestFocus83 = 83;
    private static final int METHOD_requestFocusInWindow84 = 84;
    private static final int METHOD_resetKeyboardActions85 = 85;
    private static final int METHOD_reshape86 = 86;
    private static final int METHOD_resize87 = 87;
    private static final int METHOD_revalidate88 = 88;
    private static final int METHOD_scrollRectToVisible89 = 89;
    private static final int METHOD_setBounds90 = 90;
    private static final int METHOD_setDataSource91 = 91;
    private static final int METHOD_setDefaultLocale92 = 92;
    private static final int METHOD_setInputMap93 = 93;
    private static final int METHOD_setLocation94 = 94;
    private static final int METHOD_setSize95 = 95;
    private static final int METHOD_show96 = 96;
    private static final int METHOD_size97 = 97;
    private static final int METHOD_toString98 = 98;
    private static final int METHOD_transferFocus99 = 99;
    private static final int METHOD_transferFocusBackward100 = 100;
    private static final int METHOD_transferFocusDownCycle101 = 101;
    private static final int METHOD_transferFocusUpCycle102 = 102;
    private static final int METHOD_unregisterKeyboardAction103 = 103;
    private static final int METHOD_update104 = 104;
    private static final int METHOD_updateUI105 = 105;
    private static final int METHOD_validate106 = 106;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[107];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enable18] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable18].setDisplayName ( "" );
            methods[METHOD_enableInputMethods19] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods19].setDisplayName ( "" );
            methods[METHOD_findComponentAt20] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt20].setDisplayName ( "" );
            methods[METHOD_firePropertyChange21] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange21].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke22] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke22].setDisplayName ( "" );
            methods[METHOD_getBounds23] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds23].setDisplayName ( "" );
            methods[METHOD_getClientProperty24] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty24].setDisplayName ( "" );
            methods[METHOD_getComponentAt25] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt25].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke26] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke26].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale27] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale27].setDisplayName ( "" );
            methods[METHOD_getFontMetrics28] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics28].setDisplayName ( "" );
            methods[METHOD_getInputMap29] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap29].setDisplayName ( "" );
            methods[METHOD_getInsets30] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets30].setDisplayName ( "" );
            methods[METHOD_getListeners31] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners31].setDisplayName ( "" );
            methods[METHOD_getLocation32] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation32].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners33] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners33].setDisplayName ( "" );
            methods[METHOD_getSize34] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize34].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation35] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation35].setDisplayName ( "" );
            methods[METHOD_getToolTipText36] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText36].setDisplayName ( "" );
            methods[METHOD_gotFocus37] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus37].setDisplayName ( "" );
            methods[METHOD_grabFocus38] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus38].setDisplayName ( "" );
            methods[METHOD_handleEvent39] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent39].setDisplayName ( "" );
            methods[METHOD_hasFocus40] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus40].setDisplayName ( "" );
            methods[METHOD_hide41] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide41].setDisplayName ( "" );
            methods[METHOD_imageUpdate42] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate42].setDisplayName ( "" );
            methods[METHOD_insets43] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets43].setDisplayName ( "" );
            methods[METHOD_inside44] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside44].setDisplayName ( "" );
            methods[METHOD_invalidate45] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate45].setDisplayName ( "" );
            methods[METHOD_isAncestorOf46] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf46].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot47] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot47].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent48] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent48].setDisplayName ( "" );
            methods[METHOD_keyDown49] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown49].setDisplayName ( "" );
            methods[METHOD_keyUp50] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp50].setDisplayName ( "" );
            methods[METHOD_layout51] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout51].setDisplayName ( "" );
            methods[METHOD_list52] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list52].setDisplayName ( "" );
            methods[METHOD_locate53] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate53].setDisplayName ( "" );
            methods[METHOD_location54] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location54].setDisplayName ( "" );
            methods[METHOD_lostFocus55] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus55].setDisplayName ( "" );
            methods[METHOD_minimumSize56] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize56].setDisplayName ( "" );
            methods[METHOD_mouseDown57] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown57].setDisplayName ( "" );
            methods[METHOD_mouseDrag58] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag58].setDisplayName ( "" );
            methods[METHOD_mouseEnter59] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter59].setDisplayName ( "" );
            methods[METHOD_mouseExit60] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit60].setDisplayName ( "" );
            methods[METHOD_mouseMove61] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove61].setDisplayName ( "" );
            methods[METHOD_mouseUp62] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp62].setDisplayName ( "" );
            methods[METHOD_move63] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move63].setDisplayName ( "" );
            methods[METHOD_nextFocus64] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus64].setDisplayName ( "" );
            methods[METHOD_paint65] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint65].setDisplayName ( "" );
            methods[METHOD_paintAll66] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll66].setDisplayName ( "" );
            methods[METHOD_paintComponents67] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents67].setDisplayName ( "" );
            methods[METHOD_paintImmediately68] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately68].setDisplayName ( "" );
            methods[METHOD_postEvent69] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent69].setDisplayName ( "" );
            methods[METHOD_preferredSize70] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize70].setDisplayName ( "" );
            methods[METHOD_prepareImage71] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage71].setDisplayName ( "" );
            methods[METHOD_print72] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print72].setDisplayName ( "" );
            methods[METHOD_printAll73] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll73].setDisplayName ( "" );
            methods[METHOD_printComponents74] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents74].setDisplayName ( "" );
            methods[METHOD_putClientProperty75] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty75].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction76] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction76].setDisplayName ( "" );
            methods[METHOD_remove77] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove77].setDisplayName ( "" );
            methods[METHOD_removeAll78] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll78].setDisplayName ( "" );
            methods[METHOD_removeNotify79] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify79].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener80] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener80].setDisplayName ( "" );
            methods[METHOD_repaint81] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint81].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus82] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus82].setDisplayName ( "" );
            methods[METHOD_requestFocus83] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus83].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow84] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow84].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions85] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions85].setDisplayName ( "" );
            methods[METHOD_reshape86] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape86].setDisplayName ( "" );
            methods[METHOD_resize87] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize87].setDisplayName ( "" );
            methods[METHOD_revalidate88] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate88].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible89] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible89].setDisplayName ( "" );
            methods[METHOD_setBounds90] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds90].setDisplayName ( "" );
            methods[METHOD_setDataSource91] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setDataSource", new Class[] {bsc.xml.BIXMLVector.class, java.lang.String.class, java.lang.String.class, bsc.swing.BscDefaultFrameBehavior.class}));
            methods[METHOD_setDataSource91].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale92] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale92].setDisplayName ( "" );
            methods[METHOD_setInputMap93] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap93].setDisplayName ( "" );
            methods[METHOD_setLocation94] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation94].setDisplayName ( "" );
            methods[METHOD_setSize95] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize95].setDisplayName ( "" );
            methods[METHOD_show96] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show96].setDisplayName ( "" );
            methods[METHOD_size97] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size97].setDisplayName ( "" );
            methods[METHOD_toString98] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString98].setDisplayName ( "" );
            methods[METHOD_transferFocus99] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus99].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward100] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward100].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle101] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle101].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle102] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle102].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction103] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction103].setDisplayName ( "" );
            methods[METHOD_update104] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update104].setDisplayName ( "" );
            methods[METHOD_updateUI105] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI105].setDisplayName ( "" );
            methods[METHOD_validate106] = new MethodDescriptor ( bsc.beans.JBIListPanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate106].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
 //GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
 //GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

