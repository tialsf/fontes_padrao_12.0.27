/*
 * JBIListPanelPlusBeanInfo.java
 *
 * Created on 29 de Julho de 2004, 16:01
 */

package bsc.beans;

import java.beans.*;

/**
 * @author Siga1776
 */
public class JBIListPanelPlusBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( bsc.beans.JBIListPanelPlus.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_components = 15;
    private static final int PROPERTY_containerListeners = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_debugGraphicsOptions = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_documentTarget = 21;
    private static final int PROPERTY_doubleBuffered = 22;
    private static final int PROPERTY_dropTarget = 23;
    private static final int PROPERTY_enabled = 24;
    private static final int PROPERTY_fileType = 25;
    private static final int PROPERTY_focusable = 26;
    private static final int PROPERTY_focusCycleRoot = 27;
    private static final int PROPERTY_focusCycleRootAncestor = 28;
    private static final int PROPERTY_focusListeners = 29;
    private static final int PROPERTY_focusOwner = 30;
    private static final int PROPERTY_focusTraversable = 31;
    private static final int PROPERTY_focusTraversalKeys = 32;
    private static final int PROPERTY_focusTraversalKeysEnabled = 33;
    private static final int PROPERTY_focusTraversalPolicy = 34;
    private static final int PROPERTY_focusTraversalPolicySet = 35;
    private static final int PROPERTY_font = 36;
    private static final int PROPERTY_fontSet = 37;
    private static final int PROPERTY_foreground = 38;
    private static final int PROPERTY_foregroundSet = 39;
    private static final int PROPERTY_graphics = 40;
    private static final int PROPERTY_graphicsConfiguration = 41;
    private static final int PROPERTY_height = 42;
    private static final int PROPERTY_hierarchyBoundsListeners = 43;
    private static final int PROPERTY_hierarchyListeners = 44;
    private static final int PROPERTY_ignoreRepaint = 45;
    private static final int PROPERTY_inputContext = 46;
    private static final int PROPERTY_inputMethodListeners = 47;
    private static final int PROPERTY_inputMethodRequests = 48;
    private static final int PROPERTY_inputVerifier = 49;
    private static final int PROPERTY_insets = 50;
    private static final int PROPERTY_keyListeners = 51;
    private static final int PROPERTY_layout = 52;
    private static final int PROPERTY_lightweight = 53;
    private static final int PROPERTY_loadServerFiles = 54;
    private static final int PROPERTY_locale = 55;
    private static final int PROPERTY_locationOnScreen = 56;
    private static final int PROPERTY_managingFocus = 57;
    private static final int PROPERTY_maximumSize = 58;
    private static final int PROPERTY_maximumSizeSet = 59;
    private static final int PROPERTY_minimumSize = 60;
    private static final int PROPERTY_minimumSizeSet = 61;
    private static final int PROPERTY_mouseListeners = 62;
    private static final int PROPERTY_mouseMotionListeners = 63;
    private static final int PROPERTY_mouseWheelListeners = 64;
    private static final int PROPERTY_name = 65;
    private static final int PROPERTY_nextFocusableComponent = 66;
    private static final int PROPERTY_novoEnabled = 67;
    private static final int PROPERTY_opaque = 68;
    private static final int PROPERTY_openByReport = 69;
    private static final int PROPERTY_optimizedDrawingEnabled = 70;
    private static final int PROPERTY_paintingTile = 71;
    private static final int PROPERTY_parent = 72;
    private static final int PROPERTY_peer = 73;
    private static final int PROPERTY_preferredSize = 74;
    private static final int PROPERTY_preferredSizeSet = 75;
    private static final int PROPERTY_propertyChangeListeners = 76;
    private static final int PROPERTY_registeredKeyStrokes = 77;
    private static final int PROPERTY_requestFocusEnabled = 78;
    private static final int PROPERTY_rootPane = 79;
    private static final int PROPERTY_serverFolder = 80;
    private static final int PROPERTY_showing = 81;
    private static final int PROPERTY_toolkit = 82;
    private static final int PROPERTY_toolTipText = 83;
    private static final int PROPERTY_topLevelAncestor = 84;
    private static final int PROPERTY_transferHandler = 85;
    private static final int PROPERTY_treeLock = 86;
    private static final int PROPERTY_UI = 87;
    private static final int PROPERTY_UIClassID = 88;
    private static final int PROPERTY_valid = 89;
    private static final int PROPERTY_validateRoot = 90;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 91;
    private static final int PROPERTY_vetoableChangeListeners = 92;
    private static final int PROPERTY_visible = 93;
    private static final int PROPERTY_visibleRect = 94;
    private static final int PROPERTY_width = 95;
    private static final int PROPERTY_x = 96;
    private static final int PROPERTY_y = 97;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[98];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", bsc.beans.JBIListPanelPlus.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", bsc.beans.JBIListPanelPlus.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", bsc.beans.JBIListPanelPlus.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", bsc.beans.JBIListPanelPlus.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", bsc.beans.JBIListPanelPlus.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", bsc.beans.JBIListPanelPlus.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", bsc.beans.JBIListPanelPlus.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", bsc.beans.JBIListPanelPlus.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", bsc.beans.JBIListPanelPlus.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", bsc.beans.JBIListPanelPlus.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", bsc.beans.JBIListPanelPlus.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", bsc.beans.JBIListPanelPlus.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", bsc.beans.JBIListPanelPlus.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", bsc.beans.JBIListPanelPlus.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", bsc.beans.JBIListPanelPlus.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", bsc.beans.JBIListPanelPlus.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", bsc.beans.JBIListPanelPlus.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", bsc.beans.JBIListPanelPlus.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", bsc.beans.JBIListPanelPlus.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", bsc.beans.JBIListPanelPlus.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", bsc.beans.JBIListPanelPlus.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_documentTarget] = new PropertyDescriptor ( "documentTarget", bsc.beans.JBIListPanelPlus.class, null, "setDocumentTarget" ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", bsc.beans.JBIListPanelPlus.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", bsc.beans.JBIListPanelPlus.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", bsc.beans.JBIListPanelPlus.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_fileType] = new PropertyDescriptor ( "fileType", bsc.beans.JBIListPanelPlus.class, null, "setFileType" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", bsc.beans.JBIListPanelPlus.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", bsc.beans.JBIListPanelPlus.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", bsc.beans.JBIListPanelPlus.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", bsc.beans.JBIListPanelPlus.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", bsc.beans.JBIListPanelPlus.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", bsc.beans.JBIListPanelPlus.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", bsc.beans.JBIListPanelPlus.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", bsc.beans.JBIListPanelPlus.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", bsc.beans.JBIListPanelPlus.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", bsc.beans.JBIListPanelPlus.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", bsc.beans.JBIListPanelPlus.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", bsc.beans.JBIListPanelPlus.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", bsc.beans.JBIListPanelPlus.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", bsc.beans.JBIListPanelPlus.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", bsc.beans.JBIListPanelPlus.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", bsc.beans.JBIListPanelPlus.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", bsc.beans.JBIListPanelPlus.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", bsc.beans.JBIListPanelPlus.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", bsc.beans.JBIListPanelPlus.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", bsc.beans.JBIListPanelPlus.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", bsc.beans.JBIListPanelPlus.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", bsc.beans.JBIListPanelPlus.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", bsc.beans.JBIListPanelPlus.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", bsc.beans.JBIListPanelPlus.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", bsc.beans.JBIListPanelPlus.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", bsc.beans.JBIListPanelPlus.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", bsc.beans.JBIListPanelPlus.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", bsc.beans.JBIListPanelPlus.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_loadServerFiles] = new PropertyDescriptor ( "loadServerFiles", bsc.beans.JBIListPanelPlus.class, null, "setLoadServerFiles" ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", bsc.beans.JBIListPanelPlus.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", bsc.beans.JBIListPanelPlus.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", bsc.beans.JBIListPanelPlus.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", bsc.beans.JBIListPanelPlus.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", bsc.beans.JBIListPanelPlus.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", bsc.beans.JBIListPanelPlus.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", bsc.beans.JBIListPanelPlus.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", bsc.beans.JBIListPanelPlus.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", bsc.beans.JBIListPanelPlus.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", bsc.beans.JBIListPanelPlus.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", bsc.beans.JBIListPanelPlus.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", bsc.beans.JBIListPanelPlus.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_novoEnabled] = new PropertyDescriptor ( "novoEnabled", bsc.beans.JBIListPanelPlus.class, "getNovoEnabled", "setNovoEnabled" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", bsc.beans.JBIListPanelPlus.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_openByReport] = new PropertyDescriptor ( "openByReport", bsc.beans.JBIListPanelPlus.class, null, "setOpenByReport" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", bsc.beans.JBIListPanelPlus.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", bsc.beans.JBIListPanelPlus.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", bsc.beans.JBIListPanelPlus.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", bsc.beans.JBIListPanelPlus.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", bsc.beans.JBIListPanelPlus.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", bsc.beans.JBIListPanelPlus.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", bsc.beans.JBIListPanelPlus.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", bsc.beans.JBIListPanelPlus.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", bsc.beans.JBIListPanelPlus.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", bsc.beans.JBIListPanelPlus.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_serverFolder] = new PropertyDescriptor ( "serverFolder", bsc.beans.JBIListPanelPlus.class, null, "setServerFolder" ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", bsc.beans.JBIListPanelPlus.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", bsc.beans.JBIListPanelPlus.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", bsc.beans.JBIListPanelPlus.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", bsc.beans.JBIListPanelPlus.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", bsc.beans.JBIListPanelPlus.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", bsc.beans.JBIListPanelPlus.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", bsc.beans.JBIListPanelPlus.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", bsc.beans.JBIListPanelPlus.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", bsc.beans.JBIListPanelPlus.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", bsc.beans.JBIListPanelPlus.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", bsc.beans.JBIListPanelPlus.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", bsc.beans.JBIListPanelPlus.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", bsc.beans.JBIListPanelPlus.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", bsc.beans.JBIListPanelPlus.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", bsc.beans.JBIListPanelPlus.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", bsc.beans.JBIListPanelPlus.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", bsc.beans.JBIListPanelPlus.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;     }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIListPanelPlus.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;     }//GEN-LAST:Events
    
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enable18 = 18;
    private static final int METHOD_enableInputMethods19 = 19;
    private static final int METHOD_findComponentAt20 = 20;
    private static final int METHOD_getActionForKeyStroke21 = 21;
    private static final int METHOD_getBounds22 = 22;
    private static final int METHOD_getClientProperty23 = 23;
    private static final int METHOD_getComponentAt24 = 24;
    private static final int METHOD_getConditionForKeyStroke25 = 25;
    private static final int METHOD_getDefaultLocale26 = 26;
    private static final int METHOD_getFontMetrics27 = 27;
    private static final int METHOD_getInsets28 = 28;
    private static final int METHOD_getListeners29 = 29;
    private static final int METHOD_getLocation30 = 30;
    private static final int METHOD_getPropertyChangeListeners31 = 31;
    private static final int METHOD_getSize32 = 32;
    private static final int METHOD_getToolTipLocation33 = 33;
    private static final int METHOD_getToolTipText34 = 34;
    private static final int METHOD_gotFocus35 = 35;
    private static final int METHOD_grabFocus36 = 36;
    private static final int METHOD_handleEvent37 = 37;
    private static final int METHOD_hasFocus38 = 38;
    private static final int METHOD_hide39 = 39;
    private static final int METHOD_imageUpdate40 = 40;
    private static final int METHOD_insets41 = 41;
    private static final int METHOD_inside42 = 42;
    private static final int METHOD_invalidate43 = 43;
    private static final int METHOD_isAncestorOf44 = 44;
    private static final int METHOD_isFocusCycleRoot45 = 45;
    private static final int METHOD_isLightweightComponent46 = 46;
    private static final int METHOD_keyDown47 = 47;
    private static final int METHOD_keyUp48 = 48;
    private static final int METHOD_layout49 = 49;
    private static final int METHOD_list50 = 50;
    private static final int METHOD_locate51 = 51;
    private static final int METHOD_location52 = 52;
    private static final int METHOD_lostFocus53 = 53;
    private static final int METHOD_minimumSize54 = 54;
    private static final int METHOD_mouseDown55 = 55;
    private static final int METHOD_mouseDrag56 = 56;
    private static final int METHOD_mouseEnter57 = 57;
    private static final int METHOD_mouseExit58 = 58;
    private static final int METHOD_mouseMove59 = 59;
    private static final int METHOD_mouseUp60 = 60;
    private static final int METHOD_move61 = 61;
    private static final int METHOD_nextFocus62 = 62;
    private static final int METHOD_paint63 = 63;
    private static final int METHOD_paintAll64 = 64;
    private static final int METHOD_paintComponents65 = 65;
    private static final int METHOD_paintImmediately66 = 66;
    private static final int METHOD_postEvent67 = 67;
    private static final int METHOD_preferredSize68 = 68;
    private static final int METHOD_prepareImage69 = 69;
    private static final int METHOD_print70 = 70;
    private static final int METHOD_printAll71 = 71;
    private static final int METHOD_printComponents72 = 72;
    private static final int METHOD_putClientProperty73 = 73;
    private static final int METHOD_registerKeyboardAction74 = 74;
    private static final int METHOD_remove75 = 75;
    private static final int METHOD_removeAll76 = 76;
    private static final int METHOD_removeNotify77 = 77;
    private static final int METHOD_removePropertyChangeListener78 = 78;
    private static final int METHOD_repaint79 = 79;
    private static final int METHOD_requestDefaultFocus80 = 80;
    private static final int METHOD_requestFocus81 = 81;
    private static final int METHOD_requestFocusInWindow82 = 82;
    private static final int METHOD_resetKeyboardActions83 = 83;
    private static final int METHOD_reshape84 = 84;
    private static final int METHOD_resize85 = 85;
    private static final int METHOD_revalidate86 = 86;
    private static final int METHOD_scrollRectToVisible87 = 87;
    private static final int METHOD_setBounds88 = 88;
    private static final int METHOD_setDataSource89 = 89;
    private static final int METHOD_setDefaultLocale90 = 90;
    private static final int METHOD_show91 = 91;
    private static final int METHOD_size92 = 92;
    private static final int METHOD_toString93 = 93;
    private static final int METHOD_transferFocus94 = 94;
    private static final int METHOD_transferFocusBackward95 = 95;
    private static final int METHOD_transferFocusDownCycle96 = 96;
    private static final int METHOD_transferFocusUpCycle97 = 97;
    private static final int METHOD_unregisterKeyboardAction98 = 98;
    private static final int METHOD_update99 = 99;
    private static final int METHOD_updateUI100 = 100;
    private static final int METHOD_validate101 = 101;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[102];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enable18] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable18].setDisplayName ( "" );
            methods[METHOD_enableInputMethods19] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_enableInputMethods19].setDisplayName ( "" );
            methods[METHOD_findComponentAt20] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_findComponentAt20].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke21] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke21].setDisplayName ( "" );
            methods[METHOD_getBounds22] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds22].setDisplayName ( "" );
            methods[METHOD_getClientProperty23] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty23].setDisplayName ( "" );
            methods[METHOD_getComponentAt24] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getComponentAt24].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke25] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke25].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale26] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale26].setDisplayName ( "" );
            methods[METHOD_getFontMetrics27] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics27].setDisplayName ( "" );
            methods[METHOD_getInsets28] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets28].setDisplayName ( "" );
            methods[METHOD_getListeners29] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners29].setDisplayName ( "" );
            methods[METHOD_getLocation30] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation30].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners31] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners31].setDisplayName ( "" );
            methods[METHOD_getSize32] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize32].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation33] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation33].setDisplayName ( "" );
            methods[METHOD_getToolTipText34] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText34].setDisplayName ( "" );
            methods[METHOD_gotFocus35] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus35].setDisplayName ( "" );
            methods[METHOD_grabFocus36] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus36].setDisplayName ( "" );
            methods[METHOD_handleEvent37] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent37].setDisplayName ( "" );
            methods[METHOD_hasFocus38] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus38].setDisplayName ( "" );
            methods[METHOD_hide39] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide39].setDisplayName ( "" );
            methods[METHOD_imageUpdate40] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_imageUpdate40].setDisplayName ( "" );
            methods[METHOD_insets41] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets41].setDisplayName ( "" );
            methods[METHOD_inside42] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_inside42].setDisplayName ( "" );
            methods[METHOD_invalidate43] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate43].setDisplayName ( "" );
            methods[METHOD_isAncestorOf44] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf44].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot45] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot45].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent46] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent46].setDisplayName ( "" );
            methods[METHOD_keyDown47] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyDown47].setDisplayName ( "" );
            methods[METHOD_keyUp48] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyUp48].setDisplayName ( "" );
            methods[METHOD_layout49] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout49].setDisplayName ( "" );
            methods[METHOD_list50] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE})); // NOI18N
            methods[METHOD_list50].setDisplayName ( "" );
            methods[METHOD_locate51] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_locate51].setDisplayName ( "" );
            methods[METHOD_location52] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location52].setDisplayName ( "" );
            methods[METHOD_lostFocus53] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus53].setDisplayName ( "" );
            methods[METHOD_minimumSize54] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize54].setDisplayName ( "" );
            methods[METHOD_mouseDown55] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDown55].setDisplayName ( "" );
            methods[METHOD_mouseDrag56] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDrag56].setDisplayName ( "" );
            methods[METHOD_mouseEnter57] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseEnter57].setDisplayName ( "" );
            methods[METHOD_mouseExit58] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseExit58].setDisplayName ( "" );
            methods[METHOD_mouseMove59] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseMove59].setDisplayName ( "" );
            methods[METHOD_mouseUp60] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseUp60].setDisplayName ( "" );
            methods[METHOD_move61] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_move61].setDisplayName ( "" );
            methods[METHOD_nextFocus62] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus62].setDisplayName ( "" );
            methods[METHOD_paint63] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint63].setDisplayName ( "" );
            methods[METHOD_paintAll64] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll64].setDisplayName ( "" );
            methods[METHOD_paintComponents65] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents65].setDisplayName ( "" );
            methods[METHOD_paintImmediately66] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_paintImmediately66].setDisplayName ( "" );
            methods[METHOD_postEvent67] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent67].setDisplayName ( "" );
            methods[METHOD_preferredSize68] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize68].setDisplayName ( "" );
            methods[METHOD_prepareImage69] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage69].setDisplayName ( "" );
            methods[METHOD_print70] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print70].setDisplayName ( "" );
            methods[METHOD_printAll71] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll71].setDisplayName ( "" );
            methods[METHOD_printComponents72] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents72].setDisplayName ( "" );
            methods[METHOD_putClientProperty73] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty73].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction74] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE})); // NOI18N
            methods[METHOD_registerKeyboardAction74].setDisplayName ( "" );
            methods[METHOD_remove75] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("remove", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_remove75].setDisplayName ( "" );
            methods[METHOD_removeAll76] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll76].setDisplayName ( "" );
            methods[METHOD_removeNotify77] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify77].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener78] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener78].setDisplayName ( "" );
            methods[METHOD_repaint79] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_repaint79].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus80] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus80].setDisplayName ( "" );
            methods[METHOD_requestFocus81] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus81].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow82] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow82].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions83] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions83].setDisplayName ( "" );
            methods[METHOD_reshape84] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_reshape84].setDisplayName ( "" );
            methods[METHOD_resize85] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_resize85].setDisplayName ( "" );
            methods[METHOD_revalidate86] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate86].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible87] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible87].setDisplayName ( "" );
            methods[METHOD_setBounds88] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_setBounds88].setDisplayName ( "" );
            methods[METHOD_setDataSource89] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("setDataSource", new Class[] {bsc.xml.BIXMLVector.class, java.lang.String.class, java.lang.String.class, bsc.swing.BscDefaultFrameBehavior.class})); // NOI18N
            methods[METHOD_setDataSource89].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale90] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale90].setDisplayName ( "" );
            methods[METHOD_show91] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show91].setDisplayName ( "" );
            methods[METHOD_size92] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size92].setDisplayName ( "" );
            methods[METHOD_toString93] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString93].setDisplayName ( "" );
            methods[METHOD_transferFocus94] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus94].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward95] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward95].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle96] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle96].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle97] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle97].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction98] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction98].setDisplayName ( "" );
            methods[METHOD_update99] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update99].setDisplayName ( "" );
            methods[METHOD_updateUI100] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI100].setDisplayName ( "" );
            methods[METHOD_validate101] = new MethodDescriptor ( bsc.beans.JBIListPanelPlus.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate101].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;     }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

