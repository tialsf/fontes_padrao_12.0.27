/*
 * JBIListPanel.java
 *
 * Created on July 4, 2003, 4:02 PM
 */

package bsc.beans;

import javax.swing.JInternalFrame;


/**
 *
 * @author  siga1996
 */
public class JBIMessagePanel extends javax.swing.JPanel {
	
	String parentId = null;
        String contextId = null;
	
	bsc.swing.BscDefaultFrameBehavior frame = null;
	java.util.Locale defaultLocale;
				 
	private java.util.Vector vctJBIListPanelListener;
	/** Creates new form JBIListPanel */
	public JBIMessagePanel() {
		defaultLocale = bsc.core.BscStaticReferences.getBscDefaultLocale();
		if(defaultLocale==null) {
                    defaultLocale = new java.util.Locale("pt","BR");
                }

		initComponents();
		vctJBIListPanelListener = new java.util.Vector();
	}

	public void setDataSource ( bsc.xml.BIXMLVector dataSource, String parent, String context,
				bsc.swing.BscDefaultFrameBehavior frameAux ) {
		parentId = String.valueOf(parent);
		contextId = String.valueOf(context);
		xmlTable.setDataSource(dataSource);
		frame = frameAux;
	}
	
	//Configura bot�o New
	public void setNewVisible( boolean newVisible ){
		btnNew.setVisible(newVisible);
	}
	
	public void setNewEnabled( boolean newEnabled ) {
		btnNew.setEnabled( newEnabled );
	}
	
	public boolean getNewEnabled( ) {
		return btnNew.isEnabled();
	}
	
	//Configura bot�o Reply
	public void setReplyVisible( boolean replyVisible ){
		btnReply.setVisible(replyVisible);
	}
	
	public void setReplyEnabled( boolean replyEnabled ) {
		btnReply.setEnabled( replyEnabled );
	}
	
	public boolean getReplyEnabled( ) {
		return btnReply.isEnabled();
	}
	
	//Configura bot�o Reply All
	public void setReplyAllVisible( boolean replyAllVisible ){
		btnReplyAll.setVisible(replyAllVisible);
	}
	
	public void setReplyAllEnabled( boolean replyAllEnabled ) {
		btnReplyAll.setEnabled( replyAllEnabled );
	}
	
	public boolean getReplyAllEnabled( ) {
		return btnReplyAll.isEnabled();
	}
	
	//Configura bot�o Forward
	public void setForwardVisible( boolean forwardVisible ){
		btnForward.setVisible(forwardVisible);
	}
	
	public void setForwardEnabled( boolean forwardEnabled ) {
		btnForward.setEnabled( forwardEnabled );
	}
	
	public boolean getForwardEnabled( ) {
		return btnForward.isEnabled();
	}
	
	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnNew = new javax.swing.JButton();
        btnReply = new javax.swing.JButton();
        btnReplyAll = new javax.swing.JButton();
        btnForward = new javax.swing.JButton();
        btnView = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        pnlList = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        scpList = new javax.swing.JScrollPane();
        xmlTable = new bsc.beans.JBIXMLTable();

        setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(580, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(580, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(580, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(240, 32));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_novo.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnNew.setText(bundle.getString("JBIMessagePanel_00001")); // NOI18N
        btnNew.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnNew.setMaximumSize(new java.awt.Dimension(120, 20));
        btnNew.setMinimumSize(new java.awt.Dimension(120, 20));
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNew);

        btnReply.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReply.setText(bundle.getString("JBIMessagePanel_00002")); // NOI18N
        btnReply.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReply.setMaximumSize(new java.awt.Dimension(120, 20));
        btnReply.setMinimumSize(new java.awt.Dimension(120, 20));
        tbInsertion.add(btnReply);

        btnReplyAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReplyAll.setText(bundle.getString("JBIMessagePanel_00003")); // NOI18N
        btnReplyAll.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReplyAll.setMaximumSize(new java.awt.Dimension(120, 20));
        btnReplyAll.setMinimumSize(new java.awt.Dimension(120, 20));
        tbInsertion.add(btnReplyAll);

        btnForward.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnForward.setText(bundle.getString("JBIMessagePanel_00004")); // NOI18N
        btnForward.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnForward.setMaximumSize(new java.awt.Dimension(120, 20));
        btnForward.setMinimumSize(new java.awt.Dimension(120, 20));
        tbInsertion.add(btnForward);

        btnView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnView.setText(bundle.getString("JBIMessagePanel_00005")); // NOI18N
        btnView.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnView.setMaximumSize(new java.awt.Dimension(120, 20));
        btnView.setMinimumSize(new java.awt.Dimension(120, 20));
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        tbInsertion.add(btnView);

        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("JBIMessagePanel_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(120, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(120, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlList.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMaximumSize(new java.awt.Dimension(10, 10));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlList.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlList.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        jPanel1.setLayout(new java.awt.BorderLayout());

        scpList.setBorder(null);
        scpList.setAutoscrolls(true);
        scpList.setEnabled(false);
        scpList.setPreferredSize(new java.awt.Dimension(240, 450));

        xmlTable.setPreferredScrollableViewportSize(new java.awt.Dimension(240, 450));
        xmlTable.setPreferredSize(null);
        xmlTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                xmlTableKeyReleased(evt);
            }
        });
        xmlTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                xmlTableMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                xmlTableMousePressed(evt);
            }
        });
        scpList.setViewportView(xmlTable);

        jPanel1.add(scpList, java.awt.BorderLayout.CENTER);

        pnlList.add(jPanel1, java.awt.BorderLayout.CENTER);

        add(pnlList, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void xmlTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_xmlTableKeyReleased
        for(int j=0; j<vctJBIListPanelListener.size(); j++) {
            ((bsc.beans.JBIListPanelListener) vctJBIListPanelListener.get(j)).keyPressed(new java.awt.event.ComponentEvent(this, evt.getID() ));
        }
    }//GEN-LAST:event_xmlTableKeyReleased

	private void xmlTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xmlTableMousePressed
	    for(int j=0; j<vctJBIListPanelListener.size(); j++) {
                ((bsc.beans.JBIListPanelListener) vctJBIListPanelListener.get(j)).mousePressed(new java.awt.event.ComponentEvent(this, evt.getID() ));
            }
	}//GEN-LAST:event_xmlTableMousePressed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		frame.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
		int i = xmlTable.getSelectedRow();
		if ( xmlTable.getSelectedRow() != -1 ) {
			BITableModel tableModel = (BITableModel) xmlTable.getModel();
			JInternalFrame frame1 =
						bsc.core.BscStaticReferences.getBscFormController().getForm(
								tableModel.getRecordType( xmlTable.getSelectedRow() ),
								tableModel.getRecordID( xmlTable.getSelectedRow() ),
								tableModel.getName( xmlTable.getSelectedRow() ) ).asJInternalFrame();			
			frame1.putClientProperty("FORM_PARENT",this);			
		}
	}//GEN-LAST:event_btnViewActionPerformed

	private void xmlTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xmlTableMouseClicked
		if (evt.getClickCount() == 2) {
			BITableModel tableModel = (BITableModel) xmlTable.getModel();
			JInternalFrame frame1 =
						bsc.core.BscStaticReferences.getBscFormController().getForm(
								tableModel.getRecordType( xmlTable.getSelectedRow() ),
								tableModel.getRecordID( xmlTable.getSelectedRow() ),
								tableModel.getName( xmlTable.getSelectedRow() ) ).asJInternalFrame();
			frame1.putClientProperty("FORM_PARENT",this);						
		}
	}//GEN-LAST:event_xmlTableMouseClicked

	private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
            if ( ( xmlTable.getType() != null ) && ( !"".equals( xmlTable.getType() ) ) ) {
                bsc.core.BscStaticReferences.getBscFormController().newDetailForm( xmlTable.getType(),parentId, contextId );
            }
	}//GEN-LAST:event_btnNewActionPerformed
	
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnForward;
    public javax.swing.JButton btnNew;
    public javax.swing.JButton btnReload;
    public javax.swing.JButton btnReply;
    public javax.swing.JButton btnReplyAll;
    public javax.swing.JButton btnView;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel pnlBottomRecord;
    public javax.swing.JPanel pnlLeftRecord;
    public javax.swing.JPanel pnlList;
    public javax.swing.JPanel pnlOperation;
    public javax.swing.JPanel pnlRightRecord;
    public javax.swing.JPanel pnlTools;
    public javax.swing.JPanel pnlTopRecord;
    public javax.swing.JScrollPane scpList;
    public javax.swing.JToolBar tbInsertion;
    public bsc.beans.JBIXMLTable xmlTable;
    // End of variables declaration//GEN-END:variables

   
    public void addJBIListPanelListener(bsc.beans.JBIListPanelListener listener){
	vctJBIListPanelListener.add(listener);	
    }

    public void removeJBIListPanelListener(bsc.beans.JBIListPanelListener listener){
	vctJBIListPanelListener.remove(listener);	
    }
 
	public bsc.swing.BscDefaultFrameBehavior getFrame() {
		return frame;
	}

}