/*
 * JBIMessagePanelBeanInfo.java
 *
 * Created on 13 de Dezembro de 2004, 17:18
 */

package bsc.beans;

import java.beans.*;

/**
 * @author leandroms
 */
public class JBIMessagePanelBeanInfo extends SimpleBeanInfo {
	
    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIMessagePanel.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_components = 15;
    private static final int PROPERTY_containerListeners = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_debugGraphicsOptions = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_doubleBuffered = 21;
    private static final int PROPERTY_dropTarget = 22;
    private static final int PROPERTY_enabled = 23;
    private static final int PROPERTY_focusable = 24;
    private static final int PROPERTY_focusCycleRoot = 25;
    private static final int PROPERTY_focusCycleRootAncestor = 26;
    private static final int PROPERTY_focusListeners = 27;
    private static final int PROPERTY_focusOwner = 28;
    private static final int PROPERTY_focusTraversable = 29;
    private static final int PROPERTY_focusTraversalKeys = 30;
    private static final int PROPERTY_focusTraversalKeysEnabled = 31;
    private static final int PROPERTY_focusTraversalPolicy = 32;
    private static final int PROPERTY_focusTraversalPolicySet = 33;
    private static final int PROPERTY_font = 34;
    private static final int PROPERTY_fontSet = 35;
    private static final int PROPERTY_foreground = 36;
    private static final int PROPERTY_foregroundSet = 37;
    private static final int PROPERTY_forwardEnabled = 38;
    private static final int PROPERTY_forwardVisible = 39;
    private static final int PROPERTY_graphics = 40;
    private static final int PROPERTY_graphicsConfiguration = 41;
    private static final int PROPERTY_height = 42;
    private static final int PROPERTY_hierarchyBoundsListeners = 43;
    private static final int PROPERTY_hierarchyListeners = 44;
    private static final int PROPERTY_ignoreRepaint = 45;
    private static final int PROPERTY_inputContext = 46;
    private static final int PROPERTY_inputMethodListeners = 47;
    private static final int PROPERTY_inputMethodRequests = 48;
    private static final int PROPERTY_inputVerifier = 49;
    private static final int PROPERTY_insets = 50;
    private static final int PROPERTY_keyListeners = 51;
    private static final int PROPERTY_layout = 52;
    private static final int PROPERTY_lightweight = 53;
    private static final int PROPERTY_locale = 54;
    private static final int PROPERTY_locationOnScreen = 55;
    private static final int PROPERTY_managingFocus = 56;
    private static final int PROPERTY_maximumSize = 57;
    private static final int PROPERTY_maximumSizeSet = 58;
    private static final int PROPERTY_minimumSize = 59;
    private static final int PROPERTY_minimumSizeSet = 60;
    private static final int PROPERTY_mouseListeners = 61;
    private static final int PROPERTY_mouseMotionListeners = 62;
    private static final int PROPERTY_mouseWheelListeners = 63;
    private static final int PROPERTY_name = 64;
    private static final int PROPERTY_newEnabled = 65;
    private static final int PROPERTY_newVisible = 66;
    private static final int PROPERTY_nextFocusableComponent = 67;
    private static final int PROPERTY_opaque = 68;
    private static final int PROPERTY_optimizedDrawingEnabled = 69;
    private static final int PROPERTY_paintingTile = 70;
    private static final int PROPERTY_parent = 71;
    private static final int PROPERTY_peer = 72;
    private static final int PROPERTY_preferredSize = 73;
    private static final int PROPERTY_preferredSizeSet = 74;
    private static final int PROPERTY_propertyChangeListeners = 75;
    private static final int PROPERTY_registeredKeyStrokes = 76;
    private static final int PROPERTY_replyAllEnabled = 77;
    private static final int PROPERTY_replyAllVisible = 78;
    private static final int PROPERTY_replyEnabled = 79;
    private static final int PROPERTY_replyVisible = 80;
    private static final int PROPERTY_requestFocusEnabled = 81;
    private static final int PROPERTY_rootPane = 82;
    private static final int PROPERTY_showing = 83;
    private static final int PROPERTY_toolkit = 84;
    private static final int PROPERTY_toolTipText = 85;
    private static final int PROPERTY_topLevelAncestor = 86;
    private static final int PROPERTY_transferHandler = 87;
    private static final int PROPERTY_treeLock = 88;
    private static final int PROPERTY_UI = 89;
    private static final int PROPERTY_UIClassID = 90;
    private static final int PROPERTY_valid = 91;
    private static final int PROPERTY_validateRoot = 92;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 93;
    private static final int PROPERTY_vetoableChangeListeners = 94;
    private static final int PROPERTY_visible = 95;
    private static final int PROPERTY_visibleRect = 96;
    private static final int PROPERTY_width = 97;
    private static final int PROPERTY_x = 98;
    private static final int PROPERTY_y = 99;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[100];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIMessagePanel.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIMessagePanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIMessagePanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIMessagePanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIMessagePanel.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIMessagePanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIMessagePanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIMessagePanel.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIMessagePanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIMessagePanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIMessagePanel.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIMessagePanel.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIMessagePanel.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIMessagePanel.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIMessagePanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIMessagePanel.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIMessagePanel.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIMessagePanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIMessagePanel.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIMessagePanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIMessagePanel.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIMessagePanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIMessagePanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIMessagePanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIMessagePanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIMessagePanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIMessagePanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIMessagePanel.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIMessagePanel.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIMessagePanel.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIMessagePanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIMessagePanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIMessagePanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIMessagePanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIMessagePanel.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIMessagePanel.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIMessagePanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIMessagePanel.class, "isForegroundSet", null );
            properties[PROPERTY_forwardEnabled] = new PropertyDescriptor ( "forwardEnabled", JBIMessagePanel.class, "getForwardEnabled", "setForwardEnabled" );
            properties[PROPERTY_forwardVisible] = new PropertyDescriptor ( "forwardVisible", JBIMessagePanel.class, null, "setForwardVisible" );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIMessagePanel.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIMessagePanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIMessagePanel.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIMessagePanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIMessagePanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIMessagePanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIMessagePanel.class, "getInputContext", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIMessagePanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIMessagePanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIMessagePanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIMessagePanel.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIMessagePanel.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIMessagePanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIMessagePanel.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIMessagePanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIMessagePanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIMessagePanel.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIMessagePanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIMessagePanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIMessagePanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIMessagePanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIMessagePanel.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIMessagePanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIMessagePanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIMessagePanel.class, "getName", "setName" );
            properties[PROPERTY_newEnabled] = new PropertyDescriptor ( "newEnabled", JBIMessagePanel.class, "getNewEnabled", "setNewEnabled" );
            properties[PROPERTY_newVisible] = new PropertyDescriptor ( "newVisible", JBIMessagePanel.class, null, "setNewVisible" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIMessagePanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIMessagePanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIMessagePanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIMessagePanel.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIMessagePanel.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIMessagePanel.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIMessagePanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIMessagePanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIMessagePanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIMessagePanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_replyAllEnabled] = new PropertyDescriptor ( "replyAllEnabled", JBIMessagePanel.class, "getReplyAllEnabled", "setReplyAllEnabled" );
            properties[PROPERTY_replyAllVisible] = new PropertyDescriptor ( "replyAllVisible", JBIMessagePanel.class, null, "setReplyAllVisible" );
            properties[PROPERTY_replyEnabled] = new PropertyDescriptor ( "replyEnabled", JBIMessagePanel.class, "getReplyEnabled", "setReplyEnabled" );
            properties[PROPERTY_replyVisible] = new PropertyDescriptor ( "replyVisible", JBIMessagePanel.class, null, "setReplyVisible" );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIMessagePanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIMessagePanel.class, "getRootPane", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIMessagePanel.class, "isShowing", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIMessagePanel.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIMessagePanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIMessagePanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIMessagePanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIMessagePanel.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIMessagePanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIMessagePanel.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIMessagePanel.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIMessagePanel.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIMessagePanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIMessagePanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIMessagePanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIMessagePanel.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIMessagePanel.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIMessagePanel.class, "getX", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIMessagePanel.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIMessagePanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addJBIListPanelListener2 = 2;
    private static final int METHOD_addNotify3 = 3;
    private static final int METHOD_addPropertyChangeListener4 = 4;
    private static final int METHOD_applyComponentOrientation5 = 5;
    private static final int METHOD_areFocusTraversalKeysSet6 = 6;
    private static final int METHOD_bounds7 = 7;
    private static final int METHOD_checkImage8 = 8;
    private static final int METHOD_computeVisibleRect9 = 9;
    private static final int METHOD_contains10 = 10;
    private static final int METHOD_countComponents11 = 11;
    private static final int METHOD_createImage12 = 12;
    private static final int METHOD_createToolTip13 = 13;
    private static final int METHOD_createVolatileImage14 = 14;
    private static final int METHOD_deliverEvent15 = 15;
    private static final int METHOD_disable16 = 16;
    private static final int METHOD_dispatchEvent17 = 17;
    private static final int METHOD_doLayout18 = 18;
    private static final int METHOD_enable19 = 19;
    private static final int METHOD_enableInputMethods20 = 20;
    private static final int METHOD_findComponentAt21 = 21;
    private static final int METHOD_firePropertyChange22 = 22;
    private static final int METHOD_getActionForKeyStroke23 = 23;
    private static final int METHOD_getBounds24 = 24;
    private static final int METHOD_getClientProperty25 = 25;
    private static final int METHOD_getComponentAt26 = 26;
    private static final int METHOD_getConditionForKeyStroke27 = 27;
    private static final int METHOD_getDefaultLocale28 = 28;
    private static final int METHOD_getFontMetrics29 = 29;
    private static final int METHOD_getInputMap30 = 30;
    private static final int METHOD_getInsets31 = 31;
    private static final int METHOD_getListeners32 = 32;
    private static final int METHOD_getLocation33 = 33;
    private static final int METHOD_getPropertyChangeListeners34 = 34;
    private static final int METHOD_getSize35 = 35;
    private static final int METHOD_getToolTipLocation36 = 36;
    private static final int METHOD_getToolTipText37 = 37;
    private static final int METHOD_gotFocus38 = 38;
    private static final int METHOD_grabFocus39 = 39;
    private static final int METHOD_handleEvent40 = 40;
    private static final int METHOD_hasFocus41 = 41;
    private static final int METHOD_hide42 = 42;
    private static final int METHOD_imageUpdate43 = 43;
    private static final int METHOD_insets44 = 44;
    private static final int METHOD_inside45 = 45;
    private static final int METHOD_invalidate46 = 46;
    private static final int METHOD_isAncestorOf47 = 47;
    private static final int METHOD_isFocusCycleRoot48 = 48;
    private static final int METHOD_isLightweightComponent49 = 49;
    private static final int METHOD_keyDown50 = 50;
    private static final int METHOD_keyUp51 = 51;
    private static final int METHOD_layout52 = 52;
    private static final int METHOD_list53 = 53;
    private static final int METHOD_locate54 = 54;
    private static final int METHOD_location55 = 55;
    private static final int METHOD_lostFocus56 = 56;
    private static final int METHOD_minimumSize57 = 57;
    private static final int METHOD_mouseDown58 = 58;
    private static final int METHOD_mouseDrag59 = 59;
    private static final int METHOD_mouseEnter60 = 60;
    private static final int METHOD_mouseExit61 = 61;
    private static final int METHOD_mouseMove62 = 62;
    private static final int METHOD_mouseUp63 = 63;
    private static final int METHOD_move64 = 64;
    private static final int METHOD_nextFocus65 = 65;
    private static final int METHOD_paint66 = 66;
    private static final int METHOD_paintAll67 = 67;
    private static final int METHOD_paintComponents68 = 68;
    private static final int METHOD_paintImmediately69 = 69;
    private static final int METHOD_postEvent70 = 70;
    private static final int METHOD_preferredSize71 = 71;
    private static final int METHOD_prepareImage72 = 72;
    private static final int METHOD_print73 = 73;
    private static final int METHOD_printAll74 = 74;
    private static final int METHOD_printComponents75 = 75;
    private static final int METHOD_putClientProperty76 = 76;
    private static final int METHOD_registerKeyboardAction77 = 77;
    private static final int METHOD_remove78 = 78;
    private static final int METHOD_removeAll79 = 79;
    private static final int METHOD_removeJBIListPanelListener80 = 80;
    private static final int METHOD_removeNotify81 = 81;
    private static final int METHOD_removePropertyChangeListener82 = 82;
    private static final int METHOD_repaint83 = 83;
    private static final int METHOD_requestDefaultFocus84 = 84;
    private static final int METHOD_requestFocus85 = 85;
    private static final int METHOD_requestFocusInWindow86 = 86;
    private static final int METHOD_resetKeyboardActions87 = 87;
    private static final int METHOD_reshape88 = 88;
    private static final int METHOD_resize89 = 89;
    private static final int METHOD_revalidate90 = 90;
    private static final int METHOD_scrollRectToVisible91 = 91;
    private static final int METHOD_setBounds92 = 92;
    private static final int METHOD_setDataSource93 = 93;
    private static final int METHOD_setDefaultLocale94 = 94;
    private static final int METHOD_setInputMap95 = 95;
    private static final int METHOD_setLocation96 = 96;
    private static final int METHOD_setSize97 = 97;
    private static final int METHOD_show98 = 98;
    private static final int METHOD_size99 = 99;
    private static final int METHOD_toString100 = 100;
    private static final int METHOD_transferFocus101 = 101;
    private static final int METHOD_transferFocusBackward102 = 102;
    private static final int METHOD_transferFocusDownCycle103 = 103;
    private static final int METHOD_transferFocusUpCycle104 = 104;
    private static final int METHOD_unregisterKeyboardAction105 = 105;
    private static final int METHOD_update106 = 106;
    private static final int METHOD_updateUI107 = 107;
    private static final int METHOD_validate108 = 108;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[109];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addJBIListPanelListener2] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("addJBIListPanelListener", new Class[] {bsc.beans.JBIListPanelListener.class}));
            methods[METHOD_addJBIListPanelListener2].setDisplayName ( "" );
            methods[METHOD_addNotify3] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify3].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener4] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener4].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation5] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation5].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet6] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet6].setDisplayName ( "" );
            methods[METHOD_bounds7] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds7].setDisplayName ( "" );
            methods[METHOD_checkImage8] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage8].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect9] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect9].setDisplayName ( "" );
            methods[METHOD_contains10] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains10].setDisplayName ( "" );
            methods[METHOD_countComponents11] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents11].setDisplayName ( "" );
            methods[METHOD_createImage12] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage12].setDisplayName ( "" );
            methods[METHOD_createToolTip13] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip13].setDisplayName ( "" );
            methods[METHOD_createVolatileImage14] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage14].setDisplayName ( "" );
            methods[METHOD_deliverEvent15] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent15].setDisplayName ( "" );
            methods[METHOD_disable16] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable16].setDisplayName ( "" );
            methods[METHOD_dispatchEvent17] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent17].setDisplayName ( "" );
            methods[METHOD_doLayout18] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout18].setDisplayName ( "" );
            methods[METHOD_enable19] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable19].setDisplayName ( "" );
            methods[METHOD_enableInputMethods20] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods20].setDisplayName ( "" );
            methods[METHOD_findComponentAt21] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt21].setDisplayName ( "" );
            methods[METHOD_firePropertyChange22] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange22].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke23] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke23].setDisplayName ( "" );
            methods[METHOD_getBounds24] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds24].setDisplayName ( "" );
            methods[METHOD_getClientProperty25] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty25].setDisplayName ( "" );
            methods[METHOD_getComponentAt26] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt26].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke27] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke27].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale28] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale28].setDisplayName ( "" );
            methods[METHOD_getFontMetrics29] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics29].setDisplayName ( "" );
            methods[METHOD_getInputMap30] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap30].setDisplayName ( "" );
            methods[METHOD_getInsets31] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets31].setDisplayName ( "" );
            methods[METHOD_getListeners32] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners32].setDisplayName ( "" );
            methods[METHOD_getLocation33] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation33].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners34] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners34].setDisplayName ( "" );
            methods[METHOD_getSize35] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize35].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation36] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation36].setDisplayName ( "" );
            methods[METHOD_getToolTipText37] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText37].setDisplayName ( "" );
            methods[METHOD_gotFocus38] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus38].setDisplayName ( "" );
            methods[METHOD_grabFocus39] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus39].setDisplayName ( "" );
            methods[METHOD_handleEvent40] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent40].setDisplayName ( "" );
            methods[METHOD_hasFocus41] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus41].setDisplayName ( "" );
            methods[METHOD_hide42] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide42].setDisplayName ( "" );
            methods[METHOD_imageUpdate43] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate43].setDisplayName ( "" );
            methods[METHOD_insets44] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets44].setDisplayName ( "" );
            methods[METHOD_inside45] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside45].setDisplayName ( "" );
            methods[METHOD_invalidate46] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate46].setDisplayName ( "" );
            methods[METHOD_isAncestorOf47] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf47].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot48] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot48].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent49] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent49].setDisplayName ( "" );
            methods[METHOD_keyDown50] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown50].setDisplayName ( "" );
            methods[METHOD_keyUp51] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp51].setDisplayName ( "" );
            methods[METHOD_layout52] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout52].setDisplayName ( "" );
            methods[METHOD_list53] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list53].setDisplayName ( "" );
            methods[METHOD_locate54] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate54].setDisplayName ( "" );
            methods[METHOD_location55] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location55].setDisplayName ( "" );
            methods[METHOD_lostFocus56] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus56].setDisplayName ( "" );
            methods[METHOD_minimumSize57] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize57].setDisplayName ( "" );
            methods[METHOD_mouseDown58] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown58].setDisplayName ( "" );
            methods[METHOD_mouseDrag59] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag59].setDisplayName ( "" );
            methods[METHOD_mouseEnter60] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter60].setDisplayName ( "" );
            methods[METHOD_mouseExit61] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit61].setDisplayName ( "" );
            methods[METHOD_mouseMove62] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove62].setDisplayName ( "" );
            methods[METHOD_mouseUp63] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp63].setDisplayName ( "" );
            methods[METHOD_move64] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move64].setDisplayName ( "" );
            methods[METHOD_nextFocus65] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus65].setDisplayName ( "" );
            methods[METHOD_paint66] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint66].setDisplayName ( "" );
            methods[METHOD_paintAll67] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll67].setDisplayName ( "" );
            methods[METHOD_paintComponents68] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents68].setDisplayName ( "" );
            methods[METHOD_paintImmediately69] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately69].setDisplayName ( "" );
            methods[METHOD_postEvent70] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent70].setDisplayName ( "" );
            methods[METHOD_preferredSize71] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize71].setDisplayName ( "" );
            methods[METHOD_prepareImage72] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage72].setDisplayName ( "" );
            methods[METHOD_print73] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print73].setDisplayName ( "" );
            methods[METHOD_printAll74] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll74].setDisplayName ( "" );
            methods[METHOD_printComponents75] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents75].setDisplayName ( "" );
            methods[METHOD_putClientProperty76] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty76].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction77] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction77].setDisplayName ( "" );
            methods[METHOD_remove78] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove78].setDisplayName ( "" );
            methods[METHOD_removeAll79] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll79].setDisplayName ( "" );
            methods[METHOD_removeJBIListPanelListener80] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("removeJBIListPanelListener", new Class[] {bsc.beans.JBIListPanelListener.class}));
            methods[METHOD_removeJBIListPanelListener80].setDisplayName ( "" );
            methods[METHOD_removeNotify81] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify81].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener82] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener82].setDisplayName ( "" );
            methods[METHOD_repaint83] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint83].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus84] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus84].setDisplayName ( "" );
            methods[METHOD_requestFocus85] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus85].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow86] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow86].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions87] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions87].setDisplayName ( "" );
            methods[METHOD_reshape88] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape88].setDisplayName ( "" );
            methods[METHOD_resize89] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize89].setDisplayName ( "" );
            methods[METHOD_revalidate90] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate90].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible91] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible91].setDisplayName ( "" );
            methods[METHOD_setBounds92] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds92].setDisplayName ( "" );
            methods[METHOD_setDataSource93] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setDataSource", new Class[] {bsc.xml.BIXMLVector.class, java.lang.String.class, java.lang.String.class, bsc.swing.BscDefaultFrameBehavior.class}));
            methods[METHOD_setDataSource93].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale94] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale94].setDisplayName ( "" );
            methods[METHOD_setInputMap95] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap95].setDisplayName ( "" );
            methods[METHOD_setLocation96] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation96].setDisplayName ( "" );
            methods[METHOD_setSize97] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize97].setDisplayName ( "" );
            methods[METHOD_show98] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show98].setDisplayName ( "" );
            methods[METHOD_size99] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size99].setDisplayName ( "" );
            methods[METHOD_toString100] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString100].setDisplayName ( "" );
            methods[METHOD_transferFocus101] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus101].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward102] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward102].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle103] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle103].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle104] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle104].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction105] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction105].setDisplayName ( "" );
            methods[METHOD_update106] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update106].setDisplayName ( "" );
            methods[METHOD_updateUI107] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI107].setDisplayName ( "" );
            methods[METHOD_validate108] = new MethodDescriptor ( bsc.beans.JBIMessagePanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate108].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

