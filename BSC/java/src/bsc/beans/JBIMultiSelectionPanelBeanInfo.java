/*
 * JBIMultiSelectionPanelBeanInfo.java
 *
 * Created on 16 de Mar�o de 2005, 15:25
 */

package bsc.beans;

import java.beans.*;

/**
 * @author avale
 */
public class JBIMultiSelectionPanelBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIMultiSelectionPanel.class , null );//GEN-HEADEREND:BeanDescriptor
        
        // Here you can add code for customizing the BeanDescriptor.
        
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_components = 15;
    private static final int PROPERTY_containerListeners = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_debugGraphicsOptions = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_doubleBuffered = 21;
    private static final int PROPERTY_dropTarget = 22;
    private static final int PROPERTY_enabled = 23;
    private static final int PROPERTY_focusable = 24;
    private static final int PROPERTY_focusCycleRoot = 25;
    private static final int PROPERTY_focusCycleRootAncestor = 26;
    private static final int PROPERTY_focusListeners = 27;
    private static final int PROPERTY_focusOwner = 28;
    private static final int PROPERTY_focusTraversable = 29;
    private static final int PROPERTY_focusTraversalKeys = 30;
    private static final int PROPERTY_focusTraversalKeysEnabled = 31;
    private static final int PROPERTY_focusTraversalPolicy = 32;
    private static final int PROPERTY_focusTraversalPolicySet = 33;
    private static final int PROPERTY_font = 34;
    private static final int PROPERTY_fontSet = 35;
    private static final int PROPERTY_foreground = 36;
    private static final int PROPERTY_foregroundSet = 37;
    private static final int PROPERTY_graphics = 38;
    private static final int PROPERTY_graphicsConfiguration = 39;
    private static final int PROPERTY_height = 40;
    private static final int PROPERTY_hierarchyBoundsListeners = 41;
    private static final int PROPERTY_hierarchyListeners = 42;
    private static final int PROPERTY_ignoreRepaint = 43;
    private static final int PROPERTY_inputContext = 44;
    private static final int PROPERTY_inputMethodListeners = 45;
    private static final int PROPERTY_inputMethodRequests = 46;
    private static final int PROPERTY_inputVerifier = 47;
    private static final int PROPERTY_insets = 48;
    private static final int PROPERTY_keyListeners = 49;
    private static final int PROPERTY_layout = 50;
    private static final int PROPERTY_lightweight = 51;
    private static final int PROPERTY_locale = 52;
    private static final int PROPERTY_locationOnScreen = 53;
    private static final int PROPERTY_managingFocus = 54;
    private static final int PROPERTY_maximumSize = 55;
    private static final int PROPERTY_maximumSizeSet = 56;
    private static final int PROPERTY_minimumSize = 57;
    private static final int PROPERTY_minimumSizeSet = 58;
    private static final int PROPERTY_mouseListeners = 59;
    private static final int PROPERTY_mouseMotionListeners = 60;
    private static final int PROPERTY_mouseWheelListeners = 61;
    private static final int PROPERTY_name = 62;
    private static final int PROPERTY_nextFocusableComponent = 63;
    private static final int PROPERTY_opaque = 64;
    private static final int PROPERTY_optimizedDrawingEnabled = 65;
    private static final int PROPERTY_paintingTile = 66;
    private static final int PROPERTY_parent = 67;
    private static final int PROPERTY_peer = 68;
    private static final int PROPERTY_preferredSize = 69;
    private static final int PROPERTY_preferredSizeSet = 70;
    private static final int PROPERTY_propertyChangeListeners = 71;
    private static final int PROPERTY_registeredKeyStrokes = 72;
    private static final int PROPERTY_requestFocusEnabled = 73;
    private static final int PROPERTY_rootPane = 74;
    private static final int PROPERTY_rowCount = 75;
    private static final int PROPERTY_showing = 76;
    private static final int PROPERTY_toolkit = 77;
    private static final int PROPERTY_toolTipText = 78;
    private static final int PROPERTY_topLevelAncestor = 79;
    private static final int PROPERTY_transferHandler = 80;
    private static final int PROPERTY_treeLock = 81;
    private static final int PROPERTY_UI = 82;
    private static final int PROPERTY_UIClassID = 83;
    private static final int PROPERTY_valid = 84;
    private static final int PROPERTY_validateRoot = 85;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 86;
    private static final int PROPERTY_vetoableChangeListeners = 87;
    private static final int PROPERTY_visible = 88;
    private static final int PROPERTY_visibleRect = 89;
    private static final int PROPERTY_width = 90;
    private static final int PROPERTY_x = 91;
    private static final int PROPERTY_XMLData = 92;
    private static final int PROPERTY_y = 93;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[94];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIMultiSelectionPanel.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIMultiSelectionPanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIMultiSelectionPanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIMultiSelectionPanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIMultiSelectionPanel.class, "getAncestorListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIMultiSelectionPanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIMultiSelectionPanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIMultiSelectionPanel.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIMultiSelectionPanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIMultiSelectionPanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIMultiSelectionPanel.class, "getColorModel", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIMultiSelectionPanel.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIMultiSelectionPanel.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIMultiSelectionPanel.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIMultiSelectionPanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIMultiSelectionPanel.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIMultiSelectionPanel.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIMultiSelectionPanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIMultiSelectionPanel.class, "isCursorSet", null );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIMultiSelectionPanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIMultiSelectionPanel.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIMultiSelectionPanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIMultiSelectionPanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIMultiSelectionPanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIMultiSelectionPanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIMultiSelectionPanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIMultiSelectionPanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIMultiSelectionPanel.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIMultiSelectionPanel.class, "isFocusOwner", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIMultiSelectionPanel.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIMultiSelectionPanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIMultiSelectionPanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIMultiSelectionPanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIMultiSelectionPanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIMultiSelectionPanel.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIMultiSelectionPanel.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIMultiSelectionPanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIMultiSelectionPanel.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIMultiSelectionPanel.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIMultiSelectionPanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIMultiSelectionPanel.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIMultiSelectionPanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIMultiSelectionPanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIMultiSelectionPanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIMultiSelectionPanel.class, "getInputContext", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIMultiSelectionPanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIMultiSelectionPanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIMultiSelectionPanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIMultiSelectionPanel.class, "getInsets", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIMultiSelectionPanel.class, "getKeyListeners", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIMultiSelectionPanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIMultiSelectionPanel.class, "isLightweight", null );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIMultiSelectionPanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIMultiSelectionPanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIMultiSelectionPanel.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIMultiSelectionPanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIMultiSelectionPanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIMultiSelectionPanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIMultiSelectionPanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIMultiSelectionPanel.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIMultiSelectionPanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIMultiSelectionPanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIMultiSelectionPanel.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIMultiSelectionPanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIMultiSelectionPanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIMultiSelectionPanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIMultiSelectionPanel.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIMultiSelectionPanel.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIMultiSelectionPanel.class, "getPeer", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIMultiSelectionPanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIMultiSelectionPanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIMultiSelectionPanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIMultiSelectionPanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIMultiSelectionPanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIMultiSelectionPanel.class, "getRootPane", null );
            properties[PROPERTY_rowCount] = new PropertyDescriptor ( "rowCount", JBIMultiSelectionPanel.class, "getRowCount", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIMultiSelectionPanel.class, "isShowing", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIMultiSelectionPanel.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIMultiSelectionPanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIMultiSelectionPanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIMultiSelectionPanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIMultiSelectionPanel.class, "getTreeLock", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIMultiSelectionPanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIMultiSelectionPanel.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIMultiSelectionPanel.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIMultiSelectionPanel.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIMultiSelectionPanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIMultiSelectionPanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIMultiSelectionPanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIMultiSelectionPanel.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIMultiSelectionPanel.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIMultiSelectionPanel.class, "getX", null );
            properties[PROPERTY_XMLData] = new PropertyDescriptor ( "XMLData", JBIMultiSelectionPanel.class, "getXMLData", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIMultiSelectionPanel.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
        
        // Here you can add code for customizing the properties array.
        
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIMultiSelectionPanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
        
        // Here you can add code for customizing the event sets array.
        
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enable18 = 18;
    private static final int METHOD_enableInputMethods19 = 19;
    private static final int METHOD_findComponentAt20 = 20;
    private static final int METHOD_firePropertyChange21 = 21;
    private static final int METHOD_getActionForKeyStroke22 = 22;
    private static final int METHOD_getBounds23 = 23;
    private static final int METHOD_getClientProperty24 = 24;
    private static final int METHOD_getComponentAt25 = 25;
    private static final int METHOD_getConditionForKeyStroke26 = 26;
    private static final int METHOD_getDefaultLocale27 = 27;
    private static final int METHOD_getFontMetrics28 = 28;
    private static final int METHOD_getInputMap29 = 29;
    private static final int METHOD_getInsets30 = 30;
    private static final int METHOD_getListeners31 = 31;
    private static final int METHOD_getLocation32 = 32;
    private static final int METHOD_getPropertyChangeListeners33 = 33;
    private static final int METHOD_getSize34 = 34;
    private static final int METHOD_getToolTipLocation35 = 35;
    private static final int METHOD_getToolTipText36 = 36;
    private static final int METHOD_gotFocus37 = 37;
    private static final int METHOD_grabFocus38 = 38;
    private static final int METHOD_handleEvent39 = 39;
    private static final int METHOD_hasFocus40 = 40;
    private static final int METHOD_hide41 = 41;
    private static final int METHOD_imageUpdate42 = 42;
    private static final int METHOD_insets43 = 43;
    private static final int METHOD_inside44 = 44;
    private static final int METHOD_invalidate45 = 45;
    private static final int METHOD_isAncestorOf46 = 46;
    private static final int METHOD_isFocusCycleRoot47 = 47;
    private static final int METHOD_isLightweightComponent48 = 48;
    private static final int METHOD_keyDown49 = 49;
    private static final int METHOD_keyUp50 = 50;
    private static final int METHOD_layout51 = 51;
    private static final int METHOD_list52 = 52;
    private static final int METHOD_locate53 = 53;
    private static final int METHOD_location54 = 54;
    private static final int METHOD_lostFocus55 = 55;
    private static final int METHOD_minimumSize56 = 56;
    private static final int METHOD_mouseDown57 = 57;
    private static final int METHOD_mouseDrag58 = 58;
    private static final int METHOD_mouseEnter59 = 59;
    private static final int METHOD_mouseExit60 = 60;
    private static final int METHOD_mouseMove61 = 61;
    private static final int METHOD_mouseUp62 = 62;
    private static final int METHOD_move63 = 63;
    private static final int METHOD_nextFocus64 = 64;
    private static final int METHOD_paint65 = 65;
    private static final int METHOD_paintAll66 = 66;
    private static final int METHOD_paintComponents67 = 67;
    private static final int METHOD_paintImmediately68 = 68;
    private static final int METHOD_postEvent69 = 69;
    private static final int METHOD_preferredSize70 = 70;
    private static final int METHOD_prepareImage71 = 71;
    private static final int METHOD_print72 = 72;
    private static final int METHOD_printAll73 = 73;
    private static final int METHOD_printComponents74 = 74;
    private static final int METHOD_putClientProperty75 = 75;
    private static final int METHOD_registerKeyboardAction76 = 76;
    private static final int METHOD_remove77 = 77;
    private static final int METHOD_removeAll78 = 78;
    private static final int METHOD_removeNotify79 = 79;
    private static final int METHOD_removePropertyChangeListener80 = 80;
    private static final int METHOD_repaint81 = 81;
    private static final int METHOD_requestDefaultFocus82 = 82;
    private static final int METHOD_requestFocus83 = 83;
    private static final int METHOD_requestFocusInWindow84 = 84;
    private static final int METHOD_resetKeyboardActions85 = 85;
    private static final int METHOD_reshape86 = 86;
    private static final int METHOD_resize87 = 87;
    private static final int METHOD_revalidate88 = 88;
    private static final int METHOD_scrollRectToVisible89 = 89;
    private static final int METHOD_setBounds90 = 90;
    private static final int METHOD_setDataSource91 = 91;
    private static final int METHOD_setDefaultLocale92 = 92;
    private static final int METHOD_setInputMap93 = 93;
    private static final int METHOD_setLocation94 = 94;
    private static final int METHOD_setSize95 = 95;
    private static final int METHOD_show96 = 96;
    private static final int METHOD_size97 = 97;
    private static final int METHOD_toString98 = 98;
    private static final int METHOD_transferFocus99 = 99;
    private static final int METHOD_transferFocusBackward100 = 100;
    private static final int METHOD_transferFocusDownCycle101 = 101;
    private static final int METHOD_transferFocusUpCycle102 = 102;
    private static final int METHOD_unregisterKeyboardAction103 = 103;
    private static final int METHOD_update104 = 104;
    private static final int METHOD_updateUI105 = 105;
    private static final int METHOD_validate106 = 106;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[107];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enable18] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable18].setDisplayName ( "" );
            methods[METHOD_enableInputMethods19] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods19].setDisplayName ( "" );
            methods[METHOD_findComponentAt20] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt20].setDisplayName ( "" );
            methods[METHOD_firePropertyChange21] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange21].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke22] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke22].setDisplayName ( "" );
            methods[METHOD_getBounds23] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds23].setDisplayName ( "" );
            methods[METHOD_getClientProperty24] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty24].setDisplayName ( "" );
            methods[METHOD_getComponentAt25] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt25].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke26] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke26].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale27] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale27].setDisplayName ( "" );
            methods[METHOD_getFontMetrics28] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics28].setDisplayName ( "" );
            methods[METHOD_getInputMap29] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap29].setDisplayName ( "" );
            methods[METHOD_getInsets30] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets30].setDisplayName ( "" );
            methods[METHOD_getListeners31] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners31].setDisplayName ( "" );
            methods[METHOD_getLocation32] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation32].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners33] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners33].setDisplayName ( "" );
            methods[METHOD_getSize34] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize34].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation35] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation35].setDisplayName ( "" );
            methods[METHOD_getToolTipText36] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText36].setDisplayName ( "" );
            methods[METHOD_gotFocus37] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus37].setDisplayName ( "" );
            methods[METHOD_grabFocus38] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus38].setDisplayName ( "" );
            methods[METHOD_handleEvent39] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent39].setDisplayName ( "" );
            methods[METHOD_hasFocus40] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus40].setDisplayName ( "" );
            methods[METHOD_hide41] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide41].setDisplayName ( "" );
            methods[METHOD_imageUpdate42] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate42].setDisplayName ( "" );
            methods[METHOD_insets43] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets43].setDisplayName ( "" );
            methods[METHOD_inside44] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside44].setDisplayName ( "" );
            methods[METHOD_invalidate45] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate45].setDisplayName ( "" );
            methods[METHOD_isAncestorOf46] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf46].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot47] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot47].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent48] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent48].setDisplayName ( "" );
            methods[METHOD_keyDown49] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown49].setDisplayName ( "" );
            methods[METHOD_keyUp50] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp50].setDisplayName ( "" );
            methods[METHOD_layout51] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout51].setDisplayName ( "" );
            methods[METHOD_list52] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list52].setDisplayName ( "" );
            methods[METHOD_locate53] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate53].setDisplayName ( "" );
            methods[METHOD_location54] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location54].setDisplayName ( "" );
            methods[METHOD_lostFocus55] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus55].setDisplayName ( "" );
            methods[METHOD_minimumSize56] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize56].setDisplayName ( "" );
            methods[METHOD_mouseDown57] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown57].setDisplayName ( "" );
            methods[METHOD_mouseDrag58] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag58].setDisplayName ( "" );
            methods[METHOD_mouseEnter59] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter59].setDisplayName ( "" );
            methods[METHOD_mouseExit60] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit60].setDisplayName ( "" );
            methods[METHOD_mouseMove61] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove61].setDisplayName ( "" );
            methods[METHOD_mouseUp62] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp62].setDisplayName ( "" );
            methods[METHOD_move63] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move63].setDisplayName ( "" );
            methods[METHOD_nextFocus64] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus64].setDisplayName ( "" );
            methods[METHOD_paint65] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint65].setDisplayName ( "" );
            methods[METHOD_paintAll66] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll66].setDisplayName ( "" );
            methods[METHOD_paintComponents67] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents67].setDisplayName ( "" );
            methods[METHOD_paintImmediately68] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately68].setDisplayName ( "" );
            methods[METHOD_postEvent69] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent69].setDisplayName ( "" );
            methods[METHOD_preferredSize70] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize70].setDisplayName ( "" );
            methods[METHOD_prepareImage71] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage71].setDisplayName ( "" );
            methods[METHOD_print72] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print72].setDisplayName ( "" );
            methods[METHOD_printAll73] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll73].setDisplayName ( "" );
            methods[METHOD_printComponents74] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents74].setDisplayName ( "" );
            methods[METHOD_putClientProperty75] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty75].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction76] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction76].setDisplayName ( "" );
            methods[METHOD_remove77] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove77].setDisplayName ( "" );
            methods[METHOD_removeAll78] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll78].setDisplayName ( "" );
            methods[METHOD_removeNotify79] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify79].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener80] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener80].setDisplayName ( "" );
            methods[METHOD_repaint81] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint81].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus82] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus82].setDisplayName ( "" );
            methods[METHOD_requestFocus83] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus83].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow84] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow84].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions85] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions85].setDisplayName ( "" );
            methods[METHOD_reshape86] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape86].setDisplayName ( "" );
            methods[METHOD_resize87] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize87].setDisplayName ( "" );
            methods[METHOD_revalidate88] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate88].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible89] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible89].setDisplayName ( "" );
            methods[METHOD_setBounds90] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds90].setDisplayName ( "" );
            methods[METHOD_setDataSource91] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setDataSource", new Class[] {bsc.xml.BIXMLVector.class, java.lang.String.class, bsc.swing.BscDefaultFrameBehavior.class}));
            methods[METHOD_setDataSource91].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale92] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale92].setDisplayName ( "" );
            methods[METHOD_setInputMap93] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap93].setDisplayName ( "" );
            methods[METHOD_setLocation94] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation94].setDisplayName ( "" );
            methods[METHOD_setSize95] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize95].setDisplayName ( "" );
            methods[METHOD_show96] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show96].setDisplayName ( "" );
            methods[METHOD_size97] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size97].setDisplayName ( "" );
            methods[METHOD_toString98] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString98].setDisplayName ( "" );
            methods[METHOD_transferFocus99] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus99].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward100] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward100].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle101] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle101].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle102] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle102].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction103] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction103].setDisplayName ( "" );
            methods[METHOD_update104] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update104].setDisplayName ( "" );
            methods[METHOD_updateUI105] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI105].setDisplayName ( "" );
            methods[METHOD_validate106] = new MethodDescriptor ( bsc.beans.JBIMultiSelectionPanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate106].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
        
        // Here you can add code for customizing the methods array.
        
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
 //GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
 //GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }
}

