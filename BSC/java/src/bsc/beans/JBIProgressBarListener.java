/*
 * JBIProgressBarListener.java
 *
 * Created on 10 de Junho de 2009, 11:41
 */
package bsc.beans;

import java.util.EventListener;

/**
 *
 * @author  Gilmar P. Santos
 */
public interface JBIProgressBarListener extends EventListener{
    
	/**
     * Invoked when the component has beeen started.
     */
    public void onStartBar(java.awt.event.ComponentEvent componentEvent);

	/**
     * Invoked when the component has beeen finished.
     */
	public void onFinishBar(java.awt.event.ComponentEvent componentEvent);

	/**
     * Invoked when ocurre an error.
     */
	public void onErrorBar(java.awt.event.ComponentEvent componentEvent);
	
	/**
     * Invoked when ocurre cancel.
     */
	public void onCancelBar(java.awt.event.ComponentEvent componentEvent);
	
}