package bsc.beans;

import java.beans.*;

public class JBISelectionPanelBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBISelectionPanel.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_focusTraversalPolicy = 6;
    private static final int PROPERTY_registeredKeyStrokes = 7;
    private static final int PROPERTY_XMLData = 8;
    private static final int PROPERTY_alignmentX = 9;
    private static final int PROPERTY_paintingTile = 10;
    private static final int PROPERTY_alignmentY = 11;
    private static final int PROPERTY_hierarchyListeners = 12;
    private static final int PROPERTY_accessibleContext = 13;
    private static final int PROPERTY_preferredSize = 14;
    private static final int PROPERTY_managingFocus = 15;
    private static final int PROPERTY_minimumSizeSet = 16;
    private static final int PROPERTY_focusTraversalPolicySet = 17;
    private static final int PROPERTY_y = 18;
    private static final int PROPERTY_x = 19;
    private static final int PROPERTY_cursorSet = 20;
    private static final int PROPERTY_inputMethodRequests = 21;
    private static final int PROPERTY_containerListeners = 22;
    private static final int PROPERTY_insets = 23;
    private static final int PROPERTY_componentCount = 24;
    private static final int PROPERTY_components = 25;
    private static final int PROPERTY_inputVerifier = 26;
    private static final int PROPERTY_hierarchyBoundsListeners = 27;
    private static final int PROPERTY_border = 28;
    private static final int PROPERTY_name = 29;
    private static final int PROPERTY_optimizedDrawingEnabled = 30;
    private static final int PROPERTY_graphics = 31;
    private static final int PROPERTY_minimumSize = 32;
    private static final int PROPERTY_toolTipText = 33;
    private static final int PROPERTY_focusTraversalKeysEnabled = 34;
    private static final int PROPERTY_foreground = 35;
    private static final int PROPERTY_ignoreRepaint = 36;
    private static final int PROPERTY_focusable = 37;
    private static final int PROPERTY_preferredSizeSet = 38;
    private static final int PROPERTY_visible = 39;
    private static final int PROPERTY_focusCycleRootAncestor = 40;
    private static final int PROPERTY_parent = 41;
    private static final int PROPERTY_rootPane = 42;
    private static final int PROPERTY_lightweight = 43;
    private static final int PROPERTY_width = 44;
    private static final int PROPERTY_keyListeners = 45;
    private static final int PROPERTY_toolkit = 46;
    private static final int PROPERTY_inputContext = 47;
    private static final int PROPERTY_layout = 48;
    private static final int PROPERTY_opaque = 49;
    private static final int PROPERTY_font = 50;
    private static final int PROPERTY_locale = 51;
    private static final int PROPERTY_cursor = 52;
    private static final int PROPERTY_inputMethodListeners = 53;
    private static final int PROPERTY_transferHandler = 54;
    private static final int PROPERTY_vetoableChangeListeners = 55;
    private static final int PROPERTY_doubleBuffered = 56;
    private static final int PROPERTY_visibleRect = 57;
    private static final int PROPERTY_maximumSizeSet = 58;
    private static final int PROPERTY_valid = 59;
    private static final int PROPERTY_focusCycleRoot = 60;
    private static final int PROPERTY_maximumSize = 61;
    private static final int PROPERTY_mouseMotionListeners = 62;
    private static final int PROPERTY_treeLock = 63;
    private static final int PROPERTY_bounds = 64;
    private static final int PROPERTY_focusTraversable = 65;
    private static final int PROPERTY_propertyChangeListeners = 66;
    private static final int PROPERTY_autoscrolls = 67;
    private static final int PROPERTY_componentListeners = 68;
    private static final int PROPERTY_showing = 69;
    private static final int PROPERTY_dropTarget = 70;
    private static final int PROPERTY_focusListeners = 71;
    private static final int PROPERTY_nextFocusableComponent = 72;
    private static final int PROPERTY_peer = 73;
    private static final int PROPERTY_height = 74;
    private static final int PROPERTY_topLevelAncestor = 75;
    private static final int PROPERTY_displayable = 76;
    private static final int PROPERTY_background = 77;
    private static final int PROPERTY_graphicsConfiguration = 78;
    private static final int PROPERTY_focusOwner = 79;
    private static final int PROPERTY_ancestorListeners = 80;
    private static final int PROPERTY_requestFocusEnabled = 81;
    private static final int PROPERTY_debugGraphicsOptions = 82;
    private static final int PROPERTY_backgroundSet = 83;
    private static final int PROPERTY_actionMap = 84;
    private static final int PROPERTY_mouseListeners = 85;
    private static final int PROPERTY_enabled = 86;
    private static final int PROPERTY_foregroundSet = 87;
    private static final int PROPERTY_validateRoot = 88;
    private static final int PROPERTY_UI = 89;
    private static final int PROPERTY_UIClassID = 90;
    private static final int PROPERTY_component = 91;
    private static final int PROPERTY_focusTraversalKeys = 92;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[93];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBISelectionPanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBISelectionPanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBISelectionPanel.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBISelectionPanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBISelectionPanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBISelectionPanel.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBISelectionPanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBISelectionPanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_XMLData] = new PropertyDescriptor ( "XMLData", JBISelectionPanel.class, "getXMLData", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBISelectionPanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBISelectionPanel.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBISelectionPanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBISelectionPanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBISelectionPanel.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBISelectionPanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBISelectionPanel.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBISelectionPanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBISelectionPanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBISelectionPanel.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBISelectionPanel.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBISelectionPanel.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBISelectionPanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBISelectionPanel.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBISelectionPanel.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBISelectionPanel.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBISelectionPanel.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBISelectionPanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBISelectionPanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBISelectionPanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBISelectionPanel.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBISelectionPanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBISelectionPanel.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBISelectionPanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBISelectionPanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBISelectionPanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBISelectionPanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBISelectionPanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBISelectionPanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBISelectionPanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBISelectionPanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBISelectionPanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBISelectionPanel.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBISelectionPanel.class, "getRootPane", null );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBISelectionPanel.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBISelectionPanel.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBISelectionPanel.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBISelectionPanel.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBISelectionPanel.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBISelectionPanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBISelectionPanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBISelectionPanel.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBISelectionPanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBISelectionPanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBISelectionPanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBISelectionPanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBISelectionPanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBISelectionPanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBISelectionPanel.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBISelectionPanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBISelectionPanel.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBISelectionPanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBISelectionPanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBISelectionPanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBISelectionPanel.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBISelectionPanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBISelectionPanel.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBISelectionPanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBISelectionPanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBISelectionPanel.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBISelectionPanel.class, "isShowing", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBISelectionPanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBISelectionPanel.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBISelectionPanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBISelectionPanel.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBISelectionPanel.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBISelectionPanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBISelectionPanel.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBISelectionPanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBISelectionPanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBISelectionPanel.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBISelectionPanel.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBISelectionPanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBISelectionPanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBISelectionPanel.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBISelectionPanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBISelectionPanel.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBISelectionPanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBISelectionPanel.class, "isForegroundSet", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBISelectionPanel.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBISelectionPanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBISelectionPanel.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBISelectionPanel.class, null, null, "getComponent", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBISelectionPanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_focusListener = 0;
    private static final int EVENT_keyListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_mouseWheelListener = 3;
    private static final int EVENT_hierarchyListener = 4;
    private static final int EVENT_ancestorListener = 5;
    private static final int EVENT_componentListener = 6;
    private static final int EVENT_propertyChangeListener = 7;
    private static final int EVENT_mouseMotionListener = 8;
    private static final int EVENT_mouseListener = 9;
    private static final int EVENT_inputMethodListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseExited", "mousePressed", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"inputMethodTextChanged", "caretPositionChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBISelectionPanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_refreshTable0 = 0;
    private static final int METHOD_setDataSource1 = 1;
    private static final int METHOD_updateUI2 = 2;
    private static final int METHOD_getPropertyChangeListeners3 = 3;
    private static final int METHOD_scrollRectToVisible4 = 4;
    private static final int METHOD_getClientProperty5 = 5;
    private static final int METHOD_print6 = 6;
    private static final int METHOD_unregisterKeyboardAction7 = 7;
    private static final int METHOD_removePropertyChangeListener8 = 8;
    private static final int METHOD_getConditionForKeyStroke9 = 9;
    private static final int METHOD_requestFocus10 = 10;
    private static final int METHOD_paintImmediately11 = 11;
    private static final int METHOD_paint12 = 12;
    private static final int METHOD_requestDefaultFocus13 = 13;
    private static final int METHOD_getToolTipText14 = 14;
    private static final int METHOD_requestFocusInWindow15 = 15;
    private static final int METHOD_repaint16 = 16;
    private static final int METHOD_getInputMap17 = 17;
    private static final int METHOD_getSize18 = 18;
    private static final int METHOD_requestFocus19 = 19;
    private static final int METHOD_firePropertyChange20 = 20;
    private static final int METHOD_isLightweightComponent21 = 21;
    private static final int METHOD_getInputMap22 = 22;
    private static final int METHOD_firePropertyChange23 = 23;
    private static final int METHOD_printAll24 = 24;
    private static final int METHOD_computeVisibleRect25 = 25;
    private static final int METHOD_getLocation26 = 26;
    private static final int METHOD_disable27 = 27;
    private static final int METHOD_paintImmediately28 = 28;
    private static final int METHOD_firePropertyChange29 = 29;
    private static final int METHOD_addNotify30 = 30;
    private static final int METHOD_getToolTipLocation31 = 31;
    private static final int METHOD_getListeners32 = 32;
    private static final int METHOD_firePropertyChange33 = 33;
    private static final int METHOD_reshape34 = 34;
    private static final int METHOD_grabFocus35 = 35;
    private static final int METHOD_getBounds36 = 36;
    private static final int METHOD_registerKeyboardAction37 = 37;
    private static final int METHOD_registerKeyboardAction38 = 38;
    private static final int METHOD_enable39 = 39;
    private static final int METHOD_addPropertyChangeListener40 = 40;
    private static final int METHOD_update41 = 41;
    private static final int METHOD_getActionForKeyStroke42 = 42;
    private static final int METHOD_removeNotify43 = 43;
    private static final int METHOD_firePropertyChange44 = 44;
    private static final int METHOD_firePropertyChange45 = 45;
    private static final int METHOD_getInsets46 = 46;
    private static final int METHOD_getDefaultLocale47 = 47;
    private static final int METHOD_putClientProperty48 = 48;
    private static final int METHOD_createToolTip49 = 49;
    private static final int METHOD_contains50 = 50;
    private static final int METHOD_revalidate51 = 51;
    private static final int METHOD_repaint52 = 52;
    private static final int METHOD_resetKeyboardActions53 = 53;
    private static final int METHOD_setDefaultLocale54 = 54;
    private static final int METHOD_firePropertyChange55 = 55;
    private static final int METHOD_firePropertyChange56 = 56;
    private static final int METHOD_setInputMap57 = 57;
    private static final int METHOD_applyComponentOrientation58 = 58;
    private static final int METHOD_add59 = 59;
    private static final int METHOD_validate60 = 60;
    private static final int METHOD_getComponentAt61 = 61;
    private static final int METHOD_removeAll62 = 62;
    private static final int METHOD_remove63 = 63;
    private static final int METHOD_preferredSize64 = 64;
    private static final int METHOD_remove65 = 65;
    private static final int METHOD_areFocusTraversalKeysSet66 = 66;
    private static final int METHOD_list67 = 67;
    private static final int METHOD_printComponents68 = 68;
    private static final int METHOD_paintComponents69 = 69;
    private static final int METHOD_insets70 = 70;
    private static final int METHOD_findComponentAt71 = 71;
    private static final int METHOD_deliverEvent72 = 72;
    private static final int METHOD_locate73 = 73;
    private static final int METHOD_getComponentAt74 = 74;
    private static final int METHOD_transferFocusBackward75 = 75;
    private static final int METHOD_minimumSize76 = 76;
    private static final int METHOD_add77 = 77;
    private static final int METHOD_add78 = 78;
    private static final int METHOD_add79 = 79;
    private static final int METHOD_layout80 = 80;
    private static final int METHOD_transferFocusDownCycle81 = 81;
    private static final int METHOD_isFocusCycleRoot82 = 82;
    private static final int METHOD_add83 = 83;
    private static final int METHOD_invalidate84 = 84;
    private static final int METHOD_list85 = 85;
    private static final int METHOD_countComponents86 = 86;
    private static final int METHOD_isAncestorOf87 = 87;
    private static final int METHOD_doLayout88 = 88;
    private static final int METHOD_findComponentAt89 = 89;
    private static final int METHOD_getFontMetrics90 = 90;
    private static final int METHOD_location91 = 91;
    private static final int METHOD_transferFocusUpCycle92 = 92;
    private static final int METHOD_postEvent93 = 93;
    private static final int METHOD_contains94 = 94;
    private static final int METHOD_gotFocus95 = 95;
    private static final int METHOD_setSize96 = 96;
    private static final int METHOD_list97 = 97;
    private static final int METHOD_hide98 = 98;
    private static final int METHOD_checkImage99 = 99;
    private static final int METHOD_checkImage100 = 100;
    private static final int METHOD_hasFocus101 = 101;
    private static final int METHOD_setLocation102 = 102;
    private static final int METHOD_mouseEnter103 = 103;
    private static final int METHOD_size104 = 104;
    private static final int METHOD_keyDown105 = 105;
    private static final int METHOD_move106 = 106;
    private static final int METHOD_inside107 = 107;
    private static final int METHOD_list108 = 108;
    private static final int METHOD_toString109 = 109;
    private static final int METHOD_keyUp110 = 110;
    private static final int METHOD_dispatchEvent111 = 111;
    private static final int METHOD_mouseDrag112 = 112;
    private static final int METHOD_setLocation113 = 113;
    private static final int METHOD_createImage114 = 114;
    private static final int METHOD_show115 = 115;
    private static final int METHOD_add116 = 116;
    private static final int METHOD_show117 = 117;
    private static final int METHOD_mouseUp118 = 118;
    private static final int METHOD_lostFocus119 = 119;
    private static final int METHOD_imageUpdate120 = 120;
    private static final int METHOD_resize121 = 121;
    private static final int METHOD_mouseDown122 = 122;
    private static final int METHOD_action123 = 123;
    private static final int METHOD_createVolatileImage124 = 124;
    private static final int METHOD_nextFocus125 = 125;
    private static final int METHOD_getLocation126 = 126;
    private static final int METHOD_paintAll127 = 127;
    private static final int METHOD_createVolatileImage128 = 128;
    private static final int METHOD_createImage129 = 129;
    private static final int METHOD_handleEvent130 = 130;
    private static final int METHOD_repaint131 = 131;
    private static final int METHOD_repaint132 = 132;
    private static final int METHOD_mouseExit133 = 133;
    private static final int METHOD_prepareImage134 = 134;
    private static final int METHOD_prepareImage135 = 135;
    private static final int METHOD_remove136 = 136;
    private static final int METHOD_setSize137 = 137;
    private static final int METHOD_transferFocus138 = 138;
    private static final int METHOD_enable139 = 139;
    private static final int METHOD_mouseMove140 = 140;
    private static final int METHOD_getSize141 = 141;
    private static final int METHOD_repaint142 = 142;
    private static final int METHOD_enableInputMethods143 = 143;
    private static final int METHOD_bounds144 = 144;
    private static final int METHOD_setBounds145 = 145;
    private static final int METHOD_resize146 = 146;
    private static final int METHOD_list147 = 147;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[148];
    
        try {
            methods[METHOD_refreshTable0] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("refreshTable", new Class[] {bsc.xml.BIXMLVector.class}));
            methods[METHOD_refreshTable0].setDisplayName ( "" );
            methods[METHOD_setDataSource1] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setDataSource", new Class[] {bsc.xml.BIXMLVector.class, bsc.xml.BIXMLVector.class, java.lang.String.class, java.lang.String.class}));
            methods[METHOD_setDataSource1].setDisplayName ( "" );
            methods[METHOD_updateUI2] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI2].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners3] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners3].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible4] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible4].setDisplayName ( "" );
            methods[METHOD_getClientProperty5] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty5].setDisplayName ( "" );
            methods[METHOD_print6] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print6].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction7] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction7].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener8] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener8].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke9] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke9].setDisplayName ( "" );
            methods[METHOD_requestFocus10] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus10].setDisplayName ( "" );
            methods[METHOD_paintImmediately11] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately11].setDisplayName ( "" );
            methods[METHOD_paint12] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint12].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus13] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus13].setDisplayName ( "" );
            methods[METHOD_getToolTipText14] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText14].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow15] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow15].setDisplayName ( "" );
            methods[METHOD_repaint16] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint16].setDisplayName ( "" );
            methods[METHOD_getInputMap17] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap17].setDisplayName ( "" );
            methods[METHOD_getSize18] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize18].setDisplayName ( "" );
            methods[METHOD_requestFocus19] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus19].setDisplayName ( "" );
            methods[METHOD_firePropertyChange20] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange20].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent21] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent21].setDisplayName ( "" );
            methods[METHOD_getInputMap22] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap22].setDisplayName ( "" );
            methods[METHOD_firePropertyChange23] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange23].setDisplayName ( "" );
            methods[METHOD_printAll24] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll24].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect25] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect25].setDisplayName ( "" );
            methods[METHOD_getLocation26] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation26].setDisplayName ( "" );
            methods[METHOD_disable27] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable27].setDisplayName ( "" );
            methods[METHOD_paintImmediately28] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately28].setDisplayName ( "" );
            methods[METHOD_firePropertyChange29] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange29].setDisplayName ( "" );
            methods[METHOD_addNotify30] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify30].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation31] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation31].setDisplayName ( "" );
            methods[METHOD_getListeners32] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners32].setDisplayName ( "" );
            methods[METHOD_firePropertyChange33] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange33].setDisplayName ( "" );
            methods[METHOD_reshape34] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape34].setDisplayName ( "" );
            methods[METHOD_grabFocus35] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus35].setDisplayName ( "" );
            methods[METHOD_getBounds36] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds36].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction37] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction37].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction38] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction38].setDisplayName ( "" );
            methods[METHOD_enable39] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable39].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener40] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener40].setDisplayName ( "" );
            methods[METHOD_update41] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update41].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke42] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke42].setDisplayName ( "" );
            methods[METHOD_removeNotify43] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify43].setDisplayName ( "" );
            methods[METHOD_firePropertyChange44] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange44].setDisplayName ( "" );
            methods[METHOD_firePropertyChange45] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange45].setDisplayName ( "" );
            methods[METHOD_getInsets46] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets46].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale47] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale47].setDisplayName ( "" );
            methods[METHOD_putClientProperty48] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty48].setDisplayName ( "" );
            methods[METHOD_createToolTip49] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip49].setDisplayName ( "" );
            methods[METHOD_contains50] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains50].setDisplayName ( "" );
            methods[METHOD_revalidate51] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate51].setDisplayName ( "" );
            methods[METHOD_repaint52] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint52].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions53] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions53].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale54] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale54].setDisplayName ( "" );
            methods[METHOD_firePropertyChange55] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange55].setDisplayName ( "" );
            methods[METHOD_firePropertyChange56] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange56].setDisplayName ( "" );
            methods[METHOD_setInputMap57] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap57].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation58] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation58].setDisplayName ( "" );
            methods[METHOD_add59] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add59].setDisplayName ( "" );
            methods[METHOD_validate60] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate60].setDisplayName ( "" );
            methods[METHOD_getComponentAt61] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt61].setDisplayName ( "" );
            methods[METHOD_removeAll62] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll62].setDisplayName ( "" );
            methods[METHOD_remove63] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove63].setDisplayName ( "" );
            methods[METHOD_preferredSize64] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize64].setDisplayName ( "" );
            methods[METHOD_remove65] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove65].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet66] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet66].setDisplayName ( "" );
            methods[METHOD_list67] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list67].setDisplayName ( "" );
            methods[METHOD_printComponents68] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents68].setDisplayName ( "" );
            methods[METHOD_paintComponents69] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents69].setDisplayName ( "" );
            methods[METHOD_insets70] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets70].setDisplayName ( "" );
            methods[METHOD_findComponentAt71] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt71].setDisplayName ( "" );
            methods[METHOD_deliverEvent72] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent72].setDisplayName ( "" );
            methods[METHOD_locate73] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate73].setDisplayName ( "" );
            methods[METHOD_getComponentAt74] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt74].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward75] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward75].setDisplayName ( "" );
            methods[METHOD_minimumSize76] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize76].setDisplayName ( "" );
            methods[METHOD_add77] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add77].setDisplayName ( "" );
            methods[METHOD_add78] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add78].setDisplayName ( "" );
            methods[METHOD_add79] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add79].setDisplayName ( "" );
            methods[METHOD_layout80] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout80].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle81] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle81].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot82] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot82].setDisplayName ( "" );
            methods[METHOD_add83] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add83].setDisplayName ( "" );
            methods[METHOD_invalidate84] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate84].setDisplayName ( "" );
            methods[METHOD_list85] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list85].setDisplayName ( "" );
            methods[METHOD_countComponents86] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents86].setDisplayName ( "" );
            methods[METHOD_isAncestorOf87] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf87].setDisplayName ( "" );
            methods[METHOD_doLayout88] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout88].setDisplayName ( "" );
            methods[METHOD_findComponentAt89] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt89].setDisplayName ( "" );
            methods[METHOD_getFontMetrics90] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics90].setDisplayName ( "" );
            methods[METHOD_location91] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location91].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle92] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle92].setDisplayName ( "" );
            methods[METHOD_postEvent93] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent93].setDisplayName ( "" );
            methods[METHOD_contains94] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains94].setDisplayName ( "" );
            methods[METHOD_gotFocus95] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus95].setDisplayName ( "" );
            methods[METHOD_setSize96] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize96].setDisplayName ( "" );
            methods[METHOD_list97] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list97].setDisplayName ( "" );
            methods[METHOD_hide98] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide98].setDisplayName ( "" );
            methods[METHOD_checkImage99] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage99].setDisplayName ( "" );
            methods[METHOD_checkImage100] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage100].setDisplayName ( "" );
            methods[METHOD_hasFocus101] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus101].setDisplayName ( "" );
            methods[METHOD_setLocation102] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation102].setDisplayName ( "" );
            methods[METHOD_mouseEnter103] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter103].setDisplayName ( "" );
            methods[METHOD_size104] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size104].setDisplayName ( "" );
            methods[METHOD_keyDown105] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown105].setDisplayName ( "" );
            methods[METHOD_move106] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move106].setDisplayName ( "" );
            methods[METHOD_inside107] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside107].setDisplayName ( "" );
            methods[METHOD_list108] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list108].setDisplayName ( "" );
            methods[METHOD_toString109] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString109].setDisplayName ( "" );
            methods[METHOD_keyUp110] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp110].setDisplayName ( "" );
            methods[METHOD_dispatchEvent111] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent111].setDisplayName ( "" );
            methods[METHOD_mouseDrag112] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag112].setDisplayName ( "" );
            methods[METHOD_setLocation113] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation113].setDisplayName ( "" );
            methods[METHOD_createImage114] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage114].setDisplayName ( "" );
            methods[METHOD_show115] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show115].setDisplayName ( "" );
            methods[METHOD_add116] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add116].setDisplayName ( "" );
            methods[METHOD_show117] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show117].setDisplayName ( "" );
            methods[METHOD_mouseUp118] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp118].setDisplayName ( "" );
            methods[METHOD_lostFocus119] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus119].setDisplayName ( "" );
            methods[METHOD_imageUpdate120] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate120].setDisplayName ( "" );
            methods[METHOD_resize121] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize121].setDisplayName ( "" );
            methods[METHOD_mouseDown122] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown122].setDisplayName ( "" );
            methods[METHOD_action123] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action123].setDisplayName ( "" );
            methods[METHOD_createVolatileImage124] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage124].setDisplayName ( "" );
            methods[METHOD_nextFocus125] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus125].setDisplayName ( "" );
            methods[METHOD_getLocation126] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation126].setDisplayName ( "" );
            methods[METHOD_paintAll127] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll127].setDisplayName ( "" );
            methods[METHOD_createVolatileImage128] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage128].setDisplayName ( "" );
            methods[METHOD_createImage129] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage129].setDisplayName ( "" );
            methods[METHOD_handleEvent130] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent130].setDisplayName ( "" );
            methods[METHOD_repaint131] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint131].setDisplayName ( "" );
            methods[METHOD_repaint132] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint132].setDisplayName ( "" );
            methods[METHOD_mouseExit133] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit133].setDisplayName ( "" );
            methods[METHOD_prepareImage134] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage134].setDisplayName ( "" );
            methods[METHOD_prepareImage135] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage135].setDisplayName ( "" );
            methods[METHOD_remove136] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove136].setDisplayName ( "" );
            methods[METHOD_setSize137] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize137].setDisplayName ( "" );
            methods[METHOD_transferFocus138] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus138].setDisplayName ( "" );
            methods[METHOD_enable139] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable139].setDisplayName ( "" );
            methods[METHOD_mouseMove140] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove140].setDisplayName ( "" );
            methods[METHOD_getSize141] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize141].setDisplayName ( "" );
            methods[METHOD_repaint142] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint142].setDisplayName ( "" );
            methods[METHOD_enableInputMethods143] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods143].setDisplayName ( "" );
            methods[METHOD_bounds144] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds144].setDisplayName ( "" );
            methods[METHOD_setBounds145] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds145].setDisplayName ( "" );
            methods[METHOD_resize146] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize146].setDisplayName ( "" );
            methods[METHOD_list147] = new MethodDescriptor ( bsc.beans.JBISelectionPanel.class.getMethod("list", new Class[] {}));
            methods[METHOD_list147].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

