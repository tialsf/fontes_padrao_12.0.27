/*
 * JBITextArea.java
 *
 * Created on October 7, 2003, 9:33 AM
 */

package bsc.beans;

/**
 *
 * @author  siga1996
 */
public class JBITextArea extends javax.swing.JTextArea {
	
	/** Creates a new instance of JBITextArea */
	public JBITextArea() {
		super();
		java.util.HashSet hash = new java.util.HashSet();
		hash.add (java.awt.AWTKeyStroke.getAWTKeyStroke (java.awt.event.KeyEvent.VK_TAB, 0,false));		
		this.setFocusTraversalKeys(java.awt.KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, hash);
		hash.clear();
		hash.add (java.awt.AWTKeyStroke.getAWTKeyStroke (java.awt.event.KeyEvent.VK_TAB,
							java.awt.event.InputEvent.SHIFT_DOWN_MASK, false));
		this.setFocusTraversalKeys(java.awt.KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, hash);

		this.setWrapStyleWord(true);
		this.setRows(3);
		this.setColumns(31);
		this.setLineWrap(true);
	}
	
}
