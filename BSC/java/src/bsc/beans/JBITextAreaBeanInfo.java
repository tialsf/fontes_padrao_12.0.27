package bsc.beans;

import java.beans.*;

public class JBITextAreaBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBITextArea.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_columns = 6;
    private static final int PROPERTY_focusTraversalPolicy = 7;
    private static final int PROPERTY_caretColor = 8;
    private static final int PROPERTY_registeredKeyStrokes = 9;
    private static final int PROPERTY_scrollableTracksViewportWidth = 10;
    private static final int PROPERTY_alignmentX = 11;
    private static final int PROPERTY_paintingTile = 12;
    private static final int PROPERTY_alignmentY = 13;
    private static final int PROPERTY_hierarchyListeners = 14;
    private static final int PROPERTY_accessibleContext = 15;
    private static final int PROPERTY_preferredSize = 16;
    private static final int PROPERTY_actions = 17;
    private static final int PROPERTY_tabSize = 18;
    private static final int PROPERTY_managingFocus = 19;
    private static final int PROPERTY_minimumSizeSet = 20;
    private static final int PROPERTY_scrollableTracksViewportHeight = 21;
    private static final int PROPERTY_focusAccelerator = 22;
    private static final int PROPERTY_focusTraversalPolicySet = 23;
    private static final int PROPERTY_y = 24;
    private static final int PROPERTY_x = 25;
    private static final int PROPERTY_cursorSet = 26;
    private static final int PROPERTY_inputMethodRequests = 27;
    private static final int PROPERTY_containerListeners = 28;
    private static final int PROPERTY_insets = 29;
    private static final int PROPERTY_componentCount = 30;
    private static final int PROPERTY_components = 31;
    private static final int PROPERTY_inputVerifier = 32;
    private static final int PROPERTY_hierarchyBoundsListeners = 33;
    private static final int PROPERTY_border = 34;
    private static final int PROPERTY_name = 35;
    private static final int PROPERTY_optimizedDrawingEnabled = 36;
    private static final int PROPERTY_graphics = 37;
    private static final int PROPERTY_caretListeners = 38;
    private static final int PROPERTY_minimumSize = 39;
    private static final int PROPERTY_toolTipText = 40;
    private static final int PROPERTY_focusTraversalKeysEnabled = 41;
    private static final int PROPERTY_disabledTextColor = 42;
    private static final int PROPERTY_lineWrap = 43;
    private static final int PROPERTY_foreground = 44;
    private static final int PROPERTY_selectionStart = 45;
    private static final int PROPERTY_navigationFilter = 46;
    private static final int PROPERTY_ignoreRepaint = 47;
    private static final int PROPERTY_focusable = 48;
    private static final int PROPERTY_preferredSizeSet = 49;
    private static final int PROPERTY_visible = 50;
    private static final int PROPERTY_focusCycleRootAncestor = 51;
    private static final int PROPERTY_margin = 52;
    private static final int PROPERTY_parent = 53;
    private static final int PROPERTY_rootPane = 54;
    private static final int PROPERTY_editable = 55;
    private static final int PROPERTY_lightweight = 56;
    private static final int PROPERTY_dragEnabled = 57;
    private static final int PROPERTY_width = 58;
    private static final int PROPERTY_keyListeners = 59;
    private static final int PROPERTY_toolkit = 60;
    private static final int PROPERTY_inputContext = 61;
    private static final int PROPERTY_layout = 62;
    private static final int PROPERTY_opaque = 63;
    private static final int PROPERTY_font = 64;
    private static final int PROPERTY_locale = 65;
    private static final int PROPERTY_cursor = 66;
    private static final int PROPERTY_keymap = 67;
    private static final int PROPERTY_selectedText = 68;
    private static final int PROPERTY_inputMethodListeners = 69;
    private static final int PROPERTY_transferHandler = 70;
    private static final int PROPERTY_vetoableChangeListeners = 71;
    private static final int PROPERTY_caret = 72;
    private static final int PROPERTY_doubleBuffered = 73;
    private static final int PROPERTY_visibleRect = 74;
    private static final int PROPERTY_maximumSizeSet = 75;
    private static final int PROPERTY_rows = 76;
    private static final int PROPERTY_valid = 77;
    private static final int PROPERTY_focusCycleRoot = 78;
    private static final int PROPERTY_maximumSize = 79;
    private static final int PROPERTY_mouseMotionListeners = 80;
    private static final int PROPERTY_treeLock = 81;
    private static final int PROPERTY_bounds = 82;
    private static final int PROPERTY_text = 83;
    private static final int PROPERTY_focusTraversable = 84;
    private static final int PROPERTY_propertyChangeListeners = 85;
    private static final int PROPERTY_autoscrolls = 86;
    private static final int PROPERTY_componentListeners = 87;
    private static final int PROPERTY_showing = 88;
    private static final int PROPERTY_dropTarget = 89;
    private static final int PROPERTY_focusListeners = 90;
    private static final int PROPERTY_nextFocusableComponent = 91;
    private static final int PROPERTY_caretPosition = 92;
    private static final int PROPERTY_peer = 93;
    private static final int PROPERTY_height = 94;
    private static final int PROPERTY_topLevelAncestor = 95;
    private static final int PROPERTY_document = 96;
    private static final int PROPERTY_displayable = 97;
    private static final int PROPERTY_background = 98;
    private static final int PROPERTY_selectedTextColor = 99;
    private static final int PROPERTY_selectionColor = 100;
    private static final int PROPERTY_graphicsConfiguration = 101;
    private static final int PROPERTY_selectionEnd = 102;
    private static final int PROPERTY_focusOwner = 103;
    private static final int PROPERTY_ancestorListeners = 104;
    private static final int PROPERTY_requestFocusEnabled = 105;
    private static final int PROPERTY_debugGraphicsOptions = 106;
    private static final int PROPERTY_wrapStyleWord = 107;
    private static final int PROPERTY_backgroundSet = 108;
    private static final int PROPERTY_actionMap = 109;
    private static final int PROPERTY_mouseListeners = 110;
    private static final int PROPERTY_enabled = 111;
    private static final int PROPERTY_foregroundSet = 112;
    private static final int PROPERTY_highlighter = 113;
    private static final int PROPERTY_lineCount = 114;
    private static final int PROPERTY_validateRoot = 115;
    private static final int PROPERTY_UI = 116;
    private static final int PROPERTY_preferredScrollableViewportSize = 117;
    private static final int PROPERTY_UIClassID = 118;
    private static final int PROPERTY_lineOfOffset = 119;
    private static final int PROPERTY_component = 120;
    private static final int PROPERTY_lineEndOffset = 121;
    private static final int PROPERTY_lineStartOffset = 122;
    private static final int PROPERTY_focusTraversalKeys = 123;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[124];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBITextArea.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBITextArea.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBITextArea.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBITextArea.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBITextArea.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBITextArea.class, "getColorModel", null );
            properties[PROPERTY_columns] = new PropertyDescriptor ( "columns", JBITextArea.class, "getColumns", "setColumns" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBITextArea.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_caretColor] = new PropertyDescriptor ( "caretColor", JBITextArea.class, "getCaretColor", "setCaretColor" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBITextArea.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", JBITextArea.class, "getScrollableTracksViewportWidth", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBITextArea.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBITextArea.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBITextArea.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBITextArea.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBITextArea.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBITextArea.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_actions] = new PropertyDescriptor ( "actions", JBITextArea.class, "getActions", null );
            properties[PROPERTY_tabSize] = new PropertyDescriptor ( "tabSize", JBITextArea.class, "getTabSize", "setTabSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBITextArea.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBITextArea.class, "isMinimumSizeSet", null );
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", JBITextArea.class, "getScrollableTracksViewportHeight", null );
            properties[PROPERTY_focusAccelerator] = new PropertyDescriptor ( "focusAccelerator", JBITextArea.class, "getFocusAccelerator", "setFocusAccelerator" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBITextArea.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBITextArea.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBITextArea.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBITextArea.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBITextArea.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBITextArea.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBITextArea.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBITextArea.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBITextArea.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBITextArea.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBITextArea.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBITextArea.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBITextArea.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBITextArea.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBITextArea.class, "getGraphics", null );
            properties[PROPERTY_caretListeners] = new PropertyDescriptor ( "caretListeners", JBITextArea.class, "getCaretListeners", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBITextArea.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBITextArea.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBITextArea.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_disabledTextColor] = new PropertyDescriptor ( "disabledTextColor", JBITextArea.class, "getDisabledTextColor", "setDisabledTextColor" );
            properties[PROPERTY_lineWrap] = new PropertyDescriptor ( "lineWrap", JBITextArea.class, "getLineWrap", "setLineWrap" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBITextArea.class, "getForeground", "setForeground" );
            properties[PROPERTY_selectionStart] = new PropertyDescriptor ( "selectionStart", JBITextArea.class, "getSelectionStart", "setSelectionStart" );
            properties[PROPERTY_navigationFilter] = new PropertyDescriptor ( "navigationFilter", JBITextArea.class, "getNavigationFilter", "setNavigationFilter" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBITextArea.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBITextArea.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBITextArea.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBITextArea.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBITextArea.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_margin] = new PropertyDescriptor ( "margin", JBITextArea.class, "getMargin", "setMargin" );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBITextArea.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBITextArea.class, "getRootPane", null );
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", JBITextArea.class, "isEditable", "setEditable" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBITextArea.class, "isLightweight", null );
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", JBITextArea.class, "getDragEnabled", "setDragEnabled" );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBITextArea.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBITextArea.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBITextArea.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBITextArea.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBITextArea.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBITextArea.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBITextArea.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBITextArea.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBITextArea.class, "getCursor", "setCursor" );
            properties[PROPERTY_keymap] = new PropertyDescriptor ( "keymap", JBITextArea.class, "getKeymap", "setKeymap" );
            properties[PROPERTY_selectedText] = new PropertyDescriptor ( "selectedText", JBITextArea.class, "getSelectedText", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBITextArea.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBITextArea.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBITextArea.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_caret] = new PropertyDescriptor ( "caret", JBITextArea.class, "getCaret", "setCaret" );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBITextArea.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBITextArea.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBITextArea.class, "isMaximumSizeSet", null );
            properties[PROPERTY_rows] = new PropertyDescriptor ( "rows", JBITextArea.class, "getRows", "setRows" );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBITextArea.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBITextArea.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBITextArea.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBITextArea.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBITextArea.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBITextArea.class, "getBounds", "setBounds" );
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", JBITextArea.class, "getText", "setText" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBITextArea.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBITextArea.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBITextArea.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBITextArea.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBITextArea.class, "isShowing", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBITextArea.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBITextArea.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBITextArea.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_caretPosition] = new PropertyDescriptor ( "caretPosition", JBITextArea.class, "getCaretPosition", "setCaretPosition" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBITextArea.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBITextArea.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBITextArea.class, "getTopLevelAncestor", null );
            properties[PROPERTY_document] = new PropertyDescriptor ( "document", JBITextArea.class, "getDocument", "setDocument" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBITextArea.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBITextArea.class, "getBackground", "setBackground" );
            properties[PROPERTY_selectedTextColor] = new PropertyDescriptor ( "selectedTextColor", JBITextArea.class, "getSelectedTextColor", "setSelectedTextColor" );
            properties[PROPERTY_selectionColor] = new PropertyDescriptor ( "selectionColor", JBITextArea.class, "getSelectionColor", "setSelectionColor" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBITextArea.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_selectionEnd] = new PropertyDescriptor ( "selectionEnd", JBITextArea.class, "getSelectionEnd", "setSelectionEnd" );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBITextArea.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBITextArea.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBITextArea.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBITextArea.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_wrapStyleWord] = new PropertyDescriptor ( "wrapStyleWord", JBITextArea.class, "getWrapStyleWord", "setWrapStyleWord" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBITextArea.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBITextArea.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBITextArea.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBITextArea.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBITextArea.class, "isForegroundSet", null );
            properties[PROPERTY_highlighter] = new PropertyDescriptor ( "highlighter", JBITextArea.class, "getHighlighter", "setHighlighter" );
            properties[PROPERTY_lineCount] = new PropertyDescriptor ( "lineCount", JBITextArea.class, "getLineCount", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBITextArea.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBITextArea.class, "getUI", "setUI" );
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", JBITextArea.class, "getPreferredScrollableViewportSize", null );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBITextArea.class, "getUIClassID", null );
            properties[PROPERTY_lineOfOffset] = new IndexedPropertyDescriptor ( "lineOfOffset", JBITextArea.class, null, null, "getLineOfOffset", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBITextArea.class, null, null, "getComponent", null );
            properties[PROPERTY_lineEndOffset] = new IndexedPropertyDescriptor ( "lineEndOffset", JBITextArea.class, null, null, "getLineEndOffset", null );
            properties[PROPERTY_lineStartOffset] = new IndexedPropertyDescriptor ( "lineStartOffset", JBITextArea.class, null, null, "getLineStartOffset", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBITextArea.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_containerListener = 0;
    private static final int EVENT_hierarchyListener = 1;
    private static final int EVENT_caretListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_mouseMotionListener = 4;
    private static final int EVENT_inputMethodListener = 5;
    private static final int EVENT_focusListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseWheelListener = 8;
    private static final int EVENT_ancestorListener = 9;
    private static final int EVENT_propertyChangeListener = 10;
    private static final int EVENT_mouseListener = 11;
    private static final int EVENT_hierarchyBoundsListener = 12;
    private static final int EVENT_vetoableChangeListener = 13;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[14];
    
            try {
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_caretListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "caretListener", javax.swing.event.CaretListener.class, new String[] {"caretUpdate"}, "addCaretListener", "removeCaretListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyTyped", "keyReleased"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseExited", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBITextArea.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_append0 = 0;
    private static final int METHOD_getScrollableUnitIncrement1 = 1;
    private static final int METHOD_insert2 = 2;
    private static final int METHOD_replaceRange3 = 3;
    private static final int METHOD_copy4 = 4;
    private static final int METHOD_loadKeymap5 = 5;
    private static final int METHOD_selectAll6 = 6;
    private static final int METHOD_getKeymap7 = 7;
    private static final int METHOD_paste8 = 8;
    private static final int METHOD_updateUI9 = 9;
    private static final int METHOD_modelToView10 = 10;
    private static final int METHOD_moveCaretPosition11 = 11;
    private static final int METHOD_removeKeymap12 = 12;
    private static final int METHOD_viewToModel13 = 13;
    private static final int METHOD_addKeymap14 = 14;
    private static final int METHOD_getToolTipText15 = 15;
    private static final int METHOD_select16 = 16;
    private static final int METHOD_getText17 = 17;
    private static final int METHOD_read18 = 18;
    private static final int METHOD_getScrollableBlockIncrement19 = 19;
    private static final int METHOD_removeNotify20 = 20;
    private static final int METHOD_cut21 = 21;
    private static final int METHOD_replaceSelection22 = 22;
    private static final int METHOD_write23 = 23;
    private static final int METHOD_getPropertyChangeListeners24 = 24;
    private static final int METHOD_scrollRectToVisible25 = 25;
    private static final int METHOD_getClientProperty26 = 26;
    private static final int METHOD_print27 = 27;
    private static final int METHOD_unregisterKeyboardAction28 = 28;
    private static final int METHOD_removePropertyChangeListener29 = 29;
    private static final int METHOD_getConditionForKeyStroke30 = 30;
    private static final int METHOD_requestFocus31 = 31;
    private static final int METHOD_paint32 = 32;
    private static final int METHOD_paintImmediately33 = 33;
    private static final int METHOD_requestDefaultFocus34 = 34;
    private static final int METHOD_requestFocusInWindow35 = 35;
    private static final int METHOD_repaint36 = 36;
    private static final int METHOD_getInputMap37 = 37;
    private static final int METHOD_getSize38 = 38;
    private static final int METHOD_requestFocus39 = 39;
    private static final int METHOD_firePropertyChange40 = 40;
    private static final int METHOD_isLightweightComponent41 = 41;
    private static final int METHOD_getInputMap42 = 42;
    private static final int METHOD_firePropertyChange43 = 43;
    private static final int METHOD_printAll44 = 44;
    private static final int METHOD_computeVisibleRect45 = 45;
    private static final int METHOD_getLocation46 = 46;
    private static final int METHOD_disable47 = 47;
    private static final int METHOD_paintImmediately48 = 48;
    private static final int METHOD_firePropertyChange49 = 49;
    private static final int METHOD_addNotify50 = 50;
    private static final int METHOD_getToolTipLocation51 = 51;
    private static final int METHOD_getListeners52 = 52;
    private static final int METHOD_firePropertyChange53 = 53;
    private static final int METHOD_reshape54 = 54;
    private static final int METHOD_grabFocus55 = 55;
    private static final int METHOD_getBounds56 = 56;
    private static final int METHOD_registerKeyboardAction57 = 57;
    private static final int METHOD_registerKeyboardAction58 = 58;
    private static final int METHOD_enable59 = 59;
    private static final int METHOD_addPropertyChangeListener60 = 60;
    private static final int METHOD_update61 = 61;
    private static final int METHOD_getActionForKeyStroke62 = 62;
    private static final int METHOD_firePropertyChange63 = 63;
    private static final int METHOD_firePropertyChange64 = 64;
    private static final int METHOD_getInsets65 = 65;
    private static final int METHOD_getDefaultLocale66 = 66;
    private static final int METHOD_putClientProperty67 = 67;
    private static final int METHOD_createToolTip68 = 68;
    private static final int METHOD_contains69 = 69;
    private static final int METHOD_revalidate70 = 70;
    private static final int METHOD_repaint71 = 71;
    private static final int METHOD_resetKeyboardActions72 = 72;
    private static final int METHOD_setDefaultLocale73 = 73;
    private static final int METHOD_firePropertyChange74 = 74;
    private static final int METHOD_setInputMap75 = 75;
    private static final int METHOD_firePropertyChange76 = 76;
    private static final int METHOD_applyComponentOrientation77 = 77;
    private static final int METHOD_add78 = 78;
    private static final int METHOD_validate79 = 79;
    private static final int METHOD_getComponentAt80 = 80;
    private static final int METHOD_removeAll81 = 81;
    private static final int METHOD_remove82 = 82;
    private static final int METHOD_preferredSize83 = 83;
    private static final int METHOD_remove84 = 84;
    private static final int METHOD_areFocusTraversalKeysSet85 = 85;
    private static final int METHOD_list86 = 86;
    private static final int METHOD_printComponents87 = 87;
    private static final int METHOD_paintComponents88 = 88;
    private static final int METHOD_insets89 = 89;
    private static final int METHOD_findComponentAt90 = 90;
    private static final int METHOD_deliverEvent91 = 91;
    private static final int METHOD_locate92 = 92;
    private static final int METHOD_getComponentAt93 = 93;
    private static final int METHOD_transferFocusBackward94 = 94;
    private static final int METHOD_minimumSize95 = 95;
    private static final int METHOD_add96 = 96;
    private static final int METHOD_add97 = 97;
    private static final int METHOD_add98 = 98;
    private static final int METHOD_layout99 = 99;
    private static final int METHOD_transferFocusDownCycle100 = 100;
    private static final int METHOD_isFocusCycleRoot101 = 101;
    private static final int METHOD_add102 = 102;
    private static final int METHOD_invalidate103 = 103;
    private static final int METHOD_list104 = 104;
    private static final int METHOD_countComponents105 = 105;
    private static final int METHOD_isAncestorOf106 = 106;
    private static final int METHOD_doLayout107 = 107;
    private static final int METHOD_findComponentAt108 = 108;
    private static final int METHOD_getFontMetrics109 = 109;
    private static final int METHOD_location110 = 110;
    private static final int METHOD_transferFocusUpCycle111 = 111;
    private static final int METHOD_postEvent112 = 112;
    private static final int METHOD_contains113 = 113;
    private static final int METHOD_gotFocus114 = 114;
    private static final int METHOD_setSize115 = 115;
    private static final int METHOD_list116 = 116;
    private static final int METHOD_hide117 = 117;
    private static final int METHOD_checkImage118 = 118;
    private static final int METHOD_checkImage119 = 119;
    private static final int METHOD_hasFocus120 = 120;
    private static final int METHOD_setLocation121 = 121;
    private static final int METHOD_mouseEnter122 = 122;
    private static final int METHOD_size123 = 123;
    private static final int METHOD_keyDown124 = 124;
    private static final int METHOD_move125 = 125;
    private static final int METHOD_inside126 = 126;
    private static final int METHOD_list127 = 127;
    private static final int METHOD_toString128 = 128;
    private static final int METHOD_keyUp129 = 129;
    private static final int METHOD_dispatchEvent130 = 130;
    private static final int METHOD_mouseDrag131 = 131;
    private static final int METHOD_setLocation132 = 132;
    private static final int METHOD_createImage133 = 133;
    private static final int METHOD_show134 = 134;
    private static final int METHOD_add135 = 135;
    private static final int METHOD_show136 = 136;
    private static final int METHOD_mouseUp137 = 137;
    private static final int METHOD_lostFocus138 = 138;
    private static final int METHOD_imageUpdate139 = 139;
    private static final int METHOD_resize140 = 140;
    private static final int METHOD_mouseDown141 = 141;
    private static final int METHOD_action142 = 142;
    private static final int METHOD_createVolatileImage143 = 143;
    private static final int METHOD_nextFocus144 = 144;
    private static final int METHOD_getLocation145 = 145;
    private static final int METHOD_paintAll146 = 146;
    private static final int METHOD_createVolatileImage147 = 147;
    private static final int METHOD_createImage148 = 148;
    private static final int METHOD_handleEvent149 = 149;
    private static final int METHOD_repaint150 = 150;
    private static final int METHOD_repaint151 = 151;
    private static final int METHOD_mouseExit152 = 152;
    private static final int METHOD_prepareImage153 = 153;
    private static final int METHOD_prepareImage154 = 154;
    private static final int METHOD_remove155 = 155;
    private static final int METHOD_setSize156 = 156;
    private static final int METHOD_enable157 = 157;
    private static final int METHOD_transferFocus158 = 158;
    private static final int METHOD_mouseMove159 = 159;
    private static final int METHOD_getSize160 = 160;
    private static final int METHOD_repaint161 = 161;
    private static final int METHOD_enableInputMethods162 = 162;
    private static final int METHOD_bounds163 = 163;
    private static final int METHOD_setBounds164 = 164;
    private static final int METHOD_resize165 = 165;
    private static final int METHOD_list166 = 166;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[167];
    
        try {
            methods[METHOD_append0] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("append", new Class[] {java.lang.String.class}));
            methods[METHOD_append0].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement1] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableUnitIncrement1].setDisplayName ( "" );
            methods[METHOD_insert2] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("insert", new Class[] {java.lang.String.class, Integer.TYPE}));
            methods[METHOD_insert2].setDisplayName ( "" );
            methods[METHOD_replaceRange3] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("replaceRange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_replaceRange3].setDisplayName ( "" );
            methods[METHOD_copy4] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("copy", new Class[] {}));
            methods[METHOD_copy4].setDisplayName ( "" );
            methods[METHOD_loadKeymap5] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("loadKeymap", new Class[] {javax.swing.text.Keymap.class, Class.forName("[Ljavax.swing.text.JTextComponent$KeyBinding;"), Class.forName("[Ljavax.swing.Action;")}));
            methods[METHOD_loadKeymap5].setDisplayName ( "" );
            methods[METHOD_selectAll6] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("selectAll", new Class[] {}));
            methods[METHOD_selectAll6].setDisplayName ( "" );
            methods[METHOD_getKeymap7] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_getKeymap7].setDisplayName ( "" );
            methods[METHOD_paste8] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paste", new Class[] {}));
            methods[METHOD_paste8].setDisplayName ( "" );
            methods[METHOD_updateUI9] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI9].setDisplayName ( "" );
            methods[METHOD_modelToView10] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("modelToView", new Class[] {Integer.TYPE}));
            methods[METHOD_modelToView10].setDisplayName ( "" );
            methods[METHOD_moveCaretPosition11] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("moveCaretPosition", new Class[] {Integer.TYPE}));
            methods[METHOD_moveCaretPosition11].setDisplayName ( "" );
            methods[METHOD_removeKeymap12] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("removeKeymap", new Class[] {java.lang.String.class}));
            methods[METHOD_removeKeymap12].setDisplayName ( "" );
            methods[METHOD_viewToModel13] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("viewToModel", new Class[] {java.awt.Point.class}));
            methods[METHOD_viewToModel13].setDisplayName ( "" );
            methods[METHOD_addKeymap14] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("addKeymap", new Class[] {java.lang.String.class, javax.swing.text.Keymap.class}));
            methods[METHOD_addKeymap14].setDisplayName ( "" );
            methods[METHOD_getToolTipText15] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText15].setDisplayName ( "" );
            methods[METHOD_select16] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("select", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_select16].setDisplayName ( "" );
            methods[METHOD_getText17] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getText", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getText17].setDisplayName ( "" );
            methods[METHOD_read18] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("read", new Class[] {java.io.Reader.class, java.lang.Object.class}));
            methods[METHOD_read18].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement19] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableBlockIncrement19].setDisplayName ( "" );
            methods[METHOD_removeNotify20] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify20].setDisplayName ( "" );
            methods[METHOD_cut21] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("cut", new Class[] {}));
            methods[METHOD_cut21].setDisplayName ( "" );
            methods[METHOD_replaceSelection22] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("replaceSelection", new Class[] {java.lang.String.class}));
            methods[METHOD_replaceSelection22].setDisplayName ( "" );
            methods[METHOD_write23] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("write", new Class[] {java.io.Writer.class}));
            methods[METHOD_write23].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners24] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners24].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible25] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible25].setDisplayName ( "" );
            methods[METHOD_getClientProperty26] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty26].setDisplayName ( "" );
            methods[METHOD_print27] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print27].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction28] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction28].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener29] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener29].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke30] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke30].setDisplayName ( "" );
            methods[METHOD_requestFocus31] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus31].setDisplayName ( "" );
            methods[METHOD_paint32] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint32].setDisplayName ( "" );
            methods[METHOD_paintImmediately33] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately33].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus34] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus34].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow35] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow35].setDisplayName ( "" );
            methods[METHOD_repaint36] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint36].setDisplayName ( "" );
            methods[METHOD_getInputMap37] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap37].setDisplayName ( "" );
            methods[METHOD_getSize38] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize38].setDisplayName ( "" );
            methods[METHOD_requestFocus39] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus39].setDisplayName ( "" );
            methods[METHOD_firePropertyChange40] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange40].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent41] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent41].setDisplayName ( "" );
            methods[METHOD_getInputMap42] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap42].setDisplayName ( "" );
            methods[METHOD_firePropertyChange43] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange43].setDisplayName ( "" );
            methods[METHOD_printAll44] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll44].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect45] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect45].setDisplayName ( "" );
            methods[METHOD_getLocation46] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation46].setDisplayName ( "" );
            methods[METHOD_disable47] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable47].setDisplayName ( "" );
            methods[METHOD_paintImmediately48] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately48].setDisplayName ( "" );
            methods[METHOD_firePropertyChange49] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange49].setDisplayName ( "" );
            methods[METHOD_addNotify50] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify50].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation51] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation51].setDisplayName ( "" );
            methods[METHOD_getListeners52] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners52].setDisplayName ( "" );
            methods[METHOD_firePropertyChange53] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange53].setDisplayName ( "" );
            methods[METHOD_reshape54] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape54].setDisplayName ( "" );
            methods[METHOD_grabFocus55] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus55].setDisplayName ( "" );
            methods[METHOD_getBounds56] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds56].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction57] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction57].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction58] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction58].setDisplayName ( "" );
            methods[METHOD_enable59] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable59].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener60] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener60].setDisplayName ( "" );
            methods[METHOD_update61] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update61].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke62] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke62].setDisplayName ( "" );
            methods[METHOD_firePropertyChange63] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange63].setDisplayName ( "" );
            methods[METHOD_firePropertyChange64] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange64].setDisplayName ( "" );
            methods[METHOD_getInsets65] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets65].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale66] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale66].setDisplayName ( "" );
            methods[METHOD_putClientProperty67] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty67].setDisplayName ( "" );
            methods[METHOD_createToolTip68] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip68].setDisplayName ( "" );
            methods[METHOD_contains69] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains69].setDisplayName ( "" );
            methods[METHOD_revalidate70] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate70].setDisplayName ( "" );
            methods[METHOD_repaint71] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint71].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions72] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions72].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale73] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale73].setDisplayName ( "" );
            methods[METHOD_firePropertyChange74] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange74].setDisplayName ( "" );
            methods[METHOD_setInputMap75] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap75].setDisplayName ( "" );
            methods[METHOD_firePropertyChange76] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange76].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation77] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation77].setDisplayName ( "" );
            methods[METHOD_add78] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add78].setDisplayName ( "" );
            methods[METHOD_validate79] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate79].setDisplayName ( "" );
            methods[METHOD_getComponentAt80] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt80].setDisplayName ( "" );
            methods[METHOD_removeAll81] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll81].setDisplayName ( "" );
            methods[METHOD_remove82] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove82].setDisplayName ( "" );
            methods[METHOD_preferredSize83] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize83].setDisplayName ( "" );
            methods[METHOD_remove84] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove84].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet85] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet85].setDisplayName ( "" );
            methods[METHOD_list86] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list86].setDisplayName ( "" );
            methods[METHOD_printComponents87] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents87].setDisplayName ( "" );
            methods[METHOD_paintComponents88] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents88].setDisplayName ( "" );
            methods[METHOD_insets89] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets89].setDisplayName ( "" );
            methods[METHOD_findComponentAt90] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt90].setDisplayName ( "" );
            methods[METHOD_deliverEvent91] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent91].setDisplayName ( "" );
            methods[METHOD_locate92] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate92].setDisplayName ( "" );
            methods[METHOD_getComponentAt93] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt93].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward94] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward94].setDisplayName ( "" );
            methods[METHOD_minimumSize95] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize95].setDisplayName ( "" );
            methods[METHOD_add96] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add96].setDisplayName ( "" );
            methods[METHOD_add97] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add97].setDisplayName ( "" );
            methods[METHOD_add98] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add98].setDisplayName ( "" );
            methods[METHOD_layout99] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout99].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle100] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle100].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot101] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot101].setDisplayName ( "" );
            methods[METHOD_add102] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add102].setDisplayName ( "" );
            methods[METHOD_invalidate103] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate103].setDisplayName ( "" );
            methods[METHOD_list104] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list104].setDisplayName ( "" );
            methods[METHOD_countComponents105] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents105].setDisplayName ( "" );
            methods[METHOD_isAncestorOf106] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf106].setDisplayName ( "" );
            methods[METHOD_doLayout107] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout107].setDisplayName ( "" );
            methods[METHOD_findComponentAt108] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt108].setDisplayName ( "" );
            methods[METHOD_getFontMetrics109] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics109].setDisplayName ( "" );
            methods[METHOD_location110] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("location", new Class[] {}));
            methods[METHOD_location110].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle111] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle111].setDisplayName ( "" );
            methods[METHOD_postEvent112] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent112].setDisplayName ( "" );
            methods[METHOD_contains113] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains113].setDisplayName ( "" );
            methods[METHOD_gotFocus114] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus114].setDisplayName ( "" );
            methods[METHOD_setSize115] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize115].setDisplayName ( "" );
            methods[METHOD_list116] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list116].setDisplayName ( "" );
            methods[METHOD_hide117] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide117].setDisplayName ( "" );
            methods[METHOD_checkImage118] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage118].setDisplayName ( "" );
            methods[METHOD_checkImage119] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage119].setDisplayName ( "" );
            methods[METHOD_hasFocus120] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus120].setDisplayName ( "" );
            methods[METHOD_setLocation121] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation121].setDisplayName ( "" );
            methods[METHOD_mouseEnter122] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter122].setDisplayName ( "" );
            methods[METHOD_size123] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("size", new Class[] {}));
            methods[METHOD_size123].setDisplayName ( "" );
            methods[METHOD_keyDown124] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown124].setDisplayName ( "" );
            methods[METHOD_move125] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move125].setDisplayName ( "" );
            methods[METHOD_inside126] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside126].setDisplayName ( "" );
            methods[METHOD_list127] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list127].setDisplayName ( "" );
            methods[METHOD_toString128] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString128].setDisplayName ( "" );
            methods[METHOD_keyUp129] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp129].setDisplayName ( "" );
            methods[METHOD_dispatchEvent130] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent130].setDisplayName ( "" );
            methods[METHOD_mouseDrag131] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag131].setDisplayName ( "" );
            methods[METHOD_setLocation132] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation132].setDisplayName ( "" );
            methods[METHOD_createImage133] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage133].setDisplayName ( "" );
            methods[METHOD_show134] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show134].setDisplayName ( "" );
            methods[METHOD_add135] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add135].setDisplayName ( "" );
            methods[METHOD_show136] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("show", new Class[] {}));
            methods[METHOD_show136].setDisplayName ( "" );
            methods[METHOD_mouseUp137] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp137].setDisplayName ( "" );
            methods[METHOD_lostFocus138] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus138].setDisplayName ( "" );
            methods[METHOD_imageUpdate139] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate139].setDisplayName ( "" );
            methods[METHOD_resize140] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize140].setDisplayName ( "" );
            methods[METHOD_mouseDown141] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown141].setDisplayName ( "" );
            methods[METHOD_action142] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action142].setDisplayName ( "" );
            methods[METHOD_createVolatileImage143] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage143].setDisplayName ( "" );
            methods[METHOD_nextFocus144] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus144].setDisplayName ( "" );
            methods[METHOD_getLocation145] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation145].setDisplayName ( "" );
            methods[METHOD_paintAll146] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll146].setDisplayName ( "" );
            methods[METHOD_createVolatileImage147] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage147].setDisplayName ( "" );
            methods[METHOD_createImage148] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage148].setDisplayName ( "" );
            methods[METHOD_handleEvent149] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent149].setDisplayName ( "" );
            methods[METHOD_repaint150] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint150].setDisplayName ( "" );
            methods[METHOD_repaint151] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint151].setDisplayName ( "" );
            methods[METHOD_mouseExit152] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit152].setDisplayName ( "" );
            methods[METHOD_prepareImage153] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage153].setDisplayName ( "" );
            methods[METHOD_prepareImage154] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage154].setDisplayName ( "" );
            methods[METHOD_remove155] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove155].setDisplayName ( "" );
            methods[METHOD_setSize156] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize156].setDisplayName ( "" );
            methods[METHOD_enable157] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable157].setDisplayName ( "" );
            methods[METHOD_transferFocus158] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus158].setDisplayName ( "" );
            methods[METHOD_mouseMove159] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove159].setDisplayName ( "" );
            methods[METHOD_getSize160] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize160].setDisplayName ( "" );
            methods[METHOD_repaint161] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint161].setDisplayName ( "" );
            methods[METHOD_enableInputMethods162] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods162].setDisplayName ( "" );
            methods[METHOD_bounds163] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds163].setDisplayName ( "" );
            methods[METHOD_setBounds164] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds164].setDisplayName ( "" );
            methods[METHOD_resize165] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize165].setDisplayName ( "" );
            methods[METHOD_list166] = new MethodDescriptor ( bsc.beans.JBITextArea.class.getMethod("list", new Class[] {}));
            methods[METHOD_list166].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

