/*
 * JBIXMLTable.java
 *
 * Created on July 3, 2003, 3:58 PM
 */

package bsc.beans;

import java.util.Vector;

/**
 *
 * @author  siga1996
 */
public class JBIXMLTable extends pv.jfcx.JPVTable {
    public final static int BSC_BOOLEAN		= 0;
    public final static int BSC_DATE		= 1;
    public final static int BSC_FLOAT		= 2;
    public final static int BSC_INT		= 3;
    public final static int BSC_STRING		= 4;
    public final static int BSC_PERCENT		= 5;
    public final static int BSC_IMAGEM		= 6;

    private String type = new String("");
    private boolean editable = false;
    private int floatPrecision = -1;
    
    /** Creates a new instance of JBIXMLTable */
    public JBIXMLTable() {
	super();
	
	// Propriedades
	setEditMode(3);
	
	setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
	java.util.Locale defaultLocale = bsc.core.BscStaticReferences.getBscDefaultLocale();
	if(defaultLocale==null)
	    defaultLocale = new java.util.Locale("pt","BR");
	setLocale(defaultLocale);
	
	// Prepara XML default e seta no tablemodel deste table
	BITableModel tableModel = new BITableModel();
	tableModel.setFloatPrecision( floatPrecision );
	bsc.xml.BIXMLVector vector = new bsc.xml.BIXMLVector("INICIALIZACOES", "INICIALIZACAO");
	bsc.xml.BIXMLAttributes attr = new bsc.xml.BIXMLAttributes();
	attr.set("TIPO", "INICIALIZACOES");
	attr.set("TAG000", "COLUNA");
	attr.set("CAB000", java.util.ResourceBundle.getBundle("international", defaultLocale).getString("JBIXMLTable_00001"));
	attr.set("CLA000", BITableModel.BSC_STRING);
	vector.setAttributes(attr);
	vector.addNewRecord().set("COLUNA", java.util.ResourceBundle.getBundle("international", defaultLocale).getString("JBIXMLTable_00002"));
	
	setDataSource(vector);
	setModel(tableModel);
	tableModel.setRowTracking(true);
	
	doLayout();
	
	//getTable().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
	//setPreferredSize(new java.awt.Dimension(0,800));
	
	
		/*		final BIFloatField floatField = new BIFloatField(0, 5, floatPrecision);
		floatField.setHorizontalAlignment(BIFloatField.RIGHT);
		 
		javax.swing.DefaultCellEditor floatEditor = new javax.swing.DefaultCellEditor(floatField) {
			public Object getCellEditorValue() {
				return new Float(floatField.getValue());
			}
		};
		this.setDefaultEditor( Float.class, floatEditor);*/
    }
    
/*	public java.awt.Component prepareEditor( javax.swing.table.TableCellEditor editor,
				int row, int column ) {
		java.awt.Component c = super.prepareEditor( editor, row, column );
		if ( c != null ) {
			if ( c instanceof javax.swing.text.JTextComponent ) {
				javax.swing.text.JTextComponent j = (javax.swing.text.JTextComponent) c;
				j.setCaretPosition(0);
				j.moveCaretPosition( j.getDocument().getLength() );
			}
		}
		return c;
	}
 */
    
    public void setDataSource( bsc.xml.BIXMLVector dataSource ) {
	type = dataSource.getAttributes().getString("TIPO");
	BITableModel tableModel = new BITableModel();
	
	tableModel.setFloatPrecision( floatPrecision );
	tableModel.setDataSource(dataSource);
	tableModel.setRowTracking(true);
	
	setColumnSelectionAllowed(false);
	setFocusCellBackground(new java.awt.Color(212,208,200));
	setSelectionMode(javax.swing.DefaultListSelectionModel.SINGLE_INTERVAL_SELECTION);
	setModel(tableModel);
	
	doLayout();
    }
    
    public bsc.xml.BIXMLVector getXMLData() {
	BITableModel tableModel = (BITableModel)getModel();
	return tableModel.getXMLData();
    }
    
    public void setFloatPrecision( int p ) {
	floatPrecision = p;
	if ( this.getModel() instanceof BITableModel )
	    ((BITableModel)this.getModel()).setFloatPrecision(p);
    }
    
    public int getFloatPrecision() {
	if ( this.getModel() instanceof BITableModel )
	    return ((BITableModel)this.getModel()).getFloatPrecision();
	else
	    return -1;
    }
    
    public String getType() {
	return String.valueOf( type );
    }
    
    public void setEditable( boolean edt ) {
	editable = edt;
    }
    
    public boolean getEditable() {
	return editable;
    }
    
    public void addNewRecord(bsc.xml.BIXMLRecord record){
	BITableModel tableModel = (BITableModel) this.getModel();
	tableModel.setPosVec(new Vector());
	tableModel.addNewRecord(record);
	tableModel.setRowTracking(true);
    }
    
    
    public void removeRecord(){
	BITableModel tableModel = (BITableModel) this.getModel();
	tableModel.removeRowRecord(this.getSelectedRow());
	//Atualiza��o das linhas de dados no vetor do modelo.
	tableModel.setPosVec(new Vector());
	tableModel.setRowTracking(true);
    }
    
    public int getSelectedRow(){
	BITableModel tableModel = (BITableModel) this.getModel();
	int rowSelected = tableModel.getOriginalRow(this.getFocusRow());
	return rowSelected;
    }
}


// Table Data Model
class BITableModel extends pv.jfcx.PVTableModel {
    public final static int BSC_BOOLEAN = 0;
    public final static int BSC_DATE = 1;
    public final static int BSC_FLOAT = 2;
    public final static int BSC_INT = 3;
    public final static int BSC_STRING = 4;
    public final static int BSC_PERCENT = 5;
    
    private int floatPrecision = -1;
    private boolean editable = true;
    private java.util.HashMap indice = new java.util.HashMap();
    
    // Colunas e Linhas
    Vector columns = new Vector();
    bsc.xml.BIXMLVector records = null;
    
    /** Creates a new instance of BITableModel */
    public BITableModel() {
	super();
	// Descomentar esta linha para obter cumulativo
	//addTableModelListener(new BITableModelAdapter());
    }
    
    public void setDataSource( bsc.xml.BIXMLVector records ) {
	int numberOfColumns = 0;
	String attrAux = null;
	
	this.records = records;
	columns.removeAllElements();
	
	for (java.util.Iterator e = records.getAttributes().getKeyNames(); e.hasNext() ;) {
	    attrAux = (String) e.next();
	    if ( attrAux.startsWith("TAG") )
		numberOfColumns++;
	}
	
	for ( int i = 0; i < numberOfColumns; i++ ) {
	    String zeros = "";
	    if ( i <= 9 )
		zeros = "00";
	    else if ( i <= 99 )
		zeros = "0";
	    
	    if ( !records.getAttributes().contains( "EDT" + zeros + i ) )
		columns.add( new BITableColumn(
		records.getAttributes().getString("TAG" + zeros + i),
		records.getAttributes().getString("CAB" + zeros + i),
		records.getAttributes().getString("CLA" + zeros + i) ) );
	    else
		columns.add( new BITableColumn(
		records.getAttributes().getString("TAG" + zeros + i),
		records.getAttributes().getString("CAB" + zeros + i),
		records.getAttributes().getString("CLA" + zeros + i),
		records.getAttributes().getBoolean("EDT" + zeros + i),
		records.getAttributes().getBoolean("CUM" + zeros + i) ) );
	}
	
	dataVector.clear();
	for(int row=0; row<records.size();row++) {
	    java.util.Vector v = new Vector();
	    dataVector.add(v);
	    for(int col=0; col<columns.size(); col++) {
		Object obj = "";
		BITableColumn column = (BITableColumn)columns.get(col);
		switch ( Integer.parseInt(column.getType()) ) {
		    case BSC_BOOLEAN:
			obj = new Boolean(records.get(row).getBoolean( column.getTag() ) );
			break;
		    case BSC_FLOAT:
/*						pv.jfcx.JPVNumericPlus jpvNumPlus = new pv.jfcx.JPVNumericPlus();
						tblView.setColumnType(col, pv.jfcx.JPVTable.DOUBLE, jpvNumPlus);
						jpvNumPlus.setLocale(java.util.Locale.getDefault());*/
			obj = new Double(records.get(row).getDouble( column.getTag()));
			break;
		    case BSC_STRING:
			obj = records.get(row).getString( column.getTag() );
			break;
		    case BSC_INT:
			obj = new Integer( records.get(row).getInt( column.getTag() ) );
			break;
		    case BSC_DATE:
			java.util.GregorianCalendar gc;
			gc = records.get(row).getDate( column.getTag() );
			
			if(gc != null){
			    java.text.DateFormat dateFormatter;
			    dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
			    obj = (String)dateFormatter.format(new java.util.Date(gc.get(gc.YEAR),gc.get(gc.MONTH),gc.get(gc.DAY_OF_MONTH)));
			} else
			    obj = "  /  /    ";
			
			break;
		    case BSC_PERCENT:
			obj = records.get(row).getString( column.getTag() ) + "%";
		}
		v.add(obj);
	    }
	}
	fireTableDataChanged();
    }
    
    
    public bsc.xml.BIXMLVector getXMLData() {
	String s;
	for(int row=0; row < records.size() ; row++) {
	    int actualRow = getOriginalRow(row);
	    
	    java.util.Vector v = (Vector)dataVector.get(row);
	    for(int col=0; col<columns.size(); col++) {
		Object obj = v.get(col);
		BITableColumn column = (BITableColumn)columns.get(col);
		switch ( Integer.parseInt(column.getType()) ) {
		    case BSC_BOOLEAN:
			Boolean b = (Boolean) obj;
			records.get(actualRow).set( column.getTag(), b.booleanValue() );
			break;
		    case BSC_FLOAT:
			Double d = (Double) obj;
			//Float f = (Float) obj;
			if ( floatPrecision == -1 ){
			    //records.get(row).set( column.getTag(), f.floatValue() );
			    records.get(actualRow).set( column.getTag(), d.doubleValue() );
			}else {
			    String pattern = new String( "##################." );
			    for ( int i = 0; i < floatPrecision; i++ )
				pattern = pattern + "0";
			    java.text.DecimalFormat df = new java.text.DecimalFormat(pattern);
			    records.get(actualRow).set( column.getTag(), d.doubleValue() );
			    //records.get(row).set( column.getTag(), Double.parseDouble(df.format(f.floatValue()).replace(',','.') ));
			}
			break;
		    case BSC_STRING:
			s = (String) obj;
			records.get(actualRow).set( column.getTag(), s );
			break;
		    case BSC_INT:
			Integer i = (Integer) obj;
			records.get(actualRow).set( column.getTag(), i.intValue() );
			break;
		    case BSC_DATE:
			s = (String) obj;
			records.get(actualRow).set( column.getTag(), s);
			break;
		    case BSC_PERCENT:
			s = (String) obj;
			records.get(actualRow).set( column.getTag(), s);
		}
	    }
	}
	return records;
    }
    
    private void addRecord( bsc.xml.BIXMLRecord record ) {
	if ( record != null )
	    records.add( record );
    }
    
    void setaIndice(){
	//verificar como identificar a chave, receber as chaves como parametro?
	for(int i=0; i<records.size(); i++) {
	    bsc.xml.BIXMLRecord rec = records.get(i);
	    String chave = rec.getString("ID");
	    indice.put(chave, new Integer(i));
	}
    }
    
    int getRowByIndex(String index) {
	Integer i = (Integer)indice.get(index);
	if(i != null)
	    return (i.intValue());
	else
	    return 0;
	
	
    }
    
    public void setFloatPrecision( int p ) {
	floatPrecision = p;
    }
    
    public int getFloatPrecision() {
	return floatPrecision;
    }
    
    public void setEditable( boolean edt ) {
	editable = edt;
    }
    
    public boolean getEditable() {
	return editable;
    }
    
    public int getColumnCount() {
	int cols = 0;
	if(columns != null)
	    cols = columns.size();
	return cols;
    }
    
    public int getRowCount() {
	int rows = 0;
	if(records != null)
	    rows = records.size();
	return rows;
    }
    
    public String getColumnName(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getHeader();
    }
    
    public boolean getColumnCumulative(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getCumulative();
    }
    
    public String getColumnTag(int col) {
	BITableColumn column = (BITableColumn) columns.get(col);
	return column.getTag();
    }
    
    public Class getColumnClass(int c) {
	return getValueAt(0, c).getClass();
    }
    
    public String getRecordType(int row) {
	row =  getOriginalRow(row);
	if (records.get(row).contains("TIPO"))
	    return String.valueOf(records.get(row).getString("TIPO"));
	else
	    return String.valueOf(records.get(row).getTagName());
    }
    
    public String getRecordID(int row) {
	row =  getOriginalRow(row);
	return String.valueOf(records.get(row).getString("ID"));
    }
    
    public String getName(int row) {
	row =  getOriginalRow(row);
	return String.valueOf(records.get(row).getString("NOME"));
    }
    
    public boolean isCellEditable(int row, int col) {
	row =  getOriginalRow(row);
	if ( editable ) {
	    BITableColumn column = (BITableColumn) columns.get(col);
	    return column.getEditable();
	}
	else
	    return false;
    }
    
    /*
     *Metodo para adicionar um �nico registro
     */
    public void addNewRecord( bsc.xml.BIXMLRecord record ) {
	int rowCount = this.records.size();
	this.records.add(record);
	java.util.Vector v = new Vector();
	dataVector.add(v);
	for(int col=0; col<columns.size(); col++) {
	    BITableColumn column = (BITableColumn)columns.get(col);
	    v.add(creatColumnObject(column,rowCount));
	}
	fireTableDataChanged();
    }
    
    /*
     *M�todo para remove um �nico registro.
     */
    public void removeRowRecord(int row){
	if(row > -1){
	    int actualRow = getOriginalRow(row);
	    this.records.remove(row);
	    getDataVector().removeElementAt(actualRow);
	    fireTableDataChanged();
	}
    }
    
    
    private Object creatColumnObject(BITableColumn column, int row){
	Object obj = "";
	
	switch ( Integer.parseInt(column.getType()) ) {
	    case JBIXMLTable.BSC_BOOLEAN:
		obj = new Boolean(records.get(row).getBoolean(column.getTag()));
		break;
	    case JBIXMLTable.BSC_FLOAT:
		obj = new Double(records.get(row).getDouble(column.getTag()));
		break;
	    case JBIXMLTable.BSC_STRING:
		obj = records.get(row).getString(column.getTag());
		break;
	    case JBIXMLTable.BSC_INT:
		obj = new Integer(records.get(row).getInt(column.getTag()));
		break;
	    case JBIXMLTable.BSC_DATE:
		java.util.GregorianCalendar gc;
		gc = records.get(row).getDate( column.getTag() );
		
		if(gc != null){
		    java.text.DateFormat dateFormatter;
		    dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
		    obj = (String)dateFormatter.format(new java.util.Date(gc.get(gc.YEAR),gc.get(gc.MONTH),gc.get(gc.DAY_OF_MONTH)));
		} else
		    obj = "  /  /    ";
		
		break;
	    case JBIXMLTable.BSC_PERCENT:
		obj = records.get(row).getString(column.getTag() ) + "%";
		break;
	    case JBIXMLTable.BSC_IMAGEM:
		Integer iValor = new Integer(records.get(row).getInt(column.getTag()));
		int i = iValor.intValue();
		javax.swing.ImageIcon BSCImagem = new bsc.core.BscImageResources().getImage(i);
		obj = BSCImagem;
		break;
	}
	
	return obj;
    }
    
    
    
/*	public boolean setCell(Object obj, int row, int col) {
		String s = null;
		BITableColumn column = (BITableColumn) columns.get(col);
 
		switch ( Integer.parseInt(column.getType()) ) {
			case BSC_BOOLEAN:
				Boolean b = (Boolean) obj;
				records.get(row).set( column.getTag(), b.booleanValue() );
				break;
			case BSC_FLOAT:
				Float f = (Float) obj;
				if ( floatPrecision == -1 )
					records.get(row).set( column.getTag(), f.floatValue() );
				else {
					String pattern = new String( "##################." );
					for ( int i = 0; i < floatPrecision; i++ )
						pattern = pattern + "0";
					java.text.DecimalFormat df = new java.text.DecimalFormat(pattern);
					records.get(row).set( column.getTag(), Double.parseDouble(df.format(f.floatValue()).replace(',','.') ));
				}
				break;
			case BSC_STRING:
				s = (String) obj;
				records.get(row).set( column.getTag(), s );
				break;
			case BSC_INT:
				Integer i = (Integer) obj;
				records.get(row).set( column.getTag(), i.intValue() );
				break;
			case BSC_DATE:
				s = (String) obj;
				records.get(row).set( column.getTag(), s);
				break;
			case BSC_PERCENT:
				s = (String) obj;
				records.get(row).set( column.getTag(), s);
		}
		fireTableCellUpdated(row, col);
		return true;
	}
 
	public Object getCell(int row, int col) {
		Object obj = null;
		BITableColumn column = (BITableColumn) columns.get(col);
 
		switch ( Integer.parseInt(column.getType()) ) {
			case BSC_BOOLEAN:
				obj = new Boolean (records.get(row).getBoolean( column.getTag() ) );
				break;
			case BSC_FLOAT:
				obj = new Float( records.get(row).getFloat( column.getTag() ) );
				break;
			case BSC_STRING:
				obj = records.get(row).getString( column.getTag() );
				break;
			case BSC_INT:
				obj = new Integer( records.get(row).getInt( column.getTag() ) );
				break;
			case BSC_DATE:
				java.util.GregorianCalendar gc;
				gc = records.get(row).getDate( column.getTag() );
 
				if(gc != null){
					java.text.DateFormat dateFormatter;
					dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
					//obj = (String)dateFormatter.format(new java.util.Date(gc.get(gc.YEAR),gc.get(gc.MONTH),gc.get(gc.DAY_OF_MONTH)));
					obj = (String)dateFormatter.format(gc);
				} else
					obj = "  /  /    ";
 
				break;
			case BSC_PERCENT:
				obj = records.get(row).getString( column.getTag() ) + "%";
		}
		return obj;
	}
 */
}

// Cada coluna da tabela XML
class BITableColumn {
    private String tag = new String("");
    private String header = new String("");
    private String type = new String("");
    private boolean editable = false;
    private boolean cumulative = false;
    
    public BITableColumn( String tag, String header, String type, boolean editable, boolean cumulative ) {
	this.tag = String.valueOf( tag );
	this.header = String.valueOf( header );
	this.type = String.valueOf( type );
	this.editable = editable;
	this.cumulative = cumulative;
    }
    
    public BITableColumn( String tag, String header, String type) {
	this.tag = String.valueOf( tag );
	this.header = String.valueOf( header );
	this.type = String.valueOf( type );
    }
    
    public void setTag( String tagAux ) {
	tag = String.valueOf( tagAux );
    }
    
    public String getTag() {
	return String.valueOf( tag );
    }
    
    public void setHeader( String headerAux ) {
	header = String.valueOf( headerAux );
    }
    
    public String getHeader() {
	return String.valueOf( header );
    }
    
    public String toString() {
	return getHeader();
    }
    
    public void setType( String typeAux ) {
	type = String.valueOf( typeAux );
    }
    
    public String getType() {
	return String.valueOf( type );
    }
    
    public void setEditable( boolean editableAux ) {
	editable = editableAux;
    }
    
    public boolean getEditable() {
	return editable;
    }
    
    public void setCumulative( boolean cumulativeAux ) {
	cumulative = cumulativeAux;
    }
    
    public boolean getCumulative() {
	return cumulative;
    }
    
}

// Eventos da tabela
class BITableModelAdapter implements javax.swing.event.TableModelListener {
    public void tableChanged(javax.swing.event.TableModelEvent e) {}
}
