/*
 * JBIXMLTableBeanInfo.java
 *
 * Created on 27 de Julho de 2006, 15:13
 */

package bsc.beans;

import java.beans.*;

/**
 * @author alexandreas
 */
public class JBIXMLTableBeanInfo extends SimpleBeanInfo {
    
    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JBIXMLTable.class , null );//GEN-HEADEREND:BeanDescriptor
	
	// Here you can add code for customizing the BeanDescriptor.
	
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
    
    
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoCreateColumnsFromModel = 5;
    private static final int PROPERTY_autoResizeMode = 6;
    private static final int PROPERTY_autoscrolls = 7;
    private static final int PROPERTY_background = 8;
    private static final int PROPERTY_background2 = 9;
    private static final int PROPERTY_background2Delta = 10;
    private static final int PROPERTY_backgroundSet = 11;
    private static final int PROPERTY_border = 12;
    private static final int PROPERTY_bounds = 13;
    private static final int PROPERTY_cellEditor = 14;
    private static final int PROPERTY_cellSelectionEnabled = 15;
    private static final int PROPERTY_colorModel = 16;
    private static final int PROPERTY_columnClass = 17;
    private static final int PROPERTY_columnCount = 18;
    private static final int PROPERTY_columnModel = 19;
    private static final int PROPERTY_columnName = 20;
    private static final int PROPERTY_columnSelectionAllowed = 21;
    private static final int PROPERTY_columnSelectionInterval = 22;
    private static final int PROPERTY_component = 23;
    private static final int PROPERTY_componentCount = 24;
    private static final int PROPERTY_componentListeners = 25;
    private static final int PROPERTY_componentOrientation = 26;
    private static final int PROPERTY_components = 27;
    private static final int PROPERTY_containerListeners = 28;
    private static final int PROPERTY_cursor = 29;
    private static final int PROPERTY_cursorSet = 30;
    private static final int PROPERTY_dataSource = 31;
    private static final int PROPERTY_debugGraphicsOptions = 32;
    private static final int PROPERTY_displayable = 33;
    private static final int PROPERTY_doubleBuffered = 34;
    private static final int PROPERTY_doubleClickSort = 35;
    private static final int PROPERTY_dragEnabled = 36;
    private static final int PROPERTY_dropTarget = 37;
    private static final int PROPERTY_editable = 38;
    private static final int PROPERTY_editing = 39;
    private static final int PROPERTY_editingColumn = 40;
    private static final int PROPERTY_editingRow = 41;
    private static final int PROPERTY_editMode = 42;
    private static final int PROPERTY_editorComponent = 43;
    private static final int PROPERTY_enabled = 44;
    private static final int PROPERTY_enableDeleteKey = 45;
    private static final int PROPERTY_enableInsertKey = 46;
    private static final int PROPERTY_enableSortData = 47;
    private static final int PROPERTY_filterEvents = 48;
    private static final int PROPERTY_firstColumnStyle = 49;
    private static final int PROPERTY_floatPrecision = 50;
    private static final int PROPERTY_focusable = 51;
    private static final int PROPERTY_focusCell = 52;
    private static final int PROPERTY_focusCellBackground = 53;
    private static final int PROPERTY_focusCellBorder = 54;
    private static final int PROPERTY_focusCellForeground = 55;
    private static final int PROPERTY_focusColumn = 56;
    private static final int PROPERTY_focusCycleRoot = 57;
    private static final int PROPERTY_focusCycleRootAncestor = 58;
    private static final int PROPERTY_focusListeners = 59;
    private static final int PROPERTY_focusOwner = 60;
    private static final int PROPERTY_focusRow = 61;
    private static final int PROPERTY_focusTraversable = 62;
    private static final int PROPERTY_focusTraversalKeys = 63;
    private static final int PROPERTY_focusTraversalKeysEnabled = 64;
    private static final int PROPERTY_focusTraversalPolicy = 65;
    private static final int PROPERTY_focusTraversalPolicySet = 66;
    private static final int PROPERTY_font = 67;
    private static final int PROPERTY_fontSet = 68;
    private static final int PROPERTY_foreground = 69;
    private static final int PROPERTY_foregroundSet = 70;
    private static final int PROPERTY_graphics = 71;
    private static final int PROPERTY_graphicsConfiguration = 72;
    private static final int PROPERTY_gridColor = 73;
    private static final int PROPERTY_headerAlignment = 74;
    private static final int PROPERTY_headerBorder = 75;
    private static final int PROPERTY_headerHeight = 76;
    private static final int PROPERTY_headerMultiLine = 77;
    private static final int PROPERTY_height = 78;
    private static final int PROPERTY_hierarchyBoundsListeners = 79;
    private static final int PROPERTY_hierarchyListeners = 80;
    private static final int PROPERTY_iconAt = 81;
    private static final int PROPERTY_ignoreRepaint = 82;
    private static final int PROPERTY_incrementOnEscape = 83;
    private static final int PROPERTY_inputContext = 84;
    private static final int PROPERTY_inputMethodListeners = 85;
    private static final int PROPERTY_inputMethodRequests = 86;
    private static final int PROPERTY_inputVerifier = 87;
    private static final int PROPERTY_insets = 88;
    private static final int PROPERTY_intercellSpacing = 89;
    private static final int PROPERTY_keyListeners = 90;
    private static final int PROPERTY_LAF = 91;
    private static final int PROPERTY_layout = 92;
    private static final int PROPERTY_lightweight = 93;
    private static final int PROPERTY_listOfStaticColumns = 94;
    private static final int PROPERTY_locale = 95;
    private static final int PROPERTY_locationOnScreen = 96;
    private static final int PROPERTY_lockedColumns = 97;
    private static final int PROPERTY_managingFocus = 98;
    private static final int PROPERTY_maximumSize = 99;
    private static final int PROPERTY_maximumSizeSet = 100;
    private static final int PROPERTY_mergeMovement = 101;
    private static final int PROPERTY_mergeNulls = 102;
    private static final int PROPERTY_mergeOnSort = 103;
    private static final int PROPERTY_mergeUpdate = 104;
    private static final int PROPERTY_minimumSize = 105;
    private static final int PROPERTY_minimumSizeSet = 106;
    private static final int PROPERTY_model = 107;
    private static final int PROPERTY_mouseListeners = 108;
    private static final int PROPERTY_mouseMotionListeners = 109;
    private static final int PROPERTY_mouseWheelListeners = 110;
    private static final int PROPERTY_name = 111;
    private static final int PROPERTY_nextFocusableComponent = 112;
    private static final int PROPERTY_opaque = 113;
    private static final int PROPERTY_optimizedDrawingEnabled = 114;
    private static final int PROPERTY_paintingTile = 115;
    private static final int PROPERTY_parent = 116;
    private static final int PROPERTY_peer = 117;
    private static final int PROPERTY_preferredScrollableViewportSize = 118;
    private static final int PROPERTY_preferredSize = 119;
    private static final int PROPERTY_preferredSizeSet = 120;
    private static final int PROPERTY_propertyChangeListeners = 121;
    private static final int PROPERTY_PVEditors = 122;
    private static final int PROPERTY_PVUI = 123;
    private static final int PROPERTY_registeredKeyStrokes = 124;
    private static final int PROPERTY_requestFocusEnabled = 125;
    private static final int PROPERTY_rootPane = 126;
    private static final int PROPERTY_rowCount = 127;
    private static final int PROPERTY_rowMargin = 128;
    private static final int PROPERTY_rowSelectionAllowed = 129;
    private static final int PROPERTY_rowSelectionInterval = 130;
    private static final int PROPERTY_scrollableTracksViewportHeight = 131;
    private static final int PROPERTY_scrollableTracksViewportWidth = 132;
    private static final int PROPERTY_scrollAfterSort = 133;
    private static final int PROPERTY_selectedColumn = 134;
    private static final int PROPERTY_selectedColumnCount = 135;
    private static final int PROPERTY_selectedColumns = 136;
    private static final int PROPERTY_selectedRow = 137;
    private static final int PROPERTY_selectedRowCount = 138;
    private static final int PROPERTY_selectedRows = 139;
    private static final int PROPERTY_selectionBackground = 140;
    private static final int PROPERTY_selectionForeground = 141;
    private static final int PROPERTY_selectionMode = 142;
    private static final int PROPERTY_selectionModel = 143;
    private static final int PROPERTY_showGrid = 144;
    private static final int PROPERTY_showHorizontalLines = 145;
    private static final int PROPERTY_showing = 146;
    private static final int PROPERTY_showSortIcons = 147;
    private static final int PROPERTY_showVerticalLines = 148;
    private static final int PROPERTY_startedByMouse = 149;
    private static final int PROPERTY_surrendersFocusOnKeystroke = 150;
    private static final int PROPERTY_tableHeader = 151;
    private static final int PROPERTY_toolkit = 152;
    private static final int PROPERTY_toolTipText = 153;
    private static final int PROPERTY_topLevelAncestor = 154;
    private static final int PROPERTY_transferHandler = 155;
    private static final int PROPERTY_treeLock = 156;
    private static final int PROPERTY_type = 157;
    private static final int PROPERTY_UI = 158;
    private static final int PROPERTY_UIClassID = 159;
    private static final int PROPERTY_valid = 160;
    private static final int PROPERTY_validateRoot = 161;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 162;
    private static final int PROPERTY_vetoableChangeListeners = 163;
    private static final int PROPERTY_visible = 164;
    private static final int PROPERTY_visibleRect = 165;
    private static final int PROPERTY_width = 166;
    private static final int PROPERTY_x = 167;
    private static final int PROPERTY_XMLData = 168;
    private static final int PROPERTY_y = 169;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[170];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JBIXMLTable.class, "getAccessibleContext", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JBIXMLTable.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JBIXMLTable.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JBIXMLTable.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JBIXMLTable.class, "getAncestorListeners", null );
            properties[PROPERTY_autoCreateColumnsFromModel] = new PropertyDescriptor ( "autoCreateColumnsFromModel", JBIXMLTable.class, "getAutoCreateColumnsFromModel", "setAutoCreateColumnsFromModel" );
            properties[PROPERTY_autoResizeMode] = new PropertyDescriptor ( "autoResizeMode", JBIXMLTable.class, "getAutoResizeMode", "setAutoResizeMode" );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JBIXMLTable.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JBIXMLTable.class, "getBackground", "setBackground" );
            properties[PROPERTY_background2] = new PropertyDescriptor ( "background2", JBIXMLTable.class, "getBackground2", "setBackground2" );
            properties[PROPERTY_background2Delta] = new PropertyDescriptor ( "background2Delta", JBIXMLTable.class, "getBackground2Delta", "setBackground2Delta" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JBIXMLTable.class, "isBackgroundSet", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JBIXMLTable.class, "getBorder", "setBorder" );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JBIXMLTable.class, "getBounds", "setBounds" );
            properties[PROPERTY_cellEditor] = new PropertyDescriptor ( "cellEditor", JBIXMLTable.class, "getCellEditor", "setCellEditor" );
            properties[PROPERTY_cellSelectionEnabled] = new PropertyDescriptor ( "cellSelectionEnabled", JBIXMLTable.class, "getCellSelectionEnabled", "setCellSelectionEnabled" );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JBIXMLTable.class, "getColorModel", null );
            properties[PROPERTY_columnClass] = new IndexedPropertyDescriptor ( "columnClass", JBIXMLTable.class, null, null, "getColumnClass", null );
            properties[PROPERTY_columnCount] = new PropertyDescriptor ( "columnCount", JBIXMLTable.class, "getColumnCount", null );
            properties[PROPERTY_columnModel] = new PropertyDescriptor ( "columnModel", JBIXMLTable.class, "getColumnModel", "setColumnModel" );
            properties[PROPERTY_columnName] = new IndexedPropertyDescriptor ( "columnName", JBIXMLTable.class, null, null, "getColumnName", null );
            properties[PROPERTY_columnSelectionAllowed] = new PropertyDescriptor ( "columnSelectionAllowed", JBIXMLTable.class, "getColumnSelectionAllowed", "setColumnSelectionAllowed" );
            properties[PROPERTY_columnSelectionInterval] = new IndexedPropertyDescriptor ( "columnSelectionInterval", JBIXMLTable.class, null, null, null, "setColumnSelectionInterval" );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JBIXMLTable.class, null, null, "getComponent", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JBIXMLTable.class, "getComponentCount", null );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JBIXMLTable.class, "getComponentListeners", null );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JBIXMLTable.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JBIXMLTable.class, "getComponents", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JBIXMLTable.class, "getContainerListeners", null );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JBIXMLTable.class, "getCursor", "setCursor" );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JBIXMLTable.class, "isCursorSet", null );
            properties[PROPERTY_dataSource] = new PropertyDescriptor ( "dataSource", JBIXMLTable.class, null, "setDataSource" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JBIXMLTable.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JBIXMLTable.class, "isDisplayable", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JBIXMLTable.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_doubleClickSort] = new PropertyDescriptor ( "doubleClickSort", JBIXMLTable.class, "getDoubleClickSort", "setDoubleClickSort" );
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", JBIXMLTable.class, "getDragEnabled", "setDragEnabled" );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JBIXMLTable.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", JBIXMLTable.class, "getEditable", "setEditable" );
            properties[PROPERTY_editing] = new PropertyDescriptor ( "editing", JBIXMLTable.class, "isEditing", null );
            properties[PROPERTY_editingColumn] = new PropertyDescriptor ( "editingColumn", JBIXMLTable.class, "getEditingColumn", "setEditingColumn" );
            properties[PROPERTY_editingRow] = new PropertyDescriptor ( "editingRow", JBIXMLTable.class, "getEditingRow", "setEditingRow" );
            properties[PROPERTY_editMode] = new PropertyDescriptor ( "editMode", JBIXMLTable.class, "getEditMode", "setEditMode" );
            properties[PROPERTY_editorComponent] = new PropertyDescriptor ( "editorComponent", JBIXMLTable.class, "getEditorComponent", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JBIXMLTable.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_enableDeleteKey] = new PropertyDescriptor ( "enableDeleteKey", JBIXMLTable.class, "getEnableDeleteKey", "setEnableDeleteKey" );
            properties[PROPERTY_enableInsertKey] = new PropertyDescriptor ( "enableInsertKey", JBIXMLTable.class, "getEnableInsertKey", "setEnableInsertKey" );
            properties[PROPERTY_enableSortData] = new PropertyDescriptor ( "enableSortData", JBIXMLTable.class, "getEnableSortData", "setEnableSortData" );
            properties[PROPERTY_filterEvents] = new PropertyDescriptor ( "filterEvents", JBIXMLTable.class, "getFilterEvents", "setFilterEvents" );
            properties[PROPERTY_firstColumnStyle] = new PropertyDescriptor ( "firstColumnStyle", JBIXMLTable.class, "getFirstColumnStyle", "setFirstColumnStyle" );
            properties[PROPERTY_floatPrecision] = new PropertyDescriptor ( "floatPrecision", JBIXMLTable.class, "getFloatPrecision", "setFloatPrecision" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JBIXMLTable.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_focusCell] = new IndexedPropertyDescriptor ( "focusCell", JBIXMLTable.class, null, null, null, "setFocusCell" );
            properties[PROPERTY_focusCellBackground] = new PropertyDescriptor ( "focusCellBackground", JBIXMLTable.class, "getFocusCellBackground", "setFocusCellBackground" );
            properties[PROPERTY_focusCellBorder] = new PropertyDescriptor ( "focusCellBorder", JBIXMLTable.class, "getFocusCellBorder", "setFocusCellBorder" );
            properties[PROPERTY_focusCellForeground] = new PropertyDescriptor ( "focusCellForeground", JBIXMLTable.class, "getFocusCellForeground", "setFocusCellForeground" );
            properties[PROPERTY_focusColumn] = new PropertyDescriptor ( "focusColumn", JBIXMLTable.class, "getFocusColumn", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JBIXMLTable.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JBIXMLTable.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JBIXMLTable.class, "getFocusListeners", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JBIXMLTable.class, "isFocusOwner", null );
            properties[PROPERTY_focusRow] = new PropertyDescriptor ( "focusRow", JBIXMLTable.class, "getFocusRow", null );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JBIXMLTable.class, "isFocusTraversable", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JBIXMLTable.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JBIXMLTable.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JBIXMLTable.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JBIXMLTable.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JBIXMLTable.class, "getFont", "setFont" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JBIXMLTable.class, "isFontSet", null );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JBIXMLTable.class, "getForeground", "setForeground" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JBIXMLTable.class, "isForegroundSet", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JBIXMLTable.class, "getGraphics", null );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JBIXMLTable.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_gridColor] = new PropertyDescriptor ( "gridColor", JBIXMLTable.class, "getGridColor", "setGridColor" );
            properties[PROPERTY_headerAlignment] = new PropertyDescriptor ( "headerAlignment", JBIXMLTable.class, "getHeaderAlignment", "setHeaderAlignment" );
            properties[PROPERTY_headerBorder] = new PropertyDescriptor ( "headerBorder", JBIXMLTable.class, "getHeaderBorder", "setHeaderBorder" );
            properties[PROPERTY_headerHeight] = new PropertyDescriptor ( "headerHeight", JBIXMLTable.class, "getHeaderHeight", "setHeaderHeight" );
            properties[PROPERTY_headerMultiLine] = new PropertyDescriptor ( "headerMultiLine", JBIXMLTable.class, "getHeaderMultiLine", "setHeaderMultiLine" );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JBIXMLTable.class, "getHeight", null );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JBIXMLTable.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JBIXMLTable.class, "getHierarchyListeners", null );
            properties[PROPERTY_iconAt] = new IndexedPropertyDescriptor ( "iconAt", JBIXMLTable.class, null, null, "getIconAt", null );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JBIXMLTable.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_incrementOnEscape] = new PropertyDescriptor ( "incrementOnEscape", JBIXMLTable.class, "getIncrementOnEscape", "setIncrementOnEscape" );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JBIXMLTable.class, "getInputContext", null );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JBIXMLTable.class, "getInputMethodListeners", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JBIXMLTable.class, "getInputMethodRequests", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JBIXMLTable.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JBIXMLTable.class, "getInsets", null );
            properties[PROPERTY_intercellSpacing] = new PropertyDescriptor ( "intercellSpacing", JBIXMLTable.class, "getIntercellSpacing", "setIntercellSpacing" );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JBIXMLTable.class, "getKeyListeners", null );
            properties[PROPERTY_LAF] = new PropertyDescriptor ( "LAF", JBIXMLTable.class, "getLAF", "setLAF" );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JBIXMLTable.class, "getLayout", "setLayout" );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JBIXMLTable.class, "isLightweight", null );
            properties[PROPERTY_listOfStaticColumns] = new PropertyDescriptor ( "listOfStaticColumns", JBIXMLTable.class, "getListOfStaticColumns", "setListOfStaticColumns" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JBIXMLTable.class, "getLocale", "setLocale" );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JBIXMLTable.class, "getLocationOnScreen", null );
            properties[PROPERTY_lockedColumns] = new PropertyDescriptor ( "lockedColumns", JBIXMLTable.class, "getLockedColumns", "setLockedColumns" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JBIXMLTable.class, "isManagingFocus", null );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JBIXMLTable.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JBIXMLTable.class, "isMaximumSizeSet", null );
            properties[PROPERTY_mergeMovement] = new PropertyDescriptor ( "mergeMovement", JBIXMLTable.class, "getMergeMovement", "setMergeMovement" );
            properties[PROPERTY_mergeNulls] = new PropertyDescriptor ( "mergeNulls", JBIXMLTable.class, "getMergeNulls", "setMergeNulls" );
            properties[PROPERTY_mergeOnSort] = new PropertyDescriptor ( "mergeOnSort", JBIXMLTable.class, "getMergeOnSort", "setMergeOnSort" );
            properties[PROPERTY_mergeUpdate] = new PropertyDescriptor ( "mergeUpdate", JBIXMLTable.class, "getMergeUpdate", "setMergeUpdate" );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JBIXMLTable.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JBIXMLTable.class, "isMinimumSizeSet", null );
            properties[PROPERTY_model] = new PropertyDescriptor ( "model", JBIXMLTable.class, "getModel", "setModel" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JBIXMLTable.class, "getMouseListeners", null );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JBIXMLTable.class, "getMouseMotionListeners", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JBIXMLTable.class, "getMouseWheelListeners", null );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JBIXMLTable.class, "getName", "setName" );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JBIXMLTable.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JBIXMLTable.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JBIXMLTable.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JBIXMLTable.class, "isPaintingTile", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JBIXMLTable.class, "getParent", null );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JBIXMLTable.class, "getPeer", null );
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", JBIXMLTable.class, "getPreferredScrollableViewportSize", "setPreferredScrollableViewportSize" );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JBIXMLTable.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JBIXMLTable.class, "isPreferredSizeSet", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JBIXMLTable.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_PVEditors] = new PropertyDescriptor ( "PVEditors", JBIXMLTable.class, "getPVEditors", null );
            properties[PROPERTY_PVUI] = new PropertyDescriptor ( "PVUI", JBIXMLTable.class, "getPVUI", "setPVUI" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JBIXMLTable.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JBIXMLTable.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JBIXMLTable.class, "getRootPane", null );
            properties[PROPERTY_rowCount] = new PropertyDescriptor ( "rowCount", JBIXMLTable.class, "getRowCount", null );
            properties[PROPERTY_rowMargin] = new PropertyDescriptor ( "rowMargin", JBIXMLTable.class, "getRowMargin", "setRowMargin" );
            properties[PROPERTY_rowSelectionAllowed] = new PropertyDescriptor ( "rowSelectionAllowed", JBIXMLTable.class, "getRowSelectionAllowed", "setRowSelectionAllowed" );
            properties[PROPERTY_rowSelectionInterval] = new IndexedPropertyDescriptor ( "rowSelectionInterval", JBIXMLTable.class, null, null, null, "setRowSelectionInterval" );
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", JBIXMLTable.class, "getScrollableTracksViewportHeight", null );
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", JBIXMLTable.class, "getScrollableTracksViewportWidth", null );
            properties[PROPERTY_scrollAfterSort] = new PropertyDescriptor ( "scrollAfterSort", JBIXMLTable.class, "getScrollAfterSort", "setScrollAfterSort" );
            properties[PROPERTY_selectedColumn] = new PropertyDescriptor ( "selectedColumn", JBIXMLTable.class, "getSelectedColumn", null );
            properties[PROPERTY_selectedColumnCount] = new PropertyDescriptor ( "selectedColumnCount", JBIXMLTable.class, "getSelectedColumnCount", null );
            properties[PROPERTY_selectedColumns] = new PropertyDescriptor ( "selectedColumns", JBIXMLTable.class, "getSelectedColumns", null );
            properties[PROPERTY_selectedRow] = new PropertyDescriptor ( "selectedRow", JBIXMLTable.class, "getSelectedRow", null );
            properties[PROPERTY_selectedRowCount] = new PropertyDescriptor ( "selectedRowCount", JBIXMLTable.class, "getSelectedRowCount", null );
            properties[PROPERTY_selectedRows] = new PropertyDescriptor ( "selectedRows", JBIXMLTable.class, "getSelectedRows", null );
            properties[PROPERTY_selectionBackground] = new PropertyDescriptor ( "selectionBackground", JBIXMLTable.class, "getSelectionBackground", "setSelectionBackground" );
            properties[PROPERTY_selectionForeground] = new PropertyDescriptor ( "selectionForeground", JBIXMLTable.class, "getSelectionForeground", "setSelectionForeground" );
            properties[PROPERTY_selectionMode] = new PropertyDescriptor ( "selectionMode", JBIXMLTable.class, null, "setSelectionMode" );
            properties[PROPERTY_selectionModel] = new PropertyDescriptor ( "selectionModel", JBIXMLTable.class, "getSelectionModel", "setSelectionModel" );
            properties[PROPERTY_showGrid] = new PropertyDescriptor ( "showGrid", JBIXMLTable.class, null, "setShowGrid" );
            properties[PROPERTY_showHorizontalLines] = new PropertyDescriptor ( "showHorizontalLines", JBIXMLTable.class, "getShowHorizontalLines", "setShowHorizontalLines" );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JBIXMLTable.class, "isShowing", null );
            properties[PROPERTY_showSortIcons] = new PropertyDescriptor ( "showSortIcons", JBIXMLTable.class, "getShowSortIcons", "setShowSortIcons" );
            properties[PROPERTY_showVerticalLines] = new PropertyDescriptor ( "showVerticalLines", JBIXMLTable.class, "getShowVerticalLines", "setShowVerticalLines" );
            properties[PROPERTY_startedByMouse] = new PropertyDescriptor ( "startedByMouse", JBIXMLTable.class, "isStartedByMouse", null );
            properties[PROPERTY_surrendersFocusOnKeystroke] = new PropertyDescriptor ( "surrendersFocusOnKeystroke", JBIXMLTable.class, "getSurrendersFocusOnKeystroke", "setSurrendersFocusOnKeystroke" );
            properties[PROPERTY_tableHeader] = new PropertyDescriptor ( "tableHeader", JBIXMLTable.class, "getTableHeader", "setTableHeader" );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JBIXMLTable.class, "getToolkit", null );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JBIXMLTable.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JBIXMLTable.class, "getTopLevelAncestor", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JBIXMLTable.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JBIXMLTable.class, "getTreeLock", null );
            properties[PROPERTY_type] = new PropertyDescriptor ( "type", JBIXMLTable.class, "getType", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JBIXMLTable.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JBIXMLTable.class, "getUIClassID", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JBIXMLTable.class, "isValid", null );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JBIXMLTable.class, "isValidateRoot", null );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JBIXMLTable.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JBIXMLTable.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JBIXMLTable.class, "isVisible", "setVisible" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JBIXMLTable.class, "getVisibleRect", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JBIXMLTable.class, "getWidth", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JBIXMLTable.class, "getX", null );
            properties[PROPERTY_XMLData] = new PropertyDescriptor ( "XMLData", JBIXMLTable.class, "getXMLData", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JBIXMLTable.class, "getY", null );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
	
	// Here you can add code for customizing the properties array.
	
        return properties;         }//GEN-LAST:Properties
    
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JBIXMLTable.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
	
	// Here you can add code for customizing the event sets array.
	
        return eventSets;         }//GEN-LAST:Events
    
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addCell2 = 2;
    private static final int METHOD_addColumn3 = 3;
    private static final int METHOD_addColumnSelectionInterval4 = 4;
    private static final int METHOD_addNotify5 = 5;
    private static final int METHOD_addPropertyChangeListener6 = 6;
    private static final int METHOD_addRowSelectionInterval7 = 7;
    private static final int METHOD_applyComponentOrientation8 = 8;
    private static final int METHOD_areFocusTraversalKeysSet9 = 9;
    private static final int METHOD_bounds10 = 10;
    private static final int METHOD_cellAt11 = 11;
    private static final int METHOD_changeSelection12 = 12;
    private static final int METHOD_checkImage13 = 13;
    private static final int METHOD_clearSelection14 = 14;
    private static final int METHOD_columnAdded15 = 15;
    private static final int METHOD_columnAtPoint16 = 16;
    private static final int METHOD_columnMarginChanged17 = 17;
    private static final int METHOD_columnMoved18 = 18;
    private static final int METHOD_columnRemoved19 = 19;
    private static final int METHOD_columnSelectionChanged20 = 20;
    private static final int METHOD_computeVisibleRect21 = 21;
    private static final int METHOD_contains22 = 22;
    private static final int METHOD_convertColumnIndexToModel23 = 23;
    private static final int METHOD_convertColumnIndexToView24 = 24;
    private static final int METHOD_countComponents25 = 25;
    private static final int METHOD_createDefaultColumnsFromModel26 = 26;
    private static final int METHOD_createHeadRenderers27 = 27;
    private static final int METHOD_createImage28 = 28;
    private static final int METHOD_createScrollPaneForTable29 = 29;
    private static final int METHOD_createToolTip30 = 30;
    private static final int METHOD_createVolatileImage31 = 31;
    private static final int METHOD_deliverEvent32 = 32;
    private static final int METHOD_disable33 = 33;
    private static final int METHOD_dispatchEvent34 = 34;
    private static final int METHOD_doLayout35 = 35;
    private static final int METHOD_editCellAt36 = 36;
    private static final int METHOD_editingCanceled37 = 37;
    private static final int METHOD_editingStopped38 = 38;
    private static final int METHOD_editorListener39 = 39;
    private static final int METHOD_editors40 = 40;
    private static final int METHOD_enable41 = 41;
    private static final int METHOD_enableInputMethods42 = 42;
    private static final int METHOD_findComponentAt43 = 43;
    private static final int METHOD_firePropertyChange44 = 44;
    private static final int METHOD_getActionForKeyStroke45 = 45;
    private static final int METHOD_getBounds46 = 46;
    private static final int METHOD_getCellEditor47 = 47;
    private static final int METHOD_getCellRect48 = 48;
    private static final int METHOD_getCellRenderer49 = 49;
    private static final int METHOD_getClientProperty50 = 50;
    private static final int METHOD_getColumn51 = 51;
    private static final int METHOD_getComponentAt52 = 52;
    private static final int METHOD_getConditionForKeyStroke53 = 53;
    private static final int METHOD_getDefaultEditor54 = 54;
    private static final int METHOD_getDefaultLocale55 = 55;
    private static final int METHOD_getDefaultRenderer56 = 56;
    private static final int METHOD_getFontMetrics57 = 57;
    private static final int METHOD_getInputMap58 = 58;
    private static final int METHOD_getInsets59 = 59;
    private static final int METHOD_getListeners60 = 60;
    private static final int METHOD_getLocation61 = 61;
    private static final int METHOD_getPropertyChangeListeners62 = 62;
    private static final int METHOD_getRowHeight63 = 63;
    private static final int METHOD_getScrollableBlockIncrement64 = 64;
    private static final int METHOD_getScrollableUnitIncrement65 = 65;
    private static final int METHOD_getSize66 = 66;
    private static final int METHOD_getToolTipLocation67 = 67;
    private static final int METHOD_getToolTipText68 = 68;
    private static final int METHOD_getValueAt69 = 69;
    private static final int METHOD_gotFocus70 = 70;
    private static final int METHOD_grabFocus71 = 71;
    private static final int METHOD_handleEvent72 = 72;
    private static final int METHOD_hasFocus73 = 73;
    private static final int METHOD_hide74 = 74;
    private static final int METHOD_imageUpdate75 = 75;
    private static final int METHOD_indexAt76 = 76;
    private static final int METHOD_insets77 = 77;
    private static final int METHOD_inside78 = 78;
    private static final int METHOD_invalidate79 = 79;
    private static final int METHOD_isAncestorOf80 = 80;
    private static final int METHOD_isCellEditable81 = 81;
    private static final int METHOD_isCellSelected82 = 82;
    private static final int METHOD_isColumnSelected83 = 83;
    private static final int METHOD_isFocusCycleRoot84 = 84;
    private static final int METHOD_isLightweightComponent85 = 85;
    private static final int METHOD_isRowSelected86 = 86;
    private static final int METHOD_keyDown87 = 87;
    private static final int METHOD_keyUp88 = 88;
    private static final int METHOD_layout89 = 89;
    private static final int METHOD_list90 = 90;
    private static final int METHOD_locate91 = 91;
    private static final int METHOD_location92 = 92;
    private static final int METHOD_lostFocus93 = 93;
    private static final int METHOD_mergeColumn94 = 94;
    private static final int METHOD_minimumSize95 = 95;
    private static final int METHOD_mouseDown96 = 96;
    private static final int METHOD_mouseDrag97 = 97;
    private static final int METHOD_mouseEnter98 = 98;
    private static final int METHOD_mouseExit99 = 99;
    private static final int METHOD_mouseMove100 = 100;
    private static final int METHOD_mouseUp101 = 101;
    private static final int METHOD_move102 = 102;
    private static final int METHOD_moveColumn103 = 103;
    private static final int METHOD_nextFocus104 = 104;
    private static final int METHOD_paint105 = 105;
    private static final int METHOD_paintAll106 = 106;
    private static final int METHOD_paintComponents107 = 107;
    private static final int METHOD_paintImmediately108 = 108;
    private static final int METHOD_postEvent109 = 109;
    private static final int METHOD_preferredSize110 = 110;
    private static final int METHOD_prepareEditor111 = 111;
    private static final int METHOD_prepareImage112 = 112;
    private static final int METHOD_prepareRenderer113 = 113;
    private static final int METHOD_print114 = 114;
    private static final int METHOD_printAll115 = 115;
    private static final int METHOD_printComponents116 = 116;
    private static final int METHOD_putClientProperty117 = 117;
    private static final int METHOD_registerKeyboardAction118 = 118;
    private static final int METHOD_remove119 = 119;
    private static final int METHOD_removeAll120 = 120;
    private static final int METHOD_removeCell121 = 121;
    private static final int METHOD_removeColumn122 = 122;
    private static final int METHOD_removeColumnSelectionInterval123 = 123;
    private static final int METHOD_removeEditor124 = 124;
    private static final int METHOD_removeNotify125 = 125;
    private static final int METHOD_removePropertyChangeListener126 = 126;
    private static final int METHOD_removeRowSelectionInterval127 = 127;
    private static final int METHOD_renderers128 = 128;
    private static final int METHOD_repaint129 = 129;
    private static final int METHOD_requestDefaultFocus130 = 130;
    private static final int METHOD_requestFocus131 = 131;
    private static final int METHOD_requestFocusInWindow132 = 132;
    private static final int METHOD_resetCells133 = 133;
    private static final int METHOD_resetKeyboardActions134 = 134;
    private static final int METHOD_reshape135 = 135;
    private static final int METHOD_resize136 = 136;
    private static final int METHOD_revalidate137 = 137;
    private static final int METHOD_rowAtPoint138 = 138;
    private static final int METHOD_scrollRectToVisible139 = 139;
    private static final int METHOD_scrollTo140 = 140;
    private static final int METHOD_selectAll141 = 141;
    private static final int METHOD_setBounds142 = 142;
    private static final int METHOD_setDefaultEditor143 = 143;
    private static final int METHOD_setDefaultLocale144 = 144;
    private static final int METHOD_setDefaultRenderer145 = 145;
    private static final int METHOD_setInputMap146 = 146;
    private static final int METHOD_setLocation147 = 147;
    private static final int METHOD_setPVEditors148 = 148;
    private static final int METHOD_setRowHeight149 = 149;
    private static final int METHOD_setSize150 = 150;
    private static final int METHOD_setValueAt151 = 151;
    private static final int METHOD_show152 = 152;
    private static final int METHOD_size153 = 153;
    private static final int METHOD_sizeColumnsToFit154 = 154;
    private static final int METHOD_tableChanged155 = 155;
    private static final int METHOD_toString156 = 156;
    private static final int METHOD_transferFocus157 = 157;
    private static final int METHOD_transferFocusBackward158 = 158;
    private static final int METHOD_transferFocusDownCycle159 = 159;
    private static final int METHOD_transferFocusUpCycle160 = 160;
    private static final int METHOD_unregisterKeyboardAction161 = 161;
    private static final int METHOD_update162 = 162;
    private static final int METHOD_updateSortIcons163 = 163;
    private static final int METHOD_updateUI164 = 164;
    private static final int METHOD_validate165 = 165;
    private static final int METHOD_valueChanged166 = 166;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[167];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addCell2] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addCell", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_addCell2].setDisplayName ( "" );
            methods[METHOD_addColumn3] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addColumn", new Class[] {javax.swing.table.TableColumn.class}));
            methods[METHOD_addColumn3].setDisplayName ( "" );
            methods[METHOD_addColumnSelectionInterval4] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addColumnSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_addColumnSelectionInterval4].setDisplayName ( "" );
            methods[METHOD_addNotify5] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify5].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener6] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener6].setDisplayName ( "" );
            methods[METHOD_addRowSelectionInterval7] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("addRowSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_addRowSelectionInterval7].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation8] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation8].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet9] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet9].setDisplayName ( "" );
            methods[METHOD_bounds10] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds10].setDisplayName ( "" );
            methods[METHOD_cellAt11] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("cellAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_cellAt11].setDisplayName ( "" );
            methods[METHOD_changeSelection12] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("changeSelection", new Class[] {Integer.TYPE, Integer.TYPE, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_changeSelection12].setDisplayName ( "" );
            methods[METHOD_checkImage13] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage13].setDisplayName ( "" );
            methods[METHOD_clearSelection14] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("clearSelection", new Class[] {}));
            methods[METHOD_clearSelection14].setDisplayName ( "" );
            methods[METHOD_columnAdded15] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnAdded", new Class[] {javax.swing.event.TableColumnModelEvent.class}));
            methods[METHOD_columnAdded15].setDisplayName ( "" );
            methods[METHOD_columnAtPoint16] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnAtPoint", new Class[] {java.awt.Point.class}));
            methods[METHOD_columnAtPoint16].setDisplayName ( "" );
            methods[METHOD_columnMarginChanged17] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnMarginChanged", new Class[] {javax.swing.event.ChangeEvent.class}));
            methods[METHOD_columnMarginChanged17].setDisplayName ( "" );
            methods[METHOD_columnMoved18] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnMoved", new Class[] {javax.swing.event.TableColumnModelEvent.class}));
            methods[METHOD_columnMoved18].setDisplayName ( "" );
            methods[METHOD_columnRemoved19] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnRemoved", new Class[] {javax.swing.event.TableColumnModelEvent.class}));
            methods[METHOD_columnRemoved19].setDisplayName ( "" );
            methods[METHOD_columnSelectionChanged20] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("columnSelectionChanged", new Class[] {javax.swing.event.ListSelectionEvent.class}));
            methods[METHOD_columnSelectionChanged20].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect21] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect21].setDisplayName ( "" );
            methods[METHOD_contains22] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains22].setDisplayName ( "" );
            methods[METHOD_convertColumnIndexToModel23] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("convertColumnIndexToModel", new Class[] {Integer.TYPE}));
            methods[METHOD_convertColumnIndexToModel23].setDisplayName ( "" );
            methods[METHOD_convertColumnIndexToView24] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("convertColumnIndexToView", new Class[] {Integer.TYPE}));
            methods[METHOD_convertColumnIndexToView24].setDisplayName ( "" );
            methods[METHOD_countComponents25] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents25].setDisplayName ( "" );
            methods[METHOD_createDefaultColumnsFromModel26] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createDefaultColumnsFromModel", new Class[] {}));
            methods[METHOD_createDefaultColumnsFromModel26].setDisplayName ( "" );
            methods[METHOD_createHeadRenderers27] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createHeadRenderers", new Class[] {}));
            methods[METHOD_createHeadRenderers27].setDisplayName ( "" );
            methods[METHOD_createImage28] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage28].setDisplayName ( "" );
            methods[METHOD_createScrollPaneForTable29] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createScrollPaneForTable", new Class[] {javax.swing.JTable.class}));
            methods[METHOD_createScrollPaneForTable29].setDisplayName ( "" );
            methods[METHOD_createToolTip30] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip30].setDisplayName ( "" );
            methods[METHOD_createVolatileImage31] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage31].setDisplayName ( "" );
            methods[METHOD_deliverEvent32] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent32].setDisplayName ( "" );
            methods[METHOD_disable33] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable33].setDisplayName ( "" );
            methods[METHOD_dispatchEvent34] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent34].setDisplayName ( "" );
            methods[METHOD_doLayout35] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout35].setDisplayName ( "" );
            methods[METHOD_editCellAt36] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("editCellAt", new Class[] {Integer.TYPE, Integer.TYPE, java.util.EventObject.class}));
            methods[METHOD_editCellAt36].setDisplayName ( "" );
            methods[METHOD_editingCanceled37] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("editingCanceled", new Class[] {javax.swing.event.ChangeEvent.class}));
            methods[METHOD_editingCanceled37].setDisplayName ( "" );
            methods[METHOD_editingStopped38] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("editingStopped", new Class[] {javax.swing.event.ChangeEvent.class}));
            methods[METHOD_editingStopped38].setDisplayName ( "" );
            methods[METHOD_editorListener39] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("editorListener", new Class[] {pv.jfcx.PVTableEditorListener.class, Boolean.TYPE}));
            methods[METHOD_editorListener39].setDisplayName ( "" );
            methods[METHOD_editors40] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("editors", new Class[] {}));
            methods[METHOD_editors40].setDisplayName ( "" );
            methods[METHOD_enable41] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable41].setDisplayName ( "" );
            methods[METHOD_enableInputMethods42] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods42].setDisplayName ( "" );
            methods[METHOD_findComponentAt43] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt43].setDisplayName ( "" );
            methods[METHOD_firePropertyChange44] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange44].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke45] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke45].setDisplayName ( "" );
            methods[METHOD_getBounds46] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds46].setDisplayName ( "" );
            methods[METHOD_getCellEditor47] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getCellEditor", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getCellEditor47].setDisplayName ( "" );
            methods[METHOD_getCellRect48] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getCellRect", new Class[] {Integer.TYPE, Integer.TYPE, Boolean.TYPE}));
            methods[METHOD_getCellRect48].setDisplayName ( "" );
            methods[METHOD_getCellRenderer49] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getCellRenderer", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getCellRenderer49].setDisplayName ( "" );
            methods[METHOD_getClientProperty50] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty50].setDisplayName ( "" );
            methods[METHOD_getColumn51] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getColumn", new Class[] {java.lang.Object.class}));
            methods[METHOD_getColumn51].setDisplayName ( "" );
            methods[METHOD_getComponentAt52] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt52].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke53] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke53].setDisplayName ( "" );
            methods[METHOD_getDefaultEditor54] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getDefaultEditor", new Class[] {java.lang.Class.class}));
            methods[METHOD_getDefaultEditor54].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale55] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale55].setDisplayName ( "" );
            methods[METHOD_getDefaultRenderer56] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getDefaultRenderer", new Class[] {java.lang.Class.class}));
            methods[METHOD_getDefaultRenderer56].setDisplayName ( "" );
            methods[METHOD_getFontMetrics57] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics57].setDisplayName ( "" );
            methods[METHOD_getInputMap58] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap58].setDisplayName ( "" );
            methods[METHOD_getInsets59] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets59].setDisplayName ( "" );
            methods[METHOD_getListeners60] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners60].setDisplayName ( "" );
            methods[METHOD_getLocation61] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation61].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners62] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners62].setDisplayName ( "" );
            methods[METHOD_getRowHeight63] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getRowHeight", new Class[] {}));
            methods[METHOD_getRowHeight63].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement64] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableBlockIncrement64].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement65] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getScrollableUnitIncrement65].setDisplayName ( "" );
            methods[METHOD_getSize66] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize66].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation67] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation67].setDisplayName ( "" );
            methods[METHOD_getToolTipText68] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText68].setDisplayName ( "" );
            methods[METHOD_getValueAt69] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("getValueAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getValueAt69].setDisplayName ( "" );
            methods[METHOD_gotFocus70] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus70].setDisplayName ( "" );
            methods[METHOD_grabFocus71] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus71].setDisplayName ( "" );
            methods[METHOD_handleEvent72] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent72].setDisplayName ( "" );
            methods[METHOD_hasFocus73] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus73].setDisplayName ( "" );
            methods[METHOD_hide74] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide74].setDisplayName ( "" );
            methods[METHOD_imageUpdate75] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate75].setDisplayName ( "" );
            methods[METHOD_indexAt76] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("indexAt", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_indexAt76].setDisplayName ( "" );
            methods[METHOD_insets77] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets77].setDisplayName ( "" );
            methods[METHOD_inside78] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("inside", new Class[] {java.awt.Insets.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside78].setDisplayName ( "" );
            methods[METHOD_invalidate79] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate79].setDisplayName ( "" );
            methods[METHOD_isAncestorOf80] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf80].setDisplayName ( "" );
            methods[METHOD_isCellEditable81] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isCellEditable", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_isCellEditable81].setDisplayName ( "" );
            methods[METHOD_isCellSelected82] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isCellSelected", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_isCellSelected82].setDisplayName ( "" );
            methods[METHOD_isColumnSelected83] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isColumnSelected", new Class[] {Integer.TYPE}));
            methods[METHOD_isColumnSelected83].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot84] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot84].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent85] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent85].setDisplayName ( "" );
            methods[METHOD_isRowSelected86] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("isRowSelected", new Class[] {Integer.TYPE}));
            methods[METHOD_isRowSelected86].setDisplayName ( "" );
            methods[METHOD_keyDown87] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown87].setDisplayName ( "" );
            methods[METHOD_keyUp88] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp88].setDisplayName ( "" );
            methods[METHOD_layout89] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout89].setDisplayName ( "" );
            methods[METHOD_list90] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list90].setDisplayName ( "" );
            methods[METHOD_locate91] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate91].setDisplayName ( "" );
            methods[METHOD_location92] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("location", new Class[] {}));
            methods[METHOD_location92].setDisplayName ( "" );
            methods[METHOD_lostFocus93] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus93].setDisplayName ( "" );
            methods[METHOD_mergeColumn94] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mergeColumn", new Class[] {Integer.TYPE}));
            methods[METHOD_mergeColumn94].setDisplayName ( "" );
            methods[METHOD_minimumSize95] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize95].setDisplayName ( "" );
            methods[METHOD_mouseDown96] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown96].setDisplayName ( "" );
            methods[METHOD_mouseDrag97] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag97].setDisplayName ( "" );
            methods[METHOD_mouseEnter98] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter98].setDisplayName ( "" );
            methods[METHOD_mouseExit99] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit99].setDisplayName ( "" );
            methods[METHOD_mouseMove100] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove100].setDisplayName ( "" );
            methods[METHOD_mouseUp101] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp101].setDisplayName ( "" );
            methods[METHOD_move102] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move102].setDisplayName ( "" );
            methods[METHOD_moveColumn103] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("moveColumn", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_moveColumn103].setDisplayName ( "" );
            methods[METHOD_nextFocus104] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus104].setDisplayName ( "" );
            methods[METHOD_paint105] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint105].setDisplayName ( "" );
            methods[METHOD_paintAll106] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll106].setDisplayName ( "" );
            methods[METHOD_paintComponents107] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents107].setDisplayName ( "" );
            methods[METHOD_paintImmediately108] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately108].setDisplayName ( "" );
            methods[METHOD_postEvent109] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent109].setDisplayName ( "" );
            methods[METHOD_preferredSize110] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize110].setDisplayName ( "" );
            methods[METHOD_prepareEditor111] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("prepareEditor", new Class[] {javax.swing.table.TableCellEditor.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_prepareEditor111].setDisplayName ( "" );
            methods[METHOD_prepareImage112] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage112].setDisplayName ( "" );
            methods[METHOD_prepareRenderer113] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("prepareRenderer", new Class[] {javax.swing.table.TableCellRenderer.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_prepareRenderer113].setDisplayName ( "" );
            methods[METHOD_print114] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("print", new Class[] {java.awt.Graphics.class, java.awt.print.PageFormat.class, Integer.TYPE}));
            methods[METHOD_print114].setDisplayName ( "" );
            methods[METHOD_printAll115] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll115].setDisplayName ( "" );
            methods[METHOD_printComponents116] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents116].setDisplayName ( "" );
            methods[METHOD_putClientProperty117] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty117].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction118] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction118].setDisplayName ( "" );
            methods[METHOD_remove119] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove119].setDisplayName ( "" );
            methods[METHOD_removeAll120] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll120].setDisplayName ( "" );
            methods[METHOD_removeCell121] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeCell", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_removeCell121].setDisplayName ( "" );
            methods[METHOD_removeColumn122] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeColumn", new Class[] {javax.swing.table.TableColumn.class}));
            methods[METHOD_removeColumn122].setDisplayName ( "" );
            methods[METHOD_removeColumnSelectionInterval123] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeColumnSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_removeColumnSelectionInterval123].setDisplayName ( "" );
            methods[METHOD_removeEditor124] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeEditor", new Class[] {}));
            methods[METHOD_removeEditor124].setDisplayName ( "" );
            methods[METHOD_removeNotify125] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify125].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener126] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener126].setDisplayName ( "" );
            methods[METHOD_removeRowSelectionInterval127] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("removeRowSelectionInterval", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_removeRowSelectionInterval127].setDisplayName ( "" );
            methods[METHOD_renderers128] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("renderers", new Class[] {}));
            methods[METHOD_renderers128].setDisplayName ( "" );
            methods[METHOD_repaint129] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("repaint", new Class[] {Boolean.TYPE}));
            methods[METHOD_repaint129].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus130] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus130].setDisplayName ( "" );
            methods[METHOD_requestFocus131] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus131].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow132] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow132].setDisplayName ( "" );
            methods[METHOD_resetCells133] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("resetCells", new Class[] {}));
            methods[METHOD_resetCells133].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions134] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions134].setDisplayName ( "" );
            methods[METHOD_reshape135] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape135].setDisplayName ( "" );
            methods[METHOD_resize136] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize136].setDisplayName ( "" );
            methods[METHOD_revalidate137] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate137].setDisplayName ( "" );
            methods[METHOD_rowAtPoint138] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("rowAtPoint", new Class[] {java.awt.Point.class}));
            methods[METHOD_rowAtPoint138].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible139] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible139].setDisplayName ( "" );
            methods[METHOD_scrollTo140] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("scrollTo", new Class[] {Integer.TYPE, Integer.TYPE, Boolean.TYPE}));
            methods[METHOD_scrollTo140].setDisplayName ( "" );
            methods[METHOD_selectAll141] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("selectAll", new Class[] {}));
            methods[METHOD_selectAll141].setDisplayName ( "" );
            methods[METHOD_setBounds142] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds142].setDisplayName ( "" );
            methods[METHOD_setDefaultEditor143] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setDefaultEditor", new Class[] {java.lang.Class.class, javax.swing.table.TableCellEditor.class}));
            methods[METHOD_setDefaultEditor143].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale144] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale144].setDisplayName ( "" );
            methods[METHOD_setDefaultRenderer145] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setDefaultRenderer", new Class[] {java.lang.Class.class, javax.swing.table.TableCellRenderer.class}));
            methods[METHOD_setDefaultRenderer145].setDisplayName ( "" );
            methods[METHOD_setInputMap146] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap146].setDisplayName ( "" );
            methods[METHOD_setLocation147] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation147].setDisplayName ( "" );
            methods[METHOD_setPVEditors148] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setPVEditors", new Class[] {Boolean.TYPE}));
            methods[METHOD_setPVEditors148].setDisplayName ( "" );
            methods[METHOD_setRowHeight149] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setRowHeight", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setRowHeight149].setDisplayName ( "" );
            methods[METHOD_setSize150] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize150].setDisplayName ( "" );
            methods[METHOD_setValueAt151] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("setValueAt", new Class[] {java.lang.Object.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setValueAt151].setDisplayName ( "" );
            methods[METHOD_show152] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show152].setDisplayName ( "" );
            methods[METHOD_size153] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("size", new Class[] {}));
            methods[METHOD_size153].setDisplayName ( "" );
            methods[METHOD_sizeColumnsToFit154] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("sizeColumnsToFit", new Class[] {Integer.TYPE}));
            methods[METHOD_sizeColumnsToFit154].setDisplayName ( "" );
            methods[METHOD_tableChanged155] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("tableChanged", new Class[] {javax.swing.event.TableModelEvent.class}));
            methods[METHOD_tableChanged155].setDisplayName ( "" );
            methods[METHOD_toString156] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString156].setDisplayName ( "" );
            methods[METHOD_transferFocus157] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus157].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward158] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward158].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle159] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle159].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle160] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle160].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction161] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction161].setDisplayName ( "" );
            methods[METHOD_update162] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update162].setDisplayName ( "" );
            methods[METHOD_updateSortIcons163] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("updateSortIcons", new Class[] {}));
            methods[METHOD_updateSortIcons163].setDisplayName ( "" );
            methods[METHOD_updateUI164] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI164].setDisplayName ( "" );
            methods[METHOD_validate165] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate165].setDisplayName ( "" );
            methods[METHOD_valueChanged166] = new MethodDescriptor ( bsc.beans.JBIXMLTable.class.getMethod("valueChanged", new Class[] {javax.swing.event.ListSelectionEvent.class}));
            methods[METHOD_valueChanged166].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
	
	// Here you can add code for customizing the methods array.
	
        return methods;         }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
 //GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
 //GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
	return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
	return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
	return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
	return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
	return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
	return defaultEventIndex;
    }
}

