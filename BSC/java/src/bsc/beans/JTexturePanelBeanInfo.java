package bsc.beans;

import java.beans.*;

public class JTexturePanelBeanInfo extends SimpleBeanInfo {
	

    // Bean descriptor //GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/;
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JTexturePanel.class , null );//GEN-HEADEREND:BeanDescriptor
		
		// Here you can add code for customizing the BeanDescriptor.
		
        return beanDescriptor;         }//GEN-LAST:BeanDescriptor
	
	
    // Property identifiers //GEN-FIRST:Properties
    private static final int PROPERTY_verifyInputWhenFocusTarget = 0;
    private static final int PROPERTY_componentOrientation = 1;
    private static final int PROPERTY_fontSet = 2;
    private static final int PROPERTY_locationOnScreen = 3;
    private static final int PROPERTY_mouseWheelListeners = 4;
    private static final int PROPERTY_colorModel = 5;
    private static final int PROPERTY_focusTraversalPolicy = 6;
    private static final int PROPERTY_registeredKeyStrokes = 7;
    private static final int PROPERTY_alignmentX = 8;
    private static final int PROPERTY_paintingTile = 9;
    private static final int PROPERTY_alignmentY = 10;
    private static final int PROPERTY_hierarchyListeners = 11;
    private static final int PROPERTY_accessibleContext = 12;
    private static final int PROPERTY_preferredSize = 13;
    private static final int PROPERTY_managingFocus = 14;
    private static final int PROPERTY_minimumSizeSet = 15;
    private static final int PROPERTY_focusTraversalPolicySet = 16;
    private static final int PROPERTY_y = 17;
    private static final int PROPERTY_x = 18;
    private static final int PROPERTY_cursorSet = 19;
    private static final int PROPERTY_inputMethodRequests = 20;
    private static final int PROPERTY_containerListeners = 21;
    private static final int PROPERTY_insets = 22;
    private static final int PROPERTY_componentCount = 23;
    private static final int PROPERTY_components = 24;
    private static final int PROPERTY_inputVerifier = 25;
    private static final int PROPERTY_hierarchyBoundsListeners = 26;
    private static final int PROPERTY_border = 27;
    private static final int PROPERTY_name = 28;
    private static final int PROPERTY_optimizedDrawingEnabled = 29;
    private static final int PROPERTY_graphics = 30;
    private static final int PROPERTY_minimumSize = 31;
    private static final int PROPERTY_toolTipText = 32;
    private static final int PROPERTY_focusTraversalKeysEnabled = 33;
    private static final int PROPERTY_foreground = 34;
    private static final int PROPERTY_ignoreRepaint = 35;
    private static final int PROPERTY_focusable = 36;
    private static final int PROPERTY_preferredSizeSet = 37;
    private static final int PROPERTY_visible = 38;
    private static final int PROPERTY_focusCycleRootAncestor = 39;
    private static final int PROPERTY_parent = 40;
    private static final int PROPERTY_rootPane = 41;
    private static final int PROPERTY_lightweight = 42;
    private static final int PROPERTY_width = 43;
    private static final int PROPERTY_keyListeners = 44;
    private static final int PROPERTY_toolkit = 45;
    private static final int PROPERTY_inputContext = 46;
    private static final int PROPERTY_layout = 47;
    private static final int PROPERTY_opaque = 48;
    private static final int PROPERTY_font = 49;
    private static final int PROPERTY_locale = 50;
    private static final int PROPERTY_cursor = 51;
    private static final int PROPERTY_inputMethodListeners = 52;
    private static final int PROPERTY_transferHandler = 53;
    private static final int PROPERTY_vetoableChangeListeners = 54;
    private static final int PROPERTY_doubleBuffered = 55;
    private static final int PROPERTY_visibleRect = 56;
    private static final int PROPERTY_maximumSizeSet = 57;
    private static final int PROPERTY_valid = 58;
    private static final int PROPERTY_focusCycleRoot = 59;
    private static final int PROPERTY_maximumSize = 60;
    private static final int PROPERTY_mouseMotionListeners = 61;
    private static final int PROPERTY_treeLock = 62;
    private static final int PROPERTY_bounds = 63;
    private static final int PROPERTY_focusTraversable = 64;
    private static final int PROPERTY_propertyChangeListeners = 65;
    private static final int PROPERTY_autoscrolls = 66;
    private static final int PROPERTY_componentListeners = 67;
    private static final int PROPERTY_showing = 68;
    private static final int PROPERTY_dropTarget = 69;
    private static final int PROPERTY_focusListeners = 70;
    private static final int PROPERTY_nextFocusableComponent = 71;
    private static final int PROPERTY_peer = 72;
    private static final int PROPERTY_height = 73;
    private static final int PROPERTY_topLevelAncestor = 74;
    private static final int PROPERTY_displayable = 75;
    private static final int PROPERTY_background = 76;
    private static final int PROPERTY_graphicsConfiguration = 77;
    private static final int PROPERTY_focusOwner = 78;
    private static final int PROPERTY_ancestorListeners = 79;
    private static final int PROPERTY_requestFocusEnabled = 80;
    private static final int PROPERTY_debugGraphicsOptions = 81;
    private static final int PROPERTY_backgroundSet = 82;
    private static final int PROPERTY_actionMap = 83;
    private static final int PROPERTY_mouseListeners = 84;
    private static final int PROPERTY_enabled = 85;
    private static final int PROPERTY_foregroundSet = 86;
    private static final int PROPERTY_imageTexture = 87;
    private static final int PROPERTY_validateRoot = 88;
    private static final int PROPERTY_UI = 89;
    private static final int PROPERTY_UIClassID = 90;
    private static final int PROPERTY_component = 91;
    private static final int PROPERTY_focusTraversalKeys = 92;

    // Property array 
    /*lazy PropertyDescriptor*/;
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[93];
    
        try {
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JTexturePanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" );
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JTexturePanel.class, "getComponentOrientation", "setComponentOrientation" );
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JTexturePanel.class, "isFontSet", null );
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JTexturePanel.class, "getLocationOnScreen", null );
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JTexturePanel.class, "getMouseWheelListeners", null );
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JTexturePanel.class, "getColorModel", null );
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JTexturePanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" );
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JTexturePanel.class, "getRegisteredKeyStrokes", null );
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JTexturePanel.class, "getAlignmentX", "setAlignmentX" );
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JTexturePanel.class, "isPaintingTile", null );
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JTexturePanel.class, "getAlignmentY", "setAlignmentY" );
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JTexturePanel.class, "getHierarchyListeners", null );
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JTexturePanel.class, "getAccessibleContext", null );
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JTexturePanel.class, "getPreferredSize", "setPreferredSize" );
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JTexturePanel.class, "isManagingFocus", null );
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JTexturePanel.class, "isMinimumSizeSet", null );
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JTexturePanel.class, "isFocusTraversalPolicySet", null );
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JTexturePanel.class, "getY", null );
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JTexturePanel.class, "getX", null );
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JTexturePanel.class, "isCursorSet", null );
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JTexturePanel.class, "getInputMethodRequests", null );
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JTexturePanel.class, "getContainerListeners", null );
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JTexturePanel.class, "getInsets", null );
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JTexturePanel.class, "getComponentCount", null );
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JTexturePanel.class, "getComponents", null );
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JTexturePanel.class, "getInputVerifier", "setInputVerifier" );
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JTexturePanel.class, "getHierarchyBoundsListeners", null );
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JTexturePanel.class, "getBorder", "setBorder" );
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JTexturePanel.class, "getName", "setName" );
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JTexturePanel.class, "isOptimizedDrawingEnabled", null );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JTexturePanel.class, "getGraphics", null );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JTexturePanel.class, "getMinimumSize", "setMinimumSize" );
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JTexturePanel.class, "getToolTipText", "setToolTipText" );
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JTexturePanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" );
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JTexturePanel.class, "getForeground", "setForeground" );
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JTexturePanel.class, "getIgnoreRepaint", "setIgnoreRepaint" );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JTexturePanel.class, "isFocusable", "setFocusable" );
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JTexturePanel.class, "isPreferredSizeSet", null );
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JTexturePanel.class, "isVisible", "setVisible" );
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JTexturePanel.class, "getFocusCycleRootAncestor", null );
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JTexturePanel.class, "getParent", null );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JTexturePanel.class, "getRootPane", null );
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JTexturePanel.class, "isLightweight", null );
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JTexturePanel.class, "getWidth", null );
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JTexturePanel.class, "getKeyListeners", null );
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JTexturePanel.class, "getToolkit", null );
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JTexturePanel.class, "getInputContext", null );
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JTexturePanel.class, "getLayout", "setLayout" );
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JTexturePanel.class, "isOpaque", "setOpaque" );
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JTexturePanel.class, "getFont", "setFont" );
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JTexturePanel.class, "getLocale", "setLocale" );
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JTexturePanel.class, "getCursor", "setCursor" );
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JTexturePanel.class, "getInputMethodListeners", null );
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JTexturePanel.class, "getTransferHandler", "setTransferHandler" );
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JTexturePanel.class, "getVetoableChangeListeners", null );
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JTexturePanel.class, "isDoubleBuffered", "setDoubleBuffered" );
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JTexturePanel.class, "getVisibleRect", null );
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JTexturePanel.class, "isMaximumSizeSet", null );
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JTexturePanel.class, "isValid", null );
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JTexturePanel.class, "isFocusCycleRoot", "setFocusCycleRoot" );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JTexturePanel.class, "getMaximumSize", "setMaximumSize" );
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JTexturePanel.class, "getMouseMotionListeners", null );
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JTexturePanel.class, "getTreeLock", null );
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JTexturePanel.class, "getBounds", "setBounds" );
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JTexturePanel.class, "isFocusTraversable", null );
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JTexturePanel.class, "getPropertyChangeListeners", null );
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JTexturePanel.class, "getAutoscrolls", "setAutoscrolls" );
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JTexturePanel.class, "getComponentListeners", null );
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JTexturePanel.class, "isShowing", null );
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JTexturePanel.class, "getDropTarget", "setDropTarget" );
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JTexturePanel.class, "getFocusListeners", null );
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JTexturePanel.class, "getNextFocusableComponent", "setNextFocusableComponent" );
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JTexturePanel.class, "getPeer", null );
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JTexturePanel.class, "getHeight", null );
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JTexturePanel.class, "getTopLevelAncestor", null );
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JTexturePanel.class, "isDisplayable", null );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JTexturePanel.class, "getBackground", "setBackground" );
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JTexturePanel.class, "getGraphicsConfiguration", null );
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JTexturePanel.class, "isFocusOwner", null );
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JTexturePanel.class, "getAncestorListeners", null );
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JTexturePanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" );
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JTexturePanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" );
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JTexturePanel.class, "isBackgroundSet", null );
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JTexturePanel.class, "getActionMap", "setActionMap" );
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JTexturePanel.class, "getMouseListeners", null );
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JTexturePanel.class, "isEnabled", "setEnabled" );
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JTexturePanel.class, "isForegroundSet", null );
            properties[PROPERTY_imageTexture] = new PropertyDescriptor ( "imageTexture", JTexturePanel.class, "getImageTexture", "setImageTexture" );
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JTexturePanel.class, "isValidateRoot", null );
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JTexturePanel.class, "getUI", "setUI" );
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JTexturePanel.class, "getUIClassID", null );
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JTexturePanel.class, null, null, "getComponent", null );
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JTexturePanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Properties
		
		// Here you can add code for customizing the properties array.
		
        return properties;         }//GEN-LAST:Properties
	
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_focusListener = 0;
    private static final int EVENT_keyListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_mouseWheelListener = 3;
    private static final int EVENT_hierarchyListener = 4;
    private static final int EVENT_ancestorListener = 5;
    private static final int EVENT_componentListener = 6;
    private static final int EVENT_propertyChangeListener = 7;
    private static final int EVENT_mouseMotionListener = 8;
    private static final int EVENT_mouseListener = 9;
    private static final int EVENT_inputMethodListener = 10;
    private static final int EVENT_hierarchyBoundsListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // EventSet array
    /*lazy EventSetDescriptor*/;
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
            try {
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusLost", "focusGained"}, "addFocusListener", "removeFocusListener" );
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" );
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" );
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" );
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" );
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorRemoved", "ancestorMoved", "ancestorAdded"}, "addAncestorListener", "removeAncestorListener" );
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentShown", "componentHidden", "componentMoved"}, "addComponentListener", "removeComponentListener" );
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" );
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" );
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseExited", "mousePressed", "mouseEntered", "mouseReleased"}, "addMouseListener", "removeMouseListener" );
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"inputMethodTextChanged", "caretPositionChanged"}, "addInputMethodListener", "removeInputMethodListener" );
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" );
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( bsc.beans.JTexturePanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" );
        }
        catch( IntrospectionException e) {}//GEN-HEADEREND:Events
		
		// Here you can add code for customizing the event sets array.
		
        return eventSets;         }//GEN-LAST:Events
	
    // Method identifiers //GEN-FIRST:Methods
    private static final int METHOD_paint0 = 0;
    private static final int METHOD_updateUI1 = 1;
    private static final int METHOD_getPropertyChangeListeners2 = 2;
    private static final int METHOD_scrollRectToVisible3 = 3;
    private static final int METHOD_getClientProperty4 = 4;
    private static final int METHOD_print5 = 5;
    private static final int METHOD_unregisterKeyboardAction6 = 6;
    private static final int METHOD_removePropertyChangeListener7 = 7;
    private static final int METHOD_getConditionForKeyStroke8 = 8;
    private static final int METHOD_requestFocus9 = 9;
    private static final int METHOD_paintImmediately10 = 10;
    private static final int METHOD_requestDefaultFocus11 = 11;
    private static final int METHOD_getToolTipText12 = 12;
    private static final int METHOD_requestFocusInWindow13 = 13;
    private static final int METHOD_repaint14 = 14;
    private static final int METHOD_getInputMap15 = 15;
    private static final int METHOD_getSize16 = 16;
    private static final int METHOD_requestFocus17 = 17;
    private static final int METHOD_firePropertyChange18 = 18;
    private static final int METHOD_isLightweightComponent19 = 19;
    private static final int METHOD_getInputMap20 = 20;
    private static final int METHOD_firePropertyChange21 = 21;
    private static final int METHOD_printAll22 = 22;
    private static final int METHOD_computeVisibleRect23 = 23;
    private static final int METHOD_getLocation24 = 24;
    private static final int METHOD_disable25 = 25;
    private static final int METHOD_paintImmediately26 = 26;
    private static final int METHOD_firePropertyChange27 = 27;
    private static final int METHOD_addNotify28 = 28;
    private static final int METHOD_getToolTipLocation29 = 29;
    private static final int METHOD_getListeners30 = 30;
    private static final int METHOD_firePropertyChange31 = 31;
    private static final int METHOD_reshape32 = 32;
    private static final int METHOD_grabFocus33 = 33;
    private static final int METHOD_getBounds34 = 34;
    private static final int METHOD_registerKeyboardAction35 = 35;
    private static final int METHOD_registerKeyboardAction36 = 36;
    private static final int METHOD_enable37 = 37;
    private static final int METHOD_addPropertyChangeListener38 = 38;
    private static final int METHOD_update39 = 39;
    private static final int METHOD_getActionForKeyStroke40 = 40;
    private static final int METHOD_removeNotify41 = 41;
    private static final int METHOD_firePropertyChange42 = 42;
    private static final int METHOD_firePropertyChange43 = 43;
    private static final int METHOD_getInsets44 = 44;
    private static final int METHOD_getDefaultLocale45 = 45;
    private static final int METHOD_putClientProperty46 = 46;
    private static final int METHOD_createToolTip47 = 47;
    private static final int METHOD_contains48 = 48;
    private static final int METHOD_revalidate49 = 49;
    private static final int METHOD_repaint50 = 50;
    private static final int METHOD_resetKeyboardActions51 = 51;
    private static final int METHOD_setDefaultLocale52 = 52;
    private static final int METHOD_firePropertyChange53 = 53;
    private static final int METHOD_firePropertyChange54 = 54;
    private static final int METHOD_setInputMap55 = 55;
    private static final int METHOD_applyComponentOrientation56 = 56;
    private static final int METHOD_add57 = 57;
    private static final int METHOD_validate58 = 58;
    private static final int METHOD_getComponentAt59 = 59;
    private static final int METHOD_removeAll60 = 60;
    private static final int METHOD_remove61 = 61;
    private static final int METHOD_preferredSize62 = 62;
    private static final int METHOD_remove63 = 63;
    private static final int METHOD_areFocusTraversalKeysSet64 = 64;
    private static final int METHOD_list65 = 65;
    private static final int METHOD_printComponents66 = 66;
    private static final int METHOD_paintComponents67 = 67;
    private static final int METHOD_insets68 = 68;
    private static final int METHOD_findComponentAt69 = 69;
    private static final int METHOD_deliverEvent70 = 70;
    private static final int METHOD_locate71 = 71;
    private static final int METHOD_getComponentAt72 = 72;
    private static final int METHOD_transferFocusBackward73 = 73;
    private static final int METHOD_minimumSize74 = 74;
    private static final int METHOD_add75 = 75;
    private static final int METHOD_add76 = 76;
    private static final int METHOD_add77 = 77;
    private static final int METHOD_layout78 = 78;
    private static final int METHOD_transferFocusDownCycle79 = 79;
    private static final int METHOD_isFocusCycleRoot80 = 80;
    private static final int METHOD_add81 = 81;
    private static final int METHOD_invalidate82 = 82;
    private static final int METHOD_list83 = 83;
    private static final int METHOD_countComponents84 = 84;
    private static final int METHOD_isAncestorOf85 = 85;
    private static final int METHOD_doLayout86 = 86;
    private static final int METHOD_findComponentAt87 = 87;
    private static final int METHOD_getFontMetrics88 = 88;
    private static final int METHOD_location89 = 89;
    private static final int METHOD_transferFocusUpCycle90 = 90;
    private static final int METHOD_postEvent91 = 91;
    private static final int METHOD_contains92 = 92;
    private static final int METHOD_gotFocus93 = 93;
    private static final int METHOD_setSize94 = 94;
    private static final int METHOD_list95 = 95;
    private static final int METHOD_hide96 = 96;
    private static final int METHOD_checkImage97 = 97;
    private static final int METHOD_checkImage98 = 98;
    private static final int METHOD_hasFocus99 = 99;
    private static final int METHOD_setLocation100 = 100;
    private static final int METHOD_mouseEnter101 = 101;
    private static final int METHOD_size102 = 102;
    private static final int METHOD_keyDown103 = 103;
    private static final int METHOD_move104 = 104;
    private static final int METHOD_inside105 = 105;
    private static final int METHOD_list106 = 106;
    private static final int METHOD_toString107 = 107;
    private static final int METHOD_keyUp108 = 108;
    private static final int METHOD_dispatchEvent109 = 109;
    private static final int METHOD_mouseDrag110 = 110;
    private static final int METHOD_setLocation111 = 111;
    private static final int METHOD_createImage112 = 112;
    private static final int METHOD_show113 = 113;
    private static final int METHOD_add114 = 114;
    private static final int METHOD_show115 = 115;
    private static final int METHOD_mouseUp116 = 116;
    private static final int METHOD_lostFocus117 = 117;
    private static final int METHOD_imageUpdate118 = 118;
    private static final int METHOD_resize119 = 119;
    private static final int METHOD_mouseDown120 = 120;
    private static final int METHOD_action121 = 121;
    private static final int METHOD_createVolatileImage122 = 122;
    private static final int METHOD_nextFocus123 = 123;
    private static final int METHOD_getLocation124 = 124;
    private static final int METHOD_paintAll125 = 125;
    private static final int METHOD_createVolatileImage126 = 126;
    private static final int METHOD_createImage127 = 127;
    private static final int METHOD_handleEvent128 = 128;
    private static final int METHOD_repaint129 = 129;
    private static final int METHOD_repaint130 = 130;
    private static final int METHOD_mouseExit131 = 131;
    private static final int METHOD_prepareImage132 = 132;
    private static final int METHOD_prepareImage133 = 133;
    private static final int METHOD_remove134 = 134;
    private static final int METHOD_setSize135 = 135;
    private static final int METHOD_transferFocus136 = 136;
    private static final int METHOD_enable137 = 137;
    private static final int METHOD_mouseMove138 = 138;
    private static final int METHOD_getSize139 = 139;
    private static final int METHOD_repaint140 = 140;
    private static final int METHOD_enableInputMethods141 = 141;
    private static final int METHOD_bounds142 = 142;
    private static final int METHOD_setBounds143 = 143;
    private static final int METHOD_resize144 = 144;
    private static final int METHOD_list145 = 145;

    // Method array 
    /*lazy MethodDescriptor*/;
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[146];
    
        try {
            methods[METHOD_paint0] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paint0].setDisplayName ( "" );
            methods[METHOD_updateUI1] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("updateUI", new Class[] {}));
            methods[METHOD_updateUI1].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners2] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class}));
            methods[METHOD_getPropertyChangeListeners2].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible3] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_scrollRectToVisible3].setDisplayName ( "" );
            methods[METHOD_getClientProperty4] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class}));
            methods[METHOD_getClientProperty4].setDisplayName ( "" );
            methods[METHOD_print5] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("print", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_print5].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction6] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_unregisterKeyboardAction6].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener7] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_removePropertyChangeListener7].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke8] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getConditionForKeyStroke8].setDisplayName ( "" );
            methods[METHOD_requestFocus9] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("requestFocus", new Class[] {}));
            methods[METHOD_requestFocus9].setDisplayName ( "" );
            methods[METHOD_paintImmediately10] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_paintImmediately10].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus11] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("requestDefaultFocus", new Class[] {}));
            methods[METHOD_requestDefaultFocus11].setDisplayName ( "" );
            methods[METHOD_getToolTipText12] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipText12].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow13] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("requestFocusInWindow", new Class[] {}));
            methods[METHOD_requestFocusInWindow13].setDisplayName ( "" );
            methods[METHOD_repaint14] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_repaint14].setDisplayName ( "" );
            methods[METHOD_getInputMap15] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getInputMap", new Class[] {}));
            methods[METHOD_getInputMap15].setDisplayName ( "" );
            methods[METHOD_getSize16] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_getSize16].setDisplayName ( "" );
            methods[METHOD_requestFocus17] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("requestFocus", new Class[] {Boolean.TYPE}));
            methods[METHOD_requestFocus17].setDisplayName ( "" );
            methods[METHOD_firePropertyChange18] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Byte.TYPE, Byte.TYPE}));
            methods[METHOD_firePropertyChange18].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent19] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class}));
            methods[METHOD_isLightweightComponent19].setDisplayName ( "" );
            methods[METHOD_getInputMap20] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getInputMap", new Class[] {Integer.TYPE}));
            methods[METHOD_getInputMap20].setDisplayName ( "" );
            methods[METHOD_firePropertyChange21] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Double.TYPE, Double.TYPE}));
            methods[METHOD_firePropertyChange21].setDisplayName ( "" );
            methods[METHOD_printAll22] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printAll22].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect23] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_computeVisibleRect23].setDisplayName ( "" );
            methods[METHOD_getLocation24] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_getLocation24].setDisplayName ( "" );
            methods[METHOD_disable25] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("disable", new Class[] {}));
            methods[METHOD_disable25].setDisplayName ( "" );
            methods[METHOD_paintImmediately26] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_paintImmediately26].setDisplayName ( "" );
            methods[METHOD_firePropertyChange27] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_firePropertyChange27].setDisplayName ( "" );
            methods[METHOD_addNotify28] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("addNotify", new Class[] {}));
            methods[METHOD_addNotify28].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation29] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class}));
            methods[METHOD_getToolTipLocation29].setDisplayName ( "" );
            methods[METHOD_getListeners30] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class}));
            methods[METHOD_getListeners30].setDisplayName ( "" );
            methods[METHOD_firePropertyChange31] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Character.TYPE, Character.TYPE}));
            methods[METHOD_firePropertyChange31].setDisplayName ( "" );
            methods[METHOD_reshape32] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_reshape32].setDisplayName ( "" );
            methods[METHOD_grabFocus33] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("grabFocus", new Class[] {}));
            methods[METHOD_grabFocus33].setDisplayName ( "" );
            methods[METHOD_getBounds34] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class}));
            methods[METHOD_getBounds34].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction35] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction35].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction36] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE}));
            methods[METHOD_registerKeyboardAction36].setDisplayName ( "" );
            methods[METHOD_enable37] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("enable", new Class[] {}));
            methods[METHOD_enable37].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener38] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class}));
            methods[METHOD_addPropertyChangeListener38].setDisplayName ( "" );
            methods[METHOD_update39] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("update", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_update39].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke40] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class}));
            methods[METHOD_getActionForKeyStroke40].setDisplayName ( "" );
            methods[METHOD_removeNotify41] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("removeNotify", new Class[] {}));
            methods[METHOD_removeNotify41].setDisplayName ( "" );
            methods[METHOD_firePropertyChange42] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Float.TYPE, Float.TYPE}));
            methods[METHOD_firePropertyChange42].setDisplayName ( "" );
            methods[METHOD_firePropertyChange43] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Short.TYPE, Short.TYPE}));
            methods[METHOD_firePropertyChange43].setDisplayName ( "" );
            methods[METHOD_getInsets44] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class}));
            methods[METHOD_getInsets44].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale45] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getDefaultLocale", new Class[] {}));
            methods[METHOD_getDefaultLocale45].setDisplayName ( "" );
            methods[METHOD_putClientProperty46] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class}));
            methods[METHOD_putClientProperty46].setDisplayName ( "" );
            methods[METHOD_createToolTip47] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("createToolTip", new Class[] {}));
            methods[METHOD_createToolTip47].setDisplayName ( "" );
            methods[METHOD_contains48] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_contains48].setDisplayName ( "" );
            methods[METHOD_revalidate49] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("revalidate", new Class[] {}));
            methods[METHOD_revalidate49].setDisplayName ( "" );
            methods[METHOD_repaint50] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint50].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions51] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("resetKeyboardActions", new Class[] {}));
            methods[METHOD_resetKeyboardActions51].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale52] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class}));
            methods[METHOD_setDefaultLocale52].setDisplayName ( "" );
            methods[METHOD_firePropertyChange53] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE}));
            methods[METHOD_firePropertyChange53].setDisplayName ( "" );
            methods[METHOD_firePropertyChange54] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Long.TYPE, Long.TYPE}));
            methods[METHOD_firePropertyChange54].setDisplayName ( "" );
            methods[METHOD_setInputMap55] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setInputMap", new Class[] {Integer.TYPE, javax.swing.InputMap.class}));
            methods[METHOD_setInputMap55].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation56] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class}));
            methods[METHOD_applyComponentOrientation56].setDisplayName ( "" );
            methods[METHOD_add57] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.awt.Component.class}));
            methods[METHOD_add57].setDisplayName ( "" );
            methods[METHOD_validate58] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("validate", new Class[] {}));
            methods[METHOD_validate58].setDisplayName ( "" );
            methods[METHOD_getComponentAt59] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_getComponentAt59].setDisplayName ( "" );
            methods[METHOD_removeAll60] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("removeAll", new Class[] {}));
            methods[METHOD_removeAll60].setDisplayName ( "" );
            methods[METHOD_remove61] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("remove", new Class[] {java.awt.Component.class}));
            methods[METHOD_remove61].setDisplayName ( "" );
            methods[METHOD_preferredSize62] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("preferredSize", new Class[] {}));
            methods[METHOD_preferredSize62].setDisplayName ( "" );
            methods[METHOD_remove63] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("remove", new Class[] {Integer.TYPE}));
            methods[METHOD_remove63].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet64] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE}));
            methods[METHOD_areFocusTraversalKeysSet64].setDisplayName ( "" );
            methods[METHOD_list65] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("list", new Class[] {java.io.PrintWriter.class, Integer.TYPE}));
            methods[METHOD_list65].setDisplayName ( "" );
            methods[METHOD_printComponents66] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_printComponents66].setDisplayName ( "" );
            methods[METHOD_paintComponents67] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintComponents67].setDisplayName ( "" );
            methods[METHOD_insets68] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("insets", new Class[] {}));
            methods[METHOD_insets68].setDisplayName ( "" );
            methods[METHOD_findComponentAt69] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class}));
            methods[METHOD_findComponentAt69].setDisplayName ( "" );
            methods[METHOD_deliverEvent70] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_deliverEvent70].setDisplayName ( "" );
            methods[METHOD_locate71] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_locate71].setDisplayName ( "" );
            methods[METHOD_getComponentAt72] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_getComponentAt72].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward73] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("transferFocusBackward", new Class[] {}));
            methods[METHOD_transferFocusBackward73].setDisplayName ( "" );
            methods[METHOD_minimumSize74] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("minimumSize", new Class[] {}));
            methods[METHOD_minimumSize74].setDisplayName ( "" );
            methods[METHOD_add75] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class}));
            methods[METHOD_add75].setDisplayName ( "" );
            methods[METHOD_add76] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.awt.Component.class, Integer.TYPE}));
            methods[METHOD_add76].setDisplayName ( "" );
            methods[METHOD_add77] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, Integer.TYPE}));
            methods[METHOD_add77].setDisplayName ( "" );
            methods[METHOD_layout78] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("layout", new Class[] {}));
            methods[METHOD_layout78].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle79] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("transferFocusDownCycle", new Class[] {}));
            methods[METHOD_transferFocusDownCycle79].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot80] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class}));
            methods[METHOD_isFocusCycleRoot80].setDisplayName ( "" );
            methods[METHOD_add81] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class}));
            methods[METHOD_add81].setDisplayName ( "" );
            methods[METHOD_invalidate82] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("invalidate", new Class[] {}));
            methods[METHOD_invalidate82].setDisplayName ( "" );
            methods[METHOD_list83] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE}));
            methods[METHOD_list83].setDisplayName ( "" );
            methods[METHOD_countComponents84] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("countComponents", new Class[] {}));
            methods[METHOD_countComponents84].setDisplayName ( "" );
            methods[METHOD_isAncestorOf85] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class}));
            methods[METHOD_isAncestorOf85].setDisplayName ( "" );
            methods[METHOD_doLayout86] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("doLayout", new Class[] {}));
            methods[METHOD_doLayout86].setDisplayName ( "" );
            methods[METHOD_findComponentAt87] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_findComponentAt87].setDisplayName ( "" );
            methods[METHOD_getFontMetrics88] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class}));
            methods[METHOD_getFontMetrics88].setDisplayName ( "" );
            methods[METHOD_location89] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("location", new Class[] {}));
            methods[METHOD_location89].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle90] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("transferFocusUpCycle", new Class[] {}));
            methods[METHOD_transferFocusUpCycle90].setDisplayName ( "" );
            methods[METHOD_postEvent91] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_postEvent91].setDisplayName ( "" );
            methods[METHOD_contains92] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("contains", new Class[] {java.awt.Point.class}));
            methods[METHOD_contains92].setDisplayName ( "" );
            methods[METHOD_gotFocus93] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_gotFocus93].setDisplayName ( "" );
            methods[METHOD_setSize94] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setSize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_setSize94].setDisplayName ( "" );
            methods[METHOD_list95] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("list", new Class[] {java.io.PrintStream.class}));
            methods[METHOD_list95].setDisplayName ( "" );
            methods[METHOD_hide96] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("hide", new Class[] {}));
            methods[METHOD_hide96].setDisplayName ( "" );
            methods[METHOD_checkImage97] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage97].setDisplayName ( "" );
            methods[METHOD_checkImage98] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_checkImage98].setDisplayName ( "" );
            methods[METHOD_hasFocus99] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("hasFocus", new Class[] {}));
            methods[METHOD_hasFocus99].setDisplayName ( "" );
            methods[METHOD_setLocation100] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setLocation", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setLocation100].setDisplayName ( "" );
            methods[METHOD_mouseEnter101] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseEnter101].setDisplayName ( "" );
            methods[METHOD_size102] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("size", new Class[] {}));
            methods[METHOD_size102].setDisplayName ( "" );
            methods[METHOD_keyDown103] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyDown103].setDisplayName ( "" );
            methods[METHOD_move104] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_move104].setDisplayName ( "" );
            methods[METHOD_inside105] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_inside105].setDisplayName ( "" );
            methods[METHOD_list106] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("list", new Class[] {java.io.PrintWriter.class}));
            methods[METHOD_list106].setDisplayName ( "" );
            methods[METHOD_toString107] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("toString", new Class[] {}));
            methods[METHOD_toString107].setDisplayName ( "" );
            methods[METHOD_keyUp108] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE}));
            methods[METHOD_keyUp108].setDisplayName ( "" );
            methods[METHOD_dispatchEvent109] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class}));
            methods[METHOD_dispatchEvent109].setDisplayName ( "" );
            methods[METHOD_mouseDrag110] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDrag110].setDisplayName ( "" );
            methods[METHOD_setLocation111] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setLocation", new Class[] {java.awt.Point.class}));
            methods[METHOD_setLocation111].setDisplayName ( "" );
            methods[METHOD_createImage112] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class}));
            methods[METHOD_createImage112].setDisplayName ( "" );
            methods[METHOD_show113] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("show", new Class[] {Boolean.TYPE}));
            methods[METHOD_show113].setDisplayName ( "" );
            methods[METHOD_add114] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("add", new Class[] {java.awt.PopupMenu.class}));
            methods[METHOD_add114].setDisplayName ( "" );
            methods[METHOD_show115] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("show", new Class[] {}));
            methods[METHOD_show115].setDisplayName ( "" );
            methods[METHOD_mouseUp116] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseUp116].setDisplayName ( "" );
            methods[METHOD_lostFocus117] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_lostFocus117].setDisplayName ( "" );
            methods[METHOD_imageUpdate118] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_imageUpdate118].setDisplayName ( "" );
            methods[METHOD_resize119] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("resize", new Class[] {java.awt.Dimension.class}));
            methods[METHOD_resize119].setDisplayName ( "" );
            methods[METHOD_mouseDown120] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseDown120].setDisplayName ( "" );
            methods[METHOD_action121] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class}));
            methods[METHOD_action121].setDisplayName ( "" );
            methods[METHOD_createVolatileImage122] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE, java.awt.ImageCapabilities.class}));
            methods[METHOD_createVolatileImage122].setDisplayName ( "" );
            methods[METHOD_nextFocus123] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("nextFocus", new Class[] {}));
            methods[METHOD_nextFocus123].setDisplayName ( "" );
            methods[METHOD_getLocation124] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getLocation", new Class[] {}));
            methods[METHOD_getLocation124].setDisplayName ( "" );
            methods[METHOD_paintAll125] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class}));
            methods[METHOD_paintAll125].setDisplayName ( "" );
            methods[METHOD_createVolatileImage126] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createVolatileImage126].setDisplayName ( "" );
            methods[METHOD_createImage127] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("createImage", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_createImage127].setDisplayName ( "" );
            methods[METHOD_handleEvent128] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class}));
            methods[METHOD_handleEvent128].setDisplayName ( "" );
            methods[METHOD_repaint129] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("repaint", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_repaint129].setDisplayName ( "" );
            methods[METHOD_repaint130] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("repaint", new Class[] {}));
            methods[METHOD_repaint130].setDisplayName ( "" );
            methods[METHOD_mouseExit131] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseExit131].setDisplayName ( "" );
            methods[METHOD_prepareImage132] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage132].setDisplayName ( "" );
            methods[METHOD_prepareImage133] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class}));
            methods[METHOD_prepareImage133].setDisplayName ( "" );
            methods[METHOD_remove134] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class}));
            methods[METHOD_remove134].setDisplayName ( "" );
            methods[METHOD_setSize135] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setSize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setSize135].setDisplayName ( "" );
            methods[METHOD_transferFocus136] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("transferFocus", new Class[] {}));
            methods[METHOD_transferFocus136].setDisplayName ( "" );
            methods[METHOD_enable137] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("enable", new Class[] {Boolean.TYPE}));
            methods[METHOD_enable137].setDisplayName ( "" );
            methods[METHOD_mouseMove138] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_mouseMove138].setDisplayName ( "" );
            methods[METHOD_getSize139] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("getSize", new Class[] {}));
            methods[METHOD_getSize139].setDisplayName ( "" );
            methods[METHOD_repaint140] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("repaint", new Class[] {Long.TYPE}));
            methods[METHOD_repaint140].setDisplayName ( "" );
            methods[METHOD_enableInputMethods141] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE}));
            methods[METHOD_enableInputMethods141].setDisplayName ( "" );
            methods[METHOD_bounds142] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("bounds", new Class[] {}));
            methods[METHOD_bounds142].setDisplayName ( "" );
            methods[METHOD_setBounds143] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE}));
            methods[METHOD_setBounds143].setDisplayName ( "" );
            methods[METHOD_resize144] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE}));
            methods[METHOD_resize144].setDisplayName ( "" );
            methods[METHOD_list145] = new MethodDescriptor ( bsc.beans.JTexturePanel.class.getMethod("list", new Class[] {}));
            methods[METHOD_list145].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
		
		// Here you can add code for customizing the methods array.
		
        return methods;         }//GEN-LAST:Methods
	
	
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
	
	
 //GEN-FIRST:Superclass
	
	// Here you can add code for customizing the Superclass BeanInfo.
	
 //GEN-LAST:Superclass
	
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable
	 * properties of this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 */
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}
	
	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean.  May return null if the
	 * information should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will
	 * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
	 * A client of getPropertyDescriptors can use "instanceof" to check
	 * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}
	
	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return  An array of EventSetDescriptors describing the kinds of
	 * events fired by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}
	
	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return  An array of MethodDescriptors describing the methods
	 * implemented by this bean.  May return null if the information
	 * should be obtained by automatic analysis.
	 */
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}
	
	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 * @return  Index of default property in the PropertyDescriptor array
	 * 		returned by getPropertyDescriptors.
	 * <P>	Returns -1 if there is no default property.
	 */
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}
	
	/**
	 * A bean may have a "default" event that is the event that will
	 * mostly commonly be used by human's when using the bean.
	 * @return Index of default event in the EventSetDescriptor array
	 *		returned by getEventSetDescriptors.
	 * <P>	Returns -1 if there is no default event.
	 */
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}
}

