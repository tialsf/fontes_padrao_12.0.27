/*
 * BIRequest.java
 *
 * Created on June 18, 2003, 3:56 PM
 */

package bsc.core;

import bsc.xml.*;

/**
 *
 * @author  siga1996
 */
public class BIRequest {
	
	BIXMLVector vector = new BIXMLVector("TRANSACOES", "TRANSACAO");
	BIXMLVector records = null;
	
	/** Creates a new instance of BIRequest */
	public BIRequest() {
		vector.addNewRecord().set("COMANDO", "NAODEFINIDO");
	}

	public void setCommand(String value) {
		vector.get(0).set("COMANDO", value );
	}

	public void setParameter(String paramName, String value) throws BIRequestException {
		if ( ! paramName.equals("COMANDO") )
			vector.get(0).set(paramName, value);
		else
			throw new BIRequestException( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIRequest_00001") );
	}
	
	public void setRecord(BIXMLRecord record) {
		if ( records == null)
			records = new BIXMLVector("REGISTROS", record.getTagName());
		records.add(record);
	}	
	
	public String writeXML() {
		if ( records != null )
			vector.get(0).set( records );
		return String.valueOf( vector.writeXML() );
	}
}
