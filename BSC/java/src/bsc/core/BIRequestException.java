/*
 * BIRequestException.java
 *
 * Created on June 20, 2003, 10:00 AM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BIRequestException extends java.lang.RuntimeException {
	
	/**
	 * Creates a new instance of <code>BIRequestException</code> without detail message.
	 */
	public BIRequestException() {
	}
	
	
	/**
	 * Constructs an instance of <code>BIRequestException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public BIRequestException(String msg) {
		super(msg);
	}
}
