/*
 * CommunicationController.java
 *
 * Created on June 25, 2003, 8:58 AM
 */

package bsc.core;

import bsc.net.*;
import bsc.xml.*;

/**
 *
 * @author  siga1996
 */
public class BscCommunicationController {
	
	private BIHttpClient httpClient;

	public BscCommunicationController() {
		httpClient = new BIHttpClient(getServer(), getSessionID());
	}
	
	public static String getSessionID() {
		if(bsc.core.BscStaticReferences.getBscExecutionType() == bsc.core.BscStaticReferences.BSC_APLICATION){
			return BscStaticReferences.getBscAplication().getSessionID();
		}else{
			return BscStaticReferences.getBscApplet().getSessionID();
		}
	}
	
	public static String getServer() {
		if(bsc.core.BscStaticReferences.getBscExecutionType() == bsc.core.BscStaticReferences.BSC_APLICATION){
			return BscStaticReferences.getBscAplication().getCodeBase()+ "bsccore.apw";
		}else{
			return BscStaticReferences.getBscApplet().getCodeBase() + "bsccore.apw";
		}
	}
	
	public static String getLoginServer() {
		if(bsc.core.BscStaticReferences.getBscExecutionType() == bsc.core.BscStaticReferences.BSC_APLICATION){
			return BscStaticReferences.getBscAplication().getCodeBase()+ "bsclogin.apw";
		}else{
			return BscStaticReferences.getBscApplet().getCodeBase() + "bsclogin.apw";
		}
	}

	public String getFile( String url ) {
		String resposta = new String();
		bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
							bsc.core.BscStaticReferences.getBscMainPanel().ST_CONNECTING );
		resposta = httpClient.doGet(url);
		bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
							bsc.core.BscStaticReferences.getBscMainPanel().ST_IDLE );
		return resposta;
	}			
	
	public String sendData ( String xml ) {
		String resposta = new String();
		bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
							bsc.core.BscStaticReferences.getBscMainPanel().ST_CONNECTING );
		resposta = httpClient.doPost(xml);
		bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
							bsc.core.BscStaticReferences.getBscMainPanel().ST_IDLE );
		return resposta;
	}	
	
	public String sendData ( String xml, boolean refreshStatusBar ) {
		String resposta = new String();
		if(refreshStatusBar){
			bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
								bsc.core.BscStaticReferences.getBscMainPanel().ST_CONNECTING );
		}
		
		resposta = httpClient.doPost(xml);
		
		if(refreshStatusBar){
			bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition( 
								bsc.core.BscStaticReferences.getBscMainPanel().ST_IDLE );
		}
		
		return resposta;
	}	
	
	public String sendLogin( String username, String password ) {
		String resposta = new String();
		resposta = httpClient.doLogin( getLoginServer(), username, password );
		return resposta;
	}	

	public String createSession(){
		String resposta = new String();
		
		if(bsc.core.BscStaticReferences.getBscExecutionType() == bsc.core.BscStaticReferences.BSC_APLICATION){
			resposta = httpClient.doPost(BscStaticReferences.getBscAplication().getCodeBase() + "session.apw","");
		}else{
			resposta = httpClient.doPost(BscStaticReferences.getBscApplet().getCodeBase() + "session.apw","");
		}
		
		BIXMLRecord record = null;
		BIResponse response = new BIResponse();
		response.processResponse( resposta );

		record = response.getReturnedRecord();
	
		httpClient.setSessionID(record.getString("SESSIONID"));
		
		return resposta;		
	}
	
	public BIHttpClient getHttpClient(){
		return httpClient;
	}
}