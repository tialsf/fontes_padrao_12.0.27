package bsc.core;

import bsc.xml.*;

public class BscDataController {

    static final public int BSC_ST_OK = 0;
    static final public int BSC_ST_BADID = 1;
    static final public int BSC_ST_INUSE = 2;
    static final public int BSC_ST_UNIQUE = 3;
    static final public int BSC_ST_NOCMD = 4;
    static final public int BSC_ST_NOTRANSACTION = 5;
    static final public int BSC_ST_BADXML = 6;
    static final public int BSC_ST_NORIGHTS = 7;
    static final public int BSC_ST_HASCHILD = 8;
    static final public int BSC_ST_EXPIREDSESSION = 9;
    static final public int BSC_ST_GENERALERROR = 10;
    static final public int BSC_CL_SECONDTASK = 999;
    private int operationStatus = 1000;
    private String errorMessage = new String();
    private BIXMLRecord tree = null;
    private boolean noServerMode = false;
    private BscServerSubstitute serverSubstitute = new BscServerSubstitute();
    private BscCommunicationController communicationController = new BscCommunicationController();
    private boolean inUse = false;

    public int getStatus() {
        return operationStatus;
    }

    public void setNoServeMode(boolean nsm) {
        noServerMode = nsm;
    }

    public boolean getNoServerMode() {
        return noServerMode;
    }

    public String getErrorMessage() {
        return String.valueOf(errorMessage);
    }

    public BIXMLRecord getNewTree() {
        return tree;
    }

    public synchronized BIXMLRecord loadTree(String type) {
        BIXMLRecord xmlTree = null;

        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("ARVORE");
        request.setParameter("TIPO", type);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            xmlTree = response.getReturnedRecord();
        }

        return xmlTree;
    }

    public synchronized BIXMLRecord loadTree(String type, String Id) {
        BIXMLRecord xmlTree = null;

        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("ARVORE");
        request.setParameter("TIPO", type);
        request.setParameter("ID", Id);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            xmlTree = response.getReturnedRecord();
        }

        return xmlTree;
    }

    public synchronized BIXMLRecord getRecordStructure(String type, String parentId) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        BIXMLRecord record = null;

        request.setCommand("CARREGAR");
        request.setParameter("TIPO", type);
        request.setParameter("PARENTID", parentId);
        request.setParameter("ID", "0");

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            record = response.getReturnedRecord();
        }

        return record;
    }

    public synchronized BIXMLRecord loadRecord(String id, String type) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        BIXMLRecord record = null;

        request.setCommand("CARREGAR");
        request.setParameter("TIPO", type);
        request.setParameter("ID", id);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            record = response.getReturnedRecord();
        }

        return record;
    }

    public synchronized BIXMLRecord loadRecord(String id, String type, String cLoadCmd) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        BIXMLRecord record = null;

        request.setCommand("CARREGAR");
        request.setParameter("TIPO", type);
        request.setParameter("ID", id);
        request.setParameter("LOADCMD", cLoadCmd);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            record = response.getReturnedRecord();
        }

        return record;
    }

    public synchronized void saveBase64(String fileName, String content) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        BIXMLRecord record = null;

        request.setCommand("SALVARBASE64");
        request.setParameter("FILENAME", fileName);
        request.setParameter("FILECONTENT", content);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();
    }

    public synchronized void deleteRecord(String id, String type, String delCMD) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("EXCLUIR");
        request.setParameter("TIPO", type);
        request.setParameter("ARVORETIPO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType());
        request.setParameter("ORGANIZACAO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeItemSelected());
        request.setParameter("ID", id);
        request.setParameter("DELCMD", delCMD);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();
        this.tree = response.getTree();
    }

    public synchronized void updateRecord(String type, BIXMLRecord record) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("ALTERAR");
        request.setParameter("TIPO", type);
        request.setParameter("ARVORETIPO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType());
        request.setParameter("ORGANIZACAO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeItemSelected());
        request.setRecord(record);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();
        this.tree = response.getTree();
    }

    public synchronized String insertRecord(String type, BIXMLRecord record) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        String id = null;

        request.setCommand("INCLUIR");
        request.setParameter("TIPO", type);
        request.setParameter("ARVORETIPO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType());
        request.setParameter("ORGANIZACAO", BscStaticReferences.getBscMainPanel().bscTree.getBscTreeItemSelected());
        request.setRecord(record);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();
        if (operationStatus == BSC_ST_OK) {
            id = new String(response.getID());
        } else {
            id = new String();
        }
        this.tree = response.getTree();

        return id;
    }

    public synchronized BIXMLRecord listServerFiles(String url) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();
        BIXMLRecord record = null;

        request.setCommand("LISTARARQUIVOS");
        request.setParameter("LOCAL", url);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            record = response.getReturnedRecord();
        }

        return record;
    }

    public synchronized String executeRecord(String id, String type, String execCMD) {
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("EXECUTAR");
        request.setParameter("TIPO", type);
        request.setParameter("ID", id);
        request.setParameter("EXECCMD", execCMD);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus != BSC_ST_OK) {
            id = "0";
        }

        return id;
    }

    public synchronized BIXMLRecord loadHelp(String type, String helpCMD) {
        BIXMLRecord record = null;
        BIRequest request = new BIRequest();
        BIResponse response = new BIResponse();

        request.setCommand("AJUDA");
        request.setParameter("TIPO", type);
        request.setParameter("HELPCMD", helpCMD);

        String answer = null;
        if (!noServerMode) {
            answer = communicationController.sendData(request.writeXML());
        } else {
            answer = serverSubstitute.sendData(request.writeXML());
        }

        response.processResponse(answer);
        operationStatus = response.getStatus();
        errorMessage = response.getErrorMessage();

        if (operationStatus == BSC_ST_OK) {
            record = response.getReturnedRecord();
        }

        return record;
    }

    public synchronized String doLogin(String username, String password) {
        String resposta = communicationController.sendLogin(username, password);
        return resposta;
    }

    /** Getter for property inUse.
     * @return Value of property inUse.
     *
     */
    public boolean isInUse() {
        return inUse;
    }

    /** Setter for property inUse.
     * @param inUse New value of property inUse.
     *
     */
    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public BscCommunicationController getBscCommunicationController() {
        return communicationController;
    }
}
