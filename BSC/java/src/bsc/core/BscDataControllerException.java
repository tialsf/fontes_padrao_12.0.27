/*
 * BscDataControllerException.java
 *
 * Created on June 3, 2003, 11:37 AM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BscDataControllerException extends java.lang.RuntimeException {
	
	/**
	 * Creates a new instance of <code>BscDataControllerException</code> without detail message.
	 */
	public BscDataControllerException() {
	}
	
	
	/**
	 * Constructs an instance of <code>BscDataControllerException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public BscDataControllerException(String msg) {
		super(msg);
	}
}
