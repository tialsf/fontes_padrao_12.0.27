/*
 * Controller.java
 *
 * Created on May 28, 2003, 8:49 AM
 */

package bsc.core;

import javax.swing.JDesktopPane;

/**
 *
 * @author  siga1996
 */
public class BscFormController implements bsc.swing.BscCloseTabbedPaneListener  {
	
	private bsc.applet.BscMainPanel mainPanel;
	private JDesktopPane desktop;
	private java.util.Hashtable forms = new java.util.Hashtable();
	private java.util.HashMap formClasses;
	private bsc.swing.BscCloseTabbedPane tabbedPanel;
	public static final int ST_PASTA = 0;
	public static final int ST_JANELA = 1;
	public int lastView = ST_JANELA;
	
	public BscFormController(bsc.applet.BscMainPanel frameAux, JDesktopPane dp) {
		
		mainPanel = frameAux;
		desktop = dp;
		
		// Classes de formularios do sistema
		formClasses = new java.util.HashMap();
		
		// framework
		formClasses.put("ORGANIZACOES",
			new String[] {"bsc.swing.framework.BscPrincipalFrame",		"ORGANIZACOES"});
		formClasses.put("ORGANIZACAO",
			new String[] {"bsc.swing.framework.BscOrganizacaoFrame",	"ORGANIZACOES"});
		formClasses.put("ESTRATEGIA",
			new String[] {"bsc.swing.framework.BscEstrategiaFrame",		"ORGANIZACAO"});
		formClasses.put("DESDOB",
			new String[] {"bsc.swing.framework.BscDesdobramentoFrame",	"ESTRATEGIA"});
		formClasses.put("PERSPECTIVA",
			new String[] {"bsc.swing.framework.BscPerspectivaFrame",	"ESTRATEGIA"});
		formClasses.put("OBJETIVO",
			new String[] {"bsc.swing.framework.BscObjetivoFrame",		"PERSPECTIVA"});
		formClasses.put("INDICADOR",
			new String[] {"bsc.swing.framework.BscIndicadorFrame",		"OBJETIVO"});
		formClasses.put("META",
			new String[] {"bsc.swing.framework.BscMetaFrame",		"INDICADOR"});
		formClasses.put("AVALIACAO",
			new String[] {"bsc.swing.framework.BscAvaliacaoFrame",		"AVALIACAO"});
		formClasses.put("PLANILHA",
			new String[] {"bsc.swing.framework.BscPlanilhaFrame",		"INDICADOR"});
		formClasses.put("RPLANILHA",
			new String[] {"bsc.swing.framework.BscRPlanilhaFrame",		"INDICADOR"});
		formClasses.put("DATASRC",
			new String[] {"bsc.swing.framework.BscDataSourceFrame",		"INDICADOR"});
		formClasses.put("INDDOC",
			new String[] {"bsc.swing.framework.BscIndDocFrame",		"INDICADOR"});
		formClasses.put("INICIATIVA",
			new String[] {"bsc.swing.framework.BscIniciativaFrame",		"OBJETIVO"});
		formClasses.put("FCS",
			new String[] {"bsc.swing.framework.BscFcsFrame",		"OBJETIVO"});
		formClasses.put("FCSIND",
			new String[] {"bsc.swing.framework.BscFcsIndicadorFrame",	"FCS"});
		formClasses.put("FCSMETA",
			new String[] {"bsc.swing.framework.BscFcsMetaFrame",		"FCSIND"});
		formClasses.put("FCSAVALIACAO",
			new String[] {"bsc.swing.framework.BscFcsAvaliacaoFrame",	"FCSIND"});
		formClasses.put("FCSPLAN",
			new String[] {"bsc.swing.framework.BscFcsPlanilhaFrame",	"FCSIND"});
		formClasses.put("FCSRPLAN",
			new String[] {"bsc.swing.framework.BscFcsRPlanilhaFrame",	"FCSIND"});
		formClasses.put("FCSDATASRC",
			new String[] {"bsc.swing.framework.BscFcsDataSourceFrame",	"FCSIND"});
		formClasses.put("FCSDOC",
			new String[] {"bsc.swing.framework.BscFcsDocFrame",			"FCSIND"});
		formClasses.put("INIDOC",
			new String[] {"bsc.swing.framework.BscIniDocFrame",		"INICIATIVA"});
		formClasses.put("TAREFA",
			new String[] {"bsc.swing.framework.BscTarefaFrame",		"INICIATIVA"});
		formClasses.put("TARDOC",
			new String[] {"bsc.swing.framework.BscTarDocFrame",		"TAREFA"});
		formClasses.put("RETORNO",
			new String[] {"bsc.swing.framework.BscRetornoFrame",		"TAREFA"});
		formClasses.put("PESSOA",
			new String[] {"bsc.swing.framework.BscPessoaFrame",		"ORGANIZACAO"});
		formClasses.put("PGRUPO",
			new String[] {"bsc.swing.framework.BscGrupoPessoaFrame",	"ORGANIZACAO"});
		formClasses.put("REUNIAO",
			new String[] {"bsc.swing.framework.BscReuniaoFrame",		"ORGANIZACAO"});
		formClasses.put("REUDOC",
			new String[] {"bsc.swing.framework.BscReuDocFrame",		"REUNIAO"});
		formClasses.put("REURET",
			new String[] {"bsc.swing.framework.BscReuRetFrame",		"REUNIAO"});
		formClasses.put("REUPAU",
			new String[] {"bsc.swing.framework.BscReuPauFrame",		"REUNIAO"});
		formClasses.put("MENSAGEM",
			new String[] {"bsc.swing.framework.BscMensagemFrame",	"MENSAGEM"});
		formClasses.put("MENSAGENS_ENVIADAS",
			new String[] {"bsc.swing.framework.BscMensagensEnviadasFrame",	"MENSAGENS_ENVIADAS"});
		formClasses.put("MENSAGENS_RECEBIDAS",
			new String[] {"bsc.swing.framework.BscMensagensRecebidasFrame",	"MENSAGENS_RECEBIDAS"});
		formClasses.put("MENSAGENS_EXCLUIDAS",
			new String[] {"bsc.swing.framework.BscMensagensExcluidasFrame",	"MENSAGENS_EXCLUIDAS"});
		formClasses.put("ESTIMPORT",
			new String[] {"bsc.swing.framework.BscEstruturaImportFrame",	"ORGANIZACOES"});
		formClasses.put("ESTEXPORT",
			new String[] {"bsc.swing.framework.BscEstruturaExportFrame",	"ORGANIZACOES"});
		
		// Usuarios
		formClasses.put("DIRUSUARIOS",
			new String[] {"bsc.swing.framework.BscDirUsuariosFrame",	"DIRUSUARIOS"});
		formClasses.put("USUARIO",
			new String[] {"bsc.swing.framework.BscUsuarioFrame",		"DIRUSUARIOS"});
		formClasses.put("GRUPO",
			new String[] {"bsc.swing.framework.BscGrupoFrame",		"DIRUSUARIOS"});
		formClasses.put("USERPERM",
			new String[] {"bsc.swing.framework.BscUsuarioPerm",		"DIRUSUARIOS"});
                
		//Mensagens / Alertas
		formClasses.put("SMTPCONF",
			new String[] {"bsc.swing.framework.BscSMTPConfFrame",		"DIRUSUARIOS"});
		//Agendamentos
		formClasses.put("AGENDADOR",
			new String[] {"bsc.swing.framework.BscAgendadorFrame",	        "DIRUSUARIOS"});
		//Agendamentos
		formClasses.put("AGENDAMENTO",
			new String[] {"bsc.swing.framework.BscAgendamentoFrame",	"DIRUSUARIOS"});
		
		// analisys
		formClasses.put("DASHBOARD",
			new String[] {"bsc.swing.analisys.BscMedidaDashboardFrame",	"ESTRATEGIA"});
		formClasses.put("CENTRAL",
			new String[] {"bsc.swing.analisys.BscCentralFrame",		"ESTRATEGIA"});
		formClasses.put("DRILL",
			new String[] {"bsc.swing.analisys.BscDrillFrame",		"CENTRAL"});
		formClasses.put("DRILLIND",
			new String[] {"bsc.swing.analisys.BscDrillIndicador",   "CENTRAL"});
		formClasses.put("DRILLOBJ",
			new String[] {"bsc.swing.analisys.BscDrillObjetivo",    "CENTRAL"});
		
		// map
		formClasses.put("MAPAEST",
			new String[] {"bsc.swing.map.BscMapaFrame",			"ESTRATEGIA"});
		
		formClasses.put("MAPAEST2",
			new String[] {"bsc.swing.map.BscMapaOvalFrame",			"ESTRATEGIA"});
		
		formClasses.put("TEMAEST",
			new String[] {"bsc.swing.map.BscTemaEstrategico",			"ESTRATEGIA"});
		
		// mapa de Objetivos -> FCS -> Processos
		formClasses.put("MAPAOBJ",
			new String[] {"bsc.swing.map.BscMapaObjxFcs",		"ESTRATEGIA"});
		
		formClasses.put("PERSP_DRILLD",
			new String[] {"bsc.swing.map.BscPerspectiva_DDFrame",	"ESTRATEGIA"});
		
		formClasses.put("INDICADOR_DET",
			new String[] {"bsc.swing.map.BscIndicadorDetalheFrame",	"ESTRATEGIA"});
		
		// reports
		formClasses.put("RELEST",
			new String[] {"bsc.swing.report.BscRelEstFrame",		"ESTRATEGIA"});
		formClasses.put("RELIND",
			new String[] {"bsc.swing.report.BscRelIndFrame",		"ESTRATEGIA"});
		formClasses.put("RELTAR",
			new String[] {"bsc.swing.report.BscRelTarFrame",		"ESTRATEGIA"});
		formClasses.put("REL5W2H",
			new String[] {"bsc.swing.report.BscRel5w2hFrame",		"ESTRATEGIA"});
		formClasses.put("RELBOOKSTRA",
			new String[] {"bsc.swing.report.BscRelBookFrame",		"ESTRATEGIA"});
		formClasses.put("RELEVOL",
			new String[] {"bsc.swing.report.BscRelEvolucao",		"ESTRATEGIA"});
		
		
		
		//graphs
		formClasses.put("GRAPH",
			new String[] {"bsc.swing.graph.BscGraphFrame",			"ESTRATEGIA"});
		
		
		//Desktop
		formClasses.put("DESKTOP",
			new String[] {"bsc.swing.desktop.BscDesktopFrame",		"DESKTOP"});

		formClasses.put("PWDUSUARIO",
			new String[] {"bsc.swing.framework.BscPwdUsuarioFrame",	"DESKTOP"});

	}
	
	public bsc.applet.BscMainPanel getBscMainPanel() {
		return mainPanel;
	}
	
	public void closeAll() {
		forms.clear();
	}
	
	public String getFormClassName(String type) {
		String formClassName = null;
		if (type.equals("BSC")) {
                    type = "ORGANIZACOES";
                }
		String[] formFeatures = (String[])formClasses.get(type);
		try{
			formClassName = formFeatures[0];
		}catch(Exception fe){
		}
		return formClassName;
	}
	
	public String getParentName(String type) {
		String[] formFeatures = (String[])formClasses.get(type);
		return formFeatures[1];
	}
	
	public bsc.swing.BscDefaultFrameFunctions getParentForm(String type, String parentId) {
		String parentType = getParentName(type);
		if ( parentType == null ) {
                    return null;
                } else {
                    return getFrameReference( parentType, parentId );
                }
	}
	
	private bsc.swing.BscDefaultFrameFunctions createForm(String type, String id) {
		bsc.swing.BscDefaultFrameFunctions form = null;
		String className = getFormClassName(type);
                
		if(className == null) {
                    throw new BscFormControllerException(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscFormController_00001") + type + ".");
                }
		
		Class formClass;
		java.lang.reflect.Constructor constructor;
		Class[] argumentsClass = new Class[] {int.class, String.class, String.class};
		
		// Argumentos para iniciar o form
		Object[] arguments = new Object[] {new Integer(bsc.swing.BscDefaultFrameBehavior.NORMAL), id, "0"};
		
		try {
			formClass = Class.forName(className);
			constructor = formClass.getConstructor(argumentsClass);
			form = (bsc.swing.BscDefaultFrameFunctions) constructor.newInstance(arguments);
			form.asJInternalFrame().addInternalFrameListener(new javax.swing.event.InternalFrameAdapter(){
                @Override
				public void internalFrameClosing(javax.swing.event.InternalFrameEvent closeEvent){
					windowClosed(closeEvent);
				}
			});
		} catch (ClassNotFoundException e) {
			System.out.println(e);
            e.printStackTrace();
		} catch (NoSuchMethodException e) {
			System.out.println(e);
            e.printStackTrace();
		} catch (InstantiationException e) {
			System.out.println(e);
            e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.out.println(e);
            e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println(e);
            e.printStackTrace();
		} catch (java.lang.reflect.InvocationTargetException e) {
			System.out.println(e);
            e.printStackTrace();
		}
		
		return form;
	}
	
	public boolean replaceFormInfo( String typePrev, String idPrev, String typeNew, String idNew ) {
		String namePrev = new String(typePrev + " - " + idPrev);
		String nameNew = new String(typeNew + " - " + idNew);
		bsc.swing.BscDefaultFrameFunctions frameNew =
			(bsc.swing.BscDefaultFrameFunctions) forms.get( nameNew );
		bsc.swing.BscDefaultFrameFunctions framePrev =
			(bsc.swing.BscDefaultFrameFunctions) forms.get( namePrev );
		
		// Ainda n�o existe um frameNew aberto.
		if ( frameNew == null ) {
			setNewDetailFormInfo(typeNew, idNew, framePrev );
			forms.remove(namePrev);
			return true;
		} else
			// J� existe, mas foi fechado.
			if ( !frameNew.asJInternalFrame().isVisible() ) {
			setNewDetailFormInfo(typeNew, idNew, framePrev );
			forms.remove(namePrev);
			return true;
			}
		// existe e est� aberto.
			else {
			try {
				if(! mainPanel.radJanela.isSelected()){
					javax.swing.JPanel tmpPanel =  (javax.swing.JPanel)frameNew.asJInternalFrame().getClientProperty("tmpPanel");
					tabbedPanel.setSelectedComponent(tmpPanel);
				} else{
					frameNew.asJInternalFrame().setSelected(true);
				}
			} catch (java.beans.PropertyVetoException e2) {
				bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscFormController_00002")+
					typeNew);
			}
			return false;
			}
	}
	
	public bsc.swing.BscDefaultFrameFunctions newDetailForm(String type, String parentID, String contextID ) {
		bsc.swing.BscDefaultFrameFunctions form = null;
		String className = getFormClassName(type);
		if(className == null) {
                    throw new BscFormControllerException(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscFormController_00003") + type + ".");
                }
		
		Class formClass;
		java.lang.reflect.Constructor constructor;
		Class[] argumentsClass = new Class[] {int.class, String.class, String.class};
		
		// Argumentos para iniciar o form
		Object[] arguments = new Object[] {new Integer(bsc.swing.BscDefaultFrameBehavior.INSERTING), parentID, contextID};
		
		try {
			formClass = Class.forName(className);
			constructor = formClass.getConstructor(argumentsClass);
			form = (bsc.swing.BscDefaultFrameFunctions) constructor.newInstance(arguments);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (InstantiationException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (java.lang.reflect.InvocationTargetException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		if(mainPanel.radJanela.isSelected()){
			desktop.add(form.asJInternalFrame(), javax.swing.JLayeredPane.DEFAULT_LAYER);
		}else{
			addTabbedToContainer(form, form.asJInternalFrame().getTitle());
		}
		
		try {
			form.asJInternalFrame().setSelected(true);
		} catch (java.beans.PropertyVetoException e2) {
            e2.printStackTrace();
			bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscFormController_00004")+type);
		}
		
		form.asJInternalFrame().show();
		
		return form;
	}
	
	public void setNewDetailFormInfo( String type, String newId, bsc.swing.BscDefaultFrameFunctions newFrame ) {
		String name = new String(type + " - " + newId);
		forms.put( name, newFrame );
	}
	
	public bsc.swing.BscDefaultFrameFunctions getForm(String type, String id, String title ) {
		String name = new String(type + " - " + id);
		bsc.swing.BscDefaultFrameFunctions frame = (bsc.swing.BscDefaultFrameFunctions) forms.get( name );
		
		if ( frame == null ) {
			frame = createForm( type, id );
			frame.asJInternalFrame().setTitle( frame.asJInternalFrame().getTitle()+" - " + title);
			forms.put( name, frame );
			if(mainPanel.radJanela.isSelected()){
				desktop.add(frame.asJInternalFrame(), javax.swing.JLayeredPane.DEFAULT_LAYER);
			}else{
				addTabbedToContainer(frame,title);
			}
		}else{
			if (!frame.asJInternalFrame().isVisible() ) {
				frame = createForm( type, id );
				frame.asJInternalFrame().setTitle( frame.asJInternalFrame().getTitle() + " - " + title);
				forms.put( name, frame );
				if(mainPanel.radJanela.isSelected()){
					desktop.add(frame.asJInternalFrame(), javax.swing.JLayeredPane.DEFAULT_LAYER);
				}else{
					addTabbedToContainer(frame,title);
				}
			}else{
				if(! mainPanel.radJanela.isSelected()){
					javax.swing.JPanel tmpPanel =  (javax.swing.JPanel)frame.asJInternalFrame().getClientProperty("tmpPanel");
					tabbedPanel.setSelectedComponent(tmpPanel);
				}
			}
			
		}
		
		try {
			frame.asJInternalFrame().setSelected(true);
		} catch (java.beans.PropertyVetoException e2) {
			bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscFormController_00005")+type);
		}
		frame.asJInternalFrame().show();

        //Mantem o formulario sempre MAXIMIZADO
        try {
            frame.asJInternalFrame().setMaximum(true);
        } catch (Exception e2) {
        e2.printStackTrace();
}

        return frame;
	}
	
	public bsc.swing.BscDefaultFrameFunctions getFrameReference(String type, String id ) {
		String name = new String(type + " - " + id);
		bsc.swing.BscDefaultFrameFunctions frame = (bsc.swing.BscDefaultFrameFunctions) forms.get( name );
		
		if ( frame != null ) {
                    if ( !frame.asJInternalFrame().isVisible() ) {
                        frame = null;
                    }
                }
		
		return frame;
	}
	
	public void addTabbedToContainer(bsc.swing.BscDefaultFrameFunctions frame, String title){
		javax.swing.JPanel jPainelTmp = new javax.swing.JPanel();
		
		jPainelTmp.setLayout(new java.awt.BorderLayout());
		if(tabbedPanel==null){
			tabbedPanel = new bsc.swing.BscCloseTabbedPane();
			tabbedPanel.setRequestFocusEnabled(false);
			tabbedPanel.addBscCloseTabbedPaneListener(this);
			desktop.setLayout(new java.awt.BorderLayout());
			desktop.add(tabbedPanel);
		}
		jPainelTmp.add(frame.asJInternalFrame().getContentPane());
		title = frame.asJInternalFrame().getTitle() == null?title:frame.asJInternalFrame().getTitle();
		tabbedPanel.addTab(title,frame.asJInternalFrame().getFrameIcon(), jPainelTmp, frame);
		frame.asJInternalFrame().putClientProperty("tmpPanel", jPainelTmp);
		//Selecionada a tab adicionada como atual.
		tabbedPanel.setSelectedComponent(jPainelTmp);
	}
	
	public void beforeTabRemoved(java.awt.event.ComponentEvent componentEvent) {
		javax.swing.JPanel jTmpPanel = (javax.swing.JPanel)tabbedPanel.getComponentAt(componentEvent.getID());
		bsc.swing.BscDefaultFrameFunctions removedPainel = (bsc.swing.BscDefaultFrameFunctions)jTmpPanel.getClientProperty("FrameParent");
		removedPainel.asJInternalFrame().dispose();
		String name = new String(removedPainel.getType() + " - " + removedPainel.getID());
		forms.remove(name);
	}
	
	public void afterTabRemoved(java.awt.event.ComponentEvent componentEvent) {
		removeDesktopContainer();
	}
	
	public void windowClosed(javax.swing.event.InternalFrameEvent closeEvent){
		bsc.swing.BscDefaultFrameFunctions frmFunctions  = (bsc.swing.BscDefaultFrameFunctions)closeEvent.getSource();
		String name = new String(frmFunctions.getType() + " - " + frmFunctions.getID());
		forms.remove(name);
	}
	
	/**
	 *Cria��o de um novo form de detalhes.
	 **/
	public bsc.swing.BscDefaultFrameFunctions newForm(String type, String id, String contextID ) {
		String name = new String(type + " - " + id);
		
		if(forms.containsKey(name)){
			bsc.swing.BscDefaultFrameFunctions form = isFormOpen(type, id);
			if(mainPanel.radJanela.isSelected()) {
				form.asJInternalFrame().dispose();
			}else{
				if(tabbedPanel.getComponentCount()!=0){
					javax.swing.JPanel tmpPanel =  (javax.swing.JPanel)form.asJInternalFrame().getClientProperty("tmpPanel");
					tabbedPanel.remove(tmpPanel);
				}
			}
			forms.remove(name);
		}
		
		bsc.swing.BscDefaultFrameFunctions form = newDetailForm(type,id,contextID);
		forms.put(name, form);
		return form;
	}
	
	public void changeView(){
		java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR);
		if(forms.size() > 0 ){
			//Vis�o de janelas selecionadas. Remover as tabelas. (Pastas).
			if(mainPanel.radJanela.isSelected() && tabbedPanel != null){
				desktop.remove(tabbedPanel);
				desktop.setLayout(null);
				desktop.repaint();
				tabbedPanel = null;
			}
			
			for(java.util.Enumeration iForm = forms.elements();iForm.hasMoreElements();){
				bsc.swing.BscDefaultFrameFunctions tmpFrame = (bsc.swing.BscDefaultFrameFunctions)iForm.nextElement();
				String title = tmpFrame.asJInternalFrame().getTitle();
				title = title.substring(title.indexOf("-") + 1, title.length());
				tmpFrame.asJInternalFrame().dispose();
				getForm(tmpFrame.getType(), tmpFrame.getID(),title.trim().concat(" "));
			}
			//Janelas em cascata.
			if(mainPanel.radJanela.isSelected()){
				javax.swing.JInternalFrame [] frames = desktop.getAllFrames();
				for (int i = 0; i < frames.length; i++) {
					int pos = i * 25;
					frames[i].setLocation(pos, pos);
					try{
						frames[i].setSelected(true);
					}catch(java.beans.PropertyVetoException e){
						System.out.println(e);
					}
				}
			}
		}
		if(mainPanel.radJanela.isSelected()){
			this.lastView = ST_JANELA;
		}else{
			this.lastView = ST_PASTA;
		}
	}
	
	public bsc.swing.BscDefaultFrameFunctions isFormOpen(String type, String id) {
		String name = new String(type + " - " + id);
		return (bsc.swing.BscDefaultFrameFunctions)forms.get(name);
	}
	
	public void removeFromForms(String name){
		forms.remove(name);
	}
	
	public void removeDesktopContainer(){
		if(tabbedPanel.getComponentCount()==0){
			desktop.remove(tabbedPanel);
			desktop.setLayout(null);
			desktop.repaint();
			tabbedPanel = null;
		}
	}
}
