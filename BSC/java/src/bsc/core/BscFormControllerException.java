/*
 * FormControllerException.java
 *
 * Created on June 24, 2003, 4:03 PM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BscFormControllerException extends java.lang.RuntimeException {
	
	/**
	 * Creates a new instance of <code>FormControllerException</code> without detail message.
	 */
	public BscFormControllerException() {
	}
	
	
	/**
	 * Constructs an instance of <code>FormControllerException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public BscFormControllerException(String msg) {
		super(msg);
	}
}
