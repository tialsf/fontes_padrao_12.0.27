/*
 * BscColorFeedback.java
 *
 * Created on September 8, 2003, 2:00 PM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BscImageResources {
	
	// Feedback do sistema BSC
	public static final int BSC_FB_GRAY			= 0;
	public static final int BSC_FB_REDDN		= 1;
	public static final int BSC_FB_REDUP		= 2;
	public static final int BSC_FB_REDSM		= 3;
	public static final int BSC_FB_YELLOWDN		= 4;
	public static final int BSC_FB_YELLOWUP		= 5;
	public static final int BSC_FB_YELLOWSM		= 6;
	public static final int BSC_FB_GREENDN		= 7;
	public static final int BSC_FB_GREENUP		= 8;
	public static final int BSC_FB_GREENSM		= 9;
	public static final int BSC_FB_BLUEDN		= 10;
	public static final int BSC_FB_BLUEUP		= 11;
	public static final int BSC_FB_BLUESM		= 12;
	
	// Outras imagens do BSC
	public static final int BSC_IM_ORGANIZACAO		= 13;
	public static final int BSC_IM_ESTRATEGIA		= 14;
	public static final int BSC_IM_PESSOA			= 15;
	public static final int BSC_IM_PERSPECTIVA		= 16;
	public static final int BSC_IM_OBJETIVO			= 17;
	public static final int BSC_IM_INDICADOR		= 18;
	public static final int BSC_IM_INICIATIVA		= 19;
	public static final int BSC_IM_USUARIO			= 20;
	public static final int BSC_IM_GRUPO			= 21;
	public static final int BSC_IM_REUNIAO			= 22;
	public static final int BSC_IM_DIRUSUARIO		= 23;
	public static final int BSC_IM_TAREFA			= 24;
	public static final int BSC_IM_META				= 25;
	public static final int BSC_IM_MAPAEST			= 26;
	public static final int BSC_IM_AGENDADOR		= 27;
	public static final int BSC_IM_AGENDAMENTO		= 28;
	public static final int BSC_IM_SMTPCONF			= 29;
	public static final int BSC_IM_DESKTOP			= 30;
	public static final int BSC_IM_EMAILRECIVED		= 31;
	public static final int BSC_IM_EMAILSENDED		= 32;
	public static final int BSC_IM_EMAILDELETED		= 33;	
	public static final int BSC_IM_TENDENCIA		= 34;
	public static final int BSC_IM_FCS				= 35;
	
	// Array de imagens do BSC
	private javax.swing.ImageIcon[] feedbackImages;
	
	public BscImageResources() {
		
		feedbackImages = new javax.swing.ImageIcon[36];
		feedbackImages[0] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_graysm.gif"));
		feedbackImages[1] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_reddn.gif"));
		feedbackImages[2] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_redup.gif"));
		feedbackImages[3] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_redsm.gif"));
		feedbackImages[4] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_yellowdn.gif"));
		feedbackImages[5] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_yellowup.gif"));
		feedbackImages[6] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_yellowsm.gif"));
		feedbackImages[7] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_greendn.gif"));
		feedbackImages[8] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_greenup.gif"));
		feedbackImages[9] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_greensm.gif"));
		feedbackImages[10] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_bluedn.gif"));
		feedbackImages[11] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_blueup.gif"));
		feedbackImages[12] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_bluesm.gif"));
		
		feedbackImages[13] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_organizacao.gif"));
		feedbackImages[14] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_estrategia.gif"));
		feedbackImages[15] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_pessoa.gif"));
		feedbackImages[16] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_perspectiva.gif"));
		feedbackImages[17] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_objetivo.gif"));
		feedbackImages[18] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_medida.gif"));
		feedbackImages[19] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_iniciativa.gif"));
		feedbackImages[20] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_usuario.gif"));
		feedbackImages[21] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_grupo.gif"));
		feedbackImages[22] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_reuniao.gif"));
		feedbackImages[23] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_dirusuario.gif"));
		feedbackImages[24] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_tarefa.gif"));
		feedbackImages[25] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_meta.gif"));
		feedbackImages[26] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_mapa.gif"));
		feedbackImages[27] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_agendador.gif"));
		feedbackImages[28] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_compromisso.gif"));
		feedbackImages[29] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_agendamento.gif"));
		feedbackImages[30] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_ambiente.png"));
		feedbackImages[31] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_emailrecebido.gif"));
		feedbackImages[32] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_emailenviado.gif"));
		feedbackImages[33] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_emailexcluido.gif"));
		feedbackImages[34] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_tendencia.gif"));
		feedbackImages[35] = new javax.swing.ImageIcon(this.getClass().getResource("/images/ic_20_fcs.gif"));
		
	}
	
	public javax.swing.ImageIcon getImage( int code ) {
		return feedbackImages[code];
	}
	
}
