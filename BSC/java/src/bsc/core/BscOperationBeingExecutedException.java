/*
 * BscOperationBeingExecutedException.java
 *
 * Created on June 3, 2003, 11:39 AM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BscOperationBeingExecutedException extends java.lang.RuntimeException {
	
	/**
	 * Creates a new instance of <code>BscOperationBeingExecutedException</code> without detail message.
	 */
	public BscOperationBeingExecutedException() {
	}
	
	
	/**
	 * Constructs an instance of <code>BscOperationBeingExecutedException</code> with the specified detail message.
	 * @param msg the detail message.
	 */
	public BscOperationBeingExecutedException(String msg) {
		super(msg);
	}
}
