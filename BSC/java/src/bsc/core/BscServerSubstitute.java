/*
 * BscServerSubstitute.java
 *
 * Created on September 9, 2003, 9:14 AM
 */

package bsc.core;

/**
 *
 * @author  siga1996
 */
public class BscServerSubstitute {
	
	private final String error = new String(
	"<RESPOSTAS><RESPOSTA><STATUS MSG=\"No Server Mode ON - Parāmetros desconhecidos.\">" +
				BscDataController.BSC_ST_GENERALERROR +	"</STATUS></RESPOSTA></RESPOSTAS>");
	
	public BscServerSubstitute() {
	}
				
	public String sendData ( String xml ) {
		String retorno = null;
		String fileContent = null;
		bsc.core.BscDebug.println("************ VIRTUAL SERVER **************");
		bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscServerSubstitute_00001"));
		bsc.core.BscDebug.println( xml );
		bsc.xml.BIXMLVector vector = new bsc.xml.BIXMLVector();
		vector.parseXML( xml );
		if ( vector.get(0).getString("COMANDO").equals("ARVORE") ) {
			if ( vector.get(0).getString("TIPO").equals("ORGANIZACAO") ) {
				//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/bsc/core/serversubstitute/arvore.xml").toString());
			}
			else {
				retorno = error;
			}
		}
		else
			if ( vector.get(0).getString("TIPO").equals("DASHBOARD") ) {
				if ( vector.get(0).getString("COMANDO").equals("CARREGAR") ) {
					//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/bsc/core/serversubstitute/dashboardcarregar.xml").toString());
				}
				else {
					if ( vector.get(0).getString("COMANDO").equals("ALTERAR")  ||
							vector.get(0).getString("COMANDO").equals("EXCLUIR") ) {
						//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/bsc/core/serversubstitute/dashboardalterar.xml").toString());
					}
					else
						retorno = error;
				}
			}
			else
				if ( vector.get(0).getString("TIPO").equals("PLANILHA") ) {
						//fileContent = BscCommunicationController.getFile(this.getClass().getResource("/bsc/core/serversubstitute/teste.xml").toString());
				}
				else
					retorno = error;
		
		if ( fileContent != null ) {
			bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscServerSubstitute_00002"));
			bsc.core.BscDebug.println( fileContent );
			bsc.core.BscDebug.println("******** END VIRTUAL SERVER **************");
			retorno = fileContent;
		}
		
		return retorno;
	}
}
