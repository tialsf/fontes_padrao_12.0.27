/*
 * BscStaticReferences.java
 *
 * Created on 18 de Dezembro de 2003, 11:42
 */

package bsc.core;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author  siga1728
 */
public class BscStaticReferences {
	
	public final static int BSC_FREQ_ANUAL		= 1;
	public final static int BSC_FREQ_SEMESTRAL	= 2;
	public final static int BSC_FREQ_QUADRIMESTRAL	= 9;
	public final static int BSC_FREQ_TRIMESTRAL	= 3;
	public final static int BSC_FREQ_BIMESTRAL	= 4;
	public final static int BSC_FREQ_MENSAL		= 5;
	public final static int BSC_FREQ_QUINZENAL	= 6;
	public final static int BSC_FREQ_SEMANAL	= 7;
	public final static int BSC_FREQ_DIARIA		= 8;
	
	public final static int BSC_APPLET				=	0;
	public final static int BSC_APLICATION			=	1;
	
	private static String BSC_URL = "http://localhost/bsc/";

	private static int								m_bscExecutionType;
	private static bsc.applet.BscApplication		m_bscAplication;

	private static String					m_bscServerVersion;
	private static bsc.applet.BscApplet			m_bscApplet;
	private static bsc.applet.BscMainPanel			m_bscMainPanel;
	private static bsc.core.BscImageResources		m_bscImageResources;
	private static bsc.swing.BscDefaultDialogSystem		m_bscDialogSystem;
	private static bsc.core.BscFormController		m_bscFormController;
	private static bsc.core.BscDataController		m_bscDataController;
	private static java.util.Locale				m_bscDefaultLocale;
	
	/** Getter for property bscApplet.
	 * @return Value of property bscApplet.
	 *
	 */
	public static bsc.applet.BscApplet getBscApplet() {
		return m_bscApplet;
	}
	
	/** Setter for property bscApplet.
	 * @param bscApplet New value of property bscApplet.
	 *
	 */
	public static void setBscApplet(bsc.applet.BscApplet bscApplet) {
		m_bscApplet = bscApplet;
	}
	
	/** Getter for property bscDialogSystem.
	 * @return Value of property bscDialogSystem.
	 *
	 */
	public static bsc.swing.BscDefaultDialogSystem getBscDialogSystem() {
		return m_bscDialogSystem;
	}
	
	/** Setter for property bscDialogSystem.
	 * @param bscDialogSystem New value of property bscDialogSystem.
	 *
	 */
	public static void setBscDialogSystem(bsc.swing.BscDefaultDialogSystem bscDialogSystem) {
		m_bscDialogSystem = bscDialogSystem;
	}
	
	/** Getter for property bscFormController.
	 * @return Value of property bscFormController.
	 *
	 */
	public static bsc.core.BscFormController getBscFormController() {
		return m_bscFormController;
	}
	
	/** Setter for property bscFormController.
	 * @param bscFormController New value of property bscFormController.
	 *
	 */
	public static void setBscFormController(bsc.core.BscFormController bscFormController) {
		m_bscFormController = bscFormController;
	}
	
	/** Getter for property bscImageResources.
	 * @return Value of property bscImageResources.
	 *
	 */
	public static bsc.core.BscImageResources getBscImageResources() {
		return m_bscImageResources;
	}
	
	/** Setter for property bscImageResources.
	 * @param bscImageResources New value of property bscImageResources.
	 *
	 */
	public static void setBscImageResources(bsc.core.BscImageResources bscImageResources) {
		m_bscImageResources = bscImageResources;
	}
	
	/** Getter for property bscMainPanel.
	 * @return Value of property bscMainPanel.
	 *
	 */
	public static bsc.applet.BscMainPanel getBscMainPanel() {
		return m_bscMainPanel;
	}
	
	/** Setter for property bscMainPanel.
	 * @param bscMainPanel New value of property bscMainPanel.
	 *
	 */
	public static void setBscMainPanel(bsc.applet.BscMainPanel bscMainPanel) {
		m_bscMainPanel = bscMainPanel;
	}
	
	/** Getter for property m_bscDataController.
	 * @return Value of property m_bscDataController.
	 *
	 */
	public static bsc.core.BscDataController getBscDataController() {
		return m_bscDataController;
	}
	
	/** Setter for property m_bscDataController.
	 * @param m_bscDataController New value of property m_bscDataController.
	 *
	 */
	public static void setBscDataController(bsc.core.BscDataController bscDataController) {
		m_bscDataController = bscDataController;
	}
	
	/** Getter for property m_bscDefaultLocale.
	 * @return Value of property m_bscDefaultLocale.
	 *
	 */
	public static java.util.Locale getBscDefaultLocale() {
		if(m_bscDefaultLocale==null)
			m_bscDefaultLocale = new java.util.Locale("pt","BR");
		return m_bscDefaultLocale;
	}
	
	/** Setter for property m_bscDefaultLocale.
	 * @param m_bscDefaultLocale New value of property m_bscDefaultLocale.
	 *
	 */
	public static void setBscDefaultLocale(java.util.Locale bscDefaultLocale) {
		m_bscDefaultLocale = bscDefaultLocale;
	}
	
	/**
	 * Getter for property m_bscServerVersion.
	 * @return Value of property m_bscServerVersion.
	 */
	public static java.lang.String getBscServerVersion() {
		return m_bscServerVersion;
	}
	
	/**
	 * Setter for property m_bscServerVersion.
	 * @param bscServerVersion New value of property bscServerVersion.
	 */
	public static void setBscServerVersion(java.lang.String bscServerVersion) {
		m_bscServerVersion = bscServerVersion;
	}
	
	
	public static int getBscExecutionType() {
		return m_bscExecutionType;
	}
	
	public static void setBscExecutionType(int bscExecutionType) {
		m_bscExecutionType = bscExecutionType;
	}
	
	public static bsc.applet.BscApplication getBscAplication() {
		return m_bscAplication;
	}
	
	public static void setBscAplication(bsc.applet.BscApplication bscAplication) {
		m_bscAplication = bscAplication;
	}

    /**
     * @return the BSC_URL
     */
    public static String getBscURL() {
        return BSC_URL;
    }

    /**
     * @param aBSC_URL the BSC_URL to set
     */
    public static void setBscURL(String cBscURL) {
        BSC_URL = cBscURL;
    }

    /**
     * Monta uma URL apartir da URL base do SigaBSC
     * @param cSpecificURL
     *  O caminho virtual a ser montado
     * @return URL object
     */
    public static URL mountBscURL(String cSpecificURL) {

        URL url = null;
        
        try {
            url = new URL(getBscURL() + "/" + cSpecificURL);
        } catch (MalformedURLException ex) {
            Logger.getLogger(BscStaticReferences.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return url;
    }

}