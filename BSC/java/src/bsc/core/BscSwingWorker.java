/*
 * BscSwingWorker.java
 *
 * Created on 3 de Novembro de 2004, 09:32
 */

package bsc.core;

/**
 *
 * @author  alexandreas
 */
public abstract class BscSwingWorker extends SwingWorker {
	
	private bsc.swing.BscTree threadTree;
	
	/** Creates a new instance of BscSwingWorker */
	public BscSwingWorker(bsc.swing.BscTree threadTree) {
		super();
		this.threadTree = threadTree;
	}
	
	/**
	 * Getter for property threadTree.
	 * @return Value of property threadTree.
	 */
	public  bsc.swing.BscTree getThreadTree() {
		return threadTree;
	}
	
	/**
	 * Setter for property threadTree.
	 * @param threadTree New value of property threadTree.
	 */
	public void setThreadTree(bsc.swing.BscTree threadTree) {
		this.threadTree = threadTree;
	}
	
}
