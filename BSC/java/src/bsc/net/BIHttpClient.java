package bsc.net;

import java.net.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class BIHttpClient {
	URL defaultUrl;
	String sessionId = "";
	boolean hasCookieManagement = false;
	
	public BIHttpClient(String url, String sessionID) {
		this.sessionId = sessionID;
		
		if( ! System.getProperty("java.version").startsWith("1.4.2") || !System.getProperty("java.version").startsWith("1.5.0")){
			hasCookieManagement = true;
		}else{
			hasCookieManagement = false;
		}
		
		try {
			this.defaultUrl = new java.net.URL(url);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setSessionID( String value ) {
		sessionId = String.valueOf(value);
	}
	
	public String doGet() throws BIHttpException {
		String line;
		StringBuffer buffer = new StringBuffer();
		java.io.InputStream input;
		java.io.BufferedReader dataInput;
		try {
			java.net.HttpURLConnection connection = (java.net.HttpURLConnection)defaultUrl.openConnection();
			connection.setRequestProperty("Referer", defaultUrl.toString());
			connection.setUseCaches(false);
			if (!hasCookieManagement || bsc.core.BscStaticReferences.getBscExecutionType()== bsc.core.BscStaticReferences.BSC_APLICATION){
				connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
			}
			input = connection.getInputStream();
			dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input,"ISO-8859-1"));
			while ((line = dataInput.readLine()) != null) {
				buffer.append(line);
				buffer.append('\n');
			}
			dataInput.close();
			connection.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return buffer.toString();
	}
	
	public String doGet(String strUrl) throws BIHttpException {
		String line;
		StringBuffer buffer = new StringBuffer();
		java.io.InputStream input;
		java.io.BufferedReader dataInput;
		try {
			java.net.URL url = new java.net.URL(strUrl);
			java.net.HttpURLConnection connection = (java.net.HttpURLConnection)url.openConnection();
			connection.setRequestProperty("Referer", defaultUrl.toString());
			connection.setUseCaches(false);
			if (!hasCookieManagement || bsc.core.BscStaticReferences.getBscExecutionType()== bsc.core.BscStaticReferences.BSC_APLICATION){
				connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
			}
			input = connection.getInputStream();
			dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input,"ISO-8859-1"));
			while ((line = dataInput.readLine()) != null) {
				buffer.append(line); 
				buffer.append('\n');
			}
			dataInput.close();
			connection.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return buffer.toString();
	}
	
	public String doPost(String data) throws BIHttpException {
		String line;
		StringBuffer buffer = new StringBuffer();
		java.io.InputStream input;
		java.io.BufferedReader dataInput;
		try {
			java.net.HttpURLConnection connection = (java.net.HttpURLConnection)defaultUrl.openConnection();
			connection.setRequestProperty("Referer", defaultUrl.toString());
			connection.setUseCaches(false);
			if (!hasCookieManagement || bsc.core.BscStaticReferences.getBscExecutionType()== bsc.core.BscStaticReferences.BSC_APLICATION){
				connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
			}
			
			connection.setDoOutput(true);
			java.io.PrintWriter out = new java.io.PrintWriter(connection.getOutputStream());
			out.println("bsccontent=" + java.net.URLEncoder.encode(data, "ISO-8859-1"));
			out.close();
			
			input = connection.getInputStream();
			dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input,"ISO-8859-1"));
			while ((line = dataInput.readLine()) != null) {
				buffer.append(line);
				buffer.append('\n');
			}
			dataInput.close();
			
			bsc.core.BscDebug.println("*** Request ***");
			bsc.core.BscDebug.println("URL: " + defaultUrl);
			bsc.core.BscDebug.println("Method: " + connection.getRequestMethod());
			bsc.core.BscDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
			bsc.core.BscDebug.println("Request Body:\nbsccontent=\n" + data);
			bsc.core.BscDebug.println();
			bsc.core.BscDebug.println("*** Response ***");
			bsc.core.BscDebug.println("Response Code: " + connection.getResponseCode());
			bsc.core.BscDebug.println("Response Message: " + connection.getResponseMessage());
			bsc.core.BscDebug.println("Response Body:\n" + buffer.toString());
			bsc.core.BscDebug.println();
			connection.disconnect();
		} catch (Exception e) {
			throw new BIHttpException( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIHttpClient_00001")+ e.getMessage() );
		}
		return buffer.toString();
	}
	
	public String doPost(String strUrl, String data ) throws BIHttpException{
		String line;
		StringBuffer buffer = new StringBuffer();
		java.io.InputStream input;
		java.io.BufferedReader dataInput;
		try {
			java.net.URL url = new java.net.URL(strUrl);
			java.net.HttpURLConnection connection = (java.net.HttpURLConnection)url.openConnection();
			connection.setRequestProperty("Referer", defaultUrl.toString());
			connection.setUseCaches(false);
			if(!hasCookieManagement || bsc.core.BscStaticReferences.getBscExecutionType()== bsc.core.BscStaticReferences.BSC_APLICATION){
				connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
			}
			connection.setDoOutput(true);
			java.io.PrintWriter out = new java.io.PrintWriter(connection.getOutputStream());
			out.println("bsccontent=" + java.net.URLEncoder.encode(data, "ISO-8859-1"));
			out.close();
			
			input = connection.getInputStream();
			dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input,"ISO-8859-1"));
			while ((line = dataInput.readLine()) != null) {
				buffer.append(line);
				buffer.append('\n');
			}
			dataInput.close();
			
			bsc.core.BscDebug.println("*** Request ***");
			bsc.core.BscDebug.println("URL: " + url);
			bsc.core.BscDebug.println("Method: " + connection.getRequestMethod());
			bsc.core.BscDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
			bsc.core.BscDebug.println("Request Body:\nbsccontent=\n" + data);
			bsc.core.BscDebug.println();
			bsc.core.BscDebug.println("*** Response ***");
			bsc.core.BscDebug.println("Response Code: " + connection.getResponseCode());
			bsc.core.BscDebug.println("Response Message: " + connection.getResponseMessage());
			bsc.core.BscDebug.println("Response Body:\n" + buffer.toString());
			bsc.core.BscDebug.println();
			connection.disconnect();
		} catch (Exception e) {
			throw new BIHttpException( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIHttpClient_00001")+ e.getMessage() );
		}
		return buffer.toString();
	}
	
	public String doLogin( String strUrl, String username, String password )
	throws BIHttpException {
		StringBuffer buffer = new StringBuffer();
		try {
			java.net.HttpURLConnection connection;
			java.net.URL url;
			String line;
			String data;
			java.io.InputStream input;
			java.io.BufferedReader dataInput;
			
			url = new java.net.URL(strUrl);
			connection = (java.net.HttpURLConnection)url.openConnection();
			connection.setRequestProperty("Referer", defaultUrl.toString());
			connection.setUseCaches(false);
			if(!hasCookieManagement || bsc.core.BscStaticReferences.getBscExecutionType()== bsc.core.BscStaticReferences.BSC_APLICATION){
				connection.setRequestProperty("Cookie", "SESSIONID=" + sessionId);
			}
			connection.setDoOutput(true);
			java.io.PrintWriter out = new java.io.PrintWriter(connection.getOutputStream());
			
			data = "login=" + java.net.URLEncoder.encode("true", "ISO-8859-1");
			data += "&username=" + java.net.URLEncoder.encode(username, "ISO-8859-1");
			data += "&password=" + java.net.URLEncoder.encode(password, "ISO-8859-1");
			out.println(data);
			out.close();
			
			input = connection.getInputStream();
			dataInput = new java.io.BufferedReader(new java.io.InputStreamReader(input,"ISO-8859-1"));
			while ((line = dataInput.readLine()) != null) {
				buffer.append(line);
				buffer.append('\n');
			}
			dataInput.close();
			
			bsc.core.BscDebug.println("*** Request ***");
			bsc.core.BscDebug.println("URL: " + url);
			bsc.core.BscDebug.println("Method: " + connection.getRequestMethod());
			bsc.core.BscDebug.println("Cookie: " + connection.getRequestProperty("Cookie"));
			bsc.core.BscDebug.println("Request Body:\nbsccontent=\n" + data);
			bsc.core.BscDebug.println();
			bsc.core.BscDebug.println("*** Response ***");
			bsc.core.BscDebug.println("Response Code: " + connection.getResponseCode());
			bsc.core.BscDebug.println("Response Message: " + connection.getResponseMessage());
			bsc.core.BscDebug.println("Response Body:\n" + buffer.toString());
			bsc.core.BscDebug.println();
			connection.disconnect();
		} catch (Exception e) {
			throw new BIHttpException( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIHttpClient_00001")+ e.getMessage() );
		}
		return buffer.toString();
	}
}