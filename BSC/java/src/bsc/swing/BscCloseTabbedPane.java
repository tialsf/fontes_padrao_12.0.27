package bsc.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * JTabbedPane com um iconoe no lado esquerdo e um ("X") para fechar cada tab.
 *
 */
public class BscCloseTabbedPane extends JTabbedPane implements javax.swing.event.MouseInputListener{
	javax.swing.ImageIcon iconeFecharSel    = new javax.swing.ImageIcon(getClass().getResource("/images/close-normal-sel.gif"));
	javax.swing.ImageIcon iconeFechar	    = new javax.swing.ImageIcon(getClass().getResource("/images/close-normal.gif"));
	
	final static int POS_LARGURA	    = 15;//Valor para corre��o da posi��o dos bot�es na tab
	final static int POS_ALTURA    	    = 6;//Valor para corre��o da posi��o dos bot�es na tab
	private java.util.Vector vctTabbedListeners;
	
	public BscCloseTabbedPane() {
		super();
		addMouseListener(this);
		addMouseMotionListener(this);
		vctTabbedListeners = new java.util.Vector();
		setRequestFocusEnabled(false);
	}
	
	public void addTab(String title,Icon icon,Component component){
		super.addTab(title.concat("  ") ,icon, component);
	}
	
	public void addTab(String title,Icon icon,Component component,bsc.swing.BscDefaultFrameFunctions frameParent){
		super.addTab(title.concat("  ") ,icon, component);
		javax.swing.JPanel tabTemp = (javax.swing.JPanel)getComponentAt(getComponentCount()-1);
		tabTemp.putClientProperty("FrameParent", frameParent);
	}
	
	public void mouseClicked(MouseEvent e) {
		int tabNumber=getUI().tabForCoordinate(this, e.getX(), e.getY());
		if (tabNumber < 0) return;
		
		Rectangle rect = getBoundsAt(tabNumber);
		int largura = rect.x + (int)rect.getWidth()-POS_LARGURA;
		int altura  = rect.y + POS_ALTURA;
		
		Rectangle i = new Rectangle(largura,altura,iconeFechar.getIconWidth(),iconeFechar.getIconHeight());
		
		if (i.contains(e.getX(), e.getY())) {
			for(int j=0; j<vctTabbedListeners.size(); j++)
				((bsc.swing.BscCloseTabbedPaneListener) vctTabbedListeners.get(j)).beforeTabRemoved(new java.awt.event.ComponentEvent(this, tabNumber));
			this.removeTabAt(tabNumber);
			for(int j=0; j<vctTabbedListeners.size(); j++)
				((bsc.swing.BscCloseTabbedPaneListener) vctTabbedListeners.get(j)).afterTabRemoved(new java.awt.event.ComponentEvent(this, tabNumber));
		}
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		int largura = 0;
		int altura  = 0;
		for (int i = 0, n = getTabCount(); i < n; i++) {
			Rectangle rect = getBoundsAt(i);
			largura = rect.x + (int)rect.getWidth()-POS_LARGURA;
			altura  = rect.y + POS_ALTURA;
			g.drawImage(iconeFechar.getImage(), largura ,altura ,this);
		}
	}
	
	public void mouseMoved(MouseEvent e) {
		int tabNumber=getUI().tabForCoordinate(this, e.getX(), e.getY());
		if (tabNumber < 0) return;
		Graphics g = getGraphics();
		
		Rectangle rect = getBoundsAt(tabNumber);
		int largura = rect.x + (int)rect.getWidth()-POS_LARGURA;
		int altura  = rect.y + POS_ALTURA;
		
		Rectangle i = new Rectangle(largura,altura,iconeFechar.getIconWidth(),iconeFechar.getIconHeight());
		
		if (i.contains(e.getX(), e.getY())) {
			g.drawImage(iconeFecharSel.getImage(), largura ,altura ,this);
		}else{
			g.drawImage(iconeFechar.getImage(), largura  , altura ,this);
		}
	}
	
	public void addBscCloseTabbedPaneListener(bsc.swing.BscCloseTabbedPaneListener listener){
		vctTabbedListeners.add(listener);
	}
	
	public void removeBscCloseTabbedPaneListener(bsc.swing.BscCloseTabbedPaneListener listener){
		vctTabbedListeners.remove(listener);
	}
	
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {}
}