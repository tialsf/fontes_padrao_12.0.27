/*
 * BscCloseTabbedPaneListener.java
 *
 * Created on 9 de Setembro de 2004, 16:32
 */

package bsc.swing;

import java.util.EventListener;

/**
 *
 * @author  alexandreas
 */
public interface BscCloseTabbedPaneListener extends EventListener{
    
    /**
     * Invoked when a component has been removed from the container.
     */    
    public void beforeTabRemoved(java.awt.event.ComponentEvent componentEvent);
    public void afterTabRemoved(java.awt.event.ComponentEvent componentEvent);

}
