/*
 * BscDefaultDialogSystem.java
 *
 * Created on June 27, 2003, 2:58 PM
 */

package bsc.swing;

import bsc.core.BscDataController;
import javax.swing.JOptionPane;

/**
 *
 * @author  siga1996
 */
public class BscDefaultDialogSystem {
	java.awt.Component parent = null;
	
	public BscDefaultDialogSystem (java.awt.Component parentAux) {
		parent = parentAux;
	}
	
	public void errorMessage(String error) {
		JOptionPane.showMessageDialog(parent, error, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"),
		    JOptionPane.OK_OPTION);
	}
		
	public void errorMessage( int error ) {
		switch (error) {
			case BscDataController.BSC_CL_SECONDTASK:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00020")+
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00012"), java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"),
				JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_NORIGHTS:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00004") + 
					java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00016"), java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_HASCHILD:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00013") +
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00002"),
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_EXPIREDSESSION:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00011") +
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00009"),
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_BADID:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00006") +
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00010"),
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_INUSE:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00015")+
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00003"),
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_NOCMD:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00007"),
					java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_NOTRANSACTION:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00008") +
					"NOTRANSACTION.", java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			case BscDataController.BSC_ST_BADXML:
				StringBuffer sMsg = new StringBuffer();
				sMsg.append("<html>");
				sMsg.append("<b>");
				sMsg.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00018")); //Erro de protocolo: BADXML.
				sMsg.append("</b>");
				sMsg.append("<br>");
				sMsg.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00026")); //Caracter inv�lido
				sMsg.append(" '&lt;' ");
				sMsg.append("</html>");
				JOptionPane.showMessageDialog(parent, sMsg.toString(),
					java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
				break;
			default:
				JOptionPane.showMessageDialog(parent, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00017") + error + 
					java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00014"), java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00001"), JOptionPane.OK_OPTION);
		}		
	}
	
	public boolean confirmDeletion() {
	    Object[] options = {
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00024"),
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00025")};

	    int iOption = JOptionPane.showOptionDialog(parent,
			java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00021") + 
			java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00005"),
			java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00019"),
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null, 
			options,
			options[0]);
	    
	    if(iOption == JOptionPane.YES_OPTION){
		return true;
	    }else{
		return false;
	    }
	}
	
	public void warningMessage(String strMessage){
	    Object[] options = {"OK"};
	    JOptionPane.showOptionDialog(parent, strMessage,
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00023"),
		JOptionPane.DEFAULT_OPTION, 
		JOptionPane.WARNING_MESSAGE,
		null, 
		options,
		options[0]);
	}
	
	public void informationMessage(String strMessage){
	    Object[] options = {"OK"};
	    JOptionPane.showOptionDialog(parent, strMessage,
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00022"), 
		JOptionPane.DEFAULT_OPTION, 
		JOptionPane.INFORMATION_MESSAGE,
		null, 
		options,
		options[0]);
	}
	
	public int confirmMessage(String strMessage) {
	    Object[] options = {
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00024"),
		java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00025")};
		
	    int iOption = JOptionPane.showOptionDialog(parent, strMessage,
			java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00019"),
			JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
			null, 
			options,
			options[0]);
	    
	    if(iOption == JOptionPane.YES_OPTION){
		return JOptionPane.YES_OPTION;
	    }else{
		return JOptionPane.NO_OPTION;		    		    		
	    }
	}

	/**
	 * Exibe uma caixa de confirma��o
	 * @param strMessage Mensagem para confirma��o.
	 * @param options Array com as mensagens para os bot�es.
	 * Object[] options = {"Sim","N�o"};
	 */	
	public int confirmMessage(String strMessage,Object[] options) {
	    int iOption = JOptionPane.showOptionDialog(parent, strMessage,
			java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultDialogSystem_00019"),
			JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
			null, 
			options,
			options[0]);
	    
	    return iOption;
	}

	public String inputDialog(String message, String msgDefault){
		String s = (String)JOptionPane.showInputDialog(
				parent,
				message,
				"BSC",
				JOptionPane.PLAIN_MESSAGE,
				null,
				null,
				msgDefault);
	    return s;
	}
}