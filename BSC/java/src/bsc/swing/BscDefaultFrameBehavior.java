/*
 * BscDefaultEvents.java
 *
 * Created on July 8, 2003, 2:53 PM
 */
package bsc.swing;

import bsc.applet.BscMainPanel;
import bsc.xml.*;
import bsc.core.*;
import java.awt.event.ItemListener;
import javax.swing.*;

/**
 *
 * @author  siga1996
 */
public class BscDefaultFrameBehavior {

    public final static int INSERTING = 1;
    public final static int UPDATING = 2;
    public final static int NORMAL = 3;
    public final static int DELETING = 4;
    private BscDefaultFrameFunctions frame;
    private BscDataController dataController;
    private BscDefaultDialogSystem dialogSystem;
    private bsc.xml.BIXMLRecord returnedRecord;

    public void setNoServerMode(boolean nsm) {
        dataController.setNoServeMode(nsm);
    }

    public BscDefaultFrameBehavior(BscDefaultFrameFunctions frameAux) {
        frame = frameAux;
        dataController = bsc.core.BscStaticReferences.getBscDataController();
        dialogSystem = new BscDefaultDialogSystem((JInternalFrame) frame);
    }

    public void defaultConstructor(int operation, String idAux, String contextId) {
        frame.setStatus(operation);
        if (operation == NORMAL) {
            frame.setID(idAux);
            loadRecord();
            frame.disableFields();
            frame.showOperationsButtons();
        } else if (operation == INSERTING) {
            BIXMLRecord record = getRecordStructure(idAux);
            if ((idAux != null) && (!"".equals(idAux))) {
                record.set("PARENTID", idAux);
            }
            if ((contextId != null) && (!"".equals(contextId))) {
                record.set("CONTEXTID", contextId);
            }
            frame.setRecord(record);
            //if ( frame.hasCombos() )
            frame.refreshFields();

            if (frame.getTabbedPane() != null) {
                for (int i = 1; i < frame.getTabbedPane().getTabCount(); i++) {
                    frame.getTabbedPane().setEnabledAt(i, false);
                }
            }
            frame.enableFields();
            frame.showDataButtons();
        }
    }

    public boolean saveRecord() {
        StringBuffer errorMessage = new StringBuffer();
//	    long inicio = System.currentTimeMillis();
        // retorna true se o passar pelo validFields dos Frames
        boolean retorno = frame.validateFields(errorMessage);
        if (retorno) {
            BIXMLRecord record = frame.getFieldsContents();
            BIXMLRecord recordBeforeChanges = frame.getRecord();
            String key = null;
//			System.out.println("BSCDefaultFrameBehavior. Inicio da valida��o dos records"+ (System.currentTimeMillis()-inicio) + "ms");			
            for (java.util.Iterator e = recordBeforeChanges.getKeyNames(); e.hasNext();) {
                key = (String) e.next();
                if (!recordBeforeChanges.getInternalAttributes(key).contains("RETORNA")) {
                    if (!record.contains(key)) {
                        record.set(key, recordBeforeChanges.getString(key));
                    }
                } else {
                    if ((recordBeforeChanges.getInternalAttributes(key).getBoolean("RETORNA"))
                            && (!record.contains(key))) {
                        record.set(key, recordBeforeChanges.getString(key));
                    }
                }
            }
//			System.out.println("BSCDefaultFrameBehavior. Final da valida��o dos records"+ (System.currentTimeMillis()-inicio) + "ms");			

            final bsc.xml.BIXMLRecord record2 = record;

            if (frame.getStatus() == INSERTING) {
                final bsc.core.BscSwingWorker worker = new bsc.core.BscSwingWorker(frameGetThreadTree()) {

                    @Override
                    public Object construct() {
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                        String id = dataController.insertRecord(frame.getType(), record2);
                        return id;
                    }

                    //Runs on the event-dispatching thread.
                    @Override
                    public void finished() {
                        frame.setID((String) get());
                        if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
                            BscStaticReferences.getBscFormController().setNewDetailFormInfo(frame.getType(), frame.getID(), frame);
                            loadRecord();
                        }
                        if (frame.getTabbedPane() != null) {
                            for (int i = 1; i < frame.getTabbedPane().getTabCount(); i++) {
                                frame.getTabbedPane().setEnabledAt(i, true);
                            }
                        }

                        refreshFrameStatus(getThreadTree());
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                        if (frame.asJInternalFrame() instanceof bsc.swing.BscInternalExecute) {
                            ((bsc.swing.BscInternalExecute) frame.asJInternalFrame()).afterInsert();
                        }
                    }
                };
                worker.start();
            } else if (frame.getStatus() == UPDATING) {
                final bsc.core.BscSwingWorker worker = new bsc.core.BscSwingWorker(frameGetThreadTree()) {

                    @Override
                    public Object construct() {
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                        dataController.updateRecord(frame.getType(), record2);
                        return null;
                    }

                    //Runs on the event-dispatching thread.
                    @Override
                    public void finished() {
                        refreshFrameStatus(getThreadTree());
                        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                        if (frame.asJInternalFrame() instanceof bsc.swing.BscInternalExecute) {
                            ((bsc.swing.BscInternalExecute) frame.asJInternalFrame()).afterUpdate();
                        }
                    }
                };
                worker.start();
            }
        } else {
            dialogSystem.warningMessage(errorMessage.toString());
        }
        //System.out.println("BSCDefaultFrameBehavior. Final da grava��o (Save Record)"+ (System.currentTimeMillis()-inicio) + "ms");
        frame.asJInternalFrame().repaint();

        return retorno;
    }

    public void refreshFrameStatus(bsc.swing.BscTree threadTree) {

        if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
            refreshParent();
            frame.asJInternalFrame().repaint();
            frame.showOperationsButtons();
            frame.disableFields();

            synchronizeBscCards(true);

            if (threadTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
                bsc.xml.BIXMLRecord bscXmlTreeSelected = dataController.getNewTree();
                bsc.xml.BIXMLRecord bscXmlNodeActual = BscStaticReferences.getBscMainPanel().getOrgEstTree(bscXmlTreeSelected);
                BscStaticReferences.getBscMainPanel().updateTree(bscXmlNodeActual, threadTree);
            } else {
                //Se o AdvPl nao mandou a arvore entao o usuario desabilitou esta op��o. Desta forma nao atualiza a arvore.
                if (dataController.getNewTree() != null) {

                    BscStaticReferences.getBscMainPanel().updateTree(dataController.getNewTree(), threadTree);

                    /*Tratamento para opera��es realizadas no cadastro de organiza��es.*/
                    if (frame.getType().equals("ORGANIZACAO")) {

                        /*Realiza a atualiza��o e posicionamento do combo de organiza��o.*/
                        if (frame.getStatus() == BscDefaultFrameBehavior.UPDATING) {
                            int line = BscStaticReferences.getBscMainPanel().getComboLine();
                            BscStaticReferences.getBscMainPanel().loadComboOrg();
                            BscStaticReferences.getBscMainPanel().setComboLine(line);

                        } else if (frame.getStatus() == BscDefaultFrameBehavior.INSERTING) {
                            BscStaticReferences.getBscMainPanel().loadComboOrg();
                            BscStaticReferences.getBscMainPanel().selectLastLineCombo();

                        } else if (frame.getStatus() == BscDefaultFrameBehavior.DELETING) {
                            BscStaticReferences.getBscMainPanel().loadComboOrg();
                        }
                    }
                }
            }

        } else {
            // Algum erro retornou da opera��o no servidor
            if (dataController.getStatus() == BscDataController.BSC_ST_UNIQUE) {
                dialogSystem.errorMessage(dataController.getErrorMessage());
            } else if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                dialogSystem.errorMessage(dataController.getErrorMessage());
                frame.setStatus(NORMAL);
                closeOperation();
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
                if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                    BscStaticReferences.getBscApplet().resetApplet();
                }
            }
        }

        frame.setStatus(NORMAL);
    }

    public void closeOperation() {
        try {
            //Tipo da vizualiza��o que est� sendo usada. Janela ou pastas.
            if (!BscStaticReferences.getBscMainPanel().radJanela.isSelected()) {
                javax.swing.JPanel tmpPanel = (javax.swing.JPanel) frame.asJInternalFrame().getClientProperty("tmpPanel");
                tmpPanel.getParent().remove(tmpPanel);
            }
            frame.asJInternalFrame().setClosed(true);
        } catch (java.beans.PropertyVetoException e) {
            bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultFrameBehavior_00002") + frame.getType() + "/"
                    + frame.getID());
            e.printStackTrace();
        }
    }

    public void cancelOperation() {
        if (frame.getStatus() == INSERTING) {
            frame.setStatus(NORMAL);
            closeOperation();
        } else {
            frame.refreshFields();
            frame.asJInternalFrame().repaint();
            frame.showOperationsButtons();
            frame.disableFields();
            frame.setStatus(NORMAL);
        }
    }

    public void refreshParent() {
        BscDefaultFrameFunctions parentFrame =
                BscStaticReferences.getBscFormController().getParentForm(
                frame.getType(), frame.getParentID());
        if (parentFrame != null) {
            parentFrame.loadRecord();
        }
    }

    public void deleteRecord() {
        deleteRecord("");
    }

    public void deleteRecord(String delCMD) {
        if (dialogSystem.confirmDeletion()) {
            final String finalDelCMD = delCMD;
            final String id = frame.getID();
            final String type = frame.getType();

            /*Define o status do frame como deletando.*/
            frame.setStatus(DELETING);

            final bsc.core.BscSwingWorker worker = new bsc.core.BscSwingWorker(frameGetThreadTree()) {

                @Override
                public Object construct() {
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                            java.awt.Cursor.WAIT_CURSOR));
                    dataController.deleteRecord(id, type, finalDelCMD);
                    return null;
                }

                //Runs on the event-dispatching thread.
                @Override
                public void finished() {
                    if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
                        refreshParent();

                        refreshFrameStatus(getThreadTree());

                        closeOperation();

                        try {
                            //Tipo da vizualiza��o que est� sendo usada. Janela ou pastas.
                            if (!BscStaticReferences.getBscMainPanel().radJanela.isSelected()) {
                                BscStaticReferences.getBscFormController().removeDesktopContainer();
                            }
                            frame.asJInternalFrame().setClosed(true);
                        } catch (java.beans.PropertyVetoException e) {
                            dialogSystem.errorMessage(
                                    java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultFrameBehavior_00003"));
                        }
                        synchronizeBscCards(false);
                    } else {
                        if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                            dialogSystem.errorMessage(dataController.getErrorMessage());
                            frame.setStatus(NORMAL);
                            closeOperation();
                        } else {
                            dialogSystem.errorMessage(dataController.getStatus());

                            if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                                BscStaticReferences.getBscApplet().resetApplet();
                            }
                        }
                    }
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                            java.awt.Cursor.DEFAULT_CURSOR));
                    if (frame.asJInternalFrame() instanceof bsc.swing.BscInternalExecute) {
                        ((bsc.swing.BscInternalExecute) frame.asJInternalFrame()).afterDelete();
                    }
                }
            };
            worker.start();
        }
    }

    public void editRecord() {
        frame.setStatus(UPDATING);
        frame.showDataButtons();
        frame.enableFields();
        frame.asJInternalFrame().repaint();
    }

    public void loadRecord() {
        final String id = frame.getID();
        final String type = frame.getType();
        final bsc.core.SwingWorker worker = new bsc.core.SwingWorker() {

            @Override
            public Object construct() {
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                bsc.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type);
                return recordAux;
            }

            @Override
            public void finished() {
                frame.setRecord((bsc.xml.BIXMLRecord) get());

                if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
                    frame.refreshFields();
                    frame.asJInternalFrame().repaint();
                } else {
                    if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                        dialogSystem.errorMessage(dataController.getErrorMessage());
                    } else {
                        dialogSystem.errorMessage(dataController.getStatus());
                    }

                    if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                        BscStaticReferences.getBscApplet().resetApplet();
                    }
                }
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                        java.awt.Cursor.DEFAULT_CURSOR));
            }
        };
        worker.start();
    }

    public BIXMLRecord getRecordStructure(String parentId) {
        // Aqui
        BIXMLRecord recordAux = dataController.getRecordStructure(frame.getType(), parentId);

        if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
            return recordAux;
        } else {
            dialogSystem.errorMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultFrameBehavior_00005") + frame.getType() + ".");

            if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                BscStaticReferences.getBscApplet().resetApplet();
            }

            return null;
        }
    }

    /**
     * Retorna o Item da combo box, passando o codigo do item. ("ID")
     * @param vctItens Vector com os dados da combo.
     * @param keyCode Chave para procurar
     * @param firstItemValid Indica se o primeiro item da combo � branco.
     * @return int indicando a possi��o do item, -1 Indica que o item n�o foi encontrado.
     */
    public int getComboItem(bsc.xml.BIXMLVector vctItens, String keyCode, boolean firstItemValid) {
        for (int item = 0; item < vctItens.size(); item++) {
            if (vctItens.get(item).contains("ID") && vctItens.get(item).getString("ID").equals(keyCode)) {
                if (firstItemValid) {
                    return item++;
                } else {
                    return item;
                }
            }
        }
        return -1;
    }

    public String getComboValue(java.util.Hashtable table, javax.swing.JComboBox combo) {
        if ((combo.getSelectedItem() != null)
                && (table.get((String) combo.getSelectedItem()) != null)) {
            return String.valueOf(table.get((String) combo.getSelectedItem()));
        } else {
            return String.valueOf("0");
        }
    }

    public void populateCombo(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo) {
        populateCombo(vector, keyName, table, combo, null, "");
    }

    public void populateCombo(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, String fieldName) {
        populateCombo(vector, keyName, table, combo, null, fieldName);
    }

    public void populateCombo(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, java.util.Hashtable ignoreIDList) {
        populateCombo(vector, keyName, table, combo, ignoreIDList, "");
    }

    public void populateCombo(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo, java.util.Hashtable ignoreIDList, String fieldName) {
        java.awt.event.ItemListener[] items = combo.getItemListeners();
        for (int i = 0; i < items.length; i++) {
            combo.removeItemListener(items[i]);
        }

        table.clear();
        combo.removeAllItems();
        combo.addItem("");
        //se n�o especificar o campo a ser carregado no combo, ser� o campo NOME da tabela
        if (fieldName.equals("")) {
            fieldName = "NOME";
        }

        for (int i = 0; i < vector.size(); i++) {
            table.put(vector.get(i).getString(fieldName), vector.get(i).getString("ID"));

            if (ignoreIDList != null) {
                if (!ignoreIDList.containsKey(new Integer(vector.get(i).getInt(("ID"))))) {
                    combo.addItem(vector.get(i).getString(fieldName));
                }
            } else {
                combo.addItem(vector.get(i).getString(fieldName));
            }
        }

        if (frame.getRecord().contains(keyName)) {
            for (int i = 0; i < vector.size(); i++) {
                if (vector.get(i).getInt("ID") == frame.getRecord().getInt(keyName)) {
                    combo.setSelectedItem(vector.get(i).getString(fieldName));
                }
            }
        }

        for (int i = 0; i < items.length; i++) {
            combo.addItemListener(items[i]);
        }

        return;
    }

    public void populateComboWithName(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo) {
        java.awt.event.ItemListener[] items = combo.getItemListeners();
        for (int i = 0; i < items.length; i++) {
            combo.removeItemListener(items[i]);
        }

        table.clear();
        combo.removeAllItems();
        combo.addItem("");

        for (int i = 0; i < vector.size(); i++) {
            table.put(vector.get(i).getString("NOME"),
                    vector.get(i).getString("ID"));
            combo.addItem(vector.get(i).getString("NOME"));
        }

        if (frame.getRecord().contains(keyName)) {
            for (int i = 0; i < vector.size(); i++) {
                if (vector.get(i).getString("NOME").equals(frame.getRecord().getString(keyName))) {
                    combo.setSelectedItem(vector.get(i).getString("NOME"));
                } else {
                    javax.swing.JTextField ed = (javax.swing.JTextField) ((javax.swing.plaf.basic.BasicComboBoxEditor) combo.getEditor()).getEditorComponent();
                    ed.setText(frame.getRecord().getString(keyName));
                }
            }
        }

        for (int i = 0; i < items.length; i++) {
            combo.addItemListener(items[i]);
        }

        return;
    }

    // Fernando - 04/12/03
    // Lucio    - 21/05/09
    public void executeRecord(String execCMD) {
        final String id = frame.getID();
        final String type = frame.getType();

        // Verifica opera��es execcmd envolvidas
        final boolean saveBeforeExecute = execCMD.startsWith("SALVAR;");
        if (saveBeforeExecute) {
            execCMD = execCMD.substring(7);
        }
        final String finalExecCMD = execCMD;

        // Save
        StringBuffer errorMessage = new StringBuffer();
        if (frame.validateFields(errorMessage)) {
            BIXMLRecord record = frame.getFieldsContents();
            BIXMLRecord recordBeforeChanges = frame.getRecord();
            String key = null;

            for (java.util.Iterator e = recordBeforeChanges.getKeyNames(); e.hasNext();) {
                key = (String) e.next();
                if (!recordBeforeChanges.getInternalAttributes(key).contains("RETORNA")) {
                    if (!record.contains(key)) {
                        record.set(key, recordBeforeChanges.getString(key));
                    }
                } else {
                    if ((recordBeforeChanges.getInternalAttributes(key).getBoolean("RETORNA"))
                            && (!record.contains(key))) {
                        record.set(key, recordBeforeChanges.getString(key));
                    }
                }
            }

            final bsc.xml.BIXMLRecord record2 = record;
            final bsc.core.SwingWorker worker = new bsc.core.SwingWorker() {

                @Override
                public Object construct() {
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                    if (saveBeforeExecute) {
                        dataController.updateRecord(type, record2);
                    }
                    dataController.executeRecord(id, type, finalExecCMD);
                    return id;
                }

                @Override
                public void finished() {
                    if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
                        frame.asJInternalFrame().repaint();
                        if (dataController.getErrorMessage().trim().length() > 0) {
                            dialogSystem.informationMessage(dataController.getErrorMessage());
                        }
                    } else {
                        if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                            dialogSystem.errorMessage(dataController.getErrorMessage());
                        } else {
                            dialogSystem.errorMessage(dataController.getStatus());
                        }

                        if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                            BscStaticReferences.getBscApplet().resetApplet();
                        }
                    }
                    frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
                    if (frame.asJInternalFrame() instanceof bsc.swing.BscInternalExecute) {
                        ((bsc.swing.BscInternalExecute) frame.asJInternalFrame()).afterExecute();
                    }
                    if (saveBeforeExecute) {
                        editRecord();
                    }

                }
            };
            worker.start();
        } else {
            // Erro na opera��o
            dialogSystem.errorMessage(errorMessage.toString());
        }
    }

    public void loadHelp(final String helpCMD) {
        final String id = frame.getID();
        final String type = frame.getType();
        final bsc.core.SwingWorker worker = new bsc.core.SwingWorker() {

            @Override
            public Object construct() {
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                bsc.xml.BIXMLRecord recordAux = dataController.loadHelp(type, helpCMD);
                return recordAux;
            }

            @Override
            public void finished() {
                bsc.xml.BIXMLRecord record = (bsc.xml.BIXMLRecord) get();
                if (dataController.getStatus() == BscDataController.BSC_ST_OK) {
                    try {
                        int index;
                        String url = record.getString("URL");
                        java.net.URL helpURL = new java.net.URL(url);
                        bsc.core.BscStaticReferences.getBscApplet().getAppletContext().showDocument(helpURL, "_blank");
                    } catch (java.net.MalformedURLException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                        dialogSystem.errorMessage(dataController.getErrorMessage());
                    } else {
                        dialogSystem.errorMessage(dataController.getStatus());
                    }

                    if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                        BscStaticReferences.getBscApplet().resetApplet();
                    }
                }
                frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(
                        java.awt.Cursor.DEFAULT_CURSOR));
            }
        };
        worker.start();
    }

    // Load record customizada para retornar o resultado imediatamente
    public bsc.xml.BIXMLRecord loadRecord(final String id, final String type) {
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        bsc.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != BscDataController.BSC_ST_OK) {
            if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                dialogSystem.errorMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                BscStaticReferences.getBscApplet().resetApplet();
            }
        }
        return recordAux;
    }

    public void saveBase64(final String fileName, final String fileContent) {
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        dataController.saveBase64(fileName, fileContent);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != BscDataController.BSC_ST_OK) {
            if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                dialogSystem.errorMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                BscStaticReferences.getBscApplet().resetApplet();
            }
        }
    }

    /**
     * Load record customizada para retornar o resultado imediatamente
     *
     * @param      id	    Uma <code>String</code> que contendo o valor do registro que ser� carregado.
     * @param	   type	    Uma <code>String</code> com o nome do tipo do registro que sera carregado. 
     * @param      cLoadCmd Uma <code>String</code> com os valores dos parametros separados por ;
     */
    public bsc.xml.BIXMLRecord loadRecord(final String id, final String type, final String cLoadCmd) {

        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
        bsc.xml.BIXMLRecord recordAux = dataController.loadRecord(id, type, cLoadCmd);
        frame.asJInternalFrame().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        if (dataController.getStatus() != BscDataController.BSC_ST_OK) {
            if (dataController.getStatus() == BscDataController.BSC_ST_GENERALERROR) {
                dialogSystem.errorMessage(dataController.getErrorMessage());
            } else {
                dialogSystem.errorMessage(dataController.getStatus());
            }
            if (dataController.getStatus() == BscDataController.BSC_ST_EXPIREDSESSION) {
                BscStaticReferences.getBscApplet().resetApplet();
            }
        }
        return recordAux;
    }

    private void synchronizeBscCards(boolean isUpdate) {
        javax.swing.JTree sync_Tree = null; //Arvore que est� sendo sincronizada.
        bsc.core.BscFormController sync_FormController = null;//Form controller que est� sendo sincrosizado.
        bsc.swing.BscDefaultFrameFunctions sync_Frame = null;

        //Tratamento para atualiza��o da outra �rvore aberta.
        if (BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
            sync_Tree = BscStaticReferences.getBscMainPanel().bscTreeOrganizacao;
            sync_FormController = BscStaticReferences.getBscMainPanel().bscFormControllerOrganizacao;
            sync_Tree.setAnchorSelectionPath(null);
            //Verifica se o form atual est� aberto em outra �rea.
            sync_Frame = sync_FormController.isFormOpen(frame.getType(), frame.getID());
        } else if (BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().equals(BscMainPanel.ST_ORGANIZACAO)) {
            sync_Tree = BscStaticReferences.getBscMainPanel().bscTreeAreaTrabalho;
            sync_FormController = BscStaticReferences.getBscMainPanel().bscFormControllerAreaTrabalho;
            sync_Tree.setAnchorSelectionPath(null);
            //Verifica se o form atual est� aberto em outra �rea.
            sync_Frame = sync_FormController.isFormOpen(frame.getType(), frame.getID());
        }

        if (sync_Frame != null) {
            if (isUpdate) {
                sync_Frame.loadRecord();
            } else {
                if (sync_FormController.lastView == BscFormController.ST_JANELA) {
                    try {
                        sync_Frame.asJInternalFrame().setClosed(true);
                    } catch (java.beans.PropertyVetoException e) {
                        bsc.core.BscDebug.println(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDefaultFrameBehavior_00002") + sync_Frame.getType() + "/"
                                + sync_Frame.getID());
                        e.printStackTrace();
                    }
                } else {
                    javax.swing.JPanel tmpPanel = (javax.swing.JPanel) sync_Frame.asJInternalFrame().getClientProperty("tmpPanel");
                    tmpPanel.getParent().remove(tmpPanel);
                    String name = new String(frame.getType() + " - " + frame.getID());
                    sync_FormController.removeFromForms(name);

                    //Verifica se � o �ltimo painel para remo��o do container.
                    sync_FormController.removeDesktopContainer();
                }
            }
        }
    }

    private bsc.swing.BscTree frameGetThreadTree() {
        String type = bsc.core.BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType();
        javax.swing.JTree threadTree = null;

        if (type.equals(BscMainPanel.ST_AREATRABALHO)) {
            threadTree = bsc.core.BscStaticReferences.getBscMainPanel().bscTreeAreaTrabalho;
        } else if (type.equals(BscMainPanel.ST_ORGANIZACAO)) {
            threadTree = bsc.core.BscStaticReferences.getBscMainPanel().bscTreeOrganizacao;
        } else {
            threadTree = bsc.core.BscStaticReferences.getBscMainPanel().bscTreeUsuarios;
        }
        return (bsc.swing.BscTree) threadTree;
    }

    /**
     * Seleciona um indice em um JComboBox.
     * @param combo ComboBox que ter� item selecionado.
     * @param item  indice a ser selecionado.
     */
    public void selectComboItem(JComboBox combo, int indice) {
        selectComboItem(combo, indice, true);
    }

    /**
     * Seleciona um indice em um JComboBox.
     * @param combo ComboBox que ter� item selecionado.
     * @param item  indice a ser selecionado.
     * @param ignoraListener Indica se o listener dever� ser ignorado durante o posionamento.
     */
    public void selectComboItem(JComboBox combo, int indice, boolean ignoraListener) {
        ItemListener[] items = combo.getItemListeners();

        //Remove os item listeners.
        if (ignoraListener) {
            for (int i = 0; i < items.length; i++) {
                combo.removeItemListener(items[i]);
            }
        }

        //Posiciona no item desejado.
        if (combo.getItemCount() > 0) {
            if ((indice != -1) && (indice < combo.getItemCount())) {
                combo.setSelectedIndex(indice);
            } else {
                combo.setSelectedIndex(0);
            }
        }

        //Insere os itens listeners.
        if (ignoraListener) {
            for (int i = 0; i < items.length; i++) {
                combo.addItemListener(items[i]);
            }
        }
    }
}
