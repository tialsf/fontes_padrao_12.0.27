/*
 * BscDefaultFrameFunctions.java
 *
 * Created on July 8, 2003, 2:53 PM
 */

package bsc.swing;

/**
 *
 * @author  siga1996
 */

import bsc.xml.BIXMLRecord;

public interface BscDefaultFrameFunctions {
	public void enableFields();
	public void disableFields();
	public void refreshFields();
	public BIXMLRecord getFieldsContents();
	
	public void loadRecord();
	
	public void showOperationsButtons();
	public void showDataButtons();
	public void setStatus( int value );
	public int getStatus();
	public void setID( String value );	
	public String getID( );
	public String getParentID( );
	public void setType( String value );	
	public String getType( );
	public void setRecord( bsc.xml.BIXMLRecord recordAux );	
	public BIXMLRecord getRecord( );
	
	public boolean validateFields(StringBuffer errorMessage);
	public boolean hasCombos();
	public javax.swing.JInternalFrame asJInternalFrame();
	public javax.swing.JTabbedPane getTabbedPane();
}
