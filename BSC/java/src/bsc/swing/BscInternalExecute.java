/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bsc.swing;

/**
 *
 * @author Lucio
 */
public abstract class BscInternalExecute  extends javax.swing.JInternalFrame{
    public abstract void afterExecute();
    public abstract void afterUpdate();
    public abstract void afterInsert();
    public abstract void afterDelete();
}

