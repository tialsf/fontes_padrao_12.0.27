/*
 * BscRegraTreeNode.java
 *
 /*
 * BscRegraTreeNode.java
 *
 * Created on 7 de Abril de 2004, 11:28
 */

package bsc.swing;

/**
 *
 * @author  siga1645
 */
public class BscRegraTreeNode extends bsc.swing.BscTreeNode {
	
	private bsc.xml.BIXMLRecord record;
	
	/** Creates a new instance of BscRegraTreeNode */
	public BscRegraTreeNode( String typeAux, String idAux, String nameAux, bsc.xml.BIXMLRecord record) {
		super( typeAux, idAux, nameAux );

		// Aponta para o registro XML que originou esses chkboxes
		setRecord(record);
	}
	
	/** Getter for property record.
	 * @return Value of property record.
	 *
	 */
	public bsc.xml.BIXMLRecord getRecord() {
		return record;
	}
	
	/** Setter for property record.
	 * @param record New value of property record.
	 *
	 */
	public void setRecord(bsc.xml.BIXMLRecord record) {
		this.record = record;
	}
}