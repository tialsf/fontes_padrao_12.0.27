/*
 * BscReuniaoTree.java
 *
 * Created on 30 de Agosto de 2004, 16:03
 */

package bsc.swing;

/**
 * Criar o TreeNode de Reuni�es com Id, Tipo de Elemento, Nome, Organizacao e Estrategia
 * herdado de BscTreeNode
 * @author  siga0739
 */
public class BscReuniaoTreeNode extends BscTreeNode {
    
    /** Creates a new instance of BscReuniaoTree */
    private String organizacao = new String();
    private String estrategia = new String();

    public BscReuniaoTreeNode(String typeAux, String idAux, String nameAux, String orgAux, String estAux) {

	super( typeAux, idAux, nameAux);
	organizacao = orgAux;
	estrategia = estAux;
    }
    
    public String getOrganizacao(){
	return new String ( organizacao );
    }

    public String getEstrategia(){
	return new String ( estrategia );
    }

}
