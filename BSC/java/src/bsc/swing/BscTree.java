/*
 * BscTree.java
 *
 * Created on 3 de Novembro de 2004, 09:46
 */
package bsc.swing;

/**
 *
 * @author  alexandreas
 */
public class BscTree extends javax.swing.JTree {

    private String treeType;
    private String treeItemSelected;

    public BscTree() {
        super();
    }

    /** Creates a new instance of BscTree */
    public BscTree(String treeType) {
        super();
        this.treeType = treeType;
    }

    public void setBscTreeType(String treeType) {
        this.treeType = treeType;
    }

    public String getBscTreeType() {
        return this.treeType;
    }

    public void setBscTreeItemSelected(String treeItemSelected) {
        this.treeItemSelected = treeItemSelected;
    }

    public String getBscTreeItemSelected() {
        if (this.treeItemSelected == null) {
            this.treeItemSelected = "0";
        }
        return this.treeItemSelected;
    }
}
