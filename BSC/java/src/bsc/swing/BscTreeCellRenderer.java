/*
 * BscTreeCellRenderer.java
 *
 * Created on September 30, 2003, 2:16 PM
 */

package bsc.swing;

/**
 *
 * @author  siga1996
 */
public class BscTreeCellRenderer extends javax.swing.tree.DefaultTreeCellRenderer {
	
	
	private bsc.core.BscImageResources imageResources;
	
	public BscTreeCellRenderer(bsc.core.BscImageResources imageResources) {
		this.imageResources = imageResources;
	}
	
	public java.awt.Color getBackground() {
		return null;
	}
	
	public java.awt.Component getTreeCellRendererComponent(javax.swing.JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent( tree, value, sel, expanded, leaf, row, hasFocus);
		
		javax.swing.tree.DefaultMutableTreeNode nodeAux = (javax.swing.tree.DefaultMutableTreeNode) value;
		if ( nodeAux.getUserObject().getClass().getName().equals("bsc.swing.BscTreeNode") ) {
			bsc.swing.BscTreeNode node = (bsc.swing.BscTreeNode) nodeAux.getUserObject();

			if(node.getType().equals( "ORGANIZACAO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_ORGANIZACAO));
			else if(node.getType().equals( "ESTRATEGIA" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_ESTRATEGIA));
			else if(node.getType().equals( "PERSPECTIVA" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_PERSPECTIVA));
			else if(node.getType().equals( "PESSOA" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_PESSOA));
			else if(node.getType().equals( "OBJETIVO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_OBJETIVO));
			else if(node.getType().equals( "INDICADOR" ))
                if(node.getTipoIndicador().trim().equals("T")){
    				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_TENDENCIA));
                }else{
    				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_INDICADOR));
                }
			else if(node.getType().equals( "INICIATIVA" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_INICIATIVA));
			else if(node.getType().equals( "USUARIO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_USUARIO));
			else if(node.getType().equals( "GRUPO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_GRUPO));
			else if(node.getType().equals( "REUNIAO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_REUNIAO));
			else if(node.getType().equals( "DIRUSUARIOS" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_DIRUSUARIO));
			else if(node.getType().equals( "TAREFA" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_TAREFA));
			else if(node.getType().equals( "META" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_META));
			else if(node.getType().equals( "MAPAEST" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_MAPAEST));
			else if(node.getType().equals( "AGENDADOR" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_AGENDADOR));
			else if(node.getType().equals( "AGENDAMENTO" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_AGENDAMENTO));
			else if(node.getType().equals( "SMTPCONF" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_SMTPCONF));
			else if(node.getType().equals( "DESKTOP" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_DESKTOP));
			else if(node.getType().equals( "MENSAGENS_RECEBIDAS" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_EMAILRECIVED));
			else if(node.getType().equals( "MENSAGENS_ENVIADAS" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_EMAILSENDED));
			else if(node.getType().equals( "MENSAGENS_EXCLUIDAS" ))
				setIcon(imageResources.getImage(bsc.core.BscImageResources.BSC_IM_EMAILDELETED));
			
			//setBackground(null);
			setBackgroundNonSelectionColor(null);
			setOpaque(false);
			// setToolTipText("Organizações");
		}
		
		return this;
	}
}
