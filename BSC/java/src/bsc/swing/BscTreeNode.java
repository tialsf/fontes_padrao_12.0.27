/*
 * BscTreeNode.java
 *
 * Created on June 6, 2003, 3:25 PM
 */

package bsc.swing;

/**
 *
 * @author  siga1996
 */
public class BscTreeNode extends java.lang.Object{
	private String type = new String();
	private String id = new String();
	private String name = new String();
	private String tipoIndicador = new String();
	private String treeID = new String();
	
	public boolean equals( Object obj ) {
		BscTreeNode node = (BscTreeNode) obj;
		
		if  ( ( type.equals( node.getType() ) ) &&
		( id.equals( node.getID() ) )  ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String getTreeID(){
		return treeID;
	}
	
	public void setTreeID(String id){
		treeID = id;
	}
	
	public void setID(String ID){
		id = ID;
	}
	public int hashCode() {
		int code = 0;
		
		String key = new String(type + id);
		
		for ( int i = 0; i < key.length(); i++ )
			code = code + java.lang.Character.getNumericValue(key.charAt(i));
		
		return code;
	}
	
	public BscTreeNode( String typeAux, String idAux, String nameAux ) {
		type = typeAux;
		id = idAux;
		name = nameAux;

	}
	
	public BscTreeNode( String typeAux, String idAux, String nameAux, String idTree ) {
		type = typeAux;
		id = idAux;
		name = nameAux;
		this.setTreeID(idTree);
	}

	public BscTreeNode( String typeAux, String idAux, String nameAux, String idTree, String tipoAux ) {
        tipoIndicador = tipoAux;
		type = typeAux;
		id = idAux;
		name = nameAux;
		this.setTreeID(idTree);
	}
	
	public String getType() {
		return new String( type );
	}
	
	public String getID() {
		return new String( id );
	}
	
	public String getName() {
		return new String( name );
	}
	
	public String getTipoIndicador() {
		return new String( tipoIndicador );
	}

    public String toString() {
		return name;
	}
	
}
