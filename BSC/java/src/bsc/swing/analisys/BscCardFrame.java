/*
 * BscCardFrame.java
 *
 * Created on October 27, 2003, 11:03 AM
 */

package bsc.swing.analisys;

/**
 *
 * @author  siga1996
 */
public interface BscCardFrame {
	public int getWidth();
	public int getHeight();
	public void setLocation(int x, int y);	
	public java.awt.Point getLocation();
	public void setOrder( int o );
	public java.awt.Point getCardFramePosition();
	public int getOrder();
	public void setSliderEnabled( boolean se );
	public boolean getSliderEnabled();
	public String getNome();
	public bsc.xml.BIXMLRecord getRecordData();
}
