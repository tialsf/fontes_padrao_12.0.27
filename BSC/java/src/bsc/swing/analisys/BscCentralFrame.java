package bsc.swing.analisys;

import bsc.swing.map.BscMapaMaximize;


public class BscCentralFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("CENTRAL");
	private BscMapaMaximize maximizeForm;
	public boolean isMaximize		= false;
	private java.util.Vector vectorAux = null;
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		//lblNome.setEnabled(true);
		//fldNome.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		//lblNome.setEnabled(false);
		//fldNome.setEnabled(false);
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		// Ex.:
		// strategyList.setDataSource(
		//					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
		//fldNome.setText(record.getString("NOME") );
		bsc.xml.BIXMLVector perspectivas = record.getBIXMLVector( "PERSPECTIVAS" );
		bsc.xml.BIXMLRecord perspectiva = null;
		fldDataAlvo.setCalendar( record.getDate("DATAALVO") );
		if(record.getBoolean("PARCELADA"))
			fldAnalise.setText("Parcelada");
		else
			fldAnalise.setText("Acumulada");
		
		BscCardsPanel cardsPanel = null;
		pnlData.removeAll();
		vectorAux = new java.util.Vector();
		for ( int i = 0; i < perspectivas.size(); i++ ) {
			perspectiva = perspectivas.get(i);
			cardsPanel = new BscCardsPanel();
			pnlData.add( cardsPanel );
			scpRecord.validate();
			cardsPanel.includeFrames(perspectiva);
			vectorAux.add( cardsPanel );
		}
		for ( int j = 0; j < vectorAux.size(); j++ )
			((BscCardsPanel)vectorAux.get(j)).autoOrganizeFrames();
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		//recordAux.set( "NOME", fldNome.getText() );
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMainPanel = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        pnlData = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReload = new javax.swing.JButton();
        btnMaximize = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlAnalise = new javax.swing.JPanel();
        pnlAnalise1 = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        lblAnalise = new javax.swing.JLabel();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscCentralFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlMainPanel.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                pnlMainPanelAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlData.setLayout(new javax.swing.BoxLayout(pnlData, javax.swing.BoxLayout.LINE_AXIS));
        jPanel1.add(pnlData, java.awt.BorderLayout.CENTER);

        jPanel2.setMinimumSize(new java.awt.Dimension(5, 5));
        jPanel2.setPreferredSize(new java.awt.Dimension(5, 5));
        jPanel1.add(jPanel2, java.awt.BorderLayout.WEST);

        jPanel3.setPreferredSize(new java.awt.Dimension(5, 5));
        jPanel1.add(jPanel3, java.awt.BorderLayout.EAST);

        scpRecord.setViewportView(jPanel1);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);
        scpRecord.getAccessibleContext().setAccessibleParent(pnlRecord);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        pnlMainPanel.add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(543, 70));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setAlignmentX(0.0F);
        pnlOperation.setAlignmentY(0.0F);
        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(272, 27));
        pnlOperation.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 3, 2));

        tbInsertion.setBorder(null);
        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setAlignmentX(0.0F);
        tbInsertion.setMaximumSize(new java.awt.Dimension(272, 27));
        tbInsertion.setPreferredSize(new java.awt.Dimension(255, 30));

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscCentralFrame_00002")); // NOI18N
        btnReload.setAlignmentY(0.0F);
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_maximizar.gif"))); // NOI18N
        btnMaximize.setText(bundle.getString("BscGraphFrame_00034")); // NOI18N
        btnMaximize.setAlignmentY(0.0F);
        btnMaximize.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMaximize.setMaximumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setMinimumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setPreferredSize(new java.awt.Dimension(76, 20));
        btnMaximize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaximizeActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMaximize);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setAlignmentY(0.0F);
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlAnalise.setPreferredSize(new java.awt.Dimension(271, 27));
        pnlAnalise.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 3, 4));
        pnlTools.add(pnlAnalise, java.awt.BorderLayout.EAST);

        pnlAnalise1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlAnalise1.setPreferredSize(new java.awt.Dimension(271, 45));
        pnlAnalise1.setLayout(null);

        lblDataAlvo.setText(bundle.getString("BscCentralFrame_00003")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlAnalise1.add(lblDataAlvo);
        lblDataAlvo.setBounds(20, 0, 96, 20);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        pnlAnalise1.add(fldDataAlvo);
        fldDataAlvo.setBounds(20, 20, 180, 22);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        pnlAnalise1.add(btnAvaliacao);
        btnAvaliacao.setBounds(210, 20, 26, 22);

        lblAnalise.setText(bundle.getString("BscCentralFrame_00004")); // NOI18N
        lblAnalise.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlAnalise1.add(lblAnalise);
        lblAnalise.setBounds(260, 0, 50, 20);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        pnlAnalise1.add(fldAnalise);
        fldAnalise.setBounds(260, 20, 180, 22);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        pnlAnalise1.add(btnAnalise);
        btnAnalise.setBounds(450, 20, 26, 22);

        pnlTools.add(pnlAnalise1, java.awt.BorderLayout.SOUTH);

        pnlMainPanel.add(pnlTools, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void pnlMainPanelAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_pnlMainPanelAncestorAdded
		pnlFields.revalidate();// TODO adicione seu c�digo de manipula��o aqui:
	}//GEN-LAST:event_pnlMainPanelAncestorAdded
	
	private void btnMaximizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaximizeActionPerformed
		if(! isMaximize){
			btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00035"));
			btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_minimizar.gif")));
			isMaximize = true;
			btnReload.setEnabled(false);
			btnAnalise.setEnabled(false);
			btnAvaliacao.setEnabled(false);
			setCardsEnable(false);
			maximizeForm = new BscMapaMaximize(bsc.core.BscStaticReferences.getBscMainPanel(),true,pnlMainPanel);
			maximizeForm.setCentralFrame(this);
			maximizeForm.setVisible(true);
			
		}else{
			btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00034"));
			btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_maximizar.gif")));
			isMaximize = false;
			setCardsEnable(true);
			maximizeForm.setVisible(false);
			maximizeForm.dispose();
			this.getContentPane().add(maximizeForm.pnlMapa,java.awt.BorderLayout.CENTER);
			this.doLayout();
		}
		
	}//GEN-LAST:event_btnMaximizeActionPerformed
	
	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
		bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
				new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
		bscPerformanceCard.setVisible(true);
	}//GEN-LAST:event_btnAvaliacaoActionPerformed
	
    private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
		bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
				new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
		bscAnaliseParceladaCard.setVisible(true);
    }//GEN-LAST:event_btnAnaliseActionPerformed
	
	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("ANALISE");
	}//GEN-LAST:event_btnReload1ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    public javax.swing.JButton btnMaximize;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblAnalise;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JPanel pnlAnalise;
    private javax.swing.JPanel pnlAnalise1;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
			new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	private PainelGeralGlassPane myGlassPane = null;
	
	public BscCentralFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		//myGlassPane = new PainelGeralGlassPane( this.pnlData, this.getContentPane() );
		//this.getRootPane().setGlassPane( myGlassPane );
		//myGlassPane.setVisible( false );
		
		event.setNoServerMode(false);
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return null;
	}
	
	public void setCardsEnable(boolean enable){
		for ( int j = 0; j < vectorAux.size(); j++ ){
			java.util.Vector<BscObjetivoCardFrame> vctCards = ((BscCardsPanel)vectorAux.get(j)).getObjetivoCards();
			for(java.util.Iterator<BscObjetivoCardFrame> cards = vctCards.iterator();cards.hasNext();){
				BscObjetivoCardFrame card = cards.next();
				card.setClickEnable(enable);
			}
		}
	}
}

class PainelGeralGlassPane extends javax.swing.JComponent {
	PainelGeralListener listener;
	public PainelGeralGlassPane(javax.swing.JPanel panel, java.awt.Container contentPane) {
		listener = new PainelGeralListener(panel, this, contentPane);
		addMouseListener(listener);
		addMouseMotionListener(listener);
	}
}

class PainelGeralListener extends javax.swing.event.MouseInputAdapter {
	javax.swing.JPanel panel;
	PainelGeralGlassPane glassPane;
	java.awt.Container contentPane;
	
	public PainelGeralListener(javax.swing.JPanel panel,
			PainelGeralGlassPane glassPane,
			java.awt.Container contentPane) {
		this.panel = panel;
		this.glassPane = glassPane;
		this.contentPane = contentPane;
	}
	
	public void mouseMoved(java.awt.event.MouseEvent e) {
		redispatchMouseEventDirectly(e);
	}
	
	public void mouseDragged(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
	
	public void mouseClicked(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
	
	public void mouseEntered(java.awt.event.MouseEvent e) {
		redispatchMouseEventDirectly(e);
	}
	
	public void mouseExited(java.awt.event.MouseEvent e) {
		redispatchMouseEventDirectly(e);
	}
	
	public void mousePressed(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
	
	public void mouseReleased(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
	
	private void redispatchMouseEvent(java.awt.event.MouseEvent e) {
		java.awt.Point panelPoint = javax.swing.SwingUtilities.convertPoint( glassPane, e.getPoint(), panel);
		
		if ((panelPoint.getX() < 0 ) || ( panelPoint.getY() < 0 ) ||
				(panelPoint.getX() >= panel.getWidth() ) ||
				(panelPoint.getY() >= panel.getHeight() ) ) {
			redispatchMouseEventDirectly( e );
		} else
			return;
	}
	
	private void redispatchMouseEventDirectly( java.awt.event.MouseEvent e ) {
		java.awt.Point containerPoint = javax.swing.SwingUtilities.convertPoint( glassPane, e.getPoint(), contentPane);
		java.awt.Component component = javax.swing.SwingUtilities.getDeepestComponentAt(contentPane, containerPoint.x, containerPoint.y);
		
		if ( component == null )
			return;
		else {
			java.awt.Point componentPoint = javax.swing.SwingUtilities.convertPoint( glassPane, e.getPoint(), component);
			component.dispatchEvent(new java.awt.event.MouseEvent(component,
					e.getID(),
					e.getWhen(),
					e.getModifiers(),
					componentPoint.x,
					componentPoint.y,
					e.getClickCount(),
					e.isPopupTrigger()));
		}
	}
}