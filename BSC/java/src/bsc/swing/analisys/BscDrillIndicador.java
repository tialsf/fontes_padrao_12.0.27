package bsc.swing.analisys;

public class BscDrillIndicador extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("DRILLIND");

	private String contextId = new String("0");

	public void enableFields() {
	}
	
	public void disableFields() {
	}
	
	private java.util.Hashtable hstMedidas = new java.util.Hashtable();
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		// Ex.:
		// strategyList.setDataSource(
		//					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );

		fldDataAlvo.setCalendar( record.getDate("DATAALVO") );
        txtDescricao.setText(record.getString("DESCRICAO"));
        txtMetrica.setText(record.getString("METRICA"));
        fldResponsavel.setText(record.getString("RESPONSAVEL"));
        txtFrequencia.setText(record.getString("FREQUENCIA"));
		if(record.getBoolean("PARCELADA"))
			fldAnalise.setText("Parcelada");
		else
			fldAnalise.setText("Acumulada");
		
		this.contextId = record.getString("CONTEXTID");

		bsc.xml.BIXMLVector cards = record.getBIXMLVector("CARDS");
		dskDrillDown.removeAll();
		BscIndicadorCardFrame jifPainel = null;
		BscIndicadorCardFrame jifCentral = null;

		pnlCentro.removeAll();
		for ( int i = 0; i < cards.size(); i++ ) {
            bsc.xml.BIXMLRecord rec = cards.get( i );
			if ( rec.getBoolean("VISIVEL") ) {
                    if (rec.getString("TIPOIND").trim().equals("T")){
                        jifPainel = new BscIndicadorCardFrame( rec );
                        dskDrillDown.add( jifPainel );
                        jifPainel.setClosable(false);
                        jifPainel.show();
                    }
                    else
                    {   // indicador de Resultado
                        jifCentral = new BscIndicadorCardFrame( rec );
                        pnlCentro.add( jifCentral, -1);
                        jifCentral.setClosable(false);
                        jifCentral.show();
                    }
            }
		}
		dskDrillDown.autoOrganizeFrames();
        pnlHeader.setComponents(record.getBIXMLVector("CONTEXTOS"));
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return true;
	}
	
	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlCentral = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        pnlCards = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dskDrillDown = new bsc.beans.JBICardDesktopPane();
        pnlTitle = new javax.swing.JPanel();
        lblTendencia = new javax.swing.JLabel();
        pnlIndicador = new javax.swing.JPanel();
        pnlLayout = new javax.swing.JPanel();
        pnlTitulos = new javax.swing.JPanel();
        lblDescricao = new javax.swing.JLabel();
        lblFrequencia = new javax.swing.JLabel();
        lblResponsavel = new javax.swing.JLabel();
        lblMetrica = new javax.swing.JLabel();
        pnlFields = new javax.swing.JPanel();
        jPnlNorte = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        jPnlCentro = new javax.swing.JPanel();
        txtFrequencia = new pv.jfcx.JPVEdit();
        fldResponsavel = new pv.jfcx.JPVEdit();
        jPnlSul = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtMetrica = new bsc.beans.JBITextArea();
        panelLeft = new javax.swing.JPanel();
        pnlCentro = new javax.swing.JDesktopPane();
        pnlTopo = new javax.swing.JPanel();
        lblIndicador = new javax.swing.JLabel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReload = new javax.swing.JButton();
        btnMapaEstrategico = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlHeaderTop = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Drill-Down");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRecord.setPreferredSize(new java.awt.Dimension(540, 355));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlCentral.setPreferredSize(new java.awt.Dimension(0, 157));
        pnlCentral.setLayout(new java.awt.BorderLayout());

        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlCards.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setAlignmentX(0.0F);
        jScrollPane1.setAlignmentY(0.0F);
        jScrollPane1.setAutoscrolls(true);

        dskDrillDown.setAutoOrganize(true);
        dskDrillDown.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setViewportView(dskDrillDown);

        pnlCards.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnlTitle.setPreferredSize(new java.awt.Dimension(160, 20));

        lblTendencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTendencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_tendencia.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        lblTendencia.setText(bundle.getString("BscDrillFrame_00013")); // NOI18N
        lblTendencia.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblTendencia.setMaximumSize(new java.awt.Dimension(200, 15));
        lblTendencia.setMinimumSize(new java.awt.Dimension(60, 15));
        lblTendencia.setPreferredSize(new java.awt.Dimension(200, 23));
        pnlTitle.add(lblTendencia);

        pnlCards.add(pnlTitle, java.awt.BorderLayout.NORTH);

        pnlCenter.add(pnlCards, java.awt.BorderLayout.CENTER);

        pnlCentral.add(pnlCenter, java.awt.BorderLayout.CENTER);

        pnlIndicador.setBackground(new java.awt.Color(204, 204, 204));
        pnlIndicador.setPreferredSize(new java.awt.Dimension(240, 200));
        pnlIndicador.setLayout(new java.awt.BorderLayout(1, 1));

        pnlLayout.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlLayout.setLayout(new java.awt.BorderLayout());

        pnlTitulos.setPreferredSize(new java.awt.Dimension(106, 0));
        pnlTitulos.setLayout(null);

        lblDescricao.setText(bundle.getString("BscDrillFrame_00017")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setPreferredSize(new java.awt.Dimension(152, 20));
        pnlTitulos.add(lblDescricao);
        lblDescricao.setBounds(5, 2, 105, 20);

        lblFrequencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequencia.setText(bundle.getString("BscDrillFrame_00016")); // NOI18N
        lblFrequencia.setEnabled(false);
        lblFrequencia.setPreferredSize(new java.awt.Dimension(80, 23));
        pnlTitulos.add(lblFrequencia);
        lblFrequencia.setBounds(5, 69, 105, 20);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscDrillFrame_00014")); // NOI18N
        lblResponsavel.setEnabled(false);
        lblResponsavel.setPreferredSize(new java.awt.Dimension(80, 23));
        pnlTitulos.add(lblResponsavel);
        lblResponsavel.setBounds(5, 104, 105, 20);

        lblMetrica.setText(bundle.getString("BscDrillFrame_00015")); // NOI18N
        lblMetrica.setEnabled(false);
        pnlTitulos.add(lblMetrica);
        lblMetrica.setBounds(5, 136, 105, 20);

        pnlLayout.add(pnlTitulos, java.awt.BorderLayout.WEST);

        pnlFields.setLayout(new java.awt.BorderLayout(0, 10));

        jPnlNorte.setPreferredSize(new java.awt.Dimension(60, 60));
        jPnlNorte.setLayout(new javax.swing.BoxLayout(jPnlNorte, javax.swing.BoxLayout.LINE_AXIS));

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setPreferredSize(new java.awt.Dimension(24, 19));

        txtDescricao.setEnabled(false);
        jScrollPane3.setViewportView(txtDescricao);

        jPnlNorte.add(jScrollPane3);

        pnlFields.add(jPnlNorte, java.awt.BorderLayout.NORTH);

        jPnlCentro.setAlignmentX(0.0F);
        jPnlCentro.setLayout(new java.awt.BorderLayout(0, 5));

        txtFrequencia.setEnabled(false);
        txtFrequencia.setMaxLength(60);
        txtFrequencia.setSelectAllOnDoubleClick(true);
        jPnlCentro.add(txtFrequencia, java.awt.BorderLayout.NORTH);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        jPnlCentro.add(fldResponsavel, java.awt.BorderLayout.SOUTH);

        pnlFields.add(jPnlCentro, java.awt.BorderLayout.CENTER);

        jPnlSul.setPreferredSize(new java.awt.Dimension(60, 60));
        jPnlSul.setLayout(new javax.swing.BoxLayout(jPnlSul, javax.swing.BoxLayout.LINE_AXIS));

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane4.setEnabled(false);

        txtMetrica.setEnabled(false);
        txtMetrica.setPreferredSize(new java.awt.Dimension(217, 51));
        jScrollPane4.setViewportView(txtMetrica);

        jPnlSul.add(jScrollPane4);

        pnlFields.add(jPnlSul, java.awt.BorderLayout.SOUTH);

        pnlLayout.add(pnlFields, java.awt.BorderLayout.CENTER);

        pnlIndicador.add(pnlLayout, java.awt.BorderLayout.CENTER);

        panelLeft.setBackground(new java.awt.Color(204, 204, 204));
        panelLeft.setPreferredSize(new java.awt.Dimension(239, 202));
        panelLeft.setLayout(new java.awt.BorderLayout(1, 1));

        pnlCentro.setBackground(new java.awt.Color(204, 204, 204));
        pnlCentro.setPreferredSize(new java.awt.Dimension(237, 200));
        panelLeft.add(pnlCentro, java.awt.BorderLayout.EAST);

        pnlIndicador.add(panelLeft, java.awt.BorderLayout.WEST);

        pnlCentral.add(pnlIndicador, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlCentral, java.awt.BorderLayout.CENTER);

        pnlTopo.setPreferredSize(new java.awt.Dimension(25, 25));
        pnlTopo.setLayout(new java.awt.BorderLayout());

        lblIndicador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIndicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif"))); // NOI18N
        lblIndicador.setText(bundle.getString("BscDrillFrame_00010")); // NOI18N
        lblIndicador.setPreferredSize(new java.awt.Dimension(247, 205));
        pnlTopo.add(lblIndicador, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTopo, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 93));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(400, 22));
        pnlConfirmation.setLayout(null);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDataAlvo.setText(bundle.getString("BscDrillFrame_00007")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlConfirmation.add(lblDataAlvo);
        lblDataAlvo.setBounds(0, 0, 100, 18);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fldDataAlvoActionPerformed(evt);
            }
        });
        pnlConfirmation.add(fldDataAlvo);
        fldDataAlvo.setBounds(110, 0, 100, 22);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        pnlConfirmation.add(btnAvaliacao);
        btnAvaliacao.setBounds(220, 0, 25, 22);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        pnlConfirmation.add(fldAnalise);
        fldAnalise.setBounds(260, 0, 100, 22);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        pnlConfirmation.add(btnAnalise);
        btnAnalise.setBounds(370, 0, 25, 22);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 22));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 22));

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscDrillFrame_00004")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnMapaEstrategico.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnMapaEstrategico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
        btnMapaEstrategico.setText(bundle.getString("BscDrillFrame_00005")); // NOI18N
        btnMapaEstrategico.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMapaEstrategico.setMaximumSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.setMinimumSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.setPreferredSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMapaEstrategicoActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMapaEstrategico);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlHeaderTop.setMinimumSize(new java.awt.Dimension(130, 45));
        pnlHeaderTop.setPreferredSize(new java.awt.Dimension(10, 60));
        pnlHeaderTop.setLayout(new java.awt.BorderLayout());

        pnlHeader.setMinimumSize(new java.awt.Dimension(130, 45));
        pnlHeader.setPreferredSize(new java.awt.Dimension(0, 90));
        pnlHeaderTop.add(pnlHeader, java.awt.BorderLayout.NORTH);

        pnlTools.add(pnlHeaderTop, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
	    bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
	    new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
	    bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
	    bscPerformanceCard.show();

    }//GEN-LAST:event_btnAvaliacaoActionPerformed

    private void fldDataAlvoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fldDataAlvoActionPerformed
	// T\ODO add your handling code here:
    }//GEN-LAST:event_fldDataAlvoActionPerformed

    private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
		bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
			new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
		bscAnaliseParceladaCard.show();
	// TODO add your handling code here:
    }//GEN-LAST:event_btnAnaliseActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("ANALISE");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnMapaEstrategicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMapaEstrategicoActionPerformed
		try {
			bsc.swing.BscDefaultFrameFunctions frame =
			bsc.core.BscStaticReferences.getBscFormController().getForm( "MAPAEST", this.contextId, record.getString("NOME") );
		}
		catch (bsc.core.BscFormControllerException exc ) {
			bsc.core.BscDebug.println(exc.getMessage());
		}
	}//GEN-LAST:event_btnMapaEstrategicoActionPerformed

	private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
		dskDrillDown.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentShown
	
	private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
		dskDrillDown.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentResized
			
	private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange
	}//GEN-LAST:event_formPropertyChange
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
					
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnMapaEstrategico;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private bsc.beans.JBICardDesktopPane dskDrillDown;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private pv.jfcx.JPVEdit fldResponsavel;
    private javax.swing.JPanel jPnlCentro;
    private javax.swing.JPanel jPnlNorte;
    private javax.swing.JPanel jPnlSul;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblFrequencia;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblMetrica;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblTendencia;
    private javax.swing.JPanel panelLeft;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCards;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlCentral;
    private javax.swing.JDesktopPane pnlCentro;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlHeaderTop;
    private javax.swing.JPanel pnlIndicador;
    private javax.swing.JPanel pnlLayout;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTitle;
    private javax.swing.JPanel pnlTitulos;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopo;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    private pv.jfcx.JPVEdit txtFrequencia;
    private bsc.beans.JBITextArea txtMetrica;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscDrillIndicador(int operation, String idAux, String contextId) {
	    try{
		this.contextId = contextId;
		initComponents();

		event.setNoServerMode(false);
		event.defaultConstructor( operation, idAux, contextId );
	    }catch (Exception e){
		e.printStackTrace();
	    }
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
	}
	
	public void showOperationsButtons() {
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return null;
	}
}
