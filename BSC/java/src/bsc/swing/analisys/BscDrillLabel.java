/*
 * BscDrillLabel.java
 *
 * Created on October 31, 2003, 5:29 PM
 */

package bsc.swing.analisys;

/**
 *
 * @author  siga1996
 */
public class BscDrillLabel extends javax.swing.JLabel implements java.awt.event.MouseListener{

	private String id = null;
	private String name = null;
	private BscDrillFrame frame = null;
    private BscDrillObjetivo frameObj = null;
	
	/** Creates a new instance of BscDrillLabel */
	public BscDrillLabel(String idAux, String nameAux, int feedbackAux, BscDrillFrame drillFrame) {
		id = String.valueOf( idAux );
		name = String.valueOf( nameAux );
		this.setText( nameAux );
		this.setEnabled(true);
		frame = drillFrame;
		this.setIcon( bsc.core.BscStaticReferences.getBscImageResources().getImage( feedbackAux ) );

		this.addMouseListener(this);

	}	

	/** Creates a new instance of BscDrillLabel */
	public BscDrillLabel(String idAux, String nameAux, int feedbackAux, BscDrillObjetivo drillFrameObj) {
		id = String.valueOf( idAux );
		name = String.valueOf( nameAux );
		this.setText( nameAux );
		this.setEnabled(true);
		frameObj = drillFrameObj;
		this.setIcon( bsc.core.BscStaticReferences.getBscImageResources().getImage( feedbackAux ) );

		this.addMouseListener(this);

	}	

    //public java.awt.Dimension getPreferredSize(){
	//	return new java.awt.Dimension(0, 40);
	//}

	public void mouseClicked(java.awt.event.MouseEvent e) {
	    if(e.getSource() instanceof BscDrillLabel){
				if ( frame instanceof BscDrillFrame && bsc.core.BscStaticReferences.getBscFormController().replaceFormInfo( "DRILL", frame.getID(), "DRILL", id ) ) {
					frame.pnlInfluencia.removeAll();
					frame.pnlTendencia.removeAll();
					frame.pnlInfluencia.repaint();
					frame.pnlTendencia.repaint();
					frame.setType("DRILL");
					frame.setID( id );
					frame.loadRecord();
					frame.asJInternalFrame().setTitle("Drill-Down - " + name);
				}else{
					if(frameObj instanceof BscDrillObjetivo){
						frameObj.pnlInfluencia.removeAll();
						frameObj.pnlTendencia.removeAll();
						frameObj.pnlInfluencia.repaint();
						frameObj.pnlTendencia.repaint();
						frameObj.setType("DRILLOBJ");
						frameObj.setID( id );
						frameObj.loadRecord();
						frameObj.asJInternalFrame().setTitle(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDrillObjetivo_00006")+ name);
					}
				}
	    }
	}

	public void mouseEntered(java.awt.event.MouseEvent e) {
	    if(e.getSource() instanceof BscDrillLabel){
		BscDrillLabel label = (BscDrillLabel)e.getSource();
		label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		label.setText( "<html><u>"+name+"</u></html>" );
	    }
	}

	public void mouseExited(java.awt.event.MouseEvent e) {
	    if(e.getSource() instanceof BscDrillLabel){
		BscDrillLabel label = (BscDrillLabel)e.getSource();
		label.setCursor(null);
		label.setText( name );
	    }
	}
	
	public void mousePressed(java.awt.event.MouseEvent e) {
	}
	
	public void mouseReleased(java.awt.event.MouseEvent e) {
	}
	
}
