package bsc.swing.analisys;

public class BscDrillObjetivo extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("DRILLOBJ");

	private String contextId = new String("0");

	public void enableFields() {
	}
	
	public void disableFields() {
	}
	
	private java.util.Hashtable hstMedidas = new java.util.Hashtable();
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		// Ex.:
		// strategyList.setDataSource(
		//					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );

		fldDataAlvo.setCalendar( record.getDate("DATAALVO") );
		if(record.getBoolean("PARCELADA"))
			fldAnalise.setText("Parcelada");
		else
			fldAnalise.setText("Acumulada");
		
		this.contextId = record.getString("CONTEXTID");
		bsc.xml.BIXMLVector tendencias = record.getBIXMLVector("TENDENCIAS");  //Causa

		pnlTendencia.removeAll();
		BscDrillLabel labelAux = null;
		for ( int i = 0; i < tendencias.size(); i++ ) {
			labelAux = new BscDrillLabel(
					tendencias.get(i).getString("ID"), tendencias.get(i).getString("NOME"),
					tendencias.get(i).getInt("FEEDBACK"),  this);
			pnlTendencia.add( labelAux );
			this.addMouseListener( labelAux );
			labelAux.setVisible(false);
			labelAux.setVisible(true);
		}
		pnlTendencia.doLayout();

		bsc.xml.BIXMLVector influencias = record.getBIXMLVector("INFLUENCIAS");  //Efeito
		pnlInfluencia.removeAll();
		for ( int i = 0; i < influencias.size(); i++ ){
			labelAux = new BscDrillLabel(
					influencias.get(i).getString("ID"), influencias.get(i).getString("NOME"),
					influencias.get(i).getInt("FEEDBACK"), this);

			pnlInfluencia.add( labelAux );
			this.addMouseListener( labelAux );
			labelAux.setVisible(false);
			labelAux.setVisible(true);
		}
		pnlInfluencia.doLayout();

		bsc.xml.BIXMLVector cards = record.getBIXMLVector("CARDS");
		dskDrillDown.removeAll();
		BscObjetivoCardFrame jifPainel = null;
		BscObjetivoCardFrame jifCentral = null;

		pnlCentro.removeAll();
		for ( int i = 0; i < cards.size(); i++ ) {
            bsc.xml.BIXMLRecord rec = cards.get( i );
			if ( rec.getBoolean("VISIVEL") ) {
				if( i > 0  && rec.getString("ENTITY").equals("OBJETIVO")){
                        jifPainel = new BscObjetivoCardFrame( rec );
                        dskDrillDown.add( jifPainel );
                        jifPainel.setClosable(false);
                        jifPainel.show();
				}
				else
				{if ( rec.getString("ENTITY").equals("OBJETIVO") ) {
					jifCentral = new BscObjetivoCardFrame( rec );
                    pnlCentro.add( jifCentral );
					jifCentral.setClosable(false);
					jifCentral.show();
				 }
				}
			}
		}
		dskDrillDown.autoOrganizeFrames();
        pnlHeader.setComponents(record.getBIXMLVector("CONTEXTOS"));
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		pnlInfluencia.setPreferredSize(new java.awt.Dimension(160, (40 * record.getBIXMLVector("INFLUENCIAS").size())));
		pnlTendencia.setPreferredSize(new java.awt.Dimension(160, (40 * record.getBIXMLVector("TENDENCIAS").size())));
		pnlInfluencia.repaint();
		pnlTendencia.repaint();
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return true;
	}
	
	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlCardsObjetivos = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dskDrillDown = new bsc.beans.JBICardDesktopPane();
        jPanelTitleObjetivo = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        pnlTop = new javax.swing.JPanel();
        pnlCausaEfeito = new javax.swing.JPanel();
        pnlLeft = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        pnlCausa = new javax.swing.JPanel();
        scrCausa = new javax.swing.JScrollPane();
        pnlInfluencia = new javax.swing.JPanel();
        pnlEfeito = new javax.swing.JPanel();
        scrEfeito = new javax.swing.JScrollPane();
        pnlTendencia = new javax.swing.JPanel();
        pnlRight = new javax.swing.JPanel();
        pnlObjetivo = new javax.swing.JPanel();
        pnlNorte = new javax.swing.JPanel();
        pnlSul = new javax.swing.JPanel();
        pnlLeste = new javax.swing.JPanel();
        pnlOeste = new javax.swing.JPanel();
        pnlCentro = new javax.swing.JDesktopPane();
        pnlTopo = new javax.swing.JPanel();
        lblObjetivo = new javax.swing.JLabel();
        pnlTitleCausaEfeito = new javax.swing.JPanel();
        lblCausa = new javax.swing.JLabel();
        lblEfeito = new javax.swing.JLabel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReload = new javax.swing.JButton();
        btnMapaEstrategico = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlHeaderTop = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscDrillObjetivo_00006")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                formPropertyChange(evt);
            }
        });

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRecord.setPreferredSize(new java.awt.Dimension(540, 355));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlCardsObjetivos.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlCardsObjetivos.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setAlignmentX(0.0F);
        jScrollPane1.setAlignmentY(0.0F);
        jScrollPane1.setAutoscrolls(true);

        dskDrillDown.setAutoOrganize(true);
        dskDrillDown.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setViewportView(dskDrillDown);

        pnlCardsObjetivos.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanelTitleObjetivo.setMinimumSize(new java.awt.Dimension(90, 25));
        jPanelTitleObjetivo.setPreferredSize(new java.awt.Dimension(80, 23));
        jPanelTitleObjetivo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 0, 0));

        lblTitle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_objetivo.gif"))); // NOI18N
        lblTitle.setText(bundle.getString("BscDrillObjetivo_00010")); // NOI18N
        lblTitle.setMaximumSize(new java.awt.Dimension(130, 15));
        lblTitle.setMinimumSize(new java.awt.Dimension(60, 15));
        lblTitle.setPreferredSize(new java.awt.Dimension(150, 23));
        jPanelTitleObjetivo.add(lblTitle);

        pnlCardsObjetivos.add(jPanelTitleObjetivo, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlCardsObjetivos, java.awt.BorderLayout.CENTER);

        pnlTop.setPreferredSize(new java.awt.Dimension(0, 220));
        pnlTop.setLayout(new java.awt.BorderLayout());

        pnlCausaEfeito.setLayout(new java.awt.BorderLayout());

        pnlLeft.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlCausaEfeito.add(pnlLeft, java.awt.BorderLayout.WEST);

        pnlCenter.setLayout(new javax.swing.BoxLayout(pnlCenter, javax.swing.BoxLayout.LINE_AXIS));

        pnlCausa.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlCausa.setLayout(new java.awt.BorderLayout());

        scrCausa.setAutoscrolls(true);
        scrCausa.setPreferredSize(new java.awt.Dimension(120, 0));

        pnlInfluencia.setMinimumSize(new java.awt.Dimension(10, 67));
        pnlInfluencia.setPreferredSize(new java.awt.Dimension(200, 100));
        pnlInfluencia.setLayout(new javax.swing.BoxLayout(pnlInfluencia, javax.swing.BoxLayout.Y_AXIS));
        scrCausa.setViewportView(pnlInfluencia);

        pnlCausa.add(scrCausa, java.awt.BorderLayout.CENTER);

        pnlCenter.add(pnlCausa);

        pnlEfeito.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlEfeito.setLayout(new java.awt.BorderLayout());

        scrEfeito.setAutoscrolls(true);
        scrEfeito.setPreferredSize(new java.awt.Dimension(120, 0));

        pnlTendencia.setMinimumSize(new java.awt.Dimension(10, 70));
        pnlTendencia.setPreferredSize(new java.awt.Dimension(200, 120));
        pnlTendencia.setLayout(new javax.swing.BoxLayout(pnlTendencia, javax.swing.BoxLayout.Y_AXIS));
        scrEfeito.setViewportView(pnlTendencia);

        pnlEfeito.add(scrEfeito, java.awt.BorderLayout.CENTER);

        pnlCenter.add(pnlEfeito);

        pnlCausaEfeito.add(pnlCenter, java.awt.BorderLayout.CENTER);

        pnlRight.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlCausaEfeito.add(pnlRight, java.awt.BorderLayout.EAST);

        pnlTop.add(pnlCausaEfeito, java.awt.BorderLayout.CENTER);

        pnlObjetivo.setBackground(javax.swing.UIManager.getDefaults().getColor("Desktop.background"));
        pnlObjetivo.setPreferredSize(new java.awt.Dimension(247, 200));
        pnlObjetivo.setLayout(new java.awt.BorderLayout());

        pnlNorte.setBackground(new java.awt.Color(204, 204, 204));
        pnlNorte.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlObjetivo.add(pnlNorte, java.awt.BorderLayout.NORTH);

        pnlSul.setBackground(new java.awt.Color(204, 204, 204));
        pnlSul.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlObjetivo.add(pnlSul, java.awt.BorderLayout.SOUTH);

        pnlLeste.setBackground(new java.awt.Color(204, 204, 204));
        pnlLeste.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlObjetivo.add(pnlLeste, java.awt.BorderLayout.WEST);

        pnlOeste.setBackground(new java.awt.Color(204, 204, 204));
        pnlOeste.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlObjetivo.add(pnlOeste, java.awt.BorderLayout.EAST);

        pnlCentro.setBackground(new java.awt.Color(204, 204, 204));
        pnlObjetivo.add(pnlCentro, java.awt.BorderLayout.CENTER);

        pnlTop.add(pnlObjetivo, java.awt.BorderLayout.WEST);

        pnlTopo.setPreferredSize(new java.awt.Dimension(25, 20));
        pnlTopo.setLayout(new java.awt.BorderLayout());

        lblObjetivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblObjetivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_objetivo.gif"))); // NOI18N
        lblObjetivo.setText(bundle.getString("BscDrillObjetivo_00009")); // NOI18N
        lblObjetivo.setPreferredSize(new java.awt.Dimension(247, 205));
        pnlTopo.add(lblObjetivo, java.awt.BorderLayout.WEST);

        pnlTitleCausaEfeito.setLayout(new java.awt.GridLayout(1, 2));

        lblCausa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCausa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_graydn.gif"))); // NOI18N
        lblCausa.setText(bundle.getString("BscDrillObjetivo_00002")); // NOI18N
        pnlTitleCausaEfeito.add(lblCausa);

        lblEfeito.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEfeito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_grayup.gif"))); // NOI18N
        lblEfeito.setText(bundle.getString("BscDrillObjetivo_00001")); // NOI18N
        pnlTitleCausaEfeito.add(lblEfeito);

        pnlTopo.add(pnlTitleCausaEfeito, java.awt.BorderLayout.CENTER);

        pnlTop.add(pnlTopo, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlTop, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 80));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(400, 22));
        pnlConfirmation.setLayout(null);

        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDataAlvo.setText(bundle.getString("BscDrillObjetivo_00007")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlConfirmation.add(lblDataAlvo);
        lblDataAlvo.setBounds(-10, 0, 110, 18);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fldDataAlvoActionPerformed(evt);
            }
        });
        pnlConfirmation.add(fldDataAlvo);
        fldDataAlvo.setBounds(110, 0, 100, 22);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        pnlConfirmation.add(btnAvaliacao);
        btnAvaliacao.setBounds(220, 0, 25, 22);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        pnlConfirmation.add(fldAnalise);
        fldAnalise.setBounds(260, 0, 100, 22);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        pnlConfirmation.add(btnAnalise);
        btnAnalise.setBounds(370, 0, 25, 22);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 22));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 22));

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscDrillObjetivo_00004")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnMapaEstrategico.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnMapaEstrategico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
        btnMapaEstrategico.setText(bundle.getString("BscDrillObjetivo_00005")); // NOI18N
        btnMapaEstrategico.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMapaEstrategico.setMaximumSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.setMinimumSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.setPreferredSize(new java.awt.Dimension(125, 20));
        btnMapaEstrategico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMapaEstrategicoActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMapaEstrategico);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlHeaderTop.setMinimumSize(new java.awt.Dimension(130, 45));
        pnlHeaderTop.setPreferredSize(new java.awt.Dimension(10, 45));
        pnlHeaderTop.setLayout(new java.awt.BorderLayout());

        pnlHeader.setMinimumSize(new java.awt.Dimension(130, 45));
        pnlHeader.setPreferredSize(new java.awt.Dimension(0, 90));
        pnlHeaderTop.add(pnlHeader, java.awt.BorderLayout.NORTH);

        pnlTools.add(pnlHeaderTop, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
	    bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
	    new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
	    bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
	    bscPerformanceCard.show();

    }//GEN-LAST:event_btnAvaliacaoActionPerformed

    private void fldDataAlvoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fldDataAlvoActionPerformed
	// TODO add your handling code here:
    }//GEN-LAST:event_fldDataAlvoActionPerformed

    private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
		bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
			new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
		bscAnaliseParceladaCard.show();
	// TODO add your handling code here:
    }//GEN-LAST:event_btnAnaliseActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("ANALISE");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnMapaEstrategicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMapaEstrategicoActionPerformed
		try {
			bsc.swing.BscDefaultFrameFunctions frame =
			bsc.core.BscStaticReferences.getBscFormController().getForm( "MAPAEST", this.contextId, record.getString("NOME") );
		}
		catch (bsc.core.BscFormControllerException exc ) {
			bsc.core.BscDebug.println(exc.getMessage());
		}
	}//GEN-LAST:event_btnMapaEstrategicoActionPerformed

	private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
		dskDrillDown.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentShown
	
	private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
		dskDrillDown.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentResized
			
	private void formPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_formPropertyChange
	}//GEN-LAST:event_formPropertyChange
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
					
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnMapaEstrategico;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private bsc.beans.JBICardDesktopPane dskDrillDown;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private javax.swing.JPanel jPanelTitleObjetivo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCausa;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblEfeito;
    private javax.swing.JLabel lblObjetivo;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCardsObjetivos;
    private javax.swing.JPanel pnlCausa;
    private javax.swing.JPanel pnlCausaEfeito;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JDesktopPane pnlCentro;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlEfeito;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlHeaderTop;
    public javax.swing.JPanel pnlInfluencia;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeste;
    private javax.swing.JPanel pnlNorte;
    private javax.swing.JPanel pnlObjetivo;
    private javax.swing.JPanel pnlOeste;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRight;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlSul;
    public javax.swing.JPanel pnlTendencia;
    private javax.swing.JPanel pnlTitleCausaEfeito;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTop;
    private javax.swing.JPanel pnlTopo;
    private javax.swing.JScrollPane scrCausa;
    private javax.swing.JScrollPane scrEfeito;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscDrillObjetivo(int operation, String idAux, String contextId) {
	    try{
		this.contextId = contextId;
		initComponents();

		event.setNoServerMode(false);
		event.defaultConstructor( operation, idAux, contextId );
	    }catch (Exception e){
		e.printStackTrace();
	    }
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
//		pnlConfirmation.setVisible(true);
//		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
//		pnlConfirmation.setVisible(false);
//		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return null;
	}
}
