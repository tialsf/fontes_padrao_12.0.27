/*
 * BscCardFrame.java
 *
 * Created on September 9, 2003, 5:33 PM
 */

package bsc.swing.analisys;

/**
 *
 * @author  siga1996
 */
public class BscIndicadorCardFrame extends javax.swing.JInternalFrame implements BscCardFrame {
	
	bsc.xml.BIXMLRecord record = null;
	java.util.Vector listData = new java.util.Vector();
	private String id = new String();
	String 			entId = new String(),
			entity = new String();
	boolean sliderEnabled = false;
	int order = -1;
	
	public java.awt.Point getCardFramePosition() {
		java.awt.Point p = new java.awt.Point(record.getInt("CARDX"), record.getInt("CARDY") );
		return p;
	}
	
	public void setOrder( int o ) {
		order = o;
	}
	
	public int getOrder() {
		return order;
	}
	
	public void setSliderEnabled( boolean se ) {
		sliderEnabled = se;
	}
	
	public boolean getSliderEnabled() {
		return sliderEnabled;
	}
	
	public String getNome() {
		return lblTitle.getText();
	}
	
	/** Creates new form BscCardFrame */
	public BscIndicadorCardFrame(bsc.xml.BIXMLRecord recordAux) {
		this.record = recordAux;
		this.putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
		initComponents();
		slider.setEnabled(false);

        /*WorkAround
          Omite t�tulo quando n�o receber o atributo PARENTNAME via XML.*/
        try{
            this.setTitle(record.getString("PARENTNAME"));
        }catch(Exception ex){}

		setId(String.valueOf(record.getString("ID")));
		order = record.getInt("ORDEM");
		btnIndicador.setVisible(record.getBoolean("INFLUENCIA"));
		entity = String.valueOf(record.getString("ENTITY"));
		entId = String.valueOf(record.getString("ENTID"));
		lblTitle.setType( record.getString("ENTITY") );
		lblTitle.setID( record.getString("ENTID") );
		lblTitle.setTitle( record.getString("NOME") );
		lblTitle.setText( record.getString("NOME") );
		lblTitle.setIcon( bsc.core.BscStaticReferences.getBscMainPanel().getImageResources().getImage( record.getInt("FEEDBACK") ));
		if(record.getString("TIPOIND").trim().equals("T")){
			java.awt.Color corTendencia = new java.awt.Color(190, 230, 243);
			
			lblAtual.setBackground(corTendencia);
			lblTitle.setBackground(corTendencia);
			pnlTitle.setBackground(corTendencia);
			pnlRight.setBackground(corTendencia);
			pnlLeft.setBackground(corTendencia);
			pnlValue.setBackground(corTendencia);
			pnlDireito.setBackground(corTendencia);
			pnlEsquerdo.setBackground(corTendencia);
			pnlValores.setBackground(corTendencia);
			pnlTop.setBackground(corTendencia);
			pnlBottom.setBackground(corTendencia);
			pnlL.setBackground(corTendencia);
			pnlR.setBackground(corTendencia);
			pnlAzul.setBackground(corTendencia);
			pnlVerde.setBackground(corTendencia);
			pnlAmarelo.setBackground(corTendencia);
			pnlVermelho.setBackground(corTendencia);
			pnlEscala.setBackground(corTendencia);
		}
		if(bsc.core.BscStaticReferences.getBscMainPanel().getImageResources().BSC_FB_GRAY == record.getInt("FEEDBACK") ){
			slider.setRed( 0 );
			slider.setYellow( 0 );
			slider.setGreen( 0 );
			slider.setBlue( 0 );
			slider.setPosition( 0 );
		} else {
			slider.setRed( record.getInt("VERMELHO") );
			slider.setYellow( record.getInt("AMARELO") );
			slider.setGreen( record.getInt("VERDE") );
			slider.setBlue( record.getInt("AZUL") );
			slider.setPosition( record.getInt("INDICADOR") );
		}
		
		java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
		nf.setMaximumFractionDigits(record.getInt("DECIMAIS"));
		nf.setMinimumFractionDigits(record.getInt("DECIMAIS"));
		
		String cAnterior = nf.format( record.getDouble("ANTERIOR")) + " " + record.getString("UNIDADE");
		lblAnterior.setText( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscCardPainelFrame_00001") + " " + cAnterior );
		
		String cAtual = nf.format( record.getDouble("ATUAL")) + " " + record.getString("UNIDADE");
		lblAtual.setText( cAtual );
		String cInicial = nf.format( record.getDouble("INICIAL")) + " " + record.getString("UNIDADE");
		lblInicial.setText( cInicial );
		String cFinal = nf.format( record.getDouble("FINAL")) + " " + record.getString("UNIDADE");
		lblFinal.setText( cFinal );
		
		lblValVerde.setText( nf.format( record.getDouble("RVERDE")) );
		lblUniVerde.setText( record.getString("UNIDADE") + " < Meta" );
		
		lblValVermelho.setText( nf.format( record.getDouble("RVERMELHO")) );
		lblUniVermelho.setText( record.getString("UNIDADE"));
		
		lblValAmarelo.setText( nf.format( record.getDouble("RAMARELO")) );
		lblUniAmarelo.setText( record.getString("UNIDADE"));
		
		lblValAzul.setText( nf.format( record.getDouble("RAZUL")) );
		lblUniAzul.setText( record.getString("UNIDADE"));
		
		this.setAlignmentX( record.getInt("CARDX") );
		this.setAlignmentY( record.getInt("CARDY") );
		bsc.beans.JBIHyperlinkLabel label = null;
	}
	
	public bsc.xml.BIXMLRecord getRecordData() {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord("CARD");
		recordAux.set("ID", getId());
		recordAux.set("CARDX", this.getX());
		recordAux.set("CARDY", this.getY());
		recordAux.set("ORDEM", order);
		recordAux.set("VISIVEL", true);
		recordAux.set("ENTITY", entity);
		recordAux.set("ENTID", entId);
		return recordAux;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        pnlTitle = new javax.swing.JPanel();
        lblAtual = new javax.swing.JLabel();
        lblTitle = new bsc.beans.JBIHyperlinkLabel();
        pnlSlider = new javax.swing.JPanel();
        slider = new bsc.beans.JBIColorStatusPanel();
        pnlRight = new javax.swing.JPanel();
        pnlLeft = new javax.swing.JPanel();
        pnlValue = new javax.swing.JPanel();
        pnlValores = new javax.swing.JPanel();
        lblInicial = new javax.swing.JLabel();
        lblFinal = new javax.swing.JLabel();
        pnlDireito = new javax.swing.JPanel();
        pnlEsquerdo = new javax.swing.JPanel();
        pnlEscala = new javax.swing.JPanel();
        pnlR = new javax.swing.JPanel();
        btnIndicador = new javax.swing.JButton();
        btnDwConsulta = new javax.swing.JButton();
        pnlBottom = new javax.swing.JPanel();
        pnlL = new javax.swing.JPanel();
        pnlTop = new javax.swing.JPanel();
        lblAnterior = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        pnlAzul = new javax.swing.JPanel();
        lblLegAzul = new javax.swing.JLabel();
        lblValAzul = new javax.swing.JLabel();
        lblUniAzul = new javax.swing.JLabel();
        pnlVerde = new javax.swing.JPanel();
        lblLegVerde = new javax.swing.JLabel();
        lblValVerde = new javax.swing.JLabel();
        lblUniVerde = new javax.swing.JLabel();
        pnlAmarelo = new javax.swing.JPanel();
        lblLegAmarelo = new javax.swing.JLabel();
        lblValAmarelo = new javax.swing.JLabel();
        lblUniAmarelo = new javax.swing.JLabel();
        pnlVermelho = new javax.swing.JPanel();
        lblLegVermelho = new javax.swing.JLabel();
        lblValVermelho = new javax.swing.JLabel();
        lblUniVermelho = new javax.swing.JLabel();

        setToolTipText("");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(235, 220));

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.BorderLayout());

        pnlTitle.setLayout(new java.awt.BorderLayout());

        lblAtual.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblAtual.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAtual.setText("<valor atual>");
        pnlTitle.add(lblAtual, java.awt.BorderLayout.CENTER);

        lblTitle.setHorizontalAlignment(0);
        lblTitle.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblTitle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_graysm.gif"))); // NOI18N
        lblTitle.setText("<nome da entidade>");
        lblTitle.setToolTipText("");
        pnlTitle.add(lblTitle, java.awt.BorderLayout.NORTH);

        jPanel1.add(pnlTitle, java.awt.BorderLayout.NORTH);

        pnlSlider.setOpaque(false);
        pnlSlider.setLayout(new java.awt.BorderLayout());

        slider.setEnabled(false);
        slider.setFocusable(false);
        slider.setPreferredSize(new java.awt.Dimension(100, 50));
        pnlSlider.add(slider, java.awt.BorderLayout.CENTER);

        pnlRight.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlSlider.add(pnlRight, java.awt.BorderLayout.EAST);

        pnlLeft.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlSlider.add(pnlLeft, java.awt.BorderLayout.WEST);

        jPanel1.add(pnlSlider, java.awt.BorderLayout.CENTER);

        pnlValue.setOpaque(false);
        pnlValue.setLayout(new java.awt.BorderLayout());

        pnlValores.setLayout(new java.awt.BorderLayout());

        lblInicial.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblInicial.setText("<valor Inicial>");
        pnlValores.add(lblInicial, java.awt.BorderLayout.WEST);

        lblFinal.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblFinal.setText("<valor Final>");
        pnlValores.add(lblFinal, java.awt.BorderLayout.EAST);

        pnlValue.add(pnlValores, java.awt.BorderLayout.CENTER);
        pnlValue.add(pnlDireito, java.awt.BorderLayout.EAST);
        pnlValue.add(pnlEsquerdo, java.awt.BorderLayout.WEST);

        jPanel1.add(pnlValue, java.awt.BorderLayout.SOUTH);

        getContentPane().add(jPanel1, java.awt.BorderLayout.NORTH);

        pnlEscala.setOpaque(false);
        pnlEscala.setLayout(new java.awt.BorderLayout());

        pnlR.setPreferredSize(new java.awt.Dimension(50, 50));
        pnlR.setRequestFocusEnabled(false);
        pnlR.setLayout(null);

        btnIndicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_tendencia.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnIndicador.setToolTipText(bundle.getString("BscDrillFrame_00006")); // NOI18N
        btnIndicador.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIndicadorActionPerformed(evt);
            }
        });
        pnlR.add(btnIndicador);
        btnIndicador.setBounds(10, 0, 30, 30);

        btnDwConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_dw.gif"))); // NOI18N
        btnDwConsulta.setToolTipText("Consultas do DW.");
        btnDwConsulta.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDwConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDwConsultaActionPerformed(evt);
            }
        });
        pnlR.add(btnDwConsulta);
        btnDwConsulta.setBounds(10, 30, 30, 30);

        pnlEscala.add(pnlR, java.awt.BorderLayout.EAST);
        pnlEscala.add(pnlBottom, java.awt.BorderLayout.SOUTH);
        pnlEscala.add(pnlL, java.awt.BorderLayout.WEST);

        pnlTop.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 0, 0));

        lblAnterior.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblAnterior.setText("<valor anterior>");
        pnlTop.add(lblAnterior);

        pnlEscala.add(pnlTop, java.awt.BorderLayout.NORTH);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setPreferredSize(new java.awt.Dimension(20, 20));
        jPanel3.setLayout(null);

        pnlAzul.setBackground(new java.awt.Color(204, 204, 204));
        pnlAzul.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlAzul.setLayout(null);

        lblLegAzul.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblLegAzul.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_bluelg.gif"))); // NOI18N
        lblLegAzul.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAzul.add(lblLegAzul);
        lblLegAzul.setBounds(2, 0, 20, 10);

        lblValAzul.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblValAzul.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblValAzul.setText("<999,99>");
        lblValAzul.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAzul.add(lblValAzul);
        lblValAzul.setBounds(25, 0, 60, 10);

        lblUniAzul.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblUniAzul.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUniAzul.setText("<unidade>");
        lblUniAzul.setMaximumSize(new java.awt.Dimension(38, 15));
        lblUniAzul.setMinimumSize(new java.awt.Dimension(38, 15));
        lblUniAzul.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAzul.add(lblUniAzul);
        lblUniAzul.setBounds(90, 0, 80, 10);

        jPanel3.add(pnlAzul);
        pnlAzul.setBounds(0, 10, 180, 10);

        pnlVerde.setBackground(new java.awt.Color(204, 204, 204));
        pnlVerde.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlVerde.setLayout(null);

        lblLegVerde.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblLegVerde.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_greenlg.gif"))); // NOI18N
        lblLegVerde.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVerde.add(lblLegVerde);
        lblLegVerde.setBounds(2, 0, 20, 10);

        lblValVerde.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblValVerde.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblValVerde.setText("<999,99>");
        lblValVerde.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVerde.add(lblValVerde);
        lblValVerde.setBounds(25, 0, 60, 10);

        lblUniVerde.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblUniVerde.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUniVerde.setText("<unidade>");
        lblUniVerde.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVerde.add(lblUniVerde);
        lblUniVerde.setBounds(90, 0, 70, 10);

        jPanel3.add(pnlVerde);
        pnlVerde.setBounds(0, 20, 185, 10);

        pnlAmarelo.setBackground(new java.awt.Color(204, 204, 204));
        pnlAmarelo.setPreferredSize(new java.awt.Dimension(20, 20));
        pnlAmarelo.setLayout(null);

        lblLegAmarelo.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblLegAmarelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_yellowlg.gif"))); // NOI18N
        lblLegAmarelo.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAmarelo.add(lblLegAmarelo);
        lblLegAmarelo.setBounds(2, 0, 20, 10);

        lblValAmarelo.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblValAmarelo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblValAmarelo.setText("<999,99>");
        lblValAmarelo.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAmarelo.add(lblValAmarelo);
        lblValAmarelo.setBounds(25, 0, 60, 10);

        lblUniAmarelo.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblUniAmarelo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUniAmarelo.setText("<unidade>");
        lblUniAmarelo.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlAmarelo.add(lblUniAmarelo);
        lblUniAmarelo.setBounds(90, 0, 80, 10);

        jPanel3.add(pnlAmarelo);
        pnlAmarelo.setBounds(0, 30, 185, 10);

        pnlVermelho.setBackground(new java.awt.Color(204, 204, 204));
        pnlVermelho.setLayout(null);

        lblLegVermelho.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblLegVermelho.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_redlg.gif"))); // NOI18N
        lblLegVermelho.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVermelho.add(lblLegVermelho);
        lblLegVermelho.setBounds(2, 0, 20, 10);

        lblValVermelho.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblValVermelho.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblValVermelho.setText("<999,99>");
        lblValVermelho.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVermelho.add(lblValVermelho);
        lblValVermelho.setBounds(25, 0, 60, 10);

        lblUniVermelho.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        lblUniVermelho.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUniVermelho.setText("<unidade>");
        lblUniVermelho.setPreferredSize(new java.awt.Dimension(200, 18));
        pnlVermelho.add(lblUniVermelho);
        lblUniVermelho.setBounds(90, 0, 80, 10);

        jPanel3.add(pnlVermelho);
        pnlVermelho.setBounds(0, 40, 185, 15);

        pnlEscala.add(jPanel3, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlEscala, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	
    private void btnDwConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDwConsultaActionPerformed
		bsc.swing.analisys.BscConsultaDW consultaDW = new bsc.swing.analisys.BscConsultaDW(this,true);
		consultaDW.loadDWConsultas(entId);
		consultaDW.setVisible(true);
    }//GEN-LAST:event_btnDwConsultaActionPerformed
	
    private void btnIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIndicadorActionPerformed
		bsc.swing.BscDefaultFrameFunctions frame =
				bsc.core.BscStaticReferences.getBscFormController().getForm( "DRILLIND", lblTitle.getID(), lblTitle.getTitle() );
    }//GEN-LAST:event_btnIndicadorActionPerformed
	
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDwConsulta;
    private javax.swing.JButton btnIndicador;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblAnterior;
    private javax.swing.JLabel lblAtual;
    private javax.swing.JLabel lblFinal;
    private javax.swing.JLabel lblInicial;
    private javax.swing.JLabel lblLegAmarelo;
    private javax.swing.JLabel lblLegAzul;
    private javax.swing.JLabel lblLegVerde;
    private javax.swing.JLabel lblLegVermelho;
    private bsc.beans.JBIHyperlinkLabel lblTitle;
    private javax.swing.JLabel lblUniAmarelo;
    private javax.swing.JLabel lblUniAzul;
    private javax.swing.JLabel lblUniVerde;
    private javax.swing.JLabel lblUniVermelho;
    private javax.swing.JLabel lblValAmarelo;
    private javax.swing.JLabel lblValAzul;
    private javax.swing.JLabel lblValVerde;
    private javax.swing.JLabel lblValVermelho;
    private javax.swing.JPanel pnlAmarelo;
    private javax.swing.JPanel pnlAzul;
    private javax.swing.JPanel pnlBottom;
    private javax.swing.JPanel pnlDireito;
    private javax.swing.JPanel pnlEscala;
    private javax.swing.JPanel pnlEsquerdo;
    private javax.swing.JPanel pnlL;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlR;
    private javax.swing.JPanel pnlRight;
    private javax.swing.JPanel pnlSlider;
    private javax.swing.JPanel pnlTitle;
    private javax.swing.JPanel pnlTop;
    private javax.swing.JPanel pnlValores;
    private javax.swing.JPanel pnlValue;
    private javax.swing.JPanel pnlVerde;
    private javax.swing.JPanel pnlVermelho;
    private bsc.beans.JBIColorStatusPanel slider;
    // End of variables declaration//GEN-END:variables
	
	
}

/*class CardFrameGlassPane extends javax.swing.JComponent {
	CardFrameListener listener;
	public CardFrameGlassPane(bsc.beans.JBIColorStatusPanel sliderPanel,
										java.awt.Container contentPane) {
		listener = new CardFrameListener(sliderPanel, this, contentPane);
		addMouseListener(listener);
		addMouseMotionListener(listener);
	}
}
 
class CardFrameListener extends javax.swing.event.MouseInputAdapter {
	bsc.beans.JBIColorStatusPanel sliderPanel;
	CardFrameGlassPane glassPane;
	java.awt.Container contentPane;
 
	public CardFrameListener(bsc.beans.JBIColorStatusPanel sliderPanel,
										CardFrameGlassPane glassPane, java.awt.Container contentPane) {
		this.sliderPanel = sliderPanel;
		this.glassPane = glassPane;
		this.contentPane = contentPane;
	}
 
	public void mouseMoved(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	public void mouseDragged(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	public void mouseClicked(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	public void mouseEntered(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	public void mouseExited(java.awt.event.MouseEvent e) {
		System.out.println("mouseExited");
		redispatchMouseEvent(e);
	}
 
	public void mousePressed(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	public void mouseReleased(java.awt.event.MouseEvent e) {
		redispatchMouseEvent(e);
	}
 
	private void redispatchMouseEvent(java.awt.event.MouseEvent e) {
		java.awt.Point glassPanePoint = e.getPoint();
		java.awt.Component component = null;
		java.awt.Container container = contentPane;
		java.awt.Point containerPoint = javax.swing.SwingUtilities.convertPoint( glassPane, glassPanePoint, container);
 
		component = javax.swing.SwingUtilities.getDeepestComponentAt(container, containerPoint.x, containerPoint.y);
 
		if (component == null)
			return;
		else
			if ( component.getClass().getName().equals("javax.swing.JSlider") ) {
				return;
			}
			else {
				java.awt.Point componentPoint = javax.swing.SwingUtilities.convertPoint( glassPane, glassPanePoint, component);
				component.dispatchEvent(new java.awt.event.MouseEvent(component,
						 e.getID(),
						 e.getWhen(),
						 e.getModifiers(),
						 componentPoint.x,
						 componentPoint.y,
						 e.getClickCount(),
						 e.isPopupTrigger()));
			}
	}
 
}
 */