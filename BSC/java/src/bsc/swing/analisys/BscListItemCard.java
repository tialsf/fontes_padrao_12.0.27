/*
 * BscListItemCard.java
 *
 * Created on September 4, 2003, 3:22 PM
 */

package bsc.swing.analisys;

/**
 *
 * @author  siga1996
 */
public class BscListItemCard {
	private String text = new String(""),
		id = new String("");
	private int color = 0;

	public int getColor(){
		return color;
	}

	public String getText(){
		return String.valueOf(text);
	}

	public String getID() {
		return String.valueOf(id);
	}
	
	public int hashCode() {
		return text.hashCode();
	}

	public boolean equals( Object obj ) {
		BscListItemCard listItem = (BscListItemCard) obj;

		if  ( id.equals( listItem.getID() ) )
			return true;
		else
			return false;
	}
	
	/** Creates a new instance of BscListItemCard */
	public BscListItemCard(String idAux, String t, int c) {
		id = String.valueOf(idAux);
		text = String.valueOf(t);
		color = c;
	}
	
	public String toString() {
		return String.valueOf(text);
	}
}
