package bsc.swing.analisys;

//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;

public class BscMedidaDashboardFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("DASHBOARD");
    private BscDashItens vctItens = new BscDashItens();
    private bsc.beans.JBIXMLTable tblIndicador = null;
    
    /*
     * Habilita os campos do frame painel.
     * @return Retorno Nulo.
     */
    @Override
    public void enableFields() {
        lblNome.setEnabled(true);
        lblDataAlvo.setEnabled(true);
        lblAnalise.setEnabled(true);
        fldNome.setEnabled(true);
        lblMedida.setEnabled(true);
        dskDashboard.setEnabled(true);
        int intElementos = dskDashboard.getComponentCount();
        for (int i = 0; i < intElementos; i++) {
            ((bsc.swing.analisys.BscIndicadorCardFrame) dskDashboard.getComponent(i)).setClosable(false);
        }
        chkAutoOrganizar.setEnabled(true);
        tblIndicador.setEnabled(true);
        btnAnalise.setEnabled(true);
        btnAvaliacao.setEnabled(true);
    }

    @Override
    public void disableFields() {

        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblMedida.setEnabled(false);
        dskDashboard.setEnabled(false);
        int intElementos = dskDashboard.getComponentCount();
        for (int i = 0; i < intElementos; i++) {
            ((bsc.swing.analisys.BscIndicadorCardFrame) dskDashboard.getComponent(i)).setClosable(false);
        }
        chkAutoOrganizar.setEnabled(false);
        tblIndicador.setEnabled(false);
    }
    private java.util.Hashtable hstMedidas = new java.util.Hashtable();

    @Override
    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        fldDataAlvo.setCalendar(record.getDate("DATAALVO"));
        if (record.getBoolean("PARCELADA")) {
            fldAnalise.setText("Parcelada");
        } else {
            fldAnalise.setText("Acumulada");
        }

        hstMedidas.clear();

        bsc.xml.BIXMLVector cards = record.getBIXMLVector("CARDS");
        dskDashboard.removeAll();
        BscIndicadorCardFrame jif = null;
        vctItens = new BscDashItens();
        for (int i = 0; i < cards.size(); i++) {
            boolean selected = false;

            hstMedidas.put(cards.get(i).getString("NOME"), cards.get(i).getString("ID"));
            if (cards.get(i).getBoolean("VISIVEL")) {
                jif = new BscIndicadorCardFrame(cards.get(i));
                jif.setId(cards.get(i).getString("ENTID"));
                jif.setClosable(false);
                dskDashboard.add(jif);
                jif.show();
                selected = true;
            }
            vctItens.createNewRecord(cards.get(i).getInt("ENTID"), cards.get(i).getString("NOME"), selected);
        }
        dskDashboard.autoOrganizeFrames();

        chkAutoOrganizar.setSelected(record.getBoolean("ORGANIZADO"));
        dskDashboard.setAutoOrganize(record.getBoolean("ORGANIZADO"));
        if (tblIndicador != null) {
            tblIndicador.setDataSource(vctItens.getVctDados());
        }
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        recordAux.set("NOME", fldNome.getText());
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        bsc.xml.BIXMLVector cards = new bsc.xml.BIXMLVector("CARDS", "CARD");

        javax.swing.JInternalFrame[] internalFrames = dskDashboard.getAllFrames();

        for (int i = 0; i < internalFrames.length; i++) {
            BscCardFrame frame = (BscCardFrame) internalFrames[i];
            cards.add(frame.getRecordData());
        }

        recordAux.set(cards);

        recordAux.set("ORGANIZADO", chkAutoOrganizar.isSelected());

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return true;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRecord = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dskDashboard = new bsc.beans.JBICardDesktopPane();
        jPanel2 = new javax.swing.JPanel();
        lblMedida = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        chkAutoOrganizar = new javax.swing.JCheckBox();
        fldNome = new pv.jfcx.JPVEdit();
        btnAvaliacao = new javax.swing.JButton();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        lblAnalise = new javax.swing.JLabel();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        scrIndicador = new javax.swing.JScrollPane();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportacao = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        btnAjuda = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscMedidaDashboardFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_painel.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRecord.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setAutoscrolls(true);

        dskDashboard.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setViewportView(dskDashboard);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setPreferredSize(new java.awt.Dimension(0, 130));
        jPanel2.setLayout(null);

        lblMedida.setText(bundle.getString("BscMedidaDashboardFrame_00002")); // NOI18N
        jPanel2.add(lblMedida);
        lblMedida.setBounds(10, 40, 80, 20);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setText(bundle.getString("BscMedidaDashboardFrame_00004")); // NOI18N
        jPanel2.add(lblNome);
        lblNome.setBounds(10, 0, 70, 20);

        chkAutoOrganizar.setText(bundle.getString("BscMedidaDashboardFrame_00005")); // NOI18N
        chkAutoOrganizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAutoOrganizarActionPerformed(evt);
            }
        });
        jPanel2.add(chkAutoOrganizar);
        chkAutoOrganizar.setBounds(430, 100, 130, 18);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        jPanel2.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        jPanel2.add(btnAvaliacao);
        btnAvaliacao.setBounds(620, 20, 24, 22);

        lblDataAlvo.setText(bundle.getString("BscMedidaDashboardFrame_00012")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 15));
        jPanel2.add(lblDataAlvo);
        lblDataAlvo.setBounds(430, 0, 90, 20);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        jPanel2.add(fldDataAlvo);
        fldDataAlvo.setBounds(430, 20, 180, 22);

        lblAnalise.setText(bundle.getString("BscMedidaDashboardFrame_00013")); // NOI18N
        lblAnalise.setPreferredSize(new java.awt.Dimension(100, 15));
        jPanel2.add(lblAnalise);
        lblAnalise.setBounds(430, 40, 60, 20);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        jPanel2.add(fldAnalise);
        fldAnalise.setBounds(430, 60, 180, 22);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        jPanel2.add(btnAnalise);
        btnAnalise.setBounds(620, 60, 24, 22);
        jPanel2.add(scrIndicador);
        scrIndicador.setBounds(10, 60, 400, 60);

        pnlRecord.add(jPanel2, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 22));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscMedidaDashboardFrame_00006")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscMedidaDashboardFrame_00007")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(490, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(270, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscMedidaDashboardFrame_00008")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscMedidaDashboardFrame_00009")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscMedidaDashboardFrame_00010")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("BscMedidaDashboardFrame_00011")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportacao.gif"))); // NOI18N
        btnExportacao.setText(bundle.getString("BscMedidaDashboardFrame_00014")); // NOI18N
        btnExportacao.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportacao.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportacao.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportacao.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportacaoActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportacao);

        btnAbrir.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_download.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("BscMedidaDashboardFrame_00015")); // NOI18N
        btnAbrir.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAbrir.setMaximumSize(new java.awt.Dimension(76, 20));
        btnAbrir.setMinimumSize(new java.awt.Dimension(76, 20));
        btnAbrir.setPreferredSize(new java.awt.Dimension(76, 20));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        btnAjuda.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAjuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnAjuda.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnAjuda.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAjuda.setMaximumSize(new java.awt.Dimension(76, 20));
        btnAjuda.setMinimumSize(new java.awt.Dimension(76, 20));
        btnAjuda.setPreferredSize(new java.awt.Dimension(76, 20));
        btnAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjudaActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAjuda);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
            bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(), true);

            frmChooser.setDialogType(frmChooser.BSC_DIALOG_OPEN);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");

            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                try {
                    bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                    String strFileName = new String("mapest\\" + frmChooser.getFileName());
                    java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString() + strFileName);
                    bscApplet.getAppletContext().showDocument(url, "_blank");
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
	}//GEN-LAST:event_btnAbrirActionPerformed

	private void btnExportacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportacaoActionPerformed
            jScrollPane1.getVerticalScrollBar().setValue(1);
            bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(), true);
            frmChooser.setDialogType(frmChooser.BSC_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("graphs\\*.jpg");
            frmChooser.setFileName("panel.jpg");
            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                saveComponentAsJPEG(dskDashboard, "graphs\\" + frmChooser.getFileName());
            }
	}//GEN-LAST:event_btnExportacaoActionPerformed

	private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
            bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
                    new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
            bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
            bscAnaliseParceladaCard.setVisible(true);
	}//GEN-LAST:event_btnAnaliseActionPerformed

	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
            bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
                    new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
            bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
            bscPerformanceCard.setVisible(true);
	}//GEN-LAST:event_btnAvaliacaoActionPerformed

	private void btnAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjudaActionPerformed
            event.loadHelp("DASHBOARD");
	}//GEN-LAST:event_btnAjudaActionPerformed

	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
            printJob.setPrintable(dskDashboard);
            if (printJob.printDialog()) {
                try {
                    printJob.print();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
	}//GEN-LAST:event_btnPrintActionPerformed

	private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
            dskDashboard.autoOrganizeFrames();
	}//GEN-LAST:event_formComponentResized

	private void chkAutoOrganizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAutoOrganizarActionPerformed
            dskDashboard.setAutoOrganize(chkAutoOrganizar.isSelected());
            dskDashboard.autoOrganizeFrames();
	}//GEN-LAST:event_chkAutoOrganizarActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnAjuda;
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportacao;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkAutoOrganizar;
    private bsc.beans.JBICardDesktopPane dskDashboard;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAnalise;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblMedida;
    private javax.swing.JLabel lblNome;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scrIndicador;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscMedidaDashboardFrame(int operation, String idAux, String contextId) {

        initComponents();

        //Cria��o das tabela para selecao dos indicadores
        tblIndicador = new bsc.beans.JBIXMLTable() {

            protected void processMouseEvent(java.awt.event.MouseEvent e) {
                super.processMouseEvent(e);
                tabIndicadorMouseClicked(e);
            }
        };
        scrIndicador.setViewportView(tblIndicador);

        event.setNoServerMode(false);
        event.defaultConstructor(operation, idAux, contextId);
        
        // Se for para incluir novo painel.
        if (operation == 1) {
            forceSaveBeforeEdit();
        }
    }
    
    /*
     * M�todo que for�a o usu�rio gravar o painel 
     * apenas com o nome para apenas depois manipul�-lo.
     * @return Retorno Nulo
     * @Author Helio Leal
     */
    public void forceSaveBeforeEdit() {
        lblMedida.setEnabled(false);
        lblDataAlvo.setEnabled(false);
        lblAnalise.setEnabled(false);
        dskDashboard.setEnabled(false);
        int intElementos = dskDashboard.getComponentCount();
        for (int i = 0; i < intElementos; i++) {
            ((bsc.swing.analisys.BscIndicadorCardFrame) dskDashboard.getComponent(i)).setClosable(false);
        }
        chkAutoOrganizar.setEnabled(false);
        tblIndicador.setEnabled(false);
        btnAnalise.setEnabled(false);
        btnAvaliacao.setEnabled(false);
    }

    private void tabIndicadorMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getID() == evt.MOUSE_CLICKED) {
            tblIndicador.requestFocus();
            int row = tblIndicador.getSelectedRow();
            int col = tblIndicador.getFocusColumn();
            if (col == 0) {
                boolean rowSelected = (Boolean) tblIndicador.getValueAt(row, col);
                int cardId = tblIndicador.getXMLData().get(row).getInt("ID");
                if (rowSelected) {
                    addCard(cardId);
                } else {
                    removeCard(cardId);
                }
            }
        }
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    private void saveComponentAsJPEG(Component myComponent, String fileName) {
        Dimension size = myComponent.getSize();
        BufferedImage myImage = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = myImage.createGraphics();
        myComponent.paint(g2);
        //Grava a figura l� no servidor... :)
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            //JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            //encoder.encode(myImage);
            //Salva o arquivo de imagem como JPEG
            ImageIO.write(myImage, "jpg", out);        
            String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
            event.saveBase64(fileName, strBase64);
            out.close();
            //Abre o navegador para salvar a figura localmente
            bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
            java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString() + fileName);
            bscApplet.getAppletContext().showDocument(url, "_blank");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void addCard(int cardId) {
        bsc.xml.BIXMLVector cards = record.getBIXMLVector("CARDS");
        BscIndicadorCardFrame jif = null;
        boolean frameAlreadyExists = false;

        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getInt("ENTID") == cardId) {
                javax.swing.JInternalFrame[] internalFrames = dskDashboard.getAllFrames();
                frameAlreadyExists = false;

                for (int j = 0; j < internalFrames.length; j++) {
                    BscIndicadorCardFrame frame = (BscIndicadorCardFrame) internalFrames[j];
                    if (frame.getId().equals(cards.get(i).getString("ENTID"))) {
                        frameAlreadyExists = true;
                        break;
                    }
                }

                if (!frameAlreadyExists) {
                    jif = new BscIndicadorCardFrame(cards.get(i));
                    jif.setId(cards.get(i).getString("ENTID"));
                    jif.setClosable(false);
                    dskDashboard.add(jif);
                    jif.show();
                }
            }
        }
        dskDashboard.autoOrganizeFrames();
    }

    private void removeCard(int cardId) {
        bsc.xml.BIXMLVector cards = record.getBIXMLVector("CARDS");

        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getInt("ENTID") == cardId) {
                javax.swing.JInternalFrame[] internalFrames = dskDashboard.getAllFrames();

                for (int j = 0; j < internalFrames.length; j++) {
                    BscIndicadorCardFrame frame = (BscIndicadorCardFrame) internalFrames[j];
                    if (frame.getId().equals(cards.get(i).getString("ENTID"))) {
                        frame.dispose();
                        break;
                    }
                }
            }
        }
        dskDashboard.autoOrganizeFrames();
    }
}

/**
 *
 * @author Alexandre Alves da Silva
 */
class BscDashItens {

    private bsc.xml.BIXMLAttributes attIndicador = new bsc.xml.BIXMLAttributes();
    private bsc.xml.BIXMLVector vctDados = null;

    /** Creates a new instance of KpiTabIndicador */
    public BscDashItens() {
        vctDados = new bsc.xml.BIXMLVector("INDICADORES", "INDICADOR");
        setCabecPeriodo();
    }

    public bsc.xml.BIXMLVector getVctDados() {
        return vctDados;
    }

    public void createNewRecord(int id, String indicador, boolean selected) {
        bsc.xml.BIXMLRecord newRecord = new bsc.xml.BIXMLRecord("PLANILHA");
        newRecord.set("ID", id);
        newRecord.set("INDICADOR", indicador);
        newRecord.set("SELECIONADO", selected);
        //Novo vetor de dados.
        vctDados.add(newRecord);
    }

    private void setCabecPeriodo() {
        attIndicador = new bsc.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "VALOR");
        attIndicador.set("RETORNA", "F");

        //Coluna Selecao.
        attIndicador.set("TAG000", "SELECIONADO");
        attIndicador.set("CAB000", java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00023"));
        attIndicador.set("CLA000", bsc.beans.JBIXMLTable.BSC_BOOLEAN);
        attIndicador.set("EDT000", "T");
        attIndicador.set("CUM000", "F");

        //Coluna Indicador.
        attIndicador.set("TAG001", "INDICADOR");
        attIndicador.set("CAB001", java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00001"));
        attIndicador.set("CLA001", bsc.beans.JBIXMLTable.BSC_STRING);
        attIndicador.set("EDT001", "F");
        attIndicador.set("CUM001", "F");

        vctDados.setAttributes(attIndicador);
    }
}
