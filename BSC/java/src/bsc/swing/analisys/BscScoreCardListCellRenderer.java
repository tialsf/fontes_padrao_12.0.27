/*
 * BscScoreCardListCellRenderer.java
 *
 * Created on September 4, 2003, 5:30 PM
 */

package bsc.swing.analisys;

/**
 *
 * @author  siga1996
 */
class BscScoreCardListCellRenderer implements javax.swing.ListCellRenderer {
	public java.awt.Component getListCellRendererComponent( javax.swing.JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus ) {
		bsc.beans.JBIHyperlinkLabel label = new bsc.beans.JBIHyperlinkLabel();
		BscListItemCard item = (BscListItemCard) value;
		label.setID(item.getID());
		label.setTitle(item.getText());
		label.setType("INDICADOR");
		String s = item.getText();
		label.setText(s);
		label.setIcon( bsc.core.BscStaticReferences.getBscImageResources().getImage( item.getColor() ) );
		if (isSelected) {
			label.setBackground(list.getSelectionBackground());
			label.setForeground(list.getSelectionForeground());
		}
		else {
			label.setBackground(list.getBackground());
			label.setForeground(list.getForeground());
		}
		label.setEnabled(list.isEnabled());
		label.setFont(list.getFont());
		label.setOpaque(false);
		return label;
	}
 }
