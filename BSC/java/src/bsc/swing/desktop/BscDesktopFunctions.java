/*
 * BscDesktopFunctions.java
 *
 * Created on 13 de Outubro de 2004, 10:23
 */
package bsc.swing.desktop;

/**
 *
 * @author  alexandre silva
 */
public class BscDesktopFunctions {
    //Tipo da vis�o escolhida Pasta ou Janela.
    public bsc.xml.BIXMLRecord recDesktop;
    
    private String type = new String("DESKTOP");
    private bsc.swing.BscDefaultDialogSystem dialogSystem;
    private bsc.core.BscDataController dataController;
     
    /* Creates a new instance of BscDesktopFunctions */
    public BscDesktopFunctions() {
	dataController =  bsc.core.BscStaticReferences.getBscDataController();
	dialogSystem = new bsc.swing.BscDefaultDialogSystem(bsc.core.BscStaticReferences.getBscApplet());
	load();
    }
    
    //Carrega as propriedades da desktop;
    public void load(){
	bsc.xml.BIXMLRecord recordAux = dataController.loadRecord("1", type);
	if ( dataController.getStatus() == bsc.core.BscDataController.BSC_ST_OK ) {
	    recDesktop = recordAux;
	}
	else {
	    if ( dataController.getStatus() == bsc.core.BscDataController.BSC_ST_GENERALERROR )
		dialogSystem.errorMessage(dataController.getErrorMessage() );
	    else
		dialogSystem.errorMessage(dataController.getStatus() );

	    if ( dataController.getStatus() == bsc.core.BscDataController.BSC_ST_EXPIREDSESSION) {
		bsc.core.BscStaticReferences.getBscApplet().resetApplet();
	    }
	}	
    }
}