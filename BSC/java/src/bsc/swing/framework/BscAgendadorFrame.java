package bsc.swing.framework;


public class BscAgendadorFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("AGENDADOR");

	public void enableFields() {
		// Habilita os campos do formul�rio.
	}

	public void disableFields() {
		// Desabilita os campos do formul�rio.
	}
	
	java.util.Hashtable htbResponsavel = new java.util.Hashtable();

	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		listaAgendamento.setDataSource( record.getBIXMLVector("AGENDAMENTOS"), record.getString("ID"),
					    record.getString("CONTEXTID"), this.event);
		if(record.getBoolean("SITUACAO")){
		    btnExecutando.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/loading.gif")));
		    // Executando ...
		    fldSituacao.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscAgendadorFrame_00005"));
		}
		else{
		    btnExecutando.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/stopped.gif")));
		    // Parado!
		    fldSituacao.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscAgendadorFrame_00006"));
		}

	}

	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.

		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
	    boolean retorno = true;
	    return retorno;
	}
	
	public boolean hasCombos() {
		return false;
	}

	public void populateCombos(bsc.xml.BIXMLRecord serverSMTP) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTopForm = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        listaAgendamento = new bsc.beans.JBIListPanel();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlData = new javax.swing.JPanel();
        lblStatus = new javax.swing.JLabel();
        btnExecutando = new javax.swing.JButton();
        fldSituacao = new javax.swing.JLabel();
        pnlTools = new javax.swing.JPanel();
        pnlOperation = new javax.swing.JPanel();
        tbAction = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscAgendadorFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_agendador.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        tapCadastro.setMinimumSize(new java.awt.Dimension(100, 100));
        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));
        tapCadastro.addTab(bundle.getString("BscAgendadorFrame_00001"), listaAgendamento); // NOI18N

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlData.setPreferredSize(new java.awt.Dimension(478, 270));
        pnlData.setLayout(null);

        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText(bundle.getString("BscAgendadorFrame_00002")); // NOI18N
        pnlData.add(lblStatus);
        lblStatus.setBounds(20, 50, 90, 20);

        btnExecutando.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExecutando.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/loading.gif"))); // NOI18N
        btnExecutando.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(btnExecutando);
        btnExecutando.setBounds(20, 70, 150, 22);

        fldSituacao.setFont(new java.awt.Font("MS Sans Serif", 3, 11));
        pnlData.add(fldSituacao);
        fldSituacao.setBounds(90, 50, 170, 20);

        pnlFields.add(pnlData, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbAction.setFloatable(false);
        tbAction.setRollover(true);
        tbAction.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12));
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_play.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscAgendadorFrame_00007")); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbAction.add(btnEdit);

        btnStop.setFont(new java.awt.Font("Dialog", 0, 12));
        btnStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_parar.gif"))); // NOI18N
        btnStop.setText(bundle.getString("BscAgendadorFrame_00008")); // NOI18N
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });
        tbAction.add(btnStop);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12));
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscAgendadorFrame_00009")); // NOI18N
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbAction.add(btnReload);

        pnlOperation.add(tbAction, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        tapCadastro.addTab(bundle.getString("BscAgendadorFrame_00010"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName(bundle.getString("BscAgendamentoFrame_00001")); // NOI18N
        tapCadastro.getAccessibleContext().setAccessibleDescription("Agendamentos");

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	// Iniciando ...
        fldSituacao.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscAgendadorFrame_00003"));
	event.executeRecord("START");
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
	// Parando ...
        fldSituacao.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscAgendadorFrame_00004"));
	event.executeRecord("STOP");
    }//GEN-LAST:event_btnStopActionPerformed
						
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExecutando;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnStop;
    private javax.swing.JLabel fldSituacao;
    private javax.swing.JLabel lblStatus;
    private bsc.beans.JBIListPanel listaAgendamento;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbAction;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscAgendadorFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
	}
	
	public void showOperationsButtons() {
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}	
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

}
