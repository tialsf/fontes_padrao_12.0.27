package bsc.swing.framework;

import bsc.swing.BscDefaultFrameBehavior;
import bsc.swing.BscDefaultFrameFunctions;
import java.util.Hashtable;
import java.util.ResourceBundle;

public class BscAgendamentoFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    //Frequ�ncia
    private final int FREQ_DIARIO = 1;
    private final int FREQ_SEMANAL = 2;
    private final int FREQ_MENSAL = 3;
    //A��es
    private final int ACAO_REUNIAO = 0;
    private final int ACAO_IMPORTACAO = 1;
    //
    private String type = "AGENDAMENTO";
    private String environment;
    private bsc.xml.BIXMLVector vctReunioes = null;
    private bsc.xml.BIXMLVector vctDataSrc = null;
    private bsc.xml.BIXMLVector vctComandos = null;
    private Hashtable htbElemento = new Hashtable();
    private Hashtable htbAcao = new Hashtable();
    private final String diaSemana[][] = {{"2", "Segunda-Feira"},
        {"3", "Ter�a-Feira"},
        {"4", "Quarta-Feira"},
        {"5", "Quinta-Feira"},
        {"6", "Sexta-Feira"},
        {"7", "S�bado"},
        {"1", "Domingo"}};

    private void populateCboDiaSemana() {
        cboDia.removeAllItems();
        for (int i = 0; i < diaSemana.length; i++) {
            cboDia.addItem(diaSemana[i][1]);
        }
    }

    private String getCboDiaSemanaValue() {
        return diaSemana[cboDia.getSelectedIndex()][0];
    }

    private void setCboDiaSemanaValue(String value) {
        if (!(value.equals("") || value.equals("0"))) {
            for (int i = 0; i < diaSemana.length; i++) {
                if (diaSemana[i][0].equals(value)) {
                    cboDia.setSelectedIndex(i);
                    break;
                }
            }
        }
    }

    public void enableFields() {
        tapCadastro.setEnabledAt(0, true);
        tapCadastro.setEnabledAt(1, true);
        tapCadastro.setEnabledAt(2, true);

        // Habilita os campos do formul�rio.
        setEnableFields(pnlData, true);
        setEnableFields(pnlAgendar, true);
        setEnableFields(pnlAgendar_Frequencia, true);
        setEnableFields(pnlAgendar_Periodo, true);
        fldComando.setEnabled(true);

        // outros
        if (cbAcao.getSelectedIndex() == 4) {
            cbElemento.setEnabled(false);
            lblElemento.setEnabled(false);
            btnAbrir.setEnabled(false);
            fldComando.setEnabled(true);
        }
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        setEnableFields(pnlData, false);
        setEnableFields(pnlAgendar, false);
        setEnableFields(pnlAgendar_Frequencia, false);
        setEnableFields(pnlAgendar_Periodo, false);
        fldComando.setEnabled(false);
    }

    private void setEnableFields(javax.swing.JComponent compToEnable, boolean enable) {
        for (int i = 0; i < compToEnable.getComponentCount(); i++) {
            compToEnable.getComponent(i).setEnabled(enable);
        }
    }

    public void refreshFields() {
        int nFreq = record.getInt("FREQ");

        fldNome.setText(record.getString("NOME"));
        fldDataIni.setCalendar(record.getDate("DATAINI"));
        fldHorarioIni.setText(record.getString("HORAINI"));
        fldDataFim.setCalendar(record.getDate("DATAFIM"));
        fldHorarioFim.setText(record.getString("HORAFIM"));
        fldFreq_HorarioFire.setText(record.getString("HORAFIRE"));
        fldDataExe.setCalendar(record.getDate("DATAEXE"));
        fldHorarioExe.setText(record.getString("HORAEXE"));
        fldDataNext.setCalendar(record.getDate("DATANEXT"));
        fldHorarioNext.setText(record.getString("HORANEXT"));
        fldAmbiente.setText(record.getString("ENV"));
        fldComando.setText(record.getString("ACAO"));
        populateCboDiaSemana();

        //carregar o ID da reuniao para trazer selecionado
        this.vctComandos = record.getBIXMLVector("COMANDOS");
        this.vctDataSrc = record.getBIXMLVector("DATASRCS");
        this.vctReunioes = record.getBIXMLVector("REUNIOES");

        event.populateCombo(record.getBIXMLVector("COMANDOS"), "ACAOATUAL", htbAcao, cbAcao);
        cbAcao.removeItemAt(0);
        htbAcao.remove(0);
        cbAcao.setSelectedIndex(record.getInt("IDACAO"));

        if (record.getInt("IDACAO") == ACAO_REUNIAO) {
            event.populateCombo(vctReunioes, "", htbElemento, cbElemento, "ORGREU");
            event.selectComboItem(cbElemento, record.getInt("ELEMENTO"));
        } else if (record.getInt("IDACAO") == ACAO_IMPORTACAO) {
            event.populateCombo(vctDataSrc, "", htbElemento, cbElemento);
            event.selectComboItem(cbElemento, record.getInt("ELEMENTO"));
        }

        if (status == BscDefaultFrameBehavior.INSERTING) {
            rbDiario.setSelected(true);
            spnDia.setValue(1);
            cbAcao.setSelectedIndex(0);
            fldDataIni.setCalendar(new java.util.GregorianCalendar());
            fldDataFim.setCalendar(new java.util.GregorianCalendar());
            fldHorarioIni.setText("00:00");
            fldHorarioFim.setText("23:59");
            fldFreq_HorarioFire.setText("23:59");
            cbAcao.setEnabled(true);
        } else {
            switch (nFreq) {
                case FREQ_DIARIO:	//Diario
                    rbDiario.setSelected(true);
                    spnDia.setValue(1);
                    break;
                case FREQ_SEMANAL:	//Semanal
                    rbSemanal.setSelected(true);
                    setCboDiaSemanaValue(record.getString("DIAFIRE"));
                    spnDia.setValue(1);
                    break;
                case FREQ_MENSAL:	//Mensal
                    rbMensal.setSelected(true);
                    spnDia.setValue(record.getInt("DIAFIRE"));
                    break;
            }
        }

        if (vctReunioes.size() > 0) {
            environment = vctReunioes.get(0).getString("CPATH");
        } else {
            environment = "";
        }
    }

    @Override
    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }

        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("ACAO", fldComando.getText());
        recordAux.set("DATAINI", fldDataIni.getCalendar());
        recordAux.set("DATAFIM", fldDataFim.getCalendar());
        recordAux.set("HORAINI", fldHorarioIni.getText());
        recordAux.set("HORAFIM", fldHorarioFim.getText());
        recordAux.set("ENV", fldAmbiente.getText());
        recordAux.set("HORAFIRE", fldFreq_HorarioFire.getText());

        if (rbMensal.isSelected()) {
            recordAux.set("FREQ", FREQ_MENSAL);
            recordAux.set("DIAFIRE", (Integer) spnDia.getValue());
        } else if (rbSemanal.isSelected()) {
            recordAux.set("FREQ", FREQ_SEMANAL);
            recordAux.set("DIAFIRE", Integer.parseInt(getCboDiaSemanaValue()));
        } else {
            recordAux.set("FREQ", FREQ_DIARIO);
            recordAux.set("DIAFIRE", 0);
        }

        recordAux.set("IDACAO", cbAcao.getSelectedIndex());
        recordAux.set("ELEMENTO", cbElemento.getSelectedIndex());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    @Override
    public boolean hasCombos() {
        return true;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlTarefa = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlData = new javax.swing.JPanel();
        lblAcao = new javax.swing.JLabel();
        lblAmbiente = new javax.swing.JLabel();
        fldAmbiente = new pv.jfcx.JPVEdit();
        cbAcao = new javax.swing.JComboBox();
        lblComando = new javax.swing.JLabel();
        jScrolAcao = new javax.swing.JScrollPane();
        fldComando = new javax.swing.JTextArea();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        btnAbrir = new javax.swing.JButton();
        cbElemento = new javax.swing.JComboBox();
        lblElemento = new javax.swing.JLabel();
        pnlAgendar = new javax.swing.JPanel();
        pnlAgendar_Periodo = new javax.swing.JPanel();
        lblDataIni = new javax.swing.JLabel();
        fldDataIni = new pv.jfcx.JPVDatePlus();
        fldHorarioIni = new pv.jfcx.JPVTime();
        lblDataFim = new javax.swing.JLabel();
        fldDataFim = new pv.jfcx.JPVDatePlus();
        fldHorarioFim = new pv.jfcx.JPVTime();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        pnlAgendar_Frequencia = new javax.swing.JPanel();
        rbDiario = new javax.swing.JRadioButton();
        rbSemanal = new javax.swing.JRadioButton();
        rbMensal = new javax.swing.JRadioButton();
        fldFreq_HorarioFire = new pv.jfcx.JPVTime();
        cboDia = new javax.swing.JComboBox();
        spnDia = new javax.swing.JSpinner();
        lblFreq_Horario = new javax.swing.JLabel();
        lblfreq_Dia = new javax.swing.JLabel();
        pnlDetalhes = new javax.swing.JPanel();
        pnlDetalhe_UltExec = new javax.swing.JPanel();
        lblDataExe = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        fldDataExe = new pv.jfcx.JPVDatePlus();
        fldHorarioExe = new pv.jfcx.JPVTime();
        pnlDetalhe_ProxExec = new javax.swing.JPanel();
        lblDataExe2 = new javax.swing.JLabel();
        fldDataNext = new pv.jfcx.JPVDatePlus();
        fldHorarioNext = new pv.jfcx.JPVTime();
        jLabel7 = new javax.swing.JLabel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscAgendamentoFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_compromisso.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        tapCadastro.setMinimumSize(new java.awt.Dimension(548, 403));

        pnlTarefa.setPreferredSize(new java.awt.Dimension(498, 330));
        pnlTarefa.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlData.setPreferredSize(new java.awt.Dimension(465, 290));
        pnlData.setLayout(null);

        lblAcao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAcao.setText(bundle.getString("BscAgendamentoFrame_00003")); // NOI18N
        lblAcao.setEnabled(false);
        lblAcao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAcao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAcao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblAcao);
        lblAcao.setBounds(10, 50, 80, 20);

        lblAmbiente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAmbiente.setText(bundle.getString("BscAgendamentoFrame_00008")); // NOI18N
        lblAmbiente.setEnabled(false);
        lblAmbiente.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblAmbiente);
        lblAmbiente.setBounds(10, 130, 90, 20);

        fldAmbiente.setMaxLength(160);
        pnlData.add(fldAmbiente);
        fldAmbiente.setBounds(10, 150, 400, 22);

        cbAcao.setEnabled(false);
        cbAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAcaoActionPerformed(evt);
            }
        });
        pnlData.add(cbAcao);
        cbAcao.setBounds(10, 70, 400, 22);

        lblComando.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblComando.setText(bundle.getString("BscAgendamentoFrame_00005")); // NOI18N
        lblComando.setEnabled(false);
        lblComando.setMaximumSize(new java.awt.Dimension(100, 16));
        lblComando.setMinimumSize(new java.awt.Dimension(100, 16));
        lblComando.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblComando);
        lblComando.setBounds(10, 170, 170, 20);

        jScrolAcao.setBorder(null);
        jScrolAcao.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrolAcao.setPreferredSize(new java.awt.Dimension(6, 57));

        fldComando.setLineWrap(true);
        fldComando.setRows(2);
        fldComando.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fldComando.setEnabled(false);
        jScrolAcao.setViewportView(fldComando);

        pnlData.add(jScrolAcao);
        jScrolAcao.setBounds(10, 190, 400, 70);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscAgendamentoFrame_00002")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 80, 20);

        fldNome.setMaxLength(160);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        btnAbrir.setText("...");
        btnAbrir.setToolTipText("Abrir elemento");
        btnAbrir.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAbrir.setEnabled(false);
        btnAbrir.setName("btnAbrir"); // NOI18N
        btnAbrir.setPreferredSize(new java.awt.Dimension(15, 22));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        pnlData.add(btnAbrir);
        btnAbrir.setBounds(420, 110, 26, 22);

        cbElemento.setEnabled(false);
        cbElemento.setPreferredSize(new java.awt.Dimension(26, 23));
        cbElemento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbElementoItemStateChanged(evt);
            }
        });
        pnlData.add(cbElemento);
        cbElemento.setBounds(10, 110, 400, 22);

        lblElemento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblElemento.setText(bundle.getString("BscAgendamentoFrame_00004")); // NOI18N
        lblElemento.setEnabled(false);
        pnlData.add(lblElemento);
        lblElemento.setBounds(10, 90, 90, 20);

        pnlFields.add(pnlData, java.awt.BorderLayout.CENTER);

        pnlTarefa.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscAgendamentoFrame_00012"), pnlTarefa); // NOI18N

        pnlAgendar.setLayout(null);

        pnlAgendar_Periodo.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscAgendamentoFrame_00022"))); // NOI18N
        pnlAgendar_Periodo.setLayout(null);

        lblDataIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataIni.setText(bundle.getString("BscAgendamentoFrame_00015")); // NOI18N
        lblDataIni.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataIni.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataIni.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlAgendar_Periodo.add(lblDataIni);
        lblDataIni.setBounds(10, 20, 85, 16);

        fldDataIni.setDialog(true);
        fldDataIni.setUseLocale(true);
        fldDataIni.setWaitForCalendarDate(true);
        pnlAgendar_Periodo.add(fldDataIni);
        fldDataIni.setBounds(10, 40, 180, 22);

        fldHorarioIni.setLeadingZero(true);
        fldHorarioIni.setShowSeconds(false);
        fldHorarioIni.setTwelveHours(false);
        pnlAgendar_Periodo.add(fldHorarioIni);
        fldHorarioIni.setBounds(230, 40, 180, 22);

        lblDataFim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataFim.setText(bundle.getString("BscAgendamentoFrame_00016")); // NOI18N
        lblDataFim.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataFim.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataFim.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlAgendar_Periodo.add(lblDataFim);
        lblDataFim.setBounds(10, 70, 85, 16);

        fldDataFim.setDialog(true);
        fldDataFim.setUseLocale(true);
        fldDataFim.setWaitForCalendarDate(true);
        pnlAgendar_Periodo.add(fldDataFim);
        fldDataFim.setBounds(10, 90, 180, 22);

        fldHorarioFim.setLeadingZero(true);
        fldHorarioFim.setShowSeconds(false);
        fldHorarioFim.setTwelveHours(false);
        pnlAgendar_Periodo.add(fldHorarioFim);
        fldHorarioFim.setBounds(230, 90, 180, 22);

        jLabel3.setText(bundle.getString("BscAgendamentoFrame_00020")); // NOI18N
        pnlAgendar_Periodo.add(jLabel3);
        jLabel3.setBounds(230, 20, 50, 14);

        jLabel4.setText(bundle.getString("BscAgendamentoFrame_00020")); // NOI18N
        pnlAgendar_Periodo.add(jLabel4);
        jLabel4.setBounds(230, 70, 50, 14);

        pnlAgendar.add(pnlAgendar_Periodo);
        pnlAgendar_Periodo.setBounds(10, 10, 430, 130);

        pnlAgendar_Frequencia.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscAgendamentoFrame_00023"))); // NOI18N
        pnlAgendar_Frequencia.setLayout(null);

        buttonGroup1.add(rbDiario);
        rbDiario.setText(bundle.getString("BscAgendamentoFrame_00017")); // NOI18N
        rbDiario.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbDiario.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rbDiario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbDiarioItemStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbDiario);
        rbDiario.setBounds(50, 25, 90, 30);

        buttonGroup1.add(rbSemanal);
        rbSemanal.setText(bundle.getString("BscAgendamentoFrame_00018")); // NOI18N
        rbSemanal.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbSemanal.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rbSemanal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbSemanalItemStateChanged(evt);
            }
        });
        rbSemanal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSemanalActionPerformed(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbSemanal);
        rbSemanal.setBounds(50, 55, 90, 30);

        buttonGroup1.add(rbMensal);
        rbMensal.setText(bundle.getString("BscAgendamentoFrame_00019")); // NOI18N
        rbMensal.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        rbMensal.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rbMensal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbMensalItemStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(rbMensal);
        rbMensal.setBounds(50, 85, 90, 30);

        fldFreq_HorarioFire.setLeadingZero(true);
        fldFreq_HorarioFire.setShowSeconds(false);
        fldFreq_HorarioFire.setTwelveHours(false);
        pnlAgendar_Frequencia.add(fldFreq_HorarioFire);
        fldFreq_HorarioFire.setBounds(230, 40, 180, 22);
        pnlAgendar_Frequencia.add(cboDia);
        cboDia.setBounds(230, 90, 180, 22);

        spnDia.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnDiaStateChanged(evt);
            }
        });
        pnlAgendar_Frequencia.add(spnDia);
        spnDia.setBounds(230, 90, 40, 20);

        lblFreq_Horario.setText(bundle.getString("BscAgendamentoFrame_00020")); // NOI18N
        pnlAgendar_Frequencia.add(lblFreq_Horario);
        lblFreq_Horario.setBounds(230, 20, 50, 14);

        lblfreq_Dia.setText(bundle.getString("BscAgendamentoFrame_00021")); // NOI18N
        pnlAgendar_Frequencia.add(lblfreq_Dia);
        lblfreq_Dia.setBounds(230, 70, 50, 14);

        pnlAgendar.add(pnlAgendar_Frequencia);
        pnlAgendar_Frequencia.setBounds(10, 150, 430, 130);

        tapCadastro.addTab(bundle.getString("BscAgendamentoFrame_00013"), pnlAgendar); // NOI18N

        pnlDetalhes.setLayout(null);

        pnlDetalhe_UltExec.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscAgendamentoFrame_00024"))); // NOI18N
        pnlDetalhe_UltExec.setLayout(null);

        lblDataExe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataExe.setText(bundle.getString("BscAgendamentoFrame_00026")); // NOI18N
        lblDataExe.setEnabled(false);
        lblDataExe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataExe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataExe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDetalhe_UltExec.add(lblDataExe);
        lblDataExe.setBounds(10, 20, 50, 16);

        jLabel6.setText(bundle.getString("BscAgendamentoFrame_00020")); // NOI18N
        jLabel6.setEnabled(false);
        pnlDetalhe_UltExec.add(jLabel6);
        jLabel6.setBounds(230, 20, 50, 14);

        fldDataExe.setDialog(true);
        fldDataExe.setEnabled(false);
        fldDataExe.setUseLocale(true);
        fldDataExe.setWaitForCalendarDate(true);
        pnlDetalhe_UltExec.add(fldDataExe);
        fldDataExe.setBounds(10, 40, 180, 22);

        fldHorarioExe.setEnabled(false);
        fldHorarioExe.setShowSeconds(false);
        fldHorarioExe.setTwelveHours(false);
        pnlDetalhe_UltExec.add(fldHorarioExe);
        fldHorarioExe.setBounds(230, 40, 180, 22);

        pnlDetalhes.add(pnlDetalhe_UltExec);
        pnlDetalhe_UltExec.setBounds(10, 10, 430, 90);

        pnlDetalhe_ProxExec.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscAgendamentoFrame_00025"))); // NOI18N
        pnlDetalhe_ProxExec.setLayout(null);

        lblDataExe2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataExe2.setText(bundle.getString("BscAgendamentoFrame_00026")); // NOI18N
        lblDataExe2.setEnabled(false);
        lblDataExe2.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDataExe2.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDataExe2.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDetalhe_ProxExec.add(lblDataExe2);
        lblDataExe2.setBounds(10, 20, 50, 16);

        fldDataNext.setDialog(true);
        fldDataNext.setEnabled(false);
        fldDataNext.setUseLocale(true);
        fldDataNext.setWaitForCalendarDate(true);
        pnlDetalhe_ProxExec.add(fldDataNext);
        fldDataNext.setBounds(10, 40, 180, 22);

        fldHorarioNext.setEnabled(false);
        fldHorarioNext.setShowSeconds(false);
        fldHorarioNext.setTwelveHours(false);
        pnlDetalhe_ProxExec.add(fldHorarioNext);
        fldHorarioNext.setBounds(230, 40, 180, 22);

        jLabel7.setText(bundle.getString("BscAgendamentoFrame_00020")); // NOI18N
        jLabel7.setEnabled(false);
        pnlDetalhe_ProxExec.add(jLabel7);
        jLabel7.setBounds(230, 20, 50, 14);

        pnlDetalhes.add(pnlDetalhe_ProxExec);
        pnlDetalhe_ProxExec.setBounds(10, 110, 430, 90);

        tapCadastro.addTab(bundle.getString("BscAgendamentoFrame_00014"), pnlDetalhes); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscRetornoFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscRetornoFrame_00008")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",  bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnEdit.setText(bundle1.getString("BscRetornoFrame_00005")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscRetornoFrame_00011")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscRetornoFrame_00003")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void spnDiaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnDiaStateChanged
            int nValue = (Integer) spnDia.getValue();
            if (nValue > 31) {
                spnDia.setValue(31);
            } else if (nValue < 1) {
                spnDia.setValue(1);
            }
	}//GEN-LAST:event_spnDiaStateChanged

	private void rbMensalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbMensalItemStateChanged
            setFrequency(3);
	}//GEN-LAST:event_rbMensalItemStateChanged

	private void rbSemanalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbSemanalItemStateChanged
            setFrequency(2);
	}//GEN-LAST:event_rbSemanalItemStateChanged

	private void rbDiarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbDiarioItemStateChanged
            setFrequency(1);
	}//GEN-LAST:event_rbDiarioItemStateChanged

    private void setFrequency(int action) {
        switch (action) {
            case FREQ_DIARIO: //Di�rio
                cboDia.setVisible(false);
                spnDia.setVisible(false);
                lblfreq_Dia.setVisible(false);
                break;
            case FREQ_SEMANAL: //Semanal
                cboDia.setVisible(true);
                spnDia.setVisible(false);
                lblfreq_Dia.setVisible(true);
                break;
            case FREQ_MENSAL: //Mensal
                cboDia.setVisible(false);
                spnDia.setVisible(true);
                lblfreq_Dia.setVisible(true);
        }
    }

    private void cbAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAcaoActionPerformed
        int nPos = cbAcao.getSelectedIndex();
        if (!(status == BscDefaultFrameBehavior.INSERTING || status == BscDefaultFrameBehavior.UPDATING)) {
            nPos = -1;
        }
        switch (nPos) {
            //notificar reunioes
            case 0:
                event.populateCombo(vctReunioes, "", htbElemento, cbElemento, "ORGREU");
                cbElemento.setEnabled(true);
                lblElemento.setEnabled(true);
                btnAbrir.setEnabled(true);
                fldComando.setEnabled(false);
                break;
            //importar fonte de Dados
            case 1:
                event.populateCombo(vctDataSrc, "", htbElemento, cbElemento);
                cbElemento.setEnabled(true);
                lblElemento.setEnabled(true);
                btnAbrir.setEnabled(true);
                fldComando.setEnabled(false);
                break;
            //notificar prazo de iniciativas
            case 2:
                cbElemento.setEnabled(false);
                cbElemento.removeAllItems();
                lblElemento.setEnabled(false);
                btnAbrir.setEnabled(false);
                fldComando.setText(vctComandos.get(cbAcao.getSelectedIndex()).getString("ACAO") + "('" + environment.trim() + "')");
                fldComando.setEnabled(false);
                break;
            //notificar prazo de tarefas
            case 3:
                cbElemento.setEnabled(false);
                lblElemento.setEnabled(false);
                btnAbrir.setEnabled(false);
                fldComando.setText(vctComandos.get(cbAcao.getSelectedIndex()).getString("ACAO") + "('" + environment.trim() + "')");
                fldComando.setEnabled(false);
                break;
            //outros
            case 4:
                cbElemento.setEnabled(false);
                cbElemento.removeAllItems();
                lblElemento.setEnabled(false);
                btnAbrir.setEnabled(false);
                fldComando.setEnabled(true);
                break;
        }
    }//GEN-LAST:event_cbAcaoActionPerformed

    private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
        int i = cbAcao.getSelectedIndex();
        if (cbElemento.getSelectedItem().toString().trim().length() > 0) {
            if (i == 0) {
                bsc.core.BscStaticReferences.getBscFormController().getForm("REUNIAO", htbElemento.get(cbElemento.getSelectedItem()).toString(), cbElemento.getItemAt(cbElemento.getSelectedIndex()).toString());
            }
            if (i == 1) {
                BscDefaultFrameFunctions form = bsc.core.BscStaticReferences.getBscFormController().getForm("DATASRC", htbElemento.get(cbElemento.getSelectedItem()).toString(), cbElemento.getItemAt(cbElemento.getSelectedIndex()).toString());
            }
        }
    }//GEN-LAST:event_btnAbrirActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("CADASTRANDO_INICIATIVAS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

        private void cbElementoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbElementoItemStateChanged
        int indice = cbElemento.getSelectedIndex();
            if (indice > -1) {
                if (cbElemento.getSelectedItem().toString().trim().length() > 0) {
                    String comando = "";
                    if (cbAcao.getSelectedIndex() >= 0) {
                        comando = vctComandos.get(cbAcao.getSelectedIndex()).getString("ACAO");
                    }
                    fldComando.setText(comando + "(" + htbElemento.get(cbElemento.getSelectedItem()).toString().trim() + ",'" + environment.trim() + "')");
                }
            } else {
                if (status == BscDefaultFrameBehavior.INSERTING || status == BscDefaultFrameBehavior.UPDATING) {
                    fldComando.setText("");
                }
            }
        }//GEN-LAST:event_cbElementoItemStateChanged

    private void rbSemanalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSemanalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbSemanalActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbAcao;
    private javax.swing.JComboBox cbElemento;
    private javax.swing.JComboBox cboDia;
    private pv.jfcx.JPVEdit fldAmbiente;
    private javax.swing.JTextArea fldComando;
    private pv.jfcx.JPVDatePlus fldDataExe;
    private pv.jfcx.JPVDatePlus fldDataFim;
    private pv.jfcx.JPVDatePlus fldDataIni;
    private pv.jfcx.JPVDatePlus fldDataNext;
    private pv.jfcx.JPVTime fldFreq_HorarioFire;
    private pv.jfcx.JPVTime fldHorarioExe;
    private pv.jfcx.JPVTime fldHorarioFim;
    private pv.jfcx.JPVTime fldHorarioIni;
    private pv.jfcx.JPVTime fldHorarioNext;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrolAcao;
    private javax.swing.JLabel lblAcao;
    private javax.swing.JLabel lblAmbiente;
    private javax.swing.JLabel lblComando;
    private javax.swing.JLabel lblDataExe;
    private javax.swing.JLabel lblDataExe2;
    private javax.swing.JLabel lblDataFim;
    private javax.swing.JLabel lblDataIni;
    private javax.swing.JLabel lblElemento;
    private javax.swing.JLabel lblFreq_Horario;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblfreq_Dia;
    private javax.swing.JPanel pnlAgendar;
    private javax.swing.JPanel pnlAgendar_Frequencia;
    private javax.swing.JPanel pnlAgendar_Periodo;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDetalhe_ProxExec;
    private javax.swing.JPanel pnlDetalhe_UltExec;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTarefa;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton rbDiario;
    private javax.swing.JRadioButton rbMensal;
    private javax.swing.JRadioButton rbSemanal;
    private javax.swing.JSpinner spnDia;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscAgendamentoFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return "";
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
