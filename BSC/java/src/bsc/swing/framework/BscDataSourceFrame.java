package bsc.swing.framework;

import bsc.core.BscStaticReferences;
import bsc.swing.BscDefaultDialogSystem;
import bsc.swing.BscDefaultFrameBehavior;
import bsc.swing.BscFileChooser;
import java.awt.event.ItemEvent;
import java.util.ResourceBundle;

public class BscDataSourceFrame extends bsc.swing.BscInternalExecute implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("DATASRC");
    public static int BSC_SRC_ENVIRONMENT = 1;
    public static int BSC_SRC_CUSTOM = 2;
    private ResourceBundle resBundle = ResourceBundle.getBundle("international", BscStaticReferences.getBscDefaultLocale());
    private bsc.xml.BIXMLRecord recDataWareHouse = null;
    private java.util.Hashtable htbClasse = new java.util.Hashtable(), htbDataWareHouse = new java.util.Hashtable(), htbCampos = new java.util.Hashtable(), htbIndicadores = new java.util.Hashtable(), htbConsultas = new java.util.Hashtable();
    private bsc.xml.BIXMLVector backConsultas = null, vctConsultaSel = null;
    private String idConsultaDW = new String("");
    private final String conexSrv[][] = {{"", ""},
        {"TCPIP", "TCP/IP"},
        {"LOCAL", "Local"},
        {"APPC", "APPC"},
        {"BRIDGE", "Bridge"},
        {"NPIPE", "NPipe"}};
    private final String conexBco[][] = {{"", ""},
        {"MSSQL", "Sql Server"},
        {"ORACLE", "Oracle"},
        {"SYBASE", "Sybase"},
        {"INFORMIX", "Informix"},
        {"POSTGRES", "Postgres"},
        {"MYSQL", "MySQL"},
        {"DB2", "DB2"},
        {"MSADO", "MS ADO"}};
    private static final String DATA_SRC_RESULTADO = "resultado";
    private static final String DATA_SRC_REFERENCE = "referencia";
    private static final String DATA_SRC_REFMETAS = "valormetas";
    private final String dataSourceTypes[][] = {{"", ""},
        {DATA_SRC_RESULTADO, resBundle.getString("BscDataSourceFrame_00031")},
        {DATA_SRC_REFERENCE, resBundle.getString("BscDataSourceFrame_00021")},
        {DATA_SRC_REFMETAS, resBundle.getString("BscDataSourceFrame_00032")}};
    private boolean isImporting = false;

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblAgendamento.setEnabled(true);
        lblClasse.setEnabled(true);
        cbbClasse.setEnabled(true);
        //chkRecriar.setEnabled(true);

        radAmbiente.setEnabled(true);
        radTopConnect.setEnabled(true);
        if (cbbClasse.getSelectedIndex() == 2) {
            radAmbiente.setSelected(true);
            radTopConnect.setEnabled(false);
        }

        enableTopFields();
        lblDeclara.setEnabled(true);
        txtDeclaracoes.setEnabled(true);

        //Pasta	DW
        lblEnderecoDW.setEnabled(true);
        fldUrlDW.setEnabled(true);
        btnPostEndereco.setEnabled(true);
        cbbIndDW.setEnabled(true);
        lblIndDW.setEnabled(true);
        cbbDataDW.setEnabled(true);
        lblDataDW.setEnabled(true);
        lblConsulta.setEnabled(true);
        cbbConsultas.setEnabled(true);
        lblDatawarehouse.setEnabled(true);
        cbbDataWareHouse.setEnabled(true);

        fldDWSel.setEnabled(false);
        fldDwConsultaSel.setEnabled(false);
        fldDWDataSel.setEnabled(false);
        fldIndDWSel.setEnabled(false);

        lblDSType.setEnabled(true);
        cbbDSType.setEnabled(true);
    }

    void enableTopFields() {
        lblAmbiente.setEnabled(radAmbiente.isSelected());
        fldAmbiente.setEnabled(radAmbiente.isSelected());

        lblDatabase.setEnabled(radTopConnect.isSelected());
        cboDataBase.setEnabled(radTopConnect.isSelected());
        lblAlias.setEnabled(radTopConnect.isSelected());
        fldAlias.setEnabled(radTopConnect.isSelected());
        lbServer.setEnabled(radTopConnect.isSelected());
        fldServer.setEnabled(radTopConnect.isSelected());
        lblConType.setEnabled(radTopConnect.isSelected());
        cboConType.setEnabled(radTopConnect.isSelected());
    }

    void setVisibleTabs() {
        //Tela do DataWare House.
        if (cbbClasse.getSelectedIndex() == 3) {
            paneDataSource.add(resBundle.getString("BscDataSourceFrame_00027"), pnlConsulta);
            paneDataSource.remove(pnlDeclaracao);
            paneDataSource.remove(pnlConfiguracao);
        } else {
            paneDataSource.addTab(resBundle.getString("BscDataSourceFrame_00020"), pnlDeclaracao);
            paneDataSource.addTab(resBundle.getString("BscDataSourceFrame_00023"), pnlConfiguracao);
            paneDataSource.remove(pnlConsulta);
        }
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblAgendamento.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblClasse.setEnabled(false);
        cbbClasse.setEnabled(false);
        //chkRecriar.setEnabled(false);

        radAmbiente.setEnabled(false);
        lblAmbiente.setEnabled(false);
        fldAmbiente.setEnabled(false);

        radTopConnect.setEnabled(false);
        lblDatabase.setEnabled(false);
        cboDataBase.setEnabled(false);
        lblAlias.setEnabled(false);
        fldAlias.setEnabled(false);
        lbServer.setEnabled(false);
        fldServer.setEnabled(false);
        lblConType.setEnabled(false);
        cboConType.setEnabled(false);

        txtDeclaracoes.setEnabled(false);

        lblEnderecoDW.setEnabled(false);
        fldUrlDW.setEnabled(false);
        btnPostEndereco.setEnabled(false);
        cbbIndDW.setEnabled(false);
        lblIndDW.setEnabled(false);
        cbbDataDW.setEnabled(false);
        lblDataDW.setEnabled(false);
        lblConsulta.setEnabled(false);
        cbbConsultas.setEnabled(false);
        lblDatawarehouse.setEnabled(false);
        cbbDataWareHouse.setEnabled(false);
        fldDWSel.setEnabled(false);
        fldDwConsultaSel.setEnabled(false);
        fldDWDataSel.setEnabled(false);
        fldIndDWSel.setEnabled(false);

        lblDSType.setEnabled(false);
        cbbDSType.setEnabled(false);
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        populateCboBanco();
        populateCboServer();
        populateCboDSType();

        fldNome.setText(record.getString("NOME"));
        lblCodigo.setText(record.getString("ID"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        event.populateCombo(record.getBIXMLVector("CLASSES"), "CLASSE", htbClasse, cbbClasse);
        cbbClasse.setSelectedIndex(record.getInt("CLASSE"));
        //chkRecriar.setSelected(record.getBoolean("RECRIA"));

        radAmbiente.setSelected(record.getInt("TIPOENV") == BSC_SRC_ENVIRONMENT);
        fldAmbiente.setText(record.getString("ENVIRON"));
        radTopConnect.setSelected(record.getInt("TIPOENV") == BSC_SRC_CUSTOM);
        setCboBancoValue(record.getString("TOPDB"));
        fldAlias.setText(record.getString("TOPALIAS"));
        fldServer.setText(record.getString("TOPSERVER"));
        setCboServerValue(record.getString("TOPCONTYPE"));

        txtDeclaracoes.setText(record.getString("TEXTO"));

        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");
        pnlHeader.setComponents(vctContexto);

        //Dados da consulta do DW.
        fldUrlDW.setText(record.getString("URL"));
        fldDWSel.setText(record.getString("DW"));
        fldDwConsultaSel.setText(record.getString("CONSULTA"));
        fldDWDataSel.setText(record.getString("CPO_DATA"));
        fldIndDWSel.setText(record.getString("INDICADOR"));
        idConsultaDW = record.getString("IDCONS");
        setVisibleTabs();
        setCboDSTypeValue(record.getString("TIPODS"));
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("CLASSE", cbbClasse.getSelectedIndex());
        //recordAux.set( "RECRIA", chkRecriar.isSelected() );

        recordAux.set("TIPOENV", (radAmbiente.isSelected() ? BSC_SRC_ENVIRONMENT : BSC_SRC_CUSTOM));
        recordAux.set("ENVIRON", fldAmbiente.getText());
        recordAux.set("TOPDB", getCboBancoValue());
        recordAux.set("TOPALIAS", fldAlias.getText());
        recordAux.set("TOPSERVER", fldServer.getText());
        recordAux.set("TOPCONTYPE", getCboServerValue());

        recordAux.set("TEXTO", txtDeclaracoes.getText());

        //Registro do DW.
        recordAux.set("URL", fldUrlDW.getText());
        recordAux.set("DW", fldDWSel.getText());
        recordAux.set("CONSULTA", fldDwConsultaSel.getText());
        recordAux.set("CPO_DATA", fldDWDataSel.getText());
        recordAux.set("INDICADOR", fldIndDWSel.getText());
        String tmpReg = event.getComboValue(htbConsultas, cbbConsultas);

        if (tmpReg.trim().length() == 0 || tmpReg.equals("0")) {
            tmpReg = idConsultaDW;
        }

        recordAux.set("IDCONS", tmpReg);

        recordAux.set("REFER", isDSReference());
        recordAux.set("TIPODS", getCboDSTypeValue());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        if ((fldDWSel.getText().trim().length() == 0 ||
                fldDwConsultaSel.getText().trim().length() == 0 ||
                fldDWDataSel.getText().trim().length() == 0) &&
                cbbClasse.getSelectedIndex() == 3) {

            errorMessage.append(resBundle.getString("BscDataSourceFrame_00028"));

        } else if (fldNome.getText().trim().length() == 0) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));

        } else {
            valid = true;
        }

        return valid;
    }

    @Override
    public boolean hasCombos() {
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTopConnect = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnImportar = new javax.swing.JButton();
        btnLog = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        paneDataSource = new javax.swing.JTabbedPane();
        pnlPrincipal = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel8 = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblClasse = new javax.swing.JLabel();
        cbbClasse = new javax.swing.JComboBox();
        lblAgendamento = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        lblDSType = new javax.swing.JLabel();
        cbbDSType = new javax.swing.JComboBox();
        pnlConfiguracao = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanelConfig = new javax.swing.JPanel();
        lblDatabase = new javax.swing.JLabel();
        lblAmbiente = new javax.swing.JLabel();
        lblAlias = new javax.swing.JLabel();
        lbServer = new javax.swing.JLabel();
        radAmbiente = new javax.swing.JRadioButton();
        radTopConnect = new javax.swing.JRadioButton();
        lblConType = new javax.swing.JLabel();
        btnTestar = new javax.swing.JButton();
        fldAmbiente = new pv.jfcx.JPVEdit();
        fldAlias = new pv.jfcx.JPVEdit();
        fldServer = new pv.jfcx.JPVEdit();
        cboDataBase = new javax.swing.JComboBox();
        cboConType = new javax.swing.JComboBox();
        pnlDeclaracao = new javax.swing.JPanel();
        lblDeclara = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDeclaracoes = new javax.swing.JTextArea();
        btnTestSintax = new javax.swing.JButton();
        pnlConsulta = new javax.swing.JPanel();
        pnlGrupoConsulta = new javax.swing.JPanel();
        lblEnderecoDW = new javax.swing.JLabel();
        fldUrlDW = new pv.jfcx.JPVEdit();
        btnPostEndereco = new javax.swing.JButton();
        pnlSelecaoDW = new javax.swing.JPanel();
        cbbIndDW = new javax.swing.JComboBox();
        lblIndDW = new javax.swing.JLabel();
        cbbDataDW = new javax.swing.JComboBox();
        lblDataDW = new javax.swing.JLabel();
        lblConsulta = new javax.swing.JLabel();
        cbbConsultas = new javax.swing.JComboBox();
        lblDatawarehouse = new javax.swing.JLabel();
        cbbDataWareHouse = new javax.swing.JComboBox();
        pnlSelecionadoDW = new javax.swing.JPanel();
        fldDWSel = new javax.swing.JTextField();
        fldDwConsultaSel = new javax.swing.JTextField();
        fldDWDataSel = new javax.swing.JTextField();
        fldIndDWSel = new javax.swing.JTextField();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscDataSourceFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_planilha.gif"))); // NOI18N
        setMaximumSize(new java.awt.Dimension(0, 0));
        setMinimumSize(new java.awt.Dimension(0, 0));
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(560, 360));

        tapCadastro.setMaximumSize(new java.awt.Dimension(0, 0));
        tapCadastro.setMinimumSize(new java.awt.Dimension(0, 0));
        tapCadastro.setPreferredSize(new java.awt.Dimension(0, 0));

        pnlRecord.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlRecord.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlRecord.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscDataSourceFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscDataSourceFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscDataSourceFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscDataSourceFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscDataSourceFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnImportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_play.gif"))); // NOI18N
        btnImportar.setText(bundle.getString("BscDataSourceFrame_00009")); // NOI18N
        btnImportar.setActionCommand(bundle.getString("BscDataSourceFrame_00007")); // NOI18N
        btnImportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnImportar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_play_red.gif"))); // NOI18N
        btnImportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnImportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnImportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnImportar);

        btnLog.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnLog.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_log.gif"))); // NOI18N
        btnLog.setText("Log");
        btnLog.setActionCommand(bundle.getString("BscDataSourceFrame_00007")); // NOI18N
        btnLog.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnLog.setFocusable(false);
        btnLog.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnLog.setMaximumSize(new java.awt.Dimension(76, 20));
        btnLog.setMinimumSize(new java.awt.Dimension(76, 20));
        btnLog.setPreferredSize(new java.awt.Dimension(76, 20));
        btnLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogActionPerformed(evt);
            }
        });
        tbInsertion.add(btnLog);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlFields.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlFields.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setRequestFocusEnabled(false);

        pnlData.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlData.setMinimumSize(new java.awt.Dimension(0, 0));
        pnlData.setPreferredSize(new java.awt.Dimension(600, 360));
        pnlData.setLayout(new java.awt.BorderLayout());

        paneDataSource.setPreferredSize(new java.awt.Dimension(0, 0));

        pnlPrincipal.setLayout(new java.awt.BorderLayout());

        jScrollPane5.setBorder(null);
        jScrollPane5.setPreferredSize(new java.awt.Dimension(530, 180));

        jPanel8.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 102, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscDataSourceFrame_00008")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblNome);
        lblNome.setBounds(9, 9, 98, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscDataSourceFrame_00010")); // NOI18N
        lblDescricao.setEnabled(false);
        jPanel8.add(lblDescricao);
        lblDescricao.setBounds(10, 90, 76, 20);

        lblClasse.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblClasse.setText(bundle.getString("BscDataSourceFrame_00011")); // NOI18N
        lblClasse.setEnabled(false);
        lblClasse.setMaximumSize(new java.awt.Dimension(100, 16));
        lblClasse.setMinimumSize(new java.awt.Dimension(100, 16));
        lblClasse.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblClasse);
        lblClasse.setBounds(10, 168, 98, 20);

        cbbClasse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbClasseItemStateChanged(evt);
            }
        });
        jPanel8.add(cbbClasse);
        cbbClasse.setBounds(10, 190, 180, 22);

        lblAgendamento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAgendamento.setText(bundle.getString("BscDataSourceFrame_00013")); // NOI18N
        lblAgendamento.setEnabled(false);
        lblAgendamento.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAgendamento.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAgendamento.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblAgendamento);
        lblAgendamento.setBounds(10, 50, 106, 18);

        lblCodigo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCodigo.setText("0000");
        lblCodigo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        lblCodigo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCodigo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCodigo.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblCodigo);
        lblCodigo.setBounds(10, 70, 180, 22);

        txtDescricao.setColumns(80);
        txtDescricao.setRows(0);
        jScrollPane2.setViewportView(txtDescricao);

        jPanel8.add(jScrollPane2);
        jScrollPane2.setBounds(10, 110, 400, 60);

        fldNome.setMaxLength(60);
        jPanel8.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        lblDSType.setText(bundle.getString("BscDataSourceFrame_00030")); // NOI18N
        lblDSType.setEnabled(false);
        jPanel8.add(lblDSType);
        lblDSType.setBounds(10, 210, 100, 20);

        jPanel8.add(cbbDSType);
        cbbDSType.setBounds(10, 230, 180, 22);

        jScrollPane5.setViewportView(jPanel8);

        pnlPrincipal.add(jScrollPane5, java.awt.BorderLayout.CENTER);

        paneDataSource.addTab(bundle.getString("BscDataSourceFrame_00022"), pnlPrincipal); // NOI18N

        pnlConfiguracao.setLayout(new java.awt.BorderLayout());

        jScrollPane4.setBorder(null);
        jScrollPane4.setPreferredSize(new java.awt.Dimension(530, 300));

        jPanelConfig.setLayout(null);

        lblDatabase.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDatabase.setText("dbDataBase:");
        lblDatabase.setEnabled(false);
        lblDatabase.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDatabase.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDatabase.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblDatabase);
        lblDatabase.setBounds(20, 120, 98, 18);

        lblAmbiente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAmbiente.setText(bundle.getString("BscDataSourceFrame_00014")); // NOI18N
        lblAmbiente.setEnabled(false);
        lblAmbiente.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblAmbiente);
        lblAmbiente.setBounds(20, 40, 98, 18);

        lblAlias.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAlias.setText("dbAlias:");
        lblAlias.setEnabled(false);
        lblAlias.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAlias.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAlias.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblAlias);
        lblAlias.setBounds(20, 160, 98, 18);

        lbServer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbServer.setText("dbServer:");
        lbServer.setEnabled(false);
        lbServer.setMaximumSize(new java.awt.Dimension(100, 16));
        lbServer.setMinimumSize(new java.awt.Dimension(100, 16));
        lbServer.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lbServer);
        lbServer.setBounds(20, 200, 98, 18);

        buttonGroupTopConnect.add(radAmbiente);
        radAmbiente.setText(bundle.getString("BscDataSourceFrame_00015")); // NOI18N
        radAmbiente.setEnabled(false);
        radAmbiente.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                radAmbienteStateChanged(evt);
            }
        });
        jPanelConfig.add(radAmbiente);
        radAmbiente.setBounds(20, 10, 332, 23);

        buttonGroupTopConnect.add(radTopConnect);
        radTopConnect.setText(bundle.getString("BscDataSourceFrame_00016")); // NOI18N
        radTopConnect.setEnabled(false);
        radTopConnect.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                radTopConnectStateChanged(evt);
            }
        });
        jPanelConfig.add(radTopConnect);
        radTopConnect.setBounds(20, 90, 268, 23);

        lblConType.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType.setText("dbContype:");
        lblConType.setEnabled(false);
        lblConType.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblConType);
        lblConType.setBounds(20, 240, 98, 18);

        btnTestar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnTestar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif"))); // NOI18N
        btnTestar.setText(bundle.getString("BscDataSourceFrame_00017")); // NOI18N
        btnTestar.setEnabled(false);
        btnTestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestarActionPerformed(evt);
            }
        });
        jPanelConfig.add(btnTestar);
        btnTestar.setBounds(430, 60, 137, 25);

        fldAmbiente.setMaxLength(60);
        jPanelConfig.add(fldAmbiente);
        fldAmbiente.setBounds(20, 60, 400, 22);

        fldAlias.setMaxLength(60);
        jPanelConfig.add(fldAlias);
        fldAlias.setBounds(20, 180, 400, 22);

        fldServer.setMaxLength(60);
        jPanelConfig.add(fldServer);
        fldServer.setBounds(20, 220, 400, 22);

        jPanelConfig.add(cboDataBase);
        cboDataBase.setBounds(20, 140, 400, 22);

        jPanelConfig.add(cboConType);
        cboConType.setBounds(20, 260, 400, 22);

        jScrollPane4.setViewportView(jPanelConfig);

        pnlConfiguracao.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        paneDataSource.addTab(bundle.getString("BscDataSourceFrame_00023"), pnlConfiguracao); // NOI18N

        pnlDeclaracao.setPreferredSize(new java.awt.Dimension(530, 300));
        pnlDeclaracao.setLayout(null);

        lblDeclara.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDeclara.setText(bundle.getString("BscDataSourceFrame_00018")); // NOI18N
        lblDeclara.setEnabled(false);
        lblDeclara.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDeclara.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDeclara.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlDeclaracao.add(lblDeclara);
        lblDeclara.setBounds(12, 8, 98, 18);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(500, 21));

        txtDeclaracoes.setColumns(80);
        txtDeclaracoes.setLineWrap(true);
        txtDeclaracoes.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtDeclaracoes);

        pnlDeclaracao.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 463, 290);

        btnTestSintax.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnTestSintax.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnTestSintax.setText(bundle.getString("BscDataSourceFrame_00019")); // NOI18N
        btnTestSintax.setEnabled(false);
        btnTestSintax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestSintaxActionPerformed(evt);
            }
        });
        pnlDeclaracao.add(btnTestSintax);
        btnTestSintax.setBounds(480, 160, 141, 25);

        paneDataSource.addTab(bundle.getString("BscDataSourceFrame_00020"), pnlDeclaracao); // NOI18N

        pnlConsulta.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlConsulta.setLayout(new java.awt.BorderLayout());

        pnlGrupoConsulta.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlGrupoConsulta.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlGrupoConsulta.setPreferredSize(new java.awt.Dimension(0, 0));
        pnlGrupoConsulta.setLayout(null);

        lblEnderecoDW.setText(bundle.getString("BscDataSourceFrame_00029")); // NOI18N
        pnlGrupoConsulta.add(lblEnderecoDW);
        lblEnderecoDW.setBounds(10, 10, 100, 14);

        fldUrlDW.setMaxLength(60);
        pnlGrupoConsulta.add(fldUrlDW);
        fldUrlDW.setBounds(10, 30, 400, 22);

        btnPostEndereco.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPostEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif"))); // NOI18N
        btnPostEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPostEnderecoActionPerformed(evt);
            }
        });
        pnlGrupoConsulta.add(btnPostEndereco);
        btnPostEndereco.setBounds(420, 30, 25, 22);

        pnlSelecaoDW.setBorder(javax.swing.BorderFactory.createTitledBorder("Sele��o"));
        pnlSelecaoDW.setLayout(null);

        cbbIndDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbIndDWItemStateChanged(evt);
            }
        });
        pnlSelecaoDW.add(cbbIndDW);
        cbbIndDW.setBounds(10, 170, 180, 22);

        lblIndDW.setText(bundle.getString("BscDataSourceFrame_00024")); // NOI18N
        pnlSelecaoDW.add(lblIndDW);
        lblIndDW.setBounds(10, 150, 100, 20);

        cbbDataDW.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataDWItemStateChanged(evt);
            }
        });
        pnlSelecaoDW.add(cbbDataDW);
        cbbDataDW.setBounds(10, 130, 180, 22);

        lblDataDW.setText(bundle.getString("BscDataSourceFrame_00025")); // NOI18N
        pnlSelecaoDW.add(lblDataDW);
        lblDataDW.setBounds(10, 110, 100, 20);

        lblConsulta.setText(bundle.getString("BscDataSourceFrame_00026")); // NOI18N
        pnlSelecaoDW.add(lblConsulta);
        lblConsulta.setBounds(10, 70, 100, 20);

        cbbConsultas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbConsultasItemStateChanged(evt);
            }
        });
        pnlSelecaoDW.add(cbbConsultas);
        cbbConsultas.setBounds(10, 90, 180, 22);

        lblDatawarehouse.setText("DataWareHouse:");
        pnlSelecaoDW.add(lblDatawarehouse);
        lblDatawarehouse.setBounds(10, 30, 100, 14);

        cbbDataWareHouse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataWareHouseItemStateChanged(evt);
            }
        });
        pnlSelecaoDW.add(cbbDataWareHouse);
        cbbDataWareHouse.setBounds(10, 50, 180, 22);

        pnlGrupoConsulta.add(pnlSelecaoDW);
        pnlSelecaoDW.setBounds(10, 60, 200, 220);

        pnlSelecionadoDW.setBorder(javax.swing.BorderFactory.createTitledBorder("Selecionado"));
        pnlSelecionadoDW.setPreferredSize(new java.awt.Dimension(210, 160));
        pnlSelecionadoDW.setLayout(null);
        pnlSelecionadoDW.add(fldDWSel);
        fldDWSel.setBounds(10, 50, 170, 22);
        pnlSelecionadoDW.add(fldDwConsultaSel);
        fldDwConsultaSel.setBounds(10, 90, 170, 22);
        pnlSelecionadoDW.add(fldDWDataSel);
        fldDWDataSel.setBounds(10, 130, 170, 22);
        pnlSelecionadoDW.add(fldIndDWSel);
        fldIndDWSel.setBounds(10, 170, 170, 22);

        pnlGrupoConsulta.add(pnlSelecionadoDW);
        pnlSelecionadoDW.setBounds(220, 60, 190, 220);

        pnlConsulta.add(pnlGrupoConsulta, java.awt.BorderLayout.CENTER);

        paneDataSource.addTab("Dados do DW", pnlConsulta);

        pnlData.add(paneDataSource, java.awt.BorderLayout.CENTER);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscDataSourceFrame_00001"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 75));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbbIndDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbIndDWItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            int itemSelecionado = cbbIndDW.getSelectedIndex();
            if (itemSelecionado > 0) {
                fldIndDWSel.setText((String) cbbIndDW.getSelectedItem());
            } else {
                fldIndDWSel.setText("");
            }
        }
    }//GEN-LAST:event_cbbIndDWItemStateChanged

    private void cbbDataDWItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataDWItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            int itemSelecionado = cbbDataDW.getSelectedIndex();
            if (itemSelecionado > 0) {
                fldDWDataSel.setText((String) cbbDataDW.getSelectedItem());
            } else {
                fldDWDataSel.setText("");
            }
        }
    }//GEN-LAST:event_cbbDataDWItemStateChanged

    private void cbbConsultasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbConsultasItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            int itemSelecionado = cbbConsultas.getSelectedIndex();
            if (itemSelecionado > 0) {
                bsc.xml.BIXMLRecord recConsulta = vctConsultaSel.get(itemSelecionado - 1);
                setCboBancoValue("");
                fldIndDWSel.setText("");

                //Se for maior que dois existem itens de consulta.
                if (recConsulta.size() > 2) {
                    fldDwConsultaSel.setText((String) cbbConsultas.getSelectedItem());
                    bsc.xml.BIXMLVector vctDatas = recConsulta.getBIXMLVector("DATAS");
                    bsc.xml.BIXMLVector vctIndic = recConsulta.getBIXMLVector("INDICADORES");

                    //Adicionando os campos de data.
                    if (vctDatas.size() > 0) {
                        event.populateCombo(vctDatas, "CPO_DATA", htbCampos, cbbDataDW);
                    } else {
                        cbbDataDW.removeAllItems();
                    }

                    //Adicionando os campos de indicadores.
                    if (vctIndic.size() > 0) {
                        event.populateCombo(vctIndic, "INDICADOR", htbIndicadores, cbbIndDW);
                    } else {
                        cbbIndDW.removeAllItems();
                    }

                } else {
                    fldDwConsultaSel.setText("");
                    cbbDataDW.removeAllItems();
                    cbbIndDW.removeAllItems();
                }
            } else {
                setCboBancoValue("");
                fldIndDWSel.setText("");
                fldDwConsultaSel.setText("");
                cbbDataDW.removeAllItems();
                cbbIndDW.removeAllItems();
            }
        }
    }//GEN-LAST:event_cbbConsultasItemStateChanged

    private void cbbDataWareHouseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataWareHouseItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            int itemSelecionado = cbbDataWareHouse.getSelectedIndex();
            if (itemSelecionado > 0) {
                bsc.xml.BIXMLVector dwConsultas = recDataWareHouse.getBIXMLVector("DWCONSULTAS");
                bsc.xml.BIXMLRecord recConsultas = dwConsultas.get(itemSelecionado - 1);

                //Se for maior que dois existem itens de consulta.
                if (recConsultas.size() > 2) {
                    fldDWSel.setText((String) cbbDataWareHouse.getSelectedItem());
                    vctConsultaSel = recConsultas.getBIXMLVector("QUERY_LISTS");
                    event.populateCombo(vctConsultaSel, "QUERY_LIST", htbConsultas, cbbConsultas);
                } else {
                    fldDWSel.setText("");
                    cbbConsultas.removeAllItems();
                }
            } else {
                fldDWSel.setText("");
                cbbConsultas.removeAllItems();
            }
        }
    }//GEN-LAST:event_cbbDataWareHouseItemStateChanged

    private void btnPostEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPostEnderecoActionPerformed
        bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition(1);
        bsc.core.BscStaticReferences.getBscMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));

        StringBuffer request = new StringBuffer("REQ_DATAWAREHOUSE");
        request.append("|");
        request.append(fldUrlDW.getText());
        request.append("|");
        request.append("true"); //Carrega os detalhes da consulta.

        //Requisitando os dados.
        recDataWareHouse = event.loadRecord("-1", "DWCONSULTA", request.toString());
        event.populateCombo(recDataWareHouse.getBIXMLVector("DWCONSULTAS"), "DWCONSULTA", htbDataWareHouse, cbbDataWareHouse);

        bsc.core.BscStaticReferences.getBscMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition(0);
    }//GEN-LAST:event_btnPostEnderecoActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("DADOS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void cbbClasseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbClasseItemStateChanged
            radAmbiente.setEnabled(true);
            radTopConnect.setEnabled(true);

            if (cbbClasse.getSelectedIndex() == 2) {
                radAmbiente.setSelected(true);
                radTopConnect.setEnabled(false);
            }

            enableTopFields();
            setVisibleTabs();
            populateCboDSType();
	}//GEN-LAST:event_cbbClasseItemStateChanged

	private void btnTestSintaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestSintaxActionPerformed
            event.executeRecord("TESTSINTAX");
	}//GEN-LAST:event_btnTestSintaxActionPerformed

	private void btnTestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestarActionPerformed
            event.executeRecord("TESTCON");
	}//GEN-LAST:event_btnTestarActionPerformed

	private void btnImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarActionPerformed
            btnImportar.setEnabled(false);
            isImporting = true;
            event.executeRecord("IMPORTCON");
	}//GEN-LAST:event_btnImportarActionPerformed

	private void radTopConnectStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_radTopConnectStateChanged
            if (getStatus() == BscDefaultFrameBehavior.INSERTING || getStatus() == BscDefaultFrameBehavior.UPDATING) {
                enableTopFields();
            }
	}//GEN-LAST:event_radTopConnectStateChanged

	private void radAmbienteStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_radAmbienteStateChanged
            if (getStatus() == BscDefaultFrameBehavior.INSERTING || getStatus() == BscDefaultFrameBehavior.UPDATING) {
                enableTopFields();
            }
	}//GEN-LAST:event_radAmbienteStateChanged

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void btnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogActionPerformed
        StringBuilder serverFolder = new StringBuilder("\\logs\\import\\");
        serverFolder.append(getID());
        serverFolder.append("\\*.html");

        bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(this, true);

        frmChooser.setDialogType(BscFileChooser.BSC_DIALOG_OPEN);
        frmChooser.setFileType(".html");
        frmChooser.loadServerFiles(serverFolder.toString());
        frmChooser.setVisible(true);
        if (frmChooser.isValidFile()) {
            try {
                bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                StringBuilder strFileName = new StringBuilder(bscApplet.getCodeBase().toString());
                strFileName.append("logs/import/");
                strFileName.append(getID());
                strFileName.append("/");
                strFileName.append(frmChooser.getFileName());
                java.net.URL url = new java.net.URL(strFileName.toString());
                bscApplet.getAppletContext().showDocument(url, "_blank");
            } catch (Exception e) {
                System.out.println(e);
            }
        }

}//GEN-LAST:event_btnLogActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnImportar;
    private javax.swing.JButton btnLog;
    private javax.swing.JButton btnPostEndereco;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTestSintax;
    private javax.swing.JButton btnTestar;
    private javax.swing.ButtonGroup buttonGroupTopConnect;
    private javax.swing.JComboBox cbbClasse;
    private javax.swing.JComboBox cbbConsultas;
    private javax.swing.JComboBox cbbDSType;
    private javax.swing.JComboBox cbbDataDW;
    private javax.swing.JComboBox cbbDataWareHouse;
    private javax.swing.JComboBox cbbIndDW;
    private javax.swing.JComboBox cboConType;
    private javax.swing.JComboBox cboDataBase;
    private pv.jfcx.JPVEdit fldAlias;
    private pv.jfcx.JPVEdit fldAmbiente;
    private javax.swing.JTextField fldDWDataSel;
    private javax.swing.JTextField fldDWSel;
    private javax.swing.JTextField fldDwConsultaSel;
    private javax.swing.JTextField fldIndDWSel;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldServer;
    private pv.jfcx.JPVEdit fldUrlDW;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanelConfig;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lbServer;
    private javax.swing.JLabel lblAgendamento;
    private javax.swing.JLabel lblAlias;
    private javax.swing.JLabel lblAmbiente;
    private javax.swing.JLabel lblClasse;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblConType;
    private javax.swing.JLabel lblConsulta;
    private javax.swing.JLabel lblDSType;
    private javax.swing.JLabel lblDataDW;
    private javax.swing.JLabel lblDatabase;
    private javax.swing.JLabel lblDatawarehouse;
    private javax.swing.JLabel lblDeclara;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblEnderecoDW;
    private javax.swing.JLabel lblIndDW;
    private javax.swing.JLabel lblNome;
    private javax.swing.JTabbedPane paneDataSource;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfiguracao;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlConsulta;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDeclaracao;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlGrupoConsulta;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlSelecaoDW;
    private javax.swing.JPanel pnlSelecionadoDW;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton radAmbiente;
    private javax.swing.JRadioButton radTopConnect;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTextArea txtDeclaracoes;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscDataSourceFrame(int operation, String idAux, String contextId) {
        initComponents();
        btnTestar.setVisible(false);
        btnTestSintax.setVisible(false);
        event.defaultConstructor(operation, idAux, contextId);

    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
        btnTestar.setEnabled(false);
        btnTestSintax.setEnabled(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
        btnTestar.setEnabled(true);
        btnTestSintax.setEnabled(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    private void populateCboServer() {
        cboConType.removeAllItems();
        for (int i = 0; i < conexSrv.length; i++) {
            cboConType.addItem(conexSrv[i][1]);
        }
    }

    private String getCboServerValue() {
        return conexSrv[cboConType.getSelectedIndex()][0];
    }

    private void setCboServerValue(String value) {
        for (int i = 0; i < conexSrv.length; i++) {
            if (conexSrv[i][0].equals(value)) {
                cboConType.setSelectedIndex(i);
                break;
            }
        }
    }

    private void populateCboBanco() {
        cboDataBase.removeAllItems();
        for (int i = 0; i < conexBco.length; i++) {
            cboDataBase.addItem(conexBco[i][1]);
        }
    }

    private String getCboBancoValue() {
        return conexBco[cboDataBase.getSelectedIndex()][0];
    }

    private void setCboBancoValue(String value) {
        for (int i = 0; i < conexBco.length; i++) {
            if (conexBco[i][0].equals(value)) {
                cboDataBase.setSelectedIndex(i);
                break;
            }
        }
    }

    private void populateCboDSType() {
        cbbDSType.removeAllItems();
        for (String[] strings : dataSourceTypes) {
            if (cbbClasse.getSelectedIndex() != 3 || !strings[0].equals(DATA_SRC_REFMETAS)) {
                cbbDSType.addItem(strings[1]);
            }
        }
    }

    private String getCboDSTypeValue() {
        return dataSourceTypes[cbbDSType.getSelectedIndex()][0];
    }

    private String getCboDSTypeValue(String strValue) {
        for (String[] strings : dataSourceTypes) {
            if (strings[0].equals(strValue)) {
                return strings[1];
            }
        }

        return "";
    }

    private void setCboDSTypeValue(String typeDS) {
        cbbDSType.setSelectedItem(getCboDSTypeValue(typeDS));
    }

    private boolean isDSReference() {
        return getCboDSTypeValue().equals(DATA_SRC_REFERENCE);
    }

    private boolean isDSMetas() {
        return getCboDSTypeValue().equals(DATA_SRC_REFMETAS);
    }

    @Override
    public void afterExecute() {
        if (isImporting) {
            btnImportar.setEnabled(true);
            BscDefaultDialogSystem oDialog = new BscDefaultDialogSystem(this);
            oDialog.informationMessage("Importa��o finalizada! Para detalhes da opera��o verifique o arquivo de log.");
            isImporting = false;
        }
    }

    @Override
    public void afterUpdate() {
    }

    @Override
    public void afterInsert() {
    }

    @Override
    public void afterDelete() {
    }
}
