package bsc.swing.framework;


public class BscDesdobramentoFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("DESDOB");
	private java.util.Hashtable htbEstId = new java.util.Hashtable();
	private java.util.Hashtable htbObjId = new java.util.Hashtable();
	private java.util.Hashtable htbOrigemId = new java.util.Hashtable();
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblNome.setEnabled(true);
		fldNome.setEnabled(true);
		lblEstrategiaOrigem.setEnabled(true);
		jScrollDescricao.setEnabled(true);
		lblOrganizacaoDestino.setEnabled(true);
		cbOrganizacaoDestino.setEnabled(true);
		lblEstrategiaDestino.setEnabled(true);
		cbEstrategiaDestino.setEnabled(true);
		lblOrigem.setEnabled(true);
		cbOrigem.setEnabled(true);
		lblDestino.setEnabled(true);
		cbDestino.setEnabled(true);
		lblTipo.setEnabled(true);
		cbTipo.setEnabled(true);
		pnlOrigem.setEnabled(true);
		pnlDestino.setEnabled(true);
		filterEstrategiaOrigem();
		filterOrganizacao();
		filterEstrategia();
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		// Ex.:
		lblNome.setEnabled(false);
		fldNome.setEnabled(false);
		jScrollDescricao.setEnabled(false);
		lblEstrategiaOrigem.setEnabled(false);
		lblOrganizacaoDestino.setEnabled(false);
		cbOrganizacaoDestino.setEnabled(false);
		fldEstrategiaOrigem.setEnabled(false);
		lblEstrategiaDestino.setEnabled(false);
		cbEstrategiaDestino.setEnabled(false);
		lblOrigem.setEnabled(false);
		cbOrigem.setEnabled(false);
		lblDestino.setEnabled(false);
		cbDestino.setEnabled(false);
		lblTipo.setEnabled(false);
		cbTipo.setEnabled(false);
		pnlOrigem.setEnabled(false);
		pnlDestino.setEnabled(false);
	}

	java.util.Hashtable htbTipo= new java.util.Hashtable();
	java.util.Hashtable htbOrigem = new java.util.Hashtable();
	java.util.Hashtable htbDestino= new java.util.Hashtable();
	java.util.Hashtable htbEstrategia= new java.util.Hashtable();
	java.util.Hashtable htbOrganizacao= new java.util.Hashtable();

	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldNome.setText(record.getString("NOME") );
		fldEstrategiaOrigem.setText(record.getString("ESTORIGEM"));
		event.populateCombo( record.getBIXMLVector("OBJETIVOS"), "ORIGEMID", htbOrigem, cbOrigem);
		event.populateCombo( record.getBIXMLVector("OBJETIVOS"), "DESTINOID", htbDestino, cbDestino, htbObjId);
		event.populateCombo( record.getBIXMLVector("ESTRATEGIAS"), "ESTID", htbEstrategia, cbEstrategiaDestino);
		event.populateCombo( record.getBIXMLVector("ORGANIZACOES"), "ORGID", htbOrganizacao, cbOrganizacaoDestino);

		cbOrganizacaoDestino.removeItemAt(0); //remove o item 0 pois � gerado em branco
		event.populateCombo( record.getBIXMLVector("TIPOLINKS"), "TIPODES", htbTipo, cbTipo);
		cbTipo.removeItemAt(0); //remove o item 0 pois � gerado em branco
		bsc.xml.BIXMLVector vctHeader = record.getBIXMLVector("CONTEXTOS");
		pnlHead.setComponents(vctHeader);

		if(vctHeader.size() > 0){
		    fldEstrategiaOrigem.setText(vctHeader.get(0).getString("NOME"));
			fldOrganizacaoOrigem.setText(vctHeader.get(1).getString("NOME"));
		}

	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		recordAux.set( "ENTIDADE", "OBJETIVO" ); //depois disponibilizar entidades
		recordAux.set( "NOME", fldNome.getText() );
		recordAux.set( "ORIGEMID", event.getComboValue( htbOrigem, cbOrigem ) );
		recordAux.set( "DESTINOID", event.getComboValue( htbDestino, cbDestino ) );
		recordAux.set( "TIPODES", cbTipo.getSelectedIndex()+1 );
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}

	public boolean hasCombos() {
		return false;
	}

	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        cbTipo = new javax.swing.JComboBox();
        lblTipo = new javax.swing.JLabel();
        jScrollDescricao = new javax.swing.JScrollPane();
        fldNome = new bsc.beans.JBITextArea();
        pnlOrigem = new javax.swing.JPanel();
        lblEstrategiaOrigem = new javax.swing.JLabel();
        cbOrigem = new javax.swing.JComboBox();
        lblOrigem = new javax.swing.JLabel();
        fldEstrategiaOrigem = new pv.jfcx.JPVEdit();
        fldOrganizacaoOrigem = new pv.jfcx.JPVEdit();
        lblOrganizacaoOrigem = new javax.swing.JLabel();
        pnlDestino = new javax.swing.JPanel();
        lblOrganizacaoDestino = new javax.swing.JLabel();
        cbOrganizacaoDestino = new javax.swing.JComboBox();
        lblEstrategiaDestino = new javax.swing.JLabel();
        cbEstrategiaDestino = new javax.swing.JComboBox();
        lblDestino = new javax.swing.JLabel();
        cbDestino = new javax.swing.JComboBox();
        lblFigura = new javax.swing.JLabel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHead = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Desdobramentos");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_copy.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setAutoscrolls(true);
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnSave.setText(bundle.getString("BscPerspectivaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscPerspectivaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscPerspectivaFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscPerspectivaFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscPerspectivaFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(700, 500));
        pnlData.setLayout(null);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscPerspectivaFrame_00008")); // NOI18N
        lblNome.setEnabled(false);
        pnlData.add(lblNome);
        lblNome.setBounds(20, 320, 80, 14);

        cbTipo.setPreferredSize(new java.awt.Dimension(26, 22));
        pnlData.add(cbTipo);
        cbTipo.setBounds(20, 410, 400, 22);

        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTipo.setText(bundle.getString("BscDesdobramentoFrame_00006")); // NOI18N
        lblTipo.setEnabled(false);
        pnlData.add(lblTipo);
        lblTipo.setBounds(20, 390, 80, 14);

        jScrollDescricao.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollDescricao.setViewportView(fldNome);

        pnlData.add(jScrollDescricao);
        jScrollDescricao.setBounds(20, 340, 400, 49);

        pnlOrigem.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), bundle.getString("BscDesdobramentoFrame_00003"))); // NOI18N
        pnlOrigem.setEnabled(false);
        pnlOrigem.setLayout(null);

        lblEstrategiaOrigem.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEstrategiaOrigem.setText(bundle.getString("BscDesdobramentoFrame_00002")); // NOI18N
        lblEstrategiaOrigem.setEnabled(false);
        pnlOrigem.add(lblEstrategiaOrigem);
        lblEstrategiaOrigem.setBounds(10, 60, 82, 20);

        cbOrigem.setPreferredSize(new java.awt.Dimension(26, 22));
        pnlOrigem.add(cbOrigem);
        cbOrigem.setBounds(10, 120, 400, 22);

        lblOrigem.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrigem.setText(bundle.getString("BscDesdobramentoFrame_00009")); // NOI18N
        lblOrigem.setEnabled(false);
        pnlOrigem.add(lblOrigem);
        lblOrigem.setBounds(10, 100, 78, 20);

        fldEstrategiaOrigem.setEnabled(false);
        fldEstrategiaOrigem.setMaxLength(60);
        pnlOrigem.add(fldEstrategiaOrigem);
        fldEstrategiaOrigem.setBounds(10, 80, 400, 22);

        fldOrganizacaoOrigem.setEnabled(false);
        fldOrganizacaoOrigem.setMaxLength(60);
        pnlOrigem.add(fldOrganizacaoOrigem);
        fldOrganizacaoOrigem.setBounds(10, 40, 400, 22);

        lblOrganizacaoOrigem.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrganizacaoOrigem.setText(bundle.getString("BscDesdobramentoFrame_00004")); // NOI18N
        lblOrganizacaoOrigem.setEnabled(false);
        lblOrganizacaoOrigem.setMaximumSize(new java.awt.Dimension(39, 15));
        lblOrganizacaoOrigem.setMinimumSize(new java.awt.Dimension(39, 15));
        lblOrganizacaoOrigem.setPreferredSize(new java.awt.Dimension(39, 15));
        pnlOrigem.add(lblOrganizacaoOrigem);
        lblOrganizacaoOrigem.setBounds(10, 20, 80, 20);

        pnlData.add(pnlOrigem);
        pnlOrigem.setBounds(10, 160, 430, 160);

        pnlDestino.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscDesdobramentoFrame_00005"))); // NOI18N
        pnlDestino.setEnabled(false);
        pnlDestino.setLayout(null);

        lblOrganizacaoDestino.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrganizacaoDestino.setText(bundle.getString("BscDesdobramentoFrame_00004")); // NOI18N
        lblOrganizacaoDestino.setEnabled(false);
        lblOrganizacaoDestino.setMaximumSize(new java.awt.Dimension(39, 15));
        lblOrganizacaoDestino.setMinimumSize(new java.awt.Dimension(39, 15));
        lblOrganizacaoDestino.setPreferredSize(new java.awt.Dimension(39, 15));
        pnlDestino.add(lblOrganizacaoDestino);
        lblOrganizacaoDestino.setBounds(10, 20, 80, 20);
        lblOrganizacaoDestino.getAccessibleContext().setAccessibleName("");

        cbOrganizacaoDestino.setPreferredSize(new java.awt.Dimension(26, 22));
        cbOrganizacaoDestino.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbOrganizacaoDestinoItemStateChanged(evt);
            }
        });
        pnlDestino.add(cbOrganizacaoDestino);
        cbOrganizacaoDestino.setBounds(10, 40, 400, 22);

        lblEstrategiaDestino.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEstrategiaDestino.setText(bundle.getString("BscDesdobramentoFrame_00002")); // NOI18N
        lblEstrategiaDestino.setEnabled(false);
        lblEstrategiaDestino.setMaximumSize(new java.awt.Dimension(39, 15));
        lblEstrategiaDestino.setMinimumSize(new java.awt.Dimension(39, 15));
        lblEstrategiaDestino.setPreferredSize(new java.awt.Dimension(39, 15));
        pnlDestino.add(lblEstrategiaDestino);
        lblEstrategiaDestino.setBounds(10, 60, 78, 20);

        cbEstrategiaDestino.setPreferredSize(new java.awt.Dimension(26, 22));
        cbEstrategiaDestino.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbEstrategiaDestinoItemStateChanged(evt);
            }
        });
        pnlDestino.add(cbEstrategiaDestino);
        cbEstrategiaDestino.setBounds(10, 80, 400, 22);

        lblDestino.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDestino.setText(bundle.getString("BscDesdobramentoFrame_00009")); // NOI18N
        lblDestino.setEnabled(false);
        pnlDestino.add(lblDestino);
        lblDestino.setBounds(10, 100, 75, 20);

        cbDestino.setPreferredSize(new java.awt.Dimension(26, 22));
        pnlDestino.add(cbDestino);
        cbDestino.setBounds(10, 120, 400, 22);

        pnlData.add(pnlDestino);
        pnlDestino.setBounds(10, 10, 430, 150);

        lblFigura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/des_hierarquia.gif"))); // NOI18N
        lblFigura.setText("jLabel1");
        pnlData.add(lblFigura);
        lblFigura.setBounds(490, 90, 169, 281);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscDesdobramentoFrame_00001"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 30));
        pnlTopForm.setLayout(new java.awt.BorderLayout());

        pnlHead.setMinimumSize(new java.awt.Dimension(520, 45));
        pnlTopForm.add(pnlHead, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleName(bundle.getString("BscDesdobramentoFrame_00008")); // NOI18N

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbEstrategiaDestinoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbEstrategiaDestinoItemStateChanged
	if(cbEstrategiaDestino.getSelectedIndex()> -1){
	    filterEstrategia();
	}
    }//GEN-LAST:event_cbEstrategiaDestinoItemStateChanged

    private void cbOrganizacaoDestinoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbOrganizacaoDestinoItemStateChanged
	if(cbOrganizacaoDestino.getSelectedIndex()> -1){
	    filterOrganizacao();
	}
    }//GEN-LAST:event_cbOrganizacaoDestinoItemStateChanged

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("DESDOBRAMENTO");
	}//GEN-LAST:event_btnReload1ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	    event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		event.saveRecord( );
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed
	
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbDestino;
    private javax.swing.JComboBox cbEstrategiaDestino;
    private javax.swing.JComboBox cbOrganizacaoDestino;
    private javax.swing.JComboBox cbOrigem;
    private javax.swing.JComboBox cbTipo;
    private pv.jfcx.JPVEdit fldEstrategiaOrigem;
    private bsc.beans.JBITextArea fldNome;
    private pv.jfcx.JPVEdit fldOrganizacaoOrigem;
    private javax.swing.JScrollPane jScrollDescricao;
    private javax.swing.JLabel lblDestino;
    private javax.swing.JLabel lblEstrategiaDestino;
    private javax.swing.JLabel lblEstrategiaOrigem;
    private javax.swing.JLabel lblFigura;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblOrganizacaoDestino;
    private javax.swing.JLabel lblOrganizacaoOrigem;
    private javax.swing.JLabel lblOrigem;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDestino;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHead;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlOrigem;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscDesdobramentoFrame(int operation, String idAux, String contextAux) {
		initComponents();

		event.defaultConstructor( operation, idAux, contextAux );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}

	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

	private void filterOrganizacao(){
	    int i = cbOrganizacaoDestino.getSelectedIndex();
	    if(i>=0){
		Object organizacao = htbOrganizacao.get(cbOrganizacaoDestino.getSelectedItem());
		bsc.xml.BIXMLVector BIXMLVctEstrategia = record.getBIXMLVector("ESTRATEGIAS");
		htbEstId.clear();
		for(int nPos = 0; nPos < BIXMLVctEstrategia.size(); nPos ++){
		    bsc.xml.BIXMLRecord tmpRecord = BIXMLVctEstrategia.get(nPos);
		    if(!tmpRecord.getString("PARENTID").equals(organizacao.toString())){
			htbEstId.put(new Integer(tmpRecord.getInt("ID")), "");
		    }
		}
		event.populateCombo( BIXMLVctEstrategia, "ESTID", htbEstrategia, cbEstrategiaDestino, htbEstId);
		cbEstrategiaDestino.removeItemAt(0); //remove o item 0 pois � gerado em branco
	    }
	}

	private void filterEstrategia(){
	    int i = cbEstrategiaDestino.getSelectedIndex();
	    if(i>=0){
		bsc.xml.BIXMLVector BIXMLVctObjetivo = record.getBIXMLVector("OBJETIVOS");
		htbObjId.clear();
		for(int nPos = 0; nPos < BIXMLVctObjetivo.size(); nPos ++){
		    bsc.xml.BIXMLRecord tmpRecord = BIXMLVctObjetivo.get(nPos);
		    if(!tmpRecord.getString("CONTEXTID").equals(htbEstrategia.get(cbEstrategiaDestino.getSelectedItem()))){
			htbObjId.put(new Integer(tmpRecord.getInt("ID")), "");
		    }
		}
		event.populateCombo( BIXMLVctObjetivo, "DESTINOID", htbDestino, cbDestino, htbObjId);
		cbDestino.removeItemAt(0); //remove o item 0 pois � gerado em branco
	    }
	}

	private void filterEstrategiaOrigem(){
	    String strContextId = record.getString("CONTEXTID");
	    if(strContextId.trim().length() > 0){
		bsc.xml.BIXMLVector BIXMLVctObjetivo = record.getBIXMLVector("OBJETIVOS");
		htbOrigemId.clear();
		for(int nPos = 0; nPos < BIXMLVctObjetivo.size(); nPos ++){
		    bsc.xml.BIXMLRecord tmpRecord = BIXMLVctObjetivo.get(nPos);
		    if(!tmpRecord.getString("CONTEXTID").equals(strContextId)){
			htbOrigemId.put(new Integer(tmpRecord.getInt("ID")), "");
		    }
		}
		event.populateCombo( BIXMLVctObjetivo, "ORIGEMID", htbOrigem, cbOrigem, htbOrigemId);
		cbOrigem.removeItemAt(0); //remove o item 0 pois � gerado em branco
	    }
	}
}
