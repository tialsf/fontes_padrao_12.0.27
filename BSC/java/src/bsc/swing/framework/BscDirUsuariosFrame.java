package bsc.swing.framework;


public class BscDirUsuariosFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("DIRUSUARIOS");
	
	public void enableFields() {
		//lblTotalUsuarios.setEnabled(true);
		//fldTotalUsuarios.setEnabled(true);
		//lblTotalGrupos.setEnabled(true);
		//fldTotalGrupos.setEnabled(true);
	}
	
	public void disableFields() {
		lblTotalUsuarios.setEnabled(false);
		fldTotalUsuarios.setEnabled(false);
		lblTotalGrupos.setEnabled(false);
		fldTotalGrupos.setEnabled(false);
	}
	
	public void refreshFields( ) {
		fldTotalUsuarios.setText(record.getString("TOTALUSUARIOS"));
		fldTotalGrupos.setText(record.getString("TOTALGRUPOS"));

		usersList.setDataSource( record.getBIXMLVector("USUARIOS"), "1", "0", this.event);
		groupList.setDataSource( record.getBIXMLVector("GRUPOS"), "1", "0", this.event);
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		
		recordAux.set( "TOTALUSUARIOS", fldTotalUsuarios.getText() );
		recordAux.set( "TOTALGRUPOS", fldTotalGrupos.getText() );
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}	
	
	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	public boolean hasCombos() {
		return false;
	}

	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblTotalUsuarios = new javax.swing.JLabel();
        fldTotalUsuarios = new javax.swing.JTextField();
        lblTotalGrupos = new javax.swing.JLabel();
        fldTotalGrupos = new javax.swing.JTextField();
        pnlTopRecord = new javax.swing.JPanel();
        groupList = new bsc.beans.JBIListPanel();
        usersList = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscUsuariosFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_dirusuario.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setLayout(null);

        lblTotalUsuarios.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalUsuarios.setText(bundle.getString("BscUsuariosFrame_00003")); // NOI18N
        lblTotalUsuarios.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlData.add(lblTotalUsuarios);
        lblTotalUsuarios.setBounds(10, 10, 87, 22);
        pnlData.add(fldTotalUsuarios);
        fldTotalUsuarios.setBounds(10, 30, 180, 22);

        lblTotalGrupos.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalGrupos.setText(bundle.getString("BscUsuariosFrame_00002")); // NOI18N
        lblTotalGrupos.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        pnlData.add(lblTotalGrupos);
        lblTotalGrupos.setBounds(10, 50, 80, 22);
        pnlData.add(fldTotalGrupos);
        fldTotalGrupos.setBounds(10, 70, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscUsuariosFrame_00005"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscUsuariosFrame_00001"), groupList); // NOI18N
        tapCadastro.addTab(bundle.getString("BscUsuariosFrame_00004"), usersList); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents
						
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField fldTotalGrupos;
    private javax.swing.JTextField fldTotalUsuarios;
    private bsc.beans.JBIListPanel groupList;
    private javax.swing.JLabel lblTotalGrupos;
    private javax.swing.JLabel lblTotalUsuarios;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private bsc.beans.JBIListPanel usersList;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscDirUsuariosFrame(int operation, String idAux, String contextAux) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextAux );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
	}
	
	public void showOperationsButtons() {
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}	
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}
}