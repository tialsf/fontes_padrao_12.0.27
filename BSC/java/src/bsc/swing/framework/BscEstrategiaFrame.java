package bsc.swing.framework;

import java.util.ResourceBundle;
import javax.swing.JOptionPane;

public class BscEstrategiaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("ESTRATEGIA");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblDataIni.setEnabled(true);
        fldDataIni.setEnabled(true);
        lblDataFin.setEnabled(true);
        fldDataFin.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblDataIni.setEnabled(false);
        fldDataIni.setEnabled(false);
        lblDataFin.setEnabled(false);
        fldDataFin.setEnabled(false);
    }
    java.util.Hashtable htbOrganizacao = new java.util.Hashtable();
    java.util.Hashtable htbTipoLink = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        fldDataIni.setCalendar(record.getDate("DATAINI"));
        fldDataFin.setCalendar(record.getDate("DATAFIN"));
        String strId = record.getString("PARENTID");
        // Popula as ferramentas.
        perspectivesList.setDataSource(
                record.getBIXMLVector("PERSPECTIVAS"),
                record.getString("ID"), record.getString("ID"), this.event);
        paineisList.setDataSource(
                record.getBIXMLVector("DASHBOARDS"),
                record.getString("ID"), record.getString("ID"), this.event);
        mapaList.setDataSource(
                record.getBIXMLVector("FERRAMENTAS"),
                record.getString("ID"), record.getString("ID"), this.event);
        relatoriosList.setDataSource(
                record.getBIXMLVector("RELATORIOS"),
                record.getString("ID"), record.getString("ID"), this.event);
        graficosList.setDataSource(
                record.getBIXMLVector("GRAPHS"),
                record.getString("ID"), record.getString("ID"), this.event);
        desdobramentosList.setDataSource(
                record.getBIXMLVector("DESDOBS"),
                record.getString("ID"), record.getString("ID"), this.event);
        TemaEstList.setDataSource(
                record.getBIXMLVector("TEMASEST"),
                record.getString("ID"), record.getString("ID"), this.event);



        event.populateCombo(record.getBIXMLVector("ORGANIZACOES"), "PARENTID", htbOrganizacao, cbOrganizacao);
        cbOrganizacao.removeItemAt(0); //remove o item 0 pois � gerado em branco
        event.populateCombo(record.getBIXMLVector("TIPOLINKS"), "", htbTipoLink, cbTipo);
        cbTipo.removeItemAt(0);
        pnlHead.setComponents(record.getBIXMLVector("CONTEXTOS"));
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("DATAINI", fldDataIni.getCalendar());
        recordAux.set("DATAFIN", fldDataFin.getCalendar());

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;
        //TODO - Melhorar esta valida��o.
        if (fldNome.getText().trim().length() > 0) {
            // Valida��o de datas.
            if (fldDataIni.getText().trim().length() > 0 && fldDataFin.getText().trim().length() > 0) {

                valid = fldDataIni.getCalendar().before(fldDataFin.getCalendar());

                if (!valid) {
                    errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstrategiaFrame_00028"));
                }

            } else {
                errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
            }

        } else {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }

        return valid;
    }

    @Override
    public boolean hasCombos() {
        return false;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrupo = new javax.swing.ButtonGroup();
        pnlDuplicador = new javax.swing.JPanel();
        fldNomeDesdobramento = new pv.jfcx.JPVEdit();
        lblNomeDesdobramento = new javax.swing.JLabel();
        btnDesdobrar = new javax.swing.JButton();
        cbOrganizacao = new javax.swing.JComboBox();
        lblNomeOrganizacao = new javax.swing.JLabel();
        jPanelAcao = new javax.swing.JPanel();
        jDesdobrar = new javax.swing.JRadioButton();
        jDuplicar = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        cbTipo = new javax.swing.JComboBox();
        lblTipo = new javax.swing.JLabel();
        pnlGraficos = new javax.swing.JPanel();
        graficosList = new bsc.beans.JBIListPanelPlus();
        pnlRelatorios = new javax.swing.JPanel();
        relatoriosList = new bsc.beans.JBIListPanel();
        pnlPaineis = new javax.swing.JPanel();
        paineisList = new bsc.beans.JBIListPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDataFin = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblDataIni = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        fldDataIni = new pv.jfcx.JPVDatePlus();
        fldDataFin = new pv.jfcx.JPVDatePlus();
        fldNome = new pv.jfcx.JPVEdit();
        pnlTopRecord = new javax.swing.JPanel();
        perspectivesList = new bsc.beans.JBIListPanel();
        TemaEstList = new bsc.beans.JBIListPanel();
        desdobramentosList = new bsc.beans.JBIListPanel();
        jPanel1 = new javax.swing.JPanel();
        jToolBar = new javax.swing.JToolBar();
        btnMapas = new javax.swing.JToggleButton();
        btnPaineis = new javax.swing.JToggleButton();
        btnGrafico = new javax.swing.JToggleButton();
        btnRelatorios = new javax.swing.JToggleButton();
        btnDuplicador = new javax.swing.JToggleButton();
        pnlFerramenta = new javax.swing.JPanel();
        pnlMapas = new javax.swing.JPanel();
        mapaList = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHead = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        pnlDuplicador.setLayout(null);

        fldNomeDesdobramento.setMaxLength(60);
        pnlDuplicador.add(fldNomeDesdobramento);
        fldNomeDesdobramento.setBounds(10, 32, 480, 20);

        lblNomeDesdobramento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        lblNomeDesdobramento.setText(bundle.getString("BscEstrategiaFrame_00020")); // NOI18N
        pnlDuplicador.add(lblNomeDesdobramento);
        lblNomeDesdobramento.setBounds(10, 12, 484, 14);

        btnDesdobrar.setText(bundle.getString("BscEstrategiaFrame_00023")); // NOI18N
        btnDesdobrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnDesdobrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesdobrarActionPerformed(evt);
            }
        });
        pnlDuplicador.add(btnDesdobrar);
        btnDesdobrar.setBounds(220, 170, 90, 25);
        pnlDuplicador.add(cbOrganizacao);
        cbOrganizacao.setBounds(10, 80, 480, 20);

        lblNomeOrganizacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNomeOrganizacao.setText(bundle.getString("BscEstrategiaFrame_00021")); // NOI18N
        pnlDuplicador.add(lblNomeOrganizacao);
        lblNomeOrganizacao.setBounds(10, 60, 484, 14);

        jPanelAcao.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanelAcao.setToolTipText("");
        jPanelAcao.setLayout(new java.awt.BorderLayout());

        btnGrupo.add(jDesdobrar);
        jDesdobrar.setSelected(true);
        jDesdobrar.setText(bundle.getString("BscEstrategiaFrame_00024")); // NOI18N
        jDesdobrar.setPreferredSize(new java.awt.Dimension(80, 23));
        jDesdobrar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanelAcao.add(jDesdobrar, java.awt.BorderLayout.NORTH);

        btnGrupo.add(jDuplicar);
        jDuplicar.setText(bundle.getString("BscEstrategiaFrame_00025")); // NOI18N
        jDuplicar.setMaximumSize(new java.awt.Dimension(100, 23));
        jDuplicar.setMinimumSize(new java.awt.Dimension(100, 23));
        jDuplicar.setPreferredSize(new java.awt.Dimension(80, 23));
        jPanelAcao.add(jDuplicar, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(null);
        jPanelAcao.add(jPanel2, java.awt.BorderLayout.SOUTH);

        jPanel3.setLayout(null);
        jPanelAcao.add(jPanel3, java.awt.BorderLayout.EAST);

        jPanel4.setLayout(null);
        jPanelAcao.add(jPanel4, java.awt.BorderLayout.WEST);

        pnlDuplicador.add(jPanelAcao);
        jPanelAcao.setBounds(330, 110, 160, 60);
        pnlDuplicador.add(cbTipo);
        cbTipo.setBounds(10, 130, 300, 20);

        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTipo.setText(bundle.getString("BscEstrategiaFrame_00022")); // NOI18N
        pnlDuplicador.add(lblTipo);
        lblTipo.setBounds(10, 110, 72, 14);

        pnlGraficos.setLayout(new java.awt.BorderLayout());

        graficosList.setDocumentTarget("_self");
        graficosList.setFileType("*.jpg");
        graficosList.setFocusCycleRoot(true);
        graficosList.setLoadServerFiles("graficos\\*.jpg");
        graficosList.setServerFolder("graficos\\");
            pnlGraficos.add(graficosList, java.awt.BorderLayout.CENTER);

            pnlRelatorios.setLayout(new java.awt.BorderLayout());

            relatoriosList.setNovoEnabled(false);
            pnlRelatorios.add(relatoriosList, java.awt.BorderLayout.CENTER);

            pnlPaineis.setLayout(new java.awt.BorderLayout());
            pnlPaineis.add(paineisList, java.awt.BorderLayout.CENTER);

            setClosable(true);
            setIconifiable(true);
            setMaximizable(true);
            setResizable(true);
            setTitle(bundle.getString("BscEstrategiaFrame_00001")); // NOI18N
            setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_estrategia.gif"))); // NOI18N
            setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
            setPreferredSize(new java.awt.Dimension(543, 358));

            pnlRecord.setLayout(new java.awt.BorderLayout());

            pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
            pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
            pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
            pnlTools.setLayout(new java.awt.BorderLayout());

            pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
            pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
            pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
            pnlConfirmation.setLayout(new java.awt.BorderLayout());

            tbConfirmation.setFloatable(false);
            tbConfirmation.setRollover(true);
            tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

            btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
            btnSave.setText(bundle.getString("BscEstrategiaFrame_00002")); // NOI18N
            btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
            btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
            btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
            btnSave.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
                }
            });
            tbConfirmation.add(btnSave);

            btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
            btnCancel.setText(bundle.getString("BscEstrategiaFrame_00003")); // NOI18N
            btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
            btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
            btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
            btnCancel.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
                }
            });
            tbConfirmation.add(btnCancel);

            pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

            pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

            pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
            pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
            pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
            pnlOperation.setLayout(new java.awt.BorderLayout());

            tbInsertion.setFloatable(false);
            tbInsertion.setRollover(true);
            tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

            btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
            btnEdit.setText(bundle.getString("BscEstrategiaFrame_00014")); // NOI18N
            btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
            btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
            btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
            btnEdit.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnEditActionPerformed(evt);
                }
            });
            tbInsertion.add(btnEdit);

            btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
            btnDelete.setText(bundle.getString("BscEstrategiaFrame_00004")); // NOI18N
            btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
            btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
            btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
            btnDelete.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
                }
            });
            tbInsertion.add(btnDelete);

            btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
            btnReload.setText(bundle.getString("BscEstrategiaFrame_00005")); // NOI18N
            btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
            btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
            btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
            btnReload.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnReloadActionPerformed(evt);
                }
            });
            tbInsertion.add(btnReload);

            btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
            btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
            btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
            btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
            btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
            btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
            btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
            btnReload1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnReload1ActionPerformed(evt);
                }
            });
            tbInsertion.add(btnReload1);

            pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

            pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

            pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

            pnlFields.setLayout(new java.awt.BorderLayout());
            pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
            pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
            pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

            scpRecord.setBorder(null);

            pnlData.setPreferredSize(new java.awt.Dimension(471, 171));
            pnlData.setLayout(null);

            lblNome.setForeground(new java.awt.Color(51, 51, 255));
            lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            lblNome.setText(bundle.getString("BscEstrategiaFrame_00006")); // NOI18N
            lblNome.setEnabled(false);
            pnlData.add(lblNome);
            lblNome.setBounds(10, 10, 70, 20);

            lblDataFin.setForeground(new java.awt.Color(51, 51, 255));
            lblDataFin.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            lblDataFin.setText(bundle.getString("BscEstrategiaFrame_00007")); // NOI18N
            lblDataFin.setEnabled(false);
            pnlData.add(lblDataFin);
            lblDataFin.setBounds(10, 190, 70, 20);

            lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            lblDescricao.setText(bundle.getString("BscEstrategiaFrame_00008")); // NOI18N
            lblDescricao.setEnabled(false);
            pnlData.add(lblDescricao);
            lblDescricao.setBounds(10, 50, 70, 20);

            lblDataIni.setForeground(new java.awt.Color(51, 51, 255));
            lblDataIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            lblDataIni.setText(bundle.getString("BscEstrategiaFrame_00009")); // NOI18N
            lblDataIni.setEnabled(false);
            pnlData.add(lblDataIni);
            lblDataIni.setBounds(10, 150, 70, 20);

            jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            jScrollPane3.setViewportView(txtDescricao);

            pnlData.add(jScrollPane3);
            jScrollPane3.setBounds(10, 70, 400, 80);

            fldDataIni.setDialog(true);
            fldDataIni.setUseLocale(true);
            fldDataIni.setWaitForCalendarDate(true);
            pnlData.add(fldDataIni);
            fldDataIni.setBounds(10, 170, 180, 22);

            fldDataFin.setDialog(true);
            fldDataFin.setUseLocale(true);
            fldDataFin.setWaitForCalendarDate(true);
            pnlData.add(fldDataFin);
            fldDataFin.setBounds(10, 210, 180, 22);

            fldNome.setMaxLength(60);
            pnlData.add(fldNome);
            fldNome.setBounds(10, 30, 400, 22);

            scpRecord.setViewportView(pnlData);

            pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

            pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
            pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

            pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

            tapCadastro.addTab(bundle.getString("BscEstrategiaFrame_00001"), pnlRecord); // NOI18N
            tapCadastro.addTab(bundle.getString("BscEstrategiaFrame_00010"), perspectivesList); // NOI18N
            tapCadastro.addTab(bundle.getString("BscEstrategiaFrame_00027"), TemaEstList); // NOI18N
            tapCadastro.addTab(bundle.getString("BscEstrategiaFrame_00026"), desdobramentosList); // NOI18N

            jPanel1.setLayout(new java.awt.BorderLayout());

            jToolBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
            jToolBar.setDoubleBuffered(true);
            jToolBar.setFocusable(false);
            jToolBar.setPreferredSize(new java.awt.Dimension(347, 45));

            btnGrupo.add(btnMapas);
            btnMapas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
            btnMapas.setText(" Scorecard ");
            btnMapas.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
            btnMapas.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
            btnMapas.setMargin(new java.awt.Insets(2, 20, 2, 20));
            btnMapas.setPreferredSize(new java.awt.Dimension(59, 39));
            btnMapas.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            btnMapas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
            btnMapas.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnMapasActionPerformed(evt);
                }
            });
            jToolBar.add(btnMapas);

            btnGrupo.add(btnPaineis);
            btnPaineis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_painel.gif"))); // NOI18N
            btnPaineis.setText(bundle.getString("BscEstrategiaFrame_00012")); // NOI18N
            btnPaineis.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
            btnPaineis.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
            btnPaineis.setMargin(new java.awt.Insets(2, 20, 2, 20));
            btnPaineis.setPreferredSize(new java.awt.Dimension(107, 39));
            btnPaineis.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            btnPaineis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
            btnPaineis.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnPaineisActionPerformed(evt);
                }
            });
            jToolBar.add(btnPaineis);

            btnGrupo.add(btnGrafico);
            btnGrafico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_grafico.gif"))); // NOI18N
            btnGrafico.setText(bundle.getString("BscEstrategiaFrame_00015")); // NOI18N
            btnGrafico.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
            btnGrafico.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
            btnGrafico.setMargin(new java.awt.Insets(2, 20, 2, 20));
            btnGrafico.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            btnGrafico.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
            btnGrafico.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnGraficoActionPerformed(evt);
                }
            });
            jToolBar.add(btnGrafico);

            btnGrupo.add(btnRelatorios);
            btnRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_compromisso.gif"))); // NOI18N
            btnRelatorios.setText(bundle.getString("BscEstrategiaFrame_00013")); // NOI18N
            btnRelatorios.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
            btnRelatorios.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
            btnRelatorios.setMargin(new java.awt.Insets(2, 20, 2, 20));
            btnRelatorios.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            btnRelatorios.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
            btnRelatorios.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnRelatoriosActionPerformed(evt);
                }
            });
            jToolBar.add(btnRelatorios);

            btnGrupo.add(btnDuplicador);
            btnDuplicador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_copy.gif"))); // NOI18N
            btnDuplicador.setText(bundle.getString("BscEstrategiaFrame_00019")); // NOI18N
            btnDuplicador.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
            btnDuplicador.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
            btnDuplicador.setMargin(new java.awt.Insets(2, 20, 2, 20));
            btnDuplicador.setPreferredSize(new java.awt.Dimension(53, 39));
            btnDuplicador.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            btnDuplicador.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
            btnDuplicador.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDuplicadorActionPerformed(evt);
                }
            });
            jToolBar.add(btnDuplicador);

            jPanel1.add(jToolBar, java.awt.BorderLayout.NORTH);

            pnlFerramenta.setLayout(new java.awt.BorderLayout());

            pnlMapas.setLayout(new java.awt.BorderLayout());

            mapaList.setNovoEnabled(false);
            pnlMapas.add(mapaList, java.awt.BorderLayout.CENTER);

            pnlFerramenta.add(pnlMapas, java.awt.BorderLayout.CENTER);

            jPanel1.add(pnlFerramenta, java.awt.BorderLayout.CENTER);

            tapCadastro.addTab(bundle.getString("BscEstrategiaFrame_00011"), jPanel1); // NOI18N

            getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

            pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 15));
            pnlTopForm.setLayout(new java.awt.BorderLayout());

            pnlHead.setMinimumSize(new java.awt.Dimension(520, 45));
            pnlTopForm.add(pnlHead, java.awt.BorderLayout.NORTH);

            getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

            pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
            pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
            getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

            pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
            pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
            getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

            pnlLeftForm.setMinimumSize(new java.awt.Dimension(5, 5));
            pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
            getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

            pack();
        }// </editor-fold>//GEN-END:initComponents

    private void btnPaineisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaineisActionPerformed
        pnlFerramenta.removeAll();
        pnlFerramenta.repaint();
        pnlFerramenta.add(pnlPaineis);
        pnlPaineis.setVisible(true);
        pnlPaineis.revalidate();
        pnlFerramenta.revalidate();
    }//GEN-LAST:event_btnPaineisActionPerformed

    private void btnRelatoriosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRelatoriosActionPerformed
        pnlFerramenta.removeAll();
        pnlFerramenta.repaint();
        pnlFerramenta.add(pnlRelatorios);
        pnlRelatorios.setVisible(true);
        pnlFerramenta.revalidate();
    }//GEN-LAST:event_btnRelatoriosActionPerformed

    private void btnDuplicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDuplicadorActionPerformed
        pnlFerramenta.removeAll();
        pnlFerramenta.repaint();
        pnlFerramenta.add(pnlDuplicador);
        pnlDuplicador.setVisible(true);
        pnlFerramenta.revalidate();
        jDesdobrar.setSelected(true);
    }//GEN-LAST:event_btnDuplicadorActionPerformed

    private void btnGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficoActionPerformed
        pnlFerramenta.removeAll();
        pnlFerramenta.repaint();
        pnlFerramenta.add(pnlGraficos);
        pnlGraficos.setVisible(true);
        pnlFerramenta.revalidate();
    }//GEN-LAST:event_btnGraficoActionPerformed

    private void btnMapasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMapasActionPerformed
        pnlFerramenta.removeAll();
        pnlFerramenta.repaint();
        pnlFerramenta.add(pnlMapas);
        pnlMapas.setVisible(true);
        pnlMapas.revalidate();
        pnlFerramenta.revalidate();
    }//GEN-LAST:event_btnMapasActionPerformed

	private void btnDesdobrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesdobrarActionPerformed
            bsc.swing.BscDefaultDialogSystem dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
            if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstrategiaFrame_00029")) == javax.swing.JOptionPane.YES_OPTION) {
                event.executeRecord(fldNomeDesdobramento.getText() + ";" + htbOrganizacao.get(cbOrganizacao.getSelectedItem()).toString() + ";" + jDuplicar.isSelected() + ";" + (cbTipo.getSelectedIndex() + 1));
            }
	}//GEN-LAST:event_btnDesdobrarActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("ESTRATEGIAS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            if (getStatus() != bsc.swing.BscDefaultFrameBehavior.INSERTING) // Frequencia de Avalia��o de Resultado
            {
                if (fldDataIni.getText().trim().length() > 0 && fldDataFin.getText().trim().length() > 0) {
                    if (fldDataIni.getCalendar().after(record.getDate("DATAINI")) || fldDataFin.getCalendar().before(record.getDate("DATAFIN"))) {
                        if (JOptionPane.showConfirmDialog(this,
                                ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstrategiaFrame_00016") +
                                ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstrategiaFrame_00017"),
                                ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstrategiaFrame_00018"), javax.swing.JOptionPane.YES_NO_OPTION) != javax.swing.JOptionPane.YES_OPTION) {
                            return;
                        }
                    }
                }
            }
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private bsc.beans.JBIListPanel TemaEstList;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDesdobrar;
    private javax.swing.JToggleButton btnDuplicador;
    private javax.swing.JButton btnEdit;
    private javax.swing.JToggleButton btnGrafico;
    private javax.swing.ButtonGroup btnGrupo;
    private javax.swing.JToggleButton btnMapas;
    private javax.swing.JToggleButton btnPaineis;
    private javax.swing.JToggleButton btnRelatorios;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbOrganizacao;
    private javax.swing.JComboBox cbTipo;
    private bsc.beans.JBIListPanel desdobramentosList;
    private pv.jfcx.JPVDatePlus fldDataFin;
    private pv.jfcx.JPVDatePlus fldDataIni;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldNomeDesdobramento;
    private bsc.beans.JBIListPanelPlus graficosList;
    private javax.swing.JRadioButton jDesdobrar;
    private javax.swing.JRadioButton jDuplicar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanelAcao;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar jToolBar;
    private javax.swing.JLabel lblDataFin;
    private javax.swing.JLabel lblDataIni;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeDesdobramento;
    private javax.swing.JLabel lblNomeOrganizacao;
    private javax.swing.JLabel lblTipo;
    private bsc.beans.JBIListPanel mapaList;
    private bsc.beans.JBIListPanel paineisList;
    private bsc.beans.JBIListPanel perspectivesList;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDuplicador;
    private javax.swing.JPanel pnlFerramenta;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlGraficos;
    private bsc.beans.JBIHeadPanel pnlHead;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlMapas;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPaineis;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRelatorios;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private bsc.beans.JBIListPanel relatoriosList;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscEstrategiaFrame(int operation, String idAux, String contextId) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
