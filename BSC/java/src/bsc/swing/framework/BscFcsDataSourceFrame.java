package bsc.swing.framework;

import java.util.ResourceBundle;

public class BscFcsDataSourceFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("FCSDATASRC");
    public static int BSC_SRC_ENVIRONMENT = 1;
    public static int BSC_SRC_CUSTOM = 2;

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblAgendamento.setEnabled(true);
        lblClasse.setEnabled(true);
        cbbClasse.setEnabled(true);
        //chkRecriar.setEnabled(true);

        radAmbiente.setEnabled(true);
        radTopConnect.setEnabled(true);
        if (cbbClasse.getSelectedIndex() == 2) {
            radAmbiente.setSelected(true);
            radTopConnect.setEnabled(false);
        }

        enableTopFields();
        lblDeclara.setEnabled(true);
        txtDeclaracoes.setEnabled(true);
        chkReferencia.setEnabled(true);
    }

    void enableTopFields() {
        lblAmbiente.setEnabled(radAmbiente.isSelected());
        fldAmbiente.setEnabled(radAmbiente.isSelected());

        lblDatabase.setEnabled(radTopConnect.isSelected());
        fldDataBase.setEnabled(radTopConnect.isSelected());
        lblAlias.setEnabled(radTopConnect.isSelected());
        fldAlias.setEnabled(radTopConnect.isSelected());
        lbServer.setEnabled(radTopConnect.isSelected());
        fldServer.setEnabled(radTopConnect.isSelected());
        lblConType.setEnabled(radTopConnect.isSelected());
        fldConType.setEnabled(radTopConnect.isSelected());
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblAgendamento.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblClasse.setEnabled(false);
        cbbClasse.setEnabled(false);
        //chkRecriar.setEnabled(false);
        chkReferencia.setEnabled(false);

        radAmbiente.setEnabled(false);
        lblAmbiente.setEnabled(false);
        fldAmbiente.setEnabled(false);

        radTopConnect.setEnabled(false);
        lblDatabase.setEnabled(false);
        fldDataBase.setEnabled(false);
        lblAlias.setEnabled(false);
        fldAlias.setEnabled(false);
        lbServer.setEnabled(false);
        fldServer.setEnabled(false);
        lblConType.setEnabled(false);
        fldConType.setEnabled(false);

        txtDeclaracoes.setEnabled(false);
    }
    java.util.Hashtable htbClasse = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        lblCodigo.setText(record.getString("ID"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        event.populateCombo(record.getBIXMLVector("CLASSES"), "CLASSE", htbClasse, cbbClasse);
        cbbClasse.setSelectedIndex(record.getInt("CLASSE"));
        chkReferencia.setSelected(record.getBoolean("REFER"));
        //chkRecriar.setSelected(record.getBoolean("RECRIA"));

        radAmbiente.setSelected(record.getInt("TIPOENV") == BSC_SRC_ENVIRONMENT);
        fldAmbiente.setText(record.getString("ENVIRON"));
        radTopConnect.setSelected(record.getInt("TIPOENV") == BSC_SRC_CUSTOM);
        fldDataBase.setText(record.getString("TOPDB"));
        fldAlias.setText(record.getString("TOPALIAS"));
        fldServer.setText(record.getString("TOPSERVER"));
        fldConType.setText(record.getString("TOPCONTYPE"));

        txtDeclaracoes.setText(record.getString("TEXTO"));

        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");
        pnlHeader.setComponents(vctContexto);

    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("CLASSE", cbbClasse.getSelectedIndex());
        recordAux.set("REFER", chkReferencia.isSelected());
        //recordAux.set( "RECRIA", chkRecriar.isSelected() );

        recordAux.set("TIPOENV", (radAmbiente.isSelected() ? BSC_SRC_ENVIRONMENT : BSC_SRC_CUSTOM));
        recordAux.set("ENVIRON", fldAmbiente.getText());
        recordAux.set("TOPDB", fldDataBase.getText());
        recordAux.set("TOPALIAS", fldAlias.getText());
        recordAux.set("TOPSERVER", fldServer.getText());
        recordAux.set("TOPCONTYPE", fldConType.getText());

        recordAux.set("TEXTO", txtDeclaracoes.getText());

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTopConnect = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnImportar = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel8 = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblClasse = new javax.swing.JLabel();
        cbbClasse = new javax.swing.JComboBox();
        lblAgendamento = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        chkReferencia = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanelConfig = new javax.swing.JPanel();
        lblDatabase = new javax.swing.JLabel();
        lblAmbiente = new javax.swing.JLabel();
        lblAlias = new javax.swing.JLabel();
        lbServer = new javax.swing.JLabel();
        radAmbiente = new javax.swing.JRadioButton();
        radTopConnect = new javax.swing.JRadioButton();
        lblConType = new javax.swing.JLabel();
        btnTestar = new javax.swing.JButton();
        fldAmbiente = new pv.jfcx.JPVEdit();
        fldDataBase = new pv.jfcx.JPVEdit();
        fldAlias = new pv.jfcx.JPVEdit();
        fldServer = new pv.jfcx.JPVEdit();
        fldConType = new pv.jfcx.JPVEdit();
        jPanelConfig1 = new javax.swing.JPanel();
        lblDeclara = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDeclaracoes = new javax.swing.JTextArea();
        btnTestSintax = new javax.swing.JButton();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscDataSourceFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_planilha.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscDataSourceFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscDataSourceFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscDataSourceFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscDataSourceFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscDataSourceFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnImportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ferramentas.gif"))); // NOI18N
        btnImportar.setText(bundle.getString("BscDataSourceFrame_00009")); // NOI18N
        btnImportar.setActionCommand(bundle.getString("BscDataSourceFrame_00007")); // NOI18N
        btnImportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnImportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnImportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnImportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnImportar);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 300));
        scpRecord.setRequestFocusEnabled(false);

        pnlData.setPreferredSize(new java.awt.Dimension(490, 260));
        pnlData.setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        jScrollPane5.setBorder(null);
        jScrollPane5.setPreferredSize(new java.awt.Dimension(530, 180));

        jPanel8.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscDataSourceFrame_00008")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblNome);
        lblNome.setBounds(9, 9, 98, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscDataSourceFrame_00010")); // NOI18N
        lblDescricao.setEnabled(false);
        jPanel8.add(lblDescricao);
        lblDescricao.setBounds(10, 90, 76, 20);

        lblClasse.setForeground(new java.awt.Color(51, 102, 255));
        lblClasse.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblClasse.setText(bundle.getString("BscDataSourceFrame_00011")); // NOI18N
        lblClasse.setEnabled(false);
        lblClasse.setMaximumSize(new java.awt.Dimension(100, 16));
        lblClasse.setMinimumSize(new java.awt.Dimension(100, 16));
        lblClasse.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblClasse);
        lblClasse.setBounds(10, 160, 98, 20);

        cbbClasse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbClasseItemStateChanged(evt);
            }
        });
        jPanel8.add(cbbClasse);
        cbbClasse.setBounds(10, 180, 180, 22);

        lblAgendamento.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAgendamento.setText(bundle.getString("BscDataSourceFrame_00013")); // NOI18N
        lblAgendamento.setEnabled(false);
        lblAgendamento.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAgendamento.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAgendamento.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblAgendamento);
        lblAgendamento.setBounds(10, 50, 106, 18);

        lblCodigo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCodigo.setText("0000");
        lblCodigo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        lblCodigo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCodigo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCodigo.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel8.add(lblCodigo);
        lblCodigo.setBounds(10, 70, 180, 22);

        txtDescricao.setColumns(80);
        txtDescricao.setRows(0);
        jScrollPane2.setViewportView(txtDescricao);

        jPanel8.add(jScrollPane2);
        jScrollPane2.setBounds(10, 110, 400, 50);

        fldNome.setMaxLength(60);
        jPanel8.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        chkReferencia.setText(bundle.getString("BscDataSourceFrame_00021")); // NOI18N
        chkReferencia.setEnabled(false);
        jPanel8.add(chkReferencia);
        chkReferencia.setBounds(10, 210, 180, 23);

        jScrollPane5.setViewportView(jPanel8);

        jPanel1.add(jScrollPane5, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("BscDataSourceFrame_00022"), jPanel1); // NOI18N

        jPanel3.setLayout(new java.awt.BorderLayout());

        jScrollPane4.setBorder(null);
        jScrollPane4.setPreferredSize(new java.awt.Dimension(530, 300));

        jPanelConfig.setLayout(null);

        lblDatabase.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDatabase.setText("dbDataBase:");
        lblDatabase.setEnabled(false);
        lblDatabase.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDatabase.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDatabase.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblDatabase);
        lblDatabase.setBounds(10, 110, 98, 18);

        lblAmbiente.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAmbiente.setText(bundle.getString("BscDataSourceFrame_00014")); // NOI18N
        lblAmbiente.setEnabled(false);
        lblAmbiente.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAmbiente.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblAmbiente);
        lblAmbiente.setBounds(10, 30, 98, 18);

        lblAlias.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAlias.setText("dbAlias:");
        lblAlias.setEnabled(false);
        lblAlias.setMaximumSize(new java.awt.Dimension(100, 16));
        lblAlias.setMinimumSize(new java.awt.Dimension(100, 16));
        lblAlias.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblAlias);
        lblAlias.setBounds(10, 150, 98, 18);

        lbServer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbServer.setText("dbServer:");
        lbServer.setEnabled(false);
        lbServer.setMaximumSize(new java.awt.Dimension(100, 16));
        lbServer.setMinimumSize(new java.awt.Dimension(100, 16));
        lbServer.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lbServer);
        lbServer.setBounds(10, 190, 98, 18);

        buttonGroupTopConnect.add(radAmbiente);
        radAmbiente.setText(bundle.getString("BscDataSourceFrame_00015")); // NOI18N
        radAmbiente.setEnabled(false);
        radAmbiente.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                radAmbienteStateChanged(evt);
            }
        });
        jPanelConfig.add(radAmbiente);
        radAmbiente.setBounds(10, 6, 332, 23);

        buttonGroupTopConnect.add(radTopConnect);
        radTopConnect.setText(bundle.getString("BscDataSourceFrame_00016")); // NOI18N
        radTopConnect.setEnabled(false);
        radTopConnect.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                radTopConnectStateChanged(evt);
            }
        });
        jPanelConfig.add(radTopConnect);
        radTopConnect.setBounds(10, 80, 268, 23);

        lblConType.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConType.setText("dbContype:");
        lblConType.setEnabled(false);
        lblConType.setMaximumSize(new java.awt.Dimension(100, 16));
        lblConType.setMinimumSize(new java.awt.Dimension(100, 16));
        lblConType.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanelConfig.add(lblConType);
        lblConType.setBounds(10, 230, 98, 18);

        btnTestar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnTestar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif"))); // NOI18N
        btnTestar.setText(bundle.getString("BscDataSourceFrame_00017")); // NOI18N
        btnTestar.setEnabled(false);
        btnTestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestarActionPerformed(evt);
            }
        });
        jPanelConfig.add(btnTestar);
        btnTestar.setBounds(380, 130, 137, 25);

        fldAmbiente.setMaxLength(60);
        jPanelConfig.add(fldAmbiente);
        fldAmbiente.setBounds(10, 50, 180, 22);

        fldDataBase.setMaxLength(60);
        jPanelConfig.add(fldDataBase);
        fldDataBase.setBounds(10, 130, 180, 22);

        fldAlias.setMaxLength(60);
        jPanelConfig.add(fldAlias);
        fldAlias.setBounds(10, 170, 180, 22);

        fldServer.setMaxLength(60);
        jPanelConfig.add(fldServer);
        fldServer.setBounds(10, 210, 180, 22);

        fldConType.setMaxLength(60);
        jPanelConfig.add(fldConType);
        fldConType.setBounds(10, 250, 180, 22);

        jScrollPane4.setViewportView(jPanelConfig);

        jPanel3.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("BscDataSourceFrame_00023"), jPanel3); // NOI18N

        jPanelConfig1.setPreferredSize(new java.awt.Dimension(530, 300));
        jPanelConfig1.setLayout(null);

        lblDeclara.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDeclara.setText(bundle.getString("BscDataSourceFrame_00018")); // NOI18N
        lblDeclara.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDeclara.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDeclara.setPreferredSize(new java.awt.Dimension(100, 16));
        lblDeclara.setEnabled(false);
        jPanelConfig1.add(lblDeclara);
        lblDeclara.setBounds(12, 8, 98, 18);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(500, 21));

        txtDeclaracoes.setColumns(80);
        txtDeclaracoes.setLineWrap(true);
        txtDeclaracoes.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtDeclaracoes);

        jPanelConfig1.add(jScrollPane1);
        jScrollPane1.setBounds(12, 33, 463, 175);

        btnTestSintax.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnTestSintax.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnTestSintax.setText(bundle.getString("BscDataSourceFrame_00019")); // NOI18N
        btnTestSintax.setEnabled(false);
        btnTestSintax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestSintaxActionPerformed(evt);
            }
        });
        jPanelConfig1.add(btnTestSintax);
        btnTestSintax.setBounds(331, 4, 141, 25);

        jTabbedPane1.addTab(bundle.getString("BscDataSourceFrame_00020"), jPanelConfig1); // NOI18N

        pnlData.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscDataSourceFrame_00001"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 90));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("DADOS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void cbbClasseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbClasseItemStateChanged
            radAmbiente.setEnabled(true);
            radTopConnect.setEnabled(true);

            if (cbbClasse.getSelectedIndex() == 2) {
                radAmbiente.setSelected(true);
                radTopConnect.setEnabled(false);
            }

            enableTopFields();
	}//GEN-LAST:event_cbbClasseItemStateChanged

	private void btnTestSintaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestSintaxActionPerformed
            event.executeRecord("TESTSINTAX");
	}//GEN-LAST:event_btnTestSintaxActionPerformed

	private void btnTestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestarActionPerformed
            event.executeRecord("TESTCON");
	}//GEN-LAST:event_btnTestarActionPerformed

	private void btnImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarActionPerformed
            event.executeRecord("IMPORTCON");
	}//GEN-LAST:event_btnImportarActionPerformed

	private void radTopConnectStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_radTopConnectStateChanged
            if (getStatus() == event.INSERTING || getStatus() == event.UPDATING) {
                enableTopFields();
            }
	}//GEN-LAST:event_radTopConnectStateChanged

	private void radAmbienteStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_radAmbienteStateChanged
            if (getStatus() == event.INSERTING || getStatus() == event.UPDATING) {
                enableTopFields();
            }
	}//GEN-LAST:event_radAmbienteStateChanged

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnImportar;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTestSintax;
    private javax.swing.JButton btnTestar;
    private javax.swing.ButtonGroup buttonGroupTopConnect;
    private javax.swing.JComboBox cbbClasse;
    private javax.swing.JCheckBox chkReferencia;
    private pv.jfcx.JPVEdit fldAlias;
    private pv.jfcx.JPVEdit fldAmbiente;
    private pv.jfcx.JPVEdit fldConType;
    private pv.jfcx.JPVEdit fldDataBase;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldServer;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanelConfig;
    private javax.swing.JPanel jPanelConfig1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbServer;
    private javax.swing.JLabel lblAgendamento;
    private javax.swing.JLabel lblAlias;
    private javax.swing.JLabel lblAmbiente;
    private javax.swing.JLabel lblClasse;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblConType;
    private javax.swing.JLabel lblDatabase;
    private javax.swing.JLabel lblDeclara;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton radAmbiente;
    private javax.swing.JRadioButton radTopConnect;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JTextArea txtDeclaracoes;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscFcsDataSourceFrame(int operation, String idAux, String contextId) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
        btnTestar.setEnabled(false);
        btnTestSintax.setEnabled(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
        btnTestar.setEnabled(true);
        btnTestSintax.setEnabled(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
