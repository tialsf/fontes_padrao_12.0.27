package bsc.swing.framework;

import bsc.swing.BscDefaultFrameBehavior;
import java.util.ResourceBundle;

public class BscFcsIndicadorFrame extends bsc.swing.BscInternalExecute implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("FCSIND");
    private String respId = "";
    private String tipoPessoa = " ";
    private String refRespId = "";
    private String refTipoPessoa = " ";
    private String colRespId = "";
    private String colTipoPessoa = " ";

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblUnidade.setEnabled(true);
        cbbUnidade.setEnabled(true);
        lblCasasDecimais.setEnabled(true);
        spnCasasDecimais.setEnabled(true);
        lblFrequencia.setEnabled(true);
        cbbFrequencia.setEnabled(true);
        lblResponsavel.setEnabled(true);
        pnlOrientacao.setEnabled(true);
        rdbAscendente.setEnabled(true);
        rdbDescendente.setEnabled(true);
        lblResponsavel.setEnabled(true);
        fldResponsavel.setEnabled(true);
        btnResponsavel.setEnabled(true);
        fldResponsavelColeta.setEnabled(true);
        btnResponsavelColeta.setEnabled(true);
        fldResponsavelRef.setEnabled(true);
        btnResponsavelRef.setEnabled(true);
        chkCumulativo.setEnabled(true);
        // coleta
        lblResponsavelColeta.setEnabled(true);
        lblMetrica.setEnabled(true);
        txtMetrica.setEnabled(true);
        lblForma.setEnabled(true);
        txtForma.setEnabled(true);
        // referencia
        lblNomeRef.setEnabled(true);
        fldNomeRef.setEnabled(true);
        lblDescricaoRef.setEnabled(true);
        txtDescricaoRef.setEnabled(true);
        lblUnidadeRef.setEnabled(true);
        cbbUnidadeRef.setEnabled(true);
        lblCasasDecimaisRef.setEnabled(true);
        spnCasasDecimaisRef.setEnabled(true);
        lblFrequenciaRef.setEnabled(true);
        lblResponsavelRef.setEnabled(true);
        cbbFCumula.setEnabled(true);
        cbbTCumula.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        pnlOrientacao.setEnabled(false);
        rdbAscendente.setEnabled(false);
        rdbDescendente.setEnabled(false);
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblUnidade.setEnabled(false);
        cbbUnidade.setEnabled(false);
        lblCasasDecimais.setEnabled(false);
        spnCasasDecimais.setEnabled(false);
        lblFrequencia.setEnabled(false);
        cbbFrequencia.setEnabled(false);
        lblResponsavel.setEnabled(false);
        fldResponsavel.setEnabled(false);
        btnResponsavel.setEnabled(false);
        fldResponsavelColeta.setEnabled(false);
        btnResponsavelColeta.setEnabled(false);
        fldResponsavelRef.setEnabled(false);
        btnResponsavelRef.setEnabled(false);
        chkCumulativo.setEnabled(false);
        // coleta
        lblResponsavelColeta.setEnabled(false);
        lblMetrica.setEnabled(false);
        txtMetrica.setEnabled(false);
        lblForma.setEnabled(false);
        txtForma.setEnabled(false);
        // referencia
        lblNomeRef.setEnabled(false);
        fldNomeRef.setEnabled(false);
        lblDescricaoRef.setEnabled(false);
        txtDescricaoRef.setEnabled(false);
        lblUnidadeRef.setEnabled(false);
        cbbUnidadeRef.setEnabled(false);
        lblCasasDecimaisRef.setEnabled(false);
        spnCasasDecimaisRef.setEnabled(false);
        lblFrequenciaRef.setEnabled(false);
        lblResponsavelRef.setEnabled(false);
        lblResponsavelRef.setEnabled(false);
        cbbFCumula.setEnabled(false);
        cbbTCumula.setEnabled(false);
    }
    java.util.Hashtable htbUnidade = new java.util.Hashtable(),
            htbFrequencia = new java.util.Hashtable(),
            htbUnidadeRef = new java.util.Hashtable(),
            htbFrequenciaRef = new java.util.Hashtable(),
            htbFCumula = new java.util.Hashtable(),
            htbTCumula = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        listaMetas.setDataSource(
                record.getBIXMLVector("FCSMETAS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaAvaliacoes.setDataSource(
                record.getBIXMLVector("FCSAVALIACOES"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaFontes.setDataSource(
                record.getBIXMLVector("FCSDATASRCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);

        valoresList.setDataSource(
                record.getBIXMLVector("FCSPLANS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaDocumento.setDataSource(
                record.getBIXMLVector("FCSDOCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);

        // Indicador
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        spnCasasDecimais.setValue(new Integer(record.getInt("DECIMAIS")));
        chkCumulativo.setSelected(record.getBoolean("CUMULATIVO"));
        respId = record.getString("RESPID");
        tipoPessoa = record.getString("TIPOPESSOA");
        fldResponsavel.setText(record.getString("RESPONSAVEL"));
        refRespId = record.getString("RRESPID");
        refTipoPessoa = record.getString("RTIPOPES");
        fldResponsavelRef.setText(record.getString("RRESPONSAVEL"));
        colRespId = record.getString("MEDRESPID");
        colTipoPessoa = record.getString("MEDTIPOPES");
        fldResponsavelColeta.setText(record.getString("CRESPONSAVEL"));

        if (getStatus() == BscDefaultFrameBehavior.INSERTING) {
            rdbAscendente.setSelected(true);
        } else if (record.getBoolean("ASCEND")) {
            rdbAscendente.setSelected(true);
        } else {
            rdbDescendente.setSelected(true);
        }

        event.populateComboWithName(record.getBIXMLVector("UNIDADES"), "UNIDADE",
                htbUnidade, cbbUnidade);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FREQ",
                htbFrequencia, cbbFrequencia);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FCUMULA",
                htbFCumula, cbbFCumula, getIgnoreIDList());

        event.populateCombo(record.getBIXMLVector("TIPOMONTS"), "TCUMULA",
                htbTCumula, cbbTCumula);

        // Coleta
        txtMetrica.setText(record.getString("METRICA"));
        txtForma.setText(record.getString("FORMA"));

        // Referencia
        fldNomeRef.setText(record.getString("RNOME"));
        txtDescricaoRef.setText(record.getString("RDESCRICAO"));
        spnCasasDecimaisRef.setValue(new Integer(record.getInt("RDECIMAIS")));

        event.populateComboWithName(record.getBIXMLVector("UNIDADES"), "RUNIDADE",
                htbUnidadeRef, cbbUnidadeRef);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "RFREQ",
                htbFrequenciaRef, cbbFrequenciaRef);

        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");

        pnlHeader.setComponents(vctContexto);

    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        // Indicador
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("RESPID", respId);
        recordAux.set("TIPOPESSOA", tipoPessoa);
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("DECIMAIS",
                ((Integer) spnCasasDecimais.getValue()).intValue());
        recordAux.set("UNIDADE", (String) cbbUnidade.getSelectedItem());
        recordAux.set("FREQ", event.getComboValue(htbFrequencia, cbbFrequencia));
        recordAux.set("DATASRCID", "0");
        recordAux.set("CUMULATIVO", chkCumulativo.isSelected());
        recordAux.set("ASCEND", rdbAscendente.isSelected());
        recordAux.set("FCUMULA", event.getComboValue(htbFCumula, cbbFCumula));
        recordAux.set("TCUMULA", event.getComboValue(htbTCumula, cbbTCumula));

        // Coleta
        recordAux.set("METRICA", txtMetrica.getText());
        recordAux.set("FORMA", txtForma.getText());
        recordAux.set("MEDRESPID", colRespId);
        recordAux.set("MEDTIPOPES", colTipoPessoa);

        // Referencia
        recordAux.set("RNOME", fldNomeRef.getText());
        recordAux.set("RDESCRICAO", txtDescricaoRef.getText());
        recordAux.set("RDECIMAIS", ((Integer) spnCasasDecimaisRef.getValue()).intValue());
        recordAux.set("RUNIDADE", (String) cbbUnidadeRef.getSelectedItem());
        recordAux.set("RFREQ", event.getComboValue(htbFrequenciaRef, cbbFrequenciaRef));
        recordAux.set("RDATASRCID", "0");
        recordAux.set("RRESPID", refRespId);
        recordAux.set("RTIPOPES", refTipoPessoa);

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        if (cbbFrequencia.getSelectedIndex() == 0 || fldNome.getText().trim().length() == 0) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        } else {
            valid = true;
        }

        return valid;

    }

    public boolean hasCombos() {
        return true;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpOrder = new javax.swing.ButtonGroup();
        bgpOrderRef = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblUnidade = new javax.swing.JLabel();
        cbbUnidade = new javax.swing.JComboBox();
        lblCasasDecimais = new javax.swing.JLabel();
        spnCasasDecimais = new javax.swing.JSpinner();
        cbbFrequencia = new javax.swing.JComboBox();
        lblResponsavel = new javax.swing.JLabel();
        lblFrequencia = new javax.swing.JLabel();
        pnlOrientacao = new javax.swing.JPanel();
        rdbAscendente = new javax.swing.JRadioButton();
        rdbDescendente = new javax.swing.JRadioButton();
        lblDescricao = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        chkCumulativo = new javax.swing.JCheckBox();
        cbbFCumula = new javax.swing.JComboBox();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        cbbTCumula = new javax.swing.JComboBox();
        scpRecord1 = new javax.swing.JScrollPane();
        pnlData1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtMetrica = new bsc.beans.JBITextArea();
        lblResponsavelColeta = new javax.swing.JLabel();
        lblMetrica = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtForma = new bsc.beans.JBITextArea();
        lblForma = new javax.swing.JLabel();
        fldResponsavelColeta = new pv.jfcx.JPVEdit();
        btnResponsavelColeta = new javax.swing.JButton();
        scpRecord2 = new javax.swing.JScrollPane();
        pnlData2 = new javax.swing.JPanel();
        lblNomeRef = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtDescricaoRef = new bsc.beans.JBITextArea();
        lblUnidadeRef = new javax.swing.JLabel();
        cbbUnidadeRef = new javax.swing.JComboBox();
        lblCasasDecimaisRef = new javax.swing.JLabel();
        spnCasasDecimaisRef = new javax.swing.JSpinner();
        cbbFrequenciaRef = new javax.swing.JComboBox();
        lblResponsavelRef = new javax.swing.JLabel();
        lblFrequenciaRef = new javax.swing.JLabel();
        lblDescricaoRef = new javax.swing.JLabel();
        fldNomeRef = new pv.jfcx.JPVEdit();
        fldResponsavelRef = new pv.jfcx.JPVEdit();
        btnResponsavelRef = new javax.swing.JButton();
        listaMetas = new bsc.beans.JBIListPanel();
        listaAvaliacoes = new bsc.beans.JBIListPanel();
        valoresList = new bsc.beans.JBIListPanel();
        listaFontes = new bsc.beans.JBIListPanel();
        listaDocumento = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscFcsFrame_00012")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 22));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscIndicadorFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscIndicadorFrame_00006")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscIndicadorFrame_00007")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscIndicadorFrame_00008")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscIndicadorFrame_00009")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setMinimumSize(new java.awt.Dimension(390, 60));
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(465, 236));

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscIndicadorFrame_00010")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 64, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtDescricao.setEnabled(false);
        jScrollPane3.setViewportView(txtDescricao);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(10, 70, 400, 55);

        lblUnidade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUnidade.setText(bundle.getString("BscIndicadorFrame_00011")); // NOI18N
        lblUnidade.setEnabled(false);
        pnlData.add(lblUnidade);
        lblUnidade.setBounds(10, 126, 72, 14);

        cbbUnidade.setEditable(true);
        cbbUnidade.setEnabled(false);
        pnlData.add(cbbUnidade);
        cbbUnidade.setBounds(10, 140, 180, 22);

        lblCasasDecimais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCasasDecimais.setText(bundle.getString("BscIndicadorFrame_00012")); // NOI18N
        lblCasasDecimais.setEnabled(false);
        pnlData.add(lblCasasDecimais);
        lblCasasDecimais.setBounds(230, 126, 66, 14);

        spnCasasDecimais.setEnabled(false);
        pnlData.add(spnCasasDecimais);
        spnCasasDecimais.setBounds(230, 140, 180, 22);

        cbbFrequencia.setEnabled(false);
        cbbFrequencia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFrequenciaItemStateChanged(evt);
            }
        });
        cbbFrequencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbFrequenciaActionPerformed(evt);
            }
        });
        pnlData.add(cbbFrequencia);
        cbbFrequencia.setBounds(10, 180, 180, 22);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 260, 76, 20);

        lblFrequencia.setForeground(new java.awt.Color(51, 51, 255));
        lblFrequencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequencia.setText(bundle.getString("BscIndicadorFrame_00014")); // NOI18N
        lblFrequencia.setEnabled(false);
        pnlData.add(lblFrequencia);
        lblFrequencia.setBounds(10, 160, 76, 20);

        pnlOrientacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("BscIndicadorFrame_00030"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        pnlOrientacao.setEnabled(false);
        pnlOrientacao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        pnlOrientacao.setLayout(null);

        bgpOrder.add(rdbAscendente);
        rdbAscendente.setText(bundle.getString("BscIndicadorFrame_00015")); // NOI18N
        rdbAscendente.setEnabled(false);
        pnlOrientacao.add(rdbAscendente);
        rdbAscendente.setBounds(10, 20, 100, 23);

        bgpOrder.add(rdbDescendente);
        rdbDescendente.setText(bundle.getString("BscIndicadorFrame_00016")); // NOI18N
        rdbDescendente.setEnabled(false);
        pnlOrientacao.add(rdbDescendente);
        rdbDescendente.setBounds(110, 20, 112, 20);

        pnlData.add(pnlOrientacao);
        pnlOrientacao.setBounds(10, 210, 400, 50);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscIndicadorFrame_00017")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 76, 20);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        chkCumulativo.setText(bundle.getString("BscIndicadorFrame_00029")); // NOI18N
        chkCumulativo.setActionCommand("Que");
        chkCumulativo.setEnabled(false);
        chkCumulativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCumulativoActionPerformed(evt);
            }
        });
        pnlData.add(chkCumulativo);
        chkCumulativo.setBounds(10, 310, 80, 20);

        cbbFCumula.setEnabled(false);
        cbbFCumula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbFCumulaActionPerformed(evt);
            }
        });
        pnlData.add(cbbFCumula);
        cbbFCumula.setBounds(10, 340, 180, 22);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(10, 280, 400, 22);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(420, 280, 30, 22);

        cbbTCumula.setEnabled(false);
        pnlData.add(cbbTCumula);
        cbbTCumula.setBounds(10, 370, 180, 22);

        scpRecord.setViewportView(pnlData);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00001"), scpRecord); // NOI18N

        scpRecord1.setBorder(null);
        scpRecord1.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData1.setPreferredSize(new java.awt.Dimension(460, 200));
        pnlData1.setLayout(null);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane4.setEnabled(false);
        jScrollPane4.setViewportView(txtMetrica);

        pnlData1.add(jScrollPane4);
        jScrollPane4.setBounds(10, 80, 400, 60);

        lblResponsavelColeta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavelColeta.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavelColeta.setEnabled(false);
        pnlData1.add(lblResponsavelColeta);
        lblResponsavelColeta.setBounds(10, 20, 120, 20);

        lblMetrica.setText(bundle.getString("BscIndicadorFrame_00023")); // NOI18N
        lblMetrica.setEnabled(false);
        pnlData1.add(lblMetrica);
        lblMetrica.setBounds(10, 60, 123, 18);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane5.setEnabled(false);
        jScrollPane5.setViewportView(txtForma);

        pnlData1.add(jScrollPane5);
        jScrollPane5.setBounds(10, 160, 400, 60);

        lblForma.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblForma.setText(bundle.getString("BscIndicadorFrame_00024")); // NOI18N
        lblForma.setEnabled(false);
        pnlData1.add(lblForma);
        lblForma.setBounds(10, 140, 123, 20);

        fldResponsavelColeta.setEnabled(false);
        fldResponsavelColeta.setMaxLength(60);
        fldResponsavelColeta.setSelectAllOnDoubleClick(true);
        pnlData1.add(fldResponsavelColeta);
        fldResponsavelColeta.setBounds(10, 40, 400, 22);

        btnResponsavelColeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavelColeta.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavelColeta.setEnabled(false);
        btnResponsavelColeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelColetaActionPerformed(evt);
            }
        });
        pnlData1.add(btnResponsavelColeta);
        btnResponsavelColeta.setBounds(420, 40, 30, 22);

        scpRecord1.setViewportView(pnlData1);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00022"), scpRecord1); // NOI18N

        scpRecord2.setBorder(null);
        scpRecord2.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData2.setPreferredSize(new java.awt.Dimension(460, 200));
        pnlData2.setLayout(null);

        lblNomeRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNomeRef.setText(bundle.getString("BscIndicadorFrame_00026")); // NOI18N
        lblNomeRef.setEnabled(false);
        lblNomeRef.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNomeRef.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNomeRef.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData2.add(lblNomeRef);
        lblNomeRef.setBounds(10, 10, 80, 20);

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtDescricaoRef.setEnabled(false);
        jScrollPane6.setViewportView(txtDescricaoRef);

        pnlData2.add(jScrollPane6);
        jScrollPane6.setBounds(10, 70, 400, 60);

        lblUnidadeRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUnidadeRef.setText(bundle.getString("BscIndicadorFrame_00011")); // NOI18N
        lblUnidadeRef.setEnabled(false);
        pnlData2.add(lblUnidadeRef);
        lblUnidadeRef.setBounds(10, 130, 72, 20);

        cbbUnidadeRef.setEditable(true);
        cbbUnidadeRef.setEnabled(false);
        pnlData2.add(cbbUnidadeRef);
        cbbUnidadeRef.setBounds(10, 150, 180, 22);

        lblCasasDecimaisRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCasasDecimaisRef.setText(bundle.getString("BscIndicadorFrame_00012")); // NOI18N
        lblCasasDecimaisRef.setEnabled(false);
        pnlData2.add(lblCasasDecimaisRef);
        lblCasasDecimaisRef.setBounds(230, 134, 63, 10);

        spnCasasDecimaisRef.setEnabled(false);
        pnlData2.add(spnCasasDecimaisRef);
        spnCasasDecimaisRef.setBounds(230, 150, 180, 22);

        cbbFrequenciaRef.setEnabled(false);
        pnlData2.add(cbbFrequenciaRef);
        cbbFrequenciaRef.setBounds(10, 190, 180, 22);

        lblResponsavelRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavelRef.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavelRef.setEnabled(false);
        pnlData2.add(lblResponsavelRef);
        lblResponsavelRef.setBounds(10, 210, 76, 20);

        lblFrequenciaRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequenciaRef.setText(bundle.getString("BscIndicadorFrame_00014")); // NOI18N
        lblFrequenciaRef.setEnabled(false);
        pnlData2.add(lblFrequenciaRef);
        lblFrequenciaRef.setBounds(10, 170, 76, 20);

        lblDescricaoRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricaoRef.setText(bundle.getString("BscIndicadorFrame_00017")); // NOI18N
        lblDescricaoRef.setEnabled(false);
        pnlData2.add(lblDescricaoRef);
        lblDescricaoRef.setBounds(10, 50, 76, 20);

        fldNomeRef.setEnabled(false);
        fldNomeRef.setMaxLength(60);
        pnlData2.add(fldNomeRef);
        fldNomeRef.setBounds(10, 30, 400, 22);

        fldResponsavelRef.setEnabled(false);
        fldResponsavelRef.setMaxLength(60);
        fldResponsavelRef.setSelectAllOnDoubleClick(true);
        pnlData2.add(fldResponsavelRef);
        fldResponsavelRef.setBounds(10, 230, 400, 22);

        btnResponsavelRef.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavelRef.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavelRef.setEnabled(false);
        btnResponsavelRef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelRefActionPerformed(evt);
            }
        });
        pnlData2.add(btnResponsavelRef);
        btnResponsavelRef.setBounds(420, 230, 30, 22);

        scpRecord2.setViewportView(pnlData2);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00025"), scpRecord2); // NOI18N

        pnlFields.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00001"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00019"), listaMetas); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00018"), listaAvaliacoes); // NOI18N

        valoresList.setNovoEnabled(false);
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00020"), valoresList); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00021"), listaFontes); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00031"), listaDocumento); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 75));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbbFCumulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbFCumulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbbFCumulaActionPerformed

    private void btnResponsavelRefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelRefActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(refTipoPessoa);
        vector.add(refRespId);
        vector.add(fldResponsavelRef.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        refTipoPessoa = vctSelecionados.get(0).toString();
        refRespId = vctSelecionados.get(1).toString();
        fldResponsavelRef.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelRefActionPerformed

    private void btnResponsavelColetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelColetaActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(colTipoPessoa);
        vector.add(colRespId);
        vector.add(fldResponsavelColeta.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        colTipoPessoa = vctSelecionados.get(0).toString();
        colRespId = vctSelecionados.get(1).toString();
        fldResponsavelColeta.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelColetaActionPerformed

    private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(tipoPessoa);
        vector.add(respId);
        vector.add(fldResponsavel.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        tipoPessoa = vctSelecionados.get(0).toString();
        respId = vctSelecionados.get(1).toString();
        fldResponsavel.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelActionPerformed

	private void cbbFrequenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbFrequenciaActionPerformed
            // TODO add your handling code here:
	}//GEN-LAST:event_cbbFrequenciaActionPerformed

	private void cbbFrequenciaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFrequenciaItemStateChanged
            if (getStatus() == BscDefaultFrameBehavior.INSERTING || getStatus() == BscDefaultFrameBehavior.UPDATING) {
                cbbFrequenciaRef.setSelectedIndex(cbbFrequencia.getSelectedIndex());
                event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FCUMULA",
                        htbFCumula, cbbFCumula, getIgnoreIDList());
            }
	}//GEN-LAST:event_cbbFrequenciaItemStateChanged

	private void chkCumulativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkCumulativoActionPerformed
            // TODO add your handling code here:
	}//GEN-LAST:event_chkCumulativoActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("MEDIDAS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            if (getStatus() != bsc.swing.BscDefaultFrameBehavior.INSERTING) // Frequencia de Avalia��o de Resultado
            {
                if (!event.getComboValue(htbFrequencia, cbbFrequencia).equals(record.getString("FREQ"))) {
                    if (javax.swing.JOptionPane.showConfirmDialog(this,
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00003") +
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00004"),
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00005"), javax.swing.JOptionPane.YES_NO_OPTION) != javax.swing.JOptionPane.YES_OPTION) {
                        return;
                    }
                }
            }
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpOrder;
    private javax.swing.ButtonGroup bgpOrderRef;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnResponsavelColeta;
    private javax.swing.JButton btnResponsavelRef;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbFCumula;
    private javax.swing.JComboBox cbbFrequencia;
    private javax.swing.JComboBox cbbFrequenciaRef;
    private javax.swing.JComboBox cbbTCumula;
    private javax.swing.JComboBox cbbUnidade;
    private javax.swing.JComboBox cbbUnidadeRef;
    private javax.swing.JCheckBox chkCumulativo;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldNomeRef;
    private pv.jfcx.JPVEdit fldResponsavel;
    private pv.jfcx.JPVEdit fldResponsavelColeta;
    private pv.jfcx.JPVEdit fldResponsavelRef;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblCasasDecimais;
    private javax.swing.JLabel lblCasasDecimaisRef;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblDescricaoRef;
    private javax.swing.JLabel lblForma;
    private javax.swing.JLabel lblFrequencia;
    private javax.swing.JLabel lblFrequenciaRef;
    private javax.swing.JLabel lblMetrica;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeRef;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblResponsavelColeta;
    private javax.swing.JLabel lblResponsavelRef;
    private javax.swing.JLabel lblUnidade;
    private javax.swing.JLabel lblUnidadeRef;
    private bsc.beans.JBIListPanel listaAvaliacoes;
    private bsc.beans.JBIListPanel listaDocumento;
    private bsc.beans.JBIListPanel listaFontes;
    private bsc.beans.JBIListPanel listaMetas;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlData1;
    private javax.swing.JPanel pnlData2;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlOrientacao;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton rdbAscendente;
    private javax.swing.JRadioButton rdbDescendente;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scpRecord1;
    private javax.swing.JScrollPane scpRecord2;
    private javax.swing.JSpinner spnCasasDecimais;
    private javax.swing.JSpinner spnCasasDecimaisRef;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    private bsc.beans.JBITextArea txtDescricaoRef;
    private bsc.beans.JBITextArea txtForma;
    private bsc.beans.JBITextArea txtMetrica;
    private bsc.beans.JBIListPanel valoresList;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscFcsIndicadorFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public java.util.Hashtable getIgnoreIDList() {
        java.util.Hashtable htbIgnoreIDList = new java.util.Hashtable();

        if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_ANUAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_MENSAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        }

        return htbIgnoreIDList;
    }

    @Override
    public void afterExecute() {
        listaMetas.refresh();
    }

    @Override
    public void afterUpdate() {
    }

    @Override
    public void afterInsert() {
    }

    @Override
    public void afterDelete() {
    }
}
