package bsc.swing.framework;

import java.util.*;
import bsc.core.*;

public class BscFcsMetaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("FCSMETA");

	String strDataDe  = new String(),
			strDataAte = new String();
	String tipoPessoa = "";
	String respId = "";

	Vector vctFiltroValor = new Vector();//Contem os valores que ser�o mostrados nas combox de filtro

	public void enableFields() {
		// Habilita os campos do formul�rio.		
		lblNome.setEnabled(true);
		fldNome.setEnabled(true);
		lblDescricao.setEnabled(true);
		txtDescricao.setEnabled(true);
		chkMetaParcelada.setEnabled(true);
		fldVermelho.setEnabled(true);
		fldAzul1.setEnabled(true);
		fldVerde.setEnabled(true);
		fldAmarelo.setEnabled(true);
		lblAvaliacao.setEnabled(true);
		txtAvaliacao.setEnabled(true);
		lblResponsavel.setEnabled(true);
		btnResponsavel.setEnabled(true);
		
		if(chkMetaParcelada.isSelected()){
			lblPeriodo.setEnabled(true);
			cbbPeriodo.setEnabled(true);
			lblPeriodo.setVisible(true);
			cbbPeriodo.setVisible(true);
			lblDataAlvo.setVisible(false);
			fldDataAlvo.setVisible(false);
		} else {
			lblDataAlvo.setEnabled(true);
			fldDataAlvo.setEnabled(true);
			lblPeriodo.setVisible(false);
			cbbPeriodo.setVisible(false);
			lblDataAlvo.setVisible(true);
			fldDataAlvo.setVisible(true);
		}
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblNome.setEnabled(false);
		fldNome.setEnabled(false);
		lblDescricao.setEnabled(false);
		txtDescricao.setEnabled(false);
		chkMetaParcelada.setEnabled(false);
		fldVermelho.setEnabled(false);
		fldAzul1.setEnabled(false);
		fldVerde.setEnabled(false);
		fldAmarelo.setEnabled(false);
		lblAvaliacao.setEnabled(false);
		txtAvaliacao.setEnabled(false);
		lblResponsavel.setEnabled(false);
		btnResponsavel.setEnabled(false);
		if(chkMetaParcelada.isSelected()){
			lblPeriodo.setEnabled(false);
			cbbPeriodo.setEnabled(false);
			lblPeriodo.setVisible(true);
			cbbPeriodo.setVisible(true);
			lblDataAlvo.setVisible(false);
			fldDataAlvo.setVisible(false);
		} else {
			lblDataAlvo.setEnabled(false);
			fldDataAlvo.setEnabled(false);
			lblPeriodo.setVisible(false);
			cbbPeriodo.setVisible(false);
			lblDataAlvo.setVisible(true);
			fldDataAlvo.setVisible(true);
		}
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		// Ex.:
		// strategyList.setDataSource(
		//					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );

		strDataDe   = record.getString("DATAINI");
		strDataAte  = record.getString("DATAFIN");
		
		fldNome.setText(record.getString("NOME") );
		txtDescricao.setText(record.getString("DESCRICAO"));
		chkMetaParcelada.setSelected(record.getBoolean("PARCELADA"));
		fldDataAlvo.setCalendar(record.getDate("DATAALVO"));
		fldVermelho.setMaxDecimals( record.getInt("DECIMAIS") );
		fldAzul1.setMaxDecimals( record.getInt("DECIMAIS") );
		fldVerde.setMaxDecimals( record.getInt("DECIMAIS") );
		fldAmarelo.setMaxDecimals( record.getInt("DECIMAIS") );
		fldVermelho.setDouble(record.getDouble("VERMELHO"));
		fldAzul1.setDouble(record.getDouble("AZUL1"));
		fldVerde.setDouble(record.getDouble("VERDE"));
		fldAmarelo.setDouble(record.getDouble("AMARELO"));
		txtAvaliacao.setText(record.getString("AVALMEMO"));
		fldResponsavel.setText(record.getString("RESPONSAVEL"));
		respId = record.getString("RESPID");
		tipoPessoa = record.getString("TIPOPESSOA");
		if ( record.getBoolean("ASCEND") ) {
			lblSetaParaCima.setVisible(true);
			lblMaiorValorSuperior.setVisible(true);
			lblMenorValorInferior.setVisible(true);
			lblSetaParaBaixo.setVisible(false);
			lblMenorValorSuperior.setVisible(false);
			lblMaiorValorInferior.setVisible(false);
		}
		else {
			lblSetaParaCima.setVisible(false);
			lblMaiorValorSuperior.setVisible(false);
			lblMenorValorInferior.setVisible(false);
			lblSetaParaBaixo.setVisible(true);
			lblMenorValorSuperior.setVisible(true);
			lblMaiorValorInferior.setVisible(true);
		}

		if(record.getBoolean("PARCELADA")) {
			lblPeriodo.setVisible(true);
			cbbPeriodo.setVisible(true);
			lblDataAlvo.setVisible(false);
			fldDataAlvo.setVisible(false);
		} else {
			lblPeriodo.setVisible(false);
			cbbPeriodo.setVisible(false);
			lblDataAlvo.setVisible(true);
			fldDataAlvo.setVisible(true);
		}

		setCbbFiltro();
		
		cbbPeriodo.setSelectedIndex(record.getInt("ITEM"));

		setPeriodo();

		bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");
		
		pnlHeader.setComponents(vctContexto);

	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.

		recordAux.set( "DECIMAIS", record.getString("DECIMAIS") );
		recordAux.set( "NOME", fldNome.getText() );
		recordAux.set( "DESCRICAO", txtDescricao.getText() );
		recordAux.set( "PARCELADA", chkMetaParcelada.isSelected() );
			
		if(chkMetaParcelada.isSelected()){
			recordAux.set( "DATAALVO", fldDataAlvo.getCalendar());
		} else
			recordAux.set( "DATAALVO", fldDataAlvo.getCalendar());
		
		recordAux.set( "VERMELHO", fldVermelho.getDouble() );
		recordAux.set( "AZUL1", fldAzul1.getDouble() );
		recordAux.set( "VERDE", fldVerde.getDouble() );
		recordAux.set( "AMARELO", fldAmarelo.getDouble() );
		recordAux.set( "AVALMEMO", txtAvaliacao.getText() );
		recordAux.set( "ASCEND", record.getString("ASCEND") );
		recordAux.set( "ITEM", cbbPeriodo.getSelectedIndex());
		recordAux.set( "RESPID", respId );
		recordAux.set( "TIPOPESSOA", tipoPessoa);
		
		return recordAux;
	}

	public boolean validateFields(StringBuffer errorMessage) {
		boolean validated = false;
		double nDouble = 0;
		
		if ( (fldVermelho.getDouble() != nDouble) ||
		     (fldAmarelo.getDouble() != nDouble) || 
			 (fldVerde.getDouble() != nDouble) ||
			 (fldAzul1.getDouble() != nDouble) )
		
			if ( record.getBoolean("ASCEND") ) {
				if ( ( fldVermelho.getDouble() < fldAmarelo.getDouble() ) &&
					 ( fldAmarelo.getDouble() < fldVerde.getDouble() ) &&
					 ( fldVerde.getDouble() < fldAzul1.getDouble() ) )
					validated = true;
				else {
					errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMetaFrame_00001"));
				}
			}
			else {
				if ( ( fldVermelho.getDouble() > fldAmarelo.getDouble() ) &&
					 ( fldAmarelo.getDouble() > fldVerde.getDouble() ) &&
					 ( fldVerde.getDouble() > fldAzul1.getDouble() ) )
					validated = true;
				else {
					errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMetaFrame_00002"));
				}
			}
		else {
			validated = true;
		}
		
		return validated;
	}
	
	public boolean hasCombos() {
		return true;
	}

	/*****************************************************************************/
	/*****************************************************************************/

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblDataAlvo = new javax.swing.JLabel();
        lblResponsavel = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        fldDataAlvo = new pv.jfcx.JPVDatePlus();
        chkMetaParcelada = new javax.swing.JCheckBox();
        lblPeriodo = new javax.swing.JLabel();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        cbbPeriodo = new javax.swing.JComboBox();
        scpRecord1 = new javax.swing.JScrollPane();
        pnlData1 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        fldVermelho = new pv.jfcx.JPVNumeric();
        jPanel11 = new javax.swing.JPanel();
        fldAzul1 = new pv.jfcx.JPVNumeric();
        jPanel12 = new javax.swing.JPanel();
        fldVerde = new pv.jfcx.JPVNumeric();
        jPanel13 = new javax.swing.JPanel();
        fldAmarelo = new pv.jfcx.JPVNumeric();
        lblSetaParaCima = new javax.swing.JLabel();
        lblSetaParaBaixo = new javax.swing.JLabel();
        lblMaiorValorSuperior = new javax.swing.JLabel();
        lblMenorValorSuperior = new javax.swing.JLabel();
        lblMaiorValorInferior = new javax.swing.JLabel();
        lblMenorValorInferior = new javax.swing.JLabel();
        lbMeta = new javax.swing.JLabel();
        scpRecord2 = new javax.swing.JScrollPane();
        pnlData2 = new javax.swing.JPanel();
        lblAvaliacao = new javax.swing.JLabel();
        jScrollPane32 = new javax.swing.JScrollPane();
        txtAvaliacao = new bsc.beans.JBITextArea();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscMetaFrame_00003")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_meta.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 22));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscMetaFrame_00004")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.setRequestFocusEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscMetaFrame_00005")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.setRequestFocusEnabled(false);
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscMetaFrame_00006")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.setRequestFocusEnabled(false);
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscMetaFrame_00007")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.setRequestFocusEnabled(false);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscMetaFrame_00008")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.setRequestFocusEnabled(false);
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.setRequestFocusEnabled(false);
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(470, 203));

        pnlData.setPreferredSize(new java.awt.Dimension(471, 200));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscMetaFrame_00009")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 84, 20);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscMetaFrame_00010")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 87, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtDescricao.setColumns(32);
        jScrollPane3.setViewportView(txtDescricao);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(10, 70, 400, 55);

        lblDataAlvo.setForeground(new java.awt.Color(51, 51, 255));
        lblDataAlvo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataAlvo.setText(bundle.getString("BscMetaFrame_00011")); // NOI18N
        lblDataAlvo.setEnabled(false);
        pnlData.add(lblDataAlvo);
        lblDataAlvo.setBounds(10, 150, 84, 20);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscMetaFrame_00012")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 190, 87, 20);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        fldDataAlvo.setDialog(true);
        fldDataAlvo.setUseLocale(true);
        fldDataAlvo.setWaitForCalendarDate(true);
        pnlData.add(fldDataAlvo);
        fldDataAlvo.setBounds(10, 170, 180, 22);

        chkMetaParcelada.setText(bundle.getString("BscMetaFrame_00021")); // NOI18N
        chkMetaParcelada.setActionCommand("Que");
        chkMetaParcelada.setEnabled(false);
        chkMetaParcelada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMetaParceladaActionPerformed(evt);
            }
        });
        pnlData.add(chkMetaParcelada);
        chkMetaParcelada.setBounds(10, 130, 170, 22);

        lblPeriodo.setForeground(new java.awt.Color(51, 51, 255));
        lblPeriodo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPeriodo.setText(bundle.getString("BscMetaFrame_00022")); // NOI18N
        lblPeriodo.setEnabled(false);
        pnlData.add(lblPeriodo);
        lblPeriodo.setBounds(10, 150, 87, 20);
        lblPeriodo.getAccessibleContext().setAccessibleName(bundle.getString("BscMetaFrame_00022")); // NOI18N

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(10, 210, 400, 22);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(420, 210, 30, 22);

        cbbPeriodo.setEnabled(false);
        cbbPeriodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbPeriodoActionPerformed(evt);
            }
        });
        pnlData.add(cbbPeriodo);
        cbbPeriodo.setBounds(10, 170, 180, 22);

        scpRecord.setViewportView(pnlData);

        jTabbedPane1.addTab(bundle.getString("BscMetaFrame_00023"), scpRecord); // NOI18N

        scpRecord1.setBorder(null);

        pnlData1.setPreferredSize(new java.awt.Dimension(471, 200));
        pnlData1.setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 51, 0));
        jPanel1.setLayout(null);

        fldVermelho.setAlignment(2);
        fldVermelho.setBorderStyle(2);
        fldVermelho.setDecimalSeparator(",");
        fldVermelho.setUseLocale(true);
        jPanel1.add(fldVermelho);
        fldVermelho.setBounds(0, 16, 220, 20);

        pnlData1.add(jPanel1);
        jPanel1.setBounds(10, 140, 270, 40);

        jPanel11.setBackground(new java.awt.Color(51, 51, 255));
        jPanel11.setLayout(null);

        fldAzul1.set1000Multiplier(".");
        fldAzul1.setAlignment(2);
        fldAzul1.setBorderStyle(2);
        fldAzul1.setDecimalSeparator(",");
        fldAzul1.setUseLocale(true);
        jPanel11.add(fldAzul1);
        fldAzul1.setBounds(0, 16, 220, 20);

        pnlData1.add(jPanel11);
        jPanel11.setBounds(10, 30, 270, 40);

        jPanel12.setBackground(new java.awt.Color(0, 204, 153));
        jPanel12.setLayout(null);

        fldVerde.setAlignment(2);
        fldVerde.setBorderStyle(2);
        fldVerde.setDecimalSeparator(",");
        fldVerde.setUseLocale(true);
        jPanel12.add(fldVerde);
        fldVerde.setBounds(0, 16, 220, 20);

        pnlData1.add(jPanel12);
        jPanel12.setBounds(10, 70, 270, 40);

        jPanel13.setBackground(new java.awt.Color(255, 255, 0));
        jPanel13.setLayout(null);

        fldAmarelo.setAlignment(2);
        fldAmarelo.setBorderStyle(2);
        fldAmarelo.setDecimalSeparator(",");
        fldAmarelo.setUseLocale(true);
        jPanel13.add(fldAmarelo);
        fldAmarelo.setBounds(0, 16, 220, 20);

        pnlData1.add(jPanel13);
        jPanel13.setBounds(10, 100, 270, 40);

        lblSetaParaCima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_setaparacima.gif"))); // NOI18N
        pnlData1.add(lblSetaParaCima);
        lblSetaParaCima.setBounds(330, 70, 27, 68);

        lblSetaParaBaixo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_setaparabaixo.gif"))); // NOI18N
        pnlData1.add(lblSetaParaBaixo);
        lblSetaParaBaixo.setBounds(330, 70, 27, 68);

        lblMaiorValorSuperior.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMaiorValorSuperior.setText(bundle.getString("BscMetaFrame_00013")); // NOI18N
        pnlData1.add(lblMaiorValorSuperior);
        lblMaiorValorSuperior.setBounds(290, 50, 104, 14);

        lblMenorValorSuperior.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMenorValorSuperior.setText(bundle.getString("BscMetaFrame_00014")); // NOI18N
        pnlData1.add(lblMenorValorSuperior);
        lblMenorValorSuperior.setBounds(290, 50, 104, 14);

        lblMaiorValorInferior.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMaiorValorInferior.setText(bundle.getString("BscMetaFrame_00015")); // NOI18N
        pnlData1.add(lblMaiorValorInferior);
        lblMaiorValorInferior.setBounds(290, 150, 104, 14);

        lblMenorValorInferior.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMenorValorInferior.setText(bundle.getString("BscMetaFrame_00016")); // NOI18N
        pnlData1.add(lblMenorValorInferior);
        lblMenorValorInferior.setBounds(290, 150, 104, 14);

        lbMeta.setFont(new java.awt.Font("MS Sans Serif", 1, 11)); // NOI18N
        lbMeta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbMeta.setText(bundle.getString("BscMetaFrame_00009")); // NOI18N
        pnlData1.add(lbMeta);
        lbMeta.setBounds(10, 10, 30, 18);

        scpRecord1.setViewportView(pnlData1);

        jTabbedPane1.addTab(bundle.getString("BscMetaFrame_00018"), scpRecord1); // NOI18N

        scpRecord2.setBorder(null);

        pnlData2.setPreferredSize(new java.awt.Dimension(471, 200));
        pnlData2.setLayout(null);

        lblAvaliacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAvaliacao.setText(bundle.getString("BscMetaFrame_00017")); // NOI18N
        lblAvaliacao.setEnabled(false);
        pnlData2.add(lblAvaliacao);
        lblAvaliacao.setBounds(10, 10, 72, 20);
        lblAvaliacao.getAccessibleContext().setAccessibleName(bundle.getString("BscMetaFrame_00019")); // NOI18N

        jScrollPane32.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane32.setViewportView(txtAvaliacao);

        pnlData2.add(jScrollPane32);
        jScrollPane32.setBounds(10, 30, 362, 160);

        scpRecord2.setViewportView(pnlData2);

        jTabbedPane1.addTab(bundle.getString("BscMetaFrame_00020"), scpRecord2); // NOI18N

        pnlFields.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscMetaFrame_00003"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 90));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
	java.util.Vector vector = new java.util.Vector();
	vector.add(tipoPessoa);
	vector.add(respId);
	vector.add(fldResponsavel.getText());
	String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size()-1).getString("NOME");

	java.util.Vector vctResponsavel = new java.util.Vector();
	vctResponsavel.add(vector);
	bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog( this, event, organizacao, vctResponsavel);
	dialog.show();

	java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
	tipoPessoa = vctSelecionados.get(0).toString();
	respId = vctSelecionados.get(1).toString();
	fldResponsavel.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelActionPerformed

	private void cbbPeriodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbPeriodoActionPerformed
		setPeriodo();
	}//GEN-LAST:event_cbbPeriodoActionPerformed

	private void chkMetaParceladaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMetaParceladaActionPerformed
		if(chkMetaParcelada.isSelected()){
			lblPeriodo.setEnabled(true);
			cbbPeriodo.setEnabled(true);
			lblPeriodo.setVisible(true);
			cbbPeriodo.setVisible(true);
			lblDataAlvo.setVisible(false);
			fldDataAlvo.setVisible(false);
			cbbPeriodoActionPerformed(evt);
		} else {
			lblPeriodo.setVisible(false);
			cbbPeriodo.setVisible(false);
			lblDataAlvo.setEnabled(true);
			fldDataAlvo.setEnabled(true);
			lblDataAlvo.setVisible(true);
			fldDataAlvo.setVisible(true);
		}
	}//GEN-LAST:event_chkMetaParceladaActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("META_ALVO");
	}//GEN-LAST:event_btnReload1ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		event.saveRecord( );
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed
	
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbPeriodo;
    private javax.swing.JCheckBox chkMetaParcelada;
    private pv.jfcx.JPVNumeric fldAmarelo;
    private pv.jfcx.JPVNumeric fldAzul1;
    private pv.jfcx.JPVDatePlus fldDataAlvo;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldResponsavel;
    private pv.jfcx.JPVNumeric fldVerde;
    private pv.jfcx.JPVNumeric fldVermelho;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane32;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbMeta;
    private javax.swing.JLabel lblAvaliacao;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblMaiorValorInferior;
    private javax.swing.JLabel lblMaiorValorSuperior;
    private javax.swing.JLabel lblMenorValorInferior;
    private javax.swing.JLabel lblMenorValorSuperior;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPeriodo;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblSetaParaBaixo;
    private javax.swing.JLabel lblSetaParaCima;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlData1;
    private javax.swing.JPanel pnlData2;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scpRecord1;
    private javax.swing.JScrollPane scpRecord2;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtAvaliacao;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscFcsMetaFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		fldAzul1.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
		fldAmarelo.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
		fldVermelho.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
		fldVerde.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
		
		event.defaultConstructor( operation, idAux, contextId );
	}

    private void setCbbFiltro(){
	String	strTexto    = new String(""),
		strMes	    = new String("");
	
	Vector retData = new Vector();
	
	int nAno,
	    nMes,
	    nDia;
	
	//Data de
	nAno = java.lang.Integer.parseInt(strDataDe.substring(6,10));
	nMes = java.lang.Integer.parseInt(strDataDe.substring(3,5))-1;
	nDia = java.lang.Integer.parseInt(strDataDe.substring(0,2));
	java.util.GregorianCalendar  dIni = new java.util.GregorianCalendar(nAno,nMes,nDia);
	
	//Data ate
	nAno = java.lang.Integer.parseInt(strDataAte.substring(6,10));
	nMes = java.lang.Integer.parseInt(strDataAte.substring(3,5)) -1;
	nDia = java.lang.Integer.parseInt(strDataAte.substring(0,2));
	java.util.GregorianCalendar  dFin = new java.util.GregorianCalendar(nAno,nMes,nDia);

	Integer intDia, intDiaAnt = new Integer(0),
	intMes, intMesAnt = new Integer(0),
	intAno, intAnoAnt = new Integer(0);
	
	cbbPeriodo.removeAllItems();
	vctFiltroValor.removeAllElements();
	
	for(;dFin.after(dIni);dIni.add(dIni.DATE,1)){
	    retData  = DateConv(dIni, record.getInt("FREQ"));
	    intDia   = (Integer)retData.get(0);
	    intMes   = (Integer)retData.get(1);
	    intAno   = (Integer)retData.get(2);

		if(intDia.intValue() != intDiaAnt.intValue() || intMes.intValue() != intMesAnt.intValue() || intAno.intValue()!= intAnoAnt.intValue()){
			intDiaAnt = intDia;
			intMesAnt = intMes;
			intAnoAnt = intAno;

			strTexto = setEixoX(intDia.intValue(), intMes.intValue(), intAno.intValue(),record.getInt("FREQ"),false);
		
	
			/*Como o mes em Java come�a com 0, quando o indicador mensal fizer referencia ao m�s.
			 *� necessaria a corre��o do valor adicionando 1 v�riavel strMes.
			 */
			if(record.getInt("FREQ") == 8 || record.getInt("FREQ") == 6 || record.getInt("FREQ") == 5){
				//strMes = new String().valueOf(intMes.intValue() + 1);
				strMes = new String().valueOf(intMes.intValue());
			}else{
				strMes = new String().valueOf(intMes.intValue());
			}
		
			vctFiltroValor.add(new String[] {strTexto, intAno.toString()+"-" + strMes + "-"+intDia.toString()});
	    }
	}
	for( int iItem = 0; iItem < vctFiltroValor.size(); iItem++){
	    String[] aValores = (java.lang.String[])vctFiltroValor.get(iItem);
	    cbbPeriodo.addItem(new String(aValores[0]));
	}
    }

    private String setEixoX(int intDia, int intMes, int intAno, int iIntervalo, boolean blnMeta){
	
	String strRetorno = new String("");
	java.text.DateFormatSymbols dateFormat = new java.text.DateFormatSymbols(bsc.core.BscStaticReferences.getBscDefaultLocale());
	java.text.DateFormat dateFormatter;
	Calendar calData = Calendar.getInstance();	
	
	String[] meses    = dateFormat.getMonths();
	
	if(blnMeta){
	    iIntervalo = 99;
	}
	
	switch (iIntervalo){
	    case BscStaticReferences.BSC_FREQ_DIARIA://Dia
			calData.set(intAno,intMes,intDia);
			dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);		
			strRetorno = (String)dateFormatter.format(new java.util.Date(calData.getTimeInMillis()));
			break;
		case BscStaticReferences.BSC_FREQ_SEMANAL://Semana
			strRetorno =  intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00016") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_QUINZENAL://Quinzena
			strRetorno = intDia +java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00017")+	meses[intMes] + " " + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_MENSAL://Meses
			strRetorno = meses[intMes] + " " +java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00022") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_BIMESTRAL://Bimestre
			strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00018") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_TRIMESTRAL://Trimestre
			strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00019") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_QUADRIMESTRAL://Quadrimestre
			strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00020") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_SEMESTRAL://Semestre
			strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00021") + intAno;
			break;
		case BscStaticReferences.BSC_FREQ_ANUAL://Ano
			strRetorno = String.valueOf(intAno);
			break;
		case 99:
			calData.set(intAno,intMes,intDia);
			dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);		
			strRetorno = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00011") +": " + (String)dateFormatter.format(new java.util.Date(calData.getTimeInMillis()));
			break;
	    default:
			strRetorno = "Sem intervalo";
	}
	
	return strRetorno;
    }
	
    private Vector DateConv(java.util.GregorianCalendar dData, int nFrequencia) {
	Vector vctRetorno = new Vector(); //Vetor de Retorno
	
	int intAno = 0,
	    intMes = 0,
	    intDia = 0;
	
	if(nFrequencia == BscStaticReferences.BSC_FREQ_ANUAL){ // Anual
	    intAno =    dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_SEMESTRAL){ // Semestral
	    intMes = dData.get(dData.MONTH) > dData.JUNE ? 2 : 1;
	    intAno = dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_QUADRIMESTRAL){//Quadrimestal
	    if(dData.get(dData.MONTH) <= dData.APRIL){
		intMes = 1;
	    }else if(dData.get(dData.MONTH) <= dData.AUGUST){
		intMes = 2;
	    }else{
		intMes = 3;
	    }
	    intAno = dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_TRIMESTRAL){ // Trimestral
	    if(dData.get(dData.MONTH) <= dData.MARCH){
		intMes = 1;
	    }else if(dData.get(dData.MONTH)<= dData.JUNE){
		intMes = 2;
	    }else if (dData.get(dData.MONTH)<= dData.SEPTEMBER){
		intMes = 3;
	    }else{
		intMes = 4;
	    }
	    intAno =  dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_BIMESTRAL){ // Bimestral
	    if(dData.get(dData.MONTH) <= dData.FEBRUARY){
		intMes = 1;
	    }else if(dData.get(dData.MONTH) <= dData.APRIL){
		intMes = 2;
	    }else if (dData.get(dData.MONTH) <= dData.JUNE){
		intMes = 3;
	    }else if( dData.get(dData.MONTH) <= dData.AUGUST){
		intMes = 4;
	    }else if (dData.get(dData.MONTH) <= dData.OCTOBER){
		intMes = 5;
	    }else {
		intMes = 6;
	    }
	    intAno =  dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_MENSAL){ // Mensal
	    intMes = dData.get(dData.MONTH);
	    intAno =  dData.get(dData.YEAR);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_QUINZENAL){ // Quinzenal
	    intDia   =  dData.get(dData.DATE) > 15?2:1;
	    intAno	 =  dData.get(dData.YEAR);
	    intMes	 =  dData.get(dData.MONTH);
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_SEMANAL){ // Semanal
	    intAno =  dData.get(dData.YEAR);
	    intDia =  0;
	    
	    java.util.GregorianCalendar  dFirstDay = new java.util.GregorianCalendar(intAno,0,1);
	    
	    while (dFirstDay.get(dFirstDay.DAY_OF_WEEK_IN_MONTH) != dFirstDay.MONDAY) {
		dFirstDay.add(dFirstDay.DATE, 1);
	    }
	    
	    long lDifDias = (dData.getTimeInMillis() - dFirstDay.getTimeInMillis());
	    
	    //Subtra��o de datas, transformando em dias.
	    lDifDias = ((((lDifDias/1000)/60)/60)/24);
	    
	    if(lDifDias > 0){
		intMes =  (int)(lDifDias / 7) + 1;
	    }else{
		intMes = 1;
	    }
	    
	}else if(nFrequencia == BscStaticReferences.BSC_FREQ_DIARIA){ // Diaria
	    intAno =  dData.get(dData.YEAR);
	    intMes =  dData.get(dData.MONTH);
	    intDia =  dData.get(dData.DATE);
	}
	
	vctRetorno.addElement(new Integer(intDia));
	vctRetorno.addElement(new Integer(intMes));
	vctRetorno.addElement(new Integer(intAno));
	
	return vctRetorno;
    }
	
	
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

	private void setPeriodo(){
		if(cbbPeriodo.getItemCount()>0 && chkMetaParcelada.isSelected()) {
			
			String[] arraySt =  (String[])vctFiltroValor.get(cbbPeriodo.getSelectedIndex());
			
			int nPos1 = arraySt[1].indexOf("-");
			int nPos2 = arraySt[1].indexOf("-",(nPos1+1));
			
			int nAno = Integer.parseInt(arraySt[1].substring(0, nPos1));
			int nMes = Integer.parseInt(arraySt[1].substring(nPos1+1, nPos2));
			int nDia = Integer.parseInt(arraySt[1].substring(nPos2+1));
			
			switch (record.getInt("FREQ")){
				case BscStaticReferences.BSC_FREQ_DIARIA://Dia
					java.util.GregorianCalendar dData = new java.util.GregorianCalendar(nAno,nMes,nDia);
					fldDataAlvo.setCalendar(dData);
					break;
				case BscStaticReferences.BSC_FREQ_SEMANAL://Semana
					java.util.GregorianCalendar dDataSemanal = new java.util.GregorianCalendar(nAno,0,1);
					while(dDataSemanal.get(dDataSemanal.DAY_OF_WEEK) != dDataSemanal.MONDAY){
						dDataSemanal.add(dDataSemanal.DATE, 1);
					}
					dDataSemanal.add(dDataSemanal.DATE, (((nMes*7)-7)+6));
					fldDataAlvo.setCalendar(dDataSemanal);
					break;
				case BscStaticReferences.BSC_FREQ_QUINZENAL://Quinzena
					if(nDia == 1){
						java.util.GregorianCalendar dDataQuinzenal = new java.util.GregorianCalendar(nAno,nMes,15);
						fldDataAlvo.setCalendar(dDataQuinzenal);
					} else {
						if(nMes == 11){
							nMes = 0;
							nAno++;
						} else {
							nMes++;
						}
						java.util.GregorianCalendar dDataQuinzenal = new java.util.GregorianCalendar(nAno,nMes,1);
						dDataQuinzenal.add(dDataQuinzenal.DATE, -1);
						fldDataAlvo.setCalendar(dDataQuinzenal);
					}
					break;
				case BscStaticReferences.BSC_FREQ_MENSAL://Meses
					if(nMes > 11){
						nMes = 0;
						nAno++;
					}
					java.util.GregorianCalendar dDataMensal = new java.util.GregorianCalendar(nAno,nMes,1);
					dDataMensal.roll(dDataMensal.DATE, false);
					fldDataAlvo.setCalendar(dDataMensal);
					break;
				case BscStaticReferences.BSC_FREQ_BIMESTRAL://Bimestre
					if(nMes == 1){
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,2,01);
						dDataBimestral.add(dDataBimestral.DATE, -1);
						fldDataAlvo.setCalendar(dDataBimestral);
					}else if(nMes == 2) {
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,3,30);
						fldDataAlvo.setCalendar(dDataBimestral);
					}else if(nMes == 3) {
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,5,30);
						fldDataAlvo.setCalendar(dDataBimestral);
					}else if(nMes == 4) {
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,7,31);
						fldDataAlvo.setCalendar(dDataBimestral);
					}else if(nMes == 5) {
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,9,31);
						fldDataAlvo.setCalendar(dDataBimestral);
					}else {
						java.util.GregorianCalendar dDataBimestral = new java.util.GregorianCalendar(nAno,11,31);
						fldDataAlvo.setCalendar(dDataBimestral);
					}
					break;
				case BscStaticReferences.BSC_FREQ_TRIMESTRAL://Trimestre
					if(nMes == 1){
						java.util.GregorianCalendar dDataTrimestral = new java.util.GregorianCalendar(nAno,2,31);
						fldDataAlvo.setCalendar(dDataTrimestral);
					}else if(nMes == 2) {
						java.util.GregorianCalendar dDataTrimestral = new java.util.GregorianCalendar(nAno,5,30);
						fldDataAlvo.setCalendar(dDataTrimestral);
					}else if(nMes == 3) {
						java.util.GregorianCalendar dDataTrimestral = new java.util.GregorianCalendar(nAno,8,30);
						fldDataAlvo.setCalendar(dDataTrimestral);
					}else {
						java.util.GregorianCalendar dDataTrimestral = new java.util.GregorianCalendar(nAno,11,31);
						fldDataAlvo.setCalendar(dDataTrimestral);
					}
					break;
				case BscStaticReferences.BSC_FREQ_QUADRIMESTRAL://Quadrimestre
					if(nMes == 1){
						java.util.GregorianCalendar dDataQuadrimestral = new java.util.GregorianCalendar(nAno,3,30);
						fldDataAlvo.setCalendar(dDataQuadrimestral);
					}else if(nMes == 2) {
						java.util.GregorianCalendar dDataQuadrimestral = new java.util.GregorianCalendar(nAno,7,31);
						fldDataAlvo.setCalendar(dDataQuadrimestral);
					}else {
						java.util.GregorianCalendar dDataQuadrimestral = new java.util.GregorianCalendar(nAno,11,31);
						fldDataAlvo.setCalendar(dDataQuadrimestral);
					}
					break;
				case BscStaticReferences.BSC_FREQ_SEMESTRAL://Semestre
					if(nMes == 1){
						java.util.GregorianCalendar dDataSemestral = new java.util.GregorianCalendar(nAno,5,30);
						fldDataAlvo.setCalendar(dDataSemestral);
					}else {
						java.util.GregorianCalendar dDataSemestral = new java.util.GregorianCalendar(nAno,11,31);
						fldDataAlvo.setCalendar(dDataSemestral);
					}
					break;
				case BscStaticReferences.BSC_FREQ_ANUAL://Ano
					java.util.GregorianCalendar dDataAnual = new java.util.GregorianCalendar(nAno,11,31);
					fldDataAlvo.setCalendar(dDataAnual);
					break;
			}
		}
	}
}
