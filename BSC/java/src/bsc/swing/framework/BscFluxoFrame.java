package bsc.swing.framework;


public class BscFluxoFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("FLUXO");
	private String respId = "";
	private String tipoPessoa = " ";
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblSequencia.setEnabled(true);
		fldSequencia.setEnabled(true);
		lblNome.setEnabled(true);
		txtNome.setEnabled(true);
		lblResponsavel.setEnabled(true);
		btnResponsavel.setEnabled(true);
		btnResponsavel.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblSequencia.setEnabled(false);
		fldSequencia.setEnabled(false);
		lblNome.setEnabled(false);
		txtNome.setEnabled(false);
		lblResponsavel.setEnabled(false);
		btnResponsavel.setEnabled(false);
	}
	
	java.util.Hashtable htbOrganizacao = new java.util.Hashtable();
	java.util.Hashtable htbTipoLink = new java.util.Hashtable();
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldSequencia.setValue(record.getString("SEQUENCIA") );
		txtNome.setText(record.getString("NOME") );
		String strId = record.getString("PARENTID");
		respId = record.getString("RESPID");
		tipoPessoa = record.getString("TIPOPESSOA");
		fldResponsavel.setText(record.getString("RESPONSAVEL"));

		pnlHead.setComponents(record.getBIXMLVector("CONTEXTOS"));
	}

	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );

		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		recordAux.set( "SEQUENCIA", fldSequencia.getText() );
		recordAux.set( "NOME", txtNome.getText() );
		recordAux.set( "RESPID", respId );
		recordAux.set( "TIPOPESSOA", tipoPessoa );

		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}

	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblSequencia = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        jScrollNome = new javax.swing.JScrollPane();
        txtNome = new bsc.beans.JBITextArea();
        lblResponsavel = new javax.swing.JLabel();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        fldSequencia = new pv.jfcx.JPVNumeric();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHead = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscFluxoFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_fluxo.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setOpaque(false);
        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscEstrategiaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscEstrategiaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscEstrategiaFrame_00014")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscEstrategiaFrame_00004")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscEstrategiaFrame_00005")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(471, 171));
        pnlData.setLayout(null);

        lblSequencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSequencia.setText(bundle.getString("BscFluxoFrame_00003")); // NOI18N
        lblSequencia.setEnabled(false);
        pnlData.add(lblSequencia);
        lblSequencia.setBounds(10, 10, 75, 20);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscFluxoFrame_00002")); // NOI18N
        lblNome.setEnabled(false);
        pnlData.add(lblNome);
        lblNome.setBounds(10, 50, 75, 20);

        jScrollNome.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollNome.setViewportView(txtNome);

        pnlData.add(jScrollNome);
        jScrollNome.setBounds(10, 70, 400, 60);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscFluxoFrame_00004")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 130, 80, 20);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(10, 150, 400, 20);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(420, 150, 30, 20);

        fldSequencia.setDecimalSeparator("");
        fldSequencia.setMaxDecimals(0);
        fldSequencia.setMaxIntegers(4);
        fldSequencia.setMaxLength(4);
        pnlData.add(fldSequencia);
        fldSequencia.setBounds(10, 30, 180, 20);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscFluxoFrame_00001"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 15));
        pnlTopForm.setLayout(new java.awt.BorderLayout());

        pnlHead.setMinimumSize(new java.awt.Dimension(520, 45));
        pnlTopForm.add(pnlHead, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
	java.util.Vector vector = new java.util.Vector();
	vector.add(tipoPessoa);
	vector.add(respId);
	vector.add(fldResponsavel.getText());
	String organizacao = "";

	java.util.Vector vctResponsavel = new java.util.Vector();
	vctResponsavel.add(vector);
	bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog( this, event, organizacao, vctResponsavel);
	dialog.show();

	java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
	tipoPessoa = vctSelecionados.get(0).toString();
	respId = vctSelecionados.get(1).toString();
	fldResponsavel.setText(vctSelecionados.get(2).toString());
	}//GEN-LAST:event_btnResponsavelActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("ESTRATEGIAS");
	}//GEN-LAST:event_btnReload1ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		event.saveRecord( );
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed
	
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldResponsavel;
    private pv.jfcx.JPVNumeric fldSequencia;
    private javax.swing.JScrollPane jScrollNome;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblSequencia;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHead;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtNome;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscFluxoFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}	
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

}
