package bsc.swing.framework;

import bsc.swing.BscDefaultFrameBehavior;
import bsc.xml.BIXMLRecord;
import java.awt.event.ItemEvent;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class BscIndicadorFrame extends bsc.swing.BscInternalExecute implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("INDICADOR");
    private String respId = "";
    private String tipoPessoa = " ";
    private String refRespId = "";
    private String refTipoPessoa = " ";
    private String colRespId = "";
    private String colTipoPessoa = " ";
    private bsc.xml.BIXMLRecord recDataWareHouse = null;
    private BscIndSourceQueryTable tabQuery = new BscIndSourceQueryTable();
    private java.util.Hashtable htbDataWareHouse = new java.util.Hashtable(), htbConsultas = new java.util.Hashtable(), htbUnidade = new java.util.Hashtable(), htbFrequencia = new java.util.Hashtable(), htbUnidadeRef = new java.util.Hashtable(), htbFrequenciaRef = new java.util.Hashtable(), htbFCumula = new java.util.Hashtable(), htbTCumula = new java.util.Hashtable();
    private bsc.xml.BIXMLVector backConsultas = null;
    private bsc.swing.BscDefaultDialogSystem dialogSystem = bsc.core.BscStaticReferences.getBscDialogSystem();
    // modelo para valores de PESO de indicadores 1-Valor Inicial/ 0-Valor M�nimo/ 10-Valor M�ximo/ 1-Valor de Incremento
    private SpinnerModel spmPeso = new SpinnerNumberModel(1, 0, 10, 1);

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblUnidade1.setEnabled(true);
        cbbUnidade.setEnabled(true);
        lblCasasDecimais.setEnabled(true);
        spnCasasDecimais.setEnabled(true);
        lblPeso.setEnabled(true);
        spnPeso.setEnabled(true);
        lblFrequencia.setEnabled(true);
        lblFrequenciaCumulativo.setEnabled(true);
        cbbFrequencia.setEnabled(true);
        lblResponsavel.setEnabled(true);
        rdbAscendente.setEnabled(true);
        rdbDescendente.setEnabled(true);
        lblResponsavel.setEnabled(true);
        fldResponsavel.setEnabled(true);
        btnResponsavel.setEnabled(true);
        fldResponsavelColeta.setEnabled(true);
        btnResponsavelColeta.setEnabled(true);
        fldResponsavelRef.setEnabled(true);
        btnResponsavelRef.setEnabled(true);
        chkCumulativo.setEnabled(true);
        chkTendencia.setEnabled(true);
        // coleta
        lblResponsavelColeta.setEnabled(true);
        lblMetrica.setEnabled(true);
        txtMetrica.setEnabled(true);
        lblForma.setEnabled(true);
        txtForma.setEnabled(true);
        // referencia
        lblNomeRef.setEnabled(true);
        fldNomeRef.setEnabled(true);
        lblDescricaoRef.setEnabled(true);
        txtDescricaoRef.setEnabled(true);
        lblUnidadeRef.setEnabled(true);
        cbbUnidadeRef.setEnabled(true);
        lblCasasDecimaisRef.setEnabled(true);
        spnCasasDecimaisRef.setEnabled(true);
        lblPesoRef.setEnabled(true);
        spnPesoRef.setEnabled(true);
        lblFrequenciaRef.setEnabled(true);
        lblResponsavelRef.setEnabled(true);
        cbbFCumula.setEnabled(true);
        cbbTCumula.setEnabled(true);
        listaInfluencia.setEnabled(chkTendencia.isSelected());

        lblEnderecoDW.setEnabled(true);
        lblConsulta.setEnabled(true);
        lblDatawarehouse.setEnabled(true);
        fldUrlDW.setEnabled(true);
        cbbDataWareHouse.setEnabled(true);
        cbbConsultas.setEnabled(true);
        btnAdd.setEnabled(true);
        btnRemove.setEnabled(true);
        btnPostEndereco.setEnabled(true);
        lblTipoCumulativo.setEnabled(true);
        lblFrequenciaCumulativo.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        rdbAscendente.setEnabled(false);
        rdbDescendente.setEnabled(false);
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblUnidade1.setEnabled(false);
        cbbUnidade.setEnabled(false);
        lblCasasDecimais.setEnabled(false);
        spnCasasDecimais.setEnabled(false);
        lblPeso.setEnabled(false);
        spnPeso.setEnabled(false);
        lblFrequencia.setEnabled(false);
        lblFrequenciaCumulativo.setEnabled(false);
        cbbFrequencia.setEnabled(false);
        lblResponsavel.setEnabled(false);
        fldResponsavel.setEnabled(false);
        btnResponsavel.setEnabled(false);
        fldResponsavelColeta.setEnabled(false);
        btnResponsavelColeta.setEnabled(false);
        fldResponsavelRef.setEnabled(false);
        btnResponsavelRef.setEnabled(false);
        chkCumulativo.setEnabled(false);
        chkTendencia.setEnabled(false);
        // coleta
        lblResponsavelColeta.setEnabled(false);
        lblMetrica.setEnabled(false);
        txtMetrica.setEnabled(false);
        lblForma.setEnabled(false);
        txtForma.setEnabled(false);
        // referencia
        lblNomeRef.setEnabled(false);
        fldNomeRef.setEnabled(false);
        lblDescricaoRef.setEnabled(false);
        txtDescricaoRef.setEnabled(false);
        lblUnidadeRef.setEnabled(false);
        cbbUnidadeRef.setEnabled(false);
        lblCasasDecimaisRef.setEnabled(false);
        spnCasasDecimaisRef.setEnabled(false);
        lblPesoRef.setEnabled(false);
        spnPesoRef.setEnabled(false);
        lblFrequenciaRef.setEnabled(false);
        lblResponsavelRef.setEnabled(false);
        lblResponsavelRef.setEnabled(false);
        cbbFCumula.setEnabled(false);
        cbbTCumula.setEnabled(false);
        listaInfluencia.setEnabled(false);

        lblEnderecoDW.setEnabled(false);
        lblConsulta.setEnabled(false);
        lblDatawarehouse.setEnabled(false);
        fldUrlDW.setEnabled(false);
        cbbDataWareHouse.setEnabled(false);
        cbbConsultas.setEnabled(false);
        btnAdd.setEnabled(false);
        btnRemove.setEnabled(false);
        btnPostEndereco.setEnabled(false);
        lblTipoCumulativo.setEnabled(false);
        lblFrequenciaCumulativo.setEnabled(false);
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        listaMetas.setDataSource(
                record.getBIXMLVector("METAS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaAvaliacoes.setDataSource(
                record.getBIXMLVector("AVALIACOES"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaFontes.setDataSource(
                record.getBIXMLVector("DATASRCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        valoresList.setDataSource(
                record.getBIXMLVector("PLANILHAS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaDocumento.setDataSource(
                record.getBIXMLVector("INDDOCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaInfluencia.setDataSource(
                record.getBIXMLVector("INDICADORES"),
                record.getBIXMLVector("INDTENDS"),
                "INDTENDS", "INDTEND");

        // Indicador
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        spnCasasDecimais.setValue(new Integer(record.getInt("DECIMAIS")));
        spnPeso.setValue(record.getInt("PESO"));
        chkCumulativo.setSelected(record.getBoolean("CUMULATIVO"));
        respId = record.getString("RESPID");
        tipoPessoa = record.getString("TIPOPESSOA");
        fldResponsavel.setText(record.getString("RESPONSAVEL"));
        refRespId = record.getString("RRESPID");
        refTipoPessoa = record.getString("RTIPOPES");
        fldResponsavelRef.setText(record.getString("RRESPONSAVEL"));
        colRespId = record.getString("MEDRESPID");
        colTipoPessoa = record.getString("MEDTIPOPES");
        fldResponsavelColeta.setText(record.getString("CRESPONSAVEL"));
        // T=Tendencia, " "=Indicador de Resultado
        boolean tipoInd = (record.getString("TIPOIND").trim().equals("T") ? true : false);
        chkTendencia.setSelected(tipoInd);
        if (tipoInd) {
            setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_tendencia.gif")));
        } else {
            setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif")));
        }

        if (getStatus() == BscDefaultFrameBehavior.INSERTING) {
            rdbAscendente.setSelected(true);
            tabQuery.resetExcluidos();
            tabQuery.setCabec();
            tblQuerySource.setDataSource(tabQuery.getVctDados());
        } else {
            if (record.getBoolean("ASCEND")) {
                rdbAscendente.setSelected(true);
            } else {
                rdbDescendente.setSelected(true);
            }
            tabQuery.resetExcluidos();
            tabQuery.setCabec();
            backConsultas = record.getBIXMLVector("DWCONSULTAS");
            tblQuerySource.setDataSource(backConsultas.clone2());
        }

        event.populateComboWithName(record.getBIXMLVector("UNIDADES"), "UNIDADE",
                htbUnidade, cbbUnidade);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FREQ",
                htbFrequencia, cbbFrequencia);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FCUMULA",
                htbFCumula, cbbFCumula, getIgnoreIDList());

        event.populateCombo(record.getBIXMLVector("TIPOMONTS"), "TCUMULA",
                htbTCumula, cbbTCumula);

        // Coleta
        txtMetrica.setText(record.getString("METRICA"));
        txtForma.setText(record.getString("FORMA"));

        // Referencia
        fldNomeRef.setText(record.getString("RNOME"));
        txtDescricaoRef.setText(record.getString("RDESCRICAO"));
        spnCasasDecimaisRef.setValue(new Integer(record.getInt("RDECIMAIS")));
        spnPesoRef.setValue(record.getInt("RPESO"));

        event.populateComboWithName(record.getBIXMLVector("UNIDADES"), "RUNIDADE",
                htbUnidadeRef, cbbUnidadeRef);

        event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "RFREQ",
                htbFrequenciaRef, cbbFrequenciaRef);

        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");

        pnlHeader.setComponents(vctContexto);

    }

    @Override
    public bsc.xml.BIXMLRecord getFieldsContents() {
        BIXMLRecord recordAux = new BIXMLRecord(type);
        String unidade = (String) cbbUnidade.getSelectedItem();
        String unidade_de_referencia = (String) cbbUnidadeRef.getSelectedItem();

        recordAux.set("ID", id);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("RESPID", respId);
        recordAux.set("TIPOPESSOA", tipoPessoa);
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("DECIMAIS", ((Integer) spnCasasDecimais.getValue()).intValue());
        recordAux.set("PESO", ((Integer) spnPeso.getValue()).intValue());
        recordAux.set("UNIDADE", unidade.isEmpty() ? record.getString("UNIDADE") : unidade);
        recordAux.set("FREQ", event.getComboValue(htbFrequencia, cbbFrequencia));
        recordAux.set("DATASRCID", "0");
        recordAux.set("CUMULATIVO", chkCumulativo.isSelected());
        recordAux.set("ASCEND", rdbAscendente.isSelected());
        recordAux.set("FCUMULA", event.getComboValue(htbFCumula, cbbFCumula));
        recordAux.set("TCUMULA", event.getComboValue(htbTCumula, cbbTCumula));

        // Coleta
        recordAux.set("METRICA", txtMetrica.getText());
        recordAux.set("FORMA", txtForma.getText());
        recordAux.set("MEDRESPID", colRespId);
        recordAux.set("MEDTIPOPES", colTipoPessoa);

        // Referencia
        recordAux.set("RNOME", fldNomeRef.getText());
        recordAux.set("RDESCRICAO", txtDescricaoRef.getText());
        recordAux.set("RDECIMAIS", ((Integer) spnCasasDecimaisRef.getValue()).intValue());
        recordAux.set("RPESO", ((Integer) spnPesoRef.getValue()).intValue());
        recordAux.set("RUNIDADE", unidade_de_referencia.isEmpty() ? record.getString("RUNIDADE") : unidade_de_referencia);
        recordAux.set("RFREQ", event.getComboValue(htbFrequenciaRef, cbbFrequenciaRef));
        recordAux.set("RDATASRCID", "0");
        recordAux.set("RRESPID", refRespId);
        recordAux.set("RTIPOPES", refTipoPessoa);
        recordAux.set(listaInfluencia.getXMLData());

        // T=Tendencia, " "=Indicador de Resultado
        recordAux.set("TIPOIND", (chkTendencia.isSelected() ? "T" : ""));

        //Vetor com as consultas.
        bsc.xml.BIXMLVector vctDadosCon = tblQuerySource.getXMLData();
        bsc.xml.BIXMLRecord recDadosDel = tabQuery.getDelDados();

        if (recDadosDel != null) {
            vctDadosCon.add(recDadosDel);
        }
        recordAux.set(vctDadosCon);

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        if (cbbFrequencia.getSelectedIndex() == 0 || fldNome.getText().trim().length() == 0) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        } else {
            valid = true;
        }

        return valid;
    }

    public boolean hasCombos() {
        return true;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgpOrder = new javax.swing.ButtonGroup();
        bgpOrderRef = new javax.swing.ButtonGroup();
        btnOpcConsulta = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        tblQuerySource = new bsc.beans.JBIXMLTable();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        cbbUnidade = new javax.swing.JComboBox();
        lblCasasDecimais = new javax.swing.JLabel();
        spnCasasDecimais = new javax.swing.JSpinner();
        cbbFrequencia = new javax.swing.JComboBox();
        lblResponsavel = new javax.swing.JLabel();
        lblFrequencia = new javax.swing.JLabel();
        pnlOrientacao = new javax.swing.JPanel();
        rdbDescendente = new javax.swing.JRadioButton();
        rdbAscendente = new javax.swing.JRadioButton();
        fldNome = new pv.jfcx.JPVEdit();
        lblDescricao = new javax.swing.JLabel();
        chkCumulativo = new javax.swing.JCheckBox();
        cbbFCumula = new javax.swing.JComboBox();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        chkTendencia = new javax.swing.JCheckBox();
        cbbTCumula = new javax.swing.JComboBox();
        lblPeso = new javax.swing.JLabel();
        spnPeso = new javax.swing.JSpinner();
        lblUnidade1 = new javax.swing.JLabel();
        lblTipoCumulativo = new javax.swing.JLabel();
        lblFrequenciaCumulativo = new javax.swing.JLabel();
        scpRecord1 = new javax.swing.JScrollPane();
        pnlData1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtMetrica = new bsc.beans.JBITextArea();
        lblResponsavelColeta = new javax.swing.JLabel();
        lblMetrica = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtForma = new bsc.beans.JBITextArea();
        lblForma = new javax.swing.JLabel();
        fldResponsavelColeta = new pv.jfcx.JPVEdit();
        btnResponsavelColeta = new javax.swing.JButton();
        scpRecord2 = new javax.swing.JScrollPane();
        pnlData2 = new javax.swing.JPanel();
        lblNomeRef = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtDescricaoRef = new bsc.beans.JBITextArea();
        lblUnidadeRef = new javax.swing.JLabel();
        cbbUnidadeRef = new javax.swing.JComboBox();
        lblCasasDecimaisRef = new javax.swing.JLabel();
        spnCasasDecimaisRef = new javax.swing.JSpinner();
        cbbFrequenciaRef = new javax.swing.JComboBox();
        lblResponsavelRef = new javax.swing.JLabel();
        lblFrequenciaRef = new javax.swing.JLabel();
        lblDescricaoRef = new javax.swing.JLabel();
        fldNomeRef = new pv.jfcx.JPVEdit();
        fldResponsavelRef = new pv.jfcx.JPVEdit();
        btnResponsavelRef = new javax.swing.JButton();
        lblPesoRef = new javax.swing.JLabel();
        spnPesoRef = new javax.swing.JSpinner();
        listaInfluencia = new bsc.beans.JBISelectionPanel();
        pnlConsulta = new javax.swing.JPanel();
        pnlGrupoConsulta = new javax.swing.JPanel();
        cbbDataWareHouse = new javax.swing.JComboBox();
        lblEnderecoDW = new javax.swing.JLabel();
        lblDatawarehouse = new javax.swing.JLabel();
        lblConsulta = new javax.swing.JLabel();
        fldUrlDW = new pv.jfcx.JPVEdit();
        cbbConsultas = new javax.swing.JComboBox();
        btnPostEndereco = new javax.swing.JButton();
        pnlQuery = new javax.swing.JPanel();
        toolQuery = new javax.swing.JToolBar();
        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        sepQuery = new javax.swing.JSeparator();
        btnAbrirTabela = new javax.swing.JButton();
        optTabela = new javax.swing.JRadioButton();
        optGrafico = new javax.swing.JRadioButton();
        srcQuery = new javax.swing.JScrollPane();
        listaMetas = new bsc.beans.JBIListPanel();
        listaAvaliacoes = new bsc.beans.JBIListPanel();
        valoresList = new bsc.beans.JBIListPanel();
        listaFontes = new bsc.beans.JBIListPanel();
        listaDocumento = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscIndicadorFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 22));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscIndicadorFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscIndicadorFrame_00006")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscIndicadorFrame_00007")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscIndicadorFrame_00008")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscIndicadorFrame_00009")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setMinimumSize(new java.awt.Dimension(390, 60));
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(465, 236));

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData.setPreferredSize(new java.awt.Dimension(500, 500));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscIndicadorFrame_00010")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 20, 64, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtDescricao.setEnabled(false);
        jScrollPane3.setViewportView(txtDescricao);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(20, 80, 400, 60);

        cbbUnidade.setEditable(true);
        cbbUnidade.setEnabled(false);
        pnlData.add(cbbUnidade);
        cbbUnidade.setBounds(20, 160, 180, 22);

        lblCasasDecimais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCasasDecimais.setText(bundle.getString("BscIndicadorFrame_00012")); // NOI18N
        lblCasasDecimais.setEnabled(false);
        pnlData.add(lblCasasDecimais);
        lblCasasDecimais.setBounds(240, 140, 60, 20);

        spnCasasDecimais.setEnabled(false);
        pnlData.add(spnCasasDecimais);
        spnCasasDecimais.setBounds(240, 160, 180, 22);

        cbbFrequencia.setEnabled(false);
        cbbFrequencia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFrequenciaItemStateChanged(evt);
            }
        });
        cbbFrequencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbFrequenciaActionPerformed(evt);
            }
        });
        pnlData.add(cbbFrequencia);
        cbbFrequencia.setBounds(20, 200, 180, 22);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(20, 220, 76, 20);

        lblFrequencia.setForeground(new java.awt.Color(51, 51, 255));
        lblFrequencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequencia.setText(bundle.getString("BscIndicadorFrame_00014")); // NOI18N
        lblFrequencia.setEnabled(false);
        pnlData.add(lblFrequencia);
        lblFrequencia.setBounds(20, 180, 76, 20);

        pnlOrientacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("BscIndicadorFrame_00030"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        pnlOrientacao.setEnabled(false);
        pnlOrientacao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        pnlOrientacao.setLayout(null);

        bgpOrder.add(rdbDescendente);
        rdbDescendente.setText(bundle.getString("BscIndicadorFrame_00016")); // NOI18N
        rdbDescendente.setEnabled(false);
        pnlOrientacao.add(rdbDescendente);
        rdbDescendente.setBounds(220, 20, 112, 23);

        bgpOrder.add(rdbAscendente);
        rdbAscendente.setText(bundle.getString("BscIndicadorFrame_00015")); // NOI18N
        rdbAscendente.setEnabled(false);
        pnlOrientacao.add(rdbAscendente);
        rdbAscendente.setBounds(20, 20, 112, 23);

        pnlData.add(pnlOrientacao);
        pnlOrientacao.setBounds(20, 360, 400, 50);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 40, 400, 22);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscIndicadorFrame_00017")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(20, 60, 76, 20);

        chkCumulativo.setText(bundle.getString("BscIndicadorFrame_00029")); // NOI18N
        chkCumulativo.setActionCommand("Que");
        chkCumulativo.setEnabled(false);
        chkCumulativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCumulativoActionPerformed(evt);
            }
        });
        pnlData.add(chkCumulativo);
        chkCumulativo.setBounds(20, 280, 80, 20);

        cbbFCumula.setEnabled(false);
        cbbFCumula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbFCumulaActionPerformed(evt);
            }
        });
        pnlData.add(cbbFCumula);
        cbbFCumula.setBounds(20, 330, 180, 22);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(20, 240, 400, 22);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(430, 240, 30, 22);

        chkTendencia.setText("Ind. Tend�ncia");
        chkTendencia.setActionCommand("Que");
        chkTendencia.setEnabled(false);
        chkTendencia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkTendenciaItemStateChanged(evt);
            }
        });
        pnlData.add(chkTendencia);
        chkTendencia.setBounds(240, 280, 120, 20);

        cbbTCumula.setEnabled(false);
        pnlData.add(cbbTCumula);
        cbbTCumula.setBounds(240, 330, 180, 22);

        lblPeso.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPeso.setText(bundle.getString("BscIndicadorFrame_00046")); // NOI18N
        lblPeso.setEnabled(false);
        pnlData.add(lblPeso);
        lblPeso.setBounds(240, 180, 60, 20);

        spnPeso.setModel(spmPeso);
        spnPeso.setEnabled(false);
        pnlData.add(spnPeso);
        spnPeso.setBounds(240, 200, 180, 22);

        lblUnidade1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUnidade1.setText(bundle.getString("BscIndicadorFrame_00011")); // NOI18N
        lblUnidade1.setEnabled(false);
        pnlData.add(lblUnidade1);
        lblUnidade1.setBounds(20, 140, 72, 20);

        lblTipoCumulativo.setText(bundle.getString("BscIndicadorDetalheFrame_00002")); // NOI18N
        lblTipoCumulativo.setEnabled(false);
        pnlData.add(lblTipoCumulativo);
        lblTipoCumulativo.setBounds(240, 310, 34, 14);
        lblTipoCumulativo.getAccessibleContext().setAccessibleName("lbl");

        lblFrequenciaCumulativo.setText(bundle.getString("BscIndicadorFrame_00014")); // NOI18N
        lblFrequenciaCumulativo.setEnabled(false);
        pnlData.add(lblFrequenciaCumulativo);
        lblFrequenciaCumulativo.setBounds(20, 310, 90, 14);

        scpRecord.setViewportView(pnlData);
        tblQuerySource.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tblQuerySource.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblQuerySource.setPreferredScrollableViewportSize(new java.awt.Dimension(300, 64));
        srcQuery.setViewportView(tblQuerySource);

        pnlQuery.add(srcQuery, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00001"), scpRecord); // NOI18N

        scpRecord1.setBorder(null);
        scpRecord1.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData1.setPreferredSize(new java.awt.Dimension(460, 200));
        pnlData1.setLayout(null);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane4.setEnabled(false);
        jScrollPane4.setViewportView(txtMetrica);

        pnlData1.add(jScrollPane4);
        jScrollPane4.setBounds(20, 80, 400, 60);

        lblResponsavelColeta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavelColeta.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavelColeta.setEnabled(false);
        pnlData1.add(lblResponsavelColeta);
        lblResponsavelColeta.setBounds(20, 20, 120, 20);

        lblMetrica.setText(bundle.getString("BscIndicadorFrame_00023")); // NOI18N
        lblMetrica.setEnabled(false);
        pnlData1.add(lblMetrica);
        lblMetrica.setBounds(20, 60, 123, 18);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane5.setEnabled(false);
        jScrollPane5.setViewportView(txtForma);

        pnlData1.add(jScrollPane5);
        jScrollPane5.setBounds(20, 160, 400, 55);

        lblForma.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblForma.setText(bundle.getString("BscIndicadorFrame_00024")); // NOI18N
        lblForma.setEnabled(false);
        pnlData1.add(lblForma);
        lblForma.setBounds(20, 140, 123, 20);

        fldResponsavelColeta.setEnabled(false);
        fldResponsavelColeta.setMaxLength(60);
        fldResponsavelColeta.setSelectAllOnDoubleClick(true);
        pnlData1.add(fldResponsavelColeta);
        fldResponsavelColeta.setBounds(20, 40, 400, 22);

        btnResponsavelColeta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavelColeta.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavelColeta.setEnabled(false);
        btnResponsavelColeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelColetaActionPerformed(evt);
            }
        });
        pnlData1.add(btnResponsavelColeta);
        btnResponsavelColeta.setBounds(430, 40, 30, 22);

        scpRecord1.setViewportView(pnlData1);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00022"), scpRecord1); // NOI18N

        scpRecord2.setBorder(null);
        scpRecord2.setPreferredSize(new java.awt.Dimension(460, 210));

        pnlData2.setPreferredSize(new java.awt.Dimension(460, 200));
        pnlData2.setLayout(null);

        lblNomeRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNomeRef.setText(bundle.getString("BscIndicadorFrame_00026")); // NOI18N
        lblNomeRef.setEnabled(false);
        lblNomeRef.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNomeRef.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNomeRef.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData2.add(lblNomeRef);
        lblNomeRef.setBounds(20, 20, 80, 16);

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtDescricaoRef.setEnabled(false);
        jScrollPane6.setViewportView(txtDescricaoRef);

        pnlData2.add(jScrollPane6);
        jScrollPane6.setBounds(20, 80, 400, 50);

        lblUnidadeRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUnidadeRef.setText(bundle.getString("BscIndicadorFrame_00011")); // NOI18N
        lblUnidadeRef.setEnabled(false);
        pnlData2.add(lblUnidadeRef);
        lblUnidadeRef.setBounds(20, 130, 72, 20);

        cbbUnidadeRef.setEditable(true);
        cbbUnidadeRef.setEnabled(false);
        pnlData2.add(cbbUnidadeRef);
        cbbUnidadeRef.setBounds(20, 150, 180, 22);

        lblCasasDecimaisRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCasasDecimaisRef.setText(bundle.getString("BscIndicadorFrame_00012")); // NOI18N
        lblCasasDecimaisRef.setEnabled(false);
        pnlData2.add(lblCasasDecimaisRef);
        lblCasasDecimaisRef.setBounds(240, 130, 63, 20);

        spnCasasDecimaisRef.setEnabled(false);
        pnlData2.add(spnCasasDecimaisRef);
        spnCasasDecimaisRef.setBounds(240, 150, 180, 22);

        cbbFrequenciaRef.setEnabled(false);
        pnlData2.add(cbbFrequenciaRef);
        cbbFrequenciaRef.setBounds(20, 190, 180, 22);

        lblResponsavelRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavelRef.setText(bundle.getString("BscIndicadorFrame_00013")); // NOI18N
        lblResponsavelRef.setEnabled(false);
        pnlData2.add(lblResponsavelRef);
        lblResponsavelRef.setBounds(20, 210, 76, 20);

        lblFrequenciaRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblFrequenciaRef.setText(bundle.getString("BscIndicadorFrame_00014")); // NOI18N
        lblFrequenciaRef.setEnabled(false);
        pnlData2.add(lblFrequenciaRef);
        lblFrequenciaRef.setBounds(20, 170, 76, 20);

        lblDescricaoRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricaoRef.setText(bundle.getString("BscIndicadorFrame_00017")); // NOI18N
        lblDescricaoRef.setEnabled(false);
        pnlData2.add(lblDescricaoRef);
        lblDescricaoRef.setBounds(20, 60, 76, 20);

        fldNomeRef.setEnabled(false);
        fldNomeRef.setMaxLength(60);
        pnlData2.add(fldNomeRef);
        fldNomeRef.setBounds(20, 40, 400, 22);

        fldResponsavelRef.setEnabled(false);
        fldResponsavelRef.setMaxLength(60);
        fldResponsavelRef.setSelectAllOnDoubleClick(true);
        pnlData2.add(fldResponsavelRef);
        fldResponsavelRef.setBounds(20, 230, 400, 22);

        btnResponsavelRef.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavelRef.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnResponsavelRef.setEnabled(false);
        btnResponsavelRef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelRefActionPerformed(evt);
            }
        });
        pnlData2.add(btnResponsavelRef);
        btnResponsavelRef.setBounds(450, 230, 30, 20);

        lblPesoRef.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPesoRef.setText(bundle.getString("BscIndicadorFrame_00046")); // NOI18N
        lblPesoRef.setEnabled(false);
        pnlData2.add(lblPesoRef);
        lblPesoRef.setBounds(240, 170, 60, 20);

        spnPesoRef.setModel(spmPeso);
        spnPesoRef.setEnabled(false);
        pnlData2.add(spnPesoRef);
        spnPesoRef.setBounds(240, 190, 180, 22);

        scpRecord2.setViewportView(pnlData2);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00025"), scpRecord2); // NOI18N

        listaInfluencia.setEnabled(false);
        jTabbedPane1.addTab("Influ�ncia", listaInfluencia);

        pnlConsulta.setLayout(new java.awt.BorderLayout());

        pnlGrupoConsulta.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlGrupoConsulta.setPreferredSize(new java.awt.Dimension(100, 150));
        pnlGrupoConsulta.setLayout(null);

        cbbDataWareHouse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbDataWareHouseItemStateChanged(evt);
            }
        });
        pnlGrupoConsulta.add(cbbDataWareHouse);
        cbbDataWareHouse.setBounds(20, 70, 400, 22);

        lblEnderecoDW.setText(bundle.getString("BscIndicadorFrame_00034")); // NOI18N
        pnlGrupoConsulta.add(lblEnderecoDW);
        lblEnderecoDW.setBounds(20, 10, 100, 20);

        lblDatawarehouse.setText(bundle.getString("BscIndicadorFrame_00035")); // NOI18N
        pnlGrupoConsulta.add(lblDatawarehouse);
        lblDatawarehouse.setBounds(20, 50, 100, 20);

        lblConsulta.setText(bundle.getString("BscIndicadorFrame_00036")); // NOI18N
        pnlGrupoConsulta.add(lblConsulta);
        lblConsulta.setBounds(20, 90, 100, 20);

        fldUrlDW.setMaxLength(60);
        pnlGrupoConsulta.add(fldUrlDW);
        fldUrlDW.setBounds(20, 30, 400, 22);
        pnlGrupoConsulta.add(cbbConsultas);
        cbbConsultas.setBounds(20, 110, 400, 22);

        btnPostEndereco.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPostEndereco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif"))); // NOI18N
        btnPostEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPostEnderecoActionPerformed(evt);
            }
        });
        pnlGrupoConsulta.add(btnPostEndereco);
        btnPostEndereco.setBounds(430, 30, 25, 25);

        pnlConsulta.add(pnlGrupoConsulta, java.awt.BorderLayout.NORTH);

        pnlQuery.setLayout(new java.awt.BorderLayout());

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_addconsulta.gif"))); // NOI18N
        btnAdd.setText(bundle.getString("BscIndicadorFrame_00040")); // NOI18N
        btnAdd.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnAdd.setMaximumSize(new java.awt.Dimension(76, 20));
        btnAdd.setMinimumSize(new java.awt.Dimension(76, 20));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        toolQuery.add(btnAdd);

        btnRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_removeconsulta.gif"))); // NOI18N
        btnRemove.setText(bundle.getString("BscIndicadorFrame_00041")); // NOI18N
        btnRemove.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnRemove.setMaximumSize(new java.awt.Dimension(76, 20));
        btnRemove.setMinimumSize(new java.awt.Dimension(76, 20));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        toolQuery.add(btnRemove);

        sepQuery.setOrientation(javax.swing.SwingConstants.VERTICAL);
        sepQuery.setMaximumSize(new java.awt.Dimension(10, 20));
        sepQuery.setPreferredSize(new java.awt.Dimension(0, 0));
        toolQuery.add(sepQuery);

        btnAbrirTabela.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAbrirTabela.setText(bundle.getString("BscIndicadorFrame_00037")); // NOI18N
        btnAbrirTabela.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnAbrirTabela.setMaximumSize(new java.awt.Dimension(120, 20));
        btnAbrirTabela.setMinimumSize(new java.awt.Dimension(120, 20));
        btnAbrirTabela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirTabelaActionPerformed(evt);
            }
        });
        toolQuery.add(btnAbrirTabela);

        btnOpcConsulta.add(optTabela);
        optTabela.setSelected(true);
        optTabela.setText(bundle.getString("BscIndicadorFrame_00038")); // NOI18N
        optTabela.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        optTabela.setMaximumSize(new java.awt.Dimension(80, 15));
        optTabela.setMinimumSize(new java.awt.Dimension(80, 15));
        optTabela.setPreferredSize(new java.awt.Dimension(80, 15));
        optTabela.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_consultadw_des.gif"))); // NOI18N
        toolQuery.add(optTabela);

        btnOpcConsulta.add(optGrafico);
        optGrafico.setText(bundle.getString("BscIndicadorFrame_00042")); // NOI18N
        optGrafico.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_grafico_des.gif"))); // NOI18N
        optGrafico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        optGrafico.setMaximumSize(new java.awt.Dimension(80, 15));
        optGrafico.setMinimumSize(new java.awt.Dimension(80, 15));
        optGrafico.setPreferredSize(new java.awt.Dimension(80, 15));
        optGrafico.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_grafico.gif"))); // NOI18N
        toolQuery.add(optGrafico);

        pnlQuery.add(toolQuery, java.awt.BorderLayout.NORTH);
        pnlQuery.add(srcQuery, java.awt.BorderLayout.CENTER);

        pnlConsulta.add(pnlQuery, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("BscIndicadorFrame_00033"), pnlConsulta); // NOI18N

        pnlFields.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00001"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00019"), listaMetas); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00018"), listaAvaliacoes); // NOI18N

        valoresList.setNovoEnabled(false);
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00020"), valoresList); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00021"), listaFontes); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIndicadorFrame_00031"), listaDocumento); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 60));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAbrirTabelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirTabelaActionPerformed
        int opcao = 1;
        if (optGrafico.isSelected()) {
            opcao = 2;
        }
        showConsulta(opcao);
    }//GEN-LAST:event_btnAbrirTabelaActionPerformed

    private void cbbDataWareHouseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbDataWareHouseItemStateChanged
        if (evt.getStateChange() == ItemEvent.DESELECTED) {
            int itemSelecionado = cbbDataWareHouse.getSelectedIndex();
            if (itemSelecionado > 0) {
                bsc.xml.BIXMLVector dwConsultas = recDataWareHouse.getBIXMLVector("DWCONSULTAS");
                bsc.xml.BIXMLRecord recConsultas = dwConsultas.get(itemSelecionado - 1);

                //Se for maior que dois existem itens de consulta.
                if (recConsultas.size() > 2) {
                    bsc.xml.BIXMLVector vctConsultas = recConsultas.getBIXMLVector("QUERY_LISTS");
                    event.populateCombo(vctConsultas, "ID", htbConsultas, cbbConsultas);
                } else {
                    cbbConsultas.removeAllItems();
                }
            } else {
                cbbConsultas.removeAllItems();
            }
        }
    }//GEN-LAST:event_cbbDataWareHouseItemStateChanged

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int linhaSelecionada = tblQuerySource.getFocusRow();
        if (linhaSelecionada > -1) {
            bsc.xml.BIXMLRecord recLinha = tblQuerySource.getXMLData().get(linhaSelecionada);
            tabQuery.addRecordDeleted(tblQuerySource.getXMLData().get(linhaSelecionada));
            tblQuerySource.removeRecord();
        } else {
            dialogSystem.informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00039"));
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        boolean lAddNewReg = true;
        String url = new String(""),
                dw = new String(""),
                consulta = new String(""),
                idConsulta = new String("");

        if (!fldUrlDW.getText().equals("")) {
            url = fldUrlDW.getText().toLowerCase();
        } else {
            lAddNewReg = false;
        }

        if (lAddNewReg && cbbDataWareHouse.getSelectedIndex() > 0) {
            dw = (String) cbbDataWareHouse.getSelectedItem();
        } else {
            lAddNewReg = false;
        }

        if (lAddNewReg && cbbConsultas.getSelectedIndex() > 0) {
            consulta = (String) cbbConsultas.getSelectedItem();
            idConsulta = event.getComboValue(htbConsultas, cbbConsultas);
        } else {
            lAddNewReg = false;
        }

        if (lAddNewReg) {
            tblQuerySource.addNewRecord(tabQuery.createNewRecord(url, dw, consulta, idConsulta));
        } else {
            dialogSystem.informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00039"));
        }

    }//GEN-LAST:event_btnAddActionPerformed

    private void btnPostEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPostEnderecoActionPerformed
        bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition(1);
        bsc.core.BscStaticReferences.getBscMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));

        StringBuffer request = new StringBuffer("REQ_DATAWAREHOUSE");
        request.append("|");
        request.append(fldUrlDW.getText());
        request.append("|");
        request.append("false"); //Carregar detalhes da consulta

        //Requisitando os dados.
        recDataWareHouse = event.loadRecord("-1", "DWCONSULTA", request.toString());
        event.populateCombo(recDataWareHouse.getBIXMLVector("DWCONSULTAS"), "DWCONSULTA", htbDataWareHouse, cbbDataWareHouse);

        bsc.core.BscStaticReferences.getBscMainPanel().setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));
        bsc.core.BscStaticReferences.getBscMainPanel().setStatusBarCondition(0);
    }//GEN-LAST:event_btnPostEnderecoActionPerformed

    private void cbbFCumulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbFCumulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbbFCumulaActionPerformed

    private void chkTendenciaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkTendenciaItemStateChanged
        listaInfluencia.setEnabled(chkTendencia.isSelected());
    }//GEN-LAST:event_chkTendenciaItemStateChanged

    private void btnResponsavelRefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelRefActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(refTipoPessoa);
        vector.add(refRespId);
        vector.add(fldResponsavelRef.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        refTipoPessoa = vctSelecionados.get(0).toString();
        refRespId = vctSelecionados.get(1).toString();
        fldResponsavelRef.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelRefActionPerformed

    private void btnResponsavelColetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelColetaActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(colTipoPessoa);
        vector.add(colRespId);
        vector.add(fldResponsavelColeta.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        colTipoPessoa = vctSelecionados.get(0).toString();
        colRespId = vctSelecionados.get(1).toString();
        fldResponsavelColeta.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelColetaActionPerformed

    private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
        java.util.Vector vector = new java.util.Vector(); // Instancia um novo vector.
        vector.add(tipoPessoa);
        vector.add(respId);
        vector.add(fldResponsavel.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        tipoPessoa = vctSelecionados.get(0).toString();
        respId = vctSelecionados.get(1).toString();
        fldResponsavel.setText(vctSelecionados.get(2).toString()); // Seta o texto do campos resp.
    }//GEN-LAST:event_btnResponsavelActionPerformed

	private void cbbFrequenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbFrequenciaActionPerformed
            // TODO add your handling code here:
	}//GEN-LAST:event_cbbFrequenciaActionPerformed

	private void cbbFrequenciaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFrequenciaItemStateChanged
            if (getStatus() == BscDefaultFrameBehavior.INSERTING || getStatus() == BscDefaultFrameBehavior.UPDATING) {
                cbbFrequenciaRef.setSelectedIndex(cbbFrequencia.getSelectedIndex());
                event.populateCombo(record.getBIXMLVector("FREQUENCIAS"), "FCUMULA",
                        htbFCumula, cbbFCumula, getIgnoreIDList());
            }
	}//GEN-LAST:event_cbbFrequenciaItemStateChanged

	private void chkCumulativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkCumulativoActionPerformed
            // TODO add your handling code here:
	}//GEN-LAST:event_chkCumulativoActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("MEDIDAS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            if (getStatus() != bsc.swing.BscDefaultFrameBehavior.INSERTING) // Frequencia de Avalia��o de Resultado
            {
                if (!event.getComboValue(htbFrequencia, cbbFrequencia).equals(record.getString("FREQ"))) {
                    if (javax.swing.JOptionPane.showConfirmDialog(this,
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00003") +
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00004"),
                            java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00005"), javax.swing.JOptionPane.YES_NO_OPTION) != javax.swing.JOptionPane.YES_OPTION) {
                        return;
                    }
                }
            }
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            if (backConsultas != null) {
                tblQuerySource.setDataSource(backConsultas.clone2());
            }
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void btnAcumActionPerformed(java.awt.event.ActionEvent evt) {
        if (chkCumulativo.isSelected()) {
            event.executeRecord(getID());
        } else {
            javax.swing.JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00044"),
                    java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00043"),
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgpOrder;
    private javax.swing.ButtonGroup bgpOrderRef;
    private javax.swing.JButton btnAbrirTabela;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.ButtonGroup btnOpcConsulta;
    private javax.swing.JButton btnPostEndereco;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnResponsavelColeta;
    private javax.swing.JButton btnResponsavelRef;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbConsultas;
    private javax.swing.JComboBox cbbDataWareHouse;
    private javax.swing.JComboBox cbbFCumula;
    private javax.swing.JComboBox cbbFrequencia;
    private javax.swing.JComboBox cbbFrequenciaRef;
    private javax.swing.JComboBox cbbTCumula;
    private javax.swing.JComboBox cbbUnidade;
    private javax.swing.JComboBox cbbUnidadeRef;
    private javax.swing.JCheckBox chkCumulativo;
    private javax.swing.JCheckBox chkTendencia;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldNomeRef;
    private pv.jfcx.JPVEdit fldResponsavel;
    private pv.jfcx.JPVEdit fldResponsavelColeta;
    private pv.jfcx.JPVEdit fldResponsavelRef;
    private pv.jfcx.JPVEdit fldUrlDW;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblCasasDecimais;
    private javax.swing.JLabel lblCasasDecimaisRef;
    private javax.swing.JLabel lblConsulta;
    private javax.swing.JLabel lblDatawarehouse;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblDescricaoRef;
    private javax.swing.JLabel lblEnderecoDW;
    private javax.swing.JLabel lblForma;
    private javax.swing.JLabel lblFrequencia;
    private javax.swing.JLabel lblFrequenciaCumulativo;
    private javax.swing.JLabel lblFrequenciaRef;
    private javax.swing.JLabel lblMetrica;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeRef;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblPesoRef;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblResponsavelColeta;
    private javax.swing.JLabel lblResponsavelRef;
    private javax.swing.JLabel lblTipoCumulativo;
    private javax.swing.JLabel lblUnidade1;
    private javax.swing.JLabel lblUnidadeRef;
    private bsc.beans.JBIListPanel listaAvaliacoes;
    private bsc.beans.JBIListPanel listaDocumento;
    private bsc.beans.JBIListPanel listaFontes;
    private bsc.beans.JBISelectionPanel listaInfluencia;
    private bsc.beans.JBIListPanel listaMetas;
    private javax.swing.JRadioButton optGrafico;
    private javax.swing.JRadioButton optTabela;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlConsulta;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlData1;
    private javax.swing.JPanel pnlData2;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlGrupoConsulta;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlOrientacao;
    private javax.swing.JPanel pnlQuery;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton rdbAscendente;
    private javax.swing.JRadioButton rdbDescendente;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane scpRecord1;
    private javax.swing.JScrollPane scpRecord2;
    private javax.swing.JSeparator sepQuery;
    private javax.swing.JSpinner spnCasasDecimais;
    private javax.swing.JSpinner spnCasasDecimaisRef;
    private javax.swing.JSpinner spnPeso;
    private javax.swing.JSpinner spnPesoRef;
    private javax.swing.JScrollPane srcQuery;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private javax.swing.JToolBar toolQuery;
    private bsc.beans.JBITextArea txtDescricao;
    private bsc.beans.JBITextArea txtDescricaoRef;
    private bsc.beans.JBITextArea txtForma;
    private bsc.beans.JBITextArea txtMetrica;
    private bsc.beans.JBIListPanel valoresList;
    // End of variables declaration//GEN-END:variables
    private bsc.beans.JBIXMLTable tblQuerySource;
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscIndicadorFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);

        JButton btnAcum = new JButton();
        btnAcum.setFont(new java.awt.Font("Dialog", 0, 12));
        btnAcum.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_play.gif")));
        btnAcum.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00045"));
        btnAcum.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnAcum.setMaximumSize(new java.awt.Dimension(76, 20));
        btnAcum.setMinimumSize(new java.awt.Dimension(76, 20));
        btnAcum.setPreferredSize(new java.awt.Dimension(76, 20));
        btnAcum.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcumActionPerformed(evt);
            }
        });
        listaMetas.tbInsertion.add(btnAcum);
        listaMetas.repaint();

        tblQuerySource.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblQuerySourceMouseClicked(evt);
            }
        });

    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public java.util.Hashtable getIgnoreIDList() {
        java.util.Hashtable htbIgnoreIDList = new java.util.Hashtable();

        if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_ANUAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_MENSAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (event.getComboValue(htbFrequencia, cbbFrequencia).trim().equals("" + bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL)) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        }

        return htbIgnoreIDList;
    }

    /**
     *  Evento ao clicar Mouse na planilha.
     **/
    private void tblQuerySourceMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            int opcao = 1;
            if (optGrafico.isSelected()) {
                opcao = 2;
            }
            showConsulta(opcao);
        }
    }

    private void showConsulta(int iTipo) {
        try {
            int lin = tblQuerySource.getSelectedRow();
            if (lin > -1) {
                bsc.xml.BIXMLRecord recLinha = tblQuerySource.getXMLData().get(lin);
                StringBuffer request = new StringBuffer("REQUEST_DWACCESS|");
                request.append(recLinha.getString("URL").concat("|"));
                request.append(recLinha.getString("DW"));
                bsc.xml.BIXMLRecord rRequest = event.loadRecord("-1", "DWCONSULTA", request.toString());
                //Carregando os dados da consulta
                request = new StringBuffer("http://");
                request.append(recLinha.getString("URL").toLowerCase());
                request.append("/h_m01showcons.apw?resetwindow=2&loadcons=true&issched=on&info=");
                request.append(String.valueOf(iTipo));
                request.append("&id=");
                request.append(recLinha.getString("IDCONS"));
                request.append(rRequest.getString("REQUEST"));

                //Requisita a URL.
                bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                java.net.URL url = new java.net.URL(request.toString());
                bscApplet.getAppletContext().showDocument(url, "_new");
            } else {
                dialogSystem.informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorFrame_00039"));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void afterExecute() {
        listaMetas.refresh();
    }

    @Override
    public void afterUpdate() {
        listaMetas.refresh();
    }

    @Override
    public void afterInsert() {
        listaMetas.refresh();
    }

    @Override
    public void afterDelete() {
    }
}
/*
 * BscDataSourceTable
 *
 * Created on 27 de Julho de 2005, 16:49
 *
 * @author Alexandre Alves da Silva
 */

class BscIndSourceQueryTable {

    private bsc.xml.BIXMLAttributes attIndicador = new bsc.xml.BIXMLAttributes();
    private bsc.xml.BIXMLVector vctDados = null;
    private bsc.xml.BIXMLRecord recDelDados = null;

    /** Creates a new instance of KpiTabIndicador */
    public BscIndSourceQueryTable() {
        setVctDados(new bsc.xml.BIXMLVector("DWCONSULTAS", "DWCONSULTA"));
    }

    public bsc.xml.BIXMLVector getVctDados() {
        return vctDados;
    }

    public void setVctDados(bsc.xml.BIXMLVector vctDados) {
        this.vctDados = vctDados;
    }

    public bsc.xml.BIXMLRecord createNewRecord(String url, String dw, String consulta, String idConsulta) {
        bsc.xml.BIXMLRecord newRecord = new bsc.xml.BIXMLRecord("DWCONSULTA");
        newRecord.set("ID", "-99");
        newRecord.set("URL", url);
        newRecord.set("DW", dw);
        newRecord.set("IDCONS", idConsulta);
        newRecord.set("CONSULTA", consulta);
        return newRecord;
    }

    public void addRecordDeleted(bsc.xml.BIXMLRecord record) {
        if (!record.getString("ID").equals("-99")) {
            if (recDelDados == null) {
                recDelDados = new bsc.xml.BIXMLRecord("REG_EXCLUIDO");
                recDelDados.set(new bsc.xml.BIXMLVector("EXCLUIDOS", "EXCLUIDO"));
            }
            bsc.xml.BIXMLVector vctAddReg = recDelDados.getBIXMLVector("EXCLUIDOS");
            vctAddReg.add(record);
        }
    }

    public bsc.xml.BIXMLRecord getDelDados() {
        return recDelDados;
    }

    public void resetDados() {
        setVctDados(new bsc.xml.BIXMLVector("DWCONSULTAS", "DWCONSULTA"));
    }

    public void resetExcluidos() {
        recDelDados = null;
    }

    public void setCabec() {
        resetDados();

        attIndicador = new bsc.xml.BIXMLAttributes();
        attIndicador.set("TIPO", "CONSULTA");
        attIndicador.set("RETORNA", "F");

        //Coluna Ano.
        attIndicador.set("TAG000", "URL");
        attIndicador.set("CAB000", "URL");
        attIndicador.set("CLA000", bsc.beans.JBIXMLTable.BSC_STRING);
        attIndicador.set("EDT000", "F");
        attIndicador.set("CUM000", "F");
        //Coluna Valor.
        attIndicador.set("TAG001", "DW");
        attIndicador.set("CAB001", "DataWareHouse");
        attIndicador.set("CLA001", bsc.beans.JBIXMLTable.BSC_STRING);
        attIndicador.set("EDT001", "F");
        attIndicador.set("CUM001", "F");

        //Coluna Meta.
        attIndicador.set("TAG002", "CONSULTA");
        attIndicador.set("CAB002", "Consulta");
        attIndicador.set("CLA002", bsc.beans.JBIXMLTable.BSC_STRING);
        attIndicador.set("EDT002", "F");
        attIndicador.set("CUM002", "F");

        vctDados.setAttributes(attIndicador);
    }
}
