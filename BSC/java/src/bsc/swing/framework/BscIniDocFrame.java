package bsc.swing.framework;

import java.util.ResourceBundle;

public class BscIniDocFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("INIDOC");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblLink.setEnabled(true);
        txtLink.setEnabled(true);
        lblTexto.setEnabled(true);
        txtTexto.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblLink.setEnabled(false);
        txtLink.setEnabled(false);
        lblTexto.setEnabled(false);
        txtTexto.setEnabled(false);
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        txtLink.setText(record.getString("LINK"));
        txtTexto.setText(record.getString("TEXTO"));
        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");

        pnlHeader.setComponents(vctContexto);
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("LINK", txtLink.getText());
        recordAux.set("TEXTO", txtTexto.getText());

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload2 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblLink = new javax.swing.JLabel();
        jScrollPane31 = new javax.swing.JScrollPane();
        txtLink = new bsc.beans.JBITextArea();
        lblTexto = new javax.swing.JLabel();
        jScrollPane311 = new javax.swing.JScrollPane();
        txtTexto = new bsc.beans.JBITextArea();
        btnReload1 = new javax.swing.JButton();
        fldNome = new pv.jfcx.JPVEdit();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscDocumentoFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscDocumentoFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscDocumentoFrame_00005")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscDocumentoFrame_00006")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscDocumentoFrame_00007")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscDocumentoFrame_00008")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload2.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload2.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload2.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload2.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(470, 260));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscDocumentoFrame_00009")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(12, 12, 75, 16);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscDocumentoFrame_00010")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 78, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setViewportView(txtDescricao);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(10, 70, 400, 50);

        lblLink.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLink.setText(bundle.getString("BscDocumentoFrame_00013")); // NOI18N
        lblLink.setEnabled(false);
        pnlData.add(lblLink);
        lblLink.setBounds(10, 120, 78, 20);

        jScrollPane31.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtLink.setRows(2);
        jScrollPane31.setViewportView(txtLink);

        pnlData.add(jScrollPane31);
        jScrollPane31.setBounds(10, 140, 400, 40);

        lblTexto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTexto.setText(bundle.getString("BscDocumentoFrame_00011")); // NOI18N
        lblTexto.setEnabled(false);
        pnlData.add(lblTexto);
        lblTexto.setBounds(10, 200, 75, 20);

        jScrollPane311.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane311.setPreferredSize(new java.awt.Dimension(366, 48));
        jScrollPane311.setViewportView(txtTexto);

        pnlData.add(jScrollPane311);
        jScrollPane311.setBounds(10, 220, 400, 74);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscDocumentoFrame_00012")); // NOI18N
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        pnlData.add(btnReload1);
        btnReload1.setBounds(290, 190, 122, 22);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 400, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscDocumentoFrame_00001"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 75));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReload2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload2ActionPerformed
            event.loadHelp("CADASTRANDO_INICIATIVAS");
	}//GEN-LAST:event_btnReload2ActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            try {
                bsc.core.BscStaticReferences.getBscApplet().getAppletContext().showDocument(new java.net.URL(txtLink.getText().trim()), "doc" + ((int) Math.random() * 1000));
            } catch (Exception e) {
                javax.swing.JOptionPane.showMessageDialog(this, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDocumentoFrame_00003"), java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscDocumentoFrame_00004"), javax.swing.JOptionPane.OK_OPTION);
            }
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnReload2;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane31;
    private javax.swing.JScrollPane jScrollPane311;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblLink;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblTexto;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    private bsc.beans.JBITextArea txtLink;
    private bsc.beans.JBITextArea txtTexto;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscIniDocFrame(int operation, String idAux, String contextId) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
