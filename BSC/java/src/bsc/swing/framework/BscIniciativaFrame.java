package bsc.swing.framework;

import java.util.ResourceBundle;

public class BscIniciativaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("INICIATIVA");
    private String respId = "";
    private String tipoPessoa = " ";
    bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblDataInicio.setEnabled(true);
        fldDataInicio.setEnabled(true);
        lblDataFim.setEnabled(true);
        fldDataFim.setEnabled(true);
        lblResponsavel.setEnabled(true);
        btnResponsavel.setEnabled(true);
        fldCustoReal.setEnabled(true);
        lblCustoReal.setEnabled(true);
        fldCustoEstimado.setEnabled(true);
        lblCustoEstimado.setEnabled(true);
        fldHorasEstimadas.setEnabled(true);
        lblHorasEstimadas.setEnabled(true);
        fldHorasReais.setEnabled(true);
        lblHorasReais.setEnabled(true);
        fldStatus.setEnabled(true);
        lblStatus.setEnabled(true);
        fldCompletado.setEnabled(true);
        lblCompletado.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblDataInicio.setEnabled(false);
        fldDataInicio.setEnabled(false);
        lblDataFim.setEnabled(false);
        fldDataFim.setEnabled(false);
        lblResponsavel.setEnabled(false);
        btnResponsavel.setEnabled(false);
        fldCustoReal.setEnabled(false);
        lblCustoReal.setEnabled(false);
        fldCustoEstimado.setEnabled(false);
        lblCustoEstimado.setEnabled(false);
        fldHorasEstimadas.setEnabled(false);
        lblHorasEstimadas.setEnabled(false);
        fldHorasReais.setEnabled(false);
        lblHorasReais.setEnabled(false);
        fldStatus.setEnabled(false);
        lblStatus.setEnabled(false);
        fldCompletado.setEnabled(false);
        lblCompletado.setEnabled(false);
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        fldDataInicio.setCalendar(record.getDate("DATAINI"));
        fldDataFim.setCalendar(record.getDate("DATAFIN"));

        fldCustoReal.setText(record.getString("CUSTOREAL"));
        fldCustoEstimado.setText(record.getString("CUSTOEST"));
        fldHorasEstimadas.setText(record.getString("HORASEST"));
        fldHorasReais.setText(record.getString("HORASREAL"));
        fldStatus.setText(record.getString("SITUACAO"));
        fldCompletado.setText(record.getString("COMPLETADO") + " % ");
        respId = record.getString("RESPID");
        tipoPessoa = record.getString("TIPOPESSOA");
        fldResponsavel.setText(record.getString("RESPONSAVEL"));

        listaDocumento.setDataSource(
                record.getBIXMLVector("INIDOCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaTarefa.setDataSource(
                record.getBIXMLVector("TAREFAS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);

        listaTarefa.btnNew.setVisible(false);
        if (record.getBoolean("ISRESP") || record.getBoolean("ADMINISTRADOR")) {
            listaTarefa.btnNew.setVisible(true);
        }
        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");

        //retira a altera��o se n�o for administrador ou respons�vel
        if (!record.getBoolean("ISRESP") && !record.getBoolean("ADMINISTRADOR")) {
            btnEdit.setVisible(false);
            btnDelete.setVisible(false);
        }

        pnlHeader.setComponents(vctContexto);
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        recordAux.set("CUSTOEST", record.getString("CUSTOEST"));
        recordAux.set("CUSTOREAL", record.getString("CUSTOREAL"));
        recordAux.set("HORASREAL", record.getString("HORASREAL"));
        recordAux.set("HORASEST", record.getString("HORASEST"));
        recordAux.set("SITUACAO", record.getString("SITUACAO"));
        recordAux.set("COMPLETADO", record.getString("COMPLETADO"));

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("DATAINI", fldDataInicio.getCalendar());
        recordAux.set("DATAFIN", fldDataFim.getCalendar());
        recordAux.set("RESPID", respId);
        recordAux.set("TIPOPESSOA", tipoPessoa);

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0) 
                && fldDataInicio.getText().trim().length() > 0
                && fldDataFim.getText().trim().length() > 0;

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return true;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblDescricao = new javax.swing.JLabel();
        lblDataInicio = new javax.swing.JLabel();
        lblDataFim = new javax.swing.JLabel();
        lblResponsavel = new javax.swing.JLabel();
        lblCompletado = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        lblHorasEstimadas = new javax.swing.JLabel();
        lblCustoEstimado = new javax.swing.JLabel();
        lblHorasReais = new javax.swing.JLabel();
        lblCustoReal = new javax.swing.JLabel();
        fldCompletado = new javax.swing.JLabel();
        fldCustoReal = new javax.swing.JLabel();
        fldHorasEstimadas = new javax.swing.JLabel();
        fldCustoEstimado = new javax.swing.JLabel();
        fldHorasReais = new javax.swing.JLabel();
        fldStatus = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        fldDataInicio = new pv.jfcx.JPVDatePlus();
        fldDataFim = new pv.jfcx.JPVDatePlus();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        pnlTopRecord = new javax.swing.JPanel();
        listaTarefa = new bsc.beans.JBIListPanel();
        listaDocumento = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscIniciativaFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_iniciativa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscIniciativaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscIniciativaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscIniciativaFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscIniciativaFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscIniciativaFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscIniciativaFrame_00007")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 10, 96, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setViewportView(txtDescricao);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(10, 70, 410, 60);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscIniciativaFrame_00008")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 50, 96, 20);

        lblDataInicio.setForeground(new java.awt.Color(51, 51, 255));
        lblDataInicio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataInicio.setText(bundle.getString("BscIniciativaFrame_00009")); // NOI18N
        lblDataInicio.setEnabled(false);
        pnlData.add(lblDataInicio);
        lblDataInicio.setBounds(10, 130, 96, 20);

        lblDataFim.setForeground(new java.awt.Color(51, 51, 255));
        lblDataFim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataFim.setText(bundle.getString("BscIniciativaFrame_00010")); // NOI18N
        lblDataFim.setEnabled(false);
        pnlData.add(lblDataFim);
        lblDataFim.setBounds(250, 130, 66, 20);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscIniciativaFrame_00011")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 170, 96, 20);

        lblCompletado.setText(bundle.getString("BscIniciativaFrame_00012")); // NOI18N
        pnlData.add(lblCompletado);
        lblCompletado.setBounds(10, 210, 96, 20);

        lblStatus.setText(bundle.getString("BscIniciativaFrame_00020")); // NOI18N
        pnlData.add(lblStatus);
        lblStatus.setBounds(230, 210, 74, 20);

        lblHorasEstimadas.setText(bundle.getString("BscIniciativaFrame_00013")); // NOI18N
        pnlData.add(lblHorasEstimadas);
        lblHorasEstimadas.setBounds(10, 250, 99, 20);

        lblCustoEstimado.setText(bundle.getString("BscIniciativaFrame_00014")); // NOI18N
        pnlData.add(lblCustoEstimado);
        lblCustoEstimado.setBounds(10, 290, 99, 20);

        lblHorasReais.setText(bundle.getString("BscIniciativaFrame_00015")); // NOI18N
        pnlData.add(lblHorasReais);
        lblHorasReais.setBounds(230, 250, 74, 20);

        lblCustoReal.setText(bundle.getString("BscIniciativaFrame_00016")); // NOI18N
        pnlData.add(lblCustoReal);
        lblCustoReal.setBounds(230, 290, 66, 20);

        fldCompletado.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldCompletado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fldCompletado.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldCompletado);
        fldCompletado.setBounds(10, 230, 180, 22);

        fldCustoReal.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldCustoReal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fldCustoReal.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldCustoReal);
        fldCustoReal.setBounds(230, 310, 180, 22);

        fldHorasEstimadas.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldHorasEstimadas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fldHorasEstimadas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldHorasEstimadas);
        fldHorasEstimadas.setBounds(10, 270, 180, 22);

        fldCustoEstimado.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldCustoEstimado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fldCustoEstimado.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldCustoEstimado);
        fldCustoEstimado.setBounds(10, 310, 180, 22);

        fldHorasReais.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldHorasReais.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fldHorasReais.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldHorasReais);
        fldHorasReais.setBounds(230, 270, 180, 22);

        fldStatus.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        fldStatus.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        pnlData.add(fldStatus);
        fldStatus.setBounds(230, 230, 180, 22);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 30, 410, 22);

        fldDataInicio.setDialog(true);
        fldDataInicio.setUseLocale(true);
        fldDataInicio.setWaitForCalendarDate(true);
        pnlData.add(fldDataInicio);
        fldDataInicio.setBounds(10, 150, 160, 22);

        fldDataFim.setDialog(true);
        fldDataFim.setUseLocale(true);
        fldDataFim.setWaitForCalendarDate(true);
        pnlData.add(fldDataFim);
        fldDataFim.setBounds(250, 150, 160, 22);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(10, 190, 400, 22);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(420, 190, 30, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscIniciativaFrame_00001"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIniciativaFrame_00017"), listaTarefa); // NOI18N
        tapCadastro.addTab(bundle.getString("BscIniciativaFrame_00018"), listaDocumento); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(50, 60));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlLeftForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
        java.util.Vector vector = new java.util.Vector();
        vector.add(tipoPessoa);
        vector.add(respId);
        vector.add(fldResponsavel.getText());
        String organizacao = record.getBIXMLVector("CONTEXTOS").get(record.getBIXMLVector("CONTEXTOS").size() - 1).getString("NOME");

        java.util.Vector vctResponsavel = new java.util.Vector();
        vctResponsavel.add(vector);
        bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog(this, event, organizacao, vctResponsavel);
        dialog.show();

        java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
        tipoPessoa = vctSelecionados.get(0).toString();
        respId = vctSelecionados.get(1).toString();
        fldResponsavel.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("CADASTRANDO_INICIATIVAS");
	}//GEN-LAST:event_btnReload1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel fldCompletado;
    private javax.swing.JLabel fldCustoEstimado;
    private javax.swing.JLabel fldCustoReal;
    private pv.jfcx.JPVDatePlus fldDataFim;
    private pv.jfcx.JPVDatePlus fldDataInicio;
    private javax.swing.JLabel fldHorasEstimadas;
    private javax.swing.JLabel fldHorasReais;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldResponsavel;
    private javax.swing.JLabel fldStatus;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblCompletado;
    private javax.swing.JLabel lblCustoEstimado;
    private javax.swing.JLabel lblCustoReal;
    private javax.swing.JLabel lblDataFim;
    private javax.swing.JLabel lblDataInicio;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblHorasEstimadas;
    private javax.swing.JLabel lblHorasReais;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblResponsavel;
    private javax.swing.JLabel lblStatus;
    private bsc.beans.JBIListPanel listaDocumento;
    private bsc.beans.JBIListPanel listaTarefa;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscIniciativaFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
