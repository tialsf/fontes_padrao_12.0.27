
package bsc.swing.framework;

public class BscMensagemFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("MENSAGEM");

	final static int BSC_MSG_ENVIADA = 1;
	final static int BSC_MSG_RECEBIDA = 2;
	final static int BSC_MSG_EXCLUIDA = 3;
	final static String BSC_MSG_ALTA = "1";
	final static String BSC_MSG_MEDIA = "2";
	final static String BSC_MSG_BAIXA = "3";
	final static int BSC_MSG_REPLY = 1;
	final static int BSC_MSG_REPLYALL = 2;
	final static int BSC_MSG_FORWARD = 3;
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblDe.setEnabled(true);
		cbbDe.setEnabled(true);
		lblAssunto.setEnabled(true);
		fldAssunto.setEnabled(true);
		lblMensagem.setEnabled(true);
		txtMensagem.setEnabled(true);
		lblPrioridade.setEnabled(true);
		rdbBaixa.setEnabled(true);
		rdbMedia.setEnabled(true);
		rdbAlta.setEnabled(true);
		listaPara.setEnabled(true);
		listaCC.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblDe.setEnabled(false);
		cbbDe.setEnabled(false);
		lblAssunto.setEnabled(false);
		fldAssunto.setEnabled(false);
		lblMensagem.setEnabled(false);
		txtMensagem.setEnabled(false);
		lblPrioridade.setEnabled(false);
		rdbBaixa.setEnabled(false);
		rdbMedia.setEnabled(false);
		rdbAlta.setEnabled(false);
		listaPara.setEnabled(false);
		listaCC.setEnabled(false);
	}
	
	java.util.Hashtable htbRemetente = new java.util.Hashtable();
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		
		fldAssunto.setText(record.getString("NOME"));
		txtMensagem.setText(record.getString("TEXTO"));
		rdbBaixa.setSelected(record.getString("PRIORIDADE").equals(BSC_MSG_BAIXA));
		rdbMedia.setSelected(record.getString("PRIORIDADE").equals(BSC_MSG_MEDIA));
		rdbAlta.setSelected(record.getString("PRIORIDADE").equals(BSC_MSG_ALTA));
		
		event.populateCombo(record.getBIXMLVector("REMETENTES"), "PARENTID", htbRemetente, cbbDe);
		
		listaPara.setDataSource( record.getBIXMLVector("PESSOAS"), record.getBIXMLVector("PARAS"), "PARAS", "PESSOA" );
		listaCC.setDataSource( record.getBIXMLVector("PESSOAS"), record.getBIXMLVector("CCS"), "CCS", "PESSOA" );
		
		if(record.getInt("PASTA")==BSC_MSG_ENVIADA || record.getInt("PASTA")==BSC_MSG_EXCLUIDA){
			btnReply.setVisible(false);
			btnReplyAll.setVisible(false);
		}
		
		if(status != event.INSERTING){
			//Tratamento para abrir os links.
			String strLink = record.getString("ANEXO");
			strLink = strLink.trim().endsWith(";") ? strLink : strLink.concat(";") ;
			String[] result = strLink.split(";");
			
			for(int iMenu = 0; iMenu < result.length ; iMenu++){
				String tmpLink = new String(result[iMenu]);
				if(tmpLink.trim().length()>0){
					javax.swing.JMenuItem jMnuTmpAnexo = new javax.swing.JMenuItem();
					jMnuPopAnexo.add(jMnuTmpAnexo);
					jMnuTmpAnexo.setText(tmpLink);
					jMnuTmpAnexo.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent evt) {
							javax.swing.JMenuItem mnuClique = (javax.swing.JMenuItem)evt.getSource();
							openUrl(mnuClique.getText());
						}
					});
				}
			}
		}
		
		
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("CONTEXTID") ) {
                    recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
                }
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		
		recordAux.set( "PARENTID", event.getComboValue(htbRemetente , cbbDe));
		recordAux.set( "NOME", fldAssunto.getText() );
		recordAux.set( "TEXTO", txtMensagem.getText() );
		if(rdbBaixa.isSelected()){
			recordAux.set( "PRIORIDADE", BSC_MSG_BAIXA);
		} else if(rdbMedia.isSelected()) {
			recordAux.set( "PRIORIDADE", BSC_MSG_MEDIA);
		} else {
			recordAux.set( "PRIORIDADE", BSC_MSG_ALTA);
		}
		
		recordAux.set( listaPara.getXMLData() );
		recordAux.set( listaCC.getXMLData() );
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
            boolean retorno = true;
            
            // Valida o preenchimento dos campos obrigat�rios.
            if ( cbbDe.getSelectedIndex() == 0 ) {
                errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMensagemFrame_00028"));
                retorno = false;
            } else if (fldAssunto.getText().trim().length() == 0) {
                errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMensagemFrame_00029"));
                retorno = false;
            } else if (txtMensagem.getText().trim().length() == 0) {
                errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMensagemFrame_00030"));
                retorno = false;
            } else if (listaPara.getRowCount()== 0 && listaCC.getRowCount() == 0){
                errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMensagemFrame_00026"));
                retorno = false;
            } else {
                retorno = true;
            }
            
            // Se o formul�rio n�o estiver OK, habilita novamente o bot�o Salvar.
            if (!retorno) {
                btnSave.setEnabled(true);
            }

            return retorno;
	}
	
	public boolean hasCombos() {
		return true;
	}
	
	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpPrioridade = new javax.swing.ButtonGroup();
        jMnuPopAnexo = new javax.swing.JPopupMenu();
        jMnuItemAnexo = new javax.swing.JMenuItem();
        jSeparador1 = new javax.swing.JSeparator();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnReply = new javax.swing.JButton();
        btnReplyAll = new javax.swing.JButton();
        btnForward = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblDe = new javax.swing.JLabel();
        cbbDe = new javax.swing.JComboBox();
        lblAssunto = new javax.swing.JLabel();
        fldAssunto = new javax.swing.JTextField();
        Mensagem = new javax.swing.JScrollPane();
        txtMensagem = new bsc.beans.JBITextArea();
        lblMensagem = new javax.swing.JLabel();
        lblPrioridade = new javax.swing.JLabel();
        rdbBaixa = new javax.swing.JRadioButton();
        rdbMedia = new javax.swing.JRadioButton();
        rdbAlta = new javax.swing.JRadioButton();
        listaPara = new bsc.beans.JBISelectionPanel();
        listaCC = new bsc.beans.JBISelectionPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        jMnuItemAnexo.setText(bundle.getString("BscMensagemFrame_00025")); // NOI18N
        jMnuItemAnexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuItemAnexoActionPerformed(evt);
            }
        });
        jMnuPopAnexo.add(jMnuItemAnexo);
        jMnuPopAnexo.add(jSeparador1);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(bundle.getString("BscMensagemFrame_00006")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_tarefa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 22));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 22));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 22));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscMensagemFrame_00004")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscMensagemFrame_00005")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(510, 22));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnReply.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReply.setText(bundle.getString("BscMensagemFrame_00001")); // NOI18N
        btnReply.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReply.setMaximumSize(new java.awt.Dimension(120, 20));
        btnReply.setMinimumSize(new java.awt.Dimension(100, 20));
        btnReply.setPreferredSize(new java.awt.Dimension(67, 22));
        btnReply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReplyActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReply);

        btnReplyAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReplyAll.setText(bundle.getString("BscMensagemFrame_00002")); // NOI18N
        btnReplyAll.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReplyAll.setMaximumSize(new java.awt.Dimension(120, 20));
        btnReplyAll.setMinimumSize(new java.awt.Dimension(100, 20));
        btnReplyAll.setPreferredSize(new java.awt.Dimension(67, 22));
        btnReplyAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReplyAllActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReplyAll);

        btnForward.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnForward.setText(bundle.getString("BscMensagemFrame_00003")); // NOI18N
        btnForward.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnForward.setMaximumSize(new java.awt.Dimension(120, 20));
        btnForward.setMinimumSize(new java.awt.Dimension(100, 20));
        btnForward.setPreferredSize(new java.awt.Dimension(67, 22));
        btnForward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnForwardActionPerformed(evt);
            }
        });
        tbInsertion.add(btnForward);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscEstrategiaFrame_00004")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(120, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(100, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(400, 280));

        pnlData.setPreferredSize(new java.awt.Dimension(460, 280));
        pnlData.setLayout(null);

        lblDe.setForeground(new java.awt.Color(51, 51, 255));
        lblDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDe.setText(bundle.getString("BscMensagemFrame_00010")); // NOI18N
        lblDe.setEnabled(false);
        pnlData.add(lblDe);
        lblDe.setBounds(10, 10, 71, 20);

        cbbDe.setPreferredSize(new java.awt.Dimension(24, 22));
        pnlData.add(cbbDe);
        cbbDe.setBounds(10, 32, 400, 20);

        lblAssunto.setForeground(new java.awt.Color(51, 51, 255));
        lblAssunto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAssunto.setText(bundle.getString("BscMensagemFrame_00011")); // NOI18N
        lblAssunto.setEnabled(false);
        pnlData.add(lblAssunto);
        lblAssunto.setBounds(10, 50, 71, 20);
        pnlData.add(fldAssunto);
        fldAssunto.setBounds(10, 70, 400, 22);

        Mensagem.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        Mensagem.setViewportView(txtMensagem);

        pnlData.add(Mensagem);
        Mensagem.setBounds(10, 110, 400, 170);

        lblMensagem.setForeground(new java.awt.Color(51, 51, 255));
        lblMensagem.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMensagem.setText(bundle.getString("BscMensagemFrame_00012")); // NOI18N
        lblMensagem.setEnabled(false);
        pnlData.add(lblMensagem);
        lblMensagem.setBounds(10, 90, 71, 20);

        lblPrioridade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPrioridade.setText(bundle.getString("BscMensagemFrame_00013")); // NOI18N
        lblPrioridade.setEnabled(false);
        pnlData.add(lblPrioridade);
        lblPrioridade.setBounds(10, 290, 71, 16);

        grpPrioridade.add(rdbBaixa);
        rdbBaixa.setText(bundle.getString("BscMensagemFrame_00014")); // NOI18N
        rdbBaixa.setEnabled(false);
        pnlData.add(rdbBaixa);
        rdbBaixa.setBounds(80, 290, 90, 23);

        grpPrioridade.add(rdbMedia);
        rdbMedia.setSelected(true);
        rdbMedia.setText(bundle.getString("BscMensagemFrame_00015")); // NOI18N
        rdbMedia.setEnabled(false);
        pnlData.add(rdbMedia);
        rdbMedia.setBounds(170, 290, 90, 23);

        grpPrioridade.add(rdbAlta);
        rdbAlta.setText(bundle.getString("BscMensagemFrame_00016")); // NOI18N
        rdbAlta.setEnabled(false);
        rdbAlta.setMaximumSize(new java.awt.Dimension(51, 23));
        pnlData.add(rdbAlta);
        rdbAlta.setBounds(260, 290, 90, 23);

        scpRecord.setViewportView(pnlData);

        jTabbedPane1.addTab(bundle.getString("BscMensagemFrame_00007"), scpRecord); // NOI18N
        scpRecord.getAccessibleContext().setAccessibleName("Mensagem");

        jTabbedPane1.addTab(bundle.getString("BscMensagemFrame_00008"), listaPara); // NOI18N
        listaPara.getAccessibleContext().setAccessibleName("Para");

        jTabbedPane1.addTab(bundle.getString("BscMensagemFrame_00009"), listaCC); // NOI18N
        listaCC.getAccessibleContext().setAccessibleName("CC");

        pnlFields.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscMensagemFrame_00006"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents
		
	private void btnReplyAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReplyAllActionPerformed
		bsc.swing.BscDefaultFrameFunctions frmDestino =  bsc.core.BscStaticReferences.getBscFormController().newDetailForm( "MENSAGEM", "0", "0");
		record.set("OPERACAO", BSC_MSG_REPLYALL);
		frmDestino.setRecord(record);
	}//GEN-LAST:event_btnReplyAllActionPerformed
	
	private void btnForwardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForwardActionPerformed
		bsc.swing.BscDefaultFrameFunctions frmDestino =  bsc.core.BscStaticReferences.getBscFormController().newDetailForm( "MENSAGEM", "0", "0");
		record.set("OPERACAO", BSC_MSG_FORWARD);
		frmDestino.setRecord(record);
	}//GEN-LAST:event_btnForwardActionPerformed
	
	private void btnReplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReplyActionPerformed
		bsc.swing.BscDefaultFrameFunctions frmDestino =  bsc.core.BscStaticReferences.getBscFormController().newDetailForm( "MENSAGEM", "0", "0");
		record.set("OPERACAO", BSC_MSG_REPLY);
		frmDestino.setRecord(record);
	}//GEN-LAST:event_btnReplyActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            btnSave.setEnabled(false);
            // Executa a valida��o do formul�rio
            boolean lValid = validateFields(new StringBuffer());            
        
            if (lValid) {
                record.set( "EMAIL", false);
                bsc.swing.BscDefaultDialogSystem dialogSystem = bsc.core.BscStaticReferences.getBscDialogSystem();
                // Deseja notificar por email?
                if(dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMensagemFrame_00027")) == 
                        javax.swing.JOptionPane.YES_OPTION){
                    record.set( "EMAIL", true);
                }
            }

            event.saveRecord();

            if( lValid) {
                event.closeOperation();
            }
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		event.deleteRecord();
		bsc.beans.JBIMessagePanel parentFrame = (bsc.beans.JBIMessagePanel)this.getClientProperty("FORM_PARENT");
		if(parentFrame != null){
			bsc.swing.BscDefaultFrameBehavior parentFrameBehavior = parentFrame.getFrame();
			parentFrameBehavior.loadRecord();
		}
		//event.closeOperation();
	}//GEN-LAST:event_btnDeleteActionPerformed

    private void jMnuItemAnexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuItemAnexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMnuItemAnexoActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane Mensagem;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnForward;
    private javax.swing.JButton btnReply;
    private javax.swing.JButton btnReplyAll;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbDe;
    private javax.swing.JTextField fldAssunto;
    private javax.swing.ButtonGroup grpPrioridade;
    private javax.swing.JMenuItem jMnuItemAnexo;
    private javax.swing.JPopupMenu jMnuPopAnexo;
    private javax.swing.JSeparator jSeparador1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblAssunto;
    private javax.swing.JLabel lblDe;
    private javax.swing.JLabel lblMensagem;
    private javax.swing.JLabel lblPrioridade;
    private bsc.beans.JBISelectionPanel listaCC;
    private bsc.beans.JBISelectionPanel listaPara;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JRadioButton rdbAlta;
    private javax.swing.JRadioButton rdbBaixa;
    private javax.swing.JRadioButton rdbMedia;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtMensagem;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	private bsc.xml.BIXMLRecord recAux = null;
	
	private void openUrl(String strUrl){
	    bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
	    try{
			java.net.URL url = new java.net.URL(strUrl);
			bscApplet.getAppletContext().showDocument(url,"_blank");					
		} catch (Exception e ){
			System.out.println("O link n�o pode ser aberto: \n" + e);
		}
	}
	
	public BscMensagemFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		if(recordAux.contains("OPERACAO")){
			record = recordAux;
			
			int iOperacao = recordAux.getInt("OPERACAO");
			int iPasta = recordAux.getInt("PASTA");
			int iItens = 0;
			
			bsc.xml.BIXMLVector vectorPara = record.getBIXMLVector("PARAS").clone2();
			bsc.xml.BIXMLVector vectorCC = record.getBIXMLVector("CCS").clone2();
			bsc.xml.BIXMLVector vectorRemetente = record.getBIXMLVector("REMETENTES").clone2();
			bsc.xml.BIXMLVector vectorPessoas = record.getBIXMLVector("PESSOAS").clone2();
			bsc.xml.BIXMLVector vectorRem = record.getBIXMLVector("REMS").clone2();
			bsc.xml.BIXMLVector vectorDest = record.getBIXMLVector("DESTS").clone2();
			
			if(iPasta==BSC_MSG_ENVIADA && iOperacao==BSC_MSG_FORWARD) {
				
				record.set("NOME","Enc: " + record.getString("NOME"));
				
				//Extrai todos os record do vector "PARAS"
				iItens = record.getBIXMLVector("PARAS").size();
				for(int i = 0; i < iItens ; i++)
					record.getBIXMLVector("PARAS").removeRecord(0);
				
				//Extrai todos os record do vector "CCS"
				iItens = record.getBIXMLVector("CCS").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("CCS").removeRecord(0);
				
				//Extrai todos os record do vector "PESSOAS"
				iItens = record.getBIXMLVector("PESSOAS").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("PESSOAS").removeRecord(0);
				
				//Adiciona destinatario do vector "DEST"
				for(int i = 0; i < vectorDest.size(); i++)
					record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
				
			} else if(iPasta==BSC_MSG_RECEBIDA) {
				
				//Extrai todos os record do vector "REMETENTES"
				iItens = record.getBIXMLVector("REMETENTES").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("REMETENTES").removeRecord(0);
				
				//Adiciona destinatario do vector "REMS"
				for(int i = 0; i < vectorRem.size(); i++)
					record.getBIXMLVector("REMETENTES").add(vectorRem.get(i));
				
				//Extrai todos os record do vector "PARAS"
				iItens = record.getBIXMLVector("PARAS").size();
				for(int i = 0; i < iItens ; i++)
					record.getBIXMLVector("PARAS").removeRecord(0);
				
				//Extrai todos os record do vector "CCS"
				iItens = record.getBIXMLVector("CCS").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("CCS").removeRecord(0);
				
				//Extrai todos os record do vector "PESSOAS"
				iItens = record.getBIXMLVector("PESSOAS").size();
				for(int i = 0; i < iItens ; i++)
					record.getBIXMLVector("PESSOAS").removeRecord(0);
				
				//Adiciona lista de destinatarios na lista de "PESSOAS"
				for(int i = 0; i < vectorDest.size(); i++)
					record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
				
				if(iOperacao==BSC_MSG_REPLY) {
					
					record.set("NOME","Resp: " + record.getString("NOME"));
					
					//Adiciona o remetente na lista "PARAS"
					for(int i = 0; i < vectorRemetente.size(); i++)
						record.getBIXMLVector("PARAS").add(vectorRemetente.get(i));
					
				} else if (iOperacao==BSC_MSG_REPLYALL) {
					
					record.set("NOME","Resp: " + record.getString("NOME"));
					
					//Adiciona o remetente na lista "PARAS"
					for(int i = 0; i < vectorRemetente.size(); i++)
						record.getBIXMLVector("PARAS").add(vectorRemetente.get(i));
					
					//Adiciona as pessoas da lista "PESSOAS" lista "PARAS"
					for(int i = 0; i < vectorPessoas.size(); i++)
						if(!vectorPessoas.get(i).getString("USERID").equals(record.getBIXMLVector("REMETENTES").get(0).getString("USERID")))
							record.getBIXMLVector("PARAS").add(vectorPessoas.get(i));
					
				} else if (iOperacao==BSC_MSG_FORWARD) {
					
					record.set("NOME","Enc: " + record.getString("NOME"));
					
				}
			} else if(iPasta==BSC_MSG_EXCLUIDA) {
				
				record.set("NOME","Enc: " + record.getString("NOME"));
				
				//Extrai todos os record do vector "REMETENTES"
				iItens = record.getBIXMLVector("REMETENTES").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("REMETENTES").removeRecord(0);
				
				//Adiciona destinatario do vector "REMS"
				for(int i = 0; i < vectorRem.size(); i++)
					record.getBIXMLVector("REMETENTES").add(vectorRem.get(i));
				
				//Extrai todos os record do vector "PESSOAS"
				iItens = record.getBIXMLVector("PESSOAS").size();
				for(int i = 0; i < iItens ; i++)
					record.getBIXMLVector("PESSOAS").removeRecord(0);
				
				//Adiciona lista de destinatarios na lista de "PESSOAS"
				for(int i = 0; i < vectorDest.size(); i++)
					record.getBIXMLVector("PESSOAS").add(vectorDest.get(i));
				
				//Extrai todos os record do vector "PARAS"
				iItens = record.getBIXMLVector("PARAS").size();
				for(int i = 0; i < iItens ; i++)
					record.getBIXMLVector("PARAS").removeRecord(0);
				
				//Extrai todos os record do vector "CCS"
				iItens = record.getBIXMLVector("CCS").size();
				for(int i = 0; i < iItens; i++)
					record.getBIXMLVector("CCS").removeRecord(0);
				
			}
			
			enableFields();
			refreshFields();
			
			btnReply.setVisible(false);
			
		} else {
			record = recordAux;
		}
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") ) {
                    return String.valueOf( record.getString("PARENTID") );
                } else {
                return new String("");
                }
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}
	
	
}
