package bsc.swing.framework;

import bsc.core.BscStaticReferences;
import bsc.swing.BscDefaultDialogSystem;
import bsc.swing.BscDefaultFrameBehavior;
import java.util.*;

public class BscOrganizacaoFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, bsc.beans.JBIListPanelListener {

    /***************************************************************************/
    // FUN��ES QUE PRexcECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("ORGANIZACAO");
    private Vector vctAgenda = new Vector();
    private Hashtable htbAgenda = new Hashtable();
    private int idCopyParent = 0;
    private String contextId;

    @Override
    public void enableFields() {

        // Habilita os campos do formul�rio.
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        lblMissao.setEnabled(true);
        txtMissao.setEnabled(true);
        lblVisao.setEnabled(true);
        txtVisao.setEnabled(true);
        txtQualidade.setEnabled(true);
        lblNotas.setEnabled(true);
        txtNotas.setEnabled(true);
        lblPolitica1.setEnabled(true);
        lblPolitica2.setEnabled(true);
        lblValores.setEnabled(true);
        txtValores.setEnabled(true);
        lblEndereco.setEnabled(true);
        fldEndereco.setEnabled(true);
        lblCidade.setEnabled(true);
        fldCidade.setEnabled(true);
        lblEstado.setEnabled(true);
        fldEstado.setEnabled(true);
        lblPais.setEnabled(true);
        fldPais.setEnabled(true);
        lblTelefone.setEnabled(true);
        fldTelefone.setEnabled(true);
        lblEmail.setEnabled(true);
        fldEmail.setEnabled(true);
        lblPaginaWeb.setEnabled(true);
        fldPaginaWeb.setEnabled(true);
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        tapCadastro.repaint();
    }

    @Override
    public void disableFields() {
        // Desabilita os campos do formul�rio.
        txtNotas.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        lblMissao.setEnabled(false);
        txtMissao.setEnabled(false);
        lblVisao.setEnabled(false);
        txtVisao.setEnabled(false);
        txtQualidade.setEnabled(false);
        lblNotas.setEnabled(false);
        lblPolitica1.setEnabled(false);
        lblPolitica2.setEnabled(false);
        lblValores.setEnabled(false);
        txtValores.setEnabled(false);
        lblEndereco.setEnabled(false);
        fldEndereco.setEnabled(false);
        lblCidade.setEnabled(false);
        fldCidade.setEnabled(false);
        lblEstado.setEnabled(false);
        fldEstado.setEnabled(false);
        lblPais.setEnabled(false);
        fldPais.setEnabled(false);
        lblTelefone.setEnabled(false);
        fldTelefone.setEnabled(false);
        lblEmail.setEnabled(false);
        fldEmail.setEnabled(false);
        lblPaginaWeb.setEnabled(false);
        fldPaginaWeb.setEnabled(false);
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        tapCadastro.repaint();

        /*Se for duplica��o remove a barra de opera��es.*/
        if (isDuplicate()) {
            pnlOperation.setVisible(false);
        }
    }

    @Override
    public void refreshFields() {

        /*Se n�o for duplica��o de organizacao.*/
        if (!isDuplicate()) {
            // Copia os dados da vari�vel "record" para os campos do formul�rio.
            fldNome.setText(record.getString("NOME"));
            tapCadastro.repaint();
            txtDescricao.setText(record.getString("DESCRICAO"));
            txtMissao.setText(record.getString("MISSAO"));
            txtVisao.setText(record.getString("VISAO"));
            txtNotas.setText(record.getString("NOTAS"));
            txtQualidade.setText(record.getString("QUALIDADE"));
            txtValores.setText(record.getString("VALORES"));
            fldEndereco.setText(record.getString("ENDERECO"));
            fldCidade.setText(record.getString("CIDADE"));
            fldEstado.setText(record.getString("ESTADO"));
            fldPais.setText(record.getString("PAIS"));
            fldTelefone.setText(record.getString("FONE"));
            fldEmail.setText(record.getString("EMAIL"));
            fldPaginaWeb.setText(record.getString("PAGINA"));


            strategyList.setDataSource(
                    record.getBIXMLVector("ESTRATEGIAS"),
                    record.getString("ID"), "0", this.event);

            listaPessoas.setDataSource(
                    record.getBIXMLVector("PESSOAS"),
                    record.getString("ID"), "0", this.event);
            listaGrupoPessoas.setDataSource(
                    record.getBIXMLVector("PGRUPOS"),
                    record.getString("ID"), "0", this.event);
            listaReunioes.setDataSource(
                    record.getBIXMLVector("REUNIOES"),
                    record.getString("ID"), "0", this.event);

            bsc.xml.BIXMLVector vctElemento = record.getBIXMLVector("REUNIOES");

            /*Lista de reuni�es.*/
            if (status != BscDefaultFrameBehavior.INSERTING) {
                Date novaData = new Date();
                String horaIni, horaFim, strData;
                int minutosIni, minutosFim, horaInicial, horaFinal, horarioIniEmMinutos, horarioFimEmMinutos, nPos;
                vctAgenda.removeAllElements();
                htbAgenda.clear();
                dia.removeAll();
                int intTamanho = dia.getAppointments().size();
                for (int i = intTamanho; i > 0; i--) {
                    dia.delete(i - 1);
                }
                calendario.removeAll();
                for (int intItem = 0; intItem < vctElemento.size(); intItem++) {
                    novaData = new Date(vctElemento.get(intItem).getDate("DATAREU").getTimeInMillis());
                    strData = new String(vctElemento.get(intItem).getString("DATAREU"));
                    horaIni = vctElemento.get(intItem).getString("HORAINI").trim();
                    nPos = horaIni.indexOf(":");
                    minutosIni = Integer.parseInt(horaIni.substring(nPos + 1));
                    horaInicial = Integer.parseInt(horaIni.substring(0, nPos));
                    horarioIniEmMinutos = (int) (horaInicial * 60) + minutosIni;

                    int nAno = java.lang.Integer.parseInt(strData.substring(6, 10));
                    int nMes = java.lang.Integer.parseInt(strData.substring(3, 5));
                    int nDia = java.lang.Integer.parseInt(strData.substring(0, 2));

                    GregorianCalendar dIni = new GregorianCalendar(nAno, nMes - 1, nDia, horaInicial, minutosIni);

                    horaFim = vctElemento.get(intItem).getString("HORAFIM").trim();
                    nPos = horaFim.indexOf(":");
                    minutosFim = Integer.parseInt(horaFim.substring(nPos + 1));
                    horaFinal = Integer.parseInt(horaFim.substring(0, nPos));
                    horarioFimEmMinutos = (int) (horaFinal * 60) + minutosFim;
                    String assunto = vctElemento.get(intItem).getString("NOME");

                    calendario.setSelDate(novaData);

                    novaData = new Date(dIni.getTimeInMillis());
                    dia.setDate(novaData);
                    // o calculo da diferen�a se refere a dura��o do compromisso, que deve ser em Minutos
                    pv.util.Appointment agenda = dia.add(novaData, assunto, horarioFimEmMinutos - horarioIniEmMinutos, -1, true);
                    vctAgenda.addElement(agenda.getDate());
                    htbAgenda.put(calendario.getDate(), new Integer(intItem));

                }
            }
            // Se for duplica��o de organizacao.
        } else {
            BscDefaultDialogSystem oMsg = BscStaticReferences.getBscDialogSystem();
            oMsg.informationMessage("A duplica��o da organiza��o foi conclu�da.");

            doDefaultCloseAction();
        }
    }

    @Override
    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID")); // Setar contexto.
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("MISSAO", txtMissao.getText());
        recordAux.set("VISAO", txtVisao.getText());
        recordAux.set("NOTAS", txtNotas.getText());
        recordAux.set("QUALIDADE", txtQualidade.getText());
        recordAux.set("VALORES", txtValores.getText());
        recordAux.set("ENDERECO", fldEndereco.getText());
        recordAux.set("CIDADE", fldCidade.getText());
        recordAux.set("ESTADO", fldEstado.getText());
        recordAux.set("PAIS", fldPais.getText());
        recordAux.set("FONE", fldTelefone.getText());
        recordAux.set("EMAIL", fldEmail.getText());
        recordAux.set("PAGINA", fldPaginaWeb.getText());
        recordAux.set("COPY_PARENT", String.valueOf(this.getIdCopyParent()));

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    @Override
    public boolean hasCombos() {
        return false;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jBtnCopy = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlData = new javax.swing.JPanel();
        tapOrganizationData = new javax.swing.JTabbedPane();
        scrDadosEssenciais = new javax.swing.JScrollPane();
        pnlEssencial = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        pnlDescricao = new javax.swing.JPanel();
        lblDescricao = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblMissao = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtMissao = new bsc.beans.JBITextArea();
        lblVisao = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtVisao = new bsc.beans.JBITextArea();
        lblNotas = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtNotas = new bsc.beans.JBITextArea();
        lblPolitica1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtQualidade = new bsc.beans.JBITextArea();
        lblValores = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtValores = new bsc.beans.JBITextArea();
        lblPolitica2 = new javax.swing.JLabel();
        scrDadosComplementares = new javax.swing.JScrollPane();
        pnlComplementar = new javax.swing.JPanel();
        lblEndereco = new javax.swing.JLabel();
        lblCidade = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblPais = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        lblPaginaWeb = new javax.swing.JLabel();
        fldEndereco = new pv.jfcx.JPVEdit();
        fldCidade = new pv.jfcx.JPVEdit();
        fldEstado = new pv.jfcx.JPVEdit();
        fldPais = new pv.jfcx.JPVEdit();
        fldTelefone = new pv.jfcx.JPVMask();
        fldEmail = new pv.jfcx.JPVEdit();
        fldPaginaWeb = new pv.jfcx.JPVEdit();
        strategyList = new bsc.beans.JBIListPanel();
        listaPessoas = new bsc.beans.JBIListPanel();
        listaGrupoPessoas = new bsc.beans.JBIListPanel();
        pnlPrincipal = new javax.swing.JPanel();
        jpnlData = new javax.swing.JPanel();
        listaReunioes = new bsc.beans.JBIListPanel();
        pnlCalendario = new javax.swing.JPanel();
        calendario = new pv.jfcx.JPVCalendar();
        dia = new pv.jfcx.JPVDay();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();
        pnlTopForm1 = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscOrganizacaoFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_organizacao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscOrganizacaoFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 26));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 26));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 26));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscOrganizacaoFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 26));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 26));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 26));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(400, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(400, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(400, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(400, 0));
        tbInsertion.setMinimumSize(new java.awt.Dimension(400, 0));
        tbInsertion.setPreferredSize(new java.awt.Dimension(400, 0));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscOrganizacaoFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 26));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 26));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 26));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscOrganizacaoFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 26));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 26));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 26));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        jBtnCopy.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jBtnCopy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_copy.gif"))); // NOI18N
        jBtnCopy.setText(bundle.getString("BscOrganizacaoFrame_00031")); // NOI18N
        jBtnCopy.setToolTipText(bundle.getString("BscOrganizacaoFrame_00030")); // NOI18N
        jBtnCopy.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnCopy.setFocusable(false);
        jBtnCopy.setMaximumSize(new java.awt.Dimension(76, 26));
        jBtnCopy.setMinimumSize(new java.awt.Dimension(76, 26));
        jBtnCopy.setPreferredSize(new java.awt.Dimension(76, 26));
        jBtnCopy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBtnCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCopyActionPerformed(evt);
            }
        });
        tbInsertion.add(jBtnCopy);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscOrganizacaoFrame_00006")); // NOI18N
        btnReload.setAutoscrolls(true);
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 26));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 26));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 26));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 26));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 26));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 26));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());
        pnlTopRecord.add(pnlTopForm, java.awt.BorderLayout.CENTER);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlData.setPreferredSize(new java.awt.Dimension(300, 300));
        pnlData.setLayout(new java.awt.BorderLayout());

        tapOrganizationData.setPreferredSize(new java.awt.Dimension(100, 100));

        scrDadosEssenciais.setBorder(null);
        scrDadosEssenciais.setPreferredSize(new java.awt.Dimension(50, 281));

        pnlEssencial.setAutoscrolls(true);
        pnlEssencial.setPreferredSize(new java.awt.Dimension(500, 500));
        pnlEssencial.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscOrganizacaoFrame_00007")); // NOI18N
        lblNome.setEnabled(false);
        pnlEssencial.add(lblNome);
        lblNome.setBounds(10, 20, 78, 20);

        fldNome.setEnabled(false);
        fldNome.setMaxLength(60);
        pnlEssencial.add(fldNome);
        fldNome.setBounds(10, 40, 400, 22);

        pnlDescricao.setLayout(null);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscOrganizacaoFrame_00008")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlDescricao.add(lblDescricao);
        lblDescricao.setBounds(10, 0, 78, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setViewportView(txtDescricao);

        pnlDescricao.add(jScrollPane3);
        jScrollPane3.setBounds(10, 20, 400, 60);

        lblMissao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMissao.setText(bundle.getString("BscOrganizacaoFrame_00009")); // NOI18N
        lblMissao.setEnabled(false);
        pnlDescricao.add(lblMissao);
        lblMissao.setBounds(10, 80, 78, 20);

        jScrollPane4.setViewportView(txtMissao);

        pnlDescricao.add(jScrollPane4);
        jScrollPane4.setBounds(10, 100, 400, 55);

        lblVisao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblVisao.setText(bundle.getString("BscOrganizacaoFrame_00010")); // NOI18N
        lblVisao.setEnabled(false);
        pnlDescricao.add(lblVisao);
        lblVisao.setBounds(10, 150, 78, 20);

        jScrollPane5.setViewportView(txtVisao);

        pnlDescricao.add(jScrollPane5);
        jScrollPane5.setBounds(10, 170, 400, 55);

        lblNotas.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNotas.setText(bundle.getString("BscOrganizacaoFrame_00011")); // NOI18N
        lblNotas.setEnabled(false);
        pnlDescricao.add(lblNotas);
        lblNotas.setBounds(10, 220, 78, 20);

        jScrollPane1.setViewportView(txtNotas);

        pnlDescricao.add(jScrollPane1);
        jScrollPane1.setBounds(10, 240, 400, 55);

        lblPolitica1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPolitica1.setText(bundle.getString("BscOrganizacaoFrame_00022")); // NOI18N
        lblPolitica1.setEnabled(false);
        pnlDescricao.add(lblPolitica1);
        lblPolitica1.setBounds(10, 290, 78, 20);

        jScrollPane6.setViewportView(txtQualidade);

        pnlDescricao.add(jScrollPane6);
        jScrollPane6.setBounds(10, 310, 400, 55);

        lblValores.setText(bundle.getString("BscOrganizacaoFrame_00023")); // NOI18N
        lblValores.setEnabled(false);
        pnlDescricao.add(lblValores);
        lblValores.setBounds(10, 360, 78, 20);

        jScrollPane7.setViewportView(txtValores);

        pnlDescricao.add(jScrollPane7);
        jScrollPane7.setBounds(10, 380, 400, 55);

        lblPolitica2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPolitica2.setText(bundle.getString("BscOrganizacaoFrame_00024")); // NOI18N
        lblPolitica2.setEnabled(false);
        pnlDescricao.add(lblPolitica2);
        lblPolitica2.setBounds(60, 290, 78, 20);

        pnlEssencial.add(pnlDescricao);
        pnlDescricao.setBounds(0, 64, 420, 440);

        scrDadosEssenciais.setViewportView(pnlEssencial);

        tapOrganizationData.addTab(bundle.getString("BscOrganizacaoFrame_00013"), scrDadosEssenciais); // NOI18N

        scrDadosComplementares.setBorder(null);
        scrDadosComplementares.setPreferredSize(new java.awt.Dimension(50, 235));

        pnlComplementar.setPreferredSize(new java.awt.Dimension(50, 235));
        pnlComplementar.setLayout(null);

        lblEndereco.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEndereco.setText(bundle.getString("BscOrganizacaoFrame_00012")); // NOI18N
        lblEndereco.setEnabled(false);
        lblEndereco.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEndereco.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEndereco.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblEndereco);
        lblEndereco.setBounds(20, 20, 95, 16);

        lblCidade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCidade.setText(bundle.getString("BscOrganizacaoFrame_00014")); // NOI18N
        lblCidade.setEnabled(false);
        lblCidade.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCidade.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCidade.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblCidade);
        lblCidade.setBounds(20, 60, 80, 20);

        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEstado.setText(bundle.getString("BscOrganizacaoFrame_00015")); // NOI18N
        lblEstado.setEnabled(false);
        lblEstado.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEstado.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEstado.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblEstado);
        lblEstado.setBounds(20, 100, 95, 20);

        lblPais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPais.setText(bundle.getString("BscOrganizacaoFrame_00016")); // NOI18N
        lblPais.setEnabled(false);
        lblPais.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPais.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPais.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblPais);
        lblPais.setBounds(230, 100, 95, 22);

        lblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTelefone.setText(bundle.getString("BscOrganizacaoFrame_00021")); // NOI18N
        lblTelefone.setEnabled(false);
        lblTelefone.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblTelefone);
        lblTelefone.setBounds(20, 140, 95, 20);

        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEmail.setText(bundle.getString("BscOrganizacaoFrame_00027")); // NOI18N
        lblEmail.setEnabled(false);
        lblEmail.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEmail.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEmail.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblEmail);
        lblEmail.setBounds(20, 180, 95, 20);

        lblPaginaWeb.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPaginaWeb.setText(bundle.getString("BscOrganizacaoFrame_00017")); // NOI18N
        lblPaginaWeb.setEnabled(false);
        lblPaginaWeb.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPaginaWeb.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPaginaWeb.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlComplementar.add(lblPaginaWeb);
        lblPaginaWeb.setBounds(20, 220, 95, 20);

        fldEndereco.setMaxLength(120);
        pnlComplementar.add(fldEndereco);
        fldEndereco.setBounds(20, 40, 400, 22);

        fldCidade.setMaxLength(20);
        pnlComplementar.add(fldCidade);
        fldCidade.setBounds(20, 80, 400, 22);

        fldEstado.setMaxLength(20);
        pnlComplementar.add(fldEstado);
        fldEstado.setBounds(20, 120, 180, 22);

        fldPais.setMaxLength(20);
        pnlComplementar.add(fldPais);
        fldPais.setBounds(230, 120, 180, 20);

        fldTelefone.setMask("(###) #####-####");
        fldTelefone.setText("(   )       -    ");
        pnlComplementar.add(fldTelefone);
        fldTelefone.setBounds(20, 160, 180, 22);

        fldEmail.setMaxLength(80);
        pnlComplementar.add(fldEmail);
        fldEmail.setBounds(20, 200, 400, 22);

        fldPaginaWeb.setMaxLength(80);
        pnlComplementar.add(fldPaginaWeb);
        fldPaginaWeb.setBounds(20, 240, 400, 22);

        scrDadosComplementares.setViewportView(pnlComplementar);

        tapOrganizationData.addTab(bundle.getString("BscOrganizacaoFrame_00026"), scrDadosComplementares); // NOI18N

        pnlData.add(tapOrganizationData, java.awt.BorderLayout.CENTER);

        pnlFields.add(pnlData, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscOrganizacaoFrame_00001"), pnlRecord); // NOI18N

        strategyList.setVisible(false);
        tapCadastro.addTab(bundle.getString("BscOrganizacaoFrame_00018"), strategyList); // NOI18N
        tapCadastro.addTab(bundle.getString("BscOrganizacaoFrame_00019"), listaPessoas); // NOI18N
        tapCadastro.addTab(bundle.getString("BscOrganizacaoFrame_00028"), listaGrupoPessoas); // NOI18N

        pnlPrincipal.setLayout(new java.awt.BorderLayout());

        jpnlData.setPreferredSize(new java.awt.Dimension(300, 190));
        jpnlData.setLayout(new java.awt.BorderLayout());

        listaReunioes.setAutoscrolls(true);
        jpnlData.add(listaReunioes, java.awt.BorderLayout.CENTER);

        pnlCalendario.setPreferredSize(new java.awt.Dimension(240, 240));
        pnlCalendario.setRequestFocusEnabled(false);
        pnlCalendario.setLayout(new java.awt.BorderLayout());

        calendario.setAureole(true);
        calendario.setDOW("Dom Seg Ter Qua Qui Sex S�b");
        calendario.setDOWFont(new java.awt.Font("SansSerif", 0, 10)); // NOI18N
        calendario.setHideYearField(true);
        calendario.setLocaleStrings(true);
        calendario.setName("calendario");
        calendario.setRangeSelection(false);
        calendario.setYearListCount(7);
        calendario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                calendarioMouseClicked(evt);
            }
        });
        calendario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                calendarioKeyReleased(evt);
            }
        });
        pnlCalendario.add(calendario, java.awt.BorderLayout.PAGE_START);

        dia.setBackground(new java.awt.Color(255, 255, 255));
        dia.setBounds(new java.awt.Rectangle(0, 0, 50, 0));
        dia.setEditable(false);
        dia.setEnableDeleteKey(false);
        dia.setEnableEditDialog(false);
        dia.setEndTime(1200);
        dia.setName("dia");
        dia.setShowCaption(false);
        dia.setShowDOW(false);
        dia.setTwelveHours(false);
        dia.setUseLocale(true);
        pnlCalendario.add(dia, java.awt.BorderLayout.CENTER);

        jpnlData.add(pnlCalendario, java.awt.BorderLayout.LINE_END);

        pnlPrincipal.add(jpnlData, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscOrganizacaoFrame_00025"), pnlPrincipal); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pnlTopForm1.setOpaque(false);
        pnlTopForm1.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm1, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeDay(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeDay
        // TODO add your handling code here:
    }//GEN-LAST:event_changeDay

    private void calendarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_calendarioKeyReleased
        changeDay();
        if (htbAgenda.get(calendario.getDate()) != null) {
            int posicao = ((Integer) (htbAgenda.get(calendario.getDate()))).intValue();
            listaReunioes.selectLine(posicao, posicao); // Selecionar lista de reuni�es.
        }
    }//GEN-LAST:event_calendarioKeyReleased

    private void calendarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_calendarioMouseClicked
        changeDay();
        if (htbAgenda.get(calendario.getDate()) != null) {
            int posicao = ((Integer) (htbAgenda.get(calendario.getDate()))).intValue();
            listaReunioes.selectLine(posicao, posicao);
        }
    }//GEN-LAST:event_calendarioMouseClicked
	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("ORGANIZACOES");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord(); // Recarregar.
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void jBtnCopyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCopyActionPerformed
        /*Cria um novo formul�rio do tipo organiza��o.*/
        BscOrganizacaoFrame form = (BscOrganizacaoFrame) BscStaticReferences.getBscFormController().newForm(type, this.getID(), contextId);
        /*Define o ID da organiza��o que servir� como base para a duplica��o.*/
        form.setIdCopyParent(Integer.parseInt(this.getID()));
        /*Desabilita os campos que nao ser�o utilizados no processo.*/
        form.pnlDescricao.setVisible(false);

        form.tapOrganizationData.remove(1);

        form.tapCadastro.remove(4);
        form.tapCadastro.remove(3);
        form.tapCadastro.remove(2);
        form.tapCadastro.remove(1);
}//GEN-LAST:event_jBtnCopyActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVCalendar calendario;
    private pv.jfcx.JPVDay dia;
    private pv.jfcx.JPVEdit fldCidade;
    private pv.jfcx.JPVEdit fldEmail;
    private pv.jfcx.JPVEdit fldEndereco;
    private pv.jfcx.JPVEdit fldEstado;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldPaginaWeb;
    private pv.jfcx.JPVEdit fldPais;
    private pv.jfcx.JPVMask fldTelefone;
    private javax.swing.JButton jBtnCopy;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPanel jpnlData;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEndereco;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblMissao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNotas;
    private javax.swing.JLabel lblPaginaWeb;
    private javax.swing.JLabel lblPais;
    private javax.swing.JLabel lblPolitica1;
    private javax.swing.JLabel lblPolitica2;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblValores;
    private javax.swing.JLabel lblVisao;
    private bsc.beans.JBIListPanel listaGrupoPessoas;
    private bsc.beans.JBIListPanel listaPessoas;
    private bsc.beans.JBIListPanel listaReunioes;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlCalendario;
    private javax.swing.JPanel pnlComplementar;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlDescricao;
    private javax.swing.JPanel pnlEssencial;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopForm1;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scrDadosComplementares;
    private javax.swing.JScrollPane scrDadosEssenciais;
    private bsc.beans.JBIListPanel strategyList;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JTabbedPane tapOrganizationData;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    private bsc.beans.JBITextArea txtMissao;
    private bsc.beans.JBITextArea txtNotas;
    private bsc.beans.JBITextArea txtQualidade;
    private bsc.beans.JBITextArea txtValores;
    private bsc.beans.JBITextArea txtVisao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscOrganizacaoFrame(int operation, String idAux, String contextId) {
        initComponents();
        this.contextId = contextId;
        event.defaultConstructor(operation, idAux, contextId);
        listaReunioes.addJBIListPanelListener(this);
        dia.setCaptionBorder(0);
    }

    @Override
    public void setStatus(int value) {
        status = value;

        if (value == 2/*UPDATING*/) {
            idCopyParent = 0;
        }
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        type = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(type);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void mousePressed(java.awt.event.ComponentEvent componentEvent) {
        int itemSelecionado = listaReunioes.xmlTable.getSelectedRow();
        Date compromisso = (Date) vctAgenda.get(itemSelecionado);
        calendario.setSelDate(compromisso);
        changeDay();
    }

    @Override
    public void keyPressed(java.awt.event.ComponentEvent componentEvent) {
        int itemSelecionado = listaReunioes.xmlTable.getSelectedRow();
        Date compromisso = (Date) vctAgenda.get(itemSelecionado);
        calendario.setSelDate(compromisso);
        changeDay();
    }

    private void changeDay() {
        dia.setDate(calendario.getCalendar());
    }

    /**
     * @return the idCopyParent
     */
    public int getIdCopyParent() {
        return idCopyParent;
    }

    /**
     * @param idCopyParent the idCopyParent to set
     */
    public void setIdCopyParent(int idCopyParent) {
        this.idCopyParent = idCopyParent;
    }

    /**
     *
     */
    public boolean isDuplicate() {
        boolean duplicate = false;

        if (getIdCopyParent() != 0) {
            duplicate = true;
        }
        return duplicate;
    }
}
