package bsc.swing.framework;

import java.util.ResourceBundle;

public class BscPessoaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("PESSOA");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblCargo.setEnabled(true);
        fldCargo.setEnabled(true);
        lblTelefone.setEnabled(true);
        fldTelefone.setEnabled(true);
        lblRamal.setEnabled(true);
        fldRamal.setEnabled(true);
        lblEmail.setEnabled(true);
        fldEmail.setEnabled(true);
        lblEndereco.setEnabled(true);
        fldEndereco.setEnabled(true);
        lblCidade.setEnabled(true);
        fldCidade.setEnabled(true);
        lblEstado.setEnabled(true);
        fldEstado.setEnabled(true);
        lblPais.setEnabled(true);
        fldPais.setEnabled(true);
        lblUsuario.setEnabled(true);
        cbbUsuario.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblCargo.setEnabled(false);
        fldCargo.setEnabled(false);
        lblTelefone.setEnabled(false);
        fldTelefone.setEnabled(false);
        lblRamal.setEnabled(false);
        fldRamal.setEnabled(false);
        lblEmail.setEnabled(false);
        fldEmail.setEnabled(false);
        lblEndereco.setEnabled(false);
        fldEndereco.setEnabled(false);
        lblCidade.setEnabled(false);
        fldCidade.setEnabled(false);
        lblEstado.setEnabled(false);
        fldEstado.setEnabled(false);
        lblPais.setEnabled(false);
        fldPais.setEnabled(false);
        lblUsuario.setEnabled(false);
        cbbUsuario.setEnabled(false);
    }
    java.util.Hashtable htbUsuarios = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        fldNome.setText(record.getString("NOME"));
        fldCargo.setText(record.getString("CARGO"));
        fldTelefone.setText(record.getString("FONE"));
        fldRamal.setText(record.getString("RAMAL"));
        fldEmail.setText(record.getString("EMAIL"));
        fldEndereco.setText(record.getString("ENDERECO"));
        fldCidade.setText(record.getString("CIDADE"));
        fldEstado.setText(record.getString("ESTADO"));
        fldPais.setText(record.getString("PAIS"));
        event.populateCombo(record.getBIXMLVector("USUARIOS"), "USERID",
                htbUsuarios, cbbUsuario);
        pnlHead.setComponents(record.getBIXMLVector("CONTEXTOS"));
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("CARGO", fldCargo.getText());
        recordAux.set("FONE", fldTelefone.getText());
        recordAux.set("RAMAL", fldRamal.getText());
        recordAux.set("EMAIL", fldEmail.getText());
        recordAux.set("ENDERECO", fldEndereco.getText());
        recordAux.set("CIDADE", fldCidade.getText());
        recordAux.set("ESTADO", fldEstado.getText());
        recordAux.set("PAIS", fldPais.getText());
        recordAux.set("USERID", event.getComboValue(htbUsuarios, cbbUsuario));

        return recordAux;
    }

    public boolean hasCombos() {
        return true;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblCargo = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        lblRamal = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        lblEndereco = new javax.swing.JLabel();
        lblCidade = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblPais = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        cbbUsuario = new javax.swing.JComboBox();
        fldNome = new pv.jfcx.JPVEdit();
        fldCargo = new pv.jfcx.JPVEdit();
        fldEndereco = new pv.jfcx.JPVEdit();
        fldCidade = new pv.jfcx.JPVEdit();
        fldPais = new pv.jfcx.JPVEdit();
        fldTelefone = new pv.jfcx.JPVMask();
        fldRamal = new pv.jfcx.JPVMask();
        fldEmail = new pv.jfcx.JPVEdit();
        fldEstado = new pv.jfcx.JPVMask();
        pnlTopRecord = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHead = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscPessoaFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_pessoa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscPessoaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscPessoaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscPessoaFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscPessoaFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscPessoaFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(355, 330));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscPessoaFrame_00007")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 10, 90, 20);

        lblCargo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCargo.setText(bundle.getString("BscPessoaFrame_00008")); // NOI18N
        lblCargo.setEnabled(false);
        lblCargo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCargo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCargo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCargo);
        lblCargo.setBounds(20, 90, 90, 20);

        lblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTelefone.setText(bundle.getString("BscPessoaFrame_00009")); // NOI18N
        lblTelefone.setEnabled(false);
        lblTelefone.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTelefone);
        lblTelefone.setBounds(20, 250, 90, 20);

        lblRamal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRamal.setText(bundle.getString("BscPessoaFrame_00010")); // NOI18N
        lblRamal.setEnabled(false);
        lblRamal.setMaximumSize(new java.awt.Dimension(100, 16));
        lblRamal.setMinimumSize(new java.awt.Dimension(100, 16));
        lblRamal.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblRamal);
        lblRamal.setBounds(240, 250, 40, 20);

        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEmail.setText("E-mail:");
        lblEmail.setEnabled(false);
        lblEmail.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEmail.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEmail.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblEmail);
        lblEmail.setBounds(20, 290, 90, 20);

        lblEndereco.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEndereco.setText(bundle.getString("BscPessoaFrame_00011")); // NOI18N
        lblEndereco.setEnabled(false);
        lblEndereco.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEndereco.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEndereco.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblEndereco);
        lblEndereco.setBounds(20, 130, 90, 20);

        lblCidade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCidade.setText(bundle.getString("BscPessoaFrame_00012")); // NOI18N
        lblCidade.setEnabled(false);
        lblCidade.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCidade.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCidade.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCidade);
        lblCidade.setBounds(20, 170, 90, 20);

        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEstado.setText(bundle.getString("BscPessoaFrame_00013")); // NOI18N
        lblEstado.setEnabled(false);
        lblEstado.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEstado.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEstado.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblEstado);
        lblEstado.setBounds(240, 170, 90, 20);

        lblPais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPais.setText(bundle.getString("BscPessoaFrame_00014")); // NOI18N
        lblPais.setEnabled(false);
        lblPais.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPais.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPais.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblPais);
        lblPais.setBounds(20, 210, 90, 20);

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario.setText(bundle.getString("BscPessoaFrame_00015")); // NOI18N
        lblUsuario.setEnabled(false);
        lblUsuario.setMaximumSize(new java.awt.Dimension(100, 16));
        lblUsuario.setMinimumSize(new java.awt.Dimension(100, 16));
        lblUsuario.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblUsuario);
        lblUsuario.setBounds(20, 52, 90, 16);
        pnlData.add(cbbUsuario);
        cbbUsuario.setBounds(20, 70, 180, 22);

        fldNome.setMaxLength(50);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        fldCargo.setMaxLength(20);
        pnlData.add(fldCargo);
        fldCargo.setBounds(20, 110, 400, 22);

        fldEndereco.setMaxLength(120);
        pnlData.add(fldEndereco);
        fldEndereco.setBounds(20, 150, 400, 22);

        fldCidade.setMaxLength(20);
        pnlData.add(fldCidade);
        fldCidade.setBounds(20, 190, 180, 22);

        fldPais.setMaxLength(20);
        pnlData.add(fldPais);
        fldPais.setBounds(20, 230, 180, 22);

        fldTelefone.setMask("(###)#####-####");
        fldTelefone.setText("(   )      -    ");
        pnlData.add(fldTelefone);
        fldTelefone.setBounds(20, 270, 180, 22);

        fldRamal.setMask("##########");
        pnlData.add(fldRamal);
        fldRamal.setBounds(240, 270, 180, 22);

        fldEmail.setMaxLength(80);
        pnlData.add(fldEmail);
        fldEmail.setBounds(20, 310, 400, 22);

        fldEstado.setMask("^^");
        fldEstado.setText("");
        pnlData.add(fldEstado);
        fldEstado.setBounds(240, 190, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab("Pessoa", pnlRecord);

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(10, 15));
        pnlTopForm.setLayout(new java.awt.BorderLayout());

        pnlHead.setMinimumSize(new java.awt.Dimension(520, 45));
        pnlTopForm.add(pnlHead, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("PESSOA_RESPONSAVEL");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbUsuario;
    private pv.jfcx.JPVEdit fldCargo;
    private pv.jfcx.JPVEdit fldCidade;
    private pv.jfcx.JPVEdit fldEmail;
    private pv.jfcx.JPVEdit fldEndereco;
    private pv.jfcx.JPVMask fldEstado;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldPais;
    private pv.jfcx.JPVMask fldRamal;
    private pv.jfcx.JPVMask fldTelefone;
    private javax.swing.JLabel lblCargo;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblEndereco;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPais;
    private javax.swing.JLabel lblRamal;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHead;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscPessoaFrame(int operation, String idAux, String contextAux) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextAux);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
