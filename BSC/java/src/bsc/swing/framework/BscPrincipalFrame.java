package bsc.swing.framework;

import java.awt.event.ActionEvent;


public class BscPrincipalFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("BSC");
	
	private javax.swing.JButton btnImportar;
	private javax.swing.JButton btnExportar;

	public void enableFields() {
		// Habilita os campos do formul�rio.
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		// Ex.:
		// strategyList.setDataSource(
		// record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
		listOrganizacoes.setDataSource(record.getBIXMLVector("ORGANIZACOES"), record.getString("ID"), record.getString("ID"), this.event );		
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        listOrganizacoes = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscPrincipalFrame_00001")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        tapCadastro.addTab(bundle.getString("BscPrincipalFrame_00002"), listOrganizacoes); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents
						
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private bsc.beans.JBIListPanel listOrganizacoes;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JTabbedPane tapCadastro;
    // End of variables declaration//GEN-END:variables

	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;

	private void btnImportarActionPerformed(ActionEvent evt) {
		try {
			bsc.core.BscStaticReferences.getBscFormController().getForm( 
				"ESTIMPORT", 
				"1", 
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstruturaImport_00001") );
		}
		catch (bsc.core.BscFormControllerException exc ) {
			bsc.core.BscDebug.println(exc.getMessage());
		}
	}
        
	private void btnExportarActionPerformed(ActionEvent evt) {
		try {
			bsc.core.BscStaticReferences.getBscFormController().getForm( 
				"ESTEXPORT", 
				"1", 
				java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstruturaExport_00001") );
		}
		catch (bsc.core.BscFormControllerException exc ) {
			bsc.core.BscDebug.println(exc.getMessage());
		}
	}
	public BscPrincipalFrame(int operation, String idAux, String contextId) {
		initComponents();

		btnImportar = new javax.swing.JButton();
		btnImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_importar.gif")));
		btnImportar.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstruturaImport_00001"));
		btnImportar.setFocusable(false);
		btnImportar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnImportar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		btnImportar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnImportarActionPerformed(evt);
			}
		});

		btnExportar = new javax.swing.JButton();
		btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportar.gif")));
		btnExportar.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscEstruturaExport_00001"));
		btnExportar.setFocusable(false);
		btnExportar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnExportar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		btnExportar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnExportarActionPerformed(evt);
			}
		});

		listOrganizacoes.tbInsertion.add(btnImportar);
		listOrganizacoes.tbInsertion.add(btnExportar);

		event.defaultConstructor( operation, idAux, contextId );
	}

	public void setStatus( int value ) {
		status = value;
	}

	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
	}
	
	public void showOperationsButtons() {
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}
	
}
