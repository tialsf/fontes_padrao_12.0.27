package bsc.swing.framework;

import bsc.xml.*;
import javax.swing.JInternalFrame;
import java.awt.Dimension;
import javax.swing.tree.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.*;

public class BscReuPauFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, javax.swing.event.TreeSelectionListener{
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("REUPAU");
	private Object[] elementoASelecionar;
	private String elementoID;
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblOrganizacao.setEnabled(true);
		fldOrganizacao.setEnabled(true);
		lblEstrategia.setEnabled(true);
		fldEstrategia.setEnabled(true);
		lblTipo.setEnabled(true);
		fldTipo.setEnabled(true);
		lblNome.setEnabled(true);
		fldNome.setEnabled(true);
		fldDetalhes.setEnabled(true);
		lblDetalhes.setEnabled(true);
		bscTreeElemento.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblOrganizacao.setEnabled(false);
		fldOrganizacao.setEnabled(false);
		lblEstrategia.setEnabled(false);
		fldEstrategia.setEnabled(false);
		lblTipo.setEnabled(false);
		fldTipo.setEnabled(false);
		lblNome.setEnabled(false);
		fldNome.setEnabled(false);
		fldDetalhes.setEnabled(false);
		lblDetalhes.setEnabled(false);		
		bscTreeElemento.setEnabled(false);
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldOrganizacao.setText(record.getString("ORG") );
		fldEstrategia.setText(record.getString("EST") );
		fldTipo.setText( record.getString("NOME") );
		fldNome.setText( record.getString("ELEMENTO") );
		fldDetalhes.setText( record.getString("DETALHES") );
		elementoID = record.getString("ELEMID");

		bsc.xml.BIXMLVector listaArvores = record.getBIXMLVector("ARVORES");
		// Init
		bscTreeElemento.removeAll();
		bscTreeElemento.setCellRenderer( new bsc.swing.BscTreeCellRenderer(bsc.core.BscStaticReferences.getBscImageResources()) );
		bscTreeElemento.setModel( new DefaultTreeModel(createTree( listaArvores.get(0))));

		if(status != event.INSERTING){
		    TreePath pathASelecionar = new TreePath(elementoASelecionar[elementoASelecionar.length-1]);

		    int jRows = 0;
		    while (jRows < bscTreeElemento.getRowCount() && pathASelecionar.getLastPathComponent().toString() != bscTreeElemento.getPathForRow(jRows).getLastPathComponent().toString()){
			bscTreeElemento.expandRow(jRows);
			jRows ++;
		    }

		    bscTreeElemento.setSelectionPath( pathASelecionar);
		}
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );

		recordAux.set( "NOME", fldTipo.getText() );
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTreeElemento.getLastSelectedPathComponent();
		recordAux.set( "ELEMID", ((bsc.swing.BscReuniaoTreeNode)node.getUserObject()).getID());
		recordAux.set( "DETALHES", fldDetalhes.getText() );

		recordAux.set(record.getBIXMLVector("ARVORES"));
	
		return recordAux;
	}

	public void loadTree(String type) {
		bsc.xml.BIXMLVector listaArvores = record.getBIXMLVector("ARVORES");

		// Init
		bscTreeElemento.removeAll();
		bscTreeElemento.setCellRenderer( new bsc.swing.BscTreeCellRenderer(bsc.core.BscStaticReferences.getBscImageResources()) );
		bscTreeElemento.setModel( new DefaultTreeModel(createTree( listaArvores.get(0))));
	}
	
	public DefaultMutableTreeNode createTree( BIXMLRecord treeXML ) {
		String key = new String ( (String) treeXML.getKeyNames().next() );
		DefaultMutableTreeNode root = insertGroup( treeXML.getBIXMLVector( key ), null );
		return root;
	}
	
	public DefaultMutableTreeNode insertRecord( BIXMLRecord record,	DefaultMutableTreeNode tree )
		throws BIXMLException{

		bsc.swing.BscReuniaoTreeNode treeNode = new bsc.swing.BscReuniaoTreeNode( record.getTagName(),
									    record.getAttributes().getString("ID"),
									    record.getAttributes().getString("NOME"),
									    record.getAttributes().getString("ORGNOME"),
									    record.getAttributes().getString("ESTNOME"));

		DefaultMutableTreeNode child = new DefaultMutableTreeNode(treeNode);

		for ( Iterator e = record.getKeyNames(); e.hasNext(); )
			child = insertGroup( record.getBIXMLVector( (String)e.next() ),
							child );

		if (tree != null)
		{
			tree.add(child);
			if(elementoID.equals(record.getAttributes().getString("ID")) && (fldTipo.getText().equals(record.getTagName()))){
			    elementoASelecionar = child.getPath();
			}
			return tree;
		}
		else
			return child;
	}

	public DefaultMutableTreeNode insertGroup( BIXMLVector vector, DefaultMutableTreeNode tree ) throws BIXMLException {
		DefaultMutableTreeNode child =	new DefaultMutableTreeNode( 
							    new bsc.swing.BscTreeNode(
									    vector.getAttributes().getString("TIPO"),
									    vector.getAttributes().getString("ID"),
									    vector.getAttributes().getString("NOME")));

		for ( int i = 0; i < vector.size(); i++ ) {
			child = insertRecord( vector.get(i), child );

		}

		if (tree != null) {
			tree.add(child);
			return tree;
		}
		else
			return child;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollRecord = new javax.swing.JScrollPane();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload2 = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollTree = new javax.swing.JScrollPane();
        bscTreeElemento = new javax.swing.JTree();
        scrollRecord = new javax.swing.JScrollPane();
        jPanelRecord = new javax.swing.JPanel();
        pnlNorte = new javax.swing.JPanel();
        pnlElemento = new javax.swing.JPanel();
        lblOrganizacao = new javax.swing.JLabel();
        lblEstrategia = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        fldEstrategia = new pv.jfcx.JPVEdit();
        fldOrganizacao = new pv.jfcx.JPVEdit();
        fldTipo = new pv.jfcx.JPVEdit();
        fldNome = new pv.jfcx.JPVEdit();
        pnlCard = new javax.swing.JPanel();
        jCard = new bsc.beans.JBIDesktopPane();
        pnlCenter = new javax.swing.JPanel();
        pnlDetalhes = new javax.swing.JPanel();
        pnlBottom = new javax.swing.JPanel();
        pnlRight = new javax.swing.JPanel();
        pnlLeft = new javax.swing.JPanel();
        pnlTop = new javax.swing.JPanel();
        lblDetalhes = new javax.swing.JLabel();
        scrollDetalhes = new javax.swing.JScrollPane();
        fldDetalhes = new javax.swing.JTextArea();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        jScrollRecord.setBorder(null);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscReuniaoFrame_00018")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        tapCadastro.setMinimumSize(new java.awt.Dimension(485, 110));
        tapCadastro.setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscTarDocFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(79, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscTarDocFrame_00010")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(79, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscTarDocFrame_00006")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(79, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscTarDocFrame_00011")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(79, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscTarDocFrame_00009")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(79, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload2.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload2.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload2.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload2.setPreferredSize(new java.awt.Dimension(79, 20));
        btnReload2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload2ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload2);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(150);
        jSplitPane1.setAutoscrolls(true);
        jSplitPane1.setPreferredSize(new java.awt.Dimension(200, 330));

        jScrollTree.setBorder(null);
        jScrollTree.setPreferredSize(new java.awt.Dimension(10, 10));

        bscTreeElemento.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        bscTreeElemento.setMinimumSize(new java.awt.Dimension(20, 21));
        jScrollTree.setViewportView(bscTreeElemento);

        jSplitPane1.setLeftComponent(jScrollTree);

        scrollRecord.setPreferredSize(new java.awt.Dimension(484, 304));

        jPanelRecord.setToolTipText("");
        jPanelRecord.setAutoscrolls(true);
        jPanelRecord.setPreferredSize(new java.awt.Dimension(480, 295));
        jPanelRecord.setLayout(new java.awt.BorderLayout());

        pnlNorte.setOpaque(false);
        pnlNorte.setPreferredSize(new java.awt.Dimension(10, 230));
        pnlNorte.setLayout(new java.awt.BorderLayout());

        pnlElemento.setPreferredSize(new java.awt.Dimension(250, 240));
        pnlElemento.setLayout(null);

        lblOrganizacao.setLabelFor(fldOrganizacao);
        lblOrganizacao.setText(bundle.getString("BscReuniaoFrame_00019")); // NOI18N
        lblOrganizacao.setEnabled(false);
        lblOrganizacao.setFocusable(false);
        pnlElemento.add(lblOrganizacao);
        lblOrganizacao.setBounds(10, 15, 80, 14);
        lblOrganizacao.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00019")); // NOI18N

        lblEstrategia.setLabelFor(fldEstrategia);
        lblEstrategia.setText(bundle.getString("BscReuniaoFrame_00020")); // NOI18N
        lblEstrategia.setEnabled(false);
        lblEstrategia.setFocusable(false);
        pnlElemento.add(lblEstrategia);
        lblEstrategia.setBounds(10, 55, 80, 14);
        lblEstrategia.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00020")); // NOI18N

        lblTipo.setLabelFor(fldOrganizacao);
        lblTipo.setText(bundle.getString("BscReuniaoFrame_00021")); // NOI18N
        lblTipo.setEnabled(false);
        lblTipo.setFocusable(false);
        pnlElemento.add(lblTipo);
        lblTipo.setBounds(10, 95, 80, 14);
        lblTipo.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00021")); // NOI18N

        lblNome.setLabelFor(fldOrganizacao);
        lblNome.setText(bundle.getString("BscReuniaoFrame_00026")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setFocusable(false);
        pnlElemento.add(lblNome);
        lblNome.setBounds(10, 135, 80, 14);
        lblNome.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00026")); // NOI18N

        fldEstrategia.setEditable(false);
        pnlElemento.add(fldEstrategia);
        fldEstrategia.setBounds(10, 70, 180, 22);

        fldOrganizacao.setEditable(false);
        pnlElemento.add(fldOrganizacao);
        fldOrganizacao.setBounds(10, 30, 180, 22);

        fldTipo.setEditable(false);
        pnlElemento.add(fldTipo);
        fldTipo.setBounds(10, 110, 180, 22);

        fldNome.setEditable(false);
        pnlElemento.add(fldNome);
        fldNome.setBounds(10, 150, 180, 22);

        pnlNorte.add(pnlElemento, java.awt.BorderLayout.WEST);

        pnlCard.setPreferredSize(new java.awt.Dimension(240, 255));

        jCard.setPreferredSize(new java.awt.Dimension(237, 255));
        jCard.setVisible(false);
        pnlCard.add(jCard);

        pnlNorte.add(pnlCard, java.awt.BorderLayout.CENTER);

        jPanelRecord.add(pnlNorte, java.awt.BorderLayout.NORTH);

        pnlCenter.setPreferredSize(new java.awt.Dimension(100, 50));
        pnlCenter.setLayout(new java.awt.BorderLayout());

        pnlDetalhes.setMinimumSize(new java.awt.Dimension(50, 50));
        pnlDetalhes.setPreferredSize(new java.awt.Dimension(100, 55));
        pnlDetalhes.setLayout(new java.awt.BorderLayout());

        pnlBottom.setMinimumSize(new java.awt.Dimension(5, 10));
        pnlDetalhes.add(pnlBottom, java.awt.BorderLayout.SOUTH);

        pnlRight.setMinimumSize(new java.awt.Dimension(5, 10));
        pnlDetalhes.add(pnlRight, java.awt.BorderLayout.EAST);

        pnlLeft.setMinimumSize(new java.awt.Dimension(5, 10));
        pnlDetalhes.add(pnlLeft, java.awt.BorderLayout.WEST);

        pnlTop.setMinimumSize(new java.awt.Dimension(65, 25));
        pnlTop.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 5));

        lblDetalhes.setText(bundle.getString("BscReuniaoFrame_00014")); // NOI18N
        lblDetalhes.setEnabled(false);
        pnlTop.add(lblDetalhes);
        lblDetalhes.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00014")); // NOI18N

        pnlDetalhes.add(pnlTop, java.awt.BorderLayout.NORTH);

        scrollDetalhes.setPreferredSize(new java.awt.Dimension(106, 23));

        fldDetalhes.setLineWrap(true);
        fldDetalhes.setWrapStyleWord(true);
        fldDetalhes.setAlignmentX(0.0F);
        fldDetalhes.setAlignmentY(0.0F);
        fldDetalhes.setMinimumSize(new java.awt.Dimension(10, 23));
        fldDetalhes.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        scrollDetalhes.setViewportView(fldDetalhes);

        pnlDetalhes.add(scrollDetalhes, java.awt.BorderLayout.CENTER);

        pnlCenter.add(pnlDetalhes, java.awt.BorderLayout.CENTER);

        jPanelRecord.add(pnlCenter, java.awt.BorderLayout.CENTER);

        scrollRecord.setViewportView(jPanelRecord);

        jSplitPane1.setRightComponent(scrollRecord);

        pnlRecord.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscReuniaoFrame_00018"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReload2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload2ActionPerformed
		event.loadHelp("CADASTRANDO_PAUTADEREUNIAO");
	}//GEN-LAST:event_btnReload2ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		event.saveRecord( );
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed
	
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree bscTreeElemento;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload2;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextArea fldDetalhes;
    private pv.jfcx.JPVEdit fldEstrategia;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldOrganizacao;
    private pv.jfcx.JPVEdit fldTipo;
    private bsc.beans.JBIDesktopPane jCard;
    private javax.swing.JPanel jPanelRecord;
    private javax.swing.JScrollPane jScrollRecord;
    private javax.swing.JScrollPane jScrollTree;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblDetalhes;
    private javax.swing.JLabel lblEstrategia;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblOrganizacao;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JPanel pnlBottom;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlCard;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlDetalhes;
    private javax.swing.JPanel pnlElemento;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlNorte;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRight;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTop;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JScrollPane scrollDetalhes;
    private javax.swing.JScrollPane scrollRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscReuPauFrame(int operation, String idAux, String contextId) {
		initComponents();
		bscTreeElemento.setModel( new DefaultTreeModel( new DefaultMutableTreeNode(
			new String(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMainPanel_00002")) ) ) );
		
		validate();
		
		event.defaultConstructor( operation, idAux, contextId );
		
		bscTreeElemento.addTreeSelectionListener( this );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	 
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
	    event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}
	
	public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTreeElemento.getLastSelectedPathComponent();

		if(node != null){

		    if( node.getUserObject() instanceof bsc.swing.BscReuniaoTreeNode){
			bsc.swing.BscReuniaoTreeNode bscNode = (bsc.swing.BscReuniaoTreeNode) node.getUserObject();

			fldOrganizacao.setText( bscNode.getOrganizacao());
			fldEstrategia.setText( bscNode.getEstrategia());
			fldTipo.setText(bscNode.getType());
			fldNome.setText(bscNode.getName());

			jCard.removeAll();
			
			if(fldTipo.getText().equals("INDICADOR") | fldTipo.getText().equals("OBJETIVO")) {

			    jCard.setVisible(true);
			    bsc.core.BscDataController dataController = bsc.core.BscStaticReferences.getBscDataController();
			    bsc.xml.BIXMLRecord xmlCardElemento = dataController.loadRecord(bscNode.getID(), fldTipo.getText(), "CARD");
			    bsc.swing.analisys.BscIndicadorCardFrame cardIndicador = new bsc.swing.analisys.BscIndicadorCardFrame(xmlCardElemento);
			    jCard.add(cardIndicador);
			    jCard.validate();
			} else{
			    jCard.setVisible(false);
			}
			jCard.repaint();
		    }
		}
	}
}
