package bsc.swing.framework;


public class BscReuniaoFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("REUNIAO");
	private java.util.Hashtable htbPessoas = new java.util.Hashtable();
	private java.util.Hashtable htbAux = new java.util.Hashtable();
	private bsc.xml.BIXMLVector BIvctPessoas = new bsc.xml.BIXMLVector();
	private bsc.xml.BIXMLVector BIvctAux = new bsc.xml.BIXMLVector();
	private String respId = "";
	private String tipoPessoa = " ";
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblAssunto.setEnabled(true);
		fldAssunto.setEnabled(true);
		lblDetalhes.setEnabled(true);
		fldDetalhes.setEnabled(true);
		lblData.setEnabled(true);
		fldData.setEnabled(true);
		lblHoraInicio.setEnabled(true);
		fldHoraInicio.setEnabled(true);
		lblHoraFim.setEnabled(true);
		fldHoraFim.setEnabled(true);
		lblLocal.setEnabled(true);
		fldLocal.setEnabled(true);
		lblResponsavel.setEnabled(true);
		btnResponsavel.setEnabled(true);
		listaPessoas.setEnabled(true);
	}
	
	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblAssunto.setEnabled(false);
		fldAssunto.setEnabled(false);
		lblDetalhes.setEnabled(false);
		fldDetalhes.setEnabled(false);
		lblData.setEnabled(false);
		fldData.setEnabled(false);
		lblHoraInicio.setEnabled(false);
		fldHoraInicio.setEnabled(false);
		lblHoraFim.setEnabled(false);
		fldHoraFim.setEnabled(false);
		lblLocal.setEnabled(false);
		fldLocal.setEnabled(false);
		lblResponsavel.setEnabled(false);
		fldResponsavel.setEnabled(false);
		btnResponsavel.setEnabled(true);
		listaPessoas.setEnabled(false);
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldAssunto.setText(record.getString("NOME") );
		fldDetalhes.setText(record.getString("DETALHES") );
		fldData.setCalendar(record.getDate("DATAREU"));
		fldHoraInicio.setText(record.getString("HORAINI"));
		fldHoraFim.setText(record.getString("HORAFIM"));
		fldLocal.setText(record.getString("LOCAL"));
		fldResponsavel.setText(record.getString("RESPONSAVEL"));
		respId = record.getString("RESPID");
		tipoPessoa = record.getString("TIPOPESSOA");
		listaRetorno.setDataSource( record.getBIXMLVector("REURETS"), record.getString("ID"),
					    record.getString("CONTEXTID"), this.event);
		listaDocumento.setDataSource(	record.getBIXMLVector("REUDOCS"), record.getString("ID"),
						record.getString("CONTEXTID"), this.event);
		listaPessoas.setDataSource( record.getBIXMLVector("REUCONS"), "REUCON", event );

		BIvctPessoas = listaPessoas.getXMLData();
		
		listaPauta.setDataSource( record.getBIXMLVector("REUPAUS"), record.getString("ID"),
					    record.getString("CONTEXTID"), this.event);

	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		recordAux.set( "NOME", fldAssunto.getText() );
		recordAux.set( "DETALHES", fldDetalhes.getText() );
		recordAux.set( "DATAREU", fldData.getCalendar() );
		recordAux.set( "HORAINI", fldHoraInicio.getText() );
		recordAux.set( "HORAFIM", fldHoraFim.getText() );
		recordAux.set( "LOCAL", fldLocal.getText() );
		recordAux.set( "RESPID", respId );
		recordAux.set( "TIPOPESSOA", tipoPessoa );
		recordAux.set( listaPessoas.getXMLData() );

		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
	    boolean retorno = true;
	    if(	fldAssunto.getText().trim().length() == 0 ){
		errorMessage.append( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00022"));
		//"Assunto deve ser informado.
		retorno = false;
	    }

	    if(	fldData.getText().trim().length() == 0 ){
		errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00023"));
		//"Data deve ser informada.");
		retorno = false;
	    }

	    if(	fldHoraInicio.getCalendar().after(fldHoraFim.getCalendar())){
		errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00024"));
		//"T�rmino deve ser maior que o In�cio.
		retorno = false;
	    }

	    if(	fldLocal.getText().trim().length() == 0 ){
		errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00025"));
		//"Local deve ser informado."
		retorno = false;
	    }
	    
	    return retorno;
	}
	
	public boolean hasCombos() {
		return false;
	}

	public void populateCombos(bsc.xml.BIXMLRecord serverData) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTopForm = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnNotify = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnlData = new javax.swing.JPanel();
        lblAssunto = new javax.swing.JLabel();
        lblHoraInicio = new javax.swing.JLabel();
        lblDetalhes = new javax.swing.JLabel();
        lblData = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        fldDetalhes = new bsc.beans.JBITextArea();
        fldData = new pv.jfcx.JPVDatePlus();
        fldAssunto = new pv.jfcx.JPVEdit();
        fldHoraInicio = new pv.jfcx.JPVTime();
        fldHoraFim = new pv.jfcx.JPVTime();
        lblHoraFim = new javax.swing.JLabel();
        lblLocal = new javax.swing.JLabel();
        fldLocal = new pv.jfcx.JPVEdit();
        lblResponsavel = new javax.swing.JLabel();
        fldResponsavel = new pv.jfcx.JPVEdit();
        btnResponsavel = new javax.swing.JButton();
        listaPessoas = new bsc.beans.JBIMultiSelectionPanel();
        pnlTopRecord = new javax.swing.JPanel();
        listaRetorno = new bsc.beans.JBIListPanel();
        listaDocumento = new bsc.beans.JBIListPanel();
        listaPauta = new bsc.beans.JBIListPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscReuniaoFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_reuniao.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        tapCadastro.setMinimumSize(new java.awt.Dimension(100, 100));
        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 500));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscEstrategiaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscEstrategiaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(374, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscEstrategiaFrame_00014")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscEstrategiaFrame_00004")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscEstrategiaFrame_00005")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnNotify.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnNotify.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_conectado.gif"))); // NOI18N
        btnNotify.setText(bundle.getString("BscReuniaoFrame_00028")); // NOI18N
        btnNotify.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnNotify.setMaximumSize(new java.awt.Dimension(76, 20));
        btnNotify.setMinimumSize(new java.awt.Dimension(76, 20));
        btnNotify.setPreferredSize(new java.awt.Dimension(76, 20));
        btnNotify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotifyActionPerformed(evt);
            }
        });
        tbInsertion.add(btnNotify);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(600, 600));

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(476, 300));

        pnlData.setPreferredSize(new java.awt.Dimension(600, 600));
        pnlData.setLayout(null);

        lblAssunto.setForeground(new java.awt.Color(51, 51, 255));
        lblAssunto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAssunto.setText(bundle.getString("BscReuniaoFrame_00009")); // NOI18N
        lblAssunto.setEnabled(false);
        pnlData.add(lblAssunto);
        lblAssunto.setBounds(10, 20, 80, 20);
        lblAssunto.getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00009")); // NOI18N

        lblHoraInicio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHoraInicio.setText(bundle.getString("BscReuniaoFrame_00015")); // NOI18N
        lblHoraInicio.setEnabled(false);
        pnlData.add(lblHoraInicio);
        lblHoraInicio.setBounds(10, 180, 80, 20);

        lblDetalhes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDetalhes.setText(bundle.getString("BscReuniaoFrame_00014")); // NOI18N
        lblDetalhes.setEnabled(false);
        pnlData.add(lblDetalhes);
        lblDetalhes.setBounds(10, 60, 80, 20);

        lblData.setForeground(new java.awt.Color(51, 51, 255));
        lblData.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblData.setText(bundle.getString("BscReuniaoFrame_00005")); // NOI18N
        lblData.setEnabled(false);
        pnlData.add(lblData);
        lblData.setBounds(10, 140, 80, 20);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setViewportView(fldDetalhes);

        pnlData.add(jScrollPane3);
        jScrollPane3.setBounds(10, 80, 400, 60);

        fldData.setDialog(true);
        fldData.setUseLocale(true);
        fldData.setWaitForCalendarDate(true);
        pnlData.add(fldData);
        fldData.setBounds(10, 160, 180, 22);

        fldAssunto.setMaxLength(60);
        pnlData.add(fldAssunto);
        fldAssunto.setBounds(10, 40, 400, 22);

        fldHoraInicio.setAllowNull(false);
        fldHoraInicio.setShowSeconds(false);
        fldHoraInicio.setTwelveHours(false);
        pnlData.add(fldHoraInicio);
        fldHoraInicio.setBounds(10, 200, 180, 22);

        fldHoraFim.setAllowNull(false);
        fldHoraFim.setShowSeconds(false);
        fldHoraFim.setTwelveHours(false);
        pnlData.add(fldHoraFim);
        fldHoraFim.setBounds(230, 200, 180, 22);

        lblHoraFim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHoraFim.setText(bundle.getString("BscReuniaoFrame_00016")); // NOI18N
        lblHoraFim.setEnabled(false);
        pnlData.add(lblHoraFim);
        lblHoraFim.setBounds(230, 180, 80, 20);

        lblLocal.setForeground(new java.awt.Color(51, 51, 255));
        lblLocal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLocal.setText(bundle.getString("BscReuniaoFrame_00006")); // NOI18N
        lblLocal.setEnabled(false);
        pnlData.add(lblLocal);
        lblLocal.setBounds(10, 220, 80, 20);

        fldLocal.setMaxLength(60);
        pnlData.add(fldLocal);
        fldLocal.setBounds(10, 240, 400, 22);

        lblResponsavel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblResponsavel.setText(bundle.getString("BscReuniaoFrame_00017")); // NOI18N
        lblResponsavel.setEnabled(false);
        pnlData.add(lblResponsavel);
        lblResponsavel.setBounds(10, 260, 80, 20);

        fldResponsavel.setEnabled(false);
        fldResponsavel.setMaxLength(60);
        fldResponsavel.setSelectAllOnDoubleClick(true);
        pnlData.add(fldResponsavel);
        fldResponsavel.setBounds(10, 280, 400, 22);

        btnResponsavel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnResponsavel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnResponsavel.setEnabled(false);
        btnResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResponsavelActionPerformed(evt);
            }
        });
        pnlData.add(btnResponsavel);
        btnResponsavel.setBounds(420, 280, 30, 22);

        jTabbedPane1.addTab(bundle.getString("BscReuniaoFrame_00001"), pnlData); // NOI18N

        listaPessoas.setPreferredSize(new java.awt.Dimension(260, 491));
        jTabbedPane1.addTab(bundle.getString("BscReuniaoFrame_00011"), listaPessoas); // NOI18N

        scpRecord.setViewportView(jTabbedPane1);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscReuniaoFrame_00001"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscTarefaFrame_00022"), listaRetorno); // NOI18N
        tapCadastro.addTab(bundle.getString("BscTarefaFrame_00023"), listaDocumento); // NOI18N
        tapCadastro.addTab(bundle.getString("BscReuniaoFrame_00018"), listaPauta); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleName(bundle.getString("BscReuniaoFrame_00001")); // NOI18N
        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResponsavelActionPerformed
	java.util.Vector vector = new java.util.Vector();
	vector.add(tipoPessoa);
	vector.add(respId);
	vector.add(fldResponsavel.getText());
	String organizacao = "";

	java.util.Vector vctResponsavel = new java.util.Vector();
	vctResponsavel.add(vector);
	bsc.beans.JBIMultiSelectionDialog dialog = new bsc.beans.JBIMultiSelectionDialog( this, event, organizacao, vctResponsavel);
	dialog.show();

	java.util.Vector vctSelecionados = (java.util.Vector) vctResponsavel.get(0);
	tipoPessoa = vctSelecionados.get(0).toString();
	respId = vctSelecionados.get(1).toString();
	fldResponsavel.setText(vctSelecionados.get(2).toString());
    }//GEN-LAST:event_btnResponsavelActionPerformed

    private void btnNotifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotifyActionPerformed
	event.executeRecord("ENVIAR");
    }//GEN-LAST:event_btnNotifyActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
	boolean gravou = event.saveRecord( );
	BIvctAux = listaPessoas.getXMLData();

    if((gravou && (status == event.INSERTING || status == event.UPDATING) || !BIvctPessoas.equals(BIvctAux))){
	    bsc.swing.BscDefaultDialogSystem dialogSystem = bsc.core.BscStaticReferences.getBscDialogSystem();
	    // Deseja notificar as pessoas convocadas?
	    if(dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00027")) == 
			    javax.swing.JOptionPane.YES_OPTION){
		event.executeRecord("ENVIAR");
	    }
	}
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
	event.deleteRecord();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	event.loadRecord( );
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
	event.loadHelp("REUNIOES");
    }//GEN-LAST:event_btnReload1ActionPerformed
						
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNotify;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnResponsavel;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldAssunto;
    private pv.jfcx.JPVDatePlus fldData;
    private bsc.beans.JBITextArea fldDetalhes;
    private pv.jfcx.JPVTime fldHoraFim;
    private pv.jfcx.JPVTime fldHoraInicio;
    private pv.jfcx.JPVEdit fldLocal;
    private pv.jfcx.JPVEdit fldResponsavel;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblAssunto;
    private javax.swing.JLabel lblData;
    private javax.swing.JLabel lblDetalhes;
    private javax.swing.JLabel lblHoraFim;
    private javax.swing.JLabel lblHoraInicio;
    private javax.swing.JLabel lblLocal;
    private javax.swing.JLabel lblResponsavel;
    private bsc.beans.JBIListPanel listaDocumento;
    private bsc.beans.JBIListPanel listaPauta;
    private bsc.beans.JBIMultiSelectionPanel listaPessoas;
    private bsc.beans.JBIListPanel listaRetorno;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscReuniaoFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}	
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

}
