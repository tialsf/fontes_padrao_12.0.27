package bsc.swing.framework;


public class BscSMTPConfFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("SMTPCONF");

	public void enableFields() {
		// Habilita os campos do formul�rio.
		lblConta.setEnabled(true);
		fldConta.setEnabled(true);
		lblSMTP.setEnabled(true);
		fldSMTP.setEnabled(true);
		lblPortaSMTP.setEnabled(true);
		fldPortaSMTP.setEnabled(true);
		lblUsuario.setEnabled(true);
		fldUsuario.setEnabled(true);
		lblSenha.setEnabled(true);
		fldSenha1.setEnabled(true);
	}

	public void disableFields() {
		// Desabilita os campos do formul�rio.
		lblConta.setEnabled(false);
		fldConta.setEnabled(false);
		lblSMTP.setEnabled(false);
		fldSMTP.setEnabled(false);
		lblPortaSMTP.setEnabled(false);
		fldPortaSMTP.setEnabled(false);
		lblUsuario.setEnabled(false);
		fldUsuario.setEnabled(false);
		lblSenha.setEnabled(false);
		fldSenha1.setEnabled(false);
	}
	
	java.util.Hashtable htbResponsavel = new java.util.Hashtable();

	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		fldConta.setText(record.getString("NOME") );
		fldPortaSMTP.setText(record.getString("PORTA") );
		fldSMTP.setText(record.getString("SERVIDOR"));
		fldUsuario.setText(record.getString("USUARIO"));
                fldSenha1.setText(record.getString("SENHA"));

	}

	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		// Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
		// como retorno da fun��o.
		recordAux.set( "NOME", fldConta.getText() );
		recordAux.set( "SERVIDOR", fldSMTP.getText() );
		recordAux.set( "PORTA", fldPortaSMTP.getText() );
		recordAux.set( "USUARIO", fldUsuario.getText() );
                recordAux.set( "SENHA", fldSenha1.getText() );

		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
	    boolean retorno = true;
	    if(	fldConta.getText().trim().length() == 0 ){
		errorMessage.append( java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00022"));
		//"Conta deve ser informado.
		retorno = false;
	    }

	    if(	fldSMTP.getText().trim().length() == 0 ){
		errorMessage.append(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscReuniaoFrame_00023"));
		//"SMTP deve ser informada.");
		retorno = false;
	    }

	    return retorno;
	}
	
	public boolean hasCombos() {
		return false;
	}

	public void populateCombos(bsc.xml.BIXMLRecord serverSMTP) {
		return;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
            
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTopForm = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblConta = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblSMTP = new javax.swing.JLabel();
        lblPortaSMTP = new javax.swing.JLabel();
        fldConta = new pv.jfcx.JPVEdit();
        lblSenha = new javax.swing.JLabel();
        fldSMTP = new pv.jfcx.JPVEdit();
        fldPortaSMTP = new pv.jfcx.JPVEdit();
        fldUsuario = new pv.jfcx.JPVEdit();
        fldSenha1 = new javax.swing.JPasswordField();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscSMTPConfFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_agendamento.gif"))); // NOI18N
        setName(""); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        tapCadastro.setMinimumSize(new java.awt.Dimension(100, 100));
        tapCadastro.setPreferredSize(new java.awt.Dimension(530, 300));

        pnlFields.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(500, 400));

        pnlData.setPreferredSize(new java.awt.Dimension(200, 250));
        pnlData.setLayout(null);

        lblConta.setForeground(new java.awt.Color(51, 51, 255));
        lblConta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblConta.setText(bundle.getString("BscSMTPConfFrame_00003")); // NOI18N
        lblConta.setEnabled(false);
        pnlData.add(lblConta);
        lblConta.setBounds(10, 10, 90, 20);
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        lblConta.getAccessibleContext().setAccessibleName(bundle1.getString("BscSMTPConfFrame_00003")); // NOI18N

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUsuario.setText(bundle.getString("BscSMTPConfFrame_00006")); // NOI18N
        lblUsuario.setEnabled(false);
        pnlData.add(lblUsuario);
        lblUsuario.setBounds(10, 130, 90, 20);
        lblUsuario.getAccessibleContext().setAccessibleName(bundle1.getString("BscSMTPConfFrame_00006")); // NOI18N

        lblSMTP.setForeground(new java.awt.Color(51, 51, 255));
        lblSMTP.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSMTP.setText(bundle.getString("BscSMTPConfFrame_00004")); // NOI18N
        lblSMTP.setEnabled(false);
        pnlData.add(lblSMTP);
        lblSMTP.setBounds(10, 50, 90, 20);
        lblSMTP.getAccessibleContext().setAccessibleName(bundle1.getString("BscSMTPConfFrame_00004")); // NOI18N

        lblPortaSMTP.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPortaSMTP.setText(bundle.getString("BscSMTPConfFrame_00005")); // NOI18N
        lblPortaSMTP.setEnabled(false);
        pnlData.add(lblPortaSMTP);
        lblPortaSMTP.setBounds(10, 90, 90, 20);
        lblPortaSMTP.getAccessibleContext().setAccessibleName(bundle1.getString("BscSMTPConfFrame_00005")); // NOI18N

        fldConta.setMaxLength(60);
        pnlData.add(fldConta);
        fldConta.setBounds(10, 30, 400, 22);

        lblSenha.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSenha.setText(bundle.getString("BscSMTPConfFrame_00007")); // NOI18N
        lblSenha.setEnabled(false);
        pnlData.add(lblSenha);
        lblSenha.setBounds(10, 170, 90, 20);
        lblSenha.getAccessibleContext().setAccessibleName(bundle1.getString("BscSMTPConfFrame_00007")); // NOI18N

        fldSMTP.setMaxLength(60);
        pnlData.add(fldSMTP);
        fldSMTP.setBounds(10, 70, 400, 22);

        fldPortaSMTP.setMaxLength(60);
        pnlData.add(fldPortaSMTP);
        fldPortaSMTP.setBounds(10, 110, 400, 22);

        fldUsuario.setMaxLength(60);
        pnlData.add(fldUsuario);
        fldUsuario.setBounds(10, 150, 400, 22);

        fldSenha1.setText("jPasswordField1");
        pnlData.add(fldSenha1);
        fldSenha1.setBounds(10, 190, 400, 20);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle1.getString("BscEstrategiaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("BscEstrategiaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("BscEstrategiaFrame_00014")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("BscEstrategiaFrame_00005")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle1.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTools, java.awt.BorderLayout.NORTH);

        tapCadastro.addTab(bundle1.getString("BscSMTPConfFrame_00002"), pnlFields); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName("Mensagens");
        tapCadastro.getAccessibleContext().setAccessibleDescription("Mensagens");

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord( );
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
	event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
	event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
	event.loadRecord( );
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
	event.loadHelp("REUNIOES");
    }//GEN-LAST:event_btnReload1ActionPerformed
						
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldConta;
    private pv.jfcx.JPVEdit fldPortaSMTP;
    private pv.jfcx.JPVEdit fldSMTP;
    private javax.swing.JPasswordField fldSenha1;
    private pv.jfcx.JPVEdit fldUsuario;
    private javax.swing.JLabel lblConta;
    private javax.swing.JLabel lblPortaSMTP;
    private javax.swing.JLabel lblSMTP;
    private javax.swing.JLabel lblSenha;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
	new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscSMTPConfFrame(int operation, String idAux, String contextId) {
		initComponents();
		
		event.defaultConstructor( operation, idAux, contextId );
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}	
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return tapCadastro;
	}

}
