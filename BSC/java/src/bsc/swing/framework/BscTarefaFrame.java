package bsc.swing.framework;

import bsc.swing.BscDefaultFrameBehavior;
import java.util.ResourceBundle;

public class BscTarefaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("TAREFA");
    private boolean lResponsavel = false;

    public void enableFields() {

        /*Habilita todos os campos do formul�rio quando:
         *  O usu�rio corrente � o ADMINISTRADOR.
         *  A a��o realizada for INCLUS�O.
         *  O usu�rio corrente � o RESPONSAVEL pela iniciativa.*/

        if (record.getBoolean("ADMINISTRADOR") || (status == BscDefaultFrameBehavior.INSERTING) || record.getBoolean("ISRESP")) {

            lblNome.setEnabled(true);
            fldNome.setEnabled(true);
            lblLocal.setEnabled(true);
            fldLocal.setEnabled(true);
            fldHorasEst.setEnabled(true);
            fldTerceirizacaoEst.setEnabled(true);
            fldMateriaisEst.setEnabled(true);
            fldMaoDeObraEst.setEnabled(true);
            lblHorasEst.setEnabled(true);
            lblTerceirizacaoEst.setEnabled(true);
            lblMaoDeObraEst.setEnabled(true);
            lblMateriaisEst.setEnabled(true);
            fldHorasReal.setEnabled(true);
            fldTerceirizacaoReal.setEnabled(true);
            fldMateriaisReal.setEnabled(true);
            fldMaoDeObraReal.setEnabled(true);
            lblHorasReal.setEnabled(true);
            lblTerceirizacaoReal.setEnabled(true);
            lblMaoDeObraReal.setEnabled(true);
            lblMateriaisReal.setEnabled(true);
            lblDataInicio.setEnabled(true);
            fldDataInicio.setEnabled(true);
            lblDataFim.setEnabled(true);
            fldDataFim.setEnabled(true);
            lblTexto.setEnabled(true);
            txtTexto.setEnabled(true);
            listaPessoas.setEnabled(true);
            lblPorcento.setEnabled(true);
            lblImportancia.setEnabled(true);
            cbbImportancia.setEnabled(true);
            lblUrgencia.setEnabled(true);
            cbbUrgencia.setEnabled(true);
        }
        lblCompletado.setEnabled(true);
        fldCompletado.setEnabled(true);
        lblSituacao.setEnabled(true);
        cbbSituacao.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblLocal.setEnabled(false);
        fldLocal.setEnabled(false);
        fldHorasEst.setEnabled(false);
        fldTerceirizacaoEst.setEnabled(false);
        fldMateriaisEst.setEnabled(false);
        fldMaoDeObraEst.setEnabled(false);
        lblHorasEst.setEnabled(false);
        lblTerceirizacaoEst.setEnabled(false);
        lblMaoDeObraEst.setEnabled(false);
        lblMateriaisEst.setEnabled(false);
        fldHorasReal.setEnabled(false);
        fldTerceirizacaoReal.setEnabled(false);
        fldMateriaisReal.setEnabled(false);
        fldMaoDeObraReal.setEnabled(false);
        lblHorasReal.setEnabled(false);
        lblTerceirizacaoReal.setEnabled(false);
        lblMaoDeObraReal.setEnabled(false);
        lblMateriaisReal.setEnabled(false);
        lblCompletado.setEnabled(false);
        fldCompletado.setEnabled(false);
        lblSituacao.setEnabled(false);
        cbbSituacao.setEnabled(false);
        lblDataInicio.setEnabled(false);
        fldDataInicio.setEnabled(false);
        lblDataFim.setEnabled(false);
        fldDataFim.setEnabled(false);
        lblTexto.setEnabled(false);
        txtTexto.setEnabled(false);
        listaPessoas.setEnabled(false);
        lblPorcento.setEnabled(false);
        lblImportancia.setEnabled(false);
        cbbImportancia.setEnabled(false);
        lblUrgencia.setEnabled(false);
        cbbUrgencia.setEnabled(false);
    }
    java.util.Hashtable htbSituacao = new java.util.Hashtable();
    java.util.Hashtable htbImportancia = new java.util.Hashtable();
    java.util.Hashtable htbUrgencia = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        fldLocal.setText(record.getString("LOCAL"));
        txtTexto.setText(record.getString("TEXTO"));
        fldDataInicio.setCalendar(record.getDate("DATAINI"));
        fldDataFim.setCalendar(record.getDate("DATAFIN"));
        ((pv.jfcx.JPVNumeric) fldHorasEst.getObject()).setText(record.getString("HORASEST"));

        fldTerceirizacaoEst.setDouble(record.getDouble("CE_TERCEIR"));
        fldTerceirizacaoReal.setDouble(record.getDouble("CR_TERCEIR"));
        fldMateriaisEst.setDouble(record.getDouble("CE_MATERIA"));
        fldMateriaisReal.setDouble(record.getDouble("CR_MATERIA"));
        fldMaoDeObraEst.setDouble(record.getDouble("CE_MAOOBRA"));
        fldMaoDeObraReal.setDouble(record.getDouble("CR_MAOOBRA"));

        ((pv.jfcx.JPVNumeric) fldHorasReal.getObject()).setText(record.getString("HORASREAL"));
        ((pv.jfcx.JPVNumeric) fldCompletado.getObject()).setText(record.getString("COMPLETADO"));

        listaDocumento.setDataSource(record.getBIXMLVector("TARDOCS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        listaPessoas.setDataSource(record.getBIXMLVector("TARCOBS"), "TARCOB", event);
        listaRetorno.setDataSource(record.getBIXMLVector("RETORNOS"), record.getString("ID"),
                record.getString("CONTEXTID"), this.event);
        event.populateCombo(record.getBIXMLVector("SITUACOES"), "SITID", htbSituacao, cbbSituacao);
        event.populateCombo(record.getBIXMLVector("URGENCIAS"), "URGENCIA", htbUrgencia, cbbUrgencia);
        event.populateCombo(record.getBIXMLVector("IMPORTANCIAS"), "IMPORTANCI", htbImportancia, cbbImportancia);

        bsc.xml.BIXMLVector vctContexto = record.getBIXMLVector("CONTEXTOS");

        pnlHeader.setComponents(vctContexto);
        pnlTopForm.doLayout();

        //retira a exclusao se n�o for administrador
        btnDelete.setVisible(record.getBoolean("ADMINISTRADOR"));

        //retira a altera��o se n�o for administrador ou respons�vel
        if (!record.getBoolean("RESPONSAVEL") && !record.getBoolean("ADMINISTRADOR") && !record.getBoolean("ISRESP")) {
            btnEdit.setVisible(false);
            btnDelete.setVisible(false);
        }
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("LOCAL", fldLocal.getText());
        recordAux.set("TEXTO", txtTexto.getText());
        recordAux.set("DATAINI", fldDataInicio.getCalendar());
        recordAux.set("DATAFIN", fldDataFim.getCalendar());
        recordAux.set("HORASEST", ((pv.jfcx.JPVNumeric) fldHorasEst.getObject()).getText());
        recordAux.set("CE_TERCEIR", fldTerceirizacaoEst.getDouble());
        recordAux.set("CE_MATERIA", fldMateriaisEst.getDouble());
        recordAux.set("CE_MAOOBRA", fldMaoDeObraEst.getDouble());
        recordAux.set("HORASREAL", ((pv.jfcx.JPVNumeric) fldHorasReal.getObject()).getText());
        recordAux.set("CR_TERCEIR", fldTerceirizacaoReal.getDouble());
        recordAux.set("CR_MATERIA", fldMateriaisReal.getDouble());
        recordAux.set("CR_MAOOBRA", fldMaoDeObraReal.getDouble());
        recordAux.set("COMPLETADO", ((pv.jfcx.JPVNumeric) fldCompletado.getObject()).getText());
        recordAux.set("SITID", event.getComboValue(htbSituacao, cbbSituacao));
        recordAux.set("IMPORTANCI", event.getComboValue(htbImportancia, cbbImportancia));
        recordAux.set("URGENCIA", event.getComboValue(htbUrgencia, cbbUrgencia));
        recordAux.set(listaPessoas.getXMLData());

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return true;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblHorasEst = new javax.swing.JLabel();
        lblTerceirizacaoEst = new javax.swing.JLabel();
        lblMaoDeObraEst = new javax.swing.JLabel();
        lblMateriaisEst = new javax.swing.JLabel();
        fldMateriaisEst = new pv.jfcx.JPVNumericPlus();
        fldTerceirizacaoEst = new pv.jfcx.JPVNumericPlus();
        fldHorasEst = new pv.jfcx.JPVSpinPlus();
        fldMaoDeObraEst = new pv.jfcx.JPVNumericPlus();
        jPanel11 = new javax.swing.JPanel();
        lblHorasReal = new javax.swing.JLabel();
        lblTerceirizacaoReal = new javax.swing.JLabel();
        lblMaoDeObraReal = new javax.swing.JLabel();
        lblMateriaisReal = new javax.swing.JLabel();
        fldMaoDeObraReal = new pv.jfcx.JPVNumericPlus();
        fldMateriaisReal = new pv.jfcx.JPVNumericPlus();
        fldTerceirizacaoReal = new pv.jfcx.JPVNumericPlus();
        fldHorasReal = new pv.jfcx.JPVSpinPlus();
        lblNome = new javax.swing.JLabel();
        lblCompletado = new javax.swing.JLabel();
        lblSituacao = new javax.swing.JLabel();
        lblTexto = new javax.swing.JLabel();
        jScrollPane31 = new javax.swing.JScrollPane();
        txtTexto = new bsc.beans.JBITextArea();
        lblDataFim = new javax.swing.JLabel();
        lblDataInicio = new javax.swing.JLabel();
        cbbSituacao = new javax.swing.JComboBox();
        lblPorcento = new javax.swing.JLabel();
        fldDataInicio = new pv.jfcx.JPVDatePlus();
        fldDataFim = new pv.jfcx.JPVDatePlus();
        fldNome = new pv.jfcx.JPVEdit();
        fldCompletado = new pv.jfcx.JPVSpinPlus();
        lblLocal = new javax.swing.JLabel();
        fldLocal = new pv.jfcx.JPVEdit();
        lblImportancia = new javax.swing.JLabel();
        cbbImportancia = new javax.swing.JComboBox();
        lblUrgencia = new javax.swing.JLabel();
        cbbUrgencia = new javax.swing.JComboBox();
        listaPessoas = new bsc.beans.JBIMultiSelectionPanel();
        listaRetorno = new bsc.beans.JBIListPanel();
        listaDocumento = new bsc.beans.JBIListPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscTarefaFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_tarefa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscTarefaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscTarefaFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscTarefaFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscTarefaFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscTarefaFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        scpRecord.setBorder(null);

        pnlData.setPreferredSize(new java.awt.Dimension(500, 550));
        pnlData.setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("BscTarefaFrame_00025"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel1.setLayout(null);

        lblHorasEst.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHorasEst.setText(bundle.getString("BscTarefaFrame_00007")); // NOI18N
        lblHorasEst.setEnabled(false);
        jPanel1.add(lblHorasEst);
        lblHorasEst.setBounds(10, 140, 92, 20);

        lblTerceirizacaoEst.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerceirizacaoEst.setText(bundle.getString("BscTarefaFrame_00008")); // NOI18N
        lblTerceirizacaoEst.setEnabled(false);
        jPanel1.add(lblTerceirizacaoEst);
        lblTerceirizacaoEst.setBounds(10, 100, 94, 20);

        lblMaoDeObraEst.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMaoDeObraEst.setText(bundle.getString("BscTarefaFrame_00009")); // NOI18N
        lblMaoDeObraEst.setEnabled(false);
        jPanel1.add(lblMaoDeObraEst);
        lblMaoDeObraEst.setBounds(10, 20, 94, 20);

        lblMateriaisEst.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMateriaisEst.setText(bundle.getString("BscTarefaFrame_00010")); // NOI18N
        lblMateriaisEst.setEnabled(false);
        jPanel1.add(lblMateriaisEst);
        lblMateriaisEst.setBounds(10, 60, 94, 20);

        fldMateriaisEst.setAlignment(2);
        fldMateriaisEst.setDecimalSeparator(",");
        fldMateriaisEst.setDialog(true);
        fldMateriaisEst.setMaxDecimals(2);
        fldMateriaisEst.setUseLocale(true);
        jPanel1.add(fldMateriaisEst);
        fldMateriaisEst.setBounds(10, 80, 180, 22);

        fldTerceirizacaoEst.setAlignment(2);
        fldTerceirizacaoEst.setDecimalSeparator(",");
        fldTerceirizacaoEst.setDialog(true);
        fldTerceirizacaoEst.setMaxDecimals(2);
        fldTerceirizacaoEst.setUseLocale(true);
        jPanel1.add(fldTerceirizacaoEst);
        fldTerceirizacaoEst.setBounds(10, 120, 180, 22);

        fldHorasEst.setButtonWidth(15);
        fldHorasEst.setEnabled(false);
        fldHorasEst.setForeground(new java.awt.Color(0, 0, 0));
        fldHorasEst.setPVObject(1);
        fldHorasEst.setStyle(9);
        jPanel1.add(fldHorasEst);
        fldHorasEst.setBounds(10, 160, 180, 22);

        fldMaoDeObraEst.setAlignment(2);
        fldMaoDeObraEst.setDecimalSeparator(",");
        fldMaoDeObraEst.setDialog(true);
        fldMaoDeObraEst.setMaxDecimals(2);
        fldMaoDeObraEst.setUseLocale(true);
        jPanel1.add(fldMaoDeObraEst);
        fldMaoDeObraEst.setBounds(10, 40, 180, 22);

        pnlData.add(jPanel1);
        jPanel1.setBounds(230, 320, 200, 190);
        jPanel1.getAccessibleContext().setAccessibleName(bundle.getString("BscTarefaFrame_00025")); // NOI18N

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("BscTarefaFrame_00026"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        jPanel11.setLayout(null);

        lblHorasReal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHorasReal.setText(bundle.getString("BscTarefaFrame_00011")); // NOI18N
        lblHorasReal.setEnabled(false);
        jPanel11.add(lblHorasReal);
        lblHorasReal.setBounds(10, 140, 94, 20);

        lblTerceirizacaoReal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerceirizacaoReal.setText(bundle.getString("BscTarefaFrame_00012")); // NOI18N
        lblTerceirizacaoReal.setEnabled(false);
        jPanel11.add(lblTerceirizacaoReal);
        lblTerceirizacaoReal.setBounds(10, 100, 92, 20);

        lblMaoDeObraReal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMaoDeObraReal.setText(bundle.getString("BscTarefaFrame_00013")); // NOI18N
        lblMaoDeObraReal.setEnabled(false);
        jPanel11.add(lblMaoDeObraReal);
        lblMaoDeObraReal.setBounds(12, 24, 92, 14);

        lblMateriaisReal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMateriaisReal.setText(bundle.getString("BscTarefaFrame_00014")); // NOI18N
        lblMateriaisReal.setEnabled(false);
        jPanel11.add(lblMateriaisReal);
        lblMateriaisReal.setBounds(10, 60, 92, 20);

        fldMaoDeObraReal.setAlignment(2);
        fldMaoDeObraReal.setDecimalSeparator(",");
        fldMaoDeObraReal.setDialog(true);
        fldMaoDeObraReal.setMaxDecimals(2);
        fldMaoDeObraReal.setUseLocale(true);
        jPanel11.add(fldMaoDeObraReal);
        fldMaoDeObraReal.setBounds(10, 40, 180, 22);

        fldMateriaisReal.setAlignment(2);
        fldMateriaisReal.setDecimalSeparator(",");
        fldMateriaisReal.setDialog(true);
        fldMateriaisReal.setMaxDecimals(2);
        fldMateriaisReal.setUseLocale(true);
        jPanel11.add(fldMateriaisReal);
        fldMateriaisReal.setBounds(10, 80, 180, 22);

        fldTerceirizacaoReal.setAlignment(2);
        fldTerceirizacaoReal.setDecimalSeparator(",");
        fldTerceirizacaoReal.setDialog(true);
        fldTerceirizacaoReal.setMaxDecimals(2);
        fldTerceirizacaoReal.setUseLocale(true);
        jPanel11.add(fldTerceirizacaoReal);
        fldTerceirizacaoReal.setBounds(10, 120, 180, 22);

        fldHorasReal.setButtonWidth(15);
        fldHorasReal.setForeground(new java.awt.Color(0, 0, 0));
        fldHorasReal.setPVObject(1);
        fldHorasReal.setStyle(9);
        jPanel11.add(fldHorasReal);
        fldHorasReal.setBounds(10, 160, 180, 22);

        pnlData.add(jPanel11);
        jPanel11.setBounds(10, 320, 200, 190);
        jPanel11.getAccessibleContext().setAccessibleName(bundle.getString("BscTarefaFrame_00026")); // NOI18N

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscTarefaFrame_00015")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 10, 98, 20);

        lblCompletado.setText(bundle.getString("BscTarefaFrame_00016")); // NOI18N
        lblCompletado.setEnabled(false);
        pnlData.add(lblCompletado);
        lblCompletado.setBounds(240, 260, 78, 18);

        lblSituacao.setText(bundle.getString("BscTarefaFrame_00017")); // NOI18N
        lblSituacao.setEnabled(false);
        pnlData.add(lblSituacao);
        lblSituacao.setBounds(20, 260, 66, 18);

        lblTexto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTexto.setText(bundle.getString("BscTarefaFrame_00018")); // NOI18N
        lblTexto.setEnabled(false);
        pnlData.add(lblTexto);
        lblTexto.setBounds(20, 50, 100, 18);

        jScrollPane31.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane31.setViewportView(txtTexto);

        pnlData.add(jScrollPane31);
        jScrollPane31.setBounds(20, 70, 400, 60);

        lblDataFim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataFim.setText(bundle.getString("BscTarefaFrame_00019")); // NOI18N
        lblDataFim.setEnabled(false);
        pnlData.add(lblDataFim);
        lblDataFim.setBounds(240, 170, 102, 18);

        lblDataInicio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataInicio.setText(bundle.getString("BscTarefaFrame_00020")); // NOI18N
        lblDataInicio.setEnabled(false);
        pnlData.add(lblDataInicio);
        lblDataInicio.setBounds(20, 170, 102, 18);
        pnlData.add(cbbSituacao);
        cbbSituacao.setBounds(20, 280, 180, 22);

        lblPorcento.setText("%");
        lblPorcento.setEnabled(false);
        pnlData.add(lblPorcento);
        lblPorcento.setBounds(430, 280, 30, 20);

        fldDataInicio.setDialog(true);
        fldDataInicio.setUseLocale(true);
        fldDataInicio.setWaitForCalendarDate(true);
        pnlData.add(fldDataInicio);
        fldDataInicio.setBounds(20, 190, 180, 22);

        fldDataFim.setDialog(true);
        fldDataFim.setFormat(1);
        fldDataFim.setUseLocale(true);
        fldDataFim.setWaitForCalendarDate(true);
        pnlData.add(fldDataFim);
        fldDataFim.setBounds(240, 190, 180, 22);

        fldNome.setMaxLength(120);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 30, 400, 22);

        fldCompletado.setButtonWidth(15);
        fldCompletado.setEnabled(false);
        fldCompletado.setForeground(new java.awt.Color(0, 0, 0));
        fldCompletado.setPVObject(1);
        fldCompletado.setStyle(9);
        pnlData.add(fldCompletado);
        fldCompletado.setBounds(240, 280, 180, 22);

        lblLocal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLocal.setText(bundle.getString("BscTarefaFrame_00024")); // NOI18N
        lblLocal.setEnabled(false);
        pnlData.add(lblLocal);
        lblLocal.setBounds(20, 130, 98, 18);

        fldLocal.setMaxLength(20);
        pnlData.add(fldLocal);
        fldLocal.setBounds(20, 150, 400, 22);

        lblImportancia.setText(bundle.getString("BscTarefaFrame_00027")); // NOI18N
        lblImportancia.setEnabled(false);
        pnlData.add(lblImportancia);
        lblImportancia.setBounds(20, 210, 96, 18);
        pnlData.add(cbbImportancia);
        cbbImportancia.setBounds(20, 230, 180, 22);

        lblUrgencia.setText(bundle.getString("BscTarefaFrame_00028")); // NOI18N
        lblUrgencia.setEnabled(false);
        pnlData.add(lblUrgencia);
        lblUrgencia.setBounds(240, 210, 96, 18);
        pnlData.add(cbbUrgencia);
        cbbUrgencia.setBounds(240, 230, 180, 22);

        scpRecord.setViewportView(pnlData);

        jTabbedPane1.addTab("Atributos", scpRecord);

        listaPessoas.setFocusTraversalKeysEnabled(false);
        jTabbedPane1.addTab(bundle.getString("BscTarefaFrame_00021"), listaPessoas); // NOI18N

        pnlFields.add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscTarefaFrame_00001"), pnlRecord); // NOI18N
        tapCadastro.addTab(bundle.getString("BscTarefaFrame_00022"), listaRetorno); // NOI18N
        tapCadastro.addTab(bundle.getString("BscTarefaFrame_00023"), listaDocumento); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setMinimumSize(new java.awt.Dimension(130, 75));
        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 75));
        pnlTopForm.setLayout(new java.awt.BorderLayout());
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("CADASTRANDO_INICIATIVAS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();
            if (!(mainPanel.getMenuSelecionado().equals(mainPanel.ST_AREATRABALHO)) || record.getBoolean("ADMINISTRADOR")) {
                event.deleteRecord();
            }
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbImportancia;
    private javax.swing.JComboBox cbbSituacao;
    private javax.swing.JComboBox cbbUrgencia;
    private pv.jfcx.JPVSpinPlus fldCompletado;
    private pv.jfcx.JPVDatePlus fldDataFim;
    private pv.jfcx.JPVDatePlus fldDataInicio;
    private pv.jfcx.JPVSpinPlus fldHorasEst;
    private pv.jfcx.JPVSpinPlus fldHorasReal;
    private pv.jfcx.JPVEdit fldLocal;
    private pv.jfcx.JPVNumericPlus fldMaoDeObraEst;
    private pv.jfcx.JPVNumericPlus fldMaoDeObraReal;
    private pv.jfcx.JPVNumericPlus fldMateriaisEst;
    private pv.jfcx.JPVNumericPlus fldMateriaisReal;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVNumericPlus fldTerceirizacaoEst;
    private pv.jfcx.JPVNumericPlus fldTerceirizacaoReal;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JScrollPane jScrollPane31;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblCompletado;
    private javax.swing.JLabel lblDataFim;
    private javax.swing.JLabel lblDataInicio;
    private javax.swing.JLabel lblHorasEst;
    private javax.swing.JLabel lblHorasReal;
    private javax.swing.JLabel lblImportancia;
    private javax.swing.JLabel lblLocal;
    private javax.swing.JLabel lblMaoDeObraEst;
    private javax.swing.JLabel lblMaoDeObraReal;
    private javax.swing.JLabel lblMateriaisEst;
    private javax.swing.JLabel lblMateriaisReal;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPorcento;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JLabel lblTerceirizacaoEst;
    private javax.swing.JLabel lblTerceirizacaoReal;
    private javax.swing.JLabel lblTexto;
    private javax.swing.JLabel lblUrgencia;
    private bsc.beans.JBIListPanel listaDocumento;
    private bsc.beans.JBIMultiSelectionPanel listaPessoas;
    private bsc.beans.JBIListPanel listaRetorno;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtTexto;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscTarefaFrame(int operation, String idAux, String contextId) {
        initComponents();

        //pvnumeric
        pv.jfcx.JPVNumeric num = ((pv.jfcx.JPVNumeric) fldCompletado.getObject());
        num.setUpperValidation(true);
        num.setLowerValidation(true);
        num.setMaxValue(100);
        num.setMinValue(0);
        num.setToLimit(true);
        num.setAlignment(2);
        num.setDelta(5d);

        pv.jfcx.JPVNumeric num1 = ((pv.jfcx.JPVNumeric) fldHorasEst.getObject());
        num1.setUpperValidation(true);
        num1.setLowerValidation(true);
        num1.setMinValue(0);
        num1.setToLimit(true);
        num1.setAlignment(2);

        pv.jfcx.JPVNumeric num2 = ((pv.jfcx.JPVNumeric) fldHorasReal.getObject());
        num2.setUpperValidation(true);
        num2.setLowerValidation(true);
        num2.setMinValue(0);
        num2.setToLimit(true);
        num2.setAlignment(2);

        event.defaultConstructor(operation, idAux, contextId);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
