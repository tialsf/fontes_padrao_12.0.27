package bsc.swing.framework;

public class BscUsuarioFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /*****************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /*****************************************************************************/
    private String type = new String("USUARIO");
    private String password = new String("");

    public void enableFields() {
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        cbbNome.setEnabled(true);
        lblSenha.setEnabled(true);

        btnAlterarSenha.setEnabled(!chkUsuarioProtheus.isSelected());
        fldNome.setVisible(!chkUsuarioProtheus.isSelected());
        cbbNome.setVisible(chkUsuarioProtheus.isSelected());

        chkAdministrador.setEnabled(true);
        chkUpdTree.setEnabled(true);
        chkUsuarioProtheus.setEnabled(true);

        lblPrimNome.setEnabled(true);
        fldPrimNome.setEnabled(true);

        lblCargo.setEnabled(true);
        fldCargo.setEnabled(true);

        lblTelefone.setEnabled(true);
        fldTelefone.setEnabled(true);

        lblRamal.setEnabled(true);
        fldRamal.setEnabled(true);

        lblEmail.setEnabled(true);
        fldEmail.setEnabled(true);
    }

    public void disableFields() {
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        cbbNome.setEnabled(false);
        fldNome.setVisible(!chkUsuarioProtheus.isSelected());
        cbbNome.setVisible(chkUsuarioProtheus.isSelected());

        lblSenha.setEnabled(false);
        btnAlterarSenha.setEnabled(false);

        chkAdministrador.setEnabled(false);
        chkUpdTree.setEnabled(false);
        chkUsuarioProtheus.setEnabled(false);

        lblPrimNome.setEnabled(false);
        fldPrimNome.setEnabled(false);

        lblCargo.setEnabled(false);
        fldCargo.setEnabled(false);

        lblTelefone.setEnabled(false);
        fldTelefone.setEnabled(false);

        lblRamal.setEnabled(false);
        fldRamal.setEnabled(false);

        lblEmail.setEnabled(false);
        fldEmail.setEnabled(false);
    }
    java.util.Hashtable htbNome = new java.util.Hashtable();

    public void refreshFields() {

        chkAdministrador.setSelected(record.getBoolean("ADMIN"));
        chkUpdTree.setSelected(record.getBoolean("UPDTREE"));
        chkUsuarioProtheus.setSelected(record.getBoolean("USERPROT"));

        event.populateComboWithName(record.getBIXMLVector("NOMES"), "NOME", htbNome, cbbNome);
        fldNome.setText(record.getString("NOME"));

        // Altera senha se n�o for usu�rio do Protheus
        if (chkUsuarioProtheus.isSelected()) {
            fldNome.setVisible(false);
            cbbNome.setVisible(true);
        } else {
            cbbNome.setVisible(false);
            fldNome.setVisible(true);
        }

        fldPrimNome.setText(record.getString("COMPNOME"));
        fldCargo.setText(record.getString("CARGO"));
        fldTelefone.setText(record.getString("FONE"));
        fldRamal.setText(record.getString("RAMAL"));
        fldEmail.setText(record.getString("EMAIL"));
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        if (record.contains("AUTENTIC")) {
            recordAux.set("AUTENTIC", record.getString("AUTENTIC"));
        } else {
            recordAux.set("AUTENTIC", "0");
        }

        if (chkUsuarioProtheus.isSelected()) {
            recordAux.set("NOME", (String) cbbNome.getSelectedItem());
        } else {
            recordAux.set("NOME", fldNome.getText());
        }
        recordAux.set("ADMIN", chkAdministrador.isSelected());
        recordAux.set("USERPROT", chkUsuarioProtheus.isSelected());
        recordAux.set("UPDTREE", chkUpdTree.isSelected());
        recordAux.set("ID", id);
        recordAux.set("SENHA", password);
        recordAux.set("COMPNOME", fldPrimNome.getText());
        recordAux.set("CARGO", fldCargo.getText());
        recordAux.set("FONE", fldTelefone.getText());
        recordAux.set("RAMAL", fldRamal.getText());
        recordAux.set("EMAIL", fldEmail.getText());

        return recordAux;
    }

    public void setPassword(String passwd) {
        password = String.valueOf(passwd);
        lblPasswordEdited.setVisible(true);
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean lRet = true;

        if (chkUsuarioProtheus.isSelected()) {
            if (((String) cbbNome.getSelectedItem()).trim().length() == 0) {
                errorMessage.append("Nome n�o informado!");
                lRet = false;
            }
        } else {
            if (fldNome.getText().trim().length() == 0) {
                errorMessage.append("Nome n�o informado!");
                lRet = false;
            }
        }
        return lRet;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnRules = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        tapCadastro = new javax.swing.JTabbedPane();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblSenha = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        btnAlterarSenha = new javax.swing.JButton();
        lblPasswordEdited = new javax.swing.JLabel();
        lblPrimNome = new javax.swing.JLabel();
        lblCargo = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        lblRamal = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        fldPrimNome = new pv.jfcx.JPVEdit();
        fldCargo = new pv.jfcx.JPVEdit();
        fldTelefone = new pv.jfcx.JPVMask();
        fldRamal = new pv.jfcx.JPVMask();
        fldEmail = new pv.jfcx.JPVEdit();
        cbbNome = new javax.swing.JComboBox();
        chkAdministrador = new javax.swing.JCheckBox();
        chkUsuarioProtheus = new javax.swing.JCheckBox();
        chkUpdTree = new javax.swing.JCheckBox();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscUsuarioFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_usuario.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 660, 395));
        setPreferredSize(new java.awt.Dimension(543, 375));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 25));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscUsuarioFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_x_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscUsuarioFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(370, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(370, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(400, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setMaximumSize(new java.awt.Dimension(350, 19));
        tbInsertion.setMinimumSize(new java.awt.Dimension(350, 19));
        tbInsertion.setPreferredSize(new java.awt.Dimension(400, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscUsuarioFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscUsuarioFrame_00005")); // NOI18N
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnRules.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnRules.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_seguranca.gif"))); // NOI18N
        btnRules.setText(bundle.getString("BscBaseFrame_00007")); // NOI18N
        btnRules.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnRules.setFocusable(false);
        btnRules.setMaximumSize(new java.awt.Dimension(86, 20));
        btnRules.setMinimumSize(new java.awt.Dimension(86, 20));
        btnRules.setPreferredSize(new java.awt.Dimension(86, 20));
        btnRules.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRules.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRulesActionPerformed(evt);
            }
        });
        tbInsertion.add(btnRules);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscUsuarioFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        scpRecord.setBorder(null);
        scpRecord.setMaximumSize(new java.awt.Dimension(374, 276));
        scpRecord.setMinimumSize(new java.awt.Dimension(374, 276));
        scpRecord.setPreferredSize(new java.awt.Dimension(374, 276));

        pnlData.setAutoscrolls(true);
        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblSenha.setForeground(new java.awt.Color(51, 51, 255));
        lblSenha.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSenha.setText(bundle.getString("BscUsuarioFrame_00016")); // NOI18N
        lblSenha.setEnabled(false);
        lblSenha.setMaximumSize(new java.awt.Dimension(100, 16));
        lblSenha.setMinimumSize(new java.awt.Dimension(100, 16));
        lblSenha.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblSenha);
        lblSenha.setBounds(330, 70, 100, 20);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscUsuarioFrame_00015")); // NOI18N
        lblNome.setEnabled(false);
        pnlData.add(lblNome);
        lblNome.setBounds(20, 70, 94, 20);

        btnAlterarSenha.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAlterarSenha.setText(bundle.getString("BscUsuarioFrame_00007")); // NOI18N
        btnAlterarSenha.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAlterarSenha.setEnabled(false);
        btnAlterarSenha.setIconTextGap(0);
        btnAlterarSenha.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnAlterarSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarSenhaActionPerformed(evt);
            }
        });
        pnlData.add(btnAlterarSenha);
        btnAlterarSenha.setBounds(330, 90, 90, 22);

        lblPasswordEdited.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        pnlData.add(lblPasswordEdited);
        lblPasswordEdited.setBounds(430, 90, 16, 20);

        lblPrimNome.setForeground(new java.awt.Color(51, 51, 255));
        lblPrimNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPrimNome.setText(bundle.getString("BscUsuarioFrame_00009")); // NOI18N
        lblPrimNome.setEnabled(false);
        lblPrimNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPrimNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPrimNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblPrimNome);
        lblPrimNome.setBounds(20, 110, 100, 20);

        lblCargo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCargo.setText(bundle.getString("BscUsuarioFrame_00010")); // NOI18N
        lblCargo.setEnabled(false);
        lblCargo.setMaximumSize(new java.awt.Dimension(100, 16));
        lblCargo.setMinimumSize(new java.awt.Dimension(100, 16));
        lblCargo.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblCargo);
        lblCargo.setBounds(20, 150, 100, 20);

        lblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTelefone.setText(bundle.getString("BscUsuarioFrame_00011")); // NOI18N
        lblTelefone.setEnabled(false);
        lblTelefone.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTelefone.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTelefone);
        lblTelefone.setBounds(20, 190, 100, 20);

        lblRamal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRamal.setText(bundle.getString("BscUsuarioFrame_00012")); // NOI18N
        lblRamal.setEnabled(false);
        lblRamal.setMaximumSize(new java.awt.Dimension(100, 16));
        lblRamal.setMinimumSize(new java.awt.Dimension(100, 16));
        lblRamal.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblRamal);
        lblRamal.setBounds(20, 230, 100, 20);

        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEmail.setText(bundle.getString("BscUsuarioFrame_00013")); // NOI18N
        lblEmail.setEnabled(false);
        lblEmail.setMaximumSize(new java.awt.Dimension(100, 16));
        lblEmail.setMinimumSize(new java.awt.Dimension(100, 16));
        lblEmail.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblEmail);
        lblEmail.setBounds(20, 270, 100, 20);

        fldNome.setMaxLength(25);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 90, 180, 22);

        fldPrimNome.setMaxLength(30);
        pnlData.add(fldPrimNome);
        fldPrimNome.setBounds(20, 130, 400, 22);

        fldCargo.setMaxLength(20);
        pnlData.add(fldCargo);
        fldCargo.setBounds(20, 170, 400, 22);

        fldTelefone.setMask("(###) #####-####");
        fldTelefone.setText("(   )       -    ");
        pnlData.add(fldTelefone);
        fldTelefone.setBounds(20, 210, 180, 22);

        fldRamal.setMask("##########");
        pnlData.add(fldRamal);
        fldRamal.setBounds(20, 250, 180, 22);

        fldEmail.setMaxLength(80);
        pnlData.add(fldEmail);
        fldEmail.setBounds(20, 290, 400, 22);

        cbbNome.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbbNome.setVerifyInputWhenFocusTarget(false);
        pnlData.add(cbbNome);
        cbbNome.setBounds(20, 90, 180, 22);

        chkAdministrador.setText(bundle.getString("BscUsuarioFrame_00024")); // NOI18N
        chkAdministrador.setEnabled(false);
        pnlData.add(chkAdministrador);
        chkAdministrador.setBounds(20, 40, 100, 16);

        chkUsuarioProtheus.setText(bundle.getString("BscUsuarioFrame_00018")); // NOI18N
        chkUsuarioProtheus.setActionCommand("UsuarioProtheus");
        chkUsuarioProtheus.setEnabled(false);
        chkUsuarioProtheus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkUsuarioProtheusActionPerformed(evt);
            }
        });
        pnlData.add(chkUsuarioProtheus);
        chkUsuarioProtheus.setBounds(20, 20, 120, 23);
        chkUsuarioProtheus.getAccessibleContext().setAccessibleName("UsuarioProtheus");

        chkUpdTree.setText(bundle.getString("BscUsuarioFrame_00026")); // NOI18N
        chkUpdTree.setEnabled(false);
        chkUpdTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkUpdTreeActionPerformed(evt);
            }
        });
        pnlData.add(chkUpdTree);
        chkUpdTree.setBounds(20, 330, 260, 23);

        scpRecord.setViewportView(pnlData);

        tapCadastro.addTab(bundle.getString("BscUsuarioFrame_00001"), scpRecord); // NOI18N

        pnlFields.add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pnlTopForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlTopForm.setOpaque(false);
        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomForm.setOpaque(false);
        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightForm.setOpaque(false);
        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftForm.setOpaque(false);
        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void chkUsuarioProtheusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkUsuarioProtheusActionPerformed
            btnAlterarSenha.setEnabled(!chkUsuarioProtheus.isSelected());
            fldNome.setVisible(!chkUsuarioProtheus.isSelected());
            cbbNome.setVisible(chkUsuarioProtheus.isSelected());
	}//GEN-LAST:event_chkUsuarioProtheusActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("USUARIOS");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnAlterarSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarSenhaActionPerformed
            BscSenhaDialog dialog = new BscSenhaDialog(this);

            if (dialog.isNewPassword()) {
                setPassword(dialog.getPassword());
            }

            dialog.dispose();
	}//GEN-LAST:event_btnAlterarSenhaActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
            lblPasswordEdited.setVisible(false);
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
            lblPasswordEdited.setVisible(false);
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
            event.deleteRecord();
	}//GEN-LAST:event_btnDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed

    private void btnRulesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRulesActionPerformed
        BscUsuarioPerm frame = (BscUsuarioPerm) bsc.core.BscStaticReferences.getBscFormController().getForm("USERPERM", getID(), fldNome.getText()).asJInternalFrame();
        frame.configureUsuarioPerm("U", getID());
}//GEN-LAST:event_btnRulesActionPerformed

    private void chkUpdTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkUpdTreeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkUpdTreeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterarSenha;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnRules;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbNome;
    private javax.swing.JCheckBox chkAdministrador;
    private javax.swing.JCheckBox chkUpdTree;
    private javax.swing.JCheckBox chkUsuarioProtheus;
    private pv.jfcx.JPVEdit fldCargo;
    private pv.jfcx.JPVEdit fldEmail;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVEdit fldPrimNome;
    private pv.jfcx.JPVMask fldRamal;
    private pv.jfcx.JPVMask fldTelefone;
    private javax.swing.JLabel lblCargo;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPasswordEdited;
    private javax.swing.JLabel lblPrimNome;
    private javax.swing.JLabel lblRamal;
    private javax.swing.JLabel lblSenha;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscUsuarioFrame(int operation, String idAux, String contextAux) {
        initComponents();
        validate();
        lblPasswordEdited.setVisible(false);
        event.defaultConstructor(operation, idAux, contextAux);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}

