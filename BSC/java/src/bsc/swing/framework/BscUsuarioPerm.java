package bsc.swing.framework;

import bsc.swing.BscDefaultFrameBehavior;
import bsc.swing.BscRegraTreeNode;
import bsc.xml.BIXMLException;
import bsc.xml.BIXMLRecord;
import bsc.xml.BIXMLVector;
import java.awt.event.ItemEvent;
import java.util.Iterator;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author  lucio.pelinson
 */
public class BscUsuarioPerm extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, javax.swing.event.TreeSelectionListener, java.awt.event.ActionListener {

    /*****************************************************************************/
    // FUNÇÕES QUE PRECISAM SER ALTERADAS PARA CADA FORMULÁRIO:
    /*****************************************************************************/
    private String type = new String("USERPERM");
    private BIXMLVector vecRegra;
    private String sOwner;
    private String sOwnerId;
    bsc.core.BscImageResources bscImageResources;
    bsc.core.BscDataController bscDataController;
    private bsc.util.BscToolKit oBscToolKit = new bsc.util.BscToolKit();
    private java.util.Hashtable hstOrganizacao = new java.util.Hashtable();
    public bsc.swing.BscTree bscTree;

    public void enableFields() {
        bscTreeUsuario.setEnabled(false);
        pnlPermissoes.setEnabled(false);

        for (int i = 0; i < pnlPermissoes.getComponentCount(); i++) {
            pnlPermissoes.getComponent(i).setEnabled(true);
        }
        pnlPermissoes.repaint();
    }

    public void disableFields() {
        bscTreeUsuario.setEnabled(true);
        pnlPermissoes.setEnabled(true);

        for (int i = 0; i < pnlPermissoes.getComponentCount(); i++) {
            pnlPermissoes.getComponent(i).setEnabled(false);
        }
        pnlPermissoes.repaint();
    }

    public void refreshFields() {
        doSelection();
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        recordAux.set("OWNERID", sOwnerId);
        recordAux.set("OWNER", sOwner);

        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        if (record.contains("AUTENTIC")) {
            recordAux.set("AUTENTIC", record.getString("AUTENTIC"));
        } else {
            recordAux.set("AUTENTIC", "0");
        }

        recordAux.set(vecRegra);
        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean lRet = true;

        return lRet;
    }

    public void populateCombos(bsc.xml.BIXMLRecord serverData) {
        return;
    }

    public boolean hasCombos() {
        return false;
    }

    public void configureUsuarioPerm(String owner, String ownerId) {
        sOwner = owner;
        sOwnerId = ownerId;

        loadComboOrganizacao();
    }

    private void loadComboOrganizacao() {
        BIXMLRecord record = bscDataController.loadRecord("0", "LISTA_ORGANIZACAO");
        oBscToolKit.populateCombo(record.getBIXMLVector("ORGANIZACOES"), "ID", hstOrganizacao, cbbFiltroOrganizacao, null, null);
        cbbFiltroOrganizacao.removeItemAt(0);
    }

    private void loadTree(String organizacao) {
        StringBuilder request = new StringBuilder("ARVORE");
        request.append("|");
        request.append(sOwner);
        request.append("|");
        request.append(sOwnerId);
        request.append("|");
        request.append(organizacao);

        BIXMLVector treeRecord = event.loadRecord("-1", type, request.toString()).getBIXMLVector("ARVORES");

        bscImageResources = new bsc.core.BscImageResources();

        bscTreeUsuario.removeAll();
        bscTreeUsuario.setCellRenderer(new bsc.swing.BscTreeCellRenderer(bscImageResources));

        bscTreeUsuario.setModel(new DefaultTreeModel(createTree(treeRecord.get(0))));
    }

    public DefaultMutableTreeNode createTree(BIXMLRecord treeXML) {
        String key = new String((String) treeXML.getKeyNames().next());
        DefaultMutableTreeNode root = insertGroup(treeXML.getBIXMLVector(key), null);
        return root;
    }

    public DefaultMutableTreeNode insertGroup(BIXMLVector vector, DefaultMutableTreeNode tree) throws BIXMLException {
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(
                new bsc.swing.BscTreeNode(
                vector.getAttributes().getString("TIPO"),
                vector.getAttributes().getString("ID"),
                vector.getAttributes().getString("NOME")));
        for (int i = 0; i < vector.size(); i++) {
            child = insertRecord(vector.get(i), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    public DefaultMutableTreeNode insertRecord(BIXMLRecord record, DefaultMutableTreeNode tree) throws BIXMLException {
        bsc.swing.BscRegraTreeNode treeNode = new bsc.swing.BscRegraTreeNode(
                record.getTagName(),
                record.getAttributes().getString("ID"),
                record.getAttributes().getString("NOME"),
                record);

        DefaultMutableTreeNode child = new DefaultMutableTreeNode(treeNode);

        for (Iterator e = record.getKeyNames(); e.hasNext();) {
            child = insertGroup(record.getBIXMLVector((String) e.next()), child);
        }

        if (tree != null) {
            tree.add(child);
            return tree;
        } else {
            return child;
        }
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        cbbFiltroOrganizacao = new javax.swing.JComboBox();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        pnlSeguranca = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        scrollTree = new javax.swing.JScrollPane();
        bscTreeUsuario = new javax.swing.JTree();
        pnlPermissoes = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscBaseFrame_00007")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_seguranca.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 660, 395));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 25));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscUsuarioFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_x_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscUsuarioFrame_00003")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(310, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(225, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscUsuarioFrame_00004")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscUsuarioFrame_00006")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        cbbFiltroOrganizacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbFiltroOrganizacao.setPreferredSize(new java.awt.Dimension(200, 22));
        cbbFiltroOrganizacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroOrganizacaoItemStateChanged(evt);
            }
        });
        tbInsertion.add(cbbFiltroOrganizacao);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        getContentPane().add(pnlTools, java.awt.BorderLayout.PAGE_START);

        tapCadastro.setMinimumSize(new java.awt.Dimension(400, 345));
        tapCadastro.setPreferredSize(new java.awt.Dimension(400, 345));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 2));
        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlSeguranca.setLayout(new java.awt.GridLayout(1, 0));

        jSplitPane1.setDividerLocation(400);
        jSplitPane1.setPreferredSize(new java.awt.Dimension(160, 86));

        scrollTree.setAutoscrolls(true);
        scrollTree.setMaximumSize(null);
        scrollTree.setMinimumSize(null);
        scrollTree.setRequestFocusEnabled(false);
        scrollTree.setVerifyInputWhenFocusTarget(false);

        bscTreeUsuario.setAutoscrolls(true);
        bscTreeUsuario.setMaximumSize(null);
        bscTreeUsuario.setMinimumSize(null);
        bscTreeUsuario.setPreferredSize(null);
        scrollTree.setViewportView(bscTreeUsuario);

        jSplitPane1.setLeftComponent(scrollTree);

        pnlPermissoes.setBackground(new java.awt.Color(255, 255, 255));
        pnlPermissoes.setLayout(new javax.swing.BoxLayout(pnlPermissoes, javax.swing.BoxLayout.Y_AXIS));
        jSplitPane1.setRightComponent(pnlPermissoes);

        pnlSeguranca.add(jSplitPane1);

        pnlFields.add(pnlSeguranca, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscBaseFrame_00007"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        tapCadastro.getAccessibleContext().setAccessibleName(bundle.getString("BscBaseFrame_00007")); // NOI18N

        pnlTopForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlTopForm.setOpaque(false);
        pnlTopForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlBottomForm.setOpaque(false);
        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlRightForm.setOpaque(false);
        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setMinimumSize(new java.awt.Dimension(5, 5));
        pnlLeftForm.setOpaque(false);
        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();

	}//GEN-LAST:event_btnEditActionPerformed

        private void cbbFiltroOrganizacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroOrganizacaoItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                if (cbbFiltroOrganizacao.getSelectedIndex() > -1) {
                    loadTree(oBscToolKit.getComboValue(hstOrganizacao, cbbFiltroOrganizacao));
                }
            }
}//GEN-LAST:event_cbbFiltroOrganizacaoItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree bscTreeUsuario;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbFiltroOrganizacao;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlPermissoes;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlSeguranca;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scrollTree;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscUsuarioPerm(int operation, String idAux, String contextAux) {
        initComponents();

        bscImageResources = new bsc.core.BscImageResources();
        bsc.core.BscStaticReferences.setBscImageResources(bscImageResources);
        bscDataController = bsc.core.BscStaticReferences.getBscDataController();
        bscTreeUsuario.removeAll();
        bscTreeUsuario.setCellRenderer(new bsc.swing.BscTreeCellRenderer(bscImageResources));

        validate();

        event.defaultConstructor(operation, idAux, contextAux);
        bscTreeUsuario.addTreeSelectionListener(this);
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    public void actionPerformed(java.awt.event.ActionEvent e) {
        javax.swing.JCheckBox source = (javax.swing.JCheckBox) e.getSource();
        int nPos = ((Integer) source.getClientProperty("ordem")).intValue();
        BIXMLRecord oRec = vecRegra.get(nPos);
        oRec.set("VALOR", source.isSelected());
    }

    // Zeros a esquerda, Somente para inteiros ate 999, muito rapida
    String strZero(int value) {
        String ret = "";
        if (value < 10) {
            ret = "00" + value;
        } else if (value < 100) {
            ret = "0" + value;
        }
        return ret;
    }

    public void valueChanged(TreeSelectionEvent e) {
        doSelection();
    }

    public void doSelection() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTreeUsuario.getLastSelectedPathComponent();
        javax.swing.JCheckBox chkOption;

        if (node != null) {
            // Remove chkboxes
            pnlPermissoes.removeAll();

            Object userObject = node.getUserObject();

            if (userObject instanceof bsc.swing.BscRegraTreeNode && !(userObject.toString().equals("Configurações"))) {
                btnEdit.setEnabled(true);
                BscRegraTreeNode oNode = (BscRegraTreeNode) userObject;
                bsc.xml.BIXMLAttributes attributes = oNode.getRecord().getAttributes();
                String sIdEntity = attributes.getString("ID");
                String sNomeEntity = oNode.getRecord().getTagName();

                StringBuilder request = new StringBuilder("REGRA");
                request.append("|");
                request.append(sOwner);
                request.append("|");
                request.append(sOwnerId);
                request.append("|");
                request.append(sIdEntity);
                request.append("|");
                request.append(sNomeEntity);

                vecRegra = event.loadRecord("-1", type, request.toString()).getBIXMLVector("REGRAS");

                for (int i = 0; i < vecRegra.size(); i++) {
                    BIXMLRecord oRec = vecRegra.get(i);
                    String nome = oRec.getString("NOME");
                    boolean valor = oRec.getBoolean("VALOR");

                    chkOption = new javax.swing.JCheckBox(nome, valor);
                    chkOption.setBackground(java.awt.Color.WHITE);
                    chkOption.addActionListener(this);
                    chkOption.putClientProperty("ordem", new Integer(i));
                    if (getStatus() == BscDefaultFrameBehavior.NORMAL) {
                        chkOption.setEnabled(false);
                    } else {
                        chkOption.setEnabled(true);
                    }

                    pnlPermissoes.add(chkOption);
                }

            } else {
                btnEdit.setEnabled(false);
            }

            pnlPermissoes.validate();
            pnlPermissoes.repaint();
        } else {
            btnEdit.setEnabled(false);
        }
    }
}

