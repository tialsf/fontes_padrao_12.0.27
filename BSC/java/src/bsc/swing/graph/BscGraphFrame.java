package bsc.swing.graph;

import bsc.swing.BscDefaultFrameBehavior;
import bsc.swing.BscFileChooser;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import com.infragistics.ultrachart.JFCChart;
import com.infragistics.ultrachart.shared.styles.*;
import com.infragistics.ultrachart.resources.appearance.*;
//import com.sun.image.codec.jpeg.*;
import bsc.core.*;
import javax.imageio.ImageIO;

public class BscGraphFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, ActionListener, ListSelectionListener {

    /*Constantes que identificam tipos de planilhas.*/
    final static int BSC_RESULTADOS = 1;
    final static int BSC_REFERENCIA = 2;
    final static int BSC_META = 3;
    /*Constantes que identificam tipos de gr�ficos.*/
    final static int COLUNA = 0;
    final static int BARRA = 1;
    final static int AREA = 2;
    final static int LINHA = 3;
    final static int TORTA = 4;
    final static int BOLHA = 5;
    final static int DISPERSAO = 6;
    final static int HEATMAP = 7;
    final static int BARRA3D = 8;
    final static int COLUNA3D = 9;
    final static int AREA3D = 10;
    final static int LINHA3D = 11;
    final static int HEATMAP3D = 12;
    final static int TORTA3D = 13;
    final static int STACKBAR = 14;
    final static int STACKCOLUMN = 15;
    final static int STACKCOLUMN3D = 16;
    final static int STACKBAR3D = 17;
    /*Vari�veis de trabalho.*/
    boolean doingItemEvent = false;
    JFCChart oGraph; // Objeto grafico
    JTable oTableGraph;//Tabela com os dados do gr�fico
    String strUnidadeY = new String(),
            strDataDe = new String(),
            strDataAte = new String();
    boolean blnCumulativo = true,
            blnSetInd = false;
    int nIntervaloID = 0, //Intervalo original para convers�o.
            nRefIntervaloID = 0;
    java.util.Hashtable hstEstilo = new java.util.Hashtable(),
            hstPosLegenda = new java.util.Hashtable(),
            hstIndicador = new java.util.Hashtable(),
            hstIndicadores = new java.util.Hashtable(),
            hstFrequencia = new java.util.Hashtable(),
            hstFreqOrdem = new java.util.Hashtable();
    bsc.xml.BIXMLVector vectXmlGraphs = new bsc.xml.BIXMLVector(),
            vectIndicadores = new bsc.xml.BIXMLVector(),
            vectFrequencia = new bsc.xml.BIXMLVector();
    Vector vctIndCab = new Vector(),//Legenda do eixo X.
            vctSeqCor = new Vector(),//Sequencia para cores quando o a vizualiza��o por meta est� ativada.
            vctGraphDados = new Vector(),//Contem os vetores de Indicadores, Referencia e Meta
            vctFiltroValor = new Vector();//Contem os valores que ser�o mostrados nas combox de filtro
    java.awt.event.ItemListener cbbEstiloEvento = new java.awt.event.ItemListener() {

        @Override
        public void itemStateChanged(java.awt.event.ItemEvent evt) {
            cbbEstiloRefItemStateChanged(evt);
        }
    };
    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("GRAPH");

    @Override
    public void enableFields() {
        //Objetos do Form Grafico.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblZoom.setEnabled(true);
        lblEstilo.setEnabled(true);

        if (status != BscDefaultFrameBehavior.INSERTING) {
            cbbEstiloRef.setEnabled(true);
            cbbZoom.setEnabled(true);
        }

        //Objetos do Form Indicador
        lblIndicador.setEnabled(true);
        cbbIndicador.setEnabled(true);
        jpvCorIndicador.setEnabled(true);
        lblIndicadorOriginal.setEnabled(true);
        lblIndicadorAtual.setEnabled(true);
        cbbAgrupamento.setEnabled(true);
        chkReferencia.setEnabled(true);
        if (chkReferencia.isSelected()) {
            jpvCorReferencia.setEnabled(true);
        }
        chkMeta.setEnabled(true);
        if (chkMeta.isSelected()) {
            jpvCorMeta.setEnabled(true);
        }
        lblPeriodo.setEnabled(true);
        lblPeriodoAte.setEnabled(true);
        cbbFiltroDe.setEnabled(true);
        cbbFiltroAte.setEnabled(true);
        chkLegenda.setEnabled(true);
        cbbPosLegenda.setEnabled(true);
        lblAnalise.setEnabled(true);
        btnAnalise.setEnabled(true);

        //Objetos do form avalia��o
        lblAvaliacao.setEnabled(true);
        txtAvaliacao.setEnabled(true);
        lblDataGraph.setEnabled(true);
        fldDataGraph.setEnabled(true);
        cbbEstiloRef.addItemListener(cbbEstiloEvento);
    }

    // Desabilita os campos do formul�rio.
    @Override
    public void disableFields() {
        //Objetos do Form Grafico.
        lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblEstilo.setEnabled(false);
        cbbEstiloRef.setEnabled(false);
        lblZoom.setEnabled(false);
        cbbZoom.setEnabled(false);
        //Obejtos do Form Indicador
        lblIndicador.setEnabled(false);
        cbbIndicador.setEnabled(false);
        jpvCorIndicador.setEnabled(false);
        lblIndicadorOriginal.setEnabled(false);
        lblIndicadorAtual.setEnabled(false);
        cbbAgrupamento.setEnabled(false);
        chkReferencia.setEnabled(false);
        jpvCorReferencia.setEnabled(false);
        lblPeriodo.setEnabled(false);
        lblPeriodoAte.setEnabled(false);
        chkMeta.setEnabled(false);
        jpvCorMeta.setEnabled(false);
        cbbFiltroDe.setEnabled(false);
        cbbFiltroAte.setEnabled(false);
        chkLegenda.setEnabled(false);
        cbbPosLegenda.setEnabled(false);
        lblAnalise.setEnabled(false);
        btnAnalise.setEnabled(false);

        //Objetos do form avalia��o
        lblAvaliacao.setEnabled(false);
        txtAvaliacao.setEnabled(false);
        lblDataGraph.setEnabled(false);
        fldDataGraph.setEnabled(false);
        cbbEstiloRef.removeItemListener(cbbEstiloEvento);
    }

    // Copia os dados da vari�vel "record" para os campos do formul�rio.
    @Override
    public void refreshFields() {
        Vector vctTmpIndDados;
        blnSetInd = false;

        fldNome.setText(record.getString("NOME"));
        strDataDe = record.getString("ESTDE");
        strDataAte = record.getString("ESTATE");

        event.populateCombo(record.getBIXMLVector("ESTILOS"), "ESTID", hstEstilo, cbbEstiloRef);
        cbbEstiloRef.removeItemAt(0);//Remove o item em branco.

        event.populateCombo(record.getBIXMLVector("POSLEGENDAS"), "LEGPOSID", hstPosLegenda, cbbPosLegenda);
        cbbPosLegenda.removeItemAt(0); //remove o 1� item que � branco.

        event.populateCombo(record.getBIXMLVector("INDICADORES"), "INDICADOID", hstIndicador, cbbIndicador);
        vectFrequencia = record.getBIXMLVector("FREQUENCIAS");

        event.populateCombo(vectFrequencia, "IDFREQATUA", hstFrequencia, cbbAgrupamento, getIgnoreIDList());

        chkReferencia.setSelected(record.getBoolean("REFERENCIA"));
        chkMeta.setSelected(record.getBoolean("META"));
        vectIndicadores = record.getBIXMLVector("INDICADORES");

        if (record.getBoolean("PARCELADA")) {
            fldAnalise.setText("Parcelada");
        } else {
            fldAnalise.setText("Acumulada");
        }

        for (int indice = 0; indice < vectFrequencia.size(); indice++) {
            bsc.xml.BIXMLRecord rec = vectFrequencia.get(indice);
            hstFreqOrdem.put(rec.getString("ID"), rec.getString("NOME"));
        }

        //Dados dos indicadores, ser�o usados ap�s a sele��o do indicador.
        for (int i = 0; i < vectIndicadores.size(); i++) {
            vctTmpIndDados = new Vector();
            bsc.xml.BIXMLRecord rec = vectIndicadores.get(i);
            vctTmpIndDados.add(rec.getString("FREQ"));
            vctTmpIndDados.add(rec.getString("RNOME"));
            vctTmpIndDados.add(rec.getString("RFREQ"));
            vctTmpIndDados.add(rec.getString("UNIDADE"));
            vctTmpIndDados.add(rec.getString("CUMULATIVO"));
            hstIndicadores.put(rec.getString("ID"), vctTmpIndDados);
        }

        if (status != BscDefaultFrameBehavior.INSERTING) {
            //Atualiza os objetos que depedem da sele��o do indicador.
            setIndicador();
            blnSetInd = true;

            if (Integer.parseInt(event.getComboValue(hstIndicador, cbbIndicador)) > 0) {
                cbbFiltroDe.setSelectedIndex(record.getInt("PERIODODE"));
                cbbFiltroAte.setSelectedIndex(record.getInt("PERIODOATE"));
                refreshXMLGraphs();
                loadIndicador();
                setGrafico();
            }

            txtAvaliacao.setText(record.getString("AVALIACAO"));
            fldDataGraph.setText(record.getString("GRAPHDATA"));
        }

        graphRefreshColors();
    }

    // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
    //como retorno da fun��o.
    @Override
    public bsc.xml.BIXMLRecord getFieldsContents() {
        Color oTmpColor = null;
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        //Objetos do Form Graficos
        recordAux.set("NOME", fldNome.getText());

        //Objetos do Form Indicador
        recordAux.set("INDICADOID", event.getComboValue(hstIndicador, cbbIndicador));
        recordAux.set("IDFREQATUA", event.getComboValue(hstFrequencia, cbbAgrupamento));
        recordAux.set("REFERENCIA", chkReferencia.isSelected());
        recordAux.set("META", chkMeta.isSelected());

        if (status != BscDefaultFrameBehavior.INSERTING) {
            recordAux.set("PERIODODE", cbbFiltroDe.getSelectedIndex());
            recordAux.set("PERIODOATE", cbbFiltroAte.getSelectedIndex());
        } else {
            recordAux.set("PERIODODE", 0);
            recordAux.set("PERIODOATE", 0);
        }

        //Objetos do Form Avalia��o
        recordAux.set("AVALIACAO", txtAvaliacao.getText());
        recordAux.set("GRAPHDATA", fldDataGraph.getText());

        //Objetos que compoem o XmlGraphs.
        bsc.xml.BIXMLVector vecGraphs = new bsc.xml.BIXMLVector("XMLGRAPHS", "XMLGRAPH");
        bsc.xml.BIXMLRecord recDados = vecGraphs.addNewRecord("XMLGRAPH");

        //Valores para grava�ao na alteracao.
        recDados.set("ESTILO", cbbEstiloRef.getSelectedIndex());

        recDados.set("CORPORLINHA", "F");
        recDados.set("LINHAXCOL", "F");

        recDados.set("MOSTRALEGENDA", chkLegenda.isSelected() ? "T" : "F");
        recDados.set("POSLEGENDA", cbbPosLegenda.getSelectedIndex());
        recDados.set("METASERIE", "F");

        //Necessario para quando o grafico for gravado somente com o indicador selecionado.
        recDados.set("INDCOLOR", jpvCorIndicador.getColor() == null ? -10037761 : jpvCorIndicador.getColor().getRGB());
        recDados.set("REFCOLOR", jpvCorReferencia.getColor() == null ? -9208321 : jpvCorReferencia.getColor().getRGB());
        recDados.set("METACOLOR", jpvCorMeta.getColor() == null ? -11936875 : jpvCorMeta.getColor().getRGB());
        recDados.set("ZOOM", cbbZoom.getSelectedIndex());

        recordAux.set(vecGraphs);

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public boolean hasCombos() {
        return false;
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlGraph = new javax.swing.JPanel();
        pnlTopRecord = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        lblZoom = new javax.swing.JLabel();
        String[] aZoons = { "25 %", "50 %", "75 %", "100 %", "125%","150 %","175 %","200 %" };
        cbbZoom = new javax.swing.JComboBox(aZoons);
        lblEstilo = new javax.swing.JLabel();
        cbbEstiloRef = new javax.swing.JComboBox();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlGrafico = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        scpIndicador = new javax.swing.JScrollPane();
        pnlIndicador = new javax.swing.JPanel();
        lblIndicador = new javax.swing.JLabel();
        cbbIndicador = new javax.swing.JComboBox();
        lblIndicadorOriginal = new javax.swing.JLabel();
        lblIndicadorAtual = new javax.swing.JLabel();
        cbbAgrupamento = new javax.swing.JComboBox();
        chkReferencia = new javax.swing.JCheckBox();
        lblPeriodo = new javax.swing.JLabel();
        chkMeta = new javax.swing.JCheckBox();
        lblPeriodoAte = new javax.swing.JLabel();
        fldIndicadorOriginal = new pv.jfcx.JPVEdit();
        cbbFiltroDe = new javax.swing.JComboBox();
        cbbFiltroAte = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jpvCorIndicador = new pv.jfcx.JPVColor();
        jpvCorReferencia = new pv.jfcx.JPVColor();
        jpvCorMeta = new pv.jfcx.JPVColor();
        lblAnalise = new javax.swing.JLabel();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        chkIndicador = new javax.swing.JCheckBox();
        chkLegenda = new javax.swing.JCheckBox();
        chkMetaPorSerie = new javax.swing.JCheckBox();
        cbbPosLegenda = new javax.swing.JComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        scpAvaliacao = new javax.swing.JScrollPane();
        pnlAvaliacao = new javax.swing.JPanel();
        lblAvaliacao = new javax.swing.JLabel();
        crptxtAvaliacao = new javax.swing.JScrollPane();
        txtAvaliacao = new bsc.beans.JBITextArea();
        lblDataGraph = new javax.swing.JLabel();
        fldDataGraph = new pv.jfcx.JPVDatePlus();
        pnlTopForm = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscGraphFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_grafico.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(122, 39));
        setName(""); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 105, 132));
        setPreferredSize(new java.awt.Dimension(543, 358));

        tapCadastro.setToolTipText(bundle.getString("BscGraphFrame_00001")); // NOI18N
        tapCadastro.setMinimumSize(new java.awt.Dimension(0, 0));
        tapCadastro.setPreferredSize(new java.awt.Dimension(496, 500));

        pnlGraph.setToolTipText(bundle.getString("BscGraphFrame_00001")); // NOI18N
        pnlGraph.setLayout(new java.awt.BorderLayout(0, 5));

        pnlTopRecord.setMinimumSize(new java.awt.Dimension(100, 10));
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(10, 45));
        pnlTopRecord.setLayout(null);

        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscGraphFrame_00004")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(60, 22));
        pnlTopRecord.add(lblNome);
        lblNome.setBounds(10, 0, 52, 21);
        pnlTopRecord.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        lblZoom.setText(bundle.getString("BscGraphFrame_00005")); // NOI18N
        lblZoom.setEnabled(false);
        lblZoom.setPreferredSize(new java.awt.Dimension(60, 22));
        lblZoom.setRequestFocusEnabled(false);
        pnlTopRecord.add(lblZoom);
        lblZoom.setBounds(610, 0, 30, 20);

        cbbZoom.setEnabled(false);
        cbbZoom.setMinimumSize(new java.awt.Dimension(14, 19));
        cbbZoom.setPreferredSize(new java.awt.Dimension(60, 21));
        cbbZoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbZoomActionPerformed(evt);
            }
        });
        pnlTopRecord.add(cbbZoom);
        cbbZoom.setBounds(610, 20, 180, 22);

        lblEstilo.setText(bundle.getString("BscGraphFrame_00006")); // NOI18N
        lblEstilo.setEnabled(false);
        lblEstilo.setPreferredSize(new java.awt.Dimension(50, 22));
        pnlTopRecord.add(lblEstilo);
        lblEstilo.setBounds(420, 0, 98, 22);

        cbbEstiloRef.setEnabled(false);
        cbbEstiloRef.setPreferredSize(new java.awt.Dimension(120, 22));
        cbbEstiloRef.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbEstiloRefItemStateChanged(evt);
            }
        });
        cbbEstiloRef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbEstiloRefActionPerformed(evt);
            }
        });
        pnlTopRecord.add(cbbEstiloRef);
        cbbEstiloRef.setBounds(420, 20, 180, 22);

        pnlGraph.add(pnlTopRecord, java.awt.BorderLayout.NORTH);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlGraph.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setPreferredSize(new java.awt.Dimension(600, 400));

        pnlGrafico.setLayout(new java.awt.BorderLayout(0, 60));
        scpRecord.setViewportView(pnlGrafico);

        pnlGraph.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(5, 5));
        pnlGraph.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        tapCadastro.addTab(bundle.getString("BscGraphFrame_00001"), pnlGraph); // NOI18N

        scpIndicador.setBorder(null);

        pnlIndicador.setToolTipText("");
        pnlIndicador.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlIndicador.setLayout(null);

        lblIndicador.setText(bundle.getString("BscGraphFrame_00002")); // NOI18N
        lblIndicador.setEnabled(false);
        pnlIndicador.add(lblIndicador);
        lblIndicador.setBounds(20, 10, 90, 20);

        cbbIndicador.setEnabled(false);
        cbbIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbIndicadorActionPerformed(evt);
            }
        });
        pnlIndicador.add(cbbIndicador);
        cbbIndicador.setBounds(20, 30, 400, 22);

        lblIndicadorOriginal.setText(bundle.getString("BscGraphFrame_00009")); // NOI18N
        lblIndicadorOriginal.setEnabled(false);
        pnlIndicador.add(lblIndicadorOriginal);
        lblIndicadorOriginal.setBounds(20, 90, 90, 20);

        lblIndicadorAtual.setText(bundle.getString("BscGraphFrame_00014")); // NOI18N
        lblIndicadorAtual.setEnabled(false);
        pnlIndicador.add(lblIndicadorAtual);
        lblIndicadorAtual.setBounds(20, 130, 90, 20);

        cbbAgrupamento.setEnabled(false);
        cbbAgrupamento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbAgrupamentoItemStateChanged(evt);
            }
        });
        pnlIndicador.add(cbbAgrupamento);
        cbbAgrupamento.setBounds(20, 150, 400, 22);

        chkReferencia.setText(bundle.getString("BscGraphFrame_00010")); // NOI18N
        chkReferencia.setEnabled(false);
        chkReferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkReferenciaActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkReferencia);
        chkReferencia.setBounds(20, 320, 90, 23);

        lblPeriodo.setText(bundle.getString("BscGraphFrame_00012")); // NOI18N
        lblPeriodo.setEnabled(false);
        pnlIndicador.add(lblPeriodo);
        lblPeriodo.setBounds(20, 50, 90, 20);

        chkMeta.setText(bundle.getString("BscGraphFrame_00011")); // NOI18N
        chkMeta.setEnabled(false);
        chkMeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMetaActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkMeta);
        chkMeta.setBounds(20, 270, 90, 23);

        lblPeriodoAte.setText(bundle.getString("BscGraphFrame_00013")); // NOI18N
        lblPeriodoAte.setEnabled(false);
        pnlIndicador.add(lblPeriodoAte);
        lblPeriodoAte.setBounds(240, 50, 30, 20);

        fldIndicadorOriginal.setBackground(javax.swing.UIManager.getDefaults().getColor("ComboBox.disabledBackground"));
        fldIndicadorOriginal.setEnabled(false);
        pnlIndicador.add(fldIndicadorOriginal);
        fldIndicadorOriginal.setBounds(20, 110, 180, 22);

        cbbFiltroDe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroDeItemStateChanged(evt);
            }
        });
        pnlIndicador.add(cbbFiltroDe);
        cbbFiltroDe.setBounds(20, 70, 180, 22);

        cbbFiltroAte.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroAteItemStateChanged(evt);
            }
        });
        pnlIndicador.add(cbbFiltroAte);
        cbbFiltroAte.setBounds(240, 70, 180, 22);
        pnlIndicador.add(jSeparator1);
        jSeparator1.setBounds(20, 360, 400, 10);

        jpvCorIndicador.setButtonsImage(1);
        jpvCorIndicador.setCellSize(18);
        jpvCorIndicador.setImageSize(14);
        jpvCorIndicador.setRowCount(8);
        jpvCorIndicador.setText(bundle.getString("BscGraphFrame_00002")); // NOI18N
        jpvCorIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpvCorIndicadorActionPerformed(evt);
            }
        });
        pnlIndicador.add(jpvCorIndicador);
        jpvCorIndicador.setBounds(140, 240, 100, 20);

        jpvCorReferencia.setButtonsImage(1);
        jpvCorReferencia.setCellSize(18);
        jpvCorReferencia.setImageSize(14);
        jpvCorReferencia.setText(bundle.getString("BscGraphFrame_00010")); // NOI18N
        jpvCorReferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpvCorReferenciaActionPerformed(evt);
            }
        });
        pnlIndicador.add(jpvCorReferencia);
        jpvCorReferencia.setBounds(140, 320, 100, 20);

        jpvCorMeta.setButtonsImage(1);
        jpvCorMeta.setCellSize(18);
        jpvCorMeta.setImageSize(14);
        jpvCorMeta.setText(bundle.getString("BscGraphFrame_00011")); // NOI18N
        jpvCorMeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpvCorMetaActionPerformed(evt);
            }
        });
        pnlIndicador.add(jpvCorMeta);
        jpvCorMeta.setBounds(140, 270, 100, 20);

        lblAnalise.setText(bundle.getString("BscGraphFrame_00030")); // NOI18N
        lblAnalise.setEnabled(false);
        lblAnalise.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlIndicador.add(lblAnalise);
        lblAnalise.setBounds(20, 170, 90, 20);

        fldAnalise.setBackground(javax.swing.UIManager.getDefaults().getColor("ComboBox.disabledBackground"));
        fldAnalise.setEnabled(false);
        fldAnalise.setMaxLength(60);
        pnlIndicador.add(fldAnalise);
        fldAnalise.setBounds(20, 190, 400, 22);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setBorder(null);
        btnAnalise.setBorderPainted(false);
        btnAnalise.setEnabled(false);
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        pnlIndicador.add(btnAnalise);
        btnAnalise.setBounds(430, 190, 25, 22);

        chkIndicador.setSelected(true);
        chkIndicador.setText(bundle.getString("BscGraphFrame_00002")); // NOI18N
        chkIndicador.setEnabled(false);
        chkIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkIndicadorActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkIndicador);
        chkIndicador.setBounds(20, 240, 90, 23);

        chkLegenda.setText(bundle.getString("BscGraphFrameAvancado_00003")); // NOI18N
        chkLegenda.setEnabled(false);
        chkLegenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkLegendaActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkLegenda);
        chkLegenda.setBounds(20, 380, 120, 23);

        chkMetaPorSerie.setText(bundle.getString("BscGraphFrameAvancado_00006")); // NOI18N
        chkMetaPorSerie.setEnabled(false);
        chkMetaPorSerie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMetaPorSerieActionPerformed(evt);
            }
        });
        pnlIndicador.add(chkMetaPorSerie);
        chkMetaPorSerie.setBounds(20, 290, 170, 23);

        cbbPosLegenda.setEnabled(false);
        pnlIndicador.add(cbbPosLegenda);
        cbbPosLegenda.setBounds(140, 380, 100, 20);
        pnlIndicador.add(jSeparator2);
        jSeparator2.setBounds(20, 220, 400, 10);

        scpIndicador.setViewportView(pnlIndicador);

        tapCadastro.addTab(bundle.getString("BscGraphFrame_00002"), null, scpIndicador, "null"); // NOI18N

        scpAvaliacao.setBorder(null);

        pnlAvaliacao.setLayout(null);

        lblAvaliacao.setText(bundle.getString("BscGraphFrame_00031")); // NOI18N
        lblAvaliacao.setEnabled(false);
        pnlAvaliacao.add(lblAvaliacao);
        lblAvaliacao.setBounds(20, 50, 65, 20);

        crptxtAvaliacao.setViewportView(txtAvaliacao);

        pnlAvaliacao.add(crptxtAvaliacao);
        crptxtAvaliacao.setBounds(20, 70, 400, 240);

        lblDataGraph.setText(bundle.getString("BscGraphFrame_00015")); // NOI18N
        lblDataGraph.setEnabled(false);
        pnlAvaliacao.add(lblDataGraph);
        lblDataGraph.setBounds(20, 10, 65, 20);

        fldDataGraph.setDialog(true);
        fldDataGraph.setEnabled(false);
        fldDataGraph.setUseLocale(true);
        fldDataGraph.setWaitForCalendarDate(true);
        pnlAvaliacao.add(fldDataGraph);
        fldDataGraph.setBounds(20, 30, 100, 22);

        scpAvaliacao.setViewportView(pnlAvaliacao);

        tapCadastro.addTab(bundle.getString("BscGraphFrame_00003"), scpAvaliacao); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);

        pnlTopForm.setPreferredSize(new java.awt.Dimension(10, 22));
        pnlTopForm.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(485, 95));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 23));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscBaseFrame_00001")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscBaseFrame_00002")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(400, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(300, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(400, 27));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(310, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscBaseFrame_00003")); // NOI18N
        btnEdit.setToolTipText("");
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_excluir.gif"))); // NOI18N
        btnDelete.setText(bundle.getString("BscBaseFrame_00004")); // NOI18N
        btnDelete.setToolTipText("");
        btnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDelete.setMaximumSize(new java.awt.Dimension(76, 20));
        btnDelete.setMinimumSize(new java.awt.Dimension(76, 20));
        btnDelete.setPreferredSize(new java.awt.Dimension(76, 20));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        tbInsertion.add(btnDelete);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscBaseFrame_00005")); // NOI18N
        btnReload.setToolTipText("");
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("BscGraphFrame_00029")); // NOI18N
        btnPrint.setToolTipText("");
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("BscGraphFrame_00028")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        pnlTopForm.add(pnlTools, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        pnlBottomForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);

        pnlRightForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);

        pnlLeftForm.setPreferredSize(new java.awt.Dimension(5, 5));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);
    }// </editor-fold>//GEN-END:initComponents

	private void cbbEstiloRefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbEstiloRefActionPerformed
        graphRefreshColors();
	}//GEN-LAST:event_cbbEstiloRefActionPerformed

	private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
        bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
                new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
        bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
        bscAnaliseParceladaCard.show();
	}//GEN-LAST:event_btnAnaliseActionPerformed

    private void jpvCorMetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpvCorMetaActionPerformed
        setGrafico();
    }//GEN-LAST:event_jpvCorMetaActionPerformed

    private void jpvCorReferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpvCorReferenciaActionPerformed
        setGrafico();
    }//GEN-LAST:event_jpvCorReferenciaActionPerformed

    private void jpvCorIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpvCorIndicadorActionPerformed
        setGrafico();
    }//GEN-LAST:event_jpvCorIndicadorActionPerformed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(), true);
        frmChooser.setDialogType(BscFileChooser.BSC_DIALOG_SAVE);
        frmChooser.setFileType("*.jpg");
        frmChooser.loadServerFiles("graficos\\*.jpg");
        frmChooser.setFileName("gr" + record.getString("ID") + ".jpg");
        frmChooser.show();
        if (frmChooser.isValidFile()) {
            saveComponentAsJPEG(oGraph, "graficos\\" + frmChooser.getFileName());
        }
    }//GEN-LAST:event_btnExportarActionPerformed

    private void cbbFiltroDeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroDeItemStateChanged
        if (cbbFiltroAte.getSelectedIndex() > 0 && cbbFiltroDe.getSelectedIndex() > cbbFiltroAte.getSelectedIndex()) {
            cbbFiltroDe.setSelectedIndex(cbbFiltroAte.getSelectedIndex());
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                bsc.core.BscStaticReferences.getBscDialogSystem().informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00024"));
            }
        }
    }//GEN-LAST:event_cbbFiltroDeItemStateChanged

    private void cbbFiltroAteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroAteItemStateChanged
        if (cbbFiltroAte.getSelectedIndex() < cbbFiltroDe.getSelectedIndex()) {
            cbbFiltroAte.setSelectedIndex(cbbFiltroDe.getSelectedIndex());
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                bsc.core.BscStaticReferences.getBscDialogSystem().informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00024"));
            }
        }
    }//GEN-LAST:event_cbbFiltroAteItemStateChanged

    private void cbbAgrupamentoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbAgrupamentoItemStateChanged
        //Valida��o para agrupamento semanal.
        if (nIntervaloID == BscStaticReferences.BSC_FREQ_SEMANAL && cbbAgrupamento.getSelectedIndex() != 8 && cbbAgrupamento.getSelectedIndex() != 1) {
            cbbAgrupamento.setSelectedIndex(8);
            if (evt.getStateChange() == ItemEvent.DESELECTED) {
                bsc.core.BscStaticReferences.getBscDialogSystem().informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00023"));
            }
        }

        //Valida��o para agrupamento quadrimestral
        if (nIntervaloID == BscStaticReferences.BSC_FREQ_QUADRIMESTRAL && cbbAgrupamento.getSelectedIndex() == 2) {
            cbbAgrupamento.setSelectedIndex(3);
            bsc.core.BscStaticReferences.getBscDialogSystem().informationMessage(
                    java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00026"));
        }
    }//GEN-LAST:event_cbbAgrupamentoItemStateChanged

    private void cbbZoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbZoomActionPerformed
        setZoom();
    }//GEN-LAST:event_cbbZoomActionPerformed

    private void cbbIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbIndicadorActionPerformed
        if (getStatus() == BscDefaultFrameBehavior.INSERTING || getStatus() == BscDefaultFrameBehavior.UPDATING) {
            setIndicador();
        }
    }//GEN-LAST:event_cbbIndicadorActionPerformed

    private void chkReferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkReferenciaActionPerformed
        if (chkReferencia.isSelected()) {
            jpvCorReferencia.setEnabled(true);
        } else {
            jpvCorReferencia.setEnabled(false);
        }
    }//GEN-LAST:event_chkReferenciaActionPerformed

    private void chkMetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMetaActionPerformed
        if (chkMeta.isSelected()) {
            jpvCorMeta.setEnabled(true);
        } else {
            jpvCorMeta.setEnabled(false);
        }
    }//GEN-LAST:event_chkMetaActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // frame tempor�rio
        Frame frmImp = new Frame("BSC-Impress�o");
        frmImp.pack();

        // obten��o do Toolkit
        Toolkit tk = frmImp.getToolkit();

        // inicio de sess�o de impress�o
        java.awt.PageAttributes pagina = new java.awt.PageAttributes();
        pagina.setOrientationRequested(4);
        pagina.setOrigin(java.awt.PageAttributes.OriginType.PRINTABLE);
        pagina.setMedia(java.awt.PageAttributes.MediaType.NA_LETTER);

        PrintJob pj = tk.getPrintJob(frmImp, "BSC-Grafico", null, pagina);

        // impress�o
        if (pj != null) { // testa de PrintJob v�lido
            String[] aLinhas = new String[7];
            String strData = " //";
            java.text.DateFormat dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.LONG);

            if (fldDataGraph.getDate() != null) {
                strData = (String) dateFormatter.format(fldDataGraph.getDate());
            }

            aLinhas = preperTexttoPrint(txtAvaliacao.getText());

            Graphics g = pj.getGraphics(); // obt�m contexto gr�fico
            this.oGraph.print(g);

            Font Arial = new Font("Arial", Font.PLAIN, 10);
            Font ArialBlack = new Font("Arial", Font.BOLD, 10);

            g.setFont(ArialBlack);
            g.drawString(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00027"), 03, 470);
            g.setFont(Arial);
            g.drawString(strData, 95, 470);

            for (int iLinha = 0; iLinha < 7; iLinha++) {
                g.drawString(aLinhas[iLinha], 03, 490 + (iLinha * 15));
            }

            // libera��o dos recursos
            g.dispose(); // libera recursos do contexto gr�fico
            pj.end(); // encerra impress�o
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
        event.loadRecord();
    }//GEN-LAST:event_btnReloadActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        event.saveRecord();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        event.cancelOperation();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        event.deleteRecord();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        event.editRecord();
    }//GEN-LAST:event_btnEditActionPerformed

    private void chkIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkIndicadorActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_chkIndicadorActionPerformed

    private void chkLegendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkLegendaActionPerformed
        if (chkLegenda.isSelected()) {
            cbbPosLegenda.setEnabled(true);
        } else {
            cbbPosLegenda.setEnabled(false);
        }
}//GEN-LAST:event_chkLegendaActionPerformed

    private void chkMetaPorSerieActionPerformed(java.awt.event.ActionEvent evt) {
        
    }
    
    private void cbbEstiloRefItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbEstiloRefItemStateChanged
        oGraph.setChartType(getGraphType(cbbEstiloRef.getSelectedIndex()));
    }//GEN-LAST:event_cbbEstiloRefItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cbbAgrupamento;
    private javax.swing.JComboBox cbbEstiloRef;
    private javax.swing.JComboBox cbbFiltroAte;
    private javax.swing.JComboBox cbbFiltroDe;
    private javax.swing.JComboBox cbbIndicador;
    public javax.swing.JComboBox cbbPosLegenda;
    private javax.swing.JComboBox cbbZoom;
    public javax.swing.JCheckBox chkIndicador;
    public javax.swing.JCheckBox chkLegenda;
    public javax.swing.JCheckBox chkMeta;
    public javax.swing.JCheckBox chkMetaPorSerie;
    private javax.swing.JCheckBox chkReferencia;
    private javax.swing.JScrollPane crptxtAvaliacao;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDatePlus fldDataGraph;
    private pv.jfcx.JPVEdit fldIndicadorOriginal;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private pv.jfcx.JPVColor jpvCorIndicador;
    private pv.jfcx.JPVColor jpvCorMeta;
    private pv.jfcx.JPVColor jpvCorReferencia;
    private javax.swing.JLabel lblAnalise;
    private javax.swing.JLabel lblAvaliacao;
    private javax.swing.JLabel lblDataGraph;
    private javax.swing.JLabel lblEstilo;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblIndicadorAtual;
    private javax.swing.JLabel lblIndicadorOriginal;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPeriodo;
    private javax.swing.JLabel lblPeriodoAte;
    private javax.swing.JLabel lblZoom;
    private javax.swing.JPanel pnlAvaliacao;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlGrafico;
    private javax.swing.JPanel pnlGraph;
    private javax.swing.JPanel pnlIndicador;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JScrollPane scpAvaliacao;
    private javax.swing.JScrollPane scpIndicador;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtAvaliacao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscGraphFrame(int operation, String idAux, String contextId) {
        initComponents();
        pack();
        initGrafico();
        fldDataGraph.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
        event.defaultConstructor(operation, idAux, contextId);
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        type = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(type);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    @Override
    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        event.loadRecord();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
    }

    /**
     *Trata os eventos do formul�rio.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    }

    /**
     *Atualizando o gr�fico
     */
    public void btnUpdateGraphActionPerformed() {
        if (blnSetInd && cbbIndicador.getSelectedIndex() != 0) {
            loadIndicador();
            setGrafico();
        }
    }

    /**
     *Cria o objeto gr�fico
     */
    private void initGrafico() {
        Object[] headers = new Object[]{""};
        Object[][] cells = new Object[][]{{new Double(0)}};

        //Defini��o das constantes do gr�fico.
        com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_BOTTOM_TEXT = "Balanced Scorecard";
        com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_TOP_HEIGHT = 90;

        //Criacao do objeto grafico.
        oGraph = new com.infragistics.ultrachart.JFCChart();

        //Cria��o da tabela que conter� os valores do gr�fico.
        TableModel model = new DefaultTableModel(cells, headers);
        oTableGraph = new JTable(model);

        //Atribui��o dos valores a tabela.
        oGraph.getData().setDataSource(oTableGraph);
        pnlGrafico.add(oGraph);
    }

    /**
     *Atualiza a tabela de dados do gr�fico.
     */
    private void upDateTable() {
        bsc.xml.BIXMLVector vctIndicadores = (bsc.xml.BIXMLVector) vctGraphDados.get(0),
                vctReferencia = (bsc.xml.BIXMLVector) vctGraphDados.get(1);

        DefaultTableModel model = (DefaultTableModel) oTableGraph.getModel();

        Vector vctDados = new Vector();
        vctIndCab.clear();

        if (chkMeta.isSelected() ) {
            vctDados.addElement(addDados(vctIndicadores, vctReferencia));
        } else {
            //Adiciona os itens do Indicador.
            vctDados.addElement(addDados(BSC_RESULTADOS, vctIndicadores));

            //Adiciona os dados da Referencia.
            if (chkReferencia.isSelected()) {
                vctDados.addElement(addDados(BSC_REFERENCIA, vctReferencia));
            }

            //Adiciona os dados da meta.
            if (chkMeta.isSelected()) {
                vctDados.addElement(addDados(BSC_META, vctIndicadores));
            }
        }

        model.setDataVector(vctDados, vctIndCab);
        model.fireTableChanged(new TableModelEvent(model));
    }

    /**
     *Carrega os dados os dados indicador, faz o pedido dos dados para o Protheus
     *Neste momento � atualizado os seguintes componentes em tela:
     *Frequencia, Referencia, Periodos e agrupamento.
     */
    private void loadIndicador() {
        bsc.xml.BIXMLVector vctReferencia = null,
                vctIndicador = null;

        bsc.xml.BIXMLRecord //Dados para mostrar no gr�fico.
                recRefDados = new bsc.xml.BIXMLRecord("REF_DADOS"),
                recIndDados = new bsc.xml.BIXMLRecord("IND_DADOS");

        vctGraphDados.removeAllElements();

        recIndDados = event.loadRecord(event.getComboValue(hstIndicador, cbbIndicador), "PLANILHA", setLoadCmd());
        vctIndicador = recIndDados.getBIXMLVector("VALORES");

        if (chkReferencia.isSelected()) {
            recRefDados = event.loadRecord(event.getComboValue(hstIndicador, cbbIndicador), "RPLANILHA", setLoadCmd());
            if (recRefDados.size() > 0) {
                vctReferencia = recRefDados.getBIXMLVector("VALORES");
            }
        }

        vctGraphDados.add(vctIndicador);
        vctGraphDados.add(vctReferencia);

        upDateTable();
    }

    /**
     *Atualiza os objetos que est�o gravados em XMLGRAPHS
     */
    private void refreshXMLGraphs() {
        if (record.contains("XMLGRAPHS")) {
            vectXmlGraphs = record.getBIXMLVector("XMLGRAPHS");
            bsc.xml.BIXMLRecord rec = vectXmlGraphs.get(0);
            cbbEstiloRef.setSelectedIndex(rec.getInt("ESTILO"));
            jpvCorIndicador.setColor(new java.awt.Color(rec.getInt("INDCOLOR")));
            jpvCorReferencia.setColor(new java.awt.Color(rec.getInt("REFCOLOR")));
            jpvCorMeta.setColor(new java.awt.Color(rec.getInt("METACOLOR")));
            cbbZoom.setSelectedIndex(rec.getInt("ZOOM"));
            chkLegenda.setSelected(rec.getString("MOSTRALEGENDA").equals("T"));
            cbbPosLegenda.setSelectedIndex(rec.getInt("POSLEGENDA"));
        }
    }

    /**
     * Configura  a apresenta��o para o EixoX
     * @param intDia Dia
     * @param intMes Mes
     * @param intAno Ano
     * @param iIntervalo Intervalor
     */
    private String setEixoX(int intDia, int intMes, int intAno, int iIntervalo, boolean blnMeta) {

        String strRetorno = new String("");
        java.text.DateFormatSymbols dateFormat = new java.text.DateFormatSymbols(bsc.core.BscStaticReferences.getBscDefaultLocale());
        java.text.DateFormat dateFormatter;
        Calendar calData = Calendar.getInstance();

        String[] meses = dateFormat.getMonths();

        if (blnMeta) {
            iIntervalo = 99;
        }

        switch (iIntervalo) {
            case BscStaticReferences.BSC_FREQ_DIARIA://Dia
                calData.set(intAno, intMes, intDia);
                dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
                strRetorno = (String) dateFormatter.format(new java.util.Date(calData.getTimeInMillis()));
                break;
            case BscStaticReferences.BSC_FREQ_SEMANAL://Semana
                strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00016") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_QUINZENAL://Quinzena
                strRetorno = intDia + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00017") + meses[intMes] + " " + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_MENSAL://Meses
                strRetorno = meses[intMes] + " " + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00022") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_BIMESTRAL://Bimestre
                strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00018") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_TRIMESTRAL://Trimestre
                strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00019") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_QUADRIMESTRAL://Quadrimestre
                strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00020") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_SEMESTRAL://Semestre
                strRetorno = intMes + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00021") + intAno;
                break;
            case BscStaticReferences.BSC_FREQ_ANUAL://Ano
                strRetorno = String.valueOf(intAno);
                break;
            case 99:
                calData.set(intAno, intMes, intDia);
                dateFormatter = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT);
                strRetorno = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00011") + ": " + (String) dateFormatter.format(new java.util.Date(calData.getTimeInMillis()));
                break;
            default:
                strRetorno = "Sem intervalo";
        }

        return strRetorno;
    }

    /**
     *Atualiza os objetos que depedem da sele��o do indicador.
     */
    private void setIndicador() {
        Vector vctTmpReg;
        String cIntervaloID, // Codigo do indicador de referencia
                cRefIntervaloID;

        vctTmpReg = (Vector) hstIndicadores.get((String) event.getComboValue(hstIndicador, cbbIndicador));

        if (vctTmpReg != null) {
            String strBoolean = new String();
            cIntervaloID = (String) vctTmpReg.get(0);	     //Intervalo do Indicador
            cRefIntervaloID = (String) vctTmpReg.get(2);	     //Intervalo de referencia.
            strUnidadeY = (String) vctTmpReg.get(3);	     // Unidade do eixo Y.
            strBoolean = (String) vctTmpReg.get(4);	     //Indica se o Indicador e cumulativo.
            blnCumulativo = !record.getBoolean("PARCELADA"); // strBoolean.equals("T");

            if (cIntervaloID != null && cIntervaloID.length() > 0) {
                nIntervaloID = Integer.parseInt(cIntervaloID);
            }

            if (cRefIntervaloID != null && cRefIntervaloID.length() > 0) {
                nRefIntervaloID = Integer.parseInt(cRefIntervaloID);
            }

            if (nIntervaloID > 0) {

                //Mensagem que mostra quel � o indicador original.
                String strIndOriginal = (String) hstFreqOrdem.get(new java.lang.String(cIntervaloID));
                fldIndicadorOriginal.setText(strIndOriginal);

                //Mostra s� os agrupamento disponiveis para est� planilha.
                event.populateCombo(vectFrequencia, "IDFREQATUA", hstFrequencia, cbbAgrupamento, getIgnoreIDList());
            }

            setCbbFiltros();
        }
    }

    /**
     *Com base no indicador selecionado, configura as caracteristicas do gr�fico.
     */
    public void setGrafico() {
        if (vctGraphDados.size() > 0) {

            //Atualiza as cores das s�ries do gr�fico.
            graphRefreshColors();

            //Formata��o para o titulo das s�rires
            oGraph.getAxis().setDefaultLinearAxisFormat("<DATA_VALUE:0.0> " + strUnidadeY); //Legenda do eixo Y

            //Formata��o para gr�fico por s�rie (Mostar metas usando s�rie).
            oGraph.getColorAppearance().setColorByRow(false);//Cores por Linha.

            //Legenda
            oGraph.getLegend().setVisible(false); //Desabilita para for�ar o refresh em baixo.
            oGraph.getLegend().setVisible(chkLegenda.isSelected()); //Mostra Legenda.
            oGraph.getLegend().setLocation(cbbPosLegenda.getSelectedIndex()); //Posi��o da legenda na tela.
            oGraph.getLegend().setSpanPercentage(15);// Tamanho da legenda.

            //Estilo
            oGraph.setChartType(getGraphType(cbbEstiloRef.getSelectedIndex()));

            //Titulo
            TitleAppearance appT = oGraph.getTitleTop();
            appT.setText(record.getString("NOME") + "\n" + (String) cbbIndicador.getSelectedItem());
            appT.setFont(new Font("SansSerif", Font.BOLD, 11));
            appT.setVisible(true);
            appT.setHorizontalAlign(0);
            appT.setTitleLocation(0);
        }
    }

    /**
     * Carrega os dados para as combos de filtros.
     */
    private void setCbbFiltros() {
        String strTexto = new String(""),
                strMes = new String("");

        Vector retData = new Vector();

        int nAno,
                nMes,
                nDia;

        //Data de
        nAno = java.lang.Integer.parseInt(strDataDe.substring(6, 10));
        nMes = java.lang.Integer.parseInt(strDataDe.substring(3, 5)) - 1;
        nDia = java.lang.Integer.parseInt(strDataDe.substring(0, 2));
        java.util.GregorianCalendar dIni = new java.util.GregorianCalendar(nAno, nMes, nDia);

        //Data ate
        nAno = java.lang.Integer.parseInt(strDataAte.substring(6, 10));
        nMes = java.lang.Integer.parseInt(strDataAte.substring(3, 5)) - 1;
        nDia = java.lang.Integer.parseInt(strDataAte.substring(0, 2));
        java.util.GregorianCalendar dFin = new java.util.GregorianCalendar(nAno, nMes, nDia);

        Integer intDia, intDiaAnt = new Integer(0),
                intMes, intMesAnt = new Integer(0),
                intAno, intAnoAnt = new Integer(0);

        cbbFiltroDe.removeAllItems();
        cbbFiltroAte.removeAllItems();
        vctFiltroValor.removeAllElements();

        for (; dFin.after(dIni); dIni.add(GregorianCalendar.DATE, 1)) {
            retData = DateConv(dIni, nIntervaloID);
            intDia = (Integer) retData.get(0);
            intMes = (Integer) retData.get(1);
            intAno = (Integer) retData.get(2);
            if (intDia.intValue() != intDiaAnt.intValue() || intMes.intValue() != intMesAnt.intValue() || intAno.intValue() != intAnoAnt.intValue()) {
                intDiaAnt = intDia;
                intMesAnt = intMes;
                intAnoAnt = intAno;

                strTexto = setEixoX(intDia.intValue(), intMes.intValue(), intAno.intValue(), nIntervaloID, false);

                /*Como o mes em Java come�a com 0, quando o indicador mensal fizer referencia ao m�s.
                 *� necessaria a corre��o do valor adicionando 1 v�riavel strMes.
                 */
                if (nIntervaloID == 8 || nIntervaloID == 6 || nIntervaloID == 5) {
                    strMes = String.valueOf(intMes.intValue() + 1);
                } else {
                    strMes = String.valueOf(intMes.intValue());
                }

                vctFiltroValor.add(new String[]{strTexto, intAno.toString() + "-" + strMes + "-" + intDia.toString()});
            }
        }
        for (int iItem = 0; iItem < vctFiltroValor.size(); iItem++) {
            String[] aValores = (java.lang.String[]) vctFiltroValor.get(iItem);
            cbbFiltroDe.addItem(new String(aValores[0]));
            cbbFiltroAte.addItem(new String(aValores[0]));
        }
    }

    /**
     * Configura o Zoom do gr�fico.
     */
    private void setZoom() {
        //Porcentagem do zoom
        int[] aZoom = {25, 50, 75, 100, 125, 150, 175, 200};

        //740X570 medidas para o papel A4.
        int iLargura = (740 * aZoom[cbbZoom.getSelectedIndex()]) / 100,
                iAltura = (450 * aZoom[cbbZoom.getSelectedIndex()]) / 100;

        scpRecord.setViewportView(null);

        Dimension dimMedidas = new Dimension(iLargura, iAltura);
        pnlGrafico.setPreferredSize(dimMedidas);

        scpRecord.setViewportView(pnlGrafico);
    }

    /**
     * Retorna um vector com os valores lidos de vctValores, para adi��o na tabela
     * de dados usada pelo gr�fico
     */
    private Vector addDados(bsc.xml.BIXMLVector vctIndicadores, bsc.xml.BIXMLVector vctReferencia) {
        Vector vctIndDados = new Vector(); //Dados do Indicador
        bsc.xml.BIXMLRecord tmpIndRec = null,
                tmpRefRec = null;

        vctSeqCor.removeAllElements();

        vctIndCab.addElement(" ");
        vctIndDados.add(" ");
        for (int indItem = 0; indItem < vctIndicadores.size(); indItem++) {
            tmpIndRec = vctIndicadores.get(indItem);

            if (tmpIndRec.contains("META")) {
                //Adicionando a Meta
                if (blnCumulativo) {
                    vctIndDados.addElement(new Float(tmpIndRec.getFloat("MONTANTE")));
                } else {
                    vctIndDados.addElement(new Float(tmpIndRec.getFloat("PARCELA")));
                }
                vctIndCab.addElement(getSerieLabel(tmpIndRec));
                vctSeqCor.addElement("META");
            } else {

                //Adicionando o Indicador
                if (blnCumulativo) {
                    vctIndDados.addElement(new Float(tmpIndRec.getFloat("MONTANTE")));
                } else {
                    vctIndDados.addElement(new Float(tmpIndRec.getFloat("PARCELA")));
                }
                //Ind
                vctIndCab.addElement(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00032").concat(getSerieLabel(tmpIndRec)));
                vctSeqCor.addElement("INDICADOR");

                //Adicionando a Referencia
                if (chkReferencia.isSelected()) {
                    tmpRefRec = vctReferencia.get(indItem);
                    if (blnCumulativo) {
                        vctIndDados.addElement(new Float(tmpRefRec.getFloat("MONTANTE")));
                    } else {
                        vctIndDados.addElement(new Float(tmpRefRec.getFloat("PARCELA")));
                    }

                    //Ref
                    vctIndCab.addElement(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00033").concat(getSerieLabel(tmpIndRec)));
                    vctSeqCor.addElement("REFERENCIA");
                }

            }
        }

        return vctIndDados;

    }

    /**
     * Retorna um vector com os valores lidos de vctValores, para adi��o na tabela
     * de dados usada pelo gr�fico
     */
    private Vector addDados(int intTipo, bsc.xml.BIXMLVector vctValores) {
        Vector vctIndDados = new Vector(); //Dados do Indicador

        if (intTipo == BSC_RESULTADOS) {
            vctIndDados.add(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00002"));
            vctIndCab.addElement("Labels");
        } else if (intTipo == BSC_REFERENCIA) {
            vctIndDados.add(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00010"));
        } else {
            vctIndDados.add(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00011"));
        }

        boolean lAddValor = false;
        bsc.xml.BIXMLRecord tmpRec = null;
        for (int intItem = 0; intItem < vctValores.size(); intItem++) {
            tmpRec = vctValores.get(intItem);

            if (BSC_META == intTipo) {
                if (chkMeta.isSelected() && tmpRec.contains("META")) {
                    lAddValor = true;
                } else {
                    lAddValor = false;
                }
            } else {
                if (chkMeta.isSelected() && tmpRec.contains("META")) {
                    lAddValor = false;
                } else {
                    lAddValor = true;
                }
            }

            //Checa se adiciona o valor do indicador ou zera a coluna.
            if (lAddValor) {
                //Verifica se o indicador � cumulativo
                if (blnCumulativo) {
                    vctIndDados.addElement(new Float(tmpRec.getFloat("MONTANTE")));
                } else {
                    vctIndDados.addElement(new Float(tmpRec.getFloat("PARCELA")));
                }
            } else {
                vctIndDados.addElement(new Float(0));
            }

            //Adiciona a legenda so quando for indicador
            if (intTipo == BSC_RESULTADOS && tmpRec != null) {
                vctIndCab.addElement(getSerieLabel(tmpRec));
            }
        }

        return vctIndDados;
    }

    /**
     *Devolve um array com { cAno, cMes, cDia } convertida para o padr�o de planilhas
     *de resultado do BSC
     */
    private Vector DateConv(java.util.GregorianCalendar dData, int nFrequencia) {
        Vector vctRetorno = new Vector(); //Vetor de Retorno

        int intAno = 0,
                intMes = 0,
                intDia = 0;

        if (nFrequencia == BscStaticReferences.BSC_FREQ_ANUAL) { // Anual
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_SEMESTRAL) { // Semestral
            intMes = dData.get(GregorianCalendar.MONTH) > GregorianCalendar.JUNE ? 2 : 1;
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_QUADRIMESTRAL) {//Quadrimestal
            if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.APRIL) {
                intMes = 1;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.AUGUST) {
                intMes = 2;
            } else {
                intMes = 3;
            }
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_TRIMESTRAL) { // Trimestral
            if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.MARCH) {
                intMes = 1;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.JUNE) {
                intMes = 2;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.SEPTEMBER) {
                intMes = 3;
            } else {
                intMes = 4;
            }
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_BIMESTRAL) { // Bimestral
            if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.FEBRUARY) {
                intMes = 1;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.APRIL) {
                intMes = 2;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.JUNE) {
                intMes = 3;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.AUGUST) {
                intMes = 4;
            } else if (dData.get(GregorianCalendar.MONTH) <= GregorianCalendar.OCTOBER) {
                intMes = 5;
            } else {
                intMes = 6;
            }
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_MENSAL) { // Mensal
            intMes = dData.get(GregorianCalendar.MONTH);
            intAno = dData.get(GregorianCalendar.YEAR);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_QUINZENAL) { // Quinzenal
            intDia = dData.get(GregorianCalendar.DATE) > 15 ? 2 : 1;
            intAno = dData.get(GregorianCalendar.YEAR);
            intMes = dData.get(GregorianCalendar.MONTH);
        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_SEMANAL) { // Semanal
            intAno = dData.get(GregorianCalendar.YEAR);
            intDia = 0;

            java.util.GregorianCalendar dFirstDay = new java.util.GregorianCalendar(intAno, 0, 1);

            while (dFirstDay.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.MONDAY) {
                dFirstDay.add(GregorianCalendar.DATE, 1);
            }

            long lDifDias = (dData.getTimeInMillis() - dFirstDay.getTimeInMillis());

            //Subtra��o de datas, transformando em dias.
            lDifDias = ((((lDifDias / 1000) / 60) / 60) / 24);

            if (lDifDias > 0) {
                intMes = (int) (lDifDias / 7) + 1;
            } else {
                intMes = 1;
            }

        } else if (nFrequencia == BscStaticReferences.BSC_FREQ_DIARIA) { // Diaria
            intAno = dData.get(GregorianCalendar.YEAR);
            intMes = dData.get(GregorianCalendar.MONTH);
            intDia = dData.get(GregorianCalendar.DATE);
        }

        vctRetorno.addElement(new Integer(intDia));
        vctRetorno.addElement(new Integer(intMes));
        vctRetorno.addElement(new Integer(intAno));

        return vctRetorno;
    }

    /**
     * Retorna a String de comando para a atualiza��o do gr�fico.
     */
    private String setLoadCmd() {
        String cLoadCmd = null;
        String[] aValores = null;

        aValores = (java.lang.String[]) vctFiltroValor.get(cbbFiltroDe.getSelectedIndex());
        cLoadCmd = aValores[1] + ";";
        aValores = (java.lang.String[]) vctFiltroValor.get(cbbFiltroAte.getSelectedIndex());
        cLoadCmd += aValores[1] + ";";
        cLoadCmd += String.valueOf(event.getComboValue(hstFrequencia, cbbAgrupamento));

        if (chkMeta.isSelected()) {
            cLoadCmd += ";.T.";
        } else {
            cLoadCmd += ";.F.";
        }

        return cLoadCmd;
    }

    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    private void saveComponentAsJPEG(Component myComponent, String fileName) {
        Dimension size = myComponent.getSize();
        BufferedImage myImage = new BufferedImage(size.width, size.height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = myImage.createGraphics();
        myComponent.paint(g2);
        //Grava a figura l� no servidor... :)
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            //JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            //encoder.encode(myImage);
            /*Salva o arquivo de imagem como JPEG*/
            ImageIO.write(myImage, "jpg", out);
            String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
            event.saveBase64(fileName, strBase64);
            out.close();
            //Abre o navegador para salvar a figura localmente
            bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
            java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString() + fileName);
            bscApplet.getAppletContext().showDocument(url, "_self");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private String[] preperTexttoPrint(String strTexto) {
        String strTmpTexto;
        int iFinal;
        int iReturnKey;

        StringBuffer bffTexto = new StringBuffer(strTexto);
        String[] aLinhas = new String[7];

        for (int iLinha = 0; iLinha <= 6; iLinha++) {
            iReturnKey = bffTexto.indexOf("\n");
            if (bffTexto.length() > 0) {
                if (iReturnKey > 0) {
                    if (iReturnKey > 90) {
                        iFinal = 90;
                    } else {
                        iFinal = iReturnKey;
                        bffTexto.delete(iReturnKey, ++iReturnKey);
                    }

                } else if (bffTexto.length() > 90) {
                    iFinal = 90;
                } else {
                    iFinal = bffTexto.length();
                }
                aLinhas[iLinha] = bffTexto.substring(0, iFinal);
                bffTexto.delete(0, iFinal);
            } else {
                aLinhas[iLinha] = "";
            }
        }

        return aLinhas;
    }

    private java.util.Hashtable getIgnoreIDList() {
        java.util.Hashtable htbIgnoreIDList = new java.util.Hashtable();

        if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_ANUAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUADRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_BIMESTRAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_TRIMESTRAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_MENSAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_MENSAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_QUINZENAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL), "");
            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        } else if (nIntervaloID == bsc.core.BscStaticReferences.BSC_FREQ_SEMANAL) {

            htbIgnoreIDList.put(new Integer(bsc.core.BscStaticReferences.BSC_FREQ_DIARIA), "");

        }

        return htbIgnoreIDList;
    }

    public void graphRefreshColors() {
        ColorAppearance colorApp = oGraph.getColorAppearance();

        //Valores default para as cores
        if (jpvCorIndicador.getColor() == null) {
            jpvCorIndicador.setColor(new java.awt.Color(-10037761));
        }
        if (jpvCorReferencia.getColor() == null) {
            jpvCorReferencia.setColor(new java.awt.Color(-9208321));
        }
        if (jpvCorMeta.getColor() == null) {
            jpvCorMeta.setColor(new java.awt.Color(-11936875));
        }

        //Definindo modo de pintuta do gr�fico
        if (chkMeta.isSelected()) {
            Color[] oCores = new Color[vctSeqCor.size()];
            String reg;

            for (int indice = 0; indice < vctSeqCor.size(); indice++) {
                reg = (String) vctSeqCor.get(indice);

                if (reg.equals("META")) {
                    oCores[indice] = jpvCorMeta.getColor();
                } else if (reg.equals("INDICADOR")) {
                    oCores[indice] = jpvCorIndicador.getColor();
                } else {
                    oCores[indice] = jpvCorReferencia.getColor();
                }
            }

            colorApp.setModelStyle(ColorAppearance.MODEL_CUSTOM_LINEAR);
            colorApp.setCustomPalette(oCores);

        } else {
            if ((oGraph.getChartType() == ChartType.PIE_CHART || oGraph.getChartType() == ChartType.PIE_CHART_3D)) {
                colorApp.setModelStyle(ColorAppearance.MODEL_PURE_RANDOM);
                colorApp.setScaling(4);
            } else {
                colorApp.setModelStyle(ColorAppearance.MODEL_CUSTOM_LINEAR);
                /*Cores para Resultado, Refer�ncia e Meta.*/
                if (chkReferencia.isSelected() && chkMeta.isSelected()) {
                    colorApp.setCustomPalette(new Color[]{jpvCorIndicador.getColor(), jpvCorReferencia.getColor(), jpvCorMeta.getColor()});
                /*Cores para Resultado e Refer�ncia.*/
                } else if (chkReferencia.isSelected()) {
                    colorApp.setCustomPalette(new Color[]{jpvCorIndicador.getColor(), jpvCorReferencia.getColor()});
                /*Cores para Resultado e Meta.*/
                } else if (chkMeta.isSelected()) {
                    colorApp.setCustomPalette(new Color[]{jpvCorIndicador.getColor(), jpvCorMeta.getColor()});
                /*Core para Resultado.*/
                } else {
                    colorApp.setCustomPalette(new Color[]{jpvCorIndicador.getColor()});
                }
            }
        }
        colorApp.setScaling(0);//Degrade nas colunas do grafico.

        DefaultTableModel model = (DefaultTableModel) oTableGraph.getModel();
        model.setDataVector(model.getDataVector(), vctIndCab);
    }

    private String getSerieLabel(bsc.xml.BIXMLRecord tmpRec) {
        String strRetorno;
        int iIndAgrup = 0;
        int iMes = 0;

        iIndAgrup = Integer.parseInt(event.getComboValue(hstFrequencia, cbbAgrupamento));

        //Verifica se � feita a convers�o dos indicadores
        if (cbbAgrupamento.getSelectedIndex() > nIntervaloID) {
            iIndAgrup = nIntervaloID;
        }

        //Verifica se � o agrupamento default.
        if (iIndAgrup <= 0) {
            iIndAgrup = nRefIntervaloID;
        }

        iMes = tmpRec.getInt("MES");

        //Necess�rio corrigir quando o indicador fizer refer�ncia ao m�s.
        if (iIndAgrup == 8 || iIndAgrup == 6 || iIndAgrup == 5 || tmpRec.contains("META")) {
            iMes--;
        }

        strRetorno = setEixoX(tmpRec.getInt("DIA"), iMes, tmpRec.getInt("ANO"), iIndAgrup, tmpRec.contains("META"));

        return strRetorno;
    }

    /**
     * Retorna o indice de gr�fico de acordo com o item selecionado no combo estilo.
     * @param index Item selecionado no combo estilo
     * @return Indice do tipo de gr�fico no objeto do Gr�fico.
     */
    public int getGraphType(int index) {
        int graphType = 0;

        switch (index) {
            case 0:
                graphType = BscGraphFrame.COLUNA;
                break;
            case 1:
                graphType = BscGraphFrame.BARRA;
                break;
            case 2:
                graphType = BscGraphFrame.AREA;
                break;
            case 3:
                graphType = BscGraphFrame.LINHA;
                break;
            case 4:
                graphType = BscGraphFrame.HEATMAP;
                break;
            case 5:
                graphType = BscGraphFrame.BARRA3D;
                break;
            case 6:
                graphType = BscGraphFrame.COLUNA3D;
                break;
            case 7:
                graphType = BscGraphFrame.AREA3D;
                break;
            case 8:
                graphType = BscGraphFrame.LINHA3D;
                break;
            case 9:
                graphType = BscGraphFrame.HEATMAP3D;
                break;
            case 10:
                graphType = BscGraphFrame.STACKCOLUMN;
                break;
            case 11:
                graphType = BscGraphFrame.STACKCOLUMN3D;
                break;
            case 12:
                graphType = BscGraphFrame.STACKBAR3D;
                break;
        }
        return graphType;
    }
}