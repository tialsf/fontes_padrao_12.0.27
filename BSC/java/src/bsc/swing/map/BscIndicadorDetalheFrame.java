package bsc.swing.map;

import com.infragistics.ultrachart.JFCChart;
import com.infragistics.ultrachart.shared.styles.*;
import com.infragistics.ultrachart.resources.appearance.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import javax.swing.JTable;
import javax.swing.RepaintManager;
import javax.swing.event.TableModelEvent;
import bsc.xml.BIXMLRecord;
import bsc.xml.BIXMLVector;
import java.util.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.imageio.ImageIO;

public class BscIndicadorDetalheFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, java.awt.print.Printable{
	
	//ATEN��O. Este valores dever ser iguais ao do KPI_DEFS, constantes:
	//STATUS_GREEN,STATUS_YELLOW,STATUS_RED
	public final static int STATUS_GREEN	= 11;
	public final static int STATUS_YELLOW	= 13;
	public final static int STATUS_RED		= 12;
	public final static int COL_STATUS		= 3;
	private	bsc.core.BscFormController formController = bsc.core.BscStaticReferences.getBscFormController();
	private java.awt.Color corFonte = java.awt.Color.black;
	private java.awt.Color corFundo = java.awt.Color.white;
	
	private String recordType	= new String("INDICADOR_DET");
	private	JTable		oTable	= null;	//Tabela com os dados do gr�fico
	private	String		graphIndID,
		graphDataDe,
		graphDataAte;

	
	private	BIXMLVector	planilhaDados	= null;
	private Vector		vctValores		= new Vector();
	private String[]	tipoGraficos = {
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00007"),//"Coluna"
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00008"),//"Coluna 3D"
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00009"),//"Linha"
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00010"),//"Linha 3D"
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00011"),//"Pizza"
		java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00012"),//"Pizza 3D"
	};
	private boolean isStarted		= false;
	private boolean allValueGraph	= true;
	
	public void enableFields(){}
	
	public void disableFields() {}
	
	public void refreshFields() {
		isStarted = true;
		bsc.xml.BIXMLRecord rec = getRecord();
		//Carregando os dados dos graficos.
		refreshGrafico();
		//Carregando os dados das iniciativas.
		plaIniciativas.setDataSource(rec.getBIXMLVector("INICIATIVAS"));
		applyIniciativaFormat();
		//Propriedades dos objetivos.
		lblObjetivoNome.setText(rec.getString("OBJETIVO"));
		txtObjDesc.setText(rec.getString("OBJDESC"));
		//Carregando as propriedades que foram gravadas.
		applySavedOptions();
		setRenderTable();
		setSemaforo(rec.getInt("FAROL"));
	}
	
	
	public void setSemaforo(int feedback) {
		lblObjetivo.setIcon(bsc.core.BscStaticReferences.getBscImageResources().getImage(feedback));
	}
	
	/*
	 * Configurando as propriedades das colunas, e renderer.
	 */
	private void setRenderTable(){
		String tipoColuna	= null;
		BscIniciativasTableCellRenderer cellRenderer = new BscIniciativasTableCellRenderer(plaIniciativas);
		for(int col = 0; col < plaIniciativas.getModel().getColumnCount();col++){
			//Defini��o do render especifico para as colunas do tipo texto.
			//		plaIniciativas.getColumn(col).setCellRenderer(cellRenderer);
			plaIniciativas.getColumnModel().getColumn(col).setCellRenderer(cellRenderer);
		}
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(recordType);
		recordAux.set( "ID", id );
		recordAux.set( "PARENTID", id );//ParentID = id
		
		//Op��es do gr�fico
		recordAux.set("INDICADOR_DET_TIPO_GRAPH"	, cmbTipoGrafico.getSelectedIndex());//Tipo do grafico.
		recordAux.set("INDICADOR_DET_COR_GRAPH"		, jpvCor.getColor().getRGB());//Cor do grafico.
		recordAux.set("INDICADOR_DET_POS_DIV_VER"	, splitVertical.getDividerLocation());//Divis�o do split Vertical.
		recordAux.set("INDICADOR_DET_POS_DIV_GRAPLA", splitGrafPlanilha.getDividerLocation());//Divis�o do gr�fico com a planilha de dados.
		recordAux.set("INDICADOR_DET_POS_DIV_OBJINI", splitObjetivoIniciativa.getDividerLocation());//Divis�o do objetivo com a iniciativa.
		
		//Gravando as linhas que est�o selecionadas na tabela.
		StringBuffer lineSelected = new StringBuffer("");
		boolean selected = true;
		for(int line = 0; line < plaDados.getRowCount();line++){
			selected =  plaDados.getXMLData().get(line).getBoolean("VER_GRAFICO");
			if(selected){
				lineSelected.append("T");
			}else{
				lineSelected.append("F");
			}
		}
		recordAux.set("INDICADOR_DET_LIN_SEL",lineSelected.toString());
		
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlDesktop = new javax.swing.JPanel();
        barIndicador = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        splitVertical = new javax.swing.JSplitPane();
        splitObjetivoIniciativa = new javax.swing.JSplitPane();
        pnlObjetivo = new javax.swing.JPanel();
        scrObjDesc = new javax.swing.JScrollPane();
        txtObjDesc = new javax.swing.JTextArea();
        pnlObjCabec = new javax.swing.JPanel();
        lblObjetivoNome = new javax.swing.JLabel();
        lblObjetivo = new javax.swing.JLabel();
        pnlGrafico = new javax.swing.JPanel();
        scrGraph = new javax.swing.JScrollPane();
        oGraph = new com.infragistics.ultrachart.JFCChart();
        pnlTipoGrafico = new javax.swing.JPanel();
        javax.swing.JLabel lblTipoGrafico = new javax.swing.JLabel();

        cmbTipoGrafico = cmbTipoGrafico = new javax.swing.JComboBox(tipoGraficos);
        javax.swing.JLabel lblCorGrafico = new javax.swing.JLabel();
        jpvCor = new pv.jfcx.JPVColor();
        splitGrafPlanilha = new javax.swing.JSplitPane();
        pnlIniciativas = new javax.swing.JPanel();
        lblCabIniciativa = new javax.swing.JLabel();
        scrIniciativas = new javax.swing.JScrollPane();
        plaIniciativas = new bsc.beans.JBIXMLTable();
        scrDados = new javax.swing.JScrollPane();
        plaDados = new bsc.beans.JBIXMLTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Detalhes do Indicador\n");
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_medida.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(600, 358));
        getContentPane().setLayout(new java.awt.BorderLayout(0, 5));

        pnlDesktop.setPreferredSize(new java.awt.Dimension(456, 300));
        pnlDesktop.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                pnlDesktopComponentResized(evt);
            }
        });
        pnlDesktop.setLayout(new java.awt.BorderLayout());

        barIndicador.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        barIndicador.setPreferredSize(new java.awt.Dimension(0, 30));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnSave.setText(bundle.getString("BscBaseFrame_00001")); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        barIndicador.add(btnSave);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new java.awt.Dimension(3, 25));
        barIndicador.add(jSeparator1);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("BscMapaOvalFrame_00002")); // NOI18N
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        barIndicador.add(btnPrint);

        btnExportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("BscMapaOvalFrame_00003")); // NOI18N
        btnExportar.setMaximumSize(new java.awt.Dimension(80, 23));
        btnExportar.setMinimumSize(new java.awt.Dimension(32, 32));
        btnExportar.setPreferredSize(new java.awt.Dimension(80, 23));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        barIndicador.add(btnExportar);

        btnAbrir.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_download.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnAbrir.setText(bundle1.getString("BscMapaOvalFrame_00004")); // NOI18N
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        barIndicador.add(btnAbrir);

        pnlDesktop.add(barIndicador, java.awt.BorderLayout.NORTH);

        splitVertical.setDividerLocation(260);
        splitVertical.setDividerSize(8);
        splitVertical.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitVertical.setOneTouchExpandable(true);
        splitVertical.setPreferredSize(new java.awt.Dimension(456, 300));

        splitObjetivoIniciativa.setDividerLocation(60);
        splitObjetivoIniciativa.setDividerSize(8);
        splitObjetivoIniciativa.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitObjetivoIniciativa.setMinimumSize(new java.awt.Dimension(155, 75));
        splitObjetivoIniciativa.setOneTouchExpandable(true);
        splitObjetivoIniciativa.setPreferredSize(new java.awt.Dimension(404, 150));

        pnlObjetivo.setMinimumSize(new java.awt.Dimension(116, 0));
        pnlObjetivo.setPreferredSize(new java.awt.Dimension(200, 50));
        pnlObjetivo.setLayout(new java.awt.BorderLayout());

        scrObjDesc.setBorder(null);
        scrObjDesc.setMinimumSize(new java.awt.Dimension(21, 0));
        scrObjDesc.setOpaque(false);

        txtObjDesc.setColumns(5);
        txtObjDesc.setEditable(false);
        txtObjDesc.setLineWrap(true);
        txtObjDesc.setRows(5);
        txtObjDesc.setWrapStyleWord(true);
        txtObjDesc.setBorder(null);
        txtObjDesc.setFocusable(false);
        txtObjDesc.setPreferredSize(new java.awt.Dimension(100, 75));
        scrObjDesc.setViewportView(txtObjDesc);

        pnlObjetivo.add(scrObjDesc, java.awt.BorderLayout.CENTER);

        pnlObjCabec.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlObjCabec.setLayout(new java.awt.BorderLayout());

        lblObjetivoNome.setBackground(new java.awt.Color(255, 255, 255));
        lblObjetivoNome.setText("Totvs");
        lblObjetivoNome.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblObjetivoNome.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        lblObjetivoNome.setMaximumSize(new java.awt.Dimension(2007, 14));
        lblObjetivoNome.setOpaque(true);
        lblObjetivoNome.setPreferredSize(new java.awt.Dimension(3000, 14));
        pnlObjCabec.add(lblObjetivoNome, java.awt.BorderLayout.CENTER);

        lblObjetivo.setBackground(new java.awt.Color(255, 255, 255));
        lblObjetivo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblObjetivo.setText("Objetivo:");
        lblObjetivo.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        lblObjetivo.setOpaque(true);
        lblObjetivo.setPreferredSize(new java.awt.Dimension(110, 0));
        pnlObjCabec.add(lblObjetivo, java.awt.BorderLayout.WEST);

        pnlObjetivo.add(pnlObjCabec, java.awt.BorderLayout.NORTH);

        splitObjetivoIniciativa.setLeftComponent(pnlObjetivo);

        pnlGrafico.setMinimumSize(new java.awt.Dimension(153, 0));
        pnlGrafico.setPreferredSize(new java.awt.Dimension(402, 75));
        pnlGrafico.setLayout(new java.awt.BorderLayout());

        scrGraph.setMinimumSize(new java.awt.Dimension(23, 0));
        scrGraph.setPreferredSize(new java.awt.Dimension(402, 75));

        oGraph.setChartType(com.infragistics.ultrachart.shared.styles.ChartType.LINE_CHART);
        oGraph.setColorAppearance(new com.infragistics.ultrachart.resources.appearance.ColorAppearance(new java.awt.Color(245,192,106),new java.awt.Color(0,0,255),new java.awt.Color(0,0,0),2031380));
        oGraph.setData(new com.infragistics.ultrachart.resources.appearance.DataAppearance(false,-1,true,true));
        oGraph.setDoubleBuffered(true);
        oGraph.setHeatMapChart(new com.infragistics.ultrachart.resources.appearance.HeatMapChartAppearance(10, 1));
        oGraph.setLineChart(new com.infragistics.ultrachart.resources.appearance.LineChartAppearance(5,0,5,8,8));
        oGraph.setPieChart(new com.infragistics.ultrachart.resources.appearance.PieChartAppearance(null,null,null,"<PERCENT_VALUE:0.00>%","SansSerif",0,30,344200075L));
        oGraph.setPreferredSize(new java.awt.Dimension(400, 75));
        scrGraph.setViewportView(oGraph);

        pnlGrafico.add(scrGraph, java.awt.BorderLayout.CENTER);

        pnlTipoGrafico.setMinimumSize(new java.awt.Dimension(153, 0));
        pnlTipoGrafico.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        lblTipoGrafico.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTipoGrafico.setText("Tipo:");
        lblTipoGrafico.setPreferredSize(new java.awt.Dimension(35, 15));
        pnlTipoGrafico.add(lblTipoGrafico);

        cmbTipoGrafico.setMaximumSize(new java.awt.Dimension(140, 22));
        cmbTipoGrafico.setPreferredSize(new java.awt.Dimension(100, 20));
        cmbTipoGrafico.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoGraficoItemStateChanged(evt);
            }
        });
        pnlTipoGrafico.add(cmbTipoGrafico);

        lblCorGrafico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCorGrafico.setText("Cor:");
        lblCorGrafico.setPreferredSize(new java.awt.Dimension(35, 15));
        pnlTipoGrafico.add(lblCorGrafico);

        jpvCor.setBounds(new java.awt.Rectangle(0, 0, 60, 20));
        jpvCor.setButtonsImage(1);
        jpvCor.setCellSize(16);
        jpvCor.setImageSize(14);
        jpvCor.setRowCount(8);
        jpvCor.setText("Gr�fico");
        jpvCor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpvCorActionPerformed(evt);
            }
        });
        pnlTipoGrafico.add(jpvCor);

        pnlGrafico.add(pnlTipoGrafico, java.awt.BorderLayout.NORTH);

        splitObjetivoIniciativa.setRightComponent(pnlGrafico);

        splitVertical.setLeftComponent(splitObjetivoIniciativa);

        splitGrafPlanilha.setDividerLocation(76);
        splitGrafPlanilha.setDividerSize(8);
        splitGrafPlanilha.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitGrafPlanilha.setMinimumSize(new java.awt.Dimension(189, 75));
        splitGrafPlanilha.setOneTouchExpandable(true);
        splitGrafPlanilha.setPreferredSize(new java.awt.Dimension(454, 150));

        pnlIniciativas.setMinimumSize(new java.awt.Dimension(187, 0));
        pnlIniciativas.setPreferredSize(new java.awt.Dimension(452, 75));
        pnlIniciativas.setLayout(new java.awt.BorderLayout());

        lblCabIniciativa.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCabIniciativa.setText("Iniciativas (plano de a��es)");
        lblCabIniciativa.setPreferredSize(new java.awt.Dimension(187, 30));
        pnlIniciativas.add(lblCabIniciativa, java.awt.BorderLayout.NORTH);

        plaIniciativas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        plaIniciativas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                plaIniciativasMouseClicked(evt);
            }
        });
        scrIniciativas.setViewportView(plaIniciativas);

        pnlIniciativas.add(scrIniciativas, java.awt.BorderLayout.CENTER);

        splitGrafPlanilha.setRightComponent(pnlIniciativas);

        plaDados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        plaDados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                plaDadosMouseReleased(evt);
            }
        });
        scrDados.setViewportView(plaDados);

        splitGrafPlanilha.setLeftComponent(scrDados);

        splitVertical.setRightComponent(splitGrafPlanilha);

        pnlDesktop.add(splitVertical, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlDesktop, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void plaDadosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_plaDadosMouseReleased
		if(plaDados.getEditingColumn()==0){
			insertLineChart();
		}
	}//GEN-LAST:event_plaDadosMouseReleased

	private void plaIniciativasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_plaIniciativasMouseClicked
		if(evt.getClickCount() == 2){
			openIniciativa();
		}
	}//GEN-LAST:event_plaIniciativasMouseClicked
		
	private void openIniciativa(){
		int line = plaIniciativas.getSelectedRow();
		if(line != -1 ){
			bsc.xml.BIXMLRecord recLinha = plaIniciativas.getXMLData().get(line);
			String cID = recLinha.getString("ID");
			if(! cID.equals("0")){
				bsc.swing.framework.BscIniciativaFrame frmIniativas =(bsc.swing.framework.BscIniciativaFrame)
				formController.getForm("INICIATIVA",cID,"teste titulo");
				if(formController.lastView == formController.ST_JANELA ){
					try {
						frmIniativas.setMaximum(true);
					} catch (java.beans.PropertyVetoException ex) {
						ex.printStackTrace();
					}
				}
			}
		}
		
	}
	private void pnlDesktopComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_pnlDesktopComponentResized
		if(formController.lastView == formController.ST_PASTA ){
			applyIniciativaFormat();
			updatePlaDados();
			plaDados.getTableHeader().doLayout();
			plaIniciativas.getTableHeader().doLayout();
			plaIniciativas.getTableHeader().repaint();
		}
	}//GEN-LAST:event_pnlDesktopComponentResized
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		btnSave.setEnabled(false);
		event.editRecord();
		event.saveRecord();
		btnSave.setEnabled(true);
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
		bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(),true);
		
		frmChooser.setDialogType(frmChooser.BSC_DIALOG_OPEN);
		frmChooser.setFileType("*.jpg");
		frmChooser.loadServerFiles("mapest\\*.jpg");
		
		frmChooser.setVisible(true);
		if(frmChooser.isValidFile()){
			try {
				bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
				String strFileName = new String( "mapest\\" +frmChooser.getFileName());
				java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString()+strFileName);
				bscApplet.getAppletContext().showDocument(url,"_blank");
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}//GEN-LAST:event_btnAbrirActionPerformed
	
	private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		
		//Mensagem necess�ria para fazer o "split" do painel.
		bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(),true);
		frmChooser.setDialogType(frmChooser.BSC_DIALOG_SAVE);
		frmChooser.setFileType("*.jpg");
		frmChooser.loadServerFiles("mapest\\*.jpg");
		frmChooser.setFileName("inddet"+".jpg");
		frmChooser.setVisible(true);
		if(frmChooser.isValidFile()){
			saveComponentAsJPEG("mapest\\"+frmChooser.getFileName() );
		}
	}//GEN-LAST:event_btnExportarActionPerformed
	
	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		java.awt.print.PageFormat page = new java.awt.print.PageFormat();
		java.awt.print.Paper paper = new java.awt.print.Paper();
		
		page.setOrientation(java.awt.print.PageFormat.LANDSCAPE);
		paper.setSize(27,21);
		paper.setImageableArea(10,0,28,20);
		page.setPaper(paper);
		printJob.setPrintable(this);
		
		printJob.pageDialog(page);
		if (printJob.printDialog()){
			try {
				printJob.print();
			} catch(java.awt.print.PrinterException pe) {
				System.out.println("Error printing: " + pe);
			}
		}
	}//GEN-LAST:event_btnPrintActionPerformed
	
	private void jpvCorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpvCorActionPerformed
		setGraphColor();
	}//GEN-LAST:event_jpvCorActionPerformed
	
	private void cmbTipoGraficoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoGraficoItemStateChanged
		setGraphType(cmbTipoGrafico.getSelectedIndex());
	}//GEN-LAST:event_cmbTipoGraficoItemStateChanged
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToolBar barIndicador;
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cmbTipoGrafico;
    private javax.swing.JSeparator jSeparator1;
    private pv.jfcx.JPVColor jpvCor;
    private javax.swing.JLabel lblCabIniciativa;
    private javax.swing.JLabel lblObjetivo;
    private javax.swing.JLabel lblObjetivoNome;
    private com.infragistics.ultrachart.JFCChart oGraph;
    private bsc.beans.JBIXMLTable plaDados;
    private bsc.beans.JBIXMLTable plaIniciativas;
    private javax.swing.JPanel pnlDesktop;
    private javax.swing.JPanel pnlGrafico;
    private javax.swing.JPanel pnlIniciativas;
    private javax.swing.JPanel pnlObjCabec;
    private javax.swing.JPanel pnlObjetivo;
    private javax.swing.JPanel pnlTipoGrafico;
    private javax.swing.JScrollPane scrDados;
    private javax.swing.JScrollPane scrGraph;
    private javax.swing.JScrollPane scrIniciativas;
    private javax.swing.JScrollPane scrObjDesc;
    private javax.swing.JSplitPane splitGrafPlanilha;
    private javax.swing.JSplitPane splitObjetivoIniciativa;
    private javax.swing.JSplitPane splitVertical;
    private javax.swing.JTextArea txtObjDesc;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscIndicadorDetalheFrame(int operation, String idAux, String contextId) {
		//Inicia o formul�rio maximizado.
		initComponents();
		jpvCor.setColor(java.awt.Color.ORANGE);
		createChart();
		cmbTipoGrafico.setSelectedIndex(2);
		
		plaDados.getTableHeader().addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				plaSelectAll(evt);
			}
		});
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		recordType = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( recordType );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {}
	
	public void showOperationsButtons() {}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		setRecord(event.loadRecord(getID(),getType(),getCmd()));
		refreshFields();
	}
	
	public String getCmd(){
		return new String("");
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return new javax.swing.JTabbedPane();
	}
	
	private void plaSelectAll(java.awt.event.MouseEvent evt) {
		if(evt.getButton() == evt.BUTTON3){
			com.infragistics.ultrachart.resources.appearance.DataAppearance chart = oGraph.getData();
			for(int line = 0; plaDados.getRowCount() > line ; line ++){
				plaDados.getModel().setValueAt(allValueGraph,line,0);
				showLineGraph(line, allValueGraph);
			}
			
			if(allValueGraph){
				allValueGraph = false;
			}else{
				allValueGraph = true;
			}
		}
	}
	
	/**
	 *Insere ou retira a apresenta��o de linhas no grafico.
	 *
	 **/
	private void insertLineChart(){
		int line = plaDados.getSelectedRow();
		boolean lineVisible = oGraph.getData().isRowIncluded(line);
		showLineGraph(line, ! lineVisible);
	}
	
	private void createChart() {
		Object[]	headers = new Object[]{""};
		Object[][]	cells	= new Object[][] {{new Double(0)}};
		
		//Defini��o das constantes do gr�fico.
		com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_LEFT_TEXT	= java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00013");//"Valores"
		com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_LEFT_VISIBLE	= true;
		com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_FONT_COLOR	= new java.awt.Color(180, 0, 70);
		com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_TOP_HEIGHT	= 50;
		com.infragistics.ultrachart.resources.DefaultConstants.D_TITLE_BOTTOM_TEXT	= java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00014");//"Per�odo"
		
		//
		setGraphTitle(java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00015"));
		
		//Cria��o da tabela que conter� os valores do gr�fico.
		javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(cells, headers);
		oTable = new JTable(model);
		
		//Atribui��o dos valores a tabela.
		oGraph.getData().setDataSource(oTable);
		
		setGraphColor();
	}
	
	
	/**
	 *Atualizando o grafico
	 **/
	private void refreshGrafico(){
		javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)oTable.getModel();
		Vector	vctCabecalho = new Vector();
		vctValores.clear();
		
		planilhaDados = getRecord().getBIXMLVector("COLUNAS");
		
		//Preparando as linhas dos dados.
		for (int coluna = 0; coluna < planilhaDados.size();coluna++) {
			BIXMLRecord recColuna = planilhaDados.get(coluna);
			vctValores.addElement(criaColuna(recColuna));
		}
		
		//Vetor com os titulos do cabe�alho.
		vctCabecalho.add("Labels");
		vctCabecalho.add(java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscIndicadorDetalheFrame_00014"));
		
		//Atualiza os dados recebidos
		model.setDataVector(vctValores,vctCabecalho);
		model.fireTableChanged(new TableModelEvent(model));
		
		setGraphTitle(getRecord().getString("IND_NOME"));
		
		//Atualiza da planilha de dados.
		updatePlaDados();
	}
	
	private Vector criaColuna(BIXMLRecord recColuna){
		Vector valorColuna = new Vector();
		valorColuna.add(recColuna.getString("LABELX"));
		valorColuna.add(recColuna.getDouble("VALOR"));
		
		return valorColuna;
	}
	
	private void setGraphType(int tipo){
		int graphType = 0;
		
		if(tipo == 0){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.COLUMN_CHART;
		}else if(tipo == 1){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.COLUMN_CHART_3D;
		}else if(tipo == 2){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.LINE_CHART;
		}else if(tipo == 3){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.LINE_CHART_3D;
		}else if(tipo == 4){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.PIE_CHART;
		}else if(tipo == 5){
			graphType = com.infragistics.ultrachart.shared.styles.ChartType.PIE_CHART_3D;
		}
		
		oGraph.setChartType(graphType);
		setGraphColor();
	}
	
	private void setGraphColor(){
		com.infragistics.ultrachart.resources.appearance.ColorAppearance colorApp = oGraph.getColorAppearance();
		
		
		if((oGraph.getChartType() == ChartType.PIE_CHART || oGraph.getChartType() == ChartType.PIE_CHART_3D)){
			colorApp.setModelStyle(colorApp.MODEL_PURE_RANDOM);
			colorApp.setScaling(4);
		} else {
			java.awt.Color[] cores = new java.awt.Color[] {jpvCor.getColor()};
			colorApp.setScaling(0);
			colorApp.setModelStyle(colorApp.MODEL_CUSTOM_LINEAR);
			colorApp.setColorByRow(false);
			colorApp.setCustomPalette(cores);
		}
	}
	
	private void setLegenda(){
/*
		oGraph.getLegend().setVisible(false); //Desabilita para for�ar o refresh em baixo.
 
		int legendaLocation = cmbLegenda.getSelectedIndex();
 
		if(legendaLocation > 0){
			oGraph.getLegend().setVisible(true); //Mostra Legenda.
			oGraph.getLegend().setLocation(legendaLocation); //Posi��o da legenda na tela.
			oGraph.getLegend().setSpanPercentage(17);// Tamanho da legenda.
		}
 */
	}
	
	private void setGraphTitle(String texto){
		TitleAppearance appT = oGraph.getTitleTop();
		appT.setText(texto);
		appT.setFont(new java.awt.Font("SansSerif", java.awt.Font.ITALIC, 18));
		appT.setVisible(true);
		appT.setHorizontalAlign(0);
		appT.setTitleLocation(0);
	}
	
	private void setBackGroundColor(){
		com.infragistics.ultrachart.resources.appearance.BackgroundAppearance app = oGraph.getBackgroundAppearance();
//		app.setColor(jPVCorFundo.getColor());
	}
	
	private void updatePlaDados(){
		plaDados.setDataSource(planilhaDados);
		plaDados.getParent().setBackground(corFundo);
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	private void applySavedOptions(){
		bsc.xml.BIXMLRecord rec = getRecord().getBIXMLVector("PROPRIEDADES").get(0);
		cmbTipoGrafico.setSelectedIndex(rec.getInt("INDICADOR_DET_TIPO_GRAPH"));
		jpvCor.setColor(new java.awt.Color(rec.getInt("INDICADOR_DET_COR_GRAPH")));
		splitVertical.setDividerLocation(rec.getInt("INDICADOR_DET_POS_DIV_VER"));
		splitGrafPlanilha.setDividerLocation(rec.getInt("INDICADOR_DET_POS_DIV_GRAPLA"));
		splitObjetivoIniciativa.setDividerLocation(rec.getInt("INDICADOR_DET_POS_DIV_OBJINI"));
		
		for(int line = 0; line < plaDados.getRowCount();line++){
			boolean selected =  plaDados.getXMLData().get(line).getBoolean("VER_GRAFICO");
			showLineGraph(line, selected);
		}
	}
	
	private void showLineGraph(int line, boolean visible){
		com.infragistics.ultrachart.resources.appearance.DataAppearance chart = oGraph.getData();
		boolean lineVisible = chart.isRowIncluded(line);
		if(! lineVisible && visible) {
			chart.includeRow(line, true);
		}else if(lineVisible && ! visible){
			chart.includeRow(line, false);
		}
	}
	
	
	private void applyIniciativaFormat(){
		IniciativaHeadRenderer headerRender = new IniciativaHeadRenderer();
		plaIniciativas.setSelectionBackground(corFundo);
		plaIniciativas.setSelectionForeground(corFonte);
		plaIniciativas.setForeground(corFonte);
		plaIniciativas.setGridColor(corFundo);
		plaIniciativas.setBackground(corFundo);
//		plaIniciativas.setFont(fontTabela);
		for(int col = 0; plaIniciativas.getColumnModel().getColumnCount() > col ; col++){
			plaIniciativas.getColumnModel().getColumn(col).setHeaderRenderer(headerRender);
		}
		plaIniciativas.getParent().setBackground(corFundo);
	}
	
	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex > 0) {
			return(NO_SUCH_PAGE);
		} else {
			java.awt.Graphics2D g2d = (java.awt.Graphics2D)g;
			g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
			g2d.scale( 0.75, 1.3 );
			disableDoubleBuffering(this);
			splitVertical.paint(g2d);
			enableDoubleBuffering(this);
			return(PAGE_EXISTS);
		}
	}
	
	private static void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
	
	private static void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
	
	private void saveComponentAsJPEG(String fileName) {
		java.awt.Dimension size = splitVertical.getSize();
		java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height ,java.awt.image.BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = myImage.createGraphics();
		disableDoubleBuffering(splitVertical);
		splitVertical.paint(g2);
		enableDoubleBuffering(splitVertical);
		
		//Grava a figura l� no servidor... :)
		try {
			java.io.ByteArrayOutputStream out = new  java.io.ByteArrayOutputStream();
			//com.sun.image.codec.jpeg.JPEGImageEncoder encoder = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(out);
			//encoder.encode(myImage);
                        /*Salva o arquivo de imagem como JPEG*/
                        ImageIO.write(myImage, "jpg", out);
			String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
			event.saveBase64(fileName, strBase64);
			out.close();
			//Abre o navegador para salvar a figura localmente
			bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
			java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString()+fileName);
			bscApplet.getAppletContext().showDocument(url,"_new");
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	class IniciativaHeadRenderer extends DefaultTableCellRenderer {
		private LabelHeader lblRender = new LabelHeader();
		public java.awt.Component getTableCellRendererComponent(javax.swing.JTable t,Object v, boolean s, boolean f, int r, int c) {
			lblRender.setText(v.toString());
			lblRender.setBackground(corFundo);
			lblRender.setForeground(corFonte);
			lblRender.setOpaque(true);
			return lblRender;
		}
	}
	
	class LabelHeader extends javax.swing.JLabel{
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(new BasicStroke(5.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
			g.setColor(java.awt.Color.BLACK);
			g.drawLine(0,getHeight(),getWidth(),getHeight());
		}
	}
	
	
		
	class BscIniciativasTableCellRenderer extends javax.swing.table.DefaultTableCellRenderer{
		private bsc.beans.JBIXMLTable biTable	= null;
		private final  java.awt.Color BRANCA	=	java.awt.Color.white;
		private final  java.awt.Color AMARELO	=	new java.awt.Color(255,235,155);
		private final  java.awt.Color VERDE		=	new java.awt.Color(139,191,150);
		private final  java.awt.Color VERMELHO	=	new java.awt.Color(234,106,106);
		private final  java.awt.Color CINZA		=	new java.awt.Color(228,228,228);
				
		
		public BscIniciativasTableCellRenderer(){}
		
		public BscIniciativasTableCellRenderer(bsc.beans.JBIXMLTable table){
			biTable = table;
		}
		
		// Sobreposicao para mudar o comportamento de pintura da celula.
		public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			pv.jfcx.JPVEdit oRender = new pv.jfcx.JPVEdit();
			BIXMLRecord recLinha	= null;
			
		/*
		 *Preparando o objeto para renderiza��o.
		 */
			oRender.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
			oRender.setLAF(false);
			oRender.setBorderStyle(0);
			//oRender.setFont(new java.awt.Font("Verdana",0,10));
			
			
			if(biTable != null){
				recLinha = plaIniciativas.getXMLData().get(row);
				if (hasFocus) {
					oRender.setBackground(CINZA);
				}else {
					switch(recLinha.getInt("NSITUACAO")){
						case 1 :
							oRender.setBackground(BRANCA);
							break;
						case 3:
							oRender.setBackground(VERDE);
							break;
						case 5:
							oRender.setBackground(BRANCA);
							break;
						case 6:
							oRender.setBackground(VERMELHO);
							break;
						default:
							oRender.setBackground(AMARELO);
							break;
					}
				}
			}
			
			oRender.setValue(value.toString());
			oRender.setAutoScroll(true);
			oRender.setLAF(false);
			return oRender;
		}


	}
	
}