/*
 * BscMapaFcs.java
 *
 * Created on 13 de Julho de 2005, 15:12
 */

package bsc.swing.map;

import java.awt.event.*;
import javax.swing.tree.*;
import bsc.xml.*;

/**
 *
 * @author  siga0739 - Aline
 */ 
public class BscMapaFcsPrc extends BscMapaJPanel implements javax.swing.event.TreeSelectionListener {

	public BscMapaFcsPrc( bsc.xml.BIXMLVector vector) {
		initComponents();
		bsc.xml.BIXMLVector listaArvores = vector;
		// Init
		bscTreeElemento.removeAll();
		bscTreeElemento.setCellRenderer( new bsc.swing.BscTreeCellRenderer(bsc.core.BscStaticReferences.getBscImageResources()) );
		bscTreeElemento.setModel( new DefaultTreeModel(createTree( listaArvores )));

		int jRows = 0;
		while (jRows < bscTreeElemento.getRowCount()){
		bscTreeElemento.expandRow(jRows);
		jRows ++;
		}

		bscTreeElemento.addTreeSelectionListener( this );
	}
	
	public BscMapaFcsPrc( ) {
		initComponents();
		bscTreeElemento.removeAll();
		bscTreeElemento.setCellRenderer( new bsc.swing.BscTreeCellRenderer(bsc.core.BscStaticReferences.getBscImageResources()) );
	}

	public DefaultMutableTreeNode createTree( BIXMLVector treeXML ) {
		DefaultMutableTreeNode root = insertGroup( treeXML , null );
		return root;
	}
	
	public DefaultMutableTreeNode insertRecord( BIXMLRecord record,	DefaultMutableTreeNode tree )
		throws BIXMLException{

		bsc.swing.BscTreeNode treeNode = new bsc.swing.BscTreeNode( record.getTagName(),
									    record.getString("ID"),
									    record.getString("NOME"));

		DefaultMutableTreeNode child = new DefaultMutableTreeNode(treeNode);

		for ( java.util.Iterator e = record.getKeyNames(); e.hasNext(); )
			child = insertGroup( record.getBIXMLVector( (String)e.next() ),
							child );

		if (tree != null)
		{
			tree.add(child);
			return tree;
		}
		else
			return child;
	}

	public DefaultMutableTreeNode insertGroup( BIXMLVector vector, DefaultMutableTreeNode tree ) throws BIXMLException {
		DefaultMutableTreeNode child =	new DefaultMutableTreeNode( 
							    new bsc.swing.BscTreeNode(
									    "Processos do FCS",
									    "ID",
									    java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscOrganizacaoFrame_00029")));

		for ( int i = 0; i < vector.size(); i++ ) {
			child = insertRecord( vector.get(i), child );

		}

		if (tree != null) {
			//tree.add(child);
			return tree;
		}
		else
			return child;
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    private void initComponents() {//GEN-BEGIN:initComponents
        jScrollTree = new javax.swing.JScrollPane();
        bscTreeElemento = new javax.swing.JTree();

        setLayout(null);

        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
        setFocusable(false);
        setRequestFocusEnabled(false);
        setVerifyInputWhenFocusTarget(false);
        jScrollTree.setBorder(null);
        jScrollTree.setPreferredSize(new java.awt.Dimension(50, 150));
        bscTreeElemento.setBorder(new javax.swing.border.TitledBorder(""));
        bscTreeElemento.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        bscTreeElemento.setMinimumSize(new java.awt.Dimension(20, 21));
        bscTreeElemento.setOpaque(false);
        bscTreeElemento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bscTreeElementoMouseClicked(evt);
            }
        });

        jScrollTree.setViewportView(bscTreeElemento);

        add(jScrollTree);
        jScrollTree.setBounds(1, 1, 227, 260);

    }//GEN-END:initComponents

	private void bscTreeElementoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bscTreeElementoMouseClicked
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTreeElemento.getLastSelectedPathComponent();

		if(node != null){

		    if( node.getUserObject() instanceof bsc.swing.BscTreeNode){
				bsc.swing.BscTreeNode bscNode = (bsc.swing.BscTreeNode) node.getUserObject();
				if(!(bscNode.getID().equals("ID"))){
					bsc.swing.BscDefaultFrameFunctions frame =
					bsc.core.BscStaticReferences.getBscFormController().getForm( "PROCESSO", ""+bscNode.getID(), bscNode.getName() );
				}
			}
		}
	}//GEN-LAST:event_bscTreeElementoMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree bscTreeElemento;
    private javax.swing.JScrollPane jScrollTree;
    // End of variables declaration//GEN-END:variables
	
	public void valueChanged(javax.swing.event.TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) bscTreeElemento.getLastSelectedPathComponent();
	}
}
