package bsc.swing.map;

import bsc.applet.BscMainPanel;
import bsc.beans.JBILayeredPane;
import bsc.swing.BscDefaultFrameBehavior;
import bsc.swing.BscFileChooser;
import bsc.xml.BIXMLRecord;
import bsc.xml.BIXMLVector;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.border.TitledBorder;
import javax.imageio.ImageIO;

/**
 *
 * @author Alexandre Silva
 */
public class BscMapaFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions,
        java.awt.print.Printable, MouseListener, MouseMotionListener,
        javax.swing.event.ChangeListener {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    public static final int DRAG_NOTHING = 0;
    public static final int DRAG_OBJ_CONNECT = 1;
    public static final int DRAG_OBJ_MOVING = 2;
    public static final int DRAG_PER_MOVING = 3;
    public static final int DRAG_RESIZE_LEFT = 4;
    public static final int DRAG_RESIZE_UP = 5;
    public static final int DRAG_RESIZE_RIGHT = 6;
    public static final int DRAG_RESIZE_DOWN = 7;
    public static final int LARGURABORDA = 5;
    public static final int PER_MIN_SIZE = 160;
    public static final int PER_MAX_SIZE = 600;
    public boolean isMaximize = false;
    private static final int ABAIXO = 0;
    private static final int ACIMA = 1;
    private static final int ESQUERDA = 2;
    private static final int DIREITA = 3;
    private static final Color COLOR_ENABLED = Color.black;
    private static final Color COLOR_DISABLED = Color.darkGray;
    private String type = new String("MAPAEST");
    private int dragState = DRAG_NOTHING;
    private int temaCountID = 1;
    private int lastTrackVal = 0;
    private boolean lUpdateRecord = false;
    private boolean editEnabled = false;
    private Point mousePressedLocation = new Point();
    private Point pMousePressed = new Point();
    private BscMapaLigacao ligacaoPopUp;
    private BscMapaLigacao ligacaoConnected;
    private HashMap idHashMap;
    private HashSet ligHashSet;
    private BscMapaJPanel dragSource;
    private BscMapaJPanel dragDestine;
    private JPanel jPanelVidro;
    private bsc.util.BscArrow2D virtualLine;
    private Color arrowColor = COLOR_DISABLED;
    private BscMapaTema temaPopUp;
    private BscMapaObjetivo objetivoPopUp;
    private BscMapaPerspectiva mapaPerspectivaPopUp;
    private bsc.util.BscToolKit oBscToolKit = new bsc.util.BscToolKit();
    private java.util.Hashtable hstTemasFiltro = new java.util.Hashtable();
    private bsc.xml.BIXMLVector vctTemaEstrategico;
    private BscMapaMaximize maximizeForm;

    public void enableFields() {
        // Habilita os campos do formul�rio.
        arrowColor = COLOR_ENABLED;
        jToolBarMapa.setVisible(true);
        editEnabled = true;
        fldNomeTemaVert1.setEnabled(true);
        fldNomeTemaVert2.setEnabled(true);
        fldNomeTemaVert3.setEnabled(true);

        //Configurando a drop down para apresentar o 1� item.
        if (cbbFiltroTema.getItemCount() > 0) {
            cbbFiltroTema.setSelectedIndex(0);
        }

        cbbFiltroTema.setEnabled(false);

        jPanelVidro.repaint();
    }

    public void disableFields() {
        bsc.xml.BIXMLRecord ligacao;
        bsc.xml.BIXMLVector temas;

        // Desabilita os campos do formul�rio.
        arrowColor = COLOR_DISABLED;
        jToolBarMapa.setVisible(false);
        editEnabled = false;
        fldNomeTemaVert1.setEnabled(false);
        fldNomeTemaVert2.setEnabled(false);
        fldNomeTemaVert3.setEnabled(false);

        cbbFiltroTema.setEnabled(true);

        if (status == BscDefaultFrameBehavior.UPDATING) {
            //Requisitando os novos ids ao protheus.
            bsc.xml.BIXMLRecord recRefresh = event.loadRecord(id, "MAPATEMA");
            bsc.xml.BIXMLVector vctTemas = recRefresh.getBIXMLVector("MAPATEMAS"); //Temas
            bsc.xml.BIXMLVector ligacoes = recRefresh.getBIXMLVector("MAPARELS"); //Ligacoes

            //Marcando os temas para remo��o...
            for (int perspectivaItem = 0; perspectivaItem < jPanelMapa.getComponentCount(); perspectivaItem++) {
                if (jPanelMapa.getComponent(perspectivaItem) instanceof BscMapaPerspectiva) {
                    BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) jPanelMapa.getComponent(perspectivaItem);
                    //Removendo todos os temas da perspectiva.
                    for (int temaItem = 0; temaItem < mapaPerspectiva.getLayeredPane().getComponentCount(); temaItem++) {
                        if (mapaPerspectiva.getLayeredPane().getComponent(temaItem) instanceof BscMapaTema) {
                            BscMapaTema mapaTema = (BscMapaTema) mapaPerspectiva.getLayeredPane().getComponent(temaItem);
                            mapaTema.temaSelected = true;
                        }
                    }
                }
            }

            //Removendo os temas...
            removeThemaFromPerspective();

            //Adicionando os temas...
            for (int perspectivaItem = 0; perspectivaItem < jPanelMapa.getComponentCount(); perspectivaItem++) {
                if (jPanelMapa.getComponent(perspectivaItem) instanceof BscMapaPerspectiva) {
                    BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) jPanelMapa.getComponent(perspectivaItem);
                    temas = new bsc.xml.BIXMLVector("MAPATEMAS", "MAPATEMA");
                    for (int idPersp = 0; idPersp < vctTemas.size(); idPersp++) {
                        if (mapaPerspectiva.getId() == vctTemas.get(idPersp).getInt("PARENTID")) {
                            temas.add(vctTemas.get(idPersp));
                        }
                    }
                    addThemaFromXml(mapaPerspectiva, temas);
                }
            }

            //Se j� existerem conexoes limpa.
            if (ligHashSet != null) {
                ligHashSet.clear();
            }

            //Adicionado os Temas
            for (int i = 0; i < ligacoes.size(); i++) {
                ligacao = ligacoes.get(i);
                try {
                    BscMapaLigacao bscLigacao = new BscMapaLigacao(ligacao.getString("SCRTYPE").trim().concat(ligacao.getString("SRCID")),
                            ligacao.getString("DESTYPE").trim().concat(ligacao.getString("DESTID")));
                    ligHashSet.add(bscLigacao);
                    bscLigacao.setLigacaoType(ligacao.getInt("LINETYPE"));
                    if (ligacao.getInt("LINECTRLX") == 0 && ligacao.getInt("LINECTRLY") == 0) {
                        bscLigacao.ctrl.setBounds(ligacao.getInt("LINECTRLX"), ligacao.getInt("LINECTRLY"), 0, 0);
                    } else {
                        bscLigacao.ctrl.setBounds(ligacao.getInt("LINECTRLX"), ligacao.getInt("LINECTRLY"), 4, 4);
                    }

                } catch (Exception e) {
                    System.out.println("Erro... Liga��o 2" + e);
                }
            }

            repaint();
        }
    }

    public void refreshFields() {
        BIXMLVector perspectivas;
        BIXMLVector objetivos;
        BIXMLVector temas;
        BIXMLVector ligacoes;
        BIXMLVector vctMapaProp;
        BIXMLRecord perspectiva;
        BIXMLRecord objetivo;
        BIXMLRecord ligacao;
        BIXMLRecord recMapaProp;
        BscMapaPerspectiva mapaPerspectiva;
        BscMapaObjetivo mapaObjetivo;

        fldDataAlvo.setCalendar(record.getDate("DATAALVO"));
        if (record.getString("PARCELADA").equals("S")) {
            fldAnalise.setText("Parcelada");
        } else {
            fldAnalise.setText("Acumulada");
        }

        // Ligacoes
        idHashMap = new HashMap();
        ligHashSet = new HashSet();

        // Remove anteriores
        while (jPanelMapa.getComponentCount() > 0) {
            jPanelMapa.remove(0);
        }

        jLayeredPaneMapa.setNomeEstrategia(record.getString("NOME"));

        // Perspectivas
        perspectivas = record.getBIXMLVector("PERSPECTIVAS");
        java.awt.Color perDefColor = new java.awt.Color(255, 255, 255),
                objDefColor = new java.awt.Color(162, 180, 207);

        //Temas estrat�gicos
        java.util.HashMap idTemaEstragicos = new HashMap();
        //Valor do item 0 da drop down, usado para controle da execu��o do refresh.
        String cmbValor0 = (String) cbbFiltroTema.getItemAt(0);

        if (cmbValor0.trim().length() > 0 && vctTemaEstrategico.get(cbbFiltroTema.getSelectedIndex()).contains("TEMSESTOBJ")) {
            bsc.xml.BIXMLVector vctObjetivosTemas = vctTemaEstrategico.get(cbbFiltroTema.getSelectedIndex()).getBIXMLVector("TEMSESTOBJ");
            for (int iTemas = 0; iTemas < vctObjetivosTemas.size(); iTemas++) {
                idTemaEstragicos.put(vctObjetivosTemas.get(iTemas).getString("ID"), null);
            }
            if (lUpdateRecord) {
                loadRecord();
                lUpdateRecord = false;
            }
        }

        for (int i = 0; i < perspectivas.size(); i++) {

            perspectiva = perspectivas.get(i);
            mapaPerspectiva = new BscMapaPerspectiva();
            mapaPerspectiva.setId(perspectiva.getInt("ID"));
            mapaPerspectiva.setNome(perspectiva.getString("NOME"));
            mapaPerspectiva.setDescricao(perspectiva.getString("DESCRICAO"));
            mapaPerspectiva.setOrdem(perspectiva.getInt("ORDEM"));
            mapaPerspectiva.setPreferredSize(new Dimension(948, perspectiva.getInt("HEIGHT")));
            java.awt.Color perColor = (perspectiva.getInt("BACKCOLOR") == 0) ? perDefColor : new java.awt.Color(perspectiva.getInt("BACKCOLOR"));
            mapaPerspectiva.setBackground(perColor);

            mapaPerspectiva.addMouseListener(this);
            mapaPerspectiva.addMouseMotionListener(this);

            // Objetivos
            int sx = 1, sy = 1, x, y, height, width;

            objetivos = perspectiva.getBIXMLVector("OBJETIVOS");
            for (int j = 0; j < objetivos.size(); j++) {
                objetivo = objetivos.get(j);

                if (cbbFiltroTema.getSelectedIndex() == 0 || idTemaEstragicos.containsKey(objetivo.getString("ID"))) {
                    mapaObjetivo = new BscMapaObjetivo(mapaPerspectiva);
                    mapaObjetivo.setId(objetivo.getString("ID"));
                    mapaObjetivo.setNome(objetivo.getString("NOME"));
                    mapaObjetivo.setDescricao(objetivo.getString("DESCRICAO"));
                    mapaObjetivo.setFeedback(objetivo.getInt("FEEDBACK"));
                    mapaObjetivo.setHasFcs(objetivo.getBIXMLVector("FCSS").size() > 0);
                    x = sx;
                    y = sy;
                    sx = (objetivo.getInt("MAPX") == -1) ? sx : objetivo.getInt("MAPX");
                    sy = (objetivo.getInt("MAPY") == -1) ? sy : objetivo.getInt("MAPY");

                    height = (objetivo.getInt("MAPHEIGHT") < 40) ? 40 : objetivo.getInt("MAPHEIGHT");
                    width = (objetivo.getInt("MAPWIDTH") < 100) ? 100 : objetivo.getInt("MAPWIDTH");

                    java.awt.Color objColor = (objetivo.getInt("MAPCOLOR") == 0) ? objDefColor : new java.awt.Color(objetivo.getInt("MAPCOLOR"));
                    mapaObjetivo.addMouseListener(this);
                    mapaObjetivo.addMouseMotionListener(this);
                    mapaObjetivo.getJTextCorpo().addMouseListener(this);
                    mapaObjetivo.getJTextCorpo().addMouseMotionListener(this);
                    mapaPerspectiva.getLayeredPane().add(mapaObjetivo, (j == 0) ? 2 : 1);
                    mapaObjetivo.setBounds(new Rectangle(sx, sy, width, height));
                    mapaObjetivo.jPanelSuperior.setBackground(objColor);
                    mapaObjetivo.setObjetivoType(objetivo.getInt("MAPTYPE"));
                    if (bsc.core.BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().
                            equals(BscMainPanel.ST_AREATRABALHO) && objetivo.getInt("USEROWNER") == 0) {
                        mapaObjetivo.jTextCorpo.setSelectionColor(new java.awt.Color(102, 102, 255));
                    }

                    idHashMap.put(mapaObjetivo.getId(), mapaObjetivo);
                    sx = x;
                    sy = y;
                    sx += 10;
                    sy += 10;
                    if (sy > perspectiva.getInt("HEIGHT") - 40) {
                        sy = 11;
                    }
                }
            }

            //Painel de Legendas...
            if (bsc.core.BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().
                    equals(BscMainPanel.ST_AREATRABALHO)) {
                btnEdit.setEnabled(false);
                pnlLegenda.setVisible(true);
            } else {
                pnlLegenda.setVisible(false);
            }

            //Adicionado os Temas
            temas = perspectiva.getBIXMLVector("MAPATEMAS");
            addThemaFromXml(mapaPerspectiva, temas);

            //Configuranado as propriedades do Mapa.
            vctMapaProp = record.getBIXMLVector("MAPAPROPS");
            for (int iP = 0; iP < vctMapaProp.size(); iP++) {
                recMapaProp = vctMapaProp.get(iP);
                int iDiv = recMapaProp.getInt("DIVISOES");
                iDiv = iDiv > 1 ? iDiv : 1;
                spnDivideMapa.setValue(new Integer(iDiv).toString());
                fldNomeTemaVert1.setText(recMapaProp.getString("NOMEDIV1"));
                fldNomeTemaVert2.setText(recMapaProp.getString("NOMEDIV2"));
                fldNomeTemaVert3.setText(recMapaProp.getString("NOMEDIV3"));
                //Verificando se a Nome da divis�o est� em branco...
                if (fldNomeTemaVert1.getText().length() < 1) {
                    fldNomeTemaVert1.setText(record.getString("NOME"));
                }
                if (fldNomeTemaVert2.getText().length() < 1) {
                    fldNomeTemaVert2.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00018"));
                }
                if (fldNomeTemaVert3.getText().length() < 1) {
                    fldNomeTemaVert3.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00018"));
                }
            }

            jPanelMapa.add(mapaPerspectiva);
        }

        // Ligacoes - ligHashSet
        ligacoes = record.getBIXMLVector("MAPARELS");
        for (int i = 0; i < ligacoes.size(); i++) {
            ligacao = ligacoes.get(i);
            try {
                BscMapaLigacao bscLigacao = new BscMapaLigacao(ligacao.getString("SCRTYPE").trim().concat(ligacao.getString("SRCID")),
                        ligacao.getString("DESTYPE").trim().concat(ligacao.getString("DESTID")));
                ligHashSet.add(bscLigacao);
                bscLigacao.setLigacaoType(ligacao.getInt("LINETYPE"));
                if (ligacao.getInt("LINECTRLX") == 0 && ligacao.getInt("LINECTRLY") == 0) {
                    bscLigacao.ctrl.setBounds(ligacao.getInt("LINECTRLX"), ligacao.getInt("LINECTRLY"), 0, 0);
                } else {
                    bscLigacao.ctrl.setBounds(ligacao.getInt("LINECTRLX"), ligacao.getInt("LINECTRLY"), 4, 4);
                }

            } catch (Exception e) {
                System.out.println("Erro... liga��o 1" + e);
            }
        }

        validateMapa();
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {

        bsc.xml.BIXMLVector perspectivas, objetivos, ligacoes, vctMapaTemas, vctTemaCont, vctMapaProp;
        bsc.xml.BIXMLRecord perspectiva, objetivo, ligacao, recMapaTema, recTemaCont, recMapaProp;

        BscMapaPerspectiva mapaPerspectiva;
        BscMapaObjetivo mapaObjetivo;
        BscMapaTema mapaTema;

        // Perspectivas
        perspectivas = new bsc.xml.BIXMLVector("PERSPECTIVAS", "PERSPECTIVA");
        for (int i = 0; i < jPanelMapa.getComponentCount(); i++) {
            mapaPerspectiva = (BscMapaPerspectiva) jPanelMapa.getComponent(i);
            perspectiva = new bsc.xml.BIXMLRecord("PERSPECTIVA");
            perspectiva.set("ID", mapaPerspectiva.getId());
            perspectiva.set("NOME", mapaPerspectiva.getNome());
            perspectiva.set("DESCRICAO", mapaPerspectiva.getDescricao());
            perspectiva.set("ORDEM", mapaPerspectiva.getOrdem());
            //perspectiva.set("STATUS", mapaPerspectiva.getStatus();
            perspectiva.set("HEIGHT", mapaPerspectiva.getSize().height);
            perspectiva.set("BACKCOLOR", mapaPerspectiva.getBackground().getRGB());
            // Objetivos
            objetivos = new bsc.xml.BIXMLVector("OBJETIVOS", "OBJETIVO");
            vctMapaTemas = new bsc.xml.BIXMLVector("MAPATEMAS", "MAPATEMA");
            for (int j = 0; j < mapaPerspectiva.getLayeredPane().getComponentCount(); j++) {
                if (mapaPerspectiva.getLayeredPane().getComponent(j) instanceof BscMapaObjetivo) {
                    mapaObjetivo = (BscMapaObjetivo) mapaPerspectiva.getLayeredPane().getComponent(j);
                    objetivo = new bsc.xml.BIXMLRecord("OBJETIVO");
                    objetivo.set("ID", mapaObjetivo.getId());
                    objetivo.set("NOME", mapaObjetivo.getNome());
                    objetivo.set("DESCRICAO", mapaObjetivo.getDescricao());
                    objetivo.set("MAPX", mapaObjetivo.getLocation().x);
                    objetivo.set("MAPY", mapaObjetivo.getLocation().y);
                    objetivo.set("MAPHEIGHT", mapaObjetivo.getHeight());
                    objetivo.set("MAPWIDTH", mapaObjetivo.getWidth());
                    objetivo.set("MAPCOLOR", mapaObjetivo.jPanelSuperior.getBackground().getRGB());
                    objetivo.set("MAPTYPE", mapaObjetivo.getObjetivoType());
                    objetivos.add(objetivo);


                } else if (mapaPerspectiva.getLayeredPane().getComponent(j) instanceof BscMapaTema) {
                    //Adiciona os temas.
                    mapaTema = (BscMapaTema) mapaPerspectiva.getLayeredPane().getComponent(j);
                    javax.swing.border.TitledBorder border = (javax.swing.border.TitledBorder) mapaTema.getBorder();

                    recMapaTema = new bsc.xml.BIXMLRecord("MAPATEMA");
                    recMapaTema.set("ID", mapaTema.getId());
                    recMapaTema.set("NOME", border.getTitle());
                    recMapaTema.set("X", mapaTema.getX());
                    recMapaTema.set("Y", mapaTema.getY());
                    recMapaTema.set("HEIGHT", mapaTema.getHeight());
                    recMapaTema.set("WIDTH", mapaTema.getWidth());
                    recMapaTema.set("BACKCOLOR", mapaTema.getBackground().getRGB());

                    vctTemaCont = new bsc.xml.BIXMLVector("TEMAOBJETIVOS", "TEMAOBJETIVO");

                    //Adciona os objetivos que est�o dentro dos temas.
                    for (int objItem = 0; mapaTema.getComponentCount() > objItem; objItem++) {
                        if (mapaTema.getComponent(objItem) instanceof BscMapaObjetivo) {
                            mapaObjetivo = (BscMapaObjetivo) mapaTema.getComponent(objItem);

                            recTemaCont = new bsc.xml.BIXMLRecord("TEMAOBJETIVO");
                            objetivo = new bsc.xml.BIXMLRecord("OBJETIVO");

                            objetivo.set("ID", mapaObjetivo.getId());
                            objetivo.set("NOME", mapaObjetivo.getNome());
                            objetivo.set("DESCRICAO", mapaObjetivo.getDescricao());
                            objetivo.set("MAPX", mapaObjetivo.getLocation().x + mapaTema.getX());
                            objetivo.set("MAPY", mapaObjetivo.getLocation().y + mapaTema.getY());
                            objetivo.set("MAPHEIGHT", mapaObjetivo.getHeight());
                            objetivo.set("MAPWIDTH", mapaObjetivo.getWidth());
                            objetivo.set("MAPCOLOR", mapaObjetivo.jPanelSuperior.getBackground().getRGB());
                            objetivo.set("MAPTYPE", mapaObjetivo.getObjetivoType());
                            objetivos.add(objetivo);

                            //Necessario para a rela��o Tema X Objetivo.
                            recTemaCont.set("OBJETIVOID", mapaObjetivo.getId());
                            vctTemaCont.add(recTemaCont);
                        }
                    }

                    recMapaTema.set(vctTemaCont);
                    vctMapaTemas.add(recMapaTema);
                }
            }

            perspectiva.set(objetivos);
            perspectiva.set(vctMapaTemas);
            perspectivas.add(perspectiva);
        }

        //Ligacoes
        ligacoes = new bsc.xml.BIXMLVector("MAPARELS", "MAPAREL");
        Iterator it = ligHashSet.iterator();
        while (it.hasNext()) {
            BscMapaLigacao tmp = (BscMapaLigacao) it.next();
            ligacao = new bsc.xml.BIXMLRecord("MAPAREL");
            ligacao.set("ID", 0);
            //Tratamento para o codigo diferente do Objetivo e Tema.
            String tmpCode = tmp.getSource();
            tmpCode = tmp.getSource().startsWith("T") ? tmpCode.substring(1, tmpCode.length()) : tmpCode;
            ligacao.set("SRCID", tmpCode);

            tmpCode = tmp.getDestine();
            tmpCode = tmp.getDestine().startsWith("T") ? tmpCode.substring(1, tmpCode.length()) : tmpCode;
            ligacao.set("DESTID", tmpCode);

            ligacao.set("SCRTYPE", tmp.getSource().startsWith("T") ? "T" : " ");
            ligacao.set("DESTYPE", tmp.getDestine().startsWith("T") ? "T" : " ");

            ligacao.set("LINETYPE", tmp.getLigacaoType());
            ligacao.set("LINECTRLX", tmp.ctrl.x);
            ligacao.set("LINECTRLY", tmp.ctrl.y);

            ligacoes.add(ligacao);
        }

        //Propriedades referentes ao Mapa estrat�gico que devem ser gravas.
        vctMapaProp = new bsc.xml.BIXMLVector("MAPAPROPS", "MAPAPROP");
        recMapaProp = new bsc.xml.BIXMLRecord("MAPAPROP");
        recMapaProp.set("DIVISOES", (String) spnDivideMapa.getValue());
        recMapaProp.set("NOMEDIV1", fldNomeTemaVert1.getText());
        recMapaProp.set("NOMEDIV2", fldNomeTemaVert2.getText());
        recMapaProp.set("NOMEDIV3", fldNomeTemaVert3.getText());
        vctMapaProp.add(recMapaProp);

        // Retorno
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set(perspectivas);
        recordAux.set(ligacoes);
        recordAux.set(vctMapaProp);
        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    public boolean hasCombos() {
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTemaPopMenu = new javax.swing.JPopupMenu();
        jMnuTemaColor = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        jMnuTemaTexto = new javax.swing.JMenuItem();
        jMnuTemaSelecionar = new javax.swing.JMenuItem();
        jObjetivoPopMenu = new javax.swing.JPopupMenu();
        jMnuObjetivoColor = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        radCircular = new javax.swing.JRadioButtonMenuItem();
        radRetangulo = new javax.swing.JRadioButtonMenuItem();
        grpObjetivoColor = new javax.swing.ButtonGroup();
        jPerspPopMenu = new javax.swing.JPopupMenu();
        jMnuPerspColor = new javax.swing.JMenuItem();
        jLinePopMenu = new javax.swing.JPopupMenu();
        lineTitle = new javax.swing.JMenuItem();
        Separador1 = new javax.swing.JSeparator();
        lineCurve = new javax.swing.JRadioButtonMenuItem();
        lineStraight = new javax.swing.JRadioButtonMenuItem();
        grpLineMenu = new javax.swing.ButtonGroup();
        grpEditMapa = new javax.swing.ButtonGroup();
        jDrillPopMenu = new javax.swing.JPopupMenu();
        lineTitle1 = new javax.swing.JMenuItem();
        Separador2 = new javax.swing.JSeparator();
        jIndicador = new javax.swing.JMenuItem();
        jObjetivo = new javax.swing.JMenuItem();
        jObjxFcs = new javax.swing.JMenuItem();
        pnlMainPanel = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnMaximize = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        jToolBarMapa = new javax.swing.JToolBar();
        btnSelecionar = new javax.swing.JToggleButton();
        btnDesenharTema = new javax.swing.JToggleButton();
        btnLigar = new javax.swing.JToggleButton();
        jSeparator3 = new javax.swing.JSeparator();
        buttonDelete = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        lblNumDivisoes = new javax.swing.JLabel();
        String[] strDivisoes = {"1","2","3"}; //get month names
        SpinnerListModel spnModel = new SpinnerListModel(strDivisoes);
        spnDivideMapa = new javax.swing.JSpinner(spnModel);
        jPanelDados = new javax.swing.JPanel();
        pnlLegenda = new javax.swing.JPanel();
        lblProprierarioCaixa = new javax.swing.JLabel();
        lblProprierarioTexto = new javax.swing.JLabel();
        jPanelData = new javax.swing.JPanel();
        jPnlDates = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        jPnlTemaFiltro = new javax.swing.JPanel();
        cbbFiltroTema = new javax.swing.JComboBox();
        pnlLayoutDiv = new javax.swing.JPanel();
        pnlNomeDiv = new javax.swing.JPanel();
        fldNomeTemaVert1 = new javax.swing.JTextField();
        fldNomeTemaVert2 = new javax.swing.JTextField();
        fldNomeTemaVert3 = new javax.swing.JTextField();
        jScrollPaneMapa = new javax.swing.JScrollPane();
        jLayeredPaneMapa = new bsc.beans.JBILayeredPane();
        jPanelMapa = new javax.swing.JPanel();
        jLabelCarregando = new javax.swing.JLabel();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        jMnuTemaColor.setText(bundle.getString("BscMapaFrame_00020")); // NOI18N
        jMnuTemaColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuTemaColorActionPerformed(evt);
            }
        });
        jTemaPopMenu.add(jMnuTemaColor);
        jTemaPopMenu.add(jSeparator2);

        jMnuTemaTexto.setText(bundle.getString("BscMapaFrame_00025")); // NOI18N
        jMnuTemaTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuTemaTextoActionPerformed(evt);
            }
        });
        jTemaPopMenu.add(jMnuTemaTexto);

        jMnuTemaSelecionar.setText(bundle.getString("BscMapaFrame_00023")); // NOI18N
        jMnuTemaSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuTemaSelecionarActionPerformed(evt);
            }
        });
        jTemaPopMenu.add(jMnuTemaSelecionar);

        jMnuObjetivoColor.setText(bundle.getString("BscMapaFrame_00020")); // NOI18N
        jMnuObjetivoColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuObjetivoColorActionPerformed(evt);
            }
        });
        jObjetivoPopMenu.add(jMnuObjetivoColor);
        jObjetivoPopMenu.add(jSeparator1);

        grpObjetivoColor.add(radCircular);
        radCircular.setSelected(true);
        radCircular.setText(bundle.getString("BscMapaFrame_00021")); // NOI18N
        radCircular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radCircularActionPerformed(evt);
            }
        });
        jObjetivoPopMenu.add(radCircular);

        grpObjetivoColor.add(radRetangulo);
        radRetangulo.setText(bundle.getString("BscMapaFrame_00022")); // NOI18N
        radRetangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radRetanguloActionPerformed(evt);
            }
        });
        jObjetivoPopMenu.add(radRetangulo);

        jMnuPerspColor.setText(bundle.getString("BscMapaFrame_00020")); // NOI18N
        jMnuPerspColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuPerspColorActionPerformed(evt);
            }
        });
        jPerspPopMenu.add(jMnuPerspColor);

        lineTitle.setText(bundle.getString("BscMapaFrame_00029")); // NOI18N
        lineTitle.setFocusPainted(true);
        lineTitle.setRequestFocusEnabled(false);
        jLinePopMenu.add(lineTitle);
        jLinePopMenu.add(Separador1);

        grpLineMenu.add(lineCurve);
        lineCurve.setSelected(true);
        lineCurve.setText(bundle.getString("BscMapaFrame_00030")); // NOI18N
        lineCurve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineCurveActionPerformed(evt);
            }
        });
        jLinePopMenu.add(lineCurve);

        grpLineMenu.add(lineStraight);
        lineStraight.setText(bundle.getString("BscMapaFrame_00031")); // NOI18N
        lineStraight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineStraightActionPerformed(evt);
            }
        });
        jLinePopMenu.add(lineStraight);

        lineTitle1.setText(bundle.getString("BscMapaFrame_00035")); // NOI18N
        lineTitle1.setFocusPainted(true);
        lineTitle1.setRequestFocusEnabled(false);
        jDrillPopMenu.add(lineTitle1);
        jDrillPopMenu.add(Separador2);

        jIndicador.setText(bundle.getString("BscMapaFrame_00036")); // NOI18N
        jIndicador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jIndicadorActionPerformed(evt);
            }
        });
        jDrillPopMenu.add(jIndicador);

        jObjetivo.setText(bundle.getString("BscMapaFrame_00037")); // NOI18N
        jObjetivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jObjetivoActionPerformed(evt);
            }
        });
        jDrillPopMenu.add(jObjetivo);

        jObjxFcs.setText("Objetivo x FCS");
        jObjxFcs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jObjxFcsActionPerformed(evt);
            }
        });
        jDrillPopMenu.add(jObjxFcs);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(bundle.getString("BscMapaFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlMainPanel.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 100));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 30));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(130, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscMapaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscMapaFrame_00008")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(500, 30));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(500, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscMapaFrame_00006")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscMapaFrame_00007")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("BscMapaFrame_00009")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("BscMapaFrame_00026")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_maximizar.gif"))); // NOI18N
        btnMaximize.setText(bundle.getString("BscGraphFrame_00034")); // NOI18N
        btnMaximize.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnMaximize.setMaximumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setMinimumSize(new java.awt.Dimension(76, 20));
        btnMaximize.setPreferredSize(new java.awt.Dimension(76, 20));
        btnMaximize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaximizeActionPerformed(evt);
            }
        });
        tbInsertion.add(btnMaximize);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        jToolBarMapa.setFloatable(false);
        jToolBarMapa.setMaximumSize(new java.awt.Dimension(300, 200));
        jToolBarMapa.setPreferredSize(new java.awt.Dimension(200, 70));

        grpEditMapa.add(btnSelecionar);
        btnSelecionar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSelecionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_setaselecionar.gif"))); // NOI18N
        btnSelecionar.setMnemonic('s');
        btnSelecionar.setToolTipText(bundle.getString("BscMapaFrame_00032")); // NOI18N
        btnSelecionar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSelecionar.setMaximumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setMinimumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setPreferredSize(new java.awt.Dimension(40, 20));
        btnSelecionar.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                btnSelecionarStateChanged(evt);
            }
        });
        btnSelecionar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnSelecionarItemStateChanged(evt);
            }
        });
        jToolBarMapa.add(btnSelecionar);

        grpEditMapa.add(btnDesenharTema);
        btnDesenharTema.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnDesenharTema.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_desenhartema.gif"))); // NOI18N
        btnDesenharTema.setMnemonic('d');
        btnDesenharTema.setToolTipText(bundle.getString("BscMapaFrame_00033")); // NOI18N
        btnDesenharTema.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnDesenharTema.setMaximumSize(new java.awt.Dimension(40, 20));
        btnDesenharTema.setMinimumSize(new java.awt.Dimension(40, 20));
        btnDesenharTema.setPreferredSize(new java.awt.Dimension(40, 20));
        jToolBarMapa.add(btnDesenharTema);

        grpEditMapa.add(btnLigar);
        btnLigar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnLigar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ligacoes.gif"))); // NOI18N
        btnLigar.setMnemonic('l');
        btnLigar.setToolTipText(bundle.getString("BscMapaFrame_00034")); // NOI18N
        btnLigar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnLigar.setMaximumSize(new java.awt.Dimension(40, 20));
        btnLigar.setMinimumSize(new java.awt.Dimension(40, 20));
        btnLigar.setPreferredSize(new java.awt.Dimension(40, 20));
        jToolBarMapa.add(btnLigar);

        jSeparator3.setMaximumSize(new java.awt.Dimension(10, 26));
        jToolBarMapa.add(jSeparator3);

        buttonDelete.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_x_cancelar.gif"))); // NOI18N
        buttonDelete.setMnemonic('e');
        buttonDelete.setToolTipText(bundle.getString("BscMapaFrame_00010")); // NOI18N
        buttonDelete.setActionCommand(bundle.getString("BscMapaFrame_00012")); // NOI18N
        buttonDelete.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        buttonDelete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        buttonDelete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        buttonDelete.setMargin(new java.awt.Insets(2, 4, 2, 4));
        buttonDelete.setMaximumSize(new java.awt.Dimension(40, 20));
        buttonDelete.setMinimumSize(new java.awt.Dimension(40, 20));
        buttonDelete.setPreferredSize(new java.awt.Dimension(40, 20));
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });
        jToolBarMapa.add(buttonDelete);

        jSeparator4.setMaximumSize(new java.awt.Dimension(10, 26));
        jToolBarMapa.add(jSeparator4);

        lblNumDivisoes.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblNumDivisoes.setText(bundle.getString("BscMapaFrame_00017")); // NOI18N
        jToolBarMapa.add(lblNumDivisoes);

        spnDivideMapa.setToolTipText("");
        spnDivideMapa.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        spnDivideMapa.setMaximumSize(new java.awt.Dimension(41, 26));
        spnDivideMapa.setMinimumSize(new java.awt.Dimension(21, 0));
        spnDivideMapa.setPreferredSize(new java.awt.Dimension(51, 15));
        spnDivideMapa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnDivideMapaStateChanged(evt);
            }
        });
        jToolBarMapa.add(spnDivideMapa);

        pnlTools.add(jToolBarMapa, java.awt.BorderLayout.CENTER);

        jPanelDados.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelDados.setPreferredSize(new java.awt.Dimension(10, 70));
        jPanelDados.setLayout(new java.awt.BorderLayout());

        pnlLegenda.setPreferredSize(new java.awt.Dimension(12, 12));
        pnlLegenda.setLayout(new java.awt.GridBagLayout());

        lblProprierarioCaixa.setBackground(new java.awt.Color(255, 255, 255));
        lblProprierarioCaixa.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lblProprierarioCaixa.setOpaque(true);
        lblProprierarioCaixa.setPreferredSize(new java.awt.Dimension(10, 10));
        pnlLegenda.add(lblProprierarioCaixa, new java.awt.GridBagConstraints());

        lblProprierarioTexto.setText(bundle.getString("BscMapaFrame_00013")); // NOI18N
        lblProprierarioTexto.setMaximumSize(new java.awt.Dimension(200, 16));
        lblProprierarioTexto.setMinimumSize(new java.awt.Dimension(200, 16));
        lblProprierarioTexto.setPreferredSize(new java.awt.Dimension(180, 16));
        pnlLegenda.add(lblProprierarioTexto, new java.awt.GridBagConstraints());

        jPanelDados.add(pnlLegenda, java.awt.BorderLayout.CENTER);

        jPanelData.setLayout(new java.awt.BorderLayout());

        jPnlDates.setMinimumSize(new java.awt.Dimension(250, 33));
        jPnlDates.setPreferredSize(new java.awt.Dimension(400, 30));

        lblDataAlvo.setText(bundle.getString("BscMapaFrame_00027")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(90, 15));
        lblDataAlvo.setRequestFocusEnabled(false);
        jPnlDates.add(lblDataAlvo);

        fldDataAlvo.setBounds(new java.awt.Rectangle(0, 0, 0, 22));
        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        jPnlDates.add(fldDataAlvo);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setPreferredSize(new java.awt.Dimension(25, 22));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAvaliacao);

        fldAnalise.setBounds(new java.awt.Rectangle(0, 0, 0, 22));
        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        jPnlDates.add(fldAnalise);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setPreferredSize(new java.awt.Dimension(25, 22));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAnalise);

        jPanelData.add(jPnlDates, java.awt.BorderLayout.WEST);

        jPnlTemaFiltro.setOpaque(false);
        jPnlTemaFiltro.setLayout(new javax.swing.BoxLayout(jPnlTemaFiltro, javax.swing.BoxLayout.LINE_AXIS));

        cbbFiltroTema.setMaximumSize(new java.awt.Dimension(32767, 20));
        cbbFiltroTema.setMinimumSize(new java.awt.Dimension(26, 16));
        cbbFiltroTema.setPreferredSize(new java.awt.Dimension(28, 22));
        cbbFiltroTema.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbbFiltroTemaItemStateChanged(evt);
            }
        });
        jPnlTemaFiltro.add(cbbFiltroTema);

        jPanelData.add(jPnlTemaFiltro, java.awt.BorderLayout.CENTER);

        jPanelDados.add(jPanelData, java.awt.BorderLayout.NORTH);

        pnlLayoutDiv.setMaximumSize(new java.awt.Dimension(952, 2147483647));
        pnlLayoutDiv.setOpaque(false);
        pnlLayoutDiv.setPreferredSize(new java.awt.Dimension(950, 20));
        pnlLayoutDiv.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        pnlNomeDiv.setBackground(new java.awt.Color(189, 186, 189));
        pnlNomeDiv.setToolTipText("");
        pnlNomeDiv.setMaximumSize(new java.awt.Dimension(952, 500));
        pnlNomeDiv.setPreferredSize(new java.awt.Dimension(950, 20));
        pnlNomeDiv.setRequestFocusEnabled(false);
        pnlNomeDiv.setLayout(new java.awt.GridLayout(1, 3, 0, -10));

        fldNomeTemaVert1.setBackground(new java.awt.Color(225, 225, 252));
        fldNomeTemaVert1.setFont(new java.awt.Font("MS Sans Serif", 1, 11)); // NOI18N
        fldNomeTemaVert1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeTemaVert1.setToolTipText("");
        fldNomeTemaVert1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fldNomeTemaVert1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeTemaVert1.setEnabled(false);
        fldNomeTemaVert1.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeTemaVert1.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeTemaVert1);

        fldNomeTemaVert2.setBackground(new java.awt.Color(225, 225, 252));
        fldNomeTemaVert2.setFont(new java.awt.Font("MS Sans Serif", 1, 11)); // NOI18N
        fldNomeTemaVert2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeTemaVert2.setToolTipText("");
        fldNomeTemaVert2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fldNomeTemaVert2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeTemaVert2.setEnabled(false);
        fldNomeTemaVert2.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeTemaVert2.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeTemaVert2);

        fldNomeTemaVert3.setBackground(new java.awt.Color(225, 225, 252));
        fldNomeTemaVert3.setFont(new java.awt.Font("MS Sans Serif", 1, 11)); // NOI18N
        fldNomeTemaVert3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        fldNomeTemaVert3.setToolTipText("");
        fldNomeTemaVert3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fldNomeTemaVert3.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        fldNomeTemaVert3.setEnabled(false);
        fldNomeTemaVert3.setMaximumSize(new java.awt.Dimension(0, 0));
        fldNomeTemaVert3.setPreferredSize(new java.awt.Dimension(160, 20));
        pnlNomeDiv.add(fldNomeTemaVert3);

        pnlLayoutDiv.add(pnlNomeDiv);

        jPanelDados.add(pnlLayoutDiv, java.awt.BorderLayout.SOUTH);

        pnlTools.add(jPanelDados, java.awt.BorderLayout.SOUTH);

        pnlMainPanel.add(pnlTools, java.awt.BorderLayout.NORTH);

        jScrollPaneMapa.setFocusCycleRoot(true);

        jLayeredPaneMapa.setPreferredSize(new java.awt.Dimension(1000, 1000));

        jPanelMapa.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanelMapa.setOpaque(false);
        jPanelMapa.setPreferredSize(new java.awt.Dimension(1000, 1000));
        jPanelMapa.setRequestFocusEnabled(false);
        jPanelMapa.setVerifyInputWhenFocusTarget(false);
        jPanelMapa.setLayout(new javax.swing.BoxLayout(jPanelMapa, javax.swing.BoxLayout.Y_AXIS));

        jLabelCarregando.setText(bundle.getString("BscMapaFrame_00011")); // NOI18N
        jPanelMapa.add(jLabelCarregando);

        jPanelMapa.setBounds(0, 0, 170, 20);
        jLayeredPaneMapa.add(jPanelMapa, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jScrollPaneMapa.setViewportView(jLayeredPaneMapa);

        pnlMainPanel.add(jScrollPaneMapa, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlMainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMaximizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaximizeActionPerformed
        if (!isMaximize) {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00035"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_minimizar.gif")));
            isMaximize = true;
            maximizeForm = new BscMapaMaximize(bsc.core.BscStaticReferences.getBscMainPanel(), true, pnlMainPanel);
            maximizeForm.setMapaFrame(this);
            maximizeForm.setVisible(true);
        } else {
            btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00034"));
            btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_maximizar.gif")));
            isMaximize = false;
            maximizeForm.setVisible(false);
            maximizeForm.dispose();
            this.getContentPane().add(maximizeForm.pnlMapa, java.awt.BorderLayout.CENTER);
            this.doLayout();
        }
    }//GEN-LAST:event_btnMaximizeActionPerformed

	private void jObjxFcsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jObjxFcsActionPerformed
            bsc.swing.BscDefaultFrameFunctions frame = null;
            frame = bsc.core.BscStaticReferences.getBscFormController().getForm("MAPAOBJ", "" + dragSource.getId(), dragSource.getNome());
	}//GEN-LAST:event_jObjxFcsActionPerformed

	private void jIndicadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jIndicadorActionPerformed
            bsc.swing.BscDefaultFrameFunctions frame = null;
            frame = bsc.core.BscStaticReferences.getBscFormController().getForm("DRILL", "" + dragSource.getId(), dragSource.getNome());
	}//GEN-LAST:event_jIndicadorActionPerformed

	private void jObjetivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jObjetivoActionPerformed
            bsc.swing.BscDefaultFrameFunctions frame = null;
            frame = bsc.core.BscStaticReferences.getBscFormController().getForm("DRILLOBJ", "" + dragSource.getId(), dragSource.getNome());
	}//GEN-LAST:event_jObjetivoActionPerformed

	private void btnSelecionarStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_btnSelecionarStateChanged
            jPanelVidro.repaint();
	}//GEN-LAST:event_btnSelecionarStateChanged

	private void lineStraightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineStraightActionPerformed
            if (ligacaoPopUp.getLigacaoType() == BscMapaLigacao.QUADCURVE) {
                ligacaoPopUp.setLigacaoType(BscMapaLigacao.LINE);
                jPanelVidro.repaint();
            }
	}//GEN-LAST:event_lineStraightActionPerformed

	private void lineCurveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineCurveActionPerformed
            if (ligacaoPopUp.getLigacaoType() == BscMapaLigacao.LINE) {
                ligacaoPopUp.setLigacaoType(BscMapaLigacao.QUADCURVE);
                jPanelVidro.repaint();
            }
	}//GEN-LAST:event_lineCurveActionPerformed

	private void cbbFiltroTemaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbbFiltroTemaItemStateChanged
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                refreshFields();
            }
	}//GEN-LAST:event_cbbFiltroTemaItemStateChanged

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        bsc.swing.BscDefaultDialogSystem dialogSystem;
        java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
        bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();
        int iVlrDiv = 0;
        boolean maxState = isMaximum();
        try {
            this.setMaximum(true);
        } catch (Exception e) {
        }

        if (mainPanel.bscTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
            iVlrDiv = mainPanel.jSplitPaneAreaTrabalho.getDividerLocation();
            mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(0);
        } else {
            iVlrDiv = mainPanel.jSplitPaneOrganizacao.getDividerLocation();
            mainPanel.jSplitPaneOrganizacao.setDividerLocation(0);
        }

        if (isMaximize && maximizeForm != null) {
            dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
        } else {
            dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
        }

        //Mensagem necess�ria para fazer o "split" do painel.
        if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00019")) ==
                javax.swing.JOptionPane.YES_OPTION) {
            bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(), true);
            frmChooser.setDialogType(BscFileChooser.BSC_DIALOG_SAVE);
            frmChooser.setFileType("*.jpg");
            frmChooser.loadServerFiles("mapest\\*.jpg");
            frmChooser.setFileName("mapa" + record.getString("ID") + ".jpg");
            frmChooser.setVisible(true);
            if (frmChooser.isValidFile()) {
                saveComponentAsJPEG("mapest\\" + frmChooser.getFileName());
            }
        }

        if (mainPanel.bscTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
            mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(iVlrDiv);
        } else {
            mainPanel.jSplitPaneOrganizacao.setDividerLocation(iVlrDiv);
        }

        try {
            this.setMaximum(maxState);
        } catch (Exception e) {
        }

    }//GEN-LAST:event_btnExportarActionPerformed

    private void jMnuTemaSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuTemaSelecionarActionPerformed
        if (temaPopUp.temaSelected == false) {
            temaPopUp.temaSelected = true;
        } else {
            temaPopUp.temaSelected = false;
        }
        temaPopUp.repaint();
    }//GEN-LAST:event_jMnuTemaSelecionarActionPerformed

    private void jMnuTemaTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuTemaTextoActionPerformed
        bsc.swing.BscDefaultDialogSystem dialogSystem;
        //Altera o texto do tema.
        javax.swing.border.TitledBorder bordaTema = (javax.swing.border.TitledBorder) temaPopUp.getBorder();
        Rectangle rBorda = getBorderBounds(temaPopUp);
        String strBorda;
        if (isMaximize && maximizeForm != null) {
            dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
        } else {
            dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
        }

        strBorda = dialogSystem.inputDialog(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00015"), bordaTema.getTitle());
        if (strBorda != null) {
            bordaTema.setTitle(strBorda);
            temaPopUp.repaint();
        }
    }//GEN-LAST:event_jMnuTemaTextoActionPerformed

    private void jMnuPerspColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuPerspColorActionPerformed
        Color c;
        if (isMaximize && maximizeForm != null) {
            c = JColorChooser.showDialog(maximizeForm, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        } else {
            c = JColorChooser.showDialog(this, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        }

        try {
            mapaPerspectivaPopUp.setBackground(new java.awt.Color(c.getRGB()));
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jMnuPerspColorActionPerformed

    private void radRetanguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radRetanguloActionPerformed
        if (objetivoPopUp.getObjetivoType() == BscMapaObjetivo.OBJETIVO_OVAL) {
            objetivoPopUp.setObjetivoType(BscMapaObjetivo.OBJETIVO_RECTANGLE);
        }
    }//GEN-LAST:event_radRetanguloActionPerformed

    private void radCircularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radCircularActionPerformed
        if (objetivoPopUp.getObjetivoType() == BscMapaObjetivo.OBJETIVO_RECTANGLE) {
            objetivoPopUp.setObjetivoType(BscMapaObjetivo.OBJETIVO_OVAL);
        }
    }//GEN-LAST:event_radCircularActionPerformed

    private void jMnuObjetivoColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuObjetivoColorActionPerformed
        Color c;
        if (isMaximize && maximizeForm != null) {
            c = JColorChooser.showDialog(maximizeForm, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        } else {
            c = JColorChooser.showDialog(this, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        }

        try {
            objetivoPopUp.jPanelSuperior.setBackground(new java.awt.Color(c.getRGB()));
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jMnuObjetivoColorActionPerformed

    private void jMnuTemaColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuTemaColorActionPerformed
        Color c;
        if (isMaximize && maximizeForm != null) {
            c = JColorChooser.showDialog(maximizeForm, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        } else {
            c = JColorChooser.showDialog(this, java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), mapaPerspectivaPopUp.getBackground());
        }
        try {
            temaPopUp.setBackground(new java.awt.Color(c.getRGB()));
        } catch (Exception e) {
        }

    }//GEN-LAST:event_jMnuTemaColorActionPerformed

	private void btnSelecionarItemStateChanged (java.awt.event.ItemEvent evt)//GEN-FIRST:event_btnSelecionarItemStateChanged
	{//GEN-HEADEREND:event_btnSelecionarItemStateChanged
            javax.swing.JToggleButton btnMouse = (javax.swing.JToggleButton) evt.getItem();
	}//GEN-LAST:event_btnSelecionarItemStateChanged

	private void spnDivideMapaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnDivideMapaStateChanged
            jPanelVidro.repaint();
            int iDivisao = Integer.parseInt((String) spnDivideMapa.getValue());

            pnlNomeDiv.removeAll();

            switch (iDivisao) {
                case 1:
                    pnlNomeDiv.add(fldNomeTemaVert1);
                    fldNomeTemaVert1.setVisible(true);
                    fldNomeTemaVert2.setVisible(false);
                    fldNomeTemaVert3.setVisible(false);
                    fldNomeTemaVert1.requestFocus();
                    break;
                case 2:
                    pnlNomeDiv.add(fldNomeTemaVert1);
                    pnlNomeDiv.add(fldNomeTemaVert2);
                    pnlNomeDiv.setVisible(true);
                    fldNomeTemaVert1.setVisible(true);
                    fldNomeTemaVert2.setVisible(true);
                    fldNomeTemaVert3.setVisible(false);
                    fldNomeTemaVert2.requestFocus();
                    break;
                case 3:
                    pnlNomeDiv.add(fldNomeTemaVert1);
                    pnlNomeDiv.add(fldNomeTemaVert2);
                    pnlNomeDiv.add(fldNomeTemaVert3);
                    pnlNomeDiv.setVisible(true);
                    fldNomeTemaVert1.setVisible(true);
                    fldNomeTemaVert2.setVisible(true);
                    fldNomeTemaVert3.setVisible(true);
                    fldNomeTemaVert3.requestFocus();
                    break;
                default:
                    pnlNomeDiv.setVisible(false);
                    fldNomeTemaVert1.setVisible(false);
                    fldNomeTemaVert2.setVisible(false);
                    fldNomeTemaVert3.setVisible(false);
                    break;

            }

            pnlNomeDiv.validate();

	}//GEN-LAST:event_spnDivideMapaStateChanged

	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
            bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
                    new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
            bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
            bscPerformanceCard.setVisible(true);
	}//GEN-LAST:event_btnAvaliacaoActionPerformed

    private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
        bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
                new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
        bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
        bscAnaliseParceladaCard.setVisible(true);
    }//GEN-LAST:event_btnAnaliseActionPerformed

	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
            bsc.swing.BscDefaultDialogSystem dialogSystem;
            java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
            bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();
            int iVlrDiv = 0;
            boolean maxState = isMaximum();
            try {
                this.setMaximum(true);
            } catch (Exception e) {
            }

            if (mainPanel.bscTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
                iVlrDiv = mainPanel.jSplitPaneAreaTrabalho.getDividerLocation();
                mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(0);
            } else {
                iVlrDiv = mainPanel.jSplitPaneOrganizacao.getDividerLocation();
                mainPanel.jSplitPaneOrganizacao.setDividerLocation(0);
            }

            if (isMaximize && maximizeForm != null) {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
            } else {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
            }

            //Mensagem necess�ria para fazer o "split" do painel.
            if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00019")) ==
                    javax.swing.JOptionPane.YES_OPTION) {

                printJob.setPrintable(this);
                if (printJob.printDialog()) {
                    try {
                        printJob.print();
                    } catch (java.awt.print.PrinterException pe) {
                        System.out.println("Error printing: " + pe);
                    }
                }
            }

            if (mainPanel.bscTree.getBscTreeType().equals(BscMainPanel.ST_AREATRABALHO)) {
                mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(iVlrDiv);
            } else {
                mainPanel.jSplitPaneOrganizacao.setDividerLocation(iVlrDiv);
            }

            try {
                this.setMaximum(maxState);
            } catch (Exception e) {
            }

	}//GEN-LAST:event_btnPrintActionPerformed

	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
            event.loadHelp("FERRAMENTA");
	}//GEN-LAST:event_btnReload1ActionPerformed

	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
            event.loadRecord();
	}//GEN-LAST:event_btnReloadActionPerformed

	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
            event.saveRecord();
            lUpdateRecord = true;
	}//GEN-LAST:event_btnSaveActionPerformed

	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
            if (lUpdateRecord) {
                loadRecord();
                lUpdateRecord = false;
            }
            event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed

	private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
            //event.deleteRecord();
            bsc.swing.BscDefaultDialogSystem dialogSystem;

            if (isMaximize && maximizeForm != null) {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
            } else {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
            }

            if (dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00003")) == JOptionPane.YES_OPTION) {

                Iterator it = ligHashSet.iterator();
                while (it.hasNext()) {
                    BscMapaLigacao ligacao = (BscMapaLigacao) it.next();
                    if (ligacao.isSelected()) {
                        it.remove();
                    }
                }

                jPanelVidro.repaint();
                removeThemaFromPerspective();
            }
	}//GEN-LAST:event_buttonDeleteActionPerformed

	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
            event.editRecord();
            btnSelecionar.setSelected(true);
	}//GEN-LAST:event_btnEditActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSeparator Separador1;
    private javax.swing.JSeparator Separador2;
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnCancel;
    private javax.swing.JToggleButton btnDesenharTema;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JToggleButton btnLigar;
    public javax.swing.JButton btnMaximize;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JToggleButton btnSelecionar;
    private javax.swing.JButton buttonDelete;
    private javax.swing.JComboBox cbbFiltroTema;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private javax.swing.JTextField fldNomeTemaVert1;
    private javax.swing.JTextField fldNomeTemaVert2;
    private javax.swing.JTextField fldNomeTemaVert3;
    private javax.swing.ButtonGroup grpEditMapa;
    private javax.swing.ButtonGroup grpLineMenu;
    private javax.swing.ButtonGroup grpObjetivoColor;
    private javax.swing.JPopupMenu jDrillPopMenu;
    private javax.swing.JMenuItem jIndicador;
    private javax.swing.JLabel jLabelCarregando;
    public bsc.beans.JBILayeredPane jLayeredPaneMapa;
    private javax.swing.JPopupMenu jLinePopMenu;
    private javax.swing.JMenuItem jMnuObjetivoColor;
    private javax.swing.JMenuItem jMnuPerspColor;
    private javax.swing.JMenuItem jMnuTemaColor;
    private javax.swing.JMenuItem jMnuTemaSelecionar;
    private javax.swing.JMenuItem jMnuTemaTexto;
    private javax.swing.JMenuItem jObjetivo;
    private javax.swing.JPopupMenu jObjetivoPopMenu;
    private javax.swing.JMenuItem jObjxFcs;
    public static javax.swing.JPanel jPanelDados;
    private javax.swing.JPanel jPanelData;
    private javax.swing.JPanel jPanelMapa;
    private javax.swing.JPopupMenu jPerspPopMenu;
    private javax.swing.JPanel jPnlDates;
    private javax.swing.JPanel jPnlTemaFiltro;
    private javax.swing.JScrollPane jScrollPaneMapa;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JPopupMenu jTemaPopMenu;
    private javax.swing.JToolBar jToolBarMapa;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblNumDivisoes;
    private javax.swing.JLabel lblProprierarioCaixa;
    private javax.swing.JLabel lblProprierarioTexto;
    private javax.swing.JRadioButtonMenuItem lineCurve;
    private javax.swing.JRadioButtonMenuItem lineStraight;
    private javax.swing.JMenuItem lineTitle;
    private javax.swing.JMenuItem lineTitle1;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlLayoutDiv;
    private javax.swing.JPanel pnlLegenda;
    public javax.swing.JPanel pnlMainPanel;
    private javax.swing.JPanel pnlNomeDiv;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JRadioButtonMenuItem radCircular;
    private javax.swing.JRadioButtonMenuItem radRetangulo;
    private javax.swing.JSpinner spnDivideMapa;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscMapaFrame(int operation, String idAux, String contextId) {
        initComponents();

        pnlNomeDiv.removeAll();
        pnlNomeDiv.add(fldNomeTemaVert1);
        pnlNomeDiv.setVisible(true);
        fldNomeTemaVert1.setVisible(true);
        fldNomeTemaVert2.setVisible(false);
        fldNomeTemaVert3.setVisible(false);

        //pnlLegenda.setVisible(false);

        javax.swing.JScrollBar mapaEstHortBar = jScrollPaneMapa.getHorizontalScrollBar();

        mapaEstHortBar.setUnitIncrement(10);
        mapaEstHortBar.getModel().addChangeListener(this);


        // c�digo init separado
        jPanelVidro = new JPanel() {

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);

                // Inicia graphics 2d
                Graphics2D g2 = (Graphics2D) g;
                java.awt.Composite mapaComposite = g2.getComposite();
                divideMapa(g2);
                g2.setComposite(mapaComposite);
                g2.setColor(arrowColor);
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, (editEnabled) ? RenderingHints.VALUE_ANTIALIAS_OFF : RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));

                BscMapaJPanel origem, destino;
                Iterator it = ligHashSet.iterator();
                HashMap hashAdded = new HashMap();
                while (it.hasNext()) {
                    BscMapaLigacao ligacao = (BscMapaLigacao) it.next();
                    boolean oriTemaInside = false;
                    boolean desTemaInside = false;
                    boolean itemAdded = false;

                    origem = (BscMapaJPanel) idHashMap.get(new String(ligacao.getSource()));
                    destino = (BscMapaJPanel) idHashMap.get(new String(ligacao.getDestine()));

                    String strChave = new String((String) ligacao.getSource().concat((String) ligacao.getDestine()));

                    if (hashAdded.containsKey(strChave)) {
                        itemAdded = true;
                    } else {
                        hashAdded.put(new String(ligacao.getSource()).concat((String) ligacao.getDestine()), null);
                    }

                    // Consistencia
                    if (origem == null || destino == null || itemAdded) {
                        it.remove();
                        continue;
                    }

                    // Achar ponto 1
                    Rectangle r1 = origem.getBounds();
                    Point p1 = origem.getLocation();
                    Point p11 = origem.getParent().getLocation();
                    Point p12 = origem.getParent().getParent().getLocation();
                    Point p13 = origem.getParent().getParent().getParent().getParent().getLocation();
                    p13.y = (p13.y < 0 ? 0 : p13.y);
                    p13.x = (p13.x < 0 ? 0 : p13.x);

                    p1.translate(p11.x, p11.y);
                    p1.translate(p12.x, p12.y);
                    p1.translate(p13.x, p13.y);

                    // Achar ponto 2
                    Rectangle r2 = destino.getBounds();
                    Point p2 = destino.getLocation();
                    Point p21 = destino.getParent().getLocation();
                    Point p22 = destino.getParent().getParent().getLocation();
                    Point p23 = destino.getParent().getParent().getParent().getLocation();
                    p23.y = (p23.y < 0 ? 0 : p23.y);
                    p23.x = (p23.x < 0 ? 0 : p23.x);

                    p2.translate(p21.x, p21.y);
                    p2.translate(p22.x, p22.y);
                    p2.translate(p23.x, p23.y);

                    // Achar posicao 1
                    int position;

                    //Corre��o para a impress�o do tema;
                    p1.y += origem.getParent().getParent().getParent().getLocation().y;
                    p1.x += origem.getParent().getParent().getParent().getLocation().x;

                    if (p1.y > p2.y - r2.height && p1.y < p2.y + r2.height) {
                        if (p1.x + (r1.width / 1) > p2.x + (r2.width / 2)) {
                            position = DIREITA;
                        } else {
                            position = ESQUERDA;
                        }
                    } else {
                        if (p1.y + (r1.height / 2) > p2.y + (r2.height / 2)) {
                            position = ABAIXO;
                        } else {
                            position = ACIMA;
                        }
                    }

                    // Posicao
                    if (position == ACIMA) {
                        p1.y = p1.y + r1.height;

                        p1.x = p1.x + r1.width / 2;
                        p2.x = p2.x + r2.width / 2;

                    } else if (position == ABAIXO) {
                        p2.y = p2.y + r2.height;

                        p1.x = p1.x + r1.width / 2;
                        p2.x = p2.x + r2.width / 2;

                    } else if (position == ESQUERDA) {
                        p1.x = p1.x + r1.width;

                        p1.y = p1.y + r1.height / 2;
                        p2.y = p2.y + r2.height / 2;
                    } else if (position == DIREITA) {
                        p2.x = p2.x + r2.width;

                        p1.y = p1.y + r1.height / 2;
                        p2.y = p2.y + r2.height / 2;
                    }

                    // Desenhar
                    Color c = (ligacao.isSelected() ? Color.yellow : arrowColor);
                    g2.setColor(c);
                    g2.setPaint(c);

                    if (ligacao.getArrow() == null) {
                        ligacao.setArrow(new bsc.util.BscArrow2D(p1, p2));
                    }

                    //Refaz o tipo da seta; pois na inicializa��o n�o existe o objeto "arrow"
                    ligacao.setLigacaoType(ligacao.getLigacaoType());
                    ligacao.getArrow().setLine(p1, p2, ligacao.ctrl);

                    if (ligacao.getLigacaoType() == BscMapaLigacao.LINE) {
                        ligacao.getArrow().render(g2);
                    } else {
                        ligacao.getArrow().render(g2, ligacao.ctrl, btnSelecionar.isSelected() && editEnabled, ligacao.isSelected());
                    }
                }

                // Linha virtual
                if (virtualLine != null) {
                    g2.setColor(Color.red);
                    virtualLine.render(g2);
                    g2.setColor(arrowColor);
                }
            }
        };

        jPanelVidro.setLayout(null);
        jPanelVidro.setOpaque(false);
        jLayeredPaneMapa.add(jPanelVidro, javax.swing.JLayeredPane.MODAL_LAYER);

        event.defaultConstructor(operation, idAux, contextId);

    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
        //carregar os dados da drop-down
        if (record.contains("CBBTEMASFILTRO")) {
            vctTemaEstrategico = record.getBIXMLVector("CBBTEMASFILTRO");
            oBscToolKit.populateCombo(vctTemaEstrategico, "ID", hstTemasFiltro, cbbFiltroTema, null, null);
            cbbFiltroTema.removeItemAt(0);
        }
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        pnlConfirmation.setVisible(true);
        pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        pnlConfirmation.setVisible(false);
        pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return null;
    }

    public void mouseClicked(java.awt.event.MouseEvent mouseEvent) {

        if (this.editEnabled) {
            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mouseEvent.getSource();
                ligacaoPopUp = getMapaLigacao(mouseEvent);
                if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                    if (ligacaoPopUp == null) {
                        mapaPerspectivaPopUp = mapaPerspectiva;
                        jPerspPopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    } else {
                        setLinePopMenu();
                        jLinePopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    }
                } else {
                    //Mouse dragged
                    if (ligacaoPopUp != null && btnSelecionar.isSelected()) {
                        ligacaoPopUp.setSelected(!ligacaoPopUp.isSelected());
                        jPanelVidro.repaint();
                    }
                    ligacaoPopUp = null;
                }
                //No clique edita o nome do tema.
            } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                javax.swing.border.TitledBorder bordaTema = (javax.swing.border.TitledBorder) mapaTema.getBorder();

                if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                    ligacaoPopUp = getMapaLigacao(mouseEvent);
                    if (ligacaoPopUp == null) {
                        temaPopUp = mapaTema;
                        if (temaPopUp.temaSelected == true) {
                            jMnuTemaSelecionar.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00024"));
                        } else {
                            jMnuTemaSelecionar.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00023"));
                        }
                        jTemaPopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    } else {
                        setLinePopMenu();
                        jLinePopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    }

                } else if (btnDesenharTema.isSelected() && mouseEvent.getClickCount() == 2) {
                    Rectangle rBorda = getBorderBounds(mapaTema);
                    //Verifica se foi clicado no desenho;
                    if (rBorda.contains(mouseEvent.getX(), mouseEvent.getY())) {
                        String strBorda;
                        bsc.swing.BscDefaultDialogSystem dialogSystem;
                        if (isMaximize && maximizeForm != null) {
                            dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
                        } else {
                            dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
                        }

                        strBorda = dialogSystem.inputDialog(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00015"), bordaTema.getTitle());
                        if (strBorda != null) {
                            bordaTema.setTitle(strBorda);
                            mapaTema.repaint();
                        }
                    }
                } else if (mouseEvent.getClickCount() == 1 && btnSelecionar.isSelected()) {
                    BscMapaLigacao ligacao = getMapaLigacao(mouseEvent);
                    if (ligacao == null) {
                        mapaTema.setTemaSelected();
                    } else {
                        ligacao.setSelected(!ligacao.isSelected());
                        jPanelVidro.repaint();
                    }
                }
            } else if (mouseEvent.getSource() instanceof JTextPane) {
                JTextPane jText = (JTextPane) mouseEvent.getSource();
                BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                    ligacaoPopUp = getMapaLigacao(mouseEvent);
                    if (ligacaoPopUp == null) {
                        objetivoPopUp = mapaObjetivo;
                        //Verifica como est� a vizualia��o antes de chamar o menu.
                        if (objetivoPopUp.getObjetivoType() == BscMapaObjetivo.OBJETIVO_OVAL) {
                            radCircular.setSelected(true);
                            radRetangulo.setSelected(false);
                        } else {
                            radRetangulo.setSelected(true);
                            radCircular.setSelected(false);
                        }
                        jObjetivoPopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    } else {
                        setLinePopMenu();
                        jLinePopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    }
                }
            } else if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                    ligacaoPopUp = getMapaLigacao(mouseEvent);
                    if (ligacaoPopUp != null) {
                        setLinePopMenu();
                        jLinePopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                    }
                }
            }
        }
    }

    public void mouseDragged(java.awt.event.MouseEvent mouseEvent) {
        //Faz o desenho do pontilhado para indicar onde ser� desenhado o tema.
        Point pMouse = new Point(mouseEvent.getPoint());

        if (btnDesenharTema.isSelected() && mouseEvent.getSource() instanceof BscMapaPerspectiva && dragState != DRAG_PER_MOVING) {
            BscMapaPerspectiva perspectivaAtual = (BscMapaPerspectiva) mouseEvent.getSource();
            Point pPerspOrigem = new Point(pMousePressed.x, pMousePressed.y);
            pPerspOrigem.translate(perspectivaAtual.getLocation().x, perspectivaAtual.getLocation().y);
            BscMapaPerspectiva perspectivaOrigem = (BscMapaPerspectiva) jPanelMapa.getComponentAt(pPerspOrigem.x, pPerspOrigem.y);
            if (perspectivaAtual.getId() == perspectivaOrigem.getId()) {
                perspectivaAtual.setPaintTmpBox(pMousePressed.x, pMousePressed.y, mouseEvent.getX(), mouseEvent.getY());
                jPanelVidro.repaint();
            }
        }

        if (this.editEnabled) {
            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mouseEvent.getSource();
                if (dragState == DRAG_PER_MOVING) {
                    Dimension d = mapaPerspectiva.getSize();
                    d.height = mouseEvent.getY() + 4;
                    mapaPerspectiva.setPreferredSize(new Dimension(d));
                    mapaPerspectiva.setMaxBorder(DRAG_RESIZE_DOWN);
                    mapaPerspectiva.validate();

                    validateMapa();
                } else {
                    if (ligacaoConnected != null) {
                        pMouse.translate(mapaPerspectiva.getLocation().x, mapaPerspectiva.getLocation().y);
                        moveCtrlPoint(pMouse.x + mousePressedLocation.x, pMouse.y + mousePressedLocation.y);
                    }
                }
            } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                if (dragState == DRAG_OBJ_CONNECT && btnLigar.isSelected()) {
                    //Pinta a linha quando o obejtivo � movido.
                    drawVirtualLine(mouseEvent);
                } else {
                    BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                    BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mapaTema.getParent().getParent();

                    if (ligacaoConnected != null) {
                        //Verifica se o usu�rio clicou sobre o ponto de controle da reta.
                        pMouse = translatePointToClass(mapaTema, pMouse);
                        moveCtrlPoint(pMouse.x, pMouse.y);
                    } else if (btnSelecionar.isSelected()) {
                        //Faz o redimencionamento do tema...
                        Point pLastLocation = new Point(pMouse.x - mousePressedLocation.x, pMouse.y - mousePressedLocation.y);
                        pMouse.translate(mapaTema.getLocation().x, mapaTema.getLocation().y);
                        mapaTema.mapaTemaResize(pMouse, dragState, pLastLocation);
                    }
                }
                jPanelVidro.repaint();

            } else if (mouseEvent.getSource() instanceof JTextPane || mouseEvent.getSource() instanceof BscMapaObjetivo) {
                BscMapaObjetivo mapaObjetivo;
                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                } else {
                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                }

                pMouse.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);

                if (ligacaoConnected != null) {
                    moveCtrlPoint(pMouse.x + mousePressedLocation.x, pMouse.y + mousePressedLocation.y);
                } else if (btnSelecionar.isSelected()) {
                    Point pLastLocation = new Point(mouseEvent.getX() - mousePressedLocation.x, mouseEvent.getY() - mousePressedLocation.y);
                    mapaObjetivo.objetivoResize(pMouse, dragState, pLastLocation, jPanelVidro);

                } else if (btnLigar.isSelected()) {
                    drawVirtualLine(mouseEvent);
                }
            }
        }
    }

    public void mouseEntered(java.awt.event.MouseEvent mouseEvent) {
        if (this.editEnabled) {
            if (dragState == DRAG_NOTHING) {
                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    } else {
                        jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                    BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(setMousePointer(mouseEvent, mapaTema.getBounds(), getBorderBounds(mapaTema)));
                    } else {
                        jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaTema.getBounds(), getBorderBounds(mapaTema)));
                    }

                } else if (mouseEvent.getSource() instanceof JTextPane) {
                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    } else {
                        jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    }
                }

            } else if (dragState == DRAG_OBJ_CONNECT) {
                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                    if (mapaObjetivo != dragSource) {
                        mapaObjetivo.objetivoSetBorder(true);
                        dragDestine = mapaObjetivo;
                    }
                } else if (mouseEvent.getSource() instanceof JTextPane) {
                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                    if (mapaObjetivo != dragSource) {
                        mapaObjetivo.objetivoSetBorder(true);
                        dragDestine = mapaObjetivo;
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                    BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                    if (mapaTema != dragSource) {
                        mapaTema.setTemaSelected(true);
                        dragDestine = mapaTema;
                    }
                }
            }
        } else {
            if (mouseEvent.getSource() instanceof JTextPane) {
                if (isMaximize && maximizeForm != null) {
                    //maximizeForm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                } else {
                    jLayeredPaneMapa.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                }
            }
        }
    }

    public void mouseExited(java.awt.event.MouseEvent mouseEvent) {
        if (this.editEnabled) {
            if (dragState == DRAG_NOTHING) {
                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(null);
                    } else {
                        jLayeredPaneMapa.setCursor(null);
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(null);
                    } else {
                        jLayeredPaneMapa.setCursor(null);
                    }
                } else if (mouseEvent.getSource() instanceof JTextPane) {
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(null);
                    } else {
                        jLayeredPaneMapa.setCursor(null);
                    }
                }
            } else if (dragState == DRAG_OBJ_CONNECT) {
                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                    if (mapaObjetivo != dragSource) {
                        mapaObjetivo.objetivoSetBorder(false);
                        dragDestine = null;
                    }
                } else if (mouseEvent.getSource() instanceof JTextPane) {
                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                    if (mapaObjetivo != dragSource) {
                        mapaObjetivo.objetivoSetBorder(false);
                        dragDestine = null;
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                    BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                    mapaTema.setTemaSelected(false);
                    dragDestine = null;
                }
            }
        } else {
            if (mouseEvent.getSource() instanceof JTextPane) {
                if (isMaximize && maximizeForm != null) {
                    maximizeForm.setCursor(null);
                } else {
                    jLayeredPaneMapa.setCursor(null);
                }
            }
        }
    }

    public void mouseMoved(java.awt.event.MouseEvent mouseEvent) {
        if (this.editEnabled) {
            if (dragState == DRAG_NOTHING || dragState == DRAG_PER_MOVING) {
                if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                    BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mouseEvent.getSource();
                    //System.out.println("MapaPerspectiva:"+mapaPerspectiva.getId()+" - moved:"+mouseEvent.getPoint());
                    if (mouseEvent.getY() >= (mapaPerspectiva.getSize().height - 5)) {
                        dragState = DRAG_PER_MOVING;
                        if (isMaximize && maximizeForm != null) {
                            maximizeForm.setCursor(setMousePointer(mouseEvent, mapaPerspectiva.getBounds()));
                        } else {
                            jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaPerspectiva.getBounds()));
                        }
                    } else {
                        if (isMaximize && maximizeForm != null) {
                            maximizeForm.setCursor(setMousePointer(mouseEvent, new Rectangle(0, 0, 0, 0)));
                        } else {
                            jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, new Rectangle(0, 0, 0, 0)));
                        }
                        dragState = DRAG_NOTHING;
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                    BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                    Point p = translatePointToClass(mapaTema, mouseEvent.getPoint());
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(setMousePointer(mouseEvent, mapaTema.getBounds(), getBorderBounds(mapaTema)));
                    } else {
                        jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaTema.getBounds(), getBorderBounds(mapaTema)));
                    }
                } else if (mouseEvent.getSource() instanceof BscMapaObjetivo || mouseEvent.getSource() instanceof JTextPane) {
                    BscMapaObjetivo mapaObjetivo;
                    if (mouseEvent.getSource() instanceof JTextPane) {
                        JTextPane jText = (JTextPane) mouseEvent.getSource();
                        mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                    } else {
                        mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                    }
                    if (isMaximize && maximizeForm != null) {
                        maximizeForm.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    } else {
                        jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent, mapaObjetivo.getBounds()));
                    }
                }
            }
        }
    }

    public void mousePressed(java.awt.event.MouseEvent mouseEvent) {
        pMousePressed.setLocation(mouseEvent.getPoint());

        if (this.editEnabled) {
            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                if (dragState == DRAG_PER_MOVING) {
                    mousePressedLocation.setLocation(mouseEvent.getX(), mouseEvent.getY());
                } else {
                    //Verificar se o usu�rio clicou sobre o quadrado de controle da linha.
                    BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mouseEvent.getSource();
                    ligacaoConnected = getMapaLigacao(mouseEvent);
                    if (ligacaoConnected != null) {
                        //Posiciona  o quadrado de controle onde doi clicado.
                        Point p = new Point(mouseEvent.getPoint());
                        p.translate(mapaPerspectiva.getLocation().x, mapaPerspectiva.getLocation().y);

                        mousePressedLocation.setLocation(ligacaoConnected.ctrl.x - p.x, ligacaoConnected.ctrl.y - p.y);
                        moveCtrlPoint(p.x + mousePressedLocation.x, p.y + mousePressedLocation.y);
                    }
                }

            } else if (mouseEvent.getSource() instanceof BscMapaObjetivo || mouseEvent.getSource() instanceof JTextPane) {
                BscMapaObjetivo mapaObjetivo;

                if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                    mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                } else {
                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                }

                BscMapaPerspectiva mapaPerspectiva = mapaObjetivo.getMapaPerspectiva();
                mapaPerspectiva.moveToFront(mapaObjetivo);
                mousePressedLocation.setLocation(mouseEvent.getX(), mouseEvent.getY());

                if (btnSelecionar.isSelected()) {
                    ligacaoConnected = getMapaLigacao(mouseEvent);
                    if (ligacaoConnected != null) {
                        Point p = new Point(mouseEvent.getPoint());
                        p.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);

                        mousePressedLocation.setLocation(ligacaoConnected.ctrl.x - p.x, ligacaoConnected.ctrl.y - p.y);
                        moveCtrlPoint(p.x + mousePressedLocation.x, p.y + mousePressedLocation.y);
                    } else {
                        dragState = mapaObjetivo.setDragStatus(mouseEvent);
                    }
                } else if (btnLigar.isSelected()) {
                    dragState = DRAG_OBJ_CONNECT;
                    dragSource = mapaObjetivo;
                }
            } else if (mouseEvent.getSource() instanceof BscMapaTema) {
                BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mapaTema.getParent().getParent();
                mapaPerspectiva.getLayeredPane().setLayer(mapaTema, 2);
                mapaPerspectiva.moveToFront(mapaTema);
                mousePressedLocation.setLocation(mouseEvent.getX(), mouseEvent.getY());

                if (btnSelecionar.isSelected()) {
                    //Verificar se o usu�rio clicou sobre o quadrado de controle.
                    ligacaoConnected = getMapaLigacao(mouseEvent);
                    if (ligacaoConnected != null) {
                        //Posiciona  o quadrado de controle onde doi clicado.
                        Point p = new Point(mouseEvent.getPoint());
                        p.translate(mapaPerspectiva.getLocation().x, mapaPerspectiva.getLocation().y);

                        mousePressedLocation.setLocation(ligacaoConnected.ctrl.x - p.x, ligacaoConnected.ctrl.y - p.y);
                        moveCtrlPoint(p.x + mousePressedLocation.x, p.y + mousePressedLocation.y);
                    } else {
                        dragState = mapaTema.setDragStatus(mouseEvent);
                    }

                } else if (btnLigar.isSelected()) {
                    dragState = DRAG_OBJ_CONNECT;
                    dragSource = (BscMapaJPanel) mapaTema;
                }
            }
        } else {
            if (mouseEvent.getSource() instanceof JTextPane) {
                try {

                    JTextPane jText = (JTextPane) mouseEvent.getSource();
                    dragSource = (BscMapaObjetivo) jText.getParent().getParent();

                    /*N�o dever� habilitar o menu quando maximizado.*/
                    if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
                        if (!isMaximize) {
                            if (((BscMapaObjetivo) dragSource).getHasFcs()) {
                                jObjxFcs.setEnabled(true);
                            } else {
                                jObjxFcs.setEnabled(false);
                            }

                            jDrillPopMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
                        }

                    } else {
                        if (isMaximize && maximizeForm != null) {
                            bsc.swing.BscDefaultDialogSystem dialogSystem;
                            if (isMaximize && maximizeForm != null) {
                                dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
                            } else {
                                dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
                            }
                            dialogSystem.informationMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00036"));
                        } else {
                            bsc.swing.BscDefaultFrameFunctions frame =
                                    bsc.core.BscStaticReferences.getBscFormController().getForm("DRILL", "" + dragSource.getId(), dragSource.getNome());
                        }
                    }
                } catch (bsc.core.BscFormControllerException exc) {
                    bsc.core.BscDebug.println(exc.getMessage());
                }
            }
        }
    }

    public void mouseReleased(java.awt.event.MouseEvent mouseEvent) {
        if (this.editEnabled) {
            if (isMaximize && maximizeForm != null) {
                maximizeForm.setCursor(null);
            } else {
                jLayeredPaneMapa.setCursor(null);
            }

            if (dragState != DRAG_NOTHING) {
                if (dragState == DRAG_OBJ_CONNECT) {
                    if (dragDestine != null) {
                        if (dragDestine instanceof BscMapaObjetivo) {
                            BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) dragDestine;
                            mapaObjetivo.objetivoSetBorder(false);
                        } else if (dragDestine instanceof BscMapaTema) {
                            dragDestine.setTemaSelected(false);
                        }

                        try {
                            ligHashSet.add(new BscMapaLigacao(dragSource.getId(), dragDestine.getId()));
                            if (dragSource instanceof BscMapaTema) {
                                validMapConnection(dragSource);
                            } else if (dragDestine instanceof BscMapaTema) {
                                validMapConnection(dragDestine);
                            }
                        } catch (Exception e) {
                        }
                    }
                    virtualLine = null;
                    dragDestine = null;

                } else {
                    if (mouseEvent.getSource() instanceof BscMapaTema) {
                        BscMapaTema mapaTema = (BscMapaTema) mouseEvent.getSource();
                        BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mapaTema.getParent().getParent();
                        addAllObjectiveThema(mapaPerspectiva, mapaTema, false);
                        mapaTema.setMaxMoviment();
                        //Verifica se existe um tema selecionado.
                    } else if (mouseEvent.getSource() instanceof BscMapaObjetivo || mouseEvent.getSource() instanceof JTextPane) {
                        BscMapaObjetivo mapaObjetivo;
                        if (mouseEvent.getSource() instanceof BscMapaObjetivo) {
                            mapaObjetivo = (BscMapaObjetivo) mouseEvent.getSource();
                        } else {
                            JTextPane jText = (JTextPane) mouseEvent.getSource();
                            mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                        }

                        BscMapaPerspectiva mapaPerspectiva = mapaObjetivo.getMapaPerspectiva();
                        mapaPerspectiva.objetivoShadow = false;

                        if (mapaObjetivo.getParent() instanceof BscMapaTema) {
                            BscMapaTema mapaTema = (BscMapaTema) mapaObjetivo.getParent();
                            removeObjectiveFromThema(mapaTema, mapaObjetivo);
                        } else {
                            addObjectiveToThema(mapaPerspectiva, mapaObjetivo);
                        }
                        //Corrige o posicionamento do objetivo se ele estiver fora do container.
                        mapaObjetivo.setMaxMoviment();
                    }
                }

                dragState = DRAG_NOTHING;
                jPanelVidro.repaint();

            } else if (btnDesenharTema.isSelected() && mouseEvent.getButton() != MouseEvent.BUTTON3) {
                addThemaToPerspectiva(mouseEvent);
            }
        }

        ligacaoConnected = null;
    }

    public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) throws java.awt.print.PrinterException {
        if (pageIndex > 0) {
            return (NO_SUCH_PAGE);
        } else {
            java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
            g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY() + 50);
            g2d.drawString(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("JBILayeredPane_00001") + record.getString("NOME"), 0, 0);
            g2d.scale(0.5, 0.5);
            disableDoubleBuffering(this);
            g2d.translate(0, 10);
            bsc.swing.map.BscMapaFrame.jPanelDados.paint(g2d);
            g2d.translate(0, bsc.swing.map.BscMapaFrame.jPanelDados.getHeight());
            jLayeredPaneMapa.paint(g2d);
            enableDoubleBuffering(this);
            return (PAGE_EXISTS);
        }
    }

    /**
     * Grava o objeto passado com uma figura do tipo JPEG
     */
    public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }

    public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }

    //Evento de movimentar a barra de rolagem.
    public void stateChanged(javax.swing.event.ChangeEvent evt) {
        BoundedRangeModel sourceScroll = (BoundedRangeModel) evt.getSource();
        java.awt.FlowLayout pnlLayout = (java.awt.FlowLayout) pnlLayoutDiv.getLayout();

        pnlLayout.setHgap(pnlLayout.getHgap() + (lastTrackVal - sourceScroll.getValue()));
        lastTrackVal = sourceScroll.getValue();
        pnlLayoutDiv.doLayout();
    }

    public void moveBars(BscMapaJPanel origem, Point pMouse) {
        int posVerBar = jScrollPaneMapa.getVerticalScrollBar().getValue();
        int posHorBar = jScrollPaneMapa.getHorizontalScrollBar().getValue();

        Point p1 = pMouse;
        Point p11 = origem.getLocation();
        Point p12 = origem.getParent().getLocation();
        Point p13 = origem.getParent().getParent().getLocation();
        Point p14 = origem.getParent().getParent().getParent().getLocation();

        p14.y = (p14.y < 0 ? 0 : p14.y);
        p14.x = (p14.x < 0 ? 0 : p14.x);

        p1.translate(p11.x, p11.y);
        p1.translate(p12.x, p12.y);
        p1.translate(p13.x, p13.y);
        p1.translate(p14.x, p14.y);

        //Ajuste vertical
        if ((p1.y - posVerBar) > jScrollPaneMapa.getHeight()) {
            jScrollPaneMapa.getVerticalScrollBar().setValue(jScrollPaneMapa.getVerticalScrollBar().getValue() + 10);
        }
        if ((p1.y - posVerBar) < 0) {
            jScrollPaneMapa.getVerticalScrollBar().setValue(jScrollPaneMapa.getVerticalScrollBar().getValue() - 10);
        }

        //Ajuste horizontal
        if (((p1.x + 30) - posHorBar) > jScrollPaneMapa.getWidth()) {
            jScrollPaneMapa.getHorizontalScrollBar().setValue(jScrollPaneMapa.getHorizontalScrollBar().getValue() + 10);
        }
        if (((p1.x + 30) - posHorBar) < 0) {
            jScrollPaneMapa.getHorizontalScrollBar().setValue(jScrollPaneMapa.getHorizontalScrollBar().getValue() - 10);
        }
    }

    //Controla o movimento maximo do ponto de controle e faz o movimento.
    private void moveCtrlPoint(int x, int y) {

        x = (x < 10 ? 10 : x);
        y = (y < 10 ? 10 : y);
        x = (x < (jLayeredPaneMapa.getWidth() - 30) ? x : (jLayeredPaneMapa.getWidth() - 30));
        y = (y < (jLayeredPaneMapa.getHeight() - 10) ? y : (jLayeredPaneMapa.getHeight() - 10));

        ligacaoConnected.ctrl.setLocation(x, y);
        jPanelVidro.repaint();
    }

    private void saveComponentAsJPEG(String fileName) {
        Dimension size = jLayeredPaneMapa.getSize();
        size.setSize(size.getWidth(), size.getHeight() + bsc.swing.map.BscMapaFrame.jPanelDados.getHeight());
        java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height,
                java.awt.image.BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = myImage.createGraphics();
        g2.drawString(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("JBILayeredPane_00001") + record.getString("NOME"), 0, 0);
        bsc.swing.map.BscMapaFrame.jPanelDados.paint(g2);
        g2.translate(0, bsc.swing.map.BscMapaFrame.jPanelDados.getHeight());
        JBILayeredPane.disableDoubleBuffering(jLayeredPaneMapa);
        jLayeredPaneMapa.paint(g2);
        JBILayeredPane.enableDoubleBuffering(jLayeredPaneMapa);

        //Grava a figura l� no servidor... :)
        try {
            java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream();
            //com.sun.image.codec.jpeg.JPEGImageEncoder encoder = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(out);
            //encoder.encode(myImage);
            //Salva o arquivo de imagem como JPEG
            ImageIO.write(myImage, "jpg", out);
            String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
            event.saveBase64(fileName, strBase64);
            out.close();
            //Abre o navegador para salvar a figura localmente
            bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
            java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString() + fileName);
            bscApplet.getAppletContext().showDocument(url, "_new");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void addThemaFromXml(BscMapaPerspectiva perspectiva, bsc.xml.BIXMLVector temas) {
        bsc.xml.BIXMLRecord tema;
        for (int j = 0; j < temas.size(); j++) {
            tema = temas.get(j);
            javax.swing.border.TitledBorder tilleBorder = new javax.swing.border.TitledBorder(null,
                    tema.getString("NOME"),
                    javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 11));

            //Tamanho minimo para desenho do tema.
            BscMapaTema mapaTemp = new BscMapaTema();
            mapaTemp.setLayout(new java.awt.CardLayout());
            mapaTemp.setLayout(null);
            mapaTemp.setBorder(tilleBorder);
            mapaTemp.setFocusCycleRoot(true);
            mapaTemp.setBackground(new java.awt.Color(tema.getInt("BACKCOLOR")));

            if (tema.getInt("ID") > temaCountID) {
                temaCountID = tema.getInt("ID");
            }

            String temaID = new String("T");

            mapaTemp.setId(temaID.concat(tema.getString("ID")));
            mapaTemp.setBounds(tema.getInt("X"), tema.getInt("Y"), tema.getInt("WIDTH"), tema.getInt("HEIGHT"));
            mapaTemp.addMouseListener(this);
            mapaTemp.addMouseMotionListener(this);

            perspectiva.getLayeredPane().add(mapaTemp);
            idHashMap.put(new String(mapaTemp.getId()), mapaTemp);

            addAllObjectiveThema(perspectiva, mapaTemp, false);
        }
    }

    private void divideMapa(Graphics2D g2) {
        int iDivisao = Integer.parseInt((String) spnDivideMapa.getValue());
        int iWidth = jPanelMapa.getWidth() / iDivisao; //Largura da linha

        g2.setColor(new java.awt.Color(153, 153, 153));
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.5f));
        for (int iCol = 1; iCol < iDivisao; iCol++) {
            g2.drawLine(iWidth, 0, iWidth, jPanelMapa.getHeight());
            iWidth += iWidth;
        }

    }

    /**
     * Configura o ponteiro do mouse.
     * @param  MouseEvent evento do mouse
     * @param objRectangle Retangulo que representa o objeto.
     * @return Retorna o ponteiro.
     */
    private Cursor setMousePointer(MouseEvent mouseEvent, Rectangle objRectangle) {
        Cursor pointerTema = null;
        Rectangle borRectangle = new Rectangle(0, 0, 0, 0);

        return setMousePointer(mouseEvent, objRectangle, borRectangle);
    }

    /**
     * Configura o ponteiro do mouse.
     * @param  MouseEvent evento do mouse
     * @param objRectangle Retangulo que representa o objeto.
     * @param borRectangle Borda do objeto.
     * @return Retorna o ponteiro.
     */
    private Cursor setMousePointer(MouseEvent mouseEvent, Rectangle objRectangle, Rectangle borRectangle) {
        Cursor pointerTema = null;
        Point pMouse = mouseEvent.getPoint();

        if (btnSelecionar.isSelected()) {
            BscMapaLigacao mapaLigacao = getMapaLigacao(mouseEvent);
            if (mapaLigacao != null) {
                return new Cursor(Cursor.HAND_CURSOR);
            } else if (!(mouseEvent.getSource() instanceof JTextPane)) {
                Rectangle rLado = null;
                for (int iLado = 4; iLado <= 7; iLado++) {
                    if (iLado == DRAG_RESIZE_LEFT) {
                        rLado = new Rectangle(0, 0, LARGURABORDA, objRectangle.height);
                    } else if (iLado == DRAG_RESIZE_UP) {
                        rLado = new Rectangle(0, 0, objRectangle.width, LARGURABORDA);
                    } else if (iLado == DRAG_RESIZE_RIGHT) {
                        rLado = new Rectangle(objRectangle.width - LARGURABORDA, 0, objRectangle.height, objRectangle.height);
                    } else if (iLado == DRAG_RESIZE_DOWN) {
                        rLado = new Rectangle(0, objRectangle.height - LARGURABORDA, objRectangle.width, objRectangle.height);
                    }

                    if (rLado.contains(pMouse)) {
                        if (iLado == DRAG_RESIZE_LEFT || iLado == DRAG_RESIZE_RIGHT) {
                            return new Cursor(Cursor.E_RESIZE_CURSOR);
                        } else if (iLado == DRAG_RESIZE_DOWN || iLado == DRAG_RESIZE_UP) {
                            return new Cursor(Cursor.N_RESIZE_CURSOR);
                        }
                    }
                }
            }

            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                pointerTema = new Cursor(Cursor.DEFAULT_CURSOR);
            } else {
                pointerTema = new Cursor(Cursor.MOVE_CURSOR);
            }

        } else if (btnDesenharTema.isSelected()) {
            //Verifica se o mouse est� sobre o texto.
            if (borRectangle.contains(pMouse)) {
                return new Cursor(Cursor.TEXT_CURSOR);
            } else if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                pointerTema = new Cursor(Cursor.CROSSHAIR_CURSOR);
            }
        } else if (btnLigar.isSelected()) {
            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                pointerTema = new Cursor(Cursor.DEFAULT_CURSOR);
            } else {
                pointerTema = new Cursor(Cursor.CROSSHAIR_CURSOR);
            }
        }

        return pointerTema;
    }

    private Rectangle getBorderBounds(BscMapaJPanel origem) {
        javax.swing.border.TitledBorder bordaTema = (javax.swing.border.TitledBorder) origem.getBorder();

        //Lendo o tamanho do texto em pixels;
        java.awt.Graphics2D gTexto = (java.awt.Graphics2D) origem.getGraphics();
        gTexto.setFont(bordaTema.getTitleFont());
        FontMetrics metrics = gTexto.getFontMetrics();
        int width = metrics.stringWidth(bordaTema.getTitle());
        int height = metrics.getHeight();
        int titX = 0;

        if (bordaTema.getTitleJustification() == TitledBorder.CENTER) {
            titX = (int) (origem.getWidth() - width) / 2;
        } else if (bordaTema.getTitleJustification() == TitledBorder.RIGHT) {
            titX = origem.getWidth() - width;
        }

        return new Rectangle(titX, 0, width, height);
    }

    private void addThemaToPerspectiva(java.awt.event.MouseEvent mouseEvent) {
        javax.swing.border.TitledBorder tilleBorder = new javax.swing.border.TitledBorder(null,
                java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00016"),
                javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 11));

        BscMapaPerspectiva perspAtual = (BscMapaPerspectiva) mouseEvent.getSource();
        BscMapaPerspectiva perspOrigem = (BscMapaPerspectiva) jPanelMapa.getComponentAt(pMousePressed.x, pMousePressed.y + perspAtual.getY());
        Rectangle temaModel = perspOrigem.recTmpBox;
        //Tamanho minimo para desenho do tema.
        if (temaModel.height > 50 && temaModel.width > 50) {
            BscMapaTema mapaTemp = new BscMapaTema();
            mapaTemp.setLayout(new java.awt.CardLayout());
            mapaTemp.setLayout(null);
            mapaTemp.setBorder(tilleBorder);
            mapaTemp.setFocusCycleRoot(true);
            mapaTemp.setBackground(new java.awt.Color(208, 208, 208));

            temaCountID++;
            String temaID = new String("T");
            mapaTemp.setId(temaID.concat(new Integer(temaCountID).toString()));

            //O Valor de 23 na corre��o do ponto Y � referente a uma diferen�a na borda do painel de perspectiva.
            mapaTemp.setBounds(temaModel.x, temaModel.y - 23, temaModel.width, temaModel.height);

            mapaTemp.addMouseListener(this);
            mapaTemp.addMouseMotionListener(this);

            if (mouseEvent.getSource() instanceof BscMapaPerspectiva) {
                addAllObjectiveThema(perspOrigem, mapaTemp, true);
            }

            idHashMap.put(new String(mapaTemp.getId()), mapaTemp);
            String strBorda;
            bsc.swing.BscDefaultDialogSystem dialogSystem;
            if (isMaximize && maximizeForm != null) {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(maximizeForm);
            } else {
                dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
            }

            strBorda = dialogSystem.inputDialog(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00015"), tilleBorder.getTitle());
            if (strBorda != null) {
                tilleBorder.setTitle(strBorda);
            }
        }

        perspOrigem.setPaintTmpBox(false);
        perspOrigem.repaint();
    }

    //Adiciona todos os objetivos desta perspectiva ao tema.
    private void addAllObjectiveThema(BscMapaPerspectiva mapaPerspectiva, BscMapaTema mapaTema, boolean lNewTema) {
        int iNumObjetivos = 0;

        for (int component = 0; component < mapaPerspectiva.getLayeredPane().getComponentCount(); component++) {
            if (mapaPerspectiva.getLayeredPane().getComponent(component) instanceof BscMapaObjetivo) {
                //Verifica se o pontilhado passou sobre os obejtivos.
                BscMapaObjetivo mapaObjetivo = (BscMapaObjetivo) mapaPerspectiva.getLayeredPane().getComponent(component);
                Rectangle rPontilhado = mapaTema.getBounds();
                if (rPontilhado.contains(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y)) {
                    iNumObjetivos++;
                    mapaTema.add(mapaObjetivo);
                    mapaObjetivo.setLocation(mapaObjetivo.getX() - mapaTema.getX(), mapaObjetivo.getY() - mapaTema.getY());
                    validMapConnection((BscMapaJPanel) mapaTema);
                    component--;
                }
            }
        }
        if (lNewTema) {
            mapaPerspectiva.getLayeredPane().add(mapaTema);
        } else {
            //Se o numero de objetivo for zero, e o usuario estiver selecionando um tema estrategico, marca o tema para remo��o e remove-o.
            if (iNumObjetivos == 0 && cbbFiltroTema.getSelectedIndex() != 0) {
                idHashMap.remove(mapaTema.getId());
                mapaPerspectiva.getLayeredPane().remove(mapaTema);
            }
        }

        jLayeredPaneMapa.repaint();
    }

    //Opera��o de arrastar e soltar com o objetivo.
    private void addObjectiveToThema(BscMapaPerspectiva mapaPerspectiva, BscMapaObjetivo objetivo) {
        for (int component = 0; component < mapaPerspectiva.getLayeredPane().getComponentCount(); component++) {
            if (mapaPerspectiva.getLayeredPane().getComponent(component) instanceof BscMapaTema) {
                BscMapaTema mapaTema = (BscMapaTema) mapaPerspectiva.getLayeredPane().getComponent(component);
                Rectangle rPontilhado = mapaTema.getBounds();

                if (rPontilhado.contains(objetivo.getLocation().x, objetivo.getLocation().y)) {
                    mapaTema.add(objetivo);
                    objetivo.setLocation(objetivo.getX() - mapaTema.getX(), objetivo.getY() - mapaTema.getY());
                    validMapConnection((BscMapaJPanel) mapaTema);
                    break;
                }
            }
        }
    }

    private void removeThemaFromPerspective() {
        //Lendo as perspectiva dentro do painel.
        for (int perspectivaItem = 0; perspectivaItem < jPanelMapa.getComponentCount(); perspectivaItem++) {
            if (jPanelMapa.getComponent(perspectivaItem) instanceof BscMapaPerspectiva) {
                BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) jPanelMapa.getComponent(perspectivaItem);
                //Lendo todos os temas da perspectiva
                for (int temaItem = 0; temaItem < mapaPerspectiva.getLayeredPane().getComponentCount(); temaItem++) {
                    if (mapaPerspectiva.getLayeredPane().getComponent(temaItem) instanceof BscMapaTema) {
                        BscMapaTema mapaTema = (BscMapaTema) mapaPerspectiva.getLayeredPane().getComponent(temaItem);

                        if (mapaTema.temaSelected) {
                            //Lendo todos os objetivos dentro do tema.
                            for (int objetivoItem = 0; objetivoItem < mapaTema.getComponentCount(); objetivoItem++) {
                                if (mapaTema.getComponent(objetivoItem) instanceof BscMapaObjetivo) {
                                    BscMapaObjetivo objetivo = (BscMapaObjetivo) mapaTema.getComponent(objetivoItem);
                                    mapaPerspectiva.getLayeredPane().add(objetivo);
                                    objetivoItem--;
                                    //Reposicionando o objetivo.
                                    Point pObjetivo = new Point(objetivo.getX(), objetivo.getY());
                                    pObjetivo.translate(mapaTema.getX(), mapaTema.getY());
                                    objetivo.setLocation(pObjetivo);
                                }
                            }
                            idHashMap.remove(mapaTema.getId());
                            mapaPerspectiva.getLayeredPane().remove(mapaTema);
                            temaItem--;
                        }
                    }
                }
            }
        }
    }

    private void removeObjectiveFromThema(BscMapaTema mapaTema, BscMapaObjetivo objetivo) {
        BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) mapaTema.getParent().getParent();
        Rectangle rPontilhado = mapaTema.getBounds();
        Point objetivoP = new Point(objetivo.getX(), objetivo.getY());
        objetivoP.translate(mapaTema.getLocation().x, mapaTema.getLocation().y);

        if (!rPontilhado.contains(objetivoP.x, objetivoP.y)) {
            mapaPerspectiva.getLayeredPane().add(objetivo);
            //Reposicionando o objetivo.
            Point pObjetivo = new Point(objetivo.getX(), objetivo.getY());
            pObjetivo.translate(mapaTema.getX(), mapaTema.getY());
            objetivo.setLocation(pObjetivo);
            addObjectiveToThema(mapaPerspectiva, objetivo);
        }
    }

    void validateMapa() {
        int totalHeight = 0, height = 0;
        for (int i = 0; i < jPanelMapa.getComponentCount(); i++) {
            BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) jPanelMapa.getComponent(i);
            if (i == 0) {
                mapaPerspectiva.getComponent(0).requestFocusInWindow();
            }
            Dimension d = mapaPerspectiva.getPreferredSize();

            d.height = (d.height < PER_MIN_SIZE) ? PER_MIN_SIZE : d.height;
            d.height = (d.height > PER_MAX_SIZE) ? PER_MAX_SIZE : d.height;

            mapaPerspectiva.setPreferredSize(d);
            totalHeight += d.height;
        }

        jPanelMapa.setBounds(0, 0, 948, totalHeight);
        jPanelMapa.setPreferredSize(new Dimension(948, totalHeight));
        jPanelMapa.revalidate();
        jPanelVidro.setBounds(0, 0, 948, totalHeight);
        jPanelVidro.setPreferredSize(new Dimension(948, totalHeight));
        jPanelVidro.revalidate();
        jLayeredPaneMapa.setPreferredSize(new Dimension(948, totalHeight));
        jLabelCarregando.setVisible(false);
        repaint();
    }

    //Faz a valida��o quando um obetivo � movido para dentro de um mapa,
    //para n�o permitir a conex�o de um obetivo ao tema ao qual ele est� contido.
    private void validMapConnection(BscMapaJPanel mapaTema) {
        BscMapaJPanel origem = null;
        BscMapaJPanel destino = null;
        BscMapaLigacao ligacao = null;
        String mapaId = mapaTema.getId();
        String origemId = null;
        String destinoId = null;
        String objetivoId = null;

        //Lendo todos os objetivos dentro do tema.
        for (int objetivoItem = 0; objetivoItem < mapaTema.getComponentCount(); objetivoItem++) {
            if (mapaTema.getComponent(objetivoItem) instanceof BscMapaObjetivo) {
                BscMapaObjetivo objetivo = (BscMapaObjetivo) mapaTema.getComponent(objetivoItem);
                objetivoId = objetivo.getId();
                //Verifica a conex�o para este objetivo, n�o pode ser a mesma do mapa.
                Iterator it = ligHashSet.iterator();
                while (it.hasNext()) {
                    ligacao = (BscMapaLigacao) it.next();
                    origem = (BscMapaJPanel) idHashMap.get(new String(ligacao.getSource()));
                    destino = (BscMapaJPanel) idHashMap.get(new String(ligacao.getDestine()));
                    try {
                        origemId = origem.getId();
                        destinoId = destino.getId();
                        if ((origemId.equals(mapaId) && destinoId.equals(objetivoId)) ||
                                (origemId.equals(objetivoId) && destinoId.equals(mapaId))) {
                            it.remove();
                        }
                    } catch (Exception e) {
                        System.out.println("ID n�o dispon�vel para valida��o: " + e);
                    }
                }
            }
        }
    }

    //Retorna a ligacao do mapa quando est� dentro de uma perspectiva
    private BscMapaLigacao getMapaLigacao(MouseEvent mouseEvent) {
        BscMapaLigacao ligacao;
        Point p = translatePointToClass(mouseEvent.getSource(), mouseEvent.getPoint());

        Iterator it = ligHashSet.iterator();

        while (it.hasNext()) {
            ligacao = (BscMapaLigacao) it.next();

            if (ligacao.getLigacaoType() == BscMapaLigacao.QUADCURVE) {
                Rectangle recRef = new Rectangle(ligacao.ctrl.x - 4, ligacao.ctrl.y - 4, ligacao.ctrl.width + 4, ligacao.ctrl.height + 4);
                if (recRef.contains(p) && btnSelecionar.isSelected()) {
                    return ligacao;
                }
            } else if (ligacao.getArrow().ptSegDist(p) < 2) {
                return ligacao;
            }
        }

        return null;
    }

    private Point translatePointToClass(Object objOrigem, Point pOrigem) {

        if (objOrigem instanceof BscMapaPerspectiva) {
            BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva) objOrigem;
            pOrigem.translate(mapaPerspectiva.getX(), mapaPerspectiva.getY());

        } else if (objOrigem instanceof BscMapaTema) {
            BscMapaTema mapaTema = (BscMapaTema) objOrigem;
            pOrigem.translate(mapaTema.getX(), mapaTema.getY());
            pOrigem.translate(mapaTema.getParent().getLocation().x, mapaTema.getParent().getLocation().y);
            pOrigem.translate(mapaTema.getParent().getParent().getLocation().x, mapaTema.getParent().getParent().getLocation().y);

        } else if (objOrigem instanceof BscMapaObjetivo || objOrigem instanceof JTextPane) {
            BscMapaObjetivo mapaObjetivo;

            if (objOrigem instanceof BscMapaObjetivo) {
                mapaObjetivo = (BscMapaObjetivo) objOrigem;
            } else {
                JTextPane jText = (JTextPane) objOrigem;
                mapaObjetivo = (BscMapaObjetivo) jText.getParent().getParent();
                pOrigem.translate(jText.getLocation().x, jText.getLocation().y);
            }

            pOrigem.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);
            pOrigem.translate(mapaObjetivo.getParent().getLocation().x, mapaObjetivo.getParent().getLocation().y);
            pOrigem.translate(mapaObjetivo.getParent().getParent().getLocation().x, mapaObjetivo.getParent().getParent().getLocation().y);

            //Verifica se o objetivo est� dentro do tema.
            if (mapaObjetivo.getParent() instanceof BscMapaTema) {
                pOrigem.translate(mapaObjetivo.getParent().getParent().getParent().getLocation().x, mapaObjetivo.getParent().getParent().getParent().getLocation().y);
            }

        } else {
            pOrigem.setLocation(-10, -10);
        }

        return pOrigem;
    }

    private void drawVirtualLine(MouseEvent mouseEvent) {
        boolean temaInside = false;
        JTextPane jText = null;//Indica se a origem � um objetivo.

        if (dragSource.getParent() instanceof BscMapaTema) {
            temaInside = true;
        }

        if (mouseEvent.getSource() instanceof JTextPane) {
            jText = (JTextPane) mouseEvent.getSource();
        }

        //Verifica se � necessario a movimenta��o da barra vertical.
        Point pMouse = new Point(mouseEvent.getX(), mouseEvent.getY());
        moveBars(dragSource, pMouse);

        if (getCursor().getType() != Cursor.CROSSHAIR_CURSOR) {
            if (isMaximize && maximizeForm != null) {
                maximizeForm.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            } else {
                jLayeredPaneMapa.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            }
        }
        // Desenhar reta virtual
        Point p1 = new Point(mousePressedLocation);
        Point pLevel0 = new Point(0, 0);
        if (jText != null) {
            pLevel0 = jText.getLocation();
        }

        Point pLevel1 = dragSource.getLocation();
        Point pLevel2 = dragSource.getParent().getLocation();
        Point pLevel3 = dragSource.getParent().getParent().getLocation();
        Point pLevel4 = new Point(0, 0);

        if (temaInside) {
            pLevel4 = dragSource.getParent().getParent().getParent().getLocation();
        }

        p1.translate(pLevel0.x, pLevel0.y);
        p1.translate(pLevel1.x, pLevel1.y);
        p1.translate(pLevel2.x, pLevel2.y);
        p1.translate(pLevel3.x, pLevel3.y);
        p1.translate(pLevel4.x, pLevel4.y);

        Point p2 = new Point(mouseEvent.getX(), mouseEvent.getY());

        p2.translate(pLevel0.x, pLevel0.y);
        p2.translate(pLevel1.x, pLevel1.y);
        p2.translate(pLevel2.x, pLevel2.y);
        p2.translate(pLevel3.x, pLevel3.y);
        p2.translate(pLevel4.x, pLevel4.y);

        virtualLine = new bsc.util.BscArrow2D(p1, p2);
        jPanelVidro.repaint();
    }

    private void setLinePopMenu() {
        //Verifica como est� a vizualia��o antes de chamar o menu.
        if (ligacaoPopUp != null) {
            if (ligacaoPopUp.getLigacaoType() == BscMapaLigacao.LINE) {
                lineCurve.setSelected(false);
                lineStraight.setSelected(true);
            } else {
                lineCurve.setSelected(true);
                lineStraight.setSelected(false);
            }
        }
    }
}

/** Classe representa um liga��o entre dois objetivos */
class BscMapaLigacao {

    public static final int LINE = 0;
    public static final int QUADCURVE = 1;
    public Rectangle ctrl;
    private int type = LINE;
    private String source, destine;
    private boolean selected = false;
    private bsc.util.BscArrow2D arrow;

    public BscMapaLigacao(String source, String destine) throws Exception {
        if (source.equals(destine)) {
            throw new Exception(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00005"));
        }

        this.source = source;
        this.destine = destine;
        this.selected = false;
        ctrl = new Rectangle(0, 0, 0, 0);
    }

    @Override
    public boolean equals(Object obj) {
        boolean igual = false;
        if (obj instanceof BscMapaLigacao) {
            igual = (((BscMapaLigacao) obj).source.equals(this.source) &&
                    ((BscMapaLigacao) obj).destine.equals(this.destine));
        }
        return igual;
    }

    /** Getter for property destine.
     * @return Value of property destine.
     */
    public String getDestine() {
        return destine;
    }

    /** Setter for property destine.
     * @param destine New value of property destine.
     */
    public void setDestine(String destine) {
        this.destine = destine;
    }

    /** Getter for property source.
     * @return Value of property source.
     */
    public String getSource() {
        return source;
    }

    /** Setter for property source.
     * @param source New value of property source.
     */
    public void setSource(String source) {
        this.source = source;
    }

    /** Getter for property selected.
     * @return Value of property selected.
     */
    public boolean isSelected() {
        return selected;
    }

    /** Setter for property selected.
     * @param selected New value of property selected.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /** Getter for property arrow.
     * @return Value of property arrow.
     */
    public bsc.util.BscArrow2D getArrow() {
        return arrow;
    }

    /** Setter for property arrow.
     * @param arrow New value of property arrow.
     */
    public void setArrow(bsc.util.BscArrow2D arrow) {
        this.arrow = arrow;
    }

    public void setLigacaoType(int intType) {
        this.type = intType;

        if (this.getArrow() != null) {
            this.getArrow().lineType = intType;
        }
    }

    public int getLigacaoType() {
        return this.type;
    }
}
