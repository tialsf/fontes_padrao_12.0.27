/*
 * BscMapaJPanel.java
 *
 * Created on 25 de Novembro de 2004, 14:03
 */

package bsc.swing.map;

/**
 *
 * @author  alexandreas
 */
public class BscMapaJPanel extends javax.swing.JPanel {
    
    // Campos perspectiva
    String id	= "0";
    int status	= 0;
    int ordem	= 0;
    
    String  nome	= "";
    String  descricao	= "";
    
    /** Creates a new instance of BscMapaJPanel */
    public BscMapaJPanel() {
    }
   
    /** Getter for property id.
     * @return Value of property id.
     */
    public String getId() {
	return id;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId(String id) {
	this.id = id;
    }
    
    /** Getter for property nome.
     * @return Value of property nome.
     */
    public java.lang.String getNome() {
	return nome;
    }
    
    /** Setter for property nome.
     * @param nome New value of property nome.
     */
    public void setNome(java.lang.String nome) {
	this.nome = nome;
    }
    
    /** Getter for property descricao.
     * @return Value of property descricao.
     */
    public java.lang.String getDescricao() {
	return descricao;
    }
    
    /** Setter for property descricao.
     * @param descricao New value of property descricao.
     */
    public void setDescricao(java.lang.String descricao) {
	this.descricao = descricao;
    }
    
    /** Getter for property ordem.
     * @return Value of property ordem.
     */
    public int getOrdem() {
	return ordem;
    }
    
    /** Setter for property ordem.
     * @param ordem New value of property ordem.
     */
    public void setOrdem(int ordem) {
	this.ordem = ordem;
    }
    
    /** Getter for property status.
     * @return Value of property status.
     */
    public int getStatus() {
	return status;
    }
    
    //compatibilização para o BscMapaTema
    public void setTemaSelected(boolean lSelected) {
    }
    
    public void setTemaSelected(){
    }
}
