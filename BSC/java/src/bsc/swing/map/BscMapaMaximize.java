/**
 * BscGraphAvancado.java
 *
 * Created on 04 de Abril de 2006, 14:31
 * Mostra a tela de configurações avançadas do gráfico.
 */
package bsc.swing.map;

import bsc.swing.analisys.BscCentralFrame;
import java.awt.event.*;
/**
 *
 * @author  siga1776
 *
 */
public class BscMapaMaximize extends javax.swing.JDialog  {
	public static final int  CENTRAL = 0;
	public static final int  MAPA = 1;
	public javax.swing.JPanel pnlMapa;
	private int dglType = MAPA;
	private BscMapaFrame mapaParentFrame;
	private BscCentralFrame centralParentFrame;
	private bsc.util.BscToolKit oBscToolKit	= new bsc.util.BscToolKit();
	
	public BscMapaMaximize(java.awt.Container parent, boolean modal, javax.swing.JPanel pnlParent) {
		super();
		initComponents();
		System.out.println(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		this.setSize(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		this.setLocationRelativeTo( parent );
		pnlMapa = pnlParent;
		pnlMaximize.add(pnlParent,java.awt.BorderLayout.CENTER);
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
	}
	
	public void setMapaFrame(BscMapaFrame oBscMapaFrame){
		mapaParentFrame = oBscMapaFrame;
	}
	
	public void setCentralFrame(BscCentralFrame oBscCentralFrame){
		centralParentFrame = oBscCentralFrame;
		dglType = CENTRAL;
	}
	
        private void initComponents() {//GEN-BEGIN:initComponents
                pnlMaximize = new javax.swing.JPanel();

                setTitle("BSC");
                setModal(true);
                setName("dlgAvancado");
                addComponentListener(new java.awt.event.ComponentAdapter() {
                        public void componentShown(java.awt.event.ComponentEvent evt) {
                                formComponentShown(evt);
                        }
                });
                addWindowListener(new java.awt.event.WindowAdapter() {
                        public void windowClosing(java.awt.event.WindowEvent evt) {
                                closeDialog(evt);
                        }
                });

                pnlMaximize.setLayout(new java.awt.BorderLayout());

                getContentPane().add(pnlMaximize, java.awt.BorderLayout.CENTER);

                pack();
        }//GEN-END:initComponents
		
    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
		
    }//GEN-LAST:event_formComponentShown
	
	/** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
		if(dglType == MAPA){
			mapaParentFrame.btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00034"));
			mapaParentFrame.btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_minimizar.gif")));
			mapaParentFrame.getContentPane().add(pnlMapa,java.awt.BorderLayout.CENTER);
			mapaParentFrame.isMaximize = false;
			setVisible(false);
			centralParentFrame.toFront();
			mapaParentFrame.requestFocus();
			mapaParentFrame.doLayout();
		}else{
			centralParentFrame.btnMaximize.setText(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscGraphFrame_00034"));
			centralParentFrame.btnMaximize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_minimizar.gif")));
			centralParentFrame.getContentPane().add(pnlMapa,java.awt.BorderLayout.CENTER);
			centralParentFrame.isMaximize = false;
			setVisible(false);
			centralParentFrame.doLayout();
		}
		dispose();		
    }//GEN-LAST:event_closeDialog
	
        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JPanel pnlMaximize;
        // End of variables declaration//GEN-END:variables
}