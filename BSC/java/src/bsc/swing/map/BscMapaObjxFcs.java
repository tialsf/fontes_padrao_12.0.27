package bsc.swing.map;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.imageio.ImageIO;


/**
 *
 * @author Aline Corr�a do Vale
 */
public class BscMapaObjxFcs extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, java.awt.print.Printable, MouseListener, MouseMotionListener, javax.swing.event.ChangeListener{
	
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private static final int ABAIXO		= 0;
	private static final int ACIMA		= 1;
	private static final int ESQUERDA	= 2;
	private static final int DIREITA	= 3;
	
	public static final int DRAG_NOTHING		= 0;
	public static final int DRAG_OBJ_MOVING		= 2;
	public static final int DRAG_PER_MOVING		= 3;
	public static final int DRAG_RESIZE_LEFT	= 4;
	public static final int DRAG_RESIZE_UP		= 5;
	public static final int DRAG_RESIZE_RIGHT   = 6;
	public static final int DRAG_RESIZE_DOWN    = 7;
	
	public static final int LARGURABORDA		= 5;
	
	public static final int PER_MIN_SIZE = 160;
	public static final int PER_MAX_SIZE = 600;
	
	private static final Color COLOR_ENABLED = Color.black;
	private static final Color COLOR_DISABLED = Color.darkGray;
	
	private String type = new String("MAPAOBJ");
	
	private int dragState		= DRAG_NOTHING;
	private int lastTrackVal	= 0;
	
	private boolean lUpdateRecord	= false;
	private boolean editEnabled		= false;
	
	private Point mousePressedLocation	= new Point();
	private Point pMousePressed			= new Point();
	
	private BscMapaLigacao ligacaoPopUp;
	private BscMapaLigacao ligacaoConnected;
	
	private HashMap idHashMap;
	private HashSet ligHashSet;
	
	private BscMapaJPanel dragSource;
	private BscMapaJPanel dragDestine;
	
	private JPanel jPanelVidro;
	private bsc.util.BscArrow2D virtualLine;
	
	private Color arrowColor = COLOR_DISABLED;
	private Color fcsColor = new java.awt.Color(255, 255, 229);
	
	private BscMapaFcs	fcsPopUp;
	private BscMapaFcs  MapaFcsPopUp;
	
	private bsc.util.BscToolKit oBscToolKit = new bsc.util.BscToolKit();
	
	public void enableFields() {
		// Habilita os campos do formul�rio.
		arrowColor = COLOR_ENABLED;
		jToolBarMapa.setVisible(true);
		editEnabled = true;
		
		//Configurando a drop down para apresentar o 1� item.
		jPanelVidro.repaint();
	}
	
	public void disableFields() {
		bsc.xml.BIXMLRecord ligacao;
		
		// Desabilita os campos do formul�rio.
		arrowColor = COLOR_DISABLED;
		jToolBarMapa.setVisible(false);
		editEnabled = false;

		repaint();
	}
	
	public void refreshFields( ) {
		// Copia os dados da vari�vel "record" para os campos do formul�rio.
		bsc.xml.BIXMLVector fatores, ligacoes, vctMapaProp, vctMapa;
		bsc.xml.BIXMLRecord fcs, ligacao, recMapaProp, objetivo;
		pnlHeader.setComponents(record.getBIXMLVector("CONTEXTOS"));
		
		BscMapaFcs mapaPrc;
		BscMapaFcs mapaObjetivo;
		BscMapaFcs mapaFcs = new BscMapaFcs();

		//jPanelMapa.removeAll();

		fldDataAlvo.setCalendar( record.getDate("DATAALVO") );
		if(record.getString("PARCELADA").equals("S"))
			fldAnalise.setText("Parcelada");
		else
			fldAnalise.setText("Acumulada");

		// Ligacoes
		idHashMap = new HashMap();
		ligHashSet = new HashSet();
		
		// Remove anteriores
		while(jPanelMapa.getComponentCount()>0){
			jPanelMapa.removeAll();
		}

		jLayeredPaneMapa.setNomeEstrategia( record.getString("NOME") );
		
		// Objetivos
		int sx = 1, sy = 1, x, y,height,width;
		objetivo = record.getBIXMLVector("OBJETIVOS").get(0);
		java.awt.Color objDefColor = new java.awt.Color(162,180,207);

		mapaObjetivo = new BscMapaFcs();
		mapaObjetivo.setId(objetivo.getString("ID"));
		mapaObjetivo.setNome(objetivo.getString("NOME"));
		mapaObjetivo.setDescricao(objetivo.getString("DESCRICAO"));
		mapaObjetivo.setFeedback(objetivo.getInt("FEEDBACK"));
		mapaObjetivo.setFcsType(0); //0=Objetivo , 1=FCS
		//sx = ((int) (this.getSize().getWidth() / 2))-80;
		sx = ((int) (948 / 2))-80;
		sy = 5;
		x = sx; y = sy;
		height = (objetivo.getInt("MAPHEIGHT") < 64)? 64 : objetivo.getInt("MAPHEIGHT");
		width = (objetivo.getInt("MAPWIDTH") < 160)? 160 : objetivo.getInt("MAPWIDTH");
		java.awt.Color  objColor = (objetivo.getInt("MAPCOLOR") == 0)? objDefColor : new java.awt.Color(objetivo.getInt("MAPCOLOR")) ;
		mapaObjetivo.setLocation(sx, sy);
		mapaObjetivo.addMouseListener(this);
		mapaObjetivo.addMouseMotionListener(this);
		mapaObjetivo.getJTextCorpo().addMouseListener(this);
		mapaObjetivo.getJTextCorpo().addMouseMotionListener(this);
		mapaObjetivo.setBounds(new Rectangle(sx, sy, width, height));
		mapaObjetivo.jPanelSuperior.setBackground(objColor);

		if(bsc.core.BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().
				equals(bsc.core.BscStaticReferences.getBscMainPanel().ST_AREATRABALHO)){
			if( objetivo.getInt("USEROWNER") == 0){
				mapaObjetivo.jPanelSuperior.setBackground(new java.awt.Color(102,102,255));
				mapaObjetivo.jTextCorpo.setSelectionColor(new java.awt.Color(102,102,255));
			} else{
				mapaObjetivo.jPanelSuperior.setBackground(new java.awt.Color(255,255,255));
			}
		}
		jPanelMapa.add(mapaObjetivo);

		idHashMap.put(new String(mapaObjetivo.getId()), mapaObjetivo);
		
		fatores = record.getBIXMLVector("FCSS");
		java.awt.Color fcsDefColor = new java.awt.Color(120,120,120);
		//java.awt.Color fcsTxtColor = new java.awt.Color(255,255,255);
		java.awt.Color prcColor = new java.awt.Color(162,180,207);

		for(int i=0; i<fatores.size(); i++) {
			sx += 10;
			sy += height;

			fcs = fatores.get(i);
			
			mapaFcs = new BscMapaFcs();
			mapaFcs.jPanelSuperior.setBackground(fcsDefColor);
			mapaFcs.setId(fcs.getString("ID"));
			mapaFcs.setNome(fcs.getString("NOME"));
			mapaFcs.setDescricao(fcs.getString("DESCRICAO"));
			mapaFcs.setOrdem(i);
			mapaFcs.setFeedback(fcs.getInt("FEEDBACK"));
			if (fcs.getInt("MAPCOLOR") == 0)
				mapaFcs.setFcsType(1, fcsColor.getRGB());
			else
				mapaFcs.setFcsType(1, fcs.getInt("MAPCOLOR") );

			sx = (fcs.getInt("MAPX")==-1)? sx : fcs.getInt("MAPX");
			sy = (fcs.getInt("MAPY")==-1)? sy : fcs.getInt("MAPY");

			mapaFcs.setPreferredSize(new Dimension(width, height));
			mapaFcs.addMouseListener(this);
			mapaFcs.addMouseMotionListener(this);
			mapaFcs.getJTextCorpo().addMouseListener(this);
			mapaFcs.getJTextCorpo().addMouseMotionListener(this);
			mapaFcs.setLocation(sx, sy);
			mapaFcs.setBounds(new Rectangle(sx, sy, width, height));
			idHashMap.put(new String(mapaFcs.getId()), mapaFcs);
			jPanelMapa.add(mapaFcs);
			jPanelMapa.setSize(jLayeredPaneMapa.getSize());
			
			//Painel de Legendas...
			if(bsc.core.BscStaticReferences.getBscMainPanel().bscTree.getBscTreeType().
					equals(bsc.core.BscStaticReferences.getBscMainPanel().ST_AREATRABALHO)){
				btnEdit.setEnabled(false);
				pnlLegenda.setVisible(true);
			}else{
				pnlLegenda.setVisible(false);
			}
			
			//Configuranado as propriedades do Mapa.
		
			// Ligacoes - ligHashSet
			try {
				BscMapaLigacao bscLigacao = new BscMapaLigacao(mapaObjetivo.getId(),
						fcs.getString("ID"));
				ligHashSet.add(bscLigacao);
				bscLigacao.setLigacaoType(0);
				bscLigacao.ctrl.setBounds(1,1,0,0);

			} catch(Exception e) {
				System.out.println("Erro... liga��o: " + e);
			}

		}
		validateMapa();
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		
		bsc.xml.BIXMLVector fatores, ligacoes, vctMapaProp;
		bsc.xml.BIXMLRecord fcs = null, objetivo, ligacao, recMapaProp;
		
		BscMapaFcs MapaFcs;
		BscMapaFcs mapaFcs;
		
		// Fatores Criticos de Sucesso
		fatores = new bsc.xml.BIXMLVector("FCSS", "FCS");
		for(int i=0; i<jPanelMapa.getComponentCount(); i++) {
			if(jPanelMapa.getComponent(i) instanceof BscMapaFcs){
				MapaFcs = (BscMapaFcs)jPanelMapa.getComponent(i);
				if(MapaFcs.getFcsType() == 1){ //FCS
					fcs = new bsc.xml.BIXMLRecord("FCS");
					fcs.set("ID", MapaFcs.getId());
					fcs.set("NOME", MapaFcs.getNome());
					fcs.set("DESCRICAO", MapaFcs.getDescricao());
					fcs.set("ORDEM", MapaFcs.getOrdem());
					fcs.set("MAPX", MapaFcs.getX());
					fcs.set("MAPY", MapaFcs.getY());
					if(fcsColor.getRGB() == MapaFcs.jPanelSuperior.getBackground().getRGB())
						fcs.set("MAPCOLOR", 0);
					else
						fcs.set("MAPCOLOR", MapaFcs.jPanelSuperior.getBackground().getRGB());
					fatores.add(fcs);
				}
			}
		}
		
		// Retorno
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set(fatores);

		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFcsPopMenu = new javax.swing.JPopupMenu();
        jMnuObjetivoColor = new javax.swing.JMenuItem();
        grpObjetivoColor = new javax.swing.ButtonGroup();
        grpEditMapa = new javax.swing.ButtonGroup();
        pnlTopForm = new javax.swing.JPanel();
        pnlHeader = new bsc.beans.JBIHeadPanel();
        jPanelRecord = new javax.swing.JPanel();
        jScrollPaneMapa = new javax.swing.JScrollPane();
        jLayeredPaneMapa = new bsc.beans.JBILayeredPane();
        jPanelMapa = new javax.swing.JPanel();
        jLabelCarregando = new javax.swing.JLabel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnReload1 = new javax.swing.JButton();
        jToolBarMapa = new javax.swing.JToolBar();
        btnSelecionar = new javax.swing.JToggleButton();
        jPanelDados = new javax.swing.JPanel();
        pnlLegenda = new javax.swing.JPanel();
        lblProprierarioCaixa = new javax.swing.JLabel();
        lblProprierarioTexto = new javax.swing.JLabel();
        lblRespAlheioCaixa = new javax.swing.JLabel();
        lblRespAlheioTexto = new javax.swing.JLabel();
        jPanelData = new javax.swing.JPanel();
        jPnlDates = new javax.swing.JPanel();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        pnlLayoutDiv = new javax.swing.JPanel();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        jMnuObjetivoColor.setText(bundle.getString("BscMapaFrame_00020")); // NOI18N
        jMnuObjetivoColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMnuObjetivoColorActionPerformed(evt);
            }
        });
        jFcsPopMenu.add(jMnuObjetivoColor);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle(bundle.getString("BscMapaFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlTopForm.setPreferredSize(new java.awt.Dimension(520, 45));
        pnlTopForm.setLayout(new java.awt.BorderLayout());

        pnlHeader.setMinimumSize(new java.awt.Dimension(520, 45));
        pnlTopForm.add(pnlHeader, java.awt.BorderLayout.NORTH);

        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);

        jPanelRecord.setLayout(new java.awt.BorderLayout());

        jScrollPaneMapa.setFocusCycleRoot(true);

        jPanelMapa.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanelMapa.setMaximumSize(new java.awt.Dimension(999, 999));
        jPanelMapa.setRequestFocusEnabled(false);
        jPanelMapa.setVerifyInputWhenFocusTarget(false);
        jPanelMapa.setOpaque(false);
        jPanelMapa.setLayout(null);

        jLabelCarregando.setText(bundle.getString("BscMapaFrame_00011")); // NOI18N
        jPanelMapa.add(jLabelCarregando);
        jLabelCarregando.setBounds(49, 2, 112, 14);

        jPanelMapa.setBounds(0, 0, 170, 20);
        jLayeredPaneMapa.add(jPanelMapa, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jScrollPaneMapa.setViewportView(jLayeredPaneMapa);

        jPanelRecord.add(jScrollPaneMapa, java.awt.BorderLayout.CENTER);

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setMinimumSize(new java.awt.Dimension(480, 27));
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 80));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setMaximumSize(new java.awt.Dimension(140, 27));
        pnlConfirmation.setMinimumSize(new java.awt.Dimension(100, 27));
        pnlConfirmation.setPreferredSize(new java.awt.Dimension(155, 30));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(130, 30));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnSave.setText(bundle.getString("BscMapaFrame_00002")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle.getString("BscMapaFrame_00008")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setMaximumSize(new java.awt.Dimension(400, 27));
        pnlOperation.setMinimumSize(new java.awt.Dimension(340, 27));
        pnlOperation.setPreferredSize(new java.awt.Dimension(400, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(220, 32));

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle.getString("BscMapaFrame_00006")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle.getString("BscMapaFrame_00007")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle.getString("BscMapaFrame_00009")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        btnExportar.setText(bundle.getString("BscMapaFrame_00026")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        btnReload1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_ajuda.gif"))); // NOI18N
        btnReload1.setText(bundle.getString("BscAjuda_00001")); // NOI18N
        btnReload1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload1.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload1.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload1.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReload1ActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload1);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.WEST);

        jToolBarMapa.setFloatable(false);
        jToolBarMapa.setMaximumSize(new java.awt.Dimension(300, 200));
        jToolBarMapa.setPreferredSize(new java.awt.Dimension(200, 30));

        grpEditMapa.add(btnSelecionar);
        btnSelecionar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSelecionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_setaselecionar.gif"))); // NOI18N
        btnSelecionar.setMnemonic('s');
        btnSelecionar.setToolTipText(bundle.getString("BscMapaFrame_00032")); // NOI18N
        btnSelecionar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSelecionar.setMaximumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setMinimumSize(new java.awt.Dimension(40, 20));
        btnSelecionar.setPreferredSize(new java.awt.Dimension(40, 20));
        btnSelecionar.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                btnSelecionarStateChanged(evt);
            }
        });
        btnSelecionar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnSelecionarItemStateChanged(evt);
            }
        });
        jToolBarMapa.add(btnSelecionar);

        pnlTools.add(jToolBarMapa, java.awt.BorderLayout.CENTER);

        jPanelDados.setPreferredSize(new java.awt.Dimension(10, 50));
        jPanelDados.setLayout(new java.awt.BorderLayout());

        pnlLegenda.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 0));

        lblProprierarioCaixa.setBackground(new java.awt.Color(255, 255, 255));
        lblProprierarioCaixa.setText("   ");
        lblProprierarioCaixa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblProprierarioCaixa.setOpaque(true);
        lblProprierarioCaixa.setPreferredSize(new java.awt.Dimension(11, 10));
        pnlLegenda.add(lblProprierarioCaixa);

        lblProprierarioTexto.setText(bundle.getString("BscMapaFrame_00013")); // NOI18N
        lblProprierarioTexto.setMaximumSize(new java.awt.Dimension(200, 16));
        lblProprierarioTexto.setMinimumSize(new java.awt.Dimension(200, 16));
        lblProprierarioTexto.setPreferredSize(new java.awt.Dimension(200, 16));
        pnlLegenda.add(lblProprierarioTexto);

        lblRespAlheioCaixa.setBackground(new java.awt.Color(102, 102, 255));
        lblRespAlheioCaixa.setText("   ");
        lblRespAlheioCaixa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblRespAlheioCaixa.setMaximumSize(new java.awt.Dimension(10, 18));
        lblRespAlheioCaixa.setPreferredSize(new java.awt.Dimension(11, 10));
        lblRespAlheioCaixa.setOpaque(true);
        pnlLegenda.add(lblRespAlheioCaixa);

        lblRespAlheioTexto.setText(bundle.getString("BscMapaFrame_00014")); // NOI18N
        lblRespAlheioTexto.setMaximumSize(new java.awt.Dimension(200, 16));
        lblRespAlheioTexto.setPreferredSize(new java.awt.Dimension(200, 16));
        pnlLegenda.add(lblRespAlheioTexto);

        jPanelDados.add(pnlLegenda, java.awt.BorderLayout.CENTER);

        jPanelData.setPreferredSize(new java.awt.Dimension(380, 30));
        jPanelData.setLayout(new java.awt.BorderLayout());

        jPnlDates.setMinimumSize(new java.awt.Dimension(250, 33));
        jPnlDates.setPreferredSize(new java.awt.Dimension(380, 30));

        lblDataAlvo.setText(bundle.getString("BscMapaFrame_00027")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(110, 15));
        lblDataAlvo.setRequestFocusEnabled(false);
        jPnlDates.add(lblDataAlvo);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        jPnlDates.add(fldDataAlvo);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setPreferredSize(new java.awt.Dimension(20, 20));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAvaliacao);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        jPnlDates.add(fldAnalise);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setPreferredSize(new java.awt.Dimension(20, 20));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAnalise);

        jPanelData.add(jPnlDates, java.awt.BorderLayout.WEST);

        jPanelDados.add(jPanelData, java.awt.BorderLayout.NORTH);

        pnlTools.add(jPanelDados, java.awt.BorderLayout.SOUTH);

        jPanelRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        pnlLayoutDiv.setMaximumSize(new java.awt.Dimension(952, 2147483647));
        pnlLayoutDiv.setOpaque(false);
        pnlLayoutDiv.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));
        jPanelRecord.add(pnlLayoutDiv, java.awt.BorderLayout.SOUTH);

        getContentPane().add(jPanelRecord, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	
	private void btnSelecionarStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_btnSelecionarStateChanged
		jPanelVidro.repaint();
	}//GEN-LAST:event_btnSelecionarStateChanged
				
    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = bsc.core.BscStaticReferences.getBscDialogSystem();
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		
		//Mensagem necess�ria para fazer o "split" do painel.
		if(dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00019")) ==
				javax.swing.JOptionPane.YES_OPTION){
			bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(),true);
			frmChooser.setDialogType(frmChooser.BSC_DIALOG_SAVE);
			frmChooser.setFileType("*.jpg");
			frmChooser.loadServerFiles("mapobj\\*.jpg");
			frmChooser.setFileName("mapa"+record.getString("ID") + ".jpg");
			frmChooser.show();
			if(frmChooser.isValidFile()){
				saveComponentAsJPEG("mapobj\\"+frmChooser.getFileName() );
			}
		}
		
    }//GEN-LAST:event_btnExportarActionPerformed
						
    private void jMnuObjetivoColorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMnuObjetivoColorActionPerformed
		Color c = JColorChooser.showDialog(this,java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00020"), fcsPopUp.jPanelSuperior.getBackground());
		try{
			fcsPopUp.jPanelSuperior.setBackground(new java.awt.Color(c.getRGB()));
		}catch(Exception e){}
    }//GEN-LAST:event_jMnuObjetivoColorActionPerformed
		
	private void btnSelecionarItemStateChanged (java.awt.event.ItemEvent evt)//GEN-FIRST:event_btnSelecionarItemStateChanged
	{//GEN-HEADEREND:event_btnSelecionarItemStateChanged
		javax.swing.JToggleButton btnMouse = (javax.swing.JToggleButton)evt.getItem();
	}//GEN-LAST:event_btnSelecionarItemStateChanged
		
	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
		bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
				new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
		bscPerformanceCard.show();
	}//GEN-LAST:event_btnAvaliacaoActionPerformed
	
    private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
		bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
				new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
		bscAnaliseParceladaCard.show();
    }//GEN-LAST:event_btnAnaliseActionPerformed
	
	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = bsc.core.BscStaticReferences.getBscDialogSystem();
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		
		//Mensagem necess�ria para fazer o "split" do painel.
		if(dialogSystem.confirmMessage(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaFrame_00019")) ==
				javax.swing.JOptionPane.YES_OPTION){
			
			printJob.setPrintable(this);
			if (printJob.printDialog()){
				try {
					printJob.print();
				} catch(java.awt.print.PrinterException pe) {
					System.out.println("Error printing: " + pe);
				}
			}
		}
		
	}//GEN-LAST:event_btnPrintActionPerformed
	
	private void btnReload1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReload1ActionPerformed
		event.loadHelp("FERRAMENTA");
	}//GEN-LAST:event_btnReload1ActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		event.loadRecord( );
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		event.saveRecord();
		lUpdateRecord = true;
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		if(lUpdateRecord){
			loadRecord();
			lUpdateRecord = false;
		}
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
		
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
		btnSelecionar.setSelected(true);
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnReload1;
    private javax.swing.JButton btnSave;
    private javax.swing.JToggleButton btnSelecionar;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private javax.swing.ButtonGroup grpEditMapa;
    private javax.swing.ButtonGroup grpObjetivoColor;
    private javax.swing.JPopupMenu jFcsPopMenu;
    private javax.swing.JLabel jLabelCarregando;
    private bsc.beans.JBILayeredPane jLayeredPaneMapa;
    private javax.swing.JMenuItem jMnuObjetivoColor;
    public static javax.swing.JPanel jPanelDados;
    private javax.swing.JPanel jPanelData;
    private javax.swing.JPanel jPanelMapa;
    private javax.swing.JPanel jPanelRecord;
    private javax.swing.JPanel jPnlDates;
    private javax.swing.JScrollPane jScrollPaneMapa;
    private javax.swing.JToolBar jToolBarMapa;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblProprierarioCaixa;
    private javax.swing.JLabel lblProprierarioTexto;
    private javax.swing.JLabel lblRespAlheioCaixa;
    private javax.swing.JLabel lblRespAlheioTexto;
    private javax.swing.JPanel pnlConfirmation;
    private bsc.beans.JBIHeadPanel pnlHeader;
    private javax.swing.JPanel pnlLayoutDiv;
    private javax.swing.JPanel pnlLegenda;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event =
			new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscMapaObjxFcs(int operation, String idAux, String contextId) {
		initComponents();

		pnlLegenda.setVisible(false);
		
		javax.swing.JScrollBar mapaEstHortBar = jScrollPaneMapa.getHorizontalScrollBar();
		
		mapaEstHortBar.setUnitIncrement(10);
		mapaEstHortBar.getModel().addChangeListener(this);

	
		// c�digo init separado
		jPanelVidro = new JPanel() {
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				
				// Inicia graphics 2d
				Graphics2D g2 = (Graphics2D) g;
				java.awt.Composite mapaComposite = g2.getComposite();
				g2.setComposite(mapaComposite);
				g2.setColor(arrowColor);
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, (editEnabled)?RenderingHints.VALUE_ANTIALIAS_OFF:RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				
				BscMapaJPanel origem, destino;
				Iterator it = ligHashSet.iterator();
				HashMap hashAdded = new HashMap();
				while(it.hasNext()) {
					BscMapaLigacao ligacao = (BscMapaLigacao)it.next();
					boolean itemAdded   = false;
					
					origem = (BscMapaJPanel)idHashMap.get(new String(ligacao.getSource()));
					destino = (BscMapaJPanel)idHashMap.get(new String(ligacao.getDestine()));
					String strChave = new String((String)ligacao.getSource().concat((String)ligacao.getDestine()));
					
					if(hashAdded.containsKey(strChave)){
						itemAdded   = true;;
					} else{
						hashAdded.put(new String(ligacao.getSource()).concat((String)ligacao.getDestine()),null);
					}
					
					// Consistencia
					if(origem == null || destino == null || itemAdded ) {
						it.remove();
						continue;
					}
					
					// Achar ponto 1
					Rectangle r1 = origem.getBounds();
					Point p1  = origem.getLocation();
					Point p11 = origem.getParent().getLocation();
					Point p12 = origem.getParent().getParent().getLocation();
					Point p13 = origem.getParent().getParent().getParent().getParent().getLocation();
					p13.y =  ( p13.y < 0 ? 0 : p13.y);
					p13.x =  ( p13.x < 0 ? 0 : p13.x);
					
					p1.translate(p11.x, p11.y);
					//p1.translate(p12.x, p12.y);
					//p1.translate(p13.x, p13.y);
					
					// Achar ponto 2
					Rectangle r2 = destino.getBounds();
					Point p2  = destino.getLocation();
					Point p21 = destino.getParent().getLocation();
					Point p22 = destino.getParent().getParent().getLocation();
					Point p23 = destino.getParent().getParent().getParent().getLocation();
					p23.y =  ( p23.y < 0 ? 0 : p23.y);
					p23.x =  ( p23.x < 0 ? 0 : p23.x);
					
					p2.translate(p21.x, p21.y);
					//p2.translate(p22.x, p22.y);
					//p2.translate(p23.x, p23.y);
					
					// Achar posicao 1
					int position;
					
					//Corre��o para a impress�o do tema;
					p1.y += origem.getParent().getParent().getParent().getLocation().y;
					p1.x += origem.getParent().getParent().getParent().getLocation().x;
					
					if(p1.y  > p2.y - r2.height && p1.y < p2.y + r2.height) {
						if(p1.x+(r1.width/1) > p2.x+(r2.width/2))
							position = DIREITA;
						else
							position = ESQUERDA;
					} else {
						if(p1.y+(r1.height /2) > p2.y+(r2.height /2))
							position = ABAIXO;
						else
							position = ACIMA;
					}
					
					// Posicao
					if(position == ACIMA) {
						p1.y = p1.y+r1.height;
						
						p1.x = p1.x+r1.width/2;
						p2.x = p2.x+r2.width/2;
						
					} else if(position == ABAIXO) {
						p2.y = p2.y+r2.height;
						
						p1.x = p1.x+r1.width/2;
						p2.x = p2.x+r2.width/2;
						
					} else if(position == ESQUERDA) {
						p1.x = p1.x+r1.width;
						
						p1.y = p1.y+r1.height/2;
						p2.y = p2.y+r2.height/2;
					} else if(position == DIREITA) {
						p2.x = p2.x+r2.width;
						
						p1.y = p1.y+r1.height/2;
						p2.y = p2.y+r2.height/2;
					}
					
					// Desenhar
					Color c = (ligacao.isSelected() ? Color.yellow : arrowColor);
					g2.setColor(c);
					g2.setPaint(c);
					
					if(ligacao.getArrow()==null){
						ligacao.setArrow(new bsc.util.BscArrow2D(p1,p2));
					}
					
					//Refaz o tipo da seta; pois na inicializa��o n�o existe o objeto "arrow"
					ligacao.setLigacaoType(ligacao.getLigacaoType());
					ligacao.getArrow().setLine(p1,p2,ligacao.ctrl);
					
					if(ligacao.getLigacaoType() == ligacao.LINE){
						ligacao.getArrow().render(g2);
					} else {
						ligacao.getArrow().render(g2,ligacao.ctrl,btnSelecionar.isSelected()&& editEnabled ,ligacao.isSelected());
					}
				}
				
				// Linha virtual
				if(virtualLine != null) {
					g2.setColor(Color.red);
					virtualLine.render(g2);
					g2.setColor(arrowColor);
				}
			}
		};
		
		jPanelVidro.setLayout(null);
		jPanelVidro.setOpaque(false);
		jLayeredPaneMapa.add(jPanelVidro, javax.swing.JLayeredPane.MODAL_LAYER);
		
		event.defaultConstructor( operation, idAux, contextId );
		
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		event.loadRecord();
	}

	public javax.swing.JTabbedPane getTabbedPane() {
		return null;
	}

	public void mouseClicked(java.awt.event.MouseEvent mouseEvent) {

		if(this.editEnabled) {
			if(mouseEvent.getSource() instanceof BscMapaObjetivo) {
				BscMapaFcs MapaFcs = (BscMapaFcs)mouseEvent.getSource();
				ligacaoPopUp = getMapaLigacao(mouseEvent);
				if(mouseEvent.getButton() == mouseEvent.BUTTON3){
					if(ligacaoPopUp == null){
						MapaFcsPopUp = MapaFcs;
					}
				}else {
					//Mouse dragged
					if(ligacaoPopUp != null && btnSelecionar.isSelected()){
						ligacaoPopUp.setSelected(!ligacaoPopUp.isSelected());
						jPanelVidro.repaint();
					}
					ligacaoPopUp = null;
				}
			} else if(mouseEvent.getSource() instanceof JTextPane) {
				JTextPane jText = (JTextPane)mouseEvent.getSource();
				BscMapaFcs mapaFcs = (BscMapaFcs) jText.getParent().getParent();
				if(mouseEvent.getButton() == mouseEvent.BUTTON3){
					ligacaoPopUp = getMapaLigacao(mouseEvent);
					if(ligacaoPopUp == null){
						fcsPopUp = mapaFcs;
						jFcsPopMenu.show(mouseEvent.getComponent(),mouseEvent.getX(), mouseEvent.getY());
					}
				}
			} else if(mouseEvent.getSource() instanceof BscMapaObjetivo) {
				if(mouseEvent.getButton() == mouseEvent.BUTTON3){
					ligacaoPopUp = getMapaLigacao(mouseEvent);
				}
			}
        }
	}
	
	public void mouseDragged(java.awt.event.MouseEvent mouseEvent) {
		//Faz o desenho do pontilhado para indicar onde ser� desenhado o tema
		Point pMouse = new Point(mouseEvent.getPoint());
		BscMapaFcs mapaPrc = null;
		
		if(mouseEvent.getSource() instanceof BscMapaFcs && dragState != DRAG_PER_MOVING){
			BscMapaFcs perspectivaAtual  = (BscMapaFcs)mouseEvent.getSource();
			Point pPerspOrigem = new Point(pMousePressed.x, pMousePressed.y);
			pPerspOrigem.translate(perspectivaAtual.getLocation().x, perspectivaAtual.getLocation().y);
			BscMapaFcs perspectivaOrigem = (BscMapaFcs)jPanelMapa.getComponentAt(pPerspOrigem.x, pPerspOrigem.y);
			jPanelVidro.repaint();
		}
		
		if(this.editEnabled) {
			if(mouseEvent.getSource() instanceof BscMapaFcs) {
				BscMapaFcs MapaFcs = (BscMapaFcs)mouseEvent.getSource();
				if(dragState == DRAG_PER_MOVING) {
					System.out.println("DRAG_PER_MOVING");

					Dimension d = MapaFcs.getSize();
					d.height = mouseEvent.getY() + 4;
					MapaFcs.setPreferredSize(new Dimension(d));
					MapaFcs.setMaxBorder(DRAG_RESIZE_DOWN);
					MapaFcs.validate();
					
					validateMapa();
				} else {
					if(ligacaoConnected != null){
						pMouse.translate(MapaFcs.getLocation().x, MapaFcs.getLocation().y);
						moveCtrlPoint(pMouse.x + mousePressedLocation.x, pMouse.y + mousePressedLocation.y);
					}
				}
			} else if(mouseEvent.getSource() instanceof JTextPane || mouseEvent.getSource() instanceof BscMapaFcs ) {
				BscMapaFcs mapaObjetivo;
				if(mouseEvent.getSource() instanceof BscMapaFcs){
					mapaObjetivo = (BscMapaFcs)mouseEvent.getSource();
				} else {
					JTextPane jText = (JTextPane)mouseEvent.getSource();
					mapaObjetivo = (BscMapaFcs) jText.getParent().getParent();
				}
				
				pMouse.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);
				
				if(ligacaoConnected != null){
					moveCtrlPoint(pMouse.x + mousePressedLocation.x, pMouse.y + mousePressedLocation.y);
				} else if(btnSelecionar.isSelected()){
					Point pLastLocation = new Point(mouseEvent.getX() - mousePressedLocation.x,mouseEvent.getY() - mousePressedLocation.y);
					mapaObjetivo.fcsResize(pMouse, dragState, pLastLocation,jPanelVidro);
				}

			}
		}
	}
	
	public void mouseEntered(java.awt.event.MouseEvent mouseEvent) {
		if(this.editEnabled) { 
			if(dragState == DRAG_NOTHING) {
				if(mouseEvent.getSource() instanceof BscMapaFcs){
					BscMapaFcs mapaObjetivo = (BscMapaFcs)mouseEvent.getSource();
					jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent,mapaObjetivo.getBounds()));

				}else if(mouseEvent.getSource() instanceof JTextPane){
					JTextPane jText = (JTextPane)mouseEvent.getSource();
					BscMapaFcs mapaObjetivo = (BscMapaFcs) jText.getParent().getParent();
					jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent,mapaObjetivo.getBounds()));
				}
			}
		} else {
			if(mouseEvent.getSource() instanceof JTextPane)
				jLayeredPaneMapa.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		}
	}
	
	public void mouseExited(java.awt.event.MouseEvent mouseEvent) {
		if(this.editEnabled) {
			if(dragState == DRAG_NOTHING) {
				if(mouseEvent.getSource() instanceof BscMapaFcs)
					jLayeredPaneMapa.setCursor(null);
				else if(mouseEvent.getSource() instanceof JTextPane)
					jLayeredPaneMapa.setCursor(null);
			}
		} else {
			if(mouseEvent.getSource() instanceof JTextPane)
				jLayeredPaneMapa.setCursor(null);
		}
	}
	
	public void mouseMoved(java.awt.event.MouseEvent mouseEvent) {
		if(this.editEnabled) {
			if(dragState == DRAG_NOTHING || dragState == DRAG_PER_MOVING) {
				if(mouseEvent.getSource() instanceof BscMapaFcs) {
					BscMapaFcs MapaFcs = (BscMapaFcs)mouseEvent.getSource();
					if(mouseEvent.getY() >= (MapaFcs.getSize().height - 5)) {
						dragState = DRAG_PER_MOVING;
						jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent,MapaFcs.getBounds()));
					} else {
						jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent,new Rectangle(0,0,0,0)));
						dragState = DRAG_NOTHING;
					}
				} else if (mouseEvent.getSource() instanceof BscMapaFcs || mouseEvent.getSource() instanceof JTextPane){
					BscMapaFcs mapaFcs;
					if(mouseEvent.getSource() instanceof JTextPane){
						JTextPane jText = (JTextPane)mouseEvent.getSource();
						mapaFcs = (BscMapaFcs) jText.getParent().getParent();
					} else {
						mapaFcs = (BscMapaFcs)mouseEvent.getSource();
					}
					jLayeredPaneMapa.setCursor(setMousePointer(mouseEvent,mapaFcs.getBounds()));
				}
			}
		}
		repaint();
	}
	
	public void mousePressed(java.awt.event.MouseEvent mouseEvent) {
		pMousePressed.setLocation(mouseEvent.getPoint());
		
		if(this.editEnabled) {
			if(mouseEvent.getSource() instanceof BscMapaFcs ) {
				if(dragState == DRAG_PER_MOVING){
					mousePressedLocation.setLocation(mouseEvent.getX(), mouseEvent.getY());
				}else{
					//Verificar se o usu�rio clicou sobre o quadrado de controle da linha.
					BscMapaFcs MapaFcs = (BscMapaFcs)mouseEvent.getSource();
					ligacaoConnected = getMapaLigacao(mouseEvent);
					if(ligacaoConnected != null){
						//Posiciona  o quadrado de controle onde doi clicado.
						Point p = new Point(mouseEvent.getPoint());
						p.translate(MapaFcs.getLocation().x, MapaFcs.getLocation().y);
						
						mousePressedLocation.setLocation(ligacaoConnected.ctrl.x - p.x,ligacaoConnected.ctrl.y - p.y);
						moveCtrlPoint(p.x + mousePressedLocation.x, p.y + mousePressedLocation.y);
					}
				}
				
			} else if(mouseEvent.getSource() instanceof BscMapaFcs || mouseEvent.getSource() instanceof JTextPane ) {
				BscMapaFcs mapaObjetivo;
				
				if(mouseEvent.getSource() instanceof BscMapaFcs){
					mapaObjetivo = (BscMapaFcs)mouseEvent.getSource();
				} else {
					JTextPane jText = (JTextPane)mouseEvent.getSource();
					mapaObjetivo = (BscMapaFcs) jText.getParent().getParent();
				}
				
				mousePressedLocation.setLocation(mouseEvent.getX(), mouseEvent.getY());
				
				if(btnSelecionar.isSelected()){
					ligacaoConnected = getMapaLigacao(mouseEvent);
					if(ligacaoConnected != null){
						Point p = new Point(mouseEvent.getPoint());
						p.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);
						
						mousePressedLocation.setLocation(ligacaoConnected.ctrl.x - p.x,ligacaoConnected.ctrl.y - p.y);
						moveCtrlPoint(p.x + mousePressedLocation.x, p.y + mousePressedLocation.y);
					} else {
						dragState = mapaObjetivo.setDragStatus(mouseEvent);
					}
				}
			}
		} else {
			if(mouseEvent.getSource() instanceof JTextPane) try {
				JTextPane jText = (JTextPane)mouseEvent.getSource();
				dragSource = (BscMapaFcs) jText.getParent().getParent();
				int tipo = 0;
				if(dragSource instanceof BscMapaFcs)
					tipo = ((BscMapaFcs) dragSource).getFcsType();
				if(tipo == 0){ //objetivo
					bsc.swing.BscDefaultFrameFunctions frame =
					bsc.core.BscStaticReferences.getBscFormController().getForm( "DRILL", ""+dragSource.getId(), dragSource.getNome() );
				}else{
					bsc.swing.BscDefaultFrameFunctions frame =
					bsc.core.BscStaticReferences.getBscFormController().getForm( "FCS", ""+dragSource.getId(), dragSource.getNome() );
				}
			} catch (bsc.core.BscFormControllerException exc ) {
				bsc.core.BscDebug.println(exc.getMessage());
			}
		}
	}
	
	public void mouseReleased(java.awt.event.MouseEvent mouseEvent) {
		if(this.editEnabled) {
			jLayeredPaneMapa.setCursor(null);
			if(dragState != DRAG_NOTHING) {
				if(mouseEvent.getSource() instanceof BscMapaFcs)
				if (mouseEvent.getSource() instanceof JTextPane )
				if(mouseEvent.getSource() instanceof BscMapaFcs || mouseEvent.getSource() instanceof JTextPane ) {
					BscMapaFcs mapaFcs;
					if(mouseEvent.getSource() instanceof BscMapaFcs){
						mapaFcs = (BscMapaFcs)mouseEvent.getSource();
					} else {
						JTextPane jText = (JTextPane)mouseEvent.getSource();
						mapaFcs = (BscMapaFcs) jText.getParent().getParent();
					}

					//BscMapaFcs mapaFcs = mapaFcs.getMapaFcs();
					//mapaFcs.objetivoShadow = false;

					//Corrige o posicionamento do objetivo se ele estiver fora do container.
					mapaFcs.setMaxMoviment();
				}

				dragState = DRAG_NOTHING;
				jPanelVidro.repaint();
				
			}
		}
		
		ligacaoConnected = null;
	}
	
	public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) throws java.awt.print.PrinterException {
		if (pageIndex > 0) {
			return(NO_SUCH_PAGE);
		} else {
			java.awt.Graphics2D g2d = (java.awt.Graphics2D)g;
			g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY()+50);
			g2d.drawString(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("JBILayeredPane_00001") + record.getString("NOME"), 0, 0 );
			g2d.scale( 0.5, 0.5 );
			disableDoubleBuffering(this);
			g2d.translate(0,10);
			bsc.swing.map.BscMapaFrame.jPanelDados.paint(g2d);
			g2d.translate(0,bsc.swing.map.BscMapaFrame.jPanelDados.getHeight());
			jLayeredPaneMapa.paint(g2d);
			enableDoubleBuffering(this);
			return(PAGE_EXISTS);
		}
	}
	
	/**
	 * Grava o objeto passado com uma figura do tipo JPEG
	 */
	public static void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
	
	public static void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
	
	//Evento de movimentar a barra de rolagem.
	public void stateChanged(javax.swing.event.ChangeEvent evt) {
		BoundedRangeModel sourceScroll = (BoundedRangeModel)evt.getSource();
		java.awt.FlowLayout pnlLayout = (java.awt.FlowLayout)pnlLayoutDiv.getLayout();
		
		pnlLayout.setHgap(pnlLayout.getHgap() + (lastTrackVal - sourceScroll.getValue()));
		lastTrackVal = sourceScroll.getValue();
		pnlLayoutDiv.doLayout();
	}
	
	public void moveBars(BscMapaJPanel origem,Point pMouse) {
		int posVerBar = jScrollPaneMapa.getVerticalScrollBar().getValue();
		int posHorBar = jScrollPaneMapa.getHorizontalScrollBar().getValue();
		
		Point p1  = pMouse;
		Point p11 = origem.getLocation();
		Point p12 = origem.getParent().getLocation();
		Point p13 = origem.getParent().getParent().getLocation();
		Point p14 = origem.getParent().getParent().getParent().getLocation();
		
		p14.y =  ( p14.y < 0 ? 0 : p14.y);
		p14.x =  ( p14.x < 0 ? 0 : p14.x);
		
		p1.translate(p11.x, p11.y);
		p1.translate(p12.x, p12.y);
		p1.translate(p13.x, p13.y);
		p1.translate(p14.x, p14.y);
		
		//Ajuste vertical
		if((p1.y - posVerBar) > jScrollPaneMapa.getHeight() ){
			jScrollPaneMapa.getVerticalScrollBar().setValue(jScrollPaneMapa.getVerticalScrollBar().getValue()+10);
		}
		if((p1.y - posVerBar) <  0){
			jScrollPaneMapa.getVerticalScrollBar().setValue(jScrollPaneMapa.getVerticalScrollBar().getValue()-10);
		}
		
		//Ajuste horizontal
		if(((p1.x + 30) - posHorBar) > jScrollPaneMapa.getWidth() ){
			jScrollPaneMapa.getHorizontalScrollBar().setValue(jScrollPaneMapa.getHorizontalScrollBar().getValue()+10);
		}
		if(((p1.x + 30)- posHorBar) <  0){
			jScrollPaneMapa.getHorizontalScrollBar().setValue(jScrollPaneMapa.getHorizontalScrollBar().getValue()-10);
		}
	}
	
	//Controla o movimento maximo do ponto de controle e faz o movimento.
	private void moveCtrlPoint(int x, int y){
		
		x = (x < 10 ? 10: x);
		y = (y < 10 ? 10: y);
		x = (x < (jLayeredPaneMapa.getWidth()-30) ?  x : (jLayeredPaneMapa.getWidth()-30));
		y = (y < (jLayeredPaneMapa.getHeight()-10) ? y : (jLayeredPaneMapa.getHeight()-10));
		
		ligacaoConnected.ctrl.setLocation(x,y);
		jPanelVidro.repaint();
	}
	
	private void saveComponentAsJPEG(String fileName) {
		Dimension size = jLayeredPaneMapa.getSize();
		java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height ,
				java.awt.image.BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = myImage.createGraphics();
		jLayeredPaneMapa.paint(g2);
		//Grava a figura l� no servidor... :)
		try {
			java.io.ByteArrayOutputStream out = new  java.io.ByteArrayOutputStream();
			//com.sun.image.codec.jpeg.JPEGImageEncoder encoder = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(out);
			//encoder.encode(myImage);
                        //Salva o arquivo de imagem como JPEG
                        ImageIO.write(myImage, "jpg", out);
			String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
			event.saveBase64(fileName, strBase64);
			out.close();
			//Abre o navegador para salvar a figura localmente
			bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
			java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString()+fileName);
			bscApplet.getAppletContext().showDocument(url,"_self");
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Configura o ponteiro do mouse.
	 * @param  MouseEvent evento do mouse
	 * @param objRectangle Retangulo que representa o objeto.
	 * @return Retorna o ponteiro.
	 */
	private Cursor setMousePointer(MouseEvent mouseEvent,Rectangle objRectangle){
		Rectangle borRectangle= new Rectangle(0,0,0,0);
		
		return setMousePointer(mouseEvent,objRectangle,borRectangle);
	}
	
	/**
	 * Configura o ponteiro do mouse.
	 * @param  MouseEvent evento do mouse
	 * @param objRectangle Retangulo que representa o objeto.
	 * @param borRectangle Borda do objeto.
	 * @return Retorna o ponteiro.
	 */
	private Cursor setMousePointer(MouseEvent mouseEvent,Rectangle objRectangle,Rectangle borRectangle){
		
		Cursor pointerFcs = null;
		Point pMouse = mouseEvent.getPoint();
		
		if(btnSelecionar.isSelected()){
			BscMapaLigacao mapaLigacao = getMapaLigacao(mouseEvent);
			if(mapaLigacao != null){
				return new Cursor(Cursor.HAND_CURSOR);
			}else if(! (mouseEvent.getSource() instanceof JTextPane)){
				Rectangle rLado = null;
				for(int iLado = 4; iLado <= 7; iLado++){
					if(iLado == DRAG_RESIZE_LEFT){
						rLado = new Rectangle(0,0, LARGURABORDA , objRectangle.height);
					} else if(iLado == DRAG_RESIZE_UP){
						rLado = new Rectangle(0,0,objRectangle.width , LARGURABORDA);
					} else if(iLado == DRAG_RESIZE_RIGHT){
						rLado = new Rectangle(objRectangle.width - LARGURABORDA , 0, objRectangle.height , objRectangle.height);
					} else if(iLado == DRAG_RESIZE_DOWN){
						rLado = new Rectangle(0,objRectangle.height - LARGURABORDA, objRectangle.width , objRectangle.height);
					}
					
					if (rLado.contains(pMouse)) {
						if(iLado == DRAG_RESIZE_LEFT || iLado == DRAG_RESIZE_RIGHT){
							return new Cursor(Cursor.E_RESIZE_CURSOR);
						} else if(iLado == DRAG_RESIZE_DOWN || iLado == DRAG_RESIZE_UP){
							return new Cursor(Cursor.N_RESIZE_CURSOR);
						}
					}
				}
			}
			
			if(mouseEvent.getSource() instanceof BscMapaFcs){
				pointerFcs = new Cursor(Cursor.DEFAULT_CURSOR);
			}else {
				pointerFcs = new Cursor(Cursor.MOVE_CURSOR);
			}
		}
		
		return pointerFcs;
	}
	
	private Rectangle getBorderBounds(BscMapaJPanel origem){
		javax.swing.border.TitledBorder bordaTema = (javax.swing.border.TitledBorder)origem.getBorder();
		
		//Lendo o tamanho do texto em pixels;
		java.awt.Graphics2D gTexto = (java.awt.Graphics2D)origem.getGraphics();
		gTexto.setFont(bordaTema.getTitleFont());
		FontMetrics metrics = gTexto.getFontMetrics();
		int width = metrics.stringWidth(bordaTema.getTitle());
		int height = metrics.getHeight();
		int titX = 0;
		
		if(bordaTema.getTitleJustification() == bordaTema.CENTER){
			titX = (int)(origem.getWidth() - width) / 2;
		} else if(bordaTema.getTitleJustification() == bordaTema.RIGHT){
			titX = origem.getWidth() - width;
		}
		
		return new Rectangle(titX,0,width,height);
	}
	
	void validateMapa() {
		int totalHeight = 0, height = 0;
		for(int i=0; i<jPanelMapa.getComponentCount(); i++) {
			BscMapaJPanel item = (BscMapaJPanel)jPanelMapa.getComponent(i);
			if(i==0) item.getComponent(0).requestFocusInWindow();
			Dimension d = item.getPreferredSize();

			d.height = (d.height < PER_MIN_SIZE)? PER_MIN_SIZE : d.height;
			d.height = (d.height > PER_MAX_SIZE)? PER_MAX_SIZE : d.height;

			item.setPreferredSize(d);
			totalHeight += d.height;
		}
		
		jPanelMapa.setBounds(0,0,948, totalHeight);
		jPanelMapa.setPreferredSize(new Dimension(948, totalHeight));
		jPanelMapa.revalidate();
		jPanelVidro.setBounds(0, 0, 948, totalHeight);
		jPanelVidro.setPreferredSize(new Dimension(948, totalHeight));
		jPanelVidro.revalidate();
		jLayeredPaneMapa.setPreferredSize(new Dimension(948, totalHeight));
		jLabelCarregando.setVisible(false);
		repaint();
	}
	
	//Faz a valida��o quando um obetivo � movido para dentro de um mapa,
	//para n�o permitir a conex�o de um obetivo ao tema ao qual ele est� contido.
	private void validMapConnection(BscMapaJPanel mapaTema){
		BscMapaJPanel origem    = null;
		BscMapaJPanel destino   = null;
		BscMapaLigacao ligacao  = null;
		String origemId	= null;
		String destinoId	= null;
		String objetivoId	= null;
		
		//Lendo todos os objetivos dentro do tema.
		for(int objetivoItem = 0; objetivoItem < jPanelMapa.getComponentCount();objetivoItem++){
			if(mapaTema.getComponent(objetivoItem) instanceof BscMapaFcs){
				BscMapaFcs objetivo = (BscMapaFcs)jPanelMapa.getComponent(objetivoItem);
				objetivoId=objetivo.getId();
				//Verifica a conex�o para este objetivo, n�o pode ser a mesma do mapa.
				Iterator it = ligHashSet.iterator();
				while(it.hasNext()) {
					ligacao	    = (BscMapaLigacao)it.next();
					origem	    = (BscMapaJPanel)idHashMap.get(new String(ligacao.getSource()));
					destino	    = (BscMapaJPanel)idHashMap.get(new String(ligacao.getDestine()));
					try{
						origemId    = origem.getId();
						destinoId   = destino.getId();
					}catch (Exception e){
						System.out.println("ID n�o dispon�vel para valida��o: " + e);
					}
				}
			}
		}
	}
	
	//Retorna a ligacao do mapa quando est� dentro de uma perspectiva
	private BscMapaLigacao getMapaLigacao(MouseEvent mouseEvent){
		BscMapaLigacao ligacao;
		Point p = translatePointToClass(mouseEvent.getSource(), mouseEvent.getPoint());
		
		Iterator it = ligHashSet.iterator();
		
		while(it.hasNext()) {
			ligacao = (BscMapaLigacao)it.next();
			
			if(ligacao.getLigacaoType() == ligacao.QUADCURVE){
				Rectangle recRef = new Rectangle(ligacao.ctrl.x-4,ligacao.ctrl.y-4,ligacao.ctrl.width+4,ligacao.ctrl.height+4);
				if(recRef.contains(p) && btnSelecionar.isSelected())
					return ligacao;
			}else if(ligacao.getArrow().ptSegDist(p) < 2){
				return ligacao;
			}
		}
		
		return null;
	}
	
	private Point translatePointToClass(Object objOrigem, Point pOrigem){
		
		if(objOrigem instanceof BscMapaFcs){
			BscMapaFcs MapaFcs = (BscMapaFcs)objOrigem;
			pOrigem.translate(MapaFcs.getX(), MapaFcs.getY());
			
		} else if (objOrigem instanceof BscMapaFcs || objOrigem  instanceof JTextPane ) {
			BscMapaFcs mapaObjetivo;
			
			if(objOrigem instanceof BscMapaFcs){
				mapaObjetivo = (BscMapaFcs)objOrigem;
			} else {
				JTextPane jText = (JTextPane)objOrigem;
				mapaObjetivo = (BscMapaFcs) jText.getParent().getParent();
				pOrigem.translate(jText.getLocation().x, jText.getLocation().y);
			}
			
			pOrigem.translate(mapaObjetivo.getLocation().x, mapaObjetivo.getLocation().y);
			pOrigem.translate(mapaObjetivo.getParent().getLocation().x,mapaObjetivo.getParent().getLocation().y);
			pOrigem.translate(mapaObjetivo.getParent().getParent().getLocation().x,mapaObjetivo.getParent().getParent().getLocation().y);
			
		} else {
			pOrigem.setLocation(-10,-10);
		}
		
		return pOrigem;
	}
	
	private void drawVirtualLine(MouseEvent mouseEvent){
		JTextPane jText		= null;//Indica se a origem � um objetivo.
		
		if(mouseEvent.getSource() instanceof JTextPane){
			jText = (JTextPane)mouseEvent.getSource();
		}
		
		//Verifica se � necessario a movimenta��o da barra vertical.
		Point pMouse = new Point(mouseEvent.getX(),mouseEvent.getY());
		moveBars(dragSource, pMouse);
		
		if(getCursor().getType()!= Cursor.CROSSHAIR_CURSOR)
			jLayeredPaneMapa.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		
		// Desenhar reta virtual
		Point p1 = new Point(mousePressedLocation);
		Point pLevel0 = new Point(0,0);
		if(jText != null)
			pLevel0 = jText.getLocation();
		
		Point pLevel1 = dragSource.getLocation();
		Point pLevel2 = dragSource.getParent().getLocation();
		Point pLevel3 = dragSource.getParent().getParent().getLocation();
		Point pLevel4 = new Point(0,0);
		
		p1.translate(pLevel0.x, pLevel0.y);
		p1.translate(pLevel1.x, pLevel1.y);
		p1.translate(pLevel2.x, pLevel2.y);
		p1.translate(pLevel3.x, pLevel3.y);
		p1.translate(pLevel4.x, pLevel4.y);
		
		Point p2 = new Point(mouseEvent.getX(), mouseEvent.getY());
		
		p2.translate(pLevel0.x, pLevel0.y);
		p2.translate(pLevel1.x, pLevel1.y);
		p2.translate(pLevel2.x, pLevel2.y);
		p2.translate(pLevel3.x, pLevel3.y);
		p2.translate(pLevel4.x, pLevel4.y);
		
		virtualLine = new bsc.util.BscArrow2D(p1, p2);
		jPanelVidro.repaint();
	}
	
}
