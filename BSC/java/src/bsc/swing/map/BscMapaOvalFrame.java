package bsc.swing.map;

import bsc.applet.BscMainPanel;
import bsc.xml.BIXMLRecord;
import bsc.xml.BIXMLVector;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.Color;
import javax.swing.RepaintManager;
import javax.imageio.ImageIO;

public class BscMapaOvalFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions, java.awt.print.Printable{
	/***************************************************************************/
	// FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
	/***************************************************************************/
	private String type = new String("MAPAEST2");
	private BscMapaEstrategicoOval mapaEstrategico = null;
	private boolean editing = false;
	private boolean updatePersp = true;
	private boolean maximize = true;
	
	private Color[] perspCores = {	new java.awt.Color(-11638654),
	new java.awt.Color(-10066177),
	new java.awt.Color(-16750900),
	new java.awt.Color(-11638654)
	};
	private int divPos = 0;
	private bsc.applet.BscMainPanel mainPanel = bsc.core.BscStaticReferences.getBscMainPanel();
	
	public void enableFields() {
		updatePersp = false;
		setEditing(true);
	}
	
	public void disableFields() {
		setEditing(false);
	}
	
	public void refreshFields( ) {
		this.maximizable = true;
        if(updatePersp){
			//Corrigindo o tamanho do mapa estrat�gico.
			mapaEstrategico.setSize(getSize());
			pnlData.remove(pnlCarregando);
			pnlData.doLayout();
			iniEstrategias();
		}
	}
	
	public bsc.xml.BIXMLRecord getFieldsContents( ) {
		bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
		recordAux.set( "ID", id );
		if ( record.contains("PARENTID") )
			recordAux.set( "PARENTID", record.getString("PARENTID") );
		if ( record.contains("CONTEXTID") )
			recordAux.set( "CONTEXTID", record.getString("CONTEXTID") );
		
		bsc.xml.BIXMLVector perspectivas = new bsc.xml.BIXMLVector("PERSPECTIVAS", "PERSPECTIVA");;
		
		// Perspectivas
		java.util.Vector<BscPerspectivaOval> vctPerspectiva = mapaEstrategico.getVctPerspectivas();
		for(java.util.Iterator<BscPerspectivaOval> itePers = vctPerspectiva.iterator(); itePers.hasNext();){
			BscPerspectivaOval  persp = itePers.next();
			bsc.xml.BIXMLRecord recPerspectiva = new bsc.xml.BIXMLRecord("PERSPECTIVA");
			java.awt.Font fontPersp = persp.getFontTitulo();
			recPerspectiva.set("ID",persp.getID());
			recPerspectiva.set("BACKCOLOR"	, persp.getBackground().getRGB());
			recPerspectiva.set("MP2DEGRADE"	, persp.isDegrade()?"T":"F");
			recPerspectiva.set("MP2HEIGHT"	, persp.getHeight());
			recPerspectiva.set("MP2WIDTH"	, persp.getWidth());
			recPerspectiva.set("MP2X"		, persp.getX());
			recPerspectiva.set("MP2Y"		, persp.getY());
			recPerspectiva.set("MP2TITCOR"	, persp.getTituloCor().getRGB());
			recPerspectiva.set("MP2FONTE"	, fontPersp.getName());//Fonte do titulo
			recPerspectiva.set("MP2FONTAM"	, fontPersp.getSize());//Tamanho da fonte
			recPerspectiva.set("MP2FONEST"	, fontPersp.getStyle());//Estilo da fonte
            recPerspectiva.set("ZERACUSTON"	, "F");//Estilo da fonte
			
			//Adicionando objetivos
			bsc.xml.BIXMLVector objetivos = new bsc.xml.BIXMLVector("OBJETIVOS", "OBJETIVO");
			java.util.Vector<BscMapaObjetivoOval> vctObjetivos = persp.getVctObjetivos();
			for(java.util.Iterator<BscMapaObjetivoOval> iteObjetivos = vctObjetivos.iterator(); iteObjetivos.hasNext();){
				BscMapaObjetivoOval objetivo = iteObjetivos.next();
				bsc.xml.BIXMLRecord recObj = new bsc.xml.BIXMLRecord("OBJETIVO");
				java.awt.Font fonteObj = objetivo.getTextFont();
				recObj.set("ID"			, objetivo.getId());
				recObj.set("MP2X"		, objetivo.getX());
				recObj.set("MP2Y"		, objetivo.getY());
				recObj.set("MP2WIDTH"	, objetivo.getWidth());
				recObj.set("MP2HEIGHT"	, objetivo.getHeight());
				recObj.set("MP2FONCOR"	, objetivo.getTextColor().getRGB());//Fonte do cor.
				recObj.set("MP2FONTE"	, fonteObj.getName());//Fonte do titulo
				recObj.set("MP2FONTAM"	, fonteObj.getSize());//Tamanho da fonte
				recObj.set("MP2FONEST"	, fonteObj.getStyle());//Estilo da fonte
				objetivos.add(recObj);
			}
			recPerspectiva.set(objetivos);
			perspectivas.add(recPerspectiva);
		}
		recordAux.set(perspectivas);
		return recordAux;
	}
	
	public boolean validateFields(StringBuffer errorMessage) {
		return true;
	}
	
	public boolean hasCombos() {
		return false;
	}
	
	/*****************************************************************************/
	/*****************************************************************************/
	
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlRecord = new javax.swing.JPanel();
        pnlTools = new javax.swing.JPanel();
        pnlConfirmation = new javax.swing.JPanel();
        tbConfirmation = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlOperation = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnReload = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnAbrir = new javax.swing.JButton();
        jPnlDates = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        fldAnalise = new pv.jfcx.JPVEdit();
        btnAnalise = new javax.swing.JButton();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        pnlCarregando = new javax.swing.JPanel();
        lblCarregando = new javax.swing.JLabel();

        setClosable(true);
        setForeground(java.awt.Color.white);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscMapaOvalFrame_00001")); // NOI18N
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_mapa.gif"))); // NOI18N
        setMinimumSize(new java.awt.Dimension(800, 800));
        setPreferredSize(new java.awt.Dimension(102, 39));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlTools.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTools.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTools.setLayout(new java.awt.BorderLayout());

        pnlConfirmation.setPreferredSize(new java.awt.Dimension(160, 27));
        pnlConfirmation.setLayout(new java.awt.BorderLayout());

        tbConfirmation.setFloatable(false);
        tbConfirmation.setRollover(true);
        tbConfirmation.setPreferredSize(new java.awt.Dimension(150, 32));

        btnSave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_gravar.gif"))); // NOI18N
        java.util.ResourceBundle bundle1 = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        btnSave.setText(bundle1.getString("BscBaseFrame_00001")); // NOI18N
        btnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnSave.setMaximumSize(new java.awt.Dimension(76, 20));
        btnSave.setMinimumSize(new java.awt.Dimension(76, 20));
        btnSave.setPreferredSize(new java.awt.Dimension(76, 20));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnSave);

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_cancelar.gif"))); // NOI18N
        btnCancel.setText(bundle1.getString("BscBaseFrame_00002")); // NOI18N
        btnCancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnCancel.setMaximumSize(new java.awt.Dimension(76, 20));
        btnCancel.setMinimumSize(new java.awt.Dimension(76, 20));
        btnCancel.setPreferredSize(new java.awt.Dimension(76, 20));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        tbConfirmation.add(btnCancel);

        pnlConfirmation.add(tbConfirmation, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlConfirmation, java.awt.BorderLayout.EAST);

        pnlOperation.setPreferredSize(new java.awt.Dimension(697, 30));
        pnlOperation.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);

        btnEdit.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_editar.gif"))); // NOI18N
        btnEdit.setText(bundle1.getString("BscBaseFrame_00003")); // NOI18N
        btnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnEdit.setMaximumSize(new java.awt.Dimension(76, 20));
        btnEdit.setMinimumSize(new java.awt.Dimension(76, 20));
        btnEdit.setPreferredSize(new java.awt.Dimension(76, 20));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        tbInsertion.add(btnEdit);

        btnReload.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnReload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_atualizar.gif"))); // NOI18N
        btnReload.setText(bundle1.getString("BscBaseFrame_00005")); // NOI18N
        btnReload.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnReload.setMaximumSize(new java.awt.Dimension(76, 20));
        btnReload.setMinimumSize(new java.awt.Dimension(76, 20));
        btnReload.setPreferredSize(new java.awt.Dimension(76, 20));
        btnReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReloadActionPerformed(evt);
            }
        });
        tbInsertion.add(btnReload);

        btnPrint.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_print.gif"))); // NOI18N
        btnPrint.setText(bundle1.getString("BscMapaOvalFrame_00002")); // NOI18N
        btnPrint.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnPrint.setMaximumSize(new java.awt.Dimension(76, 20));
        btnPrint.setMinimumSize(new java.awt.Dimension(76, 20));
        btnPrint.setPreferredSize(new java.awt.Dimension(76, 20));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        tbInsertion.add(btnPrint);

        btnExportar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_exportacao.gif"))); // NOI18N
        btnExportar.setText(bundle1.getString("BscMapaOvalFrame_00003")); // NOI18N
        btnExportar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnExportar.setMaximumSize(new java.awt.Dimension(76, 20));
        btnExportar.setMinimumSize(new java.awt.Dimension(76, 20));
        btnExportar.setPreferredSize(new java.awt.Dimension(76, 20));
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });
        tbInsertion.add(btnExportar);

        btnAbrir.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_download.gif"))); // NOI18N
        btnAbrir.setText(bundle.getString("BscMapaOvalFrame_00004")); // NOI18N
        btnAbrir.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnAbrir.setMaximumSize(new java.awt.Dimension(76, 20));
        btnAbrir.setMinimumSize(new java.awt.Dimension(76, 20));
        btnAbrir.setPreferredSize(new java.awt.Dimension(76, 20));
        btnAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirActionPerformed(evt);
            }
        });
        tbInsertion.add(btnAbrir);

        jPnlDates.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 2, 2));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setPreferredSize(new java.awt.Dimension(1, 20));
        jPnlDates.add(jSeparator1);

        lblDataAlvo.setText(bundle1.getString("BscMapaOvalFrame_00005")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(50, 15));
        lblDataAlvo.setRequestFocusEnabled(false);
        jPnlDates.add(lblDataAlvo);

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        jPnlDates.add(fldDataAlvo);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.setPreferredSize(new java.awt.Dimension(25, 22));
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAvaliacao);

        fldAnalise.setEditable(false);
        fldAnalise.setMaxLength(60);
        jPnlDates.add(fldAnalise);

        btnAnalise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAnalise.setPreferredSize(new java.awt.Dimension(25, 22));
        btnAnalise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnaliseActionPerformed(evt);
            }
        });
        jPnlDates.add(btnAnalise);

        tbInsertion.add(jPnlDates);

        pnlOperation.add(tbInsertion, java.awt.BorderLayout.CENTER);

        pnlTools.add(pnlOperation, java.awt.BorderLayout.CENTER);

        pnlRecord.add(pnlTools, java.awt.BorderLayout.NORTH);

        scpRecord.setBorder(null);
        scpRecord.setAutoscrolls(true);

        pnlData.setPreferredSize(new java.awt.Dimension(900, 900));
        pnlData.setLayout(new java.awt.BorderLayout());

        pnlCarregando.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 6));

        lblCarregando.setText(bundle1.getString("BscMapaFrame_00011")); // NOI18N
        pnlCarregando.add(lblCarregando);

        pnlData.add(pnlCarregando, java.awt.BorderLayout.NORTH);

        scpRecord.setViewportView(pnlData);

        pnlRecord.add(scpRecord, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnlRecord, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
	
	private void btnAnaliseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnaliseActionPerformed
		updatePersp = true;
		bsc.swing.analisys.BscAnaliseParceladaCardFrame bscAnaliseParceladaCard =
				new bsc.swing.analisys.BscAnaliseParceladaCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscAnaliseParceladaCard.setParcelada(record.getBoolean("PARCELADA"));
		bscAnaliseParceladaCard.setVisible(true);
	}//GEN-LAST:event_btnAnaliseActionPerformed
	
	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
		updatePersp = true;
		bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
				new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(),true, this);
		bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
		bscPerformanceCard.setVisible(true);
	}//GEN-LAST:event_btnAvaliacaoActionPerformed
	
	private void btnReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReloadActionPerformed
		lblCarregando.setVisible(true);
		pnlData.remove(mapaEstrategico);
		initMapaEstrategico();
		if(updatePersp){
			event.loadRecord();
		}else{
			mapaEstrategico.setSize(pnlData.getSize());
			iniEstrategias();
		}
		repaint();
		lblCarregando.setVisible(false);
	}//GEN-LAST:event_btnReloadActionPerformed
	
	private void btnAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirActionPerformed
		bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(),true);
		
		frmChooser.setDialogType(frmChooser.BSC_DIALOG_OPEN);
		frmChooser.setFileType("*.jpg");
		frmChooser.loadServerFiles("mapest\\*.jpg");
		
		frmChooser.setVisible(true);
		if(frmChooser.isValidFile()){
			try {
				bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
				String strFileName = new String( "mapest\\" +frmChooser.getFileName());
				java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString()+strFileName);
				bscApplet.getAppletContext().showDocument(url,"_blank");
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}//GEN-LAST:event_btnAbrirActionPerformed
	
	private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		
		doMaximizeForm();
		
		//Mensagem necess�ria para fazer o "split" do painel.
		bsc.swing.BscFileChooser frmChooser = new bsc.swing.BscFileChooser(bsc.core.BscStaticReferences.getBscMainPanel(),true);
		frmChooser.setDialogType(frmChooser.BSC_DIALOG_SAVE);
		frmChooser.setFileType("*.jpg");
		frmChooser.loadServerFiles("mapest\\*.jpg");
		frmChooser.setFileName("mapa"+record.getString("ID") + ".jpg");
		frmChooser.setVisible(true);
		if(frmChooser.isValidFile()){
			saveComponentAsJPEG("mapest\\"+frmChooser.getFileName() );
		}
		
		if(mainPanel.bscTree.getBscTreeType() == mainPanel.ST_AREATRABALHO){
			mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(divPos);
		}else{
			mainPanel.jSplitPaneOrganizacao.setDividerLocation(divPos);
		}
	}//GEN-LAST:event_btnExportarActionPerformed
	
	private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
		bsc.swing.BscDefaultDialogSystem dialogSystem = new bsc.swing.BscDefaultDialogSystem(this);
		java.awt.print.PrinterJob printJob = java.awt.print.PrinterJob.getPrinterJob();
		java.awt.print.PageFormat page = new java.awt.print.PageFormat();
		java.awt.print.Paper paper = new java.awt.print.Paper();
		
		page.setOrientation(java.awt.print.PageFormat.LANDSCAPE);
		paper.setSize(27,21);
		paper.setImageableArea(10,0,28,20);
		page.setPaper(paper);
		doMaximizeForm();
		printJob.setPrintable(this);
		
		printJob.pageDialog(page);
		if (printJob.printDialog()){
			try {
				printJob.print();
			} catch(java.awt.print.PrinterException pe) {
				System.out.println("Error printing: " + pe);
			}
		}
		
		if(mainPanel.bscTree.getBscTreeType() == BscMainPanel.ST_AREATRABALHO){
			mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(divPos);
		}else{
			mainPanel.jSplitPaneOrganizacao.setDividerLocation(divPos);
		}
	}//GEN-LAST:event_btnPrintActionPerformed
	
	private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
		if(mainPanel.bscTree.getBscTreeType() == BscMainPanel.ST_AREATRABALHO){
			mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(divPos);
		}else{
			mainPanel.jSplitPaneOrganizacao.setDividerLocation(divPos);
		}
	}//GEN-LAST:event_formInternalFrameClosing
	
	private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		updatePersp = true;
		event.saveRecord();
	}//GEN-LAST:event_btnSaveActionPerformed
	
	private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		event.cancelOperation();
	}//GEN-LAST:event_btnCancelActionPerformed
	
	private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
		event.editRecord();
	}//GEN-LAST:event_btnEditActionPerformed
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrir;
    private javax.swing.JButton btnAnalise;
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnReload;
    private javax.swing.JButton btnSave;
    private pv.jfcx.JPVEdit fldAnalise;
    private pv.jfcx.JPVDate fldDataAlvo;
    private javax.swing.JPanel jPnlDates;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblCarregando;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JPanel pnlCarregando;
    private javax.swing.JPanel pnlConfirmation;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlOperation;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlTools;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JToolBar tbConfirmation;
    private javax.swing.JToolBar tbInsertion;
    // End of variables declaration//GEN-END:variables
	
	private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior( this );
	private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
	private String id = new String();
	private bsc.xml.BIXMLRecord record = null;
	
	public BscMapaOvalFrame(){
		initComponents();
	}
	
	private void onBarsStateChanged(javax.swing.event.ChangeEvent evt) {
		repaint();
	}
	
	public BscMapaOvalFrame(int operation, String idAux, String contextId) {
		initComponents();
		event.defaultConstructor( operation, idAux, contextId );
		initMapaEstrategico();
	}
	
	private void initMapaEstrategico(){
		mapaEstrategico = new BscMapaEstrategicoOval(this);
		pnlData.add(mapaEstrategico);
		
		//
		javax.swing.JScrollBar mapaHorBar = scpRecord.getHorizontalScrollBar();
		javax.swing.JScrollBar mapaVerBar = scpRecord.getVerticalScrollBar();
		
		mapaHorBar.getModel().addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent evt) {
//				onBarsStateChanged(evt);
			}
		});
		
		mapaVerBar.getModel().addChangeListener(new javax.swing.event.ChangeListener() {
			public void stateChanged(javax.swing.event.ChangeEvent evt) {
				onBarsStateChanged(evt);
			}
		});
	}
	
	public void setStatus( int value ) {
		status = value;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setType( String value ) {
		type = String.valueOf( value );
	}
	
	public String getType( ) {
		return String.valueOf( type );
	}
	
	public void setID( String value ) {
		id = String.valueOf( value );
	}
	
	public String getID( ) {
		return String.valueOf( id );
	}
	
	public void setRecord( bsc.xml.BIXMLRecord recordAux ) {
		record = recordAux;
	}
	
	public bsc.xml.BIXMLRecord getRecord( ) {
		return record;
	}
	
	public void showDataButtons() {
		pnlConfirmation.setVisible(true);
		pnlOperation.setVisible(false);
	}
	
	public void showOperationsButtons() {
		pnlConfirmation.setVisible(false);
		pnlOperation.setVisible(true);
		if(isMaximize()){
			doMaximizeForm();
			setMaximize(false);
		}
	}
	
	public javax.swing.JInternalFrame asJInternalFrame() {
		return this;
	}
	
	public String getParentID() {
		if ( record.contains("PARENTID") )
			return String.valueOf( record.getString("PARENTID") );
		else
			return new String("");
	}
	
	public void loadRecord() {
		if(updatePersp){
			lblCarregando.setVisible(true);
			pnlData.remove(mapaEstrategico);
			initMapaEstrategico();
			event.loadRecord();
			repaint();
			lblCarregando.setVisible(false);
		}else{
			event.loadRecord();
		}
	}
	
	public javax.swing.JTabbedPane getTabbedPane() {
		return new javax.swing.JTabbedPane();
	}
	
	private void iniEstrategias(){
		BIXMLRecord record = getRecord();
		BIXMLVector vctPerspectivas =  record.getBIXMLVector("PERSPECTIVAS");
		int width = 0;
		int height= 0;
		int x =	0;
		int y = 0;
		int corAtual = 0;
		boolean doAutoSize = true;
		
		fldDataAlvo.setCalendar( record.getDate("DATAALVO") );
		if(record.getBoolean("PARCELADA")){
			//Parcelada
			fldAnalise.setText(java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaOvalFrame_00006"));
		}else{
			//Acumulada
			fldAnalise.setText(java.util.ResourceBundle.getBundle("international",bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscMapaOvalFrame_00007"));
		}
		
		for(int iPersp = 0; vctPerspectivas.size() > iPersp; iPersp++){
			bsc.xml.BIXMLRecord recPersp = vctPerspectivas.get(iPersp);
			bsc.swing.map.BscPerspectivaOval newPerspectiva = new bsc.swing.map.BscPerspectivaOval();
			width	= recPersp.getInt("MP2WIDTH");
			height	= recPersp.getInt("MP2HEIGHT");
			x		= recPersp.getInt("MP2X");
			y		= recPersp.getInt("MP2Y");
			
			//
			if(iPersp == 0 && width == 0 && height == 0){
				doAutoSize = true;
			}
			
			//Adicionando os objetivos as perspectivas.
			bsc.xml.BIXMLVector vctObjetivos = recPersp.getBIXMLVector("OBJETIVOS");
			for(int iObjetivo  = 0; vctObjetivos.size() > iObjetivo; iObjetivo++){
				bsc.xml.BIXMLRecord recObjetivo = vctObjetivos.get(iObjetivo);
				java.awt.Color textColor = java.awt.Color.black;
				java.awt.Font textFont = new java.awt.Font("Trebuchet MS",java.awt.Font.BOLD,11);
				int objX		= recObjetivo.getInt("MP2X");
				int objY		= recObjetivo.getInt("MP2Y");
				int objWidth	= recObjetivo.getInt("MP2WIDTH");
				int objHeigth	= recObjetivo.getInt("MP2HEIGHT");
				
				BscMapaObjetivoOval newObjetivo = new BscMapaObjetivoOval();
				newObjetivo.getTxtObjetivo().setText(recObjetivo.getString("NOME"));
				newObjetivo.setId(recObjetivo.getInt("ID"));
				
				newPerspectiva.addObjetivoOval(newObjetivo);
				if(objX ==0 && objY == 0 && objWidth == 0 && objHeigth == 0 ){
					newObjetivo.setForceSize(true);
				}else{
					newObjetivo.setForceSize(false);
					textFont = new java.awt.Font(recObjetivo.getString("MP2FONTE"),recObjetivo.getInt("MP2FONEST"),
							recObjetivo.getInt("MP2FONTAM"));
					textColor = new java.awt.Color(recObjetivo.getInt("MP2FONCOR"));
				}

                                newObjetivo.setTextFont(textFont);
				newObjetivo.setTextColor(textColor);
				newObjetivo.setBounds(objX,objY,objWidth,objHeigth);
				newObjetivo.setSemaforo(recObjetivo.getInt("FAROL"));
			}
			
			//Adicionando a perspectiva dentro do mapa estrat�gico.
			int backColor = recPersp.getInt("BACKCOLOR");
			mapaEstrategico.putPerspectiva(newPerspectiva);
			java.awt.Font	cabFonte	= new java.awt.Font("Trebuchet MS",java.awt.Font.BOLD,18);
			java.awt.Color	cabColor	= new java.awt.Color(-256);
			if(doAutoSize && backColor == 0){
				if(corAtual == 4){
					corAtual = 0;
				}
				newPerspectiva.setBackground(perspCores[corAtual]);
				corAtual++;
			}else{
				newPerspectiva.setBackground(new java.awt.Color(backColor));
				if(! recPersp.getString("MP2FONTE").equals("")){
					cabFonte = new java.awt.Font(recPersp.getString("MP2FONTE"),recPersp.getInt("MP2FONEST"), recPersp.getInt("MP2FONTAM"));
				}
				cabColor = new java.awt.Color(recPersp.getInt("MP2TITCOR"));
			}
			newPerspectiva.setFontTitulo(cabFonte);
			newPerspectiva.setID(recPersp.getInt("ID"));
			newPerspectiva.setTitulo(recPersp.getString("NOME"));
			newPerspectiva.setTituloCor(cabColor);
			newPerspectiva.setDegrade(recPersp.getBoolean("MP2DEGRADE"));
			newPerspectiva.setBounds(x,y,width,height);
			//No 1� painel acerto do tamanho do pnlData, por causa do scroll.
			if(iPersp== 0){
				pnlData.setPreferredSize(newPerspectiva.getSize());
				pnlData.revalidate();
			}
		}
		//Op��o para fazer o dimensionamento automaticos da tela.
		if(doAutoSize){
			mapaEstrategico.doPerspectivaAutoSize();
			pnlData.setPreferredSize(mapaEstrategico.getSize());
			pnlData.revalidate();
		}
	}
	
	private void setEditing(boolean editing) {
		this.editing = editing;
		
		if(mapaEstrategico != null){
			mapaEstrategico.setFormEditing(editing);
			if(editing){
				java.util.Vector<BscPerspectivaOval> vctPerspectiva = mapaEstrategico.getVctPerspectivas();
				for(java.util.Iterator<BscPerspectivaOval> itePers = vctPerspectiva.iterator(); itePers.hasNext();){
					BscPerspectivaOval  persp = itePers.next();
					persp.setAutoResize(false);
				}
			}
		}
	}
	
	private void doMaximizeForm(){
		if(mainPanel.bscTree.getBscTreeType() == mainPanel.ST_AREATRABALHO){
			divPos =  mainPanel.jSplitPaneAreaTrabalho.getDividerLocation();
			mainPanel.jSplitPaneAreaTrabalho.setDividerLocation(0);
		}else{
			divPos =  mainPanel.jSplitPaneOrganizacao.getDividerLocation();
			mainPanel.jSplitPaneOrganizacao.setDividerLocation(0);
		}
		
		try{
			this.setMaximum(true);
		}catch(Exception e){
		}
	}
	
	private boolean isMaximize() {
		return maximize;
	}
	
	private void setMaximize(boolean maximize) {
		this.maximize = maximize;
	}
	
	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex > 0) {
			return(NO_SUCH_PAGE);
		} else {
			java.awt.Graphics2D g2d = (java.awt.Graphics2D)g;
			g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
			g2d.scale( 0.75, 1.3 );
			disableDoubleBuffering(this);
			int oldColor = mapaEstrategico.getBackground().getRGB();
			mapaEstrategico.setBackground(java.awt.Color.white);
			mapaEstrategico.paint(g2d);
			mapaEstrategico.setBackground(new java.awt.Color(oldColor));
			enableDoubleBuffering(this);
			return(PAGE_EXISTS);
		}
		
	}
	
	private void saveComponentAsJPEG(String fileName) {
		Dimension size = pnlData.getSize();
		java.awt.image.BufferedImage myImage = new java.awt.image.BufferedImage(size.width, size.height ,java.awt.image.BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = myImage.createGraphics();
		disableDoubleBuffering(pnlData);
		mapaEstrategico.paint(g2);
		enableDoubleBuffering(pnlData);
		
		//Grava a figura l� no servidor... :)
		try {
			java.io.ByteArrayOutputStream out = new  java.io.ByteArrayOutputStream();
			//com.sun.image.codec.jpeg.JPEGImageEncoder encoder = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(out);
			//encoder.encode(myImage);
                        //Salva o arquivo de imagem como JPEG
                        ImageIO.write(myImage, "jpg", out);
                        String strBase64 = bsc.util.prefs.Base64.byteArrayToBase64(out.toByteArray());
			event.saveBase64(fileName, strBase64);
			out.close();
			//Abre o navegador para salvar a figura localmente
			bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
			java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString()+fileName);
			bscApplet.getAppletContext().showDocument(url,"_new");
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void disableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(false);
	}
	
	public static void enableDoubleBuffering(Component c) {
		RepaintManager currentManager = RepaintManager.currentManager(c);
		currentManager.setDoubleBufferingEnabled(true);
	}
}