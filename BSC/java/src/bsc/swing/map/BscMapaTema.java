/*
 * BscMapaTema.java
 *
 * Created on 16 de Novembro de 2004, 14:28
 */

package bsc.swing.map;

/**
 *
 * @author  siga1776
 */
public class BscMapaTema extends BscMapaJPanel{
	
	public boolean temaSelected = false;
	
	/** Creates new form BscMapaEstrategia */
	public BscMapaTema () {
		initComponents ();
		validate ();
	}
	
	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	private void initComponents() {//GEN-BEGIN:initComponents
	    jPanelConteudo = new javax.swing.JLayeredPane();
	    
	    setLayout(new java.awt.BorderLayout());
	    
	    setBorder(new javax.swing.border.TitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(102, 0, 0)));
	    setFocusCycleRoot(true);
	    jPanelConteudo.setFocusCycleRoot(true);
	    jPanelConteudo.setOpaque(true);
	    add(jPanelConteudo, java.awt.BorderLayout.CENTER);
	    
	}//GEN-END:initComponents
	
	public javax.swing.JLayeredPane getLayeredPane () {
		return jPanelConteudo;
	}
	
	public void paint (java.awt.Graphics g){
		super.paint (g);
		if(temaSelected){
			java.awt.Graphics2D g2 = (java.awt.Graphics2D) g;
			g2.setStroke (new java.awt.BasicStroke (8.0f));
			g2.setColor (java.awt.Color.yellow);
			g2.drawRect (0,0, this.getWidth (), this.getHeight ());
		}
	}
	
	public void setTemaSelected (boolean lSet){
		temaSelected = lSet;
	}
	
	public void setTemaSelected (){
		if(temaSelected){
			temaSelected = false;
		}else{
			temaSelected = true;
		}
	}
	
	//Faz o ajuste no tamanho do tema.
	public void mapaTemaResize (java.awt.Point pMouse, int iBorda,java.awt.Point pLastLocation){
		boolean lTemaResize = true;
		
		int iX	= this.getX (),
				iY	= this.getY (),
				iWidth	= this.getWidth (),
				iHeight	= this.getHeight (),
				maxBordSize	= setMaxBorder (iBorda);
		
		if(iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_LEFT){
			iX	= pMouse.x;
			iY	= this.getY ();
			iWidth	= this.getWidth () + (this.getX () - pMouse.x);
			iHeight	= this.getHeight ();
			iWidth  = maxBordSize > iWidth ? maxBordSize : iWidth;
		} else if(iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_UP){
			//Alterado o evento para mover o tema ao inv�s de mudar o tamanho.
			//C�lculo para mudar o tamanho do tema. N�o remover.
			iX	= this.getX ();
			iY	= pMouse.y;
			iWidth	= this.getWidth ();
			iHeight	= this.getHeight () + (this.getY ()- pMouse.y) ;
			iHeight = maxBordSize > iHeight ? maxBordSize : iHeight;
		} else if(iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_RIGHT ){
			iX	= this.getX ();
			iY	= this.getY ();
			iWidth	= pMouse.x - this.getX ();
			iHeight	= this.getHeight () ;
			iWidth  = maxBordSize > iWidth ? maxBordSize : iWidth;
		} else if (iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_DOWN){
			iX	= this.getX ();
			iY	= this.getY ();
			iWidth	= this.getWidth ();
			iHeight	= pMouse.y - this.getY () ;
			iHeight = maxBordSize > iHeight ? maxBordSize : iHeight;
		} else{
			lTemaResize = false;
			this.setLocation (new java.awt.Point (this.getLocation ().x+pLastLocation.x, this.getLocation ().y+pLastLocation.y));

		}
		
		if(lTemaResize){
			this.setBounds (iX,iY,iWidth,iHeight);
		}
		
		//Valida��o do tamanho m�ximo do tema.
		BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva)this.getParent ().getParent ();
		if(this.getWidth () > mapaPerspectiva.getWidth ()){
			this.setSize (mapaPerspectiva.getWidth () - 20, this.getHeight ());
		}
		if(this.getHeight () > mapaPerspectiva.getHeight ()){
			this.setSize (this.getWidth (), mapaPerspectiva.getHeight () - 100);
		}
	}
	
	//Verifica o tamanho maximo e minimo que o tema pode ter baseado no objetos contidos.
	public int setMaxBorder (int iBorda){
		int maxSize = 50;
		for(int indObjeto = 0; indObjeto< this.getComponentCount ();indObjeto++){
			if(this.getComponent (indObjeto) instanceof BscMapaObjetivo){
				BscMapaObjetivo objetivo = (BscMapaObjetivo)this.getComponent (indObjeto);
				
				if(iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_LEFT ||
						iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_RIGHT){
					maxSize = objetivo.getX ()+objetivo.getWidth () > maxSize ? objetivo.getX () + objetivo.getWidth (): maxSize;
				} else if(iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_UP ||
						iBorda == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_DOWN){
					maxSize = objetivo.getY ()+objetivo.getHeight () > maxSize ? objetivo.getY () + objetivo.getHeight (): maxSize;
				}
			}
		}
		
		return maxSize;
	}
	
	//Define a movimenta��o maxima do objeto dentro do container.
	public void setMaxMoviment (){
		int objX = 0,
				objY = 0,
				objW = 0,
				objH = 0;
		
		if (this.getParent ().getParent () instanceof BscMapaPerspectiva){
			BscMapaPerspectiva mapaPerspectiva = (BscMapaPerspectiva)this.getParent ().getParent ();
			objX = mapaPerspectiva.getX ();
			objY = mapaPerspectiva.getY ();
			objW = mapaPerspectiva.getWidth ();
			objH = mapaPerspectiva.getHeight ();
		}
		
		if(this.getY () + this.getHeight () > objH)
			this.setLocation (this.getX (), objH - (this.getHeight () + 33));
		if(this.getY () < 5)
			this.setLocation (this.getX (), 5 );
		if(this.getX () < 0)
			this.setLocation (3, this.getY ());
		if(this.getX () + this.getWidth () > objW )
			this.setLocation (objW - this.getWidth (),this.getY ());
	}
	
	//Verifica em qual borda o mouse est�.
	public int setDragStatus (java.awt.event.MouseEvent mouseEvent){
		java.awt.Rectangle rTema = this.getBounds ();
		java.awt.Rectangle rLado = null;
		int iBorda = bsc.swing.map.BscMapaFrame.DRAG_OBJ_MOVING;
		
		for(int iLado = 4; iLado < 8; iLado++){
			if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_LEFT){
				rLado = new java.awt.Rectangle (0 , 0, bsc.swing.map.BscMapaFrame.LARGURABORDA , rTema.height);
			} else if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_UP){
				rLado = new java.awt.Rectangle (0, 0 , rTema.width , bsc.swing.map.BscMapaFrame.LARGURABORDA);
			} else if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_RIGHT){
				rLado = new java.awt.Rectangle (rTema.width - bsc.swing.map.BscMapaFrame.LARGURABORDA , 0, rTema.height , rTema.height);
			} else if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_DOWN){
				rLado = new java.awt.Rectangle (0, rTema.height - bsc.swing.map.BscMapaFrame.LARGURABORDA, rTema.width , rTema.height);
			}
			
			if (rLado.contains (mouseEvent.getX (),mouseEvent.getY ())) {
				if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_LEFT){
					iBorda = bsc.swing.map.BscMapaFrame.DRAG_RESIZE_LEFT;
					break;
				} else if( iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_RIGHT){
					iBorda = bsc.swing.map.BscMapaFrame.DRAG_RESIZE_RIGHT;
					break;
				} else if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_UP){
					iBorda = bsc.swing.map.BscMapaFrame.DRAG_RESIZE_UP;
					break;
				} else if(iLado == bsc.swing.map.BscMapaFrame.DRAG_RESIZE_DOWN ){
					iBorda = bsc.swing.map.BscMapaFrame.DRAG_RESIZE_DOWN;
					break;
				}
			}
		}
		
		return iBorda;
	}
	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JLayeredPane jPanelConteudo;
	// End of variables declaration//GEN-END:variables
	
}
