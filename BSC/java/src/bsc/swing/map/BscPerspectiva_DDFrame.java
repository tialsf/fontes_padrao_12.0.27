/*
 * BscPerspectiva_DDFrame.java
 *
 * Created on 13/11/2009, 11:33:15
 */
package bsc.swing.map;

import bsc.core.BscFormController;
import bsc.xml.BIXMLRecord;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.beans.PropertyVetoException;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author valdineyg
 */
public class BscPerspectiva_DDFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    private bsc.core.BscFormController formController = bsc.core.BscStaticReferences.getBscFormController();
    private String type = new String("PERSP_DRILLD");
    private String cmd = new String("");
    private java.awt.Color corFonte = java.awt.Color.black;
    private java.awt.Color corFundo = java.awt.Color.white;
    private TableHeadRenderer headerRender = new TableHeadRenderer();

    public BscPerspectiva_DDFrame(int operation, String idAux, String contextId) {
        initComponents();
        setStatus(operation);
        showOperationsButtons();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlLeftForm = new javax.swing.JPanel();
        pnlPerspectiva = new javax.swing.JPanel();
        lblPerspectiva = new javax.swing.JLabel();
        pnlRightForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        srcObjetivo = new javax.swing.JScrollPane();

        setClosable(true);
        setMaximizable(true);

        tabObjetivo = new bsc.beans.JBIXMLTable();
        pnlLeftForm.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.LINE_START);

        pnlPerspectiva.setBackground(new java.awt.Color(255, 255, 255));
        pnlPerspectiva.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        lblPerspectiva.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblPerspectiva.setText("Perspectiva");
        lblPerspectiva.setAlignmentX(0.5F);
        lblPerspectiva.setPreferredSize(new java.awt.Dimension(400, 25));
        pnlPerspectiva.add(lblPerspectiva);

        getContentPane().add(pnlPerspectiva, java.awt.BorderLayout.PAGE_START);

        pnlRightForm.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.PAGE_END);

        pnlBottomForm.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.LINE_END);
        getContentPane().add(srcObjetivo, java.awt.BorderLayout.CENTER);
        tabObjetivo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));

        tabObjetivo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabObjetivoMouseClicked(evt);
            }
        });

        srcObjetivo.setViewportView(tabObjetivo);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void refreshFields() {
        BIXMLRecord rec = getRecord();

        if (rec != null) {
            bsc.xml.BIXMLVector vctPersps = getRecord().getBIXMLVector("PERSPECTIVAS");
            tabObjetivo.setDataSource(vctPersps.get(0).getBIXMLVector("INDICADORES"));
            applyRenderTable();
            lblPerspectiva.setText(getRecord().getString("NOME"));
        }
    }

    @Override
    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        return recordAux;
    }

    @Override
    public boolean validateFields(StringBuffer errorMessage) {
        return true;
    }

    @Override
    public boolean hasCombos() {
        return false;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblPerspectiva;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlPerspectiva;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JScrollPane srcObjetivo;
    // End of variables declaration//GEN-END:variables
    private bsc.beans.JBIXMLTable tabObjetivo;
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public void applyRenderTable() {

        tabObjetivo.setSelectionBackground(corFundo);
        tabObjetivo.setSelectionForeground(corFonte);
        tabObjetivo.setForeground(corFonte);
        tabObjetivo.setGridColor(corFundo);
        tabObjetivo.setBackground(corFundo);
        tabObjetivo.getParent().setBackground(corFundo);

        for (int col = 0; tabObjetivo.getColumnModel().getColumnCount() > col; col++) {
            tabObjetivo.getColumnModel().getColumn(col).setHeaderRenderer(headerRender);
        }

        srcObjetivo.setBorder(javax.swing.BorderFactory.createLineBorder(corFundo));
    }

    private void requestIndDetalhe() {
        int line = tabObjetivo.getSelectedRow();
        if (line != -1) {
            bsc.xml.BIXMLRecord recLinha = tabObjetivo.getXMLData().get(line);
            String indID = recLinha.getString("IND_ID");
            if (!indID.equals("0")) {
                //O ID � setado depois, para o usuario abrir sempre o mesmo formul�rio.
                bsc.swing.map.BscIndicadorDetalheFrame frameIndicador_Det = (bsc.swing.map.BscIndicadorDetalheFrame) formController.newForm("INDICADOR_DET", "0", "");
                //Setando as propriedades do formulario.
                frameIndicador_Det.setID(indID);
                frameIndicador_Det.loadRecord();
                if (formController.lastView == BscFormController.ST_JANELA) {
                    try {
                        frameIndicador_Det.setMaximum(true);
                    } catch (PropertyVetoException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void enableFields() {
    }

    @Override
    public void disableFields() {
    }

    class TableHeadRenderer extends DefaultTableCellRenderer {

        private LabelHeader lblRender = new LabelHeader();

        @Override
        public java.awt.Component getTableCellRendererComponent(javax.swing.JTable t, Object v, boolean s, boolean f, int r, int c) {
            lblRender.setText(v.toString());
            lblRender.setBackground(corFundo);
            lblRender.setForeground(corFonte);
            lblRender.setOpaque(true);
            return lblRender;
        }
    }

    class LabelHeader extends javax.swing.JLabel {

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(5.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
            g.setColor(java.awt.Color.BLACK);
            g.drawLine(0, getHeight(), getWidth(), getHeight());
        }
    }

    private void pnlTopFormComponentResized(java.awt.event.ComponentEvent evt) {
        if (formController.lastView == BscFormController.ST_PASTA) {
            applyRenderTable();
        }
    }

    private void tabObjetivoMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 2) {
            requestIndDetalhe();
        }
    }

    @Override
    public void setStatus(int value) {
        status = value;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setType(String value) {
        type = String.valueOf(value);
    }

    @Override
    public String getType() {
        return String.valueOf(type);
    }

    @Override
    public void setID(String value) {
        id = String.valueOf(value);
    }

    @Override
    public String getID() {
        return String.valueOf(id);
    }

    @Override
    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    @Override
    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    @Override
    public void showDataButtons() {
    }

    @Override
    public void showOperationsButtons() {
    }

    @Override
    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    @Override
    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    @Override
    public void loadRecord() {
        setRecord(event.loadRecord(getID(), type, getCmd()));
        refreshFields();
    }

    @Override
    public javax.swing.JTabbedPane getTabbedPane() {
        return new javax.swing.JTabbedPane();
    }

    private String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }
}
