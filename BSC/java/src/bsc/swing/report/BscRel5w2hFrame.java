package bsc.swing.report;

import java.util.ResourceBundle;

public class BscRel5w2hFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("REL5W2H");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        chkIncluirDescricao.setEnabled(true);
        lblInicioDe.setEnabled(true);
        fldInicioDe.setEnabled(true);
        lblInicioAte.setEnabled(true);
        fldInicioAte.setEnabled(true);
        lblTerminoDe.setEnabled(true);
        fldTerminoDe.setEnabled(true);
        lblTerminoAte.setEnabled(true);
        fldTerminoAte.setEnabled(true);
        lblPessoa.setEnabled(true);
        cbbPessoa.setEnabled(true);
        radEstrategia.setEnabled(true);
        radPerspectiva.setEnabled(true);
        radObjetivo.setEnabled(true);
        radIniciativa.setEnabled(true);
        lblSituacao.setEnabled(true);
        cbbSituacao.setEnabled(true);
        lblImportancia.setEnabled(true);
        cbbImportancia.setEnabled(true);
        lblUrgencia.setEnabled(true);
        cbbUrgencia.setEnabled(true);
        pnlEntidade.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
        //lblNome.setEnabled(false);
        //fldNome.setEnabled(false);
    }
    java.util.Hashtable htbPessoa = new java.util.Hashtable();
    java.util.Hashtable htbPerspectiva = new java.util.Hashtable();
    java.util.Hashtable htbObjetivo = new java.util.Hashtable();
    java.util.Hashtable htbIniciativa = new java.util.Hashtable();
    java.util.Hashtable htbSituacao = new java.util.Hashtable();
    java.util.Hashtable htbUrgencia = new java.util.Hashtable();
    java.util.Hashtable htbImportancia = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));
        fldInicioDe.setCalendar(record.getDate("INICIODE"));
        fldInicioAte.setCalendar(record.getDate("INICIOATE"));
        fldTerminoDe.setCalendar(record.getDate("TERMINODE"));
        fldTerminoAte.setCalendar(record.getDate("TERMINOATE"));

        event.populateCombo(record.getBIXMLVector("PESSOAS"), "PESID", htbPessoa, cbbPessoa);
        event.populateCombo(record.getBIXMLVector("PERSPECTIVAS"), "PERSID", htbPerspectiva, cbbPerspectiva);
        event.populateCombo(record.getBIXMLVector("OBJETIVOS"), "OBJID", htbObjetivo, cbbObjetivo);
        event.populateCombo(record.getBIXMLVector("INICIATIVAS"), "INICID", htbIniciativa, cbbIniciativa);
        event.populateCombo(record.getBIXMLVector("SITUACOES"), "SITID", htbSituacao, cbbSituacao);
        event.populateCombo(record.getBIXMLVector("URGENCIAS"), "URGENCIA", htbUrgencia, cbbUrgencia);
        event.populateCombo(record.getBIXMLVector("IMPORTANCIAS"), "IMPORTANCI", htbImportancia, cbbImportancia);

        if (!record.getString("PERSID").equals("0")) {
            cbbPerspectiva.setEnabled(true);
            radPerspectiva.setSelected(true);

            cbbObjetivo.setSelectedIndex(-1);
            cbbObjetivo.setEnabled(false);

            cbbIniciativa.setSelectedIndex(-1);
            cbbIniciativa.setEnabled(false);
        } else if (!record.getString("OBJID").equals("0")) {
            cbbObjetivo.setEnabled(true);
            radObjetivo.setSelected(true);

            cbbPerspectiva.setSelectedIndex(-1);
            cbbPerspectiva.setEnabled(false);

            cbbIniciativa.setSelectedIndex(-1);
            cbbIniciativa.setEnabled(false);
        } else if (!record.getString("INICID").equals("0")) {
            cbbIniciativa.setEnabled(true);
            radIniciativa.setSelected(true);

            cbbObjetivo.setSelectedIndex(-1);
            cbbObjetivo.setEnabled(false);

            cbbPerspectiva.setSelectedIndex(-1);
            cbbPerspectiva.setEnabled(false);
        } else {
            radEstrategia.setSelected(true);

            cbbPerspectiva.setSelectedIndex(-1);
            cbbPerspectiva.setEnabled(false);

            cbbIniciativa.setSelectedIndex(-1);
            cbbIniciativa.setEnabled(false);

            cbbObjetivo.setSelectedIndex(-1);
            cbbObjetivo.setEnabled(false);
        }
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("INICIODE", fldInicioDe.getCalendar());
        recordAux.set("INICIOATE", fldInicioAte.getCalendar());
        recordAux.set("TERMINODE", fldTerminoDe.getCalendar());
        recordAux.set("TERMINOATE", fldTerminoAte.getCalendar());

        recordAux.set("PESID", event.getComboValue(htbPessoa, cbbPessoa));
        recordAux.set("PERSID", event.getComboValue(htbPerspectiva, cbbPerspectiva));
        recordAux.set("OBJID", event.getComboValue(htbObjetivo, cbbObjetivo));
        recordAux.set("INICID", event.getComboValue(htbIniciativa, cbbIniciativa));
        recordAux.set("SITID", event.getComboValue(htbSituacao, cbbSituacao));
        recordAux.set("IMPORTANCI", event.getComboValue(htbImportancia, cbbImportancia));
        recordAux.set("URGENCIA", event.getComboValue(htbUrgencia, cbbUrgencia));

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btngrpEntidades = new javax.swing.ButtonGroup();
        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        fldNome = new pv.jfcx.JPVEdit();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        lblDescricao = new javax.swing.JLabel();
        lblPessoa = new javax.swing.JLabel();
        cbbPessoa = new javax.swing.JComboBox();
        fldInicioDe = new pv.jfcx.JPVDatePlus();
        fldInicioAte = new pv.jfcx.JPVDatePlus();
        lblInicioDe = new javax.swing.JLabel();
        lblInicioAte = new javax.swing.JLabel();
        lblTerminoDe = new javax.swing.JLabel();
        fldTerminoDe = new pv.jfcx.JPVDatePlus();
        lblTerminoAte = new javax.swing.JLabel();
        fldTerminoAte = new pv.jfcx.JPVDatePlus();
        pnlEntidade = new javax.swing.JPanel();
        radEstrategia = new javax.swing.JRadioButton();
        radPerspectiva = new javax.swing.JRadioButton();
        radObjetivo = new javax.swing.JRadioButton();
        radIniciativa = new javax.swing.JRadioButton();
        cbbPerspectiva = new javax.swing.JComboBox();
        cbbObjetivo = new javax.swing.JComboBox();
        cbbIniciativa = new javax.swing.JComboBox();
        cbbSituacao = new javax.swing.JComboBox();
        lblSituacao = new javax.swing.JLabel();
        lblImportancia = new javax.swing.JLabel();
        cbbImportancia = new javax.swing.JComboBox();
        lblUrgencia = new javax.swing.JLabel();
        cbbUrgencia = new javax.swing.JComboBox();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        pnlTopTool = new javax.swing.JPanel();
        pnlBottomTool = new javax.swing.JPanel();
        pnlCenterTool = new javax.swing.JPanel();
        pnlLeftTool = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscRel5w2hFrame_00022")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setPreferredSize(new java.awt.Dimension(484, 470));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scpRecord.setAutoscrolls(true);
        scpRecord.setPreferredSize(new java.awt.Dimension(464, 366));

        pnlData.setPreferredSize(new java.awt.Dimension(464, 524));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscRel5w2hFrame_00007")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 0, 98, 18);

        chkIncluirDescricao.setText(bundle.getString("BscRel5w2hFrame_00009")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 120, 254, 20);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(10, 60, 400, 60);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscRel5w2hFrame_00008")); // NOI18N
        lblDescricao.setEnabled(false);
        lblDescricao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblDescricao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 98, 18);

        lblPessoa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPessoa.setText(bundle.getString("BscRel5w2hFrame_00010")); // NOI18N
        lblPessoa.setEnabled(false);
        lblPessoa.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPessoa.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPessoa.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblPessoa);
        lblPessoa.setBounds(230, 370, 98, 20);

        cbbPessoa.setEnabled(false);
        pnlData.add(cbbPessoa);
        cbbPessoa.setBounds(230, 390, 180, 22);

        fldInicioDe.setDialog(true);
        fldInicioDe.setUseLocale(true);
        fldInicioDe.setWaitForCalendarDate(true);
        pnlData.add(fldInicioDe);
        fldInicioDe.setBounds(10, 310, 180, 22);

        fldInicioAte.setDialog(true);
        fldInicioAte.setUseLocale(true);
        fldInicioAte.setWaitForCalendarDate(true);
        pnlData.add(fldInicioAte);
        fldInicioAte.setBounds(230, 310, 180, 22);

        lblInicioDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInicioDe.setText(bundle.getString("BscRel5w2hFrame_00014")); // NOI18N
        lblInicioDe.setEnabled(false);
        lblInicioDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInicioDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInicioDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInicioDe);
        lblInicioDe.setBounds(10, 290, 98, 18);

        lblInicioAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInicioAte.setText(bundle.getString("BscRel5w2hFrame_00015")); // NOI18N
        lblInicioAte.setEnabled(false);
        lblInicioAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblInicioAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblInicioAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblInicioAte);
        lblInicioAte.setBounds(230, 290, 28, 18);

        lblTerminoDe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerminoDe.setText(bundle.getString("BscRel5w2hFrame_00016")); // NOI18N
        lblTerminoDe.setEnabled(false);
        lblTerminoDe.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTerminoDe.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTerminoDe.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTerminoDe);
        lblTerminoDe.setBounds(10, 330, 98, 18);

        fldTerminoDe.setDialog(true);
        fldTerminoDe.setUseLocale(true);
        fldTerminoDe.setWaitForCalendarDate(true);
        pnlData.add(fldTerminoDe);
        fldTerminoDe.setBounds(10, 350, 180, 22);

        lblTerminoAte.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTerminoAte.setText(bundle.getString("BscRel5w2hFrame_00017")); // NOI18N
        lblTerminoAte.setEnabled(false);
        lblTerminoAte.setMaximumSize(new java.awt.Dimension(100, 16));
        lblTerminoAte.setMinimumSize(new java.awt.Dimension(100, 16));
        lblTerminoAte.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblTerminoAte);
        lblTerminoAte.setBounds(230, 330, 28, 18);

        fldTerminoAte.setDialog(true);
        fldTerminoAte.setUseLocale(true);
        fldTerminoAte.setWaitForCalendarDate(true);
        pnlData.add(fldTerminoAte);
        fldTerminoAte.setBounds(230, 350, 180, 22);

        pnlEntidade.setBorder(javax.swing.BorderFactory.createTitledBorder(null, bundle.getString("BscRel5w2hFrame_00023"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 11))); // NOI18N
        pnlEntidade.setEnabled(false);
        pnlEntidade.setLayout(null);

        btngrpEntidades.add(radEstrategia);
        radEstrategia.setText(bundle.getString("BscRel5w2hFrame_00024")); // NOI18N
        radEstrategia.setEnabled(false);
        radEstrategia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radEstrategiaActionPerformed(evt);
            }
        });
        pnlEntidade.add(radEstrategia);
        radEstrategia.setBounds(40, 20, 94, 23);

        btngrpEntidades.add(radPerspectiva);
        radPerspectiva.setText(bundle.getString("BscRel5w2hFrame_00011")); // NOI18N
        radPerspectiva.setEnabled(false);
        radPerspectiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radPerspectivaActionPerformed(evt);
            }
        });
        pnlEntidade.add(radPerspectiva);
        radPerspectiva.setBounds(40, 50, 90, 23);

        btngrpEntidades.add(radObjetivo);
        radObjetivo.setText(bundle.getString("BscRel5w2hFrame_00012")); // NOI18N
        radObjetivo.setEnabled(false);
        radObjetivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radObjetivoActionPerformed(evt);
            }
        });
        pnlEntidade.add(radObjetivo);
        radObjetivo.setBounds(40, 80, 90, 23);

        btngrpEntidades.add(radIniciativa);
        radIniciativa.setText(bundle.getString("BscRel5w2hFrame_00013")); // NOI18N
        radIniciativa.setEnabled(false);
        radIniciativa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radIniciativaActionPerformed(evt);
            }
        });
        pnlEntidade.add(radIniciativa);
        radIniciativa.setBounds(40, 110, 90, 23);

        cbbPerspectiva.setEnabled(false);
        pnlEntidade.add(cbbPerspectiva);
        cbbPerspectiva.setBounds(230, 50, 180, 22);

        cbbObjetivo.setEnabled(false);
        pnlEntidade.add(cbbObjetivo);
        cbbObjetivo.setBounds(230, 80, 180, 22);

        cbbIniciativa.setEnabled(false);
        pnlEntidade.add(cbbIniciativa);
        cbbIniciativa.setBounds(230, 110, 180, 22);

        pnlData.add(pnlEntidade);
        pnlEntidade.setBounds(0, 140, 420, 150);

        cbbSituacao.setEnabled(false);
        pnlData.add(cbbSituacao);
        cbbSituacao.setBounds(10, 390, 180, 22);

        lblSituacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSituacao.setText(bundle.getString("BscRel5w2hFrame_00018")); // NOI18N
        lblSituacao.setEnabled(false);
        lblSituacao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblSituacao);
        lblSituacao.setBounds(10, 370, 98, 18);

        lblImportancia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblImportancia.setText(bundle.getString("BscTarefaFrame_00027")); // NOI18N
        lblImportancia.setEnabled(false);
        lblImportancia.setMaximumSize(new java.awt.Dimension(100, 16));
        lblImportancia.setMinimumSize(new java.awt.Dimension(100, 16));
        lblImportancia.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblImportancia);
        lblImportancia.setBounds(230, 410, 98, 18);

        cbbImportancia.setEnabled(false);
        pnlData.add(cbbImportancia);
        cbbImportancia.setBounds(230, 430, 180, 22);

        lblUrgencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUrgencia.setText(bundle.getString("BscTarefaFrame_00028")); // NOI18N
        lblUrgencia.setEnabled(false);
        lblUrgencia.setMaximumSize(new java.awt.Dimension(100, 16));
        lblUrgencia.setMinimumSize(new java.awt.Dimension(100, 16));
        lblUrgencia.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblUrgencia);
        lblUrgencia.setBounds(10, 410, 98, 18);

        cbbUrgencia.setEnabled(false);
        pnlData.add(cbbUrgencia);
        cbbUrgencia.setBounds(10, 430, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(20, 30));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(200, 200));

        btnGeraRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("BscRel5w2hFrame_00019")); // NOI18N
        btnGeraRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("BscRel5w2hFrame_00020")); // NOI18N
        btnVizualizaRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        pnlTopRecord.add(tbInsertion, java.awt.BorderLayout.EAST);

        pnlTopTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlTopTool, java.awt.BorderLayout.PAGE_START);

        pnlBottomTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlBottomTool, java.awt.BorderLayout.SOUTH);

        pnlCenterTool.setPreferredSize(new java.awt.Dimension(10, 30));
        pnlTopRecord.add(pnlCenterTool, java.awt.BorderLayout.CENTER);
        pnlTopRecord.add(pnlLeftTool, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.PAGE_START);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscRel5w2hFrame_00021"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            try {
                bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString().concat("bscreport.apw?id=rel054_".concat(this.id)));
                bscApplet.getAppletContext().showDocument(url, "_blank");
            } catch (Exception e) {
                e.printStackTrace();
            }
	}//GEN-LAST:event_btnVizualizaRelActionPerformed

	private void radEstrategiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radEstrategiaActionPerformed
            cbbPerspectiva.setSelectedIndex(-1);
            cbbObjetivo.setSelectedIndex(-1);
            cbbIniciativa.setSelectedIndex(-1);

            cbbPerspectiva.setEnabled(false);
            cbbObjetivo.setEnabled(false);
            cbbIniciativa.setEnabled(false);
	}//GEN-LAST:event_radEstrategiaActionPerformed

	private void radIniciativaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radIniciativaActionPerformed
            cbbPerspectiva.setSelectedIndex(-1);
            cbbObjetivo.setSelectedIndex(-1);

            cbbPerspectiva.setEnabled(false);
            cbbObjetivo.setEnabled(false);
            cbbIniciativa.setEnabled(true);
	}//GEN-LAST:event_radIniciativaActionPerformed

	private void radObjetivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radObjetivoActionPerformed
            cbbPerspectiva.setSelectedIndex(-1);
            cbbIniciativa.setSelectedIndex(-1);

            cbbPerspectiva.setEnabled(false);
            cbbObjetivo.setEnabled(true);
            cbbIniciativa.setEnabled(false);
	}//GEN-LAST:event_radObjetivoActionPerformed

	private void radPerspectivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radPerspectivaActionPerformed
            cbbObjetivo.setSelectedIndex(-1);
            cbbIniciativa.setSelectedIndex(-1);

            cbbPerspectiva.setEnabled(true);
            cbbObjetivo.setEnabled(false);
            cbbIniciativa.setEnabled(false);
	}//GEN-LAST:event_radPerspectivaActionPerformed

	private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            event.executeRecord("SALVAR;rel054_" + record.getString("ID") + ".html");
	}//GEN-LAST:event_btnGeraRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.ButtonGroup btngrpEntidades;
    private javax.swing.JComboBox cbbImportancia;
    private javax.swing.JComboBox cbbIniciativa;
    private javax.swing.JComboBox cbbObjetivo;
    private javax.swing.JComboBox cbbPerspectiva;
    private javax.swing.JComboBox cbbPessoa;
    private javax.swing.JComboBox cbbSituacao;
    private javax.swing.JComboBox cbbUrgencia;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private pv.jfcx.JPVDatePlus fldInicioAte;
    private pv.jfcx.JPVDatePlus fldInicioDe;
    private pv.jfcx.JPVEdit fldNome;
    private pv.jfcx.JPVDatePlus fldTerminoAte;
    private pv.jfcx.JPVDatePlus fldTerminoDe;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblImportancia;
    private javax.swing.JLabel lblInicioAte;
    private javax.swing.JLabel lblInicioDe;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPessoa;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JLabel lblTerminoAte;
    private javax.swing.JLabel lblTerminoDe;
    private javax.swing.JLabel lblUrgencia;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlBottomTool;
    private javax.swing.JPanel pnlCenterTool;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlEntidade;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftTool;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTopTool;
    private javax.swing.JRadioButton radEstrategia;
    private javax.swing.JRadioButton radIniciativa;
    private javax.swing.JRadioButton radObjetivo;
    private javax.swing.JRadioButton radPerspectiva;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscRel5w2hFrame(int operation, String idAux, String contextId) {
        initComponents();

        cbbPessoa.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
        cbbPerspectiva.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
        cbbObjetivo.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
        cbbIniciativa.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());
        cbbSituacao.setLocale(bsc.core.BscStaticReferences.getBscDefaultLocale());

        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        //pnlConfirmation.setVisible(true);
        //pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        //pnlConfirmation.setVisible(false);
        //pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
