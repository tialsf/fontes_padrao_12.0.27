package bsc.swing.report;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import pv.jfcx.*;

public class BscRelBookFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("RELBOOKSTRA");
    //Cria��o das hashs para preenchimento das combos box.
    java.util.Hashtable hstOrganiza = new java.util.Hashtable(),
            hstEstrategia = new java.util.Hashtable(),
            hstPerspectiva = new java.util.Hashtable(),
            hstTema = new java.util.Hashtable(),
            hstIniciativa = new java.util.Hashtable(),
            hstTarefa = new java.util.Hashtable(),
            hstReunaio = new java.util.Hashtable(),
            hstObjetivo = new java.util.Hashtable(),
            hstIndicador = new java.util.Hashtable();

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        chkIncluirDescricao.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
/*		lblNome.setEnabled(false);
        fldNome.setEnabled(false);
        lblDescricao.setEnabled(false);
        txtDescricao.setEnabled(false);
        chkIncluirDescricao.setEnabled(false);
         */    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        // record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));

        //Preenchendo Combo-Box
        bookPopulateCombo(cmbOrganizaDe, cmbOrganizaAte, chkOrganiza, "ORGANIZACOES", hstOrganiza);
        bookPopulateCombo(cmbEstrategiaDe, cmbEstrategiaAte, chkEstrate, "ESTRATEGIAS", hstEstrategia);
        bookPopulateCombo(cmbPerspeDe, cmbPerspeAte, chkPerspe, "PERSPECTIVAS", hstPerspectiva);
        bookPopulateCombo(cmbTemaDe, cmbTemaAte, chkTema, "TEMASEST", hstTema);
        bookPopulateCombo(cmbObjetivoDe, cmbObjetivoAte, chkObjtetivo, "OBJETIVOS", hstObjetivo);
        bookPopulateCombo(cmbIndicadorDe, cmbIndicadorAte, chkIndicador, "INDICADORES", hstIndicador);
        bookPopulateCombo(cmbIniciativaDe, cmbIniciativaAte, chkIniciativa, "INICIATIVAS", hstIniciativa);
        bookPopulateCombo(cmbTarefaDe, cmbTarefaAte, chkTarefa, "TAREFAS", hstTarefa);
        bookPopulateCombo(cmbReuniaoDe, cmbReuniaoAte, chkReuniao, "REUNIOES", hstReunaio);

        //Setando a op��o que esta gravada no bd.
        bookSetItemCombo(cmbObjetivoDe, "ORGANIZACOES", "OBJDE");
        bookSetItemCombo(cmbObjetivoAte, "ORGANIZACOES", "OBJATE");
        //
        bookSetItemCombo(cmbEstrategiaDe, "ESTRATEGIAS", "ESTRADE");
        bookSetItemCombo(cmbEstrategiaAte, "ESTRATEGIAS", "ESTRAATE");
        //
        bookSetItemCombo(cmbPerspeDe, "PERSPECTIVAS", "PERSPDE");
        bookSetItemCombo(cmbPerspeAte, "PERSPECTIVAS", "PERSPATE");
        //
        bookSetItemCombo(cmbTemaDe, "TEMASEST", "TEMADE");
        bookSetItemCombo(cmbTemaAte, "TEMASEST", "TEMAATE");
        //
        bookSetItemCombo(cmbObjetivoDe, "OBJETIVOS", "OBJDE");
        bookSetItemCombo(cmbObjetivoAte, "OBJETIVOS", "OBJATE");
        //
        bookSetItemCombo(cmbIndicadorDe, "INDICADORES", "INDDE");
        bookSetItemCombo(cmbIndicadorAte, "INDICADORES", "INDATE");
        //
        bookSetItemCombo(cmbIniciativaDe, "INICIATIVAS", "INICIADE");
        bookSetItemCombo(cmbIniciativaAte, "INICIATIVAS", "INICIAATE");
        //
        bookSetItemCombo(cmbTarefaDe, "TAREFAS", "TAREFADE");
        bookSetItemCombo(cmbTarefaAte, "TAREFAS", "TAREFAATE");
        //
        bookSetItemCombo(cmbReuniaoDe, "REUNIOES", "REUNIADE");
        bookSetItemCombo(cmbReuniaoAte, "REUNIOES", "REUNIATE");


        //Relat�rios Selecionados
        chkIncluirDescricao.setSelected(record.getString("IMPDESC").equals("T"));
        chkOrganiza.setSelected(record.getString("IMPORGANIZ").equals("T"));
        chkEstrate.setSelected(record.getString("IMPESTRAT").equals("T"));
        chkPerspe.setSelected(record.getString("IMPPERSP").equals("T"));
        chkTema.setSelected(record.getString("IMPTEMA").equals("T"));
        chkObjtetivo.setSelected(record.getString("IMPOBJET").equals("T"));
        chkIndicador.setSelected(record.getString("IMPIND").equals("T"));
        chkIniciativa.setSelected(record.getString("IMPINICIAT").equals("T"));
        chkTarefa.setSelected(record.getString("IMPTAREFA").equals("T"));
        chkReuniao.setSelected(record.getString("IMPREUNIAO").equals("T"));
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        //Parametros para filtro do relat�rio.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());

        recordAux.set("ORGDE", event.getComboValue(hstOrganiza, cmbOrganizaDe));
        recordAux.set("ORGATE", event.getComboValue(hstOrganiza, cmbOrganizaDe));

        recordAux.set("ESTRADE", event.getComboValue(hstEstrategia, cmbEstrategiaDe));
        recordAux.set("ESTRAATE", event.getComboValue(hstEstrategia, cmbEstrategiaDe));

        recordAux.set("PERSPDE",
                cmbPerspeDe.getSelectedIndex() > cmbPerspeAte.getSelectedIndex()
                ? event.getComboValue(hstPerspectiva, cmbPerspeAte)
                : event.getComboValue(hstPerspectiva, cmbPerspeDe));

        recordAux.set("PERSPATE",
                cmbPerspeAte.getSelectedIndex() < cmbPerspeDe.getSelectedIndex()
                ? event.getComboValue(hstPerspectiva, cmbPerspeDe)
                : event.getComboValue(hstPerspectiva, cmbPerspeAte));

        recordAux.set("TEMADE",
                cmbTemaDe.getSelectedIndex() > cmbTemaAte.getSelectedIndex()
                ? event.getComboValue(hstTema, cmbTemaAte)
                : event.getComboValue(hstTema, cmbTemaDe));

        recordAux.set("TEMAATE",
                cmbTemaAte.getSelectedIndex() < cmbTemaDe.getSelectedIndex()
                ? event.getComboValue(hstTema, cmbTemaDe)
                : event.getComboValue(hstTema, cmbTemaAte));

        recordAux.set("OBJDE",
                cmbObjetivoDe.getSelectedIndex() > cmbObjetivoAte.getSelectedIndex()
                ? event.getComboValue(hstObjetivo, cmbObjetivoAte)
                : event.getComboValue(hstObjetivo, cmbObjetivoDe));

        recordAux.set("OBJATE",
                cmbObjetivoAte.getSelectedIndex() < cmbObjetivoDe.getSelectedIndex()
                ? event.getComboValue(hstObjetivo, cmbObjetivoDe)
                : event.getComboValue(hstObjetivo, cmbObjetivoAte));

        recordAux.set("INDDE",
                cmbIndicadorDe.getSelectedIndex() > cmbIndicadorAte.getSelectedIndex()
                ? event.getComboValue(hstIndicador, cmbIndicadorAte)
                : event.getComboValue(hstIndicador, cmbIndicadorDe));

        recordAux.set("INDATE",
                cmbIndicadorAte.getSelectedIndex() < cmbIndicadorDe.getSelectedIndex()
                ? event.getComboValue(hstIndicador, cmbIndicadorDe)
                : event.getComboValue(hstIndicador, cmbIndicadorAte));

        recordAux.set("INICIADE",
                cmbIniciativaDe.getSelectedIndex() > cmbIniciativaAte.getSelectedIndex()
                ? event.getComboValue(hstIniciativa, cmbIniciativaAte)
                : event.getComboValue(hstIniciativa, cmbIniciativaDe));

        recordAux.set("INICIAATE",
                cmbIniciativaAte.getSelectedIndex() < cmbIniciativaDe.getSelectedIndex()
                ? event.getComboValue(hstIniciativa, cmbIniciativaDe)
                : event.getComboValue(hstIniciativa, cmbIniciativaAte));

        recordAux.set("TAREFADE",
                cmbTarefaDe.getSelectedIndex() > cmbTarefaAte.getSelectedIndex()
                ? event.getComboValue(hstTarefa, cmbTarefaAte)
                : event.getComboValue(hstTarefa, cmbTarefaDe));

        recordAux.set("TAREFAATE",
                cmbTarefaAte.getSelectedIndex() < cmbTarefaDe.getSelectedIndex()
                ? event.getComboValue(hstTarefa, cmbTarefaDe)
                : event.getComboValue(hstTarefa, cmbTarefaAte));

        recordAux.set("REUNIADE",
                cmbReuniaoDe.getSelectedIndex() > cmbReuniaoAte.getSelectedIndex()
                ? event.getComboValue(hstReunaio, cmbReuniaoAte)
                : event.getComboValue(hstReunaio, cmbReuniaoDe));

        recordAux.set("REUNIATE",
                cmbReuniaoAte.getSelectedIndex() < cmbReuniaoDe.getSelectedIndex()
                ? event.getComboValue(hstReunaio, cmbReuniaoDe)
                : event.getComboValue(hstReunaio, cmbReuniaoAte));

        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected() ? "T" : "F");
        recordAux.set("IMPORGANIZ", chkOrganiza.isSelected() ? "T" : "F");// Imprime Organizacao?
        recordAux.set("IMPESTRAT", chkEstrate.isSelected() ? "T" : "F");// Imprime Estrategia?
        recordAux.set("IMPPERSP", chkPerspe.isSelected() ? "T" : "F");// Imprime Pespectiva?
        recordAux.set("IMPTEMA", chkTema.isSelected() ? "T" : "F");// Imprime Tema?
        recordAux.set("IMPOBJET", chkObjtetivo.isSelected() ? "T" : "F");// Imprime Objetivo?
        recordAux.set("IMPIND", chkIndicador.isSelected() ? "T" : "F");// Imprime Indicador?
        recordAux.set("IMPINICIAT", chkIniciativa.isSelected() ? "T" : "F");// Imprime Iniciativa?
        recordAux.set("IMPTAREFA", chkTarefa.isSelected() ? "T" : "F");// Imprime Tarefa?
        recordAux.set("IMPREUNIAO", chkReuniao.isSelected() ? "T" : "F");// Imprime Reuniao?

        System.out.println("Valor do registro" + recordAux);
        return recordAux;

    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        srlFiltros = new javax.swing.JScrollPane();
        pnlFiltros = new javax.swing.JPanel();
        lblTitImprimi = new javax.swing.JLabel();
        lblTitDe = new javax.swing.JLabel();
        lblTitAte = new javax.swing.JLabel();
        chkOrganiza = new javax.swing.JCheckBox();
        cmbOrganizaDe = new javax.swing.JComboBox();
        cmbOrganizaAte = new javax.swing.JComboBox();
        chkEstrate = new javax.swing.JCheckBox();
        cmbEstrategiaDe = new javax.swing.JComboBox();
        cmbEstrategiaAte = new javax.swing.JComboBox();
        chkPerspe = new javax.swing.JCheckBox();
        cmbPerspeDe = new javax.swing.JComboBox();
        cmbPerspeAte = new javax.swing.JComboBox();
        chkTema = new javax.swing.JCheckBox();
        cmbTemaDe = new javax.swing.JComboBox();
        cmbTemaAte = new javax.swing.JComboBox();
        chkObjtetivo = new javax.swing.JCheckBox();
        cmbObjetivoDe = new javax.swing.JComboBox();
        cmbObjetivoAte = new javax.swing.JComboBox();
        chkIndicador = new javax.swing.JCheckBox();
        cmbIndicadorDe = new javax.swing.JComboBox();
        cmbIndicadorAte = new javax.swing.JComboBox();
        chkIniciativa = new javax.swing.JCheckBox();
        cmbIniciativaDe = new javax.swing.JComboBox();
        cmbIniciativaAte = new javax.swing.JComboBox();
        chkTarefa = new javax.swing.JCheckBox();
        cmbTarefaDe = new javax.swing.JComboBox();
        cmbTarefaAte = new javax.swing.JComboBox();
        chkReuniao = new javax.swing.JCheckBox();
        cmbReuniaoDe = new javax.swing.JComboBox();
        cmbReuniaoAte = new javax.swing.JComboBox();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        pnlTopTool = new javax.swing.JPanel();
        pnlBottomTool = new javax.swing.JPanel();
        pnlCenterTool = new javax.swing.JPanel();
        pnlLeftTool = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscRelBookFrame_00025")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setPreferredSize(new java.awt.Dimension(484, 499));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scpRecord.setAutoscrolls(true);
        scpRecord.setPreferredSize(new java.awt.Dimension(464, 466));

        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscRelBookFrame_00001")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 0, 98, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscRelBookFrame_00002")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 76, 20);

        chkIncluirDescricao.setText(bundle.getString("BscRelBookFrame_00022")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 120, 254, 20);

        txtDescricao.setColumns(80);
        txtDescricao.setRows(0);
        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(10, 60, 400, 60);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        pnlFiltros.setMinimumSize(new java.awt.Dimension(460, 250));
        pnlFiltros.setPreferredSize(new java.awt.Dimension(430, 190));
        pnlFiltros.setRequestFocusEnabled(false);
        pnlFiltros.setLayout(new java.awt.GridLayout(10, 3, 2, 2));

        lblTitImprimi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitImprimi.setText(bundle.getString("BscRelBookFrame_00004")); // NOI18N
        lblTitImprimi.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblTitImprimi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblTitImprimi.setMaximumSize(new java.awt.Dimension(150, 15));
        lblTitImprimi.setMinimumSize(new java.awt.Dimension(150, 15));
        lblTitImprimi.setPreferredSize(new java.awt.Dimension(150, 15));
        lblTitImprimi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTitImprimiMouseClicked(evt);
            }
        });
        pnlFiltros.add(lblTitImprimi);

        lblTitDe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitDe.setText(bundle.getString("BscRelBookFrame_00005")); // NOI18N
        lblTitDe.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblTitDe.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblTitDe.setMaximumSize(new java.awt.Dimension(60, 17));
        lblTitDe.setMinimumSize(new java.awt.Dimension(60, 17));
        lblTitDe.setPreferredSize(new java.awt.Dimension(60, 17));
        pnlFiltros.add(lblTitDe);

        lblTitAte.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitAte.setText(bundle.getString("BscRelBookFrame_00006")); // NOI18N
        lblTitAte.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblTitAte.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblTitAte.setMaximumSize(new java.awt.Dimension(60, 17));
        lblTitAte.setMinimumSize(new java.awt.Dimension(60, 17));
        lblTitAte.setPreferredSize(new java.awt.Dimension(60, 17));
        pnlFiltros.add(lblTitAte);

        chkOrganiza.setText("Organiza��o");
        chkOrganiza.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkOrganiza.setBorderPainted(true);
        chkOrganiza.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkOrganiza.setLabel(bundle.getString("BscRelBookFrame_00007")); // NOI18N
        chkOrganiza.setMaximumSize(new java.awt.Dimension(59, 17));
        chkOrganiza.setMinimumSize(new java.awt.Dimension(59, 17));
        chkOrganiza.setPreferredSize(new java.awt.Dimension(59, 17));
        pnlFiltros.add(chkOrganiza);

        cmbOrganizaDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbOrganizaDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbOrganizaDe);

        cmbOrganizaAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbOrganizaAte.setPreferredSize(new java.awt.Dimension(22, 17));
        cmbOrganizaAte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOrganizaAteActionPerformed(evt);
            }
        });
        pnlFiltros.add(cmbOrganizaAte);

        chkEstrate.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkEstrate.setBorderPainted(true);
        chkEstrate.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkEstrate.setLabel(bundle.getString("BscRelBookFrame_00008")); // NOI18N
        chkEstrate.setMaximumSize(new java.awt.Dimension(59, 17));
        chkEstrate.setMinimumSize(new java.awt.Dimension(59, 17));
        chkEstrate.setPreferredSize(new java.awt.Dimension(59, 17));
        pnlFiltros.add(chkEstrate);

        cmbEstrategiaDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbEstrategiaDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbEstrategiaDe);

        cmbEstrategiaAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbEstrategiaAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbEstrategiaAte);

        chkPerspe.setText("Perspectiva");
        chkPerspe.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkPerspe.setBorderPainted(true);
        chkPerspe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkPerspe.setLabel(bundle.getString("BscRelBookFrame_00009")); // NOI18N
        chkPerspe.setMaximumSize(new java.awt.Dimension(59, 17));
        chkPerspe.setMinimumSize(new java.awt.Dimension(59, 17));
        chkPerspe.setPreferredSize(new java.awt.Dimension(59, 17));
        pnlFiltros.add(chkPerspe);

        cmbPerspeDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbPerspeDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbPerspeDe);

        cmbPerspeAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbPerspeAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbPerspeAte);

        chkTema.setText("Tema");
        chkTema.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkTema.setBorderPainted(true);
        chkTema.setLabel(bundle.getString("BscRelBookFrame_00010")); // NOI18N
        pnlFiltros.add(chkTema);
        pnlFiltros.add(cmbTemaDe);
        pnlFiltros.add(cmbTemaAte);

        chkObjtetivo.setText("Objetivo");
        chkObjtetivo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkObjtetivo.setBorderPainted(true);
        chkObjtetivo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkObjtetivo.setLabel(bundle.getString("BscRelBookFrame_00011")); // NOI18N
        pnlFiltros.add(chkObjtetivo);

        cmbObjetivoDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbObjetivoDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbObjetivoDe);

        cmbObjetivoAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbObjetivoAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbObjetivoAte);

        chkIndicador.setText("Indicador");
        chkIndicador.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkIndicador.setBorderPainted(true);
        chkIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        chkIndicador.setLabel(bundle.getString("BscRelBookFrame_00012")); // NOI18N
        chkIndicador.setMaximumSize(new java.awt.Dimension(59, 17));
        chkIndicador.setMinimumSize(new java.awt.Dimension(59, 17));
        chkIndicador.setPreferredSize(new java.awt.Dimension(59, 17));
        pnlFiltros.add(chkIndicador);

        cmbIndicadorDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIndicadorDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbIndicadorDe);

        cmbIndicadorAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIndicadorAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbIndicadorAte);

        chkIniciativa.setText("Iniciativa");
        chkIniciativa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkIniciativa.setBorderPainted(true);
        chkIniciativa.setLabel(bundle.getString("BscRelBookFrame_00013")); // NOI18N
        pnlFiltros.add(chkIniciativa);

        cmbIniciativaDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIniciativaDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbIniciativaDe);

        cmbIniciativaAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIniciativaAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbIniciativaAte);

        chkTarefa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkTarefa.setBorderPainted(true);
        chkTarefa.setLabel(bundle.getString("BscRelBookFrame_00014")); // NOI18N
        pnlFiltros.add(chkTarefa);

        cmbTarefaDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbTarefaDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbTarefaDe);

        cmbTarefaAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbTarefaAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbTarefaAte);

        chkReuniao.setText("Reuni�o");
        chkReuniao.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        chkReuniao.setBorderPainted(true);
        chkReuniao.setLabel(bundle.getString("BscRelBookFrame_00015")); // NOI18N
        pnlFiltros.add(chkReuniao);

        cmbReuniaoDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbReuniaoDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbReuniaoDe);

        cmbReuniaoAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbReuniaoAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlFiltros.add(cmbReuniaoAte);

        srlFiltros.setViewportView(pnlFiltros);

        pnlData.add(srlFiltros);
        srlFiltros.setBounds(10, 140, 650, 260);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(20, 30));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(150, 150));

        btnGeraRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("BscRel5w2hFrame_00019")); // NOI18N
        btnGeraRel.setPreferredSize(new java.awt.Dimension(100, 29));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("BscRel5w2hFrame_00020")); // NOI18N
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(100, 29));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        pnlTopRecord.add(tbInsertion, java.awt.BorderLayout.EAST);

        pnlTopTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlTopTool, java.awt.BorderLayout.PAGE_START);

        pnlBottomTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlBottomTool, java.awt.BorderLayout.SOUTH);
        pnlTopRecord.add(pnlCenterTool, java.awt.BorderLayout.CENTER);
        pnlTopRecord.add(pnlLeftTool, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.PAGE_START);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscRelBookFrame_00025"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void lblTitImprimiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTitImprimiMouseClicked
            if (chkOrganiza.isSelected()) {
                bookSelectCheck(chkOrganiza, false);
                bookSelectCheck(chkEstrate, false);
                bookSelectCheck(chkPerspe, false);
                bookSelectCheck(chkTema, false);
                bookSelectCheck(chkObjtetivo, false);
                bookSelectCheck(chkIndicador, false);
                bookSelectCheck(chkIniciativa, false);
                bookSelectCheck(chkTarefa, false);
                bookSelectCheck(chkReuniao, false);
            } else {
                bookSelectCheck(chkOrganiza, true);
                bookSelectCheck(chkEstrate, true);
                bookSelectCheck(chkPerspe, true);
                bookSelectCheck(chkTema, true);
                bookSelectCheck(chkObjtetivo, true);
                bookSelectCheck(chkIndicador, true);
                bookSelectCheck(chkIniciativa, true);
                bookSelectCheck(chkTarefa, true);
                bookSelectCheck(chkReuniao, true);

                //Seleciona todos os itens das combos.
                bookSetAllCombos(cmbPerspeDe, cmbPerspeAte);
                bookSetAllCombos(cmbTemaDe, cmbTemaAte);
                bookSetAllCombos(cmbObjetivoDe, cmbObjetivoAte);
                bookSetAllCombos(cmbIndicadorDe, cmbIndicadorAte);
                bookSetAllCombos(cmbIniciativaDe, cmbIniciativaAte);
                bookSetAllCombos(cmbTarefaDe, cmbTarefaAte);
                bookSetAllCombos(cmbReuniaoDe, cmbReuniaoAte);

            }
            // TODO add your handling code here:
	}//GEN-LAST:event_lblTitImprimiMouseClicked

        private void cmbOrganizaAteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOrganizaAteActionPerformed
            // TODO add your handling code here:
        }//GEN-LAST:event_cmbOrganizaAteActionPerformed

        private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            event.executeRecord("SALVAR;rel055_" + record.getString("ID") + ".html");
}//GEN-LAST:event_btnGeraRelActionPerformed

        private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            try {
                bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString().concat("bscreport.apw?id=rel055_".concat(this.id)));
                bscApplet.getAppletContext().showDocument(url, "_blank");
            } catch (Exception e) {
                e.printStackTrace();
            }
}//GEN-LAST:event_btnVizualizaRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JCheckBox chkEstrate;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private javax.swing.JCheckBox chkIndicador;
    private javax.swing.JCheckBox chkIniciativa;
    private javax.swing.JCheckBox chkObjtetivo;
    private javax.swing.JCheckBox chkOrganiza;
    private javax.swing.JCheckBox chkPerspe;
    private javax.swing.JCheckBox chkReuniao;
    private javax.swing.JCheckBox chkTarefa;
    private javax.swing.JCheckBox chkTema;
    private javax.swing.JComboBox cmbEstrategiaAte;
    private javax.swing.JComboBox cmbEstrategiaDe;
    private javax.swing.JComboBox cmbIndicadorAte;
    private javax.swing.JComboBox cmbIndicadorDe;
    private javax.swing.JComboBox cmbIniciativaAte;
    private javax.swing.JComboBox cmbIniciativaDe;
    private javax.swing.JComboBox cmbObjetivoAte;
    private javax.swing.JComboBox cmbObjetivoDe;
    private javax.swing.JComboBox cmbOrganizaAte;
    private javax.swing.JComboBox cmbOrganizaDe;
    private javax.swing.JComboBox cmbPerspeAte;
    private javax.swing.JComboBox cmbPerspeDe;
    private javax.swing.JComboBox cmbReuniaoAte;
    private javax.swing.JComboBox cmbReuniaoDe;
    private javax.swing.JComboBox cmbTarefaAte;
    private javax.swing.JComboBox cmbTarefaDe;
    private javax.swing.JComboBox cmbTemaAte;
    private javax.swing.JComboBox cmbTemaDe;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblTitAte;
    private javax.swing.JLabel lblTitDe;
    private javax.swing.JLabel lblTitImprimi;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlBottomTool;
    private javax.swing.JPanel pnlCenterTool;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlFiltros;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftTool;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTopTool;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JScrollPane srlFiltros;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    /**
     * Verifica se o relat�rio pode ser selecionado.
     */
    private void bookSelectCheck(JCheckBox chkRelatorio, boolean bSelect) {
        if (chkRelatorio.isEnabled()) {
            chkRelatorio.setSelected(bSelect);
        }
    }

    /**
     * Seta a combo de e Ate para selecionar todos os itens
     */
    private void bookSetAllCombos(javax.swing.JComboBox cmbComboDe, javax.swing.JComboBox cmbComboAte) {
        if (cmbComboAte.getItemCount() > 0 && cmbComboDe.getItemCount() > 0) {
            cmbComboDe.setSelectedIndex(0);
            cmbComboAte.setSelectedIndex(cmbComboAte.getItemCount() - 1);
        }
    }

    /**
     * Posiciona o item da combo.
     */
    private void bookSetItemCombo(javax.swing.JComboBox cmbCombo, String cmbItens, String KeyName) {
        int retItem = event.getComboItem(record.getBIXMLVector(cmbItens), record.getString(KeyName), false);
        if (retItem != -1 && cmbCombo.getItemCount() > 0) {
            cmbCombo.setSelectedIndex(retItem);
        } else {
        }
    }

    /**
     * M�todo para preencher as combos;
     */
    private void bookPopulateCombo(JComboBox cmbComboDe, JComboBox cmbComboAte, JCheckBox chkRelatorio, String strCmbTag, java.util.Hashtable hstCmbItens) {
        bsc.xml.BIXMLVector vctItens = record.getBIXMLVector(strCmbTag);
        String tagIDde = strCmbTag.substring(0, 2).concat("IDDE"),
                tagIDAte = strCmbTag.substring(0, 2).concat("IDATE");

        //Fazendo a populariza��o da combo.
        if (vctItens.size() > 0) {
            event.populateCombo(vctItens, tagIDde, hstCmbItens, cmbComboDe);
            event.populateCombo(vctItens, tagIDAte, hstCmbItens, cmbComboAte);

            if (strCmbTag.equals("ORGANIZACOES") || strCmbTag.equals("ESTRATEGIAS")) {
                cmbComboDe.setEnabled(false);
                cmbComboAte.setEnabled(false);
            }

            cmbComboDe.removeItemAt(0);
            cmbComboAte.removeItemAt(0);
            chkRelatorio.setEnabled(true);

        } else {
            cmbComboDe.setEnabled(false);
            cmbComboAte.setEnabled(false);
            chkRelatorio.setSelected(false);
            chkRelatorio.setEnabled(false);
        }
    }

    public BscRelBookFrame(int operation, String idAux, String contextId) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);

        event.editRecord(); // Relatorio por default edit
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        //pnlConfirmation.setVisible(true);
        //pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        //pnlConfirmation.setVisible(false);
        //pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
