package bsc.swing.report;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import pv.jfcx.*;

public class BscRelEvolucao extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("RELEVOL");
    //Cria��o das hashs para preenchimento das combos box.
    java.util.Hashtable hstPerspectivaDe = new java.util.Hashtable(),
            hstPerspectivaAte = new java.util.Hashtable(),
            hstObjetivoDe = new java.util.Hashtable(),
            hstObjetivoAte = new java.util.Hashtable(),
            hstIndicadorDe = new java.util.Hashtable(),
            hstIndicadorAte = new java.util.Hashtable(),
            hstOrdemObj = new java.util.Hashtable(),
            hstOrdemInd = new java.util.Hashtable();

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        chkIncluirDescricao.setEnabled(true);
        chkIncluirReferencia.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
    }

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        // record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));

        event.populateCombo(record.getBIXMLVector("PERSPECTIVAS"), "PERSPDE", hstPerspectivaDe, cmbPerspeDe);
        event.populateCombo(record.getBIXMLVector("PERSPECTIVAS"), "PERSPATE", hstPerspectivaAte, cmbPerspeAte);
        //
        event.populateCombo(record.getBIXMLVector("OBJETIVOS"), "OBJDE", hstObjetivoDe, cmbObjetivoDe);
        event.populateCombo(record.getBIXMLVector("OBJETIVOS"), "OBJATE", hstObjetivoAte, cmbObjetivoAte);
        //
        event.populateCombo(record.getBIXMLVector("INDICADORES"), "INDDE", hstIndicadorDe, cmbIndicadorDe);
        event.populateCombo(record.getBIXMLVector("INDICADORES"), "INDATE", hstIndicadorAte, cmbIndicadorAte);
        //
        event.populateCombo(record.getBIXMLVector("ORDENSOBJ"), "ORDEMOBJ", hstOrdemObj, cmbOrdemObj);
        event.populateCombo(record.getBIXMLVector("ORDENSIND"), "ORDEMIND", hstOrdemInd, cmbOrdemInd);

        //Relat�rios Selecionados
        chkIncluirDescricao.setSelected(record.getString("IMPDESC").equals("T"));
        chkIncluirReferencia.setSelected(record.getString("IMPREF").equals("T"));

        fldDataIni.setCalendar(record.getDate("DATADE"));
        fldDataFin.setCalendar(record.getDate("DATAATE"));
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        //Parametros para filtro do relat�rio.
        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("PERSPDE", event.getComboValue(hstPerspectivaDe, cmbPerspeDe));
        recordAux.set("PERSPATE", event.getComboValue(hstPerspectivaAte, cmbPerspeAte));
        recordAux.set("OBJDE", event.getComboValue(hstObjetivoDe, cmbObjetivoDe));
        recordAux.set("OBJATE", event.getComboValue(hstObjetivoAte, cmbObjetivoAte));
        recordAux.set("INDDE", event.getComboValue(hstIndicadorDe, cmbIndicadorDe));
        recordAux.set("INDATE", event.getComboValue(hstIndicadorAte, cmbIndicadorAte));

        recordAux.set("ORDEMOBJ", event.getComboValue(hstOrdemObj, cmbOrdemObj));
        recordAux.set("ORDEMIND", event.getComboValue(hstOrdemInd, cmbOrdemInd));

        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected() ? "T" : "F");
        recordAux.set("IMPREF", chkIncluirReferencia.isSelected() ? "T" : "F");
        recordAux.set("DATADE", fldDataIni.getCalendar());
        recordAux.set("DATAATE", fldDataFin.getCalendar());

        return recordAux;

    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        lblPerspectiva = new javax.swing.JLabel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        fldNome = new pv.jfcx.JPVEdit();
        chkIncluirReferencia = new javax.swing.JCheckBox();
        lblDataIni = new javax.swing.JLabel();
        fldDataIni = new pv.jfcx.JPVDatePlus();
        fldDataFin = new pv.jfcx.JPVDatePlus();
        lblDataFin = new javax.swing.JLabel();
        cmbPerspeDe = new javax.swing.JComboBox();
        cmbPerspeAte = new javax.swing.JComboBox();
        lblObjetivo = new javax.swing.JLabel();
        cmbObjetivoDe = new javax.swing.JComboBox();
        cmbObjetivoAte = new javax.swing.JComboBox();
        cmbIndicadorDe = new javax.swing.JComboBox();
        cmbIndicadorAte = new javax.swing.JComboBox();
        cmbOrdemInd = new javax.swing.JComboBox();
        cmbOrdemObj = new javax.swing.JComboBox();
        lblOrdemInd = new javax.swing.JLabel();
        lblOrdemObj = new javax.swing.JLabel();
        lblIndicador = new javax.swing.JLabel();
        lblAte = new javax.swing.JLabel();
        lblAte1 = new javax.swing.JLabel();
        lblAte2 = new javax.swing.JLabel();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        pnlTopTool = new javax.swing.JPanel();
        pnlBottomTool = new javax.swing.JPanel();
        pnlCenterTool = new javax.swing.JPanel();
        pnlLeftTool = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscRelEvolFrame_00009")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 560, 420));
        setPreferredSize(new java.awt.Dimension(560, 420));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setPreferredSize(new java.awt.Dimension(484, 499));
        pnlFields.setLayout(new java.awt.BorderLayout());
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scpRecord.setPreferredSize(new java.awt.Dimension(464, 466));

        pnlData.setAutoscrolls(true);
        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscRelBookFrame_00001")); // NOI18N
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 0, 87, 20);
        lblNome.getAccessibleContext().setAccessibleName(bundle.getString("BscRelEvolFrame_00002")); // NOI18N

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscRelBookFrame_00002")); // NOI18N
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(20, 40, 76, 20);
        lblDescricao.getAccessibleContext().setAccessibleName(bundle.getString("BscRelEvolFrame_00010")); // NOI18N

        lblPerspectiva.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPerspectiva.setText(bundle.getString("BscRelEvolFrame_00014")); // NOI18N
        lblPerspectiva.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblPerspectiva);
        lblPerspectiva.setBounds(20, 210, 93, 20);

        chkIncluirDescricao.setText(bundle.getString("BscRelEvolFrame_00005")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(20, 120, 190, 23);

        txtDescricao.setColumns(80);
        txtDescricao.setRows(0);
        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 400, 50);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        chkIncluirReferencia.setText(bundle.getString("BscRelEvolFrame_00012")); // NOI18N
        chkIncluirReferencia.setEnabled(false);
        pnlData.add(chkIncluirReferencia);
        chkIncluirReferencia.setBounds(20, 140, 180, 23);

        lblDataIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataIni.setText(bundle.getString("BscRelEvolFrame_00013")); // NOI18N
        pnlData.add(lblDataIni);
        lblDataIni.setBounds(20, 170, 75, 20);

        fldDataIni.setDialog(true);
        fldDataIni.setUseLocale(true);
        fldDataIni.setWaitForCalendarDate(true);
        pnlData.add(fldDataIni);
        fldDataIni.setBounds(20, 190, 180, 22);

        fldDataFin.setDialog(true);
        fldDataFin.setUseLocale(true);
        fldDataFin.setWaitForCalendarDate(true);
        pnlData.add(fldDataFin);
        fldDataFin.setBounds(240, 190, 180, 22);

        lblDataFin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDataFin.setLabelFor(fldDataFin);
        lblDataFin.setText(bundle.getString("BscRelEvolFrame_00017")); // NOI18N
        pnlData.add(lblDataFin);
        lblDataFin.setBounds(240, 170, 20, 18);

        cmbPerspeDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbPerspeDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbPerspeDe);
        cmbPerspeDe.setBounds(20, 230, 180, 22);

        cmbPerspeAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbPerspeAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbPerspeAte);
        cmbPerspeAte.setBounds(240, 230, 180, 22);

        lblObjetivo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblObjetivo.setText(bundle.getString("BscRelEvolFrame_00015")); // NOI18N
        lblObjetivo.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblObjetivo);
        lblObjetivo.setBounds(20, 250, 93, 20);

        cmbObjetivoDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbObjetivoDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbObjetivoDe);
        cmbObjetivoDe.setBounds(20, 270, 180, 22);

        cmbObjetivoAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbObjetivoAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbObjetivoAte);
        cmbObjetivoAte.setBounds(240, 270, 180, 22);

        cmbIndicadorDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIndicadorDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbIndicadorDe);
        cmbIndicadorDe.setBounds(20, 310, 180, 22);

        cmbIndicadorAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbIndicadorAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbIndicadorAte);
        cmbIndicadorAte.setBounds(240, 310, 180, 22);

        cmbOrdemInd.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbOrdemInd.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbOrdemInd);
        cmbOrdemInd.setBounds(240, 350, 180, 22);

        cmbOrdemObj.setMinimumSize(new java.awt.Dimension(22, 17));
        cmbOrdemObj.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cmbOrdemObj);
        cmbOrdemObj.setBounds(20, 350, 180, 22);

        lblOrdemInd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrdemInd.setText(bundle.getString("BscRelEvolFrame_00019")); // NOI18N
        lblOrdemInd.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblOrdemInd);
        lblOrdemInd.setBounds(240, 330, 140, 20);

        lblOrdemObj.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrdemObj.setText(bundle.getString("BscRelEvolFrame_00018")); // NOI18N
        lblOrdemObj.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblOrdemObj);
        lblOrdemObj.setBounds(20, 330, 120, 20);

        lblIndicador.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblIndicador.setText(bundle.getString("BscRelEvolFrame_00016")); // NOI18N
        lblIndicador.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblIndicador);
        lblIndicador.setBounds(20, 290, 93, 20);

        lblAte.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAte.setLabelFor(fldDataFin);
        lblAte.setText(bundle.getString("BscRelEvolFrame_00017")); // NOI18N
        pnlData.add(lblAte);
        lblAte.setBounds(240, 210, 20, 18);

        lblAte1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAte1.setLabelFor(fldDataFin);
        lblAte1.setText(bundle.getString("BscRelEvolFrame_00017")); // NOI18N
        pnlData.add(lblAte1);
        lblAte1.setBounds(240, 250, 20, 18);

        lblAte2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAte2.setLabelFor(fldDataFin);
        lblAte2.setText(bundle.getString("BscRelEvolFrame_00017")); // NOI18N
        pnlData.add(lblAte2);
        lblAte2.setBounds(240, 290, 20, 18);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(20, 30));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(200, 200));

        btnGeraRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("BscRel5w2hFrame_00019")); // NOI18N
        btnGeraRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("BscRel5w2hFrame_00020")); // NOI18N
        btnVizualizaRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        pnlTopRecord.add(tbInsertion, java.awt.BorderLayout.EAST);

        pnlTopTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlTopTool, java.awt.BorderLayout.PAGE_START);

        pnlBottomTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlBottomTool, java.awt.BorderLayout.SOUTH);
        pnlTopRecord.add(pnlCenterTool, java.awt.BorderLayout.CENTER);
        pnlTopRecord.add(pnlLeftTool, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.PAGE_START);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscRelEvolFrame_00009"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
        event.executeRecord("SALVAR;rel056_" + record.getString("ID") + ".html");
}//GEN-LAST:event_btnGeraRelActionPerformed

    private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
        try {
            bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
            java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString().concat("bscreport.apw?id=rel056_".concat(this.id)));
            bscApplet.getAppletContext().showDocument(url, "_blank");
        } catch (Exception e) {
            e.printStackTrace();
        }
}//GEN-LAST:event_btnVizualizaRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private javax.swing.JCheckBox chkIncluirReferencia;
    private javax.swing.JComboBox cmbIndicadorAte;
    private javax.swing.JComboBox cmbIndicadorDe;
    private javax.swing.JComboBox cmbObjetivoAte;
    private javax.swing.JComboBox cmbObjetivoDe;
    private javax.swing.JComboBox cmbOrdemInd;
    private javax.swing.JComboBox cmbOrdemObj;
    private javax.swing.JComboBox cmbPerspeAte;
    private javax.swing.JComboBox cmbPerspeDe;
    private pv.jfcx.JPVDatePlus fldDataFin;
    private pv.jfcx.JPVDatePlus fldDataIni;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAte;
    private javax.swing.JLabel lblAte1;
    private javax.swing.JLabel lblAte2;
    private javax.swing.JLabel lblDataFin;
    private javax.swing.JLabel lblDataIni;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblIndicador;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblObjetivo;
    private javax.swing.JLabel lblOrdemInd;
    private javax.swing.JLabel lblOrdemObj;
    private javax.swing.JLabel lblPerspectiva;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlBottomTool;
    private javax.swing.JPanel pnlCenterTool;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftTool;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTopTool;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    /**
     * Seta a combo de e Ate para selecionar todos os itens
     */
    private void bookSetAllCombos(javax.swing.JComboBox cmbComboDe, javax.swing.JComboBox cmbComboAte) {
        if (cmbComboAte.getItemCount() > 0 && cmbComboDe.getItemCount() > 0) {
            cmbComboDe.setSelectedIndex(0);
            cmbComboAte.setSelectedIndex(cmbComboAte.getItemCount() - 1);
        }
    }

    /**
     * Posiciona o item da combo.
     */
    private void bookSetItemCombo(javax.swing.JComboBox cmbCombo, String cmbItens, String KeyName) {
        int retItem = event.getComboItem(record.getBIXMLVector(cmbItens), record.getString(KeyName), false);
        if (retItem != -1 && cmbCombo.getItemCount() > 0) {
            cmbCombo.setSelectedIndex(retItem);
        } else {
        }
    }

    public BscRelEvolucao(int operation, String idAux, String contextId) {
        initComponents();

        event.defaultConstructor(operation, idAux, contextId);

        event.editRecord(); // Relatorio por default edit

    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        //pnlConfirmation.setVisible(true);
        //pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        //pnlConfirmation.setVisible(false);
        //pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
