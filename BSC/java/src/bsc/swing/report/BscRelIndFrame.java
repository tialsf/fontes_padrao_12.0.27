package bsc.swing.report;

import java.util.ResourceBundle;

public class BscRelIndFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("RELIND");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        chkIncluirDescricao.setEnabled(true);
        lblDataIni.setEnabled(true);
        fldDataIni.setEnabled(true);
        lblDataFin.setEnabled(true);
        fldDataFin.setEnabled(true);
        lblPerspectivaIni.setEnabled(true);
        lblPerspectivaFin.setEnabled(true);
        cbPerspectivaAte.setEnabled(true);
        cbPerspectivaDe.setEnabled(true);
        lblSituacao.setEnabled(true);
        cbSituacao.setEnabled(true);
        lblOrdemObj.setEnabled(true);
        lblOrdemInd.setEnabled(true);
        cbOrdemObj.setEnabled(true);
        cbOrdemInd.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
    }
    java.util.Hashtable htbPerspectivaDe = new java.util.Hashtable();
    java.util.Hashtable htbPerspectivaAte = new java.util.Hashtable();
    java.util.Hashtable htbSituacoes = new java.util.Hashtable();
    java.util.Hashtable htbOrdemObj = new java.util.Hashtable();
    java.util.Hashtable htbOrdemInd = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        fldDataAlvo.setCalendar(record.getDate("DATAALVO"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));
        fldDataIni.setCalendar(record.getDate("DEDATA"));
        fldDataFin.setCalendar(record.getDate("ATEDATA"));
        event.populateCombo(record.getBIXMLVector("PERSPECTIVAS"), "DEPERSPEC", htbPerspectivaDe, cbPerspectivaDe);
        event.populateCombo(record.getBIXMLVector("PERSPECTIVAS"), "ATEPERSPEC", htbPerspectivaAte, cbPerspectivaAte);
        event.populateCombo(record.getBIXMLVector("SITUACOES"), "SITUACAO", htbSituacoes, cbSituacao);
        event.populateCombo(record.getBIXMLVector("ORDENSOBJ"), "ORDEMOBJ", htbOrdemObj, cbOrdemObj);
        event.populateCombo(record.getBIXMLVector("ORDENSIND"), "ORDEMIND", htbOrdemInd, cbOrdemInd);
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("DEDATA", fldDataIni.getCalendar());
        recordAux.set("ATEDATA", fldDataFin.getCalendar());
        recordAux.set("DEPERSPEC", event.getComboValue(htbPerspectivaDe, cbPerspectivaDe));
        recordAux.set("ATEPERSPEC", event.getComboValue(htbPerspectivaAte, cbPerspectivaAte));
        recordAux.set("SITUACAO", event.getComboValue(htbSituacoes, cbSituacao));
        recordAux.set("ORDEMOBJ", event.getComboValue(htbOrdemObj, cbOrdemObj));
        recordAux.set("ORDEMIND", event.getComboValue(htbOrdemInd, cbOrdemInd));

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        lblDataIni = new javax.swing.JLabel();
        lblDataFin = new javax.swing.JLabel();
        lblPerspectivaIni = new javax.swing.JLabel();
        lblPerspectivaFin = new javax.swing.JLabel();
        fldNome = new pv.jfcx.JPVEdit();
        fldDataIni = new pv.jfcx.JPVDatePlus();
        fldDataFin = new pv.jfcx.JPVDatePlus();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        cbPerspectivaDe = new javax.swing.JComboBox();
        cbPerspectivaAte = new javax.swing.JComboBox();
        lblSituacao = new javax.swing.JLabel();
        cbSituacao = new javax.swing.JComboBox();
        lblDataAlvo = new javax.swing.JLabel();
        fldDataAlvo = new pv.jfcx.JPVDate();
        btnAvaliacao = new javax.swing.JButton();
        cbOrdemObj = new javax.swing.JComboBox();
        lblOrdemObj = new javax.swing.JLabel();
        lblOrdemInd = new javax.swing.JLabel();
        cbOrdemInd = new javax.swing.JComboBox();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        pnlTopTool = new javax.swing.JPanel();
        pnlBottomTool = new javax.swing.JPanel();
        pnlCenterTool = new javax.swing.JPanel();
        pnlLeftTool = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscRelIndFrame_00014")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scpRecord.setAutoscrolls(true);

        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscRelIndFrame_00013")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(20, 0, 100, 18);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscRelIndFrame_00004")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(20, 40, 100, 18);

        chkIncluirDescricao.setText(bundle.getString("BscRelIndFrame_00003")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(20, 130, 180, 20);

        lblDataIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataIni.setText(bundle.getString("BscRelIndFrame_00015")); // NOI18N
        lblDataIni.setEnabled(false);
        pnlData.add(lblDataIni);
        lblDataIni.setBounds(20, 160, 100, 18);

        lblDataFin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDataFin.setText(bundle.getString("BscRelIndFrame_00005")); // NOI18N
        lblDataFin.setEnabled(false);
        pnlData.add(lblDataFin);
        lblDataFin.setBounds(240, 160, 20, 18);

        lblPerspectivaIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPerspectivaIni.setText(bundle.getString("BscRelIndFrame_00009")); // NOI18N
        lblPerspectivaIni.setEnabled(false);
        lblPerspectivaIni.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPerspectivaIni.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPerspectivaIni.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblPerspectivaIni);
        lblPerspectivaIni.setBounds(20, 200, 100, 18);

        lblPerspectivaFin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPerspectivaFin.setText(bundle.getString("BscRelIndFrame_00001")); // NOI18N
        lblPerspectivaFin.setEnabled(false);
        pnlData.add(lblPerspectivaFin);
        lblPerspectivaFin.setBounds(240, 200, 20, 18);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(20, 20, 400, 22);

        fldDataIni.setDialog(true);
        fldDataIni.setUseLocale(true);
        fldDataIni.setWaitForCalendarDate(true);
        pnlData.add(fldDataIni);
        fldDataIni.setBounds(20, 180, 180, 22);

        fldDataFin.setDialog(true);
        fldDataFin.setUseLocale(true);
        fldDataFin.setWaitForCalendarDate(true);
        pnlData.add(fldDataFin);
        fldDataFin.setBounds(240, 180, 180, 22);

        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 400, 60);

        cbPerspectivaDe.setMinimumSize(new java.awt.Dimension(22, 17));
        cbPerspectivaDe.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cbPerspectivaDe);
        cbPerspectivaDe.setBounds(20, 220, 180, 22);

        cbPerspectivaAte.setMinimumSize(new java.awt.Dimension(22, 17));
        cbPerspectivaAte.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cbPerspectivaAte);
        cbPerspectivaAte.setBounds(240, 220, 180, 22);

        lblSituacao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSituacao.setText("Objetivos:");
        lblSituacao.setEnabled(false);
        lblSituacao.setMaximumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setMinimumSize(new java.awt.Dimension(100, 16));
        lblSituacao.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblSituacao);
        lblSituacao.setBounds(20, 240, 100, 18);
        lblSituacao.getAccessibleContext().setAccessibleName(bundle.getString("BscRelIndFrame_00016")); // NOI18N

        cbSituacao.setEnabled(false);
        pnlData.add(cbSituacao);
        cbSituacao.setBounds(20, 260, 180, 22);

        lblDataAlvo.setText(bundle.getString("BscCentralFrame_00003")); // NOI18N
        lblDataAlvo.setPreferredSize(new java.awt.Dimension(100, 15));
        pnlData.add(lblDataAlvo);
        lblDataAlvo.setBounds(240, 120, 100, 18);
        lblDataAlvo.getAccessibleContext().setAccessibleName(bundle.getString("BscRelIndFrame_00017")); // NOI18N

        fldDataAlvo.setEditable(false);
        fldDataAlvo.setUseLocale(true);
        pnlData.add(fldDataAlvo);
        fldDataAlvo.setBounds(240, 140, 180, 22);

        btnAvaliacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnAvaliacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvaliacaoActionPerformed(evt);
            }
        });
        pnlData.add(btnAvaliacao);
        btnAvaliacao.setBounds(430, 140, 30, 22);

        cbOrdemObj.setMinimumSize(new java.awt.Dimension(22, 17));
        cbOrdemObj.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cbOrdemObj);
        cbOrdemObj.setBounds(20, 300, 180, 22);

        lblOrdemObj.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrdemObj.setText(bundle.getString("BscRelEvolFrame_00018")); // NOI18N
        lblOrdemObj.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblOrdemObj);
        lblOrdemObj.setBounds(20, 280, 120, 20);

        lblOrdemInd.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrdemInd.setText(bundle.getString("BscRelEvolFrame_00019")); // NOI18N
        lblOrdemInd.setPreferredSize(new java.awt.Dimension(45, 15));
        pnlData.add(lblOrdemInd);
        lblOrdemInd.setBounds(240, 280, 130, 20);

        cbOrdemInd.setMinimumSize(new java.awt.Dimension(22, 17));
        cbOrdemInd.setPreferredSize(new java.awt.Dimension(22, 17));
        pnlData.add(cbOrdemInd);
        cbOrdemInd.setBounds(240, 300, 180, 22);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(20, 30));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(200, 200));

        btnGeraRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("BscRel5w2hFrame_00019")); // NOI18N
        btnGeraRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(100, 29));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("BscRel5w2hFrame_00020")); // NOI18N
        btnVizualizaRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(100, 29));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        pnlTopRecord.add(tbInsertion, java.awt.BorderLayout.EAST);

        pnlTopTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlTopTool, java.awt.BorderLayout.PAGE_START);

        pnlBottomTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlBottomTool, java.awt.BorderLayout.SOUTH);
        pnlTopRecord.add(pnlCenterTool, java.awt.BorderLayout.CENTER);
        pnlTopRecord.add(pnlLeftTool, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.PAGE_START);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab("Relat�rio de Indicadores", pnlRecord);

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.PAGE_START);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void btnAvaliacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvaliacaoActionPerformed
            bsc.swing.analisys.BscPerformanceCardFrame bscPerformanceCard =
                    new bsc.swing.analisys.BscPerformanceCardFrame(bsc.core.BscStaticReferences.getBscMainPanel(), true, this);
            bscPerformanceCard.fldDataAnalise.setCalendar(record.getDate("DATAALVO"));
            bscPerformanceCard.setVisible(true);
	}//GEN-LAST:event_btnAvaliacaoActionPerformed

        private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
            event.executeRecord("SALVAR;rel053_" + record.getString("ID") + ".html");
}//GEN-LAST:event_btnGeraRelActionPerformed

        private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
            try {
                bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
                java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString().concat("bscreport.apw?id=rel053_".concat(this.id)));
                bscApplet.getAppletContext().showDocument(url, "_blank");
            } catch (Exception e) {
                e.printStackTrace();
            }
}//GEN-LAST:event_btnVizualizaRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAvaliacao;
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JComboBox cbOrdemInd;
    private javax.swing.JComboBox cbOrdemObj;
    private javax.swing.JComboBox cbPerspectivaAte;
    private javax.swing.JComboBox cbPerspectivaDe;
    private javax.swing.JComboBox cbSituacao;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private pv.jfcx.JPVDate fldDataAlvo;
    private pv.jfcx.JPVDatePlus fldDataFin;
    private pv.jfcx.JPVDatePlus fldDataIni;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDataAlvo;
    private javax.swing.JLabel lblDataFin;
    private javax.swing.JLabel lblDataIni;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblOrdemInd;
    private javax.swing.JLabel lblOrdemObj;
    private javax.swing.JLabel lblPerspectivaFin;
    private javax.swing.JLabel lblPerspectivaIni;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlBottomTool;
    private javax.swing.JPanel pnlCenterTool;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftTool;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTopTool;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event =
            new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscRelIndFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);
        event.editRecord(); // Relatorio por default edit
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        //pnlConfirmation.setVisible(true);
        //pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        //pnlConfirmation.setVisible(false);
        //pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
