package bsc.swing.report;

import java.util.ResourceBundle;

public class BscRelTarFrame extends javax.swing.JInternalFrame implements bsc.swing.BscDefaultFrameFunctions {

    /***************************************************************************/
    // FUN��ES QUE PRECISAM SER ALTERADAS PARA CADA FORMUL�RIO:
    /***************************************************************************/
    private String type = new String("RELTAR");

    public void enableFields() {
        // Habilita os campos do formul�rio.
        lblNome.setEnabled(true);
        fldNome.setEnabled(true);
        lblDescricao.setEnabled(true);
        txtDescricao.setEnabled(true);
        chkIncluirDescricao.setEnabled(true);
        lblDataIni.setEnabled(true);
        fldDataIni.setEnabled(true);
        lblDataFin.setEnabled(true);
        fldDataFin.setEnabled(true);
        lblPessoaIni.setEnabled(true);
        cbbPessoaDe.setEnabled(true);
        lblImportancia.setEnabled(true);
        cbbImportancia.setEnabled(true);
        lblUrgencia.setEnabled(true);
        cbbUrgencia.setEnabled(true);
        lblIniciativas.setEnabled(true);
        cbbSituacaoIniciativa.setEnabled(true);
    }

    public void disableFields() {
        // Desabilita os campos do formul�rio.
    }
    java.util.Hashtable htbUrgencia = new java.util.Hashtable();
    java.util.Hashtable htbImportancia = new java.util.Hashtable();
    java.util.Hashtable htbPessoaDe = new java.util.Hashtable();
    java.util.Hashtable htbSitIniciativa = new java.util.Hashtable();
    java.util.Hashtable htbListar = new java.util.Hashtable();

    public void refreshFields() {
        // Copia os dados da vari�vel "record" para os campos do formul�rio.
        // Ex.:
        // strategyList.setDataSource(
        //					record.getBIXMLVector("ESTRATEGIAS"), record.getString("ID") );
        fldNome.setText(record.getString("NOME"));
        txtDescricao.setText(record.getString("DESCRICAO"));
        chkIncluirDescricao.setSelected(record.getBoolean("IMPDESC"));
        fldDataIni.setCalendar(record.getDate("DEDATA"));
        fldDataFin.setCalendar(record.getDate("ATEDATA"));
        event.populateCombo(record.getBIXMLVector("PESSOAS"), "DEPESSOA", htbPessoaDe, cbbPessoaDe);
        event.populateCombo(record.getBIXMLVector("URGENCIAS"), "URGENCIA", htbUrgencia, cbbUrgencia);
        event.populateCombo(record.getBIXMLVector("IMPORTANCIAS"), "IMPORTANCI", htbImportancia, cbbImportancia);
        event.populateCombo(record.getBIXMLVector("STATUS_INICIATIVA"), "SITINI", htbSitIniciativa, cbbSituacaoIniciativa);
    }

    public bsc.xml.BIXMLRecord getFieldsContents() {
        bsc.xml.BIXMLRecord recordAux = new bsc.xml.BIXMLRecord(type);
        recordAux.set("ID", id);
        if (record.contains("PARENTID")) {
            recordAux.set("PARENTID", record.getString("PARENTID"));
        }
        if (record.contains("CONTEXTID")) {
            recordAux.set("CONTEXTID", record.getString("CONTEXTID"));
        }

        // Copia os campos do formul�rio para a v�riavel "recordAux" e devolve
        // como retorno da fun��o.

        recordAux.set("NOME", fldNome.getText());
        recordAux.set("DESCRICAO", txtDescricao.getText());
        recordAux.set("IMPDESC", chkIncluirDescricao.isSelected());
        recordAux.set("DEDATA", fldDataIni.getCalendar());
        recordAux.set("ATEDATA", fldDataFin.getCalendar());
        recordAux.set("DEPESSOA", event.getComboValue(htbPessoaDe, cbbPessoaDe));
        recordAux.set("IMPORTANCI", event.getComboValue(htbImportancia, cbbImportancia));
        recordAux.set("URGENCIA", event.getComboValue(htbUrgencia, cbbUrgencia));
        recordAux.set("SITINI", event.getComboValue(htbSitIniciativa, cbbSituacaoIniciativa));

        return recordAux;
    }

    public boolean validateFields(StringBuffer errorMessage) {
        boolean valid = false;

        valid = (fldNome.getText().trim().length() > 0);

        if (!valid) {
            errorMessage.append(ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BscValidateFields_00001"));
        }
        return valid;
    }

    public boolean hasCombos() {
        return false;
    }

    /*****************************************************************************/
    /*****************************************************************************/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tapCadastro = new javax.swing.JTabbedPane();
        pnlRecord = new javax.swing.JPanel();
        pnlFields = new javax.swing.JPanel();
        pnlBottomRecord = new javax.swing.JPanel();
        pnlRightRecord = new javax.swing.JPanel();
        pnlLeftRecord = new javax.swing.JPanel();
        scpRecord = new javax.swing.JScrollPane();
        pnlData = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        chkIncluirDescricao = new javax.swing.JCheckBox();
        fldNome = new pv.jfcx.JPVEdit();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescricao = new bsc.beans.JBITextArea();
        jPanel1 = new javax.swing.JPanel();
        lblPessoaIni = new javax.swing.JLabel();
        cbbPessoaDe = new javax.swing.JComboBox();
        lblIniciativas = new javax.swing.JLabel();
        cbbSituacaoIniciativa = new javax.swing.JComboBox();
        lblDataIni = new javax.swing.JLabel();
        fldDataIni = new pv.jfcx.JPVDatePlus();
        lblDataFin = new javax.swing.JLabel();
        fldDataFin = new pv.jfcx.JPVDatePlus();
        jPanel2 = new javax.swing.JPanel();
        lblImportancia = new javax.swing.JLabel();
        cbbImportancia = new javax.swing.JComboBox();
        cbbUrgencia = new javax.swing.JComboBox();
        lblUrgencia = new javax.swing.JLabel();
        pnlTopRecord = new javax.swing.JPanel();
        tbInsertion = new javax.swing.JToolBar();
        btnGeraRel = new javax.swing.JButton();
        btnVizualizaRel = new javax.swing.JButton();
        pnlTopTool = new javax.swing.JPanel();
        pnlBottomTool = new javax.swing.JPanel();
        pnlCenterTool = new javax.swing.JPanel();
        pnlLeftTool = new javax.swing.JPanel();
        pnlTopForm = new javax.swing.JPanel();
        pnlBottomForm = new javax.swing.JPanel();
        pnlRightForm = new javax.swing.JPanel();
        pnlLeftForm = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()); // NOI18N
        setTitle(bundle.getString("BscRelTarFrame_00008")); // NOI18N
        setNormalBounds(new java.awt.Rectangle(0, 0, 543, 358));
        setPreferredSize(new java.awt.Dimension(543, 358));

        pnlRecord.setLayout(new java.awt.BorderLayout());

        pnlFields.setLayout(new java.awt.BorderLayout());

        pnlBottomRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlBottomRecord, java.awt.BorderLayout.SOUTH);

        pnlRightRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlRightRecord, java.awt.BorderLayout.EAST);

        pnlLeftRecord.setPreferredSize(new java.awt.Dimension(2, 2));
        pnlFields.add(pnlLeftRecord, java.awt.BorderLayout.WEST);

        scpRecord.setBorder(null);
        scpRecord.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scpRecord.setAutoscrolls(true);
        scpRecord.setPreferredSize(new java.awt.Dimension(520, 330));

        pnlData.setPreferredSize(new java.awt.Dimension(400, 400));
        pnlData.setLayout(null);

        lblNome.setForeground(new java.awt.Color(51, 51, 255));
        lblNome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblNome.setText(bundle.getString("BscRelTarFrame_00016")); // NOI18N
        lblNome.setEnabled(false);
        lblNome.setMaximumSize(new java.awt.Dimension(100, 16));
        lblNome.setMinimumSize(new java.awt.Dimension(100, 16));
        lblNome.setPreferredSize(new java.awt.Dimension(100, 16));
        pnlData.add(lblNome);
        lblNome.setBounds(10, 0, 100, 20);

        lblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDescricao.setText(bundle.getString("BscRelTarFrame_00006")); // NOI18N
        lblDescricao.setEnabled(false);
        pnlData.add(lblDescricao);
        lblDescricao.setBounds(10, 40, 100, 20);

        chkIncluirDescricao.setText(bundle.getString("BscRelTarFrame_00012")); // NOI18N
        chkIncluirDescricao.setEnabled(false);
        pnlData.add(chkIncluirDescricao);
        chkIncluirDescricao.setBounds(10, 113, 254, 20);

        fldNome.setMaxLength(60);
        pnlData.add(fldNome);
        fldNome.setBounds(10, 20, 400, 22);

        jScrollPane1.setViewportView(txtDescricao);

        pnlData.add(jScrollPane1);
        jScrollPane1.setBounds(10, 60, 400, 52);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscRelTarFrame_00018"))); // NOI18N
        jPanel1.setLayout(null);

        lblPessoaIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPessoaIni.setText(bundle.getString("BscRelTarFrame_00021")); // NOI18N
        lblPessoaIni.setEnabled(false);
        lblPessoaIni.setMaximumSize(new java.awt.Dimension(100, 16));
        lblPessoaIni.setMinimumSize(new java.awt.Dimension(100, 16));
        lblPessoaIni.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel1.add(lblPessoaIni);
        lblPessoaIni.setBounds(10, 60, 100, 18);

        cbbPessoaDe.setEnabled(false);
        jPanel1.add(cbbPessoaDe);
        cbbPessoaDe.setBounds(10, 80, 400, 22);

        lblIniciativas.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblIniciativas.setText(bundle.getString("BscRelTarFrame_00022")); // NOI18N
        lblIniciativas.setEnabled(false);
        lblIniciativas.setMaximumSize(new java.awt.Dimension(100, 16));
        lblIniciativas.setMinimumSize(new java.awt.Dimension(100, 16));
        lblIniciativas.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel1.add(lblIniciativas);
        lblIniciativas.setBounds(10, 100, 100, 18);

        cbbSituacaoIniciativa.setEnabled(false);
        jPanel1.add(cbbSituacaoIniciativa);
        cbbSituacaoIniciativa.setBounds(10, 120, 180, 22);

        lblDataIni.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDataIni.setText(bundle.getString("BscRelTarFrame_00020")); // NOI18N
        lblDataIni.setEnabled(false);
        jPanel1.add(lblDataIni);
        lblDataIni.setBounds(10, 20, 100, 20);

        fldDataIni.setDialog(true);
        fldDataIni.setUseLocale(true);
        fldDataIni.setWaitForCalendarDate(true);
        jPanel1.add(fldDataIni);
        fldDataIni.setBounds(10, 40, 180, 22);

        lblDataFin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDataFin.setText(bundle.getString("BscRelTarFrame_00013")); // NOI18N
        lblDataFin.setEnabled(false);
        jPanel1.add(lblDataFin);
        lblDataFin.setBounds(230, 20, 20, 20);

        fldDataFin.setDialog(true);
        fldDataFin.setUseLocale(true);
        fldDataFin.setWaitForCalendarDate(true);
        jPanel1.add(fldDataFin);
        fldDataFin.setBounds(230, 40, 180, 22);

        pnlData.add(jPanel1);
        jPanel1.setBounds(0, 140, 420, 160);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("BscRelTarFrame_00017"))); // NOI18N
        jPanel2.setLayout(null);

        lblImportancia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblImportancia.setText(bundle.getString("BscTarefaFrame_00027")); // NOI18N
        lblImportancia.setEnabled(false);
        lblImportancia.setMaximumSize(new java.awt.Dimension(100, 16));
        lblImportancia.setMinimumSize(new java.awt.Dimension(100, 16));
        lblImportancia.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel2.add(lblImportancia);
        lblImportancia.setBounds(10, 20, 100, 18);

        cbbImportancia.setEnabled(false);
        jPanel2.add(cbbImportancia);
        cbbImportancia.setBounds(10, 40, 180, 22);

        cbbUrgencia.setEnabled(false);
        jPanel2.add(cbbUrgencia);
        cbbUrgencia.setBounds(230, 40, 180, 22);

        lblUrgencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUrgencia.setText(bundle.getString("BscTarefaFrame_00028")); // NOI18N
        lblUrgencia.setEnabled(false);
        lblUrgencia.setMaximumSize(new java.awt.Dimension(100, 16));
        lblUrgencia.setMinimumSize(new java.awt.Dimension(100, 16));
        lblUrgencia.setPreferredSize(new java.awt.Dimension(100, 16));
        jPanel2.add(lblUrgencia);
        lblUrgencia.setBounds(230, 20, 100, 18);

        pnlData.add(jPanel2);
        jPanel2.setBounds(0, 300, 420, 80);

        scpRecord.setViewportView(pnlData);

        pnlFields.add(scpRecord, java.awt.BorderLayout.CENTER);

        pnlTopRecord.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnlTopRecord.setPreferredSize(new java.awt.Dimension(20, 30));
        pnlTopRecord.setLayout(new java.awt.BorderLayout());

        tbInsertion.setFloatable(false);
        tbInsertion.setRollover(true);
        tbInsertion.setPreferredSize(new java.awt.Dimension(200, 200));

        btnGeraRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGeraRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_documento.gif"))); // NOI18N
        btnGeraRel.setText(bundle.getString("BscRel5w2hFrame_00019")); // NOI18N
        btnGeraRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnGeraRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnGeraRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnGeraRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGeraRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnGeraRel);

        btnVizualizaRel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnVizualizaRel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ic_20_visualizar.gif"))); // NOI18N
        btnVizualizaRel.setText(bundle.getString("BscRel5w2hFrame_00020")); // NOI18N
        btnVizualizaRel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        btnVizualizaRel.setMaximumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setMinimumSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.setPreferredSize(new java.awt.Dimension(100, 20));
        btnVizualizaRel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVizualizaRelActionPerformed(evt);
            }
        });
        tbInsertion.add(btnVizualizaRel);

        pnlTopRecord.add(tbInsertion, java.awt.BorderLayout.EAST);

        pnlTopTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlTopTool, java.awt.BorderLayout.PAGE_START);

        pnlBottomTool.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlTopRecord.add(pnlBottomTool, java.awt.BorderLayout.SOUTH);
        pnlTopRecord.add(pnlCenterTool, java.awt.BorderLayout.CENTER);
        pnlTopRecord.add(pnlLeftTool, java.awt.BorderLayout.WEST);

        pnlFields.add(pnlTopRecord, java.awt.BorderLayout.PAGE_START);

        pnlRecord.add(pnlFields, java.awt.BorderLayout.CENTER);

        tapCadastro.addTab(bundle.getString("BscRelTarFrame_00008"), pnlRecord); // NOI18N

        getContentPane().add(tapCadastro, java.awt.BorderLayout.CENTER);
        getContentPane().add(pnlTopForm, java.awt.BorderLayout.NORTH);
        getContentPane().add(pnlBottomForm, java.awt.BorderLayout.SOUTH);
        getContentPane().add(pnlRightForm, java.awt.BorderLayout.EAST);
        getContentPane().add(pnlLeftForm, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGeraRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGeraRelActionPerformed
        event.executeRecord("SALVAR;rel052_" + record.getString("ID") + ".html");
}//GEN-LAST:event_btnGeraRelActionPerformed

    private void btnVizualizaRelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVizualizaRelActionPerformed
        try {
            bsc.applet.BscApplet bscApplet = bsc.core.BscStaticReferences.getBscApplet();
            java.net.URL url = new java.net.URL(bscApplet.getCodeBase().toString().concat("bscreport.apw?id=rel052_".concat(this.id)));
            bscApplet.getAppletContext().showDocument(url, "_blank");
        } catch (Exception e) {
            e.printStackTrace();
        }
}//GEN-LAST:event_btnVizualizaRelActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGeraRel;
    private javax.swing.JButton btnVizualizaRel;
    private javax.swing.JComboBox cbbImportancia;
    private javax.swing.JComboBox cbbPessoaDe;
    private javax.swing.JComboBox cbbSituacaoIniciativa;
    private javax.swing.JComboBox cbbUrgencia;
    private javax.swing.JCheckBox chkIncluirDescricao;
    private pv.jfcx.JPVDatePlus fldDataFin;
    private pv.jfcx.JPVDatePlus fldDataIni;
    private pv.jfcx.JPVEdit fldNome;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDataFin;
    private javax.swing.JLabel lblDataIni;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblImportancia;
    private javax.swing.JLabel lblIniciativas;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPessoaIni;
    private javax.swing.JLabel lblUrgencia;
    private javax.swing.JPanel pnlBottomForm;
    private javax.swing.JPanel pnlBottomRecord;
    private javax.swing.JPanel pnlBottomTool;
    private javax.swing.JPanel pnlCenterTool;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlFields;
    private javax.swing.JPanel pnlLeftForm;
    private javax.swing.JPanel pnlLeftRecord;
    private javax.swing.JPanel pnlLeftTool;
    private javax.swing.JPanel pnlRecord;
    private javax.swing.JPanel pnlRightForm;
    private javax.swing.JPanel pnlRightRecord;
    private javax.swing.JPanel pnlTopForm;
    private javax.swing.JPanel pnlTopRecord;
    private javax.swing.JPanel pnlTopTool;
    private javax.swing.JScrollPane scpRecord;
    private javax.swing.JTabbedPane tapCadastro;
    private javax.swing.JToolBar tbInsertion;
    private bsc.beans.JBITextArea txtDescricao;
    // End of variables declaration//GEN-END:variables
    private bsc.swing.BscDefaultFrameBehavior event = new bsc.swing.BscDefaultFrameBehavior(this);
    private int status = bsc.swing.BscDefaultFrameBehavior.NORMAL;
    private String id = new String();
    private bsc.xml.BIXMLRecord record = null;

    public BscRelTarFrame(int operation, String idAux, String contextId) {
        initComponents();
        event.defaultConstructor(operation, idAux, contextId);

        event.editRecord(); // Relatorio por default edit
    }

    public void setStatus(int value) {
        status = value;
    }

    public int getStatus() {
        return status;
    }

    public void setType(String value) {
        type = String.valueOf(value);
    }

    public String getType() {
        return String.valueOf(type);
    }

    public void setID(String value) {
        id = String.valueOf(value);
    }

    public String getID() {
        return String.valueOf(id);
    }

    public void setRecord(bsc.xml.BIXMLRecord recordAux) {
        record = recordAux;
    }

    public bsc.xml.BIXMLRecord getRecord() {
        return record;
    }

    public void showDataButtons() {
        //pnlConfirmation.setVisible(true);
        //pnlOperation.setVisible(false);
    }

    public void showOperationsButtons() {
        //pnlConfirmation.setVisible(false);
        //pnlOperation.setVisible(true);
    }

    public javax.swing.JInternalFrame asJInternalFrame() {
        return this;
    }

    public String getParentID() {
        if (record.contains("PARENTID")) {
            return String.valueOf(record.getString("PARENTID"));
        } else {
            return new String("");
        }
    }

    public void loadRecord() {
        event.loadRecord();
    }

    public javax.swing.JTabbedPane getTabbedPane() {
        return tapCadastro;
    }
}
