/*
 * BscSeta2D.java
 *
 * Created on 29 de Setembro de 2003, 10:29
 */

package bsc.util;

import java.awt.*;
import java.awt.geom.*;

/**
 *
 * @author  siga1728
 */
public class BscArrow2D {
	
	public int lineType = 0; //0-LINE 1-QUADCURVE
	
	Line2D line;
	GeneralPath path;
	Shape shape;
	QuadCurve2D quadCurve;
	
	/** Creates a new instance of BscSeta2D */
	public BscArrow2D(Point2D p1, Point2D p2 ) {
		setLine(p1, p2);
	}
	
	public void render(Graphics2D g2) {
		g2.draw(line);
		g2.fill(shape);
	}
	
	public void render(Graphics2D g2,Rectangle ctrl,boolean lDrawCtrl,boolean lSelected ) {
		
		if(ctrl.width==0 && ctrl.height == 0 ){
			// E a primeira vez de desenho do quadrado de controle da curva.
			int px = (int)((line.getX2() - line.getX1()) /2) ;
			int py = (int)((line.getY2() - line.getY1()) /2);
			px += line.getX1();
			py += line.getY1();
			ctrl.setBounds(px, py,4,4);
		}
		
		quadCurve.setCurve(line.getX1(),line.getY1(),ctrl.getX(),ctrl.getY(),line.getX2(),line.getY2());
		g2.draw(quadCurve);
		
		//Pinta o ponto de controle
		if(lDrawCtrl){
			g2.setPaint(Color.blue);
			g2.fill(ctrl);
			g2.draw(ctrl);
			if(lSelected){
				g2.setPaint(Color.yellow);
			}else {
				g2.setPaint(Color.black);
			}
		}

		g2.fill(shape);
	}
	
	/** Getter for property line.
	 * @return Value of property line.
	 */
	public java.awt.geom.Line2D getLine() {
		return line;
	}
	
	/** Setter for property line.
	 * @param line New value of property line.
	 */
	public void setLine(Point2D p1, Point2D p2) {
		setLine(p1,p2,new Rectangle(0,0,0,0));
	}
	
	/** Setter for property line.
	 * @param line New value of property line.
	 */
	public void setLine(Point2D p1, Point2D p2, Rectangle ctrl) {
		if(line == null)
			line = new Line2D.Double(p1, p2);
		else
			line.setLine(p1, p2);
		
		path = new GeneralPath();
		path.moveTo((float)line.getX2(), (float)line.getY2());
		path.lineTo((float)line.getX2()-18, (float)line.getY2()-7);
		path.lineTo((float)line.getX2()-18, (float)line.getY2()+8);
		path.closePath();
		
		double catetoX	= 0;
		double catetoY	= 0;
		double ang		= 0;
		
		if(lineType == 1){
			if(ctrl.width==0 && ctrl.height == 0 ){
				// E a primeira vez de desenho do quadrado de controle da curva.
				int px = (int)((line.getX2() - line.getX1()) /2) ;
				int py = (int)((line.getY2() - line.getY1()) /2);
				px += line.getX1();
				py += line.getY1();
				ctrl.setBounds(px, py,4,4);
			}

			if(quadCurve == null){
				quadCurve = new QuadCurve2D.Double(line.getX1(),line.getY1(),ctrl.getX(),ctrl.getY(),line.getX2(),line.getY2());
			} else {
				quadCurve.setCurve(line.getX1(),line.getY1(),ctrl.getX(),ctrl.getY(),line.getX2(),line.getY2());
			}
			
			catetoX = quadCurve.getCtrlPt().getX() - line.getX2();
			catetoY = quadCurve.getCtrlPt().getY() - line.getY2();
		} else {
			catetoX = line.getX2() - line.getX1();
			catetoY = line.getY2() - line.getY1();
		}
		
		double raio = Math.sqrt(Math.pow(catetoX,2)+Math.pow(catetoY,2));
		double cos = catetoX/raio;
		
		if(lineType == 1){
			ang = 135.1 + Math.acos(cos) * (catetoY < 0 ? -1 : 1);
		} else {
			ang = Math.acos(cos) * (catetoY < 0 ? -1 : 1);
		}
		
		AffineTransform at = AffineTransform.getRotateInstance(ang, line.getX2(), line.getY2());
		shape = at.createTransformedShape(path);
	}
	
	public double ptSegDist(Point2D p2D) {
		double distance;
		if(path.contains(p2D))
			distance = 0.0d;
		else
			distance = line.ptSegDist(p2D);
		return distance;
	}
}