/*
 * BscDateUtil.java
 *
 * Created on 8 de Mar�o de 2006, 17:22
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package bsc.util;
import java.text.DateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author Alexandre Alves da Silva
 */
public class BscDateUtil {
    
    /** Creates a new instance of BscDateUtil */
    public BscDateUtil(){}
    
    /**
     * Recebe um Calendar e Retorna um String.
     */
    public String getCalendarString(java.util.Calendar c) {
	String value = "  /  /    ";
	if(c != null) {
	    value = new String(DateFormat.getInstance().format(c.getTime()));
	}
	return value;
    }
    
    /** Obt�m o valor GregorianCalendar do atributo referenciado pelo par�metro.
     *@param key Objeto String com o nome do atributo.
     *@return Objeto do tipo GregorianCalendar com o conte�do do atributo.
     *@throws kpi.xml.BIXMLException Caso o atributo n�o exista no registro ou caso
     * seja imposs�vel convert�-lo para um GregorianCalendar.
     */
    public GregorianCalendar getDate( String key ){
	GregorianCalendar gc = null;
	if(key.length() >= 10) {
	    try {
		gc = new GregorianCalendar(
		Integer.parseInt(key.substring(6, 9).trim()),
		Integer.parseInt(key.substring(3, 5).trim())-1,
		Integer.parseInt(key.substring(0, 2).trim()));
	    } catch (java.lang.NumberFormatException e) {
		gc = null;
	    }
	}
	return gc;
    }
}