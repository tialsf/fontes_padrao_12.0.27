/*
 * BscToolKit.java
 *
 * Created on 25 de Outubro de 2004, 14:26
 */

package bsc.util;

/**
 *
 * @author  alexandreas
 */
public class BscToolKit {
    
    /** Creates a new instance of BscToolKit */
    public BscToolKit() {}

    //Faz o carregamento do itens de uma "combo box".
    public void populateCombo(bsc.xml.BIXMLVector vector, String keyName, java.util.Hashtable table, javax.swing.JComboBox combo,
	java.util.Hashtable ignoreIDList, bsc.swing.BscDefaultFrameFunctions frame) {
	java.awt.event.ItemListener[] items = combo.getItemListeners();
	for(int i=0; i < items.length; i++)
	    combo.removeItemListener(items[i]);
	
	table.clear();
	combo.removeAllItems();
	combo.addItem("");
	
	for ( int i = 0; i < vector.size(); i++ ) {
	    table.put( vector.get(i).getString("NOME"),	vector.get(i).getString("ID"));
	    
	    if ( ignoreIDList != null ){
		if ( !ignoreIDList.containsKey(new Integer(vector.get(i).getInt(("ID")))))
		    combo.addItem( vector.get(i).getString("NOME") );
	    }
	    else {
		combo.addItem( vector.get(i).getString("NOME") );
	    }
	}
	
	if(frame != null){
	    if ( frame.getRecord().contains(keyName) )
		for ( int i = 0; i < vector.size(); i++ )
		    if ( vector.get(i).getInt("ID") == frame.getRecord().getInt(keyName) )
			combo.setSelectedItem( vector.get(i).getString("NOME") );
	}

	for(int i=0; i < items.length; i++)
	    combo.addItemListener(items[i]);
	
	return;
    }

    public String getComboValue( java.util.Hashtable table,javax.swing.JComboBox combo ) {
	if ((combo.getSelectedItem() != null) && (table.get((String) combo.getSelectedItem()) != null))
	    return String.valueOf( table.get((String)combo.getSelectedItem() ));
	else
	    return String.valueOf("0");
    }
}