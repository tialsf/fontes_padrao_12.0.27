package bsc.xml;

import java.lang.*;
import java.util.*;

/**
 *
 * @author  Alexandre Albert Gon�alves
 */
public class BIXMLAttributes extends BIXMLGeneralData{
	
	public BIXMLAttributes clone2() {
		BIXMLAttributes attrs = new BIXMLAttributes();
		for (Iterator e = getKeyNames() ; e.hasNext() ;) {
			String key = (String) e.next();
			attrs.set( key, getString( key ) );
		}
		return attrs;
	}
	
	public BIXMLAttributes() {
		super();
	}
	
	public BIXMLAttributes(org.xml.sax.Attributes attrs) {
		if (attrs != null) {
			for (int i = 0; i < attrs.getLength(); i++) {
				String aName = attrs.getLocalName(i);
				if ("".equals(aName))
					aName = attrs.getQName(i);
				set(aName, attrs.getValue(i));
			}
		}
	}
	
	public String toString () {
		StringBuffer sb = new StringBuffer();
		for (Iterator e = getKeyNames() ; e.hasNext() ;)
			try {
				String key = (String) e.next();
				sb.append(key);
				sb.append("=\"");
				sb.append(getString(key));
				sb.append("\" ");
			} catch( BIXMLException exc) {
				bsc.core.BscDebug.println(exc.getMessage());
			}
		return sb.toString();
	}
	
}