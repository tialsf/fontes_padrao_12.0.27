package bsc.xml;

import java.util.*;

/**
 *
 * @author  siga1996
 */
public class BIXMLRecord extends BIXMLGeneralData {
	
	private BIXMLAttributes recordAttributes = new BIXMLAttributes();
	private HashMap attributesVector = new HashMap();
	private String tagName = new String();
	private String recordData = new String();
	
	public BIXMLRecord clone2() {
		BIXMLRecord record = new BIXMLRecord( String.valueOf(tagName) );
		record.setAttributes( recordAttributes.clone2() );
		record.set( String.valueOf(recordData) );
		for (Iterator e = getKeyNames() ; e.hasNext() ;) {
			String key = (String) e.next();
			record.set( key, getString( key ) );
			record.setInternalAttributes( key, getInternalAttributes(key).clone2() );
		}
		return record;
	}
	
	/** Creates a new instance of BIXMLRecord */
	public BIXMLRecord (String name){
		super();
		tagName = name;
	}
	
	public BIXMLRecord(String name, BIXMLAttributes attrs ) {
		super();
		recordAttributes = attrs;
		tagName = name;
	}

	protected void setPropertyAsBIXMLVector( String key, BIXMLVector vector ) {
		props.put( key, vector );
	}
	
	protected BIXMLVector getPropertyAsBIXMLVector( String key ) {
		Object value = props.get(key);
		if(value==null)
			throw new BIXMLKeyDoesNotExistException(java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIXMLRecord_00002")+key+java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIXMLRecord_00001"));

		if (value instanceof bsc.xml.BIXMLVector )		
			return ((BIXMLVector) value);
		else {
			BIXMLVector vector = new BIXMLVector();
			vector.setMainTag( key );
			vector.setAttributes( (BIXMLAttributes) attributesVector.get( key ) );
			return vector;
		}
	}	
	
	public String getTagName() {
		return new String ( tagName );
	}
	
	public void setAttributes(BIXMLAttributes attrs) {
		recordAttributes = attrs;
	}
	
	public BIXMLAttributes getAttributes() {
		return recordAttributes;
	}
	
	public BIXMLAttributes getInternalAttributes( String key ) throws BIXMLKeyDoesNotExistException{
		if ( !contains(key) )
			throw new BIXMLKeyDoesNotExistException( "Tag \""+ key + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIXMLRecord_00001"));
		if ( !attributesVector.containsKey( key ) ) {
			attributesVector.put(key, new BIXMLAttributes());
			return (BIXMLAttributes) attributesVector.get(key);
		} else
			return (BIXMLAttributes) attributesVector.get( key );	
	}
	
	public void setInternalAttributes(String key, BIXMLAttributes attrs) throws BIXMLException {
		if ( !contains(key) )
			throw new BIXMLKeyDoesNotExistException( "Tag \""+ key + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIXMLRecord_00001"));
		if ( !attributesVector.containsKey( key ) )
			attributesVector.put(key, attrs);
		else {
			String internalKey = new String();
			for (Iterator e = attrs.getKeyNames() ; e.hasNext() ;) {
				internalKey = (String) e.next();
				getInternalAttributes(key).set(internalKey, attrs.getString(internalKey));
			}
		}
	}

	public void removeInternalAttributes( String key ) {
		attributesVector.remove( key );
	}
	
	public String getString () {
		return String.valueOf(recordData);
	}
	
	public void set( String data ) {
		recordData = String.valueOf(data);
	}
	
	public void set(BIXMLVector vect) {
		setPropertyAsBIXMLVector(vect.getMainTag(), vect);
	}
	
	public void remove(String key) throws BIXMLKeyDoesNotExistException {
		try {
			super.remove(key);
		} catch (BIXMLKeyDoesNotExistException e) {
			throw new BIXMLKeyDoesNotExistException(
				"Tag \""+ key + java.util.ResourceBundle.getBundle("international", bsc.core.BscStaticReferences.getBscDefaultLocale()).getString("BIXMLRecord_00003"));
		}
		attributesVector.remove(key);
	}

	public BIXMLVector getBIXMLVector( String key )throws BIXMLException {		
		return getPropertyAsBIXMLVector( key );
	}

	private String changeSpecialCharacters( String s ) {
		
		// Antiga implementação (lenta)
		//s = s.replaceAll("&", "&amp;");
		//s = s.replaceAll("\"", "&quot;");
		//s = s.replaceAll(">", "&gt;");
		//s = s.replaceAll("'", "&apos;");			
		//s = s.replaceAll("<", "&lt;");

		StringBuffer sb = new StringBuffer();
		for(int i=0; i<s.length(); i++) {
			char c = s.charAt(i);
			if(c == '&')
				sb.append("&amp;");
			else if(c == '\"')
				sb.append("&quot;");
			else if(c == '\'')
				sb.append("&apos;");
			else if(c == '>')
				sb.append("&gt;");
			else if(c == '<')
				sb.append("&lt;");
			else 
				sb.append(c);
		}
		
		return sb.toString();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append('<');
		sb.append(tagName);
		
		if(recordAttributes.size() != 0) {
			sb.append(' ');
			sb.append(recordAttributes.toString());
		}
		sb.append('>');
		sb.append(getString());
		sb.append('\n');
		
		for (Iterator e = getKeyNames() ; e.hasNext() ;) {
			try {
				String key = (String) e.next();
				String value = getString(key);
				if ( value.length()!=0 ) {
					if ( value.charAt(0) == '<' ) {
						sb.append(getString(key));
						sb.append('\n');
					} else {
						sb.append('<');
						sb.append(key);
						if( attributesVector.containsKey(key) ) {
							sb.append(' '); 
							sb.append(String.valueOf(getInternalAttributes(key)));
						}
						sb.append('>');
						sb.append(changeSpecialCharacters(value));
						sb.append("</");
						sb.append(key);
						sb.append(">\n");
					}
				} else {
					sb.append("<");
					sb.append(key);
					if ( attributesVector.containsKey(key) ) {
						sb.append(' ');
						sb.append(getInternalAttributes(key).toString());
					}
					sb.append('>');
					sb.append(changeSpecialCharacters(value));
					sb.append("</");
					sb.append(key);
					sb.append(">\n");
				}
			} catch( BIXMLException exc) { }
		}
		
		sb.append("</");
		sb.append(tagName);
		sb.append(">\n");
		return sb.toString();
	}
		
}
