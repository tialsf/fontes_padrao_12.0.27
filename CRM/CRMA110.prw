#INCLUDE "PROTHEUS.CH"
#INCLUDE "CRMA110.CH"  

//------------------------------------------------------------------------------
/*/ {Protheus.doc} CRMA110

Rotina que faz a chamada para o cadastro oportunidades de venda, enviando o filtro

@sample		CRMA110( cVisao )

@param		cVisao - Nome da vis�o a ser aberta inicialmente no browse 

@return		Nenhum

@author		Anderson Silva
@since		30/04/2014
@version	P12
/*/
//------------------------------------------------------------------------------
Function CRMA110( cVisao, cAlias )

Local aAddFil   	:= {}
Local cFilPri   	:= ""
Local lNoCheck  	:= .T. 
Local lSelected		:= .T.
Local lMsg			:= .T.
Local cAliasFil		:= "AO4"
Local cFilEnt		:= ""
Local cCodUsr		:= If(SuperGetMv("MV_CRMUAZS",, .F.), CRMXCodUser(), RetCodUsr()) 	

Static aCRM110Ent := {}

Default cVisao := ""  
Default cAlias := Alias()
If MPUserHasAccess("FATA300",/*nOpc*/,cCodUsr,lMsg)
	
	cFilPri   := CRMXFilEnt( "AD1", .T. )
	
	If !Empty( cFilPri )
		aAdd( aAddFil, { STR0006, cFilPri, lNoCheck, lSelected, cAliasFil, /*lFilterAsk*/, /*aFilParser*/, "AO4_FILENT" } )		// "Filtro do CRM"
	EndIf	
	
	If !( FunName() == "CRMA110" ) .And. ProcName( 2 ) <> "CRMA290RFUN" 
		If cAlias == "SA1"
			cFilEnt := " AD1_FILIAL == '" + xFilial( "AD1" ) + "' .AND. AD1_CODCLI == '" + SA1->A1_COD + "' .AND. AD1_LOJCLI == '" + SA1->A1_LOJA + "'"
			aCRM110Ent := { "SA1", SA1->A1_COD, SA1->A1_LOJA } 
		ElseIf cAlias == "SUS"
			cFilEnt := " AD1_FILIAL == '" + xFilial( "AD1" ) + "' .AND. AD1_PROSPE == '" + SUS->US_COD + "' .AND. AD1_LOJPRO == '" + SUS->US_LOJA + "'"
			aCRM110Ent := { "SUS", SUS->US_COD, SUS->US_LOJA } 
		EndIf 
	EndIf
	
	If !Empty( cFilEnt )
		aAdd( aAddFil, { STR0007, cFilEnt, lNoCheck, lSelected, "AD1", /*lFilterAsk*/, /*aFilParser*/, "FIL_ENT" } )		// "Filtro de Entidade"
	EndIf
	
	FATA300(/*nOperation*/,/*aAD1Master*/,/*aAD2Detail*/,/*aAD3Detail*/,;
			 /*aAD4Detail*/,/*aAD9Detail*/,/*aADJDetail*/,/*cFilDef*/	 ,;
			 aAddFil, cVisao ) 

	aCRM110Ent := {}
EndIf
		 
Return Nil

//------------------------------------------------------------------------------
/*/ {Protheus.doc} CRMA110Ent

Rotina para retornar o valora da variavel aCRM110Ent que contem os valores do registro posicionado

@sample	CRMA110Ent()

@param		Nenhum

@return   Array contendo os dados do registro posicionado	

@author	Victor Bitencourt
@since		01/04/2014
@version	12
/*/
//------------------------------------------------------------------------------
Function CRMA110Ent()
Return ( aCRM110Ent )

//------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

MenuDef - Opera��es que ser�o utilizadas pela aplica��o

@return   	aRotina - Array das opera��es

@author		Vendas CRM
@since		15/05/2015
@version	12
/*/
//------------------------------------------------------------------------------
Static Function MenuDef()

Private aRotina := StaticCall( FATA300, MENUDEF )

Return( aRotina )