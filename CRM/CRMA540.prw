#include "CRMA540.CH"
#Include "Protheus.ch"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "CRMDEF.CH"


//----------------------------------------------------------
/*/{Protheus.doc} CRMA540()

Rotina respons�vel pelo processamento da integra��o do uMov.Me

@param	  cXml -> contem informa��es sobre o arquivo que dever� ser 
processado
		 
@return  array

@author   Victor Bitencourt
@since    03/12/2014
@version  12.1.3
/*/
//----------------------------------------------------------
Function CRMA540(cXml)

Local aArea 	:= GetArea()
 
Local aDados 	:= {}

Local cLinha 	:= ""
Local cError    := ""
Local cWarning  := ""
Local cCaminho  := ""

Local nHandle   := -1
Local oXml		:= Nil

Private cCR540CodU  := ""
Private INCLUI      := .T.
Private lMsErroAuto := .F.
Private aRotina     := {}

Default cXml        := ""

//----------------------------------------
//	 Fazendo o Parse do Arquivo XML  
//----------------------------------------
oXML := XmlParser(cXml,"_",@cError,@cWarning) 


//----------------------------------------
//	 Processando o arquivo XML  
//----------------------------------------
If XmlChildEx( oXML, "_UMOVIMPORT" ) <> Nil

    cCaminho := oXML:_UmovImport:_RelativeDirectory:Text+"\"+oXML:_UmovImport:_FileName:Text

	//--------------------------
	//	 Abrindo o Arquivo   
	//--------------------------
	nHandle := FT_FUSE(cCaminho) 
	
	If nHandle > -1
		
		While !FT_FEof()
			
			cLinha 		:= FT_FReadLn()
			cLinha 		:= NoAcento(AnsiToOem(cLinha)) 
			aDados 		:= SEPARA(cLinha,";") 
					
			//------------------------------------------------------------------------------------
			//	 Chamando as rotinas de Grava��o comforme o tipo da Atividade a ser processada   
			//------------------------------------------------------------------------------------			
			Do Case
			 	Case aDados[_ATIV_OPER] == "INSERT" .AND. aDados[_ATIV_TIPO] == "CHECK IN" 	.AND. !Empty(aDados[_ATIV_CODUSR])
								
						GrvChkInOut(SubStr(aDados[_ATIV_DATAHORA],12,5),StrTran(SubStr(aDados[_ATIV_DATAHORA],1,10),"-",""),SubStr(aDados[_ATIV_CODENT],1,8),SubStr(aDados[_ATIV_CODENT],9,3),aDados,aDados[_ATIV_CODUSR],_CHECKIN)
								
              Case aDados[_ATIV_OPER] == "INSERT" .AND. aDados[_ATIV_TIPO] == "CHECK OUT"	.AND. !Empty(aDados[_ATIV_CODUSR])
						
						GrvChkInOut(SubStr(aDados[_ATIV_DATAHORA],12,5),StrTran(SubStr(aDados[_ATIV_DATAHORA],1,10),"-",""),SubStr(aDados[_ATIV_CODENT],1,8),SubStr(aDados[_ATIV_CODENT],9,3),aDados,aDados[_ATIV_CODUSR],_CHECKOUT)
						
              Case aDados[_ATIV_OPER] == "INSERT" .AND. aDados[_ATIV_TIPO] == "CANCELAR VISITA"	.AND. !Empty(aDados[_ATIV_CODUSR])		
               
               		CancelAtiv(SubStr(aDados[_ATIV_DATAHORA],12,5),StrTran(SubStr(aDados[_ATIV_DATAHORA],1,10),"-",""),SubStr(aDados[_ATIV_CODENT],1,8),SubStr(aDados[_ATIV_CODENT],9,3),aDados,aDados[_ATIV_CODUSR],_CANCELAT)
              
              Case aDados[_ATIV_OPER] == "INSERT" .AND. aDados[_ATIV_TIPO] == "POSSIVEL CLIENTE"	.AND. !Empty(aDados[_ATIV_CODUSR])	
               
                     GrvPossCli(Upper(AllTrim(aDados[_PSSCLIENT_TIPO])),aDados,aDados[_ATIV_CODUSR])
               		
			EndCase
		 
			//--------------------------------------------------------
			//	 Limpando as variaveis para o proximo processamento
	 		//--------------------------------------------------------			 
			
			Asize( aDados, 0)
				
			FT_FSkip()
			
		EndDo
		
		//----------------------
		//	 Fechando o Arquivo  
	 	//----------------------	
		Ft_fuse()
		
	Else
		Conout(STR0001)//"Falha na abertura do arquivo!"
	EndIf
EndIf

RestArea(aArea)

Return {.T.,"OK"}


//----------------------------------------------------------
/*/{Protheus.doc} GrvChkInOut()

Rotina responsavel pela grava��o do Check-in\ Check-out

@param	  cHora hora da atividade
	      cData - data da atividade
	      cCodEnt - c�digo da entidade
	      cAliasEnt - alias da entidade
	      aDados - array com os dados 
	      cCodUsr - C�digo de usuario
	      cTipo - Tipo de Atividade, pode ser Check-in ou Check-out
		 
@return  Nenhum

@author   Victor Bitencourt
@since    03/12/2014
@version  12.1.3
/*/
//----------------------------------------------------------
Static Function GrvChkInOut(cHora,cData,cCodEnt,cAliasEnt,aDados,cCodUsr,cTipo)

Local aArea        := GetArea()
Local aAreaAIO     := AIO->(GetArea())
Local aAreaAOF     := AOF->(GetArea())
Local aNvlEstrut   := CRMXNvlEst(cCodUsr)

Local aHrGrvAOF    := {}
Local aHoraAge     := {}
Local aExecAuto    := {}

// AIO
Local cCodUnicATIV := aDados[_ATIV_IDUNICO]
Local cDataFim     := ""
Local cHoraFim     := ""
Local cStatus      := ""


AIO->(DbSetOrder(3))	//AIO_FILIAL+AIO_TIPO+AIO_IDAGE
AOF->(DbSetOrder(7))	//AOF_FILIAL+AOF_CODUMO


//-------------------------------------------------------------------
//	 Verificando qual � o tipo de Atividade, dever� ser porcessada   
//-------------------------------------------------------------------
If cTipo == _CHECKIN  

	//---------------------------
	//	 Iniciando a transa��o   
	//---------------------------
	Begin Transaction
			
			//--------------------------------------------------
			//	 Criando o Registro do Check-in na tabela AIO   
			//--------------------------------------------------
			
			If !AIO->(DbSeek(xFilial("AIO")+cTipo+cCodUnicATIV)) 
			
				ASize(aExecAuto, 0)
				AAdd(aExecAuto,{"AIO_FILIAL",xFilial("AIO")          ,Nil})
				AAdd(aExecAuto,{"AIO_CODUSR",cCodUsr                 ,Nil})
				AAdd(aExecAuto,{"AIO_TIPO"  ,cTipo                   ,Nil})
				AAdd(aExecAuto,{"AIO_DATA"  ,STOD(cData)             ,Nil})
				AAdd(aExecAuto,{"AIO_ENT"   ,cCodEnt                 ,Nil})
				AAdd(aExecAuto,{"AIO_HORA"  ,cHora                   ,Nil})
				AAdd(aExecAuto,{"AIO_LATITU",aDados[_ATIV_LATITUDE]  ,Nil})
				AAdd(aExecAuto,{"AIO_LONGIT",aDados[_ATIV_LONGITUDE] ,Nil})
				AAdd(aExecAuto,{"AIO_ALIAS" ,"AOF"	                  ,Nil})
				AAdd(aExecAuto,{"AIO_IDAGE" ,cCodUnicATIV            ,Nil})
				
				CtrlGrvAuto("CRMA140","AIO",aExecAuto,MODEL_OPERATION_INSERT)
			EndIf	

			//-----------------------------------------------------------------------------------------
			//	 Verificando se existe atividades ja criadas no protheus, proximo a data/hora Check-in   
			//-----------------------------------------------------------------------------------------
			AOF->(DbSetOrder(4))//AOF_FILIAL+AOF_ENTIDA+AOF_CHAVE+DTOS(AOF_DTINIC)+AOF_HRINIC
			If AOF->(DbSeek(xFilial("AOF")+cAliasEnt+cCodEnt))
				While !AOF->(Eof()) .AND. AOF->AOF_FILIAL == xFilial("AOF") .AND.;
				       AOF->AOF_ENTIDA == cAliasEnt .AND. RTrim(AOF->AOF_CHAVE) == cCodEnt 
				     
				       If Empty(AOF->AOF_CODUMO) .AND. DTOS(AOF->AOF_DTINIC) == cData .AND. AOF->AOF_TIPO == TPCOMPROMISSO
							AAdd(aHrGrvAOF,{AOF->AOF_CODIGO,AOF->AOF_HRINIC+":00"})
						EndIf	
					
					AOF->(DbSkip())
				EndDo
			EndIf		
			AOF->(DbSetOrder(7))	//AOF_FILIAL+AOF_CODUMO
			
			//------------------------------------------------------------------------------------------
			//	 Verificando se array aHrGrvAOF existe registro, se sim chama a rotina para verificar
			// 	 a se a diferen�a entre a data/hora da atividade no Protheus e a data/hora do Check-in  
			//  n�o ultrapassou a toler�ncia estipulada no sistema, caso tenha ultrapassado, cria uma
			//  nova atividade, sen�o faz o vinculo das informa��es do Check-in na atividade encontrada     
			//------------------------------------------------------------------------------------------
			If Len(aHrGrvAOF) > 0
				aHoraAge := CRMV540TOL(aHrGrvAOF,cHora+":00") ///ARRUMAR ***********
			EndIf

			If !Empty(aHoraAge)	
			//----------------------------------------------------------------------------
			//	  Encontrou uma Atividade que sera relacionada com o registro do check-in.  
			//----------------------------------------------------------------------------

					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"AOF_FILIAL"  ,xFilial("AOF")  ,Nil})
					AAdd(aExecAuto,{"AOF_CODIGO"  ,aHoraAge[1]     ,Nil})
					AAdd(aExecAuto,{"AOF_CODUMO"  ,cCodUnicATIV    ,Nil})
					AAdd(aExecAuto,{"AOF_LOCAL"   ,aDados[_ATIV_LOCAL]+", "+aDados[_ATIV_LOCALNUM],Nil})

					//---------------------------------------------------------------------------------
					// Verificando se j� existe o Check-out, se sim Altera as informa��es da atividade
					// com alguns dados do Check-out e deixa o Status como Concluida.
					//---------------------------------------------------------------------------------					
					If AIO->(DbSeek(xFilial("AIO")+_CHECKOUT+cCodUnicATIV))
						AAdd(aExecAuto,{"AOF_DTFIM"  ,CRMN540GTT(cCodUnicATIV,"1",_CHECKOUT) ,Nil})
						AAdd(aExecAuto,{"AOF_HRFIM"  ,CRMN540GTT(cCodUnicATIV,"2",_CHECKOUT) ,Nil})
						AAdd(aExecAuto,{"AOF_DTCONC" ,CRMN540GTT(cCodUnicATIV,"1",_CHECKOUT) ,Nil})
                     cStatus    := STCONCLUIDO
					EndIf

					AAdd(aExecAuto,{"AOF_STATUS" ,IIF(!Empty(cStatus),cStatus,STEMANDAMENTO) ,Nil})
					AAdd(aExecAuto,{"AOF_PERCEN" ,IIF(!Empty(cStatus),"5","2")               ,Nil})	
										
					CtrlGrvAuto("CRMA180","AOF",aExecAuto,MODEL_OPERATION_UPDATE)

			Else
			//-----------------------------------------------------------------------------
			//	N�o encontrou uma Atividade que ser� relacionada com o registro do check-in,
			// ent�o cria uma nova.
			//-----------------------------------------------------------------------------
				If !AOF->(DbSeek(xFilial("AOF")+cCodUnicATIV)) 
				
				//-----------------------------------------------------------------------------
				//	verifica se n�o exite uma atividade j� criada com o mesmo c�digo do umov
				//-----------------------------------------------------------------------------
				
					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"AOF_FILIAL"  ,xFilial("AOF")              ,Nil})
					AAdd(aExecAuto,{"AOF_DTCAD"   ,dDatabase                   ,Nil})
					AAdd(aExecAuto,{"AOF_TIPO"    ,TPCOMPROMISSO               ,Nil})
					AAdd(aExecAuto,{"AOF_PERCEN"  ,"2"                         ,Nil})
					AAdd(aExecAuto,{"AOF_CODUSR"  ,cCodUsr                     ,Nil})
					AAdd(aExecAuto,{"AOF_ASSUNT"  ,Upper(aDados[_CHKIN_ASSUNT]),Nil})
					AAdd(aExecAuto,{"AOF_DTINIC"  ,STOD(cData)                 ,Nil})
					AAdd(aExecAuto,{"AOF_HRINIC"  ,cHora                       ,Nil})
								
					//---------------------------------------------------------------------------------
					// Verificando se j� existe o Check-out, se sim grava as informa��es da atividade
					// com alguns dados do Check-out
					//---------------------------------------------------------------------------------
					If AIO->(DbSeek(xFilial("AIO")+_CHECKOUT+cCodUnicATIV))
						 cDataFim   := CRMN540GTT(cCodUnicATIV,"1",_CHECKOUT)
						 cHoraFim   := CRMN540GTT(cCodUnicATIV,"2",_CHECKOUT)
						 cStatus    := STCONCLUIDO
						 AAdd(aExecAuto,{"AOF_DTCONC" ,cDataFim     ,Nil})
					EndIf
					
					AAdd(aExecAuto,{"AOF_STATUS"  ,IIF(!Empty(cStatus),cStatus,STEMANDAMENTO) ,Nil})					
					AAdd(aExecAuto,{"AOF_DTFIM"   ,IIF(!Empty(cDataFim),cDataFim,STOD(cData)) ,Nil})
					AAdd(aExecAuto,{"AOF_HRFIM"   ,IIF(!Empty(cHoraFim),cHoraFim,cHora)       ,Nil})
					AAdd(aExecAuto,{"AOF_ENTIDA"  ,cAliasEnt                   ,Nil})
					AAdd(aExecAuto,{"AOF_CHAVE"   ,cCodEnt                     ,Nil})
					AAdd(aExecAuto,{"AOF_LOCAL"   ,aDados[_ATIV_LOCAL]+", "+aDados[_ATIV_LOCALNUM] ,Nil})
					AAdd(aExecAuto,{"AOF_CODUMO"  ,cCodUnicATIV                ,Nil})
					AAdd(aExecAuto,{"AOF_IDESTN"  ,aNvlEstrut[1]               ,Nil})
					AAdd(aExecAuto,{"AOF_NVESTN"  ,aNvlEstrut[2]               ,Nil})

					CtrlGrvAuto("CRMA180","AOF",aExecAuto,MODEL_OPERATION_INSERT)
					
				EndIf	
			EndIf

	//---------------------------
	//	 Encerrando a transa��o   
	//---------------------------
	End Transaction
				
ElseIf cTipo == _CHECKOUT  

	//---------------------------
	//	 Iniciando a transa��o   
	//---------------------------
	Begin Transaction

			//--------------------------------------------------------------------------------------
			//	Verificando se existe uma atividade, j� criada com o idunico do umov e que
			// n�o esteja concluida,� feita essa valida��o, porque o Check-out pode chegar primeiro
			// que o check-in no arquivo, j� que trata-se de uma integra��o.
			//--------------------------------------------------------------------------------------
			If AOF->(DbSeek(xFilial("AOF")+cCodUnicATIV)) .AND. AOF->AOF_STATUS <> STCONCLUIDO
					
				//--------------------------------------------------------------------------------------
				//	Atualizando a data/hora Fim da Atividade no Protheus com a data/hora do Check-out 
				//--------------------------------------------------------------------------------------
				ASize(aExecAuto, 0)
				AAdd(aExecAuto,{"AOF_FILIAL" ,xFilial("AOF")  ,Nil})
				AAdd(aExecAuto,{"AOF_CODIGO" ,AOF->AOF_CODIGO ,Nil})
				AAdd(aExecAuto,{"AOF_CODUMO" ,cCodUnicATIV    ,Nil})
				AAdd(aExecAuto,{"AOF_DTFIM"  ,STOD(cData)     ,Nil})
				AAdd(aExecAuto,{"AOF_HRFIM"  ,cHora           ,Nil})
				AAdd(aExecAuto,{"AOF_STATUS" ,STCONCLUIDO     ,Nil})
				AAdd(aExecAuto,{"AOF_DTCONC" ,STOD(cData)     ,Nil})
						
				CtrlGrvAuto("CRMA180","AOF",aExecAuto,MODEL_OPERATION_UPDATE)	
					
			EndIf		
					
			//----------------------------------------------------------------------------
			//	 Verificando se existe um registro de Check-out criado na AIO com o mesmo
			//  idUnico. � feita essa valida��o, porque no arquivo pode ser que repita as
			//  informa��es do Check-out por causa dos itens  
			//----------------------------------------------------------------------------
			If !AIO->(DbSeek(xFilial("AIO")+cTipo+cCodUnicATIV))
				
					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"AIO_FILIAL",xFilial("AIO")          ,Nil})
					AAdd(aExecAuto,{"AIO_CODUSR",cCodUsr                 ,Nil})
					AAdd(aExecAuto,{"AIO_TIPO"  ,cTipo                   ,Nil})
					AAdd(aExecAuto,{"AIO_DATA"  ,STOD(cData)             ,Nil})
					AAdd(aExecAuto,{"AIO_ENT"   ,cCodEnt                 ,Nil})
					AAdd(aExecAuto,{"AIO_HORA"  ,cHora                   ,Nil})
					AAdd(aExecAuto,{"AIO_LATITU",aDados[_ATIV_LATITUDE]  ,Nil})
					AAdd(aExecAuto,{"AIO_LONGIT",aDados[_ATIV_LONGITUDE] ,Nil})
					AAdd(aExecAuto,{"AIO_ALIAS","AOF"	                  ,Nil})
					AAdd(aExecAuto,{"AIO_IDAGE",cCodUnicATIV             ,Nil})
					
					CtrlGrvAuto("CRMA140","AIO",aExecAuto,MODEL_OPERATION_INSERT)
			EndIf		

			//----------------------------------------------------------------------------
			// Verificando se existe uma observa��o do Check-out no arquivo, essa informa��o
			// vem somente na �ltima linha do arquivo, ent�o � necess�rio validar dessa forma
			// at� chegar na �ltima linha se existir a infoma��o de observa��o, pegamos e
			// atualizamos o registro de check-out na AIO
			//----------------------------------------------------------------------------
			If !Empty(aDados[_CHKOUT_OBSERV]) .AND. AIO->(DbSeek(xFilial("AIO")+cTipo+cCodUnicATIV)) 
				
					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"AIO_FILIAL",xFilial("AIO")          ,Nil})
					AAdd(aExecAuto,{"AIO_COD"   ,AIO->AIO_COD            ,Nil})
					AAdd(aExecAuto,{"AIO_TIPO"  ,cTipo                   ,Nil})
					AAdd(aExecAuto,{"AIO_IDAGE" ,cCodUnicATIV            ,Nil})
					AAdd(aExecAuto,{"AIO_OBS"   ,aDados[_CHKOUT_OBSERV]  ,Nil})

					CtrlGrvAuto("CRMA140","AIO",aExecAuto,MODEL_OPERATION_UPDATE)
			EndIf		

			
			//-----------------------------------------------------------------------------------------
			// Gravando os itens do Check-out, referentes as informa��es das possiveis negocia��es
			// n�o � feito nenhum looping aqui, porque na fun��o principal CRMA540 j� est� acontecendo
			// o looping passando linha por linha, j� que cadas iten tambem representa uma linha. 
			//-----------------------------------------------------------------------------------------
			If !Empty(aDados[_CHKOUT_IDITENS]) 
				ASize(aExecAuto, 0)
				AAdd(aExecAuto,{"AOJ_FILIAL" ,xFilial("AOJ")                                 ,Nil})
				AAdd(aExecAuto,{"AOJ_ENTIDA" ,cAliasEnt                                      ,Nil})
				AAdd(aExecAuto,{"AOJ_CHAVE"  ,cCodEnt                                        ,Nil})
				AAdd(aExecAuto,{"AOJ_CODCAT" ,aDados[_CHKOUT_IDITENS]                        ,Nil})
				AAdd(aExecAuto,{"AOJ_FEELIN" ,aDados[_CHKOUT_FEELING]                        ,Nil})
				AAdd(aExecAuto,{"AOJ_VLRPRE" ,VAL(aDados[_CHKOUT_VLPREV])                    ,Nil})
				AAdd(aExecAuto,{"AOJ_DTCAD"  ,STOD(cData)                                    ,Nil})
				AAdd(aExecAuto,{"AOJ_DTEXP"  ,STOD(StrTran(aDados[_CHKOUT_DTEXPEC],"-",""))  ,Nil})
				AAdd(aExecAuto,{"AOJ_OBS"    ,aDados[_CHKOUT_NEGOBS]                         ,Nil})
				AAdd(aExecAuto,{"AOJ_IDAGEN" ,cCodUnicATIV                                   ,Nil})
				
				CtrlGrvAuto("CRMA570","AOJ",aExecAuto,MODEL_OPERATION_INSERT)
			EndIf	

	//---------------------------
	//	 Encerrando a transa��o   
	//---------------------------		
	End Transaction
				
EndIf
		
RestArea(aAreaAOF)
RestArea(aAreaAIO)
RestArea(aArea)

Return


//----------------------------------------------------------
/*/{Protheus.doc} CancelAtiv()

Rotina responsavel pela grava��o do cancelamento da Atividade

@param	  cHora hora da atividade
	      cData - data da atividade
	      cCodEnt - c�digo da entidade
	      cAliasEnt - alias da entidade
	      aDados - array com os dados 
	      cCodUsr - C�digo de usuario
	      cTipo - Tipo de Atividade "3" = Cancelar Visita
		 
@return  Nenhum

@author   Victor Bitencourt
@since    09/12/2014
@version  12.1.3
/*/
//----------------------------------------------------------
Static Function CancelAtiv(cHora,cData,cCodEnt,cAliasEnt,aDados,cCodUsr,cTipo)

Local aArea        := GetArea()
Local aAreaAIO     := AIO->(GetArea())
Local aAreaAOF     := AOF->(GetArea())
Local aNvlEstrut   := CRMXNvlEst(cCodUsr)

Local aExecAuto    := {}
Local aHrGrvAOF    := {}
Local aHoraAge     := {}

// AIO
Local cCodUnicATIV := aDados[_ATIV_IDUNICO]


AIO->(DbSetOrder(3))	//AIO_FILIAL+AIO_TIPO+AIO_IDAGE
AOF->(DbSetOrder(7))	//AOF_FILIAL+AOF_CODUMO


Begin Transaction

	//----------------------------------------------------
	//	 Criando o Registro de Cancelamento na tabela AIO   
	//----------------------------------------------------
	
	If !AIO->(DbSeek(xFilial("AIO")+cTipo+cCodUnicATIV)) 
	
		ASize(aExecAuto, 0)
		AAdd(aExecAuto,{"AIO_FILIAL",xFilial("AIO")          ,Nil})
		AAdd(aExecAuto,{"AIO_CODUSR",cCodUsr                 ,Nil})
		AAdd(aExecAuto,{"AIO_TIPO"  ,cTipo                   ,Nil})
		AAdd(aExecAuto,{"AIO_DATA"  ,STOD(cData)             ,Nil})
		AAdd(aExecAuto,{"AIO_ENT"   ,cCodEnt                 ,Nil})
		AAdd(aExecAuto,{"AIO_HORA"  ,cHora                   ,Nil})
		AAdd(aExecAuto,{"AIO_LATITU",aDados[_ATIV_LATITUDE]  ,Nil})
		AAdd(aExecAuto,{"AIO_LONGIT",aDados[_ATIV_LONGITUDE] ,Nil})
		AAdd(aExecAuto,{"AIO_ALIAS" ,"AOF"	                  ,Nil})
		AAdd(aExecAuto,{"AIO_IDAGE" ,cCodUnicATIV            ,Nil})
		AAdd(aExecAuto,{"AIO_OBS"   ,aDados[_CANCEL_OBSERV]  ,Nil})
		
		CtrlGrvAuto("CRMA140","AIO",aExecAuto,MODEL_OPERATION_INSERT)
	EndIf	

	//------------------------------------------------------------------------------------------------
	//	 Verificando se existe atividades ja criadas no protheus, proximo a data/hora do Cancelamento  
	//------------------------------------------------------------------------------------------------
	AOF->(DbSetOrder(4))//AOF_FILIAL+AOF_ENTIDA+AOF_CHAVE+DTOS(AOF_DTINIC)+AOF_HRINIC
	If AOF->(DbSeek(xFilial("AOF")+cAliasEnt+cCodEnt))
		While !AOF->(Eof()) .AND. AOF->AOF_FILIAL == xFilial("AOF") .AND.;
		       AOF->AOF_ENTIDA == cAliasEnt .AND. RTrim(AOF->AOF_CHAVE) == cCodEnt 
		       
		       If Empty(AOF->AOF_CODUMO) .AND. DTOS(AOF->AOF_DTINIC) == cData .AND. AOF->AOF_TIPO == TPCOMPROMISSO
					AAdd(aHrGrvAOF,{AOF->AOF_CODIGO,AOF->AOF_HRINIC+":00"})
				EndIf	
			
			AOF->(DbSkip())
		EndDo
	EndIf		
	AOF->(DbSetOrder(7))	//AOF_FILIAL+AOF_CODUMO
	
	//------------------------------------------------------------------------------------------
	//	 Verificando se array aHrGrvAOF existe registro, se sim chama a rotina para verificar
	// 	 a se a diferen�a entre a data/hora da atividade no Protheus e a data/hora do Check-in  
	//  n�o ultrapassou a toler�ncia estipulada no sistema, caso tenha ultrapassado, cria uma
	//  nova atividade, sen�o faz o vinculo das informa��es do Check-in na atividade encontrada     
	//------------------------------------------------------------------------------------------
	If Len(aHrGrvAOF) > 0
		aHoraAge := CRMV540TOL(aHrGrvAOF,cHora+":00") ///ARRUMAR ***********
	EndIf

	If !Empty(aHoraAge)	
	//----------------------------------------------------------------------------
	//	  Encontrou uma Atividade que sera relacionada com o registro do check-in.  
	//----------------------------------------------------------------------------
			cCR540CodU := cCodUsr
			ASize(aExecAuto, 0)
			AAdd(aExecAuto,{"AOF_FILIAL"  ,xFilial("AOF")  ,Nil})
			AAdd(aExecAuto,{"AOF_CODIGO"  ,aHoraAge[1]     ,Nil})
			AAdd(aExecAuto,{"AOF_CODUMO"  ,cCodUnicATIV    ,Nil})
			AAdd(aExecAuto,{"AOF_LOCAL"   ,aDados[_ATIV_LOCAL]+", "+aDados[_ATIV_LOCALNUM] ,Nil})
			AAdd(aExecAuto,{"AOF_STATUS"  ,STCANCELADO     ,Nil})
			AAdd(aExecAuto,{"AOF_PERCEN"  ,"1"             ,Nil})
			AAdd(aExecAuto,{"AOF_DTCONC"  ,STOD(cData)     ,Nil})	
								
			CtrlGrvAuto("CRMA180","AOF",aExecAuto,MODEL_OPERATION_UPDATE)
			cCR540CodU := ""

	Else
	//-----------------------------------------------------------------------------
	//	N�o encontrou uma Atividade que ser� relacionada com o registro do check-in,
	// ent�o cria uma nova.
	//-----------------------------------------------------------------------------
	
		If !AOF->(DbSeek(xFilial("AOF")+cCodUnicATIV)) 
		//------------------------------------------------------------------------------------
		//  Atualizando a data/hora Fim da Atividade no Protheus com a data/hora do Check-out 
		//------------------------------------------------------------------------------------
		
			ASize(aExecAuto, 0)
			AAdd(aExecAuto,{"AOF_FILIAL"  ,xFilial("AOF")              ,Nil})
			AAdd(aExecAuto,{"AOF_DTCAD"   ,dDatabase                   ,Nil})
			AAdd(aExecAuto,{"AOF_TIPO"    ,TPCOMPROMISSO               ,Nil})
			AAdd(aExecAuto,{"AOF_PERCEN"  ,"1"                         ,Nil})
			AAdd(aExecAuto,{"AOF_CODUSR"  ,cCodUsr                     ,Nil})
			AAdd(aExecAuto,{"AOF_ASSUNT"  ,Upper(STR0002)              ,Nil})//"Cancelamento do Compromisso"
			AAdd(aExecAuto,{"AOF_DESCRI"  ,aDados[_CANCEL_OBSERV]      ,Nil})
			AAdd(aExecAuto,{"AOF_DTINIC"  ,STOD(cData)                 ,Nil})
			AAdd(aExecAuto,{"AOF_HRINIC"  ,cHora                       ,Nil})
			AAdd(aExecAuto,{"AOF_DTCONC"  ,STOD(cData)                 ,Nil})	
			AAdd(aExecAuto,{"AOF_STATUS"  ,STCANCELADO                 ,Nil})					
			AAdd(aExecAuto,{"AOF_DTFIM"   ,STOD(cData)                 ,Nil})
			AAdd(aExecAuto,{"AOF_HRFIM"   ,cHora                       ,Nil})
			AAdd(aExecAuto,{"AOF_ENTIDA"  ,cAliasEnt                   ,Nil})
			AAdd(aExecAuto,{"AOF_CHAVE"   ,cCodEnt                     ,Nil})
			AAdd(aExecAuto,{"AOF_LOCAL"   ,aDados[_ATIV_LOCAL]+", "+aDados[_ATIV_LOCALNUM] ,Nil})
			AAdd(aExecAuto,{"AOF_CODUMO"  ,cCodUnicATIV                ,Nil})
			AAdd(aExecAuto,{"AOF_IDESTN"  ,aNvlEstrut[1]               ,Nil})
			AAdd(aExecAuto,{"AOF_NVESTN"  ,aNvlEstrut[2]               ,Nil})

			CtrlGrvAuto("CRMA180","AOF",aExecAuto,MODEL_OPERATION_INSERT)
			
		EndIf	
	EndIf

//---------------------------
//	 Encerrando a transa��o   
//---------------------------
End Transaction

Return


//----------------------------------------------------------
/*/{Protheus.doc} CancelAtiv()

Rotina responsavel pela grava��o de possiveis clientes

@param	  cEntidade - entidade que o registro pertence
          aDados - array com os dados
          cCodUsr - C�digo do usu�rio
		 
@return  

@author   Victor Bitencourt
@since    09/12/2014
@version  12.1.3
/*/
//----------------------------------------------------------
Static Function GrvPossCli(cEntidade,aDados,cCodUsr)

Local aExecAuto := {}
		
Default cEntidade := ""		
		
If !Empty(cEntidade)
	
	Begin Transaction
	 	//-------------------------------------------------------------------------------
		//	Adicionando o Codigo do Usu�rio no .txt, n� variavel que a rotina 
		// CRMA200PAut do controle de acesso enxerga, para cadastrar os registros, com
		// as devidas permiss�es conforme o usu�rio   
		//-------------------------------------------------------------------------------
		cCR540CodU := cCodUsr	
		Do Case
		
		 	Case cEntidade == "SUSPECT"
			//----------------------------------------
			//	Processando dados do SUSPECT   
			//----------------------------------------
			
					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"ACH_FILIAL" ,xFilial("ACH")                 ,Nil})
					AAdd(aExecAuto,{"ACH_LOJA"   ,"01"                           ,Nil})
					AAdd(aExecAuto,{"ACH_TIPO"   ,"1"                            ,Nil})
					AAdd(aExecAuto,{"ACH_RAZAO"  ,Upper(aDados[_PSSCLIENT_NOME]) ,Nil})
					AAdd(aExecAuto,{"ACH_DDD"    ,aDados[_PSSCLIENT_DDD]         ,Nil})
					AAdd(aExecAuto,{"ACH_TEL"    ,aDados[_PSSCLIENT_TEL]         ,Nil})
					AAdd(aExecAuto,{"ACH_EST"    ,Upper(aDados[_PSSCLIENT_ESTAD]),Nil})
					AAdd(aExecAuto,{"ACH_CIDADE" ,Upper(aDados[_PSSCLIENT_MUN])  ,Nil})
					AAdd(aExecAuto,{"ACH_EMAIL"  ,aDados[_PSSCLIENT_EMAIL]       ,Nil})
					AAdd(aExecAuto,{"ACH_ORIGEM" ,"B"                            ,Nil})
					AAdd(aExecAuto,{"ACH_OBS"    ,aDados[_PSSCLIENT_OBS]         ,Nil})
					AAdd(aExecAuto,{"ACH_VEND"   ,CRMXRetVend( cCodUsr )         ,Nil})
					AAdd(aExecAuto,{"ACH_IMGUMO" ,aDados[_PSSCLIENT_FOTO]        ,Nil})
					
					CtrlGrvAuto("TMKA341","ACH",aExecAuto,MODEL_OPERATION_INSERT)
			
			Case cEntidade == "PROSPECT"
			//----------------------------------------
			//	Processando dados do PROSPECT   
			//----------------------------------------		
					ASize(aExecAuto, 0)
					AAdd(aExecAuto,{"US_FILIAL"  ,xFilial("SUS")                 ,Nil})
					AAdd(aExecAuto,{"US_LOJA"    ,"01"                           ,Nil})
					AAdd(aExecAuto,{"US_TIPO"    ,"F"                            ,Nil})
					AAdd(aExecAuto,{"US_NOME"    ,Upper(aDados[_PSSCLIENT_NOME]) ,Nil})
					AAdd(aExecAuto,{"US_EST"     ,aDados[_PSSCLIENT_ESTAD]       ,Nil})
					AAdd(aExecAuto,{"US_MUN"     ,Upper(aDados[_PSSCLIENT_MUN])  ,Nil})
					AAdd(aExecAuto,{"US_END"     ,Upper(aDados[_PSSCLIENT_ENDER]),Nil})
					AAdd(aExecAuto,{"US_DDD"     ,aDados[_PSSCLIENT_DDD]         ,Nil})
					AAdd(aExecAuto,{"US_TEL"     ,aDados[_PSSCLIENT_TEL]         ,Nil})
					AAdd(aExecAuto,{"US_ORIGEM"  ,"B"                            ,Nil})
					AAdd(aExecAuto,{"US_OBS"     ,aDados[_PSSCLIENT_OBS]         ,Nil})
					AAdd(aExecAuto,{"US_VEND"    ,CRMXRetVend( cCodUsr )         ,Nil})
					AAdd(aExecAuto,{"US_IMGUMOV" ,aDados[_PSSCLIENT_FOTO]        ,Nil})
					
					CtrlGrvAuto("TMKA260","SUS",aExecAuto,MODEL_OPERATION_INSERT)
			
		   Case cEntidade == "CLIENTE"
			//----------------------------------------
			//	Processando dados do CLIENTE   
			//----------------------------------------		
		
					ASize(aExecAuto, 0)
					aAdd(aExecAuto,{"A1_COD"		   ,CRMN540GCD("SA1")})
					aAdd(aExecAuto,{"A1_LOJA"	   ,"01"})
					aAdd(aExecAuto,{"A1_NOME"      ,Upper(aDados[_PSSCLIENT_NOME])})
					aAdd(aExecAuto,{"A1_NREDUZ"    ,Upper(SubStr(aDados[_PSSCLIENT_NOME],1,20)) })
					aAdd(aExecAuto,{"A1_TIPO"	   ,"F"})
					aAdd(aExecAuto,{"A1_EST"		   ,aDados[_PSSCLIENT_ESTAD]})
					aAdd(aExecAuto,{"A1_MUN"		   ,Upper(aDados[_PSSCLIENT_MUN])})
					aAdd(aExecAuto,{"A1_END"		   ,Upper(aDados[_PSSCLIENT_ENDER])})
					aAdd(aExecAuto,{"A1_DDD"		   ,aDados[_PSSCLIENT_DDD]})
					aAdd(aExecAuto,{"A1_TEL"		   ,aDados[_PSSCLIENT_TEL]})
					aAdd(aExecAuto,{"A1_EMAIL"	   ,aDados[_PSSCLIENT_EMAIL]})
					aAdd(aExecAuto,{"A1_ORIGCT"	   ,"B"})
					aAdd(aExecAuto,{"A1_OBSERV"	   ,aDados[_PSSCLIENT_OBS]})
					aAdd(aExecAuto,{"A1_RECCOFI"   ,"N"})
					aAdd(aExecAuto,{"A1_VEND"	   ,CRMXRetVend( cCodUsr )  })
					aAdd(aExecAuto,{"A1_IMGUMOV"   ,aDados[_PSSCLIENT_FOTO]})
		
					//---------------------------------------------------------------
					//	Validando a estrutura dos campos do execAut, fui necess�rio
					// fazer essa valida��o porque a rotina automatica de grava��o
					// do Cliente n�o est� em MVC.   
					//---------------------------------------------------------------		
					aExecAuto := CRMV540VST("SA1",aExecAuto)
		
					CtrlGrvAuto("MATA030","SA1",aExecAuto,MODEL_OPERATION_INSERT)
		EndCase
		
		//----------------------------------------------------
		//	Limpando a variavel para o proximo processamento
		//----------------------------------------------------		
		cCR540CodU := ""
	
	End Transaction

EndIf


Return


//----------------------------------------------------------
/*/{Protheus.doc} CtrlGrvAuto()

Rotina para controlar as grava��es e performance 
das rotinas automaticas

@param	  cModel - Descrica��o do model que dever� ser instanciado
          cAlias  -  alias que dever� ser feita a grava��o
          aAutoRot - array contendo as informa��es de grava��o	
          nOpcAuto - opera��o que dever� ser realizada
		 
@return  Nenhum  

@author   Victor Bitencourt
@since    03/12/2014
@version  12.1.3
/*/
//----------------------------------------------------------
Static Function CtrlGrvAuto(cModel,cAlias,aAutoRot,nOpcAuto)

Local nPosMdl	  := 0	
Local aAutoErro := {}

Do Case
	Case cAlias == "AOF"
	
		CRMA180( aAutoRot, nOpcAuto, .T.)
		
	Case cAlias == "AOJ"
		
		CRMA570(aAutoRot, nOpcAuto, .T.)
	
	Case cAlias == "AIO"
		
		CRMA140(aAutoRot, nOpcAuto, .T.)
		
	Case cAlias == "ACH"
		TMKA341(aAutoRot, nOpcAuto)
		
	Case cAlias == "SUS"
		TMKA260(aAutoRot, nOpcAuto)

	Case cAlias == "SA1"	
	
		MSExecAuto({|x,y| MATA030(x,y)},aAutoRot,nOpcAuto)
		
EndCase
		
Return
