#INCLUDE "BCDEFINITION.CH"
#INCLUDE "BCDATABASE.CH"

Static __aShared := {}

//-------------------------------------------------------------------
/*/{Protheus.doc} BCProcessTable
Define a estrutura da table de Processos (Views) e realiza a cria��o.

@author Helio Leal  
@since 29/08/2017
/*/
//-------------------------------------------------------------------
Function BCProcessTable()
	Local oQuery := Nil
	Local aTable := {}
	Local nTable := 1

	//-------------------------------------------------------------------
	// Cria a tabela de processos. 
	//-------------------------------------------------------------------  
	oQuery := TBITable():New( "I10", "I10" ) 

	//-------------------------------------------------------------------
	// Campos da tabela. 
	//-------------------------------------------------------------------  
	oQuery:AddField( TBIField():New( "I10_ENTITY", "C", 30, 0)) // Nome da entidade
	oQuery:AddField( TBIField():New( "I10_ORIGIN", "C", 03, 0)) // Tabela de Origem do Protheus 
	oQuery:AddField( TBIField():New( "I10_ISTCIA", "C", 02, 0)) // Inst�ncia
	oQuery:AddField( TBIField():New( "I10_EMPRES", "C", 12, 0)) // Empresa
	oQuery:AddField( TBIField():New( "I10_VIEW"  , "C", 50, 0)) // View
	oQuery:AddField( TBIField():New( "I10_QUERY" , "M", 10, 0)) // Query
	oQuery:AddField( TBIField():New( "I10_CRYPT" , "C", 30, 0)) // Tipo de Criptografia
	oQuery:AddField( TBIField():New( "I10_USEVW" , "C", 1, 0)) // Define se a query tem depend�ncia com outras views
	
	
	//-------------------------------------------------------------------
	// �ndices da tabela. 
	//-------------------------------------------------------------------  
	oQuery:addIndex( TBIIndex():New( "I101", {"I10_ENTITY", "I10_EMPRES", "I10_ISTCIA"}, .T.) )

	aAdd( aTable, oQuery )

	//-------------------------------------------------------------------
	// Abre a tabela. 
	//------------------------------------------------------------------- 
	For nTable := 1 To Len( aTable )
		If !( aTable[nTable]:lIsOpen() )
			aTable[nTable]:ChkStruct(.T.)
			aTable[nTable]:lOpen()
		EndIf
	Next nTable

	aSize(aTable, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCAppTable
Define a estrutura da tabela de Apps e realiza a cria��o.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCAppTable()
	Local oQuery := Nil
	Local aTable := {}
	Local nTable := 1

	//-------------------------------------------------------------------
	// Cria a tabela de apps. 
	//-------------------------------------------------------------------  
	oQuery := TBITable():New( "I11", "I11" ) 

	oQuery:AddField( TBIField():New( "I11_APP", "C", 30, 0)) // Nome do App
	oQuery:addIndex( TBIIndex():New( "I111", {"I11_APP"}, .T.) )

	aAdd( aTable, oQuery )

	//-------------------------------------------------------------------
	// Abre a tabela.
	//------------------------------------------------------------------- 
	For nTable := 1 To Len( aTable )
		If !( aTable[nTable]:lIsOpen() )
			aTable[nTable]:ChkStruct(.T.)
			aTable[nTable]:lOpen()
		EndIf
	Next nTable

	aSize(aTable, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCAppEntTable
Define a estrutura da tabela de Apps X Entidades e realiza a cria��o.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCAppEntTable()
	Local oQuery := Nil
	Local aTable := {}
	Local nTable := 1

	//-------------------------------------------------------------------
	// Cria a tabela de apps x entidades. 
	//-------------------------------------------------------------------  
	oQuery := TBITable():New( "I12", "I12" ) 

	oQuery:AddField( TBIField():New( "I12_APP", "C", 30, 0)) // Nome do App
	oQuery:AddField( TBIField():New( "I12_ENTITY", "C", 30, 0)) // Nome da entidade
	oQuery:addIndex( TBIIndex():New( "I121", {"I12_APP", "I12_ENTITY"}, .T.) )	

	aAdd( aTable, oQuery )

	//-------------------------------------------------------------------
	// Abre a tabela.
	//------------------------------------------------------------------- 
	For nTable := 1 To Len( aTable )
		If !( aTable[nTable]:lIsOpen() )
			aTable[nTable]:ChkStruct(.T.)
			aTable[nTable]:lOpen()
		EndIf
	Next nTable

	aSize(aTable, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCRecordEntity
Salva a query de uma entidade na base de dados ERP (na tabela I10).

@param oEntity, object, objeto do tipo dimens�o ou fato.
@param cCompany, String, Empresa injetada.

@Return lSuccess, True para quando a execu��o do m�todo n�o houve erros, caso contr�rio False.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCRecordEntity( oEntity, cCompany )
	Local cNameView := ""	
	Local cView     := ""
	Local cOrigin   := ""
	Local cShared   := ""
	Local cInstance := BIInstance()
	Local lEntity   := .F.
	Local nStatus	:= 0 
	
	Default oEntity    := Nil 
	Default cCompany   := ""

	cOrigin   := oEntity:GetOrigin( )
	cShared   := BIGetShare ( cOrigin )
	cView     := oEntity:GetView( cCompany )
	nStatus   := oEntity:GetStatus( )

	If nStatus == 0
	
		//-----------------------------------------------------------------------------------------------------------------
		// N�o permitir a grava��o duplicada das entidades onde a tabela de origem seja compartilhada entre as empresas. 
		//-----------------------------------------------------------------------------------------------------------------
		If ( Ascan( __aShared, { |x| x[1] == oEntity:GetEntity( ) .And. x[2] == cShared } )) == 0
			
			//-------------------------------------------------------------------
			// Identifica se � para incluir ou atualizar a entidade.
			//-------------------------------------------------------------------	
			lEntity := I10->( DBSeek( ( Padr( AllTrim( oEntity:GetEntity( ) ), 30 ) ) + ( Padr( AllTrim( cCompany  ), 12 ) ) + cInstance ) )
	
			If( RecLock( "I10", ! lEntity  ) )
				I10->I10_ENTITY := oEntity:GetEntity( )
				I10->I10_ORIGIN := oEntity:GetOrigin( )
				I10->I10_ISTCIA := cInstance
				I10->I10_EMPRES := cCompany
				I10->I10_QUERY  := cView
				I10->I10_CRYPT  := CRYPT_TYPE
				I10->I10_USEVW :=  oEntity:GetUseOtherView( )

				I10->( MsUnlock() )
			EndIf
			
			aAdd(__aShared, { oEntity:GetEntity( ), cShared } )
			Conout( STR0001 + " " + oEntity:GetEntity( ) ) // "BCDataBase: Gravando informa��es da view"			
		EndIf
	Else
		ConOut(I18n( STR0004 , {STR0005, oEntity:GetEntity( ), STR0006} ) ) // "BADataBase #1 #2 #3 ##Entidade ##n�o foi gravada na tabela de processos.
	EndIf

Return nStatus

//-------------------------------------------------------------------
/*/{Protheus.doc} BCSaveApp
Salva o App na base de dados ERP.

@param, cApp, String, Nome do App.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCSaveApp( cApp )
	Local lApp := .F.
	
	Default cApp := ""

	If ! ( Empty( cApp ) )
	
		lApp := I11->( DBSeek( ( Padr( AllTrim( cApp ), 30 ) ) ) )
		If( RecLock( "I11", ! lApp  ) )
			I11->I11_APP := cApp

			I11->( MsUnlock() )
		EndIf
		
	EndIf
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCSaveAppEntity
Salva o App X Entidade na base de dados ERP.

@param, cApp, String, Nome do App.
@param, cEntity, String, Nome da entidade.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCSaveAppEntity( cApp, cEntity )
	Local lAppEntity   := .F.
	
	Default cApp := ""
	Default cEntity := ""	

	If ! ( Empty( cApp ) )
	
		lAppEntity := I12->( DBSeek( ( Padr( AllTrim( cApp ), 30 ) ) + ( Padr( AllTrim( cEntity  ), 30 ) ) ) )

		If( RecLock( "I12", ! lAppEntity  ) )
			I12->I12_APP    := cApp
			I12->I12_ENTITY := cEntity
			I12->( MsUnlock() )
		EndIf
		
	EndIf
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCExistView
Verifica se a entidade j� foi gravada por empresa.

@param, cEntity, String, Nome da entidade.
@param, cCompany, String, C�digo da empresa.

@author  Andreia Lima
@since   23/04/2019
/*/
//-------------------------------------------------------------------
Function BCExistView( cEntity, cCompany )
	Local lExist    := .F.
	Local cInstance := BIInstance()
	
	Default cEntity    := ""
	Default cCompany   := ""	

	lExist := I10->( DBSeek( ( Padr( AllTrim( cEntity ), 30 ) ) + ( Padr( AllTrim( cCompany  ), 12 ) ) + cInstance ) )
	
Return lExist

//-------------------------------------------------------------------
/*/{Protheus.doc} BCTruncate  
Limpa todos os dados da tabela de processos (I10). 

@author  Andreia Lima
@since   23/04/2019
/*/
//------------------------------------------------------------------- 
Function BCTruncate()
	Local cDDL := ""
	Local lOk  := .T.
	Local cDatabase  := Upper( SGDB() )
	
	//-------------------------------------------------------------------
	// For�a a cria��o da tabela I10 antes de efetuar o Truncate
	//-------------------------------------------------------------------
	BCProcessTable()

	//-------------------------------------------------------------------
	// Monta a DDL.
	//-------------------------------------------------------------------
	Do Case
		Case ("DB2" $ cDataBase )
			cDDL := "DELETE FROM I10"
		Case ("POSTGRES" $ cDataBase )
			cDDL := "TRUNCATE I10"
		Otherwise
			cDDL := "TRUNCATE TABLE I10"
	EndCase
	
	//-------------------------------------------------------------------
	// Executa a DDL.
	//-------------------------------------------------------------------
	If ! ( TCSQLExec( cDDL ) == 0 )
		ConOut( "BCTruncate: " + TCSQLError() )
		lOk := .F.
	EndIf
Return lOk

//------------------------------------------------------------------- 
/*/{Protheus.doc} BCGetNameView
Retorna o nome da view a ser criada

@author  Andreia Lima
@since   23/04/2019
/*/ 
//-------------------------------------------------------------------- 
Static Function BCGetNameView( cEntity, cCompany )
	Local cView := ""
	
	cView := "VW_" + Upper(cEntity) + "_" + cCompany

Return cView

//------------------------------------------------------------------- 
/*/{Protheus.doc} BCCreateView
Cria a view

@author  Andreia Lima
@since   23/04/2019
/*/ 
//-------------------------------------------------------------------- 
Function BCCreateView( cView, cNameView )
	Local nStatus   := 0
	Local cDDL      := ""
	Local cDrop     := ""
	Local cDatabase := Upper( SGDB() )
	
	Default cView     := ""
	Default cNameView := ""
	
	Do Case
		Case ("ORACLE" $ cDataBase )
			cDDL := "CREATE OR REPLACE VIEW " + cNameView + " AS " + cView
		Case ("MSSQL" $ cDataBase )
			cDrop := "IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '" + cNameView + "') DROP VIEW " + cNameView
			cDDL :=  "CREATE VIEW " + cNameView + " AS (" + cView + ")"
		Otherwise
			nStatus := MSG_ERR_CREATE_VIEW
	EndCase
	
	If ( nStatus == 0 )
		
		If ! Empty( cDrop )
			If ! ( TCSQLExec( cDrop ) == 0 )
				ConOut( "BCCreateView: " + TCSQLError() )
				nStatus := MSG_ERR_CREATE_VIEW
			EndIf
		EndIf
		
		If ( nStatus == 0 )
			If ! ( TCSQLExec( cDDL ) == 0 )
				ConOut( "BCCreateView: " + TCSQLError() )
				nStatus := MSG_ERR_CREATE_VIEW
			EndIf
		EndIf	
	EndIf	
	
Return nStatus	

//------------------------------------------------------------------- 
/*/{Protheus.doc} BCCountEnt
Conta a quantidade de entidades foram criadas na tabela de controle.

@author  Marcia Junko
@since   16/09/2019
/*/ 
//-------------------------------------------------------------------- 
Function BCCountEnt( cEntity )
	Local nQtde		:= 0
	Local cDDL      := ""
	Local aSaveArea	:= GetArea()
	Local cAuxAlias := GetNextAlias()
	
	Default cEntity   := ""

	cDDL := "SELECT COUNT(*) AS QTDE FROM I10 WHERE I10_ENTITY = '" + cEntity + "' AND D_E_L_E_T_ = ' ' "
	DBUseArea( .T., "TOPCONN", TCGenQry( ,, cDDL ), cAuxAlias , .T., .F. )"

	IF (cAuxAlias)->(! EoF() )
		nQtde := (cAuxAlias)->QTDE
	EndIf
	
	(cAuxAlias)->(DBCloseArea())
	
	If !Empty(aSaveArea)
		RestArea(aSaveArea)
	EnDIf

Return nQtde	

//------------------------------------------------------------------- 
/*/{Protheus.doc} BCRetQuery
Retorna a query descriptografada gravada no banco de dados.

@author  Marcia Junko
@since   16/09/2019
/*/ 
//-------------------------------------------------------------------- 
Function BCRetQuery( nID )
	Local cQuery      := ""
	Local aSaveArea	:= GetArea()
	
	DBSelectArea("I10")
	DbGoto( nID )
	
	cQuery := BICrypt( I10->I10_QUERY, 2)

	If !Empty(aSaveArea)
		RestArea(aSaveArea)
	EnDIf

Return cQuery	


//------------------------------------------------------------------- 
/*/{Protheus.doc} BCRetQuery
Retorna a query descriptografada gravada no banco de dados.

@author  Marcia Junko
@since   16/09/2019
/*/ 
//-------------------------------------------------------------------- 
Function BCUpdVwName( nID, cViewName )
	Local aSaveArea	:= GetArea()
	
	DBSelectArea("I10")
	DbGoto( nID )

	If( RecLock( "I10", .F.  ) )
		I10->I10_VIEW   := cViewName
		
		I10->( MsUnlock() )
	EndIf

	If !Empty(aSaveArea)
		RestArea(aSaveArea)
	EnDIf

Return