#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL BALANCE

//-------------------------------------------------------------------
/*/{Protheus.doc} BCDRE
Visualiza as informa��es do DRE da �rea de Controladoria.

@author  Marcia Junko
@since   17/06/2019

/*/
//-------------------------------------------------------------------
Class BCBalance from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Marcia Junko
@since   17/06/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCBalance
	_Super:Setup("Balance", "CQ0")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Marcia Junko
@since   17/06/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCBalance
	Local cView := ""
	
	cView := "SELECT " + ;
		"<<CODE_LINE>> AS TOTVS_LINHA_PRODUTO, " + ;
		"<<CODE_INSTANCE>> AS INSTANCIA, " + ;
		"<<CODE_COMPANY>> AS EMPRESA, " + ;
		"'<<CQ0_COMPANY>>' AS TABELA_SALDOS, " + ;
		"CQ0_FILIAL AS FILIAL_SALDO, " + ;
		"CQ0_DATA AS DATA, " + ;
		"CQ0_CONTA AS COD_CONTA, " + ;
		"CQ0_MOEDA AS COD_MOEDA, " + ;
		"CQ0_TPSALD AS TIPO_SALDO, " + ;
		"CQ0_LP AS FLAG_LP, " + ;
		"CQ0_DTLP AS DATA_APURACAO, " + ; 
		"'<<CT1_COMPANY>>' AS TABELA_PLANO_CONTAS, " + ;
		"CT1_FILIAL AS FILIAL_PLANO_CONTAS, " + ;
		"CT1_NORMAL AS COND_NORMAL, " + ;
		"CT1_DESC01 AS DESC_CONTA, " + ;
		"CASE CT1_NORMAL WHEN '1' THEN 'D' WHEN '2' THEN 'R' ELSE  '' END AS TIPO, " + ;
		"'<<CTN_COMPANY>>' AS TABELA_LIVROS_CONTABEIS, " + ;
		"CTN.CTN_FILIAL AS FILIAL_LIVROS_CONTABEIS, " + ;
		"CTN.CTN_CODIGO AS COD_LIVRO_CONTABIL, " + ;
		"CTN.CTN_DESC AS DESC_LIVRO, " + ;
		"'<<CTS_COMPANY>>' AS TABELA_VISOES_GERENCIAIS, " + ;
		"CTS_FILIAL AS FILIAL_VISOES_GERENCIAIS, " + ;
		"CTS_CODPLA AS COD_VISAO, " + ;
		"CTS_ORDEM AS ORDEM_EXIBICAO, " + ;
		"CTS_LINHA AS LINHA_COMPOSICAO, " + ;
		"CTS_IDENT AS IDENTIFICADOR, " + ;
		"CTS_CT1INI AS CONTA_INICIAL, " + ;
		"CTS_CT1FIM AS CONTA_FINAL, " + ;
		"CTS_NOME AS NOME_VISAO, " + ;
		"CTS_DESCCG AS DESC_ENT_GERENCIAL, " + ;
		"<<FORMATVALUE(CQ0_CREDIT)>> AS SALDOCRD, " + ; 
		"<<FORMATVALUE(CQ0_DEBITO)>> AS SALDODEB, " + ;
		"CASE CT1_NORMAL " + ;
			"WHEN '1' " + ; 
				"THEN COALESCE(CQ0_DEBITO - CQ0_CREDIT, 0) " + ;
			"WHEN '2' " + ;
				"THEN COALESCE(CQ0_CREDIT - CQ0_DEBITO, 0) " + ; 
			"ELSE 0 " + ;
			"END AS SALDO_PERIODO, " + ;
		"I13.I13_TIPO AS TIPO_CONTA " + ;
	"FROM <<CQ0_COMPANY>> CQ0 " + ;
	"INNER JOIN <<CT1_COMPANY>> CT1 " + ;
		"ON CT1_FILIAL = <<SUBSTR_CT1_CQ0_FILIAL>> " + ;
		"AND CT1_CONTA = CQ0_CONTA " + ;
		"AND CT1_NTSPED in ('01', '02', '03') " + ;
		"AND CT1.D_E_L_E_T_ = ' '  " + ;	
	"INNER JOIN <<CTS_COMPANY>> CTS " + ;
		"ON CTS_FILIAL = <<SUBSTR_CTS_CQ0_FILIAL>> " + ;
		"AND CTS_CLASSE <> '1' " + ;
		"AND CTS_IDENT NOT IN ('5','6','7') " + ;
		"AND CTS.D_E_L_E_T_ = ' ' " + ;
	"INNER JOIN <<CTN_COMPANY>> CTN " + ;
		"ON CTN_FILIAL = <<SUBSTR_CTN_CQ0_FILIAL>> " + ;
		"AND CTN_PLAGER = CTS_CODPLA " + ;
		"AND CTN.D_E_L_E_T_ = ' ' " + ;
	"LEFT JOIN " + ;
		"I13 I13 ON I13.I13_COD_CC = CT1.CT1_CONTA  AND " + ;
		"I13.I13_FILIAL = CT1.CT1_FILIAL AND " + ;
		"I13.I13_EMPRES = <<CODE_COMPANY>> AND " + ;
		"I13.D_E_L_E_T_ = ' ' " + ;
	"WHERE " + ;
		"CQ0_CONTA BETWEEN CTS_CT1INI AND CTS_CT1FIM " + ;
		"AND CQ0_DATA >= <<HISTORIC_PERIOD(2)>> "  + ;
		"AND CQ0.D_E_L_E_T_ = ' ' " + ;
		"<<TEST_QUERY>> "

Return cView	