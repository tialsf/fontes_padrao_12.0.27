#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL CENTROCUSTO

//-------------------------------------------------------------------
/*/{Protheus.doc} BCCentroCusto
Visualiza as informa��es dos centros de custos.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Class BCCentroCusto from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCCentroCusto
	_Super:Setup("CentroCusto", "CQ2", , .F.)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCCentroCusto
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ2_FILIAL AS FILIAL, CQ2_DATA AS DATA, CQ2_CONTA AS CONTA," +;
		            "CQ2_CCUSTO AS CENTRO_CUSTO, CQ2_MOEDA AS MOEDA, CQ2_TPSALD AS TIPO_SALDO," + ;
		            "CQ2_LP AS LUCRO_PERDA, CQ2_STATUS AS STATUS, CQ2_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ2_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ2_CREDIT)>> AS CREDITO," + ;
		            "CQ2_DTLP AS DATA_APURACAO, CONTA_CONTABIL_INICIAL, CONTA_CONTABIL_FINAL " + ; 
              "FROM <<CQ2_COMPANY>> " + ; 
             "INNER JOIN CAROL_CONTACONTABIL " + ; 
                "ON FILIAL = <<SUBSTR_CTS_CQ2_FILIAL>> " +;
               "AND CQ2_FILIAL = FILIAL " + ;
               "AND CQ2_CONTA = CONTA_CONTABIL " + ;
             "WHERE D_E_L_E_T_ = ' ' " 
             
Return cView	