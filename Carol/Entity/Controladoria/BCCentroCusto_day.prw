#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL CCUSTO_DAY

//-------------------------------------------------------------------
/*/{Protheus.doc} BCCentroCustoDay
Visualiza as informa��es dos centros de custos.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Class BCCentroCusto_Day from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCCentroCusto_Day
	_Super:Setup("CentroCusto_Day", "CQ3", , .F.)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   13/08/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCCentroCusto_Day
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ3_FILIAL AS FILIAL, CQ3_DATA AS DATA, CQ3_CONTA AS CONTA," +;
		            "CQ3_CCUSTO AS CENTRO_CUSTO, CQ3_MOEDA AS MOEDA, CQ3_TPSALD AS TIPO_SALDO," + ;
		            "CQ3_LP AS LUCRO_PERDA, CQ3_STATUS AS STATUS, CQ3_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ3_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ3_CREDIT)>> AS CREDITO," + ;
		            "CQ3_DTLP AS DATA_APURACAO, CONTA_CONTABIL_INICIAL, CONTA_CONTABIL_FINAL " + ;  
              "FROM <<CQ3_COMPANY>> " + ; 
             "INNER JOIN CAROL_CONTACONTABIL " + ; 
                "ON FILIAL = <<SUBSTR_CTS_CQ3_FILIAL>> " +;
               "AND CQ3_FILIAL = FILIAL " + ;
               "AND CQ3_CONTA = CONTA_CONTABIL " + ;
             "WHERE D_E_L_E_T_ = ' '  " 
             
Return cView	