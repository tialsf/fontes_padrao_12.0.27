#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL CLASSEVALOR

//-------------------------------------------------------------------
/*/{Protheus.doc} BCClasseValor
Visualiza as informa��es de classe de valor por m�s.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Class BCClasseValor from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCClasseValor
	_Super:Setup("ClasseValor", "CQ6")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCClasseValor
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ6_FILIAL AS FILIAL, CQ6_DATA AS DATA, CQ6_CONTA AS CONTA," +;
		            "CQ6_CCUSTO AS CENTRO_CUSTO, CQ6_ITEM AS ITEM_CONTABIL, CQ6_CLVL AS CLASSE_VALOR, " +;
		            "CQ6_MOEDA AS MOEDA, CQ6_TPSALD AS TIPO_SALDO," + ;
		            "CQ6_LP AS LUCRO_PERDA, CQ6_STATUS AS STATUS, CQ6_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ6_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ6_CREDIT)>> AS CREDITO, CQ6_DTLP AS DATA_APURACAO " + ;
		      "FROM <<CQ6_COMPANY>> " + ; 
             "WHERE D_E_L_E_T_ = ' '  " + ;
             	"<<TEST_QUERY>> " 
             
Return cView	