#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL CLVALOR_DAY

//-------------------------------------------------------------------
/*/{Protheus.doc} BCClasseValor_Day
Visualiza as informa��es das classes de valor por dia.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Class BCClasseValor_Day from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCClasseValor_Day
	_Super:Setup("ClasseValor_Day", "CQ7")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCClasseValor_Day
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ7_FILIAL AS FILIAL, CQ7_DATA AS DATA, CQ7_CONTA AS CONTA," +;
		            "CQ7_CCUSTO AS CENTRO_CUSTO, CQ7_ITEM AS ITEM_CONTABIL, CQ7_CLVL AS CLASSE_VALOR, " +;
		            "CQ7_MOEDA AS MOEDA, CQ7_TPSALD AS TIPO_SALDO," + ;
		            "CQ7_LP AS LUCRO_PERDA, CQ7_STATUS AS STATUS, CQ7_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ7_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ7_CREDIT)>> AS CREDITO, CQ7_DTLP AS DATA_APURACAO " + ;
              "FROM <<CQ7_COMPANY>> " + ;
             "WHERE D_E_L_E_T_ = ' '  " + ;
             	"<<TEST_QUERY>> "
             
Return cView	