#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL ITEMCONTABIL

//-------------------------------------------------------------------
/*/{Protheus.doc} BCItemContabil
Visualiza as informa��es dos itens contabeis por m�s.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Class BCItemContabil from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCItemContabil
	_Super:Setup("ItemContabil", "CQ4")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCItemContabil
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ4_FILIAL AS FILIAL, CQ4_DATA AS DATA, CQ4_CONTA AS CONTA," +;
		            "CQ4_CCUSTO AS CENTRO_CUSTO, CQ4_ITEM AS ITEM_CONTABIL, " +;
		            "CQ4_MOEDA AS MOEDA, CQ4_TPSALD AS TIPO_SALDO," + ;
		            "CQ4_LP AS LUCRO_PERDA, CQ4_STATUS AS STATUS, CQ4_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ4_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ4_CREDIT)>> AS CREDITO, CQ4_DTLP AS DATA_APURACAO " + ;
              "FROM <<CQ4_COMPANY>> " + ;
             "WHERE D_E_L_E_T_ = ' '  " + ;
             	"<<TEST_QUERY>> " 
             
Return cView	