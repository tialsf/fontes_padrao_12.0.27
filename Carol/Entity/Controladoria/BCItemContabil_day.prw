#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL ITEMC_DAY

//-------------------------------------------------------------------
/*/{Protheus.doc} BCItemContabilDay
Visualiza as informa��es dos itens contabeis por dia.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Class BCItemContabil_Day from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCItemContabil_Day
	_Super:Setup("ItemContabil_Day", "CQ5")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Andr�ia Lima
@since   10/09/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCItemContabil_Day
	Local cView := ""
	
	cView := "SELECT <<CODE_LINE>> AS TOTVS_LINHA_PRODUTO," + ;
			        "<<CODE_INSTANCE>> AS INSTANCIA," + ;
		            "<<CODE_COMPANY>> AS EMPRESA," + ;
		            "CQ5_FILIAL AS FILIAL, CQ5_DATA AS DATA, CQ5_CONTA AS CONTA," +;
		            "CQ5_CCUSTO AS CENTRO_CUSTO, CQ5_ITEM AS ITEM_CONTABIL, " +;
		            "CQ5_MOEDA AS MOEDA, CQ5_TPSALD AS TIPO_SALDO," + ;
		            "CQ5_LP AS LUCRO_PERDA, CQ5_STATUS AS STATUS, CQ5_SLBASE AS SALDO_BASICO," +;
		            "<<FORMATVALUE(CQ5_DEBITO)>> AS DEBITO, <<FORMATVALUE(CQ5_CREDIT)>> AS CREDITO, CQ5_DTLP AS DATA_APURACAO " + ;
              "FROM <<CQ5_COMPANY>> " + ;
             "WHERE D_E_L_E_T_ = ' '  " + ;
             	"<<TEST_QUERY>> "
             
Return cView	