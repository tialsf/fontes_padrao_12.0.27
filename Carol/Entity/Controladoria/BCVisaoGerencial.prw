#INCLUDE "BCDEFINITION.CH"

NEW DATAMODEL VISGER

//-------------------------------------------------------------------
/*/{Protheus.doc} BCContaContabil
Visualiza as informa��es das vis�es gerenciais

@author  Marcia Junko
@since   07/10/2019

/*/
//-------------------------------------------------------------------
Class BCVisaoGerencial from BCEntity
	Method Setup( ) CONSTRUCTOR
	Method BuildView( )
EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Setup
Construtor padr�o.

@author  Marcia Junko
@since   07/10/2019
/*/
//-------------------------------------------------------------------
Method Setup( ) Class BCVisaoGerencial
	_Super:Setup("VisaoGerencial", "CTS")
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} BuildQuery
Constr�i a query da entidade.

@return aQuery, array, Retorna as consultas da entidade por empresa.

@author  Marcia Junko
@since   07/10/2019

/*/
//-------------------------------------------------------------------
Method BuildView( ) Class BCVisaoGerencial
	Local cView := ""
	
	cView := "SELECT " + ;
		"<<CODE_LINE>> AS TOTVS_LINHA_PRODUTO, " + ;
		"<<CODE_INSTANCE>> AS INSTANCIA, " + ;
		"<<CODE_COMPANY>> AS EMPRESA, " + ;
		"'<<CTS_COMPANY>>' AS TABELA, " + ;
		"CTS_FILIAL AS FILIAL, " + ;
		"CTS_CODPLA AS COD_VISAO, " + ;
		"CTS_NOME AS NOME_VISAO_GERENCIAL, " + ;
		"CTS_ORDEM AS ORDEM_EXIBICAO, " + ;
		"CTS_CONTAG AS ENT_GERENCIAL, " + ;
		"CTS_CTASUP AS ENT_SUPERIOR, " + ;
		"CTS_DESCCG AS DESC_ENT_GERENCIAL, " + ;
		"CTS_NORMAL AS COND_NORMAL, " + ;
		"CTS_COLUNA AS COLUNA, " + ;
		"CTS_CLASSE AS CLASSE, " + ;
		"CTS_IDENT AS IDENTIFICADOR, " + ;
		"CTS_LINHA AS LINHA_COMPOSICAO, " + ;
		"CTS_CT1INI AS CONTA_CONTABIL_INICIAL, " + ;
		"CTS_CT1FIM AS CONTA_CONTABIL_FINAL, " + ;
		"CTS_CTTINI AS CCUSTO_INICIAL, " + ;
		"CTS_CTTFIM AS CCUSTO_FINAL, " + ;
		"CTS_CTDINI AS ITEM_INICIAL, " + ;
		"CTS_CTDFIM AS ITEM_FINAL, " + ;
		"CTS_CTHINI AS CLASSE_INICIAL, " + ;
		"CTS_CTHFIM AS CLASSE_FINAL, " + ;
		"CTS_TPSALD AS TIPO_SALDO " + ;
		"FROM <<CTS_COMPANY>> " + ;
		"WHERE D_E_L_E_T_ = ' ' " + ;	
		"<<TEST_QUERY>> "

Return cView	