#INCLUDE 'totvs.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} BCWizard
Função principal, que tem por obtivo abrir Wizard para configurar as
contas contabéis.
@param aEmpresas, array, array contendo as empresas selecionadas.

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
Main Function BCWizard(aEmpresas)

    aWizard := BCWizardStr(aEmpresas)

    aWizard := BCWizardSc(aWizard)

    BCWizardTable()
    
    BCWizardRec(aWizard)

    
Return nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCWizardTable
Cria a estrutura da tabela I13, que será a responsavel por guardar as informações
provenientes do wizard.

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
Function BCWizardTable()
	Local oQuery := Nil
	Local aTable := {}
	Local nTable := 1

	// Cria a tabela de processos. 
	//-------------------------------------------------------------------  
	oQuery := TBITable():New( "I13", "I13" ) 

	//-------------------------------------------------------------------
	// Campos da tabela. 
	//-------------------------------------------------------------------  
	oQuery:AddField( TBIField():New( "I13_ORIGIN", "C", 03, 0)) // Tabela de Origem do Protheus 
	oQuery:AddField( TBIField():New( "I13_ISTCIA", "C", 02, 0)) // Inst�ncia
	oQuery:AddField( TBIField():New( "I13_EMPRES", "C", 12, 0)) // Empresa
	oQuery:AddField( TBIField():New( "I13_FILIAL", "C", 12, 0)) // Filial
	oQuery:AddField( TBIField():New( "I13_COD_CC", "C", 50, 0)) // Código Conta Contabil
	oQuery:AddField( TBIField():New( "I13_CC_SUP", "C", 50, 0)) // Código Conta Superior
	oQuery:AddField( TBIField():New( "I13_DES_CC", "C", 50, 0)) // Descrição Conta Contabil
	oQuery:AddField( TBIField():New( "I13_TIPO"  , "C", 01, 0)) // Descrição Conta Contabil
	//-------------------------------------------------------------------
	// �ndices da tabela. 
	//-------------------------------------------------------------------  
	oQuery:addIndex( TBIIndex():New( "I131", {"I13_EMPRES", "I13_FILIAL", "I13_ISTCIA"}, .T.) )

	aAdd( aTable, oQuery )

    TCDelFile( "I13" )
	//-------------------------------------------------------------------
	// Abre a tabela. 
	//------------------------------------------------------------------- 
	For nTable := 1 To Len( aTable )
		If !( aTable[nTable]:lIsOpen() )
			aTable[nTable]:ChkStruct(.T.)
			aTable[nTable]:lOpen()
		EndIf
	Next nTable

	aSize(aTable, 0)

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} BCWizardStr
Função responsabel pela busca e criação das estrutura das contas 
contabéis das empresas selecionadas.

@param aEmpresas, array, array contendo as empresas selecionadas.
@return aPlano, array, array contendo o plano de contas com sua 
estrutura das empresas selecionadas.

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
static function BCWizardStr(aCompany)
    local nCompany
    local aPlano := {}
    Local cFilAux := ""

    // Executando o processo para cada empresa. 
	//-------------------------------------------------------------------  
    For nCompany := 1 To Len( aCompany )
        cFilAux := 'ZZZZZZZZZ'
        nFil := 1
        AADD(aPlano, {aCompany[nCompany][2]})
 	//-------------------------------------------------------------------
	// Prepara o ambiente para execução da Query. 
	//------------------------------------------------------------------- 
        RPCSetType( 3 )
		RPCSetEnv( aCompany[nCompany][2] )
 	//-------------------------------------------------------------------
	// Carrega a tabela temporária. 
	//------------------------------------------------------------------- 
        cflow := flow()
 	//-------------------------------------------------------------------
	// Percorre a tabela temporaria para a criação do array
	//-------------------------------------------------------------------        
        While ! (cFlow)->( Eof() )

            IF ! ( Upper(Alltrim((cflow)->FILIAL)) == Upper(Alltrim(cFilAux)) )
                aadd(aPlano[nCompany], {(cflow)->FILIAL} )
                nFil++
            ENDIF
            cFilAux :=  Upper(Alltrim((cflow)->FILIAL))

            AADD(aPlano[nCompany][nFil], {(cflow)->COD_CONTA_CONTABIL, (cflow)->CONTA_SUPERIOR, getStatus((cflow)->COD_CONTA_CONTABIL, 1,1), (cflow)->DESC_CONTA })

            (cFlow)->( DBSkip() )
        ENDDO
        RpcClearENv()
    NEXT
    RPCSetEnv(aCompany[1][2])

return aPlano

//-------------------------------------------------------------------
/*/{Protheus.doc} flow
Criação de uma tabela temporaria com as contas contabeis da empresa
posicionada.
@return cFlow, string, Nome da tabela temporaria gerada.

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
static function flow()
	Local cDML 		:= ""
    Local cFlow := NIL

	//-------------------------------------------------------------------
	// Monta o DML. 
	//------------------------------------------------------------------- 
	cDML += " SELECT CT1_FILIAL AS FILIAL, CT1_CONTA AS COD_CONTA_CONTABIL, CT1_CTASUP AS CONTA_SUPERIOR, CT1_DESC01 AS DESC_CONTA" +;
            "FROM" + RetSQLName( "CT1" ) + " CT1  WHERE CT1.D_E_L_E_T_ = ' ' order by FILIAL, COD_CONTA_CONTABIL";
	//-------------------------------------------------------------------
	// Transforma o DML em ANSI. 
	//-------------------------------------------------------------------  	
	cDML := ChangeQuery( cDML )  

	//-------------------------------------------------------------------
	// Executa o DML. 
	//-------------------------------------------------------------------  	
	DBUseArea( .T., "TOPCONN", TCGenQry( ,,cDML ), cFlow := GetNextAlias() , .T., .F. )
Return cFlow 

//-------------------------------------------------------------------
/*/{Protheus.doc} getStatus
Função responsavel por classificar a conta.
@param cAccount, string, nome da conta a ser classificada.
@return cClass, string, nome da classe que a conta foi classificada.

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
static function getStatus(cAccount)
    cClass := 'N'

    cAccount := SUBSTR(alltrim(cAccount),1,1)

    DO CASE
        CASE cAccount == '1'
            cClass := "A"
        CASE cAccount == '2'
            cClass := "P"
        CASE cAccount == '3'
            cClass := "C"
        CASE cAccount == '4'
            cClass := "D"
        CASE cAccount == '5'
            cClass := "R"
    ENDCASE

return cClass
//-------------------------------------------------------------------
/*/{Protheus.doc} BCWizardRec
Função responsavel por gravar as informações no banco.
@param aWizard, array, Array com a estrutura a ser gravada no banco

@author Rodrigo Soares  
@since 10/03/2020
/*/
//-------------------------------------------------------------------
static function BCWizardRec(aWizard)
    Local cInstance := BIInstance()
    Local nEmp
    Local nFil
    Local nCC

    DBSelectArea('I13')

    FOR nEmp := 1 to len(aWizard ) // Executa por empresa
        FOR nFil := 2 to len(aWizard[nEmp]) // Executa por filial
            FOR nCC := 2 to len(aWizard[nEmp][nFil]) // Executa por Conta;
                If( RecLock( "I13", .t. ) )
                    I13->I13_ORIGIN		:= 'CT1'
                    I13->I13_ISTCIA	    := cInstance
                    I13->I13_EMPRES		:= aWizard[nEmp][1]
                    I13->I13_FILIAL	    := aWizard[nEmp][nFil][1]
                    I13->I13_COD_CC	    := aWizard[nEmp][nFil][nCC][1]
                    I13->I13_CC_SUP	    := aWizard[nEmp][nFil][nCC][2]
                    I13->I13_DES_CC	    := aWizard[nEmp][nFil][nCC][4]
                    I13->I13_TIPO	    := aWizard[nEmp][nFil][nCC][3]

                    I13->( MsUnlock() )
                ENDIF
            NEXT
        NEXT
    NEXT

RETURN NIL



