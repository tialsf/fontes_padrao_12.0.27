#INCLUDE 'TOTVS.CH'
#INCLUDE 'FWMVCDEF.CH'

#DEFINE TOTVS "TOTVS"
#DEFINE CAMPO   01
#DEFINE TIPO    02
#DEFINE TAMANHO 03
#DEFINE DECIMAL 04

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Defini��o das op��es do menu

@author vinicius.nicolau
@since 12/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function MenuDef()

    Local cRotCmpAns 	:= 'PLSSIBESP( Val( (oTmpTab:getAlias())->B3F_CHVORI ) )'
	Local cRotDifCnx 	:= 'PLSB3KB3W( Val( (oTmpTab:getAlias())->B3F_CHVORI ), .F.)'

	Private aRotina 	:= {}

	ADD OPTION aRotina Title 'Atualizar'	            Action 'CenAtuImp()'								OPERATION 3 ACCESS 0
	If B3F->(FieldPos("B3F_OK")) > 0
	    ADD OPTION aRotina Title 'Agrupar Criticas' 	Action 'CenMrkAgr()'	                			OPERATION 2 ACCESS 0
    EndIf
	ADD OPTION aRotina Title 'Exibir dif. CNX' 			Action  cRotDifCnx									OPERATION 2 ACCESS 0
    ADD OPTION aRotina Title 'Importar CNX'	            Action 'PLSVALIDSIB()'								OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Validar CNX' 			    Action  cRotCmpAns                      			OPERATION 3 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar Benefici�rio' 	Action  'CenExiCad(.F.,oTmpTab)'					OPERATION 2 ACCESS 0

Return aRotina

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Central Critica Espelho

Chamada das rotinas ao abrir a tela de Critica de espelho

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function CnCritEsp(lAuto)
	Local cAliasCon	:= GetNextAlias()
	Local cAliasTmp	:= GetNextAlias()
	Local aRetFun		:= {.F.,"Nehum registro criticado encontrado para ser corrigido.",""}
	Private oMark	:= Nil
	Private cGrpInt	:= ""
	Private cRegInt	:= ""
	Default lAuto	:= .F.


	MsgRun("Definindo consulta de registros",TOTVS,{ || aRetFun := RetornaConsulta() })

	If aRetFun[1]
		MsgRun("Consultando registros criticados",TOTVS,{ || aRetFun := ExecutaConsulta(aRetFun[3],cAliasCon) })

		MsgRun("Definindo campos da tabela",TOTVS,{ || aRetFun := RetornaCampos() })

		aCampos := aRetFun[3]
		MsgRun("Criando tabela de trabalho",TOTVS,{ || aRetFun := CriaTabTemp(cAliasTmp,aCampos) })

		oTmpTab := aRetFun[3]
		MsgRun("Carregando tabela",TOTVS,{ || aRetFun := CarregaArqTmp(cAliasCon,cAliasTmp) })
		
		MsgRun("Montando visualiza��o dos registros",TOTVS,{ || aRetFun := CriaMarkBrowse(oMark,cAliasTmp,oTmpTab,aCampos,lAuto) })
	EndIf

	If !aRetFun[1]
		Help(,,'Aviso',,aRetFun[2],1,0)
	EndIf

Return

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Cria Mark Browse

Fun��o criada para criar o Mark Browse, filtros e fun��es.

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function CriaMarkBrowse(oMark,cAliasTmp,oTmpTab,aCampos,lAuto)
	
	Local aRetFun := {.T.,"N�o foi poss�vel criar o browse para marcar os registros",""}
	Default cAliasTmp := ""
	Default oMark := Nil
	Default oTmpTab := Nil
	Default aCampos := {}
	Default aSeek := {}
	Default aFieFilter := {}
	Default lAuto := .F.

	If cAliasTmp <> "" .And. oTmpTab <> Nil .And. Len(aCampos) > 0

		oMark	:= FWMarkBrowse():New()
		oMark:SetDescription( "Cr�ticas de Espelho")
		oMark:SetAlias(cAliasTmp)
		oMark:SetFieldMark("B3F_MARK")
		oMark:oBrowse:SetDBFFilter(.T.)
		oMark:oBrowse:SetUseFilter(.T.)
		oMark:oBrowse:SetFixedBrowse(.T.)
		oMark:SetWalkThru(.F.)
		oMark:SetAmbiente(.T.)
		oMark:SetTemporary()
		oMark:oBrowse:SetSeek(.T.,RetornaSeek())
		oMark:oBrowse:SetFieldFilter(RetornaFilter())
		oMark:SetAllMark({ || MarcaBrw(oMark,cAliasTmp) })
		oMark:SetMenuDef('CENCRITESP')
		oMark:ForceQuitButton()
		oMark:SetProfileID('CENCRITESP')
		oMark:AddButton("Enviar Alt. ANS"	, { || AjustaCritica(oMark,cAliasTmp)},,,, .F., 3 )

		oMark:SetFields(CarregaCampos(aCampos))

		If !lAuto
			oMark:Activate()
		EndIf
	Else
		aRetFun[1] := .F.
	EndIf

Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PLSVALIDSIB

Fun��o criada para intermediar a valida��o do CNX ap�s a importa��o.

@param cRotCmpAns - Chamada da fun��o de valida��o

@author Vin�cius Nicolau
@since 18/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function PLSVALIDSIB()
	If PLSSIBCNX("2") > 0 
		If IsBlind() .OR. MsgYesNo("Deseja validar o CNX ?", "TOTVS")
			PLSSIBESP()
		EndIf
	EndIf
Return

Function CenAtuImp()
	//Deletar o conteudo da tabela temp
	Local cAliasTmp := oMark:Alias()
	Local cAliasCon := GetNextAlias()
	Local aRetFun	:= {}
	
	(cAliasTmp)->(dbGoTop())
	
	While !(cAliasTmp)->(Eof())
		Reclock(cAliasTmp,.F.)
			(cAliasTmp)->(DbDelete()) 
		MsUnlock()
		(cAliasTmp)->(dbSkip())
	EndDo

	//Refazer a consulta na tabela real
	aRetFun := RetornaConsulta()
	ExecutaConsulta(aRetFun[3],cAliasCon)

	//Realimentar a tabela temp
	CarregaArqTmp(cAliasCon,cAliasTmp)

	//Pedir para dar refresh na tela
	oMark:oBrowse:Refresh(.T.)
Return

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CenExiCad

Funcao criada para exibir as telas de cadastros de beneficiario.

@author vinicius.nicolau
@since 05/06/2020
/*/
//--------------------------------------------------------------------------------------------------
Function CenExiCad(lAuto,oTmpTab)

	Local lOk := .F.
	Local cAliasOri := oTmpTab:getAlias()
	Local nRecno := AllTrim((cAliasOri)->B3FREC)
	Local cCodope := (cAliasOri)->B3F_CODOPE
	Local cMatric := AllTrim((cAliasOri)->B3F_IDEORI)
	Local cRotina := ""
	Local aArea := {}

	Default lAuto := .F.

	If Empty(cAliasOri)
		If !lAuto
			Alert("Selecione uma cr�tica para visualizar o cadastro de origem")			
		EndIf
	Else

		aArea := (cAliasOri)->(GetArea())
		(cAliasOri)->(DbGoTo(Val(nRecno)))
		
		If cAliasOri == oTmpTab:getAlias()
			B3K->(MsSeek(xFilial("B3K")+cCodope+PADR(cMatric,tamSX3("B3K_MATRIC")[1])))
			cRotina := 'PLSMVCBENE'
		EndIf

		If Empty(cRotina)
			If !lAuto
				Alert("N�o existe rotina para visualizar o cadastro da tabela " + cAliasOri)
			EndIf
		Else
			If !lAuto			
				FWExecView('Visualiza��o',cRotina,MODEL_OPERATION_VIEW)
			EndIf
			lOk := .T.
		EndIf
		
		(cAliasOri)->(RestArea(aArea))

	EndIf

Return lOk

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Retorna um array de campos

Retorna campos de filtro por chave

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function RetornaSeek()
	
	Local aSeek := {}

	aAdd(aSeek,{"C�digo da Cr�tica"		,{{"","C",004,0,"C�digo da Cr�tica"		,"@!"}} } )
	aAdd(aSeek,{"C�digo da Operadora"	,{{"","C",006,0,"C�digo da Operadora"	,"@!"}} } )
	aAdd(aSeek,{"Chave de Origem"		,{{"","C",050,0,"Chave de Origem"		,"@!"}} } )

Return aSeek

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Retorna um array de campos

Retorna campos que formam a tela de Criar novos filtros

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function RetornaFilter()

	Local aFieFilter := {}

	Aadd(aFieFilter,{"B3F_CODOPE","Registro da operadora","C",006, 0,"@!"})
	Aadd(aFieFilter,{"B3F_CODCRI","Codigo da Critica"	 ,"C",004, 0,"@!"})
	Aadd(aFieFilter,{"B3F_DESCRI","Descricao da Critica" ,"C",100, 0,"@!"})
	Aadd(aFieFilter,{"B3F_CAMPOS","Campos Afetados"      ,"C",100, 0,"@!"})
	Aadd(aFieFilter,{"B3F_IDEORI","Chave Ident Origem"   ,"C",050, 0,"@!"})
	Aadd(aFieFilter,{"B3F_DESORI","Descri��o na Origem"  ,"C",050, 0,"@!"})

Return aFieFilter

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} RetornaCampos

Retorna os campos para criar a tabela temporaria 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function RetornaCampos()

	Local aCampos := {}
	Local aRetFun := {.F.,"N�o foi poss�vel definir os campos da tabela",""}
	
	aAdd(aCampos,{"B3F_MARK"   ,"C",002,0})
	aAdd(aCampos,{"B3F_CODOPE" ,"C",006,0})
	aAdd(aCampos,{"B3F_IDEORI" ,"C",050,0})
	aAdd(aCampos,{"B3F_CODCRI" ,"C",004,0})
	aAdd(aCampos,{"B3F_DESCRI" ,"C",100,0})
	aAdd(aCampos,{"B3F_CAMPOS" ,"C",100,0})
	aAdd(aCampos,{"B3F_DESORI" ,"C",050,0})
	aAdd(aCampos,{"B3FREC"	   ,"C",016,0})
	aAdd(aCampos,{"B3F_CHVORI" ,"C",016,0})

	If Len(aCampos) > 0
		aRetFun[1] := .T.
		aRetFun[3] := aCampos
	EndIf
	
Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CriaTabTemp

Cria a tabela tempor�ria que ser� utilizada no MarkBrowse

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function CriaTabTemp(cAliasTmp,aCampos)
	Local aRetFun := {.F.,"N�o foi poss�vel definir uma tabela de trabalho",""}
	Local oTmpTab := Nil
	Default aCampos := {}

	oTmpTab := FWTemporaryTable():New( cAliasTmp )
	oTmpTab:SetFields( aCampos )
	oTmpTab:AddIndex("01",{"B3F_CODCRI"})
	oTmpTab:AddIndex("02",{"B3F_CODOPE"})
	oTmpTab:AddIndex("03",{"B3F_IDEORI"})
	oTmpTab:Create()

	aRetFun[1] := .T.
	aRetFun[3] := oTmpTab

Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} RetornaConsulta

Fun��o respons�vel por montar a query que ir� buscar os registros

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function RetornaConsulta()

	Local cConsulta	:= ""
	Local aRetFun := {.F.,"N�o foi poss�vel definir a consulta",""}

	cConsulta := " SELECT "
	cConsulta += " B3F_CODOPE,B3F_IDEORI,B3F_CODCRI,B3F_DESCRI,B3F_CAMPOS,B3F_DESORI,B3F.R_E_C_N_O_ B3FREC,B3F_CHVORI "
	cConsulta += " FROM " 
	cConsulta += "   " + RetSqlName("B3F") + " B3F "
	cConsulta += " WHERE "
	cConsulta += " B3F_FILIAL = '"+xFilial("B3F")+"' "
	cConsulta += " AND B3F_ORICRI IN ('B3W','B3K') "
	cConsulta += " AND B3F_CODCRI LIKE 'E%' "
	cConsulta += " AND B3F.D_E_L_E_T_ = ''  "

	If !Empty(cConsulta)
		aRetFun[1] := .T.
		aRetFun[3] := cConsulta
	EndIf

Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ExecutaConsulta

Executa a query da fun��o RetornaConsulta()

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------

Static Function ExecutaConsulta(cConsulta,cAliasTmp)

	Local aRetFun := {.F.,"Nenhum registro encontrado para ser apresentado",""}
	Default cConsulta := ""
	Default cAliasTmp := ""

	If !Empty(cConsulta) .And. !Empty(cAliasTmp)

		If (Select(cAliasTmp) <> 0)
			dbSelectArea(cAliasTmp)
			(cAliasTmp)->(dbCloseArea())
		EndIf

		cConsulta := ChangeQuery(cConsulta)
		dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cConsulta),cAliasTmp,.F.,.T.)

		If !(cAliasTmp)->(Eof())
			aRetFun[1] := .T.
		EndIf

	EndIf

Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CarregaArqTmp

Preenche o arquivo temporario com os registros criticados

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function CarregaArqTmp(cAliasCon,cAliasTmp)

	Local aRetFun := {.F.,"N�o foi poss�vel carregar o arquivo de trabalho",""}
	
	While !(cAliasCon)->(Eof())

		(cAliasTmp)->(RecLock(cAliasTmp,.T.))
		(cAliasTmp)->B3F_MARK 		:= ""
		(cAliasTmp)->B3F_CODOPE		:= (cAliasCon)->B3F_CODOPE
		(cAliasTmp)->B3F_IDEORI  	:= (cAliasCon)->B3F_IDEORI
		(cAliasTmp)->B3F_CODCRI 	:= (cAliasCon)->B3F_CODCRI
		(cAliasTmp)->B3F_DESCRI 	:= (cAliasCon)->B3F_DESCRI
		(cAliasTmp)->B3F_CAMPOS 	:= (cAliasCon)->B3F_CAMPOS
		(cAliasTmp)->B3F_DESORI  	:= (cAliasCon)->B3F_DESORI 
		(cAliasTmp)->B3FREC			:= AllTrim(Str((cAliasCon)->B3FREC))
		(cAliasTmp)->B3F_CHVORI		:= AllTrim(Str((cAliasCon)->B3F_CHVORI))
		(cAliasTmp)->(MsUnlock())
		(cAliasCon)->(DbSkip())

		If !aRetFun[1]
			aRetFun[1] := .T.
		EndIf

	EndDo

	(cAliasCon)->(dbCloseArea())

Return aRetFun

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} CarregaCampos

Carrega os campos do browse

@author	vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function CarregaCampos(aCampos)

	Local cPicture := "@!"
	Local aFields  := {}
	Local nI := 0

	For nI := 2 to 7
		aAdd(aFields,GetColuna(aCampos[nI,CAMPO] ,X3Desc(aCampos[nI,CAMPO] ),aCampos[nI,TIPO],aCampos[nI,TAMANHO],aCampos[nI,DECIMAL],cPicture))
	Next nI
	
Return aFields

Static Function X3Desc(cCampo)
	Local cDesc := "" 
	SX3->( dbSetOrder(2) )
	If SX3->( dbSeek( cCampo ) )
		cDesc := X3Descric()
	EndIf
Return cDesc

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} GetColuna

Retorna uma coluna para o markbrowse 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function GetColuna(cCampo,cTitulo,cTipo,cPicture,nAlign,nSize,nDecimal)

	Local aColuna  := {}
	Local bData    := &("{||" + cCampo +"}") 
	Default nAlign   := 1
	Default nSize    := 20
	Default nDecimal := 0
	Default cTipo    := "C"
	
	aColuna := {cTitulo,bData,cTipo,cPicture,nAlign,nSize,nDecimal,.F.,{||.T.},.F.,{||.T.},NIL,{||.T.},.F.,.F.,{}}

Return aColuna

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MarcaBrw

Chama a funcao para marcar/desmarcar todos os registros da markbrowse 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function MarcaBrw(oMark,cAliasTmp)
	Default oMark := Nil
	Default cAliasTmp := ""

	MsgRun("Marcando / Desmarcando registros do browse",TOTVS,{ || PrcMarcaBrw(oMark,cAliasTmp) })

Return

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PrcMarcaBrw

Marcar/desmarcar todos os registros da markbrowse 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Static Function PrcMarcaBrw(oMark,cAliasTmp)
	Default oMark := Nil
	Default cAliasTmp := ""

	If !Empty(cAliasTmp) .And. oMark <> Nil
		(cAliasTmp)->(dbGoTop())
		While !(cAliasTmp)->(Eof())

			oMark:MarkRec()
			(cAliasTmp)->(dbSkip())
		EndDo
		oMark:oBrowse:Refresh(.T.)
	EndIf

Return .T.

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AjustaCritica

Envia a mensagem para a tela de corre��o ou n�o 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function AjustaCritica(oMark,cAliasTmp)

	If MsgYesNo("Deseja enviar as altera��es para ANS?")
		MsgRun("Enviando registros criticados",TOTVS,{ || ProcEnvCritica(oMark,cAliasTmp) } )
	Else
		MsgInfo("A��o cancelada pelo usu�rio.")
	EndIf

Return .T.

//--------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AjustaCritica

Chama o processamento de corrigir as criticas do registros selecionados 

@author vinicius.nicolau
@since 25/05/2020
/*/
//--------------------------------------------------------------------------------------------------
Function ProcEnvCritica(oMark,cAliasTmp)
	Local cMarca := ""
	Default oMark := Nil
	
	If !Empty(cAliasTmp) .And. oMark <> Nil

		(cAliasTmp)->(dbGoTop())
		cMarca := oMark:Mark()
		
		While !(cAliasTmp)->(Eof())
			If (cAliasTmp)->B3F_MARK == cMarca
				MV_PAR01 := dDataBase
				cRec := (cAliasTmp)->B3FREC
				If PLSALTANS(.F., .T., cRec) == "Movimenta��o criada e cr�tica corrigida."
					Reclock(cAliasTmp,.F.)
					(cAliasTmp)->(DbDelete()) 
					MsUnlock()
				EndIf
			EndIf
			(cAliasTmp)->(dbSkip())
		EndDo
		oMark:oBrowse:Refresh(.T.)
		MsgInfo("Movimenta��o criada no compromisso vigente.")
	EndIf

Return .T.
