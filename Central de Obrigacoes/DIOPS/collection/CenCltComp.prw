#include "TOTVS.CH"

Class CenCltComp from CenCollection
	   
    Method New() Constructor
    Method initEntity()
    Method podeImpDiops()
    Method initRelation()    

EndClass

Method New() Class CenCltComp
    _Super:new()
    self:oMapper := CenMprComp():New()
    self:oDao := CenDaoComp():New(self:oMapper:getFields())
return self

Method initEntity() Class CenCltComp
return CenComp():New()

Method initRelation() Class CenCltComp
    Local oRelation := CenRelation():New()
    oRelation:destroy()
return self:listRelations()

Method podeImpDiops() Class CenCltComp
    self:lFound := self:getDao():podeImpDiops()
    self:goTop()
Return self:found()
