#Include "Protheus.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} FUNGENMON
Descricao: 	Fonte com as fun��es Genericas Utilizadas no Projeto 
				Monitoramento TISS

@author Hermiro J�nior
@since 24/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------


//-------------------------------------------------------------------
/*/{Protheus.doc} GetCdMun
Descricao 

@author Hermiro J�nior
@Param: Codigo do Municipio 
@Return: .T. = Valido | .F. = Invalido
/*/
//-------------------------------------------------------------------
Function GetCdMun(cCodMun)

	Local lRet		:= .T.
	Local cQuery 	:= ''
	Local cArea		:= GetNextAlias()

	If !Empty(cCodMun) .AND. Val(cCodMun) > 0
	
		cQuery := " SELECT BID_CODMUN FROM " + RetSqlName("BID") 
		cQuery += " WHERE BID_FILIAL = '" + xFilial("BID") + "' " 
		cQuery += " AND BID_CODMUN like '" + AllTrim(cCodMun) + "%' "
		cQuery += " AND D_E_L_E_T_ = ' '" 	
		cQuery := ChangeQuery(cQuery)
		dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQuery),cArea,.F.,.T.)
		
		lRet := !(cArea)->(Eof())
		(cArea)->(dbCloseArea())
	Else
		lRet	:= .F.
	EndIf

Return lRet
//-------------------------------------------------------------------
/*/{Protheus.doc} ExisTabTiss
Descricao: 	Funcao que verifica se o codigo existe na Tabela de 
				Terminologia da TISS

@author Hermiro J�nior
@Param: Codigo do Municipio 
@Return: .T. = Valido | .F. = Invalido
/*/
//-------------------------------------------------------------------
//-> Validar se o Codigo do Procedimento existe na Tabela de Terminologia da TISS 
Function ExisTabTiss(cInf,cTab)

	Local lRet		:= .F.
	Local cQuery 	:= ''
	Local cAliasQry:= GetNextAlias()

	cQuery	:= "SELECT "
	cQuery	+= "		B2R_CODTAB, B2R_CDTERM, B2R_VIGDE, B2R_VIGATE "
	cQuery	+= " FROM "+RetSqlName('B2R')+" B2R " 
	cQuery 	+= " WHERE  "
	cQuery 	+= "		B2R_CODTAB = '"+cTab+"'	AND	"
	cQuery 	+= "		B2R_CDTERM = '"+cInf+"'	AND	"
	cQuery 	+= "		B2R.D_E_L_E_T_= ''				"

	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQuery),cAliasQry,.F.,.T.)

	If (cAliasQry)->(Eof())
		Return lRet
	Else
		//-> Verifica se o Codigo do Procedimento est� vigente 
		If StoD((cAliasQry)->B2R_VIGDE) <= Date() .And. (Empty((cAliasQry)->B2R_VIGATE) .Or. StoD((cAliasQry)->B2R_VIGATE) >= Date())
			lRet 	:= .T.
		EndIf 
	EndIf

	// Fecha a Area 
	dbSelectArea(cAliasQry)
	(cAliasQry)->(dbCloseArea())

Return lRet



