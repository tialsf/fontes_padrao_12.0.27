#include "TOTVS.CH"

Class CenCltCrit from CenCollection
	   
    Method New() Constructor
    Method initEntity()
    Method lmpCriticas()
    Method insCritGrp(cSelectGrp)
    Method bscCritBKT(oCltBKR)

EndClass

Method New() Class CenCltCrit
    _Super:new()
    self:oMapper := CenMprCrit():New()
    self:oDao := CenDaoCrit():New(self:oMapper:getFields())
return self

Method initEntity() Class CenCltCrit
return CenCrit():New()

Method lmpCriticas() Class CenCltCrit
    self:lFound := self:getDao():lmpCriticas()
return self:lFound

Method insCritGrp(cSelectGrp) Class CenCltCrit
    self:lFound := self:getDao():insCritGrp(cSelectGrp)
return self:lFound

Method bscCritBKT(oCltBKR) Class CenCltCrit
    self:lFound := self:getDao():bscCritBKT(oCltBKR)
    self:goTop()
return self:lFound