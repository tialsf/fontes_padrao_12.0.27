#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritCNPJ
Descricao: 	Critica referente ao Campo de CNPJ ou CPF.
				-> BKR_CPFCNP
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritCNPJ From Critica
	Method New() Constructor
	Method Validar()
EndClass

Method New() Class CritCNPJ

	_Super:New()
	self:setAlias('BKR')
	self:setCodCrit('M017' )
	self:setMsgCrit('CPF / CNPJ Inv�lido.')
	self:setSolCrit('O CPF / CNPJ deve ser um n�mero v�lido e existir na base de dados da Receita Federal.')
	self:setCpoCrit('BKR_CPFCNP')
	self:setCodAns('1206')

Return Self

Method Validar() Class CritCNPJ
Return !Empty(Self:oEntity:getValue("providerCpfCnpj")) .And. Self:oEntity:getValue("providerCpfCnpj") != '00000000000000' .And. CGC(Self:oEntity:getValue("providerCpfCnpj"),,.F.)

