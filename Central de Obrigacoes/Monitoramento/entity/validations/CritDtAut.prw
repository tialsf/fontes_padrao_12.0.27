#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtAut
Descricao: 	Critica referente ao Campo.
				-> BKR_DATAUT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtAut From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtAut
	_Super:New()
	self:setCodCrit('M019' )
	self:setMsgCrit('Data da Autoriza��o da Guia � Inv�lida.')
	self:setSolCrit('A data deve ser v�lida.' )
	self:setCpoCrit('BKR_DATAUT')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtAut
	Local cQuery := ""
	cQuery += " 	AND BKR_TPEVAT = '3' "
	cQuery += " 	AND BKR_DATAUT = '' "
Return cQuery