#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtFinFat
Descricao: 	Critica referente ao Campo.
				-> BKR_DTFIFT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtFinFat From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtFinFat
	_Super:New()
	self:setCodCrit('M022' )
	self:setMsgCrit('Data Final de Faturamento Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e diferente da Data Atual e menor ou igual a data de Registro da Transa��o, , maior ou igual � data de in�cio do faturamento.' )
	self:setCpoCrit('BKR_DTFIFT')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtFinFat
	Local cQuery := ""
	cQuery += " 	AND BKR_TPEVAT = '3' "
	cQuery += " 	AND ( BKR_DTFIFT = '' "
	cQuery += " 		OR BKR_DTFIFT > '" + DtoS(Date()) + "' "
	cQuery += " 		OR BKR_DTFIFT < BKR_DTINFT ) "
Return cQuery


