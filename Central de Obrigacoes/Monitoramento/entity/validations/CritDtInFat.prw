#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtInFat
Descricao: 	Critica referente ao Campo.
				-> BKR_DTINFT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtInFat From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtInFat
	_Super:New()
	self:setCodCrit('M021' )
	self:setMsgCrit('Data de Inicio de Faturamento Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e diferente da Data Atual e menor ou igual a data de Registro da Transa��o.' )
	self:setCpoCrit('BKR_DTINFT')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtInFat
	Local cQuery := ""
	cQuery += " 	AND BKR_DTINFT > '" + DtoS(Date()) + "' "
Return cQuery


