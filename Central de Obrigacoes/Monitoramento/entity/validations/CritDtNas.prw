#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtNas
Descricao: 	Critica referente ao Campo Data de Nascimento.
				-> B3K_DATNAS
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtNas From Critica
	Method New() Constructor
	Method Validar()
EndClass

Method New() Class CritDtNas
	_Super:New()
	self:setAlias('BKR')
	self:setCodCrit('M016')
	self:setMsgCrit('Data de Nascimento do Benefici�rio � Inv�lida.')
	self:setSolCrit('')
	self:setCpoCrit('B3K_DATNAS')
	self:setCodAns('1323')
Return Self

Method Validar() Class CritDtNas

	Local lRet		:= .T.
	Local cSolCrit 	:= ''
	Local oDaoBenef := DaoCenBenefi():new()
	Local oBscBenef := BscCenBenefi():new(oDaoBenef)
	Local oBenef	:= nil

	oDaoBenef:setMatric(Self:oEntity:getValue("registration"))
	oDaoBenef:setCodOpe(Self:oEntity:getValue("operatorRecord"))
	
	oBscBenef:buscar()
	If oBscBenef:hasNext()
		oBenef := oBscBenef:getNext()
		self:setDesOri(oBenef:getNomBen())  
		
		If (Empty(oBenef:getDatNas()) .AND. Empty(oBenef:getCNS()) ) .Or. oBenef:getDatNas() > StoD(Self:oEntity:getValue("executionDate"))
			cSolCrit := 'Corrigir o conteudo do Campo Data de Nascimento do Benefici�rio. O conteudo deve ser uma data valida e inferior ou igual a data de realiza��o do procedimento.'	
			lRet	:= .F.
		EndIf
		oBenef:destroy()
	Else
		cSolCrit := 'Verifique o preenchimento da Data de Nascimento do Benefici�rio.'
		lRet	:= .F.
	EndIf

	Self:setSolCrit(cSolCrit)
	
	oBscBenef:destroy()
	oBscBenef := nil
	oBenef := nil
	FreeObj(oBscBenef)
	FreeObj(oBenef)

Return lRet
