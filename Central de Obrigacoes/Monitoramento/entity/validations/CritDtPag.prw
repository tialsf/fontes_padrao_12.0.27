#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtPag
Descricao: 	Critica referente ao Campo.
				-> BKR_DTPAGT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtPag From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtPag
	_Super:New()
	self:setCodCrit('M024' )
	self:setMsgCrit('Data do Pagamento � Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e menor que a Data Atual e maior ou igual a data do Protocolo de Cobran�a. Atendimentos em rede pr�pria da operadora o campo n�o deve ser preenchido. ')
	self:setCpoCrit('BKR_DTPAGT')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtPag
	Local cQuery := ""
	cQuery += " 	AND BKR_DTPAGT <> '' "
	cQuery += " 	AND ( BKR_DTPAGT > '" + DtoS(Date()) + "' "
	cQuery += " 		OR BKR_DTPAGT < BKR_DTPROT ) "
Return cQuery


