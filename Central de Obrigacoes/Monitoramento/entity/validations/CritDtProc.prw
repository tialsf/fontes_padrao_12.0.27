#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtProc
Descricao: 	Critica referente ao Campo.
				-> BKR_DTPRGU
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtProc From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtProc
	_Super:New()
	self:setCodCrit('M025' )
	self:setMsgCrit('Data de Processamento da Guia � Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e menor que a Data Atual.')
	self:setCpoCrit('BKR_DTPRGU')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtProc
	Local cQuery := ""
	cQuery += " 	AND BKR_DTPRGU > '" + DtoS(Date()) + "' "
Return cQuery

