#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtProt
Descricao: 	Critica referente ao Campo.
				-> BKR_DTPROT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtProt From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtProt
	_Super:New()
	self:setCodCrit('M023' )
	self:setMsgCrit('Data do Protocolo de Cobran�a � Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e menor que a Data Atual e menor ou igual a data de Registro da Transa��o.' )
	self:setCpoCrit('BKR_DTPROT')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtProt
	Local cQuery := ""
	cQuery += "		AND BKR_DTPROT <> '' "
	cQuery += "		AND ( BKR_DTPROT > '" + DtoS(Date()) + "' "
	cQuery += "			OR BKR_DTPROT > BKR_DATREA ) "
Return cQuery