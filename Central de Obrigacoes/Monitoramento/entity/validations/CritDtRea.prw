#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtRea
Descricao: 	Critica referente ao Campo.
				-> BKR_DATAUT
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtRea From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtRea
	_Super:New()
	self:setCodCrit('M004')
	self:setMsgCrit('Data da Realiza��o do Atendimento/Procedimento da Guia � Inv�lida.')
	self:setSolCrit('A data deve ser v�lida e diferente da Data Atual e menor ou igual a data de Registro da Transa��o.' )
	self:setCpoCrit('BKR_DATREA')
	self:setCodAns('1323')
Return Self

Method getWhereCrit() Class CritDtRea
	Local cQuery := ""
	cQuery += "		AND (BKR_DATREA = '' OR BKR_DATREA > '" + DtoS(Date()) + "') "
Return cQuery