#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} CritDtSol
Descricao: 	Critica referente ao Campo.
				-> BKR_DATSOL
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class CritDtSol From CritGrpBKR
	Method New() Constructor
	Method getWhereCrit()
EndClass

Method New() Class CritDtSol
	_Super:New()
	self:setCodCrit('M003' )
	self:setMsgCrit('Data da Guia de Solicita��o � Inv�lida.')
	self:setSolCrit('Preencha o campo Data da Guia de Solicita��o com uma data v�lida.')
	self:setCpoCrit('BKR_DATSOL')
	self:setCodANS('1323')
Return Self

Method getWhereCrit() Class CritDtSol
	Local cQuery := ""
	cQuery += " 	AND BKR_TPEVAT IN ('2','3') "
	cQuery += " 	AND BKR_DATSOL = '' "
Return cQuery