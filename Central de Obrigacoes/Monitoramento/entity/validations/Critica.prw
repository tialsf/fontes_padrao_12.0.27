#Include "Totvs.ch"
//-------------------------------------------------------------------
/*/{Protheus.doc} Critica
Descricao: Classe abstrata das Criticas do Projeto de Monitoramento.	
@author Hermiro J�nior
@since 01/10/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Class Critica 

	Data cCodCrit
	Data cMsgCrit
	Data cSolCrit
	Data cCpoCrit
	Data cAlias
	Data cOper
	Data cObrig
	Data cAno
	Data cComp
	Data nRecno
	Data cTpVld
	Data cCodANS
	Data cChaveOri
	Data cDesOri
	Data cStatus
	Data oEntity

	Method New() Constructor
	Method destroy()
	Method setCodCrit(cCodCrit)
	Method setMsgCrit(cMsgCrit)
	Method setSolCrit(cSolCrit)
	Method setCpoCrit(cCpoCrit)
	Method setAlias(cAlias)
	Method setOper(cOper)
	Method setObrig(cObrig)
	Method setAno(cAno)
	Method setComp(cComp)
	Method setRecno(nRecno)
	Method setTpVld(cTpVld)
	Method setCodANS(cCodANS)
	Method setChaveOri(cChaveOri)
	Method setDesOri(cDesOri)
	Method setStatus(cStatus)
	Method setEntity(oEntity)
	Method getCodCrit()
	Method getMsgCrit()
	Method getSolCrit()
	Method getCpoCrit()
	Method getAlias()
	Method getOper()
	Method getObrig()
	Method getAno()
	Method getComp()
	Method getRecno()
	Method getTpVld()
	Method getCodANS()
	Method getChaveOri()
	Method getDesOri()
	Method getStatus()
	Method getObj()
	Method validar()

EndClass

Method New() Class Critica
	self:setTpVld('1')
	self:setStatus('1' )
Return Self

Method destroy() Class Critica
Return

Method setCodCrit(cCodCrit) Class Critica
	self:cCodCrit := cCodCrit
Return

Method setMsgCrit(cMsgCrit) Class Critica
	self:cMsgCrit := cMsgCrit
Return

Method setSolCrit(cSolCrit) Class Critica
	self:cSolCrit := cSolCrit
Return

Method setCpoCrit(cCpoCrit) Class Critica
	self:cCpoCrit := cCpoCrit
Return

Method setAlias(cAlias) Class Critica
	self:cAlias := cAlias
Return

Method setOper(cOper) Class Critica
	self:cOper := cOper
Return

Method setObrig(cObrig) Class Critica
	self:cObrig := cObrig
Return

Method setAno(cAno) Class Critica
	self:cAno := cAno
Return

Method setComp(cComp) Class Critica
	self:cComp := cComp
Return

Method setRecno(nRecno) Class Critica
	self:nRecno := nRecno
Return

Method setTpVld(cTpVld) Class Critica
	self:cTpVld := cTpVld
Return

Method setCodANS(cCodANS) Class Critica
	self:cCodANS := cCodANS
Return

Method setChaveOri(cChaveOri) Class Critica
	self:cChaveOri := cChaveOri
Return

Method setDesOri(cDesOri) Class Critica
	self:cDesOri := cDesOri
Return

Method setStatus(cStatus) Class Critica
	self:cStatus := cStatus
Return

Method setEntity(oEntity) Class Critica
	self:oEntity := oEntity
Return

Method getCodCrit() Class Critica
Return self:cCodCrit

Method getMsgCrit() Class Critica
Return self:cMsgCrit

Method getSolCrit() Class Critica
Return self:cSolCrit

Method getCpoCrit() Class Critica
Return self:cCpoCrit

Method getAlias() Class Critica
Return self:cAlias

Method getOper() Class Critica
Return self:cOper

Method getObrig() Class Critica
Return self:cObrig

Method getAno() Class Critica
Return self:cAno

Method getComp() Class Critica
Return self:cComp

Method getRecno() Class Critica
Return self:nRecno

Method getTpVld() Class Critica
Return self:cTpVld

Method getCodANS() Class Critica
Return self:cCodANS

Method getChaveOri() Class Critica
Return self:cChaveOri

Method getDesOri() Class Critica
Return self:cDesOri

Method getStatus() Class Critica
Return self:cStatus

Method getObj() Class Critica
Return self:oEntity

Method Validar() Class Critica
Return .F.