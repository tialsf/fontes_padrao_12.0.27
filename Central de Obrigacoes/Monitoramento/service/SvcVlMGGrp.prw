#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} 
    Funcao principal que e chamada para iniciar o servico no server
    @type  Function
    @author everton.mateus
    @since 09/12/2019
/*/
Main Function SvcVlMGGrp()
    
    Local oSvcVlMGGrp := SvcVlMGGrp():New()
    oSvcVlMGGrp:run()
    FreeObj(oSvcVlMGGrp)
    oSvcVlMGGrp := nil

return

Static Function SchedDef()
Return { "P","PLSVALSIB",,{},""}

/*/{Protheus.doc} 
    Job que processa as guias que chegaram via API
    
    @type  Function
    @author everton.mateus
    @since 06/12/2019
/*/
Function JobVlMGGrp(cEmp, cFil, lJob, cCodOpe)
    Local aSvcVldr  := {}
    Local aSvcVlInd := {}
    Default lJob    := .T.
    Default cCodOpe := ""

	If lJob
        rpcSetType(3)    
        rpcSetEnv( cEmp, cFil,,,GetEnvServer(),, )
    EndIf

    aSvcVldr := {;
                    SvcVlMGGrp():New(),;   // Guia Monitoramento  - Servi�o de valida��o em grupo
                    SvcVlGrItG():New(),;   // Guia Monitoramento  - Servi�o de valida��o em grupo dos itens
                    SvcVlGrPcG():New(),;   // Guia Monitoramento  - Servi�o de valida��o em grupo dos pacotes
                    SvcVlGrFDi():New(),;   // Fornecimento Direto - Servi�o de valida��o em grupo das guias  
                    SvcVlGrItF():New(),;   // Fornecimento Direto - Servi�o de valida��o em grupo dos itens 
                    SvcVlGrVPr():New(),;   // Preestabelecido     - Servi�o de valida��o em grupo das guias 
                    SvcVlGrORe():New();    // Outras Remunera��es - Servi�o de valida��o em grupo das guias 
                    }

    aSvcVlInd := {;
                    SVCVLINGMO():New(),;   // Guia Monitoramento  - Servico que valida individualmente do cabe�alho das guias
                    SvcVlInItG():New(),;   // Guia Monitoramento  - Servico que valida individualmente os itens das guias 
                    SvcVlInPcG():New(),;   // Guia Monitoramento  - Servico que valida individualmente os pacotes das guias 
                    SVCVLINFDI():New(),;   // Fornecimento Direto - Servico que valida individualmente os itens das guias 
                    SvcVlItFDi():New(),;   // Fornecimento Direto - Servico que valida individualmente os itens das guias 
                    SVCVLINVPR():New(),;   // Preestabelecido     - Servico que valida individualmente as guias de valor 
                    SVCVLINORE():New();    // Outras Remunera��es - Servico que valida as guias de outras remunera��es
                    }
 
    ExecVldMon(cCodOpe,aSvcVldr,aSvcVlInd,cEmp, cFil)

Return

Function ExecVldMon(cCodOpe,aSvcVldr,aSvcVlInd,cEmp, cFil)
    Local nLen      := 0
    Local nVldr     := 0

    Default aSvcVldr  := {}
    Default aSvcVlInd := {}
    Default lJob      := .T.
    Default cCodOpe   := ""
    Default cEmp      := ""
    Default cFil      := ""

    nLen := Len(aSvcVldr)
    For nVldr := 1 to nLen
       aSvcVldr[nVldr]:setProcId(ThreadId())
        aSvcVldr[nVldr]:setCodOpe(cCodOpe)
        If aSvcVldr[nVldr]:beforeProc()
            aSvcVldr[nVldr]:logMsg("W","vai processar " + GetClassName(aSvcVldr[nVldr]))
            aSvcVldr[nVldr]:runProc()
            aSvcVldr[nVldr]:logMsg("W","processou " + GetClassName(aSvcVldr[nVldr]))
        EndIf
        aSvcVldr[nVldr]:destroy()
        FreeObj(aSvcVldr[nVldr])
        aSvcVldr[nVldr] := nil
    Next nVldr

    nLen := Len(aSvcVlInd)
    For nVldr := 1 to nLen
 
        aSvcVlInd[nVldr]:logMsg("W","vai processar " + GetClassName(aSvcVlInd[nVldr]))
        JobVldInMon(cEmp, cFil, .F., aSvcVlInd[nVldr])
        aSvcVlInd[nVldr]:logMsg("W","processou " + GetClassName(aSvcVlInd[nVldr]))
        aSvcVlInd[nVldr]:destroy()
        FreeObj(aSvcVlInd[nVldr])
        aSvcVlInd[nVldr] := nil
    Next nVldr
    DelClassIntf()

Return

/*/{Protheus.doc} 
    Servi�o de valida��o em grupo das guias de monitoramento tiss
    @type  Class
    @author everton.mateus
    @since 31/01/2018
/*/
Class SvcVlMGGrp From Service
	   
    Method New() 
    Method runProc()
    
EndClass

Method New() Class SvcVlMGGrp
    _Super:New()
    self:cFila := "FILA_VLD_GUI_MON_GRP"
    self:cJob := "JobVlMGGrp"
    self:cObs := "Valida em grupo todas as formas de remuneracao"
    self:oFila := CenFilaBd():New(CenCltBKR():New())
    self:oProc := CenVldMGui():New()
    self:oCenLogger:setFileName(self:cJob)
Return self

Method runProc() Class SvcVlMGGrp
    self:oProc:setOper(self:cCodOpe)
    self:oProc:vldGrupo(self:oFila:oCollection)
Return