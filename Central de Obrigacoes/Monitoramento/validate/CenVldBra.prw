#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"

Class CenVldBra from CenValidator

    Method New() Constructor
    Method validate(oEntity)

EndClass

Method New(oRest) Class CenVldBra
    _Super:New(oRest)
Return self

Method validate(oEntity) Class CenVldBra
    Local lValid    := .T.
    
    self:cMsg := ''
    self:valOpe(oEntity:getValue("operatorRecord"))
    Iif(!lValid,self:cMsg += " Operadora nao cadastrada. ",self:cMsg := self:cMsg)

Return lValid
