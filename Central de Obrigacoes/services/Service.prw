
#INCLUDE "TOTVS.CH"

#define TOKEN    1
#define MENSAGEM 2

/*/{Protheus.doc}
    Classe abstrata que controla a execucao de um servico de processamento
    @type  Class
    @author everton.mateus
    @since 31/01/2018
/*/
Class Service

    Data cFila
    Data cLista
    Data cJob
    Data oFila
    Data oLista
    Data nStatusCode
    Data nFaultCode
    Data cFaultDesc

    Method New() Constructor
    Method destroy()
    Method run()
    Method pushFila(cMsg,cToken)
    Method popLista(cToken,cMsg)
    Method getStatus()
    Method getFault()

EndClass

Method New() Class Service
    self:oFila       := TQueue():New(self:cFila)
    self:oLista      := TList():New(self:cLista)
    self:nStatusCode := 0
    self:nFaultCode  := 0
    self:cFaultDesc  := ""
Return self

Method destroy() Class Service

    If !Empty(self:oFila)
        FreeObj(self:oFila)
        self:oFila := nil
    EndIf

    If !Empty(self:oLista)
        FreeObj(self:oLista)
        self:oLista := nil
    EndIf

Return

Method run() Class Service

    Local aMsg   := {}

    If self:oFila:checkQueue() .AND. self:oFila:setupQueue()
        While !KillApp()
            aMsg := self:oFila:getMsg()
            if !Empty(aMsg)
                SmartJob(self:cJob, GetEnvServer(), .F., aMsg[TOKEN],aMsg[MENSAGEM])
            endIf
        EndDo
    EndIf

Return self

Method pushFila(cMsg,cToken) Class Service
    Local lSuccess := .T.

    if self:oFila:checkQueue() .AND. self:oFila:setupQueue()
        cToken := self:oFila:addMsg(cMsg)
        conout("[" + self:cFila + "]["+cToken+"] : Adicionou uma requisicao")
    else
        self:cFaultDesc := self:oFila:getErro()
        lSuccess := .F.
    endif

Return lSuccess

Method popLista(cToken,cMsg) Class Service
    Local lSuccess := .T.

    If self:oLista:checkList() .AND. self:oLista:setupList()
        cMsg := self:oLista:getMsg(cToken)
        If !Empty(cMsg)
            self:oLista:delMsg(cToken)
            conout("[" + self:cLista +"]["+cToken+"] : Recuperou a resposta")
            self:nStatusCode := 201
        else
            cMsg := '{"serviceResponse":false}'
            self:nStatusCode := 202
        EndIf
    else
        self:nFaultCode := 500
        self:cFaultDesc := self:oLista:getErro()
        lSuccess := .F.
    endif

Return lSuccess

Method getStatus() Class Service
Return self:nStatusCode

Method getFault() Class Service
Return {self:nFaultCode,self:cFaultDesc}
