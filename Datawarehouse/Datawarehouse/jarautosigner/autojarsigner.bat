:: @author  Valdiney V GOMES
:: @since   23/07/2012

:: How To

:: O caminho para o diret�rio bin da instala��o do JDK deve ser informado na vari�vel de ambiente do windows PATH.
:: A m�quina utilizada para assinatura do *.jar deve utilizar o idioma Ingl�s (Estados Unidos).
:: Os arquivos *.jar, autojarsigner.bat e totvs.p12 devem estar em um mesmo diret�rio.
:: Executar o arquivo autojarsigner.bat.

@echo off
echo.
echo Informe a senha do certificado totvs.p12:
echo.
set /p pass=
@echo on

keytool -storepass %pass% -list -storetype pkcs12 -keystore totvs.p12 

:: Keystore type: PKCS12
:: Keystore provider: SunJSSE

:: Your keystore contains 1 entry

:: {02b6e1f2-e35e-4023-b311-91a352af8877}, Jul 23, 2012, PrivateKeyEntry,
:: Certificate fingerprint (MD5): 2B:4E:D0:64:D3:7F:85:47:68:BE:47:A1:AD:42:79:7A

jarsigner -storepass %pass% -storetype pkcs12 -verify -verbose -keystore totvs.p12 sigadw3.jar {02b6e1f2-e35e-4023-b311-91a352af8877}
