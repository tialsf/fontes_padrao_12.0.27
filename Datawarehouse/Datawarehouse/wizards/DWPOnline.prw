#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "DWWIZARD.CH"

#DEFINE NAME_XML_ORIGEM		"indnativosquery.xml"

// #################################################################################################
// Projeto: DATAWAREHOUSE
// Modulo : Wizard
// Fonte  : DWPOnline - Fonte para leitura do XML de indicadores nativos e cria��o de arquivo fonte
// ---------+------------------------+--------------------------------------------------------------
// Data     | Autor                  | Descricao
// ---------+------------------------+--------------------------------------------------------------
// 26.03.07 | 6362-Renato Ceadareanu | Vers�o 1
// -------------------------------------------------------------------------------------------------
    
Function LoadPGDW(aIsAll)
	local cXmlName 	:= getPathStart() + NAME_XML_ORIGEM
	local cProgram 	:= "", nInd 
	local aCons    	:= {}
	local lClose	:= .F. 
	
	Default aIsAll 	:= .F.
	
	If ! ( Select('SX6') > 0 )
		DBSelectArea('SX6') 
		lClose := .T.
	EndIf  

	if !empty(GetMV("MV_DWURL", .F., "")) .and. !empty(GetMV("MV_DWNAME", .F., ""))
		If isValidMetaWizard(cXmlName)
			aCons := getConsXML(cXmlName)
			for nInd := 1 to len(aCons)
				if aCons[nInd][4] == "T" .or. aIsAll
					if aCons[nInd][5] == "T"
					   	cProgram += " oPGOnline:AddPanel(' " + aCons[nInd][3] + " - " + STR0086 + " ',' "+ aCons[nInd][3] + "', 6 , " + ; //###"Tabela"
			   					  ",,,,,'" + aCons[nInd][2] + "1',,'" + getMv("MV_DWURL") + "','"+ getMv("MV_DWNAME") + ;
			   					  "','" + aCons[nInd][2]  + "',1) " + CRLF                                               
			  		endif
			  		if aCons[nInd][6] == "T"
					   	cProgram += " oPGOnline:AddPanel(' " + aCons[nInd][3] + " - " + STR0087 + " ',' "+ aCons[nInd][3] + "', 6 , " + ; //###"Grafico"
				   				  ",,,,,'" + aCons[nInd][2] + "2',,'" + getMv("MV_DWURL") + "','"+ getMv("MV_DWNAME") + ;
				   				  "','" + aCons[nInd][2]  + "',2) " + CRLF
				   endif
				endif
			next
		EndIf
	endif 
	
	If ( lClose ) 
		SX6->(DBCloseArea()) 
	EndIf	
return cProgram    

Function LoadMenuDW()
	local aMenus 	:= {}                                
	local cXmlName 	:= getPathStart() + NAME_XML_ORIGEM
	local nInd 
	local aCons 	:= {}  
	local lClose	:= .F. 
	
	If ! ( Select('SX6') > 0 )
		DBSelectArea('SX6') 
		lClose := .T.
	EndIf  

	if !empty(GetMV("MV_DWURL", .F., "")) .and. !empty(GetMV("MV_DWNAME", .F., ""))
		If isValidMetaWizard(cXmlName)
			aCons := getConsXML(cXmlName)
			for nInd := 1 to len(aCons)                                                                            
		  		if aCons[nInd][5] == "T"
					aadd(aMenus, { getMv("MV_DWURL"), getMv("MV_DWNAME"), aCons[nInd][2], aCons[nInd][3], "1" })
				endif
		  		if aCons[nInd][6] == "T"
					aadd(aMenus, { getMv("MV_DWURL"), getMv("MV_DWNAME"), aCons[nInd][2], aCons[nInd][3], "2" })
				endif
			next 
		EndIf
	endif    
	
	If ( lClose ) 
		SX6->(DBCloseArea()) 
	EndIf	
return aMenus