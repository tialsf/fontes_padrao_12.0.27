package br.com.microsiga.sigadw.applet;

import br.com.microsiga.sigadw.chart.AbstractDwScriptChart;
import br.com.microsiga.sigadw.chart.DwScriptTabbedChart;
import br.com.microsiga.sigadw.chart.DwScriptWindowChart;
import javax.swing.JApplet;

/**
 * JApplet de Gr�ficos para o SigaDW. Dependendo das configura��es de parameters ir�
 *  inicializar o melhor painel para visualizar o gr�fico, painel com JTabbedPanel ou
 *  painel simples
 * @author Paulo R. Vieira - TOTVS S/A
 * @since SigaDW R4
 */
public class DWScriptChart extends JApplet {
    
    private AbstractDwScriptChart dwScriptChart;
    
    public DWScriptChart() {
        super();
    }
    
    public void init() {
        loadAppletProperties();
        dwScriptChart.init();
        dwScriptChart.setChartRootPane(this);
        this.add(dwScriptChart);
    }

    @Override
    public void destroy() {
        super.destroy();
        this.dwScriptChart.destroy();
    }
    
    /**
     * Carrega as propriedades iniciais para o funcionamento do Applet de Gr�ficos do SigaDW
     */
    protected void loadAppletProperties() {
        // configura��o do modo de paineis, com abas ou na mesma janela e contendo DrillDowns
        if (this.getParameter("WINDOWLESS") != null && "TRUE".equalsIgnoreCase(this.getParameter("DW_OPENDRILL"))) {
            dwScriptChart = new DwScriptTabbedChart();
        } else {
            dwScriptChart = new DwScriptWindowChart("TRUE".equalsIgnoreCase(this.getParameter("DW_OPENDRILL")));
        }
        
        // se n�o for passado a SESSION, define as propriedades por DEFAULT
        if (this.getParameter("DWSESSION") != null) {
            AbstractDwScriptChart.setURLBase(this.getCodeBase().toString());
            AbstractDwScriptChart.setChartDirLicense(this.getParameter("CHART_DIIR_LICENSE"));
            AbstractDwScriptChart.setProgram(this.getParameter("DWPROGRAM"));
            AbstractDwScriptChart.setDWSession(this.getParameter("DWSESSION"));
            AbstractDwScriptChart.setError(this.getParameter("MSGERROR"));
            AbstractDwScriptChart.setAction(this.getParameter("DWACTION"));
            AbstractDwScriptChart.setLoading(this.getParameter("MSLOADING"));
            AbstractDwScriptChart.setQuerySleep(Integer.parseInt(this.getParameter("QUERY_TIME")));
            AbstractDwScriptChart.setQueryMaxTime(Integer.parseInt(this.getParameter("QUERY_MAX_TIME")));
            AbstractDwScriptChart.setBlockGraphRequest(Integer.parseInt(this.getParameter("BLOCK_GRAPH_REQUEST")));
            AbstractDwScriptChart.setRequestError(this.getParameter("REQUEST_ERROR"));
            AbstractDwScriptChart.setError(this.getParameter("ERROR"));
            AbstractDwScriptChart.setZoomInImg(this.getParameter("IMG_ZOOM_IN"));
            AbstractDwScriptChart.setZoomRestImg(this.getParameter("IMG_ZOOM_REST"));
            AbstractDwScriptChart.setZoomOutImg(this.getParameter("IMG_ZOOM_OUT"));
            AbstractDwScriptChart.setExportImg(this.getParameter("IMG_EXPORT"));
            AbstractDwScriptChart.setCloseImg(this.getParameter("IMG_CLOSE"));
            AbstractDwScriptChart.setExportGraphURL(this.getParameter("URL_EXPORT_GRAPH"));
            AbstractDwScriptChart.setLabelZoomIn(this.getParameter("LABEL_ZOOM_IN"));
            AbstractDwScriptChart.setLabelZoomRest(this.getParameter("LABEL_ZOOM_REST"));
            AbstractDwScriptChart.setLabelZoomOut(this.getParameter("LABEL_ZOOM_OUT"));
            AbstractDwScriptChart.setLabelExport(this.getParameter("LABEL_EXPORT"));
            AbstractDwScriptChart.setLabelClose(this.getParameter("LABEL_CLOSE"));
            AbstractDwScriptChart.setLabelNextDrill(this.getParameter("LABEL_NEXTDRILL"));
            AbstractDwScriptChart.setLabelPrevDrill(this.getParameter("LABEL_PREVDRILL"));
            //Atribui valor as propriedades da documentacao.
            AbstractDwScriptChart.setImgDocument(this.getParameter("IMG_DOCUMENT"));
            AbstractDwScriptChart.setURLDocument(this.getParameter("URL_DOCUMENT"));
            AbstractDwScriptChart.setLabelDocument(this.getParameter("LABEL_DOCUMENT"));
        } else {
            // C�DIGO PARA DEBUG
            int intQueryId = 149; //1414; // mudar o id da consulta para qual se quer debugar
            String strSessionID = "1223398828"; //mudar a sess�o para uma sess�o do SigaDW
            String strServer = "http://bi-valdiney/dw/"; // mudar a url de acesso ao SigaDW
            
            if (intQueryId == 0 || "".equals(strSessionID) || "".equals(strServer))
                throw new IllegalStateException("O m�dulo de debug para os gr�ficos do SigaDW foi inicializado com par�metros inv�lidos. Verifique pelas propriedades intQueryId, strSessionID e strServer no arquivo br.com.microsiga.sigadw.applet.DWScriptChart2; e pelas informa��es de como rodar em modo debug, para rodar em modo debug.");
            
            AbstractDwScriptChart.setDWSession(strSessionID);
            AbstractDwScriptChart.setURLBase(strServer);
            AbstractDwScriptChart.setAction("?action=querygraph&id=" + intQueryId + "&tipo=2&oper=6&loadpgdw=&amp;forceReload=" + System.currentTimeMillis());
            AbstractDwScriptChart.setExportGraphURL("?action=queryConfigExport&amp;id=" + intQueryId + "&amp;type=2&amp;tipo=4");
            AbstractDwScriptChart.setChartDirLicense("RDST-245J-39ZJ-A6R8-ED7B-1DCB");
            AbstractDwScriptChart.setProgram("w_sigadw3.apw");
            AbstractDwScriptChart.setLoading("por favor aguarde, carregando...");
            AbstractDwScriptChart.setQuerySleep(Integer.parseInt("500"));
            AbstractDwScriptChart.setQueryMaxTime(Integer.parseInt("100000"));
            AbstractDwScriptChart.setBlockGraphRequest(Integer.parseInt("7"));
            AbstractDwScriptChart.setRequestError("Aten��o, ocorreu um erro no processamento da requisi��o.");
            AbstractDwScriptChart.setError("erro na requisi��o");
            
            AbstractDwScriptChart.setZoomInImg(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/ic_zoom_in.gif");
            AbstractDwScriptChart.setZoomRestImg(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/ic_zoom.gif");
            AbstractDwScriptChart.setZoomOutImg(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/ic_zoom_out.gif");
            AbstractDwScriptChart.setExportImg(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/page_export.gif");
            AbstractDwScriptChart.setCloseImg(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/page_close.gif");

            AbstractDwScriptChart.setLabelZoomIn("Aumentar Zoom");
            AbstractDwScriptChart.setLabelZoomRest("Restaurar Zoom");
            AbstractDwScriptChart.setLabelZoomOut("Diminuir Zoom");
            AbstractDwScriptChart.setLabelExport("Exportar");
            AbstractDwScriptChart.setLabelClose("Fechar DrillDown");
            AbstractDwScriptChart.setLabelNextDrill("pr�ximo n�vel");
            AbstractDwScriptChart.setLabelPrevDrill("n�vel anterior");
            
            //Atribui valor as propriedades da documentacao.
            AbstractDwScriptChart.setImgDocument(AbstractDwScriptChart.getURLBase() + "themes/p10/hgraph/ic_doc.gif");
            AbstractDwScriptChart.setURLDocument("D"); 
            AbstractDwScriptChart.setLabelDocument("Documenta��o");
            
            this.setSize(885, 550);
        }
    }
    
}