package br.com.microsiga.sigadw.applet;

import br.com.microsiga.sigadw.definition.DwAxis;
import br.com.microsiga.sigadw.definition.DefinitionWorkspace;
import br.com.microsiga.sigadw.definition.to.Dialogy;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.definition.to.AgregationFunction;
import br.com.microsiga.sigadw.definition.to.Cube;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.GraphType;
import br.com.microsiga.sigadw.definition.to.Measure;
import java.awt.BorderLayout;
import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Applet que implementa a defini��o de uma tabela ou gr�fico
 * @author Paulo R. Vieira -  Microsiga Software S/A
 * @since SigaDW R4 17/04/2006
 */
public class DWScriptDefinition extends JApplet {
    
    // cria o objeto que conter� todas as propriedades referente a defini��o
    private DefinitionProperties properties = DefinitionProperties.getInstance();
    private DefinitionWorkspace definition;
    private static int DIMENSION_COUNTER    = 0;
    
    /**
     * Construtor principal que constr�i o applet
     */
    public DWScriptDefinition() {
        super();
    }
    
    /**
     * M�todo respons�vel pela inicializa��o do applet e o carregamento de suas propriedades
     */
    public void init() {
        System.out.println("");
        System.out.println("");
        System.out.println("------------------------------------------");
        System.out.println("==> Starting SigaDW Definition <==");
        System.out.println("Microsiga Software S/A");
        System.out.println("http://www.microsiga.com.br");
        System.out.println("");
        System.out.println("* Browser properties");
        System.out.println("Browser: "               + System.getProperty("browser"));
        System.out.println("Vendor: "                + System.getProperty("browser.vendor"));
        System.out.println("Version: "               + System.getProperty("browser.version"));
        System.out.println("File Separator: "        + System.getProperty("file.separator"));
        System.out.println("Http Agent: "            + System.getProperty("http.agent"));
        System.out.println("");
        System.out.println("* Java properties");
        System.out.println("Class Version: "         + System.getProperty("java.class.version"));
        System.out.println("Specification Vendor: "  + System.getProperty("java.specification.vendor"));
        System.out.println("Specification Version: " + System.getProperty("java.specification.version"));
        System.out.println("Vendor: "                + System.getProperty("java.vendor"));
        System.out.println("Version: "               + System.getProperty("java.version"));
        System.out.println("");
        System.out.println("* Others properties");
        System.out.println("File Separator: "        + System.getProperty("file.separator"));
        System.out.println("Line Separator: "        + System.getProperty("line.separator"));
        System.out.println("O.S. Name: "             + System.getProperty("os.name"));
        System.out.println("O.S. Version: "          + System.getProperty("os.version"));

        System.out.println("------------------------------------------");
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        
        properties.clean();
        //verifica se o applet cont�m as configura��es necess�rias para a inicializa��o,
        // sen�o contiver inicializa as vari�veis com valores de teste
        if (getParameter("imagePath") != null) {
            // carrega as propriedades oriundas da tag applet
            loadProperties();
            
            // instancia o component swing da defini��o
            this.initGUI();
        } else {
            // inicializa as vari�veis com valores de teste
            loadMockProperties();
            
            // instancia o component swing da defini��o
            this.initGUI();
            this.setSize(885, 550);
            
            // inicializa��o de valores que originalmente viriam de chamadas via JavaScript
            mockInit();
            this.definition.redrawTree();
        }
    }
    
    /**
     * M�todo respons�vel pela inicializa��o do interface gr�fico com o usu�rio
     */
    public void initGUI() {
        // instancia o component swing da defini��o
        setDefinition(new DefinitionWorkspace());
        setLayout(new BorderLayout());
        add(getDefinitionWorkspace(), BorderLayout.CENTER);
    }
    
    /**
     * M�todo respons�vel pela constru��o da �rvore de objetos do SigaDw. Usar s� no inicio.
     */
    public void buildTree() {
        getDefinitionWorkspace().redrawTree();
    }
    
    /**
     * M�todo respons�vel por definir o nome do cubo relacionado com esta defini��o
     * @param text
     *      Objeto do tipo <code>String</code> contendo o nome
     */
    public void setCubeName(String text) {
        properties.getCube().setName(text);
    }
    
    /**
     * M�todo respons�vel por adicionar um objeto do tipo dimension para estar
     *  dispon�vel para esta defini��o
     * @param text
     *      Objeto do tipo <code>String</code> contento o nome da dimens�o
     * @param ID
     *      Primitiva do tipo <code>int</code> contendo o c�digo da dimens�o
     * @param strImage
     *      Objeto do tipo <code>String</code> contendo um caminho/path para uma
     *  imagem v�lida/dispon�vel online que representar� esta dimens�o
     */
    public void addDimension(String text, int ID, String strImage) {
        int count = 0;
        Icon icon = null;
        DwField fieldTemporal;
        DwField field;
        
        // verifica se � um label de uma dimens�o
        if (text.substring(0, 1).equals("#")) {
            properties.getCube().addDimension(new Dimension(DIMENSION_COUNTER++, text.substring(1), properties.getImagePath() + strImage));
        } else {
            List lstDimensions = properties.getCube().getDimensions();
            Dimension dimension;
            if (lstDimensions.size() > 0) {
                dimension = (Dimension) lstDimensions.get(lstDimensions.size() - 1);
                
                field = new DwField(ID, (text.substring(0, 1).equals("@")) ? text.substring(1) : text, properties.getImagePath() + strImage, dimension.getId());
                dimension.addDimField(field);
                
                // verifica se o campo � temporal
                if (text.substring(0, 1).equals("@")) {
                    // itera pelos temporais adicionando � estrutura do campo
                    for (String strTemporal : properties.getTemporal()) {
                        count++;
                        fieldTemporal = new DwField(count, ID, strTemporal, properties.getImagePath() + strImage);
                        fieldTemporal.setDimID(dimension.getId());
                        field.addTemporal(fieldTemporal);
                    }
                }
            }
        }
    }
    
    /**
     * M�todo respons�vel por adicionar um objeto do tipo indicaor/measure para estar
     *  dispon�vel para esta defini��o.
     * @param text
     *      Objeto do tipo <code>String</code> contendo o nome do indicador
     * @param ID
     *      Primitiva do tipo <code>int</code> contendo o c�digo do indiciador
     * @param strImage
     *      Objeto do tipo <code>String</code> contendo um caminho/path para uma
     *  imagem v�lida/dispon�vel online que representar� esta dimens�o
     */
    public void addMeasure(String text, int ID, String strImage) {
        text = (text.indexOf('@') > -1) ? text.substring(1) : text;
        properties.getCube().addMeasure(new Measure(ID, text, properties.getImagePath() + strImage));
    }
    
    /**
     * M�todo respons�vel por adicionar um objeto do tipo fun��o agregadora para estar
     *  dispon�vel para esta defini��o.
     * @param text
     *      Objeto do tipo <code>String</code> contendo o nome do indicador
     * @param intID
     *      Primitiva do tipo <code>int</code> contendo o c�digo do indiciador
     * @param strImageIcon
     *      Objeto do tipo <code>String</code> contendo um caminho/path para uma
     *  imagem v�lida/dispon�vel online que representar� esta dimens�o
     */
    public void addAgregation(String text, int intID, String strImageIcon) {
        properties.addAgregation(new AgregationFunction(intID, text, properties.getImagePath() + strImageIcon));
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades relativas ao eixo x
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo x
     */
    public String getAxisX() {
        return (properties.isGraphOn()) ? "" : properties.getAxisXConcat();
    }
    
    /**
     * M�todo respons�vel por definir as propriedades relativas ao eixo x
     * @param strValuesX
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo x
     */
    public void setAxisX(String strValuesX) {
        if (!properties.isGraphOn()) {
            properties.setAxisXConcat(strValuesX);
            getDefinitionWorkspace().updateDefinition(DwAxis.AXIS_X);
        }
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades relativas ao eixo y
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo y
     */
    public String getAxisY() {
        return (properties.isGraphOn()) ? properties.getAxisXConcat() : properties.getAxisYConcat();
    }
    
    /**
     * M�todo respons�vel por definir as propriedades relativas ao eixo y
     * @param strValuesY
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo y
     */
    public void setAxisY(String strValuesY) {
        if (properties.isGraphOn()) {
            properties.setAxisXConcat(strValuesY);
            getDefinitionWorkspace().updateDefinition(DwAxis.AXIS_X_GRAPH);
        } else {
            properties.setAxisYConcat(strValuesY);
            getDefinitionWorkspace().updateDefinition(DwAxis.AXIS_Y);
        }
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades relativas ao eixo dos indicadores/measures
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo
     */
    public String getAxisM() {
        return properties.getAxisMConcat();
    }
    
    /**
     * M�todo respons�vel por definir as propriedades relativas ao eixo y
     * @param strValuesM
     *      Objeto do tipo <code>String</code> contendo as propriedades para o eixo y
     */
    public void setAxisM(String strValuesM) {
        properties.setAxisMConcat(strValuesM);
        getDefinitionWorkspace().updateDefinition( (properties.isGraphOn()) ? DwAxis.AXIS_MEASURE_GRAPH : DwAxis.AXIS_MEASURE );
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de gr�fico escolhido para esta defini��o
     * @return
     *      Primitiva do tipo <code>int</code> contendo o tipo de gr�fico
     */
    public int getGraphType() {
        return (properties.isGraphOn()) ? properties.getGraphType(): 0;
    }
    
    /**
     * M�todo respons�vel por definir o tipo de gr�fico desta defini��o
     * @param intGraphType
     *      Primitiva do tipo <code>int</code> contendo o tipo de gr�fico
     */
    public void setGraphType(int intGraphType) {
        properties.setGraphType(intGraphType);
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades do gr�fico
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades do gr�fico
     */
    public String getGraphProps() {
        return properties.getGraphProps();
    }
    
    /**
     * M�todo respons�vel por definir as propriedades y do gr�fico
     * @param strGraphProps
     *      Objeto do tipo <code>String</code> contendo as propriedades do gr�fico
     */
    public void setGraphProps(String strGraphProps) {
        properties.setGraphProps(strGraphProps);
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades para o eixo y do gr�fico
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades do gr�fico
     */
    public String getGraphYProps() {
        return properties.getGraphYProps();
    }
    
    /**
     * M�todo respons�vel por definir as propriedades para o eixo y do gr�fico
     * @param strGraphProps
     *      Objeto do tipo <code>String</code> contendo as propriedades do gr�fico
     */
    public void setGraphYProps(String strGraphProps) {
        properties.setGraphYProps(strGraphProps);
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public String getGraphY2Props() {
        return properties.getGraphY2Props();
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public void setGraphY2Props(String props) {
        properties.setGraphY2Props(props);
    }

    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public void setDrill(boolean blnViewList) {
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public boolean getDrill() {
        return false;
    }

    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public void setViewList(boolean blnViewList) {
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public boolean getViewList() {
        return false;
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public void setQtLevel(int intLevel) {
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public int getQtLevel() {
        return 0;
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public void setfrslevel(int nSet) {
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public int setfrslevel() {
        return 0;
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public int getfrslevel() {
        return 0;
    }
    
    /**
     * @deprecated depreciada na vers�o SigaDW R4, porque n�o era utilizada
     */
    @Deprecated
    public String getnomeatr(int index) {
        return "";
    }
    
    /**
     * M�todo que inicializa vari�veis para testes offline
     */
    private void loadMockProperties() {
        properties.setImagePath("http://bi-paulo/dw/themes/msiga/hdefinition");
        properties.setPropExtend("on");
        
        List<String> temporal = new ArrayList<String>();
        temporal.add("Ano");temporal.add("Periodo");temporal.add("Esta��o");temporal.add( "Semestre");temporal.add( "Quadrimestre");temporal.add( "Trimestre");temporal.add( "Bimestre");temporal.add( "Mes");temporal.add( "Quinzena");temporal.add( "Semana");temporal.add( "Dia");temporal.add( "Dia da Semana");temporal.add( "Dia do Ano");
        properties.setTemporal(temporal);
        
        List<GraphType> graph = new ArrayList<GraphType>();
        graph.add(new GraphType(0, "BARRA", properties.getImagePath() + "ic_barra.gif"));
        graph.add(new GraphType(1, "BARRA 3D", properties.getImagePath() + "ic_barra3d.gif"));
        graph.add(new GraphType(2, "COLUNA", properties.getImagePath() + "ic_coluna.gif"));
        graph.add(new GraphType(3, "COLUNA 3D", properties.getImagePath() + "ic_coluna3d.gif"));
        graph.add(new GraphType(4, "LINHA", properties.getImagePath() + "ic_linha.gif"));
        graph.add(new GraphType(5, "LINHA 3D", properties.getImagePath() + "ic_linha3d.gif"));
        graph.add(new GraphType(6, "PIZZA", properties.getImagePath() + "ic_pizza.gif"));
        properties.setGraphTypes(graph);

        List<Dialogy> dialogy = new ArrayList<Dialogy>();
        dialogy.add(new Dialogy("Propriedades - Eixo Y")); dialogy.add(new Dialogy("Propriedades (Eixo Y):")); dialogy.add(new Dialogy("Titulo:")); dialogy.add(new Dialogy("Escala:")); dialogy.add(new Dialogy("Base")); dialogy.add(new Dialogy("(meta):")); dialogy.add(new Dialogy("Escala Logaritmica")); dialogy.add(new Dialogy("Log(base):")); dialogy.add(new Dialogy("Escala Automatica")); dialogy.add(new Dialogy("Valor Minimo:")); dialogy.add(new Dialogy("Valor Maximo:")); dialogy.add(new Dialogy("Incremento:")); dialogy.add(new Dialogy("Eixo Y (adicional)")); dialogy.add(new Dialogy("Titulo:")); dialogy.add(new Dialogy("Escala:")); dialogy.add(new Dialogy("Base (meta):")); dialogy.add(new Dialogy("Escala Logaritmica")); dialogy.add(new Dialogy("Log(base):")); dialogy.add(new Dialogy("Escala Automatica")); dialogy.add(new Dialogy("Valor Minimo:")); dialogy.add(new Dialogy("Valor Maximo:")); dialogy.add(new Dialogy("Incremento:")); dialogy.add(new Dialogy("DrillDown: ")); dialogy.add(new Dialogy("Abrir em janelas separadas")); dialogy.add(new Dialogy("Enviar")); dialogy.add(new Dialogy("Cancelar")); dialogy.add(new Dialogy("Pontos por p�gina:"));
        dialogy.add(new Dialogy("Propriedades do Gr�fico")); dialogy.add(new Dialogy("Serie"));
        
        List<Dialogy> lstFonts = new ArrayList<Dialogy>();
        lstFonts.add(new Dialogy("Arial"));
        lstFonts.add(new Dialogy("Courier"));
        dialogy.add(new Dialogy("Fonte", lstFonts)); dialogy.add(new Dialogy("Tamanho")); dialogy.add(new Dialogy("Cor")); dialogy.add(new Dialogy("Cor de Fundo")); dialogy.add(new Dialogy("Fundo do Gr�fico")); dialogy.add(new Dialogy("Eixo X/Y"));
        
        List<Dialogy> lstAxisFonts = new ArrayList<Dialogy>();
        lstAxisFonts.add(new Dialogy("Arial Black"));
        lstAxisFonts.add(new Dialogy("Courier New"));
        dialogy.add(new Dialogy("Fonte", lstAxisFonts)); dialogy.add(new Dialogy("Tamanho")); dialogy.add(new Dialogy("Cor"));
        
        List<Dialogy> lstAxisIntens = new ArrayList<Dialogy>();
        lstAxisIntens.add(new Dialogy("Transparente"));
        lstAxisIntens.add(new Dialogy("Semi-Transparente"));
        dialogy.add(new Dialogy("Intensidade da Serie", lstAxisIntens));
        
        List<Dialogy> lstAxisSobr = new ArrayList<Dialogy>();
        lstAxisSobr.add(new Dialogy("50%"));
        lstAxisSobr.add(new Dialogy("100%"));
        dialogy.add(new Dialogy("Sobreposi��o da Serie", lstAxisSobr));
        
        dialogy.add(new Dialogy("Image de Fundo")); dialogy.add(new Dialogy("Exibir Valores")); dialogy.add(new Dialogy("Exibir Legenda")); dialogy.add(new Dialogy("T�tulo da Marca")); dialogy.add(new Dialogy("T�tulo da Marca")); dialogy.add(new Dialogy("Base"));
        dialogy.add(new Dialogy("Visibilidade no Drill Down")); dialogy.add(new Dialogy("Cor de Fundo")); dialogy.add(new Dialogy("Campo ")); dialogy.add(new Dialogy(" da dimens�o ")); dialogy.add(new Dialogy("Propriedades")); dialogy.add(new Dialogy("Temporal")); dialogy.add(new Dialogy("Sim")); dialogy.add(new Dialogy("N�o")); dialogy.add(new Dialogy("Origem DrillDown")); dialogy.add(new Dialogy("Participa do DrillDown")); dialogy.add(new Dialogy("Tipo de Gr�fico")); dialogy.add(new Dialogy("Totalizar")); dialogy.add(new Dialogy("Indicador")); dialogy.add(new Dialogy("Fun��o Agregadora"));
        dialogy.add(new Dialogy("Propriedades de Zonas")); dialogy.add(new Dialogy("Exibir Zonas")); dialogy.add(new Dialogy("T�tulo")); dialogy.add(new Dialogy("Faixa de Valores")); dialogy.add(new Dialogy("De")); dialogy.add(new Dialogy("At�")); dialogy.add(new Dialogy("Cor")); dialogy.add(new Dialogy("Primeira Zona")); dialogy.add(new Dialogy("Secunda Zona")); dialogy.add(new Dialogy("Terceira Zona")); dialogy.add(new Dialogy("do Cubo"));
        dialogy.add(new Dialogy("Para alterar estas propriedades clique com o bot�o direito do mouse"));
        
        properties.setDialogy(dialogy);
        properties.setDrillOn("Modo Drilldown (ON)");
        properties.setDrillOff("Modo Drilldown (OFF)");
        properties.setAxisX("Eixo X");
        properties.setAxisY("Eixo Y");
        properties.setAxisMeasures("Area de Indicadores");
        properties.setAxisDesc("Sum�rio da Defini��o");
        properties.setGraphTypeLabel("Tipo de Grafico");
        properties.setProps("Propriedades");
        properties.setDimension("Dimensoes");
        properties.setMeasures("Indicadores");
        properties.setGraph("Grafico");
        properties.setTable("Tabela");
        properties.setSession("1145571489");
        
        properties.setDrillDown("Drill Down");
        try {
            properties.setDrillParentImage(new ImageIcon(new URL(properties.getImagePath() + "ic_drilldown_parent.gif")));
            properties.setDrillChildImage(new ImageIcon(new URL(properties.getImagePath() + "ic_drilldown_child.gif")));
            
            properties.setTotalize("Totalizar");
            properties.setTotalizeImage(new ImageIcon(new URL(properties.getImagePath() + "ic_totalize.gif")));
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        properties.setAgregLabel("Fun��o Agregadora");
        properties.setSumario("Sum�rio");
        
        try {
            properties.setTreeLeaf(new ImageIcon(new URL(properties.getImagePath() + "ic_pastafechada.gif")));
            properties.setTreeOpen(new ImageIcon(new URL(properties.getImagePath() + "ic_pastaaberta.gif")));
            properties.setTreeClose(new ImageIcon(new URL(properties.getImagePath() + "ic_pastafechada.gif")));
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }
    
    private void mockInit() {
        this.setGraphType(5);
        this.setGraphYProps("Titulo Eixo Y;true;15;true;20;false;25;26;27;true;Titulo Y adicional;true;30;false;40;41;42;true;50");
        
        properties.setCube(new Cube(0, "VENDAS2", properties.getImagePath() + "ic_cubo.gif"));
        
        this.addDimension("#ANALISTAS", 0, "ic_dimensao.gif");
	this.addDimension("C�d. Analista", 7, "ic_atributo.gif");
	this.addDimension("Analista", 8, "ic_atributo.gif");
	this.addDimension("#CLIENTES", 0, "ic_dimensao.gif");
	this.addDimension("C�d. Cliente", 22, "ic_atributo.gif");
	this.addDimension("Estado", 24, "ic_atributo.gif");
	this.addDimension("Cliente", 23, "ic_atributo.gif");
	this.addDimension("#FAIXAS", 0, "ic_dimensao.gif");
	this.addDimension("Cod. Faixa", 20, "ic_atributo.gif");
	this.addDimension("Faixa de Resolu��o", 21, "ic_atributo.gif");
	this.addDimension("Tabela BP SX5", 19, "ic_atributo.gif");
	this.addDimension("#MODULOS", 0, "ic_dimensao.gif");
	this.addDimension("�rea", 12, "ic_atributo.gif");
	this.addDimension("M�dulo", 3, "ic_atributo.gif");
	this.addDimension("#PASSOS", 0, "ic_dimensao.gif");
	this.addDimension("C�d. Passo", 15, "ic_atributo.gif");
	this.addDimension("Passo", 16, "ic_atributo.gif");
	this.addDimension("#PERIODO", 0, "ic_dimensao.gif");
	this.addDimension("@Data Bops", 4, "ic_atributo.gif");
	this.addDimension("@Data Analista", 14, "ic_atributo.gif");
	this.addDimension("@Distribu�do", 13, "ic_atributo.gif");
	this.addDimension("#ROTINAS", 0, "ic_dimensao.gif");
	this.addDimension("Rotina", 18, "ic_atributo.gif");
	this.addDimension("C�d. Rotina", 17, "ic_atributo.gif");
	this.addDimension("#TIPO_OCORRENCIA", 0, "ic_dimensao.gif");
	this.addDimension("C�d. Ocorr�ncia", 1, "ic_atributo.gif");
	this.addDimension("Tipo Ocorr�ncia", 2, "ic_atributo.gif");
	this.addDimension("#VERSAO", 0, "ic_dimensao.gif");
	this.addDimension("Chave", 10, "ic_atributo.gif");
	this.addDimension("Vers�o", 11, "ic_atributo.gif");
	this.addDimension("Tabela", 9, "ic_atributo.gif");
	this.addMeasure("Quantidade de Bops", 31,"ic_indicador.gif");
	this.addMeasure("Prazo M�dio de Solu��o", 32,"ic_indicador.gif");
	this.addMeasure("Quant. Bops Distribuidos", 40,"ic_indicador.gif");
	this.addMeasure("Quant. Bops Resolvidos", 41,"ic_indicador.gif");
	this.addMeasure("Prazo de Distribui��o", 42,"ic_indicador.gif");
	this.addMeasure("Qtd. Bops Abertos", 51,"ic_indicador.gif");
	this.addMeasure("N�mero de Ocorr�ncias", 52,"ic_indicador.gif");
	this.addMeasure("Bops Rejeitados", 53,"ic_indicador.gif");
	this.addMeasure("EXCECOES DEVIDO A BASE ERRADA", 803,"ic_indicador.gif");
	this.addMeasure("@BOPS_VALID", -27,"ic_indicator_dinamico.gif");
	this.addMeasure("@PRAZO", -6,"ic_indicator_dinamico.gif");
        
        if (properties.isPropExtendOn()) {
        }
        
        this.addAgregation("Soma", 0, properties.getImagePath() + "ic_agreg_sum.gif");
        this.addAgregation("Contagem", 1, properties.getImagePath() + "ic_agreg_count.gif");
        this.addAgregation("Distin��o", 7, properties.getImagePath() + "ic_agreg_distinct.gif");
        this.addAgregation("M�dia", 2, properties.getImagePath() + "ic_agreg_average.gif");
        this.addAgregation("Minimo", 3, properties.getImagePath() + "ic_agreg_min.gif");
        this.addAgregation("M�ximo", 4, properties.getImagePath() + "ic_agreg_max.gif");
        this.addAgregation("M�dia Interna", 11, properties.getImagePath() + "ic_agreg_intern_average.gif");
        this.addAgregation("Acumulado", 12, properties.getImagePath() + "ic_agreg_acum.gif");
        this.addAgregation("Acumulado Hist.", 13, properties.getImagePath() + "ic_agreg_acum_hist.gif");
        this.addAgregation("Participa��o", 5, properties.getImagePath() + "ic_agreg_part.gif");
        this.addAgregation("Participa��o Total", 9, properties.getImagePath() + "ic_agreg_total_part.gif");
        this.addAgregation("Participa��o Global", 10, properties.getImagePath() + "ic_agreg_global_part.gif");
        
        properties.setAxisX("Eixo X");
        properties.setAxisY("Eixo Y");
        properties.setAxisMeasures("Indicadores");
        
        this.setAxisX("");
	this.setAxisY("(0|0)*12;(0|1)8;");
	this.setAxisM("(0|)31;(0|)41;(0|)-6;(0|)32");
    }
    
    /**
     * M�todo respons�vel por carregar todas as propriedades necess�rias para 
     *  esta defini��o oriundas da tag applet no AdvPL.
     */
    protected void loadProperties() {
        // imagePath
        String temp = getParameter("imagePath");
        if (temp != null) {
            properties.setImagePath(temp);
        }
        
        // PROPEXTEND - apresenta ou n�o propiedades extendidas dos indicadores
        temp = getParameter("PROPEXTEND");
        if (temp != null) {
            properties.setPropExtend(temp);
        }
        
        // GRAPH - verifica se � um gr�fico
        temp = getParameter("GRAPH");
        if (temp != null) {
            properties.setGraphOn(temp);
        } else {
            properties.setGraphOn("");
        }
        
        // TEMPORAL_X - carrega temporal
        for(int i=1; (temp = getParameter("TEMPORAL_"+i))!=null; i++) {
            properties.addTemporal(temp);
        }
        
        // GRAPHTYPES_X - carrega graphtypes
        for(int i = 0; (temp = getParameter("GRAPHTYPES_" + i))!=null; i++) {
            properties.addGraphType(new GraphType(i, getParameter("GRAPHTYPES_" + i),
                    properties.getImagePath() + getParameter("GRAPHTYPES_" + i + "_IMG")));
        }
        
        // DIALOGY_X - carrega DIALOGY
        for (int i = 1; (temp = getParameter("DIALOGY_" + i)) != null; i++) {
            if (getParameter("DIALOGY_" + i + "_1") != null) {
                List<Dialogy> lstChilds = new ArrayList<Dialogy>();
                String temp2 = "";
                for (int j = 1; (temp2 = getParameter("DIALOGY_" + i + "_" + j)) != null; j++) {
                    lstChilds.add(new Dialogy(getParameter("DIALOGY_" + i + "_" + j)));
                }
                properties.addDialogyWithChilds(temp, lstChilds);
            } else if (getParameter("DIALOGY_" + i + "_1_LABEL") != null) {
                List<Dialogy> lstChilds = new ArrayList<Dialogy>();
                String temp2 = "";
                for (int j = 1; (temp2 = getParameter("DIALOGY_" + i + "_" + j + "_LABEL")) != null; j++) {
                    lstChilds.add(new Dialogy(temp2, getParameter("DIALOGY_" + i + "_" + j + "_VALUE")));
                }
                properties.addDialogyWithChilds(temp, lstChilds);
            } else {
                properties.addDialogy(temp);
            }
        }
        
        // FUNC_AGREG_X - carrega FUNC_AGREG
        if (getParameter("FUNC_AGREG") != null) {
            properties.setAgregLabel(getParameter("FUNC_AGREG"));
        }
        for (int i = 1; (temp = getParameter("FUNC_AGREG_" + i)) != null; i++) {
            properties.addAgregation(new AgregationFunction(i, getParameter("FUNC_AGREG_" + i),
                    properties.getImagePath() + getParameter("FUNC_AGREG_" + i + "_IMG")));
        }
        
        // DRILLDOWN - carrega a propriedade DRILLDOWN
        temp = getParameter("DRILLDOWN");
        if (temp != null) {
            properties.setDrillDown(temp);
        }
        
        // DRILLPARENT_IMG - carrega a propriedade DRILLPARENT_IMG
        temp = getParameter("DRILLPARENT_IMG");
        if (temp != null) {
            try {
                properties.setDrillParentImage(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException mue) {
                System.err.println("Error - Image Url: " + properties.getImagePath() + temp);
                mue.printStackTrace();
            }
        }
        
        // DRILLCHILD_IMG - carrega a propriedade DRILLCHILD_IMG
        temp = getParameter("DRILLCHILD_IMG");
        if (temp != null) {
            try {
                properties.setDrillChildImage(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException mue) {
                System.err.println("Error - Image Url: " + properties.getImagePath() + temp);
                mue.printStackTrace();
            }
        }
        
        // TOTALIZE - carrega a propriedade TOTALIZE
        temp = getParameter("TOTALIZE");
        if (temp != null) {
            properties.setTotalize(temp);
        }
        
        // TOTALIZE_IMG - carrega a propriedade TOTALIZE_IMG
        temp = getParameter("TOTALIZE_IMG");
        if (temp != null) {
            try {
                properties.setTotalizeImage(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException mue) {
                System.err.println("Error - Image Url: " + properties.getImagePath() + temp);
                mue.printStackTrace();
            }
        }
        
        // DRILLON - carrega a propriedade DRILLON
        temp = getParameter("DRILLON");
        if (temp != null) {
            properties.setDrillOn(temp);
        }
        
        // DRILLOFF - carrega a propriedade DRILLOFF
        temp = getParameter("DRILLOFF");
        if (temp != null) {
            properties.setDrillOn(temp);
        }
        
        // EIXOX - carrega a propriedade EIXOX
        temp = getParameter("EIXOX");
        if (temp != null) {
            properties.setAxisX(temp);
        }
        
        // EIXOY - carrega a propriedade EIXOY
        temp = getParameter("EIXOY");
        if (temp != null) {
            properties.setAxisY(temp);
        }
        
        // AREAIND - carrega a propriedade AREAIND
        temp = getParameter("AREAIND");
        if (temp != null) {
            properties.setAxisMeasures(temp);
        }
        
        // TIPODEG - carrega a propriedade TIPODEG
        temp = getParameter("TIPODEG");
        if (temp != null) {
            properties.setGraphTypeLabel(temp);
        }
        
        // PROPS - carrega a propriedade PROPS
        temp = getParameter("PROPS");
        if (temp != null) {
            properties.setProps(temp);
        }
        
        // DIMENSOES - carrega a propriedade DIMENSOES
        temp = getParameter("DIMENSOES");
        if (temp != null) {
            properties.setDimension(temp);
        }
        
        // INDICADORES - carrega a propriedade INDICADORES
        temp = getParameter("INDICADORES");
        if (temp != null) {
            properties.setMeasures(temp);
        }
        
        // GRAFICO - carrega a propriedade GRAFICO
        temp = getParameter("GRAFICO");
        if (temp != null) {
            properties.setGraph(temp);
        }
        
        // TABELA - carrega a propriedade TABELA
        temp = getParameter("TABELA");
        if (temp != null) {
            properties.setTable(temp);
        }
        
        // SUMARIO - carrega a propriedade SUMARIO
        temp = getParameter("SUMARIO");
        if (temp != null) {
            properties.setSumario(temp);
        }
        
        // TREE_LEAF - carrega a propriedade TREE_LEAF
        temp = getParameter("TREE_LEAF");
        if (temp != null) {
            try {
                properties.setTreeLeaf(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException ex) {
                System.err.println("Error - Image TreeLeaf Url: " + properties.getImagePath() + temp);
                ex.printStackTrace();
            }
        }
        
        // TREE_OPEN - carrega a propriedade TREE_OPEN
        temp = getParameter("TREE_OPEN");
        if (temp != null) {
            try {
                properties.setTreeOpen(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException ex) {
                System.err.println("Error - Image TreeOpen Tree Url: " + properties.getImagePath() + temp);
                ex.printStackTrace();
            }
        }
        
        // TREE_CLOSE - carrega a propriedade TREE_CLOSE
        temp = getParameter("TREE_CLOSE");
        if (temp != null) {
            try {
                properties.setTreeClose(new ImageIcon(new URL(properties.getImagePath() + temp)));
            } catch (MalformedURLException ex) {
                System.err.println("Error - Image TreeClose Url: " + properties.getImagePath() + temp);
                ex.printStackTrace();
            }
        }
        
        // APPLETS_BGCOLOR - carrega a propriedade APPLETS_BGCOLORDW
        temp = getParameter("APPLETS_SELECTIONS");
        if (temp != null) {
            properties.setAppletsSelections(new Color(Integer.decode(temp)));
        }
        
        // APPLETS_BGCOLOR - carrega a propriedade APPLETS_BGCOLORDW
        temp = getParameter("APPLETS_BGCOLOR");
        if (temp != null) {
            properties.setAppletsBgcolor(new Color(Integer.decode(temp)));
        }
        
        // APPLETS_WORKSPACE_BGCOLOR - carrega a propriedade APPLETS_WORKSPACE_BGCOLOR
        temp = getParameter("APPLETS_WORKSPACE_BGCOLOR");
        if (temp != null) {
            properties.setAppletsWorkspaceBgcolor(new Color(Integer.decode(temp)));
        }
        
        // BUTTON_BGCOLOR - carrega a propriedade BUTTON_BGCOLOR
        temp = getParameter("BUTTON_BGCOLOR");
        if (temp != null) {
            properties.setButtonBgcolor(new Color(Integer.decode(temp)));
        }
        
        // BUTTON_BGCOLOR_DRILLDOWN - carrega a propriedade BUTTON_BGCOLOR_DRILLDOWN
        temp = getParameter("BUTTON_BGCOLOR_DRILLDOWN");
        if (temp != null) {
            properties.setButtonBgcolorDrillDown(new Color(Integer.decode(temp)));
        }
        
        // BUTTON_FONTCOLOR_DRILLDOWN - carrega a propriedade BUTTON_FONTCOLOR_DRILLDOWN
        temp = getParameter("BUTTON_FONTCOLOR_DRILLDOWN");
        if (temp != null) {
            properties.setButtonFontColorDrillDown(new Color(Integer.decode(temp)));
        }
        
        // BGCOLOR_DD_OK - carrega a propriedade BGCOLOR_DD_OK
        temp = getParameter("BGCOLOR_DD_OK");
        if (temp != null) {
            properties.setAcceptDropColor(new Color(Integer.decode(temp)));
        }
        
        // BGCOLOR_DD_NOK - carrega a propriedade BGCOLOR_DD_NOK
        temp = getParameter("BGCOLOR_DD_NOK");
        if (temp != null) {
            properties.setRejectDragColor(new Color(Integer.decode(temp)));
        }
        
        // DWSESSION - carrega a propriedade DWSESSION
        temp = getParameter("DWSESSION");
        if (temp != null) {
            properties.setSession(temp);
        }
    }
    
    /**
     * M�todo respons�vel por recuperar a interfece gr�fico principal da defini��o.
     * @return
     *      Objeto do tipo <code>DefinitionWorkspace</code> contendo a interface principal 
     */
    public DefinitionWorkspace getDefinitionWorkspace() {
        return definition;
    }
    
    /**
     * M�todo respons�vel por definir a interfece gr�fico principal da defini��o.
     * @param definition
     *      Objeto do tipo <code>DefinitionWorkspace</code> contendo a interface principal 
     */
    protected void setDefinition(DefinitionWorkspace definition) {
        this.definition = definition;
        
        // define a cor de fundo do gr�fico
        this.definition.setBackground(properties.getAppletsWorkspaceBgcolor());
    }    
}