package br.com.microsiga.sigadw.applet;

import br.com.microsiga.sigadw.desktop.DwDesktop;
import br.com.microsiga.sigadw.desktop.DwDesktopScenarioManager;
import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.BorderLayout;
import javax.swing.JApplet;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class DWScriptDesktop extends JApplet {
    
    protected DwDesktop m_desktop;
    
    public DWScriptDesktop() {
        super();
    }
    
    public void init() {
        System.out.println("==> Starting SigaDW Desktop <==");
        System.out.println("Microsiga Software S/A - http://www.microsiga.com.br");
        System.out.println("System proprieties:");
        System.out.println(System.getProperty("java.vendor"));
        System.out.println(System.getProperty("java.version"));
        System.out.println(System.getProperty("os.name"));
        System.out.println(System.getProperty("file.separator"));
        System.out.println(System.getProperty("line.separator"));
        
        // carrega as propriedades oriundas da tag applet
        this.loadProperties();
        
        m_desktop = new DwDesktop();
        
        setLayout(new BorderLayout());
        add(m_desktop, BorderLayout.CENTER);
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
    }
    
    public void addRoot(int nodeID, String strRootName) {
        this.addRoot(nodeID, strRootName, DwDesktopNode.dwDESKTOP_FIXED);
    }
    
    public void addRoot(int nodeID, String strRootName, int intNodeType) {
        DwDesktopNode desktop = new DwDesktopNode();
        desktop.setId(nodeID);
        desktop.setRootID(nodeID);
        desktop.setName(strRootName);
        desktop.setType(intNodeType);
        desktop.setImgCloseNode(DwDesktop.IMG_IC_TREE_PASTAFECHADA);
        desktop.setImgOpenNode(DwDesktop.IMG_IC_TREE_PASTAABERTA);
        
        try {    
            this.m_desktop.addNodeToRoot(desktop);
        } catch (Exception e) {
            System.err.println("Error during addRoot(int, String, int)");
            System.err.println("Node:" + nodeID + "," + strRootName + "," + intNodeType);
            e.printStackTrace();
        }
    }
    
    public void addRoot(int nodeID, int parentID, String strRootName, int intNodeType) {
        DwDesktopNode desktop = new DwDesktopNode();
        desktop.setId(nodeID);
        desktop.setRootID(nodeID);
        desktop.setName(strRootName);
        desktop.setType(intNodeType);
        desktop.setImgCloseNode(DwDesktop.IMG_IC_TREE_PASTAFECHADA);
        desktop.setImgOpenNode(DwDesktop.IMG_IC_TREE_PASTAABERTA);
        
        try {
            this.m_desktop.addNodeToRoot(desktop, parentID);
        } catch (Exception e) {
            System.err.println("Error during addRoot(int, int, String, int)");
            System.err.println("Node:" + nodeID + "," + parentID + "," + strRootName + "," + intNodeType);
            e.printStackTrace();
        }
    }
    
    public void addScenario(int intScenario, String strScenario) {
        this.m_desktop.createScenario(intScenario, strScenario, true, false);
    }
    
    public void addNode(String nodeName, int nodeID, int rootID, int parentID, int nodeType, boolean nodeEnabled,
            int imgSt0, int imgSt1, String cTipo, String cDescricao, String cConsDisp, boolean blnMaintAllow) {
        DwDesktopNode newNode = new DwDesktopNode();
        newNode.setId(nodeID);
        newNode.setName(nodeName);
        newNode.setDescricao(cDescricao);
        newNode.setRootID(rootID);
        newNode.setType(nodeType);
        newNode.setNodeEnabled(nodeEnabled);
        newNode.setImgCloseNode(imgSt0);
        newNode.setImgOpenNode(imgSt1);
        newNode.setQueryType(cTipo);
        newNode.setConsDisp(cConsDisp);
        newNode.setMaintAllow(blnMaintAllow);
        newNode.setScenarioId(DwDesktopScenarioManager.DEFAULT_SCENARIO_ID);
        
        try {
            this.m_desktop.updateDesktopFromDB(newNode, parentID);
        } catch (Exception e) {
            System.err.println("Error during addNode(String, int, int, int, int, boolean, int, int, String, String, String, boolean)");
            System.err.println("Node: " + nodeName + "," + nodeID + "," + rootID + "," + parentID + "," + nodeType + "," + nodeEnabled + "," + imgSt0 + "," + imgSt1 + "," + cTipo + "," + cDescricao + "," + cConsDisp + "," + blnMaintAllow);
            e.printStackTrace();
        }
    }
    
    public void expandNode(int nodeID, int rootID, int parentID, int nodeType) {
        DwDesktopNode newNode = new DwDesktopNode();
        newNode.setId(nodeID);
        newNode.setRootID(rootID);
        newNode.setType(nodeType);
        newNode.setScenarioId(DwDesktopScenarioManager.DEFAULT_SCENARIO_ID);
          
        try {
           this.m_desktop.expandDesktopNode(newNode);
        } catch (Exception e) {
            System.err.println("Error during expandNode(int, int, int, int)");
            System.err.println("Node: " + nodeID + "," + rootID + "," + parentID + "," + nodeType);
            e.printStackTrace();
        }
    }

    public void finalizedLoading() {
        this.m_desktop.updateDesktop();
    }
    
    private void loadProperties() {
        DesktopProperties properties = DesktopProperties.getInstance();
        
        String temp = getParameter("imagePath");
        if (temp != null) {
            properties.setImagePath(temp);
        }
        
        // DWSESSION - carrega a propriedade DWSESSION
        temp = getParameter("DWSESSION");
        if (temp != null) {
            properties.setSession(temp);
        }
        
        temp = getParameter("CREATE_NODE_CONN");
        if (temp != null) {
            properties.setMaxCreateNodeConn(Integer.parseInt(getParameter("CREATE_NODE_CONN")));
            properties.setMaxCreateNodeConnSleep(Integer.parseInt(getParameter("CREATE_NODE_SLEEP")));
        }
        
        // code base do applet
        properties.setCodeBase(this.getCodeBase());
        
        // LABEL_MY_FAVORITES - carrega a propriedade LABEL_MY_FAVORITES
        temp = getParameter("LABEL_MY_FAVORITES");
        if (temp != null) {
            properties.setLabelMyFavorites(temp);
        }
        
        // LABEL_DEFAULT_SCENARIO - carrega a propriedade LABEL_DEFAULT_SCENARIO
        temp = getParameter("LABEL_DEFAULT_SCENARIO");
        if (temp != null) {
            properties.setLabelDefaultScenario(temp);
        }
        
        // LABEL_SCENARIO_MAINTENAINCE - carrega a propriedade LABEL_SCENARIO_MAINTENAINCE
        temp = getParameter("LABEL_SCENARIO_MAINTENAINCE");
        if (temp != null) {
            properties.setLabelScenarioMaintenance(temp);
        }
        
        // LABEL_NEW_FOLDER - carrega a propriedade LABEL_NEW_FOLDER
        temp = getParameter("LABEL_NEW_FOLDER");
        if (temp != null) {
            properties.setLabelNewFolder(temp);
        }
        
        // LABEL_ADDING_FOLDER - carrega a propriedade LABEL_ADDING_FOLDER
        temp = getParameter("LABEL_ADDING_FOLDER");
        if (temp != null) {
            properties.setLabelAddingFolder(temp);
        }
        
        // LABEL_NEW_SCENARIO - carrega a propriedade LABEL_NEW_SCENARIO
        temp = getParameter("LABEL_NEW_SCENARIO");
        if (temp != null) {
            properties.setLabelNewScenario(temp);
        }
        
        // LABEL_ASK_DEL_SCENARIO - carrega a propriedade LABEL_ASK_DEL_SCENARIO
        temp = getParameter("LABEL_ASK_DEL_SCENARIO");
        if (temp != null) {
            properties.setLabelAskDelScenario(temp);
        }
        
        // LABEL_DEL_SCENARIO - carrega a propriedade LABEL_DEL_SCENARIO
        temp = getParameter("LABEL_DEL_SCENARIO");
        if (temp != null) {
            properties.setLabelDelScenario(temp);
        }
        
        // LABEL_URL_WALLPAPER - carrega a propriedade LABEL_URL_WALLPAPER
        temp = getParameter("LABEL_URL_WALLPAPER");
        if (temp != null) {
            properties.setLabelUrlWallpaper(temp);
        }
        
        // LABEL_WALLPAPER - carrega a propriedade LABEL_WALLPAPER
        temp = getParameter("LABEL_WALLPAPER");
        if (temp != null) {
            properties.setLabelWallpaper(temp);
        }
        
        // LABEL_NEW_LINK - carrega a propriedade LABEL_NEW_LINK
        temp = getParameter("LABEL_NEW_LINK");
        if (temp != null) {
            properties.setLabelNewLink(temp);
        }
        
        // LABEL_ADDING_LINK - carrega a propriedade LABEL_ADDING_LINK
        temp = getParameter("LABEL_ADDING_LINK");
        if (temp != null) {
            properties.setLabelAddingLink(temp);
        }
        
        // LABEL_TABLE - carrega a propriedade LABEL_TABLE
        temp = getParameter("LABEL_TABLE");
        if (temp != null) {
            properties.setLabelTable(temp);
        }
        
        // LABEL_GRAPH - carrega a propriedade LABEL_GRAPH
        temp = getParameter("LABEL_GRAPH");
        if (temp != null) {
            properties.setLabelGraph(temp);
        }
        
        // LABEL_FOLDER - carrega a propriedade LABEL_FOLDER
        temp = getParameter("LABEL_FOLDER");
        if (temp != null) {
            properties.setLabelFolder(temp);
        }
        
        // LABEL_UPPER - carrega a propriedade LABEL_UPPER
        temp = getParameter("LABEL_UPPER");
        if (temp != null) {
            properties.setLabelUpper(temp);
        }
        
        // LABEL_NEW - carrega a propriedade LABEL_NEW
        temp = getParameter("LABEL_NEW");
        if (temp != null) {
            properties.setLabelNew(temp);
        }
        
        // LABEL_ADDING_SCENARIO - carrega a propriedade LABEL_ADDING_SCENARIO
        temp = getParameter("LABEL_ADDING_SCENARIO");
        if (temp != null) {
            properties.setLabelAddScenario(temp);
        }
        
        // LABEL_FILE - carrega a propriedade LABEL_FILE
        temp = getParameter("LABEL_FILE");
        if (temp != null) {
            properties.setLabelFile(temp);
        }
        
        // LABEL_RENAME - carrega a propriedade LABEL_RENAME
        temp = getParameter("LABEL_RENAME");
        if (temp != null) {
            properties.setLabelRename(temp);
        }
        
        // LABEL_RENAMING - carrega a propriedade LABEL_RENAMING
        temp = getParameter("LABEL_RENAMING");
        if (temp != null) {
            properties.setLabelRenaming(temp);
        }
        
        // "LABEL_NEW_FILE" - carrega a propriedade LABEL_ADDING_FOLDER
        temp = getParameter("LABEL_NEW_FILE");
        if (temp != null) {
            properties.setLabelNewFile(temp);
        }
        
    }
    
}