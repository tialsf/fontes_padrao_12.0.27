package br.com.microsiga.sigadw.chart;

import ChartDirector.ChartViewer;
import ChartDirector.HotSpotAdapter;
import ChartDirector.HotSpotEvent;
import br.com.microsiga.sigadw.http.DWHttp;
import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import pv.util.PVURL;

/**
 * Classe respons�vel por ser a abstra��o das funcionalidades b�sicas de um
 *  painel para o gr�fico do SigaDW.
 * @author Paulo R. Vieira - TOTVS S/A
 * @since SigaDW R4 25 de Setembro de 2008
 */
public abstract class AbstractDwScriptChart extends JPanel implements ActionListener, ChartRequestObserver {
    private static String sURLBase;
    private static String sProgram;
    private static String sDWSession;
    private static String sErro;
    private static String sAction;
    private static String sURL;
    private static String sLoading;
    private static int intQuerySleep;
    private static int intQueryMaxTime;
    private static int intBlockGraphRequest;
    private static String sRequestError;
    private static String sError;
    private static String strImgZoomIn;
    private static String strImgZoomRest;
    private static String strImgZoomOut;
    private static String strImgExport;
    private static String strImgClose;
    private static String strURLExportGraph;
    private static String strChartDirLicense;
    private static String strLabelZoomIn;
    private static String strLabelZoomRest;
    private static String strLabelZoomOut;
    private static String strLabelExport;
    private static String strLabelClose;
    private static String strLabelNextDrill;
    private static String strLabelPrevDrill;
    //Propriedades da documenta��o do gr�fico. 
    private static String strImgDocument;
    private static String strLabelDocument; 
    private static String strURLDocument; 

    // actions gen�ricos (T�M QUE TER SEQU�NCIA ORDENADA E NUMERICA)
    protected static final int AC_ZOOM_IN       =  1;
    protected static final int AC_ZOOM_REST     =  2;
    protected static final int AC_ZOOM_OUT      =  3;
    protected static final int AC_EXPORT        =  4;
    protected static final int AC_DRILLDOWN     =  5;
    protected static final int AC_PREVDRILL     =  6;
    protected static final int AC_NEXTDRILL     =  7;
    protected static final int AC_CLOSEDRILL    =  8;
    protected static final int AC_DOCUMENT      =  9;
    protected static final int AC_GENERAL       = 99;
    
    private JPanel jpnlFooter;
    private Container cntRootPane;
    
    protected static final String PROPERTY_CHART = "dwChartProp";
    
    private DWRotatingCube warning;
    
    public AbstractDwScriptChart() {
        super();
    }
    
    /**
     * Adiciona um Chart/Gr�fico a tela
     * @param compAdded
     *      Component a ser adicionado
     */
    protected abstract void addChartContainer(JComponent compAdded);
    
    /**
     * Adiciona um Chart/Gr�fico de DrillDown na tela
     * @param compAdded
     *      Component a ser adicionado
     * @param actionType
     *      Tipo de a��o. Verificar as a��es ACTION_DRILLDOWN, ACTION_PREVPAGE e ACTION_NEXTPAGE
     */
    protected abstract void addChartContainer(JComponent compAdded, int actionType);
    
    /**
     * Atualiza o gr�fico corrente com um novo component
     * @param compRem
     *      Component a ser utilizado
     */
    protected abstract void updateChartContainer(JComponent compUpdated);
    
    /**
     * Remove o �ltimo Chart/Gr�fico da tela
     * @param compRem
     *      Component a ser removido
     */
    protected abstract void removeChartContainer();
    
    protected void addMainFooterButton(JButton jbtn) {
        this.getFooterContainer().add(jbtn);
        this.getFooterContainer().updateUI();
        this.getFooterContainer().validate();
    }
    
    public void init() {
        System.out.println("==> Starting SigaDW Chart <==");
        System.out.println("TOTVS S/A - http://www.totvs.com.br");
        System.out.println("System proprieties:");
        System.out.println(System.getProperty("java.vendor"));
        System.out.println(System.getProperty("java.version"));
        System.out.println(System.getProperty("os.name"));
        System.out.println(System.getProperty("file.separator"));
        System.out.println(System.getProperty("line.separator"));
        
        this.initializeComponents();
        this.doInitialLoading();
    }
    
    public void destroy() {
        ChartRequestHandler.getInstance().destroyInstance();
    }
    
    protected abstract void doInitialLoading();
    
    public void initializeComponents() {
        //TODO - CONFIGURA��O
//        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (IllegalAccessException ex) {
//            ex.printStackTrace();
//        } catch (ClassNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (UnsupportedLookAndFeelException ex) {
//            ex.printStackTrace();
//        } catch (InstantiationException ex) {
//            ex.printStackTrace();
//        }
        
        // defini��o de todo o layout principal do gr�fico
        this.setLayout(new BorderLayout());
        defineChartLayout();
    }
    
    /**
     * Define o layout geral do Applet de Gr�ficos do SigaDW
     */
    protected void defineChartLayout() {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        defineMainChartLayout();
        defineChartCommandsLayoutPanel();
        
        // painel com bot�es de comando
        defineChartCommandsPanel();
    }
    
    protected void defineMainChartLayout() {
        this.add(getMainChartLayoutPanel());
    }
    
    protected abstract JComponent getMainChartLayoutPanel();
    
    protected void defineChartCommandsLayoutPanel() {
        // cria��o do painel de footer, aonde ser�o adicionados bot�es
        this.jpnlFooter = new JPanel();
        this.getFooterContainer().setBackground(Color.WHITE);
        this.getFooterContainer().setLayout(new BoxLayout(this.getFooterContainer(), BoxLayout.X_AXIS));
        
        this.add(this.getFooterContainer());
    }
    
    //Cria bot�es de Comando do Gr�fico.
    protected void defineChartCommandsPanel() {
        try {
            JButton jbtn = new JButton(new ImageIcon(new URL(AbstractDwScriptChart.getZoomInImg())));
            jbtn.setActionCommand(String.valueOf(AC_ZOOM_IN));
            jbtn.addActionListener(this);
            jbtn.setToolTipText(AbstractDwScriptChart.strLabelZoomIn);
            this.addMainFooterButton(jbtn);

            jbtn = new JButton(new ImageIcon(new URL(AbstractDwScriptChart.getZoomRestImg())));
            jbtn.setActionCommand(String.valueOf(AC_ZOOM_REST));
            jbtn.addActionListener(this);
            jbtn.setToolTipText(AbstractDwScriptChart.strLabelZoomRest);
            this.addMainFooterButton(jbtn);

            jbtn = new JButton(new ImageIcon(new URL(AbstractDwScriptChart.getZoomOutImg())));
            jbtn.setActionCommand(String.valueOf(AC_ZOOM_OUT));
            jbtn.addActionListener(this);
            jbtn.setToolTipText(AbstractDwScriptChart.strLabelZoomOut);
            this.addMainFooterButton(jbtn);

            jbtn = new JButton(new ImageIcon(new URL(AbstractDwScriptChart.getExportImg())));
            jbtn.setActionCommand(String.valueOf(AC_EXPORT));
            jbtn.addActionListener(this);
            jbtn.setToolTipText(AbstractDwScriptChart.strLabelExport);
            this.addMainFooterButton(jbtn);
            
            //Verifica se existe documento associado a consulta. 
            if (!"".equals(AbstractDwScriptChart.getURLDocument())){
                jbtn = new JButton(new ImageIcon(new URL(AbstractDwScriptChart.getImgDocument())));
                jbtn.setActionCommand(String.valueOf(AC_DOCUMENT));
                jbtn.addActionListener(this);
                jbtn.setToolTipText(AbstractDwScriptChart.strLabelDocument);
                this.addMainFooterButton(jbtn);
            }       
            
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }
    
    public JComponent loadChart(String sURL){
        DWChart graf = new DWChart(AbstractDwScriptChart.getChartDirLicense(), sURL, AbstractDwScriptChart.getDWSession(), AbstractDwScriptChart.getQuerySleep(), AbstractDwScriptChart.getQueryMaxTime());
        graf.addChartRequestObserver(this);
        graf.setURLBase(AbstractDwScriptChart.getURLBase()+AbstractDwScriptChart.getProgram());
        
        return this.loadChart(graf);
    }
    
    protected JComponent loadChart(final DWChart dwChart, String sURL) {
        dwChart.loadChartParams(sURL);
        return this.loadChart(dwChart);
    }
    
    protected JComponent loadChart(final DWChart dwChart) {
        int intGraphType = dwChart.getintvalue("TIP_CHART", "0");
        final boolean blnGraphHorizOriented;
        
        final JPanel jpnlCharts = new JPanel();
        jpnlCharts.add(createChart(dwChart));
        
        JScrollPane jcplRet = new JScrollPane( jpnlCharts );
        String strTmp = dwChart.getstrvalue("TITLECHART", "Chart");
        jcplRet.setName((strTmp.length() > 25) ? strTmp.substring(0, 25)+"..." : strTmp);
        jcplRet.setToolTipText(strTmp);
        
        jcplRet.putClientProperty(PROPERTY_CHART, dwChart);
        
        if (intGraphType == DWChart.GRAPHTYPE_3D_HORIZ_BAR || intGraphType == DWChart.GRAPHTYPE_HORIZ_BAR) {
            blnGraphHorizOriented = true;
        } else {
            blnGraphHorizOriented = false;
        }
        
        AdjustmentListener scrollAdjustListener = new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                // TODO verificar o bloqueio em casos de pagina��o
                Adjustable adjScroll = e.getAdjustable();
                JScrollBar jscrBar = (JScrollBar) e.getSource();
                
                if ( dwChart.hasNextPage() && ( 
                        (!blnGraphHorizOriented && adjScroll.getValue() > 0 && adjScroll.getValue() == (adjScroll.getMaximum()-adjScroll.getVisibleAmount()))
                            || (blnGraphHorizOriented && jscrBar.getValue() == jscrBar.getMinimum())
                        ) ) {
                    adjScroll.setValue( adjScroll.getValue() - 30 );
                    jscrBar.setEnabled(false);
                    
                    try {
                        warning = DWRotatingCube.getInstance(sLoading, Color.BLUE, new Color(0x22EEEEEE, false), Color.BLACK, getMainChartLayoutPanel());
                        dwChart.handleNextPage();
                    } catch (ChartRequestException ex) {
                        Logger.getLogger("NextPage: " + AbstractDwScriptChart.class.getName()).log(Level.WARNING, ex.getMessage());
                    }
                    
                    jscrBar.setEnabled(true);
                } else if (dwChart.hasPrevPage() && (
                        (!blnGraphHorizOriented && jscrBar.getValue() == jscrBar.getMinimum())
                            || (blnGraphHorizOriented && adjScroll.getValue() > 0 && adjScroll.getValue() == (adjScroll.getMaximum()-adjScroll.getVisibleAmount()))
                        ) ) {
                    adjScroll.setValue( adjScroll.getValue() + 30 );
                    jscrBar.setEnabled(false);
                    
                    try {
                        warning = DWRotatingCube.getInstance(sLoading, Color.BLUE, new Color(0x22EEEEEE, false), Color.BLACK, getMainChartLayoutPanel());
                        dwChart.handlePreviousPage();
                    } catch (ChartRequestException ex) {
                        Logger.getLogger("PreviousPage: " + AbstractDwScriptChart.class.getName()).log(Level.WARNING, ex.getMessage());
                    }
                    
                    jscrBar.setEnabled(true);
                }
            }
        };
        
        if (dwChart.hasNextPage() || dwChart.hasPrevPage()) {
            switch (intGraphType) {
                case DWChart.GRAPHTYPE_3D_HORIZ_BAR:
                case DWChart.GRAPHTYPE_HORIZ_BAR:
                    jcplRet.getVerticalScrollBar().addAdjustmentListener(scrollAdjustListener);
                    jcplRet.getVerticalScrollBar().setValue(200);
                    jpnlCharts.setBorder(new EmptyBorder(100, 5, 100, 5));
                    break;
                case DWChart.GRAPHTYPE_3D_BAR:
                case DWChart.GRAPHTYPE_3D_LINE:
                case DWChart.GRAPHTYPE_BAR:
                case DWChart.GRAPHTYPE_LINE:
                    jcplRet.getHorizontalScrollBar().addAdjustmentListener(scrollAdjustListener);
                    jcplRet.getHorizontalScrollBar().setValue(200);
                    jpnlCharts.setBorder(new EmptyBorder(5, 100, 5, 200));
                    break;
            }
        }
        
        return jcplRet;
    }
    
    public JComponent createChart(DWChart dwChart) {
        int intChartType = dwChart.getintvalue("TIP_CHART", "0");

        JComponent m_panelret;
        if (dwChart.getstrvalue("DW_VALOR", "0").equals("1") && dwChart.getstrvalue("FIMSTRDW","NAO").equals("OK")) {
            if (intChartType == DWChart.GRAPHTYPE_PIE) {
                JComponent chart;
                m_panelret = new JPanel();
                m_panelret.setLayout(new BoxLayout(m_panelret, BoxLayout.X_AXIS));
                for (int i=0; i < dwChart.getintvalue("QTD_IND", "0"); i++) {
                    chart = getChartView(i, dwChart);
                    m_panelret.add(chart);
                }
                
                m_panelret.setSize(this.getWidth() * dwChart.getintvalue("QTD_IND", "0"), this.getHeight());
            } else {
                m_panelret = getChartView(0, dwChart);
                m_panelret.setSize(this.getWidth(), this.getHeight());
            }
        } else {
            m_panelret = getTextErro(sErro);
        }
        
        return m_panelret;
    }
    
    protected ChartViewer getChartView(int iIndex, final DWChart dwChart) {
        ChartViewer viewer = null;
        try {
            viewer = dwChart.init(iIndex + 1);

            if (dwChart.getstrvalue("DRILL_ON", "FALSE").toUpperCase().equals("TRUE")) {
                viewer.addHotSpotListener(new HotSpotAdapter() {

                    public void hotSpotClicked(HotSpotEvent e) {
                        e.setSource(dwChart);
                        handleDrillDown(e);
                    }
                });
            }
        } catch (ChartRequestException ex) {
            Logger.getLogger(AbstractDwScriptChart.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return viewer;
    }
    
    protected void handleDrillDown(HotSpotEvent e) {
        warning = DWRotatingCube.getInstance(sLoading, Color.BLUE, new Color(0x22EEEEEE, false), Color.BLACK, getMainChartLayoutPanel());
        
        DWChart chartOrigin = ((DWChart) e.getSource());
        String sURLParams = chartOrigin.getstrvalue("URLDRILLPARAMS", "");
        String sDrill = chartOrigin.getstrvalue("VAL_DRILL", "");
        String sParamValue = chartOrigin.getstrvalue("PARAMVALUEY", "");
        String sURLDD = "";
        String sValue = "";
        String sValueCod = "";
        
        if (chartOrigin.getintvalue("TIP_CHART", "0") == DWChart.GRAPHTYPE_PIE) {
            sValue = e.get("label");
        } else {
            sValue = e.get("xLabel");
        }
        
        if ("".equals(sValue)) {
            sValue = " ";
        }
        
        if (sDrill.length()>0)
            sValue = sDrill + ";" + sValue;
        
        try {
            sValueCod = java.net.URLEncoder.encode(sValue.replaceAll(DWChart.DELIM_LINHA, ";").replaceAll(DWChart.SEP_TITLE_EXP, ";"), "ISO-8859-1");
        } catch(UnsupportedEncodingException ex) {
            System.err.println(ex.getMessage());
        }
        
        sURLDD = chartOrigin.getURLBase() + sURLParams + sParamValue + chartOrigin.getstrvalue("PARAMDRILL", "") + sValueCod;
        
        this.addChartContainer(this.loadChart(sURLDD), AbstractDwScriptChart.AC_DRILLDOWN);
        
        warning.holdon();
        warning.end();
    }
    
    /**
     * Realiza a exporta��o do gr�fico
     */
    public void exportGraph() {
        try {
            warning = DWRotatingCube.getInstance(sLoading, Color.BLUE, new Color(0x22EEEEEE, false), Color.BLACK, getMainChartLayoutPanel());
            
            // string com a url base (htt://ip:porta/dw) e o programa (w_sigadw3...)
            String strHostProgram = sURLBase + sProgram;
            // string com a url de exporta��o do gr�fico
            String strUrl = strHostProgram + AbstractDwScriptChart.getExportGraphURL() + "&SESSIONID=" + AbstractDwScriptChart.getDWSession();
            
            // cria a url
            URL urlExport = new URL(strUrl);
            
            // cria o http
            DWHttp http = new DWHttp(urlExport);
            http.openRequest("POST", AbstractDwScriptChart.getDWSession());
            try {
                // passa os valores da exporta��o
                http.write("origem=JAVA&cbase64=" + URLEncoder.encode(getCurrentChart().toBase64String(), "ISO-8859-1"));
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
            http.closeRequest();
            
            // realiza o download do arquivo gerado na exporta��o do gr�fico
            PVURL.open(getChartRootPane(), strUrl + "&origem=JAVA", null);

            warning.holdon();
            warning.end();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Realiza a exporta��o do gr�fico
     */
    public void showDocument() {
            warning = DWRotatingCube.getInstance(sLoading, Color.BLUE, new Color(0x22EEEEEE, false), Color.BLACK, getMainChartLayoutPanel());   
        
            //string com a url base (htt://ip:porta/dw) e o programa (w_sigadw3...)
            String strHostProgram = sURLBase + sProgram;
            // string com a url de exporta��o do gr�fico
            String strUrl = strHostProgram + AbstractDwScriptChart.getURLDocument();
            PVURL.open(getChartRootPane(), strUrl + "&origem=JAVA;jsessionid=" + AbstractDwScriptChart.getDWSession() , null);
   
            warning.holdon();
            warning.end();
    }
    

    public static String getURLBase() {
        return sURLBase;
    }
    
    public static void setURLBase(String aSURLBase) {
        sURLBase = aSURLBase;
    }
    
    public static String getProgram() {
        return sProgram;
    }
    
    public static void setProgram(String aSProgram) {
        sProgram = aSProgram;
    }
    
    public static String getDWSession() {
        return sDWSession;
    }
    
    public static void setDWSession(String aSDWSession) {
        sDWSession = aSDWSession;
    }
    
    public static String getAction() {
        return sAction;
    }
    
    public static void setAction(String aSAction) {
        sAction = aSAction;
    }
    
    public static String getURL() {
        return sURL;
    }
    
    public static void setURL(String aSURL) {
        sURL = aSURL;
    }
    
    public static String getLoading() {
        return sLoading;
    }
    
    public static void setLoading(String aSLoading) {
        sLoading = aSLoading;
    }
    
    public static int getQuerySleep() {
        return intQuerySleep;
    }
    
    public static void setQuerySleep(int aIntQuerySleep) {
        intQuerySleep = aIntQuerySleep;
    }
    
    public static int getQueryMaxTime() {
        return intQueryMaxTime;
    }
    
    public static void setQueryMaxTime(int aIntQueryMaxTime) {
        intQueryMaxTime = aIntQueryMaxTime;
    }
    
    public static int getBlockGraphRequest() {
        return intBlockGraphRequest;
    }
    
    public static void setBlockGraphRequest(int aIntBlockGraphRequest) {
        intBlockGraphRequest = aIntBlockGraphRequest;
    }
    
    public static String getRequestError() {
        return sRequestError;
    }
    
    public static void setRequestError(String aSRequestError) {
        sRequestError = aSRequestError;
    }
    
    public static String getError() {
        return sError;
    }
    
    public static void setError(String aSError) {
        sError = aSError;
    }
    
    public static String getZoomInImg() {
        return strImgZoomIn;
    }
    
    public static void setZoomInImg(String aStrImgZoomIn) {
        strImgZoomIn = aStrImgZoomIn;
    }
    
    public static String getZoomRestImg() {
        return strImgZoomRest;
    }
    
    public static void setZoomRestImg(String aStrImgZoomRest) {
        strImgZoomRest = aStrImgZoomRest;
    }
    
    public static String getZoomOutImg() {
        return strImgZoomOut;
    }
    
    public static void setZoomOutImg(String aStrImgZoomOut) {
        strImgZoomOut = aStrImgZoomOut;
    }
    
    public static String getExportImg() {
        return strImgExport;
    }
    
    public static void setExportImg(String aStrImgExport) {
        strImgExport = aStrImgExport;
    }

    public static String getCloseImg() {
        return strImgClose;
    }
    
    public static void setCloseImg(String aStrImgClose) {
        strImgClose = aStrImgClose;
    }
    
    public static String getExportGraphURL() {
        return strURLExportGraph;
    }
    
    public static void setExportGraphURL(String aURLExportGraph) {
        strURLExportGraph = aURLExportGraph;
    }
    
    public static String getChartDirLicense() {
        return strChartDirLicense;
    }
    
    public static void setChartDirLicense(String aStrChartDirLicense) {
        strChartDirLicense = aStrChartDirLicense;
    }
    
    public static String getLabelZoomIn() {
        return strLabelZoomIn;
    }

    public static void setLabelZoomIn(String aStrLabelZoomIn) {
        strLabelZoomIn = aStrLabelZoomIn;
    }

    public static String getLabelZoomRest() {
        return strLabelZoomRest;
    }

    public static void setLabelZoomRest(String aStrLabelZoomRest) {
        strLabelZoomRest = aStrLabelZoomRest;
    }

    public static String getLabelZoomOut() {
        return strLabelZoomOut;
    }

    public static void setLabelZoomOut(String aStrLabelZoomOut) {
        strLabelZoomOut = aStrLabelZoomOut;
    }

    public static String getLabelExport() {
        return strLabelExport;
    }

    public static void setLabelExport(String aStrLabelExport) {
        strLabelExport = aStrLabelExport;
    }
    
    public static String getLabelClose() {
        return strLabelClose;
    }

    public static void setLabelClose(String aStrLabelClose) {
        strLabelClose = aStrLabelClose;
    }
    
    public static String getLabelNextDrill() {
        return strLabelNextDrill;
    }

    public static void setLabelNextDrill(String aStrLabelNextDD) {
        strLabelNextDrill = aStrLabelNextDD;
    }

    public static String getLabelPrevDrill() {
        return strLabelPrevDrill;
    }

    public static void setLabelPrevDrill(String aStrLabelPrevDD) {
        strLabelPrevDrill = aStrLabelPrevDD;
    }
    
    //Getters e Setters das propriedades da documenta��o. 
    public static String getImgDocument() {
        return strImgDocument;
    }

    public static void setImgDocument(String strImgDocument) {
        AbstractDwScriptChart.strImgDocument = strImgDocument;
    }

    public static String getLabelDocument() {
        return strLabelDocument;
    }

    public static void setLabelDocument(String strLabelDocument) {
        AbstractDwScriptChart.strLabelDocument = strLabelDocument;
    }

    public static String getURLDocument() {
        return strURLDocument;
    }

    public static void setURLDocument(String strURLDocument) {
        AbstractDwScriptChart.strURLDocument = strURLDocument;
    }
    
    
    public void actionPerformed(ActionEvent e) {
        this.handleClick(Integer.parseInt(e.getActionCommand()));
    }
    
    protected abstract DWChart getCurrentChart();
    
    protected abstract JScrollPane getCurrentComponent();
    
    protected void handleClick(int intActionCommand) {
        switch (intActionCommand) {
            case AC_ZOOM_IN:
                this.setZoomIn(5);
                break;
            case AC_ZOOM_REST:
                this.setZoomRestore();
                break;
            case AC_ZOOM_OUT:
                this.setZoomOut(5);;
                break;
            case AC_EXPORT:
                exportGraph();
                break;
            case AC_DOCUMENT:
                showDocument();
                break;
        }
    }
    
    protected void setZoomIn(int intPerc) {
        DWChart chart = this.getCurrentChart();
        chart.setZoomIn(intPerc);
        this.updateChartContainer(this.loadChart(chart));
    }
    
    protected void setZoomOut(int intPerc) {
        DWChart chart = this.getCurrentChart();
        chart.setZoomOut(intPerc);
        this.updateChartContainer(this.loadChart(chart));
    }
    
    protected void setZoomRestore() {
        DWChart chart = this.getCurrentChart();
        chart.setZoomRestore();
        this.updateChartContainer(this.loadChart(chart));
    }
    
    public JLabel getTextErro(String sErro) {
        JLabel txt = new JLabel(sErro);
        return txt;
    }

    public void chartRequestNotify(ChartRequest request) {
        this.updateChartContainer(this.loadChart(request.getOriginalChart()));
        warning.holdon();
        warning.end();
    }

    public JComponent getFooterContainer() {
        return jpnlFooter;
    }

    public Container getChartRootPane() {
        return cntRootPane;
    }

    public void setChartRootPane(Container cntRootPane) {
        this.cntRootPane = cntRootPane;
    }
    
}