package br.com.microsiga.sigadw.chart;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa uma requisi��o de processamento de uma consulta do tipo gr�fico
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 8 de Janeiro de 2007
 */
public class ChartRequest {
    
    private ChartRequestObserver chartRequestObserver;
    private DWChart originalChart;
    private ChartRequestTypesEnum requestType;
    private String sURLPage;
    private String sErro;
    private boolean blnSucess = false;
    
    /** Creates a new instance of ChartRequest */
    public ChartRequest() { }

    public AbstractMap<String, String> getResultData() {
        return getOriginalChart().getChartParams();
    }

    public void setResultData(AbstractMap<String, String> mapResultData) {
        this.getOriginalChart().setChartParams(mapResultData);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("chart=");
        sb.append(this.getOriginalChart());
        sb.append("&sURLPag=");
        sb.append(this.getURLPage());
        sb.append("&Type=");
        sb.append(this.getRequestType());
        sb.append("&sErro=");
        sb.append(this.getErro());
        sb.append("&jcmpResult=");
        sb.append(this.getOriginalChart().getChartParams());
        
        return sb.toString();
    }

    public void notifyRequestProcessed(boolean blnSucess) {
        this.setSucess(blnSucess);
        this.getChartRequestObserver().chartRequestNotify(this);
    }

    public ChartRequestObserver getChartRequestObserver() {
        return chartRequestObserver;
    }

    public void setChartRequestObserver(ChartRequestObserver chartRequestObserver) {
        this.chartRequestObserver = chartRequestObserver;
    }

    public DWChart getOriginalChart() {
        return originalChart;
    }

    public void setOriginalChart(DWChart originalChart) {
        this.originalChart = originalChart;
    }

    public ChartRequestTypesEnum getRequestType() {
        return requestType;
    }

    public void setRequestType(ChartRequestTypesEnum requestType) {
        this.requestType = requestType;
    }

    public String getURLPage() {
        return sURLPage;
    }

    public void setURLPage(String sURLPage) {
        this.sURLPage = sURLPage;
    }

    public String getErro() {
        return sErro;
    }

    public void setErro(String sErro) {
        this.sErro = sErro;
    }

    public boolean isSucess() {
        return blnSucess;
    }

    public void setSucess(boolean blnSucess) {
        this.blnSucess = blnSucess;
    }

}