package br.com.microsiga.sigadw.chart;

public class ChartRequestException extends Exception {
    
    public ChartRequestException() {
        super();
    }
    
    public ChartRequestException(String strMessage) {
        super(strMessage);
    }
    
}