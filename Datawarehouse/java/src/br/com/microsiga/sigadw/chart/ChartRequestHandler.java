package br.com.microsiga.sigadw.chart;

import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JComponent;

/**
 * Classe respons�vel por tratar de uma, e somente uma, requisi��o de uma consulta de gr�fico.
 * Ir� negar a requisi��o de uma consulta at� o t�rmino da consulta em processamento
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 8 de Janeiro de 2007
 */
public class ChartRequestHandler extends Thread {
    private boolean blnProcessing;
    
    private ChartRequest chartProcessing;
    
    private long timeLastRequest = System.currentTimeMillis();
    
    private int intBlockGraphRequest;
    
    private boolean stopRequestHandler;
    
    private boolean requestsBlocked;
    
    private static ChartRequestHandler singleton;
    
    /** Creates a new instance of ChartRequestHandler */
    private ChartRequestHandler() {
    }
    
    public static ChartRequestHandler getInstance() {
        if (singleton == null) {
            singleton = new ChartRequestHandler();
            singleton.start();
        }
        
        return singleton;
    }
    
    public void destroyInstance() {
        this.forceStopProcessing();
        try {
            this.join();
            singleton = null;
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    public void forceStopProcessing() {
        stopRequestHandler = true;
        this.blnProcessing = false;
    }
    
    public void run() {
        while (!stopRequestHandler) {
            if (blnProcessing) {
                String strURLPage = chartProcessing.getURLPage() + "&_forceReload=" + System.currentTimeMillis();
                DWChart chartRequest = chartProcessing.getOriginalChart();
                
                chartRequest.setChartParams(DWChartUtil.loadParams(strURLPage, chartRequest.getDwSession(), chartRequest.getQuerySleep(), chartRequest.getQueryMaxTime()));
                
                chartProcessing.setURLPage(strURLPage);
                chartProcessing.setSucess(true);
                chartProcessing.getChartRequestObserver().chartRequestNotify(this.chartProcessing);
                
                blnProcessing = false;
            }
            
            try {
                this.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public boolean handleRequest(ChartRequest request) {
        boolean blnReturn;
        
        if ((blnReturn = !this.blnProcessing) && !this.isRequestsBlocked()) {
            this.chartProcessing = request;
            
            this.blnProcessing = true;
            System.err.println("Request will be handled");
        } else {
            System.err.println("Request will not be handled");
        }
        
        return blnReturn;
    }
    
    protected boolean canRequest() {
        boolean blnReturn;
        long lngCurrent = System.currentTimeMillis();
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(this.timeLastRequest);
        calendar.add(Calendar.SECOND, this.intBlockGraphRequest);
        
        blnReturn = lngCurrent > calendar.getTimeInMillis();
        
        if (blnReturn) {
            this.timeLastRequest = System.currentTimeMillis();
        }
        
        return blnReturn;
    }
    
    public void setBlockGraphRequest(int intBlockGraphRequest) {
        this.intBlockGraphRequest = intBlockGraphRequest;
    }

    public boolean isRequestsBlocked() {
        return requestsBlocked;
    }

    public void setRequestsBlocked(boolean plRequestsBlocked) {
        requestsBlocked = plRequestsBlocked;
    }
    
}