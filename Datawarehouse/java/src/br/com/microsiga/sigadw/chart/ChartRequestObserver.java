package br.com.microsiga.sigadw.chart;

import ChartDirector.HotSpotEvent;

public interface ChartRequestObserver {
    
    public void chartRequestNotify(ChartRequest request);
    
}