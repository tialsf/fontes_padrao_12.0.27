package br.com.microsiga.sigadw.chart;

public enum ChartRequestTypesEnum {
    DRILLDOWN,
    PREVIOUSPAGE,
    NEXTPAGE;
}