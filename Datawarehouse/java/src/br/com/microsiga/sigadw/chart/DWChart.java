package br.com.microsiga.sigadw.chart;

import ChartDirector.*;
import br.com.microsiga.sigadw.applet.DWScriptChart;
import java.awt.*;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.*;
import java.util.List;

/**
 * Classe respons�vel por tratar de um gr�fico para o SigaDW. Cont�m as funcionalidades
 *  de integra��o com a biblioteca de gr�ficos ChartDirector e a montagem dos dados
 *  originados no SigaDW.
 * @author Paulo R. Vieira - TOTVS S/A
 * @since SigaDW R4
 */
public class DWChart extends ChartDirector.ChartViewer {
    public static final String DELIM           = "|";
    public static final String DELIM_LINHA     = "\n";
    public static final String SEP_TITLE       = "�";
    public static final String SEP_TITLE_EXP   = " � ";
    
    private double[][] m_data;
    private String[] m_labels;
    private List<byte[]> abGraphBytes = new ArrayList<byte[]>();
    
    private String fnt_nam = "Arial Bold";
    private String fnt_tam = "10";
    private String fnt_cor = "000000";
    
    private int intTitleSize;
    private int intLeftMargin;
    private int intTopMargin;
    private int intChartWidth;
    private int intChartHeight;
    private int intSerieTam;
    private int intXYTam;
    private double dblYMeta;
    private double dblPZVLini;
    private double dblPZVLFIM;
    private double dblSZVLini;
    private double dblSZVLFIM;
    private double dblTZVLini;
    private double dblTZVLFIM;
    private int intLegTam;
    private double dblValMin;
    private double dblValMax;
    private double dblValInterv;
    private int intChartRaio;
    private int intChartSerie;
    private int intXYItens;
    private double dblXYSobre;
    private double dblPercZoom;
    private int intGraphType;
    private int intBgcolor;
    
    public static final int GRAPHTYPE_HORIZ_BAR       = 0;
    public static final int GRAPHTYPE_3D_HORIZ_BAR    = 1;
    public static final int GRAPHTYPE_BAR             = 2;
    public static final int GRAPHTYPE_3D_BAR          = 3;
    public static final int GRAPHTYPE_LINE            = 4;
    public static final int GRAPHTYPE_3D_LINE         = 5;
    public static final int GRAPHTYPE_PIE             = 6;
    
    private AbstractMap<String, String> mapParams;
    private boolean lNextPage;
    private boolean lPrevPage;
    private String sURLBase;
    private String sURLNextPag;
    private String sURLPrevPag;
    private String sErro;
    private int intBlockGraphRequest;
    private String strChartDirLicense;
    private String strDwSession;
    private int intQuerySleep;
    private int intQueryMaxTime;
    
    private ChartRequestHandler requestHandler = ChartRequestHandler.getInstance();
    private ChartRequestObserver chartRequestObserver;
    private DWScriptChart windowOwner;
    
    public DWChart(String strLicense, String strInitURL, String strDwSession, int intQuerySleep, int intQueryMaxTime) {
        super();
        
        this.getRequestHandler().setBlockGraphRequest(intBlockGraphRequest);
        
        this.setChartDirLicense(strLicense);
        this.setDwSession(strDwSession);
        this.setQuerySleep(intQuerySleep);
        this.setQueryMaxTime(intQueryMaxTime);
        
        this.loadChartParams(strInitURL);
    }
    
    public ChartViewer init(int iIndex) throws ChartRequestException {
        int iQtdInd = getintvalue("QTDIND", "0");
        String sUltSerie = "";
        String sPrimSerie = "";
        String sDrill, sDrillCod;
        
        m_data = new double[iQtdInd+1][1];
        m_labels = new String[iQtdInd+1];
        
        // verifica se tiver mais de uma p�gina
        int iQtdSerie   = getintvalue("QTD_SERIE", "0");
        int iQtdPage    = getintvalue("QTD_PAG", "0");
        int iQtdProc    = getintvalue("QTD_PROC", "0");
        int iQtdItems   = getintvalue("QTD_ITENS", "0");
        
        if (iQtdSerie > iQtdPage) {
            lNextPage = iQtdSerie > iQtdProc;
            lPrevPage = (iQtdProc - iQtdItems) > 0;
        } else {
            lNextPage = false;
            lPrevPage = false;
        }
        
        try {
            sPrimSerie = getstrvalue("PRIMSERIE", "");
            sPrimSerie = sPrimSerie.replaceAll(DWChart.DELIM_LINHA, ";");
            sPrimSerie = sPrimSerie.replaceAll(DWChart.SEP_TITLE, ";");
            sPrimSerie = sPrimSerie.replaceAll("�", ";");
            sPrimSerie = java.net.URLEncoder.encode(sPrimSerie, "ISO-8859-1");

            sUltSerie = getstrvalue("ULTSERIE", "");
            sUltSerie = sUltSerie.replaceAll(DWChart.DELIM_LINHA, ";");
            sUltSerie = sUltSerie.replaceAll(DWChart.SEP_TITLE, ";");
            sUltSerie = sUltSerie.replaceAll("�", ";");
            sUltSerie = java.net.URLEncoder.encode(sUltSerie, "ISO-8859-1");
        } catch(java.io.UnsupportedEncodingException e) {
            System.err.println(e.getMessage());
        }

        sDrill = getstrvalue("VAL_DRILL", "");
        sDrillCod = "";
        try {
            sDrillCod = java.net.URLEncoder.encode(sDrill.replaceAll(DWChart.DELIM_LINHA, ";").replaceAll(DWChart.SEP_TITLE_EXP, ";"), "ISO-8859-1");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        sDrillCod = getstrvalue("PARAMDRILL", "") + sDrillCod;

        this.setURLNextPag(this.getURLBase() + getstrvalue("URLPAGEPARAMS", "") + getstrvalue("PARAMNEXTPAGE", "") + getstrvalue("PARAMVALUEY", "") + sUltSerie + sDrillCod);
        this.setURLPrevPag(this.getURLBase() + getstrvalue("URLPAGEPARAMS", "") + getstrvalue("PARAMPREVPAGE", "") + getstrvalue("PARAMVALUEY", "") + sPrimSerie + sDrillCod);
        this.setURLBase(sURLBase);
        this.setErro(sErro);
        
        return setLoad(iIndex);
    }
    
    protected ChartViewer setLoad(int iIndex) {
        ChartDirector.ChartViewer viewer = null;
        int iQtdInd = getintvalue("QTDIND", "0");
        int[] m_cor = new int[iQtdInd+1];
        int iwidth, iheight;
        boolean blnShowValue = getstrvalue("VERVALOR", "TRUE").toUpperCase().equals("TRUE");
        
        intGraphType = 0;
        
        // limpa a vari�vel com os bytes dos gr�ficos
        this.cleanGraphBytes();
        
        ChartDirector.Chart.setLicenseCode(getChartDirLicense());
        
        intGraphType = getintvalue("TIP_CHART", "0");
        
        iwidth = getintvalue("WIDTH", "100");
        iheight = getintvalue("HEIGHT", "100");
        
        iwidth += ((int) iwidth * this.getZoom());
        iheight += ((int) iheight * this.getZoom());
        
        if (intGraphType == GRAPHTYPE_HORIZ_BAR || intGraphType == GRAPHTYPE_3D_HORIZ_BAR) {
            setLabel(getstrvalue("AXIS_LABELS", "").replaceAll(SEP_TITLE, SEP_TITLE_EXP));
            intLeftMargin += getLabel().length;
        } else {
            setLabel(getstrvalue("AXIS_LABELS", "").replaceAll(SEP_TITLE, DELIM_LINHA));
        }
        
        if (intGraphType != GRAPHTYPE_PIE) {
            ChartDirector.XYChart c = new ChartDirector.XYChart(iwidth, iheight, Chart.goldColor(), -1, 2);
            
            c.addTitle(getstrvalue("TITLECHART", ""), getstrvalue("TITLE_FONT", fnt_nam), this.intTitleSize, getcolorvalue("TITLE_COLOR", fnt_cor), Chart.metalColor(0x9999ff), -1);
            c.setBackground(getcolorvalue("CORFUNDO", fnt_cor));
            this.setBgcolor(getcolorvalue("CORFUNDO", fnt_cor));
            
            if (getstrvalue("IMAGEM", "").equals("")) {
                c.setPlotArea(intLeftMargin, intTopMargin, intChartWidth, intChartHeight, getcolorvalue("CORGRAF", fnt_cor), getcolorvalue("CORGRAF", fnt_cor), -1);
            } else {
                URL uURL;
                try {
                    uURL = new URL(this.getURLBase());
                    c.setSearchPath4(uURL);
                } catch(MalformedURLException e) {
                    System.err.println("URL -> " + e.getMessage());
                    e.printStackTrace();
                }   
                c.setBgImage(getstrvalue("IMAGEM", ""), Chart.Center);
                c.setPlotArea(intLeftMargin, intTopMargin, intChartWidth, intChartHeight, getcolorvalue("CORGRAF", fnt_cor), getcolorvalue("CORGRAF", fnt_cor), -1).setBackground2(getstrvalue("IMAGEM", ""), Chart.Center);
            }
            
            int iIntens = intXYItens;
            
            for(int i=1;i<=iQtdInd;i++) {
                setData(getstrvalue("SERIE_DATA_"+Integer.toString(i), ""), i);
                m_cor[i] = retcor(new Color(getintvalue("SERIE_COR_"+Integer.toString(i), "")), iIntens);
            }
            c.yAxis().setLabelFormat(getstrvalue("SERIE_FORMAT", "{value}"));
            if (intGraphType >= GRAPHTYPE_HORIZ_BAR && intGraphType <= GRAPHTYPE_3D_BAR) {
                BarLayer layer = c.addBarLayer2(Chart.Side, iQtdInd);
                
                for(int i=1;i<=iQtdInd;i++) {
                    layer.addDataSet(getData(i), m_cor[i], getstrvalue("SERIE_"+Integer.toString(i), ""));
                    if(blnShowValue){
                        boolean blnFormatoUnico = getstrvalue("TZFORMUNIC", "FALSE").toUpperCase().equals("TRUE");
                        if(blnFormatoUnico){
                            layer.setAggregateLabelStyle(getstrvalue("SERIEFONTE", ""), intSerieTam, getcolorvalue("SERIECOR", ""));	
                            layer.setAggregateLabelFormat("{value|".concat(getstrvalue("TZDECIMAL", "0")).concat(".,}"));
			} else {
                            layer.getDataSet(i-1).setDataLabelStyle(getstrvalue("SERIEFONTE", ""), intSerieTam, getcolorvalue("SERIECOR", ""));
                            layer.getDataSet(i-1).setDataLabelFormat(getstrvalue("SERIE_FORMAT_"+Integer.toString(i), "{value}"));	
			}
                    }
                }
                
                if (intGraphType == GRAPHTYPE_3D_HORIZ_BAR || intGraphType == GRAPHTYPE_3D_BAR){
                    layer.setBarShape(Chart.CircleShape);
                    layer.set3D(10);
                }else{
                    for(int i=1;i<=iQtdInd;i++)
                        layer.setBarShape(Chart.CircleShape, i-1);
                    layer.set3D(0); // permitir uso do CircleShape em grafico 2D
                }
                
                layer.setBarGap(0.25);
                
                if (getintvalue("BARWIDTH", "0") > 0) {
                    layer.setBarWidth(getintvalue("BARWIDTH", "30"));
                }
                
                layer.setOverlapRatio(dblXYSobre/100);
            }
            
            if (intGraphType == GRAPHTYPE_LINE || intGraphType == GRAPHTYPE_3D_LINE) {
                LineLayer layer = c.addLineLayer2();
                
                for(int i=1;i<=iQtdInd;i++){
                    layer.addDataSet(getData(i), m_cor[i], getstrvalue("SERIE_"+Integer.toString(i), "")).setDataSymbol(Chart.SquareSymbol, 9);
                    
                    if (blnShowValue){
                        layer.getDataSet(i-1).setDataLabelStyle(getstrvalue("SERIEFONTE", ""), intSerieTam, getcolorvalue("SERIECOR", ""));
                        layer.getDataSet(i-1).setDataLabelFormat(getstrvalue("SERIE_FORMAT_"+Integer.toString(i), "{value}"));
                    }
                }
                
                if(intGraphType == GRAPHTYPE_3D_LINE)
                    layer.set3D(5, 5);
            }
            
            c.xAxis().setLabels(getLabel());
            c.xAxis().setLabelStyle(getstrvalue("XYFONT", fnt_nam), intXYTam, getcolorvalue("XYCOR", fnt_cor));
            c.xAxis().setTitle(getstrvalue("LABEL", ""), getstrvalue("XYFONT", fnt_nam), intXYTam, getcolorvalue("XYCOR", fnt_cor));
            
            c.yAxis().setLabelStyle(getstrvalue("XYFONT", fnt_nam), intXYTam, getcolorvalue("XYCOR", fnt_cor));
            c.yAxis().setTitle(getstrvalue("INDLABEL", ""), getstrvalue("XYFONT", fnt_nam), intXYTam, getcolorvalue("XYCOR", fnt_cor));
            if(getstrvalue("YBASE", "FALSE").toUpperCase().equals("TRUE")) {
                c.yAxis().addMark(dblYMeta, getcolorvalue("XYCOR", fnt_cor), getstrvalue("YTITMARCA", ""), getstrvalue("XYFONT", fnt_nam), intXYTam);
            }
            
            if(getstrvalue("VERZONA", "FALSE").toUpperCase().equals("TRUE")) {
                c.yAxis().addZone(dblPZVLini, dblPZVLFIM, getcolorvalue("PZCOR", fnt_cor));
                c.yAxis().addZone(dblSZVLini, dblSZVLFIM, getcolorvalue("SZCOR", fnt_cor));
                c.yAxis().addZone(dblTZVLini, dblTZVLFIM, getcolorvalue("TZCOR", fnt_cor));
                LegendBox LegMarc = c.addLegend(100, intTopMargin, false, getstrvalue("LEGFONTE", fnt_nam), intLegTam);
                LegMarc.setBackground(Chart.Transparent, Chart.Transparent);
                LegMarc.addKey(getstrvalue("PZTIT", ""), getcolorvalue("PZCOR", fnt_cor));
                LegMarc.addKey(getstrvalue("SZTIT", ""), getcolorvalue("SZCOR", fnt_cor));
                LegMarc.addKey(getstrvalue("TZTIT", ""), getcolorvalue("TZCOR", fnt_cor));
            }
            
            if (getstrvalue("YESCLOG", "FALSE").toUpperCase().equals("TRUE"))
                c.yAxis().setLogScale(dblValMin, dblValMax);
            else
                c.yAxis().setLinearScale(dblValMin, dblValMax, dblValInterv);
            
            if( (getstrvalue("VERLEG", "TRUE").toUpperCase().equals("TRUE")) || (getstrvalue("VERZONA", "FALSE").toUpperCase().equals("TRUE")) )
                c.addLegend((intLeftMargin + intChartWidth + 50), intTopMargin, true, getstrvalue("LEGFONTE", fnt_nam), intLegTam);
            
            if (intGraphType == GRAPHTYPE_HORIZ_BAR || intGraphType == GRAPHTYPE_3D_HORIZ_BAR) {
                c.swapXY(true);
            }
            
            c.layout();
            viewer = new ChartDirector.ChartViewer();
            viewer.setImage(c.makeImage());
            this.addGraphBytes(c.makeChart2(Chart.JPG));
            if (getstrvalue("DRILL_ON", "FALSE").toUpperCase().equals("TRUE")) {
                viewer.setHotSpotCursor(new Cursor(Cursor.HAND_CURSOR));
            }
            viewer.setImageMap(c.getHTMLImageMap("clickable", "", "title='{xLabel}: " + getstrvalue("SERIE_FORMAT", "{value}") + "'"));
            //viewer.setSize(new Dimension(iwidth, iheight));
        } else if (intGraphType == GRAPHTYPE_PIE) {
            setLabel(getstrvalue("AXIS_LABELS", "").replaceAll("�","\n"));
            
            for(int i=1;i<=iQtdInd;i++) {
                setData(getstrvalue("SERIE_DATA_"+Integer.toString(i), ""), i);
            }
            
            PieChart c = new PieChart(iwidth, iheight, Chart.goldColor(), -1, 2);
            
            c.addTitle(getstrvalue("TITLECHART", ""), getstrvalue("TITLE_FONT", fnt_nam), this.intTitleSize, getcolorvalue("TITLE_COLOR", fnt_cor), Chart.metalColor(0x9999ff), -1);
            c.setPieSize(intLeftMargin + intChartWidth, intTopMargin + intChartHeight, intChartRaio);
            c.set3D(15);
            c.setData(getData(iIndex), getLabel());
            
            String sTexto = "";
            if(getintvalue("QTD_SERIE", "0") > getintvalue("QTD_PAG", "0"))
                sTexto = getstrvalue("MSGPIZZA", "") + "\n";
            sTexto = sTexto + getstrvalue("TXTATRIB", "") + getstrvalue("LABEL", "") + "\n" + getstrvalue("TXTINDIC", "") + getstrvalue("SERIE_"+Integer.toString(iIndex), "");
            
            c.addTitle2(Chart.Bottom, sTexto);
            for( int i=0; i<getData(iIndex).length; i++)
                c.setExplode(i);
            
            if (intXYItens>0)
                c.setColors(Chart.transparentPalette);
            
            if (getstrvalue("VERVALOR", "TRUE").toUpperCase().equals("TRUE")) {
                c.setLabelLayout(Chart.SideLayout);
                c.setLabelFormat("{label}: " + getstrvalue("SERIE_FORMAT_"+Integer.toString(iIndex), "{value}"));
                c.setLabelStyle(getstrvalue("LEGFONTE", "Arial"), getintvalue("LEGTAM", "10"), getintvalue("LEGCOR", "-50"));
                c.setLabelStyle().setBackground(Chart.Transparent, Chart.CColor(new Color(0)));
            } else
                c.setLabelStyle("", 8, Chart.Transparent);
            
            this.setBgcolor(getcolorvalue("CORFUNDO", fnt_cor));
            c.setBackground(getcolorvalue("CORFUNDO", fnt_cor));
            
            if (!getstrvalue("IMAGEM", "").equals("")) {
                URL uURL;
                try {
                    uURL = new URL(this.getURLBase());
                    c.setSearchPath4(uURL);
                } catch(MalformedURLException e) {
                    System.err.println("URL -> " + e.getMessage());
                    e.printStackTrace();
                }
                c.setBgImage(getstrvalue("IMAGEM", ""), Chart.Center);
            }
            
            if( (getstrvalue("VERLEG", "TRUE").toUpperCase().equals("TRUE")) ) {
                c.addLegend(intLeftMargin + intChartWidth + intChartRaio + intChartSerie, intTopMargin, true, getstrvalue("LEGFONTE", fnt_nam), intLegTam);
            }
            
            viewer = new ChartDirector.ChartViewer();
            viewer.setImage(c.makeImage());
            this.addGraphBytes(c.makeChart2(Chart.JPG));
            if (getstrvalue("DRILL_ON", "FALSE").toUpperCase().equals("TRUE")) {
                viewer.setHotSpotCursor(new Cursor(Cursor.HAND_CURSOR));
            }
            viewer.setImageMap(c.getHTMLImageMap("clickable", "", "title='{label}: " + getstrvalue("SERIE_FORMAT_"+Integer.toString(iIndex), "{value}") + "'"));
        }
        
        return viewer;
    }
    
    public void setData(String sData, int x) {
        int y = 0;
        StringTokenizer stData = new StringTokenizer(sData, DELIM);
        m_data[x] = new double[stData.countTokens()];
        while(stData.hasMoreTokens()) {
            m_data[x][y] = Double.parseDouble(stData.nextToken());
            y++;
        }
    }
    
    public double[] getData(int x) {
        return m_data[x];
    }
    
    public void setLabel(String sLabel) {
        StringTokenizer stLabel = new StringTokenizer(sLabel, DELIM);
        
        m_labels = new String[stLabel.countTokens()];
        for (int i = 0; stLabel.hasMoreTokens(); i++) {
            m_labels[i] = stLabel.nextToken();
        }
    }
    
    public String[] getLabel() {
        return m_labels;
    }
    
    public int getcolorvalue(String sItem, String sPadrao) {
        return Chart.CColor(Color.decode(getstrvalue(sItem, sPadrao)));
    }
    
    public String getNameFont(String sValor) {
        String sRet = "";
        StringTokenizer stValor = new StringTokenizer(sValor, DELIM);
        if(stValor.countTokens() == 3) {
            sRet = stValor.nextToken() + " ";
            sRet = sRet + stValor.nextToken();
        }
        
        return sRet;
    }
    
    public int getSizeFont(String sValor) {
        int iRet = 10;
        StringTokenizer stValor = new StringTokenizer(sValor, DELIM);
        if(stValor.countTokens() == 3) {
            stValor.nextToken();
            stValor.nextToken();
            iRet = Integer.parseInt(stValor.nextToken());
        }
        return iRet;
    }
    
    public void vervetor(String[] str, boolean lzera, String sItem) {
        for(int i=0; i<str.length; i++) {
            if(lzera)
                str[i] = " ";
        }
    }
    
    public String getItemStr(String sItem, int nItem) {
        StringTokenizer stDado = new StringTokenizer(sItem, DELIM);
        String sValue = "";
        int i = 1;
        if(nItem > stDado.countTokens())
            nItem = 0;
        
        for(i=1;i<=nItem;i++) {
            sValue = stDado.nextToken();
        }
        return sValue;
    }
    
    public int retcor(Color cCor, int aAlpha) {
        int iRed   = cCor.getRed();
        int iGreen = cCor.getGreen();
        int iBlue  = cCor.getBlue();
        int iAlpha = aAlpha;
        
        Color newCor = new Color(iRed, iGreen, iBlue, iAlpha);
        
        return newCor.getRGB();
    }
    
    public void setURLBase(String sValue) {
        sURLBase = sValue;
    }
    
    public String getURLBase() {
        return sURLBase;
    }
    
    public void setVecRead(AbstractMap<String, String> vRead) {
        this.setChartParams(vRead);
        this.defineVecread();
    }
    
    public List<byte[]> getGraphBytes() {
        return abGraphBytes;
    }
    
    public void cleanGraphBytes() {
        abGraphBytes.clear();
    }
    
    public void addGraphBytes(byte[] bExp) {
        abGraphBytes.add(bExp);
    }
    
    public double getZoom() {
        return this.dblPercZoom;
    }
    
    protected void defineVecread() {
        intTitleSize    = Integer.parseInt(getstrvalue("TITLE_SIZE", fnt_tam));
        intLeftMargin   = getintvalue("LEFT_MARGIN", "25");
        intTopMargin    = getintvalue("TOP_MARGIN", "15");
        intChartWidth   = getintvalue("CHART_WIDTH", "100");
        intChartHeight  = getintvalue("CHART_HEIGHT", "100");
        intSerieTam     = getintvalue("SERIETAM", fnt_tam);
        intXYTam        = getintvalue("XYTAM", fnt_tam);
        dblYMeta        = getdoublevalue("YMETA", "0");
        dblPZVLini      = getdoublevalue("PZVLINI", "0");
        dblPZVLFIM      = getdoublevalue("PZVLFIM", "0");
        dblSZVLini      = getdoublevalue("SZVLINI", "0");
        dblSZVLFIM      = getdoublevalue("SZVLFIM", "0");
        dblTZVLini      = getdoublevalue("TZVLINI", "0");
        dblTZVLFIM      = getdoublevalue("TZVLFIM", "0");
        intLegTam       = getintvalue("LEGTAM", fnt_tam);
        dblValMin       = getdoublevalue("VAL_MIN","0");
        dblValMax       = getdoublevalue("VAL_MAX", "0");
        dblValInterv    = getdoublevalue("VAL_INTERV", "10");
        intChartRaio    = getintvalue("CHART_RAIO", "100");
        intChartSerie   = getintvalue("CHART_SERIE", "100");
        intXYItens      = getintvalue("XYINTENS", "0");
        dblXYSobre      = getdoublevalue("XYSOBRE", "0");
    }
    
    protected void redefineVecread() {
        intTitleSize    = intTitleSize + (int)(intTitleSize * this.getZoom());
        intLeftMargin   = intLeftMargin + (int)(intLeftMargin * this.getZoom());
        intTopMargin    = intTopMargin + (int)(intTopMargin * this.getZoom());
        intChartWidth   = intChartWidth + (int)(intChartWidth * this.getZoom());
        intChartHeight  = intChartHeight + (int)(intChartHeight * this.getZoom());
        intSerieTam     = intSerieTam + (int)(intSerieTam * this.getZoom());
        intXYTam        = intXYTam + (int)(intXYTam * this.getZoom());
        intLegTam       = intLegTam + (int)(intLegTam * this.getZoom());
        intChartRaio    = intChartRaio + (int)(intChartRaio * this.getZoom());
        intChartSerie   = intChartSerie + (int)(intChartSerie * this.getZoom());
    }

    public int getBgcolor() {
        return intBgcolor;
    }

    public void setBgcolor(int intBgcolor) {
        this.intBgcolor = intBgcolor;
    }

    public boolean hasNextPage() {
        return lNextPage;
    }

    public boolean hasPrevPage() {
        return lPrevPage;
    }
    
    public double getGraphZoomPerc() {
        return dblPercZoom;
    }
    
    public void setGraphZoomPercente(double dblPerc) {
        this.dblPercZoom = dblPerc;
    }
    
    public void setZoomIn(int intPerc) {
        this.setZoom(intPerc);
    }
    
    public void setZoomOut(int intPerc) {
        this.setZoom(intPerc * -1);
    }
    
    public void setZoomRestore() {
        setGraphZoomPercente(0.0);
        this.setZoom(0.0);
    }
    
    public void setZoom(double dblPerc) {
        int intWidth;
        int intHeight;
        
        this.dblPercZoom += dblPerc / 100.0;
        
        intWidth = getintvalue("WIDTH", "100");
        intHeight = getintvalue("HEIGHT", "100");
        
        intWidth += (int) intWidth * dblPerc;
        intHeight += (int) intHeight * dblPerc;
        
        intWidth += (int) intWidth * dblPerc;
        intHeight += (int) intHeight * dblPerc;
        
        defineVecread();
        redefineVecread();
    }

    public String getURLNextPag() {
        return sURLNextPag;
    }

    public void setURLNextPag(String sURLNextPag) {
        this.sURLNextPag = sURLNextPag;
    }

    public String getURLPrevPag() {
        return sURLPrevPag;
    }

    public void setURLPrevPag(String sURLPrevPag) {
        this.sURLPrevPag = sURLPrevPag;
    }

    public String getErro() {
        return sErro;
    }

    public void setErro(String sErro) {
        this.sErro = sErro;
    }
    
    public void loadChartParams(String sURL) {
        this.setVecRead( DWChartUtil.loadParams(sURL, getDwSession(), getQuerySleep(), getQueryMaxTime()) );
    }
    
    public String getstrvalue(String sItem, String sPadrao) {
        return DWChartUtil.getstrvalue(sItem, sPadrao, this.getChartParams());
    }
    
    public double getdoublevalue(String sItem, String sPadrao) {
        return DWChartUtil.getdoublevalue(sItem, sPadrao, this.getChartParams());
    }
    
    public int getintvalue(String sItem, String sPadrao) {
        return DWChartUtil.getintvalue(sItem, sPadrao, this.getChartParams());
    }

    public String getDwSession() {
        return strDwSession;
    }

    public void setDwSession(String strDwSession) {
        this.strDwSession = strDwSession;
    }

    public int getQuerySleep() {
        return intQuerySleep;
    }

    public void setQuerySleep(int intQuerySleep) {
        this.intQuerySleep = intQuerySleep;
    }

    public int getQueryMaxTime() {
        return intQueryMaxTime;
    }

    public void setQueryMaxTime(int intQueryMaxTime) {
        this.intQueryMaxTime = intQueryMaxTime;
    }

    public String getChartDirLicense() {
        return strChartDirLicense;
    }

    public void setChartDirLicense(String strChartDirLicense) {
        this.strChartDirLicense = strChartDirLicense;
    }

    public AbstractMap<String, String> getChartParams() {
        return mapParams;
    }

    public void setChartParams(AbstractMap<String, String> mapParams) {
        this.mapParams = mapParams;
        this.defineVecread();
        this.redefineVecread();
    }
    
    public void addChartRequestObserver(ChartRequestObserver observer) {
        this.setChartRequestObserver(observer);
    }

    public ChartRequestObserver getChartRequestObserver() {
        return chartRequestObserver;
    }

    public void setChartRequestObserver(ChartRequestObserver chartRequestObserver) {
        this.chartRequestObserver = chartRequestObserver;
    }
    
    public String toString() {
        return super.toString();
    }
    
    public String toBase64String() {
        StringBuilder sbBase64 = new StringBuilder("");
        
        if (this.getGraphBytes() != null && this.getGraphBytes().size() > 0) {
            for (Iterator<byte[]> it = abGraphBytes.iterator(); it.hasNext();) {
                sbBase64.append(DWBase64.byteArrayToBase64(it.next()));
                sbBase64.append("�");
            }
            sbBase64.deleteCharAt(sbBase64.length()-1);
        }
        
        return sbBase64.toString();
    }

    public ChartRequestHandler getRequestHandler() {
        return requestHandler;
    }
    
    public void handleNextPage() throws ChartRequestException {
        if (this.getChartRequestObserver() == null) {
            throw new IllegalStateException("It must be supplied an ChartRequestObserver for this action. Please set one at addChartRequestOberserver().");
        }
        
        if (requestHandler.canRequest()) {
            ChartRequest request = new ChartRequest();
            request.setOriginalChart(this);
            request.setChartRequestObserver(chartRequestObserver);
            request.setErro(this.getErro());
            request.setURLPage(this.getURLNextPag());
            this.requestHandler.handleRequest(request);
        } else {
            throw new ChartRequestException("ChartRequestHandler : blocked");
        }
    }
    
    public void handlePreviousPage() throws ChartRequestException {
        if (this.getChartRequestObserver() == null) {
            throw new IllegalStateException("It must be supplied an ChartRequestObserver for this action. Please set one at addChartRequestOberserver().");
        }
        
        if (requestHandler.canRequest()) {
            ChartRequest request = new ChartRequest();
            request.setOriginalChart(this);
            request.setChartRequestObserver(chartRequestObserver);
            request.setErro(this.getErro());
            request.setURLPage(this.getURLPrevPag());
            this.requestHandler.handleRequest(request);
        } else {
            throw new ChartRequestException("ChartRequestHandler : blocked");
        }
    }

    public DWScriptChart getWindowOwner() {
        return windowOwner;
    }

    public void setWindowOwner(DWScriptChart windowOwner) {
        this.windowOwner = windowOwner;
    }
    
}