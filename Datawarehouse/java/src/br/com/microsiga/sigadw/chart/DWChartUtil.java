package br.com.microsiga.sigadw.chart;

import br.com.microsiga.sigadw.http.DWHttp;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * DWChartUtil.java
 *
 * Fun��o: classe utilit�ria para a cria��o de gr�ficos no SigaDw
 *
 * Autor: Paulo R. Vieira -  Microsiga Software S/A
 * Criado em 20 de Novembro de 2006
 */
public class DWChartUtil {
    
    /** Creates a new instance of DWChartUtil */
    public DWChartUtil() {
    }
    
    public static AbstractMap<String, String> loadParams(String sURL, String sDWSession, int intQuerySleep, int intQueryMaxTime) {
        int intSleepTime = 0;
        String sRead = "";
        AbstractMap<String, String> vRet = new HashMap<String, String>();
        
        try {
            URL dwurl = new URL(sURL);
            DWHttp dwconn = new DWHttp(dwurl);
            
            if(dwconn.openRequest("GET", sDWSession)) {
                do {
                    try {
                        Thread.sleep(intQuerySleep);
                        intSleepTime += intQuerySleep;
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    
                    sRead = dwconn.read();
                } while ((sRead == null || sRead.trim().equals("")) && intSleepTime < intQueryMaxTime);
                
                StringTokenizer stDados = new StringTokenizer(sRead, "\n");
                while (stDados.hasMoreTokens()) {
                    sRead = stDados.nextToken();
                    if (sRead != null && !sRead.trim().equals("") && sRead.indexOf("=") > -1) {
                        vRet.put(sRead.substring(0, sRead.indexOf("=")).trim(), sRead.substring(sRead.indexOf("=") + 1).trim());
                    }
                }
            }
            dwconn.closeRequest();
        } catch(MalformedURLException e) {
            System.err.println("e -> " + e.getMessage());
        }
        return vRet;
    }
    
    public static String getstrvalue(String sItem, String sPadrao, AbstractMap<String, String> vParams) {
        boolean lFound = false;
        String sRet = sPadrao;
        String strTemp = vParams.get(sItem);
        
        if (strTemp != null) {
            sRet = strTemp;
        }
        
        return sRet.trim();
    }
    
    public static double getdoublevalue(String sItem, String sPadrao, AbstractMap<String, String> vParams) {
        double dblReturn;
        
        String valor = getstrvalue(sItem, sPadrao, vParams);
        
        try {
            dblReturn = Double.parseDouble(valor);
        } catch (NumberFormatException nfe) {
            dblReturn = 0.0;
        }
        
        return dblReturn;
    }
    
    public static int getintvalue(String sItem, String sPadrao, AbstractMap<String, String> vParams) {
        int intReturn;
        
        String valor = getstrvalue(sItem, sPadrao, vParams);
        
        try {
            intReturn = Integer.parseInt(valor);
        } catch (NumberFormatException nfe) {
            intReturn = 0;
        }
        
        return intReturn;
    }
    
}