package br.com.microsiga.sigadw.chart;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import pv.shape.Cube;
import pv.util.PVBorder;

public class DWRotatingCube extends Thread {
    
    private static Cube m_cube = null;
    private static Graphics m_g = null;
    private static boolean m_active;
    private static boolean m_running;
    private static Color m_background;
    private static Color m_font;
    private static Component m_owner;
    private static String m_message;
    private static Font m_f;
    private static FontMetrics fm_lab;
    private static int m_width = 0;
    private static double speed = 0.7;
    private static Color m_line;
    private static DWRotatingCube singleton;
    
    private DWRotatingCube() { }
    
    public static DWRotatingCube getInstance(String message, Color font, Color background, Color line, Component owner) {
        m_background = background;
        m_font = font;
        m_line = line;
        m_message = message;
        m_cube = new pv.shape.Cube(25, 25, m_background);
        m_owner = owner;
        
        if (singleton == null || !singleton.isAlive()) {
            singleton = new DWRotatingCube();
            m_active = true;
            m_running = true;
            singleton.start();
        }
        
        return singleton;
    }
    
    public void run() {
        while (m_running) {
            if (m_running && m_active) {
                m_f = new Font("Tahoma", Font.BOLD, 12);
                fm_lab = m_owner.getFontMetrics(m_f);
                m_width = getTextDimension(m_message, fm_lab).width+60;
                m_g = m_owner.getGraphics();
                
                Rectangle rect = new Rectangle( Math.max(((int)m_owner.getBounds().width/2)-((int)m_width/2), 0), Math.max(((int)m_owner.getBounds().height/2)-40, 0), 20 + m_width, 60);
                PVBorder.shadow(m_g, rect, m_line, m_background, 1, 2);
                PVBorder.shadow(m_g, rect, m_line, m_background, 3, 2);
                m_g.setColor(m_background);
                m_g.fillRect(rect.x, rect.y, rect.width, rect.height);
                m_cube.x = rect.x+18; m_cube.y = rect.y+12;
                m_g.setFont(m_f);
                m_g.setColor(m_font);
                m_g.drawString(m_message, rect.x+60, rect.y+30);
                m_cube.lines = Color.black;
                m_cube.angleX += speed * 6.0;
                m_cube.angleY += speed;
                m_cube.angleZ += speed;
                m_cube.paint(m_g);
                
                try {
                    Thread.sleep(1000);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                    this.holdon();
                    this.end();
                }
            }
        }
        m_owner.repaint();
    }
    
    public void holdon() {
        m_active = false;
    }
    
    public void end() {
        m_running = false;
    }
    
    /**
     * Dimension of the cell text.
     * @return The width and height of text of the cell ref.
     */
    protected Dimension getTextDimension(String txt, FontMetrics fm) {
        Dimension d = new Dimension(0, 0);
        
        if(txt.length() == 0) {
            return d;
        }
        
        d.width = fm.stringWidth(txt) + 8;
        
        return d;
    }
    
}