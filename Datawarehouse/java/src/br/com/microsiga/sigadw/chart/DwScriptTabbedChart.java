package br.com.microsiga.sigadw.chart;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * Painel com a utiliza��o de abas para a exibi��o de diversos n�veis de drill down.
 * @author Paulo R. Vieira - TOTVS S/A
 * @since SigaDW R4
 */
public class DwScriptTabbedChart extends AbstractDwScriptChart {
    private JTabbedPane tbpnlMainContainer = new JTabbedPane();
    
    public DwScriptTabbedChart() {
        super();
    }
    
    /**
     * Adiciona um Chart/Gr�fico a tela
     * @param compAdded
     *      Component a ser adicionado
     */
    protected void addChartContainer(JComponent compAdded) {
        this.addChartContainer(compAdded, AbstractDwScriptChart.AC_GENERAL);
    }
    
    protected void addChartContainer(JComponent compAdded, int actionType) {
        try {
            this.tbpnlMainContainer.addTab(compAdded.getName(), compAdded);
            
            TabbedComponentWithButton tbdComp;
            
            if (this.tbpnlMainContainer.getTabCount() > 1) {
                tbdComp = new TabbedComponentWithButton(this.tbpnlMainContainer, compAdded.getName(), "", this.getLabelClose(), new ImageIcon(new URL(DwScriptTabbedChart.getCloseImg())));
                this.tbpnlMainContainer.setTabComponentAt(this.tbpnlMainContainer.getTabCount()-1, tbdComp);
            }
            
            this.tbpnlMainContainer.setToolTipTextAt(this.tbpnlMainContainer.getTabCount()-1, compAdded.getToolTipText());
            this.tbpnlMainContainer.setSelectedComponent(compAdded);
        } catch (MalformedURLException ex) {
            Logger.getLogger(DwScriptTabbedChart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Atualiza o gr�fico corrente com um novo component
     * @param compRem
     *      Component a ser utilizado
     */
    protected void updateChartContainer(JComponent compUpdated) {
        int intTabs = this.tbpnlMainContainer.getSelectedIndex();
        JScrollPane panel = (JScrollPane) this.tbpnlMainContainer.getComponentAt(intTabs);
        panel.invalidate();
        this.tbpnlMainContainer.setComponentAt(intTabs, compUpdated);
    }
    
    /**
     * Remove o �ltimo Chart/Gr�fico da tela
     * @param compRem
     *      Component a ser removido
     */
    protected void removeChartContainer() {
        this.tbpnlMainContainer.remove(this.tbpnlMainContainer.getSelectedIndex());
    }
    
    protected void doInitialLoading() {
        JComponent jcmpApplet = this.loadChart(getURLBase() + getProgram() + getAction());
        DWChart chart = (DWChart) jcmpApplet.getClientProperty(PROPERTY_CHART);
        jcmpApplet.setName(chart.getstrvalue("TITLE", "Chart"));
        jcmpApplet.setToolTipText(chart.getstrvalue("TITLE", "Chart"));
        this.addChartContainer(jcmpApplet);
    }
    
    protected JComponent getMainChartLayoutPanel() {
        return this.tbpnlMainContainer;
    }
    
    protected DWChart getCurrentChart() {
        DWChart chart = (DWChart) getCurrentComponent().getClientProperty(PROPERTY_CHART);
        return chart;
    }
    
    protected JScrollPane getCurrentComponent() {
        return (JScrollPane) this.tbpnlMainContainer.getComponentAt(this.tbpnlMainContainer.getSelectedIndex());
    }
    
}