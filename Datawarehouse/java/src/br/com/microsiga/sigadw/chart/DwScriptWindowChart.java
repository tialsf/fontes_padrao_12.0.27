package br.com.microsiga.sigadw.chart;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

/**
 * Painel simples com a utiliza��o de janelas ou n�o. Para configurar a abertura de
 *  drilldowns em novas janelas deve-se passar o valor true no construtor dessa classe
 *  e false para abrir na mesma tela
 * @author Paulo R. Vieira - TOTVS S/A
 * @since SigaDW R4
 */
public class DwScriptWindowChart extends AbstractDwScriptChart {
    
    private CardLayout card = new CardLayout();
    private JPanel jpnlComponents = new JPanel(card);
    private boolean blnDDNewWindow;
    private JComponent currentComponent;
    private boolean blnDDFooterAdded = false;
    
    public DwScriptWindowChart(boolean blnDDNewWindow) {
        super();
        this.blnDDNewWindow = blnDDNewWindow;
    }
    
    @Override
    protected void doInitialLoading() {
        addChartContainer(this.loadChart(getURLBase() + getProgram() + getAction()));
    }

    private void defineDrillDownButtons() {
        if (!this.blnDDNewWindow) {
            JButton jbtnPrevDD = new JButton(AbstractDwScriptChart.getLabelPrevDrill());
            JButton jbtnDDNext = new JButton(AbstractDwScriptChart.getLabelNextDrill());
            JButton jbtnCloseDD = new JButton(getLabelClose());
            
            try {
                jbtnCloseDD.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                jbtnCloseDD.setIcon(new ImageIcon(new URL(getCloseImg())));
            } catch (MalformedURLException ex) {
                Logger.getLogger(DwScriptWindowChart.class.getName()).log(Level.SEVERE, null, ex);
            }
            jbtnPrevDD.setActionCommand(String.valueOf(AC_PREVDRILL));
            jbtnDDNext.setActionCommand(String.valueOf(AC_NEXTDRILL));
            jbtnCloseDD.setActionCommand(String.valueOf(AC_CLOSEDRILL));

            jbtnPrevDD.addActionListener(this);
            jbtnDDNext.addActionListener(this);
            jbtnCloseDD.addActionListener(this);

            super.addMainFooterButton(jbtnPrevDD);
            super.addMainFooterButton(jbtnDDNext);
            super.addMainFooterButton(jbtnCloseDD);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        
        switch (Integer.parseInt(e.getActionCommand())) {
            case AC_PREVDRILL:
                this.card.previous(this.getMainChartLayoutPanel());
                break;
            case AC_NEXTDRILL:
                this.card.next(this.getMainChartLayoutPanel());
                break;
            case AC_CLOSEDRILL:
                if (this.getMainComponentsPanel().getComponentCount() > 1) {
                    if (this.getMainComponentsPanel().getComponent(0) != getCurrentComponent()) {
                        this.getMainComponentsPanel().remove(getCurrentComponent());

                        if (this.getMainComponentsPanel().getComponentCount() == 1) {
                            super.getFooterContainer().removeAll();
                            super.defineChartCommandsPanel();
                            this.blnDDFooterAdded = false;
                        }
                    }
                }
                break;
        }
        
        this.updateApplication();
    }
    
    protected JComponent getMainChartLayoutPanel() {
        return getMainComponentsPanel();
    }
    
    private JPanel getMainComponentsPanel() {
        return jpnlComponents;
    }
    
    @Override
    protected void addChartContainer(JComponent compAdded) {
        addChartContainer(compAdded, AbstractDwScriptChart.AC_GENERAL);
    }
    
    protected void addChartContainer(JComponent compAdded, int actionType) {
        if (actionType == AbstractDwScriptChart.AC_DRILLDOWN && this.blnDDNewWindow) {
            JFrame newWindow = new JFrame(compAdded.getName());
            newWindow.setVisible(true);
            newWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            
            // cria um novo objeto de cronstru��o de gr�ficos para o SigaDW
            AbstractDwScriptChart chart = new DwScriptWindowChart(this.blnDDNewWindow);
            // inicializa os componentes
            chart.initializeComponents();
            chart.addChartContainer(compAdded);
            chart.setChartRootPane(this.getChartRootPane());
            
            // adiciona o objeto criado ao JFrame
            newWindow.getContentPane().add(chart, BorderLayout.CENTER);
            newWindow.pack();
        } else {
            this.currentComponent = compAdded;
            getMainComponentsPanel().add(compAdded.getName(), this.currentComponent);
            this.card.show(getMainComponentsPanel(), compAdded.getName());
            updateApplication();
            
            if (actionType != AC_GENERAL && !this.blnDDFooterAdded) {
                this.defineDrillDownButtons();
                this.blnDDFooterAdded = true;
            }
        }
    }

    @Override
    protected void updateChartContainer(JComponent compUpdated) {
        this.getMainComponentsPanel().remove(getCurrentComponent());
        addChartContainer(compUpdated);
    }

    @Override
    protected void removeChartContainer() {
        getMainComponentsPanel().removeAll();
        updateApplication();
    }

    @Override
    protected DWChart getCurrentChart() {
        return (DWChart) getCurrentComponent().getClientProperty(PROPERTY_CHART);
    }

    @Override
    protected JScrollPane getCurrentComponent() {
        Component cmpCurrent = null;
        Component[] cmpCards = this.getMainComponentsPanel().getComponents();
        for (int i = 0; i < cmpCards.length; i++) {
            if (cmpCards[i].isVisible()) {
                cmpCurrent = cmpCards[i];
                break;
            }
        }
        
        return (JScrollPane) cmpCurrent;
    }
    
    private void updateApplication() {
        getMainComponentsPanel().updateUI();
        getMainComponentsPanel().validate();
        getMainChartLayoutPanel().updateUI();
        getMainChartLayoutPanel().validate();
    }
       
}