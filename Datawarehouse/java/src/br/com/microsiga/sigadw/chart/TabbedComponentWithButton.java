package br.com.microsiga.sigadw.chart;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

public class TabbedComponentWithButton extends JPanel implements ActionListener, MouseListener {
    private JTabbedPane rootPane;
    private String strTitle;
    private String strBtnTooltip;
    private Icon icon;
    
    public TabbedComponentWithButton(JTabbedPane rootPane, String strTitle, String strTooltip, String strBtnTooltip, Icon icon) {
        super( new FlowLayout(FlowLayout.LEFT, 0, 0) );
        
        this.rootPane = rootPane;
        this.strTitle = strTitle;
        this.setToolTipText(strTooltip);
        this.strBtnTooltip=strBtnTooltip;
        this.icon = icon;
        
        this.defineLayout();
        
        this.setOpaque(false);
    }
    
    protected void defineLayout() {
        JLabel jlbl = new JLabel(this.strTitle);
        jlbl.setText(this.strTitle);
        jlbl.addMouseListener(this);
        this.add(jlbl);
        
        JButton jbtn = new JButton();
        jbtn.setIcon(this.icon);
        jbtn.setSize(1,10);
        jbtn.setUI(new BasicButtonUI());
        jbtn.setContentAreaFilled(false);
        jbtn.setOpaque(false);
        jbtn.setFocusable(false);
        jbtn.setBorderPainted(false);
        jbtn.setToolTipText(strBtnTooltip);
        jbtn.addActionListener(this);
        this.add(jbtn);
    }

    public void actionPerformed(ActionEvent e) {
        int i = this.rootPane.indexOfTabComponent(this);
        this.rootPane.remove(i);
    }
    
    public void mouseClicked(MouseEvent e) {
        int i = this.rootPane.indexOfTabComponent(this);
        this.rootPane.setSelectedIndex(i);
    }
    
    public void mousePressed(MouseEvent e) { /*n�o utilizado*/ }
    public void mouseReleased(MouseEvent e) { /*n�o utilizado*/ }
    public void mouseEntered(MouseEvent e) { /*n�o utilizado*/ }
    public void mouseExited(MouseEvent e) { /*n�o utilizado*/ }
    
}