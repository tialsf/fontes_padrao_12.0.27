package br.com.microsiga.sigadw.definition;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.*;

/**
 * Classe que implementa o posicionamento de 2 imagens como icone
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 3 de Maio de 2006
 */
public class DecoratedIcon implements Icon {
    
    private Icon originalIcon;
    private Icon decorationIcon;
    private int xDiff;
    private int yDiff;
    private Location location;
    
    // Java 1.5 enumeration
    public enum Location { UPPER_LEFT, UPPER_RIGHT, LOWER_LEFT, LOWER_RIGHT, EXPANDED_WIDTH, EXPANDED_HEIGHT, JUST_LEFT };
    
    public DecoratedIcon(Icon original, Icon decoration) {
        this(original, decoration, Location.LOWER_LEFT);
    }
    
    public DecoratedIcon(Icon original, Icon decoration, Location loc) {
        
        this.location = loc;
        this.setOriginalIcon(original);
        this.setDecorationIcon(decoration);
        if(decoration.getIconHeight() > original.getIconHeight() || 
                decoration.getIconWidth() > original.getIconWidth()) {
            throw new IllegalArgumentException("Decoration must be smaller than the original");
        }
        this.xDiff = getOriginalIcon().getIconWidth() - getDecorationIcon().getIconWidth();
        this.yDiff = getOriginalIcon().getIconHeight() - getDecorationIcon().getIconHeight();
    }
    
    public int getIconHeight() {
        return getOriginalIcon().getIconHeight();
    }
    
    public int getIconWidth() {
        int intWidth = 0;
        if (location == Location.EXPANDED_WIDTH) {
            intWidth = getOriginalIcon().getIconHeight() + getDecorationIcon().getIconWidth();
        } else if (location == Location.JUST_LEFT) {
            intWidth = getOriginalIcon().getIconHeight();
        } else {
            intWidth = getOriginalIcon().getIconWidth();
        }
        return intWidth;
    }
    
    public void paintIcon(Component owner, Graphics g, int x, int y) {
        // paint original first
        getOriginalIcon().paintIcon(owner, g, x, y);
        
        int decorationX = xDiff;
        int decorationY = yDiff;
        
        // augment x.
        if(location == Location.UPPER_RIGHT || location == Location.LOWER_RIGHT) {
            decorationX += xDiff;
        }
        
        // augment y.
        if(location == Location.LOWER_LEFT || location == Location.LOWER_RIGHT) {
            decorationY += yDiff;
        }
        
        if (location == Location.EXPANDED_WIDTH) {
            decorationX = x + getDecorationIcon().getIconWidth();
            decorationY = y;
        }
        
        if (location == Location.EXPANDED_HEIGHT) {
            decorationX = x;
            decorationY = y + getDecorationIcon().getIconHeight();
        }
        
        getDecorationIcon().paintIcon(owner, g, decorationX, decorationY);
    }

    public Icon getOriginalIcon() {
        return originalIcon;
    }

    public void setOriginalIcon(Icon originalIcon) {
        this.originalIcon = originalIcon;
    }

    public Icon getDecorationIcon() {
        return decorationIcon;
    }

    public void setDecorationIcon(Icon decorationIcon) {
        this.decorationIcon = decorationIcon;
    }
    
}