package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dialogy;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import pv.jfcx.JPVColor;
import pv.jfcx.JPVEdit;
import pv.jfcx.JPVNumeric;

/**
 * Respons�vel por exibir as propriedades para o gr�fico
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 25 de Abril de 2006, 16:52
 */
public class DefinitionGraphProperties extends javax.swing.JDialog {
	
	private DefinitionProperties properties = DefinitionProperties.getInstance();
	
	/** propriedades para o gr�fico */
	public static final int GRAPH_PROPS     = 1;
	
	/** propriedades y para o gr�fico */
	public static final int GRAPH_PROPS_Y   = 2;
	
	/**
	 * atributo que conter� a ordem dos componentes visuais organizados para definir os
	 * seus valores e recuper�-los para salv�-los
	 */
	private Component[] componentsOrder;
	
	
	
	private void setEnableFields(javax.swing.JComponent compToEnable ,boolean enable){
		for(int i = 0; i < compToEnable.getComponentCount(); i++){
			compToEnable.getComponent(i).setEnabled(enable);
		}
	}
	
	
	/**
	 * Constr�i um novo form DefinitionGraphProperties passando o frame que abriu este form,
	 * o t�tulo da janela e esta janela ser� modal
	 * @param framOwner frame que abriu este form
	 * @param strTitle t�tulo deste form
	 * @param blnModal sinaliza se este form ser� modal
	 */
	public DefinitionGraphProperties(Frame framOwner, String strTitle, boolean blnModal) {
		super(framOwner, strTitle, blnModal);
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		}
		
		initComponents();
		initComponentsLabel();
		orderComponents();
		bindComponents();
		
		
		
		// TODO - verificar se a funcionalidade de Eixo Y Adicional ser� habilitada novamente no futuro
		//jtbbProperties.setEnabledAt(1, false);
		
		jpcAxisSize.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcAxisSize.setBounds(60, 100, 110, 25);
		
		jpcSerieBgColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcSerieBgColor.setBounds(60, 100, 110, 25);
		
		jpcSerieBgGraphColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcSerieBgGraphColor.setBounds(60, 100, 110, 25);
		
		jpcSerieColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcSerieColor.setBounds(60, 100, 110, 25);
		
		jpcZoneOneColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcZoneOneColor.setBounds(60, 100, 110, 25);
		
		jpcZoneTwoColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcZoneTwoColor.setBounds(60, 100, 110, 25);
		
		jpcZoneThreeColor.setCellBorder(pv.util.PVBorder.SUNKEN + 3);
		jpcZoneThreeColor.setBounds(60, 100, 110, 25);
	}
	
	/**
	 * M�todo respons�vel por inicializar os t�tulos dos campos deste formul�rio, a partir
	 * do singleton de DefinitionProperties
	 * @see br.com.microsiga.sigadw.definition.to.DefinitionProperties
	 */
	protected void initComponentsLabel() {
		// define os labels dos bot�es Enviar e Cancelar
		jbtnOk.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SUBMIT).getName());
		jbtnCancel.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_CANCEL).getName());
		
		// define os labels dos tabs
		jtbbProperties.setTitleAt(0, properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_PROPSY_ADIC).getName());
		// TODO - verificar se a funcionalidade de Eixo Y Adicional ser� habilitada novamente no futuro
		//jtbbProperties.setTitleAt(1, properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_ADIC).getName());
		jtbbProperties.setTitleAt(1, properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_GRAPH_PROPS).getName());
		jtbbProperties.setTitleAt(2, properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_PROPS).getName());
		
		// define o label dos componentes da tab eixo y
		jckbDrillDown.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_OPEN).getName());
		jckbScaleAutom.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCAL_AUT).getName());
		jckbScaleBase.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BASE).getName());
		jckbScaleLogar.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCAL_LOG).getName());
		jlblDrillDown.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_DOWN).getName());
		jlblScaleAutomIncr.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_INCR_VAL).getName());
		jlblScaleAutomMax.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MAX_VAL).getName());
		jlblScaleAutomMin.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MIN_VAL).getName());
		jlblScaleLogarBase.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_LOG_BASE).getName());
		jlblScaleMeta.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_META).getName());
		jlblTitle.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_TITLE).getName());
		jlblMarca.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MARCA_TITLE).getName());
		((TitledBorder) jpnlScale.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCALE).getName());
		
		// define o label dos componentes da tab eixo y adicional
		jckbAxisYAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_ADIC).getName());
		jckbScalAutAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCAL_AUTO_ADIC).getName());
		jckbScalLogAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCAL_LOG_ADIC).getName());
		jlblBaseAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BASE_META_ADIC).getName());
		jlblScalAutIncrAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_INCR_VAL_ADIC).getName());
		jlblScalAutMaxAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MAX_VAL_ADIC).getName());
		jlblScalAutMinAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MIN_VAL_ADIC).getName());
		jlblScalLogBaseAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_LOG_BASE_ADIC).getName());
		jlblTitleAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_TITLE_ADIC).getName());
		jlblMarcaAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_MARCA_TITLE_ADI).getName());
		jckbScaleBaseAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BASE_ADIC).getName());
		((TitledBorder) jpnlScaleAdic.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SCALE_ADIC).getName());
		jlblDotPagAdic.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DOTPAG).getName());
		
		// define o label dos componentes da tab Propriedade Gr�fico
		
		jcmbAxisFont.removeAllItems();
		for (Dialogy dialogy : properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_FONT).getDialogyChilds()) {
			jcmbAxisFont.addItem(dialogy);
		}
		jlblAxisSize.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_SIZE).getName());
		
		jcmbAxisSerieSobr.removeAllItems();
		for (Dialogy dialogy : properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SERIES_SOBREPOS).getDialogyChilds()) {
			jcmbAxisSerieSobr.addItem(dialogy);
		}
		
		jcmbSerieFont.removeAllItems();
		for (Dialogy dialogy : properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FONT).getDialogyChilds()) {
			jcmbSerieFont.addItem(dialogy);
		}
		
		jcmbAxisSerieIntes.removeAllItems();
		for (Dialogy dialogy : properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SERIES_INTENS).getDialogyChilds()) {
			jcmbAxisSerieIntes.addItem(dialogy);
		}
		jlblAxisColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_COLOR).getName());
		jlblAxisFont.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_FONT).getName());
		jlblAxisSerieIntes.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SERIES_INTENS).getName());
		jlblAxisSerieSobr.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SERIES_SOBREPOS).getName());
		jlblSerieBgColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BGCOLOR).getName());
		jlblSerieBgGraphColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_GRAPH_BGCOLOR).getName());
		jlblSerieColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_COLOR).getName());
		jlblSerieFont.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FONT).getName());
		jlblSerieSize.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SIZE).getName());
		jlblBgImage.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BACK_IMAGE).getName());
		jcbxShowValues.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SHOW_VALUES).getName());
		jcbxShowLegende.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SHOW_LEGEND).getName());
		
		((TitledBorder) jpnlAxisXY.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_AXIS_XY).getName());
		((TitledBorder) jpnlSerie.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SERIE).getName());
		
		// define o label dos componentes da tab de propriedades de zonas
		jckbZones.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SHOW_ZONES).getName());
		((TitledBorder) jpnlZoneOne.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FIRST_ZONE).getName());
		jlblTitleZoneOne.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_TITLE).getName());
		jlblBandZoneOne.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_BANDWITH).getName());
		jlblZoneOneInitValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_INIT_VAL).getName());
		jlblZoneOneLastValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_LAST_VAL).getName());
		jlblZoneOneColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_COLOR).getName());
		
		((TitledBorder) jpnlZoneTwo.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SECOND_ZONE).getName());
		jlblTitleZoneTwo.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_TITLE).getName());
		jlblBandZoneTwo.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_BANDWITH).getName());
		jlblZoneTwoInitValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_INIT_VAL).getName());
		jlblZoneTwoLastValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_LAST_VAL).getName());
		jlblZoneTwoColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_COLOR).getName());
		
		((TitledBorder) jpnlZoneThree.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_THIRD_ZONE).getName());
		jlblTitleZoneThree.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_TITLE).getName());
		jlblBandZoneThree.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_BANDWITH).getName());
		jlblZoneThreeInitValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_INIT_VAL).getName());
		jlblZoneThreeLastValue.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_LAST_VAL).getName());
		jlblZoneThreeColor.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_ZONE_COLOR).getName());
		jRadioButton1.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FORMAT_IND).getName());
		jRadioButton2.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FORMAT_IND_OWNER).getName());
		jLabel25.setText(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_CASAS_DECIMAIS).getName());
		pnlFonte.setBorder(javax.swing.BorderFactory.createTitledBorder(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FONTE).getName()));
		pnlDecimais.setBorder(javax.swing.BorderFactory.createTitledBorder(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_CASAS_DECIMAIS).getName()));
		pnlEfeitos.setBorder(javax.swing.BorderFactory.createTitledBorder(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_EFEITOS).getName()));
		
		
		
		
		//((TitledBorder) jpnlZones.getBorder()).setTitle(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_SHOW_ZONES).getName());
	}
	
	/**
	 * M�todo respons�vel por manter a ordem em que as propriedades dos componentes ser�o
	 * apresentados/recuperados
	 */
	protected void orderComponents() {
		componentsOrder = new Component[] {
			jpetTitle,
			jckbScaleBase, jpnScaleMeta,
			jckbScaleLogar, jpnScaleLogarBase,
			jckbScaleAutom, jpnScaleAutomMin, jpnScaleAutomMax, jpnScaleAutomIncr,
			jckbAxisYAdic, jpeTitleAdic,
			jckbScalLogAdic, jpnScalLogBaseAdic,
			jckbScalAutAdic, jpnScalAutMinAdic, jpnScalAutMaxAdic, jnScalAutIncrAdic,
			jckbDrillDown, 
			jpnDotPagAdic,
			jpetMarca,
			jpetMarcaAdic,
			jckbScaleBaseAdic,
			jpnBaseAdic,
			jcmbSerieFont,
			jspSerieSize,
			jpcSerieColor,
			jcmbAxisFont,
			jspAxisSize,
			jpcAxisSize,
			jcmbAxisSerieIntes,
			jcmbAxisSerieSobr,
			jpcSerieBgColor,
			jpcSerieBgGraphColor,
			jpetBgImage,
			jcbxShowValues,
			jcbxShowLegende,
			jckbZones,
			jpetTitleZoneOne,
			jpnZoneOneInitValue,
			jpnZoneOneLastValue,
			jpcZoneOneColor,
			jpetTitleZoneTwo,
			jpnZoneTwoInitValue,
			jpnZoneTwoLastValue,
			jpcZoneTwoColor,
			jpetTitleZoneThree,
			jpnZoneThreeInitValue,
			jpnZoneThreeLastValue,
			jpcZoneThreeColor,
			spnCasasDecimais,
			jRadioButton1,
			jRadioButton2
		
		
		
		};
	}
	
	/**
	 * M�todo respons�vel por definir as propriedades para cada componente do formul�rio,
	 * a partir da ordeniza��o dos componentes realizada conforme o m�todo orderComponents()
	 * @see br.com.microsiga.sigadw.definition.DefinitionGraphProperties#orderComponents
	 */
	protected void bindComponents() {
		String strGraphProps = properties.getGraphYProps() + ";" + properties.getGraphProps();
		
		if (!strGraphProps.equals("")) {
			String[] componentsProperties = strGraphProps.split(";");
			Component comp;
			for (int i = 0; i < componentsOrder.length; i++) {
				if (i < componentsProperties.length && !componentsProperties[i].equals("")) {
					comp = componentsOrder[i];
					if (comp instanceof JComboBox) {
						((JComboBox) comp).setSelectedItem(new Dialogy(null, componentsProperties[i]));
					} else if (comp instanceof JCheckBox) {
						JCheckBox jComp = (JCheckBox) comp;
						jComp.setSelected( Boolean.parseBoolean(componentsProperties[i]) );
						if (jComp.isSelected()) {
							jComp.dispatchEvent(new MouseEvent(
								comp, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
						}
					} else if (comp instanceof JPVEdit) { // JPVNumeric cair� nessse if pois extends de JPVEdit
						((JPVEdit) comp).setText(componentsProperties[i]);
					} else if (comp instanceof JPVColor) {
						((JPVColor) comp).setColor(new Color(Integer.parseInt(componentsProperties[i])));
					} else if (comp instanceof javax.swing.JRadioButton) {
						javax.swing.JRadioButton jComp = (javax.swing.JRadioButton) comp;
						jComp.setSelected( Boolean.parseBoolean(componentsProperties[i]) );
					} else if (comp instanceof javax.swing.JSpinner) {
						javax.swing.JSpinner jComp = (javax.swing.JSpinner) comp;
						if(componentsProperties[i].toString().equals("")){
							jComp.setValue(0);
						}else{
							jComp.setValue(Integer.parseInt(componentsProperties[i].toString()));
						}
					}
					
					comp.validate();
				}
			}
			
			// campo especial. O n�o selecionamento ocorrer� no disparo do evento de click
			if (!jckbScaleAutom.isSelected()) {
				jckbScaleAutom.dispatchEvent(new MouseEvent(jckbScaleAutom, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
			}
			
			// campo especial. O n�o selecionamento ocorrer� no disparo do evento de click
			if (!jckbScalAutAdic.isSelected()) {
				jckbScalAutAdic.dispatchEvent(new MouseEvent(jckbScalAutAdic, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
			}
		}
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala base e
	 * habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScaleBaseEvent(java.awt.AWTEvent evt) {
		changeComponentEnabled(jlblScaleMeta);
		changeComponentEnabled(jpnScaleMeta);
		changeComponentEnabled(jlblMarca);
		changeComponentEnabled(jpetMarca);
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala logaritimica e
	 * habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScaleLogarEvent(java.awt.AWTEvent evt) {
		changeComponentEnabled(jlblScaleLogarBase);
		changeComponentEnabled(jpnScaleLogarBase);
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala autom�tica e
	 * habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScaleAutomEvent(java.awt.AWTEvent evt) {
		boolean blnSelected = ((JCheckBox) evt.getSource()).isSelected();
		
		changeComponentEnabled(jlblScaleAutomMin, !blnSelected);
		changeComponentEnabled(jpnScaleAutomMin, !blnSelected);
		changeComponentEnabled(jlblScaleAutomMax, !blnSelected);
		changeComponentEnabled(jpnScaleAutomMax, !blnSelected);
		changeComponentEnabled(jlblScaleAutomIncr, !blnSelected);
		changeComponentEnabled(jpnScaleAutomIncr, !blnSelected);
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de eixo adicional e
	 * habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbAxisYAdicEvent(java.awt.AWTEvent evt) {
		boolean blnEnable = ((JCheckBox) evt.getSource()).isSelected();
		
		changeComponentEnabled(jlblTitleAdic);
		changeComponentEnabled(jpeTitleAdic);
		
		changeComponentEnabled(jpnlScaleAdic);
		
		if (!blnEnable) {
			if (jckbScaleBaseAdic.isSelected()) {
				jckbScaleBaseAdic.setSelected(false);
				jckbScaleBaseAdic.dispatchEvent(new MouseEvent(jckbScaleBaseAdic, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
			}
			changeComponentEnabled(jckbScaleBaseAdic, false);
			
			if (jckbScalLogAdic.isSelected()) {
				jckbScalLogAdic.setSelected(false);
				jckbScalLogAdic.dispatchEvent(new MouseEvent(jckbScalLogAdic, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
			}
			changeComponentEnabled(jckbScalLogAdic, false);
			
			if (!jckbScalAutAdic.isSelected()) {
				jckbScalAutAdic.setSelected(true);
				jckbScalAutAdic.dispatchEvent(new MouseEvent(jckbScalAutAdic, MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), MouseEvent.SHIFT_MASK, 0, 0, 1, false));
			}
			changeComponentEnabled(jckbScalAutAdic, false);
		} else {
			changeComponentEnabled(jckbScaleBaseAdic);
			changeComponentEnabled(jckbScalLogAdic);
			changeComponentEnabled(jckbScalAutAdic);
		}
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala base no eixo
	 * adicional e habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScaleBaseAdicEvent(java.awt.AWTEvent evt) {
		changeComponentEnabled(jlblBaseAdic);
		changeComponentEnabled(jpnBaseAdic);
		changeComponentEnabled(jlblMarcaAdic);
		changeComponentEnabled(jpetMarcaAdic);
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de exibir zonas
	 * e habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbZonesKeyEvent(java.awt.AWTEvent evt) {
		changeComponentEnabled(jlblTitleZoneOne);
		changeComponentEnabled(jpetTitleZoneOne);
		changeComponentEnabled(jlblBandZoneOne);
		changeComponentEnabled(jlblZoneOneInitValue);
		changeComponentEnabled(jpnZoneOneInitValue);
		changeComponentEnabled(jlblZoneOneLastValue);
		changeComponentEnabled(jpnZoneOneLastValue);
		changeComponentEnabled(jlblZoneOneColor);
		changeComponentEnabled(jpcZoneOneColor);
		
		changeComponentEnabled(jlblTitleZoneTwo);
		changeComponentEnabled(jpetTitleZoneTwo);
		changeComponentEnabled(jlblBandZoneTwo);
		changeComponentEnabled(jlblZoneTwoInitValue);
		changeComponentEnabled(jpnZoneTwoInitValue);
		changeComponentEnabled(jlblZoneTwoLastValue);
		changeComponentEnabled(jpnZoneTwoLastValue);
		changeComponentEnabled(jlblZoneTwoColor);
		changeComponentEnabled(jpcZoneTwoColor);
		
		changeComponentEnabled(jlblTitleZoneThree);
		changeComponentEnabled(jpetTitleZoneThree);
		changeComponentEnabled(jlblBandZoneThree);
		changeComponentEnabled(jlblZoneThreeInitValue);
		changeComponentEnabled(jpnZoneThreeInitValue);
		changeComponentEnabled(jlblZoneThreeLastValue);
		changeComponentEnabled(jpnZoneThreeLastValue);
		changeComponentEnabled(jlblZoneThreeColor);
		changeComponentEnabled(jpcZoneThreeColor);
	}
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala logaritimica
	 * no eixo adicional e habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScalLogAdicEvent(java.awt.AWTEvent evt) {
		changeComponentEnabled(jpnScalLogBaseAdic);
		changeComponentEnabled(jlblScalLogBaseAdic);
	}
	
	
	/**
	 * M�todo respons�vel por tratar o selecionamento do campo de escala autom�tica no
	 * eixo adicional e habilitar ou desabilitar outros componentes
	 * @param evt objeto que cont�m o evento ocorrido na sele��o do campo
	 */
	protected void jckbScalAutAdicEvent(java.awt.AWTEvent evt) {
		boolean blnSelected = ((JCheckBox) evt.getSource()).isSelected();
		
		changeComponentEnabled(jlblScalAutMinAdic, !blnSelected);
		changeComponentEnabled(jpnScalAutMinAdic, !blnSelected);
		changeComponentEnabled(jlblScalAutMaxAdic, !blnSelected);
		changeComponentEnabled(jpnScalAutMaxAdic, !blnSelected);
		changeComponentEnabled(jlblScalAutIncrAdic, !blnSelected);
		changeComponentEnabled(jnScalAutIncrAdic, !blnSelected);
	}
	
	/**
	 * M�todo respons�vel por recuperar as propriedades do gr�fico para um determinado
	 *  tipo de propriedade passado como par�metro
	 * @param intGraphPropsType c�digo do tipo de propriedade do gr�fico
	 * @see br.com.microsiga.sigadw.definition.DefinitionGraphProperties#GRAPH_PROPS
	 * @see br.com.microsiga.sigadw.definition.DefinitionGraphProperties#GRAPH_PROPS_Y
	 */
	public String retrieveGraphProperties(int intGraphPropsType) {
		StringBuilder sbProperties = new StringBuilder();
		if (intGraphPropsType == GRAPH_PROPS_Y) {
			sbProperties.append( retrieveGraphProperties(0, 23) );
		} else if (intGraphPropsType == GRAPH_PROPS ) {
			sbProperties.append( retrieveGraphProperties(23, componentsOrder.length) );
		}
		return sbProperties.toString();
	}
	
	/**
	 * M�todo respons�vel por recuperar as propriedades do gr�fico, utilizando
	 *  o �ndice do componente de in�cio e o �ndice do componente final
	 * @param intGraphPropsInit �ndice do componente de in�cio
	 * @param intGraphPropsLast �ndice do componente de t�rmino
	 * @see br.com.microsiga.sigadw.definition.DefinitionGraphProperties#componentsOrder
	 * @see br.com.microsiga.sigadw.definition.DefinitionGraphProperties#orderComponents
	 */
	protected String retrieveGraphProperties(int intGraphPropsInit, int intGraphPropsLast){
		// salva os valores para as propriedades do gr�fico
		StringBuilder sbProperties = new StringBuilder();
		
		Component comp;
		
		for (int i = 0; i < componentsOrder.length; i ++) {
			if (i >= intGraphPropsInit && i < intGraphPropsLast) {
				if (i > intGraphPropsInit) {
					sbProperties.append(";");
				}
				
				comp = componentsOrder[i];
				if (comp instanceof JComboBox) {
					Object obj = ((JComboBox) comp).getSelectedItem();
					if (obj instanceof Dialogy) {
						sbProperties.append( ((Dialogy) obj).getValue() );
					} else {
						sbProperties.append( ((JComboBox) comp).getSelectedIndex() );
					}
				} else if (comp instanceof JCheckBox) {
					sbProperties.append( ((JCheckBox) comp).isSelected() ? "true" : "false" );
				} else if (comp instanceof JPVEdit) { // JPVNumeric cair� nessse if pois extends de JPVEdit
					sbProperties.append( ((JPVEdit) comp).getText() );
				} else if (comp instanceof JPVColor) {
					if (((JPVColor) comp).getColor() != null) {
						sbProperties.append( ((JPVColor) comp).getColor().getRGB() );
					}
				} else if (comp instanceof javax.swing.JRadioButton) {
						sbProperties.append( ((javax.swing.JRadioButton) comp).isSelected() ? "true" : "false" );
				} else if (comp instanceof javax.swing.JSpinner) {
						sbProperties.append( ((javax.swing.JSpinner) comp).getValue());
				}
			}
		}
		
		return sbProperties.toString();
	}
	
	
	private void enabledCasasDecimais(){
		if(jRadioButton2.isSelected()){
			spnCasasDecimais.setEnabled(true);
			jLabel25.setEnabled(true);
		}else{
			spnCasasDecimais.setEnabled(false);
			jLabel25.setEnabled(false);
		}
	}
	
	
	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlAxisYAdic = new javax.swing.JPanel();
        // TODO - verificar se a funcionalidade de Eixo Y Adicional ser� habilitada novamente no futuro
        jckbAxisYAdic = new javax.swing.JCheckBox();
        jlblTitleAdic = new javax.swing.JLabel();
        jpnlScaleAdic = new javax.swing.JPanel();
        jlblBaseAdic = new javax.swing.JLabel();
        jckbScalLogAdic = new javax.swing.JCheckBox();
        jlblScalLogBaseAdic = new javax.swing.JLabel();
        jpnScalLogBaseAdic = new JPVNumeric(10);
        jpnScalAutMinAdic = new JPVNumeric(0);
        jlblScalAutMaxAdic = new javax.swing.JLabel();
        jpnScalAutMaxAdic = new JPVNumeric(0);
        jnScalAutIncrAdic = new JPVNumeric(0);
        jlblScalAutIncrAdic = new javax.swing.JLabel();
        jpnBaseAdic = new JPVNumeric(0);
        jpetMarcaAdic = new pv.jfcx.JPVEdit();
        jlblMarcaAdic = new javax.swing.JLabel();
        jckbScalAutAdic = new javax.swing.JCheckBox();
        jckbScaleBaseAdic = new javax.swing.JCheckBox();
        jlblScalAutMinAdic = new javax.swing.JLabel();
        jpeTitleAdic = new pv.jfcx.JPVEdit();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jtbbProperties = new javax.swing.JTabbedPane();
        jpnlAxisYProp = new javax.swing.JPanel();
        jlblTitle = new javax.swing.JLabel();
        jpnlScale = new javax.swing.JPanel();
        jckbScaleLogar = new javax.swing.JCheckBox();
        jckbScaleAutom = new javax.swing.JCheckBox();
        jlblScaleMeta = new javax.swing.JLabel();
        jpnScaleMeta = new JPVNumeric(0);
        jlblScaleLogarBase = new javax.swing.JLabel();
        jpnScaleLogarBase = new JPVNumeric(10);
        jlblScaleAutomMin = new javax.swing.JLabel();
        jpnScaleAutomMin = new JPVNumeric(0);
        jlblScaleAutomMax = new javax.swing.JLabel();
        jpnScaleAutomMax = new JPVNumeric(0);
        jlblScaleAutomIncr = new javax.swing.JLabel();
        jpnScaleAutomIncr = new JPVNumeric(0);
        jckbScaleBase = new javax.swing.JCheckBox();
        jpetMarca = new pv.jfcx.JPVEdit();
        jlblMarca = new javax.swing.JLabel();
        jpetTitle = new pv.jfcx.JPVEdit();
        jpnlGraphProp = new javax.swing.JPanel();
        jpnlSerie = new javax.swing.JPanel();
        jcbxShowValues = new javax.swing.JCheckBox();
        pnlFonte = new javax.swing.JPanel();
        jlblSerieFont = new javax.swing.JLabel();
        jcmbSerieFont = new javax.swing.JComboBox();
        jlblSerieSize = new javax.swing.JLabel();
        jlblSerieColor = new javax.swing.JLabel();
        jpcSerieColor = new pv.jfcx.JPVColor();
        jspSerieSize = new javax.swing.JSpinner();
        pnlDecimais = new javax.swing.JPanel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        spnCasasDecimais = new javax.swing.JSpinner();
        jLabel25 = new javax.swing.JLabel();
        pnlEfeitos = new javax.swing.JPanel();
        jcmbAxisSerieSobr = new javax.swing.JComboBox();
        jcmbAxisSerieIntes = new javax.swing.JComboBox();
        jlblAxisSerieIntes = new javax.swing.JLabel();
        jlblAxisSerieSobr = new javax.swing.JLabel();
        jpnlAxisXY = new javax.swing.JPanel();
        jlblAxisFont = new javax.swing.JLabel();
        jcmbAxisFont = new javax.swing.JComboBox();
        jlblAxisSize = new javax.swing.JLabel();
        jlblAxisColor = new javax.swing.JLabel();
        jpcAxisSize = new pv.jfcx.JPVColor();
        jspAxisSize = new javax.swing.JSpinner();
        jpcSerieBgColor = new pv.jfcx.JPVColor();
        jlblSerieBgColor = new javax.swing.JLabel();
        jlblSerieBgGraphColor = new javax.swing.JLabel();
        jlblBgImage = new javax.swing.JLabel();
        jpetBgImage = new pv.jfcx.JPVEdit();
        jcbxShowLegende = new javax.swing.JCheckBox();
        jpcSerieBgGraphColor = new pv.jfcx.JPVColor();
        jpnlZones = new javax.swing.JPanel();
        jpnlZoneOne = new javax.swing.JPanel();
        jpetTitleZoneOne = new pv.jfcx.JPVEdit();
        jpnZoneOneInitValue = new pv.jfcx.JPVNumeric();
        jpnZoneOneLastValue = new pv.jfcx.JPVNumeric();
        jlblZoneOneInitValue = new javax.swing.JLabel();
        jlblZoneOneLastValue = new javax.swing.JLabel();
        jlblTitleZoneOne = new javax.swing.JLabel();
        jlblBandZoneOne = new javax.swing.JLabel();
        jpcZoneOneColor = new pv.jfcx.JPVColor();
        jlblZoneOneColor = new javax.swing.JLabel();
        jckbZones = new javax.swing.JCheckBox();
        jpnlZoneTwo = new javax.swing.JPanel();
        jpetTitleZoneTwo = new pv.jfcx.JPVEdit();
        jpnZoneTwoInitValue = new pv.jfcx.JPVNumeric();
        jpnZoneTwoLastValue = new pv.jfcx.JPVNumeric();
        jlblZoneTwoInitValue = new javax.swing.JLabel();
        jlblZoneTwoLastValue = new javax.swing.JLabel();
        jlblTitleZoneTwo = new javax.swing.JLabel();
        jlblBandZoneTwo = new javax.swing.JLabel();
        jpcZoneTwoColor = new pv.jfcx.JPVColor();
        jlblZoneTwoColor = new javax.swing.JLabel();
        jpnlZoneThree = new javax.swing.JPanel();
        jpetTitleZoneThree = new pv.jfcx.JPVEdit();
        jpnZoneThreeInitValue = new pv.jfcx.JPVNumeric();
        jpnZoneThreeLastValue = new pv.jfcx.JPVNumeric();
        jlblZoneThreeInitValue = new javax.swing.JLabel();
        jlblZoneThreeLastValue = new javax.swing.JLabel();
        jlblTitleZoneThree = new javax.swing.JLabel();
        jlblBandZoneThree = new javax.swing.JLabel();
        jpcZoneThreeColor = new pv.jfcx.JPVColor();
        jlblZoneThreeColor = new javax.swing.JLabel();
        jbtnCancel = new javax.swing.JButton();
        jbtnOk = new javax.swing.JButton();
        jlblDrillDown = new javax.swing.JLabel();
        jckbDrillDown = new javax.swing.JCheckBox();
        jlblDotPagAdic = new javax.swing.JLabel();
        jpnDotPagAdic = new pv.jfcx.JPVNumeric();

        jckbAxisYAdic.setText("jCheckBox5");
        jckbAxisYAdic.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbAxisYAdic.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbAxisYAdic.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbAxisYAdicKeyPressed(evt);
            }
        });
        jckbAxisYAdic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbAxisYAdicMouseClicked(evt);
            }
        });

        jlblTitleAdic.setText("jLabel7");
        jlblTitleAdic.setEnabled(false);

        jpnlScaleAdic.setBorder(javax.swing.BorderFactory.createTitledBorder("Title"));
        jpnlScaleAdic.setEnabled(false);

        jlblBaseAdic.setText("jLabel8");
        jlblBaseAdic.setEnabled(false);

        jckbScalLogAdic.setText("jCheckBox6");
        jckbScalLogAdic.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScalLogAdic.setEnabled(false);
        jckbScalLogAdic.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScalLogAdic.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScalLogAdicKeyPressed(evt);
            }
        });
        jckbScalLogAdic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScalLogAdicMouseClicked(evt);
            }
        });

        jlblScalLogBaseAdic.setText("jLabel9");
        jlblScalLogBaseAdic.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScalLogBaseAdicLayout = new org.jdesktop.layout.GroupLayout(jpnScalLogBaseAdic);
        jpnScalLogBaseAdic.setLayout(jpnScalLogBaseAdicLayout);
        jpnScalLogBaseAdicLayout.setHorizontalGroup(
            jpnScalLogBaseAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScalLogBaseAdicLayout.setVerticalGroup(
            jpnScalLogBaseAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnScalAutMinAdicLayout = new org.jdesktop.layout.GroupLayout(jpnScalAutMinAdic);
        jpnScalAutMinAdic.setLayout(jpnScalAutMinAdicLayout);
        jpnScalAutMinAdicLayout.setHorizontalGroup(
            jpnScalAutMinAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScalAutMinAdicLayout.setVerticalGroup(
            jpnScalAutMinAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScalAutMaxAdic.setText("jLabel11");
        jlblScalAutMaxAdic.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScalAutMaxAdicLayout = new org.jdesktop.layout.GroupLayout(jpnScalAutMaxAdic);
        jpnScalAutMaxAdic.setLayout(jpnScalAutMaxAdicLayout);
        jpnScalAutMaxAdicLayout.setHorizontalGroup(
            jpnScalAutMaxAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScalAutMaxAdicLayout.setVerticalGroup(
            jpnScalAutMaxAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jnScalAutIncrAdicLayout = new org.jdesktop.layout.GroupLayout(jnScalAutIncrAdic);
        jnScalAutIncrAdic.setLayout(jnScalAutIncrAdicLayout);
        jnScalAutIncrAdicLayout.setHorizontalGroup(
            jnScalAutIncrAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jnScalAutIncrAdicLayout.setVerticalGroup(
            jnScalAutIncrAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScalAutIncrAdic.setText("jLabel12");
        jlblScalAutIncrAdic.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnBaseAdicLayout = new org.jdesktop.layout.GroupLayout(jpnBaseAdic);
        jpnBaseAdic.setLayout(jpnBaseAdicLayout);
        jpnBaseAdicLayout.setHorizontalGroup(
            jpnBaseAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnBaseAdicLayout.setVerticalGroup(
            jpnBaseAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpetMarcaAdicLayout = new org.jdesktop.layout.GroupLayout(jpetMarcaAdic);
        jpetMarcaAdic.setLayout(jpetMarcaAdicLayout);
        jpetMarcaAdicLayout.setHorizontalGroup(
            jpetMarcaAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 371, Short.MAX_VALUE)
        );
        jpetMarcaAdicLayout.setVerticalGroup(
            jpetMarcaAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblMarcaAdic.setText("jLabel1");
        jlblMarcaAdic.setEnabled(false);

        jckbScalAutAdic.setSelected(true);
        jckbScalAutAdic.setText("jCheckBox7");
        jckbScalAutAdic.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScalAutAdic.setEnabled(false);
        jckbScalAutAdic.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScalAutAdic.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScalAutAdicKeyPressed(evt);
            }
        });
        jckbScalAutAdic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScalAutAdicMouseClicked(evt);
            }
        });

        jckbScaleBaseAdic.setText("jCheckBox1");
        jckbScaleBaseAdic.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScaleBaseAdic.setEnabled(false);
        jckbScaleBaseAdic.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScaleBaseAdic.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScaleBaseAdicKeyPressed(evt);
            }
        });
        jckbScaleBaseAdic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScaleBaseAdicMouseClicked(evt);
            }
        });

        jlblScalAutMinAdic.setText("jLabel10");
        jlblScalAutMinAdic.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnlScaleAdicLayout = new org.jdesktop.layout.GroupLayout(jpnlScaleAdic);
        jpnlScaleAdic.setLayout(jpnlScaleAdicLayout);
        jpnlScaleAdicLayout.setHorizontalGroup(
            jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlScaleAdicLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleAdicLayout.createSequentialGroup()
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jckbScalAutAdic)
                            .add(jckbScaleBaseAdic))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlScaleAdicLayout.createSequentialGroup()
                                .add(6, 6, 6)
                                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jlblMarcaAdic)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jlblBaseAdic)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jlblScalLogBaseAdic))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))
                            .add(jpnlScaleAdicLayout.createSequentialGroup()
                                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jlblScalAutMaxAdic)
                                    .add(jlblScalAutIncrAdic)
                                    .add(jlblScalAutMinAdic))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED))))
                    .add(jckbScalLogAdic))
                .add(4, 4, 4)
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnScalLogBaseAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jpnBaseAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jpnScalAutMinAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlScaleAdicLayout.createSequentialGroup()
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(jpnScalAutMaxAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(jnScalAutIncrAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(jpetMarcaAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpnlScaleAdicLayout.setVerticalGroup(
            jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlScaleAdicLayout.createSequentialGroup()
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleAdicLayout.createSequentialGroup()
                        .add(16, 16, 16)
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jlblMarcaAdic)
                            .add(jckbScaleBaseAdic)))
                    .add(jpnlScaleAdicLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(jpetMarcaAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblBaseAdic)
                    .add(jpnBaseAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleAdicLayout.createSequentialGroup()
                        .add(34, 34, 34)
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jckbScalLogAdic)
                            .add(jlblScalLogBaseAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jpnlScaleAdicLayout.createSequentialGroup()
                        .add(28, 28, 28)
                        .add(jpnScalLogBaseAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 42, Short.MAX_VALUE)
                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlScaleAdicLayout.createSequentialGroup()
                        .add(jckbScalAutAdic)
                        .add(37, 37, 37))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlScaleAdicLayout.createSequentialGroup()
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jpnScalAutMinAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jlblScalAutMinAdic))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jpnlScaleAdicLayout.createSequentialGroup()
                                .add(jpnlScaleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jlblScalAutMaxAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnScalAutMaxAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jnScalAutIncrAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jlblScalAutIncrAdic))
                        .addContainerGap())))
        );

        org.jdesktop.layout.GroupLayout jpeTitleAdicLayout = new org.jdesktop.layout.GroupLayout(jpeTitleAdic);
        jpeTitleAdic.setLayout(jpeTitleAdicLayout);
        jpeTitleAdicLayout.setHorizontalGroup(
            jpeTitleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 486, Short.MAX_VALUE)
        );
        jpeTitleAdicLayout.setVerticalGroup(
            jpeTitleAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnlAxisYAdicLayout = new org.jdesktop.layout.GroupLayout(jpnlAxisYAdic);
        jpnlAxisYAdic.setLayout(jpnlAxisYAdicLayout);
        jpnlAxisYAdicLayout.setHorizontalGroup(
            jpnlAxisYAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisYAdicLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlAxisYAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlScaleAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jpnlAxisYAdicLayout.createSequentialGroup()
                        .add(jlblTitleAdic)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpeTitleAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jckbAxisYAdic))
                .addContainerGap())
        );
        jpnlAxisYAdicLayout.setVerticalGroup(
            jpnlAxisYAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisYAdicLayout.createSequentialGroup()
                .addContainerGap()
                .add(jckbAxisYAdic)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlAxisYAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpeTitleAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblTitleAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlScaleAdic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jlblTitle.setText("jLabel1");

        jpnlScale.setBorder(javax.swing.BorderFactory.createTitledBorder("Title"));

        jckbScaleLogar.setText("jCheckBox2");
        jckbScaleLogar.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScaleLogar.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScaleLogar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScaleLogarKeyPressed(evt);
            }
        });
        jckbScaleLogar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScaleLogarMouseClicked(evt);
            }
        });

        jckbScaleAutom.setSelected(true);
        jckbScaleAutom.setText("jCheckBox3");
        jckbScaleAutom.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScaleAutom.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScaleAutom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScaleAutomKeyPressed(evt);
            }
        });
        jckbScaleAutom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScaleAutomMouseClicked(evt);
            }
        });

        jlblScaleMeta.setText("jLabel1");
        jlblScaleMeta.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScaleMetaLayout = new org.jdesktop.layout.GroupLayout(jpnScaleMeta);
        jpnScaleMeta.setLayout(jpnScaleMetaLayout);
        jpnScaleMetaLayout.setHorizontalGroup(
            jpnScaleMetaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScaleMetaLayout.setVerticalGroup(
            jpnScaleMetaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScaleLogarBase.setText("jLabel2");
        jlblScaleLogarBase.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScaleLogarBaseLayout = new org.jdesktop.layout.GroupLayout(jpnScaleLogarBase);
        jpnScaleLogarBase.setLayout(jpnScaleLogarBaseLayout);
        jpnScaleLogarBaseLayout.setHorizontalGroup(
            jpnScaleLogarBaseLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScaleLogarBaseLayout.setVerticalGroup(
            jpnScaleLogarBaseLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScaleAutomMin.setText("jLabel3");
        jlblScaleAutomMin.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScaleAutomMinLayout = new org.jdesktop.layout.GroupLayout(jpnScaleAutomMin);
        jpnScaleAutomMin.setLayout(jpnScaleAutomMinLayout);
        jpnScaleAutomMinLayout.setHorizontalGroup(
            jpnScaleAutomMinLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScaleAutomMinLayout.setVerticalGroup(
            jpnScaleAutomMinLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScaleAutomMax.setText("jLabel4");
        jlblScaleAutomMax.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScaleAutomMaxLayout = new org.jdesktop.layout.GroupLayout(jpnScaleAutomMax);
        jpnScaleAutomMax.setLayout(jpnScaleAutomMaxLayout);
        jpnScaleAutomMaxLayout.setHorizontalGroup(
            jpnScaleAutomMaxLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScaleAutomMaxLayout.setVerticalGroup(
            jpnScaleAutomMaxLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblScaleAutomIncr.setText("jLabel5");
        jlblScaleAutomIncr.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnScaleAutomIncrLayout = new org.jdesktop.layout.GroupLayout(jpnScaleAutomIncr);
        jpnScaleAutomIncr.setLayout(jpnScaleAutomIncrLayout);
        jpnScaleAutomIncrLayout.setHorizontalGroup(
            jpnScaleAutomIncrLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnScaleAutomIncrLayout.setVerticalGroup(
            jpnScaleAutomIncrLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jckbScaleBase.setText("jCheckBox1");
        jckbScaleBase.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbScaleBase.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbScaleBase.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbScaleBaseKeyPressed(evt);
            }
        });
        jckbScaleBase.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbScaleBaseMouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jpetMarcaLayout = new org.jdesktop.layout.GroupLayout(jpetMarca);
        jpetMarca.setLayout(jpetMarcaLayout);
        jpetMarcaLayout.setHorizontalGroup(
            jpetMarcaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 604, Short.MAX_VALUE)
        );
        jpetMarcaLayout.setVerticalGroup(
            jpetMarcaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblMarca.setText("jLabel1");
        jlblMarca.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnlScaleLayout = new org.jdesktop.layout.GroupLayout(jpnlScale);
        jpnlScale.setLayout(jpnlScaleLayout);
        jpnlScaleLayout.setHorizontalGroup(
            jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlScaleLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleLayout.createSequentialGroup()
                        .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jckbScaleBase)
                            .add(jckbScaleAutom))
                        .add(19, 19, 19)
                        .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jpnlScaleLayout.createSequentialGroup()
                                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jlblMarca)
                                    .add(jlblScaleMeta))
                                .add(4, 4, 4)
                                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jpetMarca, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jpnScaleMeta, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(jpnlScaleLayout.createSequentialGroup()
                                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jlblScaleAutomMin)
                                    .add(jlblScaleLogarBase)
                                    .add(jlblScaleAutomIncr)
                                    .add(jlblScaleAutomMax))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jpnScaleLogarBase, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jpnScaleAutomMin, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jpnScaleAutomIncr, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jpnScaleAutomMax, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())
                    .add(jckbScaleLogar)))
        );
        jpnlScaleLayout.setVerticalGroup(
            jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlScaleLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlScaleLayout.createSequentialGroup()
                            .add(jpetMarca, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 11, Short.MAX_VALUE)
                            .add(jpnScaleMeta, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(jlblScaleMeta))
                    .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jckbScaleBase, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jlblMarca)))
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 38, Short.MAX_VALUE)
                        .add(jckbScaleLogar)
                        .add(34, 34, 34))
                    .add(jpnlScaleLayout.createSequentialGroup()
                        .add(33, 33, 33)
                        .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jlblScaleLogarBase)
                            .add(jpnScaleLogarBase, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpnScaleAutomMin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblScaleAutomMin))
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlScaleLayout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jckbScaleAutom)
                            .add(jlblScaleAutomMax)))
                    .add(jpnlScaleLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpnScaleAutomMax, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlScaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpnScaleAutomIncr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblScaleAutomIncr))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout jpetTitleLayout = new org.jdesktop.layout.GroupLayout(jpetTitle);
        jpetTitle.setLayout(jpetTitleLayout);
        jpetTitleLayout.setHorizontalGroup(
            jpetTitleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 728, Short.MAX_VALUE)
        );
        jpetTitleLayout.setVerticalGroup(
            jpetTitleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnlAxisYPropLayout = new org.jdesktop.layout.GroupLayout(jpnlAxisYProp);
        jpnlAxisYProp.setLayout(jpnlAxisYPropLayout);
        jpnlAxisYPropLayout.setHorizontalGroup(
            jpnlAxisYPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisYPropLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlAxisYPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlScale, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jpnlAxisYPropLayout.createSequentialGroup()
                        .add(jlblTitle)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpetTitle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnlAxisYPropLayout.setVerticalGroup(
            jpnlAxisYPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisYPropLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlAxisYPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpetTitle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblTitle))
                .add(16, 16, 16)
                .add(jpnlScale, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jtbbProperties.addTab("tab1", jpnlAxisYProp);

        jpnlSerie.setBorder(javax.swing.BorderFactory.createTitledBorder("Title"));

        jcbxShowValues.setSelected(true);
        jcbxShowValues.setText("jCheckBox1");
        jcbxShowValues.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jcbxShowValues.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jcbxShowValues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbxShowValuesActionPerformed(evt);
            }
        });

        pnlFonte.setBorder(javax.swing.BorderFactory.createTitledBorder("Fonte"));
        pnlFonte.setPreferredSize(new java.awt.Dimension(268, 100));

        jlblSerieFont.setText("jLabel16");

        jcmbSerieFont.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jlblSerieSize.setText("jLabel17");

        jlblSerieColor.setText("jLabel18");

        org.jdesktop.layout.GroupLayout jpcSerieColorLayout = new org.jdesktop.layout.GroupLayout(jpcSerieColor);
        jpcSerieColor.setLayout(jpcSerieColorLayout);
        jpcSerieColorLayout.setHorizontalGroup(
            jpcSerieColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 100, Short.MAX_VALUE)
        );
        jpcSerieColorLayout.setVerticalGroup(
            jpcSerieColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout pnlFonteLayout = new org.jdesktop.layout.GroupLayout(pnlFonte);
        pnlFonte.setLayout(pnlFonteLayout);
        pnlFonteLayout.setHorizontalGroup(
            pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlFonteLayout.createSequentialGroup()
                .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(pnlFonteLayout.createSequentialGroup()
                        .add(jlblSerieFont)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jcmbSerieFont, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(pnlFonteLayout.createSequentialGroup()
                        .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jlblSerieSize)
                            .add(jlblSerieColor))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jpcSerieColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jspSerieSize, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        pnlFonteLayout.setVerticalGroup(
            pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlFonteLayout.createSequentialGroup()
                .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblSerieFont)
                    .add(jcmbSerieFont, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jlblSerieSize)
                    .add(jspSerieSize, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlFonteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblSerieColor)
                    .add(jpcSerieColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        pnlDecimais.setBorder(javax.swing.BorderFactory.createTitledBorder("Casas decimais"));

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Formata��o por indicador");
        jRadioButton1.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jRadioButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jRadioButton1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jRadioButton1StateChanged(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Formata��o �nica");
        jRadioButton2.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jRadioButton2.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jRadioButton2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jRadioButton2StateChanged(evt);
            }
        });

        spnCasasDecimais.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spnCasasDecimaisStateChanged(evt);
            }
        });

        jLabel25.setText("Casas decimais");

        org.jdesktop.layout.GroupLayout pnlDecimaisLayout = new org.jdesktop.layout.GroupLayout(pnlDecimais);
        pnlDecimais.setLayout(pnlDecimaisLayout);
        pnlDecimaisLayout.setHorizontalGroup(
            pnlDecimaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlDecimaisLayout.createSequentialGroup()
                .addContainerGap()
                .add(pnlDecimaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jRadioButton1)
                    .add(jRadioButton2)
                    .add(pnlDecimaisLayout.createSequentialGroup()
                        .add(17, 17, 17)
                        .add(jLabel25)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(spnCasasDecimais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 42, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        pnlDecimaisLayout.setVerticalGroup(
            pnlDecimaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlDecimaisLayout.createSequentialGroup()
                .addContainerGap()
                .add(jRadioButton1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButton2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlDecimaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel25)
                    .add(spnCasasDecimais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlEfeitos.setBorder(javax.swing.BorderFactory.createTitledBorder("Efeitos"));

        jcmbAxisSerieSobr.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jcmbAxisSerieIntes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jlblAxisSerieIntes.setText("jLabel23");

        jlblAxisSerieSobr.setText("jLabel24");

        org.jdesktop.layout.GroupLayout pnlEfeitosLayout = new org.jdesktop.layout.GroupLayout(pnlEfeitos);
        pnlEfeitos.setLayout(pnlEfeitosLayout);
        pnlEfeitosLayout.setHorizontalGroup(
            pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlEfeitosLayout.createSequentialGroup()
                .addContainerGap()
                .add(pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jlblAxisSerieIntes)
                    .add(jlblAxisSerieSobr))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jcmbAxisSerieSobr, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jcmbAxisSerieIntes, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(211, 211, 211))
        );
        pnlEfeitosLayout.setVerticalGroup(
            pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(pnlEfeitosLayout.createSequentialGroup()
                .addContainerGap()
                .add(pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblAxisSerieIntes)
                    .add(jcmbAxisSerieIntes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlEfeitosLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jlblAxisSerieSobr)
                    .add(jcmbAxisSerieSobr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jpnlSerieLayout = new org.jdesktop.layout.GroupLayout(jpnlSerie);
        jpnlSerie.setLayout(jpnlSerieLayout);
        jpnlSerieLayout.setHorizontalGroup(
            jpnlSerieLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlSerieLayout.createSequentialGroup()
                .add(pnlFonte, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 195, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlDecimais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(pnlEfeitos, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(jpnlSerieLayout.createSequentialGroup()
                .add(jcbxShowValues)
                .addContainerGap())
        );
        jpnlSerieLayout.setVerticalGroup(
            jpnlSerieLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlSerieLayout.createSequentialGroup()
                .add(jcbxShowValues)
                .add(14, 14, 14)
                .add(jpnlSerieLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, pnlEfeitos, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, pnlDecimais, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, pnlFonte, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
                .addContainerGap())
        );

        jpnlAxisXY.setBorder(javax.swing.BorderFactory.createTitledBorder("Title"));

        jlblAxisFont.setText("jLabel15");

        jcmbAxisFont.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jlblAxisSize.setText("jLabel21");

        jlblAxisColor.setText("jLabel22");

        org.jdesktop.layout.GroupLayout jpcAxisSizeLayout = new org.jdesktop.layout.GroupLayout(jpcAxisSize);
        jpcAxisSize.setLayout(jpcAxisSizeLayout);
        jpcAxisSizeLayout.setHorizontalGroup(
            jpcAxisSizeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 70, Short.MAX_VALUE)
        );
        jpcAxisSizeLayout.setVerticalGroup(
            jpcAxisSizeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnlAxisXYLayout = new org.jdesktop.layout.GroupLayout(jpnlAxisXY);
        jpnlAxisXY.setLayout(jpnlAxisXYLayout);
        jpnlAxisXYLayout.setHorizontalGroup(
            jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisXYLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jlblAxisColor)
                    .add(jlblAxisFont))
                .add(13, 13, 13)
                .add(jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlAxisXYLayout.createSequentialGroup()
                        .add(jcmbAxisFont, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(81, 81, 81)
                        .add(jlblAxisSize)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jspAxisSize, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jpcAxisSize, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(475, 475, 475))
        );
        jpnlAxisXYLayout.setVerticalGroup(
            jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlAxisXYLayout.createSequentialGroup()
                .add(jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlAxisXYLayout.createSequentialGroup()
                        .add(jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jlblAxisFont)
                            .add(jcmbAxisFont, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jlblAxisSize))
                        .add(jpnlAxisXYLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jpnlAxisXYLayout.createSequentialGroup()
                                .add(12, 12, 12)
                                .add(jlblAxisColor))
                            .add(jpnlAxisXYLayout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jpcAxisSize, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .add(jspAxisSize, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout jpcSerieBgColorLayout = new org.jdesktop.layout.GroupLayout(jpcSerieBgColor);
        jpcSerieBgColor.setLayout(jpcSerieBgColorLayout);
        jpcSerieBgColorLayout.setHorizontalGroup(
            jpcSerieBgColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 69, Short.MAX_VALUE)
        );
        jpcSerieBgColorLayout.setVerticalGroup(
            jpcSerieBgColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        jlblSerieBgColor.setText("jLabel19");

        jlblSerieBgGraphColor.setText("jLabel20");

        jlblBgImage.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jpetBgImageLayout = new org.jdesktop.layout.GroupLayout(jpetBgImage);
        jpetBgImage.setLayout(jpetBgImageLayout);
        jpetBgImageLayout.setHorizontalGroup(
            jpetBgImageLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 709, Short.MAX_VALUE)
        );
        jpetBgImageLayout.setVerticalGroup(
            jpetBgImageLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jcbxShowLegende.setSelected(true);
        jcbxShowLegende.setText("jCheckBox1");
        jcbxShowLegende.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jcbxShowLegende.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout jpcSerieBgGraphColorLayout = new org.jdesktop.layout.GroupLayout(jpcSerieBgGraphColor);
        jpcSerieBgGraphColor.setLayout(jpcSerieBgGraphColorLayout);
        jpcSerieBgGraphColorLayout.setHorizontalGroup(
            jpcSerieBgGraphColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 70, Short.MAX_VALUE)
        );
        jpcSerieBgGraphColorLayout.setVerticalGroup(
            jpcSerieBgGraphColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnlGraphPropLayout = new org.jdesktop.layout.GroupLayout(jpnlGraphProp);
        jpnlGraphProp.setLayout(jpnlGraphPropLayout);
        jpnlGraphPropLayout.setHorizontalGroup(
            jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlGraphPropLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlSerie, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jpnlAxisXY, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
                    .add(jpnlGraphPropLayout.createSequentialGroup()
                        .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jlblSerieBgColor)
                            .add(jlblBgImage))
                        .add(27, 27, 27)
                        .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jpetBgImage, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jpnlGraphPropLayout.createSequentialGroup()
                                .add(jpcSerieBgColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(68, 68, 68)
                                .add(jlblSerieBgGraphColor)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jpcSerieBgGraphColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 462, Short.MAX_VALUE))))
                    .add(jcbxShowLegende))
                .addContainerGap())
        );
        jpnlGraphPropLayout.setVerticalGroup(
            jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlGraphPropLayout.createSequentialGroup()
                .add(jpnlSerie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 161, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlAxisXY, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlGraphPropLayout.createSequentialGroup()
                            .add(jlblSerieBgColor)
                            .add(5, 5, 5))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jpnlGraphPropLayout.createSequentialGroup()
                            .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(jpcSerieBgGraphColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jlblSerieBgGraphColor))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                    .add(jpcSerieBgColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlGraphPropLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpetBgImage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblBgImage))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jcbxShowLegende)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jtbbProperties.addTab("tab3", jpnlGraphProp);

        jpnlZoneOne.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        org.jdesktop.layout.GroupLayout jpetTitleZoneOneLayout = new org.jdesktop.layout.GroupLayout(jpetTitleZoneOne);
        jpetTitleZoneOne.setLayout(jpetTitleZoneOneLayout);
        jpetTitleZoneOneLayout.setHorizontalGroup(
            jpetTitleZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 512, Short.MAX_VALUE)
        );
        jpetTitleZoneOneLayout.setVerticalGroup(
            jpetTitleZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneOneInitValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneOneInitValue);
        jpnZoneOneInitValue.setLayout(jpnZoneOneInitValueLayout);
        jpnZoneOneInitValueLayout.setHorizontalGroup(
            jpnZoneOneInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneOneInitValueLayout.setVerticalGroup(
            jpnZoneOneInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneOneLastValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneOneLastValue);
        jpnZoneOneLastValue.setLayout(jpnZoneOneLastValueLayout);
        jpnZoneOneLastValueLayout.setHorizontalGroup(
            jpnZoneOneLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneOneLastValueLayout.setVerticalGroup(
            jpnZoneOneLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblZoneOneInitValue.setText("jLabel1");
        jlblZoneOneInitValue.setEnabled(false);

        jlblZoneOneLastValue.setText("jLabel2");
        jlblZoneOneLastValue.setEnabled(false);

        jlblTitleZoneOne.setText("jLabel3");
        jlblTitleZoneOne.setEnabled(false);

        jlblBandZoneOne.setText("jLabel3");
        jlblBandZoneOne.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpcZoneOneColorLayout = new org.jdesktop.layout.GroupLayout(jpcZoneOneColor);
        jpcZoneOneColor.setLayout(jpcZoneOneColorLayout);
        jpcZoneOneColorLayout.setHorizontalGroup(
            jpcZoneOneColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 80, Short.MAX_VALUE)
        );
        jpcZoneOneColorLayout.setVerticalGroup(
            jpcZoneOneColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        jlblZoneOneColor.setText("jLabel1");
        jlblZoneOneColor.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnlZoneOneLayout = new org.jdesktop.layout.GroupLayout(jpnlZoneOne);
        jpnlZoneOne.setLayout(jpnlZoneOneLayout);
        jpnlZoneOneLayout.setHorizontalGroup(
            jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneOneLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jlblTitleZoneOne)
                    .add(jlblZoneOneColor)
                    .add(jlblBandZoneOne))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jpnlZoneOneLayout.createSequentialGroup()
                            .add(jpetTitleZoneOne, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(192, 192, 192))
                        .add(jpnlZoneOneLayout.createSequentialGroup()
                            .add(jlblZoneOneInitValue)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jpnZoneOneInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jlblZoneOneLastValue)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jpnZoneOneLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap()))
                    .add(jpnlZoneOneLayout.createSequentialGroup()
                        .add(jpcZoneOneColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jpnlZoneOneLayout.setVerticalGroup(
            jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneOneLayout.createSequentialGroup()
                .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblTitleZoneOne)
                    .add(jpetTitleZoneOne, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblBandZoneOne)
                    .add(jlblZoneOneInitValue)
                    .add(jpnZoneOneInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jpnZoneOneLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblZoneOneLastValue))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 11, Short.MAX_VALUE)
                .add(jpnlZoneOneLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblZoneOneColor)
                    .add(jpcZoneOneColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jckbZones.setText("jCheckBox1");
        jckbZones.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbZones.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jckbZones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jckbZonesKeyPressed(evt);
            }
        });
        jckbZones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckbZonesMouseClicked(evt);
            }
        });

        jpnlZoneTwo.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        org.jdesktop.layout.GroupLayout jpetTitleZoneTwoLayout = new org.jdesktop.layout.GroupLayout(jpetTitleZoneTwo);
        jpetTitleZoneTwo.setLayout(jpetTitleZoneTwoLayout);
        jpetTitleZoneTwoLayout.setHorizontalGroup(
            jpetTitleZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 530, Short.MAX_VALUE)
        );
        jpetTitleZoneTwoLayout.setVerticalGroup(
            jpetTitleZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneTwoInitValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneTwoInitValue);
        jpnZoneTwoInitValue.setLayout(jpnZoneTwoInitValueLayout);
        jpnZoneTwoInitValueLayout.setHorizontalGroup(
            jpnZoneTwoInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneTwoInitValueLayout.setVerticalGroup(
            jpnZoneTwoInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneTwoLastValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneTwoLastValue);
        jpnZoneTwoLastValue.setLayout(jpnZoneTwoLastValueLayout);
        jpnZoneTwoLastValueLayout.setHorizontalGroup(
            jpnZoneTwoLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneTwoLastValueLayout.setVerticalGroup(
            jpnZoneTwoLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblZoneTwoInitValue.setText("jLabel1");
        jlblZoneTwoInitValue.setEnabled(false);

        jlblZoneTwoLastValue.setText("jLabel2");
        jlblZoneTwoLastValue.setEnabled(false);

        jlblTitleZoneTwo.setText("jLabel3");
        jlblTitleZoneTwo.setEnabled(false);

        jlblBandZoneTwo.setText("jLabel3");
        jlblBandZoneTwo.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpcZoneTwoColorLayout = new org.jdesktop.layout.GroupLayout(jpcZoneTwoColor);
        jpcZoneTwoColor.setLayout(jpcZoneTwoColorLayout);
        jpcZoneTwoColorLayout.setHorizontalGroup(
            jpcZoneTwoColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 80, Short.MAX_VALUE)
        );
        jpcZoneTwoColorLayout.setVerticalGroup(
            jpcZoneTwoColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        jlblZoneTwoColor.setText("jLabel1");
        jlblZoneTwoColor.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnlZoneTwoLayout = new org.jdesktop.layout.GroupLayout(jpnlZoneTwo);
        jpnlZoneTwo.setLayout(jpnlZoneTwoLayout);
        jpnlZoneTwoLayout.setHorizontalGroup(
            jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneTwoLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(jlblTitleZoneTwo)
                        .add(jlblZoneTwoColor))
                    .add(jlblBandZoneTwo))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jpnlZoneTwoLayout.createSequentialGroup()
                            .add(jpetTitleZoneTwo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(174, 174, 174))
                        .add(jpnlZoneTwoLayout.createSequentialGroup()
                            .add(jlblZoneTwoInitValue)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jpnZoneTwoInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jlblZoneTwoLastValue)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jpnZoneTwoLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap()))
                    .add(jpnlZoneTwoLayout.createSequentialGroup()
                        .add(jpcZoneTwoColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jpnlZoneTwoLayout.setVerticalGroup(
            jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneTwoLayout.createSequentialGroup()
                .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblTitleZoneTwo)
                    .add(jpetTitleZoneTwo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblBandZoneTwo)
                    .add(jlblZoneTwoInitValue)
                    .add(jpnZoneTwoInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jpnZoneTwoLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblZoneTwoLastValue))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 9, Short.MAX_VALUE)
                .add(jpnlZoneTwoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblZoneTwoColor)
                    .add(jpcZoneTwoColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jpnlZoneThree.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        org.jdesktop.layout.GroupLayout jpetTitleZoneThreeLayout = new org.jdesktop.layout.GroupLayout(jpetTitleZoneThree);
        jpetTitleZoneThree.setLayout(jpetTitleZoneThreeLayout);
        jpetTitleZoneThreeLayout.setHorizontalGroup(
            jpetTitleZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 532, Short.MAX_VALUE)
        );
        jpetTitleZoneThreeLayout.setVerticalGroup(
            jpetTitleZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneThreeInitValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneThreeInitValue);
        jpnZoneThreeInitValue.setLayout(jpnZoneThreeInitValueLayout);
        jpnZoneThreeInitValueLayout.setHorizontalGroup(
            jpnZoneThreeInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneThreeInitValueLayout.setVerticalGroup(
            jpnZoneThreeInitValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout jpnZoneThreeLastValueLayout = new org.jdesktop.layout.GroupLayout(jpnZoneThreeLastValue);
        jpnZoneThreeLastValue.setLayout(jpnZoneThreeLastValueLayout);
        jpnZoneThreeLastValueLayout.setHorizontalGroup(
            jpnZoneThreeLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnZoneThreeLastValueLayout.setVerticalGroup(
            jpnZoneThreeLastValueLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        jlblZoneThreeInitValue.setText("jLabel1");
        jlblZoneThreeInitValue.setEnabled(false);

        jlblZoneThreeLastValue.setText("jLabel2");
        jlblZoneThreeLastValue.setEnabled(false);

        jlblTitleZoneThree.setText("jLabel3");
        jlblTitleZoneThree.setEnabled(false);

        jlblBandZoneThree.setText("jLabel3");
        jlblBandZoneThree.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpcZoneThreeColorLayout = new org.jdesktop.layout.GroupLayout(jpcZoneThreeColor);
        jpcZoneThreeColor.setLayout(jpcZoneThreeColorLayout);
        jpcZoneThreeColorLayout.setHorizontalGroup(
            jpcZoneThreeColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 80, Short.MAX_VALUE)
        );
        jpcZoneThreeColorLayout.setVerticalGroup(
            jpcZoneThreeColorLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 20, Short.MAX_VALUE)
        );

        jlblZoneThreeColor.setText("jLabel1");
        jlblZoneThreeColor.setEnabled(false);

        org.jdesktop.layout.GroupLayout jpnlZoneThreeLayout = new org.jdesktop.layout.GroupLayout(jpnlZoneThree);
        jpnlZoneThree.setLayout(jpnlZoneThreeLayout);
        jpnlZoneThreeLayout.setHorizontalGroup(
            jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneThreeLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jlblBandZoneThree)
                    .add(jlblTitleZoneThree)
                    .add(jlblZoneThreeColor))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jpnlZoneThreeLayout.createSequentialGroup()
                        .add(jpcZoneThreeColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(jpnlZoneThreeLayout.createSequentialGroup()
                        .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlZoneThreeLayout.createSequentialGroup()
                                .add(jlblZoneThreeInitValue)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jpnZoneThreeInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jlblZoneThreeLastValue)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jpnZoneThreeLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jpetTitleZoneThree, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .add(172, 172, 172))))
        );
        jpnlZoneThreeLayout.setVerticalGroup(
            jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZoneThreeLayout.createSequentialGroup()
                .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblTitleZoneThree)
                    .add(jpetTitleZoneThree, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jlblBandZoneThree)
                    .add(jlblZoneThreeInitValue)
                    .add(jpnZoneThreeInitValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jpnZoneThreeLastValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblZoneThreeLastValue))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jpnlZoneThreeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jpcZoneThreeColor, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jlblZoneThreeColor)))
        );

        org.jdesktop.layout.GroupLayout jpnlZonesLayout = new org.jdesktop.layout.GroupLayout(jpnlZones);
        jpnlZones.setLayout(jpnlZonesLayout);
        jpnlZonesLayout.setHorizontalGroup(
            jpnlZonesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZonesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jpnlZonesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlZoneThree, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jckbZones)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlZoneTwo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jpnlZoneOne, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpnlZonesLayout.setVerticalGroup(
            jpnlZonesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jpnlZonesLayout.createSequentialGroup()
                .add(jckbZones)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneOne, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneTwo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jpnlZoneThree, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        jtbbProperties.addTab("tab4", jpnlZones);

        jbtnCancel.setText("jButton1");
        jbtnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jbtnCancelMousePressed(evt);
            }
        });

        jbtnOk.setText("jButton2");
        jbtnOk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jbtnOkMousePressed(evt);
            }
        });

        jlblDrillDown.setText("jLabel6");

        jckbDrillDown.setText("jCheckBox4");
        jckbDrillDown.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jckbDrillDown.setMargin(new java.awt.Insets(0, 0, 0, 0));

        jlblDotPagAdic.setText("jLabel13");

        org.jdesktop.layout.GroupLayout jpnDotPagAdicLayout = new org.jdesktop.layout.GroupLayout(jpnDotPagAdic);
        jpnDotPagAdic.setLayout(jpnDotPagAdicLayout);
        jpnDotPagAdicLayout.setHorizontalGroup(
            jpnDotPagAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 96, Short.MAX_VALUE)
        );
        jpnDotPagAdicLayout.setVerticalGroup(
            jpnDotPagAdicLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 16, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jtbbProperties)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jlblDotPagAdic)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jpnDotPagAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(jlblDrillDown)
                        .add(10, 10, 10)
                        .add(jckbDrillDown)))
                .addContainerGap(641, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(629, Short.MAX_VALUE)
                .add(jbtnOk)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jbtnCancel)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jtbbProperties, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 355, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jlblDrillDown)
                            .add(jckbDrillDown))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jlblDotPagAdic)
                            .add(jpnDotPagAdic, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(34, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jbtnCancel)
                            .add(jbtnOk))
                        .addContainerGap())))
        );
    }// </editor-fold>//GEN-END:initComponents

	private void jRadioButton2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jRadioButton2StateChanged
		enabledCasasDecimais();
	}//GEN-LAST:event_jRadioButton2StateChanged

	private void jRadioButton1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jRadioButton1StateChanged
		enabledCasasDecimais();
	}//GEN-LAST:event_jRadioButton1StateChanged

	private void spnCasasDecimaisStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spnCasasDecimaisStateChanged
		if(Integer.parseInt(spnCasasDecimais.getValue().toString()) < 0){
			spnCasasDecimais.setValue(0);
		}else if(Integer.parseInt(spnCasasDecimais.getValue().toString()) > 15){
			spnCasasDecimais.setValue(15);
		}
	}//GEN-LAST:event_spnCasasDecimaisStateChanged
	
	private void jcbxShowValuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbxShowValuesActionPerformed
		if (this.jcbxShowValues.isSelected()){
			setEnableFields(pnlFonte,true);
			setEnableFields(pnlDecimais,true);
			setEnableFields(pnlEfeitos,true);
			enabledCasasDecimais();	
		}else{
			setEnableFields(pnlFonte,false);
			setEnableFields(pnlDecimais,false);
			setEnableFields(pnlEfeitos,false);
		}
	}//GEN-LAST:event_jcbxShowValuesActionPerformed
	
    private void jckbZonesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbZonesMouseClicked
		if (this.jckbZones.isEnabled()) {
			this.jckbZonesKeyEvent(evt);
		}
    }//GEN-LAST:event_jckbZonesMouseClicked
	
    private void jckbZonesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbZonesKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbZonesKeyEvent(evt);
		}
    }//GEN-LAST:event_jckbZonesKeyPressed
	
    private void jckbScaleBaseAdicKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScaleBaseAdicKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScaleBaseAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScaleBaseAdicKeyPressed
	
    private void jckbScaleBaseAdicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScaleBaseAdicMouseClicked
		if (this.jckbScaleBaseAdic.isEnabled()) {
			this.jckbScaleBaseAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScaleBaseAdicMouseClicked
	
    private void jckbScalAutAdicKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScalAutAdicKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScalAutAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScalAutAdicKeyPressed
	
    private void jckbScalLogAdicKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScalLogAdicKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScalLogAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScalLogAdicKeyPressed
	
    private void jckbAxisYAdicKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbAxisYAdicKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbAxisYAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbAxisYAdicKeyPressed
	
    private void jckbScaleAutomKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScaleAutomKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScaleAutomEvent(evt);
		}
    }//GEN-LAST:event_jckbScaleAutomKeyPressed
	
    private void jckbScaleLogarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScaleLogarKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScaleLogarEvent(evt);
		}
    }//GEN-LAST:event_jckbScaleLogarKeyPressed
	
    private void jckbScaleBaseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jckbScaleBaseKeyPressed
		if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
			this.jckbScaleBaseEvent(evt);
		}
    }//GEN-LAST:event_jckbScaleBaseKeyPressed
	
    private void jbtnOkMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbtnOkMousePressed
		DefinitionProperties.getInstance().setGraphYProps( retrieveGraphProperties(this.GRAPH_PROPS_Y) );
		DefinitionProperties.getInstance().setGraphProps( retrieveGraphProperties(this.GRAPH_PROPS) );
		this.setVisible(false);
    }//GEN-LAST:event_jbtnOkMousePressed
	
	
    private void jckbAxisYAdicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbAxisYAdicMouseClicked
		this.jckbAxisYAdicEvent(evt);
    }//GEN-LAST:event_jckbAxisYAdicMouseClicked
	
    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
		this.setVisible(false);
    }//GEN-LAST:event_formWindowClosed
	
    private void jckbScalLogAdicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScalLogAdicMouseClicked
		if (this.jckbScalLogAdic.isEnabled()) {
			this.jckbScalLogAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScalLogAdicMouseClicked
	
    private void jckbScalAutAdicMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScalAutAdicMouseClicked
		if (this.jckbScalAutAdic.isEnabled()) {
			this.jckbScalAutAdicEvent(evt);
		}
    }//GEN-LAST:event_jckbScalAutAdicMouseClicked
	
    private void jbtnCancelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbtnCancelMousePressed
		this.setVisible(false);
    }//GEN-LAST:event_jbtnCancelMousePressed
	
    private void jckbScaleAutomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScaleAutomMouseClicked
		this.jckbScaleAutomEvent(evt);
    }//GEN-LAST:event_jckbScaleAutomMouseClicked
	
    private void jckbScaleLogarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScaleLogarMouseClicked
		this.jckbScaleLogarEvent(evt);
    }//GEN-LAST:event_jckbScaleLogarMouseClicked
	
    private void jckbScaleBaseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckbScaleBaseMouseClicked
		this.jckbScaleBaseEvent(evt);
    }//GEN-LAST:event_jckbScaleBaseMouseClicked
	
    private void changeComponentEnabled(JComponent comp) {
        this.changeComponentEnabled(comp, !comp.isEnabled());
    }
	
    private void changeComponentEnabled(JComponent comp, boolean blnEnabled) {
	comp.setEnabled(blnEnabled);
    }
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JButton jbtnCancel;
    private javax.swing.JButton jbtnOk;
    private javax.swing.JCheckBox jcbxShowLegende;
    private javax.swing.JCheckBox jcbxShowValues;
    private javax.swing.JCheckBox jckbAxisYAdic;
    private javax.swing.JCheckBox jckbDrillDown;
    private javax.swing.JCheckBox jckbScalAutAdic;
    private javax.swing.JCheckBox jckbScalLogAdic;
    private javax.swing.JCheckBox jckbScaleAutom;
    private javax.swing.JCheckBox jckbScaleBase;
    private javax.swing.JCheckBox jckbScaleBaseAdic;
    private javax.swing.JCheckBox jckbScaleLogar;
    private javax.swing.JCheckBox jckbZones;
    private javax.swing.JComboBox jcmbAxisFont;
    private javax.swing.JComboBox jcmbAxisSerieIntes;
    private javax.swing.JComboBox jcmbAxisSerieSobr;
    private javax.swing.JComboBox jcmbSerieFont;
    private javax.swing.JLabel jlblAxisColor;
    private javax.swing.JLabel jlblAxisFont;
    private javax.swing.JLabel jlblAxisSerieIntes;
    private javax.swing.JLabel jlblAxisSerieSobr;
    private javax.swing.JLabel jlblAxisSize;
    private javax.swing.JLabel jlblBandZoneOne;
    private javax.swing.JLabel jlblBandZoneThree;
    private javax.swing.JLabel jlblBandZoneTwo;
    private javax.swing.JLabel jlblBaseAdic;
    private javax.swing.JLabel jlblBgImage;
    private javax.swing.JLabel jlblDotPagAdic;
    private javax.swing.JLabel jlblDrillDown;
    private javax.swing.JLabel jlblMarca;
    private javax.swing.JLabel jlblMarcaAdic;
    private javax.swing.JLabel jlblScalAutIncrAdic;
    private javax.swing.JLabel jlblScalAutMaxAdic;
    private javax.swing.JLabel jlblScalAutMinAdic;
    private javax.swing.JLabel jlblScalLogBaseAdic;
    private javax.swing.JLabel jlblScaleAutomIncr;
    private javax.swing.JLabel jlblScaleAutomMax;
    private javax.swing.JLabel jlblScaleAutomMin;
    private javax.swing.JLabel jlblScaleLogarBase;
    private javax.swing.JLabel jlblScaleMeta;
    private javax.swing.JLabel jlblSerieBgColor;
    private javax.swing.JLabel jlblSerieBgGraphColor;
    private javax.swing.JLabel jlblSerieColor;
    private javax.swing.JLabel jlblSerieFont;
    private javax.swing.JLabel jlblSerieSize;
    private javax.swing.JLabel jlblTitle;
    private javax.swing.JLabel jlblTitleAdic;
    private javax.swing.JLabel jlblTitleZoneOne;
    private javax.swing.JLabel jlblTitleZoneThree;
    private javax.swing.JLabel jlblTitleZoneTwo;
    private javax.swing.JLabel jlblZoneOneColor;
    private javax.swing.JLabel jlblZoneOneInitValue;
    private javax.swing.JLabel jlblZoneOneLastValue;
    private javax.swing.JLabel jlblZoneThreeColor;
    private javax.swing.JLabel jlblZoneThreeInitValue;
    private javax.swing.JLabel jlblZoneThreeLastValue;
    private javax.swing.JLabel jlblZoneTwoColor;
    private javax.swing.JLabel jlblZoneTwoInitValue;
    private javax.swing.JLabel jlblZoneTwoLastValue;
    private pv.jfcx.JPVNumeric jnScalAutIncrAdic;
    private pv.jfcx.JPVColor jpcAxisSize;
    private pv.jfcx.JPVColor jpcSerieBgColor;
    private pv.jfcx.JPVColor jpcSerieBgGraphColor;
    private pv.jfcx.JPVColor jpcSerieColor;
    private pv.jfcx.JPVColor jpcZoneOneColor;
    private pv.jfcx.JPVColor jpcZoneThreeColor;
    private pv.jfcx.JPVColor jpcZoneTwoColor;
    private pv.jfcx.JPVEdit jpeTitleAdic;
    private pv.jfcx.JPVEdit jpetBgImage;
    private pv.jfcx.JPVEdit jpetMarca;
    private pv.jfcx.JPVEdit jpetMarcaAdic;
    private pv.jfcx.JPVEdit jpetTitle;
    private pv.jfcx.JPVEdit jpetTitleZoneOne;
    private pv.jfcx.JPVEdit jpetTitleZoneThree;
    private pv.jfcx.JPVEdit jpetTitleZoneTwo;
    private pv.jfcx.JPVNumeric jpnBaseAdic;
    private pv.jfcx.JPVNumeric jpnDotPagAdic;
    private pv.jfcx.JPVNumeric jpnScalAutMaxAdic;
    private pv.jfcx.JPVNumeric jpnScalAutMinAdic;
    private pv.jfcx.JPVNumeric jpnScalLogBaseAdic;
    private pv.jfcx.JPVNumeric jpnScaleAutomIncr;
    private pv.jfcx.JPVNumeric jpnScaleAutomMax;
    private pv.jfcx.JPVNumeric jpnScaleAutomMin;
    private pv.jfcx.JPVNumeric jpnScaleLogarBase;
    private pv.jfcx.JPVNumeric jpnScaleMeta;
    private pv.jfcx.JPVNumeric jpnZoneOneInitValue;
    private pv.jfcx.JPVNumeric jpnZoneOneLastValue;
    private pv.jfcx.JPVNumeric jpnZoneThreeInitValue;
    private pv.jfcx.JPVNumeric jpnZoneThreeLastValue;
    private pv.jfcx.JPVNumeric jpnZoneTwoInitValue;
    private pv.jfcx.JPVNumeric jpnZoneTwoLastValue;
    private javax.swing.JPanel jpnlAxisXY;
    private javax.swing.JPanel jpnlAxisYAdic;
    private javax.swing.JPanel jpnlAxisYProp;
    private javax.swing.JPanel jpnlGraphProp;
    private javax.swing.JPanel jpnlScale;
    private javax.swing.JPanel jpnlScaleAdic;
    private javax.swing.JPanel jpnlSerie;
    private javax.swing.JPanel jpnlZoneOne;
    private javax.swing.JPanel jpnlZoneThree;
    private javax.swing.JPanel jpnlZoneTwo;
    private javax.swing.JPanel jpnlZones;
    private javax.swing.JSpinner jspAxisSize;
    private javax.swing.JSpinner jspSerieSize;
    private javax.swing.JTabbedPane jtbbProperties;
    private javax.swing.JPanel pnlDecimais;
    private javax.swing.JPanel pnlEfeitos;
    private javax.swing.JPanel pnlFonte;
    private javax.swing.JSpinner spnCasasDecimais;
    // End of variables declaration//GEN-END:variables
	
}