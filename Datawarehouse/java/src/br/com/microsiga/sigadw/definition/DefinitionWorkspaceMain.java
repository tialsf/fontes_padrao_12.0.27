package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.event.DwActionListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import javax.swing.JPanel;

/**
 * Interface que define a classe b�sica de painel principal da defini��o e para recupera��o dos valores nos eixos
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 28 de Abril de 2006
 */
public abstract class DefinitionWorkspaceMain extends JPanel
        implements DwActionListener<DwButton> {
    
    public DefinitionWorkspaceMain() {
        this.setBackground(DefinitionProperties.getInstance().getAppletsBgcolor());
    }
    
    /**
     * <pre>M�todo respons�vel por adicionar um listener que ir� obter informa��es sobre
     *  determinado bot�o em que o usu�rio clicar
     * 
     * @param listener
     *      Objeto do tipo <code>DwActionListener<DwButton></code> contendo um listener "generic" para um DwButton
     */
    public abstract void addInformationListener(DwActionListener<DwButton> listener);
    
    /**
     * <pre>M�todo respons�vel por retornar as propriedades dos bot�es do eixo X
     * 
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades
     */
    public abstract String getAxisXProperties();
    
    /**
     * <pre>M�todo respons�vel por retornar as propriedades dos bot�es do eixo Y
     * 
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades
     */
    public abstract String getAxisYProperties();
    
    /**
     * <pre>M�todo respons�vel por retornar as propriedades dos bot�es do eixo dos Indicadores
     * 
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades
     */
    public abstract String getAxisMeasureProperties();
    
    /**
     * M�todo respons�vel por realizar a atualiza��o TOTAL da defini��o
     */
    public abstract void updateDefinition();
    
    /**
     * <pre>M�todo respons�vel por realizar a atualiza��o de um eixo desta defini��o
     * 
     * @param intAxisType
     *      Primitiva do tipo <code>int</code> contendo o c�digo do tipo do eixo a ser atualizado
     * </pre>
     */
    public abstract void updateDefinition(int intAxisType);
    
}