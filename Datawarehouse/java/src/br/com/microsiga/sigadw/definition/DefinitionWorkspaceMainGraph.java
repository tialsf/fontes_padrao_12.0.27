package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.event.DwActionListener;
import br.com.microsiga.sigadw.event.DwCRUDListener;
import br.com.microsiga.sigadw.event.DwDrillDownListener;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.DrillDownVisibility;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import br.com.microsiga.sigadw.definition.to.GraphType;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Constr�i a apresenta��o do workspace para a defini��o de um gr�fico
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 22 de Abril de 2006, 17:26
 */
public class DefinitionWorkspaceMainGraph extends DefinitionWorkspaceMain
        implements DwCRUDListener<DwAxis, DwButton>, DwDrillDownListener<DwAxis>,
            ActionListener, MouseListener {
    
    /** propriedade que cont�m o listener para obter informa��es sobre cada bot�o do sigadw ao ser clicado pelo usu�rio */
    private DwActionListener<DwButton> listener;
    
    /** propriedade que cont�m as propriedades para esta defini��o */
    private DefinitionProperties properties = DefinitionProperties.getInstance();
    
    /** conter� o eixo aonde ocorreu o drill down */
    private DwAxis drillDonwAxis;
    
    /**
     * Creates new form DefinitionTableWorkspaceMain
     */
    public DefinitionWorkspaceMainGraph() {
        initComponents();
        
        this.addMouseListener(this);
        
        dwAxisX.addActionListener(this);
        dwAxisX.getRootDragPanel().addMouseListener(this);
        dwAxisMeasures.addActionListener(this);
        dwAxisMeasures.getRootDragPanel().addMouseListener(this);
        
        // adiciona um listener de evento de drill down
        dwAxisX.addDrillDownListener(this);
        
        // adiciona um listener de evento de adi��o e exclus�o de bot�es
        dwAxisX.addCRUDListener(this);
        dwAxisMeasures.addCRUDListener(this);
    }
    
    public void addInformationListener(DwActionListener<DwButton> listener) {
        this.listener = listener;
    }
    
    public void notifyActionListener(DwButton dwComponentSrc) {
        if (this.listener != null) {
            this.listener.notifyActionListener(dwComponentSrc);
        }
    }
    
    public void notifyDrillDown(boolean blnPropertyValue) {
        this.notifyDrillDown(blnPropertyValue, null);
    }
    
    public void notifyDrillDown(boolean blnPropertyValue, DwAxis componentSrc) {
        this.drillDonwAxis = componentSrc;
        DwButtonMeasureGraph dwButton;
        
        // verifica se o drill down estiver sendo desabilitado
        if (!blnPropertyValue) {
            // inv�lida o objeto de que cont�m o eixo aonde ocorreu o drill down anterior
            this.drillDonwAxis = null;
        }
        
        // itera pelos bot�es do eixo dos indicadores, validando a lista de visibilidade nos bot�es de indicadores
        for (Iterator it = dwAxisMeasures.getAxisButtons().iterator(); it.hasNext();) {
            dwButton = (DwButtonMeasureGraph) it.next();
            // se for evento ativa��o de drill down e tiver sido no eixo X do gr�fico
            if (blnPropertyValue && componentSrc.getAxisType() == DwAxis.AXIS_X_GRAPH) {
                // itera pelos bot�es no eixo aonde ocorreu o drill down, criando um objeto de visibilidade para cada bot�o
                // e adicionado na lista de visibilidade para os bot�es no eixo dos indicadores
                DwButtonAxisXGraph dwAxisButton;
                List<DrillDownVisibility> lstVisibility = new ArrayList<DrillDownVisibility>();
                for (Iterator itAxis = componentSrc.getAxisButtons().iterator(); itAxis.hasNext();) {
                    dwAxisButton = (DwButtonAxisXGraph) itAxis.next();
                    lstVisibility.add(new DrillDownVisibility(dwAxisButton.isActiveDriilDown(), !dwAxisButton.isActiveDriilDown(), dwAxisButton));
                }
                
                // adiciona a lista de visibilidade para o bot�o do indicador
                dwButton.injectDrillDownVisibility(lstVisibility);
            } else {
                // remove a lista de visibilidade para o bot�o do indicador
                dwButton.removeDrillVisibility();
            }
        }
        
        // notifica todos os bot�es sobre o tipo de gr�fico padr�o
        this.notifyDefaultGraphType();
    }
    
    public boolean canAdd(DwAxis dwAxisSrc, DwButton dwComponent) {
        boolean blnCanAdd = true;
        
        // verifica se o eixo � o eixo X e este j� possue um bot�o igual ao que est� sendo adicionado
        if (dwAxisSrc.getAxisType() == DwAxis.AXIS_X_GRAPH) {
            if (dwAxisSrc.getAxisButtons().contains(dwComponent)) {
                blnCanAdd = false;
            }
        } else if (dwAxisSrc.getAxisType() == DwAxis.AXIS_MEASURE_GRAPH) {
            if (dwAxisSrc.getAxisButtons().contains(dwComponent)) {
                ((DwButtonMeasure) dwComponent).setAgregFunctID(((DwButtonMeasure) dwComponent).getAgregFunctID()+1);
            }
        }
        
        return blnCanAdd;
    }
    
    public void notifyCreate(DwAxis dwAxisSrc, DwButton dwComponent) {
        if (this.drillDonwAxis != null && this.drillDonwAxis.getAxisType() == DwAxis.AXIS_X_GRAPH) {
            List<DrillDownVisibility> drillDown = new ArrayList<DrillDownVisibility>();
            DwButtonAxisXGraph dwAxisButton;
            for (Iterator itAxis = this.drillDonwAxis.getAxisButtons().iterator(); itAxis.hasNext();) {
                dwAxisButton = (DwButtonAxisXGraph) itAxis.next();
                drillDown.add(new DrillDownVisibility(dwAxisButton.isActiveDriilDown(), !dwAxisButton.isActiveDriilDown(), dwAxisButton));
            }
            
            if (dwComponent instanceof DwButtonMeasureGraph) {
                ((DwButtonMeasureGraph) dwComponent).injectDrillDownVisibility(drillDown);
            } else if (dwComponent instanceof DwButtonAxisXGraph) {
                DwButtonMeasureGraph dwButtonGraph;
                for (Iterator it = dwAxisMeasures.getAxisButtons().iterator(); it.hasNext(); ) {
                    dwButtonGraph = (DwButtonMeasureGraph) it.next();
                    dwButtonGraph.injectDrillDownVisibility(drillDown);
                }
            } else {
                System.err.println("Component inexpected. Creating component: " + dwComponent);
            }
        }
    }
    
    public void notifyDelete(DwAxis dwAxisSrc, DwButton dwComponent) {
        if (this.drillDonwAxis != null) {
            List<DrillDownVisibility> drillDown = DrillDownVisibility.generateVisibility(this.drillDonwAxis.getAxisButtons());
            if (dwComponent instanceof DwButtonMeasureGraph) {
                ((DwButtonMeasureGraph) dwComponent).injectDrillDownVisibility(drillDown);
            } else if (dwComponent instanceof DwButtonAxisXGraph) {
                DwButtonMeasureGraph dwButtonGraph;
                for (Iterator it = dwAxisMeasures.getAxisButtons().iterator(); it.hasNext(); ) {
                    dwButtonGraph = (DwButtonMeasureGraph) it.next();
                    dwButtonGraph.injectDrillDownVisibility(drillDown);
                }
            } else {
                System.err.println("Component inexpected. Deleting component: " + dwComponent);
            }
        }
    }
    
    public void notifyDefaultGraphType() {
        // itera pelo eixo x, definindo o tipo de gr�fico padr�o para cada bot�o no eixo
        for (Iterator it = dwAxisX.getAxisButtons().iterator(); it.hasNext(); ) {
            this.notifyDefaultGraphType( (DwButtonAxisXGraph) it.next() );
        }
        dwAxisX.updateAxisProperties();
    }
    
    public void notifyDefaultGraphType(DwButtonAxisXGraph dwButton) {
        dwButton.setDefaultGraphType(properties.getGraphType());
    }
    
    public String getAxisXProperties() {
        return dwAxisX.getAxisProprieties();
    }
    
    public String getAxisYProperties() {
        return null;
    }
    
    public String getAxisMeasureProperties() {
        return dwAxisMeasures.getAxisProprieties();
    }
    
    protected void definePopupMenu(java.awt.event.MouseEvent evt) {
        
        if (evt.getButton() != evt.BUTTON1 && evt.isPopupTrigger()) {
            JPopupMenu jpmProperties = new JPopupMenu();
            
            JMenuItem jmi = new JMenuItem(properties.getProps());
            jmi.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    DefinitionGraphProperties graphProperties = new DefinitionGraphProperties(new JFrame(), properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_PROPS_Y).getName(), true);
                    graphProperties.pack();
                    graphProperties.setVisible(true);
                }
            });
            jpmProperties.add(jmi);
            
            JMenu jmiGraphType = new JMenu(properties.getGraphTypeLabel());
            JMenuItem jmiType;
            for (GraphType graphType : properties.getGraphTypes()) {
                jmiType = new JMenuItem(graphType.getName(), graphType.getIconElement());
                jmiType.setActionCommand(String.valueOf(graphType.getId()));
                jmiType.addActionListener(this);
                jmiGraphType.add(jmiType);
            }
            jpmProperties.add(jmiGraphType);
            
            jpmProperties.show((JComponent) evt.getSource(), evt.getX(), evt.getY());
        }
        
    }
    
    public void actionPerformed(ActionEvent e) {
        // recupera o id relacionado ao gr�fico escolhido pelo usu�rio
        int intGrahType =  Integer.parseInt( ((JMenuItem) e.getSource()).getActionCommand() );
        
        // define a propriedade do tipo de gr�fico
        properties.setGraphType(intGrahType);
        
        // notifica os bot�es acerca da altera��o do tipo de gr�fico padr�o
        this.notifyDefaultGraphType();
    }
    
    public void updateDefinition() {
        dwAxisX.updateDefinition();
        dwAxisMeasures.updateDefinition();
    }
    
    public void updateDefinition(int intAxisType) {
        switch (intAxisType) {
            case DwAxis.AXIS_X:
            case DwAxis.AXIS_X_GRAPH:
                dwAxisX.updateDefinition();
                break;
            case DwAxis.AXIS_MEASURE_GRAPH:
                dwAxisMeasures.updateDefinition();
                
                // verifica se no eixo x tem o drill down ativo
                this.notifyDrillDown(dwAxisX.isActiveDrillDown(), dwAxisX);
                
                break;
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        dwAxisMeasures = new DwAxis(properties.getAxisMeasures(), DwAxis.AXIS_MEASURE_GRAPH);
        dwAxisX = new DwAxis(properties.getAxisX(), DwAxis.AXIS_X_GRAPH);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(dwAxisX, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
            .add(dwAxisMeasures, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(dwAxisMeasures, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(dwAxisX, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    public void mouseClicked(MouseEvent e) {
        definePopupMenu(e);
    }
    
    public void mousePressed(MouseEvent e) {
        definePopupMenu(e);
    }
    
    public void mouseReleased(MouseEvent e) {
        definePopupMenu(e);
    }
    
    public void mouseEntered(MouseEvent e) {
        definePopupMenu(e);
    }
    
    public void mouseExited(MouseEvent e) {
        definePopupMenu(e);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private br.com.microsiga.sigadw.definition.DwAxis dwAxisMeasures;
    private br.com.microsiga.sigadw.definition.DwAxis dwAxisX;
    // End of variables declaration//GEN-END:variables
    
}