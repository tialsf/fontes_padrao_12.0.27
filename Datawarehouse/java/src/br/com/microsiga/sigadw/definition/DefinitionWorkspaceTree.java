package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragGestureListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragSourceListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDropTargetListener;
import br.com.microsiga.sigadw.event.dragndrop.DwTransferable;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Color;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

/**
 * Construir a �rvore de elementos do DW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17 de Abril de 2006
 */
public class DefinitionWorkspaceTree extends JPanel implements DwDragDropEventListener<DwObject> {

    protected DefinitionProperties properties = DefinitionProperties.getInstance();
    protected DragSource dragSource;
    protected DragSourceListener dragSourceListener;
    protected DragGestureListener dragGestureListener;
    
    /** Creates new form DWElements */
    public DefinitionWorkspaceTree() {
        initComponents();
        
        this.dragSource = DragSource.getDefaultDragSource();
        this.dragSourceListener = new DwDragSourceListener();
        this.dragGestureListener = new DwDragGestureListener(this.dragSourceListener);
        
        this.dragSource.createDefaultDragGestureRecognizer(jtreeDwElements, DnDConstants.ACTION_COPY, this.dragGestureListener);
        
        DwDropTargetListener<DwObject> dwDDListener = new DwDropTargetListener<DwObject>(this, DwTransferable.sigaDwObject);
        new DropTarget(this.jtreeDwElements, DnDConstants.ACTION_COPY, dwDDListener, true);
    }
    
    public void defineDwElements() {
        jtreeDwElements.removeAll();
        
        // cria o n� raiz da �rvore
        DefaultMutableTreeNode dmtCube = new DefaultMutableTreeNode(properties.getCube());
        dmtCube.setUserObject(properties.getCube());
        
        // cria o n� contendo as dimens�es e os seus respectivos campos
        DefaultMutableTreeNode dmtDimensions = new DefaultMutableTreeNode(properties.getDimension());
        DefaultMutableTreeNode dmtDim;
        DefaultMutableTreeNode dmtFields;
        DefaultMutableTreeNode dmtFieldTemp;
        for (Dimension dimension : properties.getCube().getDimensions()) {
            dmtDim = new DefaultMutableTreeNode(dimension);
            dmtDim.setUserObject(dimension);
            dmtDimensions.add(dmtDim);
            
            // cria o n� contendo os campoas para a dimens�o em quest�o
            DefaultMutableTreeNode dmtDimension;
            for (DwField dimField : dimension.getDimFields()) {
                dmtFields = new DefaultMutableTreeNode(dimField);
                dmtFields.setUserObject(dimField);
                dmtDim.add(dmtFields);
                
                // verifica se o campo possui campos temporais
                if (dimField.hasTemporal()) {
                    // itera pelos valores temporais, acrescentando na �rvore
                    for (DwField fieldTemporal : dimField.getTemporal()) {
                        dmtFieldTemp = new DefaultMutableTreeNode(fieldTemporal);
                        dmtFieldTemp.setUserObject(fieldTemporal);
                        dmtFields.add(dmtFieldTemp);
                    }
                }
            }
        }
        dmtCube.add(dmtDimensions);
        TreeNode[] path = dmtCube.getPath();
        
        // cria o n� contendo os indicadores
        DefaultMutableTreeNode dmtMeasures = new DefaultMutableTreeNode(properties.getMeasures());
        DefaultMutableTreeNode dmtMeasure;
        for (DwObject dwObject : properties.getCube().getMeasures()) {
            dmtMeasure = new DefaultMutableTreeNode(dwObject);
            dmtMeasure.setUserObject(dwObject);
            dmtMeasures.add(dmtMeasure);
        }
        dmtCube.add(dmtMeasures);
        
        // cria o modelo que ser� definido para a �rvore
        DefaultTreeModel treeModel = new DefaultTreeModel(dmtCube);
        jtreeDwElements.setModel(treeModel);
        
        // modifica as imagens dos n�s folhas e n�o folhas
        DwTreeCellRenderer dtcr = new DwTreeCellRenderer();
        
        // define os icones da jtree
        dtcr.setLeafIcon(properties.getTreeLeaf());
        dtcr.setOpenIcon(properties.getTreeOpen());
        dtcr.setClosedIcon(properties.getTreeClose());
        
        jtreeDwElements.setCellRenderer(dtcr);
        
        // atualiza a �rvore
        treeModel.reload();
        
        // expand os n�s da �rvore, deixando o n� da primeira dimens�o em aberto
        jtreeDwElements.expandRow(1);
        jtreeDwElements.expandRow(properties.getCube().getDimensions().size() + 2);
        
        jtreeDwElements.updateUI();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jscplTree = new javax.swing.JScrollPane();
        jtreeDwElements = new javax.swing.JTree();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.X_AXIS));

        jscplTree.setViewportView(jtreeDwElements);

        add(jscplTree);

    }// </editor-fold>//GEN-END:initComponents

    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (this.jtreeDwElements != null) {
            DefaultTreeCellRenderer cellRender = (DefaultTreeCellRenderer) this.jtreeDwElements.getCellRenderer();
            if (cellRender != null) {
                cellRender.setBackground(bg);
                cellRender.setBackgroundNonSelectionColor(bg);
                cellRender.repaint();
                cellRender.updateUI();
            }
            this.repaint();
            this.updateUI();
        }
    }
    
    public void notifyDragDrop(DwObject dwObject) {
        
    }
    
    public void notifyDropEnd() {
        
    }
    
    public void notifyDropEnd(DwObject dwObject) {
        
    }
    
    public void handleDropAccept(Point point) {
        setBackground(properties.getAcceptDropColor());
    }

    public void handleDropReject(Point point) {
        setBackground(properties.getRejectDragColor());
    }

    public void handleDropExit() {
        setBackground(properties.getAppletsWorkspaceBgcolor());
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jscplTree;
    private javax.swing.JTree jtreeDwElements;
    // End of variables declaration//GEN-END:variables
    
}