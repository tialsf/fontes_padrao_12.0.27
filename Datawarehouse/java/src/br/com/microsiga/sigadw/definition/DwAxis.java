package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.event.DwActionListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.DwCRUDListener;
import br.com.microsiga.sigadw.event.DwDrillDownListener;
import br.com.microsiga.sigadw.event.DwPropertyChangeListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDropTargetListener;
import br.com.microsiga.sigadw.event.dragndrop.DwTransferable;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.TitledBorder;

/**
 * Objeto respons�vel por encapsular um "eixo" dispon�vel para o sigadw
 *  dependendo se � uma tabela ou gr�fico
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17/04/2006
 */
public class DwAxis extends javax.swing.JPanel
        implements DwPropertyChangeListener<DwButton>,
                    DwDragDropEventListener<DwObject>, ActionListener {
    
    private DefinitionProperties properties = DefinitionProperties.getInstance();
    
    /** delimitador de bot�es no formato de retorno ao AdvPL */
    private static final String STRING_DELIMITER = ";";
    
    /** define se o eixo possui DrillDown */
    private boolean blnActiveDrillDown;
    
    /** bot�o em que o drill down est� ativo */
    private DwButton dwDrillButton;
    
    /** Eixo do tipo X */
    public static final int AXIS_X              = 1;
    
    /** Eixo do tipo X espec�fico para defini��es de gr�ficos */
    public static final int AXIS_X_GRAPH        = 2;
    
    /** Eixo do tipo Y */
    public static final int AXIS_Y              = 3;
    
    /** Eixo do tipo indicadores */
    public static final int AXIS_MEASURE        = 4;
    
    /** Eixo do tipo indicadores para gr�fico */
    public static final int AXIS_MEASURE_GRAPH  = 5;
    
    /** Eixo do tipo de informa��es espec�fico para defini��es de gr�ficos */
    public static final int AXIS_INFO           = 6;
    
    /** layout para os diversos tipos de eixos */
    protected LayoutManager axisLayout;
    
    /** t�tulo do eixo */
    private String strAxisName;
    
    /** tipo de eixo */
    private int intAxisType;
    
    /** lista com os bot�es adicionados a este eixo espec�fico */
    private List<DwButton> lstAxisButtons = new ArrayList<DwButton>();
    
    /** cont�m o listener para o evento de a��o de boto�es */
    private DwActionListener<DwButton> dwButtonListener;
    
    /** listener de eventos de drill down */
    private DwDrillDownListener<DwAxis> drillDownListener;
    
    /** listener de eventos adicionar e excluir boto�es */
    private DwCRUDListener<DwAxis, DwButton> crudListener;
    
    private int intDDOrientation = LEFT_RIGTH;
    
    public static final int LEFT_RIGTH          = 1;
    public static final int RIGTH_LEFT          = 2;
    
    /**
     * Constr�i um novo objeto DwAxis
     */
    public DwAxis() {
        this("", AXIS_INFO);
    }
    
    /**
     * Cronstr�i um novo DwAxis passando como par�metro o t�tulo do eixo e o tipo de eixo.
     * @param strAxisName Objeto do tipo <code>String</code> contendo o t�tulo do eixo
     * @param iAxisType Primitiva do tipo <code>int</code> contendo o tipo de eixo
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_X
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_X_GRAPH
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_Y
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_MEASURE
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_INFO
     */
    public DwAxis(String strAxisName, int iAxisType) {
        this.setAxisName(strAxisName);
        this.setAxisType(iAxisType);
        
        initComponents();
        defineAxisLayout();
        
        DwDropTargetListener<DwObject> dwDDListener = new DwDropTargetListener<DwObject>(this, defineDataFlavor());
        new DropTarget(this, DnDConstants.ACTION_COPY, dwDDListener, true);
    }
    
    /**
     * M�todo respons�vel por definir o tipo de DataFlavor que este eixo suportar�, baseado
     *  no tipo de eixo
     * @return objeto do tipo <code>DataFlavor</code> contendo o tipo suportado
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_X
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_X_GRAPH
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_Y
     * @see br.com.microsiga.sigadw.definition.DwAxis#AXIS_MEASURE
     * @see br.com.microsiga.sigadw.event.dragndrop.DwTransferable#sigaDwObject
     * @see br.com.microsiga.sigadw.event.dragndrop.DwTransferable#sigaDwDimField
     * @see br.com.microsiga.sigadw.event.dragndrop.DwTransferable#sigaDwMeasure
     * @see br.com.microsiga.sigadw.event.dragndrop.DwTransferable#sigaDwQuery
     */
    protected DataFlavor defineDataFlavor() {
        DataFlavor dataFlavor = null;
        
        if (intAxisType == AXIS_X || intAxisType == AXIS_X_GRAPH || intAxisType == AXIS_Y) {
            dataFlavor = DwTransferable.sigaDwDimField;
        } else if (intAxisType == AXIS_MEASURE || intAxisType == AXIS_MEASURE_GRAPH) {
            dataFlavor = DwTransferable.sigaDwMeasure;
        }
        
        return dataFlavor;
    }
    
    /**
     * M�todo respons�vel por definir o tipo de layout manager para cada tipo de eixo
     */
    protected void defineAxisLayout() {
        if (intAxisType == AXIS_X || intAxisType == AXIS_X_GRAPH) {
            axisLayout = new FlowLayout(FlowLayout.LEADING);
        } else if (intAxisType == AXIS_Y) {
            axisLayout = new BoxLayout(this.getRootDragPanel(), BoxLayout.PAGE_AXIS);
        } else if (intAxisType == AXIS_MEASURE || intAxisType == AXIS_MEASURE_GRAPH) {
            axisLayout = new FlowLayout(FlowLayout.LEADING);
        } else if (intAxisType == AXIS_INFO) {
            axisLayout = new BorderLayout();
        }
        this.getRootDragPanel().setLayout(axisLayout);
    }
    
    /**
     * M�todo respons�vel por construir os bot�es associados com este objeto
     * <code>DwAxis</code>, a partir das propriedades previamente fornecidas atrav�s do
     * singleton <code>DefinitionProperties</code>
     * @see br.com.microsiga.sigadw.definition.to.DefinitionProperties#getAxis
     */
    protected void buildAxisButtons() {
        DwButton dwButton = null;
        String strButton;
        if (getAxisType() != AXIS_INFO) {
            this.getAxisButtons().clear();
            this.getRootDragPanel().removeAll();
            String strDwButtons = DefinitionProperties.getInstance().getAxis(getAxisType());
            if (strDwButtons != null) {
                StringTokenizer stDwButtons = new StringTokenizer(strDwButtons, STRING_DELIMITER);
                while (stDwButtons.hasMoreTokens()) {
                    strButton = stDwButtons.nextToken();
                    dwButton = this.buildDwButton(strButton);
                    if (strButton.indexOf('*') > 0) {
                        this.setActiveDrillDown(true);
                        this.dwDrillButton = dwButton;
                    }
                }
                this.repaintAxis();
            }
        }
    }
    
    /**
     * M�todo respons�vel por construir um bot�o, a partir de um objeto contendo
     * informa��es relacionadas com bot�o a ser criado, e exibi-lo visualmente
     * @param dwObject cont�m informa��es relacionadas com bot�o a ser criado
     * @return o novo bot�o
     */
    protected DwButton buildDwButton(DwObject dwObject) {
        return this.buildDwButton(dwObject, true);
    }
    
        /**
     * M�todo respons�vel por construir um bot�o, a partir de um objeto contendo
     * informa��es relacionadas com bot�o a ser criado, e exibi-lo visualmente
     * @param dwObject cont�m informa��es relacionadas com bot�o a ser criado
     * @param blnCheckCanAdd sinaliza se deve ser verificado se pode adicionar o
     *      bot�o neste eixo
     * @return o novo bot�o
     */
    protected DwButton buildDwButton(DwObject dwObject, boolean blnCheckCanAdd) {
        return this.buildDwButton(dwObject, blnCheckCanAdd, -1);
    }
    
    /**
     * M�todo respons�vel por construir um bot�o, a partir de um objeto contendo
     * informa��es relacionadas com bot�o a ser criado, e exibi-lo visualmente
     * @param dwObject cont�m informa��es relacionadas com bot�o a ser criado
     * @param blnCheckCanAdd sinaliza se deve ser verificado se pode adicionar o
     *      bot�o neste eixo
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     * @return o novo bot�o
     */
    protected DwButton buildDwButton(DwObject dwObject, boolean blnCheckCanAdd, int intIndex) {
        DwButton dwButton = null;
        
        if (this.getAxisType() == DwAxis.AXIS_X && dwObject.getObjectType() == DwObject.OBJECT_DIMFIELD) {
            dwButton = new DwButtonAxisX((DwField) dwObject);
        } else if (this.getAxisType() == DwAxis.AXIS_X_GRAPH && dwObject.getObjectType() == DwObject.OBJECT_DIMFIELD) {
            dwButton = new DwButtonAxisXGraph((DwField) dwObject);
        } else if (this.getAxisType() == DwAxis.AXIS_Y && dwObject.getObjectType() == DwObject.OBJECT_DIMFIELD) {
            dwButton = new DwButtonAxisY((DwField) dwObject);
        } else if (this.getAxisType() == DwAxis.AXIS_MEASURE && dwObject.getObjectType() == DwObject.OBJECT_MEASURE) {
            dwButton = new DwButtonMeasure(dwObject);
        } else if (this.getAxisType() == DwAxis.AXIS_MEASURE_GRAPH && dwObject.getObjectType() == DwObject.OBJECT_MEASURE) {
            dwButton = new DwButtonMeasureGraph(dwObject);
        }
        
        dwButton.setContentAreaFilled(false);
        dwButton.setOpaque(true);
        
        if (!blnCheckCanAdd || (blnCheckCanAdd && this.canAddDwButton(dwButton))) {
            this.getRootDragPanel().add(dwButton, intIndex);
            this.notifyAddDwButton(dwButton, intIndex);
        }
        
        DefinitionProperties.getInstance().setAxis(this.getAxisType(), getAxisProprieties());
        
        return dwButton;
    }
    
    /**
     * M�todo respons�vel por construir um bot�o, a partir das propriedades
     * relacionadas com bot�o a ser criado, e exibi-lo visualmente
     * @param strButton cont�m as propriedades relacionadas com bot�o a ser criado
     * @return o novo bot�o
     */
    public DwButton buildDwButton(String strButton) {
        return this.buildDwButton(strButton, true, -1);
    }
    
    /**
     * M�todo respons�vel por construir um bot�o, a partir das propriedades
     * relacionadas com bot�o a ser criado, e exibi-lo visualmente
     * @param strButton cont�m as propriedades relacionadas com bot�o a ser criado
     * @param blnCheckCanAdd sinaliza se deve ser verificado se pode adicionar o
     *      bot�o neste eixo
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     * @return o novo bot�o
     */
    public DwButton buildDwButton(String strButton, boolean blnCheckCanAdd, int intIndex) {
        DwButton dwButton = null;
        
        if (this.getAxisType() == DwAxis.AXIS_X) {
            dwButton = new DwButtonAxisX(strButton);
        } else if (this.getAxisType() == DwAxis.AXIS_X_GRAPH) {
            dwButton = new DwButtonAxisXGraph(strButton);
        } else if (this.getAxisType() == DwAxis.AXIS_Y) {
            dwButton = new DwButtonAxisY(strButton);
        } else if (this.getAxisType() == DwAxis.AXIS_MEASURE) {
            dwButton = new DwButtonMeasure(strButton);
        } else if (this.getAxisType() == DwAxis.AXIS_MEASURE_GRAPH) {
            dwButton = new DwButtonMeasureGraph(strButton);
        }
        
        dwButton.setContentAreaFilled(false);
        dwButton.setOpaque(true);
        
        if (!blnCheckCanAdd || (blnCheckCanAdd && this.canAddDwButton(dwButton))) {
            this.getRootDragPanel().add(dwButton, intIndex);
            this.notifyAddDwButton(dwButton, false, intIndex);
        }
        
        dwButton.updateUI();
        
        return dwButton;
    }
    
    /**
     * M�todo respons�vel por exibir um bot�o visualmente
     * @param dwButton bot�o que dever� ser exibido
     */
    public void buildDwButton(DwButton dwButton) {
        this.buildDwButton(dwButton, -1);
    }
    
    /**
     * M�todo respons�vel por exibir um bot�o visualmente
     * @param dwButton bot�o que dever� ser exibido
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     */
    public void buildDwButton(DwButton dwButton, int intIndex) {
        if (dwButton != null) {
            this.getRootDragPanel().add(dwButton, intIndex);
            this.getRootDragPanel().updateUI();
        }
    }
    
    /**
     * M�todo respons�vel por adicionar um listener de eventos de opera��es CRUD (cria��o,
     * recupera��o, atualiza��o e exclus�o) ocorridas em boto�es em um eixo
     * @param listener cont�m um listener de eventos de opera��es CRUD
     * @see br.com.microsiga.sigadw.definition.DwAxis#notifyAddDwButton
     * @see br.com.microsiga.sigadw.definition.DwAxis#notifyDelDwButton
     */
    public void addCRUDListener(DwCRUDListener<DwAxis, DwButton> listener) {
        this.crudListener = listener;
    }
    
    /**
     * M�todo respons�vel por adicionar um listener de eventos de drill down ocorridos
     * em boto�es neste eixo
     * @param listener cont�m um listener de eventos de drill down
     * @see br.com.microsiga.sigadw.definition.DwAxis#notifyDrillDown
     */
    public void addDrillDownListener(DwDrillDownListener<DwAxis> listener) {
        this.drillDownListener = listener;
    }
    
    /**
     * M�todo respons�vel por verificar se � permitido adicionar o bot�o passado como
     * par�metro neste eixo. Nesta verifica��o ser� propagado o evento de verifica��o
     * de permiss�o ao listener de opera��es CRUD (inclus�o e exclus�o) de bot�es SigaDW
     * @param dwButton cont�m o bot�o cuja verifica��o de permiss�o est� sendo cheacada
     * @return <tt>true</tt> caso seja permitido adicionar o bot�o, <tt>false</tt> caso contr�rio
     * @see br.com.microsiga.sigadw.definition.DwAxis#addCRUDListener
     */
    public boolean canAddDwButton(DwButton dwButton) {
        boolean blnAutorized = true;
        
        // verifica no listener se � permitido adicionar o bot�o ao eixo
        if (this.crudListener != null) {
            blnAutorized = this.crudListener.canAdd(this, dwButton);
        }
        
        return blnAutorized;
    }
    
    /**
     * M�todo que ser� chamado pelo objeto de respons�vel pelo evento de drag and drop
     * de bot�es, quando ocorrer a adi��o de um novo bot�o. Este m�todo ser� respons�vel
     * por criar eventos espec�ficos no bot�o, atualizar as propriedades para o eixo,
     * reconstruir o eixo e uma chamada ao listener de opera��es CRUD (caso o eixo possua
     * um listener de CRUD)
     * @param dwButton bot�o que foi adicionado ao eixo
     */
    public void notifyAddDwButton(DwButton dwButton) {
        this.notifyAddDwButton(dwButton, -1);
    }
    
    /**
     * M�todo que ser� chamado pelo objeto de respons�vel pelo evento de drag and drop
     * de bot�es, quando ocorrer a adi��o de um novo bot�o. Este m�todo ser� respons�vel
     * por criar eventos espec�ficos no bot�o, atualizar as propriedades para o eixo,
     * reconstruir o eixo e uma chamada ao listener de opera��es CRUD (caso o eixo possua
     * um listener de CRUD)
     * @param dwButton bot�o que foi adicionado ao eixo
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     */
    public void notifyAddDwButton(DwButton dwButton, int intIndex) {
        this.notifyAddDwButton(dwButton, true, intIndex);
    }
    
    /**
     * M�todo sobrecarregado que ser� chamado pelo objeto de respons�vel pelo evento
     * de drag and drop de bot�es, quando ocorrer a adi��o de um novo bot�o.
     * Este m�todo ser� respons�vel por criar eventos espec�ficos no bot�o, atualizar
     * as propriedades para o eixo, reconstruir o eixo e uma chamada ao listener de
     * opera��es CRUD (caso o eixo possua um listener de CRUD)
     * @param dwButton bot�o que foi adicionado ao eixo
     * @param blnRepaintAxis <tt>true</tt> caso seja necess�rio reconstruir o eixo e <tt>false</tt> caso contr�rio
     */
    public void notifyAddDwButton(DwButton dwButton, boolean blnRepaintAxis) {
        this.notifyAddDwButton(dwButton, blnRepaintAxis, -1);
    }
    
    /**
     * M�todo sobrecarregado que ser� chamado pelo objeto de respons�vel pelo evento
     * de drag and drop de bot�es, quando ocorrer a adi��o de um novo bot�o.
     * Este m�todo ser� respons�vel por criar eventos espec�ficos no bot�o, atualizar
     * as propriedades para o eixo, reconstruir o eixo e uma chamada ao listener de
     * opera��es CRUD (caso o eixo possua um listener de CRUD)
     * @param dwButton bot�o que foi adicionado ao eixo
     * @param blnRepaintAxis <tt>true</tt> caso seja necess�rio reconstruir o eixo e <tt>false</tt> caso contr�rio
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     */
    public void notifyAddDwButton(DwButton dwButton, boolean blnRepaintAxis, int intIndex) {
        this.addAxisButton(dwButton, intIndex);
        dwButton.addPropertyChangeListener(this);
        dwButton.addDragEventListener(this);
        dwButton.addActionListener(this);
        
        if (blnRepaintAxis) {
            this.repaintAxis();
        }
        
        // notifica a adi��o de um bot�o ao listener
        if (this.crudListener != null) {
            this.crudListener.notifyCreate(this, dwButton);
        }
        
        DefinitionProperties.getInstance().setAxis(this.getAxisType(), getAxisProprieties());
    }
    
    /**
     * M�todo que ser� chamado quando ocorrer a exclus�o de um bot�o do eixo.
     * Este m�todo ser� respons�vel atualizar as propriedades para o eixo,
     * reconstruir o eixo e uma chamada ao listener de opera��es CRUD
     * (caso o eixo possua um listener de CRUD)
     * @param dwButton bot�o que foi exclu�do do eixo
     */
    public void notifyDelDwButton(DwButton dwButton) {
        if (this.intDDOrientation == LEFT_RIGTH) {
            getAxisButtons().remove(dwButton);
        } else {
            getAxisButtons().remove(getAxisButtons().lastIndexOf(dwButton));
        }
        
        // verifica se o eixo apresenta drill down e 
        // se n�o existe mais do que 2 bot�es OU verifica se o bot�o removido � o mesmo "pai" do drill down
        // desabilitar� o drill down
        if (this.isActiveDrillDown() && (getAxisButtons().size() < 2 || dwButton.equals(this.dwDrillButton))) {
            this.setActiveDrillDown(false);
            this.dwDrillButton = null;
            this.repaintAxis();
            
            // notifica os listeners do evento de drill down
            if (this.drillDownListener != null) {
                this.drillDownListener.notifyDrillDown(false);
            }
        }
        
        // notifica a exclus�o de um bot�o ao listener inclus�o e exclus�o de bot�es
        if (this.crudListener != null) {
            this.crudListener.notifyDelete(this, dwButton);
        }
        
        dwButton.setVisible(false);
        dwButton = null;
        DefinitionProperties.getInstance().setAxis(this.getAxisType(), getAxisProprieties());
        this.rebuildAxis();
    }
    
    /**
     * M�todo que ser� chamado quando ocorrer o evento de movimento de um bot�o no seu
     * pr�prio eixo. Este m�todo ser� respons�vel atualizar as propriedades para o eixo
     * e reconstruir o eixo
     * @param dwButton bot�o que foi movimentado no eixo
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     */
    public void notifyMovedDwButton(DwButton dwButton, int intIndex) {
        getAxisButtons().remove(dwButton);
        this.buildDwButton(dwButton, intIndex);
        this.notifyAddDwButton(dwButton, intIndex);
        DefinitionProperties.getInstance().setAxis(this.getAxisType(), getAxisProprieties());
        
        if (this.isActiveDrillDown()) {
            this.repaintAxis();
        }
    }
    
    /**
     * M�todo respons�vel pela tratar um evento de drill down (ativa��o ou desativa��o).
     * Reconstr�i o eixo e notifica outros bot�es no eixo e o listener de eventos de
     * drill down sobre a ocorr�ncia do drill down
     */
    protected void notifyDrillDown() {
        int intButtonSrcIndex = getAxisButtons().lastIndexOf(this.dwDrillButton);
        
        int cont = 1;
        for (DwButton dwButtonAux : getAxisButtons()) {
            if (this.dwDrillButton != dwButtonAux) {
                if (intButtonSrcIndex > getAxisButtons().indexOf(dwButtonAux)) {
                    dwButtonAux.notifyDrillDown(false, true);
                } else {
                    dwButtonAux.notifyDrillDown(this.isActiveDrillDown(), (cont < getAxisButtons().size()));
                }
            } else if (intButtonSrcIndex == (getAxisButtons().size()-1)) {
                dwButtonAux.notifyDrillDown(false, false);
                this.dwDrillButton = null;
                this.setActiveDrillDown(false);
            }
            cont++;
        }
        
        // notifica o listener de drill down
        if (drillDownListener != null) {
            drillDownListener.notifyDrillDown(this.isActiveDrillDown(), this);
        }
    }
    
    /**
     * <pre>M�todo que ser� notificado caso algum evento de drag and drop
     *  ocorra no componente do siga dw
     *
     * @param dwObject
     *      Objeto gen�rico do tipo <code>DwObject</code> relacionado com o componente
     *  aonde ocorreu o evento de drag and drop
     * </pre>
     */
    public void notifyDragDrop(DwObject dwObject) {
        int intIndex = -1;
        int intBtnIndex = -1;
        boolean blnCheckCanAdd = true;
        double dblPosMouse;
        
        if (this.intAxisType == DwAxis.AXIS_Y) {
            dblPosMouse = this.jspMain.getVerticalScrollBar().getValue() + dwObject.getDDOrigin().getY();
            intIndex = (int) Math.round(dblPosMouse / DwButton.DIMENSION.getHeight());
        } else {
            dblPosMouse = this.jspMain.getHorizontalScrollBar().getValue() + dwObject.getDDOrigin().getX();
            intIndex = (int)  Math.round(dblPosMouse / DwButton.DIMENSION.getWidth());
        }
        
        if (dwObject.getDwObjectOrigin() == DwObject.ORIGIN_DWAXIS) {
            DwButton dwButtonSearch = new DwButtonAxisX();
            dwButtonSearch.setDwObject(dwObject);
            blnCheckCanAdd = false;
            intBtnIndex = this.getAxisButtons().indexOf(dwButtonSearch);
        }
        
        if (intBtnIndex < intIndex) {
            this.intDDOrientation = LEFT_RIGTH;
        } else {
            this.intDDOrientation = RIGTH_LEFT;
        }

        if (intIndex > this.getAxisButtons().size()) {
            intIndex = -1;
        }
        
        if (dwObject.getProperties() != null) {
            this.buildDwButton(dwObject.getProperties(), blnCheckCanAdd, intIndex);
        } else {
            this.buildDwButton(dwObject, blnCheckCanAdd, intIndex);
        }
    }
    
    public void notifyDropEnd() {
        
    }
    
    public void notifyDropEnd(DwObject dwObject) {
        if (dwObject.getComponentOrigin() == null) {
            int intIndex = this.getAxisButtons().indexOf(this.buildDwButton(dwObject));
            if (intIndex != -1) {
                this.notifyDelDwButton(this.getAxisButtons().get(intIndex));
            }
        } else {
            DwButton dwButton = (DwButton) dwObject.getComponentOrigin();
            dwButton.setDwObject(dwObject);
            this.notifyDelDwButton(dwButton);
        }
        this.repaintAxis();
    }

    public void handleDropAccept(Point point) {
        JScrollBar scrollBar;
        double dblMouseValue;
        double dblCmpSize;
        
        this.setBackground(properties.getAcceptDropColor());
        
        if (this.intAxisType == DwAxis.AXIS_Y) {
            scrollBar = this.jspMain.getVerticalScrollBar();
            dblMouseValue = point.getY();
            dblCmpSize = this.getHeight();
        } else {
            scrollBar = this.jspMain.getHorizontalScrollBar();
            dblMouseValue = point.getX();
            dblCmpSize = this.getWidth();
        }
        
        // dividir� a �rea de drag and drop em 3, verificando se o mouse est�:
        // 1) na �ltima �rea ir� fazer o rolamento da scrollbar automaticamente para a direita
        if (dblMouseValue > (dblCmpSize - (dblCmpSize/3))) {
            dblMouseValue = scrollBar.getValue() + (scrollBar.getBlockIncrement()/2);
            scrollBar.setValue((int) dblMouseValue);
        // 2) na primeira �rea ir� fazer o rolamento da scrollbar automaticamente para a esquerda
        } else if (dblMouseValue < (dblCmpSize/3)) {
            dblMouseValue = scrollBar.getValue() - (scrollBar.getBlockIncrement()/2);
            scrollBar.setValue((int) dblMouseValue);
        }
    }

    public void handleDropReject(Point point) {
        this.setBackground(properties.getRejectDragColor());
    }

    public void handleDropExit() {
        this.setBackground(properties.getAppletsWorkspaceBgcolor());
    }

    /**
     * M�todo respons�vel por tratar um evento de altera��o de propriedades em algum bot�o
     * contido neste eixo. Ser� respons�vel por atualizar informa��es relativas � este
     * eixo e eventos espec�ficos como drill down
     * @param dwComponentSrc componente de bot�o do sigadw aonde ocorreu a altera��o de propriedades
     * @param strProperty c�digo da propriedade que foi alterada
     * @param strPropertyValue valor da propriedade ap�s a altera��o
     * @see br.com.microsiga.sigadw.definition.DwButton#PROPERTY_GENERIC
     * @see br.com.microsiga.sigadw.definition.DwButton#PROPERTY_DRILL
     * @see br.com.microsiga.sigadw.definition.DwButton#PROPERTY_TOTALIZE
     */
    public void notifyPropertyChange(DwButton dwComponentSrc, String strProperty, String strPropertyValue) {
        if (DwButton.PROPERTY_DRILL.equals(strProperty)) {
            this.setActiveDrillDown(Boolean.valueOf(strPropertyValue));
            this.dwDrillButton = dwComponentSrc;
            
            this.notifyDrillDown();
        }
        
        this.updateAxisProperties();
    }

    /**
     * M�todo respons�vel por atulizar os dados deste eixo no singleton de defini��es de
     * propriedades
     * @see br.com.microsiga.sigadw.definition.to.DefinitionProperties#setAxis
     */
    public void updateAxisProperties() {
        DefinitionProperties.getInstance().setAxis(this.getAxisType(), getAxisProprieties());
    }
    
    /**
     * M�todo respons�vel por reconstruir visualmente este eixo
     */
    public void repaintAxis() {
        this.notifyDrillDown();
        this.updateDDragNDropTarget();
    }
    
    /**
     * M�todo respons�vel por reconstruir visualmente os componentes deste eixo
     */
    public void rebuildAxis() {
        this.getRootDragPanel().removeAll();
        this.getRootDragPanel().updateUI();
        
        for (DwButton dwButton : this.getAxisButtons()) {
            this.getRootDragPanel().add(dwButton);
        }
        
        this.repaintAxis();
    }
    
    public void updateDDragNDropTarget() {
        this.jspMain.updateUI();
        this.jspMain.validate();
    }
    
    /**
     * M�todo respons�vel por receber o evento de drag de bot�es do SigaDW
     * @param comp cont�m o objeto que do qual est� sendo feito o drag
     */
    public void setDrag(Object comp) {
        String strJTreeSelected = comp.toString();
        this.buildDwButton(properties.getUserDragDwObject());
    }
    
    /**
     * M�todo respons�vel pelo evento de drag de bot�es do SigaDW. Necess�rio para
     * receber o evento de drag and drop, por�m n�o realiza nenhuma a��o
     * @return recupera o objeto que do qual est� sendo feito o drop
     */
    public Object getDrag() {
        // necess�rio para o drag and drop (setDrag e getDrag)
        return "";
    }
    
    /**
     * M�todo respons�vel por adicionar um listener de eventos de a��o sobre bot�es
     * @param dwButtonListener cont�m o listener de eventos
     * @see br.com.microsiga.sigadw.definition.DwAxis#actionPerformed
     */
    public void addActionListener(DwActionListener<DwButton> dwButtonListener) {
        this.dwButtonListener = dwButtonListener;
    }
    
    /**
     * M�todo repons�vel por receber a notifica��o a��o/clique em bot�es do sigadw
     * @param ae objeto de evento de a��o
     */
    public void actionPerformed(ActionEvent ae) {
        dwButtonListener.notifyActionListener((DwButton) ae.getSource());
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de bot�es que est�o contidos neste eixo
     * @return lista de bot�es
     */
    public List<DwButton> getAxisButtons() {
        return lstAxisButtons;
    }
    
    /**
     * M�todo respons�vel por adicionar um bot�a � lista de bot�es contidos neste eixo
     * @param dwButton bot�o que ser� adicionado
     */
    public void addAxisButton(DwButton dwButton) {
        this.addAxisButton(dwButton, -1);
    }
    
    /**
     * M�todo respons�vel por adicionar um bot�a � lista de bot�es contidos neste eixo
     * @param dwButton bot�o que ser� adicionado
     * @param intIndex cont�m o �ndice no qual dever� ser criado o novo bot�o
     */
    public void addAxisButton(DwButton dwButton, int intIndex) {
        if (intIndex == -1 || intIndex > this.lstAxisButtons.size()) {
            this.lstAxisButtons.add(dwButton);
        } else {
            this.lstAxisButtons.add(intIndex, dwButton);
        }
    }
    
    /**
     * M�todo respons�vel por definir a lista de bot�es que estar�o contidos neste eixo
     * @param lstAxisButtons lista de bot�es a serem mantidos nesse eixo
     */
    public void setAxisButtons(List<DwButton> lstAxisButtons) {
        this.lstAxisButtons = lstAxisButtons;
    }
    
    /**
     * M�todo respons�vel por recuperar o t�tulo para este eixo
     * @return t�tulo do eixo
     */
    public String getAxisName() {
        return strAxisName;
    }
    
    /**
     * M�todo respons�vel por definir o t�tulo para este eixo
     * @param strAxisName t�tulo do eixo
     */
    public void setAxisName(String strAxisName) {
        this.strAxisName = strAxisName;
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo deste eixo
     * @return tipo de eixo
     * @see br.com.microsiga.sigadw.definition.DwAxis (AXIS_XXXX)
     */
    public int getAxisType() {
        return intAxisType;
    }
    
    /**
     * M�todo respons�vel por definir o tipo deste eixo
     * @param iAxisType tipo de eixo
     * @see br.com.microsiga.sigadw.definition.DwAxis (AXIS_XXXX)
     */
    public void setAxisType(int iAxisType) {
        this.intAxisType = iAxisType;
    }
    
    /**
     * M�todo respons�vel por recuperar as propriedades para este eixo e bot�es desse eixo
     * @return propriedades deste eixo
     */
    public String getAxisProprieties() {
        DwButton dwButton;
        String strButtonsProperties = "";
        
        for (Iterator it = getAxisButtons().iterator(); it.hasNext();) {
            dwButton = (DwButton) it.next();
            if (!strButtonsProperties.equals("")) {
                strButtonsProperties += STRING_DELIMITER;
            }
            strButtonsProperties += dwButton.getProperties();
        }
        
        return strButtonsProperties;
    }
    
    /**
     * M�todo respons�vel por atualizar a defini��o de propriedades para este eixo, 
     * construindo o eixo e os seus bot�es
     */
    public void updateDefinition() {
        this.buildAxisButtons();
    }
    
    public void setBackground(Color bg) {
        super.setBackground(bg);
        
        if (this.getRootDragPanel() != null) {
            jspMain.setBackground(bg);
            jpMain.setBackground(bg);
        }
    }
    
    /**
     * M�todo respons�vel por recuperar o painel principal do objeto de drag de bot�es
     * @return painel principal de drag de bot�es
     */
    public JPanel getRootDragPanel() {
        return this.jpMain;
    }

    /**
     * M�todo respons�vel por sinalizar se este eixo est� com drill down ativado
     * @return <tt>true</tt> caso esteja com drill down ativado, <tt>false</tt> caso constr�rio
     */
    public boolean isActiveDrillDown() {
        return blnActiveDrillDown;
    }

    /**
     * M�todo respons�vel por definir se este eixo est� com drill down ativado
     * @param blnActiveDrillDown <tt>true</tt> para drill down ativado, <tt>false</tt> caso contr�rio
     */
    protected void setActiveDrillDown(boolean blnActiveDrillDown) {
        this.blnActiveDrillDown = blnActiveDrillDown;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jspMain = new javax.swing.JScrollPane();
        jspMain.setBackground(properties.getAppletsWorkspaceBgcolor());
        jpMain = new javax.swing.JPanel();
        jpMain.setBackground(properties.getAppletsWorkspaceBgcolor());

        jspMain.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        ((TitledBorder) jspMain.getBorder()).setTitle(strAxisName);
        org.jdesktop.layout.GroupLayout jpMainLayout = new org.jdesktop.layout.GroupLayout(jpMain);
        jpMain.setLayout(jpMainLayout);
        jpMainLayout.setHorizontalGroup(
            jpMainLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 297, Short.MAX_VALUE)
        );
        jpMainLayout.setVerticalGroup(
            jpMainLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 211, Short.MAX_VALUE)
        );
        jspMain.setViewportView(jpMain);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jspMain, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jspMain, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jpMain;
    private javax.swing.JScrollPane jspMain;
    // End of variables declaration//GEN-END:variables
    
}