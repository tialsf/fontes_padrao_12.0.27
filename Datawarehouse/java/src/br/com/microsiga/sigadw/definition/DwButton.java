package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.DwDrillDownListener;
import br.com.microsiga.sigadw.event.DwPropertyChangeListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragGestureListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragSourceListener;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPopupMenu;

/**
 * Abstrai a apresenta��o de um button para que cada bot�o espec�fico para
 *  o SigaDW defina os itens espec�ficos para o popup menu (caso seja necess�rio)
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 23 de Abril de 2006, 17:21
 */
public abstract class DwButton extends JButton implements DwDragDropEventListener<DwObject>,
        DwDrillDownListener<DwButton>, MouseListener {
    
    /** constante de propriedade gen�rica */
    public static final String PROPERTY_GENERIC     = "";
    /** constante de propriedade de drill down */
    public static final String PROPERTY_DRILL       = "DRILL";
    /** constante de propriedade totaliza��o */
    public static final String PROPERTY_TOTALIZE    = "TOTALIZE";
    
    /** tamanho padr�o dos bot�es do SigaDW */
    public static final Dimension DIMENSION = new Dimension(120, 26);
    
    /** recupera o singleton contendo as propriedades do applet */
    protected static DefinitionProperties properties = DefinitionProperties.getInstance();
    
    /** cor default de bot�es do SigaDW */
    protected static final Color DEFAULT_BUTTON_COLOR = properties.getButtonBgcolor(); // #ECEEF2
    
    /** cor ativa da cor dos bot�es do SigaDW quando estiverem em modo drill down */
    protected static final Color DRILL_ACTIVE_COLOR   = properties.getButtonBgcolorDrillDown(); // #8793AF
    
    /** cor da fonte dos bot�es do SigaDW quando estiverem em modo drill down */
    protected static final Color DRILL_FONT_COLOR = properties.getButtonFontColorDrillDown(); // #8793AF
    
    /** propriedade que conter� o objeto do SigaDW relacionado com este bot�o */
    private DwObject dwObject;
    
    /** cont�m o popup menu a ser exibido */
    protected JPopupMenu popupMenu = new JPopupMenu();
    
    /** cont�m o listener para mudan�as em propriedades */
    private DwPropertyChangeListener<DwButton> listener;
    
    /** cont�m o listener para o evento de drag and drop de boto�es */
    private DwDragDropEventListener<DwObject> dragListener;
    
    private DragSource dragSource;
    private DragSourceListener dragSourceListener;
    private DragGestureListener dragGestureListener;
    
    private DwDragDropEventListener dragDropListener;
    
    /**
     * Cronstr�i um novo bot�o SigaDw
     */
    public DwButton() {
        super();
        
        this.updateDefaultUI();
        this.buildMenu();
        
        this.addMouseListener(this);
        this.dragSource = DragSource.getDefaultDragSource();
        this.dragSourceListener = new DwDragSourceListener();
        this.dragGestureListener = new DwDragGestureListener(this.dragSourceListener);
        
        this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this.dragGestureListener);
    }
    
    /**
     * Cronstr�i um novo bot�o SigaDw, passando como par�metro um objeto b�sico contendo
     * informa��es sobre a representa��o de informa��es que o bot�o dever� apresentar
     * @param dwObject objeto contendo informa��es sobre as informa��es que o bot�o dever� apresentar
     */
    public DwButton(DwObject dwObject) {
        super(dwObject.getName());
        
        this.dwObject = dwObject;
        this.updateDefaultUI();
        this.buildMenu();
        
        this.addMouseListener(this);
        this.dragSource = DragSource.getDefaultDragSource();
        this.dragSourceListener = new DwDragSourceListener();
        this.dragGestureListener = new DwDragGestureListener(this.dragSourceListener);
        
        this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this.dragGestureListener);
    }
    
    public DwObject getDwObject() {
        return dwObject;
    }
    
    public void setDwObject(DwObject dwObject) {
        this.dwObject = dwObject;
        
        this.defineToolTip();
    }
    
    protected void defineToolTip() {
        this.setToolTipText(this.getHumanPresentableProperties());
    }
    
    /**
     * M�todo respons�vel por adicionar um listener de eventos de altera��o de
     * propriedades de componentes do SigaDW
     * @param listener cont�m o listener de eventos de drag
     */
    public void addPropertyChangeListener(DwPropertyChangeListener<DwButton> listener) {
        if (listener != null) {
            this.listener = listener;
        }
    }
    
    /**
     * M�todo respons�vel por adicionar um listener de eventos de drag de bot�es em
     * eixos do SigaDW
     * @param dragListener cont�m o listener de eventos de drag
     */
    public void addDragEventListener(DwDragDropEventListener<DwObject> dragListener) {
        this.dragListener = dragListener;
    }
    
    /**
     * M�todo respons�vel por tratar de eventos de altera��o de propriedades em bot�es
     * SigaDw
     * @param dwButtonSrc bot�o aonde ocorreu a modifica��o de propriedade
     * @param strProperty cont�m a propriedade que foi modificada
     * @param strPropertyValue cont�m o valor ap�s a modifica��o da propriedade
     * @see br.com.microsiga.sigadw.definition.DwButton (PROPERTY_XXX)
     */
    protected void notifyAxis(DwButton dwButtonSrc, String strProperty, String strPropertyValue) {
        if (listener != null) {
            listener.notifyPropertyChange(dwButtonSrc, strProperty, strPropertyValue);
        }
    }
    
    /**
     * <pre>M�todo que ser� notificado caso algum evento de drag and drop
     *  ocorra no componente do siga dw
     *
     * @param dwObject
     *      Objeto gen�rico do tipo <code>DwObject</code> relacionado com o componente
     *  aonde ocorreu o evento de drag and drop
     * </pre>
     */
    public void notifyDragDrop(DwObject dwObject) {
        if (dragListener != null) {
            dragListener.notifyDragDrop(this.getDwObject());
        }
    }
    
    public void notifyDropEnd() {
        if (dragListener != null) {
            DwObject dwObject = this.getDwObject();
            dwObject.setComponentOrigin(this);
            dragListener.notifyDropEnd(dwObject);
        }
    }
    
    public void notifyDropEnd(DwObject dwObject) {
        if (dragListener != null) {
            dwObject.setComponentOrigin(this);
            dragListener.notifyDropEnd(dwObject);
        }
    }
    
    public void handleDropAccept(Point point) {
        if (dragListener != null) {
            dragListener.handleDropAccept(point);
        }
    }
    
    public void handleDropReject(Point point) {
        if (dragListener != null) {
            dragListener.handleDropReject(point);
        }
    }
    
    public void handleDropExit() {
        if (dragListener != null) {
            dragListener.handleDropExit();
        }
    }
    
    /**
     * M�todo respons�vel por atualizar a interface do bot�o com os valores defaults
     */
    public void updateDefaultUI() {
        // define o tamanho padr�o dos bot�es
        this.setMinimumSize(DIMENSION);
        this.setPreferredSize(DIMENSION);
        this.setMaximumSize(DIMENSION);
        
        //this.setHorizontalTextPosition(AbstractButton.LEFT);
        this.setHorizontalAlignment(AbstractButton.LEFT);
        
        // define a cor do texto padr�o dos bot�es
        this.setForeground(Color.black);
        
        // define a cor de fundo padr�o dos bot�es
        this.setBackground(DEFAULT_BUTTON_COLOR);
    }
    
    /**
     * Constr�i a �rvore de menus disponibilizado atrav�s do recurso PopupMenu, caso o
     * bot�o tenha sido definido para exibir menu
     * @see br.com.microsiga.sigadw.definition.DwButton#showPopupMenu
     */
    protected void buildMenu() {
        if (showPopupMenu()) {
            // define e construi os itens do menu popup (para cada tipo de bot�o
            definePopupMenuItens(popupMenu);
        }
    }
    
    /**
     * Reconstr�i a �rvore de menus disponibilizado atrav�s do recurso PopupMenu.
     * @see br.com.microsiga.sigadw.definition.DwButton#buildMenu
     */
    public void rebuildMenu() {
        this.popupMenu.removeAll();
        this.buildMenu();
        this.defineToolTip();
    }
    
    
    /**
     * Fecha a �rvore de menus disponibilizado atrav�s do recurso PopupMenu
     */
    protected void closeMenu() {
        popupMenu.setVisible(false);
    }
    
    public void mouseDragged(MouseEvent me) {
        // n�o � necess�rio
    }
    
    public void mouseMoved(MouseEvent e) {
        // n�o � necess�rio
    }
    
    public void mouseReleased(MouseEvent me) {
        if (me.getButton() != me.BUTTON1 && me.isPopupTrigger()) {
            popupMenu.show(me.getComponent(), me.getX(), me.getY());
        }
    }
    
    public void mouseEntered(MouseEvent me) {
        // n�o � necess�rio
    }
    public void mouseExited(MouseEvent me) {
        // n�o � necess�rio
    }
    public void mouseClicked(MouseEvent me) {
        // n�o � necess�rio
    }
    public void mousePressed(MouseEvent me) {
        if (me.getButton() != me.BUTTON1 && me.isPopupTrigger()) {
            popupMenu.show(me.getComponent(), me.getX(), me.getY());
        }
    }
    
    /**
     * <pre>M�todo respons�vel por definir se a classe concreta ir� ou n�o exibir popup menu. Caso seja preciso
     *  exibir popup menu, deve-se ent�o sobrescrever o m�todo definePopupMenuItens() com a implementa��o espec�fica.
     * 
     * @return
     *      Retorna true caso dever� ser exibido popup. false caso contr�rio
     * </pre>
     */
    protected abstract boolean showPopupMenu();

    /**
     * <pre>M�todo respons�vel por definir os elementos do popup menu da classe concreta
     * 
     * @param popupMenu
     *      Objeto do tipo <code>JPopupMenu</code> que cont�m o popup menu aonde dever�o
     *  ser adicionados os elementos filhos do popup menu
     * </pre>
     */
    protected void definePopupMenuItens(JPopupMenu popupMenu) {
        // caso seja necess�rio ser� sobrescrito pela classe filha
    }
    
    /**
     * <pre>M�todo respons�vel por recuperar as propriedades relevantes para cada bot�o espec�fico
     *
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades para o bot�o espec�fico
     * </pre>
     */
    public abstract String getProperties();

    /**
     * <pre>M�todo respons�vel por recuperar as propriedades relevantes para cada bot�o espec�fico
     *  de uma forma apresent�vel ao usu�rio final
     *
     * @return
     *      Objeto do tipo <code>String</code> contendo as propriedades para o bot�o espec�fico
     * </pre>
     */
    protected String getHumanPresentableProperties() {
        StringBuilder sbProperties = new StringBuilder();
        br.com.microsiga.sigadw.definition.to.Dimension dimension = properties.searchDimension(((DwField) getDwObject()).getDimID());
        
        sbProperties.append("<html>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FIELD).getName());  // Campo
        sbProperties.append("</b><i> \"" + this.getText() + "</i>\"");                                      // <<nome do campo>>
        
        // verifica se encontrou a dimens�o espec�fica
        if (dimension != null) {
            sbProperties.append(" <b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DIMENSION).getName() + "</b>"); // da dimens�o
            sbProperties.append("<i> \"" + dimension.getName() + "</i>\"");                                     // <<nome da dimens�o>>
        }
        
        sbProperties.append("<br><b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_PROPERTIES).getName() + ":</b>"); // Propriedades
        
        return sbProperties.toString();
    }
    
    /**
     * <pre>M�todo do listener DwDrillDownListener, respons�vel por responder a
     * notifica��o da ocorr�ncia de drill down em outro bot�o.
     *  Caso seja necess�rio ser� sobrescrito pela classe filha.
     * 
     * @param blnPropertyValue
     *      Primitiva do tipo <code>boolean</code> contendo o valor da altera��o no drill down
     * </pre>
     */
    public void notifyDrillDown(boolean blnPropertyValue) {
        
    }
    
    /**
     * <pre>M�todo do listener DwDrillDownListener, respons�vel por responder a
     * notifica��o da ocorr�ncia de drill down em outro bot�o.
     *  Caso seja necess�rio ser� sobrescrito pela classe filha.
     * 
     * @param blnPropertyValue
     *      Primitiva do tipo <code>boolean</code> contendo o valor da altera��o no drill down
     * </pre>
     */
    public void notifyDrillDown(boolean blnPropertyValue, boolean blnShowDrillOption) {
        
    }
    
    /**
     * <pre>M�todo respons�vel por responder a notifica��o da ocorr�ncia de drill down em outro bot�o.
     *  Caso seja necess�rio ser� sobrescrito pela classe filha.
     * 
     * @param blnPropertyValue
     *      Primitiva do tipo <code>boolean</code> contendo o valor da altera��o no drill down
     * @param componentSrc
     *      Objeto gen�rico do tipo <code>DwButton</code> contendo passando o componente
     *  original do evento
     * </pre>
     */
    public void notifyDrillDown(boolean blnPropertyValue, DwButton componentSrc) {
        
    }
    
    public boolean equals(Object obj) {
        boolean blnIsEqual = false;
        
        if (obj instanceof DwButton) {
            DwButton btnCompare = (DwButton) obj;
            if (btnCompare.getDwObject() != null && this.getDwObject() != null &&
                    btnCompare.getDwObject().equals(this.getDwObject())) {
                blnIsEqual = true;
            } else {
                blnIsEqual = super.equals(obj);
            }
        }
        
        return blnIsEqual;
    }
    
    public int hashCode() {
        int intReturn = super.hashCode();
        
        if (this.getDwObject() != null) {
            intReturn = this.getDwObject().getId();
        }
        
        return intReturn;
    }
    
    public String toString() {
        return this.getText();
    }

    public DwDragDropEventListener getDragDropListener() {
        return dragDropListener;
    }

    public void setDragDropListener(DwDragDropEventListener dragDropListener) {
        this.dragDropListener = dragDropListener;
    }
    
}