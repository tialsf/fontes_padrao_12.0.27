package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.DwField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPopupMenu;

/**
 * Customizar a apresenta��o de um button para o eixo X do SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class DwButtonAxisX extends DwButton {
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x
     */
    public DwButtonAxisX() {
        super();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x, passando como par�metro as
     * propriedades para o bot�o sendo criado
     * @param strButtonContent propriedades para o bot�o sendo criado
     */
    public DwButtonAxisX(String strButtonContent) {
        this();
        
        int intTemporal = Integer.parseInt(strButtonContent.substring(1, strButtonContent.indexOf("|")));
        boolean blnTemporal = (intTemporal > 0) ? true : false;
        int objId = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf(")") + 1));
        
        DwField dwObject = DefinitionProperties.getInstance().searchDimensionField(objId, blnTemporal, intTemporal);
        String strName   = dwObject.getName();
        
        super.setText(strName);
        super.setDwObject(dwObject);
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x, passando como par�metro um objeto
     * contendo informa��es sobre o campo de dimens�o relacionado com este novo bot�o
     * @param dwObject cont�m informa��es sobre o campo de dimens�o relacionado com este bot�o
     */
    public DwButtonAxisX(DwField dwObject) {
        super(dwObject);
    }
    
    protected boolean showPopupMenu() {
        return false;
    }
    
    public String getProperties() {
        this.defineToolTip();
        
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append("(");
        
        // verifica se � o bot�o de um campo temporal
        if (((DwField) getDwObject()).isTemporal()) {
            sbProperties.append( ((DwField) getDwObject()).getTemporalID() );
        } else {
            sbProperties.append("0");
        }
        sbProperties.append("|0)");
        sbProperties.append(getDwObject().getId());
        
        return sbProperties.toString();
    }
    
    protected String getHumanPresentableProperties() {
        return this.getHumanPresentableProperties(true);
    }
    
    protected String getHumanPresentableProperties(boolean blnCloseTags) {
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append(super.getHumanPresentableProperties());
        
        // verifica se � o bot�o de um campo temporal
        sbProperties.append("<ul><li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_TEMPORAL).getName() + ": </b>"); // Temporal:
        sbProperties.append("<i>");
        if (((DwField) getDwObject()).isTemporal()) {
             sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName());   // Sim
             
             // pesquisa pelo campo ao qual este campo � temporal
             DwField dwField = properties.searchDimensionField( ((DwField) getDwObject()).getId() );
             sbProperties.append(" <b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FIELD).getName() + "</b>:");  // Campo:
             sbProperties.append(" <i>\"" + dwField + "\"</i>");                                                                    // <<nome do campo>>
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName());     // N�o
        }
        sbProperties.append("</i></li>");
        
        if (blnCloseTags) {
            sbProperties.append("</ul>");
        }
        
        return sbProperties.toString();
    }
    
}