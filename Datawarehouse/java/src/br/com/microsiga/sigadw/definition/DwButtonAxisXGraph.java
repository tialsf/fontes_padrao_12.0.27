package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.definition.to.GraphType;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Customizar a apresenta��o de um button para o eixo X do SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class DwButtonAxisXGraph extends DwButtonAxisX implements ActionListener {
    
    /** propriedade que define se este bot�o � a origem de um evento de drill down */
    private boolean blnDrillDownSource = false;
    
    /** propriedade que define se este bot�o est� ativo no drill down para o eixo */
    private boolean blnActiveDriilDown = false;
    
    /** propriedade que cont�m o tipo de gr�fico escolhido para este atributo */
    private int intGraphType = -1;
    
    /** propriedade que cont�m o tipo de gr�fico default, ou seja, escolhido por default
     * para todos os atributos que n�o estejam na seq��ncia de drill down */
    private int intDefaultGraphType = 0;
    
    /** propriedade que sinaliza se o menu popup dever� exibir a op��o de drill down */
    private boolean blnShowDrillOption = true;
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x do gr�fico
     */
    public DwButtonAxisXGraph() {
        super();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x do gr�fico, passando como par�metro
     * as propriedades relacionadas com este novo bot�o
     * @param strButtonContent propriedades para este novo bot�o
     */
    public DwButtonAxisXGraph(String strButtonContent) {
        int objId;
        int intTemporal         = Integer.parseInt(strButtonContent.substring(1, strButtonContent.indexOf("|")));
        this.intGraphType       = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf("|") + 1, strButtonContent.indexOf(")")));
        this.blnDrillDownSource = this.blnActiveDriilDown = ((strButtonContent.indexOf("*") > 0) ? true : false);
        
        boolean blnTemporal = (intTemporal > 0) ? true : false;
        
        if (this.isDrillDownSource()) {
            objId = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf("*") + 1));
        } else {
            objId = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf(")") + 1));
        }
        
        DwField dwObject = DefinitionProperties.getInstance().searchDimensionField(objId, blnTemporal, intTemporal);
        String strName   = dwObject.getName();
        
        super.setText(strName);
        super.setDwObject(dwObject);
        super.rebuildMenu();
        
        this.defineIcons();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo x do gr�fico, passando como par�metro
     * um objeto contendo informa��es sobre o campo de indicador relacionado com este
     * novo bot�o
     * @param dwObject cont�m informa��es sobre o campo de dimens�o relacionado com este
     * novo bot�o
     */
    public DwButtonAxisXGraph(DwField dwObject) {
        super(dwObject);
    }
    
    protected boolean showPopupMenu() {
        return true;
    }

    protected void definePopupMenuItens(JPopupMenu popupMenu) {
        // verifica se dever� exibir a op��o de drill down
        if (this.blnShowDrillOption) {
            JCheckBox jckbDrill = new JCheckBox(properties.getDrillDown(), isDrillDownSource());
            jckbDrill.setActionCommand(PROPERTY_DRILL);
            jckbDrill.addActionListener(this);
            popupMenu.add(jckbDrill);
        }
        
        if (this.isActiveDriilDown() && !this.isDrillDownSource()) {
            JMenu jmiGraphType = new JMenu(properties.getGraphTypeLabel());
            JMenuItem jmiType;
            for (GraphType graphType : properties.getGraphTypes()) {
                jmiType = new JMenuItem(graphType.getName(), graphType.getIconElement());
                jmiType.setActionCommand(String.valueOf(graphType.getId()));
                jmiType.addActionListener(this);
                jmiGraphType.add(jmiType);
            }
            popupMenu.add(jmiGraphType);
        }
    }

    public String getProperties() {
        this.defineToolTip();
        
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append("(");
        
        // verifica se � o bot�o de um campo temporal
        if (((DwField) getDwObject()).isTemporal()) {
            sbProperties.append( ((DwField) getDwObject()).getTemporalID() );
        } else {
            sbProperties.append("0");
        }
        sbProperties.append("|");
        
        // verifica se o eixo tem drill down E se para este bot�o existe uma defini��o de gr�fico
        if (this.isActiveDriilDown() && this.intGraphType > -1) {
            // recupera o gr�fico definido pelo usu�rio
            sbProperties.append(this.intGraphType);
        } else {
            // recupera o gr�fico default para a defini��o
            sbProperties.append(this.intDefaultGraphType);
        }
        sbProperties.append(")");
        
        sbProperties.append((isDrillDownSource()) ? "*" : "");
        sbProperties.append(getDwObject().getId());
        
        return sbProperties.toString();
    }
    
    protected String getHumanPresentableProperties() {
        StringBuilder sbProperties = new StringBuilder();
        
        // recupera informa��es b�sicas de todos os bot�es
        sbProperties.append(super.getHumanPresentableProperties(false));
        
        sbProperties.append("<li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_SOURCE).getName() + ": </b>"); // Origem Drilldown
        sbProperties.append("<i>");
        if (this.isDrillDownSource()) {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName()); // Sim
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()); // N�o
        }
        sbProperties.append("</i></li>");
        
        sbProperties.append("<li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_CHILD).getName() + ": </b>"); // Participa do drilldown
        sbProperties.append("<i>");
        if (this.isActiveDriilDown()) {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName()); // Sim
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()); // N�o
        }
        sbProperties.append("</i></li>");
        
        // recupera o tipo de gr�fico
        GraphType graphType = new GraphType();
        // verifica se o eixo tem drill down e se para este bot�o existe uma defini��o de gr�fico
        if (!this.isDrillDownSource() && this.isActiveDriilDown()) {
            graphType.setId(this.intGraphType);
        } else {
            graphType.setId(this.intDefaultGraphType);
        }
        int intGraphType = properties.getGraphTypes().indexOf(graphType);
        if (intGraphType > -1) {
            sbProperties.append("<li>");
            sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_GRAPH_TYPE).getName() + ": </b>"); // Tipo de Gr�fico
            sbProperties.append("<i>"+ properties.getGraphTypes().get(intGraphType).getName() + "</i>");
            sbProperties.append("</li>");
        }
        sbProperties.append("</ul>");
        sbProperties.append("<p><i>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_HOWTOACCESSPROP).getName() + "</i></p>");  // "Para alterar estas propriedades clique com o bot�o direito do mouse"
        sbProperties.append("</html>");
        
        return sbProperties.toString();
    }

    /**
     * M�todo respons�vel por definir os icones que apresentaram as informa��es
     * representadas por este bot�o
     */
    protected void defineIcons() {
        Icon iconLeft, iconRight;
        
        if (this.isDrillDownSource()) {
            iconLeft = properties.getDrillParentImage();
        } else if (this.isActiveDriilDown()) {
            iconLeft = properties.getDrillChildImage();
        } else {
            iconLeft = null;
        }
        
        // cria um objeto de pesquisa para pesquisar o objeto correspondente ao tipo de gr�fico
        GraphType graphType = new GraphType();
        
        // verifica se o bot�o/eixo tiver drill down E este bot�o n�o for a origem do drill down
        if (this.isActiveDriilDown() && !this.isDrillDownSource()) {
            // recupera o objeto de tipo de gr�fito relacionado com o gr�fico escolhido pelo usu�rio
            iconRight = properties.searchGraphType(this.intGraphType).getIconElement();
        } else {
            // recupera o objeto de tipo de gr�fito relacionado com o gr�fico padr�o
            iconRight = properties.searchGraphType(this.intDefaultGraphType).getIconElement();
        }
        
        if (iconLeft != null && iconRight != null) {
            this.setIcon(new DecoratedIcon(iconLeft, iconRight, DecoratedIcon.Location.EXPANDED_WIDTH));
        } else if (iconRight != null) {
            this.setIcon(iconRight);
        } else if (iconLeft != null) {
            this.setIcon(iconLeft);
        } else {
            this.setIcon(null);
        }
    }
    
    /**
     * M�todo respons�vel por definir a cor de fundo e a propriedade relacionada a cor
     * de fundo para este bot�o
     */
    protected void defineBackground() {
        if (this.isActiveDriilDown() && !this.isDrillDownSource()) {
            this.setForeground(DRILL_FONT_COLOR);
            this.setBackground(DRILL_ACTIVE_COLOR);
        } else {
            super.updateDefaultUI();
        }
    }
    
    public void notifyDrillDown(boolean blnPropertyValue, boolean blnShowDrillOption) {
        super.notifyDrillDown(blnPropertyValue, blnShowDrillOption);
        
        this.blnActiveDriilDown = blnPropertyValue;
        this.blnDrillDownSource = false;
        this.blnShowDrillOption = blnShowDrillOption;
        
        // verifica se esta notifica��o � a de ativa��o do drill down
        if (this.isActiveDriilDown()) {
            // verifica se ainda n�o foi definido um gr�fico para este bot�o
            if (this.intGraphType == -1) {
                // e em caso afirmativo define o 1� gr�fico para este bot�o
                this.intGraphType = 0;
            } // em outro caso mant�m o gr�fico pr�-selecionado pelo usu�rio
        }
        
        this.defineIcons();
        this.defineBackground();
        super.rebuildMenu();
    }
    
    public void actionPerformed(ActionEvent e) {
        
        if (e.getActionCommand() == PROPERTY_DRILL) {
            JCheckBox jckb = (JCheckBox) e.getSource();
            this.blnDrillDownSource = this.blnActiveDriilDown = jckb.isSelected();
            if (this.isDrillDownSource()) {
                this.intGraphType = -1;
                this.defineBackground();
                
                super.rebuildMenu();
            }
            super.notifyAxis(this, e.getActionCommand(), String.valueOf(jckb.isSelected()));
        } else {
            this.intGraphType = Integer.parseInt(e.getActionCommand());
            super.notifyAxis(this, PROPERTY_GENERIC, String.valueOf(this.intGraphType));
        }
        
        this.defineIcons();
        super.closeMenu();
        
    }

    public boolean isDrillDownSource() {
        return blnDrillDownSource;
    }

    public boolean isActiveDriilDown() {
        return blnActiveDriilDown;
    }

    public int getDefaultGraphType() {
        return intDefaultGraphType;
    }

    public void setDefaultGraphType(int intAxisDefaultGraphType) {
        this.intDefaultGraphType = intAxisDefaultGraphType;
        this.defineIcons();
    }
    
}