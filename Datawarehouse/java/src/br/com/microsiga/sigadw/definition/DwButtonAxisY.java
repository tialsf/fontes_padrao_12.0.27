package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.DwField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JPopupMenu;

/**
 * Customizar a apresenta��o de um button para o eixo Y do SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class DwButtonAxisY extends DwButton implements ActionListener {
    
    /** propriedade totalizar */
    private boolean blnTotalize = true;
    
    /** propriedade que define se este bot�o � a origem de um evento de drill down */
    private boolean blnDrillDownSource = false;
    
    /** propriedade que define se este bot�o est� ativo no drill down para o eixo */
    private boolean blnActiveDriilDown = false;
    
    /** propriedade que sinaliza se o menu popup dever� exibir a op��o de drill down */
    private boolean blnShowDrillOption = true;
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo Y
     */
    public DwButtonAxisY() {
        super();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo Y, passando como par�metro as
     * propriedades para este novo bot�o
     * @param strButtonContent propriedades desse novo bot�o
     */
    public DwButtonAxisY(String strButtonContent) {
        this();
        
        int objId;
        int intTemporal         = Integer.parseInt(strButtonContent.substring(1, strButtonContent.indexOf("|")));
        String strTotalize      = strButtonContent.substring(strButtonContent.indexOf("|") + 1, strButtonContent.indexOf(")"));
        this.blnDrillDownSource = this.blnActiveDriilDown = (strButtonContent.indexOf("*") > 0) ? true : false;
        
        this.blnTotalize    = (strTotalize.equals("1")) ? true: false;
        boolean blnTemporal = (intTemporal > 0) ? true : false;
        
        if (this.isDrillDownSource()) {
            objId = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf("*") + 1));
        } else {
            objId = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf(")") + 1));
        }
        
        DwField dwObject = DefinitionProperties.getInstance().searchDimensionField(objId, blnTemporal, intTemporal);
        String strName   = dwObject.getName();
        
        super.setText(strName);
        super.setDwObject(dwObject);
        
        this.defineIcons();
        super.rebuildMenu();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo Y, passando como par�metro um objeto
     * contendo informa��es sobre o campo que este bot�o representar� visualmente
     * @param dwObject cont�m informa��es sobre o campo que este bot�o representar� visualmente
     */
    public DwButtonAxisY(DwField dwObject) {
        super(dwObject);
    }
    
    protected boolean showPopupMenu() {
        return true;
    }
    
    protected void definePopupMenuItens(JPopupMenu popupMenu) {
        // verifica se dever� exibir a op��o de drill down
        if (this.blnShowDrillOption) {
            JCheckBox jckbDrill = new JCheckBox(properties.getDrillDown(), blnDrillDownSource);
            jckbDrill.setActionCommand(PROPERTY_DRILL);
            jckbDrill.addActionListener(this);
            popupMenu.add(jckbDrill);
        }
        
        JCheckBox jckbTotal = new JCheckBox(properties.getTotalize(), blnTotalize);
        jckbTotal.setActionCommand(PROPERTY_TOTALIZE);
        jckbTotal.addActionListener(this);
        popupMenu.add(jckbTotal);
    }
    
    public String getProperties() {
        this.defineToolTip();
        
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append("(");
        
        // verifica se � o bot�o de um campo temporal
        if (((DwField) getDwObject()).isTemporal()) {
            sbProperties.append( ((DwField) getDwObject()).getTemporalID() );
        } else {
            sbProperties.append("0");
        }
        sbProperties.append("|");
        sbProperties.append((blnTotalize) ? "1" : "0");
        sbProperties.append(")");
        
        // verifia se est� em modo de drill
        if (blnDrillDownSource) {
            sbProperties.append("*");
        }
        
        sbProperties.append(this.getDwObject().getId());
        
        return sbProperties.toString();
    }
    
    protected String getHumanPresentableProperties() {
        StringBuilder sbProperties = new StringBuilder();
        
        // recupera informa��es b�sicas de todos os bot�es
        sbProperties.append(super.getHumanPresentableProperties());
        
        // verifica se � o bot�o de um campo temporal
        sbProperties.append("<ul><li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_TEMPORAL).getName() + ": </b>"); // Temporal:
        sbProperties.append("<i>");
        if (((DwField) getDwObject()).isTemporal()) {
             sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName());   // Sim
             
             // pesquisa pelo campo ao qual este campo � temporal
             DwField dwField = properties.searchDimensionField( ((DwField) getDwObject()).getId() );
             sbProperties.append(" <b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FIELD).getName() + "</b>:");  // Campo:
             sbProperties.append(" <i>\"" + dwField + "\"</i>");                                                                    // <<nome do campo>>
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName());     // N�o
        }
        sbProperties.append("</i></li>");
        
        // verifia se est� com totaliza��o
        sbProperties.append("<li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_TOTALIZE).getName() + ": </b>"); // Totalizar
        sbProperties.append("<i>");
        if (this.blnTotalize) {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName()); // Sim
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()); // N�o
        }
        sbProperties.append("</i></li>");
        
        // verifia se est� bot�a � a origem do drill down
        sbProperties.append("<li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_SOURCE).getName() + ": </b>"); // Origem DrillDown
        sbProperties.append("<i>");
        if (this.isDrillDownSource()) {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName()); // Sim
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()); // N�o
        }
        sbProperties.append("</i></li>");
        
        // verifia se est� em modo de drill
        sbProperties.append("<li>");
        sbProperties.append("<b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_DRILL_CHILD).getName() + ": </b>"); // Participa do drilldown
        sbProperties.append("<i>");
        if (this.isActiveDriilDown()) {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName()); // Sim
        } else {
            sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()); // N�o
        }
        sbProperties.append("</i></li>");
        
        sbProperties.append("</ul>");
        sbProperties.append("<p><i>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_HOWTOACCESSPROP).getName() + "</i></p>");  // "Para alterar estas propriedades clique com o bot�o direito do mouse"
        sbProperties.append("</html>");
        
        return sbProperties.toString();
    }
    
    /**
     * M�todo respons�vel por definir os icones que apresentaram as informa��es
     * representadas por este bot�o
     */
    protected void defineIcons() {
        Icon iconLeft = null, iconRight = null;
        
        if (blnDrillDownSource && blnTotalize) {
            iconLeft    = properties.getDrillParentImage();
            iconRight   = properties.getTotalizeImage();
        } else if (blnDrillDownSource && !blnTotalize) {
            iconLeft    = properties.getDrillParentImage();
        } else if (!blnDrillDownSource && blnTotalize && isActiveDriilDown()) {
            iconLeft    = properties.getDrillChildImage();
            iconRight   = properties.getTotalizeImage();
        } else if (!blnDrillDownSource && blnTotalize && !isActiveDriilDown()) {
            iconRight   = properties.getTotalizeImage();
        } else if (!blnDrillDownSource && !blnTotalize && isActiveDriilDown()) {
            iconLeft = properties.getDrillChildImage();
        }
        
        // define o icone
        if (iconLeft != null && iconRight != null) {
            this.setIcon(new DecoratedIcon(iconLeft, iconRight, DecoratedIcon.Location.EXPANDED_WIDTH));
        } else if (iconLeft != null) {
            this.setIcon(iconLeft);
        } else if (iconRight != null) {
            this.setIcon(iconRight);
        } else {
            this.setIcon(null);
        }
    }
    
    /**
     * M�todo respons�vel por definir a cor de fundo e a propriedade relacionada a cor
     * de fundo para este bot�o
     */
    protected void defineBackground() {
        if (this.isActiveDriilDown() && !this.isDrillDownSource()) {
            this.setForeground(DRILL_FONT_COLOR);
            this.setBackground(DRILL_ACTIVE_COLOR);
        } else {
            super.updateDefaultUI();
        }
    }
    
    public void notifyDrillDown(boolean blnPropertyValue, boolean blnShowDrillOption) {
        super.notifyDrillDown(blnPropertyValue, blnShowDrillOption);
        
        this.blnActiveDriilDown = blnPropertyValue;
        this.blnDrillDownSource = false;
        this.blnShowDrillOption = blnShowDrillOption;
        
        this.defineIcons();
        this.defineBackground();
        super.rebuildMenu();
    }
    
    public void actionPerformed(ActionEvent e) {
        JCheckBox jckb = (JCheckBox) e.getSource();
        if (e.getActionCommand() == PROPERTY_DRILL) {
            this.blnDrillDownSource = this.blnActiveDriilDown = jckb.isSelected();
            if (this.isDrillDownSource()) {
                this.defineBackground();
                super.rebuildMenu();
            }
        } else if (e.getActionCommand() == PROPERTY_TOTALIZE) {
            this.blnTotalize = jckb.isSelected();
        }
        this.defineIcons();
        super.closeMenu();
        
        super.notifyAxis(this, e.getActionCommand(), String.valueOf(jckb.isSelected()));        
    }

    public boolean isActiveDriilDown() {
        return blnActiveDriilDown;
    }

    public boolean isDrillDownSource() {
        return this.blnDrillDownSource;
    }
    
}