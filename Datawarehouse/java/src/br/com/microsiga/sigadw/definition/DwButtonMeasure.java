package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.AgregationFunction;
import br.com.microsiga.sigadw.definition.to.Cube;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;

/**
 * Customizar a apresenta��o de um button para o eixo dos Indicadores do SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class DwButtonMeasure extends DwButton implements ActionListener {
    
    /** tipo de fun��o agregadora utilizada no indicador */
    private int intAgregFunctID = 0;
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores
     */
    public DwButtonMeasure() {
        super();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores, passando as
     * propriedades para este novo bot�o
     * @param strButtonContent cont�m as propriedades para este novo bot�o
     */
    public DwButtonMeasure(String strButtonContent) {
        this();
        
        this.setAgregFunctID(Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf("(") + 1, strButtonContent.indexOf("|"))));
        
        int intId           = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf(")")+1));
        DwObject dwObject   = DefinitionProperties.getInstance().searchMeasures(intId);
        String strName      = dwObject.getName();
        
        AgregationFunction agregation = DefinitionProperties.getInstance().searchFunctionAgregation(this.getAgregFunctID());
        
        super.setText(strName);
        super.setIcon(agregation.getIconElement());
        super.setDwObject(dwObject);
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores, passando como par�metro
     * um objeto contendo informa��es sobre o campo de indicador relacionado com este
     * novo bot�o
     * @param dwObject dwObject cont�m informa��es sobre o campo de dimens�o relacionado com este
     */
    public DwButtonMeasure(DwObject dwObject) {
        super(dwObject);
        super.setIcon(properties.getAgregations().get(0).getIconElement());
        
        this.setAgregFunctID(properties.getAgregations().get(0).getId());
    }
    
    protected boolean showPopupMenu() {
        return true;
    }
    
    protected void definePopupMenuItens(JPopupMenu popupMenu) {
        JMenu jmiFunctions = new JMenu(properties.getAgregLabel());
        
        AgregationFunction agregation;
        JMenuItem jmiAgregFunc;
        for (Iterator it = properties.getAgregations().iterator(); it.hasNext();) {
            agregation = (AgregationFunction) it.next();
            
            jmiAgregFunc = new JMenuItem(agregation.getName(), agregation.getIconElement());
            jmiAgregFunc.setActionCommand(String.valueOf(agregation.getId()));
            jmiAgregFunc.addActionListener(this);
            jmiFunctions.add(jmiAgregFunc);
            
        }
        popupMenu.add(jmiFunctions);
    }
    
    public String getProperties() {
        this.defineToolTip();
        
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append("(");
        sbProperties.append(String.valueOf(getAgregFunctID()));
        sbProperties.append("||)");
        sbProperties.append(this.getDwObject().getId());
        
        return sbProperties.toString();
    }
    
    protected String getHumanPresentableProperties() {
        return this.getHumanPresentableProperties(true);
    }
    
    protected String getHumanPresentableProperties(boolean blnCloseTag) {
        StringBuilder sbProperties = new StringBuilder();
        AgregationFunction agregFunc = new AgregationFunction();
        agregFunc.setId(this.getAgregFunctID());
        
        sbProperties.append("<html><b>");
        sbProperties.append(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FIELD).getName());                      // Campo
        sbProperties.append("</b><i> \"" + this.getText() + "</i>\"");                                                          // <<nome do campo>>
        sbProperties.append(" <b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FROM_CUBE).getName() + "</b>"); // do Cubo
        
        sbProperties.append("<br><b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_PROPERTIES).getName() + ":</b>"); // Propriedades
        
        // recupera a fun��o agregadora
        int intIdxAgreg = properties.getAgregations().indexOf(agregFunc);
        if (intIdxAgreg != -1) {
            sbProperties.append("<ul>");
            sbProperties.append("<li><b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_FUNC_AGREG).getName() + ": </b>"); // Fun��o Agregadora
            sbProperties.append("<i>" + properties.getAgregations().get(intIdxAgreg).getName() + "</i>");
            sbProperties.append("</li>");
        }
        
        if (blnCloseTag) {
            sbProperties.append("</ul>");
            sbProperties.append("<p><i>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_HOWTOACCESSPROP).getName() + "</i></p>");  // "Para alterar estas propriedades clique com o bot�o direito do mouse"
            sbProperties.append("</html>");
        }
        
        return sbProperties.toString();
    }
    
    public int getAgregFunctID() {
        return intAgregFunctID;
    }
    
    public void setAgregFunctID(int intAgregFunctID) {
        this.intAgregFunctID = intAgregFunctID;
    }
    
    public void actionPerformed(ActionEvent e) {
        boolean blnNotifyChange = true;
        int intActionID = Integer.parseInt(e.getActionCommand());
        
        // verifica se o evento � no componente de "Visualiza��o"
        if (e.getSource() instanceof JRadioButtonMenuItem) {
            JRadioButtonMenuItem jrbmi = (JRadioButtonMenuItem) e.getSource();
            if (this.getIcon() != null) {
                this.setIcon(new DecoratedIcon(this.getIcon(), jrbmi.getIcon(),
                        DecoratedIcon.Location.EXPANDED_WIDTH));
            } else {
                jrbmi.setSelected(false);
                super.rebuildMenu();
                updateUI();
            }
        } else if (e.getSource() instanceof JMenuItem) {
            this.setAgregFunctID(intActionID);
            
            JMenuItem jmi = (JMenuItem) e.getSource();
            if (this.getIcon() instanceof DecoratedIcon) {
                this.setIcon(new DecoratedIcon(jmi.getIcon(),
                        ((DecoratedIcon) this.getIcon()).getDecorationIcon(),
                        DecoratedIcon.Location.EXPANDED_WIDTH));
            } else {
                this.setIcon(jmi.getIcon());
            }
        }
        
        if (blnNotifyChange) {
            super.notifyAxis(this, PROPERTY_GENERIC, null);
        }
        super.closeMenu();
    }
    
    public boolean equals(Object obj) {
        boolean blnEquals = super.equals(obj);
        
        // verifica a valida��o do "equals" do pai for igual, ent�o verifica o id da fun��o agregadora
        if (blnEquals) {
            if ( ((DwButtonMeasure) obj).getAgregFunctID() == this.getAgregFunctID() ) {
                blnEquals = true;
            } else {
                blnEquals = false;
            }
        }
        
        return blnEquals;
    }
    
}