package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.AgregationFunction;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.DrillDownVisibility;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pv.util.PVColorChooser;

/**
 * Customizar a apresenta��o de um button para o eixo dos Indicadores do SigaDW para gr�ficos
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class DwButtonMeasureGraph extends DwButtonMeasure implements ActionListener, ChangeListener {
    
    /** constante de propriedade de cor de fundo */
    public static final String PROPERTY_BGCOLOR = "measureBackgroundColor";
    
    /** define a propriedade de cor de fundo como sendo a mesma cor padr�o da cor de fundo dos bot�es */
    private int intBgColor = (DEFAULT_BUTTON_COLOR != null) ? DEFAULT_BUTTON_COLOR.getRGB() : 0;
    
    /** cont�m a visibilidade do indicador */
    private List<DrillDownVisibility> lstDrillDownVisibility = null;
    
    /** separador de propriedades de visibilidade */
    public static final String SEPARATOR_VISIBILITY = "�";
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores para gr�fico
     */
    public DwButtonMeasureGraph() {
        super();
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores para gr�fico, passando
     * como par�metro as propriedades para este novo bot�o
     * @param strButtonContent cont�m as propriedades para este novo bot�o
     */
    public DwButtonMeasureGraph(String strButtonContent) {
        this();
        
        List<DrillDownVisibility> lstVisibility = null;
        
        int intAgregID = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf("(") + 1, strButtonContent.indexOf("|")));
        super.setAgregFunctID(intAgregID);
        strButtonContent = strButtonContent.substring(strButtonContent.indexOf("|") + 1);
        
        if (strButtonContent.indexOf('|') > -1) {
            this.intBgColor = new Integer(strButtonContent.substring(0, strButtonContent.indexOf("|")));
            
            String[] strVisibility = strButtonContent.substring(strButtonContent.indexOf("|") + 1, strButtonContent.indexOf(")")).split(SEPARATOR_VISIBILITY);
            if (strVisibility.length > 0 && !strVisibility[0].trim().equals("")) {
                lstVisibility = new ArrayList<DrillDownVisibility>();
                for (int i = 0; i < strVisibility.length; i++) {
                    lstVisibility.add(new DrillDownVisibility( (strVisibility[i].equals("0")) ? false : true ));
                }
            }
        } else {
            try {
                this.intBgColor = new Integer(strButtonContent.substring(0, strButtonContent.indexOf(")")));
            } catch (NumberFormatException nfe) {
                // mant�m o valor padr�o para a cor de fundo do bot�o
            }
        }
        
        int intId           = Integer.parseInt(strButtonContent.substring(strButtonContent.indexOf(")")+1));
        DwObject dwObject   = DefinitionProperties.getInstance().searchMeasures(intId);
        String strName      = dwObject.getName();
        
        AgregationFunction agregation = DefinitionProperties.getInstance().searchFunctionAgregation(super.getAgregFunctID());
        
        super.setText(strName);
        super.setIcon(agregation.getIconElement());
        super.setDwObject(dwObject);
        
        // define a lista de visibilidades do drill down
        if (lstVisibility != null) {
            this.lstDrillDownVisibility = lstVisibility;
        }
        
        // define a cor de fundo
        this.defineBackground(new Color(intBgColor));
    }
    
    /**
     * Constr�i um novo bot�o SigaDw para o eixo dos indicadores do gr�fico, passando
     * como par�metro um objeto contendo informa��es sobre o campo de indicador
     * relacionado com este novo bot�o
     * @param dwObject cont�m informa��es sobre o campo de dimens�o relacionado com este
     * novo bot�o
     */
    public DwButtonMeasureGraph(DwObject dwObject) {
        super(dwObject);
    }
    
    protected void definePopupMenuItens(JPopupMenu popupMenu) {
        JMenu jmChooser = new JMenu(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BGCOLOR_IND).getName());
        JColorChooser chooser = new JColorChooser();
        chooser.setPreferredSize(new Dimension((int) chooser.getPreferredSize().getWidth(), (int) chooser.getPreferredSize().getHeight() - 115));
        chooser.setPreviewPanel(new JLabel());
        chooser.setColor(DEFAULT_BUTTON_COLOR);
        chooser.getSelectionModel().addChangeListener(this);
        
        JPanel jpnlChooser = new JPanel();
        jpnlChooser.add(chooser);
        jmChooser.add(jpnlChooser);
        popupMenu.add(jmChooser);
        
        super.definePopupMenuItens(popupMenu);
        
        if (lstDrillDownVisibility != null) {
            JMenu jmVisibility = new JMenu(properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_VISIBLE_DRILL).getName());
            JCheckBoxMenuItem jcmiButton;
            for (DrillDownVisibility drillVisibility : lstDrillDownVisibility) {
                jcmiButton = new JCheckBoxMenuItem(drillVisibility.getDwButton().toString());
                jcmiButton.setSelected(drillVisibility.isVisible());
                jcmiButton.setActionCommand(drillVisibility.getDwButton().getName());
                jcmiButton.addActionListener(this);
                jcmiButton.setEnabled(!drillVisibility.isDisabled());
                jmVisibility.add(jcmiButton);
            }
            popupMenu.add(jmVisibility);
        }
    }
    
    /**
     * M�todo respons�vel por definir a cor de fundo e a propriedade relacionada a cor
     * de fundo para este bot�o
     * @param bkgColor cont�m a cor de fundo a set definida
     */
    protected void defineBackground(Color bkgColor) {
        if (bkgColor != null && bkgColor.getRGB() != -1118482) {
            this.setBgColor(bkgColor.getRGB());
            this.setBackground(bkgColor);
        } else if (bkgColor == null || bkgColor.getRGB() == -1118482) {
            super.updateDefaultUI();
        }
        this.updateUI();
        this.validate();
        
        super.notifyAxis(this, PROPERTY_BGCOLOR, String.valueOf(this.getBgColor()));
    }
    
    public String getProperties() {
        this.defineToolTip();
        
        String strVisible = "";
        StringBuilder sbProperties = new StringBuilder();
        
        sbProperties.append("(");
        sbProperties.append(String.valueOf(getAgregFunctID()));
        sbProperties.append("|");
        sbProperties.append(this.getBgColor());
        
        if (this.lstDrillDownVisibility != null) {
            sbProperties.append("|");
            for (DrillDownVisibility drillVisibility : lstDrillDownVisibility) {
                if (strVisible != "") {
                    strVisible += SEPARATOR_VISIBILITY;
                }
                strVisible += (drillVisibility.isVisible() ? "1": "0");
            }
            sbProperties.append(strVisible);
        }
        
        sbProperties.append(")");
        sbProperties.append(this.getDwObject().getId());
        
        return sbProperties.toString();
    }
    
    protected String getHumanPresentableProperties() {
        StringBuilder sbProperties = new StringBuilder();
        
        // recupera informa��es b�sicas de todos os bot�es
        sbProperties.append(super.getHumanPresentableProperties(false));
        
        // recupera a propriedade de visibilidade
        if (this.lstDrillDownVisibility != null) {
            try {
                sbProperties.append("<li><b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_VISIBLE_DRILL).getName() + ":</b>");
                sbProperties.append("<br><ul>");
                String strDisabled = "";
                for (DrillDownVisibility drillVisibility : lstDrillDownVisibility) {
                    if (drillVisibility.isDisabled()) {
                        strDisabled += drillVisibility.getDwButton().toString() + " / ";
                    } else {
                        sbProperties.append("<li><b>" + strDisabled + drillVisibility.getDwButton().toString() + " -</b><i> " + (drillVisibility.isVisible() ? properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_YES).getName() : properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_NO).getName()) + "</i></li>");
                        strDisabled = "";
                    }
                }
                sbProperties.append("</ul>");
            } catch (Exception e) {
                System.err.println("Drilldown visibility e -> " + e.getMessage());
                e.printStackTrace();
                
                // recupera informa��es b�sicas de todos os bot�es
                sbProperties = new StringBuilder();
                sbProperties.append(super.getHumanPresentableProperties(false));
            }
        }
        sbProperties.append("</li>");
        
        // recupera a propriedade de cor de fundo
        Color bgColor = new Color(this.getBgColor());
        
        StringBuilder srbBgColor = new StringBuilder();
        
        // verifica se t�m 2 digitos
        if (bgColor.getRed() > 9) {
            srbBgColor.append(Integer.toHexString(bgColor.getRed()));
        } else {
            srbBgColor.append("0");
            srbBgColor.append(Integer.toHexString(bgColor.getRed()));
        }
        
        if (bgColor.getGreen() > 9) {
            srbBgColor.append(Integer.toHexString(bgColor.getGreen()));
        } else {
            srbBgColor.append("0");
            srbBgColor.append(Integer.toHexString(bgColor.getGreen()));
        }
        
        if (bgColor.getBlue() > 9) {
            srbBgColor.append(Integer.toHexString(bgColor.getBlue()));
        } else {
            srbBgColor.append("0");
            srbBgColor.append(Integer.toHexString(bgColor.getBlue()));
        }
        
        sbProperties.append("<li><b>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_BGCOLOR_IND).getName() + ": </b><font color='#" + srbBgColor.toString() + "'>" + srbBgColor.toString().toUpperCase() + "</font></li>");
        sbProperties.append("</ul>");
        sbProperties.append("<p><i>" + properties.getLabelFromDialogy(DefinitionProperties.DIALOGY_HOWTOACCESSPROP).getName() + "</i></p>");  // "Para alterar estas propriedades clique com o bot�o direito do mouse"
        sbProperties.append("</html>");
        
        return sbProperties.toString();
    }
    
    public int getBgColor() {
        return intBgColor;
    }

    public void setBgColor(int intColor) {
        this.intBgColor = intColor;
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JCheckBoxMenuItem) {
            JCheckBoxMenuItem jcbmi = (JCheckBoxMenuItem) e.getSource();
            for (DrillDownVisibility drillDownVis : this.lstDrillDownVisibility) {
                if (drillDownVis.getDwButton().getText() == e.getActionCommand()) {
                    drillDownVis.setVisible(jcbmi.isSelected());
                }
            }
        } else if (e.getActionCommand() == PROPERTY_BGCOLOR) {
            PVColorChooser chooser = new PVColorChooser();
            chooser.addActionListener(this);
        } else if (e.getSource() instanceof PVColorChooser) {
            Color bkgColor = ((PVColorChooser) e.getSource()).getColor();
            if (bkgColor != null) {
                this.setBgColor(bkgColor.getRGB());
                this.setBackground(bkgColor);
            }
        } else {
            super.actionPerformed(e);
        }
        super.notifyAxis(this, PROPERTY_GENERIC, null);
        super.closeMenu();
    }
    
    public void stateChanged(ChangeEvent e) {
        defineBackground( ((DefaultColorSelectionModel) e.getSource()).getSelectedColor() );
    }
    
    /**
     * M�todo respons�vel por determinar a visibilidade do indicador, que este bot�o
     * representa, pelos n�veis do drill down. Este m�todo receber� uma cole��o de objetos
     * de visibilidade que conter� os bot�es envolvidos no drill down e a visibilidade
     * deste indicador em cada n�vel
     * @param plstDrillDownVisibility cont�m uma cole��o de objetos de visibilidade para este indicador
     */
    public void injectDrillDownVisibility(List<DrillDownVisibility> plstDrillDownVisibility) {
        if (this.lstDrillDownVisibility != null) {
            int intIndex;
            DrillDownVisibility currentVisibility;
            for (DrillDownVisibility newVisibility : plstDrillDownVisibility) {
                intIndex = this.lstDrillDownVisibility.indexOf(newVisibility);
                if (intIndex > -1) {
                    currentVisibility = this.lstDrillDownVisibility.get(intIndex);
                    
                    if (!newVisibility.isDisabled()) {
                        if (!currentVisibility.isDisabled()) {
                            newVisibility.setVisible(currentVisibility.isVisible());
                        } else {
                            newVisibility.setVisible(true);
                        }
                    } else {
                        newVisibility.setVisible(false);
                    }
                } else if (newVisibility.getDwButton() == null || this.lstDrillDownVisibility.get(0).getDwButton() == null) {
                    currentVisibility = this.lstDrillDownVisibility.get( plstDrillDownVisibility.indexOf(newVisibility) );
                    if (currentVisibility.getDwButton() == null) {
                        newVisibility.setVisible( currentVisibility.isVisible() );
                    }
                }
            }
        }
        this.lstDrillDownVisibility = plstDrillDownVisibility;
        
        super.notifyAxis(this, PROPERTY_GENERIC, "");
        super.rebuildMenu();
    }
    
    public void removeDrillVisibility() {
        this.lstDrillDownVisibility = null;
        super.notifyAxis(this, PROPERTY_GENERIC, "");
        super.rebuildMenu();
    }
    
}