package br.com.microsiga.sigadw.definition;

import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Customizar a apresenta��o dos n�s da �rvore do SigaDw
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 22 de Abril de 2006
 */
public class DwTreeCellRenderer extends DefaultTreeCellRenderer {
    
    /** Creates a new instance of DwTreeCellRender */
    public DwTreeCellRenderer() {
    }
    
    
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        
        DefinitionProperties properies = DefinitionProperties.getInstance();
        
        Object objNode = ((DefaultMutableTreeNode) value).getUserObject();
        if (objNode instanceof DwObject) {
            DwObject cubeElement = (DwObject) objNode;
            if (cubeElement.getIconElement() != null) {
                this.setIcon(cubeElement.getIconElement());
                this.setBackgroundNonSelectionColor(properies.getAppletsWorkspaceBgcolor());
                if (properies.getAppletsSelections() != null) {
                    this.setBackgroundSelectionColor(properies.getAppletsSelections());
                }
            }
        }
        return this;
    }
    
}