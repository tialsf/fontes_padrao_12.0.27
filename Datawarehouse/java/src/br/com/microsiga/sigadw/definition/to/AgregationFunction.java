package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import javax.swing.Icon;

/**
 * Classe que representa uma fun��o agregadora
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class AgregationFunction extends DwObject {
    
    /**
     * Constr�i um JavaBean do tipo AgregationFunction
     */
    public AgregationFunction() {
    }
    
    /**
     * Constr�i um JavaBean do tipo AgregationFunction passando nome da fun��o agregadora
     * @param strAgregName
     *      Objeto do tipo <code>String</code> contendo o nome
     */
    public AgregationFunction(String strAgregName) {
        setName(strAgregName);
    }
    
    /**
     * Constr�i um JavaBean do tipo AgregationFunction passando o seu id, nome e
     *  icone correspondente
     * @param strAgregName
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconAgreg
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public AgregationFunction(String strAgregName, String strIconAgreg) {
        setName(strAgregName);
        setIconPath(strIconAgreg);
    }
    
    /**
     * Constr�i um JavaBean do tipo AgregationFunction passando o seu id, nome e
     *  icone correspondente
     * @param intID
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strAgregName
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconAgreg
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public AgregationFunction(int intID, String strAgregName, String strIconAgreg) {
        super(intID, strAgregName, strIconAgreg);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de cada objeto suportado.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getObjectType() {
        return OBJECT_AGREGATION;
    }
    
}