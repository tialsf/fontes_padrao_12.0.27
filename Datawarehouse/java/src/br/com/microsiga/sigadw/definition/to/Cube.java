package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * Representa um objeto do tipo Cubo
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17 de Abril de 2006
 */
public class Cube extends DwObject {
    
    private List<Dimension> dimensions = new ArrayList<Dimension>();
    private List<Measure> measures = new ArrayList<Measure>();
    
    /**
     * Constr�i um JavaBean do tipo Cube
     */
    public Cube() {
    }
    
    /**
     * Constr�i um JavaBean do tipo Cube passando o seu id, nome e icone correspondente
     * @param intId
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strNome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public Cube(int intId, String strNome, String strIconElement) {
        super(intId, strNome, strIconElement);
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de dimens�es relacionadas com
     *  este objeto cube
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Dimension</code> com as dimens�es
     */
    public List<Dimension> getDimensions() {
        return this.dimensions;
    }
    
    /**
     * M�todo respons�vel por definir a lista de dimens�es relacionadas com
     *  este objeto cube
     * @param dimensions
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Dimension</code> com as dimens�es
     */
    public void setDimensions(List<Dimension> dimensions) {
        this.dimensions = dimensions;
    }
    
    /**
     * M�todo respons�vel por adicionar uma dimens�o � lista de dimens�es
     *  relacionadas com este objeto cube
     * @param dimension
     *      Objeto do tipo <code>Dimension</code> com a dimens�o a ser adicionada
     */
    public void addDimension(Dimension dimension) {
        this.dimensions.add(dimension);
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de indicadores relacionados com
     *  este objeto cube
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Measure</code> com os indicadores
     */
    public List<Measure> getMeasures() {
        return measures;
    }
    
    /**
     * M�todo respons�vel por definir a lista de indicadores relacionados com
     *  este objeto cube
     * @param measures
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Measure</code> com os indicadores
     */
    public void setMeasures(List<Measure> measures) {
        this.measures = measures;
    }
    
    /**
     * M�todo respons�vel por adicionar um indicador � lista de indicadores
     *  relacionados com este objeto cube
     * @param measure
     *      Objeto do tipo <code>Measure</code> com o indicadore
     */
    public void addMeasure(Measure measure) {
        this.measures.add(measure);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de cada objeto suportado.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getObjectType() {
        return OBJECT_CUBE;
    }
    
}