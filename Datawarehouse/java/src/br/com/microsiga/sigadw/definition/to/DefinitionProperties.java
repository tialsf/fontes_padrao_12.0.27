package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.definition.DwAxis;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTree;

/**
 * Objeto respons�vel por encapsular as propriedades referentes
 *  a defini��o de uma tabela ou gr�fico
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17/04/2006
 */
public class DefinitionProperties {
    
    public static final short TABLE = 1;
    public static final short GRAPH = 2;
    
    private static DefinitionProperties singleton = new DefinitionProperties();
    
    // labels passados atrav�s da tag <applet
    private String strImagePath = "";
    private String strPropExtend = "";
    private List<String> lstTemporal = new ArrayList<String>();
    private Icon iconTemporal = null;
    private List<GraphType> lstGraphTypes = new ArrayList<GraphType>();
    private List<Dialogy> lstDialogy = new ArrayList<Dialogy>();
    private String strSumario = "";
    private String strDrillDown = "";
    private Icon iconDrillChildImage = null;
    private Icon iconDrillParentImage = null;
    private String strTotalize = "";
    private Icon iconTotalizeImage = null;
    private String strDrillOn = "";
    private String strDrillOff = "";
    private String strAxisX = "";
    private String strAxisY = "";
    private String strAxisMeasures = "";
    private String strAxisDesc = "";
    private String strGraphTypeLabel = "";
    private String strProps = "";
    private String strDimension = "";
    private String strMeasures = "";
    private String strGraph = "";
    private String strTable = "";
    private String strSession = "";
    
    // label espec�fico para o gr�fico
    private String strGraphOn = "";
    
    // propriedades que conteram os valores passados na inicializa��o - initPivot()
        // propriedades relacionados com os bot�es dos eixos
        private String strAxisXConcat = "";
        private String strAxisYConcat = "";
        private String strAxisMConcat = "";
        
        // valores espec�ficos para o gr�fico
        private int intGraphType = 0;
        private String strGraphProps = "";
        private String strGraphYProps = "";
        private String strGraphY2Props = "";
    // propriedades que conteram os valores passados na inicializa��o - initPivot()
    
    // objetos que cont�m as dimens�es, campos e indicadores
    private Cube objCube;
    
    // propriedade que conter� o objeto selecionado pelo usu�rio na JTree
    private DwField userDragDwObject;
    
    // propriedade que conter� a string com as informa��es preenchidas pelo usu�rio nas propriedades do gr�fico
    private String strGraphProperties = "";
    
    // label para o menu de fun��es agregadoras
    private String strAgregLabel = "";
    
    // propriedade que conter� todas as agrega��es poss�veis para esta defini��o
    private List<AgregationFunction> lstAgregations = new ArrayList<AgregationFunction>();
    
    // icones das folhas da �rvore de objetos do sigadw
    private Icon icTreeLeaf;
    private Icon icTreeOpen;
    private Icon icTreeClose;
    
    // cor de fundo para �reas selecionadas com o mouse
    private Color clrAppletsSelections;
    // cor de fundo da �rea de trabalho
    private Color clrAppletsBgcolor;
    // cor de fundo da �rea do applet
    private Color clrAppletsWorkspaceBgcolor = new Color(212,208,200);
    // cor de fundo padr�o do bot�o
    private Color clrButtonBgcolor;
    // cor de fundo do bot�o caso esteja com drill down
    private Color clrButtonBgcolorDrillDown;
    // cor da fonte do bot�o caso esteja com drill down
    private Color clrButtonFontColorDrillDown;
    // cor de fundo da �rea de drag and drop quando for poss�vel realizar o drop
    private Color clrAcceptDrop = new Color(0xc6fff4);
    // cor de fundo da �rea de drag and drop quando n�o for poss�vel realizar o drop
    private Color clrRejectDrag = Color.red;
    
    // constantes do posicionamento das propriedades dos dialogos
    public static final int DIALOGY_PROPS_Y         =  1;
    public static final int DIALOGY_PROPSY_ADIC     =  2;
    public static final int DIALOGY_TITLE           =  3;
    public static final int DIALOGY_SCALE           =  4;
    public static final int DIALOGY_BASE            =  5;
    public static final int DIALOGY_META            =  6;
    public static final int DIALOGY_SCAL_LOG        =  7;
    public static final int DIALOGY_LOG_BASE        =  8;
    public static final int DIALOGY_SCAL_AUT        =  9;
    public static final int DIALOGY_MIN_VAL         = 10;
    public static final int DIALOGY_MAX_VAL         = 11;
    public static final int DIALOGY_INCR_VAL        = 12;
    public static final int DIALOGY_AXIS_ADIC       = 13;
    public static final int DIALOGY_TITLE_ADIC      = 14;
    public static final int DIALOGY_SCALE_ADIC      = 15;
    public static final int DIALOGY_BASE_META_ADIC  = 16;
    public static final int DIALOGY_SCAL_LOG_ADIC   = 17;
    public static final int DIALOGY_LOG_BASE_ADIC   = 18;
    public static final int DIALOGY_SCAL_AUTO_ADIC  = 19;
    public static final int DIALOGY_MIN_VAL_ADIC    = 20;
    public static final int DIALOGY_MAX_VAL_ADIC    = 21;
    public static final int DIALOGY_INCR_VAL_ADIC   = 22;
    public static final int DIALOGY_DRILL_DOWN      = 23;
    public static final int DIALOGY_DRILL_OPEN      = 24;
    public static final int DIALOGY_SUBMIT          = 25;
    public static final int DIALOGY_CANCEL          = 26;
    public static final int DIALOGY_DOTPAG          = 27;
    public static final int DIALOGY_GRAPH_PROPS     = 28;
    public static final int DIALOGY_SERIE           = 29;
    public static final int DIALOGY_FONT            = 30;
    public static final int DIALOGY_SIZE            = 31;
    public static final int DIALOGY_COLOR           = 32;
    public static final int DIALOGY_BGCOLOR         = 33;
    public static final int DIALOGY_GRAPH_BGCOLOR   = 34;
    public static final int DIALOGY_AXIS_XY         = 35;
    public static final int DIALOGY_AXIS_FONT       = 36;
    public static final int DIALOGY_AXIS_SIZE       = 37;
    public static final int DIALOGY_AXIS_COLOR      = 38;
    public static final int DIALOGY_SERIES_INTENS   = 39;
    public static final int DIALOGY_SERIES_SOBREPOS = 40;
    public static final int DIALOGY_BACK_IMAGE      = 41;
    public static final int DIALOGY_SHOW_VALUES     = 42;
    public static final int DIALOGY_SHOW_LEGEND     = 43;
    public static final int DIALOGY_MARCA_TITLE     = 44;
    public static final int DIALOGY_MARCA_TITLE_ADI = 45;
    public static final int DIALOGY_BASE_ADIC       = 46;
    public static final int DIALOGY_VISIBLE_DRILL   = 47;
    public static final int DIALOGY_BGCOLOR_IND     = 48;
    public static final int DIALOGY_FIELD           = 49;
    public static final int DIALOGY_DIMENSION       = 50;
    public static final int DIALOGY_PROPERTIES      = 51;
    public static final int DIALOGY_TEMPORAL        = 52;
    public static final int DIALOGY_YES             = 53;
    public static final int DIALOGY_NO              = 54;
    public static final int DIALOGY_DRILL_SOURCE    = 55;
    public static final int DIALOGY_DRILL_CHILD     = 56;
    public static final int DIALOGY_GRAPH_TYPE      = 57;
    public static final int DIALOGY_TOTALIZE        = 58;
    public static final int DIALOGY_MEASURE         = 59;
    public static final int DIALOGY_FUNC_AGREG      = 60;
    public static final int DIALOGY_ZONE_PROPS      = 61;
    public static final int DIALOGY_SHOW_ZONES      = 62;
    public static final int DIALOGY_ZONE_TITLE      = 63;
    public static final int DIALOGY_ZONE_BANDWITH   = 64;
    public static final int DIALOGY_ZONE_INIT_VAL   = 65;
    public static final int DIALOGY_ZONE_LAST_VAL   = 66;
    public static final int DIALOGY_ZONE_COLOR      = 67;
    public static final int DIALOGY_FIRST_ZONE      = 68;
    public static final int DIALOGY_SECOND_ZONE     = 69;
    public static final int DIALOGY_THIRD_ZONE      = 70;
    public static final int DIALOGY_FROM_CUBE       = 71;
    public static final int DIALOGY_HOWTOACCESSPROP = 72;
	public static final int DIALOGY_FORMAT_IND = 73;
	public static final int DIALOGY_CASAS_DECIMAIS = 74;
	public static final int DIALOGY_FONTE = 75;
	public static final int DIALOGY_EFEITOS = 76;
	public static final int DIALOGY_FORMAT_IND_OWNER = 77;
	
    
    /** Creates a new instance of DefinitionProperties */
    private DefinitionProperties() {
    }
    
    /**
     * M�todo est�tico respons�vel por recuperar a inst�ncia do singleton do
     *  objeto DefinitionProperties
     * @return
     *      Objeto do tipo <code>DefinitionProperties</code> contendo o singleton
     */
    public static DefinitionProperties getInstance() {
        return singleton;
    }
    
    /**
     * M�todo respons�vel pela limpeza do objeto singleton de DefinitionProperties
     */
    public void clean() {
        this.setCube(null);
        this.setAgregations(new ArrayList<AgregationFunction>());
        this.setAxis(DwAxis.AXIS_X, "");
        this.setAxis(DwAxis.AXIS_Y, "");
        this.setAxis(DwAxis.AXIS_MEASURE, "");
        this.setAxisDesc("");
        this.setAxisMeasures("");
        this.setAxisMConcat("");
        this.setAxisX("");
        this.setAxisXConcat("");
        this.setAxisY("");
        this.setAxisYConcat("");
        this.setDialogy(new ArrayList<Dialogy>());
        this.setDimension("");
        this.setDrillOff("");
        this.setDrillOn("");
        this.setGraph("");
        this.setGraphOn("");
        this.setGraphProperties("");
        this.setGraphProps("");
        this.setGraphType(0);
        this.setGraphTypeLabel("");
        this.setGraphTypes(new ArrayList<GraphType>());
        this.setGraphYProps("");
        this.setImagePath("");
        this.setMeasures("");
        this.setPropExtend("");
        this.setProps("");
        this.setSession("");
        this.setTable("");
        this.setTemporal(new ArrayList<String>());
        this.setUserDragDwObject(null);
    }
    
    public String getImagePath() {
        return strImagePath + "/";
    }

    public void setImagePath(String strImagePath) {
        this.strImagePath = strImagePath;
    }

    public String getPropExtend() {
        return strPropExtend;
    }

    public void setPropExtend(String strPropExtend) {
        this.strPropExtend = strPropExtend;
    }

    public boolean isPropExtend() {
        boolean blnPropOn = false;
        if (this.getPropExtend().equals("on")) {
            blnPropOn = true;
        }
        return blnPropOn;
    }

    public boolean isPropExtendOn() {
        boolean blnReturn = false;
        if (this.strPropExtend.toLowerCase() == "on") {
            blnReturn = true;
        }
        return blnReturn;
    }

    public List<String> getTemporal() {
        return lstTemporal;
    }

    public void setTemporal(List<String> lstTemporal) {
        this.lstTemporal = lstTemporal;
    }

    public void addTemporal(String strTemporal) {
        this.lstTemporal.add(strTemporal);
    }
    
    public Icon getTemporalImage() {
        return iconTemporal;
    }
    
    public void setTemporalImage(Icon iconTemporal) {
        this.iconTemporal = iconTemporal;
    }
    
    public List<GraphType> getGraphTypes() {
        return lstGraphTypes;
    }

    public void setGraphTypes(List<GraphType> lstGraphTypes) {
        this.lstGraphTypes = lstGraphTypes;
    }
    
    public void addGraphType(GraphType strGraphTypes) {
        this.lstGraphTypes.add(strGraphTypes);
    }
    
    public String getGraphTypeLabel() {
        return strGraphTypeLabel;
    }

    public void setGraphTypeLabel(String strGraphType) {
        this.strGraphTypeLabel = strGraphType;
    }
    
    public List<Dialogy> getDialogy() {
        return lstDialogy;
    }
    
    public Dialogy getLabelFromDialogy(int intIndexParam) {
        return getDialogy().get(intIndexParam-1);
    }
    
    public void setDialogy(List<Dialogy> lstDialogy) {
        this.lstDialogy = lstDialogy;
    }
    
    public void addDialogy(String strDialogy) {
        this.getDialogy().add(new Dialogy(strDialogy));
    }
    
    public void addDialogyWithChilds(String strDialogy, List<Dialogy> lstDialogyChilds) {
        this.getDialogy().add(new Dialogy(strDialogy, lstDialogyChilds));
    }
    
    public String getSumario() {
        return strSumario;
    }

    public void setSumario(String strSumario) {
        this.strSumario = strSumario;
    }
    
    public String getDrillDown() {
        return strDrillDown;
    }

    public void setDrillDown(String strDrillDown) {
        this.strDrillDown = strDrillDown;
    }
    
    public Icon getDrillChildImage() {
        return iconDrillChildImage;
    }

    public void setDrillChildImage(Icon iconDrillChildImage) {
        this.iconDrillChildImage = iconDrillChildImage;
    }
    
    public Icon getDrillParentImage() {
        return iconDrillParentImage;
    }

    public void setDrillParentImage(Icon iconDrillChildImage) {
        this.iconDrillParentImage = iconDrillChildImage;
    }
    
    public String getTotalize() {
        return strTotalize;
    }

    public void setTotalize(String strTotalize) {
        this.strTotalize = strTotalize;
    }
    
    public Icon getTotalizeImage() {
        return iconTotalizeImage;
    }

    public void setTotalizeImage(Icon iconTotalizeImage) {
        this.iconTotalizeImage = iconTotalizeImage;
    }
    
    public String getDrillOn() {
        return strDrillOn;
    }

    public void setDrillOn(String strDrillOn) {
        this.strDrillOn = strDrillOn;
    }

    public String getDrillOff() {
        return strDrillOff;
    }

    public void setDrillOff(String strDrillOff) {
        this.strDrillOff = strDrillOff;
    }

    public void setAxis(int intAxis, String strContents) {
        if (intAxis == DwAxis.AXIS_X || intAxis == DwAxis.AXIS_X_GRAPH) {
            setAxisXConcat(strContents);
        } else if (intAxis == DwAxis.AXIS_Y) {
            setAxisYConcat(strContents);
        } else if (intAxis == DwAxis.AXIS_MEASURE || intAxis == DwAxis.AXIS_MEASURE_GRAPH) {
            setAxisMConcat(strContents);
        }
    }
    
    public String getAxis(int intAxis) {
        String strContents = null;
        if (intAxis == DwAxis.AXIS_X || intAxis == DwAxis.AXIS_X_GRAPH) {
            strContents = getAxisXConcat();
        } else if (intAxis == DwAxis.AXIS_Y) {
            strContents = getAxisYConcat();
        } else if (intAxis == DwAxis.AXIS_MEASURE || intAxis == DwAxis.AXIS_MEASURE_GRAPH) {
            strContents = getAxisMConcat();
        }
        return strContents;
    }
    
    public String getAxisX() {
        return strAxisX;
    }

    public void setAxisX(String strAxisX) {
        this.strAxisX = strAxisX;
    }

    public String getAxisY() {
        return strAxisY;
    }

    public void setAxisY(String strAxisY) {
        this.strAxisY = strAxisY;
    }

    public String getAxisMeasures() {
        return strAxisMeasures;
    }

    public void setAxisMeasures(String strAxisMeasures) {
        this.strAxisMeasures = strAxisMeasures;
    }
    
    public String getAxisDesc() {
        return strAxisDesc;
    }

    public void setAxisDesc(String strAxisDesc) {
        this.strAxisDesc = strAxisDesc;
    }

    public String getGraph() {
        return strGraph;
    }

    public void setGraph(String strGraph) {
        this.strGraph = strGraph;
    }

    public String getProps() {
        return strProps;
    }

    public void setProps(String strProps) {
        this.strProps = strProps;
    }

    public String getDimension() {
        return strDimension;
    }

    public void setDimension(String strDimension) {
        this.strDimension = strDimension;
    }

    public String getMeasures() {
        return strMeasures;
    }

    public void setMeasures(String strMeasures) {
        this.strMeasures = strMeasures;
    }

    public String getTable() {
        return strTable;
    }

    public void setTable(String strTable) {
        this.strTable = strTable;
    }

    public String getSession() {
        return strSession;
    }

    public void setSession(String strSession) {
        this.strSession = strSession;
    }

    public Cube getCube() {
        if (objCube == null) {
            objCube = new Cube();
        }
        return objCube;
    }

    public void setCube(Cube objCube) {
        this.objCube = objCube;
    }
    
    /**
     * M�todo respons�vel por pesquisar, por uma dimens�o, nas propriedades de
     *  dimens�es associadas com esta defini��o
     * @param intDimId c�digo id da dimens�o
     * @return Objeto do tipo <code>Dimension</code> contendo a dimens�o pesquisada
     *  ou null caso n�o exista a dimens�o relacionada
     */
    public Dimension searchDimension(int intDimId) {
        Dimension dwReturn = null;
        
        for (Dimension dimension : this.getCube().getDimensions()) {
            if (dimension.getId() == intDimId) {
                dwReturn = dimension;
                break;
            }
        }
        
        return dwReturn;
    }
    
    /**
     * M�todo respons�vel por pesquisar, por um campo de uma dimens�o, nas propriedades
     * de dimens�es associadas com esta defini��o
     * @param intFieldId c�digo id do campo
     * @return Objeto do tipo <code>DwField</code> contendo o campo relacionado com o id passado
     * como par�metro ou null caso n�o exista o campo relacionado
     */
    public DwField searchDimensionField(int intFieldId) {
        return searchDimensionField(intFieldId, false, 0);
    }
    
    /**
     * M�todo respons�vel por pesquisar, por um campo de uma uma dimens�o, nas propriedades
     * de dimens�es associadas com esta defini��o
     * @param intFieldId c�digo do id do campo a ser pesquisado
     * @param isTemporalField sinaliza se este campo a ser pesquisado � um campo temporal
     * @param intTemporalField c�digo do id do campo temporal
     * @return Objeto do tipo <code>DwField</code> contendo o campo relacionado com o id passado
     * como par�metro ou null caso n�o exista o campo relacionado
     */ 
    public DwField searchDimensionField(int intFieldId, boolean isTemporalField, int intTemporalField) {
        DwField dwField = null;
        
        for (Dimension dimension : this.getCube().getDimensions()) {
            for (DwField dwObject : dimension.getDimFields()) {
                if (dwObject.getId() == intFieldId) {
                    if (!isTemporalField) {
                        dwField = dwObject;
                    } else {
                        for (DwField dwTemporalField : dwObject.getTemporal()) {
                            if (dwTemporalField.getTemporalID() == intTemporalField) {
                                dwField = dwTemporalField;
                            }
                        }
                    }
                }
            }
        }
        return dwField;
    }
    
    /**
     * M�todo respons�vel por pesquisar um indicador nas propriedades de indicadores
     * associadas com esta defini��o
     * @param intMeasureID c�digo id do indicador a ser pesquisado
     * @return Objeto do tipo <code>Measure</code> contendo o indicador relacionado com o id
     * passado como par�metro ou null caso n�o existe co-rela��o
     */
    public Measure searchMeasures(int intMeasureID) {
        Measure dwMeasure = null;
        
        for (Measure measure : this.getCube().getMeasures()) {
            if (measure.getId() == intMeasureID) {
                dwMeasure = measure;
                break;
            }
        }
        
        return dwMeasure;
    }

    /**
     * M�todo respons�vel por pesquisar uma fun��o agregadora nas propriedades associadas
     * com esta defini��o
     * @param intFuncAgregId c�digo do id da fun��o agregadora
     * @return Objeto do tipo <code>AgregationFunction</code> contendo o indicador relacionado
     * com o id passado como par�metro ou null caso n�o existe co-rela��o
     */
    public AgregationFunction searchFunctionAgregation(int intFuncAgregId) {
        AgregationFunction agregation = null;
        
        for (Iterator it = this.getAgregations().iterator(); it.hasNext();) {
            agregation = ((AgregationFunction) it.next());
            if (agregation.getId() == intFuncAgregId) {
                break;
            }
        }
        
        return agregation;
    }
    
    /**
     * M�todo respons�vel por pesquisar um tipo de gr�fico nas propriedades de
     * associadas com esta defini��o
     * @param intGraphType c�digo do id do tipo de gr�fico
     * @return GraphType
     */
    public GraphType searchGraphType(int intGraphType) {
        // cria o objeto de pesquisa com o id passado como par�metro
        GraphType graphType = new GraphType();
        graphType.setId(intGraphType);
        
        // recupera o index do objeto relacionado com o id passado como par�metro
        int intAxisGraph = this.getGraphTypes().indexOf(graphType);
        
        // verifica se o objeto GraphType relacionado com o id existe
        if (intAxisGraph > -1) {
            // recupera o objeto GraphType relacionado com o id
            graphType = this.getGraphTypes().get(intAxisGraph);
        }
        
        return graphType;
    }

    public DwField getUserDragDwObject() {
        return userDragDwObject;
    }

    public void setUserDragDwObject(DwField userDragDwObject) {
        this.userDragDwObject = userDragDwObject;
    }

    public String getGraphProperties() {
        return strGraphProperties;
    }

    public void setGraphProperties(String strGraphProperties) {
        this.strGraphProperties = strGraphProperties;
    }

    public String getGraphOn() {
        return strGraphOn;
    }

    public void setGraphOn(String strGraphOn) {
        this.strGraphOn = strGraphOn;
    }
    
    public boolean isGraphOn() {
        boolean blnIsGraph = false;
        if (this.getGraphOn() != "") {
            blnIsGraph = true;
        }
        return blnIsGraph;
    }
    
    public String getAxisXConcat() {
        return strAxisXConcat;
    }

    public void setAxisXConcat(String strAxisXConcat) {
        this.strAxisXConcat = strAxisXConcat;
    }

    public String getAxisYConcat() {
        return strAxisYConcat;
    }

    public void setAxisYConcat(String strAxisYConcat) {
        this.strAxisYConcat = strAxisYConcat;
    }

    public String getAxisMConcat() {
        return strAxisMConcat;
    }
 
    public void setAxisMConcat(String strAxisMConcat) {
        this.strAxisMConcat = strAxisMConcat;
    }

    public int getGraphType() {
        return this.intGraphType;
    }
    
    public void setGraphType(int intGraphType) {
        this.intGraphType = intGraphType;
    }

    public String getGraphProps() {
        return strGraphProps;
    }

    public void setGraphProps(String strGraphProps) {
        this.strGraphProps = strGraphProps;
    }

    public String getGraphYProps() {
        return strGraphYProps;
    }

    public void setGraphYProps(String strGraphYProps) {
        this.strGraphYProps = strGraphYProps;
    }
    
    public String getGraphY2Props() {
        return strGraphY2Props;
    }

    public void setGraphY2Props(String strGraphY2Props) {
        this.strGraphY2Props = strGraphY2Props;
    }
    
    public List<AgregationFunction> getAgregations() {
        return lstAgregations;
    }

    public void setAgregations(List<AgregationFunction> lstAgregations) {
        this.lstAgregations = lstAgregations;
    }

    public void addAgregation(AgregationFunction poAgregation) {
        this.lstAgregations.add(poAgregation);
    }
    
    public String getAgregLabel() {
        return strAgregLabel;
    }

    public void setAgregLabel(String strAgregLabel) {
        this.strAgregLabel = strAgregLabel;
    }

    public Icon getTreeLeaf() {
        return icTreeLeaf;
    }

    public void setTreeLeaf(Icon icTreeLeaf) {
        this.icTreeLeaf = icTreeLeaf;
    }

    public Icon getTreeOpen() {
        return icTreeOpen;
    }

    public void setTreeOpen(Icon icTreeOpen) {
        this.icTreeOpen = icTreeOpen;
    }

    public Icon getTreeClose() {
        return icTreeClose;
    }

    public void setTreeClose(Icon icTreeClose) {
        this.icTreeClose = icTreeClose;
    }

    public Color getAppletsSelections() {
        return clrAppletsSelections;
    }

    public void setAppletsSelections(Color clrAppletsSelections) {
        this.clrAppletsSelections = clrAppletsSelections;
    }

    public Color getAppletsBgcolor() {
        return clrAppletsBgcolor;
    }

    public void setAppletsBgcolor(Color clrAppletsBgcolor) {
        this.clrAppletsBgcolor = clrAppletsBgcolor;
    }

    public Color getAppletsWorkspaceBgcolor() {
        return clrAppletsWorkspaceBgcolor;
    }

    public void setAppletsWorkspaceBgcolor(Color clrAppletsWorkspaceBgcolor) {
        this.clrAppletsWorkspaceBgcolor = clrAppletsWorkspaceBgcolor;
    }

    public Color getButtonBgcolor() {
        return clrButtonBgcolor;
    }

    public void setButtonBgcolor(Color clrButtonBgcolor) {
        this.clrButtonBgcolor = clrButtonBgcolor;
    }

    public Color getButtonBgcolorDrillDown() {
        return clrButtonBgcolorDrillDown;
    }

    public void setButtonBgcolorDrillDown(Color clrButtonBgcolorDrillDown) {
        this.clrButtonBgcolorDrillDown = clrButtonBgcolorDrillDown;
    }

    public Color getButtonFontColorDrillDown() {
        return clrButtonFontColorDrillDown;
    }

    public void setButtonFontColorDrillDown(Color clrButtonFontColorDrillDown) {
        this.clrButtonFontColorDrillDown = clrButtonFontColorDrillDown;
    }

    public Color getAcceptDropColor() {
        return clrAcceptDrop;
    }

    public void setAcceptDropColor(Color clrAcceptDrop) {
        this.clrAcceptDrop = clrAcceptDrop;
    }

    public Color getRejectDragColor() {
        return clrRejectDrag;
    }

    public void setRejectDragColor(Color clrRejectDrag) {
        this.clrRejectDrag = clrRejectDrag;
    }
    
}