package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Representa um dialo (par�metro do applet), podendo um dialogo conter
 *  outros dialogos filhos
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 26 de Abril de 2006
 */
public class Dialogy extends DwObject {
    
    private List<Dialogy> lstDialogyChilds = new ArrayList<Dialogy>();
    private String strValue = "";
    
    /**
     * Constr�i um JavaBean do tipo Dialogy
     */
    public Dialogy() {
    }
    
    /**
     * Constr�i um JavaBean do tipo Dialogy, passando o nome do dialogo
     */
    public Dialogy(String nome) {
        super();
        this.setName(nome);
        this.setValue(nome);
    }
    
    /**
     * Constr�i um JavaBean do tipo Dialogy passando o nome e o valor para este dialogo
     * @param nome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strValue
     *      Obejto do tipo <code>Icon</code> contendo o valor
     */
    public Dialogy(String nome, String strValue) {
        super();
        this.setName(nome);
        this.setValue(strValue);
    }
    
    /**
     * Constr�i um JavaBean do tipo Dialogy passando o nome e a lista de dialogos
     *  filhos para este dialogo
     * @param nome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param lstDialogyChilds
     *      Cole��o do tipo <code>List</code> contendo obejtos do tipo
     *  <code>Icon</code> contendo a lista de filhos
     */
    public Dialogy(String nome, List<Dialogy> lstDialogyChilds) {
        super();
        this.setName(nome);
        this.setLstDialogyChilds(lstDialogyChilds);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de cada objeto suportado.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getObjectType() {
        return OBJECT_DIALOGY;
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de dialogos filhos relacionados
     *  com este dialogo (caso existam).
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Dialogy</code> com os dialogos
     */
    public List<Dialogy> getDialogyChilds() {
        return lstDialogyChilds;
    }
    
    /**
     * M�todo respons�vel por definir a lista de dialogos filhos relacionados
     *  com este dialogo
     * @param lstDialogyChilds
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>Dialogy</code> com os dialogos
     */
    public void setLstDialogyChilds(List<Dialogy> lstDialogyChilds) {
        this.lstDialogyChilds = lstDialogyChilds;
    }
    
    /**
     * M�todo respons�vel por recuperar o valor para este dialogo
     * @return
     *      Objeto do tipo <code>String</code> contendo o atributo
     */
    public String getValue() {
        return strValue;
    }
    
    /**
     * M�todo respons�vel por definir o valor para este dialogo
     * @param strDialogyValue
     *      Objeto do tipo <code>String</code> contendo o atributo
     */
    public void setValue(String strDialogyValue) {
        this.strValue = strDialogyValue;
    }
    
    /**
     * M�todo respons�vel pela compara��o entre diversos objetos do mesmo tipo
     *  Dialogy. Esta compara��o ser� realiazada atrav�s da verifica��o do tipo dos objetos,
     *  que deveram ser do tipo <code>Dialogy</code>, do atributo name e do atributo value.
     * @param obj
     *      Objeto do tipo <code>Object</code> contendo o objeto a ser comparado com este
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o true se forem iguais
     *  e false caso contr�rio
     */
    public boolean equals(Object obj) {
        boolean blnEquals = false;
        Dialogy dialogyEquals;
        
        if (obj instanceof Dialogy) {
            dialogyEquals = (Dialogy) obj;
            if (dialogyEquals.getValue().equalsIgnoreCase(this.getValue())) {
                this.setName(dialogyEquals.getName());
                blnEquals = true;
            }
        } 
        
        return blnEquals;
    }
    
}