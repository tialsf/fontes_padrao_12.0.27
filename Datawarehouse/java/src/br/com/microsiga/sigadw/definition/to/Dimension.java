package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.definition.*;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * Representa uma dimens�o de um cubo
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 18 de Abril de 2006
 */
public class Dimension extends DwObject {
    
    private List<DwField> lstDimFields = new ArrayList<DwField>();
    
    /**
     * Constr�i um JavaBean do tipo Dimension
     */
    public Dimension() {
    }
    
    /**
     * Constr�i um JavaBean do tipo Dimension passando o seu id, nome e icone correspondente
     * @param id
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param nome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconDimension
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public Dimension(int id, String nome, String strIconDimension) {
        super(id, nome, strIconDimension);
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de campos relacionados com esta dimens�o
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>DwField</code> com os campos
     */
    public List<DwField> getDimFields() {
        return lstDimFields;
    }

    /**
     * M�todo respons�vel por definir a lista de campos relacionados com esta dimens�o
     * @param lstDimFields
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>DwField</code> com os campos
     */
    public void setDimFields(List<DwField> lstDimFields) {
        this.lstDimFields = lstDimFields;
    }
    
    /**
     * M�todo respons�vel por adicionar um campo � lista de campos relacionados
     *  com esta dimens�o
     * @param oDimFields
     *      Objeto do tipo <code>DwField</code> com o campo a ser adicionado
     */
    public void addDimField(DwField oDimFields) {
        this.lstDimFields.add(oDimFields);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de cada objeto suportado.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getObjectType() {
        return OBJECT_DIMENSION;
    }
    
}