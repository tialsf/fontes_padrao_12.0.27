package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.definition.DwButton;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe respons�vel por representar a visibilidade de indicadores 
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Junho de 2006
 */
public class DrillDownVisibility {
    
    // atributo de drill donw vis�vel
    private boolean blnVisible;
    // atributo de drill donw disabilitado
    private boolean blnDisabled;
    // atributo contendo o dwButton relacionado com esta visibilidade
    private DwButton dwButton;
    
    /**
     * Constr�i um JavaBean do tipo DrillDownVisibility
     */
    public DrillDownVisibility() {
    }
    
    /**
     * Constr�i um JavaBean do tipo DrillDownVisibility, passando como par�metro
     *  se ele est� vis�vel ou n�o
     */
    public DrillDownVisibility(boolean blnVisible) {
        this.setVisible(blnVisible);
        this.setDisabled(false);
        this.setDwButton(null);
    }
    
    /**
     * Constr�i um JavaBean do tipo DrillDownVisibility, passando como par�metro
     *  se ele est� vis�vel ou n�o, o objeto dwButton relacionado a esta visibilidade
     */
    public DrillDownVisibility(boolean blnVisible, DwButton dwButton) {
        this.setVisible(blnVisible);
        this.setDisabled(false);
        this.setDwButton(dwButton);
    }
    
    /**
     * Constr�i um JavaBean do tipo DrillDownVisibility, passando como par�metro
     *  se ele est� vis�vel ou n�o, se esta a defini��o desta visibilidade est�
     *  desabilitada e o objeto dwButton relacionado a esta visibilidade
     */
    public DrillDownVisibility(boolean blnVisible, boolean blnDisabled, DwButton dwButton) {
        this.setVisible(blnVisible);
        this.setDisabled(blnDisabled);
        this.setDwButton(dwButton);
    }
    
    /**
     * M�todo respons�vel por recuperar o atributo com a visibilidade deste objeto
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o atributo
     */
    public boolean isVisible() {
        return blnVisible;
    }
    
    /**
     * M�todo respons�vel por definir o atributo com a visibilidade deste objeto
     * @param blnVisible
     *      Primitiva do tipo <code>boolean</code> contendo o atributo
     */
    public void setVisible(boolean blnVisible) {
        this.blnVisible = blnVisible;
    }
    
    /**
     * M�todo respons�vel por recuperar o atributo com a desabilita��o ou n�o
     *  da defini��o da visibilidade para este objeto
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o atributo
     */
    public boolean isDisabled() {
        return blnDisabled;
    }
    
    /**
     * M�todo respons�vel por definir o atributo com a desabilita��o ou n�o
     *  da defini��o da visibilidade para este objeto
     * @param blnDisabled
     *      Primitiva do tipo <code>boolean</code> contendo o atributo
     */
    public void setDisabled(boolean blnDisabled) {
        this.blnDisabled = blnDisabled;
    }
    
    /**
     * M�todo respons�vel por recuperar o atributo com objeto dwbutton relacionado
     *  com esta defini��o da visibilidade para este objeto
     * @return
     *      Objeto do tipo <code>DwButton</code> contendo o atributo
     */
    public DwButton getDwButton() {
        return dwButton;
    }
    
    /**
     * M�todo respons�vel por definir o atributo com objeto dwbutton relacionado
     *  com esta defini��o da visibilidade para este objeto
     * @param dwButton
     *      Objeto do tipo <code>DwButton</code> contendo o atributo
     */
    public void setDwButton(DwButton dwButton) {
        this.dwButton = dwButton;
    }
    
    /**
     * M�todo est�tico respons�vel por gerar um lista de defini��es de visibilidades
     *  a partir de uma lista de bot�es.
     * @param lstButtons
     *      Cole��o do tipo <code>List</code> com objetos do tipo <code>DwButton</code>
     *  contendo bot�es
     */
    public static List<DrillDownVisibility> generateVisibility(List<DwButton> lstButtons) {
        List<DrillDownVisibility> lstVisibility = new ArrayList<DrillDownVisibility>();
        for (DwButton dwButton : lstButtons) {
            lstVisibility.add(new DrillDownVisibility(true, dwButton));
        }
        return lstVisibility;
    }
    
    /**
     * M�todo respons�vel pela compara��o entre diversos objetos do mesmo tipo
     *  DrillDownVisibility. Esta compara��o se dar� atrav�s do tipo dos objetos,
     *  que deveram ser do tipo <code>DrillDownVisibility</code>, e atrav�s da
     *  compara��o dos seus respectivos objetos do tipo <code>DwButton</code>.
     * @param obj
     *      Objeto do tipo <code>Object</code> contendo o objeto a ser comparado com este
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o true se forem iguais
     *  e false caso contr�rio
     */
    public boolean equals(Object obj) {
        boolean blnEquals = false;
        
        if (obj instanceof DrillDownVisibility
               && ((DrillDownVisibility) obj).getDwButton() == this.getDwButton()) {
            blnEquals = true;
        }
        
        return blnEquals;
    }
    
}