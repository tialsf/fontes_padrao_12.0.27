package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * Representa um campo para um objeto do dw (dimens�o ou indicador)
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 25 de Abril de 2006
 */
public class DwField extends DwObject {
    
    // flag que define se este campo � um campo temporal
    private boolean blnIsTemporal = false;
    
    // cont�m o id do campo temporal
    private int intTemporalID = 0;
    
    // lista de sub-campos temporais
    private List<DwField> lstTemporal = new ArrayList<DwField>();
    
    // c�digo da dimens�o deste campo
    private int intDimID = -1;
    
    /**
     * Constr�i um JavaBean do tipo DwField
     */
    public DwField() {
        super();
    }
    
    /**
     * Constr�i um JavaBean do tipo DwField passando o seu id, nome e icone correspondente
     * @param intId
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strNome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o �cone deste objeto
     */
    public DwField(int intId, String strNome, String strIconElement) {
        super(intId, strNome, strIconElement);
    }
    
    /**
     * Constr�i um JavaBean do tipo DwField passando o seu id, nome, icone correspondente e
     *  o c�digo id do campo temporal
     * @param intTemporalFieldID
     *      Primitva do tipo <code>int</code> contendo o c�digo id temporal
     * @param intId
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strNome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public DwField(int intTemporalFieldID, int intId, String strNome, String strIconElement) {
        super(intId, strNome, strIconElement);
        this.blnIsTemporal = true;
        this.setTemporalID(intTemporalFieldID);
    }
    
    /**
     * Constr�i um JavaBean do tipo DwField passando o seu id, nome, icone
     *  correspondente e o id da dimens�o relacionado com este campo
     * @param intId
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strNome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o �cone deste objeto
     * @param intDimId
     *      Primitva do tipo <code>int</code> contendo o id da dimens�o
     */
    public DwField(int intId, String strNome, String strIconElement, int intDimId) {
        super(intId, strNome, strIconElement);
        this.setDimID(intDimId);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo de cada objeto suportado.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getObjectType() {
        return OBJECT_DIMFIELD;
    }
    
    /**
     * M�todo respons�vel por identificar se este campo possui campos temporais
     *  relacionados
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo true em caso afirmativo,
     *  false caso contr�rio
     */
    public boolean hasTemporal() {
        return (lstTemporal.size() > 0);
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de campos temporais relacionados
     *  com este campo (caso existam).
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>DwField</code> com os campos temporais
     */
    public List<DwField> getTemporal() {
        return lstTemporal;
    }
    
    /**
     * M�todo respons�vel por adicionar um campo a lista de campos temporais relacionados
     *  com este campo.
     * @param fieldTemporal
     *      Objeto do tipo <code>DwField</code> com o campo temporal
     */
    public void addTemporal(DwField fieldTemporal) {
        this.lstTemporal.add(fieldTemporal);
    }

    /**
     * M�todo respons�vel por identificar se este campo � um campo temporal
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo true em caso afirmativo,
     *  false caso contr�rio
     */
    public boolean isTemporal() {
        return blnIsTemporal;
    }
    
    /**
     * M�todo respons�vel por recuperar o id da dimens�o relacionado com este campo
     * @return
     *      Primitiva do tipo <code>int</code> contendo o id
     */
    public int getDimID() {
        return intDimID;
    }
    
    /**
     * M�todo respons�vel por definir o id da dimens�o relacionado com este campo
     * @param intDimID
     *      Primitiva do tipo <code>int</code> contendo o id
     */
    public void setDimID(int intDimID) {
        this.intDimID = intDimID;
    }
    
    /**
     * M�todo respons�vel por recuperar o id do campo temporal
     * @return
     *      Primitiva do tipo <code>int</code> contendo o id
     */
    public int getTemporalID() {
        return intTemporalID;
    }
    
    /**
     * M�todo respons�vel por definir o id do campo temporal
     * @param intTemporalID
     *      Primitiva do tipo <code>int</code> contendo o id
     */
    public void setTemporalID(int intTemporalID) {
        this.intTemporalID = intTemporalID;
    }
    
    /**
     * M�todo respons�vel pela compara��o entre diversos objetos do mesmo tipo
     *  DwObject. Esta compara��o se dar� atrav�s do atributo ID e se possuem
     *  campos temporais.
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o true se forem iguais
     *  e false caso contr�rio
     */
    public boolean equals(Object obj) {
        boolean blnEquals = false;
        
        if (obj instanceof DwField) {
            DwField dwField = (DwField) obj;
            if (dwField.getId() == this.getId() && dwField.getDimID() == this.getDimID()
                    && dwField.getTemporal().equals(this.getTemporal())
                        && dwField.getTemporalID() == this.getTemporalID()
                            && dwField.hasTemporal() == this.hasTemporal()
                               && dwField.isTemporal() == this.isTemporal()) {
                blnEquals = true;
            }
        } else {
            blnEquals = super.equals(obj);
        }
        
        return blnEquals;
    }
    
}