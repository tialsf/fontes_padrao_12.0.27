package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import javax.swing.Icon;

/**
 * Classe representa um tipo espec�fico de gr�fico (barra, linh, etc.)
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Abril de 2006
 */
public class GraphType extends DwObject {
    
    /**
     * Constr�i um JavaBean do tipo GraphType
     */
    public GraphType() {
    }
    
    /**
     * Constr�i um JavaBean do tipo GraphType passando o seu id, nome e icone correspondente
     * @param intId
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param strLabel
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconGraph
     *      Obejto do tipo <code>Icon</code> contendo um �cone
     */
    public GraphType(int intId, String strLabel, String strIconGraph) {
        super.setId(intId);
        super.setName(strLabel);
        super.setIconPath(strIconGraph);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo deste Objeto
     * @return id
     *      Primitiva do tipo <code>int</code> contendo o c�digo do tipo deste objeto
     */
    public int getObjectType() {
        return OBJECT_GRPH_TYPE;
    }
    
}