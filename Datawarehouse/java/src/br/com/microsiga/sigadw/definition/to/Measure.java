package br.com.microsiga.sigadw.definition.to;

import br.com.microsiga.sigadw.definition.to.AgregationFunction;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;

/**
 * Representa um JavaBean do tipo indicador
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 25 de Abril de 2006
 */
public class Measure extends DwField {
    
    // lista com as agrega��es para este indicador
    private List<AgregationFunction> lstAgregations = new ArrayList<AgregationFunction>();
    
    /**
     * Constr�i um JavaBean do tipo Measure
     */
    public Measure() {
    }
    
    /**
     * Constr�i um JavaBean do tipo Measure passando o seu id, nome e icone correspondente
     * @param id
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param nome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o caminho para o �cone deste objeto
     */
    public Measure(int id, String nome, String strIconElement) {
        super(id, nome, strIconElement);
    }
    
    /**
     * M�todo respons�vel por recuperar o tipo deste Objeto
     * @return
     *      Primitiva do tipo <code>int</code> contendo o c�digo do tipo deste objeto
     */
    public int getObjectType() {
        return OBJECT_MEASURE;
    }
    
    /**
     * M�todo respons�vel por recuperar a lista de fun��es agregadoras relacionadas
     *  com este indicador
     * @return
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>AgregationFunction</code> contendo as fun��es agregadoras deste indicador
     */
    public List<AgregationFunction> getAgregations() {
        return lstAgregations;
    }
    
    /**
     * M�todo respons�vel por definir a lista de fun��es agregadoras relacionadas
     *  com este indicador
     * @param lstAgregations
     *      Cole��o do tipo <code>List</code> contendo objetos do tipo
     *  <code>AgregationFunction</code> contendo as fun��es agregadoras deste indicador
     */
    public void setAgregations(List<AgregationFunction> lstAgregations) {
        this.lstAgregations = lstAgregations;
    }
    
    /**
     * M�todo respons�vel por adicionar uma fun��o agregador � lista de fun��es agregadoras
     *  relacionadas com este indicador.
     * @param oAgregations
     *      Objeto do tipo <code>AgregationFunction</code> contendo s fun��o agregador
     */
    public void addAgregation(AgregationFunction oAgregations) {
        this.lstAgregations.add(oAgregations);
    }
    
}