package br.com.microsiga.sigadw.desktop;

import br.com.microsiga.sigadw.desktop.event.DwDesktopListener;
import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragGestureListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragSourceListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDropTargetListener;
import br.com.microsiga.sigadw.event.dragndrop.DwTransferable;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import br.com.microsiga.sigadw.http.DWHttp;
import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 * Classe que representa o espao de trabalho (desktop) de um usurio
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 18 de Agosto de 2006
 */
public class DwDesktop extends JPanel implements DwDesktopListener, DwDragDropEventListener<DwDesktopNode> {
    
    public static final short IMG_IC_TREE_PASTAFECHADA      = 0x0000;
    public static final short IMG_IC_TREE_PASTAABERTA       = 0x0001;
    public static final short IMG_IC_TREE_CONSULTA          = 0x0002;
    public static final short IMG_IC_TREE_CONSULTAUSUARIO   = 0x0003;
    public static final short IMG_IC_TREE_FILE              = 0x0004;
    public static final short IMG_IC_TREE_TABLE             = 0x0005;
    public static final short IMG_IC_TREE_USER_TABLE        = 0x0006;
    public static final short IMG_IC_TREE_GRAPH             = 0x0007;
    public static final short IMG_IC_TREE_USER_GRAPH        = 0x0008;
    public static final short IMG_IC_TREE_LAST              = IMG_IC_TREE_USER_GRAPH;
    
    public static final short IMG_IC_DESKTOP_32_PASTA           = 0x0000;
    public static final short IMG_IC_DESKTOP_32_NOVAPASTA       = 0x0001;
    public static final short IMG_IC_DESKTOP_32_PASTAACIMA      = 0x0002;
    public static final short IMG_IC_DESKTOP_32_TABELA          = 0x0003;
    public static final short IMG_IC_DESKTOP_32_TABELAUSUARIO   = 0x0004;
    public static final short IMG_IC_DESKTOP_32_GRAFICO         = 0x0005;
    public static final short IMG_IC_DESKTOP_32_GRAFICOUSUARIO  = 0x0006;
    public static final short IMG_IC_DESKTOP_32_FILE            = 0x0007;
    public static final short IMG_IC_DESKTOP_32_NEWFILE         = 0x0008;
    public static final short IMG_IC_DESKTOP_LAST               = IMG_IC_DESKTOP_32_NEWFILE;
    
    public static final String QUERY_PREDEFINED                 = "P";
    public static final String QUERY_USER                       = "U";
    
    public static Image m_imageListTree[];
    public static Image m_imageListDesktop[];
    
    public static final int OPER_CREATE  = 5;
    public static final int OPER_UPDATE  = 3;
    public static final int OPER_DELETE  = 4;
    public static final int OPER_DISPLAY = 9;

    public static final int SET_WALLPAPER  = 8;
    
    private DwDragSourceListener dragSourceListener;
    private DwDragGestureListener dragGestureListener;
    private DragSource dragSource;
    
    private DesktopProperties desktopPropertiesSingleton = DesktopProperties.getInstance();
    
    /**
     * Creates new form DwDesktop
     */
    public DwDesktop() {
        super();
        initImageList();
        initComponents();
        initializeComponents();
    }
    
    /**
     * Inicializa as listas de imagens da arvore e do desktop
     */
    public void initImageList() {
        m_imageListTree = new Image[IMG_IC_TREE_LAST + 1];
        m_imageListTree[IMG_IC_TREE_PASTAFECHADA] = getImage("ic_pastafechada.gif");
        m_imageListTree[IMG_IC_TREE_PASTAABERTA] = getImage("ic_pastaaberta.gif");
        m_imageListTree[IMG_IC_TREE_CONSULTA] = getImage("ic_consulta.gif");
        m_imageListTree[IMG_IC_TREE_CONSULTAUSUARIO] = getImage("ic_consultadeusuario.gif");
        m_imageListTree[IMG_IC_TREE_FILE] = getImage("ic_file.gif");
        m_imageListTree[IMG_IC_TREE_TABLE] = getImage("ic_table.gif");
        m_imageListTree[IMG_IC_TREE_USER_TABLE] = getImage("ic_user_table.gif");
        m_imageListTree[IMG_IC_TREE_GRAPH] = getImage("ic_graph.gif");
        m_imageListTree[IMG_IC_TREE_USER_GRAPH] = getImage("ic_user_graph.gif");
        
        m_imageListDesktop = new Image[IMG_IC_DESKTOP_LAST + 1];
        m_imageListDesktop[IMG_IC_DESKTOP_32_PASTA] = getImage("ic_32_pasta.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_NOVAPASTA] = getImage("ic_32_novapasta.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_PASTAACIMA] = getImage("ic_32_pastaacima.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_TABELA] = getImage("ic_32_tabela.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_GRAFICO] = getImage("ic_32_grafico.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_TABELAUSUARIO] = getImage("ic_32_tabelausuario.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_GRAFICOUSUARIO] = getImage("ic_32_graficousuario.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_FILE] = getImage("ic_32_file.gif");
        m_imageListDesktop[IMG_IC_DESKTOP_32_NEWFILE] = getImage("ic_32_newfile.gif");
    }
    
    /**
     * Carrega a lista de imagens da arvore para a arvore e
     * inicializa o desktop
     */
    public void initializeComponents() {
        this.m_tree.setBackground(Color.white);
        this.m_tree.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                TreePath treePath = ((JTree)e.getSource()).getSelectionPath();
                if (treePath != null) {
                    DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) treePath.getLastPathComponent();
                    DwDesktopNode dwDesktopNode = (DwDesktopNode) dmtn.getUserObject();
                    if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES
                            || dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
                        dwFavorites.updateFavorites(dwDesktopNode);
                    } else {
                        desktopScenarioManager.updateDesktop(dwDesktopNode);
                    }
                }
            }
        });
        // tree
       m_tree.setRootVisible(true);
       m_tree.setCellRenderer(new DwTreeCellRendererDesktop());
        
        DefaultMutableTreeNode dmtnGeneral = new DefaultMutableTreeNode("Desktop do Usuário");
        TreeModel tmDestop = new DefaultTreeModel(dmtnGeneral);
       m_tree.setModel(tmDestop);
        
        this.jscpTree.setViewportView(m_tree);
        
        this.desktopScenarioManager = new br.com.microsiga.sigadw.desktop.DwDesktopScenarioManager(this);
        this.dwFavorites = dwFavorites = new br.com.microsiga.sigadw.desktop.DwFavorites(this);
        this.jsplFavScen.setRightComponent(this.desktopScenarioManager);
        this.jsplFavScen.setLeftComponent(this.dwFavorites);
        
        setBackground(Color.white);
        
        this.dragSourceListener     = new DwDragSourceListener();
        this.dragGestureListener    = new DwDragGestureListener(this.dragSourceListener);
        this.dragSource             = DragSource.getDefaultDragSource();
        
        dragSource.createDefaultDragGestureRecognizer(m_tree, DnDConstants.ACTION_COPY, this.dragGestureListener);
        
        DwDropTargetListener dwDDlistener = new DwDropTargetListener<DwDesktopNode>(this, DwTransferable.sigaDwQuery);
        new DropTarget(m_tree, DnDConstants.ACTION_COPY, dwDDlistener, true);
    }
    
    /**
     * Carrega uma imagem a partir do diretrio passado como parmetro no applet
     * @param imageFile nome do arquivo a ser lido
     * @return objeto do tipo <code>Image</code> contendo a imagem carregada
     */
    public Image getImage(String imageFile) {
        return getImageResource(desktopPropertiesSingleton.getImagePath() + "/" + imageFile);
    }
    
    /**
     * Carrega uma imagem a partir de um caminho especifico
     * @param imageFile nome do arquivo a ser lido
     * @return objeto do tipo <code>Image</code> contendo a imagem carregada
     */
    public Image getImageResource(String imageFile) {
        Image img = null;
        BufferedInputStream bis1 = null;
        try {
            bis1 = new BufferedInputStream(new URL(imageFile).openStream());
        } catch (FileNotFoundException ex) {
            System.err.println("FileNotFoundException");
            System.err.println("Cause: " + ex.getCause());
            System.err.println("Message: " + ex.getMessage());
            System.err.println("Verify the missing image: " + imageFile);
            ex.printStackTrace();
        } catch (MalformedURLException me) {
            System.err.println("MalformedURLException");
            System.err.println("Cause: " + me.getCause());
            System.err.println("Message: " + me.getMessage());
            System.err.println("Verify the missing image: " + imageFile);
            me.printStackTrace();
        } catch (IOException me) {
            System.err.println("IOException");
            System.err.println("Cause: " + me.getCause());
            System.err.println("Message: " + me.getMessage());
            System.err.println("Verify the missing image: " + imageFile);
            me.printStackTrace();
        }
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        
        if (bis1 != null) {
            // copy into a byte array since the createImage() methods
            // in awt either take a url (which we do not have) or
            // a byte array.
            //
            //
            byte buf[] = new byte[8192];
            int pos = 0;
            int ch;
            try {
                while ((ch = bis1.read()) != -1) {
                    if (buf.length == pos) {
                        byte newbuf[] = new byte[buf.length * 2];
                        System.arraycopy(buf, 0, newbuf, 0, buf.length);
                        buf = newbuf;
                    }
                    buf[pos++] = (byte)ch;
                }
                // This is done because createImage does NOT
                // make its own copy of the bytearray.
                // so we want to make sure that we do not
                // waste space.
                //
                byte newbuf[] = new byte[pos];
                System.arraycopy(buf, 0, newbuf, 0, pos);
                
                img = Toolkit.getDefaultToolkit().createImage(newbuf);
                baos1.close();
                bis1.close();
            } catch (IOException e) {
                System.err.println("IO Exception");
                System.err.println("Cause: " + e.getCause());
                System.err.println("Message: " + e.getMessage());
                System.err.println("Verify the missing image: " + imageFile);
                e.printStackTrace();
            }
        }
        return img;
    }
    
    /**
     * Adiciona um n root na arvore
     * @param desktopRoot novo n a ser adicionado
     */
    public void addNodeToRoot(DwDesktopNode desktopRoot) {
        DefaultTreeModel treeModel      = (DefaultTreeModel) m_tree.getModel();
        DefaultMutableTreeNode dmtnRoot = new DefaultMutableTreeNode(desktopRoot.getDescricao());
                
        dmtnRoot.setUserObject(desktopRoot);
        
        treeModel.insertNodeInto(dmtnRoot, (DefaultMutableTreeNode) treeModel.getRoot(), treeModel.getChildCount(treeModel.getRoot()));
        
        m_tree.updateUI();
    }
    
    /**
     * Adiciona um n root na arvore
     * @param desktopRoot novo n a ser adicionado
     * @param intParentID id do n pai ao qual o novo n ser adicionado
     */
    public void addNodeToRoot(DwDesktopNode desktopRoot, int intParentID) {
        DefaultTreeModel treeModel          = (DefaultTreeModel) m_tree.getModel();
        DefaultMutableTreeNode dmtnParent   = (DefaultMutableTreeNode) treeModel.getRoot();
        
        DwDesktopNode nodeSearch = desktopRoot.clone();
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_WORKSPACE);
        nodeSearch.setId(intParentID);
        nodeSearch.setRootID(intParentID);
        
        this.addNodeToNode(desktopRoot, nodeSearch);
    }
    
    /**
     * Adiciona um n na arvore
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n
     * @param parentID ID do n pai deste n adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, int parentID) {
        DefaultTreeModel treeModel          = (DefaultTreeModel) m_tree.getModel();
        DefaultMutableTreeNode dmtnParent   = (DefaultMutableTreeNode) treeModel.getRoot();
        
        DwDesktopNode nodeSearch = newNode.clone();
        nodeSearch.setId(parentID);
        
        List<DefaultMutableTreeNode> lstTreeSearch = this.searchNodeTree(nodeSearch);
        if (lstTreeSearch.size() > 0) {
            dmtnParent = lstTreeSearch.get(0);
        }
        
        this.addNodeToNode(newNode, dmtnParent);
    }
    
    /**
     * Adiciona um n na arvore
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n
     * @param dmtnParentNode n pai deste n adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, DefaultMutableTreeNode dmtnParentNode) {
        DefaultTreeModel treeModel          = (DefaultTreeModel) m_tree.getModel();
        DefaultMutableTreeNode dmtnChild    = new DefaultMutableTreeNode(newNode);
        
        treeModel.insertNodeInto(dmtnChild, dmtnParentNode, dmtnParentNode.getChildCount());
        
        this.expandDesktopNode(dmtnChild);
        m_tree.updateUI();
    }
    
    public void expandDesktopNode(DwDesktopNode nodeChild) {
        List<DefaultMutableTreeNode> lstNodes = this.searchNodeTree(nodeChild);
        if (lstNodes != null && lstNodes.size() > 0) {
            this.expandDesktopNode(lstNodes.get(0));
        }
    }
    
    public void expandDesktopNode(DefaultMutableTreeNode dmtnChild) {
        for (TreeNode node : dmtnChild.getPath()) {
            if (node.getChildCount() > 0) {
                m_tree.expandRow( ((DefaultMutableTreeNode) node).getLevel() );
            }
        }
    }
    
    /**
     * Adiciona um n na arvore a partir de n pai a ser pesquisado. Se no encontrar o
     *  <code>parentNodeSearch</code> ento o novo n ser adicionado ao root
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n
     * @param parentNodeSearch objeto do tipo <code>DwDesktopNode</code> contendo o n pai deste n adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, DwDesktopNode parentNodeSearch) {
        List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree(parentNodeSearch);
        if (lstTreeNodes != null && lstTreeNodes.size() > 0) {
            this.addNodeToNode(newNode, lstTreeNodes.get(0));
        } else {
            this.addNodeToNode(newNode, 0);
        }
    }
    
    public List<DwDesktopNode> searchDesktopNode(DwDesktopNode nodeObjectCompare) {
        List<DwDesktopNode> lstDesktopNodes = new ArrayList<DwDesktopNode>();
        List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree(nodeObjectCompare);
        
        for (DefaultMutableTreeNode treeNode : lstTreeNodes) {
            lstDesktopNodes.add((DwDesktopNode) treeNode.getUserObject());
        }
        
        return lstDesktopNodes;
    }
    
    public List<DefaultMutableTreeNode> searchNodeTree(DwDesktopNode nodeObjectCompare) {
        return this.searchNodeTree((DefaultMutableTreeNode) m_tree.getModel().getRoot(), nodeObjectCompare);
    }
    
    public List<DefaultMutableTreeNode> searchNodeTree(DefaultMutableTreeNode nodeParent, DwDesktopNode nodeObjectCompare) {
        List<DefaultMutableTreeNode> lstReturnNodes = new ArrayList<DefaultMutableTreeNode>();
        DwDesktopNode nodeObject;
        DefaultMutableTreeNode nodeChild;
        
        for (int i = 0; i < nodeParent.getChildCount(); i++) {
            nodeChild = (DefaultMutableTreeNode) m_tree.getModel().getChild(nodeParent, i);
            
            nodeObject = (DwDesktopNode) nodeChild.getUserObject();
            if (nodeObject != null && nodeObject.equals(nodeObjectCompare)) {
                lstReturnNodes.add( (DefaultMutableTreeNode) nodeChild );
            }
            
            if (!nodeChild.isLeaf()) {
                lstReturnNodes.addAll( this.searchNodeTree(nodeChild, nodeObjectCompare) );
            }
        }
        
        return lstReturnNodes;
    }
    
    /**
     * Altera um n na arvore
     * @param node objeto do tipo <code>DwDesktopNode</code> contendo o n a ser
     *  alterado
     */
    public void updateNode(DwDesktopNode node) {
        this.m_tree.updateUI();
    }
    
    /**
     * Remove um n na arvore
     * @param nodeObjectCompare objeto do tipo <code>DwDesktopNode</code> contendo o n a ser removido
     */
    public void removeNode(DwDesktopNode nodeObjectCompare) {
        for (DefaultMutableTreeNode treeNode : this.searchNodeTree(nodeObjectCompare)) {
            ((DefaultMutableTreeNode) treeNode.getParent()).remove(treeNode);
        }
        m_tree.updateUI();
    }
    
    public void addScenarioNode(DwDesktopNode dwDesktopNode) {
        this.desktopScenarioManager.buildDwIcones(dwDesktopNode, false);
    }
    
    public void addScenarioNode(DwDesktopNode dwDesktopNode, boolean blnAddNode) {
        this.desktopScenarioManager.buildDwIcones(dwDesktopNode, blnAddNode);
    }
    
    public void createScenario(int intScenarioID, String strScenarioName, boolean blnAddNode, boolean blnAddDBNode) {
        this.desktopScenarioManager.createScenario(intScenarioID, strScenarioName, blnAddNode, blnAddDBNode);
    }
    
    public int createNodeDB(DwDesktopNode dwDesktopNode) {
        System.out.println("Creating desktop node: " + dwDesktopNode);
        
        String strReturn = this.doPersistNode(OPER_CREATE, dwDesktopNode);
        int intReturn = -1;
        int count = 0;
 
        // Se o n� for de imagem, resgata o nome gravado no banco para efetuar o load.
        strReturn = this.doPersistNode(SET_WALLPAPER, dwDesktopNode);
        if (!strReturn.equals("")) {
            dwDesktopNode.setName(strReturn);
        }
        
        // workaround - verifica se retornou o ID do nó criado,
        // senão acessa o base de dados para recuper-lo
        while ("".equals(strReturn) && count < desktopPropertiesSingleton.getMaxCreateNodeConn()) {
            try {
                count++;
                System.err.println("Retrieving ID from doPersistNode() failed " + strReturn);
                Thread.sleep(desktopPropertiesSingleton.getMaxCreateNodeConnSleep());
                strReturn = this.doPersistNode(OPER_DISPLAY, dwDesktopNode);
            } catch (InterruptedException ex) {
                System.err.println(System.currentTimeMillis() + " HTTPConnection Thread failed");
                System.err.println("Cause: " + ex.getCause());
                System.err.println("Message: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        
        if ("".equals(strReturn)) {
            throw new IllegalStateException("Retrieving ID from doPersistNode() failed");
        }
        
        if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_TABLE && dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_GRAPH) {
            try {
                intReturn = Integer.parseInt(strReturn);
            } catch (NumberFormatException nfe) {
                System.err.println("NumberFormatException - node creating ID");
                System.err.println("Cause: " + nfe.getCause());
                System.err.println("Message: " + nfe.getMessage());
                nfe.printStackTrace();
            }
            dwDesktopNode.setId(intReturn);
            this.updateNodeDB(dwDesktopNode);
        }
        
        System.out.println("Node Added: " + dwDesktopNode.getId() + "," + dwDesktopNode.getRootID() + "-" + dwDesktopNode);
        
        return intReturn;
    }
    
    public void deleteNodeDB(DwDesktopNode dwDesktopNode) {
        this.doPersistNode(OPER_DELETE, dwDesktopNode);
    }
    
    public void updateDesktop() {
        this.desktopScenarioManager.updateUI();
        this.dwFavorites.updateUI();
    }
    
    public void updateNodeDB(DwDesktopNode dwDesktopNode) {
        this.doPersistNode(OPER_UPDATE, dwDesktopNode);
    }
    
    public void updateDesktopFromDB(DwDesktopNode dwDesktopNode, int parentID) {
        dwDesktopNode.setRootID(parentID);
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
            dwDesktopNode.setImgCloseNode(DwDesktop.IMG_IC_TREE_PASTAFECHADA);
            dwDesktopNode.setImgOpenNode(DwDesktop.IMG_IC_TREE_PASTAABERTA);
            
            this.dwFavorites.updateNewFolder(dwDesktopNode, parentID);
        } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
            dwDesktopNode.setImgCloseNode(DwDesktop.IMG_IC_TREE_FILE);
            dwDesktopNode.setImgOpenNode(DwDesktop.IMG_IC_TREE_FILE);
            
            this.dwFavorites.updateNewFile(dwDesktopNode, parentID);
        } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_WALLPAPER) {
            if (!dwDesktopNode.getDescricao().trim().equals("")) {
                this.desktopPropertiesSingleton.setBackgroundURL(dwDesktopNode.getDescricao());
                this.desktopPropertiesSingleton.setBackgroundImage(this.getImageResource(dwDesktopNode.getDescricao()));
                this.desktopScenarioManager.updateUI();
            }
        } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_SCENARIO) {
            dwDesktopNode.setImgCloseNode(DwDesktop.IMG_IC_TREE_PASTAFECHADA);
            dwDesktopNode.setImgOpenNode(DwDesktop.IMG_IC_TREE_PASTAABERTA);
            dwDesktopNode.setScenarioId(dwDesktopNode.getId());
            dwDesktopNode.setScenarioName(dwDesktopNode.getName());
            
            this.desktopScenarioManager.createScenario(dwDesktopNode, true, false);
        } else {
            DwDesktopNode nodeSearch = dwDesktopNode.clone();
            nodeSearch.setId(parentID);
            
            nodeSearch.setType(DwDesktopNode.dwDESKTOP_COMPARE);

            List<DwDesktopNode> lstNodes = this.searchDesktopNode(nodeSearch);
            if (lstNodes != null && lstNodes.size() == 0) {
                this.addNodeToNode(dwDesktopNode, parentID);
                this.desktopScenarioManager.updateFromDB(dwDesktopNode, parentID);    
            } else {
                DwDesktopNode dwDesktopNodeParent = lstNodes.get(0);

                if (dwDesktopNodeParent.getType() == DwDesktopNode.dwDESKTOP_SCENARIO) {
                    dwDesktopNode.setScenarioId(dwDesktopNodeParent.getId());
                    dwDesktopNode.setScenarioName(dwDesktopNodeParent.getName());
                } else {
                    dwDesktopNode.setScenarioId(dwDesktopNodeParent.getScenarioId());
                }

                this.addNodeToNode(dwDesktopNode, dwDesktopNodeParent);

                if (dwDesktopNodeParent.getType() != DwDesktopNode.dwDESKTOP_FAVORITES
                        && dwDesktopNodeParent.getType() != DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER
                        && dwDesktopNodeParent.getType() != DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
                    this.desktopScenarioManager.updateFromDB(dwDesktopNode, dwDesktopNodeParent);
                    this.desktopScenarioManager.activeScenario();
                } else {
                    this.dwFavorites.updateFromDB(dwDesktopNode, dwDesktopNodeParent);
                    this.dwFavorites.activeFavorites();
                }
            }
        } 
    }
    
    protected synchronized String doPersistNode(int oper, DwDesktopNode dwDesktopNode) {
        DWHttp talk;
        URL url = null;
        String s = "", surl;
        
        // Pega a url do applet
        if (desktopPropertiesSingleton.getCodeBase() == null) {
            System.err.println("DoPersist desabilitado...");
            return null;
        }
        
        surl = desktopPropertiesSingleton.getCodeBase().toString() + "/w_sigadw3.apw?action=userDesktop&oper=" + oper + "&_forceReload=" + System.currentTimeMillis();
        
        try {
            s = "&id_parnt=" + dwDesktopNode.getRootID() + "&name=" + URLEncoder.encode(dwDesktopNode.getName(), "ISO-8859-1")
                + "&type=" + dwDesktopNode.getType() + "&tipo_cons=" + dwDesktopNode.getQueryType() + "&origem=JAVA";
            
            if (dwDesktopNode.getId() != -3) {
                s += "&id_no=" + dwDesktopNode.getId();
            }
            
            url = new URL(surl + s);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.err.println("Erro ao criar URL(" + surl + "): " + e.getMessage());
            e.printStackTrace();
        }
        
        // Conectar
        talk = new DWHttp(url);
        talk.openRequest("POST", desktopPropertiesSingleton.getSession());
        
        // Enviar
	talk.write(s);
        
        // Encerrar
	talk.closeRequest();
        
        // Receber
        String strResponse = talk.getResponse();
        
        if (strResponse != "") {
            strResponse = strResponse.trim();
        }
	return strResponse;
    }

    public void notifyDragDrop(DwDesktopNode dwDesktopNode) {
        // necessrios somente para habilitaar a funcionalidade de drag and drop
    }

    public void notifyDropEnd() {
        // necessrios somente para habilitaar a funcionalidade de drag and drop
    }

    public void notifyDropEnd(DwDesktopNode dwObject) {
        // necessrios somente para habilitaar a funcionalidade de drag and drop
    }

    public void handleDropAccept(Point point) {
        this.setBackground(desktopPropertiesSingleton.getAcceptDropColor());
    }

    public void handleDropReject(Point point) {
        this.setBackground(desktopPropertiesSingleton.getRejectDragColor());
    }

    public void handleDropExit() {
        this.setBackground(desktopPropertiesSingleton.getAppletsWorkspaceBgcolor());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jsplMain = new javax.swing.JSplitPane();
        jsplFavScen = new javax.swing.JSplitPane();
        jscpTree = new javax.swing.JScrollPane();
        m_tree = new javax.swing.JTree();

        jsplMain.setDividerSize(4);
        jsplMain.setResizeWeight(0.4);
        jsplMain.setToolTipText("");
        jsplMain.setOneTouchExpandable(true);

        jsplFavScen.setDividerLocation(130);
        jsplFavScen.setDividerSize(4);
        jsplFavScen.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jsplFavScen.setResizeWeight(0.1);
        jsplFavScen.setOneTouchExpandable(true);
        jsplMain.setRightComponent(jsplFavScen);

        jscpTree.setViewportView(m_tree);

        jsplMain.setLeftComponent(jscpTree);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jsplMain, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 861, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jsplMain, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jscpTree;
    private javax.swing.JSplitPane jsplFavScen;
    private javax.swing.JSplitPane jsplMain;
    private javax.swing.JTree m_tree;
    // End of variables declaration//GEN-END:variables
    private br.com.microsiga.sigadw.desktop.DwDesktopScenarioManager desktopScenarioManager;
    private br.com.microsiga.sigadw.desktop.DwFavorites dwFavorites;
    
}