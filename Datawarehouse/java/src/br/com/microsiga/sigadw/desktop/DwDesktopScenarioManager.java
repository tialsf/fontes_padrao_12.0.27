package br.com.microsiga.sigadw.desktop;

import static br.com.microsiga.sigadw.desktop.DwDesktop.*;
import br.com.microsiga.sigadw.desktop.event.DwDesktopListener;
import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDropTargetListener;
import br.com.microsiga.sigadw.event.dragndrop.DwTransferable;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import pv.util.PVURL;

/**
 * Classe que representa o gerenciador de cenrios do SigaDW e os seus respectivos
 *  arquivos/consultas
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 18 de Agosto de 2006
 */
public class DwDesktopScenarioManager extends javax.swing.JPanel implements ActionListener, MouseListener,
        DwDragDropEventListener<DwDesktopNode> {
    
    public static final int DEFAULT_SCENARIO_ID     = -1;
    
    protected final String ACTION_WALLPAPER     = "wallpaper";
    protected final String ACTION_ADD_SCENARIO  = "addScenario";
    protected final String ACTION_UPT_SCENARIO  = "updScenario";
    protected final String ACTION_DEL_SCENARIO  = "delScenario";
    
    protected final String ACTION_ADD_FOLDER    = "addFolder";
    protected final String ACTION_UP_FOLDER     = "upFolder";
    
    protected final String ACTION_FOLDER_OPEN   = "openFolder";
    protected final String ACTION_FOLDER_UPDATE = "updateFolder";
    protected final String ACTION_FOLDER_DELETE = "deleteFolder";
    
    private int intActiveScenario = -1;
    private List<DwDesktopNode> lstScenarios = new ArrayList<DwDesktopNode>();
    
    private final static Color ACTIVE_SCENARIO          = new Color(255,255,255);
    private final static Color ACTIVE_SCENARIO_FORE     = Color.BLACK;
    private final static Color DESACTIVE_SCENARIO       = new Color(236,233,216);
    private final static Color DESACTIVE_SCENARIO_FORE  = Color.GRAY;
    private final static Color BACKGROUND_COLOR         = new Color(255,255,255);
    
    private DwDesktopListener desktopListener;
    
    private DwDesktopNode activeNode;
    
    private DesktopProperties properties = DesktopProperties.getInstance();
    
    /**
     * Creates new form DwDesktopScenarioManager
     */
    public DwDesktopScenarioManager() {
        initComponents();
        initialize();
    }
    
    /**
     * Creates new form DwDesktopScenarioManager
     */
    public DwDesktopScenarioManager(DwDesktopListener desktopListener) {
        this.desktopListener = desktopListener;
        initComponents();
        initialize();
    }
    
    protected void initialize() {
        DwDropTargetListener<DwDesktopNode> dwDDlistener = new DwDropTargetListener<DwDesktopNode>(this, DwTransferable.sigaDwQuery);
        new DropTarget(this.getDwRootPanel(), DnDConstants.ACTION_COPY, dwDDlistener, true);
        this.getDwRootPanel().addMouseListener(this);
    }
    
    public JPanel getDwRootPanel() {
        return jpnlMain;
    }
    
    public boolean canAddNode(DwDesktopNode dwDesktopNode) {
        boolean blnCanAdd = false;
        
        if (this.activeNode == null && !this.getScenario().getNodes().contains(dwDesktopNode)) {
            blnCanAdd = true;
        } else if (this.activeNode != null && !this.activeNode.getNodes().contains(dwDesktopNode)) {
            blnCanAdd = true;
        }
        
        return blnCanAdd;
    }
    
    public void notifyDragDrop(DwDesktopNode dwDesktopNode) {
        int intRootId = 0;
        
        if (this.activeNode != null) {
            intRootId = this.activeNode.getId();
        } else {
            intRootId = this.getScenario().getId();
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
            this.copyFolder(dwDesktopNode);
        } else {
            if (dwDesktopNode.haveTable()) {
                this.createTable(dwDesktopNode, intRootId);
            }

            if (dwDesktopNode.haveGraph()) {
                this.createGraph(dwDesktopNode, intRootId);
            }
        }
    }
    
    protected void createTable(DwDesktopNode dwDesktopNode, int intRootId) {
        this.createQuery(dwDesktopNode, intRootId, DwDesktopNode.dwDESKTOP_TABLE, "10");
    }
    
    protected void createGraph(DwDesktopNode dwDesktopNode, int intRootId) {
        this.createQuery(dwDesktopNode, intRootId, DwDesktopNode.dwDESKTOP_GRAPH, "01");
    }
    
    protected void createQuery(DwDesktopNode dwDesktopNode, int intRootId, int intQueryType, String strQryDisp) {
        DwDesktopNode node = dwDesktopNode.clone();
        node.setRootID(intRootId);
        node.setType(intQueryType);
        node.setConsDisp(strQryDisp);
        if (this.canAddNode(node)) {
            this.buildDwIcones(node);
            this.desktopListener.createNodeDB(node);
        }
    }
    
    public void notifyDropEnd() {
        
    }
    
    public void notifyDropEnd(DwDesktopNode dwDesktopNode) {
        DwIcone icone;
        JComponent jcomp;
        DwDesktopNode nodeSearch = dwDesktopNode.clone();
        
        if (this.activeNode == null) {
            this.getScenario().removeNode(dwDesktopNode);
        } else {
            List<DwDesktopNode> node = this.searchNodeParent(this.getScenario(), this.activeNode);
            if (node != null && node.size() > 0) {
                node.get(0).removeNode(dwDesktopNode);
            }
        }
        
        for (Component comp : this.jpnlMain.getComponents()) {
            if (comp instanceof JComponent) {
                jcomp = (JComponent) comp;
                if (jcomp != null && jcomp.getComponentCount() == 1
                        && jcomp.getComponent(0) instanceof DwIcone) {
                    icone = (DwIcone) jcomp.getComponent(0);
                    if (icone.getDesktopNode() != null && icone.getDesktopNode().equals(dwDesktopNode)) {
                        this.jpnlMain.remove(comp);
                    }
                } else {
                    icone = (DwIcone) comp;
                    if (icone.getDesktopNode() != null && icone.getDesktopNode().equals(dwDesktopNode)) {
                        this.jpnlMain.remove(comp);
                    }
                }
            }
        }
        
        this.desktopListener.removeNode(dwDesktopNode);
        this.updateUI();
        this.desktopListener.deleteNodeDB(dwDesktopNode);
    }
    
    public void handleDropAccept(Point point) {
        this.setBackground(properties.getAcceptDropColor());
    }

    public void handleDropReject(Point point) {
        this.setBackground(properties.getRejectDragColor());
    }

    public void handleDropExit() {
        this.setBackground(properties.getAppletsWorkspaceBgcolor());
    }
    
    protected void buildDwIcones(DwDesktopNode dwDesktopNode) {
        this.buildDwIcones(dwDesktopNode, true);
    }
    
    protected void buildDwIcones(DwDesktopNode dwDesktopNode, boolean blnAddNode) {
        DwDesktopNode scenario = this.getScenario();
        dwDesktopNode.setScenarioId(scenario.getId());
        dwDesktopNode.setScenarioName(scenario.getName());
        
        if (dwDesktopNode == null) {
            throw new IllegalArgumentException("buildDwIcones: parâmetro dwDesktopNode null");
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_TABLE
                || (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_QUERY && dwDesktopNode.haveTable())) {
            DwDesktopNode nodeTable = dwDesktopNode.clone();
            nodeTable.setType(DwDesktopNode.dwDESKTOP_TABLE);
            
            final DwIcone icone = new DwIcone(m_imageListDesktop[(dwDesktopNode.getQueryType().equals(QUERY_PREDEFINED)) ? IMG_IC_DESKTOP_32_TABELA : IMG_IC_DESKTOP_32_TABELAUSUARIO],
                    dwDesktopNode.getDescricao(),  dwDesktopNode.getName(), nodeTable, this, BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);
            
            if (blnAddNode) {
                // pesquisa pelo n pai ao qual ser inserido
                List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree();
                if (lstTreeNodes.size() > 0) {
                    this.desktopListener.addNodeToNode(nodeTable, lstTreeNodes.get(0));
                } else {
                    this.desktopListener.addNodeToNode(nodeTable, 1);
                }
                
                if (this.activeNode == null) {
                    this.getScenario().addNode(nodeTable);
                } else {
                    List<DwDesktopNode> node = this.searchNodeParent(this.getScenario(), this.activeNode);
                    if (node != null && node.size() > 0) {
                        node.get(0).addNode(nodeTable);
                    }
                }
            }
            
            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        execQuery(icone);
                    }
                }
            });
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_GRAPH
                || (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_QUERY && dwDesktopNode.haveGraph())) {
            
            DwDesktopNode nodeGraph = dwDesktopNode.clone();
            nodeGraph.setType(DwDesktopNode.dwDESKTOP_GRAPH);
            
            final DwIcone icone = new DwIcone(m_imageListDesktop[(dwDesktopNode.getQueryType().equals(QUERY_PREDEFINED)) ? IMG_IC_DESKTOP_32_GRAFICO : IMG_IC_DESKTOP_32_GRAFICOUSUARIO],
                    dwDesktopNode.getDescricao(), dwDesktopNode.getName(), nodeGraph, this, BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);
            
            if (blnAddNode) {
                List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree();
                if (lstTreeNodes.size() > 0) {
                    this.desktopListener.addNodeToNode(nodeGraph, lstTreeNodes.get(0));
                } else {
                    this.desktopListener.addNodeToNode(nodeGraph, 1);
                }
                
                if (this.activeNode == null) {
                    this.getScenario().addNode(nodeGraph);
                } else {
                    List<DwDesktopNode> node = this.searchNodeParent(this.getScenario(), this.activeNode);
                    if (node != null && node.size() > 0) {
                        node.get(0).addNode(nodeGraph);
                    }
                }
            }
            
            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        execQuery(icone);
                    }
                }
            });
        }
        this.updateUI();
    }
    
    protected List<DefaultMutableTreeNode> searchNodeTree() {
        DwDesktopNode nodeScenarioSearch;
        
        // verifica se no tem nenhum n ativo
        if (this.activeNode == null) {
            nodeScenarioSearch = new DwDesktopNode();
            
            nodeScenarioSearch.setType(DwDesktopNode.dwDESKTOP_SCENARIO);
            nodeScenarioSearch.setScenarioId(this.getScenario().getId());
        } else {
            nodeScenarioSearch = this.activeNode;
        }
        
        return this.desktopListener.searchNodeTree(nodeScenarioSearch);
    }
    
    public void createScenario(String strScenarioName) {
        this.createScenario(0, strScenarioName, true, true);
    }
        
    public void createScenario(int intId, String strScenarioName, boolean blnAddNode, boolean blnAddDBNode) {
        final DwDesktopNode newScenarioNode = new DwDesktopNode();
        newScenarioNode.setId(intId);
        newScenarioNode.setName(strScenarioName);
        newScenarioNode.setRootID(DEFAULT_SCENARIO_ID);
        newScenarioNode.setType(DwDesktopNode.dwDESKTOP_SCENARIO);
        newScenarioNode.setNodeEnabled(false);
        newScenarioNode.setImgCloseNode(IMG_IC_TREE_PASTAFECHADA);
        newScenarioNode.setImgOpenNode(IMG_IC_TREE_PASTAABERTA);
        newScenarioNode.setScenarioId(intId);
        newScenarioNode.setScenarioName(strScenarioName);
        
        this.createScenario(newScenarioNode, blnAddNode, blnAddDBNode);
    }
    
    public void createScenario(final DwDesktopNode newScenarioNode) {
        this.createScenario(newScenarioNode, true, true);
    }
    
    public void createScenario(final DwDesktopNode newScenarioNode, boolean blnAddNode, boolean blnAddDBNode) {
        this.lstScenarios.add(newScenarioNode);
        
        final JButton jbtnScenario = new JButton(newScenarioNode.getName());
        jbtnScenario.setFocusPainted(false);
        jbtnScenario.setBackground(DESACTIVE_SCENARIO);
        jbtnScenario.setForeground(DESACTIVE_SCENARIO_FORE);
        jbtnScenario.setHorizontalAlignment(JButton.LEFT);
        jbtnScenario.setActionCommand(String.valueOf(newScenarioNode.getId()));
        jbtnScenario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                activeScenario( Integer.parseInt(e.getActionCommand()) );
            }
        });
        
        jbtnScenario.setContentAreaFilled(false);
        jbtnScenario.setOpaque(true);
        
        jbtnScenario.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    JPopupMenu jpm = defineScenariosMenuPopup(newScenarioNode);
                    jpm.show(jbtnScenario, e.getX(), e.getY());
                }
            }
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    JPopupMenu jpm = defineScenariosMenuPopup(newScenarioNode);
                    jpm.show(jbtnScenario, e.getX(), e.getY());
                }
            }
        });
        
        this.jpnlScenarios.add(jbtnScenario);
        this.updateUI();
        
        if (blnAddNode) {
            DwDesktopNode nodeSearch = new DwDesktopNode();
            nodeSearch.setType(DwDesktopNode.dwDESKTOP_WORKSPACE);
            List<DefaultMutableTreeNode> lstTreeNodes = this.desktopListener.searchNodeTree(nodeSearch);
            if (lstTreeNodes.size() > 0) {
                this.desktopListener.addNodeToNode(newScenarioNode, lstTreeNodes.get(0));
            } else {
                this.desktopListener.addNodeToNode(newScenarioNode, 0);
            }
        }
            
        if (newScenarioNode.getId() != DEFAULT_SCENARIO_ID && blnAddDBNode) {
            this.desktopListener.createNodeDB(newScenarioNode);
        }
    }
    
    public DwDesktopNode createScenarioNode(String strNodeName, int intNodeType) {
        DwDesktopNode nodeSearch = new DwDesktopNode();
        nodeSearch.setScenarioId(this.getScenario().getId());
        nodeSearch.setName(this.getScenario().getName());
        nodeSearch.setScenarioName(nodeSearch.getDescricao());
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_SCENARIO);
        
        return this.createScenarioNode(strNodeName, intNodeType, nodeSearch);
    }
    
    public DwDesktopNode createScenarioNode(String strNodeName, int intNodeType, DwDesktopNode nodeParent) {
        int intRootId = -1;
        
        if (this.activeNode != null) {
            intRootId = this.activeNode.getId();
        } else {
            intRootId = this.getScenario().getId();
        }
        
        return this.createScenarioNode(strNodeName, intNodeType, intRootId, nodeParent);
    }
    
    public DwDesktopNode createScenarioNode(String strNodeName, int intNodeType, int intRootId, DwDesktopNode nodeParent) {
        DwDesktopNode node = new DwDesktopNode();
        node.setId(0);
        node.setRootID(intRootId);
        node.setName(strNodeName);
        node.setType(intNodeType);
        node.setImgCloseNode(IMG_IC_TREE_PASTAFECHADA);
        node.setImgOpenNode(IMG_IC_TREE_PASTAABERTA);
        node.setNodeEnabled(false);
        node.setScenarioId(this.getScenario().getId());
        
        DwDesktopNode dwCurrentScenario = this.getScenario();
        
        if (this.activeNode == null) {
            node.setRootID(dwCurrentScenario.getScenarioId());
            dwCurrentScenario.addNode(node);
        } else {
            node.setRootID(this.activeNode.getId());
            this.activeNode.addNode(node);
        }
        
        this.desktopListener.addNodeToNode(node, nodeParent);
        
        this.desktopListener.createNodeDB(node);
        
        return node;
    }
    
    public void renameScenarioNode(DwDesktopNode node) {
        this.desktopListener.updateNode(node);
        this.desktopListener.updateNodeDB(node);
    }
    
    public void deleteScenario(int intScenarioId) {
        DwDesktopNode nodeDelete = this.getScenario(intScenarioId);
        
        int intIndex = this.lstScenarios.indexOf(nodeDelete);
        if (intIndex != -1) {
            this.lstScenarios.remove(intIndex);
            this.jpnlScenarios.remove(intIndex);
            this.updateUI();
        }
        
        this.desktopListener.removeNode(nodeDelete);
        
        this.desktopListener.deleteNodeDB(nodeDelete);
        
        if (this.intActiveScenario == intScenarioId) {
            this.activeScenario();
        }
    }
    
    public void refreshCurrent() {
        if (this.activeNode != null) {
            this.activeFolder(this.activeNode);
        } else {
            this.activeScenario(this.getScenario());
        }
    }
    
    public void activeFolder(DwDesktopNode nodeFolder) {
        this.activeNode = nodeFolder;
        
        this.getDwRootPanel().removeAll();
        this.setActiveScenario(nodeFolder.getScenarioId());
        this.setDefaultIcones();
        this.refreshFolder(nodeFolder);
        this.updateUI();
        
        this.buildFolderPath();
    }
    
    public void activeScenario() {
        this.activeScenario(DEFAULT_SCENARIO_ID);
    }
    
    public void activeScenario(int intScenarioId) {
        this.activeNode = null;
        
        this.getDwRootPanel().removeAll();
        this.setActiveScenario(intScenarioId);
        this.setDefaultIcones();
        this.refreshScenario(intScenarioId);
        this.updateUI();
        
        this.buildScenarioPath();
    }
    
    public void activeScenario(DwDesktopNode nodeScenario) {
        this.activeScenario(nodeScenario.getScenarioId());
    }
    
    public void refreshFolder(DwDesktopNode nodeFolder) {
        for (DwDesktopNode dwDesktopNode : nodeFolder.getNodes()) {
            if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_FOLDER) {
                this.buildDwIcones(dwDesktopNode, false);
            } else {
                this.getDwRootPanel().add( this.buildFolderIcone(dwDesktopNode) );
                this.updateUI();
            }
        }
    }
    
    public void refreshScenario(int intScenarioId) {
        List<DwDesktopNode> lstNodes = new ArrayList<DwDesktopNode>();
        lstNodes = this.getScenario(intScenarioId).getNodes();
        
        for (DwDesktopNode dwDesktopNode : lstNodes) {
            if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_FOLDER) {
                this.buildDwIcones(dwDesktopNode, false);
            } else {
                this.getDwRootPanel().add( this.buildFolderIcone(dwDesktopNode) );
                this.updateUI();
            }
        }
    }
    
    public void setDefaultIcones() {
        DwIcone icone;
        
        if (this.activeNode != null) {
            icone = new DwIcone(m_imageListDesktop[IMG_IC_DESKTOP_32_PASTAACIMA],
                    properties.getLabelFolder(), properties.getLabelUpper(), BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);
            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(java.awt.event.MouseEvent evt) {
                    if (evt.getClickCount() == 2) {
                        backFolder();
                    }
                }
            });
        }
        
        icone = new DwIcone(m_imageListDesktop[IMG_IC_DESKTOP_32_NOVAPASTA],
                properties.getLabelNew(), properties.getLabelFolder(), BACKGROUND_COLOR, this);
        this.getDwRootPanel().add(icone);
        icone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    createNewFolder();
                }
            }
        });
        this.updateUI();
    }
    
    public DwDesktopNode getScenario() {
        return this.getScenario(intActiveScenario);
    }
    
    public DwDesktopNode getScenario(int intScenarioId) {
        DwDesktopNode scenario = null;
        
        DwDesktopNode nodeSearch = new DwDesktopNode();
        nodeSearch.setScenarioId(intScenarioId);
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_SCENARIO);
        
        int intIndex = this.lstScenarios.indexOf(nodeSearch);
        if (intIndex != -1) {
            scenario = this.lstScenarios.get(intIndex);
        }
        
        return scenario;
    }
    
    public void setActiveScenario(int intActiveScenario) {
        this.intActiveScenario = intActiveScenario;
        
        this.jlblScenario.setText(this.getScenario().getName());
        
        DwDesktopNode nodeSearch = new DwDesktopNode();
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_SCENARIO);
        nodeSearch.setScenarioId(intActiveScenario);
        
        int intIndex = this.lstScenarios.indexOf(nodeSearch);
        if (intIndex != -1) {
            Component compActive = this.jpnlScenarios.getComponent(intIndex);
            for (Component comp : this.jpnlScenarios.getComponents()) {
                if (compActive != comp) {
                    comp.setBackground(DESACTIVE_SCENARIO);
                    comp.setForeground(DESACTIVE_SCENARIO_FORE);
                } else {
                    comp.setBackground(ACTIVE_SCENARIO);
                    comp.setForeground(ACTIVE_SCENARIO_FORE);
                }
            }
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnlTitle = new javax.swing.JPanel();
        jlblScenario = new javax.swing.JLabel();
        jlblPath = new javax.swing.JLabel();
        jsplMain = new javax.swing.JSplitPane();
        jpnlScenarioManagement = new javax.swing.JPanel();
        jcpScenarios = new javax.swing.JScrollPane();
        jpnlScenarios = new javax.swing.JPanel();
        jlblScenarioTitle = new javax.swing.JLabel();
        jlblScenarioTitle.setText(DesktopProperties.getInstance().getLabelScenarioMaintenance());
        jscpWorkspace = new javax.swing.JScrollPane();
        jpnlMain = new DwScenario();

        setBackground(new java.awt.Color(255, 255, 255));
        setAlignmentX(0.0F);
        setAlignmentY(0.0F);
        setDoubleBuffered(false);
        setLayout(new java.awt.BorderLayout());

        jpnlTitle.setBackground(new java.awt.Color(255, 255, 255));
        jpnlTitle.setLayout(new java.awt.BorderLayout());

        jlblScenario.setBackground(new java.awt.Color(123, 160, 202));
        jlblScenario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlblScenario.setForeground(new java.awt.Color(255, 255, 255));
        jlblScenario.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jlblScenario.setOpaque(true);
        jlblScenario.setPreferredSize(new java.awt.Dimension(59, 25));
        jpnlTitle.add(jlblScenario, java.awt.BorderLayout.NORTH);

        jlblPath.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jlblPath.setForeground(new java.awt.Color(22, 25, 125));
        jlblPath.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jlblPath.setOpaque(true);
        jlblPath.setPreferredSize(new java.awt.Dimension(16, 16));
        jpnlTitle.add(jlblPath, java.awt.BorderLayout.CENTER);

        add(jpnlTitle, java.awt.BorderLayout.NORTH);

        jsplMain.setBorder(null);
        jsplMain.setDividerSize(4);
        jsplMain.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jsplMain.setResizeWeight(0.5);
        jsplMain.setOneTouchExpandable(true);

        jpnlScenarioManagement.setLayout(new java.awt.BorderLayout());

        jpnlScenarios.setBackground(new java.awt.Color(255, 255, 255));
        jpnlScenarios.setLayout(new java.awt.GridLayout(0, 1));
        jcpScenarios.setViewportView(jpnlScenarios);

        jpnlScenarioManagement.add(jcpScenarios, java.awt.BorderLayout.CENTER);

        jlblScenarioTitle.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jlblScenarioTitle.setForeground(new java.awt.Color(22, 25, 125));
        jlblScenarioTitle.setOpaque(true);
        jlblScenarioTitle.setPreferredSize(new java.awt.Dimension(16, 16));
        jpnlScenarioManagement.add(jlblScenarioTitle, java.awt.BorderLayout.NORTH);

        jsplMain.setRightComponent(jpnlScenarioManagement);

        jscpWorkspace.setBackground(new java.awt.Color(255, 255, 255));

        jpnlMain.setBackground(new java.awt.Color(255, 255, 255));
        jpnlMain.setLayout(new java.awt.GridLayout(0, 8));
        jscpWorkspace.setViewportView(jpnlMain);

        jsplMain.setLeftComponent(jscpWorkspace);

        add(jsplMain, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
    
    private JPopupMenu defineScenariosMenuPopup() {
        JPopupMenu jpmScenarios = new JPopupMenu();
        
        JMenuItem jmiAddScenario = new JMenuItem(properties.getLabelAddScenario());
        jmiAddScenario.setActionCommand(ACTION_ADD_SCENARIO);
        jmiAddScenario.addActionListener(this);
        jpmScenarios.add(jmiAddScenario);
        
        return jpmScenarios;
    }
    
    private JPopupMenu defineScenariosMenuPopup(DwDesktopNode dwScenarioNode) {
        JPopupMenu jpmScenarios = this.defineScenariosMenuPopup();
        
        if (dwScenarioNode.getId() != DEFAULT_SCENARIO_ID) {
            JMenuItem jmUpdate = new JMenuItem(properties.getLabelRename());
            jmUpdate.setActionCommand(ACTION_UPT_SCENARIO + String.valueOf(dwScenarioNode.getScenarioId()));
            jmUpdate.addActionListener(this);
            jpmScenarios.add(jmUpdate);
            
            JMenuItem jmiDelScenario = new JMenuItem(properties.getLabelDelScenario());
            jmiDelScenario.setActionCommand(ACTION_DEL_SCENARIO + String.valueOf(dwScenarioNode.getScenarioId()));
            jmiDelScenario.addActionListener(this);
            jpmScenarios.add(jmiDelScenario);
        }
        
        return jpmScenarios;
    }
    
    public void createNewFolder() {
        DwDesktopNode desktopNewFolder = null;
        String strFolderName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelNewFolder(), properties.getLabelAddingFolder(), JOptionPane.QUESTION_MESSAGE);
        if (strFolderName != null && !strFolderName.trim().equals("")) {
            if (this.activeNode == null) {
                desktopNewFolder = this.createScenarioNode(strFolderName, DwDesktopNode.dwDESKTOP_FOLDER);
            } else {
                desktopNewFolder = this.createScenarioNode(strFolderName, DwDesktopNode.dwDESKTOP_FOLDER, this.activeNode);
            }
            
            this.getDwRootPanel().add(this.buildFolderIcone(desktopNewFolder));
            this.updateUI();
        }
    }
    
    protected void copyFolder(DwDesktopNode dwDesktopCopyFolder) {
        DwDesktopNode dwDesktopNewFolder;
        if (this.activeNode == null) {
            dwDesktopNewFolder = this.createScenarioNode(dwDesktopCopyFolder.getName(), DwDesktopNode.dwDESKTOP_FOLDER);
        } else {
            dwDesktopNewFolder = this.createScenarioNode(dwDesktopCopyFolder.getName(), DwDesktopNode.dwDESKTOP_FOLDER, this.activeNode);
        }
        
        this.getDwRootPanel().add(this.buildFolderIcone(dwDesktopNewFolder));
        
        this.copyFolderContents(dwDesktopNewFolder, dwDesktopCopyFolder);
        
        this.updateUI();
    }
    
    protected void copyFolderContents(DwDesktopNode desktopMainFolder, DwDesktopNode desktopFolder) {
        for (DwDesktopNode desktopNode : desktopFolder.getNodes()) {
            if (desktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
                desktopFolder = this.createScenarioNode(desktopNode.getName(), DwDesktopNode.dwDESKTOP_FOLDER, desktopMainFolder.getId(), desktopMainFolder);
                
                desktopFolder.setDescricao("");
                desktopFolder.setRootID(desktopMainFolder.getId());
                
                this.desktopListener.createNodeDB(desktopFolder);
                this.copyFolderContents(desktopFolder, desktopNode);
            } else {
                desktopNode.setRootID(desktopMainFolder.getId());
                if (this.canAddNode(desktopNode)) {
                    this.desktopListener.addNodeToNode(desktopNode, desktopMainFolder);
                    desktopMainFolder.addNode(desktopNode);
                    this.desktopListener.createNodeDB(desktopNode);
                }
            }
        }
    }
    
    public void updateScenario(DwDesktopNode nodeFolder) {
        String strFolderName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelRenaming(), nodeFolder.getName());
        if (strFolderName != null && !strFolderName.trim().equals("")) {
            for (int i = 0; i < this.jpnlScenarios.getComponents().length; i++) {
                if (((JButton) this.jpnlScenarios.getComponents()[i]).getText().equals(nodeFolder.getName())) {
                    ((JButton) this.jpnlScenarios.getComponents()[i]).setText(strFolderName);
                    break;
                }
            }
            
            this.updateDesktopNode(nodeFolder, strFolderName);
        }
    }
    
    public void updateFolder(DwDesktopNode nodeFolder) {
        String strFolderName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelRenaming(), nodeFolder.getName());
        this.updateDesktopNode(nodeFolder, strFolderName);
    }
    
    public void updateDesktopNode(DwDesktopNode nodeFolder, String strFolderName) {
        if (strFolderName != null && !strFolderName.trim().equals("")) {
            nodeFolder.setName(strFolderName);
            this.renameScenarioNode(nodeFolder);
        }
        this.updateUI();
        this.refreshCurrent();
    }
    
    public JComponent buildFolderIcone(final DwDesktopNode node) {
        final DwIcone icone = new DwIcone(m_imageListDesktop[IMG_IC_DESKTOP_32_PASTA],
                            node.getName(), "", node, this, BACKGROUND_COLOR, this);
        icone.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getButton() != MouseEvent.BUTTON1 && me.isPopupTrigger()) {
                    JPopupMenu jpmFolder = buildFolderPopup(node);
                    jpmFolder.show(icone, me.getX(), me.getY()-45);
                } else if (me.getButton() == MouseEvent.BUTTON1 && me.getClickCount() == 2) {
                    nextFolder(node);
                }
            }
            
            public void mouseReleased(MouseEvent me) {
                if (me.getButton() != MouseEvent.BUTTON1 && me.isPopupTrigger()) {
                    JPopupMenu jpmFolder = buildFolderPopup(node);
                    jpmFolder.show(icone, me.getX(), me.getY()-45);
                }
            }
        });
        
        return icone;
    }
    
    public JComponent buildFolderIcone(String strFolderName) {
        final DwDesktopNode nodeFolder = new DwDesktopNode();
        nodeFolder.setType(DwDesktopNode.dwDESKTOP_FOLDER);
        nodeFolder.setScenarioId(this.getScenario().getId());
        nodeFolder.setName(strFolderName);
        
        return this.buildFolderIcone(nodeFolder);
    }
    
    protected JPopupMenu buildFolderPopup(final DwDesktopNode node) {
        ActionListener listenerFolder = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals(ACTION_FOLDER_OPEN)) {
                    nextFolder(node);
                } else if (e.getActionCommand().equals(ACTION_FOLDER_UPDATE)) {
                    updateFolder(node);
                } else if (e.getActionCommand().equals(ACTION_FOLDER_DELETE)) {
                    notifyDropEnd(node);
                }
            }
        };
        
        JPopupMenu jpmFolder = new JPopupMenu();
        JMenuItem jmUpdate = new JMenuItem(properties.getLabelRename());
        jmUpdate.setActionCommand(ACTION_FOLDER_UPDATE);
        jmUpdate.addActionListener(listenerFolder);
        jpmFolder.add(jmUpdate);
        
        return jpmFolder;
    }
    
    public void buildScenarioPath() {
        jlblPath.setText(this.getScenario().getName() + "/");
    }
    
    public void buildFolderPath() {
        List<DwDesktopNode> node = this.searchNodeParent(this.getScenario(), this.activeNode, true);
        this.buildFolderPath(node);
    }
    
    public void buildFolderPath(List<DwDesktopNode> nodePath) {
        StringBuilder sbPath = new StringBuilder();
        for (int i = nodePath.size()-1; i > -1; i--) {
            sbPath.append(nodePath.get(i) + "/");
        }
        jlblPath.setText(this.getScenario().getName() + "/" + sbPath.toString());
    }
    
    public void backFolder() {
        List<DwDesktopNode> node = this.searchNodeParent(this.getScenario(), this.activeNode, true);
        this.buildFolderPath(node);
        
        if (node != null && node.size() > 1) {
            this.activeFolder(node.get(1));
        } else {
            this.activeScenario(this.getScenario().getId());
        }
    }
    
    public List<DwDesktopNode> searchNodeParent(DwDesktopNode nodeBase, DwDesktopNode nodeCompare) {
        return this.searchNodeParent(nodeBase, nodeCompare, false);
    }
    
    public List<DwDesktopNode> searchNodeParent(DwDesktopNode nodeBase, DwDesktopNode nodeCompare, boolean blnAddIntermediate) {
        List<DwDesktopNode> lstNodeReturn = new ArrayList<DwDesktopNode>();
        List<DwDesktopNode> lstNodeAux;
        DwDesktopNode nodeAux;
        
        if (nodeBase.getNodes().size() > 0) {
            for (DwDesktopNode node : nodeBase.getNodes()) {
                if (node.getType() == nodeCompare.getType()) {
                    if (node.equals(nodeCompare)) {
                        lstNodeReturn.add(node);
                        break;
                    } else {
                        lstNodeAux = this.searchNodeParent(node, nodeCompare, blnAddIntermediate);
                        if (lstNodeAux != null && lstNodeAux.size() > 0 && blnAddIntermediate) {
                            lstNodeReturn.addAll(lstNodeAux);
                            lstNodeReturn.add(node);
                        }
                    }
                }
            }
        }
        
        return lstNodeReturn;
    }
    
    public void nextFolder(DwDesktopNode nodeFolder) {
        this.activeFolder(nodeFolder);
        //TODO - setar focus
    }
    
    public void execQuery(DwIcone icone) {
        DwDesktopNode iconeNode = icone.getDesktopNode();
        
        StringBuilder sbURL = new StringBuilder();
        
        sbURL.append("w_sigadw.apw?action=");
        if (iconeNode.isMaintAllow()) {
            sbURL.append("queryOnlineExec");
        } else {
            sbURL.append("queryExec");
        }
        
        sbURL.append("&id=" + icone.getDesktopNode().getId() + "&oper=6&headers=0&type=");
        if (iconeNode.getType() == DwDesktopNode.dwDESKTOP_TABLE) {
            sbURL.append("1");
        } else {
            sbURL.append("2");
        }
        
        sbURL.append("&source=desktop&_forceReload=");
        sbURL.append(System.currentTimeMillis());
        
        PVURL.open(this, sbURL.toString(), null);
    }
    
    /**
     * Limpa, adiciona icones do mtodo addIconesDefault e ns filhos no desktop
     */
    public void updateDesktop() {
        this.updateDesktop(null);
    }
    
    public void updateDesktop(DwDesktopNode nodeSelected) {
        if (nodeSelected == null) {
            this.activeScenario();
        } else if (nodeSelected.getType() == DwDesktopNode.dwDESKTOP_FOLDER) {
            this.activeFolder(nodeSelected);
        } else if (nodeSelected.getType() == DwDesktopNode.dwDESKTOP_SCENARIO) {
            this.activeScenario(nodeSelected);
        }
    }
    
    public void updateFromDB(DwDesktopNode dwDesktopNode, DwDesktopNode dwDesktopNodeParent) {
        dwDesktopNodeParent.addNode(dwDesktopNode);
    }
    
    public void updateFromDB(DwDesktopNode dwDesktopNode, int intParentID) {
        this.updateFromDB(dwDesktopNode, this.getScenario(intParentID));
    }
    
    public void actionPerformed(ActionEvent e) {
        JMenuItem jmi = (JMenuItem) e.getSource();
        String Aux;
        
        if (e.getActionCommand().equals(ACTION_ADD_SCENARIO)) {
            String strScenario = JOptionPane.showInputDialog(jmi, properties.getLabelNewScenario(), properties.getLabelAddScenario(), JOptionPane.QUESTION_MESSAGE);
            if (strScenario != null && !strScenario.trim().equals("")) {
                this.createScenario(strScenario);
            }
        } else if (e.getActionCommand().contains(ACTION_UPT_SCENARIO)) {
            int intId = Integer.parseInt(e.getActionCommand().split(ACTION_UPT_SCENARIO)[1]);
            this.updateScenario(this.getScenario(intId));
        } else if (e.getActionCommand().contains(ACTION_DEL_SCENARIO)) {
            int intId = Integer.parseInt(e.getActionCommand().split(ACTION_DEL_SCENARIO)[1]);
            if (JOptionPane.showConfirmDialog(jmi, properties.getLabelAskDelScenario() + " " + this.getScenario(intId).getName(), properties.getLabelDelScenario(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                this.deleteScenario(intId);
            }
        } else if (e.getActionCommand().equals(ACTION_WALLPAPER)) {
            String strImageURL = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelUrlWallpaper(), this.properties.getBackgroundURL());
            if (strImageURL != null) {
                if (!strImageURL.equals("")) {
                    Aux = this.defineWallpaper(strImageURL);
                    this.properties.setBackgroundImage( this.desktopListener.getImageResource(Aux) );
                } else {
                    this.removeWallpaper(this.properties.getBackgroundURL());
                    this.properties.setBackgroundImage(null);
                }
                
                this.properties.setBackgroundURL(strImageURL);
                
                this.updateUI();
            }
        }
    }
    
    public String defineWallpaper(String strImageURL) {
        DwDesktopNode nodeWallpapper = new DwDesktopNode();
        
        nodeWallpapper.setName(strImageURL);
        nodeWallpapper.setType(DwDesktopNode.dwDESKTOP_WALLPAPER);
        
        this.desktopListener.createNodeDB(nodeWallpapper);
        
        return nodeWallpapper.getName();
    }
    
    public void removeWallpaper(String strImageURL) {
        DwDesktopNode nodeWallpapper = new DwDesktopNode();
        
        nodeWallpapper.setName(strImageURL);
        nodeWallpapper.setType(DwDesktopNode.dwDESKTOP_WALLPAPER);
        
        this.desktopListener.deleteNodeDB(nodeWallpapper);
    }
    
    public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
        this.updateUI();
        return super.imageUpdate(img, infoflags, x, y, width, height);
    }
    
    public void mouseClicked(MouseEvent e) {
    }
    
    public void mousePressed(MouseEvent e) {
        if (e.isPopupTrigger()) {
            JPopupMenu jpm = defineScenariosMenuPopup();
            this.defineDesktopMenuPop(jpm);
            jpm.show(this, e.getX(), e.getY());
        }
    }
    
    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            JPopupMenu jpm = defineScenariosMenuPopup();
            this.defineDesktopMenuPop(jpm);
            jpm.show(this, e.getX(), e.getY());
        }
    }
    
    public void mouseEntered(MouseEvent e) {
    }
    
    public void mouseExited(MouseEvent e) {
    }
    
    protected void defineDesktopMenuPop(JPopupMenu jpm) {
        JMenuItem jmiWallpaper = new JMenuItem(properties.getLabelWallpaper());
        jmiWallpaper.setActionCommand(ACTION_WALLPAPER);
        jmiWallpaper.addActionListener(this);
        jpm.add(jmiWallpaper);
    }
    
    public void updateUI() {
        if (this.getDwRootPanel() != null) {
            this.getDwRootPanel().updateUI();
            this.getDwRootPanel().validate();
            this.jcpScenarios.updateUI();
            this.repaint();
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jcpScenarios;
    private javax.swing.JLabel jlblPath;
    private javax.swing.JLabel jlblScenario;
    private javax.swing.JLabel jlblScenarioTitle;
    private javax.swing.JPanel jpnlMain;
    private javax.swing.JPanel jpnlScenarioManagement;
    private javax.swing.JPanel jpnlScenarios;
    private javax.swing.JPanel jpnlTitle;
    private javax.swing.JScrollPane jscpWorkspace;
    private javax.swing.JSplitPane jsplMain;
    // End of variables declaration//GEN-END:variables
    
}