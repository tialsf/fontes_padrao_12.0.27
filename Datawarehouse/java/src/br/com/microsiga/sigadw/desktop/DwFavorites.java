package br.com.microsiga.sigadw.desktop;

import br.com.microsiga.sigadw.desktop.event.DwDesktopListener;
import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDropTargetListener;
import br.com.microsiga.sigadw.event.dragndrop.DwTransferable;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import pv.util.PVURL;

/**
 * Classe que representa o espao de trabalho de favoritos do SigaDW e os seus respectivos
 *  arquivos/consultas/arquivos
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 22 de Agosto de 2006
 */
public class DwFavorites extends javax.swing.JPanel implements DwDragDropEventListener<DwDesktopNode> {
    
    private DwDesktopListener desktopListener;
    private DwDesktopNode activeNode;
    
    protected final String ACTION_FOLDER_OPEN   = "openFolder";
    protected final String ACTION_FOLDER_UPDATE = "updateFolder";
    protected final String ACTION_FOLDER_DELETE = "deleteFolder";
    
    protected final String ACTION_FILE_UPDATE = "updateFile";
    
    private final static Color BACKGROUND_COLOR = new Color(255,255,204);
    
    private DesktopProperties properties = DesktopProperties.getInstance();

    /**
     * Creates new form DwFavorites
     */
    public DwFavorites() {
        initComponents();
        initialize();
    }
    
    /**
     * Creates new form DwFavorites
     */
    public DwFavorites(DwDesktopListener desktopListener) {
        this.desktopListener = desktopListener;
        initComponents();
        initialize();
    }
    
    protected void initialize() {
        DwDropTargetListener<DwDesktopNode> dwDDlistener = new DwDropTargetListener<DwDesktopNode>(this, DwTransferable.sigaDwQuery);
        new DropTarget(this, DnDConstants.ACTION_COPY, dwDDlistener, true);
        this.setDefaultIcones();
        this.activeFavorites();
        this.getDwRootPanel().setBackground(BACKGROUND_COLOR);
    }
    
    public JPanel getDwRootPanel() {
        return jpnlFavorites;
    }
    
    public boolean canAddNode(DwDesktopNode dwDesktopNode) {
        boolean blnCanAdd = false;
        
        if (this.activeNode == null && !this.getFavorites().getNodes().contains(dwDesktopNode)) {
            blnCanAdd = true;
        } else if (this.activeNode != null && !this.activeNode.getNodes().contains(dwDesktopNode)) {
            blnCanAdd = true;
        }
        
        return blnCanAdd;
    }
    
    public void notifyDragDrop(DwDesktopNode dwDesktopNode) {
        int intRootId = 0;
        
        if (this.activeNode != null) {
            intRootId = this.activeNode.getId();
        } else {
            intRootId = this.getFavorites().getId();
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FOLDER) {
            this.copyFolder(dwDesktopNode);
        } else {
            if (dwDesktopNode.haveTable()) {
                this.createTable(dwDesktopNode, intRootId);
            }

            if (dwDesktopNode.haveGraph()) {
                this.createGraph(dwDesktopNode, intRootId);
            }
        }
        
        this.updateUI();
    }
    
    public void handleDropAccept(Point point) {
        this.setBackground(properties.getAcceptDropColor());
    }
    
    public void handleDropReject(Point point) {
        this.setBackground(properties.getRejectDragColor());
    }

    public void handleDropExit() {
        this.setBackground(BACKGROUND_COLOR);
    }
    
    protected void createTable(DwDesktopNode dwDesktopNode, int intRootId) {
        this.createQuery(dwDesktopNode, intRootId, DwDesktopNode.dwDESKTOP_TABLE, "10");
    }
    
    protected void createGraph(DwDesktopNode dwDesktopNode, int intRootId) {
        this.createQuery(dwDesktopNode, intRootId, DwDesktopNode.dwDESKTOP_GRAPH, "01");
    }
    
    protected void createQuery(DwDesktopNode dwDesktopNode, int intRootId, int intQueryType, String strQryDisp) {
        DwDesktopNode node = dwDesktopNode.clone();
        node.setRootID(intRootId);
        node.setType(intQueryType);
        node.setConsDisp(strQryDisp);
        if (this.canAddNode(node)) {
            this.buildDwIcones(node);
            this.desktopListener.createNodeDB(node);
        }
    }
    
    public void notifyDropEnd() {
    }
    
    public void notifyDropEnd(DwDesktopNode dwDesktopNode) {
        DwIcone icone;
        JComponent jcomp;
        
        if (this.activeNode == null) {
            this.getFavorites().removeNode(dwDesktopNode);
        } else {
            List<DwDesktopNode> node = this.searchNodeParent(this.activeNode);
            if (node != null && node.size() > 0) {
                node.get(0).removeNode(dwDesktopNode);
            }
        }
        
        for (Component comp : this.jpnlFavorites.getComponents()) {
            if (comp instanceof JComponent) {
                jcomp = (JComponent) comp;
                if (jcomp != null && jcomp.getComponentCount() == 1
                        && jcomp.getComponent(0) instanceof DwIcone) {
                    icone = (DwIcone) jcomp.getComponent(0);
                    if (icone.getDesktopNode() != null && icone.getDesktopNode().equals(dwDesktopNode)) {
                        this.jpnlFavorites.remove(comp);
                    }
                } else {
                    icone = (DwIcone) comp;
                    if (icone.getDesktopNode() != null && icone.getDesktopNode().equals(dwDesktopNode)) {
                        this.jpnlFavorites.remove(comp);
                    }
                }
            }
        }
        
        this.desktopListener.removeNode(dwDesktopNode);
        this.updateUI();
        
        this.desktopListener.deleteNodeDB(dwDesktopNode);
    }
    
    protected void buildDwIcones(DwDesktopNode dwDesktopNode) {
        this.buildDwIcones(dwDesktopNode, true);
    }
    
    protected void buildDwIcones(DwDesktopNode dwDesktopNode, boolean blnAddNode) {
        if (dwDesktopNode == null) {
            throw new IllegalArgumentException("buildDwIcones: parâmetro dwDesktopNode null");
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_TABLE
                || (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_QUERY && dwDesktopNode.haveTable())) {
            DwDesktopNode nodeTable = dwDesktopNode.clone();
            nodeTable.setType(DwDesktopNode.dwDESKTOP_TABLE);
            
            final DwIcone icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_TABELA], 
                    dwDesktopNode.getDescricao(), dwDesktopNode.getName(), nodeTable, this, BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);

            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        execQuery(icone);
                    }
                }
            });
            
            if (blnAddNode) {
                // pesquisa pelo n pai ao qual ser inserido
                List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree();
                if (lstTreeNodes.size() > 0) {
                    this.desktopListener.addNodeToNode(nodeTable, lstTreeNodes.get(0));
                } else {
                    this.desktopListener.addNodeToNode(nodeTable, 1);
                }
                
                if (this.activeNode == null) {
                    this.getFavorites().getNodes().add(nodeTable);
                } else {
                    List<DwDesktopNode> node = this.searchNodeParent(this.activeNode);
                    if (node != null && node.size() > 0) {
                        node.get(0).addNode(nodeTable);
                    }
                }
            }
        }
        
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_GRAPH
                || (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_QUERY && dwDesktopNode.haveGraph())) {
            
            DwDesktopNode nodeGraph = dwDesktopNode.clone();
            nodeGraph.setType(DwDesktopNode.dwDESKTOP_GRAPH);
            
            final DwIcone icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_GRAFICO],
                    dwDesktopNode.getDescricao(), dwDesktopNode.getName(), nodeGraph, this, BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);

            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        execQuery(icone);
                    }
                }
            });
            
            if (blnAddNode) {
                List<DefaultMutableTreeNode> lstTreeNodes = this.searchNodeTree();
                if (lstTreeNodes.size() > 0) {
                    this.desktopListener.addNodeToNode(nodeGraph, lstTreeNodes.get(0));
                } else {
                    this.desktopListener.addNodeToNode(nodeGraph, 1);
                }
                
                if (this.activeNode == null) {
                    this.getFavorites().getNodes().add(nodeGraph);
                } else {
                    List<DwDesktopNode> node = this.searchNodeParent(this.activeNode);
                    if (node != null && node.size() > 0) {
                        node.get(0).addNode(nodeGraph);
                    }
                }
            }
        }
    }
    
    public void execQuery(DwIcone icone) {
        DwDesktopNode iconeNode = icone.getDesktopNode();
        
        StringBuilder sbURL = new StringBuilder();
        
        sbURL.append("w_sigadw.apw?action=");
        if (iconeNode.isMaintAllow()) {
            sbURL.append("queryOnlineExec");
        } else {
            sbURL.append("queryExec");
        }
        
        sbURL.append("&id=" + icone.getDesktopNode().getId() + "&oper=6&headers=0&type=");
        if (iconeNode.getType() == DwDesktopNode.dwDESKTOP_TABLE) {
            sbURL.append("1");
        } else {
            sbURL.append("2");
        }
        
        sbURL.append("&source=desktop&_forceReload=");
        sbURL.append(System.currentTimeMillis());
        
        PVURL.open(this, sbURL.toString(), null);
    }
    
    public void execFile(DwIcone icone) {
        String strName = icone.getDesktopNode().getName();
        String strExt  = strName.substring(strName.lastIndexOf("."));
        
        if (!strName.startsWith("http://")) {
            strName = "http://" + strName;
        }
        
        StringBuilder sbURL = new StringBuilder();
        sbURL.append("w_sigadw.apw?action=");
        sbURL.append("openURL");
        sbURL.append("&extensFile=");
        sbURL.append(strExt);
        
        try {
            sbURL.append("&url=");
            sbURL.append(URLEncoder.encode(strName, "ISO-8859-1"));
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        
        sbURL.append("&source=desktop&_forceReload=");
        sbURL.append(System.currentTimeMillis());
        
        PVURL.open(this, sbURL.toString(), null);
    }
    
    protected List<DefaultMutableTreeNode> searchNodeTree() {
        DwDesktopNode nodeScenarioSearch;
        
        // verifica se no tem nenhum n ativo
        if (this.activeNode == null) {
            nodeScenarioSearch = new DwDesktopNode();
            
            // define o tipo de n como o n principal (DESKTOP)
            nodeScenarioSearch.setType(DwDesktopNode.dwDESKTOP_FAVORITES);
        } else {
            nodeScenarioSearch = this.activeNode;
        }
        
        return this.desktopListener.searchNodeTree(nodeScenarioSearch);
    }
    
    public void setDefaultIcones() {
        DwIcone icone;
        
        if (this.activeNode != null) {
            icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_PASTAACIMA],
                        properties.getLabelFolder(), properties.getLabelUpper(), BACKGROUND_COLOR, this);
            this.getDwRootPanel().add(icone);
            icone.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(java.awt.event.MouseEvent evt) {
                    if (evt.getClickCount() == 2) {
                        backFolder();
                    }
                }
            });
        }
        
        icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_NOVAPASTA],
                        properties.getLabelNew(), properties.getLabelFolder(), BACKGROUND_COLOR, this);
        this.getDwRootPanel().add(icone);
        icone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    createNewFolder();
                }
            }
        });
        
        icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_NEWFILE],
                        properties.getLabelNewFile(), properties.getLabelFile(), BACKGROUND_COLOR, this);
        this.getDwRootPanel().add(icone);
        icone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    createNewFile();
                }
            }
        });
        this.updateUI();
    }
    
    public void activeFavorites() {
        this.activeNode = null;
        
        this.getDwRootPanel().removeAll();
        this.setDefaultIcones();
        this.refreshFavorite();
        this.updateUI();
        
        this.buildFavoritePath();
    }
    
    public void refreshFavorite() {
        for (DwDesktopNode dwDesktopNode : this.getFavorites().getNodes()) {
            if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
                this.getDwRootPanel().add(this.buildFolderIcone(dwDesktopNode));
                this.updateUI();
            } else if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
                this.buildDwIcones(dwDesktopNode, false);
            } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
                this.getDwRootPanel().add(this.buildLinkIcone(dwDesktopNode));
                this.updateUI();
            }
        }
    }
    
    public void buildFavoritePath() {
        jlblPath.setText(this.getFavorites().getName() + "/");
    }
    
    public void refreshCurrent() {
        if (this.activeNode != null) {
            this.activeFolder(this.activeNode);
        } else {
            this.activeFavorites();
        }
    }
    
    public void activeFolder(DwDesktopNode nodeFolder) {
        this.activeNode = nodeFolder;
        
        this.getDwRootPanel().removeAll();
        this.setDefaultIcones();
        this.refreshFolder(nodeFolder);
        this.refreshFiles(nodeFolder);
        this.updateUI();
        this.buildFolderPath();
    }
    
    public void refreshFolder(DwDesktopNode nodeFolder) {
        for (DwDesktopNode dwDesktopNode : nodeFolder.getNodes()) {
            if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
                this.buildDwIcones(dwDesktopNode, false);
            } else {
                this.getDwRootPanel().add(this.buildFolderIcone(dwDesktopNode));
                this.updateUI();
            }
        }
    }
    
        public void refreshFiles(DwDesktopNode nodeFolder) {
        for (DwDesktopNode dwDesktopNode : nodeFolder.getNodes()) {
              if (dwDesktopNode.getType() != DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
                this.buildDwIcones(dwDesktopNode, false);
            } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_LINK) {
                this.getDwRootPanel().add(this.buildLinkIcone(dwDesktopNode));
                this.updateUI();
            }
        }
    }
    
    public void nextFolder(DwDesktopNode nodeFolder) {
        this.activeFolder(nodeFolder);
        //TODO - setar focus
    }
    
    public void backFolder() {
        List<DwDesktopNode> node = this.searchNodeParent(this.activeNode);
        this.buildFolderPath(node);
        
        if (node != null && node.size() > 1) {
            this.activeFolder(node.get(1));
        } else {
            this.activeFavorites();
        }
    }

    protected void createNewFolder() {
        DwDesktopNode desktopNewFolder = null;
        
        String strFolderName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelNewFolder(), properties.getLabelAddingFolder(), JOptionPane.QUESTION_MESSAGE);
        
        if (strFolderName != null && !strFolderName.trim().equals("")) {
            if (this.activeNode == null) {
                desktopNewFolder = this.createFavoriteNode(strFolderName, DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER);
            } else {
                desktopNewFolder = this.createFavoriteNode(strFolderName, DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER, this.activeNode);
            }
            
            this.getDwRootPanel().add(this.buildFolderIcone(desktopNewFolder));
            this.updateUI();
            
            this.desktopListener.createNodeDB(desktopNewFolder);
        }
    }
    
    protected void copyFolder(DwDesktopNode dwDesktopCopyFolder) {
        DwDesktopNode dwDesktopNewFolder;
        if (this.activeNode == null) {
            dwDesktopNewFolder = this.createFavoriteNode(dwDesktopCopyFolder.getName(), DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER);
        } else {
            dwDesktopNewFolder = this.createFavoriteNode(dwDesktopCopyFolder.getName(), DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER, this.activeNode);
        }
        
        this.getDwRootPanel().add(this.buildFolderIcone(dwDesktopNewFolder));
        this.desktopListener.createNodeDB(dwDesktopNewFolder);
        
        this.copyFolderContents(dwDesktopNewFolder, dwDesktopCopyFolder);
        
        this.updateUI();
    }
    
    protected void copyFolderContents(DwDesktopNode desktopMainFolder, DwDesktopNode desktopFolder) {
        for (DwDesktopNode desktopNode : desktopFolder.getNodes()) {
            if (desktopNode.getType() == DwDesktopNode.dwDESKTOP_FOLDER) {
                desktopFolder = this.createFavoriteNode(desktopNode.getName(), DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER, desktopMainFolder.getId(), desktopMainFolder);
                
                desktopFolder.setDescricao("");
                desktopFolder.setRootID(desktopMainFolder.getId());
                
                this.desktopListener.createNodeDB(desktopFolder);
                this.copyFolderContents(desktopFolder, desktopNode);
            } else {
                desktopNode.setRootID(desktopMainFolder.getId());
                if (this.canAddNode(desktopNode)) {
                    this.desktopListener.addNodeToNode(desktopNode, desktopMainFolder);
                    desktopMainFolder.addNode(desktopNode);
                    this.desktopListener.createNodeDB(desktopNode);
                }
            }
        }
    }
    
    public void renameFolder(DwDesktopNode nodeFolder) {
        String strFolderName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelRenaming(), nodeFolder.getName());
        if (strFolderName != null && !strFolderName.trim().equals("")) {
            nodeFolder.setName(strFolderName);
            this.renameScenarioNode(nodeFolder);
        }
        this.updateUI();
        this.refreshCurrent();
    }
    
    public void updateNewFolder(DwDesktopNode dwDesktopNode, int intNodeParent) {
        DwDesktopNode nodeSearch = new DwDesktopNode();
        nodeSearch.setId(intNodeParent);
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_COMPARE);
        
        if (intNodeParent == -1) {
            addNodeToNode(dwDesktopNode);
        } else {
            List<DwDesktopNode> lstNodes = searchNodeParent(nodeSearch);
            if (lstNodes != null && lstNodes.size() > 0) {
                addNodeToNode(dwDesktopNode, lstNodes.get(0));
            }
        }
        this.activeFavorites();
    }
    
    public void createNewFile() {
        DwDesktopNode desktopNewFile = null;
        String strLinkName = JOptionPane.showInputDialog(getDwRootPanel(), properties.getLabelNewLink(), properties.getLabelAddingLink(), JOptionPane.QUESTION_MESSAGE);
        
        if (strLinkName != null && !strLinkName.trim().equals("")) {
            if (this.activeNode == null) {
                desktopNewFile = this.createFavoriteNode(strLinkName, DwDesktopNode.dwDESKTOP_FAVORITES_LINK);
            } else {
                desktopNewFile = this.createFavoriteNode(strLinkName, DwDesktopNode.dwDESKTOP_FAVORITES_LINK, this.activeNode);
            }
            desktopNewFile.setImgCloseNode(DwDesktop.IMG_IC_TREE_FILE);
            desktopNewFile.setImgOpenNode(DwDesktop.IMG_IC_TREE_FILE);
            
            this.getDwRootPanel().add(this.buildLinkIcone(desktopNewFile));
            this.updateUI();
            
            this.desktopListener.createNodeDB(desktopNewFile);
        }
    }
    
    public void updateNewFile(DwDesktopNode dwDesktopNode, int intNodeParent) {
        DwDesktopNode nodeSearch = new DwDesktopNode();
        nodeSearch.setId(intNodeParent);
        nodeSearch.setType(DwDesktopNode.dwDESKTOP_COMPARE);
        
        if (intNodeParent == -1) {
            addNodeToNode(dwDesktopNode);
        } else {
            List<DwDesktopNode> lstNodes = searchNodeParent(nodeSearch);
            if (lstNodes != null && lstNodes.size() > 0) {
                addNodeToNode(dwDesktopNode, lstNodes.get(0));
            }
        }
        this.activeFavorites();
    }
    
    public DwDesktopNode createFavoriteNode(String strNodeName, int intNodeType) {
        DwDesktopNode nodeSearch = this.getFavorites();
        
        return this.createFavoriteNode(strNodeName, intNodeType, nodeSearch);
    }
    
    public DwDesktopNode createFavoriteNode(String strNodeName, int intNodeType, DwDesktopNode nodeParent) {
        int intRootId = -1;
        
        if (this.activeNode != null) {
            intRootId = this.activeNode.getId();
        } else {
            intRootId = this.getFavorites().getId();
        }
        
        return this.createFavoriteNode(strNodeName, intNodeType, intRootId, nodeParent);
    }
    
    public DwDesktopNode createFavoriteNode(String strNodeName, int intNodeType, int intRootId, DwDesktopNode nodeParent) {
        DwDesktopNode node = new DwDesktopNode();
        node.setId(0);
        node.setRootID(intRootId);
        node.setName(strNodeName);
        node.setType(intNodeType);
        node.setImgCloseNode(DwDesktop.IMG_IC_TREE_PASTAFECHADA);
        node.setImgOpenNode(DwDesktop.IMG_IC_TREE_PASTAABERTA);
        node.setNodeEnabled(false);
        
        addNodeToNode(node, nodeParent);
        
        return node;
    }
    
    public void renameScenarioNode(DwDesktopNode node) {
        this.desktopListener.updateNode(node);
        this.desktopListener.updateNodeDB(node);
    }
    
    public void addNodeToNode(DwDesktopNode node) {
        this.addNodeToNode(node, this.getFavorites());
    }
    
    public void addNodeToNode(DwDesktopNode node, DwDesktopNode nodeParent) {
        if (this.activeNode == null) {
            node.setRootID(-1);
            nodeParent.addNode(node);
        } else {
            node.setRootID(this.activeNode.getId());
            this.activeNode.addNode(node);
        }
        
        this.desktopListener.addNodeToNode(node, nodeParent);
    }
    
    public JComponent buildFolderIcone(final DwDesktopNode node) {
        final DwIcone icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_PASTA],
                                    node.getName(),  "", node, this, BACKGROUND_COLOR, this);
        icone.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getButton() != MouseEvent.BUTTON1 && me.isPopupTrigger()) {
                    JPopupMenu jpmFolder = buildFolderPopup(node);
                    jpmFolder.show(icone, me.getX(), me.getY()-45);
                } else if (me.getButton() == MouseEvent.BUTTON1 && me.getClickCount() == 2) {
                    nextFolder(node);
                }
            }
            
            public void mouseReleased(MouseEvent me) {
                if (me.getButton() != MouseEvent.BUTTON1 && me.isPopupTrigger()) {
                    JPopupMenu jpmFolder = buildFolderPopup(node);
                    jpmFolder.show(icone, me.getX(), me.getY()-45);
                }
            }
        });
        
        return icone;
    }
    
    protected JPopupMenu buildFolderPopup(final DwDesktopNode node) {
        ActionListener listenerFolder = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals(ACTION_FOLDER_OPEN)) {
                    nextFolder(node);
                } else if (e.getActionCommand().equals(ACTION_FOLDER_UPDATE)) {
                    renameFolder(node);
                } else if (e.getActionCommand().equals(ACTION_FOLDER_DELETE)) {
                    notifyDropEnd(node);
                }
            }
        };
        
        JPopupMenu jpmFolder = new JPopupMenu();
        
        JMenuItem jmUpdate = new JMenuItem(properties.getLabelRename());
        jmUpdate.setActionCommand(ACTION_FOLDER_UPDATE);
        jmUpdate.addActionListener(listenerFolder);
        jpmFolder.add(jmUpdate);
        
        return jpmFolder;
    }
    
    protected JPopupMenu buildFilePopup(final DwDesktopNode node) {
        ActionListener listenerFolder = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals(ACTION_FILE_UPDATE)) {
                    renameFolder(node);
                }
            }
        };
        
        JPopupMenu jpmFile = new JPopupMenu();
        
        JMenuItem jmUpdate = new JMenuItem(properties.getLabelRename());
        jmUpdate.setActionCommand(ACTION_FILE_UPDATE);
        jmUpdate.addActionListener(listenerFolder);
        jpmFile.add(jmUpdate);
        
        return jpmFile;
    }
    
    public JComponent buildLinkIcone(final DwDesktopNode node) {
        final DwIcone icone = new DwIcone(DwDesktop.m_imageListDesktop[DwDesktop.IMG_IC_DESKTOP_32_FILE],
                                    node.getName(), "", node, this, BACKGROUND_COLOR, this);
        icone.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getButton() == MouseEvent.BUTTON3 && me.isPopupTrigger()) {
                    JPopupMenu jpmFolder = buildFilePopup(node);
                    jpmFolder.show(icone, me.getX(), me.getY()-45);
                } else if (me.getClickCount() == 2) {
                    execFile(icone);
                }
            }
        });
        
        return icone;
    }
    
    public JComponent buildFolderIcone(String strFolderName) {
        final DwDesktopNode nodeFolder = new DwDesktopNode();
        nodeFolder.setType(DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER);
        nodeFolder.setName(strFolderName);
        
        return this.buildFolderIcone(nodeFolder);
    }
    
    public void buildFolderPath() {
        List<DwDesktopNode> node = this.searchNodeParent(this.activeNode);
        this.buildFolderPath(node);
    }
    
    public void buildFolderPath(List<DwDesktopNode> nodePath) {
        StringBuilder sbPath = new StringBuilder();
        for (int i = nodePath.size()-1; i > -1; i--) {
            sbPath.append(nodePath.get(i) + "/");
        }
        jlblPath.setText(this.getFavorites().getName() + "/" + sbPath.toString());
    }
    
    public DwDesktopNode getFavorites() {
        DwDesktopNode nodeFavorites = new DwDesktopNode();
        nodeFavorites.setType(DwDesktopNode.dwDESKTOP_FAVORITES);
        
        List<DwDesktopNode> lstNodes = this.desktopListener.searchDesktopNode(nodeFavorites);
        if (lstNodes != null && lstNodes.size() > 0) {
            nodeFavorites = lstNodes.get(0);
        }
        
        return nodeFavorites;
    }
    
    public List<DwDesktopNode> searchNodeParent(DwDesktopNode nodeCompare) {
        List<DwDesktopNode> nodeReturn = this.searchNodeParent(this.getFavorites(), nodeCompare);
        
        return nodeReturn;
    }
    
    public List<DwDesktopNode> searchNodeParent(DwDesktopNode nodeBase, DwDesktopNode nodeCompare) {
        List<DwDesktopNode> lstNodeReturn = new ArrayList<DwDesktopNode>();
        List<DwDesktopNode> lstNodeAux;
        DwDesktopNode nodeAux;
        
        if (nodeBase.getNodes().size() > 0) {
            for (DwDesktopNode node : nodeBase.getNodes()) {
                if (node.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
                    if (node.equals(nodeCompare)) {
                        lstNodeReturn.add(node);
                        break;
                    } else {
                        lstNodeAux = this.searchNodeParent(node, nodeCompare);
                        if (lstNodeAux != null && lstNodeAux.size() > 0) {
                            lstNodeReturn.addAll(lstNodeAux);
                            lstNodeReturn.add(node);
                        }
                    }
                }
            }
        }
        
        return lstNodeReturn;
    }
    
    public void updateUI() {
        if (this.jpnlFavorites != null) {
            this.jpnlFavorites.updateUI();
            this.jscpFavorites.updateUI();
            this.jpnlFavorites.validate();
            this.jscpFavorites.validate();
            this.repaint();
        }
    }
    
    public void updateFavorites(DwDesktopNode dwDesktopNode) {
        if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES_FOLDER) {
            this.activeFolder(dwDesktopNode);
        } else if (dwDesktopNode.getType() == DwDesktopNode.dwDESKTOP_FAVORITES) {
            this.activeFavorites();
        }
    }
    
    public void updateFromDB(DwDesktopNode dwDesktopNode, DwDesktopNode dwDesktopNodeParent) {
        dwDesktopNodeParent.addNode(dwDesktopNode);
    }
    
    public void updateFromDB(DwDesktopNode dwDesktopNode, int intParentID) {
        DwDesktopNode nodeSearch = dwDesktopNode.clone();
        nodeSearch.setId(intParentID);
        
        this.updateFromDB(dwDesktopNode, nodeSearch);
    }
    
    public void setBackground(Color bgColor) {
        super.setBackground(bgColor);
        
        if (this.getDwRootPanel() != null) {
            this.getDwRootPanel().setBackground(bgColor);
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jscpFavorites = new javax.swing.JScrollPane();
        jpnlFavorites = new javax.swing.JPanel();
        jpnlFavoritesTitle = new javax.swing.JPanel();
        jlblFavorites = new javax.swing.JLabel();
        jlblFavorites.setText(DesktopProperties.getInstance().getLabelMyFavorites());
        jlblPath = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setAlignmentX(0.0F);
        setAlignmentY(0.0F);
        setDoubleBuffered(false);
        setLayout(new java.awt.BorderLayout());

        jscpFavorites.setBorder(null);
        jscpFavorites.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jpnlFavorites.setMinimumSize(new java.awt.Dimension(10, 15));
        jpnlFavorites.setLayout(new java.awt.GridLayout(0, 8));
        jscpFavorites.setViewportView(jpnlFavorites);

        add(jscpFavorites, java.awt.BorderLayout.CENTER);

        jpnlFavoritesTitle.setLayout(new java.awt.BorderLayout());

        jlblFavorites.setBackground(new java.awt.Color(123, 160, 202));
        jlblFavorites.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jlblFavorites.setForeground(new java.awt.Color(255, 255, 255));
        jlblFavorites.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jlblFavorites.setOpaque(true);
        jlblFavorites.setPreferredSize(new java.awt.Dimension(59, 25));
        jpnlFavoritesTitle.add(jlblFavorites, java.awt.BorderLayout.NORTH);

        jlblPath.setFont(new java.awt.Font("Tahoma", 0, 10));
        jlblPath.setForeground(new java.awt.Color(22, 48, 125));
        jlblPath.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jlblPath.setOpaque(true);
        jlblPath.setPreferredSize(new java.awt.Dimension(16, 16));
        jpnlFavoritesTitle.add(jlblPath, java.awt.BorderLayout.CENTER);

        add(jpnlFavoritesTitle, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jlblFavorites;
    private javax.swing.JLabel jlblPath;
    private javax.swing.JPanel jpnlFavorites;
    private javax.swing.JPanel jpnlFavoritesTitle;
    private javax.swing.JScrollPane jscpFavorites;
    // End of variables declaration//GEN-END:variables
    
}