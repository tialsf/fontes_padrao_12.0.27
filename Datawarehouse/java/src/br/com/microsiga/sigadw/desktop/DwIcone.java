package br.com.microsiga.sigadw.desktop;

import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import br.com.microsiga.sigadw.event.dragndrop.DwDragDropEventListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragGestureListener;
import br.com.microsiga.sigadw.event.dragndrop.DwDragSourceListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * Classe que representa um �cone gen�rico do SigaDW, podendo ser a representa��o
 *  de pasta (nova pasta, pasta acima), arquivos, consultas
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R3 - atualizado e remodelado para o R4
 */
public class DwIcone extends JComponent implements MouseListener, DwDragDropEventListener<DwDesktopNode> {   
    private Dimension m_prefSize = new Dimension(80, 150);
    private ImageIcon m_image;
    private String m_label1, m_label2;
    private int m_x = 0;
    private int m_y = 0;
    private Rectangle iconRect = new Rectangle();
    private Rectangle clipRect = new Rectangle();
    
    private DragSource dragSource;
    private DwDragSourceListener dragSourceListener;
    private DwDragGestureListener dragGestureListener;

    private DwDragDropEventListener<DwDesktopNode> dragDropListener;
    private DwDesktopNode desktopNode;

    private boolean blnIconeSelected;

    private static DwIcone dwIconeSelected;
    private JComponent dwIconeOwner;

    private Color bgColor;
    
    public DwIcone(Image image, String label1, String label2, Color clrBgColor, JComponent owner) {
        this(image, label1, label2, null, null, clrBgColor, owner);
    }
    
    public DwIcone(Image image, String label1, String label2, DwDesktopNode desktopNode, DwDragDropEventListener<DwDesktopNode> dragDropListener, Color clrBgColor, JComponent owner) {
        this.m_image = new ImageIcon(image);
        this.m_label1       = label1;
        this.m_label2       = label2;
        this.bgColor        = clrBgColor;
        this.dwIconeOwner   = owner;
        
        if (desktopNode != null) {
            this.desktopNode = desktopNode;
        }
        
        if (dragDropListener != null) {
            this.dragDropListener = dragDropListener;
            
            this.dragSource = DragSource.getDefaultDragSource();
            this.dragSourceListener = new DwDragSourceListener();
            this.dragGestureListener = new DwDragGestureListener(this.dragSourceListener);
            
            this.dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this.dragGestureListener);
        }
        
        this.setBackground(this.bgColor);
        this.defineToolTip();
        this.addMouseListener(this);
    }
    
    public Dimension getPreferredSize() {
        if (m_image != null) {
            Insets insets = getInsets();
            return new Dimension(m_image.getIconWidth() + insets.left + insets.right + 50,
                                 m_image.getIconHeight()  + insets.top + insets.bottom + 50);
        } else {
            return new Dimension(100, 100);
        }
    }
    
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
    
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
    
    protected void paintComponent(Graphics g) {
        int x, y;
        
        if (isOpaque()) { //paint background
            g.setColor(getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
        }

        if (m_image != null) {
            Insets insets = getInsets();
            int iconWidth = m_image.getIconWidth();
            int iconX = getWidth() - insets.right - iconWidth;
            int iconY = insets.top;
            boolean faded = false;

            Graphics2D g2d = (Graphics2D)g.create();
            
            g.getClipBounds(clipRect);
            iconRect.setBounds(iconX, iconY, (m_prefSize.width - m_image.getIconWidth())/2, 5);
            
            g2d.drawImage(m_image.getImage(), (m_prefSize.width - m_image.getIconWidth())/2 + 5, 5, this);
            
            String[] dwIconLabels = showTitle(m_label1, g2d.getFontMetrics(), m_prefSize.width);
            
            x = positionTitle(dwIconLabels[0], g2d.getFontMetrics(), m_prefSize.width);
            y = m_image.getIconHeight() + 20;
            
            if (this.blnIconeSelected && this.dwIconeSelected == this) {
                g2d.setColor(new Color(49, 106, 197));
                g2d.fillRect(0, y-13, m_prefSize.width+15, 20);
                g2d.setColor(Color.WHITE);
            }
            
            g2d.drawString(" " + dwIconLabels[0], x, y);
            
            if (dwIconLabels.length == 2) {
                x = positionTitle(dwIconLabels[dwIconLabels.length-1], g2d.getFontMetrics(),m_prefSize.width);
                y = m_image.getIconHeight()+35;
                
                if (this.blnIconeSelected && this.dwIconeSelected == this) {
                    g2d.setColor(new Color(49, 106, 197));
                    g2d.fillRect(0, y-13, m_prefSize.width+15, 20);
                    g2d.setColor(Color.WHITE);
                }
                
                g2d.drawString(dwIconLabels[dwIconLabels.length-1], x, y);
            } else if (dwIconLabels.length > 2) {
                for (int i = 1; i < dwIconLabels.length; i++) {
                    x = positionTitle(dwIconLabels[i], g2d.getFontMetrics(),m_prefSize.width);
                    y = m_image.getIconHeight()+(20 + (i*15));
                    
                    if (this.blnIconeSelected && this.dwIconeSelected == this) {
                        g2d.setColor(new Color(49, 106, 197));
                        g2d.fillRect(0, y-13, m_prefSize.width+15, 20);
                        g2d.setColor(Color.WHITE);
                    }
                    
                    g2d.drawString(dwIconLabels[i], x, y);
                }
            } else {
                dwIconLabels = showTitle(m_label2, g2d.getFontMetrics(), m_prefSize.width);
                
                x = positionTitle(dwIconLabels[0], g2d.getFontMetrics(), m_prefSize.width);
                y = m_image.getIconHeight() + 35;
                
                if (!dwIconLabels[0].equals("") && this.blnIconeSelected && this.dwIconeSelected == this) {
                    g2d.setColor(new Color(49, 106, 197));
                    g2d.fillRect(0, y-13, m_prefSize.width+15, 20);
                    g2d.setColor(Color.WHITE);
                }
                
                g2d.drawString(" " + dwIconLabels[0], x, y);
            }
            
            g2d.dispose(); //clean up
        }
        
    }
    
    public String[] showTitle(String txt, FontMetrics fm, int x) {
        List<String> lstTitleRows = new ArrayList<String>();
        String strAux = "";
        int intMaxAscent = fm.getMaxAscent();
        
        if (this.blnIconeSelected && this.dwIconeSelected == this) {
            if (intMaxAscent > txt.length()) {
                strAux = txt;
                txt = "";
                lstTitleRows.add(strAux);
            } else {
                String[] starWords = txt.split(" ");
                boolean blnFullLine;
                for (int i = 0; i < starWords.length; i++, blnFullLine = false) {
                    if (intMaxAscent > (strAux.length() + starWords[i].trim().length())) {
                        strAux += " " + starWords[i];
                        strAux = strAux.trim();
                    } else {
                        if (strAux.trim() != "") {
                            lstTitleRows.add(strAux);
                        }
                        
                        if (starWords[i].trim().length() > intMaxAscent) {
                            strAux = starWords[i];
                            lstTitleRows.add(strAux.substring(0, intMaxAscent));
                            strAux = strAux.substring(intMaxAscent);
                        } else {
                            strAux = starWords[i];    
                        }
                    }
                }
                lstTitleRows.add(strAux);
            }
        } else {
            int intSize;
            boolean blnFullVisible = (Double.valueOf(txt.length()) / intMaxAscent) < 2.0;
            
            if ((Double.valueOf(txt.length()) / intMaxAscent) <= 1.0) {
                lstTitleRows.add(txt);
            } else {
                lstTitleRows.add(txt.substring(0, intMaxAscent) + ((blnFullVisible) ? "" : "..."));
                
                if (blnFullVisible) {
                    txt = txt.substring(intMaxAscent);
                    intSize = 0;
                } else {
                    intSize = intMaxAscent - txt.length() - 1;
                    if (intSize < 0) {
                        intSize *= -1;
                    }
                }
                
                lstTitleRows.add(((blnFullVisible) ? "" : "...") + txt.substring(intSize));
            }
        }
        
        return lstTitleRows.toArray(new String[]{});
    }
    
    public int positionTitle(String txt, FontMetrics fm, int x) {
        int ret = 0;
        if (fm.stringWidth(txt) < x)
            ret = (x - fm.stringWidth(txt)+8) / 2;
        return(ret);
    }
    
    public void notifyDragDrop(DwDesktopNode dwObject) {
        
    }

    public void notifyDropEnd() {
        this.dragDropListener.notifyDropEnd(this.getDesktopNode());
    }

    public void notifyDropEnd(DwDesktopNode dwObject) {
    }

    public void handleDropAccept(Point point) {
        this.setBackground(DesktopProperties.getInstance().getAcceptDropColor());
    }
    
    public void handleDropReject(Point point) {
        this.setBackground(DesktopProperties.getInstance().getRejectDragColor());
    }

    public void handleDropExit() {
        this.setBackground(this.bgColor);
    }
    
    public DwDesktopNode getDesktopNode() {
        return desktopNode;
    }

    public void setDesktopNode(DwDesktopNode desktopNode) {
        this.desktopNode = desktopNode;
    }

    protected void defineToolTip() {
        StringBuilder strToolTip = new StringBuilder();
        strToolTip.append(m_label1);
        
        if (!m_label2.trim().equals("")) {
            strToolTip.append(" (" + m_label2 + ")");
        }
        
        if (this.desktopNode != null) {
            if (this.desktopNode.haveTable()) {
                strToolTip.append(" [");
                strToolTip.append(DesktopProperties.getInstance().getLabelTable());
                strToolTip.append("]");
            } else if (this.desktopNode.haveGraph()) {
                strToolTip.append(" [");
                strToolTip.append(DesktopProperties.getInstance().getLabelGraph());
                strToolTip.append("]");
            }
        }
        
        this.setToolTipText(strToolTip.toString());
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        this.blnIconeSelected = true;
        this.dwIconeSelected = this;
        this.dwIconeOwner.updateUI();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
    
}