package br.com.microsiga.sigadw.desktop;

import br.com.microsiga.sigadw.desktop.to.DesktopProperties;
import java.awt.Graphics;

/**
 * Respons�vel por representar um cen�rio espec�fico para o SigaDW e exibir o
 *  conte�do para o cen�rio.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Setembro de 2006, 09:27
 */
public class DwScenario extends javax.swing.JPanel {
    
    private DesktopProperties properties = DesktopProperties.getInstance();
    
    /** Creates new form DwScenario */
    public DwScenario() {
        initComponents();
    }
    
    public void paint(Graphics graphic) {
        super.paint(graphic);
        
        if (this.properties.getBackgroundImage() != null) {
            int intWidth = (int) this.getBounds().getWidth() / 2;
            int intHeigth = (int) this.getBounds().getHeight() / 2;
            
            intWidth -= (this.properties.getBackgroundImage().getWidth(this) / 2);
            intHeigth -= (this.properties.getBackgroundImage().getHeight(this) / 2);
            
            graphic.drawImage(this.properties.getBackgroundImage(), intWidth, intHeigth, this);
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        
        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(0, 400, Short.MAX_VALUE)
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(0, 300, Short.MAX_VALUE)
                );
    }
    // </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
}