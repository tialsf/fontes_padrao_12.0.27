package br.com.microsiga.sigadw.desktop;

import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.Color;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Classe respons�vel por customizar a apresenta��o dos n�s da �rvore do SigaDw
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17 de Agosto de 2006
 */
public class DwTreeCellRendererDesktop extends DefaultTreeCellRenderer {
    
    /** Creates a new instance of DwTreeCellRender */
    public DwTreeCellRendererDesktop() {
        
    }
    
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        
        Object objNode = ((DefaultMutableTreeNode) value).getUserObject();
        if (objNode instanceof DwDesktopNode) {
            DwDesktopNode dwDesktopNode = (DwDesktopNode) objNode;
            if (leaf || !expanded) {
                this.setIcon(new ImageIcon(DwDesktop.m_imageListTree[dwDesktopNode.getImgCloseNode()]));
            } else {
                this.setIcon(new ImageIcon(DwDesktop.m_imageListTree[dwDesktopNode.getImgOpenNode()]));
            }
            this.setBackground(Color.WHITE);
            this.setBackgroundNonSelectionColor(Color.WHITE);
        }
        return this;
    }
    
}