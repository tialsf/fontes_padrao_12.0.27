package br.com.microsiga.sigadw.desktop.event;

import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.Image;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Classe respons�vel por atuar como um "listener" de eventos do desktop
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 24 de Agosto de 2006
 */
public interface DwDesktopListener {
    
    /**
     * Adiciona um n� root na arvore
     * @param desktopRoot objeto do tipo <code>DwDesktopNode</code> contendo o
     *  novo n� root
     */
    public void addNodeToRoot(DwDesktopNode desktopRoot);
    
    /**
     * Adiciona um n� root na arvore
     * @param desktopRoot objeto do tipo <code>DwDesktopNode</code> contendo o
     *  novo n� root
     */
    public void addNodeToRoot(DwDesktopNode desktopRoot, int parentID);
    
    /**
     * Adiciona um n� na arvore
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n�
     * @param parentID ID do n� pai deste n� adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, int parentID);
    
    /**
     * Adiciona um n� na arvore
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n�
     * @param dmtnParentNode n� pai deste n� adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, DefaultMutableTreeNode dmtnParentNode);
    
    /**
     * Adiciona um n� na arvore a partir de n� pai a ser pesquisado
     * @param newNode objeto do tipo <code>DwDesktopNode</code> contendo o novo n�
     * @param parentNodeSearch objeto do tipo <code>DwDesktopNode</code> contendo
     *  o n� pai deste n� adicionado
     */
    public void addNodeToNode(DwDesktopNode newNode, DwDesktopNode parentNodeSearch);
    
    /**
     * Altera um n� na arvore
     * @param node objeto do tipo <code>DwDesktopNode</code> contendo o n� a ser
     *  alterado
     */
    public void updateNode(DwDesktopNode node);
    
    /**
     * Remove um n� na arvore
     * @param node objeto do tipo <code>DwDesktopNode</code> contendo o n� a ser
     *  removido
     */
    public void removeNode(DwDesktopNode node);
    
    /**
     * Procura por um n� espec�fico do dw
     * @param nodeObjectCompare objeto do tipo <code>DwDesktopNode</code> contendo
     *  o n� a ser procurado
     * @return cole��o do tipo <code>List</cod> contendo objetos do tipo <code>DwDesktopNode</code>
     *  que representam os n�s l�gicos da pesquisa resultante
     */
    public List<DwDesktopNode> searchDesktopNode(DwDesktopNode nodeObjectCompare);
    
    /**
     * Procura por um n� espec�fico do dw
     * @param nodeObjectCompare objeto do tipo <code>DwDesktopNode</code> contendo
     *  o n� a ser procurado
     * @return cole��o do tipo <code>List</cod> contendo objetos do tipo <code>DefaultMutableTreeNode</code>
     *  que representam os n�s da pesquisa resultante
     */
    public List<DefaultMutableTreeNode> searchNodeTree(DwDesktopNode nodeObjectCompare);
    
    /**
     * Procura por um n� espec�fico do dw
     * @param nodeParent objeto do tipo <code>DefaultMutableTreeNode</code> contendo
     *  o n� ra�z do n� a ser pesquisado
     * @param nodeObjectCompare objeto do tipo <code>DwDesktopNode</code> contendo
     *  o n� a ser procurado
     * @return cole��o do tipo <code>List</cod> contendo objetos do tipo <code>DefaultMutableTreeNode</code>
     *  que representam os n�s da pesquisa resultante
     */
    public List<DefaultMutableTreeNode> searchNodeTree(DefaultMutableTreeNode nodeParent, DwDesktopNode nodeObjectCompare);
    
    /**
     * Cria/inclui um n� espec�fico do dw na base de dados
     * @param dwDesktopNode objeto do tipo <code>DwDesktopNode</code> contendo o n�
     */
    public int createNodeDB(DwDesktopNode dwDesktopNode);
    
    /**
     * Exclu� um n� espec�fico do dw na base de dados
     * @param dwDesktopNode objeto do tipo <code>DwDesktopNode</code> contendo o n�
     */
    public void deleteNodeDB(DwDesktopNode dwDesktopNode);
    
    /**
     * Atualiza um n� espec�fico do dw na base de dados
     * @param dwDesktopNode objeto do tipo <code>DwDesktopNode</code> contendo o n�
     */
    public void updateNodeDB(DwDesktopNode dwDesktopNode);
    
    /**
     * Carrega uma imagem a partir do diret�rio passado como par�metro no applet
     * @param imageFile nome do arquivo a ser lido
     * @return objeto do tipo <code>Image</code> contendo a imagem carregada
     */
    public Image getImage(String imageFile);
    
    /**
     * Carrega uma imagem a partir de um caminho especifico
     * @param imageFile nome do arquivo a ser lido
     * @return objeto do tipo <code>Image</code> contendo a imagem carregada
     */
    public Image getImageResource(String imageFile);
    
}