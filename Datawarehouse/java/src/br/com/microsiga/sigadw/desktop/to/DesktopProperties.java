package br.com.microsiga.sigadw.desktop.to;

import java.awt.Color;
import java.awt.Image;
import java.net.URL;
import javax.swing.Icon;

/**
 * Classe respons�vel por encapsular as propriedades referentes ao desktop
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 06/10/2006
 */
public class DesktopProperties {
    
    // labels passados atrav�s da tag <applet
    private String strImagePath = "";
    private String strSession = "";
    private URL urlCodeBase;
    private String strLabelMyFavorites = "";
    private String strLabelDefaultScenario = "";
    private String strLabelScenarioMaintenance = "";
    private String strLabelNewFolder = "";
    private String strLabelAddingFolder = "";
    private String strLabelNewScenario = "";
    private String strLabelAskDelScenario = "";
    private String strLabelDelScenario = "";
    private String strLabelUrlWallpaper = "";
    private String strLabelWallpaper = "";
    private String strLabelNewLink = "";
    private String strLabelAddingLink = "";
    private String strLabelTable = "";
    private String strLabelGraph = "";
    private String strLabelFolder = "";
    private String strLabelUpper = "";
    private String strLabelNew = "";
    private String strLabelAddScenario = "";
    private String strLabelFile = "";
    private String strLabelRename = "";
    private String strLabelRenaming = "";
    private String strLabelNewFile = "";
    private String strBackgroundURL;
    private Image imgBackground;
    private int intMaxCreateNodeConn = 5;
    private int intMaxCreateNodeConnSleep = 500;
    
    // icones das folhas da �rvore de objetos do sigadw
    private Icon icTreeLeaf;
    private Icon icTreeOpen;
    private Icon icTreeClose;
    
    // cor de fundo para �reas selecionadas com o mouse
    private Color clrAppletsSelections;
    // cor de fundo da �rea de trabalho
    private Color clrAppletsBgcolor;
    // cor de fundo da �rea do applet
    private Color clrAppletsWorkspaceBgcolor = new Color(212,208,200);
    // cor de fundo padr�o do bot�o
    private Color clrButtonBgcolor;
    // cor de fundo do bot�o caso esteja com drill down
    private Color clrButtonBgcolorDrillDown;
    // cor da fonte do bot�o caso esteja com drill down
    private Color clrButtonFontColorDrillDown;
    // cor de fundo da �rea de drag and drop quando for poss�vel realizar o drop
    private Color clrAcceptDrop = new Color(0xc6fff4);
    // cor de fundo da �rea de drag and drop quando n�o for poss�vel realizar o drop
    private Color clrRejectDrag = Color.red;
    
    private static DesktopProperties singleton = new DesktopProperties();
    
    /** Creates a new instance of DesktopProperties */
    private DesktopProperties() {
    }
    
    /**
     * M�todo est�tico respons�vel por recuperar a inst�ncia do singleton do
     *  objeto DefinitionProperties
     * @return
     *      Objeto do tipo <code>DefinitionProperties</code> contendo o singleton
     */
    public static DesktopProperties getInstance() {
        return singleton;
    }
    
    /**
     * M�todo respons�vel pela limpeza do objeto singleton de DefinitionProperties
     */
    public void clean() {
        
    }
    
    public String getImagePath() {
        return strImagePath + "/";
    }

    public void setImagePath(String strImagePath) {
        this.strImagePath = strImagePath;
    }

    public String getSession() {
        return strSession;
    }

    public void setSession(String strSession) {
        this.strSession = strSession;
    }

    public URL getCodeBase() {
        return urlCodeBase;
    }

    public void setCodeBase(URL urlCodeBase) {
        this.urlCodeBase = urlCodeBase;
    }

    public String getLabelMyFavorites() {
        return strLabelMyFavorites;
    }

    public void setLabelMyFavorites(String strLabelMyFavorites) {
        this.strLabelMyFavorites = strLabelMyFavorites;
    }

    public String getLabelDefaultScenario() {
        return strLabelDefaultScenario;
    }

    public void setLabelDefaultScenario(String strLabelDefaultScenario) {
        this.strLabelDefaultScenario = strLabelDefaultScenario;
    }

    public String getLabelScenarioMaintenance() {
        return strLabelScenarioMaintenance;
    }

    public void setLabelScenarioMaintenance(String strLabelScenarioMaintenance) {
        this.strLabelScenarioMaintenance = strLabelScenarioMaintenance;
    }

    public Icon getTreeLeaf() {
        return icTreeLeaf;
    }

    public void setTreeLeaf(Icon icTreeLeaf) {
        this.icTreeLeaf = icTreeLeaf;
    }

    public Icon getTreeOpen() {
        return icTreeOpen;
    }

    public void setTreeOpen(Icon icTreeOpen) {
        this.icTreeOpen = icTreeOpen;
    }

    public Icon getTreeClose() {
        return icTreeClose;
    }

    public void setTreeClose(Icon icTreeClose) {
        this.icTreeClose = icTreeClose;
    }

    public Color getAppletsSelections() {
        return clrAppletsSelections;
    }

    public void setAppletsSelections(Color clrAppletsSelections) {
        this.clrAppletsSelections = clrAppletsSelections;
    }

    public Color getAppletsBgcolor() {
        return clrAppletsBgcolor;
    }

    public void setAppletsBgcolor(Color clrAppletsBgcolor) {
        this.clrAppletsBgcolor = clrAppletsBgcolor;
    }

    public Color getAppletsWorkspaceBgcolor() {
        return clrAppletsWorkspaceBgcolor;
    }

    public void setAppletsWorkspaceBgcolor(Color clrAppletsWorkspaceBgcolor) {
        this.clrAppletsWorkspaceBgcolor = clrAppletsWorkspaceBgcolor;
    }

    public Color getButtonBgcolor() {
        return clrButtonBgcolor;
    }

    public void setButtonBgcolor(Color clrButtonBgcolor) {
        this.clrButtonBgcolor = clrButtonBgcolor;
    }

    public Color getButtonBgcolorDrillDown() {
        return clrButtonBgcolorDrillDown;
    }

    public void setButtonBgcolorDrillDown(Color clrButtonBgcolorDrillDown) {
        this.clrButtonBgcolorDrillDown = clrButtonBgcolorDrillDown;
    }

    public Color getButtonFontColorDrillDown() {
        return clrButtonFontColorDrillDown;
    }

    public void setButtonFontColorDrillDown(Color clrButtonFontColorDrillDown) {
        this.clrButtonFontColorDrillDown = clrButtonFontColorDrillDown;
    }

    public Color getAcceptDropColor() {
        return clrAcceptDrop;
    }

    public void setAcceptDropColor(Color clrAcceptDrop) {
        this.clrAcceptDrop = clrAcceptDrop;
    }

    public Color getRejectDragColor() {
        return clrRejectDrag;
    }

    public void setRejectDragColor(Color clrRejectDrag) {
        this.clrRejectDrag = clrRejectDrag;
    }

    public String getLabelNewFolder() {
        return strLabelNewFolder;
    }

    public void setLabelNewFolder(String strLabelNewFolder) {
        this.strLabelNewFolder = strLabelNewFolder;
    }

    public String getLabelAddingFolder() {
        return strLabelAddingFolder;
    }

    public void setLabelAddingFolder(String strLabelAddingFolder) {
        this.strLabelAddingFolder = strLabelAddingFolder;
    }

    public String getLabelNewScenario() {
        return strLabelNewScenario;
    }

    public void setLabelNewScenario(String strLabelNewScenario) {
        this.strLabelNewScenario = strLabelNewScenario;
    }

    public String getLabelAskDelScenario() {
        return strLabelAskDelScenario;
    }

    public void setLabelAskDelScenario(String strLabelAskDelScenario) {
        this.strLabelAskDelScenario = strLabelAskDelScenario;
    }

    public String getLabelDelScenario() {
        return strLabelDelScenario;
    }

    public void setLabelDelScenario(String strLabelDelScenario) {
        this.strLabelDelScenario = strLabelDelScenario;
    }

    public String getLabelUrlWallpaper() {
        return strLabelUrlWallpaper;
    }

    public void setLabelUrlWallpaper(String strLabelUrlWallpaper) {
        this.strLabelUrlWallpaper = strLabelUrlWallpaper;
    }

    public String getLabelWallpaper() {
        return strLabelWallpaper;
    }

    public void setLabelWallpaper(String strLabelWallpaper) {
        this.strLabelWallpaper = strLabelWallpaper;
    }

    public String getLabelNewLink() {
        return strLabelNewLink;
    }

    public void setLabelNewLink(String strLabelNewLink) {
        this.strLabelNewLink = strLabelNewLink;
    }

    public String getLabelAddingLink() {
        return strLabelAddingLink;
    }

    public void setLabelAddingLink(String strLabelAddingLink) {
        this.strLabelAddingLink = strLabelAddingLink;
    }

    public String getLabelTable() {
        return strLabelTable;
    }

    public void setLabelTable(String strLabelTable) {
        this.strLabelTable = strLabelTable;
    }

    public String getLabelGraph() {
        return strLabelGraph;
    }

    public void setLabelGraph(String strLabelGraph) {
        this.strLabelGraph = strLabelGraph;
    }

    public String getLabelFolder() {
        return strLabelFolder;
    }

    public void setLabelFolder(String strLabelFolder) {
        this.strLabelFolder = strLabelFolder;
    }

    public String getLabelUpper() {
        return strLabelUpper;
    }

    public void setLabelUpper(String strLabelUpper) {
        this.strLabelUpper = strLabelUpper;
    }

    public String getLabelNew() {
        return strLabelNew;
    }

    public void setLabelNew(String strLabelNew) {
        this.strLabelNew = strLabelNew;
    }

    public String getLabelAddScenario() {
        return strLabelAddScenario;
    }

    public void setLabelAddScenario(String strLabelAddScenario) {
        this.strLabelAddScenario = strLabelAddScenario;
    }

    public String getLabelFile() {
        return strLabelFile;
    }

    public void setLabelFile(String strLabelFile) {
        this.strLabelFile = strLabelFile;
    }
    
    public String getLabelRename() {
        return strLabelRename;
    }

    public void setLabelRename(String strLabelRename) {
        this.strLabelRename = strLabelRename;
    }

    public String getLabelRenaming() {
        return strLabelRenaming;
    }

    public void setLabelRenaming(String strLabelRenaming) {
        this.strLabelRenaming = strLabelRenaming;
    }

    public String getLabelNewFile() {
        return strLabelNewFile;
    }

    public void setLabelNewFile(String strLabelNewFile) {
        this.strLabelNewFile = strLabelNewFile;
    }

    public Image getBackgroundImage() {
        return imgBackground;
    }

    public void setBackgroundImage(Image imgBackground) {
        this.imgBackground = imgBackground;
    }

    public String getBackgroundURL() {
        return strBackgroundURL;
    }

    public void setBackgroundURL(String strBackgroundURL) {
        this.strBackgroundURL = strBackgroundURL;
    }

    public int getMaxCreateNodeConn() {
        return intMaxCreateNodeConn;
    }

    public void setMaxCreateNodeConn(int intMaxCreateNodeConn) {
        this.intMaxCreateNodeConn = intMaxCreateNodeConn;
    }

    public int getMaxCreateNodeConnSleep() {
        return intMaxCreateNodeConnSleep;
    }

    public void setMaxCreateNodeConnSleep(int intMaxCreateNodeConnSleep) {
        this.intMaxCreateNodeConnSleep = intMaxCreateNodeConnSleep;
    }
    
}