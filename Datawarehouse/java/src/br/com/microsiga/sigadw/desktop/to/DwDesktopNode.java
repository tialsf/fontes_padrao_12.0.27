package br.com.microsiga.sigadw.desktop.to;

import static br.com.microsiga.sigadw.desktop.DwDesktop.*;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe respons�vel por encapsular as propriedades referentes � elemento do desktop
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 15 de Agosto de 2006
 */
public class DwDesktopNode extends DwObject implements Serializable {
    // CONSTANTES ANTIGAS - MANTIDAS POR RAZ�ES DE COMPATIBILIZA��O
    public static final int dwDESKTOP_FOLDER            =  3;
    public static final int dwDESKTOP_FILE              =  4;
    public static final int dwDESKTOP_TABLE             =  5;
    public static final int dwDESKTOP_GRAPH             =  6;
    
    // CONSTANTES DE TIPOS DE N�S
    public static final int dwDESKTOP_COMPARE           =-99;
    public static final int dwDESKTOP_WORKSPACE         =  0;
    public static final int dwDESKTOP_FIXED             =  1;
    public static final int dwDESKTOP_SCENARIO          =  2;
    public static final int dwDESKTOP_QUERY             =  7;
    public static final int dwDESKTOP_QUERIES           =  8;
    public static final int dwDESKTOP_FAVORITES         =  9;
    public static final int dwDESKTOP_FAVORITES_FOLDER  = 10;
    public static final int dwDESKTOP_FAVORITES_LINK    = 11;
    public static final int dwDESKTOP_WALLPAPER         = 12;
    
    private String strDescricao = "";
    private int intType;
    private boolean blnNodeEnabled;
    private int intImageIndex;
    private int intRootID = -1;
    private String strConsDisp = "";
    private boolean blnHaveTable = false;
    private boolean blnHaveGraph = false;
    private int imgCloseNode;
    private int imgOpenNode;
    private String strQueryType = "";
    private boolean blnMaintAllow;
    private int intScenario = -1;
    private String strScenario = "";
    private boolean blnFavorite;
    private List<DwDesktopNode> lstDesktopNodes = new ArrayList<DwDesktopNode>();
    
    /** Creates a new instance of DwDesktopNode */
    public DwDesktopNode() {
        this.setId(-1);
    }

    public int getObjectType() {
        return super.OBJECT_DESKTOP;
    }

    public String getDescricao() {
        return strDescricao;
    }

    public void setDescricao(String strDwDescricao) {
        this.strDescricao = strDwDescricao;
    }

    public int getType() {
        return intType;
    }

    public void setType(int intDwType) {
        this.intType = intDwType;
    }

    public boolean isNodeEnabled() {
        return blnNodeEnabled;
    }

    public void setNodeEnabled(boolean blnNodeUsed) {
        this.blnNodeEnabled = blnNodeUsed;
    }

    public int getImageIndex() {
        return intImageIndex;
    }

    public void setImageIndex(int intImgSt0) {
        this.intImageIndex = intImgSt0;
    }

    public int getRootID() {
        return intRootID;
    }

    public void setRootID(int intRootID) {
        this.intRootID = intRootID;
    }

    public String getConsDisp() {
        return strConsDisp;
    }

    public void setConsDisp(String strConsDisp) {
        this.setHaveTable(strConsDisp.startsWith("1"));
        this.setHaveGraph(strConsDisp.endsWith("1"));
        this.defineImageIcons();
        
        this.strConsDisp = strConsDisp;
    }

    public boolean haveTable() {
        return blnHaveTable;
    }

    public void setHaveTable(boolean blnHaveTable) {
        this.blnHaveTable = blnHaveTable;
    }

    public boolean haveGraph() {
        return blnHaveGraph;
    }

    public void setHaveGraph(boolean blnHaveGraph) {
        this.blnHaveGraph = blnHaveGraph;
    }

    public int getImgCloseNode() {
        return imgCloseNode;
    }

    public void setImgCloseNode(int imgSt0) {
        this.imgCloseNode = imgSt0;
    }

    public int getImgOpenNode() {
        return imgOpenNode;
    }

    public void setImgOpenNode(int imgSt1) {
        this.imgOpenNode = imgSt1;
    }

    public String getQueryType() {
        return strQueryType;
    }

    public void setQueryType(String queryType) {
        this.strQueryType = queryType;
    }

    public boolean isMaintAllow() {
        return blnMaintAllow;
    }

    public void setMaintAllow(boolean blnMaintAllow) {
        this.blnMaintAllow = blnMaintAllow;
    }

    public int getScenarioId() {
        return intScenario;
    }

    public void setScenarioId(int intScenario) {
        this.intScenario = intScenario;
    }

    public String getScenarioName() {
        return this.strScenario;
    }

    public void setScenarioName(String strScenario) {
        this.strScenario = strScenario;
    }

    public boolean isFavorite() {
        return blnFavorite;
    }

    public void setFavorite(boolean blnFavorite) {
        this.blnFavorite = blnFavorite;
    }

    public void addNode(DwDesktopNode desktopNode) {
        this.lstDesktopNodes.add(desktopNode);
    }

    public void removeNode(DwDesktopNode desktopNode) {
        this.lstDesktopNodes.remove(desktopNode);
    }

    public List<DwDesktopNode> getNodes() {
        return lstDesktopNodes;
    }

    public void setNodes(List<DwDesktopNode> lstNodes) {
        this.lstDesktopNodes = lstNodes;
    }

    public void defineImageIcons() {
        if (!this.isNodeEnabled() && this.getQueryType() != null) {
            if (this.haveTable()) {
                if (this.getQueryType().equals(QUERY_USER)) {
                    this.setImgOpenNode(IMG_IC_TREE_USER_TABLE);
                    this.setImgCloseNode(IMG_IC_TREE_USER_TABLE);
                } else {
                    this.setImgOpenNode(IMG_IC_TREE_TABLE);
                    this.setImgCloseNode(IMG_IC_TREE_TABLE);
                }
            } else if (this.haveGraph()) {
                if (this.getQueryType().equals(QUERY_USER)) {
                    this.setImgOpenNode(IMG_IC_TREE_USER_GRAPH);
                    this.setImgCloseNode(IMG_IC_TREE_USER_GRAPH);
                } else {
                    this.setImgOpenNode(IMG_IC_TREE_GRAPH);
                    this.setImgCloseNode(IMG_IC_TREE_GRAPH);
                }
            }
        }
    }

    public DwDesktopNode clone() {
        DwDesktopNode dwDesktopNode = new DwDesktopNode(); 
        
        dwDesktopNode.setId(this.getId());
        dwDesktopNode.setName(this.getName());
        dwDesktopNode.setDescricao(this.getDescricao());
        dwDesktopNode.setType(this.getType());
        dwDesktopNode.setImageIndex(this.getImageIndex());
        dwDesktopNode.setRootID(this.getRootID());
        dwDesktopNode.setConsDisp(this.getConsDisp());
        dwDesktopNode.setHaveTable(this.haveTable());
        dwDesktopNode.setHaveGraph(this.haveGraph());
        dwDesktopNode.setImgCloseNode(this.getImgCloseNode());
        dwDesktopNode.setImgOpenNode(this.getImgOpenNode());
        dwDesktopNode.setQueryType(this.getQueryType());
        dwDesktopNode.setScenarioId(this.getScenarioId());
        dwDesktopNode.setScenarioName(this.getScenarioName());
        dwDesktopNode.setFavorite(this.isFavorite());
        dwDesktopNode.setMaintAllow(this.isMaintAllow());
        dwDesktopNode.setNodeEnabled(dwDesktopNode.isNodeEnabled());
        dwDesktopNode.setNodes(dwDesktopNode.getNodes());
        
        return dwDesktopNode;
    }

    public boolean equals(Object obj) {
        boolean blnEqual = false;
        
        if (obj instanceof DwDesktopNode) {
            DwDesktopNode nodeToCompare = (DwDesktopNode) obj;
            if (nodeToCompare.getType() == this.dwDESKTOP_COMPARE || nodeToCompare.getType() == this.dwDESKTOP_COMPARE) {
                if (nodeToCompare.getId() == this.getId()) {
                    blnEqual = true;
                }
            } else if (nodeToCompare.getType() == this.dwDESKTOP_QUERY || nodeToCompare.getType() == this.dwDESKTOP_QUERIES) {
                if (nodeToCompare.getRootID() == this.getRootID()) {
                    blnEqual = true;
                }
            } else if (nodeToCompare.getType() == this.getType() && this.getType() == this.dwDESKTOP_FAVORITES) {
                blnEqual = true;
            } else if (nodeToCompare.getType() == this.getType() && this.getType() == this.dwDESKTOP_FAVORITES_FOLDER) {
                if (nodeToCompare.getId() == this.getId()) {
                    blnEqual = true;
                }
            } else if (nodeToCompare.getType() == this.getType() && this.getType() == this.dwDESKTOP_FOLDER) {
                if (nodeToCompare.getId() == this.getId()) {
                    blnEqual = true;
                }
            } else if (nodeToCompare.getType() == this.getType() && this.getType() == this.dwDESKTOP_WORKSPACE) {
                blnEqual = true;
            } else if (nodeToCompare.getType() == this.getType() && this.getType() == this.dwDESKTOP_SCENARIO) {
                if (nodeToCompare.getScenarioId() == this.getScenarioId()) {
                    blnEqual = true;
                }
            } else if (nodeToCompare.getId() == this.getId()
                    && nodeToCompare.getRootID() == this.getRootID()
                    && nodeToCompare.getScenarioId() == this.getScenarioId()
                    && nodeToCompare.isFavorite() == this.isFavorite()
                    && nodeToCompare.isNodeEnabled() == this.isNodeEnabled()
                    && nodeToCompare.getType() == this.getType()) {
                blnEqual = true;
            }
        }
        
        return blnEqual;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        if (!this.getDescricao().trim().equals("")) {
            sb.append(this.getDescricao());
        }

        if (!this.getName().trim().equals("")) {
            if (!this.getDescricao().trim().equals("")) {
                sb.append(" (");
            }
            
            sb.append(this.getName());
            
            if (!this.getDescricao().trim().equals("")) {
                sb.append(")");
            }
        }

        if (this.haveTable() && this.haveGraph()) {
            sb.append(" [" + DesktopProperties.getInstance().getLabelTable() +
                    "/" + DesktopProperties.getInstance().getLabelGraph() + "]");
        } else if (this.haveTable()) {
            sb.append(" [" + DesktopProperties.getInstance().getLabelTable() + "]");
        } else if (this.haveGraph()) {
            sb.append(" [" + DesktopProperties.getInstance().getLabelGraph() + "]");
        }
        
        return sb.toString();
    }

    public int hashCode() {
        return this.getId();
    }

}