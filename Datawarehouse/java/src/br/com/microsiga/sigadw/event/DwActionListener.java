package br.com.microsiga.sigadw.event;

import java.awt.event.ActionListener;

/**
 * Interface respons�vel por lidar com action listeners de componentes <SigaDWComponent> no SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 14 de Junho de 2006
 */
public interface DwActionListener<SigaDWComponent> {
    
    /**
     * <pre>M�todo que ser� notificado caso algum componente do dw seja ativado/clicado
     * 
     * @param dwComponentSrc
     *      Objeto do tipo <code>DwButton</code> que foi a origem do evento
     * 
     * </pre>
     */
    public void notifyActionListener(SigaDWComponent dwComponentSrc);
    
}