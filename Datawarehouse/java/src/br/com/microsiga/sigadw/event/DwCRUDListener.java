package br.com.microsiga.sigadw.event;

/**
 * Representa uma interface de um listener de CRUD Operations
 *  (cria��o, recuperar, atualiza��o e exclus�o) de um componente gen�rico
 *  SigaDwComponentSrc que ocorrer em outro componente gen�rico SigaDwComponent.
 *  Somente as opera��es de permiss�o, cria��o e exclus�o ser�o implementadas.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 28 de Junho de 2006
 */
public interface DwCRUDListener<SigaDwComponentSrc, SigaDwComponent> {
    
    /**
     * M�todo respons�vel por verificar se � permitido adicionar um componente
     *  do sigadw (par�metro dwComponentSrc) ao componente base (par�metro dwComponent)
     * @param dwComponentSrc cont�m o componente cuja verifica��o de permiss�o
     *  est� sendo cheacada
     * @param dwComponent cont�m o componente aonde est� sendo feita adi��o e
     *  aonde ser� ou n�o adicionado o componente dwComponentSrc
     * @return <tt>true</tt> caso seja permitido adicionar o componente,
     *  <tt>false</tt> caso contr�rio
     */
    public boolean canAdd(SigaDwComponentSrc dwComponentSrc, SigaDwComponent dwComponent);
    
    /**
     * M�todo respons�vel por tratar a adi��o de um componente do sigadw
     *  (par�metro dwComponentSrc) ao componente base (par�metro dwComponent)
     * @param dwComponentSrc cont�m o componente que ser� adicionado
     * @param dwComponent cont�m o componente aonde est� sendo feita adi��o
     *  <tt>false</tt> caso contr�rio
     */
    public void notifyCreate(SigaDwComponentSrc dwComponentSrc, SigaDwComponent dwComponent);
    
    /**
     * M�todo respons�vel por tratar a exclus�o de um componente do sigadw
     *  (par�metro dwComponentSrc) do componente base (par�metro dwComponent)
     * @param dwComponentSrc cont�m o componente exclu�do
     * @param dwComponent cont�m o componente aonde est� sendo feita exclus�o
     *  <tt>false</tt> caso contr�rio
     */
    public void notifyDelete(SigaDwComponentSrc dwComponentSrc, SigaDwComponent dwComponent);
    
}