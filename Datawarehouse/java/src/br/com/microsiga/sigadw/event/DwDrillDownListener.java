package br.com.microsiga.sigadw.event;

/**
 * Interface de uma classe listener de eventos de drill down
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 27 de Junho de 2006
 */
public interface DwDrillDownListener<SigaDWComponent> {
    
    /**
     * <pre>M�todo respons�vel por responder a notifica��o da ocorr�ncia de drill down.
     * 
     * @param blnPropertyValue
     *      Primitiva do tipo <code>boolean</code> contendo o valor da altera��o no drill down
     * </pre>
     */
    public void notifyDrillDown(boolean blnPropertyValue);

    /**
     * <pre>M�todo respons�vel por responder a notifica��o da ocorr�ncia de drill down
     *  passando o componente aonde originou-se o drill down.
     * 
     * @param blnPropertyValue
     *      Primitiva do tipo <code>boolean</code> contendo o valor da altera��o no drill down
     * @param componentSrc
     *      Objeto gen�rico do tipo <code>SigaDWComponent</code> contendo passando o componente
     *  original do evento
     * </pre>
     */
    public void notifyDrillDown(boolean blnPropertyValue, SigaDWComponent componentSrc);
    
}