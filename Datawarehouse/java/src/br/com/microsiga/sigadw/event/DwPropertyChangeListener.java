package br.com.microsiga.sigadw.event;

import br.com.microsiga.sigadw.definition.*;

/**
 * Interface respons�vel por lidar com mudan�as em propriedades do component <SigaDWComponent> do sigadw
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 - 28 de Abril de 2006
 */
public interface DwPropertyChangeListener<SigaDWComponent> {
    
    /**
     * <pre>M�todo que ser� notificado caso alguma propriedade do componente <SigaDWComponent> ocorra
     *
     * @param dwComponentSrc
     *      Objeto do tipo <code>SigaDWComponent</code> que foi a origem da mudan�a de propriedades
     * @param strProperty
     *      Objeto do tipo <code>String</code> contendo a propriedade que foi
     *  alterada. Para visualizar os poss�veis valores, verifique as constantes (PROPERTY_XXXX)
     *  nos bot�es do sigadw (DwButton e as suas implementa��es espec�ficas)
     * @param strPropertyValue
     *      Objeto do tipo <code>String</code> contendo o valor alterado
     *
     * </pre>
     */
    public void notifyPropertyChange(SigaDWComponent dwComponentSrc, String strProperty, String strPropertyValue);
    
}