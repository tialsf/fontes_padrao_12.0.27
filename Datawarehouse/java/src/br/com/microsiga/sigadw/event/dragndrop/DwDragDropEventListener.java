package br.com.microsiga.sigadw.event.dragndrop;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Point;

/**
 * Classe respons�vel por lidar com drag and drop dos bot�es nos eixos
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 28 de Abril de 2006
 */
public interface DwDragDropEventListener<SigaDwObject extends DwObject> {
    
    /**
     * <pre>M�todo que ser� notificado caso algum evento de drag and drop
     *  ocorra no componente do siga dw
     *
     * @param dwObject
     *      Objeto gen�rico do tipo <code>DwObject</code> relacionado com o componente
     *  aonde ocorreu o evento de drag and drop
     * </pre>
     */
    public void notifyDragDrop(SigaDwObject dwObject);
    
    /**
     * M�todo que ser� notificado ao termino do evento de drag and drop. Este m�todo
     *  ser� chamado no componente aonde ocorreu o evento de drag and drop
     */
    public void notifyDropEnd();
    
    /**
     * M�todo que ser� utilizado para notificar listeners do componente que foi a
     *  origem do evento de drag and drop
     * 
     * @param dwObject
     *      Objeto gen�rico do tipo <code>DwObject</code> relacionado com o componente
     *  aonde ocorreu o evento de drag and drop
     */
    public void notifyDropEnd(SigaDwObject dwObject);
    
    /**
     * M�todo que ser� utilizado para notificar que um evento de DragNDrop est� numa
     *  �rea aonde pode ser realizado o evento de Drop.
     * 
     * @param Point
     *      Coordenadas do ponto aonde o mouse que gerou o evento est� no momento
     */
    public void handleDropAccept(Point point);
    
    /**
     * M�todo que ser� utilizado para notificar que um evento de DragNDrop n�o est� numa
     *  �rea aonde pode ser realizado o evento de Drop.
     * 
     * @param Point
     *      Coordenadas do ponto aonde o mouse que gerou o evento est� no momento
     */
    public void handleDropReject(Point point);
    
    /**
     * M�todo que ser� utilizado para notificar que um evento de DragNDrop foi finalizado
     */
    public void handleDropExit();
    
}