package br.com.microsiga.sigadw.event.dragndrop;

import java.awt.Component;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceListener;
import javax.swing.JTree;

/**
 * Objeto responsável por encapsular o listener responsável por drag and drop, startando o processo.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 23/07/2006
 */
public class DwDragGestureListener<SigaDwComponent extends Component> implements DragGestureListener {
    
    protected DragSourceListener dragSourceListener;
    
    /** Creates a new instance of DwDragGestureListener */
    public DwDragGestureListener(DragSourceListener dsl) {
        this.dragSourceListener = dsl;
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        dge.startDrag(DragSource.DefaultCopyDrop,
                new DwTransferable<SigaDwComponent>((SigaDwComponent) dge.getComponent()),
                    this.dragSourceListener);
    }
    
}