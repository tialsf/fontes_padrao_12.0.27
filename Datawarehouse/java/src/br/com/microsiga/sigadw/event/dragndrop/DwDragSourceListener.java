package br.com.microsiga.sigadw.event.dragndrop;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.Component;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;

/**
 * Objeto responsável por encapsular o listener da origem do drag and drop.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 23/07/2006
 */
public class DwDragSourceListener implements java.awt.dnd.DragSourceListener {
    
    /**
     * Creates a new instance of DwDragSourceListener
     */
    public DwDragSourceListener() {
    }
    
    public void dragEnter(DragSourceDragEvent dsde) {
        
    }
    
    public void dragOver(DragSourceDragEvent dsde) {
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    public void dragExit(DragSourceEvent dse) {
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
        if (dsde.getDropSuccess()) {
            if (dsde.getDragSourceContext().getComponent() instanceof DwDragDropEventListener) {
                ((DwDragDropEventListener) dsde.getDragSourceContext().getComponent()).notifyDropEnd();
            }
        }
    }
    
}