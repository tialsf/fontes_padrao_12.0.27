package br.com.microsiga.sigadw.event.dragndrop;

import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;

/**
 * Objeto responsável por encapsular o listener do destino do evento de drag and drop.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 23/07/2006
 */
public class DwDropTargetListener<T extends DwObject>
        implements DropTargetListener {
    
    private DwDragDropEventListener<T> dragDropListener;
    private DataFlavor dataFlavorSupported;
    
    /** Creates a new instance of DwDropTargetListener */
    public DwDropTargetListener(DwDragDropEventListener<T> dbael, DataFlavor dfToSupport) {
        this.dragDropListener = dbael;
        this.dataFlavorSupported = dfToSupport;
    }
    
    public void dragEnter(DropTargetDragEvent dtde) {
        if (dtde.getTransferable().isDataFlavorSupported(this.dataFlavorSupported)) {
            dtde.acceptDrag(DnDConstants.ACTION_COPY);
            this.dragDropListener.handleDropAccept(dtde.getLocation());
        } else {
            dtde.rejectDrag();
            this.dragDropListener.handleDropReject(dtde.getLocation());
        }
    }

    public void dragOver(DropTargetDragEvent dtde) {
        if (dtde.getTransferable().isDataFlavorSupported(this.dataFlavorSupported)) {
            dtde.acceptDrag(DnDConstants.ACTION_COPY);
            this.dragDropListener.handleDropAccept(dtde.getLocation());
        } else {
            dtde.rejectDrag();
            this.dragDropListener.handleDropReject(dtde.getLocation());
        }
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
        if (dtde.getTransferable().isDataFlavorSupported(this.dataFlavorSupported)) {
            dtde.acceptDrag(DnDConstants.ACTION_COPY);
            this.dragDropListener.handleDropAccept(dtde.getLocation());
        } else {
            dtde.rejectDrag();
            this.dragDropListener.handleDropReject(dtde.getLocation());
        }
    }

    public void dragExit(DropTargetEvent dte) {
        this.dragDropListener.handleDropExit();
    }
    
    public void drop(DropTargetDropEvent dtde) {
        dtde.acceptDrop(DnDConstants.ACTION_COPY);
        Object object;
        try {
            object = dtde.getTransferable().getTransferData(this.dataFlavorSupported);
            if (object != null) {
                dtde.dropComplete(true);
                
                T dwObject = (T) object;
                dwObject.setDDOrigin(dtde.getLocation());
                
                dragDropListener.notifyDragDrop(dwObject);
            } else {
                dtde.dropComplete(false);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (UnsupportedFlavorException ex) {
            ex.printStackTrace();
        }
        
        this.dragDropListener.handleDropExit();
    }
    
}