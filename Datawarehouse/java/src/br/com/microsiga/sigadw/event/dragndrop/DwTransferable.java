package br.com.microsiga.sigadw.event.dragndrop;

import br.com.microsiga.sigadw.definition.DwButton;
import br.com.microsiga.sigadw.definition.to.DefinitionProperties;
import br.com.microsiga.sigadw.definition.to.Dimension;
import br.com.microsiga.sigadw.definition.to.DwField;
import br.com.microsiga.sigadw.event.dragndrop.to.DwObject;
import br.com.microsiga.sigadw.definition.to.Measure;
import br.com.microsiga.sigadw.desktop.DwIcone;
import br.com.microsiga.sigadw.desktop.to.DwDesktopNode;
import java.awt.Component;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * Objeto responsável por encapsular o transferable utilizado no evento de drag and drop.
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 23/07/2006
 */
public class DwTransferable<SigaDwComponent extends Component> implements Transferable, ClipboardOwner {
    
    /** DataFlavor para transferência de objetos genéricos sigadw */
    public static final DataFlavor sigaDwObject     = new DataFlavor(DwObject.class, "Objeto do SigaDW");
    
    /** DataFlavor para transferência de campos de dimensão */
    public static final DataFlavor sigaDwDimField   = new DataFlavor(DwField.class, "Campo de dimensão do SigaDW");
    
    /** DataFlavor para transferência de indicadores */
    public static final DataFlavor sigaDwMeasure    = new DataFlavor(Measure.class, "Indicador do SigaDW");
    
    /** DataFlavor para transferência de consultas */
    public static final DataFlavor sigaDwQuery    = new DataFlavor(DwDesktopNode.class, "Consultas do SigaDW");
    
    public static final DataFlavor[] flavors = {
        DwTransferable.sigaDwObject,
        DwTransferable.sigaDwDimField,
        DwTransferable.sigaDwMeasure,
        DwTransferable.sigaDwQuery
    };
    
    public static final List lstFlavors = Arrays.asList(flavors);

    private SigaDwComponent sigaDwComponent;
    
    /** Creates a new instance of DwTransferable */
    public DwTransferable(SigaDwComponent sigaDwComponent) {
        this.sigaDwComponent = sigaDwComponent;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        boolean blnSupported = false;
        DwObject dwObject;
        
        if (flavor.equals(sigaDwDimField)) {
            dwObject = this.getComponentData();
            if (dwObject.getObjectType() == DwObject.OBJECT_DIMFIELD) {
                blnSupported = true;
            }
        } else if (flavor.equals(sigaDwMeasure)) {
            dwObject = this.getComponentData();
            if (dwObject.getObjectType() == DwObject.OBJECT_MEASURE) {
                blnSupported = true;
            }
        } else if (flavor.equals(sigaDwObject)) {
            dwObject = this.getComponentData();
            if (dwObject != null && dwObject instanceof DwObject) {
                blnSupported = true;
            }
        } else if (flavor.equals(sigaDwQuery)) {
            blnSupported = true;
        } else {
            blnSupported = false;
        }
        
        return blnSupported;
    }

    public DwObject getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        DwObject object = null;
        
        if (isDataFlavorSupported(flavor)) {
            object = this.getComponentData();
        }
        
        return object;
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }
    
    protected DwObject getComponentData() {
        DwObject object = null;
        
        if (sigaDwComponent instanceof JTree) {
            TreePath treePath = ((JTree) sigaDwComponent).getSelectionPath();
            if (treePath != null) {
                DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) treePath.getLastPathComponent();
                object = (DwObject) dmtn.getUserObject();
                object.setDwObjectOrigin(DwObject.ORIGIN_DWELEMENTS);
           }
        } else if (sigaDwComponent instanceof DwButton) {
            DwButton dwButton = ((DwButton) sigaDwComponent);
            object = dwButton.getDwObject();
            object.setProperties(dwButton.getProperties());
            object.setDwObjectOrigin(DwObject.ORIGIN_DWAXIS);
        } else if (sigaDwComponent instanceof DwIcone) {
            object = ((DwIcone) sigaDwComponent).getDesktopNode();
        }
       
       return object;
    }
    
}