package br.com.microsiga.sigadw.event.dragndrop.to;

import java.awt.Component;
import java.awt.Point;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * Representa em alto n�vel um elemento do SigaDW
 * @author Paulo R. Vieira - Microsiga Software S/A
 * @since SigaDW R4 17 de Abril de 2006
 */
public abstract class DwObject implements Serializable {
    
    // propriedades b�sicas
    private int id;
    private String name = "";
    private String strIconPath = "";
    private transient Icon iconElement;
    
    /** origem do objeto do dw
     *  @see br.com.microsiga.sigadw.event.dragndrop.to.DwObject#ORIGIN_DWELEMENTS
     *  @see br.com.microsiga.sigadw.event.dragndrop.to.DwObject#ORIGIN_DWAXIS
     */
    private int intDwObjectOrigin;
    /** componente de origem do drag and drop */
    private transient Component cmpOrigin;
    /** coordenada da origem do drag and drop */
    private Point ptnDDOrigin;
    /** propriedades do objeto dw */
    private String strProperties;
    
    // tipos de objetos supoertados
    public static final int OBJECT_CUBE         = 1;
    public static final int OBJECT_DIMENSION    = 2;
    public static final int OBJECT_DIMFIELD     = 3;
    public static final int OBJECT_MEASURE      = 4;
    public static final int OBJECT_DIALOGY      = 5;
    public static final int OBJECT_AGREGATION   = 6;
    public static final int OBJECT_GRPH_TYPE    = 7;
    public static final int OBJECT_DESKTOP      = 8;
    
    // tipos de origens de um dwobject
    public static final int ORIGIN_DWELEMENTS   = 1;
    public static final int ORIGIN_DWAXIS       = 2;
    
    /**
     * Constr�i um JavaBean do tipo DwObject
     */
    public DwObject() {
    }
    
    /**
     * Constr�i um JavaBean do tipo DwObject passando o seu id, nome e icone correspondente
     * @param id
     *      Primitiva do tipo <code>int</code> contendo o c�digo
     * @param nome
     *      Objeto do tipo <code>String</code> contendo o nome
     * @param strIconElement
     *      Obejto do tipo <code>String</code> contendo o �cone deste objeto
     */
    public DwObject(int id, String nome, String strIconElement) {
        setId(id);
        setName(nome);
        setIconPath(strIconElement);
    }
    
    /**
     * M�todo respons�vel por recuperar o atributo com o id deste objeto
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public int getId() {
        return id;
    }
    
    /**
     * M�todo respons�vel por definir o atributo com o id deste objeto
     * @param id
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * M�todo respons�vel por recuperar o atributo com o nome deste objeto
     * @return
     *      Objeto do tipo <code>String</code> contendo o atributo
     */
    public String getName() {
        return name;
    }
    
    /**
     * M�todo respons�vel por definir o atributo com o nome deste objeto
     * @param name
     *      Obejto do tipo <code>String</code> contendo o atributo
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * M�todo abstrato respons�vel por recuperar o tipo de cada objeto suportado.
     *  Dever� ser implementado pelas classes filhas que forem espec�ficas para
     *  cada objeto suportado
     * @return
     *      Primitiva do tipo <code>int</code> contendo o atributo
     */
    public abstract int getObjectType();
    
    /**
     * M�todo respons�vel pela compara��o entre diversos objetos do mesmo tipo
     *  DwObject. Esta compara��o ser� realiazada atrav�s da verifica��o do tipo dos objetos,
     *  que deveram ser do tipo <code>DwObject</code> e atrav�s do atributo ID.
     * @param obj
     *      Objeto do tipo <code>Object</code> contendo o objeto a ser comparado com este
     * @return
     *      Primitiva do tipo <code>boolean</code> contendo o true se forem iguais
     *  e false caso contr�rio
     */
    public boolean equals(Object obj) {
        boolean blnEquals = false;
        
        if (obj instanceof DwObject && ((DwObject) obj).getId() == this.getId()) {
            blnEquals = true;
        }
        
        return blnEquals;
    }
    
    /**
     * M�todo respons�vel pela defini��o de um valor num�rico exclusivo para
     *  cada objeto instanciado atrav�s dessa classe. Este valor num�rico ser�
     *  o do atributo ID.
     * @return
     *      Primitiva do tipo <code>int</code> contendo o valor exclusivo
     */
    public int hashCode() {
        return this.getId();
    }
    
    /**
     * M�todo respons�vel pela tradu��o deste objeto em string. Esta tradu��o ser�
     *  realizado atrav�s do atributo name.
     * @return
     *      Objeto do tipo <code>String</code> a tradu��o
     */
    public String toString() {
        return this.getName();
    }

    public int getDwObjectOrigin() {
        return intDwObjectOrigin;
    }

    public void setDwObjectOrigin(int intDwObjectOrigin) {
        this.intDwObjectOrigin = intDwObjectOrigin;
    }

    public Component getComponentOrigin() {
        return cmpOrigin;
    }

    public void setComponentOrigin(Component cmpOrigin) {
        this.cmpOrigin = cmpOrigin;
    }

    public Point getDDOrigin() {
        return ptnDDOrigin;
    }

    public void setDDOrigin(Point ptnOrigin) {
        this.ptnDDOrigin = ptnOrigin;
    }

    public String getProperties() {
        return strProperties;
    }

    public void setProperties(String strProperties) {
        this.strProperties = strProperties;
    }

    /**
     * M�todo respons�vel por recuperar o atributo com o �cone/imagem deste objeto
     * @return
     *      Objeto do tipo <code>Icon</code> contendo o atributo
     */
    public Icon getIconElement() {
        if (this.iconElement == null && this.getIconPath().trim() != "") {
            try {
                this.iconElement = new ImageIcon(new URL(this.getIconPath()));
            } catch (MalformedURLException ex) {
                System.err.println("Error - Image Url: " + this.getIconPath());
                ex.printStackTrace();
            }
        }
        
        return iconElement;
    }
    
    public String getIconPath() {
        return strIconPath;
    }

    public void setIconPath(String strIconElement) {
        this.strIconPath = strIconElement;
    }

}