package br.com.microsiga.sigadw.http;

import java.io.*;
import java.net.*;
/**
 * Componente permite criar uma conex�o URL para falar Http com o servidor.
 * <br>Nesta vers�o, voc� pode:
 * <OL>
 * <LI>Fazer uma requisi��o com metodo GET para solicitar dados ao servidor.
 * <LI>Fazer uma requisi��o com metodo POST para enviar dados ao servidor.
 * <LI>Pegar a resposta de cada POST efetuado.
 * </OL>
 * @version 14/08/01
 * @see #openRequest
 * @author Fernando Patelli - Microsiga Software SA
 */
public class DWHttp {
    
    BufferedReader	m_datain;
    OutputStreamWriter	m_dataout;
    URL 		m_url;
    URLConnection	m_conn;
    String		m_response;
    
    public DWHttp(URL url) {
        m_url = url;
    }

    public boolean openRequest(String method, String session) {
        
        forceGarbageCollector();

        return this.openRequest(method, session, null);
    }

    public boolean openRequest(String method, String session, String sdata) {
        method = (method==null ? "" : method.toUpperCase().trim());
        if( (!method.equals("GET") && !method.equals("POST")) )
            return false;
        
        try {
            m_conn = m_url.openConnection();
            m_conn.setRequestProperty("Cookie", "SESSIONID=" + session);
            
            if (sdata != null) {
                m_conn.setRequestProperty("Cookie", "cbase64=" + sdata);
            }
            
            if(method.equals("POST")) {
                m_conn.setDoOutput(true);
                m_dataout = new OutputStreamWriter(m_conn.getOutputStream());
            } else {
                m_conn.setDoInput(true);
                m_datain = new BufferedReader(new InputStreamReader(m_conn.getInputStream()));
            }

            return true; // ok
        } catch(UnknownServiceException e) {
            System.err.println("Erro no Protocolo: "+e.getMessage());
        } catch(IOException e) {
            System.err.println("Erro de I/O: "+e.getMessage());
        }
        return false; // erro
    }
    
    public boolean write(String data) {
        try {
            m_dataout.write(data, 0 , data.length());
        } catch (IOException ex) {
            System.err.println("Erro no write() Stream(out): " + ex.getMessage());
            ex.printStackTrace();
            return false; // erro
        }

        forceGarbageCollector();

        return true; // ok
    }
    
    public String read() {
        int i;
        char[] c = new char[1024];
        String data = "";
        try {
            while(m_datain.ready()) {
                i = m_datain.read(c, 0, c.length);
                data += new String(c, 0, i);
            }
        } catch (IOException ex) {
            System.err.println("Erro no read() pela Stream(out): " + ex.getMessage());
            ex.printStackTrace();
            return null; // erro
        }

        forceGarbageCollector();

        return data; // ok
    }
    
    public boolean closeRequest() {
        try {
            if(m_dataout!=null) {
                m_dataout.close();
                
                InputStream is = m_conn.getInputStream();
                if (is != null) {
                    m_datain  = new BufferedReader(new InputStreamReader(is));
                    m_response = read();
                }
            }
            if(m_datain!=null) {
                m_datain.close();
            }
        } catch (IOException ex) {
            System.err.println("Erro no close() das Streams(in/out): " + ex.getMessage());
            ex.printStackTrace();
            return false; // erro
        }
        
        forceGarbageCollector();

        return true; //ok
    }
    
    public String getResponse() {

        forceGarbageCollector();

        return(m_response);
    }

    private void forceGarbageCollector(){
        System.gc();
    }
}
