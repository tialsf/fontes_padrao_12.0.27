package br.com.microsiga.sigadw.applet;

public class DWJavaVersion extends java.applet.Applet
{
    public String OSName;
    public String Vendor;
    public String Version;
    public String OSArch;
    public String OSVersion;
    
    public void start()
    {
        Vendor = System.getProperty("java.vendor");
        Version = System.getProperty("java.version");
        OSArch = System.getProperty("os.arch");
        OSName = System.getProperty("os.name");
        OSVersion = System.getProperty("os.version");
    }
}
