#INCLUDE "OGA820.ch"
#include "protheus.ch"
#include "fwmbrowse.ch"
#include "fwmvcdef.ch"

#DEFINE _CRLF CHR(13)+CHR(10)

/*/{Protheus.doc} OGA820()
Rotina para cadastro de Plano de Vendas
@type  Function
@author tamyris.g
@since 27/06/2018
@version 1.0
@param param, param_type, param_descr
@return return, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Function OGA820()

Alert("Aviso. Essa rotina est� descontinuada.")

Return( Nil )

/*/{Protheus.doc} MenuDef()
Fun��o que retorna os itens para constru��o do menu da rotina
@type  Function
@author tamyris.g
@since 27/06/2018
@version 1.0
@param param, param_type, param_descr
@return return, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function MenuDef()
Local aRotina := {}

Alert("Aviso. Essa rotina est� descontinuada.")

Return( aRotina )


/*/{Protheus.doc} ModelDef()
Fun��o que retorna o modelo padrao para a rotina
@type  Function
@author tamyris.g
@since 27/06/2018
@version 1.0
@param param, param_type, param_descr
@return return, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ModelDef()
Local oModel := MPFormModel():New( "OGA920", , {| oModel | PosModelo( oModel ) } )

Alert("Aviso. Essa rotina est� descontinuada.")

Return( oModel )

/*/{Protheus.doc} ViewDef()
Fun��o que retorna a view para o modelo padrao da rotina
@type  Function
@author tamyris.g
@since 27/06/2018
@version 1.0
@param param, param_type, param_descr
@return return, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ViewDef()

Local oView    := FWFormView():New()

Alert("Aviso. Essa rotina est� descontinuada.")

Return( oView )

/*/{Protheus.doc} fDesMoe
	Fun��o para trazer a descri��o da moeda
	@type  Function
	@author Christopher.miranda
	@since 12/11/2018
	@version 1.0
	@param nMoeda, numerico, codigo moeda
	@return cDesMoe, Caracter, descri��o moed
/*/
Function OGA820DMOE(nMoeda)

	cDesMoe := SuperGetMv("MV_MOEDA"+AllTrim(Str(nMoeda,2)))

Return(cDesMoe)


