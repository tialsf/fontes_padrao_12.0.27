#Include 'protheus.ch'
#Include 'parmtype.ch'
#Include "RestFul.CH"

/*/{Protheus.doc} OGAWSUSER
Defini��o da estrutura do webservice
Ir� buscar o usuario com base no email informado, caso o usuario n�o exista retorna Falso
@PARAM CODEMAIL = Email do usuario cadastrado no protheus
Exemplo TRUE:
Return {.T.,"felipe.mendes"}
Exemplo Falso:
Return {.F.,""Usuario n�o encontrado com o e-mail ..."}
@author Felipe Mendes
@since 23/05/2018
@type class
/*/
WSRESTFUL OGAWSUSER DESCRIPTION "Servi�o REST para Autentica��o usuario AGRO"
			
WSDATA CODEMAIL As String //String que vamos receber via URL
 
WSMETHOD GET DESCRIPTION "Retorna usuario Protheus" WSSYNTAX "/OGAWSUSER || /OGAWSUSER/{CODEMAIL}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL

/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author Felipe Mendes
@since 23/05/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE CODEMAIL WSSERVICE OGAWSUSER

//--> Recuperamos o produto informado via URL 
//--> Podemos fazer dessa forma ou utilizando o atributo ::aUrlParms, que � um array com os par�metros recebidos via URL (QueryString)
Local cCodUSer := Self:CODEMAIL
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAGetUser( cCodUSer, @oResponse) //Autentica o usuario

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSFILTR
Defini��o da estrutura do webservice 
Ir� retornas os valores para filtro na integra��o
@PARAM cUSER   = Usuario protheus
@PARAM NFILTRO = Codigo do filtro (1-Cliente,2-portos) 
@author Felipe Mendes
@since 23/05/2018
@type class
/*/
WSRESTFUL OGAWSFILTR DESCRIPTION "Servi�o REST para Cria��o Filtro AGRO"

WSDATA cUSER As String //Usuario Protheus
WSDATA cFILTRO As String //Codigo do FIltro
 
WSMETHOD GET DESCRIPTION "Retorna dados para filtro " WSSYNTAX "/OGAWSFILTR || /OGAWSFILTR/{cUSER,nFILTRO}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author Felipe Mendes
@since 23/05/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER, cFILTRO WSSERVICE OGAWSFILTR

Local cCodUSer  := Self:cUSER
Local cFilt      := Self:cFILTRO
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

If cFilt == "1" //Clientes
	OGAWSFIl01(cCodUSer,@oResponse)
	
ElseIf cFilt == "2" //Portos
	OGAWSFIl02( @oResponse)
	
ElseIf cFilt == "3" //Armazem
	OGAWSFIl03( @oResponse)

ElseIf cFilt == "4" //Transportadoras
	OGAWSFIl04( @oResponse)

EndIf

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSINSEMB
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta da IE
@PARAM cCodIne   = Instru��o de Embarque
@author tamyris.g
@since 30/05/2018
@type class
/*/
WSRESTFUL OGAWSINSEMB DESCRIPTION "Servi�o REST para Consulta Instru��o de Embarque"

WSDATA cUSER As String //Usuario Protheus
WSDATA cCodIne As String //Codigo da Instru��o de Embarque
 
WSMETHOD GET DESCRIPTION "Retorna dados da IE " WSSYNTAX "/OGAWSINSEMB || /OGAWSINSEMB/{cUSER, cCodIne}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 30/05/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER, cCODINE WSSERVICE OGAWSINSEMB

Local cCodUSer  := Self:cUSER
Local cCodIne  := Self:cCODINE
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSGETIE(cCodUSer, cCodIne, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSTAKEUP
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Take-Up
@PARAM cUser = Usu�rio
@author tamyris.g
@since 18/06/2018
@type class
/*/
WSRESTFUL OGAWSTAKEUP DESCRIPTION "Servi�o REST para Consulta do Take-Up"

WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados da Reserva/Take-UP " WSSYNTAX "/OGAWSTAKEUP || /OGAWSTAKEUP/{cUSER}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 18/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER WSSERVICE OGAWSTAKEUP

Local cCodUSer  := Self:cUSER
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETTKP(cCodUSer, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSTAKEUP
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Take-Up Sin�tico
@PARAM cUser = Usu�rio
@author thiago.rover
@since 19/06/2018
@type class
/*/
WSRESTFUL OGAWSTKPSIN DESCRIPTION "Servi�o REST para Consulta do Take-Up Sin�tico"

WSDATA cCODRES As String //Codigo da reserva
WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Take-Up - Sint�tico " WSSYNTAX "/OGAWSTKPSIN || /OGAWSTKPSIN/{cCODRES}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author thiago.rover
@since 19/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cCODRES,cUSER WSSERVICE OGAWSTKPSIN
Local cCodUSer  := Self:cUSER
Local cCodURes  := Self:cCODRES
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSTKPSIN(cCodURes, @oResponse, cCodUSer)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSTKPANA
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Take-Up Anal�tico
@PARAM cUser = Usu�rio
@author thiago.rover
@since 19/06/2018
@type class
/*/
WSRESTFUL OGAWSTKPANA DESCRIPTION "Servi�o REST para Consulta do Take-Up Anal�tico"

WSDATA cCODRES As String //C�digo Reserva Protheus
WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Take-Up Anal�tico " WSSYNTAX "/OGAWSTKPANA || /OGAWSTKPANA/{cCODRES}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author thiago.rover
@since 19/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cCODRES,cUSER WSSERVICE OGAWSTKPANA
Local cCodUSer  := Self:cUSER
Local cCodURes  := Self:cCODRES
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSTKPANA(cCodURes, @oResponse, cCodUSer)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSTKPSAI
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Take-Up Saldo a Instruir
@PARAM cUser = Usu�rio
@author thiago.rover
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSTKPSAI DESCRIPTION "Servi�o REST para Consulta do Take-Up Saldo a Instruir"

WSDATA cCODRES As String //C�digo Reserva Protheus
WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Take-Up/Saldo a Instruir " WSSYNTAX "/OGAWSTKPSAI || /OGAWSTKPSAI/{cCODRES}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author thiago.rover
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cCODRES,cUSER WSSERVICE OGAWSTKPSAI
Local cCodUSer  := Self:cUSER
Local cCodURes  := Self:cCODRES
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSTKPSAI(cCodURes, @oResponse, cCodUSer)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSTKDET
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Take-Up Detalhado
@PARAM cCodRes = C�digo da Reserva
@author tamyris.g
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSTKDET DESCRIPTION "Servi�o REST para Consulta do Take-Up Detalhado"

WSDATA cCODRES As String //C�digo Reserva Protheus
WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Take-Up Detalhado " WSSYNTAX "/OGAWSTKDET || /OGAWSTKDET/{cCODRES}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cCODRES,cUSER WSSERVICE OGAWSTKDET
Local cCodUSer  := Self:cUSER
Local cCodURes  := Self:cCODRES
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETKDE(cCodURes, @oResponse, cCodUSer)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSSTAEMB
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta Status de Embarque
@PARAM cUser = Usu�rio
@author tamyris.g
@since 30/06/2018
@type class
/*/
WSRESTFUL OGAWSSTAEMB DESCRIPTION "Servi�o REST para Consulta Status de Embarques"

WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Contrato - Gr�os/MI " WSSYNTAX "/OGAWSSTAEMB || /OGAWSSTAEMB/{cUSER}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 30/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER WSSERVICE OGAWSSTAEMB

Local cCodUSer  := Self:cUSER
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETGMI(cCodUSer, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSTEMTOT
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta Status de Embarque - Totalizador por Filial
@PARAM cUser = Usu�rio
@author tamyris.g
@since 30/06/2018
@type class
/*/
WSRESTFUL OGAWSTEMTOT DESCRIPTION "Servi�o REST para Consulta Status de Embarques - Totalizador por Filial"

WSDATA cCodCTR As String //C�digo do Contrato
 
WSMETHOD GET DESCRIPTION "Retorna Totais por Filial - Contrato Gr�os/MI " WSSYNTAX "/OGAWSTEMTOT || /OGAWSTEMTOT/{cCodCTR}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 30/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cCodCTR WSSERVICE OGAWSTEMTOT

Local cCTR  := Self:cCodCTR
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETSET(cCTR, @oResponse) //Status Embarque Totalizador

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSTEMNF
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta Status de Embarque - Notas Fiscais por Filial
@PARAM cUser = Usu�rio
@author tamyris.g
@since 30/06/2018
@type class
/*/
WSRESTFUL OGAWSTEMNF DESCRIPTION "Servi�o REST para Consulta Status de Embarques - Notas Fiscais por Filial"

WSDATA cFilOri As String //Filial de Origem
WSDATA cCodCTR As String //C�digo do Contrato
WSDATA dDatIni As String   //Faixa Data Emiss�o da NF
WSDATA dDatFim As String   //Faixa Data Emiss�o da NF
 
WSMETHOD GET DESCRIPTION "Retorna NF por Filial - Contrato Gr�os/MI " WSSYNTAX "/OGAWSTEMNF || /OGAWSTEMNF/{cFilOri, cCodCTR, dDatIni, dDatFim }" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author tamyris.g
@since 30/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cFilOri, cCodCTR, dDatIni, dDatFim WSSERVICE OGAWSTEMNF

Local cFilNF := Self:cFilOri
Local cCTR   := Self:cCodCTR
Local dDtIni := Self:dDatIni
Local dDtFim := Self:dDatFim
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETSENF(cFilNF, cCTR, dDtIni, dDtFim, @oResponse) //Status Embarque Notas Fiscais

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSEMBALG
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta Embarque Internos Algod�o
@PARAM cUser = Usu�rio
@author felipe.mendes
@since 04/07/2018
@type class
/*/
WSRESTFUL OGAWSEMBALG DESCRIPTION "Servi�o REST para Consulta Embarque Algod�o Mercado Interno"

WSDATA cUSER As String //Usuario Protheus
 
WSMETHOD GET DESCRIPTION "Retorna dados do Embarque Algod�o Mercado Interno " WSSYNTAX "/OGAWSEMBALG || /OGAWSEMBALG/{cUSER}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 04/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER WSSERVICE OGAWSEMBALG

Local cCodUSer  := Self:cUSER
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSFARDALG(cCodUSer, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSNFVEND
Defini��o da estrutura do webservice
Ir� retornar os dados das Notas Fiscais de Venda
@PARAM cUser = Usu�rio
@author felipe.mendes
@since 04/07/2018
@type class
/*/
WSRESTFUL OGAWSNFVEND DESCRIPTION "Servi�o REST para Consulta das Notas Fiscais de Venda"

WSDATA cNumeroCrt As String //Contrato
 
WSMETHOD GET DESCRIPTION "Retorna dados das Notas Fiscais de Venda" WSSYNTAX "/OGAWSNFVEND || /OGAWSNFVEND/{cNumeroCrt}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 04/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cNumeroCrt WSSERVICE OGAWSNFVEND

Local cNumCrt  := Self:cNumeroCrt
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGANFVENDA(cNumCrt, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSFOBALG
Defini��o da estrutura do webservice
Ir� retornar os dados dos contratos FOB Algodao
@author felipe.mendes
@since 04/07/2018
@type class
/*/
WSRESTFUL OGAWSFOBALG DESCRIPTION "Servi�o REST para Consulta Contrato FOB Algod�o"

WSDATA cUser      As String //Usuario
WSDATA lAlg       as Boolean
 
WSMETHOD GET DESCRIPTION "Retorna dados da Consulta Contrato FOB Algod�o " WSSYNTAX "/OGAWSFOBALG || /OGAWSFOBALG/{cUser}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL

/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 04/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUser,lAlg WSSERVICE OGAWSFOBALG

Local cUsuario    := Self:cUser
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGACTRFOB(cUsuario, @oResponse, Self:lAlg)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSCIFALG
Defini��o da estrutura do webservice
Ir� retornar os dados dos contratos CIF Algodao
@author felipe.mendes
@since 04/07/2018
@type class
/*/
WSRESTFUL OGAWSCIFALG DESCRIPTION "Servi�o REST para Consulta Contrato CIF Algod�o"

WSDATA cUser      As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados da Consulta Contrato CIF Algod�o " WSSYNTAX "/OGAWSCIFALG || /OGAWSCIFALG/{cUser}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL

/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 04/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUser WSSERVICE OGAWSCIFALG

Local cUsuario    := Self:cUser
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGACTRCIF(cUsuario, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSCIFSIN
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Contrato CIF Sin�tico
@PARAM cUser = Usu�rio
@author Felipe.mendes
@since 19/07/2018
@type class
/*/
WSRESTFUL OGAWSCIFSIN DESCRIPTION "Servi�o REST para Consulta dos Contratos CIF Sin�tico"

WSDATA cContrato  As String //Codigo do Contrato
WSDATA cItem      As String //Codigo do Item de Contrato
WSDATA cSequencia As String //Sequencia do item do contrato
WSDATA cCodigoIE  As String //Codigo da Instru��o
WSDATA cUser      As String //Usuario

WSMETHOD GET DESCRIPTION "Retorna dados do Contratos CIF - Sint�tico " WSSYNTAX "/OGAWSCIFSIN || /OGAWSCIFSIN/{cContrato,cItem,cSequencia,cCodigoIE,cUser}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author Felipe.mendes
@since 19/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cContrato,cItem,cSequencia,cCodigoIE WSSERVICE OGAWSCIFSIN

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETCIF(Self:cContrato , ;
           Self:cItem     , ;
           Self:cSequencia, ;
           Self:cCodigoIE , ;
           @oResponse     , ;
           Self:cUser       )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSCIFANA
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Contrato CIF Analitico
@PARAM cUser = Usu�rio
@author Felipe.mendes
@since 19/07/2018
@type class
/*/
WSRESTFUL OGAWSCIFANA DESCRIPTION "Servi�o REST para Consulta dos Contratos CIF Analitico"

WSDATA cContrato  As String //Codigo do Contrato
WSDATA cItem      As String //Codigo do Item de Contrato
WSDATA cSequencia As String //Sequencia do item do contrato
WSDATA cCodigoIE  As String //Codigo da Instru��o
WSDATA cUser      As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados do Contratos CIF - Sint�tico " WSSYNTAX "/OGAWSCIFANA || /OGAWSCIFANA/{cContrato,cItem,cSequencia,cCodigoIE,cUser }" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author Felipe.mendes
@since 19/07/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cContrato,cItem,cSequencia,cCodigoIE,cUser WSSERVICE OGAWSCIFANA

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWSCIFANA(Self:cContrato , ;
           Self:cItem     , ;
           Self:cSequencia, ;
           Self:cCodigoIE, ;
           @oResponse     , ;
           Self:cUser      )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSCIFDE
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta do Contrato CIF Detalhado

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSCIFDE DESCRIPTION "Servi�o REST para Consulta do Contrato CIF Detalhado"

WSDATA cContrato  As String //Codigo do Contrato
WSDATA cItem      As String //Codigo do Item de Contrato
WSDATA cSequencia As String //Sequencia do item do contrato
WSDATA cCodigoIE  As String //Codigo da Instru��o
WSDATA cUser      As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados do Contrato CIF Detalhado " WSSYNTAX "/OGAWSCIFDE || /OGAWSCIFDE/{cContrato,cItem,cSequencia,cCodigoIE,cUser}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cContrato,cItem,cSequencia,cCodigoIE,cUser WSSERVICE OGAWSCIFDE

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETCDE(Self:cContrato , ;
           Self:cItem     , ;
           Self:cSequencia, ;
           Self:cCodigoIE,  ;
           @oResponse    ,  ;   
           Self:cUser )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*/{Protheus.doc} OGAWSAUTOR
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta dos Autoriza��o

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSAUTOR DESCRIPTION "Servi�o REST para Consulta do Autoriza��o"

WSDATA cUSER      As String //Usuario
WSDATA cRetorno   As String 
WSDATA cCodFil     As String OPTIONAL
WSDATA cCodAut      As String OPTIONAL
WSDATA cCoditem      As String OPTIONAL

WSMETHOD GET DESCRIPTION "Retorna dados do Autoriza��o " WSSYNTAX "/OGAWSAUTOR || /OGAWSAUTOR/{cUser, cCodFil, cCodAut, cCoditem}" //Disponibilizamos um m�todo do tipo GET
WSMETHOD PUT DESCRIPTION "Grava dados do Autoriza��o "   WSSYNTAX "/OGAWSAUTOR || /OGAWSAUTOR/{cRetorno}" //Disponibilizamos um m�todo do tipo GET
WSMETHOD POST Fardo DESCRIPTION " Retorna os Fardos da Autoriza��o  "    PATH "/bales" WSSYNTAX "/OGAWSAUTOR/bales/"		  PRODUCES APPLICATION_JSON
WSMETHOD PUT Fardo DESCRIPTION " Grava os Fardos da Autoriza��o  "       PATH "/bales" WSSYNTAX "/OGAWSAUTOR/bales/"		  PRODUCES APPLICATION_JSON
WSMETHOD POST Filtro DESCRIPTION " Retorna dados do Autoriza��o conforme filtro enviado no body  "   PATH "/filter" WSSYNTAX "/OGAWSAUTOR/filter/{cUser}" PRODUCES APPLICATION_JSON
WSMETHOD POST Blocos DESCRIPTION " Retorna os Blocos da Autoriza��o  "    PATH "/blocks" WSSYNTAX "/OGAWSAUTOR/blocks/"		  PRODUCES APPLICATION_JSON
WSMETHOD PUT Blocos DESCRIPTION " Grava os Blocos da Autoriza��o  "       PATH "/blocks" WSSYNTAX "/OGAWSAUTOR/blocks/"		  PRODUCES APPLICATION_JSON
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUSER, cCodFil, cCodAut, cCoditem WSSERVICE OGAWSAUTOR

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETATU(Self:cUSER   , ;
           @oResponse   , ;
		   Self:cCodFil , ;
		   Self:cCodAut , ; 
		   Self:cCodItem    )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)
/*/{Protheus.doc} POST Fardo OGAWSAUTOR
Retorna os Fardos da Autoriza��o
@author felipe.mendes
@since 30/01/2019
@version undefined
@type Method
/*/

WSMETHOD POST Fardo  WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())

	OGAWAUTFAR(@oRequest,  @oResponse )

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} PUT Fardo OGAWSAUTOR
Retorna os Fardos da Autoriza��o
@author felipe.mendes
@since 30/01/2019
@version undefined
@type Method
/*/

WSMETHOD PUT Fardo  WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())

	OGAWPUTFAR(@oRequest,  @oResponse )

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)
/*/{Protheus.doc} PUT
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
oResquest:= 
{
    "content": [
        {
            "Agend": [
				{
                    "Operation": "INSERT/DELETE/UPDATE",
                    "BranchId": NJJ->NJJ_FILIAL,
                    "Scheduling": NJJ->NJJ_CODROM,
                    "CodeAut": N8N->N8N_CODIGO,
                    "ItemCode": N8O->N8O_ITEM ,
                    "ApptmtDt": NJJ->NJJ_DTAGEN ,
                    "ApptmtTm": NJJ->NJJ_HRAGEN ,
                    "Plate": NJJ->NJJ_PLACA,
					"uf": NJJ->NJJ_PLACA,
					"AuthorizQty": NJJ->NJJ_QTDAUT
                }
            ]
        }
    ]
}

/*/

WSMETHOD PUT WSRECEIVE cRetorno WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTATU( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} POST Fardo OGAWSAUTOR
Retorna dados do Autoriza��o conforme filtro enviado no body
@author felipe.mendes
@since 30/01/2019
@version undefined
@type Method
/*/

WSMETHOD POST Filtro WSRECEIVE cUSER  WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())

	OGAWATUFIL(Self:cUSER   , @oRequest,  @oResponse )

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} POST
Processa as informa��es e retorna o json com os blocos de uma autoriza��o
@author claudineia.reinert	
@since 11/03/2019
@version undefined
@type Method
/*/
WSMETHOD POST Blocos  WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())

	OGAWAUTBLC(@oRequest,  @oResponse )

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} POST
Processa as informa��es e grava os blocos de uma autoriza��o
@author claudineia.reinert	
@since 11/03/2019
@version undefined
@type Method
/*/
WSMETHOD PUT Blocos  WSSERVICE OGAWSAUTOR
	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())

	OGAWPUAUBLC(@oRequest,  @oResponse )

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSMINUT
//TODO Descri��o auto-gerada.
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSMINUT DESCRIPTION "Servi�o REST para Grava��o de Processo de Minuta"

WSDATA cJsonMinut   As String //Usuario
 
WSMETHOD PUT DESCRIPTION "Grava dados do Processo de Minuta "   WSSYNTAX "/OGAWSMINUT || /OGAWSMINUT/{cJsonMinut}" //M�todo Put para grava��o de dados
 
END WSRESTFUL

/*{Protheus.doc} PUT
Fun��o para Controle da Grava��o de dados da Minuta
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type function
oResquest:= 
{
    "content": [
        {         
	        "Operation": "INSERT/DELETE/UPDATE",
	        "BranchId" : NJR->NJJ_FILIAL,
	        "Contrato" : NJR->NJR_CODCTR,
	        "Process"  : NJR->NJR_PROMTA,
	        "Status"   : NJR->NJR_STSMIN           
        }
    ]
}
*/
WSMETHOD PUT WSRECEIVE cJsonMinut WSSERVICE OGAWSMINUT
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTMIN( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSAGEND
Defini��o da estrutura do webservice
Ir� retornas os valores para consulta dos Agendamentos

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSAGEND DESCRIPTION "Servi�o REST para Consulta do Agendamento e Monitoramento"

WSDATA cUser      As String //Usuario
WSDATA cRetorno   As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados do Agendamento e Monitoramento " WSSYNTAX "/OGAWSAGEND || /OGAWSAGEND/{cUser}" //Disponibilizamos um m�todo do tipo GET
WSMETHOD PUT DESCRIPTION "Grava dados do Agendamento e Monitoramento"   WSSYNTAX "/OGAWSAGEND || /OGAWSAGEND/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUser WSSERVICE OGAWSAGEND

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETAGD(Self:cUser , ;
           @oResponse         )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} PUT
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
oResquest:= 
{
    "content": [
        {
            "MONITOR": [
				{
                    "PackingCode": 
                    "Time": 
                    "Date": 
                    "StateCod": 
                    "CityCode": 
                    "user": 
                    "Latitude": 
                    "Longitude": 
                }
            ]
        }
    ]
}

/*/

WSMETHOD PUT WSRECEIVE cRetorno WSSERVICE OGAWSAGEND
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTAGD( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)
/********************************/

/*/{Protheus.doc} OGAWSCITY
Defini��o da estrutura do webservice
Ir� retornas todas as cidades para o fluig

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSCITY DESCRIPTION "Servi�o REST para Consulta de Cidades"

WSMETHOD GET DESCRIPTION "Retorna dados da cidade para FLUIG " WSSYNTAX "/OGAWSCITY || /OGAWSCITY/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSSERVICE OGAWSCITY

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETCID(    @oResponse         )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/******************************************************/

/*/{Protheus.doc} OGAWSCNNF
Defini��o da estrutura do webservice
Consulta referente a valida��o de dados de Notas e Sinistros

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSCNNF DESCRIPTION "Servi�o REST para Valida��o de Notas e Sinistros"

WSMETHOD PUT DESCRIPTION "Retorna TRUE ou FALSE para informa��es  " WSSYNTAX "/OGAWSCNNF || /OGAWSCNNF/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} PUT
Processa as informa��es e retorna o json 
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD PUT WSSERVICE OGAWSCNNF

	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGAWSCONNF( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSCTRFX
Define estrutura de web service de contrato � Fixar
@author jean.schulze
@since 22/10/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSCTRFX DESCRIPTION "Servi�o REST para Consulta de Contratos � Fixar"

WSDATA cEmail      As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados do Contrato " WSSYNTAX "/OGAWSCTRFX || /OGAWSCTRFX/{cEmail}" //Disponibilizamos um m�todo do tipo GET

END WSRESTFUL


/*{Protheus.doc} GET
Obtem os contratos para o usu�rio
@author jean.schulze
@since 24/10/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail WSSERVICE OGAWSCTRFX

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETCTR(Self:cEmail , ;
           @oResponse         )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSCTRFX
Obtem as cad�ncias do contrato para o usu�rio
@author jean.schulze
@since 22/10/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSCTRCD DESCRIPTION "Servi�o REST para Consulta de Contratos � Fixar"

WSDATA cEmail    As String //Usuario
WSDATA cContrato As String //Usuario
 
WSMETHOD GET DESCRIPTION "Retorna dados do Contrato " WSSYNTAX "/OGAWSCTRCD || /OGAWSCTRCD/{cEmail}{cContrato}" //Disponibilizamos um m�todo do tipo GET

END WSRESTFUL


/*{Protheus.doc} GET
Obtem as cad�ncias do contrato para o usu�rio
@author jean.schulze
@since 24/10/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail, cContrato WSSERVICE OGAWSCTRCD

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETCAD(Self:cEmail     , ;
		   Self:cContrato , ;	
           @oResponse         )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSFXCAL
Calculo de pre�o de uma fixa��o
@author jean.schulze
@since 30/10/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSFXCAL DESCRIPTION "Servi�o REST para Consulta Calcular Pre�o de Uma Fixa��o"

WSDATA cEmail       As String 
WSDATA cContrato   As String 
WSDATA cChavePrev  As String 
WSDATA nQtdFixacao As Numeric 
WSDATA nVlrBolsa   As Decimal  
WSDATA nVlrBasis   As Decimal  

 
WSMETHOD GET DESCRIPTION "Retorna valores da Fixa��o." WSSYNTAX "/OGAWSFXCAL || /OGAWSFXCAL/{cEmail}{cContrato}{cChavePrev}{nQtdFixacao}{nVlrBolsa}{nVlrBasis}" //Disponibilizamos um m�todo do tipo GET

END WSRESTFUL


/*{Protheus.doc} GET
Calcula o valor de uma fixa��o
@author jean.schulze
@since 24/10/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail, cContrato, cChavePrev, nQtdFixacao, nVlrBolsa, nVlrBasis WSSERVICE OGAWSFXCAL

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGTFXCL(Self:cEmail        , ;
		   Self:cContrato    , ;	
		   Self:cChavePrev   ,;
		   Self:nQtdFixacao  ,;
		   Self:nVlrBolsa    ,;
		   Self:nVlrBasis    ,;
           @oResponse         )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSFIXAC
Rest para cria��o de Fixa��o
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSFIXAC DESCRIPTION "Servi�o REST para Fixa��o de Contrato"

WSDATA cEmail       As String 
WSDATA cContrato   As String 
WSDATA cProcess    As String 
 
WSMETHOD PUT DESCRIPTION "Grava dados da Fixa��o de Contrato"   WSSYNTAX "/OGAWSFIXAC/{cEmail}{cContrato}{cProcess}" //M�todo Put para grava��o de dados
 
END WSRESTFUL

/*{Protheus.doc} PUT
Fun��o para Controle da Grava��o de dados da Fixa��o
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type function
oResquest:= 
{
    "content": [
        {         
	        "Bolsa"        : Valor Bolsa,
	        "Basis"        : Valor Basis,
	        "Quantidade"   : N7A->N7A_QTDINT
	        "ChavePrev"    : NNY->NNY_FILIAL+NNY_CODCTR+NNY_ITEM
	        "OrdemFixacao" : N7A->N7A_TIPORD
        }
    ]
}
*/
WSMETHOD PUT WSRECEIVE cEmail, cContrato, cProcess WSSERVICE OGAWSFIXAC
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTFIX( Self:cEmail, ;
	 			Self:cContrato, ;
	 			Self:cProcess, ;
	 			oRequest , ;
	 			@oResponse )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSFIXOK
Rest para cria��o de Fixa��o
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSFIXOK DESCRIPTION "Servi�o REST para aprova��o de Fixa��o de Contrato"

WSDATA cEmail      As String 
WSDATA cContrato   As String 
WSDATA cProcess    As String 
WSDATA cTask       As String 
WSDATA cOption     As String 

 
WSMETHOD PUT DESCRIPTION "Grava dados da Aprova��o de Fixa��o de Contrato"   WSSYNTAX "/OGAWSFIXOK/{cEmail}{cContrato}{cProcess}{cOption}{cTask}" //M�todo Put para grava��o de dados
 
END WSRESTFUL

/*{Protheus.doc} PUT
Fun��o para Controle da Grava��o de dados da Fixa��o
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD PUT WSRECEIVE cEmail, cContrato, cProcess, cTask, cOption  WSSERVICE OGAWSFIXOK
	Local oResponse 	:= JsonObject():New()
    	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")

	OGWSPUTFOK( Self:cEmail, ;
	 			Self:cContrato, ;
	 			Self:cProcess, ;
	 			Self:cTask, ;
	 			Self:cOption, ;
	 			@oResponse )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSPNLIE
Defini��o da estrutura do webservice
Ir� retornar os dados dos itens de entrega do contrato para instruir
@author claudineia.reinert
@since 30/10/2018
@type class
/*/
WSRESTFUL OGAWSPNLIE DESCRIPTION "Servi�o REST para Consultar do itens de entrega do contrato para instruir"

WSDATA cEmail      As String //Usuario
WSDATA cOper       As String //Opera��o do PUT (A=Aprova��o;U=Update)
WSDATA cCampo      As String OPTIONAL
WSDATA cParam        As String OPTIONAL

WSMETHOD GET DESCRIPTION "Retorna dados dos itens de entrega do contrato para instruir " WSSYNTAX "/OGAWPNLIE/{cEmail}" //Disponibilizamos um m�todo do tipo GET
WSMETHOD GET Consulta DESCRIPTION " Retorna a consulta com base em parametro "   PATH "/Consulta" WSSYNTAX "/OGAWSPNLIE/Consulta{cCampo}/ || /OGAWSPNLIE/Consulta{cCampo}{cParam}		  PRODUCES APPLICATION_JSON

WSMETHOD PUT DESCRIPTION "Inclui ou Altera uma Instru��o de Embarque"   WSSYNTAX "/OGAWSPNLIE/{cEmail} || /OGAWSPNLIE/{cEmail}{cOper}" //M�todo Put para grava��o de dados
WSMETHOD POST Blocos DESCRIPTION "Retorna uma lista dos blocos a carregar" 	     PATH "/blocks" 		  PRODUCES APPLICATION_JSON

END WSRESTFUL

/*/{Protheus.doc} GET Consultas OGAWSPNLIE
Retorna as consultas da aba de exporta��o
@author felipe.mendes
@since 30/01/2019
@version undefined
@type Method
/*/

WSMETHOD GET Consulta WSRECEIVE cCampo,cParam  WSSERVICE OGAWSPNLIE
Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWIECONS(Self:cCampo,  @oResponse, Self:cParam )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} GET OGAWSPNLIE
Processa as informa��es e retorna o json
@author claudineia.reinert
@since 30/10/2018
@version undefined
@type Method
/*/
WSMETHOD GET WSRECEIVE cEmail WSSERVICE OGAWSPNLIE

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGAWGETDIE(Self:cEmail ,  @oResponse )

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

WSMETHOD PUT WSRECEIVE cEmail, cOper WSSERVICE OGAWSPNLIE
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")

	oRequest:fromJson(::GetContent())
		OGAWPUTIE( Self:cEmail, ;
				   Self:cOper, ;	
		 		   oRequest , ;
		 		   @oResponse )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

WSMETHOD POST Blocos WSRECEIVE cOper  WSSERVICE OGAWSPNLIE
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	oRequest:fromJson(::GetContent())
	
	OGAWIEBLC(  oRequest , ;
				@oResponse )	

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)
/*/{Protheus.doc} OGAWSCONCNT
//TODO REST para consulta de containers no FLUIG
@author claudineia.reinert
@since 22/11/2018
@version 1.0
@type class
/*/
WSRESTFUL OGAWSCONCNT DESCRIPTION "Servi�o REST para Consulta dos containers"

	WSMETHOD GET DESCRIPTION "Retorna dados para a consulta de containers " WSSYNTAX "/OGAWSCONCNT/" //Disponibilizamos um m�todo do tipo POST para buscar os dados
	WSMETHOD POST DESCRIPTION "Retorna dados para a consulta de containers " WSSYNTAX "/OGAWSCONCNT/" //Disponibilizamos um m�todo do tipo POST para buscar os dados
	WSMETHOD PUT DESCRIPTION "Grava dados do container na consulta de containers " WSSYNTAX "/OGAWSCONCNT/" //Disponibilizamos um m�todo do tipo PUT para grava��o de dados
 
END WSRESTFUL

/*/{Protheus.doc} POST
//TODO POST para buscar dados conforme parametros enviados no corpo da requisi��o para a consulta de containers no FLUIG
@author claudineia.reinert
@since 22/11/2018
@version 1.0
@type function
/*/
WSMETHOD POST WSSERVICE OGAWSCONCNT

	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGAWGCONCNT( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} GET
//TODO GET apenas para teste do servi�o de REST no Fluig para a consulta de containers
@author claudineia.reinert
@since 22/11/2018
@version 1.0
@type function
/*/
WSMETHOD GET WSSERVICE OGAWSCONCNT

	Local oResponse 	:= JsonObject():New()
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oResponse := "Requisi��o GET apenas para teste do servi�o!"
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} PUT
//TODO PUT para inclus�o/altera��o/estufagem/certifica��o container
@author claudineia.reinert
@since 27/11/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
WSMETHOD PUT WSSERVICE OGAWSCONCNT

	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGAWPUTCNT( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSDOCIE
//TODO Descri��o auto-gerada.
@author jean.schulze
@since 29/11/2018
@version 1.0
@return ${return}, ${return_description}

@type class
*/
WSRESTFUL OGAWSDOCIE DESCRIPTION "Servi�o REST para Inser��o de Documentos da IE"

WSDATA cProcess As String //Processo Pai

WSMETHOD PUT DESCRIPTION "Inser��o de Documentos da IE" WSSYNTAX "/OGAWSDOCIE || /OGAWSDOCIE/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL

/*{Protheus.doc} PUT
//TODO Descri��o auto-gerada.
@author jean.schulze
@since 29/11/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD PUT WSSERVICE OGAWSDOCIE

	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	//obtem os dados do content
	oRequest:fromJson(::GetContent())
	
	OGWSPUTDOC( Self:cProcess , ;
				oRequest      , ;
	            @oResponse     )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSCDOWN
Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSCDOWN DESCRIPTION "Servi�o REST para Consulta de Download"

WSDATA cEmail As String //Usuario Protheus

WSMETHOD GET DESCRIPTION "Retorna documentos da IE " WSSYNTAX "/OGAWSCDOWN || /OGAWSCDOWN/{cEmail}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*{Protheus.doc} GET
GET Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail WSSERVICE OGAWSCDOWN

Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

OGWSGETDOW(Self:cEmail, @oResponse)

// define o tipo de retorno do m�todo
::SetContentType("application/json")

::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*/{Protheus.doc} OGAWSPERMIS
Defini��o da estrutura do webservice
Ir� retornar as permiss�es do usuario para acesso a informa��es

@author felipe.mendes
@since 21/06/2018
@type class
/*/
WSRESTFUL OGAWSPERMIS DESCRIPTION "Servi�o REST para Consulta de regras de qualidade"

WSDATA cUser      As String //Usuario
WSDATA cTpRelat   As String //Tipo de relatorio ( 1=TAKE UP ; 2= CIF )

WSMETHOD GET DESCRIPTION "Retorna dados de regras de qualidade " WSSYNTAX "/OGAWSPERMIS || /OGAWSPERMIS/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*/{Protheus.doc} GET
Processa as informa��es e retorna o json
@author felipe.mendes
@since 21/06/2018
@version undefined
@type Method
/*/

WSMETHOD GET WSRECEIVE cUser, cTpRelat WSSERVICE OGAWSPERMIS

	Local oResponse 	:= JsonObject():New() 	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	OGAWPERMIS(  Self:cUser, ;
				 Self:cTpRelat, ;
	             @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))
	
Return(.T.)

/*/{Protheus.doc} OGAWSAPCIE
//TODO REST para Aprova��o da certifica��o de Peso na instru��o de embarque
@author gustavo.pereira
@since 06/12/2018
@version 1.0
@type class
/*/
WSRESTFUL OGAWSAPCIE DESCRIPTION "Servi�o REST de Workflow para aprova��o da certifica��o de peso na instru��o de embarque
	
	WSDATA cProcess As String //Usuario Protheus

	WSMETHOD GET DESCRIPTION "Retorna dados da instru��o de embarque " WSSYNTAX "/OGAWSAPRIE/ || /OGAWSAPRIE/{cProcess}/ " //Disponibilizamos um m�todo do tipo POST para buscar os dados
	WSMETHOD PUT DESCRIPTION "Grava a aprova��o da certifica��o de peso na instru��o de embarque " WSSYNTAX "/OGAWSAPRIE/" //Disponibilizamos um m�todo do tipo PUT para grava��o de dados
 
END WSRESTFUL


/*{Protheus.doc} GET
//TODO GET Servi�o REST para retorno de dados da instru��o de embarque para o FLUIG
@author gustavo.pereira
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cProcess WSSERVICE OGAWSAPCIE

	Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON

	OGWSGAPCIE(Self:cProcess, @oResponse)

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")

	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} PUT
//TODO PUT servi�o REST para gravar a aprova��o da certifica��o de peso realizado pelo FLUIG
@author gustavo.pereira
@since 29/11/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD PUT WSSERVICE OGAWSAPCIE

	Local oResponse 	:= JsonObject():New()
	Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	//obtem os dados do content
	oRequest:fromJson(::GetContent())
	
	OGWSPAPCIE( oRequest      , ;
	            @oResponse     )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSIEAT
Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSIEAT DESCRIPTION "Servi�o REST para Consulta de Download"

WSDATA cEmail As String //Usuario Protheus

WSMETHOD GET DESCRIPTION "Retorna documentos da IE " WSSYNTAX "/OGAWSIEAT || /OGAWSIEAT/{cEmail}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*{Protheus.doc} GET
GET Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail WSSERVICE OGAWSIEAT

	Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON
	
	OGWSGTIEAT(Self:cEmail, @oResponse)
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)


/*{Protheus.doc} OGAWSIEIT
Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSIEIT DESCRIPTION "Servi�o REST para Consulta de Download"

WSDATA cEmail 	  As String
WSDATA cCodIEmbar As String

WSMETHOD GET DESCRIPTION "Retorna documentos da IE " WSSYNTAX "/OGAWSIEIT || /OGAWSIEIT/{cEmail}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*{Protheus.doc} GET
GET Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD GET WSRECEIVE cEmail, cCodIEmbar WSSERVICE OGAWSIEIT

	Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON
	
	OGWSGIEN7S(Self:cEmail, Self:cCodIEmbar, @oResponse)
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSIEIT
Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSAUTIE DESCRIPTION "Servi�o REST para Consulta de Download"

WSDATA cCodIEmbar As String

WSMETHOD PUT DESCRIPTION "Retorna documentos da IE " WSSYNTAX "/OGAWSIEIT || /OGAWSIEIT/{cEmail}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*{Protheus.doc} GET
GET Servi�o REST para Consulta de Download
@author jean.schulze
@since 04/12/2018
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD PUT WSRECEIVE cCodIEmbar WSSERVICE OGAWSAUTIE
	Local oResponse   := JsonObject():New() //Objeto de resposta para padr�o JSON
	Local oRequest    := JsonObject():New()   
	
	//obtem os dados do content
	oRequest:fromJson(::GetContent())
	
	//cria as autorizacoes de embarque
	OGWSPUTCAR(Self:cCodIEmbar, oRequest, @oResponse)

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSENTREG
Servi�o REST para Registro de Entrega - AGRO
@author felipe.mendes
@since 31/01/2019
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSENTREG DESCRIPTION "Servi�o REST para Registro de Entrega - AGRO"

WSMETHOD PUT DESCRIPTION "Registra a chegada do Romaneio " WSSYNTAX "/OGAWSENTREG || /OGAWSENTREG/{}" //Disponibilizamos um m�todo do tipo GET
 
END WSRESTFUL


/*{Protheus.doc} PUT
@author felipe.mendes
@since 31/01/2019
@version 1.0
@return ${return}, ${return_description}
@type function
*/
WSMETHOD PUT WSSERVICE OGAWSENTREG
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTREG( oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)

/*{Protheus.doc} OGAWSMINUT
//TODO Descri��o auto-gerada.
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type class
*/
WSRESTFUL OGAWSAPNGC DESCRIPTION "Servi�o REST para Grava��o de Aprova��o de Neg�cio"

WSDATA cProcess    As String
 
WSMETHOD PUT DESCRIPTION "Grava dados do Processo de Minuta "   WSSYNTAX "/OGAWSAPNGC || /OGAWSAPNGC/{cProcess}" //M�todo Put para grava��o de dados
 
END WSRESTFUL

/*{Protheus.doc} PUT
Fun��o para Controle da Grava��o de dados da Minuta
@author jean.schulze
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@type function

*/
WSMETHOD PUT WSRECEIVE cProcess WSSERVICE OGAWSAPNGC
	Local oResponse 	:= JsonObject():New()
    Local oRequest      := JsonObject():New()   	
	
	// define o tipo de retorno do m�todo
	::SetContentType("application/json")
	
	oRequest:fromJson(::GetContent())
	
	OGWSPUTNGC( Self:cProcess , ;
				oRequest , ;
	            @oResponse         )
	
	::SetResponse(EncodeUTF8(FWJsonSerialize(oResponse, .F., .F., .T.)))

Return(.T.)



