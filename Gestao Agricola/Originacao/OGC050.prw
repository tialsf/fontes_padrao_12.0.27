#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "TOTVS.CH"
#include 'OGC050.ch'

/*{Protheus.doc} OGC050
Exibe as Fixa��es de Componente e de Pre�o para Determinado Contrato
@author jean.schulze
@since 12/10/2017
@version undefined
@param pcFilial, , descricao
@param pcCodCtr, , descricao
@param pcCodCad, , descricao
@type function
*/
Function OGC050(pcFilial, pcCodCtr, pcCodCad)
	Local aArea		  := GetArea()
	Local aCoors      := FWGetDialogSize( oMainWnd )
	Local oSize       := {}
	Local oFWL        := ""
	Local oDlg		  := Nil
	Local oPnl1       := Nil
	Local oPnlWnd1    := Nil
	Local oPnlWnd2	  := Nil 
	Local aButtons    := {}
	
	Local oBrowse1  := Nil
	Local oBrowse2	 := Nil
		
	Default pcFilial := ""
	Default pcCodCtr := ""
	Default pcCodCad := ""
	
	//tamanho da tela principal
	oSize := FWDefSize():New(.t.) //considerar o enchoice
	oSize:AddObject('DLG',100,100,.T.,.T.)
	oSize:SetWindowSize(aCoors)
	oSize:lProp 	:= .T.
	oSize:aMargins := {0,0,0,0}
	oSize:Process()
	
	oDlg := MsDialog():New(  oSize:aWindSize[1], oSize:aWindSize[2], oSize:aWindSize[3], oSize:aWindSize[4], STR0001, , , , , CLR_BLACK, CLR_WHITE, , , .t. ) //Consulta Blocos e Fardos
	
	oPnl1:= tPanel():New(oSize:aPosObj[1,1],oSize:aPosObj[1,2],,oDlg,,,,,,oSize:aPosObj[1,4],oSize:aPosObj[1,3] -30)
        
	// Instancia o layer
	oFWL := FWLayer():New()

	// Inicia o Layer
	oFWL:init( oPnl1, .F. )
	
	// Cria as divis�es horizontais
	oFWL:addLine('TOP'   , 50, .F.)
	oFWL:addLine('BOTTOM', 50, .F.)
	oFWL:addCollumn( 'CENTRAL-TOP', 100,.F., 'TOP' )
	oFWL:addCollumn( 'CENTRAL-BOT', 100,.F., 'BOTTOM' )
			
	//cria as janelas
	oFWL:addWindow( 'CENTRAL-TOP', 'Wnd1', STR0002,  100 /*tamanho*/, .F., .T.,, 'TOP' )
	oFWL:addWindow( 'CENTRAL-BOT', 'Wnd2', STR0003,  100 /*tamanho*/, .F., .T.,, 'BOTTOM' )
		
	// Recupera os Paineis das divis�es do Layer
	oPnlWnd1:= oFWL:getWinPanel( 'CENTRAL-TOP' , 'Wnd1', 'TOP' )
	oPnlWnd2:= oFWL:getWinPanel( 'CENTRAL-BOT', 'Wnd2', 'BOTTOM' )
	
		
	/****************** Fixa��es de Componente ********************************/
	//adicionando os widgets de tela
	oBrowse1 := FWMBrowse():New()    
    oBrowse1:SetAlias("N7M")
    oBrowse1:DisableDetails()
    oBrowse1:SetMenuDef( "" )    
    oBrowse1:DisableReport(.T.) 
    oBrowse1:SetProfileID("OGC050N7M")
    oBrowse1:SetFilterDefault("N7M_FILIAL = '"+pcFilial+"' .AND. N7M_CODCTR = '"+pcCodCtr+"'" + IIF(!empty(pcCodCad), ".AND. N7M_CODCAD = '"+pcCodCad+"'", ""))
    oBrowse1:SetDoubleClick({||  OGC050VIEW(N7M->N7M_CODNGC, N7M->N7M_VERSAO) }) 
    
    //legenda
    oBrowse1:AddLegend( "N7M_QTDSLD >  0", "GREEN", STR0004) 	//"Com Saldo"
	oBrowse1:AddLegend( "N7M_QTDSLD == 0", "GRAY" , STR0005) 	//"Sem Saldo"
	
    oBrowse1:Activate(oPnlWnd1)   

	
	/****************** Fixa��es de Pre�o ********************************/
	oBrowse2 := FWMBrowse():New()
    oBrowse2:DisableReport(.T.) 
    oBrowse2:DisableDetails()
    oBrowse2:SetAlias("NN8")
    oBrowse2:SetMenuDef( "" )
    oBrowse2:DisableReport(.T.) 
    oBrowse2:SetProfileID("OGC050NN8") 
    oBrowse2:SetFilterDefault("NN8_FILIAL = '"+pcFilial+"' .AND. NN8_CODCTR = '"+pcCodCtr+"'" + IIF(!empty(pcCodCad), ".AND. NN8_CODCAD = '"+pcCodCad+"'", ""))
    oBrowse2:SetDoubleClick({|| OGC050VIEW(NN8->NN8_CODNGC, NN8->NN8_VERSAO)})
    
    //legenda			
	oBrowse2:AddLegend( "NN8_QTDFIX >  NN8_QTDENT  .AND. NN8_QTDAGL == 0 ", "GREEN", STR0006) 	//Fixa��o com Saldo � Entregar
	oBrowse2:AddLegend( "NN8_QTDFIX <= NN8_QTDENT  .AND. NN8_QTDENT  > 0 ", "GRAY", STR0007) 	//Fixa��o Entregue
	oBrowse2:AddLegend( "NN8_QTDFIX == 0 .AND. NN8_QTDAGL  > 0 ", "BLUE", STR0010)				//Aglutinada
	oBrowse2:AddLegend( "NN8_QTDFIX == 0 .AND. NN8_QTDAGL == 0 ", "BR_CANCEL", STR0011)		//Cancelada
    oBrowse2:Activate(oPnlWnd2)
    
    //cria os bot�es adicionais
    Aadd( aButtons, {STR0008, {|| OGC050VIEW(N7M->N7M_CODNGC, N7M->N7M_VERSAO)}, STR0008, STR0008, {|| .T.}} )     		
    Aadd( aButtons, {STR0009, {|| OGC050VIEW(NN8->NN8_CODNGC, NN8->NN8_VERSAO)}, STR0009, STR0009, {|| .T.}} )     		
            
	oDlg:Activate( , , , .t., , ,  EnchoiceBar(oDlg, , {||  oDlg:End() } /*Fechar*/,,@aButtons,,,.f.,.f.,.f.,.f.,.f.) ) //ativa a tela
	
	N7M->( dbCloseArea() )
	NN8->( dbCloseArea() )
	
	RestArea(aArea)	
return .t.

/*{Protheus.doc} OGC050VIEW
Fun��o para visualizar o neg�cio
@author jean.schulze
@since 12/10/2017
@version undefined
@param cCodNgc, characters, descricao
@param cVersao, characters, descricao
@type function
*/
Static Function OGC050VIEW(cCodNgc, cVersao)
	
	DbSelectArea("N79")
	DbSetOrder(1)
	
	If DbSeek(xFilial("N79")+cCodNgc+cVersao)
		OGA700VISU() // fun��o de visualiza��o do OGA700
	endif
	
return .t.
