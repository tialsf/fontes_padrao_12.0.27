#INCLUDE "OGC180.CH" 
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

Static __oArqTemp  := Nil	//Objeto retorno da tabela

/*/{Protheus.doc} OGC180
//Analise de Exposi��o de Contratos Futuros
@author carlos.augusto/marcelo.ferrari
@since 09/11/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
function OGC180(cProduto)
	Local aFilBrowCtr   := {}
    Local cFiltroDef 	:= ""
	Local nCont         := 0
	Private _cFiltro    := nil
	Private _cTabCtr    := nil
	Private _oBrowse    := nil
	Private _aFieldsQry := nil
    Private _aCpsBrowCt := nil
	Private _cBasis     := ""
    Private _aIdxBrw    := {}

	//-- Prote��o de C�digo
	If .Not. TableInDic('NCS') .OR. .Not. TableInDic('NCS')  .OR. .Not. TableInDic('NCS')
		MsgNextRel() //-- � necess�rio a atualiza��o do sistema para a expedi��o mais recente
		Return()
	Endif

    If !OGC180VLDROTINA()    
        Return()
    EndIf
    
	//atalho de pesquisa
	SetKey( VK_F12, { || OGC180F12() } )
	//Pergunta para selecionar o componente BASIS.
	_cBasis := OGC180PERG(.F.)
	If Empty(_cBasis)
	   OGC180PERG(.t.)
	EndIf

	//CAMPOS DA TABELA TEMPOR�RIA
    _aFieldsQry := { ;        
		/*"�ndice"           */ {STR0016                 , "INDICE"        ,TamSX3("NK0_INDICE")[3] ,TamSX3("NK0_INDICE")[1]  ,TamSX3("NK0_INDICE")[2]  ,PesqPict("NK0","NK0_INDICE")}  ,;   //
        /*NK0_DATVEN         */ {AgrTitulo("NK0_DATVEN") , "DATVENCTO"     ,TamSX3("NK0_DATVEN")[3] ,TamSX3("NK0_DATVEN")[1]  ,TamSX3("NK0_DATVEN")[2]  ,PesqPict("NK0","NK0_DATVEN")}  ,;   //
		/*NCS_BOLSA          */ {AgrTitulo("NCS_BOLSA")  , "BOLSA"         ,TamSX3("NCS_BOLSA")[3]  ,TamSX3("NCS_BOLSA")[1]   ,TamSX3("NCS_BOLSA")[2]   ,PesqPict("NCS","NCS_BOLSA")}   ,;      //
        /*NK0_VMESAN         */ {AgrTitulo("NK0_VMESAN") , "VCTMESAN"      ,TamSX3("NK0_VMESAN")[3] ,TamSX3("NK0_VMESAN")[1]  ,TamSX3("NK0_VMESAN")[2]  ,PesqPict("NK0","NK0_VMESAN")}  ,;      //
 		/*NCS_SAFRA          */ {AgrTitulo("NCS_SAFRA")  , "SAFRA"         ,TamSX3("NCS_SAFRA")[3]  ,TamSX3("NCS_SAFRA")[1]   ,TamSX3("NCS_SAFRA")[2]   ,PesqPict("NCS","NCS_SAFRA")}   ,;      //
		/*"Qtd.Ctr.Futuro"   */ {STR0004                 , "QTDCTRFUT"     ,TamSX3("NCS_QTDE")[3]   ,TamSX3("NCS_QTDE")[1]    ,TamSX3("NCS_QTDE")[2]    ,PesqPict("NCS","NCS_QTDE")}    ,;     //Qtd Ctr Futuro" 
	 	/*"Vlr.Med.Prod/Ctr" */ {STR0005	             , "VLRMEDCTFT"    ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")}  ,;     //Vlr Md. Prod/Ctr" 
		/*"Qtd.Vol.Ctr.Fut"  */ {STR0006	             , "TTQTDCTR"      ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")}  ,;     //Qtde TT. Contrato
		/*"Vlr.Tt.Ctr.Fut"   */ {STR0007	             , "VLRTOTCTFT"    ,TamSX3("NCS_VALOR")[3]  ,TamSX3("NCS_VALOR")[1]   ,TamSX3("NCS_VALOR")[2]   ,PesqPict("NCS","NCS_VALOR")}  ,;     //Total Contrato" 
		/*NK8_CODPRO         */ {AgrTitulo("NK8_CODPRO") , "CODPRODUTO"    ,TamSX3("NK8_CODPRO")[3] ,TamSX3("NK8_CODPRO")[1]  ,TamSX3("NK8_CODPRO")[2]  ,PesqPict("NK8","NK8_CODPRO")}  ,;      //
		/*B1_DESC            */ {AgrTitulo("B1_DESC")    , "DESCPRODUT"    ,TamSX3("B1_DESC")[3]    ,TamSX3("B1_DESC")[1]     ,TamSX3("B1_DESC")[2]     ,PesqPict("SB1","B1_DESC")}     ,;      //
		/*N7C_UMPROD         */ {AgrTitulo("N7C_UMPROD") , "UNMEDPROD"     ,TamSX3("N7C_UMPROD")[3] ,TamSX3("N7C_UMPROD")[1]  ,TamSX3("N7C_UMPROD")[2]  ,PesqPict("N7C","N7C_UMPROD")}  ,;      //
		/*N8U_QTDCTR         */ {AgrTitulo("N8U_QTDCTR") , "QTDXCTRFUT"    ,TamSX3("N8U_QTDCTR")[3] ,TamSX3("N8U_QTDCTR")[1]  ,TamSX3("N8U_QTDCTR")[2]  ,PesqPict("N8U","N8U_QTDCTR")}  ,;      //
		/*"Un.Med.Ctr.Fut"   */ {STR0008                 , "UNMEDCTRFT"    ,TamSX3("N8U_UMCTR")[3]  ,TamSX3("N8U_UMCTR")[1]   ,TamSX3("N8U_UMCTR")[2]   ,PesqPict("N8U","N8U_UMCTR")}   ,;       //
		/*N7C_CODCOM         */ {AgrTitulo("N7C_CODCOM") , "COMPONENTE"    ,TamSX3("N7C_CODCOM")[3] ,TamSX3("N7C_CODCOM")[1]  ,TamSX3("N7C_CODCOM")[2]  ,PesqPict("N7C","N7C_CODCOM")}  ,; //
        /*NK7_DESCRI         */ {AgrTitulo("NK7_DESCRI") , "NOMECOMP"      ,TamSX3("NK7_DESCRI")[3] ,TamSX3("NK7_DESCRI")[1]  ,TamSX3("NK7_DESCRI")[2]  ,PesqPict("NK7","NK7_DESCRI")}  ,; //
		/*N7C_UMCOM          */ {AgrTitulo("N7C_UMCOM")  , "UNMEDCOMP "    ,TamSX3("N7C_UMCOM")[3]  ,TamSX3("N7C_UMCOM")[1]   ,TamSX3("N7C_UMCOM")[2]   ,PesqPict("N7C","N7C_UMCOM")}   ,;       //
 		/*"Cod. Moeda"       */ {STR0009                 , "CODMOEDA"      ,TamSX3("NK0_MOEDA")[3]  ,TamSX3("NK0_MOEDA")[1]  ,TamSX3("NK0_MOEDA")[2]    ,PesqPict("NK0","NK0_MOEDA")}   ,; //
		/*"Moeda"            */ {STR0010                 , "MOEDA"         ,"C"                     ,10                       ,0                        , "@!" }                        ,; //
		/*N7C_QTDCTR         */ {AgrTitulo("N7C_QTDCTR") , "QTCTNEGOCI"    ,TamSX3("N7C_QTDCTR")[3] ,TamSX3("N7C_QTDCTR")[1]  ,TamSX3("N7C_QTDCTR")[2]  ,PesqPict("N7C","N7C_QTDCTR")}  ,; //
		/*N7C_QTAFIX         */ {AgrTitulo("N7C_QTAFIX") , "QTDNEGOCIO"    ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")}  ,; //
		/*TTVLRFIX           */ {AgrTitulo("TTVLRFIX")   , "TTVLRFIX"      ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")}  ,;
        /*"BASIS"            */ {STR0011                 , "COMPBASIS"     ,TamSX3("N7C_CODCOM")[3] ,TamSX3("N7C_CODCOM")[1]  ,TamSX3("N7C_CODCOM")[2]  ,PesqPict("N7C","N7C_CODCOM")}  ,;
		/*"Un.Med.Basis"     */ {STR0013                 , "UNMEDBASIS"    ,TamSX3("N7C_UMCOM")[3]  ,TamSX3("N7C_UMCOM")[1]   ,TamSX3("N7C_UMCOM")[2]   ,PesqPict("N7C","N7C_UMCOM")}   ,;       //
		/*"Vlr.Med.Basis"    */ {STR0012                 , "VLRMEDBASI"    ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")}  ,;       //
		/*"Qtd.Ctr.a.Fixar"  */ {STR0020                 , "QTDCTRAFIX"    ,TamSX3("NCS_QTDE")[3]   ,TamSX3("NCS_QTDE")[1]    ,TamSX3("NCS_QTDE")[2]    ,PesqPict("NCS","NCS_QTDE")}    ,;       //
		/*"Qtd.Vol.a.Fixar"  */ {STR0022                 , "QTDCOMPFIX"    ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")}  ,;       //
		/*"Sld.Ctr.L.Hedge"  */{STR0021                  , "QTDCTRSHED"    ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")}  ,;       //
        /*"Sld.Vol.L.Hedge"  */{STR0023                  , "QTDCOMSHED"    ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")}  ,;       //
        /*"Vlr.Med.Ctr.S/.Basis" */ {STR0024             , "VLCTFUTBAS"    ,TamSX3("N7C_VLRCOM")[3] ,18                       ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")}   ;       //
        }     //
	
	//CAMPOS DO GRID
    _aCpsBrowCt := { ;        
 		/*"Safra"            */ {AgrTitulo("NCS_SAFRA")  , "SAFRA"         ,TamSX3("NCS_SAFRA")[3]  ,TamSX3("NCS_SAFRA")[1]   ,TamSX3("NCS_SAFRA")[2]   ,PesqPict("NCS","NCS_SAFRA")}  ,;      //
		/*"�ndice"           */ {STR0016                 , "INDICE"        ,TamSX3("NK0_INDICE")[3] ,TamSX3("NK0_INDICE")[1]  ,TamSX3("NK0_INDICE")[2]  ,PesqPict("NK0","NK0_INDICE")} ,;   //
		/*"Qtd.Ctr.Futuro"   */ {STR0004                 , "QTDCTRFUT"     ,TamSX3("NCS_QTDE")[3]   ,TamSX3("NCS_QTDE")[1]    ,TamSX3("NCS_QTDE")[2]    ,PesqPict("NCS","NCS_QTDE")}   ,;     //Qtd Ctr Futuro" 
        /*"Qtd.Ctr.a.Fixar"  */ {STR0020                 , "QTDCTRAFIX"    ,TamSX3("NCS_QTDE")[3]   ,TamSX3("NCS_QTDE")[1]    ,TamSX3("NCS_QTDE")[2]    ,PesqPict("NCS","NCS_QTDE")}   ,;       //
		/*"Sld.Ctr.L.Hedge"  */ {STR0021                 , "QTDCTRSHED"    ,TamSX3("NCS_QTDE")[3]   ,TamSX3("NCS_QTDE")[1]    ,TamSX3("NCS_QTDE")[2]    ,PesqPict("NCS","NCS_QTDE")}   ,;       //
		/*"Qtd.Vol.Ctr.Fut"  */ {STR0006	             , "TTQTDCTR"      ,TamSX3("N7C_QTAFIX")[3] ,TamSX3("N7C_QTAFIX")[1]  ,TamSX3("N7C_QTAFIX")[2]  ,PesqPict("N7C","N7C_QTAFIX")} ,;     //Qtde TT. Contrato
		/*"Qtd.Vol.a.Fixar"  */ {STR0022                 , "QTDCOMPFIX"    ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")} ,;       //
        /*"Sld.Vol.L.Hedge"  */ {STR0023                 , "QTDCOMSHED"    ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")} ,;       //
		/*"Un.Med.Ctr.Fut"   */ {STR0008                 , "UNMEDCTRFT"    ,TamSX3("N8U_UMCTR")[3]  ,TamSX3("N8U_UMCTR")[1]   ,TamSX3("N8U_UMCTR")[2]   ,PesqPict("N8U","N8U_UMCTR")}  ,;       //
	 	/*"Vlr.Med.Prod/Ctr" */ {STR0005	             , "VLRMEDCTFT"    ,TamSX3("NCS_VALOR")[3]  ,TamSX3("NCS_VALOR")[1]   ,TamSX3("NCS_VALOR")[2]   ,PesqPict("NCS","NCS_VALOR")} ,;     //Vlr Md. Prod/Ctr" 
		/*"Vlr.Med.Ctr.S/.Basis*/{STR0024                , "VLCTFUTBAS"    ,TamSX3("NCS_VALOR")[3]  ,TamSX3("NCS_VALOR")[1]   ,TamSX3("NCS_VALOR")[2]   ,PesqPict("NCS","NCS_VALOR")} ,;       //
		/*"Un.Med.Comp."     */ {STR0025                 , "UNMEDCOMP "    ,TamSX3("N7C_UMCOM")[3]  ,TamSX3("N7C_UMCOM")[1]   ,TamSX3("N7C_UMCOM")[2]   ,PesqPict("N7C","N7C_UMCOM")}  ,;       //
        /*"Vlr.Tt.Ctr.Fut"   */ {STR0007	             , "VLRTOTCTFT"    ,TamSX3("NCS_VALOR")[3]  ,TamSX3("NCS_VALOR")[1]   ,TamSX3("NCS_VALOR")[2]   ,PesqPict("NCS","NCS_VALOR")} ,;     //Total Contrato" 
 		/*"Moeda"            */ {STR0010                 , "MOEDA"         ,"C"                     ,10                       ,0                        , "@!" }                       ,; //
		/*"BASIS"            */ {STR0011                 , "COMPBASIS"     ,TamSX3("N7C_CODCOM")[3] ,TamSX3("N7C_CODCOM")[1]  ,TamSX3("N7C_CODCOM")[2]  ,PesqPict("N7C","N7C_CODCOM")} ,;
		/*"Vlr.Med.Basis"    */ {STR0012                 , "VLRMEDBASI"    ,TamSX3("N7C_VLRCOM")[3] ,TamSX3("N7C_VLRCOM")[1]  ,TamSX3("N7C_VLRCOM")[2]  ,PesqPict("N7C","N7C_VLRCOM")} ,;       //
		/*"Un.Med.Basis"     */ {STR0013                 , "UNMEDBASIS"    ,TamSX3("N7C_UMCOM")[3]  ,TamSX3("N7C_UMCOM")[1]   ,TamSX3("N7C_UMCOM")[2]   ,PesqPict("N7C","N7C_UMCOM")}  ,;       //
		/*NK8_CODPRO         */ {AgrTitulo("NK8_CODPRO") , "CODPRODUTO"    ,TamSX3("NK8_CODPRO")[3] ,TamSX3("NK8_CODPRO")[1]  ,TamSX3("NK8_CODPRO")[2]  ,PesqPict("NK8","NK8_CODPRO")} ,;      //
		/*B1_DESC            */ {AgrTitulo("B1_DESC")    , "DESCPRODUT"    ,TamSX3("B1_DESC")[3]    ,TamSX3("B1_DESC")[1]     ,TamSX3("B1_DESC")[2]     ,PesqPict("SB1","B1_DESC")}    ,;      //
		/*N7C_UMPROD         */ {AgrTitulo("N7C_UMPROD") , "UNMEDPROD"     ,TamSX3("N7C_UMPROD")[3] ,TamSX3("N7C_UMPROD")[1]  ,TamSX3("N7C_UMPROD")[2]  ,PesqPict("N7C","N7C_UMPROD")} ,;      //
		/*N8U_QTDCTR         */ {AgrTitulo("N8U_QTDCTR") , "QTDXCTRFUT"    ,TamSX3("N8U_QTDCTR")[3] ,TamSX3("N8U_QTDCTR")[1]  ,TamSX3("N8U_QTDCTR")[2]  ,PesqPict("N8U","N8U_QTDCTR")} ,;      //
		/*N7C_CODCOM         */ {AgrTitulo("N7C_CODCOM") , "COMPONENTE"    ,TamSX3("N7C_CODCOM")[3] ,TamSX3("N7C_CODCOM")[1]  ,TamSX3("N7C_CODCOM")[2]  ,PesqPict("N7C","N7C_CODCOM")} ,; //
        /*NK7_DESCRI         */ {AgrTitulo("NK7_DESCRI") , "NOMECOMP"      ,TamSX3("NK7_DESCRI")[3] ,TamSX3("NK7_DESCRI")[1]  ,TamSX3("NK7_DESCRI")[2]  ,PesqPict("NK7","NK7_DESCRI")} ,; //
        /*NK0_DATVEN         */ {AgrTitulo("NK0_DATVEN") , "DATVENCTO"     ,TamSX3("NK0_DATVEN")[3] ,TamSX3("NK0_DATVEN")[1]  ,TamSX3("NK0_DATVEN")[2]  ,PesqPict("NK0","NK0_DATVEN")} ,;   //
		/*NCS_BOLSA          */ {AgrTitulo("NCS_BOLSA")  , "BOLSA"         ,TamSX3("NCS_BOLSA")[3]  ,TamSX3("NCS_BOLSA")[1]   ,TamSX3("NCS_BOLSA")[2]   ,PesqPict("NCS","NCS_BOLSA")}  ,;      //
        /*NK0_VMESAN         */ {AgrTitulo("NK0_VMESAN") , "VCTMESAN"     ,TamSX3("NK0_VMESAN")[3]  ,TamSX3("NK0_VMESAN")[1]  ,TamSX3("NK0_VMESAN")[2]  ,PesqPict("NK0","NK0_VMESAN")}  ;      //
        }     //

	aIdx    := { {"", "INDICE"}, {"", "DATVENCTO"}, {"", "BOLSA"}, {"", "VCTMESAN"}, {"", "CODPRODUTO"}, {"", "COMPONENTE"} }	
    aAdd(_aIdxBrw,STR0027) //"�ndice"
    aAdd(_aIdxBrw,STR0028) //"Dat. Vencimento"
    aAdd(_aIdxBrw,STR0029) //"Bolsa"
    aAdd(_aIdxBrw,STR0030) //"M�s/Ano"
    aAdd(_aIdxBrw,STR0031) //"C�d. Produto"
    aAdd(_aIdxBrw,STR0032) //"Componente"
    Processa({|| _cTabCtr := MontaTabel(_aFieldsQry, aIdx )},STR0002)//"Construindo layout da tela."
	Processa({|| fLoadDados(_cFiltro)},STR0003) //"Carregando a tabela de dados."

	//Criando o Browser de Visualiza��o
	_oBrowse := FWMBrowse():New()
    _oBrowse:SetAlias(_cTabCtr)
    _oBrowse:SetDescription( STR0001 )//An�lise de Exposi��o de Contratos Futuros
    if FWIsInCallStack("OGA700")
        cFiltroDef 	:= iIf( !Empty( cProduto ), "CODPRODUTO ='"+cProduto+"'", "" )
        _oBrowse:SetFilterDefault( cFiltroDef )
    endif
    _oBrowse:DisableDetails()
    _oBrowse:SetMenuDef( "OGC180" ) //verifica para coloca as op��es de menu aqui - fixar, contrato, etc.
    _oBrowse:SetProfileID("OGC180BRW1")
	
    For nCont := 1  to Len(_aCpsBrowCt) //desconsiderar STATUS e Tipo
        If !_aCpsBrowCt[nCont][2] $ "NCS_FILIAL|NCS_CODIGO" 
        	_oBrowse:AddColumn( {_aCpsBrowCt[nCont][1]  , &("{||"+_aCpsBrowCt[nCont][2]+"}") ,_aCpsBrowCt[nCont][3],_aCpsBrowCt[nCont][6],iif(_aCpsBrowCt[nCont][3] == "N",2,1),_aCpsBrowCt[nCont][4],_aCpsBrowCt[nCont][5],.f.} )
        EndIf
        If !_aCpsBrowCt[nCont][2] $ "NCS_FILIAL|NCS_CODIGO" 
	       	aADD(aFilBrowCtr,  {_aCpsBrowCt[nCont][2], _aCpsBrowCt[nCont][1], _aCpsBrowCt[nCont][3], _aCpsBrowCt[nCont][4], _aCpsBrowCt[nCont][5], _aCpsBrowCt[nCont][6] } )
       	EndIf
        
    Next nCont

    _oBrowse:SetFieldFilter(aFilBrowCtr)
    _oBrowse:Activate()

Return .T.

Static Function fMntFiltro()
	Local cFiltro := ""
	cFiltro +=  " "
return cFiltro


/*/{Protheus.doc} Pergunta FKEY-F12 - OGC180F12
@author carlos.augusto/marcelo.ferrari
@since 09/11/2018
@version 1.0
@return ${return}, ${return_description}
@param aCpsBrow, array, descricao
@param aIdxTab, array, descricao
@type function
/*/
Function OGC180F12()
   OGC180PERG(.T.)
	//-- Carrega dados e cria arquivos tempor�rios
	//Processa( { || fLoadDados() }, STR0003)
	//_oBrowse:UpdateBrowse()
Return

Function OGC180PERG(lPergunta)
	// Abre a tela de par�metros de perguntas
	Pergunte("OGC1800001", lPergunta )
    _cBasis := MV_PAR01
	_cBasis := MV_PAR01
Return( _cBasis )


/*/{Protheus.doc} MontaTabel
@author carlos.augusto/marcelo.ferrari
@since 09/11/2018
@version 1.0
@return ${return}, ${return_description}
@param aCpsBrow, array, descricao
@param aIdxTab, array, descricao
@type function
/*/
Static Function MontaTabel(aCpsBrow, aIdxTab)
    Local nCont 	:= 0
    Local cTabela	:= ''
	Local aStrTab 	:= {}	//Estrutura da tabela

    //-- Busca no aCpsBrow as propriedades para criar as colunas
    For nCont := 1 to Len(aCpsBrow)
        aADD(aStrTab,{aCpsBrow[nCont][2], aCpsBrow[nCont][3], aCpsBrow[nCont][4], aCpsBrow[nCont][5] })
    Next nCont
   	//-- Tabela temporaria de pendencias
   	cTabela  := GetNextAlias()
   	//-- A fun��o AGRCRTPTB est� no fonte AGRUTIL01 - Fun��es Genericas
    __oArqTemp := AGRCRTPTB(cTabela, {aStrTab, aIdxTab})
    
Return cTabela


/*/{Protheus.doc} fLoadDados
//Carrega os dados da Tabela Tempor�ria
@author carlos.augusto/marcelo.ferrari
@since 09/11/2018
@version 1.0
@return ${return}, ${return_description}
@param cFiltro, characters, descricao
@type function
/*/
Function fLoadDados()
	Local cSql       := ""
	Local cNomeTabela := __oArqTemp:GetRealName()
	Local nVlrUnMedCtr := 0
	Local cDB := TcGetDB()
    Local cAliasQry := GetNextAlias()    
    Local nValor := 0


	If cDb = 'MSSQL'
		cStrDt := "GETDATE()"
	ElseIf cDb =  "ORACLE"
		cStrDt := "TO_CHAR(SYSDATE, 'YYYYMMDD')"
	EndIf

	//limpa a tabela tempor�ria
	DbSelectArea((_cTabCtr))
	ZAP
	     
	//monta po filtro padr�o
	_cFiltro := fMntFiltro() //apropria filtro
	
	cSql := "SELECT NK0.NK0_INDICE       INDICE, " + ;
            "NK0.NK0_DATVEN              DATVENCTO, " + ;
            "NK0.NK0_CODBOL              BOLSA, " + ;
            "NK0.NK0_VMESAN              VCTMESAN, " + ;
            "ISNULL(NCS.NCS_SAFRA , '' ) SAFRA, " + ;
            "ISNULL(NCS.NCS_QTDE , 0 )   QTDCTRFUT, " + ;
            "ISNULL(NCS_VLRMED , 0 )     VLRMEDCTFT, " + ;
            "ISNULL(NCS_VLRTOT , 0 )     VLRTOTCTFT, " + ;
            "SUM(0)                      TTQTDCTR, " + ;
            "NK0.NK0_CODPRO              CODPRODUTO, " + ;
            "ISNULL(B1_DESC, '')         DESCPRODUT, " + ;
            "ISNULL(NK0.NK0_UM1PRO, '')  UNMEDPROD, " + ;
            "ISNULL(N8U_QTDCTR, 0)       QTDXCTRFUT, " + ;
            "ISNULL(N8U_UMCTR, '')       UNMEDCTRFT, " + ;
            "ISNULL(N7C.N7C_CODCOM, '')  COMPONENTE, " + ;
            "ISNULL(NK7_DESCRI, '')      NOMECOMP, " + ;
            "ISNULL(N7C_UMCOM, '')       UNMEDCOMP, " + ;
            "ISNULL(NK0_MOEDA, 0)        CODMOEDA, " + ;
            "''                          MOEDA, " + ;
            "SUM(ISNULL(NNEG.QTCTNEGOCIO, 0))       QTCTNEGOCI, " + ;
            "SUM(ISNULL(NNEG.QTDNEGOCIO, 0))        QTDNEGOCIO, " + ;
            "SUM(0.0)                    TTVLRFIX, " + ;
            "ISNULL(BASIS.BASISCOM, '')  COMPBASIS, " + ;
            "ISNULL(BASIS.UMBASIS, '')   UNMEDBASIS, " + ;
            "ISNULL(BASIS.VLRMEDBASIS,0) VLRMEDBASI, " + ;
            "SUM(ISNULL(NNEG.QTCTNEGOCIO, 0)) - SUM(ISNULL(N7C_QTDCTR, 0)) QTDCTRAFIX, " + ;
            "SUM(ISNULL(NNEG.QTDNEGOCIO, 0))  - SUM(ISNULL(N7C_QTAFIX, 0)) QTDCOMPFIX, " + ;
            "SUM(0)                      QTDCTRSHED, " + ;
            "SUM(0)                      QTDCOMSHED, " + ;
            "(0.0) VLCTFUTBAS " + ;
            "FROM " + RetSqlName("NK0") + " NK0 " + ;
            "LEFT JOIN " + ; 
            " /* busca registros dos contratos de futuros ==>*/" + ;
            "  (SELECT NCS1.NCS_TICKER, " + ;
            "          NCS1.NCS_BOLSA, " + ;
            "          NCS1.NCS_SAFRA, " + ;
            "          NCS1.NCS_MESEMB, " + ;
            "          SUM(NCS1.NCS_QTDE) NCS_QTDE, " + ;
            "          (0.0) NCS_VLRTOT, " + ;
            "          CAST((SUM(NCS1.NCS_VALOR * NCS1.NCS_QTDE) / SUM(NCS1.NCS_QTDE)) AS NUMERIC(18, 6)) NCS_VLRMED, " + ;
            "          NCS1.D_E_L_E_T_ " + ;
            "   FROM " + RetSqlName("NCS") + " NCS1 " + ;
            "   WHERE NCS1.NCS_FILIAL = '" + fwxFilial("NCS") + "' " + ;
            "   GROUP BY NCS1.NCS_TICKER, " + ;
            "            NCS1.NCS_BOLSA, " + ;
            "            NCS1.NCS_SAFRA, " + ;
            "            NCS1.NCS_MESEMB, " + ;
            "            NCS1.D_E_L_E_T_) NCS ON " + ; 
            "   NK0_INDICE = NCS_TICKER AND " + ;
            "   NK0.D_E_L_E_T_ = NCS.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("N7C") + " N7C ON N7C_FILIAL = '" + fwxFilial("N7C") + "' " + ;
            "   AND NCS_TICKER = N7C_CODIDX " + ;
            "   AND NCS.D_E_L_E_T_ = N7C.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("NK7") + " NK7 ON NK7_FILIAL = '" + fwxFilial("NK7") + "' " + ;
            "   AND NK7_CODCOM = N7C.N7C_CODCOM " + ;
            "   AND NK7.D_E_L_E_T_ = N7C.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("NK8") + " NK8 ON NK8_FILIAL = '" + fwxFilial("NK8") + "' " + ;
            "   AND NK8_CODCOM = NK7_CODCOM " + ;
            "   AND NK8_CODIDX = NK0_INDICE " + ;
            "   AND NK8.D_E_L_E_T_ = NK7.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("N8U") + " N8U ON N8U_FILIAL = '" + fwxFilial("N8U") + "' " + ;
            "   AND N8U_CODBOL = NK0_CODBOL " + ;
            "   AND N8U_CODPRO = NK0_CODPRO " + ;
            "   AND N8U.D_E_L_E_T_ <> '*' " + ;
            "LEFT JOIN " + RetSqlName("N7A") + " N7A ON N7A_FILIAL = '" + fwxFilial("N7A") + "' " + ;
            "   AND N7C_CODNGC = N7A_CODNGC " + ;
            "   AND N7C_CODCAD = N7A_CODCAD " + ;
            "   AND N7C.D_E_L_E_T_ = N7A.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("N79") + " N79 ON N79_FILIAL = '" + fwxFilial("N79") + "' " + ;
            "   AND N79_CODNGC = N7A_CODNGC " + ;
            "   AND N79.D_E_L_E_T_ = N7A.D_E_L_E_T_ " + ;
            "LEFT JOIN " + RetSqlName("SB1") + " SB1 ON B1_FILIAL = '" + fwxFilial("SB1") + "' " + ;
            "   AND B1_FILIAL = '' " + ;
            "   AND B1_COD = NK0.NK0_CODPRO " + ;
            "   AND SB1.D_E_L_E_T_ = NK8.D_E_L_E_T_ " + ;
            "LEFT JOIN " + ;
            " /* busca qtd contratos e qtd componente do Novo Neg�cio ==>*/" + ;
            "  (SELECT N79NN.N79_CODCTR NNCODCTR, " + ;
            "          N7CNN.N7C_CODIDX NNCODIDX, " + ;
            "          N7CNN.N7C_BOLSA NNBOLSA, " + ;
            "          N7CNN.N7C_CODCOM NNCODCOM, " + ;
            "          SUM(N7CNN.N7C_QTDCTR) QTCTNEGOCIO, " + ;
            "          SUM(N79NN.N79_QTDNGC) QTDNEGOCIO " + ;
            "   FROM " + RetSqlName("N7C") + " N7CNN " + ;
            "   LEFT JOIN " + RetSqlName("N79") + " N79NN ON N79NN.N79_FILIAL = '" + fwxFilial("N79") + "' " + ;
            "     AND N7CNN.N7C_CODNGC = N79NN.N79_CODNGC " + ;
            "     AND N7CNN.D_E_L_E_T_ = N79NN.D_E_L_E_T_ " + ;
            "   WHERE N79NN.N79_TIPO = '1' " + ;
            "     AND N7CNN.N7C_FILIAL = '" + fwxFilial("SB1") + "' " + ;
            "     AND N79NN.N79_STATUS = '3' " + ;
            "     AND N7CNN.D_E_L_E_T_ = ' ' " + ;
            "   GROUP BY N79NN.N79_CODCTR, " + ;
            "            N7CNN.N7C_CODNGC, " + ;
            "            N7CNN.N7C_CODIDX, " + ;
            "            N7CNN.N7C_BOLSA, " + ;
            "            N7CNN.N7C_CODCOM) NNEG ON N79.N79_CODCTR = NNEG.NNCODCTR " + ;
            "AND N7C.N7C_CODIDX = NNEG.NNCODIDX " + ;
            "AND N7C.N7C_BOLSA = NNEG.NNBOLSA " + ;
            "AND N7C.N7C_CODCOM = NNEG.NNCODCOM " + ;
            "LEFT JOIN " + ;
            " /* busca vlr basis no Novo Neg�cio ==>*/" + ;
            "  (SELECT N7CBAS.N7C_CODCOM BASISCOM, " + ;
            "          N7CBAS.N7C_MOEDCO MOBASIS, " + ;
            "          N7CBAS.N7C_UMCOM UMBASIS, " + ;
            "          N7CCOM.N7C_CODIDX, " + ;
            "          N7CCOM.N7C_CODCOM, " + ;
            "          SUM(N7CBAS.N7C_QTAFIX * N7CBAS.N7C_VLRCOM) / SUM(N7CBAS.N7C_QTAFIX) VLRMEDBASIS   /* Valor m�dio do Basis FIXADO*/ " + ;
            "   FROM " + RetSqlName("N7C") + " N7CBAS " + ;
            "   INNER JOIN " + ;
            "     (SELECT N7CX.N7C_FILIAL || N7CX.N7C_CODNGC || N7CX.N7C_VERSAO || N7CX.N7C_CODCAD CHAVE, " + ;
            "             N7CX.N7C_CODIDX, " + ;
            "             N7CX.N7C_CODCOM " + ;
            "      FROM " + RetSqlName("N7C") + " N7CX " + ;
            "	  INNER JOIN " + RetSqlName("N79") + " N79X ON " + ;
            "	     N7CX.N7C_FILIAL = '" + fwxFilial("N79") + "' AND " + ;
            "		 N7CX.N7C_CODNGC = N79X.N79_CODNGC AND " + ;
            "		 N7CX.N7C_VERSAO = N79X.N79_VERSAO AND " + ;
            "		 N7CX.D_E_L_E_T_ = N79X.D_E_L_E_T_ " + ;
            "      WHERE 1=1 " + ;
            "        AND N7CX.N7C_FILIAL = '" + fwxFilial("N7C") + "' " + ;
            "		 AND N79X.N79_TIPO = '1' " + ;
            "		 AND N79X.N79_STATUS = '3' " + ;
            "        AND N7CX.N7C_FILIAL = '" + fwxFilial("N7C") + "' " + ;
            "        AND N7CX.N7C_QTAFIX > 0 " + ;
            "        AND N7CX.N7C_VLRCOM > 0 " + ;
            "        AND N7CX.D_E_L_E_T_ = ' ' ) N7CCOM ON  " + ;
            "	N7CBAS.N7C_FILIAL || N7CBAS.N7C_CODNGC || N7CBAS.N7C_VERSAO || N7CBAS.N7C_CODCAD = N7CCOM.CHAVE " + ;
            "   WHERE 1=1 " + ;
            "     AND N7CBAS.N7C_FILIAL = '" + fwxFilial("N7C") + "' " + ;
            "     AND N7CBAS.D_E_L_E_T_ = ' ' " + ;
            "     AND N7CBAS.N7C_CODCOM = '"+ _cBasis + "' /*CODIGO COMPONENTE BASIS*/  " + ;
            "     AND N7CBAS.N7C_QTAFIX > 0 " + ;
            "     AND N7CBAS.N7C_VLRCOM <> 0 " + ;
            "     AND N7CBAS.D_E_L_E_T_ = ' ' " + ;
            "   GROUP BY N7CBAS.N7C_CODCOM, " + ;
            "            N7CBAS.N7C_MOEDCO, " + ;
            "            N7CBAS.N7C_UMCOM, " + ;
            "            N7CCOM.N7C_CODIDX, " + ;
            "            N7CCOM.N7C_CODCOM) BASIS ON BASIS.N7C_CODIDX = NCS.NCS_TICKER " + ;
            "     AND BASIS.N7C_CODCOM = N7C.N7C_CODCOM " + ;
            "WHERE 1=1 " + ;
            "  AND NK0.NK0_FILIAL = '" + fwxFilial("NK0") + "' " + ;
            "  AND (NK0.NK0_DATVEN >= " + cStrDt + " OR NK0_DATVEN IS NULL OR NK0_DATVEN = '') " + ;
            "  AND (NK7_CALCUL = 'P' OR NK7_CALCUL IS NULL ) " + ;
            "  AND (NK7_FHEDGE = '1' OR NK7_FHEDGE IS NULL ) " + ;
            "  AND (NK7_BOLSA  = '1' OR NK7_FHEDGE IS NULL ) " + ;
            "  AND (N79_TIPO = '2'  OR N79_TIPO IS NULL ) " + ;
            "  AND (N79_BOLSA = NK0_CODBOL OR N79_BOLSA IS NULL ) " + ;
            "  AND (N79_TIPFIX = '1' OR N79_TIPFIX IS NULL ) " + ;            
            "  AND NK0.D_E_L_E_T_ = ' ' " + ;
            "  AND (( " + cStrDt + " BETWEEN NK8_DATINI AND NK8_DATFIM) OR NK8_DATINI IS NULL OR NK8_DATINI = '' OR NK8_DATFIM IS NULL OR NK8_DATFIM = '' ) " + ;
            "  AND (NCS_QTDE > 0 OR N7C_QTDCTR > 0) " + ;
            "GROUP BY NK0.NK0_INDICE,NK0.NK0_DATVEN,NK0.NK0_CODBOL,NK0.NK0_VMESAN, " + ;
            "         NCS.NCS_SAFRA,NCS.NCS_MESEMB,NCS.NCS_QTDE,NCS_VLRMED,NCS_VLRTOT, " + ;
            "         NK0.NK0_CODPRO,B1_DESC,NK0.NK0_UM1PRO, N8U_QTDCTR,N8U_UMCTR, " + ;
            "         N7C.N7C_CODCOM,NK7_DESCRI,N7C_UMCOM,NK0_MOEDA,NNEG.NNCODIDX, " + ;
            "         NNEG.NNBOLSA,NNEG.NNCODCOM,BASIS.BASISCOM,BASIS.UMBASIS,BASIS.VLRMEDBASIS "


	cSql := ChangeQuery(cSql)
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cSql), cAliasQry, .F., .T.)

    dbSelectArea(cAliasQry)
	(cAliasQry)->(dbGoTop())
	While (cAliasQry)->(!Eof()) 

	    cQryInsert := "INSERT INTO " + cNomeTabela 
        cQryInsert += " (INDICE,DATVENCTO,BOLSA,VCTMESAN,SAFRA,QTDCTRFUT,VLRMEDCTFT,VLRTOTCTFT,TTQTDCTR,CODPRODUTO,DESCPRODUT,UNMEDPROD,QTDXCTRFUT,UNMEDCTRFT,COMPONENTE,NOMECOMP,UNMEDCOMP,CODMOEDA,MOEDA,QTCTNEGOCI,QTDNEGOCIO,TTVLRFIX,COMPBASIS,UNMEDBASIS,VLRMEDBASI,QTDCTRAFIX,QTDCOMPFIX,QTDCTRSHED,QTDCOMSHED,VLCTFUTBAS) " 
        cQryInsert += " VALUES ( " 
        cQryInsert +=  "'" +(cAliasQry)->INDICE                 + "', " 
        cQryInsert +=  "'" +cValToChar((cAliasQry)->DATVENCTO)  + "', " 
        cQryInsert +=  "'" +(cAliasQry)->BOLSA                  + "', " 
        cQryInsert +=  "'" +(cAliasQry)->VCTMESAN               + "', " 
        cQryInsert +=  "'" +(cAliasQry)->SAFRA                  + "', " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDCTRFUT)       + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->VLRMEDCTFT)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->VLRTOTCTFT)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->TTQTDCTR)        + ", " 
        cQryInsert +=  "'" +(cAliasQry)->CODPRODUTO             + "', " 
        cQryInsert +=  "'" +(cAliasQry)->DESCPRODUT             + "', " 
        cQryInsert +=  "'" +(cAliasQry)->UNMEDPROD              + "', " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDXCTRFUT)      + ", " 
        cQryInsert +=  "'" +(cAliasQry)->UNMEDCTRFT             + "', " 
        cQryInsert +=  "'" +(cAliasQry)->COMPONENTE             + "', " 
        cQryInsert +=  "'" +(cAliasQry)->NOMECOMP               + "', " 
        cQryInsert +=  "'" +(cAliasQry)->UNMEDCOMP              + "', " 
        cQryInsert +=  cValToChar((cAliasQry)->CODMOEDA)        + ", " 
        cQryInsert +=  "'" +(cAliasQry)->MOEDA                  + "', "               
        cQryInsert +=  cValToChar((cAliasQry)->QTCTNEGOCI)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDNEGOCIO)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->TTVLRFIX)        + ", " 
        cQryInsert +=  "'" +(cAliasQry)->COMPBASIS              + "', " 
        cQryInsert +=  "'" +(cAliasQry)->UNMEDBASIS             + "', " 
        cQryInsert +=  cValToChar((cAliasQry)->VLRMEDBASI)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDCTRAFIX)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDCOMPFIX)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDCTRSHED)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->QTDCOMSHED)      + ", " 
        cQryInsert +=  cValToChar((cAliasQry)->VLCTFUTBAS)      + " ) "

        //Executa a instru��o de INSERT a partir do SQL, populando a tabela tempor�ria
	    // TCSqlExec(cQryInsert)
         nStatus := TCSqlExec(cQryInsert)
        if (nStatus < 0)
            conout("TCSQLError() " + TCSQLError())
        endif
	    (cAliasQry)->( DbSkip() )


	EndDo
	(cAliasQry)->(dbCloseArea())

	//apropria��o de dados
	DbselectArea( _cTabCtr )
	(_cTabCtr)->(DbGoTop())
	//Faz o c�lculo de convers�o de unidades de medida dos campos
	While (_cTabCtr)->( !Eof() )
		Reclock(_cTabCtr, .F.)
		
        //Calcula a quantidade total na und. medida do contrato de futuro
        (_cTabCtr)->TTQTDCTR := (_cTabCtr)->QTDCTRFUT * (_cTabCtr)->QTDXCTRFUT

		//Quantidade de contratos sem cobertura
        (_cTabCtr)->QTDCTRSHED := (_cTabCtr)->QTDCTRFUT - (_cTabCtr)->QTDCTRAFIX

        //quantidade de contratos sem cobertura na unidade de medida do contrato
        (_cTabCtr)->QTDCOMSHED =  (_cTabCtr)->TTQTDCTR - (_cTabCtr)->QTDCOMPFIX
		//Converter o Valor m�dio Unit�rio da commoditie para valor unit�rio na unidade do contrato
		//Exemplo    Commoditie: 3,65/BU   |   Contrato: 22000Kg

        //VLRMEDCTFT  Valor medio contrato futuro
        //Converter para unidade de medida da unidade do contrato futuro
        //nVlrUnMedCtr := OGX700UMVL( (_cTabCtr)->VLRMEDCTFT, (_cTabCtr)->UNMEDCOMP, (_cTabCtr)->UNMEDCTRFT, (_cTabCtr)->CODPRODUTO ) 
        nVlrUnMedCtr := iif(Empty((_cTabCtr)->VLRMEDCTFT),0,(_cTabCtr)->VLRMEDCTFT)
        nVlrTTCtr := iif(Empty((_cTabCtr)->TTQTDCTR),0,(_cTabCtr)->TTQTDCTR)

        //VLRMEDBASI   Valor medio contrato futuro
        //Converter para unidade de medida da o contrato futuro
        nVlrUnMedBas := OGX700UMVL( (_cTabCtr)->VLRMEDBASI, (_cTabCtr)->UNMEDBASIS, (_cTabCtr)->UNMEDCOMP, (_cTabCtr)->CODPRODUTO ) 

        //Calcula o Valor base para contratos futuros na respectiva unidade de medida
        (_cTabCtr)->VLCTFUTBAS =  iif(Empty(nVlrUnMedCtr),0,nVlrUnMedCtr) + iif(Empty(nVlrUnMedBas),0,nVlrUnMedBas) 

        nVlrMedCFt   := OGX700UMVL( (_cTabCtr)->VLCTFUTBAS, (_cTabCtr)->UNMEDCOMP, (_cTabCtr)->UNMEDCTRFT, (_cTabCtr)->CODPRODUTO )

        //Calcula o valor total dos contratos de Futuro
        nValor := nVlrMedCFt * nVlrTTCtr
        (_cTabCtr)->VLRTOTCTFT := round( nValor , TamSX3("N7C_VLRCOM")[2] )

		//nVlrUnMedPr :=  OGX700UMVL( (_cTabCtr)->VLRQTAXFIX , (_cTabCtr)->N7C_UMCOM , (_cTabCtr)->N7C_UMPROD , (_cTabCtr)->NK8_CODPRO) 
		//(_cTabCtr)->TTVLRFIX    := round( ( nVlrUnMedPr * (_cTabCtr)->N7C_QTAFIX ) , TamSX3("N7C_VLRCOM")[2] )   //;  N7C_QTDCTR * VLRQTAXFIX

        //Busca a descri��o da moeda
        (_cTabCtr)->MOEDA = AGRMVMOEDA( (_cTabCtr)->CODMOEDA )
        
        (_cTabCtr)->(MsUnlock())
		(_cTabCtr)->(dbSkip())
	enddo

Return(.t.)


/*/{Protheus.doc} MenuDef
@author carlos.augusto/marcelo.ferrari
@since 09/11/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function MenuDef()
	Local aRotina := {}

	ADD OPTION aRotina TITLE STR0017   ACTION "OGC180PESQ()"       OPERATION 1 ACCESS 0 //"Pesquisar"	
	
Return aRotina

Function OGC180PESQ()
local oDlgPesq, oOrdem, oChave, oBtOk, oBtCan, oBtPar
Local cOrdem
Local cChave := Space(255)
Local nOrdem := 1
Local nOpcao := 0

DEFINE MSDIALOG oDlgPesq TITLE STR0026 FROM 00,00 TO 100,500 PIXEL //Pesquisa
  @ 005, 005 COMBOBOX oOrdem VAR cOrdem ITEMS _aIdxBrw SIZE 210,08 PIXEL OF oDlgPesq ON CHANGE nOrdem := oOrdem:nAt
  @ 020, 005 MSGET oChave VAR cChave SIZE 210,08 OF oDlgPesq PIXEL
  DEFINE SBUTTON oBtOk  FROM 05,218 TYPE 1 ACTION (nOpcao := 1, oDlgPesq:End()) ENABLE OF oDlgPesq PIXEL
  DEFINE SBUTTON oBtCan FROM 20,218 TYPE 2 ACTION (nOpcao := 0, oDlgPesq:End()) ENABLE OF oDlgPesq PIXEL
  DEFINE SBUTTON oBtPar FROM 35,218 TYPE 5 WHEN .F. OF oDlgPesq PIXEL

ACTIVATE MSDIALOG oDlgPesq CENTER

If nOpcao == 1
  cChave := AllTrim(cChave)
  (_cTabCtr)->(dbSetOrder(nOrdem)) 
  (_cTabCtr)->(dbSeek(cChave))
ElseIf nOpcao == 0
  (_cTabCtr)->(dbSetOrder(1)) 
  (_cTabCtr)->(dbGoTop())
Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} OGC180MSGROTINA
Fun��o para verificar se a rotina de controle de risco est� habilitada.
@author  rafael.voltz
@since   04/12/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Function OGC180VLDROTINA()   
    
    If !SuperGetMv('MV_AGRO041', , .F.) 
        AGRHELP(STR0033, STR0034, STR0035) //"Esta rotina n�o est� habilitada para utiliza��o." "Verifique o par�metro MV_AGRO041."
        return .f.
    EndIf

Return .T.
