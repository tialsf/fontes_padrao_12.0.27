#include "protheus.ch"
#include "ogx220.ch"

/** {Protheus.doc} OGX220
Rotina chamada atrav�s do Pedido de Venda
( MATA410 - nao passa ao incluir  apenas alterar/copia/visualiza)
@author: 	Vanilda Moggio
@since: 	24/03/2015
@Uso: 		SIGAAGR - Gest�o Agr�cola
**/
Function OGX220(cOpcao)
	Local lRetorno 	:= .T.

	If SuperGetMv("MV_AGRORI",,.F.) .AND. !ISINCALLSTACK("OGA300") .And. SC5->(ColumnPos('C5_TRCNUM')) > 0
		If  cOpcao == 'C'
			SC5->C5_TRCNUM := "0"
		ElseIf  cOpcao == 'A'
			If 	!(empty(SC5->C5_TRCNUM)) .or. SC5->C5_TRCNUM != "0"
				dbSelectArea('NKT')
				NKT->( dbSetOrder( 1 ) )
				If dbSeek(fwxfilial("NKT")+SC5->C5_TRCNUM) .and. !(Empty(NKT_NUMPED))
					Help(,, STR0006,,STR0002 , 1, 0 ) //#Ajuda #Pedido de Venda para troca deve ser alterado no Acordo de Troca
					lRetorno 	:= .F.
				Endif
			Endif
		ElseIf  cOpcao == 'E'
			if 	!(empty(SC5->C5_TRCNUM)) .or. SC5->C5_TRCNUM != "0"
				dbSelectArea('NKT')
				NKT->( dbSetOrder( 1 ) )
				If dbSeek(fwxfilial("NKT")+SC5->C5_TRCNUM) .and. !empty(NKT_NUMPED)
					Help(,, STR0006,,STR0003 , 1, 0 ) //#Ajuda #Pedido de Venda para troca deve ser eliminado no Acordo de Troca
					lRetorno 	:= .F.
				endif
			Endif
		ElseIf cOpcao == 'L'
			dbSelectArea('NKT')
			NKT->( dbSetOrder( 1 ) )
			If dbSeek(fwxfilial("NKT")+SC5->C5_TRCNUM) .and. (NKT_STATUS == '01')
				Help(,, STR0006,,STR0004 , 1, 0 ) //#Ajuda #Acordo de troca deve estar liberado
				lRetorno 	:= .F.
			Endif
		Endif
	Endif 

Return( lRetorno )
