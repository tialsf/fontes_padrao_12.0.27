package uba.components;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class JTableZebra extends JTable {

	private static final long serialVersionUID = 5512210210560020728L;

	public Component prepareRenderer(TableCellRenderer renderer, int row,
			int column) {
		Component returnComp = super.prepareRenderer(renderer, row, column);
		Color alternateColor = new Color(230, 230, 250);
		Color whiteColor = Color.WHITE;
		if (!returnComp.getBackground().equals(getSelectionBackground())) {
			Color bg = (row % 2 == 0 ? alternateColor : whiteColor);
			returnComp.setBackground(bg);
			bg = null;
		}
		return returnComp;
	}
}
