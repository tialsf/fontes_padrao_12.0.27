package uba.controller;

import java.util.List;

import uba.dao.ConjuntoDAO;
import uba.exceptions.NegocioException;
import uba.modelo.Conjunto;

public class ConjuntoController {
	protected ConjuntoDAO conjuntoDAO = null;
	protected FardaoController fardaoController;

	public ConjuntoController() {
		conjuntoDAO = new ConjuntoDAO();
	}

	public void salvar(Conjunto conjunto) {
		conjuntoDAO.salvar(conjunto);
	}

	public List<Conjunto> listar() {
		return conjuntoDAO.listarTodos();
	}

	public Conjunto buscar(Conjunto conjunto) throws NegocioException {
		return conjuntoDAO.busca(conjunto);
	}

	public List<Conjunto> buscaPorCodSai(String codSAI) throws NegocioException {
		return conjuntoDAO.buscaPorCodSai(codSAI);
	}

	public boolean verificaSeSaiPertenceConjunto(String codSAI,
			Conjunto conjunto) throws NegocioException {
		boolean codSaiPertence = false;
		List<Conjunto> conjuntos = conjuntoDAO.buscaPorCodSai(codSAI);
		for (Conjunto conjunto2 : conjuntos) {
			codSaiPertence = (conjunto2.getCodConjunto().equals(conjunto
					.getCodConjunto()));
			if (codSaiPertence) break;
		}

		if (codSaiPertence)
			return true;
		else {
			throw new NegocioException(
					"Código SAI não pertence a esse conjunto.");
		}
	}
}
