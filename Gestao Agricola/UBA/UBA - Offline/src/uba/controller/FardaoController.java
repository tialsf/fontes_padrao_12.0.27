package uba.controller;

import java.util.ArrayList;
import java.util.List;

import uba.dao.FardaoDAO;
import uba.modelo.Fardao;
import uba.modelo.Fardo;
import uba.modelo.tipos.StatusFardao;

public class FardaoController {
	private FardaoDAO fardaoDAO = null;

	public FardaoController() {
		fardaoDAO = new FardaoDAO();
	}

	public void calcularPesoFardos(Fardao fardao) {
		Double pesoFardos = new Double(0);
		for (Fardo fardo : fardao.getFardos()) {
			pesoFardos += fardo.getPesoLiquido();
		}
		fardao.setPesoLiquidoFardos(pesoFardos);
	}

	public void calcularQtFardos(Fardao fardao) {
		fardao.setQtd_Fardos(fardao.getFardos().size());
	}

	public void calcularPercentual(Fardao fardao) {
		double pesodivisor = fardao.getPesoLiquido() > 0 ? fardao
				.getPesoLiquido() : fardao.getPesoPrevisto();

		fardao.setPercentual((fardao.getPesoLiquidoFardos() / pesodivisor) * 100);
	}

	public List<Fardao> listar() {
		return fardaoDAO.listarTodos();
	}

	public void salvar(Fardao fardao) {
		fardaoDAO.salvar(fardao);
	}

	public Fardao buscar(Fardao fardao) {
		return fardaoDAO.busca(fardao);
	}

	public List<Fardao> buscaPorStatus(ArrayList<StatusFardao> status) {
		return fardaoDAO.buscaPorStatus(status);
	}

	public void atualizar(Fardao fardao) {
		fardaoDAO.atualiza(fardao);
	}
}
