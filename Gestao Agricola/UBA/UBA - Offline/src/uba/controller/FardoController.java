package uba.controller;

import java.util.List;

import uba.dao.FardoDAO;
import uba.exceptions.NegocioException;
import uba.modelo.Fardao;
import uba.modelo.Fardo;

public class FardoController {
	private FardoDAO fardoDAO = null;
	private FardaoController fardaoController;

	public FardoController() {
		fardoDAO = new FardoDAO();
	}

	public void salvar(Fardo fardo) throws NegocioException {
		verificaFardoCadastrado(fardo);
		fardo.getFardao().getFardinhos().add(fardo);
		fardoDAO.salvar(fardo);
		fardaoController = new FardaoController();
		fardo.getFardao().setConjunto(fardo.getConjunto());
		fardaoController.calcularPesoFardos(fardo.getFardao());
		fardaoController.calcularQtFardos(fardo.getFardao());
		fardaoController.calcularPercentual(fardo.getFardao());
		fardaoController.atualizar(fardo.getFardao());
	}

	public List<Fardo> listar(Fardao fardao) {
		return fardoDAO.listarTodos(fardao);
	}

	public void verificaFardoCadastrado(Fardo fardo) throws NegocioException {
		if (fardoDAO.busca(fardo) != null) {
			throw new NegocioException("Fardo já cadastrado.");
		}
	}
}
