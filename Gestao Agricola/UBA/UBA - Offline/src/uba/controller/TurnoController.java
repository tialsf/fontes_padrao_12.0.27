package uba.controller;

import java.util.List;

import uba.dao.TurnoDAO;
import uba.modelo.Turno;

public class TurnoController {
	protected TurnoDAO TurnoDAO = null;

	public TurnoController() {
		TurnoDAO = new TurnoDAO();
	}

	public void salvar(Turno Turno) {
		TurnoDAO.salvar(Turno);
	}

	public List<Turno> listar() {
		return TurnoDAO.listarTodos();
	}

	public Turno buscar(Turno Turno) {
		return TurnoDAO.busca(Turno);
	}
}
