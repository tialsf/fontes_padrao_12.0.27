package uba.controller;

import java.util.List;

import uba.dao.UsuarioDAO;
import uba.modelo.BalancaDoUsuario;
import uba.modelo.Usuario;

public class UsuarioController {
	private final UsuarioDAO usuarioDAO;

	public UsuarioController() {
		usuarioDAO = new UsuarioDAO();
	}

	public Usuario BuscaUsuario(String login) {
		return usuarioDAO.BuscaUsuario(login);
	}

	public boolean autenticar(String login, String senha) {
		return usuarioDAO.autenticar(login, senha);
	}

	public void salvarSenha(Usuario usuario) {
		usuarioDAO.salvarSenha(usuario);
	}

	public List<BalancaDoUsuario> getBalancasUsauario(Usuario usuario) {
		return usuarioDAO.getBalancasUsauario(usuario);
	}

}
