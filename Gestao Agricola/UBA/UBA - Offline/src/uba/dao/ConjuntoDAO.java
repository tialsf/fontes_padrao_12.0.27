package uba.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import uba.exceptions.NegocioException;
import uba.modelo.Conjunto;
import uba.util.EntityManagerUtil;

public class ConjuntoDAO {

	public void salvar(Conjunto conjunto) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(conjunto);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public void remover(Conjunto conjunto) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(conjunto);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public List<Conjunto> listarTodos() {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Conjunto> query = entityManager.createQuery(
					"From Conjunto", Conjunto.class);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}

		return null;

	}

	public Conjunto busca(Conjunto conjunto) throws NegocioException {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		conjunto = entityManager
				.find(Conjunto.class, conjunto.getCodConjunto());
		if (conjunto == null) {
			throw new NegocioException("Conjunto inválido.");
		}
		entityManager.close();
		return conjunto;
	}

	public List<Conjunto> buscaPorCodSai(String codSAI) throws NegocioException {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Conjunto> query = entityManager.createQuery(
					"from Conjunto c  where c.codSAI = :pCodSAI",
					Conjunto.class);
			query.setParameter("pCodSAI", codSAI);
			return query.getResultList();
			/*
			 * } catch (NonUniqueResultException e) { throw new
			 * NegocioException(
			 * "Existe mais de um conjunto para essa prensa.");
			 */
		} catch (NoResultException e) {
			throw new NegocioException("Código SAI não encontrado.");
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}
}
