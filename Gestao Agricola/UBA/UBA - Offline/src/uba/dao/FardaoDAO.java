package uba.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.swing.JOptionPane;

import uba.modelo.Fardao;
import uba.modelo.tipos.StatusFardao;
import uba.util.EntityManagerUtil;

public class FardaoDAO {

	public void salvar(Fardao fardao) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(fardao);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public void atualiza(Fardao fardao) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(fardao);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public void remover(Fardao fardao) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(fardao);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public List<Fardao> listarTodos() {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Fardao> query = entityManager.createQuery(
					"from Fardao f  ", Fardao.class);
			List<Fardao> resultList = query.getResultList();
			entityManager.close();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
		return null;

	}

	public Fardao busca(Fardao fardao) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Fardao> query = entityManager.createQuery(
					"from Fardao f where f = :pFardao", Fardao.class);
			query.setParameter("pFardao", fardao);
			Fardao result = query.getSingleResult();
			return result;
		} catch (NoResultException nre) {
			JOptionPane.showMessageDialog(null, "Fardão não encontrado",
					"Erro", JOptionPane.ERROR_MESSAGE);
			return null;
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public List<Fardao> buscaPorStatus(ArrayList<StatusFardao> status) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Fardao> query = entityManager.createQuery(
					"from Fardao f  where f.statusfardao in (:pStatus)",
					Fardao.class);
			query.setParameter("pStatus", status);
			return query.getResultList();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
		return null;
	}
}
