package uba.dao;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import uba.exceptions.NegocioException;
import uba.modelo.Fardao;
import uba.modelo.Fardo;
import uba.util.EntityManagerUtil;

public class FardoDAO {
	public FardoDAO() {
	}

	public void salvar(Fardo fardo) throws NegocioException {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		fardo.CalularPesoLiquido();
		// validações de peso pelo conjunto
		if (fardo.getPesoLiquido() > fardo.getConjunto().getPesoMaixmo()) {
			throw new NegocioException("Peso maximo("
					+ fardo.getConjunto().getPesoMaixmo()
					+ ") do fardo excedido.");
		}

		if (fardo.getPesoLiquido() < fardo.getConjunto().getPesoMinimo()) {
			throw new NegocioException("Peso minimo("
					+ fardo.getConjunto().getPesoMinimo()
					+ ") do fardo não foi atingido");
		}

		try {
			fardo.setData(Calendar.getInstance());
			entityManager.getTransaction().begin();
			entityManager.persist(fardo);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			throw new NegocioException("Erro ao  cadastrar o fardo",
					e.getCause());
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public void remover(Fardo fardo) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(fardo);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public List<Fardo> listarTodos(Fardao fardao) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Fardo> query = entityManager.createQuery(
					"from Fardo f where  f.fardao = :pFardao order by fardo",
					Fardo.class);
			query.setParameter("pFardao", fardao);
			List<Fardo> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
		return null;
	}

	public Fardo busca(Fardo fardo) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Fardo> query = entityManager.createQuery(
					"from Fardo f  where f = :pFardo", Fardo.class);
			query.setParameter("pFardo", fardo);
			Fardo result = query.getSingleResult();
			return result;
		} catch (NoResultException NeR) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

}
