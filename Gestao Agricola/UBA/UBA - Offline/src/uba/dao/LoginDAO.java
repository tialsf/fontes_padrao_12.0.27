package uba.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.swing.JOptionPane;

import uba.modelo.Usuario;
import uba.modelo.UsuarioLogado;
import uba.util.EntityManagerUtil;

public class LoginDAO {
	private static Usuario usuario;

	public static Usuario getUsuario() {
		return usuario;
	}

	public Usuario BuscaUsuario(String login) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			Usuario usuario = new Usuario();
			TypedQuery<Usuario> query = entityManager.createQuery(
					"from Usuario  u where u.login = :plogin", Usuario.class);
			query.setParameter("plogin", login);
			usuario = query.getSingleResult();
			return usuario;
		} catch (NoResultException e) {
			JOptionPane.showMessageDialog(null,
					" Por favor informe um Usuario Valido!", "erro",
					JOptionPane.ERROR_MESSAGE);
			return null;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					" Por favor informe um Usuario Valido!", "erro",
					JOptionPane.ERROR_MESSAGE);
			return null;
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public boolean autenticar(String login, String senha) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			usuario = BuscaUsuario(login);
			if (usuario != null) {
				if (VerificaSenhaCadastrada(usuario)) {
					if (senha.equals(usuario.getSenha())) {
						UsuarioLogado.setUsuarioLogado(usuario);
						return true;
					} else {
						JOptionPane
								.showMessageDialog(
										null,
										"Erro de autenticação de Usuario, Por favor informe um Usuario Valido!",
										"erro", JOptionPane.ERROR_MESSAGE);
						return false;
					}
				} else {
					usuario.setSenha(senha);
					salvarSenha(usuario);
					UsuarioLogado.setUsuarioLogado(usuario);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
		return false;
	}

	public boolean VerificaSenhaCadastrada(Usuario usuario) {
		if (usuario.getSenha() == null || usuario.getSenha() == "") {
			return false;
		} else {
			return true;
		}
	}

	public void salvarSenha(Usuario usuario) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(usuario);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}
}
