package uba.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import uba.modelo.Turno;
import uba.util.EntityManagerUtil;

public class TurnoDAO {

	public void salvar(Turno Turno) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(Turno);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public void remover(Turno Turno) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(Turno);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}

	public List<Turno> listarTodos() {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			TypedQuery<Turno> query = entityManager.createQuery("From Turno",
					Turno.class);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entityManager.isOpen()) {
				entityManager.close();
			}
		}
		return null;
	}

	public Turno busca(Turno Turno) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		try {
			Turno = entityManager.find(Turno.class, Turno.getCodTurno());
			return Turno;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (entityManager.isOpen())
				entityManager.close();
		}
	}
}
