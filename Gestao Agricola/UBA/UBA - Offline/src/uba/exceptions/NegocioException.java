package uba.exceptions;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;
	private final String msg;
	private Throwable origem;

	public NegocioException(String msg) {
		this.msg = msg;
	}

	public NegocioException(String msg, Throwable origem) {
		super();
		this.msg = msg;
		this.origem = origem;
	}

	@Override
	public String getMessage() {
		return msg;
	}

	public Throwable getOrigem() {
		return origem;
	}
}
