package uba.modelo;

import javax.swing.JOptionPane;

import uba.serialComm.SerialComm;

public class Balanca {
	protected SerialComm serialcomm;

	public void SetBalanca(String porta, int braudrate, int dataBits,
			int stopBits, int parity, int flowControl, int timeout) {
		serialcomm = new SerialComm(porta, braudrate, dataBits, stopBits,
				parity, flowControl, timeout);
	}

	public boolean ConectarBalanca() {
		try {
			serialcomm.AbrirPorta();
		} catch (NoSuchPortException e) {
			JOptionPane.showMessageDialog(null, "Porta não encontrada:"
					+ serialcomm.getPorta(), "Erro", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public void habilitarLeitura() {
		serialcomm.habilitarLeitura();
	}

	public void FecharPorta() {
		serialcomm.FecharPorta();
	}

	public String getPeso() {
		return "0.0";
	}

}
