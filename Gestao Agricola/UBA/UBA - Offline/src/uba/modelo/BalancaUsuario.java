package uba.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "usuarioDaBalanca")
public class BalancaUsuario {
	@Id
	private String codigo;
	@Column
	private String codFilial;
	@Column(nullable = true)
	private String descricao;
	@Column(nullable = true)
	private String modelo;
	@Column(nullable = true)
	private String marca;
	@Column(nullable = true)
	private final Integer status = 0;
	@Column(nullable = true)
	private String porta;
	@Column(nullable = true)
	private final Integer baudrate = 0;
	@Column(nullable = true)
	private final Integer dataBits = 0;
	@Column(nullable = true)
	private final Integer stopBits = 0;
	@Column(nullable = true)
	private final Integer parity = 0;
	@Column(nullable = true)
	private final Integer flowControlMode = 0;
	@Column(nullable = true)
	private final Integer timeout = 0;
	@Column(nullable = true)
	private Integer modalidade;

	public Integer getModalidade() {
		return modalidade;
	}

	public void setModalidade(Integer modalidade) {
		this.modalidade = modalidade;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getModelo() {
		return modelo;
	}

	public String getMarca() {
		return marca;
	}

	public Integer getStatus() {
		return status;
	}

	public String getPorta() {
		return porta;
	}

	public Integer getBaudrate() {
		return baudrate;
	}

	public Integer getDataBits() {
		return dataBits;
	}

	public Integer getStopBits() {
		return stopBits;
	}

	public Integer getParity() {
		return parity;
	}

	public Integer getFlowControlMode() {
		return flowControlMode;
	}

	public Integer getTimeout() {
		return timeout;
	}

}
