package uba.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "conjunto")
public class Conjunto {
	@Id
	private String codConjunto;
	private String codFilial;
	private String empresa;
	private String descricao;
	private String codSAI;
	private String codUBA;
	private double percentualDeRendimento;
	private double pesoAdicional;
	private double pesoMaximo;
	private double pesoMinimo;

	public double getPesoMaixmo() {
		return pesoMaximo;
	}

	public void setPesoMaixmo(double pesoMaixmo) {
		pesoMaximo = pesoMaixmo;
	}

	public double getPesoMinimo() {
		return pesoMinimo;
	}

	public void setPesoMinimo(double pesoMinimo) {
		this.pesoMinimo = pesoMinimo;
	}

	@OneToOne
	@JoinColumn(name = "codProduto")
	private Produto produto;

	public String getCodConjunto() {
		return codConjunto;
	}

	public void setCodConjunto(String codConjunto) {
		this.codConjunto = codConjunto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodSAI() {
		return codSAI;
	}

	public void setCodSAI(String codSAI) {
		this.codSAI = codSAI;
	}

	public String getCodUBA() {
		return codUBA;
	}

	public void setCodUBA(String codUBA) {
		this.codUBA = codUBA;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public double getPercentualRendimento() {
		return percentualDeRendimento;
	}

	public double getPesoAdicional() {
		return pesoAdicional;
	}

	public void setgetPesoAdicional(double pesoAdicional) {
		this.pesoAdicional = pesoAdicional;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCodFilial() {
		return codFilial;
	}

	public void setCodFilial(String codFilial) {
		this.codFilial = codFilial;
	}
}
