package uba.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import uba.modelo.tipos.StatusFardao;

@Entity
@Table(name = "fardao")
public class Fardao {
	@Id
	private String fardao;

	@OneToOne
	@JoinColumn(name = "codSafra", nullable = true)
	private Safra safra;

	@OneToOne
	@JoinColumn(name = "codProduto", nullable = true)
	private Produto produto;

	@OneToOne
	@JoinColumns({
			@JoinColumn(name = "codProdutor", referencedColumnName = "codProdutor"),
			@JoinColumn(name = "codLoja", referencedColumnName = "codLoja"),
			@JoinColumn(name = "codFilial", referencedColumnName = "codFilial") })
	private Produtor produtor;

	@OneToOne
	@JoinColumn(name = "codVariedade", nullable = true)
	private Variedade variedade;

	@OneToOne
	@JoinColumn(name = "codFazenda", nullable = true)
	private Fazenda fazenda;

	private double pesoLiquido;
	private double pesoPrevisto;
	private double pesoLiquidoFardos;
	private Integer qtd_Fardos;
	private double percentual;

	@Enumerated(EnumType.STRING)
	private StatusFardao statusfardao;

	@OneToOne
	@JoinColumn(name = "conjunto_cod")
	private Conjunto conjunto;

	@OneToMany(mappedBy = "fardao", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Fardo> fardos;

	public String getFilial_cod() {
		return produtor.getFilial_cod();
	}

	public StatusFardao getStatusfardao() {
		return statusfardao;
	}

	public void setStatusfardao(StatusFardao statusfardao) {
		this.statusfardao = statusfardao;
	}

	public String getFardao() {
		return fardao;
	}

	public void setFardao(String fardao) {
		this.fardao = fardao;
	}

	public double getPesoLiquido() {
		return pesoLiquido;
	}

	public void setPesoLiquido(double pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	public Integer getQtd_Fardos() {
		return qtd_Fardos;
	}

	public void setQtd_Fardos(Integer qtd_Fardos) {
		this.qtd_Fardos = qtd_Fardos;
	}

	public double getPercentual() {
		return percentual;
	}

	public void setPercentual(double percentual) {
		this.percentual = percentual;
	}

	public List<Fardo> getFardinhos() {
		return fardos;
	}

	public void setFardinhos(List<Fardo> fardinhos) {
		fardos = fardinhos;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Produtor getProdutor() {
		return produtor;
	}

	public void setProdutor(Produtor produtor) {
		this.produtor = produtor;
	}

	public Variedade getVariedade() {
		return variedade;
	}

	public void setVariedade(Variedade variedade) {
		this.variedade = variedade;
	}

	public Fazenda getFazenda() {
		return fazenda;
	}

	public void setFazenda(Fazenda fazenda) {
		this.fazenda = fazenda;
	}

	public List<Fardo> getFardos() {
		return fardos;
	}

	public void setFardos(List<Fardo> fardos) {
		this.fardos = fardos;
	}

	public Safra getSafra() {
		return safra;
	}

	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	public double getPesoLiquidoFardos() {
		return pesoLiquidoFardos;
	}

	public void setPesoLiquidoFardos(double pesoLiquidoFardos) {
		this.pesoLiquidoFardos = pesoLiquidoFardos;
	}

	public Conjunto getConjunto() {
		return conjunto;
	}

	public void setConjunto(Conjunto conjunto) {
		this.conjunto = conjunto;
	}

	public double getPesoPrevisto() {
		return pesoPrevisto;
	}

	public void setPesoPrevisto(double pesoPrevisto) {
		this.pesoPrevisto = pesoPrevisto;
	}
}
