package uba.modelo;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "fardo")
public class Fardo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String etiqueta;
	@Column
	private String codFilial;
	@Column
	private String clasifComercial;
	@Column
	private String fardo;
	@Column
	private double pesoLiquido;
	@Column
	private double pesoBruto;
	@Column
	private boolean constelado;
	@OneToOne
	@JoinColumn(name = "codVariedade")
	private Variedade codVariedade;
	@OneToOne
	private Produtor codProdutor;
	@OneToOne
	@JoinColumn(name = "codFazenda")
	private Fazenda codFazenda;
	@Column
	private String prensa;
	@OneToOne(optional = true)
	@JoinColumn(name = "turno_cod")
	private Turno turno;
	@OneToOne
	@JoinColumn(name = "conjunto_cod")
	private Conjunto conjunto;
	@OneToOne(optional = true)
	@JoinColumn(name = "codSafra")
	private Safra safra;
	@OneToOne
	@JoinColumn(name = "codProduto")
	private Produto produto;
	@ManyToOne
	@JoinColumn(name = "fardao_fardao", referencedColumnName = "fardao")
	private Fardao fardao;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;

	public void setData(Calendar calendar) {
		data = calendar;
	}

	public Fardo() {
	}

	public Fardo(Fardao fardao, boolean constelado, String etiqueta,
			Double txtPesoBruto) {
		this.fardao = fardao;
		codFazenda = fardao.getFazenda();
		codVariedade = fardao.getVariedade();
		codProdutor = fardao.getProdutor();
		safra = fardao.getSafra();
		codFilial = fardao.getFilial_cod();
		this.constelado = constelado;
		this.etiqueta = etiqueta;
		fardo = etiqueta.substring(13, 19);
		prensa = etiqueta.substring(12, 13);
		clasifComercial = etiqueta.substring(0, 1);
		pesoBruto = txtPesoBruto;
	}

	public String getFardo() {
		return fardo;
	}

	public void setFardo(String fardo) {
		this.fardo = fardo;
	}

	public double getPesoLiquido() {
		return pesoLiquido;
	}

	public void setPesoLiquido(double pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	public double getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(double pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public boolean isConstelado() {
		return constelado;
	}

	public void setConstelado(boolean constelado) {
		this.constelado = constelado;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Fardao getFardao() {
		return fardao;
	}

	public void setFardao(Fardao fardao) {
		this.fardao = fardao;
	}

	public Conjunto getConjunto() {
		return conjunto;
	}

	public void setConjunto(Conjunto conjunto) {
		this.conjunto = conjunto;
	}

	public void CalularPesoLiquido() {
		setPesoLiquido(pesoBruto - conjunto.getPesoAdicional());
	}

	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Calendar getData() {
		return data;
	}
}
