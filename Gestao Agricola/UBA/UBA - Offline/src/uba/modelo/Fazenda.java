package uba.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fazenda")
public class Fazenda {
	@Id
	private String codFazenda;
	@Column
	private String codFilial;
	@Column
	private String descricao;

	public String getCodFazenda() {
		return codFazenda;
	}

	public void setCodFazenda(String codFazenda) {
		this.codFazenda = codFazenda;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
