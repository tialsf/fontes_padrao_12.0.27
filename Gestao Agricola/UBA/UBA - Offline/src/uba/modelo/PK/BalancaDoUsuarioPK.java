package uba.modelo.PK;

import java.io.Serializable;

public class BalancaDoUsuarioPK implements Serializable {

	private static final long serialVersionUID = 1L;
	private String codBalanca;
	private String codUsuario;

	public BalancaDoUsuarioPK() {

	}

	public BalancaDoUsuarioPK(String codBalanca, String codUsuario) {
		this.codBalanca = codBalanca;
		this.codUsuario = codUsuario;
	}

	public String getCodBalanca() {
		return codBalanca;
	}

	public void setCodBalanca(String codBalanca) {
		this.codBalanca = codBalanca;
	}

	public String getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}

}
