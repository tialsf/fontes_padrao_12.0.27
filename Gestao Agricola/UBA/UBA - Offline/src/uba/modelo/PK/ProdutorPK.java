package uba.modelo.PK;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ProdutorPK implements Serializable {
	private static final long serialVersionUID = 1L;

	private String codProdutor;
	private String codLoja;
	private String codFilial;

	public ProdutorPK() {

	}

	public ProdutorPK(String codProdutor, String codLoja, String codFilial) {
		this.codProdutor = codProdutor;
		this.codLoja = codLoja;
		this.codFilial = codFilial;
	}

	public String getCodProdutor() {
		return codProdutor;
	}

	public void setCodProdutor(String codProdutor) {
		this.codProdutor = codProdutor;
	}

	public String getCodLoja() {
		return codLoja;
	}

	public void setLoja_cod(String codLoja) {
		this.codLoja = codLoja;
	}

	public String getCodFilial() {
		return codFilial;
	}

	public void setCodFilial(String codFilial) {
		this.codFilial = codFilial;
	}

}
