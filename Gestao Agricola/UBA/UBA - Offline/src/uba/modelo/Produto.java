package uba.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
public class Produto {
	@Id
	private String codProduto;
	@Column
	private String descricao;
	@Column
	private String codFilial;

	public String getProduto() {
		return codProduto;
	}

	public void setProduto(String produto) {
		codProduto = produto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
