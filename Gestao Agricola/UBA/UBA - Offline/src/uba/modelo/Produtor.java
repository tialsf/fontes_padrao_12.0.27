package uba.modelo;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import uba.modelo.PK.ProdutorPK;

@Entity
@Table(name = "produtor")
public class Produtor {
	@EmbeddedId
	private ProdutorPK codProdutor ;

	@Column
	private String descricao;

	@Column
	private String cidadeProdutor;

	public String getProdutor() {
		return codProdutor.getCodProdutor();
	}

	public String getFilial_cod() {
		return codProdutor.getCodFilial();
	}

	public void setProdutor(String produtor) {
		codProdutor.setCodProdutor(produtor);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCidadeProdutor() {
		return cidadeProdutor;
	}

}
