package uba.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "safra")
public class Safra {
	@Id
	private String codSafra;

	@Column
	private String codFilial;

	@Column
	private String descricao;

	public String getSafra() {
		return codSafra;
	}

	public void setSafra(String safra) {
		codSafra = safra;
	}

	public String getCodSafra() {
		return codSafra;
	}

	public void setCodSafra(String codSafra) {
		this.codSafra = codSafra;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return "" + descricao + "";
	}
}
