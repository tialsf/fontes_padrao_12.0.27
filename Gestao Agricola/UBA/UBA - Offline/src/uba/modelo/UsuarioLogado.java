package uba.modelo;

public class UsuarioLogado {
	static Usuario usuarioLogado;
	static BalancaDoUsuario balanca;

	public static Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public static void setUsuarioLogado(Usuario usuarioLogado) {
		UsuarioLogado.usuarioLogado = usuarioLogado;
	}

	public static BalancaDoUsuario getBalanca() {
		return balanca;
	}

	public static void setBalanca(BalancaDoUsuario usuarioBalanca) {
		balanca = usuarioBalanca;
	}

}
