package uba.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "variedade")
public class Variedade {
	@Id
	private String codVariedade;

	@Column
	private String codFilial;

	@Column
	private String descricao;

	public String getVariedade() {
		return codVariedade;
	}

	public void setVariedade(String variedade) {
		codVariedade = variedade;
	}

	public String getCodVariedade() {
		return codVariedade;
	}

	public void setCodVariedade(String codVariedade) {
		this.codVariedade = codVariedade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
