package uba.modelo.balancas;

import gnu.io.NoSuchPortException;
import gnu.io.SerialPortEvent;

import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;

import uba.serialComm.SerialComm;
import uba.util.Balanca;

/*
 *  structure of balance
 *  STX,SWA,SWB,SWC,I,I,I,I,I,I,T,T,T,T,T,T,CR,(CS)
 */

public class Digitron extends SerialComm implements Balanca {

	public Digitron(String porta, int braudrate, int dataBits, int stopBits, int parity, int flowControl, int timeout) {
		super(porta, braudrate, dataBits, stopBits, parity, flowControl, timeout);
	}

	private char[] charArray;
	private String swa, swb, swc;
	private DecimalFormat df = new DecimalFormat();

	@Override
	public boolean ConectarBalanca() {
		try {
			super.AbrirPorta();
		} catch (NoSuchPortException e) {
			JOptionPane
					.showMessageDialog(null, "Porta não encontrada:" + getPorta(), "Erro", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	@Override
	public void habilitarLeitura() {
		super.habilitarLeitura();
	}

	@Override
	public void FecharPorta() {
		super.FecharPorta();
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		switch (event.getEventType()) {
		case SerialPortEvent.DATA_AVAILABLE:
			int dado = 0;
			if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
				try {
					while (inputStream.available() > 0) {
						dado = inputStream.read();
					}
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Erro ao ler os dado.", "Erro", JOptionPane.ERROR_MESSAGE);
				}
			}

			break;
		case SerialPortEvent.BI:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:
		case SerialPortEvent.FE:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.PE:
		case SerialPortEvent.RI:
		default:
		}
	}

	// public Toledo(String retornoBalanca) {
	// charArray = retornoBalanca.toCharArray();
	// swa = retornaBinario(charArray[1]);
	// swb = retornaBinario(charArray[2]);
	// swc = retornaBinario(charArray[3]);
	// }
	//

	private String retornaBinario(char valor) {
		String b = Integer.toBinaryString(valor);
		if (b.length() < 8) {
			return "000000000".substring(0, 8 - b.length()).concat(b);
		} else {
			return b.substring(b.length() - 8);
		}
	}

	private double getDecimais() {
		// verifica casas decimal
		String code = swa.substring(5, swa.length());
		if ("001".equals(code)) {
			df = new DecimalFormat();
			return 0.1;
		} else if ("010".equals(code)) {
			df = new DecimalFormat();
			return 1;
		} else if ("011".equals(code)) {
			df = new DecimalFormat("#0.0");
			return 10;
		} else if ("100".equals(code)) {
			df = new DecimalFormat("#0.00");
			return 100;
		} else if ("101".equals(code)) {
			df = new DecimalFormat("#0.000");
			return 1000;
		} else if ("110".equals(code)) {
			df = new DecimalFormat("#0.0000");
			return 10000;
		}
		return 1;
	}

	private boolean isSTX(String leitura) {
		return (leitura.substring(0, 1).charAt(0) == 0x02);
	}

	private boolean getSobreCarga() {
		String code = swb.substring(6, 7);
		return code.equals("0") ? false : true;
	}

	@Override
	public String getPeso() {
		StringBuffer peso = new StringBuffer();
		StringBuffer tara = new StringBuffer();
		double dPeso = 0.0;
		double dTara = 0.0;
		//
		// String leitura = serialcomm.getLeitura();
		//
		// leitura.length();
		if (!leitura.isEmpty() && isSTX(leitura)) {

			charArray = leitura.toCharArray();
			swa = retornaBinario(charArray[1]);
			swb = retornaBinario(charArray[2]);
			swc = retornaBinario(charArray[3]);

			if (!getSobreCarga() && !getPesoNegativo()) {
				// pega string de peso
				for (int i = 4; i <= 9; i++) {
					peso.append(charArray[i]);
				}
				// pega string de tara
				for (int i = 10; i <= 15; i++) {
					tara.append(charArray[i]);
				}

				dPeso = (Double.valueOf(peso.toString()) / getDecimais());
				dTara = (Double.parseDouble(tara.toString()) / getDecimais());
				dPeso = (dPeso - dTara);
				return (df.format(dPeso));
			} else {
				return "0.0";
			}
		}
		return "0.0";
	}

	private boolean getPesoNegativo() {
		String code = swb.substring(5, 6);
		return code.equals("0") ? false : true;
	}
}