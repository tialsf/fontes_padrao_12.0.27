package uba.modelo.balancas;

import java.text.DecimalFormat;

import uba.serialComm.SerialComm;
import uba.util.Balanca;

public class Saturno extends SerialComm implements Balanca {
	String retornoBalanca;
	private final DecimalFormat df = new DecimalFormat("#.000");

	// private SerialComm serialcomm;

	/*
	 * Estrutura da String 1° | Carriage Return - Valor | Fixo (0Dh = 13
	 * decimal)
	 * 
	 * 2° a 7° | Valor do Peso Instantâneo | Valor Variável. 6 dígitos ASCII
	 * (0,...,9; 31h.....39h)
	 * 
	 * 8° | Caracter Indicador de | Caracter “E” (45h) = Valor de Peso “ESTÁVEL”
	 * | Estabilidade | Caracter “O” (4Fh) = Valor de Peso “OSCILANTE”
	 * 
	 * 9° | Caracter Indicador de | Caracter “L” (4Ch) = Balança “BLOQUEADA” |
	 * Bloqueio por Erro de | Caracter “B” (42h) = Balança “LIBERADA” |
	 * Posicionamento do Veículo” |
	 * 
	 * 10° | Reservado p/uso Futuro | Valor Fixo “_” (5Fh) (caracter
	 * “underline”)
	 * 
	 * 11° | Reservado p/uso Futuro | Valor Fixo “ “ (20h) (caracter “space”)
	 * 
	 * 12° | Line Feed | Valor Fixo (0Ah = 10 decimal)
	 */

	public Saturno() {
	}

	public Saturno(String retornoBalanca) {
		this.retornoBalanca = retornoBalanca;
	}

	// public Saturno(String porta, int braudrate, int dataBits, int stopBits,
	// int parity, int flowControl, int timeout) {
	// // serialcomm = new SerialComm(porta, braudrate, dataBits, stopBits,
	// parity, flowControl, timeout);
	// // serialcomm.habilitarLeitura();
	//
	// }

	@Override
	public String getPeso() {
		return retornoBalanca;
		//
		// double peso = 0;
		// retornoBalanca = serialcomm.getLeitura();
		// if (!retornoBalanca.isEmpty() && isEstavel() && !isBloqueada()) {
		// peso = (Double.parseDouble(retornoBalanca.substring(1, 7)) / 1000);
		// } else {
		// peso = 0;
		// }
		// return String.valueOf(peso);
	}

	private boolean isEstavel() {
		String status = (retornoBalanca.substring(7, 8)).toString();
		return status.equals("O") ? false : true;
	}

	private boolean isBloqueada() {
		String status = (retornoBalanca.substring(8, 9)).toString();
		return status.equals("L") ? false : true;
	}

	public static void main(String[] args) {
		StringBuffer stringBuffer = criaString();

		System.out.println(stringBuffer);
		Saturno saturno = new Saturno(stringBuffer.toString());
		System.out.println(saturno.getPeso());
	}

	private static StringBuffer criaString() {
		String hexString = "0D,0,0,1,4,5,6,45,4C,5F,20,0A";

		StringBuffer stringBuffer = new StringBuffer();

		String[] split = hexString.split(",");
		for (int i = 0; i < split.length; i++) {
			if (split[i].length() >= 2) {
				stringBuffer.append((char) Short.parseShort(split[i], 16));
			} else {
				stringBuffer.append(split[i]);
			}
		}
		return stringBuffer;
	}

	@Override
	public boolean ConectarBalanca() {
		// TODO Auto-generated method stub
		return false;
	}
}
