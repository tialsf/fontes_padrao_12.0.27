package uba.modelo.balancas;

import uba.serialComm.SerialComm;
import uba.util.Balanca;

public class Weightec extends SerialComm implements Balanca {
	// S , B B B . B B B , T T T . T T T , L L L . L L L CR LF
	private String retornobalanca;

	public Weightec() {

	}

	public Weightec(String retornobalanca) {
		this.retornobalanca = retornobalanca;
	}

	@Override
	public String getPeso() {
		// retornobalanca = serialcomm.getLeitura();
		if (!retornobalanca.isEmpty() && isEstavel()) {
			String[] split = retornobalanca.split(",");
			return split[3].toString(); // * 1000;
		}
		return "0.0";
	}

	private boolean isEstavel() {
		String[] split = retornobalanca.split(",");
		return split[0].equals("0") ? false : true;
	}

	@Override
	public boolean ConectarBalanca() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * public static void main(String[] args) { StringBuffer stringBuffer = new
	 * StringBuffer(); stringBuffer.append("1,010.000,000.200,000.900"); int CR
	 * = 0x0D; int LF = 0x0A; stringBuffer.append((char) CR);
	 * stringBuffer.append((char) LF); Weightec weightec = new
	 * Weightec(stringBuffer.toString());
	 * 
	 * System.out.println(weightec.getPeso()); }
	 */
}
