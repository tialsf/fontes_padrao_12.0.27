package uba.serialComm;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TooManyListenersException;

import javax.swing.JOptionPane;

public class SerialComm implements SerialPortEventListener {
	private CommPortIdentifier commPortaIdentifier;
	private SerialPort portaSerial;
	private int timeout = 0;
	private int braudrate = 0;
	private int dataBits = 0; // 5;6;7;8
	private int stopBits = 0; // STOPBITS_1 =1; STOPBITS_2 =2; STOPBITS_1_5 =3;
	private int parity = 0; // PARITY_NONE =0; PARITY_EVEN =2; PARITY_ODD =1;
	private int flowControl = 0;// RTSCTS_IN = 1; RTSCTS_OUT 2 ; XONXOFF_IN 4
								// ;XONXOFF_OUT 8
	protected String porta;
	protected String leitura = new String();
	protected StringBuffer bufferMain = new StringBuffer();
	protected InputStream inputStream;
	protected int countValidPeso = 0;

	public String getPorta() {
		return porta;
	}

	public SerialComm() {

	}

	public SerialComm(String porta, int braudrate, int dataBits, int stopBits, int parity, int flowControl, int timeout) {
		super();
		this.porta = porta;
		this.braudrate = braudrate;
		this.dataBits = dataBits;
		this.stopBits = stopBits;
		this.parity = parity;
		this.timeout = timeout;
		this.flowControl = flowControl;
	}

	public void AbrirPorta() throws NoSuchPortException {

		getCommPortaIdentifier();

		try {
			// Abrir porta
			portaSerial = (SerialPort) commPortaIdentifier.open(porta, timeout);
			// configurar parametros
			portaSerial.setSerialPortParams(braudrate, dataBits, stopBits, parity);
			portaSerial.setFlowControlMode(flowControl);
			inputStream = portaSerial.getInputStream();

		} catch (UnsupportedCommOperationException e) {
			JOptionPane.showMessageDialog(null, "Não foi possivel configurar a porta:" + portaSerial.getName(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		} catch (PortInUseException e) {
			JOptionPane.showMessageDialog(null, "Não foi possivel abrir conexão com  a Porta:" + portaSerial.getName(),
					"Erro", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public void FecharPorta() {
		// remove evento
		portaSerial.removeEventListener();
		portaSerial.close();
		try {
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Obtem o controlador da porta serial
	public void getCommPortaIdentifier() throws NoSuchPortException {
		commPortaIdentifier = CommPortIdentifier.getPortIdentifier(porta);
	}

	// Enviar dados para teste
	public void enviar(String msg) {
		try {
			OutputStream outputStream = portaSerial.getOutputStream();
			outputStream.write(msg.getBytes());
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void habilitarLeitura() {
		try {
			portaSerial.addEventListener(this);
			portaSerial.notifyOnDataAvailable(true);

		} catch (TooManyListenersException e) {
			JOptionPane.showMessageDialog(null, "Erro ao habilitar evento de leitura.", "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public String getLeitura() {
		return leitura;
	}

	@Override
	public void serialEvent(SerialPortEvent event) {
		// switch (event.getEventType()) {
		// case SerialPortEvent.DATA_AVAILABLE:
		// int dado = 0;
		// bufferMain.setLength(0);
		// if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
		// try {
		// while (inputStream.available() > 0) {
		// dado = inputStream.read();
		// bufferMain.append((char) dado);
		// }
		// leitura = bufferMain.toString();
		// System.out.println("saiu do while:" + leitura.toString() + "\n");
		// System.out.println(leitura.length());
		//
		// } catch (IOException e) {
		// JOptionPane.showMessageDialog(null, "Erro ao ler os dado.", "Erro",
		// JOptionPane.ERROR_MESSAGE);
		// }
		// }
		//
		// break;
		// case SerialPortEvent.BI:
		// case SerialPortEvent.CD:
		// case SerialPortEvent.CTS:
		// case SerialPortEvent.DSR:
		// case SerialPortEvent.FE:
		// case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
		// break;
		// case SerialPortEvent.PE:
		// case SerialPortEvent.RI:
		// default:
		// }
	}
}
