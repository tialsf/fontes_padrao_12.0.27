package uba.testes;

import gnu.io.NoSuchPortException;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import uba.serialComm.SerialComm;

public class TestEnviar extends JFrame {
	private final JTextField textField;
	private Socket cliente;
	private PrintStream saida;
	private SerialComm serialComm;
	private final JComboBox cbDataBits;
	private final JComboBox cbStopBit;
	private final JComboBox cbParity;
	private final JTextField txtPorta;
	private final JTextField jtBraudrate;
	private final JTextField txtTimeout;

	public TestEnviar() {
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton btnNewButton_1 = new JButton("Conectar");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				serialComm = new SerialComm(txtPorta.getText(), Integer
						.parseInt(jtBraudrate.getText()), (cbDataBits
						.getSelectedIndex() + 5), cbStopBit.getSelectedIndex(),
						cbParity.getSelectedIndex(), 0, Integer
								.parseInt(txtTimeout.getText()));
				try {
					serialComm.AbrirPorta();
				} catch (NoSuchPortException e1) {
					e1.printStackTrace();
				}

			}
		});
		btnNewButton_1.setBounds(48, 147, 132, 23);
		panel.add(btnNewButton_1);

		JButton btnNewButton = new JButton("Enviar");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Thread myThread = new Thread(new Runnable() {
					@Override
					public void run() {
						while (true) {
							char[] array = textField.getText().toCharArray();

							StringBuffer stringBuffer = new StringBuffer(
									textField.getText());
							// stringBuffer.append((char) Short.parseShort("02",
							// 16));
							// for (char c : array) {
							// stringBuffer.append(c);
							// }
							/*
							 * stringBuffer.append((char) Short.parseShort("0D",
							 * 16));
							 */
							serialComm.enviar(stringBuffer.toString());
						}

					}
				});
				myThread.start();
			}
		});
		btnNewButton.setBounds(86, 229, 89, 23);
		panel.add(btnNewButton);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// serialComm.enviar(textField.getText());
			}
		});
		textField.setBounds(182, 230, 166, 20);
		panel.add(textField);
		textField.setColumns(10);

		JButton btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				serialComm.FecharPorta();
			}
		});
		btnDesconectar.setBounds(212, 147, 156, 23);
		panel.add(btnDesconectar);

		JLabel lblEnviarStringPara = new JLabel("Enviar String para porta COM");
		lblEnviarStringPara.setBounds(181, 205, 213, 14);
		panel.add(lblEnviarStringPara);

		JButton btnSaturno = new JButton("Saturno");
		btnSaturno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				String hexString = "0D,0,0,1,4,5,6,45,4C,5F,20,0A";

				StringBuffer stringBuffer = new StringBuffer();

				String[] split = hexString.split(",");
				for (int i = 0; i < split.length; i++) {
					if (split[i].length() >= 2) {
						stringBuffer.append((char) Short.parseShort(split[i],
								16));
					} else {
						stringBuffer.append(split[i]);
					}
				}
				textField.setText(stringBuffer.toString());
			}
		});
		btnSaturno.setBounds(10, 293, 89, 23);
		panel.add(btnSaturno);

		JButton btnWeigtec = new JButton("Weightec");
		btnWeigtec.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append("1,010.000,000.200,000.900");
				int CR = 0x0D;
				int LF = 0x0A;
				stringBuffer.append((char) CR);
				stringBuffer.append((char) LF);
				textField.setText(stringBuffer.toString());
			}
		});
		btnWeigtec.setBounds(10, 327, 89, 23);
		panel.add(btnWeigtec);

		JLabel label = new JLabel("PORTA");
		label.setBounds(14, 21, 46, 14);
		panel.add(label);

		txtPorta = new JTextField();
		txtPorta.setText("COM4");
		txtPorta.setColumns(10);
		txtPorta.setBounds(14, 46, 86, 20);
		panel.add(txtPorta);

		JLabel label_1 = new JLabel("braudrate");
		label_1.setBounds(115, 21, 84, 14);
		panel.add(label_1);

		jtBraudrate = new JTextField();
		jtBraudrate.setText("4800");
		jtBraudrate.setBounds(113, 46, 89, 20);
		panel.add(jtBraudrate);

		JLabel label_2 = new JLabel("dataBits");
		label_2.setBounds(212, 21, 84, 14);
		panel.add(label_2);

		JLabel label_3 = new JLabel("stopBits");
		label_3.setBounds(14, 77, 46, 14);
		panel.add(label_3);

		cbStopBit = new JComboBox();
		cbStopBit.setModel(new DefaultComboBoxModel(new String[] {
				"STOPBITS_1     =1", "STOPBITS_2     =2", "STOPBITS_1_5 =3" }));
		cbStopBit.setSelectedIndex(1);
		cbStopBit.setMaximumRowCount(3);
		cbStopBit.setBounds(14, 102, 139, 20);
		panel.add(cbStopBit);

		JLabel label_4 = new JLabel("parity");
		label_4.setBounds(163, 77, 46, 14);
		panel.add(label_4);

		cbParity = new JComboBox();
		cbParity.setModel(new DefaultComboBoxModel(new String[] {
				"PARITY_NONE =0", "PARITY_ODD =1", "PARITY_EVEN =2" }));
		cbParity.setSelectedIndex(0);
		cbParity.setMaximumRowCount(4);
		cbParity.setBounds(163, 102, 132, 20);
		panel.add(cbParity);

		JLabel label_5 = new JLabel("Timeout");
		label_5.setBounds(307, 77, 84, 14);
		panel.add(label_5);

		txtTimeout = new JTextField();
		txtTimeout.setText("2000");
		txtTimeout.setBounds(305, 102, 89, 20);
		panel.add(txtTimeout);

		cbDataBits = new JComboBox();
		cbDataBits.setModel(new DefaultComboBoxModel(new String[] { "5", "6",
				"7", "8" }));
		cbDataBits.setSelectedIndex(2);
		cbDataBits.setMaximumRowCount(4);
		cbDataBits.setBounds(212, 46, 89, 20);
		panel.add(cbDataBits);
	}

	public static void main(String[] args) {
		TestEnviar testeConection = new TestEnviar();
		testeConection.setSize(470, 500);
		testeConection.setLocationRelativeTo(null);
		testeConection.setVisible(true);

	}
}
