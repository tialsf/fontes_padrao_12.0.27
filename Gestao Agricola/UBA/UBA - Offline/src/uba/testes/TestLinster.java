package uba.testes;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Enumeration;
import java.util.Locale;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import uba.serialComm.SerialComm;

public class TestLinster extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SerialComm serialComm = null;
	private final String peso = new String();
	JTextField jtBraudrate;
	private final JComboBox cbDataBits;
	private final JComboBox cbStopBit;
	private final JComboBox cbParity;
	private final JLabel lbPorta;
	private final JTextField txtTimeout;
	private final JTextField txtPorta;
	private final JTextArea textArea;

	private final DecimalFormat df = new DecimalFormat("#,##0.#",
			new DecimalFormatSymbols(new Locale("pt", "BR")));
	private boolean threadRunn = true;

	public boolean isThreadRunn() {
		return threadRunn;
	}

	public void setThreadRunn(boolean threadRunn) {
		if (!threadRunn) {
			serialComm.FecharPorta();
		}
		this.threadRunn = threadRunn;
	}

	public TestLinster() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				pararProducao();
				System.exit(0);
			}
		});

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JButton btnNewButton_1 = new JButton("Conectar");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				iniciarProcesso();
			}
		});
		btnNewButton_1.setBounds(20, 156, 124, 23);
		panel.add(btnNewButton_1);

		JButton btnDesconectar = new JButton("Desconectar");
		btnDesconectar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pararProducao();
			}

		});
		btnDesconectar.setBounds(20, 190, 124, 23);
		panel.add(btnDesconectar);

		JLabel lblPorta = new JLabel("PORTA");
		lblPorta.setBounds(20, 24, 46, 14);
		panel.add(lblPorta);

		jtBraudrate = new JTextField();
		jtBraudrate.setText("4800");
		jtBraudrate.setBounds(119, 49, 89, 20);
		panel.add(jtBraudrate);

		cbDataBits = new JComboBox();
		cbDataBits.setMaximumRowCount(4);
		cbDataBits.setModel(new DefaultComboBoxModel(new String[] { "5", "6",
				"7", "8" }));
		cbDataBits.setSelectedIndex(2);
		cbDataBits.setBounds(218, 49, 89, 20);
		panel.add(cbDataBits);

		JLabel lblNewLabel = new JLabel("braudrate");
		lblNewLabel.setBounds(121, 24, 84, 14);
		panel.add(lblNewLabel);

		JLabel lblDatabits = new JLabel("dataBits");
		lblDatabits.setBounds(218, 24, 84, 14);
		panel.add(lblDatabits);

		JLabel lblStopbits = new JLabel("stopBits");
		lblStopbits.setBounds(20, 80, 46, 14);
		panel.add(lblStopbits);

		JLabel lblParity = new JLabel("parity");
		lblParity.setBounds(169, 80, 46, 14);
		panel.add(lblParity);

		cbStopBit = new JComboBox();
		cbStopBit.setMaximumRowCount(3);
		cbStopBit.setModel(new DefaultComboBoxModel(new String[] {
				"STOPBITS_1     =1", "STOPBITS_2     =2", "STOPBITS_1_5 =3" }));
		cbStopBit.setSelectedIndex(1);
		cbStopBit.setBounds(20, 105, 139, 20);
		panel.add(cbStopBit);

		cbParity = new JComboBox();
		cbParity.setMaximumRowCount(4);
		cbParity.setModel(new DefaultComboBoxModel(new String[] {
				"PARITY_NONE =0", "PARITY_ODD =1", "PARITY_EVEN =2" }));
		cbParity.setSelectedIndex(0);
		cbParity.setBounds(169, 105, 132, 20);
		panel.add(cbParity);

		lbPorta = new JLabel("Sem conex\u00E3o");
		lbPorta.setBounds(154, 160, 246, 14);
		panel.add(lbPorta);

		txtTimeout = new JTextField();
		txtTimeout.setText("2000");
		txtTimeout.setBounds(311, 105, 89, 20);
		panel.add(txtTimeout);

		JLabel lblTimeou = new JLabel("Timeout");
		lblTimeou.setBounds(313, 80, 84, 14);
		panel.add(lblTimeou);

		txtPorta = new JTextField();
		txtPorta.setText("COM4");
		txtPorta.setBounds(20, 49, 86, 20);
		panel.add(txtPorta);
		txtPorta.setColumns(10);

		textArea = new JTextArea();
		textArea.setBounds(229, 200, 112, 22);
		panel.add(textArea);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(164, 180, 300, 150);
		panel.add(scrollPane);

	}

	public static void main(String[] args) {
		TestLinster testeConection = new TestLinster();
		testeConection.setSize(528, 383);
		testeConection.listarPortas();
		testeConection.setLocationRelativeTo(null);
		testeConection.setVisible(true);

	}

	public void listarPortas() {
		int i = 0;
		String[] portas = new String[10];
		Enumeration listaDePortas = CommPortIdentifier.getPortIdentifiers();
		while (listaDePortas.hasMoreElements()) {
			CommPortIdentifier ips = (CommPortIdentifier) listaDePortas
					.nextElement();
			portas[i] = ips.getName();
			i++;
		}

	}

	private void pararProducao() {
		if (serialComm != null) {
			setThreadRunn(false);
			lbPorta.setText("Sem conexão.. ");
		}
	}

	private void iniciarProcesso() {
		serialComm = new SerialComm(txtPorta.getText(),
				Integer.parseInt(jtBraudrate.getText()),
				(cbDataBits.getSelectedIndex() + 5),
				cbStopBit.getSelectedIndex(), cbParity.getSelectedIndex(), 0,
				Integer.parseInt(txtTimeout.getText()));

		try {
			serialComm.AbrirPorta();
		} catch (NoSuchPortException e1) {
			e1.printStackTrace();
		}

		serialComm.habilitarLeitura();

		setThreadRunn(true);
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (isThreadRunn() && serialComm != null) {
					textArea.setText("");
					if (!serialComm.getLeitura().isEmpty()) {
						textArea.append("String: " + serialComm.getLeitura()
								+ "\n");
						System.out.println(serialComm.getLeitura().length());
					}
					try {
						Thread.sleep(Integer.parseInt(txtTimeout.getText()));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();

		lbPorta.setText("conectado, ouvindo porta.....");
	}
}
