package uba.testes;

import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

public class TesteUsuarioBalanca {

	public static void main(String[] args) throws IOException {
		File dir1 = new File(".");
		System.out.println("Current dir : " + dir1.getCanonicalPath());
		System.out.println(InetAddress.getLocalHost().getHostName());
		System.out.println(InetAddress.getLocalHost().getHostAddress());

		for (PrintService p : PrinterJob.lookupPrintServices()) {
			System.out.println(p.getName());
		}

	}
}
