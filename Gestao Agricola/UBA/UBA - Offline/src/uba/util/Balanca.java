package uba.util;

public interface Balanca {
	public boolean ConectarBalanca();

	public void habilitarLeitura();

	public void FecharPorta();

	public String getPeso();

}
