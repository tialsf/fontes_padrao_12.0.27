package uba.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

import uba.exceptions.NegocioException;

public class EntityManagerUtil {

	private static EntityManagerFactory entityManagerFactory;

	public static EntityManager getEntityManager() {

		Map<Object, Object> propertiesConexao = new HashMap<Object, Object>();
		Properties arqProperties;
		if (entityManagerFactory == null) {
			try {
				PropertiesUBA.createProperties();
				arqProperties = PropertiesUBA.getArqProperties();
				propertiesConexao
						.put("javax.persistence.jdbc.url", arqProperties
								.getProperty("javax.persistence.jdbc.url"));
				entityManagerFactory = Persistence.createEntityManagerFactory(
						"uba", propertiesConexao);
			} catch (NegocioException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}

		return entityManagerFactory.createEntityManager();
	}
}