package uba.util;

//
//import java.io.BufferedWriter;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//
//import javax.print.DocFlavor;
//import javax.print.DocPrintJob;
//import javax.print.PrintException;
//import javax.print.PrintService;
//import javax.print.PrintServiceLookup;
//import javax.print.SimpleDoc;
//import javax.swing.JOptionPane;
//
//import uba.modelo.Produto;
//
public class ImprimeZebra {
	//
	// private Empresa empresaSelecionada;
	//
	// private Produto produtoSelecionado;
	// private Marca marcaSelecionada;
	// private final ArrayList etiqueta = new ArrayList();
	//
	// private String tipoImpressora;
	//
	// public String imprimeEtiquetaZebra(Produto produto, Marca marca,
	// Empresa empresa, int qtdeEtiquetas, int qtdeColunas, String imp) {
	//
	// String retorno = "";
	//
	// // recebe o produto enviado por parametro
	// produtoSelecionado = produto;
	//
	// // recebe a marca enviada por parametro
	// marcaSelecionada = marca;
	//
	// // recebe a empresa enviada por parametro
	// empresaSelecionada = empresa;
	//
	// tipoImpressora = imp;
	//
	// // escreve o nome do arquivo
	// String nomeArquivo = "Etiquetas" + File.separator + "ETIQUETA - "
	// + produtoSelecionado.getCodBarra() + ".txt";
	//
	// // cria o arquivo
	// File diretorio = new File("Etiquetas");
	// diretorio.mkdir();
	// File arquivoEtiqueta = new File(nomeArquivo);
	//
	// // preenche a etiqueta com os dados do produto
	// preencheEtiqueta(qtdeEtiquetas, qtdeColunas);
	//
	// // escreve o conteúdo do arquivo
	// try {
	//
	// BufferedWriter bw = new BufferedWriter(new FileWriter(
	// arquivoEtiqueta));
	//
	// for (int i = 0; i < etiqueta.size(); i++) {
	// bw.write(etiqueta.get(i).toString());
	// bw.newLine();
	// }
	//
	// bw.close();
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// // instancia um stream para carregamento do arquivo
	// FileInputStream dadosEtiqueta = null;
	//
	// try {
	//
	// // for (int i = 0; i < qtdeLinha; i++) {
	//
	// // carrega os dados do arquivo de etiqueta
	// dadosEtiqueta = new FileInputStream(arquivoEtiqueta);
	//
	// // determina o tipo a ser impresso (txt)
	// DocFlavor docFlavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
	// SimpleDoc documentoTexto = new SimpleDoc(dadosEtiqueta, docFlavor,
	// null);
	//
	// // verifica quais impressoras sao compativeis com txt
	// PrintService impressora = PrintServiceLookup
	// .lookupDefaultPrintService();
	//
	// // cria a tarefa de impressao
	// DocPrintJob printJob = impressora.createPrintJob();
	//
	// // tenta imprimir
	// printJob.print(documentoTexto, null); //
	// (PrintRequestAttributeSet)printerAttributes);
	//
	// // fecha o arquivo
	// dadosEtiqueta.close();
	//
	// // }
	//
	// // se nao achar o arquivo
	// } catch (FileNotFoundException ex) {
	// ex.printStackTrace();
	// // se nao conseguir imprimir
	// } catch (PrintException e) {
	// JOptionPane.showMessageDialog(null,
	// "Não foi possível realizar a impressão !!", "Erro",
	// JOptionPane.ERROR_MESSAGE);
	// // se nao conseguir fechar
	// } catch (IOException ex) {
	// ex.printStackTrace();
	// }
	//
	// return retorno;
	//
	// }
	//
	// private void preencheEtiqueta(int qtdeEtiquetas, int qtdeColunas) {
	//
	// if (qtdeColunas == 3) {
	//
	// String descricao = Validacao.removeAcentuacao(
	// produtoSelecionado.getDescricao()).toUpperCase();
	// String referencia = Validacao.removeAcentuacao(
	// produtoSelecionado.getReferencia()).toUpperCase();
	// String codigo = Validacao.removeAcentuacao(
	// produtoSelecionado.getCodLocalizacao()).toUpperCase();
	// String precoVista = Validacao
	// .colocaEspacoAEsquerda(
	// 8,
	// String.format("%,.2f",
	// produtoSelecionado.getValorMinimo()));
	// String precoPrazo = Validacao.colocaEspacoAEsquerda(16,
	// String.format("%,.2f", produtoSelecionado.getValorVenda()));
	// String codBarras = produtoSelecionado.getCodBarra();
	// String marca = Validacao.removeAcentuacao(
	// marcaSelecionada.getNome()).toUpperCase();
	//
	// int qtdeLinha = 0;
	// if (qtdeEtiquetas % 3 == 0) {
	// qtdeLinha = qtdeEtiquetas / 3;
	// } else {
	// qtdeLinha = (qtdeEtiquetas / 3) + 1;
	// }
	//
	// // cabeçalho e configuração da etiqueta
	//
	// // CONFIGURAÇÃO ETIQUETA IMPRESSORA ELTRON TLP 2742
	//
	// if ("TLP2742".equals(tipoImpressora)) {
	//
	// etiqueta.add("I8,A,001");
	// etiqueta.add("");
	// etiqueta.add("");
	// etiqueta.add("Q240,024");
	// etiqueta.add("q900");
	// etiqueta.add("rN");
	// etiqueta.add("S4");
	// etiqueta.add("D7");
	// etiqueta.add("ZT");
	// etiqueta.add("JF");
	// etiqueta.add("OD");
	// etiqueta.add("R0,0");
	// etiqueta.add("f100");
	// etiqueta.add("N");
	//
	// etiqueta.add("A010,10,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A010,30,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A010,50,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A010,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A010,110,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B9,135,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A235,170,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// etiqueta.add("A295,10,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A295,30,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A295,50,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A295,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A295,110,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B305,135,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A525,170,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// etiqueta.add("A580,10,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A580,30,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A580,50,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A580,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A580,110,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B590,135,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A810,170,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// } else if ("TLP2844".equals(tipoImpressora)) {
	//
	// etiqueta.add("I8,A,001");
	// etiqueta.add("");
	// etiqueta.add("");
	// etiqueta.add("Q240,024");
	// etiqueta.add("q448");
	// etiqueta.add("rN");
	// etiqueta.add("S4");
	// etiqueta.add("D7");
	// etiqueta.add("ZT");
	// etiqueta.add("JF");
	// etiqueta.add("OD");
	// etiqueta.add("R0,0");
	// etiqueta.add("f100");
	// etiqueta.add("N");
	//
	// etiqueta.add("A15,5,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A15,25,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A15,45,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A25,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A25,105,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B25,140,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A250,190,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// etiqueta.add("A300,5,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A300,25,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A300,45,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A310,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A310,105,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B310,140,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A535,190,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// etiqueta.add("A585,5,0,2,1,1,N,\""
	// + (descricao.length() < 18 ? descricao : descricao
	// .substring(0, 17)) + "\"");
	// etiqueta.add("A585,25,0,2,1,1,N,\"REF:"
	// + (referencia.length() < 14 ? referencia : referencia
	// .substring(0, 13)) + "\"");
	// etiqueta.add("A585,45,0,2,1,1,N,\"COD:"
	// + (codigo.length() < 14 ? codigo : codigo.substring(0,
	// 13)) + "\"");
	// etiqueta.add("A595,75,0,1,2,2,N,\"Av:" + precoVista + "\"");
	// etiqueta.add("A595,105,0,1,1,2,N,\"Prazo:" + precoPrazo + "\"");
	// etiqueta.add("B595,140,0,E30,2,2,55,B,\"" + codBarras + "\"");
	// etiqueta.add("A820,190,3,1,1,1,N,\""
	// + (marca.length() < 20 ? marca : marca.substring(0, 19))
	// + "\"");
	//
	// }
	//
	// etiqueta.add("P" + String.valueOf(qtdeLinha));
	//
	// }
	//
	// }
	//
}