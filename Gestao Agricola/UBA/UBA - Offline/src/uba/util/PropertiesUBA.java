package uba.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import uba.exceptions.NegocioException;
import uba.modelo.tipos.TipoImpressao;
import uba.view.SelecionaConjunto;

public class PropertiesUBA {
	private static String caminhoDaAplicacao;
	private static Properties properties = new Properties();

	public static Properties getArqProperties() throws NegocioException {
		FileInputStream file = null;
		try {
			caminhoDaAplicacao = new File(".").getCanonicalFile()
					.getCanonicalPath();
		} catch (IOException e) {
			throw new NegocioException(
					"Erro ao ler o caminho da aplicação,CreateProperties,getArqProperties. ",
					e.getCause());
		}

		try {
			file = new FileInputStream(caminhoDaAplicacao
					+ "/properties/dados.properties");
		} catch (FileNotFoundException e) {
			throw new NegocioException(
					"Erro ao ler o arquivo properties,getArqProperties. ",
					e.getCause());
		}

		try {
			properties.load(file);
		} catch (IOException e) {
			throw new NegocioException(
					"Erro ao carregar o arquivo properties,getArqProperties. ",
					e.getCause());
		}

		return properties;
	}

	public static void createProperties() throws NegocioException {
		String nomeDiretorio;
		File arquivo;
		String resposta = "";

		try {
			caminhoDaAplicacao = new File(".").getCanonicalFile()
					.getCanonicalPath();
		} catch (IOException e1) {
			throw new NegocioException(
					"Erro ao ler o caminho da aplicação,CreateProperties. ",
					e1.getCause());

		}

		nomeDiretorio = caminhoDaAplicacao + "/properties";

		if (!new File(nomeDiretorio).exists()) { // Verifica se o diretório //
			(new File(nomeDiretorio)).mkdir(); // Cria o diretório
		}

		arquivo = new File(nomeDiretorio + "/dados.properties");

		if (!arquivo.exists()) {
			FileOutputStream out;
			try {
				out = new FileOutputStream(nomeDiretorio + "/dados.properties");

				// dados do conexão
				resposta = perguntaProperties("IP do servidor de banco de dados");
				properties.setProperty("javax.persistence.jdbc.url",
						"jdbc:mysql://" + resposta + "/uba?autoReconnect=true");

				// Object[] possibilities = { "ZEBRA", "ARGOX" };
				//
				// do {
				// resposta = (String) JOptionPane.showInputDialog(null,
				// "Informe a marca da impressora",
				// "Impressora", JOptionPane.PLAIN_MESSAGE, null, possibilities,
				// "ZEBRA");
				// } while (resposta == null);
				properties.setProperty("prop.tipoImpressora", "ZEBRA");

				// NOME DA IMPRESSORA
				resposta = perguntaProperties("Qual é o  nome  do compartilhamento da impressora?");
				properties.setProperty("prop.nomeImpressora", resposta);

				// ALINHAMENTO DE PESO NA ETIQUETA
				TipoImpressao[] possibilities = { TipoImpressao.DIREITA,
						TipoImpressao.ESQUERDA };
				TipoImpressao tipoImpressao;
				do {
					tipoImpressao = (TipoImpressao) JOptionPane
							.showInputDialog(
									null,
									"Informe o posicionamento peso na etiqueta",
									"Impressora", JOptionPane.PLAIN_MESSAGE,
									null, possibilities, TipoImpressao.ESQUERDA);
				} while (resposta == null);
				properties.setProperty("prop.alinhamentoPesoEtiqueta",
						tipoImpressao.toString());

				// NOME DA Maquina/Computardor
				resposta = "";
				do {
					resposta = JOptionPane
							.showInputDialog(
									"Qual é o  nome do computador que a impressora está conectada?",
									InetAddress.getLocalHost().getHostName());
				} while (resposta == null);
				properties.setProperty("prop.nomeMaquina", resposta);

				// Onde estar o arquivo txt do peso
				JFileChooser fileChooser = new JFileChooser(caminhoDaAplicacao);
				fileChooser
						.setDialogTitle("Informe o caminho do arquivo txt para leitura de peso da balança");
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fileChooser.setSelectedFile(new File("peso.txt"));

				do {

				} while (fileChooser.showOpenDialog(null) == 1);

				properties.setProperty("prop.lerArquivoBalanca", fileChooser
						.getSelectedFile().toString());

				properties.setProperty("prop.caminhoAplicacao",
						caminhoDaAplicacao);

				SelecionaConjunto dialog = new SelecionaConjunto();
				dialog.setLocationRelativeTo(null);
				dialog.setModal(true);
				dialog.setVisible(true);

				do {

				} while (dialog.getConjuntoSelected() == null);

				properties.setProperty("prop.conjunto", dialog
						.getConjuntoSelected().getCodConjunto());

				properties.setProperty("prop.tempoDeleitura", "1000");

				properties.store(out, "config UBA");

				out.close();
			} catch (FileNotFoundException e) {
				// ler o arquivo propreties
				throw new NegocioException(
						"Erro ao ler o arquivo ,CreateProperties. ",
						e.getCause());
			} catch (UnknownHostException e1) {
				// ler o nome da maquina
				throw new NegocioException(
						"Erro ao ler o nome da maquina ,CreateProperties. ",
						e1.getCause());
			} catch (IOException e2) {
				// gravar o arquivo proprites
				throw new NegocioException(
						"Erro ao gravar o arquivo ,CreateProperties. ",
						e2.getCause());
			}
		}
	}

	public static String perguntaProperties(String pergunta) {
		String resposta;
		do {
			resposta = JOptionPane.showInputDialog(pergunta);
		} while (resposta == null);
		return resposta;
	}

}
