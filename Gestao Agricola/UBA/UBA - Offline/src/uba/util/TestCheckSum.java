/**
 * 
 */
package uba.util;

/**
 * @author maicol.lange
 * 
 */
public class TestCheckSum {

	public static void main(String[] args) {
		char[] hexDigits = "+p`0001940000000000".toCharArray();
		byte[] bytes = "+p`0001940000000000".getBytes();

		int soma = 0;

		for (byte c : bytes) {
			System.out.println((c & 0xFF));
		}

		soma = -soma;
		// Cálculo do complemento
		// soma = -soma;
		// // O valor a ser retornado
		System.out.println((byte) (soma & 0xFF));
		//
	}

	// int i = 0;

	// byte[] message = { (byte) 0xff };
}
// private static String hex(byte[] bytes) {
// StringBuilder sb = new StringBuilder();
// for (byte b : bytes) {
// sb.append(hexDigits[(b & 0xF0) >>> 4]).append(hexDigits[b & 0x0F]);
// }
// return sb.toString();
// }
//
// public static void main(String[] args) throws Exception {
// Checksum cs1 = new CRC32(), cs2 = new Adler32();
// MessageDigest md5 = MessageDigest.getInstance("MD5");
//
// System.out.println("  CRC32  |  Adler32 | MD5");
// for (int i = 1; i < 20; ++i) {
// cs1.reset();
// cs2.reset();
// md5.reset();
// byte[] buf = new byte[i];
// buf[0] = 1;
// cs1.update(buf, 0, buf.length);
// cs2.update(buf, 0, buf.length);
// byte[] digest = md5.digest(buf);
// System.out.printf("%08X | %08X | %s%n", cs1.getValue(),
// cs2.getValue(), hex(digest));
// }
// }

