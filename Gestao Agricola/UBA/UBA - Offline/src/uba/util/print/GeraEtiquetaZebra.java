package uba.util.print;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import uba.modelo.Fardo;
import uba.modelo.Produtor;
import uba.modelo.tipos.TipoImpressao;

public class GeraEtiquetaZebra {

	private static FileWriter writer;

	public static void GerarEtiqueta(TipoImpressao tp, String nomeArq,
			Produtor produtor, Fardo fardo) {
		try {
			File arquivo = new File(nomeArq);
			if (arquivo.exists()) {
				arquivo.delete();
			}

			writer = new FileWriter(nomeArq);
			writer.append("I8,A,001\n");
			writer.append("\n");
			writer.append("\n");
			writer.append("Q320,024\n");
			writer.append("q831\n");
			writer.append("rN\n");
			writer.append("S4\n");
			writer.append("D7\n");
			writer.append("ZT\n");
			writer.append("JF\n");
			writer.append("OD\n");
			writer.append("R56,0\n");
			writer.append("f100\n");
			writer.append("N\n");
			writer.append("A45,83,0,4,1,1,N,\""
					+ fardo.getConjunto().getEmpresa() + "\"\n");
			writer.append("A44,117,0,4,1,1,N,\"" + produtor.getDescricao()
					+ "\"\n");
			writer.append("A43,149,0,4,1,1,N,\"" + produtor.getCidadeProdutor()
					+ "\"\n");
			if (tp == TipoImpressao.ESQUERDA) {
				writer.append("A43,214,0,5,1,1,N,\"" + fardo.getPesoLiquido()
						+ " KG\"\n");
			} else if (tp == TipoImpressao.DIREITA) {
				writer.append("A336,213,0,5,1,1,N,\"" + fardo.getPesoLiquido()
						+ " KG\"\n");
			}
			writer.append("P1\n");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			System.out.println("Erro: " + e);
		}

	}
}
