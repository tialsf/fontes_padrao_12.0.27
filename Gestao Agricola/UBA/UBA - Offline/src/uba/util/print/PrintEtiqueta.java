package uba.util.print;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import uba.exceptions.NegocioException;
import uba.modelo.Fardo;
import uba.modelo.Produtor;
import uba.modelo.tipos.TipoImpressao;
import uba.util.PropertiesUBA;

public class PrintEtiqueta {
	private static String tipoImpressora;
	private static String nomeImpressora;
	private static String nomeMaquina;
	private static String noArqEtiqueta;
	private static String caminhoAplicacao;
	private static TipoImpressao alinhamentoPesoEtiqueta;
	private static String nomeDiretorio;
	private static String separador = java.io.File.separator;

	public static void Print(Produtor produtor, Fardo fardo)
			throws NegocioException {
		LoadProperties();

		nomeDiretorio = caminhoAplicacao;

		switch (tipoImpressora.toUpperCase()) {
		case "ZEBRA":
			noArqEtiqueta = "etiquetaZEBRA.txt";
			GeraEtiquetaZebra.GerarEtiqueta(alinhamentoPesoEtiqueta,
					nomeDiretorio + separador + noArqEtiqueta, produtor, fardo);
			break;
		case "ARGOX":

			break;
		}

		gerarArquivoBat();
		String[] command = { "cmd.exe", "/C", "Start", "impressao.bat", "/C",
				"close" };
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			throw new NegocioException(
					"Erro ao executar  o arquivo da bath, gerarArquivoBat. ",
					e.getCause());
		}
	}

	private static void LoadProperties() throws NegocioException {
		Properties arqProperties;

		arqProperties = PropertiesUBA.getArqProperties();

		tipoImpressora = arqProperties.getProperty("prop.tipoImpressora");
		nomeImpressora = arqProperties.getProperty("prop.nomeImpressora");
		nomeMaquina = arqProperties.getProperty("prop.nomeMaquina");
		caminhoAplicacao = arqProperties.getProperty("prop.caminhoAplicacao");
		String alinhamentoPeso = arqProperties
				.getProperty("prop.alinhamentoPesoEtiqueta");

		if (alinhamentoPeso.equals(TipoImpressao.DIREITA.toString())) {
			alinhamentoPesoEtiqueta = TipoImpressao.DIREITA;
		} else {
			alinhamentoPesoEtiqueta = TipoImpressao.ESQUERDA;
		}

	}

	private static void gerarArquivoBat() throws NegocioException {
		File arquivo = new File(nomeDiretorio + separador + "impressao.bat");
		if (arquivo.exists()) {
			arquivo.delete();
		}

		BufferedWriter writer;

		try {
			writer = new BufferedWriter(new BufferedWriter(new FileWriter(
					arquivo)));
			writer.append("TYPE ");
			writer.append(noArqEtiqueta + " > ");
			writer.append("\\\\" + nomeMaquina);
			writer.append("\\" + nomeImpressora + "\n");
			writer.append("exit");
			writer.flush();
			writer.close();
		} catch (IOException e) {
			throw new NegocioException(
					"Erro ao criar o arquivo da bath, gerarArquivoBat. ",
					e.getCause());
		}

	}
}
