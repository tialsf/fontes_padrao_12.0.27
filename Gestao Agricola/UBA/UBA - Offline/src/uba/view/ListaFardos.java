package uba.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import uba.modelo.Fardao;
import uba.view.tableModel.FardoTableModel;

public class ListaFardos extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable tblFardao;
	private FardoTableModel tableModel;

	private JTable getTblFardao(Fardao fardao) {
		if (tblFardao == null) {
			tblFardao = new JTable();
			tblFardao.setSurrendersFocusOnKeystroke(true);
			tblFardao.setColumnSelectionAllowed(true);
			tblFardao.setCellSelectionEnabled(true);
			tblFardao.setBorder(UIManager.getBorder("Button.border"));
			tblFardao.setModel(getTableModel(fardao));
		}
		return tblFardao;
	}

	private FardoTableModel getTableModel(Fardao fardao) {
		if (tableModel == null) {
			tableModel = new FardoTableModel();
		}
		return tableModel;
	}

	public ListaFardos(Fardao fardao) {
		setLocationRelativeTo(null);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}

		}

		JScrollPane scrollPane = new JScrollPane(getTblFardao(fardao));
		getContentPane().add(scrollPane);
	}

}
