package uba.view;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Main extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -524948198744072155L;

	public Main() {
		setTitle("Totvs - UBA");
		setIconImage(new ImageIcon(getClass().getResource(
				"/imagens/iconeAgro.png")).getImage());
		setMinimumSize(new Dimension(800, 600));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		ViewBeneficiamento viewBeneficiamento = new ViewBeneficiamento();
		viewBeneficiamento.setVisible(true);
		getContentPane().add(viewBeneficiamento);

	}
}
