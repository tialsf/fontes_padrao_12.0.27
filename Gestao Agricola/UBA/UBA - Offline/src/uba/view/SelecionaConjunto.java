package uba.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import uba.components.JTableZebra;
import uba.modelo.Conjunto;
import uba.view.tableModel.ConjuntoTableModel;

public class SelecionaConjunto extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTableZebra tblConjunto;
	private ConjuntoTableModel tableModel;
	private Conjunto conjunto;

	private JTable getTblConjunto() {
		if (tblConjunto == null) {
			tblConjunto = new JTableZebra();
			tblConjunto.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						selecionarConjuto();
					} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						setVisible(false);
					}
				}
			});
			tblConjunto.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() > 1) {
						selecionarConjuto();
					}
				}
			});
			tblConjunto.setBorder(UIManager.getBorder("Button.border"));
			tblConjunto.setModel(getTableModel());
		}
		return tblConjunto;
	}

	private ConjuntoTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new ConjuntoTableModel();
		}
		return tableModel;
	}

	public SelecionaConjunto() {
		setLocationRelativeTo(null);
		setBounds(100, 100, 680, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setIconImage(getToolkit().createImage(
				getClass().getResource("/imagens/iconeAgro.png")));

		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							selecionarConjuto();
						} catch (ArrayIndexOutOfBoundsException erro) {
							JOptionPane.showMessageDialog(null,
									"Por favor, selecione um Conjunto", "erro",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}

		}

		JScrollPane scrollPane = new JScrollPane(getTblConjunto());
		tblConjunto.setRowSelectionAllowed(true);
		getContentPane().add(scrollPane);
		if (tblConjunto.getRowCount() > 0) {
			tblConjunto.setRowSelectionInterval(0, 0);
		}
	}

	private void selecionarConjuto() {
		conjunto = tableModel.getSocio(tblConjunto.getSelectedRow());
		setVisible(false);
	}

	public Conjunto getConjuntoSelected() {
		return conjunto;
	}

}
