package uba.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import uba.components.JTableZebra;
import uba.modelo.Fardao;
import uba.view.tableModel.FardaoTableModel;

public class SelecionaFardao extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTableZebra tblFardao;
	private FardaoTableModel tableModel;
	private Fardao fardao;

	private JTable getTblFardao() {
		if (tblFardao == null) {
			tblFardao = new JTableZebra();
			tblFardao.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						selecionarFardao();
					} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						setVisible(false);
					}
				}
			});
			tblFardao.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() > 1) {
						selecionarFardao();
					}
				}
			});
			tblFardao.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tblFardao.setSurrendersFocusOnKeystroke(true);
			tblFardao.setBorder(UIManager.getBorder("Button.border"));
			tblFardao.setModel(getTableModel());
			tblFardao.setAutoCreateRowSorter(true);
			tblFardao.getRowSorter().toggleSortOrder(0);
		}
		return tblFardao;
	}

	private FardaoTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new FardaoTableModel();
		}
		return tableModel;
	}

	public SelecionaFardao() {
		setTitle("Consulta de fardões");
		setLocationRelativeTo(null);
		setBounds(100, 100, 630, 300);
		setSize(630, 300);
		setIconImage(getToolkit().createImage(
				getClass().getResource("/imagens/iconeAgro.png")));
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {

						try {
							selecionarFardao();
						} catch (ArrayIndexOutOfBoundsException erro) {
							JOptionPane.showMessageDialog(null,
									"Por favor, selecione um fardão", "erro",
									JOptionPane.ERROR_MESSAGE);
						}

					}

				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}

		}
		JScrollPane scrollPane = new JScrollPane(getTblFardao());
		getContentPane().add(scrollPane);
		if (tblFardao.getRowCount() > 0) {
			tblFardao.setRowSelectionInterval(0, 0);
		}
	}

	private void selecionarFardao() {
		fardao = tableModel.getSocio(tblFardao.convertRowIndexToModel(tblFardao
				.getSelectedRow()));
		setVisible(false);
	}

	public Fardao getFardaoSelected() {
		return fardao;
	}

	public void setFardaoSelected(Fardao fardao) {
		this.fardao = fardao;
	}

}
