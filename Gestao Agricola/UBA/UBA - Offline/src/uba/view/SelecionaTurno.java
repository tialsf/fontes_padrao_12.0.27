package uba.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import uba.components.JTableZebra;
import uba.modelo.Turno;
import uba.view.tableModel.TurnoTableModel;

public class SelecionaTurno extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTableZebra tblTurno;
	private TurnoTableModel tableModel;
	private Turno Turno;

	private JTable getTblTurno() {
		if (tblTurno == null) {
			tblTurno = new JTableZebra();
			tblTurno.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						SelecionaTurno();
					} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						setVisible(false);
					}
				}
			});
			tblTurno.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() > 1) {
						SelecionaTurno();
					}
				}
			});
			tblTurno.setSurrendersFocusOnKeystroke(true);
			tblTurno.setBorder(UIManager.getBorder("Button.border"));
			tblTurno.setModel(getTableModel());
		}
		return tblTurno;
	}

	private TurnoTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new TurnoTableModel();
		}
		return tableModel;
	}

	public SelecionaTurno() {
		setLocationRelativeTo(null);
		setBounds(100, 100, 680, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setIconImage(getToolkit().createImage(
				getClass().getResource("/imagens/iconeAgro.png")));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							SelecionaTurno();
						} catch (ArrayIndexOutOfBoundsException erro) {
							JOptionPane.showMessageDialog(null,
									"Por favor, selecione um turno", "erro",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}

		}

		JScrollPane scrollPane = new JScrollPane(getTblTurno());
		getContentPane().add(scrollPane);
		if (tblTurno.getRowCount() > 0) {
			tblTurno.setRowSelectionInterval(0, 0);
		}
	}

	private void SelecionaTurno() {
		Turno = tableModel.getSocio(tblTurno.getSelectedRow());
		setVisible(false);
	}

	public Turno getTurnoSelected() {
		return Turno;
	}

}
