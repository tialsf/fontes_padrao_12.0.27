package uba.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Collections;
import java.util.Properties;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import uba.components.JTableZebra;
import uba.controller.ConjuntoController;
import uba.controller.FardaoController;
import uba.controller.TurnoController;
import uba.exceptions.NegocioException;
import uba.modelo.BalancaDoUsuario;
import uba.modelo.Conjunto;
import uba.modelo.Fardao;
import uba.modelo.Fardo;
import uba.modelo.Turno;
import uba.modelo.UsuarioLogado;
import uba.modelo.tipos.StatusFardao;
import uba.modelo.tipos.StatusProducao;
import uba.modelo.tipos.TipoDeProducao;
import uba.modelo.tipos.TipoPesagem;
import uba.util.PropertiesUBA;
import uba.util.print.PrintEtiqueta;
import uba.view.tableModel.FardoTableModel;

public class ViewBeneficiamento extends JInternalFrame implements
		ActionListener {
	private static final long serialVersionUID = 1L;
	private FardoTableModel tableModel;
	private FardaoController fardaoController;
	private JScrollPane JScrollPane;
	private JPanel panelTop;
	private JPanel panelLeft;
	private JPanel panelRight;
	private JTextField txtFardao;
	private Fardao fardaoSelected;
	private JTableZebra tblFardos;
	private JTextField txtEtiqueta;
	private JCheckBox chckbxConstelado;
	private JTextField txtQtFardos;
	private JTextField txtSafra;
	private JTextField txtProdutor;
	private JTextField txtVariedade;
	private JTextField txtFazenda;
	private JButton btnIniciar;
	private JButton btnParar;
	private JButton btnTurno;
	private JButton btnConjunto;
	private JButton btnBuscaFardao;
	private JButton btnEncerrar;
	private BalancaDoUsuario balancaDoUsuario;
	private Fardo fardoSelected;
	private JButton btnReimprimir;
	private Conjunto conjuntoSelected;
	private Turno TurnoSelected;
	private String codigoEtiqueta = new String();
	private String codigoFardo = new String();
	private TipoPesagem tipoPesagem = TipoPesagem.AUTOMATICA;
	private StatusProducao statusProducao = StatusProducao.PARADA;
	private double pesoValido = 0;
	private int validaPeso = 0;
	private File arquivoTxtPeso;
	private final DecimalFormat df = new DecimalFormat("#0.00");
	private JFormattedTextField txtPesoBruto;
	private JFormattedTextField txtPesoLiquidoFardao = new JFormattedTextField(
			df);
	private JFormattedTextField txtPesoLiquidoFardos = new JFormattedTextField(
			df);
	private JFormattedTextField txtPesoPrevistoFardao = new JFormattedTextField(
			df);

	private boolean threadRunn = false;
	private JLabel lblProdutor;
	private JLabel lblPesoLiquidoFardo;
	private JTextField txtConjunto;
	private JTextField txtEan;
	private JTextField txtEx;
	private JTextField txtPrensa;
	private JTextField txtPrefixo;
	private JTextField txtTurno;
	private TipoDeProducao tipoDeProducao;
	private long tempoDeleitura = 0;
	private JTextField txtPercentual;
	private JCheckBox chckbxImprimir;

	public ViewBeneficiamento() {
		super();
		setRootPaneCheckingEnabled(false);
		setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);

		criaJanela();
		forcarSelecaoAoReceberFoco(this);

		setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
				Collections.EMPTY_SET);
	}

	private void goToBotton() {
		tblFardos.getSelectionModel().setSelectionInterval(
				tblFardos.getRowCount() - 1, tblFardos.getRowCount() - 1);
		tblFardos.setRowSelectionInterval(tblFardos.getRowCount() - 1,
				tblFardos.getRowCount() - 1);

		JScrollPane.getVerticalScrollBar().setValue(
				JScrollPane.getVerticalScrollBar().getMaximum());
	}

	private JTableZebra getTblFardo() {
		if (tblFardos == null) {
			tblFardos = new JTableZebra();
			tblFardos.setModel(getTableModel());
			tblFardos.setAutoCreateRowSorter(true);
			tblFardos.getRowSorter().toggleSortOrder(0);
			tblFardos.getColumnModel().getColumn(1).setPreferredWidth(190);
			tblFardos.getColumnModel().getColumn(4).setPreferredWidth(190);
			tblFardos.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					fardoSelected = tableModel.getFardo(tblFardos
							.convertRowIndexToModel(tblFardos.getSelectedRow()));
					btnReimprimir.setEnabled(true);
				}
			});
		}
		return tblFardos;
	}

	private FardoTableModel getTableModel() {
		if (tableModel == null) {
			tableModel = new FardoTableModel();
		}
		return tableModel;
	}

	public void criaJanela() {
		setFrameIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/imagens/agro32.png")));

		panelTop = new JPanel();
		panelTop.setBorder(new TitledBorder(null, "Fard\u00E3o",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		panelLeft = new JPanel();
		panelLeft.setBorder(new TitledBorder(null, "Fardos",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		panelRight = new JPanel();
		panelRight.setBorder(new TitledBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null), "Produ\u00E7\u00E3o",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		txtEtiqueta = new JTextField();
		txtEtiqueta.setForeground(Color.BLACK);
		txtEtiqueta.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtEtiqueta.getText().length() == 20) {
					setCodigoFardo(txtEtiqueta.getText().substring(13, 19));
				} else if (txtEtiqueta.getText().length() == 6) {
					setCodigoFardo(txtEtiqueta.getText());
				} else {
					setCodigoFardo("");
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {

			}
		});

		txtEtiqueta.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtEtiqueta.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (txtEtiqueta.getText().length() >= 20)
					txtEtiqueta.selectAll();

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					if (statusProducao == StatusProducao.PARADA)
						IniciarProducao();

					if ((txtEtiqueta.getText().length() >= 6)
							&& (txtEtiqueta.getText().length() <= 20)) {

						if (txtEtiqueta.getText().length() == 20) {
							setCodigoFardo(txtEtiqueta.getText().substring(13,
									19));
						} else if (txtEtiqueta.getText().length() == 6) {
							setCodigoFardo(txtEtiqueta.getText());
						} else {
							setCodigoFardo("");
						}

						if (tipoPesagem.equals(TipoPesagem.MANUAL)
								&& !txtEtiqueta.getText().isEmpty()) {
							txtPesoBruto.requestFocus();
						} else if (!txtEtiqueta.getText().isEmpty()) {
							if (produzirFardo()) {
								txtEtiqueta.requestFocus();
							}

						}
					}
				}
			}
		});

		txtEtiqueta.setColumns(10);

		JLabel lblPesbruto = new JLabel("Peso Bruto");

		chckbxConstelado = new JCheckBox("Costelado");
		chckbxConstelado.setSelected(true);
		chckbxConstelado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					txtEtiqueta.requestFocus();
				}
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout
				.createParallelGroup(Alignment.TRAILING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addComponent(panelLeft,
										GroupLayout.DEFAULT_SIZE, 416,
										Short.MAX_VALUE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panelRight,
										GroupLayout.PREFERRED_SIZE, 426,
										GroupLayout.PREFERRED_SIZE))
				.addComponent(panelTop, GroupLayout.DEFAULT_SIZE, 784,
						Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addComponent(panelTop, GroupLayout.PREFERRED_SIZE,
								126, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addComponent(panelRight,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(panelLeft,
												GroupLayout.DEFAULT_SIZE, 276,
												Short.MAX_VALUE))));

		btnReimprimir = new JButton("Reimprimir");
		btnReimprimir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (fardoSelected != null) {
					try {
						PrintEtiqueta.Print(fardaoSelected.getProdutor(),
								fardoSelected);
					} catch (NegocioException e) {
						JOptionPane.showMessageDialog(getContentPane(),
								e.getMessage(), "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null,
							"Por favor, selecione um fardo", "erro",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		btnReimprimir.setEnabled(false);

		btnIniciar = new JButton("Iniciar");
		btnIniciar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				IniciarProducao();
			}
		});
		btnIniciar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				IniciarProducao();
			}
		});

		txtPesoBruto = new JFormattedTextField(df);
		txtPesoBruto.setEnabled(false);
		txtPesoBruto.setEditable(false);
		txtPesoBruto.setForeground(Color.BLACK);
		txtPesoBruto.setValue(0);
		txtPesoBruto.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				txtPesoBruto.setValue(0);
			}

			@Override
			public void focusGained(java.awt.event.FocusEvent evt) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						txtPesoBruto.selectAll();
					}
				});
			}
		});
		txtPesoBruto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (produzirFardo()) {
						txtEtiqueta.requestFocus();
					}
				}
			}
		});
		txtPesoBruto.setColumns(10);

		txtPesoBruto.setFont(new Font("Tahoma", Font.PLAIN, 44));

		btnParar = new JButton("Parar");
		btnParar.setEnabled(false);
		btnParar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				PararProducao();
			}

		});

		JLabel lblEtiqueta = new JLabel("Etiqueta:");

		btnBuscaFardao = new JButton("F3");
		btnBuscaFardao
				.setIcon(new ImageIcon(
						ViewBeneficiamento.class
								.getResource("/javax/swing/plaf/metal/icons/ocean/upFolder.gif")));
		btnBuscaFardao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				consultaFardao();

			}
		});

		txtFardao = new JTextField();
		btnConjunto = new JButton("");
		btnConjunto.setEnabled(false);
		btnConjunto
				.setIcon(new ImageIcon(
						ViewBeneficiamento.class
								.getResource("/javax/swing/plaf/metal/icons/ocean/upFolder.gif")));
		btnConjunto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SelecionaConjunto dialog = new SelecionaConjunto();
				dialog.setModal(true);
				dialog.setVisible(true);
				if (posicionaConjunto(dialog.getConjuntoSelected())) {
					chckbxConstelado.requestFocus();
					// txtEtiqueta.requestFocus();
				} else {
					// //txtConjunto.requestFocus();
				}
			}
		});

		txtConjunto = new JTextField();
		txtConjunto.setEnabled(false);
		txtConjunto.setEditable(false);
		txtConjunto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.getKeyCode() == KeyEvent.VK_DELETE
						|| event.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					txtEan.setText("");
					txtEx.setText("");
					txtPrefixo.setText("");
					txtPrensa.setText("");
					txtConjunto.setText("");
					setConjuntoSelected(null);
				} else {
					Conjunto conjunto = new Conjunto();
					if ((event.getKeyCode() == KeyEvent.VK_F3)) {
						SelecionaConjunto dialog = new SelecionaConjunto();
						dialog.setModal(true);
						dialog.setVisible(true);
						conjunto = dialog.getConjuntoSelected();
						if (posicionaConjunto(conjunto)) {
							txtEtiqueta.requestFocus();
						} else {
							// ////txtConjunto.requestFocus();
						}
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER
							&& !txtConjunto.getText().isEmpty()) {
						conjunto.setCodConjunto(txtConjunto.getText());
						if (posicionaConjunto(conjunto)) {
							txtEtiqueta.requestFocus();
						} else {
							// ////txtConjunto.requestFocus();
						}
					} else {
						txtEtiqueta.requestFocus();
					}
				}
			}
		});

		txtConjunto.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtConjunto.setColumns(10);

		txtEan = new JTextField();
		txtEan.setEnabled(false);
		txtEan.setEditable(false);
		txtEan.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtEan.setColumns(10);

		JLabel lblEan = new JLabel("EAN");

		txtEx = new JTextField();
		txtEx.setEnabled(false);
		txtEx.setEditable(false);
		txtEx.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtEx.setColumns(10);

		JLabel lblEx = new JLabel("EX");

		JLabel lblPrensa = new JLabel("Prensa");

		txtPrensa = new JTextField();
		txtPrensa.setEnabled(false);
		txtPrensa.setEditable(false);
		txtPrensa.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtPrensa.setColumns(10);

		JLabel lblPrefixo = new JLabel("Prefixo");

		txtPrefixo = new JTextField();
		txtPrefixo.setEnabled(false);
		txtPrefixo.setEditable(false);
		txtPrefixo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtPrefixo.setColumns(10);

		txtTurno = new JTextField();
		txtTurno.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.getKeyCode() == KeyEvent.VK_ENTER) {
					Turno turno = new Turno();
					turno.setCodTurno(txtTurno.getText());
					setTurnoSelected(turno);
					if (posicionaTurno()) {
						chckbxConstelado.requestFocus();
					} else {
						txtTurno.setText("");
						txtTurno.selectAll();
						JOptionPane.showMessageDialog(getContentPane(),
								"Turno Invalido.", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				} else if (event.getKeyCode() == KeyEvent.VK_F3) {
					SelecionaTurno dialog = new SelecionaTurno();
					dialog.setModal(true);
					dialog.setVisible(true);
					if (dialog.getTurnoSelected() != null) {
						setTurnoSelected(dialog.getTurnoSelected());
						txtTurno.setText(getTurnoSelected().getCodTurno());
						txtTurno.requestFocus();
					}
				}
			}
		});
		txtTurno.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtTurno.setColumns(10);

		btnTurno = new JButton("F3");
		btnTurno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SelecionaTurno dialog = new SelecionaTurno();
				dialog.setModal(true);
				dialog.setLocationRelativeTo(getContentPane());
				dialog.setVisible(true);
				if (dialog.getTurnoSelected() != null) {
					setTurnoSelected(dialog.getTurnoSelected());
					txtTurno.setText(getTurnoSelected().getCodTurno());
					// txtEtiqueta.requestFocus();
					txtTurno.requestFocus();
				}
			}
		});
		btnTurno.setIcon(new ImageIcon(
				ViewBeneficiamento.class
						.getResource("/javax/swing/plaf/metal/icons/ocean/upFolder.gif")));

		JLabel lblTurno = new JLabel("Turno:");

		JLabel lblConjunto = new JLabel("Conjunto:");

		txtPercentual = new JTextField();
		txtPercentual.setForeground(Color.BLACK);
		txtPercentual.setEditable(false);
		txtPercentual.setText("0,00%");
		txtPercentual.setHorizontalAlignment(SwingConstants.CENTER);
		txtPercentual.setFont(new Font("Dialog", Font.PLAIN, 50));
		txtPercentual.setColumns(10);
		txtPercentual.setBackground(Color.GREEN);

		btnEncerrar = new JButton("Finalizar Beneficiamento");
		btnEncerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EncerrarProducao();
			}
		});
		btnEncerrar.setEnabled(false);

		chckbxImprimir = new JCheckBox("Imprimir");
		chckbxImprimir.setSelected(true);
		GroupLayout gl_panelRight = new GroupLayout(panelRight);
		gl_panelRight.setHorizontalGroup(
			gl_panelRight.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelRight.createSequentialGroup()
					.addGap(4)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addComponent(lblConjunto, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panelRight.createSequentialGroup()
									.addComponent(txtConjunto, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addGap(3)
									.addComponent(btnConjunto, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)))
							.addGap(64)
							.addComponent(txtPrefixo, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblPesbruto, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panelRight.createSequentialGroup()
							.addComponent(txtPesoBruto, GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
							.addGap(190))
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addComponent(lblEtiqueta, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtEtiqueta, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panelRight.createSequentialGroup()
									.addComponent(chckbxConstelado)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(chckbxImprimir, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
							.addGap(81)
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addComponent(btnIniciar, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnParar, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnReimprimir, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addComponent(lblTurno, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panelRight.createSequentialGroup()
									.addComponent(txtTurno, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addGap(3)
									.addComponent(btnTurno)))
							.addGap(38)
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelRight.createSequentialGroup()
									.addComponent(txtEan, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(txtEx, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
									.addGap(5)
									.addComponent(txtPrensa, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panelRight.createSequentialGroup()
									.addComponent(lblEan, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
									.addGap(13)
									.addComponent(lblEx, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
									.addGap(5)
									.addComponent(lblPrensa, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblPrefixo, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))))
					.addGap(21))
				.addGroup(gl_panelRight.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnEncerrar, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(254, Short.MAX_VALUE))
				.addGroup(gl_panelRight.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtPercentual, GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
					.addGap(21))
		);
		gl_panelRight.setVerticalGroup(
			gl_panelRight.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelRight.createSequentialGroup()
					.addGap(3)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
						.addComponent(lblTurno)
						.addComponent(lblEan)
						.addComponent(lblEx)
						.addComponent(lblPrensa))
					.addGap(6)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGap(1)
							.addComponent(txtTurno, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
							.addComponent(txtEx, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(txtPrensa, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnTurno, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtEan, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPrefixo)
					.addGap(2)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRight.createSequentialGroup()
							.addComponent(lblConjunto)
							.addGap(7)
							.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelRight.createSequentialGroup()
									.addGap(1)
									.addComponent(txtConjunto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(btnConjunto, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGap(4)
							.addComponent(txtPrefixo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(7)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.BASELINE)
						.addComponent(chckbxConstelado)
						.addComponent(chckbxImprimir)
						.addComponent(btnIniciar))
					.addGap(11)
					.addGroup(gl_panelRight.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelRight.createSequentialGroup()
							.addGap(1)
							.addComponent(lblEtiqueta)
							.addGap(6)
							.addComponent(txtEtiqueta, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panelRight.createSequentialGroup()
							.addComponent(btnParar)
							.addGap(16)
							.addComponent(btnReimprimir)))
					.addGap(5)
					.addComponent(lblPesbruto)
					.addGap(4)
					.addComponent(txtPesoBruto, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addComponent(txtPercentual, GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnEncerrar, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
		);
		panelRight.setLayout(gl_panelRight);
		panelRight.setFocusTraversalPolicy(new FocusTraversalOnArray(
				new Component[] { lblTurno, lblEan, lblEx, lblPrensa, txtTurno,
						btnTurno, txtEan, lblPrefixo, txtEx, txtPrensa,
						lblConjunto, txtConjunto, btnConjunto, txtPrefixo,
						chckbxConstelado, btnIniciar, lblEtiqueta, txtEtiqueta,
						btnParar, btnReimprimir, lblPesbruto, txtPesoBruto,
						btnEncerrar, txtPercentual }));

		txtFardao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_ENTER)) {
					if (!txtFardao.getText().isEmpty()) {
						Fardao fardao = new Fardao();
						fardao.setFardao(txtFardao.getText());
						setFardaoSelected(fardao);
						carregaFardao();
						habilitarBotoes();
						txtTurno.requestFocus();
					} else {
						fardaoSelected = null;
						LimpGridFardos();
					}
				} else if ((e.getKeyCode() == KeyEvent.VK_F3)) {
					consultaFardao();
				}
			}
		});

		txtFardao.setFont(new Font("Tahoma", Font.LAYOUT_LEFT_TO_RIGHT, 11));
		txtFardao.setColumns(10);
		txtQtFardos = new JTextField();
		txtQtFardos.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtQtFardos.setEditable(false);
		txtQtFardos.setColumns(10);

		JLabel lblQuantidadeDeFardos = new JLabel("Quantidade de Fardos");

		txtSafra = new JTextField();
		txtSafra.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtSafra.setEditable(false);
		txtSafra.setColumns(10);

		txtProdutor = new JTextField();
		txtProdutor.setFont(new Font("Tahoma", Font.LAYOUT_LEFT_TO_RIGHT, 11));
		txtProdutor.setEditable(false);
		txtProdutor.setColumns(10);

		JLabel lblVariedade = new JLabel("Variedade");

		txtVariedade = new JTextField();
		txtVariedade.setFont(new Font("Tahoma", Font.LAYOUT_LEFT_TO_RIGHT, 11));
		txtVariedade.setEditable(false);
		txtVariedade.setColumns(10);

		JLabel lblFazenda = new JLabel("Fazenda");

		txtFazenda = new JTextField();
		txtFazenda.setFont(new Font("Tahoma", Font.PLAIN, 10));
		txtFazenda.setEditable(false);
		txtFazenda.setColumns(10);

		txtPesoLiquidoFardao = new JFormattedTextField(df);
		txtPesoLiquidoFardao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent keyvent) {
				if (keyvent.getKeyCode() == KeyEvent.VK_ENTER) {
					txtTurno.requestFocus();
				}
			}
		});
		txtPesoLiquidoFardao.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				Double peso = Double.valueOf(txtPesoLiquidoFardao.getText()
						.replaceAll(",", "."));
				if (peso > 0) {
					getFardaoSelected().setPesoLiquido(peso);
					fardaoController.salvar(fardaoSelected);
					txtTurno.requestFocus();
				}
			}
		});
		txtPesoLiquidoFardao.setEditable(false);
		txtPesoLiquidoFardao.setFont(new Font("Tahoma",
				Font.LAYOUT_LEFT_TO_RIGHT, 11));
		txtPesoLiquidoFardao.setColumns(10);

		txtPesoLiquidoFardos = new JFormattedTextField(df);
		txtPesoLiquidoFardos.setFont(new Font("Tahoma",
				Font.LAYOUT_LEFT_TO_RIGHT, 11));
		txtPesoLiquidoFardos.setEditable(false);
		txtPesoLiquidoFardos.setColumns(10);

		JLabel lblPesoLiquidoFardos = new JLabel("Peso Liquido Fardos");

		JLabel lblFardo = new JLabel("Codigo  Fardão");

		JLabel lblProduto = new JLabel("Produto");

		lblProdutor = new JLabel("Produtor");

		lblPesoLiquidoFardo = new JLabel("Peso Liquido Fardão");

		txtPesoPrevistoFardao = new JFormattedTextField((Format) null);
		txtPesoPrevistoFardao.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtPesoPrevistoFardao.setEditable(false);
		txtPesoPrevistoFardao.setColumns(10);

		JLabel lblPesoPrevistoFardo = new JLabel("Peso Previsto Fardão");
		GroupLayout gl_panelTop = new GroupLayout(panelTop);
		gl_panelTop
				.setHorizontalGroup(gl_panelTop
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelTop
										.createSequentialGroup()
										.addGroup(
												gl_panelTop
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblFardo,
																GroupLayout.DEFAULT_SIZE,
																168,
																Short.MAX_VALUE)
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addComponent(
																				txtFardao,
																				GroupLayout.DEFAULT_SIZE,
																				97,
																				Short.MAX_VALUE)
																		.addGap(4)
																		.addComponent(
																				btnBuscaFardao))
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addComponent(
																				lblVariedade,
																				GroupLayout.DEFAULT_SIZE,
																				162,
																				Short.MAX_VALUE)
																		.addGap(6))
														.addComponent(
																txtVariedade,
																GroupLayout.DEFAULT_SIZE,
																168,
																Short.MAX_VALUE))
										.addGap(11)
										.addGroup(
												gl_panelTop
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addGap(1)
																		.addComponent(
																				lblFazenda,
																				GroupLayout.DEFAULT_SIZE,
																				141,
																				Short.MAX_VALUE)
																		.addGap(22)
																		.addComponent(
																				lblQuantidadeDeFardos,
																				GroupLayout.DEFAULT_SIZE,
																				125,
																				Short.MAX_VALUE)
																		.addGap(7))
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addComponent(
																				txtFazenda,
																				GroupLayout.DEFAULT_SIZE,
																				154,
																				Short.MAX_VALUE)
																		.addGap(10)
																		.addComponent(
																				txtQtFardos,
																				GroupLayout.DEFAULT_SIZE,
																				132,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addGroup(
																				gl_panelTop
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								lblProduto,
																								Alignment.LEADING,
																								GroupLayout.PREFERRED_SIZE,
																								105,
																								Short.MAX_VALUE)
																						.addComponent(
																								txtSafra,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								105,
																								Short.MAX_VALUE))
																		.addGap(10)
																		.addGroup(
																				gl_panelTop
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblProdutor,
																								GroupLayout.DEFAULT_SIZE,
																								181,
																								Short.MAX_VALUE)
																						.addComponent(
																								txtProdutor,
																								GroupLayout.DEFAULT_SIZE,
																								181,
																								Short.MAX_VALUE))))
										.addGap(10)
										.addGroup(
												gl_panelTop
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblPesoLiquidoFardo,
																GroupLayout.DEFAULT_SIZE,
																175,
																Short.MAX_VALUE)
														.addComponent(
																txtPesoLiquidoFardao,
																GroupLayout.DEFAULT_SIZE,
																175,
																Short.MAX_VALUE)
														.addComponent(
																lblPesoLiquidoFardos,
																GroupLayout.DEFAULT_SIZE,
																175,
																Short.MAX_VALUE)
														.addComponent(
																txtPesoLiquidoFardos,
																GroupLayout.DEFAULT_SIZE,
																175,
																Short.MAX_VALUE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panelTop
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblPesoPrevistoFardo,
																GroupLayout.PREFERRED_SIZE,
																175,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																txtPesoPrevistoFardao,
																GroupLayout.PREFERRED_SIZE,
																175,
																GroupLayout.PREFERRED_SIZE))
										.addGap(1)));
		gl_panelTop
				.setVerticalGroup(gl_panelTop
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelTop
										.createSequentialGroup()
										.addGroup(
												gl_panelTop
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addGroup(
																				gl_panelTop
																						.createParallelGroup(
																								Alignment.BASELINE)
																						.addComponent(
																								lblFardo)
																						.addComponent(
																								lblProduto)
																						.addComponent(
																								lblProdutor)
																						.addComponent(
																								lblPesoLiquidoFardo))
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addGroup(
																				gl_panelTop
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panelTop
																										.createSequentialGroup()
																										.addGroup(
																												gl_panelTop
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addGroup(
																																gl_panelTop
																																		.createSequentialGroup()
																																		.addGap(1)
																																		.addComponent(
																																				txtFardao,
																																				GroupLayout.PREFERRED_SIZE,
																																				GroupLayout.DEFAULT_SIZE,
																																				GroupLayout.PREFERRED_SIZE))
																														.addComponent(
																																btnBuscaFardao,
																																GroupLayout.PREFERRED_SIZE,
																																20,
																																GroupLayout.PREFERRED_SIZE))
																										.addGap(11)
																										.addComponent(
																												lblVariedade)
																										.addGap(6)
																										.addComponent(
																												txtVariedade,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								gl_panelTop
																										.createSequentialGroup()
																										.addGap(1)
																										.addGroup(
																												gl_panelTop
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																txtSafra,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																txtProdutor,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE))
																										.addGap(11)
																										.addGroup(
																												gl_panelTop
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																lblFazenda)
																														.addComponent(
																																lblQuantidadeDeFardos))
																										.addGap(6)
																										.addGroup(
																												gl_panelTop
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																txtFazenda,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																txtQtFardos,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)))
																						.addGroup(
																								gl_panelTop
																										.createSequentialGroup()
																										.addComponent(
																												txtPesoLiquidoFardao,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(12)
																										.addComponent(
																												lblPesoLiquidoFardos)
																										.addGap(6)
																										.addComponent(
																												txtPesoLiquidoFardos,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE))))
														.addGroup(
																gl_panelTop
																		.createSequentialGroup()
																		.addComponent(
																				lblPesoPrevistoFardo)
																		.addGap(6)
																		.addComponent(
																				txtPesoPrevistoFardao,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap()));
		panelTop.setLayout(gl_panelTop);
		panelTop.setFocusTraversalPolicy(new FocusTraversalOnArray(
				new Component[] { btnBuscaFardao, txtFardao, lblFardo,
						lblVariedade, txtVariedade, lblFazenda,
						lblQuantidadeDeFardos, txtFazenda, txtQtFardos,
						lblProduto, txtSafra, lblProdutor, txtProdutor,
						lblPesoLiquidoFardo, txtPesoLiquidoFardao,
						lblPesoLiquidoFardos, txtPesoLiquidoFardos }));

		JScrollPane = new JScrollPane((Component) null);
		JScrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		JScrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		tblFardos = getTblFardo();

		JScrollPane.setViewportView(tblFardos);

		GroupLayout gl_panelLeft = new GroupLayout(panelLeft);
		gl_panelLeft.setHorizontalGroup(gl_panelLeft.createParallelGroup(
				Alignment.LEADING).addComponent(JScrollPane,
				GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE));
		gl_panelLeft.setVerticalGroup(gl_panelLeft.createParallelGroup(
				Alignment.LEADING).addComponent(JScrollPane,
				GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE));
		panelLeft.setLayout(gl_panelLeft);
		getContentPane().setLayout(groupLayout);

	}

	private void carregarFardos() {
		tableModel.Listar(getFardaoSelected());
	}

	public void carregaFardao() {
		fardaoController = new FardaoController();

		setFardaoSelected(fardaoController.buscar(getFardaoSelected()));

		if (getFardaoSelected() != null) {

			if (getFardaoSelected().getStatusfardao() == StatusFardao.BENEFICIADO) {
				LimpGridFardos();
				JOptionPane.showMessageDialog(null, "Fardão já beneficiado",
						"", JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			Properties arqProperties;
			try {
				arqProperties = PropertiesUBA.getArqProperties();
				String codcojunto = arqProperties.getProperty("prop.conjunto");
				Conjunto conjunto = new Conjunto();
				conjunto.setCodConjunto(codcojunto);
				posicionaConjunto(conjunto);
			} catch (NegocioException e1) {
				e1.printStackTrace();
			}

			txtFardao.setText(getFardaoSelected().getFardao().trim());

			txtPesoLiquidoFardao.setText(df.format(fardaoSelected
					.getPesoLiquido()));
			txtPesoLiquidoFardos.setText(df.format(fardaoSelected
					.getPesoLiquidoFardos()));

			txtPesoPrevistoFardao.setText(df.format(fardaoSelected
					.getPesoPrevisto()));

			if (fardaoSelected.getFazenda() != null) {
				txtFazenda.setText(fardaoSelected.getFazenda().getDescricao()
						.trim());
			}

			if (fardaoSelected.getProdutor() != null) {
				txtProdutor.setText(fardaoSelected.getProdutor().getDescricao()
						.trim());
			}

			if (fardaoSelected.getVariedade() != null) {
				txtVariedade.setText(fardaoSelected.getVariedade()
						.getDescricao().trim());
			}

			if (fardaoSelected.getSafra() != null) {
				txtSafra.setText(fardaoSelected.getSafra().getDescricao()
						.trim());
			}

			if (fardaoSelected.getProdutor() != null) {
				txtProdutor.setText(fardaoSelected.getProdutor().getDescricao()
						.trim());
			}

			txtQtFardos.setText(String.valueOf(getFardaoSelected()
					.getQtd_Fardos()));

			txtPercentual.setText(df.format(fardaoSelected.getPercentual())
					+ "%");

			if (fardaoSelected.getConjunto() != null) {

				setConjuntoSelected(fardaoSelected.getConjunto());
				txtConjunto.setText(fardaoSelected.getConjunto()
						.getCodConjunto());
				posicionaConjunto(fardaoSelected.getConjunto());

				if (fardaoSelected.getPercentual() > fardaoSelected
						.getConjunto().getPercentualRendimento()) {
					txtPercentual.setBackground(Color.RED);
				} else if (fardaoSelected.getPercentual() > (90 * fardaoSelected
						.getConjunto().getPercentualRendimento() / 100)) {
					txtPercentual.setBackground(Color.yellow);
				} else {
					txtPercentual.setBackground(Color.GREEN);
				}

			} else {
				txtPercentual.setBackground(Color.GREEN);
				txtEan.setText("");
				txtEx.setText("");
				txtPrefixo.setText("");
				txtPrensa.setText("");
				txtConjunto.setText("");
			}
			carregarFardos();
		} else {
			LimpGridFardos();
			habilitarBotoes();
		}
	}

	private void PararProducao() {
		txtPesoBruto.setValue(0);
		statusProducao = StatusProducao.PARADA;
		setCodigoEtiqueta("");
		setCodigoFardo("");
		if (isThreadRunn()) {
			setThreadRunn(false);
		}

		habilitarBotoes();
	}

	public void EncerrarProducao() {
		if (fardaoSelected != null) {
			Object[] options = { "Cancelar", "Sim" };
			int showOptionDialog = JOptionPane.showOptionDialog(null,
					"Deseja finalizar a produção?", "Finalizar produção",
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
					null, options, options[0]);
			if (showOptionDialog == 1) {
				PararProducao();

				fardaoSelected.setStatusfardao(StatusFardao.BENEFICIADO);
				fardaoController.atualizar(fardaoSelected);
				LimpGridFardos();
				txtFardao.requestFocus();
			}

		} else {
			JOptionPane.showMessageDialog(getContentPane(),
					"Não há nenhum fardo em produção.", "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
		habilitarBotoes();
	}

	public void IniciarProducao() {

		if (statusProducao == StatusProducao.PARADA) {
			Object[] options = { "Sim", "Não" };
			int showOptionDialog = JOptionPane.showOptionDialog(null,
					"Deseja iniciar a produção?", "Iniciar produção",
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
					null, options, options[0]);
			if (showOptionDialog != 0) {
				return;
			}
		}

		if (ValidaDadosParaProducao()) {
			switch ((balancaDoUsuario.getModalidade())) {
			case 0:
				tipoPesagem = TipoPesagem.AUTOMATICA;
				break;
			case 1:
				tipoPesagem = TipoPesagem.MANUAL;
				break;
			case 2:
				tipoPesagem = TipoPesagem.AMBAS;
				break;
			}

			if (tipoPesagem == TipoPesagem.AUTOMATICA
					|| tipoPesagem == TipoPesagem.AMBAS) {
				try {
					Properties arqProperties;
					arqProperties = PropertiesUBA.getArqProperties();
					String arquivoPeso = arqProperties
							.getProperty("prop.lerArquivoBalanca");

					tempoDeleitura = Long.valueOf(arqProperties.getProperty(
							"prop.tempoDeleitura").trim());

					arquivoTxtPeso = new File(arquivoPeso);

				} catch (NegocioException e1) {
					JOptionPane.showMessageDialog(getContentPane(),
							e1.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
				}

				setThreadRunn(true);
				Thread thread = new Thread(new Runnable() {
					@Override
					public synchronized void run() {
						Double peso = 0.0;
						while (isThreadRunn()) {
							if (arquivoTxtPeso.exists()) {
								try {
									FileReader fileReader = new FileReader(
											arquivoTxtPeso);
									BufferedReader lerArq = new BufferedReader(
											fileReader);
									String linha;
									while ((linha = lerArq.readLine()) != null) {
										peso = Double.parseDouble(linha.trim());
									}
									fileReader.close();
									arquivoTxtPeso.delete();

								} catch (IOException e) {
									validaPeso = 0;
									e.printStackTrace();
								}

							} else {
								peso = 0.0;
							}

							if (pesoValido == 0) {
								pesoValido = peso;
							}

							if (pesoValido == peso) {
								validaPeso++;
								System.out.println("validando peso "
										+ validaPeso);
								if (validaPeso == 2) {
									txtPesoBruto.setValue(pesoValido);
									System.out.println(pesoValido);
									validaPeso = 0;
									pesoValido = 0;
								}
							} else {
								System.out.println("peso diferente " + peso);
								pesoValido = peso;
								validaPeso = 0;
							}

							try {
								Thread.sleep(tempoDeleitura);
							} catch (InterruptedException e) {
								validaPeso = 0;
								e.printStackTrace();
							}

						}
					}
				});
				thread.start();
				statusProducao = StatusProducao.PRODUZINDO;
				habilitarBotoes();
			} else {
				if (tipoPesagem == TipoPesagem.MANUAL) {
					statusProducao = StatusProducao.PRODUZINDO;
					habilitarBotoes();
				} else {
					JOptionPane
							.showMessageDialog(
									getContentPane(),
									"Usuário não tem permissão para efetuar pesagem manualmente.",
									"Erro", JOptionPane.ERROR_MESSAGE);
				}
			}
			txtEtiqueta.requestFocus();
		}

	}

	private boolean ValidaDadosParaProducao() {
		balancaDoUsuario = null;
		// verifica se tem fardao selecionado para iniciar producao
		if (fardaoSelected == null) {
			JOptionPane.showMessageDialog(getContentPane(),
					"Por favor, selecione um fardão para iniciar a produção.",
					"Erro", JOptionPane.ERROR_MESSAGE);
			txtFardao.requestFocus();
			return false;
		}
		// verifica se tem turno selecionado para iniciar producao
		if (TurnoSelected == null) {
			JOptionPane.showMessageDialog(getContentPane(),
					"Por favor selecione um turno para produção.", "Erro",
					JOptionPane.ERROR_MESSAGE);
			txtTurno.requestFocus();
			return false;
		}

		// verifica se tem conjunto selecionado para iniciar producao
		if (conjuntoSelected == null) {
			JOptionPane.showMessageDialog(getContentPane(),
					"Por favor selecione um conjunto para produção.", "Erro",
					JOptionPane.ERROR_MESSAGE);
			// //txtConjunto.requestFocus();
			return false;
		}
		balancaDoUsuario = new BalancaDoUsuario();
		balancaDoUsuario = UsuarioLogado.getBalanca();

		if (balancaDoUsuario == null) {
			JOptionPane.showMessageDialog(getContentPane(),
					"Não há nenhuma balança vinculada para esse usuário.",
					"Informativo", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		// if ((getFardaoSelected().getStatusfardao() !=
		// StatusFardao.BENEFICIADO)
		// && (getFardaoSelected().getPesoLiquido() == 0)) {
		// JOptionPane.showMessageDialog(null,
		// "Por favor informe o Peso Liquido do Fardão.");
		// txtPesoLiquidoFardao.setEditable(true);
		// txtPesoLiquidoFardao.requestFocus();
		// return false;
		// } else {
		// txtPesoLiquidoFardao.setEditable(false);
		// }

		return true;
	}

	private void desabilitarBotoes() {
		if (getFardaoSelected() != null) {
			btnIniciar.setEnabled(true);
			if (fardaoSelected.getStatusfardao() == StatusFardao.EMBENEFICIAMENTO) {
				btnEncerrar.setEnabled(true);
			} else {
				btnEncerrar.setEnabled(false);
			}
		} else {
			btnIniciar.setEnabled(false);
		}

		btnParar.setEnabled(false);
		txtPesoBruto.setEditable(false);
		txtPesoBruto.setValue(0);
		txtEtiqueta.setText("");
		txtFardao.setEditable(true);
		btnBuscaFardao.setEnabled(true);
	}

	private void habilitarBotoes() {
		if (statusProducao == StatusProducao.PARADA) {
			btnIniciar.setEnabled(true);
			btnBuscaFardao.setEnabled(true);
			txtFardao.setEnabled(true);
			btnParar.setEnabled(false);
			txtEtiqueta.setText("");
			txtPesoBruto.setValue(0);
			btnReimprimir.setEnabled(false);
			txtPesoBruto.setEnabled(false);
			txtPesoBruto.setEditable(false);
			// //btnConjunto.setEnabled(true);
			// txtConjunto.setEnabled(true);
			btnTurno.setEnabled(true);
			txtTurno.setEnabled(true);
			if (getFardaoSelected() != null
					&& getFardaoSelected().getStatusfardao() == StatusFardao.EMBENEFICIAMENTO) {
				btnEncerrar.setEnabled(true);
			} else {
				btnEncerrar.setEnabled(false);
			}
		} else if (statusProducao == StatusProducao.PRODUZINDO) {
			btnEncerrar.setEnabled(false);
			btnIniciar.setEnabled(false);
			btnBuscaFardao.setEnabled(false);
			txtFardao.setEnabled(false);
			btnParar.setEnabled(true);
			btnConjunto.setEnabled(false);
			// txtConjunto.setEnabled(false);
			btnTurno.setEnabled(false);
			txtTurno.setEnabled(false);
			if (tipoPesagem == TipoPesagem.AUTOMATICA) {
				txtPesoBruto.setEnabled(false);
				txtPesoBruto.setEditable(false);
				txtEtiqueta.requestFocus();
			} else {
				txtPesoBruto.setEnabled(true);
				txtPesoBruto.setEditable(true);
			}
		}
	}

	private void consultaFardao() {
		SelecionaFardao dialog = new SelecionaFardao();
		dialog.setModal(true);
		dialog.setLocationRelativeTo(getContentPane());
		dialog.setVisible(true);
		if (dialog.getFardaoSelected() != null) {
			setFardaoSelected(dialog.getFardaoSelected());
			carregaFardao();
			habilitarBotoes();
			txtFardao.requestFocus();
		} else {
			habilitarBotoes();
		}
	}

	public boolean salvarFardo() {
		double peso = Double.valueOf(txtPesoBruto.getText()
				.replaceAll(",", "."));
		Fardo fardo = new Fardo(getFardaoSelected(),
				chckbxConstelado.isSelected(), getCodigoEtiqueta(), peso);
		fardo.setConjunto(getConjuntoSelected());
		fardo.setTurno(getTurnoSelected());

		fardo.setProduto(getConjuntoSelected().getProduto());
		fardo.setPesoLiquido(fardo.getPesoBruto()
				- fardo.getConjunto().getPesoAdicional());

		if (!fardaoSelected.getStatusfardao().equals(StatusFardao.BENEFICIADO)) {
			fardaoSelected.setStatusfardao(StatusFardao.EMBENEFICIAMENTO);
			fardaoController.atualizar(fardaoSelected);
		}
		try {
			tableModel.addFardo(fardo);
			carregarFardos();
			carregaFardao();
			goToBotton();

			fardoSelected = fardo;
		} catch (NegocioException e) {
			setCodigoEtiqueta("");
			txtPesoBruto.setValue(0);
			setCodigoFardo("");
			txtEtiqueta.setText("");
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public void setFardaoSelected(Fardao fardaoSelected) {
		this.fardaoSelected = fardaoSelected;
	}

	public Fardao getFardaoSelected() {
		return fardaoSelected;
	}

	public boolean isThreadRunn() {
		return threadRunn;
	}

	public void setThreadRunn(boolean threadRunn) {
		this.threadRunn = threadRunn;
	}

	private boolean produzirFardo() {
		Double peso;
		carregaFardao();
		if (statusProducao.equals(StatusProducao.PARADA)) {
			JOptionPane.showMessageDialog(null, "Produção não foi iniciada",
					"Erro", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (txtEtiqueta.getText().length() != 6
				&& txtEtiqueta.getText().length() != 20) {
			txtEtiqueta.setText("");
			JOptionPane.showMessageDialog(null, "Código da etiqueta inválido.",
					"Erro", JOptionPane.ERROR_MESSAGE);
			txtEtiqueta.requestFocus();
			return false;
		}

		try {
			validaConjunto(validaCodSaiEtiqueta(), getConjuntoSelected());
		} catch (NegocioException e) {
			// //txtConjunto.requestFocus();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		peso = Double.valueOf(txtPesoBruto.getText().replaceAll(",", "."));
		if (peso > 0.0) {

			if (salvarFardo()) {
				if (chckbxImprimir.isSelected()) {
					try {
						PrintEtiqueta.Print(fardaoSelected.getProdutor(),
								fardoSelected);
					} catch (NegocioException e) {
						JOptionPane.showMessageDialog(getContentPane(),
								"Erro ao imprimir etiqueta", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
				if (tipoDeProducao.equals(TipoDeProducao.MANUAL)) {
					GerarCodigoEtiqueta();
				} else {
					txtEtiqueta.setText("");
					txtEtiqueta.requestFocus();
				}

				txtPesoBruto.setValue(0);
				return true;
			}
		} else {
			JOptionPane
					.showMessageDialog(
							getContentPane(),
							"Não foi detectado nenhum peso. Não é possivel efetuar produção",
							"Erro", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}

	private String validaCodSaiEtiqueta() throws NegocioException {
		if (txtEtiqueta.getText().length() == 20) {
			tipoDeProducao = TipoDeProducao.LEITOR;
			return txtEtiqueta.getText().substring(0, 13);
		} else if (txtEtiqueta.getText().length() == 6) {
			tipoDeProducao = TipoDeProducao.MANUAL;
			if ((txtEan.getText() + txtEx.getText() + txtPrefixo.getText() + txtPrensa
					.getText()).isEmpty()) {
				// //txtConjunto.requestFocus();
				throw new NegocioException("Código conjunto inválido.");
			} else {
				return txtEan.getText() + txtEx.getText()
						+ txtPrefixo.getText() + txtPrensa.getText();
			}
		}
		return "";
	}

	private boolean posicionaTurno() {
		TurnoController turnoController = new TurnoController();
		setTurnoSelected(turnoController.buscar(getTurnoSelected()));
		if (getTurnoSelected() != null) {
			return true;
		} else {
			return false;
		}

	}

	private void validaConjunto(String CodSai, Conjunto conjunto)
			throws NegocioException {
		if (tipoDeProducao.equals(TipoDeProducao.LEITOR)) {
			ConjuntoController conjuntoController = new ConjuntoController();
			// conjuntoController.buscaPorCodSai(CodSai);
			conjuntoController.verificaSeSaiPertenceConjunto(CodSai, conjunto);
		}
	}

	public boolean posicionaConjunto(Conjunto conjunto) {
		try {
			ConjuntoController conjuntoController = new ConjuntoController();
			Conjunto buscar = conjuntoController.buscar(conjunto);
			setConjuntoSelected(buscar);
			txtConjunto.setText(getConjuntoSelected().getCodConjunto());
			txtEan.setText(getConjuntoSelected().getCodSAI().substring(0, 2));
			txtEx.setText(getConjuntoSelected().getCodSAI().substring(2, 3));
			txtPrefixo.setText(getConjuntoSelected().getCodSAI().substring(3,
					12));
			txtPrensa.setText(getConjuntoSelected().getCodSAI().substring(12,
					13));
			if (fardaoSelected.getFardao() != null)
				fardaoSelected.setConjunto(conjuntoSelected);
			fardaoController.atualizar(getFardaoSelected());
			return true;
		} catch (NegocioException e) {
			txtEan.setText("");
			txtEx.setText("");
			txtPrefixo.setText("");
			txtPrensa.setText("");
			txtConjunto.setText("");
			txtConjunto.selectAll();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	private void LimpGridFardos() {
		tableModel.Limpar();
		txtPesoLiquidoFardao.setText("0");
		txtFazenda.setText("0");
		txtPesoLiquidoFardao.setText("0");
		txtPesoLiquidoFardos.setText("0");
		txtFazenda.setText("");
		txtProdutor.setText("");
		txtVariedade.setText("");
		txtSafra.setText("");
		txtProdutor.setText("");
		txtQtFardos.setText("0");
		txtPercentual.setText("0.00%");
		txtPercentual.setBackground(Color.green);
		habilitarBotoes();
	}

	public static void forcarSelecaoAoReceberFoco(Container containerInicial) {
		// Correção de erro
		if (containerInicial == null) {
			return;
		}
		// Obtém os componentes do container
		Component[] comps = containerInicial.getComponents();
		if (comps != null) {
			// Percorre os componentes do container
			for (Component comp : comps) {
				// O componente é um JTextField and ?
				if (comp instanceof JTextField) {
					// Instala um FocusListener para que todo o texto do
					// JTextField
					// seja selecionado quando o componente receber o foco
					((JTextField) comp).addFocusListener(new FocusListener() {
						@Override
						public void focusGained(FocusEvent evt) {
							Object source = evt.getSource();
							if (source instanceof JTextField) {
								((JTextField) source).selectAll();
								((JTextField) source).setBackground(new Color(
										176, 196, 222));
							}
						}

						@Override
						public void focusLost(FocusEvent evt) {
							Object source = evt.getSource();
							if (source instanceof JTextField) {
								((JTextField) source).selectAll();
								((JTextField) source)
										.setBackground(SystemColor.white);
							}
						}
					});
				} else {
					if (comp instanceof Container) {
						forcarSelecaoAoReceberFoco((Container) comp);
					}
				}
			}
		}
	}

	public String getCodigoEtiqueta() {
		if (tipoDeProducao == TipoDeProducao.MANUAL) {
			String codEtiqueta = txtEan.getText() + txtEx.getText()
					+ txtPrefixo.getText() + txtPrensa.getText()
					+ getCodigoFardo();
			codigoEtiqueta = (codEtiqueta + digitoVerificador(codEtiqueta));
		} else if (txtEtiqueta.getText().length() == 20) {
			codigoEtiqueta = txtEtiqueta.getText();
		}
		return codigoEtiqueta;
	}

	public void setCodigoEtiqueta(String Etiqueta) {
		codigoEtiqueta = Etiqueta;
	}

	public void GerarCodigoEtiqueta() {
		Integer codigoFardo = Integer.parseInt(getCodigoFardo()) + 1;
		setCodigoFardo(StringUtils.leftPad(String.valueOf(codigoFardo), 6, "0"));
		txtEtiqueta.setText(getCodigoFardo());
		String codEtiqueta = txtEan.getText() + txtEx.getText()
				+ txtPrefixo.getText() + txtPrensa.getText() + getCodigoFardo();

		setCodigoEtiqueta(codEtiqueta + digitoVerificador(codEtiqueta));

	}

	public String digitoVerificador(String etiqueta) {
		int multiplicador = 3;
		int soma = 0;
		int digito = 0;
		for (int i = 0; i <= etiqueta.length() - 1; i++) {
			int numero = (Integer.parseInt(String.valueOf(etiqueta.charAt(i))));
			soma += numero * multiplicador;
			if (multiplicador == 3) {
				multiplicador = 1;
			} else {
				multiplicador = 3;
			}
		}
		digito = ((soma % 10) > 0) ? (10 - (soma % 10)) : 0;
		return String.valueOf(digito);
	}

	public Conjunto getConjuntoSelected() {
		return conjuntoSelected;
	}

	public void setConjuntoSelected(Conjunto conjuntoSelected) {
		this.conjuntoSelected = conjuntoSelected;
	}

	public Turno getTurnoSelected() {
		return TurnoSelected;
	}

	public void setTurnoSelected(Turno turnoSelected) {
		TurnoSelected = turnoSelected;
	}

	public String getCodigoFardo() {
		return codigoFardo;
	}

	public void setCodigoFardo(String codigoFardo) {
		this.codigoFardo = codigoFardo;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("action");
	}
}
