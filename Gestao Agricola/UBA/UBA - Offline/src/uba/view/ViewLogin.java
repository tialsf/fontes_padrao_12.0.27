package uba.view;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import uba.controller.UsuarioController;
import uba.exceptions.NegocioException;
import uba.modelo.UsuarioLogado;
import uba.util.PropertiesUBA;

public class ViewLogin extends JFrame {

	private static final long serialVersionUID = 1L;

	private UsuarioController usuarioController;
	private final JTextField txtUsuario;
	private final JPasswordField txtSenha;
	private static ViewLogin viewLogin;
	private JButton btnOk;

	public ViewLogin() {
		super();
		setTitle("UBA - Login");
		setIconImage(getToolkit().createImage(
				getClass().getResource("/imagens/iconeAgro.png")));

		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(392, 259);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Dados", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Usuário:");
		lblNewLabel.setBounds(69, 36, 61, 16);
		panel.add(lblNewLabel);

		txtUsuario = new JTextField();
		txtUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					txtSenha.requestFocus();
				}
			}
		});
		txtUsuario.setBounds(69, 64, 134, 28);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnOk = new JButton("Acessar");
		btnOk.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER
						|| e.getKeyCode() == KeyEvent.VK_SPACE) {
					ValidarUsuario();
				}
			}
		});
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ValidarUsuario();
			}
		});

		btnOk.setBounds(69, 175, 117, 29);
		panel.add(btnOk);

		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setBounds(69, 95, 61, 16);
		panel.add(lblSenha);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.exit(1);
			}
		});
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnCancelar.setBounds(198, 175, 117, 29);
		panel.add(btnCancelar);

		txtSenha = new JPasswordField();
		txtSenha.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					btnOk.requestFocus();
				}
			}
		});
		txtSenha.setBounds(69, 123, 134, 28);
		panel.add(txtSenha);

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(ViewLogin.class
				.getResource("/imagens/fundo.png")));
		lblNewLabel_1.setBounds(241, 3, 145, 91);
		panel.add(lblNewLabel_1);
		initialize();
	}

	private void initialize() {
		usuarioController = new UsuarioController();
		setResizable(false);
	}

	private void ValidarUsuario() {
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		btnOk.setEnabled(false);

		if (usuarioController.autenticar(txtUsuario.getText(),
				txtSenha.getText())) {
			try {

				if (SelecionaBalancaUsuario()) {
					Main main = new Main();
					main.setVisible(true);
				} else {
					System.exit(1);
				}

			} catch (NegocioException e) {
				JOptionPane.showMessageDialog(getContentPane(), e.getMessage(),
						"Erro", JOptionPane.ERROR_MESSAGE);
				txtSenha.setText("");
				txtUsuario.setText("");
				txtUsuario.requestFocus(true);
			}

		} else {
			txtSenha.setText("");
			txtUsuario.setText("");
			txtUsuario.requestFocus(true);
		}
		setCursor(null);
		btnOk.setEnabled(true);
	}

	public boolean SelecionaBalancaUsuario() throws NegocioException {

		if (usuarioController.getBalancasUsauario(
				UsuarioLogado.getUsuarioLogado()).size() > 0) {
			SelecionaBalanca dialog = new SelecionaBalanca();
			dialog.setModal(true);
			dialog.setLocationRelativeTo(viewLogin);
			viewLogin.dispose();
			dialog.setVisible(true);

			if (dialog.getBalancaSelected() != null) {
				UsuarioLogado.setBalanca(dialog.getBalancaSelected());
				return true;
			} else {
				return false;
			}

		} else {
			throw new NegocioException(
					"Usuário não possui uma balança cadastrada.");
		}
	}

	public static void main(String[] args) {

		try {
			PropertiesUBA.createProperties();
		} catch (NegocioException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Erro",
					JOptionPane.ERROR_MESSAGE);
		}
		viewLogin = new ViewLogin();
		viewLogin.setVisible(true);
		viewLogin.setLocationRelativeTo(null);
	}
}