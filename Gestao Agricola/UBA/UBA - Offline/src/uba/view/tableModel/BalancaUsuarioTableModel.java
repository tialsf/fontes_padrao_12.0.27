package uba.view.tableModel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import uba.controller.UsuarioController;
import uba.modelo.BalancaDoUsuario;
import uba.modelo.Usuario;
import uba.modelo.tipos.TipoPesagem;

public class BalancaUsuarioTableModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final UsuarioController usuarioController;
	private List<BalancaDoUsuario> linhas;
	private final String[] colunas = new String[] { "Balança", "Modalidade" };

	private static final int BALANCA = 0;
	private static final int MODALIDADE = 1;

	// Cria sem nenhuma linha
	public BalancaUsuarioTableModel() {
		linhas = new ArrayList<BalancaDoUsuario>();
		usuarioController = new UsuarioController(); //
	}

	public void Listar(Usuario usuario) {
		linhas = usuarioController.getBalancasUsauario(usuario);
		fireTableDataChanged();
	}

	public void Limpar() {
		if (!linhas.isEmpty()) {
			linhas.clear();
			fireTableDataChanged();
		}
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case BALANCA:
			return String.class;
		case MODALIDADE:
			return Integer.class;
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getColumnName");
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		BalancaDoUsuario balanca = linhas.get(rowIndex);
		switch (columnIndex) {
		case BALANCA:
			return balanca.getDescricao();
		case MODALIDADE:
			switch ((balanca.getModalidade())) {
			case 0:
				return TipoPesagem.AUTOMATICA;
			case 1:
				return TipoPesagem.MANUAL;
			case 2:
				return TipoPesagem.AMBAS;
			}
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getValueAt");
		}
	}

	public BalancaDoUsuario getUsuarioBalanca(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

}
