package uba.view.tableModel;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import uba.controller.ConjuntoController;
import uba.modelo.Conjunto;

public class ConjuntoTableModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int CONJUNTO = 0;
	private static final int DESCRICAO = 1;
	private static final int CODSAI = 2;
	private static final int EAN = 3;
	private static final int DIGEXT = 4;
	private static final int PREFIX = 5;
	private static final int PRENSA = 6;

	private final ConjuntoController ConjuntoController;
	private final List<Conjunto> linhas;
	private final String[] colunas = new String[] { "Cod Conjunto",
			"Descrição", "Cod SAI", "EAN", "DIGEXT", "PREFIX", "PRENSA" };

	public ConjuntoTableModel() {

		ConjuntoController = new ConjuntoController();

		linhas = ConjuntoController.listar();
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case CONJUNTO:
			return String.class;
		case DESCRICAO:
			return String.class;
		case CODSAI:
			return String.class;
		case EAN:
			return String.class;
		case DIGEXT:
			return String.class;
		case PREFIX:
			return String.class;
		case PRENSA:
			return String.class;
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getColumnName");
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Conjunto Conjunto = linhas.get(rowIndex);

		switch (columnIndex) {
		case CONJUNTO:
			return Conjunto.getCodConjunto();
		case DESCRICAO:
			if (Conjunto.getDescricao() == null) {
				return "";
			} else {
				return Conjunto.getDescricao();
			}

		case CODSAI:
			if (Conjunto.getCodSAI() == null) {
				return "";
			} else {
				return Conjunto.getCodSAI();
			}
		case EAN:
			if (Conjunto.getCodSAI() == null) {
				return "";
			} else {
				return Conjunto.getCodSAI().substring(0, 2);
			}
		case DIGEXT:
			if (Conjunto.getCodSAI() == null) {
				return "";
			} else {
				return Conjunto.getCodSAI().substring(2, 3);
			}
		case PREFIX:
			if (Conjunto.getCodSAI() == null) {
				return "";
			} else {
				return Conjunto.getCodSAI().substring(3, 12);
			}
		case PRENSA:
			if (Conjunto.getCodSAI() == null) {
				return "";
			} else {
				return Conjunto.getCodSAI().substring(12, 13);
			}
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getValueAt");
		}
	}

	public Conjunto getSocio(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

}
