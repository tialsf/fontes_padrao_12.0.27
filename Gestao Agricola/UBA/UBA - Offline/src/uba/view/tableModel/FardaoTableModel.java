package uba.view.tableModel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import uba.controller.FardaoController;
import uba.modelo.Fardao;
import uba.modelo.tipos.StatusFardao;

public class FardaoTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private static final int FARDAO = 0;
	private static final int SAFRA = 1;
	private static final int PRODUTOR = 2;
	private static final int PESOLIQ = 3;
	private static final int PESOESTIMADO = 4;
	private static final int STATUS = 5;

	private final FardaoController fardaoController;
	private final List<Fardao> linhas;
	private final String[] colunas = new String[] { "Fardão", "Safra",
			"Produtor", "Peso Liquido", "Peso Previsto", "status" };

	public FardaoTableModel() {
		ArrayList<StatusFardao> status = new ArrayList<StatusFardao>();
		status.add(StatusFardao.PREVISTO);
		status.add(StatusFardao.EMROMANEIO);
		status.add(StatusFardao.DISPONIVEL);
		status.add(StatusFardao.EMBENEFICIAMENTO);

		fardaoController = new FardaoController();

		linhas = fardaoController.buscaPorStatus(status);
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case FARDAO:
			return String.class;
		case SAFRA:
			return String.class;
		case PRODUTOR:
			return String.class;
		case PESOLIQ:
			return double.class;
		case PESOESTIMADO:
			return double.class;
		case STATUS:
			return String.class;
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getColumnName");
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		Fardao fardao = linhas.get(rowIndex);

		switch (columnIndex) {
		case FARDAO:
			return fardao.getFardao();
		case SAFRA:
			if (fardao.getSafra() == null) {
				return "";
			} else {
				return fardao.getSafra().getCodSafra();
			}

		case PRODUTOR:
			if (fardao.getProduto() == null) {
				return "";
			} else {
				return fardao.getProduto().getDescricao();
			}
		case PESOLIQ:
			return decimalFormat.format(fardao.getPesoLiquido());
		case PESOESTIMADO:
			return decimalFormat.format(fardao.getPesoPrevisto());
		case STATUS:
			return fardao.getStatusfardao().toString();
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getValueAt");
		}
	}

	public Fardao getSocio(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

}
