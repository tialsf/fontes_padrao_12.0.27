package uba.view.tableModel;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import uba.controller.FardoController;
import uba.exceptions.NegocioException;
import uba.modelo.Fardao;
import uba.modelo.Fardo;

import com.sun.jmx.snmp.Timestamp;

public class FardoTableModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final FardoController fardoController;
	private List<Fardo> linhas;
	private final String[] colunas = new String[] { "Fardo", "Data", "Prensa",
			"Costelado", "Turno", "Peso Bruto", "Peso Liquido" };
	private Fardao fardao;
	private static final int FARDO = 0;
	private static final int DATA = 1;
	private static final int PRENSA = 2;
	private static final int COSTELADO = 3;
	private static final int TURNO = 4;
	private static final int PESOBRUTO = 5;
	private static final int PESOLIQUIDO = 6;

	// Cria sem nenhuma linha
	public FardoTableModel() {
		linhas = new ArrayList<Fardo>();
		fardoController = new FardoController();
	}

	public void Listar(Fardao fardao) {
		this.fardao = fardao;
		linhas = fardoController.listar(fardao);
		fireTableDataChanged();
	}

	public void Limpar() {
		if (!linhas.isEmpty()) {
			linhas.clear();
			fireTableDataChanged();
		}
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case DATA:
			return Timestamp.class;
		case FARDO:
			return String.class;
		case PRENSA:
			return String.class;
		case COSTELADO:
			return String.class;
		case TURNO:
			return String.class;
		case PESOBRUTO:
			return String.class;
		case PESOLIQUIDO:
			return String.class;
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getColumnName");
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd/MM/yyyy - HH:mm:ss");

		Fardo fardo = linhas.get(rowIndex);
		switch (columnIndex) {
		case DATA:
			Date date = new Date();
			if (fardo.getData() != null)
				date = fardo.getData().getTime();
			return simpleDateFormat.format(date);
		case FARDO:
			return fardo.getFardo();
		case PRENSA:
			return fardo.getConjunto().getCodSAI().substring(12, 13);
		case COSTELADO:
			return fardo.isConstelado() ? "Sim" : "Não";
		case TURNO:
			return fardo.getTurno().getCodTurno() + "-"
					+ fardo.getTurno().getDescricao();
		case PESOBRUTO:
			return decimalFormat.format(fardo.getPesoBruto());
		case PESOLIQUIDO:
			return decimalFormat.format(fardo.getPesoLiquido());
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getValueAt");
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Fardo fardo = linhas.get(rowIndex);
		if (columnIndex == PESOLIQUIDO) {
			fardo.setPesoLiquido((Double) aValue);
		} else if (columnIndex == PESOBRUTO) {
			fardo.setPesoBruto((Double) aValue);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	public Fardo getFardo(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

	public void addFardo(Fardo fardo) throws NegocioException {
		fardoController.salvar(fardo);
		linhas = fardoController.listar(fardao);
		int ultimoIndice = getRowCount() - 1;
		fireTableRowsInserted(ultimoIndice, ultimoIndice);
	}

}
