package uba.view.tableModel;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import uba.controller.TurnoController;
import uba.modelo.Turno;

public class TurnoTableModel extends AbstractTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int CODTURNO = 0;
	private static final int DESCRICAO = 1;

	private final TurnoController TurnoController;
	private final List<Turno> linhas;
	private final String[] colunas = new String[] { "Cod Turno", "Descrição" };

	public TurnoTableModel() {

		TurnoController = new TurnoController();

		linhas = TurnoController.listar();
	}

	@Override
	public int getRowCount() {
		return linhas.size();
	}

	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	};

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case CODTURNO:
			return String.class;
		case DESCRICAO:
			return String.class;
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getColumnName");
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Turno Turno = linhas.get(rowIndex);

		switch (columnIndex) {
		case CODTURNO:
			return Turno.getCodTurno();
		case DESCRICAO:
			if (Turno.getDescricao() == null) {
				return "";
			} else {
				return Turno.getDescricao();
			}
		default:
			throw new IndexOutOfBoundsException(
					"columnIndex out of bounds - getValueAt");
		}
	}

	public Turno getSocio(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

}
