﻿using EvalCryptoCOMLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RGiesecke.DllExport;
using System.Runtime.InteropServices;
using System.Diagnostics;
using evalService;
using System.ServiceModel;
using System.Threading;
using System.Net;

namespace DLLAssinaturaDigital
{
    public class DLLAssinaturaDigital
    {
        [DllExport("ExecInClientDLL", CallingConvention = CallingConvention.Cdecl)]
        public unsafe static int ExecInClientDLL(int ID, String aPar, byte* Buf, int nBuf)
        {
            //Debugger.Launch();

            DateTime now = DateTime.Now;
            string logName = string.Format("log_{0}{1}{2}{3}{4}{5}{6}.txt",
                now.Year,
                now.Month,
                now.Day,
                now.Hour,
                now.Minute,
                now.Second,
                now.Millisecond);

            if (ID == 1)
            {
                #region Assinatura Simples

                addLog(logName, "Iniciando Assinatura Digital Simples.");

                string assinatura = string.Empty;

                #region Converte arquivo a ser assinado para Base64

                byte[] AsBytes = File.ReadAllBytes(aPar);
                String arqConvertido = Convert.ToBase64String(AsBytes);

                addLog(logName, "Arquivo convertido para base64.");

                #endregion

                SignedData signedData = new SignedData();

                #region Preenche as propriedades do objeto de assinatura digital

                signedData.Data = arqConvertido;
                signedData.Signature = ""; // Nenhuma assinatura prévia informada 
                signedData.A3Filter = 0; // Filtro de certificado do tipo A3 desabilitado 
                signedData.RemoteURL = ""; // Nenhuma URL de assinatura remota configurada 
                signedData.CPFFilter = ""; // Nenhum CPF especificado para filtro 
                signedData.PolicyId = ""; // Nenhuma política definida 
                signedData.PolicyVersion = ""; // Nenhuma versão de política definida 
                signedData.Revocation = 3; // Não realiza verificação da revogação 
                signedData.ReleasePinAfterUse = 0; // Não libera o PIN após 1ª assinatura                 
                int base64 = 1; // Tipo de dado a ser assinado Base64 
                int detached = 1; // Tipo de assinatura Detached 
                int cosign = 0; // Não realiza co-assinatura 
                string label = ""; // Sem FriendlyName 
                int verifyType = 0; // Não verifica se o certificado do assinate é ICP-Brasil (permite utilizar certificado de teste) 
                int remoteSign = 0; // Assinatura local 
                int includeChain = 0; // Inclui cadeia do assinante na assinatura 
                int byPassCertSelect = 0; // Exibe tela de seleção de certificado

                addLog(logName, "Parâmetros de assinatura digital definidos.");

                #endregion

                addLog(logName, string.Format("Iniciando assinatura digital de {0}.", aPar));

                /* Realiza a assinatura digital do documento. */
                signedData.Sign(base64, detached, cosign, label, verifyType, remoteSign, includeChain, byPassCertSelect);

                addLog(logName, "Assinatura digital finalizada.");

                Thread.Sleep(1000);

                if (signedData.LastError > 0)
                {
                    addLog(logName, "Não houveram erros na assinatura.");

                    assinatura = signedData.Signature;

                    if (assinatura != null || !assinatura.Equals(string.Empty))
                    {
                        stringToByte(ref Buf, assinatura);
                        addLog(logName, "Retornando assinatuar digital.");

                        return 1;
                    }
                    else
                    {
                        stringToByte(ref Buf, string.Format("ERRO: Ocorreu um problema ao tentar assinar o documento."));
                        return -1;
                    }
                }
                else
                {
                    stringToByte(ref Buf, string.Format("ERRO {0} - {1}",
                       signedData.LastError,
                       signedData.GetErrorDescription(signedData.LastError)));

                    return -1;
                }

                #endregion
            }
            else if (ID == 2)
            {
                #region Assinatura em Lote

                try
                {
                    addLog(logName, "Assinatura em Lote - Execução iniciada.");

                    string nomeArquivo = string.Empty;
                    DateTime dtNow = DateTime.Now;
                    nomeArquivo = string.Format("assinaturaDigital_{0}{1}{2}{3}{4}{5}{6}.xml",
                        dtNow.Year,
                        dtNow.Month,
                        dtNow.Day,
                        dtNow.Hour,
                        dtNow.Minute,
                        dtNow.Second,
                        dtNow.Millisecond);

                    DirectoryInfo di = new DirectoryInfo(aPar);
                    StringBuilder sb = new StringBuilder();

                    addLog(logName, "Iniciando geração do XML em lote.");

                    sb.AppendLine("<INPUT>");

                    foreach (FileInfo file in di.GetFiles("*.pdf"))
                    {
                        addLog(logName, string.Format("Processando documento {0}.", file.Name));

                        sb.AppendLine("<DOCUMENT>");

                        sb.AppendFormat("<DOCID>{0}</DOCID>", file.Name);

                        sb.AppendLine();

                        byte[] AsBytes = File.ReadAllBytes(file.FullName);
                        String arqConvertido = Convert.ToBase64String(AsBytes);
                        sb.AppendFormat("<DOC>{0}</DOC>", arqConvertido);

                        sb.AppendLine("</DOCUMENT>");
                    }

                    sb.AppendLine("</INPUT>");

                    addLog(logName, "Geração do XML do lote finalizada.");

                    SignedData signedData = new SignedData();

                    signedData.Data = sb.ToString();
                    signedData.Signature = ""; // Nenhuma assinatura prévia informada 
                    signedData.A3Filter = 0; // Filtro de certificado do tipo A3 desabilitado 
                    signedData.RemoteURL = ""; // Nenhuma URL de assinatura remota configurada 
                    signedData.CPFFilter = ""; // Nenhum CPF especificado para filtro 
                    signedData.PolicyId = ""; // Nenhuma política definida 
                    signedData.PolicyVersion = ""; // Nenhuma versão de política definida 
                    signedData.Revocation = 3; // Não realiza verificação da revogação 
                    signedData.ReleasePinAfterUse = 0; // Não libera o PIN após 1ª assinatura                 
                    int base64 = 1; // Tipo de dado a ser assinado Base64 
                    int detached = 1; // Tipo de assinatura Detached 
                    int cosign = 0; // Não realiza co-assinatura 
                    string label = ""; // Sem FriendlyName 
                    int verifyType = 0; // Não verifica se o certificado do assinate é ICP-Brasil (permite utilizar certificado de teste) 
                    int remoteSign = 0; // Assinatura local 
                    int includeChain = 0; // Inclui cadeia do assinante na assinatura 
                    int byPassCertSelect = 0; // Exibe tela de seleção de certificado

                    addLog(logName, "Parâmetros para assinatura digital definidos.");

                    addLog(logName, "Iniciando assinatura digital do lote.");

                    signedData.BatchSign(base64, detached, cosign, label, verifyType, remoteSign, includeChain, byPassCertSelect);

                    addLog(logName, "Assinatura digital finalizada.");

                    Thread.Sleep(1000);

                    if (signedData.Signature != null || !signedData.Signature.Equals(string.Empty))
                    {
                        addLog(logName, "Iniciando geração do XML do lote das assinaturas digitais finalizada.");

                        File.WriteAllText(aPar.Trim() + nomeArquivo.Trim(), signedData.Signature);

                        addLog(logName, "Geração do XML do lote das assinaturas digitais finalizada.");

                        stringToByte(ref Buf, nomeArquivo);

                        addLog(logName, string.Format("Retornando nome do arquivo de lotes gerado. {0}", nomeArquivo));

                        return 1;
                    }
                    else
                    {
                        stringToByte(ref Buf, string.Format("ERRO {0} - {1}",
                        signedData.LastError,
                        signedData.GetErrorDescription(signedData.LastError)));

                        return -1;
                    }
                }
                catch (Exception ex)
                {
                    stringToByte(ref Buf, string.Format("Ocorreu um erro na assinatura digital em lote: {0} - {1}",
                        ex.Message,
                        ex.StackTrace));
                    return -1;
                }

                #endregion
            }
            else if (ID == 3)
            {
                #region Validar Assinatura

                string[] parametros = aPar.Split('|');

                string assinatura = parametros[0];
                string arq = parametros[1];
                string webServiceURL = parametros[2];
                string proxyAddress = parametros[3];
                string proxyUsername = parametros[4];
                string proxyPassword = parametros[5];
                string proxyDomain = parametros[6];

                addLog(logName, "Parâmetro - Assinatura: " + assinatura);
                addLog(logName, "Parâmetro - Arquivo: " + arq);
                addLog(logName, "Parâmetro - Endereço WebService: " + webServiceURL);
                addLog(logName, "Parâmetro - Endereço Proxy: " + proxyAddress);
                addLog(logName, "Parâmetro - Usuário Proxy: " + proxyUsername);
                addLog(logName, "Parâmetro - Senha Proxy: " + proxyPassword);
                addLog(logName, "Parâmetro - Domínio Proxy: " + proxyDomain);

                if (parametros == null || parametros.Length != 7)
                {
                    addLog(logName, "Parâmetros incorretos. Abortando a operação!");

                    stringToByte(ref Buf, "Os parâmetros necessários para realizar a verificação da assinatura digital estão incorretos.");
                    return -1;
                }

                byte[] AsBytes = File.ReadAllBytes(arq);
                string arqConvertido = Convert.ToBase64String(AsBytes);

                addLog(logName, "Convertendo arquivo para base64.");

                try
                {
                    WSCryptoServerSoapClient WSCrypto = CreateWebServiceInstance(webServiceURL);

                    addLog(logName, "Criada a instância para consumo do WebService.");

                    /* Se for informado um endereço de proxy utilizará a autenticação via proxy. */
                    if (!proxyAddress.Equals(string.Empty))
                    {   
                        WebProxy proxy = new WebProxy(proxyAddress);
                        proxy.Credentials = new NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
                        WebRequest.DefaultWebProxy = proxy;

                        addLog(logName, "Definidas as credenciais de proxy, pois o mesmo foi informado no arquivo de configurações.");
                    }

                    VerifyRequest verifyRequest = new VerifyRequest();
                    VerifyRequestBody body = new VerifyRequestBody();
                    RequestDocument requestDocument = new RequestDocument();
                    RequestSignature signatureToVerify = new RequestSignature();

                    signatureToVerify.Value = assinatura;
                    signatureToVerify.Type = RequestSignatureType.Detached;
                    requestDocument.SignatureList = new RequestSignature[] { signatureToVerify };

                    requestDocument.Content = arqConvertido;
                    verifyRequest.Body = body;

                    addLog(logName, "Atributos da mensagem SOAP definidos, iniciando chamada ao WebService de Validação de Assinatura finalizado.");

                    ResponseMessage response = WSCrypto.Verify("TOTVS", "VerifyBasicData", "Verify", new RequestDocument[] { requestDocument });

                    addLog(logName, "Chamada ao WebService de Validação de Assinatura finalizado.");

                    Thread.Sleep(1000);

                    if (response.MessageStatus.StatusId != 0)
                    {
                        addLog(logName, "Erro no retorno da Validação de Assinatura. Operação cancelada.");
                        stringToByte(ref Buf, response.MessageStatus.Message);
                        return -1;
                    }

                    string certificado = string.Empty;
                    foreach (ResponseDocument document in response.OptionalOutput.DocumentList)
                    {
                        if (document.SignatureList != null && document.SignatureList.Length > 0)
                        {
                            if (document.DocumentStatusEx.StatusId != 0)
                                certificado += (document.DocumentStatusEx.Message) + "\n\n";  // donotlocalize

                            certificado += "--------------------------------------------------------------------" +  // donotlocalize
                                          "\n---------------------------- Certificado(s) ------------------------";  // donotlocalize

                            for (int i = 0; i < document.SignatureList.Length; i++)
                            {
                                ResponseSignature responseSignature = document.SignatureList[i];

                                certificado += "\n\n---------- Certificado nº " + (i + 1) + " ----------" +  // donotlocalize
                                               "\nSubject: " + responseSignature.SignerList[0].Subject +  // donotlocalize
                                               "\nTipo de certificado: " + responseSignature.SignerList[0].CertificateType +  // donotlocalize
                                               "\nHash: " + responseSignature.SignerList[0].Hash +  // donotlocalize
                                               "\nICP Brasil: " + responseSignature.SignerList[0].IcpBrasil +  // donotlocalize
                                               "\nEntidade certificadora: " + responseSignature.SignerList[0].Issuer +  // donotlocalize
                                               "\nValidade de: " + responseSignature.SignerList[0].ValidFrom + // donotlocalize
                                               "\nValidade até: " + responseSignature.SignerList[0].ValidTo + // donotlocalize
                                               "\nData da assinatura: " + responseSignature.SignerList[0].SigningTime +  // donotlocalize
                                               "\nNúmero de série: " + responseSignature.SignerList[0].SerialNumber +  // donotlocalize
                                               "\nDocumento: " + responseSignature.SignerList[0].DocumentName +  // donotlocalize
                                               "\nData nascimento: " + responseSignature.SignerList[0].BirthDate +  // donotlocalize
                                               "\nCNPJ: " + responseSignature.SignerList[0].Cnpj +  // donotlocalize
                                               "\nCPF: " + responseSignature.SignerList[0].Cpf +  // donotlocalize
                                               "\nINSS: " + responseSignature.SignerList[0].INSS +  // donotlocalize
                                               "\nLocal do título: " + responseSignature.SignerList[0].LocalDoTitulo +  // donotlocalize
                                               "\nNome: " + responseSignature.SignerList[0].Name +  // donotlocalize
                                               "\nRazão Social: " + responseSignature.SignerList[0].RazaoSocial +  // donotlocalize
                                               "\nRg: " + responseSignature.SignerList[0].Rg +  // donotlocalize
                                               "\nSeção: " + responseSignature.SignerList[0].Secao +  // donotlocalize
                                               "\nTítulo: " + responseSignature.SignerList[0].TituloEleitor +  // donotlocalize
                                               "\nZona Eleitoral: " + responseSignature.SignerList[0].ZonaEleitoral;  // donotlocalize
                            }

                        }

                        if (!certificado.Equals(string.Empty))
                        {
                            addLog(logName, "Validação da Assinatura realizada com sucesso. Processo finalizado.");
                            stringToByte(ref Buf, certificado);
                            return 1;
                        }
                        else
                        {
                            stringToByte(ref Buf, "Não foi possível realizar a validação da assinatura digital");
                            return -1;
                        }
                    }

                    stringToByte(ref Buf, "Não foi possível realizar a validação da assinatura digital");
                    return -1;

                #endregion

                }
                catch (Exception ex)
                {
                    stringToByte(ref Buf, "Não foi possível chamar o WebService de validação:" + ex.Message);
                    return -1;
                }
            }
            else
            {
                stringToByte(ref Buf, "Método não encontrado! Método 1 (Parâmetro = Arquivo) - Assinatura Digital Simples; Método 2 (Parâmetro = Diretório) - Assinatura Digital em Lote");
                return -1;
            }
        }

        internal unsafe static WSCryptoServerSoapClient CreateWebServiceInstance(string webServiceURL)
        {
            BasicHttpBinding binding = new BasicHttpBinding();

            binding.MaxReceivedMessageSize = 64000000;
            binding.MaxBufferSize = 64000000;

            return new WSCryptoServerSoapClient(binding, new EndpointAddress(webServiceURL));
        }

        private unsafe static void stringToByte(ref byte* Buf, String s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                Buf[i] = (byte)s[i];
            }
        }

        private unsafe static void addLog(string fileName, string content)
        {
            File.AppendAllText(fileName,
            string.Format("[{0}] {1}", DateTime.Now.ToString(), content));
        }
    }
}