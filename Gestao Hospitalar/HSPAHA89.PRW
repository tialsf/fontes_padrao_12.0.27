#INCLUDE "HSPAHA89.ch"
#include "protheus.CH"
#include "colors.CH"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HSPAHA89  � Autor � Jose Orfeu         � Data �  Abril/2004 ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Planos                                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Administracao Hospitalar                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function HSPAHA89()
 Private cTab := "PL"
 Private cAlias := "SX5"
 Private cCadastro := STR0001 //"Planos do Convenio"
 Private nOpc    := 0
 Private aRotina := {{OemToAnsi(STR0002 ), "axPesqui"   , 0, 1}, ; //"Pesquisar"
                     {OemToAnsi(STR0003), 'HSAHA891(2)', 0, 2}, ; //"Visualizar"
                     {OemToAnsi(STR0004   ), 'HSAHA891(3)', 0, 3}, ; //"Incluir"
                     {OemToAnsi(STR0005   ), 'HSAHA891(4)', 0, 4, 2}, ; //"Alterar"
                     {OemToAnsi(STR0006   ), 'HSAHA891(5)', 0, 5, 1}} //"Excluir"

 DbSelectArea(cAlias)
 DbSetOrder(1)
 mBrowse(06, 01, 22, 75, "SX5",,,,,,,,,,,,,, "X5_TABELA = '"+cTab+"'")
 
 Return(Nil)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � HSPAHA891� Autor �  Manoel               � Data �17/10/2001���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de Inclusao/Alteracao/Visualizacao                  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function HSAHA891(nOpc)

 cSayCpo1 := STR0007 //"Plano"
 cSayCpo2 := STR0008 //"Descricao"

 HS_TelSx5('HS_Exc89()','HS_Check89()',nOpc)

Return

Function HS_Check89()
 If DbSeek(xFilial("SX5") + "PL" + M->X5_CHAVE)
  HS_MsgInf(STR0009, STR0010, STR0012) //"Plano ja cadastrado"###"Aten��o"###"Valida��o dos Campos"
  Return(.F.)
 Endif
Return(.T.)
                                                                                                     
// Funcao de Exclusao
Function HS_Exc89()                         
 Local lRet := .T., cAliasOld := Alias()

 DbSelectArea("GCM")
 DbSetOrder(2)
 If DbSeek(xFilial("GCM") + M->X5_CHAVE)
  HS_MsgInf(STR0011, STR0010, STR0013) //"Existe este plano relacionado a um convenio (GCM). Impossivel Exclui-lo!"###"Aten��o"###"Exclus�o n�o Permitida"
  lRet := .F.
 EndIf                 
 
 DbSelectArea(cAliasOld)
 If lRet 
  nOpca := 1
  oDlg:End()          
 EndIf
Return(lRet)