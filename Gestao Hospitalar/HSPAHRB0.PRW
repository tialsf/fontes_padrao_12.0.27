#INCLUDE "HSPAHRB0.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "TopConn.ch"
#define ESC          27
#define TRACE        repl("_",79)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HSPAHRB0  � Autor � MARCELO JOSE       � Data �  09/09/04   ���
�������������������������������������������������������������������������͹��
���Descricao � indicadores de produtividade.                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP7 IDE                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function HSPAHRB0()
 Local cDesc1         := STR0001 //"Este programa tem como objetivo imprimir relatorio "
 Local cDesc2         := STR0002 //"de acordo com os parametros informados pelo usuario."
 Local cDesc3         := STR0003 //"INDICADORES DE PRODUTIVIDADE"
 Local cPict          := ""
 Local titulo         := STR0003 //"INDICADORES DE PRODUTIVIDADE"
 Local nLin           := 80

 Local Cabec1         := ""
 Local Cabec2         := ""
 Local imprime        := .T.
 Local aOrd           := {}

 Private lEnd         := .F.
 Private lAbortPrint  := .F.
 Private limite       := 80
 Private tamanho      := "P"
 Private nomeprog     := "HSPAHRB0" // Coloque aqui o nome do programa para impressao no cabecalho
 Private nTipo        := 18
 Private aReturn      := {STR0004 , 1,STR0005, 2, 2, 1, "", 1}  //"Zebrado"###"Administracao"
 Private nLastKey     := 0
 Private cbtxt        := Space(10)
 Private cbcont       := 00
 Private CONTFL       := 01
 Private m_pag        := 01
 Private wnrel        := "HSPAHRB0" // Coloque aqui o nome do arquivo usado para impressao em disco
 Private cString      := "GAD"
 Private cPerg        := "HSPRA5"

 Private nPaciente    := 0
 Private nLeitos      := 0
 Private nDias        := 0
 Private nAlta        := 0
 Private nFunc        := 0
 
 FS_IniX1()

 If !Pergunte(cPerg,.T.)
 	return
 EndIf

 nDias := SubtHoras( MV_PAR01, "00:01" , MV_PAR02, "23:59" ) / 24

 // Monta a interface padrao com o usuario...
 wnrel := SetPrint(cString, NomeProg, cPerg, @titulo, cDesc1, cDesc2, cDesc3, .F., aOrd, .T., Tamanho,, .F.)

 Processa({|| FS_MontaM()})

 If nLastKey == ESC
 	Return(Nil)
 Endif

 SetDefault(aReturn, cString)

 If nLastKey == ESC
 	Return(Nil)
 Endif

 nTipo := If(aReturn[4] == 1, 15, 18)

 // Processamento RPTSTATUS monta janela com a regua de processamento.
 RptStatus({|| RunReport(Cabec1, Cabec2, Titulo, nLin)}, Titulo)
Return(Nil)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Funcao   � FS_MontaM() � Autor � MARCELO JOSE    � Data �  09/09/04   ���
�������������������������������������������������������������������������͹��
���Descricao � Monta matriz para impressao                                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP7 IDE                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_MontaM()

 DbSelectArea("GAV")
 nLeitos := RecCount() 
 
 DbSelectArea("SRA")
 nFunc   := RecCount()
 
 DbSelectArea("GAD")
 DbSetOrder(4)
  
 ProcRegua(RecCount()) // SETREGUA -> Indica quantos registros serao processados para a regua 

 DbSeek(xFilial("GAD") + DTOS(MV_PAR01), .T.)
                                                 
 While !Eof() .And. GAD->GAD_FILIAL == xFilial("GAD") .And. GAD->GAD_DATATE <= MV_PAR02
  
  IncProc(STR0006)  //"Aguarde, processando dados"
  
  nPaciente++
  
  If !Empty(GAD->GAD_TPALTA)
      nAlta++
  Endif

	 DbSkip() // Avanca o ponteiro do registro no arquivo
 End

Return(Nil)
//******************************************************************************************************************
//Funcao    RUNREPORT  Autor : AP6 IDE               Data   12/08/04                                               *
//Descricao Funcao auxiliar chamada pela RPTSTATUS. A funcao RPTSTATUS monta a janela com a regua de processamento.*
//Uso       Programa principal                                                                                     *
//******************************************************************************************************************
Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)
 
 Local nTaxaOcup := ( (nPaciente/nDias) / (nDias * nLeitos) ) * 100
 Local nMediaPer := ( (nPaciente/nDias) / nAlta )
 Local nLeitoOci := ( nTaxaOcup * nLeitos ) - nLeitos
 Local nLeitoFun := ( nLeitos - nLeitoOci ) / nFunc

 If lAbortPrint
	 @ nLin, 00 Psay STR0007
  Return(Nil)
  
	EndIf     // Verifica o cancelamento pelo usuario...
	
 //Impressao do cabecalho do relatorio. Salto de P�gina. Neste caso o formulario tem 55 linhas...
 	Cabec(Titulo, Cabec1, Cabec2, NomeProg, Tamanho, nTipo)

 @ 08,25 Psay STR0008
 @ 09,25 Psay STR0009
 @ 11,10 Psay STR0010
 @ 12,10 Psay STR0011
 @ 13,10 Psay STR0012
 @ 15,10 Psay "                    "+Str(nPaciente,5,0) + " / " + Str(nDias,3,0)
 @ 16,10 Psay STR0013
 @ 17,10 Psay "                    "+Str(nDias,5,0) + " * " + Str(nLeitos,3,0)
 @ 19,10 Psay STR0014 + Str(nTaxaOcup,6,2)+" %"
 @ 20,10 Psay "                   _________"
 @ 21,00 Psay TRACE
 @ 23,22 Psay STR0015
 @ 24,22 Psay STR0016
 @ 26,10 Psay STR0017
 @ 27,10 Psay STR0018
 @ 28,10 Psay STR0019
 @ 30,10 Psay "                         "+Str(nPaciente,5,0) + " / " + Str(nDias,3,0)
 @ 31,10 Psay STR0020
 @ 32,10 Psay "                           " + Str(nAlta,5,0)
 @ 34,10 Psay STR0021 + Str( nMediaPer ,6,2) + STR0022
 @ 35,10 Psay "                      ________"
 @ 36,00 Psay TRACE
 @ 38,10 Psay STR0023 + Str( nLeitoOci ,6,2) + STR0024
 @ 40,10 Psay STR0025 + Str( nLeitoFun ,6,2) + STR0026
 @ 41,00 Psay TRACE


 Roda( , ,tamanho)


 // Finaliza a execucao do relatorio...
 SET DEVICE TO SCREEN

 // Se impressao em disco, chama o gerenciador de impressao...
 If aReturn[5]==1
	 dbCommitAll()
	 SET PRINTER TO
	 OurSpool(wnrel)
 Endif

 MS_FLUSH()
Return(Nil)
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Funcao   � FS_IniX1()   � Autor � MARCELO JOSE   � Data �  09/09/04   ���
�������������������������������������������������������������������������͹��
���Descricao � Inicia SX1 p/receber parametros selecionados pelo usuario  ���
���          � Nil FS_IniX1(Nil)                                          ���
�������������������������������������������������������������������������͹��
���Uso       � AP7 IDE                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_IniX1()

 Local aHelpPor := {}
 Local aHelpSpa := {}
 Local aHelpEng := {}
 Local aRegs    := {}

 _sAlias := Alias()
 DbSelectArea("SX1")

 If MsSeek(cPerg) // Se encontrar a pergunta , n�o faz nada, pois ja foi criada.
 	DbSelectArea(_sAlias)
 	Return
 Endif

 // Da Data
 AADD(aHelpPor,"Informe a data INICIAL para a      ")
 AADD(aHelpPor,"pesquisa...             											")
 AADD(aHelpSpa,"                                   ")
 AADD(aHelpSpa,"              																					")
 AADD(aHelpEng,"                                   ")
 AADD(aHelpEng,"                                   ")
 AADD(aRegs,{STR0027,STR0027,STR0027,"mv_ch1","D",06,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","N","","",aHelpPor,aHelpSpa,aHelpEng})
 
 // Ate Data
 aHelpPor := {}
 aHelpSpa := {}
 aHelpEng := {}
 AADD(aHelpPor,"Informe a data FINAL para a        ")
 AADD(aHelpPor,"pesquisa...             											")
 AADD(aHelpSpa,"                                   ")
 AADD(aHelpSpa,"              																					")
 AADD(aHelpEng,"                                   ")
 AADD(aHelpEng,"                                   ")
 AADD(aRegs,{STR0028,STR0028,STR0028,"mv_ch2","D",06,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","N","","",aHelpPor,aHelpSpa,aHelpEng})

 AjustaSx1(cPerg, aRegs)
 DbSelectArea(_sAlias)
Return(Nil)
//******************************************************************************************************************
