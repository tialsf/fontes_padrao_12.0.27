#include "protheus.ch"      
#INCLUDE "TOPCONN.CH"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �  HSPCONV      � Autor � Saude            � Data �05/02/2007���
�������������������������������������������������������������������������Ĵ��
���Descricao �  Executa atualizadores na base do cliente                  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �  Nenhum                                                    ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �  Gestao Hospitalar                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function HSPCONV()
 Local cTitulo := OemToAnsi("Verifica e Atualiza os dicionarios do M�dulo de Gest�o Hospitalar")
 Local cLbxAtu  := ""
 Local aLbxAtu  := {}

 Local oTik := LoadBitmap( GetResources(), "LBTIK" )
 Local oNo  := LoadBitmap( GetResources(), "LBNO" )   
 
 Local oDlgAtu

 //Vetor com as informa��es do atualizadores que ser�o disponibilizados para o cliente
 //Coluna 1(l�gico)   = Registro selecionado ou nao
 //Coluna 2(caracter) = Data da Libera��o do BOPS com o atualizador
 //Coluna 3(caracter) = Nome da fun��o atualizadora
 //Coluna 4(caracter) = Nome das rotinas atualizadas
 If Len(aLbxAtu) == 0
  aLbxAtu := {{.F., "25/09/06", "GH105276()", "HSPAHM28"},;
              {.F., "16/03/07", "GH118766()", "HSPAHR62"},;
              {.F., "16/03/07", "GH119974()", "HSPAHA28/HSPFUM24"},;
              {.F., "20/03/07", "GH115598()", "HSPAHA12/HSPFFSXB/HSPFUNCA/HSPFUNCB"},;
              {.F., "22/03/07", "GH119106()", "HSPAHRB4"},;
              {.F., "23/03/07", "GH121786()", "HSPAHA47/HSPAHM01/HSPAHM02/HSPAHM24/HSPAHMA7/HSPAHR13/HSPFUM24/HSPFUNCB"},;
              {.F., "30/03/07", "GH122123()", "HSPAHA12/HSPAHA49/HSPAHA86/HSPAHM01/HSPAHM02/HSPAHM24/HSPFUNCC"},;
              {.F., "09/04/07", "GH111498()", "HSPAHM24/HSPFUNCA"},;
              {.F., "09/04/07", "GH120473()", "HSPAHA18/HSPAHM38/HSPAHM40/HSPAHP18"},;
              {.F., "09/04/07", "GH122124()", "HSPAHA47/HSPAHM01/HSPAHMA7/HSPFUNCB"},;
              {.F., "17/04/07", "GH122125()", "HSPAHA47/HSPAHMA7/HSPAHA58/HSPFUNCB/HSPAHABT/HSPAHA80/HSPAHA95/HSPAHM24/HSPAHM38/HSPAHP39/HSPAHP41/HSPAHP42/HSPAHR02/HSPAHR06/HSPAHR07/HSPAHR08/HSPAHR15/HSPAHR19/HSPAHR26/HSPAHR27/HSPAHR33/HSPAHR40/HSPAHR44/HSPAHR48/HSPAHR51/HSPAHR62/HSPAHRA3/HSPAHRA8/HSPAHRAA/HSPAHRAE/HSPAHRAF/HSPAHRAG/HSPAHRB7/HSPAHRC1/HSPAHRC3/HSPAHRC4/HSPAHRC9/HSPAHRD2/HSPAHRD3/HSPAHRD6/HSPAHRDD/HSPAHRDN/HSPAHR73/HSPAHR74/HAPAHR75/HSPAHRDR/HSPAHR52/HSPAHP18"},;
              {.F., "17/04/07", "GH121681()", "HSPAHM17"},;
              {.F., "17/04/07", "GH122075()", "HSPAHA86"},;
              {.F., "19/04/07", "GH123282()", "HSPAHAA7"},;
  	           {.F., "19/04/07", "GH124313()", "HSPAHRB4"},;
  	           {.F., "20/04/07", "GH120576()", "HSPFUNCA/HSPFUNCS/HSPAHP12/HSPRBASE/HSPAHM24"},;
  	           {.F., "23/04/07", "GH113630()", "HSPAHRDN"},;
  	           {.F., "25/04/07", "GH124660()", "HSPAHM01/HSPAHM02/HSPAHM24"},;
    	      	  {.F., "25/04/07", "GH123565()", "HSPAHA54/HSPAHA59/HSPAHA60/HSPAHA61/HSPAHA68/HSPAHA75/HSPAHA76/HSPAHA78/HSPAHA79/HSPAHAA4/HSPAHA19/HSPAHA04/HSPAHA16/HSPAHA28/HSPAHA99/HSPAHAA1/HSPAHA53/HSPAHA21/HSPFUM24"},;
         		   {.F., "26/04/07", "GH125035()", "HSPFUNCA"},;
         		   {.F., "04/05/07", "GH117125()", "HSPAHA52/HSPAHM38/HSPAHM40"},;
         		   {.F., "07/05/07", "GH125347()", "HSPAHA54/HSPAHA59/HSPAHA60/HSPAHA61/HSPAHA68/HSPAHA75/HSPAHA76/HSPAHA78/HSPAHA79/HSPAHAA4/HSPAHA19/HSPAHA04/HSPAHA16/HSPAHA28/HSPAHA99/HSPAHAA1/HSPAHA53/HSPAHA21/HSPFUM24/HSPAHA81/HSPAHA7/HSPAHA15/HSPAHA62"},;
         		   {.F., "09/05/07", "GH125066()", "HSPAHRB4"},;
         		   {.F., "10/05/07", "GH124241()", "HSPAHP39/HSPAHP42"},;
         		   {.F., "11/05/07", "GH120830()", "HSPAHM24/HSPFUM24"},;
              {.F., "15/05/07", "GH125771()", "HSPAHM29/HSPAHM39"},;
              {.F., "15/05/07", "GH125942()", "HSPAHM38/HSPAHM03"},;
              {.F., "21/05/07", "GH126460()", "NENHUM"},;
              {.F., "23/05/07", "GH126427()", "NENHUM"},;
              {.F., "23/05/07", "GH122058()", "HSPAHM52"},;
              {.F., "24/05/07", "GH126693()", "NENHUM"},;              
              {.F., "29/05/07", "GH126941()", "NENHUM"},;
              {.F., "29/05/07", "GH126872()", "NENHUM"},;
              {.F., "31/05/07", "GH127140()", "NENHUM"},;
              {.F., "05/06/07", "GH126862()", "HSPAHA81"},;
              {.F., "05/06/07", "GH124321()", "HSPAHA81"},;
              {.F., "11/06/07", "GH124558()", "HSPAHM34"},;
              {.F., "12/06/07", "GH125834()", "HSPAHM17"},;
              {.F., "12/06/07", "GH126806()", "HSPAHA58"},;
              {.F., "15/06/07", "GH127194()", "NENHUM"}  ,; 
              {.F., "18/06/07", "GH122296()", "HSPAHA17/HSPAHA22/HSPAHABQ/HSPAHABU/HSPAHA26/HSPAHA27/HSPAHA90/HSPAHA53/HSPAHR22/HSPAHR90/HSPFFSXB"},; 
              {.F., "19/06/07", "GH127833()", "HSPAHM17"},;
              {.F., "22/06/07", "GH123536()", "HSPAHMA7/HSPAHR25"},;
              {.F., "28/06/07", "GH121320()", "HSPAHM24/HSPAHP12/HSPFUNCC/HSPAHP18/HSPRBASE/HSPAHM50/HSPFFSXB/HSPAHABX/HSPAHABV/HSPFUM24"},;
              {.F., "02/07/07", "GH127847()", "NENHUM"},;
              {.F., "02/07/07", "GH124264()", "HSPAHM29/HSPAHA97"},;
              {.F., "03/07/07", "GH128438()", "HSPAHP12"},;
              {.F., "03/07/07", "GH128524()", "HSPAHR65"},;
              {.F., "04/07/07", "GH112979()", "HSPAHM30"},;
              {.F., "05/07/07", "GH127673()", "HSPAHA31"},;
              {.F., "06/07/07", "GH128476()", "HSPAHM24/HSPFUNCC/HSPFFSXB"},;
              {.F., "09/07/07", "GH126771()", "HSPAHM24/HSPAHM30"},;
              {.F., "10/07/07", "GH127240()", "HSPAHM24"},;
              {.F., "10/07/07", "GH122713()", "HSPAHA58"},;
              {.F., "12/07/07", "GH128718()", "HSPAHMA7"},;
              {.F., "12/07/07", "GH122256()", "HSPAHM29/HSPFFSXB/HSPFUNCB"},;
              {.F., "13/07/07", "GH127977()", "NENHUM"},;
              {.F., "13/07/07", "GH121680()", "HSPAHP12/HSPAHM24/HSPFUM24/HSPAHM50/HSPAHABM"},;
              {.F., "17/07/07", "GH126937()", "HSPAHM24"},;
              {.F., "18/07/07", "GH129168()", "HSPAHA81"},;
              {.F., "20/07/07", "GH128862()", "HSPAHA76/HSPAHM24/HSPAHP12"},;
              {.F., "20/07/07", "GH128424()", "HSPAHA58/HSPAHM04/HSPAHM30"},;
              {.F., "23/07/07", "GH126285()", "HSPAHM38/HSPAHM40/HSPFFSXB"},;
              {.F., "24/07/07", "GH118003()", "HSPAHR28"},;
              {.F., "27/07/07", "GH117855()", "HSPAHA18"},;
              {.F., "27/07/07", "GH118812()", "HSPAHR37"},;
              {.F., "27/07/07", "GH129106()", "HSPAHR29/HSPAHR31/HSPAHR34"},;
              {.F., "27/07/07", "GH129453()", "HSPAHA32/HSPAHM34/HSPFUNCB"},;
              {.F., "27/07/07", "GH126083()", "HSPAHRC6"},;
              {.F., "28/07/07", "GH128871()", "HSPAHA15/HSPAHA81/HSPAHAA7/HSPAHABZ/HSPAHAC0/HSPAHM24/HSPAHP12/HSPFUNCA/HSPFUNCS"},;
              {.F., "30/07/07", "GH129552()", "HSPFUNCC/HSPAHM24"},;
              {.F., "30/07/07", "GH128884()", "HSPAHR26"},;
              {.F., "30/07/07", "GH128985()", "HSPAHRB4"},;
              {.F., "06/08/07", "GH128875()", "HSPAHP12/HSPAHR15/HSPAHRC6/HSPAHRD3"},;
              {.F., "07/08/07", "GH128065()", "HSPAHA81/HSPAHAA7"},;
              {.F., "08/08/07", "GH130396()", "HSPAHM39"},;
              {.F., "09/08/07", "GH128815()", "HSPAHR39"},;
              {.F., "09/08/07", "GH127231()", "HSPAHP12"},;
              {.F., "13/08/07", "GH129174()", "HSPAHM30/HSPAHM35"},;
              {.F., "13/08/07", "GH130427()", "HSPAHM24/HSPAHA18"},;
              {.F., "14/08/07", "GH112909()", "HSPFUM24/HSPFUNCA/HSPAHA58/HSPAHM30/HSPAHM04"},;
              {.F., "14/08/07", "GH127464()", "HSPAHM52/HSPFUEDI"},;
              {.F., "15/08/07", "GH130922()", "HSPAHR37"},;
              {.F., "15/08/07", "GH130983()", "HSPAHR47"},;
              {.F., "16/08/07", "GH130584()", "HSPAHRC6"},;
              {.F., "17/08/07", "GH128898()", "HSPFUNCB"},;
              {.F., "20/08/07", "GH129764()", "HSPAHM04/HSPAHM05/HSPFUNCS"},;
              {.F., "21/08/07", "GH129645()", "NENHUM"},;
              {.F., "22/08/07", "GH124297()", "HSPAHM29"},;
              {.F., "22/08/07", "GH131533()", "NENHUM"},;
              {.F., "23/08/07", "GH128901()", "HSPAHA58/HSPAHAC1/HSPAHM24/HSPFFSXB/HSPFUNCA/HSPFUNCS/HSPFUM24"},;
              {.F., "23/08/07", "GH131285()", "HSPAHA28"},;
              {.F., "24/08/07", "GH131703()", "HSPAHR15"},;
              {.F., "31/08/07", "GH130347()", "NENHUM"},;
              {.F., "03/09/07", "GH130488()", "HSPAHM24"},;
              {.F., "05/09/07", "GH131494()", "HSPAHRDS"},;
              {.F., "11/09/07", "GH132472()", "NENHUM"},;
              {.F., "11/09/07", "GH132415()", "HSPAHM04/HSPAHR03"},;
              {.F., "11/09/07", "GH131817()", "HSPAHR15"},;
              {.F., "17/09/07", "GH130890()", "HSPAHR39"},;
              {.F., "19/09/07", "GH129835()", "HSPAHR45"},;
              {.F., "18/09/07", "GH133060()", "HSPAHR26"},;
              {.F., "19/09/07", "GH132987()", "HSPAHR51"},;
              {.F., "20/09/07", "GH130614()", "HSPAHM04/HSPAHM05"},;
              {.F., "17/09/07", "GH131180()", "HSPAHRAA"},;
              {.F., "26/09/07", "GH132777()", "HSPAHA18"},;
              {.F., "27/09/07", "GH128061()", "HSPAHA18/HSPAHA80/HSPAHAC2/HSPAHM24/HSPAHM30/HSPAHM50/HSPAHP12/HSPAHP18/HSPAHR15/HSPAHRC6/HSPAHRD3/HSPFUM24/HSPFUNCA/HSPFUNCC/HSPFUNCS"},;
              {.F., "02/10/07", "GH133732()", "HSPAHA18"},;
              {.F., "03/10/07", "GH127230()", "HSPAHR32"},;        
              {.F., "03/10/07", "GH133192()", "NENHUM"},;
              {.F., "04/10/07", "GH133932()", "NENHUM"},;
              {.F., "04/10/07", "GH133032()", "HSPAHA18"},;
              {.F., "05/10/07", "GH134245()", "NENHUM"},;
              {.F., "09/10/07", "GH134213()", "NENHUM"},;
              {.F., "10/10/07", "GH134147()", "HSPAHABX/HSPFFSXB"},;
              {.F., "10/10/07", "GH134436()", "HSPAHRB4"},;
              {.F., "17/10/07", "GH112623()", "HSPAHR71"},;
              {.F., "17/10/07", "GH132899()", "HSPAHR58"},;
              {.F., "18/10/07", "GH134695()", "HSPAHA18"},;
              {.F., "19/10/07", "GH133833()", "HSPAHA32/HSPAHM34/HSPAHA35/HSPFUNCA/HSPAHAC3/HSPAHR55/HSPAHR56/HSPAHR59"},;
              {.F., "23/10/07", "GH134664()", "HSPAHM38/HSPFUNCA/HSPAHM24/HSPFUNCS"},;
              {.F., "24/10/07", "GH134904()", "HSPAHRC6/HSPAHRD3/HSPFUNCS"},;   
              {.F., "24/10/07", "GH133870()", "HSPAHM24/HSPAHP12/HSPFUNCA/HSPFUNCC/HSPFUNCS/HSPAHR15/HSPAHR63"},;
              {.F., "30/10/07", "GH132904()", "HSPAHA18"},;
              {.F., "30/10/07", "GH134946()", "HSPAHM34/HSPAHR60"},;
              {.F., "30/10/07", "GH134793()", "NENHUM"},;
              {.F., "31/10/07", "GH135488()", "HSPAHA32"},;
              {.F., "01/11/07", "GH134582()", "HSPAHR41/HSPAHR42"},;
              {.F., "01/11/07", "GH131949()", "HSPAHR71"},;
              {.F., "05/11/07", "GH135645()", "HSPAHR15"},;
              {.F., "14/11/07", "GH135075()", "HSPAHR45"},;
              {.F., "21/11/07", "GH129309()", "HSPAHAC4/HSPAHAC5/HSPAHAC6/HSPAHAC7/HAPSHA35/HSPFUNCA/HSPAHM24/HSPVLM24/HSPFUNCC/HSPAHM34/HSPAHM38/HSPAHRB4/HSPAHRD7/HSPAHRD3/HSPAHR35/HSPAHRC6/HSPAHR15/HSPAHA18"},;
              {.F., "23/11/07", "GH130670()", "HSPAHA12"},;
              {.F., "27/11/07", "GH136299()", "HSPAHA58"},;                             
              {.F., "29/11/07", "GH133859()", "HSPAHA86"},;
              {.F., "06/12/07", "GH135726()", "HSPAHM09/HSPAHM24/HSPAHM30/HSPAHM05/HSPFFSXB/HSPAHR04/HSPAHA12/HSPAHA03"},;
              {.F., "07/12/07", "GH136636()", "HSPAHABJ/HSPAHM29/HSPFFSXB/HSPFUNC"},;
              {.F., "10/12/07", "GH136653()", "HSPAHM38/HSPAHM34/HSPAHAC3/HSPAHP12/HSPFUNCA"},;
              {.F., "11/12/07", "GH137284()", "HSPAHP12"},; 
              {.F., "11/12/07", "GH136451()", "HSPAHR67"},;
              {.F., "14/12/07", "GH137773()", "HSPAHR57"},;
              {.F., "14/12/07", "GH137828()", "HSPAHR58"},;              
              {.F., "14/12/07", "GH136629()", "HSPAHM05"},;              
              {.F., "14/12/07", "GH134660()", "HSPAHM24/HSPFUNCA/HSPFUM24/HSPFUNCD"},;
              {.F., "17/12/07", "GH136980()", "HSPAHRD6/HSPAHRD8/HSPAHRDB"},;
              {.F., "19/12/07", "GH135460()", "HSPAHM17/HSPFFSXB/HSPAHM24/HSPAHAC8"},;
              {.F., "19/12/07", "GH137796()", "HSPAHR68"},;
              {.F., "20/12/07", "GH134441()", "HSPAHA18/HSPAHA51/HSPAHM50/HSPAHP18/HSPFFSXB/HSPFUEDI/HSPFUNCC"},;
              {.F., "20/12/07", "GH137676()", "HSPAHRC4/HSPFUNCB/HSPAHABQ"},;
              {.F., "20/12/07", "GH135065()", "HSPAHM34/HSPAHM38"},;
              {.F., "21/12/07", "GH136450()", "HSPAHR66"},;
              {.F., "21/12/07", "GH137837()", "HSPAHM30"},;
              {.F., "26/12/07", "GH134492()", "HSPAHR71"},;
              {.F., "26/12/07", "GH137582()", "HSPAHA81"},;
              {.F., "27/12/07", "GH138377()", "NENHUM"},;
              {.F., "28/12/07", "GH135147()", "HSPAHMA7"},;
              {.F., "21/12/07", "GH136490()", "HSPFUNCS"},;
              {.F., "21/12/07", "GH137796()", "HSPAHR68"},;
              {.F., "03/01/08", "GH138568()", "HSPAHR78"},;
              {.F., "03/01/08", "GH135910()", "HSPAHA12"},;
              {.F., "03/01/08", "GH133847()", "NENHUM"},;
              {.F., "11/01/08", "GH133602()", "HSPAHM04"},;
              {.F., "14/01/08", "GH137080()", "HSPAHM50/HSPAHM24/HSPFFSXB"},;
              {.F., "18/01/08", "GH137072()", "HSPAHM24/HSPAHM30/HSPFUM24/HSPVLM24"},;
              {.F., "18/01/08", "GH137937()", "HSPAHRAD"},;
              {.F., "18/01/08", "GH139405()", "NENHUM"},;
              {.F., "21/01/08", "GH134019()", "HSPAHM05/HSPAHR03"},;
              {.F., "21/01/08", "GH137073()", "HSPAHA58/HSPAHR61/HSPFUM24"},;
              {.F., "24/01/08", "GH138836()", "HSPAHM38/HSPAHM40/HSPAHRD6"},;
              {.F., "25/01/08", "GH139659()", "HSPAHR76"},;
              {.F., "31/01/08", "GH139990()", "HSPAHA86/HSPFUNCB"},;
              {.F., "31/01/08", "GH137076()", "HSPAHM24/HSPAHM30/HSPAHA80"},;
              {.F., "06/02/08", "GH140354()", "NENHUM"},;
              {.F., "06/02/08", "GH138950()", "HSPAHA18/HSPAHM29/HSPFUNCA"},;
              {.F., "06/02/08", "GH136876()", "HSPAHA76/HSPFUNCC"},;
              {.F., "11/02/08", "GH137081()", "HSPFUNCC"},;
              {.F., "18/02/08", "GH140800()", "HSPAHA76/HSPFUNCC"},;
              {.F., "18/02/08", "GH140621()", "HSPAHA53"},;
              {.F., "18/02/08", "GH140467()", "HSPAHACA/HSPAHAB9/HSPAHA53"},;
              {.F., "03/03/08", "GH140587()", "HSPAHMA7/HSPAHABT/HSPFUNCB/HSPAHA47"},;
              {.F., "04/03/08", "GH141170()", "HSPAHP44/HSPFITXT"},;
              {.F., "13/03/08", "GH140073()", "HSPAHM05/HSPFUNCA"},;
              {.F., "14/03/08", "GH140847()", "HSPAHA53/HSPAHA90/HSPAHABQ/HSPAHR22/HSPFFSXB/HSPFUNCB"},;
              {.F., "18/03/08", "GH140890()", "HSPAHACB/HSPAHP44"},;
              {.F., "18/03/08", "GH141486()", "HSPAHM08/HSPFUNCC/HSPAHM03/HSPAHMA7/HSPAHM17/HSPFUM24/HSPAHM24/HSPAHP12/HSPAHM30"},;
              {.F., "20/03/08", "GH134726()", "NENHUM"},;
              {.F., "24/03/08", "GH140942()", "HSPAHA18"},;
              {.F., "25/03/08", "GH140992()", "HSPAHR78"},;
              {.F., "02/04/08", "GH143144()", "HSPAHA76"},;
              {.F., "07/04/08", "GH140949()", "HSPAHA12/HSPAHM04/HSPAHM05/HSPAHM09/HSPAHM30/HSPFUNCA"},;
              {.F., "08/04/08", "GH143361()", "HSPAHA53/HSPAHABQ/HSPAHA90/HSPFFSXB/HSPAHR22"},;
              {.F., "09/04/08", "GH141979()", "HSPAHM24/HSPAHM30/HSPAHMA7/HSPAHM35"},;
              {.F., "14/04/08", "GH144035()", "HSPAHM30/HSPAHMA7/HSPAHABQ"},;
              {.F., "18/04/08", "GH143469()", "HSPAHRDU"},;
              {.F., "22/04/08", "GH145340()", "HSPAHA99/HSPAHP12/HSPAHM24/HSPAHM17/HSPAHM29/HSPAHA18/HSPAHP18/HSPAHR09/HSPAHR15/HSPAHR63/HSPAHR78/HSPAHRAE/HSPAHRC6/HSPAHRD3/HSPAHRD7/HSPAHRDN/HSPAHRDU/HSPAHRDV/HSPAHR39"},;
              {.F., "28/04/08", "GH140116()", "HSPAHR77"},;
              {.F., "30/04/08", "GH145157()", "NENHUM"},;
              {.F., "08/05/08", "GH144067()", "HSPAHA11/HSPAHM08/HSPAHM30/HSPAHMA7/HSPFUM24/HSPFUNCC/HSPFUNCD/HSPAHM03/HSPAHR80/HSPAHM17"},;
              {.F., "14/05/08", "GH145718()", "HSPFFSXB"},;
              {.F., "14/05/08", "GH144713()", "HSPAHRDV"},;
              {.F., "19/05/08", "GH144712()", "HSPAHR66"},;
              {.F., "23/05/08", "GH139372()", "HSPAHR81"},;
              {.F., "23/05/08", "GH136510()", "HSPFUNCA/HSPFUNCC/HSPFUNCD/HSPFUM24/HSPAHM30/HSPAHM52/HSPAHABS"},;
              {.F., "26/05/08", "GH145655()", "HSPAHM24/HSPFUNCA"},;
              {.F., "27/05/08", "GH138972()", "HSPAHRDI"},;
              {.F., "28/05/08", "GH144708()", "HSPAHR58"},;
              {.F., "30/05/08", "GH138842()", "HSPAHR82"},;
              {.F., "03/06/08", "GH138842()", "NENHUM"},;
              {.F., "05/06/08", "GH140581()", "NENHUM"},;
              {.F., "05/06/08", "GH144714()", "HSPAHR79"},;                                  
              {.F., "10/06/08", "GH147001()", "HSPAHR92"},;
														{.F., "16/06/08", "GH138857()", "HSPAHR27"},;
			 									 {.F., "17/06/08", "GH145668()", "HSPAHM29"},;
												  {.F., "17/06/08", "GH144007()", "NENHUM"},;
												  {.F., "19/06/08", "GH148151()", "HSPAHM38"},;
												  {.F., "01/07/08", "GH148116()", "NENHUM"},;
												  {.F., "03/07/08", "GH148108()", "HSPAHM17"},;
												  {.F., "04/07/08", "GH145489()", "HSPAHA80"},;
												  {.F., "04/07/08", "GH101019()", "NENHUM"},;
			 									 {.F., "10/07/08", "GH148160()", "HSPAHM52/HSPAHP18/HSPFUEDI"},;
												  {.F., "14/07/08", "GH149028()", "HSPAHC02"},;
												  {.F., "15/07/08", "GH144262()", "HSPAHA53/HSPFFSXB/HSPAHM33/HSPAHA90/HSPAHACE/HSPAHRC4/HSPAHA31/HSPAHABQ/HSPAHM10/HSPAHR22/HSPFUNCA/HSPAHM05/HSPAHM30/HSPAHA12/HSPAHACF/HSPAHM24"},;
												  {.F., "28/07/08", "GH148987()", "HSPFFSXB"},;
												  {.F., "04/08/08", "GH150030()", "HSPAHM24"},;
												  {.F., "06/08/08", "GH147275()", "HSPM24PA"},;
												  {.F., "07/08/08", "GH149904()", "HSPAHR85"},; 
												  {.F., "07/08/08", "GH150620()", "HSPAHR84"},;
												  {.F., "08/08/08", "GH150303()", "HSPAHR82"},;												  
 												 {.F., "13/08/08", "GH150500()", "HSPAHR29/HSPAHR31/HSPAHR34"},;
 												 {.F., "13/08/08", "GH150193()", "NENHUM"},;
 												 {.F., "21/08/08", "GH150885()", "HSPFUNCC"},;
												  {.F., "22/08/08", "GH150476()", "HSPAHM10/HSPAHACE/HSPAHM30/HSPAHMA7/HSPAHM11/HSPAHM33/HSPAHABQ"},;
												  {.F., "26/08/08", "GH149544()", "HSPAHM30/HSPAHMA7/HSPFUNCA/HSPAHA58"},;
												  {.F., "26/08/08", "GH152415()", "NENHUM"},;
												  {.F., "27/08/08", "GH152633()", "HSPAHA06"},;
												  {.F., "02/09/08", "GH152038()", "HSPAHM38"},;
												  {.F., "05/09/08", "GH152046()", "HSPAHM38"},;
												  {.F., "11/09/08", "GH149650()", "HSPFUNCC/HSPFUEDI"},;
            	 {.F., "18/09/08", "GH149550()", "HSPFUNCA/HSPAHR58"},;
												  {.F., "08/09/08", "GH151779()", "HSPFUNCS"},;
												  {.F., "08/09/08", "GH141079()", "HSPAHP12/HSPFUNCC"},;
												  {.F., "23/09/08", "GH153363()", "HSPAHABQ/HSPAHM05/HSPAHM10"},;
												  {.F., "29/09/08", "GH153438()", "HSPFUNCA"},;
												  {.F., "01/10/08", "GH139951()", "HSPAHM24/HSPFFSXB"},;
												  {.F., "01/10/08", "GH144796()", "HSPFUNCA"},;
												  {.F., "13/10/08", "GH151635()", "HSPAHM24"},;
												  {.F., "13/10/08", "GH144609()", "NENHUM"},;
												  {.F., "21/10/08", "GH155315()", "NENHUM"},;
												  {.F., "22/10/08", "GH153435()", "HSPAHABQ/HSPAHM04/HSPAHM05/HSPAHM10"},;
												  {.F., "27/10/08", "GH153759()", "HSPAHM38/HSPAHM40/HSPFUNCA"},;
 												  {.F., "03/11/08", "GH140236()", "SUS"}, ;
 												  {.F., "18/11/08", "GH153697()", "HSPFUNCA/HSPAHA81"},;
   												  {.F., "10/02/09", "GH147276()", "HSPAHP02/HSPFUNCC/HSPFUEDI/HSPAHM39/HSPAHM24"}}

 EndIf

 aSort(aLbxAtu,,, {|x, y| CTOD(x[2], "DDMMYY") < CTOD(y[2], "DDMMYY") })
 
 nOpca := 0      
 DEFINE MSDIALOG oDlgAtu TITLE cTitulo From 000, 000 to 400, 470 PIXEL of oDlgAtu
  @ 015, 005 SAY OemToAnsi("Rotinas de atualiza��o dos Dicion�rios")                OF oDlgAtu PIXEL COLOR CLR_HBLUE
  @ 025, 005 SAY OemToAnsi("Selecione os atualizadores que deseja executar:")       OF oDlgAtu PIXEL COLOR CLR_RED
  
  @ 035, 004 LISTBOX oLbxAtu VAR cLbxAtu FIELDS HEADER " ", "Data Libera��o", "Nome Atualizador", "Fontes Atualizados" COLSIZES 10, 40, 70, 800 SIZE 225, 160 OF oDlgAtu PIXEL ;
             ON DBLCLICK (aLbxAtu[oLbxAtu:nAt, 01] := !aLbxAtu[oLbxAtu:nAt, 01])
  
  oLbxAtu:SetArray(aLbxAtu)
  oLbxAtu:bLine := {|| {IIf(!aLbxAtu[oLbxAtu:nAt, 01], oNo, oTik), aLbxAtu[oLbxAtu:nAt, 02], aLbxAtu[oLbxAtu:nAt, 03], aLbxAtu[oLbxAtu:nAt, 04]}}

  tButton():New(11, 140, "Ok", oDlgAtu, {|| IIf(FS_VldAtu(aLbxAtu), (nOpcA := 1, oDlgAtu:End()), nOpcA := 0) }, 32,,,,, .T.) //Ok
  tButton():New(11, 180, "Cancelar", oDlgAtu, {||oDlgAtu:End()}, 32,,,,, .T.)//Cancelar

 ACTIVATE MSDIALOG oDlgAtu CENTERED 
 
 If nOpca == 1 .And. MsgYesNo("Deseja efetuar a atualizacao do Dicion�rio? Esta rotina deve ser utilizada em modo exclusivo ! Faca um backup dos dicion�rios e da Base de Dados antes da atualiza��o para eventuais falhas de atualiza��o !", "Aten��o")
  Processa({|| FS_ExecAtu(aLbxAtu)}, "Executando atualizador(es)")
 Endif 
 
Return(Nil)

Static Function FS_ExecAtu(aVetAtu) 
 Local nCont := 0 

 For nCont := 1 To Len(aVetAtu)
  If aVetAtu[nCont, 1]
   &(aVetAtu[nCont, 3])
  EndIf 
 Next                     

Return(Nil)

Static Function FS_VldAtu(aLbxAtu)
 Local lRet  := .F.
 Local nCont := 0 
 Local cAtuNExist := ""
 Local oTik   := LoadBitmap( GetResources(), "LBTIK" )
 Local oNo    := LoadBitmap( GetResources(), "LBNO" )   
   
 For nCont := 1 To Len(aLbxAtu)
  If aLbxAtu[nCont, 1]
   If !(lRet := FindFunction(aLbxAtu[nCont, 3]))
    cAtuNExist += aLbxAtu[nCont, 3] + Chr(13) + Chr(10)
    aLbxAtu[nCont, 1] := .F.
   EndIf
  EndIf 
 Next 
                                                                                                             
 If !(lRet := Empty(cAtuNExist))
  HS_MsgInf("Rotina(s) atualizadora(s):" + Chr(13) + Chr(10) + ;
            cAtuNExist +  Chr(13) + Chr(10) + ;
            "n�o existe(m) no reposit�rio." + CHR(10) + ;
            "Favor solicitar patch referente a esse(s) atualizador(es).", "Aten��o", "Atualiza��o de dicion�rios")
  oLbxAtu:SetArray(aLbxAtu)
  oLbxAtu:bLine := {|| {IIf(!aLbxAtu[oLbxAtu:nAt, 01], oNo, oTik), aLbxAtu[oLbxAtu:nAt, 02], aLbxAtu[oLbxAtu:nAt, 03], aLbxAtu[oLbxAtu:nAt, 04]}}
  oLbxAtu:Refresh()            
 EndIf
Return(lRet)




/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HS_Ajusta � Autor � Patricia Queiroz   � Data �   30/03/07  ���
�������������������������������������������������������������������������͹��
���Descricao � Fun��o para gravar os dados.                               ���
�������������������������������������������������������������������������ͼ��
���Parametros� aCpoNao : array com os campos que n�o devem ser alterados  ���
���            aCpoSim : array com os campos que devem ser alterados      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
                                                                            */
Function HS_Ajusta(aRegs, aArqUpd, cTexto, aCpoNao, aCpoSim, lNOrdem)

 Local nRegSx3 := 0, nColSx3 := 0
 Local aArea := GetArea()
 Local lInclui := .F.   
 Local cOrdem := "00"
 
 Default aCpoNao := {}
 Default aCpoSim := {}
 Default lNOrdem := .F.
 
 DbSelectArea("SX3")
 If lNOrdem
  DbSetOrder(1)
  DbSeek(aRegs[1][1] + "ZZ", .T.) // Posiciona no pr�ximo arquivo
  DbSkip(-1) // Volta pro ultimo registro do arquivo
  cOrdem := SX3->X3_ORDEM
 EndIf
 
 DbSetOrder(2)
 For nRegSx3 := 1 To Len(aRegs)
   
  lInclui := !DbSeek(aRegs[nRegSx3][3])
 
  If lNOrdem .And. lInclui
   cOrdem := Soma1(cOrdem, 2)
   aRegs[nRegSx3][2] := cOrdem
  EndIf 
 
  If(Ascan(aArqUpd, aRegs[nRegSx3][1]) == 0)
  	aAdd(aArqUpd, aRegs[nRegSx3][1])
  EndIf
 
  cTexto += IIf( aRegs[nRegSx3][1] $ cTexto, "", aRegs[nRegSx3][1] + "\")
  
  
  RecLock("SX3", lInclui)
   For nColSx3 := 1 To FCount()
    If lInclui .Or. ;
       (aScan(aCpoNao, {| cVet | PadR(cVet, 10) == Padr(FieldName(nColSx3),10) }) == 0 .And. ;
       (Len(aCpoSim) == 0 .Or. aScan(aCpoSim, {| cVet | PadR(cVet, 10) == Padr(FieldName(nColSx3),10) }) > 0))
    
    FieldPut(nColSx3, IIF(nColSx3 <= len(aRegs[nRegSx3]), aRegs[nRegSx3, nColSx3], "N"))
    
    Endif 
   Next
  MsUnlock()
 Next                    
 
 RestArea(aArea)
Return(Nil)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HS_Reordem � Autor � Patricia Queiroz   � Data �  30/03/07  ���
�������������������������������������������������������������������������͹��
���Descricao � Fun��o para incluir novos campos e para reordenar.         ���
�������������������������������������������������������������������������ͼ��
���Par�metros� cAlias  : Arquivo a ser alterado                           ���   
���            aCReord : array contendo os campo para reordenacao         ���   
���            cOrdem  : ordem                                            ���   
���            aRegs   : array contendo os campos para inclusao           ���   
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
                                                                            */
Function HS_Reorden(cAlias, aCReord, cOrdem, aRegs)

 Local aArea := GetArea()
 Local aOrdSx3 := {}
 Local nFReord := 0, nP1Sx3 := 0, nP2Sx3 := 0, cSx3Ordem := ""
 
 Default cOrdem := "00"
 Default aRegs  := {}
 
 // Pega ordem dos campos no dicionarios do cliente
 DbSelectArea("SX3")
 DbSetOrder(1)
 DbSeek(cAlias)
 While !Eof() .And. SX3->X3_ARQUIVO == cAlias
  aAdd(aOrdSx3, {SX3->X3_ORDEM + "0", SX3->X3_CAMPO})
  cSx3Ordem := SX3->X3_ORDEM
  DbSkip()
 End                                             
 
 // Inclui os campos do aRegs no campos do dicionario
 For nFReord := 1 To Len(aRegs)
  cSx3Ordem := Soma1(cSx3Ordem, 2)
  aAdd(aOrdSx3, {cSx3Ordem + "0", aRegs[nFReord][3]})
 Next
 
 // Ajusta ordem dos campos
 For nFReord := 1 To Len(aCReord)
  nP1Sx3 := aScan(aOrdSx3, {| aVet | aVet[2] == aCReord[nFReord][2]})
  If (nP2Sx3 := aScan(aOrdSx3, {| aVet | aVet[2] == aCReord[nFReord][1]})) > 0
   aOrdSx3[nP2Sx3][1] := Soma1(aOrdSx3[nP1Sx3][1], 3)
  EndIf 
 Next 
 
 // Ordena campos
 aSort(aOrdSx3,,, {| X, Y | X[1] < Y[1]})
 
 // Ajusta ordem do aRegs
 For nFReord := 1 To Len(aRegs)      
  If (nP1Sx3 := aScan(aOrdSx3, {| aVet | aVet[2] == aRegs[nFReord][3]})) > 0
   aRegs[nFReord][2] := aOrdSx3[nP1Sx3][1]
  EndIf 
 Next
Return(cOrdem)    

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HS_GERX2UN � Autor � Heimdall Castro    � Data �  21/09/07  ���
�������������������������������������������������������������������������͹��
���Descricao � Fun��o para incluir ind�ce no campo X2_UNICO.              ���
�������������������������������������������������������������������������ͼ��
���Par�metros� aArqUpd - Atualiza tabelas que foram modificadas no banco  ��� 
���								 	�	aUnico - array com os campos(informados na segunda pos��o) ���   
���          �          a serem inseridos no X2_UNICO para as tabelas     ���   
���          �          informadas na primeira posi��o do mesmo           ���   
���          � lAtualiza - determina se os registros que n�o possuem      ���   
���          �             inconsist�ncia ser�o atualizados ou n�o, mesmo ���   
���          �             que alguns registros possuam inconsist�ncia    ���   
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
                                                                            
                                                                            
Function HS_GERX2UN(aArqUpd, aUnico, lAtualiza)
                                
 Local nCtaFor    := 0
 Local cAlias     := ""
 Local cChave     := "" 
 Local cChaveSQL  := "" 

 Local lMsg01     := .F. // caso encontre um erro na situa��o 1, recebe .T.

 Local lMsg02     := .F. // caso encontre um erro na situa��o 2, recebe .T.

 Local lMsg03     := .F. // caso encontre um erro na situa��o 3, recebe .T.
 Local lExistChav := .F.                   
 Local aCposAtual := {}  //campos que podem ser atualizados

 Local cMsg01     := ""  //mensagem usada para os indices que j� existem no campo X2_UNICO
 Local cMsg02     := ""  //mensagem usada para os indices que possuem duplicidades
 Local cMsg03     := ""  //mensagem usada para os indices que foram inseridos sem problemas
 Local cSQL       := ""                  
 
 Default lAtualiza  := .T.
 Default aUnico     := {}
 
  
 For nCtaFor := 1 to Len(aUnico)
  cAlias     := aUnico[nCtaFor, 1]
  cChave     := aUnico[nCtaFor, 2]
  lExistChav := .F.         
  
  cChaveSQL	:=	STRTran(cChave, "+", " || '        ' || ")
  cChaveSQL	:=	STRTran(cChaveSQL, "DTOS(", "")
  cChaveSQL	:=	STRTran(cChaveSQL, ")", "") 
                                            
                                                 
  If lExistChav == .F. //se n�o existir chave no campo X2_UNICO executa a verificacao para duplicidade
   If TCCANOPEN(RetSqlName(cAlias))
    
    cSQL := " SELECT (" + cChaveSQL + ") CAMPO , COUNT(" + cChaveSQL + ") - 1 AS TOTA "
    cSQL += " FROM " + RetSqlName(cAlias)
    cSQL += " WHERE D_E_L_E_T_ <> '*' "
    cSQL += " GROUP BY " + cChaveSQL
    cSQL += " HAVING COUNT(*) > 1"
   
    cSQL :=  ChangeQuery(cSQL)
  
    TCQUERY cSQL NEW ALIAS "TMP"
    DbSelectArea("TMP")
    DbGoTop()
                   
    If !EOF()  //se encontrar registo na select � porque existem chaves duplicadas
     cMsg02 += Chr(13) + Chr(10) + "Foram encontrados os seguintes campos com duplicidade para a tabela " + cAlias + ":"
     cMsg02 += Chr(13) + Chr(10) + "Tabela   " + cChave
     While !EOF() //monta a mensagem com os registros duplicados para a chave e alias atual
      cMsg02 += Chr(13) + Chr(10) + cAlias + "      " + TMP->CAMPO
      DbSkip()
     EndDo
     lMsg02 := .T.
    Else
     AAdd(aCposAtual, {cAlias, cChave})
     If(Ascan(aArqUpd, cAlias) == 0)
  	   AAdd(aArqUpd, cAlias)
     EndIf
    EndIf                                                  
   
    DbCloseArea()
   Endif
	 Endif 
 Next nCtaFor   
 
 cMsg01 := IIf(lMsg01, cMsg01, "")
 cMsg02 := IIf(lMsg02, cMsg02, "")
 cMsg03 := IIf(lMsg03, cMsg03, "") 
 
 IIf(lMsg01 .OR. lMsg02, HS_MsgInf("Ocorreu uma ou mais inconsist�ncias durante a atualiza��o do dicion�rio de dados da empresa: "+ SM0->M0_CODIGO + ;
                                   "favor verificar log para mais detalhes.","Aten��o","Atualiza��o de chaves �nicas"), "")
   
Return(cMsg01 + cMsg02 + cMsg03) 
