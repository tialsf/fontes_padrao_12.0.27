#INCLUDE "protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
����������������������-��������������������������������������������������Ŀ��
���Fun�ao    �GH112909  � Autor � MICROSIGA             � Data � 09/08/07 ���
�����������������������-�������������������������������������������������Ĵ��
���Descri�ao � Funcao Principal                                           ���
������������������������-������������������������������������������������Ĵ��
���Uso       � Gestao Hospitalar                                          ���
������������������������-�������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function GH112909()

cArqEmp 					:= "SigaMat.Emp"
__cInterNet 	:= Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd
Private nModulo 	:= 51 // modulo SIGAHSP

Set Dele On

lEmpenho				:= .F.
lAtuMnu					:= .F.

Processa({|| ProcATU()},"Processando [GH112909]","Aguarde , processando prepara��o dos arquivos")

Return()


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ProcATU   � Autor �                       � Data �  /  /    ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Baseado na funcao criada por Eduardo Riera em 01/02/2002   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function ProcATU()
Local cTexto    	:= ""
Local cFile     	:= ""
Local cMask     	:= "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    	:= 0
Local nI        	:= 0
Local nX        	:= 0
Local aRecnoSM0 	:= {}
Local lOpen     	:= .F.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
If (lOpen := IIF(Alias() <> "SM0", MyOpenSm0Ex(), .T. ))

	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	

	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2)
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
 		nModulo := 51 // modulo SIGAHSP
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)

			ProcRegua(8)

			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01',  "Fun��es descontinuadas pelo SGBD: GeraSX3()!"  , 0, 0, {})
	
			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso("Atencao!","Ocorreu um erro desconhecido durante a atualizacao da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicionario e da tabela.",{"Continuar"},2)
					cTexto += "Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
				dbSelectArea(aArqUpd[nx])
			Next nX		

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit
		 EndIf
		Next nI
		
		If lOpen
			      
			HS_AtuNom()
			
			cTexto 				:= "Log da atualizacao " + CHR(13) + CHR(10) + cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.) + ".LOG", cTexto)
			
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12
			DEFINE MSDIALOG oDlg TITLE "Atualizador [GH112909] - Atualizacao concluida." From 3,0 to 340,417 PIXEL
				@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
				oMemo:bRClicked := {||AllwaysTrue()}
				oMemo:oFont:=oFont
				DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
				DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
	
		EndIf
		
	EndIf
		
EndIf 	

Return(Nil)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	openSM0( cNumEmp,.F. )
	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex("SIGAMAT.IND")
		Exit	
	EndIf
	Sleep( 500 )
Next nLoop

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf

Return( lOpen )

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    � HS_AtuGcz� Autor � MICROSIGA             � Data �   /  /   ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Atualiza campos novos da Guia do Atendimento               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Gest�o Hospitalar                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/                                                                          
Function HS_AtuNom()
 RpcSetType(2)
	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	nModulo := 51 // modulo SIGAHSP
	lMsFinalAuto := .F.
   
 Processa({|| FS_AtuNom()}) 
   
 RpcClearEnv()
return(nil)  


Static Function FS_AtuNom()
 Local cSql  := "" 
 Local cConcat := Iif(TcGetDb() = "MSSQL"," + "," || ") 
    
 cSql := " UPDATE "+ RetSqlName("GB2")
 cSql += "    SET GB2_NOME =  " + Iif(Alltrim(cConcat) == "+","SUBSTRING","SUBSTR") + "('RN ' "+cConcat+" '(' "+cConcat+" GB2_TPNASC "+cConcat+" ') ' "+cConcat+" GB2_MAE, 1 , "+Alltrim(Str(TamSx3("GB2_NOME")[1]))+") "
 cSql += "  WHERE GB2_FILIAL = '" + xFilial("GB2") + "' AND D_E_L_E_T_ <> '*' "
 cSql += "    AND GB2_TPNASC IN ('1','2','3','4','5') "
  
 If TcSqlExec(cSql)  < 0
  Hs_MsgInf(TcSqlError(),"Aten��o","Erro ao atualizar registros da GB2")
  Return(nil)
 EndIf
              
 cSql := " UPDATE "+ RetSqlName("GAV") + Iif(Alltrim(cConcat) == "+",""," GAV")
 cSql += "    SET GAV_NOME = (SELECT GB2_NOME "
 cSql += "                      FROM "+ RetSqlName("GB2")+ " GB2 "
 cSql += "                     WHERE GB2.GB2_FILIAL =  '" + xFilial("GB2") + "'"
 cSql += "                       AND GB2.D_E_L_E_T_ <> '*' "
 cSql += "                       AND GB2.GB2_REGATE = GAV.GAV_REGATE "
 cSql += "                       AND GB2.GB2_TPNASC = GAV.GAV_RESERV ) "
 cSql += Iif(Alltrim(cConcat) == "+","  FROM "+ RetSqlName("GAV")+" GAV ","")
 cSql += " WHERE GAV.GAV_FILIAL = '" + xFilial("GAV") + "' AND GAV.D_E_L_E_T_ <> '*'  "
 cSql += "  AND  GAV.GAV_RESERV IN ('1','2','3','4','5') "
 cSql += "  AND  GAV_REGATE <> '' "
 
 If TcSqlExec(cSql)  < 0
  Hs_MsgInf(TcSqlError(),"Aten��o","Erro ao atualizar registros da GAV")
  Return(nil)
 EndIf

 cSql := " UPDATE "+ RetSqlName("GAI") + Iif(Alltrim(cConcat) == "+",""," GAI")
 cSql += "    SET GAI_NOMPAC = (SELECT " + Iif(Alltrim(cConcat) == "+","SUBSTRING","SUBSTR") + "(GB2_NOME, 1 , "+Alltrim(Str(TamSx3("GAI_NOMPAC")[1]))+") "
 cSql += "                        FROM "+ RetSqlName("GB2")+ " GB2 "
 cSql += "                       WHERE GB2.GB2_FILIAL =  '" + xFilial("GB2") + "'"
 cSql += "                         AND GB2.D_E_L_E_T_ <> '*' "
 cSql += "                         AND GB2.GB2_REGATE = GAI.GAI_REGATE "
 cSql += "                         AND GB2.GB2_TPNASC = GAI.GAI_RESERV ) "
 cSql += Iif(Alltrim(cConcat) == "+","  FROM "+ RetSqlName("GAI")+" GAI ","")
 cSql += "  WHERE GAI.GAI_FILIAL = '" + xFilial("GB2") + "'  AND GAI.D_E_L_E_T_ <> '*' "
 cSql += "    AND GAI.GAI_RESERV in ('1','2','3','4','5') "
 
 If TcSqlExec(cSql)  < 0
  Hs_MsgInf(TcSqlError(),"Aten��o","Erro ao atualizar registros da GAI")
  Return(nil)
 EndIf

return(nil)