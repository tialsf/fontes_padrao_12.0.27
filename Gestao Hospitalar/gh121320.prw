#INCLUDE "protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
����������������������-��������������������������������������������������Ŀ��
���Fun�ao    �GH121320  � Autor � MICROSIGA             � Data � 13/06/07 ���
�����������������������-�������������������������������������������������Ĵ��
���Descri�ao � Funcao Principal                                           ���
������������������������-������������������������������������������������Ĵ��
���Uso       � Gestao Hospitalar                                          ���
������������������������-�������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function GH121320()

cArqEmp 					:= "SigaMat.Emp"
__cInterNet 	:= Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd
Private nModulo 	:= 51 // modulo SIGAHSP

Set Dele On

lEmpenho				:= .F.
lAtuMnu					:= .F.

Processa({|| ProcATU()},"Processando [GH121320]","Aguarde , processando prepara��o dos arquivos")

Return()


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ProcATU   � Autor �                       � Data �  /  /    ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Baseado na funcao criada por Eduardo Riera em 01/02/2002   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function ProcATU()
Local cTexto    	:= ""
Local cFile     	:= ""
Local cMask     	:= "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    	:= 0
Local nI        	:= 0
Local nX        	:= 0
Local aRecnoSM0 	:= {}
Local lOpen     	:= .F.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
If (lOpen := IIF(Alias() <> "SM0", MyOpenSm0Ex(), .T. ))

	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	

	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2)
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
 			nModulo := 51 // modulo SIGAHSP
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)

			ProcRegua(8)
			
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01',  "Fun��es descontinuadas pelo SGBD: GeraSX2(), GeraSX1(), GeraSX3(), GeraSIX() e GeraSXB()!"  , 0, 0, {})

			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', __GetX31Trace() , 0, 0, {})
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Ocorreu um erro desconhecido durante a atualizacao da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicionario e da tabela.",{"Continuar"},2 , 0, 0, {})
					cTexto += "Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
				dbSelectArea(aArqUpd[nx])
			Next nX		

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit
		 EndIf
		Next nI
		
		If lOpen //.And. 1 == 2
			HS_GrvFil()
			
			cTexto 				:= "Log da atualizacao " + CHR(13) + CHR(10) + cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.) + ".LOG", cTexto)
			
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12
			DEFINE MSDIALOG oDlg TITLE "Atualizador [GH121320] - Atualizacao concluida." From 3,0 to 340,417 PIXEL
				@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
				oMemo:bRClicked := {||AllwaysTrue()}
				oMemo:oFont:=oFont
				DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
				DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
	
		EndIf
		
	EndIf
		
EndIf 	

Return(Nil)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	openSM0( cNumEmp,.F. )
	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex("SIGAMAT.IND")
		Exit	
	EndIf
	Sleep( 500 )
Next nLoop

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf

Return( lOpen )

Static Function FS_GrvFil()
 Local nRecnoIni := -1024, nRecnoFim := 1024, nRecNoTot := 0
 Local cSql := ""

 RpcSetType(2)
	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	nModulo := 51 // modulo SIGAHSP
	lMsFinalAuto := .F.
	__cInterNet 	:= ""
	
 //Grava valor default da filial
 DBSelectArea("GCZ")
 DbGoTop()
 
 nRecNoTot := RecCount()
 
 ProcRegua(GCZ->(RecCount()))

 While nRecNoIni <= nRecNoTot
 
  /*
  If Empty(GCZ->GCZ_FILFAT) .Or. Empty(GCZ->GCZ_FILATE)
   RecLock("GCZ", .F.)
    GCZ->GCZ_FILFAT := GCZ->GCZ_FILIAL
    GCZ->GCZ_FILATE := GCZ->GCZ_FILIAL
   MsUnLock()
  EndIf 
                                                     
  DBSkip()
  */
  
  nRecNoIni += 1024
  nRecNoFim += 1024
  
  IncProc("Grv. Filial Fat/Ate RecNo [" + AllTrim(Str(nRecNoIni)) + "/" + AllTrim(Str(nRecNoFim)) + "]") 
  
  cSql := " UPDATE " + RetSqlName("GCZ")
  cSql += "    SET GCZ_FILFAT = GCZ_FILIAL"
  cSql += "  WHERE GCZ_FILIAL <> '  ' AND GCZ_FILFAT = '  ' AND D_E_L_E_T_ <> '*' "
  cSql += "    AND R_E_C_N_O_ BETWEEN " + AllTrim(Str(nRecNoIni)) + " AND " + AllTrim(Str(nRecNoFim))
                   
  TCSqlExec(cSql)
  
  cSql := " UPDATE " + RetSqlName("GCZ")
  cSql += "    SET GCZ_FILATE = GCZ_FILIAL"
  cSql += "  WHERE GCZ_FILIAL <> '  ' AND GCZ_FILATE = '  ' AND D_E_L_E_T_ <> '*' "
  cSql += "    AND R_E_C_N_O_ BETWEEN " + AllTrim(Str(nRecNoIni)) + " AND " + AllTrim(Str(nRecNoFim))
                   
  TCSqlExec(cSql)
 EndDo

 RpcClearEnv()
Return(Nil)
