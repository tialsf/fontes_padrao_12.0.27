#INCLUDE "protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
����������������������-��������������������������������������������������Ŀ��
���Fun�ao    �GH121680  � Autor � MICROSIGA             � Data � 12/07/07 ���
�����������������������-�������������������������������������������������Ĵ��
���Descri�ao � Funcao Principal                                           ���
������������������������-������������������������������������������������Ĵ��
���Uso       � Gestao Hospitalar                                          ���
������������������������-�������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function GH121680()

cArqEmp 					:= "SigaMat.Emp"
__cInterNet 	:= Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd
Private nModulo 	:= 51 // modulo SIGAHSP

Set Dele On

lEmpenho				:= .F.
lAtuMnu					:= .F.

Processa({|| ProcATU()},"Processando [GH121680]","Aguarde , processando prepara��o dos arquivos")

Return()


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ProcATU   � Autor �                       � Data �  /  /    ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Baseado na funcao criada por Eduardo Riera em 01/02/2002   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function ProcATU()
Local cTexto    	:= ""
Local cFile     	:= ""
Local cMask     	:= "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    	:= 0
Local nI        	:= 0
Local nX        	:= 0
Local aRecnoSM0 	:= {}
Local lOpen     	:= .F.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
If (lOpen := IIF(Alias() <> "SM0", MyOpenSm0Ex(), .T. ))

	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	

	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2)
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
 			nModulo := 51 // modulo SIGAHSP
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)

			ProcRegua(8)
			
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Fun��es descontinuadas pelo SGBD: GeraSX3(),GeraSX6()" , 0, 0, {})

			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', __GetX31Trace() , 0, 0, {})
					FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Ocorreu um erro desconhecido durante a atualizacao da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicionario e da tabela.",{"Continuar"},2 , 0, 0, {})
					cTexto += "Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
				dbSelectArea(aArqUpd[nx])
			Next nX		

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit
		 EndIf
		Next nI
		
		If lOpen .And. 1 == 2
			     
			HS_AtuGcz()
			
			cTexto 				:= "Log da atualizacao " + CHR(13) + CHR(10) + cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.) + ".LOG", cTexto)
			
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12
			DEFINE MSDIALOG oDlg TITLE "Atualizador [GH121680] - Atualizacao concluida." From 3,0 to 340,417 PIXEL
				@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
				oMemo:bRClicked := {||AllwaysTrue()}
				oMemo:oFont:=oFont
				DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
				DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
	
		EndIf
		
	EndIf
		
EndIf 	

Return(Nil)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	openSM0( cNumEmp,.F. )
	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex("SIGAMAT.IND")
		Exit	
	EndIf
	Sleep( 500 )
Next nLoop

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf

Return( lOpen )

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    � HS_AtuGcz� Autor � MICROSIGA             � Data �   /  /   ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Atualiza campos novos da Guia do Atendimento               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Gest�o Hospitalar                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/                                                                          
Function HS_AtuGcz()
 RpcSetType(2)
	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	nModulo := 51 // modulo SIGAHSP
	lMsFinalAuto := .F.

  HS_MsgInf("O campo GCZ_TPAPAC ser� atualizado para '1'(Inicial)", "Aten��o", "Atualiza��o Tipo APAC SUS.")
  Processa({|| FS_AtuAPAC()}) 
  
  HS_MsgInf("Escolha o motivo de cobran�a do APAC para atualizar a tabela de guias.", "Aten��o", "Atualiza��o Motivo de Cobran�a APAC SUS.")
  Processa({|| FS_AtuCmc()})   
  
  RpcClearEnv()
return(nil)  

Static Function FS_AtuAPAC()
 Local cSql := "" 
 
 cSql := " UPDATE "+ RetSqlName("GCZ")
 cSql += "    SET GCZ_TPAPAC = '1' "
 cSql += "  WHERE GCZ_FILIAL = '" + xFilial("GCZ") + "' AND D_E_L_E_T_ <> '*'"
 cSql += "    AND GCZ_TPAPAC IS NULL "
  
 //cSql := ChangeQuery(cSql) 
  
 TcSqlExec(cSql) 
 
return(nil)

Static Function FS_AtuCmc()
 Local nOpcA := 0
 Local oDlgCmc, oCmc,  oDesc
 Local cCodCmc   := "" 
 Local cDesCmc   := ""
 Local cPlanoSus := GetMv("MV_PSUSPAC")
 Local cConvSus  := GetMv("MV_PCONSUS")
 Local cSql := "" 
  
 If (Empty(cPlanoSus)).Or.(Empty(cConvSus))
  HS_MsgInf("Verique par�metros do conv�nio e do plano do SUS.","Aten��o","Valida��o Par�metros SUS")
  return(nil)
 ElseIf (!FS_VlPlSus(cPlanoSus))
  return(nil)
 ElseIf (!FS_VlCoSus(cConvSus))
  return(nil)   
 EndIf       
 
 cCodCmc  := SPACE(LEN(GCZ->GCZ_CMCPAC)) 
 cDesCmc  := SPACE(50) 

	nOpcA   := 0	
	DEFINE MSDIALOG oDlgCmc TITLE "Motivo da Cobran�a APAC" From 09, 00 to 16, 80 of oMainWnd
 	@ 020, 020 Say "Motivo da Cobran�a" Of oDlgCmc Pixel COLOR CLR_BLUE
	 @ 018, 080 MsGet oCmc  VAR cCodCmc Size 30 , 009 VALID FS_VldCmc(cCodCmc, @cDesCmc) F3 "GH7001" Picture "@!" OF oDlgCmc Pixel COLOR CLR_BLACK
	 @ 018, 115 MsGet oDesc VAR cDesCmc Size 100, 009 OF oDlgCmc Pixel COLOR CLR_BLACK
	 oDesc:lReadOnly := .T.
	ACTIVATE MSDIALOG oDlgCmc CENTERED ON INIT EnchoiceBar(oDlgCmc, 	{|| nOpcA := 1, IIF(FS_VldCmc(cCodCmc, @cDesCmc), oDlgCmc:End(), nOpcA := 0)}, ;
																																																																		{|| nOpcA := 0, oDlgCmc:End()})
	
	If nOpcA == 0
		Return(Nil)
 EndIf
 
 cSql := " UPDATE "+ RetSqlName("GCZ")
 cSql += "    SET GCZ_TPAPAC = '"+cCodCmc+"' "
 cSql += "  WHERE GCZ_FILIAL = '"+ xFilial("GCZ") + "' AND D_E_L_E_T_ <> '*'"
 cSql += "    AND GCZ_CODPLA = '"+ cPlanoSus + "' "
 cSql += "    AND GCZ_CODCON = '"+ cConvSus + "' " 
 cSql += "    AND GCZ_CMCPAC IS NULL "
  
 //cSql := ChangeQuery(cSql) 
  
 TcSqlExec(cSql)   
Return(Nil)                                         

Static Function 	FS_VldCmc(cCodCmc, cDesCmc)
 Local lRet := .T.
 Local aArea := GetArea()

 If EMPTY(cCodCmc)
  HS_MsgInf("Motivo de Cobran�a Inv�lido", "Aten��o", "Valida��o Motivo de Cobran�a")
 Else
  DbSelectArea("GH7")
  DbSetOrder(1)
  If !(DbSeek(xFilial("GH7") + cCodCmc))
  	HS_MsgInf("Motivo de Cobran�a Inv�lido", "Aten��o", "Valida��o Motivo de Cobran�a")
  	lRet := .F.                       
  Else
   cDesCmc := GH7->GH7_DMCPAC
  Endif
 EndIf
 
 RestArea(aArea)

Return(lRet)           

Static Function FS_VlPlSus(cCodPlano)
 Local lRet  := .T.
 Local aArea := GetArea()
 
 DbSelectArea("GCM")
 DbSetOrder(2)
 If !(DbSeek(xFilial("GCM") + cCodPlano))
 	HS_MsgInf("Plano do Sus(MV_PSUSPAC) n�o cadastrado na tabela de Plano X Conv�nios", "Aten��o", "Valida��o Plano Sus")
 	lRet := .F.                       
 Endif
 
 RestArea(aArea)
Return(lRet)  

Static Function FS_VlCoSus(cCodConv)
 Local lRet  := .T.
 Local aArea := GetArea()
 
 DbSelectArea("GA9")
 DbSetOrder(1)
 If !(DbSeek(xFilial("GA9") + cCodConv))
 	HS_MsgInf("Conv�nio do Sus(MV_PCONSUS) n�o cadastrado na tabela de Conv�nios", "Aten��o", "Valida��o Conv�nio Sus")
 	lRet := .F.                       
 Endif
 
 RestArea(aArea)
Return(lRet)