#INCLUDE "protheus.ch"
#INCLUDE "TOPCONN.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
����������������������-��������������������������������������������������Ŀ��
���Fun�ao    �GH122125  � Autor � MICROSIGA             � Data � 11/04/07 ���
�����������������������-�������������������������������������������������Ĵ��
���Descri�ao � Funcao Principal                                           ���
������������������������-������������������������������������������������Ĵ��
���Uso       � Gestao Hospitalar                                          ���
������������������������-�������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function GH122125()

cArqEmp 					:= "SigaMat.Emp"
__cInterNet 	:= Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd
Private nModulo 	:= 51 // modulo SIGAHSP

Set Dele On

lEmpenho				:= .F.
lAtuMnu					:= .F.

Processa({|| ProcATU()},"Processando [GH122125]","Aguarde , processando prepara��o dos arquivos")

Return()


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ProcATU   � Autor �                       � Data �  /  /    ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Baseado na funcao criada por Eduardo Riera em 01/02/2002   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function ProcATU()
Local cTexto    	:= ""
Local cFile     	:= ""
Local cMask     	:= "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    	:= 0
Local nI        	:= 0
Local nX        	:= 0
Local aRecnoSM0 	:= {}
Local lOpen     	:= .F.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
If (lOpen := IIF(Alias() <> "SM0", MyOpenSm0Ex(), .T. ))

	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	

	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2)
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
 			nModulo := 51 // modulo SIGAHSP
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)

			ProcRegua(8)
			
			DbSelectArea("SX3")
	 	DbSetOrder(2)
	 	If DbSeek("GBH_PROFIS") .AND. SX3->X3_TAMANHO <> 3
	 		FS_Dados("GBH")
  		FS_Dados("GHL")
  		FS_Dados("GFD")
 	 EndIf
 	 
 	 		FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Fun��es descontinuadas pelo SGBD: GeraSX2(),GeraSIX(),GeraSX1(),GeraSX3() e GeraSXB()" , 0, 0, {})
	 
			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso("Atencao!","Ocorreu um erro desconhecido durante a atualizacao da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicionario e da tabela.",{"Continuar"},2)
					cTexto += "Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
				dbSelectArea(aArqUpd[nx])
			Next nX		

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit
		 EndIf
		Next nI
		
		If lOpen
			
			cTexto 				:= "Log da atualizacao " + CHR(13) + CHR(10) + cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.) + ".LOG", cTexto)
			
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12
			DEFINE MSDIALOG oDlg TITLE "Atualizador [GH122125] - Atualizacao concluida." From 3,0 to 340,417 PIXEL
				@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
				oMemo:bRClicked := {||AllwaysTrue()}
				oMemo:oFont:=oFont
				DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
				DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
	
		EndIf
		
	EndIf
		
EndIf 	

Return(Nil)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	openSM0( cNumEmp,.F. )
	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex("SIGAMAT.IND")
		Exit	
	EndIf
	Sleep( 500 )
Next nLoop

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf

Return( lOpen )

Static Function FS_Dados(cAlias)

 Local cCampo     := cAlias + "->" + cAlias + "_PROFIS"
 Local cSql       := ""
 Local aArea      := GetArea()
 
 DbSelectArea("GH2")
 DbSelectArea(cAlias)
 
 Begin Transaction
  
  cSql := "SELECT DISTINCT " + cAlias + "." + cAlias + "_PROFIS PROFIS "
  cSql += "FROM " + RetSqlName(cAlias) + " " + cAlias + " "
  cSql += "WHERE " + cAlias + "." + cAlias + "_PROFIS <> '" + Space(Len(&(cCampo))) + "' AND "
  cSql +=  "NOT EXISTS(SELECT GH2.GH2_DSCBOR "
 	cSql +=             "FROM " + RetSqlName("GH2") +" GH2 "
  cSql +=             "WHERE GH2.GH2_FILIAL = '" + xFilial("GH2") + "' AND GH2.D_E_L_E_T_ <> '*' AND "
  If "MSSQL" $ TcGetDb()
   cSql +=                  " SUBSTRING(GH2.GH2_DSCBOR,1," + Str(Len(&(cCampo))) + ") = " + cAlias + "." + cAlias + "_PROFIS) AND "
  Else
   cSql +=                  " SUBSTR(GH2.GH2_DSCBOR,1," + Str(Len(&(cCampo))) + ") = " + cAlias + "." + cAlias + "_PROFIS) AND "
  EndIf
  cSql +=   cAlias + "." + cAlias + "_FILIAL = '" + xFilial(cAlias) + "' AND " + cAlias + ".D_E_L_E_T_ <> '*' "
  
  cSql := ChangeQuery(cSql)
  
  TcQuery cSql NEW ALIAS "TMP"
  
  DbSelectArea("TMP")
  DbGotop()

  While !TMP->(Eof())
   
   DbSelectArea("GH2")
   RecLock("GH2", .T.)
    GH2->GH2_FILIAL := xFilial("GH2")
    GH2->GH2_CDCBOR := HS_VSXENUM("GH2", "M->GH2_CDCBOR", 1)
 			GH2->GH2_DSCBOR := TMP->PROFIS
   MsUnlock()
  
   TMP->(DbSkip())
  End
   
  DbSelectArea("TMP")
  DbCloseArea()
 
 End Transaction
 
 Begin Transaction
 
  cSql := "UPDATE " + RetSqlName(cAlias) + " "
  cSql += "SET " + cAlias + "_PROFIS  = (SELECT DISTINCT GH2.GH2_CDCBOR "
  cSql +=                               "FROM " + RetSqlName("GH2") + " GH2 "
  If "MSSQL" $ TcGetDb()
   cSql +=                               "WHERE SUBSTRING(GH2.GH2_DSCBOR,1," + Str(Len(&(cCampo))) + ") = " + cAlias + "_PROFIS AND GH2.GH2_FILIAL = '" + xFilial("GH2") + "' AND "
  Else
   cSql +=                               "WHERE SUBSTR(GH2.GH2_DSCBOR,1," + Str(Len(&(cCampo))) + ") = " + cAlias + "_PROFIS AND GH2.GH2_FILIAL = '" + xFilial("GH2") + "' AND "
  EndIf 
  cSql +=                               "GH2.D_E_L_E_T_ <> '*') "
  cSql += "WHERE " + cAlias + "_FILIAL = '" + xFilial(cAlias) +  "' AND D_E_L_E_T_ <> '*' AND " + cAlias + "_PROFIS <> '" + Space(Len(&(cCampo))) + "'"
  
  TcSqlExec(cSql)
 
 End Transaction
 
 RestArea(aArea)
Return()