#INCLUDE "protheus.ch"        
#INCLUDE "TopConn.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
����������������������-��������������������������������������������������Ŀ��
���Fun�ao    �GH123565  � Autor � MICROSIGA             � Data � 19/04/07 ���
�����������������������-�������������������������������������������������Ĵ��
���Descri�ao � Funcao Principal                                           ���
������������������������-������������������������������������������������Ĵ��
���Uso       � Gestao Hospitalar                                          ���
������������������������-�������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function GH123565()

cArqEmp 					:= "SigaMat.Emp"
__cInterNet 	:= Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd
Private nModulo 	:= 51 // modulo SIGAHSP

Set Dele On

lEmpenho				:= .F.
lAtuMnu					:= .F.

Processa({|| ProcATU()},"Processando [GH123565]","Aguarde , processando prepara��o dos arquivos")

Return()


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ProcATU   � Autor �                       � Data �  /  /    ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao de processamento da gravacao dos arquivos           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Baseado na funcao criada por Eduardo Riera em 01/02/2002   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function ProcATU()
Local cTexto    	:= ""
Local cFile     	:= ""
Local cMask     	:= "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    	:= 0
Local nI        	:= 0
Local nX        	:= 0
Local aRecnoSM0 	:= {}
Local lOpen     	:= .F.

ProcRegua(1)
IncProc("Verificando integridade dos dicion�rios....")
If (lOpen := IIF(Alias() <> "SM0", MyOpenSm0Ex(), .T. ))

	dbSelectArea("SM0")
	dbGotop()
	While !Eof()
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	

	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2)
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
 			nModulo := 51 // modulo SIGAHSP
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)

			ProcRegua(8)
			
			FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', "Fun��es descontinuadas pelo SGBD: GeraSX2(),GeraSX3(),GeraSIX() e GeraSXB()" , 0, 0, {})			
	
			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso("Atencao!","Ocorreu um erro desconhecido durante a atualizacao da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicionario e da tabela.",{"Continuar"},2)
					cTexto += "Ocorreu um erro desconhecido durante a atualizacao da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
				dbSelectArea(aArqUpd[nx])
			Next nX		

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit
		 EndIf
		Next nI
		
		If lOpen
			HS_GrvGCY()
			
			cTexto 				:= "Log da atualizacao " + CHR(13) + CHR(10) + cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.) + ".LOG", cTexto)
			
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12
			DEFINE MSDIALOG oDlg TITLE "Atualizador [GH123565] - Atualizacao concluida." From 3,0 to 340,417 PIXEL
				@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
				oMemo:bRClicked := {||AllwaysTrue()}
				oMemo:oFont:=oFont
				DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
				DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
	
		EndIf
		
	EndIf
		
EndIf 	

Return(Nil)


/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �MyOpenSM0Ex� Autor �Sergio Silveira       � Data �07/01/2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Efetua a abertura do SM0 exclusivo                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Atualizacao FIS                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Static Function MyOpenSM0Ex()

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	openSM0( cNumEmp,.F. )
	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex("SIGAMAT.IND")
		Exit	
	EndIf
	Sleep( 500 )
Next nLoop

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 )
EndIf

Return( lOpen )

                                               
/****************************************************************************************/
Function HS_GrvGCY()

 If MsgYesNo("Confirma grava��o do valor padr�o do Carater de Atendimento ?")
	 Processa({|| FS_GrvGCY() })
	 RpcClearEnv()
	EndIf

Return(Nil)	

Static Function FS_GrvGCY()
 Local nOpcA := 0
 Local oDlgCar, oCar,  oDesc
 Local cCodCar  := "" 
 Local cDesCar  := ""

	RpcSetType(2)
	RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
	nModulo := 51 // modulo SIGAHSP
	lMsFinalAuto := .F.
	
	cCodCar  := SPACE(LEN(GCY->GCY_CARATE)) 
 cDesCar  := SPACE(LEN(GD1->GD1_DCARAT)) 
	
	nOpcA   := 0	
	DEFINE MSDIALOG oDlgCar TITLE "Carater" From 09, 00 to 16, 80 of oMainWnd
 	@ 020, 020 Say "Carater" Of oDlgCar Pixel COLOR CLR_BLUE
	 @ 018, 040 MsGet oCar VAR cCodCar Size 30, 009 VALID FS_VldCar(cCodCar, @cDesCar) F3 "GD1" Picture "@!" OF oDlgCar Pixel COLOR CLR_BLACK
	 @ 018, 075 MsGet oDesc VAR cDesCar Size 100, 009 OF oDlgCar Pixel COLOR CLR_BLACK
	 oDesc:lReadOnly := .T.
	ACTIVATE MSDIALOG oDlgCar CENTERED ON INIT EnchoiceBar(oDlgCar, 	{|| nOpcA := 1, IIF(FS_VldCar(cCodCar, @cDesCar), oDlgCar:End(), nOpcA := 0)}, ;
																																																																		{|| nOpcA := 0, oDlgCar:End()})
	
	If nOpcA == 0
		Return(Nil)
 Else
  //Grava valor Carater default no campo GCY_CARATE
  DBSelectArea("GCY")
  DBSetOrder(1)
  DBSeek(xFilial("GCY"))     
 
  ProcRegua(GCY->(RecCount()))

  While !Eof() .And. GCY->GCY_FILIAL == xFilial("GCY")
 
   IncProc("Gravando valor padrao do carater no atendimento [" + GCY->GCY_REGATE + "]") 
 
   If Empty(GCY->GCY_CARATE)
    RecLock("GCY", .F.)
     GCY->GCY_CARATE := cCodCar
    MsUnLock()
   EndIf 
                                                     
   DBSkip()
  EndDo
	EndIf
	
Return(Nil)                                 

Static Function 	FS_VldCar(cCodCar, cDesCar)
 Local lRet := .T.
 Local aArea := GetArea()

 If EMPTY(cCodCar)
  HS_MsgInf("Carater de Atendimento inv�lido", "Aten��o", "Carater de Atendimento")
 Else
  DbSelectArea("GD1")
  DbSetOrder(1)
  If !(DbSeek(xFilial("GD1") + cCodCar))
  	HS_MsgInf("Carater de Atendimento n�o encontrado", "Aten��o", "Carater de Atendimento")
  	lRet := .F.                       
  Else
   cDesCar := GD1->GD1_DCARAT
  Endif
 EndIf
 
 RestArea(aArea)

Return(lRet)
