#INCLUDE "HSPAHACL.ch"
#include "protheus.CH"
#include "colors.CH"
/*������������������������������������������������������������������������������������������������������������������
��������������������������������������������������������������������������������������������������������������������
����������������������������������������������������������������������������������������������������������������ͻ��
���Programa  � HSPAHACL           � Autor � Rogerio tabosa                                  � Data � 08/06/09    ���
����������������������������������������������������������������������������������������������������������������͹��
���Descricao � Cadastro de Grupos de Antibioticos                                                                ���
����������������������������������������������������������������������������������������������������������������͹��
���Uso       � SIGAHSP                                                                                           ���
����������������������������������������������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������������������������������������������
���                               ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.                              ���
����������������������������������������������������������������������������������������������������������������Ĵ��
���               �        �      �                                                                              ���
�����������������������������������������������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������������������������������������������
������������������������������������������������������������������������������������������������������������������*/

Function HSPAHACL()

Private aRotina := MenuDef()


DbSelectArea("GPE")
DbSetOrder(1)


mBrowse(06, 01, 22, 75, "GPE")

Return

Function HS_ACL(cAlias, nReg, nOpc)
Local nOpcA := 0

Private lGPF := HS_LocTab("GPF", .F.)

Private Inclui 		:= .F. 
Private nOpcE 		:= aRotina[nOpc, 4]
Private nGDOpc 		:= IIf(StrZero(nOpcE,2) $ "03/04" , GD_INSERT + GD_UPDATE + GD_DELETE, 0  )
Private aTela 		:= {}
Private aGets      	:= {}
Private aHeader 	:= {}
Private aCols      	:= {}
Private nUsado     	:= 0
Private oGAE, oGetGPF
Private lGDVazio 	:= .F.
Private nCodAnt  	:=0
Private cG1RTipo 	:= "1" 
Private cGBICtrPsc 	:= "01"   //Psicotropico e Controlado

RegToMemory("GPE",(nOpcE == 3)) //Gera variavies de memoria para o GPE

Inclui := (nOpcE == 3 .OR. nOpcE == 4)
If nOpcE == 3 //Inclusao
	M->GPE_CODGRP := GetSXENum("GPE", "GPE_CODGRP",, 1) 
EndIf


If lGPF
	HS_BDados("GPF", @aHeader, @aCols,, 1,, IIf(!Empty(M->GPE_CODGRP), "GPF->GPF_CODGRP == '" + M->GPE_CODGRP + "'", Nil))
	nCodAnt := aScan(aHeader, {| aVet | aVet[2] == "GPF_CODANT"})
EndIf

nOpcA := 0

aSize := MsAdvSize(.T.)
aObjects := {}
AAdd( aObjects, { 100, 030, .T., .T. } )
AAdd( aObjects, { 100, 070, .T., .T. } )

aInfo  := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 0, 0 }
aPObjs := MsObjSize( aInfo, aObjects, .T. )

DEFINE MSDIALOG oDlg TITLE OemToAnsi("Cadastro Grupos de Antibi�ticos") From aSize[7],0 TO aSize[6], aSize[5]	PIXEL of oMainWnd  //"Cadastro Grupos de Antibi�ticos"

oGAE:= MsMGet():New(cAlias, nReg, nOpcE,,,,, {aPObjs[1, 1], aPObjs[1, 2], aPObjs[1, 3], aPObjs[1, 4]},,,,,, oDlg)
oGAE:oBox:align:= CONTROL_ALIGN_ALLCLIENT

If lGPF
	oGetGPF := MsNewGetDados():New(aPObjs[2, 1], aPObjs[2, 2], aPObjs[2, 3], aPObjs[2, 4], nGDOpc,"HS_DuplAC(oGetGPF:oBrowse:nAt, oGetGPF:aCols, {nCodAnt})",,,,,,,,, oDlg, aHeader, aCols)
	oGetGPF:oBrowse:align:= CONTROL_ALIGN_BOTTOM
EndIf

ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar (oDlg, {|| nOpcA := 1, IIF(Obrigatorio(aGets, aTela) .And. IIf(nOpcE == 5, Fs_ExcACL(), .T.), oDlg:End(), nOpcA == 0)}, ;
{|| nOpcA := 0, oDlg:End()})
If nOpcA == 0
	While __lSx8
		RollBackSxe()
	End
ElseIf nOpcA == 1 .And. nOpcE <> 2
	Begin Transaction
	FS_GrvACL(nReg)
	End Transaction
EndIf

Return()

Function FS_GrvACL(nReg)
Local aArea := GetArea()
Local nFor
Local lAchou :=.F.

DbSelectArea("GPE")

If nOpcE <> 3
	DbGoTo(nReg)
Endif

If nOpcE == 3 .Or. nOpcE == 4 //Inclusao e Alterar
	
	RecLock("GPE", (nOpcE == 3))
	HS_GrvCpo("GPE")
	GPE->GPE_FILIAL := xFilial("GPE")
	//GPE->GPE_LOGARQ := HS_LogArq()
	MsUnlock()
 	ConfirmSx8()

	//Gravar GetDados
	
	If lGPF
		
		DbSelectArea("GPF")
		DbSetOrder(2)//GPF_FILIAL+GPF_CODGRP+GPF_CODANT
		
		For nFor :=1 To Len(oGetGPF:aCols)
			lAchou := DbSeek(xFilial("GPF") + M->GPE_CODGRP + oGetGPF:aCols[nFor, nCodAnt])
			If oGetGPF:aCols[nFor, Len(oGetGPF:aHeader)+1 ]== .T.  // Se a linha esta deletada na get e achou o kra no banco
				If lAchou .And. nOpcE <> 3
					RecLock("GPF", .F., .F. )
					DbDelete()
					MsUnlock()
					WriteSx2("GPF")
				EndIf
			Else
				RecLock("GPF", !lAchou )
				HS_GRVCPO("GPF", oGetGPF:aCols, oGetGPF:aHeader, nFor)
				GPF->GPF_FILIAL := xFilial("GPF")
				GPF->GPF_CODGRP := M->GPE_CODGRP
				MsUnlock()
			EndIf
		Next
		
	EndIf
	
ElseIf nOpcE == 5 //Exclusao
	If lGPF
		
		DbSelectArea("GPF")
		DbSetOrder(2)//GPF_FILIAL+GPF_CODGRP+GPF_CODANT
		DbSeek(xFilial("GPF") + M->GPE_CODGRP )		
		While !Eof() .And. GPF->GPF_FILIAL = xFilial("GPF") .And. GPF->GPF_CODGRP = M->GPE_CODGRP
			RecLock("GPF", .F., .F. )
			DbDelete()
			MsUnlock()
			WriteSx2("GPF")
			DbSkip()
		End
		
	EndIf
	
	RecLock("GPE", .F., .F. )
	DbDelete()
	MsUnlock()
	WriteSx2("GPE")
Endif

RestArea(aArea)

Return()

// Funcao de Exclusao
Function Fs_ExcACL()
Local lRet := .T.

/* Verifica relacionamento na GCY (Atendimento).
If HS_CountTB("GCY", "GCY_CODEMP  = '" + GAE->GAE_CODEMP + "'")  > 0
	HS_MsgInf(STR0007,STR0008,STR0006)  //"Existe(m) atendimento(s) cadastrado(s) para esta empresa. Exclus�o n�o permitida"###"Aten��o"###"Cadastro de Empresa"
	lRet := .F.
EndIf

// Verifica relacionamento na GBH (Paciente).
If lRet .And. GBH->(FieldPos("GBH_CODEMP")) > 0
	If HS_CountTB("GBH", "GBH_CODEMP  = '" + GAE->GAE_CODEMP + "'")  > 0
		HS_MsgInf(STR0009,STR0008,STR0006)  //"Existe(m) paciente(s) cadastrado(s) para esta empresa. Exclus�o n�o permitida"###"Aten��o"###"Cadastro de Empresa"
		lRet := .F.
	EndIf
EndIf  */

Return(lRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � MenuDef  � Autor � Saude                 � Data � 08/06/09 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Defini��o do aRotina (Menu funcional)                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � MenuDef()                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function MenuDef()
//��������������������������������������������������������������Ŀ
//� Define Array contendo as Rotinas a executar do programa      �
//� ----------- Elementos contidos por dimensao ------------     �
//� 1. Nome a aparecer no cabecalho                              �
//� 2. Nome da Rotina associada                                  �
//� 3. Usado pela rotina                                         �
//� 4. Tipo de Transa��o a ser efetuada                          �
//�    1 - Pesquisa e Posiciona em um Banco de Dados             �
//�    2 - Simplesmente Mostra os Campos                         �
//�    3 - Gera arquivo TXT para exportacao                      �
//�    4 - Recebe arquivo TXT                                    �
//����������������������������������������������������������������
Local aRotina :=	{{OemtoAnsi(STR0001) , "axPesqui"	, 0, 1, 0, nil},; //"Pesquisar"
					 {OemtoAnsi(STR0002) , "HS_ACL"		, 0, 2, 0, nil},; //"Visualizar"
					 {OemtoAnsi(STR0003) , "HS_ACL"		, 0, 3, 0, nil},; //"Incluir"
					 {OemtoAnsi(STR0004) , "HS_ACL"		, 0, 4, 0, nil},; //"Alterar"
					 {OemtoAnsi(STR0005) , "HS_ACL"		, 0, 5, 0, nil}} //"Excluir"
                     
Return(aRotina)