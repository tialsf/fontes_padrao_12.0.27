#INCLUDE "HSPAHF14.ch"
#include "protheus.CH"
#include "colors.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � HSPAHF14 � Autor � Bruno Santos       � Data �27/12/2005   ���
�������������������������������������������������������������������������͹��
���Descricao � Ficha atendimento ambulatorial                             ���
�������������������������������������������������������������������������͹��
���Uso       � Modulo Gestao Hospitalar                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function HSPAHF14(cModImp, nQtdVias)
    
 cModImp  := IIf(cModImp  == Nil, "0", cModImp )
 nQtdVias := IIf(nQtdVias == Nil,   1, nQtdVias)

 If cModImp == "3"
  Fs_ImpGraf(nQtdVias)
 EndIf

Return(Nil)


Static Function Fs_ImpGraf(nQtdVias)
 Local cVerso := GetMv("MV_FRVERSO",,"N")
 Local nQImp := 1
 
 For nQImp := 1 To nQtdVias
  Fs_ImpFre()   
  If cVerso == "S"
   Fs_ImpVer()    
  EndIf
 Next

Return(Nil)

Static Function Fs_ImpFre()
 Local nLinSum := 0
 Local nI := 0

 oPrint:StartPage()     				//** Inicia uma Nova Pagina
 oPrint:Box(0100,0030,2330,3230)   	// Box que circunda a pagina
 IF File("logo_hsp.bmp")
  oPrint:SayBitmap( 115,70,"logo_hsp.bmp", 600, 200 )
 Else
  oPrint:Say(115,70,"logo_hsp.bmp",Hs_FchFt(10))
 Endif
 oPrint:Say(0180,0900,aDadosEmp[2]+" - "+aDadosEmp[3] 										,Hs_FchFt(1))
 oPrint:Say(0220,0900,STR0001 + aDadosEmp[7] + STR0002 + aDadosEmp[8]	  						,Hs_FchFt(1)) //"Fone: "###" - Fax: "
 oPrint:Say(0260,0900,STR0003 + aDadosEmp[6] + " - " + aDadosEmp[4] + " - " + aDadosEmp[5]	,Hs_FchFt(1)) //"CEP: "
 oPrint:Say(0190,2500,STR0004																	,Hs_FchFt(10)) //"ADMISS�O E ALTA"
 oPrint:Line(0330,0030,0330,3230) // IMPRIME LINHA Horizontal
 oPrint:Line(1165,0030,1165,3230) // IMPRIME LINHA Horizontal Central
 oPrint:Line(0330,2300,2330,2300) // IMPRIME LINHA Vertical
 
 // Identificacao
 oPrint:Say(0350,0070,STR0005,Hs_FchFt(9)) //"I D E N T I F I C A � � O"
 oPrint:Say(0470,0070,STR0006 + GCY->GCY_REGGER+" - " + Left(GCY->GCY_NOME,30) + STR0007 + GCY->GCY_REGATE + STR0008 + Dtoc(GCY->GCY_DATATE) + STR0009 + GCY->GCY_HORATE ,Hs_FchFt(11)) //### //"Paciente: "###"     N�. Registro: "###"   Data.: "###"  Hora: "
 oPrint:Say(0510,0070,STR0010 + dtoc(POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_DTNASC")) + STR0011 + TRANSFORM(alltrim(str(Calc_anos(POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_DTNASC"), GCY->GCY_DATATE))),"@E 999") + STR0012 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_RG") ,Hs_FchFt(11)) //### //"Nascimento: "###"      Idade: "###"    R.G.: "
 oPrint:Say(0550,0050,STR0013 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_NATURA") + STR0014 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_CODNAC"),Hs_FchFt(11)) //" Local Nascimento: "###"  Nacionalidade: "
 oPrint:Say(0590,0070,STR0015 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_CORPEL") + STR0016 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_RELIGI") + STR0017 + IIF(POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_SEXO")=="0",STR0018,STR0019),Hs_FchFt(11)) //"Cor: "###"   Religi�o: "###"  Sexo: "###"Masculino"###"Feminino"
 oPrint:Say(0630,0070,STR0020 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_TEL") + STR0021 + ALLTRIM(POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_MUN")) + "/"+ POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_EST")   ,Hs_FchFt(11)) //### //"Telefone: "###"    Munic�pio: "
 oPrint:Say(0670,0070,STR0022 + ALLTRIM(POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_END"))+","+POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_NUM") + STR0023 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_BAIRRO")  ,Hs_FchFt(11)) //### //"Domicilio: "###" - Bairro: "
 oPrint:Say(0710,0070,STR0024,Hs_FchFt(11)) //"Domic�lio de Proced�ncia:____________________________________________________________________________________"
 oPrint:Say(0750,0070,STR0025 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_PROFIS") + STR0026,Hs_FchFt(11)) //"Profiss�o ou Cargo: "###" Local de Trabalho:_______________________"
 oPrint:Say(0790,0070,STR0027,Hs_FchFt(11)) //"Reparti��o:_______________________________  Secret�ria:__________________________    Fone:___________________"
 oPrint:Say(0830,0070,STR0028,Hs_FchFt(11)) //"Class:           Servidor (   )                 Benefici�rio (   )                Particular (   )"
 oPrint:Say(0870,0070,STR0029 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_NOMPAI") + STR0030,Hs_FchFt(11)) //"Nome do Pai: "###"                    N�. Reg.___________________"
 oPrint:Say(0910,0070,STR0031 + POSICIONE("GBH",1,XFILIAL("GBH")+GCY->GCY_REGGER,"GBH_NOMMAE") + STR0030,Hs_FchFt(11)) //"Nome do M�e: "###"                    N�. Reg.___________________"
 oPrint:Say(0950,0070,STR0032 + HS_IniPadr("GFD", 1, GCY->GCY_CODRES, "GFD_NOME",,.F.) + STR0033,Hs_FchFt(11)) //"Pessoa Responsavel: "###"                      Fone:_________________"
 oPrint:Say(1000,0070,STR0034,Hs_FchFt(11)) //"Endere�o Responsavel: _____________________________________________________________    Fone:_________________"
 oPrint:Say(1050,0070,STR0035,Hs_FchFt(11)) //"Local de Trabalho:_________________________________________________________________    Fone:_________________"
 oPrint:Say(1100,0070,STR0036,Hs_FchFt(11)) //"Notifica��o a:_______________________________________________________   Parentesco:__________________________"
 
 oPrint:Say(0350,2350,STR0037,Hs_FchFt(7)) //"Diagn�stico de Admiss�o:"
 oPrint:Line(0450,2350,0450,3200)
 oPrint:Line(0530,2350,0530,3200)
 oPrint:Line(0610,2350,0610,3200)
 
 oPrint:Say(0650,2350,STR0038,Hs_FchFt(7)) //"M�dico que autorizou a admiss�o:"
 oPrint:Line(0750,2350,0750,3200)
 
 oPrint:Say(0800,2350,STR0039,Hs_FchFt(7)) //"Data:"
 oPrint:Line(0830,2500,0830,3200)
 
 oPrint:Say(0850,2350,STR0040,Hs_FchFt(7)) //"Cl�nica:"
 oPrint:Say(1050,2350,STR0041,Hs_FchFt(7)) //"CID:"
 
 // Sumario
 oPrint:Say(1200,0070,STR0042,Hs_FchFt(9)) //"SUM�RIO DE ALTA:"
 oPrint:Line(1250,0650,1250,2240)
 nlinSum := 1330
 FOR nI = 1 TO 12            	
  oPrint:Line(nLinSum,0070,nLinSum,2240)
  nLinSum := nLinSum + 80
 next
 
 oPrint:Say(1200,2350,STR0043,Hs_FchFt(7)) //"Diagn�stico na Alta:"
 oPrint:Line(1300,2350,1300,3200)
 oPrint:Line(1380,2350,1380,3200)
 oPrint:Line(1460,2350,1460,3200)
 
 oPrint:Say(1530,2350,STR0039,Hs_FchFt(7)) //"Data:"
 oPrint:Line(1560,2500,1560,3200)
 
 oPrint:Say(1600,2350,STR0044,Hs_FchFt(7)) //"Dias de Hospitaliza��o:"
 oPrint:Line(1630,2700,1630,3200)
 
 oPrint:Say(1650,2600,STR0045,Hs_FchFt(3)) //"TIPO DE ALTA"
 oPrint:Say(1730,2350,STR0046,Hs_FchFt(7)) //"Iniciativa M�dica"
 oPrint:Say(1780,2350,STR0047,Hs_FchFt(7))  //"Transferencia    "
 oPrint:Say(1830,2350,STR0048,Hs_FchFt(7)) //"A Pedido         "
 oPrint:Say(1880,2350,STR0049,Hs_FchFt(7)) //"Abandono         "
 oPrint:Say(1930,2350,STR0050,Hs_FchFt(7)) //"Indisciplina     "
 oPrint:Say(1980,2350,STR0051,Hs_FchFt(7)) //"�bito            "
 nLinSum := 1730
 For nI = 1 to 6
  oPrint:Say(nLinSum,2750,"(    )",Hs_FchFt(7))
  nLinSum := nLinSum + 50 
 next
 
 oPrint:Say(2050,2400,STR0052,Hs_FchFt(3)) //"RESULTADO DE TRATAMENTO"
 oPrint:Say(2130,2350,STR0053,Hs_FchFt(7)) //"Cura       "
 oPrint:Say(2180,2350,STR0054,Hs_FchFt(7)) //"Melhorado  "
 oPrint:Say(2230,2350,STR0055,Hs_FchFt(7)) //"Inalterado "
 oPrint:Say(2280,2350,STR0056,Hs_FchFt(7)) //"Piorado    "
 nLinSum := 2130
 For nI = 1 to 4
  oPrint:Say(nLinSum,2750,"(    )",Hs_FchFt(7))
  nLinSum := nLinSum + 50 
 next
 
 oPrint:Endpage()
Return(nil)

Static Function Fs_ImpVer()
 Local nLin := 550
 Local nI := 0
 
  nLin := 550

  oPrint:StartPage()     				//** Inicia uma Nova Pagina
  IF File("logo_hsp.bmp")
   oPrint:SayBitmap( 115,70,"logo_hsp.bmp", 600, 200 )
  Else
   oPrint:Say(115,70,"logo_hsp.bmp",Hs_FchFt(10) )
  Endif
  
  oPrint:Say(0190,2400,STR0057,Hs_FchFt(10)) //  //"OBSERVA��O M�DICA"
  oPrint:Say(0400,0070,STR0058,Hs_FchFt(7))  //  //"Queixa Principal"
  oPrint:Line(0430,0400,0430,3200)
  
  oPrint:Say(0470,0070,STR0059,Hs_FchFt(7))  //  //"Hist�rico da Doen�a Atual"
  oPrint:Line(0500,0550,0500,3200)
  FOR nI = 1 TO 4
   oPrint:Line(nLin+30,0070,nLin+30,3200)
   nLin := nLin + 60
  next
  
  oPrint:Say(nLin,0070,STR0060,Hs_FchFt(7)) //  //"Antecedentes Familiares e Pessoais"
  oPrint:Line(nLin+30,0700,nLin+30,3200)
  nLin := nLin + 100
  FOR nI = 1 TO 4    
   oPrint:Line(nLin,0070,nLin,3200)
   nLin := nLin + 60
  next
  
  oPrint:Say(nLin,0070,STR0061,Hs_FchFt(7))  //, //"Alimenta��o e Desenvolvimento"
  oPrint:Line(nLin+30,0650,nLin+30,3200)
  nLin := nLin + 100
  FOR nI = 1 TO 4    
   oPrint:Line(nLin,0070,nLin,3200)
   nLin := nLin + 60
  next
  
  oPrint:Say(nLin,0070,STR0062,Hs_FchFt(7)) //  //"Exames F�sico Especial"
  oPrint:Line(nLin+30,0550,nLin+30,3200)
  nLin := nLin + 100
  FOR nI = 1 TO 4    
   oPrint:Line(nLin,0070,nLin,3200)
   nLin := nLin + 60
  next
  
  oPrint:Say(nLin,0070,STR0063,Hs_FchFt(7)) //  //"Dados positivos de outros aparelhos ou sitemas"
  oPrint:Line(nLin+30,0900,nLin+30,3200)
  nLin := nLin + 100
  FOR nI = 1 TO 4    
   oPrint:Line(nLin,0070,nLin,3200)
   nLin := nLin + 60
  next
  
  oPrint:Say(nLin,0070,STR0064,Hs_FchFt(7)) //  //"Impress�o Diagn�stica"
  oPrint:Line(nLin+30,0550,nLin+30,3200)
  nLin := nLin + 100
  FOR nI = 1 TO 2    
   oPrint:Line(nLin,0070,nLin,3200)
   nLin := nLin + 60
  next
  oPrint:Endpage()

Return(nil)