#INCLUDE "HSPAHF15.ch"
#include "protheus.CH"
#include "colors.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � HSPAHF15 � Autor � Bruno Santos       � Data �27/12/2005   ���
�������������������������������������������������������������������������͹��
���Descricao � Ficha atendimento ambulatorial                             ���
�������������������������������������������������������������������������͹��
���Uso       � Modulo Gestao Hospitalar                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function HSPAHF15(cModImp, nQtdVias)
    
 cModImp  := IIf(cModImp  == Nil, "0", cModImp )
 nQtdVias := IIf(nQtdVias == Nil,   1, nQtdVias)

 If cModImp == "3"
  Fs_ImpGraf(nQtdVias)
 EndIf

Return(Nil)

Static Function Fs_ImpGraf(nQtdVias)
 Local nQImp := 1
 LOCAL nlinSum := 0
 Local T := 0
 Local L := 0

 For nQImp := 1 To nQtdVias
  oPrint:SetLandscape()  				//** Para Imprimir em Formato Paisagem
  oPrint:StartPage()     				//** Inicia uma Nova Pagina
  oPrint:Box(0100,0030,2300,3200)   	// Box que circunda a pagina
  IF File("logo_hsp.bmp")
   oPrint:SayBitmap( 115,70,"logo_hsp.bmp", 600, 200 )
  Else
   oPrint:Say(115,70,"logo_hsp.bmp",Hs_FchFt(10) )
  Endif
  oPrint:Say(0200,1100,STR0001,Hs_FchFt(10))// //"FORMUL�RIO DE PRESCRI��O M�DICA"
  
  oPrint:Say(0350,0070,STR0002 + GCY->GCY_REGGER + "-" + GCY->GCY_NOME + STR0003 + GCY->GCY_REGATE,Hs_FchFt(12))//### //"Paciente.: "###"          Registro.: "
  oPrint:Say(0400,0070,STR0004 + HS_IniPadr("GCZ", 2, GCY->GCY_REGATE+"0", "GCZ_CODCON") + "-" + POSICIONE("GA9",  1, XFILIAL("GA9") + HS_IniPadr("GCZ", 2, GCY->GCY_REGATE+"0", "GCZ_CODCON"), "GA9_NREDUZ") + STR0005 + DTOC(GCY->GCY_DATATE) + STR0006 + GCY->GCY_HORATE + STR0007 + GCY->GCY_QUAINT + "/" + GCY->GCY_LEIINT ,Hs_FchFt(12))//######### //"Convenio.: "###"           Data..: "###"           Hora.....: "###"          Leito..: "
  oPrint:Say(0450,0070,STR0008 + GCY->GCY_CODCRM + "-" + POSICIONE("SRA", 11, XFILIAL("SRA") + GCY->GCY_CODCRM,"RA_NOME") + STR0014 + DtoC(dDataBase) +" "+Time(),Hs_FchFt(12))//"Medico...: " //"M�dico...: "
  
  nlinSum := 0500
  FOR T = 1 TO 18
   oPrint:box(nLinSum,0030,nLinSum + 100,3200)
   nLinSum := nLinSum + 100
  next

  nlinSum := 0650
  FOR T = 1 TO 17
   oPrint:line(nLinSum,1800,nLinSum,3000)
   nLinSum := nLinSum + 100
  next                     
  
  nColSum := 1850
  FOR T = 1 TO 23
   oPrint:line(0600,nColSum,2300,nColSum)
   nColSum := nColSum + 50
  next

  nlinSum := 0610
  FOR T = 1 TO 17
   nColSum := 1810
   nHora := 7
   FOR L = 1 TO 24
    oPrint:say(nLinSum,nColSum,strzero(nHora,2,0),Hs_FchFt(6))
    nHora := nHora + 1
    If nHora == 25
     nHora := 1
    endif
    nColSum := nColSum + 50
   next
   nLinSum := nLinSum + 100
  next                     

  oPrint:Line(0330,0030,0330,3200) // IMPRIME LINHA Horizontal
  oPrint:Line(0500,0180,2300,0180) // IMPRIME LINHA Vertical
  oPrint:Line(0500,1800,2300,1800) // IMPRIME LINHA Vertical
  oPrint:Line(0500,3100,2300,3100) // IMPRIME LINHA Vertical
  oPrint:Line(0500,3000,2300,3000) // IMPRIME LINHA Vertical

  oPrint:Say(0530,0050,STR0009,Hs_FchFt(12))// //"ITEM"
  oPrint:Say(0530,0800,STR0010,Hs_FchFt(12))// //"PRESCRI��O"
  oPrint:Say(0530,2300,STR0011,Hs_FchFt(12))// //"HOR�RIO"
  oPrint:Say(0530,3005,STR0012,Hs_FchFt(12))// //"UTI"
  oPrint:Say(0530,3115,STR0013,Hs_FchFt(12))// //"DEV"
  
  oPrint:EndPage()
 Next

Return(Nil)