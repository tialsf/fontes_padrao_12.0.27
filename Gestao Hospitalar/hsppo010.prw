#INCLUDE "PROTHEUS.CH" 
#INCLUDE "TopConn.ch"

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    �HSPPO010  � Autor � Rogerio Tabosa        � Data � 14/04/2009 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Monta array para Painel de Gestao Tipo 2 Padrao 1             ���
���          �                                                              ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �HSPPO010()                                                    ���
���������������������������������������������������������������������������Ĵ��
���������������������������������������������������������������������������Ĵ��
���Uso       � SIGAMDI                                                      ���
���������������������������������������������������������������������������Ĵ��
��� Atualizacoes sofridas desde a Construcao Inicial.                       ���
���������������������������������������������������������������������������Ĵ��
��� Programador  � Data   � BOPS �  Motivo da Alteracao                     ���
���������������������������������������������������������������������������Ĵ��
���              �        �      �                                          ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/


Function HSPPO010(nCodInd, lImpressao)

Local aArea		:= GetArea()
//Local aAreaGCY	:= GCY->(GetArea())
//Local cAliasRC1	:= "GCY"
Local cCodLocI	:= "  "
Local cCodLocF	:= "ZZ"
Local aRet		:= {} 
Local aRet2		:= {} 
Local cSql 		:= "" 
Local cQryGTA	:= ""
Local aDadosIn	:= {}
Local aComboIn	:= {}
Local aDadoCmb1	:= {}
Local aDadoCmb2	:= {}
Local aDadoCmb3	:= {}
Local nJ		:= 0
Local nI		:= 0
Local nPosCmb	:= 0
Local cDesCmb	:= ""

Private cMes		:= StrZero(Month(dDataBase),2)
Private cAno		:= Substr(DTOC(dDataBase),7,4)              
Private cMesAnt	:= IIf(cMes == "01", "12", StrZero(Val(cMes)-1,2))
Private cAnoAnt	:= Iif(cMesAnt == "12",Str(Val(cAno)-1),cAno)                           
Private dDtIniDAn	:= CTOD("01/"+cMesAnt+"/"+cAnoAnt)
Private dDtFimDAn	:= CTOD(StrZero(F_ULTDIA(dDtIniDAn),2)+"/"+cMesAnt+"/"+cAnoAnt)
Private cDatIniAn	:= DTOS(dDtIniDAn)
Private cDatFimAn	:= DTOS(dDtFimDAn)
Private dDataIni	:= CTOD("01/"+cMes+"/"+cAno)
Private dDataFim	:= CTOD(StrZero(F_ULTDIA(dDataBase),2)+"/"+cMes+"/"+cAno)
Private dDataIniA	:= CTOD("01/01/"+cAno)
Private dDataFimA := CTOD("31/12/"+cAno)

Private nAteCancM	:= 0
Private nAteCancD	:= 0
Private nAteCancA	:= 0


Default nCodInd 	:= 1 
Default lImpressao  := .F.

FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', nCodInd , 0, 0, {})

//������������������������������������������������������������������������Ŀ
//�                                                                        �
//�                              M E N S A L                               �
//�                                                                        �
//��������������������������������������������������������������������������
//������������������������������������������������������������������������Ŀ
//�Numero de Atendimentos cancelados por mes                               �
//��������������������������������������������������������������������������
//cAliasRC1 := GetNextAlias()

//Pergunte("FINPGL05",.F.)   


DbSelectArea("GTA")
DbGoTop()
DbGoTo(nCodInd)
//cQryGTA := E_MSMM(GTA->GTA_QUERY)

FWLogMsg('WARN',, 'SIGAPLS', funName(), '', '01', GTA->GTA_CODIND , 0, 0, {})


If !Empty(GTA->GTA_CODIND)
	If !Empty(GTA->GTA_TITCMB)
		If !Empty(GTA->GTA_CMB1)
			AADD(aComboIn,{GTA->GTA_CMB1})
			If !Empty(GTA->GTA_CMB2)			
				AADD(aComboIn,{GTA->GTA_CMB2})			
				If !Empty(GTA->GTA_CMB3)			
					AADD(aComboIn,{GTA->GTA_CMB3})
				EndIf
			EndIf
		EndIf
	EndIf

	DbSelectArea("GTB")
	DbGoTop()
	DbSetOrder(1)
	If DbSeek(xFilial("GTB") + GTA->GTA_CODIND)
		While !GTB->(Eof()) .AND. GTB->GTB_CODIND == GTA->GTA_CODIND
			//                       1                     2                  3                 4                     5               6
			AADD(aDadosIn, {Alltrim(GTB->GTB_NOME), GTB->GTB_CODITE, GTB->GTB_COMBO,FS_COLOR(GTB->GTB_COLOR),GTB->GTB_FORMAT,GTB->GTB_QUERY})
			GTB->(DbSkip())
		End	
	EndIf
Else
	Return(Nil)
EndIf


If Len(aDadosIn) == 1 
		cQryGTA := FS_INDRQry(aDadosIn[1,6])	
		cQryGTA := ChangeQuery(cQryGTA)
		TCQUERY cQryGTA NEW ALIAS "TMPGTA"
		
		Aadd( aRet, { IIf(!Empty(GTA->GTA_CMB1),Alltrim(GTA->GTA_CMB1),"Nenhum"), { ;																	 
					{ aDadosIn[1,1] + ": ", Transform(If(TMPGTA->(Eof()),0,TMPGTA->CONTADOR),"@R 999,999,999") ,aDadosIn[1,4],{ || } } } } )	
					
		TMPGTA->(DbCloseArea())						
Else                         
	For nJ := 1 to Len(aDadosIn)
	
		nPosCmb := IIf(!Empty(aDadosIn[nJ,3]),Val(aDadosIn[nJ,3]),0)
			
		cQryGTA := FS_INDRQry(aDadosIn[nJ,6])	
		cQryGTA := ChangeQuery(cQryGTA)
		TCQUERY cQryGTA NEW ALIAS "TMPGTA"
	
	    If nPosCmb == 1 
	    	AADD(aDadoCmb1,{ aDadosIn[nJ,1] + ": ", Transform(If(TMPGTA->(Eof()),0,TMPGTA->CONTADOR),IIf(!Empty(Alltrim(aDadosIn[nJ,5])),Alltrim(aDadosIn[nJ,5]),"@E 99,999.99")) ,aDadosIn[nJ,4],{ || } })
	    ElseIf nPosCmb == 2 
	    	AADD(aDadoCmb2,{ aDadosIn[nJ,1] + ": ", Transform(If(TMPGTA->(Eof()),0,TMPGTA->CONTADOR),IIf(!Empty(Alltrim(aDadosIn[nJ,5])),Alltrim(aDadosIn[nJ,5]),"@E 99,999.99")) ,aDadosIn[nJ,4],{ || } })	    
	    ElseIf nPosCmb == 3 
	    	AADD(aDadoCmb3,{ aDadosIn[nJ,1] + ": ", Transform(If(TMPGTA->(Eof()),0,TMPGTA->CONTADOR),IIf(!Empty(Alltrim(aDadosIn[nJ,5])),Alltrim(aDadosIn[nJ,5]),"@E 99,999.99")) ,aDadosIn[nJ,4],{ || } })	    	    	
		EndIf
		
		TMPGTA->(DbCloseArea())	
	Next nJ
EndIf

If Len(aDadoCmb1) > 0 .AND. Len(aRet) == 0
		Aadd( aRet, { aComboIn[1,1] , aDadoCmb1 } )
	If Len(aDadoCmb2) > 0
		Aadd( aRet, { aComboIn[2,1] , aDadoCmb2 } )
		If Len(aDadoCmb3) > 0
			Aadd( aRet, { aComboIn[3,1] , aDadoCmb3 } )
		EndIf
	EndIf
EndIf

RestArea(aArea)


Return aRet   

Static Function FS_COLOR(cColor)

//0=Preto;1=Vermelho;2-Verde;3-Azul

If Alltrim(cColor) == "0"
	Return(CLR_BLACK)
ElseIf	Alltrim(cColor) == "1"
	Return(CLR_RED)
ElseIf	Alltrim(cColor) == "2"
	Return(CLR_GREEN)
ElseIf	Alltrim(cColor) == "3"							
	Return(CLR_BLUE)
EndIf
		
Return(CLR_BLACK)

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    |FS_INDRQry� Autor � Rogerio Tabosa        � Data � 14/04/2009 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o �Validade Sintaxe da query criada pelo usu�rio                 ���
���          �                                                              ���
���������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                              ���
���������������������������������������������������������������������������Ĵ��
���������������������������������������������������������������������������Ĵ��
���Uso       � SIGAMDI                                                      ���
���������������������������������������������������������������������������Ĵ��
��� Atualizacoes sofridas desde a Construcao Inicial.                       ���
���������������������������������������������������������������������������Ĵ��
��� Programador  � Data   � BOPS �  Motivo da Alteracao                     ���
���������������������������������������������������������������������������Ĵ��
���              �        �      �                                          ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/

Static Function FS_INDRQry(cQuery)
 Local cStr := "", xValor, cQryAux := ""

 While (nPos1 := At("[", cQuery)) > 0
  If (nPos2 := At("]", cQuery)) > 0
  
   cStr   := Substr(cQuery, nPos1 + 1 , nPos2 - nPos1 - 1 )
   xValor := &(cStr)  
   
   If ValType(xValor) == "C"
    cQryAux := xValor
   ElseIf ValType(xValor) == "N"
    cQryAux := Str(xValor)
   ElseIf ValType(xValor) == "D"
    cQryAux := "'" + DtoS(xValor) + "'"
   Else
    cQryAux := "'" + xValor + "'"
   EndIf
  
   cQuery := StrTran(cQuery, "[" + cStr, cQryAux,,1)
   cQuery := StrTran(cQuery, "]", "",,1)
   
   xValor := Nil
  Else
   HS_MsgInf("Sintaxe incorreta", "Aten��o","An�lise") 
   Exit
  EndIf
 End

Return(cQuery)
                   