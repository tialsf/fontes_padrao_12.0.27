#INCLUDE 'TOTVS.CH'
#INCLUDE 'RESTFUL.CH'

/*/-----------------------------------------------------------
{Protheus.doc} WSTMSMaplink()
Call Back MapLink 
Uso: SIGATMS
@sample

@author Caio Murakami
@since 12/08/2019
@version 1.0
-----------------------------------------------------------/*/
WSRESTFUL WSTMSMaplink DESCRIPTION 'Call Back SIGATMS X MapLink'  FORMAT 'application/json,text/html'

    WSDATA Fields     AS STRING  OPTIONAL
    
    WSMETHOD POST Main DESCRIPTION 'SIGATMS X MapLink';
    WSSYNTAX "api/tms/v1/WSTMSMaplink/";
    PATH 'api/tms/v1/WSTMSMaplink';
    PRODUCES APPLICATION_JSON

    WSMETHOD GET Main DESCRIPTION 'SIGATMS X MapLink';
    WSSYNTAX "api/tms/v1/WSTMSMaplink/";
    PATH 'api/tms/v1/WSTMSMaplink';
    PRODUCES APPLICATION_JSON

ENDWSRESTFUL

/*/-----------------------------------------------------------
{Protheus.doc} POST /WSTMSMaplink
Call back integra��o tms x maplink

@param  WSTMSMaplink, caracter, Campos que ser�o retornados no GET.
@return lRet        , L�gico, Informa se o processo foi executado com sucesso.

@author Caio Murakami
@since 12/08/2019
@version 1.0
---------------------------------------------------------------/*/
WSMETHOD POST Main WSRECEIVE Callback WSSERVICE WSTMSMaplink
Local lRet          := .T. 
Local cResult       := self:aquerystring[1][2]
Local oResult       := FwJsonObject():New() 
Local cID           := ""
Local cApi          := ""
Local cFilOri       := ""
Local cViagem       := ""
Local oMapLink      := Nil
Local cURL          := "https://lbslocal-prod.apigee.net"
Local cEvent        := "/trip/v1/problems/"
Local cTypeEvent    := "POST"
Local cToken        := ""

If FWJsonDeserialize(cResult,@oResult)
     If AttIsMemberOf(oResult,"id")
        cID     := oResult:id
        DLU->(dbSetOrder(3))
        If DLU->(MsSeek( xFilial("DLU") + RTrim(cID) ) )
            cApi    := Upper( AllTrim( DLU->DLU_API ) ) 
            cFilOri := SubStr( DLU->DLU_CHVENT , 1 , TamSX3("DTQ_FILORI")[1]) 
            cViagem := SubStr( DLU->DLU_CHVENT , TamSX3("DTQ_FILORI")[1] + 1 , TamSX3("DTQ_VIAGEM")[1] )

            If AttIsMemberOf(oResult,"STATUS") .And. !Empty( oResult:status )
                cRet     := Upper( AllTrim( oResult:status ) ) 

                If cRet == "SOLVED"
                    oMapLink  := TMSBCARotaInt():New(cURL,cEvent,cTypeEvent, cFilOri , cViagem, .F. )
                    cToken    := oMapLink:Token() 
                    
                    If!Empty(cToken)

                        If cApi == "PLANNINGPOST"                            
                            oMapLink:PlaningSolution(cID)
                            oMapLink:TripPost()
                            ::SetResponse('{"id":' + cID +' } ')
                        ElseIf cApi == 'TRIPPOST'
                            oMapLink:TripSolution(cID)
                            oMapLink:calculateTolls()
                            oMapLink:UpdatePedagio()
                            ::SetResponse('{"id":' + cID +' } ')
                        EndIf
                    
                    EndIf

                    oMapLink:Destroy()
                
                EndIf

            EndIf
        EndIf
     EndIf
EndIf

Return .T. 

/*/-----------------------------------------------------------
{Protheus.doc} GET /WSTMSMaplink
Call back integra��o tms x maplink

@param  WSTMSMaplink, caracter, Campos que ser�o retornados no GET.
@return lRet        , L�gico, Informa se o processo foi executado com sucesso.

@author Caio Murakami
@since 12/08/2019
@version 1.0
---------------------------------------------------------------/*/
WSMETHOD GET Main WSSERVICE WSTMSMaplink

::SetContentType("application/json")
::SetResponse('{"id":"SIGATMS X MAPLINK", "name":"sample"}')

Return .T. 



