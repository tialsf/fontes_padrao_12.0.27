#Include 'TmsA170.ch'
#Include 'Protheus.ch'

Static lTM170GRV := ExistBlock("TM170GRV")

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA170  � Autor � Antonio C Ferreira    � Data �22.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Lote de Entrada de Notas Fiscais.                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function TMSA170(xAutoCab,nOpcAuto)

Local aCores    := {}
Local cLotNfc   := ''

Private cCadastro	:= STR0001		//"Lote de Entrada de Notas Fiscais."
Private aRotina		:= MenuDef()

Private l170Auto	:= xAutoCab <> NIL
Private aAutoCab	:= {}

Default xAutoCab    := {}
Default nOpcAuto    := 3

Aadd(aCores,{"DTP_STATUS=='1'",'BR_AMARELO'		})		// Aberto
Aadd(aCores,{"DTP_STATUS=='2'",'BR_VERDE'		})		// Digitado
Aadd(aCores,{"DTP_STATUS=='3'",'BR_AZUL'		})		// Calculado
Aadd(aCores,{"DTP_STATUS=='4'",'BR_VERMELHO'	})		// Bloqueado
Aadd(aCores,{"DTP_STATUS=='5'",'BR_LARANJA' 	})		// Erro de Gravacao

If	!l170Auto
	mBrowse(6,1,22,75,'DTP',,,,,,aCores)
Else
	lMsHelpAuto := .T.
	aAutoCab    := xAutoCab
	MBrowseAuto(nOpcAuto,Aclone(aAutoCab),'DTP')
	cLotNfc := DTP->DTP_LOTNFC
EndIf

RetIndex('DTP')

Return(cLotNfc)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA170Mnt� Autor � Antonio C Ferreira    � Data �22.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Lote de Entrada de Notas Fiscais.                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA170Mnt(ExpC1,ExpN1,ExpN2)                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                   ���
���          � ExpN1 = Numero do registro                                 ���
���          � ExpN2 = Opcao selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/

Function TMSA170Mnt( cTmsAlias, nTmsReg, nTmsOpcx, aEstrutura )

Local cServico, cTarefa, nA, lGet

//-- EnchoiceBar
Local aTmsVisual
Local aTmsAltera
Local aTmsButtons	  := {}
Local aTelOld		  := Iif( Type('aTela') == 'A', aClone( aTela ), {} )
Local aGetOld		  := Iif( Type('aGets') == 'A', aClone( aGets ), {} )
Local nOpca			  := 0
Local oTmsEnch

//-- Dialog
Local cCadOld		  := Iif( Type('cCadastro') == 'C', cCadastro, '' )
Local oTmsDlgEsp
//-- GetDados
Local aHeaOld		  := Iif( Type('aHeader') == 'A', aClone( aHeader ), {} )
Local aColOld		  := Iif( Type('aCols') == 'A', aClone( aCols ), {} )
Local NOld          := Iif( Type('N') == 'N', N, 0 )
Local aNoFields	  := {}
Local nCntFor	     := 0
Local lNaoExcluir   := .F.
Local nMaximoLinhas := 0
//-- Controle de dimensoes de objetos
Local aObjects		  := {}
Local aInfo			  := {}
//-- Checkbox
Local oAllMark
Local dDataTMS := GetMv("MV_DATATMS",.F.,CtoD(''))  
Local lDataVld := dDataBase <= dDataTMS

//-- Valida Integridade do DTP
Local aAreaDTP:= DTP->(GetArea())
Local cFilOri := ''
Local cLotNfc := ''
Local lViaCol := DTP->(ColumnPos("DTP_VIACOL")) > 0
Local lSetKey 	:= .F. 	
Local lContinua	:= .T.
//-- EnchoiceBar
Private aTela :={}
Private aGets :={}
//-- GetDados
Private aHeader	  := {}
Private aCols		  := {}
Private oTmsGetD
Private aTmsPosObj  := {}
//-- Checkbox
Private lAllMark	  := .T.   // Usado para o controle da repeticao do campo memo DTP_MOTIVO. NAO TROQUE PARA LOCAL!!!


DEFAULT cTmsAlias   := 'DTP'
DEFAULT nTmsReg	  := 1
DEFAULT nTmsOpcx	  := 2
DEFAULT aEstrutura  := {}

l170Auto := Iif(Type('l170Auto') == 'U',.F.,l170Auto)

If FunName() == "TMSA050" .And. Type('aSetKey') <> 'U'
	lSetKey	:= .T. 
	//-- Finaliza Teclas de Atalhos
	TmsKeyOff(aSetKey)
EndIf

If nTmsOpcx <> 2 .And. nTmsOpcx <> 3
	//-- Verifica se o agendamento est� sendo utilizado por outro usu�rio no painel de agendamentos
	If !TMSAVerAge("5",,,,,,,,,,,,"2",.T.,.T.,DTP->DTP_LOTNFC,,)
		lContinua	:= .F. 
	EndIf
EndIf

If	!l170Auto .And. lContinua
	If (nTmsOpcx >= 4) .And. !(AllTrim(DTP->DTP_STATUS) $ "1/2")
		//-- Limpa marcas dos agendamentos
		//-- Analisar a inser��o desta rotina antes de cada Return( .F. ) ou ( .T. ), quando utilizado TmsVerAge
		If !IsInCallStack("TMSAF76")
			TMSALimAge(StrZero(ThreadId(),20))
		EndIf
		Help(" ", 1, "TmsA17001") // "O Lote de Entrada ja foi Calculado ou Encerrado, nao e possivel manipular o mesmo."
		lContinua	:= .F. 
   ElseIf lDataVld
		//-- Limpa marcas dos agendamentos
		//-- Analisar a inser��o desta rotina antes de cada Return( .F. ) ou ( .T. ), quando utilizado TmsVerAge
		If !IsInCallStack("TMSAF76")
			TMSALimAge(StrZero(ThreadId(),20))
		EndIf
		Help ( " ", 1, "FECHATMS" )  
		lContinua	:= .F. 			
	ElseIf DTP->DTP_FILORI <> cFilAnt
		If nTmsOpcx == 4 //--Alterar
			//-- Limpa marcas dos agendamentos
			//-- Analisar a inser��o desta rotina antes de cada Return( .F. ) ou ( .T. ), quando utilizado TmsVerAge
			If !IsInCallStack("TMSAF76")
				TMSALimAge(StrZero(ThreadId(),20))
			EndIf
			Help("",1,"TMSA17011",,STR0015 + DTP->DTP_FILORI + " / " + STR0016 + DTP->DTP_LOTNFC,4,0) //--Nao e permitido alterar um Lote de outra Filial ### "Filial Origem" ### "Lote"
			lContinua	:= .F. 
		ElseIf nTmsOpcx == 5 //--Excluir
			//-- Limpa marcas dos agendamentos
			//-- Analisar a inser��o desta rotina antes de cada Return( .F. ) ou ( .T. ), quando utilizado TmsVerAge
			If !IsInCallStack("TMSAF76")
				TMSALimAge(StrZero(ThreadId(),20))
			EndIf
			Help("",1,"TMSA17012",,STR0015 + DTP->DTP_FILORI + " / " + STR0016 + DTP->DTP_LOTNFC,4,0) //--Nao e permitido excluir um Lote de outra Filial ### "Filial Origem" ### "Lote"
			lContinua	:= .F. 
		EndIf
	ElseIf nTmsOpcx == 4 .And. lViaCol .And. !Empty(DTP->DTP_VIACOL) .And. !IsInCallStack("TMSAF76") 
		//-- Limpa marcas dos agendamentos
		//-- Analisar a inser��o desta rotina antes de cada Return( .F. ) ou ( .T. ), quando utilizado TmsVerAge
		If !IsInCallStack("TMSAF76")
			TMSALimAge(StrZero(ThreadId(),20))
		EndIf
		Help (" ", 1, "TMSA17018") //N�o � permitido alterar um lote gerado pelo painel de agendamentos, por esta rotina. 
		lContinua	:= .F.  
	EndIf
EndIf

If lContinua
	//-- Configura variaveis da Enchoice
	RegToMemory( cTmsAlias, INCLUI )
	
	If INCLUI
		//-- Ajusta SXE e SXF caso estejam corrompidos.
		//-- Protecao para nao duplicar o numero do lote dentro da filial de origem.
		cFilOri := M->DTP_FILORI
		cLotNfc := M->DTP_LOTNFC
		cMay 	:= AllTrim(xFilial('DTP'))+cFilOri+cLotNfc
		FreeUsedCode()
		DTP->( DbSetOrder( 2 ) ) //-- DTP_FILIAL+DTP_FILORI+DTP_LOTNFC
		While DTP->(MsSeek(xFilial('DTP')+cFilOri+cLotNfc)) .Or. !MayIUseCode(cMay)
			ConfirmSx8()
			cLotNfc := CriaVar("DTP_LOTNFC")
			FreeUsedCode()
			cMay := AllTrim(xFilial('DTP'))+cFilOri+cLotNfc
		EndDo
		M->DTP_FILORI := cFilOri
		M->DTP_LOTNFC := cLotNfc
		If IsInCallStack('TMSAF76') .And. Type('M->DTC_NUMSOL') <> 'U'
			M->DTP_NUMSOL := M->DTC_NUMSOL
		EndIf	

		If IsInCallStack('A540RetMER') .And. ( Type('M->DUU_VIAGEM') <> 'U' .And. Type('M->DUU_FILORI') <> 'U' ).And. Posicione( "DTQ", 2,  xFilial("DTQ") + M->DUU_FILORI + M->DUU_VIAGEM , "DTQ_STATUS" ) == "2" //-- 2=Viagem em tr�nsito
			M->DTP_VIAGEM	:= M->DUU_VIAGEM
		EndIf

		RestArea( aAreaDTP )
	EndIf
	cCadastro:= STR0001 //"Lote de Entrada de Notas Fiscais."
	
	If	!l170Auto
		DC5->(DbSetOrder(1))  // DC5_FILIAL+DC5_SERVIC+DC5_ORDEM
		DC6->(DbSetOrder(1))  // DC6_FILIAL+DC6_TAREFA+DC6_ORDEM
	
		cServico := GetMV("MV_SVCLOT")  // Servico para Conferencia	de lote das notas fiscais
	
		If INCLUI
			If ((cServico==NIL) .Or. ValType(cServico)!="C" .Or. Empty(cServico))
				cServico := "xxx"
			
			ElseIf DC5->(!MsSeek(xFilial("DC5")+cServico))
				Help(" ", 1, "TmsA17003") //-- Parametro MV_SVCLOT com o Codigo de Servico invalido!
			
				cServico := "xxx"
			EndIf
		EndIf
	
	
		If (cServico != "xxx")
			//-- Configura variaveis da GetDados
			FillGetDados( nTmsOpcx, 'DUK', 1, xFilial( 'DUK' ) + M->(DTP_FILORI + DTP_LOTNFC), { ||  DUK->(DUK_FILIAL + DUK_FILORI + DUK_LOTNFC) },;
			{ || .T. }, aNoFields,	/*aYesFields*/, /*lOnlyYes*/, /*cQuery*/, /*bMontCols*/ )
		
			//-- Inicializa a Sequencia da getdados se a linha estiver em branco.
			If Len( aCols ) == 1 .And. Empty( GDFieldGet( 'DUK_SEQUEN', 1 ) )
				nA := 0
			
				If INCLUI
				
					Do While DC5->( !Eof() .And. (DC5_SERVIC == cServico) )
						cTarefa := DC5->DC5_TAREFA
					
						DC6->( MsSeek(xFilial("DC6") + cTarefa) )
						Do While DC6->( !Eof() .And. (DC6_TAREFA == cTarefa) )
							++nA
							If (nA > 1)
								aadd( aCols, Array( len(aCols[1]) ) )
							EndIf
	
							For nCntFor := 1 To Len(aHeader)
								If aHeader[nCntFor,10] != "V"
									aCols[nA,nCntFor]:= CriaVar(aHeader[nCntFor,2])
								Else
									If aHeader[nCntFor,2] == "DUK_ALI_WT"
										aCols[nA,nCntFor]:= "DUK"
									ElseIf aHeader[nCntFor,2] == "DUK_REC_WT"
										aCols[nA,nCntFor]:= 0
									Else
										aCols[nA,nCntFor]:= CriaVar(aHeader[nCntFor,2])
									EndIf
								EndIf
							Next nCntFor
							aCols[nA,Len(aHeader)+1] := .F.
	
							GDFieldPut( 'DUK_SEQUEN', StrZero(nA,Len(DUK->DUK_SEQUEN))	, nA )
							GDFieldPut( 'DUK_SERVIC', cServico                   			, nA )
							GDFieldPut( 'DUK_DESSER', Tabela("L4",cServico,.F.)			, nA )
							GDFieldPut( 'DUK_TAREFA', cTarefa                     			, nA )
							GDFieldPut( 'DUK_DESTAR', Tabela("L2",cTarefa,.F.) 			, nA )
							GDFieldPut( 'DUK_ATIVID', DC6->DC6_ATIVID       				, nA )
							GDFieldPut( 'DUK_DESATI', Tabela("L3",DC6->DC6_ATIVID,.F.)	, nA )
						
							DC6->( DbSkip() )
						EndDo
					
						DC5->( DbSkip() )
					EndDo
				
				Endif
			
				If (nA == 0)
					aCols := {}
					cServico := "xxx"
				Endif
			EndIf
		EndIf
	
	
		nMaximoLinhas := Len(aCols)
	
		lGet := (cServico != "xxx")
	
		//-- Dimensoes padroes
		aSize := MsAdvSize()
		AAdd( aObjects, { 70, 70, .T., .T. } )
		If lGet
			AAdd( aObjects, { 230, 230, .T., .T. } )
		EndIf
	
		aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 5, 5 }
		aTmsPosObj := MsObjSize( aInfo, aObjects,.T.)
	
		DEFINE MSDIALOG oTmsDlgEsp TITLE cCadastro FROM aSize[7],00 TO aSize[6],aSize[5] PIXEL
		//-- Monta a enchoice.
		oTmsEnch		:= MsMGet():New( cTmsAlias, nTmsReg, nTmsOpcx,,,, aTmsVisual, aTmsPosObj[1],aTmsAltera, 3,,,,,,.T. )
	
		If lGet
			//        MsGetDados(                   nT ,                  nL,                 nB,                  nR,            nOpc,     cLinhaOk,      cTudoOk,     cIniCpos,lDeleta,aAlter,nFreeze,lEmpty,nMax,cFieldOk,cSuperDel,aTeclas,cDelOk,oWnd)
			oTmsGetD := MSGetDados():New(aTmsPosObj[ 2, 1 ], aTmsPosObj[ 2, 2 ],aTmsPosObj[ 2, 3 ], aTmsPosObj[ 2, 4 ], nTmsOpcx,'TMSA170LinOk','TmsA170TOk',"+DUK_SEQUEN",lNaoExcluir,  ,  ,  ,nMaximoLinhas,  ,  ,  ,  ,  )
		EndIf
	
		ACTIVATE MSDIALOG oTmsDlgEsp ON INIT EnchoiceBar(oTmsDlgEsp,{||Iif( Iif(lGet, oTmsGetD:TudoOk(), .T.) .and. Obrigatorio( aGets, aTela ), (nOpca := 1,oTmsDlgEsp:End()), (nOpca :=0, .F.))},{||nOpca:=0,oTmsDlgEsp:End()},, aTmsButtons )
	Else
		If EnchAuto(cTmsAlias,aAutoCab,,nTmsOpcx,aTMSVisual,{|| Obrigatorio(aGets,aTela)})
			nOpca := 1
		EndIf
	EndIf
	
	If nTmsOpcx != 2 .And. nOpcA == 1
		
		TMSA170Grv( M->DTP_FILORI, M->DTP_LOTNFC, nTmsOpcx )
		
		If __lSX8
			ConfirmSX8()
		EndIf
	ElseIf __lSX8
		RollBackSX8()
	EndIf
	
	If !Empty( cCadOld )
		cCadastro := cCadOld
	EndIf
	
	If	!Empty( aTelOld )
		aTela		:= aClone( aTelOld )
		aGets		:= aClone( aGetOld )
	EndIf
	
	If	!Empty( aHeaOld )
		aHeader	:= aClone( aHeaOld )
		aCols	:= aClone( aColOld )
		N       := NOld
	EndIf
	
	//-- Limpa marcas dos agendamentos
	If !IsInCallStack("TMSAF76")
		TMSALimAge(StrZero(ThreadId(),20))
	EndIf
EndIf

If lSetKey
	//-- Inicializa Teclas de Atalhos
	TmsKeyOn(aSetKey)
EndIf

Return nOpca
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TmsA170Vld� Autor � Robson Alves          � Data �05.02.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Valida a quantidade de notas digitadas(DTP_QTDDIG).        ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TmsA170Vld()                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������� */
Function TmsA170Vld()

Local cCampo    	:= ReadVar()
Local lRet      	:= .T.
Local nPDatIni  	:= GDFieldPos( 'DUK_DATINI' )
Local nPDatFim  	:= GDFieldPos( 'DUK_DATFIM' )
Local lTmsCFec  	:= TmsCFec()
Local aAreaDTX  	:= {}
Local aAreaDTQ  	:= {}
Local aArea     	:= GetArea()
Local cAliasQry 	:= ""
Local cQuery    	:= ""
Local cProGen   	:= Padr(SuperGetMV('MV_PROGEN',,''),Len(SB1->B1_COD))
Local lTMSCTe   	:= SuperGetMv( "MV_TMSCTE", .F., .F. )
Local cCliLojRem	:= ""

If (cCampo == "M->DTP_QTDDIG") .And. (M->DTP_QTDDIG > M->DTP_QTDLOT)
	Help(" ", 1, "TmsA17002") // "A quantidade de notas digitadas nao pode ser maior que a quantidade de notas por lote."
	lRet := .F.
	
ElseIf (cCampo == "M->DTP_QTDLOT") .And. !Empty(M->DTP_QTDDIG) .And. (M->DTP_QTDLOT < M->DTP_QTDDIG)
	lRet := .F.
	
ElseIf  (cCampo == "M->DUK_DATINI") .And. !Empty(aCols[n][nPDatFim]) .And. (M->DUK_DATINI > aCols[n][nPDatFim]) .Or.;
	(cCampo == "M->DUK_DATFIM") .And. (M->DUK_DATFIM < aCols[n][nPDatIni])
	lRet := .F.
	
ElseIf cCampo $ "M->DTP_NUMSOL"
	DT5->(dbSetOrder(1))
	If DT5->(MsSeek(xFilial("DT5") + M->DTP_FILORI + M->DTP_NUMSOL)) 
		If !lTmsCFec
			//��������������������������������������������������������������������������Ŀ
			//� Nao permite Solicitacao de Coleta com Status diferente de 'Encerrada'    �
			//����������������������������������������������������������������������������
			If DT5->DT5_STATUS <> StrZero(4, Len(DT5->DT5_STATUS)) // Encerrada
		 		Help("",1,"TMSA05045") // So' serao aceitas Solicitacoes de Coleta Encerradas !!!
				lRet := .F.
			EndIf         
		Else
			If DT5->DT5_STATUS == StrZero(1, Len(DT5->DT5_STATUS)) .Or. ; // Em Aberto
			   DT5->DT5_STATUS == StrZero(5, Len(DT5->DT5_STATUS)) .Or. ; // Documento Informado
			   DT5->DT5_STATUS == StrZero(6, Len(DT5->DT5_STATUS)) .Or. ; // Bloqueada
			   DT5->DT5_STATUS == StrZero(9, Len(DT5->DT5_STATUS))        // Cancelada
		 		Help("",1,"TMSA05060") // So' serao aceitas Solicitacoes de Coleta com status diferente de 'Em Aberto', 'Documento informado' e 'Cancelado' !!!
				lRet := .F.
			EndIf
		EndIf
	EndIf	  
ElseIf cCampo $ "M->DTP_VIAGEM"    
	
	If !IsInCallStack("TMSF79LOT")
		lRet := TMSChkViag(M->DTP_FILORI,M->DTP_VIAGEM,.T.,.F.,.T.,,,,,.T.,.T.)   
	Else
		lRet := TMSChkViag(M->DTP_FILORI,M->DTP_VIAGEM,.T.,.F.,.F.,,,,,.T.,.T.)
	EndIf
	
	If lRet .And. !IsInCallStack("TMSA310GRV") .And. !IsInCallStack("TmsA144Grv")  //-- Fechamento De Viagem e ou Altera��o da  Viagem
		aAreaDTQ := DTQ->(GetArea()) 
		DTQ->(dbSetOrder(2))
		If ( (DTQ->(MsSeek(xFilial("DTP")+M->DTP_FILORI+M->DTP_VIAGEM))) .And. DTQ->DTQ_SERTMS=='1' ) 
		
		   	Help("",1,"TMSA17014") //-- N�o Permitido Gerar Lote De Coleta
		 
			lRet := .F.
		EndIf
		RestArea(aAreaDTQ)
	EndIf
	
	If lRet 
		If !(AllTrim(M->DTP_STATUS) $ "1,2")
			Help("",1,"TMSA17006") // Permitido alterar a Viagem somente de Lote com Status em aberto ou digitado!
			lRet := .F.
		Else
			aAreaDTQ := DTQ->(GetArea()) 
			DTQ->(dbSetOrder(2)) //DTQ_FILIAL+DTQ_FILORI+DTQ_VIAGEM+DTQ_ROTA
			If DTQ->(MsSeek(xFilial("DTQ")+M->DTP_FILORI+M->DTP_VIAGEM)) .AND. DTQ->DTQ_STATUS == "2" //2=Em Transito
				If ExistFunc("TMSLoteCli") 
					lRet := TMSLoteCli(M->DTP_FILORI, M->DTP_LOTNFC, @cCliLojRem)
					If lRet .AND. ExistFunc("TMSLoteOpe")
						lRet := TMSLoteOpe(M->DTP_FILORI, M->DTP_VIAGEM, cCliLojRem)
					EndIf
				EndIf

				If lRet .And. ExistFunc("Tm350Apoio")
					lRet:= Tm350Apoio(DTQ->DTQ_FILORI, DTQ->DTQ_VIAGEM)
				EndIf
			EndIf 	
			RestArea(aAreaDTQ)
		EndIf
	EndIf
	
	If lRet
		If IsInCallStack('A540RetMER') .And. ( Type('M->DUU_VIAGEM') <> 'U' .And. Type('M->DUU_FILORI') <> 'U' )
			If M->DTP_VIAGEM <> M->DUU_VIAGEM
				lRet	:= .F. 
				Help("",1,"TMSA17022") //-- N�o � permitido informar um n�mero de viagem diferente do informado no registro de pend�ncias.                                        
			EndIf
		EndIf
	EndIf

ElseIf cCampo $ "M->DTP_LOTNFC" .And. TmsExp() .And. Substr(FunName(),1,7)=="TMSA144"  
	If	!Empty(M->DTP_QTDDIG)
		Help("",1,"TMSA17004") // Existem Notas Fiscais utilizando este ,Lote.
		lRet := .F.
	EndIf	

ElseIf cCampo $ "M->DTP_NUMCOT"
	If M->DTP_STATUS <> StrZero(1,Len(DTP->DTP_STATUS)) //-- Em Aberto
		Help("",1,"TMSA17009") // Somente ser� permitido selecionar cota��o de frete para lote em aberto.
		lRet := .F.
   Else
   	If !Vazio() .And. ( lRet := ExistCpo('DT4',M->DTP_FILORI+M->DTP_NUMCOT,1) )
			DT4->(dbSetOrder(1))
			If DT4->(MsSeek(xFilial("DT4")+M->DTP_FILORI+M->DTP_NUMCOT))
				If DT4->DT4_STATUS <> StrZero(4,Len(DT4->DT4_STATUS)) .And. DT4->DT4_STATUS <> StrZero(9,Len(DT4->DT4_STATUS))
					If Posicione("DC5",1,xFilial("DC5")+DT4->DT4_SERVIC,"DC5_TIPRAT") == StrZero(0,Len(DC5->DC5_TIPRAT)) .Or. ; //-- Nao Tem
						DC5->DC5_TIPRAT == StrZero(4,Len(DC5->DC5_TIPRAT)) // -- Qtde Doctos
						Help("",1,"TMSA17007") // Escolha uma cota��o que possua servi�o de transporte configurado para rateio de frete (DC5_TIPRAT) e que n�o utilize rateio por documento.
						lRet := .F.
					EndIf
				Else
					Help("",1,"TMSA17008") // Nao ser� permitido selecionar cota��o de frete encerrada ou cancelada.
					lRet := .F.
				EndIf
			EndIf
			//-- Verifica se foi informado o produto diferente do generico na cotacao
			If lRet
				cAliasQry := GetNextAlias()
				cQuery := " SELECT COUNT(DVF_CODPRO) CNT "
				cQuery += " 	FROM  " + RetSqlName("DVF")
				cQuery += " 	WHERE DVF_FILIAL = '" + xFilial("DVF") + "' "
				cQuery += " 		AND DVF_FILORI = '" + M->DTP_FILORI + "' "
				cQuery += " 		AND DVF_NUMCOT = '" + M->DTP_NUMCOT + "' "
				cQuery += " 		AND DVF_CODPRO <> '" + cProGen + "' "
				cQuery += " 		AND D_E_L_E_T_ = ' ' "
				cQuery := ChangeQuery(cQuery)
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQry)
				If (cAliasQry)->CNT > 0
					Help("",1,"TMSA17010") // Somente ser� permitido selecionar cota��o de frete com produto generico informado.
					lRet := .F.
				EndIf
				(cAliasQry)->(DbCloseArea())
				RestArea( aArea )
			EndIf
		EndIf
	EndIf
ElseIf cCampo $ 'M->DTP_TIPLOT' .And. M->DTP_TIPLOT == '3'
	If M->DTP_TIPLOT == StrZero(3,Len(DTP->DTP_TIPLOT)) .And. !lTMSCTe
		Help(" ", 1, "TMSA17013") // "Lote Eletr�nico aceito apenas quando ", "o par�metro MV_TMSCTE estiver ativo"}
		lRet := .F.
	EndIf
ElseIf cCampo $ 'M->DTP_TIPLOT' .And. M->DTP_TIPLOT == '4' 
	If M->DTP_TIPLOT == StrZero(4,Len(DTP->DTP_TIPLOT)) .And. M->DTP_QTDLOT < 5 .And. !IsInCallStack("TMSA500")
		Help(" " ,1, "TMSA17016") // "Quando o Tipo do Lote for CTe �nico, a quantidade do lote de ser igual ou maior que 5!!"}
		lRet := .F.
	EndIf
	
ElseIf cCampo $ 'M->DTP_TIPLOT' .And. M->DTP_TIPLOT == '5'
	If !IsInCallStack("TMSA310GRV") .And. (IsInCallStack("TMSA144GRV") .And. M->DTQ_SERADI != "1")	//-- Fechamento De Viagem ou grava��o de viagem de entrega com servi�o adicional de coleta
		Help(" ", 1, "TMSA17019") // "Fun��o dispon�nel somente para custeio de coletas nas rotinas autom�ticas","Selecione um tipo v�lido."
		lRet := .F.
	EndIf
ElseIf cCampo $ 'M->DTP_QTDLOT' 
	If M->DTP_TIPLOT == StrZero(4,Len(DTP->DTP_TIPLOT)) .And. M->DTP_QTDLOT < 5
		Help(" " ,1, "TMSA17016") // "Quando o Tipo do Lote for CTe �nico, a quantidade do lote de ser igual ou maior que 5!!"}
		lRet := .F.
	EndIf

EndIf

Return( lRet )
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA170Lin� Autor � Antonio C Ferreira    � Data �22.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validacoes da linha da GetDados                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA170Lin()                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA170LinOk()

Local lRet     := .T.
Local nPDatIni := GDFieldPos( 'DUK_DATINI' )
Local nPHorIni := GDFieldPos( 'DUK_HORINI' )
Local nPDatFim := GDFieldPos( 'DUK_DATFIM' )
Local nPHorFim := GDFieldPos( 'DUK_HORFIM' )

//�����������������������������������������������������������������������Ŀ
//� Valida a data e hora digitada.                                        �
//�������������������������������������������������������������������������
If !GDdeleted(n) .And. (lRet:=MaCheckCols(aHeader,aCols,n))
	lRet := ValDatHor( aCols[n][nPDatFim], aCols[n][nPHorFim], aCols[n][nPDatIni], aCols[n][nPHorIni] )
EndIf

Return(lRet)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TmsA170TOk� Autor � Antonio C Ferreira    � Data �25.02.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Tudo Ok da GetDados                                        ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TmsA170TOk()

Local na
Local lRet   := .T.
Local aArea  := GetArea()

//-- Analisa se os campos obrigatorios da GetDados foram informados.
For nA := 1 to Len(aCols)
	If !( lRet := oTmsGetD:ChkObrigat( nA ) )
		Exit
	Endif
Next

//-- Analisa o linha ok.
If lRet .And. (Len(aCols) > 0)
	lRet := TMSA170LinOk()
EndIf
//-- Analisa se todas os itens da GetDados estao deletados.
If lRet .And. Ascan( aCols, { |x| x[ Len( x ) ] == .F. } ) == 0
	Help( ' ', 1, 'OBRIGAT2') //"Um ou alguns campos obrigatorios nao foram preenchidos no Browse"
	lRet := .F.
EndIf

If (AllTrim(M->DTP_STATUS) == "2") // Digitado.
	//�����������������������������������������������������������������������Ŀ
	//� Posiciona o DTP(Lote de Entrada de Notas Fiscais) e verifica se a     �
	//�quantidade de notas foi alterado para maior, entao muda-se o status do �
	//�lote para 1 = Em aberto.                                               �
	//�������������������������������������������������������������������������
	dbSelectArea("DTP")
	dbSetOrder(1)
	If MsSeek(xFilial("DTP") + M->DTP_LOTNFC) .And. M->DTP_QTDLOT > DTP->DTP_QTDLOT
		M->DTP_STATUS := "1"	
	EndIf	
EndIf

RestArea(aArea)

Return( lRet )
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA170Grv� Autor � Antonio C Ferreira   � Data �22.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gravar dados                                               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/

Static Function TMSA170Grv( cFilOri, cLote, nTmsOpcx )

Local nCntFor	:= 0
Local nCntFo1	:= 0     
Local cQuery := ""
Local cAliasNew
				
DbSelectArea("DTP")

If	nTmsOpcx == 5  // Exclusao
	DTC->(DbSetOrder(1))
	If !DTC->(MsSeek(xFilial("DTC")+cFilOri+cLote))
		Begin Transaction
		RecLock('DTP', .F., .T.)
		DbDelete()
		MsUnLock()
		If (Len(aCols) > 0)
			DUK->( DbSetOrder( 1 ) )
			While DUK->( DbSeek( xFilial('DUK') + cFilOri + cLote, .F. ) )
				//-- Exclui Complemento de Regioes .
				DUK->( RecLock('DUK', .F., .T.) )
				DUK->( DbDelete() )
				DUK->( MsUnLock() )
			EndDo
		EndIf
		//-- Atualiza Status da Cotacao de Frete
		//-- Verifica se o counteudo do campo cotacao foi zerado.
		If !Empty(DTP->DTP_NUMCOT)
			DbSelectArea("DT4")
			DbSetOrder(1)
			If MsSeek(xFilial("DT4")+DTP->DTP_FILORI+DTP->DTP_NUMCOT)
				RecLock("DT4",.F.)
				DT4->DT4_STATUS := '1'  //-- Cotacao de Frete Pendente
				MsUnLock()
			EndIf             
		EndIf
		End Transaction
	Else
		Help(" ",1,"TMSA17004") //"Existem Notas Fiscais utilizando este Lote."
	EndIf
EndIf

If	(nTmsOpcx == 3) /* Inclusao */ .or. (nTmsOpcx == 4) /* Alteracao */

	Begin Transaction
		//-- Atualiza Status da Cotacao de Frete
		If	!Empty(M->DTP_NUMCOT)
			DbSelectArea("DT4")
			DbSetOrder(1)
			If MsSeek(xFilial("DT4")+M->DTP_FILORI+M->DTP_NUMCOT)
				RecLock("DT4",.F.)
				DT4->DT4_STATUS := '4'  //-- Cotacao de Frete Encerrada
				MsUnLock()
			EndIf             
		EndIf
		//-- Verifica se o counteudo do campo cotacao foi modificado
		If nTmsOpcx == 4 
			If DTP->DTP_NUMCOT <> M->DTP_NUMCOT
				DbSelectArea("DT4")
				DbSetOrder(1)
				If !Empty(DTP->DTP_NUMCOT)
					If MsSeek(xFilial("DT4")+DTP->DTP_FILORI+DTP->DTP_NUMCOT)
						RecLock("DT4",.F.)
						DT4->DT4_STATUS := '1'  //-- Cotacao de Frete Pendente
						MsUnLock()
					EndIf
				EndIf
			EndIf
		EndIf

		RecLock( "DTP", (nTmsOpcx==3) )
		
		Aeval( dbStruct(), { |aFieldName, nI | FieldPut( nI, If('FILIAL' $ aFieldName[1],;
		xFilial( "DTP" ), M->&(aFieldName[1]) ) ) } )
		
		If  (nTmsOpcx == 4) /* Alteracao */
			DbSelectArea('DTC')
			DbSetOrder(1)
			If MsSeek(xFilial("DTc")+DTP->DTP_FILORI+DTP->DTP_LOTNFC)
				cAliasNew := CriaTrab(Nil,.F.)
				cQuery := " SELECT * "
				cQuery += "   FROM " + RetSQLName("DTC") + " DTC "
				cQuery += "   WHERE DTC.DTC_FILIAL = '" + xFilial("DTC") + "'"
				cQuery += "     AND DTC.DTC_FILORI = '" + M->DTP_FILORI + "'"
				cQuery += "     AND DTC.DTC_LOTNFC = '" + M->DTP_LOTNFC + "'"
				cQuery += "     AND DTC.DTC_DOC    = ' '"	
				cQuery += "     AND DTC.D_E_L_E_T_ = ' ' "
				cQuery := ChangeQuery( cQuery )
				dbUseArea( .T., "TOPCONN", TcGenQry( , , cQuery ), cAliasNew, .F., .T. )
				If (cAliasNew)->(Eof())
					DTP->DTP_STATUS := "3"
				Else
					DTP->DTP_STATUS := DTP->( If((DTP_QTDLOT>0) .And. (DTP_QTDLOT==DTP_QTDDIG), "2", "1") )
				EndIf	
				(cAliasNew)->(DbCloseArea())
			Else
				DTP->DTP_STATUS := DTP->( If((DTP_QTDLOT>0) .And. (DTP_QTDLOT==DTP_QTDDIG), "2", "1") )
			EndIf
		EndIf

		//-- Ponto de entrada para manipular as informa��es do lote de entrada, tabela DTP
		If lTM170GRV
		     ExecBlock("TM170GRV",.F.,.F.,{Iif(IsInCallStack("TMSA200A") .Or. IsInCallStack("T200AAGLUT"),"TMSA200A",funname()),DTP->DTP_FILORI,DTP->DTP_LOTNFC})
		EndIf

		DTP->( MsUnLock() )

		For nCntFor := 1 To Len( aCols )
			If	!GDDeleted( nCntFor )
				
				If	DUK->( MsSeek( xFilial('DUK') + cFilOri + cLote + GDFieldGet( 'DUK_SEQUEN', nCntFor ), .F. ) )
					RecLock('DUK', .F.)
				Else
					RecLock('DUK', .T.)
					
					DUK->DUK_FILIAL	 	:= xFilial('DUK')
					DUK->DUK_FILORI    := cFilOri
					DUK->DUK_LOTNFC		:= cLote
				EndIf
				
				For nCntFo1 := 1 To Len(aHeader)
					If	aHeader[nCntFo1,10] != 'V'
						DUK->( FieldPut(FieldPos(aHeader[nCntFo1,2]), aCols[nCntFor,nCntFo1]) )
					EndIf
				Next
				
				DUK->( MsUnLock() )
				
			Else
				If	DUK->( MsSeek( xFilial('DUK') + cFilOri + cLote + GDFieldGet( 'DUK_SEQUEN', nCntFor ), .F. ) )
					DUK->( RecLock('DUK', .F., .T.) )
					DUK->( DbDelete() )
					DUK->( MsUnLock() )
				EndIf
			EndIf
		Next
	End Transaction
EndIf

Return NIL
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA170Leg� Autor � Alex Egydio           � Data �13.02.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Exibe legenda do status do lote de entrada de NF do cliente���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA170Leg()                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
����������������������������������������������������������������������������� */
Function TMSA170Leg()
Local aLegenda := {}
Local aRet

aLegenda := {	{ 'BR_AMARELO'  , STR0009 },;  //'Em Aberto'
				{ 'BR_VERDE'	  , STR0010 },;  //'Digitado'
				{ 'BR_AZUL'	  , STR0011 },;  //'Calculado'
				{ 'BR_VERMELHO' , STR0012 },;  //'Bloqueado'
				{ 'BR_LARANJA'  , STR0013 }}   // 'Cancelado'

If ExistBlock("TMA170LEG")
	aRet := ExecBlock("TMA170LEG",.F.,.F., {aLegenda})
	If ValType(aRet) == "A" .And. !Empty(aRet)
		aLegenda := aClone(aRet)
	EndIf
EndIf

BrwLegenda( STR0014, STR0008 , aLegenda )  //"Status do Lote" ### "Status"

Return NIL

/*/
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Marco Bianchi         � Data �01/09/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �		1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function MenuDef()
     
Private aRotina	:= {	{ STR0002 ,'AxPesqui'  ,0,1,0,.F.},;	//'Pesquisar'
								{ STR0003 ,'TMSA170Mnt',0,2,0,NIL},; 	//'Visualizar'
								{ STR0004 ,'TMSA170Mnt',0,3,0,NIL},; 	//'Incluir'
								{ STR0005 ,'TMSA170Mnt',0,4,0,NIL},; 	//'Alterar'
								{ STR0006 ,'TMSA170Mnt',0,5,0,NIL},; 	//'Excluir'
								{ STR0007 ,'TmsA170Leg',0,6,0,.F.} } 	//'Legenda'


If ExistBlock("TMA170MNU")
	ExecBlock("TMA170MNU",.F.,.F.)
EndIf

Return(aRotina)
/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA170Whe� Autor � 	A.S.M				� Data �22.10.2014���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validacoes antes de editar o campo                         ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA170Whe()                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Logico                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � TMSA170                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function TMSA170Whe(cCampo)
Local lRet       := .T.
Local aAreaDTQ	 := {}

Default cCampo   := ReadVar()

If cCampo == "M->DTP_RATEIO"
	If INCLUI
		lRet := .T.
	ElseIf M->DTP_QTDDIG > 0
		lRet := .F.
	ElseIf M->DTP_QTDDIG == 0
		lRet := .T.
	EndIf
ElseIf cCampo == "M->DTP_VIAGEM" .AND. ALTERA
	aAreaDTQ := DTQ->(GetArea()) 
	DTQ->(dbSetOrder(2)) //DTQ_FILIAL+DTQ_FILORI+DTQ_VIAGEM+DTQ_ROTA
	If DTQ->(MsSeek(xFilial("DTP")+DTP->DTP_FILORI+DTP->DTP_VIAGEM)) .AND. DTQ->DTQ_STATUS == "2"
		lRet := .F.
	EndIf
	RestArea(aAreaDTQ)
EndIf	
Return( lRet )
