#include 'protheus.ch'
#include 'fileio.ch'
#include 'tmsao46.ch'

//-----------------------------------------------------------------
/*/{Protheus.doc} TMSBCARotaInt()
Classe criada para comunica��o com a Rota Inteligente

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
CLASS TMSBCARotaInt

    DATA cURL           As Character
    DATA cEvent         As Character
    DATA cTypeEvent     As Character
    DATA client_secret  As Character
    DATA oRestClient    As Object
    DATA aAuthorization As Array
    DATA aHeader        As Array
    DATA oParams        As Object
    DATA oPlanning      As Object
    DATA IdTokenTrip    As Character
    DATA idtokenplan    As Character
    DATA lExibeErr      As Boolean 
    
    //-- ESTRUTURA INFORMA��ES TOKEN
    DATA refresh_token_expires_in   As Numeric
    DATA api_product_list_json      As Array
    DATA organization_name          As Character
    DATA developer_email            As Character
    DATA token_type                 As Character
    DATA issued_at                  As Character
    DATA client_id                  As Character
    DATA access_token               As Character
    DATA application_name           As Character
    DATA scope                      As Character
    DATA expires_in                 As Numeric
    DATA refresh_count              As Numeric
    DATA status                     As Character
    DATA date_token                 As Date
    DATA time_token                 As Character

    //-- Informa��es Viagem
    DATA cFilOri        As Character
    DATA cViagem        As Character
    DATA aClientes      As Array
    DATA aProdutos      As Array
    DATA aVeiculos      As Array
    DATA aLogistic      As Array
    DATA aDocs          As Array
    DATA aLegs          As Array
    DATA nValPDG        As Numeric

    //-- Informa��es Planning e Trip
    DATA callback               As Object
    DATA logisticConstraints    As Array
    DATA legislationProfile     As Array
    DATA depots                 As Array
    DATA operations             As Array
    DATA products               As Array
    DATA sites                  As Array
    DATA startDate              As Object
    DATA tripsProfile           As Object
    DATA vehicleTypes           As Array
    DATA vehicles               As Array

    //-- planning
    DATA planningstaus            As Character

    METHOD New()                    Constructor   
    METHOD SetHeader()
    METHOD SetParams()
    METHOD SetDadosVge()
    METHOD SetDadosCli()
    METHOD SetVehicles()
    METHOD SetLogistic()
    METHOD CleanDate()
    METHOD Post()
    METHOD AltSeqVge()
    METHOD AddDocs()
    METHOD AddCli()

    //-- M�todos Token
    METHOD GetAccessToken()
    METHOD PutInfoToken()
    METHOD ActiveToken()
    METHOD UpdateToken()
    METHOD GetClientID()
    METHOD GetClientSecret()
    METHOD Token()

    //-- M�todos Trip
    METHOD TripPost()    
    METHOD TripListEvents()
    METHOD TripSolution()
    METHOD TripGetJob()
    METHOD TripGetProblem()

    //-- M�todos Planning
    METHOD PlanningPost()
    METHOD PlaningSolution()
    METHOD PlanListEvents()
    METHOD PlanGetJob()
    METHOD PlanGetProblem()

    //-- M�todos Tolls
    METHOD calculateTolls()
    METHOD UpdatePedagio()
    METHOD GetEixos()

    //-- M�todo IsConnect
    METHOD IsConnect()

    //-- M�todos Get a partir da Filial de Origem e Viagem (Planning)
    METHOD GetTypeCallBack()
    METHOD GetCallBack()
    METHOD GetDepots()
    METHOD GetLegislationProfile()
    METHOD GetLogisticConstraints()
    METHOD GetOperations()
    METHOD GetOptimizationProfile()
    METHOD GetProducts()
    METHOD GetSites()
    METHOD GetStartDate()
    METHOD GetTripsProfile() 
    METHOD GetVehicleTypes()
    METHOD GetVgeVehicle()
    METHOD GetCordinate()
    METHOD GetCustomerTime()
    METHOD GetAvailablePeriods()
    METHOD GetLegs()

    //-- M�todos Get a partir da Filial de Origem e Viagem (Trip)
    METHOD GetPoints()
    METHOD GetProfileName()
    METHOD GetAvoidTypes()
    METHOD GetCalcMode()
    METHOD GetRestrictZones()
    METHOD GetSpeedPreferences()
    METHOD GetTypeSpeed()
    METHOD GetEspecifVehicle()
    METHOD GetTimeWindow()

    //-- Destr�i classe e objetos
    METHOD Destroy()

END CLASS

//-----------------------------------------------------------------
/*/{Protheus.doc} New()
M�todo construtor da classe

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD New( cURL , cEvent , cTypeEvent , cFilOri , cViagem , lShowErr ) CLASS TMSBCARotaInt

Default cURL        := "https://lbslocal-prod.apigee.net"
Default cEvent      := "/trip/v1/problems/"
Default cTypeEvent  := "GET"
Default cFilOri     := ""
Default cViagem     := ""
Default lShowErr    := .F. 

If lShowErr
    Pergunte("TMSAO46",.F.)
    If mv_par04 == 1 
        lShowErr    := .T. 
    ElseIf mv_par04 == 2 
        lShowErr    := .F. 
    EndIf
EndIf

::cURL	            := cURL
::cEvent            := cEvent
::cTypeEvent        := cTypeEvent
::client_id         := ::GetClientID()
::client_secret     := ::GetClientSecret()
::oRestClient       := FwRest():New(cURL)
::aAuthorization    := {}
::aHeader           := {} 
::oParams           := JsonObject():new()
::IdTokenTrip       := ""
::lExibeErr         := lShowErr

//-- INFORMA��ES TOKEN
::refresh_token_expires_in  := 0 
::api_product_list_json     := {}
::organization_name         := ""
::developer_email           := ""
::token_type                := ""
::issued_at                 := ""
::access_token              := ""
::application_name          := ""
::scope                     := ""
::expires_in                := 0
::refresh_count             := 0
::status                    := ""
::date_token                := cToD("")
::time_token                := ""

//-- Informa��es viagem
::cFilOri       := cFilOri
::cViagem       := cViagem
::aClientes     := {}
::aVeiculos     := {}
::aProdutos     := {} 
::aLogistic     := {} 
::aDocs         := {}
::aLegs         := {} 
::nValPDG       := 0 

::oRestClient:SetPath(cEvent)

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} SetHeader()
Seta cabe�alho 

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetHeader(aHeader) CLASS TMSBCARotaInt

Default aHeader     := {} 

fwFreeObj(::aHeader)

::aHeader   := aClone(aHeader)

FwFreeObj(aHeader)
Return

//--------------------------------------------------------------------
/*/{Protheus.doc} GetToken()
Get Token

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetAccessToken(cErro) CLASS TMSBCARotaInt
Local cToken        := ""
Local cJSON         := ""
Local lRet          := .T. 
Local aHeaderStr    := {}
Local cEvent        := "/oauth/client_credential/accesstoken?grant_type=client_credentials"
Local cTypeEvent    := "getAccessTokenClientCredential"
Local cResult       := ""
Local oFile         := Nil
Local cBuffer       := ""
Local dDataToken    := Nil
Local cTimeToken    := ""
Local lContinua     := .T. 

Default cErro       := ""

cToken  := ""
Aadd(aHeaderStr, "Content-Type: application/x-www-form-urlencoded" )

cJSON   := "client_id=" + RTrim(::client_id) 
cJSON   += "&"
cJSON   += "client_secret=" + RTrim(::client_secret)

::oRestClient:SetPath(cEvent)
::oRestClient:SetPostParams( cJSON )
lRet    := ::oRestClient:Post( aHeaderStr ) 

If lRet
    cResult     := ::oRestClient:GetResult()
    If ::PutInfoToken(cResult)
        cToken      :=  ::access_token
    EndIf
Else
    cErro   := AllTrim(::oRestClient:GetLastError())
    If ::lExibeErr        
        MsgStop(cErro)
    EndIf
EndIf

Return cToken

//-----------------------------------------------------------------
/*/{Protheus.doc} PutInfoToken()
Alimenta informa��es 

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD PutInfoToken(cResult) CLASS TMSBCARotaInt
Local oObj      := Nil
Local lRet      := .T. 

Default cResult     := ""

If FWJsonDeserialize(cResult,@oObj)
    lRet    := .T. 
Else
    lRet    := .F. 
EndIf

If lRet

    ::refresh_token_expires_in  := Val(oObj:refresh_token_expires_in) 
    ::api_product_list_json     := aClone(oObj:api_product_list_json)
    ::organization_name         := oObj:organization_name
    ::developer_email           := oObj:developeremail
    ::token_type                := oObj:token_type
    ::issued_at                 := oObj:issued_at
    ::access_token              := oObj:access_token
    ::application_name          := oObj:application_name
    ::scope                     := oObj:scope
    ::expires_in                := Val(oObj:expires_in)
    ::refresh_count             := Val(oObj:refresh_count)
    ::status                    := oObj:status
    ::date_token                := dDataBase
    ::time_token                := Time()

EndIf

Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} ActiveToken()
Verifica se existe token ativo

@author Caio Murakami
@since 18/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD ActiveToken() CLASS TMSBCARotaInt
Local cQuery        := ""
Local nTimeOut      := ""
Local dData         := cToD("")
Local nTimeToken    := ""
Local cToken        := ""
Local cAliasQry     := GetNextAlias()

cQuery  := " SELECT * "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL  = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )
TcSetField(cAliasQry,"DLV_DTTOKE","D",8,0)

While (cAliasQry)->( !Eof() )
    dData       := (cAliasQry)->DLV_DTTOKE
    nTimeToken  := HoraToInt( (cAliasQry)->DLV_HRTOKE )
    nTimeOut    := HoraToInt( (cAliasQry)->DLV_TIMETK )
    
    If dData == dDataBase
        If SubHoras(Time(), IntToHora(nTimeToken)  ) < nTimeOut
            cToken  := (cAliasQry)->DLV_TOKEN
        EndIf
    ElseIf DateDiffDay(dData , dDataBase ) == 1
        nTimeToken  := SubHoras("23:59",IntToHora(nTimeToken))
        nTimeToken  += HoraToInt(Time())

        If nTimeToken  < nTimeOut
            cToken  := (cAliasQry)->DLV_TOKEN
        EndIf
    EndIf

    (cAliasQry)->( dbSkip() )
EndDo

Return cToken

//-----------------------------------------------------------------
/*/{Protheus.doc} UpdateToken()
Update Token

@author Caio Murakami
@since 19/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD UpdateToken() CLASS TMSBCARotaInt
Local aArea     := GetArea()
Local lRet      := .F. 

DLV->(dbSetOrder(1))
If DLV->( dbSeek( xFilial("DLV") + ::client_id )) .And. !Empty(::access_token)
    RecLock("DLV",.F.)
    DLV->DLV_TOKEN	:= ::access_token
    DLV->DLV_DTTOKE	:= ::date_token 
    DLV->DLV_HRTOKE	:= ::time_token 
    DLV->(MsUnlock())
EndIf

RestArea(aArea)
Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} GetClientID()
captura ultimo client id

@author Caio Murakami
@since 19/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetClientID() CLASS TMSBCARotaInt
Local cRet      := ""
Local cQuery    := ""
Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()

cQuery  := " SELECT DLV_ID "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL   = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL     = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' " 

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    cRet     := (cAliasQry)->DLV_ID
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())

RestArea(aArea)
Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} GetClientSecret()
captura ultimo client id

@author Caio Murakami
@since 19/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetClientSecret() CLASS TMSBCARotaInt
Local cRet      := ""
Local cQuery    := ""
Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()

cQuery  := " SELECT DLV_SECRET "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL   = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL     = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' " 

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    cRet     := (cAliasQry)->DLV_SECRET
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())

RestArea(aArea)
Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} GetTypeCallBack()
GetTypeCallBack

@author Caio Murakami
@since 16/08/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetTypeCallBack() CLASS TMSBCARotaInt
Local cRet      := ""
Local cQuery    := ""
Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()

cQuery  := " SELECT DLV_TIPRET "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL   = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL     = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' " 

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    cRet     := (cAliasQry)->DLV_TIPRET
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())

RestArea(aArea)
Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} Token()
Token

@author Caio Murakami
@since 19/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD Token() CLASS TMSBCARotaInt
Local cToken		:= ::ActiveToken()

If Empty(cToken)
    cToken      := ::GetAccessToken() 
    If !Empty(cToken)
        ::access_token  := cToken
        ::UpdateToken()
    EndIf
Else
    ::access_token  := cToken
EndIf

Return cToken

//-----------------------------------------------------------------
/*/{Protheus.doc} SetDadosVge()
Seta dados referente a viagem

@author Caio Murakami
@since 06/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetDadosVge() CLASS TMSBCARotaInt
Local oDadosViag    := Nil 
Local aDocsDUD      := {} 
Local aVehicles     := {} 
Local aNf           := {}
Local aColetas      := {}

oDadosViag	:= TMSBCADadosTMS():New(::cFilOri,::cViagem,3,.T.)
aDocsDUD    := oDadosViag:GetDocs()
aVehicles   := oDadosViag:GetVehicles()
aNf         := oDadosViag:GetNf()
aColetas    := oDadosViag:GetColetas()

::SetDadosCli( aDocsDUD, aNf , , aColetas )
::SetVehicles()
::SetLogistic()

Return
//-----------------------------------------------------------------
/*/{Protheus.doc} SetLogistic()

@author Caio Murakami
@since 10/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetLogistic() CLASS TMSBCARotaInt
Local cAliasQry     := GetNextAlias()
Local cQuery        := ""
Local cAtivChg		:= SuperGetMV('MV_ATIVCHG',,'') //-- Atividade de Chegada de Viagem
Local cAtvSaiCli    := SuperGetMv('MV_ATVSAIC',,'') //-- Atividade de Sa�da do cliente
Local cAtvChgCli    := SuperGetMv('MV_ATVCHGC',,'') //-- Atividade de Chegada em Cliente
Local cAtivDca      := SuperGetMV('MV_ATIVDCA',,'') //-- Atividade de Descarregamento
Local cAtivSai		:= SuperGetMV('MV_ATIVSAI',,'') //-- Atividade de Sa�da de Viagem
Local nHora         := 0
Local nMinutos      := 0
Local nTotal        := 0

cQuery  := " SELECT  DISTINCT ( DTW_ATIVID ) , DC6_DURAC , DTW_CODCLI , DTW_LOJCLI "
cQuery  += " FROM " + RetSQLName("DTW") + " DTW "
cQuery  += " INNER JOIN " + RetSQLName("DC6") + " DC6 "
cQuery  += "    ON DC6_FILIAL   = '" + xFilial("DC6") + "' "
cQuery  += "    AND DC6_ATIVID  = DTW_ATIVID "
cQuery  += "    AND DC6.D_E_L_E_T_ = '' "
cQuery  += " WHERE DTW_FILIAL   = '" + xFilial("DTW") + "' "
cQuery  += " AND DTW_FILORI     = '" + ::cFilOri + "' "
cQuery  += " AND DTW_VIAGEM     = '" + ::cViagem + "' "
/*cQuery  += " AND DTW_DATREA     = '' "
cQuery  += " AND DTW_HORREA     = '' "
Qcuery  += " AND DTW_STATUS     = '1' "*/
cQuery  += " AND DTW_ATIVID     IN ('"+ cAtivSai + "', '" + cAtivChg + "' , '" + cAtvSaiCli + "' , '" + cAtvChgCli + "' , '" + cAtivDca + "'  ) "
cQuery  += " AND DTW.D_E_L_E_T_ = '' "
cQuery  += " ORDER BY 1 "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    
    nHora       := Val( SubStr( (cAliasQry)->DC6_DURAC , 1, At(":",(cAliasQry)->DC6_DURAC ) ) )
    nMinutos    := Val( SubStr( (cAliasQry)->DC6_DURAC , At(":",(cAliasQry)->DC6_DURAC ) + 1 ) )
    nTotal      := ( nHora * 60 ) + nMinutos

    Aadd( ::aLogistic , {} ) 
    Aadd( ::aLogistic[Len(::aLogistic)] , {"DTW_ATIVID" , (cAliasQry)->DTW_ATIVID })
    Aadd( ::aLogistic[Len(::aLogistic)] , {"DTW_CODCLI" , (cAliasQry)->DTW_CODCLI })
    Aadd( ::aLogistic[Len(::aLogistic)] , {"DTW_LOJCLI" , (cAliasQry)->DTW_LOJCLI })
    Aadd( ::aLogistic[Len(::aLogistic)] , {"DC6_DURAC"  , nTotal })

    (cAliasQry)->(dbSkip())
EndDo

(cAliasQry)->( dbCloseArea() )

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} SetDadosCli()

@author Caio Murakami
@since 07/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetDadosCli( aDocsDUD , aNf , aSitesCli , aColetas ) CLASS TMSBCARotaInt
Local nCount        := 1
Local nPos          := 0 
Local nPosCol       := 0
Local cFilDoc       := ""
Local cDoc          := ""
Local cSerie        := ""
Local cCodCli       := ""
Local cLojCli       := ""
Local cCodPro       := ""
Local nFilDoc       := 1
Local nDoc          := 1
Local nSerie        := 1
Local nColFilDoc    := 1
Local nColDoc       := 1
Local lIncluiSA1    := .T.
Local lIncluiSB1    := .T. 
Local cAlias        := "SA1"
Local aAux          := {}
Local cDocTMS       := ""
Local nPriori       := 0 
Local cSeqEnd       := ""

Default aDocsDUD    := {} 
Default aNf         := {}
Default aSitesCli   := {} 
Default aColetas    := {}

If Len(aSitesCli) == 0 
    
    ::aDocs     := {}
    ::aClientes := {}
    ::aProdutos := {}

    //-- Nota Fiscal 
    If Len(aNF) > 0 

        //-- FILDOC
        GetInfoArr( aNF[1] , "DTC_FILDOC" , 1 , , @nFilDoc )
        //-- DOC
        GetInfoArr( aNF[1] , "DTC_DOC" , 1 , , @nDoc )
        //-- SERIE
        GetInfoArr( aNF[1] , "DTC_SERIE" , 1 , , @nSerie )

    EndIf

    //-- Sol. Coleta
    If Len(aColetas) > 0 
        GetInfoArr( aColetas[1], "DUM_FILDOC",1,,@nColFilDoc)
        GetInfoArr( aColetas[1], "DUM_NUMSOL",1,,@nColDoc)       
    EndIf

    For nCount := 1 To Len(aDocsDUD)

        cFilDoc     := GetInfoArr(aDocsDUD[nCount] , "DT6_FILDOC")
        cDoc        := GetInfoArr(aDocsDUD[nCount] , "DT6_DOC") 
        cSerie      := GetInfoArr(aDocsDUD[nCount] , "DT6_SERIE") 
        cDocTMS     := GetInfoArr(aDocsDUD[nCount] , "DT6_DOCTMS" )
        cCodCli     := GetInfoArr(aDocsDUD[nCount] , "CLIDES", 3 ) 
        cLojCli     := GetInfoArr(aDocsDUD[nCount] , "LOJDES", 3 )
        cSeqEnd     := GetInfoArr(aDocsDUD[nCount] , "SQEDES",3 )
        cCodPro     := ""
        cAlias      := Iif( GetInfoArr(aDocsDUD[nCount],"CLIDES",3,1)=="DUL_CODRED","DUL","SA1")
        nPos        := aScan( aNf , {|x| x[nFilDoc,2] + x[nDoc,2] + x[nSerie,2]  == cFilDoc + cDoc + cSerie })
        nPriori     := 0 
        
        If cSerie <> "COL"
            If nPos > 0 
                cCodPro     := GetInfoArr(aNf[nPos] , "DTC_CODPRO" )
            EndIf
        Else
            nPosCol     := aScan( aColetas, {|x| x[nColFilDoc,2] + x[nColDoc,2] == cFilDoc + cDoc })
            If nPosCol  > 0 
                cCodPro     := GetInfoArr(aColetas[nPosCol] , "DUM_CODPRO" )
            EndIf
        EndIf

        If cDocTMS == "6" //-- Devolu��o

            //-- Remetente
            nPriori     := 1        
            ::AddDocs( cFilDoc , cDoc , cSerie , GetInfoArr(aDocsDUD[nCount] , "DT6_PESO")  , GetInfoArr(aDocsDUD[nCount] , "DT6_METRO3") , GetInfoArr(aDocsDUD[nCount] , "DT6_CLIREM" )  , GetInfoArr(aDocsDUD[nCount] , "DT6_LOJREM" ) , cCodPro , nPriori  )
            ::AddCli( GetInfoArr(aDocsDUD[nCount] , "DT6_CLIREM" )  , GetInfoArr(aDocsDUD[nCount] , "DT6_LOJREM" ) , cSerie , cCodPro , cAlias ,cSeqEnd ) 
            //-- Destinat�rio
            nPriori     := 0
            ::AddDocs( cFilDoc , cDoc , cSerie , GetInfoArr(aDocsDUD[nCount] , "DT6_PESO")  , GetInfoArr(aDocsDUD[nCount] , "DT6_METRO3") , cCodCli , cLojCli , cCodPro ,  nPriori )
            ::AddCli( cCodCli , cLojCli , cSerie , cCodPro , cAlias , cSeqEnd) 
        Else        
            nPriori     := 1    
            ::AddDocs( cFilDoc , cDoc , cSerie , GetInfoArr(aDocsDUD[nCount] , "DT6_PESO")  , GetInfoArr(aDocsDUD[nCount] , "DT6_METRO3") , cCodCli , cLojCli , cCodPro ,  nPriori )
            ::AddCli( cCodCli , cLojCli , cSerie , cCodPro , cAlias , cSeqEnd ) 
        EndIf
       
        //-- PRODUTOS
        If Len(::aProdutos) > 0        
            nPos := aScan( ::aProdutos , {|x| x[1,2] == cCodPro })  
            If nPos > 0 
                lIncluiSB1  := .F. 
            Else
                lIncluiSB1  := .T. 
            EndIf
        EndIf

        If lIncluiSB1
            Aadd( ::aProdutos , {} )
            Aadd( ::aProdutos[Len(::aProdutos)] , { "B1_COD"  , cCodPro })  
            Aadd( ::aProdutos[Len(::aProdutos)] , { "B1_TIPO" , Posicione("SB1",1,xFilial("SB1") + cCodPro , "B1_TIPO" ) })  
        EndIf

    Next nCount
Else
    aAux        := aClone( ::aClientes )
    
    ::aClientes := Nil
    ::aClientes := {}

    For nCount := 1 To Len(aSitesCli)
        nPos := aScan( aAux , {|x| x[1,2] + x[2,2]== aSitesCli[nCount] }) 
        
        If nPos > 0 
            ::AddCli(  GetInfoArr(aAux[nPos] , "A1_COD") , GetInfoArr(aAux[nPos] , "A1_COD")  , GetInfoArr(aAux[nPos] , "DT6_SERIE") , GetInfoArr(aAux[nPos] , "B1_COD")  , GetInfoArr(aAux[nPos] , "ALIAS") )     
        EndIf

    Next nCount

EndIf

Return .T. 

//-----------------------------------------------------------------
/*/{Protheus.doc} AddCli(  )

@author Caio Murakami
@since 15/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD AddCli( cCodCli , cLojCli , cSerie , cCodPro , cAlias, cSeqEnd ) ClASS TMSBCARotaInt
Local lInclui   := .T. 

Default cCodCli     := ""
Default cLojCli     := ""
Default cSerie      := ""
Default cCodPro     := ""
Default cAlias      := ""
Default cSeqEnd     := ""

//-- CLIENTES
If Len(::aClientes) > 0       
    nPos := aScan( ::aClientes , {|x| x[1,2] + x[2,2] + x[3,2] + x[4,2] == cCodCli + cLojCli + cSerie  + cCodPro }) 
    If nPos > 0 
        lInclui := .F. 
    EndIf       
EndIf

If lInclui
    Aadd( ::aClientes , {} )
    Aadd( ::aClientes[Len(::aClientes)] , { "A1_COD"        , cCodCli } )
    Aadd( ::aClientes[Len(::aClientes)] , { "A1_LOJA"       , cLojCli } ) 
    Aadd( ::aClientes[Len(::aClientes)] , { "DT6_SERIE"     , cSerie })  
    Aadd( ::aClientes[Len(::aClientes)] , { "B1_COD"        , cCodPro })     
    Aadd( ::aClientes[Len(::aClientes)] , { "ALIAS"         , cAlias })  
    Aadd( ::aClientes[Len(::aClientes)] , { "SQEEND"        , cSeqEnd })
EndIf

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} AddDocs(  )

@author Caio Murakami
@since 15/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD AddDocs( cFilDoc , cDoc , cSerie , nPeso , nMetro3 , cCodCli , cLojCli , cCodPro ,  nPrior  ) CLASS TMSBCARotaInt

Default cFilDoc     := ""
Default cDoc        := ""
Default cSerie      := ""
Default nPeso       := 0 
Default nMetro3     := 0
Default cCodCli     := 0
Default cLojCli     := 0
Default cCodPro     := ""
Default nPrior      := 0 

Aadd( ::aDocs , {} )
Aadd( ::aDocs[Len(::aDocs)] , { "DT6_FILDOC", cFilDoc } )
Aadd( ::aDocs[Len(::aDocs)] , { "DT6_DOC"   , cDoc } )
Aadd( ::aDocs[Len(::aDocs)] , { "DT6_SERIE" , cSerie } )
Aadd( ::aDocs[Len(::aDocs)] , { "DT6_PESO"  , nPeso } )
Aadd( ::aDocs[Len(::aDocs)] , { "DT6_METRO3", nMetro3 } )
Aadd( ::aDocs[Len(::aDocs)] , { "A1_COD"    , cCodCli } )
Aadd( ::aDocs[Len(::aDocs)] , { "A1_LOJA"   , cLojCli } )

If !Empty(cCodPro)
    Aadd( ::aDocs[Len(::aDocs)] , { "B1_COD"  , cCodPro  })
EndIf

Aadd( ::aDocs[Len(::aDocs)] , { "PRIORIDADE"   , nPrior } )

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} SetVehicles()

@author Caio Murakami
@since 07/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetVehicles() CLASS TMSBCARotaInt
Local cQuery        := ""
Local cAliasQry     := GetNextAlias()
Local cCodVei       := ""
Local nVolMax       := 0 
Local nCapacM       := 0
Local nVolMax       := 0
Local nComExt       := 0 
Local nQtdEix       := 0 
Local nLarExt       := 0 
Local nAltExt       := 0 
Local nQtdEix       := 0
Local nQtdVolEix    := 0 
Local dDtIni        := CToD("")
Local dDtRea        := CToD("")
Local cHorIni       := ""
Local cHorRea       := ""

cQuery  := " SELECT DISTINCT  DTR_ITEM , DTR_QTDEIX, DTR_QTEIXV, DTR_DATINI, DTR_HORINI, DTR_DATFIM, DTR_HORFIM , " 
cQuery  += " DA3VEIC.DA3_COD  'VEICCOD' , DA3VEIC.DA3_CAPACM 'VEICCAPACM' , DA3VEIC.DA3_VOLMAX 'VEICVOLMAX', DA3VEIC.DA3_COMEXT 'VEICCOMEXT' ,"
cQuery  += " DA3VEIC.DA3_QTDEIX 'VEICQTDEIX' , DA3VEIC.DA3_LAREXT 'VEICLAREXT', DA3VEIC.DA3_ALTEXT 'VEICALTEXT' , " 
cQuery  += " DA3RB1.DA3_COD 'RB1COD' , DA3RB1.DA3_CAPACM 'RB1CAPACM' ,  DA3RB1.DA3_VOLMAX 'RB1VOLMAX', DA3RB1.DA3_COMEXT 'RB1COMEXT' , " 
cQuery  += " DA3RB1.DA3_QTDEIX 'RB1QTDEIX' , DA3RB1.DA3_LAREXT 'RB1LAREXT' , DA3RB1.DA3_ALTEXT 'RB1ALTEXT' , " 
cQuery  += " DA3RB2.DA3_COD 'RB2COD' , DA3RB2.DA3_CAPACM 'RB2CAPACM',  DA3RB2.DA3_VOLMAX 'RB2VOLMAX' , DA3RB2.DA3_COMEXT 'RB2COMEXT' , "
cQuery  += " DA3RB2.DA3_QTDEIX 'RB2QTDEIX' , DA3RB2.DA3_LAREXT 'RB2LAREXT' , DA3RB2.DA3_ALTEXT 'RB2ALTEXT' , "  
cQuery  += " DA3RB3.DA3_COD 'RB3COD' , DA3RB3.DA3_CAPACM 'RB3CAPACM',  DA3RB3.DA3_VOLMAX 'RB3VOLMAX' , DA3RB3.DA3_COMEXT 'RB3COMEXT'  , " 
cQuery  += " DA3RB3.DA3_QTDEIX 'RB3QTDEIX' , DA3RB3.DA3_LAREXT 'RB3LAREXT' , DA3RB3.DA3_ALTEXT 'RB3ALTEXT'  "  
cQuery  += " FROM " + RetSQlName("DTR") + " DTR "
cQuery  += " INNER JOIN " + RetSQLName("DA3") + " DA3VEIC "
cQuery  += " ON DA3VEIC.DA3_FILIAL  = '" + xFilial("DA3") + "' "
cQuery  += " AND DA3VEIC.DA3_COD    = DTR_CODVEI "
cQuery  += " AND DA3VEIC.D_E_L_E_T_ = '' "
cQuery  += " LEFT JOIN " + RetSQLName("DA3") + " DA3RB1 "
cQuery  += " ON DA3RB1.DA3_FILIAL  = '" + xFilial("DA3") + "' "
cQuery  += " AND DA3RB1.DA3_COD    = DTR_CODRB1 "
cQuery  += " AND DA3RB1.D_E_L_E_T_ = '' "
cQuery  += " LEFT JOIN " + RetSQLName("DA3") + " DA3RB2 "
cQuery  += " ON DA3RB2.DA3_FILIAL  = '" + xFilial("DA3") + "' "
cQuery  += " AND DA3RB2.DA3_COD    = DTR_CODRB2 "
cQuery  += " AND DA3RB2.D_E_L_E_T_ = '' "
cQuery  += " LEFT JOIN " + RetSQLName("DA3") + " DA3RB3 "
cQuery  += " ON DA3RB3.DA3_FILIAL  = '" + xFilial("DA3") + "' "
cQuery  += " AND DA3RB3.DA3_COD    = DTR_CODRB3 "
cQuery  += " AND DA3RB3.D_E_L_E_T_ = '' "
cQuery  += " WHERE "
cQuery  += " DTR_FILIAL     = '" + xFilial("DTR") + "' "
cQuery  += " AND DTR_FILORI = '" + ::cFilOri + "' "
cQuery  += " AND DTR_VIAGEM = '" + ::cViagem + "' "
cQuery  += " AND DTR.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )

    cCodVei     += Iif( !Empty( cCodVei ) , "|" + (cAliasQry)->VEICCOD, (cAliasQry)->VEICCOD )
    cCodVei     += Iif( !Empty( (cAliasQry)->RB1COD ) , "|" + (cAliasQry)->RB1COD , "" )
    cCodVei     += Iif( !Empty( (cAliasQry)->RB2COD ) , "|" + (cAliasQry)->RB2COD , "" )
    cCodVei     += Iif( !Empty( (cAliasQry)->RB3COD ) , "|" + (cAliasQry)->RB3COD , "" )
    
    nCapacM     += (cAliasQry)->VEICCAPACM + (cAliasQry)->RB1CAPACM + (cAliasQry)->RB2CAPACM + (cAliasQry)->RB3CAPACM 
    nVolMax     += (cAliasQry)->VEICVOLMAX + (cAliasQry)->RB1VOLMAX + (cAliasQry)->RB2VOLMAX + (cAliasQry)->RB3VOLMAX
    nComExt     += (cAliasQry)->VEICCOMEXT + (cAliasQry)->RB1COMEXT + (cAliasQry)->RB2COMEXT + (cAliasQry)->RB3COMEXT
    
    //--------------------------------------
    //-- Tratamento para largura externa
    //--------------------------------------
    If nLarExt < (cAliasQry)->VEICLAREXT
        nLarExt     := (cAliasQry)->VEICLAREXT
    EndIf

    If nLarExt < (cAliasQry)->RB1LAREXT
        nLarExt := (cAliasQry)->RB1LAREXT
    EndIf
    If nLarExt < (cAliasQry)->RB2LAREXT
        nLarExt := (cAliasQry)->RB2LAREXT
    EndIf
    If nLarExt < (cAliasQry)->RB3LAREXT
        nLarExt := (cAliasQry)->RB3LAREXT
    EndIf

    //--------------------------------------
    //-- Tratamento para altura externa
    //--------------------------------------
    If nAltExt < (cAliasQry)->VEICALTEXT
        nAltExt     := (cAliasQry)->VEICALTEXT
    EndIf

    If nAltExt < (cAliasQry)->RB1ALTEXT
        nAltExt := (cAliasQry)->RB1ALTEXT
    EndIf
    If nAltExt < (cAliasQry)->RB2ALTEXT
        nAltExt := (cAliasQry)->RB2ALTEXT
    EndIf
    If nAltExt < (cAliasQry)->RB3ALTEXT
        nAltExt := (cAliasQry)->RB3ALTEXT
    EndIf
    
    //--------------------------------------
    //-- Quantidade de eixos
    //--------------------------------------
    nQtdEix     += (cAliasQry)->DTR_QTDEIX
    nQtdVolEix  += (cAliasQry)->DTR_QTEIXV 

    //--------------------------------------
    //-- Datas e Hor�rios
    //--------------------------------------
    If Empty(cHorIni)
        dDtIni      := (cAliasQry)->DTR_DATINI
        dDtRea      := (cAliasQry)->DTR_DATFIM
        cHorIni     := (cAliasQry)->DTR_HORINI
        cHorRea     := (cAliasQry)->DTR_HORFIM
    EndIf

    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->( dbCloseArea() )

If !Empty(cCodVei)
    Aadd( ::aVeiculos , {} )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_COD"       , cCodVei } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_CAPACM"    , nCapacM } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_VOLMAX"    , IIf( nVolMax == 0 , 9999 , nVolMax ) } ) // } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_COMEXT"    , nComExt } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_QTDEIX"    , nQtdEix } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_LAREXT"    , nLarExt } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DA3_ALTEXT"    , nAltExt } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_QTEIXV"    , nQtdVolEix } )
    //-- Utilizado pelo m�todo GetAvailablePeriods - Para indicar a disponibilidade da janela de uso do ve�culo
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_DATINI"    , dDtIni  } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_HORINI"    , cHorIni } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_DATFIM"    , dDtRea  } )
    Aadd( ::aVeiculos[Len(::aVeiculos)] , { "DTR_HORFIM"    , cHorRea } )
EndIf

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} GetInfoArr()
Envia um problema de viagem a ser calculado

@author Caio Murakami
@since 11/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function GetInfoArr( aAux  , cLabel , nPosSeek , nPosRet , nCount )
Local xRet      := nil 

Default aAux        := {}
Default cLabel      := ""
Default nPosSeek    := 1
Default nPosRet     := 2
Default nCount      := 1

For nCount := 1 To Len(aAux)
    If AllTrim( aAux[nCount][nPosSeek] ) == AllTrim( cLabel )
        xRet    := aAux[nCount][nPosRet]
        exit
    EndIf
Next nCount

Return xRet

//-----------------------------------------------------------------
/*/{Protheus.doc} TripPost()
Envia um problema de viagem a ser calculado

@author Caio Murakami
@since 11/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD TripPost() CLASS TMSBCARotaInt
Local aAvoidance    := {}
Local aPoints       := {}
Local aSpeed        := {}
Local aHeaderStr    := {}
Local cJSON         := ""
Local cCalcMode     := ""
Local oCallBack     := Nil
Local oVehicles     := {}
Local lRet          := .T. 
Local oResult       := Nil
Local cIDTrip       := ""
Local cResult       := ""
Local nLength       := 0
Local cIDTrip       := ""
Local cRotina       := "TMSA144"
Local cApi          := "TripPost"
Local oIncDLU       := Nil
Local cError        := ""
Local cTypeSpeed    := "true"

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + ::access_token )
Aadd(aHeaderStr, "Content-Type: application/json" )

::SetDadosVge()

//-----------------------------------------
//-- vehicleSpecification
//-----------------------------------------
oVehicles   := ::GetEspecifVehicle()

//-----------------------------------------
//-- CallBack
//-----------------------------------------
oCallBack   := ::GetCallBack()

//-----------------------------------------
//-- Points
//-----------------------------------------
aPoints     := aClone( ::GetPoints() )

//-----------------------------------------
//-- Speed
//-----------------------------------------
aSpeed      := aClone( ::GetSpeedPreferences() )

/*-------------------------------------------------------------------------
//-- Avoidance
//-- TOLL_ROADS	Indicates that the calculated route should avoid toll roads.
//-- TOLL_GATES	Indicates that the calculated route should avoid toll gates.
//-- FERRIES	Indicates that the calculated route should avoid ferries.
//-- TUNNELS	Indicates that the calculated route should avoid tunnels.
//-- BRIDGES	Indicates that the calculated route should avoid brides.
//------------------------------------------------------------------------- */
aAvoidance      := aClone( ::GetAvoidTypes() )

/*-------------------------------------------------------------------------
//-- Calculation Mode
//-- THE_FASTEST	Indicates that the calculated route should consider the fastest path.
//-- THE_SHORTEST	Indicates that the calculated route should consider the path with less KM.
//------------------------------------------------------------------------- */
cCalcMode   :=  ::GetCalcMode()

/*-------------------------------------------------------------------------
//-- TypeSpeed
//-- true	Utiliza a velocidade hist�rico do tr�fego
//-- false	Utiliza a velocidade da via
//------------------------------------------------------------------------- */
cTypeSpeed  := ::GetTypeSpeed()

::oParams["points"]                 := AClone(aPoints)          //-- MANDATORY
::oParams["profileName"]            := "BRAZIL"                 //-- MANDATORY
//::oParams["avoidanceTypes"]         := AClone(aAvoidance)     //-- OPTIONAL      
::oParams["calculationMode"]        := cCalcMode                //-- OPTIONAL
::oParams["callback"]               := oCallBack                //-- OPTIONAL
//::oParams["restrictionZones"]       := AClone(aRestrit)       //-- OPTIONAL
//::oParams["speedPreferences"]       := AClone(aSpeed)         //-- OPTIONAL
::oParams["startDate"]              := Val(  FwTimeStamp(4,dDataBase, Time() )  + "000"  )  //DtoS(dDataBase)        //-- OPTIONAL
::oParams["useRealSpeeds"]          := cTypeSpeed               //-- OPTIONAL
::oParams["vehicleSpecification"]   := oVehicles                //-- OPTIONAL

cJSON   := ::oParams:ToJson()
::oRestClient:SetPath("/trip/v1/problems/")
::oRestClient:SetPostParams( cJSON )
lRet    := ::oRestClient:Post( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
cResult := ::oRestClient:GetResult()

If FWJsonDeserialize(cResult,@oResult)
    If AttIsMemberOf(oResult,"ID") .And. !Empty( oResult:ID )
        lRet            := .T. 
        cIDTrip         := oResult:ID
        ::IdTokenTrip   := cIDTrip

        oIncDLU:= TMSBCAContRotaInt():New()
        oIncDLU:InsertDLU( xFilial("DTQ"), "DTQ",  ::cFilOri + ::cViagem  , cApi, cIDTrip, cRotina, cResult, cJSON  )

    Else    
        lRet    := .F. 
    EndIf
Else
    lRet    := .F. 
EndIf

If !lRet
    cError  := AllTrim( ::oRestClient:GetLastError() )

    oIncDLU := TMSBCAContRotaInt():New()
    oIncDLU:InsertDLU( xFilial("DTQ"), "DTQ",  ::cFilOri + ::cViagem  , cApi, cError, cRotina, cResult, cJSON ,"2" )
    
    If ::lExibeErr
        MsgStop( cError )
    EndIf
    
    cIDTrip  := ""
EndIf

Return lRet //cIDTrip

//-----------------------------------------------------------------
/*/{Protheus.doc} TripGetProblem()
Retrieve a problem by ID

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD TripGetProblem()  CLASS TMSBCARotaInt
Local cToken        := ::access_token
Local cTokenTrip    := ::IdTokenTrip
Local aHeaderStr    := {}
Local lRet          := .T. 
Local cResult       := ""
Local nLength       := 0

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + cToken )
Aadd(aHeaderStr, "Content-Type: application/json" )

::oRestClient:SetPath("/trip/v1/problems/" + cTokenTrip )
lRet    := ::oRestClient:Get( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
cResult := ::oRestClient:GetResult()

Return 

//-----------------------------------------------------------------
/*/{Protheus.doc} TripSolution()
Get Solution

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD TripSolution( cTokenTrip , cResult ) CLASS TMSBCARotaInt
Local cToken        := ::access_token
Local aHeaderStr    := {}
Local lRet          := .T. 
Local nLength       := 0
Local oResult       := Nil
Local aLegs         := {}
Local nCount        := 1 
Local cRotina       := "TMSA144"
Local cApi          := "TripSolution"
Local oIncDLU       := Nil

Default cTokenTrip  := ::IdTokenTrip
Default cResult     := ""    

If Empty(cResult)
    //-----------------------------------------
    //-- Montagem Header
    //-----------------------------------------
    Aadd(aHeaderStr, "Authorization: " + "Bearer " + cToken )
    Aadd(aHeaderStr, "Content-Type: application/json" )

    ::oRestClient:SetPath("/trip/v1/solutions/" + cTokenTrip )
    lRet    := ::oRestClient:Get( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
    cResult := ::oRestClient:GetResult()
EndIf

oIncDLU:= TMSBCAContRotaInt():New()
oIncDLU:InsertDLU( xFilial("DTQ") , "DTQ", ::cFilOri + ::cViagem , cApi, cTokenTrip, cRotina, cResult )

If FWJsonDeserialize(cResult,@oResult)
    If AttIsMemberOf(oResult,"legs") 
        aLegs       := aClone(oResult:legs)
        For nCount := 1 To Len( aLegs )
            Aadd( ::aLegs , Lower( FwJsonSerialize( aLegs[nCount] , .F.  ,.T. , .F. ) ) ) 
        Next nCount
    EndIf
EndIf

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} TripGetJob()
Captura a ultima intera��o do problema enviado

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD TripGetJob() CLASS TMSBCARotaInt
Local cToken        := ::access_token
Local cTokenTrip    := ::IdTokenTrip
Local aHeaderStr    := {}
Local lRet          := .T. 
Local cResult       := ""
Local nLength       := 0
Local oResult       := Nil
Local cStatus       := ""

Sleep(3000)

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + cToken )
Aadd(aHeaderStr, "Content-Type: application/json" )

::oRestClient:SetPath("/trip/v1/jobs/" + cTokenTrip )
lRet    := ::oRestClient:Get( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
cResult := ::oRestClient:GetResult()

If FWJsonDeserialize(cResult,@oResult)
    If AttIsMemberOf(oResult,"STATUS") .And. !Empty( oResult:status )
        cStatus     := oResult:status 
    EndIf
EndIf

Return cStatus

//-----------------------------------------------------------------
/*/{Protheus.doc} PlanningPost()
Envia problema para planning

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD PlanningPost() CLASS TMSBCARotaInt
Local aHeaderStr    := {} 
Local oParams       := JsonObject():New()
Local aRet          := {}

::SetDadosVge()

//-- Callback
::CallBack             := ::GetCallBack()

//-- LogisticConstraints
::logisticConstraints   := aClone( ::GetLogisticConstraints() )

//-- Depots
::depots                := aClone( ::GetDepots() )

//-- LegislationProfiles
::legislationProfile    := aClone( ::GetLegislationProfile() )

//-- Operations
::operations            := aClone( ::GetOperations() )

//-- Products
::products              := ::GetProducts() 

//-- Sites
::sites                 := aClone( ::GetSites() )

//-- VehiclesType
::vehicleTypes          := aClone( ::GetVehicleTypes() )

//-- Vehicles
::vehicles              := aClone( ::GetVgeVehicle() )

oParams["callback"]             := ::CallBack 
oParams["depots"]               := ::depots 
oParams["legislationProfiles"]  := ::legislationProfile
oParams["logisticConstraints"]  := ::logisticConstraints
oParams["operations"]           := ::operations 
oParams["optimizationProfile"]  := "BRAZIL37"
oParams["products"]             := ::products 
oParams["sites"]                := ::sites
oParams["startDate"]            := Val(  FwTimeStamp(4,dDataBase, Time() )  + "000"  ) 
oParams["tripsProfile"]         := "BRAZIL"
oParams["vehicleTypes"]         := ::vehicleTypes  
oParams["vehicles"]             := ::vehicles 

::oPlanning     := oParams

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + ::access_token )
Aadd(aHeaderStr, "Content-Type: application/json" )

aRet   := ::Post( aHeaderSTR, oParams, "/planning/v1/problems" , "PlanningPost")

Return aRet

//-----------------------------------------------------------------
/*/{Protheus.doc} PlanGetJob()
Obtenha o trabalho mais recente de um c�lculo de problema

@author Caio Murakami
@since 11/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD PlanGetJob() CLASS TMSBCARotaInt
Local cToken        := ::access_token
Local cTokenTrip    := ::IdTokenTrip
Local aHeaderStr    := {}
Local lRet          := .T. 
Local cResult       := ""
Local nLength       := 0
Local oResult       := Nil
Local cRet          := ""

Sleep(3000)

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + cToken )
Aadd(aHeaderStr, "Content-Type: application/json" )

::oRestClient:SetPath("/planning/v1/jobs/" + cTokenTrip )
lRet    := ::oRestClient:Get( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
cResult := ::oRestClient:GetResult()

If FWJsonDeserialize(cResult,@oResult)
    If AttIsMemberOf(oResult,"STATUS") .And. !Empty( oResult:status )
        ::planningstaus     := oResult:status 
        cRet                := ::planningstaus
    EndIf
EndIf

Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} PlaningSolution()
Obt�m solu��o

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD PlaningSolution( cTokenTrip , cResult ) CLASS TMSBCARotaInt
Local cToken        := ::access_token
Local aHeaderStr    := {}
Local lRet          := .T. 
Local nLength       := 0
Local oResult       := FwJsonObject():New() 
Local aResult       := {} 
Local nCount        := 1 
Local nRotas        := 1
Local nActive       := 1
Local nDocs         := 1 
Local aSites        := {}
Local aDocs         := {}
Local cRotina       := "TMSA144"
Local cApi          := "PlaningSolution"
Local oIncDLU       := Nil

Default cTokenTrip  := ::IdTokenTrip
Default cResult     := ""

If Empty(cResult)
    //-----------------------------------------
    //-- Montagem Header
    //-----------------------------------------
    Aadd(aHeaderStr, "Authorization: " + "Bearer " + cToken )
    Aadd(aHeaderStr, "Content-Type: application/json" )

    ::oRestClient:SetPath("/planning/v1/solutions/" + cTokenTrip )
    lRet    := ::oRestClient:Get( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
    cResult := ::oRestClient:GetResult()
EndIf

oIncDLU:= TMSBCAContRotaInt():New()
oIncDLU:InsertDLU( xFilial("DTQ") , "DTQ", ::cFilOri + ::cViagem , cApi, cTokenTrip, cRotina, cResult )

If FWJsonDeserialize(cResult,@oResult)

    If AttIsMemberOf(oResult,"vehicleroutes")
        aResult     := aClone( oResult:VehicleRoutes ) 

        If VALTYPE(ARESULT) <> "U"
            For nCount := 1 To Len(aResult)

                If ATTISMEMBEROF(aResult[nCount],"ROUTES")

                    For nRotas := 1 To Len( aResult[nCount]:routes )
                        If AttIsMemberOf(aResult[nCount]:routes[nRotas] , "ACTIVITIES" )
                            For nActive := 1 To Len(aResult[nCount]:routes[nRotas]:ACTIVITIES)
                                                        
                                    If AttIsMemberOf( aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive] , "SITE")
                                        
                                        If  AttIsMemberOf( aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive] , "ACTIVITY")
                                            If AllTrim( Upper( aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]:ACTIVITY) )  == "DELIVERY"
                                                
                                                If AttIsMemberOf( aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive] , "OPERATIONS")

                                                    For nDocs := 1 To Len(aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]:OPERATIONS)
                                                       
                                                        If aScan(aDocs , {|x| x == aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]:OPERATIONS[nDocs]  } ) == 0 
                                                            Aadd( aDocs , aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]:OPERATIONS[nDocs] )
                                                        EndIf

                                                    Next nDocs

                                                EndIf
                                            EndIf                                            
                                        EndIf

                                        If aScan(aSites , {|x| x == aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]:SITE  } ) == 0 
                                            Aadd( aSites , aResult[nCount]:routes[nRotas]:ACTIVITIES[nActive]['site'] )
                                        EndIf

                                    EndIf                       

                            Next nActive
                        EndIf
                    Next nRotas

                EndIf

            Next nCount
        EndIf

        If Len(aDocs) > 0 
            ::AltSeqVge(aDocs)
        EndIf

    EndIf

EndIf

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} Post()
Realiza Post

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD Post( aHeaderSTR , oJSONParam , cPath , cEvent ) CLASS TMSBCARotaInt
Local cJSON     := ""
Local lRet      := .T. 
Local cResult   := ""
Local oResult   := Nil
Local cIDTrip   := ""
Local cRotina   := "TMSA144"
Local cApi      := "PlanningPost"
Local oIncDLU   := Nil
Local cError    := ""

Default aHeaderSTR      := {}
Default oJSONParam      := Nil
Default cPath           := ""

//-- Transforma objeto em formato JSON
cJSON   := oJSONParam:ToJson()

::oRestClient:SetPath(cPath)
::oRestClient:SetPostParams( cJSON )
lRet    := ::oRestClient:Post( aHeaderStr )  //-- "5b9a5bae9583b55da268d0ac"
cResult := ::oRestClient:GetResult()

If FWJsonDeserialize(cResult,@oResult)
    If Upper( cEvent ) == "CALCULATETOLLS"
        If AttIsMemberOf(oResult,"totalCost") 
            ::nValPDG   := oResult:totalCost
            lRet        := .T. 
            oIncDLU:= TMSBCAContRotaInt():New()
            oIncDLU:InsertDLU( xFilial("DTQ") , "DTQ", ::cFilOri + ::cViagem , cEvent, "" , cRotina , cResult, cJson ) 
        EndIf
    Else
        If AttIsMemberOf(oResult,"ID") .And. !Empty( oResult:ID )
            lRet            := .T. 
            cIDTrip         := oResult:ID
            ::IdTokenTrip   := cIDTrip

            oIncDLU:= TMSBCAContRotaInt():New()
            oIncDLU:InsertDLU( xFilial("DTQ") , "DTQ", ::cFilOri + ::cViagem , cApi, cIDTrip, cRotina, cResult, cJson )  
      
        EndIf
    EndIf
Else
    lRet    := .F. 
EndIf

If !lRet 
    cError  := ::oRestClient:GetLastError()
    
    oIncDLU:= TMSBCAContRotaInt():New()
    oIncDLU:InsertDLU( xFilial("DTQ") , "DTQ", ::cFilOri + ::cViagem , cEvent, cError , cRotina , cResult, cJson , "2" ) 
    
    If ::lExibeErr
        MsgStop( Iif( ValType(cError) == "C" , cError , "" ) +  CHR(13) + CHR(10) + cResult )
    EndIf

    cIDTrip  := ""
EndIf

Return { lRet , cResult }

//-----------------------------------------------------------------
/*/{Protheus.doc} calculateTolls()
Calcula valor do pedagio

@author Caio Murakami
@since 14/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD calculateTolls() ClASS TMSBCARotaInt
Local aHeaderStr    := {} 
Local oParams       := JsonObject():New()
Local aRet          := {}
Local oLegs         := JsonObject():New()
Local aLegs         := {}
Local nCount        := 1

aLegs   := aClone( ::GetLegs() )

oParams["legs"]         := aLegs 
oParams["source"]       := "DEFAULT"

//-----------------------------------------
//-- Montagem Header
//-----------------------------------------
Aadd(aHeaderStr, "Authorization: " + "Bearer " + ::access_token )
Aadd(aHeaderStr, "Content-Type: application/json" )

aRet   := ::Post( aHeaderSTR, oParams, "/toll/v1/calculations" , "calculateTolls")

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} GetLegs()
Recupera legs

@author Caio Murakami
@since 14/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetLegs() ClASS TMSBCARotaInt
Local aLegs     := {}
Local oLegs     := JsonObject():New()
Local oPoints   := JsonObject():New()
Local aPoints   := {}
Local nCount    := 1
Local oAux      := JsonObject():New()
Local lVolta    := .F. 

Pergunte("TMSAO46",.F.)
If mv_par01 == 1
    lVolta  := .T. 
EndIf

For nCount := 1 To Len(::aLegs)

    oLegs       := JsonObject():New()
    oAux        := JsonObject():New()
    cJSON       := ::aLegs[nCount]    
    oAux:FromJson(cJson)

    oLegs["points"]         := oAux:GetJsonObject("points")
    oLegs["vehicleType"]    := ::GetEixos(  Iif(lVolta, nCount == Len(::aLegs) , .F.  )   )

    Aadd( aLegs , oLegs )

Next nCount 


Return aLegs

//-----------------------------------------------------------------
/*/{Protheus.doc} CleanDate()
Limpa dados

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD CleanDate() CLASS TMSBCARotaInt

FwFreeObj(::aAuthorization )
FwFreeObj(::aHeader )
FwFreeObj(::api_product_list_json)
FwFreeObj(::aClientes )
FwFreeObj(::aProdutos  )
FwFreeObj(::aVeiculos)
FwFreeObj(::aLogistic )
FwFreeObj(::callback )
//FwFreeObj(::logisticConstraints )
//FwFreeObj(::legislationProfile )
//FwFreeObj(::depots )
//FwFreeObj(::operations  )
//FwFreeObj(::products  )
//FwFreeObj(::sites  )
//FwFreeObj(::startDate    )
//FwFreeObj(::tripsProfile  )
//FwFreeObj(::vehicleTypes )
FwFreeObj(::oPlanning )
FwFreeObj(::oParams )
FwFreeObj(::oRestClient )


Return

//-----------------------------------------------------------------
/*/{Protheus.doc} IsConnect()
Verifica conex�o

@author Caio Murakami
@since 03/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD IsConnect() CLASS TMSBCARotaInt
Local lRet      := .T. 

FwMsgRun( , { || cToken      := ::GetAccessToken() } , "Processando" , "Obtendo Token"  )

If !Empty(cToken)
    lRet    := .T. 
Else
    lRet    := .F. 
EndIf

Return lRet

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCallBack()
Indica endere�o de retorno

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetCallBack() CLASS TMSBCARotaInt
Local oCallBack     := JsonObject():New()
Local cURL          := ""
Local cPassword     := ""
Local cUser         := ""
Local cQuery        := ""
Local cAliasQry     := GetNextAlias()

cQuery  := " SELECT DLV_URLCAL, DLV_USRCAL , DLV_PSWCAL "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL  = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->(!Eof())
    cURL        := RTrim( Lower( (cAliasQry)->DLV_URLCAL ) )
    cUser       := RTrim( (cAliasQry)->DLV_USRCAL )
    cPassword   := RTrim( (cAliasQry)->DLV_PSWCAL )
    (cAliasQry)->(dbSkip())
EndDo

(cAliasQry)->(dbCloseArea())

If !Empty(cPassword)
    oCallBack["password"]   := cPassword
EndIf
If !Empty(cURL)
    oCallBack["url"]        := cURL
EndIf
If !Empty(cUser)
    oCallBack["user"]       := cUser
EndIf

Return oCallBack

//-----------------------------------------------------------------
/*/{Protheus.doc} GetDepots()
Obtem dep�sitos

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetDepots( cFilOri , cViagem ) CLASS TMSBCARotaInt
Local oDepots       := JsonObject():New()
Local oCordinate    := Nil
Local aDepots       := {} 
Local aAreaDTW      := DTW->(GetArea())
Local cAtivSai		:= SuperGetMV('MV_ATIVSAI',,'') //-- Atividade de Saida de Viagem
Local cKeyLogistic  := "DEFAULT"

Default cFilOri     := ""
Default cViagem     := ""

oCordinate  := ::GetCordinate("SM0","",::cFilOri)

DTW->( dbSetOrder(4) ) //-- DTW_FILIAL+DTW_FILORI+DTW_VIAGEM+DTW_ATIVID+DTW_FILATI                                                                                                          
If DTW->( dbSeek( xFilial("DTW") + ::cFilOri + ::cViagem + cAtivSai + ::cFilOri ))
    cKeyLogistic    := cAtivSai
EndIf

oDepots["coordinates"]          := oCordinate
oDepots["logisticConstraints"]  := cKeyLogistic
oDepots["name"]                 := ::cFilOri

Aadd(aDepots,oDepots)

RestArea(aAreaDTW)
Return aDepots

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCordinate()
Obtem coordenadas

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetCordinate( cAliasDAR , cFilEnt , cCodEnt , cSeqEnd ) CLASS TMSBCARotaInt
Local oCordinate    := Nil
Local cAliasQry     := GetNextAlias()
Local nValLatitu    := 0 
Local nValLongit    := 0 
Local cAliasAux     := "DUL"
Local cFilAux       := xFilial("DUL")

Default cAliasDAR   := ""
Default cFilEnt     := ""
Default cCodEnt     := ""
Default cSeqEnd     := ""

cQuery  := " SELECT DAR_LATITU, DAR_LONGIT "
cQuery  += " FROM "  +RetSQLName("DAR") + " DAR "
cQuery  += " WHERE DAR_FILIAL   = '" + xFilial("DAR") + "' "

If !Empty(cSeqEnd)
    cQuery  += " AND DAR_FILENT     IN ('" + cFilEnt + "' , '" + cFilAux + "' ) "
    cQuery  += " AND DAR_ENTIDA     IN ('" + cAliasDAR + "' , '" + cAliasAux + "' ) "
    cQuery  += " AND DAR_CODENT     = '" + RTrim(cSeqEnd) + "'  "
Else
    cQuery  += " AND DAR_FILENT     = '" + cFilEnt + "' "
    cQuery  += " AND DAR_ENTIDA     = '" + cAliasDAR + "' "
    cQuery  += " AND DAR_CODENT     = '" + RTrim(cCodEnt) + "' "
EndIf

cQuery  += " AND DAR.D_E_L_E_T_ = '' "
 
cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    oCordinate  := JsonObject():New()
    nValLatitu  := Val( StrTran((cAliasQry)->DAR_LATITU,",",".") )
    nValLongit  := Val( StrTran((cAliasQry)->DAR_LONGIT,",",".") )
    
    oCordinate["latitude"]  := nValLatitu
    oCordinate["longitude"] := nValLongit

    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->( dbCloseArea() )

Return oCordinate

//-----------------------------------------------------------------
/*/{Protheus.doc} GetLegislationProfile()
Obtem profile de legisla��o - dever� ser utilizado em conjunto com jornada de motorista (MV_CONTJOR)

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetLegislationProfile( cFilOri , cViagem ) CLASS TMSBCARotaInt
Local oLegislation  := JsonObject():New()
Local aLegislation  := {}

Default cFilOri     := ""
Default cViagem     := ""

/*oLegislation["drivingPauseDuration"]            := 0
oLegislation["drivingPauseDurationCuts"]        := {0}
oLegislation["drivingRestDuration"]             := 0
oLegislation["drivingRestDurationCuts"]         := {0}
oLegislation["maxContinuousDrivingTime"]        := 0
oLegislation["maxContinuousWorkingTime"]        := 0
oLegislation["maxDrivingBetweenTwoRests"]       := 0
oLegislation["maxWaitingTime"]                  := 0
oLegislation["maxWorkingTimeBetweenTwoRests"]   := 0
oLegislation["name"]                            := "DEFAULT"
oLegislation["waitingIsWorking"]                := .F.
oLegislation["workingPauseDuration"]            := 0
oLegislation["workingPauseDurationCuts"]        := {0}
oLegislation["workingRestDuration"]             := 0
oLegislation["workingRestDurationCuts"]         := {0}*/

oLegislation["name"]                            := "DEFAULT"

Aadd( aLegislation , oLegislation )

Return aLegislation
 
//-----------------------------------------------------------------
/*/{Protheus.doc} GetLogisticConstraints()
Indica as restri��es que devem ser levadas em conta para cada dep�sito ou site que estar� na otimiza��o

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetLogisticConstraints() CLASS TMSBCARotaInt
Local oLogistic     := Nil
Local aLogistic     := {}
Local cName         := ""
Local nCount        := 1

oLogistic   := JsonObject():New()

//-- LogisticConstraints
oLogistic["name"]                               := "DEFAULT"
oLogistic["siteUnloadingFixedTime"]             := 0
oLogistic["loadingMaxSize"]                     := Nil
oLogistic["unloadingMaxSize"]                   := Nil

Aadd(aLogistic,oLogistic)

For nCount := 1 To Len(::aLogistic)

    cName       := GetInfoArr(::aLogistic[nCount] , "DTW_ATIVID")  
    nTotal      := GetInfoArr(::aLogistic[nCount] , "DC6_DURAC") 

    oLogistic   := JsonObject():New()

    //-- LogisticConstraints
    oLogistic["name"]                               := cName
    oLogistic["siteUnloadingFixedTime"]             := nTotal
    oLogistic["loadingMaxSize"]                     := Nil
    oLogistic["unloadingMaxSize"]                   := Nil

    Aadd(aLogistic,oLogistic)

Next nCount

/*oLogistic["loadingPositionInRoute"]             := ""
oLogistic["loadingPositionInTimeWindow"]        := ""
oLogistic["name"]                               := "DEFAULT"
oLogistic["siteUnloadingFixedTime"]               := 0
oLogistic["loadingMaxSize"]                     := Nil
oLogistic["unloadingMaxSize"]                   := Nil
oLogistic["siteUnloadingFixedTime"]             := 0
oLogistic["unloadingPositionInRoute"]           := ""
oLogistic["unloadingPositionInTimeWindow"]      := ""*/

Return aLogistic

//-----------------------------------------------------------------
/*/{Protheus.doc} GetOperations()
Indica as opera��es de coleta ou entrega que devem ser consideradas na otimiza��o

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetOperations() CLASS TMSBCARotaInt
Local oOperation    := Nil
Local aOperation    := {}
Local oCustomTime   := Nil 
Local nCount        := 1

Default cFilOri     := ""
Default cViagem     := ""

For nCount := 1 To Len(::aDocs)

    oCustomTime     := ::GetCustomerTime( GetInfoArr(::aDocs[nCount],"DT6_FILDOC") ,GetInfoArr(::aDocs[nCount],"DT6_DOC"), GetInfoArr(::aDocs[nCount],"DT6_SERIE") ) 
    oOperation      := JsonObject():New()

    oOperation["id"]                        := GetInfoArr(::aDocs[nCount] , "DT6_FILDOC") + GetInfoArr(::aDocs[nCount] , "DT6_DOC") + GetInfoArr(::aDocs[nCount] , "DT6_SERIE") + GetInfoArr(::aDocs[nCount] , "A1_COD") + GetInfoArr(::aDocs[nCount] , "A1_LOJA")
    //oOperation["group"]                     := ""
    oOperation["customerSite"]              := GetInfoArr(::aDocs[nCount] , "A1_COD") + GetInfoArr(::aDocs[nCount] , "A1_LOJA")
    oOperation["customerHandlingDuration"]  := 0
    oOperation["customerTimeWindows"]       := { oCustomTime }
    oOperation["depotSite"]                 := ::cFilOri
    oOperation["depotHandlingDuration"]     := 0
    oOperation["preAllocatedVehicleName"]   := Nil
    oOperation["priority"]                  := GetInfoArr(::aDocs[nCount] , "PRIORIDADE")
    oOperation["product"]                   := GetInfoArr(::aDocs[nCount] , "B1_COD")

    If AllTrim( GetInfoArr(::aDocs[nCount] , "DT6_SERIE") ) <> "COL" 
        oOperation["type"]                      := "DELIVERY"
    Else
        oOperation["type"]                      := "COLLECTION"
    EndIf

    oOperation["volume"]                    := GetInfoArr(::aDocs[nCount] , "DT6_METRO3")
    oOperation["weight"]                    := GetInfoArr(::aDocs[nCount] , "DT6_PESO") 

    Aadd(aOperation,oOperation)

Next nCount 

Return aOperation

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCustomerTime()
Indica as opera��es de coleta ou entrega que devem ser consideradas na otimiza��o

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetCustomerTime( cFilDoc , cDoc , cSerie ) CLASS TMSBCARotaInt
Local aArea         := GetArea()
Local aAreaDT6      := DT6->( GetArea() )
Local oCustomTime   := JsonObject():New()
Local cQuery        := ""
Local cAliasQry     := GetNextAlias()
Local dDataAgd      := dDataBase
Local cHorIni       := "23:59:00"
Local cHorFim       := "23:59:59"
Local dDataFim      := dDataBase + 1

Default cFilDoc     := ""
Default cDoc        := ""
Default cSerie      := ""

cQuery  := " SELECT DYD_DATAGD, DYD_INIAGD , DYD_FIMAGD "
cQuery  += " FROM " + RetSQLName("DT6") + " DT6 "
cQuery  += " INNER JOIN " + RetSQLName("DYD") + " DYD "
cQuery  += "    ON DYD_FILIAL       = '" + xFilial("DYD") + "' "
cQuery  += "    AND DYD_FILDOC      = DT6_FILDOC "
cQuery  += "    AND DYD_DOC         = DT6_DOC "
cQuery  += "    AND DYD_SERIE       = DT6_SERIE "
cQuery  += "    AND DYD_NUMAGD      = DT6_NUMAGD "
cQuery  += "    AND DYD_ITEAGD      = DT6_ITEAGD "
cQuery  += "    AND DYD_STATUS      IN ('1','4') "
cQuery  += "    AND DYD.D_E_L_E_T_  = '' "
cQuery  += " WHERE DT6_FILIAL       = '" + xFilial("DT6") + "' "
cQuery  += "    AND DT6_FILDOC      = '" + cFilDoc + "' "
cQuery  += "    AND DT6_DOC         = '" + cDoc + "' "
cQuery  += "    AND DT6_SERIE       = '" + cSerie + "' "
cQuery  += "    AND DT6.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAliasQry, .F., .T. )

TcSetField(cAliasQry,"DYD_DATAGD","D",8,0)

While (cAliasQry)->( !Eof() )
    
    If !Empty((cAliasQry)->DYD_DATAGD)
        dDataAgd    := (cAliasQry)->DYD_DATAGD
        dDataFim    := dDataAgd
    EndIf

    If !Empty((cAliasQry)->DYD_INIAGD)
         cHorIni     := TRANSFORM( (cAliasQry)->DYD_INIAGD  + "00", "@R 99:99:99")
    EndIf

    If !Empty((cAliasQry)->DYD_FIMAGD)
        cHorFim     := TRANSFORM( (cAliasQry)->DYD_FIMAGD  + "00", "@R 99:99:99")
    EndIf

    (cAliasQry)->( dbSkip() )
EndDo 

(cAliasQry)->( dbCloseArea() )

oCustomTime["start"]    := Val( FwTimeStamp(4, dDataAgd , cHorIni )  + "000" )
oCustomTime["end"]      := Val( FwTimeStamp(4, dDataFim , cHorFim )  + "000" )

RestArea( aAreaDT6 )
RestArea( aArea )

Return oCustomTime

//-----------------------------------------------------------------
/*/{Protheus.doc} GetProducts()
Indica os produtos que ser�o coletados ou entregues em cada opera��o

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetProducts() CLASS TMSBCARotaInt
Local oProducts     := Nil
Local aProducts     := {}
Local nCount         := 1

For nCount := 1 to Len(::aProdutos)

    oProducts     := JsonObject():New()
    oProducts["name"]       := ::aProdutos[nCount,1][2]
    oProducts["type"]       := ::aProdutos[nCount,2][2]

    Aadd( aProducts , oProducts )
    
Next nCount 


Return aProducts

//-----------------------------------------------------------------
/*/{Protheus.doc} GetSites()
Indica os sites do cliente que devem ser considerados na otimiza��o. 

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetSites() CLASS TMSBCARotaInt
Local oSites        := Nil
Local oCordinate    := Nil
Local aSites        := {}
Local nCount        := 1 
Local nPos          := 0 
Local cCodCli       := ""
Local cLojCli       := ""
Local cLogistic     := "DEFAULT"
Local cAlias        := ""
Local aAux          := {}
Local cSeqEnd       := ""

Default cFilOri     := ""
Default cViagem     := ""
   
For nCount := 1 To Len(::aClientes)
    
    oSites       := JsonObject():New()

    cCodCli     := GetInfoArr(::aClientes[nCount] , "A1_COD")
    cLojCli     := GetInfoArr(::aClientes[nCount] , "A1_LOJA")
    nPos        := AScan( ::aLogistic , { |x| x[2,2] + x[3,2] == cCodCli + cLojCli })
    cLogistic   := "DEFAULT"
    cAlias      := GetInfoArr(::aClientes[nCount] , "ALIAS")
    cSeqEnd     := GetInfoArr(::aClientes[nCount] , "SQEEND")

    If nPos
        cLogistic   := GetInfoArr(::aLogistic[nPos] , "DTW_ATIVID")  
    EndIf

    If Len(aAux) == 0 .Or. AScan( aAux , {|x| x[1] == cCodCli + cLojCli } ) == 0 
 
        //-- Coordenadas geogr�ficas
        oCordinate              := ::GetCordinate(cAlias , xFilial(cAlias) , cCodCli + cLojCli , cSeqEnd)

        oSites["coordinates"]           := oCordinate
        oSites["logisticConstraints"]   := cLogistic
        oSites["name"]                  := cCodCli + cLojCli

        Aadd(aAux , { cCodCli + cLojCli } )
        Aadd(aSites,oSites)
    EndIf

Next nCount

Return aSites              

//-------------------------------------------------------------------
/*/{Protheus.doc} GetVehicleTypes()
Indica os tipos de ve�culos

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetVehicleTypes() CLASS TMSBCARotaInt
Local oTypeVeic     := JsonObject():New()
Local aTypeVeic     := {} 
Local nAux          := 1 


For nAux := 1 To Len(::aVeiculos)

    oTypeVeic     := JsonObject():New()

    oTypeVeic["maxVolume"]      := Iif( GetInfoArr(::aVeiculos[nAux] , "DA3_VOLMAX" ) == 0 , 9999 , GetInfoArr(::aVeiculos[nAux] , "DA3_VOLMAX" ) ) 
    oTypeVeic["maxWeight"]      := Iif( GetInfoArr(::aVeiculos[nAux] , "DA3_CAPACM" ) == 0 , 9999 , GetInfoArr( ::aVeiculos[nAux]  , "DA3_CAPACM" ) )
    oTypeVeic["name"]           := GetInfoArr( ::aVeiculos[nAux]  , "DA3_COD" )
    oTypeVeic["size"]           := GetInfoArr( ::aVeiculos[nAux]  , "DA3_COMEXT" )

    Aadd( aTypeVeic , oTypeVeic )

Next nAux

Return aTypeVeic

//-----------------------------------------------------------------
/*/{Protheus.doc} GetVgeVehicle()
Indica o ve�culo que deve ser considerado na otimiza��o

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetVgeVehicle() CLASS TMSBCARotaInt
Local oVehicles     := Nil
Local oAvalPer      := Nil
Local aVehicles     := {}
Local nAux          := 1

For nAux := 1 To Len(::aVeiculos)
    oAvalPer    := ::GetAvailablePeriods()
    oVehicles     := JsonObject():New()
    oVehicles["availablePeriods"]       := { oAvalPer }
    oVehicles["legislationProfile"]     := "DEFAULT"
    oVehicles["name"]                   := GetInfoArr( ::aVeiculos[nAux]  , "DA3_COD" )
    oVehicles["vehicleType"]            := GetInfoArr( ::aVeiculos[nAux]  , "DA3_COD" )

    Aadd( aVehicles, oVehicles )
Next nAux

Return aVehicles

//-----------------------------------------------------------------
/*/{Protheus.doc} GetAvailablePeriods()
Indica a janela de tempo em que um ve�culo est� dispon�vel

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetAvailablePeriods() CLASS TMSBCARotaInt
Local oAvalPer      := JsonObject():New()
Local oTimeWindow   := Nil
Local cDatIni       := ""
Local cHorIni       := ""
Local cDatFim       := ""
Local cHorFim       := ""
Local nAux          := 1 

For nAux := 1 To Len(::aVeiculos)
    cDatIni := GetInfoArr( ::aVeiculos[nAux]  , "DTR_DATINI" )
    cHorIni := GetInfoArr( ::aVeiculos[nAux]  , "DTR_HORINI" )
    cDatFim := GetInfoArr( ::aVeiculos[nAux]  , "DTR_DATFIM" )
    cHorFim := GetInfoArr( ::aVeiculos[nAux]  , "DTR_HORFIM" )
Next nAux 
    
cDatIni := Stod(cDatIni)
cHorIni := Transform(cHorIni + "00" , "@R 99:99:99")
cDatFim := Stod(cDatFim)
cHorFim := Transform(cHorFim + "00" , "@R 99:99:99")

If Empty(cDatIni)
    cDatIni := dDataBase
    cHorIni  := Time()
    cDatFim  := dDataBase + 1
    cHorFim  := "23:59:59" 
EndIf 

oTimeWindow   := JsonObject():New()

oTimeWindow["start"]    := Val( FwTimeStamp(4,cDatIni , cHorIni ) + "000"  )
oTimeWindow["end"]      := Val( FwTimeStamp(4,cDatFim , cHorFim ) + "000"  )

oAvalPer["departureSite"]   := ::cFilOri
oAvalPer["arrivalSite"]     := ::cFilOri
oAvalPer["timeWindow"]      := oTimeWindow
oAvalPer["maxRoutesNumber"] := 1

Return oAvalPer

//-----------------------------------------------------------------
/*/{Protheus.doc} GetTimeWindow()
Indica a data e a hora em que a atividade ocorrer�

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetTimeWindow() CLASS TMSBCARotaInt
Local oTimeWindow   := JsonObject():New()

oTimeWindow["start"]    := Val( FwTimeStamp(4,dDataBase ,time()) + "000"  )
oTimeWindow["end"]      := Val( FwTimeStamp(4,dDataBase + 7 ,"23:59:59")  + "000"  )

Return oTimeWindow

//-----------------------------------------------------------------
/*/{Protheus.doc} GetPoints()

Indica os waypoints que devem ser considerados na (s) rota (s). Pelo menos dois pontos devem ser especificados. 

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetPoints() CLASS TMSBCARotaInt
Local oPoints   := JsonObject():New()
Local aPoints   := {} 
Local nCount    := 1
Local cCodCli   := ""
Local cLojCli   := ""
Local cAlias    := ""
Local oCordinate:= Nil
Local aAux      := {}
Local cSeqEnd   := ""

oCordinate  := ::GetCordinate("SM0","",::cFilOri)

oPoints["latitude"]     := oCordinate['latitude']
oPoints["longitude"]    := oCordinate['longitude'] 
oPoints["siteId"]       := ::cFilOri

Aadd( aPoints , oPoints )

For nCount := 1 To Len(::aClientes)

    oPoints := JsonObject():New()

    cCodCli     := GetInfoArr(::aClientes[nCount] , "A1_COD")
    cLojCli     := GetInfoArr(::aClientes[nCount] , "A1_LOJA")   
    cAlias      := GetInfoArr(::aClientes[nCount] , "ALIAS")
    cSeqEnd     := GetInfoArr(::aClientes[nCount] , "SQEEND")

    If Len(aAux) == 0 .Or. AScan( aAux , {|x| x[1] == cCodCli + cLojCli } ) == 0 
        //-- Coordenadas geogr�ficas
        oCordinate              := ::GetCordinate(cAlias , xFilial(cAlias) , cCodCli + cLojCli , cSeqEnd )

        oPoints["latitude"]     := oCordinate['latitude']
        oPoints["longitude"]    := oCordinate['longitude'] 
        oPoints["siteId"]       := GetInfoArr(::aClientes[nCount],"A1_COD") + GetInfoArr(::aClientes[nCount],"A1_LOJA")

        Aadd( aPoints , oPoints )
        Aadd( aAux , { cCodCli + cLojCli })
    EndIf

Next nCount

Pergunte("TMSAO46",.F.)

//-- Retorna para a filial?
If mv_par01 == 1
    oCordinate  := ::GetCordinate("SM0","",::cFilOri)
    oPoints := JsonObject():New()
    
    oPoints["latitude"]     := oCordinate['latitude']
    oPoints["longitude"]    := oCordinate['longitude'] 
    oPoints["siteId"]       := ::cFilOri + "|" + ::cFilOri

    Aadd( aPoints , oPoints )
EndIf

Return aPoints

//-----------------------------------------------------------------
/*/{Protheus.doc} GetProfileName()

Indica os dados padr�o que devem ser considerados para calcular a (s) rota (s). 
O profileName par�metro suporta um ID de perfil que deve ser previamente definido 
durante a configura��o da API com a equipe de integra��o da Rota Inteligente. 
Os perfis padr�o s�o: M�XICO, COL�MBIA, CHILE, BRASIL, ARGENTINA, EGIPTO, 
EUROPA, FRAN�A, SAUDI�RBIA.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetProfileName() CLASS TMSBCARotaInt
Local cName     := "BRAZIL"

Return cName

//-----------------------------------------------------------------
/*/{Protheus.doc} GetSpeedPreferences()

Indica que a (s) rota (s) calculada (s) devem considerar diferentes velocidades 
por tipo de estrada. Pode ser usado para ajustar a velocidade considerada no c�lculo 
para qualquer tipo de ve�culo.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetSpeedPreferences() CLASS TMSBCARotaInt
Local oSpeed    := JsonObject():New()
Local aSpeed    := {}

/* ----------------------------------------------------------------------
//-- roadType 
//-- FERRY
//-- PENALIZED_LOCAL_ROAD
//-- LOCAL_ROAD
//-- PENALIZED_SECONDARY_ROAD
//-- SECONDARY_ROAD
//-- PENALIZED_MAIN_ROAD
//-- MAIN_ROAD
//-- EXPRESSWAY
//-- HIGHWAY
//
//-- speed -Indica a velocidade da estrada em quil�metros por hora
//
//-- Se n�o for especificada uma prefer�ncia de velocidade, a velocidade real da estrada para os carros ser� considerada como padr�o. 
// ---------------------------------------------------------------------- */

oSpeed["roadType" ]     := ""
oSpeed["speed" ]        := 0

Aadd( aSpeed , oSpeed )

Return aSpeed
//-----------------------------------------------------------------
/*/{Protheus.doc} GetAvoidTypes()

Indica que a (s) rota (s) calculada (s) deve (m) evitar alguns recursos

Se a �nica rota disponivel for por algum dos recursos que devem ser evitados, 
os par�metros ser�o ignorados.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetAvoidTypes() CLASS TMSBCARotaInt
Local aAvoidance   := {}

/*-------------------------------------------------------------------------
//-- Avoidance
//-- TOLL_ROADS	Indicates that the calculated route should avoid toll roads.
//-- TOLL_GATES	Indicates that the calculated route should avoid toll gates.
//-- FERRIES	Indicates that the calculated route should avoid ferries.
//-- TUNNELS	Indicates that the calculated route should avoid tunnels.
//-- BRIDGES	Indicates that the calculated route should avoid brides.
//------------------------------------------------------------------------- */
Aadd(aAvoidance, "BRIDGES")
Aadd(aAvoidance, "FERRIES")

Return aAvoidance

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCalcMode()


@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetCalcMode() CLASS TMSBCARotaInt
Local cCalcMode   := "THE_FASTEST"

/*-------------------------------------------------------------------------
//-- Calculation Mode
//-- THE_FASTEST	Indicates that the calculated route should consider the fastest path.
//-- THE_SHORTEST	Indicates that the calculated route should consider the path with less KM.
//------------------------------------------------------------------------- */
Pergunte("TMSAO46",.F.)
If mv_par02 == 1
   cCalcMode   := "THE_FASTEST"
ElseIf mv_par02 == 2
    cCalcMode   := "THE_SHORTEST"
EndIf

Return cCalcMode

//-----------------------------------------------------------------
/*/{Protheus.doc} GetStartDate()

Indica que as rotas calculadas devem come�ar em uma data e hora espec�ficas. 
O startDatepar�metro � obrigat�rio se useRealSpeeds = true, e se n�o for especificado,
 o valor ser� definido para a hora atual.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetStartDate() CLASS TMSBCARotaInt
Local dData   := DtoS(dDataBase) 

Return dData

//-----------------------------------------------------------------
/*/{Protheus.doc} GetRestrictZones()

Indica que a (s) rota (s) calculada (s) deve (m) evitar algumas zonas . 
O restrictionZones par�metro suporta uma lista de id de zona que deve ser 
previamente definida durante a constru��o do perfil.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetRestrictZones() CLASS TMSBCARotaInt
Local oRestrit  := JsonObject():New()
Local aRestrit  := {}

Aadd( aRestrit , oRestrit )

Return aRestrit

//-----------------------------------------------------------------
/*/{Protheus.doc} GetTypeSpeed()

Permite habilitar as velocidades reais para o c�lculo da viagem. 
As velocidades s�o baseadas no hist�rico de tr�fego no hor�rio definido startDate.
O useRealSpeedspar�metro suporta o seguinte argumento:

true  - Indica que as rotas calculadas devem considerar as velocidades com base no hist�rico de tr�fego.
false -	Indica que as rotas calculadas devem considerar as velocidades com base no tipo de estrada.

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetTypeSpeed() CLASS TMSBCARotaInt
Local cRealSpeed   := "true"

Pergunte("TMSAO46",.F.)
If mv_par03 == 1
    cRealSpeed   := "true"
ElseIf mv_par03 == 2
    cRealSpeed   := "false"
EndIf

Return cRealSpeed

//-----------------------------------------------------------------
/*/{Protheus.doc} GetEspecifVehicle()

Indica que a (s) rota (s) calculada (s) deve (m) considerar as seguintes restri��es de ve�culo:

@author Caio Murakami
@since 04/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetEspecifVehicle() CLASS TMSBCARotaInt
Local oVehicles     := Nil
Local nCount        := 1

oVehicles     := JsonObject():New()

//oVehicles["loadTypes"]                      := AClone(aLoadTypes)   
oVehicles["maxHeight"]                      := GetInfoArr( ::aVeiculos[nCount]  , "DA3_ALTEXT" )
oVehicles["maxLength"]                      := GetInfoArr( ::aVeiculos[nCount]  , "DA3_COMEXT" )
oVehicles["maxLengthBetweenAxles"]          := GetInfoArr( ::aVeiculos[nCount]  , "DA3_COMEXT" )
oVehicles["maxWeight"]                      := GetInfoArr( ::aVeiculos[nCount]  , "DA3_CAPACM" )
oVehicles["maxWeightForDangerousMaterials"] := GetInfoArr( ::aVeiculos[nCount]  , "DA3_CAPACM" )
oVehicles["maxWeightForExplodingMaterials"] := GetInfoArr( ::aVeiculos[nCount]  , "DA3_CAPACM" )
oVehicles["maxWeightForPollutingMaterials"] := GetInfoArr( ::aVeiculos[nCount]  , "DA3_CAPACM" )
oVehicles["maxWeightPerAxle"]               := Round( GetInfoArr( ::aVeiculos[nCount]  , "DA3_CAPACM" ) / GetInfoArr( ::aVeiculos[nCount]  , "DTR_QTDEIX" ), 2 )
oVehicles["maxWidth"]                       := GetInfoArr( ::aVeiculos[nCount]  , "DA3_LAREXT" )  

Return oVehicles

//-----------------------------------------------------------------
/*/{Protheus.doc} GetEixos()
Pega label referente a quantidade de eixos

@author Caio Murakami
@since 16/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetEixos(lVolta) CLASS TMSBCARotaInt
Local cLabel        := "TRUCK_WITH_TWO_SINGLE_AXIS"
Local cCodVei       := ""
Local nQtdEix       := 0 
Local nCount        := 1
Local aArea         := GetArea()
Local cCatVei       := ""
Local nEixoVolta    := 0

Default lVolta      := .F. 

For nCount := 1 To Len(::aVeiculos)
    cCodVei     := GetInfoArr(::aVeiculos[nCount],"DA3_COD")
    nQtdEix     := GetInfoArr(::aVeiculos[nCount],"DTR_QTDEIX")
    nEixoVolta  := GetInfoArr(::aVeiculos[nCount],"DTR_QTEIXV")

    If lVolta
        nQtdEix := nEixoVolta
    EndIf

    exit
Next nCount

/* --------------------------------
-- "CAR"
-- "MOTORCYCLE"
-- "CAR_WITH_THREE_SIMPLE_AXLES"
-- "CAR_WITH_FOUR_SIMPLE_AXLES"
-- "BUS_WITH_TWO_DOUBLE_AXLES"
-- "BUS_WITH_THREE_DOUBLE_AXLES"
-- "BUS_WITH_FOUR_DOUBLE_AXLES"
-- "BUS_WITH_FIVE_DOUBLE_AXLES"
-- "TRUCK_WITH_TWO_SINGLE_AXIS"
-- "TRUCK_WITH_TWO_DOUBLE_AXLES"
-- "TRUCK_WITH_THREE_DOUBLE_AXLES"
-- "TRUCK_WITH_FOUR_DOUBLE_AXLES"
-- "TRUCK_WITH_FIVE_DOUBLE_AXLES"
-- "TRUCK_WITH_SIX_DOUBLE_AXLES"
-- "TRUCK_WITH_SEVEN_DOUBLE_AXLES"
-- "TRUCK_WITH_EIGHT_DOUBLE_AXLES"
-- "TRUCK_WITH_NINE_DOUBLE_AXLES"
-------------------------------- */

If nQtdEix > 0 
    DA3->(dbSetOrder(1))
    If DA3->(MsSeek(xFilial("DA3") + cCodVei ))
        DUT->(dbSetOrder(1))
        If DUT->(MsSeek(xFilial("DUT") + DA3->DA3_TIPVEI ))
            /*-----------------------
            //-- 1=Comum
            //-- 2=Cavalo
            //-- 3=Carreta
            //-- 4=Especial
            //-- 5=Utilit�rio
            //-- 6=Composi��o
            ----------------------- */       
            cCatVei     := DUT->DUT_CATVEI 
        EndIf
    EndIf

    If nQtdEix == 2 
        If cCatVei == "1" .Or. Empty(cCatVei)
            cLabel  := "TRUCK_WITH_TWO_SINGLE_AXIS"
        Else
            cLabel  := "TRUCK_WITH_TWO_DOUBLE_AXLES"
        EndIf        
    ElseIf nQtdEix == 3 
        cLabel  := "TRUCK_WITH_THREE_DOUBLE_AXLES"
    ElseIf nQtdEix == 4 
        cLabel  := "TRUCK_WITH_FOUR_DOUBLE_AXLES"
    ElseIf nQtdEix == 5 
        cLabel  := "TRUCK_WITH_FIVE_DOUBLE_AXLES"
    ElseIf nQtdEix == 6 
        cLabel  := "TRUCK_WITH_SIX_DOUBLE_AXLES"
    ElseIf nQtdEix == 7 
        cLabel  := "TRUCK_WITH_SEVEN_DOUBLE_AXLES"
    ElseIf nQtdEix == 8 
        cLabel  := "TRUCK_WITH_EIGHT_DOUBLE_AXLES"
    ElseIf nQtdEix >= 9 
        cLabel  := "TRUCK_WITH_NINE_DOUBLE_AXLES"
    EndIf
EndIf

RestArea(aArea)
Return cLabel

//-----------------------------------------------------------------
/*/{Protheus.doc} UpdatePedagio()
Atualiza valores de pedagio na DTR 

@author Caio Murakami
@since 16/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD UpdatePedagio() CLASS TMSBCARotaInt
Local aAreaDTR      := DTR->( GetArea() )

DTR->(dbSetOrder(1))
If  ::nValPDG  > 0 .And. DTR->( MsSeek( xFilial("DTR") + ::cFilOri + ::cViagem ))
    RecLock("DTR",.F.)
    DTR->DTR_VALPDG     := ::nValPDG  
    MsUnlock()
EndIf

RestArea( aAreaDTR )
Return


//-----------------------------------------------------------------
/*/{Protheus.doc} AltSeqVge()
Atualiza a sequencia da viagem

@author Caio Murakami
@since 01/07/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD AltSeqVge( aDocs ) CLASS TMSBCARotaInt
Local aArea     := GetArea()
Local nCount    := 1

Default aDocs  := {} 

//-- DUD_FILIAL+DUD_FILDOC+DUD_DOC+DUD_SERIE+DUD_FILORI+DUD_VIAGEM  
DUD->(dbSetOrder(1))
For nCount := 1 To Len(aDocs)

    If DUD->( dbSeek( xFilial("DUD") + RTrim(aDocs[nCount] ) ) ) 
        RecLock("DUD", .F. )
        DUD->DUD_SEQUEN     := StrZero( nCount , TamSX3("DUD_SEQUEN")[1] )
        DUD->( MsUnlock() )
    EndIf

Next nCount 

RestArea(aArea)
Return

//-----------------------------------------------------------------
/*/{Protheus.doc} Destroy()
Destroi objeto

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD Destroy() CLASS TMSBCARotaInt

::CleanDate()

::aAuthorization        := Nil
::aHeader               := Nil
::api_product_list_json := Nil
::aClientes             := Nil
::aProdutos             := Nil
::aVeiculos             := Nil
::aLogistic             := Nil

::logisticConstraints   := Nil
::legislationProfile    := Nil
::depots                := Nil
::operations            := Nil
::products              := Nil
::sites                 := Nil
::vehicleTypes          := Nil
::vehicles              := Nil

FwFreeObj(::callback )
FwFreeObj(::oPlanning )
FwFreeObj(::oParams )
FwFreeObj(::oRestClient )

FwFreeObj(Self)
Return

//-----------------------------------------------------------------
/*/{Protheus.doc} TMSMAPSTAT()
Fun��o para obter o status da integra��o da Rota Inteligente

@author Caio Murakami
@since 06/08/2019
@version 1.0
/*/
//--------------------------------------------------------------------
Function TMSMAPSTAT( cFilOri , cViagem  , cTipoRet )
Local aArea     := GetArea()
Local xRet      := "" 
Local cQuery    := ""
Local cAliasQry := ""
Local lRotaInt  := SuperGetMV("MV_ROTAINT",,.F.)
Local cApi      := ""
Local cStatus   := ""
Local lRetorno  := .F. 

Static _oStatDLU    

Default cFilOri     := DTQ->DTQ_FILORI
Default cViagem     := DTQ->DTQ_VIAGEM
Default cTipoRet    := "1" //-- 1=Cor;2=N�meros

If lRotaInt
    xRet    := Iif(cTipoRet == "1" , "BR_BRANCO" , 1 ) //-- N�o possui integra��o

    If _oStatDLU == Nil
        _oStatDLU   := FWPreparedStatement():New()

        cQuery  := " SELECT TOP 1 R_E_C_N_O_ DLURECNO "
        cQuery  += " FROM " + RetSQLName("DLU") + " DLU "
        cQuery  += " WHERE DLU_FILIAL   = ? "
        cQuery  += " AND DLU_FILENT     = ? "
        cQuery  += " AND DLU_ENTIDA     = ? "    
        cQuery  += " AND DLU_CHVENT     = ? "
        cQuery  += " AND DLU.D_E_L_E_T_ = '' "
        cQuery  += " ORDER BY DLU_CODIGO DESC "

        cQuery      := ChangeQuery(cQuery)    
        _oStatDLU:SetQuery(cQuery)    

    EndIf

    _oStatDLU:SetString(1,xFilial("DLU"))
    _oStatDLU:SetString(2,xFilial("DTQ"))
    _oStatDLU:SetString(3,"DTQ")
    _oStatDLU:SetString(4,cFilOri + cViagem)

    cQuery  := _oStatDLU:GetFixQuery() //-- Retorna querie do objeto

    cAliasQry   := GetNextAlias()
    dbUseArea(.T., "TOPCONN", TCGENQRY(,,cQuery), cAliasQry, .F., .T.)
    
    TcSetField(cAliasQry,"R_E_C_N_O_"  ,"N",10,0)

    While (cAliasQry)->(!Eof() )
        DLU->( dbGoTo((cAliasQry)->DLURECNO) )

        cApi        := Upper( RTrim( DLU->DLU_API ) )
        cStatus     := DLU->DLU_STATUS
        lRetorno    := Iif(Empty(DLU->DLU_RETORN),.F.,.T.)
      
        If cStatus == "2"
            xRet    := Iif(cTipoRet=="1","BR_VERMELHO",8)  //-- Erro
             If cApi == "PLANNINGSOLUTION"  .Or. cApi == "PLANNINGGETSOLUTION"          
                xRet    := Iif(cTipoRet=="1","BR_AMARELO",2) //-- Aguardando retorno planning              
            ElseIf cApi == "TRIPSOLUTION" .Or. cApi == "TRIPGETSOLUTION"              
                xRet    := Iif(cTipoRet=="1","BR_AZUL_CLARO",4) //-- Aguardando retorno trip
            EndIf
        Else
            If cApi == "PLANNINGPOST" 
                xRet    := Iif(cTipoRet=="1","BR_AMARELO",2) //-- Aguardando retorno planning
            ElseIf cApi == "PLANNINGSOLUTION" .Or. cApi == "PLANNINGGETSOLUTION"
                If !lRetorno
                    xRet    := Iif(cTipoRet=="1","BR_AMARELO",2) //-- Aguardando retorno planning
                Else
                    xRet    := Iif(cTipoRet=="1","BR_LARANJA",3) //-- Aguardando envio trip
                EndIf
            ElseIf cApi == "TRIPPOST" 
                xRet    := Iif(cTipoRet=="1","BR_AZUL_CLARO",4) //-- Aguardando retorno trip
            ElseIf cApi == "TRIPSOLUTION" .Or. cApi == "TRIPGETSOLUTION"    
                 If !lRetorno
                    xRet    := Iif(cTipoRet=="1","BR_AZUL_CLARO",4) //-- Aguardando retorno trip
                Else
                    xRet    := Iif(cTipoRet=="1","BR_AZUL",5) //-- Aguardando envio pedagio
                EndIf

            ElseIf cApi == "CALCULATETOLLS"
                 If !lRetorno
                    xRet    := Iif(cTipoRet=="1","BR_VERDE_ESCURO",6) //-- Aguardando retorno pedagio
                Else
                    xRet    := Iif(cTipoRet=="1","BR_VERDE",7)   //-- Integra��o realizada
                EndIf
            EndIf

        EndIf

        (cAliasQry)->(dbSkip())
    EndDo

    (cAliasQry)->(dbCloseArea())
EndIf

RestArea(aArea)
Return xRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TMSA046Leg
Mostra Legenda Dos Status do Roteirizador
@author Caio Murakami
@since  07/08/2019
@version P12
/*/
//-------------------------------------------------------------------
Function TMSA046Leg( cAlias, nModulo , nOpc , lRetArr  )

Local aLegenda := {}

Default cAlias  := ""
Default nModulo := 32
Default nOpc    := 3
Default lRetArr := .F. 

Aadd(aLegenda,{"BR_BRANCO"	        , STR0015 })                    //-- N�o possui integra��o com roteirizador
Aadd(aLegenda,{"BR_AMARELO"	        , STR0017 + " " + STR0006 })    //-- Aguardando retorno planning
Aadd(aLegenda,{"BR_LARANJA"	        , STR0018 + " " + STR0007 })    //-- Aguardando envio trip
Aadd(aLegenda,{"BR_AZUL_CLARO"	    , STR0017 + " " + STR0007 })	//-- Aguardando retorno trip
Aadd(aLegenda,{"BR_AZUL"	        , STR0018 + " " + STR0008 + "/" + STR0009 })    //-- Aguardando envio tolls / pedagio
Aadd(aLegenda,{"BR_VERDE_ESCURO"	, STR0017 + " " + STR0008 + "/" + STR0009 })	//-- Aguardando retorno tolls / pedagio
Aadd(aLegenda,{"BR_VERDE"	        , STR0019 })	                //-- Integra��o realizada com sucesso.
Aadd(aLegenda,{"BR_VERMELHO"        , STR0016 })	                //-- Erro de integra��o.


If Len(aLegenda) > 0 .And. !lRetArr
    BrwLegenda( STR0021 , STR0021 , aLegenda )  //-- Rota Inteligente
EndIf
				
Return Iif( lRetArr,aLegenda,.T. )

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCredential()
Busca Token

@author Caio Murakami
@since 10/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function GetAccessTok()
Local cURL          := "https://lbslocal-prod.apigee.net"
Local cEvent        := "/trip/v1/problems/"
Local cTypeEvent    := "POST"
Local oRotaInt      := Nil
Local cToken        := ""
Local cPlanResult   := ""
Local cTripResult   := ""
local aRet          := {}
Local lRet          := .T. 
Local aSites        := {}

oRotaInt    := TMSBCARotaInt():New(cURL,cEvent,cTypeEvent, DTQ->DTQ_FILORI , DTQ->DTQ_VIAGEM , .T. )

FwMsgRun( , { || cToken := oRotaInt:Token() }       , STR0003 , STR0004  ) //-- Processando  "Obtendo Token"

If !Empty(cToken)
    FwMsgRun( , { || aRet   := oRotaInt:PlanningPost()} , STR0003 , STR0005 + " - " + STR0006 ) //-- "Envio da Planning"

    If Len(aRet) > 0 .And. aRet[1]
        While AllTrim( Upper(cPlanResult) ) <> "SOLVED"
            FwMsgRun( , { || cPlanResult := oRotaInt:PlanGetJob() }, STR0003, STR0010 + " " + STR0006 + " - " + cPlanResult ) //-- Resultado planning
        EndDo

        FwMsgRun( , { || oRotaInt:PlaningSolution()} , STR0003 , STR0011 + " " + STR0006  ) //--  "Solu��o da Planning"
        FwMsgRun( , { || lRet:= oRotaInt:TripPost()} , STR0003 , STR0005 + " - " + STR0007  ) //-- "Enviando Trip 

        If lRet
            While AllTrim( Upper(cTripResult) ) <> "SOLVED"
                FwMsgRun( , { || cTripResult    := oRotaInt:TripGetJob() } , STR0003 , STR0012 + " - " + STR0007  + " " + cTripResult ) //-- Aguardando status Trip
            EndDo
            
            FwMsgRun( , { || oRotaInt:TripSolution() }, STR0003 , STR0011 + " - " + STR0007  ) //-- Solu��o Trip
            FwMsgRun( , { || oRotaInt:calculateTolls() } , STR0003 , STR0005 + " - " + STR0008  ) //-- Envio de dados - Tolls
            FwMsgRun( , { || oRotaInt:UpdatePedagio() }  , STR0003 , STR0014 + " - " + STR0009  ) //-- "Atualizando pedagio"
        EndIf        

        If lRet
            aSites     := aClone( oRotaInt:GetSites() )
            
            TMSAO51(DTQ->DTQ_FILORI,DTQ->DTQ_VIAGEM, aSites)
        EndIf
    EndIf
EndIf

oRotaInt:Destroy()

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} RotaIntPrc()
Fun��o para Processamento da Rota Inteligente

@author Katia
@since 13/06/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Function RotaIntPrc(nMenu)

Local aArea := GetArea()
Local cChvEnt:= PadR((DTQ->DTQ_FILORI + DTQ->DTQ_VIAGEM), Len(DLU->DLU_CHVENT) )  
Local cSeek  := xFilial("DLU") + xFilial('DTQ') + 'DTQ' + cChvEnt

Default nMenu := 1

If nMenu == 1 //Integra
    If DTQ->DTQ_STATUS <> StrZero(1,Len(DTQ->DTQ_STATUS)) //Em Aberto
        HELP(' ',1,'Help' ,,STR0002,3,1) //-- 'A integra��o com a Rota Inteligente s� � permitida para viagem com status Em Aberto.'
        Return .F.
    EndIf

    //------ Temporario para o teste..... 
    DLU->(DbSetOrder(2))   
    If DLU->( DbSeek( cSeek ) )
        Do While !DLU->(Eof()) .And. DLU->(DLU_FILIAL+DLU_FILENT+DLU_ENTIDA+DLU_CHVENT) == cSeek
            RecLock('DLU',.F.)
            DLU->(DbDelete())
            MsUnLock()  
            
            DLU->(dbSkip())  
        EndDo
    EndIf

    GetAccessTok()

Else   //Consulta
    DLU->(DbSetOrder(2))   
    If DLU->( DbSeek( cSeek ) )
        TMSAO50()
    Else
        Help(" ", 1, "REGNOIS", , STR0001 , 2, 1) //-- "N�o h� dados de Controle de Integra��o Rota Inteligente para essa Viagem."
    EndIf    
EndIf

RestArea(aArea)
Return
