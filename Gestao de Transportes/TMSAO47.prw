#include 'protheus.ch'
#include 'fileio.ch'

//-----------------------------------------------------------------
/*/{Protheus.doc} TMSBCAHere()
Classe criada para comunica��o com Here

@author Katia
@since 07/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
CLASS TMSBCAHere

    DATA oRestClient    As Object
    DATA aHeader        As Array
   
    DATA cURL           As Character
    DATA cEvent         As Character
    DATA cTypeEvent     As Character
    DATA cApp_id        As Character
    DATA cApp_code      As Character    
    DATA cStreet        As Character   
    DATA cState         As Character   
    DATA cCity          As Character  
    DATA cDistrict      As Character   
    DATA cPostalCode    As Character   
    DATA cCountry       As Character   
    
    METHOD New()                    Constructor   
    METHOD SetHeader()
    METHOD CleanDate()

    //-- M�todos GeoCoding
    METHOD GetCode()
    METHOD GetAppId()
    METHOD GetAppCode()

    //-- Destr�i classe e objetos
    METHOD Destroy()

END CLASS

//-----------------------------------------------------------------
/*/{Protheus.doc} New()
M�todo construtor da classe

@author Katia
@since 07/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD New( cURL , cEvent , cTypeEvent , cApp_id , cApp_code, cStreet, cState, cCity, cDistrict, cPostalCode, cCountry) CLASS TMSBCAHere

Default cURL         := "http://geocoder.api.here.com"  //https://geocoder.api.here.com/6.2/
Default cEvent       := "/6.2/geocode.json"
Default cTypeEvent   := "GET"
Default cApp_id       := ::GetAppId()
Default cApp_code     := ::GetAppCode()

Default cStreet      := ""   //Endere�o
Default cState       := ""   //Estado
Default cCity        := ""   //Municipio
Default cDistrict    := ""   //Bairro
Default cPostalCode  := ""   //CEP
Default cCountry     := ""   //Pais     

::cURL	            := cURL
::cEvent            := cEvent
::cTypeEvent        := cTypeEvent
::cApp_id           := cApp_id
::cApp_code         := cApp_code
::cStreet           := cStreet
::cState            := cState 
::cCity             := cCity
::cDistrict         := cDistrict
::cPostalCode       := cPostalCode
::cCountry          := cCountry          
::oRestClient       := FwRest():New(cURL)

::oRestClient:SetPath(cEvent)

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} SetHeader()
Seta cabe�alho 

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD SetHeader(aHeader) CLASS TMSBCAHere

Default aHeader     := {} 

fwFreeObj(::aHeader)

::aHeader   := aClone(aHeader)

FwFreeObj(aHeader)
Return

//-----------------------------------------------------------------
/*/{Protheus.doc} Destroy()
Destroi objeto

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD Destroy() CLASS TMSBCAHere

::CleanDate()

::aHeader               := Nil
::oRestClient           := Nil

FwFreeObj(Self)
Return


//-----------------------------------------------------------------
/*/{Protheus.doc} CleanDate()
Limpa dados

@author Caio Murakami
@since 06/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD CleanDate() CLASS TMSBCAHere

FwFreeObj(::aHeader )
FwFreeObj(::oRestClient)

::aHeader               := {}
::oRestClient           := FwRest():New(::cURL)

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} GetCode()
Envia o pedido de Geocodigo para localizar as coordenadas geogr�ficas
com base na entrada livre.

@author Katia
@since 07/06/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetCode() CLASS TMSBCAHere
Local aHeaderStr    := {}
Local cJSON         := ""
Local aRet          := {} 
Local oResult       := Nil
Local cResult       := ""
Local cArquivo      := "maplink.log"
Local oObj          := JsonObject():New()
Local aGeoCode      := {}

Aadd(aHeaderStr, "Content-Type: application/x-www-form-urlencoded" )

//--- Exemplos
//cJson := "app_id={YOUR_APP_ID}&app_code={YOUR_APP_CODE}&searchtext=425+W+Randolph+Chicago"
//cJson := "app_id={YOUR_APP_ID}&app_code={YOUR_APP_CODE}&city=Brazil:Sao+Paulo&state=SP"
cSearchText:= RTrim(::cStreet) + " "
cSearchText+= RTrim(::cState)+ " "
cSearchText+= RTrim(::cCity)+ " "
cSearchText+= RTrim(::cDistrict)+ " "
cSearchText+= RTrim(::cCountry)+ " "
cSearchText+= RTrim(::cPostalCode)
 
cSearchText:= StrTran(cSearchText, " ", "+")
cSearchText:= FwNoAccent(cSearchText)

cJSON   := "?app_id=" + RTrim(::cApp_id) + "&" 
cJSON   += "app_code=" + RTrim(::cApp_code) + "&" 
cJSON   += "searchtext=" + cSearchText

::oRestClient:SetPath(::cEvent + cJson)
lRet    := ::oRestClient:Get() 

If lRet
    cResult:= ::oRestClient:GetResult()

    oJson:= JsonObject():new()
    oJson:fromJson(DecodeUtf8(cResult))
    //////////oJson:GetProperties()  ---ver o que tem no objeto
    //oJson["Response"]
    //oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"][1]["Latitude"]
    //oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"][1]["Longitude"]
    If oJson["Response"] <> Nil .Or. !Empyt(cResult)
        If Len(oJson["Response"]["View"])>0
            If oJson["Response"]["View"][1]<>Nil
                If Len(oJson["Response"]["View"][1]["Result"])>0
                    If oJson["Response"]["View"][1]["Result"][1]["Location"]<>Nil
                        If Len(oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"])>0
                            If oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"][1]<>Nil
                                Aadd(aGeoCode,oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"][1]["Latitude"])
                                Aadd(aGeoCode,oJson["Response"]["View"][1]["Result"][1]["Location"]["NavigationPosition"][1]["Longitude"])
                             EndIf
                        EndIf
                    EndIf
                EndIf
            EndIf
        EndIf
    EndIf
Else
     MsgStop(::oRestClient:GetLastError())
EndIf

Return aGeoCode

//-----------------------------------------------------------------
/*/{Protheus.doc} GetAppId()
captura ultimo app id - GeoCode

@author Caio Murakami
@since 08/08/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetAppId() CLASS TMSBCAHere
Local cRet      := ""
Local cQuery    := ""
Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()

cQuery  := " SELECT DLV_APPID "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL   = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL     = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' " 

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    cRet     := (cAliasQry)->DLV_APPID
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())

RestArea(aArea)
Return cRet

//-----------------------------------------------------------------
/*/{Protheus.doc} GetAppCode()
captura ultimo code id - GeoCode

@author Caio Murakami
@since 08/08/2019
@version 1.0
/*/
//--------------------------------------------------------------------
METHOD GetAppCode() CLASS TMSBCAHere
Local cRet      := ""
Local cQuery    := ""
Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()

cQuery  := " SELECT DLV_APPCOD "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL   = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL     = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' " 

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
    cRet     := (cAliasQry)->DLV_APPCOD
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->(dbCloseArea())

RestArea(aArea)
Return cRet

