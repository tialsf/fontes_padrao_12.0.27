#include "protheus.ch"
#include 'fileio.ch'

#define STR0001 "Trip"

// Alinhamento do m�todo addInLayout
#define LAYOUT_ALIGN_LEFT     1
#define LAYOUT_ALIGN_RIGHT    2
#define LAYOUT_ALIGN_HCENTER  4
#define LAYOUT_ALIGN_TOP      32
#define LAYOUT_ALIGN_BOTTOM   64
#define LAYOUT_ALIGN_VCENTER  128

// Alinhamento para preenchimento dos componentes no TLinearLayout
#define LAYOUT_LINEAR_L2R 0 // LEFT TO RIGHT
#define LAYOUT_LINEAR_R2L 1 // RIGHT TO LEFT
#define LAYOUT_LINEAR_T2B 2 // TOP TO BOTTOM
#define LAYOUT_LINEAR_B2T 3 // BOTTOM TO TOP

#define DATA_STATUS   1
#define DATA_PEDIDO   3
#define DATA_CLIENTE  4
#define DATA_LOJACLI  5
#define DATA_NOMECLI  6
#define DATA_NOMEREDZ 7
#define DATA_ENDERECO 8
#define DATA_BAIRRO   9
#define DATA_CIDADE   10
#define DATA_ESTADO   11
#define DATA_CEP      12
#define DATA_DATACHEG 13
#define DATA_HORACHEG 14
#define DATA_TIMESERV 15

#define SEM_RESTRICAO 1
#define RESTR_CALENDA 2
#define RESTR_HORARIO 3
#define RESTR_VEICULO 4

Static oWebChannel := Nil
Static oWebEngine  := Nil
Static nWebPort    := 0
Static oDlg        := Nil
Static oBrowse     := Nil
Static oGroupInfo  := Nil
Static aPedidos    := {}
Static aDestinos   := {}
Static aFieldsDest := {}
Static aFieldsPdg	 := {}
Static aOrigem     := {}
Static aFieldsOrig := {}
Static aCalcTolls	 := {} 
Static oTripResult := Nil

/*/-----------------------------------------------------------
{Protheus.doc} TMSAO51()
Montagem e visualiza��o do mapa com integra��o HERE

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Function TMSAO51( cFilOri , cViagem , aSites )
Local oSize

Default cFilOri		:= ""
Default cViagem		:= ""
Default aSites		:= {}

// Calcula as dimensoes dos objetos
oSize := FwDefSize():New( .T. )  // Com enchoicebar
// Cria Enchoice
oSize:AddObject( "MASTER", 100, 100, .T., .T. ) // Adiciona enchoice

// Dispara o calculo
oSize:Process()

// // Desenha a dialog
DEFINE MSDIALOG oDlg TITLE STR0001 FROM ;
oSize:aWindSize[1],oSize:aWindSize[2] TO ;
oSize:aWindSize[3],oSize:aWindSize[4] PIXEL STYLE NOr(WS_VISIBLE,WS_POPUP)

LoadData( cFilOri , cViagem , aSites )
WebEngine()
LayLinear()

ACTIVATE MSDIALOG oDlg

Return

/*/-----------------------------------------------------------
{Protheus.doc} LoadData()
Carrega informa��es

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function LoadData(cFilOri , cViagem , aSites)

Default cFilOri		:= ""
Default cViagem		:= ""
Default aSites		:= {} 

If Empty(aOrigem)
	aFieldsOrig := {"nomeCliente","nomeReduz","endereco","bairro","cidade","estado","cep"}
	LoadOrigem()
EndIf

aFieldsDest := {"codCliente","nomeCliente","endereco","bairro","cidade","estado","cep"}
aFieldsPdg	:= {"name" , "address" , "concession" , "price" }

LoadDestino(cFilOri , cViagem)
LoadTripData(cFilOri , cViagem)

Return

/*/-----------------------------------------------------------
{Protheus.doc} WebEngine()
Ativa engine de comunica��o WEB

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function WebEngine()
  
  // ------------------------------------------
	// Prepara o conector WebSocket
	// ------------------------------------------
	oWebChannel := TWebChannel():New()
	nWebPort    := oWebChannel:connect() // Efetua conex�o e retorna a porta do WebSocket
	
	// Verifica conex�o
	If !oWebChannel:lConnected
		MsgStop("Erro na conex�o com o WebSocket")
		Return // Aborta aplica��o
	EndIf
	ConOut(cValToChar(nWebPort))
	// ------------------------------------------
	// Define o CallBack JavaScript
	// IMPORTANTE: Este � o canal de comunica��o "vindo do Javascript para o ADVPL"
	// ------------------------------------------
	oWebChannel:bJsToAdvpl := {|self,codeType,codeContent| JsToAdvpl(self,codeType,codeContent) }

	oWebChannel:advplToJs("loadEngine", )
Return

/*/-----------------------------------------------------------
{Protheus.doc} LayLinear()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function LayLinear()

	oPanelMain := TPanel():New(0,0,Nil,oDlg,,.F.,,,CLR_WHITE,200,20)
	oPanelMain:Align := CONTROL_ALIGN_ALLCLIENT
	oPanelMain:SetCSS("QFrame{ background-color: white; }")

	//-------------------
	// Frame superior
	//-------------------
	oPanelHead := TPanel():New(0,0,Nil,oPanelMain,,.F.,,,,200,20)
	oPanelHead:Align := CONTROL_ALIGN_TOP
	oTButton := TBtnBmp2():New(0,0,26,10,'fwskin_delete_ico',,,,{||oDlg:End()},oPanelHead,,,.T. )
	oTButton:Align := CONTROL_ALIGN_RIGHT
	oSayHeader := TSay():New(4,5,{||"Entregas TMS"},oPanelHead,,,,,,.T.,,,200,10,,,,,,.T.)
	oSayHeader:SetCSS("QLabel{ font-size: 18px; }")
	oSayHeader:lTransparent := .T.

	//-------------------
	// Frame central
	//-------------------
	oPanelBody := TPanel():New(0,0,Nil,oPanelMain,,.F.,,0,0,300,300)
	oPanelBody:Align := CONTROL_ALIGN_ALLCLIENT

	oPanelLeft := TPanel():New(0,0,Nil,oPanelBody,,.F.,,0,,170,150)
	oPanelLeft:Align := CONTROL_ALIGN_LEFT

	// Itens do painel Esquerdo
	oPnLeftTop := TPanel():New(0,0,Nil,oPanelLeft,,.F.,,0,,170,180)
	oPnLeftTop:Align := CONTROL_ALIGN_TOP

	//-- Pedagio	
	oPnLeftZ := TPanel():New(0,0,Nil,oPanelLeft,,.F.,,0,,170,180)
	oPnLeftZ:Align := CONTROL_ALIGN_ALLCLIENT
		

	oPnLeftBot := TPanel():New(0,0,Nil,oPanelLeft,,.F.,,0,,170,40)
	oPnLeftBot:Align := CONTROL_ALIGN_BOTTOM 
		
	CriaBrwPed(oPnLeftTop)
	CriaDetEnt(oPnLeftBot)
	CriaBrwPdg(oPnLeftZ)

	// ------------------------------------------
	// Cria navegador embedado
	// ------------------------------------------
	oPanelCent := TPanel():New(0,0,Nil,oPanelBody,,.F.,,0,,170,150)
	oPanelCent:Align := CONTROL_ALIGN_ALLCLIENT

	oWebEngine 	:= TWebEngine():New(oPanelCent, 0, 0, 100, 100,,nWebPort)
	cLink		:= AO51GetURL()
	oWebEngine:navigate(cLink)
	oWebEngine:Align := CONTROL_ALIGN_ALLCLIENT

Return

/*/-----------------------------------------------------------
{Protheus.doc} CriaBrwPed()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function CriaBrwPed(oOwner)
Local oPanel, oColumn

oPanel := TPanel():New(0,0,,oOwner,,.T.,,,,0,0)
oPanel:Align := CONTROL_ALIGN_ALLCLIENT

// Define o Browse
oBrowse := FWBrowse():New(oPanel)
oBrowse:SetDataArray(.T.)
oBrowse:SetArray(aPedidos)
oBrowse:DisableConfig(.T.)
oBrowse:DisableReport(.T.)
oBrowse:DisableLocate(.T.)
oBrowse:DisableFilter(.T.)

// Cria uma coluna de legenda
oBrowse:AddLegend({||aPedidos[oBrowse:nAt,DATA_STATUS]==SEM_RESTRICAO}, "GREEN"  ,"Entrega Permitida")
//oBrowse:AddLegend({||aPedidos[oBrowse:nAt,DATA_STATUS]==RESTR_CALENDA}, "RED"    ,"Restri��o por Calend�rio")
//oBrowse:AddLegend({||aPedidos[oBrowse:nAt,DATA_STATUS]==RESTR_HORARIO}, "YELLOW" ,"Restri��o por Hor�rio")
//oBrowse:AddLegend({||aPedidos[oBrowse:nAt,DATA_STATUS]==RESTR_VEICULO}, "GRAY"   ,"Restri��o por Ve�culo")

// Adiciona as colunas do Browse
oColumn := FWBrwColumn():New()
oColumn:SetData({||aPedidos[oBrowse:nAt,DATA_PEDIDO]})
oColumn:SetTitle("Documento")
oColumn:SetSize(6)
oBrowse:SetColumns({oColumn})

oColumn := FWBrwColumn():New()
oColumn:SetData({||aPedidos[oBrowse:nAt,DATA_NOMEREDZ]})
oColumn:SetTitle("Cliente Destinat�rio")
oColumn:SetSize(30)
oBrowse:SetColumns({oColumn})

oBrowse:SetChange( {|| BrwPedChange() })
oBrowse:Activate()
Return oPanel

/*/-----------------------------------------------------------
{Protheus.doc} CriaBrwPdg()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function CriaBrwPdg(oOwner)
Local oPanel, oColumn , oGroupPDg , oSayPdg , oPanelAux
Local nCount 	:= 1 
Local aAux 		:= {}
Local nValPdg	:= 0 

Static oBrowsePdg := Nil

For nCount := 1 To Len(aCalcTolls)

	Aadd( aAux , { aCalcTolls[nCount,1][2] ,;
								aCalcTolls[nCount,2][2] ,;
								aCalcTolls[nCount,3][2] ,;
								aCalcTolls[nCount,4][2] ,;
								aCalcTolls[nCount,5][2] ,;
								aCalcTolls[nCount,6][2]			  } )

	nValPdg	+= aCalcTolls[nCount,6][2]	

Next nCount 

oPanel := TPanel():New(0,0,,oOwner,,.F.,,0,,170,150)
oPanel:Align := CONTROL_ALIGN_TOP

// Define o Browse
oBrowsePdg := FWBrowse():New(oPanel)
oBrowsePdg:SetDataArray(.T.)
oBrowsePdg:SetArray(aAux)
oBrowsePdg:DisableConfig(.T.)
oBrowsePdg:DisableReport(.T.)
oBrowsePdg:DisableLocate(.T.)
oBrowsePdg:DisableFilter(.T.)

// Adiciona as colunas do Browse
oColumn := FWBrwColumn():New()
oColumn:SetData({||aAux[oBrowsePdg:nAt,1]})
oColumn:SetTitle("Pedagio")
oColumn:SetSize(15)
oBrowsePdg:SetColumns({oColumn})

oColumn := FWBrwColumn():New()
oColumn:SetData({|| "R$ " + cValToChar( aAux[oBrowsePdg:nAt,6]) } )  
oColumn:SetTitle("Valor")
oColumn:SetSize(6)
oBrowsePdg:SetColumns({oColumn})

oBrowsePdg:Activate()

oPanelAux := TPanel():New(0,0,,oOwner,,.F.,,0,,170,15)
oPanelAux:Align := CONTROL_ALIGN_BOTTOM

oGroupPDg := TGroup():New(0,0,0,0,'Pedagio',oPanelAux,,,.F.)
oGroupPDg:Align := CONTROL_ALIGN_BOTTOM

oSayPdg := TSay():New( 0, 0, {|| "<b>Valor total: R$ </b>  " + cValToChar(nValPdg) },oGroupPDg, , , , , , .T., , , 0,10, , , , , , .T.)
oSayPdg:SetCSS("QLabel { padding-left: 8px; }")

Return oPanel

/*/-----------------------------------------------------------
{Protheus.doc} CriaDetEnt()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function CriaDetEnt(oOwner)
Local oLayGrid0, oLayGrid1, oSay

// Cria o grupo de informa��es
oGroupInfo := TGroup():New(0,0,0,0,'Dados da Entrega',oOwner,,,.T.)
oGroupInfo:Align := CONTROL_ALIGN_ALLCLIENT

// Cria o layout principal
oLayGrid0 := TGridLayout():New(oGroupInfo,CONTROL_ALIGN_ALLCLIENT)

// Primeira linha - Cliente
oSay := TSay():New( 0, 0, {|| "<b>Cliente:</b>  " + aPedidos[oBrowse:nAt,DATA_CLIENTE] + "-" + aPedidos[oBrowse:nAt,DATA_LOJACLI] + "  " + RTrim(aPedidos[oBrowse:nAt,DATA_NOMECLI]) },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid0:addInLayout(oSay, 1, 1,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)

// Segunda linha - Endere�o
//oLayGrid1 := TGridLayout():New(oGroupInfo, LAYOUT_LINEAR_L2R)
oSay := TSay():New( 0, 0, {|| "<b>Endere�o:</b> " + RTrim(aPedidos[oBrowse:nAt,DATA_ENDERECO]) },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid0:addInLayout(oSay, 2, 1,,,LAYOUT_ALIGN_TOP)
Aadd(oGroupInfo:aControls,oSay)

// Terceira linha - Bairro e Cidade
oLayGrid1 := TGridLayout():New(oGroupInfo, LAYOUT_LINEAR_L2R)
oSay := TSay():New( 0, 0, {|| "<b>Bairro:</b> " + RTrim(aPedidos[oBrowse:nAt,DATA_BAIRRO]) },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid1:addInLayout(oSay, 1, 1,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oSay := TSay():New( 0, 0, {|| "<b>Cidade:</b> " + RTrim(aPedidos[oBrowse:nAt,DATA_CIDADE]) },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oLayGrid1:addInLayout(oSay, 1, 2,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oLayGrid0:addInLayout(oLayGrid1, 3, 1,,,LAYOUT_ALIGN_TOP)

// Terceira linha - Cidade, Estado e CEP
oLayGrid1 := TGridLayout():New(oGroupInfo, LAYOUT_LINEAR_L2R)
oSay := TSay():New( 0, 0, {|| "<b>Estado:</b> " + aPedidos[oBrowse:nAt,DATA_ESTADO] },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid1:addInLayout(oSay, 1, 2,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oSay := TSay():New( 0, 0, {|| "<b>CEP:</b> " + aPedidos[oBrowse:nAt,DATA_CEP] },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oLayGrid1:addInLayout(oSay, 1, 3,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oLayGrid0:addInLayout(oLayGrid1, 4, 1,,,LAYOUT_ALIGN_TOP)

// Quarta linha - Chegada, Tempo e Sa�da
/*oLayGrid1 := TGridLayout():New(oGroupInfo, LAYOUT_LINEAR_L2R)
oSay := TSay():New( 0, 0, {|| "<b>Chegada Prevista:</b> " + DtoC(aPedidos[oBrowse:nAt,DATA_DATACHEG]) + " " + DtoC(aPedidos[oBrowse:nAt,DATA_HORACHEG])  },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid1:addInLayout(oSay, 2, 1,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oLayGrid0:addInLayout(oLayGrid1, 5, 1,,,LAYOUT_ALIGN_TOP)

oLayGrid1 := TGridLayout():New(oGroupInfo, LAYOUT_LINEAR_L2R)
oSay := TSay():New( 0, 0, {|| "<b>Sa�da Prevista:</b> " + DtoC(aPedidos[oBrowse:nAt,DATA_DATACHEG]) + " " + IntToHora(HoraToInt(aPedidos[oBrowse:nAt,DATA_HORACHEG],2)+HoraToInt(aPedidos[oBrowse:nAt,DATA_TIMESERV],4),2) + ":00" },oGroupInfo, , , , , , .T., , , 0,10, , , , , , .T.)
oSay:SetCSS("QLabel { padding-left: 8px; }")
oLayGrid1:addInLayout(oSay, 1, 1,,,LAYOUT_ALIGN_VCENTER)
Aadd(oGroupInfo:aControls,oSay)
oLayGrid0:addInLayout(oLayGrid1, 6, 1,,,LAYOUT_ALIGN_TOP)*/

Return oGroupInfo

/*/-----------------------------------------------------------
{Protheus.doc} BrwPedChange()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function BrwPedChange()
Local nX

If (oGroupInfo != Nil)
	For nX := 1 To Len(oGroupInfo:aControls)
		oGroupInfo:aControls[nX]:Refresh()
	Next nX
EndIf
If (oWebChannel != Nil)
	OnBrwPedChange()
EndIf

Return

/*/-----------------------------------------------------------
{Protheus.doc} JsToAdvpl()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
// ------------------------------------------
// Esta fun��o recebera todas as chamadas vindas do Javascript
// atrav�s do m�todo dialog.jsToAdvpl(), exemplo:
// dialog.jsToAdvpl("page_started", "Pagina inicializada");
// ------------------------------------------
//-----------------------------------------------------------------------------
Static Function JsToAdvpl(self,codeType,codeContent)
 
If valType(codeType) == "C"
	If codeType == "pageStarted"
		OnPageStarted()
	ElseIf codeType == "markerClick"
		OnMarkerClick(codeContent)
	EndIf
EndIf
 
Return

/*/-----------------------------------------------------------
{Protheus.doc} OnPageStarted()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function OnPageStarted()
Local oJsonObj   := JsonObject():New()
Local oJsonAux   := Nil
Local nX         := 0
Local nY         := 0
Local oJsonPosic := Nil

// Informa��es da origem
oJsonObj["origem"] 	:= JsonObject():New()
oJsonAux 						:= oJsonObj:GetJsonObject("origem")
For nY := 1 To Len(aFieldsOrig)
	oJsonAux[aFieldsOrig[nY]] := aOrigem[nY]
Next nY
oJsonAux["position"] := JsonObject():New()
oJsonAux 				:= oJsonAux:GetJsonObject("position")
oJsonAux["lat"] := aOrigem[Len(aOrigem),1]
oJsonAux["lng"] := aOrigem[Len(aOrigem),2]

// Informa��es dos destinos
oJsonObj["destinos"] := {}
For nX := 1 To Len(aDestinos)

	oJsonAux := JsonObject():New()
	Aadd(oJsonObj["destinos"], oJsonAux)

	For nY := 1 To Len(aFieldsDest)
		oJsonAux[aFieldsDest[nY]] := aDestinos[nX,nY]
	Next nY

	oJsonAux["position"] := JsonObject():New()
	oJsonAux["pedidos"]  := AClone(aDestinos[nX,Len(aDestinos[nX])])

	oJsonAux := oJsonAux:GetJsonObject("position")
	oJsonAux["lat"] := aDestinos[nX,Len(aDestinos[nX])-2]
	oJsonAux["lng"] := aDestinos[nX,Len(aDestinos[nX])-1]

Next nX

oJsonObj["legs"] := oTripResult["legs"]

oJsonAux := JsonObject():New()
oJsonObj["resumo"] 					:= oJsonAux
oJsonAux["totalDistance"] 			:= oTripResult["totalDistance"]
oJsonAux["totalNominalDuration"] 	:= oTripResult["totalNominalDuration"]
oJsonAux["totalSites"] 				:= Len(aDestinos)
oJsonAux["totalOperations"] 		:= Len(aPedidos)

oJsonObj["tolls"]		:= {}

For nX := 1  To Len(aCalcTolls)
	oJsonAux 				:= JsonObject():New()
	oJsonAux["name"]		:= GetInfoArr( aCalcTolls[nX], "name" )				
	oJsonAux["address"]		:= GetInfoArr( aCalcTolls[nX], "address" )	
	oJsonAux["concession"]	:= GetInfoArr( aCalcTolls[nX], "concession" )	
	
	For nY := 1 To Len(aFieldsPdg)
		oJsonAux[aFieldsPdg[nY]] := aCalcTolls[nX,nY]
	Next nY

	oJsonPosic := JsonObject():New()
	oJsonPosic["lat"]	:= GetInfoArr( aCalcTolls[nX], "latitude" )	
	oJsonPosic["lng"]	:= GetInfoArr( aCalcTolls[nX], "longitude" )	

	oJsonAux["position"]  := oJsonPosic
	oJsonAux["price"]			:= GetInfoArr( aCalcTolls[nX], "price" )

	Aadd(oJsonObj["tolls"], oJsonAux)

Next nX

oWebChannel:advplToJs("initMap")
oWebChannel:advplToJs("initData", oJsonObj:toJSON() )
oWebChannel:advplToJs("tolls"	, oJsonObj:toJSON()) 

FreeObj(oJsonObj)
	
Return

/*/-----------------------------------------------------------
{Protheus.doc} OnMarkerClick()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function OnMarkerClick(codeContent)
Local oJsonObj   := JsonObject():New()
Local nX

oJsonObj:fromJSON(codeContent)
If ((nX := Ascan(aPedidos,{|x| x[DATA_PEDIDO] == oJsonObj["pedido"]})) > 0)
	oBrowse:GoTo(nX, .T.)
EndIf
FreeObj(oJsonObj)

If (oGroupInfo != Nil)
	For nX := 1 To Len(oGroupInfo:aControls)
		oGroupInfo:aControls[nX]:Refresh()
	Next nX
EndIf

Return

/*/-----------------------------------------------------------
{Protheus.doc} OnBrwPedChange()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function OnBrwPedChange()
	oWebChannel:advplToJs("changeRow", aPedidos[oBrowse:nAt,DATA_CLIENTE] + aPedidos[oBrowse:nAt,DATA_LOJACLI])
Return

/*/-----------------------------------------------------------
{Protheus.doc} LoadOrigem()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function LoadOrigem()
Local aArrFilAtu := FWArrFilAtu()

SM0->(MsGoTo(aArrFilAtu[12]))

aOrigem := { SM0->M0_NOMECOM,SM0->M0_NOME,SM0->M0_ENDENT,SM0->M0_BAIRENT,SM0->M0_CIDENT,SM0->M0_ESTENT,SM0->M0_CEPENT }

DAR->(DbSetOrder(1))
If DAR->(DbSeek(xFilial("DAR")+Space(FWSizeFilial())+"SM0"+cFilAnt))
	Aadd(aOrigem, { Val( StrTran(DAR->DAR_LATITU,",",".") ), Val( StrTran(DAR->DAR_LONGIT,",",".") ) })
Else
	Aadd(aOrigem, { 0, 0 })
EndIf

Return

/*/-----------------------------------------------------------
{Protheus.doc} LoadDestino()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function LoadDestino(cFilOri , cViagem , aSites )
Local cAliasQry  	:= ""
Local nPos       	:= 0
Local oDadosViag	:= Nil
Local aClientes		:= {} 
Local aDocs			:= {} 
Local nCount		:= 1
Local cDoc			:= ""
Local cCodCli		:= ""
Local cLojCli		:= ""
Local cNome			:= ""
Local cEnd			:= ""
Local cBairro		:= ""
Local cEst			:= ""
Local cCEP			:= ""
Local cMun			:= ""
Local aLatitude	:= {}
Local nPos			:= 0
Local cServic		:= ""
Local cDoc			:= ""
Local cSerie		:= ""
Local cDocTms		:= ""
Local cSeqEnd		:= ""

Default cFilOri		:= ""
Default cViagem		:= ""

aPedidos  := {}
aDestinos := {}

oDadosViag	:= TMSBCADadosTMS():New(cFilOri,cViagem,3,.F.)
oDadosViag:AddDocs( cFilOri, cViagem )			
aDocs 	:= oDadosViag:GetDocs()

For nCount := 1 To Len(aDocs)
	
	cDoc		:= GetInfoArr(aDocs[nCount],"DT6_DOC")
	cSerie		:= GetInfoArr(aDocs[nCount],"DT6_SERIE") 
	cDocTms		:= GetInfoArr(aDocs[nCount],"DT6_DOCTMS")
	cServic		:= Iif( cSerie == "COL" ,"COLLECTION" , "DELIVERY" )
	aLatitude	:= {0,0}
	cCodCli		:= GetInfoArr(aDocs[nCount],"CLIDES",3)
	cLojCli		:= GetInfoArr(aDocs[nCount],"LOJDES",3)
	cNome		:= GetInfoArr(aDocs[nCount],"DESNREDUZ",3)
	cEnd		:= GetInfoArr(aDocs[nCount],"DESEND",3)
	cBairro		:= GetInfoArr(aDocs[nCount],"DESBAIRRO",3)
	cEst		:= GetInfoArr(aDocs[nCount],"DESEST",3)
	cCEP		:= GetInfoArr(aDocs[nCount],"DESCEP",3)
	cMun		:= GetInfoArr(aDocs[nCount],"DESMUN",3)
	cSeqEnd		:= GetInfoArr(aDocs[nCount],"SQEDES",3)

	If cDocTMS == "6" //-- Devolu��o
		//-- Remetente
		AddDest(@aDestinos , GetInfoArr(aDocs[nCount],"DT6_CLIREM") , GetInfoArr(aDocs[nCount],"DT6_LOJREM") , GetInfoArr(aDocs[nCount],"REMNREDUZ") ,  GetInfoArr(aDocs[nCount],"REMEND") ,;
				 GetInfoArr(aDocs[nCount],"REMBAIRRO") , GetInfoArr(aDocs[nCount],"REMMUN") , GetInfoArr(aDocs[nCount],"REMEST") , GetInfoArr(aDocs[nCount],"REMCEP") ,;
				 cDoc , cSerie , cSeqEnd )
	EndIf

	//-- Destinat�rio
	AddDest( @aDestinos , cCodCli , cLojCli , cNome , cEnd , cBairro , cMun , cEst , cCEP , cDoc , cSerie , cSeqEnd)

	Aadd(aPedidos, { SEM_RESTRICAO , cValToChar(nCount), cDoc , cCodCli , cLojCli , cNome ,cNome ,cEnd , cBairro , cMun , cEst , cCEP ,DDATABASE , Time() , cServic } )

Next nCount


Return

/*/-----------------------------------------------------------
{Protheus.doc} AddDest()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 15/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function AddDest( aDestinos , cCodCli , cLojCli , cNome , cEnd , cBairro , cMun , cEst , cCEP , cDoc , cSerie , cSeqEnd )
Local nPos		:= 0 
Local aLatitude	:= {} 

Default	aDestinos	:= {}
Default cCodCli		:= ""
Default	cLojCli		:= ""
Default cNome		:= ""
Default	cEnd		:= ""
Default cBairro		:= ""
Default cMun		:= ""
Default cEst		:= ""
Default cCEP		:= ""
Default cDoc		:= ""
Default	cSerie		:= ""
Default cSeqEnd		:= ""

nPos	:= aScan(aDestinos , {|x| x[1] == cCodCli + cLojCli })

If nPos == 0
	aLatitude	:= GetCordinate("SA1",xFilial("SA1") , cCodCli + cLojCli , cSeqEnd )
	Aadd( aDestinos , { cCodCli + cLojCli , cNome , cEnd , cBairro , cMun , cEst , cCEP , aLatitude[1] , aLatitude[2]  , { cDoc + "/" +  cSerie } } )	
Else
	Aadd( aDestinos[nPos][10] ,  cDoc + "/" +  cSerie )
EndIf

Return

/*/-----------------------------------------------------------
{Protheus.doc} GetCordinate()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function GetCordinate( cAliasDAR , cFilEnt , cCodEnt , cSeqEnd ) 
Local oCordinate    := Nil
Local cAliasQry     := GetNextAlias()
Local nValLatitu    := 0 
Local nValLongit    := 0 
Local cAliasAux		:= "DUL"
Local cFilAux		:= xFilial("DUL")

Default cAliasDAR   := ""
Default cFilEnt     := ""
Default cCodEnt     := ""
DEfault cSeqEnd		:= ""

cQuery  := " SELECT DAR_LATITU, DAR_LONGIT "
cQuery  += " FROM "  +RetSQLName("DAR") + " DAR "
cQuery  += " WHERE DAR_FILIAL   = '" + xFilial("DAR") + "' "

If !Empty(cSeqEnd)
    cQuery  += " AND DAR_FILENT     IN ('" + cFilEnt + "' , '" + cFilAux + "' ) "
    cQuery  += " AND DAR_ENTIDA     IN ('" + cAliasDAR + "' , '" + cAliasAux + "' ) "
    cQuery  += " AND DAR_CODENT     = '" + RTrim(cSeqEnd) + "'  "
Else
    cQuery  += " AND DAR_FILENT     = '" + cFilEnt + "' "
    cQuery  += " AND DAR_ENTIDA     = '" + cAliasDAR + "' "
    cQuery  += " AND DAR_CODENT     = '" + RTrim(cCodEnt) + "' "
EndIf

cQuery  += " AND DAR.D_E_L_E_T_ = '' "
 
cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
   
    nValLatitu  := Val( StrTran((cAliasQry)->DAR_LATITU,",",".") )
    nValLongit  := Val( StrTran((cAliasQry)->DAR_LONGIT,",",".") )
    
    (cAliasQry)->( dbSkip() )
EndDo

(cAliasQry)->( dbCloseArea() )

Return { nValLatitu ,nValLongit }

/*/-----------------------------------------------------------
{Protheus.doc} LoadTripData()

Uso: TMSAO51

@sample
//ViewDef()

@author Caio Murakami   
@since 01/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function LoadTripData(cFilOri , cViagem)
Local cTripResult 	:= ""
Local oFileReader 	:= Nil
Local nHandle				:= 0 
Local cArquivo			:= ""
Local cTolls				:= ""
Local aLegs					:= {} 
Local nCount				:= 1 
Local nAux					:= 0 
Local aTolls				:= {} 
Local oTolls				:= Nil 

oTripResult 	:= JsonObject():New()
oTolls			:= JsonObject():New()
aCalcTolls	 	:= {} 

DLU->(dbSetOrder(2))
If DLU->( MsSeek( xFilial("DLU") + xFilial("DTQ") + "DTQ" + cFilOri + cViagem  ))
	While xFilial("DLU") + xFilial("DTQ") + "DTQ" + cFilOri + cViagem == RTrim( DLU->( DLU_FILIAL+DLU_FILENT+DLU_ENTIDA+DLU_CHVENT ) )

		If Upper(AllTrim(DLU->DLU_API)) == "TRIPGETSOLUTION" .Or. Upper(AllTrim(DLU->DLU_API)) == "TRIPSOLUTION"
			cTripResult		:= DLU->DLU_RETORN			
			oTripResult:fromJSON(cTripResult)
		ElseIf Upper(AllTrim(DLU->DLU_API)) == "CALCULATETOLLS" .And. !Empty( DLU->DLU_RETORN )
			cTolls	:= DLU->DLU_RETORN	
			oTolls:fromJSON(cTolls)

			aLegs		:= oTolls:GetJsonObject("legs")
			
			For nCount := 1 To Len(aLegs)

					aTolls	:= aLegs[nCount]:GetJsonObject("tolls")

					For nAux := 1 To Len(aTolls)
						Aadd( aCalcTolls , {} ) 

						Aadd(aCalcTolls[Len(aCalcTolls)] , { "name" 		, AO51NoAcent(aTolls[nAux]:GetJsonObject("name") ) } )
						Aadd(aCalcTolls[Len(aCalcTolls)] , { "address" 		, aTolls[nAux]:GetJsonObject("address") } )
						Aadd(aCalcTolls[Len(aCalcTolls)] , { "concession" 	, aTolls[nAux]:GetJsonObject("concession") } )
						Aadd(aCalcTolls[Len(aCalcTolls)] , { "latitude" 	, aTolls[nAux]:GetJsonObject("coordinates"):GetJsonObject("latitude") } )
						Aadd(aCalcTolls[Len(aCalcTolls)] , { "longitude" 	, aTolls[nAux]:GetJsonObject("coordinates"):GetJsonObject("longitude") } )
						Aadd(aCalcTolls[Len(aCalcTolls)] , { "price" 		, aTolls[nAux]:GetJsonObject("price") } )
											
					Next nAux

			Next nCount 

		EndIf

		DLU->( dbSkip() )
	EndDo
EndIf

Return

//-----------------------------------------------------------------
/*/{Protheus.doc} GetInfoArr()
Envia um problema de viagem a ser calculado

@author Caio Murakami
@since 11/09/2019
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function GetInfoArr( aAux  , cLabel , nPosSeek , nPosRet , nCount )
Local xRet      := nil 

Default aAux        := {}
Default cLabel      := ""
Default nPosSeek    := 1
Default nPosRet     := 2
Default nCount      := 1

For nCount := 1 To Len(aAux)
    If AllTrim( aAux[nCount][nPosSeek] ) == AllTrim( cLabel )
        xRet    := aAux[nCount][nPosRet]
        exit
    EndIf
Next nCount

Return xRet

//-----------------------------------------------------------------
/*/{Protheus.doc} AO51NoAcent()
Elimina acentos

@author Caio Murakami
@since 11/09/2019
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function AO51NoAcent(cString)

Default cString 	:= ""

cString		:= fWNoAccent(cString)

cString		:= StrTran(cString,"A","a")
cString		:= StrTran(cString,"�","")
cString		:= StrTran(cString,"�","")
cString		:= StrTran(cString,"?","")
cString		:= StrTran(cString,"�","")
cString		:= StrTran(cString,"�","")
cString		:= StrTran(cString,"�","")
cString		:= StrTran(cString,"�","")

Return cString 

//-----------------------------------------------------------------
/*/{Protheus.doc} AO51GetURL()
Captura URL

@author Caio Murakami
@since 20/08/2019
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function AO51GetURL()
Local cURL		:= ""
Local cQuery	:= ""
Local cAliasQry	:= GetNextAlias()

cQuery  := " SELECT DLV_URLMAP "
cQuery  += " FROM " + RetSQLName("DLV") + " DLV "
cQuery  += " WHERE DLV_FILIAL  = '" + xFilial("DLV") + "' "
cQuery  += " AND DLV_MSBLQL = '2' "
cQuery  += " AND DLV.D_E_L_E_T_ = '' "

cQuery := ChangeQuery( cQuery )
dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery ), cAliasQry, .F., .T. )

While (cAliasQry)->( !Eof() )
	cURL	:= (cAliasQry)->DLV_URLMAP
	(cAliasQry)->(dbSkip())
EndDo

(cAliasQry)->( dbSkip() )

Return cURL
