#include 'protheus.ch'
#include 'fwmvcdef.ch'
#include 'TMSAO52.ch'

/*/-----------------------------------------------------------
{Protheus.doc} TMSAO52()
Cadastro Roteirizador
Uso: SIGATMS
@sample
//TMSAO52()
@author Caio Murakami   
@since 03/07/2019
@version 1.0
-----------------------------------------------------------/*/
Function TMSAO52()
Local oBrowse   := Nil				// Recebe o  Browse          

Private  aRotina   := MenuDef()		// Recebe as rotinas do menu.

oBrowse:= FWMBrowse():New()   
oBrowse:SetAlias("DLV")			    // Alias da tabela utilizada
oBrowse:SetMenuDef("TMSAO52")		// Nome do fonte onde esta a fun��o MenuDef
oBrowse:SetDescription( STR0002 + " " + STR0001 )		//"Cadastro Roteirizador"
oBrowse:Activate()

Return Nil

/*/-----------------------------------------------------------
{Protheus.doc} MenuDef()
Utilizacao de menu Funcional  
Uso: TMSAO52
@sample
//MenuDef()
@author Caio Murakami   
@since 03/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function MenuDef()

Local aRotina := {}

	ADD OPTION aRotina TITLE STR0007  ACTION "AxPesqui"        	OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION aRotina TITLE STR0006  ACTION "VIEWDEF.TMSAO52" 	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION aRotina TITLE STR0003  ACTION "VIEWDEF.TMSAO52" 	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION aRotina TITLE STR0004  ACTION "VIEWDEF.TMSAO52" 	OPERATION 4 ACCESS 0 // "Alterar"
    ADD OPTION aRotina TITLE STR0005  ACTION "VIEWDEF.TMSAO52" 	OPERATION 5 ACCESS 0 // "Excluir"
	ADD OPTION aRotina TITLE STR0008  ACTION "TMSAO52Con()" 	OPERATION 6 ACCESS 0 // "Testar Conex�o"
	ADD OPTION aRotina TITLE STR0009  ACTION "Pergunte('TMSAO46',.T.)" OPERATION 6 ACCESS 0 // "Propriedades Roteirizador"

Return(aRotina)  

/*/-----------------------------------------------------------
{Protheus.doc} ModelDef()
Defini��o do Modelo
Uso: TMSAO52
@sample
//ModelDef()
@author Caio Murakami   
@since 03/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function ModelDef()
Local oModel	:= Nil		// Objeto do Model
Local oStruDLV	:= Nil		// Recebe a Estrutura da tabela DLU
Local bCommit 	:= { |oModel| CommitMdl(oModel) }
Local bPosValid := { |oModel| PosVldMdl(oModel) }

oStruDLV:= FWFormStruct( 1, "DLV" )

oModel := MPFormModel():New( "TMSAO52",,bPosValid, bCommit , /*bCancel*/ ) 

oModel:AddFields( 'MdFieldDLV',, oStruDLV,,,/*Carga*/ ) 

oModel:GetModel( 'MdFieldDLV' ):SetDescription( STR0002 + " " + STR0001  ) 	//"Cadastro Roteirizador"

oModel:SetPrimaryKey({"DLV_FILIAL" , "DLV_ID"})  
     
oModel:SetActivate( )
     
Return oModel 

/*/-----------------------------------------------------------
{Protheus.doc} ViewDef()
Defini��o da View

Uso: TMSAO52

@sample
//ViewDef()

@author Caio Murakami   
@since 03/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function ViewDef()     
Local oModel	:= Nil		// Objeto do Model 
Local oStruDLV	:= Nil		// Recebe a Estrutura da tabela DLV
Local oView					// Recebe o objeto da View

oModel   := FwLoadModel("TMSAO52")
oStruDLV := FWFormStruct( 2, "DLV" )

oView := FwFormView():New()
oView:SetModel(oModel)     

oView:AddField('VwFieldDLV', oStruDLV , 'MdFieldDLV')   

oView:CreateHorizontalBox('CABECALHO', 100)  
oView:SetOwnerView('VwFieldDLV','CABECALHO')

Return oView
/*/-----------------------------------------------------------
{Protheus.doc} CommitMdl()
Commit Mdl

Uso: TMSAO52

@sample
//ViewDef()

@author Caio Murakami   
@since 17/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function CommitMdl(oModel)
Local lRet	:= .T. 

lRet	:= FwFormCommit(oModel)

Return lRet

/*/-----------------------------------------------------------
{Protheus.doc} PosVldMdl()
pos valida��es

Uso: TMSAO52

@sample
//ViewDef()

@author Caio Murakami   
@since 17/07/2019
@version 1.0
-----------------------------------------------------------/*/
Static Function PosVldMdl(oModel)
Local lRet			:= .T. 
Local aAreaDLV		:= DLV->(GetArea())
Local nOperation	:= oModel:GetOperation()

If nOperation == 3 
	lRet := ExistChav("DLV",FwFldGet("DLV_ID") )
EndIf

DLV->(dbSetOrder(1))
If DLV->( dbSeek( xFilial("DLV")  )) .And. lRet
	While DLV->( !Eof() ) .And. DLV->DLV_FILIAL == xFilial("DLV")
	
		If DLV->DLV_ID <> FwFldGet("DLV_ID") .And. DLV->DLV_MSBLQL == "2" .And. FwFldGet("DLV_MSBLQL") == "2"
			lRet	:= .F. 
			Help("",1,"TMSAO52001") //-- N�o � permitido mais de um ID ativos.                                                       
			Exit
		EndIf 
		DLV->(dbSkip())
	EndDo
EndIf

RestArea(aAreaDLV)
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TMSAO52Con
Testa Conex�o
@author Caio Murakami
@since  17/07/2019
@version P12
/*/
//-------------------------------------------------------------------
Function TMSAO52Con()
Local lRet		:= .F. 
Local oRotaInt	:= Nil 
Local cURL      := "https://lbslocal-prod.apigee.net"
Local cToken	:= ""
Local cIDDLV	:= DLV->DLV_ID
Local cIDSecret	:= DLV->DLV_SECRET
Local cErro		:= ""

If DLV->DLV_MSBLQL == '1'
	lRet	:= .F. 
Else	
	oRotaInt    := TMSBCARotaInt():New(cURL,,,cIDDLV,cIDSecret,,,.F. )
	cToken		:= oRotaInt:ActiveToken()
	
	If Empty(cToken)
		FwMsgRun( , { || cToken := oRotaInt:GetAccessToken(@cErro) } , "Processando" , "Obtendo Token"  )
		If !Empty(cToken)
			lRet:= .T.
			RecLock("DLV",.F.)
			DLV->DLV_TOKEN	:= cToken
			DLV->DLV_DTTOKE	:= dDataBase
			DLV->DLV_HRTOKE	:= Time()
			DLV->(MsUnlock())
		EndIf
	Else
		lRet:= .T.	
	EndIf
EndIf

If lRet
	MsgInfo("Token: " + cToken )
Else
	MsgStop("Token: " + STR0010) //-- N�o autorizado
EndIf

Return lRet 
