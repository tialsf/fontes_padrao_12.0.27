#INCLUDE 'PROTHEUS.CH'   
#INCLUDE 'TMSAO53.CH'

//-------------------------------------------------------------------
/*TMSAO53

Limpeza da tabela DLU

@author  Caio Murakami
@since   09/07/2019
@version 1.0      
*/
//-------------------------------------------------------------------

Function TMSAO53() 
Local bProcesso	:= {|oSelf| AO53Proces( oSelf ) }
Local cPerg		:= "TMSAO53"

oTProces := tNewProcess():New( "TMSAO53" , STR0001 , bProcesso , STR0002 + STR0003 , cPerg ,,,,,.T.,.T.) 
oTProces:SaveLog(OemToAnsi(STR0004))

Return  



//-------------------------------------------------------------------
/*AO53Proces

Processamento da limpeza --  

@author  Caio Murakami
@since   09/07/2019
@version 1.0      
*/
//-------------------------------------------------------------------

Static Function AO53Proces( oSelf ) 
Local aArea 	:= GetArea()

TcInternal(5,"*OFF")   
RptStatus({|| AO53DelDAV()}, STR0004,STR0005 )
TcInternal(5,"*ON")
RestArea(aArea)  

Return .T.
  
//-------------------------------------------------------------------
/*AO53DelDAV

Rotina realiza a limpeza da DAV 

@author  Caio Murakami
@since   09/07/2019
@version 1.0      
*/
//-------------------------------------------------------------------

Static Function AO53DelDAV()
Local cQuery		:= ""  
Local lRet          := .T. 
   
cQuery  := " DELETE "
cQuery  += " 	FROM " + RetSQLName("DLU") 
cQuery  += " 	WHERE  DLU_FILIAL = '"+ xFilial("DLU")  + "' "
cQuery  += " 		AND DLU_FILENT = '" + xFilial("DTQ")  + "' "
cQuery  += "        AND DLU_ENTIDA = 'DTQ' "
cQuery  += "         AND DLU_CHVENT >= '" + mv_par01 + mv_par02 + "' "
cQuery  += "         AND DLU_CHVENT <= '" + mv_par03 + mv_par04 + "' "

If TCSqlExec( cQuery ) <> 0
    lRet    := .F. 
EndIf

Return lRet
