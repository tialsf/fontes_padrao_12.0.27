#Include 'TmsA390.ch'
#Include 'Protheus.ch'
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA390  � Autor � Antonio C Ferreira    � Data �10.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Prazos de Regioes                                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA390(ExpL1, ExpA1, ExpN1)                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpL1 - Array Contendo os Campos (Rot. Automatica)         ���
���          � ExpN1 - Opcao Selecionada (Rot. Automatica)                ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nil                                                        ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function TMSA390(aRotAuto, nOpcAuto)

Private cCadastro   := ""
Private lMsHelpAuto := .F.
Private l390Auto    := ( Valtype(aRotAuto) == "A" )
Private aRotina     := MenuDef()

cCadastro := STR0001 //'Prazos de Regioes'

If !l390Auto
	DTD->(dbSetOrder(1))
	mBrowse( 6, 1, 22, 75, "DTD", , , , , , , , )
Else
	lMsHelpAuto := .T.
	MsRotAuto(nOpcAuto,aRotAuto,'DTD')
EndIf

RetIndex('DTD')

Return NIL

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA390Mnt� Autor � Antonio C Ferreira    � Data �10.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Prazos de Regioes                                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA390Mnt(ExpC1,ExpN1,ExpN2)                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                   ���
���          � ExpN1 = Numero do registro                                 ���
���          � ExpN2 = Opcao selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA390Mnt( cTmsAlias, nTmsReg, nTmsOpcx )

Local nTamACols		:= 1
//-- EnchoiceBar
Local aTmsVisual	:= {}
Local aTmsAltera	:= {}
Local aTmsButtons	:= {}
Local aTelOld		:= Iif( Type('aTela') == 'A', aClone( aTela ), {} )
Local aGetOld		:= Iif( Type('aGets') == 'A', aClone( aGets ), {} )
Local nOpca			:= 0
Local oTmsEnch

//-- Dialog
Local cCadOld		:= Iif( Type('cCadastro') == 'C', cCadastro, '' )
Local oTmsDlgEsp
//-- GetDados
Local aHeaOld		:= Iif( Type('aHeader') == 'A', aClone( aHeader ), {} )
Local aColOld		:= Iif( Type('aCols') == 'A', aClone( aCols ), {} )
Local aNoFields		:= {}
Local aYesFields	:= {}
//-- Controle de dimensoes de objetos
Local aObjects		:= {}
Local aInfo			:= {}
//-- Checkbox
Local oAllMark

Local oStructDTD	:= Nil
Local aFldsDTD		:= {}
Local nI			:= 0

//-- EnchoiceBar
Private aTela	:= {}
Private aGets	:= {}
//-- GetDados
Private aHeader		:= {}
Private aCols		:= {}
Private oTmsGetD
Private aTmsPosObj	:= {}
//-- Checkbox

DEFAULT cTmsAlias := 'DTD'
DEFAULT nTmsReg   := 1
DEFAULT nTmsOpcx  := 2

If !l390Auto
	//-- Configura variaveis da Enchoice
	RegToMemory( cTmsAlias, INCLUI )
EndIf

If !l390Auto
	oStructDTD := FWFormStruct(1, "DTD")
	aFldsDTD := oStructDTD:aFields

	For nI := 1 To Len(aFldsDTD)
		If X3USO(GetSX3Cache(aFldsDTD[nI][3], "X3_USADO")) .And. !(Rtrim(aFldsDTD[nI][3]) $ "DTD_FILIAL" )
			Aadd( aTmsVisual, Rtrim(aFldsDTD[nI][3]) )
			If (GetSX3Cache(aFldsDTD[nI][3], "X3_VISUAL") != "V")
				Aadd( aTmsAltera, Rtrim(aFldsDTD[nI][3]) )
			EndIf
		EndIf
	Next

	FreeObj(oStructDTD)

	aSize(aFldsDTD, 0)
	aFldsDTD := Nil

EndIf

If !l390Auto
	//-- Dimensoes padroes
	aSize := MsAdvSize()
	AAdd( aObjects, { 100, 100, .T., .T. } )
	aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 5, 5 }
	aTmsPosObj := MsObjSize( aInfo, aObjects,.T.)

	DEFINE MSDIALOG oTmsDlgEsp TITLE cCadastro FROM aSize[7],00 TO aSize[6],aSize[5] PIXEL
		//-- Monta a enchoice.
		oTmsEnch		:= MsMGet():New( cTmsAlias, nTmsReg, nTmsOpcx,,,, aTmsVisual, aTmsPosObj[1],aTmsAltera, 3,,,,,,.T. )
	ACTIVATE MSDIALOG oTmsDlgEsp ON INIT EnchoiceBar(oTmsDlgEsp,{|| if(TMSA390TOk(nTmsOpcx), (nOpca := 1,oTmsDlgEsp:End()), (nOpca := 0)) },{||nOpca:=0,oTmsDlgEsp:End()},, aTmsButtons )

Else

	nOpca := 1

EndIf

If nTmsOpcx != 2 .And. nOpcA == 1

	TMSA390Grv( cTmsAlias, nTmsReg, nTmsOpcx )

EndIf

If !Empty( cCadOld )
	cCadastro := cCadOld
EndIf

If	!Empty( aTelOld )
	aTela := aClone( aTelOld )
	aGets := aClone( aGetOld )
EndIf

If	!Empty( aHeaOld )
	aHeader := aClone( aHeaOld )
	aCols   := aClone( aColOld )
EndIf

Return .F.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA390TOk � Autor � Antonio C Ferreira   � Data �10.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gravar dados                                               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA390TOk(nTmsOpcx)

Local aAreaDTD := DTD->( GetArea() )
Local aAreaDVN := DVN->( GetArea() )
Local lRet     := .T.
Local nReg     := if(!INCLUI, Recno(), 0)

DbSelectArea( "DTD" )
DbSetOrder( 1 )

If (INCLUI .or. ALTERA) .And. MsSeek( xFilial("DTD") + M->(DTD_CDRORI+DTD_CDRDES+DTD_TIPTRA) ) .And. (Recno() != nReg) 
	Help(' ', 1, 'TMSA39001')	//-- Prazo ja cadastrado!
	lRet := .F.
EndIf
DVN->(DbSetOrder(1))
If	nTmsOpcx == 4 .Or. ALTERA //Se for alteracao ou exclusao
	If DVN->(MsSeek(xFilial('DVN')+M->(DTD_CDRORI+DTD_CDRDES+DTD_TIPTRA ) ) )
			If !MsgYesNo(STR0010,STR0009) //"Existem clientes utilizando este prazo, confirma alteracao?"
				lRet := .F.
			EndIf
	EndIf
EndIf

//--Valida os campos obrigat�rios.
lRet := OBrigatorio(aGets,aTela)

RestArea( aAreaDTD )
RestArea( aAreaDVN )

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA390Grv� Autor � Antonio C Ferreira   � Data �10.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gravar dados                                               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Static Function TMSA390Grv( cAlias, nReg, nTmsOpcx )

Local nCntFor	:= 0

l390Auto := If (Type("l390Auto") == "U",.F.,l390Auto)

If	(nTmsOpcx == 3) .or. (nTmsOpcx == 4)
	If !l390Auto
		Begin Transaction
			RecLock( "DTD", (nTmsOpcx==3) )
			Aeval( dbStruct(), { |aFieldName, nI | FieldPut( nI, If('FILIAL' $ aFieldName[1],;
			xFilial( "DTD" ), M->&(aFieldName[1]) ) ) } )
		End Transaction
	Else
		Begin Transaction
			AxIncluiAuto(cAlias,'TMSA390TOk()',,nTmsOpcx,nReg)	
		End Transaction
	EndIf

ElseIf	(nTmsOpcx == 5)
	Begin Transaction
		RecLock("DTD", .F.)
		dbDelete()
		MsUnLock()
	End Transaction
EndIf

Return NIL

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA390Vld� Autor � Alex Egydio           � Data �16.09.2003���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validacoes do sistema                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA390Vld()

Local aAreaDTD := DTD->(GetArea())
Local cCampo   := ReadVar()
Local lRet     := .T.
Local lSinc    := TmsSinc() //chamada do sincronizador 
Local nTamTmp  := Len(DTD->DTD_TMEMBI) //-- Tamanho campos tempo
Local cCpoTmp  := StrZero(0,nTamTmp)

//-- Prazo informado por cliente
If !lSinc
	//-- Nao permitir que o prazo minimo, seja maior que o prazo estipulado na regiao
	If	cCampo == 'M->DTD_TMEMBI'

		If	M->DTD_TMEMBI > M->DTD_TMEMBF .And. M->DTD_TMEMBF <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMEMBI'))
			lRet := .F.
		EndIf

	ElseIf cCampo == 'M->DTD_TMTRAI'

		If	M->DTD_TMTRAI > M->DTD_TMTRAF .And. M->DTD_TMTRAF <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMTRAI'))
			lRet := .F.
		EndIf

	ElseIf cCampo == 'M->DTD_TMDISI'

		If	M->DTD_TMDISI > M->DTD_TMDISF .And. M->DTD_TMDISF <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMDISI'))
			lRet := .F.
		EndIf
	//-- Nao permitir que o prazo maximo, seja menor que o prazo minimo estipulado na regiao
	ElseIf cCampo == 'M->DTD_TMEMBF'

		If	M->DTD_TMEMBF < M->DTD_TMEMBI .And. M->DTD_TMEMBI <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMEMBF'))
			lRet := .F.
		EndIf

	ElseIf cCampo == 'M->DTD_TMTRAF'

		If	M->DTD_TMTRAF < M->DTD_TMTRAI .And. M->DTD_TMTRAI <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMTRAF'))
			lRet := .F.
		EndIf

	ElseIf cCampo == 'M->DTD_TMDISF'

		If	M->DTD_TMDISF < M->DTD_TMDISI .And. M->DTD_TMDISI <> cCpoTmp
			cCampo := AllTrim(RetTitle('DTD_TMDISF'))
			lRet := .F.
		EndIf

	EndIf
	If ! lRet
		Help('',1,'TMSA39002',,cCampo,4,1) //--"O tempo informado esta maior que o tempo especificado para a regiao"
	EndIf
EndIf

RestArea(aAreaDTD)

Return(lRet)

/*/
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Marco Bianchi         � Data �01/09/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �    1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function MenuDef()

Private aRotina	:= {	{ STR0002 ,'TmsXPesqui',0,1,0,.F.},; //'Pesquisar'
						{ STR0003 ,'TMSA390Mnt',0,2,0,NIL},; //'Visualizar'
						{ STR0004 ,'TMSA390Mnt',0,3,0,NIL},; //'Incluir'
						{ STR0005 ,'TMSA390Mnt',0,4,0,NIL},; //'Alterar'
						{ STR0006 ,'TMSA390Mnt',0,5,0,NIL} } //'Excluir'

If ExistBlock("TMA390MNU")
	ExecBlock("TMA390MNU",.F.,.F.)
EndIf

Return(aRotina)
