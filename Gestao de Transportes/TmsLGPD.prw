#Include "protheus.ch"

//-------------------------------------------------------------------
/*TMLGPDCpPr
Retorna campos sensíveis e pessoais do Alias e utilizados na Rotina.
@author  Leandro Paulino
@since   09/12/2019
@version 1.0      
*/
//-------------------------------------------------------------------

Function TMLGPDCpPr(aCposRotin,cAlias)  //--Retorna campos Sensíveis     

Local aCposAlias 	:= {} //--Campos que são sensiveis ou pessoais do Alias
Local nCountCpo		:= 0
Local aRetCpoUsa	:= {} //--Campos que são sensíveis ou pessoais e são usados na rotina.  

Default aCposRotin 	:= {} //--Array com os campos que são apresentados pela Rotina
Default cAlias		:= ''

If !Empty(cAlias) .And. Len(aCposRotin) > 0
	aCposAlias := FwProtectedDataUtil():GetAliasFieldsInList(cAlias)
	If Len(aCposRotin) > 0
		For nCountCpo := 1 To Len(aCposAlias)								
			If Ascan( aCposRotin, { |x|  AllTrim(x) == aCposAlias[nCountCpo]:CFIELD } ) > 0
				AADD(aRetCpoUsa,aCposAlias[nCountCpo]:CFIELD)
			EndIf
		Next nCountCpo
	EndIf
 EndIf

Return aRetCpoUsa