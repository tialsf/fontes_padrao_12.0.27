#Include 'TmsA395.ch'
#Include 'Protheus.ch'
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA395  � Autor � Wellington A Santos   � Data �24/01/2004���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Prazos de Regioes por cliente                              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA395(ExpA1, ExpN1)                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 - Array Contendo os Campos (Rot. Automatica)         ���
���          � ExpN1 - Opcao Selecionada (Rot. Automatica)                ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nil                                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Function TMSA395(aRotAuto, nOpcAuto)

Local aCampos       := {}
Local cAlias        := "DVN"
Local aCores        := {}

Private cCadastro   := ""
Private lMsHelpAuto := .F.
Private l395Auto    := ( Valtype(aRotAuto) == "A" )
Private aRotina     := MenuDef()

cCadastro := STR0008 //'Prazos de Regioes por cliente'

If !l395Auto
	Aadd(aCores,{"DVN_STATUS=='1'", 'BR_VERMELHO' }) // Bloqueado
	Aadd(aCores,{"DVN_STATUS!='1'", 'BR_VERDE'    }) // Liberado

	DVN->(dbSetOrder(1))
	Aadd( aCampos, { "Cod. Cliente", "DVN_CODCLI" } )
	Aadd( aCampos, { "Loja Cliente", "DVN_LOJCLI" } )
	Aadd( aCampos, { "Nome Cliente", 'CriaVar("DVN_NOMCLI")' } )
	mBrowse( 6, 1, 22, 75, cAlias, aCampos, , , , ,aCores)
Else
	lMsHelpAuto := .T.
	MsRotAuto(nOpcAuto,aRotAuto,cAlias)
EndIf

RetIndex(cAlias)

Return NIL

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA395Mnt� Autor � Wellington A Santos   � Data �24/01/2004���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Prazos de Regioes                                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA395Mnt(ExpC1,ExpN1,ExpN2)                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                   ���
���          � ExpN1 = Numero do registro                                 ���
���          � ExpN2 = Opcao selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.             ���
�������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Motivo da Alteracao                     ���
�������������������������������������������������������������������������Ĵ��
���            �        �      �                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA395Mnt( cTmsAlias, nTmsReg, nOpcx )

Local nTamACols		:= 1
//-- EnchoiceBar
Local aTmsVisual	:= {}
Local aTmsAltera	:= {}
Local aButtons		:= {}
Local aTelOld		:= Iif( Type('aTela') == 'A', aClone( aTela ), {} )
Local aGetOld		:= Iif( Type('aGets') == 'A', aClone( aGets ), {} )
Local nOpca			:= 0
Local oTmsEnch

Local aNoFields		:= {}
Local aYesFields	:= {}
//-- Controle de dimensoes de objetos
Local aObjects		:= {}
Local aInfo			:= {}
//-- Checkbox
Local oAllMark
Local nCont     := 0
Local aDVNStru  := FwFormStruct(2,"DVN")

Private aEst       := {}

//Salva Variaveis GetDados
SaveInter()

//-- EnchoiceBar
Private aTela[0][0]
Private aGets[0]
//-- GetDados
Private aHeader		:= {}
Private aCols		:= {}
Private oTmsGetD
Private aTmsPosObj	:= {}
//-- Checkbox

DEFAULT cTmsAlias := 'DVN'
DEFAULT nTmsReg   := 1
DEFAULT nOpcx     := 2

If nOpcx == 3
	bSavKeyF4   := SetKey( VK_F4 , { || TMSA395UF() } )
	Aadd(aButtons, {"WEB", {|| TMSA395UF()}, STR0001, STR0018 }) //"Prazos de Regioes"###"Praz.Reg"
EndIf

If !l395Auto
	//-- Configura variaveis da Enchoice
	RegToMemory( cTmsAlias, INCLUI )
EndIf

If !l395Auto
	For nCont := 1 to Len(aDVNStru:aFields)	
		If GetSX3Cache(aDVNStru:aFields[nCont][1], "X3_ARQUIVO") == cTmsAlias			
			Aadd( aTmsVisual, Rtrim(GetSX3Cache(aDVNStru:aFields[nCont][1], "X3_CAMPO")))
			If GetSX3Cache(aDVNStru:aFields[nCont][1], "X3_VISUAL")!= "V"
				Aadd( aTmsAltera,Rtrim(GetSX3Cache(aDVNStru:aFields[nCont][1], "X3_CAMPO")))
			EndIf	
		EndIf					
	Next nCont
EndIf

	
If !l395Auto
	//-- Dimensoes padroes
	aSize := MsAdvSize()
	AAdd( aObjects, { 100, 100, .T., .T. } )
	aInfo := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 5, 5 }
	aTmsPosObj := MsObjSize( aInfo, aObjects,.T.)

	DEFINE MSDIALOG oTmsDlgEsp TITLE cCadastro FROM aSize[7],00 TO aSize[6],aSize[5] PIXEL
		//-- Monta a enchoice.
		oTmsEnch := MsMGet():New( cTmsAlias, nTmsReg, Iif( nOpcx==6, 2, nOpcx ),,,, aTmsVisual, aTmsPosObj[1],aTmsAltera, 3,,,,,,.T. )
	ACTIVATE MSDIALOG oTmsDlgEsp ON INIT EnchoiceBar(oTmsDlgEsp,{|| if(TMSA395TOk(), (nOpca := 1,oTmsDlgEsp:End()), (nOpca := 0)) },{||nOpca:=0,oTmsDlgEsp:End()},, aButtons )
Else
	nOpca := 1
EndIf

If nOpcx != 2 .And. nOpcA == 1

	If nOpcx == 6	//--  Liberar
		TmsA395Lib()
	EndIf

	TMSA395GRV( cTmsAlias, nTmsReg, nOpcx )

	//����������������������������������������������������������������Ŀ
	//�Se for Inclusao de Ajuste,Grava a Tabela para as Regioes de Des-�
	//�tino que fazem parte dos Estados Informados no Botao da Enchoice�
	//������������������������������������������������������������������
	If nOpcx == 3 
		CursorWait()
		MsgRun(STR0012,,{|| TMA395RegDes(nOpcx) }) //--"Aguarde, gerando Prazos de Regioes de Clientes para os Estados informados..." 
		CursorArrow()
	EndIf

EndIf

If	!Empty( aTelOld )
	aTela := aClone( aTelOld )
	aGets := aClone( aGetOld )
EndIf

//Restaura Variaveis Getdados
RestInter()

Return .F.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA395TOk� Autor � Wellington A Santos  � Data �24/01/2004���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gravar dados                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA395TOk()

Local aAreaDVN := DVN->( GetArea() )
Local aAreaDTD := DTD->( GetArea() )
Local lRet     := .T.
Local nReg     := if(!INCLUI, DVN->(Recno()), 0)

DbSelectArea("DVN")
DbSetOrder(1) //DVN_FILIAL+DVN_CODCLI+DVN_LOJCLI+DVN_CDRORI+DVN_CDRDES+DVN_TIPTRA
If (Inclui .or. Altera) .And. MsSeek(xFilial("DVN") + M->(DVN_CODCLI+DVN_LOJCLI+DVN_CDRORI+DVN_CDRDES+DVN_TIPTRA)) .And. (Recno() != nReg) 
	Help(' ', 1, 'TMSA39501')	//-- "Prazo ja cadastrado!"
	lRet := .F.
EndIf
If (Inclui .or. Altera) .And. lRet .And. DTD->(!MsSeek( xFilial("DTD") + M->(DVN_CDRORI+DVN_CDRDES+DVN_TIPTRA) ) )
	Help(' ', 1, 'TMSA39503')	//-- "Este prazo de cliente nao existe na tabela de prazo de regiao"
	lRet := .F.
EndIf
If (Inclui .or. Altera) .And. lRet 
	If Val(M->DVN_TMCLII) == 0 .And. Val(M->DVN_TMCLIF) == 0
		Help(' ', 1, 'TMSA39507')	//-- "Os campos 'Tempo De' e 'Tempo Ate' nao podem estar zerados"
		lRet := .F.
	EndIf
EndIf

RestArea( aAreaDVN )
RestArea( aAreaDTD )

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA395GRV� Autor � Wellington A Santos  � Data �10.05.2002���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gravar dados                                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Static  Function TMSA395GRV( cAlias, nReg, nOpcx )

Local nCntFor	:= 0

l395Auto := If (Type("l900Auto") == "U",.F.,l900Auto)

If	(nOpcx == 3) .or. (nOpcx == 4) .or. (nOpcx == 6)
	If !l395Auto
		Begin Transaction
			RecLock( cAlias, (nOpcx==3) )
			Aeval( dbStruct(), { |aFieldName, nI | FieldPut( nI, If('FILIAL' $ aFieldName[1],;
			xFilial( cAlias ), M->&(aFieldName[1]) ) ) } )
		End Transaction
	Else
		Begin Transaction
			AxIncluiAuto(cAlias,'TMSA395TOk()',,nOpcx,nReg)	
		End Transaction
	EndIf

ElseIf	(nOpcx == 5)
	Begin Transaction
		RecLock(cAlias, .F.)
		dbDelete()
		MsUnLock()
	End Transaction
EndIf

Return NIL
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA395Vld� Autor �Wellington A Santos    � Data �27/01/2004���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Validacoes do sistema                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA395Vld()

Local aAreaDTD := DTD->(GetArea())
Local cTempo   := ''
Local lSinc    := TmsSinc() //chamada do sincronizador
Local cCampo   := ReadVar()
Local nTamTmp  := TamSx3('DVN_TMCLII')[1]-2 //-- Tamanho Horas menos Minutos

If !lSinc 
	//-- Posiciona no prazo de regioes
	DTD->(DbSetOrder(1))
	If	DTD->(MsSeek(xFilial('DVN')+M->DVN_CDRORI+M->DVN_CDRDES+M->DVN_TIPTRA))
		If	cCampo == 'M->DVN_TMCLII'
			M->DVN_TMCLIF := M->DVN_TMCLII
			If	M->DVN_TMCLII > M->DVN_TMCLIF
				Help('',1,'TMSA39505')	//-- Tempo minimo de entrega do cliente esta maior que tempo maximo
				Return(.F.)
			EndIf
			//-- Tempo inicial de embarque, trafego e distribuicao informado no prazo de regioes
			cTempo := StrTran(IntToHora(TmsHrToInt(DTD->DTD_TMEMBI)+TmsHrToInt(DTD->DTD_TMTRAI)+TmsHrToInt(DTD->DTD_TMDISI),nTamTmp),":","")
			//-- Nao permite que tempo minimo de entrega de um cliente, seja menor que o informado no prazo de regioes
			If	M->DVN_TMCLII < cTempo
				Help('',1,'TMSA39502',,STR0011 + Transf(Val(cTempo),PesqPict('DTD','DTD_TMEMBI')),4,1) //"Tempo minimo de entrega do cliente esta menor que o informado no prazo de regioes"###"Prazo de Regioes: "
				M->DVN_STATUS := StrZero(1, Len(DVN->DVN_STATUS)) //Bloqueado
			Else
				M->DVN_STATUS := StrZero(2, Len(DVN->DVN_STATUS)) //Liberado
			EndIf
		ElseIf cCampo == 'M->DVN_TMCLIF'
			If	M->DVN_TMCLIF < M->DVN_TMCLII
				Help('',1,'TMSA39506')	//-- Tempo maximo de entrega do cliente esta menor que tempo minimo
				Return(.F.)
			EndIf
			//-- Tempo final de embarque, trafego e distribuicao informado no prazo de regioes
			cTempo := StrTran(IntToHora(TmsHrToInt(DTD->DTD_TMEMBF)+TmsHrToInt(DTD->DTD_TMTRAF)+TmsHrToInt(DTD->DTD_TMDISF),nTamTmp),":","")
			//-- Nao permite que tempo maximo de entrega de um cliente, seja maior que o informado no prazo de regioes
			If	M->DVN_TMCLIF < cTempo 
				Help('',1,'TMSA39504',,STR0011 + Transf(Val(cTempo),PesqPict('DTD','DTD_TMEMBF')),4,1) //"Tempo maximo de entrega do cliente esta maior que o informado no prazo de regioes"###"Prazo de Regioes: "
				M->DVN_STATUS := StrZero(1, Len(DVN->DVN_STATUS)) //Bloqueado
			Else
				M->DVN_STATUS := StrZero(2, Len(DVN->DVN_STATUS)) //Liberado
			EndIf
		EndIf
	EndIf
EndIf

RestArea(aAreaDTD)

Return(.T.)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � TMSA395UF   � Autor �Patricia A. Salomao � Data �23.01.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Monta uma Parambox contendo todos os Estados em que o Prazo���
���          � de Regioes podera ser criado                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nil                                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Static Function TMSA395UF()

Local aEstados := {}
Local aRet     := {}
Local lCkb
Local lInv     := .F.
Local nX
Local aSX512   := {}

aSX512 	 := FwGetSX5("12",)
For nX := 1 to Len(aSX512)
	lCkb := .F.
	aAdd(aEstados, {4,"",lCkb,AllTrim(aSX512[nX,3]) + " - " + aSX512[nX,4],80,,.F.} )
Next nX	

aRet := {}
If ParamBox( aEstados, STR0013 , @aRet, , {{ 5, { |oPanel| TMSA395Marc(aEstados,@aRet,@lInv),oPanel:Refresh() }, STR0014 }} , .T.) 
	For nX := 1 To Len(aRet)
		If aRet[nX]
			AADD(aEst, SubStr(aEstados[nX,4],1,2) )
		EndIf
	Next nX
EndIf

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA395Mar� Autor �Patricia A. Salomao    � Data �23.01.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca/Desmarca todos os CheckBox do PARAMBOX()             ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � TMSA395Marc()                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 - Array contendo a Tabela de Estados                 ���
���          � ExpA2 - Array de controle dos Estados Marcados             ���
���          � ExpL1 - Marca/Desmarca                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nil                                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function TMSA395Marc(aEstados,aRet,lInv)
Local nX := 0

For nX := 1 To Len(aEstados)
	aAdd(aRet,If(lInv,.F.,.T.))
	&("MV_PAR"+StrZero(nx,2)) := If(lInv,.F.,.T.)
Next nX
lInv := !lInv

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMA395RegD� Autor �Patricia A. Salomao    � Data �23.01.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Gera Prazo de Regioes  para as Regioes pertencentes         ���
���          �aos Estados Informados no Botao de Estados (Enchoice)       ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpN1 - Opcao Selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nil                                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function TMA395RegDes(nOpcx)
Local aAreaDVN  := DVN->(GetArea())
Local nCntFor   := 0
Local cTempoI   := ''
Local cTempoF   := ''
Local cAliasNew := ''
Local cQuery    := ''
Local cEst      := ''
Local aMsgErr   := {}
Local aVisErr   := {}

If !Empty(aEst)
	For nCntFor := 1 To Len(aEst)
		cEst += "'" + aEst[nCntFor] + "',"
	Next

	cEst := Substr(cEst,1,Len(cEst) - 1)
	cAliasNew := GetNextAlias()
	cQuery := " SELECT DISTINCT DUY_GRPVEN, DTD_TMEMBI, DTD_TMTRAI, DTD_TMDISI, DTD_TMEMBF, DTD_TMTRAF, DTD_TMDISF"
	cQuery += " FROM "
	cQuery += RetSqlname("DUY") + " DUY,"
	cQuery += RetSqlName("DTD") + " DTD,"
	cQuery += RetSqlName("DVN") + " DVN "
	cQuery += " WHERE DUY_FILIAL = '" + xFilial("DUY") + "'"
	cQuery += "   AND DUY_EST IN (" + cEst + ")"
	cQuery += "   AND DUY.D_E_L_E_T_ = ' ' "

	//--  E' Obrigatorio existir 'Prazo de Regiao' cadastrado
	cQuery += "   AND DTD_FILIAL = '" + xFilial("DTD") + "'"
	cQuery += "   AND DTD_CDRORI = '" + M->DVN_CDRORI + "'"
	cQuery += "   AND DTD_CDRDES = DUY_GRPVEN "
	cQuery += "   AND DTD_TIPTRA = '" + M->DVN_TIPTRA + "'"
	cQuery += "   AND DTD.D_E_L_E_T_ = ' ' "

	//-- Verifica se ja existe o 'Prazo de Regiao por Cliente' Cadastrado (DVN)
	cQuery += " AND NOT EXISTS ( SELECT *  FROM "
	cQuery += RetSqlName("DVN") + " DVN "
	cQuery += " WHERE DVN_FILIAL = '" + xFilial("DVN") + "'"
	cQuery += "   AND DVN_CODCLI = '" + M->DVN_CODCLI + "'"
	cQuery += "   AND DVN_LOJCLI = '" + M->DVN_LOJCLI + "'"
	cQuery += "   AND DVN_CDRORI = '" + M->DVN_CDRORI + "'"
	cQuery += "   AND DVN_CDRDES = DUY_GRPVEN "
	cQuery += "   AND DVN_TIPTRA = '" + M->DVN_TIPTRA + "'"
	cQuery += "   AND DVN.D_E_L_E_T_ = ' ' )"
	cQuery := ChangeQuery(cQuery)
	DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),cAliasNew,.T.,.T.)

	Do While (cAliasNew)->( !Eof() )
		M->DVN_CDRDES := (cAliasNew)->DUY_GRPVEN
		aMsgErr       := {}
		//-- Nao permite que tempo minimo de entrega de um cliente, seja menor que o informado no prazo de regioes
		cTempoI := StrTran(IntToHora(TmsHrToInt((cAliasNew)->DTD_TMEMBI)+TmsHrToInt((cAliasNew)->DTD_TMTRAI)+TmsHrToInt((cAliasNew)->DTD_TMDISI),3),":","")		
		//-- Tempo final de embarque, trafego e distribuicao informado no prazo de regioes
		cTempoF := StrTran(IntToHora(TmsHrToInt((cAliasNew)->DTD_TMEMBF)+TmsHrToInt((cAliasNew)->DTD_TMTRAF)+TmsHrToInt((cAliasNew)->DTD_TMDISF),3),":","")

		If	M->DVN_TMCLII < cTempoI
			AAdd( aMsgErr, { STR0015 + M->DVN_CDRORI + ' / ' + STR0016 + M->DVN_CDRDES	, '01', "TMSA390()" } )
			//-- Carrega o Array com as mensagens de Erro
			AaddMsgErr( aMsgErr, @aVisErr )
			M->DVN_STATUS   := StrZero(1, Len(DVN->DVN_STATUS))	// -- Status de Bloqueado
		ElseIf	M->DVN_TMCLIF < cTempoF //-- Nao permite que tempo maximo de entrega de um cliente, seja maior que o informado no prazo de regioes
			AAdd( aMsgErr, { STR0017 + M->DVN_CDRORI + ' / ' + STR0016 + M->DVN_CDRDES	, '01', "TMSA390()" } )
			//-- Carrega o Array com as mensagens de Erro
			AaddMsgErr( aMsgErr, @aVisErr )
			M->DVN_STATUS   := StrZero(1, Len(DVN->DVN_STATUS))	// -- Status de Bloqueado
		EndIf

		TMSA395Grv("DVN", , nOpcx )
		(cAliasNew)->(dbSkip())
	EndDo
	(cAliasNew)->( DbCloseArea() )

EndIf

If ! Empty( aVisErr )
	TmsMsgErr( aVisErr )
EndIf

RestArea(aAreaDVN)

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA395Leg� Autor � Gilson da Silva       � Data �13.03.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Exibe a legenda do status do prazo do Cliente.             ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA395Leg()

BrwLegenda( cCadastro, STR0020  ,; 	//'Status'
 {{	'BR_VERMELHO'    , STR0021 },; 	//'Bloqueado'
  {	'BR_VERDE'   	 , STR0022 }}) //'Liberado'

Return NIL 
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �TMSA395Lib� Autor � Gilson da Silva       � Data �20.03.2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Modifica o Status do prazo para liberado e grava o usu�rio,���
���          � data e hora de libera��o.                                  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������*/
Function TMSA395Lib()

If DVN->DVN_STATUS  == StrZero(1, Len(DVN->DVN_STATUS))
	M->DVN_STATUS   := StrZero(2, Len(DVN->DVN_STATUS))
	M->DVN_USRAPV   := RetCodUsr()
	M->DVN_DATLIB   := dDataBase
	M->DVN_HORLIB   := Left(Time(),5)
EndIf
Return Nil

/*/
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MenuDef   � Autor � Marco Bianchi         � Data �01/09/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Utilizacao de menu Funcional                               ���
���          �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Array com opcoes da rotina.                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Parametros do array a Rotina:                               ���
���          �1. Nome a aparecer no cabecalho                             ���
���          �2. Nome da Rotina associada                                 ���
���          �3. Reservado                                                ���
���          �4. Tipo de Transa��o a ser efetuada:                        ���
���          �    1 - Pesquisa e Posiciona em um Banco de Dados           ���
���          �    2 - Simplesmente Mostra os Campos                       ���
���          �    3 - Inclui registros no Bancos de Dados                 ���
���          �    4 - Altera o registro corrente                          ���
���          �    5 - Remove o registro corrente do Banco de Dados        ���
���          �5. Nivel de acesso                                          ���
���          �6. Habilita Menu Funcional                                  ���
�������������������������������������������������������������������������Ĵ��
���   DATA   � Programador   �Manutencao efetuada                         ���
�������������������������������������������������������������������������Ĵ��
���          �               �                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()

Private aRotina	:= {}

aRotina := {	{ STR0002 ,'TmsXPesqui',0, 1,0,.F.},; //'Pesquisar'
				{ STR0003 ,'TMSA395Mnt',0, 2,0,NIL},; //'Visualizar'
				{ STR0004 ,'TMSA395Mnt',0, 3,0,NIL},; //'Incluir'
				{ STR0005 ,'TMSA395Mnt',0, 4,0,NIL},; //'Alterar'
				{ STR0006 ,'TMSA395Mnt',0, 5,0,NIL},; //'Excluir'
				{ STR0019 ,'TMSA395Mnt',0, 4,0,NIL},; //'Aprovar'
				{ STR0007 ,'TMSA395Leg',0,11,0,NIL} } //'Legenda'

If ExistBlock("TMA395MNU")
	ExecBlock("TMA395MNU",.F.,.F.)
EndIf

Return(aRotina)
