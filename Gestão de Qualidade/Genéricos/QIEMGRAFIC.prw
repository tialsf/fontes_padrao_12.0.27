#include "TOTVS.CH"
#INCLUDE "FWBROWSE.CH"
#include "FWMVCDEF.ch"


/*/{Protheus.doc} QIEMGRAFIC
//TODO Descri��o auto-gerada.
@author carlos.augusto
@since 21/05/2019
@version 1.0
@return ${return}, ${return_description}
@param aDataPar, array, descricao
@param nTipo, characters, descricao 1=Valores Individuais e Amplitude Movel, 2=Pareto
@type function
/*/
Function QIEMGRAFIC(aDataPar, nTipo, aMedicoes, aTitCarCon, aLimites, cDirPng, cArqPNG, lXbXbr)

    Local nI		  as numeric
    Local oKendo	  as object
    Local oDlgM		  as object
    Local aJsData	  as array
    Local aSeries	  as array
	Local aMed        as array
    Local aLim		  as array
	Local aResult	  as array
	Local aValueAxis  as array
	Local aCategories as array
	Local aData       as array
	Local aMyAxis     as array
	Local cNameDlg    as char
	Local aCoors 	  as array
	Local aComments	  as array
	Local nX	      as numeric
	Local nY	      as numeric

	Local nMenor := 999999999
	Local nMaior := 0
	Local nMedia := 0
	PRIVATE cDirePng := cDirPng
	PRIVATE cNamePng := cArqPNG
	DEFAULT aTitCarCon := {"Valores Individuais", "Amplitude Movel"}
    DEFAULT lXbXbr     := .F.

	aMed        := {}
    aJsData     := {}
    aSeries     := {}
    aLimits     := {}
    aCategories := {}
	aValueAxis	:= {}
	aData    	:= {}
	aMyAxis    	:= {}
	aComments   := {}
	aCoors 		:= FWGetDialogSize(oMainWnd) //Tamanho tela

	If nTipo == 1
	    cNameDlg := 'Cartas de Controle'
	elseif nTipo == 2
		cNameDlg := 'Diagrama de Pareto'
	ElseIf nTipo == 3
		cNameDlg := 'Histograma'
	ElseIf nTipo == 4
		cNameDlg := 'Estudo de Linearidade MSA 4� Edi��o'
	ElseIf nTipo == 5
		cNameDlg := 'Tend�ncias'
	ElseIf nTipo == 6 
		cNameDlg := 'Relat�rio �ndice Qualidade'
	ElseIf nTipo == 7
		cNameDlg := 'Relatorio de Performance de Sistemas de Medi��o'
	ElseIf nTipo == 8
		cNameDlg := 'Carta Individual'	    
	EndIf
	
	oDlgM := TDialog():New(0,0,aCoors[3],aCoors[4] / 2,cNameDlg,,,,,CLR_BLACK,CLR_WHITE,,,.T.)
	oFwLayer := FwLayer():New()
	oFwLayer:Init( oDlgM, .f., .t. )
	oFWLayer:AddLine( 'INDIV', 5, .F. )
	oFWLayer:AddLine( 'MOV', 95, .F. )
	oFWLayer:AddCollumn( 'INDIVCOL' , 100, .T., 'INDIV' )
	oFWLayer:AddCollumn( 'MOVCOL' , 100, .T., 'MOV' )
	oPnInd := oFWLayer:GetColPanel( 'INDIVCOL', 'INDIV' )
	oPnMov := oFWLayer:GetColPanel( 'MOVCOL', 'MOV' )

	If nTipo == 1 //Cartas de Controle

		For nI := 1 To len(aDataPar)
	    	If ValType(aDataPar[nI]) == "C"
	    		aDataPar[nI] := SuperVal(aDataPar[nI])
	    	EndIf
	    Next

        IF lXbXbr
			aMedEstat := {}

			FOR nX := 1 TO LEN(aMedicoes)
				FOR nY := 1 TO LEN(aMedicoes[nX])
					AADD(aMedEstat, SUPERVAL(aMedicoes[nX, nY]))
				NEXT nY
			NEXT nX
		ELSE
			aMedEstat := aDataPar
		ENDIF
	    
	    //Valores Individuais--------
	    aJsData := QIEMJsVI(aDataPar)
	    aLim := CalcVI(aDataPar)
	    //---------------------------
	
	    //oKendo := KendoChart():New(oPnInd, 800, 1000, "Valores Individuais", "bottom", .F.)
	    oKendo := KendoChart():New(oPnMov, 495, 742)
	    oKendo:AddChart('chart1',aTitCarCon[1], "bottom", .F., .T., 650, 275)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    TButton():New( 005, 055, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    TButton():New( 005, 105, "Estat�sticas", oPnInd, {|| QIEMESTATI(aMedEstat, aLimites) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    Aadd(aSeries, KendoSeries():New("Valor","y1", "x1", "cat1",, "line","normal"))
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0))
	    Aadd(aLimits, KendoLimit():New("UCL", aLim[1], "red"))
	    Aadd(aLimits, KendoLimit():New("CL", aLim[2], "blue"))
	    Aadd(aLimits, KendoLimit():New("LCL", aLim[3], "green"))
	   // oKendo:SetLabelPadding(135)
	    oKendo:SetCategories('chart1', aCategories)
	    oKendo:SetLimits('chart1', aLimits)
	    oKendo:SetSeries('chart1', aSeries)
	    oKendo:SetData('chart1', aJsData)
	    
	    //Amplitude Movel------------
	    aJsData := QIEMJsAM(aLim[4])
	    aLim := CalcAM(aLim[4])
	    //---------------------------
	    
		aSeries     := {}
		aLimits     := {}
		aCategories := {}
	    
	    oKendo:AddChart('chart2', aTitCarCon[2], "bottom", .T.)
	    Aadd(aSeries, KendoSeries():New("Valor","y1", "x1", "cat1",, "line","normal"))
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0))
	    Aadd(aLimits, KendoLimit():New("UCL", aLim[1], "red"))
	    Aadd(aLimits, KendoLimit():New("CL", aLim[2], "blue"))
	    Aadd(aLimits, KendoLimit():New("LCL", aLim[3], "green"))
	    oKendo:SetCategories('chart2', aCategories)
	    oKendo:SetLimits('chart2', aLimits)
	    oKendo:SetSeries('chart2', aSeries)
	    oKendo:SetData('chart2', aJsData)

    ElseIf nTipo == 2 //Diagrama de Pareto

		ASORT(aDataPar,,, { |x, y| x[1] > y[1] } )

		aResult:= QIEMJsDP(aDataPar, 4)
		aJsData := aResult[1]

		aMedicoes := {}

		FOR nX := 1 TO LEN(aDataPar)
			AADD(aMedicoes, {CVALTOCHAR(aDataPar[nX, 1])})
		NEXT nX

	    oKendo := KendoChart():New(oPnMov, 350, 742)
	    oKendo:AddChart('chart1', "Diagrama de Pareto", "bottom", .F.)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		Aadd(aSeries, KendoSeries():New("Valor","y1", "x1", "cat1", 'y1', "column",,,.T.))
	    Aadd(aSeries, KendoSeries():New("Pareto Acumulado","y2", "x2", "cat2", 'y2', "line",,,.T.))
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0))
	    Aadd(aCategories, KendoCategory():New("cat2",,.F., .T., 0))
		Aadd(aValueAxis, KendoValueAxis():New('y1', 'Valores' ,0, aResult[2]))
		Aadd(aValueAxis, KendoValueAxis():New('y2', 'Acumulado %' ,0, 100, 10))
		oKendo:SetValueAxis('chart1', aValueAxis)
	    oKendo:SetCategories('chart1', aCategories)
	    oKendo:SetSeries('chart1', aSeries)
	    oKendo:SetData('chart1', aJsData)

	ElseIf nTipo == 3 //Histrograma

		For nI := 1 To len(aDataPar)
	    	If ValType(aDataPar[nI]) == "C"
	    		aDataPar[nI] := SuperVal(aDataPar[nI])
	    	EndIf
	    Next

        IF lXbXbr
			aMedEstat := {}

			FOR nX := 1 TO LEN(aMedicoes)
				FOR nY := 1 TO LEN(aMedicoes[nX])
					AADD(aMedEstat, SUPERVAL(aMedicoes[nX, nY]))
				NEXT nY
			NEXT nX
		ELSE
			aMedEstat := aDataPar
		ENDIF

		histArry := Histograma():New("Teste", aDataPar)

		aCol1       := histArry:nb:p
		aCol2       := histArry:frequencia
		aCol3       := aDataPar
		aCortes     := histArry:Cortes
		nPrimeiro   := histArry:nb:pontos[1]
		nIncremento := histArry:nb:nIncrBarra
		nMenor      := histArry:menor
		nMedia      := histArry:media
		nMaior      := histArry:maior

		aadd(aCol2, 0)
		AIns( aCol2, 1 )
		aCol2[1] := 0

		oData := JsonObject():New()
		oData['yDist'] := aCol1[1]
		oData['xDist'] := nPrimeiro
		oData['yFreq'] := aCol2[2]
		oData['xFreq'] := nPrimeiro
		Aadd(aJsData, oData)

		For nI := 2 To Len(aCol2)
			if nI <= len(aCol1)
				oData := JsonObject():New()

				oData['yDist'] := aCol1[nI]
				oData['xDist'] := (nPrimeiro + ((nI) * nIncremento)) - nIncremento

				If nI > 2
					oData['yFreq'] := aCol2[nI]
				EndIf
				oData['xFreq'] := (nPrimeiro + ((nI) * nIncremento)) - nIncremento		

				Aadd(aJsData, oData) 
			EndIf
		Next

		aCol1 := ASORT(aCol1,,, { |x, y| x > y } )
		nMaxDist := aCol1[1]
		aCol2 := ASORT(aCol2,,, { |x, y| x > y } )
		nMaxFreq := aCol2[1]

		aSeries     := {}
		aCategories := {}
		aValueAxis  := {}

		aadd(aSeries, KendoSeries():New("" ,"yDist", "xDist", "cDist", 'yDist', "line",,'red'))
		aadd(aSeries, KendoSeries():New("" ,"yFreq", "xFreq", "cFreq", 'yFreq', "bar",,'#0000FF'))

		aSeries[Len(aSeries)]:RemoveMarkers()
		aSeries[Len(aSeries)]:SetWidth(6)

		Aadd(aCategories, KendoCategory():New("cDist",,.F., .F., 000, .t.))
		Aadd(aCategories, KendoCategory():New("cFreq",,.F., .T., -90, .f.))

		nMenor := (nPrimeiro - nMenor) * nIncremento
		nMedia := (nMedia - nPrimeiro) / nIncremento 
		nMaior := (nMaior - nPrimeiro) / nIncremento

		aCategories[Len(aCategories)]:AddNote(nMenor, '', '#FF0000', "longDashDotDot", 180)
		aCategories[Len(aCategories)]:AddNote(nMedia, '', '#FF0000', "solid",          180)
		aCategories[Len(aCategories)]:AddNote(nMaior, '', '#FF0000', "longDashDotDot", 180)

		aCategories[Len(aCategories)]:SetNoteLength(290) //Altura calculada em pixels

		Aadd(aValueAxis, KendoValueAxis():New('yDist', '' ,0, nMaxDist*1.05)) //5 % a mais para n�o ficar grudado em cima
		Aadd(aValueAxis, KendoValueAxis():New('yFreq', '' ,0, nMaxFreq*1.05))

		oKendo := KendoChart():New(oPnMov, 495, 742)
	    oKendo:AddChart('chart1',"Histograma" , "bottom", .F.)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		TButton():New( 005, 105, "Estat�sticas", oPnInd, {|| QIEMESTATI(aMedEstat, aLimites) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		oKendo:AddChart('chart1', "Histograma", "bottom", .T.)
		oKendo:SetValueAxis('chart1', aValueAxis)
		oKendo:SetCategories('chart1', aCategories)
		oKendo:SetAxisCrossingValue(aCategories, Len(aCol2))
		oKendo:SetSeries('chart1', aSeries)
		oKendo:SetData('chart1', aJsData)
	
	ElseIf nTipo == 4 //Linearidade MSA

		aData := fGetPontos(aDataPar)

		aMedicoes := fGetMedLin(aDataPar[6])

		oKendo := KendoChart():New(oPnMov, 350, 742)
	    oKendo:AddChart('chart1', "Grafico de Linearidade " + aDataPar[1], "bottom", .F.)
		TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		//Regress�o
		Aadd(aSeries, KendoSeries():New("Regressao","y1", "x1", , , "scatterLine","normal","#000000",.F.,))
		//M�dias das T�ndencias
		Aadd(aSeries, KendoSeries():New("Medias das Tendencias","y2", "x2", , , "scatter","normal","#FFE135",.F.,,"cross"))
		//Resultados Intermedi�rios
		Aadd(aSeries, KendoSeries():New("Resultados Intermediarios","y3", "x3", , , "scatter","normal",,.F.,,))
		//Intervalo de Confian�a Superior
		Aadd(aSeries, KendoSeries():New("Intervalo de Confianca Superior","y4", "x4", , , "scatterLine","normal","#ff2800",.F.,"dot"))
		//Intervalo de Confian�a Inferior
		Aadd(aSeries, KendoSeries():New("Intervalo de Confianca Inferior","y5", "x5", , , "scatterLine","normal","#ff2800",.F.,"dot"))
		//cName, cField, cCategoryField, cCategoryAxis, cValueAxis, cType, cStyle, cColor, lLabels, cDashType, cMakerType, lVsbleLeg

		aSeries[1]:RemoveMarkers()
		aSeries[4]:RemoveMarkers()
		aSeries[5]:RemoveMarkers()

		//Encontra o proximo ponto conforme tamanho do menor ponto do eixo xy encontrado(ajuste estetico)
		nYMin := fGetYmin(aData[2])
		nXMin := fGetYmin(aData[3])

		//cEixo, nMin, nMax, nStep, nAxsCrosVal
 		Aadd(aMyAxis, KendoAxis():New("y",nYMin,,,nYMin))
 		Aadd(aMyAxis, KendoAxis():New("x",nXMin,,,nXMin))
		oKendo:SetKendoAxis('chart1', aMyAxis)

		oKendo:SetSeries('chart1', aSeries)
	    oKendo:SetData('chart1', aData[1])

		Aadd(aComments, KendoComments():New("Valor de Referencia " + "Tendencia = "+aDataPar[3]+ " Grau de Ajuste(R2) = " + aDataPar[2]))
		oKendo:SetComments('chart1', aComments)
	
	ElseIf nTipo == 5 // Histrograma Metrologia

		aDados    := {}
		aData     := {}
		aMedicoes := {}
		aMyAxis   := {}
		aMedias   := {}

		For nI := 2 To len(aDataPar)
	    	If ValType(aDataPar[nI]) == "C"
	    		AADD( aDados, SuperVal(aDataPar[nI]) )
			Else
				AADD( aDados, aDataPar[nI] )
	    	EndIf
	    	AADD( aMedicoes, {aDados[LEN(aDados)]} )
	    Next

		oDesvPad   := NB():New(aDados)
		aMedias    := oDesvPad:mediasNB(aDados)
		nDesvPad   := oDesvPad:getDesvPad(aDados)
		nMedMaior  := aMedias[2] + (nDesvPad * 4)
		nMedMenor  := aMedias[2] - (nDesvPad * 4)
		nIncrement := (nMedMaior - nMedMenor) / (100 - 1)
		aValores   := getValores(nMedMenor, nIncrement, 100)
		aNormPad   := getNorPad(aValores, nDesvPad, aMedias[2])

		FOR nX := 1 TO LEN(aValores)
			AADD( aData, {aValores[nX], aNormPad[nX]} )

			oData := JsonObject():New()

			oData['yDist'] := aValores[nX]
			oData['xDist'] := aNormPad[nX]	

			AADD(aJsData, oData)

		NEXT nX

		AADD(aSeries, KendoSeries():New("" ,"xDist", "yDist", "cDist", 'xDist', "scatterLine",,))
		AADD(aMyAxis, KendoAxis():New("x",NOROUND(nMedMenor - 1, 0),NOROUND(nMedMaior + 1, 0),,NOROUND(nMedMenor - 1, 0), 0.5, -90))
		aMyAxis[Len(aMyAxis)]:AddNoteAxis(aLimites[2], '', '#FF0000', "longDashDot", 190) //LSL - (lower specification limit)		
		aMyAxis[Len(aMyAxis)]:AddNoteAxis(aLimites[1], '', '#FF0000', "solid",       190) //REFERENCIA		
		aMyAxis[Len(aMyAxis)]:AddNoteAxis(aLimites[3], '', '#FF0000', "longDashDot", 190) //USL - (upper specification limit) 
		
		oKendo := KendoChart():New(oPnMov, 495, 742)
	    oKendo:AddChart('chart1', ALLTRIM(aDataPar[1]), "bottom", .F.,,,,.F.)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )

		aSeries[1]:RemoveMarkers()
		
		oKendo:SetKendoAxis('chart1', aMyAxis)
		oKendo:SetSeries('chart1', aSeries)
		oKendo:SetData('chart1', aJsData)

	ElseIf nTipo == 6 //Relat�rio �ndice Qualidade

 		aResult:= QIEMJsDP(aDataPar, 4)
		aJsData := aResult[1]

        aMedicoes := {}

		FOR nX := 1 TO LEN(aDataPar)
			AADD(aMedicoes, {CVALTOCHAR(aDataPar[nX, 1])})
		NEXT nX

		oKendo := KendoChart():New(oPnMov, 350, 742)
	    oKendo:AddChart('chart1', aTitCarCon[1], "bottom", .F.,,,,.F.)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		Aadd(aSeries, KendoSeries():New("Produto","y1", "x1", "cat1", 'y1', "column",,,.T.))
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0))
		Aadd(aValueAxis, KendoValueAxis():New('y1', 'IQF' ,0, aResult[2]))
		oKendo:SetValueAxis('chart1', aValueAxis)
	    oKendo:SetCategories('chart1', aCategories)
	    oKendo:SetSeries('chart1', aSeries)
	    oKendo:SetData('chart1', aJsData)

		oKendo:lReport := .T.

	ElseIf nTipo == 7 //Relatorio de Performance de Sistemas de Medi��o 

		aDadosTmp1 := {}
		aDadosSer1 := {}
		aDadosTmp2 := {}
		aDadosSer2 := {}

		nIni := ASCAN( aDataPar, "[INICIO DE DADOS]" )
		nFim := ASCAN( aDataPar, "[FIM DE DADOS]" ) - 1
		nFator := (nFim - nIni) / SUPERVAL(aDataPar[9])

		For nI := 1 To (nFim - nIni)

			nPosVazio := AT(" ", aDataPar[nI + nIni])

			cTempValue := LTRIM(SUBSTR(aDataPar[nI + nIni], nPosVazio, LEN(aDataPar[nI + nIni])))

			nPosVazio := AT(" ", cTempValue)

			cValue1 := (ALLTRIM(SUBSTR(cTempValue, 1, nPosVazio)))
			cValue2 := (ALLTRIM(SUBSTR(cTempValue, nPosVazio, LEN(cTempValue))))

	    	IF Mod( nI, nFator ) == 0 //https://tdn.totvs.com/display/tec/Mod
				AADD( aDadosTmp1, SUPERVAL(cValue1) )
				AADD( aDadosTmp2, SUPERVAL(cValue2) )
				AADD( aDadosSer1, aDadosTmp1 )
				AADD( aDadosSer2, aDadosTmp2 )
				aDadosTmp1 := {}
				aDadosTmp2 := {}
			ELSE
				AADD(aDadosTmp1, SUPERVAL(cValue1))
				AADD(aDadosTmp2, SUPERVAL(cValue2))
			ENDIF

	    Next nI

		nCont := 1

		FOR nX := 1 TO LEN(aDadosSer1)

			For nY := 1 To len(aDadosSer1[nX])
				oData := JsonObject():New()
				oData['y' + CVALTOCHAR( nX )] := aDadosSer1[nX, nY]
				oData['x' + CVALTOCHAR( nX )] := nCont
				oData['z' + CVALTOCHAR( nX )] := aDadosSer2[nX, nY]
				oData['w' + CVALTOCHAR( nX )] := nCont
				nCont ++
				Aadd(aData, oData)
			Next

			IF nX == 1
				cContador := "A"
			ELSE
				cContador := UPPER( __SOMA1( cContador ) )
			ENDIF

			cCor := fRetCorAle()

			Aadd(aSeries, KendoSeries():New("Operador " + cContador,'y' + CVALTOCHAR( nX ), 'x' + CVALTOCHAR( nX ), "cat1",, "line","normal",cCor,,,,,))
			Aadd(aSeries, KendoSeries():New("",                     'z' + CVALTOCHAR( nX ), 'w' + CVALTOCHAR( nX ), "cat1",, "line","normal",cCor,,,,,))

		NEXT nX

		oKendo := KendoChart():New(oPnMov, 495, 742)
	    oKendo:AddChart('chart1', aTitCarCon[1], "bottom", .F., .T., 650, 275, .F.)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    //TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0,,.F.))
	    Aadd(aLimits, KendoLimit():New("Valor Medido", SUPERVAL(aDataPar[3]), "#000000","solid")) //Black
	    Aadd(aLimits, KendoLimit():New("Valor Real", SUPERVAL(aDataPar[5]), "#7fff00","dash")) //Verde Paris
	   	Aadd(aValueAxis, KendoValueAxis():New('y1', 'MEDIA(U.M)' ,0,))
		oKendo:SetValueAxis('chart1', aValueAxis)
	    oKendo:SetCategories('chart1', aCategories)
	    oKendo:SetLimits('chart1', aLimits)
	    oKendo:SetSeries('chart1', aSeries)
	    oKendo:SetData('chart1', aData)
	    
		nIni := ASCAN( aDataPar, "[INICIO AMPLITUDE]" )
		nFim := ASCAN( aDataPar, "[FIM AMPLITUDE]" ) - 1

		aDadosTmp1 := {}
		aDadosSer1 := {}

		For nI := 1 To (nFim - nIni)

			nPosVazio := AT(" ", aDataPar[nI + nIni])

			cTempValue := LTRIM(SUBSTR(aDataPar[nI + nIni], nPosVazio, LEN(aDataPar[nI + nIni])))

			nPosVazio := AT(" ", cTempValue)

			cValue1 := (ALLTRIM(SUBSTR(cTempValue, 1, nPosVazio)))

	    	IF Mod( nI, nFator ) == 0 //https://tdn.totvs.com/display/tec/Mod
				AADD( aDadosTmp1, SUPERVAL(cValue1) )
				AADD( aDadosSer1, aDadosTmp1 )
				aDadosTmp1 := {}
			ELSE
				AADD(aDadosTmp1, SUPERVAL(cValue1))
			ENDIF

	    Next nI

		aSeries     := {}
		aLimits     := {}
		aCategories := {}
		aData       := {}
		aValueAxis  := {}
		nCont       := 1

		FOR nX := 1 TO LEN(aDadosSer1)

			For nY := 1 To len(aDadosSer1[nX])
				oData := JsonObject():New()
				oData['y' + CVALTOCHAR( nX )] := aDadosSer1[nX, nY]
				oData['x' + CVALTOCHAR( nX )] := nCont
				nCont ++
				Aadd(aData, oData)
			Next

			IF nX == 1
				cContador := "A"
			ELSE
				cContador := UPPER( __SOMA1( cContador ) )
			ENDIF

			Aadd(aSeries, KendoSeries():New("Operador " + cContador,'y' + CVALTOCHAR( nX ), 'x' + CVALTOCHAR( nX ), "cat1",, "line","normal",fRetCorAle(),,,,,))

		NEXT nX
	    
		oKendo:AddChart('chart2', aTitCarCon[2], "bottom", .F., .T., 650, 275, .F.)
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0,,.F.))
	    Aadd(aLimits, KendoLimit():New("Media Amplit.", SUPERVAL(aDataPar[7]), "#000000","solid")) //Black
		Aadd(aValueAxis, KendoValueAxis():New('y1', 'AMPLITUDE(U.M)' ,0,))
		oKendo:SetValueAxis('chart2', aValueAxis)
	    oKendo:SetCategories('chart2', aCategories)
	    oKendo:SetLimits('chart2', aLimits)
	    oKendo:SetSeries('chart2', aSeries)
	    oKendo:SetData('chart2', aData)

		oKendo:lReport := .T.
	ElseIf nTipo == 8 //Carta Individual 

		For nI := 1 To len(aDataPar)
	    	If ValType(aDataPar[nI]) == "C"
	    		aDataPar[nI] := SuperVal(aDataPar[nI])
	    	EndIf
	    Next
		 
		aMedEstat := {}

		FOR nX := 1 TO LEN(aMedicoes)
			FOR nY := 1 TO LEN(aMedicoes[nX])
				aAdd(aMedEstat,SUPERVAL(aMedicoes[nX, nY]))
			NEXT nY
		NEXT nX

		//Valores Individuais--------
	    aJsData := QIEMJsVI(aDataPar)
	    aLim := CalLim(aMedEstat)
	    //---------------------------
	
	    oKendo := KendoChart():New(oPnMov, 495, 742)
	    oKendo:AddChart('chart1',aTitCarCon[1], "bottom", .F., .T., 650, 275)
	    TButton():New( 005, 005, "Imprimir", oPnInd, {|| oKendo:Print() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    TButton():New( 005, 55, "Medi��es", oPnInd, {|| QIEMEDICOE(aMedicoes) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		TButton():New( 005, 105, "Estat�sticas", oPnInd, {|| QIEMESTATI(aMedEstat, aLimites) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
	    Aadd(aSeries, KendoSeries():New("Valor","y1", "x1", "cat1",, "line","normal"))
	    Aadd(aCategories, KendoCategory():New("cat1",,.F., .T., 0))
	    Aadd(aLimits, KendoLimit():New("UCL", aLim[1], "red"))
	    Aadd(aLimits, KendoLimit():New("CL", aLim[2], "blue"))
	    Aadd(aLimits, KendoLimit():New("LCL", aLim[3], "green"))
		oKendo:SetCategories('chart1', aCategories)
	    oKendo:SetLimits('chart1', aLimits)
	    oKendo:SetSeries('chart1', aSeries)
		oKendo:SetData('chart1', aJsData)
	
	EndIf
    oDlgM:Activate() 
Return


/*/{Protheus.doc} CalLim
//De acordo com o array de medi��es informado, retorna UCL, Media e LCL dos Valores Individuais
@author thiago.rover
@since 17/08/2020
@version 1.0
@return { nUCL, aMedias[2], nLCL }
@param aDataAM
@type function
/*/
Function CalLim(aDataAM)
	Local oDesvPad    := NB():New(aDataAM)
	Local aMedias     := oDesvPad:mediasNB(aDataAM)
	Local nDesvPad    := oDesvPad:getDesvPad(aDataAM)
	Local nUCL        := 0
	Local nLCL        := 0

	//Limite superior
	nUCL  := (aMedias[2]) + (nDesvPad * 3)
	
	//Limite inferior
	nLCL  := (aMedias[2]) - (nDesvPad * 3)
	
Return { nUCL, aMedias[2], nLCL }


/*/{Protheus.doc} CalcVI
//De acordo com o array informado, retorna UCL,Media,LCL dos Valores Individuais
@author carlos.augusto
@since 20/05/2019
@version 1.0
@return ${return}, ${return_description}
@param aDataPar, array, descricao
@type function
/*/
Function CalcVI(aDataPar)
	Local nMedAmos
	Local nAmostras
	Local nAmpMed
	Local nI
	Local nSoma := 0
	Local nUCL
	Local nLCL
	Local nVlIdxAM := 0
	Local aLocalAM := {}
	
	For nI := 1 To len(aDataPar)
   		nSoma += aDataPar[nI]
   		If nI != 1
			nVlIdxAM :=	ABS(aDataPar[nI] - aDataPar[nI-1])
			aAdd(aLocalAM,nVlIdxAM)
   		EndIf
	Next
	
	nAmostras := len(aDataPar)
	
	//Media Amostra
	nMedAmos := nSoma / nAmostras
	
	nSoma := 0
    
	//Amplitude Media
	For nI := 1 To (nAmostras-1)
   		nSoma += ABS(aLocalAM[nI])
	Next
	
	nAmpMed := nSoma / (nAmostras-1)
    
	//Media MR/2
	nMedMR := nAmpMed/QIEMTabFat(1, 2)
	
	nMedMRK := nMedMR * 3
 
	nUCL	 := nMedAmos + nMedMRK 
	nLCL	 := nMedAmos - nMedMRK
	
Return {nUCL,nMedAmos,nLCL,aLocalAM}


/*/{Protheus.doc} CalcAM
//De acordo com o array informado, retorna UCL,Media,LCL da Amplitude Movel
@author carlos.augusto
@since 20/05/2019
@version 1.0
@return ${return}, ${return_description}
@param aDataAM, array, descricao
@type function
/*/
Function CalcAM(aDataAM)
	Local nAmostras
	Local nAmpMed
	Local nI
	Local nSoma := 0
	Local nUCL
	Local nLCL
		
	nAmostras := len(aDataAM)
	   
	//Amplitude Media
	For nI := 1 To Len(aDataAM)
   		nSoma += ABS(aDataAM[nI])
	Next
	
	nAmpMed := nSoma / nAmostras
    
	//Limite superior
	nUCL	 := QIEMTabFat(1, 8) * nAmpMed
	
	//Limite inferior
	nLCL	 := QIEMTabFat(1, 7) * nAmpMed
	
Return {nUCL,nAmpMed,nLCL}


/*/{Protheus.doc} QIEMJsVI
//Transforma o array XY em objeto json para oKendo:SetData
//Valores Indivisuais
@author carlos.augusto
@since 20/05/2019
@version 1.0
@return ${return}, ${return_description}
@param aDataPar, array, descricao
@type function
/*/
Static Function QIEMJsVI(aDataPar) 
	Local nI
	Local aData as array
	Local oData as array
	aData := {}
	
    For nI := 1 To len(aDataPar)
        oData := JsonObject():New()
        oData['y1'] := aDataPar[nI]
        oData['x1'] := nI
        Aadd(aData, oData)
    Next
    
Return aData


/*/{Protheus.doc} QIEMJsAM
//Transforma o array XY em objeto json para oKendo:SetData
//Amplitude Movel
@author carlos.augusto
@since 20/05/2019
@version 1.0
@return ${return}, ${return_description}
@param aParAM, array, descricao
@type function
/*/
Static Function QIEMJsAM(aParAM) 
	Local nI
	Local aData as array
	Local oData as array
	aData := {}
	
    For nI := 1 To len(aParAM)
        oData := JsonObject():New()
        oData['y1'] := aParAM[nI]
        oData['x1'] := nI
        Aadd(aData, oData)
    Next
    
Return aData

Static Function QIEMJsDP(aParDP, nCasasDec) 
	Local nI
	Local aData as array
	Local oData as array
	Local nSomaTot := 0
	Local nSomaAtual := 0
	aData := {}
	
	For nI := 1 To len(aParDP)
		IF EMPTY(nCasasDec)
			nSomaTot += SuperVal(aParDP[nI][1])
			aParDP[nI][1] := SuperVal(aParDP[nI][1])
		ELSE
			nSomaTot += NOROUND(SuperVal(aParDP[nI][1]), nCasasDec)
			aParDP[nI][1] := NOROUND(SuperVal(aParDP[nI][1]), nCasasDec)
		ENDIF
	Next
	
    For nI := 1 To len(aParDP)
        oData := JsonObject():New()
        oData['y1'] := aParDP[nI][1]
        oData['x1'] := aParDP[nI][2]
        nSomaAtual  += (aParDP[nI][1] / nSomaTot) * 100 
        oData['y2'] := nSomaAtual
        oData['x2'] := nI
        Aadd(aData, oData)
    Next
    
Return {aData, nSomaTot}


/*/{Protheus.doc} QIEMTabFat
//Retorna o fator com base no numero de elementos que formam uma amostra.
@author carlos.augusto
@since 20/05/2019
@version 1.0
@return ${return}, ${return_description}
@param nIdx, numeric, descricao
@param nFat, numeric, descricao
@type function
/*/
Function QIEMTabFat(nIdx, nFat)

	Local aFatores := {}
	Local nValor   := 0

	//			    Fatores para Linha Central|Fatores para Limites de Controle|
	//Nro de Eleme Amostra  d2     1/d2     d3   D1         D2   D3         D4
	Aadd(aFatores,{2	,1.128	,0.8865	,0.853	,0		,3.686	,0		,3.267})
	Aadd(aFatores,{3	,1.693	,0.5907	,0.888	,0		,4.358	,0		,2.574})
	Aadd(aFatores,{4	,2.059	,0.4857	,0.880	,0		,4.698	,0		,2.282})
	Aadd(aFatores,{5	,2.326	,0.4299	,0.864	,0		,4.918	,0		,2.114})
	Aadd(aFatores,{6	,2.534	,0.3946	,0.848	,0		,5.078	,0		,2.004})
	Aadd(aFatores,{7	,2.704	,0.3698	,0.833	,0.204	,5.204	,0.076	,1.924})
	Aadd(aFatores,{8	,2.847	,0.3512	,0.820	,0.388	,5.306	,0.136	,1.864})
	Aadd(aFatores,{9	,2.970	,0.3367	,0.808	,0.547	,5.393	,0.184	,1.816})
	Aadd(aFatores,{10	,3.078	,0.3249	,0.797	,0.687	,5.469	,0.223	,1.777})
	Aadd(aFatores,{11	,3.173	,0.3152	,0.787	,0.811	,5.535	,0.256	,1.744})
	Aadd(aFatores,{12	,3.258	,0.3069	,0.778	,0.922	,5.594	,0.283	,1.717})
	Aadd(aFatores,{13	,3.336	,0.2998	,0.770	,1.025	,5.647	,0.307	,1.693})
	Aadd(aFatores,{14	,3.407	,0.2935	,0.763	,1.118	,5.696	,0.328	,1.672})
	Aadd(aFatores,{15	,3.472	,0.2880	,0.756	,1.203	,5.741	,0.347	,1.653})
	
	nValor := aFatores[nIdx][nFat]


Return nValor

/*/{Protheus.doc} fGetPontos
Converte o array com as coordenadas no formato caractere em coordenadas json
@type  Static Function
@author rafael.kleestadt / lucas.briesemeister
@since 30/04/2020
@version 1.0
@param aDataPar, array, array com as coordenadas no formato {{cPontox, cPontoy}}
@return aData, array, array com as coordenadas no formato json {[x1:nPontox, y1:nPontoy]}
@example
(examples)
@see (links_or_references)
/*/
Static Function fGetPontos(aDataPar)
Local nX         := 0
Local nY         := 0
Local nPosPtVigr := 0
Local nXValue    := 0
Local nYValue    := 0
Local nMenorY    := 0
Local nMenorX    := 0
Local aData      := {}
Local oData      := {}
Local aRet       := {}
LOCAL cIndex     := ""

FOR nX := 4 TO LEN(aDataPar)
	cIndex := cValToChar(nX - 3)
	FOR nY := 1 TO LEN(aDataPar[nX])
		IF VALTYPE(aDataPar[nX, nY]) = "C"

			nPosPtVigr := 0 
			nXValue    := 0 
			nYValue    := 0 

			nPosPtVigr := AT(";", aDataPar[nX,nY])
			nXValue    := SUPERVAL(ALLTRIM(SUBSTR(aDataPar[nX,nY], 1, AT(";", aDataPar[nX,nY]) - 1)))
			nYValue    := SUPERVAL(ALLTRIM(SUBSTR(aDataPar[nX,nY], nPosPtVigr + 1, LEN(aDataPar[nX,nY]))))

			//Guarda o menor ponto do eixo y
			IF nYValue < nMenorY .OR. nX == 1
				nMenorY := nYValue
			ENDIF

			//Guarda o menor ponto do eixo x
			IF nXValue < nMenorX .OR. nX == 4
				nMenorX := nXValue
			ENDIF

			oData := JsonObject():New()
            oData['x' + cIndex] := nXValue
            oData['y' + cIndex] := nYValue
            Aadd(aData, oData)
			
		ENDIF
	NEXT nY
NEXT nX

aRet := {aData, nMenorY, nMenorX}

RETURN aRet

/*/{Protheus.doc} fGetYmin
Encontra o proximo ponto conforme tamanho do menor ponto do eixo y encontrado(ajuste estetico)
@type  Static Function
@author rafael.kleestadt
@since 30/04/2020
@version 1.0
@param nMinValue, number, menor ponto do eixo y encontrado
@return nRet, number, proximo ponto onde deve ser posicionado o eixo x com base no ponto calculado para o eixo y
@example
nMinValue := 5,10, nRet := 6
nMinValue := 50,10, nRet := 60
nMinValue := 500,10, nRet := 510
nMinValue := 5000,10, nRet := 5100
@see (links_or_references)
/*/
Static Function fGetYmin(nMinValue)
Local nCasas    := 0
Local nValueMin := 0
Local nRet      := 0

nCasas := LEN(cValtochar(ROUND(ABS(nMinValue), 0))) //positiva, arredonda e conta as casas at� o ponto

nValueMin := SUPERVAL(PADR( "1", nCasas, "0" )) // calcula o valor a ser somado ao menor ponto do eixo y

IF nMinValue < 0
	nRet := ROUND(ABS(nMinValue) + nValueMin, 0) // soma e arredonda o step ao menor ponto do eixo y
ELSE
	nRet := ROUND(ABS(nMinValue) - nValueMin, 0) // soma e arredonda o step ao menor ponto do eixo y
ENDIF

IF nMinValue < 0 //se negativo torna o valor negativo novamente
	nRet -= nRet*2
ENDIF
	
Return nRet

/*/{Protheus.doc} QIEMEDICOE()
Cria browse das medi��es
@type  Function
@author rafael.kleestadt
@since 24/06/2020
@version 1.0
@param aMedicoes, array, matriz de medi��es
@return return_var, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Function QIEMEDICOE(aMedicoes)
Local aSize       := FWGetDialogSize(oMainWnd) // Obt�m a a �rea de trabalho e tamanho da dialog
Local oDlgMed     := NIL
PRIVATE oBrwMed   := NIL	
DEFAULT aMedicoes := {{""}}

	oDlgMed := TDialog():New(0,0,(aSize[3] / 2),(aSize[4] / 3),"Amostras",,,,,CLR_BLACK,CLR_WHITE,,,.T.)
	oBrwMed := FWBrowse():New(oDlgMed)
	oBrwMed:SetDataArray()
	oBrwMed:SetDescription("Amostras")  // "Amostras"
    oBrwMed:DisableFilter()
    oBrwMed:DisableConfig()
	oBrwMed:SetArray( aMedicoes ) // Define Fwbrowse para receber um Array.
	
    MBColumn(aMedicoes) //Adiciona as colunas no Browse
    oBrwMed:Activate()
 	oBrwMed:Enable()
    oBrwMed:Refresh(.T.)
	oDlgMed:Activate()
	
Return NIL

/*/{Protheus.doc} MBColumn
Cria as colunas de medi��es dinamicamente
@type  Static Function
@author rafael.kleestadt
@since 24/06/2020
@version 1.0
@param aCols, array, matriz de medi��es
@return return_var, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function MBColumn(aCols)
Local nX		:= 0

For nX := 1 To Len(aCols[1])
	oColumn := FWBrwColumn():New()
	oColumn:SetData( &(" { || aCols[oBrwMed:At(),"+STransType(nX)+"]}") )
	oColumn:SetTitle(CVALTOCHAR(nX))
	oColumn:SetSize(TamSx3("QPS_MEDICA")[1]) 
	oColumn:SetDecimal(TamSx3("QPS_MEDICA")[2]) 
	oBrwMed:SetColumns({oColumn})
Next nX 

Return NIL

/*/{Protheus.doc} fGetMedLin
Cria array com base nas medi��es para exibir na tela de medi��es
@type  Static Function
@author rafael.kleestadt
@since 30/04/2020
@version 1.0
@param aDados, array, array com as medi��es
@return aRet, array, array com as medi��es no formato {[x1:nPontox, y1:nPontoy]}
@example
(examples)
@see (links_or_references)
/*/
Static Function fGetMedLin(aDados)
Local nX         := 0
Local nXValue    := 0
Local nYValue    := 0
Local nPosPtVigr := 0
Local aRet       := {}

FOR nX := 1 TO LEN(aDados)

	nPosPtVigr := 0 
	nXValue    := 0 
	nYValue    := 0 

	nPosPtVigr := AT(";", aDados[nX])
	nXValue    := SUPERVAL(ALLTRIM(SUBSTR(aDados[nX], 1, nPosPtVigr - 1)))
	nYValue    := SUPERVAL(ALLTRIM(SUBSTR(aDados[nX], nPosPtVigr + 1, LEN(aDados[nX]))))

	AADD(aRet, {nXValue, nYValue})
			
NEXT nX

RETURN aRet

/*/{Protheus.doc} getValores
Gera um vetor de dados com base no menor ponto qtd de pontos e incremnto
@type  Static Function
@author rafael.kleestadt
@since 08/07/2020
@version 1.0
@param nMedMenor, numerico, media das medi��es menos quatro vezes o desvio padr�o das medi��es
@param nIncrement, numerico, media das medi��es mais quatro vezes o desvio padr�o das medi��es menos a media menor dividida pela quantidade de pontos menos 1
@param nQtdPontos, numerico, qtd de pontos que se deja para gerar o sino
@return aValores, array, vetor de pontos distribuidos entre a media menor e maior
@example
(examples)
@see (links_or_references)
/*/
Static Function getValores(nMedMenor, nIncrement, nQtdPontos)
Local nX       := 0
Local aValores := ARRAY(nQtdPontos)

For nX := 1 To nQtdPontos

	If nX == 1
		aValores[nX] := nMedMenor
	Else
		aValores[nX] := aValores[nX - 1] + nIncrement
	EndIf

Next nX

Return aValores

/*/{Protheus.doc} getNorPad
Retorna um array com os valores da distribui��o normal de cada ponto(aPontos)
@type  Function
@author rafael.kleestadt / marcos.wagner
@since 06/07/2020
@version 1.1
@param aPontos, array, pontos calculados com base no desvio padr�o das medi��es
@param nDesvPad, number, desvio padr�o das medi��es
@param nMedia, number, m�dia das medi��es
@return aFPM, array, array com os valores da distribui��o normal de cada ponto(aPontos)
@example
(examples)
@see https://pt.wikipedia.org/wiki/Distribui%C3%A7%C3%A3o_normal
/*/
Function getNorPad(aPontos, nDesvPad, nMedia)
Local x_t        := {}
Local aFPM       := {}
Local aPadroes   := {}
Local q          := {}
Local nI         := 0
Local nEuler     := 2.718281828459045235360287 //Constante de Euler
Local nPi        := 3.141592653589793238462643 //N�mero pi
DEFAULT nDesvPad := ::getDesvPad(aPontos)
DEFAULT nMedia   := ::mediasNB(aPontos)[2]

	For nI := 1 to Len(aPontos)
		AADD(x_t, aPontos[nI])
	Next

	xt := aClone(x_t)

	p        := Array(Len(xt)) // density probability of normal distriobution
	aPadroes := Array(Len(xt)) // temporary data
	q        := Array(Len(xt)) // temporary data

	For nI := 1 to Len(x_t)
		aPadroes[nI] := (xt[nI] - nMedia) / nDesvPad //Padroniza��o (Z)
		q[nI]        := -0.5 * (aPadroes[nI] ^ 2.0) // -Z ao quadrado / 2
		p[nI]        := ((1.0) / ((nDesvPad * ( (2.0 * nPi) ^ (0.5)))) * (nEuler ^ q[nI]))
		AADD(aFPM,p[nI])
	Next

Return aFPM

/*/{Protheus.doc} QIEMIMGGRAF()
Gera a imagem png recebida do JS e copia para a pasta temp com o nome recebido da fun��o
@type  Function
@author rafael.kleestadt
@since 17/07/2020
@version 1.0
@param self, object, objeto twebengine
@param cCodeContent, caracter, imagen png do grafico no formato base64
@param cPathPng, caracter, locl temporario da imagen o grafico
@return return_var, return_type, return_description
@example
(examples)
@see https://pt.wikipedia.org/wiki/Base64
/*/
Function QIEMIMGGRAF(self, cCodeContent, cPathPng)
Local cPngRet    := ""
DEFAULT cNamePng := ::aOptions[1]["chartId"] + ".png"

cPngRet  := GetTempPath() + SUBSTR(cNamePng, 1, AT(".", cNamePng)) + "png"

nPosPtVigr := AT(",", cCodeContent)
        
cData := SUBSTR(cCodeContent, nPosPtVigr + 1, LEN(cCodeContent))

nHandle := fcreate(cPngRet)
FWrite(nHandle, Decode64(cData))
fclose(nHandle)

IF (GetRemoteType() == 5)
	DO CASE
		CASE FWIsInCallStack("QIER120")
			cPngRetAdi  := GetTempPath() + "QIER120_" + SUBSTR(cNamePng, 1, AT(".", cNamePng)) + "png"
		CASE FWIsInCallStack("QIER130")
			cPngRetAdi  := GetTempPath() + "QIER130_" + SUBSTR(cNamePng, 1, AT(".", cNamePng)) + "png"
		CASE FWIsInCallStack("QIER300")
			cPngRetAdi  := GetTempPath() + "QIER300_" + SUBSTR(cNamePng, 1, AT(".", cNamePng)) + "png"
		CASE FWIsInCallStack("QMTR190")
			cPngRetAdi  := GetTempPath() + "QMTR190_" + SUBSTR(cNamePng, 1, AT(".", cNamePng)) + "bmp"
	ENDCASE

	nHandle := fcreate(cPngRetAdi)
	FWrite(nHandle, Decode64(cData))
	fclose(nHandle)
ENDIF

//Fecha a dialog ap�s 1 segundo
oTimer := TTimer():New(1000, {|| ::OWEBENGINE:OWND:End() }, ::OWEBENGINE:OWND )
oTimer:Activate()
	
Return NIL

/*/{Protheus.doc} fRetCorAleat
Retorna uma cor aleat�ria gerada com base numa paleta de 190 cores nomeadas que variam de "Alizarina" at� " �ndigo" passando por " Verde Paris" e " Rosa brilhante".
@type  Static Function
@author rafael.kleestadt
@since 22/07/2020
@version 1.0
@param param_name, param_type, param_descr
@return cCor, caracter, cor hexadecimal aleat�ria
@example
(examples)
@see https://encycolorpedia.pt/named
/*/
Static Function fRetCorAle()
LOCAL cCor   := "" 
LOCAL aCores := {"#e32636",;//Alizarina
                 "#ffff00",;//Amarelo
                 "#ffffe0",;//Amarelo claro
                 "#adff2f",;//Amarelo esverdeado
                 "#fafad2",;//Amarelo ouro claro
                 "#eead2d",;//Amarelo queimado
                 "#dda0dd",;//Ameixa
                 "#9966cc",;//Ametista
                 "#ffebcd",;//Am�ndoa
                 "#7ba05b",;//Aspargo
                 "#0000ff",;//Azul
                 "#f0f8ff",;//Azul alice
                 "#6a5acd",;//Azul ard�sia
                 "#8470ff",;//Azul ard�sia claro
                 "#483d8b",;//Azul ard�sia escuro
                 "#7b68ee",;//Azul ard�sia m�dio
                 "#b8cad4",;//Azul areado
                 "#4682b4",;//Azul a�o
                 "#b0c4de",;//Azul a�o claro
                 "#5f9ea0",;//Azul cadete
                 "#054f77",;//Azul camarada
                 "#f0ffff",;//Azul celeste
                 "#007fff",;//Azul celeste brilhante
                 "#add8e6",;//Azul claro
                 "#0047ab",;//Azul cobalto
                 "#87ceeb",;//Azul c�u
                 "#87cefa",;//Azul c�u claro
                 "#00bfff",;//Azul c�u profundo
                 "#00008b",;//Azul escuro
                 "#6495ed",;//Azul flor de milho
                 "#5d8aa8",;//Azul for�a a�rea
                 "#1e90ff",;//Azul furtivo
                 "#a6aa3e",;//Azul manteiga
                 "#120a8f",;//Azul marinho
                 "#191970",;//Azul meia-noite
                 "#0000cd",;//Azul m�dio
                 "#084d6e",;//Azul petr�leo
                 "#b0e0e6",;//Azul p�lvora
                 "#4169e1",;//Azul real
                 "#248eff",;//Azul taparuere
                 "#8a2be2",;//Azul violeta
                 "#f4c430",;//A�afr�o
                 "#f5f5dc",;//Bege
                 "#800000",;//Bord�
                 "#900020",;//Borgonha
                 "#ffffff",;//Branco
                 "#faebd7",;//Branco antigo
                 "#f8f8ff",;//Branco fantasma
                 "#fffaf0",;//Branco floral
                 "#f5f5f5",;//Branco fuma�a
                 "#ffdead",;//Branco navajo
                 "#cd7f32",;//Bronze
                 "#f0e68c",;//Caqui
                 "#bdb76b",;//Caqui escuro
                 "#8b5742",;//Caramelo
                 "#d8bfd8",;//Cardo
                 "#dc143c",;//Carmesim
                 "#712f26",;//Carmim
                 "#f5fffb",;//Carmim carn�ceo
                 "#8b0000",;//Castanho avermelhado / Vermelho escuro
                 "#d2b48c",;//Castanho claro
                 "#ed9121",;//Cenoura
                 "#de3163",;//Cereja
                 "#f400a1",;//Cereja Hollywood
                 "#d2691e",;//Chocolate
                 "#e0ffff",;//Ciano claro
                 "#008b8b",;//Ciano escuro
                 "#808080",;//Cinza
                 "#708090",;//Cinza ard�sia
                 "#778899",;//Cinza ard�sia claro / Dainise
                 "#2f4f4f",;//Cinza ard�sia escuro
                 "#d3d3d3",;//Cinza claro
                 "#a9a9a9",;//Cinza escuro
                 "#696969",;//Cinza fosco
                 "#dcdcdc",;//Cinza m�dio
                 "#b87333",;//Cobre
                 "#fff5ee",;//Concha
                 "#ff7f50",;//Coral
                 "#f08080",;//Coral claro
                 "#f0dc82",;//Couro
                 "#fffdd0",;//Creme
                 "#ffe4c4",;//Creme de marisco
                 "#f5fffa",;//Creme de menta
                 "#daa520",;//Dourado
                 "#b8860b",;//Dourado escuro
                 "#eee8aa",;//Dourado p�lido
                 "#ff2400",;//Escarlate
                 "#50c878",;//Esmeralda
                 "#d19275",;//Feldspato
                 "#b7410e",;//Ferrugem
                 "#3d2b1f",;//Fuligem
                 "#ff00ff",;//F�chsia / Magenta
                 "#831d1c",;//Gren�
                 "#2e8b57",;//Herbal
                 "#000000",;//Jabuti preto / Preto
                 "#00a86b",;//Jade
                 "#ff4500",;//Jambo
                 "#ffa500",;//Laranja
                 "#ff8c00",;//Laranja escuro
                 "#e6e6fa",;//Lavanda
                 "#fff0f5",;//Lavanda avermelhada
                 "#c8a2c8",;//Lil�s
                 "#fde910",;//Lima
                 "#00ff00",;//Lim�o (cor) / Verde espectro
                 "#faf0e6",;//Linho
                 "#deb887",;//Madeira
                 "#8b008b",;//Magenta escuro
                 "#e0b0ff",;//Malva
                 "#ffefd5",;//Mam�o batido
                 "#f0fff0",;//Man�
                 "#fffff0",;//Marfim
                 "#964b00",;//Marrom
                 "#f4a460",;//Marrom amarelado
                 "#a52a2a",;//Marrom claro
                 "#bc8f8f",;//Marrom rosado
                 "#8b4513",;//Marrom sela
                 "#fbec5d",;//Milho
                 "#fff8dc",;//Milho Claro
                 "#ffe4b5",;//Mocassim
                 "#ffdb58",;//Mostarda
                 "#000080",;//Naval
                 "#fffafa",;//Neve
                 "#cc7722",;//Ocre
                 "#808000",;//Oliva
                 "#556b2f",;//Oliva escura
                 "#6b8e23",;//Oliva parda
                 "#da70d6",;//Orqu�dea
                 "#9932cc",;//Orqu�dea escura
                 "#ba55d3",;//Orqu�dea m�dia
                 "#ffd700",;//Ouro
                 "#cd853f",;//Pele
                 "#c0c0c0",;//Prata
                 "#ffdab9",;//P�ssego
                 "#800080",;//P�rpura
                 "#9370db",;//P�rpura m�dia
                 "#111111",;//Quantum
                 "#fdf5e6",;//Renda antiga
                 "#ffcbdb",;//Rosa
                 "#ff007f",;//Rosa brilhante
                 "#fc0fc0",;//Rosa chocante
                 "#ffb6c1",;//Rosa claro
                 "#ffe4e1",;//Rosa emba�ado
                 "#ff69b4",;//Rosa forte
                 "#ff1493",;//Rosa profundo
                 "#993399",;//Roxo
                 "#6d351a",;//R�tilo
                 "#fa7f72",;//Salm�o
                 "#ffa07a",;//Salm�o claro
                 "#e9967a",;//Salm�o escuro
                 "#ff8247",;//Siena
                 "#705714",;//S�pia
                 "#e2725b",;//Terracota
                 "#b22222",;//Tijolo refrat�rio
                 "#ff6347",;//Tomate
                 "#f5deb3",;//Trigo
                 "#ff2401",;//Tri�ssico
                 "#40e0d0",;//Turquesa
                 "#00ced1",;//Turquesa escura
                 "#48d1cc",;//Turquesa m�dia
                 "#afeeee",;//Turquesa p�lida
                 "#ec2300",;//Urucum
                 "#008000",;//Verde
                 "#9acd32",;//Verde amarelado
                 "#90ee90",;//Verde claro
                 "#006400",;//Verde escuro
                 "#228b22",;//Verde floresta
                 "#ccff33",;//Verde fluorescente
                 "#7cfc00",;//Verde grama
                 "#32cd32",;//Verde lima
                 "#20b2aa",;//Verde mar claro
                 "#8fbc8f",;//Verde mar escuro
                 "#3cb371",;//Verde mar m�dio
                 "#78866b",;//Verde militar
                 "#7fff00",;//Verde Paris
                 "#00ff7f",;//Verde primavera
                 "#00fa9a",;//Verde primavera m�dio
                 "#98fb98",;//Verde p�lido
                 "#008080",;//Verde-azulado
                 "#ff0000",;//Vermelho
                 "#cd5c5c",;//Vermelho indiano
                 "#d02090",;//Vermelho violeta
                 "#c71585",;//Vermelho violeta m�dio
                 "#db7093",;//Vermelho violeta p�lido
                 "#ee82ee",;//Violeta
                 "#9400d3",;//Violeta escuro
                 "#00ffff",;//�gua / Ciano
                 "#7fffd4",;//�gua-marinha
                 "#66cdaa",;//�gua-marinha m�dia
                 "#ffbf00",;//�mbar
                 "#4b0082"} //�ndigo

				 cCor := aCores[Randomize( 1, LEN(aCores) )]
	
Return cCor

/*/{Protheus.doc} QIEMESTATI
Calcula, exibe e exporta os dados estatisticos do grafico
@type  Function
@author rafael.kleestadt
@since 23/07/2020
@version 1.0
@param aMedicoes, array, array com as medi��es
@param aLimites, array, "[TARGET]", "[LSL]", "[USL]"
@return return_var, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Function QIEMESTATI(aMedicoes, aLimites)
LOCAL oDesvPad    := NB():New(aMedicoes)
LOCAL aMedias     := oDesvPad:mediasNB(aMedicoes)
LOCAL nDesvPad    := oDesvPad:getDesvPad(aMedicoes)
LOCAL nCp         := TRANSFORM((aLimites[3] - aLimites[2]) / (6 * nDesvPad),"@E 999.99999")       //Cp = (USL � LSL) / (6*  Standard Deviation)
LOCAL nCpk        := fCalcCpk(aMedias, aLimites, nDesvPad)                                        //Cpk = Min (Cpl, Cpu)
LOCAL nCg         := TRANSFORM((2 * (aLimites[3] - aLimites[2])) / (6 * nDesvPad),"@E 999.99999") //Cg = (2* (USL � LSL)) / (6* Standard Deviation)
LOCAL nCgk        := TRANSFORM(((aLimites[3] - aMedias[2]) / (3 * nDesvPad)),"@E 999.99999")      //Cgk = (USL - Process Mean) / (3* Standard Deviation)
LOCAL nPp         := TRANSFORM((aLimites[3] - aLimites[2]) / (6 * nDesvPad),"@E 999.99999")       //Pp = (USL � LSL) / (6* Standard Deviation)
LOCAL nPpk        := fCalcPpk(aMedias, aLimites, nDesvPad)                                        //Ppk = min( (Process Mean - LSL) / (3* Standard Deviation), (USL - Process Mean) / (3* Standard Deviation))
LOCAL nCr         := TRANSFORM((1 / SUPERVAL(nCp)),"@E 999.99999")                                //Cr = 1/ Cp =  (6*  Standard Deviation) /  (USL � LSL)
LOCAL nPr         := TRANSFORM((1 / SUPERVAL(nCp)),"@E 999.99999")                                //Pr = 1/ Cp =  (6*  Standard Deviation) /  (USL � LSL)
LOCAL nCpl		  := TRANSFORM(((aMedias[2] - aLimites[2]) / (3 * nDesvPad)),"@E 999.99999")      //Cpl = (Process Mean � LSL)/(3*Standard Deviation)
LOCAL nCpu        := TRANSFORM(((aLimites[3] - aMedias[2]) / (3 * nDesvPad)),"@E 999.99999")      //Cpu = (USL � Process Mean)/(3*Standard Deviation)
LOCAL nSigma      := TRANSFORM(nDesvPad,"@E 999.99999")                                           //Sigma = Standard Deviation 
LOCAL nEstSigma   := TRANSFORM(( aMedias[2] / fRetValTab(aMedicoes, "d2") ),"@E 999.99999")       //Est. Sigma = (Process Mean / constant for calculating the control limit)
LOCAL nMean       := TRANSFORM(aMedias[2],"@E 999.99999")                                         //Mean = Process Mean
LOCAL nKurtosis   := TRANSFORM(0,"@E 999.99999")
LOCAL nN          := TRANSFORM(LEN(aMedicoes),"@E 999.99999")                                     //N = Number of samples
LOCAL nSkew       := TRANSFORM(0,"@E 999.99999")
LOCAL aCabExcel   := {"Cp", "Cg", "Pp", "Cr", "Cpl", "Cpu", "Sigma", "Mean", "N", "Cpk", "Cgk", "Ppk", "Pr", "Est. Sigma", "Kurtosis", "Skew"}
LOCAL aItensExcel := {nCp, nCg, nPp, nCr,nCpl, nCpu, nSigma, nMean, nN, nCpk, nCgk, nPpk, nPr, nEstSigma, nKurtosis, nSkew }

	DEFINE DIALOG oDlg TITLE "Estat�sticas" FROM 10, 0 TO 300,330 PIXEL
		@ 0.3 ,0008 SAY "�ndices do gr�fico"
		@ 125 ,0003 BUTTON oBtnExpot PROMPT "Exportar" SIZE 60, 14 ACTION fExport(aCabExcel, aItensExcel) OF oDlg PIXEL

		@ 1.3 ,00.5 SAY "Cp"
		@ 1.3 ,0003	MSGET nCp 	WHEN .F.
		@ 1.3 ,0010	SAY "Cpk"
		@ 1.3 ,13.5	MSGET nCpk 	WHEN .F.

		@ 2.3 ,00.5 SAY "Cg"
		@ 2.3 ,0003	MSGET nCg  	WHEN .F.
		@ 2.3 ,0010 SAY "Cgk"
		@ 2.3 ,13.5	MSGET nCgk  WHEN .F.

		@ 3.3 ,00.5 SAY "Pp"
		@ 3.3 ,0003	MSGET nPp  	WHEN .F.
		@ 3.3 ,0010 SAY "Ppk"
		@ 3.3 ,13.5	MSGET nPpk 	WHEN .F.

		@ 4.3 ,00.5 SAY "Cr"
		@ 4.3 ,0003	MSGET nCr  	WHEN .F.
		@ 4.3 ,0010 SAY "Pr"
		@ 4.3 ,13.5	MSGET nPr  	WHEN .F.
		
		@ 5.3 ,00.5 SAY "Cpu"
		@ 5.3 ,0003 MSGET nCpu 	WHEN .F.
		@ 5.3 ,0010 SAY "Est.Sigma"
		@ 5.3 ,13.5	MSGET nEstSigma	WHEN .F.

		@ 6.3 ,00.5 SAY "Sigma"    
		@ 6.3 ,0003	MSGET nSigma 	WHEN .F.
		@ 6.3 ,0010 SAY "N"   
		@ 6.3 ,13.5 MSGET nN    WHEN .F.
		//@ 6.3 ,0010 SAY "Est.Sigma"
		//@ 6.3 ,13.5	MSGET nEstSigma	WHEN .F.

		@ 7.3 ,00.5 SAY "Mean"        
		@ 7.3 ,0003	MSGET nMean  	WHEN .F.
		//@ 7.3 ,0010 SAY "Kurtosis"
		//@ 7.3 ,13.5	MSGET nKurtosis	WHEN .F.

		//@ 8.3 ,00.5 SAY "N"   
		//@ 8.3 ,0003 MSGET nN    WHEN .F.
		//@ 8.3 ,0010 SAY "Skew"
		//@ 8.3 ,13.5	MSGET nSkew WHEN .F.

	ACTIVATE MSDIALOG oDlg CENTERED 
	
Return NIL

/*/{Protheus.doc} fExport
Funcao que exporta os valores da tela para o Microsoft Excel no formato .CSV
@type  Static Function
@author rafael.kleestadt
@since 23/07/2020
@version 1.0
@param aCabExcel, array, array contendo os campos para o cabe�alho
@return aItensExcel, array, array de dados
@example
(examples)
@see https://tdn.totvs.com/x/LoJdAg
/*/
Static Function fExport(aCabExcel, aItensExcel)

MsgRun("Favor Aguardar.....", "Exportando os Registros para o Excel",;
{||DlgToExcel({{"CABECALHO", "Dados estat�sticos do gr�fico",;
aCabExcel,aItensExcel}})})
	
Return NIL

/*/{Protheus.doc} fCalcCpk(aMedias, aLimites, nDesvPad)
Calcula o Cpk
@type  Static Function
@author rafael.kleestadt
@since 23/07/2020
@version 1.0
@param aMedias, array, array com as medias da medi��o
@param aLimites, array, "[TARGET]", "[LSL]", "[USL]"
@param nDesvPad, float, desvio padr�o
@return nCpk, float, Cpk
@example
(examples)
@see (links_or_references)
/*/
Static Function fCalcCpk(aMedias, aLimites, nDesvPad)
Local nCpkInf := (aMedias[2] - aLimites[2]) / (3 * nDesvPad)
Local nCpkSup := (aLimites[3] - aMedias[2]) / (3 * nDesvPad)
Local nCpk    := TRANSFORM(IIF(nCpkInf < nCpkSup, nCpkInf, nCpkSup),"@E 999.99999")
	
Return nCpk

/*/{Protheus.doc} fCalcPpk(aMedias, aLimites, nDesvPad)
Calcula o Ppk
@type  Static Function
@author rafael.kleestadt
@since 23/07/2020
@version 1.0
@param aMedias, array, array com as medias da medi��o
@param aLimites, array, "[TARGET]", "[LSL]", "[USL]"
@param nDesvPad, float, desvio padr�o
@return nCpk, float, Ppk
(examples)
@see (links_or_references)
/*/
Static Function fCalcPpk(aMedias, aLimites, nDesvPad)
Local nPpkInf := (aMedias[2] - aLimites[2]) / (3 * nDesvPad)
Local nPpkSup := (aLimites[3] - aMedias[2]) / (3 * nDesvPad)
Local nPpk    := TRANSFORM(IIF(nPpkInf < nPpkSup, nPpkInf, nPpkSup),"@E 999.99999")
	
Return nPpk

/*/{Protheus.doc} fRetValTab
Retorna a constante conforme a tabela de valores das constanes para c�lculo dos limites de controle
@type  Static Function
@author rafael.kleestadt
@since 24/07/2020
@version 1.0
@param aMedicoes, array, array de medi��es
@param cType, caracere, tipo da constantea ser retornada
@return nValor, float, constante para c�lculo do limite de controle
@example
(examples)
@see https://www.iso.org/standard/15366.html
/*/
Static Function fRetValTab(aMedicoes, cType)
Local aTabela := {}
Local aTipos  := {"A", "A2", "A3", "B3", "B4", "B5", "B6", "D1", "D2", "D3", "D4", "c4", "1/c4", "d2", "1/d2"}
Local nCol    := ASCAN( aTipos, cType )
Local nLin    := IIF(LEN(aMedicoes) <= 25, LEN(aMedicoes) - 1, 24)
Local nValor  := 0

//                   Fatores para Limites de Controle                                             Fatores para Linha Central
// n                 A      A2     A3     B3     B4     B5     B6     D1     D2     D3     D4     c4      1/c4    d2     1/d2
/*02*/AADD(aTabela, {2.121, 1.880, 2.659, 0.000, 3.267, 0.000, 2.606, 0.000, 3.686, 0.000, 3.267, 0.7979, 1.2533, 1.128, 0.8865} )
/*03*/AADD(aTabela, {1.732, 1.023, 1.954, 0.000, 2.568, 0.000, 2.276, 0.000, 4.358, 0.000, 2.574, 0.8862, 1.1284, 1.693, 0.5907} )
/*04*/AADD(aTabela, {1.500, 0.729, 1.628, 0.000, 2.266, 0.000, 2.088, 0.000, 4.698, 0.000, 2.282, 0.9213, 1.0854, 2.059, 0.4857} )
/*05*/AADD(aTabela, {1.342, 0.577, 1.427, 0.000, 2.089, 0.000, 1.964, 0.000, 4.918, 0.000, 2.114, 0.9400, 1.0638, 2.326, 0.4299} )
/*06*/AADD(aTabela, {1.225, 0.483, 1.287, 0.030, 1.970, 0.029, 1.874, 0.000, 5.078, 0.000, 2.004, 0.9515, 1.0510, 2.534, 0.3946} )
/*07*/AADD(aTabela, {1.134, 0.419, 1.182, 0.118, 1.882, 0.113, 1.806, 0.204, 5.204, 0.076, 1.924, 0.9594, 1.0423, 2.704, 0.3698} )
/*08*/AADD(aTabela, {1.061, 0.373, 1.099, 0.185, 1.815, 0.179, 1.751, 0.388, 5.306, 0.136, 1.864, 0.9650, 1.0363, 2.847, 0.3512} )
/*09*/AADD(aTabela, {1.000, 0.337, 1.032, 0.239, 1.761, 0.232, 1.707, 0.547, 5.393, 0.184, 1.816, 0.9693, 1.0317, 2.970, 0.3367} )
/*10*/AADD(aTabela, {0.949, 0.308, 0.975, 0.284, 1.716, 0.276, 1.669, 0.687, 5.469, 0.223, 1.777, 0.9727, 1.0281, 3.078, 0.3249} )
/*11*/AADD(aTabela, {0.905, 0.285, 0.927, 0.321, 1.679, 0.313, 1.637, 0.811, 5.535, 0.256, 1.744, 0.9754, 1.0252, 3.173, 0.3152} )
/*12*/AADD(aTabela, {0.866, 0.266, 0.886, 0.354, 1.646, 0.346, 1.610, 0.922, 5.594, 0.283, 1.717, 0.9776, 1.0229, 3.258, 0.3069} )
/*13*/AADD(aTabela, {0.832, 0.249, 0.850, 0.382, 1.618, 0.374, 1.585, 1.025, 5.647, 0.307, 1.693, 0.9794, 1.0210, 3.336, 0.2998} )
/*14*/AADD(aTabela, {0.802, 0.235, 0.817, 0.406, 1.594, 0.399, 1.563, 1.118, 5.696, 0.328, 1.672, 0.9810, 1.0194, 3.407, 0.2935} )
/*15*/AADD(aTabela, {0.775, 0.223, 0.789, 0.428, 1.572, 0.421, 1.544, 1.203, 5.741, 0.347, 1.653, 0.9823, 1.0180, 3.472, 0.2880} )
/*16*/AADD(aTabela, {0.750, 0.212, 0.763, 0.448, 1.552, 0.440, 1.526, 1.282, 5.782, 0.363, 1.637, 0.9835, 1.0168, 3.532, 0.2831} )
/*17*/AADD(aTabela, {0.728, 0.203, 0.739, 0.466, 1.534, 0.458, 1.511, 1.356, 5.820, 0.378, 1.622, 0.9845, 1.0157, 3.588, 0.2787} )
/*18*/AADD(aTabela, {0.707, 0.194, 0.718, 0.482, 1.518, 0.475, 1.496, 1.424, 5.856, 0.391, 1.608, 0.9854, 1.0148, 3.640, 0.2747} )
/*19*/AADD(aTabela, {0.688, 0.187, 0.698, 0.497, 1.503, 0.490, 1.483, 1.487, 5.891, 0.403, 1.597, 0.9862, 1.0140, 3.689, 0.2711} )
/*20*/AADD(aTabela, {0.671, 0.180, 0.680, 0.510, 1.490, 0.504, 1.470, 1.549, 5.921, 0.415, 1.585, 0.9869, 1.0133, 3.735, 0.2677} )
/*21*/AADD(aTabela, {0.655, 0.173, 0.663, 0.523, 1.477, 0.516, 1.459, 1.605, 5.951, 0.425, 1.575, 0.9876, 1.0126, 3.778, 0.2647} )
/*22*/AADD(aTabela, {0.640, 0.167, 0.647, 0.534, 1.466, 0.528, 1.448, 1.659, 5.979, 0.434, 1.566, 0.9882, 1.0119, 3.819, 0.2618} )
/*23*/AADD(aTabela, {0.626, 0.162, 0.633, 0.545, 1.455, 0.539, 1.438, 1.710, 6.006, 0.443, 1.557, 0.9887, 1.0114, 3.858, 0.2592} )
/*24*/AADD(aTabela, {0.612, 0.157, 0.619, 0.555, 1.445, 0.549, 1.429, 1.759, 6.031, 0.451, 1.548, 0.9892, 1.0109, 3.895, 0.2567} )
/*25*/AADD(aTabela, {0.600, 0.153, 0.606, 0.565, 1.435, 0.559, 1.420, 1.806, 6.056, 0.459, 1.541, 0.9896, 1.0105, 3.931, 0.2544} )
/* n = n�mero de replicatas */

nValor := aTabela[nLin, nCol]
	
Return nValor
