#Include 'Protheus.ch'
#Include 'FWEditPanel.ch'

/*-------------------------------------------------------------------
{Protheus.doc} QLT100
Fun��o MVC sem dicion�rios
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Function QLT100()
	
	FWExecView("QDO","QLT100",3,,{|| .T.})
	 
Return

/*-------------------------------------------------------------------
{Protheus.doc} ModelDef
Funcao do ModelDef MVC
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Static Function ModelDef()
Local oModel
Local oStr 	:= getModelStruct()	// CAMPOS DO FIELD
Local oStr1 := getMStr1() 		// CAMPOS DA GRID  

	oModel := MPFormModel():New('MVCTEMPF',,,{|oModel| Commit(oModel) })
	
	oModel:SetDescription("Anonimiza��o")
	oModel:AddFields("MASTER",,oStr,,,)
    oModel:addGrid('DETAILSTR1','MASTER',oStr1)
  
	oModel:getModel("MASTER"):SetDescription("FORM FIELD")
    oModel:getModel("DETAILSTR1"):SetDescription("GRID")
	oModel:SetPrimaryKey({})

Return oModel

/*-------------------------------------------------------------------
{Protheus.doc} ViewDef
Funcao do ViewDef MVC
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Static Function ViewDef()
Local oView
Local oModel := ModelDef() 
Local oStr	:= getViewStruct()    // CAMPOS DO FORM FIELD DA VIEW 

	oView := FWFormView():New()
	
	oView:SetModel(oModel)
	oView:AddField('FORM1' , oStr,'MASTER' ) 

	oView:CreateHorizontalBox( 'BOXSUPERIOR', 100)
	oView:SetOwnerView('FORM1','BOXSUPERIOR')

	oView:EnableTitleView('FORM1' , "Codigo do usu�rio a anonimizar" ) 


Return oView

/*-------------------------------------------------------------------
{Protheus.doc} getModelStruct
Montagem dos campos no model para o oSTR
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Static function getModelStruct()
Local oStruct := FWFormModelStruct():New()
Local nTam    := TamSx3( "QAA_MAT" )[1] 
	
	oStruct:AddField('C�digo da Matr�cula','Codigo' , 'COD', 'C', nTam, 0, , , {}, .T., , .F., .F., .F.,  )
	//Adicionar um botao no MVC
	oStruct:AddField('Anonimizar','Anonimizar' , 'LOAD', 'BT', 1, 0, { |oMdl| Anoni100(oMdl), .T. }, , {}, .F., , .F., .F., .F., , )
	
return oStruct

/*-------------------------------------------------------------------
{Protheus.doc} getViewStruct
Montagem dos campos na View para o oSTR
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
static function getViewStruct()

Local oStruct := FWFormViewStruct():New()
 
	oStruct:AddField( 'COD','1','C�digo da Matr�cula','Codigo',, 'C' ,'@!',,,.T.,,,,,,.F.,, )
	oStruct:AddField( 'LOAD','2','Anonimizar','Anonimizar',, 'BT' ,,,,,,,,,,,, )

return oStruct

/*-------------------------------------------------------------------
{Protheus.doc} getModelStruct
Montagem dos campos na Model para o oStr1
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Static function getMStr1()

Local oStruct := FWFormModelStruct():New()

	oStruct:AddField('C�digo da Matr�cula','Codigo' , 'COD', 'C', 6, 0, , , {}, .T., , .F., .F., .F.,  )
	
return oStruct

/*-------------------------------------------------------------------
{Protheus.doc} Gerar
Botao Gerar dentro do field MVC 
@author  thiago.rover
@since   29/06/2020
-------------------------------------------------------------------*/
Static Function Anoni100(oField)

	Local cCod 	  	:= oField:GetValue("COD")
	Local lRet		:= .F.
	
	lret := QLTLGPD(cCod)

Return 
