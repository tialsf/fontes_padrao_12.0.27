#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RESTFUL.CH"

#DEFINE _CRLF CHR(13)+CHR(10)

/*---------------------------------------------------------/
    {Protheus.doc} 
    @type  Function QLTLGPD
    @author thiago.rover
    @since 30/06/2020
    @version version
/----------------------------------------------------------*/
Function QLTLGPD(cCod) 

    Local lRet      := .F.
    Local cMessage	:= ""
    Local cMensagem := "Os campos com permiss�o de anonimiza��o vinculados a matricula "+cCod+" foram anonimizados com sucesso nas tabelas: "+_CRLF+_CRLF
    Local cConteudo := ""
    Local cQuery1   := ""

    // Cadastro de Usu�rios
    DbSelectArea("QAA")
	QAA->(dbSetOrder(1))
	If QAA->(dbSeek(xFilial("QAA")+cCod))
		lRet := FwProtectedDataUtil():ToAnonymizeByRecno( "QAA", {QAA->(Recno())}, , @cMessage )
        If lRet
            cConteudo := "-> QAA - Cadastro de Usu�rios"  +_CRLF      
        EndIf
    EndIf

    // Nao Conformidades
    cQuery1 := " SELECT * FROM " + RetSqlName("QI2")+" QI2 "
	cQuery1 += " WHERE QI2_MAT = '"+ cValToChar(cCod) +"' OR QI2_MATRES = '"+ cValToChar(cCod) +"' "
    cQuery1 += " AND QI2_CONREA <> '' AND QI2_CODACA <> '' AND QI2_REVACA <> '' "
	cQuery1 += " AND D_E_L_E_T_ <> '*' "
    cQuery1 := ChangeQuery(cQuery1)

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"QI2TRB",.T.,.T.)

    DbSelectArea("QI2") 
    // Respons�vel pela N�o Conformidade
    QI2->(dbSetOrder(4))
    WHILE QI2TRB->(!EOF())
        If QI2->(dbSeek(QI2TRB->QI2_FILRES+cCod))
            lRet := FwProtectedDataUtil():ToAnonymizeByRecno( "QI2", {QI2->(Recno())}, , @cMessage ) 
            If lRet
                cConteudo += "-> QI2 - Respons�vel pela N�o Conformidades "+_CRLF     
            Endif
        EndIf
        QI2TRB->(DbSkip())
    ENDDO

    QI2TRB->(DBGoTop())

    // Digitador da N�o Conformidade
    QI2->(dbSetOrder(6))
    WHILE QI2TRB->(!EOF())
        If QI2->(dbSeek(QI2TRB->QI2_FILMAT+cCod))
            lRet := FwProtectedDataUtil():ToAnonymizeByRecno( "QI2", {QI2->(Recno())}, , @cMessage ) 
            If lRet
                cConteudo += "-> QI2 - Digitador da N�o Conformidades  "+_CRLF       
            Endif
        EndIf
        QI2TRB->(DbSkip())
    ENDDO

    QI2TRB->(DbCLOSEAREA())

    If !Empty(cConteudo)
        MsgInfo( cMensagem + cConteudo  )
    EndIf

Return lRet
