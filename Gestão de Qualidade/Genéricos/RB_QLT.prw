#INCLUDE "Protheus.ch"

//------------------------------------------------------------------------------------
/*/{Protheus} RBE_QLT
@autor     thiago.rover
@descricao atualiza��o de Dicion�rio para UpdDistr
@since     Janeiro/2020
@using     UpdDistr para Qualidade Protheus
/*/
//------------------------------------------------------------------------------------
Function RBE_QLT( cVersion, cMode, cRelStart, cRelFinish, cLocaliz )

Local nCount        := 0
Local aTabelas      := {}
    
Default cVersion    := ''
Default cMode       := '1'
Default cRelStart   := "007"
Default cRelFinish  := ''
Default cLocaliz    := ''

#IFDEF TOP
    TmsLogMsg( ,   'In�cio RBE_QLT: '+Time())

    aadd(aTabelas, {"QAA", "QAA_CC"})
    aadd(aTabelas, {"QAB", "QAB_CCD"})
    aadd(aTabelas, {"QAB", "QAB_CCP"})
    aadd(aTabelas, {"QAD", "QAD_CUSTO"})
    aadd(aTabelas, {"QD0", "QD0_DEPTO"})
    aadd(aTabelas, {"QD1", "QD1_DEPTO"})
    aadd(aTabelas, {"QD2", "QD2_DEPTO"})
    aadd(aTabelas, {"QD8", "QD8_DEPTO"})
    aadd(aTabelas, {"QDD", "QDD_DEPTOA"})
    aadd(aTabelas, {"QDG", "QDG_DEPTOA"})
    aadd(aTabelas, {"QDH", "QDH_DEPTOA"})
    aadd(aTabelas, {"QDH", "QDH_DEPTOE"})
    aadd(aTabelas, {"QDJ", "QDJ_DEPTO"})
    aadd(aTabelas, {"QDN", "QDN_DEPTO"})
    aadd(aTabelas, {"QDR", "QDR_DEPRES"})
    aadd(aTabelas, {"QDR", "QDR_DEPDE"})
    aadd(aTabelas, {"QDT", "QDT_DEPTO"})
    aadd(aTabelas, {"QDU", "QDU_DEPTO"})
    aadd(aTabelas, {"QDU", "QDU_DEPBX"})
    aadd(aTabelas, {"QF3", "QF3_DEPTO"})
    aadd(aTabelas, {"QI2", "QI2_ORIDEP"})
    aadd(aTabelas, {"QI2", "QI2_DESDEP"})
    aadd(aTabelas, {"QM1", "QM1_DISTR"})
    aadd(aTabelas, {"QM9", "QM9_DEPTO"})
    aadd(aTabelas, {"QN4", "QN4_DEPRSA"})
    aadd(aTabelas, {"QUH", "QUH_CCUSTO"})
    aadd(aTabelas, {"QUM", "QUM_CCUSTO"})
    aadd(aTabelas, {"QUO", "QUO_DEPTO"})
    aadd(aTabelas, {"QM2", "QM2_DEPTO"})
    aadd(aTabelas, {"QML", "QML_DEPTO"})
    aadd(aTabelas, {"QAE", "QAE_DEPTO"})
    aadd(aTabelas, {"QAF", "QAF_DEPTO"})
    aadd(aTabelas, {"QAG", "QAG_DEPTO"})
    aadd(aTabelas, {"QDG", "QDG_DEPTO"})
    aadd(aTabelas, {"QDH", "QDH_DEPTOD"})
    aadd(aTabelas, {"QI2", "QI2_DEPCON"})
    aadd(aTabelas, {"QI2", "QI2_CONTRO"})

    //Executar uma vz por Empresa
    If cMode == "1"
        // Release maior ou igual a '007'
        If cRelStart >= "007"
            For nCount := 1 To Len(aTabelas)
                If TCCanOpen(RetSqlName(aTabelas[nCount][1]))
                    ExecQry(aTabelas[nCount][1], aTabelas[nCount][2] )
                Endif
            Next
        Endif
    Endif 

    TmsLogMsg( ,   'Encerramento RBE_QLT: '+Time())
#ENDIF

Return NIL
//------------------------------------------------------------------------------------
/*/{Protheus.doc} ExecQry
@type   Function
@author thiago.rover
@since  Janeiro/2020
@using  UpdDistr para Qualidade Protheus
/*/
//------------------------------------------------------------------------------------
Function ExecQry(cTab, cCampos)

Local cConteudo   := ""

//Remove Grupo de campos SX3
DbSelectArea("SX3")
SX3->(dbSetOrder(2))

#IFDEF TOP
    IF SX3->(DbSeek(cCampos))
        RecLock("SX3", .F.)
        SX3->X3_GRPSXG := cConteudo
		SX3->(MsUnlock())
    Endif
#ENDIF

Return ()