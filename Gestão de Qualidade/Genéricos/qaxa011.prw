#INCLUDE "PROTHEUS.CH"
#INCLUDE "QAXA011.CH"

/*������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���          �          � Autor �Quality Celerina          � Data �   /  /   ���
����������������������������������������������������������������������������Ĵ��
���Descri�ao �                                                               ���
����������������������������������������������������������������������������Ĵ��
���Uso       �SIGA Quality Celerina                                          ���
����������������������������������������������������������������������������Ĵ��
���Observacao�                                                               ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
������������������������������������������������������������������������������*/

Function QAXA011()

MsgRun( OemToAnsi( STR0007 ), OemToAnsi( STR0008 ), {|| QAQDOVrPen()}) // "Verificando necessidade de transfer�ncia de pend�ncias" ### "Aguarde..."

/*
As funcoes abaixo correspondem ao modulo SIGAQDO - Controle de Documentos
*/
/*������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���Fun�ao    �QAQDOVrPen� Autor �Newton Rogerio Ghiraldelli� Data �   /  /   ���
����������������������������������������������������������������������������Ĵ��
���Descri�ao �                                                               ���
����������������������������������������������������������������������������Ĵ��
���Sintaxe   �QAQDOVrPen()                                                   ���
����������������������������������������������������������������������������Ĵ��
���Parametro �                                                               ���
����������������������������������������������������������������������������Ĵ��
���Uso       �SIGAQDO - Generico                                             ���
����������������������������������������������������������������������������Ĵ��
���Observacao�                                                               ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���  Data  � BOPS � Programador �Alteracao                                   ���
����������������������������������������������������������������������������Ĵ��
���        �      �             �                                            ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
������������������������������������������������������������������������������*/

Function QAQDOVrPen()
Local oTempTable	:= NIL
Local nC     		:= 0
Local nOrdem 		:= 0

Local cCUsr  		:= ""
Local aUsr   		:= {}    

Private cChave := ""
Private aQD0   := {}
Private inclui := .t.

If Len( cUserName ) < 150
	cUsername += Space( 150 - Len( cUserName ) )
EndIf

Aadd( aUsr, { "USR_FILIAL", "C", TamSX3("RA_FILIAL")[1], 00 } )
Aadd( aUsr, { "USR_MAT",    "C", TamSX3("RA_MAT")[1],    00 } )
Aadd( aUsr, { "USR_CC",     "C", TamSX3("RA_CC")[1],     00 } )

oTempTable := FWTemporaryTable():New( "USR" )
oTempTable:SetFields( aUsr )
oTempTable:AddIndex("indice1", {"USR_FILIAL","USR_MAT","USR_CC"} )
oTempTable:Create()

DbSelectArea("SRA")
DbSetOrder(1)
SRA->( DbSetFilter( {|| SRA->RA_SITFOLH == "D"} ) )
SRA->( DbGotop() )

While !(SRA->( Eof() ) )
      cChave := SRA->RA_FILIAL + SRA->RA_MAT + SRA->RA_CC
      If !( USR->( DbSeek( cChave ) ) )
         If RecLock( "USR", .t. )
            USR->USR_FILIAL  := SRA->RA_FILIAL
            USR->USR_MAT     := SRA->RA_MAT
            USR->USR_CC      := SRA->RA_CC
            USR->( MsUnLock() )
         Endif
      Endif   
      SRA->( DbSkip() )
Enddo

SRA->( DbSetFilter({||}) )

DbSelectArea("SRE")
DbSetOrder(1)
SRE->( DbSetFilter( {|| SRE->RE_FILIALD != cFilAnt .and. SRE->RE_MATD != cMatCod .and. SRE->RE_CCD != cMatDep } ) )
SRE->( DbGotop() )
While !( SRE->( Eof() ) )
      cChave := SRE->RE_FILIALD + SRE->RE_MATD
      If !( USR->( DbSeek( cChave ) ) )
         If RecLock( "USR", .t. )
            USR->USR_FILIAL  := SRE->RE_FILIALD
            USR->USR_MAT     := SRE->RE_MATD
            USR->USR_CC      := SRE->RE_CCD
            USR->( MsUnLock() )
         Endif
      Endif   
      SRE->( DbSkip() )
Enddo

SRE->( DbSetFilter({||}) )

DbSelectArea("QDH")
DbSetOrder(1)
DbSetFilter({|| Left( QDH->QDH_STATUS, 1 ) !="L" .and. QDH->QDH_OBSOL !="S" })

DbSelectArea("USR")
USR->( DbGotop() )
While !( USR->( Eof() ) )
      DbSelectArea("QDH")
      QDH->( DbGotop() )
      While !( QDH->( Eof() ) )
            cChave := QDH->QDH_FILIAL + QDH->QDH_DOCTO + QDH->QDH_RV
            If QDH->QDH_STATUS $ "D  ,I  "
               DbSelectArea("QD1")
               QD1->( DbSetOrder( 1 ) )
               If QD1->( DbSeek( cChave ) )
                  While !( QD1->( Eof() ) ) .And. QD1->QD1_FILIAL + QD1->QD1_DOCTO + QD1->QD1_RV == cChave
                        If QD1->QD1_FILMAT + QD1->QD1_MAT + QD1->QD1_DEPTO == USR->USR_FILIAL + USR->USR_MAT + USR->USR_CC
                           If Left( QD1->QD1_TPPEND, 1 ) == "D" .or. ( Left( QD1->QD1_TPPEND, 1 ) == "I" .and. QD1->QD1_PENDEN == "P" )
                               If QD1->QD1_TPPEND == "D"
                                 nOrdem := 1
                              ElseIf QD1->QD1_TPPEND == "I"
                                 nOrdem := 6
                              Endif
                              Aadd( aQD0,{ QD1->QD1_FILMAT, QD1->QD1_MAT, QD1->QD1_DEPTO, QD1->QD1_DOCTO, QD1->QD1_RV, Left( QD1->QD1_TPPEND, 1 ), nOrdem } )
                           Endif
                        Endif
                        QD1->( DbSkip() )
                  Enddo 
               Endif
               DbSelectArea("QDH")
            Endif      
            DbSelectArea("QDG")
            QDG->( DbSetOrder( 6 ) )
            If QDG->( DbSeek( cChave ) )
               While !( QDG->( Eof() ) )  .And. QDG->QDG_FILIAL + QDG->QDG_DOCTO + QDG->QDG_RV == cChave
                     If QDG->QDG_FILMAT + QDG->QDG_DEPTO + QDG->QDG_MAT == USR->USR_FILIAL + USR->USR_CC + USR->USR_MAT
                        Aadd( aQD0,{ QDG->QDG_FILMAT, QDG->QDG_MAT, QDG->QDG_DEPTO, QDG->QDG_DOCTO, QDG->QDG_RV, "U", 7 } )
                     Endif
                     QDG->( DbSkip() )
               Enddo         
               DbSelectArea("QDH")
            Endif
            DbSelectArea("QD0")
            QD0->( DbSetOrder( 1 ) )
            If QD0->( DbSeek( cChave ) )
               While !( QD0->( Eof() ) ) .and. QD0->QD0_FILIAL + QD0->QD0_DOCTO + QD0->QD0_RV == cChave
                     If QD0->QD0_FILMAT + QD0->QD0_MAT + QD0->QD0_DEPTO == USR->USR_FILIAL + USR->USR_MAT + USR->USR_CC
                        If QD0->QD0_AUT == "E"
                           nOrdem := 2
                        ElseIf QD0->QD0_AUT == "R"
                           nOrdem := 3
                        ElseIf QD0->QD0_AUT == "A"
                           nOrdem := 4
                        ElseIf QD0->QD0_AUT == "H"
                           nOrdem := 5
                        Endif
                        Aadd( aQD0,{ QD0->QD0_FILMAT, QD0->QD0_MAT, QD0->QD0_DEPTO, QD0->QD0_DOCTO, QD0->QD0_RV, QD0->QD0_AUT, nOrdem } )
                     Endif
                     QD0->( DbSkip() )
               Enddo         
            Endif
            DbSelectArea("QDH")
            If Len( aQD0 ) >= 4000
               Exit
            EndIf
            DbSelectArea("QDH")
            QDH->( DbSkip() )
      Enddo      
      DbSelectArea("USR")
      USR->( DbSkip() )
Enddo

aQD0 := aSort( aQD0,,,{ |x,y| x[1] + x[2] + x[4] + x[5] + str(x[7]) < y[1] + y[2] + y[4] + y[5] + str(y[7]) } ) 

If Len( aQD0 ) > 0
   If VerSenha(102)	 
      If MsgYesNo( OemToAnsi( STR0001 ) + chr(13) +;            //"Foram encontradas pend�ncias que necessitam ser transferidas para outros usu�rios."
                   OemToAnsi( STR0002 ) + chr(13) + chr(13) +;  //"Apesar de ter acesso a esta rotina se voc� iniciar este processo n�o ter� como interromp�-lo."
                   OemToAnsi( STR0003 ), OemToAnsi( STR0004 ) ) //"Deseja realizar a transfer�ncia das pend�ncias ?" ### "Aten��o"
         For nC := 1 to Len( aQD0 )
             cChave :=" "
             QA010MsUsr( nC )
         Next
         DbSelectArea("USR")
         USR->( DbSetOrder( 1 ) )
         USR->( DbGoTop() )          
         DbSelectArea("QD1")
         QD1->( DbSetOrder( 3 ) )
         QD1->( DbSetFilter( {|| (Left( QD1->QD1_TPPEND, 1 )=="L" .and. QD1->QD1_PENDEN=="P" ) } ) )
         QD1->( DbGotop() )
         While ( USR->(! Eof() ) )          
               If QD1->( DbSeek( USR->USR_FILIAL + USR->USR_MAT ) )
                  While USR->USR_FILIAL + USR->USR_MAT == QD1->QD1_FILMAT + QD1->QD1_MAT
                        Begin Transaction
                              If Reclock( "QD1", .f. )							
                                 QD1->QD1_FMATBX := cFilAnt
                                 QD1->QD1_MATBX  := cMatCod
                                 QD1->QD1_DEPBX  := cMatDep
                                 QD1->QD1_PENDEN := "B"
                                 QD1->QD1_DTBAIX := dDataBase
                                 QD1->QD1_HRBAIX := Substr( Time(), 1, 5 )
                                 QD1->QD1_LEUDOC := "S"
                                 QD1->( MsUnlock() )
                              Endif
                        End Transaction
                        QD1->( DbSkip() )
                  Enddo      
               Endif
               DbSelectArea("CTT")
               CTT->( DbSetOrder( 1 ) )
               CTT->( DbGotop() )
               While ( CTT->( ! Eof() ) )
                     If CTT->CTT_FILIAL + CTT->CTT_MAT == USR->USR_FILIAL + USR->USR_MAT
                        Begin Transaction
                              If Reclock( "CTT", .f. )							
                                 CTT->CTT_MAT := Space( 06 )
                                 CTT->( MsUnlock() )
                              Endif
                        End Transaction
                     Endif                       
                     CTT->( DbSkip() )
               Enddo 
               USR->( DbSkip() )
         Enddo                     

        QD1->( DbSetFilter({||}) )

         DbSelectArea("QDH")
         QDH->( DbSetOrder( 1 ) )
         QDH->( DbSetFilter( { || Left( QDH_STATUS, 1 ) !="L" } ) )
         QDH->( DbGoTop() )
         While QDH->(!Eof() )
               aQDG   := {}
               cChave := QDH->QDH_FILIAL + QDH->QDH_DOCTO + QDH->QDH_RV
               DbSelectArea("QDG")
               QDG->( DbSetOrder( 6 ) )
               QDG->( DbGotop() )  
               If QDG->( DbSeek( cChave ) )
                  While QDG->( ! Eof() ) .and. cChave == QDG->QDG_FILIAL + QDG->QDG_DOCTO + QDG->QDG_RV
                        If ( nItem := AScan( aQDG, { |x| x[1]==QDG->QDG_FILIAL .and. x[2]==QDG->QDG_DOCTO .and. x[3]==QDG->QDG_RV .and. x[4]==QDG->QDG_FILMAT .and. x[5]==QDG->QDG_DEPTO .and. x[6]==QDG->QDG_TIPO } )  == 0 )        
                           Aadd( aQDG,{ QDG->QDG_FILIAL, QDG->QDG_DOCTO, QDG->QDG_RV, QDG->QDG_FILMAT, QDG->QDG_DEPTO, QDG->QDG_TIPO } )
                        Endif
                        QDG->( DbSkip() )
                  Enddo
               Endif
               DbSelectArea("QDJ")
               QDJ->( DbSetorder( 1 ) )
               If QDJ->( DbSeek( cChave ) )
                  While QDJ->( ! Eof() ) .and. cChave == QDJ->QDJ_FILIAL + QDJ->QDJ_DOCTO + QDJ->QDJ_RV       
                        If ( nItem := AScan( aQDG, { |x| x[1]==QDJ->QDJ_FILIAL .and. x[2]==QDJ->QDJ_DOCTO .and. x[3]==QDJ->QDJ_RV .and. x[4]==QDJ->QDJ_FILMAT .and. x[5]==QDJ->QDJ_DEPTO .and. x[6]==QDJ->QDJ_TIPO } ) == 0 )         
                           Begin Transaction
                                 If ( Reclock( "QDJ", .f. ) )
                                    QDJ->( DbDelete() )   
                                    QDJ->( MsUnlock() )
                                 Endif
                           End Transaction      
                        Endif   
                        QDJ->( DbSkip() )
                  Enddo                
               Endif
               QDJ->( DbGotop() )
               If Len( aQDG ) > 0
                  For nC := 1 to Len( aQDG )
                      DbSelectArea("QDJ")
                      QDJ->( DbSetorder( 1 ) )
                      If !( QDJ->( DbSeek( aQDG[ nC, 1 ] + aQDG[ nC, 2 ] + aQDG[ nC, 3 ] + aQDG[ nC, 6 ] + aQDG[ nC, 4 ] + aQDG[ nC, 5 ] ) ) )
                         Begin Transaction
                               If Reclock( "QDJ", .t. )							
                                  QDJ->QDJ_FILIAL := aQDG[ nC, 1 ]
                                  QDJ->QDJ_DOCTO  := aQDG[ nC, 2 ]
                                  QDJ->QDJ_RV     := aQDG[ nC, 3 ]
                                  QDJ->QDJ_FILMAT := aQDG[ nC, 4 ]
                                  QDJ->QDJ_DEPTO  := aQDG[ nC, 5 ]
                                  QDJ->QDJ_TIPO   := aQDG[ nC, 6 ]
                                  QDJ->( MsUnlock() )
                               Endif
                         End Transaction
                      Endif
                  Next             
               Endif       
               DbSelectArea( "QDH" )
               QDH->( DbSkip() )
         Enddo
		 
		QDH->( DbSetFilter({||}) )
		
      Else
         oTempTable:Delete()
         Final( STR0005 ) //"Sistema abortado pelo usu�rio"
      Endif
   Else
      oTempTable:Delete()
      Final( STR0006 ) //"Usu�rio n�o autorizado transferir Pend�ncias"
   Endif
Else
   MsgAlert( OemToAnsi( STR0009 ), OemToAnsi( STR0004 ) )//"Nao foi encontrado registros para a opera��o de transfer�ncia de pend�ncias" ### "Aten��o"
Endif

oTempTable:Delete()

Return .T.
