#INCLUDE "QDOR044.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "Report.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �QDOR044   �Autor  �Leandro Sabino      � Data �  03/07/06	  ���
�������������������������������������������������������������������������͹��
���Desc.     � Relatorio Lista de Documentos                              ���
���          � (Versao Relatorio Personalizavel)                          ���
�������������������������������������������������������������������������͹��
���Uso       � Generico                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                            
Function QDOR044()
Local oReport
 
If TRepInUse()
	Pergunte("QDR043",.F.) 
    oReport := ReportDef()
    oReport:PrintDialog()
Else
	 QDOR044R3()	// Executa vers�o anterior do fonte
EndIf

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ReportDef()   � Autor � Leandro Sabino   � Data � 03/07/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Montar a secao				                              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ReportDef()				                                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � QDOR044                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportDef()
Local cTitulo := OemToAnsi(STR0001) // "LISTA DE DOCUMENTOS"
Local cDesc1  := OemToAnsi(STR0002) // "Este programa ir� imprimir uma rela��o dos documentos, seus "
Local cDesc2  := OemToAnsi(STR0003) // "elaboradores, revisores, aprovadores e homologadores, "
Local cDesc3  := OemToAnsi(STR0004) // "de acordo com os par�metros definidos pelo usu�rio."
Local oSection1 

DEFINE REPORT oReport NAME "QDOR044" TITLE cTitulo PARAMETER "QDR043" ACTION {|oReport| PrintReport(oReport)} DESCRIPTION (cDesc1+cDesc2+cDesc3)
oReport:SetLandscape(.T.)

DEFINE SECTION oSection1 OF oReport TABLES "QDH" TITLE OemToAnsi(STR0024)
DEFINE CELL NAME "QDH_DOCTO"    OF oSection1 ALIAS "QDH" 
DEFINE CELL NAME "QDH_RV"       OF oSection1 ALIAS "QDH" 
DEFINE CELL NAME "QDH_TITULO"   OF oSection1 ALIAS "QDH" Auto Size
DEFINE CELL NAME "cElaborador"  OF oSection1 ALIAS "  " TITLE OemToAnsi(STR0019)  SIZE 15 //"Elaboradores"
DEFINE CELL NAME "cRevisor"     OF oSection1 ALIAS "  " TITLE OemToAnsi(STR0020)  SIZE 15 //"Revisores"
DEFINE CELL NAME "cAprovador"   OF oSection1 ALIAS "  " TITLE OemToAnsi(STR0021)  SIZE 14 //"Aprovadores"
DEFINE CELL NAME "cHomologador" OF oSection1 ALIAS "  " TITLE OemToAnsi(STR0022)  SIZE 14 //"Homologador"
DEFINE CELL NAME "cDescricao"   OF oSection1 ALIAS "  " TITLE OemToAnsi(STR0023)  SIZE 12 //"Status"
DEFINE CELL NAME "QDH_DTVIG"    OF oSection1 ALIAS "QDH" 

Return oReport

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PrintReport   � Autor � Leandro Sabino   � Data � 03/07/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Imprimir os campos do relatorio                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PrintReport(ExpO1)  	     	                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto oPrint                                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � QDOR020                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/                  
Static Function PrintReport(oReport) 
Local oSection1 	:= oReport:Section(1)
Local nTam1         := 0
Local nTam2         := 0
Local nTamMax       := 0
Local nI            := 0
Local nAcho         := 0
Local aDigitadores  := {}
Local aElaboradores := {}
Local aRevisores    := {}
Local aAprovadores  := {}
Local aHomologadores:= {}
Local cIndex1       := ""
Local cFiltro       := ""
Local cKey          := ""

MakeAdvplExpr(oReport:uParam)

mv_par20 := IIf( mv_par20 < mv_par19, mv_par19, mv_par20 )
	
DbSelectArea("QDH")
DbSetOrder(1)

DbSelectArea("QDG")
DbSetOrder(1)

DbSelectArea("QD0")
DbSetOrder(1)

cFiltro:= 'QDH_STATUS $ "' +mv_par01+'" .AND.'
cFiltro+= 'QDH_CODTP >= "' +mv_par03+'" .AND. QDH_CODTP <= "' +mv_par04+'" .AND.'
cFiltro+= 'QDH_CODASS >= "'+mv_par05+'" .AND. QDH_CODASS <= "'+mv_par06+'" .AND.'
cFiltro+= 'QDH_FILDEP >= "'+mv_par24+'" .AND. '
cFiltro+= 'QDH_DEPTOD >= "'+mv_par07+'" .AND. '
cFiltro+= 'QDH_FILDEP <= "'+mv_par25+'" .AND. '
cFiltro+= 'QDH_DEPTOD <= "'+mv_par08+'" .AND. '
cFiltro+= 'QDH_FILMAT >= "'+mv_par26+'" .AND. '
cFiltro+= 'QDH_DEPTOE >= "'+mv_par09+'" .AND. '
cFiltro+= 'QDH_FILMAT <= "'+mv_par27+'" .AND. '
cFiltro+= 'QDH_DEPTOE <= "'+mv_par10+'" .AND. '
cFiltro+= 'QDH_DOCTO >= "' +mv_par15+'" .AND. QDH_DOCTO <= "' +mv_par16+'" .AND.'
cFiltro+= 'QDH_RV >= "'    +mv_par17+'" .AND. QDH_RV <= "'    +mv_par18+'" '

If mv_par02	== 1   // Vigente
	cFiltro+= ' .And. QDH_CANCEL<>"S".And. QDH_OBSOL<>"S".And.QDH_STATUS = "L  "'
Elseif mv_par02 == 2   // Obsoleto
	cFiltro+= ' .AND. QDH_CANCEL <> "S"'+' .AND. QDH_OBSOL == "S"'	
Elseif mv_par02 == 3   // Cancelado
	cFiltro+= ' .AND. QDH_CANCEL == "S"'
ElseIf mv_par02 == 4   // Todas Revisoes
	cFiltro+= ' .AND. QDH_CANCEL <> "S"'
EndIf

If mv_par23	== 2
	cFiltro+= ' .AND. QDH_DTOIE == "I"'
Elseif mv_par23 == 3
	cFiltro+= ' .AND. QDH_DTOIE == "E"'
EndIf

oSection1:SetFilter(cFiltro)

QDH->(DbGotop())

While QDH->(!Eof()) .And. QDH->QDH_FILIAL == xFilial("QDH")	  

	If !QDOR044Vld()
		QDH->(DbSkip())
		Loop
	EndIf

	If !Empty(mv_par19) .And. Alltrim(QDH_STATUS) == "L"
		If !( DTOS(QDH->QDH_DTVIG) >= DTOS(mv_par19) .And. DTOS(QDH->QDH_DTVIG) <= DTOS(mv_par20) )
			QDH->(dbSkip())
			Loop
		Endif
	Endif

	aDigitadores	:={}	
	aElaboradores	:={}
	aRevisores		:={}
	aAprovadores	:={}
	aHomologadores	:={}		

	If QD0->(DbSeek(xFilial("QD0")+QDH->QDH_DOCTO+QDH->QDH_RV))
		While QD0->(!Eof()) .And. QD0->QD0_FILIAL+QD0->QD0_DOCTO+QD0->QD0_RV == xFilial("QD0")+QDH->QDH_DOCTO+QDH->QDH_RV
			If QD0->QD0_AUT == "E"
				nAcho:= ASCAN(aElaboradores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
				If nAcho == 0
					AADD(aElaboradores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
				EndIf
			Endif
			If QD0->QD0_AUT == "R"
				nAcho:= ASCAN(aRevisores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
				If nAcho == 0
					AADD(aRevisores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
				EndIf
			Endif
			If QD0->QD0_AUT == "A"
				nAcho:= ASCAN(aAprovadores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
				If nAcho == 0
					AADD(aAprovadores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
				EndIf
			Endif
			If QD0->QD0_AUT == "H"
				nAcho:= ASCAN(aHomologadores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
				If nAcho == 0
					AADD(aHomologadores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
				EndIf
			EndIf
			QD0->(DbSkip())
		EndDo
	EndIf
    
    oSection1:Init()
    
	//�������������������������������������������������Ŀ
	//� Imprime STATUS de acordo com tabela Q7 - SX5    �
	//���������������������������������������������������
	If QDH->QDH_STATUS <> "L  "
		DbSelectArea("SX5")
		If SX5->(DbSeek(xFilial("SX5")+"Q7"+QDH->QDH_STATUS))
			oSection1:Cell("cDescricao"):SetValue(Substr(X5Descri(),1,12),1,15)
		EndIf		
	Else
		oSection1:Cell("cDescricao"):SetValue(OemToAnsi(STR0009),1,15)//"Publicado"
	EndIf
		
	//�������������������������������������������������Ŀ
	//� Tamanho maximo do La�o (For-Next)               �
	//���������������������������������������������������
	nTam1:=Max(Len(aElaboradores),Len(aRevisores))
	nTam2:=Max(Len(aAprovadores),Len(aHomologadores))
	nTamMax:=Max(nTam1,nTam2)

    For nI:= 1 to nTamMax
        
		If nI > 1
			oSection1:Cell("QDH_DOCTO"):Hide()
			oSection1:Cell("QDH_RV"):Hide()
			oSection1:Cell("QDH_TITULO"):Hide()
			oSection1:Cell("cDescricao"):Hide()
			oSection1:Cell("QDH_DTVIG"):Hide()
		Else
			oSection1:Cell("QDH_DOCTO"):Show()
			oSection1:Cell("QDH_RV"):Show()
			oSection1:Cell("QDH_TITULO"):Show()		
			oSection1:Cell("cDescricao"):Show()
			oSection1:Cell("QDH_DTVIG"):Show()
		Endif	

		If Len( aElaboradores ) >= nI
			oSection1:Cell("cElaborador"):SetValue(QA_NUSR(aElaboradores[nI,1],aElaboradores[nI,2],.T.,"A"),1,15)			
		Else
			oSection1:Cell("cElaborador"):SetValue("")
		EndIf		
			
		If Len( aRevisores ) >= nI
			oSection1:Cell("cRevisor"):SetValue(QA_NUSR(aRevisores[nI,1],aRevisores[nI,2],.T.,"A"),1,15)
		Else
		   oSection1:Cell("cRevisor"):SetValue("")
		EndIf
	
		If Len( aAprovadores ) >= nI
			oSection1:Cell("cAprovador"):SetValue(QA_NUSR(aAprovadores[nI,1],aAprovadores[nI,2],.T.,"A"),1,15)
        Else
 		    oSection1:Cell("cAprovador"):SetValue("")
		EndIf
	
		If Len(aHomologadores) >= nI
			oSection1:Cell("cHomologador"):SetValue(QA_NUSR(aHomologadores[nI,1],aHomologadores[nI,2],.T.,"A"),1,15)
        Else
            oSection1:Cell("cHomologador"):SetValue("")
		EndIf

		oSection1:PrintLine()
	Next nI
	
	QDH->(DbSkip())
EndDo

oSection1:Finish()

Return NIL

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    � QDOR044  � Autor � Eduardo de Souza      � Data � 19/02/02 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Relatorio Lista de Documentos                              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 � QDOR044()                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso		 � Siga Quality ( Controle de Documentos )                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���  Data  �  BOPS � Programador� Alteracao                               ���
�������������������������������������������������������������������������Ĵ��
���08/10/02� ----- � Eduardo S. � Inclusao das perguntas 24 a 29 permitin-���
���        �       �            � filtrar por filial dos usuarios.        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Function QDOR044R3()

Local	cTitulo := OemToAnsi(STR0001) // "LISTA DE DOCUMENTOS"
Local	cDesc1  := OemToAnsi(STR0002) // "Este programa ir� imprimir uma rela��o dos documentos, seus "
Local	cDesc2  := OemToAnsi(STR0003) // "elaboradores, revisores, aprovadores e homologadores, "
Local	cDesc3  := OemToAnsi(STR0004) // "de acordo com os par�metros definidos pelo usu�rio."
Local	cString := "QDH" 
Local	wnrel   := "QDOR044"
Local	Tamanho := "G"

Private cPerg   := "QDR043"
Private aReturn := {OemToAnsi(STR0005),1,OemToAnsi(STR0006),1,2,1,"",1} // "Zebrado" ### "Administra��o"
Private nLastKey:= 0
Private INCLUI  := .f.	// Colocada para utilizar as funcoes

//��������������������������������������������������������������������Ŀ
//� Verifica as perguntas selecionadas                                 �
//����������������������������������������������������������������������
//��������������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                               �
//� mv_par01	// Situacao do Documento   O - Todos                    �
//� mv_par02	// Impr. Documento (Vigente/Cancelado/Obsoleto/Ambos)   �
//� mv_par03	// De  Tipo de Documento                                �
//� mv_par04	// Ate Tipo de Documento                                �
//� mv_par05	// De  Assunto                                          �
//� mv_par06	// Ate Assunto                                          �
//� mv_par07	// De  Depto Distr.                                     �
//� mv_par08	// Ate Depto Distr.                                     �
//� mv_par09	// De  Depto Usr Digit.                                 �
//� mv_par10	// Ate Depto Usr Digit.                                 �
//� mv_par11	// De  Depto Destino                                    �
//� mv_par12	// Ate Depto Destino                                    �
//� mv_par13	// De  Usuario                                          �
//� mv_par14	// Ate Usuario                                          �
//� mv_par15	// De  Documento                                        �
//� mv_par16	// Ate Documento                                        �
//� mv_par17	// De  Revisao                                          �
//� mv_par18	// Ate Revisao                                          �
//� mv_par19	// De  Periodo                                          �
//� mv_par20	// Ate Periodo                                          �
//� mv_par21	// De  Pasta                                            �
//� mv_par22	// Ate Pasta                                            �
//� mv_par23	// Documento (Ambos/Interno/Externo)                    �
//� mv_par24	// De Filial Departamento Distribuidor                  �
//� mv_par25	// Ate Filial Departamento Distribuidor                 �
//� mv_par26	// De Filial Departamento Digitador                     �
//� mv_par27	// Ate Filial Departamento Digitador                    �
//� mv_par28	// De Filial Usuario Destino                            �
//� mv_par29	// Ate Filial Usuario Destino                           �
//�����������������������������������������������������������������������
Pergunte(cPerg,.f.)

wnrel :=SetPrint(cString,wnrel,cPerg,ctitulo,cDesc1,cDesc2,cDesc3,.F.,,.F.,Tamanho)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

RptStatus({|lEnd| QDOR044Imp(@lEnd,cTitulo,wnRel,tamanho)},cTitulo)

Return .t.

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �QDOR044Imp� Autor � Rodrigo de A. Sartorio� Data � 13/07/98 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Envia para funcao que faz a impressao do relatorio.        ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � QDOR044Imp(ExpL1,ExpC1,ExpC2,ExpC3)                        ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpL1 - Cancela Relatorio                                  ���
���          � ExpC1 - Titulo do Relatorio                                ���
���          � ExpC2 - Nome do Relatorio                                  ���
���          � ExpC3 - Tamanho do Relatorio                               ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � QDOR040                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function QDOR044Imp(lEnd,cTitulo,wnRel,Tamanho)

Local	cCabec1       := ""
Local	cCabec2       := ""
Local	cbtxt         := Space(10)
Local	nTipo         := GetMV("MV_COMP")
Local	cbcont        := 0
Local	nTam1         := 0
Local	nTam2         := 0
Local	nTamMax       := 0
Local nI            := 0
Local	nAcho         := 0
Local aDigitadores  := {}
Local aElaboradores := {}
Local aRevisores    := {}
Local	aAprovadores  := {}
Local	aHomologadores:= {}
Local cIndex1       := ""
Local cFiltro       := ""
Local	cKey          := ""

Private Limite	:= 220

mv_par20 := IIf( mv_par20 < mv_par19, mv_par19, mv_par20 )

cCabec1 := OemToAnsi(STR0010)+OemToAnsi(STR0011)+OemToAnsi(STR0012)
           // "DOCUMENTO         REV  TITULO                                                                                                "
           // "ELABORADORES     REVISORES        APROVADORES      "
           // "HOMOLOGADORES    STATUS        VIGENCIA"
cCabec2:= " "
Li     := 60
m_pag  := 1

QD0->(DbSetOrder(1))
QDG->(DbSetOrder(1))
QDH->(DbSetOrder(1))

//�������������������������������������������������Ŀ
//� Cria Indice Condicional nos arquivos utilizados �
//���������������������������������������������������
cIndex1 := CriaTrab( Nil, .F. )
cKey:= QDH->(IndexKey())

cFiltro:= 'QDH_STATUS $ "' +mv_par01+'" .AND.'
cFiltro+= 'QDH_CODTP >= "' +mv_par03+'" .AND. QDH_CODTP <= "' +mv_par04+'" .AND.'
cFiltro+= 'QDH_CODASS >= "'+mv_par05+'" .AND. QDH_CODASS <= "'+mv_par06+'" .AND.'
cFiltro+= 'QDH_FILDEP >= "'+mv_par24+'" .AND. '
cFiltro+= 'QDH_DEPTOD >= "'+mv_par07+'" .AND. '
cFiltro+= 'QDH_FILDEP <= "'+mv_par25+'" .AND. '
cFiltro+= 'QDH_DEPTOD <= "'+mv_par08+'" .AND. '
cFiltro+= 'QDH_FILMAT >= "'+mv_par26+'" .AND. '
cFiltro+= 'QDH_DEPTOE >= "'+mv_par09+'" .AND. '
cFiltro+= 'QDH_FILMAT <= "'+mv_par27+'" .AND. '
cFiltro+= 'QDH_DEPTOE <= "'+mv_par10+'" .AND. '
cFiltro+= 'QDH_DOCTO >= "' +mv_par15+'" .AND. QDH_DOCTO <= "' +mv_par16+'" .AND.'
cFiltro+= 'QDH_RV >= "'    +mv_par17+'" .AND. QDH_RV <= "'    +mv_par18+'"'

If mv_par02	== 1   // Vigente
	cFiltro+= ' .And. QDH_CANCEL<>"S".And. QDH_OBSOL<>"S".And.QDH_STATUS = "L  "'
Elseif mv_par02 == 2   // Obsoleto
	cFiltro+= ' .AND. QDH_CANCEL <> "S"'+' .AND. QDH_OBSOL == "S"'	
Elseif mv_par02 == 3   // Cancelado
	cFiltro+= ' .AND. QDH_CANCEL == "S"'
ElseIf mv_par02 == 4   // Todas Revisoes
	cFiltro+= ' .AND. QDH_CANCEL <> "S"'
EndIf

If mv_par23	== 2
	cFiltro+= ' .AND. QDH_DTOIE == "I"'
Elseif mv_par23 == 3
	cFiltro+= ' .AND. QDH_DTOIE == "E"'
EndIf

If ! Empty(aReturn[7])	// Filtro de Usuario
	cFiltro += " .And. (" + aReturn[7] + ")"
Endif

IndRegua("QDH",cIndex1,cKey,,cFiltro,OemToAnsi(STR0007))	//"Selecionando Registros.."

If QDH->(DbSeek(xFilial("QDH")))
	SetRegua(LastRec()) // Total de Elementos da Regua	
	While QDH->(!Eof()) .And. QDH->QDH_FILIAL == xFilial("QDH")	
		IncRegua()

		If !QDOR044Vld()
			QDH->(DbSkip())
			Loop
		EndIf

		If !Empty(mv_par19) .And. Alltrim(QDH_STATUS) == "L"
			If !( DTOS(QDH->QDH_DTVIG) >= DTOS(mv_par19) .And. DTOS(QDH->QDH_DTVIG) <= DTOS(mv_par20) )
				QDH->(dbSkip())
				Loop
			Endif
		Endif

		aDigitadores	:={}	
		aElaboradores	:={}
		aRevisores		:={}
		aAprovadores	:={}
		aHomologadores	:={}		
		If QD0->(DbSeek(xFilial("QD0")+QDH->QDH_DOCTO+QDH->QDH_RV))
			While QD0->(!Eof()) .And. QD0->QD0_FILIAL+QD0->QD0_DOCTO+QD0->QD0_RV == xFilial("QD0")+QDH->QDH_DOCTO+QDH->QDH_RV
				If QD0->QD0_AUT == "E"
					nAcho:= ASCAN(aElaboradores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
					If nAcho == 0
						AADD(aElaboradores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
					EndIf
				ElseIf QD0->QD0_AUT == "R"
					nAcho:= ASCAN(aRevisores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
					If nAcho == 0
						AADD(aRevisores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
					EndIf
				ElseIf QD0->QD0_AUT == "A"
					nAcho:= ASCAN(aAprovadores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
					If nAcho == 0
						AADD(aAprovadores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
					EndIf
				ElseIf QD0->QD0_AUT == "H"
					nAcho:= ASCAN(aHomologadores,{|x| x[1] == QD0->QD0_FILMAT .And. x[2] == QD0->QD0_MAT})
					If nAcho == 0
						AADD(aHomologadores,{QD0->QD0_FILMAT,QD0->QD0_MAT})
					EndIf
				EndIf
				QD0->(DbSkip())
			EndDo
		EndIf
		
		If lEnd
			Li++
			@ PROW()+1,001 PSAY OemToAnsi(STR0008) // "CANCELADO PELO OPERADOR"
			Exit
		EndIf
			
		If Li > 58
			Cabec(cTitulo,cCabec1,cCabec2,wnrel,Tamanho,nTipo)
		EndIf
			
		@ Li,000 PSay Substr(Alltrim(QDH->QDH_DOCTO ),1,16)
		@ Li,018 PSay Substr(Alltrim(QDH->QDH_RV),1,3)
		@ Li,023 PSay Substr(Alltrim(QDH->QDH_TITULO),1,100)
					
		If Len( aElaboradores ) >= 1
			@ Li,125	PSay Substr(QA_NUSR(aElaboradores[1,1],aElaboradores[1,2],.T.,"A"),1,15)
		EndIf		
			
		If Len( aRevisores ) >= 1
			@ Li,142	PSay Substr(QA_NUSR(aRevisores[1,1],aRevisores[1,2],.T.,"A"),1,15)
		EndIf
	
		If Len( aAprovadores ) >= 1
			@ Li,159	PSay Substr(QA_NUSR(aAprovadores[1,1],aAprovadores[1,2],.T.,"A"),1,15)
		EndIf
	
		If Len(aHomologadores) >= 1
			@ Li,176	PSay Substr(QA_NUSR(aHomologadores[1,1],aHomologadores[1,2],.T.,"A"),1,15)
		EndIf
	
		//�������������������������������������������������Ŀ
		//� Imprime STATUS de acordo com tabela Q7 - SX5    �
		//���������������������������������������������������
		If QDH->QDH_STATUS <> "L  "
			DbSelectArea("SX5")
			If SX5->(DbSeek(xFilial("SX5")+"Q7"+QDH->QDH_STATUS))
				@ Li,193 PSay Substr(X5Descri(),1,12)
			EndIf		
		Else
			@ Li,193 PSay OemToAnsi(STR0009) //"Publicado"
		EndIf
			
		@ Li,207 PSay QDH->QDH_DTVIG
		Li++

		//�������������������������������������������������Ŀ
		//� Tamanho maximo do La�o (For-Next)               �
		//���������������������������������������������������
		nTam1:=Max(Len(aElaboradores),Len(aRevisores))
		nTam2:=Max(Len(aAprovadores),Len(aHomologadores))
		nTamMax:=Max(nTam1,nTam2)

		For nI:= 2 to nTamMax
			If lEnd
				Li++
				@ PROW()+1,001 PSAY OemToAnsi(STR0008) // "CANCELADO PELO OPERADOR"
				Exit
			EndIf
			
			If Li > 58
				Cabec(cTitulo,cCabec1,cCabec2,wnrel,Tamanho,nTipo)
			EndIf
			If nI <= Len(aElaboradores)
				@ Li,125 PSay Substr(QA_NUSR(aElaboradores[nI,1],aElaboradores[nI,2],.T.,"A"),1,15)
			EndIf		
				
			If nI <= Len(aRevisores)
				@ Li,142 PSay Substr(QA_NUSR(aRevisores[nI,1],aRevisores[nI,2],.T.,"A"),1,15)
			EndIf
			If nI <= Len(aAprovadores)
				@ Li,159 PSay Substr(QA_NUSR(aAprovadores[nI,1],aAprovadores[nI,2],.T.,"A"),1,15)
			EndIf
			If nI <= Len(aHomologadores)
				@ Li,176 PSay Substr(QA_NUSR(aHomologadores[nI,1],aHomologadores[nI,2],.T.,"A"),1,15)
			EndIf
			Li++
		Next nI
		QDH->(DbSkip())
	EndDo
EndIf

If Li != 60
	Roda(cbcont,cbtxt,tamanho)
EndIf

//��������������������������������������������������������������Ŀ
//� Devolve as ordens originais dos arquivos                     �
//����������������������������������������������������������������
RetIndex("QDH")
Set Filter to

//��������������������������������������������������������������Ŀ
//� Apaga indices de trabalho                                    �
//����������������������������������������������������������������
cIndex1 += OrdBagExt()
Delete File &(cIndex1)

Set Device To Screen

If aReturn[5] = 1
	Set Printer TO 
	DbCommitAll()
	Ourspool(wnrel)
Endif

MS_FLUSH()

Return (.T.)

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    �QDOR044Vld� Autor � Eduardo de Souza      � Data � 19/02/02 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Valida registro para impressao                             ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � QDOR044Vld()                                               ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � QDOR044                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function QDOR044Vld()

Local lRet    := .T.

//��������������������������������������������������������������Ŀ
//� Valida documento, Revisao e Status                           �
//����������������������������������������������������������������
If !(QDH->QDH_STATUS $ mv_par01 )
	lRet:= .F.
EndIf

If lRet
	//��������������������������������������������������������������Ŀ
	//� Valida Usuario Destino                                       �
	//����������������������������������������������������������������
	lRet:= .F.
	If QDG->(DbSeek(xFilial("QDG")+QDH->QDH_DOCTO+QDH->QDH_RV))
		While QDG->(!Eof()) .And. xFilial("QDG")+QDH->QDH_DOCTO+QDH->QDH_RV == QDG->QDG_FILIAL+QDG->QDG_DOCTO+QDG->QDG_RV
			If QDG->QDG_SIT <> "I"
				If QDG->QDG_FILMAT >= mv_par28 .And. QDG->QDG_FILMAT <= mv_par29 .And. ;
					QDG->QDG_MAT >= mv_par13 .And. QDG->QDG_MAT <= mv_par14 .And. ;
					QDG->QDG_DEPTO >= mv_par11 .And. QDG->QDG_DEPTO <= mv_par12
					If QDG->QDG_CODMAN >= mv_par21 .And. QDG->QDG_CODMAN <= mv_par22 .And. QDG->QDG_RECEB=="S"
						lRet := .T.
						Exit
					Endif
				EndIf
			EndIf
			QDG->(DbSkip())
		Enddo
	Endif
EndIf

Return lRet
