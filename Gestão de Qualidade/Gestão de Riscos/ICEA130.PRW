#INCLUDE "PROTHEUS.CH"
#INCLUDE "TCBROWSE.CH"
#INCLUDE "ICEA130.CH"
#DEFINE _PICTURE		16

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130    矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � CADASTRO DE Sub-Processos                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/

Static Function MenuDef()
Local aRotina   := { {OemToAnsi(STR0002) ,"AXPESQUI"		,0 ,1},;   //Pesquisar
                       {OemToAnsi(STR0003) ,"ICEA130VIS"	,0 ,2},;   //Visualizar
                       {OemToAnsi(STR0004) ,"ICEA130INC"	,0 ,3},;   //Incluir
                       {OemToAnsi(STR0005) ,"ICEA130ALT"	,0 ,4},;   //Alterar
                       {OemToAnsi(STR0006) ,"ICEA130EXC"	,0 ,5} }   //Excluir

Return aRotina

Function ICEA130(aRotAuto,nOpc)

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Declaracao de Variaveis                                             �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
Private cCadastro := OemtoAnsi(STR0001)    // Cadastro de Sub-Processo
Private cDelFunc  := ".T."                 // Validacao para a exclusao. Pode-se utilizar ExecBlock
Private cString   := "IC4"                 // Alias de Trabalho

Private aMemos    := {{"IC4_CODOBJ","IC4_OBJETI"},{"IC4_CODFUN","IC4_FUNBAS"},{"IC4_CODFOR","IC4_FORMUL"}} // Array resp. pela gravacao dos campos memo

Private aRotina := MenuDef()

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Definicao de variaveis para rotina de inclusao automatica    �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
Private l130Auto := ( aRotAuto <> NIL )
nOpc := iif(nOpc == Nil, 3, nOpc)

DbSelectArea(cString)
DbSetOrder(1)

If l130Auto
   MsRotAuto(nOpc,aRotAuto,cString)
Else                                                                                     
   mBrowse(6,1,22,75,cString)
EndIf   

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130VIS 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Visualiza嚻o                                   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130VIS(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130VIS(cAlias,nReg,nOpc)

Local nOpcao := 0
Local aButtons:= {} 
Local lIntQDO := SuperGetMV("MV_ICEXQDO") == "1"
Local lVisual := .T.

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Botao para Visualizacao do Documento anexo ao Ensaio		 �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If lIntQDO
	Aadd(aButtons,{"VERNOTA",{||If(!lVisual,Help(" ",1,"QPNVIEWDOC"),QDOVIEW(,M->IC4_DOCTO,QA_UltRvDc(M->IC4_DOCTO,dDataBase,.f.,.f.)))},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
Else
	Aadd(aButtons,{"VERNOTA",{||aFluxo:=ICEA130QDO()},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
EndIf 

nOpcao := AxVisual(cAlias,nReg,nOpc,nil,nil,nil,nil,aButtons,nil,nil)

DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130INC 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Inclusao                                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130INC(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130INC(cAlias,nReg,nOpc)

Local nOpcao := 0
Local aFluxo := {}
Local aButtons:= {} 
Local lIntQDO := SuperGetMV("MV_ICEXQDO") == "1"

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Botao para Visualizacao do Documento anexo ao Ensaio		 �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If lIntQDO
	Aadd(aButtons,{"VERNOTA",{||If(!Inclui,Help(" ",1,"QPNVIEWDOC"),QDOVIEW(,M->IC4_DOCTO,QA_UltRvDc(M->IC4_DOCTO,dDataBase,.f.,.f.)))},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
Else
//	Aadd(aButtons,{"VERNOTA",{||If(Empty(M->IC4_CODTP).OR.Empty(M->IC4_NOMDOC),Help(" ",1,"ICEA130FLW"),aFluxo:=ICEA130QDO())},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
	Aadd(aButtons,{"VERNOTA",{||aFluxo:=ICEA130QDO()},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
EndIf

If ( Type("l130Auto") == "U" ) .or. !l130Auto
    nOpcao := AxInclui(cAlias,nReg,nOpc,,,,,,,aButtons)      
//	nOpcao :=AxInclui( cAlias, nReg, nOpc)
Else
   nOpcao := AxIncluiAuto(cAlias,nil,nil,nil,nil,nil,nil,nil,nil,aButtons)
EndIf 
If lIntQDO .and. nOpcao == 1
	DbSelectArea("QDH")
	If DbSeek(xFilial("QDH")+IC4->IC4_DOCTO+IC4->IC4_RV)
		DbSelectArea(cAlias)
		RecLock(cAlias,.F.)
	    Replace IC4_CODTP		With QDH->QDH_CODTP 
    	Replace IC4_TITULO		With QDH->QDH_TITULO
	    Replace IC4_NOMDOC		With QDH->QDH_NOMDOC
    	MsUnLock()
	EndIf		
Else
	If Len(aFluxo) > 0  	
		DbSelectArea(cAlias)
		RecLock(cAlias,.F.)
	    Replace IC4_CODTP		With aFluxo[1]
    	Replace IC4_TITULO		With aFluxo[2]
	    Replace IC4_NOMDOC		With aFluxo[3]
    	MsUnLock()
	EndIf
EndIf	    
DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130ALT 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Alteracao                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130ALT(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130ALT(cAlias,nReg,nOpc)

Local nOpcao := 0
Local aFluxo := {}
Local lRet   := .T.
Local aButtons:= {}
Local lIntQDO := SuperGetMV("MV_ICEXQDO") == "1"

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Botao para Visualizacao do Documento anexo ao Ensaio		 �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If lIntQDO
	Aadd(aButtons,{"VERNOTA",{||If(!Altera,Help(" ",1,"QPNVIEWDOC"),QDOVIEW(,M->IC4_DOCTO,QA_UltRvDc(M->IC4_DOCTO,dDataBase,.f.,.f.)))},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
Else
//	Aadd(aButtons,{"VERNOTA",{||If(Empty(M->IC4_CODTP).OR.Empty(M->IC4_NOMDOC),Help(" ",1,"ICEA130FLW"),aFluxo:=ICEA130QDO())},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
	Aadd(aButtons,{"VERNOTA",{||aFluxo:=ICEA130QDO()},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
EndIf

// Verificar se este sub-processo j� n鉶 possui respostas
//lRet := ICExVldOpc(nOpc,IC4->IC4_CODQT,nil)

If lRet
   nOpcao := AxAltera(cAlias,nReg,nOpc,nil,nil,nil,nil,nil,nil,nil,aButtons)

	If lIntQDO .and. nOpcao == 1
		DbSelectArea("QDH")
		If DbSeek(xFilial("QDH")+IC4->IC4_DOCTO+IC4->IC4_RV)
			DbSelectArea(cAlias)
			RecLock(cAlias,.F.)
		    Replace IC4_CODTP		With QDH->QDH_CODTP 
	    	Replace IC4_TITULO		With QDH->QDH_TITULO
		    Replace IC4_NOMDOC		With QDH->QDH_NOMDOC
    		MsUnLock()
		EndIf		
	Else
		If Len(aFluxo) > 0  	
			DbSelectArea(cAlias)
			RecLock(cAlias,.F.)
		    Replace IC4_CODTP	  		With aFluxo[1]
    		Replace IC4_TITULO			With aFluxo[2]
		    Replace IC4_NOMDOC		    With aFluxo[3]
    		MsUnLock()
		EndIf
	EndIf	    
EndIf   

DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130EXC 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Exclusao                                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130EXC(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130EXC(cAlias,nReg,nOpc)

Local lRet   := .T.
Local aButtons:= {} 
Local lIntQDO := SuperGetMV("MV_ICEXQDO") == "1"
Local lExclui := .T.

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Botao para Visualizacao do Documento anexo ao Ensaio		 �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If lIntQDO
	Aadd(aButtons,{"VERNOTA",{||If(!lExclui,Help(" ",1,"QPNVIEWDOC"),QDOVIEW(,M->IC4_DOCTO,QA_UltRvDc(M->IC4_DOCTO,dDataBase,.f.,.f.)))},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
Else
//	Aadd(aButtons,{"VERNOTA",{||If(Empty(IC4->IC4_CODTP).OR.Empty(M->IC4_NOMDOC),Help(" ",1,"ICEA130FLW"),aFluxo:=ICEA130QDO())},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
	Aadd(aButtons,{"VERNOTA",{||aFluxo:=ICEA130QDO()},STR0012,STR0013}) //"Visualizar o conteudo do Documento..." ### "Fluxo"
EndIf

// Verificar se este question醨io j� n鉶 possui respostas
//lRet := ICExVldOpc(nOpc,IC4->IC4_CODQT,nil,nil,nil,aButtons)

If lRet
	// Vou Verificar se ha perguntas para este sub-processo
	cSeekFil := xFilial("IC9") + IC6->IC6_CODQT + IC4->IC4_PROCES
	DbSelectArea("IC9")
	DbSetOrder(6) // FILIAL - PROCESSO - SUB-PROCESSO
	If DbSeek( cSeekFil )
	   MsgStop(STR0010) //"Imposs韛el efetuar a exclus鉶, pois existem quest鮡s relacionadas ao Sub-Processo."
	Else
	   If MsgNoYes(STR0011) // "Confirma a exclus鉶 do sub-processo ?")    
			//Apaga sub-processo
			DbSelectArea("IC4")
			If RecLock("IC4",.F.)
				DbDelete()
				MsUnLock()
			EndIf
	   EndIf
	EndIf
EndIf	
	
DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130PRO 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Valida o sub-processo de associacao do grupo a um processo 潮�
北�          � pertencente a uma pesquisa ativa.                          潮� 
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130PRO()                                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � ICEA130 - SX3                                              潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130PRO()

Local aArea 	:= GetArea() 
Local lRet  	:= .T. // Variavel para controle da existencia de Respostas
Local cSeekPai  := ""  // Variavel utilizada para o seek na tabela Pai, para otimizar o sub-processo
Local cSeekFil  := ""  // Variavel utilizada para o seek na tabela Filha, para otimizar o sub-processo

DbSelectArea("IC6")
DbSetOrder(3) // FILIAL - PESQUISA - PROCESSO
cSeekPai := xFilial("IC6") + M->IC4_CODQT
If DbSeek( cSeekPai )
   // Tentando localizar alguma resposta para a pesquisa
   cSeekFil := xFilial("ICC") + IC6->IC6_CODPQ
   DbSelectArea("ICC")
   DbSetOrder(2) // FILIAL - PESQUISA
   If DbSeek( cSeekFil )
	   DbSelectArea("ICD")
	   DbSetOrder(1) // FILIAL - PESQUISA
	   // Se n鉶 foram encontradas respostas para o processo e a Pesquisa estiver com o Status Ativa alertar o usu醨io
       If DbSeek( xFilial("ICD") + ICC->ICC_RESPPQ )
	  	   MsgStop(STR0007) // "Imposs韛el vincular novos Grupos a este Question醨io, pois j� existem respostas cadastradas."
	  	   lRet := .F.
	   EndIf
   Else
      // Vou verificar se a Pesquisa encontra-se ativa para alertar o usuario
	  DbSelectArea("IC5")
	  DbSetOrder(1)
  	  If DbSeek( xFilial("IC5") + IC6->IC6_CODPQ )
  	      If IC5->IC5_STATUS == "1"  			// Se a pesquisa estiver Ativa
			 MsgStop(STR0008+chr(13)+chr(10)+; 	// "A Pesquisa a qual pertence este Grupo de Question醨io encontra-se Ativa, por閙 sem respostas."
			 		 STR0009) 				    // "Fa鏰 a desativa玢o da mesma antes de prosseguir com a opera玢o."
			 lRet := .F.
		  EndIf
	  EndIf
   EndIf
EndIf   

RestArea(aArea)
Return(lRet)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA130QT  矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Valida o Processo + Sub-Processo							  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130QT()                                                潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � ICEA130 - SX3                                              潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130QT()

Local aArea 	:= GetArea() 
Local lRet  	:= .T. // Variavel para controle da existencia de Respostas

If !Empty(M->IC4_CODQT) .AND. !Empty(M->IC4_PROCES)
	DbSelectArea("IC4")
	DbSetOrder(1)	  // FILIAL + PROCESSO + SUB-PROCESSO
	If DbSeek( xFilial("IC4")+M->IC4_CODQT+IC4_PROCES )
 	   Help("",1,"ICEA13001")
	   lRet := .F.
    EndIf
ElseIf !Empty(M->IC4_CODQT) .AND. Empty(M->IC4_PROCES)    
	DbSelectArea("IC6")
	DbSetOrder(3)	  // FILIAL + PROCESSO
	If !DbSeek( xFilial("IC6")+M->IC4_CODQT )
 	   Help("",1,"ICEA13002")
	   lRet := .F.
    EndIf
EndIf   
RestArea(aArea)
Return(lRet)
                
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEA130QDO � Autor � Lucas               � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Valida o sub-processo de associacao do grupo a um processo 潮�
北�          � pertencente a uma pesquisa ativa.                          潮� 
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA130QDO()                                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � ICEA130 - SX3                                              潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA130QDO()

Local lIntQDO := SuperGetMV("MV_ICEXQDO") == "1"
Local oDlgFlow
Local oComboFlow
Local oTitleFlow
Local oNameFlow
Local oFolder
Local cTitleFlow := CriaVar("IC4_TITULO")
Local cNameFlow := CriaVar("IC4_NOMDOC")
Local aComboFlow := {"Aplicativo WordPad MFC",;			//"Aplicativo WordPad MFC"
					 "Bloco de Notas",;	        		//"Bloco de Notas"
					 "Microsoft Office Power Point",;	//"Microsoft Office Power Point"
					 "Microsoft Office Word",;			//"Microsoft Office Word"
					 "Microsoft Visio",;				//"Microsoft Visio"
					 "Outros"}                          //"Outros"...
					 
Local cComboFlow := aComboFlow[1]
Local aPathFile  := QDOPATH()
Local cPathDocs  := aPathFile[1]		// Diretorio que contem os arquivos documentos
Local cPathMods  := aPathFile[2]		// Diretorio que contem os arquivos modelos
Local cPathTrms  := aPathFile[3]		// Diertorio que contem os arquivos temporarios		
Local aFluxo := {}            
Local nPosCBox := 0

//Local oOpc1 := LoadBitMap(GetResources(), "PARAMETROS")
///Local oOpc2 := LoadBitMap(GetResources(), "MSVISIO")

//oOpc1:cCaption	:= PADR(OemToAnsi("Microsoft Visio"),28)	

//aComboFlow[5] :=  oOpc1

Private aUsrMat := QA_USUARIO()
Private cMatFil := aUsrMat[2]
Private cMatCod := aUsrMat[3]
Private cMatDep := aUsrMat[4]
Private lSolicitacao:= .F.
Private cCadastro   := OemToAnsi( "Cadastro de Documentos" )

If ! lIntQDO	// Sem integracao com controle de documentos

	If ! Inclui
    	cComboFlow := aComboFlow[Val(Iif(Empty(M->IC4_CODTP),"01",M->IC4_CODTP))]
      	cTitleFlow := IC4->IC4_TITULO
      	cNameFlow  := IC4->IC4_NOMDOC
    EndIf   
      
	DEFINE MSDIALOG oDlgFlow FROM 100,000 TO 340,633 STYLE nOr(DS_MODALFRAME,WS_POPUP,WS_CAPTION) TITLE cCadastro+" - "+"Fluxos" PIXEL

		@ 015,000 FOLDER oFolder PROMPTS OemToAnsi("Fluxos") SIZE 317,100 OF oDlgFlow PIXEL // "Fluxos" 

		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		//� Botoes definidos apenas para o folder nao perder o Foco na troca de Folders     �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
		DEFINE SBUTTON FROM 5000,5000 TYPE 6 ACTION .T. ENABLE OF oFolder:aDialogs[1] // botao oculto Folder 1

		//oFolder:bChange := {|| IF(QDO050ChFo(oFolder:nOption,lDuplDoc),"",oFolder:nOption:=1) } 
	
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Campos, Tipo do Documento, Titulo e Nome do Arquivo... �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
        @ 005,002 SAY "Tipo do Documento"	OF oFolder:aDialogs[1] PIXEL SIZE 80,09 COLOR CLR_HBLUE
		@ 005,060 COMBOBOX oComboFlow	VAR cComboFlow ITEMS aComboFlow;
							SIZE 90, 50 OF oFolder:aDialogs[1] PIXEL ON CHANGE ICECboxOk() WHEN .T.
        
		@ 017,002 SAY "Titulo do Documento" OF oFolder:aDialogs[1] PIXEL SIZE 80,09 COLOR CLR_HBLUE
		@ 017,060 MSGET oTitleFlow		VAR cTitleFlow OF oFolder:aDialogs[1] PIXEL SIZE 105,09		VALID NaoVazio(cTitleFlow)
        
       	@ 029,002 SAY "Nome do Arquivo" OF oFolder:aDialogs[1] PIXEL SIZE 80,09 COLOR CLR_HBLUE
		@ 029,060 MSGET oNameFlow		VAR cNameFlow PICT "@!" OF oFolder:aDialogs[1] PIXEL SIZE 105,09	VALID NaoVazio(cNameFlow)

		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Abre Documentos (Fluxo)...                             �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		@ 100,020 BTNBMP oFlowButtom	 RESOURCE "PARAMETROS" SIZE 172,24 OF oFolder:aDialogs[1] PIXEL ;
						 ACTION ICExCallFlw(cPathTrms,cPathDocs,cNameFlow,cComboFlow)

		oFlowButtom:cCaption:= PADR(OemToAnsi("Fluxo"),20)	// "Fluxo"
		oFlowButtom:cToolTip:= OemToAnsi("Digite/Visualize o conteudo do Documento...")

		@ 118,010 SAY OemToAnsi("Clicando sobre o botao, abrira o documento") OF oFolder:aDialogs[1] PIXEL // "Selecione a op噭o clicando sobre os botoes"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Ativando a Tela                                      �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
	ACTIVATE MSDIALOG oDlgFlow ON INIT EnchoiceBar(oDlgFlow,{||nOpcA := 1,;
		If(ICEEnchoOk(),oDlgFlow:End(),nOpcA:=0)},{||oDlgFlow:End()})
    
    nPosCbox := Ascan(aComboFlow,cComboFlow)
    AADD(aFluxo,StrZero(nPosCbox,1))
    AADD(aFluxo,cTitleFlow)
    AADD(aFluxo,cNameFlow)
EndIf
Return(aFluxo)            