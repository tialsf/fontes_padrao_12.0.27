#INCLUDE "PROTHEUS.CH"
#INCLUDE "TCBROWSE.CH"
#INCLUDE "ICEA140.CH"
#DEFINE _PICTURE		16

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140    矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � CADASTRO DE GRUPOS                                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/

Static Function MenuDef()

Local aRotina   := { {OemToAnsi(STR0002) ,"AXPESQUI"		,0 ,1},;   //Pesquisar
                      {OemToAnsi(STR0003) ,"ICEA140VIS"	,0 ,2},;   //Visualizar
                      {OemToAnsi(STR0004) ,"ICEA140INC"	,0 ,3},;   //Incluir
                      {OemToAnsi(STR0005) ,"ICEA140ALT"	,0 ,4},;   //Alterar
                      {OemToAnsi(STR0006) ,"ICEA140EXC"	,0 ,5} }   //Excluir

Return aRotina

Function ICEA140(aRotAuto,nOpc)

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//� Declaracao de Variaveis                                             �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�

Private cCadastro := OemtoAnsi(STR0001)    // Cadastro de Grupo de Pesquisas
Private cDelFunc  := ".T."                 // Validacao para a exclusao. Pode-se utilizar ExecBlock
Private cString   := "IC7"                 // Alias de Trabalho

Private aRotina := MenuDef()

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Definicao de variaveis para rotina de inclusao automatica    �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
Private l140Auto := ( aRotAuto <> NIL )
nOpc := iif(nOpc == Nil, 3, nOpc)

DbSelectArea(cString)
DbSetOrder(1)

If l140Auto
   MsRotAuto(nOpc,aRotAuto,cString)
Else
   mBrowse(6,1,22,75,cString)
EndIf   

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140VIS 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Visualiza嚻o                                   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA140VIS(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA140VIS(cAlias,nReg,nOpc)

Local nOpcao := 0

nOpcao := AxVisual(cAlias,nReg,nOpc,nil,nil,nil,nil,nil,nil,nil)

DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140INC 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Inclusao                                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA140INC(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA140INC(cAlias,nReg,nOpc)

Local nOpcao := 0

If ( Type("l140Auto") == "U" ) .or. !l140Auto
   nOpcao := AxInclui(cAlias,nReg,nOpc,nil,nil,nil,nil,nil,nil,nil)
Else
   nOpcao := AxIncluiAuto(cAlias,nil,nil,nil,nil)
EndIf   

DbSelectarea(cAlias)
Return(.T.)


/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140ALT 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Alteracao                                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA140ALT(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA140ALT(cAlias,nReg,nOpc)

Local nOpcao := 0
Local lRet   := .T.

// Verificar se este question醨io j� n鉶 possui respostas
lRet := ICExVldOpc(nOpc,IC7->IC7_CODQT,nil)

If lRet
   nOpcao := AxAltera(cAlias,nReg,nOpc,nil,nil,nil,nil,nil,nil,nil,nil)
EndIf   

DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140EXC 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa de Exclusao                                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA140EXC(ExpC1,ExpN1,ExpN2)                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北�          � ExpN1 = Numero do registro                                 潮�
北�          � ExpN2 = Op玢o selecionada                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA140EXC(cAlias,nReg,nOpc)

Local lRet   := .T.

// Verificar se este question醨io j� n鉶 possui respostas
lRet := ICExVldOpc(nOpc,IC7->IC7_CODQT,nil)

If lRet
	// Vou Verificar se este subgrupo est� amarrado a alguma quest鉶
	cSeekFil := xFilial("SC9") + IC7->IC7_CODGRP
	DbSelectArea("IC9")
	DbSetOrder(6) // FILIAL - GRUPO
	If DbSeek( cSeekFil )
	   MsgStop(STR0010) //"Imposs韛el efetuar a exclus鉶, pois existem quest鮡s relacionadas ao Grupo."
	Else
	   If MsgNoYes(STR0011) // "Confirma a exclus鉶 do Grupo e poss韛eis Subgrupos a ele relacionados ?")    
			// Vou Apagar os Subgrupos do Question醨io
			cSeekFil := xFilial("IC8") + IC7->IC7_CODGRP
			DbSelectArea("IC8")
			DbSetOrder(5) // FILIAL - GRUPO - SUBGRUPO
			If DbSeek( cSeekFil )
				While  !Eof() .And. IC8->IC8_FILIAL + IC8->IC8_CODGRP == cSeekFil
					If RecLock("IC8")
						DbDelete()
						MsUnlock()
					Endif
					DbSkip()
				End
			EndIf
            // Apago o Grupo sobre o qual estou posicionada
            DbSelectArea("IC7")			
			If RecLock("IC7")
 			   DbDelete()
			   MsUnlock()
 		    Endif
	   EndIf
	EndIf
EndIf	
	
DbSelectarea(cAlias)
Return(.T.)

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEA140QST 矨utor  � Lucas                � Data � 14/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Valida o processo de associacao do grupo a um questionario 潮�
北�          � pertencente a uma pesquisa ativa.                          潮� 
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEA140QST()                                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � ICEA140 - SX3                                              潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEA140QST()

Local aArea 	:= GetArea() 
Local lRet  	:= .T. // Variavel para controle da existencia de Respostas
Local cSeekPai  := ""  // Variavel utilizada para o seek na tabela Pai, para otimizar o processo
Local cSeekFil  := ""  // Variavel utilizada para o seek na tabela Filha, para otimizar o processo

DbSelectArea("IC6")
DbSetOrder(3) // FILIAL - PESQUISA - QUESTIONARIO
cSeekPai := xFilial("IC6") + M->IC7_CODQT
If DbSeek( cSeekPai )
   // Tentando localizar alguma resposta para a pesquisa
   cSeekFil := xFilial("ICC") + IC6->IC6_CODPQ
   DbSelectArea("ICC")
   DbSetOrder(1) // FILIAL - PESQUISA
   If DbSeek( cSeekFil )
      // Se n鉶 foram encontradas respostas para o questionario e a Pesquisa estiver com o Status Ativa alertar o usu醨io
  	  MsgStop(STR0007) // "Imposs韛el vincular novos Grupos a este Question醨io, pois j� existem respostas cadastradas."
	  lRet := .F.
   Else
      // Vou verificar se a Pesquisa encontra-se ativa para alertar o usuario
	  DbSelectArea("IC5")
	  DbSetOrder(1)
  	  If DbSeek( xFilial("IC5") + IC6->IC6_CODPQ )
  	      If IC5->IC5_STATUS == "1"  			// Se a pesquisa estiver Ativa
			 MsgStop(STR0008+chr(13)+chr(10)+; 	// "A Pesquisa a qual pertence este Grupo de Question醨io encontra-se Ativa, por閙 sem respostas."
			 		 STR0009) 				    // "Fa鏰 a desativa玢o da mesma antes de prosseguir com a opera玢o."
			 lRet := .F.
		  EndIf
	  EndIf
   EndIf
Endif   

If Len(aArea) > 0
   RestArea(aArea)
Endif

Return(lRet)