
#INCLUDE "ICEA400.CH"
#INCLUDE "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ICEA400  �Autor  � Lucas		     	 � Data �  25/11/04   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina para processar as respostas da pesquisa desejada e  ���
���          � iniciar a gravacao dos eventos para posterir avaliacao de  ���
���          � riscos...                                                  ���
�������������������������������������������������������������������������͹��
���Uso       �  SIGAICE/ICEA400                                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function MenuDef()

Local aRotAdic	:= {} 			// Retorno do P.E. IC400ROT

Local aRotina := {{ STR0001,	"AxPesqui", 0, 1},;	   				//"Pesquisar"
	 			  { STR0002,	"ICEA400PRO", 0, 2}}	   			//"Processar"

If ExistBlock("IC400ROT")
	If Valtype(aRotAdic := ExecBlock("IC400ROT",.F.,.F.)) == "A"
		AEval(aRotAdic,{|x|AAdd( aRotina,x)})
	EndIf
EndIf

Return aRotina


Function ICEA400(aRotAuto,nOpc)


Private aRotina := MenuDef()

Private cCadastro := STR0003 //"Processar Respostas"

//��������������������������������������������������������������Ŀ
//� Definicao de variaveis para rotina de inclusao automatica    �
//����������������������������������������������������������������
Private lIC400Auto := ( aRotAuto <> NIL )


//���������������������������������������������������Ŀ
//�Nao permite a abertura simultanea para o modelo MDI�
//�����������������������������������������������������
If !ExcProcess("ICEICC")
	Help("  ",1,"ICEEXCICC")
	Return(.F.)
Endif

//��������������������������������������������������������������Ŀ
//� Ponto de entrada - Adiciona rotinas ao aRotina               �
//����������������������������������������������������������������


If lIC400Auto
	MsRotAuto(nOpc,aRotAuto,"IC5")
Else
	//��������������������������������������������������������������Ŀ
	//� Endereca a funcao de BROWSE                                  �
	//����������������������������������������������������������������
	mBrowse(,,,,"IC5")
Endif

Return(.T.)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ICEA400PRO  �Autor  � Lucas              Data �  26/11/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Utiliza este recurso para montar a regua de progresso.      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP8                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ICEA400Pro(lFirst)

Local lExiste	:= .F.												// Flag para indicar se o usuario esta no cadastro de Operadores
Local cUltimo	:= GetNewPar("MV_ICERESP","  ")		// Data e hora da ultima atualizacao
Local lRet		:= .T.										 			// Retorno da funcao

If ( Type("lIC400Auto") == "U" )
	lIC400Auto := .F.
EndIf

//�������������������������������������������������������������Ŀ
//� Verificar se a pesquisa esta encerrada ou desativada.       �
//���������������������������������������������������������������
If !Empty(IC5->IC5_DATINA) .OR. IC5->IC5_STATUS == "2"
	Help("",1,"ICEA40001")
	lRet := .F.
EndIf	

//�������������������������������������������������������������Ŀ
//� Se for entrada automatica ou estiver no SCHEDULE.			�
//���������������������������������������������������������������
If lRet 
	If lIC400Auto  .OR. IsBlind()
		BatchProcess(,,, { || ICE400Proces() } )
	Else
		Processa( {|| lRet := ICE400Proces() }, STR0004) 		//"Processando respostas..."
	Endif

	//�������������������������������������������������������������������������������Ŀ
	//�Atualiza o parametro MV_ICERESP que indica quando foi feita a ultima atualizacao�
	//���������������������������������������������������������������������������������
	DbSelectarea("SX6")
	DbSetOrder(1)
	If MsSeek(xFilial("SX6") + "MV_ICERESP")
		PutMv("MV_ICERESP",DtoC(dDatabase)+"-"+Time())
	Endif
EndIf	
Return( lRet )

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ICE400Proces  �Autor  � Lucas              Data �  26/11/04   ���
�������������������������������������������������������������������������͹��
���Desc.     �Utiliza este recurso para montar a regua de progresso.      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � ICEA400                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ICE400Proces()
Local aArea := GetArea()
Local nUsrNoResp := 0
Local aUsersPesq := {}
Local nK := 0
Local cCodCRO := CriaVar("IC2_MAT")"
Local cEvento := CriaVar("ICH_EVENTO")
Local nRisco := 0
Local cRisco := ""
Local nPesos := 0
Local aPesos := {}
Local lAppend := .F.
Local cCodPQ := IC5->IC5_CODPQ
Local oFont
Local oDlgProc
Local oMemoPQ 
Local cMemoPQ := MSMM(IC5->IC5_CODOBJ)
Local oMemoTxt 
Local cMemoTxt := STR0010+STR0011+STR0012+STR0013
Local nOpcProc := 0
Local lICE12001 := ExistBlock("ICE12001")
Local lICE12002	:= ExistBlock("ICE12002")
Local lICE12003 := ExistBlock("ICE12003")
Local lICE12004 := ExistBlock("ICE12004")
Local lICE12005 := ExistBlock("ICE12005")
Local lICE12006 := ExistBlock("ICE12006")
Local lICE12007 := ExistBlock("ICE12007")
If ( Type("lIC400Auto") == "U" )
	lIC400Auto := .F.
EndIf

//��������������������������������������������������������������Ŀ
//� Pegar todos os destinatarios executaroes do Pesquisa           													
//����������������������������������������������������������������
aUsersPesq := ICEXDestPQ(cCodPQ)

nUsrNoResp := 0
For nK := 1 To Len(aUsersPesq)

	//��������������������������������������������������������������Ŀ
	//� Verifica se ha algum usuario/participante que nao respondeu a pesquisa...                                �
	//����������������������������������������������������������������
    DbSelectArea("ICC")
    DbSetOrder(2)
    If DbSeek(xFilial("ICC")+cCodPQ+aUsersPesq[nK][1])
       DbSelectArea("ICD")
	   DbSetOrder(1)
	   If ! DbSeek(xFilial("ICD")+ICC->ICC_RESPPQ)
	          nUsrNoResp ++
	   EndIf
	EndIf   
Next nK

If nUsrNoResp > 0
	//��������������������������������������������������������������Ŀ
	//� Ha destinatarios sem responder a pesquisa, imprimir relatorio de Abstinencia...		                    �
	//����������������������������������������������������������������
	If MsgYesNo(STR0005,{STR0006,STR0007})
		ICER130(cCodPQ)
	Endif	
Else	 
	//��������������������������������������������������������������Ŀ
	//� Todos responderam a Pesquisa, iniciar gravacao dos Eventos para Avalicao...                             �
	//����������������������������������������������������������������
    If ! lIC400Auto

		DEFINE FONT oFont NAME "Arial" SIZE 10,12 BOLD

		DEFINE MSDIALOG oDlgProc TITLE cCadastro FROM 80,80 TO 380,714 PIXEL

			//���������������������������������������������������������������Ŀ
			//� Exibe Dados da Pesquisa (Codigo,Descricao e objetivo).        �
			//�����������������������������������������������������������������
			@ 08,002 SAY STR0008 OF oDlgProc PIXEL SIZE 45,09 
			@ 08,039 MSGET IC5->IC5_CODPQ		 PICT 	 "@!" F3 "IC5" OF oDlgProc PIXEL SIZE 40,09 	WHEN .F.	
			@ 08,085 MSGET IC5->IC5_DESC  		 PICT     "@!" OF oDlgProc PIXEL SIZE 207,09 	WHEN .F.	
	 		
			@ 28,002 SAY STR0009 OF oDlgProc PIXEL SIZE 45,18 
			@ 28,039 GET oMemoPQ VAR cMemoPQ MEMO SIZE 252,40 PIXEL OF oDlgProc VALID oMemoPQ:Refresh() WHEN .F.

			@ 78,005 GET oMemoTxt VAR cMemoTxt MEMO SIZE 286,40 PIXEL OF oDlgProc VALID oMemoTxt:Refresh() WHEN .F.
                                
			DEFINE SBUTTON FROM 132, 240 TYPE 1 ACTION ( nOpcProc := 1, oDlgProc:End()) ENABLE OF oDlgProc
			DEFINE SBUTTON FROM 132, 278 TYPE 2 ACTION ( nOpcProc := 2, oDlgProc:End()) ENABLE OF oDlgProc

		ACTIVATE MSDIALOG oDlgProc
	Else
		nOpcProc := 1
	EndIf	

    If nOpcProc <> 1
    	RestArea(aArea)
    	Return
    EndIf
	     
	//��������������������������������������������������������������Ŀ
	//� Pegar codigo do CRO-Gestor de Riscos para gravar no cabecalho de eventos...							 �
	//����������������������������������������������������������������
	cCodCRO := ICEA120Cro(cCodPQ)       
	
	//��������������������������������������������������������������Ŀ
	//� Posicionar na resposta da Pesquisa, Questionario e Processos...                                              �
	//����������������������������������������������������������������
    ICC->(DbSetOrder(2))
    ICC->(DbSeek(xFilial("ICC")+IC5->IC5_CODPQ))

    ICD->(DbSetOrder(1))
    ICD->(DbSeek(xFilial("ICD")+ICC->ICC_RESPPQ))

	//���������������������������������������������������������������������������Ŀ
	//� Criar campo ICC_CODQT e incluir campo no chave do indice 1 da tabeka ICD. �
	//� Apos criacao subsitituir indice abaixo por: 								    � 
	//�   DbSetOrder(1) 														  � 
	//�   DbSeek(xFilial("IC4")+ICD->ICD_CODQT+ICD->ICD_PROCES)                   �      
	//����������������������������������������������������������������������������� 
	IC4->(DbSetOrder(1))
	IC4->(DbSeek(xFilial("IC4")+ICD->ICD_CODQT+ICD->ICD_PROCES))

	//�������������������������������������������������������������Ŀ
	//� Inicio da Transacao                                         �
	//��������������������������������������������������������������� 
	BEGIN TRANSACTION	

	DbSelectArea("ICH")
	DbSetOrder(2)		//FILIAL+PESQUISA+QUESTIONARIO+PROCESSO
	cKeyICH := xFilial("ICH")+IC5->IC5_CODPQ+ICD->ICD_CODQT+ICD->ICD_PROCES

	If DbSeek(cKeyICH)
		cEvento := ICH->ICH_EVENTO
	Else
		cEvento := GetSxeNum("ICH","ICH_EVENTO")
	EndIf

	DbSelectArea("ICH")
	DbSetOrder(1)
	If ! DbSeek(xFilial("ICH")+cEvento)
   		lAppend := .T.
	Else
   		lAppend := .F.
	EndIf
	RecLock("ICH",lAppend)
	Replace ICH_FILIAL		With xFilial("ICH")
	Replace ICH_EVENTO 	With cEvento
	Replace ICH_CODPQ		With ICC->ICC_CODPQ
	Replace ICH_CODQT		With IC4->IC4_CODQT
	Replace ICH_PROCES	With IC4->IC4_PROCES
	Replace ICH_MAT   		With cCodCRO
	
	ConfirmSX8()
	MsUnlock()
  	
	//��������������������������������������������������������������Ŀ
	//� Gravar Itens dos Eventos...		                             �
	//����������������������������������������������������������������
	DbSelectArea("ICD")
	DbSetOrder(1)
	DbSeek(xFilial("ICD")+ICC->ICC_RESPPQ)

	nRisco := 0	
	While !Eof() .AND. ICD_FILIAL == xFilial("ICD") .AND. ICD_RESPPQ == ICC->ICC_RESPPQ

	    nRisco ++
	    cRisco := StrZero(nRisco,2)

	    IC5->(DbSetOrder(1))
    	IC5->(DbSeek(xFilial("IC5")+ICC->ICC_CODPQ))
	    
		//��������������������������������������������������������������Ŀ
		//� Gravar Eventos de Riscos com base no conceito e respostas... �
		//����������������������������������������������������������������
		If IC5->IC5_GESTAO == "1" //Projetos...

			//������������������������������������������������������������������Ŀ
			//� Posicionar no aqruivo de perguntas para pegar alguns dados...    �
			//��������������������������������������������������������������������
			IC9->(DbSetOrder(1))
			IC9->(DbSeek(xFilial("IC9")+ICH->ICH_CODQT+ICH->ICH_PROCES+ICD->ICD_CODQST))
				
			//������������������������������������������������������������������Ŀ
			//� Pegar descricao das alternativas e colunas da pergunta.          �
			//��������������������������������������������������������������������
			aLinhas := ICEA120GAlt(IC9->IC9_CODQT,ICD->ICD_CODQST,3)
			//������������������������������������������������������������������Ŀ
			//� Pegar pontuacao da pergunta.                                     �
			//��������������������������������������������������������������������
			aPesos  := ICEA120GAlt(IC9->IC9_CODQT,ICD->ICD_CODQST,4)

			//������������������������������������������������������������������Ŀ
			//� Pegar descricao da pergunta.                                     �
			//��������������������������������������������������������������������
			aColunas := ICEA120GCol(IC9->IC9_CODQT,IC9->IC9_CODQST)

			cMemo := MSMM(IC9->IC9_CODCOM)
			
			//��������������������������������������������������������������Ŀ
			//� Gravando Metodo conforme Gestao Definida...                  �
			//����������������������������������������������������������������
			DbSelectArea("ICI")
			DbSetOrder(1)
			If ! DbSeek(xFilial("ICI")+ICH->ICH_EVENTO+cRisco)
			   	lAppend := .T.
			Else
				lAppend := .F.
			EndIf	
			RecLock("ICI",lAppend)
			Replace ICI_FILIAL	With xFilial("ICI")
			Replace ICI_EVENTO	With ICH->ICH_EVENTO
			Replace ICI_ITEM	With cRisco
			Replace ICI_DESC 	With Subs(cMemo,1,80)
			Replace ICI_CODALT	With ICD->ICD_CODALT
			Replace ICI_COMALT	With ICD->ICD_COMALT
			Replace ICI_CODCOL	With ICD->ICD_CODCOL
			If Len(aLinhas) > 0 .and. !Empty(ICI_CODALT)
				Replace ICI_DESALT	With aLinhas[Val(ICI_CODALT)]
				If aPesos[Val(ICI_CODALT)] >= 1 .AND. aPesos[Val(ICI_CODALT)] <= 6
					Replace ICI_PESO	With aPesos[Val(ICI_CODALT)]
				Else
					Replace ICI_VALOR	With aPesos[Val(ICI_CODALT)]
				EndIf
			EndIf		
			If Len(aColunas) > 0 .and. !Empty(ICI_CODCOL)
				Replace ICI_DESCOL	With aLinhas[Val(ICI_CODCOL)]
			EndIf
		    MsUnLock()
		    
			//��������������������������������������������������������������Ŀ
			//� Soma os pesos para obter a pontuacao...                      �
			//����������������������������������������������������������������
		    nPesos := nPesos + ICI->ICI_PESO
	    
			//��������������������������������������������������������������Ŀ
			//� Pto de entrada apos gravacao do gestao definida...           �
			//����������������������������������������������������������������
	 		If lICE12001
	 			ExecBlock("ICE12001",.F.,.F.)
		 	EndIf
		
		//����������������������������������������������������������������Ŀ
		//� Gravar Eventos de Riscos com base no conceito Sarbanes-Oxley...�
		//������������������������������������������������������������������
		ElseIf IC5->IC5_GESTAO == "2"    

			//������������������������������������������������������������������Ŀ
			//� Gravar eventos de riscos somente para a pergunta 1 (Risk?)...    �
			//��������������������������������������������������������������������	           
            If ICD->ICD_CODQST == "000001"
   				//������������������������������������������������������������������Ŀ
				//� Posicionar no aqruivo de perguntas para pegar alguns dados...    �
				//��������������������������������������������������������������������	
				IC9->(DbSetOrder(1))
				IC9->(DbSeek(xFilial("IC9")+ICH->ICH_CODQT+ICH->ICH_PROCES+ICD->ICD_CODQST))
				
				//������������������������������������������������������������������Ŀ
				//� Pegar descricao das alternativas e colunas da pergunta.          �
				//��������������������������������������������������������������������
				aLinhas := ICEA120GAlt(IC9->IC9_CODQT,ICD->ICD_CODQST,3)
				//������������������������������������������������������������������Ŀ
				//� Pegar pontuacao da pergunta.                                     �
				//��������������������������������������������������������������������
				aPesos  := ICEA120GAlt(IC9->IC9_CODQT,ICD->ICD_CODQST,4)
    
				If !Empty(GetSX3Cache("ICP_RISCO","X3_CAMPO"))
				   nItem := 0
				   DbSelectArea("ICP")	
				   DbSetOrder(1)
				   DbSeek(xFilial("ICP")+ICC->ICC_CODPQ+IC6->IC6_CODQT+IC4->IC4_PROCES+ICD->ICD_CODQST)
				   While !Eof() .AND. ICP_FILIAL == xFilial("ICP") .AND.;
						ICP_CODPQ == ICC->ICC_CODPQ .AND. ICP_CODQT == ICD->ICD_CODQT .AND.;
						ICP_PROCES == IC4->IC4_PROCES .AND.;
						ICP_CODQST == ICD->ICD_CODQST 
						nItem ++					              
						cItem := StrZero(nItem,2)
						DbSelectArea("ICI")
						DbSetOrder(1)		
						If ! DbSeek(xFilial("ICI")+ICH->ICH_EVENTO+ICP->ICP_RISCO)
						   	lAppend := .T.
						Else
							lAppend := .F.
						EndIf
						RecLock("ICI",lAppend)
						Replace ICI_FILIAL	With xFilial("ICJ")
						Replace ICI_EVENTO	With ICH->ICH_EVENTO
						Replace ICI_RISCO	With ICP->ICP_RISCO
						Replace ICI_ITEM	With cItem
						Replace ICI_DESC	With ICP->ICP_DESCRI
				        MsUnLock()
   						//��������������������������������������������������������������Ŀ
						//� Pto de entrada apos gravacao da Gestao Sarbanes-Oxley...     �
						//����������������������������������������������������������������
	 					If lICE12002
	 						ExecBlock("ICE12002",.F.,.F.)
			 			EndIf
				        DbSelectArea("ICP")
				        DbSkip()
					End    
			    EndIf   
 
				//����������������������������������������������������������������Ŀ
				//� Correr tabela de Risco Standard-ICI e gravar demais metodos... �
				//������������������������������������������������������������������
				DbSelectArea("ICI")
				DbSetOrder(1)
				If DbSeek(xFilial("ICI")+ICH->ICH_EVENTO)

		   			While !Eof() .and. ICI_FILIAL == xFilial("ICI");
		   		 		.and. ICI_EVENTO == ICH->ICH_EVENTO

						//������������������������������������������������������������Ŀ
						//� Gravar Metodo Mosler...                                    �
						//��������������������������������������������������������������
		   				DbSelectArea("ICJ")
						DbSetOrder(1)
						If ! DbSeek(xFilial("ICJ")+ICI->ICI_EVENTO+ICI->ICI_ITEM)
				   			lAppend := .T.
						Else
							lAppend := .F.
						EndIf		
						RecLock("ICJ",lAppend)
						Replace ICJ_FILIAL	With xFilial("ICJ")
						Replace ICJ_EVENTO	With ICI->ICI_EVENTO
						Replace ICJ_ITEM	With ICI->ICI_ITEM
						Replace ICJ_RISCO 	With ICI->ICI_RISCO
						Replace ICJ_DESC 	With ICI->ICI_DESC
						MsUnLock()
			 		
						//����������������������������������������������������������������Ŀ
						//� Pto de entrada apos gravacao do conceito Metodologia Mosler... �
						//������������������������������������������������������������������
	 					If lICE12005
			 				ExecBlock("ICE12005",.F.,.F.)
	 					EndIf
		
						//��������������������������������������������������������������Ŀ
						//� Gravando Metodo Willian T. Fine...                           �
						//����������������������������������������������������������������
						DbSelectArea("ICK")
						DbSetOrder(1)
						If ! DbSeek(xFilial("ICK")+ICI->ICI_EVENTO+ICI->ICI_ITEM)
						   	lAppend := .T.
						Else
							lAppend := .F.
						EndIf
						RecLock("ICK",lAppend)
						Replace ICK_FILIAL	With xFilial("ICK")
						Replace ICK_EVENTO	With ICI->ICI_EVENTO
						Replace ICK_ITEM	With ICI->ICI_ITEM
						Replace ICK_RISCO 	With ICI->ICI_RISCO
						Replace ICK_DESC 	With ICI->ICI_DESC
						MsUnLock()
			 		
						//����������������������������������������������������������������Ŀ
						//� Pto de entrada apos gravacao do conceito Metodologia T Fine... �
						//������������������������������������������������������������������
	 					If lICE12006
					 		ExecBlock("ICE12006",.F.,.F.)
	 					EndIf
	
						//��������������������������������������������������������������Ŀ
						//� Gravando Metodo Estatistico...                               �
						//����������������������������������������������������������������
						DbSelectArea("ICL")
						DbSetOrder(1)
						If ! DbSeek(xFilial("ICL")+ICI->ICI_EVENTO+ICI->ICI_ITEM)
						   	lAppend := .T.
						Else
							lAppend := .F.
						EndIf
						RecLock("ICL",lAppend)
						Replace ICL_FILIAL	With xFilial("ICL")
						Replace ICL_EVENTO	With ICI->ICI_EVENTO
						Replace ICL_ITEM	With ICI->ICI_ITEM 
						Replace ICL_RISCO 	With ICI->ICI_RISCO
						Replace ICL_DESC 	With ICI->ICI_DESC	
						MsUnLock()
			 		
						//����������������������������������������������������������������Ŀ
						//� Pto de entrada apos gravacao do conceito Metodologia T Fine... �
						//������������������������������������������������������������������
					 	If lICE12007
	 						ExecBlock("ICE12007",.F.,.F.)
					 	EndIf
						ConfirmSX8()
				
						DbSelectArea("ICI")
						DbSkip()
        			End
       		 	EndIf	
  			EndIf	        
		//����������������������������������������������������������������Ŀ
		//� Gravar Eventos de Riscos com base no Gestao Basileia II...     �
		//������������������������������������������������������������������
		ElseIf IC5->IC5_GESTAO == "3" 
			//��������������������������������������������������������������Ŀ
			//� Pto de entrada apos gravacao do conceito Basileia II...      �
			//����������������������������������������������������������������
	 		If lICE12003
	 			ExecBlock("ICE12003",.F.,.F.)
		 	EndIf
  	
		//����������������������������������������������������������������Ŀ
		//� Gravar Eventos de Riscos com base no conceito definido pelo    �
		//� Usuario...                                                     �
		//������������������������������������������������������������������
		Else
			//����������������������������������������������������������������Ŀ
			//� Pto de entrada apos gravacao do conceito do Usuario...         �
			//������������������������������������������������������������������
		 	If lICE12004
		 		ExecBlock("ICE12004",.F.,.F.)
	 		EndIf
		EndIf 
		
		
		DbSelectArea("ICD")
	 	DbSkip()
  	End
	//��������������������������������������������������������������Ŀ
	//� Grava pontuacao e Classe para o Eventp/Processo...           �
	//����������������������������������������������������������������
	If nPesos > 0
		RecLock("ICH",.F.)
		Replace ICH_PONTOS	With nPesos
		Replace ICH_CLASSE	With ICEXClasse(ICH_PONTOS)[1]
		MsUnLock()
	EndIf	
	
	//��������������������������������������������������������������Ŀ
	//� Fim da Transacao...                                          �
	//����������������������������������������������������������������
	END TRANSACTION

EndIf
RestArea(aArea)
Return
