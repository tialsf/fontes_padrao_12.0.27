#include "PROTHEUS.CH"
#include "FONT.CH"
#include "COLORS.CH"
#include "ICEC020.CH"
#include "MSGRAPHI.CH"

#define IMPACTO			1
#define PROBABILIDADE	2

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEC020    矨utor  � Lucas				� Data � 21/09/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Programa para Avaliacao de Riscos...                       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE                                                    潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/

Static Function MenuDef()

Local aRotina := {{STR0001, "AxPesqui"        , 0, 1,,.F.},; 	//"Pesquisar"
				  {STR0002, "ICEA200VIS"      , 0, 2 }} 	//"Visualizar"
				//{STR0004, "ICEA200Legenda"  , 0, 5 }}  	//"Legenda"

Return aRotina

Function ICEC020(aRotAuto,nOpc)

Local aCores := {}
Local cFiltro := ""
Local nTam := 0
Local cUsrLogin := ""
Local cUsrMatri := ""

Private cCadastro  := OemToAnsi(STR0005) //"Avaliacao de Riscos"
Private aCheckList := {}
Private aTopico    := {}
Private aRotina := MenuDef()

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Efetuar Selects para que os arquivos sejam criados.  �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
DbSelectArea("ICH")
DbSelectArea("ICI")
DbSelectArea("ICJ")
DbSelectArea("ICK")
DbSelectArea("ICL")
DbSelectArea("ICM")
DbSelectArea("ICN")

//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
//� Se for administrador nao processar filtro.       �
//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
If ! ICEXIsAdmin(__cUserId)
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Filtrar respostas por Usuario...                             �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
	cUsrLogin := ICEXUser(__cUserId,2)
	cUsrMatri := ICEXMat(cUsrLogin)
	nTam := TamSX3("ICH_MAT")[1]
	cFiltro := "ICH_MAT='"+Subs(cUsrMatri,1,nTam)+"'"
	
	DbSelectArea("ICH")

EndIf

MBrowse( 6, 1,22,75,"ICH",,,,,,aCores,,,,,,,,,,,, cFiltro )

Return(NIL)

