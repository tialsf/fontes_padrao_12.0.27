#include "protheus.ch"
#include "msGraphi.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ICEP010  � Autor � Rafael S. Bernardi    � Data �02/04/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Painel de Gestao - Pesquisas X Riscos                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Void            											  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � SIGAICE                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ICEP010()

Local aRetPanel := {} //Array com os dados que serao exibidos no painel
Local nX
Local aDesCpo   := SX3Desc({"IC5_CODPQ","ICI_EVENTO","ICI_DESC"})
Private aDados  := {}

dbSelectArea("ICH")
dbSelectArea("IC5")
dbSelectArea("ICI")

Pergunte("ICEP10",.F.)

//Geracao dos Dados para o Browse
ICEGerRis()

aAdd(aRetPanel,{||})
aAdd(aRetPanel,{aDesCpo[1],aDesCpo[2],aDesCpo[3]})//Filial###Evento Potencial de Risco###Descriaco do Risco
aAdd(aRetPanel,aDados)
aAdd(aRetPanel,{"CENTER","CENTER","LEFT"})

Return aRetPanel

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �ICEGerRis � Autor � Rafael S. Bernardi    � Data �02/04/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Gera os dados do painel de gestao Pesquisas X Riscos        ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                 											  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �SIGAICE                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
Function ICEGerRis()
Local cAliasQry := GetNextAlias()
Local nX

BeginSql Alias cAliasQry

SELECT IC5.IC5_CODPQ, ICI.ICI_EVENTO, ICI.ICI_DESC FROM %TABLE:ICI% ICI
JOIN %TABLE:ICH% ICH ON ICH.ICH_FILIAL = %XFILIAL:ICH% AND
		   ICH.ICH_EVENTO = ICI.ICI_EVENTO AND
		   ICH.%NOTDEL%
JOIN %TABLE:IC5% IC5 ON IC5.IC5_FILIAL = %XFILIAL:IC5% AND
		   IC5.IC5_CODPQ = ICH.ICH_CODPQ AND
		   IC5.%NOTDEL%
WHERE ICI.ICI_FILIAL = %XFILIAL:ICI% AND
      IC5.IC5_CODPQ BETWEEN %EXP:mv_par01% AND %EXP:mv_par02% AND
      ICI.%NOTDEL%

EndSql

dbSelectArea(cAliasQry)
If !(cAliasQry)->(Eof())
	While !(cAliasQry)->(Eof())
		aAdd(aDados,{&(cAliasQry+"->IC5_CODPQ"),&(cAliasQry+"->ICI_EVENTO"),&(cAliasQry+"->ICI_DESC")})
		(cAliasQry)->(DbSkip())
	EndDo
Else
	aAdd(aDados,{"","",""})
EndIf

(cAliasQry)->(DbCloseArea())

Return Nil