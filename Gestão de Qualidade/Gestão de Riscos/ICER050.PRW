#INCLUDE "ICER050.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "Report.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ICER050   �Autor  �Leandro Sabino      � Data �  20/07/06   ���
�������������������������������������������������������������������������͹��
���Desc.     � Relatorio de Pesquisas (SigaICE).                          ���
���          � (Versao Relatorio Personalizavel)                          ���
�������������������������������������������������������������������������͹��
���Uso       � Generico                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                            
Function ICER050()
Local oReport

Pergunte("ICR050",.F.) 
oReport := ReportDef()
oReport:PrintDialog()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ReportDef()   � Autor � Leandro Sabino   � Data � 18/07/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Montar a secao				                              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ReportDef()				                                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ICER050                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportDef()
Local cTitulo   := OemToAnsi(STR0001) 	// "Relatorio de Pesquisas"
Local cDesc1    := OemToAnsi(STR0002) 	// "Este programa ir� imprimir a rela��o das pesquisas"
Local cDesc2 	:= OemToAnsi(STR0003) 	// "que origina os procedimentos de controle e gerenciamento"
Local cDesc3    := OemToAnsi(STR0004) 	// "de riscos, considerando as normas de controles internos"
Local aMidia 	:= { STR0022,STR0023,STR0024}
Local aStatus 	:= { STR0025,STR0026}
Local aGestao 	:= { STR0027,STR0028,STR0029}
Local oSection1 
Local oSection2
Local oSection3  
Local oSection4

DEFINE REPORT oReport NAME "ICER050" TITLE cTitulo PARAMETER "ICR050" ACTION {|oReport| PrintReport(oReport)} DESCRIPTION (cDesc1+cDesc2+cDesc3)
oReport:SetPortrait()

DEFINE SECTION oSection1 OF oReport TABLES "IC5" TITLE STR0011 // "Pesquisa"
DEFINE CELL NAME "IC5_CODPQ"  OF oSection1 ALIAS "IC5" 
DEFINE CELL NAME "IC5_DESC"   OF oSection1 ALIAS "IC5" 
DEFINE CELL NAME "IC5_GESTAO" OF oSection1 ALIAS "IC5" TITLE  TITSX3("IC5_GESTAO")[1] SIZE 100 BLOCK{|| aGestao[Val(IC5->IC5_GESTAO)]}
DEFINE CELL NAME "IC5_DATINA" OF oSection1 ALIAS "IC5" 

DEFINE SECTION oSection2 OF oSection1 TABLES "IC5" TITLE STR0030 // "Processos"
DEFINE CELL NAME "IC5_MIDIA"  OF oSection2 ALIAS "IC5" TITLE  TITSX3("IC5_MIDIA")[1]  SIZE 15 BLOCK{|| aMidia[Val(IC5->IC5_MIDIA)]	}
DEFINE CELL NAME "IC5_STATUS" OF oSection2 ALIAS "IC5" TITLE  TITSX3("IC5_STATUS")[1] SIZE 15 BLOCK{|| aStatus[Val(IC5->IC5_STATUS)]}
DEFINE CELL NAME "IC5_DTDE"   OF oSection2 ALIAS "IC5" 
DEFINE CELL NAME "IC5_INICIO" OF oSection2 ALIAS "IC5" 
DEFINE CELL NAME "IC5_DTATE"  OF oSection2 ALIAS "IC5" 
DEFINE CELL NAME "IC5_FINAL"  OF oSection2 ALIAS "IC5" 

DEFINE SECTION oSection3 OF oSection2 TABLES "IC5" TITLE STR0019 // "Publico Alvo"
DEFINE CELL NAME "IC5_PUBLIC" OF oSection3 ALIAS "IC5" 
DEFINE CELL NAME "IC5_RESP"   OF oSection3 ALIAS "IC5" 

DEFINE SECTION oSection4 OF oSection3 TABLES "IC5" TITLE STR0013 // "Descricao"
DEFINE CELL NAME "cDescricao" OF oSection4 ALIAS "IC5" TITLE TITSX3("IC5_OBJETI")[1] SIZE 100 LINE BREAK BLOCK{|| MSMM(IC5->IC5_CODOBJ)}

Return oReport

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PrintReport   � Autor � Leandro Sabino   � Data � 19/06/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Relatorio de Pesquisas (SigaICE) 						  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PrintReport(ExpO1)  	     	                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto oPrint                                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ICER050                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/                  
Static Function PrintReport(oReport) 
Local oSection1 := oReport:Section(1)
Local oSection2 := oSection1:Section(1)
Local oSection3 := oSection2:Section(1)
Local oSection4 := oSection3:Section(1)
Local cQuery	:= ""

MakeAdvplExpr(oReport:uParam)	

DbSelectArea("IC5")
DbSetOrder(1)
	
cQuery := 'IC5_FILIAL  ==  "' +xFilial("IC5")+ '"'
cQuery += ' .AND. IC5_CODPQ   >= "' +MV_PAR01+ '" .AND. IC5_CODPQ  <= "' + MV_PAR02 +'"' 
    
oSection1:SetFilter(cQuery) 
oSection2:SetRelation({|| xFilial("IC5")+IC5->IC5_CODPQ},"IC5",1,.T.)
oSection2:SetParentFilter( { |cParam| IC5->IC5_FILIAL+IC5->IC5_CODPQ == cParam },{ || IC5->IC5_FILIAL+IC5->IC5_CODPQ})  
oSection3:SetRelation({|| xFilial("IC5")+IC5->IC5_CODPQ},"IC5",1,.T.)
oSection3:SetParentFilter( { |cParam| IC5->IC5_FILIAL+IC5->IC5_CODPQ == cParam },{ || IC5->IC5_FILIAL+IC5->IC5_CODPQ})   
oSection4:SetRelation({|| xFilial("IC5")+IC5->IC5_CODPQ},"IC5",1,.T.)
oSection4:SetParentFilter( { |cParam| IC5->IC5_FILIAL+IC5->IC5_CODPQ == cParam },{ || IC5->IC5_FILIAL+IC5->IC5_CODPQ})

oSection1:Print()
oSection1:Finish()
    		
Return NIL			    
