#INCLUDE "ICER090.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "Report.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ICER090   �Autor  �Leandro Sabino      � Data �  28/07/06   ���
�������������������������������������������������������������������������͹��
���Desc.     � Relatorio de Pesquisas (SigaICE).                          ���
���          � (Versao Relatorio Personalizavel)                          ���
�������������������������������������������������������������������������͹��
���Uso       � Generico                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                            
Function ICER090()
Local oReport

Pergunte("ICR090",.F.) 
oReport := ReportDef()
oReport:PrintDialog()        

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ReportDef()   � Autor � Leandro Sabino   � Data � 28/07/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Montar a secao				                              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ReportDef()				                                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ICER090                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportDef()
Local oReport                                             
Local oSection1, oSection2,oSection3,oSection4,oSection5, oSection6
Local cTitulo   := OemToAnsi(STR0001) 	// "Follow-Up"
Local cDesc1    := OemToAnsi(STR0002) 	// "Este programa ira                  imprimir o acompanhamento de todo o processo"
Local cDesc2 	:= OemToAnsi(STR0003) 	// "desde a pesquisa ate o plano de acao dos processos internos mais"
Local cDesc3    := OemToAnsi(STR0004) 	// "os respectivos eventos potencias de risco."

DEFINE REPORT oReport NAME "ICER090" TITLE cTitulo PARAMETER "ICR090" ACTION {|oReport| PrintReport(oReport)} DESCRIPTION (cDesc1+cDesc2+cDesc3)
oReport:SetLandscape(.T.)

DEFINE SECTION oSection1 OF oReport TABLES "IC5"
DEFINE CELL NAME "IC5_CODPQ" OF oSection1 ALIAS "IC5" 
DEFINE CELL NAME "IC5_DESC"  OF oSection1 ALIAS "IC5" 

DEFINE SECTION oSection2 OF oSection1 TABLES "IC6"
DEFINE CELL NAME "IC6_CODQT" OF oSection2 ALIAS "IC6" TITLE  STR0012 PICTURE PesqPict("IC6","IC6_CODQT") SIZE (TamSx3("IC6_CODQT")[1]) //Processo
DEFINE CELL NAME "IC6_DESC"  OF oSection2 ALIAS "IC6" 

DEFINE SECTION oSection3 OF oSection2 TABLES "IC4"
DEFINE CELL NAME "IC4_PROCES" OF oSection3 ALIAS "IC4" TITLE  TitSx3("IC4_PROCES")[1] PICTURE PesqPict("IC4","IC4_PROCES") SIZE (TamSx3("IC4_PROCES")[1]) 
DEFINE CELL NAME "IC4_DESCRI" OF oSection3 ALIAS "IC4" 
DEFINE CELL NAME "cDESCRI" OF oSection3 ALIAS "IC4" TITLE  STR0014 SIZE (TamSx3("IC4_DESCRI")[1]) BLOCK{||IIF(IC4->IC4_PERGS=="1",STR0016,STR0017)}//Sim###Nao
DEFINE CELL NAME "cDESCRI" OF oSection3 ALIAS "IC4" TITLE  STR0015 SIZE (TamSx3("QMZ_DTSAID")[1]) BLOCK{||IF(IC4->IC4_FLUXOS=="1",STR0016,STR0017)}//Sim###Nao
DEFINE CELL NAME "cDESCRI" OF oSection3 ALIAS "IC4" TITLE  STR0019 SIZE (TamSx3("QMZ_DTSAID")[1]) BLOCK{||If(IC4->IC4_FLUXOS=="1".AND.!EMPTY(IC4->IC4_NOMDOC),LEFT(Alltrim(IC4->IC4_NOMDOC),25)," ")}

DEFINE SECTION oSection4 OF oSection3 TABLES "IC6"
DEFINE CELL NAME "cCODPQ" OF oSection4 ALIAS "IC6" TITLE  STR0018 BLOCK{||MSMM(IC4->IC4_CODOBJ)}
                                                                    
DEFINE SECTION oSection5 OF oSection4 TABLES "ICI"
DEFINE CELL NAME "ICI_RISCO" OF oSection5 ALIAS "ICI" TITLE  STR0023 //Risco
DEFINE CELL NAME "ICI_DESC"  OF oSection5 ALIAS "ICI" TITLE  STR0024 SIZE 39 //Descricao do Risco
DEFINE CELL NAME "ICI_RISCO" OF oSection5 ALIAS "ICI" TITLE  STR0025 SIZE 62 //Risco Referencia 

DEFINE SECTION oSection6 OF oSection5 TABLES "ICI"
DEFINE CELL NAME "cREV"    OF oSection6 ALIAS "ICI" TITLE  TitSx3("ICN_REV")[1]   SIZE (TamSx3("ICN_REV")[1])

DEFINE CELL NAME "cDTREV"  OF oSection6 ALIAS "ICI" TITLE  TitSx3("ICN_DTREV")[1] SIZE (TamSx3("ICN_DTREV")[1])
DEFINE CELL NAME "cNOME"   OF oSection6 ALIAS "ICI" TITLE  TitSx3("QAA_NOME")[1]  SIZE (TamSx3("QAA_NOME")[1])

DEFINE CELL NAME "cREGIST" OF oSection6 ALIAS "ICI" TITLE  STR0032 //Registrada
DEFINE CELL NAME "cMAT"    OF oSection6 ALIAS "ICI" TITLE  STR0032 //Responsavel
DEFINE CELL NAME "cCODACA" OF oSection6 ALIAS "ICI" TITLE  STR0033 // ID: R/P

Return oReport

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PrintReport   � Autor � Leandro Sabino   � Data � 28/07/06 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Follow-up						 						  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PrintReport(ExpO1)  	     	                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto oPrint                                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ICER050                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/                  
Static Function PrintReport(oReport) 
Local oSection1   := oReport:Section(1)
Local oSection2   := oReport:Section(1):Section(1)
Local oSection3   := oReport:Section(1):Section(1):Section(1)
Local oSection4   := oReport:Section(1):Section(1):Section(1):Section(1)
Local oSection5   := oReport:Section(1):Section(1):Section(1):Section(1):Section(1) 
Local oSection6   := oReport:Section(1):Section(1):Section(1):Section(1):Section(1):Section(1)

Local lIntegraQNC := GetNewPar("MV_ICEXQNC","2")=="1"
Local aGestao     := { STR0020,STR0021,STR0022}
Local lCabec      := .T.

MakeAdvplExpr(oReport:uParam)	

DbSelectArea("IC5")
DbSetOrder(1)
DbSeek(xFilial("IC5")+mv_par01,.T.)

While !Eof() .and. IC5_FILIAL == xFilial("IC5") .and. IC5_CODPQ <= mv_par02
	DbSelectArea("IC6")
	DbSetOrder(1)
	DbSeek(xFilial("IC6")+IC5->IC5_CODPQ)

	While !Eof() .and. IC6_FILIAL == xFilial("IC6") .and. IC6_CODPQ == IC5->IC5_CODPQ 
		//���������������������������������������������������������������Ŀ
		//� Considerar os Questionarios conforme perguntas....	           �
		//�����������������������������������������������������������������
    	If IC6->IC6_CODQT < mv_par03 .OR. IC6->IC6_CODQT > mv_par04
			DbSelectArea("IC6")
			DbSkip()
		EndIf
		
   		DbSelectArea("IC4")
		DbSetOrder(1)
		DbSeek(xFilial("IC4")+IC6->IC6_CODQT)

		While !Eof() .and. IC4_FILIAL == xFilial("IC4") .and. IC4_CODQT == IC6->IC6_CODQT

		   	If IC4->IC4_PROCES < mv_par05 .OR. IC4->IC4_PROCES > mv_par06
				DbSelectArea("IC4")
				DbSkip()
			EndIf

			cSindex := IC5->IC5_CODPQ+IC6->IC6_CODQT+IC4->IC4_PROCES
			
			DbSelectArea("ICH")
			DbSetOrder(2)
			If DbSeek(xFilial("ICH")+cSindex)
                
				While !Eof() .and. ICH_FILIAL == xFilial("ICH") .and. ICH_CODPQ == IC5->IC5_CODPQ .and.;
							   	   ICH_CODQT == IC6->IC6_CODQT .and. ICH_PROCES == IC4->IC4_PROCES				
					
					If lCabec 
						oSection1:Init()//Pesquisa
						oSection2:Init()//Processo
						oSection3:Init()//Sub-Processo
						oSection4:Init()//Objetivo
							
						oSection1:PrintLine()
						oSection2:PrintLine()
						oSection3:PrintLine()
						oSection4:PrintLine()
						lCabec := .F.
										
						oReport:PrintText(" ___________________________",oReport:Row(),025) 
						oReport:SkipLine(1)	
						oReport:PrintText("/ "+aGestao[Val(IC5->IC5_GESTAO)],oReport:Row(),025) 
						oReport:PrintText("\__________________________________________________________________________________" ,oReport:Row(),025) 
						oReport:SkipLine(1)			
						
					Endif
							
					DbSelectArea("ICI")
					DbSetOrder(1)
					DbSeek(xFilial("ICI")+ICH->ICH_EVENTO)

					While ICI->(!Eof()) .and. ICI->ICI_FILIAL == xFilial("ICI") .and. ICI->ICI_EVENTO == ICH->ICH_EVENTO

    					oSection5:Init()
						oSection5:PrintLine()
			
						oReport:SkipLine(1) 
						oReport:PrintText(STR0026,oReport:Row(),0850) //PLANO DE ACAO
						oReport:SkipLine(1)	

                        oReport:PrintText(STR0027+": "+ICI->ICI_RISCO+"  "+STR0028+IIF(!Empty(ICI->ICI_CODACA),LEFT(ICI->ICI_CODACA,64)," "),oReport:Row(),025) //"Risco"##PLANO DE  ACAO/FNC"
						oReport:SkipLine(1)	
 
						DbSelectArea("ICM")	
						ICM->(DbSetOrder(1))
						ICM->(DbSeek(xFilial("ICM")+ICI->ICI_CODACA))	

						
						If !lIntegraQNC 
                            DbSelectArea("QAA")
							QAA->(DbSetOrder(1))
							QAA->(DbSeek(xFilial("QAA")+ICM->ICM_MAT))
				   			 						
				   			oReport:PrintText(STR0029+": "+QAA->QAA_NOME,oReport:Row(),025)//Responsavel
							oReport:SkipLine(1)		
							oReport:PrintText(STR0030+": "+STR0031+"             "+STR0033,oReport:Row(),025)//Revisao##Registrada##ID.QNC 
				    		oReport:SkipLine(1)	

							DbSelectArea("ICN")
							ICN->(DbSetOrder(1))
							ICN->(DbSeek(xFilial("ICN")+ICM->ICM_CODIGO))
							
							oSection6:Init()
							
							While !Eof() .AND. ICN_FILIAL == xFilial("ICN") .AND.ICN_CODIGO == ICM->ICM_CODIGO

								QAA->(DbSetOrder(1))
								QAA->(DbSeek(xFilial("QAA")+ICN->ICN_MAT))    								
	                                                       
								oSection6:Cell("cREV"):SetValue(ICN->ICN_REV)
								oSection6:Cell("cREV"):Show()

								oSection6:Cell("cDTREV"):SetValue(ICN->ICN_DTREV)
								oSection6:Cell("cDTREV"):Show()

								oSection6:Cell("cNOME"):SetValue(QAA->QAA_NOME)
								oSection6:Cell("cNOME"):Show()

								oSection6:Cell("cREGIST"):Hide()
								oSection6:Cell("cREGIST"):HideHeader()
								
								oSection6:Cell("cMAT"):Hide()         
								oSection6:Cell("cMAT"):HideHeader()
								
								oSection6:Cell("cCODACA"):Hide()					
								oSection6:Cell("cCODACA"):HideHeader()
								
							    oSection6:PrintLine()
							    oSection6:Finish()                             
								DbSkip()
					    	End    
					    Else
							DbSelectArea("QI2")
							QI2->(DbSetOrder(5))
							QI2->(DbSeek(xFilial("ICM")+ICI->ICI_CODACA))
	
							oReport:PrintText(STR0028+" "+QI2->QI2_DESCR,oReport:Row(),025) //PLANO DE  ACAO/FNC"
							oReport:SkipLine(1)	

							DbSelectArea("QAA")
	                        QAA->(DbSetOrder(1))
							QAA->(DbSeek(xFilial("QAA")+QI2->QI2_MATRES))			   			
			
							oReport:PrintText(STR0029+" "+QAA->QAA_NOME,oReport:Row(),025) //Responsavel
							oReport:SkipLine(1)	
				    		
							oSection6:Init()                           

							DbSelectArea("QI2")
							QI2->(DbSetOrder(5))
							QI2->(DbSeek(xFilial("QI2")+ICM->ICM_CODIGO))
							
							While QI2->(!Eof()) .AND. QI2->QI2_FILIAL == xFilial("QI2") .AND. QI2->QI2_FNC == ICM->ICM_CODIGO

								oSection6:Cell("cREV"):SetValue(ICN->ICN_REV)
								oSection6:Cell("cREV"):Show()
								
								oSection6:Cell("cDTREV"):Hide()
								oSection6:Cell("cDTREV"):HideHeader()
								
								oSection6:Cell("cNOME"):Hide()
								oSection6:Cell("cNOME"):HideHeader()
								
								oSection6:Cell("cREGIST"):SetValue(QI2->QI2_REGIST)
								oSection6:Cell("cREGIST"):Show()
								
								oSection6:Cell("cMAT"):SetValue(QAA->QAA_MAT) 
								oSection6:Cell("cMAT"):Show()
								
								oSection6:Cell("cCODACA"):SetValue(QI2->QI2_CODACA)
								oSection6:Cell("cCODACA"):Show()
								
								oSection6:PrintLine()
  
					  	  	    DbSkip()
					    	End   		    
					    EndIf	   	
            
					    DbSelectArea("ICI")
	        			DbSkip() 
	        				      
	        			oSection5:Finish()
	        			
	   				End
		        	DbSelectArea("ICH")
		        	DbSkip() 
		        	lCabec := .T.	      
	    	    End  
	  		Endif
        	DbSelectArea("IC4")
	    	DbSkip()	      
		End
    	DbSelectArea("IC6")
	    DbSkip()	      
	End	     
	DbSelectArea("IC5")
    DbSkip()
End
Return Nil					    