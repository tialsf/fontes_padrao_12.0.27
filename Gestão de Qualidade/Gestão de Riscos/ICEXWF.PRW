#INCLUDE "ICEXWF.CH"
#INCLUDE "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICEWF090    | Autor � Lucas              � Data � 05.10.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Enviar email aos Destinatarios da Pesquisa, e gravar o     ���
���          � cabecalho de respostas...                                  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICEWF090()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpL1 := Expressao Logica...                               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � ICEA090/SIGAICE                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICEWF090()

Local nI := 0
Local cCodPQ   := ""
Local cSeqFld  := ""
Local cArquivo := ""
Local cFiltro  := ""
Local cUsrPQ   := ""
Local cUsrName := ""
Local cEntidade := ""

Local aDestina  := {}
Local aUsuarios := {}
Local aArea := GetArea()

Local cMailConta
Local cMailServer
Local cMailSenha
Local cSendConta
Local lEnviaMail := .F.
Local aWhere := {}
Local cCabEMail  := ""
Local cMensEMail := ""
Local cSubject := ""
Local nC
Local oProcess 
Local cDirFlow    := GetNewPar("MV_QDIRFLW","\MP8\WORKFLOW\")
Local cStartPath := GetSrvProfString("Startpath","")
Local nFiltro := 0

AADD(aWhere,{"AC4","AC4_FILIAL","AC4_FILIAL,AC4_PARTNE"})
AADD(aWhere,{"ACH","ACH_FILIAL","ACH_FILIAL,ACH_CODIGO"})
AADD(aWhere,{"SA1","A1_FILIAL" ,"A1_FILIAL,A1_COD,A1_LOJA"})
AADD(aWhere,{"SA2","A2_FILIAL" ,"A2_FILIAL,A2_COD,A2_LOJA"})
AADD(aWhere,{"SA4","A4_FILIAL" ,"A4_FILIAL,A4_COD"})
AADD(aWhere,{"SU2","U2_FILIAL" ,"U2_FILIAL,U2_COD,U2_CONCOR"})
AADD(aWhere,{"SUS","US_FILIAL" ,"US_FILIAL,US_COD,US_LOJA"})
AADD(aWhere,{"JA2","JA2_FILIAL","JA2_FILIAL,JA2_NUMRA"})
AADD(aWhere,{"IC2","IC2_FILIAL","IC2_FILIAL,IC2_COMITE,IC2_MAT"})
AADD(aWhere,{"QAA","QAA_FILIAL","QAA_FILIAL,QAA_MAT"})

//�������������������������������������������������������������Ŀ
//� Processar regra e enviar email para Destinatarios...        �
//���������������������������������������������������������������
//If MsgYesNo(STR0001,{STR0002,STR0003})		//Enviar email aos destinatarios da pesquisa?###Sim###Nao
	lEnviaMail := .T.
//EndIf	


DbSelectArea("ICE")
DbSetOrder(1)
DbSeek(xFilial("ICE")+IC5->IC5_CODPQ)   

While !Eof() .AND. ICE_FILIAL == xFilial("ICE") .AND. ICE_CODPQ == IC5->IC5_CODPQ
		
	cCodPQ   := ICE->ICE_CODPQ
	cSeqFld  := ICE->ICE_SEQFLD
	cArquivo := ICE->ICE_ARQUIV
	
	nFiltro := 1	    
	While ! Eof() ;
		.AND. ICE_FILIAL == xFilial("ICE");
		.AND. ICE_CODPQ == cCodPQ .AND. ICE_SEQFLD == cSeqFld ;
		.AND. ICE_ARQUIV == cArquivo

		If ICE_CAMPO == "TODOS(*)  "
			cFiltro := ".T."
		Else
			If AllTrim(ICE_OPER1) == "1"
				nTam := TamSX3(ICE_CAMPO)[1]
				cFiltro += "AND "+AllTrim(ICE_CAMPO)+" = '"+Subs(ICE_CONT1,1,nTam)+"'"
			EndIf	
		EndIf
		DbSelectArea("ICE")
		DbSkip()
	End	
	nFiltro:=0			      
	AADD(aDestina,{cSeqFld,cArquivo,cFiltro})
End

aUsuarios := {}
aContacts := {}

cSubject   := IC5->IC5_DESC  
cMensEMail := cDirFlow+"ICEPesquisa.htm"
aAtaches   := cDirFlow+"ICEPesquisa.htm"

For nI := 1 To Len(aDestina)
		
	cFiltro := aDestina[nI][3]
	If AllTrim(aDestina[nI][3])=="!Eof()"
		cFiltro := ".T."
	EndIf

	DbSelectArea(aDestina[nI][2])
	DbSetOrder(1)
        
		nPosWhere := Ascan(aWhere,{|x| x[1]==aDestina[nI][2]})
		If cFiltro == ".T."
			lQuery := .T.
		   	cQuery := " SELECT * FROM "+ RetSqlName(aDestina[nI][2])
			cQuery += " ORDER BY "+aWhere[nPosWhere][3]
		Else
		   	cQuery := " SELECT * FROM "+ RetSqlName(aDestina[nI][2])
		   	If nPosWhere > 0
				lQuery := .T.
				cQuery += " WHERE "+ aWhere[nPosWhere][2]+" = '" + xFilial(aDestina[nI][2]) + "' "															
				cQuery += cFiltro
				cQuery += " AND D_E_L_E_T_ = ' ' "
				cQuery += " ORDER BY "+aWhere[nPosWhere][3]
			EndIf
			cQuery := ChangeQuery(cQuery)
			
			cAliasQry := aDestina[nI][2]+"QRY"

			If Select(cAliasQry) > 0
				DbSelectArea(cAliasQry)
				DbCloseArea()
			EndIf
			DbUseArea(.T.,"TOPCONN", TCGenQry(,,cQuery), cAliasQry, .F., .T.)

		EndIf		
 
	DbGoTop()
 			
	While !Eof()
			
		If !lQuery
			lFiltro := If(cFiltro==".T.",.T.,&(cFiltro))
		Else
			lFiltro := .T.
		EndIf
				   
		If lFiltro	

	      //���������������������������������������������������������������Ŀ
			//� Inicializar array de destinatarios para enviar comunicacao... �
			//�����������������������������������������������������������������				
			If Subs(Alias(),1,3) $ "AC4"
				AADD(aUsuarios,{AC4_NOME,;
							    AC4_HPAGE,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := AC4_MAT
				cUsrName := AC4_NOME
				
			ElseIf Subs(Alias(),1,3) $ "ACH"
				AADD(aUsuarios,{ACH_RAZAO,;
							    ACH_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := ACH_CODIGO
				cUsrName := ACH_RAZAO

			ElseIf Subs(Alias(),1,3) $ "SA1"
				AADD(aUsuarios,{A1_CONTATO,;
							    A1_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := A1_COD
				cUsrName := A1_CONTATO

			ElseIf Subs(Alias(),1,3) $ "SA2"
				AADD(aUsuarios,{A2_CONTATO,;
							    A2_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := A2_COD
				cUsrName := A2_CONTATO

			ElseIf Subs(Alias(),1,3) $ "SA4"
				AADD(aUsuarios,{A4_NOME,;
							    A4_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := A4_COD
				cUsrName := A4_CONTATO			

			ElseIf Subs(Alias(),1,3) $ "SUS"
				AADD(aUsuarios,{US_NOME,;
							    US_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := US_COD
				cUsrName := US_NOME
					   			
			ElseIf Subs(Alias(),1,3) $ "JA2"
				AADD(aUsuarios,{JA2_NOMCON,;
							    JA2_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})
				cUsrPQ := JA2_NUMRA
				cUsrName := JA2_NOMCON			

			ElseIf Subs(Alias(),1,3) $ "IC2"
				//AADD(aUsuarios,{IC2_NOME,;
				//			    IC2_EMAIL,;
				//			    {{cCabEMail,cMensEMail,aAtaches,""}}})
				QAA->(DbSetOrder(1))
				QAA->(DbSeek(xFilial("QAA")+IC2->IC2_MAT))
				cUsrPQ := QAA->QAA_MAT
				cUsrName := QAA->QAA_NOME
				cEntidade := "IC2"

				If IC2_NIVEL == "4" 
					AAdd( aContacts, { AllTrim(QAA->QAA_NOME), AllTrim( QAA->QAA_EMAIL ) } )	
				Else
					cUsrPQ := ""
					cUsrName := ""
					cEntidade := ""	
				EndIf	

			ElseIf Subs(Alias(),1,3) $ "QAA"
				AADD(aUsuarios,{QAA_NOME,;
							    QAA_EMAIL,;
							    {cCabEmail,cMensEmail,"",""}})

				cUsrPQ := QAA_MAT
				cUsrName := QAA_NOME
			EndIf
				
	        //�������������������������������������������������������������Ŀ
			//� Gravar cabecalho do arquivo de respostas...                 �
			//���������������������������������������������������������������
			If !Empty(cUsrPQ)   .AND. ICEXHaPergs(IC5->IC5_CODPQ)
				cRespPQ := GetSXENum("ICC","ICC_RESPPQ")
				DbSelectArea("ICC")
				DbSetOrder(2)
				If ! DbSeek(xFilial("ICC")+IC5->IC5_CODPQ+cUsrPQ)
	 			   RecLock("ICC",.T.)
				   Replace ICC_FILIAL	With xFilial("ICC")
				   Replace ICC_RESPPQ	With cRespPQ
				   Replace ICC_CODPQ	With IC5->IC5_CODPQ
				   Replace ICC_MAT		With cUsrPQ
				   Replace ICC_ENTIDA	With cEntidade
				   Replace ICC_FINALI	With "2"
				   MsUnLock()
				   
				   ConfirmSX8()
			   
				EndIf
			EndIf	   
		EndIf							
					
        DbSelectArea(aDestina[nI][2])
        DbSkip()
     End           
Next nI

//�������������������������������������������������������������Ŀ
//� Verifica se existe Destinatarios...                         �
//���������������������������������������������������������������
If Len( aContacts ) == 0
	MsgAlert(STR0004)                    //Destinatario(s) nao cadastrado(s) para essa pesquisa."
	RestArea( aArea )
Endif

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
//If Len( aContacts ) > 0
//	MsgInfo(STR0005)						//Enviando email ao destinatario(s) cadastrado(s) para essa pesquisa.
//EndIf

If File(cDirFlow+"WFICEPQ.htm")

	For nC := 1 To Len(aContacts)

		//�������������������������������������������������������������Ŀ
		//| Repete os procedimentos para cada destinatario.				�
		//���������������������������������������������������������������
		//�������������������������������������������������������������Ŀ
		//| Inicializacao da classe de processos do workflow.			�
		//���������������������������������������������������������������
		oProcess := TWFProcess():New( "ICEPQ", cSubject + " #" + IC5->IC5_CODPQ )

		//�������������������������������������������������������������Ŀ
		//| Seleciono o html que sera utilizado no corpo da mensagem.   |
		//���������������������������������������������������������������
		oProcess:NewTask( cSubject, cDirFlow+"WFICEPQ.htm" )

		//�������������������������������������������������������������Ŀ
		//| Identificacao da Pesquisa									|
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "PQ_CODPQ", IC5->IC5_CODPQ )
		oProcess:ohtml:ValByName( "PQ_DESC" , IC5->IC5_DESC )
		oProcess:ohtml:ValByName( "PQ_DTDE" , IC5->IC5_DTDE )
		oProcess:ohtml:ValByName( "PQ_DTATE", IC5->IC5_DTATE )
		oProcess:ohtml:ValByName( "PQ_RESP" , IC5->IC5_RESP )

		//�������������������������������������������������������������Ŀ
		//| Nome do Destinatario									    |
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "ID_NOME", aContacts[nC][1])	// Destinatario
		oProcess:ohtml:ValByName( "ID_CODPQ",IC5->IC5_CODPQ)	// Cod. da Pesquisa
	
		oProcess:cSubject := cSubject	// Assunto

		//�������������������������������������������������������������Ŀ
		//| Inicializa a geracao do processo e envio da mensagem.		|
		//���������������������������������������������������������������
		//oP:ohtml:ValByName( "BEGIN_TEXT", aContacts[nC,1] )
		oProcess:cTo := aContacts[nC,1] + "<" + aContacts[nC,2] + ">"
		oProcess:Start()
	Next nC
Else
	Help("",1,"NOHTM")
EndIf	


RestArea(aArea)
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICEWF120    | Autor � Lucas              � Data � 05.10.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Enviar email ao Gestor de Risco informando que Usuario     ���
���          � participante do Comite respondeu o Questionario/Pesquisa.  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICEWF120()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpL1 := Expressao Logica...                               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � ICEA120/SIGAICE                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICEWF120()
Local aContacts := {}
Local cUser := ""
Local cSubject := STR0006+" -  "+IC5->IC5_DESC 
Local nC 
Local lTodos := .F.
Local cQuery := ""
Local aArea := GetArea()
Local aAsk := {}
Local aRisco := {}
Local nI := 0
Local cDirFlow    := GetNewPar("MV_QDIRFLW","\MP8\WORKFLOW\")
//Local cStartPath := GetSrvProfString("Startpath","")

cUser := ICEXUser(__cUserId,4)

If ICC->ICC_ENTIDA == "IC2"
	DbSelectArea("ICE")
	DbSetOrder(1)
	If DbSeek(xFilial("ICE")+ICC->ICC_CODPQ)
	   While ! Eof() .and. ICE_FILIAL == xFilial("ICE") .and. ICE_CODPQ == ICC->ICC_CODPQ
	   		If AllTrim(ICE_CAMPO) == "TODOS(*)"
	   			lTodos := .T.
	   		Else
	   			cQuery := ""
	   		Endif
	   		DbSkip()
	   	End
		If lTodos
			DbSelectArea("IC2")
			DbSetOrder(1)
			If DbSeek(xFilial("IC2"))
				cComite := IC2_COMITE
			EndIf
		EndIf
	EndIf
Else 
	//
EndIf	 					

//�������������������������������������������������������������Ŀ
//� Pegar email do Gestor de Riscos no Comite...                �
//���������������������������������������������������������������	   			
DbSelectArea("IC2")
DbSetOrder(1)
If DbSeek(xFilial("IC2")+cComite)
   While !Eof() .and. IC2_FILIAL == xFilial("IC2") .and. IC2_COMITE == cComite
   		If IC2_NIVEL == "3"
   			QAA->(DbSetOrder(1))
   			QAA->(DbSeek(xFilial("QAA")+IC2->IC2_MAT))
			AAdd( aContacts, { AllTrim(QAA->QAA_NOME), AllTrim(QAA->QAA_EMAIL) } )
        EndIf
        DbSkip()
   End     
EndIf

//�������������������������������������������������������������Ŀ
//� Verifica se existe Destinatarios...                         �
//���������������������������������������������������������������
If Len( aContacts ) == 0
	MsgAlert(STR0004)
	RestArea( aArea )
Endif

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
//If Len( aContacts ) > 0
//	MsgInfo(STR0005)
//EndIf

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
DbSelectArea("IC6")
DbSetOrder(1)
DbSeek(xFilial("IC6")+ICC->ICC_CODPQ)
While ! Eof() .and. IC6_CODPQ == ICC->ICC_CODPQ
	aAsk := {}
	aRisco := {}
	DbSelectArea("IC9")
	DbSetOrder(1)
	DbSeek(xFilial("IC9")+IC6->IC6_CODQT)
	While ! Eof() .and. IC9_CODQT == IC6->IC6_CODQT
 		AAdd(aAsk,{IC9->IC9_DESC,IC9->IC9_OBSERV})
		DbSelectArea("IC9")
		DbSkip()
	End
	DbSelectArea("IC6")
	DbSkip()
End				

If File(cDirFlow+"WFICEPQ.htm")

	For nC := 1 To Len(aContacts)

		DbSelectArea("IC5")
		DbSetOrder(1)
		DbSeek(xFilial("IC5")+ICC->ICC_CODPQ)
	
		//�������������������������������������������������������������Ŀ
		//| Repete os procedimentos para cada destinatario.				�
		//���������������������������������������������������������������
		//�������������������������������������������������������������Ŀ
		//| Inicializacao da classe de processos do workflow.			�
		//���������������������������������������������������������������
		oProcess := TWFProcess():New( "ICERESP", cSubject + " #" + IC5->IC5_CODPQ )

		//�������������������������������������������������������������Ŀ
		//| Seleciono o html que sera utilizado no corpo da mensagem.   |
		//���������������������������������������������������������������
		oProcess:NewTask( cSubject, cDirFlow+"WFICERESP.htm" )

		//�������������������������������������������������������������Ŀ
		//| Identificacao da Pesquisa									|
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "PQ_CODPQ", IC5->IC5_CODPQ )
		oProcess:ohtml:ValByName( "PQ_DESC" , IC5->IC5_DESC )
		oProcess:ohtml:ValByName( "PQ_DTDE" , IC5->IC5_DTDE )
		oProcess:ohtml:ValByName( "PQ_DTATE", IC5->IC5_DTATE )

		//�������������������������������������������������������������Ŀ
		//| Nome do Gestor de Risco									    |
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "ID_GESTOR", aContacts[nC][1])	// Nome do Gestor de Risco

		//�������������������������������������������������������������Ŀ
		//| Nome do Usuario Participante							    |
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "ID_NOME", cUser )			// Usuario

		oProcess:ohtml:ValByName( "PQ_CODPQ",  IC5->IC5_CODPQ)	// Pesquisa
		oProcess:ohtml:ValByName( "ID_DATFIM", ICC->ICC_DATFIM)	// Data de Finalizacao
		oProcess:ohtml:ValByName( "ID_HORFIM", ICC->ICC_HORFIM)	// Hora de Finalizacao

		//�������������������������������������������������������������Ŀ
		//| Identificacao da Perguntas									|
		//���������������������������������������������������������������
		For nI := 1 To Len(aAsk)
			AAdd(oProcess:ohtml:ValByName( "A.ASK" ),  aAsk[nI][1] )
	    Next nI
    
		//�������������������������������������������������������������Ŀ
		//| Riscos									|
		//���������������������������������������������������������������
		For nI := 1 To Len(aRisco)
			AAdd(oProcess:ohtml:ValByName( "B.RISK" ),  aRisco[nI][1] )
			AAdd(oProcess:ohtml:ValByName( "B.DESC" ),  aRisco[nI][2] )
	    Next nI

		oProcess:cSubject := cSubject	// Assunto

		//�������������������������������������������������������������Ŀ
		//| Inicializa a geracao do processo e envio da mensagem.		|
		//���������������������������������������������������������������
		//oP:ohtml:ValByName( "BEGIN_TEXT", aContacts[nC,1] )
		oProcess:cTo := aContacts[nC,1] + "<" + aContacts[nC,2] + ">"
		oProcess:Start()
	Next nC
Else
	Help("",1,"NOHTM")
EndIf
	
RestArea(aArea)
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICEWF120    | Autor � Lucas              � Data � 05.10.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Enviar email ao Gestor de Risco informando que Usuario     ���
���          � participante do Comite respondeu o Questionario/Pesquisa.  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICEWF120()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpL1 := Expressao Logica...                               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � ICEA120/SIGAICE                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICEWF200()
Local aContacts := {}
Local cUser := ""
Local cSubject := STR0006+"  -  "+IC5->IC5_DESC 
Local nC 
Local lTodos := .F.
Local cQuery := ""
Local aArea := GetArea()
Local aAsk := {}
Local aRisco := {}
Local nI := 0
Local cDirFlow    := GetNewPar("MV_QDIRFLW","\MP8\WORKFLOW\")
//Local cStartPath := GetSrvProfString("Startpath","")

cUser := ICEXUser(__cUserId,4)

If ICC->ICC_ENTIDA == "IC2"
	DbSelectArea("ICE")
	DbSetOrder(1)
	If DbSeek(xFilial("ICE")+ICC->ICC_CODPQ)
	   While ! Eof() .and. ICE_FILIAL == xFilial("ICE") .and. ICE_CODPQ == ICC->ICC_CODPQ
	   		If AllTrim(ICE_CAMPO) == "TODOS(*)"
	   			lTodos := .T.
	   		Else
	   			cQuery := ""
	   		Endif
	   		DbSkip()
	   	End
		If lTodos
			DbSelectArea("IC2")
			DbSetOrder(1)
			If DbSeek(xFilial("IC2"))
				cComite := IC2_COMITE
			EndIf
		EndIf
	EndIf
Else 
	//
EndIf				
			   					

//�������������������������������������������������������������Ŀ
//� Pegar email do CEO e CFO ... 					            �
//���������������������������������������������������������������	   			
/*/
DbSelectArea("IC2")
DbSetOrder(1)
If DbSeek(xFilial("IC2")+cComite)
   While !Eof() .and. IC2_FILIAL == xFilial("IC2") .and. IC2_COMITE == cComite

		QAA->(DbSetOrder(1))
		QAA->(DbSeek(xFilial("QAA")+IC2->IC2_MAT))

   		If IC2_NIVEL == "1"
			AAdd( aContacts, { AllTrim(QAA->QAA_NOME), AllTrim( QAA->QAA_EMAIL ),"CEO" } )
  		ElseIf IC2_NIVEL == "2"
			AAdd( aContacts, { AllTrim(QAA->QAA_NOME), AllTrim( QAA->QAA_EMAIL ),"CFO" } )
        EndIf
        DbSkip()
   End     
EndIf
/*/

//�������������������������������������������������������������Ŀ
//� Verifica se existe Destinatarios...                         �
//���������������������������������������������������������������
If Len( aContacts ) == 0
	MsgAlert(STR0004)
	RestArea( aArea )
Endif

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
aAsk := {}
DbSelectArea("IC6")
DbSetOrder(1)
If DbSeek(xFilial("IC6")+IC5->IC5_CODPQ)
	While !Eof() .and. IC6_CODPQ == IC5->IC5_CODPQ
	     cCodPQ := IC6_CODPQ
         cCodQT := IC6_CODQT
		
		 DbSelectArea("IC9")
		 DbSetOrder(1)
		 If DbSeek(xFilial("IC9")+IC6->IC6_CODQT)
			While !Eof() .and. IC9_CODQT == IC6->IC6_CODQT
				AAdd(aAsk,{IC9->IC9_DESC})
				DbSkip()
			End
		EndIf
		DbSelectArea("IC6")		
		DbSkip()
	End
EndIf
		
aRisco := {}
AAdd(aRisco,{"1","Alteracao do limite de credito"})
AAdd(aRisco,{"2","Alteracao da data de vencimento do limite de credito"})
AAdd(aRisco,{"3","Cheques pre-datados nao foram considerados no limite de credito"})
AAdd(aRisco,{"4","Verificacao trimestral do limite de credito"})
AAdd(aRisco,{"5","Credito aprovado manualmente com titulos em atraso"})

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
If File(cDirFlow+"WFICEPQ.htm")
	
	For nC := 1 To Len(aContacts)

		DbSelectArea("IC5")
		DbSetOrder(1)
		DbSeek(xFilial("IC5")+ICH->ICH_CODPQ)
	
		//�������������������������������������������������������������Ŀ
		//| Repete os procedimentos para cada destinatario.				�
		//���������������������������������������������������������������
		//If aContacts[nC][3] == "CEO"
		//�������������������������������������������������������������Ŀ
		//| Inicializacao da classe de processos do workflow.			�
		//���������������������������������������������������������������
		oProcess := TWFProcess():New( "ICEAAVCEO", cSubject + " #" + IC5->IC5_CODPQ )

		//�������������������������������������������������������������Ŀ
		//| Seleciono o html que sera utilizado no corpo da mensagem.   |
		//���������������������������������������������������������������
		oProcess:NewTask( cSubject, cDirFlow+"WFICEARCEO.htm" )
    	//Else
		//�������������������������������������������������������������Ŀ
		//| Inicializacao da classe de processos do workflow.			�
		//���������������������������������������������������������������
	    //oProcess := TWFProcess():New( "ICEAAVCFO", cSubject + " #" + IC5->IC5_CODPQ )

		//�������������������������������������������������������������Ŀ
		//| Seleciono o html que sera utilizado no corpo da mensagem.   |
		//���������������������������������������������������������������
		//oProcess:NewTask( cSubject, cDirFlow+"WFICEARCEO.htm" )
    	//EndIf
    
		//�������������������������������������������������������������Ŀ
		//| Identificacao da Pesquisa									|
		//���������������������������������������������������������������
		oProcess:ohtml:ValByName( "PQ_CODPQ", IC5->IC5_CODPQ )
		oProcess:ohtml:ValByName( "PQ_DESC" , IC5->IC5_DESC )
		oProcess:ohtml:ValByName( "PQ_DTDE" , IC5->IC5_DTDE )
		oProcess:ohtml:ValByName( "PQ_DTATE", IC5->IC5_DTATE )

		//�������������������������������������������������������������Ŀ
		//| Nome do Gestor de Risco									    |
		//���������������������������������������������������������������
		//If aContacts[nC][3] == "CEO"
			oProcess:ohtml:ValByName( "ID_CEO", aContacts[nC][1])	// Nome do CEO
		//Else
		//	oProcess:ohtml:ValByName( "ID_CFO", aContacts[nC][1])	// Nome do CEO
		//EndIf	

		oProcess:ohtml:ValByName( "PQ_CODPQ",  IC5->IC5_CODPQ)	// Pesquisa
		oProcess:ohtml:ValByName( "ID_DATFIM", ICC->ICC_DATFIM)	// Data de Finalizacao
		oProcess:ohtml:ValByName( "ID_HORFIM", ICC->ICC_HORFIM)	// Hora de Finalizacao

		//�������������������������������������������������������������Ŀ
		//| Nome do Gestor    										    |
		//���������������������������������������������������������������
		//oProcess:ohtml:ValByName( "ID_GESTOR", cUser )			// Usuario
    	
		//�������������������������������������������������������������Ŀ
		//| Identificacao da Perguntas									|
		//���������������������������������������������������������������
		For nI := 1 To Len(aAsk)
			AAdd(oProcess:ohtml:ValByName( "A.ASK" ),  aAsk[nI][1] )
    	Next nI
    
		//�������������������������������������������������������������Ŀ
		//| Riscos									|
		//���������������������������������������������������������������
		For nI := 1 To Len(aRisco)
			AAdd(oProcess:ohtml:ValByName( "B.RISK" ),  aRisco[nI][1] )
			AAdd(oProcess:ohtml:ValByName( "B.DESC" ),  aRisco[nI][2] )
    	Next nI

		oProcess:cSubject := cSubject	// Assunto

		//�������������������������������������������������������������Ŀ
		//| Inicializa a geracao do processo e envio da mensagem.		|
		//���������������������������������������������������������������
		//oP:ohtml:ValByName( "BEGIN_TEXT", aContacts[nC,1] )
		oProcess:cTo := aContacts[nC,1] + "<" + aContacts[nC,2] + ">"
		oProcess:Start()
	Next nC
Else
	Help(" ",1,"NOHTM")
EndIf
RestArea(aArea)
Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICEWF200A   | Autor � Lucas              � Data � 05.10.04 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Enviar email ao Gestor de Risco informando que Usuario     ���
���          � participante do Comite respondeu o Questionario/Pesquisa.  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICEWF200A()                                                ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpL1 := Expressao Logica...                               ���
�������������������������������������������������������������������������Ĵ��
���Uso       � ICEA120/SIGAICE                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICEWF200A()
Local aContacts := {}
Local cSubject := "Audit Trail - Cadastro de Clientes" 
Local nC 
Local nI
Local aArea := GetArea()

AAdd( aContacts, { "JOSE LUCAS DA SILVA", "JLUCAS@MICROSIGA.COM.BR" } )

//�������������������������������������������������������������Ŀ
//� Verifica se existe Destinatarios...                         �
//���������������������������������������������������������������
If Len( aContacts ) == 0
	MsgAlert("Destinatario(s) nao cadastrado(s) para essa pesquisa.")
	RestArea( aArea )
Endif

//�������������������������������������������������������������Ŀ
//� Enviar emails aos Destinatarios...                          �
//���������������������������������������������������������������
aAlter := {}
AAdd(aAlter,{"02/11/04","005025","002451","Limite de Credito",10000.00,20000.00})
AAdd(aAlter,{"05/11/04","001288","003000","Limite de Credito", 5000.00, 7000.00})
AAdd(aAlter,{"06/11/04","001522","000025","Limite de Credito", 3000.00, 4000.00})
AAdd(aAlter,{"12/11/04","005025","002451","Limite de Credito",20000.00,12000.00})

For nC := 1 To Len(aContacts)

	//�������������������������������������������������������������Ŀ
	//| Inicializacao da classe de processos do workflow.			�
	//���������������������������������������������������������������
	oProcess := TWFProcess():New( "AUDITRAIL", cSubject + " #" )

	//�������������������������������������������������������������Ŀ
	//| Seleciono o html que sera utilizado no corpo da mensagem.   |
	//���������������������������������������������������������������
	oProcess:NewTask( cSubject, "\workflow\wfaudit.htm" )
  
	//�������������������������������������������������������������Ŀ
	//| Identificacao da Pesquisa									|
	//���������������������������������������������������������������
	For nI := 1 To Len(aAlter)
		AAdd(oProcess:ohtml:ValByName( "A.DATA" ),  aAlter[nI][1] )
		AAdd(oProcess:ohtml:ValByName( "A.AUTOR" ) , aAlter[nI][2] )
		AAdd(oProcess:ohtml:ValByName( "A.CODIGO" ), aAlter[nI][3] )
		AAdd(oProcess:ohtml:ValByName( "A.CAMPO" ), aAlter[nI][4] )
		AAdd(oProcess:ohtml:ValByName( "A.OLD" ), Transform(aAlter[nI][5],"@E 9,999,999.99") )
		AAdd(oProcess:ohtml:ValByName( "A.NEW" ), Transform(aAlter[nI][6],"@E 9,999,999.99") )
    Next nI
    
	oProcess:cSubject := cSubject	// Assunto

	//�������������������������������������������������������������Ŀ
	//| Inicializa a geracao do processo e envio da mensagem.		|
	//���������������������������������������������������������������
	//oP:ohtml:ValByName( "BEGIN_TEXT", aContacts[nC,1] )
	oProcess:cTo := aContacts[nC,1] + "<" + aContacts[nC,2] + ">"
	oProcess:Start()
Next nC

RestArea(aArea)
Return
