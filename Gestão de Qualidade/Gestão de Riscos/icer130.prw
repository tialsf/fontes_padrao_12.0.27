#INCLUDE "ICER130.CH"
#INCLUDE "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICER130  � Autor � Lucas				    � Data � 15.04.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Relatorio de Abstinencia                                   ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICER130(lBrow,nChart)                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpL1 = Chamada da mBrowse                                 ���
���          � ExpN1 = Imprime ou nao o grafico                           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Function ICER130(cCodPQ)
Local oPrint
Local lPergunte := .F.
Local cStartPath 	:= GetSrvProfString("Startpath","")
Local cJPEG := " "

Private cFileLogo  	:= "LGRL"+SM0->M0_CODIGO+FWCodFil()+".BMP" // Empresa+Filial
cCodPQ := If(PCount()>0,cCodPQ,"")

//�����������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros																		   					�
//� mv_par01				// Da Pesquisa																								�
//� mv_par02				// Participantes ?  Todos Abstinentes										 						�
//� mv_par03				// Imprime ? Impressora ou Tela														       			�
//�������������������������������������������������������������������
//�������������������������������������������������������������������
	
lPergunte := Pergunte("ICR130",.T.)

oPrint	:= TMSPrinter():New(STR0001)					 //"Relatorio de Abstencao"

If ! oPrint:Setup() .or. ! oPrint:IsPrinterActive()
	Return
EndIf	

cCodPQ := ALLTRIM(mv_par01)
If Empty(cJPEG)
	MsgRun(STR0002,"",{|| CursorWait(), ICR130Imp(oPrint,cCodPQ) ,CursorArrow()}) //"Gerando Visualizacao, Aguarde..."
Else
	ICR130Imp(oPrint,cCodPQ)
Endif

If ( lPergunte )
	If mv_par03 == 2
        cJPEG := "ICER130"
		If !Empty(cJPEG)
			oPrint:SaveAllAsJPEG(cStartPath+cJPEG,870,840,140)
		EndIF
		oPrint:Preview() 
	Else
		oPrint:Print()
	EndIf
EndIf

Return Nil

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICR130IMP  � Autor � Lucas				� Data � 15.04.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Relatorio de Abstinencia                                   ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICR130Imp(ExpO1)                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto o Print                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICR130Imp(oPrint,cCodPQ)

Local aArea	:= GetArea()
Local nPag 	:= 1
Local nLin	 	:= 0
Local cFileLogo  := "LGRL"+SM0->M0_CODIGO+FWCodFil()+".BMP" // Empresa+Filial
Local aCampos := {}
Local cArqTrab := ""
Local nPosQuest := 0
Local nPosProcess := 0
Local aPQ := {}
Local aQuest := {}
Local aProcess := {}
Local aSN := { STR0008, STR0009 }

Private oFont16, oFont08, oFont10, oFont07, oFont06

//��������������������������������������������������������������Ŀ
//� Montar arquivo de Trabalho para Consumo RealxStandard.       �
//����������������������������������������������������������������
AADD(aCampos,{ "TRB_FILIAL"     ,"C", FWSizeFilial(), 0 })
AADD(aCampos,{ "TRB_CODPQ"  ,"C", 06, 0 })
AADD(aCampos,{ "TRB_CODQT"   ,"C", 06, 0 })
AADD(aCampos,{ "TRB_PROCES","C", 06, 0 })
AADD(aCampos,{ "TRB_MAT",       "C", 06, 0 })
AADD(aCampos,{ "TRB_NOME"    ,"C", 30, 0 })
AADD(aCampos,{ "TRB_RESP",     "C", 01, 0 })

oTempTable := FWTemporaryTable():New( "TRB" )
oTempTable:SetFields( aCampos )
oTempTable:AddIndex("indice1", {"TRB_FILIAL","TRB_CODPQ","TRB_MAT","TRB_CODQT","TRB_PROCES"} )
oTempTable:Create()

//���������������������������������������������������������������Ŀ
//�  Gerar arquivo de trabalho                             �
//�����������������������������������������������������������������
ICR130Trb(oPrint,cCodPQ)

If !File(cFileLogo)
	cFileLogo := "LGRL" + SM0->M0_CODIGO+".BMP" // Empresa
Endif

If nLin > 1810				// Espaco minimo para colocacao do rodape	
	nPag++
	oPrint:EndPage() 		// Finaliza a pagina
	oPrint:StartPage() 		// Inicia uma nova pagina		
	oPrint:SayBitmap(05,0005, cFileLogo,328,82)             // Tem que estar abaixo do RootPath
	oPrint:SayBitmap(05,2100, "Logo.bmp",237,58) 
	nLin := 150
Endif

//���������������������������������������������������������������Ŀ
//� Percorrer arquivo de Pesquisas...                             �
//�����������������������������������������������������������������
DbSelectArea("TRB")
DbSetOrder(1)
DbSeek(xFilial("IC5"))				//FILIAL+CODPQ+CODQT+PROCES+MAT

While !Eof() .and. TRB_FILIAL == xFilial("IC5")

	cCodPQ := TRB->TRB_CODPQ
    aQuest  := {}
    aProcess := {} 
    
    IC5->(DbSetOrder(1))
    IC5->(DbSeek(xFilial("IC5")+TRB->TRB_CODPQ))
    
    aPQ := { TRB->TRB_CODPQ,IC5->IC5_DESC }
    
	oPrint:StartPage() 		// Inicia uma nova pagina

	oFont16	:= TFont():New("Arial",16,16,,.F.,,,,.T.,.F.)
	oFont08	:= TFont():New("Arial",08,08,,.F.,,,,.T.,.F.)
	oFont10	:= TFont():New("Arial",10,10,,.F.,,,,.T.,.F.)
	oFont07	:= TFont():New("Arial",07,07,,.F.,,,,.T.,.F.)
	oFont06	:= TFont():New("Arial",06,06,,.F.,,,,.T.,.F.)

	nLin := ICR130Cab(oPrint,nPag)				// Funcao que monta o cabecalho
	
	//���������������������������������������������������������������Ŀ
	//� Quebrar por Pesquisa																				                                  �
	//�����������������������������������������������������������������
	While !Eof() .and. TRB_FILIAL == xFilial("IC5") .and. TRB_CODPQ == cCodPQ
	
			If nLin > 2810		
				nPag++
				oPrint:EndPage() 		// Finaliza a pagina
				oPrint:StartPage() 		// Inicia uma nova pagina		
				oPrint:SayBitmap(05,0005, cFileLogo,328,82)             // Tem que estar abaixo do RootPath
				oPrint:SayBitmap(05,2100, "Logo.bmp",237,58) 
			
				nLin := ICR130Cab(oPrint,nPag)				// Funcao que monta o cabecalho
			Endif

			If mv_par02 == 1 .OR. (mv_par02 == 2 .AND. TRB->TRB_RESP == "2")

				nLin += 80	      
 
				oPrint:Box( nLin-20, 30, nLin+60, 2350 )//geral

				// Construcao da grade
				oPrint:Line( nLin-20, 0220, nLin+60, 0220 )   	// vertical
				oPrint:Line( nLin-20, 0420, nLin+60, 0420 )   	// vertical
				oPrint:Line( nLin-20, 0660, nLin+60, 0660 )   	// vertical
				oPrint:Line( nLin-20, 1540, nLin+60, 1540 )   	// vertical
				oPrint:Line( nLin-20, 1690, nLin+60, 1690 )   	// vertical
				oPrint:Line( nLin-20, 1830, nLin+60, 1830 )   	// vertical

				oPrint:Say(nLin,0080,TRB->TRB_CODPQ,oFont08)
				oPrint:Say(nLin,0260,TRB->TRB_CODQT,oFont08)
				oPrint:Say(nLin,0460,TRB->TRB_PROCES,oFont08)
				oPrint:Say(nLin,0700,TRB->TRB_MAT+" - "+TRB->TRB_NOME,oFont08)

		        If !Empty(TRB->TRB_RESP)
		        	If TRB->TRB_RESP == "1"
		        		oPrint:Say(nLin,1605,"X",oFont08)
		        	Else
		        		oPrint:Say(nLin,1750,"X",oFont08)       	
		        	EndIf	
				EndIf 
				
				//���������������������������������������������������������������Ŀ
				//� Preparar dados para a Legenda																                                  �
				//�����������������������������������������������������������������
				IC6->(DbSetOrder(1))
				IC6->(DbSeek(xFilial("IC6")+TRB->TRB_CODPQ+TRB->TRB_CODQT))
				
				nPosQuest := Ascan(aQuest,{ |x| x[1] == TRB->TRB_CODQT })
				If nPosQuest == 0
					AADD(aQuest,{TRB->TRB_CODQT,IC6->IC6_DESC})
				EndIf		

				IC4->(DbSetOrder(1))
				IC4->(DbSeek(xFilial("IC4")+TRB->TRB_CODQT+TRB->TRB_PROCES))
				
				nPosProcess := Ascan(aProcess,{ |x| x[1] == TRB->TRB_PROCES })
				If nPosProcess == 0
					AADD(aProcess,{TRB->TRB_PROCES,IC4->IC4_DESCRI})			
	            EndIf    
	        EndIf    
			DbSelectArea("TRB")
			DbSkip()
	End
	ICR130Legend(oPrint,aPQ,aQuest,aProcess,nLin,nPag)
	
	oPrint:EndPage() 		// Finaliza a pagina
End						

oTempTable:Delete()
RestArea(aArea)

Return
                                
/*/
��������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICR130Cab� Autor � Lucas                 � 15.06.01        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Cabecalho do relatorio                                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Cabecalho(ExpO1,ExpN1)                                     ���
�������������������������������������������������������������������������Ĵ��
���Parametros�		 ExpO1 = Objeto oPrint                                      ���
���          � 			ExpN1 = Contador de paginas                                ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � ICER130                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICR130Cab(oPrint,nPag)
Local nLin := 160

If !File(cFileLogo)
	cFileLogo := "LGRL" + SM0->M0_CODIGO+".BMP" // Empresa
Endif

oPrint:StartPage() 		// Inicia uma nova pagina

oPrint:SayBitmap(05,0005, cFileLogo,328,82)             // Tem que estar abaixo do RootPath
oPrint:SayBitmap(05,2100, "Logo.bmp",237,58) 

oPrint:Say(075,850,STR0001,oFont16 ) 						//"RELATORIO DE ABSTENCOES

//Box Cabecalho
oPrint:Box( 150, 30, 220, 2350 )

oPrint:Line( 150, 0220, 220, 0220 )   	// vertical
oPrint:Line( 150, 0420, 220, 0420 )   	// vertical
oPrint:Line( 150, 0660, 220, 0660 )   	// vertical
oPrint:Line( 150, 1540, 220, 1540 )   	// vertical
oPrint:Line( 190, 1690, 220, 1690 )   	// vertical
oPrint:Line( 150, 1830, 220, 1830 )   	// vertical

// Descricao do Cabecalho

// 1a Linha
oPrint:Say(160,0050,STR0003,oFont08 ) 							//"Pesquisa"
oPrint:Say(160,0245,STR0004,oFont08 ) 							//"Processo"
oPrint:Say(160,0428,STR0005,oFont08 ) 							//"Sub-Processo"
oPrint:Say(160,0700,STR0006,oFont08 ) 							//"Participante"
oPrint:Say(158,1600,STR0007,oFont08 ) 							//"Respondeu?
oPrint:Say(160,1840,STR0011,oFont08 ) 							//"OBS:"

// Construcao das linhas do cabecalho
oPrint:Line( 185, 1545, 187, 1830 )   	// horizontal da respondeu
// 2a Linha
oPrint:Say(190,1590,STR0008,oFont08 ) 							//"Sim
oPrint:Say(190,1732,STR0009,oFont08 ) 							//"Nao


Return(nLin)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �ICR130Legend� Autor� Lucas		     	� Data � 15.04.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Legenda de pagina do relatorio de Abstinencia              ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICR130Legend(ExpO1,ExpA1,ExpA2,ExpA3,ExpN1)                ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto o Print                                     ���
��� 		 � ExpA1 = Array com dados da Pesquisa                        ���
��� 		 � ExpA2 = Array com dados do Questionario                    ���
��� 		 � ExpA3 = Array com dados do Processo                        ���
��� 		 � ExpN1 = Contador de Linhas nLin                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICR130Legend(oPrint,aPQ,aQuest,aProcess,nLin,nPag)
Local nI := 0

AjustaPag(oPrint,@nLin,nPag)	

//Box Legenda
nLin += 80
AjustaPag(oPrint,@nLin,nPag)	
oPrint:Box(nLin, 30, nLin+80, 1350 )
AjustaPag(oPrint,@nLin,nPag)	
oPrint:Say(nLin+20,600,STR0012,oFont08 )//"Legenda"
nLin += 80

AjustaPag(oPrint,@nLin,nPag)	
oPrint:Box(nLin, 30, nLin+80, 1350 )
AjustaPag(oPrint,@nLin,nPag)	
oPrint:Say(nLin,50,STR0003,oFont08 )//"Pesquisa

AjustaPag(oPrint,@nLin,nPag)	
oPrint:Line(nLin, 290, nLin+80, 290 )// vertical
oPrint:Say(nLin,330,aPQ[1]+" - "+aPQ[2],oFont08 )
AjustaPag(oPrint,@nLin,nPag)	

For nI := 1 To Len(aQuest)
	nLin += 80
	AjustaPag(oPrint,@nLin,nPag)	
	oPrint:Box(nLin, 30, nLin+80, 1350 )
	If nI == 1
		AjustaPag(oPrint,@nLin,nPag)	
		oPrint:Say(nLin,050,STR0013,oFont08)//Questionario
	EndIf	
	AjustaPag(oPrint,@nLin,nPag)	
	oPrint:Say(nLin,330,aQuest[nI][1]+" - "+aQuest[nI][2],oFont08 )
	oPrint:Line(nLin, 290, nLin+80, 290 )// vertical
Next nI

For nI := 1 To Len(aProcess)
	nLin += 80
	oPrint:Box(nLin, 30, nLin+80, 1350 )
	If nI == 1
		AjustaPag(oPrint,@nLin,nPag)	
		oPrint:Say(nLin,050,STR0005,oFont08)//Processo
	EndIf	
	AjustaPag(oPrint,@nLin,nPag)	
	oPrint:Say(nLin,330,aProcess[nI][1]+" - "+aProcess[nI][2],oFont08 )
	oPrint:Line( nLin, 290, nLin+80, 290 )// vertical
Next nI

nLin += 80

Return 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICR130Foot  � Autor � Lucas				� Data � 15.04.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Rodape de pagina do relatorio de Abstinencia               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICR130Trb(ExpO1)                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto o Print                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICR130Foot(oPrint,nPag,nLin)
Return 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � ICR130TRB  � Autor � Lucas				� Data � 15.04.05 ���
�������������������������������������������������������������������������Ĵ��
���Descricao � Relatorio de Abstinencia                                   ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � ICR130Trb(ExpO1)                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpO1 = Objeto o Print                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function ICR130Trb(oPrint)
Local aArea := GetArea()
Local cCodPQ := ""
Local cCodQT := ""
Local nK := 0
Local aUsersPesq := {}
  
//���������������������������������������������������������������Ŀ
//� Percorrer arquivo de Pesquisas...                             �
//�����������������������������������������������������������������
DbSelectArea("IC5")
DbSetOrder(1)
DbSeek(xFilial("IC5")+mv_par01)	//FILIAL+CODPQ+MAT

While !Eof() .and. IC5_FILIAL == xFilial("IC5") .and. IC5_CODPQ == mv_par01

	cCodPQ := IC5->IC5_CODPQ
	
	//���������������������������������������������������������������Ŀ
	//� Percorrer Questionarios...                                    �
	//�����������������������������������������������������������������
    DbSelectArea("IC6")
    DbSetOrder(1)
    DbSeek(xFilial("IC6")+cCodPQ)
	While !Eof() .and. IC6_FILIAL == xFilial("IC6") .and. IC6_CODPQ == cCodPQ
	
				cCodQT := IC6->IC6_CODQT

				//���������������������������������������������������������������Ŀ
				//� Percorrer Processos...                                    �
				//�����������������������������������������������������������������
    			DbSelectArea("IC4")
   				DbSetOrder(1)
			    DbSeek(xFilial("IC4")+cCodQT)
				While !Eof() .AND. IC4_FILIAL == xFilial("IC4")  .AND.	IC4_CODQT == cCodQT                                      

						//��������������������������������������������������������������Ŀ
						//� Pegar todos os destinatarios executaroes do Pesquisa           													
						//����������������������������������������������������������������
						If IC5->IC5_MIDIA == "2" 
							aUsersPesq := ICEXDestPQ(IC5->IC5_CODPQ)
						Else
							aUsersPesq := ICEXDestICC(IC5->IC5_CODPQ)
						EndIf

						For nK := 1 To Len(aUsersPesq) 
						
								QAA->(DbSetOrder(1))
								QAA->(DbSeek(xFilial("QAA")+aUsersPesq[nK][1]))
								
								//��������������������������������������������������������������Ŀ
								//� Verifica se ha algum usuario/participante que nao respondeu a pesquisa...                                �
								//����������������������������������������������������������������
							    DbSelectArea("ICC")
							    DbSetOrder(2)
							    If DbSeek(xFilial("ICC")+cCodPQ+aUsersPesq[nK][1])
							       DbSelectArea("ICD")
								   DbSetOrder(1)
								   If ! DbSeek(xFilial("ICD")+ICC->ICC_RESPPQ)
								   	   DbSelectArea("TRB")	
								   	   RecLock("TRB",.T.)
								   	   Replace	TRB_FILIAL		With xFilial("IC5")
								   	   Replace TRB_CODPQ		With IC5->IC5_CODPQ
								   	   Replace TRB_CODQT		With IC6->IC6_CODQT
   								   	   Replace TRB_PROCES	With IC4->IC4_PROCES
   								   	   Replace TRB_MAT			With aUsersPesq[nK][1]
   								   	   Replace TRB_NOME		With QAA->QAA_NOME
   								   	   Replace TRB_RESP   		With "2"
   								   	   MsUnLock()
                                   Else
								   	   DbSelectArea("TRB")	
								   	   RecLock("TRB",.T.)
								   	   Replace	TRB_FILIAL		With xFilial("IC5")
								   	   Replace TRB_CODPQ		With IC5->IC5_CODPQ
								   	   Replace TRB_CODQT		With IC6->IC6_CODQT
   								   	   Replace TRB_PROCES	With IC4->IC4_PROCES
   								   	   Replace TRB_MAT			With aUsersPesq[nK][1]
   								   	   Replace TRB_NOME		With QAA->QAA_NOME
   								   	   Replace TRB_RESP   		With "1"
   								   	   MsUnLock()
                                  EndIf
                                EndIf
						Next nK

                        DbSelectArea("IC4")
                        DbSkip()
	    		End     
      			DbSelectArea("IC6")
   				DbSkip()
		End     
		DbSelectArea("IC5")
		DbSkip()
End     
RestArea(aArea)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaPag � Autor �Leandro Sabino		 � Data � 28/08/2007  ���
�������������������������������������������������������������������������͹��
���Desc.     �Controla o salto de pagina								  ���
�������������������������������������������������������������������������͹��
���Uso       � ICER130                                                    ���                   
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaPag(oPrint,nLin,nPag)

If nLin > 2810		
	nPag++
	oPrint:EndPage()  // Finaliza a pagina
	oPrint:StartPage()// Inicia uma nova pagina		
	oPrint:SayBitmap(05,0005, cFileLogo,328,82) // Tem que estar abaixo do RootPath
	oPrint:SayBitmap(05,2100, "Logo.bmp",237,58) 
	nLin := 150
Endif

Return
