#INCLUDE "TOTVS.CH"
#INCLUDE "ICEXFIL.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "APVISIO.CH"

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矷CEXFIL  	 矨utor  � Lucas           	    � Data � 10/05/05 潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Carregar dados nas tabelas conforme gestao SOX. ..         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � SIGAICE (ICEA110,ICEA200)                                  潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILICG   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na tabela de riscos-ICG  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilICG()			     								  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilICG()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aRisco := {} 
Local aDescri := {}
Local aConseq := {}
Local aFato := {}

If lCria .and. IC5->IC5_GESTAO == "2"	//Sarbanes-Oxley

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Risco de Mercado/Financeiro...                             �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aRisco   ,{"000001",STR0009,"01"})     //"RISCO DE TAXAS DE JUROS"
	
	AADD(aDescri,  {"000001",STR0010})  //"Este pode ser definido como o risco de perda no valor economico de uma carteira, decorrente dos efeitos de mudancas adversas das taxas de juros."
	AADD(aFato     ,{"000001",STR0011+STR0012+STR0013}) //"1) Eventual perda do valor de mercado de t韙ulos publicos (BBCs,Brady bonds,etc) ou privado (corporate eurobonds,etc)"###"2) Encarecimento do custo de funding"###"3) Queda da taxa de reinvestimento"
	AADD(aConseq,{"000001",STR0014}) //"Queda da rentabilidade financeira"

	AADD(aRisco   ,{"000002",STR0015,"01"})  //"RISCO DE TAXAS DE CAMBIO"
	AADD(aDescrI,  {"000002",STR0016})  //"Este pode ser definido como o risco de perdas devido a mudancas adversas nas taxas de cambio."
	AADD(aFato     ,{"000002",STR0017+STR0018})//"1) Variacao nos precos de NTN-Ds,NBC-Es,NBC-Fs de ativos internacionais negociados em moeda estrangeira devido a apreciacao/depreciacao relativa de moedas."###"2) Descasamentos em um carteira indexada a alguma moeda estrangeira"
	AADD(aConseq,{"000002",STR0014})  //"Queda da rentabilidade financeira"

	AADD(aRisco   ,{"000003",STR0019,"01"})       //"RISCO DE COMMODITIES"
	AADD(aDescri  ,{"000003",STR0020})         //"Este pode ser definido como o risco de perdas devido a mudancas no valor de mercado de carteiras de commodities."
	AADD(aFato     ,{"000003",STR0021})  //"1) Variacao nos precos de carteiras constituidas por ouro, prata, platina, soja, boi gordo, cacau, etc"
	AADD(aConseq,{"000003",STR0014})     //"Queda da rentabilidade financeira"

	AADD(aRisco   ,{"000004",STR0022,"01"})       //"RISCO DE ACOES"
	AADD(aDescri,  {"000004",STR0023})  //"Este pode ser definido como o risco de perdas devido a mudancas no valor de mercado de acoes"
	AADD(aFato     ,{"000004",STR0024})    //"1) Variacao nos precos de carteiras constituidas por acoes como Petrobras PN, Vale PN, Eletrobras PNB ADRs de Usiminas PN,etc."
	AADD(aConseq,{"000004",STR0025})       //"Queda da rentabilidade e liquidez financeira"

	AADD(aRisco   ,{"000005",STR0026,"01"})  //"RISCO DE LIQUIDEZ"
	AADD(aDescri,  {"000005",STR0027})  //"Este pode ser definido como o risco de perdas devido a incapacidade de se desfazer rapidamente uma posicao ou obter 'Funding', devido a condicoes de mercado."

	AADD(aFato,    { "000005",STR0028+STR0029+STR0030})  //"1) Carteiras de Eurobonds brasileiros, acoes de segunda e terceira linhas, alguns contratos futuros negociados na BM&F, etc."###" 2) Situacoes onde nao e possivel 'rolar' dividas nos mercados financeiros" ###"3) Ajustes de margens que venham a consumir a liquidez da insitituicao"

	AADD(aConseq,{"000005",STR0025})    //"Queda da rentabilidade e liquidez financeira"

	AADD(aRisco   ,{"000006",STR0031,"01"})  //"RISCO DE DERIVATIVOS"
	AADD(aDescri  ,{"000006",STR0032})  //"Este pode ser definido como o risco de perdas devido ao uso de derivativos (seja para especulacao, seja por hedge)"
	AADD(aFato     ,{"000006",STR0033})  //"1) Variacoes no valor de posicoes de contratos de swaps, futuros, a termo, opcoes, etc..."
	AADD(aConseq,{"000006",STR0025}) //"Queda da rentabilidade e liquidez financeira"
	
	AADD(aRisco   ,{"000007",STR0034,"01"})  //"RISCO DE HEDGE"
	AADD(aDescri  ,{"000007",STR0035})  //"Este pode ser definido como o risco de perdas devido ao uso inapropriado de instrumentos para hedge."
	AADD(aFato     ,{"000007",STR0028+STR0029+STR0030})  //"1) Carteiras de Eurobonds brasileiros, acoes de segunda e terceira linhas, alguns contratos futuros negociados na BM&F, etc."###" 2) Situacoes onde nao e possivel 'rolar' dividas nos mercados financeiros" ###"3) Ajustes de margens que venham a consumir a liquidez da insitituicao"
	AADD(aConseq,{"000007",STR0025})//"Queda da rentabilidade e liquidez financeira"

	AADD(aRisco   ,{"000008",STR0036,"01"})    //"RISCO DE CONCENTRACAO"
	AADD(aDescri  ,{"000008",STR0037})    //"Este pode ser definido como o risco de perdas devido a nao diversificacao do risco mercado de carteiras de investimentos."
	AADD(aFato     ,{"000008",STR0038})  //"1) Investimentos excessivamente concentrados em poucos indexadores, moedas, ativos, vencimentos, etc."
	AADD(aConseq,{"000008",STR0025})//"Queda da rentabilidade e liquidez financeira"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Risco de Credito...    					                                                              .                       �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aRisco   ,{"000009",STR0039,"02"})  //"RISCO DE INADIMPLENCIA"
	
	AADD(aDescri,  {"000009",STR0040})  //"Este pode ser definido como o risco de perda pela incapacidade de pagamento do tomador de um emprestimo, contraparte de um contrato ou emissor de um titulo."
	AADD(aFato     ,{"000009",STR0041+ STR0042}) //"1) Nao pagamento de juros e/ou principal de credito pessoal, emprestimo para pessoa juridica, cartao de credito, leasing, etc."###" 2) Nao pagamento de juros e/ou principal de titulos de renda fixa (nacionais/internacionais publicos/privados) pelo emissor"
	AADD(aConseq,{"000009",STR0043})  //"Evasao na disponibilidade de receitas no fluxo de caixa"

	AADD(aRisco   ,{"000010",STR0044,"02"})  //"RISCO DE DEGRADACAO DO CREDITO"
	
	AADD(aDescri,  {"000010",STR0045})  //"Este pode ser definido como o risco de perda pela degradacao da qualidade crediticia do tomador de um emprestimo, contraparte de uma transacao ou emissor de um titulo levando a uma diminuicao no valor das obrigacoes."
	AADD(aFato     ,{"000010",STR0046})  //"1) Perdas em titulos soberanos e/ou corporativos pela reducao do rating do pais emissor."
	AADD(aConseq,{"000010",STR0047})     //"Diminuicao no valor das obrigacoes"

	AADD(aRisco   ,{"000011",STR0048,"02"})  //"RISCO DE DEGRADACAO DAS GARANTIAS"
	AADD(aDescri,  {"000011",STR0049}) //"Este pode ser definido como o risco de perda pela degradacao da qualidade das garantias oferecidas por um tomador de um emprestimo, contraparte de uma transacao ou emissor de um titulo."
	AADD(aFato    ,{"000011",STR0050})  //"1) Emprestimos cujas garantias nao mais existam;  2) Depreciacao no valor das garantias depositadas em bolsas de derivativos"
	AADD(aConseq,{"000011",STR0047})  //"Diminuicao no valor das obrigacoes"

	AADD(aRisco   ,{"000012",STR0051,"02"})  //"RISCO SOBERANO"
	AADD(aDescri,  {"000012",STR0052})  //"Este pode ser definido como o risco de perda pela incapacidade de pagamento do tomador de um emprestimo, contraparte de um contrato ou emissor de um titulo, em honrar seus compromissos em funcao de restricoes impostas por seu pais sede."
	AADD(aFato     ,{"000012",STR0053})  //"1) Transacoes que envolvam transferencias internacionais de titulos ou de cambio."
	AADD(aConseq, {"000012",STR0054})  //"Perda das obrigacoes e direitos"

	AADD(aRisco   ,{"000013",STR0055,"02"})  //"RISCO DE FINANCIADOR"
	AADD(aDescri,  {"000013",STR0056})  //"Este pode ser definido como o risco de perdas por inadimplencia do financiador de uma transacao, potencializada quando o contrato nao contempla acordo de liquidacao por compensacao de direitos e obrigacoes (netting agreement)."
	AADD(aFato     ,{"000013",STR0057})   //"1) Repurchase transactions que nao contemplam o netting de direitos e obrigacoes ao contraios dos (ISMA Agreements)."
	AADD(aConseq, {"000013",STR0054})  //"Perda das obrigacoes e direitos"

	AADD(aRisco   ,{"000014",STR0058,"02"})  //"RISCO DE CONCENTRACAO DE CREDITO"
	AADD(aDescri,  {"000014",STR0059})       //"Este pode ser definido como o risco de perdas em decorrencia da nao diversificacao de risco de credito de investimento"
	AADD(aFato     ,{"000014",STR0060})  //"1) Concentrar emprestimos em poucos setores da economia, classes de ativos, etc.; 2) Possuir parte substancial dos passivos de um devedor"
	AADD(aConseq, {"000014",STR0061})  //"Fuga de investidores para outras instituicoes"


	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Risco de Operacional...      			                                                              .                       �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aRisco   ,{"000015",STR0062,"03"})       //"RISCO DE OVERLOAD"
	AADD(aDescri,  {"000015",STR0063})      //"Este pode ser definido como o risco de perda por sobrecargas nos sistemas eletrico, telefonico, de processamento de dados, etc."
	AADD(aFato     ,{"000015",STR0064})  //"1) Sistemas nao operacionais em agencias bancarias, por acumulo de informacao nos canais de comunicacao com a central de atendimento;2) Linhas telefonicas constantemente ocupadas."
	AADD(aConseq,{"000015",STR0065})       // "Comprometimento na qualidade dos servicos prestados"

	AADD(aRisco   ,{"000016",STR0066,"03"})     //"RISCO DE OBSOLESCENCIA"
	AADD(aDescri,  {"000016",STR0067})
	AADD(aFato     ,{"000016",STR0068})  //"1) Versoes de softwares nao compativeis com hardware antigo;2) Impossibilidade de integrar sistemas computacionais desenvolvidos em plataformas e versoes de softwares diferentes."
	AADD(aConseq,{"000016",STR0069})   //"Comprometimento na producao e ou qualidade dos servicos prestados"

	AADD(aRisco   ,{"000017",STR0070,"03"})   //"RISCO DE PRESTEZA E CONFIABILIDADE"
	AADD(aDescri,  {"000017",STR0071}) //"Este pode ser definido como o risco de perdas, pelo fato de informacoes nao poderem ser recebidas, processadas, armazenadas e transmitidas em tempo habil e de forma confiavel."
	AADD(aFato     ,{"000017",STR0072})  //"1) Situacoes onde informacoes consolidadas sobre exposicao de um banco nao podem ser obtidas em tempo habil para analise;2) Impossibilidade de prestar informacoes precisas em determinados horarios devido a atualizacao de bancos de dados ocorrer por processamento em batch."
	AADD(aConseq,{"000017",STR0069})   //"Comprometimento na producao e ou qualidade dos servicos prestados"

	AADD(aRisco   ,{"000018",STR0073,"03"})  //"RISCO DE EQUIPAMENTO"
	AADD(aDescri,  {"000018",STR0074})//"Este pode ser definido como o risco de perdas por falhas nos equipamentos eletricos, de processamento e transmissao de dados telefonicos, de seguranca, etc."
	AADD(aFato     ,{"000018",STR0075})   //"1) Rede de micros contaminados por virus; 2) Discos rigidos danificados;2) Telefonia nao operacional por falta de reparos."
	AADD(aConseq,{"000018",STR0076})    //"Queda na producao, qualidade dos servicos e retrabalhos."

	AADD(aRisco   ,{"000019",STR0077,"03"})    //"RISCO DE ERRO NAO INTENCIONAL"
	AADD(aDescri,  {"000019",STR0078})  //"Este pode ser definido como o risco de perdas em decorrencias de equivoco, omissao, distracao ou negligencia de funcionarios telefonicos, de seguranca, etc."
	AADD(aFato     ,{"000019",STR0079})  //"1) Mal atendimento de correntistas ou clientes (ma vontade, falta de informacao, etc); 2) Posicionamento da tesouraria no mercado do contrario ao especificado pelo Comite de Investimentos."
	AADD(aConseq,{"000019",STR0080})   //"Queda na qualidade dos servicos prestados."

	AADD(aRisco   ,{"000020",STR0081,"03"})     //"RISCO DE FRAUDES"
	AADD(aDescri,  {"000020",STR0082})  //"Este pode ser definido como o risco de perdas em decorrencias de comportamento fraudulentos (adulteracao de controles, descumprimento intencional das normas da empresa, desvio de valores, divulgacao de informacoes erradas, etc."
	AADD(aFato     ,{"000020",STR0083})  //"1) Desvio de dinheiro de agencia bancaria ou caixa central; 2) Aceitacao de 'incentivos' de clientes para conceder credito em valores mais elevados."
	AADD(aConseq,{"000020",STR0084})    //"Prejuizo generalizado para a empresa."

	AADD(aRisco   ,{"000021",STR0085,"03"})   //"RISCO DE QUALIFICACAO"
	AADD(aDescri,  {"000021",STR0086})  //"Este pode ser definido como o risco de perdas pelo fato de funcionarios desempenharem tarefas sem qualificacao apropriada a funcao."
	AADD(aFato     ,{"000021",STR0087+STR0088+STR0089})  //"1) Uso de estrategia de hedge com derivativos, sem conhecimento por parte do operador das limitacoes;"###" 2) Calculo de perdas & lucros em carteiras, sem conhecimento dos mercados;"###" 3) Iniciar operacoes em mercados 'sofisticados', sem contar com equipes (back-office e front-office) devidamente preparadas."
	AADD(aConseq,{"000021",STR0090})  //"Perda na qualidade dos servicos."

	AADD(aRisco   ,{"000022",STR0091,"03"})  //"RISCO DE PRODUTOS & SERVICOS"
	AADD(aDescri,  {"000022",STR0092}) //"Este pode ser definido como o risco de perdas em decorrencias da venda de produtos ou prestacao de servicos ocorrer de forma indevida, ou sem atender as necessidades e demandas de clientes."
	AADD(aFato     ,{"000022",STR0093})  //" 1) Envio de cartoes de credito sem consulta previa ao cliente; 2) Recomendar a clientes de perfil conservador o investimento em fundos de derivativos alavancados diante de um bom desempenho no passado recente destes mesmos fundos."
	AADD(aConseq,{"000022",STR0094})   //"Fuga de Clientes para concorrencia."

	AADD(aRisco   ,{"000023",STR0095,"03"})  //"RISCO DE REGULAMENTACAO"
	AADD(aDescri,  {"000023",STR0096})//"Este pode ser definido como o risco de perdas em decorrencias de alteracoes, impropriedades ou inexistencia de normas para controles internos ou externos."
	AADD(aFato     ,{"000023",STR0097}) //"1) Alteracao de margens de garantia ou de limites de oscilacao em bolsas de derivativos sem aviso antecipado ao mercado; 2) Front-office responsavel pela operacao de Back-Office."
	AADD(aConseq,{"000023",STR0098})    //"Falhas no processo operacional."

	AADD(aRisco   ,{"000024",STR0099,"03"})
	AADD(aDescri,  {"000024",STR0100}) //"Este pode ser definido como o risco de perdas pelo desenvolvimento, utilizacao ou interpretacao incorreta dos resultados fornecidos por modelos, incluindo a utilizacao de dados incorretos."
	AADD(aFato     ,{"000024",STR0101})  //"1) Utilizar software comprado de terceiros, sem conhecimento de suas limitacoes;2) Utilizar modelos matematicos, sem conhecimento de suas hipoteses simplificadoras."
	AADD(aConseq,{"000024",STR0102})        //"Queda na qualidade dos servicos ou producao."

	AADD(aRisco   ,{"000025",STR0103,"03"})   //"RISCO DE LIQUIDACAO FINANCEIRA"
	AADD(aDescri,  {"000025",STR0104})  //"Este pode ser definido como o risco de perdas em decorrencia de falhas nos procedimentos e controles de finalizacao das transacoes."
	AADD(aFato     ,{"000025",STR0105})  //"1) Envio ou recebimento de divisas em pracas com diferentes fusos horarios, feriados, regras operacionais, etc."
	AADD(aConseq,{"000025",STR0106})  //"Falhas nos processos operacionais."

	AADD(aRisco   ,{"000026",STR0107,"03"})  //"RISCO SISTEMICO"
	AADD(aDescri,  {"000026",STR0108}) //"Este pode ser definido como o risco de perdas devido a alteracoes no ambiente operacional."
	AADD(aFato     ,{"000026",STR0109}) //"1) Alteracao abrupta de limites operacionais em bolsas, levando todas as instituicoes financeiras a dificuldades; 2) Modificacao repentina de base de calculo de tributos corporativos."
	AADD(aConseq,{"000026",STR0110})  //"Reducao de receitas nas instituicoes."

	AADD(aRisco   ,{"000027",STR0111,"03"}) //"RISCO DE CONCENTRACAO OPERACIONAL"
	AADD(aDescri,  {"000027",STR0112})   //"Este pode ser definido como o risco de perdas por depender de poucos produtos, clientes e/ou mercados."
	AADD(aFato     ,{"000027",STR0113})   //"1) Bancos que so operam financiando clientes de determinado segmento (por exemplo, setor automotivo, lojistas)."
	AADD(aConseq,{"000027",STR0114})    //"Rentabilidade limitada."

	AADD(aRisco   ,{"000028",STR0115,"03"})  //"RISCO DE IMAGEM"
	AADD(aDescri,  {"000028",STR0116})  //"Este pode ser definido como o risco de perdas em decorrencia de alteracao da reputacao juntos a clientes, concorrentes, orgaos governamentais, etc."
	AADD(aFato     ,{"000028",STR0117}) //"1) Boatos sobre a saude de uma instituicao, desencadeando corrida para saques."
	AADD(aConseq,{"000028",STR0118})    //"Fuga de clientes e/ou parceiros."

	AADD(aRisco   ,{"000029",STR0119,"03"})   //"RISCO DE CATASTROFE"
	AADD(aDescri,  {"000029",STR0120})     //"Este pode ser definido como o risco de perdas devida a catastrofes (naturais ou nao)."
	AADD(aFato     ,{"000029",STR0121})//"1) Desastres naturais (enchentes) que dificultem a operacao diaria da instituicao ou areas criticas como centros de processamento de dados, de telecomunicacoes, etc.; 2) Destruicao do patrimonio da instituicao por desastres que abalem a estrutura civil de predios (colisao de avioes, caminhoes, etc), incendios, rebelioes, etc."
	AADD(aConseq,{"000029",STR0122}) //"Encerramento parcial ou total da operacao."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Risco Legal...      			                                                              .                       �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aRisco   ,{"000030",STR0123,"04"})  //"RISCO DE LEGISLACAO"
	AADD(aDescri,  {"000030",STR0124})  //"Este pode ser definido como o risco de perdas decorrentes de sancoes por reguladores e indenizacoes por danos a terceiros por violacao da legislacao vigente."
	AADD(aFato     ,{"000030",STR0125})  //"1) Multas por nao cumprimento de exigibilidades; 2) Indenizacoes pagas a clientes por nao cumprimento da legislacao."
	AADD(aConseq,{"000030",STR0126}) //"Dividas judiciais, reducao de lucro"

	AADD(aRisco   ,{"000031",STR0127,"04"})  //"RISCO TRIBUTARIO"
	AADD(aDescri,  {"000031",STR0128})  //"Este pode ser definido como o risco de perdas devido a criacao ou nova interpretacao da incidencia de tributos"
	AADD(aFato     ,{"000031",STR0129})   //"1) Criacao de impostos novos sobre ativos e/ou produtos; 2) Recolhimento de novas contribuicao sobre receitas."
	AADD(aConseq,{"000031",STR0130})  //"Dividas tributarias, reducao de lucro"

	AADD(aRisco   ,{"000032",STR0131,"04"}) //"RISCO DE CONTRATO"
	AADD(aDescri,  {"000032",STR0132})  //"Este pode ser definido como o risco de perdas decorrentes de julgamentos desfavoraveis por contratos omissos, mal redigidos ou sem o devido amparo legal"
	AADD(aFato     ,{"000032",STR0133})  //1) Pessoa sem poder para assinar contratos representando a instituicao; 2)Nao execucao pronta de garantias,requerendo o acionamento juridico. 3) Responsabilidades cobertas nos contratos de terceirizacao colocadas de forma pouco objetivas."
	AADD(aConseq,{"000032",STR0134})  //"Falta de recebimento dos direitos"

    Processa( { | lEnd | CriaICG(aRisco,aDescri,aFato,aConseq)} ,STR0135,STR0136)   //"Aguarde!"###"Gerando registros para tabela de riscos..."
EndIf
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaICG    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em ICG com base nos array pre-definidos	  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaICE(ExpA1,ExpA2,ExpA3)			      			  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aRisco							          潮�
北�          � ExpA2 := Array aFato			      					      潮�
北�          � ExpA3 := Array aConseq        				  		      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaICG(aRisco,aDescri,aFato,aConseq)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aRisco) )
     	                                                                           
For nI := 1 To Len(aRisco)

		IncProc()
	
		SX5->(DbSeek(xFilial("SX5")+"I1"+ aRisco[nI][3]))
		
		DbSelectArea("ICG")
		If ! DbSeek(xFilial("ICG")+aRisco[nI][1])
		    RecLock("ICG",.T.)
			Replace ICG_FILIAL		With xFilial("ICG")
		    Replace ICG_RISCO		With aRisco[nI][1]
		    Replace ICG_DESC     	With aRisco[nI][2]
		    Replace ICG_TIPO		With aRisco[nI][3]
//		    Replace ICG_DESTIP		With X5Descri()
		    
		    If ! Empty(aDescri[nI][2]) 
			   	MSMM(ICG->ICG_CODOBJ,,,aDescri[nI][2],1,,,"ICG","ICG_CODOBJ")
				ConfirmSX8()		
			EndIf
			
		    If ! Empty(aFato[nI][2]) 
			   	MSMM(ICG->ICG_CODFAT,,,aFato[nI][2],1,,,"ICG","ICG_CODFAT")
				ConfirmSX8()		
			EndIf

		    If ! Empty(aConseq[nI][2]) 
			   	MSMM(ICG->ICG_CODCON,,,aConseq[nI][2],1,,,"ICG","ICG_CODCON")
				ConfirmSX8()		
			EndIf
			MsUnLock()
		Endif
Next nI
RestArea( aArea )	
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILICQ   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela pontos de controle...                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilICQ()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilICQ()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aPontos := {} 
Local aMemos := {}

If lCria .and. IC5->IC5_GESTAO == "2"	//Sarbanes-Oxley

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Modulo ICE/01-Nossa Missao...	        �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
    AADD(aPontos,{ "000001",STR0137,"ICE","01"})  //"Existe definicao da missao em sua organizacao..."
    AADD(aMemos,{ "000001",STR0138})   //"Existe definicao da missao em sua organizacao? E no seu departamento? De uma nota para este item de controle em sua organizacao:"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Modulo ICE/02-Politica de Controles Internos... �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�  
    AADD(aPontos,{ "000002",STR0139,"ICE","02"})  //"Existe uma politica formalizada definida..."
    AADD(aMemos,{ "000002",STR0140})  //"Existe uma politica formalizada definida e difundida a respeito de controles internos, na sua empresa? De sua nota:"

    AADD(aPontos,{ "000003",STR0141,"ICE","02"}) //"Concordancia formal com os padroes eticos..."
    AADD(aMemos,{ "000003",STR0142})  //"Concordancia formal com os padroes eticos estabelecidos pela corporacao"

    AADD(aPontos,{ "000004",STR0143,"ICE","02"}) //"Pessoal de confianca com habilidade..."
    AADD(aMemos,{"000004",STR0144})  //"Pessoal de confianca com habilidade, treinamento e experiencia necessarios para executar satisfatoriamente as suas responsabilidades."
	
	AADD(aPontos,{ "000005",STR0145,"ICE","02"})  //"Concordancia formal com as politicas..."
    AADD(aMemos,{"000005",STR0146})  //"Concordancia formal com as politicas de conflito de interesse e etica de negocios estabelecida pela companhia."

	AADD(aPontos,{ "000006",STR0147,"ICE","02"})//"Um plano organizacional..."
    AADD(aMemos,{"000006",STR0148}) //"Um plano organizacional (Organograma) que inclua a delegacao e a coordenacao das responsabilidades estabelecidas com a devida segregacao de funcoes incompativeis."

	AADD(aPontos,{ "000007",STR0149,"ICE","02"})  //"Uma adequada estrutura contabil..."
    AADD(aMemos,{"000007",STR0150}) //"Uma adequada estrutura contabil em cada unidade operacional, incluindo tecnicas contabeis e de orcamento, um plano de contas adequado, manuais de procedimentos, e onde aplicaveis, fluxogramas detalhados das transacoes."
                                           
	AADD(aPontos,{ "000008",STR0151,"ICE","02"})  //"Adequados procedimentos para autorizacao..."
    AADD(aMemos,{"000008",STR0152})//"Adequados procedimentos para autorizacao e aprovacao de transacoes"
                                            
	AADD(aPontos,{ "000009",STR0153,"ICE","02"}) //"Registro das transacoes em detalhes razoaveis..."
    AADD(aMemos,{"000009",STR0154})  //"Registro das transacoes em detalhes razoaveis, exatos, e em tempo habil"

	AADD(aPontos,{ "000010",STR0155,"ICE","02"})  //"Revisao completa da preparacao dos relatorios..."
    AADD(aMemos,{"000010",STR0156}) //"Revisao completa da preparacao dos relatorios contabeis e financeiros para uso interno e externo"
    
   	AADD(aPontos,{ "000011",STR0157,"ICE","02"})//"Instalacoes fisicas adequadas..."
    AADD(aMemos,{"000011",STR0158}) //"Instalacoes fisica adequadas e salvaguardas para prevenir movimentacao inadequada de bens, e para proteger os registros contabeis da empresa"

   	AADD(aPontos,{ "000012",STR0159,"ICE","02"})//"Monitaramento regular...."
    AADD(aMemos,{"000012",STR0160})  //"Monitormaneto regular e adequada pela gerencia operacional"

   	AADD(aPontos,{ "000013",STR0161,"ICE","02"})  //"Revisao dos sistemas e controles...."
    AADD(aMemos,{"000013",STR0162})  //"Revisao dos sistemas e controles da empresa por um extensivo programa de auditoria interna e externa"

   	AADD(aPontos,{ "000014",STR0163,"ICE","02"})  //"Coordenacao da auditoria..."
    AADD(aMemos,{"000014",STR0164}) //"Coordenacao da auditoria interna com a auditoria externa pelo Controller"

   	AADD(aPontos,{ "000015",STR0165,"ICE","02"}) //"Avaliacao continua..."
    AADD(aMemos,{"000015",STR0166}) //"Avaliacao continua da relacao custo/beneficio dos controles existentes"

   	AADD(aPontos,{ "000016",STR0167,"ICE","02"}) //"Comprometimento dos niveis gerenciais..."
    AADD(aMemos,{"000016",STR0168}) //"Comprometimento dos niveis gerenciais para com controles internos projetados para detectar fraudes e praticas ilegais e antieticas que ponham em risco o conceito da empresa"

   	AADD(aPontos,{ "000017",STR0169,"ICE","02"}) //"Controle superior..."
    AADD(aMemos,{"000017",STR0170})  //"Controle superior e autorizacao por escrito para quaisquer transgressoes de controles intrenos de grande importancia para a empresa. Esta autorizacao por escrito dever� ser arquivada no departamento de Auditoria Interna."

  	AADD(aPontos,{ "000018",STR0171,"ICE","02"}) //"A definicao de responsabilidades..."
    AADD(aMemos,{"000018",STR0172})//"A definicao de responsabilidades e funcoes departamentais dever ser expressa na forma de um organograma, suportado por adequadas descricoes de funcoes."

  	AADD(aPontos,{ "000019",STR0173,"ICE","02"}) //"Manuais de procedimentos detalhados..."
    AADD(aMemos,{"000019",STR0174}) //"Manuais de procedimentos detalhados, em suporte as politicas e diretrizes emitidas pela Diretoria, devem ser desenvolvidos a fim de garantir consistencia no processamento das transacoes diarias. Sempre que aplicaveis as politicas e procedimentos gerados pela Matriz devem ser adotados, conforme os Manuais Internacionais existentes."

  	AADD(aPontos,{ "000020",STR0175,"ICE","02"})//"Um adequado plano de contas deve existir..."
    AADD(aMemos,{"000020",STR0176})//"Um adequado plano de contas dever existir, juntamente com um Manual de Contabilidade com procedimentos detalhados para para o registro nas diversas contas"

  	AADD(aPontos,{ "000021",STR0177,"ICE","02"}) //"Um sistema eficiente de relatorios..."
    AADD(aMemos,{"000021",STR0178})//"Um sistema eficiente de relatorios gerenciais deve existir para fornecer dados consistentes a administracao.Estes relatorios devem incluir o Balancete Mensal, Fluxo de Caixa, alem dos relatorios operacionais"

  	AADD(aPontos,{ "000022",STR0179,"ICE","02"})  //"A elaboracao de instrucoes por escrito..."
    AADD(aMemos,{"000022",STR0180})//"A elaboracao de instrucoes por escrito e fluxogramas para documentar o processamento de pedidos, ordens de compra pedidos de cheque, recebimentos de caixa, etc, e util para trazser a luz ineficiencias no processamento e deficiencias nos controles internos. As praticas e controles adotadas serao mais bem mantidos se definidos por escrito."

  	AADD(aPontos,{ "000023",STR0181,"ICE","02"}) //"Os seguintes departamentos devem ser independentes..."
    AADD(aMemos,{"000023",STR0182})  //"Os seguintes departamentos devem ser independentes um do outro: Contabilidade, Faturamento, Credito e Cobranca, Compras,Recebimentos, Contas a Pagar, Caixa, Vendas, Expedicao, Planejamento da Producao e deve haver adequada segregacao de funcoes."

  	AADD(aPontos,{ "000024",STR0183,"ICE","02"}) //"O encaminhamento de documentos..."
    AADD(aMemos,{"000024",STR0184}) //"O encaminhamento de documentos e valores diretamente as pessoas autorizadas a recebe-los e importante para a protecao dos controles internos."

  	AADD(aPontos,{ "000025",STR0185,"ICE","02"})//"Controles adequados devem..."
    AADD(aMemos,{"000025",STR0186})//"Controles adequados devem ser exercidos na definicao e delegacao de responsabilidades na manutencao de numerarios e ativos da Companhia, quando da admissao de pessoal para exercer tais funcoes. Os funcionarios exercendo estas responsabilidades devem gozar suas ferias anualmente, enquanto suas obrigacoes sao exercidas por outra pessoa."

 	AADD(aPontos,{ "000026",STR0187,"ICE","02"}) //"Adequada seguranca fisica..."
    AADD(aMemos,{"000026",STR0188})//"Adequada seguranca fisica dever ser exercida sobre as dependencias da Companhia. Registros contabeis de importancia devem ser protegidos contra incendios, inundacoes e deterioracoes por outras causas."

 	AADD(aPontos,{ "000027",STR0189,"ICE","02"}) //"Qualquer ocorrencia de desvios..."
    AADD(aMemos,{"000027",STR0190}) //"Qualquer ocorrencia de desvios de bens da Companhia ou operacoes fraudulentas deve ser imediato reportadas a Controladoria."

 	AADD(aPontos,{ "000028",STR0191,"ICE","02"})  //"Nenhum funcionario..."
    AADD(aMemos,{"000028",STR0192}) //"Nenhum funcionario devera liberar informacoes ou dar entrevista a orgaos de midia sem antes consultar os niveis superiores."

 	AADD(aPontos,{ "000029",STR0193,"ICE","02"}) //"Nao devera existir nos departamentos..."
    AADD(aMemos,{"000029",STR0194})    //"Nao devera existir dentros dos departamentos funcoes e controles exercido em duplicidades."

 	AADD(aPontos,{ "000030",STR0195,"ICE","02"})  //"O departamento juridico deve controlar..."
    AADD(aMemos,{"000030",STR0196})//"O departamento juridico deve controlar, conforme as determinacoes da Controladoria, quais as relacoes da empresa com  terceiros devem ser objeto de instrumento contratual. Os contratos, antes de formalizados, devem ser analisados pelo Departamento Juridico."

 	AADD(aPontos,{ "000031",STR0197,"ICE","02"}) //"Um procedimento adequado..."
    AADD(aMemos,{"000031",STR0198})  //"Um procedimento adequado dever existir quanto ao 'folow-up' da expiracao destes contratos e sua renovacao."

 	AADD(aPontos,{ "000032",STR0199,"ICE","02"})  //"O uso de veiculos departamentais..."
    AADD(aMemos,{"000032",STR0200})  //"O uso de veiculos departamentais deve restringir-se aos interesses da Companhia. As gerencias departamentais deverao adotar controles adequados sobre seu uso. Os veiculos deverao ser mantidos nas dependencias da Companhia sempre que nao estejam sendo usados."

 	AADD(aPontos,{ "000033",STR0201,"ICE","02"})//"A Companhia espera..."
    AADD(aMemos,{"000033",STR0202})//"A Companhia espera de seus funcionarios uma conduta profissional no relacionamento com o ambiente interno e externo da empresa."
                                                                           
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Pontos de Controle/Modulo Gestao de RH/04-Administaracao de Pessoal... �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
 	AADD(aPontos,{ "000034",STR0203,"GPE","04"}) //"Todos os funcionarios da Companhia..."
    AADD(aMemos,{"000034",STR0204})   //"Todos os funcionarios da Companhia devem gozar suas ferias regularmente."

 	AADD(aPontos,{ "000035",STR0205,"GPE","04"}) //"A execucao de horas extras..."
    AADD(aMemos,{"000035",STR0206})//"A execucao de horas extras dever ser aprovada previa e formalmente pelos niveis hierarquicos superiores"
   
 	AADD(aPontos,{ "000036",STR0207,"GPE","04"})   //"Toda alteracao no cadastro..."
    AADD(aMemos,{"000036",STR0208}) //"Toda alteracao no cadastro de funcionarios devera ser formalmente aprovada pelos niveis adequados"

 	AADD(aPontos,{ "000037",STR0209,"GPE","04"})//"Os dados que influem no calculo..."
    AADD(aMemos,{"000037",STR0210})//"Os dados que influem no calculo dos salarios devem ser mantidos constantemente atualizados"

 	AADD(aPontos,{ "000038",STR0211,"GPE","04"})//"O sistema de arquivo de informacoes..."
    AADD(aMemos,{"000038",STR0212})//"O sistema de arquivo de informacoes deve ser atualizado e atender as exigencias para a otima execucao das funcoes do departamento"

 	AADD(aPontos,{ "000039",STR0213,"GPE","04"})//"E funcao do Departamento..."
    AADD(aMemos,{"000039",STR0214})//"E funcao do Departamento de Administracao de Pessoal providenciar o atendimento as exigencias da legislacao trabalhista, a fim de evitar a sua duplicidade e prevenir o seu mau uso."

 	AADD(aPontos,{ "000040",STR0215,"GPE","04"})//"O acesso aos arquivos e informacoes..."
    AADD(aMemos,{"000040",STR0216})//"O acesso aos arquivos e informacoes do departamento dever ser escrito a pessoa cuja funcao o requeira."

 	AADD(aPontos,{ "000041",STR0217,"GPE","04"})//"As mudancas de salarios..."
    AADD(aMemos,{"000041",STR0218})//"As mudancas de salarios, exceto as ocasionadas por dissidio coletivo, devem ser objeto de revisao pela Gerencia mediante relatorio adequado, emitido mensalmente por via de sistemas."

 	AADD(aPontos,{ "000042",STR0219,"GPE","04"})//"As mudancas no cadastro..."
    AADD(aMemos,{"000042",STR0220})//"As mudancas ocorridas no cadastro de funcionarios devem ser checadas com os documentos originais, apos serem processadas pelo computador. Esta checagem deve ficar evidenciada."

 	AADD(aPontos,{ "000043",STR0219,"GPE","04"}) //"As mudancas no cadastro..."
    AADD(aMemos,{"000043",STR0220})//"As mudancas ocorridas no cadastro de funcionarios devem ser checadas com os documentos originais, apos serem processadas pelo computador. Esta checagem deve ficar evidenciada."

	AADD(aPontos,{ "000044",STR0221,"GPE","04"})//"A alimentacao de informacoes ao sistema..."
    AADD(aMemos,{"000044",STR0222})//"A alimentacao de informacoes ao sistema dever ser feita por meio dos documentos originais, em que as autorizacoes foram formalmente obtidas."

	AADD(aPontos,{ "000045",STR0223,"GPE","04"})//"A compilacao de tabelas..."
    AADD(aMemos,{"000045",STR0224})//"A compilacao de tabelas de faixas salariais deve seguir normas de verificacao quanto a sua exatidao, e seu uso deve ser confidencial."

	AADD(aPontos,{ "000046",STR0225,"GPE","04"}) //"A admissao de novos funcionarios..."
    AADD(aMemos,{"000046",STR0226})//"A admissao de novos funcionarios deve ser precedida de investigacoes adequadas, de acordo com as responsabiliaddes a serem assumidas"
                                           
	AADD(aPontos,{ "000047",STR0227,"GPE","04"}) //"Em consonancias as politicas..."
    AADD(aMemos,{"000047",STR0228+STR0229+STR0230+STR0231+STR0232})//"Em consonancias as politicas da Matriz, e tambem politica da empresa adotar uma postura ausnete de preconceito,"###" discriminacao ou favorecimento baseado em diferencas de raca, sexo, cor, religiao, origem, nacionalidade ou defeitos"###" fisicos, em relacao a quaisquer atividades de pessoal, incluindo recrutamento, selecao, admissao, transferencia, promocoes,"### " treinamento, beneficios sociais, programas de seguro de vida, programas de recreacoes, etc., deverao ser administrados de"###" acordo com esta politica. Todos os funcionarios da Companhia deverao adotar uma conduta condizente com esta politica."

	AADD(aPontos,{ "000048",STR0233,"GPE","04"})//"Os beneficios existentes devem..."
    AADD(aMemos,{"000048",STR0234})//"Os beneficios existentes devem ser adequados e plenamente difundidos a todos os niveis de pessoal com direito a eles."

	AADD(aPontos,{ "000049",STR0235,"GPE","04"}) //"A empresa adotara um conjunto..."
    AADD(aMemos,{"000049",STR0236})//"A empresa adotar um conjunto de ponto eletronico, horario flexivel e banco de horas a fim de evitar a formacao de contigencias trabalhistas. Todas as horas extras devem ser previamente aprovadas pela gerencia."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Ativo Fixo/05-Administracao de Ativos...  �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000050",STR0237,"ATF","05"})//"Controles adequados devem ser exercidos..."
    AADD(aMemos,{"000050",STR0238}) //"Controles adequados devem ser exercidos sobre todas as propriedades da Companhia."

	AADD(aPontos,{ "000051",STR0239,"ATF","05"})//"A movimentacao de bens..."
    AADD(aMemos,{"000051",STR0240})//"A movimentacao de bens entre os varios centros de custos da Companhia deve ser disciplinada e formalizada a fim de manter o controle sobre a localizacao deles."

	AADD(aPontos,{ "000052",STR0241,"ATF","05"}) //"A definicao do que deve ser imobilizado..."
    AADD(aMemos,{"000052",STR0242})//"A definicao do que ser imobilizado deve seguir os preceitos legais, assim como os metodos de correcao e depreciacao"

	AADD(aPontos,{ "000053",STR0243,"ATF","05"}) //"A venda ou baixa de qualquer ativo..."
    AADD(aMemos,{"000053",STR0244})//"A venda ou baixa de qualquer ativo da Companhia deve ser expressa e formalmente aprovada pela controladoria."

	AADD(aPontos,{ "000054",STR0245,"ATF","05"})  //"A compra de ativos..."
    AADD(aMemos,{"000054",STR0246+STR0247+STR0248})//"A compra de ativos deve seguir os processos de aprovacao adequados e imediata identificacao deve ser providenciada."### " As compras nao devem ser efetuadas diretamente pelos departamentos usuarios e sim pelo Departamento de Compras"###" Companhia, salvo excecoes expressamente aprovadas pela Diretoria."

	AADD(aPontos,{ "000055",STR0249,"ATF","05"}) //"Todos os bens da Companhia..."
    AADD(aMemos,{"000055",STR0250+STR0251})//"Todos os bens da Companhia devem trazer sua plaqueta de identificacao. A identificacao deve obedecer a um ordem numerica"###" sequencial, em que a duplicidade de registros nao deve exisitir."

	AADD(aPontos,{ "000056",STR0252,"ATF","05"})//"Os registros contabeis de inventario..."
    AADD(aMemos,{"000056",STR0253+STR0254})//"Os registros contabeis de inventario devem sempre estar atualizados, refletindo a situacao real das propriedades da Companhia."###" Listagem completa dos bens existentes dever ser mantida disponivel pela Contabilidade."
 
 	AADD(aPontos,{ "000057",STR0255,"ATF","05"})//"Registros adequados devem..."
    AADD(aMemos,{"000057",STR0256+STR0257})//"Registros adequados devem ser mantidos sobre ativos da Companhia em poder de terceiros. Na realizacao do balanco estes ativos"###" tambem devem ser objeto de contagem fisica e confirmacao de sua existencia."

 	AADD(aPontos,{ "000058",STR0258,"ATF","05"})//"A aquisicao de qualquer ativo fixo..."
    AADD(aMemos,{"000058",STR0259+STR0260})//"A aquisicao de qualquer ativo fixo deve obedecer aos criterios de aprovacao exigidos na emissao do documento Autorizacao para"###" Compra de Ativos."

 	AADD(aPontos,{ "000058",STR0258,"ATF","05"}) //"A aquisicao de qualquer ativo fixo..."
    AADD(aMemos,{"000058",STR0259+STR0260})//"A aquisicao de qualquer ativo fixo deve obedecer aos criterios de aprovacao exigidos na emissao do documento Autorizacao para"###" Compra de Ativos."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Controle de Auditoria/26-Qualidade Total... �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
 	AADD(aPontos,{ "000059",STR0261,"QAD","26"})  //"A Auditoria Interna tem como objetivo..."
    AADD(aMemos,{"000059",STR0262+ STR0263+STR0264 }) //"A Auditoria Interna tem como objetivo prestar uma avaliacao independente das diversas operacoes e controles da Companhia,"###" para determinar se os procedimentos e politicas estao sendo seguidos, se os padroes estabelecidos sao cumpridos, se os"###" recursos sao usados efeicente e economicamente, e se os objetivos da organizacao estao sendo atingidos."

 	AADD(aPontos,{ "000060",STR0265,"QAD","26"}) //"A funcao de Auditoria Interna..."
    AADD(aMemos,{"000060",STR0266})   //"A funcao de Auditoria Interna se reportara ao nivel maximo da organizacao."

 	AADD(aPontos,{ "000061",STR0267,"QAD","26"}) //"Os auditores internos..."
    AADD(aMemos,{"000061",STR0268})//"Os auditors internos terao acesso irrestrito a quaisquer documentos propriedades e pessoal da Companhia."

 	AADD(aPontos,{ "000062",STR0269,"QAD","26"})//"A funcao da auditoria devera..."
    AADD(aMemos,{"000062",STR0270+STR0271}) //"A funcao da auditoira devera sempre ir alem da simples verificacao de se as regras sao adequadas e se os objetivos"###" estao sendo cumpridos.Ela devera preocupar-se em otimizar sempre a efeciencia operacional dentro da empresa."

 	AADD(aPontos,{ "000063",STR0272,"QAD","26"}) //"Devera preocupar-se em verificar..." 
    AADD(aMemos,{"000063",STR0273+STR0274})//"Devera preocupar-se em verificar se as leis e regulamentacoes internas e externas estao sendo cumpridas, e se os" ###" relatorios e controles financeiros sao exatos e adequados." 

 	AADD(aPontos,{ "000064",STR0275,"QAD","26"})//"Revisara a gerencia de recursos pessoais..."
    AADD(aMemos,{"000064",STR0276+STR0277})//"Revisara a gerencia de recursos pessoais, materiais e financeiros, a fim de identificar possiveis deficiencias no seu uso,"###" e permitir a sua correcao e sua otimizacao."

 	AADD(aPontos,{ "000065",STR0278,"QAD","26"})//"Avaliara planos e metas..."
    AADD(aMemos,{"000065",STR0279})//"Avaliara plano e metas a fim de determinar se os objetivos sao atingidos."

 	AADD(aPontos,{ "000066",STR0280,"QAD","26"})//"Devera, ainda,manter boas relacoes..."
    AADD(aMemos,{"000066",STR0281})//"Devera, ainda manter boas relacoes com a Auditoria Externa, coordenando seus trabalhos para melhor resultado conjunto."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Financeiro/06-Caixas Bancos...          �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000067",STR0282,"FIN","06"})  //"Toda conta bancaria..."
    AADD(aMemos,{"000067",STR0283})//"Toda conta bancaria deve ser previamente autorizada pela diretoria, e todas as contas devem ter sua finalidade justificada."

	AADD(aPontos,{ "000068",STR0284,"FIN","06"})//"Nao deverao ser mantidas..."
    AADD(aMemos,{"000068",STR0285})//"Nao deverao ser mantidas contas bancarias inativas."

	AADD(aPontos,{ "000069",STR0286,"FIN","06"})//"A responsabilidade de recebimentos..."
    AADD(aMemos,{"000069",STR0287+STR0288}) //"A responsabilidade de recebimento e depositos de valores deve estar centralizada no menor numero de pessoas, e as funcoes"###" devem estar segregadas."

	AADD(aPontos,{ "000070",STR0289,"FIN","06"})//"Deve haver um sistema..."
    AADD(aMemos,{"000070",STR0290+STR0291})//"Deve haver um sistema de conferencias internas no departamento de Tesourraria"###", conforme os recurso humanos o permitam."

	AADD(aPontos,{ "000071",STR0292,"FIN","06"})  //"Os funcionarios com alta responsabilidade..."
    AADD(aMemos,{"000071",STR0293+STR0294+STR0295})//"Os funcionarios com alta responsabilidade sobre numerarios devem ser"###" afiancados e cobertos por seguro e devem gozar suas ferias anualmente,"###" sendo suas funcoes exercidads por pessoal substituto."
                           
	AADD(aPontos,{ "000071",STR0296,"FIN","06"})//"Os funcionarios do departamento da Tesouraria..."
    AADD(aMemos,{"000071",STR0297+STR0298})//"Os funcionarios do departamento da Tesouraria nao devem executar funcoes alheias"###" ao departmento, conforme a disponibilidade de recursos humanos."

	AADD(aPontos,{ "000072",STR0299,"FIN","06"})//"Deve haver registros independentes..."
    AADD(aMemos,{"000072",STR0300+STR0301+STR0302})//"Deve haver registros independentes da conatbilidade na Tesouraria, sobre"###" titulos e valores mantidos no departamento, e periodicamente deve haver"###" um procedimento de contagem e conciliacao com a contabilidade."

	AADD(aPontos,{ "000073",STR0303,"FIN","06"})//"Todo e qualquer emprestimo..."
    AADD(aMemos,{"000073",STR0304})//"Todo e qualquer emprestimo bancario deve ser aprovado."

	AADD(aPontos,{ "000074",STR0305,"FIN","06"})//"Deve ser elaborada uma procuracao..."
    AADD(aMemos,{"000074",STR0306+STR0307+STR0308+STR0309+STR0310})//"Deve ser elaborada um procuracao que especifique detalhadamente"###" quem sao as pessoas com poderes de assinatura em nome da empresa,"###" e quais seus limites. Somente assinaturas em conjunto devem ser usados acima de certo limite,"###" e sempre os dois niveis hierarquicos mais altos disponiveis no momento devem assinar."###" Cada pessoa autorizada deve ter uma copia desta procuracao."

	AADD(aPontos,{ "000075",STR0311,"FIN","06"})//"No desligamento de qualquer pessoa..."
    AADD(aMemos,{"000075",STR0312+STR0313})//"No desligamento de qualquer pessoa com poderes de procuracao, deve haver controle sobre"###" o seu cancelamento, que deve ser feito de imediato nos bancos."

	AADD(aPontos,{ "000076",STR0314,"FIN","06"})//"Todo adiantamento a funcionarios..."
    AADD(aMemos,{"000076",STR0315+STR0316})//"Todo adiantamento a funcionarios ou a terceiros dever ser feito mediamente um formulario"###" apropriado em que as aprovacoes dos niveis gerenciais adequados fiquem evidenciadas."

	AADD(aPontos,{ "000077",STR0317,"FIN","06"})//"Os limites de credito aos clientes..."
    AADD(aMemos,{"000077",STR0318+STR0319+STR0320})//"Os limites de credito aos clientes dever ser previamente aprovados e reavaliados"###" periodicamente pela Diretoria Financeira. Excessos aos limites estipulados devem"###" ser aprovados ser aprovados pelos niveis adequados."

	AADD(aPontos,{ "000078",STR0321,"FIN","06"})//"A concessao de credito aos clientes..."
    AADD(aMemos,{"000078",STR0322+STR0323})//"A concessao de credito aos clientes dever ser criteriosa e coberta por garantias"###" reais, de acordo com as determinacoes da Diretoria Financeira."

	AADD(aPontos,{ "000079",STR0324,"FIN","06"})//"O departamento deve contantemente..."
    AADD(aMemos,{"000079",STR0325+STR0326})//"O departamento  dever constantemente atualizar as informacoes e dados" ###" relativos a situacao econimico-financeira dos clientes reais."

	AADD(aPontos,{ "000080",STR0327,"FIN","06"})//"As relacoes empresa-distribuidor..."
    AADD(aMemos,{"000080",STR0328+STR0329})//"As relacoes empresa-distribuidor-representante devem ser suportadas"###" por instrumento contratual definindo as responsabilidades, direitos e obrigacoes da partes."

	AADD(aPontos,{ "000081",STR0330,"FIN","06"})//"Prorrogacoes de vencimentos..."
    AADD(aMemos,{"000081",STR0331+STR0332})//"Prorragacoes de vencimentos de titulos devem estar devidamente suportadas e suas aprovacoes"###" evidenciadas pela gerencia."

	AADD(aPontos,{ "000082",STR0333,"FIN","06"})//"As posicoes contabeis..."
    AADD(aMemos,{"000082",STR0334;
                        + STR0335})//"As posicoes contabeis dos registros dos distribuidores devem ser periodicamente confrontadas"###" com as posicoes contabeis da Companhia."

	AADD(aPontos,{ "000083",STR0336,"FIN","06"})//"E funcao do departamento..."
    AADD(aMemos,{"000083",STR0337+STR0338})//"E funcao do departamento otimizar o uso dos numerarios cobrados, por medio de comunicacoes"###" imediatas com a rede bancaria e com a Tesouraria da Companhia."

	AADD(aPontos,{ "000084",STR0339,"FIN","06"})//"A aprovacao dos Pedidos..."
    AADD(aMemos,{"000084",STR0340})//"A aprovacao dos Pedidos do Faturamento deve ser criteriosoa e adequada"

	AADD(aPontos,{ "000085",STR0341,"FIN","06"})//"Um estrito controle sequencial..."
    AADD(aMemos,{"000085",STR0342}) //"Um estrito controle sequencial numerico sobre a emissao de titulos e duplicatas dever ser exercido."

	AADD(aPontos,{ "000086",STR0343,"FIN","06"})//"As duplicatas emitidas e nao negociadas..."
    AADD(aMemos,{"000086",STR0344+STR0345})//"As duplicadas emitidas e nao negociadas devem ser mantidas em cofre,"###" ao qual o acesso dever ser restrito, e cuja combinacao dever ser trocada periodicamente."

	AADD(aPontos,{ "000087",STR0346,"FIN","06"})//"Os titulos so devem ser endossados..."
    AADD(aMemos,{"000087",STR0347})//"Os titulos so devem ser endossados apos terem sido efetivamente negociados."

	AADD(aPontos,{ "000088",STR0348,"FIN","06"})  //"A utilizacao de assinaturas..."
    AADD(aMemos,{"000088",STR0349})//"A utilizacao de assinaturas automaticas dever ser devidamente protegida."

	AADD(aPontos,{ "000089",STR0350,"FIN","06"})//"A baixa de titulos deve..."
    AADD(aMemos,{"000089",STR0351})  //"A baixa de titulos dever ser adequadamente suportada pelos avisos de cobranca bancarios."

	AADD(aPontos,{ "000090",STR0352,"FIN","06"})//"O acesso ao sistema..."
    AADD(aMemos,{"000090",STR0353})  //"O acesso ao sistema deve ser restrito a pessoas autorizasa, tao somente."

	AADD(aPontos,{ "000091",STR0354,"FIN","06"})//"Os distribuidores devem..."
    AADD(aMemos,{"000091",STR0355+STR0356})//"Os distribuidores devem ser instruidos a remeterem eventuais pagamentos"###"em cheques diretamente a Tesouraria."

	AADD(aPontos,{ "000092",STR0357,"FIN","06"})//"Os lancamentos de creditos..."
    AADD(aMemos,{"000092",STR0358+STR0359+STR0360})//"Os lancamentos de creditos incobraveis a conta de Provisao para Devedores"###" Duvidosos devem ser feitos somente depois de esgotadas as possibilidades"###" reais de cobranca, e devem ser aprovados pela Tesouraria e Contraladoria."

	AADD(aPontos,{ "000093",STR0361,"FIN","06"})//"O cancelamento de qualquer titulo..."
    AADD(aMemos,{"000093",STR0362+STR0363})//"O cancelamento de qualquer titulo deve ser extensivamente suportado, e aprovado"###" pelos niveis gerenciais adequados."

	AADD(aPontos,{ "000094",STR0364,"FIN","06"})//"Deve haver uma conciliacao mensal..."
    AADD(aMemos,{"000094",STR0365})//"Deve haver uma conciliacao mensal da posicao do contas a receber com registros contabeis."

	AADD(aPontos,{ "000095",STR0366,"FIN","06"})//"Adiantamentos a funcionarios..."
    AADD(aMemos,{"000095",STR0367+STR0368})//"Adiantamentos a funcionarios devem ser efetuado por meio de um formulario apropriado"###" que raga a aprovacao formal dos niveis gerenciais adequados"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Financeiro/15-Despesas de Viagens...    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000096",STR0369,"FIN","15"})//"O acerto de despesas de viagens..."
    AADD(aMemos,{"000096",STR0370+STR0371 })//"O acerto de despesas de viagens dever ser efetuado de imediato, ate tres dias"###" apos termino da viagem."

	AADD(aPontos,{ "000097",STR0372,"FIN","15"})//"A Companhia deve evitar..."
    AADD(aMemos,{"000097",STR0373+STR0374+STR0375})//"A Companhia deve evitar adiantamento de viagens sempre que possivel, adotando o reembolso"###" imediato das despesas apresentadas pelo funcionario,  e incentivando o uso do cartao de credito"### " empresarial ou do proprio funcionario."

	AADD(aPontos,{ "000098",STR0376,"FIN","15"})//"O preenchimento dos Relatorios de Viagens..."
    AADD(aMemos,{"000098",STR0377;
                          + STR0378})//"O preenchimento dos ReLatorios de Viagens e o processamento das despesas de viagens"###" deve seguir os procedimentos emanados da Controladoria e aprovado pela Diretoria."

	AADD(aPontos,{ "000099",STR0379,"FIN","15"})//"Periodicamente, as posicoes de saldos..."
    AADD(aMemos,{"000099",STR0380+STR0381})//"Periodicamente, as posicoes de saldos e despesas de viagens dos funcionarios devem"###" ser reconciliadas e, anualmente, confirmadas por eles, formalmente."

	AADD(aPontos,{ "000100",STR0379,"FIN","15"})//"Periodicamente, as posicoes de saldos..."
    AADD(aMemos,{"000100",STR0380+STR0381})//"Periodicamente, as posicoes de saldos e despesas de viagens dos funcionarios devem"

	AADD(aPontos,{ "000101",STR0382,"FIN","15"})//"E politica da Companhia..."
    AADD(aMemos,{"000101",STR0383+STR0384})//"E politica da Companhia os hoteis a serem usados pelos funcionarios em viagens,"###" a fim de manter acordos com redes hoteleira e extarir dai os descontos possiveis."

	AADD(aPontos,{ "000102",STR0385,"FIN","15"}) //"A emissao de Cartao de Credito..."
    AADD(aMemos,{"000102",STR0386+STR0387})//"A emissao de Cartao de Credito da Companhia para um funcionario deve ser precedida"

	AADD(aPontos,{ "000103",STR0388,"FIN","15"}) //"Deve existir um controle..."
    AADD(aMemos,{"000103",STR0389+STR0390})//"Deve exisitir um controle centralizado sobre a utilizacao de cartoes de credito"### " da Companhia. O Controller tera esta responsabilidade."

	AADD(aPontos,{ "000104",STR0391,"FIN","15"})//"Os cartoes de credito..."
    AADD(aMemos,{"000104",STR0392+STR0393})//"Os cartoes de credito em nome da Companhia nao deverao ser usados para"###" pagamento de contas pessoais, mesmo com reembolso posterior."

	AADD(aPontos,{ "000105",STR0394,"FIN","15"})//"As posicoes individuais..."
    AADD(aMemos,{"000105",STR0395+STR0396})//"As posicoes individuais dos funcionarios, usuarios de cartoes de credito"###" da Companhia, devem ser reconciliadas periodicamente."

	AADD(aPontos,{ "000105",STR0397,"FIN","15"})//"O uso de cartoes de credito..."
    AADD(aMemos,{"000105",STR0398+STR0399+STR0400 })//"O uso de cartoes de credito da Companhia deve ser suportado formalmente"###" por recibos adequados e originais. A baixa da conta do funcionario so deve"###" ocorrer mediante a apresentacao da documentacao e relatorio de despesas."

	AADD(aPontos,{ "000106",STR0401,"FIN","15"})//"Quando do desligamento de um funcionario..."
    AADD(aMemos,{"000106",STR0402+STR0403})//"Quando do desligamento de um funcionario usuario de cartao de credito"###" da Companhia, deve ser exercido controle sobre seu cancelamento previo."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Financeiro/16-Codigo de Etica...        �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000107",STR0404,"FIN","16"})//"A organizacao e seus colaboradores..."
    AADD(aMemos,{"000107",STR0405+STR0406})//"A organizacao e seus colaboradores deverao cumprir todas as exigencias"###" legais aplicaveis aos mais elevados padroes de sociedade em que atuam."

	AADD(aPontos,{ "000108",STR0407,"FIN","16"})//"O uso de receitas da organizacao..."
    AADD(aMemos,{"000108",STR0408+STR0409})//"O uso de recenitas da organizacao para qualquer fim ilegal ou improprio"

	AADD(aPontos,{ "000109",STR0410,"FIN","16"})//"Para nenhum fim sera estabelecido..."
    AADD(aMemos,{"000109",STR0411+STR0412})//"Para nenhum fim sera estabelecido qualquer fundo ou recurso da organizacao"###" que nao seja devidamente regsitrado em seus livros contabeis."

	AADD(aPontos,{ "000110",STR0413,"FIN","16"})//"Em nenhuma circunstacia..."
    AADD(aMemos,{"000110",STR0414+STR0415+STR0416})//"Em nehuma circunstancia serao feitos lancamentos falsos ou ilusorios nos"###" livros contabeis da organizacao, e nenhum funcionario seu sera participante"###" ou conivente com tais atos."
    

	AADD(aPontos,{ "000111",STR0417,"FIN","16"}) //"Nenhum pagamento da organizacao..."
    AADD(aMemos,{"000111",STR0418+STR0419})//"Nenhum pagamento da organizacao dever ser aprovado sem que se encontre"###" apoiado por documentaxao adequada e com o fim especifico q que se propoe"

	AADD(aPontos,{ "000112",STR0420,"FIN","16"})//"Sao obrigatorios o seguimento..."
    AADD(aMemos,{"000112",STR0421+STR0422})//"Sao obrigatorios os eguyimento constante dos principios de contabilidade"###" geralmente aceitos e os controles internos estabelecidos pelos administradores da organizacao."

	AADD(aPontos,{ "000113",STR0423,"FIN","16"})//"Contribuicoes politicas..."
    AADD(aMemos,{"000113",STR0424+STR0425})//"Nenhum recurso ou fundo da organizacao sera usado para contribuir com campanhas"###" politicas, a nao ser o previsto em lei."

	AADD(aPontos,{ "000114",STR0426,"FIN","16"})//"Em nenhuma circunstancia..."
    AADD(aMemos,{"000114",STR0427+STR0428})//"Em nehuma circunstancia qualquer colaborador da organizacao sera reembolsado por despesas"###" com objetivos politicos, direta ou indiretamente."

	AADD(aPontos,{ "000115",STR0429,"FIN","16"})//"O relacionamento da organizacao..."
    AADD(aMemos,{"000115",STR0430+STR0431+STR0432+STR0433+STR0434+STR0435})//"O relacionamento da organizacao com orgaos e pessoal governamental sera em todos"###" os seus aspectos conduzido de modo que o seu conhecimento publico em todos os"###" detalhes nao coloque em risco o conceito e a integridade da organizacao, Assim,"###" nao sera permitido qualquer contribuicao ou pagament a funcionarios do governo,"###" incluindo-se nesta proibicao presentes de valor substancial ou entretenimento"###" incluindo-se nesta proibicao presentes de valor substancial ou entretenimento"###" faustoso, seja direta ou indiretamente."

	AADD(aPontos,{ "000116",STR0436,"FIN","16"})//"A empresa nao efetuara pagamentos..."
    AADD(aMemos,{"000116",STR0437+STR0438})//"A empresa nao efetuara forma de pagamentos ilicitos a agentes fiscais de quaisquer"###" orgaos, de forma direta ou indireta."

	AADD(aPontos,{ "000117",STR0439,"FIN","16"})//"Esta proibicao tambem se aplica..."
    AADD(aMemos,{"000117",STR0440+STR0441+STR0442})//"Esta proibicao tambem se aplica a contribuicoes e pagamentos indiretos feitos"###" por qualquer maneira medinate consultores, assessores, fornecedores, cliente"###" ou quaisquer terceiros."

	AADD(aPontos,{ "000118",STR0443,"FIN","16"})//"A administracao da organizacao..."
    AADD(aMemos,{"000118",STR0444+STR0445+STR0446})//"A administracao da organizacao e suas filiais serao responsaveis pela"###" pela execucao e cumprimento dstas poiliticas, sendo responsaveis pela sua"###" divulgacao em todos os niveis da organizacao."

	AADD(aPontos,{ "000119",STR0447,"FIN","16"})//"Os niveis de Supervisores e Gerentes..."
    AADD(aMemos,{"000119",STR0448+STR0449+STR0450})//"Os niveis de Supervisores e Gerentes, assim como funcionarios em posicoes sensitivas"###" a estes pagamentos, deverao, por escrito, periodica e obrigatoriamente confirmar que"###", segundo suas consciencias, estao cumprindo estas politicas."

	AADD(aPontos,{ "000120",STR0451,"FIN","16"})//"Qualquer colaborador da organizacao..."
    AADD(aMemos,{"000120",STR0452+STR0453+STR0454})//"Qualquer colaborador da organizacao que julgar necessitar de esclarecimentos"###" sobre estas poiliticas devera dirigir-se ao Departamento Juridico da empresa, e"###" discutir o assunto."

	AADD(aPontos,{ "000121",STR0455,"FIN","16"})//"Qualquer violacao destas politicas..."
    AADD(aMemos,{"000121",STR0456+STR0457})//"Qualquer violacao desta politicas deve de imediato ser notificada ao"###" Departamento Juridico da empresa."

	AADD(aPontos,{ "000122",STR0458,"FIN","16"})//"A empresa tomara acao disciplinar..."
    AADD(aMemos,{"000122",STR0459+STR0460})//"A empresa tomara acao dsciplinar apropriada, incluindo-se a demissao"###" por justa causa, contra todos os envolvidos em quaisquer violacoes desta politicas."

	AADD(aPontos,{ "000123",STR0461,"FIN","16"})//"A organizacao nao tera..."
    AADD(aMemos,{"000123",STR0462+STR0463})//"A organizacao nao tera, em suas posicoes internas, quaisquer consultores ou"###" ou funcionarios do governo."

	AADD(aPontos,{ "000124",STR0464,"FIN","16"})//"Todas as contas e depositos..."
    AADD(aMemos,{"000124",STR0465+STR0466})//"Todas as contas e depositos mantidos no estrangeiro serao claramente"###" identificados nos livros e registros contabeis da empresa."
                        
	AADD(aPontos,{ "000125",STR0467,"FIN","16"})//"Todos os recebimentos em dinheiro..."
    AADD(aMemos,{"000125",STR0468+STR0469+STR0470})//"Todos os recebimentos em dinheiro pela organizacao no estrangeiro serao"###" prontamente contabilizados em nome da empresa e depositados em conta"###" bancaria autorizada pela Diretoria Financeira."

	AADD(aPontos,{ "000126",STR0471,"FIN","16"})//"Todos os pagamentos efetuados..."
    AADD(aMemos,{"000126",STR0472+STR0473+STR0474})//"Todos os pagamentos efetuados no estrangeiro deverao ser feitos de maneira"###" que o anuncio de quaisquer detalhes nao cause constrangimentos ou ponha"###" em duvida a integridade e reputacao da organizacao."

	AADD(aPontos,{ "000127",STR0475,"FIN","16"})//"Sera fornecida copia desta..."
    AADD(aMemos,{"000127",STR0476+STR0477})//"Sera fornecida copia desta politica a cada colaborador da organizacao"###" acima dos niveis de Supervisao e aos que estejam especificamente expostos a ela"

	AADD(aPontos,{ "000128",STR0478,"FIN","16"})//"Anualmente, eles deverao assinar..."
    AADD(aMemos,{"000128",STR0479+STR0480})//"Anualmente, eles devarao assinar declaracao de que nao tem conhecimento"###" de qualquer violacao a respeito." 

	AADD(aPontos,{ "000129",STR0481,"FIN","16"})//"E responsabilidade de cada um..."
    AADD(aMemos,{"000129",STR0482+STR0483})//"Anualmente, eles devarao assinar declaracao de que nao tem conhecimento"###"E responsabilidade de cada um dos funcionarios assegurarem a divulgacao"###" apropriada e o cumprimento desta politica."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Faturamento/17-Expedicao...             �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000130",STR0484,"FAT","17"})//"A expedicao deve ser suportada..."
    AADD(aMemos,{"000130",STR0485+STR0486})//"A expedicao deve ser suportada pela documentacao adequada para"###" efetuar o embarque e despacho das mercadorias."  	

	AADD(aPontos,{ "000131",STR0487,"FAT","17"})//"Os funcionarios da Expedicao..."
    AADD(aMemos,{"000131",STR0488+STR0489})//"Os funcionarios da Expedicao nao devem ter acesso aos estoques da Companhia"###" ou ter quaisquer atribuicoes de faturamento e emissao das notas fiscais."

	AADD(aPontos,{ "000132",STR0490,"FAT","17"})//"Deve haver evidencia..."
    AADD(aMemos,{"000132",STR0491+STR0492})//"Deve haver evidencia de checagem das mercadorias expedidas com os"###" dados dos pedidos e notas fiscais."

	AADD(aPontos,{ "000133",STR0493,"FAT","17"})//"O embarque de mercadorias..."
    AADD(aMemos,{"000133",STR0494+STR0495+STR0496})//"O embarque de mercadorias deve ser observado atentamente por funcionarios"###" da Companhia, e checagem adequada deve existir, afim de confirmar que toda"###" a mercadoria embarcada esteja suportada pela devida nota fiscal."

	AADD(aPontos,{ "000133",STR0493,"FAT","17"})//"O embarque de mercadorias..."
    AADD(aMemos,{"000133",STR0494+STR0495+STR0496})//"O embarque de mercadorias deve ser observado atentamente por funcionarios"###" da Companhia, e checagem adequada deve existir, afim de confirmar que toda"###" a mercadoria embarcada esteja suportada pela devida nota fiscal."

	AADD(aPontos,{ "000134",STR0497,"FAT","17"})//"A consolidacao de cargas..."
    AADD(aMemos,{"000134",STR0498+STR0499+STR0500})//"A consolidacao de cargas, a fim de diminuir o custo dos fretes, deve ser"###" considerada sempre que possivel. A otimizacao da utilizacao do espaco"### " dos caminhoes deve sempre ser considerada no seu carregamento."

	AADD(aPontos,{ "000135",STR0501,"FAT","17"})//"O transporte dos produtos..."
    AADD(aMemos,{"000135",STR0502+STR0503+STR0504+STR0505})//"O transporte dos produtos, quando o relacionamento com as companhias"###" transportadoras e firme e cativo, deve ser objeto de instrumento"###" contratual entre as partes, devendo ser efetuadas cotacoes periodicas"###" pelo Departamento de Compras."

	AADD(aPontos,{ "000136",STR0506,"FAT","17"})//"Comprovante do recebimento..."
    AADD(aMemos,{"000136",STR0507+STR0508+STR0509})//"Comprovante do recebimento das mercadorias para transporte, por parte"### " das companhias transportadoras, deve ser sempre obtido na documentacao"### " apropriada (canhoto)."

	AADD(aPontos,{ "000137",STR0510,"FAT","17"})//"Os fretes so devem ser pagos..."
    AADD(aMemos,{"000137",STR0511+STR0512+STR0513})//"Os fretes so devem ser pagos apos a aprovacao pelo Departamento usuario."###" Adequadas informacoes a respeito do veiculo e a assinatura do condutor devem"###" ser sempre coletadas nos Pedidos de Faturamento."

	AADD(aPontos,{ "000138",STR0514,"FAT","17"})//"O ambiente fisico da area..."
    AADD(aMemos,{"000138",STR0515+STR0516})//"O ambiente fisico da area de expedicao deve estar devidamente segregado"###" das outras areas."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Faturamento/18-Faturamento...           �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000139",STR0517,"FAT","18"})//"Todo o faturamento de produtos..."
    AADD(aMemos,{"000139",STR0518+STR0519})//"Todo o faturamento de produtos e servcios deve estar suportado pelo"###" formulario Pedido de Faturamento, devidamente preenchido."

	AADD(aPontos,{ "000140",STR0520,"FAT","18"})//"Os pedidos de Faturamento..."
    AADD(aMemos,{"000140",STR0521+STR0522})//"Os pedidos de Faturamento deverao ser assinados pelos niveis adequados"###" e previamente aprovados pelo Departamento de Credito e Cobranca."

	AADD(aPontos,{ "000141",STR0523,"FAT","18"})
    AADD(aMemos,{"000141",STR0524+STR0525+STR0526})//"Os pedidos de Faturamento de produtos que nao sejam os normalmente"###" comercializados pela Companhia, como a venda de Ativos, devem trazer"###" a aprovacao da Controaldoria."

	AADD(aPontos,{ "000142",STR0527,"FAT","18"})//"O numero sequencial do pedido..."
    AADD(aMemos,{"000142",STR0528+STR0529})//"O numero sequencial do pedido de Faturamento deve constar na fatura e"###" e vice-versa, para referencia."

	AADD(aPontos,{ "000143",STR0520,"FAT","18"})////"Os pedidos de Faturamento..."
    AADD(aMemos,{"000143",STR0530+STR0531+STR0532})//"Os Pedidos de Faturamento devem evidenciar a construcao dos precos"###" finais das mercadorias e a concessao de quaisquer descontos com a"###" devida aprovacao dos niveis adequados"

	AADD(aPontos,{ "000144",STR0533,"FAT","18"})//"Os preceitos legais devem..."
    AADD(aMemos,{"000144",STR0534+STR0535+STR0536})//"Os preceitos legais devem ser estritamente obedecidos quando da"###" emissao de Notas Fiscais e faturas, por pessoal treinado e com"###" conhecimento tecnico suficiente."

	AADD(aPontos,{ "000145",STR0537,"FAT","18"})//"As Notas Fiscais/Faturas depois..."
    AADD(aMemos,{"000145",STR0538+ STR0539+STR0540})//"As Notas Fiscais/Faturas depois de emitidas devem ser conferidas"###" emissao de Notas Fiscais e faturas, por pessoal treinado e com"###" por outra pessoa, e esta conferencia evidenciada."

	AADD(aPontos,{ "000146",STR0541,"FAT","18"}) //"Estrito controle sequencial..."
    AADD(aMemos,{"000146",STR0542+STR0543})//"Estrito controle sequencial numerico deve ser exercido sobre os"###" arquivos de Notas Fiscais/Faturas emitidas, para cada serie."
 
	AADD(aPontos,{ "000147",STR0544,"FAT","18"})//"Os estoques em branco de..."
    AADD(aMemos,{"000147",STR0545+STR0546+STR0547})//"Os estoques em brano de Notas Fiscais/Faturas devem ser protegidos"###" de acesso nao autorizado, e o seu uso deve obedecer estritamente a"###" sua ordem sequencial."

	AADD(aPontos,{ "000148",STR0548,"FAT","18"})//"Os canhotos destacaveis..."
    AADD(aMemos,{"000148",STR0549+STR0550+STR0551})//"Os canhotos destacaveis, comprovantes da efetiva entrega das mercadoria"###" devem ser coletados e mantidos em ordem numerica. De preferencia, devem"###" ser colados na copia da referida nota fiscal, no arquivo sequencial."

	AADD(aPontos,{ "000149",STR0552,"FAT","18"})//"Deve haver um limite minimo..."
    AADD(aMemos,{"000149",STR0553+STR0554+STR0555})//"Deve haver sempre um limite minimo de valor para se efetuar um faturamento,"###" de acordo com as determinacoes da Diretoria. Excecoes a esta regra devem"###" ser tratadas com bom senso."

	AADD(aPontos,{ "000150",STR0556,"FAT","18"})//"Procedimentos adequados de corte..."
    AADD(aMemos,{"000150",STR0557+STR0558+STR0559})//"Procedimentos adequados de corte devem ser seguidos no fechamento do mes,"###" devendo as mercadorias faturadas ate aquela data serem efetivamente expedidas"###" e embarcadas."

	AADD(aPontos,{ "000151",STR0560,"FAT","18"})//"Deve haver suporte legal..."
    AADD(aMemos,{"000151",STR0561+STR0562+STR0563+STR0564+STR0565}) //"Deve haver suporte legal para o cancelamento de notas fiscais."###" Todas as vias das NFs canceladas devem estar em arquivo. No corpo de cada"###" NF cancelada deve ser aposto carimbo que evidencie aprovacao do cancelamento"###" pelo nivel gerencial, data e motivo. Para cancelamentos fora do mes deve haver"###" procedimentos legais quanto ao ressarcimento dos impostos."

	AADD(aPontos,{ "000152",STR0566,"FAT","18"})//"As atividades de faturamento..."
    AADD(aMemos,{"000152",STR0567+STR0568+STR0569})//"As atividades de fatiramento nao devem ser gargalo que puna o cliente."###" O faturamento devera ser efetaudo prontamente e ao longo de todas as horas"###" comerciais, se assim as necessidades dos clientes requeiram."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Compras/09-Compras...                   �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000153",STR0570,"COM","07"})//"A funcao do compras e definida..."
    AADD(aMemos,{"000153",STR0571+STR0572+STR0573})//"A funcao do compras e definida como obtencao dos recurso necessarios pelo"###" melhor preco, na melhor quantidade, no tempo certo e no lugar certo,"###" observando-se as exigencias de qualidade."

	AADD(aPontos,{ "000154",STR0574,"COM","07"})//"Todas as compras da Companhia..."
    AADD(aMemos,{"000154",STR0575+STR0576+STR0577+STR0578+STR0579+STR0580})//"Todas as compras da Companhia, exceto das de pequenos itens pagos"###" pelo fundos de caixa devem ser efetuadas com a emissao de uma ordem"###" de compras formal. Cada ordem de compra deve ser suportada por uma"###" requisicao de material devidamente aprovada por pessoas autorizadas."###" Excecoes a esta regra devem ser expressamente aprovadas por escrito"###" pela Diretoria."

	AADD(aPontos,{ "000155",STR0581,"COM","07"})//"A autoridade de assinar..."
    AADD(aMemos,{"000155",STR0582+STR0583+STR0584})//"A autoridade de assinar uma ordem de compra deve estar formalizada"###" e obedecer a limites de valor, conforme os niveis hierarquicos, e de"###" acordo com as determinacoes da Diretoria."
                        
	AADD(aPontos,{ "000156",STR0585,"COM","07"})//"Uma ordem de compra..."
    AADD(aMemos,{"000156",STR0586+STR0587})//"Uma ordem de compra nao deve nunca ser emitida apos o fato ter-se consumado"###", ou seja, apos a compra ja ter sido efetuada, e sim, sempre, previamente."
                          
	AADD(aPontos,{ "000157",STR0588,"COM","07"})//"No minimo tres cotacoes..."
    AADD(aMemos,{"000157",STR0589+STR0590})//"No minimo tres cotacoes de diferentes forncedores devem ser obtidas, para"//" compras em que os valores e quantidades envolvidas sejam substanciais."

	AADD(aPontos,{ "000158",STR0591,"COM","07"})//"A coleta de novas cotacoes..."
    AADD(aMemos,{"000158",STR0592+STR0593})//"A coleta de novas cotacoes de precos de itens de compra continua deve ser"### " efetuada de acordo com a periodicidade estabelicida pela Diretoria da area."

	AADD(aPontos,{ "000159",STR0594,"COM","07"})//"Os processos de selecao de cotacoes..."
    AADD(aMemos,{"000159",STR0595+ STR0596+ STR0597})//"Os processos de selecao de cotacoes devem ser documentados e arquivados de"###" maneira adequada para eventual revisao posterior pela administracao e"###" e auditoria interna."

	AADD(aPontos,{ "000160",STR0598,"COM","07"})//"Decisoes de escolher outro fornecedor..."
    AADD(aMemos,{"000160",STR0599+ STR0600})//"Decisoes de escolher outro forncedor que nao o mais competitivo devem"###" ser suportadas e aprovadas devidamente."

	AADD(aPontos,{ "000161",STR0601,"COM","07"})//"Os contatos com fornecedores..."
    AADD(aMemos,{"000161",STR0602+ STR0603+ STR0604})//"Os contatos com fornecedores de materiais, tanto diretos como indiretos,"###" devem ser feitos pelo pessoal de compras, e nunca diretamente pelos"###" departamentos usuarios."

	AADD(aPontos,{ "000162",STR0605,"COM","07"})//"Adequado controle sequencial..."
    AADD(aMemos,{"000162",STR0606+ STR0607+ STR0608})//"Adequado controle sequencial decver ser exercido no arquivamento de"###" Requisicoes e Ordens de Compra. Tais formularios devem ser pre numerados"###" e um controle apropriado exercido sobre os estoques em branco."

	AADD(aPontos,{ "000163",STR0609,"COM","07"})//"Deve existir um sistema adequado..."
    AADD(aMemos,{"000163",STR0610+STR0611 })//"Deve existir um sistema adequado que registre os recebimentos parciais"###" nas ordens de compras em aberto, dando baixa e trazendo o saldo atualizado."

	AADD(aPontos,{ "000164",STR0612,"COM","07"})//"As vantagens de fabricar ou comprar..."
    AADD(aMemos,{"000164",STR0613+STR0614 })//"As vantagens de fabricar ou comprar fora devem ser sempre avaliadas pelo"###" pessoal de suprimentos mediante estudos formais de precos."

	AADD(aPontos,{ "000165",STR0615,"COM","07"})//"Os ajustes de precos..."
    AADD(aMemos,{"000165",STR0616+ STR0617+ STR0618})//"Os ajustes de precos solicitiados pelos fornecedores devem ser previamente"###" analisados e aprovados pelos niveis competentes. Relatorios mensais devem"###" ser extraidos do sistema mostrando as alteracoes de preco do periodo."

	AADD(aPontos,{ "000166",STR0619,"COM","07"})//"A gerencia de compras devera..."
    AADD(aMemos,{"000166",STR0620+ STR0621+ STR0622})//"A gerencia de compras devera certificar-se de que foram processados"###" somente ajustes de preco devida e previamente aprovados. Estes relatorios"###" devem trazer evidencia de revisao (visto do Gerente), e devem ser arquivados."

	AADD(aPontos,{ "000167",STR0619,"COM","07"})//"A gerencia de compras devera..."
    AADD(aMemos,{"000167",STR0620+ STR0621+ STR0622})//"A gerencia de compras devera certificar-se de que foram processados"###" somente ajustes de preco devida e previamente aprovados. Estes relatorios"###" devem trazer evidencia de revisao (visto do Gerente), e devem ser arquivados."

	AADD(aPontos,{ "000168",STR0623,"COM","07"})//"As diferencas entre as condicoes..."
    AADD(aMemos,{"000168",STR0624+ STR0625+ STR0626})//"As diferencas entre as condicoes efetivamente negociadas com os fornecedores"###" e as condicoes reais das compras devem ser devidamente analisadas pelos"###" niveis apropriados, e acao corretiva cabivel deve ser tomada."

	AADD(aPontos,{ "000169",STR0627,"COM","07"})//"A gerencia de Compras devera analisar as diferencas de preco, quantidade..."
    AADD(aMemos,{"000169",STR0624+STR0628})//"A diferencas entre as condicoes efetivamente negociadas com os fornecedores"### " adequados obtidos do sistema. Tal analise devera ser evienciada pelo visto do gerente."

	AADD(aPontos,{ "000170",STR0629,"COM","07"})//"Antes de pagas, as contas de fretes..."
    AADD(aMemos,{"000170",STR0630+ STR0631})//"Antes de pagas, as contas de fretes sobre compras devem ser analisadas adequadamente pelo"###" departamento, a fim de verificar sua exatidao em confronto com as condicoes controladas."
                        
	AADD(aPontos,{ "000171",STR0632,"COM","07"})//"A inclusao ou exclusao..."
    AADD(aMemos,{"000171",STR0633+ STR0634})//"A inclusao ou exclusao de um fornecedor no arquivo de fornecedores autorizados deve"###" efetuar-se por  meio de um processo formal devidamente aprovado pela Gerencia de Compras."

	AADD(aPontos,{ "000172",STR0635,"COM","07"})//"Dentro das possibilidades..."
    AADD(aMemos,{"000172",STR0636+ STR0637})//"Dentro das possibilidades deve exisitir um rodizio dos compradores em suas varias"###" areas de compras, no departamento."

	AADD(aPontos,{ "000173",STR0638,"COM","07"})//"Toda a alteracao de dados..."
    AADD(aMemos,{"000173",STR0639+ STR0640})//"Toda a aletaracao de dados das ordens de compra deve ser formalmente aprovadas"###" pelos niveis hierarquicos adequados, antes de efetivada."

	AADD(aPontos,{ "000174",STR0641,"COM","07"})//"A funcao de desenvolvimento..."
    AADD(aMemos,{"000174",STR0642+ STR0643})//"A funcao de desenvolvimento de novos fornecedores deve questionar continuamente"###" a possibilidade de fornecedores alternativos."

	AADD(aPontos,{ "000175",STR0644,"COM","07"})//"Nao devem ser permitidas..."
    AADD(aMemos,{"000175",STR0645+STR0646})///"Nao devem ser permitidas compras de itens pessoais, para uso dos funcionarios,"###" pelos canais de compras da Companhia."

	AADD(aPontos,{ "000176",STR0647,"COM","07"})//"O mais alto padrao etico..."
    AADD(aMemos,{"000176",STR0648+ STR0649+ STR0650})//"O mais alto padrao etico dever ser observado no relacinamento com os"###" fornecedores. O recebimento de brindes e favores dos fornecedores e"###" terminantemente proibido."

	AADD(aPontos,{ "000177",STR0651,"COM","07"})//"Todos os compradores da Companhia..."
    AADD(aMemos,{"000177",STR0652+ STR0653})//"Todos os compradores da Companhia devem obrigatoriamente assinar"###" Declaraco de Conflioto de Interesse e o Codigo de Etica da Companhia."
          
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Contabilidade/11-Contabilidade          �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000178",STR0654,"CTB","11"})//"As transacoes contabeis..."
    AADD(aMemos,{"000178",STR0655+ STR0656})//"As transacoes contabeis devem ser executadas de acordo com as"###" autorizacoes da administracao."
          
	AADD(aPontos,{ "000179",STR0657,"CTB","11"})//"As transacoes devem ser registradas..."
    AADD(aMemos,{"000179",STR0658+ STR0659+ STR0660+ STR0661})//"As transacoes devem ser registradas necessariamente a fim de permitir o"###" preparo e elaboracao dos relatorios contabeis de acordo com os principios"###" contabeis ou outros criterios aplicaveis a estes relatorios, e manter controle"###" sobre os ativos da empresa."

	AADD(aPontos,{ "000180",STR0662,"CTB","11"})//"O acesso aos ativos da empresa..."
    AADD(aMemos,{"000180",STR0663+ STR0664})//"O acesso aos ativos da empresa seja permitido somente de acordo com a aprovacao"###" e autorizacao da administracao."
                        
	AADD(aPontos,{ "000181",STR0665,"CTB","11"})//"Os regitros contabeis dos ativos..."
    AADD(aMemos,{"000181",STR0666+ STR0667+ STR0668})//"Os registros contabeis dos ativos sejam periodicamente comparados com sua"###" existencia fisica e que sejam efetuados os ajustes necessarios, a respeito"###" das diferencas."
                        
	AADD(aPontos,{ "000182",STR0669,"CTB","11"})//"As operacoes do Departamento de Contabilidade..."
    AADD(aMemos,{"000182",STR0670+ STR0671+ STR0672})//"As operacoes do Departamento de Contabilidade devem estar suportadas por um"###" Manual de Procedimentos Contabeis em que sejam especificados o uso adequado"###" das contas e o registro das transacoes."

	AADD(aPontos,{ "000183",STR0673,"CTB","11"})//"Um plano de contas completo..."
    AADD(aMemos,{"000183",STR0674+STR0675+STR0676+STR0677}) //"Um plano de contas completo e atualizado dever ser mantido de acordo com os"###" principios de contabilidade. Uma descricao dos itens a serem lancados nas contas"###" deve existir, afim de promover a consistencia dos registros. O nome das contas"###" deve expressar ao maximo e claramente a sua natureza."})
                        
	AADD(aPontos,{ "000184",STR0678,"CTB","11"})//"O software de sistema integrado (ERP)..."
    AADD(aMemos,{"000184",STR0679+STR0680+STR0681+STR0744})//"O software de sistema integrado (ERP) adotado pela empresa deve, primeiro, preencher"###" todos os requisitos dos principios de contabilidade e legislacao locais."###" para apos, mediante ajsutes, tambem preencher os requisitos dos principos"###" de contabilidade da Matriz, se no exterior."

	AADD(aPontos,{ "000185",STR0682,"CTB","11"})//"Dentro do departamento de Contabilidade..."
    AADD(aMemos,{"000185",STR0683+STR0684+STR0685})//"Dentro do departamento de Contabilidade e Tesouraria, a segregacao das funcoes de registro,"###" lancamentos e reconciliacoes das transacoes e a custodia das propriedades da Companhia e"###" fundamental a salvaguardas dos ativos."

	AADD(aPontos,{ "000186",STR0686,"CTB","11"})//"O acesso aos livros e registros..."
    AADD(aMemos,{"000186",STR0687+STR0688})//"O acesso aos livros e registros contabeis deve ser limitado as pessoas cujas"###" responsabilidades assim o requeiram."

	AADD(aPontos,{ "000187",STR0689,"CTB","11"})//"As decisoes sobre cobertura..."
    AADD(aMemos,{"000187",STR0690+STR0691})//"As deciscoes sobre cobertura de seguros devem ser de responsabilidade dos niveis"###" gerenciais adequados, de acordo com as politicas da Companhia."

	AADD(aPontos,{ "000188",STR0692,"CTB","11"})//"Os lancamentos contabeis devem..."
    AADD(aMemos,{"000188",STR0693+STR0694+STR0695+STR0696+STR0698})//"Os lancamentos contabeis devem ser padronizados no tocante ao seu"###" conteudo e identificacao. Eles devem ser explicitos e suportados por dados"###" prontamente identificaveis. Todos os lancamentos contabeis devem ser revisados"###" e aprovados de acordo com os niveis de responsabilidade pre-estabelecidos,"###" obedecendo a uma adequada separacao das funcoes."
                        
	AADD(aPontos,{ "000189",STR0699,"CTB","11"})//"Os relatorios financeiros..."
    AADD(aMemos,{"000189",STR0700+STR0701+STR0702+STR0703+STR0704})//"Os relatorios finaceiros emitidos periodicamente devem ser suficientemente"###" informativos e detalhados, a fim de trazer a luz flutuacoes anormais nas areas"###" importantes da Companhia. A comparaco dos resultados periodicos aos dados"###" prontamente identificaveis. Todos os lancamentos contabeis devem ser revisados"###" previamente orcados deve ser efetuada pelos proprios departamentos."

	AADD(aPontos,{ "000190",STR0705,"CTB","11"})//"As contas com a Matriz..."
    AADD(aMemos,{"000190",STR0706})//"As contas com a Matriz e as co-irmas devem ser reconciliadas mensalmente (Intercompany)."
    
	AADD(aPontos,{ "000191",STR0707,"CTB","11"})//"Os lancamentos devem..."
    AADD(aMemos,{"000191",STR0708+STR0709})//"Os lancamentos devem estar suportados por doucmentacao original adequada."###" Deve haver evidencia do processamento em todos os documentos (ex. carimbo LANCADO)."

	AADD(aPontos,{ "000192",STR0710,"CTB","11"})//"A Gerencia de Contabilidade devera..."
    AADD(aMemos,{"000192",STR0711+STR0712})//"A Gerencia de Contabilidade devera dispor de uma listagem a fim de controlar"###" o processamento de lancamentos contabeis-chave a serem efetuados mensalmente."

	AADD(aPontos,{ "000193",STR0713,"CTB","11"})//"O formulario de resumo dos lancamentos..."
    AADD(aMemos,{"000193",STR0714+STR0715})//"O formulario de resumo dos lancamentos deve trazer adequadamente"###" as aprovacoes e vistos necessarios quando do processamento."

	AADD(aPontos,{ "000194",STR0716,"CTB","11"})//"Deve existir evidencia..."
    AADD(aMemos,{"000194",STR0717})//"Deve existir evidencia sobre a analise e conciliacao mensal das contas."
    
	AADD(aPontos,{ "000195",STR0718,"CTB","11"})//"A numeracao automatica..."
    AADD(aMemos,{"000195",STR0719+STR0720})//"A numeracao automatica de lancamentos contabeis efetuada pelo sistema"###"deve ser objeto de controles adequados a fim de prevenir a sua duplicidade."
                       
	AADD(aPontos,{ "000196",STR0721,"CTB","11"})//"Os livros fiscais devem ser periodicamente..."
    AADD(aMemos,{"000196",STR0722+STR0723})//"Os livros fiscais devem ser periodicamente reconciliados com registros"###" contabeis e os ajustes devem ser efetuados dentro do periodo em questao."

	AADD(aPontos,{ "000196",STR0721,"CTB","11"})//"Os livros fiscais devem ser periodicamente..."
    AADD(aMemos,{"000196",STR0722+STR0723})//"Os livros fiscais devem ser periodicamente reconciliados com registros"###" contabeis e os ajustes devem ser efetuados dentro do periodo em questao."

	AADD(aPontos,{ "000197",STR0724,"CTB","11"})//"As conversoes dos demonstrativos..."
    AADD(aMemos,{"000197",STR0725+STR0726})//"As conversoes dos demonstrativos financeiros para outras moedas devem"###" seguir os procedimentos geralmente aceitos (FASB 08,52,95 e 109,IAS,etc.)"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Contabilidade/11-Controle Orcamentario... �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000198",STR0727,"CTB","12"})//"E responsabilidade da gerencia..."
    AADD(aMemos,{"000198",STR0278+STR0729+STR0730})//"E responsabilidade da gerencia de cada Departamento certificar-se de que"###" os lancamentos de despesas ao seu Departamento sao realmente de sua competencia"###" A analise efetuada deve ser evidenciada de forma apropriada."

	AADD(aPontos,{ "000199",STR0731,"CTB","12"})//"Os pedidos de lancamentos..."
    AADD(aMemos,{"000199",STR0732+STR0733})//"Os pedidos de lancamentos de reclassificacao de despesas pelos departamentos"###" devem ser emitidos por escrito e aprovados no minimo pelo nivel de supervisao."
                       
	AADD(aPontos,{ "000200",STR0734,"CTB","12"})//"A analise do excesso de despesas..."
    AADD(aMemos,{"000200",STR0735+STR0736})//"A analise do excesso de despesas em comparacao ao orcado deve ter efetuada e"###" investigada sua causa, para acoes corretivas cabiveis e obtencao de base de informacoes."

	AADD(aPontos,{ "000201",STR0737,"CTB","12"})//"E de responsabilidade da gerencias..."
    AADD(aMemos,{"000201",STR0738+STR0739})//"E de responsabilidade da gerencias departamentais a analise das possibilidades de"###" reducao de custos dentro de seus departamentos."
                       
	AADD(aPontos,{ "000202",STR0740,"CTB","12"})//"As contas telefoncias devem..."
    AADD(aMemos,{"000202",STR0741+STR0742+STR0743 })//"As contas telefoncias devem ser analisadas pelos departamentos usuarios. O uso dos"###" telefones e internet dever ser restrito aos assuntos de interesse da Companhia,"###" e controlado efetivamente."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//�  Pontos de Controle/Estoque Custo/14-Custos...              �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPontos,{ "000203",STR0745,"EST","14"})//"Preenche os requisitos exigidos..."
    AADD(aMemos,{"000203",STR0746+STR0747})//"Preenche os requisitos exigidos pelas leis nacionais e pelos principios de"###" contabilidade geralmente aceitos."
                       
	AADD(aPontos,{ "000204",STR0748,"EST","14"})//"Fornece dados consistentes..."
    AADD(aMemos,{"000204",STR0749+STR0750})//"Fornece dados consistentes na determinacao dos valores do inventario"###" dos resultados da Companhia, e para a elaboracao de relatorios gerenciais."

	AADD(aPontos,{ "000205",STR0751,"EST","14"})//"Fornece dados consistentes e ageis..."
    AADD(aMemos,{"000205",STR0752+STR0753})//"Fornece dados consistentes e ageis, para as areas de marketing e vendas,"###" para determinacao do precos dos produtos finais."

	AADD(aPontos,{ "000206",STR0754,"EST","14"})//"Registra e fornece dados..."
    AADD(aMemos,{"000206",STR0755+STR0756})//"Registra e fornece dados para um adequado controle dos custos e para a"###" a tomada de decisoes na otimizacao da utilizacao dos recursos existentes."

	AADD(aPontos,{ "000207",STR0757,"EST","14"})//"O sistema de custos deve ser integrado..."
    AADD(aMemos,{"000207",STR0758})//"O sistema de custos deve ser integrado com a contabilidade da empresa."
    
	AADD(aPontos,{ "000208",STR0759,"EST","14"})//"O sistema deve oferecer aos orgaos..."
    AADD(aMemos,{"000208",STR0760+STR0761})//"O sistema deve oferecer aos orgaos governamentais dados consistentes para"###" pleitear possiveis aumentos de precos."
                         
	AADD(aPontos,{ "000209",STR0762,"EST","14"})//"Os metodos de avaliacao de custos..."
    AADD(aMemos,{"000209",STR0763+STR0764+STR0765})//"Os metodos de avaliacao de custos devem ser definidos pela Controladoria"###" e seguirao as determinacoes da administracao em conjunto com as normas"###" da Matriz, e em sintonia com legislacao legal."
             		                                                                                     
 	Processa( { | lEnd | CriaICQ(aPontos,aMemos)} ,STR0766,STR0767)//"Aguarde!"###"Gerando registros de pontos de controle..."
 	
EndIf
Return
         
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaICQ    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em ICG com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaICQ(ExpA1,ExpA2,ExpA3)						      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aPontos			 					      潮�
北�          � ExpA2 := Array aMemos			    	  				  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaICQ(aPontos,aMemos)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aPontos) )
     	                                                                           
For nI := 1 To Len(aPontos)

		IncProc()

		DbSelectArea("ICQ")
		If ! DbSeek(xFilial("ICQ")+aPontos[nI][1])
		    RecLock("ICQ",.T.)
		    Replace ICQ_PONTO		With aPontos[nI][1]
		    Replace ICQ_DESC     	With aPontos[nI][2]
		    Replace ICQ_MODULO		With aPontos[nI][3]
		    If Len(aPontos[nI]) > 3
			    Replace ICQ_TOPICO		With aPontos[nI][4]
			EndIf		    
		    If ! Empty(aMemos[nI][2]) 
			   	MSMM(ICQ->ICQ_CODDES,,,aMemos[nI][2],1,,,"ICQ","ICQ_CODDES")
				ConfirmSX8()		
			EndIf
			MsUnLock()
		Endif
Next nI
RestArea( aArea )	
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILIC6   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela pontos de controle...                               潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilIC6()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilIC6()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local cQuest := ""
Local aFuncoes := {}
Local aDoctos := {}



If lCria .and. IC5->IC5_GESTAO == "2"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria questionarios e perguntas para a Gestao Sarbanes-Oxley �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�

	cQuest := GetSXENum("IC6","IC6_CODQT")	
	AADD(aQuest,  {cQuest,STR0768})//"PROCESSO DE COMPRAS"
	AADD(aFuncoes,{cQuest,STR0769+STR0770})//"Aquisicao de propriedades, mao de obra;Bens e servicos;Pagamento das compras;"###" Classificacao, preparo de controles, e informacao do que foi comprado e do que foi pago"
	AADD(aDoctos, {cQuest,STR0771+STR0772+STR0773+STR0774+STR0775+STR0776})//" Requisicao de compras;Ordem de Compras;Relatorios de despesas;Relatorio de Recebimentos"###" Nota fiscal do fornecedor;Cartas de correcao de notas fiscais"###" requisicoes e copias de cheques;resumos contabeis (vouchers);autorizacao para despesas"###	" autorizacoes de gastos de capital;Documentacao legal do departamento de pessoal"###" Registros de ponto;autorizacoes para horas extras;Relatorios de folha de pagamento"###" Notas de debito/credito;Romaneios;Relatorios da portaria"
	
	cQuest := GetSXENum("IC6","IC6_CODQT")		
	AADD(aQuest,  {cQuest,STR0777})//"PROCESSO DE VENDAS"
	AADD(aFuncoes,{cQuest,STR0778+STR0779+STR0780+STR0781+STR0782+STR0783})//"Concessao de credito;Adminstracao e controle dos pedidos de clientes;"###" Faturamento;Expedicao;Controle e administracao de comissoes"###" Controle e administracao de garantias;Contas a receber;Cobranca e recebimentos de caixa"###" Correcao de notas fiscais;Custos e formacao de precos;Vendas e sua administracao"###" Recebimentos de caixa;Provisoes para devedores duvidosos;Baixa de contas a receber"###" Cobrancas judiciais;Administracao tributaria;Provisoes para despesas com garantias"
	AADD(aDoctos, {cQuest,STR0771+STR0772+STR0773+STR0774+STR0775+STR0776})//" Requisicao de compras;Ordem de Compras;Relatorios de despesas;Relatorio de Recebimentos"###" Nota fiscal do fornecedor;Cartas de correcao de notas fiscais"###" requisicoes e copias de cheques;resumos contabeis (vouchers);autorizacao para despesas"###	" autorizacoes de gastos de capital;Documentacao legal do departamento de pessoal"###" Registros de ponto;autorizacoes para horas extras;Relatorios de folha de pagamento"###" Notas de debito/credito;Romaneios;Relatorios da portaria"

	cQuest := GetSXENum("IC6","IC6_CODQT")	
	AADD(aQuest,  {cQuest,STR0784})//"PROCESSO DE PRODUCAO"
	AADD(aFuncoes,{cQuest,STR0785+STR0786+STR0787})//"Controle da producao;Contabilidade de custos;"###" Administracao de estoques;Inventarios"###" Manutencao de Ativos"
	AADD(aDoctos, {cQuest,STR0788+STR0789+STR0790+STR0791+STR0792})//"Controles de Ponto;Requisicao de materiais;Ordens de Producao"###" Controles de sucata;Mapas de custeio"###	" Autorizacoes de ajuste de inventarios"###" Autorizacoes para desepesas de capital"###	" Autorizacao de movimentacao de ativos"
	
	Processa( { | lEnd | CriaIC6(aQuest,aFuncoes,aDoctos)} ,STR0766,STR0767)//"Aguarde!"###"Gerando registros de pontos de controle..."
 	
EndIf
Return
			
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaIC6    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em IC6 com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaIC6(ExpA1,ExpA2,ExpA3)						      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aQuest/Questoes pre definidas.   		      潮�
北�          � ExpA2 := Array aFuncoes/Nome do Sub-processos-rotinas.	  潮�
北�          � ExpA3 := Array aDoctos/Nome dos relarorios obrigatorios.	  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaIC6(aQuest,aFuncoes,aDoctos)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aQuest) )
     	                                                                           
For nI := 1 To Len(aQuest)

	IncProc()

	DbSelectArea("IC6")
	DbSetOrder(1)
	If ! DbSeek(xFilial("IC6")+IC5->IC5_CODPQ+aQuest[nI][1])

			RecLock("IC6",.T.)
			Replace	IC6_FILIAL	With xFilial("IC6")
			Replace IC6_CODPQ	With IC5->IC5_CODPQ
//			Replace IC6_DESCPQ	With IC5->IC5_DESC
			Replace IC6_CODQT	With aQuest[nI][1]
			Replace IC6_DESC	With aQuest[nI][2]
	
		    If ! Empty(aFuncoes[nI][2]) 
			   	MSMM(IC6->IC6_CODFUN,,,aFuncoes[nI][2],1,,,"IC6","IC6_CODFUN")
				ConfirmSX8()		
			EndIf
		    If ! Empty(aDoctos[nI][2]) 
			   	MSMM(IC6->IC6_CODFOR,,,aDoctos[nI][2],1,,,"IC6","IC6_CODFOR")
				ConfirmSX8()		
			EndIf
			MsUnLock()
		EndIf
Next nI
RestArea( aArea )	
Return
			
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILIC4   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela de processos...		                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilIC4()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilIC4()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local cProcess := ""
Local aProcessos := {}

If lCria .and. IC5->IC5_GESTAO == "2"
	
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Processos basicos para a Gestao de Sarnbanes-Oxley...  �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aProcessos,{aQuest[1][1],"000001",STR0793   			,STR0768,"000001"}) //"AQUISICAO DE PROPRIEDADES"
	AADD(aProcessos,{aQuest[1][1],"000002",STR0794				,STR0768,"000001"}) //"AQUISICAO DE MAO DE OBRA"
	AADD(aProcessos,{aQuest[1][1],"000003",STR0795				,STR0768,"000001"}) //"AQUISICAO DE BENS E SERVICOS"
	AADD(aProcessos,{aQuest[1][1],"000004",STR0796				,STR0768,"000001"}) //"PAGAMENTO DAS COMPRAS"
	AADD(aProcessos,{aQuest[1][1],"000005",STR0797			    ,STR0768,"000001"}) //"CLASSIFICACAO"
	AADD(aProcessos,{aQuest[1][1],"000006",STR0798				,STR0768,"000001"}) //"PREPARO DE CONTROLES"
	AADD(aProcessos,{aQuest[1][1],"000007",STR0799				,STR0768,"000001"}) //"COMPRAS X PAGAMENTOS"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Processos basicos para a Gestao de Sarnbanes-Oxley...  �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aProcessos,{aQuest[2][1],"000001",STR0800				,STR0777,"000001"}) //"CONCESSAO DE CREDITO"
	AADD(aProcessos,{aQuest[2][1],"000002",STR0801				,STR0777,"000001"}) //"ADMINISTRACAO E CONTROLE PV"
	AADD(aProcessos,{aQuest[2][1],"000003",STR0802             ,STR0777,"000001"}) //"FATURAMENTO"
	AADD(aProcessos,{aQuest[2][1],"000004",STR0803             ,STR0777,"000001"}) //"EXPEDICAO"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Processos basicos para a Gestao de Sarnbanes-Oxley...  �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aProcessos,{aQuest[3][1],"000001",STR0804       		,STR0784,"000001"})//"CONTROLE DA PRODUCAO"
	AADD(aProcessos,{aQuest[3][1],"000002",STR0805    	   		,STR0784,"000001"})//"CONTABILIDADE DE CUSTOS"
	AADD(aProcessos,{aQuest[3][1],"000003",STR0806  	   		,STR0784,"000001"})//"ADMINISTRACAO DE ESTOQUES"
	AADD(aProcessos,{aQuest[3][1],"000004",STR0807         	,STR0784,"000001"})//"INVENTARIOS"
	AADD(aProcessos,{aQuest[3][1],"000005",STR0808       		,STR0784,"000001"})//"MANUTENCAO DE ATIVOS"
	
	Processa( { | lEnd | CriaIC4(aProcessos)} ,STR0766,STR0809) //"Aguarde!"###"Gerando registros dos sub-processos..."
 	
EndIf
Return
			
/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaIC4    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em IC4 com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaIC4(ExpA1)									      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aProcessos/Processo pre definidos...	      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaIC4(aProcessos)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aProcessos) )
     	                                                                           
For nI := 1 To Len(aProcessos)

	IncProc()

	DbSelectArea("IC4")
	DbSetOrder(1)
	If ! DbSeek(xFilial("IC4")+aProcessos[nI][1]+aProcessos[nI][2])

			RecLock("IC4",.T.)
			Replace	IC4_FILIAL	With xFilial("IC4")
			Replace IC4_CODQT	With aProcessos[nI][1]
			Replace IC4_PROCES	With aProcessos[nI][2]
			Replace IC4_DESCRI	With aProcessos[nI][3]
//			Replace IC4_DESCQT	With aProcessos[nI][4]
			Replace IC4_FCS		With aProcessos[nI][5]
			Replace IC4_PERGS	With "N"
			Replace IC4_FLUXOS	With "N"
			Replace IC4_STATUS	With "2"
			MsUnLock()
	EndIf
Next nI
RestArea( aArea )	
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILIC9   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela de perguntasro...		                              潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilIC9()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilIC9()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aPergs   := {}
Local aChoices := {}

If lCria .and. IC5->IC5_GESTAO == "2"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Perguntas para o primeiro Questionario...				    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPergs,{aQuest[1][1],"000001","1","1","",STR0810,"","","",STR0811,0,0,150,0.00,"2","2",""})//"Controles de Atividades? (S/N - Descreva)"###"Observacao:"
	AADD(aPergs,{aQuest[1][1],"000002","1","1","",STR0812    ,"","","",""           ,0,0,150,0.00,"2","1","ICP"})//"Possiveis Riscos? (S/N - Classifique)"
	AADD(aPergs,{aQuest[1][1],"000003","1","1","",STR0813  ,"","","",""           ,0,0,150,0.00,"2","1","ICR"}) //"Pontos de Controle? (S/N - Classifique)"
	AADD(aPergs,{aQuest[1][1],"000004","1","1","",STR0814          ,"","","",""           ,0,0,150,0.00,"2","1","ICU"})//"Evidencias? (S/N - Classifique)"
	//AADD(aPergs,{"000001","000005","3","1","","Audit Test?"                    ,"","","",""        ,0,0,0  ,0.00,"2"})
	//AADD(aPergs,{"000001","000006","3","1","","Remanation plan (Reference)?"   ,"","","",""        ,0,0,0  ,0.00,"2"})

	AADD(aChoices,{aQuest[1][1],"000001","01","2",STR0815          ,"2",0.00,"","2"})//"Sim"
	AADD(aChoices,{aQuest[1][1],"000001","02","2",STR0816          ,"2",0.00,"","2"})//"Nao"
	AADD(aChoices,{aQuest[1][1],"000001","03","2",STR0817			,"2",0.00,"","1"})//"Nao se Aplica"
	AADD(aChoices,{aQuest[1][1],"000002","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[1][1],"000002","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[1][1],"000003","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[1][1],"000003","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[1][1],"000004","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[1][1],"000004","02","2",STR0816          ,"2",0.00,"","2"})			

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Perguntas para o segundo Questionario...				    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPergs,{aQuest[2][1],"000001","1","1","",STR0810,"","","",STR0811,0,0,150,0.00,"2","2",""})
	AADD(aPergs,{aQuest[2][1],"000002","1","1","",STR0812    ,"","","",""           ,0,0,150,0.00,"2","1","ICP"})
	AADD(aPergs,{aQuest[2][1],"000003","1","1","",STR0813  ,"","","",""           ,0,0,150,0.00,"2","1","ICR"})
	AADD(aPergs,{aQuest[2][1],"000004","1","1","",STR0814          ,"","","",""           ,0,0,150,0.00,"2","1","ICU"})
	//AADD(aPergs,{"000002","000005","3","1","","Audit Test?"                    ,"","","",""        ,0,0,0  ,0.00,"2"})
	//AADD(aPergs,{"000002","000006","3","1","","Remanation plan (Reference)?"   ,"","","",""        ,0,0,0  ,0.00,"2"})

	AADD(aChoices,{aQuest[2][1],"000001","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000001","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000001","03","2",STR0817			,"2",0.00,"","1"})
	AADD(aChoices,{aQuest[2][1],"000002","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000002","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000003","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000003","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000004","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[2][1],"000004","02","2",STR0816          ,"2",0.00,"","2"})			

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Perguntas para o terceiro Questionario...				    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aPergs,{aQuest[3][1],"000001","1","1","",STR0810,"","","",STR0811,0,0,150,0.00,"2","2",""})
	AADD(aPergs,{aQuest[3][1],"000002","1","1","",STR0812    ,"","","",""           ,0,0,150,0.00,"2","1","ICP"})
	AADD(aPergs,{aQuest[3][1],"000003","1","1","",STR0813  ,"","","",""           ,0,0,150,0.00,"2","1","ICR"})
	AADD(aPergs,{aQuest[3][1],"000004","1","1","",STR0814          ,"","","",""           ,0,0,150,0.00,"2","1","ICU"})
	//AADD(aPergs,{"000003","000005","3","1","","Audit Test?"                    ,"","","",""        ,0,0,0  ,0.00,"2"})
	//AADD(aPergs,{"000003","000006","3","1","","Remanation plan (Reference)?"   ,"","","",""        ,0,0,0  ,0.00,"2"})

	AADD(aChoices,{aQuest[3][1],"000001","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000001","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000001","03","2",STR0817			,"2",0.00,"","1"})
	AADD(aChoices,{aQuest[3][1],"000002","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000002","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000003","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000003","02","2",STR0816          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000004","01","2",STR0815          ,"2",0.00,"","2"})
	AADD(aChoices,{aQuest[3][1],"000004","02","2",STR0816          ,"2",0.00,"","2"})			

	Processa( { | lEnd | CriaIC9(aPergs,aChoices)} ,STR0766,STR0818)//"Aguarde!"###"Gerando registros das questoes..."
 	
EndIf
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaIC9    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em IC9 com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaIC9(ExpA1,ExpA2) 							      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aPergs/Perguntas pre definidas...		      潮�
北�          � ExpA2 := Array aChoices/Alternativas das Perguntas...      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaIC9(aPergs,aChoices)
Local aArea := GetArea()
Local nI := 0
Local nA := 0
   
ProcRegua( Len(aPergs) )

IC6->(DbSetOrder(1))
     	                                                                           
For nI := 1 To Len(aPergs)
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Atualiza arquivo de Questoes...            �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁		
	IncProc()    
	
	DbSelectArea("IC9")
	DbSetOrder(1)		//QUESTIONARIO + QUESTAO
	If !DbSeek(xFilial("IC9")+aPergs[nI][1]+aPergs[nI][2])
	
		IC6->(DbSeek(xFilial("IC6")+IC5->IC5_CODPQ+aPergs[nI][1]))
		
   		RecLock("IC9",.T.)
		Replace IC9_FILIAL	With xFilial("IC9")
		Replace IC9_CODQT	With aPergs[nI][1]
//		Replace IC9_DESCQT	With IC6->IC6_DESC
		Replace IC9_CODQST	With aPergs[nI][2]
		Replace	IC9_TIPQST	With aPergs[nI][3]	
		Replace	IC9_TIPEXI	With aPergs[nI][4]
		Replace	IC9_REPETE	With aPergs[nI][5]
		Replace	IC9_DESC	With SubStr(aPergs[nI][6],1,TamSx3("IC9_DESC")[1])
		Replace	IC9_CODCOM	With aPergs[nI][7]
		Replace	IC9_CODGRP	With aPergs[nI][8]
		Replace	IC9_CODSUB	With aPergs[nI][09]
		Replace	IC9_OBSERV	With aPergs[nI][10]
		Replace	IC9_LIMCOM	With aPergs[nI][11]
		Replace	IC9_LIMSEL	With aPergs[nI][12]
		Replace	IC9_LIMRES	With aPergs[nI][13]
		Replace	IC9_PONTO	With aPergs[nI][14]
		Replace	IC9_SHWCOM	With aPergs[nI][15]
		Replace	IC9_SHWCOM	With aPergs[nI][15]
		Replace	IC9_SHWCOM	With aPergs[nI][15]
		Replace	IC9_TEMTAB	With aPergs[nI][16]
		Replace	IC9_TABELA	With aPergs[nI][17]
		
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Atualiza campo MEMO do arquivo de Questoes �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		MSMM(IC9->IC9_CODCOM,,,aPergs[nI][6],1,,,"IC9","IC9_CODCOM")
	    ConfirmSX8()
		MsUnLock()
	
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Cria arquivo de alternativas...            �
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		For nA := 1 To Len(aChoices)
				
			If  aChoices[nA][1]	== aPergs[nI][1]	
				DbSelectArea("ICA")
				DbSetOrder(1)		//QUESTIONARIO + QUESTAO + ALTERNATIVA
				If ! DbSeek(xFilial("ICA")+aChoices[nA][1]+aChoices[nA][2]+aChoices[nA][3])
					RecLock("ICA",.T.)
					Replace ICA_FILIAL	With xFilial("ICA")
					Replace ICA_CODQT	With aChoices[nA][1]
					Replace ICA_CODQST	With aChoices[nA][2]
					Replace	ICA_CODALT	With aChoices[nA][3]
					Replace	ICA_POSCOM	With aChoices[nA][4]
					Replace	ICA_DESC	With aChoices[nA][5]
					Replace	ICA_HABILI	With aChoices[nA][6]	
					Replace	ICA_PONTO	With aChoices[nA][7]
					Replace	ICA_SALTO	With aChoices[nA][8]
					Replace	ICA_CANCQT	With aChoices[nA][9]
					MsUnLock()
		       	EndIf
			EndIf
	    Next nA	
	EndIf
Next nI	
RestArea(aArea)
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILICS   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela de Objetivo de Controles...	                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilICS()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilICS()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aObjetivo := {}

If lCria .and. IC5->IC5_GESTAO == "2"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Objetivos de Controles para o Processo de Compras...   �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aObjetivo,{aQuest[1][1],"000001","000001",STR0819})//"Assegurar-se da real necessidade de aquisicao dos bens e servicos"
	AADD(aObjetivo,{aQuest[1][1],"000001","000002",STR0820+STR0821 })//"Determinar um nivel otimo para quantidade de compra, prazos de entrega"###" e prazos de pagamento para minimizar os custos."
	AADD(aObjetivo,{aQuest[1][1],"000001","000003",STR0822})//"Obter precos competitivos e o melhor valor na compras efetuadas"
	AADD(aObjetivo,{aQuest[1][1],"000001","000004",STR0823+STR0824})//"Estabelecer relacoes com fornecedores qualificados, que possam"###" fornecer servicos e bens permanentemente nos padroes de qualidade adotados."
	AADD(aObjetivo,{aQuest[1][1],"000001","000005",STR0825+STR0826+STR0827})//"Assegurar que os fornecedores sao responsaveis a conseguir as"###" as especificacoes de qualidade, e tem comprometimento na solucao de possiveis"###" problemas de qualidade de seus produtos."
	AADD(aObjetivo,{aQuest[1][1],"000001","000006",STR0828+STR0829})//"Minimizar as contigencias potenciais inseridas nos contratos"###" com fornecedores."
	AADD(aObjetivo,{aQuest[1][1],"000001","000007",STR0830+STR0831})//"Assegurar que as quantidades e qualidade dos materiais entregues"###" ou servicos prestados estao de acordo com as especificacoes negociadas."
	AADD(aObjetivo,{aQuest[1][1],"000001","000008",STR0832+STR0833})//"Assegurar que produtos com defeito, qualidade inferior, ou incorretos"###" sejam prontamente identificados no recebimento deles."
	AADD(aObjetivo,{aQuest[1][1],"000001","000009",STR0834+STR0835})//"Assegurar que os materiais recebidos sejam mantidos em seguranca,"###" prontamente."
	AADD(aObjetivo,{aQuest[1][1],"000001","000010",STR0836+STR0837})//"Verificacao da nota fiscal/fatura do fornecedor quanto a precos corretos,"###" quantidade, especificacoes de qualidade e condicoes de compra."
	AADD(aObjetivo,{aQuest[1][1],"000001","000011",STR0838+STR0839})//"Assegurar que todos os pagamentos efetuados aos fornecedores"###" representam corretamente os precos e quantidade por eles fornecidos."
	AADD(aObjetivo,{aQuest[1][1],"000001","000012",STR0840+STR0841})//"Assegurar que a empresa receba efetivamente todos os creditos por diferencas"###" de quantidades, rejeicao por qualidade ou especificacoes de bens e servicos."
	AADD(aObjetivo,{aQuest[1][1],"000001","000013",STR0842})//"Assegurar que todos os pagamentos sao registrados devidamente."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Objetivos de Controles para o Processo de Vendas...    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aObjetivo,{aQuest[2][1],"000001","000001",STR0843+STR0844})//"Assegurar que todos os pedidos dos clientes sao registrados"###" com as informacoes completas"
	AADD(aObjetivo,{aQuest[2][1],"000001","000002",STR0845+STR0846})//"Assegurar que todos os pedidos recebam uma classificacao e"###" e um processamento adequado."
	AADD(aObjetivo,{aQuest[2][1],"000001","000003",STR0847})//"Assegurar que todos os pedidos recebam classificacao e controle adequado."

	AADD(aObjetivo,{aQuest[2][1],"000001","000004",STR0848})//"Assegurar que todos os pedidos os embarques estejam completos e corretos."
	AADD(aObjetivo,{aQuest[2][1],"000001","000005",STR0849})//"Assegurar que todos os faturamentos embarcados sejam processados devidamente."
	AADD(aObjetivo,{aQuest[2][1],"000001","000006",STR0850})//"Assegurar que todos os itens atendidos sao faturados."
	AADD(aObjetivo,{aQuest[2][1],"000001","000007",STR0851})//"Assegurar que somente o que atendido e faturado."
	AADD(aObjetivo,{aQuest[2][1],"000001","000008",STR0852})//"Assegurar que todas as notas fiscais trazem seus precos corretos."
	AADD(aObjetivo,{aQuest[2][1],"000001","000009",STR0853})//"Assegurar que os custos de expedicao sejam minimizados."
	AADD(aObjetivo,{aQuest[2][1],"000001","000009",STR0854+STR0855})//"Assegurar que todas as notas fiscais sao devidamente lancadas no contas"###" a receber do cliente."
	AADD(aObjetivo,{aQuest[2][1],"000001","000010",STR0856})//"Assegurar que os recebimentos de caixa sao processados e depositados."
	AADD(aObjetivo,{aQuest[2][1],"000001","000011",STR0857+STR0858})//"Assegurar que todos os itens devolvidos sao processados novamente"###" para dar entrada nos estoques."
	AADD(aObjetivo,{aQuest[2][1],"000001","000012",STR0859})//"Assegurar que os creditos sao prontamente lancados na conta do cliente"

	AADD(aObjetivo,{aQuest[2][1],"000001","000013",STR0860})//"Assegurar que os todas as contas a receber sao cobradas."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Objetivos de Controles para o Processo de Vendas...    �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aObjetivo,{aQuest[3][1],"000001","000001",STR0861+STR0862})//"O que, quanto e quando determinado produtos deve ser"###" fabricado tem que ser de acordo com o autorizado pela administracao."

	AADD(aObjetivo,{aQuest[3][1],"000001","000002",STR0863+STR0864})//"Quando e em que condicoes a venda de ativos da empresa"###" deve ser efetuada, conforme autorizacao da administracao."

	AADD(aObjetivo,{aQuest[3][1],"000001","000003",STR0865+STR0866})//"Ajustes de inventario devem ser feitos com a autorizacao"###" da administracao."

	AADD(aObjetivo,{aQuest[3][1],"000001","000004",STR0867+STR0868})//"Deve haver um sistema de custos adequado e integrado"###" a contabilidade."

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria Objetivos de Controles para o Processo Financeiro...   �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aObjetivo,{"000004","000001","000001",STR0869+STR0870})//"As fontes de capitais devem ser as autorizadas pela"###" administracao."

	AADD(aObjetivo,{"000004","000001","000002",STR0871+STR0872})//"Montantes, o tempo e as condicoes de emprestimos devem"###" ser is autorizados pela adiministracao."

	AADD(aObjetivo,{"000004","000001","000003",STR0873+STR0874})//"O processamento das transacoes deve ser executado conforme"###" o estabelecido pela administracao."

	AADD(aObjetivo,{"000004","000001","000004",STR0875+STR0876})//"Os valores tomados de terceiros devem ser contabilizados"###" adequadamente."

	AADD(aObjetivo,{"000004","000001","000005",STR0877+STR0878})//"Acesso aos valores e caixa da empresa deve ser permitido"###" somente com autorizacao."

	Processa( { | lEnd | CriaICS(aObjetivo)} ,STR0766,STR0879)//"Gerando registros do objetivos..."
 	
EndIf
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaICS    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em ICS com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaIC9(ExpA1,ExpA2) 							      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aObjetivo...							      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaICS(aObjetivo)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aObjetivo) )

IC6->(DbSetOrder(1))
     	                                                                           
For nI := 1 To Len(aObjetivo)
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Atualiza arquivo de Objetivo de Controle...�
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁		
	IncProc()    
	
	DbSelectArea("ICS")
	DbSetOrder(1)		//PESQUISA+QUESTIONARIO+PROCESSO+OBJETIVO
	If ! DbSeek(xFilial("ICS")+IC5->IC5_CODPQ+aObjetivo[nI][1]+aObjetivo[nI][2]+aObjetivo[nI][3])
   		RecLock("ICS",.T.)
		Replace ICS_FILIAL	With xFilial("IC9")
		Replace ICS_CODPQ	With IC5->IC5_CODPQ
		Replace ICS_CODQT 	With aObjetivo[nI][1]
		Replace ICS_PROCES	With aObjetivo[nI][2]
		Replace ICS_OBJCON	With aObjetivo[nI][3]
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Atualiza campo MEMO do arquivo de Objetivos�
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		MSMM(ICS->ICS_CODDES,,,aObjetivo[nI][4],1,,,"ICS","ICS_CODDES")
	    ConfirmSX8()
		MsUnLock()
	EndIf
Next nI	
RestArea(aArea)
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILIC3   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela de Fator Critico de Sucesso...                      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilIC3()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilIC3()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aFator := {}

If lCria .and. IC5->IC5_GESTAO == "2"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria registros para Fatores Critico de Sucesso...           �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aFator,{"000001",STR0880,STR0881+STR0882})//"QUALIDADE"###"Incorpora tecnologia, desenvolve e tambem a implementa para que"###" os produtos, processos e servicos sejam executados com qualidade."

	AADD(aFator,{"000002",STR0883,STR0884+STR0885})//"MARCAS-IMAGEM"###"As marcas sao o maior patrimonio. Elas carregam a reputacao,"###" sao os lastros. Protege-las e responsabilidade de cada um dos colaboradores."

	AADD(aFator,{"000003",STR0886,STR0887+STR0888+STR0889})//"CLIENTES-PDV-LOGISTICA"###"Os clientes sao o elo com os consumidores e merecem"###" constante suporte, apoio comercial e de marketing. Nao podem e nem devem ficar"###" desabastecidos."

	AADD(aFator,{"000004",STR0890,STR0891+STR0892+STR0893})//"CONSUMIDORES-LOGISTICA"###"Os consumidores sao os verdadeiros patroes. Sua"###" crescente preferencia pelos produtos e a principal meta. Por esta razao a"###" logistica/distribuicao nao pode falhar."

	AADD(aFator,{"000005",STR0894,STR0895+STR0896+STR0897})//"GENTE-RECURSOS HUMANOS"###"Investir permanentemente em gente. O senso de"###" urgencia e paradigma nas grandes e pequenas acoes do cotidiano, bem como"###" o sentimento de 'dono do negocio'."
                    
	AADD(aFator,{"000006",STR0898,STR0899+STR0900+STR0901})//"LUCRO-CONTROLE DE PERDAS"###"O lucro representa a garantia do continuo"###" crescimento da empresa. O desperdicio e as perdas devem ser controlados"###" e evitados, tanto os diretos como os indiretos."
 
	Processa( { | lEnd | CriaIC3(aFator)} ,STR0766,STR0902)//"Gerando a tabela dos FCS..."
 	
EndIf
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaIC3    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em IC3 com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaIC3(ExpA1) 	  								      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aFator...								      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaIC3(aFator)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aFator) )

IC6->(DbSetOrder(1))
     	                                                                           
For nI := 1 To Len(aFator)
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Atualiza arquivo de Objetivo de Controle...�
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁		
	IncProc()    
	
	DbSelectArea("IC3")
	DbSetOrder(1)		//FCS
	If ! DbSeek(xFilial("IC3")+aFator[nI][1])
   		RecLock("IC3",.T.)
		Replace IC3_FILIAL	With xFilial("IC3")
		Replace IC3_FCS		With aFator[nI][1]
		Replace IC3_DESCRI	With aFator[nI][2]
		//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
		//� Atualiza campo MEMO do arquivo de Objetivos�
		//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
		MSMM(IC3->IC3_CODOBJ,,,aFator[nI][3],1,,,"IC3","IC3_CODOBJ")
	    ConfirmSX8()
		MsUnLock()
	EndIf
Next nI	
RestArea(aArea)
Return 

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � ICEFILICT   矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Prepara dados para gerar automaticamente registros na	  潮�
北�          � tabela de Evidencias...                                    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ICEFilICT()			   									  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function ICEFilICT()
Local lCria := GetNewPar("MV_ICEFILL","1")=="1"
Local aEvidencia := {}

If lCria .and. IC5->IC5_GESTAO == "2"

	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	//� Cria registros de Evidencias para Compras...                �
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
	AADD(aEvidencia,{"MATR100",STR0903,"COM","07"})	//"REL. SOLICITACAO DE COMPRAS"
	AADD(aEvidencia,{"MATR103",STR0904,"COM","07"})	//"REL. DOCTOS DE ENTRADA"
	AADD(aEvidencia,{"MATR110",STR0905,"COM","07"})	//"REL. PEDIDO DE COMPRAS"
	AADD(aEvidencia,{"MATR120",STR0906,"COM","07"})	//"REL. PEDIDOS EM ABERTO"
	AADD(aEvidencia,{"MATR150",STR0907,"COM","07"})	//"REL. DE COTACOES"
	AADD(aEvidencia,{"MATR160",STR0908,"COM","07"})	//"REL. COTACOES EM ABERTO"
	AADD(aEvidencia,{"MATR170",STR0909,"COM","07"})	//"BOLETIM DE ENTRADA"
	AADD(aEvidencia,{"MATR180",STR0910,"COM","07"})	//"NF DE DEVOLUCAO"
	AADD(aEvidencia,{"MATR190",STR0911,"COM","07"})	//"ITENS DA NF"
	AADD(aEvidencia,{"MATR200",STR0912,"COM","07"})	//"REL. DIVERGENCIA SCxPC"
	AADD(aEvidencia,{"MATR440",STR0913,"COM","07"})	//"ITENS POR PONTO DE PEDIDO"

	Processa( { | lEnd | CriaICT(aEvidencia)} ,STR0766,STR0914)//"Gerando a tabela de Evidencias..."
 	
EndIf
Return

/*
苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    �  CriaICT    矨utor  � Lucas              � Data � 11/05/04 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Grava registos em ICT com base nos array pre-definidos.    潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � Void CriaICT(ExpA1) 	  								      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpA1 := Array aEvidencias...						      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � SIGAICE (ICEXFIL.PRW)                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Function CriaICT(aEvidencia)
Local aArea := GetArea()
Local nI := 0
   
ProcRegua( Len(aEvidencia) )
   	                                                                           
For nI := 1 To Len(aEvidencia)
	//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
	//� Atualiza arquivo de Objetivo de Controle...�
	//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁		
	IncProc()    
	
	DbSelectArea("ICT")
	DbSetOrder(1)		//ICT_FUNCAO
	If ! DbSeek(xFilial("ICT")+aEvidencia[nI][1])
   		RecLock("ICT",.T.)
		Replace ICT_FILIAL	With xFilial("IC3")
		Replace ICT_FUNCAO	With aEvidencia[nI][1]
		Replace ICT_DESCRI	With aEvidencia[nI][2]
		Replace ICT_MODULO	With aEvidencia[nI][3]
		Replace ICT_TOPICO	With aEvidencia[nI][4]
		MsUnLock()
	EndIf
Next nI	
RestArea(aArea)
Return