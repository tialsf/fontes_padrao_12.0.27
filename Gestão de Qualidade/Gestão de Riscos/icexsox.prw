#include 'protheus.ch'
#include 'eicconst.ch'
#include 'msole.ch'
#include 'ICEXSOX.CH'

FUNCTION ICEXSOX()
	ProcessaDoc({ || ICEXSOX_Imp(), STR0001, STR0002 }) //"Impressao do Certificado Lei Sarbanes-Oxley"###"Abrindo Aplicativo"
Return

FUNCTION ICEXSOX_Imp()  
Private oWord
Private aQPath		:= QDOPATH()
Private cQPathD		:= aQPath[2] //Define onde esta o .dot original
Private cQPathTrm	:= aQPath[3] //Define onde sera gravado a copia do .dot
Private cTxtWord	:= Alltrim(GetMv("MV_QICEDOT",.F.,"certiSOX.dot")) //Nome do .Dot
 
oWord := OLE_CreateLink( 'TMsOleWord97' )

OLE_SetProperty( oWord, oleWdVisible, .F. )


CpyS2T(cQPathD+cTxtWord,cQPathTrm,.T.)
OLE_OpenFile  ( oWord, cQPathTrm+cTxtWord )


RegProcDoc( 2 )
IncProcDoc( STR0003 ) // "Definindo configuracoes ..."
IF ExistBlock( "ICEASOX" )
	ExecBlock( "ICEASOX", .f., .f.,{ 1 } )
Endif

OLE_SetDocumentVar ( oWord, 'ICE_Emp', IIF(!Empty(AllTrim(SM0->M0_NOMECOM)),AllTrim(SM0->M0_NOMECOM),AllTrim(SM0->M0_NOME)))
OLE_SetDocumentVar ( oWord, 'ICE_CEO', GetMv("MV_QICECEO",.F.," ")) 			// Nome do CEO (Presidente)
OLE_SetDocumentVar ( oWord, 'ICE_CFO', GetMv("MV_QICECFO",.F.," "))		// Nome do CFO (Diretor Financeiro)

IncProcDoc( STR0004 ) //"Atualizando variaveis..."
OLE_UpDateFields   ( oWord ) 
	

IncProcDoc( STR0005 )  //"Imprimindo Arquivo..."     
OLE_PrintFile( oWord,"ALL",,,1) 
Inkey(5)

OLE_SetProperty( oWord, oleWdVisible, .T. )

IncProcDoc( STR0006 ) //"Fechando Arquivo..."
OLE_CloseFile( oWord )
OLE_CloseLink( oWord )
 
Return

