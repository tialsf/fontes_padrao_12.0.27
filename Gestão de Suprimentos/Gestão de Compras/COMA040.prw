
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �COMA040   �Autor  �Silvia Monica       � Data �  26/04/11   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cadastro de Divergencias da Nota Fiscal Entrada             ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP Especifico para CNI                                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function COMA040

/*BEGINDOC
//��������������������������
//�Declaracao de Variaveis �
//��������������������������
ENDDOC*/          

Local cVldAlt := ".T."           // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.


Private cString := "COF"

dbSelectArea("COF")
dbSetOrder(1)

AxCadastro(cString,"Cadastro de Divergencias","ExcDiv()",cVldAlt)

Return

//------------------------------------------------------------------------------------------------
// Trata a exclusao do cadastro de divergencias
//------------------------------------------------------------------------------------------------

Function ExcDiv()

Local aArea  := GetArea()                  
Local lRet      := .T.
 

dbSelectArea("COG")
dbSetOrder(2)
                                                                                                    
if  dbSeek(xFilial("COG")+COF->COF_CODIGO)
    MsgInfo("Codigo foi usado em NF, n�o pode ser deletado")
    lRet := .F.
Endif

RestArea(aArea)	
Return (lRet)                                             
         

//---------------------------------------------------------------------------------------
//   Funcao para incluir e alterar as divergencias no checkbox da Pre-Nota
//---------------------------------------------------------------------------------------

 Function CA040MAN( _aDivPNF )

 Local _cArea    := GetArea()
 Local n := 0
  
 dbSelectArea("COG")    
 dbSetOrder(1)   

	  For n := 1 To Len(_aDivPNF) 
		     If  _aDivPNF[n][1] = .T.  .And. (INCLUI .Or. ALTERA)
			     dbSeek(xFilial("COG")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)+_aDivPNF[n][3])
			     If  !found()
			         RecLock( "COG", .T. )
			              COG->COG_FILIAL := xFilial()
			              COG->COG_DOC    := SF1->F1_DOC
			              COG->COG_SERIE  := SF1->F1_SERIE 
			              COG->COG_FORNEC := SF1->F1_FORNECE
			              COG->COG_LOJA   := SF1->F1_LOJA
			              COG->COG_CODIGO := _aDivPNF[n][3]
			         MsUnLock()    
    	         Endif    
   			      _aDivPNF[n][1] := .F.                            
   			 Elseif _aDivPNF[n][1] = .F. .And. ALTERA
			     dbSeek(xFilial("COG")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)+_aDivPNF[n][3])   			 
   			 	 If found()       
     		         RecLock( "COG", .F. )
   			 	          dbDelete()  
			         MsUnLock()    
   			 	 Endif 
		     Endif 
	  Next                 
	  

    _aDivPNF := {}            // limpa o checkbox
  
    
 RestArea(_cArea)			                 
	      
Return

//---------------------------------------------------------------------------------
//   Exclui as divergencias que estiverem  no checkbox da Pre-Nota
//---------------------------------------------------------------------------------

Function CA040EXC()

 Local _cArea    := GetArea()  
 
 dbSelectArea("COG")    
 dbSetOrder(1)   

 dbSeek(xFilial("COG")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA))

 Do While !Eof() .and. xFilial("COG")+COG->(COG_DOC+COG_SERIE+COG_FORNECE+COG_LOJA) = xFilial("SF1")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA) 
    RecLock( "COG", .F. )
      dbDelete()  
    MsUnLock()    
   	dbSkip()
 Enddo 

//------------------------------------------------------------------------------
//   limpa o checkbox das divergencias
//------------------------------------------------------------------------------

 If  Type("_aDivPNF") != "U"
    _aDivPNF := {}
 Endif   
    
 RestArea(_cArea)			     
Return


//------------------------------------------------------------------------------------------------
//   mostra as divergencias no checkbox que estiverem cadastradas na Pre-Nota
//------------------------------------------------------------------------------------------------
		     
Function CA040VER(cDoc,cSerie,cFornece,cLoja,cDiv)
Local lRet := .F.    
Local _cArea:= GetArea()

dbSelectArea("COG")    
dbSetOrder(1)  
 
lRet := dbSeek(xFilial("COG")+cDoc+cSerie+cFornece+cLoja+cDiv)
                      
RestArea(_cArea)	
Return(lRet)
