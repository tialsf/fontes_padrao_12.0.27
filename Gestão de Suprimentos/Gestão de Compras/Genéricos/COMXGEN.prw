#INCLUDE "PROTHEUS.CH"

Static lFWPDCanUse 	:= FindFunction("FWPDCanUse") .And. FWPDCanUse(.T.)
Static lPessoal		:= lFWPDCanUse .And. VerSenha(192)
Static lSensivel	:= lFWPDCanUse .And. VerSenha(193)
Static lRTxtLGPD	:= FindFunction("RetTxtLGPD")
Static lOfLGPD		:= FindFunction("OfuscaLGPD")

/*/{Protheus.doc} COMXHDCO()
Monta aHeader a aCols

@param	cAlias		- Alias
@param	aField		- Array com campos a serem apresentados
@param	aNotField	- Arrya com campos que n�o ser�o apresentados

@author rodrigo.mpontes
@since 07/10/19
@version 12
/*/
Function COMXHDCO(cAliasTmp,aField,aNotField)

Local aRetHead		:= {}
Local aAllFields	:= (cAliasTmp)->(DbStruct())
Local lInclui		:= .T.
Local nX			:= 0

Default aField		:= {}
Default	aNotField	:= {}

aAllFields := FWVetByDic(aAllFields,cAliasTmp,.F.)

For nX := 1 To Len(aAllFields)
	If X3Uso(GetSx3Cache(aAllFields[nX,1],'X3_USADO'))
		lInclui := .T.
		
		If Len(aField) > 0
			lInclui := aScan(aField,{|x| AllTrim(x) == AllTrim(aAllFields[nX,1])}) > 0
		Elseif Len(aNotField) > 0
			lInclui := aScan(aNotField,{|x| AllTrim(x) == AllTrim(aAllFields[nX,1])}) == 0
		Endif
		
		If lInclui
			aAdd(aRetHead,{ Trim(GetSx3Cache(aAllFields[nX,1],'X3_TITULO')),;
							GetSx3Cache(aAllFields[nX,1],'X3_CAMPO'),;
							GetSx3Cache(aAllFields[nX,1],'X3_PICTURE'),;
							GetSx3Cache(aAllFields[nX,1],'X3_TAMANHO'),;
							GetSx3Cache(aAllFields[nX,1],'X3_DECIMAL'),;
							GetSx3Cache(aAllFields[nX,1],'X3_VALID'),;
							,;
							GetSx3Cache(aAllFields[nX,1],'X3_TIPO'),;
							GetSx3Cache(aAllFields[nX,1],'X3_F3'),;
							,})
		Endif
	Endif
Next nX

Return aRetHead

/*/{Protheus.doc} COMTemSXI
	Dado <cEventId> busca na tabela SXI se existe algum cadastro
de usuario que deva receber alerta para o evento.
@author philipe.pompeu
@since 13/12/2019
@return lResult, se existem registros para <cEventId>
@param cEventID, caractere, evento a ser buscado.
/*/
Function COMTemSXI(cEventID)
	Local lResult	:= .F.
	Local cQuerySXI	:= ""
	Local cAliasSXI	:= GetNextAlias()
	Default cEventID := 'XXX'
	
	cQuerySXI   := "SELECT COUNT(*) AS NREG "
	cQuerySXI   += "FROM SXI SXI "
	cQuerySXI   += "WHERE SXI.XI_EVENTID='"+ cEventID +"' AND "
	cQuerySXI   += "SXI.D_E_L_E_T_= ' ' "
	cQuerySXI   := ChangeQuery(cQuerySXI)
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuerySXI),cAliasSXI)
	
	lResult := (cAliasSXI)->NREG > 0
	
	(cAliasSXI)->(dbCloseArea())
Return( lResult )


/*/{Protheus.doc} AjusCNL
//TODO Ajusta registros tabela CNL 
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Function AjusCNL()

	Local oProcess
	Local nOpc := 0

	nOpc := Aviso('Aten��o', 'Essa rotina tem como objetivo efetuar o ajuste da tabela CNL (Tipo de Planilha), corrigindo o campo Semi-Fixo (CNL_TPSFIX), de acordo com o preenchimento do campo Ctr.Fixo (CNL_CTRFIX).', {'Processar','Imprimir' ,'Cancelar'}, 2)
	If nOpc <> 3
		oProcess := MsNewProcess():New({|lEnd| AjustaCNL(@oProcess, @lEnd, nOpc) },"Atualizando Dados da CNL - Tipos de Planilha","Lendo Tipos de Planilha",.T.)
		oProcess:Activate()
	Endif

Return

/*/{Protheus.doc} AjustaCNL
//TODO Ajusta registros tabela CNL 
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return lRet

@type function
/*/     

Function AjustaCNL(oProcess, lEnd, nOpc)
	Local lRet 	      := .T.
	Private nTotal	  := 0
	
	Default oProcess := Nil 
	Default nOpc := 1


	lRet := .F.
	cAliasCNL := GetNextAlias()

	If oProcess <> Nil
		oProcess:SetRegua1(2)
		oProcess:IncRegua1("Lendo Tipo de Planilhas") // Lendo Tipo de Planilha 
	Endif

	BeginSQL Alias cAliasCNL

	SELECT	CNL.CNL_FILIAL AS FILIAL, CNL.CNL_CODIGO AS CODIGO, CNL.CNL_DESCRI AS DESCRICAO

	FROM %Table:CNL% CNL

	WHERE CNL_FILIAL = %xFilial:CNL%
	AND CNL.%NotDel%
	AND (CNL_TPSFIX = '0' OR CNL_TPSFIX = '')
	AND CNL_CTRFIX = '3'


	EndSql

	If oProcess <> Nil
		Count To nTotal
		oProcess:IncRegua1("Ser�o processados:"+ alltrim(str(nTotal))+"registros") //Ser�o processados x registros
		oProcess:SetRegua2(nTotal)
		DbSelectArea(cAliasCNL)
		(cAliasCNL)->(DbGoTop())

		If nTotal == 0
			Alert("Nenhum registro encontrado para processamento!") //Nenhum registro encontrado para processamento
			lRet := .F. 
	 		Return lRet
		EndIf
	EndIf

	If nOpc = 2 
		Imprel()
	Else
		ProcUPD(oProcess)
	EndIf	

	(cAliasCNL)->(DbCloseArea())

Return lRet


/*/{Protheus.doc} ImpRel
// Realiza impress�o dos registros que ser�o modificados
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return ${return}, ${return_description}

@type function
*/
static Function ImpRel()
	Local oReport

	oReport:= ReportDef()
	oReport:PrintDialog()

Return

/*/{Protheus.doc} ReportDef
//Rela��o de registros que ser�o modificados 
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function ReportDef()

	Local oReport
	Local oSection

	oReport:= TReport():New("FIXCNL","Fix Cadastro Tipo de Planilha",, {|oReport| ReportPrint(oReport)},OemToAnsi("'Este relat�rio imprime uma rela��o dos ajustes que s�o efetuados na CNL (Tipo de Planilha"))


	oSection := TRSection():New(oReport,OemToAnsi("CNL1"),{"CNL"},/*{Array com as ordens do relat�rio}*/,/*Campos do SX3*/,/*Campos do SIX*/)

	oSection:SetHeaderPage()

	TRCell():New(oSection,"CNL_FILIAL", "CNL")
	TRCell():New(oSection,"CNL_CODIGO", "CNL")
	TRCell():New(oSection,"CNL_DESCRI", "CNL")

Return(oReport)


/*/{Protheus.doc} ReportPrint
//Rela��o de registros que ser�o modificados 
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return ${return}, ${return_description}
@param oReport, object, descricao
@type function
/*/
Static Function ReportPrint(oReport)

	Local oSection  := oReport:Section(1)

	
	oReport:SetMeter(CNL->(LastRec()))
	oSection:Init()

	While  !(cAliasCNL)->(Eof())

		oSection:Cell("CNL_FILIAL"):SetValue((cAliasCNL)->FILIAL)
		oSection:Cell("CNL_CODIGO"):SetValue((cAliasCNL)->CODIGO)
		oSection:Cell("CNL_DESCRI"):SetValue((cAliasCNL)->DESCRICAO)

		oSection:PrintLine()
		oReport:IncMeter()
		(cAliasCNL)->(dbSkip())

	EndDo

	oSection:Finish()

Return NIL


/*/{Protheus.doc} ProcUPD
//Atualiza��o dos registros
@author fabiano.dantas
@since 24/12/2019
@version 1.0
@return ${return}, ${return_description}
@param oProcess, object, descricao
@type function
/*/
Static function ProcUPD(oProcess)
	Local nStatus := 0
	
	While !(cAliasCNL)->(Eof())

		cUpDate := " UPDATE "+RetSqlName("CNL")
		cUpDate += " 	SET CNL_TPSFIX = '1'"
		cUpDate += " 	WHERE CNL_FILIAL = '"+xFilial("CNL")+"' "
		cUpDate += " 	  AND CNL_CODIGO = '"+(cAliasCNL)->CODIGO+"' "
		cUpDate += " 	  AND D_E_L_E_T_ = ' ' "

		nStatus := TcSqlExec(cUpDate)
		If nStatus < 0
			conout("TCSQLError() " + TCSQLError())
  		EndIf

		If oProcess <> Nil
			oProcess:IncRegua2("Processando registro" + alltrim(str(nTotal)) )
		EndIf

		(cAliasCNL)->(dbSkip())
		

	EndDo

	If oProcess <> Nil
		msginfo("T�rmino do Ajuste - CNL")
	EndIf 
	
Return NIL


/*/{Protheus.doc} RetTxtLGPD()
Retorna Texto de acordo com o acesso LGPD do Usu�rio ao campo informado

@param	cTxt		- Texto Atual
@param	cCampo		- Nome do campo a verificar
@param	lTamCpo		- .T. -> Retorna string com tamanho do campo / 
					  .F. -> Limpa caracteres em Branco do final

@author rd.santos
@since 27/12/19
@version 12
/*/

Function RetTxtLGPD(cTxt,cCampo,lTamCpo)
Local aGrupos		:= {}
Local nX			:= 0
Default lTamCpo		:= .F.

If lFWPDCanUse .And. FwProtectedDataUtil():IsFieldInList( cCampo )
	aGrupos := FwProtectedDataUtil():GetFieldGroups( cCampo )
	For nX := 1 To Len(aGrupos)
		If   (aGrupos[nX]:IsPersonal() .And. !lPessoal); 
		.Or. (aGrupos[nX]:IsSensible() .And. !lSensivel)
			cTxt := If(lTamCpo,Padr(cTxt,TamSX3(cCampo)[1],''),Rtrim(cTxt))
			cTxt := FwProtectedDataUtil():ValueAsteriskToAnonymize( cTxt )
			Exit
		Endif
	Next nX
Endif

Return cTxt

/*/{Protheus.doc} OfuscaLGPD()
Ofusca um campo MsGet de acordo com o acesso do Usu�rio ao campo informado 

@param	oGet		- Objeto do tipo MsGet (TGet)
@param	cCampo		- Campo associado ao campo MsGet

@author rd.santos
@since 27/12/19
@version 12
/*/

Function OfuscaLGPD(oGet,cCampo)
Local aGrupos		:= {}
Local nX			:= 0
Local lRet			:= .F.

If lFWPDCanUse .And. FwProtectedDataUtil():IsFieldInList( cCampo )
	aGrupos := FwProtectedDataUtil():GetFieldGroups( cCampo )
	For nX := 1 To Len(aGrupos)
		If   (aGrupos[nX]:IsPersonal() .And. !lPessoal); 
		.Or. (aGrupos[nX]:IsSensible() .And. !lSensivel)
			If oGet <> NIL
				oGet:lObfuscate := .T.
				oGet:bWhen 		:= { || .F. }
			Endif
			lRet := .T.
			Exit
		Endif
	Next nX
Endif

Return lRet

/*/{Protheus.doc} SuprLGPD()
Verifica se o LGPD deve ser ofuscado para os m�dulos de Suprimentos 

@author rd.santos
@since 17/01/20
@version 12
/*/
Function SuprLGPD()
Local lRet := .F.

lRet := lRTxtLGPD  	.And. ; // FindFunction("RetTxtLGPD")
		lOfLGPD 	.And. ; // FindFunction("OfuscaLGPD")
		lFWPDCanUse 		// FindFunction("FWPDCanUse") .And. FWPDCanUse(.T.)

Return lRet
