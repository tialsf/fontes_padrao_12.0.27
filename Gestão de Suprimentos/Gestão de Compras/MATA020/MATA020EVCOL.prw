#include 'Protheus.ch'
#include 'FWMVCDef.ch'
#INCLUDE "MATA020.CH"

/*/{Protheus.doc} MATA020EVCOL
Eventos do MVC para a COLOMBIA, qualquer regra que se aplique somente para COLOMBIA
deve ser criada aqui, se for uma regra geral deve estar em MATA020EVDEF.

Todas as valida��es de modelo, linha, pr� e pos, tamb�m todas as intera��es com a grava��o
s�o definidas nessa classe.

Importante: Use somente a fun��o Help para exibir mensagens ao usuario, pois apenas o help
� tratado pelo MVC. 

Documenta��o sobre eventos do MVC: http://tdn.totvs.com/pages/viewpage.action?pageId=269552294

@type classe
 
@author Juliane Venteu
@since 02/02/2017
@version P12.1.17
/*/
CLASS MATA020EVCOL From FWModelEvent
	
	DATA cCodigo
	DATA cLoja
	
	DATA nOPC
	
	METHOD New() CONSTRUCTOR
	
	METHOD InTTS()
	METHOD ModelPosVld()
	
ENDCLASS

//-----------------------------------------------------------------
METHOD New() CLASS MATA020EVCOL

Return

/*/{Protheus.doc} InTTS
Metodo executado ap�s a grava��o dos dados, mas dentro da transa��o.

N�o retorna nada, se chegou at� aqui os dados ser�o gravados.

@type metodo
 
@author Juliane Venteu
@since 02/02/2017
@version P12.1.17
 
/*/
METHOD InTTS(oModel, cID) CLASS MATA020EVCOL
Local aAreaCV0 := CV0->(GetArea())
Local cItm

	If ::nOpc == MODEL_OPERATION_INSERT
		cItm := GetSxENum( "CV0", "CV0_ITEM" )
		Begin Transaction
			RecLock("CV0",.T.)
			CV0->CV0_FILIAL	:=xFilial("CV0")
			CV0->CV0_PLANO	:="01"
			CV0->CV0_ITEM		:=cItm
			CV0->CV0_CODIGO 	:= IIF(M->A2_TIPDOC=="31",M->A2_CGC,M->A2_PFISICA)
			CV0->CV0_CLASSE  	:= "2"
			CV0->CV0_NORMAL 	:= "2"
			CV0->CV0_ENTSUP 	:= "22"
			CV0->CV0_DTIEXI 	:= dDatabase
			CV0->CV0_TIPO00 	:= "02"
			CV0->CV0_DESC   	:= M->A2_NOME
			CV0->CV0_TIPO01 	:= M->A2_TIPDOC  
			CV0->CV0_COD   	:= M->A2_COD
		   CV0->CV0_LOJA  	:= M->A2_LOJA
			MsUnlock()
			ConfirmSX8()  
		End Transaction
	
	ElseIf ::nOpc == MODEL_OPERATION_UPDATE
		DbSelectArea("CV0")
		DbSetOrder(4)//CV0_FILIAL+CV0_COD+CV0_TIPO00+CV0_CODIGO
		If DbSeek(xFilial("CV0")+M->A2_COD+'02'+M->A2_CGC) .OR. DbSeek(xFilial("CV0")+M->A2_COD+'02'+M->A2_PFISICA)
			RecLock("CV0",.F.)
			CV0->CV0_DESC	:= M->A2_NOME
			CV0->CV0_COD	:= M->A2_COD
		   CV0->CV0_LOJA	:= M->A2_LOJA
		   MsUnlock()
		Elseif DbSeek(xFilial("CV0")+M->A2_COD+'02')
			RecLock("CV0",.F.)
			CV0->CV0_DESC	:= M->A2_NOME
			CV0->CV0_COD	:= M->A2_COD
		   CV0->CV0_LOJA	:= M->A2_LOJA
		   CV0->CV0_CODIGO := IIF(M->A2_TIPDOC=="31",M->A2_CGC,M->A2_PFISICA)
		   CV0->CV0_TIPO01 	:= M->A2_TIPDOC 
		   MsUnlock()
		EndIf
		
	ElseIf ::nOpc == MODEL_OPERATION_DELETE
		DbSelectArea("CV0")
		DbSetOrder(4)//CV0_FILIAL+CV0_COD+CV0_TIPO00+CV0_CODIGO
		If DbSeek(xFilial("CV0")+::cCodigo+'02'+M->A2_CGC) .OR. DbSeek(xFilial("CV0")+::cCodigo+'02'+M->A2_PFISICA)
			RecLock("CV0",.F.)
			dbDelete()
		   MsUnlock()
		EndIf
	EndIf

RestArea(aAreaCV0)		
Return

/*/{Protheus.doc} ModelPosVld
Executa a valida��o do modelo antes de realizar a grava��o dos dados.
Se retornar falso, n�o permite gravar.

@type metodo
 
@author Juliane Venteu
@since 02/02/2017
@version P12.1.17
 
/*/
METHOD ModelPosVld(oModel, cID) CLASS MATA020EVCOL
Local lValid := .T.
	
	::nOpc := oModel:GetOperation()
	::cCodigo := oModel:GetValue("SA2MASTER","A2_COD")
	::cLoja := oModel:GetValue("SA2MASTER","A2_LOJA")
		
	If ::nOpc == MODEL_OPERATION_UPDATE .Or. ::nOpc == MODEL_OPERATION_INSERT
		lValid  := A020ValDoc()

		If !Empty(M->A2_TIPDOC)
			If M->A2_TIPDOC=="31" //Tipo de Documento de Indetificacion NIT
				If EMPTY(M->A2_CGC)	                                          
					Help( ,, 'HELP',, STR0073, 1, 0) //Se debe indicar el campo NIT del Proveedor
					lValid:=.F.
			EndIf              
				IF !EMPTY(M->A2_PFISICA)
					Help( ,, 'HELP',, STR0074, 1, 0)//El campo RG/Ced Ext, debe estara Vacio
					lValid:=.F.
				Endif
			Else
				If !EMPTY(M->A2_CGC)	                                          
					Help( ,, 'HELP',, STR0075, 1, 0)//"El campo NIT del Proveedor, debe estara Vacio"
					lValid:=.F.
				EndIf              
				If EMPTY(M->A2_PFISICA)
					Help( ,, 'HELP',, STR0076, 1, 0)//Se debe indicar el campo RG/Ced Ext
					lValid:=.F.
				EndIf
			Endif  
		EndIf             
	EndIf

Return lValid

