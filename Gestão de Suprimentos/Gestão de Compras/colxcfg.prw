#Include "Protheus.ch"
#Include "ApWizard.ch"
#include "TopConn.ch"
#include "RwMake.ch"
#include "TbIconn.ch" 

#DEFINE _CRLF	Chr(13) + Chr(10)

/*/{Protheus.doc} COLXCFG
Wizard para configura��o do ambiente TOTVS Colabora��o

@author Rafael Duram Santos
@since 21/10/2013
/*/

Function COLXCFG()

Local oNewPag	:= nil
Local oStepWiz 	:= nil
Local oDlg 		:= nil
Local oPanel	:= nil
Local nConfig	:= 1
Local cMVNGINN	:= Space(50)
Local cMVNGLID	:= Space(50)
Local cTitle	:= "Configurador do Totvs Colabora��o / Importador XML"
Local cMessage	:= "Preencha os campos necess�rios para configura��o do TOTVS Colabora��o / Importador XML."
Local cText		:= "Essa ferramenta tem finalidade de facilitar a configura��o do Totvs Colabora��o / Importador XML"

Private oBrw1
Private oBrw2
Private oBrw3
Private aParamF1	:= {{"MV_COMCOL1",CONSPAR("MV_COMCOL1")[2]},{"MV_COMCOL2",CONSPAR("MV_COMCOL2")[2]},{"MV_MSGCOL",CONSPAR("MV_MSGCOL")[2]},{"MV_FILREP",CONSPAR("MV_FILREP")[2]}} 
Private aParamF2	:= {{"MV_XMLCFPC",CONSPAR("MV_XMLCFPC")[2]},{"MV_XMLCFBN",CONSPAR("MV_XMLCFBN")[2]},{"MV_XMLCFDV",CONSPAR("MV_XMLCFDV")[2]},{"MV_XMLCFND",CONSPAR("MV_XMLCFND")[2]},{"MV_XMLCFNO",CONSPAR("MV_XMLCFNO")[2]}}
Private aParamF3	:= {{"MV_CTECLAS",CONSPAR("MV_CTECLAS")[2]},{"MV_XMLPFCT",CONSPAR("MV_XMLPFCT")[2]},{"MV_XMLTECT",CONSPAR("MV_XMLTECT")[2]},{"MV_XMLCPCT",CONSPAR("MV_XMLCPCT")[2]}}

DEFINE WIZARD oWizard 	TITLE cTitle ;
       					HEADER cTitle ;
       					MESSAGE cMessage ;
       					TEXT cText ;
       					NEXT {||.T.} ;
		 				FINISH {|| .T. } ;
       					PANEL
       					
// Primeira etapa	
CREATE PANEL oWizard ;
				MESSAGE cMessage ;
				HEADER cTitle;
				BACK {|| .T. } ;
				NEXT {|| WIZVLD(1,nConfig) } ;
				FINISH {||  } ;
				PANEL
				
oPanel	:= oWizard:GetPanel(2)
WIZPG(oPanel,1,@nConfig)

// Segunda Etapa
CREATE PANEL oWizard ;
				MESSAGE cMessage ;
				HEADER cTitle ;
				BACK {|| .T. } ;
				NEXT {|| .T. } ;
				FINISH {||  } ;
				PANEL
				
oPanel	:= oWizard:GetPanel(3)
WIZPG(oPanel,2)

// Terceira Etapa
CREATE PANEL oWizard ;
				MESSAGE cMessage ;
				HEADER cTitle ;
				BACK {|| .T. } ;
				NEXT {|| .T. } ;
				FINISH {||  } ;
				PANEL
				
oPanel	:= oWizard:GetPanel(4)
WIZPG(oPanel,3,,@cMVNGINN,@cMVNGLID)

// Quarta Etapa
CREATE PANEL oWizard ;
				MESSAGE cMessage ;
				HEADER cTitle ;
				BACK {|| .T. } ;
				NEXT {|| .T. } ;
				FINISH {|| WIZCOMMIT(nConfig,cMVNGINN,cMVNGLID),.T.} ;
				PANEL
				
oPanel	:= oWizard:GetPanel(5)
WIZPG(oPanel,4)
       					    					
ACTIVATE WIZARD oWizard CENTERED
			
Return
 
/*/{Protheus.doc} WIZPG
Constru��o das etapas de configura��o

@param oPanel	Objeto do painel a ser apresentado
@param nPg		Etapa
@param nConfig	1-Totvs Colabora��o / 2-Importador XML
@param cNGINN	Parametro para importar XMLs
@param cNGLID	Parametro para salvar XMLs importados

@author rodrigo.mpontes
@since 05/08/19
/*/
Static Function WIZPG(oPanel,nPg,nConfig,cNGINN,cNGLID)

Local cDesc		:= ""
Local cLink		:= ""
Local cMVNGIN	:= SuperGetMV("MV_NGINN",.F.,Space(50))
Local cMVNGLI	:= SuperGetMV("MV_NGLIDOS",.F.,Space(50))
Local aConfig	:= {"Totvs Colabora��o","Importador XML"}
Local aFolder	:= {"Geral","NF-e","CT-e"}
Local aHdFold	:= {"Parametro","Conteudo"}
Local aTmFold	:= {10,50}
Local aBrowse	:= {}

If nPg == 1 //Pagina 1
	oDesc := TSay():New(10,10,{|| "Informe se deseja configurar o TOTVS Colabora��o ou Importador XML"},oPanel,,,,,,.T.,,,400,20)
	
	oRadio := TRadMenu():New (40,10,aConfig,,oPanel,,,,,,,,100,12,,,,.T.)
	oRadio:bSetGet := {|u|Iif (PCount()==0,nConfig,nConfig:=u)}
	
	cDesc := '<p>Link Guia de refer�ncia:</p>'
	cDesc += '<p>Totvs Colabora��o</p>'
	cDesc += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=271661626">http://tdn.totvs.com/pages/releaseview.action?pageId=271661626</a></p>'
	cDesc += '<p></p>'
	cDesc += '<p>Importador XML</p>'
	cDesc += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=485858148">http://tdn.totvs.com/pages/releaseview.action?pageId=485858148</a></p>'
	cDesc += '<p></p>'
	
	oDesc2 := TSay():New(70,10,{||cDesc},oPanel,,,,,,.T.,,,400,300,,,,,,.T.)
	
Elseif nPg == 2 //Pagina 2
	cDesc := '<p>As configura��es de <strong>Agendamento (COLAUTOREAD/SCHEDCOMCOL)</strong> e <strong>E-mail (Eventviewer - Evento 052/053)</strong> dever�o ser realizadas manualmente.</p>'
	cDesc += '<p></p>'
	cDesc += '<p>Totvs Colabora��o - Agendamento / E-mail</p>'
	cDesc += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=271662306">http://tdn.totvs.com/pages/releaseview.action?pageId=271662306</a></p>'
	cDesc += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=271662413">http://tdn.totvs.com/pages/releaseview.action?pageId=271662413</a></p>'
	cDesc += '<p></p>'
	cDesc += '<p>Importador XML - Agendamento / E-mail</p>'
	cDesc += '<p><a href="http://tdn.totvs.com/display/public/PROT/Agendamento+%28Schedule%29+-+Importador+XML">http://tdn.totvs.com/display/public/PROT/Agendamento+%28Schedule%29+-+Importador+XML</a></p>'
	cDesc += '<p><a href="http://tdn.totvs.com/display/public/PROT/E-mail+%28Eventviewer%29+-+Importador+XML">http://tdn.totvs.com/display/public/PROT/E-mail+%28Eventviewer%29+-+Importador+XML</a></p>'	
	
	oDesc3 := TSay():New(10,10,{||cDesc},oPanel,,,,,,.T.,,,250,300,,,,,,.T.)

Elseif nPg == 3 //Pagina 3
	If !Empty(cMVNGIN)
		cNGINN := cMVNGIN
	Endif
	
	If !Empty(cMVNGLI)
		cNGLID := cMVNGLI
	Endif
	
	cDesc := "Definir o caminho da onde ser�o importados os XML (Par�metros)" + CRLF + CRLF +;
			 "Obs: o caminho deve estar dentro do DATA do Protheus."
	
	oDesc4 := TSay():New(10,10,{|| cDesc},oPanel,,,,,,.T.,,,400,20)
	
	oDesc5 := TSay():New(40,10,{|| "MV_NGINN: "},oPanel,,,,,,.T.,,,40,20)
	oGet1  := TGet():New(38,60,{|u|If(PCount()==0,cNGINN,cNGINN := u ) },oPanel,120,10,"@!",,,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"cNGINN",,,,)
	
	oDesc6 := TSay():New(60,10,{|| "MV_NGLIDOS: "},oPanel,,,,,,.T.,,,40,20)
	oGet2  := TGet():New(58,60,{|u|If(PCount()==0,cNGLID,cNGLID := u ) },oPanel,120,10,"@!",,,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"cNGLID",,,,)
	
	cLink := '<p>Totvs Colabora��o - Estrutura</p>'
	cLink += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=271662259">http://tdn.totvs.com/pages/releaseview.action?pageId=271662259</a></p>'
	cLink += '<p></p>'
	cLink += '<p>Importador XML - Estrutura</p>'
	cLink += '<p><a href="http://tdn.totvs.com/pages/releaseview.action?pageId=485869252">http://tdn.totvs.com/pages/releaseview.action?pageId=485869252</a></p>'
	
	oDesc7 := TSay():New(80,10,{||cLink},oPanel,,,,,,.T.,,,400,300,,,,,,.T.)
	
Elseif nPg == 4 //Pagina 4
	oTFolder := TFolder():New(05,05,aFolder,,oPanel,,,,.T.,,290,130)
	
	//Folder 1 - Geral
	oBrw1 	:= TWBrowse():New(05,05,280,115,,aHdFold,aTmFold,oTFolder:aDialogs[1],,,,,,,,,,,,.F.,,.T.,,.F.,,,)
	oBrw1:SetArray(aParamF1)
	oBrw1:bLine	:= { || {   aParamF1[oBrw1:nAt,1],;
							aParamF1[oBrw1:nAt,2]}}
	oBrw1:bLDblClick := {|| SELPARAM(oBrw1,oBrw1:nAT,aParamF1)}
			
	//Folder 2 - NF-e
	oBrw2 	:= TWBrowse():New(05,05,280,115,,aHdFold,aTmFold,oTFolder:aDialogs[2],,,,,,,,,,,,.F.,,.T.,,.F.,,,)
	oBrw2:SetArray(aParamF2)
	oBrw2:bLine	:= { || {   aParamF2[oBrw2:nAt,1],;
							aParamF2[oBrw2:nAt,2]}}
	oBrw2:bLDblClick := {|| SELPARAM(oBrw2,oBrw2:nAT,aParamF2)}
	
	//Folder 3 - CT-e
	oBrw3 	:= TWBrowse():New(05,05,280,115,,aHdFold,aTmFold,oTFolder:aDialogs[3],,,,,,,,,,,,.F.,,.T.,,.F.,,,)
	oBrw3:SetArray(aParamF3)
	oBrw3:bLine	:= { || {   aParamF3[oBrw3:nAt,1],;
							aParamF3[oBrw3:nAt,2]}}
	oBrw3:bLDblClick := {|| SELPARAM(oBrw3,oBrw3:nAT,aParamF3)}
Endif

Return  
 
/*/{Protheus.doc} WIZVLD
Valida��o de alguma informa��o

@param nPg		Etapa
@param nConfig	1-Totvs Colabora��o / 2-Importador XML

@author rodrigo.mpontes
@since 05/08/19
/*/
Static Function WIZVLD(nPg,nConfig)

Local lRet		:= .T.
Local cTexto	:= ""
Local cTabCKO	:= ""

If nPg == 1 //Pagina 1
	If nConfig == 2 //Importador XML
		If CKO->(FieldPos("CKO_ARQXML")) == 0 .Or. Empty(CKO->(IndexKey(5)))
			cTexto := "Para utilizar o importador XML � necessario verificar compatibilidade." + CRLF + CRLF + ;
					  "http://tdn.totvs.com/display/public/PROT/IX01+-+Compatibilizadores"
					  
			WIZHLP(cTexto)
			lRet := .F.
		Endif
	Endif
	
	cTabCKO := RetSqlName("CKO")
	If cTabCKO <> "CKOCOL"
		If Empty(cTexto)
			cTexto := "Ser� necessario ajustar a tabela CKO, para CKOCOL na SX2"
		Else
			cTexto += "Ser� necessario ajustar a tabela CKO, para CKOCOL na SX2"
		Endif
		lRet := .F.
	Endif
	
	If !lRet
		WIZHLP(cTexto)
	Endif	
Endif

Return lRet

/*/{Protheus.doc} WIZHLP
Tela de apresenta��o de algum erro ou finalizando a configura��o.

@param cTexto	Texto a ser apresentado

@author rodrigo.mpontes
@since 05/08/19
/*/
Static Function WIZHLP(cTexto)

DEFINE MSDIALOG oDlgHlp TITLE "Help" FROM 000,000 TO 300,400 PIXEL

oFont := TFont():New('Arial',,-14,.T.)
oMultHlp := tMultiget():new(00,00,{|u| if(pCount() > 0,cTexto := u,cTexto)},oDlgHlp,195,145,oFont,,,,,.T.,,,,,,.T.,,,,.F.,.T.)

ACTIVATE MSDIALOG oDlgHlp CENTERED

Return

/*/{Protheus.doc} SELPARAM
Selecionado parametro para modifica��o.
Apresenta��o detalhada do parametro

@param oObj		Objeto TwBrowse em edi��o
@param nLinha	Linha em edi��o
@param aParam	Array em edi��o

@author rodrigo.mpontes
@since 05/08/19
/*/
Static Function SELPARAM(oObj,nLinha,aParam)

Local cDesc		:= ""
Local cParam	:= ""
Local xConteud
Local aInfo		:= CONSPAR(oObj:aArray[nLinha,1])

cParam		:= aInfo[1]
xConteud	:= aInfo[2]
cDesc		:= aInfo[3]

DEFINE MSDIALOG oDlgPar TITLE "Parametros" FROM 000,000 TO 300,400 PIXEL

oNomPar 	:= TSay():New(10,05,{|| "Parametro: " + cParam},oDlgPar,,,,,,.T.,,,100,20)
oConPar 	:= TSay():New(30,05,{|| "Conteudo: "},oDlgPar,,,,,,.T.,,,40,20)
oDescPar 	:= TGet():New(28,50,{|u|If(PCount()==0,xConteud,xConteud := u ) },oDlgPar,120,10,"!@",,,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F. ,,"xConteud",,,,)
	
oFont := TFont():New('Arial',,-14,.T.)
oMultPar := tMultiget():new(50,05,{|u| if(pCount() > 0,cDesc := u,cDesc)},oDlgPar,195,65,oFont,,,,,.T.,,,,,,.T.,,,,.F.,.T.)

oBtn1 := TButton():New(125,110,"Salvar",oDlgPar,{|| SAVEPAR(oObj,nLinha,xConteud,aParam),oDlgPar:End()}, 40,20,,,.F.,.T.,.F.,,.F.,,,.F. ) 
oBtn2 := TButton():New(125,150,"Cancelar",oDlgPar,{|| oDlgPar:End()}, 40,20,,,.F.,.T.,.F.,,.F.,,,.F. ) 

ACTIVATE MSDIALOG oDlgPar CENTERED

Return

/*/{Protheus.doc} SAVEPAR
Salva edi��o do parametro / refresh em tela

@param oObj			Objeto TwBrowse em edi��o
@param nLinha		Linha em edi��o
@param xConteudo	Conteudo do parametro
@param aParam		Array em edi��o

@author rodrigo.mpontes
@since 05/08/19
/*/

Static Function SAVEPAR(oObj,nLinha,xConteudo,aParam)

aParam[nLinha,2]		:= xConteudo
oObj:aArray[nLinha,2]	:= xConteudo

oObj:SetArray(aParam)
oObj:bLine	:= { || {   aParam[oObj:nAt,1],;
						aParam[oObj:nAt,2]}}
oObj:Refresh()

Return

/*/{Protheus.doc} CONSPAR
Consulta parametro na SX6

@param cMVParam	Parametro para consulta na SX6

@author rodrigo.mpontes
@since 05/08/19
/*/

Static Function CONSPAR(cMVParam)

Local aRet		:= {}
Local cParam	:= ""
Local cDesc		:= ""
Local xConteudo

DbSelectArea("SX6")
If SX6->(DbSeek(xFilial("SX6") + cMVParam))
	cParam 		:= cMVParam
	xConteudo	:= X6Conteud()
	cDesc		:= AllTrim(X6Descric()) + AllTrim(X6Desc1()) + AllTrim(X6Desc2())
Endif

aAdd(aRet,cParam)
aAdd(aRet,xConteudo)
aAdd(aRet,cDesc)

Return aRet

/*/{Protheus.doc} WIZCOMMIT
Commit das altera��o dos parametros

@param nConfig	1-Totvs Colabora��o / 2-Importador XML
@param cMVNGINN	Parametro para importar XML
@param cMVNGLID	Parametro para salvar XMLs lidos

@author rodrigo.mpontes
@since 05/08/19
/*/

Static Function WIZCOMMIT(nConfig,cMVNGINN,cMVNGLID)

Local nI	:= 0

If nConfig == 2 //Importador XML
	PutMV("MV_IMPXML",.T.)
Endif

If !Empty(cMVNGINN)
	PutMV("MV_NGINN",cMVNGINN)
Endif

If !Empty(cMVNGLID)
	PutMV("MV_NGLIDOS",cMVNGLID)
Endif

For nI := 1 To Len(aParamF1)
	PutMV(aParamF1[nI,1],aParamF1[nI,2])
Next nI

For nI := 1 To Len(aParamF2)
	PutMV(aParamF2[nI,1],aParamF2[nI,2])
Next nI

For nI := 1 To Len(aParamF3)
	PutMV(aParamF3[nI,1],aParamF3[nI,2])
Next nI

WIZHLP("Parametros atualizados com sucesso")

Return