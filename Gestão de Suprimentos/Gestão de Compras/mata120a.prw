#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} GravaSC7A
Fun豫o utilizada para gera豫o de pedidos de compra pelo SIGAEIC quando MV_EIC0008 = .F.
@author leonardo.magalhaes
@since 09/10/2019
@return Nil
@type function
/*/
Function GravaSC7A(nOpcao,cCodProd,nQuant,nPreco,cNumPed,cLocal,cCusto,cFornece,cLoja,dEmissao,cNumSc,dDataPr,cItem,cSequen,cObserv,cOrigem,cItemSc,nQtSegum,cSegum,nMoeda,cFluxo,nTaxa,cNumPo,cAprov, cConapro, nDespesa , nDesconto)

LOCAL aArea	    := GetArea()
LOCAL aAreaSCX  := SCX->(GetArea())
LOCAL aAreaSCH  := SCH->(GetArea())
LOCAL aEntCtb 	:= CtbEntArr()
LOCAL nQtdOri   := 0
LOCAL nDifSC1   := 0
LOCAL nQuantSC  := 0
LOCAL nQtQuje   := 0
LOCAL nDecCus1  := GetSX3Cache("CH_CUSTO1", "X3_DECIMAL")
LOCAL nDecCus2  := GetSX3Cache("CH_CUSTO2", "X3_DECIMAL")
LOCAL nDecCus3  := GetSX3Cache("CH_CUSTO3", "X3_DECIMAL")
LOCAL nDecCus4  := GetSX3Cache("CH_CUSTO4", "X3_DECIMAL")
LOCAL nDecCus5  := GetSX3Cache("CH_CUSTO5", "X3_DECIMAL")
LOCAL nX		:= 0
LOCAL lAtuSB2   :=.F.
LOCAL cUser	    := RetCodUsr()
LOCAL cGrComPad	:= ""
LOCAL cCpoSCX 	:= ""
LOCAL cCpoSCH 	:= ""

/*
旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
쿗ISTA DE PARAMETROS A SER UTILIZADA PARA A INTEGRACAO COM O MODULO    �
쿏E IMPORTACAO                                                         �
쳐컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴캑
�	OPCAO       : (1) = Inclusao, (2) = Alteracao, (3) = Exclusao      �
� 	CODIGO      : Codigo do Produto  - Obrigatorio                     �
�  QUANTIDADE  : Quantidade Principal - Obrigatorio                    �
�  PRECO       : Preco Unitario do Item - Obrigatorio                  �
�  NUMERO      : Numero do Pedido - Obrigatorio >>> Este e' o proprio  �
�                Numero do Purchase Order                              �
�  LOCAL       : Local de Entrada >>> Opcional - Caso nao passado, ser졂
�                assumido o local padr꼘 do Produto                    �
�  CENTRO CUSTO: Centro de Custo >>> Opcional - Caso nao passado, ser� �
�                assumido o Centro de Custo do Produto                 �
�  FORNECEDOR  : Codigo do Fornecedor >>> Obrigatorio                  �
�  LOJA        : Loja do Fornecedor >>> Obrigatorio -  O SIGAEIC n꼘 se�
�                utiliza de loja. Neste caso, ser� assumido '01'       �
�  EMISSAO     : Data de Emiss꼘 do Pedido >>> Opcional - Se nao for   �
�                passdo ser� assumido a pr줽ria DATA BASE              �
�  NUMERO SC   : Numero da Solicita뇙o de Compra >>> Opcional - Se nao �
�                passada ser� assumido brancos                         �
�  DATA PREVIST: Data Prevista para entrega >>> Opcional - Se nao      �
�                passada ser� assumida a data base                     �
�  ITEM        : Numero do Item do Pedido >>> Opcional - SIGACOM       �
�  SEQUENCIA   : Numero do Sequencia  >>> Opcional - SIGAEIC           �
�  OBSERV      : Observacao do item >>> Opcional                       �
�  ORIGEM      : Rotina Geradora do Pedido >>> Obrigatoria             �
�  ITEM SC     : Item da Solicitacao de Compra >>> Opcional            �
�  QUANT SEG UN: Quant na segunda unidade de medida                    �
�  SEGUNDA UNID: Segunda Unidade Medida                                �
�  MOEDA       : Moeda do Pedido                                       �
�  FLUXO CAIXA : Indicador de fluxo de caixa ou nao                    �
�  TAXA MOEDA  : Taxa da Moeda                                         �
�  NUMERO DO PO: Numero do PO (Purchase Order)                         �
�  APROVADOR   : Aprovador do Pedido                                   �
�  CONT. APROV.: Controle de Aprovacao                                 �
읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸


*/
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� ATENCAO:                               �
//� O campo item � usado pelo SIGA com 2   �
//� posi뇯es o que para o SIGAEIC n꼘 faz  �
//� sentido. Desta forma, ser� considerado �
//� o campo C7_SEQUEN (Sequencia do Pedido)�
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸

dEmissao := If(dEmissao==NIl,dDataBase,dEmissao)
nOpcao   := If(nOpcao==Nil,1,nOpcao)
cItem    := If(cItem==Nil,StrZero(1,Len(SC7->C7_ITEM)),cItem)
dDatapr  := If(dDataPr==NIl,dDataBase,dDataPr)
cObserv  := If(cObserv==Nil,"PEDIDO DE IMPORTACAO",cObserv)
cItemSc  := If(cItemSc==Nil,StrZero(1,Len(SC1->C1_ITEM)),cItemSc)
cSequen  := If(cSequen==Nil,"0001",cSequen)
nQtSegum := If(nQtSegum==Nil,0,nQtSegum)
cSegum	 := If(cSegum==Nil,"  ",cSegum)
nMoeda   := If(nMoeda==Nil,1,nMoeda)
cFluxo   := If(cFluxo==Nil,"S",cFluxo)
nTaxa    := If(nTaxa==Nil,RecMoeda(dDataBase,nMoeda), nTaxa)
cNumPo   := Iif(cNumPo==NIl,"",cNumPo)
cAprov   := Iif(cAprov==NIl,"",cAprov)
cConapro := Iif(cConapro==NIl,"",cConapro)
nDespesa := If(nDespesa==Nil,0,nDespesa)
nDesconto := Iif(nDesconto==Nil,0,nDesconto)//ACB - 24/11/2010

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Verifica o grupo de aprovacao do Comprador.                  �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
dbSelectArea("SY1")
dbSetOrder(3)
If MsSeek(xFilial()+RetCodUsr())
	cGrComPad	:= If(!Empty(Y1_GRUPCOM),SY1->Y1_GRUPCOM,Space(Len(SC7->C7_GRUPCOM)))
EndIf

//Verifica a existencia da SC para atualizacao do SB2
dbSElectArea("SC1")
dbSetOrder(1)
If !dbSeek(xFilial()+cNumSC)
	lAtuSB2:= .T.
EndIf

// Posiciona no fornecedor
dbSelectArea("SA2")
dbSetOrder(1)
dbSeek(xFilial()+cFornece+cLoja)

// Posiciona no produto
dbSelectArea("SB1")
dbSetOrder(1)
dbSeek(xFilial()+cCodProd)

cLocal := Iif(cLocal==Nil,RetFldProd(SB1->B1_COD,"B1_LOCPAD"),cLocal)

// Verifica se o pedido/item ja'existe
If nOpcao == 1
	dbSelectArea("SC7")
	dbSetOrder(1)
	If	(dbSeek(xFilial()+cNumPed+cItem+cSequen)) .OR.;
		(dbSeek(xFilial()+cNumPed+'01  '+cSequen))// Alex Wallauer (AWR)/Average - 17/02/2003 - Seek p/ os pedidos antigos (versao anterior)
		Help(" ",1,"SC7FOUND")
		RestArea(aArea)
		Return (.F.)
	EndIf
EndIf

// Posiciona nos saldos de estoque
If lAtuSB2
	dbSelectArea("SB2")
	dbSetOrder(1)
	If !(dbSeek(xFilial()+cCodProd+cLocal))
		CriaSB2(cCodProd,cLocal)
	EndIf
EndIf

If nOpcao == 2 .Or. nOpcao == 3
	dbSelectArea("SC7")
	If	(dbSeek(xFilial()+cNumPed+cItem+cSequen)) .OR.;
		(dbSeek(xFilial()+cNumPed+'01  '+cSequen))// Alex Wallauer (AWR)/Average - 17/02/2003 - Seek p/ os pedidos antigos (versao anterior)
		If (nOpcao == 3)
			Reclock("SC7")
			cItemSc:= SC7->C7_ItemSC
			dbDelete()
			If lAtuSB2
				Reclock("SB2",.F.)
				SB2->B2_SALPEDI -= (SC7->C7_QUANT - SC7->C7_QUJE)
			EndIf
			dbSelectArea("SC1")
			dbSetOrder(2)
			If dbSeek(xFilial()+cCodProd+cNumSc+cItemSc)
				RecLock("SC1",.F.)
				SC1->C1_QUJE := SC1->C1_QUJE - ( SC7->C7_QUANT - SC7->C7_QUJE )
				MsUnlock()
			EndIf
			DbSelectArea("SCH")
			SCH->(DbSetOrder(1)) //-- CH_FILIAL + CH_PEDIDO + CH_FORNECE + CH_LOJA + CH_ITEMPD + CH_ITEM
			If SCH->(DbSeek(xFilial("SCH") + cNumPed + cFornece + cLoja + cItem))
				While SCH->(!Eof()) .And. ((SCH->CH_PEDIDO + SCH->CH_FORNECE + SCH->CH_LOJA + SCH->CH_ITEMPD) == (cNumPed + cFornece + cLoja + cItem))
					RecLock("SCH")
						SCH->(DbDelete())
					SCH->(MsUnLock())
					SCH->(DbSkip())
				EndDo
			EndIf
		Else
			nQtdOri := (SC7->C7_QUANT - SC7->C7_QUJE)
			If lAtuSB2
				Reclock("SB2",.F.)
				SB2->B2_SALPEDI := B2_SALPEDI - nQtdOri + nQuant
			EndIf
		Endif
		//Elizabete-Average-01/2003
		//Ao alterar ou deletar um item da PO que foi originada por uma autorizacao de
		//entrega no SIGACOM, deve-se atualizar o saldo no SC3 do contrato de parceria
		If SC7->C7_TIPO == 2 .AND. !EMPTY(SC7->C7_NUMSC)
			If SC3->(dbSeek(xFilial("SC3")+SC7->C7_NUMSC + SC7->C7_ITEM))
				SC3->(RecLock("SC3",.F.))
				If nOpcao == 2  // alteracao
					SC3->C3_QUJE := (SC3->C3_QUJE - SC7->C7_QUANT) + nQuant
				Else
					SC3->C3_QUJE := SC3->C3_QUJE + SC7->C7_QUANT
				EndIf
				SC3->C3_ENCER := IIf(SC3->C3_QUANT - SC3->C3_QUJE > 0," ","E")
				MsUnlock()
			EndIf
		EndIf

	Else
		Help(" ",1,"SC7NOFOUND")
		RestArea(aArea)
		Return (.F.)
	Endif
Endif

If nOpcao == 1
	RecLock("SC7",.T.)
Else
	RecLock("SC7")
Endif

// Atualiza Registro do Pedido de Compras
If (nOpcao == 1 .or. nOpcao == 2)
	nDifSC1	:= SC7->C7_QUANT - nQuant
	Replace C7_FILIAL      With xFilial("SC7")
	Replace C7_FILENT      With xFilial("SC7")
	Replace C7_NUM         With cNumPed
	Replace C7_PRODUTO     With cCodProd
	Replace C7_LOCAL       With cLocal
	Replace C7_DESCRI      With SB1->B1_DESC
	Replace C7_PRECO       With nPreco
	Replace C7_UM          With SB1->B1_UM
	Replace C7_SEGUM       With SB1->B1_SEGUM
	Replace C7_QTSEGUM     With ConvUm(SC7->C7_PRODUTO,nQuant,nQtSegum,2)
	Replace C7_CONTATO     With SA2->A2_CONTATO
	Replace C7_OBS         With cObserv
	Replace C7_EMISSAO     With dEmissao
	Replace C7_CONTA       With SB1->B1_CONTA
	Replace C7_FORNECE     With cFornece
	Replace C7_LOJA        With cLoja
	Replace C7_NUMIMP      With cNumPed
	Replace C7_CC          With cCusto
	Replace C7_QUANT       With nQuant
	Replace C7_DATPRF      With dDataPr
	Replace C7_TOTAL       With C7_PRECO*C7_QUANT
	If nOpcao == 1
		Replace C7_TIPO        With 1
		Replace C7_ORIGEM      With cOrigem
		Replace C7_NUMSC       With cNumSc
	EndIf
	Replace C7_ITEM        With cItem
	Replace C7_SEQUEN      With cSequen
	Replace C7_QTSEGUM     With nQtSegum
	Replace C7_SEGUM       With cSegum
	Replace C7_FLUXO       With cFluxo
	Replace C7_MOEDA       With nMoeda
	Replace C7_TXMOEDA     With nTaxa
	Replace C7_APROV       With cAprov
	Replace C7_CONAPRO     With cConapro
	Replace C7_USER        With cUser
	Replace C7_GRUPCOM     With cGrComPad
	Replace C7_DESPESA     With nDespesa
	Replace C7_VLDESC       With nDesconto//ACB - 24/11/2010
	Replace C7_PO_EIC With cNumPo

	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//쿙a inclusao gravar o DEFAULT para ocampo C7_FILENT que eh obrigatorio�
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	If nOpcao == 1
		Replace C7_FILENT	With	CriaVar('C7_FILENT',.T.)
	Endif
	dbSelectArea("SC1")
	dbSetOrder(1)
	If dbSeek(xFilial("SC1")+cNumSc+cItemSc)

		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//쿙a inclusao gravar o DEFAULT para ocampo C7_FILENT que eh obrigatorio�
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		SC7->C7_CC      := SC1->C1_CC
		SC7->C7_CLVL    := SC1->C1_CLVL
		SC7->C7_CONTA   := SC1->C1_CONTA
		SC7->C7_ITEMCTA := SC1->C1_ITEMCTA

		If nOpcao == 1
			nQuantSC := SC7->C7_QUANT
		Else
			nQuantSC :=  nDIFSC1
		EndIf
		While !Eof() .And. SC1->C1_FILIAL+SC1->C1_NUM+SC1->C1_ITEM == xFilial("SC1")+cNumSC+cItemSc
			If SC1->C1_PRODUTO == cCodProd
				RecLock("SC1",.F.)
				If nOpcao == 1
					nQtQuje	:= SC1->C1_QUJE
					SC1->C1_QUJE	:= SC1->C1_QUJE + IF(nQuantSC>SC1->C1_QUANT-nQtQuje,SC1->C1_QUANT-nQtQuje,nQuantSC)
					SC7->C7_ITEMSC  := SC1->C1_ITEM
					nQuantSC -= IF(nQuantSC>SC1->C1_QUANT-nQtQuje,SC1->C1_QUANT-nQtQuje,nQuantSC)
				Else
					nQtQuje	:= SC1->C1_QUJE
					SC1->C1_QUJE	:= SC1->C1_QUJE - If(nQuantSC>nQtQuje,nQtQuje,nQuantSC)
					nQuantSC -= If(nQuantSC>nQtQuje,nQtQuje,nQuantSC)
				EndIf
				MsUnlock()
			EndIf
			dbSkip()
		Enddo

		If nOpcao == 1
			DbSelectArea("SCX")
			SCX->(DbSetOrder(1)) //-- CX_FILIAL + CX_SOLICIT + CX_ITEMSOL + CX_ITEM
			If SCX->(DbSeek(xFilial("SCX") + cNumSc + cItemSc))
				While SCX->(!Eof()) .And. ((SCX->CX_SOLICIT + SCX->CX_ITEMSOL) == (cNumSc + cItemSc))
					RecLock("SCH", .T.)
						SCH->CH_FILIAL 	:= xFilial("SCH")
						SCH->CH_PEDIDO 	:= cNumPed
						SCH->CH_FORNECE := cFornece
						SCH->CH_LOJA 	:= cLoja
						SCH->CH_ITEMPD 	:= cItem
						SCH->CH_ITEM 	:= SCX->CX_ITEM
						SCH->CH_PERC 	:= SCX->CX_PERC
						SCH->CH_CC 		:= SCX->CX_CC
						SCH->CH_CONTA 	:= SCX->CX_CONTA 
						SCH->CH_ITEMCTA := SCX->CX_ITEMCTA
						SCH->CH_CUSTO1 	:= Round((SC7->C7_TOTAL * (SCX->CX_PERC / 100)), nDecCus1)
						If nMoeda == 2
							SCH->CH_CUSTO2 := Round((SC7->C7_TOTAL * (SCX->CX_PERC / 100)), nDecCus2)
						ElseIf nMoeda == 3 
							SCH->CH_CUSTO3 := Round((SC7->C7_TOTAL * (SCX->CX_PERC / 100)), nDecCus3) 
						ElseIf nMoeda == 4
							SCH->CH_CUSTO4 := Round((SC7->C7_TOTAL * (SCX->CX_PERC / 100)), nDecCus4) 
						ElseIf nMoeda == 5
							SCH->CH_CUSTO5 := Round((SC7->C7_TOTAL * (SCX->CX_PERC / 100)), nDecCus5) 
						EndIf
						SCH->CH_PERC 	:= SCX->CX_PERC
						For nX := 1 To Len(aEntCtb)
							cCpoSCH := "CH_EC" + aEntCtb[nX]
							cCpoSCX := "CX_EC" + aEntCtb[nX]
			
							&("SCH->" + cCpoSCH + "DB") := &("SCX->" + cCpoSCX + "DB")
							&("SCH->" + cCpoSCH + "CR") := &("SCX->" + cCpoSCX + "CR")
						Next nX
					SCH->(MsUnLock())
					SCX->(DbSkip())
				EndDo
			EndIf
		EndIf
	EndIf

	//-- Executa avalia豫o do tipo de Compra
	SC7->C7_TIPCOM := MRetTipCom(,.T.,"PC")

Endif

If nOpcao == 2
	DbSelectArea("SCH")
	SCH->(DbSetOrder(1)) //-- CH_FILIAL + CH_PEDIDO + CH_FORNECE + CH_LOJA + CH_ITEMPD + CH_ITEM
	If SCH->(DbSeek(xFilial("SCH") + cNumPed + cFornece + cLoja + cItem))
		While SCH->(!Eof()) .And. ((SCH->CH_PEDIDO + SCH->CH_FORNECE + SCH->CH_LOJA + SCH->CH_ITEMPD) == (cNumPed + cFornece + cLoja + cItem))
			RecLock("SCH", .F.)
				SCH->CH_CUSTO1 := Round((SC7->C7_TOTAL * (SCH->CH_PERC / 100)), nDecCus1)
				If nMoeda == 2
					SCH->CH_CUSTO2 := Round((SC7->C7_TOTAL * (SCH->CH_PERC / 100)), nDecCus2)
				ElseIf nMoeda == 3 
					SCH->CH_CUSTO3 := Round((SC7->C7_TOTAL * (SCH->CH_PERC / 100)), nDecCus3) 
				ElseIf nMoeda == 4
					SCH->CH_CUSTO4 := Round((SC7->C7_TOTAL * (SCH->CH_PERC / 100)), nDecCus4) 
				ElseIf nMoeda == 5
					SCH->CH_CUSTO5 := Round((SC7->C7_TOTAL * (SCH->CH_PERC / 100)), nDecCus5) 
				EndIf
			SCH->(MsUnLock())
			SCH->(DbSkip())
		EndDo
	EndIf
EndIf

If nOpcao == 1 .And. lAtuSB2
	Reclock("SB2")
	SB2->B2_SALPEDI += SC7->C7_QUANT
Endif
If lAtuSB2
	SB2->(MsUnlock())
EndIf

RestArea(aAreaSCX)
RestArea(aAreaSCH)

If ExistBlock("GRVSC7")
	ExecBlock("GRVSC7",.F.,.F.)
Endif
SC7->(MsUnlock())
RestArea(aArea)

Return 