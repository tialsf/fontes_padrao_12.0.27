#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "MATA162.CH"

#DEFINE CRLF Chr(13) + Chr(10)


//-------------------------------------------------------------------
/*/{Protheus.doc} MATA162
    An�lise de Cota��es
@author leonardo.magalhaes
@since 22/04/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function MATA162()

    Local oBrowse    as Object      //-- Objeto que recebe o BrowseDef

    Private aRotina  as Array       //-- Private que recebe as op��es de Menu


    //-- Inicializa��o das vari�veis
    oBrowse := FwLoadBrw("MATA162")
    aRotina := MenuDef()

    //-- Ativa o Browse principal
    oBrowse:Activate()

    //-- Desativa o Browse principal
    oBrowse:DeActivate()
    oBrowse:Destroy()
    
    //-- Limpa a mem�ria
    FreeObj(oBrowse)
    oBrowse := Nil

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} BrowseDef
    Defini��o do objeto de browse da rotina.

@author leonardo.magalhaes
@since 19/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function BrowseDef()

    Local oRet       as Object       //-- Objeto de retorno do BrowseDef

    Local aLegenda	 as Array       //-- Controle de legenda padr�o
    Local aLegUsr	 as Array       //-- Customiza��o de legenda via ponto de entrada

    Local cUsrFilt   as Character   //-- Filtro de usu�rio para o Browse

    Local lMT162LEG  as Logical     //-- Controla existencia do ponto de entrada MT162LEG
    Local lMT162FIL  as Logical     //-- Controla existencia do ponto de entrada MT162FIL

    Local nX		 as Numeric

    //-- Inicializa��o das vari�veis
    oRet       := FwMBrowse():New()
    aLegenda   := {}
    aLegUsr    := {}
    cUsrFilt   := ""
    lMT162LEG  := ExistBlock("MT162LEG")
    lMT162FIL  := ExistBlock("MT162FIL")
    nX         := 0

    //-- Defini��o da legenda padr�o
    Aadd(aLegenda, {"DHU_STATUS == '1'", "GREEN" , STR0001}) //-- "A analisar"
    Aadd(aLegenda, {"DHU_STATUS == '2'", "YELLOW", STR0002}) //-- "Analisada parcialmente"
    Aadd(aLegenda, {"DHU_STATUS == '3'", "RED"   , STR0003}) //-- "Totalmente analisada"

    //-- Ponto de Entrada para customiza��o da legenda
    If lMT162LEG
        aLegUsr := ExecBlock("MT162LEG", .F., .F., {aLegenda})
        If ValType(aLegUsr) == "A" .And. Len(aLegUsr) > 0
            aLegenda := aClone(aLegUsr)
        EndIf
    EndIf

    For nX := 1 To Len(aLegenda)
        If Len(aLegenda[nX]) >= 3
            oRet:AddLegend(aLegenda[nX][1], aLegenda[nX][2], aLegenda[nX][3])
        EndIf
    Next nX

    oRet:SetAlias("DHU") //-- Tabela DHU (Cabe�alho da Cota��o)
    oRet:SetDescription(STR0004) //-- "An�lise de Cota��o"
    oRet:DisableDetails() //-- Desabilita a apresenta��o dos detalhes no Browse					

    //-- Ponto de entrada para filtragem do Browse
    If lMT162FIL
        cUsrFilt := ExecBlock("MT162FIL", .F., .F., {"DHU"})
        If ValType(cUsrFilt) == "C" .And. !Empty(cUsrFilt)
            oRet:SetFilterDefault(cUsrFilt)
        EndIf
    EndIf

Return oRet


//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
    Defini��o do menu de op��es.

@author leonardo.magalhaes
@since 22/04/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

    Local aRet       as Array   //-- Retorno com as op��es do MenuDef
    Local aRetUsr    as Array   //-- Auxiliar para considerar op��es de usu�rio

    Local lMT162BUT  as Logical //-- Controla existencia do Ponto de Entrada MT162BUT

    //-- Inicializa��o das vari�veis
    aRet       := {}
    aRetUsr    := {}
    lMT162BUT  := ExistBlock("MT162BUT")

    //-- Defini��o das op��es padr�o
    ADD OPTION aRet TITLE STR0005 ACTION "PesqBrw" OPERATION 1 ACCESS 0 //-- "Pesquisar"
    ADD OPTION aRet TITLE STR0006 ACTION "A162Anl" OPERATION 4 ACCESS 0 //-- "Analisar"
    ADD OPTION aRet TITLE STR0017 ACTION "A162Imp" OPERATION 3 ACCESS 0 //-- "Importar"
    ADD OPTION aRet TITLE STR0007 ACTION "MsDocument('DHU', DHU->(RecNo()), 2)" OPERATION 2 ACCESS 0 //-- "Conhecimento"

    //-- Ponto de entrada utilizado para inserir novas op��es
    If lMT162BUT
        aRetUsr := ExecBlock("MT162BUT", .F., .F., {aRet})
        If ValType(aRetUsr) == "A" .And. Len(aRetUsr) > 0
            aRet := aClone(aRetUsr)
        EndIf
    EndIf

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
    Defini��o do modelo de dados.

@author leonardo.magalhaes
@since 22/04/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

    Local oModel   as Object //-- Modelo de dados
    Local oStruDHU as Object //-- Estrutura do cabe�alho
    Local oStruDHV as Object //-- Estrutura do grid de produtos
    Local oStruSC8 as Object //-- Estrutura do grid de propostas de fornecimento

    //-- Inicializa��o das vari�veis
    oModel   := MPFormModel():New("MATA162", /*bPre*/, /*bPos*/, /*bCommit*/, /*bCancel*/)
    oStruDHU := FWFormStruct(1, "DHU")
    oStruDHV := FWFormStruct(1, "DHV")
    oStruSC8 := FWFormStruct(1, "SC8")

    //-- Campos adicionais - DHV
    oStruDHV:AddField( ;
                        AllTrim("") , ; 							// [01] C Titulo do campo
                        AllTrim("") , ; 							// [02] C ToolTip do campo
                        "DHV_LEGEND", ;              	 		    // [03] C identificador (ID) do Field
                        "C" , ;                     				// [04] C Tipo do campo
                        50 , ;                      				// [05] N Tamanho do campo
                        0 , ;                       				// [06] N Decimal do campo
                        NIL , ;                     				// [07] B Code-block de valida��o do campo
                        NIL , ;                     				// [08] B Code-block de valida��o When do campo
                        NIL , ;                     				// [09] A Lista de valores permitido do campo
                        NIL , ;                     				// [10] L Indica se o campo tem preenchimento obrigat�rio
                        {|| A162SetLeg("DHV_LEGEND", oModel)}, ;	// [11] B Code-block de inicializacao do campo
                        NIL , ;                     				// [12] L Indica se trata de um campo chave
                        NIL , ;                     				// [13] L Indica se o campo pode receber valor em uma opera��o de update.
                        .T. ) 

    //-- Campos adicionais - SC8
    oStruSC8:AddField( ;
                        AllTrim("") , ; 							// [01] C Titulo do campo
                        AllTrim("") , ; 							// [02] C ToolTip do campo
                        "C8_MARKW" , ;              	 		    // [03] C identificador (ID) do Field
                        "C" , ;                     				// [04] C Tipo do campo
                        50 , ;                      				// [05] N Tamanho do campo
                        0 , ;                       				// [06] N Decimal do campo
                        NIL , ;                     				// [07] B Code-block de valida��o do campo
                        NIL , ;                     				// [08] B Code-block de valida��o When do campo
                        NIL , ;                     				// [09] A Lista de valores permitido do campo
                        NIL , ;                     				// [10] L Indica se o campo tem preenchimento obrigat�rio
                        {||}, ;	                                    // [11] B Code-block de inicializacao do campo
                        NIL , ;                     				// [12] L Indica se trata de um campo chave
                        NIL , ;                     				// [13] L Indica se o campo pode receber valor em uma opera��o de update.
                        .T. ) 

    oStruSC8:AddField( ;
                        AllTrim("") , ; 							// [01] C Titulo do campo
                        AllTrim("") , ; 							// [02] C ToolTip do campo
                        "C8_LEGEND" , ;              	 		    // [03] C identificador (ID) do Field
                        "C" , ;                     				// [04] C Tipo do campo
                        50 , ;                      				// [05] N Tamanho do campo
                        0 , ;                       				// [06] N Decimal do campo
                        NIL , ;                     				// [07] B Code-block de valida��o do campo
                        NIL , ;                     				// [08] B Code-block de valida��o When do campo
                        NIL , ;                     				// [09] A Lista de valores permitido do campo
                        NIL , ;                     				// [10] L Indica se o campo tem preenchimento obrigat�rio
                        {|| A162SetLeg("C8_LEGEND", oModel)}, ;	    // [11] B Code-block de inicializacao do campo
                        NIL , ;                     				// [12] L Indica se trata de um campo chave
                        NIL , ;                     				// [13] L Indica se o campo pode receber valor em uma opera��o de update.
                        .T. ) 
    
    //-- Adiciona ao modelo um submodelo de edi��o por campo (FormField)
    oModel:AddFields("DHUMASTER" /*cId*/, /*cOwner*/, oStruDHU /*oModelStruct*/, /*bPre*/, /*bPost*/, /*bLoad*/)
    
    //-- Adiciona ao modelo um submodelo de edi��o por grid (FormGrid)
    oModel:AddGrid("DHVDETAIL" /*cId*/, "DHUMASTER" /*cOwner*/, oStruDHV /*oModelStruct*/, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bPost*/, /*bLoad*/ )
    oModel:AddGrid("SC8DETAIL" /*cId*/, "DHVDETAIL" /*cOwner*/, oStruSC8 /*oModelStruct*/, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bPost*/, /*bLoad*/)

    //-- Atribui a chave prim�ria da entidade Modelo
    oModel:SetPrimaryKey({"DHU_FILIAL", "DHU_NUM"})

    //-- Atribui o crit�rio de relacionamento entre os submodelo
    oModel:SetRelation("DHVDETAIL", {{"DHV_FILIAL", "xFilial('DHV')"}, {"DHV_NUM", "DHU_NUM"}}, DHV->(IndexKey(1))) //-- DHV_FILIAL, DHV_NUM, DHV_ITEM, DHV_PRODUTO
    oModel:SetRelation("SC8DETAIL", {{"C8_FILIAL", "xFilial('SC8')"}, {"C8_NUM", "DHV_NUM"}, {"C8_IDENT", "DHV_ITEM"},{"C8_PRODUTO", "DHV_CODPRO"}}, SC8->(IndexKey(3))) //-- C8_FILIAL, C8_NUM, C8_FORNECE, C8_LOJA, C8_ITEM, C8_NUMPRO, C8_ITEMGRD

    //-- Atribui uma descri��o ao modelo
    oModel:SetDescription(STR0004) //-- "An�lise de Cota��o"

    //-- Define que determinados campos n�o podem ser repetidos dentro do grid
    oModel:GetModel("DHVDETAIL"):SetUniqueLine({"DHV_ITEM", "DHV_CODPRO"})
    oModel:GetModel("SC8DETAIL"):SetUniqueLine({"C8_NUM", "C8_FORNECE", "C8_LOJA", "C8_FORNOME", "C8_NUMPRO"})

    //-- Define que os grids n�o podem receber inser��o/exclus�o/atualiza��o de linhas
    oModel:GetModel("DHVDETAIL"):SetNoInsertLine(.T.)
    oModel:GetModel("DHVDETAIL"):SetNoDeleteLine(.T.)
    oModel:GetModel("DHVDETAIL"):SetNoUpdateLine(.T.)

    oModel:GetModel("SC8DETAIL"):SetNoInsertLine(.T.)
    oModel:GetModel("SC8DETAIL"):SetNoUpdateLine(.T.)
    oModel:GetModel("SC8DETAIL"):SetNoUpdateLine(.T.)

Return oModel


//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
    Defini��o da interface visual.

@author leonardo.magalhaes
@since 22/04/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

    Local oModel   as Object //-- Modelo de dados
    Local oView    as Object //-- Modelo de dados
    Local oStruDHU as Object //-- Estrutura do header
    Local oStruDHV as Object //-- Estrutura do grid de Produtos
    Local oStruSC8 as Object //-- Estrutura do grid de Fornecedores

    //-- Inicializa��o das vari�veis
    oModel   := FWLoadModel("MATA162")
    oView    := FWFormView():New()
    oStruDHU := FWFormStruct(2, "DHU", {|cCampo| !(AllTrim(cCampo) $ "DHU_STATUS|DHU_ORIGEM")})
    oStruDHV := FWFormStruct(2, "DHV", {|cCampo| !(AllTrim(cCampo) $ "DHV_NUM|DHV_STATUS|DHV_ORIGEM")})
    oStruSC8 := FWFormStruct(2, "SC8", {|cCampo| !(AllTrim(cCampo) $ "C8_NUM|C8_ITEM|C8_PRODUTO|C8_IDENT|C8_QUANT|C8_UM|C8_SEGUM|C8_QTSEGUM|C8_MARKAUD|C8_CODED|C8_NUMPR|C8_NUMSC|C8_ITEMSC|C8_ITEMGRD|C8_NUMCON|C8_NUMPED|C8_ITEMPED|C8_GRADE|C8_GRUPCOM|C8_WF|C8_TPDOC|C8_OK|C8_ITSCGRD|C8_ACCNUM|C8_ACCITEM|C8_ORIGEM") .And. !(AllTrim(cCampo) == "C8_DESCRI")})

    //-- Campos adicionais - DHV
	oStruDHV:AddField( ;                                // Ord. Tipo Desc.
	                     "DHV_LEGEND" , ;             	// [01] C Nome do Campo
	                     "00" , ;                     	// [02] C Ordem
	                     AllTrim("") , ;				// [03] C Titulo do campo
	                     "" , ;					        // [04] C Descri��o do campo
	                     { "" } , ;   		    	    // [05] A Array com Help
	                     "C" , ;                      	// [06] C Tipo do campo
	                     "@BMP" , ;                   	// [07] C Picture
	                     NIL , ;                      	// [08] B Bloco de Picture Var
	                     "" , ;                       	// [09] C Consulta F3
	                     .F. , ;                      	// [10] L Indica se o campo � evit�vel
	                     NIL , ;                      	// [11] C Pasta do campo
	                     NIL , ;                      	// [12] C Agrupamento do campo
                         NIL , ;                      	// [13] A Lista de valores permitido do campo (Combo)
                         NIL , ;                      	// [14] N Tamanho Maximo da maior op��o do combo
                         NIL , ;                        // [15] C Inicializador de Browse
                         .T. , ;                      	// [16] L Indica se o campo � virtual
                         NIL ) 

    //-- Campos adicionais - SC8
	oStruSC8:AddField( ;                                // Ord. Tipo Desc.
	                     "C8_MARKW" , ;             	// [01] C Nome do Campo
	                     "00" , ;                     	// [02] C Ordem
	                     AllTrim("") , ;				// [03] C Titulo do campo
	                     "" , ;					        // [04] C Descri��o do campo
	                     { "" } , ;   		    	    // [05] A Array com Help
	                     "C" , ;                      	// [06] C Tipo do campo
	                     "@BMP" , ;                   	// [07] C Picture
	                     NIL , ;                      	// [08] B Bloco de Picture Var
	                     "" , ;                       	// [09] C Consulta F3
	                     .F. , ;                      	// [10] L Indica se o campo � evit�vel
	                     NIL , ;                      	// [11] C Pasta do campo
	                     NIL , ;                      	// [12] C Agrupamento do campo
                         NIL , ;                      	// [13] A Lista de valores permitido do campo (Combo)
                         NIL , ;                      	// [14] N Tamanho Maximo da maior op��o do combo
                         NIL , ;                        // [15] C Inicializador de Browse
                         .T. , ;                      	// [16] L Indica se o campo � virtual
                         NIL )

	oStruSC8:AddField( ;                                // Ord. Tipo Desc.
	                     "C8_LEGEND" , ;             	// [01] C Nome do Campo
	                     "01" , ;                     	// [02] C Ordem
	                     AllTrim("") , ;				// [03] C Titulo do campo
	                     "" , ;					        // [04] C Descri��o do campo
	                     { "" } , ;   		    	    // [05] A Array com Help
	                     "C" , ;                      	// [06] C Tipo do campo
	                     "@BMP" , ;                   	// [07] C Picture
	                     NIL , ;                      	// [08] B Bloco de Picture Var
	                     "" , ;                       	// [09] C Consulta F3
	                     .F. , ;                      	// [10] L Indica se o campo � evit�vel
	                     NIL , ;                      	// [11] C Pasta do campo
	                     NIL , ;                      	// [12] C Agrupamento do campo
                         NIL , ;                      	// [13] A Lista de valores permitido do campo (Combo)
                         NIL , ;                      	// [14] N Tamanho Maximo da maior op��o do combo
                         NIL , ;                        // [15] C Inicializador de Browse
                         .T. , ;                      	// [16] L Indica se o campo � virtual
                         NIL ) 
    
    //-- Define o relacionamento da View com o Model
    oView:SetModel(oModel)

    //-- Adiciona ao view um formul�rio do tipo FormFields
    oView:AddField("VIEW_DHU", oStruDHU, "DHUMASTER")

    //-- Adiciona ao view um formul�rio do tipo FwFormGrid
    oView:AddGrid("VIEW_DHV" , oStruDHV, "DHVDETAIL")
    oView:AddGrid("VIEW_SC8" , oStruSC8, "SC8DETAIL")

    //-- Box do cabe�alho da cota��o
    oView:CreateHorizontalBox("TOPBOX", 10)
    oView:CreateFolder("TOPFLD", "TOPBOX")
    oView:AddSheet("TOPFLD", "TOPSHEET", STR0008) //-- "Cabe�alho da Cota��o "

    //-- Box dos produtos
    oView:CreateHorizontalBox("MIDBOX", 40)
    oView:CreateVerticalBox('MIDVERT1', 100, "MIDBOX")
    oView:CreateVerticalBox("MIDVERT2", 100, "MIDBOX", .T.)
    oView:AddOtherObject("OTHER_PANEL1", {|oPanel| A162BtGrd(oPanel, oView, "DHVDETAIL")}) //-- Bot�es do grid de Produtos

    //-- Box das propostas de fornecimento
    oView:CreateHorizontalBox("DOWNBOX", 50)
    oView:CreateVerticalBox('DOWNVERT1', 100, "DOWNBOX")
    oView:CreateVerticalBox("DOWNVERT2", 100, "DOWNBOX", .T.)
    oView:AddOtherObject("OTHER_PANEL2", {|oPanel| A162BtGrd(oPanel, oView, "SC8DETAIL")}) //-- Bot�es do grid de Propostas de Fornecimento

    //-- Relaciona o ID da View com o box para exibi��o
    oView:SetOwnerView("VIEW_DHU", "TOPBOX")
    oView:SetOwnerView("VIEW_DHV", "MIDVERT1")
    oView:SetOwnerView("VIEW_SC8", "DOWNVERT1")
    oView:SetOwnerView("OTHER_PANEL1", "MIDVERT2")
    oView:SetOwnerView("OTHER_PANEL2", "DOWNVERT2")

    //-- Define propriedades de filtro e pesquisa nos grids de produtos e propostas
    oView:SetViewProperty("*", "ENABLENEWGRID") 
    oView:SetViewProperty("DHVDETAIL", "GRIDNOORDER") 
    oView:SetViewProperty("DHVDETAIL", "GRIDFILTER", {.T.}) 
    oView:SetViewProperty("*", "GRIDSEEK", {.T.})

    //-- T�tulos
    oView:EnableTitleView("VIEW_DHU", STR0008) //-- "Cabe�alho da Cota��o"
    oView:EnableTitleView("VIEW_DHV", STR0009) //-- "Produtos"
    oView:EnableTitleView("VIEW_SC8", STR0010) //-- "Propostas de Fornecimento"

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} A162Anl
    Realiza a an�lise de cota��o.

@author leonardo.magalhaes
@since 22/04/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function A162Anl()

    FwExecView(STR0004, "MATA162", MODEL_OPERATION_UPDATE) //-- "An�lise de Cota��o"

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} A162Imp
    Realiza a chamada das fun��es de importa��o de dados para as 
    tabelas DHU (Cabe�alho de Cota��es) e DHV (Itens de Cota��es).

@author leonardo.magalhaes
@since 13/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function A162Imp()

    Processa({|| A162DHV(), A162DHU()}, STR0018, STR0019, .F.) //"Aguarde" "Buscando dados para importa��o"

Return Nil 


//-------------------------------------------------------------------
/*/{Protheus.doc} A162DHV
    Executa a importa��o de dados para a tabela DHV 
    (Itens de Cota��es).

@author leonardo.magalhaes
@since 13/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function A162DHV()

    Local cAliasAux as Character
    Local cFilDHV   as Character
    Local cFilSB1   as Character

    Local nTotReg   as Numeric

    If FWAliasInDic("DHV", .F.)

        cAliasAux := GetNextAlias()
        cFilDHV   := xFilial("DHV")
        cFilSB1   := xFilial("SB1")

        BeginSQL Alias cAliasAux		
            SELECT 
            DISTINCT    C8_NUM,
                        C8_IDENT,
                        C8_PRODUTO,
                        C8_QUANT,
                        C8_UM,
                        C8_SEGUM,
                        C8_QTSEGUM
            FROM        %Table:SC8% SC8
            WHERE       C8_FILIAL        = %xFilial:SC8% 
                        AND SC8.%NotDel%		
        EndSQL

        Count To nTotReg
        (cAliasAux)->(DbGoTop())
        ProcRegua(nTotReg)

        DbSelectArea("DHV")
        DHV->(DbSetOrder(1))

        If nTotReg > 0
            Begin Transaction
                While !(cAliasAux)->(Eof())
                    If !(DHV->(DbSeek(cFilDHV + (cAliasAux)->C8_NUM + (cAliasAux)->C8_IDENT + (cAliasAux)->C8_PRODUTO)))
                        RecLock("DHV", .T.)
                            DHV->DHV_FILIAL  := cFilDHV
                            DHV->DHV_NUM     := (cAliasAux)->C8_NUM
                            DHV->DHV_STATUS  := "1" //-- Reavaliar m�todo de defini��o de status para registros do legado
                            DHV->DHV_ITEM    := (cAliasAux)->C8_IDENT
                            DHV->DHV_CODPRO  := (cAliasAux)->C8_PRODUTO
                            DHV->DHV_DESCR   := Posicione("SB1", 1, cFilSB1 + (cAliasAux)->C8_PRODUTO, "B1_DESC")
                            DHV->DHV_ORIGEM  := FunName()
                            DHV->DHV_UM      := (cAliasAux)->C8_UM
                            DHV->DHV_QUANT   := (cAliasAux)->C8_QUANT
                            DHV->DHV_SEGUM   := (cAliasAux)->C8_SEGUM
                            DHV->DHV_QTSEGUM := (cAliasAux)->C8_QTSEGUM
                        DHV->(MsUnlock())
                    EndIf
                    (cAliasAux)->(DbSkip())
                EndDo
            End Transaction
        Else
             Help(,, "A162NOIMP",, STR0020, 1, 0) //-- "N�o h� dados a serem importados!"
        EndIf

        (cAliasAux)->(DbCloseArea())

    EndIf

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} A162DHU
    Executa a importa��o de dados para a tabela DHU 
    (Cabe�alho de Cota��es).

@author leonardo.magalhaes
@since 13/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Function A162DHU()

    Local cAliasAux as Character
    Local cFilDHU   as Character

    Local nTotReg   as Numeric

    If FWAliasInDic("DHU", .F.)
        
        cAliasAux := GetNextAlias()
        cFilDHU   := xFilial("DHU")

        BeginSQL Alias cAliasAux		
            SELECT 
            DISTINCT    C8_NUM
            FROM        %Table:SC8% SC8
            WHERE       C8_FILIAL        = %xFilial:SC8% 
                        AND SC8.%NotDel%		
        EndSQL

        Count To nTotReg
        (cAliasAux)->(DbGoTop())
        ProcRegua(nTotReg)

        DbSelectArea("DHU")
        DHU->(DbSetOrder(1))

        If nTotReg > 0
            Begin Transaction
                While !(cAliasAux)->(Eof())
                    If !(DHU->(DbSeek(cFilDHU + (cAliasAux)->C8_NUM)))
                        RecLock("DHU", .T.)
                            DHU->DHU_FILIAL := cFilDHU
                            DHU->DHU_NUM    := (cAliasAux)->C8_NUM
                            DHU->DHU_TPDOC  := "1"
                            DHU->DHU_STATUS := "1" //-- Reavaliar m�todo de defini��o de status para registros do legado
                            DHU->DHU_ORIGEM := FunName() 
                        DHU->(MsUnlock())
                    EndIf
                    (cAliasAux)->(DbSkip())
                EndDo
            End Transaction
        Else
            Help(,, "A162NOIMP",, STR0020, 1, 0) //-- "N�o h� dados a serem importados!"
        EndIf

        (cAliasAux)->(DbCloseArea())

    EndIf

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} A162BtGrd
    Bot�es do grid de Produtos.

@author leonardo.magalhaes
@since 07/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function A162BtGrd(oPanel, oView, cIdGrid)

    Local oModel as Object //-- Modelo de dados utilizado na carga do grid

    Local lWhen  as Logical //-- Controla se os bot�es estar�o habilitados ou n�o

    Default oPanel  := Nil
    Default oView   := FwViewActive()
    Default cIdGrid := ""

    //-- Inicializa��o das vari�veis
    oModel := FWModelActive()
    lWhen  := .F.

    If oModel <> Nil .And. oModel:GetId() ==  "MATA162"
        
        lWhen  := !(oModel:GetOperation() == MODEL_OPERATION_VIEW)

        If cIdGrid == "DHVDETAIL" //-- Grid de Produtos
            @ oPanel:nTop + 15, 5 Button STR0011 + CRLF + STR0012 + CRLF + STR0013 Size 40, 25 Message STR0015 Pixel Action A162PosP(oModel) of oPanel When lWhen //-- "Hist�rico do Produto" "Consultar o hist�rico do produto"
        ElseIf cIdGrid == "SC8DETAIL" //-- Grid de Propostas de Fornecimento
            @ oPanel:nTop + 15, 5 Button STR0011 + CRLF + STR0012 + CRLF + STR0014 Size 40, 25 Size 55, 20 Message STR0016 Pixel Action A162PosF(oModel) of oPanel When lWhen //-- "Hist�rico do Fornecedor" "Consultar o hist�rico do fornecedor"
        EndIf

    EndIf 

Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} A162PosP
    Executa a consulta de posi��o do produto (hist�rico).

@author leonardo.magalhaes
@since 07/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function A162PosP(oModel)
    
    Local aArea    as Array
     
    Local cCodProd as Character

    Default oModel := FwModelActive()

    //-- Inicializa��o das vari�veis
    aArea    := {}
    cCodProd := ""

    If oModel <> Nil .And. oModel:GetId() == "MATA162"

        aArea    := GetArea()
        cCodProd := oModel:GetModel("DHVDETAIL"):GetValue("DHV_CODPRO")

        MaFisSave()
        MaFisEnd()

        If !FwIsInCallStack("MACOMVIEW")
            If !Empty(cCodProd)
                MaComView(cCodProd)
            EndIf
        EndIf

        MaFisRestore()
        RestArea(aArea)

    EndIf

    FwFreeArray(aArea)

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} A162PosF
    Executa a consulta de posi��o do fornecedor (hist�rico).

@author leonardo.magalhaes
@since 07/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function A162PosF(oModel)
    
    Local aArea	  as Array

    Local cCodFor as Character
    Local cLojFor as Character  

    Default oModel := FwModelActive()

    //-- Inicializa��o das vari�veis
    aArea   := {}
    cCodFor := ""
    cLojFor := ""
    
    If oModel <> Nil .And. oModel:GetId() == "MATA162" 
    
        aArea   := GetArea()
        cCodFor := oModel:GetModel("SC8DETAIL"):GetValue("C8_FORNECE")
        cLojFor := oModel:GetModel("SC8DETAIL"):GetValue("C8_LOJA")

        DbSelectArea("SA2")
        SA2->(DbSetOrder(1)) //-- A2_FILIAL, A2_COD, A2_LOJA
        If SA2->(DbSeek(xFilial("SA2") + cCodFor + cLojFor))
            If Pergunte("FIC030", .T.) 	
                FinC030("Fc030Con")
            EndIf
        EndIf

        RestArea(aArea)

    EndIf

    FwFreeArray(aArea)

Return Nil


//-------------------------------------------------------------------
/*/{Protheus.doc} A162SetLeg
    Define a legenda para os campos de legenda.

@author leonardo.magalhaes
@since 07/05/2020
@version 1.0
@return Nil
/*/
//-------------------------------------------------------------------
Static Function A162SetLeg(cIdField, oModel)

    Local cRet as Character

    Default cIdField := ""
    Default oModel   := FWModelActive()

    //-- Inicializa��o de vari�veis
    cRet := ""

    If oModel <> Nil .And. oModel:GetId() == "MATA162"
        If cIdField == "C8_LEGEND"
            If Empty(SC8->C8_NUMCON)
                cRet := "BR_GREEN"
            Else
                cRet := "BR_RED"
            EndIf 
        ElseIf cIdField == "DHV_LEGEND"
            If DHV->DHV_STATUS == "1"
                cRet := "BR_GREEN"
            Else
                cRet := "BR_RED"
            EndIf 
        EndIf
    EndIf

Return cRet