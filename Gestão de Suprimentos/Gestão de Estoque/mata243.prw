#INCLUDE	'TOTVS.CH'
//----------------------------------------
/*/{Protheus.doc}	AtuDesSD3
Gera SD3	apura��o	de	custo
@author
@since 13/03/2015
@version	1.0
aArrSD3[1] - Tipo	movimenta��o
aArrSD3[2] - Documento
aArrSD3[3] - Produto
aArrSD3[4] - Lote
aArrSD3[5] - Sub-Lote
aArrSD3[6] - Quantidade
aArrSD3[7] - %	de	rateio do custo
aArrSD3[8] - Local
aArrSD3[9] - Endere�o
aArrSD3[10] - Identificador DCF
aArrSD3[11] - Cf
aArrSD3[12] - Chave
aArrSD3[13] - Servico
aArrSD3[14] - Status Servico
@Obs   Utilizado pelos processo
	  - Montagem e desmontagem
	  - Transferencia entre armazens
	  - Movimento interno requisi��o 
/*/
//----------------------------------------
Function AtuDesSD3(aArrSD3,nOperac)
Local aAreaAnt   := GetArea() 
Local lLocS3     := FindFunction("LocalizS3")
Local cCusMed    := GetMv("MV_CUSMED")
Local aLockSB2   := {}
Local aCtbDia    := {}
Local aSvCus     := {}
Local aSoma      := {}
Local nHdlPrv    := 0
Local nI         := 0
Local nX         := 0
Local cLoteEst   := Iif(Found(),Trim(X5Descri()),"EST ")
Local nTamDec    := TamSx3("D3_CUSTO1")[2]
Local nTotal     := 0
Local cArquivo   := ""
Local lContinua  := .T.
Local cTm        := ""
Local cDocumento := ""
Local cProduto   := ""
Local cLoteCtl   := ""
Local cNumLote   := ""
Local nQuant     := 0
Local nRateio    := ""
Local cLocal     := ""
Local cEndereco  := ""
Local cIdDCF     := ""
Local lMontagem  := (nOperac == "1")

Private cNumSeq	:=	""

	//----------------------+
	// Inicializa variaveis |
	//----------------------+
	cTm        := aArrSD3[1][1]  // Tipo movimenta��o
	cDocumento := aArrSD3[1][2]  // Documento
	cProduto   := aArrSD3[1][3]  // Produto
	cLoteCtl   := aArrSD3[1][4]  // Lote
	cNumLote   := aArrSD3[1][5]  // Sub-Lote
	nQuant     := aArrSD3[1][6]  // Quantidade
	nRateio    := aArrSD3[1][7]  // % de rateio do custo
	cLocal     := aArrSD3[1][8]  // Local
	cEndereco  := aArrSD3[1][9]  // Endere�o
	cIdDCF     := aArrSD3[1][10] // Id DCF
	cCf        := aArrSD3[1][11] // cCf
	cChave     := aArrSD3[1][12] // Chave
	cServico   := aArrSD3[1][13] // Servico WMS
	cStatus    := aArrSD3[1][14] // Status servico WMS
	cRegra     := aArrSD3[1][15] // Regra WMS
	//-----------------------------------------------+
	// Verifica se o custo medio � calculado On-Line |
	//-----------------------------------------------+
	If	cCusMed == "O"
		//----------------------------------------------------+
		// Se necessario cria o cabecalho do arquivo de prova |
		//----------------------------------------------------+
		nHdlPrv := HeadProva(cLoteEst,"WMSA510",Subs(cUsuario,7,6),@cArquivo)
		If	nHdlPrv <= 0
			lContinua := .F.
		EndIf
	EndIf
	If	lContinua
		//---------------------------+
		// Tratamento para Dead-Lock |
		//---------------------------+
		For nX := 1	to	Len(aArrSD3)
			If aScan(aLockSB2,aArrSD3[nX][3]+aArrSD3[nX][8]) == 0
				aAdd(aLockSB2,aArrSD3[nX][3]+aArrSD3[nX][8])
			EndIf
		Next nX
		//---------------------------+
		// Tratamento para Dead-Lock |
		//---------------------------+
		If	MultLock("SB2",aLockSB2,1)
			//-----------------------------------------------+
			// Pega o proximo numero sequencial de movimento |
			//-----------------------------------------------+
			If !lMontagem
				cNumSeq := ProxNum()
			EndIf
			//---------------------------------------+
			// Atualiza arquivo de saldos em estoque |
			//---------------------------------------+
			dbSelectArea("SB1")
			MsSeek(xFilial("SB1")+cProduto)
			dbSelectArea("SB2")
			MsSeek(xFilial("SB2")+cProduto+cLocal)
			If	Eof()
				CriaSB2(cProduto,cLocal)
			EndIf
			If Rastro(cProduto)
				dbSelectArea("SB8")
				SB8->(dbSetOrder(2))
				SB8->(dbSeek(xFilial("SB8")+cNumLote+cLoteCtl+cProduto)) //B8_FILIAL+B8_NUMLOTE+B8_LOTECTL+B8_PRODUTO+B8_LOCAL+DTOS(B8_DTVALID)
			EndIf
			RecLock("SD3",.T.)
			Replace D3_FILIAL  With	xFilial("SD3")
			Replace D3_TM      With cTm	
			Replace D3_DOC     With cDocumento
			Replace D3_COD     With cProduto
			Replace D3_QUANT   With nQuant
			Replace D3_QTSEGUM With ConvUm(cProduto,nQuant,0,2)
			Replace D3_RATEIO  With nRateio
			Replace D3_LOCAL   With cLocal
			Replace D3_EMISSAO With dDataBase
			Replace D3_UM      With SB1->B1_UM
			Replace D3_GRUPO   With SB1->B1_GRUPO
			Replace D3_NUMSEQ  With Iif(!lMontagem,cNumSeq,ProxNum())
			Replace D3_SEGUM   With SB1->B1_SEGUM
			Replace D3_CF      With cCf
			Replace D3_CHAVE   With cChave
			Replace D3_TIPO    With SB1->B1_TIPO
			Replace D3_CONTA   With SB1->B1_CONTA
			Replace D3_ITEMCTA With SB1->B1_ITEMCC
			Replace D3_CLVL    With SB1->B1_CLVL
			Replace D3_CC      With SB1->B1_CC
			Replace D3_IDDCF   With cIdDCF
			Replace D3_SERVIC  With cServico
			Replace D3_STSERV  With cStatus
			Replace D3_REGWMS  With cRegra
			Replace D3_USUARIO With CUSERNAME
			If Rastro(cProduto)
				Replace D3_LOTECTL With cLoteCtl
				Replace D3_NUMLOTE With IIf(Rastro(cProduto,"S"),cNumLote,CriaVar("D3_NUMLOTE"))
				Replace D3_DTVALID With SB8->B8_DTVALID
				Replace D3_POTENCI With SB8->B8_POTENCI
			EndIf

			//---------------------------------+
			//	Pega os 15 custos	medios atuais |
			//---------------------------------+
			aCM := PegaCMAtu(SD3->D3_COD,SD3->D3_LOCAL)
			//-------------------------------+
			// Grava o custo da movimentacao |
			//-------------------------------+
			aCusto := GravaCusD3(aCM)
			//---------------------------------------------------+
			// Atualiza o saldo atual (VATU) com os dados do SD3 |
			//---------------------------------------------------+
			If	!B2AtuComD3(aCusto)
				If !lMontagem
					aSvCus := AClone(aCusto)
					aSoma  := AClone(aCusto)
				EndIf
				//-----------------------------------------------+
				// Verifica se o custo medio � calculado On-Line |
				//-----------------------------------------------+
				If	cCusMed == "O"
					//---------------------------------------+
					// Gera o lancamento no arquivo de prova |
					//---------------------------------------+
					nTotal += DetProva(nHdlPrv,"670","WMSA510",cLoteEst)
				EndIf
				If lMontagem
					cNumSeq := ProxNum()
				EndIf
				For nI := 2	To	Len(aArrSD3)
					//----------------------+
					// Inicializa variaveis |
					//----------------------+
					cTm        := aArrSD3[nI][1] //	Tipo movimenta��o (999/499)
					cDocumento := aArrSD3[nI][2] //	Documento
					cProduto   := aArrSD3[nI][3] //	Produto
					cLoteCtl   := aArrSD3[nI][4] //	Lote
					cNumLote   := aArrSD3[nI][5] //	Sub-Lote
					nQuant     := aArrSD3[nI][6] //	Quantidade
					nRateio    := aArrSD3[nI][7] //	% de rateio do custo
					cLocal     := aArrSD3[nI][8] //	Local
					cEndereco  := aArrSD3[nI][9] //	Endere�o
					cIdDCF     := aArrSD3[nI][10] // Id DCF
					cCf        := aArrSD3[nI][11] // Cf
					cChave     := aArrSD3[nI][12] // Chave
					cServico   := aArrSD3[nI][13] // Servico WMS
					cStatus    := aArrSD3[nI][14] // Status servico WMS			
					cRegra     := aArrSD3[nI][15] // Regra WMS
					//-- Recria produto se n�o existir
					dbSelectArea("SB1")
					MsSeek(xFilial("SB1")+cProduto)
					dbSelectArea("SB2")
					If	!(MsSeek(xFilial("SB2")+cProduto+cLocal))
						CriaSB2(cProduto,cLocal)
					EndIf
					If Rastro(cProduto)
						dbSelectArea("SB8")
						SB8->(dbSetOrder(2))
						SB8->(dbSeek(xFilial("SB8")+cNumLote+cLoteCtl+cProduto)) //B8_FILIAL+B8_NUMLOTE+B8_LOTECTL+B8_PRODUTO+B8_LOCAL+DTOS(B8_DTVALID)
					EndIf					
					RecLock("SD3",.T.)
					Replace D3_FILIAL  With xFilial("SD3")
					Replace D3_TM      With cTm
					Replace D3_DOC     With cDocumento
					Replace D3_COD     With cProduto
					Replace D3_QUANT   With nQuant
					Replace D3_QTSEGUM With ConvUm(SB1->B1_COD,nQuant,0,2)
					Replace D3_RATEIO  With nRateio
					Replace D3_LOCAL   With cLocal
					Replace D3_EMISSAO With dDataBase
					Replace D3_UM      With SB1->B1_UM
					Replace D3_GRUPO   With SB1->B1_GRUPO
					Replace D3_NUMSEQ  With cNumSeq
					Replace D3_SEGUM   With SB1->B1_SEGUM
					Replace D3_CF      With cCf
					Replace D3_CHAVE   With cChave
					Replace D3_TIPO    With SB1->B1_TIPO
					Replace D3_CONTA   With SB1->B1_CONTA
					Replace D3_ITEMCTA With SB1->B1_ITEMCC
					Replace D3_CLVL    With SB1->B1_CLVL
					Replace D3_CC      With SB1->B1_CC
					Replace D3_IDDCF   With cIdDCF
					Replace D3_SERVIC  With cServico
					Replace D3_STSERV  With cStatus
					Replace D3_REGWMS  With cRegra					
					Replace D3_USUARIO With CUSERNAME
					If Rastro(cProduto)
						Replace D3_LOTECTL With cLoteCtl
						Replace D3_NUMLOTE With IIf(Rastro(cProduto,"S"),cNumLote,CriaVar("D3_NUMLOTE"))
						Replace D3_DTVALID With SB8->B8_DTVALID
						Replace D3_POTENCI With SB8->B8_POTENCI
					EndIf
					
					If	!__lPyme	.Or. (lLocS3 .And. LocalizS3())
						Replace D3_LOCALIZ With	cEndereco
					EndIf

					//--------------------------------------------+
					//	Grava	o custo da movimentacao	com rateio |
					//--------------------------------------------+
					If !lMontagem
						aCusto := {0,0,0,0,0}
						For nX := 1	To	5
							cCampo := "D3_CUSTO"+StrZero(nX,1,0)
	
							aCusto[nX] := Round(aSvCus[nX] * nRateio / 100,nTamDec)
							aSoma[nX] -= aCusto[nX]
	
							Replace &(cCampo)	With aCusto[nX]
						Next nX
					Else
						//---------------------------------+
						//	Pega os 15 custos	medios atuais |
						//---------------------------------+
						aCM := PegaCMAtu(SD3->D3_COD,SD3->D3_LOCAL)
						//---------------------------------+
						//	Grava	o custo da movimentacao	|
						//---------------------------------+
						aCusto := GravaCusD3(aCM)
					EndIf
					
					//-----------------------------------------------------+
					//	Atualiza	o saldo atual (VATU)	com os dados do SD3 |
					//-----------------------------------------------------+
					If	!B2AtuComD3(aCusto)
						//-----------------------------------------------+
						// Verifica se o custo medio � calculado On-Line |
						//-----------------------------------------------+
						If	cCusMed == "O"
							//---------------------------------------+
							// Gera o lancamento no arquivo de prova |
							//---------------------------------------+
							nTotal += DetProva(nHdlPrv,"672","WMSA510",cLoteEst)
						EndIf
					EndIf
				Next nI

				//-----------------------------------------------+
				// Verifica se o custo medio � calculado On-Line |
				//-----------------------------------------------+
				If	cCusMed == "O"
					//-------------------------------------+
					// Inicializa perguntas deste programa |
					//-------------------------------------+
					//--------------------------------------------------------------+
					// mv_par01 - Se mostra e permite digitar lancamentos contabeis |
					// mv_par02 - Se deve aglutinar os lancamentos contabeis        |
					//--------------------------------------------------------------+
					Pergunte("MTA260",.F.)
					lDigita := Iif(mv_par01 == 1,.T.,.F.)
					lAglutina := Iif(mv_par02 == 1,.T.,.F.)

					//----------------------------------------------------------+
					// Se ele criou o arquivo de prova ele deve gravar o rodap� |
					//----------------------------------------------------------+
					RodaProva(nHdlPrv,nTotal)
					If	( FindFunction( "UsaSeqCor" )	.And.	UsaSeqCor()	)
						cCodDiario := CtbaVerdia()
						aCtbDia := {{"SD3",SD3->(RECNO()),cCodDiario,"D3_NODIA","D3_DIACTB"}}
					Else
						aCtbDia := {}
					EndIF
					cA100Incl(cArquivo,nHdlPrv,3,cLoteEst,lDigita,lAglutina,,,,,,aCtbDia)
				EndIf
				SD3->(MsUnlock())
			EndIf
		EndIf
	EndIf
Return Nil
//----------------------------------------
/*/{Protheus.doc}	EstDesSD3
Gera SD3	apura��o	de	custo
@author
@since 13/03/2015
@version	1.0
@param aArrSD3[1] - Produto
        aArrSD3[2] - Local    
/*/
//----------------------------------------
Function EstDesSD3(aArrSD3, cSeqEst, nOperac)
Local aAreaAnt  := GetArea()
Local bCampo    := {|nCPO|	Field(nCPO)}
Local nX        := 0
Local i         := 0
Local lContinua := .T.
Local aLockSD3  := {}
Local aLockSB2  := {}
Local aCtbDia   := {}
Local cArquivo  := ""
Local cCusMed   := GetMv("MV_CUSMED")
Local nHdlPrv   := 0
Local nTotal    := 0
Local cLoteEst  := Iif(Found(),Trim(X5Descri()),"EST ")
Local cQuery    := ""
Local cAliasSD3 := ""
Local nRecnoSD3 := 0

	//-----------------------------------------------+
	// Verifica se o custo medio � calculado On-Line |
	//-----------------------------------------------+
	If	cCusMed == "O"

		nHdlPrv := HeadProva(cLoteEst,"WMSA510",Subs(cUsuario,7,6),@cArquivo)
		If	nHdlPrv <= 0
			lContinua := .F.
		EndIf
	EndIf

	If	lContinua
		//---------------------------+
		// Tratamento para Dead-Lock |
		//---------------------------+
		//Produto Origem
		For nX := 1	to	Len(aArrSD3)
			If	aScan(aLockSD3,aArrSD3[nX][1]+aArrSD3[nX][2]+cSeqEst)==0
				aadd(aLockSD3,aArrSD3[nX][1]+aArrSD3[nX][2]+cSeqEst)
			EndIf
			If	aScan(aLockSB2,aArrSD3[nX][1]+aArrSD3[nX][2])==0
				aadd(aLockSB2,aArrSD3[nX][1]+aArrSD3[nX][2])
			EndIf
		Next nX
		//---------------------------+
		// Tratamento para Dead-Lock |
		//---------------------------+
		If	MultLock("SD3",aLockSD3,3)	.And.	MultLock("SB2",aLockSB2,1)
			//----------------------------------+
			// Gera movimento inverso da origem |
			//----------------------------------+
			//-------------------------+
			// Grava o Flag de estorno |
			//-------------------------+
			cQuery := " SELECT R_E_C_N_O_ RECNOSD3 "
			cQuery +=   " FROM "+RetSqlName("SD3")
			cQuery +=  " WHERE D3_FILIAL  = '"+xFilial("SD3")+"' "
			cQuery +=    " AND D_E_L_E_T_ = ' ' "
			cQuery +=    " AND D3_NUMSEQ  = '"+cSeqEst+"' "
			cQuery +=  " ORDER BY R_E_C_N_O_ "
			cQuery := ChangeQuery(cQuery)
			cAliasSD3 := GetNextAlias()
			dbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),cAliasSD3,.F.,.T.)
			If (cAliasSD3)->(!Eof())
				nRecnoSD3 := (cAliasSD3)->RECNOSD3
			EndIf
			(cAliasSD3)->(dbCloseArea())
			dbSelectArea("SD3")
			SD3->(dbGoTo(nRecnoSD3))
			If	SD3->D3_ESTORNO == "S"
				Help(" ",1,"A242ESTORN")
			Else
				RecLock("SD3",.F.)
				Replace D3_ESTORNO With	"S"
				MsUnlock()
				//---------------------------------------------------+
				// Salva a integridade dos campos de Bancos de Dados |
				//---------------------------------------------------+
				For i	:=	1 To FCount()
					M->&(EVAL(bCampo,i))	:=	FieldGet(i)
				Next i
				//---------------------------------------------------------+
				// Cria o registro de estorno com mesmos dados do original |
				//---------------------------------------------------------+
				RecLock("SD3",.T.)
				For i	:=	1 To FCount()
					FieldPut(i,M->&(EVAL(bCampo,i)))
				Next i
				cTipMov := SubsTr(D3_TM,2,1)
				Replace D3_TM      With Iif(nOperac == "2","499","999") 
				Replace D3_CF      With Iif(D3_TM <= "500","DE"+cTipMov,"RE"+cTipMov)
				Replace D3_CHAVE   With SubStr(D3_CF,2,1)+IIf(D3_TM <= "500","9","0")
				Replace D3_USUARIO With CUSERNAME
				MsUnlock()

				//------------------------------+
				// Pega o custo da movimentacao |
				//------------------------------+
				aCusto := PegaCusD3()
				//---------------------------------------------------+
				// Atualiza o saldo atual (VATU) com os dados do SD3 |
				//---------------------------------------------------+
				If	!B2AtuComD3(aCusto)
					//-----------------------------------------------+
					// Verifica se o custo medio � calculado On-Line |
					//-----------------------------------------------+
					If	cCusMed == "O"
						//---------------------------------------+
						// Gera o lancamento no arquivo de prova |
						//---------------------------------------+
						nTotal += DetProva(nHdlPrv,"672","WMSA510",cLoteEst)
					EndIf
					
					//-----------------------------------+
					// Gera movimento inverso do destino |
					//-----------------------------------+
					cQuery := " SELECT R_E_C_N_O_ RECNOSD3 "
					cQuery +=   " FROM "+RetSqlName("SD3")
					cQuery +=  " WHERE D3_FILIAL  = '"+xFilial("SD3")+"' "
					cQuery +=    " AND D_E_L_E_T_ = ' ' "
					cQuery +=    " AND D3_NUMSEQ  = '"+cSeqEst+"' "
					cQuery +=    " AND D3_ESTORNO <> 'S' "
					cQuery +=  " ORDER BY R_E_C_N_O_ "
					cQuery := ChangeQuery(cQuery)
					cAliasSD3 := GetNextAlias()
					dbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),cAliasSD3,.F.,.T.)
					While (cAliasSD3)->(!Eof())
						nRecnoSD3 := (cAliasSD3)->RECNOSD3
						//-------------------------+
						// Grava o Flag de estorno |
						//-------------------------+
						dbSelectArea("SD3")
						SD3->(dbGoTo(nRecnoSD3))
						RecLock("SD3",.F.)
						Replace D3_ESTORNO With "S"
						MsUnlock()
						//---------------------------------------------------+
						// Salva a integridade dos campos de Bancos de Dados |
						//---------------------------------------------------+
						For i	:=	1 To FCount()
							M->&(EVAL(bCampo,i))	:=	FieldGet(i)
						Next i

						//---------------------------------------------------------+
						// Cria o registro de estorno com mesmos dados do original |
						//---------------------------------------------------------+
						RecLock("SD3",.T.)
						For i	:=	1 To FCount()
							FieldPut(i,M->&(EVAL(bCampo,i)))
						Next i
						Replace D3_TM			With Iif(nOperac == "2","999","499") 
						Replace D3_CF      With Iif(D3_TM <= "500","DE"+cTipMov,"RE"+cTipMov)
						Replace D3_CHAVE   With SubStr(D3_CF,2,1)+IIf(D3_TM <= "500","9","0")						
						Replace D3_USUARIO	With CUSERNAME
						MsUnlock()

						//------------------------------+
						// Pega o custo da movimentacao |
						//------------------------------+
						aCusto := PegaCusD3()
						//---------------------------------------------------+
						// Atualiza o saldo atual (VATU) com os dados do SD3 |
						//---------------------------------------------------+
						If	!B2AtuComD3(aCusto)
							//-----------------------------------------------+
							// Verifica se o custo medio � calculado On-Line |
							//-----------------------------------------------+
							If	cCusMed == "O"
								//---------------------------------------+
								// Gera o lancamento no arquivo de prova |
								//---------------------------------------+
								nTotal += DetProva(nHdlPrv,"670","WMSA510",cLoteEst)
							EndIf
						EndIf
						(cAliasSD3)->(dbSkip())
					EndDo
					(cAliasSD3)->(dbCloseArea())
				EndIf
			EndIf
		EndIf

		//-----------------------------------------------+
		// Verifica se o custo medio � calculado On-Line |
		//-----------------------------------------------+
		If	cCusMed == "O"
			//-------------------------------------+
			// Inicializa perguntas deste programa |
			//-------------------------------------+
			//--------------------------------------------------------------+
			// mv_par01 - Se mostra e permite digitar lancamentos contabeis |
			// mv_par02 - Se deve aglutinar os lancamentos contabeis        |
			//--------------------------------------------------------------+
			Pergunte("MTA260",.F.)
			lDigita	 := Iif(mv_par01 == 1,.T.,.F.)
			lAglutina := Iif(mv_par02 == 1,.T.,.F.)

			//----------------------------------------------------------+
			// Se ele criou o arquivo de prova ele deve gravar o rodap� |
			//----------------------------------------------------------+
			RodaProva(nHdlPrv,nTotal)
			If	( FindFunction( "UsaSeqCor" )	.And.	UsaSeqCor()	)
				cCodDiario := CtbaVerdia()
				aCtbDia := {{"SD3",SD3->(RECNO()),cCodDiario,"D3_NODIA","D3_DIACTB"}}
			Else
				 aCtbDia	:=	{}
			EndIF
			cA100Incl(cArquivo,nHdlPrv,3,cLoteEst,lDigita,lAglutina,,,,,,aCtbDia)
		EndIf
	EndIf
	RestArea(aAreaAnt)
Return Nil