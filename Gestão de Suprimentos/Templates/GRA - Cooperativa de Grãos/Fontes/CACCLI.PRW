#include "rwmake.ch"
/*
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北 transfere dados do associado(fornecedor) para cliente       � 20.03.01
潮�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
*/

User Function CACCLI()

cString:="SA2"

@ 0,0 TO 230,500 DIALOG oDlg TITLE "COPIA ASSOCIADOS"
@ 020,010 say "SOMENTE OS DADOS CADASTRAIS COMUNS SERAO COPIADOS"
@ 030,010 say "DO FORNECEDOR PARA CLIENTE"
@ 040,010 say "OS ASSOCIADOS TEM CODIGO COMECANDO COM -A-"
@ 060,090 BUTTON "Confirma" SIZE 70,20 ACTION ProcProc()
@ 060,170 BUTTON "Abandona" SIZE 70,20 ACTION Close(oDlg)
ACTIVATE DIALOG oDlg CENTERED

Return

Static function ProcProc()
RptStatus({|| RptDetail()})
Return

Static Function RptDetail()
dbselectArea("SA1")
dbsetOrder(1)

dbselectArea("SA2")
dbsetOrder(1)
SetRegua(RecCount())
dbseek(xfilial("SA2")+"A",.T.)
while !EOF() .and. subs(sa2->a2_cod,1,1) == "A"
	IncRegua()
	dbselectArea("SA1")
	if !dbseek(xfilial("SA1")+sa2->a2_cod+sa2->a2_loja)
		reclock("SA1",.t.)
		sa1->a1_filial:=xfilial("SA2")
		sa1->a1_cod   :=sa2->a2_cod
		sa1->a1_loja  :=sa2->a2_loja
		sa1->a1_risco :="E"
		sa1->a1_nome  :=sa2->a2_nome
		sa1->a1_nreduz:=sa2->a2_nreduz
		sa1->a1_end   :=sa2->a2_end
		sa1->a1_bairro:=sa2->a2_bairro
		sa1->a1_mun   :=sa2->a2_mun
		sa1->a1_est   :=sa2->a2_est
		sa1->a1_cep   :=sa2->a2_cep
		sa1->a1_cgc   :=sa2->a2_cgc
		sa1->a1_inscr :=sa2->a2_inscr
		sa1->a1_inscrm:=sa2->a2_inscrm
		sa1->a1_email :=sa2->a2_email
		sa1->a1_tel   :=sa2->a2_tel
		sa1->a1_telex :=sa2->a2_telex
		sa1->a1_fax   :=sa2->a2_fax
		sa1->a1_hpage :=sa2->a2_hpage
		msunlock()
	Endif
	dbselectArea("SA2")
	dbskip()
EndDo
Alert("Processo Finalizado")
Close(oDlg)
Return

