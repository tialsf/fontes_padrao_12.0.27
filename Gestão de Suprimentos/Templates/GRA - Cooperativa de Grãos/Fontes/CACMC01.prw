#Include "RwMake.ch"

User Function CACMC01()

Local __aRet   := Array(2)
Local __aCpos  := {}
Local __aDados := {}

// Exemplos de array com campos de cabelho e as respectivas estruturas

AAdd( __aCpos, {AllTrim("Transf. Fixar" ),;		// Descricao do campo
				"TRF_FIXAR",;					// Nome dado ao campo
				"@E 999,999,999.99",;			// Picture de Exibicao
				14,	;                       	// Tamanho do campo
				2,;								// Qtde de casas decimais
				"N" })							// Tipo do campo

AAdd( __aCpos, {AllTrim("Transf. Dep�sito" ),;
				"TRF_DEPOSITO",;
				"@E 999,999,999.99",;
				14,	;
				2,;
				"N" })      
			
// Descricao do array de dados
// [1]	=> Numero do Romaneio
// [2]	=> Numero da NF 
// [3]	=> Serie da NF
// [4]	=> Codigo do Produto
// [5]	=> Codigo da Operacao / TES
// [6] 	=> Data da Operacao
// [7]	=> Quantidade da Operacao
// [8]	=> Sinal da Opercao (+ Soma ao saldo / - Subtrai do saldo)
// [9]	=> Campo no qual sera exibido o dado na tela (informar o nome colocado na posicao 2 do array __aCpos)

// Exemplos de array com os dados a serem exibidos                                                                          
AAdd(__aDados,{"", "000001", "UNI", "SOJA COMERC", "001", CToD("01/01/2005"), 25000.00, "-", "TRF_FIXAR"})				
AAdd(__aDados,{"", "000002", "UNI", "SOJA COMERC", "004", CToD("02/01/2005"),  5000.00, "-", "TRF_DEPOSITO"})				

// Acerta array de retorno
__aRet[1] := aClone(__aCpos)
__aRet[2] := aClone(__aDados)

Return(__aRet)