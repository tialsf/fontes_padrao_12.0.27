#include "CACMIR05.CH"
#include "Protheus.ch"
#include "TopConn.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �CACMIR05  � Autor � Ricardo Berti         � Data � 02/12/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Relatorio de Retencoes / Perdas  			                  ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 �CACMIR05()                             				      ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 �Template de Cooperativa de Graos							  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Descri��o � PLANO DE MELHORIA CONTINUA        �Programa    CACMIR05.PRW���
�������������������������������������������������������������������������Ĵ��
���ITEM PMC  � Responsavel              � Data                            ���
�������������������������������������������������������������������������Ĵ��
���      01  �Ricardo Berti             � 28/12/2005                      ���
���      02  �Ricardo Berti             � 05/12/2005                      ���
���      03  �Ricardo Berti             � 05/12/2005                      ���
���      04  �Ricardo Berti             � 05/12/2005                      ���
���      05  �Ricardo Berti             � 28/12/2005                      ���
���      05  �                          �                                 ���
���      06  �                          �                                 ���
���      07  �                          �                                 ���
���      08  �Ricardo Berti             � 20/03/2006                      ���
���      09  �                          �                                 ���
���      10  �Ricardo Berti             � 20/03/2006                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACMIR05()

Local cDesc1  := OemToAnsi(STR0001) //"Este programa tem como objetivo imprimir relatorio "
Local cDesc2  := OemToAnsi(STR0002) //"de acordo com os parametros informados pelo usuario."
Local cDesc3  := OemToAnsi(STR0003) //"Retencoes / Perdas de Graos"
Local cPict   := ""
Local titulo  := OemToAnsi(STR0003) //"Retencoes / Perdas de Graos"
Local Cabec1  := ""
Local Cabec2  := ""
Local imprime := .T.
Local aOrdem  := {OemToAnsi(STR0004),OemToAnsi(STR0005),OemToAnsi(STR0006),OemToAnsi(STR0007),OemToAnsi(STR0008)} //"Por NF Origem"###"Por Cli/Forn"###"Por Produto"###"Por Dt.Emiss�o"###"Por Dt.Chegada"
Local cPerg   := "CAMR05"
Local aDados  := {}
Local cStrAux := ""
Local lEnd	  := .F.
Local cbCont  := 0
Local cbtxt   := Space(10)
Local Tamanho := "G" 
Local nTipo   := 15
Local wnrel   := "CACMIR05"   //Nome Default do relatorio em Disco
Local limite  := 220  
Local cString := "LJX"

Private aReturn      := { OemToAnsi(STR0009), 1, OemToAnsi(STR0010), 2, 2, 1, "", 1}  //PARAM 4: 1 RETRATO 2 PAISAGEM //"Zebrado"###"Administra��o" 
Private nLastKey     := 0

ChkTemplate("COL")

// Tratamento de perguntas
AjustaSX1(cPerg)

If Pergunte(cPerg,.T.)
	///////////////// Parametros ////////////////////////
	// MV_PAR01 => Do CLI/Fornecedor                   //
	// MV_PAR02 => Da Loja                             //
	// MV_PAR03 => Ate CLI/Fornecedor                  //
	// MV_PAR04 => Ate Loja                            //
	// MV_PAR05 => Do  Produto						   //
	// MV_PAR06 => Ate Produto						   //
	// MV_PAR07 => Do  Tipo de Grao (Grupo de produtos)//
	// MV_PAR08 => Ate Tipo de Grao (Grupo de produtos)//
	// MV_PAR09 => Tipo 1=Perda/2=Retencao/3=Ganho/4=Todos//
	// MV_PAR10 => Status Pendente/Baixada             //
	// MV_PAR11 => Da  Nota origem                      //
	// MV_PAR12 => Ate Nota origem                      //
	// MV_PAR13 => Data Emissao Inicial                //
	// MV_PAR14 => Data Emissao Final                  //
	// MV_PAR15 => Data Chegada Inicial                //
	// MV_PAR16 => Data Chegada Final                  //
	/////////////////////////////////////////////////////

	//���������������������������������������������������������������������Ŀ
	//� Monta a interface padrao com o usuario...                           �
	//�����������������������������������������������������������������������
	wnrel := SetPrint(cString,wnRel,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrdem,.F.,Tamanho,,.F.)

	If nLastKey <> 27       
		// Executa pesquisa das Retencoes
		MsgRun(OemToAnsi(STR0011),OemToAnsi(STR0013),{|| PesqRet(@aDados) }) //"Selecionando dados..."###"Aguarde..."

		SetDefault(aReturn,cString)
		If nLastKey <> 27
			//���������������������������������������������������������������������Ŀ
			//� Processamento. RPTSTATUS monta janela com a regua de processamento. �
			//�����������������������������������������������������������������������
			RptStatus({|lEnd| RCAMR05(@lEnd,Cabec1,Cabec2,Titulo,aDados,cbCont,Tamanho,cbTxt,nTipo,Limite,wnRel) },Titulo)
		EndIf
	EndIf
EndIf
Return NIL
 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 � PesqRet  � Autor �Ricardo Berti          � Data � 06/12/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Executa query para pesquisa das retencoes, atualizando um  ���
���          � array com dados dos registros do resultado				  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 � PesqRet(ExpA1)                          				      ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 = array vazio, a ser atualizado com dados da pesquisa���
�������������������������������������������������������������������������Ĵ��
���Retorno	 � Nenhum			                                          ���
�������������������������������������������������������������������������Ĵ��
���Uso		 � CACMIR05                                          		  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PesqRet(aDados)

Local aAreaAtu := GetArea()
Local aAreaSD1 := SD1->(GetArea())
Local cQuery
Local nOrdem   := aReturn[8]
Local dD1Emissao
Local cMotivo
Local cAliasLJX  := GetNextAlias()

dbSelectArea("SA1")
dbSetOrder(1)
dbSelectArea("SD2")
dbSetOrder(1)
dbSelectArea("SB1")
dbSetOrder(1)
dbSelectArea("LJX")
dbSetOrder(1)

cQuery := "SELECT "
cQuery += "LJX_RETENC,LJX_TIPO,LJX_STATUS,LJX_NFSAID,LJX_SESAID,LJX_CLIENT,LJX_LOJA,LJX_PRODUT, "
cQuery += "D2_QUANT AS LJX_QUA,D2_EMISSAO,LJX_DTCHEG,LJX_MOTIVO,LJX_QTDRET, "
cQuery += "LJX_NFENT,LJX_SEENT,LJX_PUMCHE "
cQuery += " FROM "
cQuery += " "+RetSqlName("LJX")+" LJX, "+RetSqlName("SD2")+" SD2 "
cQuery += " WHERE "
cQuery += "(LJX_FILIAL = '"+xFilial("LJX")+"') AND "
If !Empty(MV_PAR01) .And. !Empty(MV_PAR03)
	cQuery += 	"(LJX_CLIENT >= '"+RTrim(MV_PAR01)+"'  AND "+;
           		" LJX_CLIENT <= '"+RTrim(MV_PAR03)+"') AND "
ElseIf !Empty(MV_PAR01)
	cQuery += "(LJX_CLIENT >= '"+RTrim(MV_PAR01)+"') AND "
ElseIf !Empty(MV_PAR03)
	cQuery += "(LJX_CLIENT <= '"+RTrim(MV_PAR03)+"') AND "
EndIf
If MV_PAR10 <> 3
	cQuery += "(LJX_STATUS = '"+Str(MV_PAR10-1,1)+"') AND "
EndIf
If !Empty(MV_PAR02) .And. !Empty(MV_PAR04)
	cQuery +=	"(LJX_LOJA <= '"+MV_PAR04+"'   AND "+;
	         	" LJX_LOJA >= '"+MV_PAR02+"' ) AND "
ElseIf !Empty(MV_PAR02) .And. !Empty(MV_PAR04)
	cQuery += "(LJX_LOJA <= '"+MV_PAR04+"') AND "
ElseIf !Empty(MV_PAR02) .And. Empty(MV_PAR04)
	cQuery += "(LJX_LOJA >= '"+MV_PAR02+"' ) AND "
EndIf
If !Empty(MV_PAR05) .And. !Empty(MV_PAR06)
	cQuery += "(LJX_PRODUT >= '"+RTrim(MV_PAR05)+"' AND LJX_PRODUT <= '"+RTrim(MV_PAR06)+"') AND "
ElseIf !Empty(MV_PAR06)
	cQuery += "(LJX_PRODUT <= '"+RTrim(MV_PAR06)+"') AND "
ElseIf !Empty(MV_PAR05)
	cQuery += "(LJX_PRODUT >= '"+RTrim(MV_PAR05)+"') AND "
EndIf
If !Empty(MV_PAR07)
	cQuery += "(LJX_PRODUT IN (SELECT B1_COD "
	cQuery += "FROM "+RetSqlName("SB1")+" SB1 "
	cQuery += "WHERE B1_FILIAL = '"+xFilial("SB1")+"' AND "
	cQuery += "B1_GRUPO BETWEEN '"+AllTrim(MV_PAR07)+"' AND '"+AllTrim(MV_PAR08)+"' AND "
	cQuery += "SB1.D_E_L_E_T_ <> '*')) AND "
EndIf
If MV_PAR09 <> 4  // '1'=PERDA  '2'=RETENCAO  '3'=GANHO  4= TODOS
	cQuery += "(LJX_TIPO = '"+Str(MV_PAR09,1)+"') AND "
EndIf
If !Empty(MV_PAR11)
	cQuery += "(LJX_NFSAID >= '"+MV_PAR11+"' AND LJX_NFSAID <= '"+MV_PAR12+"') AND "
EndIF
If !Empty(MV_PAR15)
	cQuery += "(LJX_DTCHEG BETWEEN '"+DToS(MV_PAR15)+"' AND '"+DToS(MV_PAR16)+"') AND "
EndIf
cQuery += "(SD2.D2_FILIAL = '"+xFilial("SD2")+"') AND "
cQuery += "(LJX_NFSAID = SD2.D2_DOC AND LJX_SESAID = SD2.D2_SERIE AND "+;
	" LJX_CLIENT = SD2.D2_CLIENTE AND LJX_LOJA = SD2.D2_LOJA AND "+;
	" LJX_PRODUT = SD2.D2_COD AND SD2.D_E_L_E_T_ <> '*'  ) AND "
If !Empty(MV_PAR13)  
 	cQuery += "(D2_EMISSAO BETWEEN '"+DToS(MV_PAR13)+"' AND '"+DToS(MV_PAR14)+"') AND "
EndIf
cQuery += " LJX.D_E_L_E_T_ <> '*' "
cQuery += " ORDER BY "
Do Case
Case nOrdem == 1
	cQuery += "LJX_NFSAID "	
Case nOrdem == 2
	cQuery += "LJX_CLIENT,LJX_LOJA "	
Case nOrdem == 3
	cQuery += "LJX_PRODUT,LJX_NFSAID "	
Case nOrdem == 4
	cQuery += "D2_EMISSAO,LJX_NFSAID "	
Case nOrdem == 5
	cQuery += "LJX_DTCHEG,LJX_NFSAID "	
EndCase
cQuery := ChangeQuery(cQuery)
dbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery),cAliasLJX, .T., .T.) //"Seleccionado registros"

TCSetField(cAliasLJX, "D2_EMISSAO"	, "D", 8, 0)	
TCSetField(cAliasLJX, "LJX_DTCHEG"	, "D", 8, 0)	
TCSetField(cAliasLJX, "LJX_QUA"		, "N", TamSX3("D2_QUANT"	)[1], TamSX3("D2_QUANT")	[2])	
TCSetField(cAliasLJX, "LJX_QTDRET"	, "N", TamSX3("LJX_QTDRET"	)[1], TamSX3("LJX_QTDRET")	[2])	
TCSetField(cAliasLJX, "LJX_PUMCHE"	, "N", TamSX3("LJX_PUMCHE"	)[1], TamSX3("LJX_PUMCHE")	[2])	

dbGoTop()
While !Eof()
	If Empty(LJX_NFENT)
		dD1Emissao := Ctod("")
	Else // Retencao baixada tem NFE:
   		SD1->(DbSeek(xFilial("SD1")+(cAliasLJX)->LJX_NFENT+(cAliasLJX)->LJX_SEENT+(cAliasLJX)->LJX_CLIENT+(cAliasLJX)->LJX_LOJA+(cAliasLJX)->LJX_PRODUT))
		dD1Emissao := SD1->D1_EMISSAO
	EndIf
	If !Empty((cAliasLJX)->LJX_MOTIVO)
		cMotivo := X3COMBO("LJX_MOTIVO",(cAliasLJX)->LJX_MOTIVO)
	Else
		cMotivo := Space(12)
	EndIf
	AAdd(aDados,{LJX_RETENC,  LJX_TIPO,	  LJX_STATUS, LJX_NFSAID, LJX_SESAID,;	//  1- 5
		 		 LJX_CLIENT,  LJX_LOJA,	  LJX_PRODUT, LJX_QUA,    D2_EMISSAO,;	//  6-10
				 LJX_DTCHEG,  LJX_MOTIVO, LJX_QTDRET, LJX_NFENT,  LJX_SEENT,;  // 11-15
				 dD1Emissao,  LJX_PUMCHE, cMotivo })	
	dbSkip()
End
dbCloseArea()
RestArea(aAreaSD1)
RestArea(aAreaAtu)

Return(Nil) 


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 � RCAMR05  � Autor �Ricardo Berti          � Data � 05/12/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Funcao auxiliar chamada pela RPTSTATUS. A funcao RPTSTATUS ���
���          � monta a janela com a regua de processamento.               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 � RCAMR05(ExpL1,ExpC1,ExpC2,ExpC3,ExpA1,ExpC4,ExpC5,... 	  ���
���			 �      ...ExpC6,ExpN1,ExpN2,ExpC7)                   	 	  ���	     
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpL1 = Var.para controle de interrupcao pelo usuario	  ���
���          � ExpC1 = 1a.linha do cabecalho 							  ���
���          � ExpC2 = 2a.linha do cabecalho 							  ���
���          � ExpC3 = Titulo do relatorio   							  ���
���          � ExpA1 = Array contendo dados (dos registros) para impressao���
���          � ExpC4 = Informacao 1 para rodape'			 			  ���
���          � ExpC5 = Tamanho do relatorio				 			      ���
���          � ExpC6 = Informacao 2 para rodape'			 			  ���
���          � ExpN1 = Tipo de reducao (compactacao)              	      ���
���          � ExpN2 = Coluna limite                                      ���
���          � ExpC7 = Nome do relatorio 	  							  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum			                                          ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA - Template Graos                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function RCAMR05(lEnd,Cabec1,Cabec2,Titulo,aDados,cbCont,Tamanho,cbTxt,nTipo,Limite,wnRel)

Local aNFTotGer := {}
Local cMotivo := ""
Local cCliFor
Local cLoja
Local cTipo
Local cTipoOper
Local cChaveLJX := ""
Local cDescTipo
Local nomeProg := "CACMIR05"
Local nLimPag  := If(aReturn[4] == 2, 38, 55)  // Se ==2, indica modo paisagem
Local nI		:=0
Local nX		:=0
Local nQtd	   := 0
Local nTQNFPer := 0 // Total NF	  perdas
Local nTQNFRet := 0 // Total NF	  retencoes
Local nTQNFGan := 0 // Total NF	  ganhos
Local nTQtdPer := 0 // Total qtde perdas
Local nTQtdRet := 0 // Total qtde retencoes
Local nTQtdGan := 0 // Total qtde ganhos
Local nTQtdGer := 0 // Total qtde Geral  
Local nTQNFGer := 0 // Total NF  Geral  

Local nColTot	:= 162
Local lFirst	:= .T.
Local nSeek

m_pag   := 1
li		:= 80

Cabec1  :=Space(10)+OemToAnsi(STR0016) //	   	"     Nro.     Sta  NF                                                                                    Qtde. NF  Dt.Saida   Data                % Um. Qtd.Perda            NF Dev/     Data    "
Cabec2  :=Space(10)+OemToAnsi(STR0017) //	   	"Tipo Retencao tus  Orig.  Sr  Cliente/Fornec.                Lj Produto                                  Orig.(Kg) NF Orig. Chegada  Motivo       Cheg. /Retencao  Saldo NF  Compra  Sr  Emissao "
//					   			                 PER  12345678 BAIX 123456 123 123456789.123456789.123456789. 11 123456789.123456789.123456789.123456789. 9,999,999 11/11/11 11/11/11 123456789.12 12345 9,999,999 9,999,999  123456  111 11/11/11
//				         		                 10        20        30        40        50        60        70        80        90        100       110       120       130       140       150       160       170       180       190       200       210       220
//									   01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

// Caso nao exista dados para impressao, aborta a rotina
If Empty(aDados)
	MsgInfo(OemToAnsi(STR0018)) //"N�o existem dados para impress�o."
Else

	//���������������������������������������������������������������������Ŀ
	//� SETREGUA -> Indica quantos registros serao processados para a regua �
	//�����������������������������������������������������������������������
	SetRegua( Len(aDados) )
	For nI := 1 To Len(aDados)

		If lEnd
			@PROW()+1,00 PSAY OemToAnsi(STR0019) //"*** CANCELADO PELO OPERADOR ***"
			Exit
		Endif

		//���������������������������������������������������������������������Ŀ
		//� Impressao do cabecalho do relatorio. . .                            �
		//�����������������������������������������������������������������������
		If lFirst .Or. Li > nLimPag
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
			lFirst := .F.
		Endif

		cCliFor		:= aDados[ni][6]
		cLoja		:= aDados[ni][7]
		cNomCliFor	:= Left(POSICIONE("SA1",1,xFilial("SA1")+cCliFor+cLoja,"A1_NOME"),30)
		nQtd		:= aDados[nI][13]
		cTipo		:= aDados[ni][2]
		If cTipo == '1'
			cDescTipo:= OemToAnsi(STR0023) // "PER "
			nTQtdPer += nQtd
			nTQNFPer += aDados[ni][09]
	        nQSaldo  := aDados[ni][09] - nQtd
			nTQtdGer += nQtd
			cTipoOper := '1'
		ElseIf cTipo == '2'
			cDescTipo:= OemToAnsi(STR0024) // "RET "
			nTQtdRet += nQtd
			nTQNFRet += aDados[ni][09]
	        nQSaldo  := aDados[ni][09] - nQtd
			nTQtdGer += nQtd
			cTipoOper := '1'
		ElseIf cTipo == '3'
			cDescTipo:= OemToAnsi(STR0014) //"GAN "
			nTQtdGan += nQtd
			nTQNFGan += aDados[ni][09]
	        nQSaldo  := aDados[ni][09] + nQtd
			nTQtdGer -= nQtd
			cTipoOper := '2'
		Else
			cDescTipo := cTipo
			cTipoOper := '3'
		EndIf

		// Total NF Geral: notas nao repetidas
		// Chave de busca: 	4-Nota + 5-Serie + 6-Cli/For + 7-Loja + 8-Produto
		cChaveLJX	:= aDados[ni][04]+aDados[ni][05]+aDados[ni][06]+aDados[ni][07]+aDados[ni][08]
		nSeek := Ascan(aNFTotGer,{|x| x[1]+x[2]+x[3]+x[4]+x[5] == cChaveLJX })
		If nSeek == 0
			AAdd(aNFTotGer,{aDados[ni][04],aDados[ni][05],aDados[ni][06],aDados[ni][07],aDados[ni][08]})
			nTQNFGer += aDados[nI][09]
		EndIf

		cStatus	:= aDados[ni][3]
		If cStatus == '0'
			cDescStat := OemToAnsi(STR0025) //"PEND "
		ElseIf cStatus == '1'
			cDescStat := OemToAnsi(STR0012) //"BAIX "
		Else
			cDescStat := Left(cStatus+Space(5),5)
		EndIf

		cMotivo := aDados[nI][18]

		@ Li,10 PSAY Left(cDescTipo,4)
		@ Li,15 PSAY aDados[nI][1]	 // LJX_RETEN
		@ Li,24 PSAY cDescStat	 	
		@ Li,29 PSAY aDados[nI][4]	 // NF Saida
		@ Li,36 PSAY aDados[nI][5]	 // Serie "
		@ Li,40 PSAY cNomCliFor
		@ Li,71 PSAY cLoja
		@ Li,74 PSAY Left(POSICIONE("SB1",1,xFilial("SB1")+aDados[ni][8],"B1_DESC")+Space(10),40)
		@ Li,115 PSay aDados[ni][09]	PICTURE "@E 9,999,999" // Qtde NF
		@ Li,125 PSay aDados[ni][10]        		         // Dt. NF
		@ Li,134 PSay aDados[ni][11]                        // Dt. Chegada
		@ Li,143 PSay Left(cMotivo,12)                     // Motivo
		@ Li,156 PSay aDados[ni][17]	PICTURE "@EZ 99.99" 	// %Um.Chegada
		@ Li,nColTot	PSay nQtd 		PICTURE "@E 9,999,999"	 // Qtde Retencao
		@ Li,nColTot+10	PSay nQSaldo	PICTURE "@E 9,999,999"	 // Saldo NF
		@ Li,183 PSay aDados[ni][14]                        // NF Dev/Compra
		@ Li,191 PSay aDados[ni][15]                        // Serie       
		@ Li,195 PSay aDados[ni][16]        		         // Dt. NFE (D1_EMISSAO)
		Li++     
		ProcRegua()
	Next

	If ! lEnd
		If Li+7 > nLimPag
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
		Endif
		Li++
		@ Li,074 PSAY OemToAnsi(STR0020) //"(1)Total Perdas..............."
		@ Li,109 		PSAY nTQNFPer 			PICTURE "@E 999,999,999,999"
		@ Li,nColTot-2	PSAY nTQtdPer 			PICTURE "@E 999,999,999"
		@ Li,nColTot+10	PSay nTQNFPer-nTQtdPer	PICTURE "@E 9,999,999"	 // Tot.Saldo Perdas
		Li++
		@ Li,074 PSAY OemToAnsi(STR0021) //	"(2)Total Retencoes............"
		@ Li,109 		PSAY nTQNFRet 			PICTURE "@E 999,999,999,999"
		@ Li,nColTot-2	PSAY nTQtdRet			PICTURE "@E 999,999,999"
		@ Li,nColTot+10	PSay nTQNFRet-nTQtdRet	PICTURE "@E 9,999,999"	 // Tot.Saldo Ret
		Li++
		@ Li,074 PSAY OemToAnsi(STR0015) //	"(3)Total Ganhos..............."
		@ Li,109 		PSAY nTQNFGan 		 	PICTURE "@E 999,999,999,999"
		@ Li,nColTot-2	PSAY nTQtdGan			PICTURE "@E 999,999,999"
		@ Li,nColTot+10	PSay nTQNFGan+nTQtdGan	PICTURE "@E 9,999,999"	 // Tot.Saldo Ganhos
		Li++
		Li++
		@ Li,074 		PSAY OemToAnsi(STR0022) //"Total GERAL .................."
		@ Li,109 		PSAY nTQNFGer			PICTURE "@E 999,999,999,999"
		@ Li,nColTot-2	PSAY nTQtdGer			PICTURE "@E 999,999,999"
		@ Li,nColTot+10	PSay nTQNFGer-nTQtdGer	PICTURE "@E 9,999,999"	 // Saldo Geral
	EndIf

	IF li != 80
	    roda(cbcont,cbtxt,tamanho)
	EndIF
	//���������������������������������������������������������������������Ŀ
	//� Finaliza a execucao do relatorio...                                 �
	//�����������������������������������������������������������������������
	SET DEVICE TO SCREEN

	//���������������������������������������������������������������������Ŀ
	//� Se impressao em disco, chama o gerenciador de impressao...          �
	//�����������������������������������������������������������������������
	If aReturn[5]==1
		dbCommitAll()
		SET PRINTER TO
		OurSpool(wnrel)
	Endif

	MS_FLUSH()

EndIf

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSX1 �Autor  �Ricardo Berti         � Data �20/03/2006���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ajusta parametros(SX1) p/ o grupo de perguntas do relatorio���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Sintaxe  � AjustaSX1(ExpC1)                                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = nome do grupo de perguntas deste relatorio         ���
�������������������������������������������������������������������������Ĵ��
��� Retorno  � Nenhum			                                          ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � CACMIR05                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function AjustaSX1(cPerg)

Local aArea		:= GetArea()
Local aHelpP01	:= {}
Local aHelpE01	:= {}
Local aHelpS01	:= {}
Local aHelpP02	:= {}
Local aHelpE02	:= {}
Local aHelpS02	:= {}
Local aHelpP03	:= {}
Local aHelpE03	:= {}
Local aHelpS03	:= {}
Local aHelpP04	:= {}
Local aHelpE04	:= {}
Local aHelpS04	:= {}
Local aHelpP05	:= {}
Local aHelpE05	:= {}
Local aHelpS05	:= {}
Local aHelpP06	:= {}
Local aHelpE06	:= {}
Local aHelpS06	:= {}
Local aHelpP07	:= {}
Local aHelpE07	:= {}
Local aHelpS07	:= {}
Local aHelpP08	:= {}
Local aHelpE08	:= {}
Local aHelpS08	:= {}
Local aHelpP09	:= {}
Local aHelpE09	:= {}
Local aHelpS09	:= {}
Local aHelpP10	:= {}
Local aHelpE10	:= {}
Local aHelpS10	:= {}
Local aHelpP11	:= {}
Local aHelpE11	:= {}
Local aHelpS11	:= {}
Local aHelpP12	:= {}
Local aHelpE12	:= {}
Local aHelpS12	:= {}
Local aHelpP13	:= {}
Local aHelpE13	:= {}
Local aHelpS13	:= {}
Local aHelpP14	:= {}
Local aHelpE14	:= {}
Local aHelpS14	:= {}
Local aHelpP15	:= {}
Local aHelpE15	:= {}
Local aHelpS15	:= {}
Local aHelpP16	:= {}
Local aHelpE16	:= {}
Local aHelpS16	:= {}
Local nTamSX1   := Len(SX1->X1_GRUPO)

dbSelectArea("SX1")
dbSetOrder(1)

If dbSeek(PADR(cPerg,nTamSX1)+"09") .And. Trim(SX1->X1_PERGUNT) == "Selecione o tipo"
	RecLock("SX1",.F.)
    dbDelete()
    MsUnlock()
EndIf

Aadd( aHelpP01, "Informe o cliente/fornecedor inicial    " )
Aadd( aHelpP01, "para filtro.                            " )
Aadd( aHelpP01, "                                        " )

Aadd( aHelpE01, "Enter initial customer/supplier for     " )
Aadd( aHelpE01, "filter.                                 " )
Aadd( aHelpE01, "                                        " )

Aadd( aHelpS01, "Informe el cliente/proveedor inicial    " )
Aadd( aHelpS01, "para filtro.                            " )
Aadd( aHelpS01, "                                        " )

Aadd( aHelpP02, "Informe a loja inicial para filtro.     " )
Aadd( aHelpP02, "                                        " )
Aadd( aHelpP02, "                                        " )

Aadd( aHelpE02, "Enter initial unit for filter.          " )
Aadd( aHelpE02, "                                        " )
Aadd( aHelpE02, "                                        " )

Aadd( aHelpS02, "Informe la tienda inicial para filtro.  " )
Aadd( aHelpS02, "                                        " )
Aadd( aHelpS02, "                                        " )

Aadd( aHelpP03, "Informe o cliente/fornecedor final para " )
Aadd( aHelpP03, "filtro.                                 " )
Aadd( aHelpP03, "                                        " )

Aadd( aHelpE03, "Enter final customer/supplier for       " )
Aadd( aHelpE03, "filter.                                 " )
Aadd( aHelpE03, "                                        " )

Aadd( aHelpS03, "Informe el cliente/proveedor final      " )
Aadd( aHelpS03, "para filtro.                            " )
Aadd( aHelpS03, "                                        " )

Aadd( aHelpP04, "Informe a loja final para filtro.       " )
Aadd( aHelpP04, "                                        " )
Aadd( aHelpP04, "                                        " )

Aadd( aHelpE04, "Enter final unit for filter.           " )
Aadd( aHelpE04, "                                        " )
Aadd( aHelpE04, "                                        " )

Aadd( aHelpS04, "Informe la tienda final para filtro.    " )
Aadd( aHelpS04, "                                        " )
Aadd( aHelpS04, "                                        " )

Aadd( aHelpP05, "Informe o codigo do produto inicial para" )
Aadd( aHelpP05, " filtro.                                " )
Aadd( aHelpP05, "                                        " )

Aadd( aHelpE05, "Enter the product initial code for      " )
Aadd( aHelpE05, "filter.                     			 " )
Aadd( aHelpE05, "                             			 " )

Aadd( aHelpS05, "Informe el codigo del producto inicial  " )
Aadd( aHelpS05, "para filtro.                            " )
Aadd( aHelpS05, "                                        " )

Aadd( aHelpP06, "Informe o codigo do produto final para  " )
Aadd( aHelpP06, "filtro.                                 " )
Aadd( aHelpP06, "                                        " )

Aadd( aHelpE06, "Enter the product final code for filter." )
Aadd( aHelpE06, "                        		  	     " )
Aadd( aHelpE06, "                             			 " )

Aadd( aHelpS06, "Informar el codigo del producto final   " )
Aadd( aHelpS06, "para filtro.                            " )
Aadd( aHelpS06, "                                        " )

Aadd( aHelpP07, "Informe o Tipo de Grao inicial para     " )
Aadd( aHelpP07, "filtro.                                 " )
Aadd( aHelpP07, "                                        " )

Aadd( aHelpE07, "Enter initial grain type for filter.    " )
Aadd( aHelpE07, "                             			 " )
Aadd( aHelpE07, "                             			 " )

Aadd( aHelpS07, "Informe el Tipo de Grano inicial para   " )
Aadd( aHelpS07, "filtro.                                 " )
Aadd( aHelpS07, "                                        " )

Aadd( aHelpP08, "Informe o Tipo de Grao final para filtro" )
Aadd( aHelpP08, ".                                       " )
Aadd( aHelpP08, "                                        " )

Aadd( aHelpE08, "Enter final grain type for filter.      " )
Aadd( aHelpE08, "                             			 " )
Aadd( aHelpE08, "                             			 " )

Aadd( aHelpS08, "Informe el Tipo de Grano final para     " )
Aadd( aHelpS08, "filtro.                                 " )
Aadd( aHelpS08, "                                        " )

Aadd( aHelpP09, "Informe o tipo de registro, para filtro." )
Aadd( aHelpP09, "                                        " )
Aadd( aHelpP09, "                                        " )

Aadd( aHelpE09, "Enter type of record for filter.        " )
Aadd( aHelpE09, "                                        " )
Aadd( aHelpE09, "                                        " )

Aadd( aHelpS09, "Informe el tipo de registro para filtro." )
Aadd( aHelpS09, "                                        " )
Aadd( aHelpS09, "                                        " )

Aadd( aHelpP10, "Informe o Status da Retencao/Perda para " )
Aadd( aHelpP10, "filtro.                                 " )
Aadd( aHelpP10, "                                        " )

Aadd( aHelpE10, "Enter status of withholding/loss for    " )
Aadd( aHelpE10, "filter.                                 " )
Aadd( aHelpE10, "                                        " )

Aadd( aHelpS10, "Informe el estatus da retencion/perdida " )
Aadd( aHelpS10, "para filtro.                            " )
Aadd( aHelpS10, "                                        " )


Aadd( aHelpP11, "Informe o numero da nota fiscal inicial " )
Aadd( aHelpP11, "para filtro.                            " )
Aadd( aHelpP11, "                                        " )

Aadd( aHelpE11, "Enter initial outflow invoice for       " )
Aadd( aHelpE11, "filter.                                 " )
Aadd( aHelpE11, "                                        " )

Aadd( aHelpS11, "Informe la factura de salida inicial    " ) 
Aadd( aHelpS11, "para filtro.                            " ) 
Aadd( aHelpS11, "                                        " )

Aadd( aHelpP12, "Informe o numero da nota fiscal final   " )
Aadd( aHelpP12, "para filtro.                            " )
Aadd( aHelpP12, "                                        " )

Aadd( aHelpE12, "Enter final outflow invoice for filter. " )
Aadd( aHelpE12, "                                        " )
Aadd( aHelpE12, "                                        " )

Aadd( aHelpS12, "Informe la factura de salida final      " ) 
Aadd( aHelpS12, "para filtro.                            " ) 
Aadd( aHelpS12, "                                        " )

Aadd( aHelpP13, "Informe a data de emissao inicial para  " )
Aadd( aHelpP13, "filtro das notas de sa�da.              " )
Aadd( aHelpP13, "                                        " )

Aadd( aHelpE13, "Enter initial issue date to filter      " )
Aadd( aHelpE13, "outflow invoices.                       " )
Aadd( aHelpE13, "                                        " )

Aadd( aHelpS13, "Informe la fecha de emision inicial para" )
Aadd( aHelpS13, "filtro de las facturas de salida.       " )
Aadd( aHelpS13, "                                        " )

Aadd( aHelpP14, "Informe a data de emissao final para    " )
Aadd( aHelpP14, "filtro das notas de sa�da.              " )
Aadd( aHelpP14, "                                        " )

Aadd( aHelpE14, "Enter final issue date to filter outflow" )
Aadd( aHelpE14, "invoices.                               " )
Aadd( aHelpE14, "                                        " )

Aadd( aHelpS14, "Indique la fecha de emision final para  " )
Aadd( aHelpS14, "filtro de las facturas de salida.       " )
Aadd( aHelpS14, "                                        " )

Aadd( aHelpP15, "Informe a data de chegada inicial das   " )
Aadd( aHelpP15, "NFs.Sa�da com reten��es, para filtro.   " )
Aadd( aHelpP15, "                                        " )

Aadd( aHelpE15, "Enter initial arrival date of outflow   " )
Aadd( aHelpE15, "invoices with withholdings for filter.  " )
Aadd( aHelpE15, "                                        " )

Aadd( aHelpS15, "Informe la fecha de llegada inicial de  " )
Aadd( aHelpS15, "las facturas de salida con retenciones  " )
Aadd( aHelpS15, "para filtro.                            " )

Aadd( aHelpP16, "Informe a data de chegada final das     " )
Aadd( aHelpP16, "NFs.Sa�da com reten��es, para filtro.   " )
Aadd( aHelpP16, "                                        " )

Aadd( aHelpE16, "Enter final arrival date of outflow     " )
Aadd( aHelpE16, "invoices with withholdings for filter.  " )
Aadd( aHelpE16, "                                        " )

Aadd( aHelpS16, "Informe la fecha de llegada final de    " )
Aadd( aHelpS16, "las facturas de salida con retenciones  " )
Aadd( aHelpS16, "para filtro.                            " )

// Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/Var01/Def01/Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/VaR14/Def04/Cnt04/Var05/Def05/Cnt05

PutSx1(	cPerg,"01","Do Cliente/Fornec. ?","�Del Cli/Proveedor ?","From Cust/Suplier  ?","mv_ch1",;
	"C",TamSx3("LJX_CLIENT")[1],0,0,"G","","FOR","","","mv_par01","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"02","Da Loja            ?","�De la Tienda      ?","From Unit          ?","mv_ch2",;
	"C",TamSx3("LJX_LOJA")[1],0,0,"G","","","","","mv_par02","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"03","Ate' Cliente/Forn. ?","�Hasta Cli/Proveed.?","To Cust/Suplier    ?","mv_ch3",;
	"C",TamSx3("LJX_CLIENT")[1],0,0,"G","","FOR","","","mv_par03","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"04","Ate' Loja          ?","�Hasta Tienda      ?","To Unit            ?","mv_ch4",;
	"C",TamSx3("LJX_LOJA")[1],0,0,"G","","","","","mv_par04","","",""," ","","","","","","","","","",;
	"","","",{},{},{})		

PutSx1(	cPerg,"05","Do Produto         ?","�De producto       ?","From product       ?","mv_ch5",;
	"C",TamSx3("LJX_PRODUT")[1],0,0,"G","ExistCpo('SB1',mv_par05)","SB1","","","mv_par05","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"06","Ate' Produto       ?","�A producto        ?","To product         ?","mv_ch6",;
	"C",TamSx3("LJX_PRODUT")[1],0,0,"G","ExistCpo('SB1',mv_par06)","SB1","","","mv_par06","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"07","Do Tipo de Grao    ?","�Hasta Tipo de Grano?","From Grain Type    ?","mv_ch7",;
	"C",TamSx3("B1_GRUPO")[1],0,0,"G","","SBM","","","mv_par07","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"08","Ate' Tipo de Grao  ?","�A Tipo de Grano   ?","To Grain Type      ?","mv_ch8",;
	"C",TamSx3("B1_GRUPO")[1],0,0,"G","","SBM","","","mv_par08","","",""," ","","","","","","","","","",;
	"","","",{},{},{})

PutSx1(	cPerg,"09","Tipo de Registro    ","Tipo de Registro    ","Type of record      ","mv_ch9",;
		"N",1,0,4,"C","NaoVazio()","","","","mv_par09","Perda","Perdida","Loss","","Reten��o","Retencion","Withholding","Ganho","Ganho","Ganho","Todos","Todos","All",;
		"","","",{},{},{})

PutSx1(	cPerg,"10","Status             ?","�Estatus           ?","Status             ?","mv_chA",;
		"N",1,0,3,"C","NaoVazio()","","","","mv_par10","Pendente","Pendiente","Pending","","Baixada","De Baja","Posted","Ambos","Ambos","Both","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"11","No.NF sa�da De     ?","�De factura origen ?","From origin invoice?","mv_chB",;
		"C",TamSX3("F2_DOC")[1],0,0,"G","","","","","mv_par11","","","","","","","","","","","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"12","No.NF sa�da Ate'   12?","�Hasta factura origen?","To origin invoice  ?","mv_chC",;
		"C",TamSX3("F2_DOC")[1],0,0,"G","","","","","mv_par12","","","","","","","","","","","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"13","Data Emissao De    ?","�De Fecha de Emision?","From Issue Date   ?","mv_chD",;
		"D",8,0,0,"G","","","","","mv_par13","","","","01/01/2006","","","","","","","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"14","Data Emissao Ate'  ?","�A Fecha de Emision?","To Issue Date     ?","mv_chE",;
		"D",8,0,0,"G","","","","","mv_par14","","","","01/01/2006","","","","","","","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"15","Data Chegada De    ?","�De Fecha de llegada?","From date of arrival?","mv_chF",;
		"D",8,0,0,"G","","","","","mv_par15","","","","","","","","","","","","","",;
		"","","",{},{},{})

PutSx1( cPerg,"16","Data Chegada Ate'  ?","�Hasta Fecha llegada?","To date of arrival  ?","mv_chG",;
		"D",8,0,0,"G","","","","","mv_par16","","","","","","","","","","","","","",;
		"","","",{},{},{})

PutSX1Help("P."+cPerg+"01.",aHelpP01,aHelpE01,aHelpS01)
PutSX1Help("P."+cPerg+"02.",aHelpP02,aHelpE02,aHelpS02)
PutSX1Help("P."+cPerg+"03.",aHelpP03,aHelpE03,aHelpS03)
PutSX1Help("P."+cPerg+"04.",aHelpP04,aHelpE04,aHelpS04)
PutSX1Help("P."+cPerg+"05.",aHelpP05,aHelpE05,aHelpS05)
PutSX1Help("P."+cPerg+"06.",aHelpP06,aHelpE06,aHelpS06)
PutSX1Help("P."+cPerg+"07.",aHelpP07,aHelpE07,aHelpS07)
PutSX1Help("P."+cPerg+"08.",aHelpP08,aHelpE08,aHelpS08)
PutSX1Help("P."+cPerg+"09.",aHelpP09,aHelpE09,aHelpS09)
PutSX1Help("P."+cPerg+"10.",aHelpP10,aHelpE10,aHelpS10)
PutSX1Help("P."+cPerg+"11.",aHelpP11,aHelpE11,aHelpS11)
PutSX1Help("P."+cPerg+"12.",aHelpP12,aHelpE12,aHelpS12)
PutSX1Help("P."+cPerg+"13.",aHelpP13,aHelpE13,aHelpS13)
PutSX1Help("P."+cPerg+"14.",aHelpP14,aHelpE14,aHelpS14)
PutSX1Help("P."+cPerg+"15.",aHelpP15,aHelpE15,aHelpS15)
PutSX1Help("P."+cPerg+"16.",aHelpP16,aHelpE16,aHelpS16)

RestArea(aArea)
Return Nil
