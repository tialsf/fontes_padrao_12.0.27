#INCLUDE "gatcol01.ch"
#INCLUDE "RwMake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatCOL001  � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Dispara todos os gatilhos utilizados nas rotinas de        ���
���          � romaneios de entr/saida de graos e Retencao/Perdas         ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � T_GATCOL001()                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
��� Retorno  � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � (Template COL_GRA) CACMIA04, CACMIA16                      ���
�������������������������������������������������������������������������Ĵ��
���ATUALIZACOES SOFRIDAS DESDE A CONSTRUCAO INICIAL                       ���
�������������������������������������������������������������������������Ĵ��
���PROGRAMADOR   � DATA   � BOPS �  MOTIVO DA ALTERACAO                   ���
�������������������������������������������������������������������������Ĵ��
���Ricardo Berti �22/12/05�090290�GatNFERet() = Gatilho p/campo NF Entrada���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������

�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Descri��o � PLANO DE MELHORIA CONTINUA        �Programa   GATCOL001.PRW���
�������������������������������������������������������������������������Ĵ��
���ITEM PMC  � Responsavel              � Data                            ���
�������������������������������������������������������������������������Ĵ��
���      01  �Ricardo Berti		        � 01/02/2006                      ���
���      02  �Ricardo Berti		        � 22/12/2005                      ���
���      03  �                          �                                 ���
���      04  �Ricardo Berti		        � 22/12/2005                      ���
���      05  �Ricardo Berti		        � 01/02/2006                      ���
���      06  �                          �                                 ���
���      07  �                          �                                 ���
���      08  �Ricardo Berti		        � 22/12/2005                      ���
���      09  �                          �                                 ���
���      10  �Ricardo Berti		        � 22/12/2005                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function GatCOL001()  

Local cVarAtu := ReadVar()
Local lRet    := .T.

If HasTemplate("COL")

	Do Case
		Case AllTrim(cVarAtu) == "M->LBT_PLACA"
			lRet := GatPlaca()                 
		Case AllTrim(cVarAtu) == "M->LBT_CODTRA"		
			lRet := GatTransp()                 
		Case AllTrim(cVarAtu) == "M->LBT_FORNEC"		
	 		lRet := GatFornece()
		Case AllTrim(cVarAtu) == "M->LBT_CLIENT"		
	 		lRet := GatClient()
		Case AllTrim(cVarAtu) == "M->LBT_PRODUT" 
		    lRet := GatProduto()
		Case (AllTrim(cVarAtu) == "M->LBT_PESO1") .Or. (AllTrim(cVarAtu) == "M->LBT_PESO2")		
			lRet := GatPeso()	// cVarAtu
		Case AllTrim(cVarAtu) == "M->LBT_AVARIA"		
			lRet := GatDesc()                 									
		Case AllTrim(cVarAtu) == "M->LBT_IMPURE"
			lRet := GatDesc()                 									
		Case AllTrim(cVarAtu) == "M->LBT_OUTROS"		
			lRet := GatDesc()                 									
		Case AllTrim(cVarAtu) == "M->LBT_PDESCA"		
			lRet := CalcGer()
		Case AllTrim(cVarAtu) == "M->LBT_PH"		
			lRet := GatPH()		
		Case AllTrim(cVarAtu) == "M->LBT_TRIGUI"		
			lRet := GatTrigui()
		Case AllTrim(cVarAtu) == "M->LBT_UMIDAD"		
			lRet := GatUmidade()		
		Case AllTrim(cVarAtu) == "M->LJX_NFSAID"		
			lRet := GatNFSRet()		
		Case AllTrim(cVarAtu) == "M->LJX_NFENT"		
			lRet := GatNFERet()		
		Case AllTrim(cVarAtu) == "M->LBT_SAFRA"	 .Or. AllTrim(cVarAtu) == "M->LBT_TPTRAN"		
			lRet := GatTransge()		
	EndCase
EndIf	
Return(lRet) 


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatPlaca   � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_PLACA.                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatPlaca()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatPlaca()

Local aAreaAtu := GetArea()
Local lRet     := .T.

dbSelectArea("SA4")
dbSetOrder(4)
If MsSeek(xFilial("SA4")+M->LBT_PLACA)
	M->LBT_CODTRA := SA4->A4_COD
	M->LBT_TRANSP := SA4->A4_NOME
	M->LBT_CIDADE := SA4->A4_MUN
	M->LBT_ESTADO := SA4->A4_EST
Else
	lRet := .F.
EndIf 

// Restaura a area original
RestArea(aAreaAtu)

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatTransp  � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_CODTRA                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatTransp()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatTransp()

Local aAreaAtu := GetArea()
Local lRet     := .T.

dbSelectArea("SA4")
dbSetOrder(1)
If MsSeek(xFilial("SA4")+M->LBT_CODTRA)
	M->LBT_TRANSP := SA4->A4_NOME
	M->LBT_CIDADE := SA4->A4_MUN
	M->LBT_ESTADO := SA4->A4_EST    
	M->LBT_PLACA  := SA4->A4_PLACA	
Else
	lRet := .F.
EndIf 

// Restaura a area original
RestArea(aAreaAtu)

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatFornece � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_FORNECE                     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatFornece()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatFornece()

Local aAreaAtu := GetArea()
Local lRet     := .T.

dbSelectArea("SA2")
dbSetOrder(1)
If MsSeek(xFilial("SA2")+M->LBT_FORNEC+M->LBT_LOJA)
	M->LBT_NOMFOR := SA2->A2_NOME
	M->LBT_PROPRI := SA2->A2_END
 	M->LBT_LOJA   := SA2->A2_LOJA
Else
	lRet := .F.
EndIf 

// Restaura a area original
RestArea(aAreaAtu)

Return(lRet)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatClient  � Autor � Ricardo Berti       � Data � 31/01/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_CLIENT                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatClient()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatClient()

Local aAreaAtu := GetArea()
Local lRet     := .T.

dbSelectArea("SA1")
dbSetOrder(1)
If MsSeek(xFilial("SA1")+M->LBT_CLIENT+M->LBT_LOJA)
	M->LBT_NOMFOR := SA1->A1_NOME
	M->LBT_PROPRI := SA1->A1_END
 	M->LBT_LOJA   := SA1->A1_LOJA
Else
	lRet := .F.
EndIf 

// Restaura a area original
RestArea(aAreaAtu)

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatProduto � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_PRODUT                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatProduto()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatProduto()

Local aAreaAtu := GetArea()
Local lRet     := .T.    

dbSelectArea("SB1")
dbSetOrder(1)
If MsSeek(xFilial("SB1")+M->LBT_PRODUT)
	If (SA2->A2_SEMENT$"S|1" .And. !(SB1->B1_GRAO$"1|3")) .Or.;
	   (SA2->A2_SEMENT$"N|2" .And. !(SB1->B1_GRAO$"1|3"))
	   
	   	MsgAlert(OemToAnsi(STR0008)) //"O produto selecionado n�o pode ser utilizado para o produtor informado."
		lRet := .F.
	Else
		M->LBT_DESCAR := SB1->B1_CODDES
		M->LBT_DESPRO := SB1->B1_DESC
		M->LBT_TIPOGR := SB1->B1_GRUPO
		
		If MV_PAR01 == 1
			M->LBT_CODOP  := SB1->B1_TE
		Else                           
			M->LBT_CODOP  := SB1->B1_TS
		EndIf             
		
		// Sugere o codigo da safra, de acordo com os dados do romaneio.
		LBU->( dbSetOrder(5) )
		If LBU->( MsSeek(xFilial("LBU")+SB1->B1_GRUPO) )		
			While !LBU->( Eof() ) .And. LBU->LBU_FILIAL+LBU->LBU_TIPOGR == xFilial("LBU")+SB1->B1_GRUPO
				If DToS(M->LBT_DATA) >= DToS(LBU->LBU_INICIO) .And. DToS(M->LBT_DATA) <= DToS(LBU->LBU_FIM)
					M->LBT_SAFRA := LBU->LBU_CODIGO
					Exit
				EndIf
				LBU->( dbSkip() )
			End
		EndIf

	 	GatTransge()

	EndIf
Else
	lRet := .F.
EndIf 

RestArea(aAreaAtu)
Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatTransge � Autor � Ricardo Berti       � Data � 04/04/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Gatilho disparado dos campos LBT_SAFRA,LBT_PRODUT,LBT_TPTRAN���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatTransge()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.			                                              ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatTransge()

Local aAreaAtu := {}
Local aAreaLBU := {}
Local lRet     := .T.    

If MV_PAR01 == 1
	If !Empty(M->LBT_SAFRA)
		aAreaAtu := GetArea()
		aAreaLBU := GetArea("LBU")
		LBU->( dbSetOrder(3) )
		If LBU->( MsSeek(xFilial("LBU")+M->LBT_SAFRA) )
			If M->LBT_TPTRAN == '2'
				M->LBT_PROYAL := LBU->LBU_PROYDE
			ElseIf M->LBT_TPTRAN == '3'
				M->LBT_PROYAL := LBU->LBU_PROYTE
			Else
				M->LBT_PROYAL := 0
			EndIf		
		EndIf
	EndIf
EndIf
If !Empty(aAreaLBU)
	RestArea(aAreaLBU)
EndIf
If !Empty(aAreaLBU)
	RestArea(aAreaAtu)
EndIf

Return(lRet)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatPeso    � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_PESO1 ou LBT_PESO2          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatPeso()	                                              ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatPeso()  // cCpoAtu

Local lRet := .T.

// Tratamento para Romaneio de Entrada e Saida
If MV_PAR01 == 1
	If M->LBT_PESO1 < M->LBT_PESO2     
		MsgAlert("A primeira pesagem deve ser maior que a segunda pesagem.")
		lRet := .F.
	EndIf
Else     
	If (M->LBT_PESO1 > M->LBT_PESO2) .And.  (M->LBT_PESO2 > 0)     
		MsgAlert(OemToAnsi(STR0003)) //"A primeira pesagem deve ser menor que a segunda pesagem."
		lRet := .F.
	EndIf
EndIf

If lRet                
	If MV_PAR01 == 1
		M->LBT_QTDTOT := (M->LBT_PESO1 - M->LBT_PESO2)
	Else 
		M->LBT_QTDTOT := (M->LBT_PESO2 - M->LBT_PESO1)	
	EndIf

	// Realiza todos os calculos dos descontos e demais valores
	GatDesc()
	
	M->LBT_HORA2  := Iif(!Empty(M->LBT_Peso2),Time(),"")                                                                 
EndIf

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatDesc    � Autor � Ricardo Berti       � Data � 14/01/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disp.pelos campos de descontos (recalcula todos)   ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatDesc()		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.                                                        ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatDesc()

// Realiza o calculo dos descontos em cascata
CalcDesc()

// Realiza o calculo dos demais valores
CalcGer()

Return(.T.)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatUmidade � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_UMIDAD                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatUmidade()		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.                                                        ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatUmidade()

Local aAreaAtu := GetArea()
                           
dbSelectArea("LBS")
dbSetOrder(2)
If MsSeek(XFILIAL("LBS")+M->LBT_TIPOGR+StrZero(M->LBT_UMIDAD,TamSX3("LBT_UMIDAD")[1],TamSX3("LBT_UMIDAD")[2]))
	M->LBT_DESCUM := LBS->LBS_DESCUM                                                                                     
	M->LBT_SECAGE := LBS->LBS_SECAGE                                                                                     
EndIf

// Realiza todos os calculos dos descontos e demais valores
GatDesc()
           
// Restaura a area original
RestArea(aAreaAtu)

Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatTrigui  � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_TRIGUI                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatTrigui()		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatTrigui()

Local lRet := .T.

If (M->LBT_PH < GetMv("MV_PHMIN",,72)) .And. (M->LBT_TRIGUI <> 100.00)
	MsgAlert(OemToAnsi(STR0004)+AllTrim(Str(GetMv("MV_PHMIN",,72)))+OemToAnsi(STR0005)) //"O PH informado � menor que "###"%. Toda a carga sera descartada para triguilho."
	lRet := .F.            
Else
	// Executa calculos para triguilho.
	CalcGer()
EndIf

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatPH      � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LBT_PH                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatPH()  		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.                                                        ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatPH()

If M->LBT_PH != 0
	// Determina, atraves do PH informado, se eh trigo ou triguilho
	If M->LBT_PH < GetMv("MV_PHMIN",,72)
		// Eh triguilho
		M->LBT_TRIGUI := 100.00
	Else
		M->LBT_TRIGUI := 0
	EndIf
		
	// Executa calculos para triguilho.
	CalcGer()
EndIf

Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CalcDesc   � Autor � Ricardo Berti       � Data � 14/01/10 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Realiza os calculos para cada desconto (em cascata)        ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CalcDesc()  		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.                                                        ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CalcDesc()
Local nPesoBase := 0
Local nPesoBase2:= 0

M->LBT_DESIMP := ROUND((M->LBT_QTDTOT - (M->LBT_QTDTOT * ((100 - M->LBT_IMPURE) / 100))),0)
M->LBT_DESAVA := ROUND((M->LBT_QTDTOT - (M->LBT_QTDTOT * ((100 - M->LBT_AVARIA) / 100))),0)
nPesoBase	  := M->LBT_QTDTOT - M->LBT_DESIMP - M->LBT_DESAVA
	
M->LBT_DESUMD := ROUND((nPesoBase - (nPesoBase * ((100 - M->LBT_DESCUM) / 100))),0)
nPesoBase2	  := M->LBT_QTDTOT - M->LBT_DESIMP - M->LBT_DESAVA - M->LBT_DESUMD
	
M->LBT_DESOUT := ROUND((nPesoBase2 - (nPesoBase2 * ((100 - M->LBT_OUTROS) / 100))),0)

Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CalcGer    � Autor � Julio Cesar         � Data � 20/09/04 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Realiza o calculos dos totais de graos/sementes aptos e    ���
���          � quantidade real                                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CalcGer()  		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.                                                        ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CalcGer()

Local nQtde := 0
      
M->LBT_TOTDES := ROUND(M->LBT_DESIMP + M->LBT_DESUMD + M->LBT_DESOUT + M->LBT_DESAVA,0)                                  
nQtde := (M->LBT_QTDTOT - M->LBT_TOTDES)                                                                         
If MV_PAR01 == 1          
	M->LBT_QTDREA := nQtde
Else
	M->LBT_QTDSAI := nQtde
EndIf

If M->LBT_TRIGUI > 0
	M->LBT_KGTRIG := Round(nQtde * (M->LBT_TRIGUI / 100), 0)
	M->LBT_DSCTRG := SB1->B1_DSCTRIG                       
Else                
	M->LBT_KGTRIG := 0
	M->LBT_DSCTRG := ""
EndIf

M->LBT_QTDDES := Round((nQtde - M->LBT_KGTRIG) * (M->LBT_PDESCA / 100), 0)

If M->LBT_QTDDES == 0
	M->LBT_PDESCA := 0
EndIf

M->LBT_GRAOS  := Round((nQtde - M->LBT_KGTRIG - M->LBT_QTDDES), 0)                                                           
nQtde := (nQtde - M->LBT_KGTRIG)
If MV_PAR01 == 1          
	M->LBT_QTDREA := nQtde
Else
	M->LBT_QTDSAI := nQtde
EndIf
	
Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatNFSRet  � Autor � Ricardo Berti       � Data � 25/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LJX_NFSAID                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatNFSRet() 		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatNFSRet()

LOCAL aArea		:= GetArea()
LOCAL cSerie	:= ""
Local aAreaSD2	:= SD2->(GetArea())
Local aAreaSF4	:= SF4->(GetArea())
LOCAL lRet		:= .T.

DbSelectArea("SD2")
DbSetOrder(3)
cSerie	:= If(Empty(M->LJX_SESAID),SD2->D2_SERIE,M->LJX_SESAID)
lRet	:= DbSeek(xFilial("SD2")+M->LJX_NFSAID+cSerie)
M->LJX_NFSAID	:= SD2->D2_DOC
M->LJX_SESAID	:= SD2->D2_SERIE
M->LJX_CLIENT	:= SD2->D2_CLIENTE
M->LJX_LOJA		:= SD2->D2_LOJA
M->LJX_PRODUT	:= SD2->D2_COD
M->LJX_DESPRO	:= POSICIONE("SB1",1,xFilial("SB1")+SD2->D2_COD,"B1_DESC")
M->LJX_VALOR	:= SD2->D2_PRCVEN
M->LJX_PODER3	:= " "
M->LJX_LOCAL 	:= "  "
If lRet
	SF4->(dbSetOrder(1))
	IF SF4->(MsSeek(xFilial("SF4")+SD2->D2_TES))
		If SF4->F4_PODER3 $ "DR"  //  Remessa, Devol.
			M->LJX_PODER3	:= SF4->F4_PODER3
			M->LJX_LOCAL 	:= SD2->D2_LOCAL
	    EndIf
    EndIf
	RestArea(aAreaSF4)
EndIf
RestArea(aAreaSD2)
RestArea(aArea)

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � GatNFERet  � Autor � Ricardo Berti       � Data � 22/12/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gatilho disparado do campo LJX_NFENT                       ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � GatNFERet() 		                                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � GatCOL001                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function GatNFERet()

LOCAL aArea		:= GetArea()
LOCAL cSerie	:= ""
//Local aAreaSD1	:= SD1->(GetArea())
LOCAL lRet		:= .T.

//DbSelectArea("SD1")
//DbSetOrder(1)
M->LJX_NFENT 	:= SD1->D1_DOC
cSerie := If(Empty(M->LJX_SEENT),SD1->D1_SERIE,M->LJX_SEENT)
/*
If lRet .And. ( M->LJX_CLIENT <> SD1->D1_FORNECE .Or. M->LJX_LOJA	<> SD1->D1_LOJA .Or. M->LJX_PRODUT	<> SD1->D1_COD)
	If SD1->D1_FORNECE <> M->LJX_CLIENT
	   	MsgAlert(OemToAnsi(STR0006)) //"O cliente/fornecedor da reten��o/perda difere da nota selecionada."
		lRet := .F.
	EndIf
	If SD1->D1_COD <> M->LJX_PRODUT
	   	MsgAlert(OemToAnsi(STR0009)) //"O produto da reten��o/perda n�o consta na nota selecionada."
		lRet := .F.
	EndIf
	If lRet
	   	MsgAlert(OemToAnsi(STR0010)) //"Nota de Dev./Compra inv�lida para a reten��o/perda."
		lRet := .F.
	EndIf
EndIf   
*/
M->LJX_DTNFE	:= SD1->D1_EMISSAO
//RestArea(aAreaSD1)
RestArea(aArea)

Return(lRet)
