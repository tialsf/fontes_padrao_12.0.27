#Include "RwMake.ch"

User Function CACR0401()

Local aRet  := Array(2)    
Local aCpos := {}
Local aDados:= {}
                                                                              
// Estrutura do array aCpos
// 1 => Nome dado ao 'campo'
// 2 => Descricao que sera exibida no Cabec1
// 3 => Descricao que sera exibida no Cabec2
// 4 => Tamanho total do campo, incluido pontos, virgulas e decimais
// 5 => Picture que sera utilizada na impressao
// 6 => Tipo do campo
// 7 => Sempre valor zero (Uso reservado da rotina)
AAdd(aCpos,{"ETRANSF", "Entradas", "p/Transf",11,"@E 999,999,999","N",0} )
AAdd(aCpos,{"STRANSF", "Sa�das"  , "p/Transf",11,"@E 999,999,999","N",0} )


// Estrutura do array aDados
// 1 => Codigo do Fornecedor
// 2 => Loja do Fornecedor
// 3 => Codigo da operacao (TES)
// 4 => Data da Movimentacao
// 5 => Quantidade da movimentacao
// 6 => Tipo de Opercao ('+' Entrada, '-' Saida)
// 7 => Campo onde devera ser impressa a quantidade da movimentacao
AAdd(aDados,{"000003", "01", "501", CToD('01/03/2005'), 2500 , "-", "STRANSF"})
AAdd(aDados,{"000003", "01", "001", CToD('03/03/2005'), 10500, "+", "ETRANSF"})

aRet[1] := aClone(aCpos)
aRet[2] := aClone(aDados)

Return(aRet)