#INCLUDE "rwmake.ch"
#include "colors.ch"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MTA410  � Autor � Taques             � Data �  09/02/2004  ���
�������������������������������������������������������������������������͹��
���Descricao � programa para execu�a� da tes externa                      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico SEMENTES GUERRA                                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function SF1100I()  
    
    Local aArea 	:= GetArea()
    Local aAreaF1	:= SF1->(GetArea())
    _CODFO   := SF1->F1_FORNECE
    _LOJA    := SF1->F1_LOJA  
    _CODFOC  := SA2->A2_COD
    _LOJFO   := SA2->A2_LOJA
    _NOMEF   := SA2->A2_NOME
    _LOJA    := SF1->F1_LOJA
     
    dbSelectArea("SF1")
    dbSetOrder(2)
    
      If dbSeek(xFilial("SF1")+ _CODFO + _LOJA)
         If _CODFO == _CODFOC .AND. _LOJA == _LOJFO
            RecLock("SF1",.f.)
            SF1->F1_NOME := _NOMEF   
            SEF->(Msunlock())
         Endif
     EndIf
    
    RestArea(aAreaF1)
    RestArea(aArea)
Return .t.

User Function SD1140I()  
   
    Local aArea 	:= GetArea()
    Local aAreaF1	:= SF1->(GetArea())
    _CODFO   := SF1->F1_FORNECE
    _LOJA    := SF1->F1_LOJA  
    _CODFOC  := SA2->A2_COD
    _LOJFO   := SA2->A2_LOJA
    _NOMEF   := SA2->A2_NOME
    _LOJA    := SF1->F1_LOJA
     
    dbSelectArea("SF1")
    dbSetOrder(2)
    
      If dbSeek(xFilial("SF1")+ _CODFO + _LOJA)
         If _CODFO == _CODFOC .AND. _LOJA == _LOJFO
            RecLock("SF1",.f.)
            SF1->F1_NOME := _NOMEF   
            SEF->(Msunlock())
         Endif
     EndIf
    RestArea(aAreaF1)
    RestArea(aArea)
Return .t.
     