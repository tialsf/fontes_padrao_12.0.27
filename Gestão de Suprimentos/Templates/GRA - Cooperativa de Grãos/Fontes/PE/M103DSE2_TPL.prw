#Include "Protheus.ch"

/*/
Autor:     Fernando Roquete  
Data:      21/01/2004
Funcao:    SF1100E
Descricao: Exclusao da NFE, exclui tambem a relacao da NFE x Romaneios (LEW), atualiza LKT (KIT) e deleta tit.forn.transg(Monsanto)
/*/
Template FUNCTION M103DSE2()

Local aAreaAtu  := GetArea()
//Local aTitPag   := {}
Local cTipo

Local cPrefixo := SF1->F1_PREFIXO
Local cChaveSF1:= SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA
Local aAreaSF1 := GetArea()
Local aAreaSA2 := GetArea("SA2")
//Local aCabNFE  := {} 
Local cParcela
Local lRet	   := .T.

If HasTemplate("COL") .And. ( Type("l103Auto") == "U" .Or. !l103Auto ) 
    
	// Exclui Titulo Fornec. Transgenico
	dbSelectArea("SF1")
	dbSetOrder(1) 
	If dbSeek(xFilial("SF1")+cChaveSF1)
	/*	AAdd(aCabNFE,{"F1_DOC"  	,SF1->F1_DOC		,Nil})
		AAdd(aCabNFE,{"F1_SERIE"	,SF1->F1_SERIE		,Nil})
		AAdd(aCabNFE,{"F1_EMISSAO"	,SF1->F1_EMISSAO	,Nil})
		AAdd(aCabNFE,{"F1_FORNECE"	,SF1->F1_FORNECE	,Nil})
		AAdd(aCabNFE,{"F1_LOJA"	  	,SF1->F1_LOJA		,Nil})
		AAdd(aCabNFE,{"F1_ESPECIE"	,SF1->F1_ESPECIE	,Nil})
			
	*/		
		dbSelectArea("SA2")
		dbsetorder(1)
	
		If dbSeek(xFilial("SA2")+SF1->F1_FORNTR+SF1->F1_LOJATR) .And. !Empty(SF1->F1_FORNTR)
		    cTipo 	     := MVNOTAFIS
			//cNatureza    := MaFisRet(,"NF_NATUREZA")
		    cParcela	 := StrZero(1,LEN(SE2->E2_PARCELA))
		    lRet         := .T.	    
			dbSelectArea("SE2")
			aAreaSE2 := GetArea()
			dbSetOrder(6)
			If dbSeek(cSeek:=xFilial("SE2")+SA2->A2_COD+SA2->A2_LOJA+cPrefixo+SF1->F1_DOC)
			    Do While !Eof() .And. SE2->(E2_FILIAL+E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM) == cSeek
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Verifica se o titulo nao esta em bordero                          �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					If !Empty(SE2->E2_NUMBOR)
						Help("",1,"FA050BORD")
						lRet := .F. 
					EndIf
		
					//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					//� Verifica se titulo ja foi baixado total ou parcialmente			  �
					//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
					If lRet .And. !Empty(SE2->E2_BAIXA)
						Help(" ",1,"FA050BAIXA")
						lRet := .F. 
					EndIf
					If lRet .And. SE2->E2_VALOR != SE2->E2_SALDO
	 					Help(" ",1,"BAIXAPARC")
						lRet := .F. 
					EndIf
					If lRet 
				        Reclock('SE2',.F.)
				        dbDelete()
				        MsUnLock()
				    EndIf	
			    	SE2->(dbSkip())
	            EndDo 
	                
	            /*			                               
				AAdd(aTitPag,{"E2_FILIAL"	,SE2->E2_FILIAL 	,Nil})		    
				AAdd(aTitPag,{"E2_PREFIXO"	,SE2->E2_PREFIXO	,Nil})
				AAdd(aTitPag,{"E2_NUM"		,SE2->E2_NUM		,Nil})
				AAdd(aTitPag,{"E2_PARCELA"	,SE2->E2_PARCELA	,Nil})
				AAdd(aTitPag,{"E2_TIPO"		,SE2->E2_TIPO		,Nil})
				AAdd(aTitPag,{"E2_FORNECE"	,SE2->E2_FORNECE	,Nil})
				AAdd(aTitPag,{"E2_LOJA"		,SE2->E2_LOJA		,Nil})
					
				lMsHelpAuto := .T.
				lMsErroAuto := .F.
				
				//Chamada do FINA050 (Contas a Pagar)
		        MSExecAuto({| a,b,c,d,e,f,g| FINA050(a,b,c,d,e,f,g)} ,aTitPag, , 5) 
				If lMsErroAuto
					MostraErro()
					DisarmTransaction()
					Break
				EndIf    
	*/				
			EndIf	
		EndIf
	EndIf
	// Restaura as area original do arquivo SF1
	If !Empty(aAreaSF1)
		RestArea(aAreaSF1)
	EndIf
	
	RestArea(aAreaSA2)
	
	dbSelectArea("LEW")
	dbSetorder(1)
	dbSeek(xFilial("LEW")+SF1->F1_Doc+SF1->F1_Serie+SF1->F1_Fornece+SF1->F1_Loja,.F.)
	While !Eof() .And. LEW->LEW_Filial ==xFilial("LEW")  .And.;
		LEW->LEW_Doc    ==SF1->F1_Doc     .And.;
		LEW->LEW_Serie  ==SF1->F1_Serie   .And.;
		LEW->LEW_Fornec ==SF1->F1_Fornece .And.;
		LEW->LEW_Loja   ==SF1->F1_Loja
		 
		dbSelectArea("LBT")
		dbSetorder(6)
		dbSeek(xFilial("LBT")+LEW->LEW_ROMANE,.F.)
		
		If Found()
			If (Alltrim(LBT->LBT_Produt) == Alltrim(LEW->LEW_Produt)) .And. !Empty(LBT->LBT_DESCAR)    // eh semente
				Reclock("LBT",.F.)
				LBT->LBT_NFESEM :=" "
				MsUnlock()
			Else
				Reclock("LBT",.F.)
				LBT->LBT_NFEDES :=" "
				MsUnlock()
			Endif
		Endif
		
		dbSelectArea("LEW")
		Reclock("LEW",.F.)
		dbDelete()
		MsUnlock()
		
		dbSkip()
	EndDo
	
	
	// Atualiza KITS
	dbSelectArea("LKT")
	dbSetorder(1)
	
	While MsSeek(xFilial("LKT")+SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA,.F.)
			
		Reclock("LKT",.F.)
		LKT->LKT_DOC     := Space(Len(LKT->LKT_DOC))
		LKT->LKT_SERIE   := Space(Len(LKT->LKT_SERIE))	
		LKT->LKT_QKITPG  := 0				
		MsUnlock()
				
	EndDo
	
	RestArea(aAreaAtu)
EndIf	

RETURN