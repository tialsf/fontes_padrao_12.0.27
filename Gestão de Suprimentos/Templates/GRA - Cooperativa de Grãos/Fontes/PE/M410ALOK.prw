#Include "RwMake.ch" 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M410ALOK  �Autor  � Julio Cesar        � Data � 22/02/2005  ���
�������������������������������������������������������������������������͹��
���Desc.     � Bloqueia a alteracao de Pedidos gerados atraves de        )���
���          � Romaneios.                                                )���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function M410ALOK()

Local lRet     := .T.
Local aAreaAtu := GetArea()
Local aAreaLEW := {}

If Altera
	dbSelectArea("LEW")
	aAreaLEW := GetArea()
	dbSetOrder(3)
	If MsSeek(xFilial("LEW")+SC5->C5_NUM)
		MsgInfo("Esse Pedido de Venda foi gerado atrav�s de um ou mais Romaneios de Sa�da. O mesmo n�o poder� ser alterado. Caso seja necess�rio exclua o Pedido de Venda e gere um novo.")
		lRet := .F.
	EndIf
	// Restaura area do arquivo LEW
	RestArea(aAreaLEW)
	
	// Restaura area original
	RestArea(aAreaAtu)
EndIf

Return(lRet)