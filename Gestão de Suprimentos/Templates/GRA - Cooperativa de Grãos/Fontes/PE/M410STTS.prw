#Include "RwMake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M410STTS  �Autor  � Julio Cesar        � Data � 22/02/2005  ���
�������������������������������������������������������������������������͹��
���Desc.     � Exclui amarracao Pedido X Romaneio                        )���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function M410STTS()

Local aAreaAtu := GetArea()
Local aAreaLEW := {}
Local aNumRom  := {}

// Tratamento para Exclusao
If !Inclui .And. !Altera
	dbSelectArea("LEW")
	aAreaLEW := GetArea()
	dbSetOrder(3)
	If MsSeek(xFilial("LEW")+M->C5_NUM)
		While !Eof() .And. LEW_FILIAL+LEW_NUM == xFilial("LEW")+M->C5_NUM
			// Armazena numero do romaneio
			AAdd(aNumRom, LEW_ROMANE)

			// Deleta amarracao Pedido X Romaneio			
			RecLock("LEW", .F.)
			dbDelete()
			MsUnLock()
			
			dbSkip()
		End
	EndIf
	// Restaura a area do arquivo LEW
	RestArea(aAreaLEW)
	
	// Restaura area original
	RestArea(aAreaAtu)
EndIf

Return Nil