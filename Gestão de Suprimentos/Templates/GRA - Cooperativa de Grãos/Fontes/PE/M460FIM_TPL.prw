#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � M460Fim  �Autor  � Julio Cesar        � Data � 23/02/2005  ���
�������������������������������������������������������������������������͹��
���Descricao � P.E. p/ criar a amarracao NF de Saida X Romaneio (LEW).	  ���
���          � Caso o romaneio nao exista, tambem o cria na tabela LBT    ���
�������������������������������������������������������������������������͹��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Retorno	 � Nenhum                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function M460FIM()

Local aAreaAtu := GetArea()
Local aAreaSC9 := {}
Local aAreaLEW := {}
Local aAreaLBT := {}
Local aNumRom  := {}
Local nI       := 0
Local cStatus  := ""
Local cEnd     := ""
Local cNome    := ""
Local cNumRom  := ""
Local cMay     := ""
Local lNewLew  := .T.

If HasTemplate("COL") .And. Type('__aSC9')=="A" .And. ( Type("l310") == "U" .Or. !l310 )

	For nI := 1 To Len(__aSC9)
		dbSelectArea("SC9")
		If Empty(aAreaSC9)
			aAreaSC9 := GetArea()
		EndIf
		dbGoTo(__aSC9[nI])
		If !Empty(SC9->C9_NFISCAL)
			dbSelectArea("LEW")
			If Empty(aAreaLEW)
				aAreaLEW := GetArea()
			EndIf
			dbSetOrder(3)
			If MsSeek(xFilial("LEW")+SC9->C9_PEDIDO)
				While !Eof() .And. LEW_FILIAL+LEW_NUM == xFilial("LEW")+SC9->C9_PEDIDO
					If !Empty(LEW_DOC)
 						dbSkip()
						Loop
					EndIf
									
					// Atualiza campos da amarracao Romaneio X NF de Saida
					RecLock("LEW",.F.)
					LEW_DOC    := SC9->C9_NFISCAL
					LEW_SERIE  := SC9->C9_SERIENF
					LEW_ESPECI := Posicione("SF2",1,xFilial("SF2")+SC9->C9_NFISCAL+SC9->C9_SERIENF+SC9->C9_CLIENTE+SC9->C9_LOJA,"F2_ESPECIE")
					MsUnLock()
	                                                                            
					// Pesquisa qual o tipo de de Pedido de Venda
					cStatus := Posicione("SC5",1,xFilial("SC5")+SC9->C9_PEDIDO,"C5_TPSAIGR")
					
					// Armazena dados no array de Romaneios
					AAdd(aNumRom,{LEW_ROMANE, SC9->C9_NFISCAL, SC9->C9_SERIENF, cStatus}) 
					dbSkip()      
					
					lNewLew := .F.
				End                                                              
			EndIf
			          
			// Inclui um novo registro de amarracao e seu respectivo romaneio
			If lNewLew .And. Posicione("SB1",1,xFilial("SB1")+SC9->C9_PRODUTO,"B1_GRAO")$"1|3"                                                
			
				// Pesquisa qual o tipo de de Pedido de Venda
				cStatus := Posicione("SC5",1,xFilial("SC5")+SC9->C9_PEDIDO,"C5_TPSAIGR")
	
				// Nao existe amarracao pq a saida foi realizada sem a existencia de um Romaneio de Saida. 
				// Nesse caso e necessario criar o romaneio de saida (LBT) e sua respectiva amarracao no arquivo LEW.
	
				// Obtem numero correto do romaneio
				cNumRom := CriaVar("LBT_ROMANE")
				dbSelectArea("LBT")
				aAreaLBT := GetArea()
				dbSetOrder(6)
				cMay := Alltrim(xFilial("LBT"))+cNumRom
				FreeUsedCode()
				While LBT->(MsSeek(xFilial("LBT")+cNumRom)).Or. !MayIUseCode(cMay)
					ConfirmSx8()
					cNumRom := CriaVar("LBT_ROMANE")
					FreeUsedCode()
					cMay := Alltrim(xFilial("LBT"))+cNumRom
				End
				If __lSX8
					// Confirma o numero de romaneio obtido atraves do SXE ou do HardLock
					ConfirmSX8()
				EndIf
			
				dbSelectArea("LBT")
				RestArea(aAreaLBT)
				RecLock("LBT", .T.)
				LBT_FILIAL := xFilial("LBT")
				LBT_ROMANE := cNumRom                                                                                                    
				If cStatus $ "1|2"
					LBT_FORNEC := SF2->F2_CLIENTE
					cEnd := Posicione("SA2",1,xFilial("SA2")+SF2->F2_CLIENTE+SF2->F2_LOJA,"A2_END")               
					cNome:= Posicione("SA2",1,xFilial("SA2")+SF2->F2_CLIENTE+SF2->F2_LOJA,"A2_NOME")				
				Else
					LBT_CLIENT := SF2->F2_CLIENTE                                              
					cEnd := Posicione("SA1",1,xFilial("SA1")+SF2->F2_CLIENTE+SF2->F2_LOJA,"A1_END")				
					cNome:= Posicione("SA1",1,xFilial("SA1")+SF2->F2_CLIENTE+SF2->F2_LOJA,"A1_NOME")				
				EndIf   
				LBT_LOJA   := SF2->F2_LOJA
				LBT_NOMFOR := cNome
				LBT_PRODUT := SD2->D2_COD
				LBT_DATA   := SD2->D2_EMISSAO
				LBT_HORA   := Time()
				LBT_QTDSAI := SD2->D2_QUANT
				LBT_GRAOS  := SD2->D2_QUANT
				LBT_STATUS := cStatus
				LBT_PROPRI := cEnd
				LBT_VALOR  := SD2->D2_PRCVEN
				LBT_NFSAID := SD2->D2_DOC
				LBT_SESAID := SD2->D2_SERIE
				LBT_CODTRA := SF2->F2_TRANSP 
			    LBT_TIPOGR := SD2->D2_GRUPO
			    LBT_CODOP  := SD2->D2_TES
			    LBT_HORA2  := SF2->F2_HORA
				MsUnLock()     
	
				If __lSX8 
					ConfirmSX8()               
				EndIf
				
				dbSelectArea("LEW")
				RecLock("LEW",.T.)
				LEW_Filial := xFilial("LEW")
				LEW_DOC    := SD2->D2_DOC
				LEW_SERIE  := SD2->D2_SERIE
	
				If cStatus $ "1|2"			
					LEW_FORNEC := SF2->F2_CLIENTE
				Else                                  
					LEW_CLIENT := SF2->F2_CLIENTE
				EndIf
				
				LEW_LOJA   := SF2->F2_LOJA
				LEW_ROMANE := LBT->LBT_ROMANE
				LEW_PRODUT := SD2->D2_COD
				LEW_ESPECI := SF2->F2_ESPECIE
				LEW_NUM    := SC9->C9_PEDIDO
				LEW_TPGER  := "1"
				MsUnLock()
			EndIf
		EndIf                                                                    
	Next nI
	// Restaura area do arquivo SC9 
	If !Empty(aAreaSC9)
		RestArea(aAreaSC9)
	EndIf	
	// Restaura area do arquivo LEW
	If !Empty(aAreaLEW)
		RestArea(aAreaLEW)
	EndIf
	
	If !Empty(aNumRom)
		dbSelectArea("LBT")
		aAreaLBT := GetArea()
		dbSetOrder(6)
	
		For nI := 1 To Len(aNumRom)
			If MsSeek(xFilial("LBT")+aNumRom[nI][01])
				RecLock("LBT",.F.)
				LBT_NFSAID := aNumRom[nI][02]
				LBT_SESAID := aNumRom[nI][03]
				LBT_STATUS := aNumRom[nI][04] 
				MsUnLock()
			EndIf			
		Next nI           
	EndIf
	
	// Restaura a area do arquivo LBT
	If !Empty(aAreaLBT)
		RestArea(aAreaLBT)
	EndIf
	
	// Restaura a area original
	RestArea(aAreaAtu)
	
	// Reinicializa variavel publica __aSC9
	__aSC9 := Nil

EndIf
Return Nil
