#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � M460MARK �Autor  � Julio Cesar        � Data � 23/02/2005  ���
�������������������������������������������������������������������������͹��
���Desc.     � Cria array publico __aSC9, para que posteriormente seja    ���
���          � determinado qual pedido foi gerado atraves de Romaneio de  ���
���          � Saida.                                                     ���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function M460MARK()

Local aAreaAtu := GetArea()
Local cQuery   := ""

Public __aSC9  := {}

If HasTemplate("COL")

	cQuery := "SELECT SC9.R_E_C_N_O_ AS NUMREG, C9_PEDIDO "
	cQuery += "FROM "+RetSqlName("SC9")+" SC9 "
	cQuery += "WHERE C9_FILIAL = '"+xFilial("SC9")+"' AND "
	cQuery += "C9_OK = '"+ParamIxb[1]+"' AND "
	cQuery += "D_E_L_E_T_ <> '*' "
	cQuery += "ORDER BY C9_PEDIDO"
	cQuery := ChangeQuery(cQuery)
	
	dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "SC9TMP", .F., .T.)
	While !Eof()        
		AAdd(__aSC9, SC9TMP->NUMREG)
		dbSkip()
	End
	// Fecha area utilizada pelo TopConnect
	dbCloseArea()                         
	
	// Restaura a area original
	RestArea(aAreaAtu)
	
EndIf	

Return(.T.)