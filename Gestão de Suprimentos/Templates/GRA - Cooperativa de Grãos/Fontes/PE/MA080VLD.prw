#Include "RwMake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MA080VLD �Autor  � Julio Cesar        � Data � 23/02/2005  ���
�������������������������������������������������������������������������͹��
���Desc.     � Cria a opercao, na tabela IO, qdo eh craido ou alterado    ���
���          � algum codigo de TES.                                       ���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function MA080VLD()

Local __nOpc     := ParamIxb[1]
Local __aAreaAtu := GetArea()
Local __aAreaSX5 := {}
Local __cCodOp   := ""

If __nOpc == 3
	__cCodOp := M->F4_CODIGO  
Else 
	__cCodOp := SF4->F4_CODIGO
EndIf                      

dbSelectArea("SX5")
__aAreaSX5 := GetArea()
dbSetOrder(1)
If !MsSeek(xFilial("SX5")+"IO"+__cCodOp)
	RecLock("SX5",.T.)
	X5_FILIAL := xFilial("SX5")
	X5_TABELA := "IO"
	X5_CHAVE  := __cCodOp
Else
	RecLock("SX5",.F.)
EndIf

// Campos comuns a alteracao e inclusao
X5_DESCRI := M->F4_TEXTO
X5_DESCSPA:= M->F4_TEXTO
X5_DESCENG:= M->F4_TEXTO
MsUnLock()
                        
// Restaura a area original
RestArea(__aAreaAtu)

Return(.T.)