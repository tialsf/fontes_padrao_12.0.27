#Include "Protheus.ch"

User Function MGRAVSB8()

If GdFieldPos("D1_SAFRA") > 0
    RecLock("SB8",.F.)
	SB8->B8_SAFRA := GdFieldGet("D1_SAFRA", n)    	
    MsUnLock()
EndIf 

Return Nil