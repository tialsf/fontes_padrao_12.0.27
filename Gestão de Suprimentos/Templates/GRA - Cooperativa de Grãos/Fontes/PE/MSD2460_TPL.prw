#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �MSD2460   � Autor � Julio Cesar           � Data � 19/03/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Realiza o acerto do saldo a complememtar, qdo eh realizada  ���
���          �devolucao de compra a fixar.                                ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 �MSD2460()                              				      ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 �Template de Cooperativa de Graos							  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function MSD2460()

Local aAreaAtu   := GetArea()
Local aAreaSE2   := {}
Local aAreaSA2   := {}
Local lFixar     := .F.                                
Local cFixSer    := SuperGetMV("MV_FIXSER",.F.,"AFX")
Local nQtdDev    := 0         
Local nQtdSaca   := 0           
Local nArredSaca := TamSx3("M2_MOEDA4")[2]     

If HasTemplate("COL")

	lFixar := Posicione("SF4", 1, xFilial("SF4")+SD2->D2_TES, "F4_FIXAR") $ "1S"
	
	// Realiza a atualizacao do titulo provisorio a Fixar
	If lFixar .And. !Empty(SD2->D2_NFORI) 
		SB1->( dbSetOrder(1) )
		SB1->( MsSeek(xFilial("SB1")+SD2->D2_COD) )
	
		nQtdDev  := SD2->D2_QUANT
		nQtdSaca := Round(nQtdDev / SB1->B1_CONV, nArredSaca)
	              
		dbSelectArea("SE2")
		aAreaSE2 := GetArea()
		dbSetOrder(6)
		If MsSeek(xFilial("SE2")+SD2->D2_CLIENTE+SD2->D2_LOJA+cFixSer+SD2->D2_NFORI)
			RecLock("SE2",.F.)
			E2_SALDO   := Iif((E2_SALDO - nQtdSaca) > 0.016667,(E2_SALDO - nQtdSaca),0)
			E2_MOVIMEN := dDataBase
			MsUnlock()
					
			//�����������������������������������������������������Ŀ
			//� Atualiza saldo do fornecedor                        �
			//�������������������������������������������������������
			nMoeda 	 := Int( Val( GetMv("MV_MCUSTO") ) )
			nMoedSE2 := E2_MOEDA
			dDtEmiss := E2_EMISSAO
			
			dbSelectArea("SA2")
			aAreaSA2 := GetArea()
			dbSetOrder(1)
			MsSeek(xFilial("SA2")+SD2->D2_CLIENTE+SD2->D2_LOJA)
			RecLock("SA2",.F.)
			A2_SALDUP  -= Round(NoRound(xMoeda(nQtdSaca,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,3),3),2)
			A2_SALDUPM -= Round(NoRound(xMoeda(nQtdSaca,SE2->E2_MOEDA,nMoeda,SE2->E2_EMISSAO,3),3),2)
			MsUnlock()
			
			// Restaura area do arquivo SA2
			RestArea(aAreaSA2)
		EndIf 
		// Restaura area do arquivo SE2
		RestArea(aAreaSE2)
	EndIf               
	
	// Restaura area original
	RestArea(aAreaAtu)
EndIf	
Return Nil