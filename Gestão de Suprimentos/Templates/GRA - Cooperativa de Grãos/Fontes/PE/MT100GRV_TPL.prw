#Include "Protheus.ch"

/*/
Autor:     Fernando Roquete  
Data:      21/01/2004
Funcao:    MT100GRV
Descricao: Gravacao da Selecao dos Romaneios
/*/
Template FUNCTION MT100GRV()
                     
Local _lPendNFE := .F.   
Local lGravaRom := .F.
Local aAreaAtu  := GetArea()
Local aAreaLBT  := {}
Local aAreaLEW  := {}
Local nI        := 0

If HasTemplate("COL") .And. ( Type("l310") == "U" .Or. !l310 ) .And. Type("aGrvRomaneio") <> "U"

	For nI := 1 To Len(aCols)         
		// Caso exista um item valido significa que pode ser gerada a amarracao NF x Romaneio de Entrada
		If !aCols[nI,Len(aHeader)+1]
			lGravaRom := .T.
			Exit
		EndIf
	Next nI
	
	If lGravaRom
	
		For nI := 1 To Len(aGrvRomaneio)
			If aGrvRomaneio[nI,1]
				If aGrvRomaneio[nI,6]=="S"
					_lPendNFE :=.T.
				Endif
				
				dbSelectArea("LEW")
				If Empty(aAreaLEW)
					aAreaLEW := GetArea()
				EndIf
				Reclock("LEW",.T.)
				LEW->LEW_Filial := xFilial("LEW")
				LEW->LEW_Doc    := CNFISCAL
				LEW->LEW_Serie  := CSERIE
				LEW->LEW_Fornec := CA100FOR
				LEW->LEW_Loja   := CLOJA
				LEW->LEW_Romane := aGrvRomaneio[nI,2]
				LEW->LEW_Produt := aGrvRomaneio[nI,7]
				LEW->LEW_ESPECI := CESPECIE
				LEW->LEW_TPGER  := "2"
				MsUnlock()
				
				dbSelectArea("LBT")
				If Empty(aAreaLBT)
					aAreaLBT := GetArea()
				EndIf
				dbSetorder(6)
				If dbSeek(xFilial("LBT")+LEW->LEW_Romane,.F.)
					If (Alltrim(LBT->LBT_Produt) == Alltrim(LEW->LEW_Produt)) .And. !Empty(LBT->LBT_DESCAR)    // eh semente
						Reclock("LBT",.F.)
						LBT->LBT_NFESEM :="S"
						MsUnlock()
						_MatPend :="DESCARTE"
					Else
						Reclock("LBT",.F.)
						LBT->LBT_NFEDES :="S"
						MsUnlock()
						_MatPend :="SEMENTE"
					Endif
				Endif
			Endif
		Next _I
			
		If _lPendNFE
			MsgStop("Existe Nota Fiscal de "+_MatPend+" para ser emitada do fornecedor: "+CA100FOR+"-"+CLOJA)
		Endif
	Endif
	
	// Restaura area original do arquivo LEW
	If !Empty(aAreaLEW)
		RestArea(aAreaLEW)
	EndIf
	                
	// Restaura area original do arquivo LBT
	If !Empty(aAreaLBT)
		RestArea(aAreaLBT)
	EndIf
	
	// Restaura area original
	RestArea(aAreaAtu)
          
EndIf

Return Nil