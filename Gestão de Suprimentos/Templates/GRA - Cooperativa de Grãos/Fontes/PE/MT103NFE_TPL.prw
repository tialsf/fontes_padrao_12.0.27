#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MT103NFE  �Autor  � Julio Cesar        � Data � 30/09/2004  ���
�������������������������������������������������������������������������͹��
���Desc.     � Declaracao de variaveis publicas utilizadas na rotina de   ���
���          � NF de entrada.                                             ���
�������������������������������������������������������������������������͹��
���Uso       � MATA103 - Template COL                                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template FUNCTION MT103NFE()

Public aVetVen      := {} // define vetor como public para utilizar nos pontos de entrada relacionados na NFE
Public aRomaneio    := {} 
Public aGrvRomaneio := {} 
Public aSelRom      := {}  // Controla os romaneios selecionados na rotina de NFE
Public __lSelRom    := .T. // Determina se a rotina de selecao de romaneio ja foi chamada   

Return Nil