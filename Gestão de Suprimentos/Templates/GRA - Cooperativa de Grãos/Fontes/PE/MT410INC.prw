#Include "RwMake.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT410INC �Autor  � Julio Cesar        � Data � 23/02/2005  ���
�������������������������������������������������������������������������͹��
���Desc.     � Cria a amarraco Pedido X Romaneio.                         ���
�������������������������������������������������������������������������͹��
���Uso       � Template de Cooperativa de Graos                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function MT410INC()
                    
Local aAreaAtu  := GetArea()
Local aAreaLEW  := {}
Local nI        := 0
Local lGrvRom   := .F.

ChkTemplate("COL")

For nI := 1 To Len(aCols)         
	// Caso exista um item valido significa que pode ser gerada a amarracao Pedido de Venda x Romaneio de Saida
	If !aCols[nI,Len(aHeader)+1]  
		lGrvRom := .T.
		Exit
	EndIf
Next nI

If lGrvRom
	For nI := 1 To Len(__aGrvRomaneio)
		If __aGrvRomaneio[nI,1]
			dbSelectArea("LEW")
			If Empty(aAreaLEW)
				aAreaLEW := GetArea()
			EndIf
			Reclock("LEW",.T.)
			LEW->LEW_Filial := xFilial("LEW")
			If M->C5_TIPO == "N"                
				LEW->LEW_CLIENT := M->C5_CLIENTE
			Else
				LEW->LEW_FORNEC := M->C5_CLIENTE
			EndIf
			LEW->LEW_LOJA   := M->C5_LOJACLI
			LEW->LEW_ROMANE := __aGrvRomaneio[nI,2]
			LEW->LEW_PRODUT := __aGrvRomaneio[nI,7]
			LEW->LEW_NUM    := M->C5_NUM          
			LEW->LEW_TPGER  := "2"
			MsUnlock()
		Endif
	Next nX             
	// Reinicializa array de romaneios que devem ser gravados
	__aGrvRomaneio := {}                           
	__aSelRom      := {}
Endif

// Restaura area original do arquivo LEW
If !Empty(aAreaLEW)
	RestArea(aAreaLEW)
EndIf

// Restaura area original
RestArea(aAreaAtu)

Return Nil