#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �MTA410    � Autor � Julio Cesar           � Data � 26/01/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Valida se o usuario selecionou o tipo de saida de graos,    ���
���          �caso tenha utilizado Romaneio de Saida no PV.               ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 �MTA410()                              				      ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 �Template de Cooperativa de Graos							  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function MTA410()

Local lRet     := .T.
Local nPosProd := aScan(aHeader,{|x| Trim(x[2]) == "C6_PRODUTO" })
Local nI       := 0
Local cTpProd  := ""

If HasTemplate("COL")  .And. ( Type("l310") == "U" .Or. !l310 ) 

	If ( Type("__aGrvRomaneio") == "U" )
		Public	__aGrvRomaneio := {} 
	Endif

	If Inclui .And. !Empty(__aGrvRomaneio) .And. Empty(M->C5_TPSAIGR)
		MsgAlert("Foi selecionado um ou mais Romaneios de Sa�da, para gera��o do Pedido de Venda. "+;
		         "Por favor determine qual o tipo de sa�da de Gr�o/Semente, no campo 'Saida Graos' "+;
		         "(C5_TPSAIGR), localizado no cabe�alho do Pedido de Venda.")
		         
		lRet := .F.
	
	ElseIf Inclui .And. Empty(M->C5_TPSAIGR)
		For nI := 1 To Len(aCols)
			If !aCols[nI][Len(aHeader)+1]
				cTpProd := Posicione("SB1",1,xFilial("SB1")+aCols[n][nPosProd],"B1_GRAO")
				If cTpProd $ "1|3"
					MsgAlert("Foi informado um ou mais produtos do tipo Gr�o/Semente, no Pedido de Venda. "+;
					         "Por favor determine qual o tipo de sa�da de Gr�o/Semente, no campo 'Saida Graos' "+;
					         "(C5_TPSAIGR), localizado no cabe�alho do Pedido de Venda.")
					         
					lRet := .F.				
					Exit
				EndIf
			EndIf
		Next nI
	EndIf

EndIf
Return(lRet)