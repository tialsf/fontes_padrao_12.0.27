#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MT100GE2 �Autor  � Choite             � Data � 26/02/04    ���
�������������������������������������������������������������������������͹��
���Desc.     � APOS GRAVAR O TITULO                              (MATA103)���
�������������������������������������������������������������������������͹��
���Uso       �Altera valor do titulo gerado pela NF e do provisorio.      ���
���          �Grava o desc.total da NFE no Titulo da NF Compl.(kit+royalt)���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function MT100GE2()

LOCAL aArea 	:= GetArea()
LOCAL aAreaSF1  := SF1->(GetArea())
LOCAL aAreaSD1  := SD1->(GetArea())
LOCAL aAreaSB1  := SB1->(GetArea())
LOCAL aAreaSE2  := SE2->(GetArea())
LOCAL aAreaSF4  := SF4->(GetArea())
LOCAL aAreaSA2  := SA2->(GetArea())
local cChaveSE2
Local cChaveINS
Local cCliINSS  := SubStr(SuperGetMV("MV_FORINSS",.F.,Space(TamSx3("A2_COD")[1])),1,TamSx3("A2_COD")[1])
local cSD1Ori
local cChaveSD1	:= SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA
LOCAL nQtdComp
LOCAL nQtdSaca
LOCAL nPosSD1C
LOCAL nVal1UM
LOCAL nValRef
local nMoeda
local nMoedSE2
local dDtEmiss
LOCAL nValFun0	:= 0  // valor do funcrural da NF Original  08/06/04
Local nArredVIns := TamSx3("D1_VALINS")[2]
Local nArredVTot := TamSx3("D1_TOTAL")[2]
Local nArredVUni := TamSx3("D1_VUNIT")[2]
Local nArredSaca := TamSx3("M2_MOEDA4")[2]

If HasTemplate("COL") .And. ( Type("l310") == "U" .Or. !l310 ) 

	dbSelectArea("SA2")
	dbsetorder(1)
	dbSeek(xFilial("SA2")+SE2->E2_FORNECE+SE2->E2_LOJA)
	
	// Se for complementar, atualiza saldo do titulo provisorio
	IF SF1->F1_TIPO=="C" .AND. l103Auto // apena para automatico, na rotina GERACOMP.PRW
		
		BEGIN TRANSACTION
		
		// Posiciona Produto, neste momento esta no ultimo item da complementar
		DbSelectArea("SB1")
		DbSeek(xFilial("SB1")+SD1->D1_COD)
	
		// Posiciona no primeiro item da nota complementar	
		DbSelectArea("SD1")
		DbSetOrder(1) 
		DbSeek(xFilial("SD1")+cChaveSD1)
		            
		nNovoValor := 0 // para resolver problema do PE a103vla da v710
	
		WHILE !Eof() .And. xFilial("SD1")==SD1->D1_FILIAL .AND. ;
		      cChaveSD1 == (SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA)
			
			cSD1Ori	  := SD1->D1_NFORI+SD1->D1_SERIORI+SD1->D1_FORNECE+SD1->D1_LOJA  // chave do item da nf origem
			cChaveSE2 := SD1->D1_FORNECE+SD1->D1_LOJA+substr(GETMV("MV_FIXSER"),1,3)+SD1->D1_NFORI
			nPosSD1C  := RECNO()
			
			// Realizar tratamento para codigo do fornecedor INSS
			If Len(cCliINSS) < TamSX3("A2_COD")[1]
				cCliINSS += Space(TamSX3("A2_COD")[1] - Len(cCliINSS))
			Else
				cCliINSS :=	SubStr(cCliINSS,1,TamSX3("A2_COD")[1])
			EndIf
			cChaveINS := cCliINSS+"00"+SF1->F1_PREFIXO+SD1->D1_NFORI
	
			// converte para primeira unidade, POIS A MOEDA EST� NA SEGUNDA UNIDADE (SACA)
			IF SB1->B1_CONV<>0
				IF SB1->B1_TIPCONV=="M"
					nVal1UM := Round(SD1->D1_VALSACA * SB1->B1_CONV,nArredVUni)
				ELSE
					nVal1UM := Round(SD1->D1_VALSACA / SB1->B1_CONV,nArredVUni)
				ENDIF
			ENDIF
			
			nQtdComp := SD1->D1_QTDCOMP
			nQtdSaca := Round(nQtdComp / SB1->B1_CONV,nArredSaca)
			
			// Posiciona item da NF original para olhar TES
			DbSelectArea("SD1")
			DbSetOrder(1) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
			DbSeek(xFilial("SD1")+cSD1Ori)
			
			// Posiciona tabela de TES para verifica se o TES da NF original e� a fixar
			DbSelectArea("SF4")
			DbSetOrder(1)
			DbSeek(xFilial("SF4")+SD1->D1_TES)
			IF SF4->F4_FIXAR == "S"
				// Pesquisa o titulo e acumula valor de funrural da NF Origem
				DbSelectArea("SE2")
				DbSetOrder(6) //E2_FILIAL+E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO
				If DbSeek(xFilial("SE2")+cChaveINS)
					nValFun0 += Round(nQtdComp * SD1->D1_VUNIT * (SD1->D1_ALIQINS / 100),nArredVIns)
				EndIf
				
				// acumula o valor para alterar o titulo gerado
				nNovoValor += Round(nVal1UM * nQtdComp,nArredVTot)
	
				// Abate valor do titulo provisorio
				IF DbSeek(xFilial("SE2")+cChaveSE2)
					RecLock("SE2",.F.)
					SE2->E2_SALDO	:= IIf((SE2->E2_SALDO - nQtdSaca) > 0.016667,(SE2->E2_SALDO - nQtdSaca),0)
					SE2->E2_BAIXA   := DDATABASE
					SE2->E2_MOVIMEN := DDATABASE
					MsUnlock()
					
					nValRef := nQtdSaca // VALOR A ATUALIZAR
					
					//�����������������������������������������������������Ŀ
					//� Atualiza saldo do fornecedor                        �
					//�������������������������������������������������������
					nMoeda 	 := Int(Val(GetMv("MV_MCUSTO")))
					nMoedSE2 := SE2->E2_MOEDA
					dDtEmiss := SE2->E2_EMISSAO
					RecLock("SA2",.F.)
					SA2->A2_SALDUP -= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,3),3),2)
					SA2->A2_SALDUPM-= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,nMoeda,SE2->E2_EMISSAO,3),3),2)
					MsUnlock()
				ENDIF
			ENDIF
			
			DbSelectArea("SD1")
			DbGoto(nPosSD1C)
			DbSkip()
		END
	
		IF nNovoValor <> 0
			RestArea(aAreaSE2)  // volta a posicao do titulo gerado pela complementar		
			nValRef	   := (nNovoValor - SE2->E2_VALOR)
			nNovoValor -= SE2->E2_INSS
			nNovoValor -= nValFun0 // abate funrural da NF Original
			
			RecLock("SE2",.F.)
			SE2->E2_VALOR 	:= nNovoValor
			SE2->E2_VLCRUZ	:= SE2->E2_VALOR
			SE2->E2_SALDO	:= SE2->E2_VALOR
			MsUnlock()
	
			//�����������������������������������������������������Ŀ
			//� Atualiza saldo do fornecedor                        �
			//�������������������������������������������������������
			nMoeda 	 := Int(Val(GetMv("MV_MCUSTO")))
			nMoedSE2 := SE2->E2_MOEDA
			dDtEmiss := SE2->E2_EMISSAO
	
			RecLock("SA2",.F.)             
			SA2->A2_SALDUP -= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,3),3),2)
			SA2->A2_SALDUPM-= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,nMoeda,SE2->E2_EMISSAO,3),3),2)
			MsUnlock()
		ENDIF			
		If SF1->F1_DESCONT > 0
			//�����������������������������������������������������������������������Ŀ
			//�Grava o desconto total (kit + royalty) da NFE Complem. no seu Titulo   �
			//�������������������������������������������������������������������������
			RestArea(aAreaSE2)  // volta a posicao do titulo gerado pela complementar		
			RecLock("SE2",.F.)
			SE2->E2_DECRESC	+= SF1->F1_DESCONT
			SE2->E2_SDDECRE	+= SF1->F1_DESCONT
			MsUnlock()
		EndIf
		
		END TRANSACTION
	ENDIF
	
	// Se for normal, exclui duplicata, deixando apenas o titulo de INSS
	IF SF1->F1_TIPO == "N"
		// Posiciona tabela de TES para verifica se o TES da NF original e� a fixar
		DbSelectArea("SF4")
		DbSetOrder(1)
		DbSeek(xFilial("SF4")+SD1->D1_TES)
		
		// quando for a fixar, exclui duplicatas, deixando apenas a duplicata de INSS
		IF SF4->F4_FIXAR == "S"
			// VALIDAR PARA AUMENTAR SEGURAN�A
			IF SE2->E2_PREFIXO+SE2->E2_NUM+SE2->E2_TIPO+SE2->E2_FORNECE+SE2->E2_LOJA==;
				SF1->F1_PREFIXO+SF1->F1_DOC+'NF '+SF1->F1_FORNECE+SF1->F1_LOJA
				
				BEGIN TRANSACTION
				
				nValRef := SE2->E2_SALDO // VALOR A ATUALIZAR
				Reclock("SE2",.f.)
				DbDelete()
				MsUnlock()
				
				//�����������������������������������������������������Ŀ
				//� Atualiza saldo do fornecedor                        �
				//�������������������������������������������������������
				nMoeda 	 := Int(Val(GetMv("MV_MCUSTO")))
				nMoedSE2 := SE2->E2_MOEDA
				dDtEmiss := SE2->E2_EMISSAO
				RecLock("SA2",.F.)
				SA2->A2_SALDUP -= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,3),3),2)
				SA2->A2_SALDUPM-= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,nMoeda,SE2->E2_EMISSAO,3),3),2)
				MsUnlock()
				
				END TRANSACTION
			ENDIF
		ENDIF
	ENDIF
	
	// Restaura as areas originais 
	RestArea(aAreaSF1)
	RestArea(aAreaSD1)
	RestArea(aAreaSB1)
	RestArea(aAreaSE2)
	RestArea(aAreaSF4)
	RestArea(aAreaSA2)
	RestArea(aArea)
EndIf
	
Return Nil                           
