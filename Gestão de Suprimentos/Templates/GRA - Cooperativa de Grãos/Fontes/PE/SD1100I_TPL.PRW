/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SD1100I  �Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Desc.     � GRAVA DADOS DA NFE NO ROMANEIO DE ENTRADA         (MATA103)���
�������������������������������������������������������������������������͹��
���Uso       � Gravar dados da NFE no romaneio                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function SD1100I()
LOCAL aAreaAtu := GetArea()   
Local aAreaSF4 := {}
Local aAreaLEW := {}
Local aAreaLBT := {}

If HasTemplate("COL") .And. ( Type("l310") == "U" .Or. !l310 ) 

	dbSelectArea("SF4")
	aAreaSF4 := GetArea()
	dbSetOrder(1)
	dbSeek(xFilial("SF4")+SD1->D1_TES)
	
	IF SD1->D1_TIPO='N' .Or. SD1->D1_TIPO='B' // mad 23/07/04
	    dbSelectArea("LEW")
	    aAreaLEW := GetArea()
	    dbSetOrder(1)
	    dbSeek(xFilial("LEW")+SF1->F1_DOC+SF1->F1_SERIE)
	    While !Eof() .And. (xFilial("LEW")+SF1->F1_DOC+SF1->F1_SERIE == LEW->LEW_FILIAL+LEW->LEW_DOC+LEW->LEW_SERIE)
	    	dbSelectArea("LBT")
	    	aAreaLBT := GetArea()
	    	dbSetOrder(6)
	    	If dbSeek(xFilial("LBT")+LEW->LEW_ROMANE)
				RecLock("LBT",.F.)
				LBT->LBT_STATUS  := Iif(SF4->F4_FIXAR == "S", "CF", Iif(SF4->F4_PODER3 == "N","RM","EN") )
				LBT->LBT_NUMNOT  := SD1->D1_DOC 	
				LBT->LBT_SERNOTA := SD1->D1_SERIE	
				LBT->LBT_VALOR	 := SD1->D1_VUNIT
				MsUnlock()
	        EndIf
	
	        dbSelectArea("LEW")
	    	dbSkip()
	    EndDo                                        
	EndIf
	
	// Restaura area original do arquivo SF4
	RestArea(aAreaSF4)
	
	// Restaura area original do arquivo LEW
	If !Empty(aAreaLEW)
		RestArea(aAreaLEW)
	EndIf
	
	// Restaura area original do arquivo LBT
	If !Empty(aAreaLBT)
		RestArea(aAreaLBT)
	EndIf
	
	// Restaura area original
	RestArea(aAreaAtu)      

EndIf	

Return Nil