#Include "Protheus.ch"  

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o	 �SF2520E   � Autor � Julio Cesar           � Data � 26/01/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Realiza todo o tratamento na amarracao NF saida X Romaneio  ���
���          �e no romaneio qdo uma NF de saida eh cancelada.             ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 �SF2520E()                             				      ���
�������������������������������������������������������������������������Ĵ��
��� Uso		 �Template de Cooperativa de Graos							  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function SF2520E()

Local aAreaAtu   := GetArea()
Local aAreaLEW   := {}
Local aAreaLBT   := {}
Local nInd       := If(SF2->F2_TIPO$"B|D",1,4)                            
Local cCpoCliFor := ''
Local aNumRom    := {}
Local nI         := 0

// So pesquisa a amarracao caso se trate de um Grao/Semente
If HasTemplate("COL") .And. Posicione("SB1",1,xFilial("SB1")+SD2->D2_COD,"B1_GRAO")$"1|3"

	cCpoCliFor := If(nInd == 1,LEW->(FieldPos("LEW_FORNEC")),LEW->(FieldPos("LEW_CLIENT")))
	dbSelectArea("LEW")
	aAreaLEW := GetArea()
	dbSetOrder(nInd)
	If MsSeek(xFilial("LEW")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE)                         
		While !Eof() .And. xFilial("LEW")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA == ;
		      LEW_FILIAL+LEW_DOC+LEW_SERIE+FieldGet(cCpoCliFor)+LEW_LOJA
					      
			RecLock("LEW",.F.)
			// Caso nao tenha sido gerado automaticamente apenas acerta os campos, caso contrario exclui a amarracao.
			If LEW_TPGER <> "1"
				LEW_DOC   := ""
				LEW_SERIE := ""
				LEW_ESPECI:= ""        
				AAdd(aNumRom, {LEW_ROMANE, LEW_TPGER})
			Else                              
				AAdd(aNumRom, {LEW_ROMANE, LEW_TPGER})
				dbDelete()
			EndIf
			MsUnLock()
			dbSkip()
		End
	EndIf   
	// Restaura area do arquivo LEW
	RestArea(aAreaLEW)
	
	dbSelectArea("LBT")
	aAreaLBT := GetArea()
	dbSetOrder(6)	
	For nI := 1 To Len(aNumRom)
		If MsSeek(xFilial("LBT")+aNumRom[nI][1])
			RecLock("LBT",.F.)
			If aNumRom[nI][2] <> "1" 
				LBT_NFSAID := ""
				LBT_SESAID := ""
				LBT_STATUS := "RS"
			Else 
				dbDelete()
			EndIf
			MsUnLock()
		EndIf
	Next nI         
	// Restaura area do arquivo LBT
	RestArea(aAreaLBT)

	// Restaura area original
	RestArea(aAreaAtu)
EndIf	

Return Nil
