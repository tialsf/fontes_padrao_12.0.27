/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SF1100E  �Autor  � Choite             � Data � 08/03/04    ���
�������������������������������������������������������������������������͹��
���Desc.     � ANTES DE EXCLUIR O SF1                            (MATA103)���
�������������������������������������������������������������������������͹��
���Uso       � Excluir nota a fixar e titulo provisorio                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
USER FUNCTION SF1100E()
LOCAL aArea 	:= GetArea()
LOCAL aAreaSF1  := SF1->(GetArea())
LOCAL aAreaSD1  := SD1->(GetArea())
LOCAL aAreaSE2  := SE2->(GetArea())
LOCAL aAreaSA2  := SA2->(GetArea())

// MAD - 29/07/04
LOCAL aAreaSF3  := SF3->(GetArea())
LOCAL aAreaLEW  := LEW->(GetArea())
LOCAL aAreaLBT  := LBT->(GetArea())
//

local cChaveSF1
local cChaveSD1
local nQtdComp
local nMoeda
local nMoedSE2
local dDtEmiss
LOCAL nValRef

/*/
Autor:     Fernando Roquete
Data:      21/01/2004
Funcao:    SF1100E
Descricao: Exclusao da NFE, exclui tambem a relacao da NFE x Romaneios
/*/

dbSelectArea("LEW")
dbSetorder(1)
dbSeek(xFilial("LEW")+SF1->F1_Doc+SF1->F1_Serie+SF1->F1_Fornece+SF1->F1_Loja,.F.)
While ! Eof() .And. LEW->LEW_Filial ==xFilial("LEW")  .And.;
	LEW->LEW_Doc    ==SF1->F1_Doc     .And.;
	LEW->LEW_Serie  ==SF1->F1_Serie   .And.;
	LEW->LEW_Fornec ==SF1->F1_Fornece .And.;
	LEW->LEW_Loja   ==SF1->F1_Loja
	
	dbSelectArea("LBT")
	dbSetorder(6)
	dbSeek(xFilial("LBT")+LEW->LEW_Romane,.F.)
	If Found()
		If Alltrim(LBT->LBT_Produt)==Alltrim(LEW->LEW_Produt)    // eh semente
			Reclock("LBT",.F.)
			LBT->LBT_NFESEM :=" "
			MsUnlock()
		Else
			Reclock("LBT",.F.)
			LBT->LBT_NFEDES :=" "
			MsUnlock()
		Endif
	Endif
	
	dbSelectArea("LEW")
	Reclock("LEW",.F.)
	LEW->(dbDelete())
	MsUnlock()

	dbSkip()
End
RestArea(aArea)

dbSelectArea("SA2")
dbsetorder(1)
dbSeek(xFilial("SA2")+SF1->F1_FORNECE+SF1->F1_LOJA)

// TRATAMENTO DE EXCLUSAO DE NOTA A FIXAR, QUANDO EXCLUI UMA NOTA NORMAL DE ENTRADA
IF SF1->F1_TIPO=="N"
	
	BEGIN TRANSACTION
	
	// Exclui titulo de INSS
	// cForInss := substr( GetMV( "MV_FORINSS" ),1,6)+"00" // mad - 09/08/04
	cForInss := GetMV( "MV_FORINSS" ) + "  " + "00" // mad - 09/08/04
	DbSelectArea("SE2")
	DbSetOrder(6) //E2_FILIAL+E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO

	// IF DbSeek(xFilial("SE2")+"INSS  "+"00"+SF1->F1_PREFIXO+SF1->F1_DUPL)
	IF DbSeek(xFilial("SE2")+cForInss+SF1->F1_PREFIXO+SF1->F1_DUPL) // mad - 09/08/02
		
		WHILE !eof() .AND. xFilial("SE2")==SE2->E2_FILIAL .AND.;
			(cForInss+SF1->F1_PREFIXO+SF1->F1_DUPL)==(E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM) // mad - 09/08/02
			// ("INSS  "+"00"+SF1->F1_PREFIXO+SF1->F1_DUPL)==(E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM)
			
			IF SE2->E2_SALDO==SE2->E2_VALOR
				RecLock("SE2",.F.)
				SE2->(DbDelete())
				MsUnlock()
				
			ENDIF
			
			DbSkip()
		END
		
	ENDIF

	
	cChaveSF1:= SF1->F1_DOC+substr(GETMV("MV_FIXSER"),1,3)+SF1->F1_FORNECE+SF1->F1_LOJA
	
	// Posiciona cabecalho da NF a fixar
	DbSelectArea("SF1")
	DbSetOrder(1) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
	
	IF DbSeek(xFilial("SF1")+cChaveSF1)
		
		// Exclui Livros
		DbSelectarea("SF3")
		DbSetOrder(4) //F3_FILIAL+F3_CLIEFOR+F3_LOJA+F3_NFISCAL+F3_SERI
		IF DbSeek(xFilial("SF3")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_DOC+SF1->F1_SERIE)
			RecLock("SF3",.F.)
			SF3->(DbDelete())
			MsUnLock()
		ENDIF
		
		// Exclui titulo provisorio
		DbSelectArea("SE2")
		DbSetOrder(6) //E2_FILIAL+E2_FORNECE+E2_LOJA+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO
		IF DbSeek(xFilial("SE2")+SF1->F1_FORNECE+SF1->F1_LOJA+SF1->F1_PREFIXO+SF1->F1_DUPL)
			nValRef := SE2->E2_SALDO // VALOR A ATUALIZAR
			RecLock("SE2",.F.)
			SE2->(DbDelete())
			MsUnlock()
			
			//�����������������������������������������������������Ŀ
			//� Atualiza saldo do fornecedor                        �
			//�������������������������������������������������������
			nMoeda 	 := Int(Val(GetMv("MV_MCUSTO")))
			nMoedSE2 := SE2->E2_MOEDA
			dDtEmiss := SE2->E2_EMISSAO
			
			DbSelectArea("SA2") // MAD - 29/07/04
			RecLock("SA2",.F.)
			SA2->A2_SALDUP -= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,1,SE2->E2_EMISSAO,3),3),2)
			SA2->A2_SALDUPM-= Round(NoRound(xMoeda(nValRef,SE2->E2_MOEDA,nMoeda,SE2->E2_EMISSAO,3),3),2)
			MsUnlock()
			
		ENDIF
		
		// Exclui itens da nota
		DbSelectArea("SD1")
		DbSetOrder(1) //D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
		DbSeek(xFilial("SD1")+cChaveSF1)
		while !EOF() .and. xFilial("SD1")==SD1->D1_FILIAL .AND.;
			(SF1->F1_DOC+SF1->F1_SERIE+SF1->F1_FORNECE+SF1->F1_LOJA)==(SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_FORNECE+SD1->D1_LOJA)
			
			RecLock("SD1",.F.)
			SD1->(DbDelete())
			MsUnLock()
			DbSkip()
		END
		
		
		// Exclui cabecalho da Nota
        dbSelectArea("SF1") // MAD - 29/07/01
		RecLock("SF1",.F.)
		SF1->(DbDelete())
		MsUnLock()
		
	ENDIF
	
	END TRANSACTION
	
ENDIF

// MAD - 29/07/04
RestArea(aAreaSF3)
RestArea(aAreaLEW)
RestArea(aAreaLBT)
// 

RestArea(aAreaSF1)
RestArea(aAreaSD1)
RestArea(aAreaSE2)
RestArea(aAreaSA2)
RestArea(aArea) 

// RETURN()
RETURN


