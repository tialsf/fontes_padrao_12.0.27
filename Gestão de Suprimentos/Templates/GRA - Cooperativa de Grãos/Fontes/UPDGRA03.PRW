#INCLUDE "Protheus.ch"

/*
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    砋pdGRA03  � Autor 砇icardo Berti          � Data � 18.02.09 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Atualizacao das tabelas do COL_GRA         				  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Nenhum                                                     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Template COL_GRA                                           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Nenhum                                                     潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
User Function UpdGRA03() //Para maiores detalhes sobre a utlizacao deste fonte consulte FNC 10010
					    
cArqEmp := "SigaMat.Emp"
__cInterNet := Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd 

Set Dele On

lHistorico 	:= MsgYesNo("Deseja efetuar a atualiza玢o do Dicion醨io? Esta rotina deve ser utilizada em modo exclusivo ! Faca um backup dos dicion醨ios e da Base de Dados antes da atualiza玢o para eventuais falhas de atualiza玢o !", "Aten玢o")
lEmpenho	:= .F.
lAtuMnu		:= .F.

DEFINE WINDOW oMainWnd FROM 0,0 TO 01,30 TITLE "Atualiza玢o do Dicion醨io"

ACTIVATE WINDOW oMainWnd ;
	ON INIT If(lHistorico,(Processa({|lEnd| GraProc(@lEnd)},"Processando","Aguarde , processando prepara玢o dos arquivos",.F.) , Final("Atualiza玢o efetuada!")),oMainWnd:End())
	
Return

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � GRAProc  � Autor 砇icardo Berti          � Data � 18.02.09 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao dos arquivos           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao COL_GRA                                        潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function GRAProc(lEnd)
Local cTexto    := ''
Local cFile     :=""
Local cMask     := "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    := 0
Local nI        := 0
Local nX        :=0
Local aRecnoSM0 := {}     
Local lOpen     := .F. 

ProcRegua(1)
IncProc("Verificando integridade dos dicion醨ios....")
If ( lOpen := MyOpenSm0Ex() )

	dbSelectArea("SM0")
	dbGotop()
	While !Eof() 
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0 //--So adiciona no aRecnoSM0 se a empresa for diferente
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	
		
	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2) 
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
			nModulo := 2 // MODULO COMPRAS

			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)
			//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
			//矨tualiza o dicionario de dados (SX3) �
			//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
			IncProc("Analisando Dicionario de Dados...")
			cTexto += GRAAtuSX3()

			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso("Aten玢o!","Ocorreu um erro desconhecido durante a atualiza玢o da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicion醨io e da tabela.",{"Continuar"},2)
					cTexto += "Ocorreu um erro desconhecido durante a atualiza玢o da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
				EndIf
			Next nX		
			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit 
			EndIf 

		Next nI 
		   
		If lOpen
			
			cTexto := "Log da atualiza玢o "+CHR(13)+CHR(10)+cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12   //6,15
			DEFINE MSDIALOG oDlg TITLE "Atualiza玢o conclu韉a." From 3,0 to 340,417 PIXEL
			@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
			oMemo:bRClicked := {||AllwaysTrue()}
			oMemo:oFont:=oFont
			DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
			DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
			
		EndIf 
		
	EndIf
		
EndIf 	

Return(.T.)


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矴RAAtuSX3 � Autor 砇icardo Berti          � Data � 18.02.09 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao do SX3 - Campos        潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao COL_GRA                                        潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌*/
Static Function GRAAtuSX3()
Local aEstrut        := {}
Local aSX3           := {}
Local cTexto         := ''
Local cAlias         := ''
Local i              := 0
Local j              := 0

aEstrut:= { "X3_ARQUIVO","X3_ORDEM"  ,"X3_CAMPO"  ,"X3_TIPO"   ,"X3_TAMANHO","X3_DECIMAL","X3_TITULO" ,"X3_TITSPA" ,"X3_TITENG" ,;
	"X3_DESCRIC","X3_DESCSPA","X3_DESCENG","X3_PICTURE","X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO","X3_F3"     ,"X3_NIVEL"  ,;
	"X3_RESERV" ,"X3_CHECK"  ,"X3_TRIGGER","X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT","X3_VLDUSER",;
	"X3_CBOX"   ,"X3_CBOXSPA","X3_CBOXENG","X3_PICTVAR","X3_WHEN"   ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER", "X3_PYME"}

/*
//--Pesquisa um campo existente para gravar o Reserv e o Usado
	If SX3->(MsSeek("LJX_NFSAID")) //Este campo e obrigatorio e permite alterar
		For nI := 1 To SX3->(FCount())
			If "X3_RESERV" $ SX3->(FieldName(nI))
				cReservObrig := SX3->(FieldGet(FieldPos(FieldName(nI))))
			EndIf
			If "X3_USADO"  $ SX3->(FieldName(nI))
				cUsadoObrig  := SX3->(FieldGet(FieldPos(FieldName(nI))))
			EndIf
		Next 							
	EndIf		
*/

//Atualiza玢o de campos do Template ref. ao grupo de campos

dbSelectArea("SXG")
dbSelectArea("SX3")
SX3->(DbSetOrder(2))	

If SXG->(dbSeek("018"))

	If SX3->(dbSeek("LBT_NFFORN")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
			Aadd(aSX3,{"LBT"})
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LBT_NUMNOT")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LBT_NFSAID")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LEW_DOC")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018", X3_PICTURE WITH "@!"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LEW"}) == 0
				Aadd(aSX3,{"LEW"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LKT_DOC")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LKT"}) == 0
				Aadd(aSX3,{"LKT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LJX_NFSAID")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018", X3_PICTURE WITH "@!"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LJX"}) == 0
				Aadd(aSX3,{"LJX"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LJX_NFENT")) .And. (SX3->X3_GRPSXG <> "018" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "018", X3_PICTURE WITH "@!"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LJX"}) == 0
				Aadd(aSX3,{"LJX"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
EndIf

If SXG->(dbSeek("001"))

	If SX3->(dbSeek("LBT_CLIENT")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LBT_FORNEC")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LEW_FORNEC")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LEW"}) == 0
				Aadd(aSX3,{"LEW"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LEW_CLIENT")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LEW"}) == 0
				Aadd(aSX3,{"LEW"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LJX_CLIENT")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LJX"}) == 0
				Aadd(aSX3,{"LJX"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LKT_FORNEC")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		 	If Ascan(aSX3,{ |x| x[1] == "LKT"}) == 0
				Aadd(aSX3,{"LKT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LBU_FORNTR")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBU"}) == 0
				Aadd(aSX3,{"LBU"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("F1_FORNTR")) .And. (SX3->X3_GRPSXG <> "001" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "001"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "SF1"}) == 0
				Aadd(aSX3,{"SF1"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
EndIf

If SXG->(dbSeek("002"))

	If SX3->(dbSeek("LBT_LOJA")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LEW_LOJA")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LEW"}) == 0
				Aadd(aSX3,{"LEW"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LJX_LOJA")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LJX"}) == 0
				Aadd(aSX3,{"LJX"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LKT_LOJA")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		 	If Ascan(aSX3,{ |x| x[1] == "LKT"}) == 0
				Aadd(aSX3,{"LKT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LBU_LOJA")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBU"}) == 0
				Aadd(aSX3,{"LBU"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("F1_LOJATR")) .And. (SX3->X3_GRPSXG <> "002" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "002"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "SF1"}) == 0
				Aadd(aSX3,{"SF1"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
EndIf
	
If SXG->(dbSeek("030"))

	If SX3->(dbSeek("LBT_PRODUT")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LEW_PRODUT")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		 	If Ascan(aSX3,{ |x| x[1] == "LEW"}) == 0
				Aadd(aSX3,{"LEW"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LJX_PRODUT")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LJX"}) == 0
				Aadd(aSX3,{"LJX"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LKT_PRODUT")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LKT"}) == 0
				Aadd(aSX3,{"LKT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("B1_CODDES")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "SB1"}) == 0
				Aadd(aSX3,{"SB1"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("B1_DSCTRIG")) .And. (SX3->X3_GRPSXG <> "030" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "030"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "SB1"}) == 0
				Aadd(aSX3,{"SB1"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
EndIf

If SXG->(dbSeek("010"))

	If SX3->(dbSeek("LBT_ESTADO")) .And. (SX3->X3_GRPSXG <> "010" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "010"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LBT"}) == 0
				Aadd(aSX3,{"LBT"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
	If SX3->(dbSeek("LDY_EST")) .And. (SX3->X3_GRPSXG <> "010" .Or. SX3->X3_TAMANHO <> SXG->XG_SIZE)
		RecLock("SX3")
		Replace X3_GRPSXG WITH "010"
		If SX3->X3_TAMANHO <> SXG->XG_SIZE
			Replace X3_TAMANHO WITH SXG->XG_SIZE
		  	If Ascan(aSX3,{ |x| x[1] == "LDY"}) == 0
				Aadd(aSX3,{"LDY"})
	  		EndIf
		EndIf
		dbCommit()
		MSUnlock()        
	EndIf
EndIf

ProcRegua(Len(aSX3))

SX3->(DbSetOrder(2))	

For i:= 1 To Len(aSX3)
	If !Empty(aSX3[i][1])
		If !(aSX3[i,1]$cAlias)
			cAlias += aSX3[i,1]+"/"
			aAdd(aArqUpd,aSX3[i,1])
		EndIf
	EndIf
Next i

If Len(aSX3) > 0
	cTexto := 'Foram alterados os dicionarios das seguintes tabelas : '+cAlias+CHR(13)+CHR(10)
EndIf

Return cTexto



/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    矼yOpenSM0Ex� Autor 矼icrosiga             � Data �07/01/2003潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Efetua a abertura do SM0 exclusivo                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao FIS                                            潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function MyOpenSM0Ex()

Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 To 20
	dbUseArea( .T.,, "SIGAMAT.EMP", "SM0", .F., .F. ) 
	If !Empty( Select( "SM0" ) ) 
		lOpen := .T. 
		dbSetIndex("SIGAMAT.IND") 
		Exit	
	EndIf
	Sleep( 500 ) 
Next nLoop 

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 ) 
EndIf                                 

Return( lOpen ) 