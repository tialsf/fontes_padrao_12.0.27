#include "Protheus.ch"
#include "TopConn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA04 � Autor �MARCO AURELIO (TRT005) � Data � 25/09/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Fechamento do Graos por Periodo                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

// Observacoes
// - No fechamento verificar possibilidade de exibir valores e financeiros separados;
// - Programa que far� o levantamento das entradas, perman�ncia, e sa�da do graos de cooperado;
//		- Sera calculado o valor a ser pago pelo armazenamento por kg/dia, conforme valor gravado no 
//        parametro MV_MIARM;
//	 	- A partir deste levantamento ser� gerado o Contas a Receber para o cooperado, como o valor a ser pago pelo
//	   	  armazenamento e tamb�m secagem;
//		- Ser� poss�vel escolher:
//			* Um, varios ou todos os cooperados - listbox;
//			* Gera custo armazenamento - flag;
//			* Gera custo secagem - flag;
//			* Periodo a ser considerado - get dtini e dtfim;
// - Resta definir sobre a parte financeira;
// - Foi implementado quebra tecnica;

Template Function CACMIA04()

Local cQuery  := ""

PRIVATE oTik   := LoadBitmap( GetResources(), "LBTIK" )
PRIVATE oNo    := LoadBitmap( GetResources(), "LBNO" )
PRIVATE aVetVen:= {}
PRIVATE cTipoGr:= Space(4) //Tipo de Grao
PRIVATE cDescGr, oDescGr, oGerCrc
PRIVATE lGerCrc := .F.

ChkTemplate("COL")

cQuery := "SELECT DISTINCT A2_COD, A2_LOJA, A2_NOME, A2_END"
cQuery += " FROM "
cQuery += RetSqlName("SA2")+" SA2, "
cQuery += RetSqlName("LBT")+" LBT"
cQuery += " WHERE"             
cQuery += " SA2.A2_FILIAL = '" + xFilial("SA2") + "' AND"
cQuery += " LBT.LBT_FILIAL = '" + xFilial("LBT") + "' AND"
cQuery += " LBT.LBT_FORNEC = SA2.A2_COD AND"
cQuery += " LBT.LBT_LOJA = SA2.A2_LOJA AND"
cQuery += " LBT.D_E_L_E_T_ <> '*' AND SA2.D_E_L_E_T_ <> '*' "
cQuery += " ORDER BY A2_NOME"
cQuery := ChangeQuery(cQuery)

dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "RQRY", .F., .T.)

dbGoTop()
While !Eof()
	//01-marca,02-cod,03-nome,04-loja,05-end,06-qtde,07-armaz,08-seca,09-quebra,10-Total Quebra,11-Finan.Quebra
	//12-Total Arm, 13-Financ.Arm, 14-Total Sec, 15-Financ.Sec
	AAdd(aVetVen,{.T.,A2_COD,A2_NOME,A2_LOJA,A2_END,0,0,0,0,0,0,0,0,0,0})
	DbSkip()
EndDo

If Len(aVetVen) == 0
	AAdd(aVetVen,{.T.,A2_COD,A2_NOME,A2_LOJA,A2_END,0,0,0,0,0,0,0,0,0,0})
Endif

dbSelectArea("RQRY")
dbCloseArea()  //FECHA O ALIAS DA QUERY

lMarc   := .t. // Controla Tela de Fornecedores (Marca ou Desmarca Todos)
lWhenF  := .t.
lWhenD  := .t.
lArm    := .t.
lSec    := .t.
lQuebra := .t.
lNfRec  := .f.
dDatIni := dDataBase-30
dDatFim := dDataBase
nTxArm  := nOpca := 0

DEFINE MSDIALOG oDlg TITLE OemToAnsi("") From 9,1 to 037,100 of oMainWnd
@ 13,001 LISTBOX oLbox FIELDS HEADER ;
OemToAnsi("M"),;
OemToAnsi("Codigo"),;
OemToAnsi("Fornecedor"),;
OemToAnsi("Lj"),;
OemToAnsi("Propriedade"),;
OemToAnsi("Qtd"),;
OemToAnsi("Armaz"),;
OemToAnsi("Secagem"),;
OemToAnsi("Quebra"),;
OemToAnsi("Total Qbr"),;
OemToAnsi("Financ.Qbr"),;
OemToAnsi("Total Arm"),;
OemToAnsi("Financ.Arm"),;
OemToAnsi("Total Sec"),;
OemToAnsi("Financ.Sec");
COLSIZES 5,25,110,10,100,30,30,30,30,30,30,30,30,30,150;
SIZE 392,150 OF oDlg PIXEL ON DBLCLICK (nPos:=oLbox:nAt,FS_Marc(),oLbox:Refresh(),oLbox:nAt:=nPos) Centered
oLbox:SetArray(aVetVen)
oLbox:bLine := { || { if(aVetVen[oLbox:nAt,01] == .f.,oNo,oTik),;
aVetVen[oLbox:nAt,02],;
aVetVen[oLbox:nAt,03],;
aVetVen[oLbox:nAt,04],;
aVetVen[oLbox:nAt,05],;
aVetVen[oLbox:nAt,06],;
Transform(aVetVen[oLbox:nAt,07],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,08],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,09],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,10],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,11],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,12],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,13],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,14],"@E 999,999.99"),;
Transform(aVetVen[oLbox:nAt,15],"@E 999,999.99")}}

@ 165,005 BUTTON oBtn1 PROMPT "Marca/Desmarca Todos"   SIZE 60,10 OF oDlg PIXEL ACTION FS_InvFlag()
@ 175,005 BUTTON oBtn2 PROMPT "Imprimir"               SIZE 60,10 OF oDlg PIXEL ACTION CaImprime()
@ 185,005 BUTTON oBtn3 PROMPT "Detalhes"               SIZE 60,10 OF oDlg PIXEL ACTION Detalhes()
@ 175,090 Say OemToAnsi("Periodo") of Odlg Pixel
@ 175,120 MSGET    oDatIni VAR dDatIni  PICTURE "@D"  VALID !Empty(dDatIni) SIZE 43,04 OF oDlg PIXEL COLOR CLR_BLACK  when lWhenD
@ 175,170 MSGET    oDatFim VAR dDatFim  PICTURE "@D"  VALID !Empty(dDatFim) SIZE 43,04 OF oDlg PIXEL COLOR CLR_BLACK  when lWhenD

@ 175,240 Say OemToAnsi("Tipo de Grao") of Odlg Pixel
@ 175,280 MSGET  oTipoGr VAR cTipoGr  PICTURE "@!" F3 "SBM" VALID f_ValGrao(cTipoGr) SIZE 30,04 OF oDlg PIXEL COLOR CLR_BLACK  when lWhenD
@ 175,320 Say    oDescGr VAR cDescGr  SIZE 90,6 OF oDlg PIXEL COLOR CLR_BLUE

@ 195,005 CHECKBOX oQuebra VAR lQuebra  PROMPT oemtoansi("Quebra ?")                SIZE 53, 10
@ 195,090 CHECKBOX oArm    VAR lArm     PROMPT oemtoansi("Armazenamento ?")         SIZE 53, 10
@ 195,170 CHECKBOX oSec    VAR lSec     PROMPT oemtoansi("Secagem ?")               SIZE 53, 10
@ 195,240 CHECKBOX oGerCrc VAR lGerCrc  PROMPT oemtoansi("Gera Contas a Receber ?") SIZE 100, 10

//CASO TENHA QUE ABILITAR A ROTINA PARA GERACAO DE NOTA DE SERVI�O, LIBERE O CHECKBOX ABAIXO
//@ 195,260 CHECKBOX oNfRec VAR lNfRec PROMPT oemtoansi("Recibo [ ] - Nf Servico [X]") SIZE 100, 10 when .F.

ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg,{||nOpca := 1,Processa({|| CaFecha("Processando...")}),oLbox:Refresh()},{||nOpca := 0,oDlg:End() })

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CaFecha  � Autor � MARCO AURELIO         |      |          ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Gera Titulo no CRC                                         ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CaFecha()

Local aVetValQb:= {}
Local aRetSafra:= {}
Local cQuery   := ""       
Local cASafra  := ""
Local aAreaAtu := GetArea()
Local aAreaSB6 := {}
Local iForn    := 0
Local nCntDia  := 0
Local _j       := 0                
Local nDec     := MsDecimais(1)
Local cPrefSEC := T_fGetPref("SEC")
Local cPrefARM := T_fGetPref("ARM")
Local cPrefQUE := T_fGetPref("QUE")
Local cPrefs   := cPrefSEC+Space(TamSX3("E1_PREFIXO")[1] - Len(cPrefSEC))+"','"+;
                  cPrefARM+Space(TamSX3("E1_PREFIXO")[1] - Len(cPrefARM))+"','"+;
                  cPrefQUE+Space(TamSX3("E1_PREFIXO")[1] - Len(cPrefQUE))
Local lGerARM := .F.
Local lGerSEC := .F.
Local lGerQUE := .F.
Local lCAC04GFIN := ExistBlock('CAC04GFIN')
Local aRetGerFin := {}
Local lRetGerFin := .T.

// Observacoes
// - Receber o valor de armazenagem e os dias de carencia para esta safra
// - Receber o codigo do fornecedor e loja do vetor e gerar uma
//   query com as informacoes da movimentacao de estoque
// - Considerar os dias de car�ncia e par�metro sobre pre�o armazenagem
// - Criar tabela para armazenar dias de car�ncia para safras no periodo

If Empty(cTipoGr)
   MsgInfo("Tipo de Grao nao foi informado!","Atencao!")
   Return
Endif                  

// Limpa os valores do array aVetVen.
aEval(aVetVen,{|x,y| x[06]:=x[07]:=x[08]:=x[09]:=x[10]:=x[11]:=x[12]:=x[13]:=x[14]:=x[15]:=0 })

// Verificacao da Safra
aRetSafra := T_CaPesqSafra(dDatIni,dDatFim,cTipoGr) 
If !aRetSafra[1]
	MsgAlert("Per�odo digitado n�o coincide com nenhuma safra cadastrada!")
	Return
Else
	cASafra := aRetSafra[2]
EndIf

cSafra    := (cASafra)->LBU_CODIGO
cCarencia := AllTrim(STR((cASafra)->LBU_CARENC))	//dias de carencia para iniciar cobran�a de armazenamento
ndias     := dDatFim - dDatIni   					//quantidade de dias a processar
nTxArm	  := ((cASafra)->LBU_VLARM / 30)  //Valor da Armazenagem por Dia (sera' dividida pelo fator conv.da saca)
nIndQM    := (cASafra)->LBU_QUEBRA //Indice de calculo para a 'Quebra'
nQtdDel   := 0

//Pesquisa itens nao marcados se encontra deleta
While (nPos := Ascan(aVetVen,{|x| x <> Nil .and. x[1]==.F. })) > 0 
	Adel(aVetVen,nPos)                                               
	nQtdDel++
EndDo

aSize(aVetVen,Len(aVetVen)-nQtdDel)

Slvndias   := ndias
SlvdDatFim := dDatFim
SlvdDatIni := dDatIni

//Percorre os fornecedores para os calculos
For iForn := 1 To Len(aVetVen) 
	
	//Verifica se ha uma fechamento dentro do periodo especificado
	//se houver a data inicio e um dia apos o ultimo fechamento
	
	nVlArm:=0 ; nVlSec:=0  ; aLevSldArm := {} ; nTotQuebra := 0
	
	ProcRegua(nDias)
	
	// Select para buscar os titulos j� gerados no periodo de Contas a receber de secagem e armazenagem
	// Jeane - 24/09/02
	_aSe1:={}
	cQuery := "SELECT E1_EMISSAO, E1_PREFIXO, E1_NUM, E1_VALOR, E1_FILIAL, E1_CLIENTE, E1_LOJA "
	cQuery += "FROM " + RetSqlName("SE1") + " "
	cQuery += "WHERE "
	cQuery += "E1_FILIAL = '" + xFilial("SE1") + "' AND "	
	cQuery += "E1_PREFIXO IN ('"+cPrefs+"') AND "
	cQuery += "E1_EMISSAO BETWEEN '" + DTOS(dDatIni)  + "' AND '" + DTOS(dDatFim)  + "' AND "
	cQuery += "E1_CLIENTE = '"+aVetVen[iForn,2]+"' AND E1_LOJA = '"+aVetVen[iForn,4]+"' AND "
	cQuery += "SUBSTRING(E1_HIST,9,4) = '"+cTipoGR+"' AND "
	cQuery += "D_E_L_E_T_ <> '*'"
	cQuery := ChangeQuery(cQuery)
	
	dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "QSE1", .F., .T.)
	
	TCSetField("QSE1", "E1_EMISSAO", "D", 8, 0)	
	TCSetField("QSE1", "E1_VALOR"  , "N", TamSx3("E1_VALOR")[1], TamSx3("E1_VALOR")[2])	

	dbGoTop()
	While !Eof()
		AAdd(_aSE1,{QSE1->E1_EMISSAO,QSE1->E1_PREFIXO,QSE1->E1_VALOR})
		dbskip()
	End   
	dbSelectArea("QSE1")      
	dbCloseArea()
	
	dbSelectArea("SA2")      
	If lArm
		For nCntDia := 0 To nDias
			IncProc("Fornecedor " + aVetVen[iForn,2] + "   Loja "+ aVetVen[iForn,4] +"               " + ALLTRIM(str(nCntDia)) + " de " + ALLTRIM(str(nDias))+" dias")
		
			dDataFim   := dDatIni + nCntDia  //data final para funcao de calculo de saldos
		
			aLevSldArm := T_CaCalcTerc(aVetVen[iForn,2],aVetVen[iForn,4],dDatIni,dDataFim,cTipoGr)  //CLIENTE, LOJA E PER�ODO
			nVlArm     += aLevSldArm[2]
		Next nCntDia
		
		// ******** PARA ARMAZENAMENTO
		// Alteracao feita por jeane - 24/09/02
		// Debita recebimentos anteriores
		// Incluido por Choite em 25/03/03 - so debita valor do titulo se tiver valor de armazenagem, senao exibde
		// valor negativo                
		If nVlArm > 0
			nTotFin := 0
			aVetVen[iForn,12] := Round(nVlArm,nDec)		//Insere total sem financeiro
			For _j := 1 To Len(_aSE1)
				IF Alltrim(_aSE1[_J,2]) == T_fGetPref("ARM")	  // armazenagem
					nVlArm  -= _aSE1[_J,3]
					nTotFin += _aSE1[_J,3]
				ENDIF
			Next _j
			aVetVen[iForn,13] := Round(nTotFin,nDec)	//Total de Titulos gerados para armazenagem'
			aVetVen[iForn,07] := Round(nVlArm,nDec)  	//Insere total com financeiro
			//***************************************
			
			aVetVen[iForn,6] := Round(aLevSldArm[1],nDec)  //Saldo no ultimo dia do periodo
			
		EndIf
	EndIf
	
	IF lSec                      
		MsAguarde( {|| nVlSec := CaSecagem(aVetVen[iForn,2],aVetVen[iForn,4],dDatIni,dDatFim)}, "Aguarde...", "Calculando o valor de secagem..." )
		
		// ********* PARA SECAGEM
		// Alteracao feita por jeane - 24/09/02
		// Debita recebimentos anteriores
		// Incluido por Choite em 25/03/03 - so debita valor do titulo se tiver valor de secagem, senao exibde
		// valor negativo
		If nVlSec > 0
			nTotFin := 0                     
			aVetVen[iForn,14] := Round(nVlSec,nDec)	//Insere total sem financeiro  	
			For _j := 1 To Len(_aSE1)
				If AllTrim(_aSE1[_J,2]) == T_fGetPref("SEC")	 // secagem
					nVlSec  -= _aSE1[_J,3]  
					nTotFin += _aSE1[_J,3]
				ENDIF
			Next _j
			aVetVen[iForn,15] := Round(nTotFin,nDec)	//Total de Titulos gerados para secagem
			aVetVen[iForn,08] := Round(nVlSec,nDec)	//Insere total com financeiro
					
		EndIf
	EndIf
	
	// calculo do valor de quebra
	If lQuebra
		MsAguarde( {|| T_CalcValQB(aVetVen[iForn,2],aVetVen[iForn,4],dDatIni,dDatFim,cTipoGr,@aVetValQb)}, "Aguarde...", "Calculando o valor de quebra..." )		
 		nTotQuebra := 0
		For _j := 1 To Len(aVetValQb)
			nTotQuebra += aVetValQb[_j,3]
		Next _j 
	
		// ******** PARA QUEBRA
		// Debita recebimentos anteriores
		// Incluido por Choite em 25/03/03 - so debita valor do titulo se tiver valor de armazenagem, senao exibde
		// valor negativo
		If nTotQuebra > 0
			nTotFin := 0
			aVetVen[iForn,10] := Round(nTotQuebra,nDec)	 //Insere total sem financeiro
			For _j := 1 To Len(_aSE1)
				If AllTrim(_aSE1[_J,2]) == T_fGetPref("QUE")
					nTotQuebra -= _aSE1[_J,3]
					nTotFin    += _aSE1[_J,3]
				EndIf
			Next _j
			aVetVen[iForn,11] := Round(nTotFin,nDec) 		//Total de Titulos gerados para quebra
			aVetVen[iForn,09] := Round(nTotQuebra,nDec)	//Insere total com financeiro

		EndIf
	EndIf
	
	If lGerCrc

		If lArm .And. aVetVen[iForn,07] > 0
			lRetGerFin := .T.
			If lCAC04GFIN
				aRetGerFin := ExecBlock('CAC04GFIN', .F., .F., {"ARM",aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,07],cSafra,cTipoGR})
				If ValType(aRetGerFin) == "A"
					If ValType(aRetGerFin[1]) == "L"
						lRetGerFin := aRetGerFin[1]
					EndIf
				Endif
			EndIf
			lGerArm := .T.
			If lRetGerFin
				lGerARM := CaGrFinanc(aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,07],cSafra,"ARM",cTipoGR)   //gerar financeiro dos custos de Armazenamento para o fornecedor
			EndIf
			// Atualiza a tela de detalhes                
			If lGerARM .And. lRetGerFin
				aVetVen[iForn,13] += aVetVen[iForn,07]	
				aVetVen[iForn,07] := 0
			EndIf
		EndIf

		If lSec .And. aVetVen[iForn,08] > 0
			lRetGerFin := .T.
			If lCAC04GFIN
				aRetGerFin := ExecBlock('CAC04GFIN', .F., .F., {"SEC",aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,08],cSafra,cTipoGR})
				If ValType(aRetGerFin) == "A"
					If ValType(aRetGerFin[1]) == "L"
						lRetGerFin := aRetGerFin[1]
					EndIf
				Endif
			EndIf
			lGerSec := .T.
			If lRetGerFin
				lGerSEC := CaGrFinanc(aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,08],cSafra,"SEC",cTipoGR)  //gerar financeiro dos custos de Secagem para o fornecedor
			EndIf
			// Atualiza a tela de detalhes                				
			If lGerSEC .And. lRetGerFin
				aVetVen[iForn,15] += aVetVen[iForn,08]
				aVetVen[iForn,08] := 0
			EndIf
		EndIf

		// Gera financeiro da quebra
		If lQuebra .And. aVetVen[iForn,09] > 0
			lRetGerFin := .T.
			If lCAC04GFIN
				aRetGerFin := ExecBlock('CAC04GFIN', .F., .F., {"QUE",aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,09],cSafra,cTipoGR})
				If ValType(aRetGerFin) == "A"
					If ValType(aRetGerFin[1]) == "L"
						lRetGerFin := aRetGerFin[1]
					EndIf
				Endif
			EndIf
			lGerQUE := .T.
			If lRetGerFin
				lGerQUE := CaGrFinanc(aVetVen[iForn,2],aVetVen[iForn,4],aVetVen[iForn,09],cSafra,"QUE",cTipoGR)  //gerar financeiro dos custos de QUEBRA para o fornecedor
			EndIf
			// Atualiza a tela de detalhes                				
			If lGerQUE .And. lRetGerFin
				aVetVen[iForn,11] += aVetVen[iForn,09]	
				aVetVen[iForn,09] := 0					
			EndIf
		EndIf

		If lArm .And. lGerARM  // Armazenagem gerou contas a receber
			//O UPDATE BAIXO GRAVA O CAMPO B6_CRCMI = 'S' com o objetivo de
			//informar que ja foi gerado armazemanento para o periodo
			//de modo que evita fechar mais de uma vez uma mesma movimentacao
			cQuery := " SELECT SB6.R_E_C_N_O_ AS NREC"
			cQuery += " FROM "
			cQuery += RetSqlName("SB6") + " SB6, "
			cQuery += RetSqlName("SF4") + " SF4, "
			cQuery += RetSqlName("SB1") + " SB1 "
			cQuery += " WHERE"                                
			cQuery += " SF4.F4_FILIAL = '" + xFilial("SF4") + "' AND"
			cQuery += " SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND"
			cQuery += " SB6.B6_FILIAL = '" + xFilial("SB6") + "' AND"
			cQuery += " SB6.B6_PRODUTO = SB1.B1_COD AND"
			cQuery += " SB6.B6_CLIFOR = '"+aVetVen[iForn,2]+"' AND"
			cQuery += " SB6.B6_LOJA = '"+aVetVen[iForn,4]+"' AND"			
			cQuery += " SB6.B6_TES = SF4.F4_CODIGO AND"
			cQuery += " SB1.B1_GRUPO = '"+cTipoGr+"' AND"
			cQuery += " SB6.B6_DTDIGIT BETWEEN '"+DtoS(dDatIni)+"' AND '"+DtoS(dDatFim)+"' AND"
			cQuery += " SB6.B6_CRCMI = ' ' AND"
			cQuery += " SB6.D_E_L_E_T_ <> '*' AND"
			cQuery += " SB1.D_E_L_E_T_ <> '*' AND"
			cQuery += " SF4.D_E_L_E_T_ <> '*'"
			cQuery := ChangeQuery(cQuery)
	
			dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "SB6TMP", .F., .T.)
			
			dbSelectArea("SB6")
			aAreaSB6 := GetArea()
			While !SB6TMP->(Eof())
				dbGoTo(SB6TMP->NREC)
				RecLock("SB6",.F.)
				B6_CRCMI   := "S"
				B6_DTCRCMI := dDatFim
				MsUnLock()
				SB6TMP->(dbSkip())				
			EndDo
		
			// Restaura a area original do arquivo SB6   
			RestArea(aAreaSB6)
  			             
			// Fecha area temporaria do arquivo SB6
			dbSelectArea("SB6TMP")
			dbCloseArea()
		EndIf    
        
	EndIf
Next

If lGerCrc
	lGerCrc := .F.
	oGerCrc:Refresh()
EndIf

// Fecha area temporaria do arquivo LBU
dbSelectArea(cASafra)
dbCloseArea()

// Restaura area original
RestArea(aAreaAtu)                         

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_Marc        � Autor �  Manoel          � Data   26/06/00 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca no ListBox do Filtro                                 ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_Marc()

If aVetVen[oLbox:nAt,1] == .f.
	aVetVen[oLbox:nAt,1] := .t.
Else
	aVetVen[oLbox:nAt,1] := .f.
Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_ParcE1      � Autor �  Manoel          � Data   19/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Acha valor numerico para Parcelas com valor Alfa           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������

Static Function FS_ParcE1(cParc)

if cParc == "A"
	nParc := "10"
Elseif cParc == "B"
	nParc := "11"
Elseif cParc == "C"
	nParc := "12"
Elseif cParc == "D"
	nParc := "13"
Elseif cParc == "E"
	nParc := "14"
Elseif cParc == "F"
	nParc := "15"
Elseif cParc == "G"
	nParc := "16"
Elseif cParc == "H"
	nParc := "17"
Elseif cParc == "I"
	nParc := "18"
Elseif cParc == "J"
	nParc := "19"
Elseif cParc == "K"
	nParc := "20"
Elseif cParc == "L"
	nParc := "21"
Elseif cParc == "M"
	nParc := "22"
Elseif cParc == "N"
	nParc := "23"
Elseif cParc == "O"
	nParc := "24"
Endif

Return(nParc)
*/

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_InvFlag     � Autor �  Manoel          � Data   20/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca / Desmarca Vendedores Selecionados                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function FS_InvFlag()

Local iMarc := 0
if lMarc  == .T.
	for iMarc = 1 to Len(aVetVen)
		aVetVen[iMarc,1] := .f.
	Next
	lMarc := .f.
Else
	for iMarc = 1 to Len(aVetVen)
		aVetVen[iMarc,1] := .t.
	Next
	lMarc := .t.
Endif

oLbox:Refresh()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CaSecagem �Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Desc.     � Calcula o valor a ser cobrado pela secagem, considerando   ���
���          � apenas registro cujo campo LBT_DESCSEC <> N                ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CaSecagem(cCodcli,cLoja,dDtIni,dDtFim)

Local cQuery    := ""
Local aAreaAtu  := GetArea()                                             
Local aAreaLBT  := {}

nSecagem := 0

cQuery := "SELECT SUM(LBT_QTDREA *(LBT_SECAGE / SB1.B1_CONV)) SECAGE"
cQuery += " FROM "
cQuery += RetSqlName("LBT") + " LBT, "
cQuery += RetSqlName("SB1") + " SB1 "
cQuery += " WHERE"                  
cQuery += " LBT.LBT_FILIAL = '" + xFilial("LBT") + "' AND"
cQuery += " SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND"
cQuery += " LBT.LBT_PRODUT = SB1.B1_COD AND"
cQuery += " SB1.B1_CONV > 0 AND"
cQuery += " LBT.LBT_TIPOGR = '" + cTipoGr + "' AND"
cQuery += " LBT.LBT_FORNEC = '" + cCodCli + "' AND"
cQuery += " LBT.LBT_DATA BETWEEN '" + DtoS(dDtIni) + "' AND '" + DtoS(dDtFim)  + "' AND"
cQuery += " LBT.LBT_LOJA = '"+ cLoja +"' AND "
//cQuery += " LBT.LBT_DESCSE = 'N' AND"
cQuery += " LBT.D_E_L_E_T_ <> '*' AND "
cQuery += " SB1.D_E_L_E_T_ <> '*'"
cQuery := ChangeQuery(cQuery)

dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "LBTTMP", .F., .T.)
dbGoTop()

nSecagem := LBTTMP->SECAGE

dbSelectArea("LBTTMP")
dbCloseArea()

If lGerCrc .And. lSec .And. (nSecagem > 0)
	cQuery := "SELECT R_E_C_N_O_ AS NREC"
	cQuery += " FROM "+RetSqlName("LBT")+" LBT"
	cQuery += " WHERE LBT.LBT_FILIAL = '"+xFilial("LBT")+"' AND "
	cQuery += " LBT.LBT_TIPOGR = '" + cTipoGr + "' AND"
	cQuery += " LBT.LBT_FORNEC = '"+cCodCli+"' AND"
	cQuery += " LBT.LBT_DATA BETWEEN '" + DTOS(dDtIni) + "' AND '" + DTOS(dDtFim)  + "' AND"
	cQuery += " LBT.LBT_LOJA = '"+cLoja+"' AND "
	cQuery += " LBT_DESCSE = 'N' AND"
	cQuery += " LBT.D_E_L_E_T_ <> '*'"
	cQuery := ChangeQuery(cQuery)                                                        
	
	dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "LBTTMP", .F., .T.)	
    LBTTMP->(dbGoTop())
    	
	dbSelectArea("LBT")
	aAreaLBT := GetArea()
	While !LBTTMP->(Eof())
		dbGoTo(LBTTMP->NREC)
		RecLock("LBT",.F.)
		LBT_DESCSE := 'S'		
		MsUnLock()
		LBTTMP->(dbSkip())		
	End	
	// Restaura a area orginal do arquivo LBT
	RestArea(aAreaLBT)

	// Fecha a area utilizada pelo TOP
	dbSelectArea("LBTTMP")
	dbCloseArea()
	
	// Restaura a area original
	RestArea(aAreaAtu)
	
/*
	If TCSQLEXEC(cQuery) < 0
		ALERT("Ocorreu um erro ao Marcar Secagem no LBT, chame o Suporte")
	Endif
*/	
Endif

Return(nSecagem)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CaCalcTerc�Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Desc.     � Calculao Estoque de acordo com os parametros abaixo:       ���
���          � nSaldo := CaCalcTerc(aVetVen[iForn,2],aVetVen[iForn,4],dDatIni,dDataFim)  //CLIENTE, LOJA E PER�ODO ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CaCalcTerc(cCodcli,cLoja,dDtIni,dDtFim,cTipoGR)

Local cQuery   := ""
Local aSldTerc := {0,0}
Local aCrcMi   := {"S","S",dDtIni}

Default cTipoGR := ""

cQuery := "SELECT"
cQuery += " SB6.B6_CLIFOR, SB6.B6_LOJA, SUM(SB6.B6_QUANT) AS B6_QUANT, SF4.F4_PODER3, "
cQuery += " MIN(SB6.B6_CRCMI) AS B6_CRCMI, MAX(B6_DTCRCMI) AS B6_DTCRCMI, SB1.B1_COD, SB1.B1_GRUPO, SB1.B1_CONV"
cQuery += " FROM "
cQuery += RetSqlName("SB6") + " SB6, "
cQuery += RetSqlName("SF4") + " SF4, "
cQuery += RetSqlName("SB1") + " SB1 "
cQuery += " WHERE"
cQuery += " SF4.F4_FILIAL = '" + xFilial("SF4") + "' AND"
cQuery += " SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND"
If !Empty(cTipoGR)
	cQuery += " SB1.B1_GRUPO = '" + cTipoGr + "' AND"
EndIf
cQuery += " SB6.B6_FILIAL = '" + xFilial("SB6") + "' AND"
cQuery += " SB6.B6_PRODUTO = SB1.B1_COD AND"     
cQuery += " SB1.B1_CONV > 0 AND"     
cQuery += " SB6.B6_CLIFOR = '"+cCodCli+"' AND"
cQuery += " SB6.B6_LOJA = '" +cLoja+"' AND"
cQuery += " SB6.B6_TES     = SF4.F4_CODIGO AND"
cQuery += " SB6.B6_DTDIGIT BETWEEN '" + DTOS(dDtIni) + "' AND '" + DTOS(dDtFim)  + "' AND"
cQuery += " SB6.D_E_L_E_T_ <> '*' AND SF4.D_E_L_E_T_ <> '*' AND SB1.D_E_L_E_T_ <> '*'"
cQuery += " GROUP BY"
cQuery += " SB6.B6_CLIFOR,SB6.B6_LOJA,SB6.B6_PRODUTO,SF4.F4_PODER3,SB1.B1_COD,SB1.B1_GRUPO,SB1.B1_CONV"
cQuery := ChangeQuery(cQuery)

dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "TERC", .F., .T.)	

// Acerta o formato dos campos de acordo com o tipo e tamanho.
TCSetField("TERC", "B6_QUANT", "N", TamSx3("B6_QUANT")[1], TamSx3("B6_QUANT")[2])	
TCSetField("TERC", "B6_DTCRCMI", "D", 8, 0)	

TERC->(dbGoTop())
DO WHILE !EOF()
	IF TERC->F4_PODER3 = 'R'
		aSldTerc[1] += TERC->B6_QUANT
		aSldTerc[2] += TERC->B6_QUANT * (nTxArm / TERC->B1_CONV)
		aCrcMi[1]   := TERC->B6_CRCMI   //Ver se Registro est� marcado
		aCrcmi[3]   := TERC->B6_DTCRCMI //Data do ultimo fechamento
	ELSEIF TERC->F4_PODER3 = 'D'
		aSldTerc[1] -= TERC->B6_QUANT
		aSldTerc[2] -= TERC->B6_QUANT * (nTxArm / TERC->B1_CONV)
		aCrcMi[2]   := TERC->B6_CRCMI //Ver se Registro est� marcado
		aCrcmi[3]   := TERC->B6_DTCRCMI //Data do ultimo fechamento
	ENDIF
	DBSKIP()
ENDDO
        
dbSelectArea("TERC")
dbCloseArea()

Return(aSldTerc)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CaGrFinanc�Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Desc.     �Gera o Titulo Provisorio no Financeiro                      ���
�������������������������������������������������������������������������͹��
���Uso       �Cooperativa de Graos                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CaGrFinanc(cCodcli,cLoja,nValor,cSafra,cArmSec,cTipoGR)

Local aTitulo
Local lRet     := .T.
Local aAreaSX5 := {}
Local aAreaAtu := GetArea()
Local aAreaSE1 := {}
Local cNumTit  := ""

Private lMSHelpAuto := .t. // para mostrar os erro na tela
Private lMSErroAuto := .f. // inicializa como falso, se voltar verdadeiro e' que deu erro

cPrefixo  := T_fGetPref(cArmSec) // marcos dias 29/06/04
cNatureza := T_fGetNatu(cArmSec)

Iif(lNfRec==.F.,cTipo:="REC",cTipo:="PR")       // sempre "REC" marcos dias ?!?

dbSelectArea("SX5")
aAreaSX5 := GetArea()
dbSetOrder(1)
If !dbSeek(xFilial("SX5")+"05"+"REC")
	RecLock("SX5",.T.)   // Corrigido para .T. em 15/09/05, Ricardo Berti
	X5_FILIAL  := xFilial("SX5")
	X5_TABELA  := "05"
	X5_CHAVE   := "REC"
	X5_DESCRI  := "Recebimento"
	X5_DESCSPA := "Recebimento"
	X5_DESCENG := "Recebimento"
	MsUnLock()
EndIf
// Restaura a area original do arquivo SX5
RestArea(aAreaSX5)
		
// Prefixo de secagem (4)
If cArmSec == "SEC"
	cNumTit  := GetMv("MV_NUMSEC")
	cPrefixo := Iif(AllTrim(cPrefixo) $ "4.18.20",cPrefixo,"4")+Space(TamSx3("E1_PREFIXO")[01] - Len(cPrefixo))
Endif

// Prefixo de armazenagem (18)
If cArmSec == "ARM"
	cNumTit  := GetMv("MV_NUMARM")
	cPrefixo := Iif(AllTrim(cPrefixo) $ "4.18.20",cPrefixo,"18")+Space(TamSx3("E1_PREFIXO")[01] - Len(cPrefixo))
EndIf

// Prefixo de quebra (20)
If cArmSec == "QUE"
	cNumTit  := GetMv("MV_NUMQUE")
	cPrefixo := Iif(AllTrim(cPrefixo) $ "4.18.20",cPrefixo,"20")+Space(TamSx3("E1_PREFIXO")[01] - Len(cPrefixo))
EndIf

dbselectArea("SE1")
aAreaSE1 := GetArea()
dbSetOrder(1)
While .T.
	If !dbSeek(xFilial("SE1")+cPrefixo+cNumTit)
		Exit
	EndIf
	cNumTit := Soma1(cNumTit,TamSX3("E1_NUM")[01])
End                  
// Restaura a area do arquivo SE1
RestArea(aAreaSE1)

// Restaura a area original
RestArea(aAreaAtu)

//Fim da formula para gerar o n�mero do titulo
BEGIN Transaction
aTitulo :=	{	{"E1_PREFIXO"	,cPrefixo  ,Nil},;
				{"E1_NUM"		,cNumTit   ,Nil},;
				{"E1_PARCELA"	," "       ,Nil},;
				{"E1_TIPO"		,cTipo     ,Nil},;
				{"E1_NATUREZ"	,cNatureza ,Nil},;
				{"E1_CLIENTE"	,cCodcli   ,Nil},;
				{"E1_LOJA"		,cLoja     ,Nil},;
				{"E1_EMISSAO"	,dDatFim   ,Nil},;
				{"E1_VENCTO"	,dDatFim   ,Nil},;
				{"E1_VENCREA"	,DataValida(dDatFim,.T.),Nil},;  // Inserido .T. (AVANCA data) 15/09/05 Ricardo Berti
				{"E1_VALOR"		,NoRound(nValor,2)    ,Nil},; // marcos dias
				{"E1_PEDIDO"	,cSafra    ,Nil},;
				{"E1_HIST"		,"ESPECIE:"+cTipoGR,Nil}}

lMSHelpAuto := .T. // para mostrar os erro na tela
lMSErroAuto := .F. // inicializa como falso, se voltar verdadeiro e' que deu erro

MSExecAuto({|x,y| FINA040(x,y)},aTitulo,3)

If lMsErroAuto
	MostraErro()
	DisarmTransaction()
	lRet := .F.
Else
	If cArmSec == "ARM"
		PutMV("MV_NUMARM",cNumTit)	
	ElseIf cArmSec == "SEC"
		PutMV("MV_NUMSEC",cNumTit)	 
	ElseIf cArmSec == "QUE"
		PutMV("MV_NUMQUE",cNumTit)	 
	EndIf
Endif
END Transaction

Return(lRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CaImprime �Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Descricao �Imprime os dados do list box                                ���
�������������������������������������������������������������������������͹��
���Uso       �Cooperativa de Graos                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CaImprime

aOrd           := {}
cDesc1         := "Este programa tem como objetivo imprimir o relatorio de "
cDesc2         := "Graos: Armazenamento e Secagem"
cDesc3         := "de acordo com os parametros informados pelo usuario."
cPict          := ""
lEnd           := .F.
lAbortPrint    := .F.
nTipo          := 15  //CONDENSADO
limite         := 132
tamanho        := "M"
nomeprog       := "CACMIA04"
aReturn        := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
nLastKey       := 0
Titulo         := "Graos: Armazenamento e Secagem"
nLin           := 80
cbtxt          := Space(10)
cbcont         := 00
CONTFL         := 01
m_pag          := 01
imprime        := .T.
cPerg          := Nil // "CAMA04"
wnrel          := Cperg  // Coloque aqui o nome do arquivo usado para impressao em disco
Cabec1         := ""
Cabec2         := ""

cString        := "LBT"

dbSelectArea(cString)
dbSetOrder(1)

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho,,.F.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

Processa({|| RptProc("Aguarde, Selecionando Dados...")})

return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RptProc   �Autor  �                    � Data �             ���
�������������������������������������������������������������������������͹��
���Desc.     �apenas para usar barra de progress�o na rotina de impress�o ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function RptProc()

Local iForn := 0
cabec1 := "                                      Periodo de "+DTOC(dDatIni)+" ate "+DTOC(dDatFim)
cabec2 := "Cod     Nome                                  Propriedade                                 Qtd   Armazenagem      Secagem      Quebra"

nTqtd:=nTArm:=nTSec:=nTQue:=0

For iForn := 1 to Len(aVetVen)
	
	if nLin > 60
		Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
		nLin := 9
	endif
	
	@ nLin,00  PSAY aVetVen[iForn,2]
	@ nLin,08  PSAY Substr(aVetVen[iForn,3],1,35)
	@ nLin,45  PSAY SubStr(aVetVen[iForn,5],1,35)
	@ nLin,82  PSAY TRANSFORM(aVetVen[iForn,6],"@E 999,999,999")
	@ nLin,97  PSAY TRANSFORM(aVetVen[iForn,7],"@E 999,999.99")
	@ nLin,110 PSAY TRANSFORM(aVetVen[iForn,8],"@E 999,999.99")
	@ nLin,122 PSAY TRANSFORM(aVetVen[iForn,9],"@E 999,999.99")
	
	nTQtd += aVetVen[iForn,6]
	nTArm += aVetVen[iForn,7]
	nTSec += aVetVen[iForn,8]
	nTQue += aVetVen[iForn,9]
	
	nLin++
	
Next

nLin++   ;  @ nLin,00 PSAY REPLICATE("-",132)  ;   nLin++
@ nLin,82  PSAY TRANSFORM(nTQtd,"@E 999,999,999")
@ nLin,94  PSAY TRANSFORM(nTArm,"@E 99,999,999.99")
@ nLin,108 PSAY TRANSFORM(nTSec,"@E 9,999,999.99")
@ nLin,122 PSAY TRANSFORM(nTQue,"@E 999,999.99")

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	dbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

Return


Static Function detalhes()

nOpca2 := 0
DEFINE MSDIALOG ODLG4 TITLE "Detalhes de " + aVetVen[oLbox:nAt,3]+"Lj:"+aVetVen[oLbox:nAt,2] From 9,10 to 20,60 of oMainWnd
@ 015,053 SAY OemToAnsi("Total") of ODLG4 PIXEL COLOR CLR_BLUE
@ 015,093 SAY OemToAnsi("-     Financeiro") of ODLG4 PIXEL COLOR CLR_BLUE
@ 015,143 SAY OemToAnsi("=     Liquido") of ODLG4 PIXEL COLOR CLR_BLUE

@ 030,004 SAY OemToAnsi("Armazenagem") of ODLG4 PIXEL COLOR CLR_BLUE
@ 030,050 MSGET obj1 VAR aVetVen[oLbox:nAt,12] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 030,100 MSGET obj2 VAR aVetVen[oLbox:nAt,13] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 030,150 MSGET obj3 VAR aVetVen[oLbox:nAt,07] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.

@ 045,004 SAY OemToAnsi("Secagem") of ODLG4 PIXEL COLOR CLR_BLUE
@ 045,050 MSGET obj1 VAR aVetVen[oLbox:nAt,14] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 045,100 MSGET obj2 VAR aVetVen[oLbox:nAt,15] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 045,150 MSGET obj3 VAR aVetVen[oLbox:nAt,08] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.

@ 060,004 SAY OemToAnsi("Quebra") of ODLG4 PIXEL COLOR CLR_BLUE
@ 060,050 MSGET obj1 VAR aVetVen[oLbox:nAt,10] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 060,100 MSGET obj2 VAR aVetVen[oLbox:nAt,11] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.
@ 060,150 MSGET obj3 VAR aVetVen[oLbox:nAt,09] PICTURE "@E 99,999,999.99" SIZE 40,4 OF oDlg4 PIXEL COLOR CLR_BLACK  when .f.


ACTIVATE MSDIALOG ODLG4 ON INIT EnchoiceBar(oDlg4,{||nOpca2:= 1, oDlg4:End()} , {|| nOpca2 := 2, oDlg4:End()})  CENTER

//01-marca,02-cod,03-nome,04-loja,05-end,06-qtde,07-armaz,08-seca,09-quebra,10-Total Quebra,11-Finan.Quebra
//12-Total Arm, 13-Financ.Arm, 14-Total Sec, 15-Financ.Sec

Return

Template Function fGetPref(cTipo) // marcos dias 29/06/04

Local cRet := "00"
Local cTmp
Local i    := 0

cTmp := GetMv("MV_TIPOPRE",,"SEC=4,ARM=18,QUE=20")

For i:=1 to Len(cTmp)
   if Substr(cTmp,i,3) == cTipo
      cTmp := Substr(cTmp,i+4)
      if at(',',cTmp) > 0
         nPos := at(',',cTmp)-1
      Else
         nPos := Len(cTmp)
      Endif   
      cRet := Substr(cTmp,1,nPos)
   Endif
Next

Return(cRet)

Template Function fGetNatu(cTipo) // marcos dias 29/06/04

Local cRet := "000000"
Local cTmp
Local i    := 0

cTmp := GetMv("MV_TIPONAT")

For i:=1 to Len(cTmp)
   if Substr(cTmp,i,3) == cTipo
      cTmp := Substr(cTmp,i+4)
      if at(',',cTmp) > 0
         nPos := at(',',cTmp)-1
      Else
         nPos := Len(cTmp)
      Endif   
      cRet := Substr(cTmp,1,nPos)
   Endif
Next

Return(cRet)

///////////////////////////

Static Function f_ValGrao(cTipoGr)

dbSelectArea("SBM")
dbSetOrder(1)
if dbSeek(xFilial("SBM")+cTipoGr)
   cDescGr := SBM->BM_DESC
   oDescGr:Refresh()
Else
   Return(.F.)
Endif

Return(.T.)

/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Funcao    �CaPesqSafra � Autor �Julio Cesar            � Data � 21/09/04 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Determina se existe safra, de acordo com os parametros       ���
���          � informados.                                                  ���
���������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                         ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/
Template Function CaPesqSafra(dDtaIni,dDtaFim,cTipoGR)

Local cQuery	:= ""
Local cQueryPE	:= ""
Local lRet		:= .T.
Local aRet		:= {}

Default cTipoGR := ""

cQuery := "SELECT"
cQuery += " LBU.LBU_CARENC, LBU.LBU_VLARM, LBU.LBU_CODIGO, LBU.LBU_QUEBRA, LBU.LBU_TIPOGR"
cQuery += " FROM"
cQuery += " " + RetSqlName("LBU") + " LBU"
cQuery += " WHERE"
cQuery += " LBU.LBU_FILIAL = '" + xFilial("LBU") + "' AND"       
If !Empty(cTipoGR)
	cQuery += " LBU.LBU_TIPOGR = '" + cTipoGr + "' AND"
EndIf
cQuery += " LBU.LBU_INICIO BETWEEN '" + DtoS(dDtaIni) + "' AND '" + DtoS(dDtaFim)  + "' AND"
cQuery += " LBU.D_E_L_E_T_ <> '*'"
//����������������������������������������������������������������������Ŀ
//� CAC04QRY - Ponto de Entrada p/ manipulacao da Query - filtro em LBU  �
//������������������������������������������������������������������������
If ExistBlock("CAC04QRY")
	cQueryPE := Execblock('CAC04QRY', .F., .F., {cQuery})
	cQuery   := If(ValType(cQueryPE)=='C', cQueryPE, cQuery)
Endif
cQuery := ChangeQuery(cQuery)

dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "SAFRA", .F., .T.)

If Eof()
	lRet := .F.
	dbSelectArea("SAFRA")
	dbCloseArea()
EndIf
                  
AAdd(aRet, lRet)
AAdd(aRet, "SAFRA")

Return(aRet)
