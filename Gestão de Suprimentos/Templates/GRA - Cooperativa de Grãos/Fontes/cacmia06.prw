#include "Protheus.ch"
#include "TopConn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA06 � Autor �MARCO AURELIO (TRT005) � Data � 25/09/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Levantamento dos Titulos no CR ref. Secagem de Graos       ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACMIA06()
LOCAL aArea := GetArea()
LOCAL cQuery
LOCAL i         := 0

PRIVATE oTik    := LoaDbitmap( GetResources(), "LBTIK" )
PRIVATE oNo     := LoaDbitmap( GetResources(), "LBNO" )
PRIVATE aVetVen := {}
Private nTotTIt := 0
Private nTotTitTemp := 0
Private cPesquisa
CHKTEMPLATE("COL")

If alltrim(M->C5_CLIENTE) == ''  .or. alltrim(aCols[n][2]) == ''
	alert('Esta Rotina devera ser executada apos a digitacao do cliente, loja e item de pedido')
	return
Endif


//LEVANTAMENTO DOS TITULOS REFERENTE AO GRAOS PARA O FORNECEDOR ESCOLHIDO
cQuery := "SELECT "
cQuery += " A1_NOME, E1_EMISSAO, E1_VENCREA, E1_VALOR, E1_SALDO, E1_NUM, E1_TIPO, E1_PREFIXO"
cQuery += " FROM "
cQuery += RetSqlName("SE1") + " SE1,"
cQuery += RetSqlName("SA1") + " SA1"
cQuery += " WHERE"
cQuery += " SE1.E1_FILIAL = '" + XFILIAL("SE1") + "' "
cQuery += " AND SA1.A1_FILIAL = '" + XFILIAL("SA1") + "'"
cQuery += " AND SE1.D_E_L_E_T_ <> '*' "
cQuery += " AND SA1.D_E_L_E_T_ <> '*' "
cQuery += " AND E1_PREFIXO = 'MI' AND E1_TIPO = 'PR' AND E1_NATUREZ = 'ARMMILHO'"
cQuery += " AND A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA"
cQuery += " AND E1_SALDO > 0 AND E1_CLIENTE = '" + M->C5_CLIENTE + "'  AND E1_LOJA = '" + M->C5_LOJAENT +"' "
TCQUERY cQuery ALIAS RQRY NEW
DbGoTop()

If EOF()
	ALERT("Nenhuma entrada necessitando de nota para esta propriedade!")
	RQRY->(DbCloseArea())
	RETURN
Endif

while !eof()
	// A1_NOME, E1_EMISSAO,E1_VENCREA, E1_VALOR, E1_SALDO, E1_NUM,E1_TIPO,E1_PREFIXO,
	
	cEmissao  := SUBSTR(E1_EMISSAO,7,2)+"/"+SUBSTR(E1_EMISSAO,5,2)+"/"+SUBSTR(E1_EMISSAO,1,4)
	cVencReal := SUBSTR(E1_VENCREA,7,2)+"/"+SUBSTR(E1_VENCREA,5,2)+"/"+SUBSTR(E1_VENCREA,1,4)
	cValor    := TransForm(E1_VALOR, "@E 999,999,999.99")
	
	nTotTit += E1_SALDO  //TOTAL A SER GRAVADO NA NOTA
	
	aadd(aVetVen,{.t.,RQRY->A1_NOME,cEmissao,cVencReal,cValor,E1_SALDO,RQRY->E1_NUM,RQRY->E1_TIPO,RQRY->E1_PREFIXO})
	
	DbSkip()
Enddo

lMarc   := .t. // Controla Tela de Fornecedores (Marca ou Desmarca Todos)
nPerFin := 0
cTabPre := Space(3)

nOpca   := 0

DEFINE MSDIALOG oDlg TITLE OemToAnsi("") From 9,1 to 28,83 of oMainWnd

//aadd(aVetVen,{.t.,RQRY->A1_NOME,cEmissao,cVencReal,cValor,cSaldo,RQRY->E1_NUM,RQRY->E1_TIPO,RQRY->E1_PREFIXO})
@ 13,002 LISTBOX oLbox FIELDS HEADER ;
OemToAnsi("M"),;
OemToAnsi("Fornecedor"),;
OemToAnsi("Emissao"),;
OemToAnsi("Vencimento"),;
OemToAnsi("Valor"),;
OemToAnsi("Saldo"),;
OemToAnsi("Num Titulo");
COLSIZES 5,50,30,30,30,30,30;
SIZE 325,100 OF oDlg PIXEL ON DBLCLICK (nPos:=oLbox:nAt,FS_Marc(),oLbox:Refresh(),oLbox:nAt:=nPos) Centered
oLbox:SetArray(aVetVen)
oLbox:bLine := { || { If(aVetVen[oLbox:nAt,01] == .f.,oNo,oTik),;
aVetVen[oLbox:nAt,02],;
aVetVen[oLbox:nAt,03],;
aVetVen[oLbox:nAt,04],;
aVetVen[oLbox:nAt,05],;
TransForm(aVetVen[oLbox:nAt,06],"@E 999,999.99"),;
aVetVen[oLbox:nAt,07] }}

@ 127,002 BUTTON oBtn PROMPT "Marca/Desmarca Todos" SIZE 60,10 OF oDlg PIXEL ACTION FS_InvFlag() //EXTRAIDO DO PROGRAMA  Cfgx047()

@ 127,200 Say OemToAnsi("Saldo Total") of Odlg Pixel
@ 127,250 MSGET oTotIt VAR nTotTit  PICTURE "@E 999,999,999.99"  SIZE 43,4 OF oDlg PIXEL COLOR CLR_BLACK  when .f.

ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg,{||nOpca := 1,oDlg:End()},{||nOpca := 0,oDlg:End() })

If nopca == 1
	ntottit:=0
	For i = 1 to len(avetven)  //atualizando a soma das quantidades
		If avetven[i,1] == .t.
			ntottit += avetven[i,6]
			DbSelectArea("SE1")
			Dbsetorder(2)
			//		se1    2     e1_filial+e1_cliente+e1_loja+e1_prefixo+e1_num+e1_parcela+e1_tipo
			//aadd(avetven,{.t.,rqry->a1_nome,cemissao,cvencreal,cvalor,csaldo,rqry->e1_num,rqry->e1_tipo,rqry->e1_prefixo})
			cPesquisa := xfilial("SE1")+m->c5_cliente+m->c5_lojaent+avetven[i,9]+avetven[i,7]+" "+avetven[i,8]
			If DbSeek(cPesquisa)
				reclock("SE1",.f.)
				se1->e1_pedido := m->c5_num
				msunlock()
			Endif
		Endif
	Next
	
	acols[n][5] := ntottit   //valor total
Endif

dbSelectArea("RQRY")
RQRY->(DbCloseArea())

RestArea(aArea)
return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_Marc        � Autor �  Manoel          � Data   26/06/00 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca no ListBox do Filtro                                 ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_Marc()
LOCAL i
If aVetVen[oLbox:nAt,1] == .f.
	aVetVen[oLbox:nAt,1] := .t.
Else
	aVetVen[oLbox:nAt,1] := .f.
Endif

nTotTit	:= 0
For i:=1 to len(aVetVen)  //atualizando a soma das quantidades
	If aVetVen[i,1] == .t.
		nTotTit += aVetVen[i,6]
	Endif
Next

oTotIt:Refresh()
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_InvFlag     � Autor �  Manoel          � Data   20/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca / Desmarca Vendedores Selecionados                   ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_InvFlag()
LOCAL i
If lMarc
	For i:=1 to Len(aVetVen)
		aVetVen[i,1] := .f.
	Next
	lMarc := .f.
Else
	For i:=1 to Len(aVetVen)
		aVetVen[i,1] := .t.
	Next
	lMarc := .t.
Endif

nTotTit := 0
For i:= 1 to len(aVetVen)  //atualizando a soma das quantidades
	If aVetVen[i,1] == .t.
		nTotTit += aVetVen[i,6]
	Endif
Next

oTotIt:Refresh()
oLbox:Refresh()

Return
