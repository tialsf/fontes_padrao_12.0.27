#include "Protheus.ch"
#include "TopConn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA07 � Autor �MARCO AURELIO (TRT005) � Data � 25/09/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Levantamento do Titulos a Receber                          ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACMIA07()
LOCAL cQuery
PRIVATE cValor 	 := 0
PRIVATE nTotTit	 := 0
PRIVATE nTotBx	 := 0
PRIVATE aVetVen	 := {}
PRIVATE cContador:= "000001"
PRIVATE cPerg	 := "CAMA07"
PRIVATE oTik 	 := LoadBitmap( GetResources(), "LBTIK" )
PRIVATE oNo  	 := LoadBitmap( GetResources(), "LBNO" )
CHKTEMPLATE("COL")

AjustaSX1(cPerg)
if !Pergunte(cPerg,.t.)
	Return
Endif

cQuery := "SELECT "
cQuery += " A1_COD, A1_NOME, A1_LOJA, A1_END, E1_EMISSAO, E1_VENCREA, E1_VALOR, E1_SALDO, E1_NUM, E1_TIPO, E1_PREFIXO, E1_NATUREZ"
cQuery += " FROM "
cQuery += RetSqlName("SE1") + " SE1,"
cQuery += RetSqlName("SA1") + " SA1"
cQuery += " WHERE"
cQuery += " SE1.E1_FILIAL = '" + XFILIAL("SE1") + "' "
cQuery += " AND SA1.A1_FILIAL = '" + XFILIAL("SA1") + "'"
cQuery += " AND SE1.D_E_L_E_T_ <> '*' "
cQuery += " AND SA1.D_E_L_E_T_ <> '*' "
cQuery += " AND E1_TIPO = 'REC' AND E1_PREFIXO IN ('4','18')"
cQuery += " AND A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA"
cQuery += " AND E1_SALDO <> E1_VALOR AND E1_CLIENTE = '" + MV_PAR01 + "'  AND E1_LOJA = '" + MV_PAR02 +"' "
cQuery += " ORDER BY"
cQuery += " A1_NOME,A1_LOJA,E1_EMISSAO"
TCQUERY cQuery ALIAS RQRY NEW

// cQuery += " AND E1_SALDO > 0 AND E1_CLIENTE = '" + MV_PAR01 + "'  AND E1_LOJA = '" + MV_PAR02 +"' " // mad - 29/07/04

DbGoTop()
IF EOF()
	// ALERT("Este Fornecedor n�o tem titulos em aberto no Contas a Receber!")
	ALERT("Este Fornecedor n�o tem titulos PAGOS (baixados) no Contas a Receber!")
	DBSELECTAREA("RQRY")
	dbCloseArea()
	RETURN
ENDIF

while !eof()
	cEmissao  := SUBSTR(E1_EMISSAO,7,2)+"/"+SUBSTR(E1_EMISSAO,5,2)+"/"+SUBSTR(E1_EMISSAO,1,4)
	cVencReal := SUBSTR(E1_VENCREA,7,2)+"/"+SUBSTR(E1_VENCREA,5,2)+"/"+SUBSTR(E1_VENCREA,1,4)
	
	// nTotTit += E1_SALDO  //TOTAL A SER GRAVADO NA NOTA
	nTotTit += E1_VALOR - E1_SALDO // MAD - 29/07/04
	
	aadd(aVetVen,{.t.,RQRY->A1_NOME,RQRY->A1_LOJA,cEmissao,cVencReal,RQRY->E1_VALOR,RQRY->E1_SALDO,RQRY->E1_NUM,RQRY->E1_TIPO,RQRY->E1_PREFIXO,RQRY->E1_NATUREZ,RQRY->A1_END,RQRY->A1_COD})
	
	DbSkip()
Enddo

DBSELECTAREA("RQRY")
dbCloseArea()

lMarc   	:= .t. // Controla Tela de Fornecedores (Marca ou Desmarca Todos)
lWhenF 	:= lWhenD := .t.
lBxTit 	:= .f.
nOpca 	:= 0
nTotBx 	:= nTotTit

DEFINE MSDIALOG oDlg TITLE OemToAnsi("") From 9,1 to 037,94 of oMainWnd
@ 13,002 LISTBOX oLbox FIELDS HEADER ;
OemToAnsi("M"),;
OemToAnsi("Fornecedor"),;
OemToAnsi("Loja"),;
OemToAnsi("Emissao"),;
OemToAnsi("Vencimento"),;
OemToAnsi("Valor"),;
OemToAnsi("Saldo"),;
OemToAnsi("Num Titulo"),;
OemToAnsi("Tipo"),;
OemToAnsi("Prefixo"),;
OemToAnsi("Natureza");
COLSIZES 5,100,20,20,20,20,20,20,20,20,20;
SIZE 366,150 OF oDlg PIXEL ON DBLCLICK (nPos:=oLbox:nAt,FS_Marc(),oLbox:Refresh(),oLbox:nAt:=nPos) Centered
oLbox:SetArray(aVetVen)
oLbox:bLine := { || { If(aVetVen[oLbox:nAt,01] == .f.,oNo,oTik),;
aVetVen[oLbox:nAt,02],;
aVetVen[oLbox:nAt,03],;
aVetVen[oLbox:nAt,04],;
aVetVen[oLbox:nAt,05],;
TransForm(aVetVen[oLbox:nAt,06],"@E 999,999.99"),;
TransForm(aVetVen[oLbox:nAt,07],"@E 999,999.99"),;
aVetVen[oLbox:nAt,08],;
aVetVen[oLbox:nAt,09],;
aVetVen[oLbox:nAt,10],;
aVetVen[oLbox:nAt,11] }}

@ 165,005 BUTTON oBtnMD PROMPT "Marca/Desmarca Todos" SIZE 60,10 OF oDlg PIXEL ACTION FS_InvFlag()
@ 175,005 BUTTON oBtnImp PROMPT "Imprimir Recibo" SIZE 60,10 OF oDlg PIXEL ACTION CaImprime()
// @ 172,130 CHECKBOX oBxTit VAR lBxTit PROMPT oemtoansi("Baixa Titulos ?") SIZE 53, 10
@ 172,220 Say OemToAnsi("Saldo Total") of Odlg Pixel
@ 170,250 MSGET oTotIt VAR nTotTit  PICTURE "@E 999,999,999.99"  SIZE 43,4 OF oDlg PIXEL COLOR CLR_BLACK  when .f.
@ 182,220 Say OemToAnsi("Valor Pago") of Odlg Pixel
@ 180,250 MSGET oTotBx VAR nTotBx  PICTURE "@E 999,999,999.99"  SIZE 43,4 OF oDlg PIXEL COLOR CLR_BLACK  when .T.

ACTIVATE MSDIALOG oDlg CENTERED ON INIT EnchoiceBar(oDlg,{||nOpca := 0,oDlg:End() },{||nOpca := 0,oDlg:End() })
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_Marc        � Autor �  Manoel          � Data   26/06/00 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca no ListBox do Filtro                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_Marc()

LOCAL i	:= 0

If aVetVen[oLbox:nAt,1] == .f.
	aVetVen[oLbox:nAt,1] := .t.
Else
	aVetVen[oLbox:nAt,1] := .f.
Endif

nTotTit:=0
For i:=1 to len(aVetVen)  //atualizando a soma das quantidades
	If aVetVen[i,1] == .t.
		nTotTit += aVetVen[i,7]
	Endif
Next

nTotBx := nTotTit

oTotIt:Refresh()
oTotBx:Refresh()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �FS_InvFlag     � Autor �  Manoel          � Data   20/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Marca / Desmarca Vendedores Selecionados                   ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function FS_InvFlag()
LOCAL iMarc, i
if lMarc
	for iMarc:=1 to Len(aVetVen)
		aVetVen[iMarc,1] := .f.
	Next
	lMarc := .f.
Else
	for iMarc:=1 to Len(aVetVen)
		aVetVen[iMarc,1] := .t.
	Next
	lMarc := .t.
Endif

nTotTit	:= 0
For i:=1 to len(aVetVen)  //atualizando a soma das quantidades
	If aVetVen[i,1] == .t.
		nTotTit += aVetVen[i,7]
	Endif
Next

nTotBx := nTotTit
oLbox:Refresh()
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CaImprime     � Autor �  Manoel          � Data   20/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprime os dados do list box                               ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function CaImprime
Local aTitulo := {}
Local lMsErroAuto := .F.
LOCAL i
Private nVlBx := nTotBx

If lBxTit == .T. .and. nVlBx > 0
	For i:=1 to len(avetven)
		If avetven[i,1] == .t. .and. nVlBx > 0
			
			if nVlBx>=aVetVen[i,7]
				nAutValRec := 0
				nVlBx -= aVetVen[i,7]
				aVetVen[i,7]	:= 0
			else
				nAutValRec := nVlBx
				aVetVen[i,7]	-= nVlBx
				nVlBx := 0
			endif
			
			oLbox:Refresh()

			/*
			aTitulo := {{"E1_PREFIXO"	  ,avetven[i,10]			,Nil},;
			{"E1_NUM"		    ,avetven[i,8]				,Nil},;
			{"E1_PARCELA"	  ," "              	,Nil},;
			{"E1_TIPO"	    ,avetven[i,9]     	,Nil},;
			{"AUTBANCO"    ,"CXG"              ,Nil},;
			{"AUTAGENCIA"	  ,"GERAL"          	,Nil},;
			{"AUTCONTA"	    ,"G0002"          	,Nil},;
			{"AUTMOTBX"	    ,"NOR"            	,Nil},;
			{"AUTDTBAIXA"	  ,dDataBase        	,Nil},;
			{"AUTDTCREDITO" ,dDataBase        	,Nil},;
			{"AUTHIST"	    ,'Baixa Automatica'	,Nil},;
			{"AUTVALREC"	  ,nAutValRec					,Nil }}

			MSExecAuto({|x,y| fina070(x,y)},aTitulo,3) //Inclusao
			
			If lMsErroAuto
				Alert("Erro ao Baixar Titulo " + aVetVen[i,8])
			Else
				Alert("Baixa do Titulo " + aVetVen[i,8] + " Realizada com �xito")
			Endif
			*/
			
		Endif
	Next
Endif

cMensE 	:= ""
cMensArm := ""
cMensSec := ""
For i:=1 to len(avetven)
	If avetven[i,1] == .T.
		DO CASE
			CASE aVetVen[i,10] = "18" ; cMensArm := " ARMAZENAMENTO "
			CASE aVetVen[i,10] = "4"  ; cMensSec := " SECAGEM "
		ENDCASE
	Endif
Next
cMensE := if(cMensArm <> "" .and. cMensSec <> ""," e ","")

aOrd := {}
cDesc1         := "Este programa tem como objetivo imprimir "
cDesc3         := "Recibo - Graos - Armazenamento ou Secagem"
cDesc2         := "de acordo com os parametros informados pelo usuario."
cPict          := ""
lEnd         := .F.
lAbortPrint  := .F.
nTipo     := 18  //Normal
limite    := 80
tamanho   := "P"
nomeprog         := "CACMIA07"
aReturn          := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
nLastKey        := 0
Titulo       := "R  E  C  I  B  O"
nLin         := 80
cbtxt      := Space(10)
cbcont     := 00
CONTFL     := 01
m_pag      := 01
imprime      := .T.
cPerg       := "CAMA07"
wnrel      := Cperg  // Coloque aqui o nome do arquivo usado para impressao em disco
Cabec1       := ""
Cabec2       := ""

cString := "LBT"

dbSelectArea(cString)
dbSetOrder(1)

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho,,.F.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

Processa({|| RptProc("Aguarde, Selecionando Dados...")})
return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � RptProc       � Autor �  Manoel          � Data   20/12/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � apenas para usar barra de progress�o na rotina de impress�o���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function RptProc()

Local nCtd	:= 1

If GetMv("MV_MILHORC", .T.) //ATUALIZA O PARAMETRO PARA NUMERA��O DO RECIBO
	cContador := STRZERO(val(GETMV("MV_MILHORC"))+1,6)
	RecLock("SX6",.F.)
	SX6->X6_CONTEUD := cContador
ELSE
	RecLock("SX6", .T.)
	SX6->X6_FIL      := "  "
	SX6->X6_VAR      := "MV_MILHORC"
	SX6->X6_TIPO     := "C"
	SX6->X6_DESCRIC  := "Numeracao automatica para Recibo de Grao"
	SX6->X6_DESC1    := "Este e o numero do ultimo recibo impresso"
	SX6->X6_SIGACOM  := "X"
	SX6->X6_SIGAEST  := "X"
	SX6->X6_CONTEUD  := "000001"
EndIf

MSUNLOCK()

cValor := Transform(ntotBx,"@e 999,999.99")
nValor := nTotBx
cabec1 := PADC(SM0->M0_NOMECOM,80)
cabec2 := PADC("DEPARTAMENTO DE GRAOS",80)

For nCtd := 1 to 2
	nlin := Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)+1
/*	
	@ nLin  ,00 PSAY " Reg no DCOR sob 8.325 - Reg no DAC sob No. 1.497  -  Inc  Est  698.00.695.112"
	@ nLin+1,00 PSAY "        C.G.C 72.698.509/0001-60    -   Rua Arcebispo Lemieux, 1310/1444"
	@ nLin+2,00 PSAY "   Fone (18) 5851-1511   Fax 5851-1440 - Cx Postal 091 - Tupi Paulista - SP"
	@ nLin+3,00 PSAY " Lat: Av. 9 de julho, 1436 - F 5851-2110 - CEP 17.930-000 - Tupi Paulista - SP"
*/
	@ nLin+4,00 PSAY REPLICATE("_",132)
	
	@ nLin+9,37 PSAY "No. " + cContador + space(22) + cValor
	
	@ nLin+13,00 PSAY "Recebemos do Sr. " + AVetVen[1,2]
	_extval:=extenso(nValor)+REPL("*",250)
	@ nLin+16,00  PSAY "a quantia supra de " + subs(_extval,1,61)
	@ nLin+17,00  PSAY subs(_extval,62,80)
	@ nLin+20,00 PSAY "Referente a " + cMensArm + cMensE + cMensSec + " de Graos"
	
	@ nLin+27,25 PSAY Ltrim(str(day(ddatabase))) + " de "+ Ltrim(MesExtenso(month(ddatabase))) + " de " + Ltrim(str(Year(ddatabase)))
	
	@ nLin+34,00 PSAY "                        ______________________________________________________"
	@ nLin+35,00 PSAY "                                          Assinatura do Caixa"
	@ nLin+50,00 PSAY REPLICATE("_",80)
	@ nLin+51,00 PSAY "       Isento de Imposto Federal de Selo Art 40 Dec. no 22.239 de 19/12/32"
	@ nLin+52,00 PSAY "       Revigorado pelos Dec Lei no 681 de 01/08/38 o no. 8.401 de 19/12/46"
	
Next

SET DEVICE TO SCREEN

If aReturn[5]==1
	dbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif
MS_FLUSH()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �AjustaSX1  �Autor  �Ricardo Berti         � Data �24/10/2007���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Cria o grupo de perguntas utilizado pela rotina 			  ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � CACMIA07                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function AjustaSX1(cPerg)

Local aArea     := GetArea()
Local aHelpP01	:= {}
Local aHelpE01	:= {}
Local aHelpS01	:= {}
Local aHelpP02	:= {}
Local aHelpE02	:= {}
Local aHelpS02	:= {}
Local nTamSX1   := Len(SX1->X1_GRUPO)

dbSelectArea("SX1")
dbSetOrder(1)
If MsSeek(PADR(cPerg,nTamSX1)+"01") .And. Empty(X1_PERSPA)
	While !Eof() .And. Trim(X1_GRUPO) == cPerg
		RecLock("SX1",.F.)
		dbDelete()
		MsUnLock()
		dbSkip()
	EndDo		
EndIf

Aadd( aHelpP01, "Informe o Fornecedor a ser considerado  " )
Aadd( aHelpP01, "na selecao dos dados.                   " )
Aadd( aHelpP01, "                                        " )

Aadd( aHelpE01, "Enter supplier to be considered in the  " )
Aadd( aHelpE01, "selection of data.                      " )
Aadd( aHelpE01, "                                        " )

Aadd( aHelpS01, "Informe el proveedor que se considerar� " )
Aadd( aHelpS01, "en la selecci�n de datos.               " )
Aadd( aHelpS01, "                                        " )

Aadd( aHelpP02, "Informe a Loja a ser considerada na     " )
Aadd( aHelpP02, "selecao dos dados.                      " )
Aadd( aHelpP02, "                                        " )

Aadd( aHelpE02, "Enter unit to be considered in the      " )
Aadd( aHelpE02, "selection of data.                      " )
Aadd( aHelpE02, "                                        " )

Aadd( aHelpS02, "Informe la tienda que se considerar� en " )
Aadd( aHelpS02, "la selecci�n de datos.                  " )
Aadd( aHelpS02, "                                        " )

PutSx1(	cPerg,"01","Fornecedor ?","�Proveedor ?","Supplier ?","mv_ch1",;
		"C",TamSx3("A2_COD")[1],0,0,"G","","FOR","","","mv_par01","","",""," ","","","","","","","","","",;
		"","","",{},{},{})

PutSx1(	cPerg,"02","Loja ?","�Tienda ?","Unit ?","mv_ch2",;
		"C",TamSx3("A2_LOJA")[1],0,0,"G","","","","","mv_par02","","",""," ","","","","","","","","","",;
		"","","",{},{},{})

PutSX1Help("P."+cPerg+"01.",aHelpP01,aHelpE01,aHelpS01)
PutSX1Help("P."+cPerg+"02.",aHelpP02,aHelpE02,aHelpS02)

RestArea(aArea)
Return