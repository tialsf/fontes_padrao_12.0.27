#include "protheus.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA08 � Autor �  Choite               � Data � 17/02/03 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Tabela de preco de Graos                                   ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACMIA08
Private cCadastro := "Tabela de Preco de Graos"
Private cDelFunc := ".T.
Private cString  := "LEX"
Private aRotina := { {"Pesquisar"  ,"AxPesqui"   ,0,1} ,;
							{"Visualizar" ,"AxVisual"   ,0,2} ,;
							{"Incluir"    ,"AxInclui"   ,0,3} ,;
							{"Alterar"    ,"AxAltera"   ,0,4} ,;
							{"Excluir"    ,"AxDeleta"   ,0,5}  ,;
							{"Ind. Quebra","T_INDQUEBRA",0,4}}


CHKTEMPLATE("COL")

dbSelectArea("LEX")
dbSetOrder(1)

dbSelectArea(cString)
mBrowse( 6,1,22,75,cString)
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � INDQUEBRA� Autor �  Jose Choite Kita Jr  � Data � 04/11/02 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Indice de Quebra                                           ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos e Leite                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function INDQUEBRA()
Local cAlias := alias()
Local nIndQM

If !Empty(GetMv("MV_INDQM")) //ATUALIZA O PARAMETRO
	nIndQM := GetMV("MV_INDQM")
Else
	nIndQM := 0.00556
	RecLock("SX6", .T.)
	SX6->X6_FIL      := "  "
	SX6->X6_VAR      := "MV_INDQM"
	SX6->X6_TIPO     := "N"
	SX6->X6_DESCRIC  := "Indice de calculo para quebra do Graos"
	SX6->X6_DESC1    := "Valor por dia := indice de quebra * qtde dias * (valor_saco/60)"
	SX6->X6_CONTEUD  := "0.00556"
EndIf

If !EMPTY(GetMv("MV_DIASQM")) //ATUALIZA O PARAMETRO
	nDiasQM := GetMV("MV_DIASQM")
Else
	nDiasQM := 0
	RecLock("SX6", .T.)
	SX6->X6_FIL      := "  "
	SX6->X6_VAR      := "MV_DIASQM"
	SX6->X6_TIPO     := "N"
	SX6->X6_DESCRIC  := "Carencia de dias a cobrar na quebra"
	SX6->X6_DESC1    := "Apos esta quantidade de dias sera cobrada a quebra do Graos"
	SX6->X6_CONTEUD  := "0"
EndIf

nOpca:=2
DEFINE MSDIALOG oDlg TITLE OemToAnsi("Indice de Quebra do Graos") From 3,0 to 10,30 of oMainWnd
@ 020,005 SAY OemtoAnsi("Indice") OF oDlg PIXEL COLOR CLR_BLUE
@ 020,035 MSGET oIndQM VAR nIndQM  PICTURE "9.99999" SIZE 40,4 OF oDlg PIXEL COLOR CLR_BLUE
@ 035,005 SAY OemtoAnsi("Dias Isentos") OF oDlg PIXEL COLOR CLR_BLUE
@ 035,035 MSGET oDiasQM VAR nDiasQM PICTURE "99" SIZE 40,4 OF oDlg PIXEL COLOR CLR_BLUE

ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{||nopca:=1,oDlg:End()},{||oDlg:End()}) CENTER

if nOpca==1
	dbSelectArea("SX6")
	PutMv("MV_INDQM",nIndQM)
	PutMv("MV_DIASQM",nDiasQM)
Endif

dbSelectArea(cAlias)

Return(.T.)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � CALCVALKG� Autor � Choite             � Data �  17/02/03   ���
�������������������������������������������������������������������������͹��
���Descricao � Atualiza preco por kilo, na inclusao ou alteracao          ���
�������������������������������������������������������������������������͹��
���Uso       � colocar no valid do LEX_VALSAC                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function CALCVALKG()
if Inclui .OR. Altera
	M->LEX_VALKIL := round(M->LEX_VALSAC/60,2)
Endif
Return .t.
