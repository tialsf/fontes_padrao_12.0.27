#include "Protheus.ch"
#include "TopConn.ch"

/*
Autor:     Fernando Roquete  
Data:      21/01/2004
Funcao:    CACMIA10
Descricao: Selecao do indice para desconto ref. umidade]
*/         

Template Function CACMIA10()
 ChkTemplate("COL")
 _lRet   :=.T. 
 _Indice :=0.00           
 _IndUmid:=M->LBT_Umidad
 _TipoGrd:=M->LBT_TipoGrd
 
 cQuery := "SELECT LBS_UMIDAD,LBS_DESCUM FROM " + RetSqlName("LBS") + " "
 cQuery += "WHERE D_E_L_E_T_ <> '*' AND "
 cQuery += "LBS_FILIAL = '" + xFilial("LBS") + "' AND "	
 cQuery += "LBS_TIPOGR= '" + _TipoGrd + "' "
 cQuery += "ORDER BY LBS_FILIAL,LBS_TIPOGR,LBS_UMIDAD"
 TCQUERY cQuery ALIAS QLBS NEW
 
 dbSelectArea("QLBS")
 While ! EoF() .And. _Indice==0
   If QLBS->LBS_Umidad = _IndUmid
     _Indice:=QLBS->LBS_DescUm
   Endif   
   dbskip()
 End 
 dbCloseArea()
 
 If Eof()
    MsgStop("Erro. Nao existe indice de Umidade")
    _lRet:=.F.  
 Else
    M->LBT_DescUm:=_Indice
 Endif 

Return(_lRet)