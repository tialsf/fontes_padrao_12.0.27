#INCLUDE "cacmia12.ch"
#INCLUDE "TOPCONN.CH"
#Include "Protheus.ch"

/*/
Autor....: Fernando Roquete
Data.....: 21/01/2004
Funcao...: CACMIA12
Descricao: Selecao dos Romaneios de Entrada
/*/

Template Function CACMIA12()

LOCAL _oDlg,oQual
LOCAL oOk 		 := LoadBitmap( GetResources(), "LBOK")
LOCAL oNo 		 := LoadBitmap( GetResources(), "LBNO")
LOCAL cVarQ      := "  "
LOCAL cCadastro  := OemToAnsi(STR0001) //"SELECAO DOS ROMANEIOS"
LOCAL aAreaAtu   := GetArea()
Local aAreaLBT   := {}
Local aAreaSA2   := {}
Local aAreaLEW   := {}
Local nPosCod    := aScan(aHeader,{|x| Trim(x[2]) == "D1_COD"})
Local nPosQtde   := aScan(aHeader,{|x| Trim(x[2]) == "D1_QUANT"})
Local nPosCOpe   := aScan(aHeader,{|x| Trim(x[2]) == "D1_CF"})
Local nPosVUni   := aScan(aHeader,{|x| Trim(x[2]) == "D1_VUNIT"})
Local nPosTot    := aScan(aHeader,{|x| Trim(x[2]) == "D1_TOTAL"})
Local nPosTES    := aScan(aHeader,{|x| Trim(x[2]) == "D1_TES"})
Local nPrcCust   := 0
Local _TpMov     :=""
Local _TotQtd    := 0
Local _Produto   := Alltrim(&(ReadVar()))
Local _I         := 0
Local _Flag      := .F.
Local cQuery     := ""
Local _cCodRoman := ""
Local _sCodOp    := ""
Local nDec       := MsDecimais(1)
Local nOpc       := 1
Local lRet       := .T.
Local cTpProd    := Posicione("SB1", 1, xFilial("SB1") + _Produto, "B1_GRAO")
Local cDescrP    := Posicione("SB1", 1, xFilial("SB1") + _Produto, "B1_DESC")
Local cSegUM     := Posicione("SB1", 1, xFilial("SB1") + _Produto, "B1_SEGUM")
Local nFatConv   := Posicione("SB1", 1, xFilial("SB1") + _Produto, "B1_CONV")
Local nMoeda     := Posicione("SB1", 1, xFilial("SB1") + _Produto, "B1_MOEDA")
Local nPosRom    := 0
Local cValid     := ""
Local cRVarAnt   := ReadVar()
Local cProdBKP   := aCols[n][nPosCod]
Local lFixar     := .F.
Local cQueryPE 	 := ""
Local aRomHead	 := {}
Local aRomaFld	 := {}
Local aRomHeadPE := {}
Local aRomaView	 := {} 
Local aRomaNew	 := {}
Local aStrucLBT  := {}
Local cALiasLBT	 := GetNextAlias()
Local nLBT 		 := 0
Local i			 := 0
Local j			 := 0 
Local lValidaPe	 :=.F.

If HASTEMPLATE("COL")

	aStrucLBT  := LBT->(dbStruct())
	// Caso seja devolucao nao existe a necessida de romaneio de entrada
	If Type("CTIPO") <> "U" .And. cTipo == "D"
		Return(lRet)
	EndIf
     
	// Esta sendo utilizada rotina automatica
	If Type("l103Auto") <> "U" .And. l103Auto
		Return(lRet)
	EndIf
	               
	// So executa a rotina a cada nova chamada                   
	If Type("__lSelRom") == "U" .Or.  !__lSelRom
		Return(lRet)	
	EndIf
	
	// Determina se eh necessario realizar a pesquisa de romaneios para o produto.
	If Empty(cTpProd)
		MsgInfo(OemToAnsi(STR0002)+AllTrim(M->D1_COD)+" - "+RTrim(cDescrP)+OemToAnsi(STR0003)) //"O produto "###" n�o est� com o campo 'Grao ?' (B1_GRAO) preenchido. Por favor, quando poss�vel, corrija o cadastro do produto."
	Else
		// Caso o produto nao seja grao ou semente nao existe a necessidade de verificar a existencia de romaneio.
		If !(cTpProd$"1|3")
			Return(lRet)
		EndIf
	EndIf
	
	cQuery := "SELECT LBT_ROMANE, LBT_QTDDES, LBT_GRAOS, LBT_QTDREA, LBT_DESCAR, LBT_NFEDES, LBT_NFESEM, "
	cQuery += "LBT_FORNEC, LBT_LOJA, LBT_PRODUT, LBT_DATA, LBT_DSCTRG, LBT_KGTRIG, LBT_UMIDAD, LBT_PH, X5_DESCRI"
	
	//��������������������������������������������������Ŀ
	//� Efetua a chamada do ponto de entrada CACM12QRY   �
	//����������������������������������������������������
	If Existblock('CACM12QRY')
		cQueryPE := Execblock('CACM12QRY', .F., .F., {cQuery})
		cQuery   += If(ValType(cQueryPE)=='C', cQueryPE, "")
	Endif
	
	cQuery += " FROM "+RetSqlName("LBT")+" LBT, " + RetSqlName("SX5") + " SX5 "
	cQuery += "WHERE (LBT.LBT_FILIAL = '" + xFilial("LBT") + "') AND "
	cQuery += "(LBT.LBT_FORNEC = '" + CA100FOR + "' AND "
	cQuery += "LBT.LBT_LOJA = '" + CLOJA + "') AND "                      
	cQuery += "LBT.LBT_PESO2 > 0 AND "
	cQuery += "(LBT.LBT_STATUS IN ('CF','EA','RM','EN')) AND "
	cQuery += "(LBT.LBT_ROMANE NOT IN (SELECT LEW_ROMANE "
	cQuery += "FROM "+RetSqlName("LEW")+" LEW "
	cQuery += "WHERE LEW.LEW_FILIAL = '"+xFilial("LEW")+"' AND "
	cQuery += "LEW.LEW_FORNEC = '"+CA100FOR+"' AND "
	cQuery += "LEW.LEW_LOJA = '"+CLOJA+"' AND "
	cQuery += "LEW.LEW_PRODUT = '"+_PRODUTO+"' AND "
	cQuery += "LEW.LEW_ROMANE = LBT.LBT_ROMANE AND "
	cQuery += "LEW.D_E_L_E_T_ <> '*')) AND "
	cQuery += "(SX5.X5_FILIAL = '" + xFilial("SX5") + "') AND "
	cQuery += "(SX5.X5_TABELA = 'IO' AND "
	cQuery += "SX5.X5_CHAVE  = LBT.LBT_CODOP) AND "
	cQuery += "LBT.D_E_L_E_T_<>'*' AND "
	cQuery += "SX5.D_E_L_E_T_<>'*' "
	cQuery += "ORDER BY  LBT_ROMANE "
	cQuery := ChangeQuery(cQuery)
	
	dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), cAliasLBT, .F., .T.)
	
	For nLBT := 1 to Len(aStrucLBT)
		If aStrucLBT[nLBT,2] != 'C' .and.  FieldPos(aStrucLBT[nLBT][1]) > 0
	    	TCSetField(cALiasLBT, aStrucLBT[nLBT,1], aStrucLBT[nLBT,2],aStrucLBT[nLBT,3],aStrucLBT[nLBT,4])
	 	EndIf	
	Next
	
	dbGoTop()
	If Eof() 
		If cTpProd$"1|3"
			MsgAlert(OemToAnsi(STR0004)) //"Produto precisa de romaneio para que a entrada possa ser realizada!!!"
			lRet := .F.
		ElseIf Empty(cTpProd)
			MsgAlert(OemToAnsi(STR0005+; //"N�o foi encontrado nenhum romaneio para o produto. Para determinar se existe "
			         STR0006+; //"a necessidade do romaneio, � necess�rio que o campo 'Grao ?' seja informado "
			         STR0007+; //"no cadastro do produto. Por favor, corrija o cadastro do produto e posteriormente "
			         STR0008)) //"retorne � rotina de NF de Entrada."
			lRet := .F.		         
		EndIf
		dbCloseArea()
		RestArea(aAreaAtu)
		Return(lRet)
	Endif
	
	// Reinicializa o array aRomaneio
	aRomaneio := {}                 
	While !Eof()
		_Flag     := .F.      
		
		If AllTrim((cAliasLBT)->LBT_PRODUT) <> AllTrim(_PRODUTO)
			If Empty((cAliasLBT)->LBT_DESCAR) 
				If Empty((cAliasLBT)->LBT_DSCTRG) // mad 19/08/04
					dbSkip()
					Loop
				EndIf	
			Else
				If !Alltrim((cAliasLBT)->LBT_DESCAR)==_PRODUTO // mad 23/07/04
					dbSkip()
					Loop
				EndIf
			EndIf
		EndIf
		
		dbSelectArea("LBT")
		If Empty(aAreaLBT)
			aAreaLBT := GetArea()
		EndIf
		dbSetorder(6)
		If dbSeek(xFilial("LBT")+(cAliasLBT)->LBT_ROMANE,.F.)
			_TpMov := IIf((Alltrim(LBT->LBT_PRODUT) == Alltrim(_Produto)), Iif(Empty(LBT->LBT_DESCAR), "COMERCIAL", "SEMENTE"), Iif(Upper(AllTrim(_Produto)) == "TRIGUILHO","DSCTRIG","DESCARTE")) // mad 19/08/04
			
			dbSelectArea("SA2")
			If Empty(aAreaSA2)
				aAreaSA2 := GetArea()
			EndIf
			dbSetorder(1)
			If dbSeek(xFilial("SA2") + CA100FOR + CLOJA, .F.) .And. !(SA2->A2_SEMENT$"S|1")
				MsgStop(OemToAnsi(STR0014)) //"Fornecedor inv�lido, nao � produtor de Semente/Gr�o"
				dbSelectArea(cAliasLBT)
				dbCloseArea()
				RestArea(aAreaAtu)
				Return(.F.)
			Endif
		Endif
		
		If !Empty(aGrvRomaneio)          
			_Result := aScan(aGrvRomaneio,{|x| Trim(x[7]) == Trim(_Produto) })						
			If _Result == 0
				MsgAlert(OemToAnsi(STR0015)) //"Foi selecionado um Gr�o/Semente diferente do selecionado anteriormente."
				aRomaneio := {}
				lRet      := .F.
				Exit
			EndIf
		EndIf
		
		If _TpMov == "COMERCIAL"
			_Qtd := (cAliasLBT)->LBT_QTDREA
			
			// mad 23/07/04
			dbSelectArea("LEW")
			If Empty(aAreaLEW)
				aAreaLEW := GetArea()
			EndIf
			dbSetOrder(2)
			If dbSeek(xFilial("LEW")+(cAliasLBT)->LBT_ROMANE)
				dbSelectArea(cAliasLBT)
				dbSkip()
				Loop
			EndIf
		ElseIF _TpMov == "SEMENTE"
			_Qtd := (cAliasLBT)->LBT_GRAOS
		ELseIF _TpMov == "DESCARTE"
			_Qtd := (cAliasLBT)->LBT_QTDDES
		ElseIf _TpMov == "DSCTRIG"
			_Qtd := (cAliasLBT)->LBT_KGTRIG
		Endif
		
		_Pend := "N"
		
		If _TpMov == "SEMENTE"
			If ((cAliasLBT)->LBT_NFEDES <> "S") .And. ((cAliasLBT)->LBT_QTDDES > 0)
				_Pend := "S"
			Endif
		ElseIf _TpMov == "DESCARTE" // mad 19/08/04 
			If ((cAliasLBT)->LBT_NFESEM <> "S") .And. ((cAliasLBT)->LBT_GRAOS > 0)
				_Pend := "S"
			Endif
		ElseIf _TpMov == "DSCTRIG"                             // MAD 19/08/04 
			If ((cAliasLBT)->LBT_NFESEM <> "S") .And. ((cAliasLBT)->LBT_KGTRIG > 0)
				_Pend := "S"
			Endif
		Endif
		
		_Result := aScan(aRomaneio,{ |aVet| aVet[2] == (cAliasLBT)->LBT_ROMANE})
		If _Result <> 0
			If aRomaneio[_Result,1]
				_Flag:=.T.
			Endif
		Else
			If (_TpMov == "SEMENTE") .And. ((cAliasLBT)->LBT_NFEDES == "S") //"SEMENTE"
				_Flag:=.T.
			Endif
			If (_TpMov == "DESCARTE") .And. ((cAliasLBT)->LBT_NFESEM == "S")
				_Flag:=.T.
			Endif
			If (_TpMov == "DSCTRIG") .And. ((cAliasLBT)->LBT_NFESEM == "S") // MAD 19/08/04 
				_Flag:=.T.
			Endif
		Endif
		
	    If _Qtd > 0   // mad 19/08/04
	    	If !Empty(aSelRom)
		    	_Result := aScan(aSelRom,{|x| Trim(x[1])==Trim((cAliasLBT)->LBT_ROMANE) .And. Trim(x[3])==AllTrim(_PRODUTO) })
		    Else
				_Result := 0
			EndIf
				    
			If _Result == 0
				AADD(aRomaneio,{_Flag,(cAliasLBT)->LBT_ROMANE,(cAliasLBT)->LBT_DATA,_Qtd,_TpMov,_Pend,AllTrim(_PRODUTO),(cAliasLBT)->X5_Descri,(cAliasLBT)->LBT_PH})
				AAdd(aSelRom,{(cAliasLBT)->LBT_ROMANE,0,AllTrim(_PRODUTO)})
			Else
				If aSelRom[_Result][2] == 0 .Or. (aSelRom[_Result][2] == n)
				   AADD(aRomaneio,{_Flag,(cAliasLBT)->LBT_ROMANE,(cAliasLBT)->LBT_DATA,_Qtd,_TpMov,_Pend,AllTrim(_PRODUTO),(cAliasLBT)->X5_Descri,(cAliasLBT)->LBT_PH})
				EndIf
			EndIf
		EndIf		
		dbSelectArea((cAliasLBT))
		dbSkip()
	End    
	
	//��������������������������������������������������Ŀ
	//� Efetua a chamada do ponto de entrada CACM12FLD   �
	//����������������������������������������������������	
	If ExistBlock( "CACM12FLD" )
		aRomaFld := ExecBlock( "CACM12FLD", .F., .F.,{aRomaneio, (cAliasLBT)} )
		lValidaPe:= Len (aRomaFld) == Len(aRomaneio)
		If lValidaPe
			If ValType(aRomaFld) == 'A'     
	 			aRomaNew:= aClone(aRomaneio)
		 		aRomaneio:= {}
				For j:=1 To Len(aRomaNew)
				aAdd(aRomaneio,Array(Len(aRomaNew[1])+Len(aRomaFld[1])))	
					For i:=1 to Len(aRomaNew[1])
						aRomaneio[j,i] := aRomaNew[j,i]	   		
					Next i
					For i:=1 to Len(aRomaFld[1])
						aRomaneio[j,9+i] := aRomaFld[j,i]
					Next i
				Next j
			Endif
		Endif
	EndIf
	
	dbSelectArea(cAliasLBT)
	dbCloseArea()
	
	If Len(aRomaneio)> 0 
		nOpc := 0
	
		aRomHead:={"",STR0016,STR0017,STR0018,STR0019,STR0020,STR0021}
			
		//��������������������������������������������������Ŀ
		//� Efetua a chamada do ponto de entrada CACM12CAB   �
		//����������������������������������������������������
	    	
		If ExistBlock( "CACM12CAB" )
			aRomHeadPE := ExecBlock( "CACM12CAB", .F., .F.,{aRomHead} )
			If lValidaPe
				if ValType(aRomHeadPE) == 'A'
					nTamHead := Len(aRomHead)
	  				For i:=1 to Len(aRomHeadPE)
						aAdd(aRomHead,"")
						aRomHead[nTamHead+i] := aRomHeadPE[i]
					Next
				Endif
			Endif
		EndIf
	
		aSort(aRomaneio,,,{|tVet1, tVet2| tVet1[8]+DToC(tVet1[3])+tVet1[2] < tVet2[8]+DToC(tVet2[3])+tVet2[2]})
		
		DEFINE MSDIALOG _oDlg TITLE cCadastro From 145,0 To 500,650 OF oMainWnd PIXEL
		@ 05,010 LISTBOX oQual VAR cVarQ Fields HEADER 'INIT' SIZE 300,150 ON DBLCLICK (aRomaneio:=CA710Troca(oQual:nAt,aRomaneio),oQual:Refresh()) ON RIGHT CLICK ListBoxAll(nRow,nCol,@oQual,oOk,oNo,@aRomaneio) NOSCROLL OF _oDlg  PIXEL //"Romaneio"###"Data Romaneio"###"Qtde"###"Operacao"###"Tipo Movto"###"Indice PH."
	
		oQual:SetArray(aRomHead)
		oQual:aHeaders:=aRomHead
	  
		oQual:SetArray(aRomaneio)
	    bBloco := "{|| {If(aRomaneio[oQual:nAt,1],oOk,oNo),  " 
	    bBloco += " aRomaneio[oQual:nAt,2],aRomaneio[oQual:nAt,3],aRomaneio[oQual:nAt,4],aRomaneio[oQual:nAt,8],aRomaneio[oQual:nAt,5],aRomaneio[oQual:nAt,9]"
	    If lValidaPe
	    	For i := 10 To Len(aRomaneio[1])
				bBloco += " ,aRomaneio[oQual:nAt,"+AllTrim(str(i))+"]"
	 		Next       
		Endif
		bBloco += "}}"
		          
		oQual:bLine := &(bBloco)
	
		DEFINE SBUTTON FROM 160,245 TYPE 1 ACTION (GrvRomaneio(@_TotQtd),nOpc:=1,_oDlg:End()) ENABLE OF _oDlg
		DEFINE SBUTTON FROM 160,275 TYPE 2 ACTION (nOpc:=2,_oDlg:End()) ENABLE OF _oDlg
		
		ACTIVATE MSDIALOG _oDlg CENTERED
	
	Endif
	
	If nOpc == 1
		_cCodRoman   := ""
		_sCodOp      := ""
		For _I:=1 to Len(aRomaneio)
			If aRomaneio[_I,1]
				_cCodRoman := aRomaneio[_I,2]
				
				dbSelectArea("LBT")
				If Empty(aAreaLBT)
					aAreaLBT := GetArea()
				EndIf
				dbSetorder(6)
				dbSeek(xFilial("LBT")+_cCodRoman,.F.)
				If Empty(_sCodOp)
					_sCodOp += RTrim(LBT->LBT_CODOP) // retorna no campo d1_tes da nota o codigo da opera��o
				Else
					If !RTrim(LBT->LBT_CODOP) $ RTrim(_sCodOp)
						MsgStop(OemToAnsi(STR0022)) //"Romaneios com c�digos de opera��es diferentes. Verifique!!!"
					EndIf
				EndIf
	            
		    	_Result := aScan(aSelRom,{|x| Trim(x[1])==Trim(aRomaneio[_I,2]) .And. x[3]==aRomaneio[_I,7] })
		    	aSelRom[_Result][2] := n
			Endif
		Next _I
	
		SF4->( dbSetOrder(1) )
		If !SF4->( MsSeek(xFilial("SF4")+_sCodOp) )
			MsgInfo(OemToAnsi(STR0023+; //"O codigo de operacao nao foi encontrado no arquivo de TES (SF4). Caso n�o queira que "
			        STR0024+; //"seja utilizado o TES padr�o do produto, por favor, saia da rotina de NF de Entrada e "
			        STR0025)) //"cadastre a opera��o, informada no Romaneio, no arquivo de TES"
				
		    lRet := .F.
		Else
			lFixar := SF4->F4_FIXAR $ "1S"
		EndIf
	                                                                                               
		If lRet
			If lFixar // .Or. FunName() == "MATA103"  // BOPS 88950: Sempre trazer custo reposicao - Ricardo Berti 23/11/05
				// Se compra a fixar retorna o preco de custo...
				nPrcCust := Posicione("SB1", 1, xFilial("SB1")+M->D1_COD, "B1_CUSTD")               
		
				If nPrcCust == 0
					MsgAlert(OemToAnsi(STR0026)) //"O produto informado n�o possui pre�o de custo cadastrado. Por favor, informe um pre�o de custo, no cadastro de produtos e, ap�s, retorne � rotina de NF de Entrada."
					lRet := .F.
				EndIf
			Else                                            
				// caso contrario traz o preco, por kilo, do grao no dia.
				nPrcCust := If(nFatConv==0,0,Round( RecMoeda(dDataBase, nMoeda) / nFatConv, 4 )) // 20/09/05 Ricardo Berti, Calc.Custo alterado de 0 p/ 4 casas dec. no ROUND()
							
				If nPrcCust == 0
					MsgAlert(OemToAnsi(STR0027+DToC(dDataBase)+STR0028)) //"O produto n�o possui cota��o cadastrada para o dia "###". Por favor realize este cadastro e posteriormente retorne � rotina de Entrada de Gr�os para realizar a opera��o de compra � vista do produto."
					lRet := .F.
				EndIf
			EndIf
		EndIf
	
		If lRet  
			// Primeiramente eh necessario acertar as variaveis referentes ao campo do Produto
			aCols[n][nPosCod] := M->D1_COD
		                 
			// Determina a quantidade                                      
			If (nPosQtde > 0)
				SX3->( dbSetOrder(2) )
				SX3->( MsSeek("D1_QUANT") )
				cValid := RTrim(SX3->X3_VALID)+Iif(!Empty(SX3->X3_VALID) .And. !Empty(SX3->X3_VLDUSER)," .And. "+RTrim(SX3->X3_VLDUSER),Iif(!Empty(SX3->X3_VLDUSER),RTrim(SX3->X3_VLDUSER),"")) 
				aCols[n][nPosQtde] := _TotQtd
				M->D1_QUANT := _TotQtd		
				__ReadVar := "M->D1_QUANT"
				
				lRet := &(cValid)
			
				If lRet .And. ExistTrigger("D1_QUANT")
				   RunTrigger(2,n,,"D1_QUANT")
				EndIf
			EndIf
	
			If lRet .And. SX3->( MsSeek("D1_VUNIT") ) .And. (nPosVUni > 0)
				cValid := RTrim(SX3->X3_VALID)+Iif(!Empty(SX3->X3_VALID) .And. !Empty(SX3->X3_VLDUSER)," .And. "+RTrim(SX3->X3_VLDUSER),Iif(!Empty(SX3->X3_VLDUSER),RTrim(SX3->X3_VLDUSER),"")) 
				aCols[n][nPosVUni] := nPrcCust
				M->D1_VUNIT := nPrcCust
				__ReadVar := "M->D1_VUNIT"
				
				lRet := &(cValid)
			
				If ExistTrigger("D1_VUNIT")
				   RunTrigger(2,n,,"D1_VUNIT")
				EndIf
			EndIf	
	
			If lRet .And. SX3->( MsSeek("D1_TOTAL") ) .And. (nPosTot > 0)
				cValid := RTRim(SX3->X3_VALID)+Iif(!Empty(SX3->X3_VALID) .And. !Empty(SX3->X3_VLDUSER)," .And. "+RTrim(SX3->X3_VLDUSER),Iif(!Empty(SX3->X3_VLDUSER),RTrim(SX3->X3_VLDUSER),"")) 
				aCols[n][nPosTot] := Round(_TotQtd * nPrcCust,nDec)
				M->D1_TOTAL := Round(_TotQtd * nPrcCust,nDec)
				__ReadVar := "M->D1_TOTAL"
				
				lRet := &(cValid)
			
				If lRet .And. ExistTrigger("D1_TOTAL")
				   RunTrigger(2,n,,"D1_TOTAL")
				EndIf         
			EndIf                       
	
			If lRet .And. SX3->( MsSeek("D1_TES") ) .And. (nPosTES > 0)
				cValid := RTrim(SX3->X3_VALID)+Iif(!Empty(SX3->X3_VALID) .And. !Empty(SX3->X3_VLDUSER)," .And. "+RTrim(SX3->X3_VLDUSER),Iif(!Empty(SX3->X3_VLDUSER),RTrim(SX3->X3_VLDUSER),"")) 
				aCols[n][nPosTES] := _sCodOp
				M->D1_TES := _sCodOp
				__ReadVar := "M->D1_TES"
					
				lRet := &(cValid)
				
				If lRet .And. ExistTrigger("D1_TES")
				   RunTrigger(2,n,,"D1_TES")
				EndIf         
			EndIf
	
			// Restaura o valor da variavel __ReadVar
			__ReadVar := cRVarAnt
			
		 	If lRet .And. ExistBlock("CACM12ROM") 
		 		ExecBlock( "CACM12ROM", .F., .F.)
		 	Endif
			
		EndIf
		
		// Restaura area original do arquivo LEW    
		If !Empty(aAreaLEW)
			RestArea(aAreaLEW)
		EndIf
		                  
		// Restaura area original do arquivo SA2
		If !Empty(aAreaSA2)
			RestArea(aAreaSA2)
		EndIf
		
		// Restaura area original do arquivo LBT
		If !Empty(aAreaLBT)
			RestArea(aAreaLBT)
		EndIf
		
		// Restaura area original
		RestArea(aAreaAtu)
	
		// Retornar o codigo original para o aCols
		If !lRet
			aCols[n][nPosCod] := cProdBKP            
			
			Return(lRet)
		EndIf
		                                 
		// Valoriza array aGrvRomaneio, para que ele seja utilizado em outras rotinas.
		If Empty(aGrvRomaneio)
			aGrvRomaneio := aClone(aRomaneio)
		Else
			For _I := 1 To Len(aRomaneio)
			    _Result := aScan(aGrvRomaneio,{|x| Trim(x[2]) == Trim(aRomaneio[_I][02]) .And. x[7]==aRomaneio[_I,7] })					
			    If aGrvRomaneio[_Result][1] <> aRomaneio[_I][01]
			    	aGrvRomaneio[_Result][1] := aRomaneio[_I][01]
			    EndIf
			Next _I	
		EndIf
	Else
		lRet := .F.
	EndIf

	RestArea(aAreaAtu)
EndIf
	
Return(lRet)

//---------------------------------------------------------------------------------------------------------------//             

Static Function GrvRomaneio(_TotQtd)

Local _I := 0

For _I := 1 To Len(aRomaneio)
	If aRomaneio[_I,1]
		_TotQtd += aRomaneio[_I,4]
	Endif
Next

Return
