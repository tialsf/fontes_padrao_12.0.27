#INCLUDE "cacmia16.ch"
#INCLUDE "Protheus.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA16 � Autor �Ricardo Berti          � Data � 24/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Controle de Retencao / Perda de Graos                      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CacMia16()						                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function CACMIA16()

Local cPerg      := "CACA16"
Local aAreaAtu   := GetArea()
Local aCampos    := {}
Local aCores :=	{{ "LJX_STATUS=='0'", 'BR_VERMELHO'},; // PENDENTE
				 { "LJX_STATUS=='1'", 'ENABLE'		}} // BAIXADO
Local aIndex     := {}
Local cString	:= "LJX"
Local nReg
Local nOpc
Local cFiltro    := ""

//Private cString, nReg, nOpc
Private bFiltraBrw := {}
Private aRotina	:= {{OemToAnsi(STR0002)	,"AxPesqui"		,0,1	},; //"Pesquisar"
					{OemToAnsi(STR0003)	,"AxVisual"		,0,2	},; //"Visualizar"
					{OemToAnsi(STR0004)	,"T_CacA16Inc"	,0,3	},; //"Incluir"
					{OemToAnsi(STR0005)	,"T_CacA16Bx"	,0,4	},; //"Baixar"
					{OemToAnsi(STR0006)	,"T_CacA16Exc"	,0,5	},; //"Excluir"
					{OemToAnsi(STR0007)	,"T_CacA16Leg"	,0,5,2}}	//"Legenda"
//					 {"Imprimir"			,"U_Cacmir16"				,5,2	},; //Imprime Pendencias

Private cCadastro := OemToAnsi(STR0001) //"Reten��es / Perdas - Sa�das de Gr�os"

ChkTemplate("COL")
/*
CriaSX1(cPerg)
If !Pergunte(cPerg,.T.)
Return Nil
EndIf
*/
// SetKey(VK_F12, {|| Pergunte(cPerg,.T.), CAC02AtBr() })

// Campos que serao exibidos no browse
aCampos := PCposBrw()

cFiltro	:= "(LJX_FILIAL == '"+xFilial("LJX")+"') " //  .And. "
// Status Referentes a Saidas
//If ExistBlock("CACM2FIL")
//	cFiltro += " .And. "+ExecBlock("CACM2FIL",.F.,.F.,{mv_par01,cFiltro})
//EndIf

AjustaSX3()
AjustaHelp()
           
bFiltraBrw	:= { || FilBrowse( cString, @aIndex, @cFiltro ) }
Eval( bFiltraBrw )
mBrowse(6, 1,22,75,cString,aCampos,,,,2,aCores,,,2)
EndFilBrw(cString,aIndex)

SetKey(VK_F12,Nil)

RestArea(aAreaAtu)

Return Nil
    

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CacA16Inc � Autor � Ricardo Berti        � Data � 25/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chama Inclusao de Retencao / Perda de Graos			      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CacA16Inc(ExpC1,ExpN1,ExpN2)                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                   ���
���          � ExpN1 = Numero do registro                                 ���
���          � ExpN2 = Opcao selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template COL_GRA - Cooperativa de Graos                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CacA16Inc(cAlias,nReg,nOpc)

Local aCpoInibe:= {{"LJX_STATUS","LJX_NFENT","LJX_SEENT","LJX_DTNFE"}}
Local aCposEnc := PCposEnc(aCpoInibe)
Local nOpca    := AxInclui(cAlias,nReg,nOpc,aCposEnc,,aCposEnc,"T_Ca16VldPer()")

If __lSX8
	If nOpca == 1
		// Confirma o numero de retencao obtido atraves do SXE ou do HardLock
		ConfirmSX8()
	Else
		// Cancela o numero de retencao obtido atraves do SXE ou do HardLock
		RollBackSx8()
	EndIf
EndIf

If nOpca == 1 .And. ExistBlock( "CACM16GRV" )
	ExecBlock( "CACM16GRV", .F., .F., {nOpc} )
EndIf

// Atualiza o Browse
Eval( bFiltraBrw )

Return Nil
    

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CacA16Exc�Autor  �Ricardo Berti          � Data � 25/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Exclusao da Retencao / Perda de Graos				      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CacA16Exc(ExpC1,ExpN1,ExpN2)		                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo							          ���
���			 � ExpN1 = Numero do registro 						          ���
���			 � ExpN2 = Numero da opcao selecionada 						  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CacA16Exc(cAlias,nReg,nOpc)

Local aCpoInibe:= {{""}}
Local aCposEnc := PCposEnc(aCpoInibe)
Local nOpca    := AxDeleta(cAlias,nReg,nOpc,"CA16Del",aCposEnc)

If nOpca == 2 .And. ExistBlock( "CACM16GRV" )
	ExecBlock( "CACM16GRV", .F., .F., {nOpc} )
EndIf

// Atualiza o Browse
Eval( bFiltraBrw )

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � Ca16Del  �Autor  �Ricardo Berti          � Data � 08/11/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Rotina p/atualizar SB2 (SD.P3 Acum) na Exclusao da Retencao���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ca16Del()	                                      		  ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function CA16Del()

Local lCA16Del := .T.
Local aAreaAtu := GetArea()

If LJX->LJX_STATUS == "0" .And. LJX->LJX_PODER3 $ "DR" // .And. LJX->LJX_TIPO == "2"  // RETENCAO
	//Local cMsg	   := ""
	//cMsg += "Retencao/Perda pendente! Estorno SB2 ser� feito! "
	//����������������������������������������������������������Ŀ
	//�Atualiza SB2: SD.AC.PODER3								 �
	//������������������������������������������������������������
	If LJX->LJX_TIPO == "3"  // Ganho
		CA16SB2("-",LJX->LJX_TES,LJX->LJX_PRODUT,LJX->LJX_LOCAL,LJX->LJX_QTDRET)
	Else
		CA16SB2("+",LJX->LJX_TES,LJX->LJX_PRODUT,LJX->LJX_LOCAL,LJX->LJX_QTDRET)
	EndIf
	RestArea(aAreaAtu)
EndIf

Return(lCA16Del)

    
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � Ca16SB2  �Autor  �Ricardo Berti          � Data � 28/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Atualizacao Sd.Poder 3 Acumulado (SB2)                     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ca16SB2(ExpC1,ExpC2,ExpC3,ExpC4,ExpN1)                     ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Tipo do Movto (+ / -)					          ���
���			 � ExpC2 = TES                 					         	  ���
���			 � ExpC3 = Cod.do Produto     						          ���
���			 � ExpC4 = Local estoque   						        	  ���
���			 � ExpN1 = Qtde. movimentada (retencao)						  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CA16SB2(cTipoMov,cTES,cProduto,cLocal,nQtdRet)

Local nSd1		 := 0
Local nSd2		 := 0
Local aAreaAtu 	 := GetArea()
Local aAreaSF4	 := SF4->(GetArea())
Local aAreaSB2	 := SB2->(GetArea())

If SF4->(MsSeek(xFilial("SF4") + cTES))
	
	If SF4->F4_PODER3 $ "DR"
		
		//������������������������������������������������������������������������Ŀ
		//� Atualiza os acumulados do poder de terceiros (SB2)                     �
		//��������������������������������������������������������������������������
		dbSelectArea("SB2")
		dbSetOrder(1)
		If !MsSeek(xFilial("SB2") + cProduto + cLocal)
			CriaSb2(cProduto,cLocal)
		EndIf
		RecLock("SB2",.F.)
		
		nSd1 := SB2->B2_QTER
		If ( SF4->F4_PODER3 == "D" )
			If SF4->F4_ESTOQUE == "N"
				SB2->B2_QTER -= ( nQtdRet * If(cTipoMov="+",1,-1) )
				nSd2 := SB2->B2_QTER
			Else
				nSd1 := SB2->B2_QTNP
				SB2->B2_QTNP -= ( nQtdRet * If(cTipoMov="+",1,-1) )
				nSd2 := SB2->B2_QTNP
			EndIf
		Else
			If SF4->F4_ESTOQUE == "N"
				SB2->B2_QTER += ( nQtdRet * If(cTipoMov="+",1,-1) )
				nSd2 := SB2->B2_QTER
			Else
				nSd1 := SB2->B2_QNPT
				SB2->B2_QNPT += ( nQtdRet * If(cTipoMov="+",1,-1) )
				nSd2 := SB2->B2_QNPT
			EndIf
		EndIf
		//MsgInfo(If(cTipoMov="+","Estorno ","Atualiz.")+;
		SB2->(MsUnLock())
		MsgInfo( OemToAnsi(STR0008) + LTrim(Str(nSd1)) + OemToAnsi(STR0009) + LTrim(Str(nSd2)) )
		// Saldo Ac.em Poder de Terceiros (SB2) ser� atualizado de ... para ....
		RestArea(aAreaSB2)
	EndIf
EndIf
RestArea(aAreaSF4)
RestArea(aAreaAtu)

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CacA16Leg�Autor  �Ricardo Berti          � Data � 25/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Mostra a Legenda da mBrowse                                ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CacA16Leg()						                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T.		                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CacA16Leg()

BrwLegenda(cCadastro,OemToAnsi(STR0010),{; //"Status"
{"BR_VERMELHO" 	, OemToAnsi("0 - "+OemToAnsi(STR0011)	)},; //"0 - Pendente"
{"ENABLE"	 	, OemToAnsi("1 - "+OemToAnsi(STR0012)  	)}}) //"1 - Baixado"

Return(.T.)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CacA16Bx �Autor  �Ricardo Berti          � Data � 27/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Baixa da Retencao / Perda de Graos				          ���
���          �ATENCAO: SE TIPO = PERDA nao atualiza saldo Ac. Poder 3     ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � CacA16Bx(ExpC1,ExpN1,ExpN2)                                ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Alias do arquivo                                   ���
���          � ExpN1 = Numero do registro                                 ���
���          � ExpN2 = Opcao selecionada                                  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CacA16Bx(cAlias,nReg,nOpc)

Local aAreaAtu := GetArea()
Local aCpoInibe:= {{"LJX_NFSAID","LJX_DTRET","LJX_TIPO","LJX_MOTIVO","LJX_QTDRET","LJX_STATUS","LJX_DTCHEG"}}
Local aCposEnc := PCposEnc(aCpoInibe)
Local nOpca   

If LJX->LJX_STATUS == "1"  // BAIXADA
	MsgInfo(OemToAnsi(STR0028),OemToAnsi(STR0033)) // "Retencao j� baixada!"###"Atencao"
	lRet := .F.
Else

	nOpca := AxAltera(cAlias,nReg,nOpc,,aCposEnc,,,"T_Ca16VldBx()")
	
	If nOpca == 1	
	
		RecLock("LJX",.F.)
		
		// somente se Retencao ou Ganho
		If LJX->LJX_PODER3$"DR" .And. (LJX->LJX_TIPO=="2".Or.LJX->LJX_TIPO=="3") 
			
			//����������������������������������������������������������Ŀ
			//�Atualiza SB2: SD.AC.PODER3								 �
			//������������������������������������������������������������
			
			CA16SB2(If(LJX->LJX_TIPO=="3","-","+"),LJX->LJX_TES,LJX->LJX_PRODUT,LJX->LJX_LOCAL,LJX->LJX_QTDRET)
			
			RestArea(aAreaAtu)
		EndIf
		
		LJX->LJX_STATUS := "1"
		LJX->(MsUnlock())
		
		// Atualiza o Browse
		Eval( bFiltraBrw )
	EndIf

EndIf
// Restaura area original
RestArea(aAreaAtu)

Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �Ca16VldBx �Autor  �Ricardo Berti          � Data � 22/12/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Executa validacao dos dados obrigatorios na Baixa          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ca16VldBx()        				                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Template COL_GRA Cooperativa de Graos                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function Ca16VldBx()

Local lRet		:= .T.
Local aAreaSD1	:= SD1->(GetArea())
Local aArea		:= GetArea()

If LJX->LJX_STATUS == "1"  // BAIXADO
	MsgInfo(OemToAnsi(STR0028),OemToAnsi(STR0033)) // "Retencao j� baixada!" //"Aten��o"
	lRet := .F.
ElseIf Empty(M->LJX_NFENT)
	MsgAlert(OemToAnsi(STR0037),OemToAnsi(STR0033)) //"Informe a Nota Fiscal ref. � Dev.ou Compra da reten��o/perda."
	lRet := .F.
Else
 	DbSelectArea("SD1")
	DbSetOrder(1)
	cSerie := If(Empty(M->LJX_SEENT),SD1->D1_SERIE,M->LJX_SEENT)
   	If ! DbSeek(xFilial("SD1")+M->LJX_NFENT+cSerie+M->LJX_CLIENT+M->LJX_LOJA+M->LJX_PRODUT)
		MsgAlert(OemToAnsi(STR0037),OemToAnsi(STR0033)) //"Informe a Nota Fiscal ref. � Dev.ou Compra da reten��o/perda."
		lRet := .F.
	EndIf		
	If SD1->D1_FORNECE <> M->LJX_CLIENT
	   	MsgAlert(OemToAnsi(STR0038),OemToAnsi(STR0033)) //"O cliente/fornecedor da reten��o/perda difere da nota selecionada."
		lRet := .F.
	EndIf
	If SD1->D1_COD <> M->LJX_PRODUT
	   	MsgAlert(OemToAnsi(STR0039),OemToAnsi(STR0033)) //"O produto da reten��o/perda n�o consta na nota selecionada."
		lRet := .F.
	EndIf
	RestArea(aAreaSD1)
	RestArea(aArea)
	
EndIf

Return(lRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �Ca16VldPer�Autor  �Ricardo Berti          � Data � 24/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Executa validacao dos dados obrigatorios da Retencao/Perda ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � Ca16VldPer()        				                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � .T. / .F.                                                  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function Ca16VldPer()

Local lRet		 := .T.
Local cMsg		 := OemToAnsi(STR0013) //"Por favor, para que a Reten��o possa ser finalizada, "
Local aArea		 := GetArea()
Local aAreaSD2	 := SD2->(GetArea())
Local aAreaSF4	 := SF4->(GetArea())

If Empty(M->LJX_NFSAID)
	MsgAlert(cMsg + OemToAnsi(STR0014)) //"informe a Nota Fiscal ref. � reten��o/perda."
	lRet := .F.
	
ElseIf Empty(M->LJX_QTDRET)
	MsgAlert(cMsg + OemToAnsi(STR0015)) //"informe a qtde. da reten��o/perda."
	lRet := .F.
	
ElseIf Empty(M->LJX_DTRET)
	MsgAlert(cMsg + OemToAnsi(STR0016)) //"informe a data da reten��o/perda."
	lRet := .F.
	
ElseIf !Empty(M->LJX_NFSAID+M->LJX_SESAID)  // Atualizacao de saldos
	dbSelectArea("SD2")
	dbSetOrder(3)
	If MsSeek(xFilial("SD2")+M->LJX_NFSAID+M->LJX_SESAID+M->LJX_CLIENT+M->LJX_LOJA+M->LJX_PRODUT)
		If M->LJX_QTDRET > SD2->D2_QUANT-SD2->D2_QTDEDEV
			MsgAlert(OemToAnsi(STR0036))  //"Qtde. informada maior que qtde. da NF Saida - Dev."
			lRet := .F.
		ElseIf M->LJX_DTRET < SD2->D2_EMISSAO
			MsgAlert(OemToAnsi(STR0017))  //"Data Retencao/Perda informada menor que emissao da NF."
			lRet := .F.
		ElseIf	M->LJX_DTCHEG < M->LJX_DTRET .Or. ;
			M->LJX_DTCHEG > dDataBase
			MsgAlert(OemToAnsi(STR0035))  //"Data da Chegada invalida."
			lRet := .F.
		Else
			M->LJX_LOCAL := SD2->D2_LOCAL
			M->LJX_TES   := SD2->D2_TES
			//����������������������������������������������������������Ŀ
			//�Verifica a NF controla PODER3		  	 				 �
			//������������������������������������������������������������
			// Verifica a TES
			SF4->(dbSetOrder(1))
			IF SF4->(MsSeek(xFilial("SF4")+SD2->D2_TES))
				If SF4->F4_PODER3 $ "DR"  //  Remessa, Devol.
					M->LJX_PODER3 := SF4->F4_PODER3
					//����������������������������������������������������������Ŀ
					//�Atualiza SB2: SD.AC.PODER3								 �
					//������������������������������������������������������������
					
					CA16SB2(If(M->LJX_TIPO=="3","+","-"),M->LJX_TES,M->LJX_PRODUT,M->LJX_LOCAL,M->LJX_QTDRET)
					
				EndIf
			EndIf
		EndIf
	EndIf
	
	RestArea(aAreaSF4)
	RestArea(aAreaSD2)
EndIf

If lRet
	M->LJX_STATUS := "0"
EndIf
RestArea(aArea)

Return(lRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PCposEnc �Autor  �Ricardo Berti          � Data � 24/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Monta array pelo SX3 com campos a serem exibidos		      ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PCposEnc(ExpA1)    				                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpA1 = array com campos que nao devem ser exibidos		  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpA1 = array com campos que serao exibidos			      ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PCposEnc(aCpoInibe)

Local aCposEnc := {}

SX3->( dbSetOrder(1) )
SX3->( MsSeek("LJX") )
While !SX3->( Eof() ) .And. SX3->X3_ARQUIVO == "LJX"
	If X3USO(SX3->X3_USADO) .And. (cNivel >= SX3->X3_NIVEL)

//	   If aCpoInibe[1][1] == NIL .Or. aScan(aCpoInibe, {| x | x[1] == AllTrim(SX3->X3_CAMPO)}) = 0
//	   linha acima nao funciona	
	   If aCpoInibe[1][1] == NIL .Or. aScan(aCpoInibe[1], AllTrim(SX3->X3_CAMPO)) = 0
			AAdd(aCposEnc, SX3->X3_CAMPO)
		EndIf
	EndIf
	SX3->( dbSkip() )
End

Return(aCposEnc)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � PCposBrw �Autor  �Ricardo Berti          � Data � 24/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Trata alguns campos e a seq.a serem exibidos na mBrowse	  ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � PCposBrw() 	    				                          ���
�������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum													  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � ExpA1 = array aCampos = campos prioritarios exibidos		  ���
�������������������������������������������������������������������������Ĵ��
���Uso       � COL_GRA Cooperativa de Graos                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function PCposBrw()

Local aCampos := {}

dbSelectArea("LJX")
dbSetOrder(1)

AAdd(aCampos,{OemToAnsi(STR0018),"LJX_RETENC"}) //"Nr.Ret/Perda"
AAdd(aCampos,{OemToAnsi(STR0022),"LJX_DTRET" }) //"Data Ret."
AAdd(aCampos,{OemToAnsi(STR0019),{|| IF(LJX->LJX_TIPO=='1',OemToAnsi(STR0020),If(LJX->LJX_TIPO=='2',OemToAnsi(STR0021),OemToAnsi(STR0040))   ) }} ) //"Tipo"###"Perda   "###"Retencao" //"Ganho"
AAdd(aCampos,{OemToAnsi(STR0023),{|| POSICIONE("SA1",1,xFilial("SA1")+LJX->LJX_CLIENT+LJX->LJX_LOJA,"A1_NOME") }} ) //"Cliente/Forn."
AAdd(aCampos,{OemToAnsi(STR0024),"LJX_QTDRET" }) //"Peso Ret/Perda"
AAdd(aCampos,{OemToAnsi(STR0025),"LJX_PRODUT"}) //"Produto"
AAdd(aCampos,{OemToAnsi(STR0026),{|| Left(POSICIONE("SB1",1,xFilial("SB1")+LJX->LJX_PRODUT,"B1_DESC"),30) }} ) //"Descr.Produto"
AAdd(aCampos,{OemToAnsi(STR0027),"LJX_MOTIVO" }) //"Cod.Motivo"
AAdd(aCampos,{OemToAnsi(STR0030),"LJX_VALOR" }) //"Valor Un."
AAdd(aCampos,{OemToAnsi(STR0031),"LJX_DTDIG" }) //"Data Dig."
AAdd(aCampos,{OemToAnsi(STR0032),"LJX_CLIENT"}) //"Cod.Cli/For"
AAdd(aCampos,{OemToAnsi(STR0029),"LJX_LOJA"}) //"Loja"

Return(aCampos)


/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    �AjustaSX3 � Autor � Ricardo Berti		    � Data � 17/03/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao �Complementa validacoes de campos para tabela LJX            ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   � AjustaSX3()                                         		  ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Template COL_GRA	 	                                      ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AJUSTASX3()

Local cAlias  := Alias()
Local aAreaSX3:= GetArea("SX3")

DbSelectArea("SX3")
dbSetOrder(2)  // Campo

If DbSeek("LJX_TIPO")
  	If !("3" $ X3_VALID) .Or. !("3=" $ X3_CBOX)
		RecLock("SX3")
   		If !("3" $ X3_VALID)
			Replace X3_VALID WITH 'Pertence("123")'
 		EndIf
		If !("3=" $ X3_CBOX)
			Replace X3_CBOX    WITH "1=Perda;2=Retencao;3=Ganho",;
  			        X3_CBOXSPA WITH "1=Perdida;2=Retencion;3=Ganancia",;
  			        X3_CBOXENG WITH "1=Loss;2=Withholding;3=Profit"
		EndIf			
		MSUnlock()
	Endif
Endif
If DbSeek("LJX_MOTIVO")
	If !("04" $ X3_VALID) .Or. !("04=" $ X3_CBOX)
		RecLock("SX3")
		If !("4" $ X3_VALID)
			Replace X3_VALID WITH 'Pertence("01#02#03#04#05#99")'
		EndIf
		If !("4=" $ X3_CBOX)
			Replace X3_CBOX    WITH "01=Transporte;02=Dif.Calibragem Balancas;03=Seguro;04=Impureza;05=Umidade;99=Outros",;
  			        X3_CBOXSPA WITH "01=Transporte;02=Dif.Calibr.;03=Seguro;04=Impureza;05=Humedad;99=Otros",;
  			        X3_CBOXENG WITH "01=Transport;02=Calibr.Diff.;03=Insurance;04=Impurity;05=Humidity;99=Other"
		EndIf			
		MSUnlock()
	Endif
Endif
RestArea(aAreaSX3)
DbSelectArea(cAlias)
Return

/*/
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaHelp   � Autor � Ricardo Berti 	   	� Data � 23/03/10 ���
�����������������������������������������������������������������������������Ĵ��
���Descri��o � Ajusta Helps para campos do Template       		     		  ���
�����������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum	  										      	      ���
�����������������������������������������������������������������������������Ĵ��
��� Uso		 � CACMIA16                                                  	  ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Static Function AjustaHelp()

Local aHelpPor :={}
Local aHelpEng :={"  "}
Local aHelpSpa :={"  "}  

aHelpPor	:=	{"N�mero da NF de sa�da relacionada  ",;
				 "a reten��o/perda."}
PutHelp("PLJX_NFSAID",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Data da reten��o/perda."}
PutHelp("PLJX_DTRET",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Determina o tipo da ocorr�ncia:    ",;
				 "1 - Perda                          ",;
				 "2 - Reten��o                       ",;
				 "3 - Ganho"}
PutHelp("PLJX_TIPO",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Determina o motivo que originou a  ",;
				 "ocorr�ncia."}
PutHelp("PLJX_MOTIVO",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Quantidade retida/perdida ou ganha."}
PutHelp("PLJX_QTDRET",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Data da chegada da carga no        ",;
				 "destino."}
PutHelp("PLJX_DTCHEG",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Campo informativo. Informar a      ",;
				 "umidade verificada na chegada do   ",;
				 "gr�o no destino."}
PutHelp("PLJX_PUMCHE",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Nota fiscal de entrada ref. a dev. ",;
				 "ou compra da quantidade retida do  ",;
				 "gr�o."}
PutHelp("PLJX_NFENT",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"N�mero da retencao ou perda.       "}
PutHelp("PLJX_RETENC",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"N�mero de s�rie da NF de sa�da."}
PutHelp("PLJX_SESAID",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"C�digo do cliente ou Fornecedor    ",;
				 "ref. a NF de sa�da."}
PutHelp("PLJX_CLIENT",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Loja do cliente ou Fornecedor ref. ",;
				 "a NF de sa�da."}
PutHelp("PLJX_LOJA",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"C�digo do produto ref. NF de sa�da."}
PutHelp("PLJX_PRODUT",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Descri��o do produto ref. NF sa�da."}
PutHelp("PLJX_DESPRO",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Data da digita��o da reten��o."}
PutHelp("PLJX_DTDIG",aHelpPor,aHelpEng,aHelpSpa,.F.)
 
aHelpPor	:=	{"Situa��o da reten��o: Pendente ou  ",;
				 "baixada."}
PutHelp("PLJX_STATUS",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Valor unit�rio do gr�o na NF de    ",;
				 "sa�da (ref.a 1a.unidade de medida)."}
PutHelp("PLJX_VALOR",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Indica se a TES do item na NF de   ",;
				 "sa�da atualiza poder de terceiros. "}
PutHelp("PLJX_PODER3",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Armaz�m utilizado na NF de sa�da.  "}
PutHelp("PLJX_LOCAL",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"N�mero de s�rie da NF de dev. ou   ",;
				 "compra."}
PutHelp("PLJX_SEENT",aHelpPor,aHelpEng,aHelpSpa,.F.)

aHelpPor	:=	{"Data de emiss�o da NF de dev. ou   ",;
				 "compra."}
PutHelp("PLJX_DTNFE",aHelpPor,aHelpEng,aHelpSpa,.F.)

Return NIL