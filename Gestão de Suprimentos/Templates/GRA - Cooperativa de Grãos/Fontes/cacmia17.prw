#INCLUDE "cacmia17.ch"
/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIA17 � Autor �Ricardo Berti          � Data � 04/04/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Consulta arquivo Saldo de Kits do Produtor                 ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe	 � T_CACMIA17()                             				  ���
�������������������������������������������������������������������������Ĵ��
���Retorno   � Nenhum                                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso		 �COL_GRA - Template Cooperativa de Graos	 				  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Template Function CACMIA17()

CHKTEMPLATE("COL")

aRotina := { { OemToAnsi(STR0001), "AxPesqui" , 0, 1},; //"Pesquisa"
             { OemToAnsi(STR0002), "AxVisual" , 0, 2}} //"Visualizar"

//����������������������������Ŀ
//� Define o cabecalho da tela �
//������������������������������

cCadastro := OemToAnsi(STR0003) //"Saldo de Kits x Produtor"

mBrowse(6, 1,22,75,"LKT") 

Return Nil