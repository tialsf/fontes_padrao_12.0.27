#include "Protheus.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACMIR02 � Autor �MARCO AURELIO (TRT005) � Data � 25/09/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Impressao do Romaneio da Entrada/Saida de Graos            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACMIR02()

aOrd         := {}
cDesc1       := "Este programa tem como objetivo imprimir"
cDesc2       := "Entradas e Saidas de Graos"
cDesc3       := "de acordo com os parametros informados pelo usuario."
cPict        := ""
lEnd         := .F.
lAbortPrint  := .F.
limite       := 40
tamanho      := "P"
nomeprog     := "CACMIR02"
nTipo        := 18  //18 - NORMAL   15 - COMPRIMIDO
aReturn      := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
nLastKey     := 0
Titulo       := ""
nLin         := 80
cbtxt        := Space(10)
cbcont       := 00
CONTFL       := 01
m_pag        := 01
imprime      := .T.
cPerg        := "CAMR02"
cPerg        := ""
wnrel        := cPerg  // Coloque aqui o nome do arquivo usado para impressao em disco
Cabec1       := ""
Cabec2       := ""
cString      := "LBT"
nTpRom       := 0

dbSelectArea(cString)
dbSetOrder(1)

If FunName() <> "CACMIA02"
	ValidPerg()  // por enquanto n�o ser� usada nenhuma pergunta, j� que o registro j� est� posicionado
	
	If !Pergunte(cPerg,.T.)

       RPergMia02() // restaura perguntas do CACMIA02 (tp.roman)

	   Return
	Endif
	  
	nTpRom := MV_PAR06
Else
	nTpRom := MV_PAR01
EndIf

Titulo := "ROMANEIO DE "+Iif(nTpRom==1,"ENTRADA","SAIDA")

dbSelectarea("LBT")

// mad - 10/08/04
If Empty(LBT->LBT_PESO2)
	ApMsgInfo("Romaneio sem a 2a. Pesagem informada. Corrija o romaneio.","Aten��o!")

    RPergMia02() // restaura perguntas do CACMIA02 (tp.roman)

	Return
Endif
//

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho,,.F.)

If nLastKey == 27

    RPergMia02() // restaura perguntas do CACMIA02 (tp.roman)

	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27

    RPergMia02() // restaura perguntas do CACMIA02 (tp.roman)

	Return
Endif

Processa({|| RptProc("Aguarde, Selecionando Dados...")})

RPergMia02() // restaura perguntas do CACMIA02 (tp.roman)

Return

//---------------------------------------------------------------------------------------------------------------//

Static Function RptProc

Local cAux    := ""
Local cTpConv := ""
Local cCalc   := ""

dbSelectarea("LBT")
setprc(0,0)
nlin := 0
@ nlin,00 psay padc(Alltrim(SM0->M0_NOMECOM) + " - ROMANEIO DE "+Iif(nTpRom==1,"ENTRADA","SA�DA"),80)
nLin ++
nLin ++
dbSelectarea("LBT")

SB1->( dbSetOrder(1) )
SB1->( MsSeek(xFilial("SB1")+LBT->LBT_PRODUT) )
cTpConv  := Iif(SB1->B1_TIPCONV == "M", "*", "/")

cNomFor  :=POSICIONE("SA2",1,xFilial("SA2")+LBT->LBT_FORNEC+LBT->LBT_LOJA,"A2_NOME")  //C	40		Nom Produtor	@!
cMunFor  :=POSICIONE("SA2",1,xFilial("SA2")+LBT->LBT_FORNEC+LBT->LBT_LOJA,"A2_MUN")	  //C	40		Nom Produtor	@!
cUFFor   :=POSICIONE("SA2",1,xFilial("SA2")+LBT->LBT_FORNEC+LBT->LBT_LOJA,"A2_EST")	  //C	40		Nom Produtor	@!
cDescProd:=SB1->B1_DESC
cDescOP  :=POSICIONE("SX5",1,xFilial("SX5")+"IO"+LBT->LBT_CODOP,"X5_DESCRI")
cDescarte:=SB1->B1_DESC

@ nLin,00 PSAY "ROMANEIO NUMERO:"+LBT->LBT_Romane
nLin ++

@ nLin,00 PSAY "Cod Forn.   Loja    Nome Fornecedor"
nLin ++
@ nLin,00 PSAY LBT->LBT_FORNEC+space(7)+LBT->LBT_LOJA+space(5)+cNomFor
nLin ++
@ nLin,00 PSAY "Municipio: " + SubStr(cMunFor,1,49) +"  UF: " + cUFFor + "   Nota Produtor:  " + alltrim(LBT->LBT_NFFORN)
nLin ++
@ nLin,00 PSAY REPLICATE("-",80)
nLin++
@ nLin,00 PSAY "Transportador                            Placa    Cidade               Estado"
nLin ++
@ nLin,00 PSAY LBT->LBT_CODTRA+"-"+Left( AllTrim(LBT->LBT_TRANSP), 30)
@ nLin,41 PSAY alltrim(LBT->LBT_PLACA)
@ nLin,50 PSAY alltrim(LBT->LBT_CIDADE)
@ nLin,71 PSAY alltrim(LBT->LBT_ESTADO)
nLin ++
@ nLin,00 PSAY REPLICATE("-",80)
nLin++
@ nLin,00 PSAY "Produto:   " + LBT->LBT_PRODUT+" " + SubStr(cDescProd,1,40)
@ nLin,65 PSAY "SAFRA: " + STR(YEAR(LBT->LBT_DATA),4)
nLin++
@ nLin,00 PSAY "Operacao:  " + LBT->LBT_CODOP + " " + cDescOP
nLin++
@ nLin,00 PSAY "Peso da 1a Pesagem (KG) "
cAux := LTrim(TRANSFORM(LBT->LBT_PESO1,"@E 999,999,999,999"))
@ nLin,25 PSAY Replicate(".", 14 - Len(cAux) )+" "+cAux     
@ nLin,50 PSAY "Hora:"
@ nLin,57 PSAY LBT->LBT_Hora1
nLin++
@ nLin,00 PSAY "Peso da 2a Pesagem (KG) "      
cAux := LTrim(TRANSFORM(LBT->LBT_PESO2,"@E 999,999,999,999"))
@ nLin,25 PSAY Replicate(".", 14 - Len(cAux) )+" "+cAux                                    
@ nLin,50 PSAY "Hora:"
@ nLin,57 PSAY LBT->LBT_Hora2
nLin++
@ nLin,00 PSAY "Peso Bruto (KG) ........."                       
cAux := LTrim(TRANSFORM(LBT->LBT_QTDTOT,"@E 999,999,999,999"))
cCalc:= "LBT->LBT_QTDTOT"+cTpConv+"SB1->B1_CONV"
@ nLin,25 PSAY Replicate(".", 14 - Len(cAux) )+" "+cAux                                    
@ nLin,50 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "Indice de Umidade ........."            
@ nLin,28 PSAY TRANSFORM(LBT->LBT_UMIDAD,"@E 99.99") 
@ nLin,38 PSAY TRANSFORM(LBT->LBT_DESCUM,"@E 99.99")+" %"
@ nLin,46 PSAY Replicate(".", 6)
@ nLin,52 PSAY TRANSFORM(LBT->LBT_DESCUM*(LBT->LBT_QTDTOT/100),"@E 999,999,999")
cCalc:= "(LBT->LBT_DESCUM*(LBT->LBT_QTDTOT/100))"+cTpConv+"SB1->B1_CONV"
@ nLin,65 PSAY "KG"
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "(%) de Impureza ...................."
@ nLin,38 PSAY TRANSFORM(LBT->LBT_IMPURE,"@E 99.99")+" %"
@ nLin,46 PSAY Replicate(".", 6)
@ nLin,52 PSAY TRANSFORM(LBT->LBT_IMPURE*(LBT->LBT_QTDTOT/100),"@E 999,999,999")
cCalc:= "(LBT->LBT_IMPURE*(LBT->LBT_QTDTOT/100))"+cTpConv+"SB1->B1_CONV"
@ nLin,65 PSAY "KG"                                                                         
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "(%) de Outros Descontos ............"
@ nLin,38 PSAY TRANSFORM(LBT->LBT_OUTROS,"@E 99.99")+" %"
@ nLin,46 PSAY Replicate(".", 6)
@ nLin,52 PSAY TRANSFORM(LBT->LBT_OUTROS*(LBT->LBT_QTDTOT/100),"@E 999,999,999")
cCalc:= "(LBT->LBT_OUTROS*(LBT->LBT_QTDTOT/100))"+cTpConv+"SB1->B1_CONV"
@ nLin,65 PSAY "KG"                                                                         
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "(%) Avariados ......................"
@ nLin,38 PSAY TRANSFORM(LBT->LBT_AVARIA,"@! 99.99")+" %"
@ nLin,46 PSAY Replicate(".", 11)
@ nLin,52 PSAY TRANSFORM(LBT->LBT_AVARIA*(LBT->LBT_QTDTOT/100),"@E 999,999,999")
cCalc:= "(LBT->LBT_AVARIA*(LBT->LBT_QTDTOT/100))"+cTpConv+"SB1->B1_CONV"
@ nLin,65 PSAY "KG"                                                                         
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "Total dos descontos ................................"
@ nLin,52 PSAY TRANSFORM(LBT->LBT_TOTDES,"@E 999,999,999")         
cCalc:= "LBT->LBT_TOTDES"+cTpConv+"SB1->B1_CONV"
@ nLin,65 PSAY "KG"                                                                         
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++
@ nLin,00 PSAY "Quantidade Liquida ................................."

If SB1->B1_TRIGO == "S" .AND. EMPTY(LBT->LBT_DESCAR)   
	cAux := TRANSFORM(IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI)+LBT->LBT_KGTRIG,"@E 999,999,999")
	cCalc:= "IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI)+LBT->LBT_KGTRIG"+cTpConv+"SB1->B1_CONV"
Else
	SB1->( MsSeek(xFilial("SB1")+LBT->LBT_PRODUT) )
	cTpConv   := Iif(SB1->B1_TIPCONV == "M", "*", "/")
	cAux := TRANSFORM(IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI),"@E 999,999,999")            
	cCalc:= "IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI)"+cTpConv+"SB1->B1_CONV"
EndIf
@ nLin,52 PSAY cAux
@ nLin,65 PSAY "KG"
@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
nLin++

If SB1->B1_TRIGO == "S"
	@ nLin,00 PSAY "Indice PH .........................."                        
	@ nLin,37 PSAY TRANSFORM(LBT->LBT_PH,"@! 99.999")
	nLin++
	
	If !Empty(LBT->LBT_TRIGUI)
		SB1->( MsSeek(xFilial("SB1")+LBT->LBT_PRODUT) )
		SB1->( MsSeek(xFilial("SB1")+SB1->B1_DSCTRIG) ) 

		cTpConv   := Iif(SB1->B1_TIPCONV == "M", "*", "/")
		cCalc     := "LBT->LBT_KGTRIG"+cTpConv+"SB1->B1_CONV"
	
		@ nLin,00 PSAY "(%) de Triguilho ..................."
		@ nLin,37 PSAY TRANSFORM(LBT->LBT_TRIGUI,"@E 999.99")+" %"
		@ nLin,46 PSAY Replicate(".", 6)
		@ nLin,52 PSAY TRANSFORM(LBT->LBT_KGTRIG,"@E 999,999,999")
		@ nLin,65 PSAY "KG"                                                                 
		@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
		nLin++
	EndIf
EndIf

If LBT->LBT_QTDDES > 0
	SB1->( MsSeek(xFilial("SB1")+LBT->LBT_DESCAR) )
	cTpConv   := Iif(SB1->B1_TIPCONV == "M", "*", "/")
	cCalc     := "LBT->LBT_QTDDES"+cTpConv+"SB1->B1_CONV"
	@ nLin,00 PSAY "Descarte:"+Left(LBT->LBT_DESCAR,10)
	@ nLin,20 PSAY SubStr(cDescarte,1,22)
	@ nLin,43 PSAY TRANSFORM(LBT->LBT_PDESCA,"@E 999.99")+" %"
	@ nLin,54 PSAY TRANSFORM(LBT->LBT_QTDDES,"@E 9,999,999")
	@ nLin,65 PSAY "KG"
	@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
	nLin++
EndIf

If (LBT->LBT_QTDDES > 0) .And. (LBT->LBT_GRAOS > 0)
	SB1->( MsSeek(xFilial("SB1")+LBT->LBT_PRODUT) )

	cTpConv   := Iif(SB1->B1_TIPCONV == "M", "*", "/")
	cCalc     := "LBT->LBT_GRAOS"+cTpConv+"SB1->B1_CONV"

	@ nLin,20 PSAY Iif(Empty(LBT->LBT_DESCAR), " ", "GRAOS APTOS PARA SEMENTE")
	@ nLin,54 PSAY TRANSFORM(LBT->LBT_GRAOS, "@E 9,999,999")
	@ nLin,65 PSAY "KG"
	@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
	nLin++
EndIf

If (LBT->LBT_QTDDES == 0) .And. (LBT->LBT_GRAOS > 0)
	SB1->( MsSeek(xFilial("SB1")+LBT->LBT_PRODUT) )

	cDescProd := SB1->B1_DESC
	cTpConv   := Iif(SB1->B1_TIPCONV == "M", "*", "/")
	cCalc     := "IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI)"+cTpConv+"SB1->B1_CONV"

	@ nLin,00 PSAY LBT->LBT_PRODUT
	@ nLin,20 PSAY SubStr(cDescProd,1,30)
	@ nLin,54 PSAY TRANSFORM(IIf(nTpRom==1,LBT->LBT_QTDREA,LBT->LBT_QTDSAI),"@E 9,999,999")
	@ nLin,65 PSAY "KG"
	@ nLin,69 PSAY "("+LTrim( TRANSFORM(&(cCalc), "@E 999,999,999,999") )+" "+SB1->B1_SEGUM+")"
	nLin++
EndIf

nLin++
@ nLin,00 PSAY REPLICATE("_",80)
nLin++
@ nLin,00 PSAY "Endereco/Entrega:"+SM0->M0_EndEnt
nLin++
@ nLin,40 PSAY Alltrim(SM0->M0_CidEnt)+"-"+SM0->M0_ESTENT+","+DTOC(DDATABASE)
nlin:=nLin+2
@ nLin,05 PSAY "_____________________________      _____________________________"
nLin++
@ nLin,05 PSAY "Fornecedor ou Representante"
@ nLin,40 PSAY Alltrim(SM0->M0_NOMECOM)

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	dbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

Return

/*
Funcion: VALIDPERG
Descricao: Criar as perguntas referentes a este relatorio no SX1
*/

Static Function ValidPerg
Local i := 0
Local j := 0
_sAlias := Alias()
aRegs := {}
dbSelectArea("SX1")
dbSetOrder(1)
cPerg := PADR(cPerg,6)

// Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/Var01/Def01/Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/VaR14/Def04/Cnt04/Var05/Def05/Cnt05
aAdd(aRegs,{cPerg,"01","Data Inicial       ?","","","mv_ch1","D",8,0,0,"G","","mv_par1","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"02","Data Final         ?","","","mv_ch2","D",8,0,0,"G","","mv_par2","","","","","","","","","","","","","",""})
AADD(aRegs,{cPerg,"03","Fornecedor         ?","","","mv_ch3","C",6,0,0,"G","","mv_par3","","","","","","","","","","","","","","","","","","","","","","","","","LBT","","",""})
AADD(aRegs,{cPerg,"04","Loja               ?","","","mv_ch4","C",2,0,0,"G","","mv_par4","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
AADD(aRegs,{cPerg,"05","Tipo Relatorio     ?","","","mv_ch5","N",1,0,0,"C","","mv_par5","Detalhado","","","","","Resumido","","","","","","","","","","","","","","","","","","",""})
AADD(aRegs,{cPerg,"06","Romaneio de        ?","","","mv_ch6","N",1,0,0,"C","","mv_par6","Entrada","Ingreso","Input","","","Saida","Salida","Output","","","","","","","","","","","","","","","","",""})

For i:=1 to Len(aRegs)
	If !dbSeek(cPerg+aRegs[i,2])
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			If j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			Endif
		Next
		MsUnlock()
	Endif
Next

dbSelectArea(_sAlias)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �RPERGMIA02� Autor �RICARDO BERTI          � Data � 26/09/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Restaura conj.perguntas ao sair da impressao do Romaneio,  ���
���          � de Entrada/Saida de Graos, corrigindo erro de TYPE MISMATCH���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RPergMia02()

Pergunte("CACA02",.F.)
Return
