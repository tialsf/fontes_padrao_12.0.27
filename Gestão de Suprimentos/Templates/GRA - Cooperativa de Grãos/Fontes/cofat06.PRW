#include "rwmake.ch" 

User Function Cofat06()

/*
	Programa......:	COFAT06
	Autor.........:	Geraldo E. Franciscon
	Data..........:	29/12/00
	Descricao.....:	Verifica se o Fornecedor esta cadastrado como Cliente
	Mudanca Versao:	** indice ok ** 17/02/2003 10:17
	Alterado      : DAniel em 16/06/2003 pesquisa codigo ja cadastrado e atualizacao de dados
*/

Local cLojaPE	:= ""
Local cLoja		:= ""
Local cCodCli	:= ""
Local aArea		:= GetArea()
Local aAreaSA1	:= SA1->(GetArea())

dbSelectArea( "SA1" )
dbSetOrder(3)
If dbSeek( xFilial("SA1") + M->A2_CGC )
	
	cCodCli	:= SA1->A1_COD
	cLoja	:= SA1->A1_LOJA
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� CACREPCF - Ponto de Entrada utilizado para alterar a loja para replica dos dados  �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	If ExistBlock("CACREPCF")
		RestArea(aAreaSA1)
		cLojaPE := ExecBlock("CACREPCF",.F.,.F.,{2,cCodCli,cLoja})
		dbSelectArea("SA1")
		dbSetOrder(1)
		If ValType(cLojaPE) == "C" .And. !Empty(cLojaPE) .And. MsSeek( xFilial("SA1") + cCodCli + cLojaPE )
			cLoja := cLojaPE
		EndIf
		MsSeek( xFilial("SA1") + cCodCli + cLoja )
	EndIf
	If !_SetAutoMode()
		MsgBox("Este Fornecedor esta' cadastrado como Cliente com o Codigo " + SA1->A1_COD + " e Loja " + cLoja + ".","Consistencia entre Fornecedor X Cliente")
	EndIf

	dbSelectArea( "SA2" )
	dbSetOrder(1)
	MsSeek( xFilial("SA2") + cCodCli + cLoja)
   	If  SA2->A2_COD + SA2->A2_LOJA <> cCodCli + cLoja 
       	M->A2_Cod  := cCodCli
		M->A2_Loja := cLoja
	Endif  
   	M->A2_Nome	 := SA1->A1_Nome
    M->A2_Nreduz := SA1->A1_Nreduz
	M->A2_Pessoa := SA1->A1_Tipo
   	M->A2_Mun    := SA1->A1_Mun
	M->A2_Est    := SA1->A1_Est
	M->A2_Bairro := SA1->A1_Bairro
    M->A2_Cep    := SA1->A1_Cep
	M->A2_endcob := SA1->A1_End
	M->A2_End    := SA1->A1_End
    M->A2_tel    := SA1->A1_Tel
	M->A2_Inscr  := SA1->A1_Inscr
	M->A2_fax    := SA1->A1_Fax
	M->A2_contato := SA1->A1_Contato
	M->A2_Inscrm  := SA1->A1_Inscrm
    M->A2_email   := SA1->A1_Email
	M->A2_Hpage   := SA1->A1_Hpage
	  
Endif

RestArea(aAreaSA1)
RestArea(aArea)
Return( M->A2_CGC )
