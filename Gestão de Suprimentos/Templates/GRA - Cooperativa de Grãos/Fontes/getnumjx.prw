#Include "Protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �GetNumJX  � Autor �Ricardo Berti          � Data � 24/10/05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Determina o numero a ser utilizado pelo Retencao/Perdas    ���
�������������������������������������������������������������������������Ĵ��
���Uso       � CACMIA16 - Template COL-GRA                                ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function GetNumJX()

Local aAreaAtu:= GetArea()
Local aAreaLJX:= {}                   
Local cQuery  := ""
Local cNumMax := ""
Local cNumRet := GetSxeNum("LJX","LJX_RETENC")
Local cMay    := ""

ChkTemplate("COL")

// Caso o SXE e o SXF estejam corrompidos cNumRet estava se repetindo.
cMay := Alltrim(xFilial("LJX"))+cNumRet
FreeUsedCode()                         

// Verifica se o numero fornecido pelo arquivo SXE esta correto ou precisa ser alterado
dbSelectArea("LJX")
aAreaLJX := GetArea()
dbSetOrder(2)                                                                         
dbSeek(xFilial("LJX")+cNumRet,.T.)
If !Eof()
        // Caso o numero esteja incorreto eh necessario verificar qual numero de retencao e' valido.
        cQuery := "SELECT MAX(LJX_RETENC) AS NMAX "
        cQuery += "FROM "+RetSqlName("LJX")+" LJX "
        cQuery += "WHERE LJX.LJX_FILIAL = '"+xFilial("LJX")+"' AND "
        cQuery += "LJX.D_E_L_E_T_ <> '*'"
		cQuery := ChangeQuery(cQuery)

        dbUseArea( .T., "TOPCONN", TCGenQry(,,cQuery), "QLJX", .F., .T.)
		dbGoTop()
	
        cNumMax := QLJX->NMAX
        dbSelectArea("QLJX")
		dbCloseArea()
                                                                                                                    
        // Executa funcao para determinar a numeracao corretado do COD.RETENCAO
        MsAguarde( {|| GetNewNum(@cNumRet,@cNumMax,@cMay)}, "Aguarde...", "Acertando a numeracao para retencoes..." )
EndIf
// Restaura a area original do arquivo LJX
RestArea(aAreaLJX)                     

// Restaura a area original
RestArea(aAreaAtu)

Return(cNumRet)

//--------------------------------------------------------------------------------------------------------------//

Static Function GetNewNum(cNumRet,cNumMax,cMay)

// Caso a numeracao do arquivo SXE nao esteja correta ou o numero ja esteja sendo utilizado por outra
// estacao eh necessario utilizar outro numero.
While (cNumRet <= cNumMax) .Or. !MayIUseCode(cMay)
	ConfirmSx8()                                  
    cNumRet := GetSxeNum("LJX","LJX_RETENC")
	FreeUsedCode()
    cMay := Alltrim(xFilial("LJX"))+cNumRet
End         
	
Return
