#INCLUDE "Protheus.ch"

/*
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    砋pdGRA02  � Autor 砇icardo Berti          � Data � 31.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Atualizacao das tabelas COL_GRA - Controle de Transgenicos 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� Nenhum                                                     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Template COL_GRA                                           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � Nenhum                                                     潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
User Function UpdGRA02() //Para maiores detalhes sobre a utlizacao deste fonte,
					    // leia o boletim "Controle de Transgenicos"
					    
cArqEmp := "SigaMat.Emp"
__cInterNet := Nil

PRIVATE cMessage
PRIVATE aArqUpd	 := {}
PRIVATE aREOPEN	 := {}
PRIVATE oMainWnd 

Set Dele On

lHistorico 	:= MsgYesNo("Deseja efetuar a atualiza玢o do Dicion醨io? Esta rotina deve ser utilizada em modo exclusivo ! Faca um backup dos dicion醨ios e da Base de Dados antes da atualiza玢o para eventuais falhas de atualiza玢o !", "Aten玢o")
lEmpenho	:= .F.
lAtuMnu		:= .F.

DEFINE WINDOW oMainWnd FROM 0,0 TO 01,30 TITLE "Atualiza玢o do Dicion醨io"

ACTIVATE WINDOW oMainWnd ;
	ON INIT If(lHistorico,(Processa({|lEnd| GraProc(@lEnd)},"Processando","Aguarde , processando prepara玢o dos arquivos",.F.) , Final("Atualiza玢o efetuada!")),oMainWnd:End())
	
Return

/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � GRAProc  � Autor 砇icardo Berti          � Data � 31.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao dos arquivos           潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao COL_GRA                                        潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function GRAProc(lEnd)
Local cTexto    := ''
Local cFile     :=""
Local cMask     := "Arquivos Texto (*.TXT) |*.txt|"
Local nRecno    := 0
Local nI        := 0
Local nX        :=0
Local aRecnoSM0 := {}     
Local lOpen     := .F. 

ProcRegua(1)
IncProc("Verificando integridade dos dicion醨ios....")
If ( lOpen := MyOpenSm0Ex() )

	dbSelectArea("SM0")
	dbGotop()
	While !Eof() 
  		If Ascan(aRecnoSM0,{ |x| x[2] == M0_CODIGO}) == 0 //--So adiciona no aRecnoSM0 se a empresa for diferente
			Aadd(aRecnoSM0,{Recno(),M0_CODIGO})
		EndIf			
		dbSkip()
	EndDo	
		
	If lOpen
		For nI := 1 To Len(aRecnoSM0)
			SM0->(dbGoto(aRecnoSM0[nI,1]))
			RpcSetType(2) 
			RpcSetEnv(SM0->M0_CODIGO, SM0->M0_CODFIL)
			nModulo := 2 // MODULO COMPRAS
			lMsFinalAuto := .F.
			cTexto += Replicate("-",128)+CHR(13)+CHR(10)
			cTexto += "Empresa : "+SM0->M0_CODIGO+SM0->M0_NOME+CHR(13)+CHR(10)
			//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪目
			//矨tualiza o dicionario de arquivos (SX2) �
			//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪馁
  			ProcRegua(8)
			IncProc("Analisando Dicionario de Arquivos...")
			cTexto += GRAAtuSX2()
			//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
			//矨tualiza o dicionario de dados (SX3) �
			//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
			IncProc("Analisando Dicionario de Dados...")
			cTexto += GRAAtuSX3()
			//谀哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
			//矨tualiza os gatilhos (SX7)           �
			//滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪�
//			IncProc("Analisando Gatilhos...")
//			GRAAtuSX7()
			//谀哪哪哪哪哪哪哪哪哪哪哪哪目
			//矨tualiza os indices (SIX) �
			//滥哪哪哪哪哪哪哪哪哪哪哪哪馁
			IncProc("Analisando arquivos de 韓dices. "+"Empresa : "+SM0->M0_CODIGO+" Filial : "+SM0->M0_CODFIL+"-"+SM0->M0_NOME) 
			cTexto += GRAAtuSIX()

			__SetX31Mode(.F.)
			For nX := 1 To Len(aArqUpd)
				IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				If Select(aArqUpd[nx])>0
					dbSelecTArea(aArqUpd[nx])
					dbCloseArea()
				EndIf
				X31UpdTable(aArqUpd[nx])
				If __GetX31Error()
					Alert(__GetX31Trace())
					Aviso("Aten玢o!","Ocorreu um erro desconhecido durante a atualiza玢o da tabela : "+ aArqUpd[nx] + ". Verifique a integridade do dicion醨io e da tabela.",{"Continuar"},2)
					cTexto += "Ocorreu um erro desconhecido durante a atualiza玢o da estrutura da tabela : "+aArqUpd[nx] +CHR(13)+CHR(10)
                Else
					IncProc("Atualizando estruturas. Aguarde... ["+aArqUpd[nx]+"]")
				EndIf
				
			Next nX		
		
			U_AtuTransge() //Atualiza os novos campos criados

			RpcClearEnv()
			If !( lOpen := MyOpenSm0Ex() )
				Exit 
			EndIf 
		Next nI 
		   
		If lOpen
			
			cTexto := "Log da atualiza玢o "+CHR(13)+CHR(10)+cTexto
			__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
			DEFINE FONT oFont NAME "Mono AS" SIZE 5,12   //6,15
			DEFINE MSDIALOG oDlg TITLE "Atualiza玢o conclu韉a." From 3,0 to 340,417 PIXEL
			@ 5,5 GET oMemo  VAR cTexto MEMO SIZE 200,145 OF oDlg PIXEL
			oMemo:bRClicked := {||AllwaysTrue()}
			oMemo:oFont:=oFont
			DEFINE SBUTTON  FROM 153,175 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
			DEFINE SBUTTON  FROM 153,145 TYPE 13 ACTION (cFile:=cGetFile(cMask,""),If(cFile="",.t.,MemoWrite(cFile,cTexto))) ENABLE OF oDlg PIXEL //Salva e Apaga //"Salvar Como..."
			ACTIVATE MSDIALOG oDlg CENTER
			
		EndIf 
		
	EndIf
		
EndIf 	

Return(.T.)


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    矨tuTransge � Autor 砇icardo Berti         � Data �04/04/2006潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Atualiza o  campo  criado (B1_TRANSGE)                     潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � UPDTMS48                                                   潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
User Function AtuTransge()

Local cQuery    := ''
Local cAliasQry := GetNextAlias()

If TCCanOpen(RetSQLName("SB1")) //Se nao existir a tabela nao processa a atualizacao
	cQuery := " SELECT B1_TRANSGE, SB1.R_E_C_N_O_ R_E_C_N_O_ "
	cQuery += "   FROM "
	cQuery += RetSQLName("SB1") + " SB1 "
	cQuery += "    WHERE B1_FILIAL = '" + xFilial("SB1") + "' "
	cQuery += "      AND B1_TRANSGE <> '2' AND SB1.D_E_L_E_T_ = ' ' "
	
	cQuery := ChangeQuery(cQuery)
	
	dbUseArea( .T., "TOPCONN", TCGENQRY(,,cQuery),cAliasQry, .F., .T.)
	
	While (cAliasQry)->(!Eof())
		cQuery := " UPDATE " + RetSqlName("SB1") + " SET B1_TRANSGE = '1'"
		cQuery += "   WHERE B1_TRANSGE <> '2' AND R_E_C_N_O_  = '" + AllTrim(Str(R_E_C_N_O_)) + "' "
		TCSqlExec( cQuery ) 
		(cAliasQry)->(DbSkip())
	EndDo
	
	(cAliasQry)->(DbCloseArea())
	
Endif	
	
Return Nil


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矴RAAtuSX3 � Autor 砇icardo Berti          � Data � 18.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao do SX3 - Campos        潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao COL_GRA                                        潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌*/
Static Function GRAAtuSX3()
Local aSX3           := {}
Local aEstrut        := {}
Local i              := 0
Local j              := 0
Local lSX3	         := .F.
Local cTexto         := ''
Local cAlias         := ''
Local cReserv        := ''
Local cReservObrig   := ''
Local cUsado         := ''
Local cUsadoObrig    := ''
Local nI             := 0
Local cOrdem

aEstrut:= { "X3_ARQUIVO","X3_ORDEM"  ,"X3_CAMPO"  ,"X3_TIPO"   ,"X3_TAMANHO","X3_DECIMAL","X3_TITULO" ,"X3_TITSPA" ,"X3_TITENG" ,;
	"X3_DESCRIC","X3_DESCSPA","X3_DESCENG","X3_PICTURE","X3_VALID"  ,"X3_USADO"  ,"X3_RELACAO","X3_F3"     ,"X3_NIVEL"  ,;
	"X3_RESERV" ,"X3_CHECK"  ,"X3_TRIGGER","X3_PROPRI" ,"X3_BROWSE" ,"X3_VISUAL" ,"X3_CONTEXT","X3_OBRIGAT","X3_VLDUSER",;
	"X3_CBOX"   ,"X3_CBOXSPA","X3_CBOXENG","X3_PICTVAR","X3_WHEN"   ,"X3_INIBRW" ,"X3_GRPSXG" ,"X3_FOLDER", "X3_PYME"}

dbSelectArea("SX3")
SX3->(DbSetOrder(2))

//--Pesquisa um campo existente para gravar o Reserv e o Usado
	If SX3->(MsSeek("LBT_PRODUT")) //Este campo e obrigatorio e permite alterar
		For nI := 1 To SX3->(FCount())
			If "X3_RESERV" $ SX3->(FieldName(nI))
				cReservObrig := SX3->(FieldGet(FieldPos(FieldName(nI))))
			EndIf
			If "X3_USADO"  $ SX3->(FieldName(nI))
				cUsadoObrig  := SX3->(FieldGet(FieldPos(FieldName(nI))))
			EndIf
		Next 							
	EndIf		
If SX3->(MsSeek("LBT_DESPRO")) //Este campo e somente visualizacao
	For nI := 1 To SX3->(FCount())
		If "X3_RESERV" $ SX3->(FieldName(nI))
			cReserv := SX3->(FieldGet(FieldPos(FieldName(nI))))
		EndIf
		If "X3_USADO"  $ SX3->(FieldName(nI))
			cUsado  := SX3->(FieldGet(FieldPos(FieldName(nI))))
		EndIf
	Next
EndIf

//Criacao de novos campos em uma tabela nova, nao precisa verificar ordem
		Aadd(aSX3,{"LKT",;			//Arquivo
			"01",;						//Ordem
			"LKT_FILIAL",;				//Campo
			"C",;						   //Tipo
			2,;						   //Tamanho
			0,;							//Decimal
			"Filial",;			      //Titulo
			"Sucursal",;			   //Titulo SPA
			"Branch",;			      //Titulo ENG
			"Filial do Sistema",;	//Descricao
			"Sucursal",;				//Descricao SPA
			"Branch of the system.",;		//Descricao ENG
			"@!",;					   //Picture
			"",;							//VALID
			"",;							//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			"",;							//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
			"",;							//VISUAL
			"",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;							//CBOX
			"",;							//CBOX SPA
			"",;							//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME
			
		Aadd(aSX3,{"LKT",;			//Arquivo
			"02",;						//Ordem
			"LKT_DOC",;				//Campo
			"C",;						   //Tipo
			6,;						   //Tamanho
			0,;							//Decimal
			"Docto.      ",;		      //Titulo
			"Docto.      ",;		      //Titulo SPA
			"Docto.      ",;		      //Titulo ENG
			"No.NF Entrada",;		      //Descricao
			"No.NF Entrada",;		      //Descricao SPA
			"No.NF Entrada",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"03",;						//Ordem
			"LKT_SERIE",;				//Campo
			"C",;						   //Tipo
			3,;						   //Tamanho
			0,;							//Decimal
			"Serie       ",;		      //Titulo
			"Serie       ",;		      //Titulo SPA
			"Serie       ",;		      //Titulo ENG
			"Serie NF Entrada",;		      //Descricao
			"Serie NF Entrada",;		      //Descricao SPA
			"Serie NF Entrada",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"04",;						//Ordem
			"LKT_FORNEC",;				//Campo
			"C",;						   //Tipo
			6,;						   //Tamanho
			0,;							//Decimal
			"Fornecedor  ",;		      //Titulo
			"Fornecedor  ",;		      //Titulo SPA
			"Fornecedor  ",;		      //Titulo ENG
			"Cod.Fornecedor  ",;		      //Descricao
			"Cod.Fornecedor  ",;		      //Descricao SPA
			"Cod.Fornecedor  ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"05",;						//Ordem
			"LKT_LOJA",;				//Campo
			"C",;						   //Tipo
			2,;						   //Tamanho
			0,;							//Decimal
			"Loja        ",;		      //Titulo
			"Loja        ",;		      //Titulo SPA
			"Loja        ",;		      //Titulo ENG
			"Loja        ",;		      //Descricao
			"Loja        ",;		      //Descricao SPA
			"Loja        ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"06",;						//Ordem
			"LKT_ROMANE",;				//Campo
			"C",;						   //Tipo
			8,;						   //Tamanho
			0,;							//Decimal
			"Romaneio    ",;		      //Titulo
			"Romaneio    ",;		      //Titulo SPA
			"Romaneio    ",;		      //Titulo ENG
			"Romaneio    ",;		      //Descricao
			"Romaneio    ",;		      //Descricao SPA
			"Romaneio    ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"07",;						//Ordem
			"LKT_PRODUT",;				//Campo
			"C",;						   //Tipo
			15,;						   //Tamanho
			0,;							//Decimal
			"Produto     ",;		      //Titulo
			"Produto     ",;		      //Titulo SPA
			"Produto     ",;		      //Titulo ENG
			"Cod.Produto     ",;		      //Descricao
			"Cod.Produto     ",;		      //Descricao SPA
			"Cod.Produto     ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;			//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"08",;						//Ordem
			"LKT_ITEM",;			//Campo
			"C",;					   //Tipo
			3,;						   //Tamanho
			0,;							//Decimal
			"Item",;		      //Titulo
			"Item",;		      //Titulo SPA
			"Item",;		      //Titulo ENG
			"Item",;		      //Descricao 
			"Item",;		      //Descricao SPA
			"Item",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"09",;						//Ordem
			"LKT_QKITAC",;			//Campo
			"N",;					   //Tipo
			3,;						   //Tamanho
			0,;							//Decimal
			"Q.Kits a Cob",;		      //Titulo
			"Q.Kits a Cob",;		      //Titulo SPA
			"Q.Kits a Cob",;		      //Titulo ENG
			"Qtde.Kits a Cobrar     ",;		//Descricao 
			"Qtde.Kits a Cobrar     ",;		//Descricao SPA
			"Qtde.Kits a Cobrar     ",;		//Descricao ENG
			"@E 999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		Aadd(aSX3,{"LKT",;			//Arquivo
			"10",;						//Ordem
			"LKT_QKITPG",;			//Campo
			"N",;					   //Tipo
			3,;						   //Tamanho
			0,;							//Decimal
			"Q.Kits Pagos",;		      //Titulo
			"Q.Kits Pagos",;		      //Titulo SPA
			"Q.Kits Pagos",;		      //Titulo ENG
			"Qtde.Kits Pagos        ",;		//Descricao 
			"Qtde.Kits Pagos        ",;		//Descricao SPA
			"Qtde.Kits Pagos        ",;		//Descricao ENG
			"@E 999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"S",;							//BROWSE
			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

//Criacao de novos campos em tabela, verificando ordem 

	cOrdem := NewX3Ord("LBT")
	If !Empty( cOrdem )
		Aadd(aSX3,{"LBT",;			//Arquivo
			cOrdem,;				//Ordem
			"LBT_TPTRAN",;			//Campo
			"C",;					   //Tipo
			1,;						   //Tamanho
			0,;							//Decimal
			"Transgenico",;		      //Titulo
			"Transgenico",;		      //Titulo SPA
			"Transgenico",;		      //Titulo ENG
			"Tipo de Transgenico",;		//Descricao
			"Tipo de Transgenico",;		//Descricao SPA
			"Tipo de Transgenico",;		//Descricao ENG
			"@!",;					   //Picture
			"Pertence('123')",;		//VALID
			cUsadoObrig,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReservObrig,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"1=Nao Transgenico;2=Declarado;3=Testado",;			//CBOX
			"1=Nao Transgenico;2=Declarado;3=Testado",;			//CBOX SPA
			"1=Nao Transgenico;2=Declarado;3=Testado",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"LBT",;			//Arquivo
			cOrdem,;				//Ordem
			"LBT_TOTKIT",;			//Campo
			"N",;					   //Tipo
			3,;						   //Tamanho
			0,;							//Decimal
			"Qtde.Kits Ut",;		      //Titulo
			"Qtde.Kits Ut",;		      //Titulo SPA
			"Qtde.Kits Ut",;		      //Titulo ENG
			"Qt.Kits Utiliz.p/Testes",;		//Descricao 
			"Qt.Kits Utiliz.p/Testes",;		//Descricao SPA
			"Qt.Kits Utiliz.p/Testes",;		//Descricao ENG
			"@E 999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"LBT",;			//Arquivo
			cOrdem,;				//Ordem
			"LBT_PROYAL",;			//Campo
			"N",;					   //Tipo
			5,;						   //Tamanho
			2,;							//Decimal
			"% Royalty   ",;		      //Titulo
			"% Royalty   ",;		      //Titulo SPA
			"% Royalty   ",;		      //Titulo ENG
			"% Royalty   ",;		      //Descricao
			"% Royalty   ",;		      //Descricao SPA
			"% Royalty   ",;		      //Descricao ENG
			"@E 99.99",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	EndIf

	cOrdem := NewX3Ord("SD1")
	Aadd(aSX3,{"SD1",;			//Arquivo
			cOrdem,;				//Ordem
			"D1_SFORNUT",;			//Campo
			"N",;					   //Tipo
			12,;						   //Tamanho
			0,;							//Decimal
			"Sd.For.Tr.Ut",;		      //Titulo 
			"Sd.For.Tr.Ut",;		      //Titulo SPA
			"Sd.For.Tr.Ut",;		      //Titulo ENG
			"Saldo Forn.Transg.Utiliz.",;		      //Descricao
			"Saldo Forn.Transg.Utiliz.",;		      //Descricao SPA
			"Saldo Forn.Transg.Utiliz.",;		      //Descricao ENG
			"@E 999,999,999,999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	cOrdem  := Soma1(cOrdem)
	Aadd(aSX3,{"SD1",;			//Arquivo
			cOrdem,;				//Ordem
			"D1_VROYALT",;			//Campo
			"N",;					   //Tipo
			14,;						   //Tamanho
			2,;							//Decimal
			"Vl.Royalty  ",;		      //Titulo
			"Vl.Royalty  ",;		      //Titulo SPA
			"Vl.Royalty  ",;		      //Titulo ENG
			"Valor Royalty  ",;		      //Descricao 
			"Valor Royalty  ",;		      //Descricao SPA
			"Valor Royalty  ",;		      //Descricao ENG
			"@E 9,999,999,999.99",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME


  	cOrdem := NewX3Ord("SF1")
	Aadd(aSX3,{"SF1",;			//Arquivo
			cOrdem,;				//Ordem
			"F1_FORNTR",;			//Campo
			"C",;					   //Tipo
			6,;						   //Tamanho
			0,;							//Decimal
			"Fornec.Trans",;		      //Titulo
			"Fornec.Trans",;		      //Titulo SPA
			"Fornec.Trans",;		      //Titulo ENG
			"Cod.Forn.de Transgenicos ",;		      //Descricao
			"Cod.Forn.de Transgenicos ",;		      //Descricao SPA
			"Cod.Forn.de Transgenicos ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"SF1",;			//Arquivo
			cOrdem,;				//Ordem
     		"F1_LOJATR",;			//Campo
			"C",;					   //Tipo
			2,;						   //Tamanho
			0,;							//Decimal
			"Loja Forn.Tr",;		      //Titulo
			"Loja Forn.Tr",;		      //Titulo SPA
			"Loja Forn.Tr",;		      //Titulo ENG
			"Loja Fornec.Transgenicos ",;		      //Descricao
			"Loja Fornec.Transgenicos ",;		      //Descricao SPA
			"Loja Fornec.Transgenicos ",;		      //Descricao ENG
			"@!",;					   //Picture
			"",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;		//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	cOrdem  := Soma1(cOrdem)
	Aadd(aSX3,{"SF1",;			//Arquivo
			cOrdem,;				//Ordem
			"F1_SFORNTR",;			//Campo
			"N",;					   //Tipo
			12,;						   //Tamanho
			0,;							//Decimal
			"Sd.Dg.For.Tr",;		      //Titulo 
			"Sd.Dg.For.Tr",;		      //Titulo SPA
			"Sd.Dg.For.Tr",;		      //Titulo ENG
			"Saldo Digit.Forn.Transgen",;		      //Descricao
			"Saldo Digit.Forn.Transgen",;		      //Descricao SPA
			"Saldo Digit.Forn.Transgen",;		      //Descricao ENG
			"@E 999,999,999,999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	cOrdem  := Soma1(cOrdem)
	Aadd(aSX3,{"SF1",;			//Arquivo
			cOrdem,;				//Ordem
			"F1_QTKITPG",;			//Campo
			"N",;					   //Tipo
			6,;						   //Tamanho
			0,;							//Decimal
			"Q.Kits Pagos",;		      //Titulo
			"Q.Kits Pagos",;		      //Titulo SPA
			"Q.Kits Pagos",;		      //Titulo ENG
			"Qtde.Kits Pagos        ",;		//Descricao 
			"Qtde.Kits Pagos        ",;		//Descricao SPA
			"Qtde.Kits Pagos        ",;		//Descricao ENG
			"@E 999,999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	cOrdem  := Soma1(cOrdem)
	Aadd(aSX3,{"SF1",;			//Arquivo
			cOrdem,;				//Ordem
			"F1_VUKITPG",;			//Campo
			"N",;					   //Tipo
			18,;						   //Tamanho
			6,;							//Decimal
			"Vl.Unit.Kit",;		      //Titulo
			"Vl.Unit.Kit",;		      //Titulo SPA
			"Vl.Unit.Kit",;		      //Titulo ENG
			"Valor Unitario Kit",;		      //Descricao 
			"Valor Unitario Kit",;		      //Descricao SPA
			"Valor Unitario Kit",;		      //Descricao ENG
			"@E 999,999,999.999999",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"V",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME


	cOrdem := NewX3Ord("SB1")
	Aadd(aSX3,{"SB1",;			//Arquivo
			cOrdem,;				//Ordem
			"B1_TRANSGE",;			//Campo
			"C",;					   //Tipo
			1,;						   //Tamanho
			0,;							//Decimal
			"Transgenico ",;		      //Titulo 
			"Transgenico ",;		      //Titulo SPA
			"Transgenico ",;		      //Titulo ENG
			"Transgenico ",;		      //Descricao
			"Transgenico ",;		      //Descricao SPA
			"Transgenico ",;		      //Descricao ENG
			"@!",;					   //Picture
			"Pertence('12')",;		//VALID
			cUsadoobrig,;						//USADO
			"'1'",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReservObrig,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"1=Nao;2=Sim",;			//CBOX
			"1=Nao;2=Sim",;			//CBOX SPA
			"1=Nao;2=Sim",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			"N"})							//PYME

	cOrdem := NewX3Ord("LBU")
	If !Empty( cOrdem )
		Aadd(aSX3,{"LBU",;			//Arquivo
			cOrdem,;				//Ordem
			"LBU_PROYDE",;			//Campo
			"N",;					   //Tipo
			5,;						   //Tamanho
			2,;							//Decimal
			"%Royal.Decl.",;		      //Titulo
			"%Royal.Decl.",;		      //Titulo SPA
			"%Royal.Decl.",;		      //Titulo ENG
			"%Royalty Transg.Declarado",;		      //Descricao
			"%Royalty Transg.Declarado",;		      //Descricao SPA
			"%Royalty Transg.Declarado",;		      //Descricao ENG
			"@E 99.99",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"LBU",;			//Arquivo
			cOrdem,;				//Ordem
			"LBU_PROYTE",;			//Campo
			"N",;					   //Tipo
			5,;						   //Tamanho
			2,;							//Decimal
			"%Royal.Test.",;		      //Titulo
			"%Royal.Test.",;		      //Titulo SPA
			"%Royal.Test.",;		      //Titulo ENG
			"%Royalty Transg.Testado  ",;		      //Descricao
			"%Royalty Transg.Testado  ",;		      //Descricao SPA
			"%Royalty Transg.Testado  ",;		      //Descricao ENG
			"@E 99.99",;					   //Picture
			"Positivo()",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME


		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"LBU",;			//Arquivo
			cOrdem,;				//Ordem
			"LBU_FORNTR",;			//Campo
			"C",;					   //Tipo
			6,;						   //Tamanho
			0,;							//Decimal
			"Fornec.Trans",;		      //Titulo
			"Fornec.Trans",;		      //Titulo SPA
			"Fornec.Trans",;		      //Titulo ENG
			"Cod.Forn.de Transgenicos ",;		      //Descricao
			"Cod.Forn.de Transgenicos ",;		      //Descricao SPA
			"Cod.Forn.de Transgenicos ",;		      //Descricao ENG
			"@!",;					   //Picture
			"Vazio().Or.ExistCpo('SA2',M->LBU_FORNTR)",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"SA2",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"",;							//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME

		cOrdem  := Soma1(cOrdem)
		Aadd(aSX3,{"LBU",;			//Arquivo
			cOrdem,;				//Ordem
     		"LBU_LOJATR",;			//Campo
			"C",;					   //Tipo
			2,;						   //Tamanho
			0,;							//Decimal
			"Loja        ",;		      //Titulo
			"Tienda      ",;		      //Titulo SPA
			"Unit        ",;		      //Titulo ENG
			"Loja                     ",;		      //Descricao
			"Tienda             	  ",;		      //Descricao SPA
			"Unit                     ",;		      //Descricao ENG
			"@!",;					   //Picture
			"If(Empty(M->LBU_FORNTR),Vazio(),ExistCpo('SA2',M->LBU_FORNTR+M->LBU_LOJATR))",;		//VALID
			cUsado,;						//USADO
			"",;							//RELACAO
			"",;							//F3
			1,;							//NIVEL
			cReserv,;					//RESERV
			"",;							//CHECK
			"",;							//TRIGGER
			"T",;							//PROPRI
			"N",;							//BROWSE
  			"A",;							//VISUAL
			"R",;							//CONTEXT
			"",;							//OBRIGAT
			"",;							//VLDUSER
			"",;			//CBOX
			"",;			//CBOX SPA
			"",;			//CBOX ENG
			"",;							//PICTVAR
			"!Empty(M->LBU_FORNTR)",;		//WHEN
			"",;							//INIBRW
			"",;							//SXG
			"",;							//FOLDER			
			""})							//PYME


		EndIf


ProcRegua(Len(aSX3))

SX3->(DbSetOrder(2))	

For i:= 1 To Len(aSX3)
	If !Empty(aSX3[i][1])
		If !dbSeek(aSX3[i,3])
			lSX3	:= .T.
			If !(aSX3[i,1]$cAlias)
				cAlias += aSX3[i,1]+"/"
				aAdd(aArqUpd,aSX3[i,1])
			EndIf
			RecLock("SX3",.T.)
			For j:=1 To Len(aSX3[i])
				If FieldPos(aEstrut[j])>0 .And. aSX3[i,j] != NIL
					FieldPut(FieldPos(aEstrut[j]),aSX3[i,j])
				EndIf
			Next j
			dbCommit()
			MsUnLock()
			IncProc("Atualizando Dicion醨io de Dados...") //
		Endif
	EndIf
Next i

If lSX3
	cTexto := 'Foram alteradas as estruturas das seguintes tabelas : '+cAlias+CHR(13)+CHR(10)
EndIf

Return cTexto


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矴RAAtuSX2 � Autor 砇icardo Berti          � Data � 18.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao do SX2 - Arquivos      潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao COL_GRA                                        潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function GRAAtuSX2()
Local aSX2   := {}
Local aEstrut:= {}
Local i      := 0
Local j      := 0
Local cTexto := ''
Local lSX2	 := .F.
Local cAlias := ''
Local cPath
Local cNome

aEstrut:= {"X2_CHAVE","X2_PATH","X2_ARQUIVO","X2_NOME","X2_NOMESPA","X2_NOMEENG","X2_DELET","X2_MODO","X2_TTS","X2_ROTINA","X2_PYME"}

ProcRegua(Len(aSX2))

dbSelectArea("SX2")
SX2->(DbSetOrder(1))	
MsSeek("LBT")
cPath := SX2->X2_PATH
cNome := Substr(SX2->X2_ARQUIVO,4,5)

	aadd(aSX2,{	"LKT",; 								//Chave
		cPath,;											//Path
		"LKT"+cNome,;									//Nome do Arquivo
		"Saldo Kits / Produtor",;		//Nome Port
		"Saldo Kits / Produtor",;		//Nome Esp
		"Saldo Kits / Produtor",;		//Nome Ing
		0,;												//Delete
		"C",;												//Modo - (C)Compartilhado ou (E)Exclusivo
		"",;												//TTS
		"",;												//Rotina
		"N"})												//Pyme
		
For i:= 1 To Len(aSX2)
	If !Empty(aSX2[i][1])
		If !MsSeek(aSX2[i,1])
			lSX2	:= .T.
			If !(aSX2[i,1]$cAlias)
				cAlias += aSX2[i,1]+"/"
			EndIf
			RecLock("SX2",.T.)
			For j:=1 To Len(aSX2[i])
				If FieldPos(aEstrut[j]) > 0
					FieldPut(FieldPos(aEstrut[j]),aSX2[i,j])
				EndIf
			Next j
			SX2->X2_PATH := cPath
			SX2->X2_ARQUIVO := aSX2[i,1]+cNome
			dbCommit()
			MsUnLock()
			IncProc("Atualizando Dicionario de Arquivos...") //
		EndIf
	EndIf
Next i

Return cTexto


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    矴RAAtuSIX � Autor 砇icardo Berti          � Data � 31.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Funcao de processamento da gravacao do SIX - Indices       潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao GRA                                            潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function GRAAtuSIX()
//INDICE ORDEM CHAVE DESCRICAO DESCSPA DESCENG PROPRI F3 NICKNAME SHOWPESQ
Local cTexto    := ''
Local lSix      := .F.
Local aSix      := {}
Local aEstrut   := {}
Local i         := 0
Local j         := 0
Local cAlias    := ''
Local lDelInd   := .F.

aEstrut:= {"INDICE","ORDEM","CHAVE","DESCRICAO","DESCSPA","DESCENG","PROPRI","F3","NICKNAME","SHOWPESQ"}

	aadd(aSIX,{"LKT",;   												//Indice
		"1",;                 											//Ordem
		"LKT_FILIAL+LKT_DOC+LKT_SERIE+LKT_FORNEC+LKT_LOJA+LKT_PRODUT+LKT_ROMANE+LKT_ITEM",;  //Chave
		"Docto. + Serie + Fornecedor + Loja + Produto + Romaneio + Item",;  		//Descricao Port.
		"Docto. + Serie + Fornecedor + Loja + Produto + Romaneio + Item",;  		//Descricao SPA
		"Docto. + Serie + Fornecedor + Loja + Produto + Romaneio + Item",;  		//Descricao ENG
		"T",; 															//Proprietario
		"",;  															//F3
		"",;  															//NickName
		"S"}) 															//ShowPesq

	aadd(aSIX,{"LKT",;   												//Indice
		"2",;                 											//Ordem
		"LKT_FILIAL+LKT_FORNEC+LKT_LOJA+LKT_ROMANE+LKT_ITEM",;  //Chave
		"Fornecedor + Loja + Romaneio + Item",;  		//Descricao Port.
		"Fornecedor + Loja + Romaneio + Item",;  		//Descricao SPA
		"Fornecedor + Loja + Romaneio + Item",;  		//Descricao ENG
		"T",; 															//Proprietario
		"",;  															//F3
		"",;  															//NickName
		"S"}) 															//ShowPesq
		
ProcRegua(Len(aSIX))

dbSelectArea("SIX")
SIX->(DbSetOrder(1))

For i:= 1 To Len(aSIX)
	If !Empty(aSIX[i,1])
		If !MsSeek(aSIX[i,1]+aSIX[i,2])
			RecLock("SIX",.T.)
			lDelInd := .F.
		Else
			RecLock("SIX",.F.)
			lDelInd := .T. //Se for alteracao precisa apagar o indice do banco
		EndIf
		
		If UPPER(AllTrim(CHAVE)) != UPPER(Alltrim(aSIX[i,3]))
			aAdd(aArqUpd,aSIX[i,1])
			lSix := .T.
			If !(aSIX[i,1]$cAlias)
				cAlias += aSIX[i,1]+"/"
			EndIf
			For j:=1 To Len(aSIX[i])
				If FieldPos(aEstrut[j])>0
					FieldPut(FieldPos(aEstrut[j]),aSIX[i,j])
				EndIf
			Next j
			dbCommit()        
			MsUnLock()
			cTexto  += (aSix[i][1] + " - " + aSix[i][3] + Chr(13) + Chr(10))
			If lDelInd
				TcInternal(60,RetSqlName(aSix[i,1]) + "|" + RetSqlName(aSix[i,1]) + aSix[i,2]) //Exclui sem precisar baixar o TOP
			Endif	
		EndIf
		IncProc("Atualizando 韓dices...")
	EndIf
Next i

If lSix
	cTexto += "蚽dices atualizados  : "+cAlias+CHR(13)+CHR(10)
EndIf

Return cTexto


/*苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘苘�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪穆哪哪哪穆哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    矼yOpenSM0Ex� Autor 砈ergio Silveira       � Data �07/01/2003潮�
北媚哪哪哪哪呐哪哪哪哪哪牧哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Efetua a abertura do SM0 exclusivo                         潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Atualizacao FIS                                            潮�
北滥哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�*/
Static Function MyOpenSM0Ex()

Local lOpen := .F. 
Local nLoop := 0 

For nLoop := 1 To 20
	dbUseArea( .T.,, "SIGAMAT.EMP", "SM0", .F., .F. ) 
	If !Empty( Select( "SM0" ) ) 
		lOpen := .T. 
		dbSetIndex("SIGAMAT.IND") 
		Exit	
	EndIf
	Sleep( 500 ) 
Next nLoop 

If !lOpen
	Aviso( "Atencao !", "Nao foi possivel a abertura da tabela de empresas de forma exclusiva !", { "Ok" }, 2 ) 
EndIf                                 

Return( lOpen ) 

/*
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噭o    � NewX3Ord � Autor 砇icardo Berti          � Data � 31.03.06 潮�
北媚哪哪哪哪呐哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噭o � Determina o no.da prox.ordem no SX3, p/ uso quando incluir 潮�
北�          � um novo campo numa tabela                                  潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砈intaxe   � ExpN1 := NewX3Ord(ExpC1)                                   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砅arametros� ExpC1 = Alias do arquivo                                   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砇etorno   � ExpN1 = No.da ordem a utilizar em novo campo               潮�
北�          �         Obs.: Se nao existir a tabela, retorna em branco   潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北� Uso      � Geral                                                      潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌�
*/
Static Function NewX3Ord(cAlias)

Local cOrdem := " "
aAreaSX3	 := GetArea("SX3")
aArea		 := Alias()

SX3->(DbSetOrder(1))	
SX3->(MsSeek(cAlias))
If SX3->(!Eof()) .And. SX3->X3_ARQUIVO == cAlias
	Do While SX3->(!Eof()) .And. SX3->X3_ARQUIVO == cAlias
		SX3->(dbSkip())
	EndDo
	SX3->(dbSkip(-1))
	cOrdem  := Soma1(SX3->X3_ORDEM)
EndIf
RestArea(aAreaSX3)
dbSelectArea(aArea)

Return(cOrdem)

// criar parametro MV_PROKIT