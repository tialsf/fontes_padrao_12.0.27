#INCLUDE "caccor08.ch"
#include "Protheus.ch"
#include "TopConn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACCOR08 � Autor � Marco Aurelio TRT005  � Data � 25/09/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Relatorio de FUNRURAL                                      ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos e Leite                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACCOR08()

CHKTEMPLATE("COL")

nTVALORR    := 0
nTFunrural  := 0 

nqtd        := 0 
aOrd        := {}
cDesc1      := STR0001 //"Este programa tem como objetivo imprimir relatorio "
cDesc2      := STR0002 //"de acordo com os parametros informados pelo usuario."
cDesc3      := STR0003 //"Relatorio de Funrural"
cPict       := ""
lEnd        := .F.
lAbortPrint := .F.
limite      := 80
tamanho     := "P"
nomeprog    := "CACCOR08"
nTipo       := 18  //COMPRIMIDO 
aReturn     := { STR0004, 1, STR0005, 1, 2, 1, "", 1} //"Zebrado"###"Administracao"
nLastKey    := 0
cPerg       := "CACR08"
Titulo      := STR0003 //"Relatorio de Funrural"
nLin        := 80
cbtxt       := Space(10)
cbcont      := 00
CONTFL      := 01
m_pag       := 01
imprime     := .T.
wnrel       := "CACR08" // Coloque aqui o nome do arquivo usado para impressao em disco
cString     := "SF1"

dbSelectArea(cString)
dbSetOrder(1)

ValidPerg() 

if !Pergunte(cPerg,.t.)
   Return
Endif        

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
   Return
Endif

Processa({|| RptProc(STR0006)}) //"Processando Filtro"
return

Static function RptProc

cQuery := "SELECT"
cQuery += "  LBB.LBB_CODFOR, LBB.LBB_NOMFOR, SUM(SF1.F1_INSS) FUNRURAL, SUM(SF1.F1_BASEINS) BASE"
cQuery += " FROM"
cQuery += " " + RetSqlName("SF1") + " SF1,"
cQuery += " " + RetSqlName("LBB") + " LBB"
cQuery += " WHERE"
cQuery += " SF1.F1_FORNECE = LBB.LBB_CODFOR AND"
cQuery += " SF1.F1_LOJA = LBB.LBB_LOJA AND"
cQuery += " SF1.F1_EMISSAO BETWEEN '" +Dtos(mv_par01)+ "' AND '" +Dtos(mv_par02) + "' AND"
IF( AllTrim(MV_PAR03) <> "" , cQuery +=  "  LBB.LBB_EST = '" +ALLTRIM(mv_par03) + "' AND",)
cQuery += " LBB.D_E_L_E_T_ = ' ' AND SF1.D_E_L_E_T_ = ' '"
cQuery += " GROUP BY"
cQuery += "    LBB.LBB_CODFOR,LBB.LBB_NOMFOR"
cQuery += " ORDER BY"
cQuery += "    LBB.LBB_NOMFOR ASC"

TCQUERY cQuery ALIAS RQRY NEW

cabec1:=IF( ALLTRIM(MV_PAR03) <> "", STR0007 + MV_PAR03+SPACE(39),SPACE(52)) ;  //"Estado..: "
              +STR0008+dtoc(mv_par01)+ STR0009 +dtoc(mv_par02) + "" //"Per�odo "###" a "
                                                               
cabec2:=STR0010 //"Produtor                                        Base                 Funrural"
      // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
      //										 9,999,999.99             9,999,999.99
_wfim:=RecCount() 

ProcRegua(_wfim)    

while !EOF() 

   IncProc(STR0011)  	 //"Gerando Relat�rio... "

   If lAbortPrint
      @nLin,00 PSAY STR0012 //"*** CANCELADO PELO OPERADOR ***"
      Exit
   Endif

   if nLin>60 
 	  Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)   
 	  nLin:=9
   endif

	@nLin,00 PSAY LBB_NOMFOR
	@nLin,40 PSAY TRANSFORM(BASE,"@E 9,999,999.99") 
	@nLin,65 PSAY TRANSFORM(FUNRURAL  ,"@E 9,999,999.99")  


    nTFunrural += FUNRURAL
    
    nTVALORR  += BASE
    
   	dbskip()
	
	nLin++  //;	nQtd++

  
EndDo

nLin++  ; nLin++
               
@nLin,00 PSAY STR0013 //"Totais -------------------------------> "
@nLin,40 PSAY TRANSFORM(nTVALORR,"@E 9,999,999.99")
@nLin,65 PSAY TRANSFORM(nTFunrural,"@E 9,999,999.99") 

RQRY->(DBCLOSEAREA())  //Fecha o Alias Temporario

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
   dbCommitAll()
   SET PRINTER TO
   OurSpool(wnrel)
Endif

MS_FLUSH()

Return

/* 
	Funcion: VALIDPERG   
	Descricao: Criar as perguntas referentes a este relatorio no SX1
*/

Static Function ValidPerg
Local i, j
Local _sAlias	:= Alias()
Local aRegs		:= {}
Local nTamSX1   := Len(SX1->X1_GRUPO)

dbSelectArea("SX1")
dbSetOrder(1)
If MsSeek(PADR(cPerg,nTamSX1)+"01") .And. Empty(X1_PERSPA)
	While !Eof() .And. Trim(X1_GRUPO) == cPerg
		RecLock("SX1",.F.)
		dbDelete()
		MsUnLock()
		dbSkip()
	EndDo		
EndI
// Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/Var01/Def01/Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/VaR08/Def04/Cnt04/Var05/Def05/Cnt05

aAdd(aRegs,{cPerg,"01","Data Inicial","Data Inicial","Data Inicial","mv_ch1","D",8,0,0,"G","","mv_par01","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"02","Data Final","","","mv_ch2","D",8,0,0,"G","","mv_par02","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"03","Estado","","","mv_ch3","C",2,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","12","","",""})
For i:=1 to Len(aRegs)
	If !MsSeek(PADR(cPerg,nTamSX1)+aRegs[i,2])
        RecLock("SX1",.T.)
        For j:=1 to FCount()
            If j <= Len(aRegs[i])
                FieldPut(j,aRegs[i,j])
            Endif
        Next
        MsUnlock()
    Endif
Next

dbSelectArea(_sAlias)
Return