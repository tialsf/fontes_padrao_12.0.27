#INCLUDE "caccor22.ch"
#include "Protheus.ch"
#include "TopConn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    � CACCOR22 � Autor � Microsiga SJRP        � Data � 28/02/01 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Relatorio de Tanques                                       ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Cooperativa de Graos e Leite                               ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Template Function CACCOR22()

CHKTEMPLATE("COL")

nqtd        :=0  //CONTADOR PARA TOTALIZAR OS REGISTROS IMPRESSOS
aOrd        := {}
CbTxt       := ""
cDesc1      := STR0001 //"Este programa tem como objetivo imprimir relatorio "
cDesc2      := STR0002 //"de acordo com os parametros informados pelo usuario."
cDesc3      := STR0003 //"Relatorio de Proprietario por Tanque"
cPict       := ""
lEnd        := .F.
lAbortPrint := .F.
limite      := 80
tamanho     := "P"
nomeprog    := "CACCOR22"
nTipo       := 18
aReturn     := { STR0004, 1, STR0005, 1, 2, 1, "", 1} //"Zebrado"###"Administracao"
nLastKey    := 0
cPerg       := ""
//cPerg       := "CACR22"
Titulo      := STR0006 //"Relacao de Proprietarios por Tanque"
nLin        := 80
cbtxt       := Space(10)
cbcont      := 00
CONTFL      := 01
m_pag       := 01
imprime     := .T.
wnrel       := "CACR22" // Coloque aqui o nome do arquivo usado para impressao em disco
cString     := "SA2"

dbSelectArea(cString)
dbSetOrder(1)

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
   Return
Endif

Processa({|| RptProc(STR0007)}) //"Processando Filtro"
return

static function RptProc

cQuery:="SELECT"
cQuery += " LBF.LBF_CODTAN, LBF.LBF_NOMFOR, LBB.LBB_CODPRO, LBB.LBB_DESC, LBB.LBB_NOMFOR, LBB.LBB_TANQCO"
cQuery += " FROM "+RetSqlName("LBB")+" LBB,"+RetSqlName("LBF")+" LBF" 
cQuery += " WHERE"
cQuery += "    LBB.LBB_CODTAN = LBF.LBF_CODTAN AND"
cQuery += "    LBB.LBB_TANQCO = 'S' AND"
cQuery += "    LBB.D_E_L_E_T_ = ' ' AND LBF.D_E_L_E_T_ = ' '"
cQuery += " ORDER BY"
cQuery += "    LBF.LBF_CODTAN ASC"
    
    
TCQUERY cQuery ALIAS RQRY NEW

cabec1:=""
cabec2:=STR0008 //"   CodProp     Propriedade                  Proprietario "

cTanque:=LBF_CODTAN

_wfim:=RecCount() ; ProcRegua(_wfim)    
while !EOF() 

   IncProc(STR0009)  	 //"Gerando Relat�rio... "

   If lAbortPrint
      @nLin,00 PSAY STR0010 //"*** CANCELADO PELO OPERADOR ***"
      Exit
   Endif

   if nLin>50 .or. LBF_CODTAN<>cTanque
      Cabec1:=STR0011+LBF_CODTAN+STR0012+LBF_NOMFOR //"Tanque "###"    Proprietario "
 	  Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)   
   	  cTanque:=LBF_CODTAN
 	  nLin:=9
   endif

	@nLin,03 PSAY LBB_CODPRO 
	@nLin,15 PSAY alltrim(LBB_DESC)
	@nLin,44 PSAY alltrim(LBB_NOMFOR)
	
	dbskip()
	
	nLin++  //;	nQtd++
   
EndDo

RQRY->(DBCLOSEAREA())

//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
   dbCommitAll()
   SET PRINTER TO
   OurSpool(wnrel)
Endif

MS_FLUSH()

Return
