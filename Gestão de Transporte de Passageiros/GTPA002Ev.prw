#include 'Protheus.ch'
#include 'FWMVCDef.ch' 
#include 'GTPA002.CH'

/*/{Protheus.doc} GTPA002EV
// GTPA002EV
Eventos do MVC relacionados a integra��o de trechos e tarifas do modulo GTP.

Todas as valida��es de modelo, linha, pr� e pos, tamb�m todas as intera��es com a grava��o
s�o definidas nessa classe.

Importante: Use somente a fun��o Help para exibir mensagens ao usuario, pois apenas o help
� tratado pelo MVC. 

@author jacomo.fernandes
@since 23/02/2017
@version P12.1.17
@see FWModelEvent - Documenta��o sobre eventos do MVC
@link http://tdn.totvs.com/pages/viewpage.action?pageId=269552294
@type class
/*/
CLASS GTPA002EV FROM FWModelEvent
	
	DATA oMdl003	as object //Model do GTPA003 -- Trechos e Tarifas
	
	DATA aDelete	as array
	
	METHOD New() CONSTRUCTOR	
	METHOD Destroy()
	METHOD InTTS()
	METHOD ModelPosVld()
	
ENDCLASS

//-----------------------------------------------------------------
/*/{Protheus.doc} New
// New - Classe contrutora
@author jacomo.fernandes
@since 23/02/2017
@version P12.1.17

@type method
/*/
METHOD New() CLASS GTPA002EV
	::aDelete	:= {}
Return
//-----------------------------------------------------------------
/*/{Protheus.doc} Destroy
//Destroy - Quando o modelo do cadastro de Linhas � destruido, destroi tamb�m o modelo do Trechos e tarifas.
Tratamento realizado para n�o deixar lixo na mem�ria.

@author jacomo.fernandes
@since 23/02/2017
@version P12.1.17

@type method
/*/
METHOD Destroy() CLASS GTPA002EV
	
	aSize(::aDelete,0)
	::aDelete := Nil
	
	If ValType(::oMdl003) == "O"
		::oMdl003:DeActivate()
		::oMdl003:Destroy()
	Endif
	
Return
//-----------------------------------------------------------------
/*/{Protheus.doc} InTTS
// InTTS - M�todo que � chamado pelo MVC quando ocorrer as a��es do commit Ap�s as grava��es por�m 
antes do final da transa��o
@author jacomo.fernandes
@since 23/02/2017
@version P12.1.17
@param oModel, object, modelo principal
@param cModelId, characters, Id do SubModel
@type method
/*/
METHOD InTTS(oModel, cModelId) CLASS GTPA002EV
Local lRet	:= .F.	
If ValType(::oMdl003) == "O" 
	If ::oMdl003:IsActive()
		lRet := .T.
	Else
		lRet := ::oMdl003:Activate()
	Endif
Endif

If lRet
	::oMdl003:CommitData()
Endif

FwModelActive(oModel)
Return
//-----------------------------------------------------------------
/*/{Protheus.doc} ModelPosVld
// ModelPosVld - M�todo que � chamado pelo MVC quando ocorrer as a��es de pos valida��o do Model
@author jacomo.fernandes
@since 23/02/2017
@version P12.1.17
@param oModel, object, modelo principal
@param cModelId, characters, Id do SubModel
@type method
/*/
METHOD ModelPosVld(oModel, cModelId) CLASS GTPA002EV
Local aArea		:= GetArea()
Local oMdlGI2	:= oModel:GetModel('FIELDGI2')
Local oMdlG5I	:= oModel:GetModel('GRIDG5I')
Local oMdlGI3	:= nil
Local oMdlGI4	:= nil
Local lRet		:= .T.
Local n1		:= 0
Local n2		:= 0
Local nItem		:= 1
Local nOpc		:= oModel:GetOperation()
Local lDelete	:= nOpc == MODEL_OPERATION_DELETE
Local cLinha	:= oMdlGI2:GetValue('GI2_COD')
Local cVia		:= oMdlGI2:GetValue('GI2_VIA')
Local cOrgao	:= oMdlGI2:GetValue('GI2_ORGAO')
Local cStatus	:= oMdlGI2:GetValue('GI2_MSBLQL')
Local cRev		:= "000"
Local cLocOri	:= ""
Local cLocDes	:= ""
Local lOk	:= .T.
Local nX	:= 0
Local nTamItem	:= TamSx3("GI4_ITEM")[1] 
Local cAliasGI3	:= GetNextAlias()
Local cAliasGI4	:= GetNextAlias()
Local cDeletado	:= oMdlGI2:GetValue('GI2_DEL')
Local aTxGI4	:= {}
Local nA		:= 1
Local nPos		:= 0


GI3->(DBOrderNickname('GI3REVISA'))//GI3_FILIAL+GI3_LINHA+GI3_REVISA
GI4->(DbSetOrder(3))
If (nOpc == MODEL_OPERATION_INSERT .Or. nOpc == MODEL_OPERATION_UPDATE) .And. oMdlGI2:GetValue('GI2_REVISA') != '000'
	If IsBlind() .or. MsgYesNo(STR0032 + CRLF +; //"A altera��o dos trechos ir� impactar nos seguintes cadastros: "
							STR0033 + CRLF + ; //"1. Se��es"
							STR0034 + CRLF + ; //"2. Hor�rios"
							STR0035 + CRLF + CRLF +; //"3. Viagens"
							STR0036,STR0037) //" Deseja confirmar opera��o?""Aten��o"
	Else
		Help( ,, 'Help',"GTPA002", STR0038, 1, 0 )// "Opera��o Cancelada."
		lRet 	 := .F.
	Endif
Endif

If lRet
	::oMdl003	:= FwLoadModel('GTPA003')
	//Realiza pesquisa da ultima revisao
	BeginSql  Alias cAliasGI3
		
		SELECT GI3_REVISA
			FROM %Table:GI3% GI3
			WHERE GI3.GI3_LINHA =  %Exp:cLinha%
			AND GI3.GI3_HIST = %Exp:'2'%
			AND GI3.%NotDel%
			
	EndSql	
	If (cAliasGI3)->(!Eof())
		cRev	:= (cAliasGI3)->GI3_REVISA
	EndIf
	
	//Verifica se e uma exclus�o sem versionamento
	If !lDelete
		//Verifica se e uma inclus�o ou altera��o com versionamento
		If nOpc == MODEL_OPERATION_INSERT
			::oMdl003:SetOperation(MODEL_OPERATION_INSERT)
			
			If GI3->(DbSeek(xFilial('GI3')+cLinha+(cAliasGI3)->GI3_REVISA)) .And. GI4->(DbSeek(xFilial('GI4')+cLinha+(cAliasGI3)->GI3_REVISA))
				cRev	:= StrZero(Val(cRev)+1,tamsx3('GI3_REVISA')[1])
			EndIf
			
		Else
			//Verifica se e uma exclus�o com versionamento 
			::oMdl003:SetOperation(MODEL_OPERATION_UPDATE)
			
			//Posicionando na ultima revisao ativa
			dbSelectArea("GI3")	
			dbSelectArea("GI4")	
			If (cAliasGI3)->(!Eof()) 
				GI3->(DbSeek(xFilial('GI3')+cLinha+(cAliasGI3)->GI3_REVISA))
				GI4->(DbSeek(xFilial('GI4')+cLinha+(cAliasGI3)->GI3_REVISA))
			EndIf
		EndIF
	//Verifica se existe a linha 
	ElseIf GI3->(DbSeek(xFilial('GI3')+cLinha+(cAliasGI3)->GI3_REVISA))
		::oMdl003:SetOperation(MODEL_OPERATION_DELETE)
	Endif
	
	If ::oMdl003:Activate()
		
		oMdlGI3	:= ::oMdl003:GetModel('GI3MASTER')
		oMdlGI4	:= ::oMdl003:GetModel('GI4DETAIL')
		If (nOpc == MODEL_OPERATION_INSERT .Or. nOpc == MODEL_OPERATION_UPDATE) .And. !lDelete .And. cDeletado == '2'
							
			If nOpc == MODEL_OPERATION_INSERT
			//Adicionar uma condi��o adicionar revis�o apenas para caso altera��o com revisao
				lRet := oMdlGI3:LoadValue('GI3_LINHA',cLinha)
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_VIA',cVia)
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_ORGAO',cOrgao)
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_REVISA',cRev)
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_DEL','2')
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_MSBLQL',cStatus)
			ElseIf nOpc == MODEL_OPERATION_UPDATE
				lRet := lRet .and. oMdlGI3:LoadValue('GI3_MSBLQL',cStatus)
			EndIF
			//Gera�ao dos trecho para GI4
			If lRet
				//Realiza pesquisa da GI4 para armazenar os dados da tarifa antes de ser deletado
				dbSelectArea("GI4")
				GI4->(DbSetOrder(4))//Filia + Codigo Linha + Historico
				If GI4->(DbSeek(xFilial('GI4')+cLinha+'2')) 
					While GI4->(!Eof()) .AND. GI4->GI4_LINHA == cLinha .AND. GI4_HIST == '2'
						aAdd(aTxGI4,{GI4->GI4_LOCORI, GI4->GI4_LOCDES,;
								GI4->GI4_VIGTAR, GI4->GI4_TAR,;
								GI4->GI4_VIGTAX, GI4->GI4_TAX,;
								GI4->GI4_VIGPED, GI4->GI4_PED,;
								GI4->GI4_VIGSGF, GI4->GI4_SGFACU,;
								GI4->GI4_KMPED, GI4->GI4_KMASFA,;
								GI4->GI4_KMTERR, GI4->GI4_KM,;
								GI4->GI4_CCS, GI4->GI4_TEMPO,;
								GI4->GI4_TARANU, GI4->GI4_MSBLQL})
						GI4->(dbSkip())
					EndDo
				EndIf
				
				oMdlGI4:DelAllLine()
				
				For n1 := 1 To oMdlG5I:Length()
					If !oMdlG5I:IsDeleted(n1)   	
						cLocOri := oMdlG5I:GetValue('G5I_LOCALI',n1) 
						For n2 := n1+1 to oMdlG5I:Length()
							cItem := STRZERO(nItem,nTamItem)
							If !oMdlG5I:IsDeleted( n2 )	
								cLocDes := oMdlG5I:GetValue('G5I_LOCALI',n2) // Locais Destino
								If oMdlG5I:GetValue('G5I_VENDA',n1) <> "2" .and. oMdlG5I:GetValue('G5I_VENDA',n2) <> "2"
									oMdlGI4:GoLine(oMdlGI4:AddLine())
									lRet := oMdlGI4:SetValue('GI4_ITEM'		, cItem )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_LOCORI'	, cLocOri )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_LOCDES'	, cLocDes )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_SENTID'	, '1' )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_MSBLQL'	, '1' )
								Endif

								If !lRet
									Exit
								Else
									nItem++
								Endif
							
							EndIf
						Next
						If !lRet
							Exit
						Endif
					Else
						If aScan(::aDelete,oMdlG5I:GetValue("G5I_LOCALI")) == 0
							Aadd(::aDelete,oMdlG5I:GetValue('G5I_LOCALI', n1 ))// Trechos que foram deletados	
						Endif
					EndIf	  
				Next
			
			Endif
			
			If lRet
				For n1 := oMdlG5I:Length() To 1 Step -1
					If !oMdlG5I:IsDeleted(n1)   	
						cLocOri := oMdlG5I:GetValue('G5I_LOCALI',n1) 
						For n2 := n1-1 to 1  Step -1
							cItem := STRZERO(nItem,nTamItem)
							If !oMdlG5I:IsDeleted( n2 )	
								cLocDes := oMdlG5I:GetValue('G5I_LOCALI',n2) // Locais Destino
								If oMdlG5I:GetValue('G5I_VENDA',n1) <> "2" .and. oMdlG5I:GetValue('G5I_VENDA',n2) <> "2"
									oMdlGI4:GoLine(oMdlGI4:AddLine())
									lRet := oMdlGI4:SetValue('GI4_ITEM'		, cItem )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_LOCORI'	, cLocOri )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_LOCDES'	, cLocDes )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_SENTID'	, '2' )
									lRet := lRet .and. oMdlGI4:SetValue('GI4_MSBLQL'	, '1' )
								Endif

								If !lRet
									Exit
								ElseIf lOk
									nItem++
								Endif
							
							EndIf
						Next
						If !lRet
							Exit
						Endif
					EndIf	  
				Next
			
			Endif
			
			If lRet
				For n1 :=  1 to Len(::aDelete)
					If !oModel:GetModel('GRIDG5I'):SeekLine({{'G5I_LOCALI',::aDelete[n1]}})
						Do While oMdlGI4:SeekLine({{'GI4_LOCORI',::aDelete[n1]}}) .OR. oMdlGI4:SeekLine({{'GI4_LOCDES',::aDelete[n1]}})
							If !(lRet := oMdlGI4:DeleteLine())
								Exit
							Endif
						Enddo
					Endif
					If !lRet
						Exit
					Endif
				Next
			Endif
			//Adicionando as tarifas 
			For nA	:= 1 To oMdlGI4:Length()
				oMdlGI4:GoLine(nA)
				nPos := aScan(aTxGI4,{|x| x[1] == oMdlGI4:GetValue("GI4_LOCORI") .AND. x[2] == oMdlGI4:GetValue("GI4_LOCDES")})
				If nPos > 0 
					oMdlGI4:SetValue('GI4_VIGTAR',aTxGI4[nPos][3])
					oMdlGI4:SetValue('GI4_TAR',aTxGI4[nPos][4])
					oMdlGI4:SetValue('GI4_VIGTAX',aTxGI4[nPos][5])
					oMdlGI4:SetValue('GI4_TAX',aTxGI4[nPos][6])
					oMdlGI4:SetValue('GI4_VIGPED',aTxGI4[nPos][7])
					oMdlGI4:SetValue('GI4_PED',aTxGI4[nPos][8])
					oMdlGI4:SetValue('GI4_VIGSGF',aTxGI4[nPos][9])
					oMdlGI4:SetValue('GI4_SGFACU',aTxGI4[nPos][10])
					oMdlGI4:SetValue('GI4_KMPED',aTxGI4[nPos][11])
					oMdlGI4:SetValue('GI4_KMASFA',aTxGI4[nPos][12])
					oMdlGI4:SetValue('GI4_KMTERR',aTxGI4[nPos][13])
					oMdlGI4:SetValue('GI4_KM',aTxGI4[nPos][14])
					oMdlGI4:SetValue('GI4_CCS',aTxGI4[nPos][15])
					oMdlGI4:SetValue('GI4_TEMPO',aTxGI4[nPos][16])
					oMdlGI4:SetValue('GI4_TARANU',aTxGI4[nPos][17])
					oMdlGI4:SetValue('GI4_MSBLQL',aTxGI4[nPos][18])
				EndIF
				
			Next nA		
		//Desativando o registro caso for deletado
		ElseIf nOpc == MODEL_OPERATION_UPDATE .And. !lDelete .And. cDeletado == '1'
			
			//Adicionando data de altera��o e mudadando historico? para sim
			oMdlGI3:SetValue('GI3_DTALT', DDATABASE)
			oMdlGI3:SetValue('GI3_HIST', '1')
			oMdlGI3:SetValue('GI3_DEL', '1')
			//Adicionando data de altera��o e mudadando historico? para sim
			// GI4 Grid com todos os trecho gerado
			For nX	:= 1 to oMdlGI4:Length()
				
				oMdlGI4:GoLine(nX)
				oMdlGI4:SetValue('GI4_DTALT', DDATABASE)
				oMdlGI4:SetValue('GI4_HIST', '1')
	
			Next nX
		
		Endif
		
		If !lRet .or. !::oMdl003:VldData()
			JurShowErro( ::oMdl003:GetErrormessage() )
			lRet := .F.
		EndIf 
	Endif
Endif
FwModelActive(oModel)
RestArea(aArea)
Return lRet
