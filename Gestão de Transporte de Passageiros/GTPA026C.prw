#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'PARMTYPE.CH'
#INCLUDE 'TOTVS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "GTPA026C.CH"

/*/{Protheus.doc} GTPA026C()
Fun��o que faz a chamada para gera��o dos t�tulos POS
@type function
@author flavio.martins
@since 04/06/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Function GTPA026C(lJob, cEmp, cFil, cAgencia, cNumFch)

Default lJob     := .F.
Default cAgencia := ''
Default cNumFcha := ''

If lJob
    RpcSetType(3)
    RpcClearEnv()
    RpcSetEnv(cEmp,cFil,,,'GTP',,)
Endif

    GeraTitPos(lJob, cAgencia, cNumFch)

Return

/*/{Protheus.doc} GeraTitPos()
Gera��o de t�tulos das vendas por POS
@type function
@author flavio.martins
@since 04/06/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
Static Function GeraTitPos(lJob, cAgencia, cNumFch)
Local aTitSE1 	:= {}
Local aParcelas := {}
Local lRet		:= .T.
Local cAliasQry	:= GetNextAlias()
Local cParc		:= StrZero(1,TamSx3('E1_PARCELA')[1])
Local cNum		:= ''
Local cFilAtu	:= cFilAnt
Local cPrefixo  := PadR("POS",TamSx3('E1_PREFIXO')[1])  
Local cTipo     := PadR("TF" ,TamSx3('E1_TIPO')[1])
Local nParcelas := 0
Local nParc		:= 0
Local cHistTit	:= ""	
Local cPath     := GetSrvProfString("StartPath","")
Local cFile     := ""
Local lGT700LPE	:= ExistBlock("GT700LPE")
Local aParam	:= {}
Local nRecno    := 0
Local cStatus   := ""
Local cCarDeb   := GTPGetRules('TPCARDDEBI', .F., Nil, "CD")
Local cCarCred  := GTPGetRules('TPCARDCRED', .F., Nil, "CC")
Local cCarParc  := GTPGetRules('TPCARDPARC', .F., Nil, "CP")
Local aNewFlds  := {'GQM_CONFER', 'GQM_DTCONF', 'GQM_USUCON', 'GQM_FILTIT'}
Local lNewFlds  := GTPxVldDic('GQM', aNewFlds, .F., .T.)

Default cMsgErro	:= ""
Default cMsgTit		:= ""
Default lJob		:= .F.

Private lMsErroAuto	:= .F.

If !(lNewFlds)
    If !(lJob)
        FwAlertHelp(STR0007, STR0008) // "Dicion�rio desatualizado", "Atualize o dicion�rio para utilizar esta rotina" 
    Endif

    Return
Endif

BeginSql Alias cAliasQry

    SELECT GQL.GQL_CODADM,
        GQL.GQL_TPVEND,
        GQM.GQM_CODIGO,
        GQM.GQM_CODNSU,
        GQM.GQM_CODAUT,
        GQM.GQM_DTVEND,
        GQM.GQM_VALOR,
        GQM.GQM_QNTPAR,
        GQM.R_E_C_N_O_ RECNO,
        GI6.GI6_FILRES,
        SAE.AE_COD,
        SAE.AE_TIPO,
        G58.G58_NATURE,
        SA1.A1_COD,
        SA1.A1_LOJA
    FROM %Table:GQL% GQL
    INNER JOIN %Table:GQM% GQM 
        ON GQM.GQM_FILIAL = GQL.GQL_FILIAL
    AND GQM.GQM_CODGQL = GQL.GQL_CODIGO
    AND GQM.%NotDel%
    INNER JOIN %Table:GI6% GI6 
        ON GI6.GI6_FILIAL = %xFilial:GI6%
    AND GI6.GI6_CODIGO = GQL.GQL_CODAGE
    AND GI6.%NotDel%
    LEFT JOIN %Table:SAE% SAE 
        ON SAE.AE_FILIAL = %xFilial:SAE%
    AND SAE.AE_COD = GQL.GQL_CODADM
    AND SAE.%NotDel%
    LEFT JOIN %Table:G58% G58 
        ON G58.G58_FILIAL = %xFilial:G58%
    AND G58.G58_CODADM = SAE.AE_COD
    AND G58.%NotDel%
    LEFT JOIN %Table:SA1% SA1 
        ON SA1.A1_FILIAL = %xFilial:SA1%
    AND SA1.A1_COD = G58.G58_CLIENT
    AND SA1.A1_LOJA = G58.G58_LOJA
    AND SA1.%NotDel%
    WHERE GQL.GQL_FILIAL = %xFilial:GQL%
    AND GQL.GQL_CODAGE = %Exp:cAgencia%
    AND GQL.GQL_NUMFCH = %Exp:cNumFch%
    AND GQM.GQM_CONFER = '2'
    AND GQM.GQM_STATIT = '0'
    AND GQL.%NotDel%

EndSql
        
Begin Transaction

While (cAliasQry)->(!Eof()) 

    nRecno  := (cAliasQry)->RECNO
    cStatus := "1"

    If Empty((cAliasQry)->AE_COD)
        cStatus  := '2'
        cMsgErro := STR0001 // 'Administradora da venda n�o encontrada na tabela SAE'
    ElseIf Empty((cAliasQry)->A1_COD)       
        cStatus  := '2'
        cMsgErro := STR0002 // 'Cliente n�o encontrado na amarra��o Administradora x Bandeira'
    ElseIf Empty((cAliasQry)->GI6_FILRES)
        cStatus  := '2'
        cMsgErro := STR0003 // 'Filial respons�vel n�o informada no cadastro de ag�ncia'
    ElseIf Empty((cAliasQry)->G58_NATURE)
        cStatus  := '2'
        cMsgErro := STR0004 // 'Natureza n�o informada na amarra��o Administradora x Bandeira'
    Endif

    If cStatus == '2'

        GQM->(dbGoto(nRecno))

        Reclock("GQM", .F.)
            GQM->GQM_STATIT := cStatus
            GQM->GQM_MOTERR := cMsgErro
        GQM->(MsUnlock())

        (cAliasQry)->(dbSkip())
        Loop

    Endif

    nParcelas := Val((cAliasQry)->GQM_QNTPAR)

    If nParcelas <= 0
        nParcelas := 1
    Endif

    If AllTrim((cAliasQry)->AE_TIPO) == "CD"
        cTipo	:= cCarDeb 
    ElseIf nParcelas > 1
        cTipo	:= cCarParc 
    Else
        cTipo	:= cCarCred 
    Endif

    If !Empty((cAliasQry)->GI6_FILRES)
        cFilAnt := (cAliasQry)->GI6_FILRES
    Endif

    aParcelas := ParcTit(nParcelas,(cAliasQry)->GQM_VALOR ,STOD((cAliasQry)->GQM_DTVEND),(cAliasQry)->GQL_TPVEND)

    cNum := GtpTitNum('SE1', cPrefixo, cParc, cTipo)

    aTitSE1 := {}

    For nParc := 1 to Len(aParcelas)
        aTitSE1 :=	{;
                        { "E1_PREFIXO"	, cPrefixo									, Nil },; //Prefixo 
                        { "E1_NUM"		, cNum										, Nil },; //Numero
                        { "E1_PARCELA"	, StrZero(nParc,TamSx3('E1_PARCELA')[1])	, Nil },; //Parcela
                        { "E1_TIPO"		, cTipo										, Nil },; //Tipo
                        { "E1_NATUREZ"	, (cAliasQry)->G58_NATURE					, Nil },; //Natureza
                        { "E1_CLIENTE"	, (cAliasQry)->A1_COD  			    		, Nil },; //Cliente
                        { "E1_LOJA"		, (cAliasQry)->A1_LOJA 		    			, Nil },; //Loja
                        { "E1_EMISSAO"	, STOD((cAliasQry)->GQM_DTVEND)				, Nil },; //Data Emiss�o
                        { "E1_VALOR"	, aParcelas[nParc][1]						, Nil },; //Valor
                        { "E1_SALDO"	, aParcelas[nParc][1]	   					, Nil },; //Saldo
                        { "E1_VENCTO"	, aParcelas[nParc][2]						, Nil },; //Data Vencimento
                        { "E1_VENCREA"	, aParcelas[nParc][3]						, Nil },; //Data Vencimento Real
                        { "E1_HIST"		, cHistTit									, Nil },; //HIst�rico
                        { "E1_NSUTEF"	, (cAliasQry)->GQM_CODNSU   				, Nil },; //HIst�rico
                        { "E1_CARTAUT"	, (cAliasQry)->GQM_CODAUT					, Nil },; //HIst�rico
                        { "E1_ORIGEM"	, "GTPA500"							    	, Nil };  //Origem										
                    }
        
        MsExecAuto( { |x,y| FINA040(x,y)} , aTitSE1, 3) // 3-Inclusao,4-Altera��o,5-Exclus�o		
        
        If !lMsErroAuto

            GQM->(dbGoto(nRecno))

            Reclock("GQM", .F.)
                GQM->GQM_STATIT := cStatus
                GQM->GQM_FILTIT := SE1->E1_FILIAL
                GQM->GQM_PRETIT := SE1->E1_PREFIXO
                GQM->GQM_NUMTIT := SE1->E1_NUM
                GQM->GQM_PARTIT := SE1->E1_PARCELA
                GQM->GQM_TIPTIT := SE1->E1_TIPO
            GQM->(MsUnlock())

            /*If lGT700LPE
                Aadd(aParam, (cAliasQry)->G6Y_FILIAL )
                Aadd(aParam, (cAliasQry)->G6Y_CODIGO )
                Aadd(aParam, (cAliasQry)->G6Y_TPLANC )
                Aadd(aParam, (cAliasQry)->G6Y_NUMFCH )
                Aadd(aParam, (cAliasQry)->G6Y_CODAGE )
                Aadd(aParam, (cAliasQry)->G6Y_ITEM   )
                ExecBlock("GT700LPE", .f., .f., aParam)
            EndIf	*/	
        Else
            If !lJob
                MostraErro()
            Else
                cMsgErro := MostraErro(cPath,cFile)

                Reclock("GQM", .F.)
                    GQM->GQM_STATIT := '2'
                    GQM->GQM_MOTERR := cMsgErro
                GQM->(MsUnlock())

                Exit
            Endif
        Endif

    Next nParc	

    (cAliasQry)->(dbSkip())

EndDo

cFilAnt := cFilAtu

End Transaction

If Select(cAliasQry) > 0
    (cAliasQry)->(dbCloseArea())
Endif

If !lJob
    FwAlertSuccess(STR0005, STR0006) // "T�tulos gerados com sucesso","Gera��o dos T�tulos de POS"
Endif	

Return