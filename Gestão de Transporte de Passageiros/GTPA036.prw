#include "GTPA036.CH"
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'

Static cG036Table  := ""    //Nome da Tabela Tempor�ria do ResultSet das Ag�ncias

Static aG036Radio  := {}    //Array com as op��es escolhidas pelo usu�rio no Menu Radio da tela do wizard (segundo step)
Static aG036Trans  := {}    //Array De Para dos campos do arquivo DARUMA para o respectivo campo da tabela GIC
Static aG036Log    := {}    //Array com o Log de Erros que ocorreram nas importa��es 
Static aG036GICData:= {}

Static oWizard		:= nil

Static nG036LnFile  := 0    //vari�vel num�rica que guarda a linha atual do arquivo que est� sendo importado
Static nG036CtrErro := 0    //vari�vel num�rica que � atualizada de acordo com erro que � encontrado nos casos de problemas com a leitura dos arquivos que ser�o importados    
Static nG036QtDados := 0
Static nG36LRecFile	:= 0

Static lG036First   := .t. //Primeira Troca entre o Passo 2 com Passo 3

/*/{Protheus.doc} GTPA036
    Programa de importa��o e exporta��o de arquivos da m�quina DARUMA
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GTPA036()

Local oPageWiz  := nil
Local oView     := nil


Local bCancel   := {||  FWAlertInfo(STR0001),; //"O Assistente de Configura��o foi Cancelado. As informa��es parametrizadas ser�o perdidas!"
                       .T.}

//Cria arquivo Tempor�rio de Log
GA036CTable()

oWizard := FWWizardControl():New()

oWizard:SetSize({oMainWnd:nClientHeight * 0.95,oMainWnd:nClientWidth * 0.95})
oWizard:ActiveUISteps()

//Primeira P�gina - Introdu��o
oPageWiz := oWizard:AddStep("1")
oPageWiz:SetStepDescription(STR0002)	//"1-Introdu��o"
oPageWiz:SetConstruction( {|oPnl| GA036WizScr(oPnl,"0")} ) //Define o bloco de constru��o
oPageWiz:SetCancelAction({|| .T.})//Valida acao cancelar, nao deixa sair do wizard //"Wizard n�o pode ser cancelado!"

//Segunda P�gina - defini��o do tipo de arquivo (layout) que ser� utilizado
oPageWiz := oWizard:AddStep("2")
oPageWiz:SetStepDescription(STR0003)	//"2-Tipo de Arquivo"
oPageWiz:SetConstruction( {|oPnl| GA036WizScr(oPnl,"1")} ) //Define o bloco de constru��o
oPageWiz:SetNextAction( {|| GA036Vld("2",@oView)} )//Define o bloco ao clicar no bot�o Pr�ximo
oPageWiz:SetCancelAction(bCancel)//Valida acao cancelar, nao deixa sair do wizard //"Wizard n�o pode ser cancelado!"

//Terceira P�gina - Lista de Ag�ncias
oPageWiz := oWizard:AddStep("3")
oPageWiz:SetStepDescription(STR0004)	//"3-Ag�ncias"
oPageWiz:SetConstruction( {|oPnl| oView := GA036WizScr(oPnl,"2")} ) //Define o bloco de constru��o
oPageWiz:SetNextAction( {|lOk|  lOk := .t., lOk := GA036Vld("3"), Iif(lOk,GA036Commit(oView),lOk) } )//Define o bloco ao clicar no bot�o Pr�ximo
oPageWiz:SetCancelAction(bCancel)//Valida acao cancelar, nao deixa sair do wizard //"Wizard n�o pode ser cancelado!"

//Ativa Wizard
oWizard:Activate()

//Verifica se h� log de Erro de importa��o
If ( Len(GA036CArrLog()) > 0 )
    
    FwAlertWarning(STR0089,STR0090)   //"Processamento conclu�do, mas com pend�ncias. Veja o Log de Erros a seguir" //"Wizard - Daruma"

    //Atualiza arquivo tempor�rio de Log
    G36CCargaTabLog()
    
    //Exibe o log de erros de importa��o
    FWExecView(STR0005,"VIEWDEF.GTPA036C",MODEL_OPERATION_VIEW,,{|| .T.})//"Log de Erros de Importa��es - DARUMA"

Else
    FwAlertSuccess(STR0091,STR0090) //"Wizard Concluido com sucesso" //"Wizard - Daruma"
EndIf

//Desativa Wizard
oWizard:Destroy()

GA036Destroy(@oView)

Return()

/*/{Protheus.doc} GA036Vld
    Valida��o do Wizard
    
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 06/03/2017
    @version P12
    @param cPgWiz, caracter, nro da p�gina do Wizard que est� sendo validada
    @return lRet,logic, .t. - validado com sucesso
    @example
    @see (links_or_references)
/*/
Static Function GA036Vld(cPgWiz,oView)

Local oFWLayer
Local aSaveLines  := {}
Local cTitle      := ""
Local cMsgProb    := ""
Local cMsgSolu    := ""

Local lRet := .t.
Local nCont       := 0
Local n1,n2

If ( cPgWiz == "2" )
    
    If ( !Empty(aG036Radio) )

        If ( aG036Radio[1] == 3 .and. aG036Radio[2] == 0 )
        
            lRet := .f.
            
            cTitle      := STR0006//"Par�metros n�o preenchidos"
            cMsgProb    := STR0007//"N�o foi selecionado o Layout para o arquivo tipo Flash!"
            cMsgSolu    := STR0008//"Selecione um Layout, quando o tipo de arquivo for Flash. "

        EndIf    

        If ( lRet )

            If ( Empty(cG036Table) .Or. Select(cG036Table) == 0 ) 
                lRet := GA036ListaAgencias()
            EndIf

            (cG036Table)->(DbGoTop())

            If ( (cG036Table)->(!Eof())  )

                If ( lRet .And. !lG036First )

                    If ( ValType(oView) == "O" )
                        oView:DeActivate()
                        oView:Destroy()
                    EndIf
                    
                    If ( Len(oWizard:aWizStru) >= 3 .And. ValType(oWizard:aWizStru[3]) == "O" )
                        
                        lRet := GA036ListaAgencias()                    
                        
                        If ( lRet )
                            oView := G036SetView(oFWLayer,oWizard:aWizStru[3]:oUserPanel)
                        EndIf
                    
                    EndIf

                EndIf
            
            Else    

                lRet := .f.    

                cTitle      := STR0103 // "Sem Ag�ncias"
                cMsgProb    := STR0104 // "N�o h� Ag�ncias para serem listadas!"
                cMsgSolu    := STR0105 // "Verifique o cadastro de Ag�ncia. "
                cMsgSolu    += STR0106 // "� necess�rio que a ag�ncia possua venda embarcada, "
                cMsgSolu    += STR0107 // "bem como configura��es para a leitura de arquivos DARUMA."

            EndIf
            
        EndIf

    Else

        lRet := .F.    

        cTitle      := STR0009//"Par�metros n�o preenchidos"
        cMsgProb    := STR0010//"N�o foi selecionado nenhum tipo de arquivo!"
        cMsgSolu    := STR0011//"Escolha uma op��o de tipo de arquivo. "
        cMsgSolu    += STR0012//"Se optar pelo Tipo Flash, escolha tamb�m um Layout."

    EndIf

ElseIf ( cPgWiz == "3")
    
    oView := FwViewActive()
	aSaveLines  := FWSaveRows()
	If ( aG036Radio[1] <> 3 )
		If ( Empty(oView:GetModel("MASTER"):GetValue("XXX_DATADE")) .And. Empty(oView:GetModel("MASTER"):GetValue("XXX_DATATE")) )
			lRet := .f.
	       
			cTitle      := STR0013//"Par�metros n�o preenchidos"
			cMsgProb    := STR0014//"Os par�metros STR0015 e STR0016 n�o podem ficar sem preenchimento."//'Data De'//'Data at�'//"Os par�metros 'Data De' e 'Data at�' n�o podem ficar sem preenchimento."
			cMsgSolu    := STR0017//"Defina os par�metros mencionados."
	    EndIf
	Endif
	If ( lRet )
		
        If lRet .and. ( !oView:GetModel('GI6DETAIL'):SeekLine({{'GI6_MARK',.T.}}) )
			lRet := .f.
			cTitle      := STR0018//"Nenhum item Marcado"
			cMsgProb    := STR0019//"N�o h� ag�ncias selecionadas."
			cMsgSolu    := STR0020//"Selecione uma ou mais ag�ncias para poder prosseguir."
		Endif
		
		If lRet .and. aG036Radio[1] <> 3 //N�o � arquivo Flash
			If Empty(oView:GetModel('GI6DETAIL'):GetValue('GI6_FILRES'))
				lRet := .F.
				cTitle      := STR0108 //"Filial Responsavel n�o preenchida"
				cMsgProb    := STR0109 //"Agencia selecionada n�o possu� filial responsavel"
				cMsgSolu    := STR0110 //"Cadastre a filial responsavel no registro da agencia"
			Endif
		Endif
		
		If lRet .and. aG036Radio[1] == 3  
			If !oView:GetModel('GZ1DETAIL'):SeekLine({{'GZ1_MARK',.T.}}) 
				lRet		:= .F.
				cTitle		:= STR0018//"Nenhum item Marcado"
				cMsgProb	:= STR0097//"N�o h� linhas selecionadas."
				cMsgSolu	:= STR0098//"Selecione uma ou mais linhas para poder prosseguir."
			Endif
			If lRet
				For n1 := 1 to oView:GetModel('GI6DETAIL'):Length()
					If oView:GetModel('GI6DETAIL'):GetValue('GI6_MARK',n1)
						oView:GetModel('GI6DETAIL'):GoLine(n1)
						nCont := 0
						For n2 := 1 to oView:GetModel('GZ1DETAIL'):Length()
							If oView:GetModel('GZ1DETAIL'):GetValue('GZ1_MARK',n2)
								nCont++
								If oView:GetModel('GZ1DETAIL'):GetValue('GZ1_SENTID',n2) == "3" //Ambos
									nCont++
								Endif
							Endif
							If nCont > 4
								lRet := .F.
								Exit
							EndIf
						Next
					Endif
					If !lRet
						Exit
					Endif
				Next
				If !lRet
					cTitle		:= STR0099//"Muitas linhas selecionadas"
					cMsgProb	:= STR0100//"Foram marcadas mais de 4 linhas para a mesma agencia"
					cMsgSolu	:= STR0101//"Selecione at� 4 linhas para prosseguir"
				Endif
			Endif
		Endif
        FWRestRows(aSaveLines)
	EndIf

EndIf
If ( !lRet )
	FWAlertHelp(cMsgProb,cMsgSolu,cTitle)
EndIf

Return(lRet)

/*/{Protheus.doc} GA036WizScr
    Fun��o respons�vel pela montagem dos pain�is do Wizard
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  oPanel, objeto, inst�ncia da classe tPanel
            cPage, caractere, nro da p�gina (step) do wizard que � montada
    @return xRet, qualquer, de acordo com a p�gina que � montada, um retorno diferente ser� usado.
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036WizScr(oPanel, cPage)

Local oFWlayer  := nil
Local oWin      := nil
Local oTxtLn1   := nil
Local oTxtLn2   := nil
Local oFont     := nil
Local nTam1     := -18
Local nTam2     := -16
Local nLin      := 40
Local nCol      := 100
Local nCol2     := 130
Local xRet

oFWLayer := FWLayer():New()
oFWLayer:Init( oPanel, .F.)
oFWLayer:AddCollumn("C1", 100, .T.)
oFWLayer:AddLine("L1", 100)

//Componentes da Interface
Do Case
Case ( cPage == "0" )   //Introdu��o

    oFWLayer:addWindow("C1", "WIN00", STR0021, 100,.F., .F., {|| Nil })//"Assinstente de Configura��o - DARUMA"

    oWin := oFWLayer:getWinPanel("C1", "WIN00")

    oFont  := TFont():New('Arial',,nTam1,.T.,.T.)
    oTxtLn1 := TSay():New(nLin,nCol, {|| STR0022},oWin,,oFont,,,,.T.,,,)//"Seja Bem Vindo ao Assistente de Configura��o de Importa��o/Exporta��o de arquivos DARUMA!"

    oFont  := TFont():New('Arial',,nTam2,.f.,.f.)
    oTxtLn2 := TSay():New(nLin+25,nCol2, {|| STR0023},oWin,,oFont,,,,.T.,,,)//"Este assistente ir� auxili�-lo na importa��o e exporta��o de dados de vendas das m�quinas DARUMA."

Case ( cPage == "1" )   //Tipo de Arquivo
    
    oFWLayer:addWindow("C1", "WIN01", STR0024, 100,.F., .F., {|| Nil })		//"Planilha de C�lculo"//"Selecione o Tipo de Arquivo a ser importado/exportado"

    oWin    := oFWLayer:getWinPanel("C1", "WIN01")
    GA036Radio(oWin)
 
Case ( cPage == "2" )   //Lista de Ag�ncias

    If ( lG036First )
        lG036First := .f.
    EndIf

    xRet := G036SetView(oFWLayer,oPanel)  
    
EndCase

Return(xRet)

/*/{Protheus.doc} GA036Radio
    Montagem do Menu Radio
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param param, param_type, param_descr
    @return oWindow, objeto, inst�ncia da classe tPanel
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Radio(oWindow)
 
Local nItem     := 0 
Local nItFlash  := 0
Local nCol      := 10
Local nLin      := 10
Local nLin2     := 30
Local nTam1     := -14
Local aItems    := {}
Local oRadio    := Nil
Local oRadFlash := Nil
Local oFont     := Nil
Local oTxtLn1   := Nil
Local cCSS      := ""

oFont  := TFont():New('Arial',,nTam1,.T.,.T.)
oTxtLn1 := TSay():New(nLin,nCol, {|| STR0026},oWindow,,oFont,,,,.T.)//"Selecione um dos tipos de arquivos abaixo. Para o tipo de arquivo Flash, dever� ser escolhido tamb�m o seu Layout"

cCSS := "QRadioButton:indicator { width: 16px; height: 16px; }"

//Montagem do Menu Radio dos tipos de arquivos
oFont   := TFont():New('Arial',,-18,.f.,.T.)
aItems  := {STR0028, STR0029, STR0027}//'Arquivo Flash (exporta��o)'//'Arquivo P�s-venda'//'Arquivo n�o formatado'
oRadio  := TRadMenu():Create(oWindow,,25,10,aItems,,,,,,,,140,25)
oRadio:bSetGet := {|u| Iif( PCount() == 0, nItem, nItem := u ) }
oRadio:bChange := {|| GA036EnableRadio(oRadFlash,nItem) }
oRadio:bLostFocus := {|| aG036Radio := {nItem,nItFlash} }
oRadio:SetCSS(cCSS)
oRadio:Refresh()

//Montagem do Menu Radio para os layouts do Tipo de Arquivo Flash
oFont   := TFont():New('Arial',,-10,.f.,.f.)
aItems  := {STR0030,STR0031}//'Layout: FS420'//'Layout: FS420N'
oRadFlash := TRadMenu():Create(oWindow,,25+nLin2,25,aItems,,,,,,,,140,25)
oRadFlash:bSetGet := {|u| Iif( PCount() == 0, nItFlash, nItFlash := u ) }
oRadFlash:bLostFocus := {|| aG036Radio := {nItem,nItFlash} }
oRadFlash:SetCSS(cCSS)
oRadFlash:Disable(1)
oRadFlash:Disable(2)
oRadFlash:Refresh()

Return()

/*/{Protheus.doc} GA036EnableRadio
    Fun��o que habilita ou desabilita Radio Button, dependendo do item que foi marcado pelo usu�rio
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param oRadFlash, objeto, inst�ncia da classe TRadMenu
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036EnableRadio(oRadFlash,nItChose)

If (nItChose == 3)
    oRadFlash:EnableItem(1,.t.)
    oRadFlash:EnableItem(2,.t.)
Else
    oRadFlash:EnableItem(1,.F.)
    oRadFlash:EnableItem(2,.F.)    
EndIf

oRadFlash:Refresh()

Return()

/*/{Protheus.doc} GA036ListaAgencias
    Monta o ResultSet para listar as ag�ncias que possuem venda embarcada e configura��o de 
    arquivo Daruma
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param
    @return lRet, l�gico, .t. - o ResultSet retornou dados
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036ListaAgencias()

Local lRet := .T.

Local cWhere   := ""
Local cMark     := "% 'F' GI6_MARK%"

If ( ValType(aG036Radio) == "A" .And. Len(aG036Radio) > 0)
     
    If ( aG036Radio[1] == 1 )
        cWhere := "% GZ0_ARQPVE <> '' %"
    ElseIf ( aG036Radio[1] == 2 )
        cWhere := "% GZ0_ARQNFO <> '' %"
    Else
        cWhere := "% GZ0_ARQFLA <> '' %"
        cMark := "% 'F' GI6_MARK%"
    EndIf

    If ( !Empty(cG036Table) .And. Select(cG036Table) > 0 )
        (cG036Table)->(DbCloseArea())
    EndIf

    cG036Table := GetNextAlias()

    BeginSQL Alias cG036Table
        
        Column GI6_MARK as Logical
        
        SELECT
            DISTINCT
            %Exp:cMark%,
            GI6.GI6_CODIGO,
            GI6_DESCRI,
            GI6_FILRES,
            GZ0_ARQPVE POS_VENDA,
            GZ0_ARQNFO  NO_FORMAT,
            GZ0_ARQFLA  ARQ_FLASH,
            GI6.R_E_C_N_O_ GI6_RECNO
        FROM
            %Table:GI6% GI6
        INNER JOIN
            %Table:GZ0% GZ0
        ON
            GZ0_FILIAL = %XFilial:GZ0%
            AND GZ0_CODGI6 = GI6_CODIGO
            AND %Exp:cWhere%            
            AND GZ0.%NotDel%
        INNER JOIN
        	%Table:G9X% G9X
        ON
        	G9X.%NotDel%
        	AND G9X_FILIAL = %xFilial:G9X%
        	AND G9X_CODUSR = %Exp:RetCodUsr()% 
        	AND G9X_CODGI6 = GI6_CODIGO
        WHERE
            GI6_FILIAL = %XFilial:GI6%
            AND GI6_VEMBAR = '1' 
            AND GI6_MSBLQL = '2'
            AND GI6.%NotDel%
		ORDER BY GI6.GI6_CODIGO
    EndSQL

    lRet := (cG036Table)->(!Eof())
Else
	FWAlertHelp(STR0092,STR0093,STR0094)   //"Configura��es Incorretas."//"N�o h� ag�ncias a serem listadas." //"Verifique as configura��es de importa��o/exporta��o da Ag�ncia (Cadastro)"
	lRet := .F.
EndIf

Return(lRet)

/*/{Protheus.doc} GA036Commit
    Fun��o principal para a execu��o do processamento de importa��o/exporta��o
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param oView, objeto, inst�ncia da classe FwFormView
    @return lRet, l�gico, .t. processado, .f. - problemas para processar.
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Commit(oView)

Local lRet  := .t.

//Selecionando os diret�rios das Ag�ncias marcadas
If ( aG036Radio[1] == 1 )            //Importa��o do Arquivo P�s-Venda
    MsgRun(STR0033, STR0032, {|| GA036ReadFile(oView,"POS_VENDA")} ) //"Arquivos P�s-Vendas"//"Processando arquivos"
ElseIf ( aG036Radio[1] == 2 )        //Importa��o do Arquivo N�o Formatado
    MsgRun(STR0035, STR0034, {|| GA036ReadFile(oView,"NO_FORMAT")} )     //"Arquivos N�o Formatados"//"Processando arquivos"
Else
    FwMsgRun(, {||GA036Flash(oView,aG036Radio[2]),STR0095,STR0096 }) //"Gera��o do Arquivo Flash"  //"Gravando arquivo Flash..."
EndIf

Return(lRet)

/*/{Protheus.doc} GA036ReadFile
    Percorre o grid das ag�ncias, para efetuar a leitura de cada arquivo (de cada ag�ncia)
    que dever� ser importado (tanto no tipo pos-venda quanto no n�o formatado)
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  oView, objeto, inst�ncia da classe FwFormView
            cField, caractere, qual o tipo de arquivo de importa��o
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036ReadFile(oView,cField)

Local oGrid    := oView:GetModel("GI6DETAIL")
Local oTabela  := Nil
Local nI       := 0
Local aDirFilt  := {} 

//Leitura da lista de ag�ncias
For nI := 1 to oGrid:Length()

    oGrid:GoLine(nI)

	//somente as ag�ncias selecionadas � que ser�o processadas
    If ( oGrid:GetValue("GI6_MARK") )

        aDirFilt := GA036FilDir(AllTrim(oGrid:GetValue(cField,nI)),oView:GetModel("MASTER"):GetValue("XXX_DATADE"), oView:GetModel("MASTER"):GetValue("XXX_DATATE"))

        If ( Len(aDirFilt) > 0 )
        
            GA036ProcUpd(aDirFilt,oView,cField)
        	    
        EndIf    

    EndIf

Next nI

oTabela := GA036EObjTab()

Return()

/*/{Protheus.doc} GA036FilDir
    Filtra os arquivos que ser�o importados. Para o filtro s�o utilizados
    os par�metros Data de/at� definidos pelo usu�rio no terceiro painel do Wizard
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cPath, caractere, diret�rio dos Arquivos 
            dDataDe, data, data inicial do filtro
            dDataAte, data, data final do filtro
    @return aDirFilt, array, array com a lista dos arquivos que foram filtrados
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036FilDir(cPath,dDataDe,dDataAte)

Local aDir      := {}
Local aDirFilt  := {}

Local cDir      := ""

Local bFiltro   := {|x|  Iif(;
                                x[3] >= dDataDe .and.; 
                                x[3] <= dDataAte,;
                                    aAdd(aDirFilt,{x[1],x[3],cPath}) ,;
                                    nil;
                            ) }

If ( !Substr(cPath,Len(cPath)) $ "\|/" )
    cDir := cPath + "\*.*"
Else    
    cDir := cPath + "*.*"
EndIf   

aDir := Directory(cDir,"A")    //Somente os Arquivos

aEval(aDir,bFiltro)

Return(aDirFilt)

/*/{Protheus.doc} GA036ProcUpd
    Fun��o avalia qual o tipo de arquivo que est� sendo importado e chama fun��o de apoio 
    de acordo com o tipo;
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  aDirectory, array, array com os diret�rios e arquivos que devem ser lidos
            oView, objeto, inst�ncia da classe FwFormView
            cField, caractere, Tipo de Arquivo que ser� importado
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036ProcUpd(aDirectory,oView,cField)

Local aLog		:= {}
Local cArq		:= ""
Local cAgencia	:= ""
Local nI		:= 0
Local nPAg		:= 0
Local nPFile	:= 0

Local lMove	:= .f.

Default oView := FwViewActive()

//percorre a lista de aquivos DARUMA de um diret�rio para uma ag�ncia.
For nI := 1 to Len(aDirectory)
	
	//Inicializa a vari�vel com .f., a cada itera��o
	lMove	:= .F.
	cArq	:= aDirectory[nI,3]+"\"+aDirectory[nI,1]
	cAgencia:= oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO")
	
    If ( cField == "POS_VENDA" )
        GA036PosVenda(cArq,oView)
    Else
        GA036NaoFormat(cArq,oView)        
    EndIf    
	
	aLog := GA036CArrLog()
    
    //Move o arquivo para a pasta Lidos
    nPAg := aScan(aLog,{|x| Alltrim(x[2]) == Alltrim(cAgencia) })
    
    If ( nPAg > 0 )
    
    	nPFile := aScan(aLog[nPAg,4],{|x| Alltrim(x[2]) == Alltrim(cArq) })
    		
    	If ( nPFile == 0 )    	
    		lMove := .T.	//Somente move arquivo para pasta Lidos, quando n�o der erro no processamento
    	ElseIf (aScan(aLog[nPAg,4,nPFile,6],{|x| Alltrim(x[8]) == Alltrim('1') })) == 0
    		lMove := .T.
    	EndIf
    Else
    	lMove := .t.	//Somente move arquivo para pasta Lidos, quando n�o der erro no processamento
    EndIf
    
    If ( lMove )
    	GA036CpyFile(aDirectory[nI,3],aDirectory[nI,1],.t.)
    EndIf
    
Next nI

Return()


/*/{Protheus.doc} GA036ReducaoZ
Importa��o do arquivo do Tipo P�s-venda - Tag da Redu��o Z
@type function
@author jacomo.fernandes
@since 24/08/2018
@version 1.0
@param cEcfSeq, character, (Descri��o do par�metro)
@param cEcfSer, character, (Descri��o do par�metro)
@param cArquivo, character, (Descri��o do par�metro)
@param oView, objeto, (Descri��o do par�metro)
@param nLnFile, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036ReducaoZ(cEcfSeq,cEcfSer,cArquivo,oView,nLnFile)

Local lGoAhead	:= .f.
Local lReducao 	:= .f.
Local lProssiga	:= .f.
Local nHandle	:= 0
Local nLine		:= 0
Local nP		:= 0
Local nPos		:= 0
Local cLineRead	:= ""
Local aData		:= {}
Local aReducaoZ	:= {}
Local dDtIni	:= CtoD('  /  /  ')
Local dDtFim    := CtoD('  /  /  ')
Local bSkip     := {|| nG036LnFile := GA036RetLnRead(), FT_FSkip(), GA036CAddLn() }

Default nLnFile	:= 0

If ( nLnFile == 0 )

	//Abre o Arquivo
	nHandle := FT_FUse(cArquivo)
	
	If ( nHandle > 0 )
		lGoAhead := .t.
		nLine := 1
	EndIf
	
Else

	lGoAhead := .t.
	nLine := nLnFile
	
EndIf

If ( lGoAhead )
	 
	aData := GetDataRedZ() 
	
	FT_FGoto(nLine)
	
	While ( !FT_FEof() )
	
		cLineRead := FT_FReadLn()
		//Apartir desse momento comessa as redu��es Z gravadas no arquivo
		If ( At("RMD",Upper(cLineRead)) > 0 ) 
			lReducao := .t.
		EndIf
		
		If ( lReducao )
			
			//Data de Movimento da Redu��o Z
			If ( At("/",cLineRead) > 0 .And. ( CtoD(Substr(cLineRead,1,10)) <> Ctod('  /  /  ' ) ) )
				
				dDtIni	:= CtoD('  /  /  ')
				dDtFim	:= CtoD('  /  /  ')
								
				//Inicia a leitura como falso para verificar se deve incluir a redu��o
				lProssiga := .F.
				
				If ( nP := aScan(aData,{|x| x[1] == "FI_PDV"}) ) > 0 
					aData[nP,2] := cEcfSeq
				EndIf
					
				If ( nP := aScan(aData,{|x| x[1] == "FI_SERPDV"}) ) > 0 
					aData[nP,2] := cEcfSer
				EndIf

				If ( nP := aScan(aData,{|x| x[1] == "FI_DTMOVTO"}) ) > 0 
					aData[nP,2] := Alltrim(Substr(cLineRead,01,10))	
				EndIf
				
				If ( CtoD(Substr(cLineRead,14,10)) <> Ctod('  /  /  ' ) ) //se a Segunda data n�o for informada, n�o continuar o processo 
					lProssiga := .T.
					dDtIni	:= CtoD(Substr(cLineRead,01,10))
					dDtFim	:= CtoD(Substr(cLineRead,14,10))
				Else
					lProssiga := .T.
					dDtIni	:= CtoD(Substr(cLineRead,01,10))
					dDtFim	:= dDtIni+1
				Endif
				
			Endif
			
			If lProssiga
				
				//Valor Base
				If ( At("VB",cLineRead) > 0 )
											
					nP := aScan(aData,{|x| x[1] == "FI_VALCON"})
				
					If ( nP > 0 )
						nPos	:= At("$",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrTran(StrTran(Alltrim(Substr(cLineRead,nPos+1,18)),".",""),",",".")
						EndIf
					EndIf
				
				//Grande Total Inicial	
				ElseIf ( At("GTi",cLineRead) > 0 ) 	
				
					nP := aScan(aData,{|x| x[1] == "FI_GTINI"})
				
					If ( nP > 0 )
						
						nPos	:= At("$",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrTran(StrTran(Alltrim(Substr(cLineRead,nPos+1,18)),".",""),",",".")
						EndIf
								
					EndIf
				
				//Grande Total Final + CRO	
				ElseIf ( At("GTf",cLineRead) > 0 )
					
					nP := aScan(aData,{|x| x[1] == "FI_GTFINAL"})
					
					If ( nP > 0 )
						
						nPos	:= At("$",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrTran(StrTran(Alltrim(Substr(cLineRead,nPos+1,18)),".",""),",",".")
						EndIf
								
					EndIf
					
					nP := aScan(aData,{|x| x[1] == "FI_CRO"})
				
					If ( nP > 0 )
					
						nPos := At("CRO:",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrZero(Val(Substr(cLineRead,nPos+5)),TAMSX3('FI_CRO')[1])
						EndIf
					
					EndIf
				
				//Contador de Ordem de Opera��o Inicial + Contador de Ordem de Opera��o Final + Contador Redu��o Z
				ElseIf ( At("COOi",cLineRead) > 0 )
					
					nP := aScan(aData,{|x| x[1] == "FI_NUMINI"})
					
					If ( nP > 0 )
						nPos := At("COOi:",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrZero(Val(Substr(cLineRead,nPos+6,6)),TAMSX3('FI_NUMINI')[1])
						EndIf
					EndIf
					
					nP := aScan(aData,{|x| x[1] == "FI_NUMFIM"})
					
					If ( nP > 0 )
						nPos := At("COOf:",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrZero(Val(Substr(cLineRead,nPos+6,6)),TAMSX3('FI_NUMFIM')[1])
						EndIf
					EndIf
					
					nP := aScan(aData,{|x| x[1] == "FI_NUMREDZ"})
					
					If ( nP > 0 )
						nPos := At("CRZ:",cLineRead)
						If ( nPos > 0 )
							aData[nP,2] := StrZero(Val(Substr(cLineRead,nPos+5)),TAMSX3('FI_NUMREDZ')[1])
						EndIf
					EndIf
				
				ElseIf ( At("[BASE DE CALCULO]",cLineRead) > 0 )
				
					RedZBaseCalc(aData)	
						
				ElseIf ( At("[IMPOSTO]",cLineRead) > 0 )
					
					RedZImpostos(aData)
					
					//a tag "[IMPOSTO]" � a tag de condi��o de t�rmino de uma  Redu��o Z
					nP := aScan(aData,{|x| x[1] == "FI_DTMOVTO"})
				
					If ( nP > 0 )
						aAdd(aReducaoZ,{cArquivo,GtpCastType(aData[nP,2],GetSx3Cache("FI_DTMOVTO","X3_TIPO")),aClone(aData),dDtIni,dDtFim})
					EndIf
					
					aData := GetDataRedZ() 
					
				EndIf
											
			EndIf
			 
		EndIf
		
		Eval(bSkip)
	
	End While

EndIf

Return(aReducaoZ)

/*/{Protheus.doc} RedZBaseCalc
(long_description)
@type function
@since 18/09/2018
@version 1.0
@param aData, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function RedZBaseCalc(aData)

Local cFldBase	:= ""
Local cFldCod	:= ""
Local cAux		:= ""
Local cLineRead	:= ""

Local nP	:= 0
Local nPos	:= 0
Local nAliq	:= 0

Local bSkip     := {|| nG36LRecFile := FT_FRecno(),FT_FSkip(), nG036LnFile := GA036CAddLn() }
Local bSkipBack	:= {|x| FT_FGoTo(nG36LRecFile), nG036LnFile := GA036CSubLn()}

While ( !FT_FEof() )
	
	cLineRead := FT_FReadLn()
	
	//Valor base de c�lculo do ICMS que ser� inserido no campo equivalente a Al�quota(ex: se a Al�quota � 12,00%, o campo � FI_BAS12) + 
	//Valor do Campo de c�digo da base
	If ( At("T",cLineRead) > 0 .And. At("%",cLineRead) > 0) 
		
		If At("T//",cLineRead) == 0 
		
			nP 		:= At("T",cLineRead) 
			nPos	:= RAt("%",cLineRead)
		
			nAliq 	:= Val(StrTran(Substr(cLineRead,nP+1,nPos - (nP+1)),",","."))		
			cAux 	:= GA036AliqField(nAliq)
		
			cFldBase	:= "FI_BAS" + cAux 
			cFldCod		:= "FI_COD" + cAux
		
			//Valor base do ICMS de acordo com a al�quota retornada pela leitura do arquivo	 			
			nP := aScan(aData,{|x| x[1] == cFldBase})
		
			If ( nP > 0 ) .AND. Empty(aData[nP,2])
			
				nPos := At("$",cLineRead)
			
				If ( nPos > 0 )
					aData[nP,2] := StrTran(Substr(cLineRead,nPos+1,10),",",".")
				EndIf
		
			EndIf
		
			//Conte�do do campo que guarda o c�digo da al�quota.
			nP := aScan(aData,{|x| x[1] == cFldCod})
		
			If ( nP > 0 )
			
				aData[nP,2] := "T"+If( Len(cAux)==1 ,'0'+cAux,cAux)+'00'
		
			EndIf
			
		Endif
		
		If Rat("I:", cLineRead) > 0
		
			nPos	:= RAt("$",cLineRead)
			
			
			If ( nPos > 0 )
			
				nP := aScan(aData,{|x| x[1] == "FI_ISENTO"})
				
				aData[nP,2] := StrTran(Substr(cLineRead,nPos+1,10),",",".")
				
			EndIf
		
		Endif
		
		If Rat("N:", cLineRead) > 0
		
			nPos	:= RAt("$",cLineRead)
			
			
			If ( nPos > 0 )
			
				nP := aScan(aData,{|x| x[1] == "FI_NTRIB"})
				
				aData[nP,2] := StrTran(Substr(cLineRead,nPos+1,10),",",".")
				
			EndIf
		
		Endif
		
	EndIf
	
	
	Eval(bSkip)
	
	If ( At("[IMPOSTO]", FT_FReadLn()) > 0 )
		Eval(bSkipBack)
		Exit
	EndIf
	
End While

Return()

/*/{Protheus.doc} RedZImpostos
(long_description)
@type function
@since 18/09/2018
@version 1.0
@param aData, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function RedZImpostos(aData)

Local cLineRead	:= ""

Local nPos		:= 0
Local nValICMS	:= 0

Local bSkip     := {|| nG36LRecFile := FT_FRecno(),FT_FSkip(), nG036LnFile := GA036CAddLn() }
Local bSkipBack	:= {|x| FT_FGoTo(nG36LRecFile), nG036LnFile := GA036CSubLn()}

While ( !FT_FEof() )
	
	cLineRead := FT_FReadLn()
	
	//Valor base de c�lculo do ICMS que ser� inserido no campo equivalente a Al�quota(ex: se a Al�quota � 12,00%, o campo � FI_BAS12) + 
	//Valor do Campo de c�digo da base
	If ( At("T",cLineRead) > 0 .And. At("%",cLineRead) > 0 .And. At("T//",cLineRead) == 0 )
		
		nPos := At("$",cLineRead)
			
		If ( nPos > 0 )
			nValICMS := Val(Alltrim(Substr(cLineRead,nPos+1,10)))
		EndIf
		
	EndIf
	
	Eval(bSkip)
	
	If ( At("[TRECHOS]", FT_FReadLn()) > 0 ) .OR. ( At("/",FT_FReadLn()) > 0 .And. ( CtoD(Substr(FT_FReadLn(),1,10)) <> Ctod('  /  /  ' ) ) )
		Eval(bSkipBack)
		Exit
	EndIf
	
End While

Return(nValICMS)

/*/{Protheus.doc} GetDataRedZ
(long_description)
@type function
@since 18/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GetDataRedZ() 

Local aData	:= {}

aAdd(aData,{"FI_DTMOVTO",GtpCastType(,GetSx3Cache("FI_DTMOVTO","X3_TIPO"))})	//Tipo: Data
aAdd(aData,{"FI_NUMERO"	,GtpCastType(,GetSx3Cache("FI_NUMERO","X3_TIPO"))})	//Tipo: Caracter
aAdd(aData,{"FI_PDV"	,GtpCastType(,GetSx3Cache("FI_PDV","X3_TIPO"))})		//Tipo: Caracter
aAdd(aData,{"FI_SERPDV"	,GtpCastType(,GetSx3Cache("FI_SERPDV","X3_TIPO"))})	//Tipo: Caracter
aAdd(aData,{"FI_NUMREDZ",GtpCastType(,GetSx3Cache("FI_NUMREDZ","X3_TIPO"))})	//Tipo: Caracter
aAdd(aData,{"FI_GTINI"	,GtpCastType(,GetSx3Cache("FI_GTINI","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_GTFINAL",GtpCastType(,GetSx3Cache("FI_GTFINAL","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_NUMINI"	,GtpCastType(,GetSx3Cache("FI_NUMINI","X3_TIPO"))})	//Tipo: Caracter
aAdd(aData,{"FI_NUMFIM"	,GtpCastType(,GetSx3Cache("FI_NUMFIM","X3_TIPO"))})	//Tipo: Caracter
aAdd(aData,{"FI_BAS5"	,GtpCastType(,GetSx3Cache("FI_BAS5","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS7"	,GtpCastType(,GetSx3Cache("FI_BAS7","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS12"	,GtpCastType(,GetSx3Cache("FI_BAS12","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS17"	,GtpCastType(,GetSx3Cache("FI_BAS17","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS18"	,GtpCastType(,GetSx3Cache("FI_BAS18","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS19"	,GtpCastType(,GetSx3Cache("FI_BAS19","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_BAS25"	,GtpCastType(,GetSx3Cache("FI_BAS25","X3_TIPO"))})	//Tipo: Num�rico
aAdd(aData,{"FI_COD5"	,GtpCastType(,GetSx3Cache("FI_COD5","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD7"	,GtpCastType(,GetSx3Cache("FI_COD7","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD12"	,GtpCastType(,GetSx3Cache("FI_COD12","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD17"	,GtpCastType(,GetSx3Cache("FI_COD17","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD18"	,GtpCastType(,GetSx3Cache("FI_COD18","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD19"	,GtpCastType(,GetSx3Cache("FI_COD19","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_COD25"	,GtpCastType(,GetSx3Cache("FI_COD25","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_CRO"	,GtpCastType(,GetSx3Cache("FI_CRO","X3_TIPO"))})		//Tipo: Caractere
aAdd(aData,{"FI_VALCON"	,GtpCastType(,GetSx3Cache("FI_VALCON","X3_TIPO"))})	//Tipo: Caractere
aAdd(aData,{"FI_ISENTO"	,cValToChar(GtpCastType(,GetSx3Cache("FI_ISENTO","X3_TIPO")))})	//Tipo: Caractere
aAdd(aData,{"FI_NTRIB"	,cValToChar(GtpCastType(,GetSx3Cache("FI_NTRIB","X3_TIPO")))})	//Tipo: Caractere

Return(aData)
 
/*/{Protheus.doc} GA036AliqField
(long_description)
@type function
@since 18/09/2018
@version 1.0
@param nAliq, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036AliqField(nAliq) 

Local cFldBase	:= ""

cFldBase 	:= cValToChar(Int(nAliq))

Return(cFldBase)

/*/{Protheus.doc} GA036PosVenda
Importa��o do arquivo do Tipo P�s-venda
@type function
@author Fernando Radu Muscalu
@since 27/03/2017
@version 1.0
@param cArquivo, character, caminho e nome do arquivo que ser� processado
@param oView, objeto, inst�ncia da classe FwFormView
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036PosVenda(cArquivo,oView)

Local lRet      := .T.
Local lExit     := .f.
Local lNeedSrv  := .f.
Local lProssiga := .f.
Local lReducaoZ	:= .f.
Local cMsgErro  := ""
Local cLine     := ""
Local cLineOper := ""
Local cAgencia	:= oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO")
Local cEcfSeq   := ""
Local cECFSer   := ""
Local cColab    := ""
Local cLineChk  := ""
Local cCodSrv   := ""
Local cLineLinha:= ""
Local lIgnora	:= .F.
Local nHandle   := 0
Local nMax4Chk  := 20
Local aData     := {}
Local aNextTo   := {}
Local aRedZ		:= {}
Local bSkip     := {|| nG36LRecFile := FT_FRecno(),FT_FSkip(), nG036LnFile := GA036CAddLn() }
Local bSkipBack	:= {|| FT_FGoTo(nG36LRecFile), nG036LnFile := GA036CSubLn()}
Local oTable	:= GA036EObjTab()

//Abre o Arquivo
nHandle := FT_FUse(cArquivo)

If ValType(oTable) == "O"
	oTable:Delete()
	GTPDestroy(oTable)
Endif

If ( nHandle > 0 )
    
    GA036RstTrans() //Reinicia o controle dos campos do arquivo que foram lidos
    GA036CResetLn() //Reinicia a contagem  das linhas do arquivo que foram lidas
    GA036CAddLn()   //Adiciona 1 � contagem de Linhas do arquivo

    While ( !FT_FEof() )
		
        If ( !lReducaoZ .And. !lProssiga )

            If ( GA036RetLnRead() <= nMax4Chk )
                
                If ( At("ECF",cLineChk) > 0 .Or. At("SELECAO DO OPERADOR",cLineChk) > 0 ) 
                    
                    lProssiga := .t.
                    
                    GA036Scatter(cLineChk,aData)

                    //Guarda informa��o do ECF para quando houver quebra de colaborador, o n�mero de ECF ser� mantido
                    If ( Empty(cEcfSer) .And. At("ECF:",Upper(cLineChk)) > 0 )
                        cEcfSer	:= GA036RetData(aData,"GIC_ECFSER")
                        cECFSeq := SubStr(cEcfSer,4)
                        GA036SetData(aData,"GIC_ECFSEQ",cECFSeq)
                        
                        If !VldEstacaoPdv(cEcfSer,cAgencia,cArquivo,aData)
                        	lRet := .F.
                        	Exit
						Endif
                    EndIf    

                EndIf

            EndIf    

        EndIf

        cLine := FT_FReadLn() 

        If ( !lProssiga )
            cLineChk := cLine
            Eval(bSkip)
            Loop    
        EndIf    

        If ( lProssiga )

            If ( !lReducaoZ .And. !Empty(cLine) )

                If ( At("SELECAO DO OPERADOR:",Upper(cLine)) > 0 )    
                    cLineOper   := cLine
                ElseIf ( At("SELECAO DA LINHA:",Upper(cLine)) > 0 )
                	cLineLinha	:= cLine
                EndIf

                //Separa as informa��es do Arquivo de acordo com as marca��es, nos respectivos campos de GIC.
                GA036Scatter(cLine,aData)

                //Guarda informa��o do colaborador (operador) para quando houver quebra de Servi�o, mas o c�digo
                //do operador ser� mantido
                If ( At("SELECAO DO OPERADOR:",Upper(cLine)) > 0 )
                    
                    If ( cColab <> GA036RetData(aData,"GIC_COLAB") )
                        cColab :=  GA036RetData(aData,"GIC_COLAB")
                    EndIf

                EndIf

            Endif

            Eval(bSkip)
            
            If ( !lReducaoZ )
            	GA036SetData(aData,"GIC_TIPO","E")
			EndIf
			
            //Quebra cada bloco de bilhete que h� no arquivo.
            //O n�mero da Passagem ou bilhete se encontra na linha da maraca��o "DE:"
            If ( !lReducaoZ .And. Len(aData) >= nG036QtDados )    //Quebra de Bilhete

                cCodSrv := GA419Viagem( GA036RetData(aData,"GIC_LINHA"),;   //Param cLinha -> C�d. da Linha
                                        GA036RetData(aData,"GIC_LOCORI"),;  //Param cLocOri -> C�d. da Localidade de Origem (Sa�da)
                                        GA036RetData(aData,"GIC_LOCDES"),;  //Param cLocDes -> C�d. da Localidade de Destino (Chegada)
                                        GA036RetData(aData,"GIC_DTVIAG"),;  //Param dData -> Data do Servi�o/Viagem
                                        GA036RetData(aData,"GIC_HORA"))     //Param cHora -> Hora do Servi�o/Viagem     

                GA036SetData(aData,"GIC_CODSRV",cCodSrv)                            
				
				lIgnora := .F.
                //Efetua as valida��es necess�rias
                If ( GA036Valid(aData,cArquivo,@lExit,aNextTo,oView,,@lNeedSrv,@lIgnora) )    

                    G036SetGIC(aData)
                    GA036CargaTmp(aData,Alltrim(cAgencia),cArquivo,lIgnora)
                
                Else
                	//P�ra de ler o arquivo atual
                	lRet := .f.
                	Exit
					
                EndIf                 
                
                //Remove os itens que n�o condizem mais para um novo registro da GIC, 
                //mas mant�m �queles que condizem
                GA036RstData(aData)    
            
            //Fim dos dados necess�rios para as gera��es de registros em GIC
            ElseIf ( !lReducaoZ .And. At("GT:",Upper(FT_FReadLn())) > 0 )    
                
                GA036RstTrans()
                                
                While ( !FT_FEof() )
                	
                	If ( At("[RMD]",Upper(FT_FReadLn())) > 0 )
                		lReducaoZ := .t.
                		Eval(bSkipBack)
                		Exit
                	EndIf	
                	
                	Eval(bSkip)
                	
                End While
                
			ElseIf ( lReducaoZ .And. At("RMD",Upper(FT_FReadLn())) > 0 )
            	
            	aRedZ := GA036ReducaoZ(cEcfSeq,cEcfSer,cArquivo,oView,FT_FRecno())
            	
            	If ( Len(aRedZ) == 0 )
            		lRet := .f.
            		GA036ErrorLog(cAgencia,;
			        				cArquivo,;
			        				STR0111,; //"Redu��o Z n�o gerada ou n�o realizada para esse arquivo"
			        				cEcfSer,;
			        				"SFI",;
			        				"!ALL!",;
		                    		"!FL_REDUZ!",;
			        				-1,;
			        				,)
            	Endif	
            	
            	GA036RstTrans()
                Exit
            	
            EndIf
            
			If ( !lReducaoZ .And.; 
                    (!Empty(cLineOper) .And. At("SELECAO DO OPERADOR:",Upper(FT_FReadLn())) > 0 .And.; 
                        Alltrim(cLineOper) <> Alltrim(FT_FReadLn()));    //Quebra do Operador
                )
                
                GA036RstTrans()
				cLineOper := "" 
			
			ElseIf ( !lReducaoZ .And.;
						(!Empty(cLineLinha) .And. At("SELECAO DA LINHA:",Upper(FT_FReadLn())) > 0 .And.;
							Alltrim(cLineLinha) <>	Alltrim(FT_FReadLn()));
					)		
            	
            	GA036RstTrans()
            	cLineLinha := ""
            	           	
            EndIf
            
        Else

            Exit

        EndIf

    EndDo

Else
	lRet := .F.
	
    nG036CtrErro++

    //Gera Log de Falha
    cMsgErro := STR0040 + chr(13)//"N�o foi poss�vel abrir o arquivo de Importa��o. "
    cMsgErro += FError()

    GA036ErrorLog(  cAgencia,;
                    cArquivo,;
                    cMsgErro,;
                    StrZero(nG036CtrErro,TamSx3("GIC_ECFSEQ")[1]),;
                    "!FILE!",;
                    "!FL_IMPORT!",;
                    STR0041,;//"Abertura do Arquivo"
                    -1,;
                    aData)
EndIf

FT_FUse()   //Fecha o Arquivo

If ( !lProssiga )
	
    nG036CtrErro++  
    
    cMsgErro := STR0038 + Alltrim(cArquivo) + STR0037//" n�o possui o formato do Modelo P�s-Venda"//"Arquivo "
    
    GA036ErrorLog(  cAgencia,;
                    cArquivo,;
                    cMsgErro,;
                    StrZero(nG036CtrErro,TamSx3("GIC_ECFSEQ")[1]),;
                    "!FILE!",;
                    "!FL_IMPORT!",;
                    STR0039,;//"Layout do Arquivo"
                    -1)
	
ElseIf lRet
	
	//Efetuar a grava��o
	G036PersistData(cArquivo,oView,aRedZ,cEcfSeq,cEcfSer)
	
EndIf

Return
						
/*/{Protheus.doc} GA036NaoFormat
    importa��o do arquivo do tipo N�o Formatado
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cArquivo, caracter, caminho e nome do arquivo que ser� processado
            oView, objeto, inst�ncia da classe FwFormView
   
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036NaoFormat(cArquivo,oView)

Local cECF      := ""
Local cLine     := ""
Local cColab    := ""
Local cLinhaVia := ""
Local cBilhete  := ""
Local cLineChk  := ""
Local cCodSrv   := ""
Local cMsgErro	:= ""

Local nHandle   := 0
Local nMax4Chk  := 25

Local aData     := {}
Local aNextTo   := {}

Local lExit     := .f.
Local lBrkOper  := .f.
Local lBrkLinh  := .f.
Local lNeedSrv  := .f.
Local lProssiga := .f.
Local lIgnora 	:= .F.
Local bSkip     := {|| nG036LnFile := GA036RetLnRead(), FT_FSkip(), GA036CAddLn() }

//Abre o Arquivo
nHandle := FT_FUse(cArquivo)

If ( nHandle > 0 )

    GA036RstTrans() //Reinicia o controle dos campos do arquivo que foram lidos
    GA036CResetLn() //Reinicia a contagem  das linhas do arquivo que foram lidas
    GA036CAddLn()   //Adiciona 1 � contagem de Linhas do arquivo

    While ( !FT_FEof() )

        If ( !lProssiga )

            If ( GA036RetLnRead() <= nMax4Chk )
                
                If ( SubStr(cLineChk,1,2) == "EC" ) //ECF
                    lProssiga := .t.
                    GA036Scatter(cLineChk,aData,.F.)
                EndIf

            EndIf    

        EndIf

        cLine := FT_FReadLn()

        If ( !lProssiga )
            cLineChk := cLine
            Eval(bSkip)
            Loop
        EndIf     

        If ( lProssiga )

            //Leia a pr�xima linha do arquivo, caso o registro n�o seja B1, B0 ou A0
            If ( !(Substr(cLine,1,2) $ "EC|B1|B0|A0") )
                Eval(bSkip)
                Loop
            EndIf    

            If ( Substr(cLine,1,2) == "B1" )
                cColab := cLine
            ElseIf ( Substr(cLine,1,2) == "B0" )    
                cLinhaVia := cLine
            ElseIf ( Substr(cLine,1,2) == "A0" )
                cBilhete := cLine
            EndIf

            //Separa as informa��es do Arquivo de acordo com as marca��es, nos respectivos campos de GIC.
            GA036Scatter(cLine,aData,.F.)
            
            Eval(bSkip)
            
            If ( !Empty(FT_FReadLn()) )

                //Verifica quebra de passagem - Cada Passagem (A0) ir� gerar um registro em GIC
                If ( Substr(FT_FReadLn(),1,2) == "A0" .And. cBilhete <> FT_FReadLn() )
                    
                    If ( Empty(cECF) )
                        cECF := GA036RetData(aData,"GIC_ECFSEQ")
                    EndIf

                    cCodSrv := GA419Viagem( GA036RetData(aData,"GIC_LINHA"),;   //Param cLinha -> C�d. da Linha
                                            GA036RetData(aData,"GIC_LOCORI"),;  //Param cLocOri -> C�d. da Localidade de Origem (Sa�da)
                                            GA036RetData(aData,"GIC_LOCDES"),;  //Param cLocDes -> C�d. da Localidade de Destino (Chegada)
                                            GA036RetData(aData,"GIC_DTVIAG"),;  //Param dData -> Data do Servi�o/Viagem
                                            GA036RetData(aData,"GIC_HORA"))     //Param cHora -> Hora do Servi�o/Viagem 
                    
                    GA036SetData(aData,"GIC_CODSRV",cCodSrv)    
					lIgnora := .F.
                    //Efetua a valida��o dos dados que ser�o gravados em GIC
                    If ( GA036Valid(aData,cArquivo,@lExit,aNextTo,oView,.f.,@lNeedSrv,@lIgnora) )    
                        
                    	G036SetGIC(aData)
                    	GA036CargaTmp(aData,Alltrim(oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO")),cArquivo,lIgnora)
                    
                    Else

                        //O N�mero de ECF n�o foi validado, ent�o, j� pode analisar o pr�ximo arquivo,
                        //assim encerra a varredura do arquivo atual
                        If ( lExit )  
                            Exit
                        EndIf

                        //Nos casos de n�o serem validados, o colaborador (operador) ou 
                        //o c�digo da viagem (n�mero do servi�o) ou a linha, varre uma parte do
                        //arquivo at� encontrar o pr�ximo operador (marca��o: "SELECAO DO OPERADOR:")    
                        //ou o pr�ximo servi�o (marca��o: "NUMERO DO SERVICO:")
                        If ( !Empty(aNextTo) )  

                            Eval(bSkip) //Pr�xima linha
                            
                            bWhile := {|x|  x := Iif( Len(aNextTo) == 2,; 
                                                        IIf( SubStr(FT_FReadLn(),1,2) == aNextTo[1],;
                                                                Eval({|| lBrkLinh := .f., lBrkOper := .t.}),;
                                                                Eval({|| lBrkOper := .f., lBrkLinh := SubStr(FT_FReadLn(),1,2) == aNextTo[2]});
                                                            ),;
                                                        Eval({|| lBrkLinh := .f., lBrkOper := SubStr(FT_FReadLn(),1,2) == aNextTo[1]});
                                                    ) }    
                            
                            //Percorre pelo arquivo DARUMA at� encontrar os marcadores que 
                            //est�o no array aNextTo
                            While ( !FT_FEof() .And. !Eval(bWhile) )
                                Eval(bSkip)
                            EndDo

                            //Se for somente quebra de Servi�o
                            If ( lBrkLinh .Or. lBrkOper )  //repensar o bloco do If
                                GA036RstTrans()
                            EndIf
                            
                            //Volta para o come�o do While, para captar os dados desde o come�o    
                            Loop

                        Endif

                    EndIf

                    GA036RstTrans()

                EndIf

            EndIf

            If ( Upper(Substr(FT_FReadLn(),1,2)) == "GT" )
                GA036RstTrans()
                Exit
            EndIf    

        Else
            
            Exit

        EndIf

    EndDo

Else
    
    nG036CtrErro++

    //Gera Log de Falha
    cMsgErro := STR0040 + chr(13)//"N�o foi poss�vel abrir o arquivo de Importa��o. "
    cMsgErro += FError()

    GA036ErrorLog(  oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
                    cArquivo,;
                    cMsgErro,;
                    StrZero(nG036CtrErro,TamSx3("GIC_ECFSEQ")[1]),;
                    "!FILE!",;
                    "!FL_IMPORT!",;
                    STR0041,;//"Abertura do Arquivo"
                    -1)
EndIf

FT_FUse()

If ( !lProssiga )

    nG036CtrErro++
            
    GA036ErrorLog(  oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
                    cArquivo,;
                    STR0043 + Alltrim(cArquivo) + STR0102,;//" n�o caracteriza o Modelo N�o Formatado"//"Arquivo "
                    StrZero(nG036CtrErro,TamSx3("GIC_ECFSEQ")[1]),;
                    "!FILE!",;
                    "!FL_IMPORT!",;
                    STR0044,;//"Layout do Arquivo"
                    -1)

EndIf


Return

/*/{Protheus.doc} GA036Scatter
    Carrega os dados necess�rios para gera��o dos bilhetes.
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cLine, caractere, Linha do arquivo para a capta��o dos dados
            aData, array, array com os dados carregados
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            lPosVenda, l�gico, .t. - arquivo p�s Venda; .f. - arquivo n�o formatado    
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Scatter(cLine,aData,lPosVenda)

Local nPos  := 0
Local nP    := 0
Local nI    := 0
Local nIni  := 0

Default lPosVenda := .t.

If ( Len(aG036Trans) == 0 )
    GA036SetTrans(lPosVenda)
EndIf

If ( lPosVenda )

    nIni := aScan(aG036Trans,{|x| !x[6]})

    If ( nIni == 0)
        nIni := 1
    EndIf

    For nI := nIni to Len(aG036Trans)

        If ( !aG036Trans[nI,Len(aG036Trans[nI])] )

            If ( ( nPos := At(aG036Trans[nI,1],Upper(cLine)) ) > 0 )
                
                Eval(aG036Trans[nI,5],aData,nI,cLine,nPos)
                aG036Trans[nI,Len(aG036Trans[nI])] := .T.

                nP := aScan(aG036Trans,{|x| x[4] == aG036Trans[nI,4] .And. !x[6] })

                If ( nP > 0 .And. nP <> nI )
                    
                    If ( ( nPos := At(aG036Trans[nP,1],Upper(cLine)) ) > 0 )

                        Eval(aG036Trans[nP,5],aData,nP,cLine,nPos)    
                        aG036Trans[nP,Len(aG036Trans[nP])] := .T.
                        
                        Exit    

                    EndIf
                
                Else
                    Exit
                EndIf

            EndIf

        Endif

    Next nI

Else    
    GA036RecursiveScat(cLine,aData)
EndIf

Return()

/*/{Protheus.doc} GA036RecursiveScat
    Carrega os dados necess�rios para gera��o dos bilhetes, quando se 
    tratar do tipo de arquivo N�o Formatado. Esta fun��o e recursiva e sua ideia � agilizar
    e captar diversos dados em cada linha do arquivo que � lida.
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cLine, caractere, Linha do arquivo para a capta��o dos dados
            aData, array, array com os dados carregados
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            nInd, num�rico, �ndice utilizado no array est�tico aG036Trans. Este array
            representa uma transcri��o do tipo de registro ou marca do arquivo importado
            para o campo da tabela GIC, cujo o dado ser� gravado    
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036RecursiveScat(cLine,aData,nInd)

Local nIndex    := aScan(aG036Trans,{|x| Substr(x[1],1,2) == Substr(cLine,1,2) })
Local nLen      := 0

Default nInd    := nIndex

If ( nInd > 0 .And. !(aG036Trans[nInd,6]) )

    If ( aG036Trans[nInd,1] == "ECF:" )
        nLen := 1
    EndIf    

    Eval(aG036Trans[nInd,5],aData,nInd,cLine,nLen) 
    
    aG036Trans[nInd,6] := .t.
    
    nIndex := aScan(aG036Trans,{|x| x[4] == aG036Trans[nInd,4] .And. !x[6]} )

    If ( nIndex > 0 )
        GA036RecursiveScat(cLine,aData,nIndex)
    EndIf    

EndIf

Return()
    
/*/{Protheus.doc} GTPStrNamed    
    Fun��o que dever� captar um conte�do espec�fico dentro da linha do arquivo pos-venda que 
    est� sendo importado

    O Par�metro cExpression deve ser uma fun��o que receba como �nico par�metro valor do tipo caractere 
    e devolva um valor l�gico de prefer�ncia, avaliando o conte�do da string passada por par�metro. 
    Por exemplo, fun��es como 'IsDigit' ou 'IsAlpha' s�o valores v�lidos para o par�metro cExpression

    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cTexto, caractere, Texto que se deseja captar o c�digo da localidade
            cToken, caractere, Define qual caractere � utilizado como marca��o
            para a localiza��o do conte�do espec�fico desejado.
            cExpression, caractere, Fun��o que verifica se conte�do espec�fico procurado � v�lido.
            nPosIni, num�rico, nro da coluna inicial na linha ou texto que se analisa.
    @return cString, caractere, Conte�do espec�fico desejado. Por exemplo, localidade de origem ou de destino
    @example
    (examples)
    @see (links_or_references)
/*/
Function GTPStrNamed(cTexto,cToken,cExpression,nPosIni)

Local cString   := ""
Local cAux      := ""

Local nPPar     := 0

Local aPares    := {}

Default cExpression := "IsDigit"

aAdd(aPares,{"(",")"})
aAdd(aPares,{"[","]"})
aAdd(aPares,{"{","}"})

nPPar := aScan(aPares,{|x| x[1] == cToken})
cAux := Substr(cTexto,nPosIni)    
cAux := Substr(cAux,At(cToken,cAux)) 

If ( nPPar > 0 .And. !Empty(cAux) )
    
    cAux2 := Substr(cAux,2,At(aPares[nPPar,2],cAux)-2)
	
	If ( !Empty(cAux2) )
	
	    If !( &(cExpression + "('" + cAux2 + "')") )
	         cString := GTPStrNamed(cAux,cToken,cExpression,At(aPares[nPPar,2],cAux)+1)
	    EndIf
	
	EndIf
	
EndIf

cString := cAux2

Return(cString)

/*/{Protheus.doc} GA036SetTrans
    
    Inicializa o array est�tico aG036Trans. Este array representa uma transcri��o das 
    marca��es ou registros contidos nos arquivos da Daruma, tipos pos-venda ou n�o formatados, para
    os respectivos campos da tabela de bilhetes (Passagens) - GIC

    Mapa do array est�tico aG036Trans
    aG036Trans[n,1] - Caractere: Campo do Arquivo DARUMA
    aG036Trans[n,2] - Caractere: Campo da tabela GIC
    aG036Trans[n,3] - Num�rico: Tamanho do campo (em rela��o ao arquivo e n�o a tabela)
    aG036Trans[n,4] - Num�rico: Indicador de Linha de leitura. Utilizado para pegar todos os dados da mesma linha
    aG036Trans[n,5] - Bloco: Bloco de C�digo a ser executado
                    Params: a - Array que ser� atualizado (ex: aData)
                            b - n�mero da Linha atual posicionada no array aG036Trans
                            c - Conte�do string com a Linha que est� posicionado no arquivo da DARUMA
                            d - n�mero da coluna do arquivo DARUMA que inicia o valor de conte�do desejado.
                            e - varia conforme a necessidade do bloco
    aG036Trans[n,7] - L�gico: .T. diz que esta linha do array j� foi processada
    
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param lPosVenda, l�gico, .t. - tipo de arquivo p�s-venda; .f. - tipo de arquivo n�o formatado
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036SetTrans(lPosVenda)

Local bExecute      := {|a,b,c,d,e| GA036Data(a,b,c,d,lPosVenda)}

Default lPosVenda   := .T.

aG036Trans := {}

aAdd(aG036Trans,{"ECF:","GIC_ECFSER",6,1,bExecute,.f.})
aAdd(aG036Trans,{"ECF:","GIC_ECFSEQ",6,1,bExecute,.f.})

If ( lPosVenda )
    
	aAdd(aG036Trans,{"SELECAO DO OPERADOR:","GIC_COLAB",6,2,bExecute,.f.})
	aAdd(aG036Trans,{"NUMERO DO SERVICO:","GIC_CODSRV",6,3,bExecute,.f.})
	aAdd(aG036Trans,{"SELECAO DA LINHA:","GIC_LINHA",6,4,bExecute,.f.})
	aAdd(aG036Trans,{"SENTIDO:","GIC_SENTID",5,5,bExecute,.f.})
	aAdd(aG036Trans,{"HORA:","GIC_HORA",5,5,bExecute,.f.})
	aAdd(aG036Trans,{"DE:","GIC_LOCORI",6,6,bExecute,.f.})
	aAdd(aG036Trans,{"PARA:","GIC_LOCDES",6,6,bExecute,.f.})
	aAdd(aG036Trans,{"TARIFA:","GIC_TAR",12,7,bExecute,.f.})
	aAdd(aG036Trans,{"SEGURO:","GIC_SGFACU",12,8,bExecute,.f.})
	aAdd(aG036Trans,{"PEDAGIO:","GIC_PED",9,8,bExecute,.f.})
	
    nG036QtDados := Len(aG036Trans) + 4

Else
    
    aAdd(aG036Trans,{"B1:018;006","GIC_COLAB",,2,bExecute,.f.})
    aAdd(aG036Trans,{"B0:004;008","GIC_DTVIAG",,3,bExecute,.f.})
    aAdd(aG036Trans,{"B0:018;006","GIC_LINHA",,3,bExecute,.f.}) //Este caso aqui est� estranho
    aAdd(aG036Trans,{"B0:054;001","GIC_SENTID",,3,bExecute,.f.})
    aAdd(aG036Trans,{"B0:056;004","GIC_HORA",,3,bExecute,.f.})
    aAdd(aG036Trans,{"B0:111;006","GIC_CODSRV",,3,bExecute,.f.})
    aAdd(aG036Trans,{"A0:004;008","GIC_DTVEND",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:013;004","GIC_HRVEND",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:018;006","GIC_BILHET",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:042;006","GIC_LOCORI",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:066;006","GIC_LOCDES",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:078;006","GIC_TAR",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:093;006","GIC_SGFACU",,4,bExecute,.f.})
    aAdd(aG036Trans,{"A0:102;006","GIC_PED",,4,bExecute,.f.})
    
    nG036QtDados := Len(aG036Trans)

EndIf

Return()

/*/{Protheus.doc} nomeFunction
    Fun��o executada atrav�s de bloco de c�digo, contido em elemento no array est�tico aG036Trans. Ela � respons�vel por efetuar a Atualiza��o
    do array que conter� os dados que ser�o persistidos na tabela GIC.
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            nPTrans, num�rico, �ndice para o elemento do array est�ti aG036Trans
            cRow, caractere, linha lida do arquivo importado
            nCol, num�rico, coluna da linha lida do arquivo importado
            lPosVenda, l�gico, .t. - tipo de arquivo pos-venda; .f. - tipo de arquivo n�o formatado
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Data(aData,nPTrans,cRow,nCol,lPosVenda)

Local aPos  := {}

Local nP    := aScan(aData,{|x| x[1] == aG036Trans[nPTrans,2]}) //Pesquisa o campo que ser� atualizado
Local nP2   := 0

Local xData := nIl

If ( aG036Trans[nPTrans,1] == "ECF:" )
    xData := SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1]) + 1, aG036Trans[nPTrans,3])
Else

    If ( lPosVenda )

        Do Case
        Case ( aG036Trans[nPTrans,1] $ "SELECAO DO OPERADOR:" )//( aG036Trans[nPTrans,1] $ "SELECAO DO OPERADOR:|NUMERO DO SERVICO:" )

            xData := SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1]) + 1, aG036Trans[nPTrans,3])

        Case ( aG036Trans[nPTrans,1] $ "SELECAO DA LINHA:" )

            //Data da Viagem
            nP2 := aScan(aData,{|x| x[1] == "GIC_DTVIAG"}) //Pesquisa o campo que ser� atualizado

            If ( nP2 > 0 )

                aData[nP2,2] := CToD(SubStr(cRow,1,10))
                aData[nP2,3] := GA036RetLnRead()

            Else

                aAdd(aData, {"GIC_DTVIAG",;
                            CToD(SubStr(cRow,1,10)),;
                            GA036RetLnRead()})

            EndIf

            //Linha
            xData := SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1]) + 1, aG036Trans[nPTrans,3])
            
        Case ( aG036Trans[nPTrans,1] $ "SENTIDO:" )

            xData := IIf(Alltrim(Upper(SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1]) + 1,aG036Trans[nPTrans,3]))) == "IDA","1","2")

        Case ( aG036Trans[nPTrans,1] $ "HORA:" )

            xData := StrTran(SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1]) + 1,aG036Trans[nPTrans,3]),":","")

        Case ( aG036Trans[nPTrans,1] $ "DE:|PARA:" )

            If ( aG036Trans[nPTrans,1] == "DE:" )

                //Data de Venda
                nP2   := aScan(aData,{|x| x[1] == "GIC_DTVEND"}) //Pesquisa o campo que ser� atualizado
                
                xData := CToD(SubStr(cRow,1,10))
                
                If ( nP2 > 0 )

                    aData[nP2,2] := xData
                    aData[nP2,3] := GA036RetLnRead()

                Else

                    aAdd(aData, {"GIC_DTVEND",;
                                xData,;
                                GA036RetLnRead()})

                EndIf

                //Hora da Venda
                nP2   := aScan(aData,{|x| x[1] == "GIC_HRVEND"}) //Pesquisa o campo que ser� atualizado
                
                xData := StrTran(SubStr(cRow,12,5),":","")
                
                If ( nP2 > 0 )

                    aData[nP2,2] := xData
                    aData[nP2,3] := GA036RetLnRead()

                Else

                    aAdd(aData, {"GIC_HRVEND",;
                                xData,;
                                GA036RetLnRead()})

                EndIf

            EndIf

            //Local de Origem/Local de Destino
            xData	:= GTPStrNamed(cRow,"(","IsDigit",nCol + Len(aG036Trans[nPTrans,1]) + 1)
			xData	:= StrZero(vAL(xData),TamSx3('GI1_COD')[1])
			
			If aG036Trans[nPTrans,1] == "PARA:" .and. GA036RetData(aData,"GIC_SENTID") == '2'
				Ga036AltVolta(aData,xData)
			Endif
			
        Case ( aG036Trans[nPTrans,1] $ "TARIFA:|SEGURO:|PEDAGIO:" )

                If ( aG036Trans[nPTrans,1] == "TARIFA:" )

                    //Bilhete
                    nP2   := aScan(aData,{|x| x[1] == "GIC_BILHET"}) //Pesquisa o campo que ser� atualizado
                    
                    xData := SubStr(cRow,1,6)
                    
                    If ( nP2 > 0 )

                        aData[nP2,2] := xData
                        aData[nP2,3] := GA036RetLnRead()

                    Else

                        aAdd(aData, {"GIC_BILHET",;
                                    xData,;
                                    GA036RetLnRead()})

                    EndIf
					
					//Aliquota
                    nP2   := aScan(aData,{|x| x[1] == "GIC_ALICMS"}) //Pesquisa o campo que ser� atualizado
                    
                    xData := Val(StrTran(StrTran(SubStr(cRow,53,5),"R$",""),",","."))
                    
                    If ( nP2 > 0 )

                        aData[nP2,2] := xData
                        aData[nP2,3] := GA036RetLnRead()

                    Else

                        aAdd(aData, {"GIC_ALICMS",;
                                    xData,;
                                    GA036RetLnRead()})

                    EndIf
                    
                EndIf

                //Tarifa, Seguro ou Ped�gio
                xData := Val(StrTran(StrTran(SubStr(cRow,nCol + Len(aG036Trans[nPTrans,1])+1,aG036Trans[nPTrans,3]),"R$",""),",","."))

        EndCase

    Else
        
        aPos := Separa(SubStr(aG036Trans[nPTrans,1],4),";")

        If ( aG036Trans[nPTrans,2] $ "GIC_HORA|GIC_HRVEND" )
            xData := GTPCastType(Substr(cRow,Val(aPos[1]),Val(aPos[2])),"C","9999")
        ElseIf ( Alltrim(aG036Trans[nPTrans,2]) $ "GIC_TAR|GIC_SGFACU|GIC_PED" )
            xData := Val(SubStr(Substr(cRow,Val(aPos[1]),Val(aPos[2])),1,4) + "." + SubStr(Substr(cRow,Val(aPos[1]),Val(aPos[2])),5))
        ElseIf ( Substr(aG036Trans[nPTrans,2],5,2) == "DT" )
            xData := CToD(GTPCastType(Substr(cRow,Val(aPos[1]),Val(aPos[2])),"C","@R 99/99/9999"))    
        Else    
            xData := Substr(cRow,Val(aPos[1]),Val(aPos[2]))

            If ( xData == "I" )
                xData := "1"
            ElseIf (xData == "V")
                xData := "2"    
            EndIf

        EndIf                   

    EndIf

EndIf

If ( nP > 0 )
        
    aData[nP,2] := xData
    aData[nP,3] := GA036RetLnRead()

Else

    aAdd(aData, {aG036Trans[nPTrans,2],;
                xData,;
                GA036RetLnRead()})

EndIf

Return()

/*/{Protheus.doc} GA036GetTrans
    Fun��o que retorna o array est�tico aG036Trans
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return aG036Trans, array, array est�tico que � a transcri��o entre marca��es ou registros do arquivo 
    importado e os campos da tabela GIC.
        aG036Trans[n,1] - Caractere: Campo do Arquivo DARUMA
        aG036Trans[n,2] - Caractere: Campo da tabela GIC
        aG036Trans[n,3] - Num�rico: Tamanho do campo (em rela��o ao arquivo e n�o a tabela)
        aG036Trans[n,4] - Num�rico: Indicador de Linha de leitura. Utilizado para pegar todos os dados da mesma linha
        aG036Trans[n,5] - Bloco: Bloco de C�digo a ser executado
                        Params: a - Array que ser� atualizado (ex: aData)
                                b - n�mero da Linha atual posicionada no array aG036Trans
                                c - Conte�do string com a Linha que est� posicionado no arquivo da DARUMA
                                d - n�mero da coluna do arquivo DARUMA que inicia o valor de conte�do desejado.
                                e - varia conforme a necessidade do bloco
        aG036Trans[n,7] - L�gico: .T. diz que esta linha do array j� foi processada

    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036GetTrans()
Return(aG036Trans)

/*/{Protheus.doc} GA036RstTrans
    Inicializa o campo do array est�tico aG036Trans para determinar que um dado ainda n�o foi captado para um 
    campo espec�fico na tabela GIC.
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param cListReset, caractere, Sequ�ncia de marca��es ou registros que ser�o inicializados
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036RstTrans(cListReset)

Local nI := 0

Default cListReset := ""

For nI := 1 to Len(aG036Trans)

    If ( Empty(cListReset) )
        aG036Trans[nI,6] := .f.
    ElseIf ( Upper(Alltrim(aG036Trans[nI,1])) $ Upper(cListReset) )
        aG036Trans[nI,6] := .f.
    EndIf    

Next nI

Return()

/*/{Protheus.doc} GA036RstData
    Reinicializa o array aData - respons�vel pelos dados que ser�o persistidos na tabela GIC
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036RstData(aData)

Local aCloned       := aClone(aData)

Local cListReset    := "DE:/PARA:/TARIFA:/SEGURO:/PEDAGIO:/A0"

Local nI    := 0
Local nP    := 0
Local nP2   := 0
Local nCont := 0

For nI := 1 to Len(aCloned)

    nP := aScan(aG036Trans,{|x| Alltrim(Upper(x[2])) == aCloned[nI,1]})

    If ( nP > 0 .And. aG036Trans[nP,1] $ cListReset )
        
        If ( (nP2 := aScan(aData,{|x| x[1] == aCloned[nI,1] })) > 0 )
            
            aDel(aData,nP2)
            nCont++
            
            Do Case
            Case ( aG036Trans[nP,1] == "DE:" )

                If ( (nP2 := aScan(aData, {|x| x[1] == "GIC_DTVEND"})) > 0 )

                    aDel(aData,nP2)
                    nCont++
                
                    If ( (nP2 := aScan(aData, {|x| x[1] == "GIC_HRVEND"})) > 0 )
                        
                        aDel(aData,nP2)
                        nCont++

                    EndIf

                EndIf

            Case ( aG036Trans[nP,1] == "TARIFA:" ) 
                
                If ( (nP2 := aScan(aData, {|x| x[1] == "GIC_BILHET"})) > 0 )
                        
                    aDel(aData,nP2)
                    nCont++

                EndIf   

            EndCase

        Endif

    EndIf

Next nI

GA036RstTrans(cListReset)

aSize(aData,Len(aData)-nCont)

Return()

/*/{Protheus.doc} GA036Valid
    Efetua as valida��es necess�rias para que seja importado o bilhete.
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            cArquivo, caractere, caminho e arquivo que est� sendo importado
            lNextFile, l�gico, .t. - deve terminar a leitura do arquivo atual e ler o pr�ximo arquivo
            aNextLine, array, array que demonstra em qual entidade (marca) ser� efetuada a quebra no arquivo, 
                                dependendo da valida��o que pegou inconsist�ncia.
            oView, objeto, inst�ncia da classe FwFormView
            lPosVenda, l�gico, .t. - tipo de arquivo de pos-venda; .f. - tipo de arquivo n�o formatado
            lNeedSet, l�gico, .t. - � necess�rio efetuar o acerto do c�digo do servi�o do bilhete importado.
    @return lRet, l�gico, .t. - validado com sucesso
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Valid(aData,cArquivo,lNextFile,aNextLine,oView,lPosVenda,lNeedSet,lIgnora)

Local cMsgErro  := ""
Local cValue    := ""
Local cField    := ""
Local cLblColab := ""
Local lRet      := .t.
Local aArea     := {}

Default lNextFile := .f.
Default aNextLine := {}
Default lPosVenda := .t.   
Default lNeedSet := .f.
Default lIgnora		:= .F.

//Para Passagens sem servi�os, dever� ser atualizado um arquivo Tempor�rio
If ( lNeedSet )
    lNeedSet := .f.
EndIf    

cLblColab := IIf(lPosVenda,"SELECAO DO OPERADOR:","B1")

//Checa se n�o existe o bilhete cadastrado.
If ( !GA036ChkPass(aData) )
	
	//Valida��o se o colaborador existe
    If ( lRet )

        aArea := GYG->(GetArea())
        
        cValue := GA036RetData(aData,"GIC_COLAB")

        If !Posicione('GYG',1,xFilial('GYG')+cValue,"GYG_CODIGO") == cValue
            If !Posicione('GYG',2,xFilial('GYG')+cValue,"GYG_FUNCIO") == cValue

                cField := "GIC_COLAB"
                
                lRet := .f.

                cMsgErro := STR0051 + Alltrim(cValue) + STR0050//" - n�o existe cadastrado!"//"O colaborador - c�digo: "
                
                aNextLine := {cLblColab}
            Else 
                GA036SetData(aData,"GIC_COLAB",Posicione('GYG',2,xFilial('GYG')+cValue,"GYG_CODIGO"))
            Endif
        EndIf    

    EndIf
    
    //Valida se a Linha de transporte de passageiros existe
    If ( lRet )
        
        aArea := GI2->(GetArea())
        
        cValue := GA036RetData(aData,"GIC_LINHA")

        GI2->(DbSetOrder(4))    //GI2_FILIAL+GI2_CODIGO+GI2_HIST

        If ( !GI2->(DbSeek(XFilial("GI2") + cValue +"2" )) )
            
            lRet := .f.

            cMsgErro := STR0055 + Alltrim(cValue) + STR0054        //" - n�o existe cadastrada!"//"A Linha - c�digo: "
       
        EndIf

        RestArea(aArea)

        If ( !LRet )
            
            aNextLine := {cLblColab}
            
            cField := "GIC_LINHA"
            
        EndIf


    EndIf

  	If !lRet     
  
        GA036ErrorLog(oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
        				cArquivo,;
        				cMsgErro,;
        				GA036RetData(aData,"GIC_ECFSEQ"),;
        				"GIC",;
        				cField,;
        				GA036RetData(aData,cField),;
        				GA036RetData(aData,cField,.t.),;
        				aData)
   
    EndIf

Else   
    
    //lRet := .f.
	lIgnora := .T.

    cMsgErro := STR0062// + chr(13) + chr(10)//"J� existe este Bilhete, Servi�o, Linha e Colaborador cadastrado!"
    
    cField := "GIC_BILHET"
    
    GA036ErrorLog(oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
    				cArquivo,;
    				cMsgErro,;
    				GA036RetData(aData,"GIC_ECFSEQ"),;
    				"GIC",;
    				cField,;
    				GA036RetData(aData,cField),;
    				GA036RetData(aData,cField,.t.),;
    				aData,;
    				'2')


EndIf

Return(lRet)

/*/{Protheus.doc} GA036RetData
    Busca o conte�do do campo da tabela GIC que ser� atualizado, dentro do array aData
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            cField, caractere, Campo pesquisado
            lRetLine, l�gico, .t. - retorna qual � a linha do arquivo referente ao campo pesquisado;
                                .f. - retorna o conte�do referente ao campo pesquisado
    @return xRet, qualquer, cone�do referente ao campo pesquisado
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036RetData(aData,cField,lRetLine)

Local xRet  := nil

Local nP    := aScan(aData,{|x| Alltrim(Upper(x[1])) == Alltrim(Upper(cField))})

Default lRetLine := .f.

If ( nP > 0 )
    
    If ( !lRetLine )
        xRet := aData[nP,2]
    Else
        xRet := aData[nP,3]
    EndIf

EndIf

Return(xRet)

/*/{Protheus.doc} GA036RetData
    Atualiza o conte�do do campo da tabela GIC, localizado no Array aData. 
    @type  Function
    @author Fernando Radu Muscalu
    @since 07/04/2017
    @version 1
    @param  cField, caractere, Campo pesquisado
            xContent, qualquer, conte�do que atualizar� o campo (cField)
            lLine, l�gico,  .t. - Atualiza o Elemento [3] - que � o nro da linha do arquivo;
                            .f. - Atualiza o Elemento [2] - que � o conte�do do campo
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036SetData(aData,cField,cContent,lLine)

Local nP    := aScan(aData,{|x| Alltrim(Upper(x[1])) == Alltrim(Upper(cField))})

Default lLine := .f.

If ( nP > 0 )
    
    If ( !lLine )
        aData[nP,2] := cContent
    Else
        aData[nP,3] := cContent
    EndIf
Else
	
	If ( !Empty(cContent) .And. cField == "GIC_CODSRV" )
		
		If ( !lLine )
		
			aAdd(aData, {"GIC_CODSRV",;
	        	        cContent,;
                		GA036RetLnRead()})
		Else
			aAdd(aData, {"GIC_CODSRV",;
	        	        "",;
                		cContent})
		EndIf
		
	EndIf
	
EndIf

Return()


/*/{Protheus.doc} GA036ErrorLog
    Atualiza o log de erros
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cAgencia, caractere, c�digo da ag�ncia
            cArquivo, caractere, pasta e arquivo
            cMsgErro, caractere, mensagem de erro
            cECF, caractere, nro do ECF
            cTable, caractere, alias da Tabela
            cField, caractere, campo da tabela
            xValue, qualquer, conte�do que seria atualizado
            nLine, num�rico, nro da linha do arquivo
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036ErrorLog(cAgencia,cArquivo,cMsgErro,cECF,cTable,cField,xValue,nLine,aData,cTpMsg)

Local aDataCab := { cAgencia,;  //[XXX_AGENCI]
                    Posicione("GI6",1,XFilial("GI6")+cAgencia,"GI6_DESCRI"),;
                    cArquivo,;
                    dDataBase,;
                    Time(),;
                    cECF }

Local aDataDet := {}

Default cECF    := ""
Default cTable  := ""
Default cField  := ""
Default nLine   := 0
Default aData	:= {}
Default cTpMsg	:= '1' //Erro

aDataDet := {   cECF,;
                StrZero(nLine,6),;
                cTable,;
                cField,;
                cMsgErro,;
                GTPCastType(xValue,"C"),; 
                aClone(aData),;
                cTpMsg}

GA036CSetLog(aDataCab,aDataDet)

Return()

/*/{Protheus.doc} GA036AliasTable
    Retorna o conte�do da vari�vel est�tica cG036Table. Alias da tabela do ResultSet
    da Lista de Ag�ncias utilizada no Wizard
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return cG036Table, caractere, alias do ResultSet
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036AliasTable()
Return(cG036Table)

/*/{Protheus.doc} GA036Destroy
    Fun��o que destroy (finaliza) as inst�ncias da Classe FwTemporaryTable - que foram
	utilizadas para a montagem das tabelas tempor�rias. Esta fun��o tamb�m zera as vari�veis 
    est�ticas do fonte
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
    /*/
Static Function GA036Destroy(oView)

//Mata arquivo tempor�rio (ResultSet de Ag�ncias)
If ( !Empty(cG036Table) .And. Select(cG036Table) > 0 )
    (cG036Table)->(DbCloseArea())
EndIf

//Limpa as vari�veis estaticas do fonte GTPA036.PRW
aG036Radio      := {}
aG036Trans      := {}    //Array De Para dos campos do arquivo DARUMA para o respectivo campo da tabela GIC
aG036Log        := {}    //Array com o Log de Erros que ocorreram nas importa��es 
nG036LnFile     := 0
nG036CtrErro    := 0
nG036QtDados    := 0
lG036First      := .T.

//Finaliza a inst�ncia da Classe FwFormView
If ( Valtype(oView) == "O" .And. oView:ClassName() == "FWFORMVIEW" )

    If ( oView:IsActive() )
        oView:DeActivate()
    EndIf   

    oView:Destroy()

EndIf
	
//Destr�i os arquivos tempor�rios de Log de Erros
GA036CDestroy()
//Dest�i arquivo tempor�rio de Ajuste de Bilhetes - Interface para inclus�o de servi�o
GA036EDestroy()
//Limpa o Array de Log
GA036CLogReset()

Return()

/*/{Protheus.doc} GA036ChkPass
    Verifica se o bilhete (passagem) j� existe no sistema
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado

    @return lRet, l�gico, .t. validado com sucesso
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036ChkPass(aData)

Local lRet      := .t.
Local cNxtAlias := GetNextAlias()

BeginSQL Alias cNxtAlias

    SELECT
        DISTINCT 
        GIC_ECFSEQ,
        1 EXISTE
    FROM
        %Table:GIC% GIC
    WHERE
        GIC_FILIAL = %XFilial:GIC%
        AND GIC_ECFSEQ = %Exp:GA036RetData(aData,"GIC_ECFSEQ")%
        AND GIC_ECFSER = %Exp:GA036RetData(aData,"GIC_ECFSER")%
        AND GIC_DTVEND = %Exp:GA036RetData(aData,"GIC_DTVEND")%
        AND GIC_BILHET = %Exp:GA036RetData(aData,"GIC_BILHET")%        
        AND GIC_LINHA = %Exp:GA036RetData(aData,"GIC_LINHA")%        
        AND GIC.%NotDel%
EndSQL

lRet := (cNxtAlias)->(EXISTE) == 1


(cNxtAlias)->(DbCloseArea())

Return(lRet)

/*/{Protheus.doc} GA036CpyECF
Fun��o responsavel para realizar c�pia de uma maquina de uma agencia para outra agencia
@type function
@author jacomo.fernandes
@since 24/08/2018
@version 1.0
@param cECFSer, character, (Descri��o do par�metro)
@param cAgeAux, character, (Descri��o do par�metro)
@param cAgencia, character, (Descri��o do par�metro)
@param cMsgErro, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036CpyECF(cECFSer,cAgeAux,cAgencia,cMsgErro)

Local oModelECF := FwLoadModel("GTPA013")
Local oMdlGZ2	:= oModelECF:GetModel('GZ2MASTER')
Local oStrGZ2	:= oMdlGZ2:GetStruct() 
Local aStrGZ2	:= oStrGz2:GetFields()
Local lRet      := .t.

Local aAreaGZ2  := GZ2->(GetArea())
Local aErro     := {}
Local n1		:= 0

oStrGZ2:SetProperty('GZ2_CODIGO',MODEL_FIELD_VALID, {|| AllwaysTrue() })

If ( !Empty(cECFSer) )

    GZ2->(DbSetOrder(1))

    If (GZ2->(DbSeek(XFilial("GZ2") + cECFSer + cAgeAux)))

        oModelECF:SetOperation(MODEL_OPERATION_INSERT)
        oModelECF:Activate()
        For n1	:= 1 To Len(aStrGZ2)
        	If !oMdlGZ2:SetValue(aStrGZ2[n1][3],GZ2->&(aStrGZ2[n1][3]))
        		lRet	:= .F.
        		Exit
        	Endif
        Next
        oMdlGZ2:SetValue('GZ2_AGENCI',cAgencia)
	
        If ( lRet )
            
            If ( oModelECF:VldData() )
                lRet := oModelECF:CommitData()
            Else
                lRet := .f.    
            EndIf
		Endif
		
        If ( !lRet )

            aErro := oModelECF:GetErrorMessage()

            cMsgErro := Alltrim(aErro[6]) + ". " + Alltrim(aErro[7])

        EndIf
        oModelECF:DeActivate()
	Else
		lRet		:= .F.
		cMsgErro	:= STR0112 // "ECF n�o encontrada para realizar � c�pia"
    EndIf

EndIf

RestArea(aAreaGZ2)

//Mata o Model de GZ2
oModelECF:Destroy()

Return(lRet)

/*/{Protheus.doc} GA036CargaTmp
    Carrega arquivo tempor�rio com os bilhetes que n�o possuem servi�o. Este 
    arquivo � baseado na tabela GIC
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  aData, array, array com os dados para persist�ncia na tabela GIC
                aData[n,1], caractere, nome do campo da tabela de GIC
                aData[n,2], qualquer, conte�do que ir� ser atribu�do para o campo
                aData[n,3], num�rico, nro da linha do arquivo em que o dado foi captado
            cAgencia, caractere, c�digo da ag�ncia
            cArquivo, caractere, diret�rio e nome do arquivo    
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036CargaTmp(aData,cAgencia,cArquivo,lIgnora)

Local aStructMVC    := {}
Local cAliasGIC     := ""
Local nI            := 0
Local oTabela       := Nil

oTabela    := GA036EObjTab()

If ( ValType(oTabela) == "U" )

    //Montagem do Arquivo de Tempor�rio de Ajustes de Passagens - Este arquivo � espelhado na tabela GIC
    aStructMVC := GA036ETable()

    //Configura os arrays est�ticos aG36ESMdlStr e aG36ESVwStr do programa GTPA036E 
    G036ESetStruct(aStructMVC[1],aStructMVC[2])

    //Retorna a inst�ncia da FwTemporaryTable que foi utilizada para criar a tabela tempor�ria
    oTabela    := GA036EObjTab()

EndIf

//Capta o Alias da Tabela Tempor�ria
cAliasGIC := oTabela:GetAlias()

//Inclui os dados na tabela tempor�ria
RecLock(cAliasGIC,.t.)

(cAliasGIC)->(GIC_AGENCI) 	:= cAgencia
(cAliasGIC)->(GIC_ARQUIV) 	:= cArquivo
(cAliasGIC)->(LN_FILE)		:= StrZero(GA036RetLnRead(),6)
(cAliasGIC)->(IGNORA)		:= If(lIgnora,'T','F')

For nI := 1 to Len(aData)
    (cAliasGIC)->&(aData[nI,1]) := aData[nI,2]
Next nI

(cAliasGIC)->(MsUnlock())

Return()


/*/{Protheus.doc} GA036CpyFile
    Efetua a c�pia do arquivo importado para dentro da pasta Lidos
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param  cPath, caractere, diret�rio de origem
            cFile, caractere, nome do arquivo
            lMove, l�gico, .t. - apaga o arquivo no diret�rio de origem
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036CpyFile(cPath,cFile,lMove)

Local cNewPath  := ""

Default lMove   := .f.

If cFile = "PA050820.TXT"
    Return
Endif

If ( !Empty(cPath) .And. !Empty(cFile) )
    cNewPath := cPath + "\" + "Lidos"
    FwMakeDir(cNewPath)
EndIf

__CopyFile(cPath+"\"+cFile,cNewPath+"\"+cFile)

If ( lMove )
    
    If ( File(cNewPath+"\"+cFile) )
        FErase(cPath+"\"+cFile)
    EndIf

EndIf

Return()

/*/{Protheus.doc} G036SetView
    Montagem da View do MVC da Listagem de Ag�ncias para os arquivos de tipos: P�s-venda e N�o Formatado
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param oFwLayer, objeto, Inst�ncia da classe FWLayer
    @return oView, objeto, inst�ncia da Classe FwForm
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function G036SetView(oFwLayer,oPanel)

Local oWin
Local oView

Default oFwLayer := nil

If ( Valtype(oFwLayer) == "U" )

    oFWLayer := FWLayer():New()

    oFWLayer:Init( oPanel, .F.)
    oFWLayer:AddCollumn("C1", 100, .T.)
    oFWLayer:AddLine("L1", 100)

EndIf

oFWLayer:addWindow("C1", "WIN02", STR0025, 100,.F., .F., {|| Nil })		//"Planilha de C�lculo"//"Marque as Ag�ncias desejadas"

oWin    := oFWLayer:getWinPanel("C1", "WIN02")

If aG036Radio[1] <> 3
    oView := FWLoadView("GTPA036A") 
Else
    oView := FWLoadView("GTPA036D")
Endif	    

oView:SetOperation(MODEL_OPERATION_UPDATE)
oView:SetOwner(oWin)
oView:Activate()

Return(oView)

/*/{Protheus.doc} G036GICData
    Fun��o para retornar o Array Est�tico aG036GICData
    @type  Function
    @author Fernando Radu Muscalu
    @since 17/04/2017
    @version 1
    @param 
    @return aG036GICData, array, array com uma c�pia do array aData que � utilizado
    		nas fun��es GA036Scatter e GA036Data. Estas fun��es s�o respons�veis por
    		alimentar este array com os dados do Bilhete (GIC) que ser� persistido
    		no banco de dados. 
    @example
    (examples)
    @see (links_or_references)
/*/
Function G036GICData()
Return(aG036GICData)

/*/{Protheus.doc} G036GICData
    Fun��o para clonar o array aData (parametro) no array est�tico aG036GICData
    @type  Function
    @author Fernando Radu Muscalu
    @since 17/04/2017
    @version 1
    @param aData, array, array com os dados do bilhete.
    		aData[n,1]: <campo de gic>
    		aData[n,2]: <conte�do que ir� atualizar o campo>	
    @return nil, nulo, sem retorno    		 
    @example
    (examples)
    @see (links_or_references)
/*/
Function G036SetGIC(aData)

Default aData		:= {}

If ( Len(aData) == 0 )

	aG036GICData := {}
	
	aAdd(aG036GICData,{"GIC_COLAB",})
    aAdd(aG036GICData,{"GIC_DTVIAG",})
    aAdd(aG036GICData,{"GIC_LINHA",}) //Este caso aqui est� estranho
    aAdd(aG036GICData,{"GIC_SENTID",})
    aAdd(aG036GICData,{"GIC_HORA",})
    aAdd(aG036GICData,{"GIC_CODSRV",})
    aAdd(aG036GICData,{"GIC_DTVEND",})
    aAdd(aG036GICData,{"GIC_HRVEND",})
    aAdd(aG036GICData,{"GIC_BILHET",})
    aAdd(aG036GICData,{"GIC_LOCORI",})
    aAdd(aG036GICData,{"GIC_LOCDES",})
    aAdd(aG036GICData,{"GIC_TAR",})
    aAdd(aG036GICData,{"GIC_SGFACU",})
    aAdd(aG036GICData,{"GIC_PED",})
    aAdd(aG036GICData,{"GIC_TIPO",})
    
Else
	aG036GICData := aClone(aData)
EndIf	

Return()


/*/{Protheus.doc} Ga036AltVolta
(long_description)
@type function
@author jacomo.fernandes
@since 30/07/2018
@version 1.0
@param aData, array, (Descri��o do par�metro)
@param cLocDes, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function Ga036AltVolta(aData,cLocDes)
Local cAliasGI2	:= GetNextAlias()
Local cCodLin	:= GA036RetData(aData,"GIC_LINHA")
Local cLocOri	:= GA036RetData(aData,"GIC_LOCORI")

	BeginSql alias cAliasGI2
	
		SELECT
		    DISTINCT GI2_COD
		FROM 
			%Table:GI2% GI2
		INNER JOIN 
			%Table:GI4% GI4 
		ON 
			GI4.GI4_FILIAL = GI2.GI2_FILIAL
			AND GI4.GI4_LINHA = GI2.GI2_COD
			AND GI4.GI4_LOCORI = %Exp:cLocOri%
			AND GI4.GI4_LOCDES = %Exp:cLocDes%
			AND GI4.GI4_SENTID = '1'
			AND GI4.GI4_HIST   = '2'
			AND GI4.%NOTDEL%
		INNER JOIN 
			%Table:GID% GID 
		ON
			GID.GID_FILIAL = GI2.GI2_FILIAL
			AND GID.GID_LINHA = GI2.GI2_COD
			AND GID.GID_HIST = '2'
			AND GID.%NOTDEL%
		WHERE
		    GI2.GI2_FILIAL = %xFilial:GI2%
			AND GI2.GI2_NUMLIN IN 
			(
				SELECT
					DISTINCT
					GI2_NUMLIN 
				FROM
					%Table:GI2% 
				WHERE 
					GI2_NUMLIN = 
					(
						SELECT 
							DISTINCT
							GI2_NUMLIN 
						FROM
							%Table:GI2% 
						WHERE
							GI2_FILIAL = %xFilial:GI2% 
							AND GI2_COD = %Exp:cCodLin% 
							AND GI2_HIST = '2'
							AND %NOTDEL%
					)
			)
			AND GI2.GI2_HIST = '2'
			AND GI2.%NOTDEL%
			
	EndSql

	IF (cAliasGI2)->(!Eof()) .and. cCodLin <> (cAliasGI2)->GI2_COD
		nPos   := aScan(aData,{|x| x[1] == "GIC_LINHA"}) //Pesquisa o campo que ser� atualizado
        
        If ( nPos > 0 )
            aData[nPos,2] := (cAliasGI2)->GI2_COD
            aData[nPos,3] := GA036RetLnRead()
        Else
            aAdd(aData, {"GIC_LINHA",(cAliasGI2)->GI2_COD,GA036RetLnRead()})
        EndIf
		
		nPos   := aScan(aData,{|x| x[1] == "GIC_SENTID"}) //Pesquisa o campo que ser� atualizado
        
        If ( nPos > 0 )
            aData[nPos,2] := '1'
            aData[nPos,3] := GA036RetLnRead()
        Else
            aAdd(aData, {"GIC_LINHA",'1',GA036RetLnRead()})
        EndIf
		
	Endif

Return 

/*/{Protheus.doc} G036PersistData
(long_description)
@type function
@author jacomo.fernandes
@since 30/07/2018
@version 1.0
@param cArquivo, character, (Descri��o do par�metro)
@param oView, objeto, (Descri��o do par�metro)
@param aRedZ, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function G036PersistData(cArquivo,oView,aRedZ,cEcfSeq,cEcfSer)

Local oTabela	:= GA036EObjTab()
Local oModelGIC	:= Nil
Local oStruGIC	:= Nil

Local aSeek		:= {}
Local aResult	:= {}
Local aReducao	:= {}
Local aField	:= {}
Local aErro		:= {}
Local aData		:= {}

Local cCOOIni	:= ""
Local cMsgErro	:= ""
Local cLine 	:= ""

Local nI		:= 0
Local nX		:= 0
Local nY		:= 0
Local nZ		:= 0
Local nP		:= 0
Local nGTIni	:= 0
Local nPLnFile	:= 0

Local dDataMvto	:= SToD("")
Local dDataBkp	:= SToD("")

Local lRet		:= .T.

Local cFilOld	:= cFilAnt
Local cFilRes	:= Posicione('GI6',1,xFilial('GI6')+oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),'GI6_FILRES')

G036SetGIC()
aData := G036GICData()

cFilAnt	:= cFilRes

Begin Transaction

	For nI := 1 to Len(aRedZ)
		
		//Fun��o responsavel para ajustar a data do movimento quando um arquivo gera uma redu��o Z entre dois dias
		CheckVldMov(aRedZ[nI],oTabela)
		
		//Prepara os dados da redu��o Z para grava��o
		PrepareDataToCommit(aRedZ[nI,3],@dDataMvto,@cCOOIni,@nGTIni,@aReducao,@aField)
		
		//Fun��o para tratar as aliquotas quando n�o forem preenchidas automaticamente pela redu��o Z
		UpdAliqRZ(aRedZ[nI,3],dDataMvto,aField)
		
		//Salva Database do sistema
		dDataBkp := dDataBase
		
		//Altera data base do sistema
		dDataBase := dDataMvto
		
		//Verifica se j� existe uma redu��o e que se deve deleta-la antes de grava-la
		If VldRedZ(dDataMvto,aReducao,aRedZ,nI)
			If ( !Lj160Grv(dDataMvto,cCOOIni,nGTIni,aReducao,0,0,aField,0) )
			
				//lRet := .f.
		        GA036ErrorLog(oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
		        				cArquivo,;
		        				STR0113,; // "Redu��o Z j� existente."
		        				cEcfSer,;
		        				"SFI",;
		        				"!ALL!",;
	                    		"!FL_REDUZ!",;
		        				-1,;
		        				,;
		        				'2')
            				   
	    	Endif
		Else
			lRet := .F.
		Endif
						        				 
		If lRet
			
			nP := aScan(aRedZ[nI,3],{|x| "FI_COD" $ Alltrim(x[1])})
			
			If ( nP > 0 )
			
				RecLock("SFI",.F.)
			
				For nZ := nP to Len(aRedZ[nI,3])
					
					If ( "FI_COD" $ aRedZ[nI,3][nZ,1] )
					 
						If ( !Empty(aRedZ[nI,3][nZ,2]) )	
							SFI->&(aRedZ[nI,3][nZ,1]) := aRedZ[nI,3][nZ,2]
						EndIf
							
					Else
						Exit
					EndIf
					
				Next nZ
				
				SFI->(MsUnlock())
			
			EndIf
			
			aSeek := {}
			
			aAdd(aSeek,{"GIC_DTVEND",dDataMvto})
			aAdd(aSeek,{"IGNORA",'F'})
			
			If ( GTPSeekTemp(oTabela,aSeek,aResult) )
				
				oModelGIC	:= FwLoadModel("GTPA115")
				oStruGIC	:= oModelGIC:GetModel('GICMASTER'):GetStruct()
				
				oStruGIC:SetProperty("GIC_TIPO"		,	MODEL_FIELD_INIT 		, {|| "E"	} )  //VENDA EMBARCADA
				oStruGIC:SetProperty("GIC_ORIGEM"	,	MODEL_FIELD_INIT 		, {|| "2"	} )  //AUTOMATICO
				oStruGIC:SetProperty("GIC_CODIGO"	,	MODEL_FIELD_INIT 		, {|| GTPXENUM('GIC','GIC_CODIGO',1)	} )  //Numero sequencial
								 
				For nX := 2 to Len(aResult)
					
					oModelGIC:SetOperation(MODEL_OPERATION_INSERT)
            		oModelGIC:Activate()
            		
            		For nY := 1 to Len(aResult[1])
            			
            			If ( oModelGIC:GetModel("GICMASTER"):HasField(aResult[1][nY]) )
            				
            				nP := aScan(aData,{|x| Alltrim(Upper(x[1])) == aResult[1][nY]})
            				
            				If ( Alltrim(aResult[1][nY])+"|" $ "GIC_ECFSEQ|GIC_LOCDES|GIC_LOCORI|" )
            					lRet := oModelGIC:GetModel("GICMASTER"):LoadValue(aResult[1][nY],aResult[nX][nY])
            				ElseIf ( aResult[1][nY] == "GIC_TIPO" )
            					lRet := oModelGIC:GetModel("GICMASTER"):LoadValue("GIC_TIPO","E")
            				Else
            					lRet := oModelGIC:GetModel("GICMASTER"):SetValue(aResult[1][nY],aResult[nX][nY])
            				EndIf
            				
            				nPLnFile := aScan(aResult[1],{|x| Alltrim(x) == "LN_FILE" })
            				
            				If ( nPLnFile > 0 )
            					cLine := aResult[1][nPLnFile] 
            				EndIf
            				
	            			If ( !lRet )
	            			
	            				If ( nP > 0 ) 
	            					GA036SetData(aData,aResult[1][nY],aResult[nX][nY])
	            				EndIf
	            			
	            				Exit
	            				
	            			Else
	            			
	            				If ( nP > 0 ) 
	            					GA036SetData(aData,aResult[1][nY],oModelGIC:GetModel("GICMASTER"):GetValue(aResult[1][nY]))
	            				EndIf
	            			
	            			EndIf
            			
            			EndIf
            			
            		Next nY
            		
            		If ( lRet )
            			lRet := oModelGIC:GetModel("GICMASTER"):LoadValue("GIC_ORIGEM","2")
            		EndIf            		
            		            		
            		If ( lRet .And. oModelGIC:VldData() )
    					lRet := oModelGIC:CommitData()
    				Else
    					lRet := .f.	
    				EndIf
            		
            		If ( !lRet )
            			
            			SetDataDefault(aData)
            			
            			aErro       := oModelGIC:GetErrorMessage()
				        cMsgErro    := Alltrim(aErro[6]) + ". " + Alltrim(aErro[7])
				
				        GA036ErrorLog(oView:GetModel("GI6DETAIL"):GetValue("GI6_CODIGO"),;
				        				cArquivo,;
				        				cMsgErro,;
				        				cECFSer,;
				        				"GIC",;
				        				aErro[4],;
				        				Iif(Valtype(aErro[8]) == "U","",aErro[8]),;
				        				Val(cLine),;
				        				aData)
            			
            			oModelGIC:DeActivate()
            			
            			Exit
            		Else
            			oModelGIC:DeActivate()
            		EndIf	
            		
				Next nX
				If lRet
					UpdTempOk(oTabela,dDataMvto)
				Endif
			EndIf
			
		EndIf
		
	Next nI
	
	If ( !lRet )
		DisarmTransaction()
	EndIf
	
End Transaction

cFilAnt	:= cFilOld

Return(lRet)

/*/{Protheus.doc} PrepareDataToCommit
(long_description)
@type function
@author jacomo.fernandes
@since 30/07/2018
@version 1.0
@param aRedZ, array, (Descri��o do par�metro)
@param dDataMvto, data, (Descri��o do par�metro)
@param cCOOIni, character, (Descri��o do par�metro)
@param nGTIni, num�rico, (Descri��o do par�metro)
@param aReducao, array, (Descri��o do par�metro)
@param aField, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function PrepareDataToCommit(aRedZ,dDataMvto,cCOOIni,nGTIni,aReducao,aField)

Local nP	:= 0

Local lCont	:= .F.

nP := aScan(aRedZ,{|x| x[1] == "FI_DTMOVTO"})

If ( nP > 0 )
	dDataMvto := GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_DTMOVTO","X3_TIPO"))
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_NUMINI"})

If ( nP > 0 )
	cCOOIni := GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_NUMINI","X3_TIPO"))
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_GTINI"})

If ( nP > 0 )
	nGTIni := GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_GTINI","X3_TIPO"))	
EndIf

aField := {}

For nP := 1 to Len(aRedZ)
	
	If ( "FI_BAS" $ aRedZ[nP,1] )
		
		If ( !Empty(aRedZ[nP,2]) )
			aAdd(aField,{aRedZ[nP,1],GtpCastType(aRedZ[nP,2],GetSx3Cache(aRedZ[nP,1],"X3_TIPO"))})
		EndIf	
	EndIf
	
Next nP

aReducao := {}

aAdd(aReducao, dDataMvto)														//[01]-Database?

nP := aScan(aRedZ,{|x| x[1] == "FI_PDV"})

If ( lCont := (nP > 0) )
	aAdd(aReducao, padr(GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_PDV","X3_TIPO")),TamSx3('FI_PDV')[1]))	//[02]-FI_PDV	//Ser� utilizado para compor o n�mero do movimento
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_SERPDV"})

If ( lCont .And. (lCont := (nP > 0)) )
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_SERPDV","X3_TIPO")))	//[03]-FI_SERPDV
EndIf	

nP := aScan(aRedZ,{|x| x[1] == "FI_NUMREDZ"})

If ( lCont .And. (lCont := (nP > 0)) )
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_NUMREDZ","X3_TIPO")))//[04]-FI_NUMREDZ
EndIf	

nP := aScan(aRedZ,{|x| x[1] == "FI_GTFINAL"})

If ( lCont .And. (lCont := (nP > 0)) )
	aAdd(aReducao, aRedZ[nP,2]) 												//[05]-FI_GTFINAL
EndIf	

aAdd(aReducao, "")																//[06]-Sem uso, mas deve ficar em branco	
																														
nP := aScan(aRedZ,{|x| x[1] == "FI_NUMFIM"})

If ( lCont .And. (lCont := (nP > 0)) )
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_NUMFIM","X3_TIPO")))	//[07]-FI_NUMFIM
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_CANCEL"})

If ( lCont .And. (lCont := (nP > 0)) )											//[08]-FI_CANCEL
	aAdd(aReducao, aRedZ[nP,2])													
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_VALCON"})

If ( lCont .And. (lCont := (nP > 0)) )											//[09]-FI_VALCON
	aAdd(aReducao, aRedZ[nP,2])	
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_DESC"})

If ( lCont .And. (lCont := (nP > 0)) )											//[10]-FI_DESC
	aAdd(aReducao, aRedZ[nP,2])		
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_SUBTRIB"})

If ( lCont .And. (lCont := (nP > 0)) )											//[11]-FI_SUBTRIB
	aAdd(aReducao, aRedZ[nP,2])	
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_ISENTO"})

If ( lCont .And. (lCont := (nP > 0)) )											//[12]-FI_ISENTO
	aAdd(aReducao, aRedZ[nP,2])	
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_NTRIB"})

If ( lCont .And. (lCont := (nP > 0)) )											//[13]-FI_NTRIB
	aAdd(aReducao, aRedZ[nP,2])
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

aAdd(aReducao, "")																//[14]-FI_PERCFIS

nP := aScan(aRedZ,{|x| x[1] == "FI_COO"})

If ( lCont .And. (lCont := (nP > 0)) )											//[15]-FI_COO
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_COO","X3_TIPO")))	
Else
	aAdd(aReducao, GtpCastType(,GetSx3Cache("FI_COO","X3_TIPO")))
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_OUTROSR"})

If ( lCont .And. (lCont := (nP > 0)) )											//[16]-FI_OUTROSR
	aAdd(aReducao, aRedZ[nP,2])	
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_ISS"})

If ( lCont .And. (lCont := (nP > 0)) )											//[17]-FI_ISS
	aAdd(aReducao, aRedZ[nP,2])
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_CRO"})

If ( lCont .And. (lCont := (nP > 0)) )
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_CRO","X3_TIPO")))	//[18]-FI_CRO
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_DESISS"})

If ( lCont .And. (lCont := (nP > 0)) )											//[19]-FI_DESISS
	aAdd(aReducao, aRedZ[nP,2])
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_CANISS"})

If ( lCont .And. (lCont := (nP > 0)) )											//[20]-FI_CANISS
	aAdd(aReducao, aRedZ[nP,2])	
Else
	aAdd(aReducao, "")
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_DTREDZ"})

If ( lCont .And. (lCont := (nP > 0)) )											//[21]-FI_DTREDZ
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_DTREDZ","X3_TIPO")))	
Else
	aAdd(aReducao, GtpCastType(,GetSx3Cache("FI_DTREDZ","X3_TIPO")))
	lCont := .t.	
EndIf

nP := aScan(aRedZ,{|x| x[1] == "FI_HRREDZ"})

If ( lCont .And. (lCont := (nP > 0)) )											//[22]-FI_HRREDZ
	aAdd(aReducao, GtpCastType(aRedZ[nP,2],GetSx3Cache("FI_HRREDZ","X3_TIPO")))	
Else
	aAdd(aReducao, GtpCastType(,GetSx3Cache("FI_HRREDZ","X3_TIPO")))
	lCont := .t.	
EndIf

Return()

/*/{Protheus.doc} SetDataDefault
(long_description)
@type function
@author jacomo.fernandes
@since 30/07/2018
@version 1.0
@param aData, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetDataDefault(aData)

Local nP	:= 0 
Local nI	:= aScan(aData,{|x| Valtype(x[2]) == "U" })

If ( nI == 0 )
	nI := 1
EndIf	

For nP := nI to Len(aData)
            				
	If ( Valtype(aData[nP,2]) == "U" )
		aData[nP,2] := GtpCastType(,GetSx3Cache(aData[nP,1],"X3_TIPO"))
	EndIf
	
Next nP

Return() 		

/*/{Protheus.doc} VldEstacaoPdv
Fun��o responsavel para validar se existe a ECF, caso ela exista para outra agencia, realiza a copia dessa maquina para a agencia informada
@type function
@author jacomo.fernandes
@since 24/08/2018
@version 1.0
@param cEcfSer, character, (Descri��o do par�metro)
@param cAgencia, character, (Descri��o do par�metro)
@param cMsgErro, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function VldEstacaoPdv(cEcfSer,cAgencia,cArquivo,aData)
Local lRet		:= .T.
Local aSeek		:= {{"GZ2_CODIGO",cEcfSer }}
Local aResult	:= {{"GZ2_CODIGO","GZ2_AGENCI"}}
Local cMsgErro	:= ""
	
	GTPSeekTable("GZ2",aSeek,aResult)
	
	If ( Len(aResult) <= 1  ) //Se n�o encontrar retorna falso
		lRet := .f.
        cMsgErro := STR0046 + Alltrim(cEcfSer ) + STR0045//" - n�o existe no cadastro de ECF!"//"O n�mero de ECF: "
        
    ElseIf (aScan(aResult,{|x| x[2] = cAgencia}) == 0)//Se encontrou mas n�o pra agencia, realiza a c�pia dessa maquina
    	If !GA036CpyECF(cEcfSer ,aResult[2][2],cAgencia,@cMsgErro)//Fun��o responsavel para cria��o da maquina para a agencia
			lRet := .F.
    	Endif
    EndIf
    
    
    If !lRet
    	GA036ErrorLog(	cAgencia		,;	//Agencia
	                    cArquivo		,;	//Arquivo
	                    cMsgErro		,;	//Mensagem de Erro
	                    cEcfSer			,;	//ECF
	                    "GZ2"			,;	//tabela
	                    "GZ2_CODIGO"	,;	//Campo
	                    cEcfSer			,;	//"Abertura do Arquivo" //Conteudo
	                    GA036RetLnRead(),;	//Linha do Arquivo
	                    aData			)	//aData
    EndIf

	GTPDestroy(aSeek)
	GTPDestroy(aResult)
Return lRet

/*/{Protheus.doc} CheckVldMov
Fun��o responsavel para checagem da data do movimento
@type function
@author jacomo.fernandes
@since 24/08/2018
@version 1.0
@param aRedZ, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function CheckVldMov(aRedZ,oTabela)

Local aData			:= aRedZ[3]
Local nPosVlrBase	:= aScan(aData,{|x| "FI_VALCON" $ Alltrim(x[1])})
Local nPosDtMov		:= aScan(aData,{|x| "FI_DTMOVTO" $ Alltrim(x[1])})
Local nVlrBase		:= 0 
Local cTableName	:= ""
Local cAliasTmp		:= GetNextAlias()
Local dDtMovim		:= CtoD('  /  /  ')
Local nQtdReg		:= 0

If aRedz[4] <> aRedz[5] .and. ValType(oTabela) == "O" //Se a data informada de inicial e final forem diferentes, verifica qual data � a correta
	
	If nPosVlrBase > 0
		nVlrBase := aData[nPosVlrBase][2]
	Endif
	
	cTableName	:= "%"+oTabela:GetRealName()+"%"
	
	BeginSql Alias cAliasTmp
		SELECT 
			GIC_DTVEND,
			SUM(GIC_TAR)	AS VALTAR,
			SUM(GIC_PED)	AS VALPED,
			SUM(GIC_SGFACU)	AS VALSEG
		
		FROM %Exp:cTableName% 
		WHERE  
			GIC_DTVEND IN (%Exp:aRedz[4]%,%Exp:aRedz[5]%)
			AND %NotDel%
		GROUP BY GIC_DTVEND
		ORDER BY GIC_DTVEND
	
	EndSql
	
	nQtdReg	:= (cAliasTmp)->(ScopeCount())
	
	If nQtdReg <> 0
	
		(cAliasTmp)->(DbGoTop())
		If nQtdReg == 1
			If Val(nVlrBase) == (cAliasTmp)->(VALTAR+VALPED+VALSEG) .and. StoD((cAliasTmp)->GIC_DTVEND) == aRedz[4]
				dDtMovim	:= aRedz[4]
			Else
				dDtMovim	:= aRedz[5]
			Endif
		Else
			While !(cAliasTmp)->(Eof())
				If Val(nVlrBase) == (cAliasTmp)->(VALTAR+VALPED+VALSEG)
					dDtMovim := StoD((cAliasTmp)->GIC_DTVEND)
					Exit
				Endif
				(cAliasTmp)->(DbSkip())
			End
		Endif
			
		If nPosDtMov > 0 .and. !Empty(dDtMovim)
			aData[nPosDtMov][2] := DtoC(dDtMovim)
		Endif
		
	Endif
	(cAliasTmp)->(DbCloseArea())
		
Endif
Return

/*/{Protheus.doc} VldRedZ
Fun��o responsavel para valida��o da Redu��o Z
@type function
@author jacomo.fernandes
@since 28/08/2018
@version 1.0
@param dDataMvto, data, (Descri��o do par�metro)
@param aReducao, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function VldRedZ(dDataMvto,aReducao,aRedZ,nI)
Local lRet		:= .T.
Local lDeleta	:= .T.
Local cPdv		:= aReducao[2]
Local cCRZ		:= Padr(aReducao[4],TamSx3('FI_NUMREDZ')[1])

Local cAliasTmp	:= GetNextAlias()
Local oTable	:= GA036EObjTab()
Local cTableName:= ''

Local nValTot	:= 0
Local nQtd		:= 0

aEval(aRedz,{|x| If(x[2] == dDataMvto, nValTot += Val(x[3,25,2]),) , If(x[2] == dDataMvto,nQtd++, )})

DbSelectArea("SFI")

SFI->( DbSetOrder(1) )  //FI_FILIAL, FI_DTMOVTO, FI_PDV, FI_NUMREDZ
//Ignora o numero do CRZ no momento da busca devido as maquinas gerarem numera��o diferente
//Porem verificamos se a numera��o for igual para n�o reprocessarmos o mesmo arquivo
//Se Encontrar, deleta e realizar� uma nova importa��o
If SFI->( DbSeek(xFilial("SFI") + DTOS(dDataMvto) + cPdv /*+  cCRZ*/ ) ) .and. SFI->FI_NUMREDZ <> cCRZ
	
	If nQtd > 1 .and. ValType(oTable) == "O"
		cTableName	:= "%"+oTable:GetRealName()+"%"
		
		BeginSql Alias cAliasTmp
			Select
				CAST(SUM(GIC_TAR)	 AS NUMERIC(12,2))	AS VALTAR,
				CAST(SUM(GIC_PED)	 AS NUMERIC(12,2))	AS VALPED,
				CAST(SUM(GIC_SGFACU) AS NUMERIC(12,2))	AS VALSEG
			From %Exp:cTableName%
			Where
				GIC_DTVEND = %Exp:dDataMvto%
			Group by GIC_DTVEND
		EndSql
		lDeleta := ( nValTot <> (cAliasTmp)->(VALTAR + VALPED + VALSEG ))
	Endif
	
	If lDeleta
		SFI->(RecLock("SFI",.F.,.T.))
		SFI->(dbDelete())
		SFI->(MsUnlock())
	Endif
Endif

Return lRet	


/*/{Protheus.doc} UpdAliqRZ

Fun��o responsavel para quando n�o tiver a base de calculo definida no arquivo, preencher automaticamente os campos

@type function
@author jacomo.fernandes
@since 29/08/2018
@version 1.0
@param aData, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function UpdAliqRZ(aData,dDtMovto,aField)
Local oTable		:= GA036EObjTab()
Local cAliasTmp		:= GetNextAlias()
Local cTableName	:= ""
Local cAux			:= ""
Local cFldBas		:= ""
Local cFldCod		:= ""
Local nP			:= 0
If ValType(oTable) == "O"
	
	If ValType(aField) == "A" .and. Len(aField) == 0
		cTableName	:= "%"+oTable:GetRealName()+"%"
		
		BeginSql Alias cAliasTmp
			Select
				GIC_ALICMS,
				CAST(SUM(GIC_TAR)	 AS NUMERIC(12,2))	AS VALTAR,
				CAST(SUM(GIC_PED)	 AS NUMERIC(12,2))	AS VALPED,
				CAST(SUM(GIC_SGFACU) AS NUMERIC(12,2))	AS VALSEG
			From %Exp:cTableName%
			Where
				GIC_DTVEND = %Exp:dDtMovto%
			Group by GIC_ALICMS
			ORDER BY GIC_ALICMS
		EndSql
		
		While !(cAliasTmp)->(EOF())
			cAux 	:= GA036AliqField((cAliasTmp)->GIC_ALICMS)
			cFldBas	:= "FI_BAS" + cAux
			cFldCod	:= "FI_COD" + cAux
			nValBase:= (cAliasTmp)->(VALTAR+VALPED+VALSEG)
			
			aAdd(aField,{cFldBas, GtpCastType(nValBase,GetSx3Cache(cFldBas,"X3_TIPO")) } )
			//aAdd(aData[23],SubsTr(cFldBas,7))
			
			//Valor base do ICMS de acordo com a al�quota retornada pela leitura do arquivo	 			
			If(nP := aScan(aData,{|x| x[1] == cFldBas})) > 0
				aData[nP][2]:= nValBase
			Endif
			
			//Conte�do do campo que guarda o c�digo da al�quota.	 			
			If(nP := aScan(aData,{|x| x[1] == cFldCod})) > 0
				aData[nP][2]:= "T"+If( Len(cAux)==1 ,'0'+cAux,cAux)+'00'
			Endif
			
			(cAliasTmp)->(DbSkip())
		End
		(cAliasTmp)->(DbCloseArea())
		
	Endif
	 
Endif

Return

/*/{Protheus.doc} UpdTempOk
Fun��o responsavel para atualizar a tabela temporaria marcando como Ignorar = 'T' os bilhetes que j� foram integrados
@type function
@author jacomo.fernandes
@since 30/08/2018
@version 1.0
@param oTabela, objeto, (Descri��o do par�metro)
@param dDataMvto, data, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function UpdTempOk(oTabela,dDataMvto)
Local cQuery	:= ""
Local cTmpName	:= oTabela:GetRealName()

cQuery	:= "Update "+ cTmpName + Chr(13)+Chr(10)
cQuery	+= "Set IGNORA = 'T'" + Chr(13)+Chr(10)
cQuery	+= "Where GIC_DTVEND = '"+DtoS(dDataMvto)+"'"

TcSqlExec(cQuery)
TcSqlExec('COMMIT')
Return
