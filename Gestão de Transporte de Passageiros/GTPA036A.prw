#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'
#INCLUDE 'GTPA036A.CH'

Static oG036Table  := nil   //objeto inst�ncia da Classe FWTemporaryTable

/*/{Protheus.doc} ModelDef
    Fun��o que define o modelo de dados para a listagem de ag�ncias utilizada no programa
	GTPA036 - Inporta��o/Exporta��o DARUMA
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oModel, objeto, inst�ncia da classe FwFormModel
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ModelDef()

Local oStrCab	:= FWFormModelStruct():New()
Local oStrGI6	:= FWFormStruct( 1, "GI6",,.F. )

Local aRelation := {}

//Cria tabela tempor�ria para o cabe�alho fake utilizado no Modelo
GA036Table()

//Define as estruturas dos submodelos no que diz respeito ao Modelo de Dados
GA036Struct(oStrCab,oStrGI6,"M")

oModel := MPFormModel():New("GTPA036A")
oModel:AddFields("MASTER", /*cOwner*/, oStrCab,{|oSubMdl,cAct,cFld,xVl| GA036MdlVld(oSubMdl,cAct,cFld,xVl) })

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
oModel:AddGrid("GI6DETAIL", "MASTER", oStrGI6,,,,,{|oMdl| GA036Load(oMdl)})

aRelation := { { "GI6_CODIGO", "XXX_CODGI6"} }

oModel:SetRelation("GI6DETAIL",aRelation,GI6->(IndexKey(1)))

oModel:GetModel("GI6DETAIL"):SetOptional(.T.)
oModel:GetModel("GI6DETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GI6DETAIL"):SetNoInsertLine(.T.)

//Define as descri��es dos submodelos e modelo
oModel:SetDescription(STR0001) // "Assistente de Importa��o/Exporta��o DARUMA"
oModel:GetModel("MASTER"):SetDescription(STR0002) // "Assistente"
oModel:GetModel("GI6DETAIL"):SetDescription(STR0003) // "Lista de Ag�ncias"

//Defini��o da Chave �nica
oModel:SetPrimaryKey({})

oModel:SetDeActivate( {|| GA036Destroy() } )

Return(oModel)

/*/{Protheus.doc} ViewDef
    Fun��o que define a View para para a listagem de ag�ncias utilizada no programa
	GTPA036 - Inporta��o/Exporta��o DARUMA
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oView, objeto, inst�ncia da Classe FWFormView
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ViewDef()

Local oView		:= nil
Local oModel	:= FwLoadModel("GTPA036A")
Local oStrCab   := FWFormViewStruct():New()
Local oStrGI6	:= FWFormStruct( 2, "GI6",,.F. )

//Define as estruturas dos submodelos no que diz respeito � View
GA036Struct(oStrCab,oStrGI6,"V",oModel:GetModel("GI6DETAIL"):GetStruct())

// Cria o objeto de View
oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )

oView:AddField("VIEW_CAB", oStrCab, "MASTER" )
oView:AddGrid("VIEW_GI6", oStrGI6, 'GI6DETAIL')

// Divis�o Horizontal
oView:CreateHorizontalBox("HEADER",25)
oView:CreateHorizontalBox("GRID",75)

oView:SetOwnerView("VIEW_CAB", "HEADER")
oView:SetOwnerView("VIEW_GI6", "GRID")

//Habitila os t�tulos dos modelos para serem apresentados na tela
oView:EnableTitleView("VIEW_GI6")

Return(oView)


/*/{Protheus.doc} GA036Struct
    Fun��o respons�vel por criar a estrutura dos submodelos, tanto para o model quanto view,
	utilizados pelo MVC. 
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 	oStrCab, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (CABE�ALHO)
			oStrGI6, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (ITENS)
			cTipo, caractere, Tipo de Estrutura que ser� criada - "M" - model; "V" - View
			oStrMdlGrd, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (ITENS).
				Usado somente na montagem da view
    @return nil, nulo, Sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Struct(oStrCab,oStrGI6,cTipo,oStrMdlGrd)

Local aFldsGI6  := {}

Local nI        := 0

Local cAliasTab := GA036AliasTable()

If ( cTipo == "M" ) //Estrutura do Modelo de dados
    
    oStrCab:AddTable(oG036Table:GetAlias(), , STR0004, {|| oG036Table:GetRealName() }) // "Estrutura Fake do Cabe�alho"
    
    //Estrutura do Cabe�alho
    oStrCab:AddField(	STR0005,;								// 	[01]  C   Titulo do campo // "C�digo GI6"
				 		STR0005,;							// 	[02]  C   ToolTip do campo // "C�digo GI6"
				 		"XXX_CODGI6",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		6,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		
	
    oStrCab:AddField(	STR0006,;									// 	[01]  C   Titulo do campo // "Data de"
				 		STR0006,;									// 	[02]  C   ToolTip do campo // "Data de"
				 		"XXX_DATADE",;							// 	[03]  C   Id do Field
				 		"D",;									// 	[04]  C   Tipo do campo
				 		8,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		

    oStrCab:AddField(	STR0007,;									// 	[01]  C   Titulo do campo // "Data At�"
				 		STR0007,;									// 	[02]  C   ToolTip do campo // "Data At�"
				 		"XXX_DATATE",;							// 	[03]  C   Id do Field
				 		"D",;									// 	[04]  C   Tipo do campo
				 		8,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
	
	
	oStrCab:AddTrigger("XXX_DATADE"		,"XXX_DATATE"			,{||.T.},{|oMdl,cField,xVal| xVal})
	
    //Estrutura do Grid
	oStrGI6:AddField(	"",;									// 	[01]  C   Titulo do campo
				 		"",;									// 	[02]  C   ToolTip do campo
				 		"GI6_MARK",;							// 	[03]  C   Id do Field
				 		"L",;									// 	[04]  C   Tipo do campo
				 		1,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual	

    oStrGI6:AddField(	STR0008,;									// 	[01]  C   Titulo do campo // "Arq. P�s-Venda"
				 		STR0009,;									// 	[02]  C   ToolTip do campo // "Diret�rio dos arquivos P�s-Vendas"
				 		"POS_VENDA",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQPVE")[1],;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual	                         
    
    oStrGI6:AddField(	STR0010,;								// 	[01]  C   Titulo do campo // "Arq. Sem Formato"
				 		STR0011,;								// 	[02]  C   ToolTip do campo // "Diret�rio dos arquivos sem formata��es"
				 		"NO_FORMAT",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQNFO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
    
    oStrGI6:AddField(	STR0012,;								// 	[01]  C   Titulo do campo // "Arq. Flash"
				 		STR0013,;								// 	[02]  C   ToolTip do campo // "Diret�rio dos arquivos Flash"
				 		"ARQ_FLASH",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQFLA")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual    


	oStrGI6:SetProperty("*",MODEL_FIELD_OBRIGAT,.F.)
	oStrGI6:SetProperty("*",MODEL_FIELD_VALID,{|| .T.})

Else    //Estrutura da View

    //Composi��o da Estrutura do Cabe�alho
    oStrCab:AddField(	"XXX_DATADE",;				// [01]  C   Nome do Campo
						"01",;						// [02]  C   Ordem
						STR0006,;					// [03]  C   Titulo do campo // "Data de"
						STR0006,;					// [04]  C   Descricao do campo // "Data de"
						{STR0006},;	    		    // [05]  A   Array com Help // "Data de"
						"GET",;				    	// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    oStrCab:AddField(	"XXX_DATATE",;				// [01]  C   Nome do Campo
						"02",;						// [02]  C   Ordem
						STR0007,;					// [03]  C   Titulo do campo // "Data At�"
						STR0007,;					// [04]  C   Descricao do campo // "Data At�"
						{STR0007},;	    		// [05]  A   Array com Help // "Data At�"
						"GET",;				    	// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    // Composi��o da Estrutura do Grid
	oStrGI6:AddField(	"GI6_MARK",;				// [01]  C   Nome do Campo
						"01",;						// [02]  C   Ordem
						"",;						// [03]  C   Titulo do campo
						"",;						// [04]  C   Descricao do campo
						{STR0014},;					// [05]  A   Array com Help // "Selecionar"
						"CHECK",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    oStrGI6:AddField(	"POS_VENDA",;				// [01]  C   Nome do Campo
						"04",;						// [02]  C   Ordem
						STR0008,;					// [03]  C   Titulo do campo // "Arq. P�s-Venda"
						STR0009,;					// [04]  C   Descricao do campo // "Diret�rio dos arquivos P�s-Vendas"
						{STR0009},;			// [05]  A   Array com Help // "Diret�rio dos arquivos P�s-Vendas"
						"GET",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
    
    oStrGI6:AddField(	"NO_FORMAT",;				// [01]  C   Nome do Campo
						"05",;						// [02]  C   Ordem
						STR0010,;				    // [03]  C   Titulo do campo // "Arq. Sem Formato"
						STR0011,;				    // [04]  C   Descricao do campo // "Diret�rio dos arquivos sem formata��es"
						{STR0011},;			        // [05]  A   Array com Help // "Diret�rio dos arquivos sem formata��es"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
                        
    oStrGI6:AddField(	"ARQ_FLASH",;				// [01]  C   Nome do Campo
						"06",;						// [02]  C   Ordem
						STR0012,;			    	// [03]  C   Titulo do campo // "Arq. Flash"
						STR0013,;				    // [04]  C   Descricao do campo // "Diret�rio dos arquivos Flash"
						{STR0013},;			        // [05]  A   Array com Help // "Diret�rio dos arquivos Flash"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    If ( ValType(oStrMdlGrd) == "O" )                    
    
        aFldsGI6 := oStrMdlGrd:GetFields()                    
        
        For nI := 1 to Len(aFldsGI6)
            
            If ( (cAliasTab)->(FieldPos(aFldsGI6[nI,3])) == 0 )
                oStrGI6:RemoveField(aFldsGI6[nI,3])
            Else

                If ( aFldsGI6[nI,3] == "GI6_DESCRI" )
                    oStrGI6:SetProperty(aFldsGI6[nI,3],MVC_VIEW_CANCHANGE,.f.)
                EndIf

            EndIf

        Next nI

    EndIf

EndIf

Return()

/*/{Protheus.doc} GA036Table
    Montagem da tabela tempor�ria (utilizada no cabe�alho da tela)
    @type  Static Function
    @author user
    @since 27/03/2017
    @version version
    @param 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Table()

Local aStruct	:= {}
	
AAdd(aStruct,{"XXX_FAKE","C",1,0})

oG036Table := FWTemporaryTable():New()

oG036Table:SetFields(aStruct)
oG036Table:AddIndex("index1", {"XXX_FAKE"})
oG036Table:Create()

Return()

/*/{Protheus.doc} GA036Destroy
    Fun��o que destroy (finaliza) a inst�ncia da Classe FwTemporaryTable - que fora
	utilizada para a montagem da tabela tempor�ria.
    @type  Static Function
    @author user
    @since 27/03/2017
    @version version
    @param 
    @return  nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036Destroy()

If ( !FwIsInCallStack('FwAlertExitPage') )
	
	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ( ValType(oG036Table) == "O" )
		oG036Table:Delete()
		FreeObj(oG036Table)
	EndIf

EndIf

Return()

/*/{Protheus.doc} GA036Load
    Fun��o respons�vel pela carga do grid da tela. Ela lista as ag�ncias. 
    @type  Static Function
    @author user
    @since 27/03/2017
    @version version
    @param oGrid, objeto, inst�ncia da Classe FwFormGridModel
    @return aReg, array, array com os dados que ser�o apresentados no Grid da interface.
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036Load(oGrid)

Local aReg  := {}
Local aFlds := {}
Local aAux  := {}

Local cAliasTab := GA036AliasTable()

Local nI    := 0

aFlds := oGrid:GetStruct():GetFields()

While ( (cAliasTab)->(!Eof()) )
			
    For nI := 1 to Len(aFlds)

        If ( (cAliasTab)->(FieldPos(aFlds[nI,3])) > 0 )
            aAdd(aAux,(cAliasTab)->&(aFlds[nI,3]))
        Else
            aAdd(aAux,GTPCastType(,aFlds[nI,4]))
        EndIf

    Next nI
    
    aAdd(aReg,{(cAliasTab)->GI6_RECNO,aClone(aAux)})
    aAux := {}
    
    (cAliasTab)->(DbSkip())
    
EndDo

Return(aReg)

/*/{Protheus.doc} GA036MdlVld
    Fun��o para a valida��o de campos.
    @type  Static Function
    @author user
    @since 27/03/2017
    @version version
    @param 	oSubModel, objeto, inst�ncia da classe FwFormFieldsModel
			cAction, caractere, tipo de opera��o que est� sendo validada (ex: "SETVALUE")
			cField, caractere, campo que est� sendo validado
			xValue, qualquer, valor que � validado.
    @return lRet, l�gico, .t. valida��o feita com sucesso.
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036MdlVld(oSubModel,cAction,cField,xValue)

Local cTitle    := ""
Local cMsgProb  := ""
Local cMsgSolu  := ""

Local lRet      := .t.

If ( oSubModel:GetModel():GetOperation() == MODEL_OPERATION_UPDATE )

    If ( cAction == "SETVALUE" )

        If ( cField == "XXX_DATADE" )

            If ( !Empty(oSubModel:GetValue("XXX_DATATE")) .And. xValue > oSubModel:GetValue("XXX_DATATE") )
                
                lRet := .f.

                cTitle      := STR0015 // "Per�odo incorreto"
                cMsgProb    := STR0016 // "O preenchimento do Par�metro 'Data de' n�o pode ser superior ao conte�do de 'Data At�' "
                cMsgSolu    := STR0017 + DToC(oSubModel:GetValue("XXX_DATATE")) // "Entre com uma data que seja inferior � "

            EndIf

        ElseIf ( cField == "XXX_DATATE" )

            If ( !Empty(oSubModel:GetValue("XXX_DATADE")) .And. xValue < oSubModel:GetValue("XXX_DATADE") )
                
                lRet := .f.
                
                cTitle      := STR0015 // "Per�odo incorreto"
                cMsgProb    := STR0018 // "O preenchimento do Par�metro 'Data At�' n�o pode ser inferior ao conte�do de 'Data De' "
                cMsgSolu    := STR0019 + DToC(oSubModel:GetValue("XXX_DATADE")) // "Entre com uma data que seja superior � " 

            EndIf

        EndIf

    EndIf

EndIf

If ( !lRet )
    oSubModel:GetModel():SetErrorMessage(oSubModel:GetModel():GetId(),,oSubModel:GetModel():GetId(),,cTitle,cMsgProb,cMsgSolu)
EndIf

Return(lRet)

/*/{Protheus.doc} GA036ObjectTable
    Fun��o para retornar a vari�vel est�tica do fonte: oG036Table
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oG036Table, objeto, inst�ncia de FwTemporaryTable
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036ObjectTable()
Return(oG036Table)
