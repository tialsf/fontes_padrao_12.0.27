#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'
#INCLUDE 'GTPA036B.CH'

/*/{Protheus.doc} ModelDef
    Fun��o que define o modelo de dados para atualiza��o da tabela de cadastro de ECF (GZ2)
	@type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oModel, objeto, inst�ncia da classe FwFormModel
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ModelDef()

Local oStrGZ2	:= FWFormStruct( 1, "GZ2",,.F. )

oModel := MPFormModel():New("GTPA036B")
oModel:AddFields("GZ2MASTER", /*cOwner*/, oStrGZ2)

//Define as descri��es dos submodelos e modelo
oModel:SetDescription(STR0001) // "Cadastro de ECF"
oModel:GetModel("GZ2MASTER"):SetDescription(STR0002) // "Dados"

Return(oModel)


