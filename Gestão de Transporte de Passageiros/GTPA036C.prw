#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'
#INCLUDE 'GTPA036C.CH'

//Declara��o das vari�veis est�ticas
Static oG036CCab  	:= nil  //objeto inst�ncia da Classe FWTemporaryTable
Static oG036CGrd  	:= nil  //objeto inst�ncia da Classe FWTemporaryTable
Static oG036CAge	:= nil	//objeto inst�ncia da Classe FWTemporaryTable
Static oG036CFake	:= nil	//objeto inst�ncia da Classe FWTemporaryTable

Static aG036CLog	:= {}	//Array com os dados do Log de erros

Static nG036ReadLn	:= 0	//Linha atual lida do arquivo de importa��o da DARUMA (TIPOS: POS-VENDA E NAO FORMATADO)

Static cG036CodLog	:= ""	//C�digo �nico para o Log

/*/{Protheus.doc} ModelDef
    Fun��o que define o modelo de dados para o Log De Erros. Tais erros ocorrem no momento
	da valida��o dos dados importados ou no momento da persist�ncia dos dados. 
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oModel, objeto, inst�ncia da classe FwFormModel
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ModelDef()

Local oModel	:= Nil

oModel := GA036CSetModel()

Return(oModel)

/*/{Protheus.doc} ViewDef
    Fun��o que define a View (MVC) para para a tela de Log de Erros
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return oView, objeto, inst�ncia da Classe FWFormView
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ViewDef()

Local oView		:= nil

//Define a View
oView := GA036CSetView()
oView:AddUserButton("Imprimir Log", 'PRINT', {|oView| G036CPrintViag(oView) } )

Return(oView)

//------------------------------------------------------------------------------
/*/{Protheus.doc} GA409PrintViag()
                                   
Executa a impress�o do Grid de Viagens.

@example 	GA409PrintViag(oView)

@param		oView	Objeto. Inst�ncia da Classe FwFormView

@return     Nil

@author     Fernando Radu Muscalu
@since      02/02/2018
@version    12                
/*/
//------------------------------------------------------------------------------
Static Function G036CPrintViag(oView)

Local nLine := oView:GetModel("MAIN"):GetLine()

oView:GetViewObj("MAIN")[3]:oBrowse:Report()
oView:GetModel("MAIN"):GoLine(nLine)
oView:Refresh("VIEW_MAIN")

Return()

/*/{Protheus.doc} GA036CStruct
    Fun��o respons�vel por criar a estrutura dos submodelos, tanto para o model quanto view,
	utilizados pelo MVC. 
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 	oStrMain, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (FAKE)
			oStrAge, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (MAIN)
			oStrCab, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (MASTER)
			oStrGrd, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (DETAIL)
			cTipo, caractere, Tipo de Estrutura que ser� criada - "M" - model; "V" - View
			lOneGrid, l�gico, define se ser� utilizado somente uma grid para a listagem dos erros (.t.).
				Caso seu conte�do seja falso, ser� montada uma tela com tr�s grids 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function GA036CStruct(oStrMain,oStrAge,oStrCab,oStrGrd,cTipo,lOneGrid)

Local aAreaSX3	:= SX3->(GetArea())
Local aFldGIC 	:= G036GICData()
Local aTamSx3   := {}
Local nI        := 0
Local cNotSelect:= "GIC_ECFSER"

Default lOneGrid := .t.

SX3->(DbSetOrder(2))

If ( cTipo == "M" ) //Estrutura do Modelo de dados

	//Define a tabela tempor�ria para a estrutura do Model - Main (principal): Fake
	oStrMain:AddTable(oG036CFake:GetAlias(), , STR0018, {|| oG036CAge:GetRealName() }) // "FAKE"

	//Estrutura do Field Fake
	oStrMain:AddField(	STR0001,;								// 	[01]  C   Titulo do campo // "C�digo do Log"
						STR0001,;								// 	[02]  C   ToolTip do campo // "C�digo do Log"
						"FAKE_COD",;							// 	[03]  C   Id do Field
						"C",;									// 	[04]  C   Tipo do campo
						8,;										// 	[05]  N   Tamanho do campo
						0,;										// 	[06]  N   Decimal do campo
						Nil,;									// 	[07]  B   Code-block de valida��o do campo
						Nil,;									// 	[08]  B   Code-block de valida��o When do campo
						Nil,;									//	[09]  A   Lista de valores permitido do campo
						.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
						Nil,;									//	[11]  B   Code-block de inicializacao do campo
						.F.,;									//	[12]  L   Indica se trata-se de um campo chave
						.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
						.T.)									// 	[14]  L   Indica se o campo � virtual

	If ( !lOneGrid )

		//Define a tabela Tempor�ria para a Estrutura do Model - Lista de Ag�ncia
		oStrAge:AddTable(oG036CAge:GetAlias(), , STR0019, {|| oG036CAge:GetRealName() }) // "Ag�ncias"
		//Define a tabela Tempor�ria para a Estrutura do Model - Lista de Arquivos
		oStrCab:AddTable(oG036CCab:GetAlias() , , STR0020, {|| oG036CCab:GetRealName() }) // "Arquivos x Ag�ncias"
		//Define a tabela Tempor�ria para a Estrutura do Model - Lista de Erros
		oStrGrd:AddTable(oG036CGrd:GetAlias(), , STR0021, {|| oG036CGrd:GetRealName() }) // "Erros de Importa��o"

		oStrAge:AddField(	STR0002,;									// 	[01]  C   Titulo do campo // "Ag�ncia"
							STR0002,;									// 	[02]  C   ToolTip do campo // "Ag�ncia"
							"ZZW_AGENCI",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_CODIGO")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual			 		

		oStrAge:AddField(	STR0003,;								// 	[01]  C   Titulo do campo // "Nome da Ag�ncia"
							STR0003,;								// 	[02]  C   ToolTip do campo // "Nome da Ag�ncia"
							"ZZW_NMAGEN",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_DESCRI")[1],;				// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		//Estrutura do Cabe�alho
		oStrCab:AddField(	STR0004,;								// 	[01]  C   Titulo do campo // "C�digo do Log"
							STR0004,;								// 	[02]  C   ToolTip do campo  // "C�digo do Log"
							"ZZZ_CODLOG",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrCab:AddField(	STR0002,;								// 	[01]  C   Titulo do campo // "Ag�ncia"
							STR0002,;							    // 	[02]  C   ToolTip do campo // "Ag�ncia"
							"ZZZ_AGENCI",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_CODIGO")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrCab:AddField(	STR0005,;									// 	[01]  C   Titulo do campo // "ECF/Controle"
							STR0006,;									// 	[02]  C   ToolTip do campo // "N�mero de ECF ou C�digo de Controle"
							"ZZZ_ECF",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GIC_ECFSER")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 			 		

		oStrCab:AddField(	STR0007,;									// 	[01]  C   Titulo do campo // "Arquivo"
							STR0007,;									// 	[02]  C   ToolTip do campo // "Arquivo"
							"ZZZ_ARQUIV",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							260,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual
							
		oStrCab:AddField(	STR0008,;									// 	[01]  C   Titulo do campo // "Data do Log"
							STR0008,;									// 	[02]  C   ToolTip do campo // "Data do Log"
							"ZZZ_DATA",;							// 	[03]  C   Id do Field
							"D",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 

		oStrCab:AddField(	STR0009,;									// 	[01]  C   Titulo do campo // "Hora do Log"
							STR0009,;									// 	[02]  C   ToolTip do campo // "Hora do Log"
							"ZZZ_HORA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 						 
		
		//Estrutura do Grid
		oStrGrd:AddField(	STR0001,;									// 	[01]  C   Titulo do campo // "C�digo do Log"
							STR0001,;									// 	[02]  C   ToolTip do campo // "C�digo do Log"
							"ZZY_CODLOG",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrGrd:AddField(	STR0002,;									// 	[01]  C   Titulo do campo // "Ag�ncia"
							STR0002,;									// 	[02]  C   ToolTip do campo // "Ag�ncia"
							"ZZY_AGENCI",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_CODIGO")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrGrd:AddField(	STR0005,;									// 	[01]  C   Titulo do campo // "ECF/Controle"
							STR0006,;									// 	[02]  C   ToolTip do campo // "N�mero de ECF ou C�digo de Controle"
							"ZZY_ECF",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GIC_ECFSER")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrGrd:AddField(	STR0010,;									// 	[01]  C   Titulo do campo // "Linha do Arquivo"
							STR0010,;									// 	[02]  C   ToolTip do campo // "Linha do Arquivo"
							"ZZY_LINHA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							6,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual	

		oStrGrd:AddField(	STR0011,;							    // 	[01]  C   Titulo do campo // "Tabela"
							STR0012,;				// 	[02]  C   ToolTip do campo // "Tabela a ser Atualizada"
							"ZZY_TABELA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							3,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual							 

		oStrGrd:AddField(	STR0013,;									// 	[01]  C   Titulo do campo // "Campo"
							STR0014,;									// 	[02]  C   ToolTip do campo // "Campo a ser Atualizado"
							"ZZY_CAMPO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							10,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual							 

		oStrGrd:AddField(	STR0015,;									// 	[01]  C   Titulo do campo // "Valor do Campo"
							STR0015,;									// 	[02]  C   ToolTip do campo // "Valor do Campo"
							"ZZY_VLRCPO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							10,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 

		oStrGrd:AddField(	STR0016,;									// 	[01]  C   Titulo do campo // "Motivo"
							STR0017,;									// 	[02]  C   ToolTip do campo // "Motivo de Log"
							"ZZY_MOTIVO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							300,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

	Else
	
		//Define a tabela Tempor�ria para a Estrutura do Model - Lista de Ag�ncia
		oStrAge:AddTable("ONEGRID",{} , STR0022) //"Log de Erros"

		//Estrutura da Grid de Ag�ncias
		oStrAge:AddField(	STR0001,;									// 	[01]  C   Titulo do campo // "C�digo do Log"
							STR0001,;									// 	[02]  C   ToolTip do campo // "C�digo do Log"
							"CODLOG",;								// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual			 		

		oStrAge:AddField(	STR0002,;								// 	[01]  C   Titulo do campo // "Ag�ncia"
						    STR0002,;								// 	[02]  C   ToolTip do campo // "Ag�ncia"
							"AGENCI",;								// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual			 		

		oStrAge:AddField(	STR0003,;							// 	[01]  C   Titulo do campo "Nome da Ag�ncia"
							STR0003,;						// 	[02]  C   ToolTip do campo "Nome da Ag�ncia"
							"NMAGEN",;								// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GI6_DESCRI")[1],;				// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual

		oStrAge:AddField(	STR0005,;									// 	[01]  C   Titulo do campo // "ECF/Controle"
							STR0006,;									// 	[02]  C   ToolTip do campo // "N�mero de ECF ou C�digo de Controle"
							"ECF",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							TamSx3("GIC_ECFSER")[1],;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 			 		

		oStrAge:AddField(	STR0007,;								// 	[01]  C   Titulo do campo // "Arquivo"
							STR0007,;								// 	[02]  C   ToolTip do campo // "Arquivo"
							"ARQUIV",;								// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							260,;									// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual
							
		oStrAge:AddField(	STR0008,;									// 	[01]  C   Titulo do campo // "Data do Log"
							STR0008,;									// 	[02]  C   ToolTip do campo // "Data do Log"
							"DATA",;							// 	[03]  C   Id do Field
							"D",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 

		oStrAge:AddField(	STR0009,;									// 	[01]  C   Titulo do campo // "Hora do Log"
							STR0009,;									// 	[02]  C   ToolTip do campo // "Hora do Log"
							"HORA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							8,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 						 
		
		oStrAge:AddField(   STR0010,;									// 	[01]  C   Titulo do campo // "Linha do Arquivo"
							STR0010,;									// 	[02]  C   ToolTip do campo // "Linha do Arquivo"
							"LINHA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							6,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual	

		oStrAge:AddField(	STR0011,;									// 	[01]  C   Titulo do campo // "Tabela"
							STR0011,;									// 	[02]  C   ToolTip do campo // "Tabela a ser Atualizada"
							"TABELA",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							3,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual							 

		oStrAge:AddField(	STR0013,;									// 	[01]  C   Titulo do campo // "Campo"
							STR0014,;									// 	[02]  C   ToolTip do campo // "Campo a ser Atualizado"
							"CAMPO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							10,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual							 

		oStrAge:AddField(	STR0015,;									// 	[01]  C   Titulo do campo // "Valor do Campo"
							STR0015,;									// 	[02]  C   ToolTip do campo // "Valor do Campo"
							"VLRCPO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							10,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual						 

		oStrAge:AddField(	STR0016,;									// 	[01]  C   Titulo do campo // "Motivo"
							STR0017,;									// 	[02]  C   ToolTip do campo // "Motivo de Log"
							"MOTIVO",;							// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							300,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							Nil,;									//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual
		
		For nI := 1 to Len(aFldGIC)
			
			If ( SX3->(DbSeek(aFldGIC[nI,1])) )
				aTamSx3 := TamSx3(aFldGIC[nI,1])
				oStrAge:AddField(	FWX3Titulo(aFldGIC[nI,1]),;			// 	[01]  C   Titulo do campo // "Motivo"
									FWSX3Util():GetDescription(aFldGIC[nI,1]),;							// 	[02]  C   ToolTip do campo // "Motivo de Log"
									Alltrim(aFldGIC[nI,1]),;							// 	[03]  C   Id do Field
									aTamSx3[3],;									// 	[04]  C   Tipo do campo
									aTamSx3[1],;										// 	[05]  N   Tamanho do campo
									aTamSx3[2],;										// 	[06]  N   Decimal do campo
									Nil,;									// 	[07]  B   Code-block de valida��o do campo
									Nil,;									// 	[08]  B   Code-block de valida��o When do campo
									GTPXCBox(aFldGIC[nI,1]),;									//	[09]  A   Lista de valores permitido do campo
									.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
									Nil,;									//	[11]  B   Code-block de inicializacao do campo
									.F.,;									//	[12]  L   Indica se trata-se de um campo chave
									.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
									.T.)									// 	[14]  L   Indica se o campo � virtual
			EndIf
			
		Next nI
		oStrAge:AddField(	STR0028,;									// 	[01]  C   Titulo do campo // "Motivo" //"Tp Msg"
							STR0028,;									// 	[02]  C   ToolTip do campo // "Motivo de Log" //"Tp Msg"
							"TPMSG",;								// 	[03]  C   Id do Field
							"C",;									// 	[04]  C   Tipo do campo
							1,;										// 	[05]  N   Tamanho do campo
							0,;										// 	[06]  N   Decimal do campo
							Nil,;									// 	[07]  B   Code-block de valida��o do campo
							Nil,;									// 	[08]  B   Code-block de valida��o When do campo
							{'1=Erro','2=Alerta'},;					//	[09]  A   Lista de valores permitido do campo
							.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
							Nil,;									//	[11]  B   Code-block de inicializacao do campo
							.F.,;									//	[12]  L   Indica se trata-se de um campo chave
							.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
							.T.)									// 	[14]  L   Indica se o campo � virtual
		
	EndIf
    
Else    //Estrutura da View


	If ( lOneGrid )

		oStrAge:AddField(	"AGENCI",;				// [01]  C   Nome do Campo
							"01",;						// [02]  C   Ordem
							STR0002,;					// [03]  C   Titulo do campo // "Ag�ncia"
							STR0002,;					// [04]  C   Descricao do campo // "Ag�ncia"
							{STR0002},;	    		// [05]  A   Array com Help // "Ag�ncia"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"NMAGEN",;				// [01]  C   Nome do Campo
							"02",;						// [02]  C   Ordem
							STR0003,;					// [03]  C   Titulo do campo // "Nome da Ag�ncia"
							STR0003,;					// [04]  C   Descricao do campo // "Nome da Ag�ncia"
							{STR0003},;	    		// [05]  A   Array com Help // "Nome da Ag�ncia"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"ECF",;					// [01]  C   Nome do Campo
							"03",;						// [02]  C   Ordem
							STR0005,;				// [03]  C   Titulo do campo // "ECF/Controle"
							STR0006,;			// [04]  C   Descricao do campo // "N�mero de ECF ou C�digo de Controle"
							{STR0006},;			// [05]  A   Array com Help // "N�mero de ECF ou C�digo de Controle"
							"CHECK",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"ARQUIV",;				// [01]  C   Nome do Campo
							"04",;						// [02]  C   Ordem
							STR0007,;					// [03]  C   Titulo do campo // "Arquivo"
							STR0007,;					// [04]  C   Descricao do campo // "Arquivo"
							{STR0007},;	    		// [05]  A   Array com Help // "Arquivo"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						

		oStrAge:AddField(	"DATA",;				// [01]  C   Nome do Campo
							"05",;						// [02]  C   Ordem
							STR0008,;					// [03]  C   Titulo do campo // "Data do Log"
							STR0008,;					// [04]  C   Descricao do campo // "Data do Log"
							{STR0008},;	    		// [05]  A   Array com Help // "Data do Log"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						

		oStrAge:AddField(	"HORA",;				// [01]  C   Nome do Campo
							"06",;						// [02]  C   Ordem
							STR0009,;				// [03]  C   Titulo do campo // "Hora do Log"
							STR0009,;				// [04]  C   Descricao do campo // "Hora do Log"
							{STR0009},;	    	// [05]  A   Array com Help // "Hora do Log"
							"GET",;				    	// [06]  C   Tipo do campo
							"@R 99:99:99",;				// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"LINHA",;				// [01]  C   Nome do Campo
							"07",;						// [02]  C   Ordem
							STR0010,;						// [03]  C   Titulo do campo // "Linha do Arquivo"
							STR0010,;						// [04]  C   Descricao do campo // "Linha do Arquivo"
							{STR0010},;			// [05]  A   Array com Help // "Linha do Arquivo"
							"GET",;					// [06]  C   Tipo do campo
							"@E 999999",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo
		
		oStrAge:AddField(	"TABELA",;				// [01]  C   Nome do Campo
							"08",;						// [02]  C   Ordem
							STR0011,;						// [03]  C   Titulo do campo // "Tabela"
							STR0012,;						// [04]  C   Descricao do campo // "Tabela a ser Atualizada"
							{STR0012},;			// [05]  A   Array com Help // "Tabela a ser Atualizada"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
							
		oStrAge:AddField(	"CAMPO",;				// [01]  C   Nome do Campo
							"09",;						// [02]  C   Ordem
							STR0013,;						// [03]  C   Titulo do campo // "Campo"
							STR0014,;						// [04]  C   Descricao do campo // "Campo a ser Atualizado"
							{STR0014},;			// [05]  A   Array com Help // "Campo a ser Atualizado"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"VLRCPO",;				// [01]  C   Nome do Campo
							"10",;						// [02]  C   Ordem
							STR0015,;						// [03]  C   Titulo do campo // "Valor do Campo"
							STR0015,;						// [04]  C   Descricao do campo // "Valor do Campo"
							{STR0023},;			// [05]  A   Array com Help // "Valor (conte�do) a ser atualizado no campo."
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						

		oStrAge:AddField(	"MOTIVO",;				// [01]  C   Nome do Campo
							"11",;						// [02]  C   Ordem
							STR0016,;						// [03]  C   Titulo do campo // "Motivo"
							STR0017,;						// [04]  C   Descricao do campo // "Motivo de Log"
							{STR0017},;			// [05]  A   Array com Help // "Motivo de Log"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo
							
		For nI := 1 to Len(aFldGIC)
			
			If ( !(Alltrim(aFldGIC[nI,1]) $ cNotSelect) )
			
				If ( SX3->(DbSeek(aFldGIC[nI,1])) )		
					aTamSx3 := TamSx3(aFldGIC[nI,1])
					oStrAge:AddField(	Alltrim(aFldGIC[nI,1]),;		// [01]  C   Nome do Campo
										StrZero(nI+11,2),;				// [02]  C   Ordem
										FWX3Titulo(aFldGIC[nI,1]),;		// [03]  C   Titulo do campo // "Motivo"
										FWSX3Util():GetDescription(aFldGIC[nI,1]),;						// [04]  C   Descricao do campo // "Motivo de Log"
										{FWSX3Util():GetDescription(aFldGIC[nI,1])},;			// [05]  A   Array com Help // "Motivo de Log"
										FWSX3Util():GetFieldType(aFldGIC[nI,1]),;					// [06]  C   Tipo do campo
										Alltrim(GetSx3Cache(aFldGIC[nI,1],"X3_PICTURE")),;						// [07]  C   Picture
										NIL,;						// [08]  B   Bloco de Picture Var
										"",;						// [09]  C   Consulta F3
										.F.,;						// [10]  L   Indica se o campo � alteravel
										NIL,;						// [11]  C   Pasta do campo
										"",;						// [12]  C   Agrupamento do campo
										GTPXCBox(aFldGIC[nI,1]),;						// [13]  A   Lista de valores permitido do campo (Combo)
										NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
										NIL,;						// [15]  C   Inicializador de Browse
										.T.,;						// [16]  L   Indica se o campo � virtual
										NIL,;						// [17]  C   Picture Variavel
										.F.)						// [18]  L   Indica pulo de linha ap�s o campo
				
				EndIf
			
			EndIf
			
		Next nI	
		
		oStrAge:AddField(	"TPMSG",;				// [01]  C   Nome do Campo
							"00",;						// [02]  C   Ordem
							STR0028,;						// [03]  C   Titulo do campo // "Motivo" //"Tp Msg"
							STR0028,;						// [04]  C   Descricao do campo // "Motivo de Log" // "Tp Msg"
							{STR0028},;			// [05]  A   Array com Help // "Motivo de Log" // "Tp Msg"
							"COMBOBOX",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							{'1=Erro','2=Alerta'},;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo
							
		
	Else
		
		//Composi��o da estrutura Main - Grid da Lista das Ag�ncias
		oStrAge:AddField(	"ZZW_CODLOG",;				// [01]  C   Nome do Campo
							"01",;						// [02]  C   Ordem
							STR0001,;					// [03]  C   Titulo do campo // "C�digo do Log"
							STR0001,;					// [04]  C   Descricao do campo // "C�digo do Log"
							{STR0001},;	    		// [05]  A   Array com Help // "C�digo do Log"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"ZZW_AGENCI",;				// [01]  C   Nome do Campo
							"02",;						// [02]  C   Ordem
							STR0002,;					// [03]  C   Titulo do campo // "Ag�ncia"
							STR0002,;					// [04]  C   Descricao do campo // "Ag�ncia"
							{STR0002},;	    		// [05]  A   Array com Help // "Ag�ncia"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrAge:AddField(	"ZZW_NMAGEN",;				// [01]  C   Nome do Campo
							"03",;						// [02]  C   Ordem
							STR0003,;					// [03]  C   Titulo do campo // "Nome da Ag�ncia"
							STR0003,;					// [04]  C   Descricao do campo // "Nome da Ag�ncia"
							{STR0003},;	    		// [05]  A   Array com Help // "Nome da Ag�ncia"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		//Composi��o da Estrutura dos Arquivos da Ag�ncia
		oStrCab:AddField(	"ZZZ_ECF",;					// [01]  C   Nome do Campo
							"01",;						// [02]  C   Ordem
							STR0006,;				// [03]  C   Titulo do campo // "ECF/Controle"
							STR0007,;			// [04]  C   Descricao do campo // "N�mero de ECF ou C�digo de Controle"
							{STR0007},;			// [05]  A   Array com Help // "N�mero de ECF ou C�digo de Controle"
							"CHECK",;					// [06]  C   Tipo do campo 
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrCab:AddField(	"ZZZ_ARQUIV",;				// [01]  C   Nome do Campo
							"02",;						// [02]  C   Ordem
							STR0007,;					// [03]  C   Titulo do campo // "Arquivo"
							STR0007,;					// [04]  C   Descricao do campo // "Arquivo"
							{STR0007},;	    		// [05]  A   Array com Help // "Arquivo"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						


		oStrCab:AddField(	"ZZZ_DATA",;				// [01]  C   Nome do Campo
							"03",;						// [02]  C   Ordem
							STR0008,;					// [03]  C   Titulo do campo // "Data do Log"
							STR0008,;					// [04]  C   Descricao do campo // "Data do Log"
							{STR0008},;	    		// [05]  A   Array com Help // "Data do Log"
							"GET",;				    	// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						

		oStrCab:AddField(	"ZZZ_HORA",;				// [01]  C   Nome do Campo
							"04",;						// [02]  C   Ordem
							STR0009,;				// [03]  C   Titulo do campo // "Hora do Log"
							STR0009,;				// [04]  C   Descricao do campo // "Hora do Log"
							{STR0009},;	    	// [05]  A   Array com Help // "Hora do Log"
							"GET",;				    	// [06]  C   Tipo do campo
							"@R 99:99:99",;				// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.T.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		// Composi��o da Estrutura do Grid Dos Erros do Arquivo
		oStrGrd:AddField(	"ZZY_LINHA",;				// [01]  C   Nome do Campo
							"01",;						// [02]  C   Ordem
							STR0010,;						// [03]  C   Titulo do campo // "Linha do Arquivo"
							STR0010,;						// [04]  C   Descricao do campo // "Linha do Arquivo"
							{STR0010},;			// [05]  A   Array com Help //"Linha do Arquivo"
							"GET",;					// [06]  C   Tipo do campo
							"@E 999999",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo
		
		oStrGrd:AddField(	"ZZY_TABELA",;				// [01]  C   Nome do Campo
							"02",;						// [02]  C   Ordem
							STR0011,;						// [03]  C   Titulo do campo // "Tabela"
							STR0012,;						// [04]  C   Descricao do campo // "Tabela a ser Atualizada"
							{STR0012},;			// [05]  A   Array com Help // "Tabela a ser Atualizada"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
							
		oStrGrd:AddField(	"ZZY_CAMPO",;				// [01]  C   Nome do Campo
							"03",;						// [02]  C   Ordem
							STR0013,;						// [03]  C   Titulo do campo // "Campo"
							STR0014,;						// [04]  C   Descricao do campo // "Campo a ser Atualizado"
							{STR0014},;			// [05]  A   Array com Help // "Campo a ser Atualizado"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo

		oStrGrd:AddField(	"ZZY_VLRCPO",;				// [01]  C   Nome do Campo
							"04",;						// [02]  C   Ordem
							STR0015,;						// [03]  C   Titulo do campo // "Valor do Campo"
							STR0015,;						// [04]  C   Descricao do campo // "Valor do Campo"
							{STR0023},;			// [05]  A   Array com Help // "Valor (conte�do) a ser atualizado no campo."
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo						

		oStrGrd:AddField(	"ZZY_MOTIVO",;				// [01]  C   Nome do Campo
							"05",;						// [02]  C   Ordem
							STR0016,;						// [03]  C   Titulo do campo // "Motivo"
							STR0017,;						// [04]  C   Descricao do campo  // "Motivo de Log"
							{STR0017},;			// [05]  A   Array com Help // "Motivo de Log"
							"GET",;					// [06]  C   Tipo do campo
							"",;						// [07]  C   Picture
							NIL,;						// [08]  B   Bloco de Picture Var
							"",;						// [09]  C   Consulta F3
							.F.,;						// [10]  L   Indica se o campo � alteravel
							NIL,;						// [11]  C   Pasta do campo
							"",;						// [12]  C   Agrupamento do campo
							NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
							NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
							NIL,;						// [15]  C   Inicializador de Browse
							.T.,;						// [16]  L   Indica se o campo � virtual
							NIL,;						// [17]  C   Picture Variavel
							.F.)						// [18]  L   Indica pulo de linha ap�s o campo
	EndIf

EndIf

RestArea(aAreaSX3)

GtpDestroy(aTamSx3)

Return()

/*/{Protheus.doc} GA036CSetLog
    Fun��o que carrega em mem�ria (em array est�tico) os dados do log
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/02/2017
    @version 1
    @param	aDataCab, array, Dados refrente a ag�ncia e arquivo que foi importado.
				aDataCab[1], caractere, c�digo da ag�ncia
                aDataCab[2], caractere, nome da ag�ncia
                aDataCab[3], caractere, diret�ro e nome do arquivo importado
                aDataCab[4], data, data do log (dDataBase)
                aDataCab[5], caractere, Hora do log (Time())
            	aDataCab[6], caractere, n�mero do ECF
			aDataDet, array, dados do detalhe do log
				aDataDet[1], caractere, n�mero do ECF
                aDataDet[2], caractere, linha do arquivo de importa��o que deu o erro
                aDataDet[3], caractere, Alias da Tabela que deveria ser atualizada
                aDataDet[4], caractere, campo que seria atualizado
                aDataDet[5], caractere, mensagem de erro
                aDataDet[6], caractere, qual � o conte�do que iria atualizar no campo referido acima
    @return aG036CLog, array, array est�tico com o seguinte mapeamento:
				aG036CLog[n,1], caractere, c�digo do log
				aG036CLog[n,2], caractere, c�digo da ag�ncia
				aG036CLog[n,3], caractere, nome da ag�ncia
				aG036CLog[n,4], array, array com os dados do arquivo
					aG036CLog[n,4][y,1], caractere, c�digo da ag�ncia
					aG036CLog[n,4][y,2], caractere, caminho e nome do arquivo importado
					aG036CLog[n,4][y,3], data, data do log
					aG036CLog[n,4][y,4], caractere, hora do log
					aG036CLog[n,4][y,5], array, detalhes do erro
						aG036CLog[n,4][y,5][x,1], caractere, n�mero do ECF
						aG036CLog[n,4][y,5][x,2], caractere, linha do arquivo de importa��o que deu o erro
						aG036CLog[n,4][y,5][x,3], caractere, Alias da Tabela que deveria ser atualizada
						aG036CLog[n,4][y,5][x,4], caractere, campo que seria atualizado
						aG036CLog[n,4][y,5][x,5], caractere, mensagem de erro
						aG036CLog[n,4][y,5][x,6], caractere, qual � o conte�do que iria atualizar no campo referido acima
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036CSetLog(aDataCab,aDataDet)

Local aAgenLog	:= {}
Local aDetLog	:= {}
Local nPAgencia	:= 0
Local nPHeader	:= 0
Local nPItems	:= 0

nPHeader := aScan(aG036CLog,{|x| x[2] == aDataCab[1]})

If ( nPHeader == 0 )

	aAdd(aG036CLog,{	cG036CodLog,;	//C�digo do Log
						aDataCab[1],;	//C�digo da Ag�ncia
						aDataCab[2],;	//Descri��o (Nome) da Ag�ncia						
						{} })

	nPHeader := Len(aG036CLog)

EndIf

aAgenLog := aClone(aG036CLog[nPHeader,4])

nPAgencia := aScan(aAgenLog,{|x| x[1] == aDataCab[1] .And. x[5] == aDataCab[6] .And. x[2] == aDataCab[3] })

If ( nPAgencia == 0 )

	aAdd(aAgenLog,{		aDataCab[1],;	//C�digo da Ag�ncia
						aDataCab[3],;	//Arquivo
						aDataCab[4],;	//Data do Log
						aDataCab[5],;	//Hora do Log
						aDataCab[6],;	//Nro do ECF
						{} })

	nPAgencia := Len(aAgenLog)

EndIf     

aDetLog := aClone(aAgenLog[nPAgencia,6])

nPItems := aScan(aDetLog,{|x| x[1]+x[2]+x[3]+x[4]+x[6] == aDataDet[1]+aDataDet[2]+aDataDet[3]+aDataDet[4]+aDataDet[6] })

//Itens do par�metro array aDataDet da Fun��o GA036CAddLog
If ( nPItems == 0 )

	aAdd(aDetLog,{	aDataDet[1],;
					aDataDet[2],;
					aDataDet[3],;
					aDataDet[4],;
					aDataDet[5],;
					aDataDet[6],;
					aClone(aDataDet[7]),;
					aDataDet[8]})

Else

	aDetLog[nPItems,1] 	:= aDataDet[1]
	aDetLog[nPItems,2]	:= aDataDet[2]
	aDetLog[nPItems,3] 	:= aDataDet[3]
	aDetLog[nPItems,4]	:= aDataDet[4]
	aDetLog[nPItems,5]	:= aDataDet[5]
	aDetLog[nPItems,6]	:= aDataDet[6]
	aDetLog[nPItems,7]	:= aClone(aDataDet[7])
	aDetLog[nPItems,8]	:= aDataDet[8]

EndIf

aG036CLog[nPHeader,4] := aClone(aAgenLog)
aG036CLog[nPHeader,4][nPAgencia,6] := aClone(aDetLog) 

Return(aG036CLog)

/*/{Protheus.doc} GA036CTable
    Fun��o respons�vel por criar as tabelas tempor�rias que ser� utilizado para a montagem 
	do MVC de Log De Erros
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036CTable()

Local aStrFake		:= {}
Local aStrZZZ		:= {}
Local aStrZZY		:= {}
Local aStrZZW		:= {}
Local aGICFld		:= {} 
Local aTamSx3       := {}
Local nI			:= 0

G036SetGIC()

aGICFld := G036GICData()

If ( Select("FAKE") > 0 )
	FAKE->(DbCloseArea())
EndIf	

If ( Select("ZZW") > 0 )
	ZZW->(DbCloseArea())
EndIf

If ( Select("ZZY") > 0 )
	ZZY->(DbCloseArea())
EndIf

If ( Select("ZZZ") > 0 )
	ZZZ->(DbCloseArea())
EndIf

If ( ValType(oG036CFake) == "U" )
	
	aAdd(aStrFake,{"FAKE_COD","C",8,0})
	
	oG036CFake := FWTemporaryTable():New("FAKE")
	
	oG036CFake:SetFields(aStrFake)
	oG036CFake:AddIndex("FAKE_INDEX", {"FAKE_COD"})
	oG036CFake:Create()

EndIf

If ( ValType(oG036CAge) == "U" )	
	
	aAdd(aStrZZW,{"ZZW_CODLOG","C",8,0})
	aAdd(aStrZZW,{"ZZW_AGENCI","C",TamSx3("GI6_CODIGO")[1],0})
	aAdd(aStrZZW,{"ZZW_NMAGEN","C",TamSx3("GI6_DESCRI")[1],0})

	oG036CAge := FWTemporaryTable():New("ZZW")

	oG036CAge:SetFields(aStrZZW)
	oG036CAge:AddIndex("ZZW_INDEX", {"ZZW_CODLOG","ZZW_AGENCI"})
	oG036CAge:Create()

EndIf

If ( Valtype(oG036CCab) == "U" )

	//Estrutura DO Arquivo ZZZ - Cabe�alho
	aAdd(aStrZZZ,{"ZZZ_CODLOG","C",8,0})
	aAdd(aStrZZZ,{"ZZZ_AGENCI","C",TamSx3("GI6_CODIGO")[1],0})
	aAdd(aStrZZZ,{"ZZZ_ECF","C",TamSx3("GIC_ECFSER")[1],0})
	aAdd(aStrZZZ,{"ZZZ_ARQUIV","C",260,0,})
	aAdd(aStrZZZ,{"ZZZ_DATA","D",8,0,})
	aAdd(aStrZZZ,{"ZZZ_HORA","C",8,0})
	
	oG036CCab := FWTemporaryTable():New("ZZZ")
	
	oG036CCab:SetFields(aStrZZZ)
	oG036CCab:AddIndex("ZZZ_INDEX", {"ZZZ_CODLOG","ZZZ_AGENCI","ZZZ_ECF"})
	oG036CCab:Create()

EndIf

If ( Valtype(oG036CGrd) == "U" )

	//Estrutura do Aqruivo ZZY - Grid
	aAdd(aStrZZY,{"ZZY_CODLOG","C",8,0})
	aAdd(aStrZZY,{"ZZY_AGENCI","C",TamSx3("GI6_CODIGO")[1],0})
	aAdd(aStrZZY,{"ZZY_ECF","C",TamSx3("GIC_ECFSER")[1],0})
	aAdd(aStrZZY,{"ZZY_LINHA","C",6,0})
	aAdd(aStrZZY,{"ZZY_TABELA","C",10,0})
	aAdd(aStrZZY,{"ZZY_CAMPO","C",20,0})
	aAdd(aStrZZY,{"ZZY_VLRCPO","C",30,0})
	aAdd(aStrZZY,{"ZZY_MOTIVO","C",300,0})
	aAdd(aStrZZY,{"ZZY_TPMSG","C",1,0})
	
	SX3->(DbSetOrder(2))
	
	For nI := 1 to Len(aGICFld)
		
		If ( SX3->(DbSeek(aGICFld[nI,1])) )
            aTamSx3 := TamSx3(aGICFld[nI,1])
			aAdd(aStrZZY,{Alltrim(aGICFld[nI,1]),aTamSx3[3],aTamSx3[1],aTamSx3[2]})	
		EndIf
		
	Next nI
	
	oG036CGrd := FWTemporaryTable():New("ZZY")
	
	oG036CGrd:SetFields(aStrZZY)
	oG036CGrd:AddIndex("ZZY_INDEX", {"ZZY_TPMSG","ZZY_CODLOG","ZZY_AGENCI","ZZY_ECF","ZZY_LINHA","ZZY_TABELA","ZZY_CAMPO","ZZY_VLRCPO"})
	oG036CGrd:Create()

EndIf

GtpDestroy(aTamSx3)

Return()

/*/{Protheus.doc} G36CCargaTabLog
	Esta fun��o � chamada quando h� necessidade de gravar os dados do array est�tico aG036CLog
	nas tabelas tempor�rias.
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param 
	@return nil, nulo, sem retorno
	@example
	(examples)
	@see (links_or_references)
/*/
Function G36CCargaTabLog()

Local lInsert	:= .f.

Local nI 		:= 0
Local nX		:= 0
Local nY		:= 0
Local nW		:= 0

Local cAliasZZW	:= oG036CAge:GetAlias()
Local cAliasZZZ	:= oG036CCab:GetAlias()
Local cAliasZZY	:= oG036CGrd:GetAlias()
Local cAliasFake:= oG036CFake:GetAlias()
Local cChave	:= ""

If ( !Empty(cG036CodLog) .Or. Len(aG036CLog) > 0)

	(cAliasZZW)->(DbSetOrder(1))
	(cAliasZZZ)->(DbSetOrder(1))
	(cAliasZZY)->(DbSetOrder(1))
	(cAliasFake)->(DbSetOrder(1))

	If ( (cAliasFake)->(DbSeek(cG036CodLog)) )
		lInsert := .f.
	Else
		linsert := .t.
	EndIf	

	RecLock(cAliasFake,lInsert)

	(cAliasFake)->FAKE_COD := cG036CodLog

	(cAliasFake)->(MsUnlock())

	For nY := 1 to Len(aG036CLog)

		If ( (cAliasZZW)->(DbSeek(cG036CodLog+aG036CLog[nY,2])) )
			lInsert := .f.
		Else
			lInsert := .t.
		EndIf

		RecLock(cAliasZZW,lInsert)

		(cAliasZZW)->(ZZW_CODLOG)	:= cG036CodLog		//C�d. do Log
		(cAliasZZW)->(ZZW_AGENCI)	:= aG036CLog[nY,2]	//C�d. Agencia
		(cAliasZZW)->(ZZW_NMAGEN) 	:= aG036CLog[nY,3]	//Nome da Ag�ncia

		(cAliasZZW)->(MsUnlock())

		For nI := 1 to Len(aG036CLog[nY,4])
			
			//Pesquisa por Agencia + Arquivo
			If ( (cAliasZZZ)->(DbSeek(aG036CLog[nY,4][nI,1]+aG036CLog[nY,4][nI,5])) )	
				lInsert := .f.
			Else
				lInsert := .t.	
			EndIf

			RecLock(cAliasZZZ,lInsert)
			
			(cAliasZZZ)->(ZZZ_CODLOG)	:= cG036CodLog
			(cAliasZZZ)->(ZZZ_AGENCI)	:= aG036CLog[nY,2]
			(cAliasZZZ)->(ZZZ_ECF)		:= aG036CLog[nY,4][nI,5]
			(cAliasZZZ)->(ZZZ_ARQUIV) 	:= aG036CLog[nY,4][nI,2]
			(cAliasZZZ)->(ZZZ_DATA) 	:= aG036CLog[nY,4][nI,3]
			(cAliasZZZ)->(ZZZ_HORA) 	:= aG036CLog[nY,4][nI,4]

			(cAliasZZZ)->(MsUnlock())

			For nX := 1 to Len(aG036CLog[nY,4][nI,6])
				
				//Busca por Ag�ncia + Nro ECF + linha do arquivo que deu Erro
				cChave := aG036CLog[nY,2]				//ZZY_AGENCI: C�d. Agencia
				cChave += aG036CLog[nY,4][nI,5]			//ZZY_ECF: Nro do ECF
				cChave += aG036CLog[nY,4][nI,6][nX,2]	//ZZY_LINHA: Nro da Linha do Arquivo
				cChave += aG036CLog[nY,4][nI,6][nX,3]	//ZZY_TABELA: Alias da Tabela
				cChave += aG036CLog[nY,4][nI,6][nX,4]	//ZZY_CAMPO: Nome do Campo
				cChave += aG036CLog[nY,4][nI,6][nX,6]	//ZZY_VLRCPO: Conte�do do Campo

				If ( (cAliasZZY)->(DbSeek(cChave)))
					lInsert := .f.
				Else
					lInsert := .t.
				EndIf				

				RecLock(cAliasZZY,lInsert)

				(cAliasZZY)->(ZZY_CODLOG)	:= cG036CodLog
				(cAliasZZY)->(ZZY_AGENCI)	:= aG036CLog[nY,2]
				(cAliasZZY)->(ZZY_ECF)		:= aG036CLog[nY,4][nI,5]
				(cAliasZZY)->(ZZY_LINHA)	:= aG036CLog[nY,4][nI,6][nX,2]
				(cAliasZZY)->(ZZY_TABELA)	:= aG036CLog[nY,4][nI,6][nX,3]
				(cAliasZZY)->(ZZY_CAMPO)	:= aG036CLog[nY,4][nI,6][nX,4]
				(cAliasZZY)->(ZZY_MOTIVO)	:= aG036CLog[nY,4][nI,6][nX,5]
				(cAliasZZY)->(ZZY_VLRCPO)	:= aG036CLog[nY,4][nI,6][nX,6]
				(cAliasZZY)->(ZZY_TPMSG)	:= aG036CLog[nY,4][nI,6][nX,8]
				
				For nW := 1 to Len(aG036CLog[nY,4][nI,6][nX,7])
					(cAliasZZY)->&(aG036CLog[nY,4][nI,6][nX,7][nW,1]) := aG036CLog[nY,4][nI,6][nX,7][nW,2] 
				Next nW
				
				(cAliasZZY)->(MsUnlock())

			Next nX

		Next nI

	Next nY 

EndIf

Return()

/*/{Protheus.doc} GA036CDestroy
    Fun��o que destroy (finaliza) as inst�ncias da Classe FwTemporaryTable - que foram
	utilizadas para a montagem das tabelas tempor�rias.
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return nil, nulo, sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036CDestroy()

If ( !FwIsInCallStack('FwAlertExitPage') )
	
	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ( ValType(oG036CFake) == "O" )
		oG036CFake:Delete()
		FreeObj(oG036CFake)
	EndIf

	If ( ValType(oG036CAge) == "O" )
		oG036CAge:Delete()
		FreeObj(oG036CAge)
	EndIf

	If ( ValType(oG036CCab) == "O" )
		oG036CCab:Delete()
		FreeObj(oG036CCab)
	EndIf

	If ( ValType(oG036CGrd) == "O" )
		oG036CGrd:Delete()
		FreeObj(oG036CGrd)
	EndIf
	
EndIf

Return()

/*/{Protheus.doc} GA036CArrLog
    Fun��o respons�vel por retornar o array est�tico aG036CLog
    @type  Function
    @author Fernando Radu Muscalu
    @since 27/03/2017
    @version 1
    @param 
    @return aG036CLog, array, Array est�tico que possui as informa��es dos logs de erros 
				aG036CLog[n,1], caractere, c�digo do log
				aG036CLog[n,2], caractere, c�digo da ag�ncia
				aG036CLog[n,3], caractere, nome da ag�ncia
				aG036CLog[n,4], array, array com os dados do arquivo
					aG036CLog[n,4][y,1], caractere, c�digo da ag�ncia
					aG036CLog[n,4][y,2], caractere, caminho e nome do arquivo importado
					aG036CLog[n,4][y,3], data, data do log
					aG036CLog[n,4][y,4], caractere, hora do log
					aG036CLog[n,4][y,5], array, detalhes do erro
						aG036CLog[n,4][y,5][x,1], caractere, n�mero do ECF
						aG036CLog[n,4][y,5][x,2], caractere, linha do arquivo de importa��o que deu o erro
						aG036CLog[n,4][y,5][x,3], caractere, Alias da Tabela que deveria ser atualizada
						aG036CLog[n,4][y,5][x,4], caractere, campo que seria atualizado
						aG036CLog[n,4][y,5][x,5], caractere, mensagem de erro
						aG036CLog[n,4][y,5][x,6], caractere, qual � o conte�do que iria atualizar no campo referido acima
    @example
    (examples)
    @see (links_or_references)
/*/
Function GA036CArrLog()
Return(aG036CLog)


/*/{Protheus.doc} GA036CLogReset
	Fun��o respons�vel por inicializar o array est�tico aG036CLog em branco.
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version version
	@param 
	@return nil, nulo, sem retorno
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CLogReset()

aG036CLog := {}

Return()

/*/{Protheus.doc} GA036RetLnRead
	Fun��o que retorna o conte�do da vair�vel est�tica nG036ReadLn. Esta vari�vel � utilizadas
	para armazenar a linha atual do arquivo de importa��o que est� sendo lido pelo programa GTPA036
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param 
	@return nG036ReadLn, num�rico, nro da linha do arquivo importado
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036RetLnRead()
Return (nG036ReadLn)

/*/{Protheus.doc} GA036CAddLn
	Incrementa em 1 a vari�vel est�tica nG036ReadLn em um.
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param 
	@return nG036ReadLn, num�rico, nro da linha do arquivo importado com o incremento de 1.
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CAddLn()

nG036ReadLn++

Return (nG036ReadLn)

/*/{Protheus.doc} GA036CSubLn
	Decrementa em 1 a vari�vel est�tica nG036ReadLn
	@type  Function
	@author FernandO Radu Muscalu
	@since 27/03/2017
	@version 1
	@param 
	@return nG036ReadLn, num�rico, nro da linha do arquivo importado com o Decremento de 1.
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CSubLn()

nG036ReadLn--

Return (nG036ReadLn)

/*/{Protheus.doc} GA036CResetLn
	Inicializa a vair�vel est�tica nG036ReadLn com o valor 0.
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param 
	@return nG036ReadLn, num�rico, retorna o conte�do de nG036ReadLn j� com o valor em 0
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CResetLn()

nG036ReadLn := 0

Return (nG036ReadLn)

/*/{Protheus.doc} GA036CLoad
	Fun��o respons�vel pela carga dos submodelos do MVC.
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	oSub, objeto, inst�ncia da classe FWFormFieldsModel ou da classe FWFormGridModel
			cIdSub, caractere, id do submodelo que ser� carregado
			lOneGrid, l�gico, .t. quando se carrega somente um grid
	@return aRet, array, array com os dados a serem carregados no submodelo de cIdSub
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function GA036CLoad(oSub,cIdSub,lOneGrid)

Local cAliasTab	:= ""

Local nI		:= 0

Local aFldSub	:= {}
Local aRet 		:= {}
Local aAux		:= {}

Default lOneGrid := .t.

aFldSub := oSub:GetStruct():GetFields()

If ( cIdSub == "FAKE" )

	cAliasTab := oG036CFake:GetAlias()
	
	(cAliasTab)->(DbGoTop())

	For nI := 1 to Len(aFldSub)

		If ( (cAliasTab)->(FieldPos(aFldSub[nI,3])) > 0 )
			aAdd(aAux,(cAliasTab)->&(aFldSub[nI,3]))
		Else
			aAdd(aAux,GTPCastType(,aFldSub[nI,4]))
		EndIf

	Next nI

	aAdd(aRet,aClone(aAux))
	aAdd(aRet,(cAliasTab)->(Recno()))

Else

	If ( lOneGrid )
		cAliasTab := GaNewLoad()
	Else

		If ( cIdSub == "MAIN" ) 
			cAliasTab := oG036CAge:GetAlias()
		ElseIf ( cIdSub == "MASTER" )
			cAliasTab := oG036CCab:GetAlias()
		Else
			cAliasTab := oG036CGrd:GetAlias()
		EndIf		

	EndIf

	(cAliasTab)->(DbGoTop())

	While ( (cAliasTab)->(!Eof()) )
				
		For nI := 1 to Len(aFldSub)

			If ( (cAliasTab)->(FieldPos(aFldSub[nI,3])) > 0 )
				aAdd(aAux,(cAliasTab)->&(aFldSub[nI,3]))
			Else
				aAdd(aAux,GTPCastType(,aFldSub[nI,4]))
			EndIf

		Next nI
		
		aAdd(aRet,{(cAliasTab)->(Recno()),aClone(aAux)})
		aAux := {}
		
		(cAliasTab)->(DbSkip())
		
	EndDo

EndIf

If ( lOneGrid )
	(cAliasTab)->(DbCloseArea())
EndIf	

Return(aRet)

/*/{Protheus.doc} GaNewLog
	Captura o pr�ximo c�digo do log de erro
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	
	@return cRet, caractere, valor do pr�ximo c�digo de log
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function GaNewLog()

Local cNxtAlias := GetNextAlias()
Local cFrom		:= "%" + oG036CFake:GetRealName() + "%"
Local cRet		:= ""

BeginSQL Alias cNxtAlias

	SELECT
		MAX(FAKE_COD) LAST_COD
	FROM
		%Exp:cFrom% TMP_TAB	
EndSQL

If ( (cNxtAlias)->(!Eof())  )
	cRet := Soma1((cNxtAlias)->LAST_COD)
Else
	cRet := StrZero(1,8)
EndIf		

(cNxtAlias)->(DbCloseArea())

Return(cRet)

/*/{Protheus.doc} GA036CCodLog
	Inicia a vari�vel est�tica cG036CodLog com o pr�ximo c�digo de log
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	
	@return nil, nulo, sem retorno
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CCodLog()

If ( Empty(cG036CodLog) )
	cG036CodLog := GaNewLog()
EndIf

Return()

/*/{Protheus.doc} GA036CRetLog
	Retorna o conte�do da vari�vel est�tica cG036CodLog
	@type  Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	
	@return cG036CodLog, caractere, c�digo do Log
	@example
	(examples)
	@see (links_or_references)
/*/
Function GA036CRetLog()
Return(cG036CodLog)

/*/{Protheus.doc} GA036CSetModel
	Esta fun��o configura o modelo de dados do MVC
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	lOneGrid, l�gico, .t. - o modelo de dados utilizar� somente uma grid para montagem do 
		log de erros	
	@return oModel, objeto, inst�ncia da classe FwFormModel
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function GA036CSetModel(lOneGrid)

Local oModel	:= Nil
Local oStrMain	:= FWFormModelStruct():New()
Local oStrAge	:= Nil
Local oStrCab	:= Nil
Local oStrGrd	:= Nil

Local aRelation := {}

Default lOneGrid := .t.

//Define as estruturas dos submodelos no que diz respeito ao Modelo de Dados
oModel := FWFormModel():New("GTPA036C",{|| .t.},{|| .t.},{|| .t.},{|| .t.})

If ( lOneGrid )
	
	//Cria��o das Estruturas
	oStrGrd := FWFormModelStruct():New()
	GA036CStruct(oStrMain,oStrGrd,,,"M",lOneGrid)
	
	oModel:AddFields("FAKE", /*cOwner*/, oStrMain,,,{|oSub| GA036CLoad(oSub,"FAKE") })
	
	//Defini��o da Grid que Lista as Ag�ncias que deram erro na Importa��o
	oModel:AddGrid("MAIN", "FAKE", oStrGrd,,,,,{|oSub| GA036CLoad(oSub,"MAIN",lOneGrid) })

Else

	//Cria��o das Estruturas
	oStrAge	:= FWFormModelStruct():New()
	oStrCab	:= FWFormModelStruct():New()
	oStrGrd	:= FWFormModelStruct():New()

	//Define as estruturas dos submodelos no que diz respeito ao Modelo de Dados
	GA036CStruct(oStrMain,oStrAge,oStrCab,oStrGrd,"M",lOneGrid)

	//Defini��o do Cabe�alho "Fake" do Modelo De Dados.
	oModel:AddFields("FAKE", /*cOwner*/, oStrMain,,,{|oSub| GA036CLoad(oSub,"FAKE") })
	//Defini��o da Grid que Lista as Ag�ncias que deram erro na Importa��o
	oModel:AddGrid("MAIN", "FAKE", oStrAge,,,,,{|oSub| GA036CLoad(oSub,"MAIN") })
	//Defini��o da Grid que Lista dos arquivos que deram erro na Importa��o
	oModel:AddGrid("MASTER", "MAIN", oStrCab,,,,,{|oSub| GA036CLoad(oSub,"MASTER") })
	//Defini��o da Grid que Lista os Erros dos Arquivos que foram ou tentaram ser importados
	oModel:AddGrid("DETAIL", "MASTER", oStrGrd,,,,,{|oSub| GA036CLoad(oSub,"DETAIL") })

	oModel:GetModel("MAIN"):SetOptional(.T.)
	oModel:GetModel("MASTER"):SetOptional(.T.)
	oModel:GetModel("DETAIL"):SetOptional(.T.)

	//Cria��o das Rela��es
	aRelation := {{"ZZW_CODLOG","FAKE_COD"} } 
	oModel:SetRelation('MAIN',aRelation,(oG036CAge:GetAlias())->(IndexKey(1)))

	aRelation := {{"ZZZ_CODLOG","ZZW_CODLOG"},{"ZZZ_AGENCI","ZZW_AGENCI"}} 
	oModel:SetRelation('MASTER',aRelation,(oG036CCab:GetAlias())->(IndexKey(1)))

	aRelation := {{"ZZY_CODLOG","ZZZ_CODLOG"},{"ZZY_AGENCI","ZZZ_AGENCI"},{"ZZY_ECF","ZZZ_ECF"}}
	oModel:SetRelation('DETAIL',aRelation,(oG036CGrd:GetAlias())->(IndexKey(1)))

	//Somente para visualizar os dados
	oModel:GetModel("FAKE"):SetOnlyView(.t.)
	oModel:GetModel("MAIN"):SetOnlyView(.t.)
	oModel:GetModel("MASTER"):SetOnlyView(.t.)
	oModel:GetModel("DETAIL"):SetOnlyView(.t.)

	//Define as descri��es dos submodelos e modelo
	oModel:SetDescription(STR0024) // "Log de Erros de Importa��o"
	oModel:GetModel("FAKE"):SetDescription(STR0024) // "Log de Erros de Importa��o"
	oModel:GetModel("MAIN"):SetDescription(STR0025) // "Lista de Ag�ncias"
	oModel:GetModel("MASTER"):SetDescription(STR0026) // "Arquivos Importados/Exportados"
	oModel:GetModel("DETAIL"):SetDescription(STR0027) // "Lista de Erros Encontrados"


EndIf

//Defini��o da Chave �nica
oModel:SetPrimaryKey({})

Return(oModel)

/*/{Protheus.doc} GA036CSetView
	Esta fun��o configura a view do MVC
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	lOneGrid, l�gico, .t. - a view utilizar� somente uma grid para a apresenta��o 
		do log de erros	
	@return oView, objeto, inst�ncia da classe FWFormView
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function GA036CSetView(lOneGrid)

Local oView		:= FWFormView():New()
Local oModel	:= FwLoadModel("GTPA036C")
Local oStrAge  	:= Nil
Local oStrCab   := Nil
Local oStrGrd	:= Nil

Default lOneGrid := .t.

If ( lOneGrid )
	
	oStrGrd := FWFormViewStruct():New()
	
	GA036CStruct(,oStrGrd,,,"V")

	oView:SetModel( oModel )
	oView:AddGrid("VIEW_MAIN", oStrGrd, "MAIN" )
	oView:CreateHorizontalBox("BX_MAIN",100)
	oView:SetOwnerView("VIEW_MAIN", "BX_MAIN")
	oView:EnableTitleView("VIEW_MAIN")

Else

 	oStrAge  	:= FWFormViewStruct():New()
 	oStrCab   	:= FWFormViewStruct():New()
 	oStrGrd		:= FWFormViewStruct():New()

	//Define as estruturas dos submodelos no que diz respeito � View
	GA036CStruct(,oStrAge,oStrCab,oStrGrd,"V")

	// Define qual o Modelo de dados ser� utilizado
	oView:SetModel( oModel )

	oView:AddGrid("VIEW_MAIN", oStrAge, "MAIN" )
	oView:AddGrid("VIEW_CAB", oStrCab, "MASTER" )
	oView:AddGrid("VIEW_GRID", oStrGrd, 'DETAIL')

	// Divis�o Horizontal
	oView:CreateHorizontalBox("BX_MAIN",30)
	oView:CreateHorizontalBox("HEADER",30)
	oView:CreateHorizontalBox("GRID",40)

	oView:SetOwnerView("VIEW_MAIN", "BX_MAIN")
	oView:SetOwnerView("VIEW_CAB", "HEADER")
	oView:SetOwnerView("VIEW_GRID", "GRID")

	//Habitila os t�tulos dos modelos para serem apresentados na tela
	oView:EnableTitleView("VIEW_MAIN")
	oView:EnableTitleView("VIEW_CAB")
	oView:EnableTitleView("VIEW_GRID")

EndIf

Return(oView)

/*/{Protheus.doc} GaNewLoad
	Fun��o respons�vel pela montagem do ResultSet para carga do submodelo em grid do MVC, 
	quando este utilizadas somente um Grid para apresentar a lista de erros que forma o log.
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param
	@return cNxtAlias, caractere, Alias da tabela do ResultSet
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function GaNewLoad()

Local cNxtAlias := GetNextAlias()
Local cTabFAKE	:= "%" + oG036CFake:GetRealName() + "%"
Local cTabZZW	:= "%" + oG036CAge:GetRealName() + "%"
Local cTabZZZ	:= "%" + oG036CCab:GetRealName() + "%"
Local cTabZZY	:= "%" + oG036CGrd:GetRealName() + "%"
Local cFldZZY	:= ""
Local cNotSelect:= "ZZY_CODLOG|ZZY_AGENCI|ZZY_ECF|GIC_ECFSER"

Local aFldZZY	:= oG036CGrd:GetStruct():GetFields()

aEval(aFldZZY,{|x| cFldZZY += Iif( !(Alltrim(x[1]) $ cNotSelect),Iif(Substr(x[1],1,3) == "ZZY", x[1] + " " + Substr(x[1],5), x[1] ) + ",","")})

cFldZZY := "% " + SubStr(cFldZZY,1,Rat(",",cFldZZY)-1) + " %"

BeginSQL Alias cNxtAlias

 	Column DATA as Date
	Column GIC_DTVEND as Date
	Column GIC_DTVIAG as Date
	
	SELECT 
		//FAKE_COD	CODLOG,
		ZZW_AGENCI	AGENCI,
		ZZW_NMAGEN	NMAGEN,
		ZZZ_ECF		ECF,
		ZZZ_ARQUIV	ARQUIV,
		ZZZ_DATA	DATA,
		ZZZ_HORA	HORA,
		%Exp:cFldZZY% 
	FROM 
		%Exp:cTabFAKE% FAKE
	INNER JOIN
		%Exp:cTabZZW% ZZW
	ON
		ZZW.ZZW_CODLOG = FAKE.FAKE_COD
		AND ZZW.D_E_L_E_T_ = ''
	INNER JOIN
		%Exp:cTabZZZ% ZZZ
	ON
		ZZW.ZZW_AGENCI = ZZZ.ZZZ_AGENCI
		AND ZZZ.D_E_L_E_T_ = ''
	INNER JOIN
		%Exp:cTabZZY% ZZY
	ON
		ZZZ.ZZZ_AGENCI = ZZY.ZZY_AGENCI
		AND ZZZ.ZZZ_ECF = ZZY.ZZY_ECF
		AND ZZY.D_E_L_E_T_ =''
	ORDER BY
		FAKE_COD,
		ZZW_AGENCI,
		TPMSG,
		ZZZ_ECF,
		ZZY_LINHA

EndSQL

Return (cNxtAlias)
