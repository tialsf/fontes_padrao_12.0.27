#include "GTPA036D.CH"
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'

Static oG036Table  := nil   //objeto inst�ncia da Classe FWTemporaryTable

/*/{Protheus.doc} ModelDef

@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@return $oModel, $Modelo de dados
@example
(examples)
@see (links_or_references)
/*/Static Function ModelDef()

Local oStrCab	:= FWFormModelStruct():New()
Local oStrGI6	:= FWFormStruct( 1, "GI6",,.F. )
Local oStrGZ0	:= FWFormStruct( 1, "GZ0",,.F. )
Local oStrGZ1	:= FWFormStruct( 1, "GZ1",,.F. )
Local oStrGI4	:= FWFormStruct( 1, "GI4",,.F. )
Local oStrG5I	:= FWFormStruct( 1, "G5I",,.F. )

//Cria tabela tempor�ria para o cabe�alho fake utilizado no Modelo
GA036Table()

//Define as estruturas dos submodelos no que diz respeito ao Modelo de Dados
GA036Struct(oStrCab,oStrGI6,oStrGZ1,"M")

oModel := MPFormModel():New("GTPA036D")
oModel:AddFields("MASTER", /*cOwner*/, oStrCab)

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
oModel:AddGrid("GI6DETAIL", "MASTER", oStrGI6,,,,,{|oMdl| GA036Load(oMdl)})
oModel:AddGrid("GZ0DETAIL", "GI6DETAIL", oStrGZ0)
oModel:AddGrid("GZ1DETAIL", "GI6DETAIL", oStrGZ1)
oModel:AddGrid("G5IDETAIL", "GZ1DETAIL", oStrG5I)
oModel:AddGrid("GI4ADETAIL", "G5IDETAIL", oStrGI4)
oModel:AddGrid("GI4BDETAIL", "G5IDETAIL", oStrGI4)

oModel:SetRelation("GI6DETAIL",{{ "GI6_CODIGO"	, "XXX_CODGI6"} },GI6->(IndexKey(1)))
oModel:SetRelation("GZ0DETAIL",{{ "GZ0_CODGI6"	, "GI6_CODIGO"} },GZ0->(IndexKey(1)))
oModel:SetRelation("GZ1DETAIL",{{ "GZ1_CODGI6"	, "GI6_CODIGO"} },GZ1->(IndexKey(1)))
oModel:SetRelation("G5IDETAIL",{{ "G5I_CODLIN"	, "GZ1_LINHA"},{ 'G5I_VENDA', "1"},{'G5I_HIST','2'} },G5I->(IndexKey(1)))
oModel:SetRelation("GI4ADETAIL",{{ "GI4_LINHA"	, "G5I_CODLIN"} ,{ "GI4_LOCORI"	, "G5I_LOCALI"}, {'GI4_SENTID','1'},{'GI4_HIST','2'} },'GI4_ITEM' ) //Ida
oModel:SetRelation("GI4BDETAIL",{{ "GI4_LINHA"	, "G5I_CODLIN"} ,{ "GI4_LOCDES"	, "G5I_LOCALI"}, {'GI4_SENTID','2'},{'GI4_HIST','2'} },'GI4_ITEM' ) //Volta

oModel:GetModel("GI6DETAIL"):SetOptional(.T.)
oModel:GetModel("GI6DETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GI6DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("GI6DETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("GZ1DETAIL"):SetOptional(.T.)
oModel:GetModel("GZ1DETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GZ1DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("GZ1DETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("GZ0DETAIL"):SetOptional(.T.)
oModel:GetModel("GZ0DETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GZ0DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("GZ0DETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("GI4ADETAIL"):SetOptional(.T.)
oModel:GetModel("GI4ADETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GI4ADETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("GI4ADETAIL"):SetOnlyQuery(.T.)

oModel:GetModel("GI4BDETAIL"):SetOptional(.T.)
oModel:GetModel("GI4BDETAIL"):SetNoDeleteLine(.T.)
oModel:GetModel("GI4BDETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("GI4BDETAIL"):SetOnlyQuery(.T.)

//Define as descricoes dos submodelos e modelo
oModel:SetDescription(STR0001)//"Assistente de Importacao/Exportacao DARUMA"
oModel:GetModel("MASTER"):SetDescription(STR0002)//"Assistente"
oModel:GetModel("GI6DETAIL"):SetDescription(STR0003)//"Lista de Agencias"

//Definicao da Chave unica
oModel:SetPrimaryKey({})

oModel:SetDeActivate( {|| GA036DDestroy() } )

Return(oModel)

/*/{Protheus.doc} ViewDef
@type function
@author jacom
@since 30/03/2017
@version 12.0
@return $oView, $View do modelo
@example
(examples)
@see (links_or_references)
/*/Static Function ViewDef()
Local oView		:= nil
Local oModel	:= FwLoadModel("GTPA036D")
Local oStrGI6	:= FWFormStruct( 2, "GI6",,.F. )
Local oStrGZ1	:= FWFormStruct( 2, "GZ1",,.F. )

//Define as estruturas dos submodelos no que diz respeito � View
GA036Struct(,oStrGI6,oStrGZ1,"V",oModel:GetModel("GI6DETAIL"):GetStruct())

GTPXRmvFld(oStrGI6,'GI6_FILIAL')
GTPXRmvFld(oStrGI6,'GI6_CODIGO')

GTPXRmvFld(oStrGZ1,'GZ1_FILIAL')
GTPXRmvFld(oStrGZ1,'GZ1_CODGI6')

// Cria o objeto de View
oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )

oView:AddGrid("VIEW_GI6", oStrGI6, 'GI6DETAIL')
oView:AddGrid("VIEW_GZ1", oStrGZ1, 'GZ1DETAIL')

// Divis�o Horizontal

oView:CreateHorizontalBox("GRIDGI6",50)
oView:CreateHorizontalBox("GRIDGZ1",50)

oView:SetOwnerView("VIEW_GI6", "GRIDGI6")
oView:SetOwnerView("VIEW_GZ1", "GRIDGZ1")

//Habitila os t�tulos dos modelos para serem apresentados na tela
oView:EnableTitleView("VIEW_GI6")

Return(oView)

/*/{Protheus.doc} GA036Struct
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@param oStrCab, objeto, Estrutura do cabe�alho 
@param oStrGI6, objeto, Estrutura da tabela GI6
@param oStrGZ1, objeto, Estrutura da tabela GZ1
@param cTipo, character, Qual o Tipo (M = Model/ V = Views)
@param oStrMdlGrd, objeto, Estrutura da query
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function GA036Struct(oStrCab,oStrGI6,oStrGZ1,cTipo,oStrMdlGrd)

Local aFldsGI6	:= {}
Local nI			:= 0
Local cAliasTab	:= GA036AliasTable()
Local aAux			:= {}

If ( cTipo == "M" ) //Estrutura do Modelo de dados
    
    oStrCab:AddTable(oG036Table:GetAlias(), , STR0004, {|| oG036Table:GetRealName() })//"Estrutura Fake do Cabe�alho"
    
    //Estrutura do Cabe�alho
    oStrCab:AddField(	STR0005,;									// 	[01]  C   Titulo do campo//"C�digo GI6"
				 		STR0006,;									// 	[02]  C   ToolTip do campo//"C�digo GI6"
				 		"XXX_CODGI6",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		6,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		
   
    //Estrutura do Grid
	oStrGI6:AddField(	"",;									// 	[01]  C   Titulo do campo
				 		"",;									// 	[02]  C   ToolTip do campo
				 		"GI6_MARK",;							// 	[03]  C   Id do Field
				 		"L",;									// 	[04]  C   Tipo do campo
				 		1,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual	

    oStrGI6:AddField(	STR0007,;									// 	[01]  C   Titulo do campo//"Arq. P�s-Venda"
				 		STR0008,;									// 	[02]  C   ToolTip do campo//"Diret�rio dos arquivos P�s-Vendas"
				 		"POS_VENDA",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQPVE")[1],;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual	                         
    
    oStrGI6:AddField(	STR0009,;									// 	[01]  C   Titulo do campo//"Arq. Sem Formato"
				 		STR0010,;									// 	[02]  C   ToolTip do campo//"Diret�rio dos arquivos sem formata��es"
				 		"NO_FORMAT",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQNFO")[1],;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
    
    oStrGI6:AddField(	STR0011,;							// 	[01]  C   Titulo do campo//"Arq. Flash"
				 		STR0012,;		// 	[02]  C   ToolTip do campo//"Diret�rio dos arquivos Flash"
				 		"ARQ_FLASH",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ0_ARQFLA")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual    


	oStrGI6:SetProperty("*",MODEL_FIELD_OBRIGAT,.F.)
	oStrGI6:SetProperty("*",MODEL_FIELD_VALID,{|| .T.})
	
	//Estrutura do Grid
	oStrGZ1:AddField(	"",;									// 	[01]  C   Titulo do campo
				 		"",;									// 	[02]  C   ToolTip do campo
				 		"GZ1_MARK",;							// 	[03]  C   Id do Field
				 		"L",;									// 	[04]  C   Tipo do campo
				 		1,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		{||GA036DVld() },;						// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual	

	oStrGZ1:AddField(	STR0027,;									// 	[01]  C   Titulo do campo "Sentido"
				 		"",;									// 	[02]  C   ToolTip do campo
				 		"GZ1_SENTID",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		1,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		{STR0028/*'1=Ida'*/,STR0029/*'2=Volta'*/,STR0030/*'3=Ambos'*/},;		//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		{|| "1"},;								//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.F.)									// 	[14]  L   Indica se o campo � virtual	

aAux := FwStruTrigger( 'GI6_MARK', 'GI6_MARK', 'GTPA036MARK(1)', .F.,nil,nil,nil,"!FwIsInCallStack('GTPA036MARK')" )
oStrGI6:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger( 'GZ1_MARK', 'GZ1_MARK', 'GTPA036MARK(2)', .F.,nil,nil,nil,"!FwIsInCallStack('GTPA036MARK')" )
oStrGZ1:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

Else    //Estrutura da View
 	oStrGZ1:SetProperty('*',MVC_VIEW_CANCHANGE,.F.)
	
 	// Composi��o da Estrutura do Grid
	oStrGI6:AddField(	"GI6_MARK",;				// [01]  C   Nome do Campo
						"01",;						// [02]  C   Ordem
						"",;						// [03]  C   Titulo do campo
						"",;						// [04]  C   Descricao do campo
						{STR0013},;			// [05]  A   Array com Help//"Selecionar"
						"CHECK",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    oStrGI6:AddField(	"POS_VENDA",;				// [01]  C   Nome do Campo
						"04",;						// [02]  C   Ordem
						STR0014,;						// [03]  C   Titulo do campo//"Arq. Pos-Venda"
						STR0015,;						// [04]  C   Descricao do campo//"Diretorio dos arquivos Pos-Vendas"
						{STR0016},;			// [05]  A   Array com Help//"Diretorio dos arquivos Pos-Vendas"
						"GET",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
    
    oStrGI6:AddField(	"NO_FORMAT",;				// [01]  C   Nome do Campo
						"05",;						// [02]  C   Ordem
						STR0017,;						// [03]  C   Titulo do campo//"Arq. Sem Formato"
						STR0018,;						// [04]  C   Descricao do campo//"Diretorio dos arquivos sem formatacoes"
						{STR0019},;			// [05]  A   Array com Help//"Diretorio dos arquivos sem formatacoes"
						"GET",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
                        
    oStrGI6:AddField("ARQ_FLASH",;				// [01]  C   Nome do Campo
						"06",;						// [02]  C   Ordem
						STR0020,;						// [03]  C   Titulo do campo//"Arq. Flash"
						STR0021,;						// [04]  C   Descricao do campo//"Diretorio dos arquivos Flash"
						{STR0022},;			// [05]  A   Array com Help//"Diretorio dos arquivos Flash"
						"GET",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.F.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
	
	oStrGZ1:AddField(	"GZ1_MARK",;				// [01]  C   Nome do Campo
						"01",;						// [02]  C   Ordem
						"",;						// [03]  C   Titulo do campo
						STR0023,;						// [04]  C   Descricao do campo
						{STR0023},;			// [05]  A   Array com Help//"Selecionar"
						"CHECK",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

	oStrGZ1:AddField(	"GZ1_SENTID",;				// [01]  C   Nome do Campo
						"99",;						// [02]  C   Ordem
						STR0027,;						// [03]  C   Titulo do campo "Sentido"
						STR0027,;						// [04]  C   Descricao do campo
						{STR0024},;			// [05]  A   Array com Help//"Sentido a ser selecionado"
						"COMBO",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						{STR0028/*'1=Ida'*/,STR0029/*'2=Volta'*/,STR0030/*'3=Ambos'*/},;		//	[09]  A   Lista de valores permitido do campo
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.F.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    
    If ( ValType(oStrMdlGrd) == "O" )                    
    
        aFldsGI6 := oStrMdlGrd:GetFields()                    
        
        For nI := 1 to Len(aFldsGI6)
            
            If ( (cAliasTab)->(FieldPos(aFldsGI6[nI,3])) == 0 )
                oStrGI6:RemoveField(aFldsGI6[nI,3])
            Else

                If ( aFldsGI6[nI,3] == "GI6_DESCRI" )
                    oStrGI6:SetProperty(aFldsGI6[nI,3],MVC_VIEW_CANCHANGE,.f.)
                EndIf

            EndIf

        Next nI

    EndIf

EndIf

Return()

/*/{Protheus.doc} GA036Table

@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036Table()

Local aStruct	:= {}
	
AAdd(aStruct,{"XXX_FAKE","C",1,0})

oG036Table := FWTemporaryTable():New()

oG036Table:SetFields(aStruct)
oG036Table:AddIndex("index1", {"XXX_FAKE"})
oG036Table:Create()

Return()

/*/{Protheus.doc} GA036DDestroy
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GA036DDestroy()

If ( !FwIsInCallStack('FwAlertExitPage') )
	
	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ( ValType(oG036Table) == "O" )
		oG036Table:Delete()
		FreeObj(oG036Table)
	EndIf

EndIf

Return()

/*/{Protheus.doc} GA036Load
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 1.0
@param oGrid, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036Load(oGrid)

Local aReg  := {}
Local aFlds := {}
Local aAux  := {}

Local cAliasTab := GA036AliasTable()

Local nI    := 0

aFlds := oGrid:GetStruct():GetFields()

While ( (cAliasTab)->(!Eof()) )
			
    For nI := 1 to Len(aFlds)

        If ( (cAliasTab)->(FieldPos(aFlds[nI,3])) > 0 )
            aAdd(aAux,(cAliasTab)->&(aFlds[nI,3]))
        Else
            aAdd(aAux,GTPCastType(,aFlds[nI,4]))
        EndIf

    Next nI
    
    aAdd(aReg,{(cAliasTab)->GI6_RECNO,aClone(aAux)})
    aAux := {}
    
    (cAliasTab)->(DbSkip())
    
EndDo

Return(aReg)

/*/{Protheus.doc} GTPA036MARK
Fun��o do gatilho de preenchimento do Campo de marca��o
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 1.0
@param nTipo, num�rico, Informa se o mark � da tabela de agencia ou de linha
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GTPA036MARK(nTipo)

Local lRet	:= .F.
Local oModel	:= FwModelActive()
Local oMdlGI6	:= oModel:GetModel('GI6DETAIL')
Local oMdlGZ1	:= oModel:GetModel('GZ1DETAIL')
Local oView	:= FwViewActive()
Local n1		:= 0
Local nLine	:= oMdlGZ1:GetLine() 

If nTipo == 1//Veio da Agencia
	For	n1 := 1 to oMdlGZ1:Length()
		If !oMdlGZ1:IsDeleted(n1)
			oMdlGZ1:GoLine(n1)
			oMdlGZ1:SetValue('GZ1_MARK',oMdlGI6:GetValue('GI6_MARK'))
		Endif
	Next 
Else
	If oMdlGZ1:GetValue('GZ1_MARK') .and. !oMdlGI6:GetValue('GI6_MARK')
		oMdlGI6:SetValue('GI6_MARK',.T.)
	ElseIf !oMdlGZ1:GetValue('GZ1_MARK')
		If !oMdlGZ1:SeekLine({{'GZ1_MARK',.T.}})
			oMdlGI6:SetValue('GI6_MARK',.F.)
		Endif	
	Endif
Endif

oMdlGZ1:GoLine(nLine)
oView:Refresh()

Return lRet

/*/{Protheus.doc} GA036Flash
Fun��o de grava��o do arquivo flash
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@param oView, objeto, (Descri��o do par�metro)
@param nModelo, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GA036Flash(oView,nModelo)
Local oModel			:= oView:GetModel()
Local oMdlGI6			:= oModel:GetModel('GI6DETAIL')
Local oMdlGZ1			:= oModel:GetModel('GZ1DETAIL')
Local n1,n2,nLin		:= 0
Local aDados			:= {}

Static cDesLinha:= "[DESC_LINHA]" +CHR(13)+CHR(10)
Static cCidades := ""
Static cCodCity := ""
Static cEstados := ""
Static cTarifas := ""
Static cSeguros := ""
Static cPedagios:= ""

For n1 := 1 to oMdlGI6:Length()
	If oMdlGI6:GetValue('GI6_MARK',n1) //Seleciono a Agencia
		oMdlGI6:GoLine(n1)
		aDados		:= {}
		cDesLinha:= "[DESC_LINHA]" +CHR(13)+CHR(10)
		cCidades := ""
		cCodCity := ""
		cEstados := ""
		cTarifas := ""
		cSeguros := ""
		cPedagios:= ""
		For n2 := 1 to oMdlGZ1:Length()
			If oMdlGZ1:GetValue('GZ1_MARK',n2) //Busco as Informa��es das linhas
				oMdlGZ1:GoLine(n2)
				If oMdlGZ1:GetValue('GZ1_SENTID') == '3' .or. oMdlGZ1:GetValue('GZ1_SENTID')  == '1' // Se ambos ou Ida
					ImpLinha(oModel,aDados,'1',@nLin)
				Endif
				If oMdlGZ1:GetValue('GZ1_SENTID') == '3' .or. oMdlGZ1:GetValue('GZ1_SENTID')  == '2' // Se ambos ou Volta
					ImpLinha(oModel,aDados,'2',@nLin)
				Endif
				
			Endif
		Next
		aAdd(aDados,cDesLinha )
		aAdd(aDados,"[DESC_ADICIONAL]"+ Chr(13)+ Chr(10) )
		aAdd(aDados,cCidades)
		aAdd(aDados,cEstados)
		aAdd(aDados,cTarifas)
		aAdd(aDados,cSeguros)
		aAdd(aDados,cPedagios)
		aAdd(aDados,"[PRODUTO_EXTRA]"+ Chr(13)+ Chr(10) )
		aAdd(aDados,cCodCity)
		
		If nModelo == 2
			aAdd(aDados,"[CATEGORIA_TIPO_PASSAGEM]")
			aAdd(aDados,"1"+ Chr(13)+Chr(10))
			
			aAdd(aDados,"[CATEGORIA_MSG_DISPLAY]")
			aAdd(aDados,'"NORMAL"'+ Chr(13)+Chr(10))
			
			aAdd(aDados,"[CATEGORIA_MSG_IMPRESSA]")
			aAdd(aDados,'" "'+ Chr(13)+Chr(10))
			
			aAdd(aDados, "[CATEGORIA_%_TARIFA_CHEIA]")
			aAdd(aDados, "10000"+ Chr(13)+Chr(10))
			
			aAdd(aDados, "[CATEGORIA_INSTRUMENTO_LEGAL]")
			aAdd(aDados,'" "')
		Endif 

		GA036DARQ(aDados,oMdlGI6:GetValue('ARQ_FLASH'), oMdlGI6:GetValue('GI6_CODIGO'))
	Endif
Next

Return()

/*/{Protheus.doc} ImpLinha
@type function
@author jacomo.fernandes
@since 28/03/2017
@version 1.0
@param oModel, objeto, Modelo de dados
@param aDados, array, Array contendo os dados a serem transcritos para o arquivos
@param cSentido, character, Informa se � Ida ou Voltas
@param nLin, num�rico, Numero da Linha que ser� gerada
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ImpLinha(oModel,aDados,cSentido,nLin)
Local oMdlG5I	:= oModel:GetModel('G5IDETAIL')
Local oMdlGZ1	:= oModel:GetModel('GZ1DETAIL')
Local oMdlGI4	:= NIL
Local cOldUF			:= ""
Local cNewUF			:= ""
Local nUF				:= 0
Local n3,n4

Local aStrTran	:= { {'(','- '} , {')',' '}, {'[','- '} , {']',' '} , {'{','- '} , {'}',' '}  , {'  ',' '} }

nLin++

cCidades += "[CIDADES_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)
cCodCity += "[COD_CIDADES_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)
cEstados += "[COD_ESTADOS_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)

If cSentido == '1'
	cLocOri	:= Posicione('GI2',1,xFilial('GI2')+oMdlGZ1:GetValue('GZ1_LINHA'),"GI2_LOCINI") 
	cLocDest	:= Posicione('GI2',1,xFilial('GI2')+oMdlGZ1:GetValue('GZ1_LINHA'),"GI2_LOCFIM")
	oMdlGI4	:= oModel:GetModel('GI4ADETAIL')	
Else
	cLocOri	:= Posicione('GI2',1,xFilial('GI2')+oMdlGZ1:GetValue('GZ1_LINHA'),"GI2_LOCFIM") 
	cLocDest	:= Posicione('GI2',1,xFilial('GI2')+oMdlGZ1:GetValue('GZ1_LINHA'),"GI2_LOCINI")
	oMdlGI4	:= oModel:GetModel('GI4BDETAIL')
	
	For n3 := oMdlG5I:Length() to 1 Step -1 //Pego as informa��es das se��es 
		oMdlG5I:GoLine(n3)
		cCidades	+= '"'+AllTrim(oMdlG5I:GetValue('G5I_DESLOC'))+'",'
		cCodCity	+= AllTrim(oMdlG5I:GetValue('G5I_LOCALI'))+','
		cNewUF := Posicione('GI1',1,xFilial('GI1')+oMdlG5I:GetValue('G5I_LOCALI'),"GI1_UF") 
		If Empty(cOldUF) .or. cOldUF <> cNewUF
			cOldUF	:= cNewUf
			IF nUF <= 3 
				nUF++
			Endif  
		Endif
		cEstados += cValToChar(nUF)+',' 
	Next
Endif

cDesOri		:= AllTrim(Posicione('GI1',1,xFilial('GI1')+cLocOri,"GI1_DESCRI"))
cDesOri		:= GTPRmvChar(cDesOri	,aStrTran)

cDesDest	:= Alltrim(Posicione('GI1',1,xFilial('GI1')+cLocDest,"GI1_DESCRI"))
cDesDest	:= GTPRmvChar(cDesDest	,aStrTran)

cDesLinha	+= '"'+oMdlGZ1:GetValue('GZ1_LINHA')+" "+cDesOri + " x "+ cDesDest+'"'+Chr(13)+Chr(10)

For n3 := 1 to oMdlG5I:Length() //Pego as informa��es das se��es
	oMdlG5I:GoLine(n3)				
	If n3 == 1
		cTarifas += "[TARIFAS_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)
		cSeguros += "[SEGUROS_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)
		cPedagios+= "[PEDAGIOS_L"+cValToChar(nLin)+"]"+CHR(13)+CHR(10)
	Endif
	If cSentido == "1"
		cCidades	+= '"'+AllTrim(oMdlG5I:GetValue('G5I_DESLOC'))+'",'
		cCodCity	+= AllTrim(oMdlG5I:GetValue('G5I_LOCALI'))+','
		cNewUF := Posicione('GI1',1,xFilial('GI1')+oMdlG5I:GetValue('G5I_LOCALI'),"GI1_UF") 
		If Empty(cOldUF) .or. cOldUF <> cNewUF
			cOldUF	:= cNewUf
			IF nUF <= 3 
				nUF++
			Endif  
		Endif
		cEstados += cValToChar(nUF)+',' 
	Endif
	For n4 := 1 to oMdlGI4:Length()
		oMdlGI4:GoLine(n4)
		If !oMdlGI4:IsEmpty()
			cTarifas += ValPadrao(oMdlGI4,oMdlGI4:GetValue('GI4_TAR'	),.T.) +','
			cSeguros += ValPadrao(oMdlGI4,oMdlGI4:GetValue('GI4_SGFACU'	),.F.) +','
			cPedagios+= ValPadrao(oMdlGI4,oMdlGI4:GetValue('GI4_PED'	),.F.) +','
			If n4 == oMdlGI4:Length()
				cTarifas := SubStr(cTarifas,1,Len(cTarifas)-1) +CHR(13)+CHR(10)
				cSeguros := SubStr(cSeguros,1,Len(cSeguros)-1)+CHR(13)+CHR(10)
				cPedagios:= SubStr(cPedagios,1,Len(cPedagios)-1)+CHR(13)+CHR(10)
			Endif
		Endif
	Next
Next
cTarifas	+= CHR(13)+ CHR(10)
cSeguros	+= CHR(13)+ CHR(10)
cPedagios	+= CHR(13)+ CHR(10)

cCidades := SubStr(cCidades,1,Len(cCidades)-1)+CHR(13)+CHR(10)+ Chr(13)+Chr(10)
cCodCity := SubStr(cCodCity,1,Len(cCodCity)-1)+CHR(13)+CHR(10)+ Chr(13)+Chr(10)
cEstados := SubStr(cEstados,1,Len(cEstados)-1)+CHR(13)+CHR(10) + Chr(13)+Chr(10)

GTPDestroy(aStrTran)

Return

/*/{Protheus.doc} GA036DARQ
Fun��o de cria��o do Arquivo
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 12.0
@param aDados, array, Array com dados a serem gravados
@param cDir, character, nome do diret�rio que ser� gravado
@param cName, character, nome do arquivo que ser� gerado
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036DARQ(aDados,cDir, cName)

Local n1			:= 0
Local cNomeArq	:= cName+"_"+DtoS(Date())+"_"+StrTran(Time(),":","")+".txt"
Local oFWriter	:= nil 

cDir := AllTrim(cDir)

IF !File(cDir)
	FwMakeDir(cDir)
Endif
If Right(cDir,1) <> "\"
	cDir+="\"
Endif
oFWriter := FWFileWriter():New( cDir+cNomeArq ,.t.)

If oFWriter:Create()
	For n1 := 1 to Len(aDados)
		If !oFWriter:Write( aDados[n1] + Chr(13) + Chr(10))
			MsgInfo(oFWriter:Error():Message)
			Exit
		Endif
	Next
Else
	MsgInfo(oFWriter:Error():Message)
	Return
EndIf
oFWriter:Close()

Return

/*/{Protheus.doc} ValPadrao
Fun��o para devolver o valor correto no arquivo 
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 1.0
@param oMdlGI4, objeto, modelo de dados
@param nVal, num�rico, valor buscado do modelo
@return $nRet, ${Valor a ser gravado}
@example
(examples)
@see (links_or_references)
/*/
Static Function ValPadrao(oMdlGI4,nVal,lTarifa)

Local nRet		:= 0
Local cRet		:= ""
Local oModel	:= oMdlGI4:GetModel()
Local oMdlG5I	:= oModel:GetModel('G5IDETAIL')
Local nLinG5I	:= oMdlG5I:GetLine() 
Local nLinGI4	:= oMdlGI4:GetLine() 
Local nValPad	:= Round(oModel:GetModel('GZ0DETAIL'):GetValue('GZ0_VLCRGA'),0)

Default lTarifa := .T.

nVal	:= nVal

If oMdlGI4:GetValue('GI4_MSBLQL')  == '1'
	nVal := 0
Endif

If nVal <> 0 
	nRet := nVal
Else
	If !lTarifa
		nRet := 0
	ElseIf nLinG5I == 1 .Or. nLinG5I == oMdlG5I:Length() //Se for a Primeira ou a Ultima linha da Se��o 
		nRet := nValPad
	ElseIf nLinGI4	== 1  .Or. nLinGI4 == oMdlGI4:Length()
		nRet := nValPad
	Endif
Endif
cRet := RetPict(nRet)
Return cRet 

/*/{Protheus.doc} RetPict
Retorna o Valor na Picture necess�ria
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 1.0
@param nVal, num�rico, Valor a ser convertido em String
@return ${cRet}, ${Valor convertido em string}
@example
(examples)
@see (links_or_references)
/*/
Static Function RetPict(nVal)

Local cRet	:= ""

If nVal == 0
	cRet	:= "0"
ElseIF nVal >= 655.35
	cRet	:= "65535"
Else
	cRet := Alltrim(Transf(nVal,"@E 9999999.99"))
	cRet := StrTran(cRet, ",","")
Endif

Return cRet

/*/{Protheus.doc} GA036DVld
Valida��o do campo de Marcado
@type function
@author jacomo.fernandes
@since 30/03/2017
@version 1.0
@return $lRet, $Se Verdadeiro, pode ser marcado
@example
(examples)
@see (links_or_references)
/*/
Static Function GA036DVld()

Local lRet		:= .T.
Local oModel	:= FwModelActive()
Local cLinha	:= oModel:GetModel('GZ1DETAIL'):GetValue('GZ1_LINHA')

If oModel:GetModel('GZ1DETAIL'):GetValue('GZ1_MARK')
	lRet := Posicione('GI2',1,xFilial('GI2')+cLinha, 'GI2_MSBLQL') == '2'
Endif

If !lRet .And. !FwIsInCallStack('GTPA036MARK')
	oModel:SetErrorMessage('GZ1_ITENS','GZ1_MARK','GZ1_ITENS','GZ1_MARK','REQBLOQ',STR0025,STR0026)//'Linha selecionada se encontra bloqueada'##'Verifique o cadastros de linhas'
Endif

Return lRet
