#include "GTPA036F.CH"
#Include 'Protheus.ch'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TBICONN.CH'

Static aG036Filter		:= {} //Array com regras de filtro de bilhetes

/*/{Protheus.doc} ModelDef
    MVC do programa que parametriza a inclus�o de servi�os atrav�s de range de sele��o.
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 17/04/2017
    @version 1
    @param 
    @return oModel, objeto, inst�ncia da classe FwFormModel
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ModelDef()

Local oStruct	:= FWFormModelStruct():New()

//Define as estruturas dos submodelos no que diz respeito ao Modelo de Dados
G036FStruct(oStruct,"M")

oModel := FWFormModel():New("GTPA036F",{|| .t.},{|| .t.},{|oModel| G036FCommit(oModel) },{|| .t.})

oModel:AddFields("MASTER", /*cOwner*/, oStruct,,,{|oModel| G036FLoad(oModel)})

//Define as descri��es dos submodelos e modelo
oModel:SetDescription(STR0001)	//"Informar Servi�os"
oModel:GetModel("MASTER"):SetDescription(STR0002) //"Dados"

//Defini��o da Chave �nica
oModel:SetPrimaryKey({})
oModel:SetActivate({|oModel| oModel:GetModel("MASTER"):LoadValue("TMP_CODSRV","") })

Return(oModel)

/*/{Protheus.doc} ViewDef
    Fun��o que define a View para o MVC que define servi�o para um range de registros,
    escolhido pelo usu�rio
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 17/04/2017
    @version 1
    @param 
    @return oView, objeto, inst�ncia da Classe FWFormView
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function ViewDef()

Local oView		:= nil
Local oModel	:= FwLoadModel("GTPA036F")
Local oStruct   := FWFormViewStruct():New()

//Define as estruturas dos submodelos no que diz respeito � View
G036FStruct(oStruct,"V")

// Cria o objeto de View
oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado
oView:SetModel( oModel )

oView:AddField("VIEW_CAB", oStruct, "MASTER" )

// Divis�o Horizontal
oView:CreateHorizontalBox("HEADER",100)

oView:SetOwnerView("VIEW_CAB", "HEADER")

Return(oView)

/*/{Protheus.doc} G036FStruct
    Fun��o respons�vel por criar a estrutura dos submodelos, tanto para o model quanto view,
	utilizados pelo MVC. 
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 11/04/2017
    @version 1
    @param 	oStrCab, objeto, inst�ncia da Classe FWFormModelStruct ou FWFormViewStruct (CABE�ALHO)
			cTipo, caractere, Tipo de Estrutura que ser� criada - "M" - model; "V" - View			
    @return nil, nulo, Sem retorno
    @example
    (examples)
    @see (links_or_references)
/*/
Static Function G036FStruct(oStruct,cTipo)

//Define os blocos de execu��o com as regras de Filtro
aAdd(aG036Filter,{"TMP_ECFDE","GIC_ECFSEQ",{|a,b| a >= b } })
aAdd(aG036Filter,{"TMP_ECFATE","GIC_ECFSEQ",{|a,b,c| c := Iif(!Empty(b),b,Replicate("Z",TamSx3("GZ2_CODIGO")[1])), a <= c } })
aAdd(aG036Filter,{"TMP_DATADE","GIC_DTVIAG",{|a,b| a >= b } })
aAdd(aG036Filter,{"TMP_DATATE","GIC_DTVIAG",{|a,b,c| c := Iif(!Empty(b),b,dDataBase), a <= c } })
aAdd(aG036Filter,{"TMP_COLDE","GIC_COLAB",{|a,b| a >= b } })
aAdd(aG036Filter,{"TMP_COLATE","GIC_COLAB",{|a,b,c| c := Iif(!Empty(b),b,Replicate("Z",TamSx3("GYG_CODIGO")[1])), a <= c } })
aAdd(aG036Filter,{"TMP_BILDE","GIC_BILHET",{|a,b| a >= b } })
aAdd(aG036Filter,{"TMP_BILATE","GIC_BILHET",{|a,b,c| c := Iif(!Empty(b),b,Replicate("Z",TamSx3("GIC_BILHET")[1])), a <= c } })

If ( cTipo == "M" ) //Estrutura do Modelo de dados
    
    oStruct:AddTable("TMPFIL",{},STR0003)//"Tabela para Filtro"
    
    //Estrutura do Cabe�alho    
    oStruct:AddField(	STR0004,;								// 	[01]  C   Titulo do campo	//"Servi�o"
				 		STR0004,;								// 	[02]  C   ToolTip do campo 	//"Servi�o"
				 		"TMP_CODSRV",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GYN_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		{|oSubMdl| ExistCpo("GYN",oSubMdl:GetValue("TMP_CODSRV")) },;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.f.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		
	
    oStruct:AddField(	STR0005,;							// 	[01]  C   Titulo do campo 	//"Nro. ECF de"
				 		STR0005,;							// 	[02]  C   ToolTip do campo	//"Nro. ECF de"
				 		"TMP_ECFDE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ2_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		
	
    oStruct:AddField(	STR0006,;								// 	[01]  C   Titulo do campo	//"Nro. ECF Ate"
				 		STR0006,;								// 	[02]  C   ToolTip do campo 	//"Nro. ECF Ate"
				 		"TMP_ECFATE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GZ2_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual			 		

	oStruct:AddField(	STR0007,;								// 	[01]  C   Titulo do campo 	//"Dt. Viagem de"
				 		STR0007,;								// 	[02]  C   ToolTip do campo 	//"Dt. Viagem de"
				 		"TMP_DATADE",;							// 	[03]  C   Id do Field
				 		"D",;									// 	[04]  C   Tipo do campo
				 		8,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
				 		
    oStruct:AddField(	STR0008,;									// 	[01]  C   Titulo do campo	//"Dt. Viagem Ate"
				 		STR0008,;									// 	[02]  C   ToolTip do campo	//"Dt. Viagem Ate"
				 		"TMP_DATATE",;							// 	[03]  C   Id do Field
				 		"D",;									// 	[04]  C   Tipo do campo
				 		8,;										// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual

	oStruct:AddField(	STR0009,;								// 	[01]  C   Titulo do campo	//"Colaborador de"
				 		STR0009,;								// 	[02]  C   ToolTip do campo 	//"Colaborador de"
				 		"TMP_COLDE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GYG_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
	
	oStruct:AddField(	STR0010,;								// 	[01]  C   Titulo do campo	//"Colaborador ate"
				 		STR0010,;								// 	[02]  C   ToolTip do campo 	//"Colaborador ate"
				 		"TMP_COLATE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GYG_CODIGO")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
	
	oStruct:AddField(	STR0011,;								// 	[01]  C   Titulo do campo	//"Bilhete de"
				 		STR0011,;								// 	[02]  C   ToolTip do campo 	//"Bilhete de"
				 		"TMP_BILDE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GIC_BILHET")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
	
	oStruct:AddField(	STR0012,;								// 	[01]  C   Titulo do campo	//"Bilhete ate"
				 		STR0012,;								// 	[02]  C   ToolTip do campo 	//"Bilhete ate"
				 		"TMP_BILATE",;							// 	[03]  C   Id do Field
				 		"C",;									// 	[04]  C   Tipo do campo
				 		TamSx3("GIC_BILHET")[1],;				// 	[05]  N   Tamanho do campo
				 		0,;										// 	[06]  N   Decimal do campo
				 		Nil,;									// 	[07]  B   Code-block de valida��o do campo
				 		Nil,;									// 	[08]  B   Code-block de valida��o When do campo
				 		Nil,;									//	[09]  A   Lista de valores permitido do campo
				 		.F.,;									//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 		Nil,;									//	[11]  B   Code-block de inicializacao do campo
				 		.F.,;									//	[12]  L   Indica se trata-se de um campo chave
				 		.F.,;									//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 		.T.)									// 	[14]  L   Indica se o campo � virtual
				 		
Else    //Estrutura da View
					
    //Composi��o da Estrutura do Cabe�alho
    oStruct:AddField(	"TMP_CODSRV",;				// [01]  C   Nome do Campo
						"01",;						// [02]  C   Ordem
						STR0004,;					// [03]  C   Titulo do campo	//"Servi�o"
						STR0004,;					// [04]  C   Descricao do campo //"Servi�o"
						{STR0004},;	    		    // [05]  A   Array com Help 	//"Servi�o"
						"GET",;				    	// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"GYN",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
					 		
    oStruct:AddField(	"TMP_ECFDE",;				// [01]  C   Nome do Campo
						"02",;						// [02]  C   Ordem
						STR0005,;					// [03]  C   Titulo do campo 	//"Nro. ECF de"
						STR0005,;					// [04]  C   Descricao do campo //"Nro. ECF de"
						{STR0005},;	    		    // [05]  A   Array com Help 	//"Nro. ECF de"
						"GET",;				    	// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    oStruct:AddField(	"TMP_ECFATE",;				// [01]  C   Nome do Campo
						"03",;						// [02]  C   Ordem
						STR0006,;					// [03]  C   Titulo do campo 	//"Nro. ECF Ate"
						STR0006,;					// [04]  C   Descricao do campo //"Nro. ECF Ate"
						{STR0006},;		    		// [05]  A   Array com Help 	//"Nro. ECF Ate"
						"GET",;				    	// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
    
	oStruct:AddField(	"TMP_DATADE",;				// [01]  C   Nome do Campo
						"04",;						// [02]  C   Ordem
						STR0007,;					// [03]  C   Titulo do campo	//"Dt. Viagem de"
						STR0007,;					// [04]  C   Descricao do campo	//"Dt. Viagem de"
						{STR0007},;					// [05]  A   Array com Help 	//"Dt. Viagem de"
						"GET",;					// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo

    oStruct:AddField(	"TMP_DATATE",;				// [01]  C   Nome do Campo
						"05",;						// [02]  C   Ordem
						STR0008,;					// [03]  C   Titulo do campo	//"Dt. Viagem Ate"
						STR0008,;					// [04]  C   Descricao do campo //"Dt. Viagem Ate"
						{STR0008},;					// [05]  A   Array com Help 	//"Dt. Viagem Ate"
						"GET",;						// [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
    
    oStruct:AddField(	"TMP_COLDE",;				// [01]  C   Nome do Campo
						"06",;						// [02]  C   Ordem
						STR0009,;					// [03]  C   Titulo do campo			//"Colaborador de"
						STR0009,;					// [04]  C   Descricao do campo 		//"Colaborador de"
						{STR0009},;			        // [05]  A   Array com Help 			//"Colaborador de"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
                        
    oStruct:AddField(	"TMP_COLATE",;				// [01]  C   Nome do Campo
						"07",;						// [02]  C   Ordem
						STR0010,;			    	// [03]  C   Titulo do campo 	//"Colaborador ate"
						STR0010,;				    // [04]  C   Descricao do campo //"Colaborador ate"
						{STR0010},;			        // [05]  A   Array com Help		//"Colaborador ate"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo
	
	oStruct:AddField(	"TMP_BILDE",;				// [01]  C   Nome do Campo
						"08",;						// [02]  C   Ordem
						STR0011,;				    // [03]  C   Titulo do campo 	//"Bilhete de"
						STR0011,;				    // [04]  C   Descricao do campo //"Bilhete de"
						{STR0011},;			        // [05]  A   Array com Help 	//"Bilhete de"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo                     
                        
    oStruct:AddField(	"TMP_BILATE",;				// [01]  C   Nome do Campo
						"09",;						// [02]  C   Ordem
						STR0012,;			    	// [03]  C   Titulo do campo	//"Bilhete ate"
						STR0012,;				// [04]  C   Descricao do campo //"Bilhete ate"
						{STR0012},;			// [05]  A   Array com Help 	//"Bilhete ate"
						"GET",;					    // [06]  C   Tipo do campo
						"",;						// [07]  C   Picture
						NIL,;						// [08]  B   Bloco de Picture Var
						"",;						// [09]  C   Consulta F3
						.T.,;						// [10]  L   Indica se o campo � alteravel
						NIL,;						// [11]  C   Pasta do campo
						"",;						// [12]  C   Agrupamento do campo
						NIL,;						// [13]  A   Lista de valores permitido do campo (Combo)
						NIL,;						// [14]  N   Tamanho maximo da maior op��o do combo
						NIL,;						// [15]  C   Inicializador de Browse
						.T.,;						// [16]  L   Indica se o campo � virtual
						NIL,;						// [17]  C   Picture Variavel
						.F.)						// [18]  L   Indica pulo de linha ap�s o campo					
						
	oStruct:AddGroup( 'SERVICO', STR0014, '', 2 )//'Inserir Nro. Servi�o'
	oStruct:AddGroup( 'FILTRO', STR0015, '', 2 )//'Filtro de Dados'
    
    //Agrupa todos os campos em 'FILTRO'
    oStruct:SetProperty("*", MVC_VIEW_GROUP_NUMBER, "FILTRO" )

    //Agrupa o campo abaixo em 'SERVICO'
    oStruct:SetProperty("TMP_CODSRV", MVC_VIEW_GROUP_NUMBER, "SERVICO" )

EndIf

Return()

/*/{Protheus.doc} G036FLoad
	Fun��o respons�vel pela carga dos submodelos do MVC.
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	oSub, objeto, inst�ncia da classe FWFormFieldsModel ou da classe FWFormGridModel
			cIdSub, caractere, id do submodelo que ser� carregado
			lOneGrid, l�gico, .t. quando se carrega somente um grid
	@return aRet, array, array com os dados a serem carregados no submodelo de cIdSub
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function G036FLoad(oSub)

Local nI		:= 0

Local aFldSub	:= {}
Local aRet 		:= {}
Local aAux		:= {}

aFldSub := oSub:GetStruct():GetFields()

aAux := Array(Len(aFldSub))

For nI := 1 to Len(aFldSub)
	
	If ( "DATA" $ Alltrim(aFldSub[nI,3]) )
		aAux[nI] := GTPCastType("",aFldSub[nI,4])
	Else
		aAux[nI] := GTPCastType(,aFldSub[nI,4])
	EndIf
	
	
Next nI

aAdd(aRet,aClone(aAux))
aAdd(aRet,1)

Return(aRet)

/*/{Protheus.doc} G036FCommit
	Fun��o respons�vel pelo Commit do MVC. Nesta fun��o, ser� alimentado os dados do MVC GTPA036E 
	@type  Static Function
	@author Fernando Radu Muscalu
	@since 27/03/2017
	@version 1
	@param	oSub, objeto, inst�ncia da classe FWFormFieldsModel ou da classe FWFormGridModel
			cIdSub, caractere, id do submodelo que ser� carregado
			lOneGrid, l�gico, .t. quando se carrega somente um grid
	@return aRet, array, array com os dados a serem carregados no submodelo de cIdSub
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function G036FCommit(oModel)

Local oVwServ	:= G036EGetView()
Local oMdlServ	:= oVwServ:GetModel()
Local lFirstReplace	:= .T.
Local nI		:= 0
Local nX		:= 0

For nI := 1 to oMdlServ:GetModel("GICDETAIL"):Length()
	
	oMdlServ:GetModel("GICDETAIL"):GoLine(nI)
	
	For nX := 1 to Len(aG036Filter)
		
		lRet := Eval(aG036Filter[nX,3],oMdlServ:GetModel("GICDETAIL"):GetValue(aG036Filter[nX,2]),oModel:GetModel("MASTER"):GetValue(aG036Filter[nX,1]))
		
		If lRet .and. !G036FValid(oModel:GetModel("MASTER"):GetValue("TMP_CODSRV"),oMdlServ,,.t.) .AND. ExistCpo('GYN',oModel:GetModel("MASTER"):GetValue("TMP_CODSRV"))
			If lFirstReplace
				lRet := FwAlertYesNo( STR0016, STR0017 ) // "Viagem informada n�o confere com os parametros de linha/data/hora. Deseja Continuar?" / "Aten��o!!"
				lFirstReplace := .F.
			Endif
		
		Endif
		
		If ( !lRet )
			Exit
		EndIf	
		
	Next nX
	
	If ( lRet )
		oMdlServ:GetModel("GICDETAIL"):LoadValue("GIC_CODSRV",oModel:GetModel("MASTER"):GetValue("TMP_CODSRV"))			
	EndIf
	
Next nI

oVwServ:Refresh()

Return(.t.)

/*/{Protheus.doc} G036FValid
    Fun��o que valida se o servi�o buscado se adere ao bilhete analisado. 
    @type  Static Function
    @author Fernando Radu Muscalu
    @since 19/04/2017
    @version 1
    @param	cServico, caractere, c�digo do servi�o buscado
    		oMdlServ, objeto, inst�ncia da Classe FWFormModel
    		nLin, num�rico, nro da linha do Grid 'GICDETAIL', cujo bilhete � feito a an�lise 
    @return lRet, l�gico, .t. - Servi�o adere ao bilhete (passagem)
    @example
    (examples)
    @see (links_or_references)
/*/
Function G036FValid(cServico,oMdlServ,nLin,lChkSrv)

Local cLinha 	:= ""
Local cLocOri	:= ""
Local cLocDes	:= ""
Local cHora		:= ""
Local dData		:= SToD("")
Local lRet		:= .f.
Local aArea		:= GYN->(GetArea())

Default nLin	:= oMdlServ:GetModel("GICDETAIL"):GetLine()	//Linha do Grid
Default lChkSrv	:= .f.										//Checa Servi�o (cadastro de Hor�rio) - .t. sim; .f. -n�o

cLinha 		:= oMdlServ:GetModel("GICDETAIL"):GetValue("GIC_LINHA",nLin)
cLocOri		:= oMdlServ:GetModel("GICDETAIL"):GetValue("GIC_LOCORI",nLin)
cLocDes		:= oMdlServ:GetModel("GICDETAIL"):GetValue("GIC_LOCDES",nLin)
dData		:= oMdlServ:GetModel("GICDETAIL"):GetValue("GIC_DTVIAG",nLin)
cHora		:= oMdlServ:GetModel("GICDETAIL"):GetValue("GIC_HORA",nLin)

If ( !lChkSrv )

	GYN->(DbSetOrder(1))	//GYN_FILIAL + GYN_CODIGO

	If ( GYN->(DbSeek(XFilial("GYN") + cServico )) )
		
		If ( GYN->GYN_LINCOD == cLinha .And. GYN->GYN_DTINI == dData .And.;
				GYN->GYN_HRINI == cHora )
			lRet := .t.
		EndIf

	EndIf
	RestArea(aArea)

Else

	lRet := ( cServico ==  GA419Viagem(cLinha,cLocOri,cLocDes,dData,cHora) )
	If !lRet  .And. !FwIsInCallStack('G036FCOMMIT')
		lRet := FwAlertYesNo(STR0016,STR0017) // "Viagem informada n�o confere com os parametros de linha/data/hora. Deseja Continuar?" / "Aten��o!!" 
	Endif
	
EndIf

Return(lRet)
