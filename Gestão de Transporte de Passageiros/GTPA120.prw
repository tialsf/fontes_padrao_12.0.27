#Include "GTPA120.ch"
#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPA120()
Cadastro de Ped�gio

@sample	GTPA120()

@return	oBrowse	Retorna o Cadastro de Ped�gio

@author	Gustavo Silva -  Inova��o
@since		02/01/2019
@version 1.0
/*/
//------------------------------------------------------------------------------------------

Function GTPA120()

Local oBrowse	

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G9T')
oBrowse:SetDescription(STR0001) //'Cadastro de Ped�gio'
oBrowse:Activate()
Return Nil

Static Function MenuDef()

Local aRotina	:= {}

ADD OPTION aRotina TITLE STR0002 ACTION 'VIEWDEF.GTPA120' OPERATION 2 ACCESS 0 // Visualizar //'Visualizar'
ADD OPTION aRotina TITLE STR0003   ACTION 'VIEWDEF.GTPA120' OPERATION 3 ACCESS 0 // Incluir //'Incluir'
ADD OPTION aRotina TITLE STR0004   ACTION 'VIEWDEF.GTPA120' OPERATION 4 ACCESS 0 // Alterar //'Alterar'
ADD OPTION aRotina TITLE STR0005   ACTION 'VIEWDEF.GTPA120' OPERATION 5 ACCESS 0 // Excluir //'Excluir'

Return ( aRotina )

Static Function ModelDef()

Local bPosValid := {|oModelF|PosValid(oModelF)}
Local oStruG9T := FWFormStruct( 1, 'G9T' )
Local oModel	:= nil
oModel := MPFormModel():New('GTPA120')

oModel:AddFields( 'G9TMASTER', /*cOwner*/, oStruG9T,,bPosValid )
oModel:SetDescription( STR0006 ) //'Modelo de Dados de Usu�rio'
oModel:GetModel( 'G9TMASTER' ):SetDescription( STR0001 ) //'Cadastro de Ped�gio'
oModel:SetPrimaryKey({"G9T_FILIAL","G9T_CODIGO"})

Return oModel

Static Function ViewDef()

Local oModel   := FWLoadModel( 'GTPA120' )
Local oStruG9T := FWFormStruct( 2, 'G9T' )
Local oView := FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEW_G9T', oStruG9T, 'G9TMASTER' )
oView:SetDescription(STR0001)  //'Cadastro de Ped�gio'
oView:CreateHorizontalBox( 'TELA' , 100 )
oView:SetOwnerView( 'VIEW_G9T', 'TELA' )

Return oView

/*/{Protheus.doc} PosValid(oModelF)
Fun��o respons�vel por validar se o registro j� existe e n�o permitir que o
local de origem e destino sejam os mesmos.
@type function
@author gustavo.silva2
@since 05/01/2019
@version 1.0
@param oMdl, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/

Static Function PosValid(oModelF)
Local oModel    := oModelF:GetModel()
Local oModelG9T	:= oModel:GetModel('G9TMASTER')
Local lRet		:= .T.
Local aArea     := GetArea()
Local aAreaG9T  := G9T->( GetArea() )
Local cCodOrig 	:= oModelG9T:GetValue('G9T_CODORI')
Local cCodDes 	:= oModelG9T:GetValue('G9T_CODDES')
Local nTotal 	:= oModelG9T:GetValue('G9T_VALTOT')
Local nOpc		:= oModel:GetOperation()

If nOpc <> MODEL_OPERATION_DELETE
	If cCodOrig == cCodDes
		lRet := .F.
		Help( ,, 'Help',, STR0007, 1, 0 ) //'O local de Origem n�o pode ser o mesmo de Destino'
	EndIf
	
	If Empty(cCodOrig) .OR. Empty(cCodDes)
		lRet:= .F.
		Help( ,, 'Help',, STR0008, 1, 0 ) //'O campo C�d.Origem/ C�d.Destino est� vazio. Preencha com um n�mero v�lido'
	EndIf
EndIf

If nOpc == MODEL_OPERATION_INSERT
	DBSelectArea("G9T")
	G9T->( dbSetOrder(2) )
	If G9T->(dbSeek(xFilial("G9T") +cCodOrig + cCodDes))
		lRet:= .F.
		Help( ,, 'Help',, STR0009, 1, 0 ) //'Esse registro j� existe'
	Endif
EndIf
	
RestArea( aAreaG9T)
RestArea( aArea )

Return lRet
