#Include 'Protheus.ch'
#Include "FWMVCDEF.CH"
#Include 'GTPA417.ch'
#INCLUDE 'PARMTYPE.CH'

Static lHabilit	:= .T.
//-------------------------------------------------------------------
/*/{Protheus.doc} GTPA417()

Cadastro de Comiss�es de Contratos de Turismo.
@sample 	GTPA417()

@return 	oBrowse  

@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------
Function GTPA417()
	
	Local oBrowse		:= Nil
		
		oBrowse := FWMBrowse():New()		
		oBrowse:SetAlias('GQS')  
		oBrowse:SetDescription(STR0001)	//Comiss�o de Contratos de Turismo;     
		oBrowse:AddLegend("GQS_STATUS == '2' "	, "GREEN"	,STR0010)//"Ativo"
		oBrowse:AddLegend("GQS_STATUS <> '2' "	, "RED"		,STR0011)//"Inativo" 
		oBrowse:Activate()

Return
//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()
MenuDef

@return 	aRotina  

@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------
Static Function MenuDef() 

	Local aRotina := {}
	
		ADD OPTION aRotina TITLE STR0002  	ACTION 'VIEWDEF.GTPA417' OPERATION 2 ACCESS 0 //Visualizar
		ADD OPTION aRotina TITLE STR0003 	ACTION 'VIEWDEF.GTPA417' OPERATION 3 ACCESS 0 //Incluir
		ADD OPTION aRotina TITLE STR0004 	ACTION 'VIEWDEF.GTPA417' OPERATION 4 ACCESS 0 //Alterar
		ADD OPTION aRotina TITLE STR0005 	ACTION 'VIEWDEF.GTPA417' OPERATION 5 ACCESS 0 //Excluir
		ADD OPTION aRotina TITLE STR0006	ACTION 'VIEWDEF.GTPA417' OPERATION 8 ACCESS 0 //Imprimir
		ADD OPTION aRotina TITLE STR0007 	ACTION 'VIEWDEF.GTPA417' OPERATION 9 ACCESS 0 //Copiar
	
Return ( aRotina )
//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()
Modelo de dados

@return 	oModel  

@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------
Static Function ModelDef()
	
	Local oStruGQS	:= FWFormStruct( 1, 'GQS') // Recebe a Estrutura da Tabela Comiss�o de Contratos de Turismo;          
	Local oStruGQT	:= FWFormStruct( 1, 'GQT' )// Recebe a Estrutura da Tabela Processamento Comiss�o de Contratos de Turismo;
	Local bPosGQT	:= {|oModelGrid,nLine| VldComis(oModelGrid,nLine) } 
	//Local bPos417 	:= { |oModel| VlTudOK(oModel) }
	Local bActive	:= {|oModel| VldAcess(oModel)}
	
		oStruGQS:SetProperty('GQS_CODIGO' 	, MODEL_FIELD_WHEN   	, {|| .F. })
		oStruGQS:SetProperty('GQS_AGENCI' 	, MODEL_FIELD_WHEN   	, {|| INCLUI })
		oStruGQS:SetProperty('GQS_VEND' 	, MODEL_FIELD_WHEN   	, {|| INCLUI })
		oStruGQS:SetProperty('GQS_CODIGO' 	, MODEL_FIELD_VALID   	, {|oModel| VldCdGQS(oModel) } )
		oStruGQS:SetProperty('GQS_AGENCI' 	, MODEL_FIELD_VALID   	, {|oModel| VldAgen(oModel) .AND. VldCtr(oModel) } )'
		oStruGQS:SetProperty('GQS_STATUS'	, MODEL_FIELD_INIT		, {|| '2'} )
		oStruGQS:SetProperty('GQS_STATUS' 	, MODEL_FIELD_WHEN   	, {|| !INCLUI})
		oStruGQS:SetProperty('GQS_STATUS' 	, MODEL_FIELD_VALID   	, {|oModel| VldCtr(oModel) } )
		oStruGQS:SetProperty('GQS_VEND' 	, MODEL_FIELD_VALID   	, {|oModel| VldVend(oModel) .AND. VldCtr(oModel) } )
		oStruGQS:SetProperty("GQS_DSR"		,MODEL_FIELD_OBRIGAT	, .T.)
		oStruGQS:SetProperty('GQS_COMSUP' 	, MODEL_FIELD_WHEN   	, {|| lHabilit})
		oStruGQS:SetProperty('GQS_DSR' 		, MODEL_FIELD_WHEN   	, {|| lHabilit})
	
		oStruGQT:SetProperty("GQT_CVEND"	,MODEL_FIELD_OBRIGAT	, .F.)
		oStruGQT:SetProperty('GQT_PROD' 	, MODEL_FIELD_WHEN   	, {|| INCLUI .OR. Empty(FwFldGet('GQT_PROD')) })
		oStruGQT:SetProperty('*'			, MODEL_FIELD_WHEN 		, {|| lHabilit})
		oModel := MPFormModel():New("GTPA417",/*bPreValid*, /*bPosValid*/, /*bCommit*/, /*bCancel*/ )
		
		oModel:SetDescription(STR0001) // "Comiss�o de Contratos de Turismo"
		
		// ------------------------------------------+
		// ATRIBUI UM COMPONENTE PARA CADA ESTRUTURA |
		// ------------------------------------------+
		oModel:AddFields( 'GQSMASTER',/*cOwner*/, oStruGQS )
		
		oModel:AddGrid( 'GQTDETAIL', 'GQSMASTER', oStruGQT, /*bLinePre*/,/* bLinePost*/bPosGQT, /*bPre*/ , bPosGQT/*bPost*/, /*bLoad*/)
		
		// -------------------------------------------------+
		// FAZ RELACIONAMENTO ENTRE OS COMPONENTES DO MODEL |
		// -------------------------------------------------+
		oModel:SetRelation( 'GQTDETAIL',{{'GQT_FILIAL', 'xFilial( "GQT" )'},{ 'GQT_CODIGO','GQS_CODIGO'},{ 'GQT_CVEND','GQS_VEND'}} , GQT->( IndexKey( 1 ) )  )
		
		
		// ----------------------------------------+
		//  N�o permite linhas duplicadas          |
		// ----------------------------------------+
		oModel:GetModel("GQTDETAIL"):SetUniqueLine( {'GQT_PROD','GQT_CVEND'} )
		
		// ----------------------------------------+
		//  Define a Chave Primaria do Modelo      |
		// ----------------------------------------+
		oModel:SetPrimaryKey( {"GQS_FILIAL","GQS_VEND","GQS_CODIGO","GQS_AGENCI"} )

		oModel:SetVldActivate(bActive)
		
Return(oModel)
//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()
Defini��o do Modelo de Visualiza��o

@return 	oView  

@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------    
Static Function ViewDef()

	Local oView     := FwFormView():New()       // Recebe o objeto da View
	Local oModel    := FwLoadModel( "GTPA417" )	// Objeto do Model 
	Local oStruGQS	:= FWFormStruct( 2, 'GQS' )	
	Local oStruGQT	:= FWFormStruct( 2, 'GQT' ) 
		
		oStruGQS:SetProperty("GQS_VEND",MVC_VIEW_LOOKUP,"SA3")	
		
		oStruGQT:SetProperty("GQT_PROD",MVC_VIEW_LOOKUP,"SB1")		
		oStruGQT:RemoveField("GQT_CVEND")
		oStruGQT:RemoveField("GQT_CODIGO")
		
		//-- Seta o Model para o modelo view
		oView:SetModel(oModel)
		
		//-------------------------------------------+
		// ATRIBUI UM COMPONENTE PARA CADA ESTRUTURA |
		//-------------------------------------------+
		oView:AddField( 'VIEW_GQSMASTER'	, oStruGQS	, 'GQSMASTER' )
		oView:AddGrid ( 'VIEW_GQTDETAIL'	, oStruGQT	, 'GQTDETAIL' )
		
		//-------------------------------------------+
		// DEFINE EM % A DIVISAO DA TELA, HORIZONTAL |
		//-------------------------------------------+
		oView:CreateHorizontalBox( 'SUPERIOR'	, 40 )
		oView:CreateHorizontalBox( 'INFERIOR'	, 60 )
		
		//-------------------------------------------+
		// DEFINE UM BOX PARA CADA COMPONENTE DO MVC |
		//-------------------------------------------+
		oView:SetOwnerView( 'VIEW_GQSMASTER' , 'SUPERIOR' )
		oView:SetOwnerView( 'VIEW_GQTDETAIL' , 'INFERIOR' )
		
		// Liga a identificacao do componente
		oView:EnableTitleView ('VIEW_GQSMASTER'	,STR0001 )// 'Comiss�o de Contratos de Turismo'
		oView:EnableTitleView ('VIEW_GQTDETAIL' ,STR0009 )// 'Comiss�o por Produto'

Return ( oView )
//-------------------------------------------------------------------  
/*/{Protheus.doc} VldCdGQS
Valida codigo do cadastro de comiss�o 
@type function
@author crisf
@since 30/10/2017
@version 1.0
@param oMldG5D, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------  
Function VldCdGQS(oMldG5S)

	Local cCodGQS	:= oMldGQS:GetValue("GQS_CODIGO")
	Local cVenGQS	:= oMldGQS:GetValue("GQS_VEND")
	Local aAreaGQS	:= GQS->(GetArea())
	Local lValido	:= .T.
	
		dbSelectArea("GQS")
		GQS->(dbSetOrder(1))//GQS_FILIAL, GQS_VEND, GQS_CODIGO
		if GQS->(dbSeek(xFilial("GQS")+cVenGQS+cCodGQS))
		
			While GQS->(dbSeek(xFilial("GQS")+cVenGQS+cCodGQS))
			
				ProxNum(@cCodGQS)
				
				lValido	:= oMldGQS:SetValue("GQS_CODIGO",cCodGQS)
					
			EndDo
			
		EndIf
				
		RestArea(aAreaGQS)
	
	
Return lValido
//-------------------------------------------------------------------
/*/{Protheus.doc} ProxNum
Busca o proximo numero disponivel
@type function
@author crisf
@since 30/10/2017
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function ProxNum(cNumProx)

	Local cTmpGQS	

		cTmpGQS	:= GetNextAlias()
		
		BeginSQL alias cTmpGQS
		
			  SELECT  MAX(GQS_CODIGO) CODIGOATUAL
			  FROM %Table:GQS% GQS
			  WHERE GQS.%NotDel%					  
 	
		EndSQL			
		
		cNumProx	:= SOMA1((cTmpGQS)->CODIGOATUAL)
		
		(cTmpGQS)->(dbCloseArea())
			
Return 
//-------------------------------------------------------------------
/*/{Protheus.doc} VldAgen
Valida ag�ncia
@type function
@author crisf
@since 17/11/2017
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function VldAgen(oModel)

	Local aAreaGI6	:= GI6->(GetArea())
	Local oView		 := FwViewActive()  
	Local cCodAgen	:= oView:GetModel("GQSMASTER"):GetValue("GQS_AGENCI")
	Local lVldAgen	:= .T.
	
		//Verificado se o c�digo da ag�ncia informado existente
		dbSelectArea("GI6")
		GI6->(dbSetORder(1))
		if !GI6->(dbSeek(xFilial("GI6")+cCodAgen))
			
			FWAlertHelp(STR0012+cCodAgen+STR0013,STR0014)//"C�digo da ag�ncia ("##") inexistente."##"Informar um c�digo da ag�ncia existente." 
			lVldAgen	:= .F.
		
		Elseif FieldPos('GI6_MSBLQL')> 0 .AND. GI6->GI6_MSBLQL <> '2'
			
			FWAlertHelp(STR0012+cCodAgen+STR0015,STR0016)//"C�digo da ag�ncia ("## ##"Informar um c�digo da ag�ncia ativo."
			lVldAgen	:= .F.	
				
		Elseif GI6->GI6_TIPO <> '1'
		
			FWAlertHelp(STR0031,STR0032)//"Somente ag�ncia do tipo pr�pria poder� utilizar este cadastro."##"Revisar o cadastro da ag�ncia."	
			lVldAgen	:= .F.	
							
		EndIf
		
		RestArea(aAreaGI6)
		
Return 	lVldAgen	
//-------------------------------------------------------------------
/*/{Protheus.doc} VldVend
Valida o Vendedor
@type function
@author crisf
@since 17/11/2017
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function VldVend(oModel)

	Local aAreaSA3	:= SA3->(GetArea())
	Local oView		 := FwViewActive()  
	Local cCodVend	:= oView:GetModel("GQSMASTER"):GetValue("GQS_VEND")
	Local lVldVen	:= .T.

		dbSelectArea("SA3")
		SA3->(dbSetORder(1))
		if !SA3->(dbSeek(xFilial("SA3")+cCodVend))	
		
			FWAlertHelp(STR0025,STR0026)//"C�digo n�o cadastrado"##"Informar um codigo existente. Clique na Lupa para pesquisar."
			lVldVen	:= .F.
		
		Elseif FieldPos('A3_MSBLQL')> 0 .AND. SA3->A3_MSBLQL <> '2'
			
			FWAlertHelp(STR0033+cCodVend+STR0034,STR0035)//"C�digo do vendedor "##" bloqueado/inativo." ##"Informar um c�digo de vendedor ativo."
			lVldVen	:= .F.		
			
		EndIf

		RestArea(aAreaSA3)
			
Return lVldVen
//-------------------------------------------------------------------
/*/{Protheus.doc} VldCtr
Valida status de contrato
@type function
@author crisf
@since 26/10/2017
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function VldCtr(oModel)

	Local oView		 := FwViewActive()  
	Local cCodAgen	:= oView:GetModel("GQSMASTER"):GetValue("GQS_AGENCI")
	Local cCodVend	:= oView:GetModel("GQSMASTER"):GetValue("GQS_VEND")
	Local cTMPGQS	:= GetNextAlias()
	Local lVldCtr	:= .T.
		 
		cCodAgen	:= StrTran(StrTran(cCodAgen,'"',' '),"'"," ")
		cCodVend	:= StrTran(StrTran(cCodVend,'"',' '),"'"," ")
		
		if oView:GetModel("GQSMASTER"):GetValue("GQS_STATUS") == '2'
			
			//Verifica se existe algum cadastro de Comiss�o de Contrato digitada que esteja ativo.
			BeginSQL alias cTMPGQS
			
				  SELECT GQS_CODIGO
				  FROM %Table:GQS% GQS
				  WHERE GQS.%NotDel%	
				    AND GQS.GQS_FILIAL = %xFilial:GQS%
				    AND GQS.GQS_VEND = %Exp:cCodVend%
				 	AND GQS.GQS_AGENCI = %Exp:cCodAgen%				 	
					AND GQS.GQS_STATUS = '2'
	 	
			EndSQL
			//GetlastQuery()[2] retorna a consulta 		
			
			if !(cTMPGQS)->(Eof())
			
				FWAlertHelp(STR0017+cCodAgen+STR0018+cCodVend+STR0019+(cTMPGQS)->GQS_CODIGO+STR0015,STR0020)
				//"Para este c�digo de ag�ncia("##") e vendedor ("##") existe um cadastro ativo ("## "Alterar o c�digo de cadastro ativo."
				lVldCtr	:= .F.	
				
			EndIf
		
		(cTMPGQS)->(dbCloseArea())
		
		EndIf

Return lVldCtr
//-------------------------------------------------------------------
/*/{Protheus.doc} VldAcess
valida acesso de manuten��o do cadastro
@type function
@author crisf
@since 30/10/2017
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function VldAcess(oModel)
	
	Local lVldAcess	:= .T.
	Local cTmpG94	:= ''
	
		if  oModel:GetOperation() == 4 .OR. oModel:GetOperation() == 5
			
			if GQS->GQS_STATUS == '1' 
		
				FWAlertHelp(STR0021,STR0022)//"Este cadastro esta inativo."## "Cadastros inativos n�o podem ser modificados."
				//"Este cadastro esta inativo."##"Cadastros inativos n�o podem ser modificados."
				lVldAcess	:= .F.
			
			Else
			
			//Verifico se o cadastro esta associado a algum calculo de comiss�o.
				cTmpG94	:= GetNextAlias()
				
				BeginSql Alias cTmpG94			
					SELECT 
						G94.G94_CODIGO, 
						G94.G94_VEND
					FROM 
						%Table:G94% G94
					WHERE 
						G94_FILIAL = %xFilial:G94%
					  	AND G94_SIMULA = ' '
					  	AND G94_CODGQS = %exp:GQS->GQS_CODIGO%
					  	AND G94.%NotDel%	  
				EndSql
				//GetlastQuery()[2]
				if !(cTmpG94)->(Eof())
				
					FWAlertHelp(STR0027+(cTmpG94)->G94_CODIGO+STR0028+(cTmpG94)->G94_VEND+STR0029,STR0030)
					//"Existe pelo menos um processamento de comiss�o ( C�digo"##" vendedor "
					//") calculada baseada neste cadastro, portanto, este cadastro n�o poder� ser exclu�do."
					//"Avaliar a possibilidade de excluir o calculo de comiss�o, caso a mesma n�o tenha sido exportada para a folha de pagamento."
					if oModel:GetOperation() == 5
						//Um cadastro vinculado n�o pode ser modificado mantendo assim o hist�rico de percentuais aplicados.
						lVldAcess	:= .F.
						
					Elseif oModel:GetOperation() == 4
						//Permite a visualiza��o da grid e somente poder� inativar o cadastro.
						lHabilit	:= .F.
					
					EndIf
					
				EndIf
				
				(cTmpG94)->(dbCloseArea())
				
			EndIf
			
		EndIf
		
Return lVldAcess
//-------------------------------------------------------------------
/*/{Protheus.doc} VldComis
Valida preenchimento de percentuais
@type function
@author crisf
@since 30/10/2017
@version 1.0
@param oMdlGQT, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///-------------------------------------------------------------------
Static Function VldComis(oMdlGQT,nLine)

	Local lVldCom	:= .F.
	
		if !oMdlGQT:IsDeleted(nLine) .AND. oMdlGQT:GetValue("GQT_COMISS",nLine) <= 0.00 .OR. oMdlGQT:GetValue("GQT_COMISS",nLine) > 100.00 
		
			FWAlertHelp(STR0023,STR0024)
			//"Preenchimento incorreto"## "Preencher o campo %Comiss�o. Permitido percentuais maiores que 0.00% limitado a 100%"
			lVldCom	:= .F.
		
		Else
		
			lVldCom	:= .T.
				
		EndIf		
	
Return lVldCom