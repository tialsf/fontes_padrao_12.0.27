#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'GTPA710.CH'


/*/{Protheus.doc} GTPA710
Fun��o responsavel para trazer o Browse do cadastro de Regras de produtos x Tipos de Bilhetes
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GTPA710()

Local oBrowse		:= FWMBrowse():New()

oBrowse:SetAlias('G9O')
oBrowse:SetDescription(STR0001)	//Cadastro de Regras de Produtos x Tipos de Bilhetes

oBrowse:SetMenuDef('GTPA710')

oBrowse:Activate()

Return ( oBrowse )

/*/{Protheus.doc} ModelDef
Fun��o responsavel para a defini��o do modelo
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ModelDef()

Local oModel		:= nil
Local oStruG9O	:= FWFormStruct(1,'G9O')
Local bPosValid	:= {|oModel|G710PosVld(oModel)}

SetModelStruct(oStruG9O)

oModel := MPFormModel():New('GTPA710', /*bPreValidacao*/, bPosValid, /*bCommit*/, /*bCancel*/ )

oModel:AddFields('G9OMASTER',/*cOwner*/,oStruG9O)
oModel:SetDescription(STR0001) //Cadastro de Regras de Produtos x Tipos de Bilhetes
oModel:GetModel('G9OMASTER'):SetDescription(STR0002)	//Regras de Produtos x Tipos de Bilhetes
oModel:SetPrimaryKey({"G9O_FILIAL","G9O_ORIGEM","G9O_TIPO","G9O_STATUS"})

Return ( oModel )

/*/{Protheus.doc} SetModelStruct
Fun��o responsavel para altera��o da estrutura do modelo
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@param oStruG9O, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetModelStruct(oStruG9O)

oStruG9O:AddTrigger("G9O_PRDTAR", "G9O_DSCTAR"  ,{ || .T. }, { |oMdl| Posicione('SB1',1,xFilial('SB1')+oMdl:GetValue('G9O_PRDTAR'),'B1_DESC') } )
oStruG9O:AddTrigger("G9O_PRDTAX", "G9O_DSCTAX"  ,{ || .T. }, { |oMdl| Posicione('SB1',1,xFilial('SB1')+oMdl:GetValue('G9O_PRDTAX'),'B1_DESC') } )
oStruG9O:AddTrigger("G9O_PRDPED", "G9O_DSCPED"  ,{ || .T. }, { |oMdl| Posicione('SB1',1,xFilial('SB1')+oMdl:GetValue('G9O_PRDPED'),'B1_DESC') } )
oStruG9O:AddTrigger("G9O_PRDSEG", "G9O_DSCSEG"  ,{ || .T. }, { |oMdl| Posicione('SB1',1,xFilial('SB1')+oMdl:GetValue('G9O_PRDSEG'),'B1_DESC') } )
oStruG9O:AddTrigger("G9O_PRDOUT", "G9O_DSCOUT"  ,{ || .T. }, { |oMdl| Posicione('SB1',1,xFilial('SB1')+oMdl:GetValue('G9O_PRDOUT'),'B1_DESC') } )

oStruG9O:AddTrigger("G9O_GQCCOD", "G9O_GQCDES"  ,{ || .T. }, { |oMdl| Posicione('GQC',1,xFilial('GQC')+oMdl:GetValue('G9O_GQCCOD'),'GQC_DESCRI') } )

Return

/*/{Protheus.doc} ViewDef
Fun��o responsavel para a defini��o da interface
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ViewDef()

Local oModel	:= FwLoadModel('GTPA710') 
Local oView		:= FWFormView():New()
Local oStruG9O	:= FWFormStruct(2, 'G9O')

SetViewStruct(oStruG9O)

oView:SetModel(oModel)

oView:AddField('VIEW_G9O' ,oStruG9O,'G9OMASTER')

oView:CreateHorizontalBox('TELA', 100)

oView:SetOwnerView('VIEW_G9O','TELA')

oView:SetDescription(STR0001) //Cadastro de Regras de Produtos x Tipos de Bilhetes

Return ( oView )

/*/{Protheus.doc} SetViewStruct
Fun��o responsavel para alterar a estrutura da View
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@param oStruG9O, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetViewStruct(oStruG9O)

oStruG9O:AddGroup('GRP001', 'Dados do Bilhetes','', 2)

oStruG9O:SetProperty( 'G9O_CODIGO'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')
oStruG9O:SetProperty( 'G9O_ORIGEM'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')
oStruG9O:SetProperty( 'G9O_TIPO'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')
oStruG9O:SetProperty( 'G9O_STATUS'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')
oStruG9O:SetProperty( 'G9O_GQCCOD'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')
oStruG9O:SetProperty( 'G9O_GQCDES'	, MVC_VIEW_GROUP_NUMBER, 'GRP001')

oStruG9O:AddGroup('GRP002', 'Produtos','', 2) // Taxas e Tarifas

oStruG9O:SetProperty( 'G9O_PRDTAR', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_PRDTAX', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_PRDPED', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_PRDSEG', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_PRDOUT', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_DSCTAR', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_DSCTAX', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_DSCPED', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_DSCSEG', MVC_VIEW_GROUP_NUMBER, 'GRP002')
oStruG9O:SetProperty( 'G9O_DSCOUT', MVC_VIEW_GROUP_NUMBER, 'GRP002')



Return 

/*/{Protheus.doc} MenuDef
Fun��o responsavel para definir as opera��es do browse
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function MenuDef()

Local aRotina	:= {}

ADD OPTION aRotina TITLE STR0003    ACTION 'VIEWDEF.GTPA710' OPERATION 2 ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004    ACTION 'VIEWDEF.GTPA710' OPERATION 3 ACCESS 0 // Incluir
ADD OPTION aRotina TITLE STR0005    ACTION 'VIEWDEF.GTPA710' OPERATION 4 ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006    ACTION 'VIEWDEF.GTPA710' OPERATION 5 ACCESS 0 // Excluir

Return ( aRotina )

/*/{Protheus.doc} G710PosVld
Fun��o responsavel para p�s valida��o do modelo (Tudo OK)
@type function
@author jacomo.fernandes
@since 05/09/2018
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function G710PosVld(oModel)
Local lRet	:= .T.
Local oMdlG9O	:= oModel:GetModel('G9OMASTER')

// Se j� existir a chave no banco de dados no momento do commit, a rotina 
If (oMdlG9O:GetOperation() == MODEL_OPERATION_INSERT .OR. oMdlG9O:GetOperation() == MODEL_OPERATION_UPDATE)
	lRet := ExistChav("G9O", oMdlG9O:GetValue("G9O_ORIGEM") + oMdlG9O:GetValue("G9O_TIPO") + oMdlG9O:GetValue("G9O_STATUS")+ oMdlG9O:GetValue("G9O_GQCCOD"),2 )
EndIf

Return lRet
