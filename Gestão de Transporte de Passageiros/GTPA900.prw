#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "GTPA900.CH"

/*/{Protheus.doc} GTPA900()
(long_description)
@type  Function
@author Lucivan Severo Correia
@since 25/06/2020
@version 1
@param 
@return 
@example
(examples)
@see (links_or_references)
/*/
Function GTPA900()

	Local oBrowse
	Local lHasNewTables := ChkFile('GY0') .AND. ChkFile('GYD') .AND. ChkFile('GQI');
							 .AND. ChkFile('GQZ') .AND. ChkFile('GYX') .AND. ChkFile('GQJ')

	If lHasNewTables
		oBrowse := FWMBrowse():New()
		oBrowse:SetAlias("GY0")
		oBrowse:SetDescription(STR0007) //"Or�amento de Contrato"
		
		oBrowse:AddLegend('GY0_STATUS == "1"',"YELLOW"	,STR0041)//"Em or�amento"
		oBrowse:AddLegend('GY0_STATUS == "2"',"GREEN"	,STR0042)//"Contrato gerado"
		oBrowse:AddLegend('GY0_STATUS == "3"',"RED"	    ,STR0043)//"Cancelado"
		oBrowse:ACTIVATE()
	Else
		MsgInfo(STR0017) // "Banco de dados desatualizado, n�o � poss�vel iniciar a rotina"
	EndIf

Return

/*/{Protheus.doc} MenuDef
(long_description)
@type  Static Function
@author Lucivan Severo Correia
@since 25/06/2020
@version 1
@param 
@return aRotina, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function MenuDef()
	Local aRotina := {}

	ADD OPTION aRotina TITLE STR0001 ACTION "VIEWDEF.GTPA900" OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina TITLE STR0002 ACTION "VIEWDEF.GTPA900" OPERATION 3 ACCESS 0 //"Incluir"
	ADD OPTION aRotina TITLE STR0003 ACTION "VIEWDEF.GTPA900" OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina TITLE STR0004 ACTION "VIEWDEF.GTPA900" OPERATION 5 ACCESS 0 //"Excluir"
	ADD OPTION aRotina TITLE STR0024 ACTION "GTPA900CTR()"    OPERATION 2 ACCESS 0 // Gerar contrato 
	ADD OPTION aRotina TITLE STR0025 ACTION "GTPA900CAN()"    OPERATION 2 ACCESS 0 // Cancelar contrato 

Return aRotina

/*/{Protheus.doc} ModelDef
	(long_description)
	@type  Function
	@author Lucivan Severo Correia
	@since 25/06/2020
	@version 1
	@param param_name, param_type, param_descr
	@return oModel, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
	/*/
Static Function ModelDef()
	Local oModel
	Local oStruGY0      := FWFormStruct(1,"GY0")//cabe�alho cadastral
	Local oStruGYD      := FWFormStruct(1,"GYD")//dados da linha
	Local oStruGQJ      := FWFormStruct(1,"GQJ")//Custos adicionais Cadastral
	Local oStruGYX      := FWFormStruct(1,"GYX")//custos adicionais linha
	Local oStruGQZ      := FWFormStruct(1,"GQZ")//custos operacionais
	Local oStruGQI      := FWFormStruct(1,"GQI")//itinerarios
	Local oStruGQ8      := FWFormStruct(1,"GQ8")//Lista de Passageiros - Header
	Local oStruGQB      := FWFormStruct(1,"GQB")//Lista de Passageiros - Detail
	Local bPreGYD       := { |oModel, nLine, cOperation, cField, uValue| GYDLnPre(oModel, nLine, cOperation, cField, uValue)}
	Local bPreGQI       := { |oModel, nLine, cOperation, cField, uValue| GQILnPre(oModel, nLine, cOperation, cField, uValue)}
	Local bPosValid     := { |oModel|PosValid(oModel)}	

	Local lHasNewTables := ChkFile('GY0') .AND. ChkFile('GYD') .AND. ChkFile('GQI');
							 .AND. ChkFile('GQZ') .AND. ChkFile('GYX') .AND. ChkFile('GQJ')
	
	If lHasNewTables

		SetModelStruct(oStruGY0,oStruGYD,oStruGQJ,oStruGYX,oStruGQZ,oStruGQI)

		oModel := MPFormModel():New("GTPA900", /*bPreValid*/, bPosValid, /*bCommit*/, /*bCancel*/ )
		oModel:setDescription(STR0007) //"Or�amento de Contrato"

		oModel:addFields("GY0MASTER",,oStruGY0)
		
		oModel:AddGrid("GQJDETAIL" , "GY0MASTER"  , oStruGQJ)
		oModel:AddGrid("GYDDETAIL" , "GY0MASTER"  , oStruGYD, bPreGYD)
		oModel:AddGrid("GQIDETAIL" , "GYDDETAIL"  , oStruGQI, bPreGQI)
		oModel:AddGrid("GYXDETAIL" , "GYDDETAIL"  , oStruGYX)
		oModel:AddGrid("GQZDETAIL" , "GYDDETAIL"  , oStruGQZ)

		oModel:AddGrid("GQ8DETAIL" , "GYDDETAIL"  , oStruGQ8)
		oModel:AddGrid("GQBDETAIL" , "GQ8DETAIL"  , oStruGQB)

		oModel:getModel("GY0MASTER"):SetDescription(STR0008) //"Dados do Or�amento de Contrato"
		oModel:getModel('GQJDETAIL'):SetDescription(STR0010) //"Custos"		
		oModel:getModel("GYDDETAIL"):SetDescription(STR0009) //"Itiner�rios"
		oModel:getModel("GQIDETAIL"):SetDescription(STR0011) //"Percurso"
		oModel:getModel('GYXDETAIL'):SetDescription(STR0010) //"Custos"
		oModel:getModel('GQZDETAIL'):SetDescription(STR0010) //"Custos"

		If GY0->(FieldPos("GY0_FILIAL")) > 0 .AND. GY0->(FieldPos("GY0_NUMERO")) > 0
			oModel:SetPrimaryKey({"GY0_FILIAL","GY0_NUMERO"})
		EndIf

		If GQJ->(FieldPos("GQJ_FILIAL")) > 0 .AND. GQJ->(FieldPos("GQJ_CODIGO")) > 0 
		oModel:SetRelation("GQJDETAIL", ;
			{{"GQJ_FILIAL",'xFilial("GQJ")'},;
			{"GQJ_CODIGO","GY0_NUMERO"}})
		EndIf

		If GYD->(FieldPos("GYD_FILIAL")) > 0 .AND. GYD->(FieldPos("GYD_NUMERO")) > 0
		oModel:SetRelation("GYDDETAIL", ;
			{{"GYD_FILIAL",'xFilial("GYD")'},;
			{"GYD_NUMERO","GY0_NUMERO"  }})
		EndIf

		If GQI->(FieldPos("GQI_FILIAL")) > 0 .AND. GQI->(FieldPos("GQI_CODIGO")) > 0
		oModel:SetRelation("GQIDETAIL", ;
			{{"GQI_FILIAL",'xFilial("GQI")'},;
			{"GQI_CODIGO","GYD_NUMERO"},;
			{"GQI_ITEM  ","GYD_CODGYD"}})
		EndIf

		If GYX->(FieldPos("GYX_FILIAL")) > 0 .AND. GYX->(FieldPos("GYX_CODIGO")) > 0 
		oModel:SetRelation("GYXDETAIL", ;
			{{"GYX_FILIAL",'xFilial("GYX")'},;
			{"GYX_CODIGO","GYD_NUMERO"},;
			{"GYX_ITEM  ","GYD_CODGYD"}})
		EndIf

		If GQZ->(FieldPos("GQZ_FILIAL")) > 0 .AND. GQZ->(FieldPos("GQZ_CODIGO")) > 0
		oModel:SetRelation("GQZDETAIL", ;
			{{"GQZ_FILIAL",'xFilial("GQZ")'},;
			{"GQZ_CODIGO","GY0_NUMERO"}, ;
			{"GQZ_ITEM  ","GYD_CODGYD"}})
		EndIf

		If GQ8->(FieldPos("GQ8_FILIAL")) > 0 .AND. GQ8->(FieldPos("GQ8_CODGY0")) > 0 .AND. GQ8->(FieldPos("GQ8_CODGYD")) > 0
		oModel:SetRelation("GQ8DETAIL", ;
			{{"GQ8_FILIAL",'xFilial("GQ8")'},;
			{"GQ8_CODGY0","GYD_NUMERO"}, ;
			{"GQ8_CODGYD","GYD_CODGYD"}})
		EndIf

		If GQB->(FieldPos("GQB_FILIAL")) > 0 .AND. GQB->(FieldPos("GQB_CODIGO")) > 0
		oModel:SetRelation("GQBDETAIL", ;
			{{"GQB_FILIAL",'xFilial("GQB")'},;
			{"GQB_CODIGO","GQ8_CODIGO"}})
		EndIf

		oModel:GetModel('GQBDETAIL'):SetDelAllLine(.T.)

		oModel:SetOptional("GYDDETAIL", .T. )
		oModel:SetOptional("GQIDETAIL", .T. )
		oModel:SetOptional("GQJDETAIL", .T. )
		oModel:SetOptional("GYXDETAIL", .T. )
		oModel:SetOptional("GQZDETAIL", .T. )
		oModel:SetOptional("GQ8DETAIL", .T. )
		oModel:SetOptional("GQBDETAIL", .T. )

	Else
		MsgInfo(STR0017) // "Banco de dados desatualizado, n�o � poss�vel iniciar a rotina"
	EndIf

Return oModel

/*/{Protheus.doc} PosValid
(long_description)
@type  Static Function
@author Teixeira
@since 20/08/2020
@version 1.0
@param oModel, objeto, param_descr
@return lRet, L�gico, L�gico
@example
(examples)
@see (links_or_references)
/*/
Static Function PosValid(oModel)

Local oMdlGYD	:= oModel:GetModel('GYDDETAIL')
Local oMdlGQI	:= oModel:GetModel('GQIDETAIL')
Local lRet	    := .T.
Local nGqi      := 0
Local nGyd      := 0

If oModel:GetOperation() == MODEL_OPERATION_INSERT .OR. oModel:GetOperation() == MODEL_OPERATION_UPDATE
    
    For nGyd := 1 To oMdlGYD:Length()
		If !oMdlGYD:IsDeleted(nGyd)
			oMdlGYD:GoLine(nGyd)

			nGqi:= oMdlGQI:Length()
			While nGqi > 1
				If !oMdlGQI:IsDeleted(nGqi)
					oMdlGQI:GoLine(nGqi)
					lRet := oMdlGQI:GetValue("GQI_CODDES",nGqi) == oMdlGYD:GetValue("GYD_LOCFIM",nGyd)
					EXIT
				EndIf
				nGqi--
			End
		EndIf
	Next nGyd
	If !lRet
    	Help(,,"PosValid",, STR0026, 1,0)//"Necess�rio que a ultima linha do itnerario finalize com a localidade final da linha"
    EndIf

EndIf

Return lRet

/*/{Protheus.doc} GYDLnPre
(long_description)
@type  Static Function
@author Teixeira
@since 01/10/2019
@version version
@param oModelGYD, param_type, param_descr
@param nLine, param_type, param_descr
@param cOperation, param_type, param_descr
@param cField, param_type, param_descr
@param uValue, param_type, param_descr
@return lRet, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function GYDLnPre(oModel, nLine, cOperation, cField, uValue)
Local lRet      := .T.
Local nCnt      := 0
Local cMsgTitu  := ""
Local cMsgErro  := ""
Local oMdl      := FwModelActive()
Local oModelGYD := oMdl:GetModel("GYDDETAIL")
Local oModelGQI := oMdl:GetModel("GQIDETAIL")
Local oModelGYX := oMdl:GetModel("GYXDETAIL")
Local oModelGQZ := oMdl:GetModel("GQZDETAIL")

IF (cOperation == "DELETE") 
    
    If oModelGQI:Length() > 0
        nCnt:= oModelGQI:Length()
		While nCnt > 0
			If !oModelGQI:IsDeleted(nCnt)
				oModelGQI:Goline(nCnt)
				oModelGQI:DeleteLine(.T.)
			EndIf
			nCnt--
		End
	EndIf
	
	If oModelGYX:Length() > 0
        For nCnt := 1 To oModelGYX:Length()
            If !oModelGYX:IsDeleted(nCnt)
                oModelGYX:Goline(nCnt)
                oModelGYX:DeleteLine(.T.)
            EndIf
        Next nCnt
	EndIf
	If oModelGQZ:Length() > 0
        For nCnt := 1 To oModelGQZ:Length()
            If !oModelGQZ:IsDeleted(nCnt)
                oModelGQZ:Goline(nCnt)
                oModelGQZ:DeleteLine(.T.)
            EndIf
        Next nCnt
	EndIf
ELSEIF (cOperation == "UNDELETE")
    
    If oModelGQI:Length() > 0
        For nCnt := 1 To oModelGQI:Length()
            If oModelGQI:IsDeleted(nCnt)
                oModelGQI:Goline(nCnt)
                oModelGQI:UnDeleteLine(.T.)
            EndIf
        Next nCnt
	EndIf
	If oModelGYX:Length() > 0
        For nCnt := 1 To oModelGYX:Length()
            If oModelGYX:IsDeleted(nCnt)
                oModelGYX:Goline(nCnt)
                oModelGYX:UnDeleteLine(.T.)
            EndIf
        Next nCnt
	EndIf
	If oModelGQZ:Length() > 0
        For nCnt := 1 To oModelGQZ:Length()
            If oModelGQZ:IsDeleted(nCnt)
                oModelGQZ:Goline(nCnt)
                oModelGQZ:UnDeleteLine(.T.)
            EndIf
        Next nCnt
	EndIf
ELSEIF (cOperation == "SETVALUE" .AND. cField == "GYD_LOCFIM")
    lRet := !(oModelGYD:SeekLine({{'GYD_LOCINI',oModelGYD:GetValue("GYD_LOCINI")},{'GYD_LOCFIM',uValue}}))
	If !lRet .AND. oModelGYD:GetLine() != nLine
		cMsgTitu := STR0027//"Linha duplicada"
		cMsgErro := STR0028//"Sequ�ncia j� utilizada"
		Help(,,cMsgTitu,, cMsgErro, 1,0)
	EndIf

ENDIF

Return lRet

/*/{Protheus.doc} GQILnPre
(long_description)
@type  Static Function
@author Teixeira
@since 01/10/2019
@version version
@param oModelGYD, param_type, param_descr
@param nLine, param_type, param_descr
@param cOperation, param_type, param_descr
@param cField, param_type, param_descr
@param uValue, param_type, param_descr
@return lRet, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function GQILnPre(oModel, nLine, cOperation, cField, uValue)
Local lRet      := .T.
Local nCnt      := 0
Local nTamGrid  := 0
Local nLinhaAtu := 0
Local cMsgTitu  := ""
Local cMsgErro  := ""
Local oMdl      := FwModelActive()
Local oMdlGQI   := oMdl:GetModel("GQIDETAIL")

IF (cOperation == "DELETE") 
    
	nCnt:= oMdlGQI:Length()
	While nCnt > 1
		If !oMdlGQI:IsDeleted(nCnt)
			nTamGrid := nCnt
			EXIT
		EndIf
		nCnt--
	End
	If nLine != nTamGrid .AND. nTamGrid > 0
		lRet:= .F.
		cMsgTitu := STR0029//"Altera��o indevida"
		cMsgErro := STR0030//"N�o pode alterar, por n�o ser ultima linha."
		Help(,,cMsgTitu,, cMsgErro, 1,0)
	EndIf
ELSEIF (cOperation == "UNDELETE")
    
    nCnt:= oMdlGQI:Length()
	While nCnt > 1
		If !oMdlGQI:IsDeleted(nCnt)
			nTamGrid := nCnt
			EXIT
		EndIf
		nCnt--
	End
	If nLine != nTamGrid .AND. nTamGrid > 0 .AND. nLinhaAtu > 0
		lRet:= .F.
		cMsgTitu := STR0029//"Altera��o indevida"
		cMsgErro := STR0030//"N�o pode alterar, por n�o ser ultima linha."
		Help(,,cMsgTitu,, cMsgErro, 1,0)
	EndIf
ELSEIF (cOperation == "SETVALUE" .AND. cField == "GQI_CODDES")
    nCnt:= oMdlGQI:Length()
	While nCnt > 1
		If !oMdlGQI:IsDeleted(nCnt)
			nTamGrid := nCnt
			EXIT
		EndIf
		nCnt--
	End
	If nLine != nTamGrid .AND. nTamGrid > 0 .AND. nLinhaAtu > 0
		lRet:= .F.
		cMsgTitu := STR0029//"Altera��o indevida"
		cMsgErro := STR0030//"N�o pode alterar, por n�o ser ultima linha."
		Help(,,cMsgTitu,, cMsgErro, 1,0)
	EndIf

ENDIF

Return lRet

/*/{Protheus.doc} SetModelStruct
	(long_description)
	@type  Static Function
	@author Teixeira
	@since 18/08/2020
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
	/*/
Static Function SetModelStruct(oStruGY0,oStruGYD,oStruGQJ,oStruGYX,oStruGQZ,oStruGQI)
Local aArea     := GetArea()
Local aTrigAux  := {}
Local bInit	    := {|oMdl,cField,uVal| FieldInit(oMdl,cField,uVal)}
Local bTrig     := {|oMdl,cField,uVal| FieldTrigger(oMdl,cField,uVal)}
Local bFldVld	:= {|oMdl,cField,uVal,nLine,uOldValue| FieldValid(oMdl,cField,uVal,nLine,uOldValue)}

	If GQZ->(FieldPos("GQZ_CODCLI")) > 0
		oStruGQZ:RemoveField("GQZ_CODCLI")
	EndIf
	If GQZ->(FieldPos("GQZ_CODLOJ")) > 0
		oStruGQZ:RemoveField("GQZ_CODLOJ")
	EndIf
	DbSelectArea("SX3")
	SX3->(dbSetOrder(2))
	If SX3->(dbSeek( "GQZ_NOMCLI"))
		oStruGQZ:RemoveField("GQZ_NOMCLI")
	EndIf
	If GQZ->(FieldPos("GQZ_INIVIG")) > 0
		oStruGQZ:RemoveField("GQZ_INIVIG")
	EndIf
	If GQZ->(FieldPos("GQZ_FIMVIG")) > 0
		oStruGQZ:RemoveField("GQZ_FIMVIG")
	EndIf
	If GQZ->(FieldPos("GQZ_TPDESC"))  > 0
		oStruGQZ:RemoveField("GQZ_TPDESC")
	EndIf
	If GQZ->(FieldPos("GQZ_VALOR")) > 0
		oStruGQZ:RemoveField("GQZ_VALOR")
	EndIf
	If GQZ->(FieldPos("GQZ_TIPCUS")) > 0
		oStruGQZ:RemoveField("GQZ_TIPCUS")
	EndIf

	If GY0->(FieldPos("GY0_DTINIC")) > 0
		oStruGY0:SetProperty('GY0_DTINIC',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_UNVIGE")) > 0
		oStruGY0:SetProperty('GY0_UNVIGE',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_MOEDA")) > 0
		oStruGY0:SetProperty('GY0_MOEDA' ,MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_CONDPG")) > 0
		oStruGY0:SetProperty('GY0_CONDPG',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_TPCTO")) > 0
		oStruGY0:SetProperty('GY0_TPCTO' ,MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_FLGREJ")) > 0
		oStruGY0:SetProperty('GY0_FLGREJ',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_FLGCAU")) > 0
		oStruGY0:SetProperty('GY0_FLGCAU',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_CLIENT")) > 0
		oStruGY0:SetProperty('GY0_CLIENT',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_LOJACL")) > 0
		oStruGY0:SetProperty('GY0_LOJACL',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_CODVD")) > 0
		oStruGY0:SetProperty('GY0_CODVD',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_TABPRC")) > 0
		oStruGY0:SetProperty('GY0_TABPRC',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_TIPPLA")) > 0
		oStruGY0:SetProperty('GY0_TIPPLA',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	If GY0->(FieldPos("GY0_PRODUT")) > 0
		oStruGY0:SetProperty('GY0_PRODUT',MODEL_FIELD_OBRIGAT, .T. )
	EndIf
	
	If GY0->(FieldPos("GY0_CODCN9")) > 0
		oStruGY0:SetProperty('GY0_CODCN9',MODEL_FIELD_OBRIGAT, .F. )
	EndIf

	oStruGQJ:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)
	oStruGYX:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)
	oStruGQZ:SetProperty('*', MODEL_FIELD_OBRIGAT, .F.)

	If GQI->(FieldPos("GQI_CODORI")) > 0
		aTrigAux := FwStruTrigger("GQI_CODORI", "GQI_DESORI", "Posicione('GI1',1,xFilial('GI1') + FwFldGet('GQI_CODORI'), 'GI1_DESCRI')")
		oStruGQI:AddTrigger(aTrigAux[1],aTrigAux[2],aTrigAux[3],aTrigAux[4])	
	EndIf
	If GQI->(FieldPos("GQI_CODORI")) > 0
		aTrigAux := FwStruTrigger("GQI_CODDES", "GQI_DESDES", "Posicione('GI1',1,xFilial('GI1') + FwFldGet('GQI_CODDES'), 'GI1_DESCRI')")
		oStruGQI:AddTrigger(aTrigAux[1],aTrigAux[2],aTrigAux[3],aTrigAux[4])	
	EndIf
	If GYD->(FieldPos("GYD_LOCFIM")) > 0
		oStruGYD:AddTrigger("GYD_LOCFIM","GYD_LOCFIM",{||.T.},bTrig)	
	EndIf
	
	If GQJ->(FieldPos("GQJ_CUSUNI")) > 0
		oStruGQJ:AddTrigger("GQJ_CUSUNI","GQJ_CUSUNI",{||.T.},bTrig)	
	EndIf
	If GQJ->(FieldPos("GQJ_QUANT")) > 0
		oStruGQJ:AddTrigger("GQJ_QUANT","GQJ_QUANT",{||.T.},bTrig)	
	EndIf
	If GQJ->(FieldPos("GQJ_UN")) > 0
		oStruGQJ:AddTrigger("GQJ_UN","GQJ_UN",{||.T.},bTrig)	
	EndIf
	If GYX->(FieldPos("GYX_CUSUNI")) > 0
		oStruGYX:AddTrigger("GYX_CUSUNI","GYX_CUSUNI",{||.T.},bTrig)	
	EndIf
	If GYX->(FieldPos("GYX_QUANT")) > 0
		oStruGYX:AddTrigger("GYX_QUANT","GYX_QUANT",{||.T.},bTrig)	
	EndIf
	If GYX->(FieldPos("GYX_UM")) > 0
		oStruGYX:AddTrigger("GYX_UM","GYX_UM",{||.T.},bTrig)	
	EndIf
	If GQZ->(FieldPos("GQZ_CUSUNI")) > 0
		oStruGQZ:AddTrigger("GQZ_CUSUNI","GQZ_CUSUNI",{||.T.},bTrig)	
	EndIf
	If GQZ->(FieldPos("GQZ_QUANT")) > 0
		oStruGQZ:AddTrigger("GQZ_QUANT","GQZ_QUANT",{||.T.},bTrig)	
	EndIf
	If GQZ->(FieldPos("GQZ_UM")) > 0
		oStruGQZ:AddTrigger("GQZ_UM","GQZ_UM",{||.T.},bTrig)	
	EndIf
	If GQJ->(FieldPos("GQJ_CODGIM")) > 0
		oStruGQJ:AddTrigger("GQJ_CODGIM", "GQJ_CODGIM", {||.T.},bTrig)
	EndIf
	If GYX->(FieldPos("GYX_CODGIM")) > 0
		oStruGYX:AddTrigger("GYX_CODGIM", "GYX_CODGIM", {||.T.},bTrig)
	EndIf
	

	If GYD->(FieldPos("GYD_PRODUT")) > 0
		oStruGYD:SetProperty('GYD_PRODUT', MODEL_FIELD_VALID, bFldVld)
	EndIf
	If GYD->(FieldPos("GYD_LOCFIM")) > 0
		oStruGYD:SetProperty('GYD_LOCFIM', MODEL_FIELD_VALID, bFldVld)
	EndIf

	If GQI->(FieldPos("GQI_CODDES")) > 0
		oStruGQI:SetProperty('GQI_CODDES', MODEL_FIELD_VALID, bFldVld)
	EndIf

	If GQJ->(FieldPos("GQJ_CODGIM")) > 0
		oStruGQJ:SetProperty('GQJ_CODGIM', MODEL_FIELD_VALID, bFldVld)
	EndIf

	If GYX->(FieldPos("GYX_CODGIM")) > 0
		oStruGYX:SetProperty('GYX_CODGIM', MODEL_FIELD_VALID, bFldVld)
	EndIf

	If GY0->(FieldPos("GY0_NUMERO")) > 0
		oStruGY0:SetProperty("GY0_NUMERO" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GYD->(FieldPos("GYD_NUMERO")) > 0
		oStruGYD:SetProperty("GYD_NUMERO" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQZ->(FieldPos("GQZ_CODIGO")) > 0
		oStruGQZ:SetProperty("GQZ_CODIGO" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQI->(FieldPos("GQI_CODIGO")) > 0
		oStruGQI:SetProperty("GQI_CODIGO" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQI->(FieldPos("GQI_CODORI")) > 0
		oStruGQI:SetProperty("GQI_CODORI" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQI->(FieldPos("GQI_DESORI")) > 0
		oStruGQI:SetProperty("GQI_DESORI" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQI->(FieldPos("GQI_DESDES")) > 0
		oStruGQI:SetProperty("GQI_DESDES" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GYX->(FieldPos("GYX_CODIGO")) > 0
		oStruGYX:SetProperty("GYX_CODIGO" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQI->(FieldPos("GQI_ITEM")) > 0
		oStruGQI:SetProperty("GQI_ITEM" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GYX->(FieldPos("GYX_ITEM")) > 0
		oStruGYX:SetProperty("GYX_ITEM" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQZ->(FieldPos("GQZ_ITEM")) > 0
		oStruGQZ:SetProperty("GQZ_ITEM" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GY0->(FieldPos("GY0_MOEDA")) > 0
		oStruGY0:SetProperty("GY0_MOEDA" , MODEL_FIELD_INIT, bInit)
	EndIf
	If GQJ->(FieldPos("GQJ_CODIGO")) > 0
		oStruGQJ:SetProperty("GQJ_CODIGO" , MODEL_FIELD_INIT, bInit)
	EndIf

RestArea(aArea)	
Return 

/*/{Protheus.doc} FieldInit
(long_description)
@type  Static Function
@author Teixeira
@since 24/07/2020
@version 1
@param oMdl, object, modelo
@param cField, caracter, campo
@return uRet, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function FieldInit(oMdl,cField,uVal)

	Local uRet      := nil
	Local oModel	:= oMdl:GetModel()
	Local lInsert	:= oModel:GetOperation() == MODEL_OPERATION_INSERT
	Local aArea     := GetArea()
	Local oModelGYD := oModel:GetModel("GYDDETAIL")
	Local nLine     := oModelGYD:GetLine()
	Local oModelGQI := oModel:GetModel("GQIDETAIL")
	Local nLineGQI  := oModelGQI:GetLine()

	Do Case
	Case cField == "GY0_NUMERO"
		uRet := GETSXENUM("GY0","GY0_NUMERO")
	Case cField == "GYD_NUMERO"
		uRet := If(lInsert,FWFLDGET("GY0_NUMERO"),GY0->GY0_NUMERO)
	Case cField == "GQZ_CODIGO"
		uRet := If(lInsert,FWFLDGET("GY0_NUMERO"),GY0->GY0_NUMERO)
	Case cField == "GQI_CODIGO"
		uRet := If(lInsert,FWFLDGET("GY0_NUMERO"),GY0->GY0_NUMERO)
	Case cField == "GQJ_CODIGO"
		uRet := If(lInsert,FWFLDGET("GY0_NUMERO"),GY0->GY0_NUMERO)
	Case cField == "GYX_CODIGO"
		uRet := If(lInsert,FWFLDGET("GY0_NUMERO"),GY0->GY0_NUMERO)
	
	Case cField == "GQI_ITEM"
		uRet := oModelGYD:GetValue("GYD_CODGYD",nLine)
	Case cField == "GYX_ITEM"
		uRet := oModelGYD:GetValue("GYD_CODGYD",nLine)
	Case cField == "GQZ_ITEM"
		uRet := oModelGYD:GetValue("GYD_CODGYD",nLine)
	Case cField == "GQI_DESORI"
		If nLineGQI > 0
			uRet := oModelGQI:GetValue("GQI_CODDES",nLineGQI)
			uRet := IIF(INCLUI .AND. nLineGQI < 1, "", POSICIONE("GI1", 1, XFILIAL("GI1") + uRet, "GI1_DESCRI") )
		Else
			uRet := ""
		EndIf
	Case cField == "GQI_DESDES"
		uRet := IIF(INCLUI, "", POSICIONE("GI1", 1, XFILIAL("GI1") + uVal, "GI1_DESCRI") )

	Case cField == "GY0_MOEDA"
		uRet := "1"
	
	Case cField == "GQI_CODORI"
		If nLineGQI > 0
			uRet := oModelGQI:GetValue("GQI_CODDES",nLineGQI)
			oModelGQI:LoadValue("GQI_DESORI",POSICIONE("GI1", 1, XFILIAL("GI1") + uRet, "GI1_DESCRI") )
		Else
			uRet := ""
		EndIf
	EndCase

	RestArea(aArea)

Return uRet

/*/{Protheus.doc} FieldTrigger
//TODO Descri��o auto-gerada.
@author Teixeira
@since 14/08/2019
@version 1.0
@return ${return}, ${return_description}
@param oMdl, object, descricao
@param cField, characters, descricao
@param uVal, undefined, descricao
@type function
/*/
Static Function FieldTrigger(oMdl,cField,uVal)
Local uRet     := ""
Local n1       := 0
Local oModel   := FwModelActive()
Local oMdlGY0  := oModel:GetModel('GY0MASTER')
Local oMdlGYD  := oModel:GetModel('GYDDETAIL')
Local oMdlGQJ  := oModel:GetModel('GQJDETAIL')
Local oMdlGYX  := oModel:GetModel('GYXDETAIL')
Local oMdlGQI  := oModel:GetModel("GQIDETAIL")
Local oMdlGQZ  := oModel:GetModel("GQZDETAIL")
Local nLineGYD := oMdlGYD:GetLine()
Local nLineGQI := oMdlGQI:GetLine()
Local nLineGQJ := oMdlGQJ:GetLine()
Local nLineGYX := oMdlGYX:GetLine()
Local nLineGQZ := oMdlGQZ:GetLine()
Do Case
	Case cField == 'GYD_LOCFIM'
        uRet := oMdlGYD:GetValue("GYD_LOCINI")
		oMdlGYD:GoLine(nLineGYD)
		If nLineGQI > 1
			For n1	:= 1 to oMdlGQI:Length()
				If !oMdlGQI:IsDeleted(n1)
					oMdlGQI:GoLine(n1)
					oMdlGQI:Deleteline()
				Endif
			Next
		EndIf
		If nLineGQI != 1
			oMdlGQI:AddLine()
		EndIf
        oMdlGQI:SetValue('GQI_CODORI',uRet)
		oMdlGQI:SetValue('GQI_CODDES',uVal)
	Case cField == 'GQJ_CODGIM'
		TrigGIMPrd(oMdlGQJ, oMdlGY0:GetValue('GY0_PRODUT'), uVal)
	Case cField == 'GYX_CODGIM'
		TrigGIMPrd(oMdlGYX, oMdlGYD:GetValue('GYD_PRODUT'), uVal)
	Case cField = "GQJ_CUSUNI"
		uRet := oMdlGQJ:GetValue('GQJ_CUSUNI',nLineGQJ) * oMdlGQJ:GetValue('GQJ_QUANT',nLineGQJ)
		oMdlGQJ:SetValue('GQJ_VALTOT',uRet)
	Case cField = "GQJ_QUANT"
		uRet := oMdlGQJ:GetValue('GQJ_CUSUNI',nLineGQJ) * oMdlGQJ:GetValue('GQJ_QUANT',nLineGQJ)
		oMdlGQJ:SetValue('GQJ_VALTOT',uRet)
	Case cField = "GYX_CUSUNI"
		uRet := oMdlGYX:GetValue('GYX_CUSUNI',nLineGYX) * oMdlGYX:GetValue('GYX_QUANT',nLineGYX)
		oMdlGYX:SetValue('GYX_VALTOT',uRet)
	Case cField = "GYX_QUANT"
		uRet := oMdlGYX:GetValue('GYX_CUSUNI',nLineGYX) * oMdlGYX:GetValue('GYX_QUANT',nLineGYX)
		oMdlGYX:SetValue('GYX_VALTOT',uRet)
	Case cField = "GQZ_CUSUNI"
		uRet := oMdlGQZ:GetValue('GQZ_CUSUNI',nLineGQZ) * oMdlGQZ:GetValue('GQZ_QUANT',nLineGQZ)
		oMdlGQZ:SetValue('GQZ_VALTOT',uRet)
	Case cField = "GQZ_QUANT"
		uRet := oMdlGQZ:GetValue('GQZ_CUSUNI',nLineGQZ) * oMdlGQZ:GetValue('GQZ_QUANT',nLineGQZ)
		oMdlGQZ:SetValue('GQZ_VALTOT',uRet)
	Case cField == "GQJ_UN"
		oMdlGQJ:SetValue('GQJ_DESCUN',POSICIONE("SAH",1,XFILIAL("SAH")+uVal,"AH_UMRES"))
	Case cField == "GYX_UM"
		oMdlGYX:SetValue('GYX_DESUM',POSICIONE("SAH",1,XFILIAL("SAH")+uVal,"AH_UMRES"))
	Case cField == "GQZ_UM"
		oMdlGQZ:SetValue('GQZ_DESUM',POSICIONE("SAH",1,XFILIAL("SAH")+uVal,"AH_UMRES"))
EndCase

Return uVal

/*/{Protheus.doc} FieldValid
(long_description)
@type  Static Function
@author Teixeira
@since 25/09/2019
@version 1.0
@param oMdl, objeto, Modelo 
@param cField, caracter, Campo
@param uVal, undefined, Modelo
@param nLine, numerico, Modelo
@param uOldValue, undefined, Modelo
@return uRet, undefined, Valor de retorno
@example
(examples)
@see (links_or_references)
/*/
Static Function FieldValid(oMdl,cField,uVal,nLine,uOldValue)

Local lRet      := .T.
Local cMsgErro  := ""
Local cMsgTitu  := ""
Local oModel    := FwModelActive()
Local oMdlGYD   := oModel:GetModel("GYDDETAIL")
Local oMdlGQI   := oModel:GetModel("GQIDETAIL")
Local nCnt      := 0
Local nTamGrid  := 0
Local nLinhaAtu := 0
Local aAreaAux  := {}

Do Case 
	Case cField == 'GYD_PRODUT'
        lRet := oMdlGYD:SeekLine({{'GYD_PRODUT',uVal}})
        If lRet .AND. oMdlGYD:GetLine() != nLine
			cMsgTitu := STR0031//"Produto duplicado"
			cMsgErro := STR0032//"S� pode um produto por linha"
			lRet := .F.
        EndIf
		oMdlGYD:GoLine(nLine)
		
		aAreaAux := SB1->(GetArea())
		SB1->(DbSetOrder(1))//B1_FILIAL+B1_COD
		If !SB1->(DbSeek(xFilial('SB1')+uVal))
			lRet	:= .F.
			cMsgTitu:= STR0033//"Produto n�o encontrado"
			cMsgErro:= STR0034//"Selecione algum produto que exista"
			
		ElseIf !RegistroOk("SB1")
			lRet	:= .F.
			cMsgTitu:= STR0035//"Produto informado se encontra bloqueado" 
			cMsgErro:= STR0036//"Informe um produto valido"
			
		ElseIf SB1->B1_TIPO <> "SV"
			lRet	:= .F.
			cMsgTitu:= STR0037//"Tipo de Produto inv�lido."
			cMsgErro:= STR0038//"O tipo do produto de ser do tipo servi�o 'SV'"
		
		Endif 
		RestArea(aAreaAux)
	
	Case cField == 'GYD_LOCFIM'
		
		lRet := oMdlGYD:SeekLine({{'GYD_LOCINI',oMdlGYD:GetValue("GYD_LOCINI")},{'GYD_LOCFIM',uVal}})
		If lRet .AND. oMdlGYD:GetLine() != nLine
			cMsgTitu := STR0027//"Linha duplicada"
			cMsgErro := STR0028//"Sequ�ncia j� utilizada"
			lRet := .F.
		EndIf
		oMdlGYD:GoLine(nLine)

	Case cField == 'GQI_CODDES'
		nLinhaAtu:= oMdlGQI:GetLine()
		nCnt:= oMdlGQI:Length()
		While nCnt > 1
			If !oMdlGQI:IsDeleted(nCnt)
				nTamGrid := nCnt
				EXIT
			EndIf
			nCnt--
		End
		If nLinhaAtu != nTamGrid .AND. nTamGrid > 0 .AND. nLinhaAtu > 0
			lRet:= .F.
			cMsgTitu := STR0029//"Altera��o indevida"
			cMsgErro := STR0030//"N�o pode alterar, por n�o ser ultima linha."
		EndIf

	Case cField $ 'GQJ_CODGIM|GYX_CODGIM'
		GIM->(DbSetOrder(1))
		If !GIM->(DbSeek(xFilial('GIM')+uVal))
			lRet:= .F.
			cMsgTitu := STR0044 //"C�digo n�o encontrado"
			cMsgErro := STR0045 //"Utilize um c�digo v�lido"
		Endif

EndCase

If !(EMPTY(cMsgTitu)) .AND. !(EMPTY(cMsgErro))
	Help(,,cMsgTitu,, cMsgErro, 1,0)
EndIf

Return lRet

/*/{Protheus.doc} ViewDef
(long_description)
@type  Function
@author Lucivan Severo Correia
@since 25/06/2020
@version 1
@param param_name, param_type, param_descr
@return oView, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function ViewDef()
	Local oModel        := ModelDef()
	Local oView
	Local oStruGY0      := FWFormStruct(2,"GY0")//cabe�alho cadastral
	Local oStruGYD      := FWFormStruct(2,"GYD")//dados da linha
	Local oStruGQJ      := FWFormStruct(2,"GQJ")//Custos adicionais Cadastral
	Local oStruGYX      := FWFormStruct(2,"GYX")//custos adicionais linha
	Local oStruGQZ      := FWFormStruct(2,"GQZ")//custos operacionais
	Local oStruGQI      := FWFormStruct(2,"GQI")//itinerarios
	Local lHasNewTables := ChkFile('GY0') .AND. ChkFile('GYD') .AND. ChkFile('GQI');
							 .AND. ChkFile('GQZ') .AND. ChkFile('GYX') .AND. ChkFile('GQJ')

	If lHasNewTables
		
		SetViewStruct(oStruGY0,oStruGYD,oStruGQJ,oStruGYX,oStruGQZ,oStruGQI)
		// Cria o objeto de View
		oView := FWFormView():New()

		// Define qual o Modelo de dados ser� utilizado
		oView:SetModel(oModel)

		oView:AddField("VIEW_GY0", oStruGY0 ,"GY0MASTER" )
		oView:AddGrid("VIEW_GYD" , oStruGYD ,"GYDDETAIL" )
		oView:AddGrid('VIEW_GQJ' , oStruGQJ ,"GQJDETAIL" )
		oView:AddGrid('VIEW_GYX' , oStruGYX ,"GYXDETAIL" )
		oView:AddGrid('VIEW_GQZ' , oStruGQZ ,"GQZDETAIL" )
		oView:AddGrid("VIEW_GQI" , oStruGQI ,"GQIDETAIL" )

		oView:CreateFolder("FOLDER")
		// Divis�o Horizontal
		oView:AddSheet( "FOLDER", "ABA01", STR0012) //"Cadastral"
		oView:CreateHorizontalBox( "SUPERIOR", 60,,,"FOLDER", "ABA01")
		oView:CreateHorizontalBox( "INFERIOR", 40,,,"FOLDER", "ABA01")

		oView:EnableTitleView("VIEW_GQJ" , STR0013) //"Custos adicionais/Cortesia"

		oView:AddSheet( "FOLDER", "ABA02", STR0014) //"Precifica��o/Itiner�rio"
		oView:CreateHorizontalBox( "BOX_SUPERIOR", 30,,,"FOLDER", "ABA02")

		oView:CreateHorizontalBox( "BOX_ITINERARIO", 30, , , "FOLDER", "ABA02")
		oView:CreateHorizontalBox( "BOX_CUSTOS", 20, , , "FOLDER", "ABA02")
		oView:CreateHorizontalBox( "BOX_CUSTOS_OPERACIONAIS" , 20, /*owner*/,/*lUsePixel*/, "FOLDER", "ABA02" )

		oView:EnableTitleView("VIEW_GYD"  , STR0015) //"Dados da Linha"
		oView:EnableTitleView("VIEW_GQI"  , STR0009) //"Itiner�rio"
		oView:EnableTitleView("VIEW_GYX" , STR0013) //"Custos adicionais/Cortesia"
		oView:EnableTitleView("VIEW_GQZ" , STR0016) //"Custos operacionais"

		oView:SetOwnerView("VIEW_GY0" , "SUPERIOR")
		oView:SetOwnerView("VIEW_GYD" , "BOX_SUPERIOR")
		oView:SetOwnerView("VIEW_GQJ" , "INFERIOR")
		oView:SetOwnerView("VIEW_GYX" , "BOX_CUSTOS")
		oView:SetOwnerView("VIEW_GQZ" , "BOX_CUSTOS_OPERACIONAIS")
		oView:SetOwnerView("VIEW_GQI" , "BOX_ITINERARIO")

		oView:AddIncrementField( 'VIEW_GQJ', 'GQJ_SEQ' )
		oView:AddIncrementField( 'VIEW_GYX', 'GYX_SEQ' )
		oView:AddIncrementField( 'VIEW_GQI', 'GQI_SEQ' )
		oView:AddIncrementField( 'VIEW_GQZ', 'GQZ_SEQ' )

		oView:AddUserButton( STR0039, "", {|| GA900Calc(oView)},,VK_F5 )//"C�lculo Custo"
		oView:AddUserButton( STR0040, "", {|| GA900Inc(oView)},,VK_F7,{MODEL_OPERATION_INSERT} )//"Listagem passageiros"
	Else
		MsgInfo(STR0017) // "Banco de dados desatualizado, n�o � poss�vel iniciar a rotina"
	EndIf
Return oView

/*/{Protheus.doc} SetViewStruct
(long_description)
@type  Static Function
@author Teixeira
@since 18/08/2020
@version version
@param param_name, param_type, param_descr
@return return_var, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function SetViewStruct(oStruGY0,oStruGYD,oStruGQJ,oStruGYX,oStruGQZ,oStruGQI)

Local aArea := GetArea()

	If GQZ->(FieldPos("GQZ_CODCLI")) > 0
		oStruGQZ:RemoveField("GQZ_CODCLI")
	EndIf
	If GQZ->(FieldPos("GQZ_CODLOJ")) > 0
		oStruGQZ:RemoveField("GQZ_CODLOJ")
	EndIf
	DbSelectArea("SX3")
	SX3->(dbSetOrder(2))
	If SX3->(dbSeek( "GQZ_NOMCLI"))
		oStruGQZ:RemoveField("GQZ_NOMCLI")
	EndIf
	If GQZ->(FieldPos("GQZ_INIVIG")) > 0
		oStruGQZ:RemoveField("GQZ_INIVIG")
	EndIf
	If GQZ->(FieldPos("GQZ_FIMVIG")) > 0
		oStruGQZ:RemoveField("GQZ_FIMVIG")
	EndIf
	If GQZ->(FieldPos("GQZ_TPDESC"))  > 0
		oStruGQZ:RemoveField("GQZ_TPDESC")
	EndIf
	If GQZ->(FieldPos("GQZ_VALOR")) > 0
		oStruGQZ:RemoveField("GQZ_VALOR")
	EndIf
	If GQZ->(FieldPos("GQZ_TIPCUS")) > 0
		oStruGQZ:RemoveField("GQZ_TIPCUS")
	EndIf

	If GY0->(FieldPos("GY0_STATUS")) > 0
		oStruGY0:RemoveField("GY0_STATUS")
	EndIf
	If GY0->(FieldPos("GY0_CODCN9")) > 0
		oStruGY0:RemoveField("GY0_CODCN9")
	EndIf

	If GYD->(FieldPos("GYD_CODGI2")) > 0
		oStruGYD:RemoveField("GYD_CODGI2")
	EndIf
	
	If GQJ->(FieldPos("GQJ_PLAN")) > 0
		oStruGQJ:RemoveField("GQJ_PLAN")
	EndIf

	If GYX->(FieldPos("GYX_PLAN")) > 0
		oStruGYX:RemoveField("GYX_PLAN")
	EndIf
	
	If GYD->(FieldPos("GYD_NUMERO")) > 0
		oStruGYD:SetProperty('GYD_NUMERO', MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GYD->(FieldPos("GYD_CODGYD")) > 0
		oStruGYD:SetProperty('GYD_CODGYD', MVC_VIEW_CANCHANGE , .F. )
	EndIf

	If GQI->(FieldPos("GQI_CODORI")) > 0
		oStruGQI:SetProperty('GQI_CODORI', MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GQI->(FieldPos("GQI_DESORI")) > 0
		oStruGQI:SetProperty('GQI_DESORI', MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GQI->(FieldPos("GQI_DESDES")) > 0
		oStruGQI:SetProperty('GQI_DESDES', MVC_VIEW_CANCHANGE , .F. )
	EndIf

	If GYX->(FieldPos("GYX_CODIGO")) > 0
		oStruGYX:SetProperty('GYX_CODIGO', MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GYX->(FieldPos("GYX_SEQ")) > 0
		oStruGYX:SetProperty('GYX_SEQ'   , MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GYX->(FieldPos("GYX_ITEM")) > 0
		oStruGYX:SetProperty('GYX_ITEM'  , MVC_VIEW_CANCHANGE , .F. )
	EndIf

	If GQZ->(FieldPos("GQZ_CODIGO")) > 0
		oStruGQZ:SetProperty('GQZ_CODIGO', MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GQZ->(FieldPos("GQZ_SEQ")) > 0
		oStruGQZ:SetProperty('GQZ_SEQ'   , MVC_VIEW_CANCHANGE , .F. )
	EndIf
	If GQZ->(FieldPos("GQZ_ITEM")) > 0
		oStruGQZ:SetProperty('GQZ_ITEM'  , MVC_VIEW_CANCHANGE , .F. )
	EndIf

	If GY0->(FieldPos("GY0_TPCTO")) > 0
		oStruGY0:SetProperty('GY0_TPCTO' , MVC_VIEW_LOOKUP, "CN1")
	EndIf

	If GYD->(FieldPos("GYD_LOCINI")) > 0
		oStruGYD:SetProperty('GYD_LOCINI', MVC_VIEW_LOOKUP, "GI1")
	EndIf
	If GYD->(FieldPos("GYD_LOCFIM")) > 0
		oStruGYD:SetProperty('GYD_LOCFIM', MVC_VIEW_LOOKUP, "GI1")
	EndIf
	If GYD->(FieldPos("GYD_ORGAO")) > 0
		oStruGYD:SetProperty('GYD_ORGAO' , MVC_VIEW_LOOKUP, "GI0")
	EndIf

	If GQI->(FieldPos("GQI_CODORI")) > 0
		oStruGQI:SetProperty('GQI_CODORI', MVC_VIEW_LOOKUP, "GI1")
	EndIf
	If GQI->(FieldPos("GQI_CODDES")) > 0
		oStruGQI:SetProperty('GQI_CODDES', MVC_VIEW_LOOKUP, "GI1")
	EndIf
	
	If GQJ->(FieldPos("GQJ_SEQ")) > 0
		oStruGQJ:SetProperty("GQJ_SEQ"    , MVC_VIEW_ORDEM, '01')
	EndIf
	If GQJ->(FieldPos("GQJ_CODGIM")) > 0
		oStruGQJ:SetProperty("GQJ_CODGIM" , MVC_VIEW_ORDEM, '02')
	EndIf
	If GQJ->(FieldPos("GQJ_DCUSTO")) > 0
		oStruGQJ:SetProperty("GQJ_DCUSTO" , MVC_VIEW_ORDEM, '03')
	EndIf
	If GQJ->(FieldPos("GQJ_FORMUL")) > 0
		oStruGQJ:SetProperty("GQJ_FORMUL" , MVC_VIEW_ORDEM, '04')
	EndIf
	If GQJ->(FieldPos("GQJ_TIPO")) > 0
		oStruGQJ:SetProperty("GQJ_TIPO"   , MVC_VIEW_ORDEM, '05')
	EndIf
	If GQJ->(FieldPos("GQJ_CUSUNI")) > 0
		oStruGQJ:SetProperty("GQJ_CUSUNI" , MVC_VIEW_ORDEM, '06')
	EndIf
	If GQJ->(FieldPos("GQJ_UN")) > 0
		oStruGQJ:SetProperty("GQJ_UN"     , MVC_VIEW_ORDEM, '07')
	EndIf
	If GQJ->(FieldPos("GQJ_QUANT")) > 0
		oStruGQJ:SetProperty("GQJ_QUANT"  , MVC_VIEW_ORDEM, '08')
	EndIf
	If GQJ->(FieldPos("GQJ_VALTOT")) > 0
		oStruGQJ:SetProperty("GQJ_VALTOT" , MVC_VIEW_ORDEM, '09')
	EndIf

	If GYD->(FieldPos("GYD_CODGYD")) > 0
		oStruGYD:SetProperty("GYD_CODGYD" , MVC_VIEW_ORDEM, '02')
	EndIf
	If GYD->(FieldPos("GYD_ORGAO")) > 0
		oStruGYD:SetProperty("GYD_ORGAO"  , MVC_VIEW_ORDEM, '03')
	EndIf
	If GYD->(FieldPos("GYD_PRODUT")) > 0
		oStruGYD:SetProperty("GYD_PRODUT" , MVC_VIEW_ORDEM, '04')
	EndIf
	If GYD->(FieldPos("GYD_LOCINI")) > 0
		oStruGYD:SetProperty("GYD_LOCINI" , MVC_VIEW_ORDEM, '05')
	EndIf
	If GYD->(FieldPos("GYD_LOCFIM")) > 0
		oStruGYD:SetProperty("GYD_LOCFIM" , MVC_VIEW_ORDEM, '06')
	EndIf

	If GQI->(FieldPos("GQI_CODIGO")) > 0
		oStruGQI:SetProperty("GQI_CODIGO", MVC_VIEW_ORDEM, '01')
	EndIf
	If GQI->(FieldPos("GQI_SEQ")) > 0
		oStruGQI:SetProperty("GQI_SEQ"   , MVC_VIEW_ORDEM, '02')
	EndIf
	If GQI->(FieldPos("GQI_ITEM")) > 0
		oStruGQI:SetProperty("GQI_ITEM"  , MVC_VIEW_ORDEM, '03')
	EndIf
	If GQI->(FieldPos("GQI_CODORI")) > 0
		oStruGQI:SetProperty("GQI_CODORI", MVC_VIEW_ORDEM, '04')
	EndIf
	If GQI->(FieldPos("GQI_DESORI")) > 0
		oStruGQI:SetProperty("GQI_DESORI", MVC_VIEW_ORDEM, '05')
	EndIf
	If GQI->(FieldPos("GQI_CODDES")) > 0
		oStruGQI:SetProperty("GQI_CODDES", MVC_VIEW_ORDEM, '06')
	EndIf
	If GQI->(FieldPos("GQI_DESDES")) > 0
		oStruGQI:SetProperty("GQI_DESDES", MVC_VIEW_ORDEM, '07')
	EndIf
	If GQI->(FieldPos("GQI_TIPLOC")) > 0
		oStruGQI:SetProperty("GQI_TIPLOC", MVC_VIEW_ORDEM, '99')
	EndIf

	
	If GYX->(FieldPos("GYX_CODIGO")) > 0
		oStruGYX:SetProperty("GYX_CODIGO" , MVC_VIEW_ORDEM, '01')
	EndIf
	If GYX->(FieldPos("GYX_SEQ")) > 0
		oStruGYX:SetProperty("GYX_SEQ"    , MVC_VIEW_ORDEM, '02')
	EndIf
	If GYX->(FieldPos("GYX_ITEM")) > 0
		oStruGYX:SetProperty("GYX_ITEM" , MVC_VIEW_ORDEM, '03')
	EndIf
	If GYX->(FieldPos("GYX_CODGIM")) > 0
		oStruGYX:SetProperty("GYX_CODGIM" , MVC_VIEW_ORDEM, '04')
	EndIf
	If GYX->(FieldPos("GYX_DCUSTO")) > 0
		oStruGYX:SetProperty("GYX_DCUSTO" , MVC_VIEW_ORDEM, '05')
	EndIf
	If GYX->(FieldPos("GYX_FORMUL")) > 0
		oStruGYX:SetProperty("GYX_FORMUL" , MVC_VIEW_ORDEM, '06')
	EndIf
	If GYX->(FieldPos("GYX_TIPO")) > 0
		oStruGYX:SetProperty("GYX_TIPO"   , MVC_VIEW_ORDEM, '07')
	EndIf
	If GYX->(FieldPos("GYX_CUSUNI")) > 0
		oStruGYX:SetProperty("GYX_CUSUNI" , MVC_VIEW_ORDEM, '08')
	EndIf
	If GYX->(FieldPos("GYX_UM")) > 0
		oStruGYX:SetProperty("GYX_UM"     , MVC_VIEW_ORDEM, '09')
	EndIf
	If GYX->(FieldPos("GYX_DESUM")) > 0
		oStruGYX:SetProperty("GYX_DESUM"     , MVC_VIEW_ORDEM, '10')
	EndIf
	If GYX->(FieldPos("GYX_QUANT")) > 0
		oStruGYX:SetProperty("GYX_QUANT"  , MVC_VIEW_ORDEM, '11')
	EndIf
	If GYX->(FieldPos("GYX_VALTOT")) > 0
		oStruGYX:SetProperty("GYX_VALTOT" , MVC_VIEW_ORDEM, '12')
	EndIf

	If GQZ->(FieldPos("GQZ_CODIGO")) > 0
		oStruGQZ:SetProperty("GQZ_CODIGO", MVC_VIEW_ORDEM, '01')
	EndIf
	If GQZ->(FieldPos("GQZ_SEQ")) > 0
		oStruGQZ:SetProperty("GQZ_SEQ"   , MVC_VIEW_ORDEM, '02')
	EndIf
	If GQZ->(FieldPos("GQZ_ITEM")) > 0
		oStruGQZ:SetProperty("GQZ_ITEM"  , MVC_VIEW_ORDEM, '03')
	EndIf

RestArea(aArea)
Return 

/*/{Protheus.doc} GA900Inc
(long_description)
@type  Static Function
@author Teixeira
@since 12/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function GA900Inc(oView)
Local oModel	:= oView:GetModel()
Local n1        := 0
Local n2        := 0
Local aFldsGQ8  := oModel:GetModel('GQ8DETAIL'):GetStruct():GetFields()
Local aFldsGQB  := oModel:GetModel('GQBDETAIL'):GetStruct():GetFields()

Private oMdl901

GTPA901ORC(oModel)

If oMdl901:IsActive()

	For n1 := 1 To Len(aFldsGQ8)
		oModel:GetModel('GQ8DETAIL'):LoadValue(aFldsGQ8[n1][3], oMdl901:GetModel('GQ8MASTER'):GetValue(aFldsGQ8[n1][3]))
	Next

	oModel:GetModel('GQBDETAIL'):DelAllLine()

	For n2 := 1 To oMdl901:GetModel('GQBDETAIL'):Length()

		If !Empty(oModel:GetModel('GQBDETAIL'):GetValue('GQB_ITEM')) 
			oModel:GetModel('GQBDETAIL'):AddLine()
		Endif

		For n1 := 1 To Len(aFldsGQB)
			oModel:GetModel('GQBDETAIL'):LoadValue(aFldsGQB[n1][3], oMdl901:GetModel('GQBDETAIL'):GetValue(aFldsGQB[n1][3], n2))
		Next

	Next

	oModel:GetModel('GQ8DETAIL'):LoadValue('GQ8_CODGY0', oModel:GetModel('GYDDETAIL'):GetValue('GYD_NUMERO'))
	oModel:GetModel('GQ8DETAIL'):LoadValue('GQ8_CODGYD', oModel:GetModel('GYDDETAIL'):GetValue('GYD_CODGYD'))

	oMdl901:DeActivate()
	oMdl901:Destroy()

Endif

Return nil

/*/{Protheus.doc} TrigGIMPrd
Preenche grid da GIM com as tabelas de custos conforme o produto informado
@type function
@author jacomo.fernandes
@since 19/07/2018
@version 1.0
@param oMdlG6S, objeto, (Descri��o do par�metro)
@param cProduto, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function TrigGIMPrd(oMdl, cProduto, cCodigo)
Local cAliasTmp	:= GetNextAlias()
Local cWhere	:= ""
Local lTrgGQJ	:= .F. //Se vier pelo gatilho do campo GQJ_CODGIM, n�o permitir que inclua linha.
Local cTable	:= oMdl:GetStruct():GetTable()[1]
Default cCodigo	:= ""

If GIM->(FieldPos('GIM_UTILIZ')) > 0
	cWhere := " AND GIM.GIM_UTILIZ = '2' "
Endif

If !Empty(cCodigo)
	cWhere	:= " AND GIM.GIM_COD = '"+cCodigo+"' "
	lTrgGQJ := .T.
Endif

cWhere := "%"+cWhere+"%"

BeginSql Alias cAliasTmp
	SELECT 
		GIM.GIM_COD AS CODGIM,  
		GIM.GIM_DESCRI AS DCUSTO, 
		GIM.GIM_UM AS UN, 
		SAH.AH_UMRES AS DESCUN,
		(CASE 
			WHEN GIM.GIM_TPCUST = '1' 
				THEN '2'
			ELSE '1' 
		END) AS FORMUL, 
		GIM.GIM_CBASE AS CUSUNI,
		(CASE 
			WHEN GIM.GIM_TPCUST = '1' 
				THEN GIM.GIM_CBASE 
			ELSE 0 
		END) AS VALTOT
	FROM %Table:GIM% GIM
		INNER JOIN %Table:SAH% SAH ON
			SAH.AH_FILIAL = %xFilial:SAH%
			AND SAH.AH_UNIMED = GIM.GIM_UM
			AND SAH.%NotDel%
		
	WHERE 
		GIM_FILIAL = %xFilial:GIM%
//		AND GIM_PRODUT = %Exp: cProduto %

		AND GIM.%NotDel%
		
		%Exp: cWhere %
		
	Order By GIM.GIM_FILIAL, GIM.GIM_COD
EndSql

If (cAliasTmp)->(!EoF())
	While (cAliasTmp)->(!EoF())
		
		If !lTrgGQJ .and. (!oMdl:IsEmpty() .or. !Empty(oMdl:GetValue('GQJ_CODGIM')))
			oMdl:AddLine()
		Endif
		
		oMdl:LoadValue(cTable+'_CODGIM'	,(cAliasTmp)->CODGIM	)
		oMdl:LoadValue(cTable+'_DCUSTO'	,(cAliasTmp)->DCUSTO	)
		oMdl:LoadValue(cTable+'_FORMUL'	,(cAliasTmp)->FORMUL	)
		oMdl:LoadValue(If (cTable == 'GQJ','GQJ_UN','GYX_UM'), 	  (cAliasTmp)->UN	)
		oMdl:LoadValue(If (cTable == 'GQJ','GQJ_UN','GYX_DESUM'), (cAliasTmp)->UN	)
		oMdl:LoadValue(cTable+'_QUANT'	, 1						)
		oMdl:LoadValue(cTable+'_CUSUNI'	,(cAliasTmp)->CUSUNI	)
		oMdl:LoadValue(cTable+'_VALTOT'	,(cAliasTmp)->VALTOT	)
		oMdl:LoadValue(cTable+'_PLAN'	,Posicione('GIM',1,xFilial('GIM')+(cAliasTmp)->CODGIM,'GIM_PLAN') )

		(cAliasTmp)->(DbSkip())
	End 
Endif

(cAliasTmp)->(DbCloseArea())

Return

/*/{Protheus.doc} GA900Calc
(long_description)
@type function
@author jacomo.fernandes
@since 20/07/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function GA900Calc(oView)
Local oModel			:= oView:GetModel()
Local oMdlGY0			:= oModel:GetModel('GY0MASTER')
Local oMdlGQI			:= oModel:GetModel('GQIDETAIL')
Local oMdlGQJ			:= oModel:GetModel('GQJDETAIL')
Local oMdlGYX			:= oModel:GetModel('GYXDETAIL')
Local oMdlGQZ			:= oModel:GetModel('GQZDETAIL')
Local oWorkSheet		:= FWUIWorkSheet():New(/*oWinPlanilha*/,.F. , /*WS_ROWS*/, /*WS_COLS*/)
Local nCell				:= 0
Local n1				:= 0
Local nValue			:= 0

oWorkSheet:lShow := .F.

For n1 := 1 to oMdlGQJ:Length()
	If !oMdlGQJ:IsDeleted(n1) .and. oMdlGQJ:GetValue('GQJ_FORMUL',n1) == '1'
		oMdlGQJ:GoLine(n1)
		oWorkSheet:LoadXmlModel(oMdlGQJ:GetValue('GQJ_PLAN'))
		
		For nCell := 2 To oWorkSheet:NTOTALLINES			
			If oWorkSheet:CellExists("A"+ cValTochar(nCell))
				
				cCellValue	:= oWorkSheet:GetCellValue("A"+ cValTochar(nCell))
				cCellValue	:= AllTrim(cCellValue) 
				
				Do Case
					Case oMdlGY0:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGY0:GetValue(cCellValue))
					Case oMdlGQI:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQI:GetValue(cCellValue))
					Case oMdlGQJ:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQJ:GetValue(cCellValue))
					Case oMdlGYX:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGYX:GetValue(cCellValue))
					Case oMdlGQZ:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQZ:GetValue(cCellValue))
				EndCase

			EndIf
		Next
		
		If oWorkSheet:CellExists("D2") 	
			nValue := oWorkSheet:GetCellValue("D2")
		EndIf
				
		oMdlGQJ:SetValue('GQJ_CUSUNI',nValue)
		
		oWorkSheet:Close()
		
	Endif
Next

For n1 := 1 to oMdlGYX:Length()
	If !oMdlGYX:IsDeleted(n1) .and. oMdlGYX:GetValue('GYX_FORMUL',n1) == '1'
		oMdlGYX:GoLine(n1)
		oWorkSheet:LoadXmlModel(oMdlGYX:GetValue('GYX_PLAN'))
		
		For nCell := 2 To oWorkSheet:NTOTALLINES			
			If oWorkSheet:CellExists("A"+ cValTochar(nCell))
				
				cCellValue	:= oWorkSheet:GetCellValue("A"+ cValTochar(nCell))
				cCellValue	:= AllTrim(cCellValue) 
				
				Do Case
					Case oMdlGY0:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGY0:GetValue(cCellValue))
					Case oMdlGQI:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQI:GetValue(cCellValue))
					Case oMdlGQJ:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQJ:GetValue(cCellValue))
					Case oMdlGYX:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGYX:GetValue(cCellValue))
					Case oMdlGQZ:GetStruct():HasField(cCellValue)
						oWorkSheet:SetCellValue("C" + cValToChar(nCell), oMdlGQZ:GetValue(cCellValue))
				EndCase

			EndIf
		Next
		
		If oWorkSheet:CellExists("D2") 	
			nValue := oWorkSheet:GetCellValue("D2")
		EndIf
				
		oMdlGYX:SetValue('GYX_CUSUNI',nValue)
		
		oWorkSheet:Close()
		
	Endif
Next

GTPDestroy(oWorkSheet)

Return

/*/{Protheus.doc} GTPA900CTR
(long_description)
@type  Function
@author Osmar Cioni
@since 19/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Function GTPA900CTR()
Local lRet		:= .T.


If GY0->GY0_STATUS != '1' 
	FwAlertWarning(STR0023, STR0020) //"Cria��o de contrato permitido apenas para registros com o status 'Em Aberto'", "Aten��o"
	Return
Else
	FwMsgRun( ,{|| lRet := G900GerCtr()},, STR0022 ) //"Gerando Contrato...." 	
Endif

Return

/*/{Protheus.doc} G900GerCtr
(long_description)
@type  Static Function
@author Osmar Cioni
@since 19/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function G900GerCtr()
Local lRet      := .T.
Local oModel    := Nil
Local cContrat  := CN300Num()
Local nX 		:= Nil
Local nY		:= Nil
Local nTotCadast:= 0
Local nTotAdic  := 0
Local nTotCustos:= 0
Local cMsgErro  := ""
Local oModelOrc	:= FwLoadModel("GTPA900")

	oModelOrc:SetOperation(MODEL_OPERATION_UPDATE) 
	oModelOrc:Activate()
 
    oModel := FWLoadModel('CNTA301')
    oModel:SetOperation(MODEL_OPERATION_INSERT)
    oModel:Activate()
     
    //Cabe�alho do Contrato  - //GY0MASTER
    oModel:SetValue('CN9MASTER','CN9_NUMERO', cContrat)
    oModel:SetValue('CN9MASTER','CN9_DTINIC', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_DTINIC'))
    oModel:SetValue('CN9MASTER','CN9_UNVIGE', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_UNVIGE'))
    oModel:SetValue('CN9MASTER','CN9_VIGE',   oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_VIGE')  )
    oModel:SetValue('CN9MASTER','CN9_MOEDA',  oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_MOEDA')) //VERIFICAR TAM/TIPO
    oModel:SetValue('CN9MASTER','CN9_TPCTO',  oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_TPCTO'))
    oModel:SetValue('CN9MASTER','CN9_CONDPG', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_CONDPG'))
    oModel:SetValue('CN9MASTER','CN9_AUTO',  '1')  
     
    //Cliente/Fornecedor do Contrato
    oModel:SetValue('CNCDETAIL','CNC_CLIENT'    , oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_CLIENT'))
    oModel:SetValue('CNCDETAIL','CNC_LOJACL'    , oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_LOJACL'))

	//Vendedor do Contrato
    oModel:SetValue('CNUDETAIL','CNU_CODVD'    , oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_CODVD'))
    oModel:SetValue('CNUDETAIL','CNU_PERCCM'  , oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_PCOMVD'))
	
    //Planilhas do Contrato
    oModel:SetValue('CNADETAIL','CNA_NUMERO', PadL("1", Len(CNA->CNA_NUMERO),"0"))
    oModel:SetValue('CNADETAIL','CNA_CLIENT', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_CLIENT'))
    oModel:SetValue('CNADETAIL','CNA_LOJACL', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_LOJACL'))
    oModel:SetValue('CNADETAIL','CNA_TIPPLA', oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_TIPPLA'))
     
	//GQZ-CUSTOS ADICIONAIS/CORTESIA
	//GQJ-Custo Cadastral
	//GYX-Custo Linha
	For nX := 1 to oModelOrc:GetModel('GQJDETAIL'):Length() //Custo Cadastral
		oModelOrc:GetModel('GQJDETAIL'):goline(nX)
		nTotCadast += oModelOrc:GetModel('GQJDETAIL'):GetValue('GQJ_VALTOT')
    Next nX 

    //Itens da Planilha do Contrato
	For nX := 1 to oModelOrc:GetModel('GYDDETAIL'):Length() //DADOS DA LINHA
		oModelOrc:GetModel('GYDDETAIL'):goline(nX)

		If nX > 1
			oModel:GetModel('CNBDETAIL'):AddLine()
		EndIf
		oModel:SetValue('CNBDETAIL','CNB_ITEM', PadL( nX, Len(CNB->CNB_ITEM),"0") )
		oModel:SetValue('CNBDETAIL','CNB_PRODUT'	,IIF(!(EMPTY(oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_PRODUT'))),oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_PRODUT'),oModelOrc:GetModel('GY0MASTER'):GetValue('GY0_PRODUT')))
		oModel:SetValue('CNBDETAIL','CNB_QUANT' 	,1)
		
		//Custo Adicional / Cortesias (GYX)
		nTotAdic:= 0
		For nY := 1 to oModelOrc:GetModel('GYXDETAIL'):Length()
			nTotAdic += oModelOrc:GetModel('GYXDETAIL'):GetValue('GYX_VALTOT')		
		Next nY	

		//CUSTOS OPERACIONAIS (GQZ)
		nTotCustos:= 0
		For nY := 1 to oModelOrc:GetModel('GQZDETAIL'):Length()
			nTotCustos += oModelOrc:GetModel('GQZDETAIL'):GetValue('GQZ_VALTOT')			
		Next nY	
		
		nTotal := nTotCustos + IIF( nTotAdic > 0 , nTotAdic, nTotCadast )
		oModel:SetValue('CNBDETAIL','CNB_VLUNIT' 	,nTotal)	

	Next nX
 
 	Begin Transaction

    If (oModel:VldData()) /*Valida o modelo como um todo*/
        oModel:CommitData()//--Grava Contrato

		If G900GerLin(oModelOrc)
			oModelOrc:SetValue('GY0MASTER','GY0_CODCN9', cContrat)	
			oModelOrc:SetValue('GY0MASTER','GY0_STATUS', '2')	
			If (oModelOrc:VldData())
				oModelOrc:CommitData()
				FwAlertSuccess(STR0021, 'Ok') //"Contrato gerado com sucesso","'Ok'
			EndIf
		Else
			lRet := .F.
			DisarmTransaction()
			Break	
		EndIf
	Else
		cMsgErro := Alltrim(oModel:GetErrorMessage()[6]) + ". " + Alltrim(oModel:GetErrorMessage()[7])
		Help(,,"G900GerCtr",, cMsgErro, 1,0)
    EndIf

	End Transaction

Return lRet

/*/{Protheus.doc} G900GerLin
(long_description)
@type  Static Function
@author Osmar Cioni
@since 19/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function G900GerLin(oModelOrc)
Local lRet 		:= .T.
Local oModel 	:= FwLoadModel("GTPA002")
Local oMdlGI2	:= Nil
Local oMdlG5I	:= Nil
Local nX		:= Nil
Local nY		:= Nil
Local cMsgErro  := ""

	For nX := 1 to oModelOrc:GetModel('GYDDETAIL'):Length() //DADOS DA LINHA
		
		oModelOrc:GetModel('GYDDETAIL'):goline(nX)
		If EMPTY(oModelOrc:GetModel('GYDDETAIL'):GetValue("GYD_LOCINI")) .AND. EMPTY(oModelOrc:GetModel('GYDDETAIL'):GetValue("GYD_LOCFIM"))
			Help(,,"G900GerLin",, "N�o foi cadastrado linha para esse or�amento", 1,0)
			lRet := .F.
			EXIT
		EndIf
		If lRet
			oModel:SetOperation(MODEL_OPERATION_INSERT)
			
			If oModel:Activate()
				oMdlGI2 := oModel:GetModel('FIELDGI2')
				oMdlG5I := oModel:GetModel('GRIDG5I')

				oMdlGI2:SetValue('GI2_COD', GETSXENUM('GI2', 'GY2_COD') )
				oMdlGI2:SetValue('GI2_ORGAO',oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_ORGAO')) 
				oMdlGI2:SetValue('GI2_PREFIX','CONTRATO')
				oMdlGI2:LoadValue('GI2_LOCINI',oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_LOCINI'))
				oMdlGI2:LoadValue('GI2_LOCFIM',oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_LOCFIM'))
				oMdlGI2:SetValue('GI2_KMIDA',oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_KMIDA'))
				oMdlGI2:SetValue('GI2_KMVOLT',oModelOrc:GetModel('GYDDETAIL'):GetValue('GYD_KMVOLT'))
				
				For nY := 1 to oModelOrc:GetModel('GQIDETAIL'):Length()+1
					oModelOrc:GetModel('GQIDETAIL'):goline(nY)
					If nY > 1
						oModel:GetModel('GRIDG5I'):AddLine()
					EndIf
					If nY > oModelOrc:GetModel('GQIDETAIL'):Length()
						oMdlG5I:SetValue('G5I_SEQ',SOMA1( oModelOrc:GetModel('GQIDETAIL'):GetValue('GQI_SEQ')) )
						oMdlG5I:SetValue('G5I_LOCALI',oModelOrc:GetModel('GQIDETAIL'):GetValue('GQI_CODDES'))
						oMdlG5I:SetValue('G5I_KM',0)	
						oMdlG5I:SetValue('G5I_VENDA','1') 
					Else
						oMdlG5I:SetValue('G5I_SEQ',oModelOrc:GetModel('GQIDETAIL'):GetValue('GQI_SEQ'))
						oMdlG5I:SetValue('G5I_LOCALI',oModelOrc:GetModel('GQIDETAIL'):GetValue('GQI_CODORI'))
						oMdlG5I:SetValue('G5I_KM',oModelOrc:GetModel('GQIDETAIL'):GetValue('GQI_KM'))	
						oMdlG5I:SetValue('G5I_VENDA','1') 
					EndIf

				Next nY

				If (oModel:VldData())
					oModel:CommitData()
					oModelOrc:GetModel('GYDDETAIL'):SetValue('GYD_CODGI2', oMdlGI2:GetValue('GI2_COD') )					
				Else
					cMsgErro := Alltrim(oModel:GetErrorMessage()[6]) + ". " + Alltrim(oModel:GetErrorMessage()[7])
					Help(,,"G900GerLin",, cMsgErro, 1,0)
					lRet := .F.
				EndIf

				oModel:DeActivate()

			Endif
		EndIf
	Next nX

Return lRet

/*/{Protheus.doc} G900GerCan
(long_description)
@type  Static Function
@author Osmar Cioni
@since 19/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Function GTPA900CAN()

If GY0->GY0_STATUS == '1' 
	FwAlertWarning(STR0019, STR0020) //"Cria��o de contrato permitido apenas para registros com o status 'Em Aberto'", "Aten��o"
	Return
Else
	FwMsgRun( ,{|| lRet := G900GerCan()},, STR0018 ) //"Cancelando Contrato...." 	
Endif

Return


/*/{Protheus.doc} G900GerCan
(long_description)
@type  Static Function
@author Osmar Cioni
@since 19/08/2020
@version 1.0
@param oView, param_type, param_descr
@return nil, return_type, return_description
@example
(examples)
@see (links_or_references)
/*/
Static Function G900GerCan()
Local cContrato := GY0->GY0_CODCN9

CN9->(DBSetOrder(1))
If CN9->( DbSeek( xFilial("CN9") + cContrato  ))//Deve se posicionar no contrato que ter� sua situa��o alterada
 
    CN100Situac('CN9',CN9->(Recno()),4, "01",.T.)//Muda a situa��o do contrato para cancelado
	
	RecLock("GY0",.F.)
	GY0->GY0_STATUS := '3'
	GY0->(MsUnlock())

EndIf

Return
