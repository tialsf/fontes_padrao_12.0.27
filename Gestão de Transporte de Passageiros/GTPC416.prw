#Include 'Protheus.ch'
#Include "FWMVCDEF.CH"
#Include "GTPC416.ch"

 //-------------------------------------------------------------------
/*/{Protheus.doc} GTPC416()

Executa o pergunte para tela de consulta das comiss�es existentes.

@return 	lRet - OK
  
@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//------------------------------------------------------------------- 
Function GTPC416()

Local cPrograma	:= 'GTPC416'
Local nOperation 	:= MODEL_OPERATION_UPDATE
Local bCancel		:= {|| TPAlterModify()  }
Local lRet			:= .T.
Local aButtons 	:={ {.F.,Nil},;                                             //  1 - Copiar
	                  {.F.,Nil},;                                             //  2 - Recortar
	                  {.F.,Nil},;                                             //  3 - Colar
	                  {.F.,Nil},;                                             //  4 - Calculadora
	                  {.F.,Nil},;                                             //  5 - Spool        
	                  {.F.,Nil},;                                             //  6 - Imprimir
	                  {.F.,Nil},;                                             //  7 - Confirmar # Salvar
	                  {.T.,STR0001},;  											//  8 - Cancelar  # Fechar
	                  {.F.,Nil},;                                             //  9 - WalkTrhough
	                  {.F.,Nil},;                                             // 10 - Ambiente
	                  {.F.,Nil},;                                             // 11 - Mashup
	                  {.F.,Nil},;                                             // 12 - Help
	                  {.F.,Nil},;                                             // 13 - Formul�rio HTML
	                  {.F.,Nil} }                                             // 14 - ECM         



	If Pergunte("GTPC416", .T.)
	
		DbSelectArea("G94")
		DbSetOrder(2) //-- G94_FILIAL+G94_AGENCI+G94_DATADE+G94_DATATE
		
		If DBSeek(xFilial("G94") + MV_PAR01 + DTOS(MV_PAR02) + DTOS(MV_PAR03)   )
	
			nRet := FWExecView(STR0002, cPrograma, nOperation, /*oDlg*/, {|| .T. } ,/*bOk*/, /*nPercReducao*/, aButtons, bCancel , /*cOperatId*/, /*cToolBar*/, /*oMdlA303 */)//--  'Consulta' 
		Else
			Help( ,, STR0003,"GTPC416",STR0004, 1, 0 )//"Aviso"//"'Nenhum registro foi encontrado. Favor verificar os par�metros.'"
			lRet := .F.
		EndIf
		
	EndIf

Return(lRet)

 //-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

Modelo de dados

@return 	oModel
  
@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//------------------------------------------------------------------- 
Static Function ModelDef()

Local oStruG94	:= FWFormStruct( 1, 'G94' )          
Local oStruG95	:= FWFormStruct( 1, 'G95' )// Recebe a Estrutura da Tabela Detalhes do C�lculo de Comiss�o de Contratos;
Local bPosValid 	:= { |oModel| TP418TudOK(oModel) }
Local aRelacao	:= {}


TPAddFields(@oStruG95, .F.)

oModel := MPFormModel():New("GTPC416",/*bPreValid*, /*bPosValid*/, /*bCommit*/, /*bCancel*/ )

oModel:SetDescription(STR0005) // "Consulta Comiss�es dos Contratos de Turismo"

// ------------------------------------------+
// ATRIBUI UM COMPONENTE PARA CADA ESTRUTURA |
// ------------------------------------------+
oModel:AddFields( 'G94MASTER',/*cOwner*/, oStruG94 )

oModel:AddGrid( 'G95DETAIL', 'G94MASTER', oStruG95, /*bLinePre*/,/* bLinePost*/, /*bPre*/ , /*bPost*/, /*{|oSubMdl| TPC416LoadGrid(oSubMdl) } */ )

oModel:GetModel('G95DETAIL'):SetOptional(.T.)

oModel:AddCalc('SOMA',  'G94MASTER', 'G95DETAIL', 'G95_VLRCOM',  'TOTCOMISSAO',  'SUM', , , STR0006)//'TOTAL'

oModel:GetModel("SOMA"):AddEvents("SOMA","TOTCOMISSAO","",{||.T.})

oModel:GetModel( 'G95DETAIL' ):SetNoInsertLine( .T. )
oModel:GetModel( 'G95DETAIL' ):SetNoUpdateLine( .T. )
oModel:GetModel( 'G95DETAIL' ):SetNoDeleteLine( .T. )

oModel:SetActivate( {|oModel| TPC416LoadGrid(oModel)   } )

Return(oModel)

 //-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

Modelo de visualiza��o

@return 	oView
  
@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------       
Static Function ViewDef()

Local oView      	:= FwFormView():New()       // Recebe o objeto da View
Local oModel     	:= FwLoadModel( "GTPC416" )	// Objeto do Model 
Local oStruG94	:= FWFormStruct( 2, 'G94', {|x|  Alltrim(x)+"|" $ "G94_AGENCI|G94_DAGENC|G94_DATADE|G94_DATATE|"} )	
Local oStruG95	:= FWFormStruct( 2, 'G95', {|x|  Alltrim(x)+"|" $ "G95_PROD|G95_DPROD|G95_VALTOT|G95_COMISS|G95_VLRCOM|"} ) 
Local oStrCalc 	:= FWCalcStruct( oModel:GetModel('SOMA') )


TPAddFields(@oStruG95, .T.)

//-- ORDENA OS CAMPOS
oStruG95:SetProperty("G95_PROD"		, MVC_VIEW_ORDEM, '01')
oStruG95:SetProperty("G95_DPROD"	, MVC_VIEW_ORDEM, '02')
oStruG95:SetProperty("G95_VEND"		, MVC_VIEW_ORDEM, '03')
oStruG95:SetProperty("G95_DVEND"	, MVC_VIEW_ORDEM, '04')
oStruG95:SetProperty("G95_VALTOT"	, MVC_VIEW_ORDEM, '05')
oStruG95:SetProperty("G95_COMISS"	, MVC_VIEW_ORDEM, '06')
oStruG95:SetProperty("G95_VLRCOM"	, MVC_VIEW_ORDEM, '07')

oStruG94:SetProperty("*"         , MVC_VIEW_CANCHANGE, .F.)
	
//-- Seta o Model para o modelo view
oView:SetModel(oModel)

//-------------------------------------------+
// ATRIBUI UM COMPONENTE PARA CADA ESTRUTURA |
//-------------------------------------------+
oView:AddField( 'VIEW_G94MASTER'	, oStruG94	, 'G94MASTER' )
oView:AddGrid ( 'VIEW_G95DETAIL'	, oStruG95	, 'G95DETAIL' )
oView:AddField( 'VIEW_CALC' 		,oStrCalc  , 'SOMA')

//-------------------------------------------+
// DEFINE EM % A DIVISAO DA TELA, HORIZONTAL |
//-------------------------------------------+
oView:CreateHorizontalBox( 'SUPERIOR'	, 20 )
oView:CreateHorizontalBox( 'MEIO'		, 70 )
oView:CreateHorizontalBox( 'INFERIOR'	, 10 )

//-------------------------------------------+
// DEFINE UM BOX PARA CADA COMPONENTE DO MVC |
//-------------------------------------------+
oView:SetOwnerView( 'VIEW_G94MASTER'	, 'SUPERIOR' )
oView:SetOwnerView( 'VIEW_G95DETAIL'	, 'MEIO' )
oView:SetOwnerView( 'VIEW_CALC'			, 'INFERIOR') // Totais

// Liga a identificacao do componente
oView:EnableTitleView ('VIEW_G94MASTER'	,STR0007)// 'Comiss�o de Contratos de Turismo'
oView:EnableTitleView ('VIEW_G95DETAIL' 	,'')// ''


Return ( oView )

 //-------------------------------------------------------------------
/*/{Protheus.doc} TPC416LoadGrid()

Faz a carga dos dados na grid
@return 	
  
@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//-------------------------------------------------------------------    
Static Function TPC416LoadGrid(oModel)

Local oSubMdl		:= oModel:GetModel('G95DETAIL')
Local cAlias		:= GetNextAlias()

Local cAgencia 	:= MV_PAR01
Local dDtIni		:= MV_PAR02
Local dDtFim		:= MV_PAR03


	BeginSQL Alias cAlias
	
		SELECT
		  G94.G94_CODIGO,
		  G95.G95_PROD,
		  SB1.B1_DESC DPROD,
		  G94.G94_VEND VEND,
		  SA3.A3_NOME DVEND,
		  G95.G95_VALTOT,
		  G95.G95_COMISS,
		  G95.G95_VLRCOM
		FROM %table:G94% G94,
		     %table:G95% G95,
		     %table:SB1% SB1,
		     %table:SA3% SA3
		WHERE G94.G94_FILIAL = %xfilial:G94%
		AND G95.G95_FILIAL	= %xfilial:G95%
		AND SB1.B1_FILIAL 	= %xfilial:SB1%
		AND SA3.A3_FILIAL 	= %xfilial:SA3%
		AND G94.G94_CODIGO 	= G95.G95_CODG94
		AND G94.G94_SIMULA = G95.G95_SIMULA	
		AND G94.G94_SIMULA = ''	
		AND G94.G94_AGENCI 	= %EXP: cAgencia %
		AND SB1.B1_COD 		= G95.G95_PROD
		AND SA3.A3_COD 		= G94.G94_VEND
		AND G94.G94_DATADE 	= %EXP: dDtIni %
		AND G94.G94_DATATE 	= %EXP: dDtFim %
		AND G94.%notDel%  
		AND G95.%notDel%  
		AND SB1.%notDel%  
		AND SA3.%notDel%  
		ORDER BY  G95.G95_PROD, G94.G94_VEND 
		
	EndSQL

	If (cAlias)->(!EoF())
	
		oSubMdl:SetNoInsertLine( .F. )
		oSubMdl:SetNoUpdateLine( .F. )
		oSubMdl:SetNoDeleteLine( .F. )


		While (cAlias)->(!EoF())
				
			If  !( oSubMdl:GetLine() == 1 .And. oSubMdl:IsEmpty()  )
				oSubMdl:AddLine()
			EndIf
			
			oSubMdl:SetValue('G95_CODG94'	, (cAlias)->G94_CODIGO )
			oSubMdl:SetValue('G95_PROD'		, (cAlias)->G95_PROD )
			oSubMdl:SetValue('G95_DPROD'	, (cAlias)->DPROD )
			oSubMdl:SetValue('G95_VEND'		, (cAlias)->VEND )
			oSubMdl:SetValue('G95_DVEND'	, (cAlias)->DVEND )
			oSubMdl:SetValue('G95_VALTOT'	, (cAlias)->G95_VALTOT )
			oSubMdl:SetValue('G95_COMISS'	, (cAlias)->G95_COMISS )
			oSubMdl:SetValue('G95_VLRCOM'	, (cAlias)->G95_VLRCOM )
			
	
			(cAlias)->(DbSkip())
			
		EndDo	
	
		oSubMdl:SetNoInsertLine( .T. )
		oSubMdl:SetNoUpdateLine( .T. )
		oSubMdl:SetNoDeleteLine( .T. )	

	EndIf

	(cAlias)->(DbCloseArea())

Return(.T.)




//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TPAddFields()
Adiciona Campos no cabe�alho

@sample	TPAddFields(oStruct,lView)

@param	oStruct - Objeto Struct
        lView   - Logico
  
@author	Lucas Brustolin -  Inova��o
@since		01/02/2016
@version	P12
/*/
//------------------------------------------------------------------------------------------
Static Function TPAddFields(oStruct,lView)
	
	Local aArea   	:= GetArea()
	Local bLField1	:= {|| }//{|oSubMdl| TPLoadField(oSubMdl,"G95_VEND" ) }
	Local bLField2	:= {|| }//{|oSubMdl| TPLoadField(oSubMdl,"G95_DVEND" ) }
	
	Default lView := .F.
	
	
	If !lView
		//-------------------------------+
		// lView = .T. - Estrutura Model |
		//-------------------------------+
		
		oStruct:AddField(STR0008		,;	//Descri��o (Label) do campo // 	"Vendedor"
		STR0008						,;	//Descri��o Tooltip do campo // 	"Vendedor"
		"G95_VEND"						,;	//Identificador do campo
		"C"								,;	//Tipo de dado
		TamSX3("A3_COD")[1]			,;	//Tamanho
		0								,;	//Decimal
		{||.T.}						,;	//Valid do campo
		{||.T.}						,;	//When do campo
		{}								,;	//Lista de Op��es (Combo)
		.F.								,;	//Indica se campo � obrigat�rio
		Nil								,;	//inicializador Padr�o
		.F.								,;	//Indica se o campo � chave
		.F.								,;	//Indica se o campo pode receber um valor em uma opera��o update
		.T.)								//Indica se o campo � virtual
		
		oStruct:AddField(	STR0009	,;	//Descri��o (Label) do campo // "Nome"
		STR0009 						,;	//Descri��o Tooltip do campo // "Nome"
		"G95_DVEND"					,;	//Identificador do campo
		"C"								,;	//Tipo de dado
		TamSX3("A3_NOME")[1]			,;	//Tamanho
		0								,;	//Decimal
		{||.T.}						,;	//Valid do campo
		{||.T.}						,;	//When do campo
		{}								,;	//Lista de Op��es (Combo)
		.F.								,;	//Indica se campo � obrigat�rio
		Nil								,;	//inicializador Padr�o
		.F.								,;	//Indica se o campo � chave
		.F.								,;	//Indica se o campo pode receber um valor em uma opera��o update
		.T.)								//Indica se o campo � virtual
		
	Else
	
	
		//------------------------------+
		// lView = .F. - Estrutura View |
		//------------------------------+
		
		oStruct:AddField("G95_VEND"		,;  // [01] Campo
		"03"            					,;  // [02] Ordem
		STR0008	      					,;  // [03] Titulo	 # "Vendedor"
		STR0008      						,;  // [04] Descricao # "Vendedor"
		Nil	            					,;  // [05] Help
		"GET"           					,;  // [06] Tipo do campo   COMBO, Get ou CHECK
		""									,;	// [07] Picture
											,;  // [08] PictVar
		""			 	 					,;	// [09] F3
		.F. 	 		  					,; 	// [10] Editavel
											,;  // [11] Folder
											,;	// [12] Group
											,;	// [13] Lista Combo
											,;	// [14] Tam Max Combo
											,;	// [15] Inic. Browse
		.T.     )				 				// [16] Virtual
		
		oStruct:AddField("G95_DVEND"	,;	// [01] Campo
		"04"            					,;	// [02] Ordem
		STR0009      						,;	// [03] Titulo # "Nome"
		STR0009      						,;	// [04] Descricao # "Nome"
		Nil	            					,;	// [05] Help
		"GET"           					,;	// [06] Tipo do campo   COMBO, Get ou CHECK
		""									,;	// [07] Picture
											,;	// [08] PictVar
		""			 	 					,;	// [09] F3
		.F. 	 		  					,;	// [10] Editavel
											,;	// [11] Folder
											,;	// [12] Group
											,;	// [13] Lista Combo
											,;	// [14] Tam Max Combo
											,;	// [15] Inic. Browse
		.T.     )				 				// [16] Virtual
		
	EndIF
	
Return()


 //-------------------------------------------------------------------
/*/{Protheus.doc} TPAlterModify()

Seta o Modelo de dados como n�o alterado, para inibir a msg padr�o,
pois a tela � aberta em opera��o de altera��o.
 
@return 	
  
@author	Lucas Brustolin -  Inova��o
@since		03/02/2016
@version 	P12
/*/
//------------------------------------------------------------------- 
Static Function TPAlterModify()

Local oModel := FwModelActive()

	oModel:lModify := .F.


Return(.T.)
