#Include 'Protheus.ch'

Function GTPExpECF()

Local cFilResp	:= ""
Local cMessage	:= ""

Local aResult	:= {{"GI6_FILRES"}}
Local aLogAcum	:= {}

Local lRec		:= .t.

cMessage := "Deseja efetuar a exporta��o dos registros de m�quinas Darumas? " + chr(13) 
cMessage += "Ao responder sim, o programa ir� processar e gravar os dados das m�quinas Daruma "
cMessage += "na tabela de esta��es do m�dulo SIGALOJA." + chr(13)
cMessage += "Cada exporta��o bem sucedidade ir� inativar o registro na tabela GZ2 (Cadastros de ECF)"

If ( MsgYesNo(cMessage) )
	
	CursorWait()
	
	GZ2->(DbGoTop())
	
	While ( GZ2->(!Eof()) )
		
		If ( GZ2->GZ2_MSBLQL <> "1" )
		
			If ( !Empty(GZ2->GZ2_AGENCI) )
			
				aSeek := {}
				
				aAdd(aSeek, {"GI6_CODIGO",GZ2->GZ2_AGENCI})
				
				If ( GTPSeekTable("GI6",aSeek,aResult) )
					
					If ( !Empty(aResult[2,1]) )
						
						lRec := GravaSLG(aResult[2,1])
						
						If ( !lRec )
							AddLog(aLogAcum,"N�o foi poss�vel incluir os dados na tabela de Esta��es (SLG) do m�dulo SIGALOJA. N�o h� c�digo livre",aResult[2,1])
							Exit
						Else
						
							RecLock("GZ2",.F.)
							
							GZ2->GZ2_MSBLQL := "1"
							
							GZ2->(MsUnLock())
						
						EndIf
						
					Else
						AddLog(aLogAcum,"A filial respons�vel pela Ag�ncia informada no ECF n�o est� preenchida",aResult[2,1])
					EndIf
					
				Else
					AddLog(aLogAcum,"Ag�ncia informada no ECF n�o foi encontrada.")
				EndIf
				
			Else
				AddLog(aLogAcum,"O ECF n�o possui ag�ncia atrelada a ele.")
			EndIf
		
		EndIf
		
		GZ2->(DbSkip())
	
	End While
	
	If ( Len(aLogAcum) > 0 )
		GTPShowLog(aLogAcum)
	EndIf
	
	CursorArrow()

EndIf

Return()

Static Function GravaSLG(cFilECF)

//CHAVE �NICA: LG_FILIAL+LG_CODIGO
Local cFilOld	:= cFilAnt
Local cStation	:= ""
Local lGravado	:= .t.
Local aCab		:= {} 

Private lMsErroAuto	:= .F.

cFilAnt	:= cFilECF

cStation := GetNextId(cFilECF)

If ( !Empty(cStation) )
		
	//Armazena informacoes da Estacao
	aAdd( aCab, {"LG_CODIGO"	, SubStr(GZ2->GZ2_CODIGO,4)	, Nil} )
	aAdd( aCab, {"LG_PDV"		, SubStr(GZ2->GZ2_CODIGO,4)	, Nil} )
	aAdd( aCab, {"LG_NOME"		, GZ2->GZ2_DESCRI			, Nil} )
	aAdd( aCab, {"LG_IMPFISC"	, GZ2->GZ2_SERIE			, Nil} )
	aAdd( aCab, {"LG_SERPDV"	, GZ2->GZ2_CODIGO			, Nil} )
	aAdd( aCab, {"LG_SERIE"		, cStation					, Nil} )
	aAdd( aCab, {"LG_INTCNS"	, .F.						, Nil} )
	aAdd( aCab, {"LG_GAVSTAT"	, .F.						, Nil} )
	
	Begin Transaction 
		MSExecAuto({|a,b,c,d| LOJA121(a,b,c,d)}, Nil, Nil, aCab, 3)
									
		If lMsErroAuto
			aErroAuto := GetAutoGRLog()
		
			DisarmTransaction()
		
			lGravado	:= .F. 
			Break																							 
		EndIf
	End Transaction	
Else
	lGravado := .f.
EndIf

cFilAnt	:= cFilOld

Return(lGravado) 

Static Function ExistECF()

Local lRet	:= .t.

Return(lRet)

Static Function GetNextId(cFilECF)

Local cNxtId	:= ""
Local cNxtAlias	:= ""

cNxtAlias := GetNextAlias()

BeginSQL Alias cNxtAlias

SELECT
	MAX(LG_SERIE) LG_SERIE
FROM
	(
		SELECT 
			LG_SERIE
		FROM 
			%Table:SLG% SLG
		WHERE
			LG_FILIAL = %Exp:cFilECF%
			AND SUBSTRING(LG_SERIE,1,1) = 'D'
			AND SLG.%NotDel%
	) TABELA

EndSQL

If ( !Empty((cNxtAlias)->LG_SERIE) )

	If ( Soma1((cNxtAlias)->LG_SERIE) <= "DZZ" )
		cNxtId := Soma1((cNxtAlias)->LG_SERIE)
	Else
		cNxtId := ""
	EndIf	 

Else
	cNxtId := "D00"
EndIf

Return(cNxtId)

Static Function AddLog(aLogAcum,cLogMessage,cFilECF)

Local aLog	:= {}

Default cFilECF := "N/A"

aAdd(aLog,{"ECF: ",GZ2->GZ2_CODIGO})
aAdd(aLog,{"DESCRICAO: ",GZ2->GZ2_DESCRI})
aAdd(aLog,{"AGENCIA: ",GZ2->GZ2_AGENCI})
aAdd(aLog,{"FILIAL ESTACAO: ",cFilECF})
aAdd(aLog,{"PROBLEMA: ",Alltrim(cLogMessage)})

aAdd(aLogAcum,aClone(aLog))

aLog := {}

Return()

Function GTPShowLog(aLog)

Local nI	:= 0
Local nX	:= 0	

For nI := 1 to Len(aLog)

	For nX := 1 to Len(aLog[nI])

		AutoGrLog(aLog[nI,nX,1] + Alltrim(aLog[nI,nX,2]))
	
	Next nX
	
	AutoGrLog("")
	AutoGrLog(Replicate("_",25))
	AutoGrLog("")
	
Next nI

MostraErro()

Return()