#INCLUDE "PROTHEUS.CH"
#INCLUDE "PARMTYPE.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "RPTDEF.CH"

/*/{Protheus.doc} GTPJ003
Fun��o responsavel pela cria��o do job para acertar bilhetes integrados errados
@type function
@author jacomo.fernandes
@since 31/12/2018
@version 1.0
@param aParam, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GTPJ003(aParam)

//PREPARE ENVIRONMENT EMPRESA aParam[1] FILIAL aParam[2]
	
	AcertaBilhete()
	
//RESET ENVIRONMENT

Return

Static Function AcertaBilhete()
Local aRecGZV	:= {} //Guarda o Recno da GZV para acerta-lo Dps

//Fun��o responsavel pelo Acerto do BilRef
AcertaBilRef(aRecGZV)

//Fun��o responsavel pelo acerto da viagem
AcertaViagem(aRecGZV)

//Fun��o Responsavel para atualizar o Status da GZV
AtualizaStatus(aRecGZV)

GTPDestroy(aRecGZV)

Return



/*/{Protheus.doc} AcertaBilRef
(long_description)
@type function
@author jacomo.fernandes
@since 31/12/2018
@version 1.0
@param aRecGZV, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AcertaBilRef(aRecGZV)
Local cAliasTmp	:= GetNextAlias()
Local aAreaGIC	:= GIC->(GetArea())

Local nRecGZV := 0
Local cBilRef := ""
Local cExtGIC := ""
Local cCodSrv := ""
Local cExtGYN := ""

BeginSql Alias cAliasTmp
	select 
		GIC.GIC_FILIAL,
		GIC.GIC_CODIGO,
		GIC.GIC_BILREF,
		GZV.GZV_EXTGIC,
		GIC.GIC_CODSRV,
		GZV.GZV_EXTGYN,
		GIC.R_E_C_N_O_ as GICREC,
		GZV.R_E_C_N_O_ as GZVREC,
		XXFGIC.XXF_INTVAL as INTVAL
	From %Table:GZV% GZV
		INNER JOIN %Table:GIC% GIC ON
			GIC.GIC_FILIAL+GIC.GIC_CODIGO = GZV.GZV_CHAVE
			AND GIC.GIC_BILREF = '' AND GZV.GZV_EXTGIC <> ''
			AND GIC.%NotDel%
		INNER JOIN XXF XXFGIC on
			XXFGIC.XXF_ALIAS = 'GIC'
			and XXFGIC.XXF_EXTVAL = GZV.GZV_EXTGIC
			and XXFGIC.%NotDel%
		
	WHERE 
		GZV.GZV_ALIAS = 'GIC'
		AND GZV.GZV_STATUS <> '1' 
		AND GZV.%NotDel%
	ORDER BY GZV.GZV_STATUS
EndSql

DbSelectArea('GIC')

While (cAliasTmp)->(!Eof())
	GIC->(DbGoTo((cAliasTmp)->GICREC))
	RecLock('GIC',.F.)

		GIC->GIC_BILREF := Separa((cAliasTmp)->INTVAL, '|' )[3]
		
	GIC->(MsUnLock())
	
	nRecGZV := (cAliasTmp)->GZVREC
	cBilRef := GIC->GIC_BILREF
	cExtGIC := (cAliasTmp)->GZV_EXTGIC
	cCodSrv := GIC->GIC_CODSRV
	cExtGYN := (cAliasTmp)->GZV_EXTGYN
		
	SetRecGZV(aRecGZV,nRecGZV,cBilRef,cExtGIC,cCodSrv,cExtGYN)
	
	(cAliasTmp)->(DbSkip())							
EndDo

(cAliasTmp)->(DbCloseArea())

RestArea(aAreaGIC)

Return


/*/{Protheus.doc} AcertaViagem
(long_description)
@type function
@author jacomo.fernandes
@since 31/12/2018
@version 1.0
@param aRecGZV, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AcertaViagem(aRecGZV)
Local cAliasTmp	:= GetNextAlias()
Local aAreaGIC	:= GIC->(GetArea())

Local nRecGZV := 0
Local cBilRef := ""
Local cExtGIC := ""
Local cCodSrv := ""
Local cExtGYN := ""

BeginSql Alias cAliasTmp
	select 
		GIC.GIC_FILIAL,
		GIC.GIC_CODIGO,
		GIC.GIC_CODSRV,
		GZV.GZV_EXTGYN,
		GIC.GIC_BILREF,
		GZV.GZV_EXTGIC,
		GIC.R_E_C_N_O_ as GICREC,
		GZV.R_E_C_N_O_ as GZVREC,
		XXFGYN.XXF_INTVAL as INTVAL
	From %Table:GZV% GZV
		INNER JOIN %Table:GIC% GIC ON
			GIC.GIC_FILIAL+GIC.GIC_CODIGO = GZV.GZV_CHAVE
			AND GIC.GIC_CODSRV = '' AND GZV.GZV_EXTGYN <> ''
			AND GIC.%NotDel%
		INNER JOIN XXF XXFGYN on
			XXFGYN.XXF_ALIAS = 'GYN'
			and XXFGYN.XXF_EXTVAL = GZV.GZV_EXTGYN
			and XXFGYN.%NotDel%
		
	WHERE 
		GZV.GZV_ALIAS = 'GIC'
		AND GZV.GZV_STATUS <> '1'  
		AND GZV.%NotDel%
	ORDER BY GZV.GZV_STATUS
EndSql

DbSelectArea('GIC')

While (cAliasTmp)->(!Eof())
	GIC->(DbGoTo((cAliasTmp)->GICREC))
	RecLock('GIC',.F.)

		GIC->GIC_CODSRV := Separa((cAliasTmp)->INTVAL, '|' )[3]
		
	GIC->(MsUnLock())
	
	nRecGZV := (cAliasTmp)->GZVREC
	cBilRef := GIC->GIC_BILREF
	cExtGIC := (cAliasTmp)->GZV_EXTGIC
	cCodSrv := GIC->GIC_CODSRV
	cExtGYN := (cAliasTmp)->GZV_EXTGYN
		
	SetRecGZV(aRecGZV,nRecGZV,cBilRef,cExtGIC,cCodSrv,cExtGYN)
	
	(cAliasTmp)->(DbSkip())							
EndDo

(cAliasTmp)->(DbCloseArea())

RestArea(aAreaGIC)

Return


/*/{Protheus.doc} AtualizaStatus
(long_description)
@type function
@author jacomo.fernandes
@since 31/12/2018
@version 1.0
@param aRecGZV, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AtualizaStatus(aRecGZV)
Local aAreaGZV	:= GZV->(GetArea())
Local n1		:= 0

DbSelectArea('GZV')

For n1 := 1 To Len(aRecGZV)
	GZV->(DbGoTo(aRecGZV[n1][1]))
	
	RecLock('GZV',.F.)
		
		GZV->GZV_STATUS := aRecGZV[n1][2] 
	
	GZV->(MsUnLock())
Next

RestArea(aAreaGZV)

Return


/*/{Protheus.doc} SetRecGZV
(long_description)
@type function
@author jacomo.fernandes
@since 31/12/2018
@version 1.0
@param aRecGZV, array, (Descri��o do par�metro)
@param nRecGZV, num�rico, (Descri��o do par�metro)
@param cBilRef, character, (Descri��o do par�metro)
@param cExtGIC, character, (Descri��o do par�metro)
@param cCodSrv, character, (Descri��o do par�metro)
@param cExtGYN, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetRecGZV(aRecGZV,nRecGZV,cBilRef,cExtGIC,cCodSrv,cExtGYN)
Local lOk		:= .T.
Local cStatus	:= "1" //Sucesso
Local nPos		:= 0
If (nPos := aScan(aRecGZV,{|x| x[1] == nRecGZV })) == 0
	aAdd(aRecGZV,{nRecGZV,''})
	nPos := Len(aRecGZV)
	
Endif
//Se o ExtVal estiver Preenchido e o Campo Referencia n�o, quer dizer que esta errado
If !Empty(cExtGIC) .and. Empty(cBilRef)
	lOk	:= .F.
Elseif !Empty(cExtGYN) .and. Empty(cCodSrv)
	lOk	:= .F.
Endif

If !lOk	
	cStatus := "4" //Reprocessado
Endif 

aRecGZV[nPos][2] := cStatus

Return