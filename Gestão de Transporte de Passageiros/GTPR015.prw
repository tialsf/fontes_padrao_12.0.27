#include 'PROTHEUS.CH'
#include 'PARMTYPE.CH'

//-------------------------------------------------------------------
/*/{Protheus.doc} GTPR016
Relat�rio de Rela��o de Contratos, filtro por cliente e data

@author fabio.veiga
@since 23/07/2018
@version 1.0

@type function
/*/
//-------------------------------------------------------------------
Function GTPR015()

Local cPerg := "GTPR015"
Private oReport

	If Pergunte( cPerg, .T. )
    	oReport := ReportDef( cPerg )
    	oReport:PrintDialog()
	Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef
Defini��es do Relat�rio

@author fabio.veiga
@since 23/07/2018
@version 1.0

@type function
/*/
//-------------------------------------------------------------------

Static Function ReportDef(cPerg)
Local oReport   := Nil
Local oSection1 := Nil
Local oSection2 := Nil
Local oBreak    := nil	
Local oBreak2	:= nil
Local oBreak3	:= nil

	oReport := TReport():New("GTPR015","Rela��o de Contratos - Por cliente e data",cPerg,{|oReport| ReportPrint(oReport)},"texto") //"Veiculo x Escala"
	oReport:nFontBody := 8
	oReport:SetTotalInLine(.T.)
	oReport:SetLeftMargin(05) 
	
    oSection1:= TRSection():New(oReport,"Dados Filial", {"AD1","SM0"}, , .F., .T.) //

	TRCell():New(oSection1, "FILIAL", "AD1", "FIL. COD" , /*Picture*/, /*Tamanho*/, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
    TRCell():New(oSection1,"M0_FILIAL","SM0","FIL. NOME",,20)
    TRCell():New(oSection1,"PROPOSTA","ADZ","CONTRATO")
    TRCell():New(oSection1,"ORIGEM","GIN",,,20)
    TRCell():New(oSection1,"DESTINO","GIN",,,20)
    TRCell():New(oSection1,"CARRO","ST9")
    TRCell():New(oSection1,"DTVIAGEM","AD1",,,10)
    TRCell():New(oSection1,"KILOMETROS","GIP","KM",,05)    
    TRCell():New(oSection1,"VALOR","ADZ",,"@E 999,999,999.99",10)
    TRCell():New(oSection1,"VLKM","GIO","R$/KM","@E 999,999,999.99",06)

    oSection1:Cell('M0_FILIAL'):SetBlock({|oTrCell|  FwFilialName(cEmpAnt,oSection1:Cell('FILIAL'):GetText()) })
    
    oBreak := TRBreak():New(oSection1,oSection1:Cell("FILIAL"),,.F.)
 	TRFunction():New(oSection1:Cell("FILIAL"),"TOTAL","COUNT",oBreak,,"@E 999999",,.F.,.F.)
 	TRFunction():New(oSection1:Cell("KILOMETROS"),"TOTAL","SUM",oBreak,,"@E 999,999,999",,.F.,.F.)
 	TRFunction():New(oSection1:Cell("VALOR"),"TOTAL","SUM",oBreak,,"@E 999,999,999.99",,.F.,.T.)
 	TRFunction():New(oSection1:Cell("VLKM"),"TOTAL","SUM",oBreak,,"@E 999,999,999.99",,.F.,.T.)
 	
Return(oReport)

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint
Fun��o respons�vel pela impress�o.

@author fabio.veiga
@since 23/07/2018
@version 1.0

@type function
/*/
//-------------------------------------------------------------------

Static Function ReportPrint(oReport)
Local oSection1     := oReport:Section("Dados Filial")
    SetQrySection(oReport)

    nKmAcumul := 0
	
    oSection1:Print()	
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SetQrySection
description
@author  fabio.veiga
@since   23/07/2018
@version P12
/*/
//-------------------------------------------------------------------
Static Function SetQrySection(oReport)

Local oSection1     := oReport:Section("Dados Filial")
Local cAliasSec1    := GetNextAlias()
    
    Pergunte( "GTPR015", .F. )
    
    oSection1:BeginQuery()
        BeginSql Alias cAliasSec1
			SELECT	AD1.AD1_FILIAL 	FILIAL, 
					ADZ.ADZ_PROPOS 	PROPOSTA,
					GI1A.GI1_DESCRI ORIGEM,
					GI1B.GI1_DESCRI DESTINO,					 
					ST9.T9_CODBEM	CARRO, 
					CONVERT (CHAR,Cast(GIN.GIN_DSAIDA as datetime), 103) DTVIAGEM,
					GIP.GIP_KMCONT 	KILOMETROS, 
					SUM (GIO.GIO_VALTOT) + ADZ.ADZ_PRCVEN VALOR, 
					ROUND( (SUM (GIO.GIO_VALTOT) + ADZ.ADZ_PRCVEN) / GIP.GIP_KMCONT,2) VLKM
				
			FROM %Table:AD1% AD1 (NOLOCK)
	
			INNER JOIN %Table:ADY% ADY ON (ADY.ADY_OPORTU = AD1.AD1_NROPOR)				
			
			INNER JOIN %Table:GIN% GIN ON (GIN.GIN_PROPOS = ADY.ADY_PROPOS AND GIN.GIN_ITEM = '01')
			
			INNER JOIN %Table:GI1% GI1A ON (GI1A.GI1_COD = GIN.GIN_LOCOR)
			
			INNER JOIN %Table:GI1% GI1B ON (GI1B.GI1_COD = GIN.GIN_LOCDES)			
			
			INNER JOIN %Table:GIP% GIP ON (GIP.GIP_PROPOS = ADY.ADY_PROPOS)
				
			INNER JOIN (SELECT DISTINCT(T9_CODBEM), T9_NOME, R_E_C_D_E_L_ FROM %Table:ST9%) ST9 
			ON(ST9.T9_CODBEM = GIP.GIP_CODBEM AND ST9.R_E_C_D_E_L_ = 0)			
			
			INNER JOIN %Table:GIO% GIO ON (GIO.GIO_PROPOS = ADY.ADY_PROPOS)
				
			INNER JOIN %Table:ADZ% ADZ ON (ADZ.ADZ_PROPOS = ADY.ADY_PROPOS AND ADZ.ADZ_ITEMOR = '01')				
			
			Where 	ADY.ADY_CLIENT = %exp:(MV_PAR01)%
					AND ADY.ADY_DATA = %exp:DtoS(MV_PAR02)%
					AND AD1.D_E_L_E_T_ = ''

			GROUP BY AD1.AD1_FILIAL, ADZ.ADZ_PROPOS, GI1A.GI1_DESCRI, GI1B.GI1_DESCRI, ST9.T9_CODBEM, GIN.GIN_DSAIDA , GIP.GIP_KMCONT, ADZ.ADZ_PRCVEN
        EndSql
    oSection1:EndQuery()    

Return nil
