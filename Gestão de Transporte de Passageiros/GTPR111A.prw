#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'GTPR111A.CH'
 
 //-------------------------------------------------------------------
/*/{Protheus.doc} GTPR111A()
Relat�rio de vales de presta��o de contas

@sample GTPR111A()
@return Nil

@author	Renan Ribeiro Brando -  Inova��o
@since		08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Function GTPR111A()
       Local oReport     := Nil
	   If FindFunction("TRepInUse") .And. TRepInUse()
	       // Interface de impressao
	       oReport := ReportDef()
	       oReport:PrintDialog()
	   Else
	       R3()
	   EndIf
Return
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef()

@sample ReportDef()
@return oReport - Objeto - Objeto TREPORT

@author	Renan Ribeiro Brando -  Inova��o
@since		08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportDef()
       Local oReport
       Local cAliasGQQ := GetNextAlias()
       //---------------------------------------
       // Cria��o do componente de impress�o
       //---------------------------------------
       oReport := TReport():New("GTPR111A", STR0001, "GTPR111A", {|oReport| ReportPrint(oReport, cAliasGQQ)}, STR0002 ) // #Presta��o de Contas de Vales , #Relat�rio de presta��o de contas de vale.
       oReport:SetTotalInLine(.F.)
        Pergunte("GTPR111A", .F.)
       If (Empty(MV_PAR01) .OR. Empty(MV_PAR02) .OR. Empty(MV_PAR03) .OR. Empty(MV_PAR04))
		    Help(,,"Help", "GTPR111A", STR0010, 1, 0)//"Par�metros n�o informados de forma correta."
		    Return oReport
	   EndIf
      
       oSection := TRSection():New(oReport, STR0001, "GQQ", /*{Array com as ordens do relat�rio}*/, /*Campos do SX3*/, /*Campos do SIX*/) // #Presta��o de Contas de Vales
       oSection:SetTotalInLine(.F.)

       TRCell():New(oSection, "GQQ_CODIGO", "GQQ", STR0003, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #C�digo da Presta��o
       TRCell():New(oSection, "GQQ_CODFUN", "GQQ", STR0009, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #C�digo do Funcion�rio
       TRCell():New(oSection, "GQQ_NOMFUN", "GQQ", STR0004, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Nome do Funcion�rio
       TRCell():New(oSection, "GQQ_NUMVAL", "GQQ", STR0005, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #N�mero do Vale
       TRCell():New(oSection, "GQQ_DTPRES", "GQQ", STR0006, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Data de Presta��o
       TRCell():New(oSection, "GQQ_VALOR" , "GQQ", STR0007, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Valor Presta��o
       TRCell():New(oSection, "GQQ_SLDDEV", "GQQ", STR0008, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Saldo Devedor do Vale
       
       oSection:Cell('GQQ_NOMFUN'):SetBlock({||POSICIONE('SRA',1,xFilial('SRA') + (cAliasGQQ)->GQQ_CODFUN,'RA_NOME')}) 
Return oReport
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint()

@sample ReportPrint()

@author	Renan Ribeiro Brando -  Inova��o
@since		08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportPrint(oReport, cAliasGQQ)
       Local oSection := oReport:Section(1)
       Local cParam1     := cValToChar(MV_PAR01) // Funcion�rios de?
       Local cParam2     := cValToChar(MV_PAR02) // Funcion�rios at�?
       Local cParam3     := DTOS(MV_PAR03) // Data de presta��o de?
       Local cParam4     := DTOS(MV_PAR04) // Data de presta��o at�?
       //---------------------------------------
       // Query do relat�rio da secao 1
       //---------------------------------------
       oSection:BeginQuery()
          BeginSQL Alias cAliasGQQ
             SELECT *
                FROM %table:GQQ% GQQ
                WHERE 
              		GQQ.GQQ_FILIAL = %xFilial:GQQ%
              		AND GQQ.%NotDel%   
                    AND GQQ.GQQ_CODFUN BETWEEN %Exp:cParam1% AND %Exp:cParam2%
                    AND GQQ.GQQ_DTPRES BETWEEN %Exp:cParam3% AND %Exp:cParam4%   
          EndSQL
         oSection:EndQuery()
oSection:Print()
Return
