#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'GTPR112.CH'

 //-------------------------------------------------------------------
/*/{Protheus.doc} GTPR112()
Relat�rio de prorroga��es de vigencia 

@sample GTPR112()

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Function GTPR112()
       Local oReport     := Nil
       If FindFunction("TRepInUse") .And. TRepInUse()
             // Interface de impressao
             oReport := ReportDef()
             oReport:PrintDialog()
       Else
             R3()
       EndIf
Return
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef()

@sample ReportDef()
@return oReport - Objeto - Objeto TREPORT

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportDef()
       Local oReport
       Local cAliasGQU := GetNextAlias()
       //---------------------------------------
       // Cria��o do componente de impress�o
       //---------------------------------------
       oReport := TReport():New("GTPR112", STR0001, "GTPR112", {|oReport| ReportPrint(oReport, cAliasGQU)}, "" ) // #Prorroga��es de Vig�ncia de Vales, #Relat�rio de prorroga��o de vig�ncia de vales de funcion�rios.
       oReport:SetTotalInLine(.F.)
       Pergunte("GTPR112", .F.)
       If (Empty(MV_PAR01) .OR. Empty(MV_PAR02) .OR. Empty(MV_PAR03) .OR. Empty(MV_PAR04))
		    Help(,,"Help", "GTPR112", STR0011, 1, 0)//"Par�metros n�o informados de forma correta."
		    Return oReport
	 EndIf
      
       oSection := TRSection():New(oReport, STR0001, "GQU", /*{Array com as ordens do relat�rio}*/, /*Campos do SX3*/, /*Campos do SIX*/) // "Prorroga��es de Vig�ncia de Vales"
       oSection:SetTotalInLine(.F.)

       TRCell():New(oSection, "GQU_CODIGO", "GQU", STR0003, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #C�digo da Prorroga��o
       TRCell():New(oSection, "GQU_CODFUN", "GQU", STR0010, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #C�digo do Funcion�rio
       TRCell():New(oSection, "GQU_NOMFUN", "GQU", STR0004, /*Picture*/, /*Tamanho*/, /*lPixel*/, {||POSICIONE('SRA',1,xFilial('SRA') + (cAliasGQU)->GQU_CODFUN,'RA_NOME')}) // #Nome do Funcion�rio
       TRCell():New(oSection, "GQU_NUMVAL", "GQU", STR0005, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #N�mero do Vale
       TRCell():New(oSection, "GQU_DTPROR", "GQU", STR0006, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Data da Prorroga��o
       TRCell():New(oSection, "GQU_DTANTV", "GQU", STR0007, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Vig�ncia Anterior
       TRCell():New(oSection, "GQU_DTNVVG", "GQU", STR0008, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Nova Vig�ncia
       TRCell():New(oSection, "GQU_NUMPRO", "GQU", STR0009, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #N�mero de Prorroga��o
    
Return oReport
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint()
@sample ReportPrint()

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportPrint(oReport, cAliasGQU)
       Local oSection := oReport:Section(1)
       Local cParam1  := cValToChar(MV_PAR01) // Funcion�rios de?
       Local cParam2  := cValToChar(MV_PAR02) // Funcion�rios at�?
       Local cParam3  := DTOS(MV_PAR03) // Data de cadastro da prorroga��o de?
       Local cParam4  := DTOS(MV_PAR04) // Data de cadastro da prorroga��o at�?
       Local cParam5  := DTOS(MV_PAR05) // Data da nova vig�ncia de?
       Local cParam6  := DTOS(MV_PAR06) // Data da nova vig�ncia at�?
        //---------------------------------------
       // Query do relat�rio da secao 1
       //---------------------------------------
       oSection:BeginQuery()
          BeginSQL Alias cAliasGQU
             SELECT *
                FROM %table:GQU% GQU
                WHERE 
              	GQU.GQU_FILIAL = %xFilial:GQU%
              	AND GQU.%NotDel%   
                  AND GQU.GQU_CODFUN BETWEEN %Exp:cParam1% AND %Exp:cParam2%
                  AND GQU.GQU_DTPROR BETWEEN %Exp:cParam3% AND %Exp:cParam4% 
                  AND GQU.GQU_DTNVVG BETWEEN %Exp:cParam5% AND %Exp:cParam6% 
          EndSQL
        oSection:EndQuery()
oSection:Print()
Return