#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'GTPR113.CH'

 //-------------------------------------------------------------------
/*/{Protheus.doc} GTPR113()
Relat�rio de autoriza��o de desconto de vales em folha 

@sample GTPR113()

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Function GTPR113()
       Local oReport     := Nil
       If FindFunction("TRepInUse") .And. TRepInUse()
             // Interface de impressao
             oReport := ReportDef()
             oReport:PrintDialog()
       Else
             R3()
       EndIf
Return
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef()
Imprime o recibo de um vale.

@sample ReportDef()
@return oReport - Objeto - Objeto TREPORT

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportDef()
       Local oReport
       Local cAliasG96 := GetNextAlias()
       //---------------------------------------
       // Cria��o do componente de impress�o
       //---------------------------------------
       oReport := TReport():New("GTPR113", STR0001, "GTPR113", {|oReport| ReportPrint(oReport, cAliasG96)}, STR0002 ) // #Autoriza��o de Pagamamento na Folha de Vales , #Relat�rio de autoriza��o na folha de pagamento de vales de funcion�rios.
       oReport:SetTotalInLine(.F.)
       Pergunte("GTPR113", .F.)
       If (Empty(MV_PAR01) .OR. Empty(MV_PAR02) .OR. Empty(MV_PAR03) .OR. Empty(MV_PAR04))
		    Help(,,"Help", "GTPR113", STR0011, 1, 0)//"Par�metros n�o informados de forma correta."
		    Return oReport
	 EndIf
      
       oSection := TRSection():New(oReport, STR0001, "G96", /*{Array com as ordens do relat�rio}*/, /*Campos do SX3*/, /*Campos do SIX*/) // #Autoriza��o de Pagamamento na Folha de Vales
       oSection:SetTotalInLine(.F.)

       TRCell():New(oSection, "G96_CODIGO", "G96", STR0003, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) //#C�digo da Autoriza��o
       TRCell():New(oSection, "G96_CODFUN", "G96", STR0010, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) //#C�digo do Funcion�rio
       TRCell():New(oSection, "G96_NOMFUN", "G96", STR0004, /*Picture*/, /*Tamanho*/, /*lPixel*/, {||POSICIONE('SRA',1,xFilial('SRA') + (cAliasG96)->G96_CODFUN,'RA_NOME')}) // #Nome do Funcion�rio
       TRCell():New(oSection, "G96_NUMVAL", "G96", STR0005, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #N�mero do Vale
       TRCell():New(oSection, "G96_DTAUTO", "G96", STR0006, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Data de Presta��o
       TRCell():New(oSection, "G96_PARCEL", "G96", STR0007, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Qtd. de Parcelas
       TRCell():New(oSection, "G96_CODRGB", "G96", STR0008, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Cod. Lan�amento no RH
       TRCell():New(oSection, "G96_SLDDEV", "G96", STR0009, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // #Saldo Devedor do Vale
    
Return oReport
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint()
Seleciona dados para o Relatorio de vales autorizados para desconto

@sample ReportPrint()

@author	Renan Ribeiro Brando -  Inova��o
@since	08/03/2017
@version	P12
/*/
//-------------------------------------------------------------------
Static Function ReportPrint(oReport, cAliasG96)
       Local oSection := oReport:Section(1)
       Local cParam1     := cValToChar(MV_PAR01) // Funcion�rios de?
       Local cParam2     := cValToChar(MV_PAR02) // Funcion�rios at�?
       Local cParam3     := DTOS(MV_PAR03) // Data de autoriza��o de?
       Local cParam4     := DTOS(MV_PAR04) // Data de autoriza��o at�?
       //---------------------------------------
       // Query do relat�rio da secao 1
       //---------------------------------------
       oSection:BeginQuery()
          BeginSQL Alias cAliasG96
             SELECT *
                FROM %table:G96% G96
                WHERE 
              	G96.G96_FILIAL = %xFilial:G96%
              	AND G96.%NotDel%     
                  AND G96.G96_CODFUN BETWEEN %Exp:cParam1% AND %Exp:cParam2%
                  AND G96.G96_DTAUTO BETWEEN %Exp:cParam3% AND %Exp:cParam4% 
          EndSQL
        oSection:EndQuery()
oSection:Print()
Return