#Include 'Protheus.ch'
#include 'GTPR418.CH'
//-------------------------------------------------------------------
/*/{Protheus.doc} GTPR419()
Relat�rio de Comiss�o de Colaborador 

@sample GTPR419()

@author Flavio Martins
@since 01/12/2017
@version P12
/*/
//-------------------------------------------------------------------
Function GTPR419()

Local oReport
Local cPerg  := 'GTPR419'

	If !FwIsInCallStack("GTPC415A")
		
		If !Pergunte(cPerg, .T.)
		
			Return
			
		Endif
		
	Endif
	
	oReport := ReportDef(cPerg)
	oReport:PrintDialog()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef()
Lista as comiss�es de acordo com os par�metros selecionados

@sample ReportDef(cPerg)

@param cPerg - caracter - Nome da Pergunta

@return oReport - Objeto - Objeto TREPORT

@author Flavio Martins 
@since 01/12/2017
@version P12
/*/
//-------------------------------------------------------------------
Static Function ReportDef(cPerg)

Local cTitle   := "Comiss�o de Colaboradores" 
Local cHelp    := "Gera o rel�torio de comiss�o de colaboradores"
Local cAliasQry   := GetNextAlias()
Local oReport
Local oSection1


oReport := TReport():New('GTPR419',cTitle,cPerg,{|oReport|ReportPrint(oReport,cAliasQry)},cHelp,/*lLandscape*/,/*uTotalText*/,/*lTotalInLine*/,/*cPageTText*/,/*lPageTInLine*/,/*lTPageBreak*/,/*nColSpace*/)
oReport:SetLandScape(.T.)
oReport:SetTotalInLine(.F.)

oSection1 := TRSection():New(oReport, cTitle, cAliasQry)
TRCell():New(oSection1,"GIC_COLAB",	"GIC", STR0003, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) //  "C�digo Colab."  
TRCell():New(oSection1,"GYG_NOME",		"GIC", STR0004, /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Nome do Colaborador"
TRCell():New(oSection1,"GIC_LINHA",	"GIC", "C�d. Linha", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "C�d. Linha"
TRCell():New(oSection1,"NOMELINHA",	"GIC", "Descri�ao da Linha", /*Picture*/, (TamSX3("GI1_DESCRI")[1]*2)+2/*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Descri�ao da Linha"
TRCell():New(oSection1,"GIC_TIPO",		"GIC", "Tipo de Bilhete", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Descri�ao da Linha"
TRCell():New(oSection1,"GIC_DTVEND",	"GIC", "Data da Venda", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Data da Venda"
TRCell():New(oSection1,"GIC_VALTOT",	"GIC", "Valor Total", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Valor Total"
TRCell():New(oSection1,"VALBASE",		"", "Valor Base", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Valor Base"
TRCell():New(oSection1,"GIC_PERCOM",	"", "% Comiss�o", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "% Comiss�o"
TRCell():New(oSection1,"GIC_VALCOM",	"", "Valor Comiss�o", /*Picture*/, /*Tamanho*/, /*lPixel*/, /*{|| code-block de impressao }*/) // "Valor Comiss�o"

TRFunction():New(oSection1:Cell("GIC_VALTOT"),NIL,"SUM",,,"@E 99,999,999.99",,.F.,.T.,.F.,,)
TRFunction():New(oSection1:Cell("VALBASE"),NIL,"SUM",,,"@E 99,999,999.99",,.F.,.T.,.F.,,)
TRFunction():New(oSection1:Cell("GIC_VALCOM"),NIL,"SUM",,,"@E 99,999,999.99",,.F.,.T.,.F.,,)


oSection1:SetColSpace(1,.F.)
oSection1:SetAutoSize(.F.)
//oSection1:SetLineBreak(.F.)

Return oReport

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint()

@sample ReportPrint(oReport, cAliasQry)

@param oReport - Objeto - Objeto TREPORT
	   cAliasQry  - Alias  - Nome do Alias para utiliza��o na Query

@author Flavio Martins 
@since 01/12/2017
@version P12
/*/
//-------------------------------------------------------------------
Static Function ReportPrint(oReport,cAliasQry)
Local oSection1	:= oReport:Section(1)
Local cTpRec		:= MV_PAR01
Local cColabDe	:= MV_PAR02
Local cColabAte	:= MV_PAR03
Local cDataDe		:= MV_PAR04
Local cDataAte	:= MV_PAR05
Local cExpFol		:= ""

	If MV_PAR06 == 1
		cExpFol := "%" + "GQ6.GQ6_EXPFOL IS NOT NULL" + "%"
	Else
		cExpFol := "%" + "GQ6.GQ6_EXPFOL IS NULL" + "%"
	Endif

	oSection1:BeginQuery()

	BeginSQL Alias cAliasQry
		
		SELECT	GIC.GIC_COLAB, 
				GYG.GYG_NOME,
				GYK.GYK_DESCRI,
				GIC.GIC_LINHA,
				GIC.GIC_TIPO,
				GIC.GIC_DTVEND,
				GIC.GIC_VALTOT,
				(GIC.GIC_VALTOT - GIC.GIC_VALIMP) VALBASE,
				GIC.GIC_PERCOM,
				GIC.GIC_VALCOM
		FROM %Table:GIC% GIC
		INNER JOIN %Table:GYG% GYG
			ON GYG.GYG_FILIAL = %xFilial:GYG% 
			AND GYG.GYG_CODIGO = GIC.GIC_COLAB
		INNER JOIN %Table:GYK% GYK
			ON GYK.GYK_FILIAL = %xFilial:GYK% 
			AND GYK.GYK_CODIGO = GYG.GYG_RECCOD
		INNER JOIN %Table:GQ6% GQ6
			ON GQ6.GQ6_FILIAL = %xFilial:GQ6% 
			AND GQ6.GQ6_CODIGO = GIC.GIC_CODGQ6
		WHERE
		GYG.GYG_RECCOD = %Exp:cTpRec%
		AND GIC.GIC_COLAB BETWEEN %Exp:cColabDe% AND %Exp:cColabAte% 
		AND GIC.GIC_DTVEND BETWEEN %Exp:cDataDe% AND %Exp:cDataAte%
		AND %Exp:cExpFol% 
		AND GIC.%NotDel%		
		ORDER BY 	GIC.GIC_COLAB, 
					GIC.GIC_DTVEND			
					
	EndSQL 

	oSection1:EndQuery()

	oSection1:Print()

Return
