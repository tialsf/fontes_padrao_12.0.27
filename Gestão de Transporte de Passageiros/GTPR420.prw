#Include 'Protheus.ch'
#INCLUDE 'GTPR420.CH'
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPR420
Gera��o Mapa Movimento Metropolitano
@type function
@author crisf
@since 02/12/2017
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
Function GTPR420()

	Local lProcessa := .T.
	Private oReport
			
		//If !TRepInUse()
			
		//	Alert(STR0001)//"A impress�o em TREPORT dever� estar habilitada. Favor verificar o par�metro MV_TREPORT."
		//	lProcessa := .F.
	
		//EndIf
	
		If lProcessa
	
			oReport:= ReportDef()
			
			if oReport <> Nil
			
				oReport:PrintDialog()
	
			EndIf
			
		EndIf

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ReportDef
(long_description)
@type function
@author crisf
@since 02/12/2017
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
Static Function ReportDef()
	
	Local cPerg		:= "GTPR420"
	Local cTitulo	:= STR0002//"Mapa Movimento Metropolitano"
	Local cDescrRel	:= STR0003//"Listar� a Linhas versus movimenta��es."
	Local aDiasPer	:= {}
	Local cDiaUtil	:= ""
	Local cSabados	:= ""
	Local cDomFer	:= ""	
	Local oReport	
	Local cTmpGI2	:= GetNextAlias()
	Local cAnoMes	:= ''
		
		oReport:= TReport():New(cPerg+"_"+StrTran(Time(),":",""), cTitulo, cPerg, {|oReport| ReportPrint( oReport, aDiasPer, cDiaUtil, cSabados, cDomFer, cTmpGI2, cAnoMes )}, cDescrRel)
		oReport:SetLandscape()
		oReport:HideParamPage()
		
		if Pergunte(oReport:uParam,.T.)
			
			//Mes/Ano
			If Empty(MV_PAR01) .OR.  Empty(MV_PAR02) 
				
				Help(,,"Help", cPerg+"MESANO", STR0004, 1, 0)// "Verificar se o M�s(MM) e o Ano(AAAA) est�o preenchidos."
				Return
				
			EndIf
			
			//Validando o M�s informado
			If MV_PAR01 < 0 .OR.  MV_PAR01 > 12
				
				Help(,,"Help", cPerg+"MES", STR0005, 1, 0)// "Informar o m�s corretamente."
				Return
				
			EndIf
		
			//Validando o Ano informado
			If MV_PAR02 < 2015
				
				Help(,,"Help", cPerg+"ANO", STR0006, 1, 0)//"Informar o ano com 4 digitos (formato AAAA) e a partir de 2015."
				Return
				
			EndIf			
			
			//Validando o preenchimento do 'Codigo da Linha' se o 'Tipo de Linha' n�o tiver preenchido.						
			If  Empty(MV_PAR03) .Or. Empty(MV_PAR04)
			
				if  Empty(MV_PAR05)
								
					Help(,,"Help", cPerg+"LINHA", , 1, 0)//"Deve-se informar pelo menos o c�digo da Linha."
					Return
				
				EndIf
					
			EndIf
				
			//Carrega os dias do m�s/ano informado, separando por dias ut�is, s�bado, domingo/feriado
			CarreDias( @aDiasPer, @cDiaUtil, @cSabados,  @cDomFer )
			cAnoMes	:= StrZero(MV_PAR02,4)+StrZero(MV_PAR01,2)
			
			//Pesquisa os dados da linha
			PesqGI2( @cTmpGI2, cAnoMes, cDiaUtil, cSabados, cDomFer)
			
			if !(cTmpGI2)->(Eof())
				
				Return(oReport)
		
			Else
			
				Help(,,"Help", cPerg, STR0008 , 1, 0)//"Para os parametros informados n�o existem dados."
				Return
				
			EndIf
			
		Else
		
			Alert( STR0009 )//"Cancelado pelo usu�rio"
			
		EndIf
				
Return 
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} CarreDias
(long_description)
@type function
@author jacomo.fernandes
@since 26/06/2018
@version 1.0
@param aDiasPer, array, (Descri��o do par�metro)
@param cDiaUtil, character, (Descri��o do par�metro)
@param cSabados, character, (Descri��o do par�metro)
@param cDomFer, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
//------------------------------------------------------------------------------------------
Static Function CarreDias( aDiasPer, cDiaUtil,  cSabados,  cDomFer )
 	
 	Local dDtBase	:= Ctod('01/'+Str(MV_PAR01)+"/"+Str(MV_PAR02))
 	Local dDtIni	:= dDtBase
	Local dDtFim	:= Ctod(StrZero(last_day(dDtBase),2)+'/'+Str(MV_PAR01)+"/"+Str(MV_PAR02))
	Local dDatI		:= Ctod('//')
	Local aDtFeria	:= {}
	
	Default cDiaUtil:= ""
	Default cSabados:= ""
	Default cDomFer	:= "" 
    //Carrega os feriados		
	aDtFeria	:=  GTPxGetFer( dDtIni, dDtFim )
	 
 	For dDatI := dDtIni to dDtFim
		If Dow(dDatI) == 1 .or. aScan(aDtFeria,{|x| x[1] == DToS(dDatI) }) > 0 // Domingo ou Feriado
			cDomFer	+= "'"+Dtos(dDatI)+"',"
		Elseif Dow(dDatI) == 7 //Sabado
			cSabados	+= "'"+Dtos(dDatI)+"',"
		Else //Dia Ultil
			cDiaUtil	+= "'"+Dtos(dDatI)+"',"
		Endif	
		
		aAdd( aDiasPer, StrZero(Day(dDatI),2) )
		
 	Next dDatI
	cDomFer	 := "%"+SubStr(cDomFer	,1,Len(cDomFer	)-1)+"%"
	cSabados := "%"+SubStr(cSabados ,1,Len(cSabados )-1)+"%"
	cDiaUtil := "%"+SubStr(cDiaUtil ,1,Len(cDiaUtil )-1)+"%"
Return 
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ReportPrint
(long_description)
@type function
@author crisf
@since 02/12/2017
@version 1.0
@param oReport, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
Static Function ReportPrint( oReport, aDiasPer, cDiaUtil, cSabados, cDomFer, cTmpGI2, cAnoMes )

	Local nLnIni	:= 250
	Local nColIni	:= 010
	Local nLnFim	:= 250
	Local nFator	:= 2680 / Len(aDiasPer)  // 68//taamnho da celula referente ao dia
	Local nColFim	:= 3180 //((len(aDiasPer))*nFator)+1100 //75+75+30+100+170//DIAS+SERVI�O+HORARIO+CAP+MEDIA+TOTAL
	Local nDias		:= 0								
	Local oArial10N	:= TFont():New("Arial",10,10,,.T.,,,,.T.,.F.)	// Negrito	
	Local oAria808N	:= TFont():New("Arial",08,08,,.T.,,,,.T.,.F.)	// Negrito
	Local oAria707	:= TFont():New("Arial",07,07,,.T.,,,,.T.,.F.)	// Negrito
	Local oAria707N	:= TFont():New("Arial",07,07,,.T.,,,,.T.,.F.)	// Negrito
	Local oArial10	:= TFont():New("Arial",10,10,,.F.,,,,.T.,.F.)	// Normal
	Local oArial07	:= TFont():New("Arial",07,07,,.F.,,,,.T.,.F.)	// Normal
	Local oArial08	:= TFont():New("Arial",08,08,,.F.,,,,.T.,.F.)	// Normal
	local nColAux	:= 0
	Local cChFilLn	:= ''
	Local cChLnSrv	:= ''	
	Local nTtIda	:= 0
	Local nTtVolta	:= 0
	Local cConteu	:= ''
	Local nMedia	:= 0
	Local nTtMes	:= 0
	Local cTmpVeic	:= GetNextAlias()
	Local cTmpGIC	:= GetNextalias()
	Local aTotais	:= {}
	Local nTtGeral	:= 0
	Local cDescLinha	:= ""
	Local nCountDay := 0
	Local ntotMed		:= 0
	Local nSent			:= 0
	Local cChvNumL		:= ''
	Local cChave		:= ''
	Local aTotPer		:= {} 
	Local cNumLin		:= ''
	Local cFilLin		:= ''
	Local nIdaDU		:= 0
	Local nIdaSA		:= 0
	Local nIdaDO		:= 0
	Local nVolDU		:= 0
	Local nVolSA		:= 0
	Local nVolDO		:= 0
	Local nOpLin		:= 0
	Local cTotais		:= ''
	Local nContador		:= 0
	Local aTotIda		:= 0
	Local aTotVolta		:= 0
	
		(cTmpGI2)->(dbGotop())
		While !(cTmpGI2)->(Eof())
			If Alltrim(cFilLin) + Alltrim(cNumLin) == Alltrim((cTmpGI2)->GYN_FILIAL) + Alltrim((cTmpGI2)->GI2_NUMLIN)
				nIdaDU		+=	(cTmpGI2)->DIAUTIL_IDA
				nIdaSA		+=  (cTmpGI2)->SABADO_IDA
				nIdaDO		+= 	(cTmpGI2)->DOMFER_IDA
				nVolDU		+=	(cTmpGI2)->DIAUTIL_VOLTA
				nVolSA		+=	(cTmpGI2)->SABADO_VOLTA
				nVolDO		+=	(cTmpGI2)->DOMFER_VOLTA
			Else
				If !Empty(cNumLin)
					aAdd( aTotPer, {cFilLin,cNumLin,nIdaDU,nIdaSA,nIdaDO,nVolDU,nVolSA,nVolDO} )
					cFilLin		:=  (cTmpGI2)->GYN_FILIAL
					cNumLin 	:=  (cTmpGI2)->GI2_NUMLIN
				Else
					cFilLin		:=  (cTmpGI2)->GYN_FILIAL
					cNumLin 	:=  (cTmpGI2)->GI2_NUMLIN
				Endif
				nIdaDU		:= 	(cTmpGI2)->DIAUTIL_IDA
				nIdaSA		:=  (cTmpGI2)->SABADO_IDA
				nIdaDO		:= 	(cTmpGI2)->DOMFER_IDA
				nVolDU		:=	(cTmpGI2)->DIAUTIL_VOLTA
				nVolSA		:=	(cTmpGI2)->SABADO_VOLTA
				nVolDO		:=	(cTmpGI2)->DOMFER_VOLTA
			EndIf
			
			(cTmpGI2)->(dbSkip())
		End
		aAdd( aTotPer, {cFilLin,cNumLin,nIdaDU,nIdaSA,nIdaDO,nVolDU,nVolSA,nVolDO} )
		(cTmpGI2)->(dbGotop())
		While !(cTmpGI2)->(Eof())
			
			If cChave <> (cTmpGI2)->GYN_FILIAL+(cTmpGI2)->GI2_NUMLIN 
				If !Empty(cChave)
				 	oReport:Finish()
				 	oReport:EndPage()
				Endif 
				oReport:StartPage()	
				nLnIni		:= 250
				nLnFim		:= 250
				nTtGeral	:= 0
				ntotMed		:= 0
				aSize(aTotais,0)
				aTotais		:= array(len(aDiasPer))
				cChave := (cTmpGI2)->GYN_FILIAL+(cTmpGI2)->GI2_NUMLIN	
			Endif
			if cChFilLn	<> (cTmpGI2)->GYN_FILIAL+(cTmpGI2)->GI2_NUMLIN  +(cTmpGI2)->GYN_LINCOD 
				If cChvNumL == (cTmpGI2)->GI2_NUMLIN
					nSent++
					If nLnIni > 900
						oReport:Finish()
						oReport:StartPage()	
						nLnIni		:= 250
						nLnFim		:= 250
					Endif
					nTtGeral	:= 0
					ntotMed		:= 0
					aSize(aTotais,0)
					aTotais		:= array(len(aDiasPer))
					cChvNumL := (cTmpGI2)->GI2_NUMLIN
				Else
					nSent := 1
					If nLnIni > 900
						oReport:Finish()
						oReport:StartPage()	
						nLnIni		:= 250
						nLnFim		:= 250
					Endif
					nTtGeral	:= 0
					ntotMed		:= 0
					aSize(aTotais,0)
					aTotais		:= array(len(aDiasPer))
					cChvNumL := (cTmpGI2)->GI2_NUMLIN
				Endif
				cChFilLn	:= (cTmpGI2)->GYN_FILIAL+(cTmpGI2)->GI2_NUMLIN +(cTmpGI2)->GYN_LINCOD 
			 	
				//Pesquisa os veiculos utilizados
				PesqVeic(@cTmpVeic, cAnoMes, cDiaUtil, cSabados, cDomFer, (cTmpGI2)->GYN_LINCOD )
				
				//Carrega os dados dos bilhetes 
				PesqGIC( @cTmpGIC, aDiasPer, cAnoMes, (cTmpGI2)->GYN_LINCOD )
					
				nOpLin := aScan(aTotPer, {|x|;
										Alltrim(x[01]) == Alltrim((cTmpGI2)->GYN_FILIAL)		;         
										.and. Alltrim(x[02]) == Alltrim((cTmpGI2)->GI2_NUMLIN)	})
				
				
					//------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------------------------------------------------------------------------------------------
					cDescLinha	:= (cTmpGI2)->GI2_NUMLIN + " "
					cDescLinha	+= AllTrim(Posicione('GI1',1, xFilial('GI1')+(cTmpGI2)->GI2_LOCINI,"GI1_DESCRI")) + " X "
					cDescLinha	+= AllTrim(Posicione('GI1',1, xFilial('GI1')+(cTmpGI2)->GI2_LOCFIM,"GI1_DESCRI")) + " "
					cDescLinha	+= "("+ AllTrim(Posicione('GQC',1, xFilial('GQC')+(cTmpGI2)->GI2_TIPLIN ,"GQC_DESCRI"))+ ")"
										
					oReport:Box(nLnIni, nColIni, nLnFim+100, nColFim)

					oReport:Say(nLnIni+30, nColIni+50, STR0010, oAria808N)//"EMPRESA"
					oReport:Say(nLnIni+30, nColIni+250, SM0->M0_FILIAL, oAria808N)
					oReport:Say(nLnIni+30, nColIni+850, STR0011, oAria808N)//"REFERENTE"
					oReport:Say(nLnIni+30, nColIni+1050, STR0012 + MesExtenso(MV_PAR01)+STR0013+StrZero(MV_PAR02,4), oAria808N)//"M�s de "##" de "##
					oReport:Say(nLnIni+30, nColIni+1450, "Prefixo " + (cTmpGI2)->GI2_PREFIX + '  ' + Substr(cDescLinha,6,Len(cDescLinha)), oAria808N)//"PREFIXO  "					

					//------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------------------------------------------------------------------------------------------

					nLnIni += 100
					nLnfim := nLnIni+270
					
					oReport:Box(nLnIni, nColIni, nLnFim, nColIni+800)
					
					oReport:Say(nLnIni+20, nColIni+50, STR0014, oAria808N)//"HORARIO"
					oReport:Say(nLnIni+20, nColIni+200, STR0015, oAria808N)//"IDA"
					oReport:Say(nLnIni+20, nColIni+300, STR0016, oAria808N)// "+"
					oReport:Say(nLnIni+20, nColIni+400, STR0017, oAria808N)//"VOLTA"
					oReport:Say(nLnIni+20, nColIni+600, STR0018, oAria808N)// "TOTAL"
					oReport:Say(nLnIni+90, nColIni+50, STR0019, oAria808N)//"D.UTEIS"
					oReport:Say(nLnIni+130, nColIni+50, STR0020, oAria808N)//"SABADOS"
					oReport:Say(nLnIni+170, nColIni+50, STR0021, oAria808N)//"DOM/FER"
					oReport:Say(nLnIni+210, nColIni+50, STR0022, oAria808N)//"M�S"
					
					//Coluna Ida						
					oReport:Say(nLnIni+90, nColIni+0200, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][3],(cTmpGI2)->DIAUTIL_IDA))), oArial08 )
					oReport:Say(nLnIni+130, nColIni+0200, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][4],(cTmpGI2)->SABADO_IDA))), oArial08 )
					oReport:Say(nLnIni+170, nColIni+0200, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][5],(cTmpGI2)->DOMFER_IDA))), oArial08 )
					nTtIda	:=  If(nOpLin > 0,(aTotPer[nOpLin][3]+aTotPer[nOpLin][4]+aTotPer[nOpLin][5]),(cTmpGI2)->DIAUTIL_IDA+(cTmpGI2)->SABADO_IDA+(cTmpGI2)->DOMFER_IDA)
					oReport:Say(nLnIni+210, nColIni+0200, Alltrim(Str(nTtIda)), oArial08 )
				
					//Coluna Volta
					oReport:Say(nLnIni+90, nColIni+0400, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][6],(cTmpGI2)->DIAUTIL_VOLTA))), oArial08 )
					oReport:Say(nLnIni+130, nColIni+0400, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][7],(cTmpGI2)->SABADO_VOLTA))), oArial08 )
					oReport:Say(nLnIni+170, nColIni+0400, Alltrim(Str(If(nOpLin > 0,aTotPer[nOpLin][8],(cTmpGI2)->DOMFER_VOLTA))), oArial08 )
					nTtVolta	:=  If(nOpLin > 0,(aTotPer[nOpLin][6]+aTotPer[nOpLin][7]+aTotPer[nOpLin][8]),(cTmpGI2)->DIAUTIL_VOLTA+(cTmpGI2)->SABADO_VOLTA+(cTmpGI2)->DOMFER_VOLTA)
					oReport:Say(nLnIni+210, nColIni+0400, Alltrim(Str( nTtVolta)), oArial08 )
					
					//Total
					oReport:Say(nLnIni+90, nColIni+0600, Alltrim(Str( If(nOpLin > 0,aTotPer[nOpLin][3]+aTotPer[nOpLin][6],(cTmpGI2)->DIAUTIL_IDA+(cTmpGI2)->DIAUTIL_VOLTA))), oArial08 )
					oReport:Say(nLnIni+130, nColIni+0600, Alltrim(Str( If(nOpLin > 0,aTotPer[nOpLin][4]+aTotPer[nOpLin][7],(cTmpGI2)->SABADO_IDA+(cTmpGI2)->SABADO_VOLTA))), oArial08 )
					oReport:Say(nLnIni+170, nColIni+0600, Alltrim(Str( If(nOpLin > 0,aTotPer[nOpLin][5]+aTotPer[nOpLin][8],(cTmpGI2)->DOMFER_IDA+(cTmpGI2)->DOMFER_VOLTA))), oArial08 )
					oReport:Say(nLnIni+210, nColIni+0600, Alltrim(Str( nTtIda+nTtVolta)), oArial08 )
					
					//["CARACTERISTICAS"]---------------------------------------------------------------------------------------------------------------
					
					oReport:Box(nLnIni, nColIni+800, nLnFim, nColIni+1400)					
					
					oReport:Say(nLnIni+20, nColIni+850, STR0023, oAria808N)//"CARACTERISTICAS"

					//"EXTENS�O"
					oReport:Say(nLnIni+90, nColIni+850, STR0024, oAria808N)
					oReport:Say(nLnIni+90, nColIni+1150,  Alltrim(Str((cTmpGI2)->GI2_KMIDA))+STR0028, oArial08)//" KM"
					
					//"TEMPO PERCURSO"
					oReport:Say(nLnIni+130, nColIni+850, STR0025, oAria808N)
					oReport:Say(nLnIni+130, nColIni+1150,  Transform((cTmpGI2)->GI2_HRPADR,"@R 99:99"), oArial08)
					
					//"PONTOS DE PARADA"
					oReport:Say(nLnIni+170, nColIni+850, STR0026, oAria808N)
					oReport:Say(nLnIni+170, nColIni+1150,  Alltrim(Str((cTmpGI2)->GI2_PONPAR)), oArial08)
					
					//"VELOCIDADE M�DIA"
					oReport:Say(nLnIni+210, nColIni+850, STR0027, oAria808N)
					oReport:Say(nLnIni+210, nColIni+1150,  Alltrim(Str((cTmpGI2)->GI2_KMMED)), oArial08)
					
					//["VEICULOS UTILIZADOS"]------------------------------------------------------------------------------------------------------------------------------------
											
					oReport:Box(nLnIni, nColIni+1400, nLnFim, nColIni+2000)											
											
					oReport:Say(nLnIni+20, nColIni+1450, STR0029, oAria808N)//"VEICULOS UTILIZADOS"
					oReport:Say(nLnIni+90, nColIni+1600, STR0030, oAria808N)//"NORMAL"
					oReport:Say(nLnIni+90, nColIni+1800, STR0031, oAria808N)//"EXTRA"
					oReport:Say(nLnIni+130, nColIni+1450, STR0019, oAria808N)//"D.UTEIS"
					oReport:Say(nLnIni+170, nColIni+1450, STR0020, oAria808N)//"SABADOS"
					oReport:Say(nLnIni+210, nColIni+1450, STR0021, oAria808N)//"DOM/FER"
				
					(cTmpVeic)->(dbGotop())
					if !(cTmpVeic)->(Eof())
						
						oReport:Say(nLnIni+130, nColIni+1600,  Alltrim(Str((cTmpVeic)->DIAUTIL_N)), oArial08 )
						oReport:Say(nLnIni+170, nColIni+1600,  Alltrim(Str((cTmpVeic)->SABADO_N)), oArial08 )
						oReport:Say(nLnIni+210, nColIni+1600,  Alltrim(Str((cTmpVeic)->DOMFER_N)), oArial08 )
						
						oReport:Say(nLnIni+130, nColIni+1800,  Alltrim(Str((cTmpVeic)->DIAUTIL_E)), oArial08 )
						oReport:Say(nLnIni+170, nColIni+1800,  Alltrim(Str((cTmpVeic)->SABADO_E)), oArial08 )
						oReport:Say(nLnIni+210, nColIni+1800,  Alltrim(Str((cTmpVeic)->DOMFER_E)), oArial08 )
					
					EndIf
					(cTmpVeic)->(dbCloseArea())		
					
					//------------------------------------------------------------------------------------------------------------------------------------
					
					oReport:Box(nLnIni, nColIni+2000, nLnFim, nColFim)
				
					oReport:Say(nLnIni+20, nColIni+2050, STR0032, oAria808N)//"SENTIDO"
					oReport:Say(nLnIni+70, nColIni+2050, If(nSent = 1, 'IDA','VOLTA'), oArial08)
					oReport:Say(nLnIni+120, nColIni+2050, "LINHA ", oAria808N) //"LINHA"
					oReport:Say(nLnIni+120, nColIni+2160, Substr(cDescLinha,1,5), oArial08 ) // numLin
					oReport:Say(nLnIni+170, nColIni+2050, Substr(cDescLinha,6,At('(',Substr(cDescLinha,6,Len(cDescLinha) ) )- 1 ), oArial08 ) //Descricao Linha
					
					//------------------------------------------------------------------------------------------------------------------------------------
					//------------------------------------------------CABE�ALHO DO SERVI�OS=HORARIOS X DIAS DO MES X QTDE---------------------------------
					
					nLnIni += 270
					
					oReport:Box(nLnIni, nColIni, nLnFim, nColFim)
					oReport:Say(nLnIni+20, nColIni+10, STR0034, oAria707N )//"HORA"
					oReport:Say(nLnIni+20, nColIni+100, "CARRO", oAria707N )//"CARRO"					
					oReport:Say(nLnIni+20, nColIni+210, STR0035, oAria707N)//"CAP"					
				
					nColAux := 210
					
					For nDias := 1 to len(aDiasPer) //Conte�do da linha HORA

						oReport:Say(nLnIni+20, nColAux += nFator, aDiasPer[nDias], oAria707N)
						
					Next nDias
						
					oReport:Say(nLnIni+20, nColAux+=75, STR0036, oAria707N )//"MEDIA"
					oReport:Say(nLnIni+20, nColAux+=105, STR0037, oAria707N )//"TOTAL"
					
					//-----------------------------------------------SERVI�OS=HORARIOS x DIAS----------------------------------------------------------------------
					
					If !(cTmpGIC)->(Eof())
					
						cChLnSrv	:= (cTmpGIC)->GIC_LINHA+(cTmpGIC)->GYN_HRINI
													
						While  !(cTmpGIC)->(Eof())
							
							nLnIni += 70
							
							oReport:Box(nLnIni, nColIni, nLnIni, nColFim)
							oReport:Box(nLnIni, nColIni, nLnIni, nColFim)

							oReport:Say(nLnIni+20, nColIni+10, Transform((cTmpGIC)->GYN_HRINI, "@R 99:99"), oAria707N )
							oReport:Say(nLnIni+20, nColIni+130,  Alltrim(Str((cTmpGIC)->CARRO)), oAria707N )	

							nCountDay := 0
							
							While cChLnSrv == (cTmpGIC)->GIC_LINHA+(cTmpGIC)->GYN_HRINI
		
								nColAux := 210
								
								For nDias := 1 to len(aDiasPer)
								
									cConteu	:= &('(cTmpGIC)->TT'+aDiasPer[nDias])
									
									oReport:Say(nLnIni+20, nColAux += nFator, Alltrim(Str(cConteu)), oArial07 )

									if Valtype(aTotais[nDias]) <> 'N'
										
										aTotais[nDias]	:= 0
											
									EndIf
									
									aTotais[nDias]	:= aTotais[nDias]+cConteu

									If cConteu > 0
										nCountDay	++
									Endif
									
								Next nDias
							
								nMedia	:= INT(((cTmpGIC)->TOTALBILH/nCountDay))//(cTmpGIC)->MEDIA
								ntotMed += nMedia
								nTtMes	:= (cTmpGIC)->TOTALBILH
								
							(cTmpGIC)->(dbSkip())
								
							EndDo
							
							cChLnSrv	:= (cTmpGIC)->GIC_LINHA+(cTmpGIC)->GYN_HRINI
							
							oReport:Say(nLnIni+20, nColAux+=85, Alltrim(Str(nMedia)), oArial07) //Conte�do do campo MEDIA
							oReport:Say(nLnIni+20, nColAux+=105, Alltrim(Str(nTtMes)), oArial07)
							
							nContador += 1
							
						EndDo
						
					EndIf
					//-------------------------------------------------------TOTAIS-------------------------------------------------------------------
					nLnIni	:= nLnIni + 75
					
					oReport:Box(nLnIni, nColIni, nLnIni, nColFim)
				//	oReport:Say(nLnIni+20, nColIni+10, 'TOTAIS', oAria808N)//STR0038 = "TOTAIS"
					oReport:Box(nLnIni+70, nColIni, nLnIni+70, nColFim)
					
					nColAux := 210
					
					For nDias := 1 to len(aTotais)
						If ValType(aTotais[nDias]) <> 'N'
							aTotais[nDias] := 0
						Endif
						cTotais := Alltrim(Str(aTotais[nDias]))
						If empty(cTotais)
							cTotais := '0'
						Endif
						
						oReport:Say(nLnIni+20, nColAux += nFator, cTotais, oAria707N)
					
						nTtGeral	:= nTtGeral+Val(cTotais)
						
					Next nDias	
					
					nTotDias	:= len(aTotais)
					
					If nSent = 1
						aTotIda	:= aClone(aTotais)
						aadd(aTotIda, ntotMed)
						aadd(aTotIda, nTtGeral)
					Else
						aTotVolta := aClone(aTotais)
						aadd(aTotVolta, ntotMed)
						aadd(aTotVolta, nTtGeral)
						aadd(aTotais,'' )
						aadd(aTotais,'' )
						For nContador := 1 to Len(aTotVolta)
							aTotais[nContador] := aTotIda[nContador] + aTotVolta[nContador]
						Next nContador
					Endif
					
					oReport:Say(nLnIni+20, nColAux += 85, Alltrim(Str(ntotMed)), oAria707N )
					oReport:Say(nLnIni+20, nColAux += 105, Alltrim(Str(nTtGeral)), oAria707N )
					
				(cTmpGIC)->(dbCloseArea())
			Endif
			
			oReport:Box(620, nColIni, nLnIni+70, nColIni)
			oReport:Box(620, nColIni+90, nLnIni+70, nColIni+90)
			oReport:Box(620, nColIni+200, nLnIni+70, nColIni+200)
			oReport:Box(620, nColIni+270, nLnIni+70, nColIni+270)
			
			nColAux := 270
								
			For nDias := 1 to len(aDiasPer)
			
				oReport:Box(620, nColAux += nFator, nLnIni+70, nColAux)
									
			Next nDias
			
			oReport:Box(620, nColAux += 100, nLnIni+70, nColAux)
			oReport:Box(620, nColFim+2, nLnIni+70, nColFim+2)
			
			(cTmpGI2)->(dbSkip())
			
			//---------------------TOTALIZADOR GERAL
			
			If nSent == 2 

				nLnIni += 100
				
				oReport:Box(nLnIni, nColIni, nLnIni, nColFim)
				oReport:Box(nLnIni, nColIni, nLnIni+60, nColIni)
				oReport:Box(nLnIni, nColIni+270, nLnIni+60, nColIni+270)
				
				oReport:Say(nLnIni+20, nColIni+10, 'TOTAL', oAria707N )
					
				nColAux := 210

				For nDias := 1 to len(aTotais)
				
					If nDias <= nTotDias
					
						oReport:Say(nLnIni+20, nColAux += nFator, AllTrim(Str(aTotais[nDias])), oAria707N)
						oReport:Box(nLnIni, nColAux+60, nLnIni+60, nColAux+60)
						
					Else

						If nDias < Len(aTotais)
							oReport:Say(nLnIni+20, nColAux += 85, AllTrim(Str(aTotais[nDias])), oAria707N)
							oReport:Box(nLnIni, nColAux + 70, nLnIni+60, nColAux + 70)
						Else
							oReport:Say(nLnIni+20, nColAux += 105, AllTrim(Str(aTotais[nDias])), oAria707N)
							oReport:Box(nLnIni, nColFim, nLnIni+60, nColFim)
						Endif
						
					Endif
					
				Next nDias
				
				oReport:Box(nLnIni+60, nColIni, nLnIni+60, nColFim)
				
			Endif
			//-----------------------			
			
		EndDo

		oReport:EndPage()
		oReport:Finish()
		oReport:lNoPrint := .F.
		(cTmpGI2)->(dbCloseArea())
		
Return
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PesqGI2
Pesquisa e retorna o resumo da(s) linha(s)
@type function
@author crisf
@since 03/12/2017
@version 1.0
@param cTmpGI2, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
Static Function PesqGI2( cTmpGI2, cAnoMes, cDiaUtil, cSabados, cDomFer)

	Local cFilGI2	:= ""
	Local cFilGI4	:= '%'+ If(MV_PAR07 <> 3," and GI4.GI4_MSBLQL = '"+STRZERO(MV_PAR07,1)+"' ",'' )+'%'
	
	cFilGI2	+= If(!Empty(MV_PAR05)	," and GI2.GI2_TIPLIN = '"+MV_PAR05+"' "	,'' )
	cFilGI2	+= If(MV_PAR06 <> 3		," and GI2.GI2_MSBLQL = '"+STRZERO(MV_PAR06,1)+"' "	,'' )
	cFilGI2	:= "%"+cFilGI2+"%" 	
		
	BeginSql Alias cTmpGI2

		SELECT 1 as ORD,GYN.GYN_FILIAL, GI2.GI2_NUMLIN,GYN.GYN_LINCOD,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%)  THEN  GYN.GYN_DTINI
			END )AS DIAUTIL_IDA,
		0 AS DIAUTIL_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%) THEN  GYN.GYN_DTINI
			END )AS DIAUTIL_TOTAL,
		
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cSabados%) THEN  GYN.GYN_DTINI
			END )AS SABADO_IDA,
		0 AS SABADO_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cSabados%) THEN  GYN.GYN_DTINI
			END )AS SABADO_TOTAL,
		
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDomFer%) THEN  GYN.GYN_DTINI
			END )AS DOMFER_IDA,
		0 AS DOMFER_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDomFer%) THEN  GYN.GYN_DTINI
			END )AS DOMFER_TOTAL,
		COUNT( GYN.GYN_DTINI )AS TOTAL_IDA,
		0 AS TOTAL_VOLTA,
		COUNT(GYN.GYN_DTINI)AS TOTAL_GERAL,			
		GI2_KMIDA GI2_KMIDA, 
		GI2_HRPADR, 
		GI2_PONPAR, 
		GI2_KMMED, 
		GI2_LOCINI,
		GI2_LOCFIM,
		GI2_TIPLIN,
		GI2_PREFIX	
		FROM %Table:GI2% GI2		       
			INNER JOIN %Table:GYN% GYN ON
				GYN.GYN_FILIAL = %xFilial:GYN%
				AND GYN.GYN_LINCOD = GI2.GI2_COD
				AND GYN.GYN_TIPO = '1'
				AND SUBSTRING(GYN.GYN_DTINI,1,6) = %exp:cAnoMes%	
				AND GYN.%NotDel%
			LEFT JOIN %table:GI4% GI4 ON
				GI4.GI4_FILIAL = GI2.GI2_FILIAL
				AND GI4.GI4_LINHA = GI2.GI2_COD
				AND GI4.GI4_LOCORI = GYN.GYN_LOCORI
				AND GI4.GI4_LOCDES = GYN.GYN_LOCDES
				AND GI4.GI4_HIST = '2'
				AND GI4.%NotDel%
				%exp:cFilGI4%
		WHERE
			GI2.GI2_FILIAL = %xFilial:GI2%
			AND GI2.GI2_NUMLIN BETWEEN %Exp:ALLTRIM(MV_PAR03)% And %Exp:ALLTRIM(MV_PAR04)%
			AND GI2.GI2_HIST = '2'
			AND GI2.%NotDel%
			AND GI2.GI2_KMIDA > 0 
			%exp:cFilGI2%
		GROUP BY GYN.GYN_FILIAL,GI2.GI2_NUMLIN, GYN.GYN_LINCOD, GI2_KMIDA, GI2_HRPADR, GI2_PONPAR, GI2_KMMED, GI2_LOCINI,GI2_LOCFIM,GI2_TIPLIN,GI2_PREFIX	
		
		UNION 
		
		SELECT 2 as ORD,GYN.GYN_FILIAL,GI2.GI2_NUMLIN, GYN.GYN_LINCOD,
		0 AS DIAUTIL_IDA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%)  THEN  GYN.GYN_DTINI
			END )AS DIAUTIL_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%) THEN  GYN.GYN_DTINI
			END )AS DIAUTIL_TOTAL,
		
		0 AS SABADO_IDA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cSabados%) THEN  GYN.GYN_DTINI
			END )AS SABADO_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cSabados%) THEN  GYN.GYN_DTINI
			END )AS SABADO_TOTAL,
		
		0 AS DOMFER_IDA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDomFer%)  THEN  GYN.GYN_DTINI
			END )AS DOMFER_VOLTA,
		COUNT(CASE 
		    WHEN GYN.GYN_DTINI IN (%exp:cDomFer%) THEN  GYN.GYN_DTINI
			END )AS DOMFER_TOTAL,
		0 AS TOTAL_IDA,
		COUNT(GYN.GYN_DTINI )AS TOTAL_VOLTA,
		COUNT(GYN.GYN_DTINI)AS TOTAL_GERAL,			
		GI2_KMVOLT GI2_KMIDA, 
		GI2_HRPADR, 
		GI2_PONPAR, 
		GI2_KMMED, 
		GI2_LOCINI,
		GI2_LOCFIM,
		GI2_TIPLIN,
		GI2_PREFIX	
		FROM %Table:GI2% GI2		       
			INNER JOIN %Table:GYN% GYN ON
				GYN.GYN_FILIAL = %xFilial:GYN%
				AND GYN.GYN_LINCOD = GI2.GI2_COD
				AND GYN.GYN_TIPO = '1'
				AND SUBSTRING(GYN.GYN_DTINI,1,6) = %exp:cAnoMes%
				AND GYN.%NotDel%	
			LEFT JOIN %table:GI4% GI4 ON
				GI4.GI4_FILIAL = GI2.GI2_FILIAL
				AND GI4.GI4_LINHA = GI2.GI2_COD
				AND GI4.GI4_LOCORI = GYN.GYN_LOCORI
				AND GI4.GI4_LOCDES = GYN.GYN_LOCDES
				AND GI4.GI4_HIST = '2'				
				AND GI4.%NotDel%
				%exp:cFilGI4%
		WHERE
			GI2.GI2_FILIAL = %xFilial:GI2%
			AND GI2.GI2_NUMLIN BETWEEN %Exp:ALLTRIM(MV_PAR03)% And %Exp:ALLTRIM(MV_PAR04)%
			AND GI2.GI2_HIST = '2'
			AND GI2.%NotDel%
			AND GI2.GI2_KMVOLT > 0
			%exp:cFilGI2%		
		
		GROUP BY GYN.GYN_FILIAL,GI2.GI2_NUMLIN, GYN.GYN_LINCOD, GI2_KMVOLT, GI2_HRPADR, GI2_PONPAR, GI2_KMMED, GI2_LOCINI,GI2_LOCFIM,GI2_TIPLIN,GI2_PREFIX
		ORDER BY GYN.GYN_FILIAL,GI2.GI2_NUMLIN, ORD
	EndSql

					  
Return 
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PesqVeic
Retorna resumo de veiculos utilizados
@type function
@author crisf
@since 04/12/2017
@version 1.0
@param ${param}, ${param_type}, ${param_descr}
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
Static Function PesqVeic( cTmpVeic, cAnoMes, cDiaUtil, cSabados, cDomFer, cLinha )
	
	Local cFilGI4	:= '%'+ If(MV_PAR07 <> 3," and GI4.GI4_MSBLQL = '"+STRZERO(MV_PAR07,1)+"' ",'' )+'%'
	
	BeginSql Alias cTmpVeic
		select
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%)  AND GYN.GYN_EXTRA = 'F' THEN GQE.GQE_RECURS 
					END) AS DIAUTIL_N,	
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cSabados%) AND GYN.GYN_EXTRA = 'F' THEN GQE.GQE_RECURS
					END) AS SABADO_N,
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cDomFer%) AND GYN.GYN_EXTRA = 'F'  THEN GQE.GQE_RECURS
					END) AS DOMFER_N,
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cDiaUtil%)  AND GYN.GYN_EXTRA = 'T' THEN GQE.GQE_RECURS 
					END) AS DIAUTIL_E,
	
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cSabados%) AND GYN.GYN_EXTRA = 'T' THEN GQE.GQE_RECURS
					END) AS SABADO_E,
			COUNT(	DISTINCT CASE
						WHEN GYN.GYN_DTINI IN (%exp:cDomFer%) AND GYN.GYN_EXTRA = 'T'  THEN GQE.GQE_RECURS
					END) AS DOMFER_E
		from %Table:GIC% GIC
			INNER JOIN %Table:GYN% GYN ON
				GYN.GYN_FILIAL = GIC.GIC_FILIAL
				AND GYN.GYN_CODIGO = GIC.GIC_CODSRV
				AND GYN.GYN_TIPO = '1'
				AND GYN.%NotDel%
			INNER JOIN %Table:GQE% GQE ON
				GQE.GQE_FILIAL = GYN.GYN_FILIAL
				AND GQE.GQE_VIACOD = GYN.GYN_CODIGO
				and GQE.GQE_TRECUR = '2'
				AND GQE.%NotDel%
			INNER JOIN %Table:GI2% GI2 ON
				GI2.GI2_FILIAL = GIC.GIC_FILIAL
				AND GI2.GI2_COD = GIC.GIC_LINHA
				AND GI2.GI2_HIST = '2'
				AND GI2.%NotDel%
			LEFT JOIN %Table:GI4% GI4 ON
				GI4.GI4_FILIAL = GI2.GI2_FILIAL
				AND GI4.GI4_LINHA = GI2.GI2_COD
				AND GI4.GI4_LOCORI = GIC.GIC_LOCORI
				AND GI4.GI4_LOCDES = GIC.GIC_LOCDES
				AND GI4.GI4_HIST = '2'
				%exp:cFilGI4%
				AND GI4.%NotDel%
		WHERE
			GIC.GIC_FILIAL = %xFilial:GIC%
			AND GIC.GIC_LINHA = %Exp:cLinha%
			AND SUBSTRING(GIC.GIC_DTVIAG, 1, 6) = %Exp:cAnoMes%
			AND ((GIC.GIC_TIPO IN ('I','T','E','M') AND GIC.GIC_STATUS IN ('V','E','T')) 
			OR (GIC.GIC_TIPO IN ('P','W') AND GIC.GIC_STATUS = 'E'))
			AND GIC.%NotDel%

	EndSql

Return 
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} PesqGIC
(long_description)
@type function
@author crisf
@since 03/12/2017
@version 1.0
@param cTmpGIC, character, (Descri��o do par�metro)
@param aDiasPer, array, (Descri��o do par�metro)
@param cAnoMes, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*///------------------------------------------------------------------------------------------
 Static Function PesqGIC( cTmpGIC, aDiasPer, cAnoMes, cLinha )

	Local nDias		:= 0
	Local nTtDias	:= len(aDiasPer)
	Local cSelect	:= ''
	Local cFilGI2	:= ""
	Local cFilGI4	:= '%'+ If(MV_PAR07 <> 3," and GI4.GI4_MSBLQL = '"+STRZERO(MV_PAR07,1)+"' ",'' )+'%'
	
	cFilGI2	+= If(!Empty(MV_PAR05)	," and GI2.GI2_TIPLIN = '"+MV_PAR05+"' "	,'' )
	cFilGI2	+= If(MV_PAR06 <> 3		," and GI2.GI2_MSBLQL = '"+STRZERO(MV_PAR06,1)+"' "	,'' )
	cFilGI2	:= "%"+cFilGI2+"%" 	
	
	//Monta as colunas dos dias para aglutinar os valores respectivos
	For nDias	:= 1 to nTtDias

		cSelect	+=	"	COUNT((CASE SUBSTRING(GIC.GIC_DTVEND,7,2) "+CRLF
		cSelect	+=	"				WHEN '"+aDiasPer[nDias]+"' THEN GIC_BILHET "+CRLF	
		cSelect	+=	"		END)) AS  TT"+aDiasPer[nDias]+", "+CRLF

	Next nDias
	
	cSelect	:= "%"+cSelect+"%"

	BeginSql Alias cTmpGIC
		
		Select
			GIC.GIC_FILIAL, 
			GIC.GIC_LINHA, 
			CASE WHEN GYN.GYN_HRINI IS NULL THEN 'N�o Encontrado' 
				ELSE GYN.GYN_HRINI END GYN_HRINI, 
			GYN.GYN_CODGID,
			(SELECT COUNT(DISTINCT(GQE_RECURS)) FROM %table:GIC% GIC
			 	LEFT JOIN %table:GQE% GQE ON GQE.GQE_FILIAL = GIC.GIC_FILIAL 
			 		AND GQE.GQE_VIACOD = GIC.GIC_CODSRV 
			 		AND GQE.%NotDel% 
			 WHERE
			GIC_FILIAL = %xFilial:GIC%
			AND GIC.%NotDel%
			AND SUBSTRING(GIC.GIC_DTVEND, 1, 6) = %Exp:cAnoMes%
			AND GIC_LINHA = %Exp:cLinha%
			AND GIC_CODGID = GYN.GYN_CODGID) CARRO, 			
			%Exp: cSelect %
			COUNT(GIC.GIC_BILHET)/%Exp: nTtDias % MEDIA,
			COUNT(GIC.GIC_BILHET) TOTALBILH
		From %table:GIC% GIC
			LEFT JOIN %table:GYN% GYN ON
				GYN.GYN_FILIAL = GIC.GIC_FILIAL
				AND GYN.GYN_CODIGO = GIC.GIC_CODSRV
				AND GYN.GYN_TIPO = '1'
				AND GYN.%NotDel%
			INNER JOIN %table:GI2% GI2 ON
				GI2.GI2_FILIAL = GIC.GIC_FILIAL 
				AND GI2.GI2_COD = GIC.GIC_LINHA
				AND GI2.GI2_HIST = '2'
				AND GI2.%NotDel%
				%Exp:cFilGI2%
			LEFT JOIN %table:GI4% GI4 ON
				GI4.GI4_FILIAL = GI2.GI2_FILIAL
				AND GI4.GI4_LINHA = GI2.GI2_COD
				AND GI4.GI4_LOCORI = GIC.GIC_LOCORI
				AND GI4.GI4_LOCDES = GIC.GIC_LOCDES
				AND GI4.GI4_HIST = '2'
				AND GI4.%NotDel%
				%Exp:cFilGI4%
		Where 
			GIC.GIC_FILIAL = %xFilial:GIC%
			AND GIC.GIC_LINHA = %Exp:cLinha%
			AND ((GIC.GIC_TIPO IN ('I','T','E','M') AND GIC.GIC_STATUS IN ('V','E','T')) 
			OR (GIC.GIC_TIPO IN ('P','W') AND GIC.GIC_STATUS = 'E'))
			AND SUBSTRING(GIC.GIC_DTVEND, 1, 6) = %Exp:cAnoMes%
			AND GIC.%NotDel%
		GROUP BY GIC.GIC_FILIAL, GIC.GIC_LINHA, GYN.GYN_HRINI,GYN.GYN_CODGID,GIC.GIC_SENTID
		ORDER BY GIC.GIC_FILIAL, GIC.GIC_LINHA, GYN.GYN_HRINI,GIC.GIC_SENTID
		
		
	EndSql

Return 
