#Include 'Protheus.ch'
#INCLUDE "FWPrintSetup.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "TBICONN.CH"

Static oFontAr20N		:= nil
Static oFontAr17N		:= nil
Static oFontAr15N		:= nil
Static oFontAr12N		:= nil

USER Function GTPR428()
PREPARE ENVIRONMENT EMPRESA "01" FILIAL "0101101" //"0101101" //"0201120"
	GR428CALL()
RESET ENVIRONMENT

RETURN

Static Function GR428CALL() 
Local oPrint			:= nil
Local cNome				:= "RMD.REL"
Local nDevice			:= IMP_PDF
Local lAdjustToLegacy	:= .F. 
Local lDisabeSetup		:= .T.
Local lViewPDF			:= .T.
Local cAliasTmp			:= nil
oFontAr20N		:= TFont():New('Arial',20,20,.T.)
oFontAr17N		:= TFont():New('Arial',17,17,.T.)
oFontAr15N		:= TFont():New('Arial',15,15,.T.)
oFontAr12N		:= TFont():New('Arial',12,12,.T.)

//cFilCent			:= 

oPrint := FWMSPrinter():New( cNome, nDevice, lAdjustToLegacy, , lDisabeSetup, , , , , , ,  lViewPDF) 
oPrint:SetLandScape(.T.)
oPrint:SetPaperSize(DMPAPER_A4) 
oPrint:cPathPDF := GetTempPath()

If File(oPrint:cPathPDF+"\"+StrTran(cNome,".REL",".PDF"))
	fErase(oPrint:cPathPDF+"\"+StrTran(cNome,".REL",".PDF"))
Endif
/*
SELECT 
	GIC.GIC_TIPDOC,
	GIC.GIC_SERIE,
	GIC.GIC_SUBSER,
	MIN(GIC_NUMDOC) MINDOC,
	MAX(GIC_NUMDOC) MAXDOC,
	SUM(GIC_VALTOT)VALORTOTAL,
	SUM(GIC_VLBICM) BASEICMS,
	GIC_ALICMS,
	SUM(GIC_VLICMS)VALORICMS
FROM GZU010 GZU
	INNER JOIN GIC010 GIC ON
		GIC.GIC_FILIAL = GZU.GZU_FILIAL
		AND GIC.GIC_AGENCI = GZU.GZU_AGENCI
		AND GIC.GIC_CODRMD = GZU.GZU_DOC
		AND GIC.D_E_L_E_T_ = ''

GROUP BY GIC.GIC_TIPDOC,GIC.GIC_SERIE,GIC.GIC_SUBSER,GIC_ALICMS 
ORDER BY GIC.GIC_TIPDOC,GIC.GIC_SERIE,GIC.GIC_SUBSER,GIC_ALICMS 
*/




If !(oPrint:nModalResult == PD_CANCEL) 
	
	oPrint:StartPage()
	
	SetPageLayout(oPrint,cAliasTmp)
	SetPageInfos(oPrint,cAliasTmp)
	
	oPrint:EndPage()
	
	oPrint:Preview()
	oPrint:Deactivate() 
Else
	oPrint:Deactivate() 
Endif

GTPDestroy(oPrint)
GTPDestroy(oFontAr20N)
GTPDestroy(oFontAr17N)
GTPDestroy(oFontAr15N)
GTPDestroy(oFontAr12N)

Return



Static Function SetPageLayout(oPrint,cAliasTmp)
Local nLinIni			:= 20
Local nColIni			:= 10
Local nLinFim			:= 150
Local nColFim			:= 340

	//Cabe�alho
	//-----Dados da Empresa--------------------------------------------------------------------------------------------
	oPrint:Box(nLinIni,nColIni,nLinFim,nColFim)
	oPrint:Say(nLinIni+25	,nColIni+5		,"Nome: "	, oFontAr17N)///*"NOME DO ESTABELECIMENTO CENTRALIZADOR"*/
	//oPrint:Say(nLinIni+25	,nColIni+50		,cFilName	, oFontAr15N)///*"NOME DO ESTABELECIMENTO CENTRALIZADOR"*/
	
	oPrint:Say(nLinIni+75	,nColIni+5		,"End: ", oFontAr17N)//"ENDERE�O"
	//oPrint:Say(nLinIni+75	,nColIni+50		,Alltrim(SM0->M0_ENDENT)+","/*+AllTrim(SM0->M0_CIDENT)+' - '+AllTrim(SM0->M0_ESTENT)*/, oFontAr15N)//"ENDERE�O"
	//oPrint:Say(nLinIni+100	,nColIni+50		,AllTrim(SM0->M0_CIDENT)+' - '+AllTrim(SM0->M0_ESTENT)	, oFontAr15N)//"Cidade-Estado"
	
	oPrint:Say(nLinIni+125	,nColIni+5		,"CNPJ: "	, oFontAr17N)//"CGC E INSC. ESTADUAL"
	//oPrint:Say(nLinIni+125	,nColIni+50		,TransForm(SM0->M0_CGC,"@R 99.999.999/9999-99")	, oFontAr15N)//"CGC E INSC. ESTADUAL"
	
	//-----Dados da Agencia--------------------------------------------------------------------------------------------
	oPrint:Box(nLinIni		,nColIni+= 325	,nLinFim-30	,nColFim+=150	)
	//oPrint:Say(nLinIni+15	,nColIni+5		,"NOME, AGENCIA,"	, oFontAr17N)//DATA
	//oPrint:Say(nLinIni+30	,nColIni+5		,"FILIAL"			, oFontAr17N)//DATA
	//oPrint:Say(nLinIni+45	,nColIni+5		,"OU POSTO"			, oFontAr17N)//DATA
	//oPrint:Say(nLinIni+60	,nColIni+5		,"ENDERE�O"			, oFontAr17N)//DATA
	
	//oPrint:Say(nLinIni+90	,nColIni+10		,"CGC", oFontAr17N)//DATA
	
	//-----Data-------------------------------------------------------------------------------------------------
	oPrint:Box(nLinIni+100	,nColIni		,nLinFim	,nColFim		)
	oPrint:Say(nLinIni+120	,nColIni+10		,"DATA: ", oFontAr17N)//DATA
	//oPrint:Say(nLinIni+120	,nColIni+70		,DtoC(dDataBase), oFontAr15N)//DATA
	
	
	//-----Resumo Movimento Diario------------------------------------------------------------------------------------------
	oPrint:Box(nLinIni		,nColIni+=150	,nLinFim	,nColFim+=335	)//RMD
	oPrint:Say(nLinIni+25	,nColIni+40		,"RESUMO DE MOVIMENTO DI�RIO", oFontAr20N)//RMD
	
	oPrint:Say(nLinIni+60	,nColIni+15		,"N�:", oFontAr17N)//Nr:
	
	oPrint:Say(nLinIni+60	,nColIni+100	,"S�RIE - SUBS�RIE", oFontAr17N)//S�RIE - SUBS�RIE
	oPrint:Line(nLinIni+61	,nColIni+260	,nLinIni+61	,nColIni+279)
	oPrint:Say(nLinIni+60	,nColIni+280	,"� VIA", oFontAr17N)//VIA
	
	oPrint:Say(nLinIni+90	,nColIni+15	,"NATUREZA DO TRANSPORTE", oFontAr17N)
	
	//-----Titulo Colunas------------------------------------------------------------------------------------------
	nLinIni := 160 
	nLinFim := 185
	nColIni	:= 10
	nColFim	:= 185
	
	oPrint:Box(nLinIni	,nColIni			,nLinFim	,nColFim	)//DocEmitidos
	oPrint:Say(nLinIni+15,nColIni+50,"Doc. Emitidos", oFontAr15N)
	
	nLinIni := nLinFim 
	nLinFim := 210
	nColIni	:= 10
	nColFim	:= 60
	
	oPrint:Box(nLinIni	,nColIni 			,nLinFim	,nColFim		)//TIPO
	oPrint:Say(nLinIni+15,nColIni+10,"Tipo", oFontAr15N)
	
	oPrint:Box(nLinIni	,nColIni:= nColFim	,nLinFim	,nColFim+=050	)//S�rie
	oPrint:Say(nLinIni+15,nColIni+10,"S�rie", oFontAr15N)
	
	oPrint:Box(nLinIni	,nColIni:= nColFim	,nLinFim	,nColFim+=075	)//Numeros
	oPrint:Say(nLinIni+15,nColIni+10,"N�meros", oFontAr15N)
	
	nLinIni := 160 
	nLinFim := 210
	nColIni	:= 185
	nColFim	:= 335
	
	oPrint:Box(nLinIni	,nColIni 			,nLinFim	,nColFim		)//Valor Contabil
	oPrint:Say(nLinIni+30,nColIni+40,"Valor Cont�bil", oFontAr15N)
	
	//Val c/Debito do Imposto
	nLinFim := 160
	nColIni	:= nColFim
	nColFim	:= 485
	
	oPrint:Box(nLinIni	,nColIni	,nLinFim	,nColFim	)//Val c/Debito do Imposto
	oPrint:Say(nLinIni+11,nColIni+40,"Val c/ Debito", oFontAr15N)
	oPrint:Say(nLinIni+23,nColIni+50,"do Imposto", oFontAr15N)
	
	
	nLinIni := 185 
	nLinFim := 210
	nColIni	:= 335
	nColFim	:= 395
	
	oPrint:Box(nLinIni	,nColIni			,nLinFim	,nColFim		)//Base Calculo 
	oPrint:Say(nLinIni+10,nColIni+10,"Base de", oFontAr12N)
	oPrint:Say(nLinIni+20,nColIni+10,"Calculo", oFontAr12N)
	
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=30	)//Aliquota     
	oPrint:Say(nLinIni+15,nColIni+5,"Aliq.", oFontAr12N)
	
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=60	)//ICMS         
	oPrint:Say(nLinIni+15,nColIni+15,"ICMS", oFontAr15N)
	
	//Valor sem Debito
	nLinIni := 160 
	nLinFim := 185
	nColIni	:= nColFim //485
	nColFim	:= 685
	oPrint:Box(nLinIni	,nColIni ,nLinFim	,nColFim		)//Valor sem Debito 
	oPrint:Say(nLinIni+15,nColIni+50,"Valor sem Debito", oFontAr15N)
	
	nLinIni := 185 
	nLinFim := 210
	nColIni	:= 485
	nColFim	:= 585
	
	oPrint:Box(nLinIni	,nColIni			,nLinFim	,nColFim		)//"Isentos e N�o Tributados"
	oPrint:Say(nLinIni+10,nColIni+20,"Isentos e N�o", oFontAr12N)
	oPrint:Say(nLinIni+20,nColIni+30,"Tributados"	, oFontAr12N)
	
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=100	)//outros         
	oPrint:Say(nLinIni+15,nColIni+20,"Outros", oFontAr15N)
	
	nLinIni := 160 
	nLinFim := 210
	nColIni	:= 685
	nColFim	:= 825
	
	oPrint:Box(nLinIni	,nColIni ,nLinFim	,nColFim	)//observa��es    
	oPrint:Say(nLinIni+30,nColIni+35,"Observa��es", oFontAr15N)
	
	//-----Prenchimento dos dados-------------------------------------------------------------------
	
	nLinIni := nLinFim 
	nLinFim := 510
	nColIni	:= 10
	nColFim	:= 60
	
	//Docs Emitidos
	oPrint:Box(nLinIni	,nColIni 			,nLinFim	,nColFim		)//TIPO
	oPrint:Box(nLinIni	,nColIni:= nColFim	,nLinFim	,nColFim+=050	)//S�rie
	oPrint:Box(nLinIni	,nColIni:= nColFim	,nLinFim	,nColFim+=075	)//Numeros
	oPrint:Box(nLinIni	,nColIni:= nColFim	,nLinFim	,nColFim+=150	)//Valor Contabil
	
	//Val c/Debito do Imposto
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=60	)//Base Calculo 
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=30	)//Aliquota     
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=60	)//ICMS         
	
	
	//Valor sem Debito
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=100	)//Isentos e N�o tributados 
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=100	)//outros         
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=140	)//observa��es    
	
    
	
	//Totais
	nLinIni := nLinFim
	nColIni	:= 10
	nLinFim	:= nLinIni + 30 
	nColFim	:= 185
	
	oPrint:Box(nLinIni	,nColIni			,nLinFim	,nColFim		)//Totais
	oPrint:Say(nLinIni+20,nColIni+50,"T O T A I S", oFontAr15N)
	
	oPrint:Box(nLinIni	,nColIni := nColFim	,nLinFim	,nColFim+=150	)//Tot Valor Contabil 
	
	//Val c/Debito do Imposto
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=060	)//Base Calculo 
	
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=30	)//Aliquota     
	oPrint:FillRect ( {nLinIni+1	,nColIni+1 ,nLinFim-1	,nColFim-1}, TBrush():New2( , CLR_LIGHTGRAY ) ) 
	
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=60	)//ICMS         
	
	
	//Valor sem Debito
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=100	)//??? tributados 
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=100	)//outros         
	oPrint:Box(nLinIni	,nColIni:= nColFim ,nLinFim	,nColFim+=140	)//observa��es  
	oPrint:FillRect ( {nLinIni+1	,nColIni+1 ,nLinFim-1	,nColFim-1}, TBrush():New2( , CLR_LIGHTGRAY )  ) 

Return

Static Function SetPageInfos(oPrint,cAliasTmp)

Local nLinIni			:= 20
Local nColIni			:= 10

Local cFilCent			:= GTPGetRules("FILRMD"+FwCodEmp())
Local cFilName		:= FWFilialName(cEmpAnt,cFilCent)
Local aFilCent		:= GetEmpArr(cFilCent) 
Local aFilAgenc		:= GetEmpArr("010111B")

//Cabe�alho
	//-----Dados da Empresa--------------------------------------------------------------------------------------------
	oPrint:Say(nLinIni+25	,nColIni+50		,aFilCent[1]	, oFontAr15N)///*"NOME DO ESTABELECIMENTO CENTRALIZADOR"*/
	
	oPrint:Say(nLinIni+75	,nColIni+50		,aFilCent[2]+","/*+AllTrim(SM0->M0_CIDENT)+' - '+AllTrim(SM0->M0_ESTENT)*/, oFontAr15N)//"ENDERE�O"
	oPrint:Say(nLinIni+100	,nColIni+50		,Capital(aFilCent[3])+' - '+aFilCent[4]	, oFontAr15N)//"Cidade-Estado"
	
	oPrint:Say(nLinIni+125	,nColIni+50		,TransForm(aFilCent[5],"@R 99.999.999/9999-99")	, oFontAr15N)//"CGC E INSC. ESTADUAL"
	
	//-----Dados da Agencia--------------------------------------------------------------------------------------------
	nColIni+= 325
	oPrint:Say(nLinIni+15	,nColIni+5		,aFilAgenc[1]	, oFontAr15N)//Nome Agencia
	oPrint:Say(nLinIni+45	,nColIni+5		,aFilAgenc[2]	, oFontAr15N)//Endere�o
	oPrint:Say(nLinIni+60	,nColIni+5		,Capital(aFilAgenc[3])+' - '+aFilAgenc[4]	, oFontAr15N)//Cidade-Estado

	
	oPrint:Say(nLinIni+90	,nColIni+5		,TransForm(aFilAgenc[5],"@R 99.999.999/9999-99"), oFontAr15N)//CNPJ agencia
	
	//-----Data-------------------------------------------------------------------------------------------------
	oPrint:Say(nLinIni+120	,nColIni+70		,DtoC(dDataBase), oFontAr15N)//DATA
	
	
	//-----Resumo Movimento Diario------------------------------------------------------------------------------------------
	nColIni+=150
	oPrint:Say(nLinIni+60	,nColIni+45		,"000.000", oFontAr15N)//Nr:
	
	//oPrint:Say(nLinIni+60	,nColIni+100	,"S�RIE - SUBS�RIE", oFontAr17N)//S�RIE - SUBS�RIE
	//oPrint:Line(nLinIni+61	,nColIni+260	,nLinIni+61	,nColIni+279)
	oPrint:Say(nLinIni+60	,nColIni+270	,"1", oFontAr17N)//VIA
	//oPrint:Say(nLinIni+60	,nColIni+280	,"� VIA", oFontAr17N)//VIA
	
	//oPrint:Say(nLinIni+90	,nColIni+15	,"NATUREZA DO TRANSPORTE", oFontAr17N)

	
	//prenchimento dos dados
	nLinIni := 210 
	nColIni	:= 10
	
	
	//Totais
	nLinIni := 510
	nColIni	:= 10
	

Return



Static Function GetEmpArr(cFilAux)
Local cOldFil	:= cFilAnt
Local aFilAux	:= nil
Local aAreaSM0	:= SM0->(GetArea())

SM0->(DbSetOrder(1))
SM0->(DbSeek(cEmpAnt+cFilAux))

aFilAux := {Alltrim(SM0->M0_FILIAL),Alltrim(SM0->M0_ENDCOB),Alltrim(SM0->M0_CIDCOB),Alltrim(SM0->M0_ESTCOB),ALLTRIM(SM0->M0_CGC)}

SM0->(DbSeek(cEmpAnt+cOldFil))

RestArea(aAreaSM0)

Return aFilAux	