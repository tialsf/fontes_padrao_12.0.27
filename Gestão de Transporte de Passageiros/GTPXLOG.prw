#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "GTPXLOG.CH"

//------------------------------------------------------------------------------
/*/{Protheus.doc} GTPLog
Classe responsavel para cria��o de log
@type Class
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
CLASS GTPLog From FWSerialize
	
	DATA cTitulo    as CHARACTER
	DATA lSalva     as LOGICAL
	DATA lShow      as LOGICAL
	DATA cText      as CHARACTER
	DATA lHasInfo   as LOGICAL
	DATA cClassName as CHARACTER
    Data cFunName   as Character
    Data oModel     as Object
    Data dDtIni     as Date
    Data cHrIni     as Character

	METHOD New(cTitulo,lSalva,lShow,cFunName) CONSTRUCTOR
	METHOD Destroy()
	
    METHOD ClassName()

	METHOD SetText(cText)
	METHOD GetText()
	METHOD HasInfo()
	METHOD ShowLog()
    Method ShowView()
    Method SaveLog()

ENDCLASS

//------------------------------------------------------------------------------
/*/{Protheus.doc} Destroy
Metodo responsavel pela constru��o da classe
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
METHOD New(cTitulo,lSalva,lShow,cFunName) Class GTPLog
Default cTitulo     := ""
Default lSalva      := .F.
Default lShow       := !IsBlind()
Default cFunName    := FunName()

Self:cTitulo    := cTitulo
Self:lSalva     := lSalva
Self:lShow      := lShow
Self:cText      := ''
Self:lHasInfo   := .F.
Self:cClassName := "GTPLog"
Self:cFunName   := cFunName
Self:oModel     := nil
Self:dDtIni     := dDataBase
Self:cHrIni     := Time()

Return Self

//------------------------------------------------------------------------------
/*/{Protheus.doc} Destroy
Metodo responsavel pela destrui��o da classe
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
METHOD Destroy() Class GTPLog

Self:cTitulo    := ''
Self:lSalva     := .F.
Self:lShow      := .F.
Self:cText      := ''
Self:lHasInfo   := .F.
Self:cFunName   := ''

GtpDestroy(Self:oModel)
GtpDestroy(Self)
	
Return Nil

//------------------------------------------------------------------------------
/*/{Protheus.doc} SetText
Metodo responsavel retornar o nome da classe
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
METHOD ClassName() Class GTPLog
Return Self:cClassName()

//------------------------------------------------------------------------------
/*/{Protheus.doc} SetText
Metodo responsavel para gravar o texto no log
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
METHOD SetText(cText) Class GTPLog
Default cText := ""

Self:lHasInfo:= .T.

Self:cText	 += cText + Chr(13)+Chr(10)

Return Self:cText

//------------------------------------------------------------------------------
/*/{Protheus.doc} GetText
Metodo responsavel para retornar o texto preenchido no log
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:cText
/*/
//------------------------------------------------------------------------------
METHOD GetText() Class GTPLog
Return Self:cText

//------------------------------------------------------------------------------
/*/{Protheus.doc} HasInfo
Metodo responsavel para verificar se foi preenchido algum dado no log
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:lHasInfo
/*/
//------------------------------------------------------------------------------
METHOD HasInfo() Class GTPLog
Return Self:lHasInfo

//------------------------------------------------------------------------------
/*/{Protheus.doc} ShowLog
Metodo responsavel pela visualiza��o do log para o usu�rio
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return Self:GetText()
/*/
//------------------------------------------------------------------------------
METHOD ShowLog() Class GTPLog

If Self:lShow
	Self:ShowView()
Endif

If Self:lSalva
    Self:SaveLog()
Endif 

Return Self:GetText()

//------------------------------------------------------------------------------
/*/{Protheus.doc} ShowView
Metodo responsavel pela visualiza��o do log para o usu�rio
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
/*/
//------------------------------------------------------------------------------
METHOD ShowView() Class GTPLog
Local oMdlLog       := FwLoadModel('GTPXLOG')
Local oViewExec     := FWViewExec():New()
Local aButtons      := GtpBtnView(.F.,"",.T.,STR0001)//"Fechar"

oMdlLog:SetOperation(MODEL_OPERATION_INSERT)
oMdlLog:Activate()
oMdlLog:GetModel("GZIMASTER"):LoadValue('GZI_PARAME',Self:cText)
oMdlLog:lModify := .F.

oViewExec:setSource("GTPXLOG")
oViewExec:setModel(oMdlLog)
oViewExec:setReduction(75)
oViewExec:SetTitle(Self:cTitulo)
oViewExec:SetButtons(aButtons)

oViewExec:openView()

oMdlLog:Destroy()

GtpDestroy(oMdlLog)
GtpDestroy(oViewExec)

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} SaveLog
Metodo responsavel pela persisistencia dos dados na tabela GZI
@type method
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
/*/
//------------------------------------------------------------------------------
METHOD SaveLog() Class GTPLog

Local oMdlGZI     := nil

Self:oModel := FwLoadModel('GTPA038')
oMdlGZI     := Self:oModel:GetModel('GZIMASTER')

Self:oModel:SetOperation(MODEL_OPERATION_INSERT)

If Self:oModel:Activate()
    oMdlGZI:LoadValue('GZI_DESCRI' ,Self:cTitulo    )
    oMdlGZI:LoadValue('GZI_DTINI'  ,Self:dDtIni     )
    oMdlGZI:LoadValue('GZI_HRINI'  ,Self:cHrIni     )
    oMdlGZI:LoadValue('GZI_PARAME' ,Self:cText      )
Endif

If Self:oModel:VldData()
    Self:oModel:CommitData()
Endif

Self:oModel:DeActivate()
Self:oModel:Destroy()

Return

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Fun��o responsavel pela defini��o do modelo
@type Static Function
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return oModel, retorna o Objeto do Menu
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()
Local oModel	:= nil
Local oStrGZI	:= FWFormModelStruct():New()

GTPxCriaCpo(oStrGZI,{"GZI_PARAME"},.T.)

oModel := MPFormModel():New('GTPXLOG', /*bPreValidacao*/, /*bPosValid*/, /*bCommit*/, /*bCancel*/ )

oModel:AddFields('GZIMASTER',/*cOwner*/,oStrGZI,/*bPre*/,/*bPos*/,/*bLoad*/)

oModel:SetDescription(STR0002)//'Log de Processamento'

oModel:GetModel('GZIMASTER'):SetDescription(STR0002)//'Log de Processamento' 

oModel:GetModel('GZIMASTER'):SetOnlyQuery(.T.)

oModel:SetPrimaryKey({''})

Return oModel

//------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Fun��o responsavel pela defini��o da view
@type Static Function
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@return oView, retorna o Objeto da View
/*/
//------------------------------------------------------------------------------
Static Function ViewDef()
Local oView		:= FWFormView():New()

oView:CreateHorizontalBox('LOG', 100)	

oView:AddOtherObject("MEMO_PANEL", {|oPanel| ShowMemoPanel(oPanel,oView)})

oView:SetOwnerView("MEMO_PANEL",'LOG')

oView:AddUserButton( STR0003, "", {|oView| SaveMemo(oView)} )//"Salvar Log"

Return oView

//------------------------------------------------------------------------------
/* /{Protheus.doc} ShowMemoPanel
Fun��o responsavel para salvar o memo do campo GZI_PARAME
@type Static Function
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@param oPanel, object, (Descri��o do par�metro)
@param oView, object, (Descri��o do par�metro)
/*/
//------------------------------------------------------------------------------

Static Function ShowMemoPanel(oPanel,oView)
Local cMemo         := oView:GetModel('GZIMASTER'):GetValue('GZI_PARAME')
Local oFont         := TFont():New('Courier new',,-16,.F.)
Local aClientRect   := oPanel:GetClientRect()
Local n1            := aClientRect[1]+2
Local n2            := aClientRect[2]+2
Local n3            := Round(aClientRect[3]*0.493,0)
Local n4            := Round(aClientRect[4]*0.485,0)

	@ n1,n2 GET oMemo  VAR cMemo MEMO SIZE n3,n4 OF oPanel PIXEL 
	oMemo:bRClicked := {||AllwaysTrue()}
	oMemo:oFont:=oFont
	oMemo:EnableVSCroll(.T.)
	oMemo:Disable(.T.)

Return

//------------------------------------------------------------------------------
/* /{Protheus.doc} SaveMemo
Fun��o responsavel para salvar o memo do campo GZI_PARAME
@type Static Function
@author jacomo.fernandes
@since 24/01/2020
@version 1.0
@param oView, object, (Descri��o do par�metro)
/*/
//------------------------------------------------------------------------------
Static Function SaveMemo(oView)
Local cMask	:= STR0004+" (*.TXT) |*.txt|"//"Arquivos Texto"
Local cFile	:= cGetFile(cMask,OemToAnsi(STR0005))//"Salvar Como..."
Local cMemo	:= oView:GetModel('GZIMASTER'):GetValue('GZI_PARAME')

If !Empty(cFile)
	If At('.TXT',Upper(cFile)) == 0
		cFile+= ".txt"
	Endif
	MemoWrite(cFile,cMemo)
Endif

Return 