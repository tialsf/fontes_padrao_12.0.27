#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"           
#INCLUDE "GTPXVLDREC.CH"

//------------------------------------------------------------------------------
/* /{Protheus.doc} GTPVldDocRec
Fun��o responsavel pela valida��o dos documentos do recurso
@type Function
@author 
@since 17/07/2019
@version 1.0
@param , character, (Descri��o do par�metro)
@return , return_description
@example
(examples)
@see (links_or_references)
/*/
//------------------------------------------------------------------------------
Function GTPVldDocRec(cTpRecurso,cCodRec,dData)
Local lRet      := .T.
Local lPergunta := .T.
Local cMsgYesNo := ""
Local cAliasTmp := GetNextAlias()

BeginSql Alias cAliasTmp
    Column G6W_DTFIM as Date
    Column G6W_DTMAX as Date
    Select 
        (Case
            when G6W_DTMAX < %Exp:dData% then 'ERROR'
            when G6W_DTFIM < %Exp:dData% then 'WARNING'
            ELSE ''
        End) as PROBLEMA,
        G6W.G6W_CODG6U,
        G6U.G6U_DESCRI,
        G6W.G6W_DTFIM,
        G6W.G6W_DTMAX
    From %Table:G6V% G6V
        Inner Join %Table:G6W% G6W on
            G6W.G6W_FILIAL = G6V.G6V_FILIAL
            AND G6W_CODIGO = G6V.G6V_CODIGO
            AND G6W_STATUS <> '2'
            AND G6W_DTFIM < %Exp:dData%
            AND G6W.%NotDel%
        Inner Join %Table:G6U% G6U on
            G6U.G6U_FILIAL = %xFilial:G6U%
            AND G6U.G6U_CODIGO = G6W.G6W_CODG6U
            AND G6U.%NotDel%
    Where
        G6V.G6V_FILIAL = %xFilial:G6V%
        AND G6V_TRECUR = %Exp:cTpRecurso%
        AND G6V_RECURS = %Exp:cCodRec%
        AND G6V_STATUS <> '2'
        AND G6V.%NotDel%
EndSql

While (cAliasTmp)->(!Eof())
    If Alltrim((cAliasTmp)->PROBLEMA) == "ERROR"
        lRet        := .F.
        lPergunta   := .F.
        Exit
    ElseIf Empty(cMsgYesNo)
        lRet        := .F.
        lPergunta   := .T.
        cMsgYesNo   := STR0001//"O documento #1 - #2 encontra-se vencido desde o dia #3, porem est� dentro da toler�ncia m�xima (#4). Deseja Continuar?"
        cMsgYesNo   := I18n(cMsgYesNo,{(cAliasTmp)->G6W_CODG6U,(cAliasTmp)->G6U_DESCRI,(cAliasTmp)->G6W_DTFIM,(cAliasTmp)->G6W_DTMAX})
    Endif

    (cAliasTmp)->(DbSkip())
End
(cAliasTmp)->(DbCloseArea())

If !lRet .and. lPergunta
    lRet    := FwAlertYesNo(cMsgYesNo,STR0002)//"Aten��o!!!"
Endif

Return lRet