#INCLUDE "PROTHEUS.CH"                
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "GTPIRJ001.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPIRJ001

Adapter REST da rotina de LOCALIDADE

@type 		function
@sample 	GTPIRJ001(lJob)
@param 	 	lJob, logical - indica se a chamada foi realizada atrav�s de JOB (.T.) ou n�o (.F.)
@return		Logical - informa se o processo foi finalizado com sucesso (.T.) ou n�o (.F.)	 	
@author 	thiago.tavares
@since 		25/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GTPIRJ001(lJob)

Local aArea  := GetArea() 
Local lRet   := .T.

Default lJob := .F. 

If !lJob .and. FwAlertYesNo(STR0001)		// "Para integrar as Localidades � preciso integrar Estados e Cidades. Deseja realizar a integra��o?"
	GTPIRJA001(.F.)	// Estado
	GTPIRJB001(.F.)	// Cidades
EndIf		 

FwMsgRun( , {|oSelf| lRet := GI001Receb(lJob, oSelf)}, , STR0002)		// "Processando registros de Localidades... Aguarde!" 

RestArea(aArea)
GTPDestroy(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001Receb

Fun��o utilizada para executar o recebimento da integra��o e atualizar o registro

@type 		function
@sample 	GI001Receb(lJob, oMessage)
@param 		lJob, logical    - informa se a chamada foi realizada atrav�s de job (.T.) ou n�o (.F.) 
			oMessage, objeto - trata a mensagem apresentada em tela
@return 	lRet, logical    - resultado do processamento da rotina (.T. / .F.)
@author 	thiago.tavares
@since 		29/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Static Function GI001Receb(lJob, oMessage)

Local oRJIntegra  := GtpRjIntegra():New()
Local oModel	  := FwLoadModel("GTPA001")
Local oMdlGI1	  := Nil
Local aFldDePara  := {}
Local aDeParaXXF  := {}
Local aCampos	  := {"GI1_FILIAL", "GI1_COD"}
Local cIntID	  := ""
Local cIntAux	  := ""
Local cExtID	  := ""
Local cCode		  := ""
Local cErro		  := ""
Local cTagName    := ""
Local cCampo      := ""
Local cTipoCpo    := ""
Local xValor      := ""
Local cEstado     := ""
Local nX          := 0
Local nY          := 0
Local nOpc		  := 0
Local nPos        := 0
Local nTotReg     := 0
Local lOk		  := .F.
Local lRet        := .T.
Local lContinua   := .T.
Local lOnlyInsert := .F.
Local lOverWrite  := .F.
Local lMessage	  := ValType(oMessage) == 'O'

oRJIntegra:SetPath("/localidade/todas")
oRJIntegra:SetServico('Localidade')

aFldDePara	:= oRJIntegra:GetFieldDePara()
aDeParaXXF  := oRJIntegra:GetFldXXF()

If oRJIntegra:Get()
	GI1->(DbSetOrder(1))	// GI1_FILIAL+GI1_COD
	nTotReg := oRJIntegra:GetLenItens()
	For nX := 0 To nTotReg
		lContinua := .T.
		If lMessage .And. !lJob
			oMessage:SetText(I18N(STR0003, {cValtoChar(nX + 1), nTotReg + 1}))		// "Processando registros de localidades - #1/#2... Aguarde!" 
			ProcessMessages()
		EndIf

		If !Empty((cExtID := oRJIntegra:GetJsonValue(nX, 'idLocalidade', 'C'))) 
		  	cCode := GTPxRetId("TotalBus", "GI1", "GI1_COD", cExtID, @cIntID, 3, @lOk, @cErro, aCampos, 1)
			If Empty(cIntID)  
				nOpc := MODEL_OPERATION_INSERT
			ElseIf lOk .And. GI1->(DbSeek(xFilial('GI1') + cCode))
				nOpc := MODEL_OPERATION_UPDATE
			Else
				lContinua := .F.
				oRJIntegra:oGTPLog:SetText(cErro)
			EndIf

			If lContinua
				oModel:SetOperation(nOpc)
				If oModel:Activate()
					oMdlGI1 := oModel:GetModel("GI1MASTER")

					For nY := 1 To Len(aFldDePara)
						// recuperando a TAG e o respectivo campo da tabela 
						cTagName    := aFldDePara[nY][1] 
						cCampo      := aFldDePara[nY][2]
						cTipoCpo    := aFldDePara[nY][3]
						lOnlyInsert := aFldDePara[nY][6]
						lOverWrite  := aFldDePara[nY][7]
						
						// recuperando atrav�s da TAG o valor a ser inserido no campo 
						If !Empty(cTagName) .And. !Empty((xValor := oRJIntegra:GetJsonValue(nX, cTagName, cTipoCpo)))
							
							// verificando a necessidade de realizar o DePara XXF
							If (nPos := aScan(aDeParaXXF, {|x| x[1] == cCampo})) > 0
								xValor := GTPxRetId("TotalBus", aDeParaXXF[nPos, 2], aDeParaXXF[nPos, 3], xValor, @cIntAux, aDeParaXXF[nPos, 4], @lOk, @cErro, aDeParaXXF[nPos, 6], aDeParaXXF[nPos, 5])
							EndIf

							// se ao preencher o c�digo do municipio os c�digos de UF e PAIS estiverem vazios � preciso preenche-los 
							If cCampo == "GI1_CDMUNI" .And. (Empty(oMdlGI1:GetValue("GI1_UF")) .Or. Empty(oMdlGI1:GetValue("GI1_PAIS")))
								If Empty(oMdlGI1:GetValue("GI1_UF"))
									oMdlGI1:SetValue("GI1_UF", (cEstado := Posicione("CC2", 3, xFilial("CC2") + xValor, "CC2_EST")))
								EndIf 
								oMdlGI1:SetValue("GI1_PAIS", IIF(cEstado != "EX", "105", "999"))
								CC2->(DbSetOrder(1))
							EndIf

							If nOpc == MODEL_OPERATION_INSERT .And. lOnlyInsert .And. Empty(oMdlGI1:GetValue(cCampo)) 
								lContinua := oRJIntegra:SetValue(oMdlGI1, cCampo, xValor)
							ElseIf (nOpc == MODEL_OPERATION_INSERT .And. !lOnlyInsert) .Or. (nOpc == MODEL_OPERATION_UPDATE .And. lOverWrite) 
								lContinua := oRJIntegra:SetValue(oMdlGI1, cCampo, xValor)
							EndIf

							If !lContinua 
								oRJIntegra:oGTPLog:SetText(I18N(STR0004, {cCampo, GTPXErro(oModel)}))		// "Falha ao gravar o valor do campo #1 (#2)." 
								Exit	
							EndIf
						EndIf
					Next nY
					
					If !lContinua 
						Exit
					Else
						If (lContinua := oModel:VldData())
							oModel:CommitData()
							CFGA070MNT("TotalBus", "GI1", "GI1_COD", cExtID, IIF(!Empty(cIntId), cIntId, GTPxMakeId(oMdlGI1:GetValue('GI1_COD'), 'GI1')))
						Else							
							oRJIntegra:oGTPLog:SetText(I18N(STR0005, {GTPXErro(oModel)}))		// "Falha ao gravar os dados (#1)." 
						EndIf
					EndIf
					oModel:DeActivate()
				Else
					oRJIntegra:oGTPLog:SetText(I18N(STR0006, {GTPXErro(oModel)}))		// "Falha ao corregar modelos de dados (#1)." 
					Exit
				EndIf
			EndIf
		EndIf
	Next nX
Else
	oRJIntegra:oGTPLog:SetText(I18N(STR0007, {oRJIntegra:GetLastError()}))		// "Falha ao processar o retorno do servi�o (#1)." 
EndIf

If !lJob .And. oRJIntegra:oGTPLog:HasInfo() 
	oRJIntegra:oGTPLog:ShowLog()
	lRet := .F.
ElseIf !lJob .And. !oRJIntegra:oGTPLog:HasInfo()
	If lMessage 
		oMessage:SetText(STR0008)		// "Processo finalizado." 
		ProcessMessages()
	Else
		Alert(STR0008)		// "Processo finalizado."
	EndIf	
EndIf

oRJIntegra:Destroy()
GTPDestroy(oModel)
GTPDestroy(oMdlGI1)
GTPDestroy(aFldDePara)
GTPDestroy(aDeParaXXF)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001Job

Fun��o utilizada para consumir o servi�o atrav�s de um JOB

@type 		function
@sample 	GI001Job(aParams)
@param		aParam, array - lista de par�metros 	 	
@return 	
@author 	jacomo.fernandes
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GI001Job(aParams)

//---Inicio Ambiente
RPCSetType(3)
RpcSetEnv(aParams[1], aParams[2])

GTPIRJ001(.T.)

RpcClearEnv()

Return