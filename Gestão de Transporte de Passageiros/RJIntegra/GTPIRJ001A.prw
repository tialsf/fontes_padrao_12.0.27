#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "GTPIRJ001A.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPIRJA001

Adapter REST da rotina de ESTADO

@type 		function
@sample 	GTPIRJA001(lJob)
@param 	 	lJob, logical - indica se a chamada foi realizada atrav�s de JOB (.T.) ou n�o (.F.)
@return		Logical - informa se o processo foi finalizado com sucesso (.T.) ou n�o (.F.)	 	
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GTPIRJA001(lJob)

Local aArea  := GetArea() 
Local lRet   := .T.

Default lJob := .F. 

FwMsgRun( , {|oSelf| lRet := GI001AReceb(lJob, oSelf)}, , STR0001)		// "Processando registros de Estados... Aguarde!" 

RestArea(aArea)
GTPDestroy(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001AReceb

Fun��o utilizada para executar o recebimento da integra��o e atualizar o registro

@type 		function
@sample 	GI001AReceb(cRestResult, oMessage)
@param 		lJob, logical    - informa se a chamada foi realizada atrav�s de job (.T.) ou n�o (.F.) 
			oMessage, objeto - trata a mensagem apresentada em tela
@return 	Logical - resultado do processamento da rotina (.T. / .F.)
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Static Function GI001AReceb(lJob, oMessage)

Local oRJIntegra := GtpRjIntegra():New()
Local cIntID     := ""
Local cExtID     := ""
Local cCode	     := ""
Local nX	     := 0
Local nTotReg    := 0
Local lRet       := .T.   
Local lMessage   := ValType(oMessage) == 'O'

oRJIntegra:SetPath("/estado/todos")
oRJIntegra:cMainList := 'estado'

If oRJIntegra:Get()
	nTotReg := oRJIntegra:GetLenItens()
	For nX := 0 To nTotReg 
		If lMessage .And. !lJob
			oMessage:SetText(I18N(STR0002, {cValtoChar(nX + 1), nTotReg + 1}))		// "Processando registros de Estados - #1/#2... Aguarde!" 
			ProcessMessages()
		EndIf

		If !Empty((cExtID := oRJIntegra:GetJsonValue(nX, 'idEstado', 'C'))) 
			cExtID := cValToChar(cExtID)
			cCode  := GTPxRetId("TotalBus", "SX5", "X5_CHAVE", cExtID, @cIntID, 4)
			If Empty(cCode)				
				If !Empty(FWGetSX5( "12", oRJIntegra:GetJsonValue(nX, 'codigoEstado', 'C') ) )
					CFGA070MNT("TotalBus", "SX5", "X5_CHAVE", cExtID, GTPxMakeId({"12", FWGetSX5( "12", oRJIntegra:GetJsonValue(nX, 'codigoEstado', 'C') )[1][3] }, "SX5"))
				Else
					CFGA070MNT("TotalBus", "SX5", "X5_CHAVE", cExtID, GTPxMakeId({"12", "EX"}, "SX5"))
				EndIf
			EndIf
		EndIf	
	Next nX
Else
	oRJIntegra:oGTPLog:SetText(I18N(STR0003, {oRJIntegra:GetLastError()}))		// "Falha ao processar o retorno do servi�o (#1)." 
EndIf

If !lJob .And. oRJIntegra:oGTPLog:HasInfo() 
	oRJIntegra:oGTPLog:ShowLog()
	lRet := .F.
ElseIf !lJob .And. !oRJIntegra:oGTPLog:HasInfo()
	If lMessage 
		oMessage:SetText(STR0004)		// "Processo finalizado." 
		ProcessMessages()
	Else
		Alert(STR0004)		// "Processo finalizado."
	EndIf	
EndIf

oRJIntegra:Destroy()

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001AJob

Fun��o utilizada para consumir o servi�o atrav�s de um JOB

@type 		function
@sample 	GI001AJob(aParams)
@param		aParam, array - lista de par�metros 	 	
@return 	
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GI001AJob(aParams)

//---Inicio Ambiente
RPCSetType(3)
RpcSetEnv(aParams[1], aParams[2])

GTPIRJA001(.T.)

RpcClearEnv()

Return