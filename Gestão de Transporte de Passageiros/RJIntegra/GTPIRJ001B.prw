#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "GTPIRJ001B.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPIRJB001

Adapter REST da rotina de CIDADE

@type 		function
@sample 	GTPIRJB001(lJob)
@param 	 	lJob, logical - indica se a chamada foi realizada atrav�s de JOB (.T.) ou n�o (.F.)
@return		Logical - informa se o processo foi finalizado com sucesso (.T.) ou n�o (.F.)	 	
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GTPIRJB001(lJob)

Local aArea := GetArea() 
Local lRet  := .T.

Default lJob := .F. 

FwMsgRun( , {|oSelf| lRet := GI001BReceb(lJob, oSelf)}, , STR0001)		// "Processando registros de Cidades... Aguarde!" 

RestArea(aArea)
GTPDestroy(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001BReceb

Fun��o utilizada para executar o recebimento da integra��o e atualizar o registro

@type 		function
@sample 	GI001BReceb(lJob, oMessage)
@param 		lJob, logical    - informa se a chamada foi realizada atrav�s de job (.T.) ou n�o (.F.) 
			oMessage, objeto - trata a mensagem apresentada em tela
@return 	Logical - resultado do processamento da rotina (.T. / .F.)
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Static Function GI001BReceb(lJob, oMessage)

Local oRJIntegra := GtpRjIntegra():New()
Local cIntID	 := ""
Local cExtID	 := ""
Local cCode		 := ""
Local cCodEstado := ""
Local cCodIBGE	 := ""
Local nX		 := 0
Local nTotReg    := 0 
Local lRet       := .T. 
Local lMessage	 := ValType(oMessage) == 'O'

oRJIntegra:SetPath("/cidade/todas")
oRJIntegra:cMainList := 'cidade'

If oRJIntegra:Get()
	CC2->(DbSetOrder(1))	// CC2_FILIAL+CC2_EST+CC2_CODMUN
	nTotReg := oRJIntegra:GetLenItens()
	For nX := 0 To nTotReg 
		If lMessage .And. !lJob
			oMessage:SetText(I18N(STR0002, {cValtoChar(nX + 1), nTotReg + 1}))		// "Processando registros de Cidades - #1/#2... Aguarde!" 
			ProcessMessages()
		EndIf

		cExtID     := oRJIntegra:GetJsonValue(nX, 'idCidade', 'C')  
		cCodEstado := oRJIntegra:GetJsonValue(nX, 'idEstado', 'C')
		cCodIBGE   := oRJIntegra:GetJsonValue(nX, 'codIBGE', 'C')
		cDescMun   := oRJIntegra:GetJsonValue(nX, 'descCidade', 'C')
		
		If !Empty(cExtID) .And. !Empty(cCodEstado) .And. !Empty(cCodIBGE) .And. !Empty(cDescMun)
			cExtID	   := cValToChar(cExtID)
			cCode      := GTPxRetId("TotalBus", "CC2", "CC2_CODMUN", cExtID, @cIntID, 4)
			cCodEstado := GTPxRetId("TotalBus", "SX5", "X5_CHAVE", cValToChar(cCodEstado), , 4)
			cCodIBGE   := cValToChar(cCodIBGE)
			
			If Len(cCodIBGE) > 5 //Com Estado
				cCodIBGE := SubStr(cCodIBGE, 3)
			EndIf
			
			If Empty(cCode) .And. !Empty(cCodEstado)
				If CC2->(DbSeek(xFilial('CC2') + cCodEstado + cCodIBGE))
					CFGA070MNT("TotalBus", "CC2", "CC2_CODMUN", cExtID, GTPxMakeId({cCodEstado, cCodIBGE}, "CC2"))
				Else
					If RecLock('CC2', .T.)
			  			CC2->CC2_FILIAL	:= xFilial('CC2')				
						CC2->CC2_EST	:= cCodEstado
						CC2->CC2_CODMUN := cCodIBGE
						CC2->CC2_MUN	:= cDescMun
						CFGA070MNT("TotalBus", "CC2", "CC2_CODMUN", cExtID, GTPxMakeId({cCodEstado, cCodIBGE}, "CC2"))
					EndIf
					CC2->(MsUnlock())
				EndIf
			EndIf
		EndIf	
	Next nX	
Else
	oRJIntegra:oGTPLog:SetText(I18N(STR0003, {oRJIntegra:GetLastError()}))		// "Falha ao processar o retorno do servi�o (#1)." 
EndIf

If !lJob .And. oRJIntegra:oGTPLog:HasInfo() 
	oRJIntegra:oGTPLog:ShowLog()
	lRet := .F.
ElseIf !lJob .And. !oRJIntegra:oGTPLog:HasInfo()
	If lMessage 
		oMessage:SetText(STR0004)		// "Processo finalizado." 
		ProcessMessages()
	Else
		Alert(STR0004)		// "Processo finalizado."
	EndIf	
EndIf

oRJIntegra:Destroy()

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI001BJob

Fun��o utilizada para consumir o servi�o atrav�s de um JOB

@type 		function
@sample 	GI001BJob(aParams)
@param		aParam, array - lista de par�metros 	 	
@return 	
@author 	thiago.tavares
@since 		28/03/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GI001BJob(aParams)

//---Inicio Ambiente
RPCSetType(3)
RpcSetEnv(aParams[1], aParams[2])

GTPIRJB001(.T.)

RpcClearEnv()

Return