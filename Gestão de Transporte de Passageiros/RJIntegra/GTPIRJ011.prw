#Include "Protheus.ch"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "GTPIRJ011.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPIRJ011

Adapter REST da rotina de CATEGORIAS DE LINHA

@type 		function
@sample 	GTPIRJ011(lJob)
@param 	 	lJob, logical - indica se a chamada foi realizada atrav�s de JOB (.T.) ou n�o (.F.)
@return		Logical - informa se o processo foi finalizado com sucesso (.T.) ou n�o (.F.)	 	
@author 	jacomo.fernandes
@since 		03/04/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GTPIRJ011(lJob)

Local aArea  := GetArea() 
Local lRet   := .T.

Default lJob := .F. 

FwMsgRun( , {|oSelf| lRet := GI011Receb(lJob, oSelf)}, , STR0001)		// "Processando registros de Categorias de Linha... Aguarde!" 

RestArea(aArea)
GTPDestroy(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI011Receb

Fun��o utilizada para executar o recebimento da integra��o e atualizar o registro

@type 		function
@sample 	GI011Receb(lJob, oMessage)
@param 		lJob, logical    - informa se a chamada foi realizada atrav�s de job (.T.) ou n�o (.F.) 
			oMessage, objeto - trata a mensagem apresentada em tela
@return 	lRet, logical    - resultado do processamento da rotina (.T. / .F.)
@author 	jacomo.fernandes
@since 		03/04/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Static Function GI011Receb(lJob, oMessage)

Local oRJIntegra  := GtpRjIntegra():New()
Local oModel	  := FwLoadModel("GTPA011")
Local oMdlGYR	  := Nil
Local aFldDePara  := {}
Local aDeParaXXF  := {}
Local aCampos	  := {"GYR_FILIAL", "GYR_CODIGO"}
Local cIntID	  := ""
Local cIntAux     := ""
Local cExtID	  := ""
Local cCode		  := ""
Local cErro		  := ""
Local cTagName    := ""
Local cCampo      := ""
Local cTipoCpo    := ""
Local xValor      := ""
Local nX          := 0
Local nY          := 0
Local nOpc		  := 0
Local nTotReg     := 0
Local lOk		  := .F.
Local lRet        := .T.
Local lContinua   := .T.
Local lOnlyInsert := .F.
Local lOverWrite  := .F.
Local lMessage	  := ValType(oMessage) == 'O'

oRJIntegra:SetPath("/classe/todas")
oRJIntegra:SetServico("Classe")

aFldDePara	:= oRJIntegra:GetFieldDePara()
aDeParaXXF  := oRJIntegra:GetFldXXF()

If oRJIntegra:Get()
	GYR->(DbSetOrder(1))	// GYR_FILIAL+GYR_CODIGO
	nTotReg := oRJIntegra:GetLenItens()	
	For nX := 0 To nTotReg
		lContinua := .T.
		If lMessage .And. !lJob
			oMessage:SetText(I18N(STR0002, {cValtoChar(nX + 1), nTotReg + 1}))		// "Processando registros de Categorias de Linha - #1/#2... Aguarde!" 
			ProcessMessages()
		EndIf
		
		If !Empty(cExtID := oRJIntegra:GetJsonValue(nX, 'idClasse' ,'C'))
		  	cCode := GTPxRetId("TotalBus", "GYR", "GYR_CODIGO", cExtID, @cIntID, 3, @lOk, @cErro, aCampos, 1)
			If Empty(cIntID) 
				nOpc := MODEL_OPERATION_INSERT
			ElseIf lOk .And. GYR->(DbSeek(xFilial('GYR') + cCode))
				nOpc := MODEL_OPERATION_UPDATE
			Else
				lContinua := .F.
				oRJIntegra:oGTPLog:SetText(cErro)
			EndIf
			
			If lContinua
				oModel:SetOperation(nOpc)
				If oModel:Activate()
					oMdlGYR	:= oModel:GetModel("GYRMASTER")

					For nY := 1 To Len(aFldDePara)
						// recuperando a TAG e o respectivo campo da tabela 
						cTagName    := aFldDePara[nY][1] 
						cCampo      := aFldDePara[nY][2]
						cTipoCpo    := aFldDePara[nY][3]
						lOnlyInsert := aFldDePara[nY][6]
						lOverWrite  := aFldDePara[nY][7]

						// recuperando atrav�s da TAG o valor a ser inserido no campo 
						If !Empty(cTagName) .And. !Empty((xValor := oRJIntegra:GetJsonValue(nX, cTagName, cTipoCpo)))

							// verificando a necessidade de realizar o DePara XXF
							If (nPos := aScan(aDeParaXXF, {|x| x[1] == cCampo})) > 0
								xValor := GTPxRetId("TotalBus", aDeParaXXF[nPos, 2], aDeParaXXF[nPos, 3], xValor, @cIntAux, aDeParaXXF[nPos, 4], @lOk, @cErro, aDeParaXXF[nPos, 6], aDeParaXXF[nPos, 5])
							EndIf

							If nOpc == MODEL_OPERATION_INSERT .And. lOnlyInsert .And. Empty(oMdlGYR:GetValue(cCampo)) 
								lContinua := oRJIntegra:SetValue(oMdlGYR, cCampo, xValor)
							ElseIf (nOpc == MODEL_OPERATION_INSERT .And. !lOnlyInsert) .Or. (nOpc == MODEL_OPERATION_UPDATE .And. lOverWrite) 
								lContinua := oRJIntegra:SetValue(oMdlGYR, cCampo, xValor)
							EndIf

							If !lContinua 
								oRJIntegra:oGTPLog:SetText(I18N(STR0003, {cCampo, GTPXErro(oModel)}))		// "Falha ao gravar o valor do campo #1 (#2)." 
								Exit	
							EndIf
						EndIf
					Next nY
						
					If !lContinua 
						Exit
					Else
						If (lContinua := oModel:VldData()) 
							oModel:CommitData()
							CFGA070MNT("TotalBus", "GYR", "GYR_CODIGO", cExtID, IIF(!Empty(cIntId), cIntId, GTPxMakeId(oMdlGYR:GetValue('GYR_CODIGO'), 'GYR')))
						EndIf

						If !lContinua
							oRJIntegra:oGTPLog:SetText(I18N(STR0004, {GTPXErro(oModel)}))		// "Falha ao gravar os dados (#1)." 
						EndIf
					EndIf
					oModel:DeActivate()
				Else
					oRJIntegra:oGTPLog:SetText(I18N(STR0005, {GTPXErro(oModel)}))		// "Falha ao corregar modelos de dados (#1)." 
					Exit
				EndIf
			EndIf
		EndIf  	
	Next nX	
Else
	oRJIntegra:oGTPLog:SetText(I18N(STR0006, {oRJIntegra:GetLastError()}))		// "Falha ao processar o retorno do servi�o (#1)." 
EndIf

If !lJob .And. oRJIntegra:oGTPLog:HasInfo() 
	oRJIntegra:oGTPLog:ShowLog()
	lRet := .F.
ElseIf !lJob .And. !oRJIntegra:oGTPLog:HasInfo()
	If lMessage 
		oMessage:SetText(STR0007)		// "Processo finalizado." 
		ProcessMessages()
	Else
		Alert(STR0007)		// "Processo finalizado."
	EndIf	
EndIf

oRJIntegra:Destroy()
GTPDestroy(oModel)
GTPDestroy(oMdlGYR)
GTPDestroy(aFldDePara)
GTPDestroy(aDeParaXXF)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI011Job

Fun��o utilizada para consumir o servi�o atrav�s de um JOB

@type 		function
@sample 	GI011Job(aParams)
@param		aParam, array - lista de par�metros 	 	
@return 	
@author 	jacomo.fernandes
@since 		03/04/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GI011Job(aParam)

//---Inicio Ambiente
RPCSetType(3)
RpcSetEnv(aParam[1], aParam[2])

GTPIRJ011(.T.)

RpcClearEnv()

Return