#Include "GTPIRJ427.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "FWMVCDEF.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GTPIRJ427

Adapter REST da rotina de Receitas e Despesas

@type 		function
@sample 	GTPIRJ420(lJOb)
@param 	 	lJob, logical - indica se a chamada foi realizada atrav�s de JOB (.T.) ou n�o (.F.)
@return		Logical - informa se o processo foi finalizado com sucesso (.T.) ou n�o (.F.)	 	
@author 	henrique.toyada
@since 		24/07/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Function GTPIRJ427(lJob)

Local aArea  := GetArea() 
Local lRet   := .T.

Default lJob := .F. 

If !Pergunte("GTPIRJ427", !lJob)
	Return
EndIf

FwMsgRun( , {|oSelf| lRet := GI427Receb(lJob, oSelf, MV_PAR01, MV_PAR02, MV_PAR04, MV_PAR03, MV_PAR05, MV_PAR06, MV_PAR07)}, , STR0001) //"Processando registros de Receitas e Despesas... Aguarde!"

RestArea(aArea)
GTPDestroy(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} GI427Receb

Fun��o utilizada para executar o recebimento da integra��o e atualizar o registro

@type 		function
@sample 	GI427Receb(lJob, oMessage)
@param 		lJob, logical    - informa se a chamada foi realizada atrav�s de job (.T.) ou n�o (.F.) 
			oMessage, objeto - trata a mensagem apresentada em tela
@return 	lRet, logical    - resultado do processamento da rotina (.T. / .F.)
@author 	thiago.tavares
@since 		03/04/2019
@version 	1.0
/*/
//------------------------------------------------------------------------------------------
Static Function GI427Receb(lJob, oMessage, cEmpRJ, dDtIni, cHrIni, dDtFim, cHrFim, cAgeIni, cAgeFim)

Local oRJIntegra  := GtpRjIntegra():New()
Local oModel	  := FwLoadModel("GTPA427")
Local oMdlGZT	  := Nil
Local oMdlGZP     := Nil
Local aFldDePara  := {}
Local aDeParaXXF  := {}
Local aCampos	  := {}
Local cIntID	  := ""
Local cIntAux     := ""
Local cExtID	  := ""
Local cCode		  := ""
Local cErro		  := ""
Local cTagName    := ""
Local cCampo      := ""
Local cTipoCpo    := ""
Local xValor      := ""
Local cFormPag    := ""
Local nX          := 0
Local nY          := 0
Local nOpc		  := 0
Local nTotReg     := 0
Local lOk		  := .F.
Local lRet        := .T.
Local lContinua   := .T.
Local lOnlyInsert := .F.
Local lOverWrite  := .F.
Local lMessage	  := ValType(oMessage) == 'O'

Default cEmpRJ    := oRJIntegra:GetEmpRJ(cEmpAnt, cFilAnt)
Default dDtIni    := dDataBase-1
Default cHrIni    := '0000'
Default dDtFim    := dDataBase-1
Default cHrFim    := '2359'
Default cAgeIni   := ''
Default cAgeFim   := ''

oRJIntegra:SetPath("/receitaDespesa")
oRJIntegra:SetServico("ReceitaDespesa")

//Dever� ser alterado depois para pegar os valores corretos
oRJIntegra:SetParam('empresa'     , ALLTRIM(cEmpRJ)                                   )//'10'
oRJIntegra:SetParam('dataInicial' , SUBSTR(DTOS(dDtIni),3,8) + STRTRAN(cHrIni,":","") )//'1903260000'
oRJIntegra:SetParam('dataFinal'   , SUBSTR(DTOS(dDtFim),3,8) + STRTRAN(cHrFim,":","") )//'1903262359'

If !Empty(cAgeIni)
	oRJIntegra:SetParam('agenciaInicio', AllTrim(cAgeIni))
Endif
If !Empty(cAgeFim)
	oRJIntegra:SetParam('agenciaFim', AllTrim(cAgeFim))
Endif

aFldDePara	:= oRJIntegra:GetFieldDePara()
aDeParaXXF  := oRJIntegra:GetFldXXF()

If oRJIntegra:Get()
	GZT->(DbSetOrder(1))	// GZC_FILIAL+GZC_CODIGO
	nTotReg := oRJIntegra:GetLenItens()	
	For nX := 0 To nTotReg
		lContinua := .T.
		If lMessage .And. !lJob
			oMessage:SetText(I18N(STR0002, {cValtoChar(nX + 1), nTotReg + 1})) //"Processando registros de Receitas e Despesas - #1/#2... Aguarde!"
			ProcessMessages()
		EndIf
		
		If !Empty(cExtID := oRJIntegra:GetJsonValue(nX, 'idReceitaDespesa' ,'C',.F.))
		  	cCode := GTPxRetId("TotalBus", "GZT", "GZT_CODIGO", cExtID, @cIntID, 3, @lOk, @cErro, aCampos, 1)
			If Empty(cIntID) 
				nOpc := MODEL_OPERATION_INSERT
			ElseIf lOk .And. GZT->(DbSeek(xFilial('GZT') + cCode))
				nOpc := MODEL_OPERATION_UPDATE
			Else
				lContinua := .F.
				oRJIntegra:oGTPLog:SetText(cErro)
			EndIf
			
			If lContinua
				oModel:SetOperation(nOpc)
				If oModel:Activate()
					oMdlGZT	:= oModel:GetModel("GZTMASTER")
					oMdlGZP	:= oModel:GetModel("GZPPAGT")

					For nY := 1 To Len(aFldDePara)
						// recuperando a TAG e o respectivo campo da tabela 
						cTagName    := aFldDePara[nY][1] 
						cCampo      := aFldDePara[nY][2]
						cTipoCpo    := aFldDePara[nY][3]
						lOnlyInsert := aFldDePara[nY][6]
						lOverWrite  := aFldDePara[nY][7]
						
						cFormPag := oRJIntegra:GetJsonValue(nX, "formaPagamento1", "C")
						
						// recuperando atrav�s da TAG o valor a ser inserido no campo 
						If !Empty(cTagName) .And. !Empty((xValor := oRJIntegra:GetJsonValue(nX, cTagName, cTipoCpo)))

							// verificando a necessidade de realizar o DePara XXF
							If (nPos := aScan(aDeParaXXF, {|x| x[1] == cCampo})) > 0
								xValor := GTPxRetId("TotalBus", aDeParaXXF[nPos, 2], aDeParaXXF[nPos, 3], xValor, @cIntAux, aDeParaXXF[nPos, 4], @lOk, @cErro, aDeParaXXF[nPos, 6], aDeParaXXF[nPos, 5])
							EndIf

							If !(cCampo $ 'GZP_DCART|GZP_ITEM|GZP_TPAGTO|GZP_VALOR|GZP_QNTPAR|GZP_NSU|GZP_AUT')	
								If nOpc == MODEL_OPERATION_INSERT .And. lOnlyInsert .And. Empty(oMdlGZT:GetValue(cCampo)) 
									lContinua := oRJIntegra:SetValue(oMdlGZT, cCampo, xValor)
								ElseIf (nOpc == MODEL_OPERATION_INSERT .And. !lOnlyInsert) .Or. (nOpc == MODEL_OPERATION_UPDATE .And. lOverWrite) 
									lContinua := oRJIntegra:SetValue(oMdlGZT, cCampo, xValor)
								EndIf
							Else
								If cFormPag $ 'CR|DE' 
									If nOpc == MODEL_OPERATION_INSERT .And. lOnlyInsert .And. Empty(oMdlGZP:GetValue(cCampo)) 
										lContinua := oRJIntegra:SetValue(oMdlGZP, cCampo, xValor)
									ElseIf (nOpc == MODEL_OPERATION_INSERT .And. !lOnlyInsert) .Or. (nOpc == MODEL_OPERATION_UPDATE .And. lOverWrite) 
										lContinua := oRJIntegra:SetValue(oMdlGZP, cCampo, xValor)
									EndIf
								EndIf
							EndIf
							If !lContinua 
								oRJIntegra:oGTPLog:SetText(I18N(STR0003, {cCampo, GTPXErro(oModel)})) //"Falha ao gravar o valor do campo #1 (#2)."
								Exit	
							EndIf
						EndIf
					Next nY
						
					If !lContinua 
						Exit
					Else
						If (lContinua := oModel:VldData()) 
							oModel:CommitData()
							CFGA070MNT("TotalBus", "GZT", "GZT_CODIGO", cExtID, IIF(!Empty(cIntId), cIntId, GTPxMakeId(oMdlGZT:GetValue('GZT_CODIGO'), 'GZT')))
						EndIf

						If !lContinua
							oRJIntegra:oGTPLog:SetText(I18N(STR0004, {GTPXErro(oModel)})) //"Falha ao gravar os dados (#1)."
						EndIf
					EndIf
					oModel:DeActivate()
				Else
					oRJIntegra:oGTPLog:SetText(I18N(STR0005, {GTPXErro(oModel)})) //"Falha ao corregar modelos de dados (#1)."
					Exit
				EndIf
			EndIf
		EndIf  	
	Next nX	
Else
	oRJIntegra:oGTPLog:SetText(I18N(STR0006, {oRJIntegra:GetLastError()})) //"Falha ao processar o retorno do servi�o (#1)."
EndIf

If !lJob .And. oRJIntegra:oGTPLog:HasInfo() 
	oRJIntegra:oGTPLog:ShowLog()
	lRet := .F.
ElseIf !lJob .And. !oRJIntegra:oGTPLog:HasInfo()
	If lMessage 
		oMessage:SetText(STR0007) //"Processo finalizado."
		ProcessMessages()
	Else
		Alert(STR0007) //"Processo finalizado."
	EndIf	
EndIf

oRJIntegra:Destroy()
GTPDestroy(oModel)
GTPDestroy(oMdlGZT)
GTPDestroy(aFldDePara)
GTPDestroy(aDeParaXXF)

Return lRet
//GI427Job({'10',STOD('20190326'),'0000',STOD('20190326'),'2359'})
Function GI427Job(aParam)

Local nPosEmp := IF(Len(aParam) > 7, 8, IF(Len(aParam) > 5, 6, 1))
Local nPosFil := IF(Len(aParam) > 7, 9, IF(Len(aParam) > 5, 9, 2))

//---Inicio Ambiente
RPCSetType(3)
RpcSetEnv(aParam[nPosEmp],aParam[nPosFil])
If Len(aParam) == 9
	GI427Receb(.T., Nil, aParam[1], STOD(aParam[2]), aParam[3], STOD(aParam[4]), aParam[5], aParam[6], aParam[7])
ElseIf Len(aParam) == 7
	GI427Receb(.T., Nil, aParam[1], STOD(aParam[2]), aParam[3], STOD(aParam[4]), aParam[5])
Else
	GTPIRJ427(.T.)
EndIf

RpcClearEnv()

Return