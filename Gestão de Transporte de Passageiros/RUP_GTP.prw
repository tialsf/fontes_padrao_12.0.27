#Include 'Protheus.ch'

#DEFINE	DF_CAMPO	1
#DEFINE	DF_CONTEUDO	2

//------------------------------------------------------------------------------
/*/{Protheus.doc} RUP_GTP()
Fun��es de compatibiliza��o e/ou convers�o de dados para as tabelas do sistema.
@sample		RUP_GTP("12", "2", "003", "005", "BRA")
@param		cVersion	- Vers�o do Protheus 
@param		cMode		- Modo de execu��o	- "1" = Por grupo de empresas / "2" =Por grupo de empresas + filial (filial completa)
@param		cRelStart	- Release de partida	- (Este seria o Release no qual o cliente est�)
@param		cRelFinish	- Release de chegada	- (Este seria o Release ao final da atualiza��o)
@param		cLocaliz	- Localiza��o (pa�s)	- Ex. "BRA"
@return		Nil
@author		jacomo.fernandes
@since		31/03/2017
@version	12
/*/
//------------------------------------------------------------------------------
Function RUP_GTP(cVersion, cMode, cRelStart, cRelFinish, cLocaliz)

Local aArea		:= GetArea()
Local aAreaSX2  := SX2->(GetArea())
Local aAreaSX3  := SX3->(GetArea())
Local aAreaSX7	:= SX7->(GetArea())
Local lJob   	:= Iif(Select("SX6")==0,.T.,.F.)
Local aSX2      := {}
Local aSX3		:= {}
Local aDelSx2   := {}
Local aDelSx3	:= {}
Local aDelSx7	:= {}
Local aDelSx9	:= {}
Local aDelSix	:= {}
Local aAtuTab	:= {}
Local nX        := 0
Local nStart    := Time()

Default cMode	:= "1"

If cMode == "1"
	If GTPxVldDic('G52', , .T., .F.)
		aAdd(aSX2,{"G52"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G53', , .T., .F.)
		aAdd(aSX2,{"G53"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZH', , .T., .F.)
		aAdd(aSX2,{"GZH"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G55', , .T., .F.)
		aAdd(aSX2,{"G55"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5H', , .T., .F.)
		aAdd(aSX2,{"G5H"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G5F', , .T., .F.)
		aAdd(aSX2,{"G5F"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GI0', , .T., .F.)
		aAdd(aSX2,{"GI0"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GQD', , .T., .F.)
		aAdd(aSX2,{"GQD"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GI1', , .T., .F.)
		aAdd(aSX2,{"GI1"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G57', , .T., .F.)
		aAdd(aSX2,{"G57"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G59', , .T., .F.)
		aAdd(aSX2,{"G59"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5D', , .T., .F.)
		aAdd(aSX2,{"G5D"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYM', , .T., .F.)
		aAdd(aSX2,{"GYM"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G5E', , .T., .F.)
		aAdd(aSX2,{"G5E"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5G', , .T., .F.)
		aAdd(aSX2,{"G5G"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5I', , .T., .F.)
		aAdd(aSX2,{"G5I"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5L', , .T., .F.)
		aAdd(aSX2,{"G5L"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9U', , .T., .F.)
		aAdd(aSX2,{"G9U"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G6T', , .T., .F.)
		aAdd(aSX2,{"G6T"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G6X', , .T., .F.)
		aAdd(aSX2,{"G6X"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G6Y', , .T., .F.)
		aAdd(aSX2,{"G6Y"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G6Z', , .T., .F.)
		aAdd(aSX2,{"G6Z"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYA', , .T., .F.)
		aAdd(aSX2,{"GYA"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G94', , .T., .F.)
		aAdd(aSX2,{"G94"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYL', , .T., .F.)
		aAdd(aSX2,{"GYL"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GYK', , .T., .F.)
		aAdd(aSX2,{"GYK"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GYR', , .T., .F.)
		aAdd(aSX2,{"GYR"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GYW', , .T., .F.)
		aAdd(aSX2,{"GYW"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G95', , .T., .F.)
		aAdd(aSX2,{"G95"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQC', , .T., .F.)
		aAdd(aSX2,{"GQC"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZW', , .T., .F.)
		aAdd(aSX2,{"GZW"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G96', , .T., .F.)
		aAdd(aSX2,{"G96"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9D', , .T., .F.)
		aAdd(aSX2,{"G9D"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9V', , .T., .F.)
		aAdd(aSX2,{"G9V"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G5K', , .T., .F.)
		aAdd(aSX2,{"G5K"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZI', , .T., .F.)
		aAdd(aSX2,{"GZI"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ5', , .T., .F.)
		aAdd(aSX2,{"GZ5"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ6', , .T., .F.)
		aAdd(aSX2,{"GZ6"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ7', , .T., .F.)
		aAdd(aSX2,{"GZ7"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GI5', , .T., .F.)
		aAdd(aSX2,{"GI5"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GY5', , .T., .F.)
		aAdd(aSX2,{"GY5"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GY6', , .T., .F.)
		aAdd(aSX2,{"GY6"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GY7', , .T., .F.)
		aAdd(aSX2,{"GY7"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ4', , .T., .F.)
		aAdd(aSX2,{"GZ4"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ8', , .T., .F.)
		aAdd(aSX2,{"GZ8"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ9', , .T., .F.)
		aAdd(aSX2,{"GZ9"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G9O', , .T., .F.)
		aAdd(aSX2,{"G9O"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G58', , .T., .F.)
		aAdd(aSX2,{"G58"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('G9X', , .T., .F.)
		aAdd(aSX2,{"G9X"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9Y', , .T., .F.)
		aAdd(aSX2,{"G9Y"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9Z', , .T., .F.)
		aAdd(aSX2,{"G9Z"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI2', , .T., .F.)
		aAdd(aSX2,{"GI2"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI3', , .T., .F.)
		aAdd(aSX2,{"GI3"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI4', , .T., .F.)
		aAdd(aSX2,{"GI4"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI6', , .T., .F.)
		aAdd(aSX2,{"GI6"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI8', , .T., .F.)
		aAdd(aSX2,{"GI8"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9A', , .T., .F.)
		aAdd(aSX2,{"G9A"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GIC', , .T., .F.)
		aAdd(aSX2,{"GIC"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GID', , .T., .F.)
		aAdd(aSX2,{"GID"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIE', , .T., .F.)
		aAdd(aSX2,{"GIE"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIH', , .T., .F.)
		aAdd(aSX2,{"GIH"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9B', , .T., .F.)
		aAdd(aSX2,{"G9B"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GII', , .T., .F.)
		aAdd(aSX2,{"GII"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIM', , .T., .F.)
		aAdd(aSX2,{"GIM"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9T', , .T., .F.)
		aAdd(aSX2,{"G9T"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GQ1', , .T., .F.)
		aAdd(aSX2,{"GQ1"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ2', , .T., .F.)
		aAdd(aSX2,{"GQ2"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ3', , .T., .F.)
		aAdd(aSX2,{"GQ3"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ4', , .T., .F.)
		aAdd(aSX2,{"GQ4"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ6', , .T., .F.)
		aAdd(aSX2,{"GQ6"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ7', , .T., .F.)
		aAdd(aSX2,{"GQ7"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ9', , .T., .F.)
		aAdd(aSX2,{"GQ9"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQA', , .T., .F.)
		aAdd(aSX2,{"GQA"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQE', , .T., .F.)
		aAdd(aSX2,{"GQE"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQG', , .T., .F.)
		aAdd(aSX2,{"GQG"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQH', , .T., .F.)
		aAdd(aSX2,{"GQH"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQK', , .T., .F.)
		aAdd(aSX2,{"GQK"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQL', , .T., .F.)
		aAdd(aSX2,{"GQL"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQM', , .T., .F.)
		aAdd(aSX2,{"GQM"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQP', , .T., .F.)
		aAdd(aSX2,{"GQP"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQQ', , .T., .F.)
		aAdd(aSX2,{"GQQ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQS', , .T., .F.)
		aAdd(aSX2,{"GQS"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQT', , .T., .F.)
		aAdd(aSX2,{"GQT"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZS', , .T., .F.)
		aAdd(aSX2,{"GZS"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GQU', , .T., .F.)
		aAdd(aSX2,{"GQU"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQV', , .T., .F.)
		aAdd(aSX2,{"GQV"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQW', , .T., .F.)
		aAdd(aSX2,{"GQW"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQX', , .T., .F.)
		aAdd(aSX2,{"GQX"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQY', , .T., .F.)
		aAdd(aSX2,{"GQY"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY1', , .T., .F.)
		aAdd(aSX2,{"GY1"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY2', , .T., .F.)
		aAdd(aSX2,{"GY2"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY3', , .T., .F.)
		aAdd(aSX2,{"GY3"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY4', , .T., .F.)
		aAdd(aSX2,{"GY4"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY8', , .T., .F.)
		aAdd(aSX2,{"GY8"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYE', , .T., .F.)
		aAdd(aSX2,{"GYE"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYF', , .T., .F.)
		aAdd(aSX2,{"GYF"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYG', , .T., .F.)
		aAdd(aSX2,{"GYG"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYH', , .T., .F.)
		aAdd(aSX2,{"GYH"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYI', , .T., .F.)
		aAdd(aSX2,{"GYI"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYJ', , .T., .F.)
		aAdd(aSX2,{"GYJ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYN', , .T., .F.)
		aAdd(aSX2,{"GYN"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYO', , .T., .F.)
		aAdd(aSX2,{"GYO"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYP', , .T., .F.)
		aAdd(aSX2,{"GYP"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYQ', , .T., .F.)
		aAdd(aSX2,{"GYQ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYT', , .T., .F.)
		aAdd(aSX2,{"GYT"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQ5', , .T., .F.)
		aAdd(aSX2,{"GQ5"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GYU', , .T., .F.)
		aAdd(aSX2,{"GYU"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYZ', , .T., .F.)
		aAdd(aSX2,{"GYZ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZ0', , .T., .F.)
		aAdd(aSX2,{"GZ0"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZ1', , .T., .F.)
		aAdd(aSX2,{"GZ1"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZC', , .T., .F.)
		aAdd(aSX2,{"GZC"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'C'},{'X2_MODOEMP' ,'C'} } })
	EndIf
	If GTPxVldDic('GZ2', , .T., .F.)
		aAdd(aSX2,{"GZ2"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZA', , .T., .F.)
		aAdd(aSX2,{"GZA"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZB', , .T., .F.)
		aAdd(aSX2,{"GZB"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZD', , .T., .F.)
		aAdd(aSX2,{"GZD"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZE', , .T., .F.)
		aAdd(aSX2,{"GZE"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZF', , .T., .F.)
		aAdd(aSX2,{"GZF"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZG', , .T., .F.)
		aAdd(aSX2,{"GZG"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZJ', , .T., .F.)
		aAdd(aSX2,{"GZJ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZK', , .T., .F.)
		aAdd(aSX2,{"GZK"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5C', , .T., .F.)
		aAdd(aSX2,{"G5C"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G6R', , .T., .F.)
		aAdd(aSX2,{"G6R"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G6S', , .T., .F.)
		aAdd(aSX2,{"G6S"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G9C', , .T., .F.)
		aAdd(aSX2,{"G9C"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZL', , .T., .F.)
		aAdd(aSX2,{"GZL"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZM', , .T., .F.)
		aAdd(aSX2,{"GZM"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZO', , .T., .F.)
		aAdd(aSX2,{"GZO"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZP', , .T., .F.)
		aAdd(aSX2,{"GZP"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZQ', , .T., .F.)
		aAdd(aSX2,{"GZQ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZT', , .T., .F.)
		aAdd(aSX2,{"GZT"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZU', , .T., .F.)
		aAdd(aSX2,{"GZU"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIN', , .T., .F.)
		aAdd(aSX2,{"GIN"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIO', , .T., .F.)
		aAdd(aSX2,{"GIO"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIP', , .T., .F.)
		aAdd(aSX2,{"GIP"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIQ', , .T., .F.)
		aAdd(aSX2,{"GIQ"		,{{'X2_MODO' ,'E'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZV', , .T., .F.)
		aAdd(aSX2,{"GZV"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIR', , .T., .F.)
		aAdd(aSX2,{"GIR"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIY', , .T., .F.)
		aAdd(aSX2,{"GIY"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIX', , .T., .F.)
		aAdd(aSX2,{"GIX"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIW', , .T., .F.)
		aAdd(aSX2,{"GIW"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIV', , .T., .F.)
		aAdd(aSX2,{"GIV"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIU', , .T., .F.)
		aAdd(aSX2,{"GIU"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQZ', , .T., .F.)
		aAdd(aSX2,{"GQZ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQI', , .T., .F.)
		aAdd(aSX2,{"GQI"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYD', , .T., .F.)
		aAdd(aSX2,{"GYD"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - inicio
	If GTPxVldDic('G56', , .T., .F.)
		aAdd(aSX2,{"G56"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('G5B', , .T., .F.)
		aAdd(aSX2,{"G5B"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI7', , .T., .F.)
		aAdd(aSX2,{"GI7"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GI9', , .T., .F.)
		aAdd(aSX2,{"GI9"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIA', , .T., .F.)
		aAdd(aSX2,{"GIA"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIB', , .T., .F.)
		aAdd(aSX2,{"GIB"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIF', , .T., .F.)
		aAdd(aSX2,{"GIF"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIG', , .T., .F.)
		aAdd(aSX2,{"GIG"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIJ', , .T., .F.)
		aAdd(aSX2,{"GIJ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIK', , .T., .F.)
		aAdd(aSX2,{"GIK"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIL', , .T., .F.)
		aAdd(aSX2,{"GIL"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIS', , .T., .F.)
		aAdd(aSX2,{"GIS"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIT', , .T., .F.)
		aAdd(aSX2,{"GIT"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GIU', , .T., .F.)
		aAdd(aSX2,{"GIU"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQF', , .T., .F.)
		aAdd(aSX2,{"GQF"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZN', , .T., .F.)
		aAdd(aSX2,{"GZN"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GZR', , .T., .F.)
		aAdd(aSX2,{"GZR"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf

	If GTPxVldDic('GQ8', , .T., .F.)
		aAdd(aSX2,{"GQ8"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQB', , .T., .F.)
		aAdd(aSX2,{"GQB"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GY0', , .T., .F.)
		aAdd(aSX2,{"GY0"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYD', , .T., .F.)
		aAdd(aSX2,{"GYD"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQI', , .T., .F.)
		aAdd(aSX2,{"GQI"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQJ', , .T., .F.)
		aAdd(aSX2,{"GQJ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GYX', , .T., .F.)
		aAdd(aSX2,{"GYX"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf
	If GTPxVldDic('GQZ', , .T., .F.)
		aAdd(aSX2,{"GQZ"		,{{'X2_MODO' ,'C'},{'X2_MODOUN' ,'E'},{'X2_MODOEMP' ,'E'} } })
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - fim

	For nX :=1 To Len(aSX2)
		If !ExistDadosTabela(aSX2[nX][1]) 
			AjustaSX2({aSX2[nX]})	
		EndIf
	Next nX

	If GTPxVldDic('G6X',{"G6X_DESCAG"} , .T., .T.)
		aAdd(aSx3,{"G6X_DESCAG"	,{ {'X3_INIBRW','POSICIONE("GI6",1,XFILIAL("GI6")+G6X->G6X_AGENCI,"GI6_DESCRI")'} } })
	EndIf
	If GTPxVldDic('GYT',{"GYT_DESCRI"} , .T., .T.)
		aAdd(aSx3,{"GYT_DESCRI"	,{ {'X3_INIBRW','POSICIONE("GI1",1,XFILIAL("GI1")+GYT->GYT_LOCALI,"GI1_DESCRI")'} } })
	EndIf
	If GTPxVldDic('G6T',{"G6T_DESCRI"} , .T., .T.)
		aAdd(aSx3,{"G6T_DESCRI"	,{ {'X3_INIBRW','fDesc("GI6",G6T->G6T_AGENCI,"GI6_DESCRI")'} } })
	EndIf
	If GTPxVldDic('G99',{"G99_NOMREM"} , .T., .T.)
		aAdd(aSx3,{"G99_NOMREM"	,{ {'X3_INIBRW','fDesc("SA1", G99->G99_CLIREM+G99->G99_LOJREM,"A1_NOME") '} } })    
	EndIf
	If GTPxVldDic('G99',{"G99_NOMDES"} , .T., .T.)
		aAdd(aSx3,{"G99_NOMDES"	,{ {'X3_INIBRW','fDesc("SA1", G99->G99_CLIDES+G99->G99_LOJDES,"A1_NOME") '} } })    
	EndIf
	If GTPxVldDic('G99',{"G99_DESEMI"} , .T., .T.)
		aAdd(aSx3,{"G99_DESEMI"	,{ {'X3_INIBRW','fDesc("GI6", G99->G99_CODEMI,"GI6_DESCRI")              '} } })    
	EndIf
	If GTPxVldDic('G99',{"G99_DESREC"} , .T., .T.)
		aAdd(aSx3,{"G99_DESREC"	,{ {'X3_INIBRW','fDesc("GI6", G99->G99_CODREC,"GI6_DESCRI")              '} } })    
	EndIf
	If GTPxVldDic('G99',{"G99_DESPRO"} , .T., .T.)
		aAdd(aSx3,{"G99_DESPRO"	,{ {'X3_INIBRW','fDesc("SB1", G99->G99_CODPRO,"B1_DESC")                 '} } })  
	EndIf
	If GTPxVldDic('GQH',{"GQH_TPDESC"} , .T., .T.)
		aAdd(aSx3,{"GQH_TPDESC"	,{ {'X3_INIBRW','POSICIONE("GYA",1,XFILIAL("GYA")+GQH->GQH_TIPO,"GYA_DESCRI") '} } })
	EndIf
	If GTPxVldDic('GQH',{"GQH_TPDESC"} , .T., .T.)
		aAdd(aSx3,{"GQH_TPDESC"	,{ {'X3_RELACAO','IIF(!INCLUI,POSICIONE("GYA",1,XFILIAL("GYA")+GQH->GQH_TIPO,"GYA_DESCRI"),"")'} } })
	EndIf
	If GTPxVldDic('GQP',{"GQP_DEPART"} , .T., .T.)
		aAdd(aSx3,{"GQP_DEPART"	,{ {'X3_VISUAL','A'} } })
	EndIf
	If GTPxVldDic('GYN',{"GYN_CONF"} , .T., .T.)
		aAdd(aSx3,{"GYN_CONF"	,{ {'X3_RELACAO','2'} } })
	EndIf
	If GTPxVldDic('GQS',{"GQS_AGENCI"} , .T., .T.)
		aAdd(aSx3,{"GQS_AGENCI"	,{ {'X3_VISUAL','A'} } })
	EndIf
	If GTPxVldDic('GIC',{"GIC_AGENCI"} , .T., .T.)
		aAdd(aSx3,{"GIC_AGENCI"	,{ {'X3_VISUAL','A'} } })
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - inicio
	If GTPxVldDic('G57',{"G57_CODIGO"} , .T., .T.)
		aAdd(aSx3,{"G57_CODIGO"	,{ {'X3_F3','GIIFIL'} } })
	EndIf
	If GTPxVldDic('GI2',{"GI2_KMTOTA"} , .T., .T.)
		aAdd(aSx3,{"GI2_KMTOTA"	,{ {'X3_TAMANHO', 9} } }) 
	EndIf
	If GTPxVldDic('GI2',{"GI2_KMTOTA"} , .T., .T.)
		aAdd(aSx3,{"GI2_KMTOTA"	,{ {'X3_PICTURE', '@E 999,999.99'} } })
	EndIf
	If GTPxVldDic('GI8',{"GI8_AGENCI"} , .T., .T.)
		aAdd(aSx3,{"GI8_AGENCI" ,{ {'X3_TAMANHO', 8} } }) 
	EndIf
	If GTPxVldDic('GI2',{"GI2_NUMMOV"} , .T., .T.)
		aAdd(aSx3,{"GI2_NUMMOV"	,{ {'X3_PICTURE', '999999999999'} } })
	EndIf
	If GTPxVldDic('GIE',{"GIE_NLOCHR"} , .T., .T.)
		aAdd(aSx3,{"GIE_NLOCHR"	,{ {'X3_INIBRW','POSICIONE("GI1",1,XFILIAL("GI1")+GIE->GIE_LOCHOR,"GI1_DESCRI"),"")'} } })
	EndIf
	If GTPxVldDic('GQL',{"GQL_CODIGO"} , .T., .T.)
		aAdd(aSx3,{"GQL_CODIGO"	,{ {'X3_VISUAL','V'} } })
	EndIf
	//Adicionado para ajuste de dicion�rio base congelada - fim

	If Len(aSX3) > 0
		AjustaSx3(aSX3,@aAtuTab)
	Endif

	If Len(aAtuTab) > 0
		UpdBanco(aAtuTab)
	Endif	
	//Tabelas n�o utilizadas
	AADD(aDelSx2,"GY9")
	AADD(aDelSx2,"GYS")
	AADD(aDelSx2,"GYV")
	AADD(aDelSx2,"GYY")

	IF Len(aDelSx2) > 0
		DelSx2(aDelSx2)
	Endif

	If GTPxVldDic('GI2',{"GI2_CATEG"} , .T., .T.)
		aAdd(aDelSX7, {'GI2_CATEG ','002'})
	EndIf
	If GTPxVldDic('G56',{"G56_CODVEI"} , .T., .T.)
		aAdd(aDelSX7, {'G56_CODVEI','001'})
	EndIf
	If GTPxVldDic('G56',{"G56_SGSEGU"} , .T., .T.)
		aAdd(aDelSX7, {'G56_SGSEGU','001'})
	EndIf
	If GTPxVldDic('G56',{"G56_SGTIPO"} , .T., .T.)
		aAdd(aDelSX7, {'G56_SGTIPO','001'})
	EndIf
	If GTPxVldDic('G56',{"G56_VITIPO"} , .T., .T.)
		aAdd(aDelSX7, {'G56_VITIPO','001'})
	EndIf
	If GTPxVldDic('GI5',{"GI5_MOTORI"} , .T., .T.)
		aAdd(aDelSX7, {'GI5_MOTORI','001'})
	EndIf
	If GTPxVldDic('GIB',{"GIB_AGENCI"} , .T., .T.)
		aAdd(aDelSX7, {'GIB_AGENCI','001'})
	EndIf
	If GTPxVldDic('GIB',{"GIB_FILIAL"} , .T., .T.)
		aAdd(aDelSX7, {'GIB_FILIAL','001'})
	EndIf
	If GTPxVldDic('GIB',{"GIB_LINHA"} , .T., .T.)
		aAdd(aDelSX7, {'GIB_LINHA ','001'})
	EndIf
	If GTPxVldDic('GIH',{"GIH_CODTER"} , .T., .T.)
		aAdd(aDelSX7, {'GIH_CODTER','001'})
	EndIf
	If GTPxVldDic('GIH',{"GIH_TPDOC"} , .T., .T.)
		aAdd(aDelSX7, {'GIH_TPDOC ','001'})
	EndIf
	If GTPxVldDic('GIJ',{"GIJ_CLICAR"} , .T., .T.)
		aAdd(aDelSX7, {'GIJ_CLICAR','001'})
	EndIf
	If GTPxVldDic('GIJ',{"GIJ_LOJCAR"} , .T., .T.)
		aAdd(aDelSX7, {'GIJ_LOJCAR','001'})
	EndIf
	If GTPxVldDic('GIS',{"GIS_CODREQ"} , .T., .T.)
		aAdd(aDelSX7, {'GIS_CODREQ','001'})
	EndIf
	If GTPxVldDic('GIU',{"GIU_CODREQ"} , .T., .T.)
		aAdd(aDelSX7, {'GIU_CODREQ','001'})
	EndIf
	If GTPxVldDic('GIY',{"GIY_VEICUL"} , .T., .T.)
		aAdd(aDelSX7, {'GIY_VEICUL','001'})
	EndIf
	If GTPxVldDic('GIY',{"GIY_VEICUL"} , .T., .T.)
		aAdd(aDelSX7, {'GIY_VEICUL','001'})
	EndIf
	If GTPxVldDic('GIZ',{"GIZ_COD"} , .T., .T.)
		aAdd(aDelSX7, {'GIZ_COD'   ,'001'})
	EndIf
	If GTPxVldDic('GIZ',{"GIZ_VEICUL"} , .T., .T.)
		aAdd(aDelSX7, {'GIZ_VEICUL','001'})
	EndIf
	If GTPxVldDic('GQA',{"GQA_CODVEI"} , .T., .T.)
		aAdd(aDelSX7, {'GQA_CODVEI','002'})
	EndIf
	If GTPxVldDic('GY9',{"GY9_CODCAT"} , .T., .T.)
		aAdd(aDelSX7, {'GY9_CODCAT','001'})
	EndIf
	If GTPxVldDic('GYY',{"GYY_CODACS"} , .T., .T.)
		aAdd(aDelSX7, {'GYY_CODACS','001'})
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - inicio

	If GTPxVldDic('GI2',{"GI2_LINHA"} , .T., .T.)
		aAdd(aDelSX7, {'GI2_LINHA' ,'001'})
	EndIf
	If GTPxVldDic('GIE',{"GIE_LINHA"} , .T., .T.)
		aAdd(aDelSX7, {'GIE_LINHA' ,'001'})
	EndIf
	If GTPxVldDic('GIE',{"GIE_LOCHOR"} , .T., .T.)
		aAdd(aDelSX7, {'GIE_LOCHOR','001'})
	EndIf
	If GTPxVldDic('GIN',{"GIN_DCHEGA"} , .T., .T.)
		aAdd(aDelSX7, {'GIN_DCHEGA','001'})
	EndIf
	If GTPxVldDic('GIN',{"GIN_HCHEGA"} , .T., .T.)
		aAdd(aDelSX7, {'GIN_HCHEGA','001'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_CUSTO"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_CUSTO' ,'001'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_CUSTO"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_CUSTO' ,'002'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_CUSUNI"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_CUSUNI','001'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_FILIAL"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_FILIAL','001'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_QUANT"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_QUANT' ,'001'})
	EndIf
	If GTPxVldDic('GIO',{"GIO_UM"} , .T., .T.)
		aAdd(aDelSX7, {'GIO_UM'    ,'001'})
	EndIf
	If GTPxVldDic('GIP',{"GIP_CODBEM"} , .T., .T.)
		aAdd(aDelSX7, {'GIP_CODBEM','001'})
	EndIf
	If GTPxVldDic('GIP',{"GIP_CODBEM"} , .T., .T.)
		aAdd(aDelSX7, {'GIP_CODBEM','002'})
	EndIf
	If GTPxVldDic('GIP',{"GIP_CONFIG"} , .T., .T.)
		aAdd(aDelSX7, {'GIP_CONFIG','001'})
	EndIf
	If GTPxVldDic('GIQ',{"GIQ_TRECHO"} , .T., .T.)
		aAdd(aDelSX7, {'GIQ_TRECHO','001'})
	EndIf
	//Adicionado para ajuste de dicion�rio base congelada - fim

	If GTPxVldDic('GQ8',{"GQ8_CODLOC"} , .T., .T.)
		aAdd(aDelSX7, {'GQ8_CODLOC','001'})
	EndIf

	IF Len(aDelSx7) > 0
		DelSx7(aDelSx7)
	Endif

	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('CAX',, .T., .F.)
		aAdd(aDelSX9, {'G56','CAX'})
	EndIf
	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('DA3',, .T., .F.)
		aAdd(aDelSX9, {'G56','DA3'})
	EndIf
	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('G57',, .T., .F.)
		aAdd(aDelSX9, {'G56','G57'})
	EndIf
	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('G58',, .T., .F.)
		aAdd(aDelSX9, {'G56','G58'})
	EndIf
	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('G59',, .T., .F.)
		aAdd(aDelSX9, {'G56','G59'})
	EndIf
	If GTPxVldDic('GI5',, .T., .F.) .AND. GTPxVldDic('GYG',, .T., .F.)
		aAdd(aDelSX9, {'GI5','GYG'})
	EndIf
	If GTPxVldDic('GIB',, .T., .F.) .AND. GTPxVldDic('GI2',, .T., .F.)
		aAdd(aDelSX9, {'GIB','GI2'})
	EndIf
	If GTPxVldDic('GIB',, .T., .F.) .AND. GTPxVldDic('GI5',, .T., .F.)
		aAdd(aDelSX9, {'GIB','GI5'})
	EndIf
	If GTPxVldDic('GIB',, .T., .F.) .AND. GTPxVldDic('GI6',, .T., .F.)
		aAdd(aDelSX9, {'GIB','GI6'})
	EndIf
	If GTPxVldDic('GIF',, .T., .F.) .AND. GTPxVldDic('GI2',, .T., .F.)
		aAdd(aDelSX9, {'GIF','GI2'})
	EndIf
	If GTPxVldDic('GIH',, .T., .F.) .AND. GTPxVldDic('GI7',, .T., .F.)
		aAdd(aDelSX9, {'GIH','GI7'})
	EndIf
	If GTPxVldDic('GIS',, .T., .F.) .AND. GTPxVldDic('GIR',, .T., .F.)
		aAdd(aDelSX9, {'GIS','GIR'})
	EndIf
	If GTPxVldDic('GIT',, .T., .F.) .AND. GTPxVldDic('GIR',, .T., .F.)
		aAdd(aDelSX9, {'GIT','GIR'})
	EndIf
	If GTPxVldDic('GIU',, .T., .F.) .AND. GTPxVldDic('GIR',, .T., .F.)
		aAdd(aDelSX9, {'GIU','GIR'})
	EndIf
	If GTPxVldDic('GIW',, .T., .F.) .AND. GTPxVldDic('DA4',, .T., .F.)
		aAdd(aDelSX9, {'GIW','DA4'})
	EndIf
	If GTPxVldDic('GIX',, .T., .F.) .AND. GTPxVldDic('GI5',, .T., .F.)
		aAdd(aDelSX9, {'GIX','GI5'})
	EndIf
	If GTPxVldDic('GIY',, .T., .F.) .AND. GTPxVldDic('DA3',, .T., .F.)
		aAdd(aDelSX9, {'GIY','DA3'})
	EndIf
	If GTPxVldDic('GIZ',, .T., .F.) .AND. GTPxVldDic('DA3',, .T., .F.)
		aAdd(aDelSX9, {'GIZ','DA3'})
	EndIf
	If GTPxVldDic('GY9',, .T., .F.) .AND. GTPxVldDic('GYR',, .T., .F.)
		aAdd(aDelSX9, {'GY9','GYR'})
	EndIf
	If GTPxVldDic('GYY',, .T., .F.) .AND. GTPxVldDic('GYV',, .T., .F.)
		aAdd(aDelSX9, {'GYY','GYV'})
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - inicio
	If GTPxVldDic('GYU',, .T., .F.) .AND. GTPxVldDic('DA3',, .T., .F.)
		aAdd(aDelSX9, {'GYU','DA3'})
	EndIf
	If GTPxVldDic('GI8',, .T., .F.) .AND. GTPxVldDic('GI7',, .T., .F.)
		aAdd(aDelSX9, {'GI8','GI7'})
	EndIf
	If GTPxVldDic('GI2',, .T., .F.) .AND. GTPxVldDic('GY9',, .T., .F.)
		aAdd(aDelSX9, {'GI2','GY9'})
	EndIf
	If GTPxVldDic('GYI',, .T., .F.) .AND. GTPxVldDic('GYG',, .T., .F.)
		aAdd(aDelSX9, {'GYI','GYG'})
	EndIf
	If GTPxVldDic('GIC',, .T., .F.) .AND. GTPxVldDic('GZ2',, .T., .F.)
		aAdd(aDelSX9, {'GIC','GZ2'})
	EndIf
	If GTPxVldDic('GIJ',, .T., .F.) .AND. GTPxVldDic('SA1',, .T., .F.)
		aAdd(aDelSX9, {'GIJ','SA1'})
	EndIf
	If GTPxVldDic('GIK',, .T., .F.) .AND. GTPxVldDic('SA6',, .T., .F.)
		aAdd(aDelSX9, {'GIK','SA6'})
	EndIf
	If GTPxVldDic('GQG',, .T., .F.) .AND. GTPxVldDic('GI1',, .T., .F.)
		aAdd(aDelSX9, {'GQG','GI1'})
	EndIf
	If GTPxVldDic('GQW',, .T., .F.) .AND. GTPxVldDic('SQB',, .T., .F.)
		aAdd(aDelSX9, {'GQW','SQB'})
	EndIf
	If GTPxVldDic('G56',, .T., .F.) .AND. GTPxVldDic('SX5',, .T., .F.)
		aAdd(aDelSX9, {'G56','SX5'})
	EndIf
	//Adicionado para ajuste de dicion�rio base congelada - fim

	If GTPxVldDic('GQB',, .T., .F.) .AND. GTPxVldDic('SX5',, .T., .F.)
		aAdd(aDelSX9, {'GQB','SX5'})
	EndIf
	If GTPxVldDic('GQZ',, .T., .F.) .AND. GTPxVldDic('SA1',, .T., .F.)
		aAdd(aDelSX9, {'GQZ','SA1'})
	EndIf
	If GTPxVldDic('GQI',, .T., .F.) .AND. GTPxVldDic('GI4',, .T., .F.)
		aAdd(aDelSX9, {'GQI','GI4'})
	EndIf
	If GTPxVldDic('GYD',, .T., .F.) .AND. GTPxVldDic('GYA',, .T., .F.)
		aAdd(aDelSX9, {'GYD','GYA'})
	EndIf
	If GTPxVldDic('GYD',, .T., .F.) .AND. GTPxVldDic('GYG',, .T., .F.)
		aAdd(aDelSX9, {'GYD','GYG'})
	EndIf

	IF Len(aDelSx9) > 0
		DelSx9(aDelSx9)
	Endif

	If GTPxVldDic('G56',{'G56_CHANOM'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHANOM'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHCAMB'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHCAMB'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHCOMB'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHCOMB'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHCOMP'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHCOMP'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHEIXO'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHEIXO'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMAPL'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMAPL'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMARC'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMARC'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMDIF'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMDIF'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMEDP'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMEDP'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMODE'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMODE'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMODM'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMODM'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHMORI'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHMORI'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHNUME'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHNUME'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHPOTM'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHPOTM'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHQTPN'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHQTPN'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHRDIF'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHRDIF'})
	EndIf
	If GTPxVldDic('G56',{'G56_CHRODA'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CHRODA'})
	EndIf
	If GTPxVldDic('G56',{'G56_CODVEI'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CODVEI'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRACES'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRACES'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRANO'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRANO '})
	EndIf
	If GTPxVldDic('G56',{'G56_CHCAMB'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRBANH'})
	EndIf
	If GTPxVldDic('G56',{'G56_CREMPE'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CREMPE'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRMARC'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRMARC'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRMODE'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRMODE'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRNUME'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRNUME'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRSENT'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRSENT'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRTANQ'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRTANQ'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRTIPO'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRTIPO'})
	EndIf
	If GTPxVldDic('G56',{'G56_CRVIDR'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_CRVIDR'})
	EndIf
	If GTPxVldDic('G56',{'G56_DCODVE'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_DCODVE'})
	EndIf
	If GTPxVldDic('G56',{'G56_EPCATE'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_EPCATE'})
	EndIf
	If GTPxVldDic('G56',{'G56_EPCERT'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_EPCERT'})
	EndIf
	If GTPxVldDic('G56',{'G56_EPESTA'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_EPESTA'})
	EndIf
	If GTPxVldDic('G56',{'G56_EPPLAC'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_EPPLAC'})
	EndIf
	If GTPxVldDic('G56',{'G56_EPRENA'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_EPRENA'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGABRA'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGABRA'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGAPOL'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGAPOL'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGDSEG'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGDSEG'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGDTIP'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGDTIP'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGSEGU'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGSEGU'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGTIPO'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGTIPO'})
	EndIf
	If GTPxVldDic('G56',{'G56_SGVENC'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_SGVENC'})
	EndIf
	If GTPxVldDic('G56',{'G56_UTILIZ'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_UTILIZ'})
	EndIf
	If GTPxVldDic('G56',{'G56_VIDTIN'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_VIDTIN'})
	EndIf
	If GTPxVldDic('G56',{'G56_VIDTIP'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_VIDTIP'})
	EndIf
	If GTPxVldDic('G56',{'G56_VIDTVC'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_VIDTVC'})
	EndIf
	If GTPxVldDic('G56',{'G56_VINUME'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_VINUME'})
	EndIf
	If GTPxVldDic('G56',{'G56_VITIPO'}, .T., .T.)
		aAdd(aDelSx3, {'G56' ,'G56_VITIPO'})
	EndIf
	If GTPxVldDic('G5B',{'G5B_ALIAS'}, .T., .T.)
		aAdd(aDelSx3, {'G5B' ,'G5B_ALIAS'})
	EndIf
	If GTPxVldDic('G5B',{'G5B_CODIGO'}, .T., .T.)
		aAdd(aDelSx3, {'G5B' ,'G5B_CODIGO'})
	EndIf
	If GTPxVldDic('G5B',{'G5B_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'G5B' ,'G5B_DESCRI'})
	EndIf
	If GTPxVldDic('GI5',{'GI5_CPF'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_CPF'})
	EndIf
	If GTPxVldDic('GI5',{'GI5_MOTORI'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_MOTORI'})
	EndIf
	If GTPxVldDic('GI5',{'GI5_NMOTOR'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_NMOTOR'})
	EndIf
	If GTPxVldDic('GI5',{'GI5_NOME'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_NOME  '})
	EndIf
	If GTPxVldDic('GI5',{'GI5_STATUS'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_STATUS'})
	EndIf
	If GTPxVldDic('GI7',{'GI7_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GI7' ,'GI7_DESCRI'})
	EndIf
	If GTPxVldDic('GI7',{'GI7_DTALT'}, .T., .T.)
		aAdd(aDelSx3, {'GI7' ,'GI7_DTALT '})
	EndIf
	If GTPxVldDic('GI7',{'GI7_DTINC'}, .T., .T.)
		aAdd(aDelSx3, {'GI7' ,'GI7_DTINC '})
	EndIf
	If GTPxVldDic('GI7',{'GI7_TIPO'}, .T., .T.)
		aAdd(aDelSx3, {'GI7' ,'GI7_TIPO  '})
	EndIf
	If GTPxVldDic('GI9',{'GI9_AGENCI'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_AGENCI'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_ITEM  '})
	EndIf
	If GTPxVldDic('GI9',{'GI9_PERPED'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_PERPED'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_PERSEG'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_PERSEG'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_PERTAR'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_PERTAR'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_PERTAX'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_PERTAX'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_VIGFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_VIGFIM'})
	EndIf
	If GTPxVldDic('GI9',{'GI9_VIGINI'}, .T., .T.)
		aAdd(aDelSx3, {'GI9' ,'GI9_VIGINI'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_AGENCI'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_AGENCI'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_DATA  '})
	EndIf
	If GTPxVldDic('GIA',{'GIA_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_DESCRI'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_DESFOR'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_DESFOR'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_DOC'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_DOC'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_FORNEC'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_FORNEC'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_LOJA'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_LOJA'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_NUMTIT'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_NUMTIT'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_PARCEL'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_PARCEL'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_PREFIX'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_PREFIX'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_VALPED'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_VALPED'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_VALSEG'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_VALSEG'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_VALTAR'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_VALTAR'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_VALTAX'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_VALTAX'})
	EndIf
	If GTPxVldDic('GIA',{'GIA_VALTOT'}, .T., .T.)
		aAdd(aDelSx3, {'GIA' ,'GIA_VALTOT'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_AGENCI'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_AGENCI'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_BILOK'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_BILOK '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_BILTOT'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_BILTOT'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_DATA  '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_DESCRI'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_DTVIAG'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_DTVIAG'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_HORAR'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_HORAR '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_LINHA '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_LOTE'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_LOTE  '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_MOTCOB'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_MOTCOB'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_NLINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_NLINHA'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_NUMFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_NUMFIM'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_NUMINI'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_NUMINI'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_OK'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_OK'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_SENTID'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_SENTID'})
	EndIf
	If GTPxVldDic('GIB',{'GIB_SERIE'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_SERIE '})
	EndIf
	If GTPxVldDic('GIB',{'GIB_TPLOTE'}, .T., .T.)
		aAdd(aDelSx3, {'GIB' ,'GIB_TPLOTE'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_CARRO'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_CARRO '})
	EndIf
	If GTPxVldDic('GIF',{'GIF_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_DATA  '})
	EndIf
	If GTPxVldDic('GIF',{'GIF_DCHEGD'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_DCHEGD'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_DCHERD'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_DCHERD'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_DCHERO'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_DCHERO'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_DSAIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_DSAIGO'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_HCHEGD'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_HCHEGD'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_HCHERD'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_HCHERD'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_HCHERO'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_HCHERO'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_HORCAB'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_HORCAB'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_HSAIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_HSAIGO'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_LINHA '})
	EndIf
	If GTPxVldDic('GIF',{'GIF_NLINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_NLINHA'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_SENTID'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_SENTID'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_SERVIC'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_SERVIC'})
	EndIf
	If GTPxVldDic('GIF',{'GIF_TPVIA'}, .T., .T.)
		aAdd(aDelSx3, {'GIF' ,'GIF_TPVIA '})
	EndIf
	If GTPxVldDic('GIG',{'GIG_AGENC'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_AGENC '})
	EndIf
	If GTPxVldDic('GIG',{'GIG_BILFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_BILFIM'})
	EndIf
	If GTPxVldDic('GIG',{'GIG_BILINI'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_BILINI'})
	EndIf
	If GTPxVldDic('GIG',{'GIG_DTFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_DTFIM '})
	EndIf
	If GTPxVldDic('GIG',{'GIG_DTINI'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_DTINI '})
	EndIf
	If GTPxVldDic('GIG',{'GIG_SERIE'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_SERIE '})
	EndIf
	If GTPxVldDic('GIG',{'GIG_TERCEI'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_TERCEI'})
	EndIf
	If GTPxVldDic('GIG',{'GIG_TOTAL'}, .T., .T.)
		aAdd(aDelSx3, {'GIG' ,'GIG_TOTAL '})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_CARTAO'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_CARTAO'})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_CLICAR'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_CLICAR'})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_DESCRI'})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_LOJCAR'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_LOJCAR'})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_MVBCO'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_MVBCO '})
	EndIf
	If GTPxVldDic('GIJ',{'GIJ_NCLICA'}, .T., .T.)
		aAdd(aDelSx3, {'GIJ' ,'GIJ_NCLICA'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_OK'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_OK'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_VALOR '})
	EndIf
	If GTPxVldDic('GIK',{'GIK_AGE'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_AGE'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_AGENCI'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_AGENCI'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_BCO'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_BCO'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_CTA'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_CTA'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_DATA'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_DTMOV'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_DTMOV'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_FORPAG'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_FORPAG'})
	EndIf
	If GTPxVldDic('GIK',{'GIK_LOTE'}, .T., .T.)
		aAdd(aDelSx3, {'GIK' ,'GIK_LOTE'})
	EndIf
	If GTPxVldDic('GIL',{'GIL_AGE'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_AGE   '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_AGENCI'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_AGENCI'})
	EndIf
	If GTPxVldDic('GIL',{'GIL_BCO'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_BCO   '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_CTA'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_CTA   '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_DATA  '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_LOTE'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_LOTE  '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_NATURE'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_NATURE'})
	EndIf
	If GTPxVldDic('GIL',{'GIL_OK'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_OK    '})
	EndIf
	If GTPxVldDic('GIL',{'GIL_RECPAG'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_RECPAG'})
	EndIf
	If GTPxVldDic('GIL',{'GIL_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GIL' ,'GIL_VALOR '})
	EndIf
	If GTPxVldDic('GIR',{'GIR_APLIC'}, .T., .T.)
		aAdd(aDelSx3, {'GIR' ,'GIR_APLIC '})
	EndIf
	If GTPxVldDic('GIR',{'GIR_COD'}, .T., .T.)
		aAdd(aDelSx3, {'GIR' ,'GIR_COD   '})
	EndIf
	If GTPxVldDic('GIR',{'GIR_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GIR' ,'GIR_DESCRI'})
	EndIf
	If GTPxVldDic('GIR',{'GIR_LINHAS'}, .T., .T.)
		aAdd(aDelSx3, {'GIR' ,'GIR_LINHAS'})
	EndIf
	If GTPxVldDic('GIS',{'GIS_CODREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIS' ,'GIS_CODREQ'})
	EndIf
	If GTPxVldDic('GIS',{'GIS_DESREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIS' ,'GIS_DESREQ'})
	EndIf
	If GTPxVldDic('GIS',{'GIS_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GIS' ,'GIS_ITEM  '})
	EndIf
	If GTPxVldDic('GIS',{'GIS_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIS' ,'GIS_LINHA '})
	EndIf
	If GTPxVldDic('GIT',{'GIT_CODREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIT' ,'GIT_CODREQ'})
	EndIf
	If GTPxVldDic('GIT',{'GIT_DESREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIT' ,'GIT_DESREQ'})
	EndIf
	If GTPxVldDic('GIT',{'GIT_DTFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIT' ,'GIT_DTFIM '})
	EndIf
	If GTPxVldDic('GIT',{'GIT_DTINIC'}, .T., .T.)
		aAdd(aDelSx3, {'GIT' ,'GIT_DTINIC'})
	EndIf
	If GTPxVldDic('GIT',{'GIT_VEICUL'}, .T., .T.)
		aAdd(aDelSx3, {'GIT' ,'GIT_VEICUL'})
	EndIf
	If GTPxVldDic('GIU',{'GIU_CODREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_CODREQ'})
	EndIf
	If GTPxVldDic('GIU',{'GIU_DESREQ'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_DESREQ'})
	EndIf
	If GTPxVldDic('GIU',{'GIU_DTFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_DTFIM '})
	EndIf
	If GTPxVldDic('GIU',{'GIU_DTINIC'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_DTINIC'})
	EndIf
	If GTPxVldDic('GIU',{'GIU_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_ITEM  '})
	EndIf
	If GTPxVldDic('GIU',{'GIU_MOTORI'}, .T., .T.)
		aAdd(aDelSx3, {'GIU' ,'GIU_MOTORI'})
	EndIf
	If GTPxVldDic('GIV',{'GIV_COD'}, .T., .T.)
		aAdd(aDelSx3, {'GIV' ,'GIV_COD   '})
	EndIf
	If GTPxVldDic('GIV',{'GIV_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GIV' ,'GIV_DESCRI'})
	EndIf
	If GTPxVldDic('GIW',{'GIW_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GIW' ,'GIW_ITEM  '})
	EndIf
	If GTPxVldDic('GIW',{'GIW_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIW' ,'GIW_LINHA '})
	EndIf
	If GTPxVldDic('GIW',{'GIW_MOTORI'}, .T., .T.)
		aAdd(aDelSx3, {'GIW' ,'GIW_MOTORI'})
	EndIf
	If GTPxVldDic('GIW',{'GIW_NMOTOR'}, .T., .T.)
		aAdd(aDelSx3, {'GIW' ,'GIW_NMOTOR'})
	EndIf
	If GTPxVldDic('GIX',{'GIX_COBRAD'}, .T., .T.)
		aAdd(aDelSx3, {'GIX' ,'GIX_COBRAD'})
	EndIf
	If GTPxVldDic('GIX',{'GIX_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GIX' ,'GIX_ITEM  '})
	EndIf
	If GTPxVldDic('GIX',{'GIX_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIX' ,'GIX_LINHA '})
	EndIf
	If GTPxVldDic('GIX',{'GIX_NCOBR'}, .T., .T.)
		aAdd(aDelSx3, {'GIX' ,'GIX_NCOBR '})
	EndIf
	If GTPxVldDic('GIY',{'GIY_DESVEI'}, .T., .T.)
		aAdd(aDelSx3, {'GIY' ,'GIY_DESVEI'})
	EndIf
	If GTPxVldDic('GIY',{'GIY_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GIY' ,'GIY_ITEM  '})
	EndIf
	If GTPxVldDic('GIY',{'GIY_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GIY' ,'GIY_LINHA '})
	EndIf
	If GTPxVldDic('GIY',{'GIY_VEICUL'}, .T., .T.)
		aAdd(aDelSx3, {'GIY' ,'GIY_VEICUL'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_COD'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_COD'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_DESLOC'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_DESLOC'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_DESVEI'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_DESVEI'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_DTFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_DTFIM '})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_DTINIC'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_DTINIC'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_HORAEN'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_HORAEN'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_HORASA'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_HORASA'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_KMENTR'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_KMENTR'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_KMSAID'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_KMSAID'})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_LOCAL'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_LOCAL '})
	EndIf
	If GTPxVldDic('GIZ',{'GIZ_VEICUL'}, .T., .T.)
		aAdd(aDelSx3, {'GIZ' ,'GIZ_VEICUL'})
	EndIf
	If GTPxVldDic('GY9',{'GY9_CODCAT'}, .T., .T.)
		aAdd(aDelSx3, {'GY9' ,'GY9_CODCAT'})
	EndIf
	If GTPxVldDic('GY9',{'GY9_CODIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GY9' ,'GY9_CODIGO'})
	EndIf
	If GTPxVldDic('GY9',{'GY9_CODORG'}, .T., .T.)
		aAdd(aDelSx3, {'GY9' ,'GY9_CODORG'})
	EndIf
	If GTPxVldDic('GY9',{'GY9_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GY9' ,'GY9_DESCRI'})
	EndIf
	If GTPxVldDic('GYS',{'GYS_CODCAT'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_CODCAT'})
	EndIf
	If GTPxVldDic('GYS',{'GYS_CODIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_CODIGO'})
	EndIf
	If GTPxVldDic('GYS',{'GYS_CODORG'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_CODORG'})
	EndIf
	If GTPxVldDic('GYS',{'GYS_COEFI'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_COEFI '})
	EndIf
	If GTPxVldDic('GYS',{'GYS_KMMAX'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_KMMAX '})
	EndIf
	If GTPxVldDic('GYS',{'GYS_KMMIN'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_KMMIN '})
	EndIf
	If GTPxVldDic('GYS',{'GYS_TIPO'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_TIPO  '})
	EndIf
	If GTPxVldDic('GYS',{'GYS_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GYS' ,'GYS_VALOR '})
	EndIf
	If GTPxVldDic('GYV',{'GYV_CODIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GYV' ,'GYV_CODIGO'})
	EndIf
	If GTPxVldDic('GYV',{'GYV_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GYV' ,'GYV_DESCRI'})
	EndIf
	If GTPxVldDic('GYV',{'GYV_MODELO'}, .T., .T.)
		aAdd(aDelSx3, {'GYV' ,'GYV_MODELO'})
	EndIf
	If GTPxVldDic('GYV',{'GYV_OBSERV'}, .T., .T.)
		aAdd(aDelSx3, {'GYV' ,'GYV_OBSERV'})
	EndIf
	If GTPxVldDic('GYY',{'GYY_CODACS'}, .T., .T.)
		aAdd(aDelSx3, {'GYY' ,'GYY_CODACS'})
	EndIf
	If GTPxVldDic('GYY',{'GYY_CODVEI'}, .T., .T.)
		aAdd(aDelSx3, {'GYY' ,'GYY_CODVEI'})
	EndIf
	If GTPxVldDic('GYY',{'GYY_DESACS'}, .T., .T.)
		aAdd(aDelSx3, {'GYY' ,'GYY_DESACS'})
	EndIf
	If GTPxVldDic('GYY',{'GYY_QTDACS'}, .T., .T.)
		aAdd(aDelSx3, {'GYY' ,'GYY_QTDACS'})
	EndIf
	If GTPxVldDic('G9P',{'G9P_CODG6X'}, .T., .T.)
		aAdd(aDelSx3, {'G9P' ,'G9P_CODG6X'})
	EndIf
	If GTPxVldDic('G9P',{'G9P_NUMDOC'}, .T., .T.)
		aAdd(aDelSx3, {'G9P' ,'G9P_NUMDOC'})
	EndIf
	If GTPxVldDic('G9P',{'G9P_SERIE'}, .T., .T.)
		aAdd(aDelSx3, {'G9P' ,'G9P_SERIE'})
	EndIf
	If GTPxVldDic('G9P',{'G9P_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'G9P' ,'G9P_VALOR'})
	EndIf

	If GTPxVldDic('GQJ',{'GQJ_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GQJ' ,'GQJ_DESCRI'})
	EndIf
	If GTPxVldDic('GQJ',{'GQJ_CLASSI'}, .T., .T.)
		aAdd(aDelSx3, {'GQJ' ,'GQJ_CLASSI'})
	EndIf

	If GTPxVldDic('GYX',{'GYX_USER'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_USER'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_HRCANC'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_HRCANC'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_DTCANC'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_DTCANC'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_STATUS'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_STATUS'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_MOTOR2'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_MOTOR2'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_MOTOR1'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_MOTOR1'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_VEIC'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_VEIC'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_HRPREV'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_HRPREV'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_DTPREV'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_DTPREV'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_HRVIAG'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_HRVIAG'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_DTVIAG'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_DTVIAG'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_KMREAL'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_KMREAL'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_KMPROV'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_KMPROV'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_INTFIM'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_INTFIM'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LOCDES'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LOCDES'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_INTINI'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_INTINI'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LOCOR'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LOCOR'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_MOTIVO'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_MOTIVO'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LOCINT'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LOCINT'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_HRINT'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_HRINT'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LININT'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LININT'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LOCTER'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LOCTER'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_CONTRA'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_CONTRA'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_LINHA'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_LINHA'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_IDENT'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_IDENT'})
	EndIf
	If GTPxVldDic('GYX',{'GYX_CODSER'}, .T., .T.)
		aAdd(aDelSx3, {'GYX' ,'GYX_CODSER'})
	EndIf

	//Adicionado para ajuste de dicion�rio base congelada - inicio
	If GTPxVldDic('G99',{'G99_CODLAN'}, .T., .T.)
		aAdd(aDelSx3, {'G99' ,'G99_CODLAN'})
	EndIf
	If GTPxVldDic('GI5',{'GI5_MSBLQL'}, .T., .T.)
		aAdd(aDelSx3, {'GI5' ,'GI5_MSBLQL'})
	EndIf
	If GTPxVldDic('GIO',{'GIO_PLAN'}, .T., .T.)
		aAdd(aDelSx3, {'GIO' ,'GIO_PLAN'})
	EndIf
	If GTPxVldDic('GIQ',{'GIQ_DESLMT'}, .T., .T.)
		aAdd(aDelSx3, {'GIQ' ,'GIQ_DESLMT'})
	EndIf
	If GTPxVldDic('GZN',{'GZN_ASSOCI'}, .T., .T.)
		aAdd(aDelSx3, {'GZN' ,'GZN_ASSOCI'})
	EndIf
	If GTPxVldDic('GZN',{'GZN_ORIGEM'}, .T., .T.)
		aAdd(aDelSx3, {'GZN' ,'GZN_ORIGEM'})
	EndIf
	If GTPxVldDic('GZN',{'GZN_VARIAV'}, .T., .T.)
		aAdd(aDelSx3, {'GZN' ,'GZN_VARIAV'})
	EndIf
	If GTPxVldDic('GZR',{'GZR_CODGYG'}, .T., .T.)
		aAdd(aDelSx3, {'GZR' ,'GZR_CODGYG'})
	EndIf
	If GTPxVldDic('GZR',{'GZR_CODGYQ'}, .T., .T.)
		aAdd(aDelSx3, {'GZR' ,'GZR_CODGYQ'})
	EndIf
	If GTPxVldDic('GZR',{'GZR_DTREF'}, .T., .T.)
		aAdd(aDelSx3, {'GZR' ,'GZR_DTREF'})
	EndIf
	If GTPxVldDic('GZR',{'GZR_SITRH'}, .T., .T.)
		aAdd(aDelSx3, {'GZR' ,'GZR_SITRH'})
	EndIf
	If GTPxVldDic('GZR',{'GZR_TPDIA'}, .T., .T.)
		aAdd(aDelSx3, {'GZR' ,'GZR_TPDIA'})
	EndIf 
	//Adicionado para ajuste de dicion�rio base congelada - fim

	If GTPxVldDic('GQ8',{'GQ8_CODLOC'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_CODLOC'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_DESCLO'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_DESCLO'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_QTDTXE'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_QTDTXE'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTTXE'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTTXE'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_QTDTAR'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_QTDTAR'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTTAR'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTTAR'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_QTDSEG'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_QTDSEG'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTSEG'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTSEG'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_QTDPED'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_QTDPED'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTPED'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTPED'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTOUT'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTOUT'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TOTGER'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TOTGER'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TIPO'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TIPO'})
	EndIf 
	If GTPxVldDic('GQ8',{'GQ8_TIPOAG'}, .T., .T.)
		aAdd(aDelSx3, {'GQ8' ,'GQ8_TIPOAG'})
	EndIf 
	If GTPxVldDic('GQB',{'GQB_SERVIC'}, .T., .T.)
		aAdd(aDelSx3, {'GQB' ,'GQB_SERVIC'})
	EndIf 
	If GTPxVldDic('GQB',{'GQB_DESCRI'}, .T., .T.)
		aAdd(aDelSx3, {'GQB' ,'GQB_DESCRI'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_CODCLI'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_CODCLI'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_CODLOJ'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_CODLOJ'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_NOMCLI'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_NOMCLI'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_INIVIG'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_INIVIG'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_FIMVIG'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_FIMVIG'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_TPDESC'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_TPDESC'})
	EndIf 
	If GTPxVldDic('GQZ',{'GQZ_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GQZ' ,'GQZ_VALOR '})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_ITEM'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_ITEM'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_CODGI4'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_CODGI4'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_LOCORI'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_LOCORI'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_NLOCO'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_NLOCO'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_LOCDES'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_LOCDES'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_NLOCD'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_NLOCD'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_SENTID'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_SENTID'})
	EndIf 
	If GTPxVldDic('GQI',{'GQI_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GQI' ,'GQI_VALOR'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODIGO'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODIGO'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODLOT'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODLOT'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODLOT'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODLOT'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODTPD'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODTPD'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_TDDESC'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_TDDESC'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_VALOR'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_VALOR'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_DATA'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_DATA'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_JUSTIF'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_JUSTIF'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_LANCAM'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_LANCAM'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODAGE'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODAGE'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_STATUS'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_STATUS'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODJUS'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODJUS'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_EMISSO'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_EMISSO'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_NEMISS'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_NEMISS'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_AGEDES'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_AGEDES'})
	EndIf 
	If GTPxVldDic('GYD',{'GYD_CODG6X'}, .T., .T.)
		aAdd(aDelSx3, {'GYD' ,'GYD_CODG6X'})
	EndIf 

	If Len(aDelSx3) > 0
		DelSx3(aDelSx3)
	Endif

	If GTPxVldDic('GI5',, .T., .F.)
		aAdd(aDelSix, {'GI5','GI5_FILIAL+GI5_CPF'})
	EndIf
	If GTPxVldDic('GIA',, .T., .F.)
		aAdd(aDelSix, {'GIA','GIA_FILIAL+GIA_AGENCI'})
	EndIf
	If GTPxVldDic('GIA',, .T., .F.)
		aAdd(aDelSix, {'GIA','GIA_FILIAL+GIA_PREFIX+GIA_NUMTIT+GIA_PARCEL+GIA_FORNEC+GIA_LOJA'})
	EndIf
	If GTPxVldDic('GIB',, .T., .F.)
		aAdd(aDelSix, {'GIB','GIB_FILIAL+GIB_AGENCI'})
	EndIf
	If GTPxVldDic('GIT',, .T., .F.)
		aAdd(aDelSix, {'GIT','GIT_FILIAL+GIT_CODREQ'})
	EndIf
	If GTPxVldDic('GIV',, .T., .F.)
		aAdd(aDelSix, {'GIV','GIV_FILIAL+GIV_DESCRI'})
	EndIf
	If GTPxVldDic('GY9',, .T., .F.)
		aAdd(aDelSix, {'GY9','GY9_FILIAL+GY9_CODIGO+GY9_CODORG+GY9_CODCAT'})
	EndIf
	If GTPxVldDic('GY9',, .T., .F.)
		aAdd(aDelSix, {'GY9','GY9_FILIAL+GY9_CODORG+GY9_CODCAT'})
	EndIf
	If GTPxVldDic('GYS',, .T., .F.)
		aAdd(aDelSix, {'GYS','GYS_FILIAL+GYS_CODCAT+GYS_CODORG'})
	EndIf
	If GTPxVldDic('GYS',, .T., .F.)
		aAdd(aDelSix, {'GYS','GYS_FILIAL+GYS_CODIGO+GYS_CODORG'})
	EndIf
	If GTPxVldDic('GYV',, .T., .F.)
		aAdd(aDelSix, {'GYV','GYV_FILIAL+GYV_CODIGO'})
	EndIf
	If GTPxVldDic('GYY',, .T., .F.)
		aAdd(aDelSix, {'GYY','GYY_FILIAL+GYY_CODVEI+GYY_CODACS'})
	EndIf
	If GTPxVldDic('GQ8',, .T., .F.)
		aAdd(aDelSix, {'GQ8','GQ8_FILIAL+GQ8_CODIGO+GQ8_CODLOC+GQ8_TIPO+GQ8_TIPOAG'})
	EndIf
	If GTPxVldDic('GQB',, .T., .F.)
		aAdd(aDelSix, {'GQB','GQB_FILIAL+GQB_CODIGO+GQB_ITEM+GQB_SERVIC'})
	EndIf
	If GTPxVldDic('GQI',, .T., .F.)
		aAdd(aDelSix, {'GQI','GQI_FILIAL+GQI_CODIGO+GQI_ITEM'})
	EndIf
	If GTPxVldDic('GYD',, .T., .F.)
		aAdd(aDelSix, {'GYD','GYD_FILIAL+GYD_CODLOT+GYD_CODTPD'})
	EndIf
	If GTPxVldDic('GYD',, .T., .F.)
		aAdd(aDelSix, {'GYD','GYD_FILIAL+GYD_CODIGO'})
	EndIf

	IF Len(aDelSix) > 0
		DelSix(aDelSix)
	Endif

EndIf

RestArea(aAreaSX2)
RestArea(aAreaSX3)
RestArea(aAreaSX7)
RestArea(aArea)

aSize(aSX2,0)
aSX2 := Nil

aSize(aSX3,0)
aSX3 := Nil

aSize(aDelSx7,0)
aDelSx7 := Nil

aSize(aDelSx3,0)
aDelSx3 := Nil

aSize(aDelSx9,0)
aDelSx9 := Nil

aSize(aDelSix,0)
aDelSix := Nil

aSize(aAtuTab,0)
aAtuTab:= Nil

Return Nil

/*/{Protheus.doc} ExistDadosTabela
(long_description)
@type function
@author jacomo.fernandes
@since 17/05/2019
@version 1.0
@param cAliasAux, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ExistDadosTabela(cAliasxpto)
Local lRet			:= .T.
Local cAliasTmp		:= GetNextAlias()
Default cAliasxpto	:= ''

If AliasInDic(cAliasxpto) .And. Select(cAliasxpto) == 0
	lRet := .F.
Else
	If Select(cAliasxpto) > 0 
		DbSelectArea(cAliasxpto)
		cAliasxpto	:= '%'+RetSqlName(cAliasxpto)+'%'

		BeginSql Alias cAliasTmp
			
			Select
				Count(*) as TOTAL
			FROM %Exp:cAliasxpto%
			Where
				%NotDel%	
		EndSql
		
		lRet := ((cAliasTmp)->TOTAL) > 0
		
		(cAliasTmp)->(DbCloseArea())
	Endif
Endif

Return lRet

/*/{Protheus.doc} AjustaSX2
Ajusta o Dicion�rio SX2
@type function
@author jacomo.fernandes
@since 11/04/2017
@version 1.0
@param aSx3, array, Array contendo os campos a serem alterados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AjustaSX2(aSX2)
Local nInd1 := 0
Local nInd2 := 0
Local nTamX2Chv	:= Len(SX2->X2_CHAVE)


DbSelectArea("SX2")
SX2->(DbSetOrder(1))//X2_CHAVE
For nInd1 := 1 to Len(aSX2)// Seleciona a chave
	If	SX2->( DbSeek( PadR( aSX2[nInd1][DF_CAMPO], nTamX2Chv ) ) )
		SX2->(RecLock("SX2",.F.))	
		For nInd2 := 1 to Len(aSX2[nInd1][2]) //Ajustes do Sx2
			
			//Macro Substitui��o dos campos do Sx2 
			SX2->&(aSX2[nInd1][2][nInd2][DF_CAMPO]) := aSX2[nInd1][2][nInd2][DF_CONTEUDO]
		Next
		SX2->(DBCommit())	
		SX2->(MSUnlock())
	EndIf
Next

Return

/*/{Protheus.doc} AjustaSx3
Ajusta o Dicion�rio SX3
@type function
@author jacomo.fernandes
@since 11/04/2017
@version 1.0
@param aSx3, array, Array contendo os campos a serem alterados
@param aAtuTab, array, Parametro passado por referencia para atualizar o banco
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AjustaSx3(aSx3,aAtuTab)
Local nInd1 := 0
Local nInd2 := 0
Local nTamX3CPO	:= Len(SX3->X3_CAMPO)

SX3->(DbSetOrder(2))//X3_CAMPO
For nInd1 := 1 to Len(aSx3)// Seleciona Campo
	If	SX3->( DbSeek( PadR( aSx3[nInd1][DF_CAMPO], nTamX3CPO ) ) )
		SX3->(RecLock("SX3",.F.))	
		For nInd2 := 1 to Len(aSx3[nInd1][2]) //Ajustes do Sx3
			
			//Macro Substitui��o dos campos do Sx3 
			SX3->&(aSx3[nInd1][2][nInd2][DF_CAMPO]) := aSx3[nInd1][2][nInd2][DF_CONTEUDO]
		
			//Verifica se a Tabela teve o Contexto ou o Tamanho foi alterado
			If Alltrim(aSx3[nInd1][2][nInd2][DF_CAMPO]) $ "X3_CONTEXT,X3_TAMANHO" 
				If aScan(aAtuTab, SubStr(aSx3[nInd1,DF_CAMPO],1,3)) == 0
					aAdd(aAtuTab, SubStr(aSx3[nInd1,DF_CAMPO],1,3))
				EndIf
			Endif
		Next
		SX3->(MSUnlock())
	EndIf
Next
Return


/*/{Protheus.doc} UpdBanco
Fun��o chamada para atualizar o banco de dados
@type function
@author jacom
@since 11/04/2017
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function UpdBanco(aAtuTab)
Local nInd1	    := 0
Local cTexto	:= ""
Local lJob   	:= Iif(Select("SX6")==0,.T.,.F.)
Local nStart    := Time()
// Atualiza as defini��es de tabela quando houver necessidade 
//de ajuste em tamanho de coluna em tabela j� existente.

For nInd1 := 1 To Len(aAtuTab)
	
	If Select(aAtuTab[nInd1]) > 0

		dbSelectArea(aAtuTab[nInd1])
		dbCloseArea()

	EndIf
	cTexto += " " + aAtuTab[nInd1] + ", "
	
	X31UpdTable(aAtuTab[nInd1])
	dbSelectArea(aAtuTab[nInd1])

Next nInd1

Return


/*/{Protheus.doc} GTPRUP
(long_description)
@type function
@author jacom
@since 11/04/2017
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Function GTPRUP(cVersion, cMode, cRelStart, cRelFinish, cLocaliz)

Default cVersion		:= "12"
Default cMode			:= "1"
Default cRelStart		:= "014"
Default cRelFinish	    := "016"
Default cLocaliz		:= "BRA"

FwMsgRun( ,{||RUP_GTP(cVersion, cMode, cRelStart, cRelFinish, cLocaliz)},,"Executando RUP...")

Return()

/*/{Protheus.doc} DelSx7
Fun��o para deletar relacionamento de tabela
@type function
@author jacom
@since 11/04/2017
@version 1.0
@param aDelSx9, array, Array contendo os dados a serem deletados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function DelSx7(aDelSx7)
Local nInd1 := 0

//Mata os relacionamentos
DbSelectArea("SX7")
SX7->(DbSetOrder(1)) //X7_CAMPO+X7_SEQUENC
For nInd1 := 1 To Len(aDelSX7)
	If SX7->(DbSeek(aDelSX7[nInd1][1]+aDelSX7[nInd1][2]))
		Reclock("SX7",.F.)
		SX7->( DbDelete() )
		SX7->(MsUnlock())
	EndIf
NEXT
Return

/*/{Protheus.doc} DelSx2
Fun��o para deletar Tabela do dicion�rio
@type function
@author gtp
@since 18/06/2020
@version 1.0
@param aDelSx2, array, Array contendo os dados a serem deletados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function DelSx2(aDelSx2)
Local nInd1 	:= 0
Local aAtuTab	:= {}

dbSelectArea("SX2")
SX2->(dbSetOrder(1)) 

For nInd1 := 1 To Len(aDelSX2)

	If SX2->(dbSeek(aDelSX2[nInd1]))
		Reclock("SX2",.F.)
		SX2->( dbDelete() )
		SX2->(MsUnlock())

		If aScan(aAtuTab, aDelSX2[nInd1]) == 0
			aAdd(aAtuTab, aDelSX2[nInd1])
		Endif

	EndIf

Next

For nInd1 := 1 To Len(aAtuTab)

	X31UpdTable(aAtuTab[nInd1])
	
Next
Return

/*/{Protheus.doc} DelSx3
Fun��o para deletar campos do dicion�rio
@type function
@author flavio.martins
@since 22/01/2020
@version 1.0
@param aDelSx3, array, Array contendo os dados a serem deletados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function DelSx3(aDelSx3)
Local nInd1 	:= 0
Local aAtuTab	:= {}

dbSelectArea("SX3")
SX3->(dbSetOrder(2)) 

For nInd1 := 1 To Len(aDelSX3)

	If SX3->(dbSeek(aDelSX3[nInd1][2]))
		Reclock("SX3",.F.)
		SX3->( dbDelete() )
		SX3->(MsUnlock())

		If aScan(aAtuTab, aDelSX3[nInd1][1]) == 0
			aAdd(aAtuTab, aDelSX3[nInd1][1])
		Endif

	EndIf

Next

For nInd1 := 1 To Len(aAtuTab)

	X31UpdTable(aAtuTab[nInd1])
	
Next

Return

/*/{Protheus.doc} DelSIX
Fun��o para deletar relacionamento de tabela
@type function
@author jacom
@since 11/04/2017
@version 1.0
@param aDelSIX, array, Array contendo os dados a serem deletados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function DelSIX(aDelSIX)
Local nInd1 	:= 0
Local aAtuTab	:= {}

//Mata os relacionamentos
DbSelectArea("SIX")
SIX->(DbSetOrder(1)) // INDICE+ORDEM
For nInd1 := 1 To Len(aDelSIX)
	If SIX->(DbSeek(aDelSIX[nInd1][1]))
		While SIX->(!Eof()) .and. SIX->INDICE = aDelSIX[nInd1][1] 
			If AllTrim(SIX->CHAVE) == aDelSIX[nInd1][2]
				Reclock("SIX",.F.)
				SIX->( DbDelete() )
				SIX->(MsUnlock())
				If aScan(aAtuTab, aDelSIX[nInd1,1] ) == 0
					aAdd(aAtuTab, aDelSIX[nInd1,1] )
				EndIf
				Exit
			Endif
			SIX->(DbSkip())
		End
	EndIf
Next

For nInd1 := 1 To Len(aAtuTab)

	X31UpdTable(aAtuTab[nInd1])
	
Next


Return

/*/{Protheus.doc} DelSx9
Fun��o para deletar relacionamento de tabela
@type function
@author jacom
@since 11/04/2017
@version 1.0
@param aDelSx9, array, Array contendo os dados a serem deletados
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function DelSx9(aDelSx9)
Local nInd1 := 0

//Mata os relacionamentos
DbSelectArea("SX9")
SX9->(DbSetOrder(2)) // X9_CDOM+X9_DOM
For nInd1 := 1 To Len(aDelSX9)
	If SX9->(DbSeek(aDelSX9[nInd1][1]+aDelSX9[nInd1][2] ))
		While SX9->(!Eof()) .and. SX9->X9_CDOM == aDelSX9[nInd1][1] .and. SX9->X9_DOM == aDelSX9[nInd1][2]
			//If SX9->X9_EXPCDOM == Padr(aDelSX9[nInd1][3] ,Len(SX9->X9_EXPCDOM)) .and. SX9->X9_EXPDOM == Padr(aDelSX9[nInd1][4] ,Len(SX9->X9_EXPDOM)) 
				Reclock("SX9",.F.)
				SX9->( DbDelete() )
				SX9->(MsUnlock())
				Exit
			//Endif
			SX9->(DbSkip())
		End
	EndIf
Next
Return
