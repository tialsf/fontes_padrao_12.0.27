#INCLUDE "TURA001.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA001()

CADASTRO DE TAXAS - SIGATUR

@sample 	TURA001()
@return                             
@author  	Servi�os
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA001()
Local oBrowse	:= Nil
 
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G3A')
oBrowse:SetDescription(STR0001) //"Cadastro de Taxas"
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE TAXAS - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA001()
@return     oModel                       
@author  	Servi�os
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG3A := FWFormStruct(1,'G3A',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA001',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G3AMASTER',/*cOwner*/,oStruG3A,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de Taxas

Return(oModel)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE TAXAS - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA001()
@return     oView                       
@author  	Servi�os
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA001')
Local oStruG3A := FWFormStruct(2,'G3A')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G3A', oStruG3A,'G3AMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G3A','TELA')

Return(oView)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE TAXAS - DEFINE AROTINA (MVC) 

@sample 	TURA001()
@return     aRotina                       
@author  	Servi�os
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Pesquisar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA001'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA001'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA001'	OPERATION 4	ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA001'	OPERATION 5	ACCESS 0 // Excluir

Return(aRotina)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )
Local aRet := {}
aRet:= TURI001( cXml, nTypeTrans, cTypeMessage )
Return aRet
