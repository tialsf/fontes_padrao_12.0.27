#INCLUDE "TURA002.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA002()

CADASTRO DE TERMINAL DE PASSAGEIROS - SIGATUR

@sample 	TURA002()
@return                             
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA002()

Local oBrowse	:= FWMBrowse():New()
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse:SetAlias('G3B')
oBrowse:AddLegend("G3B_MSBLQL=='1'", "GRAY" , STR0007)	// "Bloqueado"
oBrowse:AddLegend("G3B_MSBLQL=='2'", "GREEN", STR0008)	// "Ativo"
oBrowse:SetDescription(STR0001) // Cadastro de Terminal de Passageiros
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE TERMINAL DE PASSAGEIROS - DEFINE MODELO DE DADOS (MVC) 

@sample 	ModelDef()
@return  	oModel: objeto do modelo
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel	:= Nil
Local oStruG3B 	:= FWFormStruct(1,'G3B',/*bAvalCampo*/,/*lViewUsado*/)

oModel 	:= MPFormModel():New('TURA002',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G3BMASTER',/*cOwner*/,oStruG3B,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de Terminal de Passageiros

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Rotina respons�vel pela defini��o da interface do cadastro de terminal de passageiros.

@sample 	ViewDef()
@return   	oView: objeto da vis�o
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView		:= FWFormView():New()
Local oModel   	:= FWLoadModel('TURA002')
Local oStruG3B 	:= FWFormStruct(2,'G3B')

oView:SetModel(oModel)
oView:AddField('VIEW_G3B', oStruG3B,'G3BMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G3B','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Rotina respons�vel pela defini��o do menu do cadastro de terminal de passageiros.

@sample 	MenuDef()
@return  	aRotina: array com itens do menu
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Pesquisar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA002'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA002'	OPERATION 3	ACCESS 0 // Incluir
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA002'	OPERATION 4	ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA002'	OPERATION 5	ACCESS 0 // Excluir

Return(aRotina)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI002( cXml, nTypeTrans, cTypeMessage )

Return aRet