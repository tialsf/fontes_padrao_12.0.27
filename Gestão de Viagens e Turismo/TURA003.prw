#INCLUDE "TURA003.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA003()

CADASTRO DE CLASSE DE SERVI�O - SIGATUR

@sample 	TURA003()
@return
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA003()
	Local oBrowse	:= Nil
	
	//------------------------------------------------------------------------------------------
	// Cria o Browse
	//------------------------------------------------------------------------------------------
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('G3C')
	oBrowse:AddLegend	( "G3C_MSBLQL=='1'", "GRAY"  , STR0007 )// "Bloqueado"
	oBrowse:AddLegend	( "G3C_MSBLQL=='2'", "GREEN" , STR0008 )// "Ativo"
	oBrowse:SetDescription(STR0001) // Cadastro de Classe de Servi�os
	oBrowse:Activate()
	
Return(.T.)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE CLASSE DE SERVI�O - DEFINE MODELO DE DADOS (MVC)

@sample 	TURA003()
@return  	oModel
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()
	
	Local oModel
	Local oStruG3C := FWFormStruct(1,'G3C',/*bAvalCampo*/,/*lViewUsado*/)
	
	
	oModel:= MPFormModel():New('TURA003',/*bPreValidacao*/,{ | oModel | TudoOk( oModel ) }/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
	oModel:AddFields('G3CMASTER',/*cOwner*/,oStruG3C,/*Criptog()/,/*bPosValidacao*/,/*bCarga*/)
	//oModel:SetPrimaryKey({})
	oModel:SetDescription(STR0001) // Cadastro de Classe de Servi�os
	
Return(oModel)


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE CLASSE DE SERVI�O - DEFINE A INTERFACE DO CADASTRO (MVC)

@sample 	TURA003()
@return   	oView
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()
	
	Local oView
	Local oModel   := FWLoadModel('TURA003')
	Local oStruG3C := FWFormStruct(2,'G3C')
	
	oView:= FWFormView():New()
	oView:SetModel(oModel)
	oView:AddField('VIEW_G3C', oStruG3C,'G3CMASTER')
	oView:CreateHorizontalBox('TELA',100)
	oView:SetOwnerView('VIEW_G3C','TELA')
	
Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE CLASSE DE SERVI�O - DEFINE AROTINA (MVC)

@sample 	TURA003()
@return  	aRotina
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
	ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA003'	OPERATION 2	ACCESS 0 // Visualizar
	ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA003'	OPERATION 3	ACCESS 0 // Incluir
	ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA003'	OPERATION 4	ACCESS 0 // Modificar
	ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA003'	OPERATION 5	ACCESS 0 // Borrar
	ADD OPTION aRotina Title STR0009 ACTION 'VIEWDEF.TURA003'	OPERATION 9	ACCESS 0 // Copiar
	
Return(aRotina)

/*/{Protheus.doc} TudoOk
Fun��o que verifica se o c�digo de Classe de Servi�o j� existe para o Fornecedor.
@author	Elton Teodoro Alves
@since 22/04/2015
@version 1.0
@param oModel, objeto, Modelo de dados da Field
@return L�gico Retorno da valida��o do modelo de dados
/*/
Static Function TudoOk( oModel )
	
	Local	lRet		:=	.T.
	Local	nOperation	:=	oModel:GetOperation()
	Local	aArea		:=	GetArea()
	
	If cValToChar( nOperation ) $ '3'
		
		DbSelectArea( 'G3C' )
		DbSetOrder( 2 ) // G3C_FILIAL+G3C_CODFOR+G3C_LJFOR+G3C_CODIGO
		
		If DbSeek(;
				xFilial( 'G3C' ) +;
				oModel:GetModel( 'G3CMASTER' ):GetValue( 'G3C_CODFOR' ) +;
				oModel:GetModel( 'G3CMASTER' ):GetValue( 'G3C_LJFOR' ) +;
				oModel:GetModel( 'G3CMASTER' ):GetValue( 'G3C_CODIGO' ) )
			
			Help( ,, 'Help',, STR0010, 1, 0 ) // J� Existe este C�digo cadastrado para este Fornecedor.
			
			lRet	:=	.F.
			
		End If
		
		RestArea( aArea )
		
	End If
	
Return lRet

/*/{Protheus.doc} TA003CPFOR
Fun��o utilizada no inicializador padr�o dos campos G3C_CODFOR e G3C_LJFOR, para trazer o C�digo/Loja do Fornecedor do registro posicionado. 
@author elton.alves
@since 28/09/2015
@version P12
@return Caracter, Conte�do do Campo do registro posicionado. 
/*/
Function TA003CPFOR()
	
	Local cRet   := ''
	local oModel := FwModelActive()
	
	If oModel:IsCopy()
		
		cRet := G3C->&( SubStr( ReadVar(), 4, Len( ReadVar() ) - 3  ) )
		
	EndIf
	
Return cRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	15/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}
aRet:= TURI003( cXml, nTypeTrans, cTypeMessage )

Return aRet