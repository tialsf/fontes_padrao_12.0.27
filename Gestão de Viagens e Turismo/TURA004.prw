#INCLUDE "TURA004.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA004()

CADASTRO DE CART�O TURISMO - SIGATUR

@sample 	TURA004()
@return		lRet: .T.
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA004()

Local oBrowse	:= FWMBrowse():New()

oBrowse:SetAlias('G3D')
oBrowse:AddLegend( "G3D_MSBLQL == '2'"	, "GREEN" 	, STR0007 ) 	// "Ativo"
oBrowse:AddLegend( "G3D_MSBLQL == '1'"	, "GRAY" 	, STR0008 ) 	// "Bloqueado"
oBrowse:SetDescription(STR0001)		// Cadastro de Cart�es Turismo
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Define modelo de dados (MVC)

@sample 	ModelDef()
@return  	oModel: objeto do modelo de dados
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local bPosValid := { |oMdl| TA004POSVLD( oMdl ) }
Local bLoad     := { |oMdl| TA004LOAD( oMdl ) }
Local oModel    := MPFormModel():New('TURA004',/*bPreValidacao*/, bPosValid ,/*bCommit*/,/*bCancel*/)
Local oStruG3D  := FWFormStruct(1,'G3D',/*bAvalCampo*/,/*lViewUsado*/)
Local xAux := {}
oStruG3D:SetProperty( 'G3D_NCARD'	,MODEL_FIELD_VALID,{|| .T. })
oStruG3D:SetProperty( 'G3D_DFECHA'	,MODEL_FIELD_VALID,{||oModel:GetValue('G3DMASTER','G3D_DFECHA') >= 0 .and. oModel:GetValue('G3DMASTER','G3D_DFECHA') <= 31})
oStruG3D:SetProperty( 'G3D_DVENC'	,MODEL_FIELD_VALID,{||oModel:GetValue('G3DMASTER','G3D_DVENC') >= 0 .and. oModel:GetValue('G3DMASTER','G3D_DVENC') <= 31})
oStruG3D:SetProperty( 'G3D_DIADE'	,MODEL_FIELD_VALID,{||oModel:GetValue('G3DMASTER','G3D_DIADE') >= 1 .and. oModel:GetValue('G3DMASTER','G3D_DIADE') <= 31})
oStruG3D:SetProperty( 'G3D_DIAATE'	,MODEL_FIELD_VALID,{||oModel:GetValue('G3DMASTER','G3D_DIAATE') >= 1 .and. oModel:GetValue('G3DMASTER','G3D_DIAATE') <= 31})
oStruG3D:SetProperty( 'G3D_DCONC'	,MODEL_FIELD_VALID,{|| Vazio() .Or. ( Val(oModel:GetValue('G3DMASTER','G3D_DCONC')) >= 0 .And. Val(oModel:GetValue('G3DMASTER','G3D_DCONC'))  <= 31 )})

xAux := FwStruTrigger( 'G3D_NCARD', 'G3D_NRESU', 'RIGHT(Alltrim(FwFldGet("G3D_NCARD")),4 )', .F. )
oStruG3D:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

oModel:AddFields('G3DMASTER', /*cOwner*/, oStruG3D, /*Criptog()/, /*bPosValidacao*/, /*bCancel*/, bLoad)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de Cart�es Turismo

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Define a interface do cadastro (MVC)

@sample 	ViewDef
@return   	oView: objeto da vis�o dos dados
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()
	
Local oView    := FWFormView():New()
Local oModel   := FWLoadModel('TURA004')
Local oStruG3D := FWFormStruct(2,'G3D')

oView:SetModel(oModel)
oView:AddField('VIEW_G3D', oStruG3D, 'G3DMASTER')
oView:CreateHorizontalBox('TELA', 100)
oView:SetOwnerView('VIEW_G3D', 'TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Define menu do cadastro (MVC)

@sample 	MenuDef()
@return  	aRotina: array com op��es do menu
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()
	
Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Pesquisar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA004'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA004'	OPERATION 3	ACCESS 0 // Incluir
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA004'	OPERATION 4	ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA004'	OPERATION 5	ACCESS 0 // Excluir
ADD OPTION aRotina TITLE STR0013 ACTION 'TURA004LEG()'		OPERATION 5 	ACCESS 0 // Legenda	

Return(aRotina)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004LOAD

Carrega dados descriptografados quando opera��o diferente de insert.

@sample 	TA004LOAD(oModel)
@return  	aDados: dados descriptografados
@author  	J.L.CAMATTA
@since   	05/03/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Static Function TA004LOAD(oMdl)

Local aArea	   	 := GetArea()
Local aCampos    := oMdl:GetStruct():GetFields()
Local aLoad		 := {}
Local aDados	 := {}
Local nOperation := oMdl:GetOperation()
Local nX		 := 0
Local uValor 	 := Nil

// Carrega campos G3D_CODSEG e G3D_NCARD descriptografados quando opera��o
// for diferente de insert
If nOperation <> MODEL_OPERATION_INSERT
	For nX := 1 To Len(aCampos)
		If !aCampos[nX][MODEL_FIELD_VIRTUAL]
       		Do Case
       			Case AllTrim(aCampos[nX][MODEL_FIELD_IDFIELD]) == 'G3D_CODSEG'       				
       				uValor := RC4Crypt(TurHex2Crypt(AllTrim(G3D->G3D_CODSEG)), G3D->G3D_CODIGO, .F.)
        			aAdd(aLoad, uValor)

       			Case AllTrim(aCampos[nX][MODEL_FIELD_IDFIELD]) == 'G3D_NCARD'				    
				    uValor := RC4Crypt(TurHex2Crypt(G3D->G3D_NCARD) , G3D->G3D_CODIGO, .F.)
         			aAdd(aLoad, uValor)

         		OtherWise
         			uValor := G3D->&(aCampos[nX][MODEL_FIELD_IDFIELD])
         		 	aAdd(aLoad, uValor)
       		EndCase
       	Else
			uValor := CriaVar(aCampos[nX][MODEL_FIELD_IDFIELD], .F.)
			aAdd(aLoad, uValor)
        EndIf
	Next

	aDados:= {aLoad, G3D->(Recno())}
EndIf

RestArea(aArea)

Return aDados

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004POSVLD
Criptografa o campo

@sample 	TA004POSVLD(oModel)
@return  	lRet: indica se o modelo � v�lido
@author  	Fanny Mieko Suzuki
@since   	05/03/2015
@version  	P12
@param		oModel
/*/
//------------------------------------------------------------------------------------------
Static Function TA004POSVLD(oModel)

Local aAreaG3D  	:= G3D->( GetArea() )
Local nOperation	:= oModel:GetOperation()
Local oMdl			:= oModel:GetModel('G3DMASTER')
Local aSaveLines	:= FWSaveRows()
Local lRet       	:= .T.

If ( nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE )

	// Obriga preenchimento de ao menos um Segmento de Neg�cio
	If !TA004VLDSEG(oMdl)
		Help("TURA004", 1, STR0010, , STR0015, 1, 0) // "Aten��o"   "Escolha pelo menos um segmento"
		lRet := .F.
	EndIf

	// Valida concilia��o
	If lRet .And. !TA004VLDCONC(oMdl)
		Help('TURA004', 1, STR0010, , STR0014, 1, 0) // "Aten��o" 	"Os campos da concilia��o devem ser preenchidos."
   		lRet := .F.
	EndIf
EndIf

If lRet .And. nOperation == MODEL_OPERATION_INSERT

	// Valida se numero do cart�o/Tipo j� est�o cadastrados
	If !TA004VLDKEY()
		Help('TURA004', 1, STR0010, , STR0012, 1, 0) // "Aten��o" 	"N�mero de Cart�o/Tipo j� cadastrado."
		lRet := .F.
	EndIf

	// Criptografa o campo do c�digo de seguran�a do cart�o do cliente
	If lRet
		lRet := TA004CRPTN(oMdl)
	EndIf
EndIf

RestArea(aAreaG3D)

FWRestRows( aSaveLines )

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004VLDKEY
Valida chave da tabela G3D

@sample 	TA004VLDKEY()
@return  	lRet: indica se o n�mero do cart�o j� foi cadastrado anteriormente
@author  	Veronica de Almeida
@since   	14/09/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Static Function TA004VLDKEY()

Local aArea     := GetArea()
Local cAliasQry := GetNextAlias()
Local cCodBan   := FwFldGet('G3D_CODBAN')
Local cCodigo   := FwFldGet('G3D_CODIGO')
Local cNCard    := FwFldGet('G3D_NCARD')
Local cNCardQry := ''
Local cNumResu  := FwFldGet('G3D_NRESU')
Local cTipo     := FwFldGet('G3D_TIPO')
Local lRet      := .T.

// Pesquisa registros com mesmo tipo, c�digo banc�rio e n�mero resumido
// para deduzir a compara��o do n�mero do cart�o
BeginSql Alias cAliasQry
		SELECT G3D_FILIAL, G3D_CODIGO, G3D_TIPO, G3D_NCARD
		FROM %table:G3D% G3D
		WHERE G3D_FILIAL	= %xFilial:G3D%
		AND G3D_TIPO		= %Exp:cTipo%
		AND G3D_NRESU		= %Exp:cNumResu%
		AND G3D_CODBAN		= %Exp:cCodBan%
	  	AND G3D.%NotDel%
	EndSql

// para os registros selecionados, descriptografa e compara com valor informado
// pelo usu�rio. Caso sejam iguais, n�o permite inclus�o e exibe mensagem de Help
If (cAliasQry)->(!EOF())
	While lRet .And. (cAliasQry)->(!EOF())
		cNCardQry := RC4Crypt( LEFT((cAliasQry)->G3D_NCARD,16), (cAliasQry)->G3D_CODIGO, .F. )
		If cNCardQry == cNCard
			lRet := .F.
		EndIf
		(cAliasQry)->(DbSkip())
	EndDo
EndIf

(cAliasQry)->( DbCloseArea() )

RestArea( aArea )
                    
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004VLDSEG
Obriga sele��o de no minimo um Segmento de Neg�cio

@sample 	TA004VLDSEG(oModel)
@return  	lRet: indica se segmento est� validado
@author  	Veronica de Almeida
@since   	14/09/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Static Function TA004VLDSEG(oMdl)

Local lLazer  := oMdl:GetValue( "G3D_LAZER" )
Local lCorpor := oMdl:GetValue( "G3D_CORP" )
Local lEvento := oMdl:GetValue( "G3D_EVENTO" )
Local lRet    := .T.

// Se ao menos um segmento n�o for informado, n�o permite inclus�o/altera��o
If !lCorpor .AND. !lEvento .AND. !lLazer
	lRet:= .F.
EndIf

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004VLDDT

Valida se o m�s/ano informado � menor que a mes/ano corrente.
Chamado do X3_VALID do campo G3D_AVALID

@sample 	TA004VLDDT(cAno)
@return  	lRet: indica se o mes/ano � v�lido
@author  	Lucas.Brustolin
@since   	13/05/2015
@version  	P12
@param		cAno - Ano Informado pelo usuario
/*/
//------------------------------------------------------------------------------------------
Function TA004VLDDT(cAno)
	
Local cMes := STRZERO(Val(FwFldGet("G3D_MVALID")), 2)
Local lRet := .T.

If !Empty(cMes) .And. !Empty(cAno)
	If Val(AnoMes(Date())) > Val( cAno + cMes )
		Help('TURA004', 1, STR0010, , STR0016, 1, 0 ) // "Aten��o" - "M�s/Ano de validade deve ser maior ou igual a data do sistema."
		lRet := .F.
	EndIf
EndIf

Return(lRet)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA004LEG( )

Exibe a tela de Legenda.

@sample 	TURA004LEG()             
@author  	Lucas.Brustolin
@since   	13/05/2015
@version  	P12
@return 	lRet
/*/
//------------------------------------------------------------------------------------------
Function TURA004LEG()
			 
BrwLegenda(STR0001, STR0017, {{"BR_VERDE", STR0007}, {"BR_CINZA", STR0008}})   // "Cadastro de Cart�es Turismo"    "Legenda"	"Ativo"	"Bloqueado"	

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004VLDCONC

Valida campos obrigatorios se for conciliacao

@sample 	TA004VLDCONC(oModel)
@return  	lRet: indica se concilia��o est� validada
@author  	Veronica de Almeida
@since   	14/09/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Static Function TA004VLDCONC(oMdl)

Local lRet        := .T.
Local cConc       := FwFldGet('G3D_CONC')
Local lDConcEmpty := Empty(FwFldGet('G3D_DCONC'))
Local lPConcEmpty := Empty(FwFldGet('G3D_PCONC'))

// Caso a conciliacao igual sim avalia o preenchimento dos campos relacionados
If cConc == '1' .And. (lDConcEmpty .Or. lPConcEmpty)
	lRet := .F.
EndIf

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA004CRPTN
Criptografa c�digo de seguran�a (G3D_CODSEG) e n�mero do cart�o (G3D_NCARD) utilizando como
chave de criptografia o c�digo do cart�o (G3D_CODIGO)

@sample 	TA004CRPTN(oMdl)
@author  	Veronica de Almeida
@since   	14/09/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Function TA004CRPTN(oMdl)

Local lRet := .T.

If (lRet := oMdl:SetValue("G3D_CODSEG", IIF(!Empty(oMdl:GetValue("G3D_CODSEG")), RC4Crypt(Left(oMdl:GetValue("G3D_CODSEG"), 3), M->G3D_CODIGO, .T.), "")))
	If (lRet := oMdl:SetValue("G3D_NCARD", RC4Crypt(Left(oMdl:GetValue("G3D_NCARD"), 16), M->G3D_CODIGO, .T.)))
		If (lRet := oMdl:SetValue("G3D_NRESU", Left(oMdl:GetValue("G3D_NRESU"), 19)))
		EndIf
	EndIf
EndIf

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI004( cXml, nTypeTrans, cTypeMessage )

Return aRet