#INCLUDE "TURA006.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

#DEFINE GRIDMAXLIN 99999

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA006()

CADASTRO DE ENTIDADES ADICIONAIS - SIGATUR

@sample 	TURA006()
@return                             
@author  	Fanny Mieko Suzuki 
@since   	25/02/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA006()

Local oBrowse	:= Nil
 
 dbSelectArea('G3F')

// Cria o Browse 
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G3F')
oBrowse:AddLegend	( "G3F_MSBLQL=='1'", "GRAY"  , STR0008 )// "Bloqueado"
oBrowse:AddLegend	( "G3F_MSBLQL=='2'", "GREEN" , STR0009 )// "Ativo"
oBrowse:SetDescription(STR0001) // "Cadastro de Entidades Adicionais"
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO DE ENTIDADES ADICIONAIS - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA006()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	25/02/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

// CRIA ESTRUTURA DO OBJETO
Local oModel
Local oStruG3F := FWFormStruct(1,'G3F',/*bAvalCampo*/,/*lViewUsado*/)
Local oStruG3G := FWFormStruct(1,'G3G',/*bAvalCampo*/,/*lViewUsado*/)

//oStruG3F:SetProperty('G3F_VALIDA',MODEL_FIELD_VALID,{|oModel| TR6VALIDA(oModel)/*:GetModel( 'G3FMASTER' ),oModel:GetModel( 'G3GDETAIL' )*/})

// CRIA OBJETO DO MODELO DE DADOS
oModel:= MPFormModel():New('TUR006',,{|oModel|TR6VALCHAV(oModel, oModel:GetModel( 'G3FMASTER' ),oModel:GetModel( 'G3GDETAIL' ))},{ |oModel| T006GRAVA( oModel )} /*COMMIT*/,/*CANCEL*/)
                                          
// ADICIONA AO MODELO ESTRUTURA DE FORMUL�RIO DE EDI��O POR CAMPOS
oModel:AddFields('G3FMASTER',/*cOwner*/,oStruG3F,/*Criptog()*/,{|oModel|TR6VLSEG(oModel:GetModel( 'G3FMASTER' ))},/*bCarga*/)

// ADICIONA AO MODELO ESTRUTURA DE FORMUL�RIO DE EDI��O DE GRID
oModel:AddGrid( 'G3GDETAIL', 'G3FMASTER', oStruG3G, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/,/*bPosValidacao*/ , /*BLoad*/ )

// Define a inser��o de um item no grid como .F.
oModel:GetModel( 'G3GDETAIL' ):SetOptional( .T. )

// Define o n�mero m�ximo de linhas que o model poder� receber, de acordo com a define GRIDMAXLIN.
oModel:GetModel( 'G3GDETAIL' ):SetMaxLine(GRIDMAXLIN)

// ALTERA PROPRIEDADE DO CAMPO PARA N�O OBRIGAT�RIO
oStruG3G:SetProperty( 'G3G_CLIENT'	,	MODEL_FIELD_OBRIGAT,.F.)
oStruG3G:SetProperty( 'G3G_LOJA'	, 	MODEL_FIELD_OBRIGAT,.F.)
oStruG3G:SetProperty( 'G3G_TIPO'	, 	MODEL_FIELD_OBRIGAT,.F.)

// FAZ RELACIONAMENTO ENTRE COMPONENTES DO MODEL
oModel:SetRelation( 'G3GDETAIL', { { 'G3G_FILIAL', 'xFilial( "G3G" )' }, { 'G3G_CLIENT', 'G3F_CODCLI' },{ 'G3G_LOJA', 'G3F_LOJA' },{ 'G3G_TIPO', 'G3F_TIPO' } }, G3G->( IndexKey( 1 ) ) )// G3G_FILIAL+G3G_CLIENT+G3G_LOJA+G3G_TIPO

// ADICIONA A DESCRI��O DO MODELO DE DADO
oModel:SetDescription(STR0001) // "Cadastro de Entidades Adicionais"

// ADICIONA A DESCRI��O DO COMPONENTE DO MODELO DE DADOS
oModel:GetModel( 'G3FMASTER' ):SetDescription( STR0001 ) // "Cadastro de Entidades Adicionais"
oModel:GetModel( 'G3GDETAIL' ):SetDescription( STR0002 )// "Itens - Entidades Adicionais"

oModel:GetModel("G3GDETAIL"):SetUniqueLine({"G3G_ITEM"})

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO DE ENTIDADES ADICIONAIS - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA006()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	25/02/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

// CRIA ESTRUTURA A SER USADA NA VIEW
Local oView
Local oModel  	:= FWLoadModel('TURA006')
// CRIA OBJETO DE MODELO DE DADOS BASEADO NA MODELDEF
Local oStruG3F 	:= FWFormStruct(2,'G3F')
Local oStruG3G 	:= FWFormStruct(2,'G3G')

// REMOVE CAMPOS DO GRID
oStruG3G:RemoveField("G3G_CLIENT")
oStruG3G:RemoveField("G3G_LOJA")
oStruG3G:RemoveField("G3G_TIPO")

// CRIA OBJETO DA VIEW
oView:= FWFormView():New()

// DEFINE QUAL MODELO DE DADOS SER� UTILIZADO
oView:SetModel(oModel)

// ADICIONA VIEW DE CONTROLE DO TIPO FORMFIELDS
oView:AddField('VIEW_G3F', oStruG3F,'G3FMASTER')

// ADICIONA VIEW DE CONTROLE DO TIPO FORMGRID
oView:AddGrid(  'VIEW_G3G', oStruG3G, 'G3GDETAIL' )

// CRIA BOX HORIZONTAL
oView:CreateHorizontalBox( 'SUPERIOR', 40 )
oView:CreateHorizontalBox( 'INFERIOR', 60 )

// RELACIONA O ID DA VIEW COM BOX DE EXIBI��O
oView:SetOwnerView( 'VIEW_G3F', 'SUPERIOR' )
oView:SetOwnerView( 'VIEW_G3G', 'INFERIOR' )

// CRIA BOT�O PARA IMPORTAR ITENS NA TELA DO REGISTRO
oView:AddUserButton(STR0010, "", { |oView| TA006Impt(oView, oModel)},,,{MODEL_OPERATION_UPDATE}) // "Importa Itens"

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO DE ENTIDADES ADICIONAIS - DEFINE AROTINA (MVC) 

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	25/02/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}
Local aArea   := GetArea()

// ADICIONA MENU
ADD OPTION aRotina TITLE STR0003  ACTION 'PesqBrw' 			OPERATION 1  ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0004  ACTION 'VIEWDEF.TURA006'	OPERATION 2  ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0005  ACTION 'VIEWDEF.TURA006'	OPERATION 3  ACCESS 0 // "Incluir"
ADD OPTION aRotina TITLE STR0006  ACTION 'VIEWDEF.TURA006'	OPERATION 4  ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0007  ACTION 'VIEWDEF.TURA006'	OPERATION 5  ACCESS 0 // "Excluir"

RestArea(aArea)

Return(aRotina)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA006Impt(oView, oModel)

TELA DE IMPORTA��O DE ARQUIVO DE ENTIDADE ADICIONAL  

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function TA006Impt(oView, oModel)

Local oModDet   := oModel:GetModel('G3GDETAIL')
Local aArea     := GetArea()
Local cAlias    := GetNextAlias()			// Abre tabela qd utilizar query
Local cTitulo   := STR0041 					// "Importa��o - Itens da Entidade Adicional"
Local lAchou    := .F.						// Verifica se achou o registro da tela no arquivo de importa��o
Local lBloq     := .F.						// Bloquea n�o encontrados
Local lContinua := .F.						// Verifica se importa ou n�o o Arquivo
Local lSemLog   := .F.
Local aDados    := {} 						// Carrega o arquivo selecionado
Local aImport   := {}						// Registros importados
Local aNImport  := {}						// Registros n�o importados
Local aDelet    := {}						// Registros excluidos
Local aBloq     := {}						// Registros bloqueados
Local aFldsExt  := TA006FldsExt(oModDet)
Local cLocal    := ""						// Caminho do Arquivo a ser importado
Local cCodCli   := ""
Local cLoja     := ""
Local cFilG3F   := ""
Local cTipo     := ""
Local cItem     := ""
Local cDescr    := ""
Local cTxtCsv   := ""
Local cLogPath  := ""
Local cArquivo  := ""
Local cRootPath := GetSrvProfString("ROOTPATH","") + '\' // Caminho do Protheus_Data
Local cTempPath := GetTempPath() + '\' // Caminho do tempor�rio do windows
Local nHandCre  := 0
Local nHandOp   := 0
Local nLength   := 0
Local nX, nY    := 1
Local nI, nD    := 1
Local nA, nB    := 1
Local lNImport  := .F.
Local cData     := Date()

Private oMark

If oModel:GetValue('G3FMASTER', 'G3F_MSBLQL') == '1' .Or. oModel:GetValue('G3FMASTER', 'G3F_VALIDA') == '2' 
	If oModel:GetValue('G3FMASTER', 'G3F_MSBLQL') == '1' 
		Help( "TA006Impt", 1, STR0022, , STR0048, 1, 0) // "Aten��o" - "Importa��o n�o permitida porque o registro est� bloqueado."
	ElseIf oModel:GetValue('G3FMASTER', 'G3F_VALIDA') == '2'
		Help( "TA006Impt", 1, STR0022, , STR0049, 1, 0) // "Aten��o" - "Importa��o n�o permitida porque o registro n�o aceita itens (Valida��o igual N�o)."
	EndIf 	 
Else
	// MONTA TELA DE PARAMETROS PARA IMPORTA��O DE ARQUIVO
	DEFINE MSDIALOG oDlg TITLE cTitulo FROM 0, 0 TO 180, 700 PIXEL
	
	// CAMINHO DO ARQUIVO A SER IMPORTADO
	@ 15, 15  Say    STR0011 	OF oDlg PIXEL // "Arquivo para Importa��o ? "
	@ 15, 85  MsGet  oLocal Var cLocal Size 180, 10 OF oDlg PIXEL Valid(cLocal := Alltrim(cLocal))
	@ 15, 270 BUTTON STR0015	SIZE 50, 12 ACTION (cLocal := cGetFile( I18N("#1|*.csv|#2|*.txt", {STR0046, STR0047}), STR0016, 2, , .T., ,.F., .T. ) ) OF oDlg PIXEL Valid IIF(cLocal == "",cLocal := Space(50),) // "Procurar" / "Selecione o arquivo (.CSV ou .TXT)" / "Arquivo CSV" / "Arquivo TXT" 
	
	// INFORMA SE BLOQUEIA N�O ENCONTRADOS
	@ 35, 15 CheckBox olBloq Var lBloq Prompt STR0012  Size 150,9 Pixel Of oDlg // "Bloquear n�o encontrados ? "   
	
	// BOT�O CONTINUA
	@ 55, 120  BUTTON STR0017 SIZE 50, 11 ACTION ( Iif(ExistArq(cLocal),(lContinua:= .T.,oDlg:End()),) )	OF oDlg PIXEL // "Ok"
	
	// BOT�O CANCELA
	@ 55, 180 BUTTON STR0018 SIZE 50, 11 ACTION (lContinua := .F.,oDlg:End()) OF oDlg PIXEL // "Cancelar"
	
	ACTIVATE MSDIALOG oDlg CENTERED 
	  
	// SE BOT�O CONTINUA  
	IF lContinua
	
		cFilG3F := G3F->G3F_FILIAL
		cCodCli := G3F->G3F_CODCLI
		cLoja   := G3F->G3F_LOJA
		cTipo   := G3F->G3F_TIPO
		
		// CRIA ARQUIVO DE LOG
		cArquivo := DTOS(dDatabase) + "_" + Substr(time(),1,2) + "_" + Substr(time(),4,2) + "_" + cCodCli + "_" + cLoja +".TXT"
		
		// VERIFICA SE ARQUIVO DE LOG FOI CRIADO
		IF (nHandCre := FCREATE(cRootPath + cArquivo, 0)) < 0
			IF (nHandCre := FCREATE(cTempPath + cArquivo, 0)) < 0
				lSemLog := FwAlertYesNo(STR0051)		// "N�o foi poss�vel criar arquivo onde seria registrado o log da importa��o. Deseja continuar?" 
			ELSE
				cLogPath := cTempPath
			ENDIF
		ELSE	
			cLogPath := cRootPath
		ENDIF
	
		// VALIDA SE CODIGO DO CLIENTE, LOJA E TIPO FORAM INFORMADOS
		IF cCodCli == "" .OR. cLoja == "" .OR. cTipo == ""
			Help( "TA006Impt", 1, STR0022, , STR0019, 1, 0) // "Aten��o" - "Informar o C�digo do cliente, a Loja e o Tipo de Entidade para que os itens possam ser importados"	 
		ELSE
			// SELECIONA ARQUIVO
			FT_FUSE(cLocal)
			
			// VAI PARA INICIO DO ARQUIVO
			FT_FGOTOP()
			
			// GUARDA ARQUIVO EM ARRAY SEPARADO POR ;
			WHILE !FT_FEOF()
				cLinha := FT_FReadLn()
				aAdd(aDados,{})
				aDados[Len(aDados)]:= STRTOKARR(cLinha, ";")
				FT_FSKIP()
			END
			
			// FECHA ARQUIVO
			FT_FUSE()
		
			If !TA006VldDados(aDados)
				Help( "TA006Impt", 1, STR0022, , STR0045, 1, 0) // "Aten��o" - "A estrutura do arquivo � inv�lida. Importa��o n�o realizada."	 
			ELSE
				// PERCORRE ITENS DO MODEL
				FOR nX := 1 to oModDet:Length()
					oModDet:GoLine(nX)
					
					If Empty(AllTrim(oModDet:GetValue("G3G_ITEM") + oModDet:GetValue("G3G_DESCR")))
						oModDet:DeleteLine()
					Else
						// LOCALIZANDO NO ARQUIVO O ITEM DO MODEL
						If aScan(aDados, {|x| AllTrim(x[1]) == AllTrim(oModDet:GetValue("G3G_ITEM"))}) == 0
						
							// VERIFICA SE ITEM TEVE MOVIMENTA��O OU SE N�O � BLOQUEADO
					    	IF !lBloq .AND. !TA006VldX9(oModDet) .AND. oModDet:GetValue("G3G_MSBLQL") == "2"
					    		aAdd(aDelet, {oModDet:GetValue("G3G_ITEM"), oModDet:GetValue("G3G_DESCR")})		// DELETA REGISTRO DE ITEM
								oModDet:DeleteLine()
							ELSE // SE ITEM DO ARQUIVO E MODEL FOR <>
					    		aAdd(aBloq, {oModDet:GetValue("G3G_ITEM"), oModDet:GetValue("G3G_DESCR")})			// BLOQUEADOS OU COM MOVIMENTA��O
								oModDet:SetValue( "G3G_MSBLQL", "1")	
							ENDIF
						EndIf
					EndIf
		    	NEXT nX	    
		    	
				// PERCORRE ITENS DO ARQUIVO
				FOR nY := 1 To len(aDados)
					cItem	:= AllTrim(aDados[nY][1])
					cDescr	:= AllTrim(aDados[nY][2])
					
					lAchou   := .F.
					lNImport := .F.
					
					// VALIDA TAM DOS CAMPOS A SEREM IMPORTADOS
					IF (Len(aDados[nY][1])) >= 1 .AND. (Len(aDados[nY][1])) <= TamSX3("G3G_ITEM" )[1] .AND. ;
					   (Len(aDados[nY][2])) >= 1 .AND. (Len(aDados[nY][2])) <= TamSX3("G3G_DESCR")[1]
					   
						// LOCALIZANDO O ITEM NO MODEL
						If oModDet:SeekLine({{'G3G_FILIAL', cFilG3F}, ;
		                                   {'G3G_CLIENT', cCodCli}, ;
		                                   {'G3G_LOJA'  , cLoja  }, ;
		                                   {'G3G_TIPO'  , cTipo  }, ;
		                                   {'G3G_ITEM'  , cItem  }})
					   
							// VERIFICA SE N�O � DELETADO
							IF !oModDet:IsDeleted()
								IF Upper(AllTrim(oModDet:GetValue("G3G_DESCR"))) <> Upper(AllTrim(aDados[nY][2])) .Or. Len(aFldsExt) > 0
									lAchou := .T.
								ELSE
									// SE DESCRI��O FOR =
									lNImport := .T.
								ENDIF
							ENDIF
						EndIf
		
						IF lAchou
							// ADICIONA ITEM IMPORTADO/ATUALIZADO
							aAdd(aImport,{})
							aImport[Len(aImport)]:= { cItem + space(TamSX3("G3G_ITEM")[1]-Len(cItem)), cDescr + space(TamSX3("G3G_DESCR")[1]-Len(cDescr))} 
																			
							// ATUALIZA DESCRI��O NO MODEL
							oModDet:SetValue( "G3G_DESCR", 	cDescr)	
							
							// ATUALIZANDO OS CAMPOS EXTRAS
							If Len(aDados[nY]) > 2
								For nI := 1 To Len(aFldsExt)
									If (nI + 2) <= Len(aDados[nY])
										oModDet:SetValue(aFldsExt[nI][3], aDados[nY][nI + 2])
									EndIf
								Next nI
							EndIf 
						ELSE
							IF lNImport
								//ADICIONA ITEM N�O IMPORTADO
								aAdd(aNImport, {})
								aNImport[Len(aNImport)]:= { cItem + space(TamSX3("G3G_ITEM")[1]-Len(cItem)), cDescr + space(TamSX3("G3G_DESCR")[1]-Len(cDescr))}  			
							ELSE
								// ADICIONA ITEM IMPORTADO
								aAdd(aImport,{})
								aImport[Len(aImport)]:= { cItem + space(TamSX3("G3G_ITEM")[1]-Len(cItem)), cDescr + space(TamSX3("G3G_DESCR")[1]-Len(cDescr))} 
								
								// INSERI REGISTRO NO MODEL
								oModDet:AddLine()
								oModDet:SetValue( "G3G_CLIENT",	cCodCli)
								oModDet:SetValue( "G3G_LOJA", 	cLoja)
								oModDet:SetValue( "G3G_TIPO", 	cTipo)
								oModDet:SetValue( "G3G_ITEM", 	cItem)
								oModDet:SetValue( "G3G_DESCR", 	cDescr)
								oModDet:SetValue( "G3G_MSBLQL",	"2")						
		
								// ATUALIZANDO OS CAMPOS EXTRAS
								If Len(aDados[nY]) > 2
									For nI := 1 To Len(aFldsExt)
										If (nI + 2) <= Len(aDados[nY])
											oModDet:SetValue(aFldsExt[nI][3], aDados[nY][nI + 2])
										EndIf
									Next nI
								EndIf 
							ENDIF				
						ENDIF
					ELSE
						// N�O IMPORTADO -  FORA DA ESTRUTURA
						cItem	:= Substr(cItem,1,6)
						cDescr := Substr(cDescr,1,40)
						aAdd(aNImport, {})
						aNImport[Len(aNImport)]:= { cItem + space(TamSX3("G3G_ITEM")[1]-Len(cItem)), cDescr + space(TamSX3("G3G_DESCR")[1]-Len(cDescr))} 			
					ENDIF	
				NEXT nY
	
				If !lSemLog
					// CABE�ALHO DO ARQUIVO
					FSEEK(nHandCre, 0, FS_END)
					FWRITE(nHandCre, STR0025 + CRLF + CRLF) // "RELAT�RIO DE IMPORTA��O DE ITENS DE ENTIDADE ADICIONAL"
					FWRITE(nHandCre, STR0031 + cCodCli + STR0032 +  cLoja + STR0033 + cTipo + STR0034 + DTOS(cData) + CRLF + CRLF) // " C�digo do Cliente "	/ " Loja " / " Tipo " / " Data "
					
					// ADICIONA NO ARQUIVO DE LOG DOS DELETADOS
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre, STR0035 + STR0028 + CRLF) // " DELETADOS: " - "Item da Entidade sem movimenta��o"
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre,  STR0023 + space(TamSX3("G3G_ITEM")[1]-Len(STR0023)) +" ; "+ STR0024 + space(TamSX3("G3G_DESCR")[1]-Len(STR0024)) + CRLF) // "ITEM" / "DESCRI��O" 
				
					FOR nD := 1 To Len(aDelet) 
						FWRITE( nHandCre, aDelet[nD][1] +" ; "+ aDelet[nD][2] + CRLF)
					NEXT nD
					FWRITE( nHandCre, + CRLF + CRLF)
					
					// ADICIONA NO ARQUIVO DE LOG DOS BLOQUEADOS
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre, STR0036 + STR0027 + CRLF) // " BLOQUEADOS: " - "Item da Entidade n�o deletado - com movimenta��o ou bloqueado"
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre,  STR0023 + space(TamSX3("G3G_ITEM")[1]-Len(STR0023)) +" ; "+ STR0024 + space(TamSX3("G3G_DESCR")[1]-Len(STR0024)) + CRLF) // "ITEM" / "DESCRI��O" 
				
					FOR nB := 1 To Len(aBloq) 
						FWRITE( nHandCre, aBloq[nB][1] +" ; "+ aBloq[nB][2] + CRLF )
					NEXT nB
					FWRITE( nHandCre, + CRLF + CRLF)
					
					// ADICIONA NO ARQUIVO DE LOG OS IMPORTADOS
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre, STR0037 + STR0026 + CRLF) // " IMPORTADOS: " - "Item da entidade importado ou atualizado"
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre,  STR0023 + space(TamSX3("G3G_ITEM")[1]-Len(STR0023)) +" ; "+ STR0024 + space(TamSX3("G3G_DESCR")[1]-Len(STR0024)) + CRLF) // "ITEM" / "DESCRI��O" 
				
					FOR nI := 1 To Len(aImport) 
						FSEEK(nHandCre, 0, FS_END)
						FWRITE( nHandCre, aImport[nI][1] +" ; "+ aImport[nI][2] + CRLF )
					NEXT nI
					FWRITE( nHandCre, + CRLF + CRLF)
					
					// ADICIONA NO ARQUIVO DE LOG OS N�O IMPORTADOS
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre, STR0038 + STR0029 + CRLF) // " N�O IMPORTADOS: " - "Registro a ser importado fora da formata��o (Item(06); e Descri��o(40))"
					FWRITE( nHandCre, "====================================================================================================== "+ CRLF)
					FWRITE( nHandCre,  STR0023 + space(TamSX3("G3G_ITEM")[1]-Len(STR0023)) +" ; "+ STR0024 + space(TamSX3("G3G_DESCR")[1]-Len(STR0024)) + CRLF) // "ITEM" / "DESCRI��O" 
				
					FOR nX := 1 To Len(aNImport) 
						FWRITE( nHandCre, aNImport[nX][1] +" ; "+ aNImport[nX][2] + CRLF )
					NEXT nX
					
					FCLOSE(nHandCre)
					If FwAlertYesNo(I18N(STR0052, {cLogPath}))		// "Arquivo de log de importa��o gerado em #1. Deseja visualiz�-lo?" 
						ShellExecute('Open', cLogPath + cArquivo, '', GetTempPath(), 1)
					EndIf   
				EndIf                
			EndIf		
		ENDIF
	ENDIF	
EndIf
	
Return NIL

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ExistArq()

VERIFICA EXISTENCIA DO ARQUIVO SELECIONADO E PARAMETROS PARA IMPORTA��O DO ITEM

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
@param		cLocal - Caminho do arquivo CSV
@return 	lRet
/*/
//------------------------------------------------------------------------------------------

Static Function ExistArq(cLocal)

Local lRet 	:= .T.

IF Alltrim(cLocal) != ""
//Verifica extens�o
	IF !(Lower(RIGHT(cLocal, 3)) $ "csv|txt")
		Help( "TA006Impt", 1, STR0022, , STR0019, 1, 0) // "Aten��o" - "Extens�o do arquivo deve ser .CSV ou .TXT"
		lRet := .F.
	ENDIF
	//Verifica se o arquivo existe
	IF !FILE(cLocal,,.F.)
		Help( "TA006Impt", 1, STR0022, , STR0020, 1, 0) // "Aten��o" - "Informar um caminho valido para abertura do arquivo .CSV ou .TXT!"
		lRet := .F.
	ENDIF
ELSE
	Help( "TA006Impt", 1, STR0022, , STR0019, 1, 0) // "Aten��o" - "Informar um caminho valido para abertura do arquivo .CSV ou .TXT!"
	lRet := .F.
ENDIF

Return lRet        

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TR6VALCHAV(oModG3F,oModG3G)

VALIDA CHAVE DOS CAMPOS NA TABELA G3F

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
@param		cLocal - Caminho do arquivo CSV
@return 	lRet
/*/
//------------------------------------------------------------------------------------------

Static Function TR6VALCHAV(oModel,oG3FMASTER, oG3GDETAIL)

Local lRet 		:= .T.
Local nX			:= 0
Local nOperation	:= oModel:GetOperation()

// VERIFICA SE � INCLUS�O OU ATUALIZA��O
IF nOperation == MODEL_OPERATION_INSERT
	// FUN��O QUE RETORNA F CASO CHAVE J� EXISTA
	lRet := ExistChav('G3F',FwFldGet('G3F_CODCLI')+FwFldGet('G3F_LOJA')+FwFldGet('G3F_TIPO'),1)
ENDIF	

// CHAMA FUN��O PARA VALIDAR SE ITEM � OBRIGATORIO 
IF lRet == .T.
	lRet := TR6VALIDA(oG3FMASTER, oG3GDETAIL)
Else	
	Help( "TURA006", 1, STR0043, , STR0044, 1, 0)	//"Aten��o","S� � permitido um registro de cabe�alho por cliente/loja/tipo. Verifique se n�o h� outro cadastro para este cliente/loja/tipo."
ENDIF

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TR6VALIDA(oG3FMASTER, oG3GDETAIL)

VALIDA SE OBRIGATORIO ITEM 

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
@param		cLocal - Caminho do arquivo CSV
@return 	lRet
/*/
//------------------------------------------------------------------------------------------

Static Function TR6VALIDA(oG3FMASTER, oG3GDETAIL)

Local nOperation	:= 0
Local cValid 		:= ""
Local lRet 		:= .T.


// INFORMA SE ITEM DEVE OU N�O SER VALIDADO
cValid 		:= FwFldGet("G3F_VALIDA")

// VERIFICAR QUAL O TIPO DE OPERA��O ESTA SENDO REALIZADA
nOperation 	:= oG3FMASTER:GetOperation()

// VERIFICA SE � INCLUS�O OU ALTERA��O?
IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 
	// VALIDA SE TEM ITEM?
	IF cValid == "1"
		// SE ITEM N�O FOR VAZIO
		IF !oG3GDETAIL:IsEmpty() .And. oG3GDETAIL:Length( .T. ) > 0
			lRet:= .T.
		// CASO VAZIO ABRE TELA DE HELP
		ELSE
			Help( "TR6VALIDA", 1, STR0022, , STR0040, 1, 0) // "Aten��o" - "Campo Valida��o = Sim. - Obritat�rio ter pelo menos um item cadastrado!"
			lRet:= .F.
		ENDIF	
	ENDIF
ENDIF

Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TR6VLSEG(oG3FMASTER)

VALIDA SE FOI SELECIONADO ALGUM SEGMENTO PARA ESTA ENTIDADE

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
@param		cLocal - Caminho do arquivo CSV
@return 	lRet
/*/
//------------------------------------------------------------------------------------------

Static Function TR6VLSEG(oG3FMASTER)

Local lRet			:= .T.
Local nOperation	:= 0
Local lLazer		:= FwFldGet("G3F_LAZER")
Local lCorpor		:= FwFldGet("G3F_CORP")
Local lEvento		:= FwFldGet("G3F_EVENTO")
Local cCodCli		:= FwFldGet("G3F_CODCLI")
Local cLojCli		:= FwFldGet("G3F_LOJA")
Local cCodEad		:= FwFldGet("G3F_TIPO")
Local lObrigat	:= FwFldGet("G3F_OBRIGA") == "1"
Local cValid 		:= ""
Local aCodCompl	:= {}
Local nSeg			:= 0

// VERIFICAR QUAL O TIPO DE OPERA��O ESTA SENDO REALIZADA
nOperation 	:= oG3FMASTER:GetOperation()

// VERIFICA SE � INCLUS�O OU ALTERA��O?
If nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 
	// VALIDA SE NENHUM SEGMENTO FOI SELECIONADO PARA ESTA ENTIDADE
	If lCorpor == .F. .AND. lEvento == .F. .AND. lLazer == .F.
		Help( "TR6VLSEG", 1, STR0022, , STR0042, 1, 0) // "Aten��o" - "� necess�rio selecionar pelo menos um segmento para esta Entidade."
		lRet:= .F.
	EndIf
EndIf

//Valida se Entidade n�o obrigat�ria est� referenciada no Agrupamento de algum complemento do cliente
If lRet 

	For nSeg := 1 To 3 //Corpo, Evento, Lazer
	
		aCodCompl := Tur18RtCmp(cCodCli,cLojCli,nSeg) //Buscar o complemento do cliente para cada segmento, consideranto complemento compartilhado
		
		If !aCodCompl[2]
			DbSelectarea("G67")
			G67->(DbsetOrder(1))
			G67->(DbSeek(xFilial("G67")+aCodCompl[1]))
			While G67->(!Eof()) .And. G67->(G67_FILIAL+G67_CODIGO) == xFilial("G67")+aCodCompl[1]
				If G67->G67_CODEAD == cCodEad 
					If (nOperation == MODEL_OPERATION_DELETE .Or. ;
					   ((nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE) .And.;
					   (!lObrigat .Or. ((nSeg == 1 .And. !lCorpor) .Or. (nSeg == 2 .And. !lEvento) .Or. (nSeg == 3 .And. !lLazer)))))
					
						Help(,,"TR6VLSEG", ,I18N(STR0050, {aCodCompl[1]}), 1, 0)	// "Esta entidade adicional deve ser obrigat�ria, pois � utilizada no complemento: #1"	
						Return .F.
					EndIf
				EndIf
				G67->(DbSkip())
			EndDo
		EndIf
	
	Next nSeg

EndIf

Return lRet


//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA006MASC()     

PASSA A MASCARA A SER UTILIZADA NO ITEM DO GRID

@sample 	TURA006()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/02/2015
@version  	P12
@return 	cMascara
/*/
//------------------------------------------------------------------------------------------

Function TA006MASC()     
Local cMascara	 := "@!"
Local oModel 	 
Local nOperation 

If IsInCallStack("Tura006")
	oModel 	   := FWModelActive()
	If !Empty(oModel:GetValue('G3FMASTER','G3F_MASCAR'))
		cMascara:= oModel:GetValue('G3FMASTER','G3F_MASCAR')
	EndIf
EndIf
	
Return cMascara

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
@return		aRet[2] � String contendo informa��es sobre o processamento
@return		aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Jacomo Lisa
@since   	22/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}
SETROTINTEG("TURA006")
aRet:= TURI006( cXml, nTypeTrans, cTypeMessage )

Return aRet

Function T006GRAVA( oModel )
Local lRet := .T.
	lRet := FWFormCommit( oModel  ,/*[bBefore]*/, /*[bAfter]*/, /*[ bAfterSTTS ]*/, /*<bInTTS>*/, /*[bABeforeTTS]*/, {|oModel| TA006EAI(oModel)}/*<bIntegEAI >*/ )
	If oModel:HasErrorMessage()
		lRet := .F.
	Endif
Return lRet

Function TA006EAI( oModel )
Local cXml := ""
Local nRet := 1
If FwHasEAI("TURA006",.T. /*lVerifySend*/) .and. !FwIsInCallStack('TURI006')
	 
 	If !Ti006Send()
 	 	nRet := 0
	Endif
Endif
Return nRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA006FldsExt

Fun��o que ir� retornar os campos customizados da tabela G3G

@sample 	TA006FldsExt(oModel)
@param		oModel - Objeto do modelo de dados
@return  	aRet - Array com a estrutura de metadado dos campos da classe
             [n] Array com os campos
             [n][01] ExpC: Titulo
             [n][02] ExpC: Tooltip
             [n][03] ExpC: IdField
             [n][04] ExpC: Tipo
             [n][05] ExpN: Tamanho
             [n][06] ExpN: Decimal
             [n][07] ExpB: Valid
             [n][08] ExpB: When
             [n][09] ExpA: Lista de valores ( Combo )
             [n][10] ExpL: Obrigat�rio
             [n][11] ExpB: Inicializador padr�o
             [n][12] ExpL: Campo chave
             [n][13] ExpL: Campo atualizavel
             [n][14] ExpL: Campo virtual   
             [n][15] ExpC: Valid do usuario em formato texto e sem alteracao, usado para se criar o aheader de compatibilida
@author  	Thiago Tavares
@since   	10/02/2017
@version  	P12.1.14
/*/
//------------------------------------------------------------------------------------------
Static Function TA006FldsExt(oModel)

Local nX      := 0
Local aRet    := {}
Local aCpsG3G := oModel:GetStruct():GetFields()		// Armazena os campos da tabela G3G
Local aCpsPad := {'G3G_FILIAL', ;
                  'G3G_CLIENT', ;
                  'G3G_LOJA'  , ;
                  'G3G_TIPO'  , ;
                  'G3G_ITEM'  , ;
                  'G3G_DESCR' , ;
                  'G3G_MSBLQL', ;
                  'G3G_EMAIL'}
                  
For nX := 1 To Len(aCpsG3G)
	If aScan(aCpsPad, {|x| x == aCpsG3G[nX][3]}) == 0
		aAdd(aRet, aCpsG3G[nX])
	EndIf
Next nX 
                    
Return aRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA006VldX9

Fun��o que valida o relacionamento da tabela G3G antes de excluir o item na importa��o

@sample 	TA006VldX9(oModel)
@param		oModel - Objeto do modelo de dados
@return  	lRet - L�gico, indicando se o item j� foi utilizado em algum dos relacionamentos (G4A, G4B e G80)
@author  	Thiago Tavares
@since   	24/02/2017
@version  	P12.1.14
/*/
//------------------------------------------------------------------------------------------
Static Function TA006VldX9(oModel)

Local aArea   := GetArea()
Local cAlias  := GetNextAlias()
Local cClient := oModel:GetValue("G3G_CLIENT")
Local cLoja   := oModel:GetValue("G3G_LOJA")
Local cTpEnt  := oModel:GetValue("G3G_TIPO")
Local cIdItem := oModel:GetValue("G3G_ITEM")

Local lRet    := .F.

BeginSql Alias cAlias
	SELECT G4A_ITEM, G4B_ITEM, G80_ITENT
	FROM %Table:G3G% G3G
	LEFT JOIN %Table:G4A%  G4A ON G4A_FILIAL LIKE RTRIM(G3G.G3G_FILIAL)+'%' AND 
	                              G4A_TPENT  = G3G_TIPO AND
	                              G4A_ITEM   = G3G_ITEM AND 
	                              G4A.%NotDel%
	LEFT JOIN %Table:G4B%  G4B ON G4B_FILIAL LIKE RTRIM(G3G.G3G_FILIAL)+'%' AND 
	                              G4B_TPENT  = G3G_TIPO AND 
	                              G4B_ITEM   = G3G_ITEM AND 
	                              G4B.%NotDel%
	LEFT JOIN %Table:G80%  G80 ON G80_FILIAL LIKE RTRIM(G3G.G3G_FILIAL)+'%' AND 
	                              G80_TPENT  = G3G_TIPO AND 
	                              G80_ITENT  = G3G_ITEM AND 
	                              G80.%NotDel%
	WHERE G3G_FILIAL = %xFilial:G3G% AND
	      G3G_CLIENT = %Exp:cClient% AND
	      G3G_LOJA   = %Exp:cLoja% AND
	      G3G_TIPO   = %Exp:cTpEnt% AND
	      G3G_ITEM   = %Exp:cIdItem% AND
	     (G4A_ITEM  IS NOT NULL OR 
	      G4B_ITEM  IS NOT NULL OR 
	      G80_ITENT IS NOT NULL) AND 
	      G3G.%NotDel%
EndSQL

If (cAlias)->(!EOF())
	lRet := .T.
EndIf
(cAlias)->(DbCloseArea())

RestArea(aArea)

Return lRet 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA006VldDados

Fun��o que valida a estrutura do arquivo antes de importa-lo

@sample 	TA006VldDados(oModel)
@param		aDados - Array, com os dados extra�dos do arquivo
@return  	lRet - L�gico, indicando se a estrutura do arquivo � valida ou n�o
@author  	Thiago Tavares
@since   	24/02/2017
@version  	P12.1.14
/*/
//------------------------------------------------------------------------------------------
Static Function TA006VldDados(aDados)

Local nX   := 0
Local lRet := .T.

For nX := 1 To Len(aDados)
	// VALIDA TAM DOS CAMPOS A SEREM IMPORTADOS
	IF !(Len(aDados[nX]) >= 2 .And. ;
	     Len(aDados[nX][1]) >= 1 .AND. Len(aDados[nX][1]) <= TamSX3("G3G_ITEM" )[1] .AND. ;
	     Len(aDados[nX][2]) >= 1 .AND. Len(aDados[nX][2]) <= TamSX3("G3G_DESCR")[1])
		lRet := .F.	
		Exit	
	EndIf
Next nX

Return lRet