#Include "PROTHEUS.CH"
#Include "FWMVCDEF.CH"
#Include "TURA007.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} TURA007
Cadastro de Agentes de Viagem.
@author Elias dos Santos Silva
@since 12/03/2015
@version 12
@return lRet 
/*/
//-------------------------------------------------------------------

Function TURA007()
Local oBrowse  := Nil
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G3H')
oBrowse:SetDescription(STR0001) // Cadastro de Agentes de Viagem
oBrowse:Activate()

Return 

//-------------------------------------------------------------------
/*/{Protheus.doc} MODELDEF()
Cadastro de Agentes de Viagem.
@author Elias dos Santos Silva
@since 12/03/2015
@version 12
@return oModel 
/*/
//-------------------------------------------------------------------

Static Function ModelDef()

Local oStructG3H
Local oStructG3I
Local oStructG3L

//Criando estruturas
oStructG3H := FWFormStruct(1,"G3H")
oStructG3I := FWFormStruct(1,"G3I")
oStructG3L := FWFormStruct(1,"G3L")
oStructTPC := FWFormStruct(1,"G3H")
oStructTPE := FWFormStruct(1,"G3H")
oStructTPL := FWFormStruct(1,"G3H")
oModel := MPFormModel():New('TURA007',,{ |oModel| TURA007POS( oModel ) })

// Adicionando Fields e Grids
oModel:AddFields('G3HMASTER',,oStructG3H)
oModel:AddGrid('G3IDETAIL'  ,'G3HMASTER',oStructG3I,,{| oModel | VldPstSeg(oModel) }) //Posto de Atendimento
oModel:AddGrid('G3ILAZER'   ,'G3HMASTER',oStructG3I,,{| oModel | VldPstSeg(oModel) }) //Posto de Atendimento
oModel:AddGrid('G3IEVENTO'  ,'G3HMASTER',oStructG3I,,{| oModel | VldPstSeg(oModel) }) //Posto de Atendimento
oModel:AddGrid('G3LDETAIL'  ,'G3HMASTER',oStructG3L,,{| oModel | TUR37VldUn(oModel) }) //Unidade de Negocio
oModel:AddGrid('G3LEVENTO'  ,'G3HMASTER',oStructG3L,,{| oModel | TUR37VldUn(oModel) }) //Unidade de Negocio
oModel:AddGrid('G3LLAZER'   ,'G3HMASTER',oStructG3L,,{| oModel | TUR37VldUn(oModel) }) //Unidade de Negocio

// Relacionando as estruturas
oModel:SetRelation( 'G3IDETAIL', { { 'G3I_FILIAL', 'xFilial( "G3I" )' }, { 'G3I_CODAGE' , 'G3H_CODAGE'  }, {'G3I_SEGMEN','"1"'} } , G3I->( IndexKey( 1 ) ) )  
oModel:SetRelation( 'G3LDETAIL', { { 'G3L_FILIAL', 'xFilial( "G3L" )' }, { 'G3L_CODAGE' , 'G3H_CODAGE'  }, {'G3L_SEGMEN','"1"'} } , G3L->( IndexKey( 1 ) ) ) 
oModel:SetRelation( 'G3IEVENTO', { { 'G3I_FILIAL', 'xFilial( "G3I" )' }, { 'G3I_CODAGE' , 'G3H_CODAGE'  }, {'G3I_SEGMEN','"2"'} } , G3I->( IndexKey( 1 ) ) )
oModel:SetRelation( 'G3ILAZER' , { { 'G3I_FILIAL', 'xFilial( "G3I" )' }, { 'G3I_CODAGE' , 'G3H_CODAGE'  }, {'G3I_SEGMEN','"3"'} } , G3I->( IndexKey( 1 ) ) )
oModel:SetRelation( 'G3LEVENTO', { { 'G3L_FILIAL', 'xFilial( "G3L" )' }, { 'G3L_CODAGE' , 'G3H_CODAGE'  }, {'G3L_SEGMEN','"2"'} } , G3L->( IndexKey( 1 ) ) )  
oModel:SetRelation( 'G3LLAZER' , { { 'G3L_FILIAL', 'xFilial( "G3L" )' }, { 'G3L_CODAGE' , 'G3H_CODAGE'  }, {'G3L_SEGMEN','"3"'} } , G3L->( IndexKey( 1 ) ) )  

// Os Grids n�o s�o obrigat�rios, somente verifcando o tipo de atendimento na fun��o TURA007POS
oModel:GetModel('G3IDETAIL'):SetOptional(.T.)
oModel:GetModel('G3LDETAIL'):SetOptional(.T.)
oModel:GetModel('G3IEVENTO'):SetOptional(.T.)
oModel:GetModel('G3ILAZER' ):SetOptional(.T.)
oModel:GetModel('G3LEVENTO'):SetOptional(.T.)
oModel:GetModel('G3ILAZER' ):SetOptional(.T.)
oModel:GetModel('G3LLAZER' ):SetOptional(.T.)

// Altera campos para n�o obrigat�rio
oStructG3L:SetProperty( 'G3L_CODAGE' ,	MODEL_FIELD_OBRIGAT,.F.)
oStructG3I:SetProperty( 'G3I_CODAGE' ,	MODEL_FIELD_OBRIGAT,.F.)
oStructG3L:SetProperty( 'G3L_SEGMEN' ,	MODEL_FIELD_OBRIGAT,.F.)
oStructG3I:SetProperty( 'G3I_SEGMEN' ,	MODEL_FIELD_OBRIGAT,.F.)

// Posto de Atendimento n�o pode se repetir no Grid
oModel:GetModel( 'G3IDETAIL' ):SetUniqueLine( { 'G3I_FILPST','G3I_POSTO' } )
oModel:GetModel( 'G3IEVENTO' ):SetUniqueLine( { 'G3I_FILPST','G3I_POSTO' } )
oModel:GetModel( 'G3ILAZER'  ):SetUniqueLine( { 'G3I_FILPST','G3I_POSTO' } )

// Unidade de Atendimento n�o pode se repetir no Grid
oModel:GetModel( 'G3LDETAIL' ):SetUniqueLine( { 'G3L_UNIDAD' } )
oModel:GetModel( 'G3LEVENTO' ):SetUniqueLine( { 'G3L_UNIDAD' } )
oModel:GetModel( 'G3LLAZER' ):SetUniqueLine( { 'G3L_UNIDAD' } )

oModel:SetDescription(STR0001) // Cadastro de Agentes de Viagem
oModel:GetModel('G3HMASTER'):SetDescription(STR0002) // Agentes de Viagem
oModel:GetModel('G3IDETAIL'):SetDescription(STR0003) // Posto de Atendimento
oModel:GetModel('G3LDETAIL'):SetDescription(STR0004) // Unidade de Negocio

// Inclui Valida��o no campo
oStructG3L:SetProperty( 'G3L_PADRAO' ,	MODEL_FIELD_VALID,{|oModel| At07VLUN(oModel)})

Return oModel


//-------------------------------------------------------------------
/*/{Protheus.doc} VIEWDEF()
Cadastro de Agentes de Viagem.
@author Elias dos Santos Silva
@since 12/03/2015
@version 12
@return oView 
/*/
//-------------------------------------------------------------------

Static Function ViewDef()

Local oStructG3H	:= Nil
Local oStructG3I	:= Nil
Local oStructG3L	:= Nil
Local oStructTpC	:= Nil
Local oStructTpE	:= Nil
Local oStructTpL	:= Nil
Local oModel		:= Nil
Local oView 		:= Nil
Local cAgente		:= ""

// Criando estruturas
oStructG3H := FwFormStruct(2,"G3H")
oStructG3I := FwFormStruct(2,"G3I")
oStructG3L := FwFormStruct(2,"G3L")

// Selecionando Campos
oStructTpc := FWFormStruct(2,"G3H", {|cCampo|AllTrim(cCampo) $ "G3H_TPATCO"})
oStructTpE := FWFormStruct(2,"G3H", {|cCampo|AllTrim(cCampo) $ "G3H_TPATEV"})
oStructTpL := FWFormStruct(2,"G3H", {|cCampo|AllTrim(cCampo) $ "G3H_TPATLZ"})

oModel := FWLoadModel("TURA007")
oView := FWFormView():New()

oView:SetModel(oModel)

// Remove campos da estrutura
oStructG3H:RemoveField("G3H_TPATCO")
oStructG3H:RemoveField("G3H_TPATEV")
oStructG3H:RemoveField("G3H_TPATLZ")
oStructG3I:RemoveField("G3I_CODAGE")
oStructG3I:RemoveField("G3I_SEGMEN")
oStructG3L:RemoveField("G3L_CODAGE")
oStructG3L:RemoveField("G3L_SEGMEN")

//Adicionando as estruturas na View
oView:AddField("VIEW_G3H",oStructG3H,"G3HMASTER")
oView:AddField("VIEW_TPC",oStructTpC,"G3HMASTER")
oView:AddField("VIEW_TPE",oStructTpE,"G3HMASTER")
oView:AddField("VIEW_TPL",oStructTpL,"G3HMASTER")

oView:AddGrid("VIEW_G3I",oStructG3I,"G3IDETAIL")
oView:AddGrid("VIEW_G3L",oStructG3L,"G3LDETAIL")

oView:AddGrid("VIEW_G3IA",oStructG3I,'G3IEVENTO')
oView:AddGrid("VIEW_G3IB",oStructG3I,'G3ILAZER')
oView:AddGrid("VIEW_G3LA",oStructG3L,'G3LEVENTO')
oView:AddGrid("VIEW_G3LB",oStructG3L,'G3LLAZER')

// Box superior e inferior
oView:CreateHorizontalBox("SUPERIOR",20)
oView:CreateHorizontalBox("INFERIOR",80)

// Pastas Corporativo, Evento e Lazer
oView:CreateFolder("PASTAS","INFERIOR")
oView:AddSheet("PASTAS","ABACORP", STR0009 ) // Corporativo
oView:AddSheet("PASTAS","ABAEVEN", STR0010 ) // Eventos
oView:AddSheet("PASTAS","ABALAZE", STR0011 ) // Lazer 

// Criando as box dentro do Folder Pastas
oView:CreateVerticalBox("VBOX01",50,,, 'PASTAS','ABACORP')
oView:CreateHorizontalBox("VBOX01SUP",20,"VBOX01",,"PASTAS","ABACORP")
oView:CreateHorizontalBox("VBOX01INF",80,"VBOX01",,"PASTAS","ABACORP")
oView:CreateVerticalBox("VBOX02",50,,, 'PASTAS','ABACORP')
oView:CreateVerticalBox("VBOX03",50,,, 'PASTAS','ABAEVEN')
oView:CreateHorizontalBox("VBOX03SUP",20,"VBOX03",,"PASTAS","ABAEVEN")
oView:CreateHorizontalBox("VBOX03INF",80,"VBOX03",,"PASTAS","ABAEVEN")
oView:CreateVerticalBox("VBOX04",50,,, 'PASTAS','ABAEVEN')
oView:CreateVerticalBox("VBOX05",50,,, 'PASTAS','ABALAZE')
oView:CreateHorizontalBox("VBOX05SUP",20,"VBOX05",,"PASTAS","ABALAZE")
oView:CreateHorizontalBox("VBOX05INF",80,"VBOX05",,"PASTAS","ABALAZE")
oView:CreateVerticalBox("VBOX06",50,,, 'PASTAS','ABALAZE')

// Relacionado as Views com as Box
oView:SetOwnerView("VIEW_G3H","SUPERIOR")
oView:SetOwnerView("VIEW_TPC","VBOX01SUP")
oView:SetOwnerView("VIEW_G3I","VBOX01INF")
oView:SetOwnerView("VIEW_G3L","VBOX02")	
oView:SetOwnerView("VIEW_TPE","VBOX03SUP")
oView:SetOwnerView("VIEW_G3IA","VBOX03INF")
oView:SetOwnerView("VIEW_G3LA","VBOX04")
oView:SetOwnerView("VIEW_TPL","VBOX05SUP")
oView:SetOwnerView("VIEW_G3IB","VBOX05INF")
oView:SetOwnerView("VIEW_G3LB","VBOX06")

oView:EnableTitleView("VIEW_G3I",STR0003)  // Posto de Atendimento
oView:EnableTitleView("VIEW_G3L",STR0004)  // Unidade de Negocio
oView:EnableTitleView("VIEW_G3IA",STR0003) // Posto de Atendimento
oView:EnableTitleView("VIEW_G3LA",STR0004) // Unidade de Negocio
oView:EnableTitleView("VIEW_G3IB",STR0003) // Posto de Atendimento
oView:EnableTitleView("VIEW_G3LB",STR0004) // Unidade de Negocio

Return oView


//-------------------------------------------------------------------
/*/{Protheus.doc} MENUDEF()
Cadastro de Agentes de Viagem.
@author Elias dos Santos Silva
@since 12/03/2015
@version 12
@return aRotina 
/*/
//-------------------------------------------------------------------

Static Function MenuDef()
Local aRotina := {}

ADD OPTION aRotina Title STR0008	Action 'VIEWDEF.TURA007' OPERATION 2 ACCESS 0 //Visualizar
ADD OPTION aRotina Title STR0005	Action 'VIEWDEF.TURA007' OPERATION 3 ACCESS 0 //Incluir
ADD OPTION aRotina Title STR0006	Action 'VIEWDEF.TURA007' OPERATION 4 ACCESS 0 //Alterar
ADD OPTION aRotina Title STR0007	Action 'VIEWDEF.TURA007' OPERATION 5 ACCESS 0 //Excluir


Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} VldPstSeg()
Valida��o ao adicionar postos no grid.
@author Elias dos Santos Silva
@since 12/03/2015
@version 12 
/*/
//-------------------------------------------------------------------

Function VldPstSeg(oModel)
Local lRet := .T.
Local oView			:= FWViewActive()
Local cFolder  		:= oModel:cID
Local aArea			:= GetArea()

// Valida��o do Tipo de Atendimento
If cFolder == 'G3IDETAIL' .AND. FWFLDGET("G3H_TPATCO") != "2" 
	lRet := .F.
	Help( "VldPstSeg", 1, STR0012, , STR0013 , 1, 0) //"Aten��o","Somente � permitido adicionar postos se o tipo de atendimento for igual a listados." 	

Elseif cFolder == 'G3IEVENTO' .AND. FWFLDGET("G3H_TPATEV") != "2" 
	lRet := .F.
	Help( "VldPstSeg", 1, STR0012, , STR0015, 1, 0) //"Aten��o","Somente � permitido adicionar postos se o tipo de atendimento for igual a listados."
				
Elseif cFolder == 'G3ILAZER' .AND. FWFLDGET("G3H_TPATLZ") != "2" 
	lRet := .F.
	Help( "VldPstSeg", 1, STR0012, , STR0017, 1, 0) //"Aten��o","Somente � permitido adicionar postos se o tipo de atendimento for igual a listados."
		
Endif

RestArea(aArea)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TURA007POS(oModel)
Cadastro de Agentes de Viagem.
@author Elias dos Santos Silva
@since 12/03/2015
@Param oModel
@version 12
@return lRet 
/*/
//-------------------------------------------------------------------

Static Function TURA007POS(oModel)

Local aArea		:= GetArea()
Local lRet			:= .T.
Local nX			:= 0
Local nPadrao		:= 0
Local nTam			:= 0
Local nOperation	:= oModel:GetOperation()
Local oModelICo	:= oModel:GetModel('G3IDETAIL')
Local oModelIEv	:= oModel:GetModel('G3IEVENTO')
Local oModelILz	:= oModel:GetModel('G3ILAZER')
Local oModelLCo	:= oModel:GetModel('G3LDETAIL')
Local oModelLEv	:= oModel:GetModel('G3LEVENTO')
Local oModelLLz	:= oModel:GetModel('G3LLAZER')
Local cTipo		:= oModel:GetValue("G3HMASTER","G3H_TIPO")
Local cTpatco	:= oModel:GetValue("G3HMASTER","G3H_TPATCO")
Local cTpatev	:= oModel:GetValue("G3HMASTER","G3H_TPATEV")
Local cTpatlz	:= oModel:GetValue("G3HMASTER","G3H_TPATLZ")
Local cCodVen	:= oModel:GetValue("G3HMASTER","G3H_CODVEN")

// Valida se o Agente � Consultor ou Gerente e se for exige pelo menos um relacionamento com posto de atendimento.
If cTipo $ "1/3" 
	If ((oModelICo:IsEmpty() .And. cTpatco == '2') .Or. cTpatco == '3') .Or. ;
	   ((oModelIEv:IsEmpty() .And. cTpatev == '2') .Or. cTpatev == '3') .Or. ;
	   ((oModelILz:IsEmpty() .And. cTpatlz == '2') .Or. cTpatlz == '3')
		lRet := .f.
		Help( "TURA007POS", 1, STR0012, , STR0019, 1, 0) //"Aten��o","� obrigat�rio o relacionamento do Agente de Viagem com pelo menos um Posto de Atendimento quando o seu tipo � 'Consultor' ou 'Gerente'"				
	Endif
Endif

If !(oModelLCo:IsEmpty()) 
	For nX := 1 To oModelLCo:Length()
		oModelLCo:GoLine(nX)
		If !oModelLCo:IsDeleted() .and. oModelLCo:GetValue("G3L_PADRAO") == .T.
			nPadrao:= (nPadrao + 1)		
		Endif
	Next nX	
	If nPadrao <> 1 .AND. oModelLCo:Length(.T.)> 1
		lRet := .f.
		Help( "TURA007POS", 1, STR0012, , If(nPadrao < 1, STR0021,STR0022 ), 1, 0) //STR0012 -- "Aten��o", STR0021 -- "� obrigat�rio ter pelo menos uma Unidade de Neg�cio selecionada como Padr�o. " STR0022 -- "N�o � permitido possuir mais que uma Unidade Padr�o"
	Endif
Endif

If !(oModelLEv:IsEmpty()) 
	nX := 0
	nPadrao := 0
	For nX := 1 To oModelLEv:Length()
		oModelLEv:GoLine(nX)
		If !oModelLEv:IsDeleted() .and. oModelLEv:GetValue("G3L_PADRAO") == .T.
			nPadrao:= (nPadrao + 1)		
		EndIf
	Next nX	
	If nPadrao <> 1 .AND. oModelLEv:Length(.T.)> 1
		lRet := .f.
		Help( "TURA007POS", 1, STR0012, , If(nPadrao < 1, STR0021,STR0022 ), 1, 0) //STR0012 -- "Aten��o", STR0021 -- "� obrigat�rio ter pelo menos uma Unidade de Neg�cio selecionada como Padr�o. " STR0022 -- "N�o � permitido possuir mais que uma Unidade Padr�o"
	Endif
Endif
	
If  !(oModelLLz:IsEmpty())  
	nX := 0
	nPadrao := 0
	For nX := 1 To oModelLLz:Length()
		oModelLLz:GoLine(nX)
		If !oModelLLz:IsDeleted() .and. oModelLLz:GetValue("G3L_PADRAO") == .T.
			nPadrao:= (nPadrao + 1)		
		EndIf
	Next nX	
	If nPadrao <> 1 .AND. oModelLLz:Length(.T.)> 1
		lRet := .f.
		Help( "TURA007POS", 1, STR0012, , If(nPadrao < 1, STR0021,STR0022 ), 1, 0) //STR0012 -- "Aten��o", STR0021 -- "� obrigat�rio ter pelo menos uma Unidade de Neg�cio selecionada como Padr�o. " STR0022 -- "N�o � permitido possuir mais que uma Unidade Padr�o"
	Endif
Endif

// valida��o referente defeito PCDEF-104924
If lRet .And. (nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE)
	DbSelectArea('G3H')
	G3H->(DbSetOrder(2))		// G3H_FILIAL+G3H_CODVEN
	If G3H->(DbSeek(xFilial('G3H') + cCodVen))
		If ((G3H->(FieldPos('G3H_MSBLQL')) > 0 .And. G3H->G3H_MSBLQL == '2') .Or. G3H->(FieldPos('G3H_MSBLQL')) == 0) .And. G3H->G3H_CODAGE != oModel:GetValue('G3HMASTER', 'G3H_CODAGE')
			lRet := .F.
			Help( "TURA007POS", 1, STR0012, , STR0024, 1, 0) //STR0012 -- "Aten��o",		"O Vendedor informado j� est� relacionado a outro Agente. Favor informar o c�digo de outro Vendedor."
		EndIf 
	EndIf
	G3H->(DbCloseArea())			
EndIf

RestArea(aArea)

Return lRet

Static Function TUR37VldUn(oModelG3L)

Local lRet			:= .T.
Local nX			:= 0
Local nPadrao		:= 0

If !(oModelG3L:IsEmpty()) 
	For nX := 1 To oModelG3L:Length()
		oModelG3L:GoLine(nX)
		If !oModelG3L:IsDeleted() .and. oModelG3L:GetValue("G3L_PADRAO") == .T.
			nPadrao:= (nPadrao + 1)		
		Endif
	Next nX	
	If nPadrao > 1 .AND. oModelG3L:Length()> 1
		lRet := .f.
		Help( "TURA007POS", 1, STR0012, , STR0022, 1, 0) //"Aten��o","� obrigat�rio ter pelo menos uma Unidade de Neg�cio selecionada como Padr�o. "
	Endif
Endif

Return lRet

//============================================================================================================
/*/{Protheus.doc} At07VLUN(oMdl,oView)

VALIDA��O PARA DEIXAR APENAS UMA UNIDADE DE NEG�CIO SELECIONADA COMO PADR�O

@sample 	At07VLUN()
@return  	aRotina   
@PARAM     oMdl               
@author  	Fanny Mieko Suzuki
@since   	18/06/2015
@version  	P12
@return 	lRet
/*/
//============================================================================================================

Static Function At07VLUN(oMdl)

Local nx			:= 0
Local nOperation	:= oMdl:GetOperation() // VERIFICAR QUAL O TIPO DE OPERA��O ESTA SENDO REALIZADA
Local oView			:= FWViewActive()
Local lPadrao		:= oMdl:GetValue("G3L_PADRAO")
Local cUnidad		:= oMdl:GetValue("G3L_UNIDAD")
Local aSaveLines	:= FWSaveRows() 
Local aArea			:= GetArea()

IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE .AND. lPadrao

	FOR nX := 1 To oMdl:Length()
		oMdl:GoLine(nX)
		IF !oMdl:IsDeleted() .AND. oMdl:GetValue("G3L_UNIDAD") <> cUnidad .AND. (oMdl:GetValue("G3L_PADRAO"))
			oMdl:LoadValue("G3L_PADRAO",.F.)
		ENDIF
	NEXT nX	
	oMdl:GoLine(1)
ENDIF		

FWRestRows(aSaveLines) 

If !IsBlind()
	oView:Refresh()
Endif	
RestArea(aArea)

RETURN .T.

//============================================================================================================
/*/{Protheus.doc} TA007G3IWHEN()

VALIDA��O PARA HABILITAR OS CAMPOS DA G3IDETAIL

@sample 	TA007G3IWHEN()
@return  	lRet, vari�vel l�gica   
@PARAM                    
@author  	Thiago Tavares
@since   	27/11/2015
@version  	P12.1.7
/*/
//============================================================================================================
Function TA007G3IWHEN()

Local lRet   := .F.
Local oModel := FWModelActive()

If oModel:GetValue('G3HMASTER', 'G3H_TPATCO') == '2' .Or. ;
   oModel:GetValue('G3HMASTER', "G3H_TPATEV") == '2' .Or. ;
   oModel:GetValue('G3HMASTER', 'G3H_TPATLZ') == '2' 
	lRet := .T.
EndIf

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}
SETROTINTEG("TURA007")
aRet:= TURI007( cXml, nTypeTrans, cTypeMessage )

Return aRet