#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'FWEditPanel.CH'
#INCLUDE 'TURA009.CH'

/*/{Protheus.doc} TURA009
Rotina de manuten��o do cadastro de complemento de condi��es de pagamento.
@author Rogerio Melonio
@since 04/01/2016
@version 1.0
/*/
Function TURA009()
Local oBrowse		:= NIL  
Static aCamposG66	:= {}

dbSelectArea('G66')

oBrowse := FWMBrowse():New()
oBrowse:SetAlias( 'G66' )
oBrowse:SetDescription( STR0001 ) // Complemento de Condi��o de Pagamento
oBrowse:Activate()

Return
	
//-------------------------------------------------------------------
/*{Protheus.doc} MenuDef()
Definicao do Menu
@author Rogerio Melonio
@since 04/01/2016
@version 1.0
@return aRotina (vetor com botoes da EnchoiceBar)
*/
//-------------------------------------------------------------------
Static Function MenuDef()  
Local aRotina 	:= {} //Array utilizado para controlar opcao selecionada

ADD OPTION aRotina TITLE STR0002     	ACTION "PesqBrw"				OPERATION 1 ACCESS 0	// "Pesquisar"
ADD OPTION aRotina TITLE STR0003		ACTION "VIEWDEF.TURA009" 		OPERATION 2 ACCESS 0	// "Visualizar"
ADD OPTION aRotina TITLE STR0004		ACTION "VIEWDEF.TURA009"		OPERATION 3 ACCESS 0	// "Incluir"
ADD OPTION aRotina TITLE STR0005   		ACTION "VIEWDEF.TURA009"  		OPERATION 4 ACCESS 0	// "Alterar"
ADD OPTION aRotina TITLE STR0006      	ACTION "VIEWDEF.TURA009"		OPERATION 5 ACCESS 0	// "Excluir"
ADD OPTION aRotina TITLE STR0007		ACTION "VIEWDEF.TURA009"		OPERATION 8 ACCESS 0	// "Imprimir"

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()
Definicao do Modelo
@author Rogerio Melonio
@since 04/01/2016
@version 1.0
@return oModel
/*/
//-------------------------------------------------------------------
Static Function ModelDef()  
Local oModel
Local oStruG66 := FWFormStruct(1,'G66',/*bAvalCampo*/,/*lViewUsado*/)
Local nCampo   	:= 0
Local bWhen		:= { || .T. }
Local nDia		:= 0
Local cCampoDia	:= ""
Local bLoad := {|oFieldModel| Tur09Load(oFieldModel)}

aCamposG66 := {}

For nDia := 1 To 31
	cCampoDia := 'G66_DIA' + StrZero(nDia,2)
	//--> [01] - campo, [02] - tipo, [03] - tamanho, [04] mascara, [05] - descri��o, [06] - titulo, [07] - combo, [08] - consulta padr�o, [09] - bWhen, [10] - bValid, [11] bInit
	aAdd( aCamposG66,{cCampoDia	,'L',01,'',StrZero(nDia,2)	,StrZero(nDia,2),NIL,NIL ,bWhen,NIL,NIL } )
Next

cCampoDia := 'G66_DIAUD'
aAdd( aCamposG66,{cCampoDia	,'L',01,'','UD'	, 'UD',NIL,NIL ,bWhen,NIL,NIL	} )

//--> [01] - campo, [02] - tipo, [03] - tamanho, [04] mascara, [05] - descri��o, [06] - titulo, [07] - combo, [08] - consulta padr�o, [09] - bWhen, [10] - bValid, [11] bInit
aAdd( aCamposG66,{'G66_DIASEG'	,'L',01,'',STR0008,STR0008,NIL,NIL ,bWhen,NIL,NIL } ) // Segunda-Feira
aAdd( aCamposG66,{'G66_DIATER'	,'L',01,'',STR0009,STR0009,NIL,NIL ,bWhen,NIL,NIL } ) // Ter�a-Feira
aAdd( aCamposG66,{'G66_DIAQUA'	,'L',01,'',STR0010,STR0010,NIL,NIL ,bWhen,NIL,NIL } ) // Quarta-Feira 
aAdd( aCamposG66,{'G66_DIAQUI'	,'L',01,'',STR0011,STR0011,NIL,NIL ,bWhen,NIL,NIL } ) // Quinta-Feira
aAdd( aCamposG66,{'G66_DIASEX'	,'L',01,'',STR0012,STR0012,NIL,NIL ,bWhen,NIL,NIL } ) // Sexta-Feira
aAdd( aCamposG66,{'G66_DIASAB'	,'L',01,'',STR0013,STR0013,NIL,NIL ,bWhen,NIL,NIL } ) // S�bado 
aAdd( aCamposG66,{'G66_DIADOM'	,'L',01,'',STR0014,STR0014,NIL,NIL ,bWhen,NIL,NIL } ) // Domingo 

For nCampo := 1 To Len(aCamposG66)
	cCampo := aCamposG66[nCampo][01]
	//-- Adiciona campos header do filtro de busca de fornecedor 
	oStruG66:AddField(aCamposG66[nCampo][05],;	// 	[01]  C   Titulo do campo
				 	aCamposG66[nCampo][06]	,;	// 	[02]  C   ToolTip do campo
				 	cCampo					,;	// 	[03]  C   Id do Field
				 	aCamposG66[nCampo][02]	,;	// 	[04]  C   Tipo do campo
				 	aCamposG66[nCampo][03]	,;	// 	[05]  N   Tamanho do campo
				 	0						,;	// 	[06]  N   Decimal do campo
				 	aCamposG66[nCampo][10]	,;	// 	[07]  B   Code-block de valida��o do campo
				 	aCamposG66[nCampo][09]	,;	// 	[08]  B   Code-block de valida��o When do campo
				 	aCamposG66[nCampo][07]	,;	//	[09]  A   Lista de valores permitido do campo
				 	.F.						,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 	aCamposG66[nCampo][11]	,;	//	[11]  B   Code-block de inicializacao do campo
				 	.F.						,;	//	[12]  L   Indica se trata-se de um campo chave
				 	.T.						,;	//	[13]  L   Indica se o campo pode receber valor em uma opera��o de update.
				 	.T.						)	// 	[14]  L   Indica se o campo � virtual
	oStruG66:SetProperty(cCampo,MODEL_FIELD_WHEN,{ ||.T. } )
	oStruG66:SetProperty(cCampo,MODEL_FIELD_NOUPD, .F. )
Next

oModel := MPFormModel():New('TURA009',/*bPreValidacao*/,/*bPosValidacao*/,{|oModel|TUR09Grv(oModel)}/*bCommit*/,/*bCancel*/)
oModel:AddFields('G66MASTER',/*cOwner*/,oStruG66,/*bPreValidacao*/,/*bPosValidacao*/, bLoad /*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription( STR0001 )

oModel:SetActivate({|oModel| TUR09Act(oModel)})

Return(oModel)

//--------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()
Definicao da View
@author Rogerio Melonio
@since 04/01/2016
@version 1.0
@return oView
/*/
//--------------------------------------------------------------------
Static Function ViewDef()  
Local oView		:= Nil
Local oModel   	:= FWLoadModel('TURA009')
Local oStruG66 	:= FWFormStruct(2,'G66')
Local nCampo	:= 0
Local cCampo 	:= ""
Local cOrdem 	:= ""

oView := FWFormView():New()
oView:SetModel(oModel)

For nCampo := 1 To Len(aCamposG66)
	//--> [01] - campo, [02] - tipo, [03] - tamanho, [04] mascara, [05] - descri��o, [06] - titulo, [07] - combo, [08] - consulta padr�o, [09] - bWhen, [10] - bValid, [11] bInit
	cCampo := aCamposG66[nCampo][01]
	cOrdem := StrZero(nCampo,2)
	//-- Adiciona campos header do filtro de busca de fornecedor 
	oStruG66:AddField(cCampo						,;	// [01]  C   Nome do Campo
					cOrdem						,;	// [02]  C   Ordem
					aCamposG66[nCampo][05] 		,;	// [03]  C   Titulo do campo
					aCamposG66[nCampo][06] 		,;	// [04]  C   Descricao do campo
					{}							,;	// [05]  A   Array com Help
					aCamposG66[nCampo][02]		,;	// [06]  C   Tipo do campo
					aCamposG66[nCampo][04]		,;	// [07]  C   Picture
					NIL							,;	// [08]  B   Bloco de Picture Var
					aCamposG66[nCampo][08]		,;	// [09]  C   Consulta F3
					.T.							,;	// [10]  L   Indica se o campo � alteravel
					NIL							,;	// [11]  C   Pasta do campo
					NIL							,;	// [12]  C   Agrupamento do campo
					NIL							,;	// [13]  A   Lista de valores permitido do campo (Combo)
					NIL							,;	// [14]  N   Tamanho maximo da maior op��o do combo
					NIL							,;	// [15]  C   Inicializador de Browse
					.T.							,;	// [16]  L   Indica se o campo � virtual
					NIL							,;	// [17]  C   Picture Variavel
					.F.							)	// [18]  L   Indica pulo de linha ap�s o campo
Next

oView:AddField('VIEW_G66', oStruG66,'G66MASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G66','TELA')

oStruG66:AddGroup( 'GRUPO01', STR0015 , '' , 1 ) // Complemento
oStruG66:AddGroup( 'GRUPO02', STR0016 , '' , 2 ) // Exce��es - Dias do M�s
oStruG66:AddGroup( 'GRUPO03', STR0017 , '' , 2 ) // Exce��es - Dias da Semana

// Colocando todos os campos para um agrupamento'
oStruG66:SetProperty( '*' , MVC_VIEW_GROUP_NUMBER, 'GRUPO01' )

oStruG66:RemoveField( 'G66_DIAMES' )
oStruG66:RemoveField( 'G66_DIASEM' )

// Trocando o agrupamento de alguns campos
For nCampo := 1 To Len(aCamposG66)
	cCampo := aCamposG66[nCampo][01]
	cGrupo := Iif(nCampo <= 32 , 'GRUPO02', 'GRUPO03')
	oStruG66:SetProperty(cCampo,MVC_VIEW_GROUP_NUMBER, cGrupo )
	oStruG66:SetProperty(cCampo,MVC_VIEW_CANCHANGE, .T. )
Next

oView:SetViewProperty( "G66MASTER", "SETLAYOUT", { FF_LAYOUT_HORZ_DESCR_TOP , 16 } )

Return(oView)

//-------------------------------------------------------------------
/*/{Protheus.doc} TUR09Grv()
Gravacao do Complemento de condi��o de pagamento
Converte os flags dos campos dos dias do m�s e da semana em string

@author Rogerio Melonio
@since 05/01/2016
@version P12
@return lRet
/*/
//--------------------------------------------------------------------
Function TUR09Grv(oModel)
Local lRet		:= .T.
Local nCampo		:= 0
Local oModG66	:= 	oModel:GetModel('G66MASTER')
Local cDiasSem	:= ""
Local cDiasMes	:= ""
Local cFlag		:= "S"
Local cCampo 	:= ""

If ( oModel:GetOperation() = MODEL_OPERATION_INSERT ) .Or. ( oModel:GetOperation() = MODEL_OPERATION_UPDATE )  
	
	For nCampo := 1 To Len(aCamposG66)
		cCampo := aCamposG66[nCampo][01]
		If oModG66:GetValue(cCampo)
			cFlag := "S"
		Else
			cFlag := "N"
		Endif
		If nCampo <= 32
			cDiasMes += cFlag
		Else
			cDiasSem += cFlag
		Endif
	Next 
			 
	oModG66:LoadValue("G66_DIAMES",cDiasMes)
	oModG66:LoadValue("G66_DIASEM",cDiasSem)
	
Endif

lRet := FWFormCommit(oModel)
 
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} TUR09Act()
Na ativa��o do modelo na inclus�o, inicializa os campos do s�bado e domingo

@author Rogerio Melonio
@since 05/01/2016
@version P12
@return 
/*/
//--------------------------------------------------------------------
Function TUR09Act(oModel)
Local oModG66	:= oModel:GetModel("G66MASTER") 
Local cDiasMes	:= AllTrim(oModG66:GetValue("G66_DIAMES"))
Local cDiasSem	:= AllTrim(oModG66:GetValue("G66_DIASEM"))
Local nX 		:= 1
Local cStrDia	:= ""
Local nCampo	:= 1
Local cCampo 	:= ""

If oModel:GetOperation() = MODEL_OPERATION_INSERT
	oModG66:LoadValue("G66_DIASAB",.T.)
	oModG66:LoadValue("G66_DIADOM",.T.)
Endif

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} TUR09Vld()
Fun��o de valida��o do tipo de condi��o de pagamento
@author Rogerio Melonio
@since 27/01/2016
@version P12
@return 
/*/
//--------------------------------------------------------------------
Function TUR09Vld(cCondPG)
Local lRet 	:= .T.
Local cTipo := ""

If !Empty(cCondPG)
	cTipo := Posicione("SE4",1,xFilial("SE4")+cCondPG,"E4_TIPO")
 	If cTipo <> "9"
		lRet := .F.
		Help( ,, 'Help',, STR0018 , 1, 0 ) // "Somente Condi��es de Pagamento com tipo 9 podem ser utilizadas."
 	Endif
Endif

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Tur09Load
Na carga do modelo converte as strings dos campos dos dias do m�s 
e da semana em campos l�gicos

@author Fernando Radu Muscalu
@since 21/12/2016
@version 
/*/
//-------------------------------------------------------------------
Function Tur09Load(oFieldModel)

Local aCampos 	:= {}
Local aFields	:= {}
Local aDiasSem	:= {"G66_DIASEG","G66_DIATER","G66_DIAQUA","G66_DIAQUI","G66_DIASEX","G66_DIASAB","G66_DIADOM"}

Local cDiasMes	:= G66->G66_DIAMES
Local cDiasSem	:= G66->G66_DIASEM
Local cCampo 	:= ""

Local nX 		:= 1
Local nP		:= 0

//Fun��o que retorna a carga dos campos de G66 para o submodelo Field G66MASTER
aCampos := TurLoadSubMdl(oFieldModel:GetStruct(),"F","G66",,,{{"G66_DSCCPG",{||Posicione("SE4",1,xFilial("SE4")+G66->G66_CODIGO,"E4_DESCRI")}}},.F.)

aFields := oFieldModel:GetStruct():GetFields()

//Carga das Exce��es de Dias no m�s
For nX := 1 To Len(AllTrim(cDiasMes))
	
	cCampo := "G66_DIA" + StrZero(nX,2)
	
	nP := aScan(aFields,{|x| Alltrim(x[3]) == cCampo })
	
	If ( nP > 0 )
		aCampos[1,nP] := IIf(Substr(cDiasMes,nX,1) == "S",.t.,.f.)
	Else
		
		If ( nX == 32 )
			
			nP := aScan(aFields,{|x| Alltrim(x[3]) == "G66_DIAUD" })
			
			If ( nP > 0 )
				aCampos[1,nP] := IIf(Substr(cDiasMes,nX,1) == "S",.t.,.f.)
			EndIf
				
		Endif
		
	EndIf
		
Next nX

//Carga das Exce��es de Dias na semana
For nX := 1 To Len(AllTrim(cDiasSem))
	
	cCampo := aDiasSem[nX]
	
	nP := aScan(aFields,{|x| Alltrim(x[3]) == cCampo })
	
	If ( nP > 0 )
		aCampos[1,nP] := IIf(Substr(cDiasSem,nX,1) == "S",.t.,.f.)
	EndIf
		
Next nX

aAdd(aCampos,G66->(Recno()))

Return(aCampos)


/*/{Protheus.doc} TA009Cond
Fun��o para calculo de parcelas do Tipo 9 utilizando o complemento cadastrado
@author Anderson Toledo
@since 28/04/2016
@version 
/*/
Function TA009Cond( cCondPgto, nValor, dData )
	Local aParcelas 	:= {}
	Local aExcecoes	:= {}
	Local aDow 		:= { 'DIASEG', 'DIATER', 'DIAQUA', 'DIAQUI', 'DIASEX', 'DIASAB', 'DIADOM' }
	Local cCampoSimu	:= ''
	Local dDtCont		:= ctod( ' / / ' )
	Local dDtVenc		:= ctod( ' / / ' )
	Local nQtdPar		:= 0
	Local nVlrParc  	:= 0
	Local nVlrDif		:= 0
	Local nX			:= 0
	
	DEFAULT dData	:= dDataBase

	G66->( dbSetOrder(1) ) 
	If G66->( dbSeek( xFilial( 'G66' ) + cCondPgto ) )
		//Adiciona as excess�es
		For nX := 1 to 32
			If nX <= 31
				cCampoSimu := 'G66_DIAM' + cValToChar(nX)
			Else
				cCampoSimu := 'G66_UDIA'
			Endif
			
			aAdd( aExcecoes, { cCampoSimu, SubStr( G66->G66_DIAMES,nX,1 ) == 'S' } )
		Next
		
		For nX := 1 to len(aDow)
			aAdd( aExcecoes, { 'G66_' + aDow[nX], SubStr( G66->G66_DIASEM,nX,1 ) == 'S'} )
		Next
		
		
		nVlrParc  	:= nValor / G66->G66_QTDPAR
		
		// Calcula o valor das parcelas
		If SuperGetMv( 'MV_ARRDPRC',.F.,"1" ) = '1'
			
			nVlrParc := Round( nVlrParc, 2 )
			
		Else
			
			nVlrParc := NoRound( nVlrParc, 2 )
			
		EndIf

		// Calcula se h� diferen�a dpo somat�rio das parcelas com o valor total de faturamento
		nVlrDif := nValor - ( nVlrParc * G66->G66_QTDPAR )
	
		// Verifica a data do in�cio da contagem
		If G66->G66_INCCNT == '1' // Fechamento
			dDtCont := dData
		
		ElseIf G66->G66_INCCNT == '2' // Fora da Semana
		
			dDtCont := dData + ( 8 - Dow( dData ) )
		
		Else // Fora do M�s
		
			dDtCont := DaySum( LastDate( dData ), 1 )
		
		EndIf
		
		// Calcula primeiro vencimento
		dDtVenc := TRSomaDias( dDtCont, G66->G66_QTDDIA + G66->G66_QTDIAT, G66->G66_CONTG, G66->G66_FERIAD, aExcecoes)
		
		For nX := 1 To G66->G66_QTDPAR
		
			If SuperGetMv( 'MV_DIFPARC',.F.,"1"  ) == '1' .And. nX == 1 // Verifica se adciona diferen�a de soma de parcelas na primeira parcela
			
				aAdd( aParcelas, { dDtVenc, nVlrParc + nVlrDif } )
			
			ElseIf SuperGetMv( 'MV_DIFPARC',.F.,"1" ) == '2' .And. nX == nQtdPar  // Verifica se adciona diferen�a de soma de parcelas na �ltima parcela
			
				aAdd( aParcelas, { dDtVenc, nVlrParc + nVlrDif } )
			
			Else
			
				aAdd( aParcelas, { dDtVenc, nVlrParc } )
			
			EndIf
		
			dDtVenc := TRSomaDias( dDtVenc, G66->G66_QTDDIA , G66->G66_CONTG, G66->G66_FERIAD, aExcecoes )
		
		Next nX
		
		
		
	EndIf


Return aParcelas