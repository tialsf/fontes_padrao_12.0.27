#INCLUDE "TURA010.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA010()

CADASTRO POSTO DE ATENDIMENTO - SIGATUR

@sample 	TURA010()
@return                             
@author  	Fanny Mieko Suzuki
@since   	11/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA010()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G3M')
oBrowse:SetDescription(STR0001) // "Cadastro de Posto de Atendimento"
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef()

CADASTRO POSTO DE ATENDIMENTO - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA010()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	11/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG3M := FWFormStruct(1,'G3M',/*bAvalCampo*/,/*lViewUsado*/)


oModel:= MPFormModel():New('TURA010',/*bPreValidacao*/,/*bPosValidacao*/,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G3MMASTER',/*cOwner*/,oStruG3M,/*Criptog()/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // "Cadastro de Posto de Atendimento"

Return(oModel)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef()

CADASTRO POSTO DE ATENDIMENTO - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA010()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	11/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA010')
Local oStruG3M := FWFormStruct(2,'G3M')

oView:= FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G3M', oStruG3M,'G3MMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G3M','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef()

CADASTRO POSTO DE ATENDIMENTO - DEFINE AROTINA (MVC) 

@sample 	TURA010()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	11/03/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // "Pesquisar"
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA010'	OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA010'	OPERATION 3	ACCESS 0 // "Incluir"
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA010'	OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA010'	OPERATION 5	ACCESS 0 // "Excluir"

Return(aRotina)

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI010( cXml, nTypeTrans, cTypeMessage )

Return aRet