#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA011.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA011
Fun��o que executa a manuten��o no cadastro de Formas de Pagamento.
@author Elton Teodoro Alves
@since 29/04/2015
@version P12
/*/
//------------------------------------------------------------------------------------------
Function TURA011()
	
	Local	oBrowse	:=	FwMBrowse():New()
	Local	cAlias	:=	'G3N'
	
	oBrowse:SetAlias( cAlias )
	
	oBrowse:AddLegend	( "G3N_MSBLQL=='1'", "GRAY"  , STR0016 ) // Bloqueado
	oBrowse:AddLegend	( "G3N_MSBLQL=='2'", "GREEN" , STR0017 ) // Ativo
	
	oBrowse:SetDescription( STR0015 )	// "Cadastro de Formas de Pagamento"
	
	oBrowse:Activate()
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Fun��o que monta o menu do programa.
@author Elton Teodoro Alves
@since 29/04/2015
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina Title STR0001	ACTION 'PesqBrw'			OPERATION 1 ACCESS 0 // Pesquisa
	ADD OPTION aRotina Title STR0002	ACTION 'VIEWDEF.TURA011'	OPERATION 2 ACCESS 0 // Visualizar
	ADD OPTION aRotina Title STR0003	ACTION 'VIEWDEF.TURA011'	OPERATION 3 ACCESS 0 // Incluir
	ADD OPTION aRotina Title STR0004	ACTION 'VIEWDEF.TURA011'	OPERATION 4 ACCESS 0 // Alterar
	ADD OPTION aRotina Title STR0005	ACTION 'VIEWDEF.TURA011'	OPERATION 5 ACCESS 0 // Excluir
	ADD OPTION aRotina Title STR0006	ACTION 'VIEWDEF.TURA011'	OPERATION 8 ACCESS 0 // Imprimir
	ADD OPTION aRotina Title STR0007	ACTION 'VIEWDEF.TURA011'	OPERATION 9 ACCESS 0 // Copiar
	
Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Fun��o que monta o modelo de dados do programa.
@author Elton Teodoro Alves
@since 29/04/2015
@version 1.0
@return Objeto Retorno do modelo de dados
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()
	
	Local	oModel		:=	MpFormModel():New( 'TURA011' , , { | oModel | TA011TUDOK( oModel ) } )
	Local	oStruG3N	:=	FWFormStruct( 1, 'G3N' )
	Local	oStruG3O	:=	FWFormStruct( 1, 'G3O' )
	Local 	bPreG3O 	:= 	{|oModel,nLinha,cAcao,cCampo| TURA11PreVl(oModel, nLinha, cAcao, cCampo) }
	
	oModel:AddFields( 'G3NMASTER', , oStruG3N )
	oModel:AddGrid( 'G3ODETAIL', 'G3NMASTER', oStruG3O,bPreG3O,{ | oModel, nLinha | LinhaOk( oModel, nLinha ) } )
	
	oModel:SetRelation( 'G3ODETAIL' , { { 'G3O_FILIAL', "xFilial( 'G3N' )" }, { 'G3O_CODIGO', 'G3N_CODIGO' } }, G3O->(IndexKey( 1 ) ) )
	
	oModel:SetDescription( STR0015 )	// "Cadastro de Formas de Pagamento"
	oModel:GetModel( 'G3NMASTER' ):SetDescription( STR0008 ) // Cabe�alho da Forma de Pagamento
	oModel:GetModel( 'G3ODETAIL' ):SetDescription( STR0009 ) // Itens da Forma de Pagamento
	
Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Fun��o que monta a View do programa.
@author Elton Teodoro Alves
@since 29/04/2015
@version 1.0
@return Objeto Retorno do modelo de interface gr�fica
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()
	
	Local	oModel		:=	FWLoadModel( 'TURA011' )
	Local	oStruG3N	:=	FWFormStruct( 2, 'G3N')
	Local	oStruG3O	:=	FWFormStruct( 2, 'G3O')
	Local	oView		:=	FWFormView():New()
	
	oStruG3N:AddGroup( 'CABEC', '', '', 1 )
	oStruG3N:AddGroup( 'TIPOSEG', STR0027, '', 2 ) //'Segmento de Neg�cio'
	
	oStruG3N:SetProperty( '*' , MVC_VIEW_GROUP_NUMBER, 'CABEC' )
	
	oStruG3N:SetProperty( 'G3N_CORP'	, MVC_VIEW_GROUP_NUMBER, 'TIPOSEG' )
	oStruG3N:SetProperty( 'G3N_EVENTO'	, MVC_VIEW_GROUP_NUMBER, 'TIPOSEG' )
	oStruG3N:SetProperty( 'G3N_LAZER'	, MVC_VIEW_GROUP_NUMBER, 'TIPOSEG' )
	
	oView:SetModel( oModel )
	oView:AddField( 'VIEWG3N', oStruG3N, 'G3NMASTER' )
	oView:AddGrid( 'VIEWG3O', oStruG3O, 'G3ODETAIL' )
	
	oView:CreateHorizontalBox( 'SUPERIOR', 40 )
	oView:CreateHorizontalBox( 'INFERIOR', 60 )
	
	oView:AddIncrementField( 'VIEWG3O', 'G3O_ITEM' )
	
	oView:EnableTitleView( 'VIEWG3O', STR0028 ) //'Tipos da Forma de Pagamento'
	
	oView:SetOwnerView( 'VIEWG3N', 'SUPERIOR' )
	oView:SetOwnerView( 'VIEWG3O', 'INFERIOR' )
	
Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} LinhaOk
Fun��o que valida as linhas da Grid
Verifica se a soma dos percentuais dos tipos de formas de pagamento n�o ultrapassa 100%
@author Elton Teodoro Alves
@since 29/04/2015
@version 1.0
@param oModel, objeto, Modelo de dados da Grid
@param nLinha, num�rico, Linha posicionada na Grid
@return L�gico Retorno da valida��o da linha
/*/
//------------------------------------------------------------------------------------------
Static Function LinhaOk( oModel, nLinha )
	
	Local	nX			:=	0
	Local  nTipoAtu	:= oModel:GetValue( 'G3O_TIPO') 
	
	For nX := 1 To oModel:Length()
		If !oModel:IsDeleted(nX)
			If nLinha <> nX .And. nTipoAtu == oModel:GetValue( 'G3O_TIPO', nX)
				Help( ,, 'Help',, STR0026, 1, 0 ) //"N�o � permitido repetir o tipo da FOP"
				Return .F.
			EndIf
		Endif
	Next nX
	
	If oModel:GetValue( 'G3O_TIPO',  oModel:nLine ) == '1'
		If Empty( oModel:GetValue( 'G3O_ORICP', oModel:nLine ) ) .Or. Empty( oModel:GetValue( 'G3O_ORICR', oModel:nLine ) )
			Help( ,, 'Help',, STR0022, 1, 0 ) // Obrigat�rio informar Origem da Condi��o de Pagamento e Origem da Condi��o de Recebimento para FOP tipo 1=Intermedia��o.
			Return	.F.
		EndIf
	EndIf
	
	If oModel:GetValue( 'G3O_TIPO',  oModel:nLine ) == '2'
		If Empty( oModel:GetValue( 'G3O_ORICR', oModel:nLine ) )
			Help( ,, 'Help',, STR0023, 1, 0 ) // Obrigat�rio informar Origem da Condi��o de Recebimento para FOP tipo 2=Direto Agencia.
			Return	.F.
		EndIf		
	EndIf
	
	If oModel:GetValue( 'G3O_ORICP', oModel:nLine ) == '2'
		If ( Empty( oModel:GetValue( 'G3O_CONDCP', oModel:nLine ) ) .Or. Empty( oModel:GetValue( 'G3O_NATCP', oModel:nLine ) ) )
			Help( ,, 'Help',, STR0011, 1, 0 ) // Obrigat�rio informar Cond. de Pagto e Natureza de Pagto quando origem da Cond. de Pagto for 2=Fixa.
			Return	.F.
		EndIf
	EndIf
	
	If oModel:GetValue( 'G3O_ORICR', oModel:nLine ) == '2'
		If ( Empty( oModel:GetValue( 'G3O_CONDCR', oModel:nLine ) ) .Or. Empty( oModel:GetValue( 'G3O_NATCR', oModel:nLine ) ) )
			Help( ,, 'Help',, STR0012, 1, 0 ) // Obrigat�rio informar Cond. de Recbto e Natureza de Recbto quando origem da Cond. de Recbto for 2=Fixa.
			Return	.F.
		EndIf
	EndIf
	
Return .T.

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA011TUDOK
Fun��o que valida todo Modelo de Dados
Verifica se pelo menos um segmento de neg�cio foi selecionado e
se a soma dos percentuais dos tipos de formas de pagamento totaliza 100%
@author Elton Teodoro Alves
@since 29/04/2015
@version 1.0
@param oModel, objeto, Modelo de dados
@return L�gico Retorno da valida��o do modelo de dados
/*/
//------------------------------------------------------------------------------------------
Static Function TA011TUDOK( oModel )
	
	Local	lRet		:=	.T.
	Local	nOperation	:=	oModel:GetOperation()
	Local	nX			:=	0
	Local	nTotPerc	:=	0
	
	If cValToChar( nOperation ) $ '34'
		
		For nX := 1 To oModel:GetModel( 'G3ODETAIL' ):Length()
			If !oModel:GetModel( 'G3ODETAIL' ):IsDeleted(nX)
				nTotPerc	+=	oModel:GetModel( 'G3ODETAIL' ):GetValue( 'G3O_PERC', nX)
			Endif
		Next nX
		
		If ! nTotPerc = 100
			
			Help( ,, 'Help',, STR0013, 1, 0 ) // A soma dos percentuais dos tipos de FOP deve totalizar 100%.
			
			lRet	:=	.F.
			
		End If
		
		If ! (	oModel:GetModel( 'G3NMASTER' ):GetValue( 'G3N_CORP' ) .Or.;
				oModel:GetModel( 'G3NMASTER' ):GetValue( 'G3N_EVENTO' ) .Or.;
				oModel:GetModel( 'G3NMASTER' ):GetValue( 'G3N_LAZER' ) )
			
			Help( ,, 'Help',, STR0014, 1, 0 ) // Pelo menos um segmento de neg�cio deve ser selecionado.
			
			lRet	:=	.F.
			
		End If
		
	End If
	
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA011CLR
Fun��o utilizada no gatilho 001 dos campo G3O_TIPO, G3O_ORICP e G3O_ORICR para limpar o conte�do dos campos da Grid conforme regra definida.
@author Elton Teodoro Alves
@since 20/10/2015
@version P12
/*/
//------------------------------------------------------------------------------------------
Function TA011CLR()
	
	Local oModel  := FwModelActve()
	Local oModG3O := oModel:GetModel( 'G3ODETAIL'  )
	Local cTipo   := oModG3O:GetValue( 'G3O_TIPO'  )
	Local cOriCp  := oModG3O:GetValue( 'G3O_ORICP' )
	Local cOriCr  := oModG3O:GetValue( 'G3O_ORICR' )
	
	If cTipo == '1' // Verifica se FOP � do tipo intermedia��o
		
		If cOriCp # '2' // Se Origem da Condi��o de Pagamento � diferente de Fixa e limpa os campos de condi��o de pagamento e origem de pagamento.
			
			oModG3O:LoadValue( 'G3O_CONDCP' , '' )
			oModG3O:LoadValue( 'G3O_NATCP'  , '' )
			
		End If
		
		If cOriCr # '2' // Se Origem da Condi��o de recebimento � diferente de Fixa e limpa os campos de condi��o de recebimento e origem de recebimento.
			
			oModG3O:LoadValue( 'G3O_CONDCR' , '' )
			oModG3O:LoadValue( 'G3O_NATCR'  , '' )
			
		End If
		
	ElseIf cTipo == '2' // Verifica se FOP � do tipo Direto Ag�ncia
		
		// Limpa os campos de origem da condi��o de pagamento, da condi��o de pagamento e natureza de pagamento
		oModG3O:LoadValue( 'G3O_ORICP'  , '' )
		oModG3O:LoadValue( 'G3O_CONDCP' , '' )
		oModG3O:LoadValue( 'G3O_NATCP'  , '' )
		
		If cOriCr # '2' // Se Origem da Condi��o de recebimento � diferente de Fixa e limpa os campos de condi��o de recebimento e origem de recebimento.
			
			oModG3O:LoadValue( 'G3O_CONDCR' , '' )
			oModG3O:LoadValue( 'G3O_NATCR'  , '' )
			
		End If
		
	ElseIf cTipo $ '34' .Or. Empty( cTipo ) // Verifica se FOP � do tipo Pag. Direto ou Cart�o Cliente ou est� em branco
		
		// Limpa os campos de origem da condi��o de pagamento, da condi��o de pagamento e natureza de pagamento
		// Limpa os campos de origem da condi��o de recebimento, da condi��o de recebimento e natureza de recebimento
		oModG3O:LoadValue( 'G3O_ORICP'  , '' )
		oModG3O:LoadValue( 'G3O_CONDCP' , '' )
		oModG3O:LoadValue( 'G3O_NATCP'  , '' )
		oModG3O:LoadValue( 'G3O_ORICR'  , '' )
		oModG3O:LoadValue( 'G3O_CONDCR' , '' )
		oModG3O:LoadValue( 'G3O_NATCR'  , '' )
		
	End If
	
Return

/*/{Protheus.doc} TA011VLDNT
Valida a Natureza Financeira informadas nos campos G3O_NATCP e G3O_NATCR
@author Elton Teodoro Alves
@since 20/10/2015
@version P12
@return L�gico Retorno da valida��o da Natureza Financeira informada
/*/
Function TA011VLDNT()
	
	Local lRet     := .T.
	Local aArea    := GetArea()
	Local cReadVar := ReadVar()
	Local cCond    := ''
	
	cReadVar := SubStr( cReadVar, 4, Len( cReadVar ) - 3 ) // Atribui a vari�velI em que campo a fun��o foi chamada
	
	cCond := If( cReadVar == 'G3O_NATCP', 'D', 'R' ) // Atribui a vari�vel a condi��o da natureza D=Despesa / R=Receita
	
	DbSelectArea( 'SED' )
	DbSetOrder(1) // ED_FILIAL+ED_CODIGO
	
	If DbSeek( xFilial( 'SED' ) + &( ReadVar() ) ) // Posiciona no registro do Cadastro da Natureza Financeira
		
		If ! SED->ED_TIPO $ ' /2' // Verifica se a Natureza Financeira � Sint�tica
			
			lRet := .F.
			
			Help( ,, 'Help',, STR0018, 1, 0 ) // Natureza Financeira deve ser Anal�tica
			
		ElseIf SED->ED_COND # cCond // Verifica se a natureza financeira tem a condi��o esperada para o campo
			
			lRet := .F.
			
			If cCond == 'R'
				
				Help( ,, 'Help',, STR0019, 1, 0 ) // Natureza Financeira a Receber deve ser uma Receita
				
			End If
			
			If cCond == 'D'
				
				Help( ,, 'Help',, STR0020, 1, 0 ) // Natureza Financeira a Pagar deve ser uma Despesa
				
			End If
			
		End If
		
	Else
		
		Help( ,, 'Help',, STR0021, 1, 0 ) // Natureza n�o cadastrada
		
	End If
	
	RestArea( aArea )
	
Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA11PreVl
                Realiza pr� valida��es na linha do Grid. 

@sample            TURA11PreVl(oMdl, nLine, cAcao, cCampo)
@param                              [oMdl],objeto,Modelo de Dados a ser utilizado para a grava��o dos dados.
@param                              [nLine],numerico,Linha que est� sendo editada.
@param                              [cAcao],caractere,Eventos executados: DELETE;UNDELETE;CANSETVALUE,SETVALUE,ETC.
@param                              [cCampo],caractere,Nome do campo sendo editado.
@Return             logico    Resultado da Vali��o 
@since                 29/10/2015
@version            P12

/*/
//------------------------------------------------------------------------------
Static Function TURA11PreVl(oModel, nLinha, cAcao, cCampo)
Local lRet := .T.

If cAcao == "CANSETVALUE" .AND. ;
	cCampo == 'G3O_ORICR' .AND. ;
	(oModel:GetValue("G3O_TIPO") $ '4') .AND. ;
	(!Empty(oModel:GetValue("G3O_ORICR")))
                lRet := .F.
                Help( , , STR0024, , STR0025, 1, 0)	// "Aten��o"  "A informa��o n�o pode ser editado"                 
EndIf

Return( lRet )

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI011( cXml, nTypeTrans, cTypeMessage )

Return aRet
