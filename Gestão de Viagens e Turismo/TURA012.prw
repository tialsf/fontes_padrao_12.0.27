#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA012.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA012

Fun��o de cadastro de Tipos de Vistos.

@sample 	TURA012()
@return   	.T.                          
@author	Elton Teodoro Alves
@since 		22/04/2015
@version 	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA012()
	
Local oBrowse	:=	FwMBrowse():New()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G4F')
oBrowse:SetDescription(STR0009) // "Cadastro de Tipos de Visto"
oBrowse:Activate()

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MENUDEF

Fun��o que monta o Menu de Rotinas do Cadastro de Vistos

@sample 	MenuDef()
@return  	aRotina                       
@author	Elton Teodoro Alves
@since 		22/04/2015
@version 	P12
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()
	
Local aRotina := {}

ADD OPTION aRotina Title STR0001	ACTION 'PesqBrw'			OPERATION 1 ACCESS 0 // Pesquisa
ADD OPTION aRotina Title STR0002	ACTION 'VIEWDEF.TURA012'	OPERATION 2 ACCESS 0 // Visualizar
ADD OPTION aRotina Title STR0003	ACTION 'VIEWDEF.TURA012'	OPERATION 3 ACCESS 0 // Incluir
ADD OPTION aRotina Title STR0004	ACTION 'VIEWDEF.TURA012'	OPERATION 4 ACCESS 0 // Alterar
ADD OPTION aRotina Title STR0005	ACTION 'VIEWDEF.TURA012'	OPERATION 5 ACCESS 0 // Excluir

Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} MODELDEF

Fun��o que monta o Modelo de Dados do cadastro de Tipos de Vistos

@sample 	ModelDef()
@return  	oModel                       
@author	Elton Teodoro Alves
@since 		22/04/2015
@version 	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()
	
Local oModel	:=	MpFormModel():New( 'TURA012' )
Local oStruct	:=	FWFormStruct( 1, 'G4F' )

oModel:AddFields( 'MASTER', , oStruct )
oModel:SetDescription(STR0009) // "Cadastro de Tipos de Visto"
oModel:GetModel( 'MASTER' ):SetDescription( STR0008 ) // Dados do Cadastro de Vistos

Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} VIEWDEF

Fun��o que monta a View de Dados do cadastro de Tipos de Vistos

@sample 	ViewDef()
@return  	oView                       
@author	Elton Teodoro Alves
@since 		22/04/2015
@version 	P12
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()
	
Local oModel	:=	FWLoadModel( 'TURA012' )
Local oStruct	:=	FWFormStruct( 2, 'G4F' )
Local oView	:=	FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEW', oStruct, 'MASTER' )
oView:CreateHorizontalBox( 'TELA', 100 )
oView:SetOwnerView( 'VIEW', 'TELA' )

Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Thiago Tavares
@since   	30/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI012( cXml, nTypeTrans, cTypeMessage )

Return aRet