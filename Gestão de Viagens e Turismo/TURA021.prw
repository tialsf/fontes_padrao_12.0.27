#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA021.CH'

/*/{Protheus.doc} TURA021
Fun��o de cadastro de Categoria de ve�culos .
@author	Elton Teodoro Alves
@since 20/05/2015
@version 1.0
/*/
Function TURA021()
	
	Local	oBrowse	:=	FwMBrowse():New()
	Local	cAlias	:=	'G4Y'
	
	oBrowse:SetAlias( cAlias )
	
	oBrowse:SetDescription( STR0009 )	// "Cadastro de Categoria de Ve�culos"
	
	oBrowse:Activate()
	
Return

/*/{Protheus.doc} MENUDEF
Fun��o que monta o Menu de Rotinas do Cadastro de Tipo de ve�culo
@author	Elton Teodoro Alves
@since 20/05/2015
@version 1.0
/*/
Static Function MenuDef()
	
	Local aRotina := {}
	
	ADD OPTION aRotina Title STR0001	ACTION 'PesqBrw'			OPERATION 1 ACCESS 0 // Pesquisa
	ADD OPTION aRotina Title STR0002	ACTION 'VIEWDEF.TURA021'	OPERATION 2 ACCESS 0 // Visualizar
	ADD OPTION aRotina Title STR0003	ACTION 'VIEWDEF.TURA021'	OPERATION 3 ACCESS 0 // Incluir
	ADD OPTION aRotina Title STR0004	ACTION 'VIEWDEF.TURA021'	OPERATION 4 ACCESS 0 // Alterar
	ADD OPTION aRotina Title STR0005	ACTION 'VIEWDEF.TURA021'	OPERATION 5 ACCESS 0 // Excluir
	ADD OPTION aRotina Title STR0006	ACTION 'VIEWDEF.TURA021'	OPERATION 8 ACCESS 0 // Imprimir
	ADD OPTION aRotina Title STR0007	ACTION 'VIEWDEF.TURA021'	OPERATION 9 ACCESS 0 // Copiar
	
Return aRotina

/*/{Protheus.doc} MODELDEF
Fun��o que monta o Modelo de Dados do cadastro de Categoria de ve�culos
@author	Elton Teodoro Alves
@since 20/05/2015
@version 1.0
/*/
Static Function ModelDef()
	
	Local	oModel	:=	MpFormModel():New( 'TURA021' )
	Local	oStruct	:=	FWFormStruct( 1, 'G4Y')
	
	oModel:AddFields( 'MASTER', , oStruct )
	oModel:SetDescription( STR0009 )	// "Cadastro de Categoria de Ve�culos"
	oModel:GetModel( 'MASTER' ):SetDescription( STR0008 ) // Dados do Cadastro de Categoria de Ve�culos
	
Return oModel

/*/{Protheus.doc} VIEWDEF
Fun��o que monta a View de Dados do cadastro de Categoria de ve�culos
@author	Elton Teodoro Alves
@since 20/05/2015
@version 1.0
/*/
Static Function ViewDef()
	
	Local	oModel	:=	FWLoadModel( 'TURA021' )
	Local	oStruct	:=	FWFormStruct( 2, 'G4Y' )
	Local	oView	:=	FWFormView():New()
	
	oView:SetModel( oModel )
	oView:AddField( 'VIEW', oStruct, 'MASTER' )
	oView:CreateHorizontalBox( 'TELA', 100 )
	oView:SetOwnerView( 'VIEW', 'TELA' )
	
Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Thiago Tavares
@since   	22/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI021( cXml, nTypeTrans, cTypeMessage )

Return aRet