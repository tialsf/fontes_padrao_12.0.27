#INCLUDE 'TOTVS.CH'
#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FWMVCDEF.CH'
#INCLUDE 'TURA025.CH'

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TURA025

Fun��o que executa a manuten��o no cadastro de Grupo de Fornecedores.

@author 	Elton Teodoro Alves
@since 		21/05/2015
@version 	p12.1.6
/*/
//---------------------------------------------------------------------------------
Function TURA025()
	
Local	oBrowse := FwMBrowse():New()
Local	cAlias  := 'G5M'

oBrowse:SetAlias( cAlias )
oBrowse:SetDescription( STR0013 )   // "Cadastro de Grupos de Fornecedor"
oBrowse:Activate()

Return

//---------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Fun��o que monta o menu do programa.

@author 	Elton Teodoro Alves
@since 		21/05/2015
@version 	p12.1.6
/*/
//---------------------------------------------------------------------------------
Static Function MenuDef()
	
Local aRotina := {}

ADD OPTION aRotina Title STR0001	ACTION 'PesqBrw'			OPERATION 1 ACCESS 0 // Pesquisa
ADD OPTION aRotina Title STR0002	ACTION 'VIEWDEF.TURA025'	OPERATION 2 ACCESS 0 // Visualizar
ADD OPTION aRotina Title STR0003	ACTION 'VIEWDEF.TURA025'	OPERATION 3 ACCESS 0 // Incluir
ADD OPTION aRotina Title STR0004	ACTION 'VIEWDEF.TURA025'	OPERATION 4 ACCESS 0 // Alterar
ADD OPTION aRotina Title STR0005	ACTION 'VIEWDEF.TURA025'	OPERATION 5 ACCESS 0 // Excluir
ADD OPTION aRotina Title STR0006	ACTION 'VIEWDEF.TURA025'	OPERATION 8 ACCESS 0 // Imprimir
ADD OPTION aRotina Title STR0007	ACTION 'VIEWDEF.TURA025'	OPERATION 9 ACCESS 0 // Copiar

Return aRotina

//---------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Fun��o que monta o modelo de dados do programa.

@author 	Elton Teodoro Alves
@since 		21/05/2015
@version 	P12.1.6
@return 	Objeto Retorno do modelo de dados
/*/
//---------------------------------------------------------------------------------
Static Function ModelDef()
	
Local oModel   := MpFormModel():New( 'TURA025' , /*bPreValidacao*/, { | oModel | TA025TUDOK( oModel ) }/*bPosValidacao*/, /*bCommit*/, /*bCancel*/)
Local oStruG5M := FWFormStruct( 1, 'G5M' )
Local oStruG5N := FWFormStruct( 1, 'G5N' )

oStruG5M:SetProperty('G5M_PORPRD', MODEL_FIELD_INIT	,  {|| "2" })

oModel:AddFields( 'G5MMASTER', , oStruG5M )
oModel:AddGrid( 'G5NDETAIL', 'G5MMASTER', oStruG5N, /*bLinePre*/, { | oModel, nLinha | TA025LINOK( oModel, nLinha ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
oModel:GetModel('G5NDETAIL'):SetUniqueLine( { 'G5N_FORNEC', 'G5N_LOJA' } )
oModel:GetModel('G5NDETAIL'):SetOptional( .T. )

oModel:SetRelation( 'G5NDETAIL', { { 'G5N_FILIAL', "xFilial( 'G5M' )" }, { 'G5N_CODIGO', 'G5M_CODIGO' } }, G5N->(IndexKey( 1 ) ) )

oModel:SetDescription( STR0013 )   // "Cadastro de Grupos de Fornecedor"
oModel:GetModel( 'G5MMASTER' ):SetDescription( STR0008 ) // Cabe�alho do Grupo de Fornecedores
oModel:GetModel( 'G5NDETAIL' ):SetDescription( STR0009 ) // Itens do Grupo de Fornecedores

Return oModel

//---------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Fun��o que monta a View do programa.

@author 	Elton Teodoro Alves
@since 		21/05/2015
@version 	P12.1.6
@return Objeto Retorno do modelo de interface gr�fica
/*/
//---------------------------------------------------------------------------------
Static Function ViewDef()
	
Local	oModel   := FWLoadModel( 'TURA025' )
Local	oStruG5M := FWFormStruct( 2, 'G5M')
Local	oStruG5N := FWFormStruct( 2, 'G5N')
Local	oView    := FWFormView():New()

oView:SetModel( oModel )
oView:AddField( 'VIEWG5M', oStruG5M, 'G5MMASTER' )
oView:AddGrid ( 'VIEWG5N', oStruG5N, 'G5NDETAIL' )

oView:CreateHorizontalBox( 'SUPERIOR', 20 )
oView:CreateHorizontalBox( 'INFERIOR', 80 )

oView:EnableTitleView( 'VIEWG5N', 'Fornecedores' )

oView:SetOwnerView( 'VIEWG5M', 'SUPERIOR' )
oView:SetOwnerView( 'VIEWG5N', 'INFERIOR' )

Return oView

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA025LINOK

Fun��o que valida as linhas da Grid
Verifica se o fornecedor com loja em branco, que indica que todas as lojas fazem parte do grupo, n�o tem j� informado seu cadastro com loja preenchida e vice versa.

@author 	Elton Teodoro Alves
@since 		21/04/2015
@version 	P12.1.6
@param 		oModel, objeto, Modelo de dados da Grid
@param 		nLinha, num�rico, Linha posicionada na Grid
@return 	L�gico Retorno da valida��o da linha
/*/
//---------------------------------------------------------------------------------
Static Function TA025LINOK( oModel, nLinha )
	
Local	lRet		:=	.T.
Local	cFornec	:=	oModel:GetValue( 'G5N_FORNEC' )
Local	cLoja		:=	oModel:GetValue( 'G5N_LOJA' )
Local	aData		:=	oModel:GetData()
Local	aOldData	:=	oModel:GetOldData()
Local	nPosFornec	:=	aScan( aOldData[ 1 ], { | X | X [ 2 ] == 'G5N_FORNEC' } )
Local	nPosLoja	:=	aScan( aOldData[ 1 ], { | X | X [ 2 ] == 'G5N_LOJA' } )

If ! Empty(cLoja) .And.	aScan( aData, { | X | X[ 1, 1, nPosFornec ] == cFornec .And. Empty(X[ 1, 1, nPosLoja ] ) .And. ! X[ 3 ] } )
	Help( , , 'Help', , STR0010, 1, 0 ) // Fornecedor j� informado com Loja em Branco, o que indica que todas as lojas comp�em o Grupo de Fornecedores.
	lRet	:=	.F.
ElseIf Empty(cLoja) .And. aScan( aData, { | X | X[ 1, 1, nPosFornec ] == cFornec .And. ! Empty(X[ 1, 1, nPosLoja ] ) .And. ! X[ 3 ] } )
	Help( , , 'Help', , STR0011, 1, 0 ) //""
	lRet	:=	.F.	
EndIf

Return lRet

//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA025TUDOK

Fun��o que valida todo Modelo de Dados
Verifica se foi definido que o Grupo de Fornecedores teve definido o agrupamento por Produto (G5M_PORPRD == '1')
obrigando assim a informar um grupo de produto v�lido

@author 	Elton Teodoro Alves
@since 		21/05/2015
@version 	P12.1.6
@param 		oModel, objeto, Modelo de dados
@return 	L�gico Retorno da valida��o do modelo de dados
/*/
//---------------------------------------------------------------------------------
Static Function TA025TUDOK( oModel )
	
Local lRet     := .T.
Local aArea    := GetArea()
Local aAreaG5M := G5M->(GetArea())
Local nX       := 0 

If oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE 
	If oModel:GetModel( 'G5MMASTER' ):GetValue( 'G5M_PORPRD' ) == '1' .And. Empty(oModel:GetModel( 'G5MMASTER' ):GetValue( 'G5M_GRPPRD' ))
		Help( , , 'Help', , STR0012, 1, 0 ) // Agrupamento definido por Produto, informe um Grupo de Produto V�lido.
		lRet := .F.
	EndIf
	If lRet .AND. oModel:GetModel( 'G5MMASTER' ):GetValue("G5M_PORPRD")=="2" .AND. TURXEmptyGrid(oModel:GetModel('G5NDETAIL'))
		Help( , , 'Help', , STR0020, 1, 0 ) //"Para agrupamentos que n�o sejam por produto � necess�rio informar ao menos um fornecedor."  
		lRet := .F.	
	EndIf		
EndIf


Return lRet
//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Fun��o para chamar o Adapter para integra��o via Mensagem �nica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml � O XML recebido pelo EAI Protheus
			cType � Tipo de transa��o
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage � Tipo da mensagem do EAI
				'20' � Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' � Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' � Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] � Vari�vel l�gica, indicando se o processamento foi executado com sucesso (.T.) ou n�o (.F.)
			aRet[2] � String contendo informa��es sobre o processamento
			aRet[3] � String com o nome da mensagem �nica deste cadastro                        
@author  	Thiago Tavares
@since   	30/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet := TURI025( cXml, nTypeTrans, cTypeMessage )

Return aRet



//---------------------------------------------------------------------------------
/*/{Protheus.doc} TA025VldFor

Valida a existencia do fornecedor quando informado Codigo e Loja ou apenas Codigo.

@author 	Inova��o
@since 		12/08/2016
@version 	P12.1.6
@param 		
@return 	Valida��o -> .T. ou .F.  
/*/
//---------------------------------------------------------------------------------
Function TA025VldFor()

Local oModel  := FwModelActive()
Local oMdlG5N := oModel:GetModel('G5NDETAIL')
Local cFornec := oMdlG5N:GetValue('G5N_FORNEC')
Local cLoja	:= oMdlG5N:GetValue('G5N_LOJA')
Local lRet 	:= .T.

DbSelectArea('SA2') 
DbSetOrder(1)

If Empty(cLoja)
	cLoja := ""
EndIf
	
If !Empty(cFornec) 
	lRet := SA2->( DBSeek(xFilial('SA2') + cFornec + cLoja )  )
EndIf


Return(lRet)