#INCLUDE "TURA026.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA026()

CADASTRO DE TIPO DE EMISS�O - SIGATUR

@sample 	TURA026()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA026()

Local oBrowse	:= FWMBrowse():New()

// criando o registro padr�o 99 - TODOS
Processa({|| TURA026G5O()}) 

oBrowse:SetAlias('G5O')
oBrowse:SetDescription(STR0001) // Cadastro de Tipo de Emiss�o
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE TIPO DE EMISS�O - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA026()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG5O := FWFormStruct(1,'G5O',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA026',/*bPreValidacao*/,{|oModel|TR26VALCHAV(oModel)},/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5OMASTER',/*cOwner*/,oStruG5O,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de Tipo de Emiss�o

oModel:SetActivate()
oModel:SetVldActivate({|oModel|TURA026VLD(oModel)})

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE TIPO DE EMISS�O - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA026()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA026')
Local oStruG5O := FWFormStruct(2,'G5O')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5O', oStruG5O,'G5OMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5O','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE TIPO DE EMISS�O - DEFINE AROTINA (MVC) 

@sample 	TURA026()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Pesquisar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA026'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA026'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA026'	OPERATION 4	ACCESS 0 // Alterar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA026'	OPERATION 5	ACCESS 0 // Excluir

Return(aRotina)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} TR26VALCHAV(oModel)

VALIDA CAMPO CHAVE NA TABELA G5O

@sample 	TURA026()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12

*/
//------------------------------------------------------------------------------------------

Static Function TR26VALCHAV(oModel)

Local lRet 		:= .T.
Local nOperation	:= oModel:GetOperation()

// VERIFICA SE � INCLUS�O OU ATUALIZA��O
IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 
	// FUN��O QUE RETORNA F CASO CHAVE J� EXISTA
	lRet := ExistChav('G5O',FwFldGet('G5O_CODIGO'),1)  

ENDIF	


Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc}TURA026VLD(oModel)

VALIDA EXCLUS�O / ALTERA��O DO TIPO DE EMISS�O

@sample 	TURA026VLD()
@return  	aRotina                       
@author  	Marcos Caio Vicentin
@since   	09/11/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Static Function TURA026VLD(oModel)

Local lRet 		:= .T.
Local nOperation	:= oModel:GetOperation()

// VERIFICA SE � INCLUS�O OU ATUALIZA��O
IF (nOperation == MODEL_OPERATION_DELETE .OR. nOperation == MODEL_OPERATION_UPDATE ) .and. !IsInCallSt("IpcCfg020A") 
	IF G5O->G5O_CODIGO == Replicate("9",TamSx3('G5O_CODIGO')[1] ) 
		Help("TURA026VLD", 1, STR0007, , STR0008, 1, 0) // "Aten��o" - "Tipo 99 Tipo de Emiss�o Todos - N�o � possivel Excluir /  Alterar o Registro"
		lRet := .F.
	ENDIF
ENDIF

Return lRet

//------------------------------------------------------------------------------------------
/*{Protheus.doc}TURXG5O()

INSERE O REGISTRO DE CENTRO DE CUSTO

@sample 	TURXG5O()
@return  	lRet                       
@author  	Thiago Tavares
@since   	12/11/2015
@version  	P12.1.8
*/
//------------------------------------------------------------------------------------------
Function TURA026G5O()

Local lRet      := .T.
Local aArea     := GetArea()
Local oModel	

G5O->(DbSetOrder(1))
If !G5O->(DbSeek(xFilial('G5O')+Replicate("9",TamSx3('G5O_CODIGO')[1] ) ))
	oModel := FWLoadModel('TURA026') 
	oModel:SetOperation(MODEL_OPERATION_INSERT)
	If oModel:Activate()
		oModel:GetModel('G5OMASTER'):SetValue('G5O_CODIGO' ,Replicate("9",TamSx3('G5O_CODIGO')[1] ) )
		oModel:GetModel('G5OMASTER'):SetValue('G5O_DESCR' ,STR0012)
		If oModel:VldData() 
			oModel:CommitData()
		Endif
	Endif
	oModel:Deactivate() 
EndIf

RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI026( cXml, nTypeTrans, cTypeMessage )

Return aRet