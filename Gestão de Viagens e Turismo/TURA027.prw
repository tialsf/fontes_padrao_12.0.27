#INCLUDE "TURA027.CH" 
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA027()

CADASTRO DE MOTIVO DE DIVERGENCIA - SIGATUR

@sample 	TURA027()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA027()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G5P')
oBrowse:SetDescription(STR0001) // Cadastro de MOTIVO DE DIVERGENCIA
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE MOTIVO DE DIVERGENCIA - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA027()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG5P := FWFormStruct(1,'G5P',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA027',/*bPreValidacao*/,{|oModel|TR27VALCHAV(oModel)},/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5PMASTER',/*cOwner*/,oStruG5P,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de MOTIVO DE DIVERGENCIA

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE MOTIVO DE DIVERGENCIA - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA027()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA027')
Local oStruG5P := FWFormStruct(2,'G5P')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5P', oStruG5P,'G5PMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5P','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE MOTIVO DE DIVERGENCIA - DEFINE AROTINA (MVC) 

@sample 	TURA027()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA027'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA027'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA027'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA027'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} TR27VALCHAV(oModel)

VALIDA CAMPO CHAVE NA TABELA G5P

@sample 	TURA027()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12

*/
//------------------------------------------------------------------------------------------

Static Function TR27VALCHAV(oModel)

Local lRet 		:= .T.
Local nOperation	:= oModel:GetOperation()

// VERIFICA SE � INCLUS�O OU ATUALIZA��O
IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 
	// FUN��O QUE RETORNA F CASO CHAVE J� EXISTA
	lRet := ExistChav('G5P',FwFldGet('G5P_CODIGO'),1)  

ENDIF	


Return lRet
