#INCLUDE "TURA028.CH" 
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA028()

CADASTRO DE MOTIVO DE INTERVENCAO - SIGATUR

@sample 	TURA028()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA028()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G5Q')
oBrowse:SetDescription(STR0001) // Cadastro de MOTIVO DE INTERVENCAO
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE MOTIVO DE INTERVENCAO - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA028()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG5Q := FWFormStruct(1,'G5Q',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA028',/*bPreValidacao*/,{|oModel|TR27VALCHAV(oModel)},/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5QMASTER',/*cOwner*/,oStruG5Q,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // Cadastro de MOTIVO DE INTERVENCAO

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE MOTIVO DE INTERVENCAO - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA028()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA028')
Local oStruG5Q := FWFormStruct(2,'G5Q')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5Q', oStruG5Q,'G5QMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5Q','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE MOTIVO DE INTERVENCAO - DEFINE AROTINA (MVC) 

@sample 	TURA028()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA028'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA028'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA028'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA028'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} TR27VALCHAV(oModel)

VALIDA CAMPO CHAVE NA TABELA G5Q

@sample 	TURA028()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12

*/
//------------------------------------------------------------------------------------------

Static Function TR27VALCHAV(oModel)

Local lRet 		:= .T.
Local nOperation	:= oModel:GetOperation()

IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 
	lRet := ExistChav('G5Q',FwFldGet('G5Q_CODIGO'),1)  
ENDIF	

Return lRet
