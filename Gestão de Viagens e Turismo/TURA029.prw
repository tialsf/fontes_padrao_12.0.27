#INCLUDE "TURA029.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA029()

CADASTRO DE IMPOSTOS - SIGATUR

@sample 	TURA029()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA029()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G5R')
oBrowse:SetDescription(STR0001) // CADASTRO DE IMPOSTOS
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE IMPOSTOS - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA029()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel
Local oStruG5R := FWFormStruct(1,'G5R',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA029',/*bPreValidacao*/,,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5RMASTER',/*cOwner*/,oStruG5R,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // CADASTRO DE IMPOSTOS

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE IMPOSTOS - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA029()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA029')
Local oStruG5R := FWFormStruct(2,'G5R')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5R', oStruG5R,'G5RMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5R','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE IMPOSTOS - DEFINE AROTINA (MVC) 

@sample 	TURA029()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA029'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA029'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA029'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA029'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)



//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )
Local aRet := {}
aRet:= TURI029( cXml, nTypeTrans, cTypeMessage )
Return aRet
