#INCLUDE "TURA030.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA030()

CADASTRO DE CIDADES - SIGATUR

@sample 	TURA030()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------
Function TURA030()

Local oBrowse	:= FWMBrowse():New()
 
oBrowse:SetAlias('G5S')
oBrowse:SetDescription(STR0001) // CADASTRO DE CIDADES
oBrowse:Activate()

Return(.T.)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE CIDADES - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA030()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel
Local oStruG5S := FWFormStruct(1,'G5S',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA030',/*bPreValidacao*/,{ |oModel| TR30VLDCHV(oModel) },/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5SMASTER',/*cOwner*/,oStruG5S,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // CADASTRO DE CIDADES

Return(oModel)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE CIDADES - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA030()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA030')
Local oStruG5S := FWFormStruct(2,'G5S')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5S', oStruG5S,'G5SMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5S','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE CIDADES - DEFINE AROTINA (MVC) 

@sample 	TURA030()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA030'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA030'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA030'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA030'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} TR30VLDCHV

VALIDA CAMPO CHAVE NA TABELA G5S

@sample 	TR30VLDCHV(oModel)
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12

*/
//------------------------------------------------------------------------------------------
Static Function TR30VLDCHV(oModel)

Local lRet 		:= .T.
Local nOperation	:= oModel:GetOperation()

If nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE
	DbSelectArea("G5S") //Cidades
	G5S->(DbSetOrder(2))	//G5S_FILIAL, G5S_CIDADE, G5S_SIGLA, G5S_PAIS 
	If G5S->(DbSeek(xFilial("G5S")+FwFldGet("G5S_CIDADE")+FwFldGet("G5S_SIGLA") + FwFldGet("G5S_PAIS"))) 
		If !(nOperation == MODEL_OPERATION_UPDATE .And. G5S->G5S_CODIGO == FwFldGet("G5S_CODIGO"))
			Help( "TURA030", 1, STR0007, , STR0008, 1, 0) //"Aten��o", "Chave duplicada - Cidade / Sigla / Pa�s duplicada!"
			lRet := .F.
		EndIf	
	EndIf
EndIf	

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	28/09/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI030( cXml, nTypeTrans, cTypeMessage )

Return aRet
