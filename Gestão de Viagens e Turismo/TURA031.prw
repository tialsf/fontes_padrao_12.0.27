#INCLUDE "TURA031.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA031()

CADASTRO DE MOEDA DE TURISMO - SIGATUR

@sample 	TURA031()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA031()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G5T')
oBrowse:AddLegend	( "G5T_MSBLQL=='1'", "RED"  , STR0007 )// "Bloqueado"
oBrowse:AddLegend	( "G5T_MSBLQL=='2'", "GREEN" , STR0008 )// "Ativo"
oBrowse:SetDescription(STR0001) // CADASTRO DE MOEDA DE TURISMO
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE MOEDA DE TURISMO - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA031()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG5T := FWFormStruct(1,'G5T',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA031',/*bPreValidacao*/,{|oModel| TUR031TOK(oModel)   },/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5TMASTER',/*cOwner*/,oStruG5T,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // CADASTRO DE MOEDA DE TURISMO

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE MOEDA DE TURISMO - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA031()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA031')
Local oStruG5T := FWFormStruct(2,'G5T')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5T', oStruG5T,'G5TMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5T','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE MOEDA DE TURISMO - DEFINE AROTINA (MVC) 

@sample 	TURA031()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA031'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA031'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA031'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA031'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} TUR031TOK()

REALIZA A VALIDA��O DO MODELO DE DADOS.
1. VALIDA DUPLICIDADE DE REGISTRO
2. VALIDA AMARRA��ES AO ALTERAR E EXCLUIR MOEDA.
 
@sample 	TUR031TOK()
@Param 	oModel = Modelo de dados
@return   	lRet = .T. OK 
@author  	Inova��o
@since   	25/04/2016
@version  	P12
*/
//------------------------------------------------------------------------------------------

Function TUR031TOK(oModel)

Local aArea      := G5T->(GetArea())
Local cAliasQry  := "" 
Local cCadastro  := ''
Local nOperation := oModel:GetOperation()
Local lAmarracao := .F.
Local lRet       := .T.

If nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE .Or. nOperation == MODEL_OPERATION_DELETE   

	// verificando se a moeda financeira j� foi vinvulado a uma moeda do turismo
	If (nOperation == MODEL_OPERATION_INSERT .Or. nOperation == MODEL_OPERATION_UPDATE) ;
		.And. FwFldGet("G5T_MOEDAF") <> 0 .And. FwFldGet("G5T_MSBLQL") == '2'  
		cAliasQry  := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT G5T_CODIGO,G5T_DESCR
			FROM %table:G5T% G5T 
			WHERE G5T_FILIAL = %xFilial:G5T% AND 
			      G5T_CODIGO <> %Exp:FwFldGet("G5T_CODIGO")% AND
			      G5T_MOEDAF = %Exp:FwFldGet("G5T_MOEDAF")% AND
			      G5T_MSBLQL = '2' AND
			      G5T.%notDel%		  
		EndSql
		
		If (cAliasQry)->(!EOF()) .And. (cAliasQry)->(!BOF())
			Help( , , 'TURA031', , I18N(STR0009 + CRLF + STR0010, {(cAliasQry)->G5T_CODIGO, (cAliasQry)->G5T_DESCR}), 1, 0) 	//'Aten��o'	"Moeda Financeira j� utilizada pela Moeda Turismo (#1) #2."
			lRet := .F.
		Endif 	  
		(cAliasQry)->(DbCloseArea())
	EndIf

	If (lRet .And. nOperation == MODEL_OPERATION_UPDATE .And. FwFldGet("G5T_MOEDAF") <> G5T->G5T_MOEDAF) .Or. nOperation == MODEL_OPERATION_DELETE
		If !TA031VlUso('G3Q')
			lAmarracao := .T.
			cCadastro  := STR0024		// "Registro de Venda -> Item de Venda"
		ElseIf !TA031VlUso('G3R')
			lAmarracao := .T.		
			cCadastro  := STR0013		// "Registro de Venda -> Documento de Reserva"
		ElseIf !TA031VlUso('G44')
			lAmarracao := .T.		
			cCadastro  := STR0020		// "Registro de Venda -> Tarifas"
		ElseIf !TA031VlUso('G45')
			lAmarracao := .T.
			cCadastro  := STR0018		// "Registro de Venda -> Tarifas Cotadas"
		ElseIf !TA031VlUso('G46')
			lAmarracao := .T.		
			cCadastro  := STR0021		// "Registro de Venda -> Taxas"
		ElseIf !TA031VlUso('G47')
			lAmarracao := .T.		
			cCadastro  := STR0022		// "Registro de Venda -> Extras"
		ElseIf !TA031VlUso('G48')
			lAmarracao := .T. 
			cCadastro  := STR0023		// "Registro de Venda -> Acordos"
		ElseIf !TA031VlUso('G4C')
			lAmarracao := .T.
			cCadastro  := STR0025		// "Registro de Venda -> Item Financeiro"
		ElseIf !TA031VlUso('G4D')
			lAmarracao := .T.		
			cCadastro  := STR0026		// "Registro de Venda -> Dados do Cart�o"
		ElseIf !TA031VlUso('G4E')
			lAmarracao := .T.		
			cCadastro  := STR0019		// "Registro de Venda -> Reembolso"
		ElseIf !TA031VlUso('G8G')
			lAmarracao := .T.		
			cCadastro  := STR0016		// "Fatura de Adiantamento"
		ElseIf !TA031VlUso('G8T')
			lAmarracao := .T.		
			cCadastro  := STR0014		// "An�lise de Cr�dito (Turismo)"
		ElseIf !TA031VlUso('G90')
			lAmarracao := .T.		
			cCadastro  := STR0017		// "Fatura de Cart�o"
		EndIf

		If lAmarracao
			Help( , , 'TURA031', , I18N(STR0015 + CRLF + STR0011, {cCadastro}), 1, 0)	// 'Opera��o n�o permitida'  "A moeda est� em uso, verifique o cadastro: #1"	
			lRet := .F.	
		EndIf
	EndIf
	 
EndIf

RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------------------
/*{Protheus.doc} TURMOEDFIN()

VALIDA SE A MOEDA FINANCEIRA EST� VINCULADA A MOEDA DE SISTEMA.

@sample 	TURMOEDFIN()
@Param 	cMoedaTur = Moeda Financeira/Turismo
@return   	lRet = .T. OK 
@author  	Inova��o
@since   	25/04/2016
@version  	P12
*/
//------------------------------------------------------------------------------------------
Function TURMOEDFIN(cMoedaTur,nMoeda)

Local lRet 		:= .T.
Default nMoeda	:=  0

	nMoeda := Posicione("G5T",1, xFilial("G5T") + cMoedaTur, "G5T_MOEDAF" )  

	// Valida moeda financeira x sistema
	If !( nMoeda > 0 .And. nMoeda <= MOEDFIN() )
		Help( , , 'TURMOEDFIN', ,STR0027, 1, 0) //##"Moeda informada n�o vinculada � uma moeda do sistema, vide cadastro de moedas"
		lRet := .F. 
	EndIf

Return(lRet)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} TURRecMoeda()

RECUPERA A TAXA DE CAMBIO COM BASE NA DATA MAIS PROXIMA / MOEDA

@sample 	TURRecMoeda()
@Param 	dData = Data base 
@Param 	cMoeda = Moeda Financeira
@return   	nTxMoeda Taxa de Cambio                       
@author  	Inova��o
@since   	25/04/2016
@version  	P12
*/
//------------------------------------------------------------------------------------------
Function TURRecMoeda(dData,cMoeda)

Local aArea   	:= GetArea()
Local nTxMoeda	:= 0
Local nMoeda

If ValType(dData) == "C"
	dData := CTOD(dData)
EndIf

//=============================================
//�Posiciona arquivo G9L na data mais proxima �
//=============================================
DBSelectArea("G9L") // TAXA DE CAMBIO 
DbSetOrder(1) //G9L_FILIAL+DTOS(G9L_DATA)+G9L_MOEDA

If DbSeek(xFilial("G9L") + DTOS( dData ) + cMoeda )
	nTxMoeda := G9L->G9L_TAXA
Else
	
	G9L->( DbSetOrder(2) )
	G9L->( DbSeek(xFilial("G9L") + cMoeda + DTOS( dData ), .T.) )
	If  G9L->( Eof() )
		G9L->( DbSkip(-1) )
	EndIf

	If cMoeda == G9L->G9L_MOEDA
		nTxMoeda := G9L->G9L_TAXA
	EndIf
	
EndIf

RestArea(aArea)

Return(nTxMoeda)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} IntegDef

Funcao para chamar o Adapter para integracao via Mensagem Unica 

@sample 	IntegDef( cXML, nTypeTrans, cTypeMessage )
@param		cXml - O XML recebido pelo EAI Protheus
			cType - Tipo de transacao
				'0'- para mensagem sendo recebida (DEFINE TRANS_RECEIVE)
				'1'- para mensagem sendo enviada (DEFINE TRANS_SEND) 
			cTypeMessage - Tipo da mensagem do EAI
				'20' - Business Message (DEFINE EAI_MESSAGE_BUSINESS)
				'21' - Response Message (DEFINE EAI_MESSAGE_RESPONSE)
				'22' - Receipt Message (DEFINE EAI_MESSAGE_RECEIPT)
				'23' - WhoIs Message (DEFINE EAI_MESSAGE_WHOIS)
@return  	aRet[1] - Variavel logica, indicando se o processamento foi executado com sucesso (.T.) ou nao (.F.)
			aRet[2] - String contendo informacoes sobre o processamento
			aRet[3] - String com o nome da mensagem Unica deste cadastro                        
@author  	Jacomo Lisa
@since   	01/10/2015
@version  	P12.1.8
/*/
//------------------------------------------------------------------------------------------
Static Function IntegDef( cXML, nTypeTrans, cTypeMessage )

Local aRet := {}

aRet:= TURI031( cXml, nTypeTrans, cTypeMessage )

Return aRet

//------------------------------------------------------------------------------------------
/*{Protheus.doc} TA031VlUso()

VALIDA SE A MOEDA ESTA SENDO UTILIZADA EM OUTRO LUGAR

@sample 	TA031VlUso(cAli)
@Param 	cAli = Alias a ser pesquisado
@return   	lRet = .T. OK 
@author  	Inova��o
@since   	25/10/2016
@version  	P12
*/
//------------------------------------------------------------------------------------------
Static Function TA031VlUso(cAli)
Local lRet := .T.
Local cCodigo := FwFldGet('G5T_CODIGO')
Local cAliasQry := GetNextAlias()
Default cAli := ""

Do Case
	Case cAli == "G3Q"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G3Q% G3Q 
			WHERE G3Q_FILIAL = %xFilial:G3Q% AND (
				  G3Q_MOEDA  = %Exp:cCodigo% OR 
				  G3Q_MOEDCL = %Exp:cCodigo%) AND 
				  G3Q.%notDel%
		EndSql

	Case cAli == "G3R"
		BeginSql Alias cAliasQry
			SELECT Count(*) as nTotReg
			FROM %table:G3R% G3R 
			WHERE G3R_FILIAL = %xFilial:G3R% AND 
				  G3R_MOEDA  = %Exp:cCodigo% AND 
				  G3R.%notDel%			
		EndSql

	Case cAli == "G44"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G44% G44 
			WHERE G44_FILIAL = %xFilial:G44% AND 
				  G44_MOEDFO = %Exp:cCodigo% AND 
				  G44.%notDel%
		EndSql
			
	Case cAli == "G45"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G45% G45 
			WHERE G45_FILIAL = %xFilial:G45% AND 
				  G45_MOEDTC = %Exp:cCodigo% AND 
				  G45.%notDel%
		EndSql	

	Case cAli == "G46"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg 
			FROM %table:G46% G46 
			WHERE G46_FILIAL = %xFilial:G46% AND 
				  G46_MOEDFO = %Exp:cCodigo% AND 
				  G46.%notDel%
		EndSql
				
	Case cAli == "G47"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg 
			FROM %table:G47% G47
			WHERE G47_FILIAL = %xFilial:G47% AND 
				  G47_MOEDFO = %Exp:cCodigo% AND 
				  G47.%notDel%			  
		EndSql
			
	Case cAli == "G48"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G48% G48 
			WHERE G48_FILIAL = %xFilial:G48% AND 
				  G48_MOEDA  = %Exp:cCodigo% AND 
				  G48.%notDel%
		EndSql

	Case cAli == "G4C"
		BeginSql Alias cAliasQry
			SELECT Count(*) as nTotReg
			FROM %table:G4C% G4C  
			WHERE G4C_FILIAL = %xFilial:G4C% AND 
				  G4C_MOEDA  = %Exp:cCodigo% AND 
				  G4C.%notDel%
		EndSql

	Case cAli == "G4D"
		BeginSql Alias cAliasQry
			SELECT Count(*) as nTotReg
			FROM %table:G4D% G4D 
			WHERE G4D_FILIAL = %xFilial:G4D% AND 
				  G4D_MOEDA  = %Exp:cCodigo% AND 
				  G4D.%notDel% 
		EndSql
												
	Case cAli == "G4E"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G4E% G4E  
			WHERE G4E_FILIAL = %xFilial:G4E% AND (
				  G4E_MOEDA  = %Exp:cCodigo% OR 
				  G4E_MOEDFO = %Exp:cCodigo%) AND 
				  G4E.%notDel%
		EndSql 
					
	Case cAli == "G8G"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G8G% G8G  
			WHERE G8G_FILIAL = %xFilial:G8G% AND 
				  G8G_MOEDA  = %Exp:cCodigo% AND 
				  G8G.%notDel%
		EndSql 

	Case cAli == "G8T"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G8T% G8T  
			WHERE G8T_FILIAL = %xFilial:G8T% AND 
				  G8T_MOEDA  = %Exp:cCodigo% AND 
				  G8T.%notDel% 
		EndSql
													
	Case cAli == "G90"
		BeginSql Alias cAliasQry 
			SELECT Count(*) as nTotReg
			FROM %table:G90% G90 
			WHERE G90_FILIAL = %xFilial:G90% AND 
				  G90_MOEDA  = %Exp:cCodigo% AND 
				  G90.%notDel%
		EndSql	
EndCase
			
If (cAliasQry)->nTotReg > 0
	lRet:= .F.
Endif	

(cAliasQry)->(DbCloseArea())
Return lRet