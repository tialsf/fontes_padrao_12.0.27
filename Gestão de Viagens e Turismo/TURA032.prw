#INCLUDE "TURA032.CH" 
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA032()

CADASTRO DE MOTIVO DE CANCELAMENTO - SIGATUR

@sample 	TURA032()
@return                             
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
/*/
//------------------------------------------------------------------------------------------

Function TURA032()
Local oBrowse	:= Nil
 
//------------------------------------------------------------------------------------------
// Cria o Browse 
//------------------------------------------------------------------------------------------
oBrowse := FWMBrowse():New()
oBrowse:SetAlias('G5U')
oBrowse:AddLegend	( "G5U_MSBLQL=='1'", "RED"  , STR0007 )// "Bloqueado"
oBrowse:AddLegend	( "G5U_MSBLQL=='2'", "GREEN" , STR0008 )// "Ativo"
oBrowse:SetDescription(STR0001) // CADASTRO DE MOTIVO DE CANCELAMENTO
oBrowse:Activate()

Return(.T.)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef()

CADASTRO DE MOTIVO DE CANCELAMENTO - DEFINE MODELO DE DADOS (MVC) 

@sample 	TURA032()
@return  	oModel                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStruG5U := FWFormStruct(1,'G5U',/*bAvalCampo*/,/*lViewUsado*/)

oModel := MPFormModel():New('TURA032',/*bPreValidacao*/,,/*bCommit*/,/*bCancel*/)
oModel:AddFields('G5UMASTER',/*cOwner*/,oStruG5U,/*bPreValidacao*/,/*bPosValidacao*/,/*bCarga*/)
oModel:SetPrimaryKey({})
oModel:SetDescription(STR0001) // CADASTRO DE MOTIVO DE CANCELAMENTO

Return(oModel)


//------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef()

CADASTRO DE MOTIVO DE CANCELAMENTO - DEFINE A INTERFACE DO CADASTRO (MVC) 

@sample 	TURA032()
@return   	oView                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function ViewDef()

Local oView
Local oModel   := FWLoadModel('TURA032')
Local oStruG5U := FWFormStruct(2,'G5U')

oView := FWFormView():New()
oView:SetModel(oModel)
oView:AddField('VIEW_G5U', oStruG5U,'G5UMASTER')
oView:CreateHorizontalBox('TELA',100)
oView:SetOwnerView('VIEW_G5U','TELA')

Return(oView)

//------------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef()

CADASTRO DE MOTIVO DE CANCELAMENTO - DEFINE AROTINA (MVC) 

@sample 	TURA032()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	27/05/2015
@version  	P12
*/
//------------------------------------------------------------------------------------------

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0002 ACTION 'PesqBrw' 			OPERATION 1	ACCESS 0 // Buscar
ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA032'	OPERATION 2	ACCESS 0 // Visualizar
ADD OPTION aRotina TITLE STR0004 ACTION 'VIEWDEF.TURA032'	OPERATION 3	ACCESS 0 // Incluir 
ADD OPTION aRotina TITLE STR0005 ACTION 'VIEWDEF.TURA032'	OPERATION 4	ACCESS 0 // Modificar
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA032'	OPERATION 5	ACCESS 0 // Borrar

Return(aRotina)



