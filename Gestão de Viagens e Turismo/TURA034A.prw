#Include 'Protheus.ch'

/*/{Protheus.doc} ViewDef
View estendida do Tura034 com o filtro j� ativo na abertura da view 
@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
Static Function ViewDef()
	Local oView 	:= FwLoadView( 'TURA034' )
	Local oViewG3Q 	:= oView:GetSubView( 'G3Q_ITENS' )
	
	oViewG3Q:SetFilter( .T. )
	
	oView:SetAfterViewActivate({|oView| T34AFilter( oView ) })
	
	
Return oView

/*/{Protheus.doc} T34AFilter
Fun��o para aplicar o filtro na view G3Q 
@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
Static Function T34AFilter( oView )
	Local cIdItem := T39GetIdItem()		
	Local aFilter := { 	{"G3Q_IDITEM","FIELD","Id Item Igual a '"+cIdItem+"'","G3Q_IDITEM== '"+cIdItem+"'","G3Q_IDITEM='"+cIdItem+"'"},;
						{"==","OPERATOR","","",""},;
						{cIdItem,"EXPRESSION","","","",.T.} }
					
	oView:GetSubView('G3Q_ITENS'):oBrowse:AddFilter(	'Item',;
														'G3Q_IDITEM=="'+cIdItem+'"',;
														.T.					,;
														.T.					,;
														/*[ cAlias]*/		,;
														/*[ lFilterAsk]*/	,; 
														aFilter				,;
														/*[ cID]() */ 		)
														
	oView:GetSubView('G3Q_ITENS'):oBrowse:ExecuteFilter( .T. )
															
	Tura34ChgFld()
Return