#INCLUDE "TURA034.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

//------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Criacao do ModelDef

@sample 	Modeldef()
@param		Nenhum
@since		06/11/2017
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel    := Nil
Local oStrG3P   := FWFormStruct(1, 'G3P')
Local oStrG3Q   := FWFormStruct(1, 'G3Q', /*bAvalCampo*/, .F.)
Local oStrG3R   := FWFormStruct(1, 'G3R', /*bAvalCampo*/, .F.)
Local oStrG3S   := FWFormStruct(1, 'G3S', /*bAvalCampo*/, .F.)
Local oStrG3T   := FWFormStruct(1, 'G3T', /*bAvalCampo*/, .F.)
Local oStrG3U   := FWFormStruct(1, 'G3U', /*bAvalCampo*/, .F.)
Local oStrG3V   := FWFormStruct(1, 'G3V', /*bAvalCampo*/, .F.)
Local oStrG3W   := FWFormStruct(1, 'G3W', /*bAvalCampo*/, .F.)
Local oStrG3X   := FWFormStruct(1, 'G3X', /*bAvalCampo*/, .F.)
Local oStrG3Y   := FWFormStruct(1, 'G3Y', /*bAvalCampo*/, .F.)
Local oStrG3Z   := FWFormStruct(1, 'G3Z', /*bAvalCampo*/, .F.)
Local oStrG40   := FWFormStruct(1, 'G40', /*bAvalCampo*/, .F.)
Local oStrG41   := FWFormStruct(1, 'G41', /*bAvalCampo*/, .F.)
Local oStrG42   := FWFormStruct(1, 'G42', /*bAvalCampo*/, .F.)
Local oStrG43   := FWFormStruct(1, 'G43', /*bAvalCampo*/, .F.)
Local oStrG44   := FWFormStruct(1, 'G44', /*bAvalCampo*/, .F.)
Local oStrG45   := FWFormStruct(1, 'G45', /*bAvalCampo*/, .F.)
Local oStrG46   := FWFormStruct(1, 'G46', /*bAvalCampo*/, .F.)
Local oStrG47   := FWFormStruct(1, 'G47', /*bAvalCampo*/, .F.)
Local oStrG4A   := FWFormStruct(1, 'G4A', /*bAvalCampo*/, .F.)
Local oStrG4B   := FWFormStruct(1, 'G4B', /*bAvalCampo*/, .F.)
Local oStrG48A  := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F.)
Local oStrG48B  := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F.)
Local oStrG49   := FWFormStruct(1, 'G49', /*bAvalCampo*/, .F.)
Local oStrG4E   := FWFormStruct(1, 'G4E', /*bAvalCampo*/, .F.)
Local oStrG4CA  := FWFormStruct(1, 'G4C', /*bAvalCampo*/, .F.)
Local oStrG4CB  := FWFormStruct(1, 'G4C', /*bAvalCampo*/, .F.)
Local oStrG4D   := FWFormStruct(1, 'G4D', /*bAvalCampo*/, .F.)
Local oStrG9KA  := FWFormStruct(1, 'G9K', /*bAvalCampo*/, .F.)
Local oStrG9KB  := FWFormStruct(1, 'G9K', /*bAvalCampo*/, .F.)
Local oStrEntid := FWFormModelStruct():New()

//Realiza altera��es no Metadados que n�o constam no dicion�rio
TA034StruM(/*oStrG3P*/, oStrG3Q, oStrG3R, /*oStrG3S*/, /*oStrG3T*/, /*oStrG3U*/, /*oStrG3V*/, /*oStrG3W*/, /*oStrG3X*/, /*oStrG3Y*/, /*oStrG3Z*/, /*oStrG40*/, /*oStrG41*/, /*oStrG42*/, /*oStrG43*/, /*oStrG44*/, /*oStrG45*/, /*oStrG46*/, /*oStrG47*/, oStrG4A, /*oStrG4B*/, oStrG48A, oStrG48B, /*oStrG49*/, oStrG4E, oStrG4CA, oStrG4CB, oStrEntid, /*oStrG4D*/, oStrG9KA, oStrG9KB)

//Inicializa o modelo
oModel := MPFormModel():New('TURA034B', /*bPreValidacao*/, {|oModel| Tur34TdOk(oModel, .T., .T.)} /*bPosValidacao*/, {|oModel| T034GRAVA(oModel)} /*bCommit*/, /*bCancel*/)

oModel:AddFields("G3P_FIELDS", /*cOwner*/, oStrG3P, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/)

oModel:AddGrid("G3Q_ITENS", "G3P_FIELDS", oStrG3Q, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3Q_ITENS", {{"G3Q_FILIAL", "xFilial('G3Q')"}, {"G3Q_NUMID", "G3P_NUMID"}, {"G3Q_CONINU", "''"}}, G3Q->(IndexKey(1)))

oModel:AddGrid("G3R_ITENS", "G3Q_ITENS", oStrG3R, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3R_ITENS", {{"G3R_FILIAL", "xFilial('G3R')"}, {"G3R_NUMID", "G3Q_NUMID"}, {"G3R_IDITEM", "G3Q_IDITEM"}, {"G3R_NUMSEQ", "G3Q_NUMSEQ"}, {"G3R_CONINU", "''"}}, G3R->(IndexKey(1)))

oModel:AddGrid("G3S_ITENS", "G3Q_ITENS", oStrG3S, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3S_ITENS", {{"G3S_FILIAL", "xFilial('G3S')"}, {"G3S_NUMID", "G3Q_NUMID"}, {"G3S_IDITEM", "G3Q_IDITEM"}, {"G3S_NUMSEQ", "G3Q_NUMSEQ"}, {"G3S_CONINU", "''"}}, G3S->(IndexKey(1)))

oModel:AddGrid("G4B_ITENS", "G3S_ITENS", oStrG4B, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4B_ITENS", {{"G4B_FILIAL", "xFilial('G4B')"}, {"G4B_NUMID", "G3S_NUMID"}, {"G4B_IDITEM", "G3S_IDITEM"}, {"G4B_NUMSEQ", "G3S_NUMSEQ"}, {"G4B_CLIENT", "G3Q_CLIENT"}, {"G4B_CODPAX", "G3S_CODPAX"}, {"G4B_CONINU", "''"}}, G4B->(IndexKey(1))) 
oModel:GetModel("G4B_ITENS"):SetOnlyQuery(.T.)

oModel:AddFields("ENTIDADE", "G3Q_ITENS", oStrEntid, /*bPreValidacao*/, /*bPosValidacao*/, {|oFieldModel, lCopy| Tur34Ent(oFieldModel, lCopy)}/*bCarga*/)
oModel:GetModel("ENTIDADE"):SetDescription("Rateio")
oModel:SetRelation("ENTIDADE", {{"G3Q_FILIAL", "G3Q_FILIAL"}, {"G3Q_NUMID", "G3Q_NUMID"}, {"G3Q_IDITEM", "G3Q_IDITEM"}, {"G3Q_NUMSEQ", "G3Q_NUMSEQ"}}, G3Q->(IndexKey(1)))

oModel:AddGrid("G4A_ITENS", "G3Q_ITENS", oStrG4A, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4A_ITENS", {{"G4A_FILIAL", "xFilial('G4A')"}, {"G4A_NUMID", "G3Q_NUMID"}, {"G4A_IDITEM", "G3Q_IDITEM"}, {"G4A_NUMSEQ", "G3Q_NUMSEQ"}, {"G4A_CLIENT", "G3Q_CLIENT"}, {"G4A_CONINU", "''"}}, G4A->(IndexKey(1)))

oModel:AddGrid("G3T_ITENS", "G3Q_ITENS", oStrG3T, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3T_ITENS", {{"G3T_FILIAL", "xFilial('G3T')"}, {"G3T_NUMID", "G3Q_NUMID"}, {"G3T_IDITEM", "G3Q_IDITEM"}, {"G3T_NUMSEQ", "G3Q_NUMSEQ"}, {"G3T_CONINU", "''"}}, G3T->(IndexKey(1)))

oModel:AddGrid("G3U_ITENS", "G3Q_ITENS", oStrG3U, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3U_ITENS", {{"G3U_FILIAL", "xFilial('G3U')"}, {"G3U_NUMID", "G3Q_NUMID"}, {"G3U_IDITEM", "G3Q_IDITEM"}, {"G3U_NUMSEQ", "G3Q_NUMSEQ"}, {"G3U_CONINU", "''"}}, G3U->(IndexKey(1)))

oModel:AddGrid("G3V_ITENS", "G3Q_ITENS", oStrG3V, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3V_ITENS", {{"G3V_FILIAL", "xFilial('G3V')"}, {"G3V_NUMID", "G3Q_NUMID"}, {"G3V_IDITEM", "G3Q_IDITEM"}, {"G3V_NUMSEQ", "G3Q_NUMSEQ"}, {"G3V_CONINU", "''"}}, G3V->(IndexKey(1)))

oModel:AddGrid("G3W_ITENS", "G3Q_ITENS", oStrG3W, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3W_ITENS", {{"G3W_FILIAL", "xFilial('G3W')"}, {"G3W_NUMID", "G3Q_NUMID"}, {"G3W_IDITEM", "G3Q_IDITEM"}, {"G3W_NUMSEQ", "G3Q_NUMSEQ"}, {"G3W_CONINU", "''"}}, G3W->(IndexKey(1)))

oModel:AddGrid("G3X_ITENS", "G3Q_ITENS", oStrG3X, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3X_ITENS", {{"G3X_FILIAL", "xFilial('G3X')"}, {"G3X_NUMID", "G3Q_NUMID"}, {"G3X_IDITEM", "G3Q_IDITEM"}, {"G3X_NUMSEQ", "G3Q_NUMSEQ"}, {"G3X_CONINU", "''"}}, G3X->(IndexKey(1)))

oModel:AddGrid("G3Y_ITENS", "G3Q_ITENS", oStrG3Y, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3Y_ITENS", {{"G3Y_FILIAL", "xFilial('G3Y')"}, {"G3Y_NUMID", "G3Q_NUMID"}, {"G3Y_IDITEM", "G3Q_IDITEM"}, {"G3Y_NUMSEQ", "G3Q_NUMSEQ"}, {"G3Y_CONINU", "''"}}, G3Y->(IndexKey(1)))

oModel:AddGrid("G3Z_ITENS", "G3Q_ITENS", oStrG3Z, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3Z_ITENS", {{"G3Z_FILIAL", "xFilial('G3Z')"}, {"G3Z_NUMID", "G3Q_NUMID"}, {"G3Z_IDITEM", "G3Q_IDITEM"}, {"G3Z_NUMSEQ", "G3Q_NUMSEQ"}, {"G3Z_CONINU", "''"}}, G3Z->(IndexKey(1)))

oModel:AddGrid("G40_ITENS", "G3Q_ITENS", oStrG40, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G40_ITENS", {{"G40_FILIAL", "xFilial('G40')"}, {"G40_NUMID", "G3Q_NUMID"}, {"G40_IDITEM", "G3Q_IDITEM"}, {"G40_NUMSEQ", "G3Q_NUMSEQ"}, {"G40_CONINU", "''"}}, G40->(IndexKey(1)))

oModel:AddGrid("G41_ITENS", "G3Q_ITENS", oStrG41, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G41_ITENS", {{"G41_FILIAL", "xFilial('G41')"}, {"G41_NUMID", "G3Q_NUMID"}, {"G41_IDITEM", "G3Q_IDITEM"}, {"G41_NUMSEQ", "G3Q_NUMSEQ"}, {"G41_CONINU", "''"}}, G41->(IndexKey(1)))

oModel:AddGrid("G42_ITENS", "G3Q_ITENS", oStrG42, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G42_ITENS", {{"G42_FILIAL", "xFilial('G42')"}, {"G42_NUMID", "G3Q_NUMID"}, {"G42_IDITEM", "G3Q_IDITEM"}, {"G42_NUMSEQ", "G3Q_NUMSEQ"}, {"G42_CONINU", "''"}}, G42->(IndexKey(1)))

oModel:AddGrid("G43_ITENS", "G3Q_ITENS", oStrG43, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G43_ITENS", {{"G43_FILIAL", "xFilial('G43')"}, {"G43_NUMID", "G3Q_NUMID"}, {"G43_IDITEM", "G3Q_IDITEM"}, {"G43_NUMSEQ", "G3Q_NUMSEQ"}, {"G43_CONINU", "''"}}, G43->(IndexKey(1)))

oModel:AddGrid("G44_ITENS", "G3Q_ITENS", oStrG44, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G44_ITENS", {{"G44_FILIAL", "xFilial('G44')"}, {"G44_NUMID", "G3Q_NUMID"}, {"G44_IDITEM", "G3Q_IDITEM"}, {"G44_NUMSEQ", "G3Q_NUMSEQ"}, {"G44_CONINU", "''"}}, G44->(IndexKey(1)))

oModel:AddGrid("G45_ITENS", "G3Q_ITENS", oStrG45, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G45_ITENS", {{"G45_FILIAL", "xFilial('G45')"}, {"G45_NUMID", "G3Q_NUMID"}, {"G45_IDITEM", "G3Q_IDITEM"}, {"G45_NUMSEQ", "G3Q_NUMSEQ"}}, G45->(IndexKey(1)))

oModel:AddGrid("G46_ITENS", "G3Q_ITENS", oStrG46, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G46_ITENS", {{"G46_FILIAL", "xFilial('G46')"}, {"G46_NUMID", "G3Q_NUMID"}, {"G46_IDITEM", "G3Q_IDITEM"}, {"G46_NUMSEQ", "G3Q_NUMSEQ"}, {"G46_CONINU", "''"}}, G46->(IndexKey(1)))

oModel:AddGrid("G47_ITENS", "G3Q_ITENS", oStrG47, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G47_ITENS", {{"G47_FILIAL", "xFilial('G47')"}, {"G47_NUMID", "G3Q_NUMID"}, {"G47_IDITEM", "G3Q_IDITEM"}, {"G47_NUMSEQ", "G3Q_NUMSEQ"}, {"G47_CONINU", "''"}}, G47->(IndexKey(1)))

oModel:AddGrid("G49_ITENS", "G3Q_ITENS", oStrG49, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G49_ITENS", {{"G49_FILIAL", "xFilial('G49')"}, {"G49_NUMID", "G3Q_NUMID"}, {"G49_IDITEM", "G3Q_IDITEM"}, {"G49_NUMSEQ", "G3Q_NUMSEQ"}, {"G49_CONINU", "''"}}, G49->(IndexKey(1)))

oModel:AddGrid("G4E_ITENS", "G3Q_ITENS", oStrG4E, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4E_ITENS", {{"G4E_FILIAL", "xFilial('G4E')"}, {"G4E_NUMID", "G3Q_NUMID"}, {"G4E_IDITEM", "G3Q_IDITEM"}, {"G4E_NUMSEQ", "G3Q_NUMSEQ"}, {"G4E_CONINU", "''"}}, G4E->(IndexKey(1)))

oModel:AddGrid("G4D_ITENS", "G3Q_ITENS", oStrG4D, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4D_ITENS", {{"G4D_FILIAL", "xFilial('G4D')"}, {"G4D_NUMID", "G3Q_NUMID"}, {"G4D_IDITEM", "G3Q_IDITEM"}, {"G4D_NUMSEQ", "G3Q_NUMSEQ"}, {"G4D_CONINU", "''"}}, G4D->(IndexKey(1)))

oModel:AddGrid("G48A_ITENS", "G3Q_ITENS", oStrG48A, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G48A_ITENS", {{"G48_FILIAL", "xFilial('G48')"}, {"G48_NUMID", "G3Q_NUMID"}, {"G48_IDITEM", "G3Q_IDITEM"}, {"G48_NUMSEQ", "G3Q_NUMSEQ"}, {"G48_CLIFOR", "'1'"}, {"G48_CONINU", "''"}}, G48->(IndexKey(1)))

oModel:AddGrid("G48B_ITENS", "G3Q_ITENS", oStrG48B, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G48B_ITENS", {{"G48_FILIAL", "xFilial('G48')"}, {"G48_NUMID", "G3Q_NUMID"}, {"G48_IDITEM", "G3Q_IDITEM"}, {"G48_NUMSEQ", "G3Q_NUMSEQ"}, {"G48_CLIFOR", "'2'"}, {"G48_CONINU", "''"}}, G48->(IndexKey(1)))

oModel:AddGrid("G4CA_ITENS", "G3Q_ITENS", oStrG4CA, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4CA_ITENS", {{"G4C_FILIAL", "xFilial('G4C')"}, {"G4C_NUMID", "G3Q_NUMID"}, {"G4C_IDITEM", "G3Q_IDITEM"}, {"G4C_NUMSEQ", "G3Q_NUMSEQ"}, {"G4C_CLIFOR", "'1'"}, {"G4C_CONINU", "''"}}, G4C->(IndexKey(1)))

oModel:AddGrid("G4CB_ITENS", "G3Q_ITENS", oStrG4CB, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G4CB_ITENS", {{"G4C_FILIAL", "xFilial('G4C')"}, {"G4C_NUMID", "G3Q_NUMID"}, {"G4C_IDITEM", "G3Q_IDITEM"}, {"G4C_NUMSEQ", "G3Q_NUMSEQ"}, {"G4C_CLIFOR", "'2'"}, {"G4C_CONINU", "''"}}, G4C->(IndexKey(1)))

oModel:AddGrid("G9KA_ITENS", "G3Q_ITENS", oStrG9KA, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G9KA_ITENS", {{"G9K_FILIAL", "xFilial('G9K')"}, {"G9K_NUMID", "G3Q_NUMID"}, {"G9K_IDITEM", "G3Q_IDITEM"}, {"G9K_NUMSEQ", "G3Q_NUMSEQ"}, {"G9K_CLIFOR", "'1'"}, {"G9K_CONINU", "''"}}, G9K->(IndexKey(1)))

oModel:AddGrid("G9KB_ITENS", "G3Q_ITENS", oStrG9KB, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G9KB_ITENS", {{"G9K_FILIAL", "xFilial('G9K')"}, {"G9K_NUMID", "G3Q_NUMID"}, {"G9K_IDITEM", "G3Q_IDITEM"}, {"G9K_NUMSEQ", "G3Q_NUMSEQ"}, {"G9K_CLIFOR", "'2'"}, {"G9K_CONINU", "''"}}, G9K->(IndexKey(1)))

//Define todos os GRIDs como opcionais, para inclusao apenas do RV
oModel:GetModel('G3Q_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3R_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3S_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3T_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3U_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3V_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3W_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3X_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3Y_ITENS' ):SetOptional(.T.)
oModel:GetModel('G3Z_ITENS' ):SetOptional(.T.)
oModel:GetModel('G40_ITENS' ):SetOptional(.T.)
oModel:GetModel('G41_ITENS' ):SetOptional(.T.)
oModel:GetModel('G42_ITENS' ):SetOptional(.T.)
oModel:GetModel('G43_ITENS' ):SetOptional(.T.)
oModel:GetModel('G44_ITENS' ):SetOptional(.T.)
oModel:GetModel('G45_ITENS' ):SetOptional(.T.)
oModel:GetModel('G46_ITENS' ):SetOptional(.T.)
oModel:GetModel('G47_ITENS' ):SetOptional(.T.)
oModel:GetModel('G49_ITENS' ):SetOptional(.T.)
oModel:GetModel('G4B_ITENS' ):SetOptional(.T.)
oModel:GetModel('G4A_ITENS' ):SetOptional(.T.)
oModel:GetModel('ENTIDADE'  ):SetOptional(.T.)
oModel:GetModel('G4E_ITENS' ):SetOptional(.T.)
oModel:GetModel('G4D_ITENS' ):SetOptional(.T.)
oModel:GetModel('G48A_ITENS'):SetOptional(.T.)
oModel:GetModel('G48B_ITENS'):SetOptional(.T.)
oModel:GetModel('G4CA_ITENS'):SetOptional(.T.)
oModel:GetModel('G4CB_ITENS'):SetOptional(.T.)
oModel:GetModel('G9KA_ITENS'):SetOptional(.T.)
oModel:GetModel('G9KB_ITENS'):SetOptional(.T.)

oModel:SetOnDemand(.T.)

Return(oModel)