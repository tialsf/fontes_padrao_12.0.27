#INCLUDE "TURA035.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWADAPTEREAI.CH"

Static oCacheHash := tHashMap():New()


//============================================================================================================
/*/{Protheus.doc} TURA035()

CADASTRO DE PAR�METRO FISCAL E FINANCEIRO DE RECEITA - SIGATUR

@sample 	TURA035()
@return                             
@author  	Fanny Mieko Suzuki
@since   	25/06/2015
@version  	P12
@return	.T.
/*/
//============================================================================================================

Function TURA035()

Local oBrowse	:= Nil

		// CRIA O BROWSE
		oBrowse:= FWMBrowse():New()
		oBrowse:SetAlias('G6A')
		oBrowse:SetDescription(STR0001)  //"Par�metro Fiscal e Financeiro de Receita"
		oBrowse:Activate()

Return .T.                   
  
  
//============================================================================================================
/*/{Protheus.doc} MENUDEF()

MENUDEF - CADASTRO DE PAR�METRO FISCAL E FINANCEIRO DE RECEITA - SIGATUR

@sample 	TURA035()
@return                             
@author  	Fanny Mieko Suzuki
@since   	25/06/2015
@version  	P12
@return	aRotina
/*/
//============================================================================================================

Static Function MENUDEF()     

Local aRotina := {}
Local aArea   := GetArea()

		// ADICIONA MENU
		ADD OPTION aRotina TITLE STR0005  ACTION 'PesqBrw' 			OPERATION 1  ACCESS 0 // "Pesquisar"
		ADD OPTION aRotina TITLE STR0006  ACTION 'VIEWDEF.TURA035'	OPERATION 2  ACCESS 0 // "Visualizar"
		ADD OPTION aRotina TITLE STR0007  ACTION 'VIEWDEF.TURA035'	OPERATION 3  ACCESS 0 // "Incluir"
		ADD OPTION aRotina TITLE STR0008  ACTION 'VIEWDEF.TURA035'	OPERATION 4  ACCESS 0 // "Alterar"
		ADD OPTION aRotina TITLE STR0009  ACTION 'VIEWDEF.TURA035'	OPERATION 5  ACCESS 0 // "Excluir"
	
	RestArea(aArea)
	
Return(aRotina) 
 
//============================================================================================================
/*/{Protheus.doc} TURA035()

MODELDEF - CADASTRO DE PAR�METRO FISCAL E FINANCEIRO DE RECEITA - SIGATUR

@sample 	TURA035()
@return                             
@author  	Fanny Mieko Suzuki
@since   	25/06/2015
@version  	P12
@return	oModel
/*/
//============================================================================================================

Static Function MODELDEF()

// CRIA ESTRUTURA DO OBJETO
Local oModel      := Nil
Local oStructG6A  := Nil
Local oStructG6B  := Nil
Local auMovStatus := {}
Local xAux        := Nil

	// MONTA A ESTRUTURA DO FORMUL�RIO COM BASE NO DICION�RIO DE DADOS
	oStructG6A := FWFormStruct(1,"G6A",/*bAvalCampo*/,/*lViewUsado*/)
	oStructG6B := FWFormStruct(1,"G6B",/*bAvalCampo*/,/*lViewUsado*/)
	
	// INCLUI GATILHO PARA O CAMPO G6A_CDCLAS
	xAux := FwStruTrigger( 'G6A_CDCLAS', 'G6A_CDCLAS', 'Ta035Class()', .F. )
	oStructG6A:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])
	
	// CRIA OBJETO DO MODELO DE DADOS
	oModel:= MPFormModel():New('TURA035',,,/*CANCEL*/,/*CANCEL*/)
	                                          
	// ADICIONA AO MODELO ESTRUTURA DE FORMUL�RIO DE EDI��O POR CAMPOS
	oModel:AddFields('G6AMASTER',/*cOwner*/,oStructG6A,/*Criptog()*/,{||TR035PrCmt()},/*bCarga*/)
	
	// ADICIONA AO MODELO ESTRUTURA DE FORMUL�RIO DE EDI��O DE GRID
	oModel:AddGrid( 'G6BDETAIL', 'G6AMASTER', oStructG6B, /*bLinePre*/,{|oModel|TR035VlLine(oModel)},/*bPosValidacao*/ , /*BLoad*/ )
	
	// DEFINE A INSER��O DE UM ITEM NO GRID
	oModel:GetModel( 'G6BDETAIL' ):SetOptional( .T. )
	
	// FAZ RELACIONAMENTO ENTRE COMPONENTES DO MODEL
	oModel:SetRelation( 'G6BDETAIL', { { 'G6B_FILIAL', 'xFilial("G6B")' }, { 'G6B_CODIGO', 'G6A_CODIGO' } }, G6B->( IndexKey( 1 ) ) )// G6B_FILIAL+G6B_CODIGO+G6B_ITEM
	
	// ADICIONA A DESCRI��O DO MODELO DE DADOS
	oModel:SetDescription(STR0002) // "Par�metro Fiscal e Financeiro de Receita - Item"
	
	// Define a inser��o de um item no grid como 
	oModel:GetModel( 'G6BDETAIL' ):SetOptional( .F. )
	
	
	// ADICIONA A DESCRI��O DO COMPONENTE DO MODELO DE DADOS
	oModel:GetModel( 'G6AMASTER' ):SetDescription( STR0001 ) // "Par�metro Fiscal e Financeiro de Receita"
	oModel:GetModel( 'G6BDETAIL' ):SetDescription( STR0002 ) // "Par�metro Fiscal e Financeiro de Receita - Item"
	
	// ALTERA PROPRIEDADE DO CAMPO PARA N�O OBRIGAT�RIO
	oStructG6B:SetProperty( 'G6B_CODIGO' ,	MODEL_FIELD_OBRIGAT,.F.)
	
	// INCLUI VALIDA��O NO CAMPO
	oStructG6B:SetProperty( 'G6B_ALQIMP' ,	MODEL_FIELD_VALID,{|oModel| At035Valida(oModel)})
			
	oModel:GetModel("G6BDETAIL"):SetUniqueLine({"G6B_CODIGO","G6B_ITEM"})
	
	oModel:SetActivate({|oModel| TA35Active( oModel ) })
	
	
Return(oModel)


//============================================================================================================
/*/{Protheus.doc} VIEWDEF()

CADASTRO DE PAR�METRO FISCAL E FINANCEIRO DE RECEITA - SIGATUR

@sample 	TURA035()
@return                             
@author  	Fanny Mieko Suzuki
@since   	25/06/2015
@version  	P12
@return	oView
/*/
//============================================================================================================

Static Function VIEWDEF()

Local oView  		:= Nil
Local oModel  	:= FWLoadModel("TURA035")
Local oStructG6A 	:= Nil
Local oStructG6B 	:= Nil
Local aButtons  	:= {}
Local aUsrBut   	:= {} 
Local nX			:= 0

		
	// CRIA OBJETO DE MODELO DE DADOS BASEADO NA MODELDEF
	oStructG6A 	:= FWFormStruct(2,'G6A')
	oStructG6B 	:= FWFormStruct(2,'G6B')
	
	// REMOVE CAMPOS DO FIELD
	oStructG6B:RemoveField("G6B_CODIGO")
		
	// CRIA OBJETO DA VIEW
	oView:= FWFormView():New()
	
	// DEFINE QUAL MODELO DE DADOS SER� UTILIZADO
	oView:SetModel(oModel)
	
	// ADICIONA VIEW DE CONTROLE DO TIPO FORMFIELDS
	oView:AddField('VIEW_G6A', oStructG6A,'G6AMASTER')
	
	// ADICIONA VIEW DE CONTROLE DO TIPO FORMGRID
	oView:AddGrid('VIEW_G6B', oStructG6B, 'G6BDETAIL' )
	
	// CRIA BOX HORIZONTAL
	oView:CreateHorizontalBox( 'SUPERIOR', 40 )
	oView:CreateHorizontalBox( 'INFERIOR', 60 )
	
	// AUTO INCREMENTA OS VALORES DO ITEM E DO NIVEL
	oView:AddIncrementField( 'G6BDETAIL', 'G6B_ITEM' )
	
	// RELACIONA O ID DA VIEW COM BOX DE EXIBI��O
	oView:SetOwnerView( 'VIEW_G6A', 'SUPERIOR' )
	oView:SetOwnerView( 'VIEW_G6B', 'INFERIOR' )

	
Return oView

//============================================================================================================
/*/{Protheus.doc} TR035PrCmt()

VALIDA SE FOI SELECIONADO ALGUM SEGMENTO PARA ESTE GRUPO  DE PRODUTO

@sample 	TR035PrCmt()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	18/06/2015
@version  	P12
@return 	lRet
/*/
//============================================================================================================

Static Function TR035PrCmt()

Local nOperation	:= 0
Local oModel		:= FwModelActive()
Local oG6AMASTER	:= oModel:GetModel("G6AMASTER")
Local oG6BDETAIL	:= oModel:GetModel("G6BDETAIL")
Local lLazer		:= ""
Local lCorpor		:= ""
Local lEvento		:= ""
Local cValid 		:= ""
Local lRet 		:= .T.
Local cQuery		:= ""
Local cAliasG6A	:= GetNextAlias()
Local aAreaG6A 	:= GetArea()
Local cCodigo		:= ""
Local cCdClas		:= ""
Local cCdProd		:= ""
Local nX			:= 0
Local nLineAtu	:= 0

// VERIFICAR QUAL O TIPO DE OPERA��O ESTA SENDO REALIZADA
nOperation 	:= oG6AMASTER:GetOperation()

cCorpor 	:= IIF(FwFldGet("G6A_CORP"),"T","F")
cEvento 	:= IIF(FwFldGet("G6A_EVENTO"),"T","F")
cLazer 	:= IIF(FwFldGet("G6A_LAZER"),"T","F")
cCodigo	:= FwFldGet("G6A_CODIGO")
cCdClas	:= FwFldGet("G6A_CDCLAS")
cCdProd	:= FwFldGet("G6A_CODPRD")

// VERIFICA SE � INCLUS�O OU ALTERA��O?
If nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 

	// VALIDA SE NENHUM SEGMENTO FOI SELECIONADO PARA ESTA ENTIDADE
	If cCorpor == "F" .AND. cEvento == "F" .AND. cLazer == "F"
		Help( "TR035PrCmt", 1, STR0003, , STR0010, 1, 0) // "Aten��o" - "� necess�rio selecionar pelo menos um segmento para este Grupo de Produto."
		lRet:= .F.
	// VERIFICA SE J� EXISTE ALGUM REGISTRO QUE UTILIZE ESTA CLASSE E QUE TENHA O MESMO SEGMENTO SELECIONADO
	Else
		dbSelectArea("G6A")
		G6A->(dbSetOrder(1))
		cQuery  := "SELECT G6A_CODIGO, G6A_CDCLAS, G6A_CORP, G6A_EVENTO, G6A_LAZER "
		cQuery  += " FROM " + RetSqlName( 'G6A' )
		cQuery  += " WHERE D_E_L_E_T_ = ' ' "  
		cQuery  += " AND G6A_FILIAL ='" + FWXFILIAL("G6A")+ "' "   
		cQuery  += " AND G6A_CODIGO <> '" + cCodigo + "' "  
		cQuery  += " AND G6A_CDCLAS = '" + cCdClas + "' "  
		If cCdClas == "V01"
			cQuery  += " AND G6A_CODPRD = '" + cCdProd + "' "
		EndIf
		cQuery += "AND ("
		If cCorpor == "T"
			cQuery  += "G6A_CORP = 'T' " 
		EndIf
		If cEvento == "T"
			If cCorpor == "T"
				cQuery  += " OR G6A_EVENTO = 'T' " 
			Else
				cQuery  += " G6A_EVENTO = 'T' " 
			EndIf
		EndIf
		If cLazer == "T"
			If cCorpor == "T" .Or. cEvento == "T"
				cQuery  += " OR G6A_LAZER = 'T' "
			Else
				cQuery  += " G6A_LAZER = 'T' "
			EndIf
		EndIf
		cQuery  += ")"
			
		dbUseArea(.T.,"TOPCONN", TCGenQry(,,cQuery),cAliasG6A, .F., .T.)
		dbSelectArea(cAliasG6A)
		
		If (cAliasG6A)->(!Eof())
 			If cCdClas == 'V01'
 				Help( "TR035PrCmt", 1, STR0003, , STR0015, 1, 0) // "Aten��o" - "N�o � poss�vel ter dois registros com a mesma Classe / Segmento / Produto selecionados."
			Else
				Help( "TR035PrCmt", 1, STR0003, , STR0011, 1, 0) // "Aten��o" - "N�o � poss�vel ter dois registros com a mesma Classe / Segmento selecionados."
			EndIf
			lRet  := .F.
		EndIf   
		
		(cAliasG6A)->(dbCloseArea())
		
		RestArea(aAreaG6A) 
		
		If lRet .And. cCdClas == 'V01'
					
			nLineAtu := oG6BDETAIL:GetLine()
			
			For nX := 1 To oG6BDETAIL:Length()
				oG6BDETAIL:GoLine(nX)
				If !oG6BDETAIL:IsDeleted() .And. !Empty(oG6BDETAIL:GetValue("G6B_CODGRP"))
					lRet := .F.
					Help( "TR035ComGrp", 1, STR0003, , STR0014, 1, 0) // "Aten��o" //"Par�metros de Venda de Servi�o Pr�prio (V01) n�o podem ter Grupos de Produtos informados."
					Exit
				EndIf
			Next
			
			oG6BDETAIL:GoLine(nLineAtu)
			
		EndIf
		
	EndIf
EndIf

	
Return lRet


//============================================================================================================
/*/{Protheus.doc} TR035VlLine(oG6BDETAIL)

VALIDA SE TIPO DE FORNECEDOR = AMBOS E N�O DEIXA INCLUIR OUTRO GRUPO DE PRODUTO IGUAL

@sample 	TR035VlLine()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	18/06/2015
@version  	P12
@return 	lRet
/*/
//============================================================================================================

Static Function TR035VlLine(oModel)

Local lRet 		:= .T.
Local cCodigo		:= ""
Local cCodgrp		:= ""
Local cTpforn		:= ""
Local cItem		:= ""
Local nX			:= 0
Local nOperation	:= 0
Local oMdlDet		:= oModel

// VERIFICAR QUAL O TIPO DE OPERA��O ESTA SENDO REALIZADA
nOperation 	:= oMdlDet:GetOperation()

cCodigo 	:= FwFldGet("G6A_CODIGO")
cCodgrp 	:= FwFldGet("G6B_CODGRP")
cTpforn 	:= FwFldGet("G6B_TPFORN")
cItem		:= FwFldGet("G6B_ITEM")

// VERIFICA SE � INCLUS�O OU ALTERA��O?
IF nOperation == MODEL_OPERATION_INSERT .OR. nOperation == MODEL_OPERATION_UPDATE 

	FOR nX := 1 To oMdlDet:Length()
		oMdlDet:GoLine(nX)
		// VERIFICA SE N�O � DELETADO
		IF !oMdlDet:IsDeleted()
			IF cTpforn == "3" .AND.(AllTrim(oMdlDet:GetValue("G6B_CODGRP"))) == cCodgrp .AND. (AllTrim(oMdlDet:GetValue("G6B_ITEM"))) <> cItem
				Help( "TR035VlLine", 1, STR0003, , STR0013, 1, 0) // "Aten��o" - "N�o � poss�vel inserir este Grupo de Produto como AMBOS, pois j� existe este Grupo de Produto cadastrado!"
				lRet:= .F.
				EXIT	
			ELSE// SE ITENS FOREM = 	
				IF (AllTrim(oMdlDet:GetValue("G6B_CODGRP"))) == cCodgrp .AND. (AllTrim(oMdlDet:GetValue("G6B_TPFORN"))) == "3" .AND. (AllTrim(oMdlDet:GetValue("G6B_ITEM"))) <> cItem
					Help( "TR035VlLine", 1, STR0003, , STR0012, 1, 0) // "Aten��o" - "J� existe este Grupo de Produto, aonde o Tipo de Fornecedor esta cadastrado como AMBOS!"
					lRet:= .F.
					EXIT	
				ELSEIF (AllTrim(oMdlDet:GetValue("G6B_CODGRP"))) == cCodgrp .AND. (AllTrim(oMdlDet:GetValue("G6B_TPFORN"))) == cTpforn .AND. (AllTrim(oMdlDet:GetValue("G6B_ITEM"))) <> cItem
					Help( "TR035VlLine", 1, STR0003, , STR0013, 1, 0) // "Aten��o" - "J� existe este Grupo de Produto cadastrado!"
					lRet:= .F.
					EXIT	
				ENDIF
				
			ENDIF	
			
		ENDIF	
		
	NEXT nX	
	
ENDIF		
			
	
Return lRet


//============================================================================================================
/*/{Protheus.doc} At035Valida(oMdl)

VALIDA DE VALOR DA ALIQUOTA � POSITIVO E MENOR QUE 100

@sample 	A035ValSeg()
@return  	aRotina                       
@author  	Fanny Mieko Suzuki
@since   	18/06/2015
@version  	P12
@return 	lRet
/*/
//============================================================================================================


Static Function At035Valida(oMdl)
Local lRet := .T.

IF !(oMdl:GetValue('G6B_ALQIMP') >= 0 .AND. oMdl:GetValue('G6B_ALQIMP') <= 100)
	lRet := .F.
ENDIF

RETURN lRet


//============================================================================================================
/*/{Protheus.doc} Ta035Class

Fun��o para Gatilho dos segmentos de Neg�cio conforme escolha da Classifica��o do Acordo

@since 17/03/2016
/*/
//============================================================================================================

Function Ta035Class()
Local oModel    	:= FwModelActive()
Local oModelG6A	:= oModel:GetModel("G6AMASTER")
Local xRetorno  	:= &(ReadVar())

oModelG6A:GetStruct():SetProperty("G6A_CORP"		,MODEL_FIELD_WHEN,{||.T.})
oModelG6A:GetStruct():SetProperty("G6A_EVENTO"	,MODEL_FIELD_WHEN,{||.T.})
oModelG6A:GetStruct():SetProperty("G6A_LAZER"		,MODEL_FIELD_WHEN,{||.T.})

If SubStr(xRetorno,1,1) == "M" //Acordos de Metas
	oModelG6A:SetValue("G6A_CORP", .T.)
	oModelG6A:SetValue("G6A_EVENTO", .T.)
	oModelG6A:SetValue("G6A_LAZER", .T.)
	oModelG6A:GetStruct():SetProperty("G6A_CORP"		,MODEL_FIELD_WHEN,{||.F.})
	oModelG6A:GetStruct():SetProperty("G6A_EVENTO"	,MODEL_FIELD_WHEN,{||.F.})
	oModelG6A:GetStruct():SetProperty("G6A_LAZER"		,MODEL_FIELD_WHEN,{||.F.})
EndIf


Return xRetorno


//============================================================================================================
/*/{Protheus.doc} Ta35Active

Fun��o de Ativa��o do Model

@since 17/03/2016
/*/
//============================================================================================================

Function Ta35Active(oModel)

Local oModelG6A	:= oModel:GetModel("G6AMASTER")
Local cCodClass	:= oModelG6A:GetValue("G6A_CDCLAS")
Local nOperation	:= oModel:GetOperation()

If nOperation == MODEL_OPERATION_INSERT .Or. (nOperation == MODEL_OPERATION_UPDATE .And. SubStr(cCodClass,1,1) <> "M") 
	
	oModelG6A:GetStruct():SetProperty("G6A_CORP"		,MODEL_FIELD_WHEN,{||.T.})
	oModelG6A:GetStruct():SetProperty("G6A_EVENTO"	,MODEL_FIELD_WHEN,{||.T.})
	oModelG6A:GetStruct():SetProperty("G6A_LAZER"		,MODEL_FIELD_WHEN,{||.T.})

Else

	oModelG6A:GetStruct():SetProperty("G6A_CORP"		,MODEL_FIELD_WHEN,{||.F.})
	oModelG6A:GetStruct():SetProperty("G6A_EVENTO"	,MODEL_FIELD_WHEN,{||.F.})
	oModelG6A:GetStruct():SetProperty("G6A_LAZER"		,MODEL_FIELD_WHEN,{||.F.})

EndIf

Return .T.


//============================================================================================================
/*/{Protheus.doc} T35PesqPFF(cFilPesq, cClass, cSegNeg, cCodPrd, cCodCli, cLojaCli)

Fun��o para pesquisa de dados dos parametros Fiscais e Financeiros

@sample 	T35PesqPFF                  
@author  	Jose Domingos Caldana Jr
@since   	18/06/2015
@version  	P12
@return 	aParFis := {cNat,cTes,cSerie,cPrdFat,cCntOrc}
/*/
//============================================================================================================

Function T35PesqPFF(cFilPesq, cClass, cSegNeg, cCodPrd, cCliFor, cCodigo, cLoja)

Local aParFis 		:= NIL
Local aArea			:= GetArea()

Local cTpCli		:= ""
Local cCodGrp		:= ""
Local cTpRV			:= ""
Local cAliasQry 	:= GetNextAlias()
Local cWhere		:= "%%"

Local lRet			:= .T.
Local lCache		:= .F.

lCache := oCacheHash:Get(cFilPesq + cClass + cSegNeg + cCodPrd + cCliFor + cCodigo + cLoja, aParFis)
				
If !lCache

	If cCliFor == "2"
	
		SA2->(dbSetOrder(1))
	
		If SA2->(dbSeek( xFilial('SA2',cFilPesq) + cCodigo + cLoja ))
			cCodigo := SA2->A2_CLIENTE  
			cLoja	 := SA2->A2_LOJCLI	
		Else
			cCodigo := ""  
			cLoja	 := ""	
		EndIf
		
	EndIf
	
	If Empty(cClass)
		lRet := .F.
	ElseIf Empty(cSegNeg) .And. SubStr(cClass,1,1) <> "M" 
		lRet := .F.
	ElseIf (Empty(cCliFor) .Or. Empty(cCodigo) .Or. Empty(cLoja))
		lRet := .F.
	EndIf
	
	If lRet
	
		If ( SubStr(cClass,1,1) == "M" )	//verifica se o produto � de apura��o de metas
			cWhere := "% AND G6B_CODGRP = ' ' %"
		Else
			
			If ( !Empty(cCodPrd) )
			
				SB1->(dbSetOrder(1))
				
				If SB1->(dbSeek(xFilial('SB1',cFilPesq) + cCodPrd ))
					cCodGrp := SB1->B1_GRUPO
					cWhere := "% AND (G6B_CODGRP = '" + cCodGrp + "' OR G6B_CODGRP = ' ')%"
				EndIf
			
			EndIf
			
		EndIf
		
		SA1->(dbSetOrder(1))
		
		If SA1->(dbSeek( xFilial('SA1',cFilPesq) + cCodigo + cLoja ))
			
			cTpCli := IIf( SA1->A1_EST == 'EX', '2', '1' )
			
			//Para paremetros de Metas, todos os segmentos devem estar habilitados.
			If cSegNeg == '1' .Or. SubStr(cClass,1,1) == "M" //Metas 
				cTpRV := "% AND G6A_CORP = 'T' %"
			ElseIf cSegNeg == '2' .Or. SubStr(cClass,1,1) == "M" //Metas 
				cTpRV := "% AND G6A_EVENTO = 'T' %"
			ElseIf cSegNeg == '3' .Or. SubStr(cClass,1,1) == "M" //Metas 
				cTpRV := "% AND G6A_LAZER = 'T' %"
			EndIf
			
			BeginSql Alias cAliasQry
				
				SELECT G6A_CODPRD, G6A_SERIE, G6B_CODTS, G6B_CODNAT, G6A_CODCO
					FROM %Table:G6A% G6A
					INNER JOIN %Table:G6B% G6B
						ON G6A_CODIGO = G6B_CODIGO
							%Exp:cWhere%
							AND (G6B_TPFORN = %Exp:cTpCli% OR G6B_TPFORN = '3')
							AND G6A_FILIAL = G6B_FILIAL
							AND G6B.%notDel%
					WHERE G6A_FILIAL = %Exp:xFilial("G6A",cFilPesq)%
							AND G6A_CDCLAS = %Exp:cClass%
							AND (G6A_CDCLAS <> 'V01' OR G6A_CODPRD = %Exp:cCodPrd%)
							AND G6A.%notDel%
							%Exp:cTpRV%
					ORDER BY G6B_CODGRP DESC
					
			EndSql
			
			If (cAliasQry)->(!Eof())
					aParFis := {	(cAliasQry)->G6B_CODNAT,;
									(cAliasQry)->G6B_CODTS,;
									(cAliasQry)->G6A_SERIE,;
									(cAliasQry)->G6A_CODPRD,;
									(cAliasQry)->G6A_CODCO }
			EndIf
			
			(cAliasQry)->(dbCloseArea())
	
		EndIf
		
	EndIf

	If ValType(aParFis) == "U"
		aParFis := {"","","","",""}
	EndIf

	oCacheHash:Set(cFilPesq + cClass + cSegNeg + cCodPrd + cCliFor + cCodigo + cLoja, aParFis)

	RestArea(aArea)

EndIf

Return aParFis



//============================================================================================================
/*/{Protheus.doc} PesqPFF(cFilPesq, cClass, cSegNeg, cCodPrd, cCodCli, cLojaCli)

Fun��o para pesquisa de dados dos parametros Fiscais e Financeiros

/*/
//============================================================================================================
User Function PesqPFF()

Local aParFis := {}

If Pergunte("TST1035",.T.)

	aParFis := T35PesqPFF(cFilAnt, MV_PAR01, STR(MV_PAR02,1), MV_PAR03, STR(MV_PAR04,1), MV_PAR05, MV_PAR06)

	FwAlertInfo("Natureza: "+aParFis[1] + Chr(10)+ Chr(13)+;
				 "TES: "+aParFis[2] + Chr(10)+ Chr(13)+;
				 "S�rie: "+aParFis[3] + Chr(10)+ Chr(13)+;
				 "Produto: "+aParFis[4] + Chr(10)+ Chr(13)+;
				 "C. Or�ament: "+aParFis[5] + Chr(10)+ Chr(13);
				 ,"Par�metros Fiscais e Financeiros")

EndIf

Return


//============================================================================================================
/*/{Protheus.doc} T35DelCache()

Fun��o para limpar objeto oCacheHash

@sample 	T35DelCache                  
@author  	Jose Domingos Caldana Jr
@since   	18/06/2015
@version  	P12
@return 	
/*/
//============================================================================================================

Function T35DelCache()

oCacheHash:Clean()

Return