#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA038.CH"
#INCLUDE "FILEIO.CH"

#DEFINE GRIDMAXLIN 99999

Static oTur038Mdl   := Nil
Static oTur038oView := Nil
Static oBrowse		:= Nil
Static _cT38Msg     := ""

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AtuT38Msg

Retona mensagem de erro para grava��o do log

@author    Philip Pellegrini
@version   1.00
@since     19/05/2017
/*/
//--------------------------------------------------------------------------------------------------------------------
Function AtuT38Msg(cMsg)

_cT38Msg += cMsg + Chr(13) + Chr(10)
 	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur038Modelo

Retona o model e view est�ticos

@author    Enaldo Cardoso Junior
@version   1.00
@since     11/03/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur038Modelo()
Return ({oTur038Mdl, oTur038oView})

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038

Importacao de Faturas Aereas

@author    Marcelo Cardoso Barbosa
@version   1.00
@since     13/07/2015
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038()
	
SetKey (VK_F12, {|a, b| AcessaPerg("TURA038", .T.)})
SetKey (VK_F11, {|a, b| AcessaPerg("TURA041", .T.)})	

If TURExistX1("TA038BRW")
	SetKey (VK_F10, {|a, b| AcessaPerg("TA038BRW", .T.), TA038BRW(oBrowse)})	
EndIf

oBrowse	:= FwMBrowse():New()

oBrowse:SetAlias("G6H")
T038ADDLEG(oBrowse)		//Adiciona legendas no browse
If SX2->(DbSeek("G6H"))
	oBrowse:SetDescription(Capital(X2Nome()))
End If

oBrowse:AddButton(STR0023, {|| TURA038IMP()}) 	// Importa��o da Fatura
oBrowse:AddButton(STR0071, {|| CFGA650()}) 		// Log da Importa��o da Fatura

oBrowse:Activate()

Set Key VK_F12 To
Set Key VK_F10 To

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef

Fun��o que monta o menu do programa.

@author Marcelo Cardoso Barbosa
@since 20/07/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function MenuDef()
	
Local aRotina := {}
	
ADD OPTION aRotina Title STR0004 ACTION "PesqBrw"		 OPERATION 1 ACCESS 0 		// Pesquisa
ADD OPTION aRotina Title STR0005 ACTION "TA038MnuOpc(1)" OPERATION 2 ACCESS 0 		// Visualizar
ADD OPTION aRotina Title STR0006 ACTION "TA038MnuOpc(3)" OPERATION 3 ACCESS 0		// Incluir
ADD OPTION aRotina Title STR0007 ACTION "TA038MnuOpc(4)" OPERATION 4 ACCESS 0		// Alterar
ADD OPTION aRotina Title STR0008 ACTION "TA038MnuOpc(5)" OPERATION 5 ACCESS 0 		// Excluir
ADD OPTION aRotina Title STR0029 ACTION "Tu038Asso()"	 OPERATION 6 ACCESS 0		// Associa��o Automatica
ADD OPTION aRotina Title STR0031 ACTION "TA038AsMan()"	 OPERATION 2 ACCESS 0 		// Associa��o Manual
ADD OPTION aRotina Title STR0032 ACTION "TURA058"	     OPERATION 4 ACCESS 0		// Antecipa��o
ADD OPTION aRotina Title STR0033 ACTION "FwMsgRun(, {|| Tura038Efe()}, , '" + STR0034 + "')"	OPERATION 6 ACCESS 0			//"Efetiva��o"	//"Aguarde, realizando efetiva��o..."
ADD OPTION aRotina Title STR0053 ACTION "FwMsgRun(, {|| Tura038DEf()}, , '" + STR0052 + "')"	OPERATION 6 ACCESS 0			//"Desfaz Efetiva��o"	//"Aguarde, realizando o estorno da efetiva��o..."	
ADD OPTION aRotina Title STR0132 ACTION "FwMsgRun(, {|oSelf| TA038DeCon(oSelf)}, , '" + STR0038 + "')"	OPERATION 4 ACCESS 0	//"Excluir concilia��es"###"Excluindo concilia��es..."	
	
Return aRotina

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MnuOpc

Fun��o Auxiliar para chamar a execu��o da View. Esta fun��o � executada pelo menu da rotina 

@type Function
@author Fernando Radu Muscalu
@since 28/07/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038MnuOpc(nOpc)

Local cOperacao	:= ""
	
Do Case
	Case nOpc == 1
		cOperacao := STR0005	//"Visualizar"
	Case nOpc == 3
		cOperacao := STR0006	//"Incluir"
	Case nOpc == 4
		cOperacao := STR0007	//"Alterar"
	Case nOpc == 5
		cOperacao := STR0008	//"Excluir"			
End Case

FWExecView(cOperacao, "TURA038", nOpc, , {|| .T.})

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038AsMan

Chama a associa��o manual apenas pela execview de visualiza��o.

@author Enaldo Cardoso
@since 13/04/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038AsMan()

Local aArea    := GetArea()
Local oModel   := FwModelActive()
Local aButtons := {{.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.T., STR0014}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}}	// "Fechar"

If G6H->G6H_EFETIV == "2"
	If SoftLock("G6H")
  		FWExecView(STR0005, "TURA038", MODEL_OPERATION_UPDATE, , {|| .T.} , , , aButtons)	//"VISUALIZAR"
		G6H->(MsUnLock())
	EndIf
Else
	Help(" ", 1, "TA038AsMan", , STR0056, 1, 0)	//	"Fatura j� efetivada, n�o ser� permitido realizar essa opera��o"
EndIf

RestArea(aArea)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Fun��o que monta o modelo de dados do programa

@author Marcelo Cardoso Barbosa
@since 13/07/2015
@version 1.0
@Return Objeto Retorno do modelo de dados
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()
	
Local	oModel		:= Nil
Local	oStruG6H	:= FWFormStruct(1, "G6H")
Local	oStruG6I	:= FWFormStruct(1, "G6I", /*bAvalCampo*/, .F.)
Local 	oStruG8Y	:= FwFormStruct(1, "G8Y") //T�tulos x Faturas
Local	aAux		:= {}
Local 	bPreG6I 	:= 	{|oModel, nLinha, cAcao, cCampo| TURA38PreVl(oModel, nLinha, cAcao, cCampo)}

oStruG6H:SetProperty("G6H_FORNEC", MODEL_FIELD_NOUPD, .T.)
oStruG6H:SetProperty("G6H_LOJA"	 , MODEL_FIELD_NOUPD, .T.)
oStruG6I:SetProperty("G6I_TPFOP" , MODEL_FIELD_INIT , {|| "0"})
oStruG6I:SetProperty("G6I_TPFOP" , MODEL_FIELD_WHEN , {|| (Empty(FwFldGet("G6I_CONCIL")) .And. Empty(FwFldGet("G6I_VLRLIQ"))) .Or. FwIsInCallStack("RUNTRIGGER") .Or. FwIsInCallStack("TA038PSLN") .Or. FwIsInCallStack("TURA038IMP")})

Ta38Struct("G6I", oStruG6I, "M")

oModel := MPFormModel():New("TURA038", /*bPreValid*/, {|oMdl| TA038TdOk(oMdl)}, {|oMdl| TA038PSMD(oMdl)}, /*bCancel*/)
oModel:AddFields("G6HMASTER", , oStruG6H)

oStruG6I:AddField("", "", "G6I_LEGCON", "BT", 1, 0, /*bValid*/, /*bWhen*/, /*aValues*/, .F., {|oMdl| Tur038Leg(oMdl)} /*bInit*/, /*lKey*/, /*lNoUpd*/, .T. /*lVirtual*/, /*cValid*/) 
oModel:AddGrid("G6IDETAIL", "G6HMASTER", oStruG6I, bPreG6I, /*bLinePost*/, /*bPre*/, /*bPost */)
oModel:SetRelation("G6IDETAIL", {{"G6I_FILIAL", "xFilial('G6H')"}, {"G6I_FATURA", "G6H_FATURA"}}, G6I->(IndexKey(1)))
oModel:GetModel("G6IDETAIL"):SetUniqueLine({"G6I_FATURA", "G6I_ITEM"})

oModel:AddGrid("G8Y_ITENS", "G6HMASTER", oStruG8Y)
oModel:SetRelation("G8Y_ITENS", {{"G8Y_FILIAL", "xFilial('G6H')"}, {"G8Y_FATURA", "G6H_FATURA"}}, G8Y->(IndexKey(1)))
oModel:GetModel("G8Y_ITENS"):SetDescription(STR0094) //"Titulos"
oModel:GetModel("G8Y_ITENS"):SetNoInsertLine(.F.)
oModel:GetModel("G8Y_ITENS"):SetNoUpdateLine(.F.)
oModel:GetModel("G8Y_ITENS"):SetNoDeleteLine(.F.)
oModel:GetModel("G8Y_ITENS"):SetOptional(.T.)

If !FwIsInCallStack("Tura038Imp") 
	oModel:AddCalc("TOTAL", "G6HMASTER", "G6IDETAIL", "G6I_VLRLIQ", "LIQUIDO", "FORMULA", {|| oModel:GetModel("G6IDETAIL"):GetValue("G6I_STATUS") <> "2" }/*bCondition*/, /*bInitValue*/, STR0119, {|oModel, nTotalAtual, xValor, lSomando| TA038CALC(oModel, nTotalAtual, xValor, lSomando)}/*bFormula*/)	// "Valor Total L�quido"
	oModel:GetModel("TOTAL"):SetDescription(STR0119) //'Valor Total Liquido'
EndIf

aAux := FwStruTrigger("G6I_BILHET", "G6I_LOCALI", "", .F., Nil, Nil, Nil, "!Empty(FwFldGet('G6I_BILHET'))")
oStruG6I:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G6I_LOCALI", "G6I_BILHET", '', .F., Nil, Nil, Nil, "!Empty(FwFldGet('G6I_LOCALI'))")
oStruG6I:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G6I_TIPOIT", "G6I_TIPOIT", "TA038ClearCpo()")
oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

If !FwIsInCallStack("IMPORTGOL") .And. !FwIsInCallStack("Tura038Imp")
	aAux := FwStruTrigger("G6I_TIPOIT", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TARIFA", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TARAVI", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TARCRE", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TAXAS" , "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger("G6I_TAXADU", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TXCCDU", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_VLRCOM", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger("G6I_VLRINC", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TXREE" , "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_VLRLIQ", "G6I_TPFOP", "TA038TpFOP()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])	
	
	aAux := FwStruTrigger("G6I_TARIFA", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TAXAS" , "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger("G6I_TAXADU", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TXCCDU", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_VLRCOM", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger("G6I_VLRINC", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_TXREE" , "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger("G6I_VLRLIQ", "G6I_TPOPER", "TA038TpOper()")
	oStruG6I:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])
EndIf

oModel:SetDescription(STR0003) 							//Fatura A�rea
oModel:GetModel("G6HMASTER"):SetDescription(STR0001) 	// Cabe�alho da Fatura A�rea
oModel:GetModel("G6IDETAIL"):SetDescription(STR0002) 	// Itens da Fatura A�rea
oModel:GetModel("G6IDETAIL"):SetMaxLine(GRIDMAXLIN)		// Define o n�mero m�ximo de linhas que o model poder� receber, de acordo com a define GRIDMAXLIN.
oModel:SetVldActivate({|oModel| TURA038VLE(oModel)})   

Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Fun��o que monta a View do Programa

@author Marcelo Cardoso Barbosa
@since 20/07/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()
	
Local oView		:= FWFormView():New()
Local oModel	:= FWLoadModel("TURA038")
Local oStruG6H	:= FWFormStruct(2, "G6H")
Local oStruG6I	:= FWFormStruct(2, "G6I")
Local oStruG8Y	:= FwFormStruct(2, "G8Y") //T�tulos x Fatura
Local oStruCalc	:= FWCalcStruct(oModel:GetModel("TOTAL") )

oStruG6I:RemoveField("G6I_FATURA")
oStruG6I:RemoveField("G6I_NUMID")
oStruG6I:RemoveField("G6I_IDITEM")
oStruG6I:RemoveField("G6I_NUMSEQ")
oStruG6I:SetProperty("G6I_TPFOP", MVC_VIEW_CANCHANGE, .T.)

oStruG6I:AddField("G6I_LEGCON", "01", "", "", {}, "BT", "", Nil, Nil, .F., "", Nil, Nil, Nil, Nil, .T., Nil)
Ta38Struct("G6I", oStruG6I, "V")

TA41ViewOrder(oStruG6I, "TURA038", "G6IDETAIL")

If FwIsInCallStack("TA038AsMan")
	oView:AddUserButton(STR0059, STR0058, {|oView| Ta38aExe()}   , , , {MODEL_OPERATION_UPDATE})	//"Pesquisar Doc.Reserva"
	oView:AddUserButton(STR0109, STR0110, {|oView| Ta38aExe("M")}, , , {MODEL_OPERATION_UPDATE})	//"Pesq. Metas"#"Pesquisa de Metas"
	
	oModel:GetModel("G6HMASTER"):SetOnlyView(.T.)
	oModel:GetModel("G6IDETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("G6IDETAIL"):SetNoUpdateLine(.T.)
	oModel:GetModel("G6IDETAIL"):SetNoDeleteLine(.T.)
	
	oView:ShowUpdateMsg(.F.)
	
	oModel:SetActivate({|oModel| oModel:lModify := .T.})
	oModel:SetCommit({|| .T.})
Else
	oView:AddUserButton(STR0057, STR0058, {|oView| Ta38GIt(oView)}          , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE})		// "Gerar Item Fatura"
	oView:AddUserButton(STR0098, STR0058, {|oView| Ta38CancIt(oView)}       , , , {MODEL_OPERATION_UPDATE})								// "Ativa/Cancela ACM"
	oView:AddUserButton(STR0111, STR0111, {|oView| TA38SetMeta(oView)}      , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE})		// "Definir como Meta"#"Definir como Meta"
	oView:AddUserButton(STR0112, STR0112, {|oView| TA38SetMeta(oView, .T.)} , , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE})		// "Desfazer Meta"#"Desfazer Meta"
	oView:AddUserButton(STR0144, STR0144, {||      TURA038N()}              , , ,{ MODEL_OPERATION_UPDATE, MODEL_OPERATION_VIEW})		// Total por Iata
EndIf

oView:AddUserButton(STR0122, STR0122, {|oView|Tur038Leg(oView, .T.)})	// "Legendas"
oStruG6H:SetProperty("G6H_LAYOUT", MVC_VIEW_CANCHANGE, .F.)
oStruG6H:SetProperty("G6H_TIPO"  , MVC_VIEW_CANCHANGE, .F.)

oTur038Mdl := oModel
	
oView:SetModel(oModel)
oView:AddField("VIEWG6H" , oStruG6H , "G6HMASTER")
oView:AddGrid("VIEWG6I"  , oStruG6I , "G6IDETAIL")
oView:AddField("VIEWCALC", oStruCalc, "TOTAL")
oView:AddGrid("VIEW_G8Y" , oStruG8Y , "G8Y_ITENS")	
oView:EnableTitleView("VIEW_G8Y")
	
oView:AddIncrementField("VIEWG6I", "G6I_ITEM")

oView:EnableTitleView("VIEWG6I", STR0002 ) //'Itens da Fatura A�rea'

// Divis�o Horizontal
oView:CreateHorizontalBox("ALL", 100)
oView:CreateFolder("FOLDER_ALL"	,"ALL")

// Pastas
oView:AddSheet("FOLDER_ALL", "ABA_ALL_FAT",	STR0095) // "Fatura"
oView:AddSheet("FOLDER_ALL", "ABA_ALL_TIT",	STR0094) // "T�tulos"

oView:CreateHorizontalBox("SUPERIOR"	 , 030, , , "FOLDER_ALL", "ABA_ALL_FAT")
oView:CreateHorizontalBox("INTERMEDIARIA", 012, , , "FOLDER_ALL", "ABA_ALL_FAT")
oView:CreateHorizontalBox("INFERIOR"	 , 058, , , "FOLDER_ALL", "ABA_ALL_FAT")
oView:CreateHorizontalBox("TITULOS"		 , 100, , , "FOLDER_ALL", "ABA_ALL_TIT")

oView:SetOwnerView("VIEWG6H" , "SUPERIOR")
oView:SetOwnerView("VIEWCALC", "INTERMEDIARIA")
oView:SetOwnerView("VIEWG6I" , "INFERIOR")
oView:SetOwnerView("VIEW_G8Y", "TITULOS")

oView:SetViewProperty("VIEWG6I", "GRIDFILTER", {.T.})

oTur038oView := oView
oView:SetViewProperty("VIEWG6I", "GRIDDOUBLECLICK", {{|oGrid, cField, nLineGrid, nLineModel| Ta038DbClk(oGrid, cField, nLineGrid, nLineModel)}})
	
Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur038Leg

Define as cores da legenda quando selecionada a op��o associa��o manual.

@author    Enaldo Cardoso Junior
@version   1.00
@since     11/03/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur038Leg(oModel,lTela)

Local oLegenda := FwLegend():New()
Local bGray	   := {|| G6I->G6I_STATUS == '2'} //Cancelado
Local bOrange  := {|| G6I->G6I_META   == '1' .And. !Empty(G6I->G6I_CONCIL)} //Item de Meta Associado
Local bBlue	   := {|| G6I->G6I_META   == '1' .And.  Empty(G6I->G6I_CONCIL)} //Item de Meta n�o Associado
Local bGreen   := {|| G6I->G6I_META   <> '1' .And.  Empty(G6I->G6I_CONCIL)} //N�o Associado/Conciliado
Local bRed	   := {|| G6I->G6I_META   <> '1' .And. !Empty(G6I->G6I_CONCIL)} //Associado/Conciliado

Default lTela  := .F.

If !lTela .And. oModel:GetLine() > 0
	bGray	:= {|| oModel:GetValue("G6I_STATUS") == '2'} //Cancelado
	bOrange	:= {|| oModel:GetValue("G6I_META")   == '1' .And. !Empty(oModel:GetValue("G6I_CONCIL"))} //Item de Meta Associado
	bBlue	:= {|| oModel:GetValue("G6I_META")   == '1' .And.  Empty(oModel:GetValue("G6I_CONCIL"))} //Item de Meta n�o Associado
	bGreen	:= {|| oModel:GetValue("G6I_META")   <> '1' .And.  Empty(oModel:GetValue("G6I_CONCIL"))} //N�o Associado/Conciliado
	bRed	:= {|| oModel:GetValue("G6I_META")   <> '1' .And. !Empty(oModel:GetValue("G6I_CONCIL"))} //Associado/Conciliado
EndIf

oLegenda:Add(bGray	, "GRAY"  , STR0100) // "Cancelado"
oLegenda:Add(bOrange, "ORANGE", STR0123) // "Item de Meta Associado"
oLegenda:Add(bBlue	, "BLUE"  , STR0124) // "Item de Meta n�o Associado"
oLegenda:Add(bGreen	, "GREEN" , STR0125) // "N�o Associado/Conciliado"
oLegenda:Add(bRed	, "RED"   , STR0126) // "Associoado/Conciliado"

oLegenda:Activate()

If lTela
	oLegenda:View()
Else
	cRet := oLegenda:Execute()
EndIf

oLegenda:DeActivate()
FreeObj(oLegenda)
oLegenda := Nil  
DelClassIntf()
                                                                                                                        
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta38GIt

Chama a rotina de busca de documento de reserva para realizar associa��o manual e gera��o de item da fatura.

@author    Enaldo Cardoso Junior
@version   1.00
@since     11/03/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta38GIt(oView)

Local oMddlG6H := oView:GetModel("G6HMASTER") 

oMddlG6H:GetStruct():SetProperty("G6H_FORNEC", MODEL_FIELD_OBRIGAT, .F.)
oMddlG6H:GetStruct():SetProperty("G6H_LOJA"	 , MODEL_FIELD_OBRIGAT, .F.)

If oMddlG6H:VldData()
	If oMddlG6H:GetValue("G6H_LAYOUT") == "3" // Aereo Manual
		FwMsgRun(, {|| FWExecView(STR0037, "VIEWDEF.TURA038A", MODEL_OPERATION_UPDATE,, {|| .T.})}, , STR0038)		// "Gera��o de item da fatura"	//"Aguarde..."
		TA038ADestroy()		//Apaga as tabelas tempor�rias
	Else
		MsgInfo(STR0039)	//"� permitido a gera��o de item da fatura apenas para fatura layout tipo igual � 3-A�reo Manual."
	EndIf
Else
	FwAlertError(STR0078, STR0054)	//"Opera��o n�o premitida enquanto existirem campos n�o preenchidos (Cabe�alho - G6HMASTER)."//"Aten��o"
EndIf

oMddlG6H:GetStruct():SetProperty("G6H_FORNEC", MODEL_FIELD_OBRIGAT, .T.)
oMddlG6H:GetStruct():SetProperty("G6H_LOJA"	 , MODEL_FIELD_OBRIGAT, .T.)

oMddlG6H := Nil

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta38aExe

Chamada da fun��o de pesquisa de documento de reserva.

@author    Enaldo Cardoso Junior
@version   1.00
@since     11/03/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta38aExe(cTipo)

Local oModel	:= Tur038Modelo()[1]
Local oModelG6I := oModel:GetModel("G6IDETAIL")
Local cConcil	:= oModelG6I:GetValue("G6I_CONCIL")
Local lRet 		:= .T.

Default cTipo	:= "D"

If Empty(cConcil)
	If oModelG6I:GetValue("G6I_STATUS") <> '2'
	
		oModel:GetModel("G6IDETAIL"):SetNoInsertLine(.F.)
		oModel:GetModel("G6IDETAIL"):SetNoUpdateLine(.F.)
		oModel:GetModel("G6IDETAIL"):SetNoDeleteLine(.F.)
		
		If cTipo == "D" .And. oModelG6I:GetValue("G6I_META") <> "1" 	//Pesquisa de Documento de Reserva
			If oModelG6I:GetValue("G6I_TPFOP") == '0' 	// Tipo de FOP desconhecida
				FwAlertHelp(STR0130, STR0131, STR0054)	// "Item selecionado marcado como Tipo Pagamento 'Desconhecido'"###"Revise os valores informados no item da fatura antes de realizar a associa��o."###Aten��o"
			Else
				FwMsgRun( , {|| FWExecView(STR0040, "VIEWDEF.TURA038A", MODEL_OPERATION_UPDATE,, {|| .T.}) }, , STR0041)	//"Pesquisa de Documentos de Reserva"	//"Pesquisando documentos..."
			EndIf
			
		ElseIf cTipo == "M" .And. oModelG6I:GetValue("G6I_META") == "1" //Pesquisa de Metas	
			FwMsgRun( , {|| FWExecView(STR0110, "VIEWDEF.TURA038M", MODEL_OPERATION_UPDATE, , {|| .T.})}, , STR0041)	//"Pesquisa de Metas"	//"Pesquisando documentos..."
		EndIf
		
		oModel:GetModel("G6IDETAIL"):SetNoInsertLine(.T.)
		oModel:GetModel("G6IDETAIL"):SetNoUpdateLine(.T.)
		oModel:GetModel("G6IDETAIL"):SetNoDeleteLine(.T.)
			
		oModel:DeActivate()
  		oModel:Activate()
	Else
		If !FwIsInCallStack("Ta38GIt")
			Help(, , "Ta38aExe", , STR0101, 1, 0)	//"N�o pode ser associado item cancelado."
			lRet := .F.
		EndIf	
	EndIf
Else
	If !FwIsInCallStack("Ta38GIt")
		Help( , , "Ta38aExe", , STR0043, 1, 0)		//"O item da fatura selecionado j� est� associado."
		lRet := .F.
	EndIf	
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038Per

Fun��o que valida o campo Periodo (G6H_PERIOD) em funcao do campo Periodiciade (G6H_PERCID)

@author Marcelo Cardoso Barbosa
@since 22/07/2015
@version 1.0
@param oModel, objeto, Modelo de dados
@Return L�gico Retorna se o conteudo do campo � v�lido ou n�o
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038PER()

Local lRetPer  := .T.
Local cPeriodi := FWFldGet("G6H_PERCID")
Local cPeriodo := FWFldGet("G6H_PERIOD")

Do Case
	Case cPeriodi == "1"
		If cPeriodo < "1" .Or. cPeriodo > "4"
			Help( , , "TURA038PER", , STR0009, 1, 0) // Para Periodicidade Semanal (1), informe um Periodo entre 1 e 4
			lRetPer := .F.
		EndIf
		   
	Case cPeriodi == "2"
		If cPeriodo < "1" .Or. cPeriodo > "3"
			Help( , , "TURA038PER", , STR0010, 1, 0) // Para Periodicidade Decendial (2), informe um Periodo entre 1 e 3
			lRetPer := .F.
		EndIf
	
	Case cPeriodi == "3"
		If cPeriodo < "1" .Or. cPeriodo > "2"
			Help( , , "TURA038PER", , STR0011, 1, 0) // Para Periodicidade Quinzenal (3), informe um Periodo entre 1 e 2
			lRetPer := .F.
		EndIf
	
	Case cPeriodi == "4"
		If cPeriodo < "1" .Or. cPeriodo > "1"
			Help( , , "TURA038PER", , STR0012, 1, 0) // Para Periodicidade Mensal (4), informe um Periodo entre 1 e 1
			lRetPer := .F.
		EndIf
	
	Case cPeriodi == "5"
		If cPeriodo < "1" .Or. cPeriodo > "2"
			Help( , , "TURA038PER", , STR0013, 1, 0) // Para Periodicidade Duo Decendial (5), informe um Periodo entre 1 e 2
			lRetPer := .F.
		EndIf
EndCase
 
Return lRetPer

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038GPR

Fun��o chamada na Pr�-Execu��o do Layout Mile de IMPORTACAO DE FATURA AEREA PADRAO GOL (TURA038G)  

@author Marcelo Cardoso Barbosa
@since 22/07/2015
@version 1.0
@param Interface, VetorAdicional, Informa��es Layout, Modelo
@Return Objeto Retorna o Model
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038GPR(lInterface, aVetor, xAdic, oModelPre)

Local nStopHere := 1

Return oModelPre

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038POS

Fun��o chamada na P�s-Execu��o do Layout Mile de IMPORTACAO DE FATURA AEREA PADRAO 

@author Marcelo Cardoso Barbosa
@since 07/08/2015
@version 1.0
@param Interface, VetorAdicional, Informa��es Layout, Modelo
@Return Sem retorno
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038POS(lInterface, aVetor, xAdic, oModel)

Local aArea      := GetArea()
Local aSaveLines := FWSaveRows() 
Local oModelG6H  := oModel:GetModel("G6HMASTER")
Local oModelG6I  := oModel:GetModel("G6IDETAIL")
Local cG6HForn   := oModelG6H:GetValue("G6H_FORNEC")
Local cG6HLoja   := oModelG6H:GetValue("G6H_LOJA")
Local cReport    := " "
Local lErroMile  := .F.
Local nI         := 0
Local nValItem   := 0
Local nCred		 := 0
Local nDeb		 := 0
		
If !(lErroMile := oMileImport:Error())
	For nI := 1 To oModelG6I:Length()
		oModelG6I:GoLine(nI)
		
		TURA038CC(oModelG6I)			
		
		If oModelG6I:GetValue('G6I_TIPOIT') $ '3|5' 	//REEMBOLSO/ACM
			If oModelG6I:GetValue('G6I_TPOPER') == '2'  //1-DEB/2-CRED
				nValItem -=  oModelG6I:GetValue('G6I_VLRLIQ')
				nCred    += (oModelG6I:GetValue('G6I_VLRLIQ') * -1)
			Else
				nValItem += oModelG6I:GetValue('G6I_VLRLIQ')
				nDeb     += oModelG6I:GetValue('G6I_VLRLIQ')					
			EndIf
		Else 
			If oModelG6I:GetValue('G6I_TPOPER') == '2'  //1-DEB/2-CRED
				nValItem +=  oModelG6I:GetValue('G6I_VLRLIQ')
				nCred    += (oModelG6I:GetValue('G6I_VLRLIQ') * -1)
			Else
				nValItem -= oModelG6I:GetValue('G6I_VLRLIQ')
				nDeb     += oModelG6I:GetValue('G6I_VLRLIQ')
			EndIf
		EndIf
		
		If MV_PAR09 == 2
			Tura038Ex(oModelG6I)
		EndIf
		
		If MV_PAR10 == 2				
			Tur38ExIAT(oModelG6I, StrTokArr(MV_PAR11, '|'))
		EndIf
	Next nI
	
	nValItem := nCred + nDeb
	
	G4S->(DbSetOrder(1)) 	// G4S_FILIAL+G4S_FORNEC+G4S_LOJA
	If G4S->(DbSeek(xFilial("G4S") + cG6HForn +	cG6HLoja))
		cReport := G4S->G4S_REPORT 
	EndIf 
	
	If !IsInCallStack("FWMILEIMPORT")
		Help( , , "TURA038POS", , STR0025, 1, 0 ) // A soma dos valores l�quidos da fatura, subtraindo-se os reembolsos, n�o � igual ao Valor Total informado no cabe�alho da Fatura.
	Else
		RecLock("G6H", .F.)
		G6H->G6H_VALOR  := nValItem
		G6H->G6H_ARQUIV := cTURA038Fl 
		G6H->G6H_REPORT := IIF(cReport $ " 2", "2", "1") 
		G6H->(MsUnLock()) 	
	EndIf
EndIf

FWRestRows(aSaveLines)
RestArea(aArea)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038GTR

Fun��o chamada na Pr�-Execu��o do Layout Mile de IMPORTACAO DE FATURA AEREA PADRAO GOL (TURA038G)  

@author Marcelo Cardoso Barbosa
@since 22/07/2015
@version 1.0
@param Interface, VetorAdicional, Informa��es Layout, Modelo
@Return Objeto Retorna o Model
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038GTR(lInterface, aVetor, xAdic, oModelTra)

Local nStopHere := 1

Return oModelTra

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038GVL

Fun��o chamada na Pr�-Execu��o do Layout Mile de IMPORTACAO DE FATURA AEREA PADRAO GOL (TURA038G)  

@author Marcelo Cardoso Barbosa
@since 22/07/2015
@version 1.0
@param Interface, VetorAdicional, Informa��es Layout, Modelo
@Return L�gico Retorna se a Unidade de Informacao � v�lida ou n�o.
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038GVL(lInterface, aVetor, xAdic, oModelVal)

Local nStopHere := 1

Return oModelVal

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038NUM

Fun��o chamada pela interface Mile para obten��o do n�mero da Fatura  

@author Marcelo Cardoso Barbosa
@since 04/08/2015
@version 1.0
@param Interface, VetorAdicional, Informa��es Layout, Modelo
@Return L�gico Retorna se a Unidade de Informacao � v�lida ou n�o.
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038NUM()

Local cNumFat := GetSXENum("G6H", "G6H_FATURA")
	
G6H->(ConfirmSX8())

Return cNumFat

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038EMI

Fun��o chamada para ler informa��o de Emiss�o do Item da Fatura e lan�a-la no Cabe�alho  

@author Marcelo Cardoso Barbosa
@since 03/08/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038EMI(xDtEmis)

Local oModel    := FwModelActive()
Local oModelG6H := oModel:GetModel("G6HMASTER")
Local dDtEmis   := dDataBase 
Local dDtVencto := dDataBase
Local nStopHere := 0
Local lRetEmi   := .F.
Local lRetVnc   := .F.

If !Empty(xDtEmis)
	dDtEmis := CtoD(SubStr(xDtEmis, 01, 02) + '/' + SubStr(xDtEmis, 03, 02) + '/20' + SubStr(xDtEmis, 05, 02))
EndIf

dDtVencto := dDtEmis + 13

If dDtVencto <= dDataBase
	dDtVencto := dDataBase + 1
EndIf

lRetEmi := oModelG6H:SetValue("G6H_EMISSA", dDtEmis)
lRetVnc := oModelG6H:SetValue("G6H_VENCTO", dDtVencto)
		
Return dDtEmis

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038IMP

Fun��o chamada para preparar a a Importacao de faturas pelo Layout Mile de IMPORTACAO DE FATURA AEREA  

@author Marcelo Cardoso Barbosa
@since 24/07/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038IMP(lAutom,aConfig)

Local aArea    := GetArea()	
Local lProsseg := .T.
Local aDirect  := {}
Local cDrive   := ""
Local cDir     := ""
Local cNome    := ""
Local cExt     := ""
Local cPath    := ""
Local nOk      := 0
Local nErro    := 0
Local nArq     := 0

Private oMileImport := FWMile():New(.T.)
Private lTURA038OK  := .T.
Default lAutom		:= .F.
Private nTipoImp    := 0 
Private cTURA038Fl  := ""
Private cFornec     := ""
Private cLoja       := ""

Default aConfig		:= {} 	

CONOUT("TURA038IMP THREAD " + AllTrim(Str(ThreadID())))

RestArea(aArea)

If IIF(!lAutom, Pergunte("TURA038IMP", .T.), .T.) 
	If lAutom
		MV_PAR01 := aConfig[1]
		MV_PAR02 := 1 
		MV_PAR03 := aConfig[2] + aConfig[3]
	EndIf

	nTipoImp := MV_PAR02
	cFornec  := MV_PAR04
	cLoja    := MV_PAR05
	
	SA2->(DbSetOrder(1))	// A2_FILIAL+A2_COD+A2_LOJA
	If SA2->(!DbSeek(xFilial("SA2") + cFornec + cLoja))
		Help( , , "TURA038IMP", , STR0028, 1, 0) // Codigo + Loja de Fornecedor inexistente. 
		lProsseg  := .F.
	EndIf
	
	If MV_PAR02 == 1 // Arquivo
		SplitPath(MV_PAR03, @cDrive, @cDir, @cNome, @cExt)
	
		If File(AllTrim(MV_PAR03))
			aAdd(aDirect, {cNome + cExt, 100, dDataBase, Time(), Space(1)})
			cPath := cDrive + cDir
		Else
			Help( , , "TURA038IMP", , STR0017, 1, 0) // Arquivo inexistente
			lProsseg  := .F.
		EndIf

	Else
		If !ExistDir(AllTrim(MV_PAR03))
			Help( , , "TURA038IMP", , STR0018, 1, 0) // Diret�rio de importa��o inexistente
			lProsseg  := .F.
		Else
			cPath   := Upper(AllTrim(MV_PAR03))
			aDirect := Directory(cPath + "*.")
	
			If Len(aDirect) < 1
				Help( , , "TURA038IMP", , STR0019, 1, 0) // Nenhum arquivo dispon�vel no diret�rio selecionado
				lProsseg  := .F.
			EndIf
		EndIf
	EndIf
	
	If MV_PAR01 == 2 // A�reo Localizador (Gol)
		If !ExistBlock("TURA038G")
			Help( , , "TURA038IMP", , STR0027, 1, 0) // "O fonte TURA038G deve estar compilado no ambiente corrente de modo a permitir a importa��o de Layouts padr�o A�reo Localizador." 
			lProsseg  := .F.
		EndIf
	EndIf

	If MV_PAR10 == 2 .And. Empty(MV_PAR11)
		FwAlertHelp(STR0117, STR0118, "MV_PAR11")	//'N�o foi informado nenhum c�digo IATA'##'Favor informar algum c�digo no campo "Quais IATAS?" ou altere a op��o do campo "Filtrar IATA?"'
		lProsseg  := .F.
	EndIf
	
	If lProsseg
		For nArq := 1 To Len(aDirect)
			lProsseg := .T.
			If !Empty(aDirect[nArq][1]) .And. aDirect[nArq][1] <> NIL
				G6H->(DbSetOrder(5)) 	// G6H_FILIAL + G6H_ARQUIV
				If !Empty(AllTrim(aDirect[nArq][1])) .And. G6H->(DbSeek(xFilial("G6H") + aDirect[nArq][1]))
					Help( , , "TURA038IMP", , I18N(STR0026, {G6H->G6H_FATURA}) + Upper(aDirect[nArq][1]), 1, 0) // N�o � poss�vel importar o arquivo, pois j� existe uma fatura gravada a partir de um arquivo com este nome: 
					lProsseg := .F.
				EndIf
			EndIf

			If lProsseg
				If MV_PAR01 == 2 // Aero Localizador
					U_TURA038G(aDirect[nArq][1], cPath)
				
				ElseIf MV_PAR01 == 1 .Or. MV_PAR01 == 3 .Or. MV_PAR01 == 4		// 1=BSP; 3=BSP - Avianca; 4=BSP - TAM
					cTURA038Fl := aDirect[nArq][1]
					oMileImport := FWMILE():New()
					oMileImport:SetOperation("1")

					If MV_PAR01 == 1 
						oMileImport:SetLayout("TURA038B")
					ElseIf MV_PAR01 == 3
						oMileImport:SetLayout("TURA038H")
					Else
						oMileImport:SetLayout("TURA038I")
					EndIf 

					oMileImport:SetTXTFile(cPath + aDirect[nArq][1])
					If oMileImport:Activate()
			
						FwMsgRun( , {|| oMileImport:Import()}, , STR0113)	// "Aguarde... Realizando importa��o."
						If oMileImport:Error()
							nErro += 1
						Else
							nOk += 1
						EndIf
				
						oMileImport:Deactivate()
						Ta038Refsh()
					Else
						Help( , , "TURA038IMP", , STR0020, 1, 0) // Falha ao importar o arquivo
					EndIf
				EndIf
			EndIf
		Next nArq
		
		If nErro > 0
		 	If !lAutom
				Help( , , "TURA038IMP", , STR0021 + AllTrim(Str(nErro)), 1, 0) // N�mero de arquivos nao importados:
			EndIf
		EndIf

		If nOk > 0 .And. lTURA038OK
			If !lAutom
				Help( , , "TURA038IMP", , STR0022 + AllTrim(Str(nOk)), 1, 0) // N�mero de arquivos nao importados:
			EndIf
		EndIf
	EndIf
EndIf

RestArea(aArea)

Return nErro == 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura038DR

Realiza a busca dos documentos de reserva.

@author Enaldo Cardoso Junior
@since 08/02/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura038DR()

Local lRet := .T.

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura038Efe

Realiza a efetiva��o.

@author Enaldo Cardoso Junior
@since 15/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura038Efe()

Local aArea     := GetArea()
Local cAliasG6I := GetNextAlias()
Local cMsg		:= ""
Local cMsg2		:= ""

Local lRet	 	:= .T.

If SoftLock("G6H")
	BeginSql Alias cAliasG6I
		SELECT G6I_FILIAL, G6I_CONCIL, G6I_SITUAC, G6I_NUMCIA, G6I_FATURA, G6I_ITEM, G6I_META
		FROM %Table:G6H% G6H 
		INNER JOIN %Table:G6I% G6I ON G6I.G6I_FILIAL = G6H.G6H_FILIAL AND G6I.G6I_FATURA = G6H.G6H_FATURA AND G6I.G6I_STATUS <>  '2' AND G6I.%NotDel%
		WHERE G6H_FATURA = %Exp:G6H->G6H_FATURA% AND G6H.%NotDel%
	EndSql
		
	While (cAliasG6I)->(!Eof()) .And. lRet
		If (cAliasG6I)->G6I_SITUAC <> "3"
			Help( , , "TA038EFET", , I18N(STR0061, {(cAliasG6I)->G6I_CONCIL}), 1, 0 ) //"N�o ser� poss�vel efetivar a fatura, a concilia��o #1[Num Conciliacao]# possui itens pendentes de aprova��o."
			lRet := .F.
		EndIf
		
		If lRet .And. Empty((cAliasG6I)->G6I_NUMCIA)
			Help( , , "TA038EFET", , STR0070, 1, 0)	// "Associa��es n�o realizadas. Por favor preencha o campo num cia (G6I_NUMCIA) dos itens da fatura"
			lRet := .F.
		EndIf
						
		//Verifica se � item de concilia��o de meta. 
		If lRet .And. (cAliasG6I)->G6I_META == "1"	
			G6M->(DbSetOrder(2)) //G6M_FILCON+G6M_FATMET+G6M_ITFAT+G6M_CONCIL
			If G6M->(!DbSeek((cAliasG6I)->(G6I_FILIAL + G6I_FATURA + G6I_ITEM + G6I_CONCIL)))
				lRet  := .F.
				cMsg  := STR0114 + AllTrim((cAliasG6I)->G6I_FATURA + "/" + (cAliasG6I)->G6I_ITEM) + STR0115		// "O item da fatura "  ###  //" est� designado como meta, mas n�o est� conciliado." 
				cMsg2 := STR0116	//"Verifique este item na op��o de Associa��o Manual da rotina de Fatura A�rea." 
				FwAlertHelp(cMsg, cMsg2)
			EndIf
		EndIf
		(cAliasG6I)->(DbSkip())
	EndDo
	(cAliasG6I)->(DbCloseArea())
		
	If lRet
		If Empty(G6H->G6H_SERIE) .Or. Empty(G6H->G6H_CONDPG)
			MsgInfo(STR0068)	// "Efetiva��o n�o permitida, por favor preencher os campos serie (G6H_SERIE) e condi��o de pagamento (G6H_CONDPG) da fatura."
			lRet := .F.
		EndIf
	
	EndIf

	If !lRet
		MsgInfo(STR0062) 		// "Efetiva��o n�o permitida. Por favor verifica item pendente da fatura."
	Else
		TuraEfetiv("1", "2")	// "Gerar", "A�reo"
	EndIf
	
	G6H->(MsUnLock())
EndIf	

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura038DEf()

Desfaz a efetiva��o.

@author Enaldo Cardoso Junior
@since 04/04/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura038DEf()

Local aArea := GetArea()
Local lRet  := .F.

If G6H->G6H_EFETIV == "2"	//N�o Conciliada
	MsgInfo(STR0063) 		// "N�o permitido desfazer a efetiva��o de faturas que n�o foram efetivadas."
	lRet := .F.
Else
	If SoftLock("G6H")
		TuraEfetiv("2", "2")	// "Estornar", "A�reo"
		lRet := .T.
		G6H->(MsUnLock())
	EndIf
EndIf

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura038Asso

Confirma a gera��o da associa��o automatica.

@author Enaldo Cardoso Junior
@since 08/02/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tu038Asso( lAuto, aItemG6I )

Local aArea    := GetArea()	
Local lRet     := .T.
Local cFatura  := ""
Local nTotConc := 0
Local cDirFile := GetMv("MV_DIRFTF", , "C:\TOTVS\") + "Log\"
Local cArqLog  := "Associacao" + FwTimeStamp() + ".log"
Local nLogId   := 0
	
Default aItemG6I := {}
Default lAuto	 := .F.
	
_cT38Msg := ""
	
If SoftLock("G6H")
	G6H->(DbSetOrder(1))	// G6H_FILIAL+G6H_FATURA
	If G6H->(DbSeek(xFilial("G6H") + G6H->G6H_FATURA))
		cFatura := G6H->G6H_FATURA	
	EndIf

	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM
	If G6I->(DbSeek(xFilial("G6I") + cFatura))
		While G6I->(!Eof()) .And. G6I->G6I_FATURA == cFatura
			If Empty(G6I->G6I_NUMCIA)
				Help(,,STR0054,,STR0070,1,0)	//"Associa��es n�o realizadas,"Por favor preencha o campo num cia (G6I_NUMCIA) dos itens da fatura"
				lRet := .F.
				Exit
			EndIf
			G6I->(DbSkip())
		EndDo
	EndIf
	
	lRet := T38VlParAc()		
	
	If lRet
		If lAuto .Or. ApMsgYesNo( STR0064 + G6H->G6H_FATURA+" ?"  ) //"Confirma a gera��o da associa��o automatica da fatura "
			FwMakeDir(cDirFile)
			nLogId := FCreate(cDirFile + cArqLog)

			FwMsgRun( , {|oSelf| lRet := T38AutAss(@nTotConc, aItemG6I, oSelf)} , , STR0030)	//"Aguarde, gerando associa��es..."

			If lRet .And. nTotConc > 0
				FwMsgRun( , {|oSelf| lRet := T38AutAcr(G6H->G6H_FATURA, aItemG6I, oSelf)}, , STR0080) //"Aguarde, gerando acertos autom�ticos..."
				If lRet
					FwMsgRun( , {|| lRet := T38AtuSt(cFatura)}, , STR0128)	//"Atualizando status"
				EndIf
			EndIf				

			If lRet
				If !IsBlind()
					If nTotConc == 0
						FwAlertWarning(STR0096, STR0029) //"N�o foram encontrados registros de venda. Verifique o fornecedor de reporte e o c�digo dos bilhetes/localizadores."###"Associa��o Autom�tica"
					Else
						FwAlertSuccess(I18N(STR0092, {nTotConc}), STR0029)//"Processo finalizado com sucesso, foram incluida(s)/alterada(s) #1[ num. concilia��es ]# concilia��o(�es)."###"Associa��o Autom�tica"
					EndIf
				EndIf
			EndIf

			//Log de Erros 
			FWrite(nLogId, _cT38Msg, Len(_cT38Msg)) 
			FClose(nLogId)
			
			If !Empty(_cT38Msg) .And. FwAlertYesNo(STR0143, STR0054) 			// "Processo finalizado, mas ocorreram falhas. Deseja ver o LOG de envio de associa��o autom�tica?" 		"Aten��o"			
				ShellExecute("Open", cDirFile + cArqLog, "", GetTempPath(), 1)
			EndIf  
		EndIf		
	EndIf
	
	G6H->(MsUnLock())
EndIf

RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038VLE

Inicializador do Model validando a opera��o de exclus�o

@author Marcelo Cardoso Barbosa
@since 06/08/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038VLE(oModel)

Local aArea      := GetArea()
Local nOperation := oModel:GetOperation()
Local lRet       := .T.
Local lConcil    := .F.
Local cAliasG8X  := ""

//Exclus�o
If  lRet .And. nOperation == MODEL_OPERATION_DELETE 
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	If G6I->(DbSeek(xFilial("G6I") + G6H->G6H_FATURA))
		While G6I->(!Eof()) .And. G6I->G6I_FILIAL + G6I->G6I_FATURA == G6H->G6H_FILIAL + G6H->G6H_FATURA 
			If G6I->G6I_SITUAC == "3"
				lRet := .F.
				Help( , , "TURA038VLE", , STR0024, 1, 0 )		// Esta fatura n�o pode ser excluida, pois j� est� aprovada. Efetue a rotina de desaprova��o antes de excluir a fatura.
				Exit
			EndIf

			If !lConcil .And. !Empty(G6I->G6I_CONCIL)
				lConcil := .T.
			EndIf
			
			If lConcil
				lRet := .F.
				Help( , , "TURA038VLE", , STR0067, 1, 0 )  // "Existem itens associados, exclus�o n�o permitida."
				Exit			
			EndIf
			G6I->(DbSkip()) 
		EndDo
	EndIf
	
	//N�o permitir dele��o caso tenha pagamento antecipado gravado
	If lRet
		cAliasG8X := GetNextAlias()
		BeginSql Alias cAliasG8X
			SELECT Count(*) NREGS 
			FROM %Table:G8X% G8X 
			WHERE G8X_FILIAL = %xFilial:G8X% AND 
			      G8X_FATURA = %Exp:G6H->G6H_FATURA% AND 
			      G8X_FORNEC = %Exp:G6H->G6H_FORNEC% AND 
			      G8X_LOJA   = %Exp:G6H->G6H_LOJA% AND 
			      G8X_TIPOFA  = '1' AND 
			      %notDel%
		EndSql
		
		If (cAliasG8X)->NREGS > 0
			TurHelp(STR0145, STR0146, "TURA038VLE") //"N�o � poss�vel excluir fatura com Antecipa��o gerada."###"Exclua a Antecipa��o antes de excluir a fatura."
			lRet := .F.
		EndIf
		(cAliasG8X)->( DbCloseArea() )
	EndIf
EndIf

If lRet .And. nOperation == MODEL_OPERATION_UPDATE
	If G6H->G6H_EFETIV = "1" .And. !FwIsInCallStack("TA039DEfe")
		Help( , , "TURA038VLE", , STR0066, 1, 0)  // "Fatura j� efetivada, altera��o n�o permitida."
		lRet := .F.
	EndIf	
EndIf

If lRet .And. nOperation == MODEL_OPERATION_DELETE
	If G6H->G6H_EFETIV = "1" .And. !FwIsInCallStack("TA039DEfe")
		Help( , , "TURA038VLE", , STR0066, 1, 0) 	// "Fatura j� efetivada, altera��o n�o permitida."
		lRet := .F.
	EndIf		
EndIf

RestArea(aArea) 

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038VLV
Inicializador do Model validando o valor total da Fatura
@author Marcelo Cardoso Barbosa
@since 06/08/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038VLV(oModel)

Local aArea      := GetArea()
Local nOperation := oModel:GetOperation()
Local lRet		 := .T.

//Exclus�o
If  lRet .And. nOperation == MODEL_OPERATION_DELETE .And. __nOper == 0
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	If G6I->(DbSeek(xFilial("G6I") + G6H->G6H_FATURA))	
		While G6I->(!Eof()) .And. G6I->G6I_FILIAL + G6I->G6I_FATURA == G6H->G6H_FILIAL + G6H->G6H_FATURA
			If G6I->G6I_SITUAC == "3"
				lRet := .F.
				Help( , , "TURA038VLV", , STR0024, 1, 0)	//Esta fatura n�o pode ser excluida, pois j� est� aprovada. Efetue a rotina de desaprova��o antes de excluir a fatura.
				Exit
			EndIf
			G6I->(DbSkip()) 
		EndDo
	EndIf
EndIf

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038VLA
Executa a valida��es na importa��o da Fatura A�rea
@author Marcelo Cardoso Barbosa
@since 10/08/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038VLA(lInterface, aVetor, xAdic, oModel)

Local aArea := GetArea()
Local lRet	:= .T.

If ctura038Fl <> NIL
 	G6H->(DbSetOrder(5))	// G6H_FILIAL + G6H_ARQUIV
	If G6H->(DbSeek(xFilial("G6H") + ctura038Fl)) .And. !Empty(AllTrim(ctura038Fl))
		Help( , , "TURA038VLA", , STR0026 + Upper(ctura038Fl), 1, 0) // N�o � poss�vel importar o arquivo, pois j� existe uma fatura gravada a partir de um arquivo com este nome: 
		lRet       := .F.
		lTURA038OK := .F.
		G6H->(RollBackSX8())
	EndIf
EndIf

RestArea(aArea) 

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta038Refsh

Atualiza o Browse.

@author Enaldo Cardoso Junior
@since 11/04/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta038Refsh()
	If ValType(oBrowse) == "O"
		oBrowse:Refresh()
	EndIf
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tr38VldSerie

Valida��o do campo G6H_SERIE.

@author Enaldo Cardoso Junior
@since 25/04/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tr38VldSerie()

Local aArea := GetArea()
Local lRet  := .T.

SX5->(DbSetOrder(1))	// X5_FILIAL+X5_TABELA+X5_CHAVE
If SX5->(!DbSeek(xFilial("SX5") + "01" + M->G6H_SERIE))
	lRet := .F.
EndIf

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038MOE

Fun��o que retorna o c�digo correto da moeda para arquivo BSP

@author Enaldo Cardoso Junior
@since 05/05/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038MOE(xA)

Local aArea  := GetArea()
Local cMoeda := ""

G5T->(DbSetOrder(4))	// G5T_FILIAL+G5T_SIMBOL
If G5T->(DbSeek(xFilial("G5T") + xA))
	cMoeda := G5T->G5T_CODIGO
EndIf
	
RestArea(aArea)
	
Return cMoeda

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038ClearCpo()

Fun��o limpa os campos na troca do tipo do item.

@type function
@author Inova��o;
@since 21/06/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038ClearCpo()

Local oModel	:= FwModelActive()
Local oItensGI6	:= oModel:GetModel("G6IDETAIL")
Local cTipoIt	:= oItensGI6:GetValue("G6I_TIPOIT")

oItensGI6:ClearField("G6I_TARIFA")
oItensGI6:ClearField("G6I_CODTAX")
oItensGI6:ClearField("G6I_TAXAS")
oItensGI6:ClearField("G6I_TAXADU")
oItensGI6:ClearField("G6I_PERCOM")
oItensGI6:ClearField("G6I_VLRCOM")
oItensGI6:ClearField("G6I_VLRINC")
oItensGI6:ClearField("G6I_VLRLIQ")
	
Return cTipoIt

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038TpOper()

Fun��o para obter o tipo de opera��o.

@type function
@author Enaldo Cardoso Junior
@since 27/05/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038TpOper()

Local oModel	:= FwModelActive()
Local oItensGI6	:= oModel:GetModel("G6IDETAIL")
Local nVlrLiq 	:= 0
Local cTipo	  	:= ""
Local cTipoIt	:= oItensGI6:GetValue("G6I_TIPOIT")
Local cTpFOP	:= FwFldGet("G6I_TPFOP")

If cTpFOP == "1"	
	nVlrLiq := + FwFldGet("G6I_TARIFA") + FwFldGet("G6I_TAXAS") - FwFldGet("G6I_TXREE") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC")	
ElseIf cTpFOP == "2"
	nVlrLiq := - FwFldGet("G6I_TAXADU") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC") + FwFldGet("G6I_TXCCDU")
ElseIf cTpFOP == "3"
	nVlrLiq := + FwFldGet("G6I_TARAVI") + FwFldGet("G6I_TAXAS") - FwFldGet("G6I_TXREE") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC")
EndIf
	
If nVlrLiq >= 0
	If cTipoIt	$ "3|5" //REEMBOLSO/ACM	
		cTipo := "2" //Cr�dito
	Else 
		cTipo := "1" //D�bito
	EndIf	
Else
	If cTipoIt	$ "3|5" //REEMBOLSO/ACM	
		cTipo := "1" //D�bito
	Else 			
		cTipo := "2" //Cr�dito
	EndIf	
EndIf

Return cTipo

//------------------------------------------------------------------------------
/*/{Protheus.doc} TURA38PreVl

Realiza pr� valida��es na linha do Grid. 

@sample     TURA38PreVl(oMdl, nLine, cAcao, cCampo)
@param      [oMdl],objeto,Modelo de Dados a ser utilizado para a grava��o dos dados.
@param      [nLine],numerico,Linha que est� sendo editada.
@param      [cAcao],caractere,Eventos executados: DELETE;UNDELETE;CANSETVALUE,SETVALUE,ETC.
@param      [cCampo],caractere,Nome do campo sendo editado.
@Return     logico    Resultado da Vali��o 
@version    P12
/*/
//------------------------------------------------------------------------------
Static Function TURA38PreVl(oModel, nLinha, cAcao, cCampo)

Local lRet := .T.

If cAcao == "CANSETVALUE" .And. !Empty(oModel:GetValue("G6I_CONCIL"))
	lRet := .F.
ElseIf cAcao == "DELETE" .And. !Empty(oModel:GetValue("G6I_CONCIL"))
	lRet := .F.
	Help(, , "TURA38PreVl", , STR0077, 1, 0)	// "Item associado, dele��o n�o permitida."
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038TpFOP()

Fun��o para obter o tipo de FOP.

@type function
@author Osmar Cioni
@since 27/07/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038TpFOP()

Local oModel	:= FwModelActive()
Local oItensGI6	:= oModel:GetModel("G6IDETAIL")
Local cTipoFop 	:= "0"

If (- FwFldGet("G6I_TAXADU") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC") + FwFldGet("G6I_TXCCDU")) == FwFldGet("G6I_VLRLIQ") .Or. ;
   (- FwFldGet("G6I_TAXADU") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC") + FwFldGet("G6I_TXCCDU")) == FwFldGet("G6I_VLRLIQ") * -1
	cTipoFop := "2" //Cart�o
	
ElseIf (+ FwFldGet("G6I_TARIFA") + FwFldGet("G6I_TAXAS") - FwFldGet("G6I_TXREE") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC")) == FwFldGet("G6I_VLRLIQ") .Or. ;
	   (+ FwFldGet("G6I_TARIFA") + FwFldGet("G6I_TAXAS") - FwFldGet("G6I_TXREE") - FwFldGet("G6I_VLRCOM") - FwFldGet("G6I_VLRINC")) == FwFldGet("G6I_VLRLIQ") * -1
	cTipoFop := "1" //Intermedia��o
Else		
	cTipoFop := "0" //Desconhecida
EndIf		
	
Return cTipoFop

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38VlParAc()

Fun��o para validar os par�metros utilizados na gera��o de acertos.

@type function
@author Anderson Toledo
@since 13/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38VlParAc()

Local aArea  := GetArea()
Local lRet 	 := .T.
Local cError := STR0081 + CRLF //"Solicite ao administrador do sistema o verificar os itens abaixo: " 

lRet := T38VldPrm("MV_TURACDU", "C11", @cError)
lRet := T38VldPrm("MV_TURACCM", "F01", @cError) .And. lRet
lRet := T38VldPrm("MV_TURACIN", "F02", @cError) .And. lRet
lRet := T38VldPrm("MV_TURACTF", "F03", @cError) .And. lRet

If Empty(SuperGetMV("MV_TURACTX"))
	cError += I18N(STR0082, {"MV_TURACTX"}) + CRLF 	// "Par�metro '#1[ Parametro ]#': O par�metro n�o est� configurado."
	lRet := .F.
Else
	G3A->(DbSetOrder(1))	// G3A_FILIAL+G3A_CODIGO
	If G3A->(!DbSeek(xFilial("G3A") + SuperGetMV("MV_TURACTX")))
		cError += I18N(STR0083, {"MV_TURACTX", SuperGetMV("MV_TURACTX")}) + CRLF 	// "Par�metro '#1[ Parametro ]#': O c�digo de taxa '#2[ c�digo taxa ]#' n�o existe."	
		lRet := .F.
	EndIf	
EndIf
	
If !lRet
	Help( , , "T38VlParAc", , cError, 1, 0)	
EndIf
	
RestArea(aArea)	
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38VldPrm()
Fun��o para validar os campos do acordo utilizado na gera��o de acertos.
@type function
@author Anderson Toledo
@since 13/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38VldPrm(cParam, cClass, cError)

Local aArea     := GetArea()
Local cRev		:= ""
Local cVlrParam := ""
Local cTable	:= ""
Local cBlql		:= ""
Local cAcordo	:= ""
Local cRevisao	:= ""
Local cTpFee	:= ""
Local cForApl	:= ""
Local cTpVal	:= ""
Local cDtIni 	:= ""
Local cDtFim 	:= ""
Local cTpRegApl := ""
Local lRet      := .T.

If SubStr(cClass, 1, 1) == "F" //Acordos de fornecedor
	cTable	  := "G4W"
	cAcordo	  := "G4W_CODACO"
	cRevisao  := "G4W_CODREV"
	cBlql	  := "G4W_MSBLQL"
	cTpFee	  := "G4W_CLASSI"
	cForApl   := "G4W_FORAPL"
	cTpVal	  := "G4W_TPVAL"
	cTpCob	  := "G4W_DESCFT"
	cDtIni 	  := "G4W_VIGINI"
	cDtFim 	  := "G4W_VIGFIM"
	cTpRegApl := "2"
Else //Acordor de cliente
	cTable	  := "G5V"
	cAcordo	  := "G5V_CODACO"
	cRevisao  := "G5V_CODREV"
	cBlql	  := "G5V_MSBLQL"
	cTpFee	  := "G5V_TPFEE"
	cForApl	  := "G5V_FORAPL"
	cTpVal	  := "G5V_TPVAL"
	cTpCob	  := "G5V_FTSERV"
	cDtIni 	  := "G5V_VIGINI"
	cDtFim 	  := "G5V_VIGFIM"
	cTpRegApl := "1"
EndIf

If Empty(SuperGetMV(cParam)) 
	cError += I18N(STR0084, {cParam}) + CRLF  									// "Par�metro '#1[ Parametro ]#': O par�metro n�o est� configurado."
	lRet   := .F.
Else
	cVlrParam := PadR(SuperGetMV(cParam), TamSX3("G4W_CODACO")[1])
	cRev      := T38RevAco(SubStr(cClass, 1, 1), cVlrParam)

	If Empty(cRev)
		cError += I18N(STR0085, {cParam, cVlrParam}) + CRLF 					// "Par�metro '#1[ Parametro ]#': N�o foi encontrado revis�o para o acordo '#2[ Valor par�metro ]#' ."
		lRet   := .F.
	Else
		(cTable)->(DbSetOrder(1)) 	// G4W->G4W_FILIAL+G4W_CODACO+G4W_CODREV+G4W_MSBLQL		G5V->G5V_FILIAL+G5V_CODACO+G5V_CODREV
		If (cTable)->(DbSeek(xFilial(cTable) + cVlrParam + cRev))
			If (cTable)->&(cBlql) == "1" 		// Registro bloqueado
				cError += I18N(STR0105, {cParam, cVlrParam}) + CRLF 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' est� bloqueado para uso.'"
				lRet   := .F.
			EndIf

			If (cTable)->&(cTpFee) <> cClass 	// Classifica��o do acordo
				cError += I18N(STR0093, {cParam, cVlrParam, cClass}) + CRLF 	// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' n�o est� configurado com a classifica��o '#3[ classifica��o ]#.'"
				lRet   := .F.
			EndIf

			If (cTable)->&(cForApl) <> "1" 		// Forma de aplica��o
				cError += I18N(STR0086, {cParam, cVlrParam}) + CRLF 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' n�o est� configurado para aplica��o manual."
				lRet   := .F.			
			EndIf
	
			If (cTable)->&(cTpVal) <> "2" 		// Tipo de valor
				cError += I18N(STR0087, {cParam, cVlrParam}) + CRLF 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' n�o est� configurado para valor fixo."
				lRet   := .F.		
			EndIf
	
			If (cTable)->&(cTpCob) <> "1" 		// Verifica se a cobran�a � na fatura
				cError += I18N(STR0088, {cParam, cVlrParam}) + CRLF 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' n�o est� configurado para aplica��o na fatura."
				lRet   := .F.
			EndIf

			If (cTable)->&(cDtIni) > dDataBase // Inicio data vig�ncia
				cError += I18N(STR0089, {cParam, cVlrParam}) + CRLF				// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' data inicial da vig�ncia do acordo maior que data base."
				lRet   := .F.	
			EndIf

			If (cTable)->&(cDtFim) < dDataBase // Final data vig�ncia
				cError += I18N(STR0090, {cParam, cVlrParam}) + CRLF	 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' data final da vig�ncia do acordo menor que data base."
				lRet   := .F.		
			EndIf

			G4X->(DbSetOrder(4))	// G4X_FILIAL+G4X_TPACOR+G4X_CODACO+G4X_CODREG
			If G4X->(DbSeek(xFilial("G4X") + cTpRegApl + (cTable)->&(cAcordo + cRevisao)))
				cError += I18N(STR0097, {cParam, cVlrParam}) + CRLF	 			// "Par�metro '#1[ Parametro ]#': Acordo '#2[ Valor par�metro ]#' possui regra de aplica��o, retire as regras de aplica��o deste acordo."
				lRet   := .F.
			EndIf
		Else
			cError += I18N(STR0091, {cParam, cVlrParam}) + CRLF 				// "N�o foi encontrado o acordo do par�metro '#1[ Parametro ]#' com c�digo '#2[ Valor par�metro ]#'."
			lRet   := .F.
		EndIf
	EndIf
EndIf

RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38RevAco()

Fun��o para obter a �ltima vers�o de um acordo

@type function
@author Anderson Toledo
@since 13/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38RevAco(cTpAco, cCodAco)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias()
Local cRet	    := ""

If cTpAco == "F"
	BeginSql Alias cAliasAux
		SELECT Max(G4W_CODREV) G4W_CODREV
		FROM %Table:G4W%
		WHERE G4W_FILIAL = %xFilial:G4W% AND G4W_CODACO = %Exp:cCodAco% AND %notDel%
	EndSql	   
	If (cAliasAux)->(!Eof())
		cRet := (cAliasAux)->G4W_CODREV
	EndIf
Else
	BeginSql Alias cAliasAux
		SELECT Max(G5V_CODREV) G5V_CODREV
			FROM %Table:G5V%
			WHERE G5V_FILIAL = %xFilial:G5V% AND G5V_CODACO = %Exp:cCodAco% AND %notDel%
	EndSql
	
	If (cAliasAux)->(!Eof())
		cRet := (cAliasAux)->G5V_CODREV
	EndIf
EndIf
(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return cRet 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur038Ls
(long_description)
@type function
@author osmar.junior
@since 10/10/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Tur038Ls(oModel)

Local oMdl	  := oModel
Local oMdlG6I := Nil
Local cStatus := ""
Local cRet    := ""
	
If oMdl:GetId() == "TURA038"
	oMdlG6I := oMdl:GetModel("G6IDETAIL")
	If oMdlG6I:GetLine() == 0
		cStatus := G6I->G6I_STATUS
	Else
		cStatus := oMdlG6I:GetValue("G6I_STATUS")
	EndIf
	
	If FwIsInCallStack("AddLine") .Or. Empty(cStatus)
		cRet := "BR_VERDE"
	Else
		cRet := "BR_VERMELHO"
	EndIf
EndIf
	
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta38CancIt
(long_description)
@type function
@author osmar.junior
@since 10/10/2016
@version 1.0
@param oView, objeto, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Ta38CancIt(oView)

Local oMdlG6H   := oView:GetModel("G6HMASTER")
Local oMdlG6I   := oView:GetModel("G6IDETAIL")
Local nLinhaG6I := oMdlG6I:GetLine()
Local nI        := 0
	
For nI := 1 to oMdlG6I:Length()
	If oMdlG6I:GetValue("G6I_MARK", nI)
		oMdlG6I:GoLine(nI)											
		
		If Empty(oMdlG6I:GetValue("G6I_CONCIL"))
		
			If oMdlG6I:GetValue("G6I_STATUS") == "1" 	//Ativo		
				oMdlG6I:LoadValue("G6I_STATUS", "2")
				oMdlG6I:LoadValue("G6I_LEGCON", Tur038Leg(oMdlG6I))

				If oMdlG6I:GetValue("G6I_TPOPER") == "1" //1=DEBITO |2=CREDITO
					oMdlG6H:LoadValue("G6H_VALOR", oMdlG6H:GetValue("G6H_VALOR") - oMdlG6I:GetValue("G6I_VLRLIQ"))
				Else
					oMdlG6H:LoadValue("G6H_VALOR", oMdlG6H:GetValue("G6H_VALOR") + oMdlG6I:GetValue("G6I_VLRLIQ"))
				EndIf	
			Else	//Cancelado		
				oMdlG6I:LoadValue("G6I_STATUS", "1")	
				oMdlG6I:LoadValue("G6I_LEGCON", Tur038Leg(oMdlG6I))

				If oMdlG6I:GetValue("G6I_TPOPER") == "1" //1=DEBITO |2=CREDITO
					oMdlG6H:LoadValue("G6H_VALOR", oMdlG6H:GetValue("G6H_VALOR") + oMdlG6I:GetValue("G6I_VLRLIQ"))
				Else
					oMdlG6H:LoadValue("G6H_VALOR", oMdlG6H:GetValue("G6H_VALOR") - oMdlG6I:GetValue("G6I_VLRLIQ"))
				EndIf		
			EndIf		
			oMdlG6I:LoadValue("G6I_MARK", .F.)
		Else
			FwAlertError(STR0102, STR0054)	//"N�o permitido, pois o item esta em concilia��o." //"Aten��o"
		EndIf				
	EndIf
Next nI
	
oMdlG6I:GoLine(nLinhaG6I)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TUR38ABIAT
(long_description)
@type function
@author osmar.junior
@since 19/10/2016
@version 1.0
@param cCia, character, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TUR38ABIAT(cCia)

Local aArea     := GetArea()
Local cAbIata   := "99"
Local cAliasG4S := GetNextAlias()

BeginSql Alias cAliasG4S
	SELECT G4S_ABIATA
	FROM %Table:G4S% G4S
	WHERE G4S.G4S_FILIAL = %xFilial:G4S% AND G4S.G4S_IATA = %Exp:AllTrim(cCia)% AND G4S.%NotDel%
EndSql

If (cAliasG4S)->(!Eof())
	cAbIata := (cAliasG4S)->G4S_ABIATA
EndIf

(cAliasG4S)->(DbCloseArea())

RestArea(aArea)

Return cAbIata

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura038Ex
(long_description)
@type function
@author osmar.junior
@since 01/11/2016
@version 1.0
@param oModelG6I, objeto, (Descri��o do par�metro)
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Tura038Ex(oModelG6I)

Local aArea     := GetArea()
Local nTarifa   := oModelG6I:GetValue("G6I_TARIFA")
Local nTaxas	:= oModelG6I:GetValue("G6I_TAXAS")
Local nTaxaDu	:= oModelG6I:GetValue("G6I_TAXADU")
Local nCCDU	    := oModelG6I:GetValue("G6I_TXCCDU")
Local nComis	:= oModelG6I:GetValue("G6I_VLRCOM")
Local nIncent	:= oModelG6I:GetValue("G6I_VLRINC")
Local nReemb	:= oModelG6I:GetValue("G6I_VLRREE")
Local nTxREE	:= oModelG6I:GetValue("G6I_TXREE")
Local nVlrLiq	:= oModelG6I:GetValue("G6I_VLRLIQ")
Local cTipoItem	:= oModelG6I:GetValue("G6I_TIPOIT")

If 	Empty(nTarifa) .And. Empty(nTaxas) .And. Empty(nTaxaDu) .And. Empty(nCCDU)   .And. Empty(nComis) .And. ;
    Empty(nIncent) .And. Empty(nReemb) .And. Empty(nTxREE)  .And. Empty(nVlrLiq) .And.  cTipoItem $ "1|2"
	
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	IF G6I->(DbSeek(xFilial("G6I") + oModelG6I:GetValue("G6I_FATURA") + oModelG6I:GetValue("G6I_ITEM")))
		RecLock("G6I", .F.)
		G6I->(DbDelete())
		G6I->(MsUnLock())
	EndIf	
EndIf

RestArea(aArea)

Return 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur38ExIAT
(long_description)
@type function
@author osmar.junior
@since 21/12/2016
@version 1.0
@param oModelG6I, objeto, (Descri��o do par�metro)
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Tur38ExIAT(oModelG6I,aIATA)

Local aArea   := GetArea()
Local nIndice := 0

nIndice := aScan(aIATA, {|X| X == oModelG6I:GetValue("G6I_IATA")})

If nIndice == 0
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	IF G6I->(DbSeek(xFilial("G6I") + oModelG6I:GetValue("G6I_FATURA") + oModelG6I:GetValue("G6I_ITEM")))		
		RecLock("G6I", .F.)			
		G6I->(DbDelete())
		G6I->(MsUnLock())
	EndIf
EndIf	

RestArea(aArea)

Return 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TLBoxIATA
(long_description)
@type function
@author osmar.junior
@since 22/12/2016
@version 1.0
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TLBoxIATA()

Local oDlg      := Nil
Local oOk       := LoadBitmap(GetResources(), "LBOK") //CHECKED    //LBOK  //LBTIK
Local oNo       := LoadBitmap(GetResources(), "LBNO") //UNCHECKED  //LBNO
Local oChk      := Nil
Local oLbx 	    := Nil
Local aVetor    := {}
Local cVar      := ""
Local cReturn   := ""
Local cG3MCod   := GetSX3Cache("G3M_CODIGO", "X3_TITULO")
Local cG3MDesc  := GetSX3Cache("G3M_DESCR" , "X3_TITULO")
Local cG3MNIata := GetSX3Cache("G3M_NIATA" , "X3_TITULO")
Local cG3MPcc   := GetSX3Cache("G3M_PCC"   , "X3_TITULO")
Local lChk	    := .F.
Local lMark     := .F.

IF MV_PAR10 == 2
	G3M->(DbSetOrder(1))	// G3M_FILIAL+G3M_CODIGO
	G3M->(DbGoTop())
	While G3M->(!Eof())
		If G3M->(FieldPos("G3M_MSBLQL")) > 0 .And. G3M->G3M_MSBLQL == "2"
			aAdd(aVetor, {lMark, G3M->G3M_CODIGO, G3M->G3M_DESCR, G3M->G3M_NIATA, G3M->G3M_PCC})
		EndIf
		G3M->(DbSkip())
	EndDo
	
	DEFINE MSDIALOG oDlg TITLE STR0108 FROM 0,0 TO 270,700 PIXEL		//"Sele��o de Moedas"
	   
	@ 10,10 LISTBOX oLbx VAR cVar FIELDS HEADER " ", cG3MCod, cG3MDesc, cG3MNIata, cG3MPcc SIZE 330,095 OF oDlg PIXEL ON dblClick(aVetor[oLbx:nAt, 1] := !aVetor[oLbx:nAt, 1], oLbx:Refresh())
	
	oLbx:SetArray(aVetor)
	oLbx:bLine := {|| {IIF(aVetor[oLbx:nAt, 1], oOk, oNo), ;
		                   aVetor[oLbx:nAt, 2], ;
		                   aVetor[oLbx:nAt, 3], ;
	                       aVetor[oLbx:nAt, 4], ;
	                       aVetor[oLbx:nAt, 5]}}
		 
	@ 110,10 CHECKBOX oChk VAR lChk PROMPT STR0106 + "/" + STR0107 SIZE 60,007 PIXEL OF oDlg; 		//Marca/Desmarca
	          ON CLICK(IIF(lChk, TLbxMarca(@oLbx, @aVetor, lChk), TLbxMarca(@oLbx, @aVetor, lChk)))
	
	DEFINE SBUTTON FROM 107,213 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg
	
	ACTIVATE MSDIALOG oDlg CENTER
	
	aEval(aVetor, {|X| IIF(X[1] == .T., cReturn += SUBSTR(X[4], 1, 8) + "|", )})
	MV_PAR11 := cReturn
EndIf

RestArea(aArea)

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TLbxMarca
(long_description)
@type function
@author osmar.junior
@since 22/12/2016
@version 1.0
@param oLbx, objeto, (Descri��o do par�metro)
@param aVetor, array, (Descri��o do par�metro)
@param lMarca, ${param_type}, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TLbxMarca(oLbx, aVetor, lMarca)

Local nX := 0

For nX := 1 To Len(aVetor)
   aVetor[nX][1] := lMarca
Next nX

oLbx:Refresh()

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA38SetMeta

Configura os itens da fatura selecionados para fazer parte do processo de Concilia��o Meta

@type function
@author Fernando Radu Muscalu
@since 12/12/2016
@version 1.0

@param	oView: objeto, Objeto FWFormGrid
@Return ${nil}, ${Sem retorno}

@example
{|oView| TA38SetMeta(oView)}

@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA38SetMeta(oView, lDesfaz)

Local oMdlG6I := oView:GetModel("G6IDETAIL") 
Local nI      := 0

Default lDesfaz	:= .F.

For nI := 1 to oMdlG6I:Length()
	If !oMdlG6I:IsDeleted() .And. oMdlG6I:GetValue("G6I_MARK", nI) .And. Empty(oMdlG6I:GetValue("G6I_CONCIL", nI))
		oMdlG6I:GoLine(nI)
		
		If !lDesfaz	
			oMdlG6I:LoadValue("G6I_META", "1")
			oMdlG6I:LoadValue("G6I_LEGCON", Tur038Leg(oMdlG6I))
		Else
			oMdlG6I:LoadValue("G6I_META", "2")
			oMdlG6I:LoadValue("G6I_LEGCON", Tur038Leg(oMdlG6I))
		EndIf
	EndIf
Next nI

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta38Struct

Adiciona campos para as estruturas que s�o passadas via par�metro.

@type function
@author Fernando Radu Muscalu
@since 12/12/2016
@version 1.0

@param	cTable: caractere, Nome da Tabela
		oStruct: objeto, objeto da classe FwFormModelStruct ou FwFormViewStruct (dependendo do conte�do do
				par�metro cModelOrView
		cModelOrView: caractere, "M" - Model; "V" - View		
@Return ${nil}, ${Sem retorno}

@example
Ta38Struct("G6I",oStruG6I,"M")
Ta38Struct("G6I",oStruG6I,"V")

@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta38Struct(cTable, oStruct, cModelOrView)

If cModelOrView == "M"
	If cTable == "G6I"	
		oStruct:AddField("",;				//Descri��o (Label) do campo
						 "",;				//Descri��o Tooltip do campo//"Acima"
						 "G6I_MARK",;		//Identificador do campo
						 "L",;				//Tipo de dado
						 01,;				//Tamanho
						 00,;				//Decimal
						 nil,;				//Valid do campo
						 nil,;				//When do campo
						 {},;				//Lista de Op��es (Combo)
						 .F.,;				//Indica se campo � obrigat�rio
						 NIL,;				//inicializador Padr�o
						 .F.,;				//Indica se o campo � chave
						 .F.,;				//Indica se o campo pode receber um valor em uma opera��o update
						 .T.)				//Indica se o campo � virtual	
		
	EndIf
Else
	If cTable == "G6I"
		oStruct:AddField("G6I_MARK", "01", "", "", {}, "L", "@BMP", Nil, Nil, Nil, Nil, Nil, Nil, Nil, Nil, .T.)
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038CALC
Func�o que calcula o valor liquido e atribui em um campo virtual

@type function
@author Jacomo Lisa
@since 20/01/2017
@version 1.0

@param	oModel: Modelo de dados
		nTotalAtual: Valor atual atribuido no campo LIQUIDO
		xValor: Valor a ser considerado no calculo G6I_VLRLIQ
		lSomando: se ir� somar ou subtrair (Inclus�o ou dele��o de linha)
@Return ${nil}, ${Sem retorno}

@example
@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038CALC(oModel, nTotalAtual, xValor, lSomando)

Local nValor  := xValor
Local cTpOper := oModel:GetModel("G6IDETAIL"):GetValue("G6I_TPOPER")

If (lSomando .And. cTpOper == "2") .Or. (!lSomando .And. cTpOper <> "2")  
	nValor := xValor * -1
EndIf

Return nTotalAtual + nValor

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038PSLN

Func�o que calcula o valor liquido e atribui em um campo virtual

@type function
@author Jacomo Lisa
@since 20/01/2017
@version 1.0

@param	oModel: Modelo de dados
		nTotalAtual: Valor atual atribuido no campo LIQUIDO
		xValor: Valor a ser considerado no calculo G6I_VLRLIQ
		lSomando: se ir� somar ou subtrair (Inclus�o ou dele��o de linha)
@Return ${nil}, ${Sem retorno}

@example
@see (links_or_references)
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038PSLN(oMdl, nLine)
Return oMdl:SetValue("G6I_TPFOP", TA038TpFOP())

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta038DbClk
Fun��o utilizada para duplo clique e mostrar a legenda do campo 
@type Static Function
@author Jacomo Lisa
@since 23/01/2017
@version 1.0
@Return $.T.
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Ta038DbClk(oGrid,cField,nLineGrid,nLineModel)

If cField == "G6I_LEGCON"
	Tur038Leg(oGrid, .T.)
EndIf

Return .T. 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T038LEGBRW

Fun��o utilizada para verificar se a fatura possui concilia��o

@type Static Function
@author Jacomo Lisa
@since 23/01/2017
@version 1.0
@Return ${lRet} ,${lRet = retorna se encontrou concilia��o para a fatura posicionada}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T038LEGBRW(nOpc)

Local aArea     := GetArea()
Local cAliasG6I	:= GetNextAlias()
Local lNoAss	:= .F.
Local lAprov	:= .F.
Local lNoAprov	:= .F.
Local lRet		:= .F.
Default nOpc	:= 0

BeginSql Alias cAliasG6I
	SELECT Distinct 
		(CASE 
			WHEN G6J.G6J_APROVA IS NULL THEN '1' //N�o associado
			WHEN G6J.G6J_APROVA = '3'	THEN '2' //Totalmente Aprovado
			ELSE '3' //Parcialmente ou n�o aprovado
		END) G6J_APROVA
	FROM %Table:G6H% G6H 
	INNER JOIN %Table:G6I% G6I ON G6I.G6I_FILIAL = G6H.G6H_FILIAL AND G6I.G6I_FATURA = G6H.G6H_FATURA AND G6I.G6I_STATUS <> '2' AND G6I.%NotDel%
	LEFT JOIN  %Table:G6J% G6J ON G6J.G6J_FILIAL = G6H.G6H_FILIAL AND G6J.G6J_FATURA = G6H.G6H_FATURA AND G6J.G6J_CONCIL = G6I.G6I_CONCIL AND G6J.%NotDel%
	WHERE G6H.G6H_FILIAL = %Exp:G6H->G6H_FILIAL% AND G6H.G6H_FATURA = %Exp:G6H->G6H_FATURA% AND G6H.%NotDel%
EndSql

Do While (cAliasG6I)->(!Eof())
	If (cAliasG6I)->G6J_APROVA == '1'
		lNoAss	:= .T.
	Elseif (cAliasG6I)->G6J_APROVA == '2'
		lAprov	:= .T.
	Else
		lNoAprov:= .T.
	EndIf
	(cAliasG6I)->(DbSkip())
EndDo
(cAliasG6I)->(DbCloseArea())

IF nOpc == 1 //N�o Efetivada
	lRet :=  lAprov .And. !lNoAprov .And. !lNoAss
Elseif nOpc == 2 //"N�o Associado" 
	lRet := !lAprov .And. !lNoAprov .And. lNoAss
ElseIf nOpc == 3 //Em Conciliacao
	lRet := .T.
Else 
	lRet := .F.
EndIf

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038BRW
Fun��o utilizada para verificar se ir� ou n�o utilizar a legenda no browse
@type Static Function
@author Jacomo Lisa
@since 23/01/2017
@version 1.0
@Return 
@example TA038BRW(oBrowse)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038BRW(oBrowse)

Local aArea	  := GetArea()
Local oLegend := oBrowse:aLegends[1,2]
Local nPos	  := aScan(oLegend:aLegend, {|x| x[1] == "T038LEGBRW(1)"})

Pergunte("TA038BRW", .F.)

If MV_PAR01 == 1
	If nPos == 0
		aSize(oLegend:aLegend, 0)
		T038ADDLEG(oBrowse)
	EndIf
Else
	If nPos > 0
		aSize(oLegend:aLegend, 0)
		T038ADDLEG(oBrowse)
	EndIf
EndIf

oBrowse:Refresh()
RestArea(aArea)

Return .T.

//------------------------------------------------------------
/*/{Protheus.doc} T038ADDLEG
Fun��o que cria as legendas no browse
@type Static Function
@author Jacomo Lisa
@since 23/01/2017
@version 1.0
@Return 
@example TA038BRW(oBrowse)
@see (links_or_references)
/*///---------------------------------------------------------
Static Function T038ADDLEG(oBrowse)

oBrowse:AddLegend("G6H->G6H_EFETIV == '1'", "GREEN", STR0120, , .F.) // "Efetivada"

If TURExistX1("TA038BRW") 
	Pergunte("TA038BRW", .F.)
	If MV_PAR01 == 1
		oBrowse:AddLegend("T038LEGBRW(1)", "RED"   , STR0121, , .F.) // "Nao Efetivada"
		oBrowse:AddLegend("T038LEGBRW(2)", "BLUE"  , STR0129, , .F.) // "N�o Associado"
		oBrowse:AddLegend("T038LEGBRW(3)", "YELLOW", STR0127, , .F.) // "Em Concilia��o"
	Else
		oBrowse:AddLegend("G6H->G6H_EFETIV == '2'", "RED", STR0121, , .F.) // "Nao Efetivada"
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038DeCon()

Fun��o para exclus�o das concilia��es de uma fatura

@type function
@author Anderson Toledo
@since 11/04/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038DeCon(oMessage)

Local aArea     := GetArea()
Local oMdlConc 	:= Nil 
Local aConcil	:= Nil
Local nX		:= 0
Local lMessage	:= ValType(oMessage) == "O"

If G6H->G6H_EFETIV == "1"
	FwAlertHelp(STR0134, STR0135, "T38JAEFET")	//"Fatura j� efetivada."###"Estorne a efetiva��o para excluir as concilia��es."
	Return
EndIf

If SoftLock("G6H")
	G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA

	If !ApMsgYesNo(STR0136) //"Deseja excluir as concilia��es geradas?"
		Return
	EndIf
	
	aConcil	 := T38GetConc(G6H->G6H_FATURA)
	oMdlConc := FwLoadModel("TURA039MDL")
	oMdlConc:SetCommit({|oModel| TA039ACom(oModel, oMessage)})
	
	For nX := 1 To Len(aConcil)
		If lMessage
			oMessage:SetText(I18N(STR0137, {cValToChar(nX), cValToChar(Len(aConcil))})) //"Excluindo concilia��es #1[conc. atual]# de #2[total conc.]#"
			ProcessMessages()
		EndIf

		If G6J->(DbSeek(xFilial("G6J") + aConcil[nX]))
			oMdlConc:SetOperation(MODEL_OPERATION_DELETE)
			oMdlConc:Activate()
			
			If oMdlConc:VldData()
				oMdlConc:CommitData()
			Else
				FwAlertHelp(I18N(STR0138, {G6J->G6J_CONCIL}), TurGetErrorMsg(oMdlConc), "T38EXCLUICONC")	//"N�o foi poss�vel excluir a concilia��o #1 [Concilia��o]"### 	
			EndIf
			oMdlConc:DeActivate()
		EndIf
	Next nX
	oMdlConc:Destroy()

	G6H->(MsUnLock())
EndIf
	
RestArea(aArea)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038CC
(long_description)
@type function
@author osmar.junior
@since 22/12/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA038CC(oModel)

Local aArea     := GetArea()
Local nTarifa 	:= oModel:GetValue("G6I_TARIFA")
Local nTaxas	:= oModel:GetValue("G6I_TAXAS")
Local nTaxaDu	:= oModel:GetValue("G6I_TAXADU")
Local nCCDU		:= oModel:GetValue("G6I_TXCCDU")
Local nComis	:= oModel:GetValue("G6I_VLRCOM")
Local nIncent	:= oModel:GetValue("G6I_VLRINC")
Local nReemb	:= oModel:GetValue("G6I_VLRREE")
Local nTxREE	:= oModel:GetValue("G6I_TXREE")
Local nVlrLiq	:= oModel:GetValue("G6I_VLRLIQ")
Local cTipoItem	:= oModel:GetValue("G6I_TIPOIT")
Local nTarifaAV	:= oModel:GetValue("G6I_TARAVI")
Local nTarifaCC	:= oModel:GetValue("G6I_TARCRE")

If MV_PAR01 == 3 //AVIANCA
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	IF G6I->(DbSeek( xFilial("G6I") + oModel:GetValue("G6I_FATURA") + oModel:GetValue("G6I_ITEM")))		
		RecLock("G6I", .F.)	
		
		If oModel:GetValue("G6I_TIPOIT") == "3"
			If !Empty(oModel:GetValue("G6I_PAGCC"))	//CARTAO
				If oModel:GetValue("G6I_TARAVI") > 0 .And. oModel:GetValue("G6I_TARCRE") > 0
				 	G6I->G6I_VLRLIQ := nTarifaAV + nTaxas - nTxREE - nComis - nIncent 	// Comiss�o+Incentivo	
					oModel:SetValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)
					G6I->G6I_TPFOP := "2"				
				Else
					G6I->G6I_VLRLIQ := oModel:GetValue("G6I_VLRCOM") + oModel:GetValue("G6I_VLRINC") //Comiss�o+Incentivo
					oModel:SetValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)
					G6I->G6I_TPFOP := "2"
				EndIf
			Else				//FATURADO
				G6I->G6I_VLRLIQ := nTarifa + nTaxas - nTxREE - nComis - nIncent		//Tarifa Avista + Taxa - Multa - comiss�o - Incentivo
				oModel:SetValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)	
				G6I->G6I_TPFOP := "1"
			EndIf
			
		ElseIf oModel:GetValue("G6I_TIPOIT") $ "1|2"
			If !Empty(oModel:GetValue("G6I_PAGCC"))	//CARTAO
				G6I->G6I_VLRLIQ := (nTaxaDu - nCCDU) + nComis + nIncent			//Comiss�o+Incentivo			 	
				oModel:SetValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)
				G6I->G6I_TPFOP := "2"
			Else									//FATURADO
				If nTarifaAV <> 0 .Or. nTarifaCC <> 0
					G6I->G6I_VLRLIQ := nTarifa + nTaxas - nTxREE - nComis - nIncent	//Tarifa Avista + Taxa - Multa - comiss�o - Incentivo		 	
					oModel:SetValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)	
					G6I->G6I_TPFOP := "1"
				EndIf
			EndIf
		
		ElseIf oModel:GetValue("G6I_TIPOIT") == "5" .And. Empty(oModel:GetValue("G6I_TARIFA")) .And. Empty(oModel:GetValue("G6I_TAXAS")) .And. Empty(oModel:GetValue("G6I_TAXADU")) 
			G6I->G6I_VLRLIQ := oModel:GetValue("G6I_VLRCOM") + oModel:GetValue("G6I_VLRINC") //Comiss�o+Incentivo
			oModel:LoadValue("G6I_VLRLIQ", G6I->G6I_VLRLIQ)
			G6I->G6I_TPFOP := "1"
		EndIf	
						
		G6I->(MsUnLock())
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038PSMD

Fun��o utilizada para ap�s a importa��o da fatura atribuir a FOP

@type function
@author osmar.junior
@since 12/06/2017
@version 1.0
@param oMdl, objeto, (Descri��o do par�metro)
@Return ${Return}, ${Return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038PSMD(oModel)

Local oMdlG6I := oModel:GetModel("G6IDETAIL")
Local nI      := 0
	
If FwIsInCallStack("TURA038IMP")
	For nI := 1 To oMdlG6I:Length()
		oMdlG6I:GoLine(nI)
		TA038PSLN(oMdlG6I, nI)
	Next nI
EndIf 
	
FWFormCommit(oModel)
	
Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038TdOk

P�s valida��o do modelo.

@author paulo.barbosa
@since 19/04/2018
@version 1.0

@param oModel, object, Modelo da fatura a�rea
@type function
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038TdOk(oModel)

Local aArea     := GetArea()
Local lRet      := .T.
Local oModelG6H := oModel:GetModel("G6HMASTER")

//N�o permitir alterar valor caso ja exista pagamento/recebimento antecipado gerado.
G8X->(DbSetOrder(1))
If G8X->(DbSeek(xFilial("G8X") + "1" + oModelG6H:GetValue("G6H_FATURA") + oModelG6H:GetValue("G6H_FORNEC") + oModelG6H:GetValue("G6H_LOJA")))
	If (oModelG6H:GetValue("G6H_VALOR") <> G8X->G8X_VALOR .And. G8X->G8X_TIPO == "PA") .Or. (oModelG6H:GetValue("G6H_VALOR") * -1 <> G8X->G8X_VALOR .And. G8X->G8X_TIPO == "RA")
		lRet := .F.
		TurHelp(STR0147, STR0148, "TA038TdOk") //"N�o � poss�vel alterar o valor da fatura com Antecipa��o gerada."###"Exclua a Antecipa��o antes de alterar o valor."
	EndIf
EndIf

RestArea(aArea)

Return lRet