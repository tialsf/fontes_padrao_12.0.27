#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWEDITPANEL.CH"
#INCLUDE "TURA038A.CH"

#DEFINE GRIDMAXLIN 99999

Static oTblHeader := Nil //Tabela tempor�ria com os campos referente a pesquisa
Static oTblDetail := Nil //Tabela tempor�ria com os campos referente aos DR's encontrados
Static nLinMark   := 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Modelo de dados

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel	 := MPFormModel():New("TURA038A", /*bPreValidacao*/, /*bPosValid*/, {|oModel| TA038ACommit(oModel)}, {|oModel| FWFormCancel(oModel)})
Local oStructCab := FWFormModelStruct():New()
Local oStructDet := FWFormModelStruct():New()
Local bLoadCab	 := {|oField| T38ALoadCab(oField)}
Local bLoadGrid	 := {|oGrid|  T38ALoadGrid(oGrid)}

TA038ACrTb()

oStructCab:AddTable(oTblHeader:GetAlias(), , STR0070, {|| oTblHeader:GetRealName()}) //"Dados compara��o"
oStructDet:AddTable(oTblDetail:GetAlias(), , STR0071, {|| oTblDetail:GetRealName()}) //"Documentos de reserva"

AddFldCab(oStructCab, 1)
AddFldDet(oStructDet, 1)

AddGatilhos(oStructCab)

oModel:AddFields("TMP_MASTER", /*cOwner*/, oStructCab, /*<bPre >*/, /*<bPost >*/, bLoadCab)

oModel:AddGrid("TMP_DETAIL", "TMP_MASTER", oStructDet, {|oModelGrid, nLine, cAction, cField| TA038APre(oModelGrid, nLine, cAction, cField)}, /*bLinePost*/, /*bPre*/, /*bLinePost*/, bLoadGrid)
oModel:GetModel("TMP_DETAIL"):SetOptional(.T.)
oModel:SetRelation("TMP_DETAIL", {{"THREADID", cValToChar(ThreadId())}}, (oTblDetail:GetAlias())->(IndexKey(1)))
oModel:GetModel("TMP_DETAIL"):SetMaxLine(GRIDMAXLIN)

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)
oModel:SetPrimaryKey({})

Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

ViewDef

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	 := FWLoadModel("TURA038A")
Local oView		 := FWFormView():New()
Local oStructCab := FWFormViewStruct():New()
Local oStructDet := FWFormViewStruct():New()

oView:SetModel(oModel)

AddFldCab(oStructCab, 2)
AddFldDet(oStructDet, 2)

AddGroups(oStructCab)

oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER")
oView:AddGrid("VIEW_TMPD",  oStructDet, "TMP_DETAIL")

oView:AddUserButton(STR0049 + "-F5", "CLIPS", {|oView| FwMsgRun( , {|| TA038AQry(oView)} , , STR0001)}, , VK_F5)	//"Pesquisar"##"Pesquisando documentos..."
oView:AddUserButton(STR0048, "", {|oView| TA038AMark(oView), oView:Refresh()}) // "(Des)Marcar Todos"

oView:CreateHorizontalBox("SUPERIOR", 50)
oView:CreateHorizontalBox("INFERIOR", 50)

oView:SetOwnerView("VIEW_TMPM", "SUPERIOR")
oView:SetOwnerView("VIEW_TMPD", "INFERIOR")

oView:SetFieldAction("TMP_OK", {|oView, cIDView, cField, xValue| oView:Refresh()})

oView:SetCloseOnOk({|| .T.})
oView:ShowUpdateMsg(.F.)
oView:ShowInsertMsg(.F.)

Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Pre-valida��o

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038APre(oModelGrid, nLine, cAction, cField)

Local cMsgError  := STR0024 + CRLF //"Encontrada inconsist�ncias abaixo: "
Local lRet 		 := .T.
Local nX		 := 0
Local oTur038Mdl := Nil
Local aVldDR	 := {}

If !FwIsInCallStack("Ta38GIt")
	oTur038Mdl := Tur038Modelo()[1]:GetModel("G6IDETAIL")

	If cField == "TMP_OK"
		If Empty((aVldDR := T38CndAss(oTur038Mdl:GetValue("G6I_TIPOIT"))))
			FwAlertWarning(STR0047, STR0017) // "N�o existe configura��o para o tipo selecionado, n�o ser� poss�vel associar o item."###"Aten��o"
			lRet := .F.
		Else
			For nX := 1 To Len(aVldDR)
				If oModelGrid:HasField(aVldDR[nX][1])
					xTmpFld := oModelGrid:GetValue(aVldDR[nX][1])
					
					If aScan(aVldDR[nX][2], {|x| x ==  xTmpFld}) == 0
						cMsgError += STR0021 + oModelGrid:GetStruct():GetProperty(aVldDR[nX][1], 1) + CRLF 		//"Campo: "
						cMsgError += STR0022 + T38DscCpo(cValToChar(xTmpFld), aVldDR[nX][1]) + CRLF 			//"Valor atual: " 
						cMsgError += STR0023 + T38DscCpo(aVldDR[nX][2], aVldDR[nX][1]) + CRLF 					//"Valor(es) suportado(s): "
						FwAlertHelp(cMsgError, STR0069, "TA038APRE") 											//"Realize o ajuste no item da fatura ou no registro de venda."
						lRet := .F.
						Exit
					EndIf
				EndIf
			Next nX
		EndIf
	EndIf
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038AQry

Cria a consulta que retorna os documentos de reserva a serem associados.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038AQry()

Local aArea 	   := GetArea()
Local oView		   := FwViewActive()
Local oModel	   := FwModelActive()
Local oModelMaster := oView:GetModel("TMP_MASTER")
Local cAliasHead   := oTblHeader:GetAlias()
Local cAliasDtl    := oTblDetail:GetAlias()
Local cRealName	   := oTblDetail:GetRealName()
Local cFilterSA1   := ""
Local cFilterSA2   := ""
Local cFilterG3Q   := ""
Local cFilterG3R   := ""
Local cFilterG3M   := ""
Local nX		   := 0
Local nTam		   := 0

(cAliasDtl)->( __DbZap())
(cAliasHead)->( __DbZap())

RecLock(cAliasHead, .T.)
For nX := 1 To (oTblHeader:GetAlias())->(FCount())
	(cAliasHead)->(FieldPut(nX, oModelMaster:GetValue((cAliasHead)->(FieldName(nX))))) 
Next nX
(cAliasHead)->(MsUnlock())

//Filtros Where da tabela G3R
If !Empty(oModelMaster:GetValue("TMP_XEMIDE")) .And. !Empty(oModelMaster:GetValue("TMP_XEMIAT"))
	cFilterG3R += " AND G3R_EMISS BETWEEN '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "' AND '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "' "
ElseIf !Empty(oModelMaster:GetValue("TMP_XEMIDE")) .And. Empty(oModelMaster:GetValue("TMP_XEMIAT"))
	cFilterG3R += " AND G3R_EMISS >= '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "' "
ElseIf Empty(oModelMaster:GetValue("TMP_XEMIDE")) .And. !Empty(oModelMaster:GetValue("TMP_XEMIAT"))
	cFilterG3R += " AND G3R_EMISS <= '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "' "
EndIf 

If !Empty(oModelMaster:GetValue("TMP_XDOC"))
	cFilterG3R += " AND G3R_DOC LIKE '%" + AllTrim(oModelMaster:GetValue("TMP_XDOC")) + "%' "
EndIf

If !oModelMaster:GetValue("TMP_XCANC")
	cFilterG3R += " AND G3R_STATUS <> '4' "
EndIf

//Filtros Where da tabela SA2
nTam := cExpFil("SA2")
If nTam > 0
	cFilterSA2 := " AND SA2_REP.A2_FILIAL = SUBSTRING(G3RDET.G3R_FILIAL, 1, " + cValToChar(nTam) + ") " 
EndIf

If !Empty(oModelMaster:GetValue("TMP_XFORND"))
	cFilterSA2 +=  " AND SA2_REP.A2_COD >= '" + oModelMaster:GetValue("TMP_XFORND") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XLOJAD"))
	cFilterSA2 += " AND SA2_REP.A2_LOJA >= '" + oModelMaster:GetValue("TMP_XLOJAD") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XFORNA"))
	cFilterSA2 += " AND SA2_REP.A2_COD <= '" + oModelMaster:GetValue("TMP_XFORNA") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XLOJAA"))
	cFilterSA2 += " AND SA2_REP.A2_LOJA <= '" + oModelMaster:GetValue("TMP_XLOJAA") + "' "
EndIf

//Filtros Where da tabala G3Q
If !Empty(oModelMaster:GetValue("TMP_XPOSTD"))
	cFilterG3Q += " AND G3Q_POSTO >= '" + oModelMaster:GetValue("TMP_XPOSTD") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XPOSTA"))
	cFilterG3Q += " AND G3Q_POSTO <= '" + oModelMaster:GetValue("TMP_XPOSTA") + "' "
EndIf

If oModelMaster:GetValue("TMP_XREEMB") = "1" //Venda-> Emiss�o/Reemiss�o 
	cFilterG3Q += " AND G3Q_OPERAC <> '2' "
ElseIf oModelMaster:GetValue("TMP_XREEMB") = "2" //Reembolso
	cFilterG3Q += " AND G3Q_OPERAC = '2' "
EndIf

//Filtros Where da tabela SA1
nTam := cExpFil("SA1")
If nTam > 0
	cFilterSA1 := " AND SA1.A1_FILIAL = SUBSTRING(G3Q_FILIAL, 1, " + cValToChar(nTam) + ") " 
EndIf

If !Empty(oModelMaster:GetValue("TMP_XCLIDE"))
	cFilterSA1 += " AND A1_COD >= '" + oModelMaster:GetValue("TMP_XCLIDE") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XCLLJD"))
	cFilterSA1 += " AND A1_LOJA >= '" + oModelMaster:GetValue("TMP_XCLLJD") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XCLIAT"))
	cFilterSA1 += " AND A1_COD <= '" + oModelMaster:GetValue("TMP_XCLIAT") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XCLLJA"))
	cFilterSA1 += " AND A1_LOJA <= '" + oModelMaster:GetValue("TMP_XCLLJA") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XIATAD"))
	cFilterG3M += " AND G3M_DR.G3M_NIATA >= '" + oModelMaster:GetValue("TMP_XIATAD") + "' "
EndIf

If !Empty(oModelMaster:GetValue("TMP_XIATAA"))
	cFilterG3M += " AND G3M_DR.G3M_NIATA <= '" + oModelMaster:GetValue("TMP_XIATAA") + "' "
EndIf

If Empty(oModelMaster:GetValue("TMP_XIATAD")) .And. Empty(oModelMaster:GetValue("TMP_XIATAA"))
	nTam := cExpFil("G3M")
	If nTam > 0
		cFilterG3M += " AND G3M_DR.G3M_FILIAL = SUBSTRING(G3R_MSFIL, 1, " + cValToChar(nTam) + ") " 
	EndIf
EndIf

cSts := "INSERT INTO " + cRealName + "(THREADID, TMP_OK,G3R_MSFIL,G3R_NUMID,G3R_IDITEM,G3R_NUMSEQ,G3R_DOC,G3R_CONJUG,G3R_EMISS,G3R_TARIFA,G3R_TAXA,G3R_EXTRAS,G3R_VLCOMI,G3R_TAXADU,G3R_VLINCE,G3R_TOTREC,G3R_VLFIM,G3R_TPSEG,G3R_FORNEC,G3R_LOJA,G3R_MOEDA,G3R_TXREE,A2_NOME,G3Q_POSTO,G3M_DESCR,G3Q_CLIENT,G3Q_LOJA,A1_NOME,G3R_CONCIL,G3R_OPERAC,G3R_TPDOC,G3R_FILIAL,G3R_POSTOR,G3R_STATUS,G3R_TXFORN,G3R_FORREP,G3R_LOJREP,G3R_NOMREP) "
cSts +=		" SELECT " + cValToChar(ThreadId()) + " AS THREADID,'F',G3R_MSFIL,G3RDET.G3R_NUMID,G3RDET.G3R_IDITEM,G3RDET.G3R_NUMSEQ,G3R_DOC,G3R_CONJUG,G3R_EMISS,G3R_TARIFA,G3R_TAXA,G3R_EXTRAS,TMP2.G3R_VLCOMI,TMP2.G3R_TAXADU,TMP2.G3R_VLINCE,"
cSts +=		"   (TMP2.G3R_VLCOMI+TMP2.G3R_VLINCE+TMP2.G3R_TXFORN) G3R_TOTREC,"
cSts +=		"   (G3R_VLFIM + (G3R_TOTREC - (TMP2.G3R_VLCOMI+TMP2.G3R_VLINCE+TMP2.G3R_TXFORN))) G3R_VLFIM ,G3R_TPSEG,G3R_FORNEC,G3R_LOJA,G3R_MOEDA,G3R_TXREE,A2_NOME,G3Q_POSTO,G3M_IV.G3M_DESCR,G3Q_CLIENT,G3Q_LOJA,A1_NOME,G3R_CONCIL,G3R_OPERAC,G3R_TPDOC,G3RDET.G3R_FILIAL,G3R_POSTOR,G3R_STATUS,(TMP2.G3R_TXFORN*-1) G3R_TXFORN,G3R_FORREP,G3R_LOJREP,SA2_REP.A2_NOME AS G3R_NOMREP " 
cSts +=		" FROM ("
cSts +=		" SELECT G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, ISNULL(SUM(COMISSAO),0) G3R_VLCOMI, ISNULL(SUM(INCENTIVO),0) G3R_VLINCE, ISNULL(SUM(TAXAFOR),0) G3R_TXFORN, G3R_TAXADU FROM ("
cSts +=			" SELECT G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, G48_CLASS, G48_TPACD, G3R_TAXADU, "
cSts +=			" CASE WHEN G48_CLASS = 'F01' AND G48_TPACD = '1' THEN SUM(G48.G48_VLACD) WHEN G48_CLASS = 'F01' AND G48_TPACD = '2' THEN SUM(G48.G48_VLACD * -1) END COMISSAO, "
cSts +=			" CASE WHEN G48_CLASS = 'F02' AND G48_TPACD = '1' THEN SUM(G48.G48_VLACD) WHEN G48_CLASS = 'F02' AND G48_TPACD = '2' THEN SUM(G48.G48_VLACD * -1) END INCENTIVO, "
cSts +=			" CASE WHEN G48_CLASS = 'F03' AND G48_TPACD = '1' THEN SUM(G48.G48_VLACD) WHEN G48_CLASS = 'F03' AND G48_TPACD = '2' THEN SUM(G48.G48_VLACD * -1) END TAXAFOR "
cSts +=			" FROM " + RetSqlName("G3R") + " G3R "
cSts +=			" LEFT JOIN " + RetSqlName("G48") + " G48 "
cSts +=				" ON G48.G48_FILIAL = G3R.G3R_FILIAL "
cSts +=					" AND G48.G48_NUMID = G3R.G3R_NUMID "
cSts +=					" AND G48.G48_IDITEM = G3R.G3R_IDITEM "
cSts +=					" AND G48.G48_NUMSEQ = G3R.G3R_NUMSEQ "
cSts +=					" AND G48.G48_CLIFOR = '2' "
cSts +=					" AND G48.G48_STATUS = '1' "
cSts +=					" AND G48.G48_COMSER = '1'	"			
cSts +=					IIF(TCSrvType() == "AS/400", " AND G48.@DELETED@ = ' ' ", " AND G48.D_E_L_E_T_ = ' ' ")
cSts +=					" WHERE G3R.G3R_NUMSEQ = '01' " 
cSts +=					" AND G3R.G3R_TPSEG = '1' "
cSts +=					IIF(TCSrvType() == "AS/400", " AND G3R.@DELETED@ = ' ' ", " AND G3R.D_E_L_E_T_ = ' ' ")
cSts +=					" AND G3R.G3R_CONCIL = ' ' "
cSts +=					cFilterG3R
cSts +=				" GROUP BY G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, G48_CLASS, G48_TPACD, G3R_TAXADU) TMP1 "
cSts +=			" GROUP BY G3R_FILIAL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, G3R_TAXADU) TMP2 "
cSts +=			" INNER JOIN " + RetSqlName("G3R") + " G3RDET "
cSts +=				" ON G3RDET.G3R_FILIAL = TMP2.G3R_FILIAL "
cSts +=					" AND G3RDET.G3R_NUMID = TMP2.G3R_NUMID "
cSts +=					" AND G3RDET.G3R_IDITEM = TMP2.G3R_IDITEM "
cSts +=					" AND G3RDET.G3R_NUMSEQ = TMP2.G3R_NUMSEQ "			
cSts +=					IIF(TCSrvType() == "AS/400", " AND G3RDET.@DELETED@ = ' ' ", " AND G3RDET.D_E_L_E_T_ = ' ' ")
cSts +=		"	INNER JOIN " + RetSqlName("G3Q") + " G3Q "
cSts +=			" ON G3Q.G3Q_NUMID = G3RDET.G3R_NUMID "
cSts +=				" AND G3Q.G3Q_IDITEM = G3RDET.G3R_IDITEM "
cSts +=				" AND G3Q.G3Q_NUMSEQ = G3RDET.G3R_NUMSEQ "			
cSts +=				" AND G3Q.G3Q_FILIAL = G3RDET.G3R_FILIAL "
cSts += 			IIF(TCSrvType() == "AS/400", " AND G3Q.@DELETED@ = ' ' ", " AND G3Q.D_E_L_E_T_ = ' ' ")
cSts +=				cFilterG3Q
cSts +=		"	INNER JOIN " + RetSqlName("G3M") + " G3M_IV ON G3M_IV.G3M_FILIAL = G3Q_FILPST AND G3M_IV.G3M_CODIGO = G3Q.G3Q_POSTO AND " + IIF(TCSrvType() == "AS/400", "G3M_IV.@DELETED@ = ' ' ", "G3M_IV.D_E_L_E_T_ = ' ' ")
cSts +=		"	INNER JOIN " + RetSqlName("G3M") + " G3M_DR ON G3M_DR.G3M_CODIGO = G3RDET.G3R_POSTOR AND " + IIF(TCSrvType() == "AS/400", "G3M_DR.@DELETED@ = ' ' ", "G3M_DR.D_E_L_E_T_ = ' ' ") + cFilterG3M
cSts +=		"   INNER JOIN " + RetSqlName("SA1") + " SA1 ON SA1.A1_COD = G3Q.G3Q_CLIENT AND SA1.A1_LOJA = G3Q.G3Q_LOJA AND " + IIF(TCSrvType() == "AS/400", "SA1.@DELETED@ = ' ' ", "SA1.D_E_L_E_T_ = ' ' ") + cFilterSA1
cSts +=		"   INNER JOIN " + RetSqlName("SA2") + " SA2_REP ON SA2_REP.A2_COD = G3RDET.G3R_FORREP AND SA2_REP.A2_LOJA = G3RDET.G3R_LOJREP AND " + IIF(TCSrvType() == "AS/400", "SA2_REP.@DELETED@ = ' ' ", "SA2_REP.D_E_L_E_T_ = ' ' ") + cFilterSA2

lRet := TCSQLExec(cSts) >= 0
	
oModel:DeActivate()	
oModel:Activate()
oView:Refresh()

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038ACrTb

Cria os campos fakes.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038ACrTb()

Local aStruHeader := {}
Local aStruDetail := {}
Local aDRFields	  := {"G3R_MSFIL", "G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC", "G3R_CONJUG", "G3R_EMISS", "G3R_TARIFA", "G3R_TAXA", "G3R_EXTRAS", "G3R_VLCOMI", "G3R_TAXADU", "G3R_VLINCE", "G3R_TOTREC", "G3R_TPSEG", "G3R_FORNEC", "G3R_LOJA", "G3R_MOEDA", "G3T_ABCIA", "A2_NOME", "G3Q_POSTO", "G3M_DESCR", "G3Q_CLIENT", "G3Q_LOJA", "A1_NOME", "G3R_CONCIL", "G3R_OPERAC", "G3R_STATUS", "G3R_VLFIM", "G3R_TPDOC", "G3R_FILIAL", "G3R_POSTOR", "G3M_NIATA", "G3R_TXREE", "G3R_TXFORN", "G3R_FORREP", "G3R_LOJREP", "G3R_NOMREP"}
Local nX	      := 0

aAdd(aStruHeader, {"TMP_XGRPPR", "C", 10					 , 0})
aAdd(aStruHeader, {"TMP_XEMIDE", "D", 08					 , 0})
aAdd(aStruHeader, {"TMP_XEMIAT", "D", 08				 	 , 0})
aAdd(aStruHeader, {"TMP_XFORND", "C", TamSX3("A2_COD")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XLOJAD", "C", TamSX3("A2_LOJA")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XNOMFO", "C", TamSX3("A2_NOME")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XFORNA", "C", TamSX3("A2_COD")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XLOJAA", "C", TamSX3("A2_LOJA")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XNOMFA", "C", TamSX3("A2_NOME")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XPOSTD", "C", TamSX3("G3P_POSTO")[1] , 0})
aAdd(aStruHeader, {"TMP_XNOMPD", "C", TamSX3("G3P_DESPST")[1], 0})
aAdd(aStruHeader, {"TMP_XPOSTA", "C", TamSX3("G3P_POSTO")[1] , 0})
aAdd(aStruHeader, {"TMP_XNOMPA", "C", TamSX3("G3P_DESPST")[1], 0})
aAdd(aStruHeader, {"TMP_XCLIDE", "C", TamSX3("A1_COD")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCLLJD", "C", TamSX3("A1_LOJA")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCLNDE", "C", TamSX3("A1_NOME")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCLIAT", "C", TamSX3("A1_COD")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCLLJA", "C", TamSX3("A1_LOJA")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCLINA", "C", TamSX3("A1_NOME")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XCANC" , "L", 1						 , 0})
aAdd(aStruHeader, {"TMP_XDOC"  , "C", TamSX3("G3Q_DOC")[1]	 , 0})
aAdd(aStruHeader, {"TMP_XREEMB", "C", 1	                     , 0})
aAdd(aStruHeader, {"TMP_XIATAD", "C", TamSX3("G3M_NIATA")[1] , 0})
aAdd(aStruHeader, {"TMP_XIATAA", "C", TamSX3("G3M_NIATA")[1] , 0})

oTblHeader := FWTemporaryTable():New()
oTblHeader:SetFields(aStruHeader)
oTblHeader:AddIndex("index1", {"TMP_XGRPPR"})
oTblHeader:Create()

aAdd(aStruDetail, {"TMP_OK"	 , "L", 1 , 0})
aAdd(aStruDetail, {"THREADID", "C", 10, 0})

For nX := 1 To Len(aDRFields)
	aAdd(aStruDetail, {AllTrim(aDRFields[nX]), TamSX3(aDRFields[nX])[3], TamSX3(aDRFields[nX])[1], TamSX3(aDRFields[nX])[2]})
Next

oTblDetail := FWTemporaryTable():New()
oTblDetail:SetFields(aStruDetail)
oTblDetail:AddIndex("index1", {"G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC"})
oTblDetail:Create()

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldCab

Adiciona os campos do cabe�alho.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldCab(oStruct, nOpc)

Local oTur038Mdl := Nil

If nOpc == 1
	oStruct:AddField(STR0025, ; 																			// [01] C Titulo do campo
					 STR0025, ; 																			// [02] C ToolTip do campo
					 "TMP_XGRPPR", ; 																		// [03] C identificador (ID) do Field
					 "C", ; 																				// [04] C Tipo do campo
					 1, ; 																					// [05] N Tamanho do campo
					 0, ; 																					// [06] N Decimal do campo
					 {|| .T.}, ; 																			// [07] B Code-block de valida��o do campo
					 Nil, ; 																				// [08] B Code-block de valida��o When do campo
					 {STR0026, STR0027}, ; 																	// [09] A Lista de valores permitido do campo
					 .T., ; 																				// [10] L Indica se o campo tem preenchimento obrigat�rio
					 FwBuildFeature(STRUCT_FEATURE_INIPAD, IIF(FwIsInCallStack("TURA042"), "1", "2")), ; 	// [11] B Code-block de inicializacao do campo
					 Nil, ; 																				// [12] L Indica se trata de um campo chave
					 Nil, ; 																				// [13] L Indica se o campo pode receber valor em uma opera��o de update.
					 .T.) 				   																	// [14] L Indica se o campo � virtual

	oStruct:AddField(STR0028, ""     , "TMP_XEMIDE", "D", 08                     , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Emiss�o De"
	oStruct:AddField(STR0029, ""     , "TMP_XEMIAT", "D", 08                     , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Emiss�o At�"
	oStruct:AddField(STR0030, ""     , "TMP_XFORND", "C", TamSX3("A2_COD")[1]    , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Fornecedor De"
	oStruct:AddField(STR0031, ""     , "TMP_XLOJAD", "C", TamSX3("A2_LOJA")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Loja De"
	oStruct:AddField(STR0032, ""     , "TMP_XNOMFO", "C", TamSX3("A2_NOME")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Fornecedor De"
	oStruct:AddField(STR0033, ""     , "TMP_XFORNA", "C", TamSX3("A2_COD")[1]    , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Fornecedor At�"
	oStruct:AddField(STR0034, ""     , "TMP_XLOJAA", "C", TamSX3("A2_LOJA")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Loja At�"
	oStruct:AddField(STR0035, ""     , "TMP_XNOMFA", "C", TamSX3("A2_NOME")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Fornecedor At�" 
	oStruct:AddField(STR0036, ""     , "TMP_XPOSTD", "C", TamSX3("G3P_POSTO")[1] , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Posto De"
	oStruct:AddField(STR0037, ""     , "TMP_XNOMPD", "C", TamSX3("G3P_DESPST")[1], 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Posto De"
	oStruct:AddField(STR0038, ""     , "TMP_XPOSTA", "C", TamSX3("G3P_POSTO")[1] , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Posto At�"
	oStruct:AddField(STR0039, ""     , "TMP_XNOMPA", "C", TamSX3("G3P_DESPST")[1], 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Posto At�" 
	oStruct:AddField(STR0040, ""     , "TMP_XCLIDE", "C", TamSX3("A1_COD")[1]    , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Cliente De"
	oStruct:AddField(STR0031, ""     , "TMP_XCLLJD", "C", TamSX3("A1_LOJA")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Loja De"
	oStruct:AddField(STR0042, ""     , "TMP_XCLNDE", "C", TamSX3("A1_NOME")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Cliente De"
	oStruct:AddField(STR0043, ""     , "TMP_XCLIAT", "C", TamSX3("A1_COD")[1]    , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Cliente At�"
	oStruct:AddField(STR0034, ""     , "TMP_XCLLJA", "C", TamSX3("A1_LOJA")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Loja At�"
	oStruct:AddField(STR0045, ""     , "TMP_XCLINA", "C", TamSX3("A1_NOME")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Nome Cliente At�"
	oStruct:AddField(STR0008, ""     , "TMP_XCANC" , "L", 1                      , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Listar Cancelados"
	oStruct:AddField(STR0046, ""     , "TMP_XDOC"  , "C", TamSX3("G3Q_DOC")[1]   , 0, {|| .T.}, {|| .T.}, Nil, .F., Nil, .F., .F., .T.)		// "Localizador/Bilhete"
	oStruct:AddField(STR0060, STR0060, "TMP_XIATAD", "C", TamSX3("G3M_NIATA")[1] , 0, {|| .T.}, Nil     , Nil, .F., Nil, Nil, Nil, .T.)		// "De IATA"
	oStruct:AddField(STR0061, STR0061, "TMP_XIATAA", "C", TamSX3("G3M_NIATA")[1] , 0, {|| .T.}, Nil     , Nil, .F., Nil, Nil, Nil, .T.)		// "At� IATA"
	oStruct:AddField(STR0056, STR0056, "TMP_XREEMB", "C", 1, 0, {|| .T.}, Nil, {STR0057, STR0058, STR0059}, .T., FwBuildFeature(STRUCT_FEATURE_INIPAD, "3"), Nil, Nil, .T.) 		// "Venda/Reemb?"  ### "1=Venda";"2=Reembolso";"3=Ambos"

	If FwIsInCallStack("Ta38GIt")
		oTur038Mdl := Tur038Modelo()[1]:GetModel("G6HMASTER")

		oStruct:SetProperty("TMP_XFORND", MODEL_FIELD_INIT, {|| oTur038Mdl:GetValue("G6H_FORNEC")})
		oStruct:SetProperty("TMP_XLOJAD", MODEL_FIELD_INIT, {|| oTur038Mdl:GetValue("G6H_LOJA")})
		oStruct:SetProperty("TMP_XNOMFO", MODEL_FIELD_INIT, {|| Posicione("SA2", 1, xFilial("SA2") + oTur038Mdl:GetValue("G6H_FORNEC") + oTur038Mdl:GetValue("G6H_LOJA"), "A2_NOME")})
		oStruct:SetProperty("TMP_XFORNA", MODEL_FIELD_INIT, {|| oTur038Mdl:GetValue("G6H_FORNEC")})
		oStruct:SetProperty("TMP_XLOJAA", MODEL_FIELD_INIT, {|| oTur038Mdl:GetValue("G6H_LOJA")})
		oStruct:SetProperty("TMP_XNOMFA", MODEL_FIELD_INIT, {|| Posicione("SA2",1,xFilial("SA2") + oTur038Mdl:GetValue("G6H_FORNEC") + oTur038Mdl:GetValue("G6H_LOJA"), "A2_NOME")})
	EndIf
Else
	oStruct:AddField("TMP_XGRPPR" 	   , ; 			// [01] C Nome do Campo
					 "01"			   , ; 			// [02] C Ordem
					 STR0025 	       , ; 			// [03] C Titulo do campo
					 STR0025 	       , ; 			// [04] C Descri��o do campo
					 {} 			   , ; 			// [05] A Array com Help
					 "COMBO"		   , ; 			// [06] C Tipo do campo
					 "@!" 			   , ; 			// [07] C Picture
					 Nil 			   , ; 			// [08] B Bloco de Picture Var
					 "" 			   , ; 			// [09] C Consulta F3
					 .F. 			   , ; 			// [10] L Indica se o campo � edit�vel
					 Nil 			   , ; 			// [11] C Pasta do campo
					 Nil 			   , ; 			// [12] C Agrupamento do campo
					 {STR0026, STR0027}, ; 			// [13] A Lista de valores permitido do campo (Combo)
					 Nil 			   , ; 			// [14] N Tamanho M�ximo da maior op��o do combo
					 Nil 			   , ; 			// [15] C Inicializador de Browse
					 .T. 			   , ; 			// [16] L Indica se o campo � virtual
					 Nil) 				  			// [17] C Picture Vari�vel

	oStruct:AddField("TMP_XEMIDE", "02", STR0028, STR0028, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Emiss�o De"
	oStruct:AddField("TMP_XEMIAT", "03", STR0029, STR0029, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Emiss�o At�"
	oStruct:AddField("TMP_XFORND", "04", STR0030, STR0030, {}, "GET"  , ""  , Nil, "SA2", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Fornecedor De"
	oStruct:AddField("TMP_XLOJAD", "05", STR0031, STR0031, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Loja De"
	oStruct:AddField("TMP_XNOMFO", "06", STR0032, STR0032, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Fornecedor De"
	oStruct:AddField("TMP_XFORNA", "07", STR0033, STR0033, {}, "GET"  , ""  , Nil, "SA2", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Fornecedor At�"
	oStruct:AddField("TMP_XLOJAA", "08", STR0034, STR0034, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Loja At�"
	oStruct:AddField("TMP_XNOMFA", "09", STR0035, STR0035, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Fornecedor At�"
	oStruct:AddField("TMP_XPOSTD", "10", STR0036, STR0036, {}, "GET"  , ""  , Nil, "G3M", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Posto De"
	oStruct:AddField("TMP_XNOMPD", "11", STR0037, STR0037, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Posto De"
	oStruct:AddField("TMP_XPOSTA", "12", STR0038, STR0038, {}, "GET"  , ""  , Nil, "G3M", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Posto At�"
	oStruct:AddField("TMP_XNOMPA", "13", STR0039, STR0039, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Posto At�"
	oStruct:AddField("TMP_XCLIDE", "14", STR0040, STR0040, {}, "GET"  , ""  , Nil, "SA1", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Cliente De"
	oStruct:AddField("TMP_XCLLJD", "15", STR0031, STR0031, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Loja At�"
	oStruct:AddField("TMP_XCLNDE", "16", STR0042, STR0042, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Cliente De"
	oStruct:AddField("TMP_XCLIAT", "17", STR0043, STR0043, {}, "GET"  , ""  , Nil, "SA1", .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Cliente At�"
	oStruct:AddField("TMP_XCLLJA", "18", STR0034, STR0034, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Loja At�"
	oStruct:AddField("TMP_XCLINA", "19", STR0045, STR0045, {}, "GET"  , ""  , Nil, ""   , .F., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Nome Cliente At�"
	oStruct:AddField("TMP_XDOC"  , "20", STR0046, STR0046, {}, "GET"  , ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Localizador/Bilhete"
	oStruct:AddField("TMP_XCANC" , "21", STR0008, STR0008, {}, "CHECK", ""  , Nil, ""   , .T., Nil, Nil, {} , Nil, Nil, .T., Nil)		// "Listar Cancelados"
	oStruct:AddField("TMP_XIATAD", "22", STR0060, STR0060, {}, "GET"  , "@!", Nil, "" 	, .T., Nil, Nil, Nil, Nil, Nil, .T., Nil)		// "De IATA"
	oStruct:AddField("TMP_XIATAA", "23", STR0061, STR0061, {}, "GET"  , "@!", Nil, ""   , .T., Nil, Nil, Nil, Nil, Nil, .T., Nil)		// "At� IATA"
	oStruct:AddField("TMP_XREEMB", "24", STR0056, STR0056, {}, "COMBO", "@!", Nil, ""   , .T., Nil, Nil, {STR0057, STR0058, STR0059}, Nil, Nil, .T., Nil)		// "Venda/Reemb?"  ### "1=Venda";"2=Reembolso";"3=Ambos" 				
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldDet

Adiciona as colunas da grid.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldDet(oStructDet, nOpc)

Local aArea    := GetArea() 
Local aStruHis := (oTblDetail:GetAlias())->(DbStruct())
Local bVldLine := Nil
Local nX	   := 0
Local lRet     := .F.

If FwIsInCallStack("Ta38GIt")
	bVldLine := Nil
Else
	bVldLine := {|oMdl, cCampo, xValueNew, nLine, xValueOld| vldMark(oMdl, cCampo, xValueNew, nLine, xValueOld)}
EndIf 

//Adiciona o campo OK
If nOpc == 1
	oStructDet:AddField(""		, ; 	// [01] C Titulo do campo
						"" 		, ; 	// [02] C ToolTip do campo
						"TMP_OK", ; 	// [03] C identificador (ID) do Field
						"L"		, ; 	// [04] C Tipo do campo
						1		, ; 	// [05] N Tamanho do campo
						0		, ; 	// [06] N Decimal do campo
						bVldLine, ; 	// [07] B Code-block de valida��o do campo
						Nil 	, ; 	// [08] B Code-block de valida��o When do campos
						Nil 	, ; 	// [09] A Lista de valores permitido do campo
						Nil 	, ; 	// [10] L Indica se o campo tem preenchimento obrigat�rio
						Nil		, ; 	// [11] B Code-block de inicializacao do campo
						Nil 	, ; 	// [12] L Indica se trata de um campo chave
						.F. 	, ; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						.F.) 			// [14] L Indica se o campo � virtual
Else
	oStructDet:AddField("TMP_OK", ; 	// [01] C Nome do Campo
						"00"	, ; 	// [02] C Ordem
						""		, ; 	// [03] C Titulo do campo
						""	    , ; 	// [04] C Descri��o do campo
						Nil		, ; 	// [05] A Array com Help
						"CHECK"	, ; 	// [06] C Tipo do campo
						"@!"	, ; 	// [07] C Picture
						Nil 	, ; 	// [08] B Bloco de Picture Var
						"" 		, ; 	// [09] C Consulta F3
						.T. 	, ; 	// [10] L Indica se o campo � edit�vel
						Nil 	, ; 	// [11] C Pasta do campo
						Nil 	, ; 	// [12] C Agrupamento do campo
						Nil 	, ; 	// [13] A Lista de valores permitido do campo (Combo)
						Nil 	, ; 	// [14] N Tamanho M�ximo da maior op��o do combo
						Nil 	, ; 	// [15] C Inicializador de Browse
						.F. 	, ; 	// [16] L Indica se o campo � virtual
						Nil) 			// [17] C Picture Vari�vel
EndIf

SX3->(DbSetOrder(2))	// X3_CAMPO
For nX := 1 To Len(aStruHis)
	If SX3->(DbSeek(aStruHis[nX][1]))
		If nOpc == 1
			oStructDet:AddField(GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), "", AllTrim(aStruHis[nX][1]), TamSX3(aStruHis[nX][1])[3], TamSX3(aStruHis[nX][1])[1], TamSX3(aStruHis[nX][1])[2], Nil, Nil, Nil, Nil, Nil, Nil, .T., .F.)
		ElseIf nOpc == 2
			If !Empty(X3CBOX())
				aCBox := Separa(X3CBox(), ";")
			Else
				aCBox := Nil
			EndIf
			
			lRet := oStructDet:AddField(AllTrim(aStruHis[nX][1]), StrZero(nX, 2), GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), GetSX3Cache(aStruHis[nX][1], "X3_DESCRIC"), Nil, TamSX3(aStruHis[nX][1])[3], X3Picture(aStruHis[nX][1]), Nil, "", .T., Nil, Nil, aCBox, Nil, Nil, .F., Nil)
			SX3->(DbSetOrder(2))	// X3_CAMPO
		EndIf
	EndIf
Next nX

RestArea(aArea)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} VldMark
Permite a marca��o de apenas um item no mark.
@type Static Function
@author Enaldo Cardoso Junior
@since 14/03/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function VldMark(oMdlBase, cCampo, xValueNew, nLine, xValueOld)

Local nLinAtual := 0
Local lRet      := !Empty(oMdlBase:GetValue("G3R_NUMID"))

If lRet .And. xValueNew
	nLinAtual := oMdlBase:GetLine()
	
	If nLinAtual # nLinMark
		If nLinMark # 0
			oMdlBase:GoLine(nLinMark)
			oMdlBase:SetValue("TMP_OK", .F.) // desmarca o item anterior
		EndIf
		oMdlBase:GoLine(nLinAtual)  // retorna ao item posicionado antes
	EndIf
	nLinMark := nLinAtual
Else
	nLinMark := 0
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddGatilhos

Faz os gatilhos.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddGatilhos(oStruct)

Local aAux := {}

aAux := FwStruTrigger("TMP_XFORND", "TMP_XLOJAD", "'  '", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XFORND", "TMP_XLOJAD", "A2_LOJA", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XLOJAD", "TMP_XNOMFO", "Posicione('SA2', 1, xFilial('SA2') + FwFldGet('TMP_XFORND') + FwFldGet('TMP_XLOJAD'), 'A2_NOME')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XFORNA", "TMP_XLOJAA", "'  '", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XFORNA", "TMP_XLOJAA", "A2_LOJA", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XLOJAA", "TMP_XNOMFA", "Posicione('SA2', 1, xFilial('SA2') + FwFldGet('TMP_XFORNA') + FwFldGet('TMP_XLOJAA'), 'A2_NOME')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XPOSTD", "TMP_XNOMPD", "Posicione('G3M', 1, xFilial('G3M') + FwFldGet('TMP_XPOSTD'), 'G3M_DESCR')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XPOSTA", "TMP_XNOMPA", "Posicione('G3M', 1, xFilial('G3M') + FwFldGet('TMP_XPOSTA'), 'G3M_DESCR')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLIDE", "TMP_XCLLJD", "'  '", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLIDE", "TMP_XCLLJD", "A1_LOJA", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLLJD", "TMP_XCLNDE", "Posicione('SA1', 1, xFilial('SA1') + FwFldGet('TMP_XCLIDE') + FwFldGet('TMP_XCLLJD'), 'A1_NOME')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLIAT", "TMP_XCLLJA", "'  '", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLIAT", "TMP_XCLLJA", "A1_LOJA", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("TMP_XCLLJA", "TMP_XCLINA", "Posicione('SA1', 1, xFilial('SA1') + FwFldGet('TMP_XCLIAT') + FwFldGet('TMP_XCLLJA'), 'A1_NOME')", .F.)
oStruct:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddGroups
Cria os grupos dos campos.
@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddGroups(oStruct)

oStruct:AddGroup("GRUPO01", STR0063, "", 1)		// "Grupo"
oStruct:AddGroup("GRUPO02", STR0064, "", 1)		// "Emiss�o"
oStruct:AddGroup("GRUPO03", STR0065, "", 1)		// "Fornecedor"	
oStruct:AddGroup("GRUPO04", STR0066, "", 1)		// "Posto"		
oStruct:AddGroup("GRUPO05", STR0067, "", 1)		// "Cliente"		
oStruct:AddGroup("GRUPO06", STR0011, "", 1)		// "Bilhete/Cancelados"
oStruct:AddGroup("GRUPO07", STR0062, "", 1)		// "Opera��o"

oStruct:SetProperty("TMP_XGRPPR", MVC_VIEW_GROUP_NUMBER, "GRUPO01")
oStruct:SetProperty("TMP_XEMIDE", MVC_VIEW_GROUP_NUMBER, "GRUPO02")
oStruct:SetProperty("TMP_XEMIAT", MVC_VIEW_GROUP_NUMBER, "GRUPO02")
oStruct:SetProperty("TMP_XFORND", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XLOJAD", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XNOMFO", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XFORNA", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XLOJAA", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XNOMFA", MVC_VIEW_GROUP_NUMBER, "GRUPO03")
oStruct:SetProperty("TMP_XPOSTD", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XNOMPD", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XPOSTA", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XNOMPA", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XIATAD", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XIATAA", MVC_VIEW_GROUP_NUMBER, "GRUPO04")
oStruct:SetProperty("TMP_XCLIDE", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCLLJD", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCLNDE", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCLIAT", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCLLJA", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCLINA", MVC_VIEW_GROUP_NUMBER, "GRUPO05")
oStruct:SetProperty("TMP_XCANC" , MVC_VIEW_GROUP_NUMBER, "GRUPO06")
oStruct:SetProperty("TMP_XDOC"  , MVC_VIEW_GROUP_NUMBER, "GRUPO06")
oStruct:SetProperty("TMP_XREEMB", MVC_VIEW_GROUP_NUMBER, "GRUPO07")

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA38AFat
Gera��o dos itens da fatura.
@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA38AFat(oModelPesq, oMdlPesqResult)

Local aArea      := GetArea()
Local oMdlTUR038 := Tur038Modelo()[1]
Local oMdlG6H    := oMdlTUR038:GetModel("G6HMASTER")
Local oMdlG6I	 := oMdlTUR038:GetModel("G6IDETAIL")
Local oMdlDetail := oModelPesq:GetModel("TMP_DETAIL")
Local nX		 := 0
Local nY		 := 0
Local nValLiq	 := 0
Local cCodDoc	 := ""
Local cItem		 := ""
Local cTipoIt	 := ""
Local cTpFop	 := ""
Local lSeek		 := .F.
Local lMarcou	 := .T.
Local lGerItem	 := FwIsInCallStack("Ta38GIt")

G9K->(DbSetOrder(1))	//G9K_FILIAL+G9K_NUMID+G9K_IDITEM+G9K_NUMSEQ+G9K_CLIFOR+G9K_SEQ
G6H->(DbSetOrder(1))	//G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA

//Efetua gera��o do item da fatura dentro do modelo.
If oMdlTUR038:Activate()
	For nX := 1 To oMdlDetail:Length()
		If lGerItem
			lSeek := oMdlDetail:aDataModel[nX][1][1][1] == .T.
			oMdlDetail:GoLine(nX)
		Else
			lSeek := lMarcou
			If !lMarcou
				Exit
			EndIf					
		EndIf
		
		//Se estiver marcado adiciona.
		If lSeek
			IF lGerItem .And. Empty(oMdlG6H:GetValue("G6H_FORNEC") + oMdlG6H:GetValue("G6H_LOJA"))
				oMdlG6H:SetValue("G6H_FORNEC", oMdlDetail:GetValue("G3R_FORREP"))
				oMdlG6H:SetValue("G6H_LOJA"  , oMdlDetail:GetValue("G3R_LOJREP"))
			EndIf

			cItem := Tur038SeqIf()
			If oMdlG6I:AddLine()
				oMdlG6I:LoadValue("G6I_LEGCON", Tur038Leg(oMdlG6I))
				oMdlG6I:SetValue("G6I_FATURA" , G6H->G6H_FATURA)
				oMdlG6I:SetValue("G6I_ITEM"   , cItem)
				
				If oMdlPesqResult:GetValue("G3R_STATUS") == "4"
					cTipoIt := "0"				
					oMdlG6I:SetValue("G6I_TARIFA", 0)
					oMdlG6I:SetValue("G6I_TAXAS" , 0)
					oMdlG6I:SetValue("G6I_TAXADU", 0)
					oMdlG6I:SetValue("G6I_VLRCOM", 0)
					oMdlG6I:SetValue("G6I_VLRINC", 0)
					oMdlG6I:SetValue("G6I_TXREE" , 0)
				Else
					cTipoIt := "1"					
					Do Case
   						Case oMdlPesqResult:GetValue("G3R_OPERAC") == "1" //Emiss�o
   							If oMdlPesqResult:GetValue("G3R_TPDOC") == "5" // ADM
   							 	cTipoIt := "4" 
   							ElseIf oMdlPesqResult:GetValue("G3R_TPDOC") == "7" // VMPD
   							 	cTipoIt := "7" 	
   							EndIf	       	
   							 					
   						Case oMdlPesqResult:GetValue("G3R_OPERAC") == "2" //Reembolso
			    			If oMdlPesqResult:GetValue("G3R_TPDOC") == "6" // ACM
   							 	cTipoIt := "5"
   							Else
			    				cTipoIt := "3"
			    			EndIf
			    				
			    		Case oMdlPesqResult:GetValue("G3R_OPERAC") == "3" //Reemiss�o
   							If oMdlPesqResult:GetValue("G3R_TPDOC") == "7" // VMPD
   							 	cTipoIt := "7" 	
   							EndIf	       	
			    	EndCase						
					
					oMdlG6I:SetValue("G6I_TIPOIT", cTipoIt)
					oMdlG6I:SetValue("G6I_TAXAS" , oMdlPesqResult:GetValue("G3R_TAXA") + oMdlPesqResult:GetValue("G3R_EXTRAS"))
				
					If oMdlPesqResult:GetValue("G3R_OPERAC") == "2" 
						oMdlG6I:SetValue("G6I_TAXADU", oMdlPesqResult:GetValue("G3R_TAXADU") * -1)
						oMdlG6I:SetValue("G6I_VLRCOM", oMdlPesqResult:GetValue("G3R_VLCOMI") * -1)
						oMdlG6I:SetValue("G6I_VLRINC", oMdlPesqResult:GetValue("G3R_VLINCE") * -1)
						oMdlG6I:SetValue("G6I_TXCCDU", oMdlPesqResult:GetValue("G3R_TXFORN") * -1)						
					Else
						oMdlG6I:SetValue("G6I_TAXADU", oMdlPesqResult:GetValue("G3R_TAXADU"))
						oMdlG6I:SetValue("G6I_VLRCOM", oMdlPesqResult:GetValue("G3R_VLCOMI"))
						oMdlG6I:SetValue("G6I_VLRINC", oMdlPesqResult:GetValue("G3R_VLINCE"))
						oMdlG6I:SetValue("G6I_TXCCDU", oMdlPesqResult:GetValue("G3R_TXFORN"))
					EndIf
					
					If cTipoIt == "3"
						oMdlG6I:SetValue("G6I_TXREE", oMdlPesqResult:GetValue("G3R_TXREE"))
					EndIf
				EndIf
				
				oMdlG6I:SetValue("G6I_NUMCIA", Tur038Cia(oMdlPesqResult:GetValue("G3R_DOC"), cTipoIt, oMdlPesqResult:GetValue("G3R_FORNEC"), oMdlPesqResult:GetValue("G3R_LOJA"), oMdlPesqResult:GetValue("G3R_FILIAL")))
				oMdlG6I:SetValue(IIF(oMdlPesqResult:GetValue("G3R_TPDOC") == "3", "G6I_LOCALI", "G6I_BILHET"), AllTrim(oMdlPesqResult:GetValue("G3R_DOC")))
				oMdlG6I:SetValue("G6I_EMISSA", oMdlPesqResult:GetValue("G3R_EMISS"))
				oMdlG6I:SetValue("G6I_MOEDA" , oMdlPesqResult:GetValue("G3R_MOEDA"))
				oMdlG6I:SetValue("G6I_SITUAC", "1")	//N�o Associado
				oMdlG6I:SetValue("G6I_NUMID" , "")
				oMdlG6I:SetValue("G6I_IDITEM", "")
				oMdlG6I:SetValue("G6I_NUMSEQ", "")
			
				//Por ultimo para acertar FOP
				oMdlG6I:SetValue("G6I_TARIFA", oMdlPesqResult:GetValue("G3R_TARIFA"))
				cTpFop := T38ATpFop(oMdlPesqResult)

				If cTpFop == "1"
					nValLiq := + oMdlG6I:GetValue("G6I_TARIFA") + oMdlG6I:GetValue("G6I_TAXAS") - oMdlG6I:GetValue("G6I_TXREE") - oMdlG6I:GetValue("G6I_VLRCOM") - oMdlG6I:GetValue("G6I_VLRINC")				
				Else
					nValLiq := - oMdlG6I:GetValue("G6I_TAXADU") - oMdlG6I:GetValue("G6I_VLRCOM") - oMdlG6I:GetValue("G6I_VLRINC") + oMdlG6I:GetValue("G6I_TXCCDU")
				EndIf

				//Inverte o Liquido, pois sempre � positivo e a fun��o TA038TpOper inverte a opera��o Credito / Debito 
				oMdlG6I:SetValue("G6I_VLRLIQ", IIF(nValLiq < 0, nValLiq * -1, nValLiq))
				
				//Como FOP Cart�o pode ter valor zero o gatilho n�o � executado e a FOP fica como Desconhecida, portanto, chama a rotina diretamente
				If nValLiq == 0
					oMdlG6I:LoadValue("G6I_TPFOP", TA038TpFOP())
				EndIf
				
				oMdlG6I:SetValue('G6I_TPOPER', TA038TpOper())
			EndIf
			lMarcou := .F.
		EndIf
	Next nX
	oMdlG6I:GoLine(1)
EndIf

RestArea(aArea)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038ACommit
Commit, faz a grava��o das associa��es manuais.
@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038ACommit(oModelPesq)

Local oTur038Mdl 	 := Tur038Modelo()[1]
Local oMdlPesqResult := oModelPesq:GetModel("TMP_DETAIL")
Local oModelConc	 := Nil
Local oMdlG6I	 	 := Nil
Local oMdlG6J	 	 := Nil
Local aConcil		 := {}
Local aManAss		 := {}
Local aItemG6I		 := {}
Local lRet       	 := .T.
Local lGerItem		 := FwIsInCallStack("Ta38GIt")

If lGerItem
	TA38AFat(oModelPesq,oMdlPesqResult)
Else	//Associa��o manual.
	BEGIN TRANSACTION
		oModelConc := FwLoadModel("TURA038D")
		oModelConc:SetOperation(MODEL_OPERATION_UPDATE)
		oModelConc:Activate()
		
		oMdlG6I := oModelConc:GetModel("G6I_ITENS")
		oMdlG6J := oModelConc:GetModel("G6J_ITENS")
		If oMdlPesqResult:SeekLine({{'TMP_OK', .T.}})
			//Pesqisa se j� existe uma concilia��o aberta para este posto ou cria uma nova
			If T38AddConc(oModelConc, oMdlPesqResult:GetValue("G3R_OPERAC"), oMdlPesqResult:GetValue("G3R_POSTOR"), oMdlPesqResult:GetValue("G3M_NIATA"), "2")
				If oMdlG6I:SeekLine({{"G6I_ITEM", oTur038Mdl:GetValue("G6IDETAIL","G6I_ITEM")}})
					aAdd(aItemG6I, oMdlG6I:GetValue("G6I_ITEM"))
					
					//Armazena as informa��es necess�rias para a associa��o manual
					aManAss	:= {}
					aAdd(aManAss, {oMdlG6I:GetValue("G6I_ITEM"), oMdlPesqResult:GetValue("G3R_MSFIL"), oMdlPesqResult:GetValue("G3R_NUMID"), oMdlPesqResult:GetValue("G3R_IDITEM"), oMdlPesqResult:GetValue("G3R_NUMSEQ")}) 	
					lRet := T39IRunAsM(aManAss, oModelConc, oMdlG6I:GetLine())
				EndIf
					
				If aScan(aConcil, {|x| x == oMdlG6J:GetValue("G6J_CONCIL") } ) == 0
					aAdd(aConcil, oMdlG6J:GetValue("G6J_CONCIL"))
				EndIf
			Else
				lRet := .F.
			EndIf
		EndIf	
		
		If lRet .And. oModelConc:VldData()
			oMdlG6J:SetOnlyQuery(.T.)
			oModelConc:CommitData()	
	
			If (lRet := T38AutAcr(G6H->G6H_FATURA, aItemG6I))
				lRet := T38AtuNat(aConcil)
			EndIf	
			oMdlG6J:SetOnlyQuery(.F.)
		Else
			lRet := .F.
		EndIf
		
		If lRet	
			FwMsgRun(,{|| lRet := T38AtuSt(oModelConc:GetValue("G6H_MASTER", "G6H_FATURA"))}, , STR0068)	//"Atualizando status"
		EndIf
		
		oModelConc:DeActivate()
		oModelConc:Destroy()	

		If !lRet
			DisarmTransaction()
			Break		
		EndIf	
	END TRANSACTION
EndIf

FwModelActive(oModelPesq)

Return lRet

//------------------------------------------------------------------------------
/*/{Protheus.doc} Tur038SeqIf

Retorna a sequencia de item da fatura

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function Tur038SeqIf()

Local oMdlTUR038 := Tur038Modelo()[1]
Local oMdlG6I	 := oMdlTUR038:GetModel("G6IDETAIL")
Local cSequencia := ""
Local nX		 := 0
	
oMdlG6I:GoLine(oMdlG6I:Length())
cSequencia := oMdlG6I:GetValue("G6I_ITEM")	

If !Empty(oMdlG6I:GetValue("G6I_TIPOIT"))
	cSequencia := StrZero(Val(cSequencia) + 1, 6)
EndIf	

Return cSequencia

//------------------------------------------------------------------------------
/*/{Protheus.doc} Tur038Cia

Busca c�digo da cia a�rea

@since		10/06/2015
@version	P12
/*/
//------------------------------------------------------------------------------
Static Function Tur038Cia(cCodDoc, cTipoIt, cFornec, cFornLoja, cFilDR)

Local aArea   := GetArea()
Local cCodCia := ""
Local cQuery  := GetNextAlias()

If	cTipoIt == "3"	//Reembolso

	//Busca na G3Q - Item de venda, qual a cia a�rea do documento origem.
	BeginSql Alias cQuery
		SELECT G3Q_DOCORI 
		FROM %Table:G3Q% G3Q 
		WHERE G3Q_FILIAL = %Exp:cFilDR% AND G3Q_OPERAC = '2' AND G3Q_DOC = %Exp:cCodDoc% AND G3Q.%NotDel%
	EndSql

	If (cQuery)->(!Eof())
		cCodDoc := (cQuery)->G3Q_DOCORI
	EndIf
	(cQuery)->(DbCloseArea())
EndIf

//Busca a maior sequencia do item da fatura
cQuery := GetNextAlias()
BeginSql Alias cQuery
	SELECT G3T_ABCIA FROM %Table:G3R% G3R 
	INNER JOIN %Table:G3T% G3T ON G3T_FILIAL = G3R_FILIAL AND G3T_NUMID = G3R_NUMID AND G3T_IDITEM = G3R_IDITEM AND G3T_NUMSEQ = G3R_NUMSEQ AND G3T.%NotDel%
	WHERE G3R_DOC = %Exp:cCodDoc% AND G3R_FORNEC = %Exp:cFornec% AND G3R_LOJA = %Exp:cFornLoja% AND G3R.%NotDel%
EndSql

If (cQuery)->(!Eof())
	cCodCia := (cQuery)->G3T_ABCIA
EndIf
(cQuery)->(DbCloseArea())

RestArea(aArea)

Return cCodCia

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MaxSq
Fun��o para retornar a pr�xima sequ�ncia de um documento de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto do ModelDef instanciado
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038MaxSq(oModel)

Local aArea     := GetArea()
Local aLines	:= FwSaveRows()
Local oModelG3Q	:= oModel:GetModel("G3Q_ITENS")
Local cIdItem	:= ""
Local cTmpAlias	:= GetNextAlias()
Local nX		:= 0

//a consulta ao banco de dados serve para garantir que nenhum outro usu�rio gerou uma nova sequ�ncia em outra concilia��o para o mesmo item de venda
BeginSql Alias cTmpAlias
	SELECT MAX(G3Q_NUMSEQ) G3Q_NUMSEQ, MAX(G3Q_IDITEM) G3Q_IDITEM 
	FROM %Table:G3Q% 
	WHERE G3Q_FILIAL = %Exp:oModelG3Q:GetValue("G3Q_FILIAL")% AND G3Q_NUMID = %Exp:oModelG3Q:GetValue("G3Q_NUMID")% AND G3Q_IDITEM = %Exp:oModelG3Q:GetValue("G3Q_IDITEM")% AND %NotDel%
EndSql

If (cTmpAlias)->(!Eof())
	cSeqG3Q := (cTmpAlias)->G3Q_NUMSEQ
	cIdItem	:= (cTmpAlias)->G3Q_IDITEM

	//Verifica na concilia��o atual se n�o houve novas sequ�ncias ainda n�o comitadas
	For nX := 1 to oModelG3q:Length()
		If oModelG3Q:GetValue("G3Q_IDITEM", nX) == cIdItem .And. oModelG3q:GetValue("G3Q_NUMSEQ", nX) > cSeqG3Q
			cSeqG3Q := oModelG3q:GetValue("G3Q_NUMSEQ", nX)
		EndIf
	Next nX
EndIf 

FwRestRows(aLines)
RestArea(aArea)
	
Return Soma1(cSeqG3Q)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038ADestroy

Destroi os objetos.

@author Enaldo Cardoso Junior
@since 12/02/2015
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038ADestroy()

//Verifica se o objeto FwTemporaryTable est� instanciado
If ValType(oTblHeader) == "O"
	oTblHeader:Delete()
	FreeObj(oTblHeader)
EndIf

//Verifica se o objeto FwTemporaryTable est� instanciado
If ValType(oTblDetail) == "O"
	oTblDetail:Delete()
	FreeObj(oTblDetail)
EndIf
	
Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38Arr2Str

Serializa um vetor para utilizar como busca no load filter

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
@param aValues, array, vetor com os tipos que ser�o filtrados
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38Arr2Str(aValues)

Local cRet := ""
Local nX   := 0

For nX := 1 To Len(aValues)
	If nX > 1
		cRet += ","	
	EndIf
	cRet += cValToChar(aValues[nX])
Next nX
	
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AtuNat

Fun��o para atualiza��o das naturezas da concilia��o

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38AtuNat(aConcil)

Local aArea      := GetArea()
Local oModel 	 := FwLoadModel("TURA039MDL")
Local oModelG6I  := Nil
Local lExistFunc := FindFunction("U_TURNAT")
Local lRet		 := .T.
Local nX		 := 0
Local nY		 := 0

oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_SITUAC", "'5'", MVC_LOADFILTER_EQUAL}})

G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA
For nX := 1 To Len(aConcil)
	G6J->(DbSeek(xFilial("G6J") + aConcil[nX]))

	If oModel:Activate()
		oModelG6I := oModel:GetModel("G6I_ITENS")
	
		For nY := 1 to oModelG6I:Length()
			oModelG6I:GoLine(nY)
	
			If oModelG6I:GetValue("G6I_SITUAC") == "5"
				//Realiza o ajuste dos acordos de fornecedor
				T38CAtuAco(oModel)
	
				If lExistFunc
					TA038AtNat(oModel)
				EndIf
	
				oModelG6I:LoadValue("G6I_SITUAC", "2") //Documento Associado
			EndIf
		Next nY
	
		If oModel:VldData()
			oModel:CommitData()
		Else
			JurShowErro(oModel:GetErrorMessage())
			lRet := .F.
		EndIf
	
		oModel:DeActivate()
	Else
		JurShowErro(oModel:GetErrorMessage())
		lRet := .F.
	EndIf
Next nX

oModel:Destroy()

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38ATpFop

Fun��o para selecionar a forma de pagamento 

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38ATpFop(oModel)

Local aArea     := GetArea() 
Local cAliasG9K	:= GetNextAlias()
Local cRet		:= ''

BeginSql Alias cAliasG9K
	SELECT G9K_TPFOP, G9K_MODO 
	FROM %Table:G9K% G9K 
	WHERE G9K_FILIAL = %Exp:oModel:GetValue("G3R_FILIAL")% AND
	      G9K_NUMID  = %Exp:oModel:GetValue("G3R_NUMID")%  AND 
	      G9K_IDITEM = %Exp:oModel:GetValue("G3R_IDITEM")% AND 
	      G9K_NUMSEQ = %Exp:oModel:GetValue("G3R_NUMSEQ")% AND 
	      G9K_CLIFOR = '2' AND 
	      G9K_TPFOP IN ('1', '3') AND 
	      G9K_VLPERC <> 0 AND 
	      G9K.%NotDel%
	ORDER BY G9K_TPFOP
EndSql

If (cAliasG9K)->(!EOF())
	If (cAliasG9K)->G9K_TPFOP == "1" .And. (cAliasG9K)->G9K_MODO == "4" //Cart�o Ag�ncia
		cRet := "3"
	Else
		cRet := (cAliasG9K)->G9K_TPFOP
	EndIf
EndIf
(cAliasG9K)->(DbCloseArea())

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038AMark

Marcar/Desmarcar 

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038AMark(oView)

Local oMdlDetail := oView:GetModel("TMP_DETAIL")
Local nX		 := 0
Local lOk		 := .T.

If FwAlertYesNo(STR0054, STR0055)	//"Deseja marcar/desmarcar todos?"##"Marcar/Desmarcar"
	For nX := 1 To oMdlDetail:Length()
		oMdlDetail:GoLine(nX)
		If nX == 1
			lOk := IIF(oMdlDetail:GetValue("TMP_OK"), .F., .T.)
		EndIf
				
		oMdlDetail:SetValue("TMP_OK", lOk)
	Next nX
EndIf
oMdlDetail:GoLine(1)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38DscCpo

Descri��o do campo 

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38DscCpo(xValue, cField)

Local aValues 	:= IIF(ValType(xValue) == "A", xValue, {xValue})
Local aVlrCBox	:= {}
Local aTmpVlr	:= {}
Local cRet		:= ""
Local lRet		:= .T.	
Local nX		:= 0

SX3->(DbSetOrder(2))	// X3_CAMPO
If SX3->(DbSeek(cField))
	If !Empty(X3CBox())
		aTmpVlr := StrTokArr(X3CBox(), ";")
		
		For nX := 1 To Len(aTmpVlr)
			aAdd(aVlrCBox, StrTokArr(aTmpVlr[nX], "="))
		Next nX
		
		For nX := 1 To Len(aValues)
			If nX > 1
				cRet += ","
			EndIf
			
			If (nPos := aScan(aVlrCBox, {|x| x[1] == aValues[nX]})) > 0
				cRet += aVlrCBox[nPos][1] + "=" + aVlrCBox[nPos][2] 
			Else
				cRet += aValues[nX]
			EndIf
		Next nX
	Else
		lRet := .F.
	EndIf
Else
	lRet := .F.
EndIf

If !lRet
	cRet := T38Arr2Str(aValues)
EndIf

Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38ALoadCab

Carrega campos do cabe�alho de pesquisa 

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38ALoadCab(oField)
Return FWLoadByAlias(oField, oTblHeader:GetAlias(), oTblHeader:GetRealName())

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38ALoadGrid

Carrega campos da grid (resultado da pesquisa) 

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38ALoadGrid( oGrid )
Return FWLoadByAlias(oGrid, oTblDetail:GetAlias(), oTblDetail:GetRealName(), , .F.)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38ALoadGrid

Retorna a filial para query de acordo com o compartilhamento  

@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function cExpFil(cTabela)

Local cCompE := FWModeAccess(cTabela,1)
Local cCompU := FWModeAccess(cTabela,2)
Local cCompF := FWModeAccess(cTabela,3)
Local nTam	 := FWSizeFilial()
Local cRet	 := '%%'

If cCompE == "C" //Empresa Compartilhada
	nTam -= Len(FWSM0Layout( , 1)) 
EndIf

If cCompU == "C" //Unidade Compartilhada
	nTam -= Len(FWSM0Layout( , 2))
EndIf

If cCompF == 'C' //Filial Compartilhada
	nTam -= Len(FWSM0Layout( , 3))
EndIf

Return nTam