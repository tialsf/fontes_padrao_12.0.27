#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA038C.CH"		

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AutAss

Fun��o para associa��o autom�tica dos itens da fatura a�rea

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38AutAss(nTotConc, aItemG6I, oMessage) 

Local aArea      := GetArea()
Local oModel 	 := Nil
Local oModelG6I	 := Nil
Local oModelConc := Nil
Local cAliasAux	 := ""
Local nX		 := 0
Local nPercAtu	 := 0
Local cErroAss	 := ""
Local lRet		 := .T.
Local lAtuMsg	 := ValType(oMessage) == "O"

Default nTotConc := 0

oModel := FwLoadModel("TURA038D")
oModel:SetOperation(MODEL_OPERATION_UPDATE)

If !Empty(aItemG6I)
	oModel:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_ITEM", T39Arr2Str(aItemG6I), MVC_LOADFILTER_IS_CONTAINED}})
EndIf

oModel:Activate()

oModelG6I := oModel:GetModel( 'G6I_ITENS' )

For nX := 1 To oModelG6I:Length()
	oModelG6I:GoLine(nX)
	
	If !Empty(oModelG6I:GetValue("G6I_SITUAC")) .And. oModelG6I:GetValue("G6I_SITUAC") <> "1" //Item da fatura j� associado
		Loop
	ElseIf oModelG6I:GetValue("G6I_TPFOP") == "0" //Tipo de FOP desconhecida 
		Loop
	ElseIf oModelG6I:GetValue("G6I_STATUS") == "2" //Item Cancelado
		Loop		
	EndIf
	
	cAliasAux := GetNextAlias()
	If T38FindDR(cAliasAux, oModel) 
		BEGIN TRANSACTION
		
			//Pesqisa se j� existe uma concilia��o aberta para este posto ou cria uma nova
			If T38AddConc(oModel, (cAliasAux)->G3R_OPERAC, (cAliasAux)->G3R_POSTOR, (cAliasAux)->G3M_NIATA, "1")
				
				//Cria o vinculo da G6I com G3R				
				If T38G6IxG3R(oModel, (cAliasAux)->G3R_MSFIL, (cAliasAux)->G3R_NUMID, (cAliasAux)->G3R_IDITEM, (cAliasAux)->G3R_NUMSEQ, .T., @cErroAss)	
					
					//Faz o update na G3R para este n�o ser associado novamente
					T38UpdG3R(oModel, (cAliasAux)->G3R_NUMID, (cAliasAux)->G3R_IDITEM, (cAliasAux)->G3R_NUMSEQ, (cAliasAux)->G3R_FILIAL, (cAliasAux)->G3R_MSFIL)
				Else					
					lRet := .F.	
					DisarmTransaction()
					Break
				EndIf
			Else
				lRet := .F.	
				DisarmTransaction()
				Break		
			EndIf
		END TRANSACTION
	EndIf
	
	If Select(cAliasAux) > 0
		(cAliasAux)->(DbCloseArea())
	EndIf	
	
	If lAtuMsg
		T38AtuMsg(oMessage, @nPercAtu, nX, oModelG6I:Length(), STR0002) //"Aguarde, gerando associa��es... "
	EndIf	
Next nX

If lRet
	oModel:GetModel("G6J_ITENS"):SetOnlyQuery(.T.)
	If oModel:VldData()
		oModel:CommitData()

		//Variavel utilizada na mensagem de sucesso, informa quantas concilia��es a fatura possui
		If oModel:GetModel("G6J_ITENS"):IsEmpty()
			nTotConc := 0
		Else
			nTotConc := oModel:GetModel("G6J_ITENS"):Length(.T.)
		EndIf
	Else
		lRet := .F.
		If IsInCallStack("Tu038Asso")
			AtuT38Msg(STR0003 + oModel:GetValue("G6H_MASTER", "G6H_FATURA") + CRLF + oModel:GetErrorMessage()[6])		//"Fatura: "
		Else
			JurShowErro(oModel:GetErrorMessage())
		EndIf
		
	EndIf
	oModel:GetModel("G6J_ITENS"):SetOnlyQuery(.F.)
Else
	lRet := .F.
	If IsInCallStack("Tu038Asso")
		JurShowErro(oModelConc:GetErrorMessage() + '   '  + cErroAss)
	EndIf
EndIf

oModel:DeActivate()
oModel:Destroy()	

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38FindDR

Fun��o para pesquisa do documento de reserva com base no item da fatura

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38FindDR(cAliasAux, oModel) 

Local aArea      := GetArea()
Local aCondAss 	 := T38CndAss(oModel:GetValue("G6I_ITENS", "G6I_TIPOIT"))
Local cCondTp 	 := T38Tp2Sql(aCondAss)
Local cCondKey	 := T38Key2Sql(T38KeyAss(), .T., oModel )	
Local cCondWhere := ""
Local cFatura	 :=	oModel:GetValue("G6H_MASTER", "G6H_FATURA")
	
If Empty(aCondAss)	
	Return .F.
EndIf	
	
If !Empty(cCondTp) .And. !Empty(cCondKey)
	cCondWhere := "% AND " + cCondTp + " AND " + cCondKey + " %"  	
ElseIf !Empty( cCondTp )
	cCondWhere := "% AND " + cCondTp + " %"  		
ElseIf !Empty(cCondKey)
	cCondWhere := "% AND " + cCondKey + " %" 
EndIf

BeginSql Alias cAliasAux
	SELECT G3R_MSFIL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, G3R_POSTOR, G3R_FILIAL, G3R_CONJUG, G3R_OPERAC, G3M_NIATA
	FROM %Table:G3R% G3R
	INNER JOIN %Table:G6H% G6H ON G6H_FILIAL = %xFilial:G6H% AND G6H_FORNEC = G3R_FORREP AND G6H_LOJA = G3R_LOJREP AND G6H_FATURA = %Exp:cFatura% AND G6H.%notDel%
	INNER JOIN %Table:G3Q% G3Q ON G3Q_FILIAL = G3R_FILIAL AND G3Q_NUMID = G3R_NUMID AND G3Q_IDITEM = G3R_IDITEM AND G3Q_NUMSEQ = G3R_NUMSEQ AND G3Q.%notDel%		
	INNER JOIN %Table:G3M% G3M ON G3M_FILIAL = G3R_FILIAL AND G3M_CODIGO = G3R_POSTOR AND G3M.%notDel%
	WHERE G3R_CONCIL = %Exp:Space(TamSx3("G3R_CONCIL")[1])% AND G3R.%notDel% %Exp:cCondWhere%
	ORDER BY G3R_EMISS, G3R_IDITEM, G3R_NUMSEQ 	
EndSql

RestArea(aArea)

Return (cAliasAux)->(!EOF())

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AddConc

Fun��o para avalidar se j� existe uma concilia��o aberta para o item da fatura, ou criar uma nova

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38AddConc(oModel, cG3ROperac, cPosto, cNumIATA, cTpAss)

Local aArea   := GetArea()
Local oMdlG6H := oModel:GetModel("G6H_MASTER") 
Local oMdlG6J := oModel:GetModel("G6J_ITENS") 
Local aSeek	  := {}
Local cOperac := ""
Local lRet    := .T.

If cG3ROperac == "M"
	cOperac := "3"
Else
	cOperac := IIF(cG3ROperac == "2", "2", "1") 
EndIf

aAdd(aSeek, {"G6J_FATURA", oMdlG6H:GetValue("G6H_FATURA")})
aAdd(aSeek, {"G6J_CPIATA", cPosto									  })
aAdd(aSeek, {"G6J_TIPO"	 , cOperac 									  })

If !oModel:GetModel( 'G6J_ITENS' ):SeekLine( aSeek )
	//Se n�o encontrou nenhuma concilia��o em aberto que satisfa�a as condi��es, cria uma nova
	If oMdlG6J:IsEmpty() .Or. oMdlG6J:Length() < oMdlG6J:AddLine() 
		oMdlG6J:SetValue("G6J_CONCIL", TurGetNum("G6J", "G6J_CONCIL"))
		oMdlG6J:SetValue("G6J_FATURA", oMdlG6H:GetValue("G6H_FATURA")) 
		oMdlG6J:SetValue("G6J_CPIATA", cPosto)	
		oMdlG6J:SetValue("G6J_NRIATA", cNumIATA)	
		oMdlG6J:SetValue("G6J_ASSOCI", "1")
		oMdlG6J:SetValue("G6J_APROVA", "1")	
		oMdlG6J:SetValue("G6J_TIPO"  , cOperac)
		oMdlG6J:SetValue("G6J_AUTO"  , cTpAss)
		oMdlG6J:SetValue("G6J_EFETIV", "2")
		oMdlG6J:SetValue("G6J_FATFOR", oMdlG6H:GetValue("G6H_FATFOR"))
		oMdlG6J:SetValue("G6J_FORNEC", oMdlG6H:GetValue("G6H_FORNEC"))
		oMdlG6J:SetValue("G6J_LOJA"  , oMdlG6H:GetValue("G6H_LOJA"))
		oMdlG6J:SetValue("G6J_VENCTO", oMdlG6H:GetValue("G6H_VENCTO"))
		
		//TODO Devido falha na transa��o, devemos gravar a concilia��o via reclock at� que o problema seja resolvido
		//mas n�o podemos deixar de atualizar a G6J no model pois alguns dados s�o verificados durante o processo
		//a corre��o pode ser feita ap�s libera��o e aplica��o da corre��o da issue DFRM1-10646
		RecLock("G6J", .T.)
		G6J->G6J_FILIAL := xFilial("G6J")
		G6J->G6J_CONCIL	:= oMdlG6J:GetValue("G6J_CONCIL")
		G6J->G6J_FATURA	:= oModel:GetValue("G6H_MASTER", "G6H_FATURA")
		G6J->G6J_CPIATA	:= cPosto
		G6J->G6J_NRIATA	:= cNumIATA
		G6J->G6J_ASSOCI	:= "1"
		G6J->G6J_APROVA	:= "1"
		G6J->G6J_TIPO	:= cOperac
		G6J->G6J_AUTO	:= cTpAss
		G6J->G6J_EFETIV	:= "2"
		G6J->G6J_DIVERG	:= "2"
		G6J->G6J_FATFOR	:= oMdlG6H:GetValue("G6H_FATFOR")
		G6J->G6J_FORNEC	:= oMdlG6H:GetValue("G6H_FORNEC")
		G6J->G6J_LOJA	:= oMdlG6H:GetValue("G6H_LOJA")
		G6J->G6J_VENCTO	:= oMdlG6H:GetValue("G6H_VENCTO")
		G6J->(MsUnlock())
	Else
		lRet  := .F.
	EndIf
EndIf

RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38G6IxG3R

Fun��o para criar o v�nculo do item da fatura com o documento de reserva

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38G6IxG3R(oModel, cFilDR, cNumId, cIdItem, cNumSeq, lAutomatico, cErroAss)

Local aArea      := GetArea()
Local cError     := ""
Local oMdlG6I    := oModel:GetModel("G6I_ITENS")
Local oMdlG6J    := oModel:GetModel("G6J_ITENS")
Local cFilialAux := oMdlG6I:GetValue("G6I_FILIAL")
Local cFatura	 := oMdlG6I:GetValue("G6I_FATURA")
Local cItem		 := oMdlG6I:GetValue("G6I_ITEM")
Local lRet       := .T.

Default	cErroAss  := ""

If oMdlG6I:SetValue("G6I_CONCIL", oMdlG6J:GetValue("G6J_CONCIL"))
	If oMdlG6I:SetValue("G6I_FILDR", cFilDR)	
		If oMdlG6I:SetValue("G6I_NUMID", cNumId)	
			If oMdlG6I:SetValue("G6I_IDITEM", cIdItem)
				If oMdlG6I:SetValue("G6I_NUMSEQ", cNumSeq)
					If !oMdlG6I:SetValue("G6I_ASSAUT", IIF(lAutomatico, "1", "2"))	//1-Sim;2-N�o
						cError:= "G6I_ASSAUT" + STR0007 + IIF(lAutomatico, "1", "2")		// " N�o pode ser preenchido: "
						lRet := .F.					
					EndIf
				Else
					cError:= "G6I_NUMSEQ" + STR0007 + cNumSeq		// " N�o pode ser preenchido: "
					lRet := .F.
				EndIf
			Else
				cError:= "G6I_IDITEM" + STR0007 + cIdItem		// " N�o pode ser preenchido: "
				lRet := .F.
			EndIf
		Else
			cError:= "G6I_NUMID" + STR0007 + cNumId		// " N�o pode ser preenchido: "
			lRet := .F.
		EndIf
	Else
		cError:= "G6I_FILDR" + STR0007 + cFilDR		// " N�o pode ser preenchido: "
		lRet := .F.
	EndIf
Else
	cError:= "G6I_CONCIL" + STR0007 + oMdlG6J:GetValue("G6J_CONCIL") 		// " N�o pode ser preenchido: "
	lRet := .F.
EndIf

//Se associa��o autom�tica ou gera acerto na associa��o manual, o campo 'Status' � iniciado como 4 - Pendente avalia��o autom�tica,
//na rotina de acerto autom�tico estes casos ser�o avaliados e alterados para 2 - Associado
If lRet
	If !oMdlG6I:SetValue("G6I_SITUAC", IIF(lAutomatico .Or. !IsInCallStack("TA39JCAlFO"), "4", "2"))		//5 - Pendente avalia��o natureza
		cError:= "G6I_SITUAC" + STR0007 + "4"		// " N�o pode ser preenchido: "
		lRet := .F.
	Else
		G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
		If G6I->(DbSeek(cFilialAux + cFatura + cItem))
			RecLock("G6I")	
			G6I->G6I_CONCIL	:= oMdlG6J:GetValue("G6J_CONCIL")				
			G6I->G6I_FILDR	:= cFilDR
			G6I->G6I_NUMID	:= cNumId
			G6I->G6I_IDITEM	:= cIdItem
			G6I->G6I_NUMSEQ	:= cNumSeq
			G6I->G6I_ASSAUT	:= IIF(lAutomatico, "1", "2")
			G6I->G6I_SITUAC	:= oMdlG6I:GetValue("G6I_SITUAC")
			G6I->(MsUnlock())
		EndIf	
	EndIf
EndIf

cErroAss := cError

RestArea(aArea)

oMdlG6I := Nil
oMdlG6J := Nil

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38UpdG3R

Fun��o para criar o vinculo do documento de reserva com a concilia��o

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38UpdG3R( oModel, cNumId, cIdItem, cNumSeq, cFilG3R, cG3RMsFil )

Local aArea := GetArea()
Local cSts  := ""
Local lRet  := .T.	
	
//Cria o v�nculo da G3R com a concilia��o	
G3R->(DbSetOrder(1))	// G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ+G3R_CONINU
If G3R->(DbSeek(cFilG3R + cNumId + cIdItem))
	While(G3R->(G3R_FILIAL + G3R_NUMID + G3R_IDITEM) == cFilG3R + cNumId + cIdItem)
		If(G3R->G3R_NUMSEQ == cNumSeq .Or. G3R->G3R_SEQACR == cNumSeq) .And. Empty(G3R->G3R_CONCIL)
			RecLock("G3R",.F.)					
			G3R->G3R_CONCIL	:= oModel:GetValue("G6J_ITENS", "G6J_CONCIL")
			G3R->G3R_FATURA	:= oModel:GetValue("G6J_ITENS", "G6J_FATURA")
			G3R->G3R_FILCON	:= xFilial("G6J")
			G3R->(MsUnlock())
		EndIf
		G3R->(DbSkip())
	EndDo
Else
	lRet := .F.
EndIf

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38CndAss

Fun��o que retorna os valores aceitos por tipo para a realiza��o da associa��o

@type function
@author Anderson Toledo
@since 23/08/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38CndAss( cTipo )

Local aCondAss := {}

Do Case
	Case cTipo == "0" //Documento Cancelado
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}})					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1", "3"}}) 				//Tipo de opera��o: 1 = Emiss�o, 3 = Reemiss�o
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}}) 		//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado

	Case cTipo == "1"	//Venda Mista (MCO, MPD, ARV, etc.) 
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}})					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1", "3"}})				//Tipo de opera��o: 1 = Emiss�o, 3 = Reemiss�o
		aAdd(aCondAss, {"G3R_TPDOC"	, {"2", "3", "7", "8"}})	//Tipo documento: 2 = Bilhete, 3 = Localizador, 7 = EMD, 8 = MCO
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado

	Case cTipo == "2"	//Venda de Bilhete (e-Ticket)
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}})					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1", "3"}})				//Tipo de opera��o: 1 = Emiss�o, 3 = Reemiss�o
		aAdd(aCondAss, {"G3R_TPDOC"	, {"2", "3"}})				//Tipo documento: 2 = Bilhete, 3 = Localizador
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado
		
	Case cTipo == "3"	//Reembolso
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}})					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"2"}})					//Tipo de opera��o: 2 = Reembolso
		aAdd(aCondAss, {"G3R_TPDOC"	, {"2", "3", "7", "8"}})	//Tipo documento: 2 = Bilhete, 3 = Localizador, 7 = EMD, 8 = MCO
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado

	Case cTipo == "4"	//ADM
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}}) 					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1", "3"}}) 				//Tipo de opera��o: 1 = Emiss�o, 3 = Reemiss�o
		aAdd(aCondAss, {"G3R_TPDOC"	, {"5"}}) 					//Tipo documento: 5 = ADM
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado
		
	Case cTipo == "5"	//ACM
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}}) 					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"2"}}) 					//Tipo de opera��o: 2 = Reembolso
		aAdd(aCondAss, {"G3R_TPDOC"	, {"6"}}) 					//Tipo documento: 6 = ACM
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado

	Case cTipo == "7"	//VMPD 
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}}) 					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1", "3"}}) 				//Tipo de opera��o: 1 = Emiss�o, 3 = Reemiss�o
		aAdd(aCondAss, {"G3R_TPDOC"	, {"7"}}) 					//Tipo documento: 7 = EMD
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado

	Case cTipo == "9"	//BSP 
		aAdd(aCondAss, {"G3R_TPSEG"	, {"1"}}) 					//Tipo de segmento: 1 = A�reo
		aAdd(aCondAss, {"G3R_OPERAC", {"1"}}) 					//Tipo de opera��o: 1 = Emiss�o
		aAdd(aCondAss, {"G3R_TPDOC"	, {"5"}}) 					//Tipo documento: 5 = ADM
		aAdd(aCondAss, {"G3R_STATUS", {"1", "3", "4"}})			//Status: 1 = Pendente, 3 = Finalizado, 4 = Cancelado
EndCase

Return aCondAss

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38KeyAss

Fornece os campos chaves e valores para a associa��o

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38KeyAss()

Local aKeyAss := {}
	
aAdd(aKeyAss, {"G3R_FORREP", {|oMdl| FwFldGet("G6H_FORNEC", ,oMdl)}, .T., {|| .T.}})	
aAdd(aKeyAss, {"G3R_LOJREP", {|oMdl| FwFldGet("G6H_LOJA"  , ,oMdl)}, .T., {|| .T.}})	
aAdd(aKeyAss, {"G3R_DOC"   , {|oMdl| FwFldGet("G6I_BILHET", ,oMdl)}, .F., {|| !Empty(FwFldGet("G6I_BILHET"))}})
aAdd(aKeyAss, {"G3R_DOC"   , {|oMdl| FwFldGet("G6I_LOCALI", ,oMdl)}, .F., {|| !Empty(FwFldGet("G6I_LOCALI"))}})
	
Return aKeyAss

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38Key2Sql

Fun��o para parse do vetor de tipos aceitos da concilia��o para SQL

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38Key2Sql(aCond, lAutomatico, oModel)

Local cCond := ""
Local nX	:= 0

Default lAutomatico := .T.

For nX := 1 To Len(aCond)
	If (aCond[nX][3] .Or. lAutomatico) .And. Eval(aCond[nX][4])
		If Len(cCond) > 0 
			cCond += " AND "
		EndIf
		cCond += aCond[nX][1] + " = '" + Eval(aCond[nX][2], oModel) + "'"
	EndIf
Next nX
		
Return cCond

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38Tp2Sql
(long_description)
@type function
@author anderson.toledo
@since 23/08/2016
@version 1.0
@param aCond, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38Tp2Sql(aCond)

Local cCond := ""
Local nX	:= 0
Local nY	:= 0

For nX := 1 To Len(aCond)
	If nX > 1
		cCond += " AND "
	EndIf
	
	If Len(aCond[nX][2]) > 1
		cCond += "( "
		For nY := 1 To Len(aCond[nX][2])
			If nY > 1 
				cCond += " OR "
			EndIf
			cCond += aCond[nX][1] + " = '" + aCond[nX][2][nY] + "'"	
		Next nY
		cCond += " )"
	Else
		cCond += aCond[nX][1] + " = '" + aCond[nX][2][1] + "'"
	EndIf
Next nX

Return cCond

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38GetConc

Fun��o que retorna dos os c�digos da concilia��o vinculados a fatura

@type function
@author Anderson Toledo
@since 23/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38GetConc(cFatura, aItemG6I, lPendAval)

Local aArea     := GetArea()
Local aFaturas	:= {}	
Local cExpWhere	:= ""
Local cAliasAux := GetNextAlias()

Default aItemG6I  := {}
Default lPendAval := .F.

If !Empty(aItemG6I)
	cExpWhere += " AND G6I_ITEM IN " + T39Arr2Str(aItemG6I, .T.) + " "
EndIf

If lPendAval
	cExpWhere += " AND G6I_SITUAC = '4' "
EndIf	

cExpWhere := "% " + cExpWhere + " %"

BeginSql Alias cAliasAux
	SELECT DISTINCT G6I_CONCIL 
	FROM %Table:G6I%
	WHERE G6I_FILIAL = %xFilial:G6I% AND G6I_FATURA = %Exp:cFatura% AND G6I_CONCIL <> %Exp:Space(TamSx3("G3R_CONCIL")[1])% AND %notDel% %Exp:cExpWhere%
EndSql

While (cAliasAux)->(!EOF())
	If !Empty((cAliasAux)->G6I_CONCIL)
		aAdd(aFaturas, (cAliasAux)->G6I_CONCIL)
	EndIf
	(cAliasAux)->(DbSkip())
EndDo
(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return aFaturas

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AutAcr

Fun��o utilizada para verificar e gerar os acertos necess�rios em itens associados automaticamente

@type function
@author Anderson Toledo
@since 23/08/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38AutAcr(cFatura, aItemG6I, oMessage)

Local oModelConc := Nil
Local oModelG6J  := Nil 
Local oModelG6I	 := Nil
Local oModelG48B := Nil
Local aConcil 	 := T38GetConc(cFatura, aItemG6I, .T.)
Local cMsg		 := ""
Local lExistFunc := Findfunction("U_TURNAT")
Local lAtuMsg	 := ValType(oMessage) == "O"
Local lExit		 := .F.
Local lGerouAc   := .F.
Local lRet		 := .T.
Local nX		 := 0
Local nY		 := 0
Local nPercAtu	 := 0	
Local nTotal	 := 0

//J� aprova os itens com valores e FOP�s corretas via update
T38CAprIt(cFatura, aItemG6I)

G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA
G6I->(DbSetOrder(7))	// G6I_FILIAL+G6I_CONCIL
oModelConc := FwLoadModel("TURA039MDL")
oModelConc:GetModel("G6I_ITENS"):bLoad := {|oMdl| TA038CLOAD(oMdl)}

For nX := 1 To Len(aConcil)
	If G6J->(DbSeek(xFilial("G6J") + aConcil[nX]))
		G6I->(DbSeek(xFilial("G6J") + aConcil[nX]))
		lExit 	 := .F.
		nPercAtu := 0
		nItemAtu := 1	

		If (nTotal := T38CTotConcil(cFatura, aConcil[nX], "4"))
			While !lExit
				lGerouAc := .F.
				oModelConc:SetOperation(MODEL_OPERATION_UPDATE)
				oModelConc:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_CONCIL", aConcil[nX], MVC_LOADFILTER_EQUAL}})
				oModelConc:Activate()
	
				oModelG6J  := oModelConc:GetModel("G6J_MASTER")
				oModelG6I  := oModelConc:GetModel("G6I_ITENS")
				oModelG48B := oModelConc:GetModel("G48B_ITENS")

				oModelG6J:SetOnlyQuery(.F.)
				oModelG6I:SetOnlyQuery(.F.)
				oModelG48B:SetNoUpdateLine(.F.)
				
				If oModelG6I:IsEmpty()
					lExit := .T.
				Else
					If oModelG6I:GetValue("G6I_CONCIL") == aConcil[nX] .And. oModelG6I:GetValue("G6I_SITUAC") == "4" 
						cFatura  := oModelG6J:GetValue("G6J_FATURA")
						cItemFat := oModelG6I:GetValue("G6I_ITEM")
						cNumID   := oModelG6I:GetValue("G6I_NUMID")
						cIdItem  := oModelG6I:GetValue("G6I_IDITEM")
					
						If lAtuMsg
							cMsg := I18N(STR0001, {nX, Len(aConcil)}) //"Verificando necessidade de acertos, concilia��o #1[Atual]# de #2[Total]#. Progresso: "
							T38AtuMsg(oMessage, @nPercAtu, nItemAtu, nTotal, cMsg)
						EndIf
				
						//Realiza o ajuste dos acordos de fornecedor
						T38CAtuAco(oModelConc)				
	
						//Ajusta as naturezas
						If lExistFunc
							TA038AtNat(oModelConc)
						EndIf
	
						//Gera acerto caso seja necess�rio
						If (lGerouAc := TA039VerAc(oModelConc))
							oModelG6I:LoadValue("G6I_SITUAC", "2") //Documento Associado
						EndIf
						
						If lGerouAc .And. oModelConc:VldData()
							oModelConc:CommitData()
						Else
							If IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0003 + cFatura + CRLF + STR0005 + cItemFat + CRLF + STR0006 + cNumID + ' - ' + cIdItem + CRLF + oModelConc:GetErrorMessage()[6]) 	// "Fatura: "   // "Item Fatura: "  // "RV - Seq.: "
							Else
								JurShowErro(oModelConc:GetErrorMessage())
							EndIf
						
							//Desassocia todos os RV's selecionados					
							If Tur39EsAss(oModelConc, .T.)
								oModelG6J:SetOnlyQuery(.T.)
								oModelG6I:SetOnlyQuery(.T.)
								
								//Retira o valor dos campos utilizados no Relation, impedindo que o model reatribua os valores na G3R
								oModelG6J:LoadValue("G6J_FILIAL", "")
								oModelG6J:LoadValue("G6J_CONCIL", "")
								
								If oModelConc:VldData()
									oModelConc:CommitData()
								Else
									If IsInCallStack("Tu038Asso") 
										AtuT38Msg(STR0004 + STR0003 + cFatura + CRLF + STR0005 + cItemFat + CRLF + STR0006 + cNumID + ' - ' + cIdItem + CRLF + oModelConc:GetErrorMessage()[6]) 	// "N�o foi poss�vel desassociar o item automaticamente, dever� ser realizado via concilia��o manualmente."  // "Fatura: "  // "Item Fatura: "  // "RV - Seq.: "
									Else
										JurShowErro( oModelConc:GetErrorMessage() )
									EndIf     
																
									//TRATAR PARA EXCLUIR
									If ApagarG6J(cFatura, aConcil[nX])
										oModelG6J:SetOnlyQuery(.F.)
										oModelG6I:SetOnlyQuery(.F.)
										oModelConc:DeActivate()
										oModelConc:SetOperation(MODEL_OPERATION_DELETE)
										oModelConc:Activate()
										If oModelConc:VldData()
											oModelConc:CommitData()
										EndIf
									EndIf
								EndIf
							Else
								If IsInCallStack("Tu038Asso")
									AtuT38Msg(STR0004 + STR0003 + cFatura + CRLF + STR0005 + cItemFat + CRLF + STR0006 + cNumID + ' - ' + cIdItem + CRLF + oModelConc:GetErrorMessage()[6]) 	// "N�o foi poss�vel desassociar o item automaticamente, dever� ser realizado via concilia��o manualmente."  // "Fatura: "  // "Item Fatura: "  // "RV - Seq.: "
								Else
									JurShowErro(oModelConc:GetErrorMessage())
								EndIf
							EndIf
						EndIf
	
						nItemAtu++
						If nItemAtu > nTotal 
							lExit := .T.
						EndIf
					EndIf
				EndIf
				oModelConc:DeActivate()
			EndDo
		EndIf
	EndIf
Next nX
oModelConc:Destroy()

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38CAtuAco

Fun��o utilizada para atualiza��o do status de na fatura para o acordo do fornecedor

@type function
@author Anderson Toledo
@since 26/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38CAtuAco( oModel )

Local oModelG3R	  := oModel:GetModel("G3R_ITENS")
Local oModelG48B  := oModel:GetModel("G48B_ITENS")
Local nX          := 0
Local nY 		  := 0
Local lCanUpdLine := !(oModelG48B:CanUpdateLine())	

oModelG48B:SetNoUpdateLine( .F. )

For nX := 1 to oModelG3R:Length()
	If !oModelG48B:IsEmpty()
		For nY := 1 to oModelG48B:Length()
			oModelG48B:GoLine(nY)
			If oModelG48B:GetValue("G48_STATUS") == "1"
				oModelG48B:LoadValue("G48_RECCON", oModelG48B:GetValue("G48_COMSER") == "1")
			EndIf
		Next nY
	EndIf
Next nX

oModelG48B:SetNoUpdateLine(lCanUpdLine)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038AtNat

Fun��o utilizada para atualiza��o de naturezas da concilia��o a�rea

@type function
@author Anderson Toledo
@since 14/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA038AtNat(oModel, cFilDR)

Local oModelG3P	  := oModel:GetModel("G3P_FIELDS")
Local oModelG3R	  := oModel:GetModel("G3R_ITENS")
Local oModelG48B  := oModel:GetModel("G48B_ITENS")
Local cNatureza   := ""
Local cTipo 	  := ""
Local cFornecedor := ""
Local cLoja 	  := ""	
Local nX		  := 0
Local nY		  := 0
Local lCanUpdLine := !(oModelG48B:CanUpdateLine())	

Default cFilDR	  := cFilAnt

oModelG48B:SetNoUpdateLine(.F.)

For nX := 1 To oModelG3R:Length()
	oModelG3R:GoLine(nX)

	If Empty(oModelG3R:GetValue("G3R_NATURE"))
		cTipo     := IIF( oModelG3R:GetValue("G3R_OPERAC") $ "1|3|4", "1", "2")
		cNatureza := U_TURNAT(oModelG3R:GetValue("G3R_MSFIL"), cTipo, "", oModelG3P:GetValue("G3P_SEGNEG"), oModelG3R:GetValue("G3R_PROD"), "2", oModelG3R:GetValue("G3R_FORREP"), oModelG3R:GetValue("G3R_LOJREP"))

		SED->(DbSetOrder(1))	// ED_FILIAL+ED_CODIGO
		If SED->(DbSeek(xFilial("SED", cFilDR) + cNatureza))
			oModelG3R:LoadValue("G3R_NATURE", cNatureza)
		EndIf
	EndIf

	If !oModelG48B:IsEmpty()
		SED->(DbSetOrder(1))	// ED_FILIAL+ED_CODIGO
		G4W->(DbSetOrder(1))	// G4W_FILIAL+G4W_CODACO+G4W_CODREV+G4W_MSBLQL
		For nY := 1 To oModelG48B:Length()
			oModelG48B:GoLine(nY)

			If Empty(oModelG48B:GetValue("G48_NATURE"))
				If oModelG48B:GetValue("G48_TPACD") == "1"
					cTipo := "3"
				ElseIf oModelG48B:GetValue("G48_TPACD") == "2"
					cTipo := "4"
				Else
					Loop
				EndIf

				G4W->(DbSeek(xFilial("G4W") + oModelG48B:GetValue("G48_CODACD") + oModelG48B:GetValue("G48_CODREC")))
				If G4W->G4W_TPENT == "2" 
					//Fornecedor de Reporte
					cFornecedor := oModelG3R:GetValue("G3R_FORREP")
					cLoja 		:= oModelG3R:GetValue("G3R_LOJREP")
				Else 
					//Fornecedor de Produto
					cFornecedor	:= oModelG3R:GetValue("G3R_FORNEC")
					cLoja		:= oModelG3R:GetValue("G3R_LOJA")
				EndIf
			
				cNatureza := U_TURNAT(oModelG3R:GetValue("G3R_MSFIL"), cTipo, oModelG48B:GetValue("G48_CLASS"), oModelG3P:GetValue("G3P_SEGNEG"), oModelG3R:GetValue("G3R_PROD"), "2", cFornecedor, cLoja)
				If SED->(DbSeek(xFilial("SED") + cNatureza))
					oModelG48B:LoadValue("G48_NATURE", cNatureza)
				EndIf
			EndIf
		Next nY
	EndIf
Next nX

oModelG48B:SetNoUpdateLine(lCanUpdLine)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AtuSt

Fun��o atualiza os status 

@type function
@author Anderson Toledo
@since 26/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38AtuSt(cFatura)

Local oModel	:= Nil 
Local oModelG6J	:= Nil
Local oModelG6I	:= Nil
Local nX		:= 0
Local lRet		:= .T.

Default cFatura	:= G6H->G6H_FATURA	

G6H->(DbSetOrder(1))	// G6H_FILIAL+G6H_FATURA
If G6H->(DbSeek(xFilial("G6H") + cFatura))
	oModel := FwLoadModel("TURA038D")

	oModel:SetOperation(MODEL_OPERATION_UPDATE)
	oModel:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_CONCIL", "''", MVC_LOADFILTER_NOT_EQUAL}})
	oModel:Activate()

	oModelG6J := oModel:GetModel("G6J_ITENS")
	oModelG6I := oModel:GetModel("G6I_ITENS")

	For nX := 1 to oModelG6J:Length()
		oModelG6J:GoLine(nX)
		//Se n�o encontrar nenhum G6I "Associado" ou "Pendente aval.", entende-se que est�o todos "aprovados"
		//e o status da G6J � alterado para "aprovado total"
		If !oModelG6I:SeekLine({{"G6I_CONCIL", oModelG6J:GetValue("G6J_CONCIL")}, {"G6I_SITUAC", "2"}}) .And.;
		   !oModelG6I:SeekLine({{"G6I_CONCIL", oModelG6J:GetValue("G6J_CONCIL")}, {"G6I_SITUAC", "4"}}) .And.;
		   !oModelG6I:SeekLine({{"G6I_CONCIL", oModelG6J:GetValue("G6J_CONCIL")}, {"G6I_SITUAC", "5"}})
			lRet := oModelG6J:LoadValue("G6J_APROVA","3")

		//Se achar algum registro com a situa��o = "Associado", procura-se ao menos um registro "aprovado"
		//para alterar o status da G6J para "parcialmente aprovado"
		ElseIf oModelG6I:SeekLine({{"G6I_CONCIL", oModelG6J:GetValue("G6J_CONCIL")}, {"G6I_SITUAC", "3"}})
			lRet := oModelG6J:LoadValue("G6J_APROVA", "2")
		Else
			lRet := oModelG6J:LoadValue("G6J_APROVA", "1")
		EndIf

		If !lRet
			Exit
		EndIf
	Next nX

	If lRet .And. oModel:VldData() 
		oModel:CommitData()
	Else
		lRet := .F.
		If IsInCallStack("Tu038Asso")
			AtuT38Msg(STR0003 + oModel:GetValue("G6H_MASTER", "G6H_FATURA") + CRLF + oModel:GetErrorMessage()[6]) 	//"Fatura: "
		Else
			JurShowErro(oModel:GetErrorMessage())
		EndIf
	EndIf
	oModel:DeActivate()
	oModel:Destroy()	
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38AtuMsg

Fun��o para atualiza��o da mensagem de processamento informando o percentual j� processado

@type function
@author Anderson Toledo
@since 14/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38AtuMsg(oMessage, nPercAtu, nItemAtu, nTotal, cMsg)

Local nPerc := Int((nItemAtu * 100) / nTotal)

Default	cMsg := ""

If nPerc > nPercAtu
	oMessage:SetText(cMsg + cValToChar(nPerc) + "%")
	ProcessMessages()
	
	//Atualiza o pr�ximo % de atualiza��o da mensagem, sempre de 5% em 5%
	nPercAtu := nPerc + Round(100 / nTotal, 0) 
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38CAprIt

Fun��o para atualiza��o via update de itens pendentes de avalia��o da gera��o de acerto

@type function
@author Anderson Toledo
@since 12/09/2017
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T38CAprIt(cFatura, aItemG6I)

Local cSts 	:= ''

Default aItemG6I := {}

cSts := "UPDATE " + RetSqlName("G6I") + " "
cSts +=		"SET G6I_SITUAC = '2' "
cSts +=		"WHERE G6I_FILIAL + G6I_FATURA + G6I_ITEM IN ( "
cSts += 													"SELECT G6I_FILIAL + G6I_FATURA + G6I_ITEM "
cSts +=															"FROM (SELECT G6I_FILIAL,G6I_FATURA,G6I_ITEM,G6I_MOEDA,G6I_TARIFA,G6I_TAXAS,G6I_TAXADU,G6I_VLRCOM,G6I_VLRINC,G6I_TXREE,G6I_TXCCDU,G3R_OPERAC,G3R_MOEDA,G3R_STATUS,SUM( G3R_TARIFA ) G3R_TARIFA,SUM( G3R_TAXA + G3R_EXTRAS ) G3R_TAXA,SUM( G3R_TAXADU ) G3R_TAXADU,SUM( G3R_VLCOMI ) G3R_VLCOMI,SUM( G3R_VLINCE ) G3R_VLINCE,SUM( G3R_TXREE ) G3R_TXREE,SUM( G3R_TXFORN ) G3R_TXFORN, G6I_TIPOIT "
cSts +=																  "FROM " + RetSqlName("G6I") + " G6I " 
cSts +=																  "INNER JOIN " + RetSqlName("G3R") + " G3R "
cSts +=																			"ON G3R_NUMID = G6I_NUMID "
cSts +=																			"AND G3R_IDITEM = G6I_IDITEM "
cSts +=																			"AND G3R_FILIAL = G6I_FILDR "
cSts +=																			"AND G3R_CONCIL = G6I_CONCIL "
cSts +=																			IIF(TCSrvType() == "AS/400", "AND G3R.@DELETED@ = ' ' ", "AND G3R.D_E_L_E_T_ = ' ' ")
cSts +=															"WHERE G6I_FATURA = '" + cFatura + "' " 
cSts +=																"AND G6I_SITUAC = '4' "
cSts +=																"AND G6I_TPFOP IN (SELECT (CASE " 
cSts +=																								"WHEN G9K_TPFOP = '1' AND G9K_MODO = '1' THEN '1' "	//Intermedia��o
cSts +=																								"WHEN G9K_TPFOP = '3' AND G9K_MODO = '5' THEN '2' " // Cart�o Cliente
cSts +=																								"WHEN G9K_TPFOP = '1' AND G9K_MODO = '4' THEN '2' " // Cart�o Ag�ncia
cSts +=																							"END) "
cSts +=																					"FROM " + RetSqlName("G9K") + " G9K "
cSts +=																					"WHERE G9K_FILIAL = G3R_FILIAL "
cSts +=																									"AND G9K_NUMID = G3R_NUMID "
cSts +=																									"AND G9K_IDITEM = G3R_IDITEM "
cSts +=																									"AND G9K_VLPERC > '0' "
cSts +=																									"AND G9K_CLIFOR = '2' "
cSts +=																									IIF(TCSrvType() == "AS/400", "AND G9K.@DELETED@ = ' ' ", "AND G9K.D_E_L_E_T_ = ' ' ") 
cSts +=																						") " 

If len( aItemG6I ) > 0
	cSts +=															"AND G6I_ITEM IN " + T39Arr2Str(aItemG6I, .T.) + " "	
EndIf

cSts +=																"AND G6I_FILIAL = '" + xFilial("G6I") + "' "
cSts +=																IIF(TCSrvType() == "AS/400", "AND G6I.@DELETED@ = ' ' ", "AND G6I.D_E_L_E_T_ = ' ' ")
cSts +=															"GROUP BY G6I_FILIAL,G6I_FATURA,G6I_ITEM,G6I_MOEDA,G6I_TARIFA,G6I_TAXAS,G6I_TAXADU,G6I_VLRCOM,G6I_VLRINC,G6I_TXREE,G6I_TXCCDU,G3R_OPERAC,G3R_MOEDA,G3R_STATUS,G6I_TIPOIT "
cSts +=														") TRB "
cSts +=														"WHERE G6I_MOEDA = G3R_MOEDA " 
cSts +=															"AND G6I_TARIFA = G3R_TARIFA " 
cSts +=															"AND G6I_TAXAS = G3R_TAXA "
cSts +=															"AND G6I_TXREE = G3R_TXREE "
cSts +=															"AND G6I_TAXADU = (CASE WHEN G3R_OPERAC = 2 THEN G3R_TAXADU * -1 Else G3R_TAXADU END) "
cSts +=															"AND G6I_VLRCOM = (CASE WHEN G3R_OPERAC = 2 THEN G3R_VLCOMI * -1 Else G3R_VLCOMI END) "
cSts +=															"AND G6I_VLRINC = (CASE WHEN G3R_OPERAC = 2 THEN G3R_VLINCE * -1 Else G3R_VLINCE END) "
cSts +=															"AND G6I_TXCCDU = (CASE WHEN G3R_OPERAC = 2 THEN G3R_TXFORN * -1 Else G3R_TXFORN END) "
cSts +=															"AND ((G6I_TIPOIT IN ('1', '2', '3', '4', '5', '7', '9') AND G3R_STATUS ='1') OR (G6I_TIPOIT = '0' AND G3R_STATUS = '4')) "
cSts +=													") "
cSts += IIF(TCSrvType() == "AS/400", " AND @DELETED@ = ' ' ", "AND D_E_L_E_T_ = ' ' ")

Return TCSQLExec(cSts) >= 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T38CTotConcil

Fun��o que retorna a quantidade de itens de um determinado STATUS de uma concilia��o 

@type function
@author Anderson Toledo
@since 14/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T38CTotConcil(cFatura, cConcil, cStatus)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias()
Local cExpWhere := ""
Local nRet		:= 0

If !Empty(cStatus)
	cExpWhere := "% AND G6I_SITUAC = '" + cStatus + "' %"
EndIf

BeginSql Alias cAliasAux
	SELECT COUNT(*) TOTAL
	FROM %Table:G6I%
	WHERE G6I_FATURA = %Exp:cFatura% AND G6I_CONCIL = %Exp:cConcil% AND G6I_FILIAL = %xFilial:G6I% AND %notDel% %Exp:cExpWhere%
EndSql

nRet := (cAliasAux)->TOTAL
(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return nRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ApagarG6J

Fun��o que retorna se h� itens numa concilia��o 

@type function
@author Anderson Toledo
@since 14/09/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ApagarG6J(cFatura, cConcil)

Local aArea     := GetArea()
Local lRet      := .F.
Local cAliasAux := GetNextAlias()

BeginSql Alias cAliasAux
	SELECT COUNT(*) TOTAL
	FROM %Table:G6I%
	WHERE G6I_FATURA = %Exp:cFatura% AND G6I_CONCIL = %Exp:cConcil% AND G6I_FILIAL = %xFilial:G6I% AND %notDel%
EndSql

lRet := (cAliasAux)->TOTAL == 0
(cAliasAux)->(DbCloseArea())
	
RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038CLOAD

Carrega apenas um registro da G6I.

@since   	05/03/2015
@version  	12.1.4
/*/
//------------------------------------------------------------------------------------------
Static Function TA038CLOAD(oMdl)

Local aArea	  := GetArea()
Local aCampos := oMdl:GetStruct():GetFields()
Local aLoad	  := {}
Local aDados  := {}
Local nX	  := 0

G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
G6I->(DbSeek(xFilial("G6I") + G6J->G6J_FATURA))
While G6I->G6I_FATURA == G6J->G6J_FATURA
	If G6I->G6I_SITUAC != '4' .Or. Empty(G6I->G6I_CONCIL)
		G6I->(DbSkip())
		Loop
	Else
		If G6I->G6I_CONCIL == G6J->G6J_CONCIL
			For nX := 1 To Len(aCampos)
				If !(aCampos[nX][MODEL_FIELD_VIRTUAL])
					aAdd(aLoad, G6I->&(aCampos[nX][MODEL_FIELD_IDFIELD]))
				Else
					aAdd(aLoad, CriaVar(aCampos[nX][MODEL_FIELD_IDFIELD], .F.))
				EndIf
			Next
			aDados := {{G6I->(Recno()), aLoad}}
			Exit
		Else
			G6I->(DbSkip())
		EndIf
	EndIf
EndDo

RestArea(aArea)

Return aDados