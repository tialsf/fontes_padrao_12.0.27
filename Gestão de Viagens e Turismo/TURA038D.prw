#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

ModelDef para manipulação das associações na conciliações

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel := MPFormModel():New("TURA038D", /*bPreValidacao*/, /*bPosValid*/, /*bCommit*/, /*bCancel*/)	

oModel:AddFields("G6H_MASTER", /*cOwner*/, FwFormStruct(1, "G6H" , ,.F.))
oModel:GetModel("G6H_MASTER"):SetOnlyQuery(.T.)
	
oModel:AddGrid("G6I_ITENS", "G6H_MASTER", FWFormStruct(1, "G6I" , ,.F.))
oModel:SetRelation("G6I_ITENS", {{"G6I_FILIAL", "xFilial('G6H')"}, {"G6I_FATURA", "G6H_FATURA"}}, G6I->(IndexKey(1)))
oModel:GetModel("G6I_ITENS"):SetNoInsertLine(.T.)											
	
oModel:AddGrid("G6J_ITENS", "G6H_MASTER", FWFormStruct(1, "G6J" , ,.F.))
oModel:SetRelation("G6J_ITENS", {{"G6J_FILIAL", "xFilial('G6H')"}, {"G6J_FATURA", "G6H_FATURA"}}, G6J->(IndexKey(1)))
		
Return oModel