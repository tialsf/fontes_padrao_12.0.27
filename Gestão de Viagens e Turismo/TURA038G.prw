#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA038G

Fun��o para tratamento da importa��o de Fatura A�rea padr�o Gol

@author    Marcelo Cardoso Barbosa
@version   1.00
@since     11/08/2015
/*/
//--------------------------------------------------------------------------------------------------------------------
User Function TURA038G(cArquivo, cPath) 

Local aArea        := GetArea()
Local aMdViG6H     := {}
Local oViewTu38	   := Nil
Local oMdlTu38	   := Nil

Private _nStopHere := 1
Private cG6ITipoIt := "0"
Private cNumLinGol := StrZero(1, TamSX3("G6I_NUMLIN")[1])
Private cG6IItem   := StrZero(1, TamSX3("G6I_ITEM")[1])
Private nTamCabec  := 10
Private nAtuLin    := 1
Private _cLinha    := ""
Private _aCabec    := {}
Private _aItem     := {}
Private _aItens    := {}
Private _aLinha    := {}
Private _cFatura   := GetSXENum("G6H", "G6H_FATURA")
Private _nItem     := 1
Private _nTotFat   := 0
Private _cReport   := " "
Private _cLocali   := ""
Private _cBilhet   := ""
Private _dEmissa   := CToD("")
Private cCiaAerea  := Posicione("G4S", 1, xFilial("G4S") + MV_PAR04, "G4S_ABIATA")

If MV_PAR01 == 2 .And. Empty(cCiaAerea)
	Help("", 1, "Aten��o", , "Por favor, selecione um fornecedor a�reo que possua o campo Abre IATA (G4S_ABIATA) preenchido corretamente.", 1, 0)
	Return .F.
EndIf

If MV_PAR01 == 2 .And. Empty(MV_PAR06)
	Help("", 1, "Aten��o", , "Para o layout (A�reo Localizador) � necess�rio informar a moeda nos par�metros de importa��o.", 1, 0)
	Return .F.
EndIf		

G6H->(DBSetOrder(5)) 		// G6H_FILIAL + G6H_ARQUIV
If G6H->(DBSeek(xFilial("G6H") + AllTrim(Upper(cArquivo)))) .And. !Empty(AllTrim(cArquivo))
	Help( , , "Aten��o", , "N�o � poss�vel importar o arquivo, pois j� existe uma fatura gravada a partir de um arquivo com este nome: " + UPPER(cArquivo), 1, 0)  
	lTURA038OK := .F.
	RollBackSX8()
	Return .F.
EndIf

G4S->(DbSetOrder(1)) 	// G4S_FILIAL+G4S_FORNEC+G4S_LOJA
If G4S->(DBSeek(xFilial("G4S") + cFornec +	cLoja))
	_cReport := G4S->G4S_REPORT 
EndIf 

aAdd(_aCabec, {"G6H_FATURA", _cFatura                    , NIL})
aAdd(_aCabec, {"G6H_TIPO"  , IIF(nTipoImp == 2, "1", "2"), NIL})
aAdd(_aCabec, {"G6H_LAYOUT", "4"                         , NIL})
aAdd(_aCabec, {"G6H_FORNEC", cFornec                     , NIL})
aAdd(_aCabec, {"G6H_LOJA"  , cLoja                       , NIL})
aAdd(_aCabec, {"G6H_EFETIV", "2"                         , NIL})
aAdd(_aCabec, {"G6H_DTIMP" , dDataBase                   , NIL})
aAdd(_aCabec, {"G6H_ARQUIV", Upper(cArquivo)             , NIL})
aAdd(_aCabec, {"G6H_REPORT", _cReport                    , NIL})

FT_FUse(cPath + cArquivo)

While !FT_FEOF()
	_cLinha := Upper(AllTrim(FT_FReadLn()))
	_aLinha := STRTOKARR(_cLinha, ";")
	If nAtuLin <= nTamCabec .Or. (IIF(Len(_aLinha) = 2, _aLinha[1] == "GERA��O:", .F. ) )
		ProcCabec()
	Else
		ProcItens()
	EndIf
	FT_FSkip()
	nAtuLin += 1
EndDo

FT_FUse()

aAdd(_aCabec, {"G6H_VALOR", _nTotFat, NIL})

If !IsBlind()
	FwMsgRun( , {|| ImportGol("G6H", "G6I", _aCabec, _aItens)}, , "Aguarde... Realizando importa��o.")
	Ta038Refsh()
Else
	ImportGol( "G6H", "G6I", _aCabec, _aItens)
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ProcCabec

Processa informa��es do cabe�alho

@author    Marcelo Cardoso Barbosa
@version   1.00
@since     11/08/2015
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ProcCabec()

Local cPeriodo := ""

If Len(_aLinha) > 0
	Do Case
		Case AllTrim(_aLinha[1]) == "FATURA:"
			aAdd(_aCabec, {"G6H_FATFOR", _aLinha[2], NIL})
		
		Case AllTrim(_aLinha[1]) == "PER�ODO:"
			_cPeriodo := IIF(Val(SubsTr(AllTrim(_aLinha[2]), 1, 2)) > 15, "2", "1")
			aAdd(_aCabec, {"G6H_PERCID", cValToChar(MV_PAR07)             , NIL})
			aAdd(_aCabec, {"G6H_PERIOD", cValToChar(MV_PAR08)             , NIL})
			aAdd(_aCabec, {"G6H_MES"   , SubsTr(AllTrim(_aLinha[2]), 4, 2), NIL})
			aAdd(_aCabec, {"G6H_ANO"   , SubsTr(AllTrim(_aLinha[2]), 7, 4), NIL})
			
		Case AllTrim(_aLinha[1]) == "GERA��O:"
			aAdd(_aCabec, {"G6H_EMISSA", CToD(AllTrim(_aLinha[2])), NIL})
			
		Case AllTrim(_aLinha[1]) == "VENCIMENTO:"
			aAdd(_aCabec, {"G6H_VENCTO", CToD(AllTrim(_aLinha[2])), NIL})
			
	EndCase
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ProcCabec

Processa informa��es dos itens

@author    Marcelo Cardoso Barbosa
@version   1.00
@since     11/08/2015
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ProcItens()

Local cNumLin  := ""
Local _nSinal  := 0
Local _nTarAvi := 0
Local _nTarCre := 0
Local _nTarifa := 0
Local _nTaxas  := 0
Local _nTaxaDU := 0
Local _nTxCCDU := 0
Local _nVlrLiq := 0
Local _aPula   := {}
	
aAdd(_aPula, "SUB - TOTAL DOM�STICO")	
aAdd(_aPula, "SEM MOVIMENTO")
aAdd(_aPula, "SUB - TOTAL INTERNACIONAL")
aAdd(_aPula, "TOTAL - � VISTA / A CR�DITO")
aAdd(_aPula, "ISS")	
aAdd(_aPula, "INFORMATIVO: TOTAL SU")
aAdd(_aPula, "FIM DO ARQUIVO")

If Len(_aLinha) >= 1
	If AScan(_aPula, AllTrim(_aLinha[1])) <> 0
		cG6ITipoIt := "0"		
		Return
	EndIf 
EndIf

If Len(_aLinha) > 1 .And. Upper("Valor L�quido") == Upper(AllTrim(_aLinha[1]))
	_nTotFat := Val(StrTran(_aLinha[2], ",", "."))
	Return
EndIf

If Len(_aLinha) == 1 
	Do Case
		Case _aLinha[1] == "VENDAS DOM�STICAS"
			cG6ITipoIt := "1"
		
		Case _aLinha[1] == "VENDAS INTERNACIONAIS"
			cG6ITipoIt := "1"
			
		Case _aLinha[1] == "REEMBOLSOS"
			cG6ITipoIt := "3"
			
	EndCase
	Return

ElseIf Len(_aLinha) > 1
	_aItem := {}

	IF cG6ITipoIt > "0" 
		cNumLin := StrZero(nAtuLin, TamSX3("G6I_NUMLIN")[1])
		
		If nAtuLin == 230
			_nStopHere := 1
		EndIf
		
		If Len(_aLinha) > 2	
			aAdd(_aItem, {"G6I_FATURA", _cFatura                              , NIL})
			aAdd(_aItem, {"G6I_ITEM"  , StrZero(_nItem, TamSX3("G6I_ITEM")[1]), NIL})
			aAdd(_aItem, {"G6I_NUMLIN", cNumLin                               , NIL})
			aAdd(_aItem, {"G6I_NUMCIA", cCiaAerea                             , NIL})
			aAdd(_aItem, {"G6I_MOEDA" , IIF(MV_PAR01 == 2, MV_PAR06, "BRL")   , NIL})
			aAdd(_aItem, {"G6I_ACERTO",  "2"                                  , NIL})
			aAdd(_aItem, {"G6I_SITUAC",  "1"                                  , NIL})
		EndIf
		
		_nItem += 1

		// Apenas para Venda/Reembolso
		If Len(_aLinha) > 2	
			_nSinal  := IIF(cG6ITipoIt == "3", -1, 1)  
			_nTarAvi := Val(StrTran(_aLinha[4], ",", ".")) * _nSinal
			_nTarCre := Val(StrTran(_aLinha[5], ",", ".")) * _nSinal				    
			_nTarifa := IIF(_nTarAvi == 0, _nTarCre, _nTarAvi)				
			_nTaxaDU := Val(StrTran(_aLinha[7], ",", ".")) * -1
			_nTaxas  := (Val(StrTran(_aLinha[6], ",", ".")) -_nTaxaDU) * _nSinal
			_nTxCCDU := Val(StrTran(_aLinha[8], ",", "."))
			_nVlrLiq := Val(StrTran(_aLinha[11], ",", ".")) 
			_nTpFop	 := IIF( AllTrim(_aLinha[12]) == "INV", "1", "2")
			_cLocali := _aLinha[1]
			_cBilhet := _aLinha[2]
			_dEmissa := CtoD(_aLinha[3])

			aAdd(_aItem, {"G6I_TIPOIT", cG6ITipoIt                                , NIL})
			aAdd(_aItem, {"G6I_LOCALI", _cLocali                                  , NIL})
			aAdd(_aItem, {"G6I_EMISSA", CtoD(_aLinha[3])                          , NIL})
			aAdd(_aItem, {"G6I_TARIFA", _nTarifa                                  , NIL})				
			aAdd(_aItem, {"G6I_TAXAS" , _nTaxas                                   , NIL}) 
			aAdd(_aItem, {"G6I_TAXADU", IIF(_nTaxaDU < 0, _nTaxaDU * -1, _nTaxaDU), NIL})								
			aAdd(_aItem, {"G6I_VLRLIQ", IIF(_nVlrLiq < 0, _nVlrLiq * -1, _nVlrLiq), NIL})

			If _nTpFop == "1"
				If cG6ITipoIt == "1"
					aAdd(_aItem, {"G6I_TPOPER", "1", NIL})
				Else
					aAdd(_aItem, {"G6I_TPOPER", "2", NIL})
				EndIf 	
			Else
				If cG6ITipoIt == "1" .Or. _nVlrLiq == 0
					aAdd(_aItem, {"G6I_TPOPER", "2", NIL})
				Else
					aAdd(_aItem, {"G6I_TPOPER", "1", NIL})
				EndIf
			EndIf

			aAdd(_aItem, {"G6I_TPFOP" , _nTpFop                                   , NIL})
			aAdd(_aItem, {"G6I_TXREE" , 0                                         , NIL})
			aAdd(_aItem, {"G6I_TXCCDU", IIF(_nTxCCDU < 0, _nTxCCDU * -1, _nTxCCDU), NIL})
			aAdd(_aItens, _aItem) 

		// Apenas para taxas Gol
		Else
			_nSinal   := IIF(Val(StrTran(_aLinha[2], ",", ".")) < 0, -1, 1)	
			_nTaxaGol := Val(StrTran(_aLinha[2], ",", ".")) 
			
			If !Empty(_nTaxaGol)
				If cG6ITipoIt == "1" 	//Venda
					_aItens[Len(_aItens)][12, 2] += _nTaxaGol
				Else					//Reembolso
					If Upper(AllTrim(_aLinha[1])) $ 'OCPENALTY|XC|OCNOSHOW'
						_aItens[Len(_aItens)][17, 2] += _nTaxaGol		//G6I_TXREE
					Else
						_aItens[Len(_aItens)][12, 2] += _nTaxaGol		//G6I_TAXAS
					EndIf
				EndIf
			EndIf
		EndIf				
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ImportGol

Grava os dados 

@author    Marcelo Cardoso Barbosa
@version   1.00
@since     11/08/2015
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ImportGol(cMaster, cDetail, aCpoMaster, aCpoDetail)

Local aArea   := GetArea()
Local oModel  := Nil
Local oAux    := Nil
Local oStruct := Nil
Local nI      := 0
Local nJ      := 0
Local nPos    := 0
Local aAux    := {}
Local aC      := {}
Local aH      := {}
Local nItErro := 0
Local lRet    := .T.
Local lAux    := .T.

(cDetail)->(DBSetOrder(1))
(cMaster)->(DBSetOrder(1))

// Aqui ocorre o inst�nciamento do modelo de dados (Model)
// Neste exemplo instanciamos o modelo de dados do fonte COMP022_MVC
// que � a rotina de manuten��o de musicas
oModel := FWLoadModel('TURA038')

// Temos que definir qual a opera��o deseja: 3 � Inclus�o / 4 � Altera��o / 5 - Exclus�o
oModel:SetOperation(MODEL_OPERATION_INSERT)

// Antes de atribuirmos os valores dos campos temos que ativar o modelo
oModel:Activate()

// Instanciamos apenas a parte do modelo referente aos dados de cabe�alho
oAux := oModel:GetModel(cMaster + 'MASTER')

// Obtemos a estrutura de dados do cabe�alho
oStruct := oAux:GetStruct()
aAux    := oStruct:GetFields()

If lRet
	For nI := 1 To Len(aCpoMaster)
		// Verifica se os campos passados existem na estrutura do cabe�alho
		If (nPos := aScan(aAux, {|x| AllTrim(x[3]) == AllTrim(aCpoMaster[nI][1])})) > 0

			// � feita a atribui��o do dado ao campo do Model do cabe�alho
			If !(lAux := oModel:SetValue(cMaster + 'MASTER', aCpoMaster[nI][1], aCpoMaster[nI][2]))
			
				// Caso a atribui��o n�o possa ser feita, por algum motivo (valida��o, por exemplo)
				// o m�todo SetValue retorna .F.
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next nI
	
EndIf

If lRet
	// Instanciamos apenas a parte do modelo referente aos dados do item
	oAux := oModel:GetModel(cDetail + 'DETAIL')
	
	// Obtemos a estrutura de dados do item
	oStruct := oAux:GetStruct()
	aAux := oStruct:GetFields()
	nItErro := 0
	
	For nI := 1 To Len(aCpoDetail)

		// Inclu�mos uma linha nova
		// ATEN��O: Os itens s�o criados em uma estrutura de grid (FORMGRID), 
		//portanto j� � criada uma primeira linha
		//branco automaticamente, desta forma come�amos a inserir novas
		//linhas a partir da 2� vez
		If nI > 1
			// Inclu�mos uma nova linha de item
			If (nItErro := oAux:AddLine()) <> nI
				// Se por algum motivo o m�todo AddLine() n�o consegue incluir a linha,
				// ele retorna a quantidade de linhas j�
				// existem no grid. Se conseguir retorna a quantidade mais 1
				lRet := .F.
				Exit
			EndIf
		EndIf
		
		For nJ := 1 To Len(aCpoDetail[nI])
			// Verifica se os campos passados existem na estrutura de item
			If (nPos := aScan(aAux, {|x| AllTrim(x[3]) == AllTrim(aCpoDetail[nI][nJ][1])})) > 0
				If !(lAux := oModel:SetValue(cDetail + 'DETAIL', aCpoDetail[nI][nJ][1], aCpoDetail[nI][nJ][2]))
					// Caso a atribui��o n�o possa ser feita, por algum motivo (valida��o, por exemplo)
					// o m�todo SetValue retorna .F.
					lRet := .F.
					nItErro := nI
					Exit
				EndIf
			EndIf
		Next nJ
		
		If !lRet
			Exit
		EndIf
	Next nI

EndIf

If lRet
	// Faz-se a valida��o dos dados, note que diferentemente das tradicionais "rotinas autom�ticas"
	// neste momento os dados n�o s�o gravados, s�o somente validados.
	If (lRet := oModel:VldData())
		// Se os dados foram validados faz-se a grava��o efetiva dos
		// dados (commit)
		oModel:CommitData()
		ConfirmSX8()
	EndIf
EndIf

If !lRet
	// Se os dados n�o foram validados obtemos a descri��o do erro para gerar
	// LOG ou mensagem de aviso
	aErro := oModel:GetErrorMessage()
	RollBackSX8()
	
	// A estrutura do vetor com erro �:
	// [1] identificador (ID) do formul�rio de origem
	// [2] identificador (ID) do campo de origem
	// [3] identificador (ID) do formul�rio de erro
	// [4] identificador (ID) do campo de erro
	// [5] identificador (ID) do erro
	// [6] mensagem do erro
	// [7] mensagem da solu��o
	// [8] Valor atribu�do
	// [9] Valor anterior
	
	AutoGrLog("Id do formul�rio de origem:" + " [" + AllToChar(aErro[1]) + "]")
	AutoGrLog("Id do campo de origem: "     + " [" + AllToChar(aErro[2]) + "]")
	AutoGrLog("Id do formul�rio de erro: "  + " [" + AllToChar(aErro[3]) + "]")
	AutoGrLog("Id do campo de erro: "       + " [" + AllToChar(aErro[4]) + "]")
	AutoGrLog("Id do erro: "                + " [" + AllToChar(aErro[5]) + "]")
	AutoGrLog("Mensagem do erro: "          + " [" + AllToChar(aErro[6]) + "]")
	AutoGrLog("Mensagem da solu��o: "       + " [" + AllToChar(aErro[7]) + "]")
	AutoGrLog("Valor atribu�do: "           + " [" + AllToChar(aErro[8]) + "]")
	AutoGrLog("Valor anterior: "            + " [" + AllToChar(aErro[9]) + "]")
	
	If nItErro > 0
		AutoGrLog("Erro no Item: " + " [" + AllTrim( AllToChar( nItErro ) ) + "]")
	EndIf
	
	MostraErro()
EndIf

// Desativamos o Model
oModel:DeActivate()

Return lRet