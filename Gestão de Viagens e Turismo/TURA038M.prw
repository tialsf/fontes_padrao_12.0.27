#INCLUDE "PROTHEUS.CH"
#INCLUDE "TURA038M.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWEDITPANEL.CH"

Static oTblHeader := Nil //Tabela tempor�ria com os campos referente a pesquisa
Static oTblDetail := NIl //Tabela tempor�ria com os campos referente aos DR's encontrados
Static nLinMark   := 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef

Define o modelo de dados TURA038M, utilizado na pesquisa de metas

@type static function

@param	
@return oModel:	Objeto. Objeto da Classe MPFormModel

@example
oModel := ModelDef()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references)
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel	 := MPFormModel():New("TURA038M", /*bPreValidacao*/, /*bPosValid*/, {|oModel| TA038MCommit(oModel)}, {|oModel| FWFormCancel(oModel)})
Local oStructCab := FWFormModelStruct():New()
Local oStructDet := FWFormModelStruct():New()

TA038MCrTb()

oStructCab:AddTable(oTblHeader:GetAlias(), , STR0001, {|| oTblHeader:GetRealName()})	//"Dados compara��o"
oStructDet:AddTable(oTblDetail:GetAlias(), , STR0002, {|| oTblDetail:GetRealName()})	//"Metas"

AddFldCab(oStructCab, 1)
AddFldDet(oStructDet, 1)

oModel:AddFields("TMP_MASTER", /*cOwner*/, oStructCab)
oModel:GetModel("TMP_MASTER"):SetOnlyQuery(.T.)

oModel:AddGrid("TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:GetModel("TMP_DETAIL"):SetOnlyQuery(.T.)
oModel:SetRelation("TMP_DETAIL", {}, (oTblDetail:GetAlias())->(IndexKey(1)))

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

oModel:SetPrimaryKey({})
oModel:SetDeActivate({|| TA038MDestroy()})
	
Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef

Define a view TURA038M, utilizado na pesquisa de metas

@type static function

@param	
@return oView:	Objeto. Objeto da Classe FwFormView

@example
oView := ViewDef()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel   	 := FWLoadModel("TURA038M")
Local oView		 := FWFormView():New()
Local oStructCab := FWFormViewStruct():New()
Local oStructDet := FWFormViewStruct():New()

oView:SetModel( oModel )

AddFldCab(oStructCab, 2)
AddFldDet(oStructDet, 2)

oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER")
oView:AddGrid("VIEW_TMPD" , oStructDet, "TMP_DETAIL")
oView:EnableTitleView("VIEW_TMPD")

oView:AddUserButton(STR0011 + "-F5", "CLIPS", {|oView| FwMsgRun( , {|| TA038MQry(oView)}, , STR0012)}, , VK_F5)		//"Pesquisar"##"Pesquisando documentos..."

oView:CreateHorizontalBox("SUPERIOR", 30)
oView:CreateHorizontalBox("INFERIOR", 70)

oView:SetOwnerView("VIEW_TMPM", "SUPERIOR")
oView:SetOwnerView("VIEW_TMPD", "INFERIOR")

oView:SetFieldAction("TMP_OK", {|oView| oView:Refresh()})

oView:SetCloseOnOk({|| .T.})
oView:ShowUpdateMsg(.F.)
oView:ShowInsertMsg(.F.)

oView:SetAfterViewActivate({|oView| TA038MQry(oView, .T.), oView:Refresh()})

Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MQry

Fun��o que executa a query que busca as apura��es de metas para serem listadas em grid na tela de pesquisa de metas.

@type static function

@param	oView: Objeto. Objeto da Classe FWFormView	
@return nil

@example
TA038MQry(oView)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038MQry(oView, lInit)

Local aArea        := GetArea()
Local oModel	   := oView:GetModel()
Local oModelMaster := oView:GetModel("TMP_MASTER")
Local oModelDetail := oView:GetModel("TMP_DETAIL")
Local cAliasG6L	   := GetNextAlias()
Local cWhere	   := ""
Local lNewLine	   := .F.
Local nX		   := 0
Local aApura	   := {}
Local bScan		   := {|x| Alltrim(x[1] + x[2] + x[3]) == Alltrim((cAliasG6L)->(G6L_FILIAL + G6L_CODAPU + G6M_TIPOAC)) .And. x[4] == (cAliasG6L)->G6M_TOTAL .And. x[5] == (cAliasG6L)->G6L_DTGERA}
Default lInit	   := .F.

If !lInit
	If Empty(oModelMaster:GetValue("TMP_APUATE"))
		FwAlertHelp(STR0007, STR0008, STR0009)	//"N�o h� dados suficientes para efetuar a pesquisa."#"Informe ao menos os par�metros Apura��o de e Apura��o at�, antes de efetuar a consulta."#"Pesquisa de Metas"
		Return()
	EndIf
EndIf

oModelDetail:SetNoInsertLine(.F.)
oModelDetail:SetNoDeleteLine(.F.)
oModelDetail:ClearData()

If !lInit

	cWhere := "%"
	If !Empty(oModelMaster:GetValue("TMP_APUDE"))
		cWhere += " AND G6L_CODAPU >= '" + oModelMaster:GetValue("TMP_APUDE") + "'" 
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_APUATE"))
		cWhere += " AND G6L_CODAPU <= '" + oModelMaster:GetValue("TMP_APUATE") + "'" 
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_DATADE"))
		cWhere += " AND G6L_DTGERA >= '" + DtoS(oModelMaster:GetValue("TMP_DATADE")) + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_DATATE"))
		cWhere += " AND G6L_DTGERA <='" + DtoS(oModelMaster:GetValue("TMP_DATATE")) + "'"
	EndIf
	cWhere += "%"

	BeginSql Alias cAliasG6L
		
		Column G6L_DTGERA AS DATE
		Column G6L_DTINI  AS DATE
		Column G6L_DTFIM  AS DATE
		
		//Query que traz as apura��es de metas que N�O possuem fatura G81, mas que a apura��o n�o foi associada
		SELECT '2' FATURADO, G6L_FILIAL, G6L_CODAPU, G6L_DTGERA, G6L_DTINI, G6L_DTFIM, G6M_SEGNEG, G6M_TIPOAC, G6M_VLACD, G6M_PERDES, G6M_VLDESC, G6M_PERCTX, G6M_VLTXAD, G6M_TOTAL, G81_IDIFA
		FROM %Table:G6L% G6L
		INNER JOIN %Table:G6M% G6M ON G6M_FILIAL = G6L_FILIAL AND G6M_CODAPU = G6L_CODAPU AND G6M_CONCIL = '' AND G6M.%NotDel%
		LEFT JOIN  %Table:G81% G81 ON G81_FILIAL = G6M_FILIAL AND G81_CODAPU = G6M_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS <> '2' AND G81.%NotDel%	
		WHERE G6L_FILIAL = %XFilial:G6L% 
		     %Exp:cWhere% AND 
		     G6L_TPAPUR = '2' AND 
		     G6L_FORNEC = %Exp:G6H->G6H_FORNEC% AND 
		     G6L_LJFORN = %Exp:G6H->G6H_LOJA% AND 
		     NOT EXISTS (SELECT 1 
		                 FROM %Table:G81% G81 
		                 WHERE G81_FILIAL = G6L_FILIAL AND G81_CODAPU = G6L_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS = '2' AND G81.%NotDel%) AND 
		     G6L.%NotDel%
		
		UNION

		//Query que traz as apura��es de metas que possuem n�mero de fatura mas N�O foram Associadas
		SELECT '1' FATURADO, G6L_FILIAL, G6L_CODAPU, G6L_DTGERA, G6L_DTINI, G6L_DTFIM, G6M_SEGNEG, G6M_TIPOAC, G6M_VLACD, G6M_PERDES, G6M_VLDESC, G6M_PERCTX, G6M_VLTXAD, G6M_TOTAL, G81_IDIFA 
		FROM %Table:G6L% G6L
		INNER JOIN %Table:G6M% G6M ON G6M_FILIAL = G6L_FILIAL AND G6M_CODAPU = G6L_CODAPU AND G6M_FATMET = %Exp:G6H->G6H_FATURA% AND G6M_CONCIL = '' AND G6M.%NotDel%
		LEFT JOIN  %Table:G81% G81 ON G81_FILIAL = G6M_FILIAL AND G81_CODAPU = G6M_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS <> '2' AND G81.%NotDel%	
		WHERE G6L_FILIAL = %XFilial:G6L% AND 
		      G6L_TPAPUR = '2' AND 
		      G6L_FORNEC = %Exp:G6H->G6H_FORNEC% 
		      AND G6L_LJFORN = %Exp:G6H->G6H_LOJA% AND 
		      NOT EXISTS (SELECT 1 
		                  FROM %Table:G81% G81 
		                  WHERE G81_FILIAL = G6L_FILIAL AND G81_CODAPU = G6L_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS = '2' AND G81.%NotDel%) AND 
		      G6L.%NotDel%
		ORDER BY FATURADO, G6L_FILIAL, G6L_CODAPU, G6L_DTGERA
	EndSQL
Else
	BeginSQL Alias cAliasG6L
		Column G6L_DTGERA AS DATE
		Column G6L_DTINI  AS DATE
		Column G6L_DTFIM  AS DATE
		
		//Query que traz as apura��es de metas que possuem n�mero de fatura mas N�O foram Associadas
		SELECT '1' FATURADO, G6L_FILIAL, G6L_CODAPU, G6L_DTGERA, G6L_DTINI, G6L_DTFIM, G6M_SEGNEG, G6M_TIPOAC, G6M_VLACD, G6M_PERDES, G6M_VLDESC, G6M_PERCTX, G6M_VLTXAD, G6M_TOTAL, G81_IDIFA
		FROM %Table:G6L% G6L
		INNER JOIN %Table:G6M% G6M ON G6M_FILIAL = G6L_FILIAL AND G6M_CODAPU = G6L_CODAPU AND G6M_FATMET = %Exp:G6H->G6H_FATURA% AND G6M_CONCIL = '' AND G6M.%NotDel%
		LEFT JOIN  %Table:G81% G81 ON G81_FILIAL = G6M_FILIAL AND G81_CODAPU = G6M_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS <> '2' AND G81.%NotDel%	
		WHERE G6L_FILIAL = %XFilial:G6L% AND 
		      G6L_TPAPUR = '2' AND 
		      G6L_FORNEC = %Exp:G6H->G6H_FORNEC% AND 
		      G6L_LJFORN = %Exp:G6H->G6H_LOJA% AND 
		      NOT EXISTS (SELECT 1 
		                  FROM %Table:G81% G81 
		                  WHERE G81_FILIAL = G6L_FILIAL AND G81_CODAPU = G6L_CODAPU AND G81_SEGNEG = G6M_SEGNEG AND G81_STATUS = '2' AND G81.%NotDel%) AND 
		      G6L.%NotDel%
		ORDER BY FATURADO, G6L_FILIAL, G6L_CODAPU, G6L_DTGERA
	EndSQL
EndIf

While (cAliasG6L)->(!Eof())
	If Len(aApura) == 0 .Or. aScan(aApura, bScan) == 0
		aAdd(aApura, {G6L_FILIAL, G6L_CODAPU, G6M_TIPOAC, G6M_TOTAL, G6L_DTGERA})		
		
		If lNewLine
			oModelDetail:AddLine()
		EndIf
	
		For nX := 1 to (cAliasG6L)->(FCount())
			If ((cAliasG6L)->(FieldName(nX)) <> "FATURADO")		
				oModelDetail:SetValue((cAliasG6L)->(FieldName(nX)), (cAliasG6L)->(FieldGet(nX)))
			EndIf	
		Next nX
		
		oModelDetail:LoadValue("G6M_TPDESC", Posicione("G8B", 1, xFilial("G8B") + (cAliasG6L)->G6M_TIPOAC, "G8B_DESCRI"))
		lNewLine := .T.
	EndIf
	(cAliasG6L)->(DbSkip())
EndDo
(cAliasG6L)->(DbCloseArea())

oModelDetail:GoLine(1)
oModelDetail:SetNoInsertLine(.T.)
oModelDetail:SetNoDeleteLine(.T.)

RestArea(aArea)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MCrTb

Fun��o que cria as tabelas tempor�rias para composi��o da tela de pesquisa. S�o criadas tabela para o cabe�alho e para o item

@type static function

@param		
@return nil

@example
TA038MCrTb()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038MCrTb()

Local nX	      := 0
Local aStruHeader := {}
Local aStruDetail := {}
Local aMetaFld	  := {"G6L_FILIAL", "G6L_CODAPU", "G6L_DTGERA", "G6L_DTINI", "G6L_DTFIM", "G6M_SEGNEG", "G6M_TIPOAC", "G6M_TPDESC", "G6M_VLACD", "G6M_PERDES", "G6M_VLDESC", "G6M_PERCTX", "G6M_VLTXAD", "G6M_TOTAL", "G81_IDIFA"}

//Estrutura do Cabe�alho
aAdd(aStruHeader, {"TMP_APUDE" , "C", TamSx3("G6L_CODAPU")[1],0})
aAdd(aStruHeader, {"TMP_APUATE", "C", TamSx3("G6L_CODAPU")[1],0})
aAdd(aStruHeader, {"TMP_DATADE", "D", 08,0})
aAdd(aStruHeader, {"TMP_DATATE", "D", 08,0})	

oTblHeader := FWTemporaryTable():New()
oTblHeader:SetFields(aStruHeader)
oTblHeader:AddIndex("index1", {"TMP_APUDE", "TMP_APUATE"})
oTblHeader:Create()

aAdd(aStruDetail, {"TMP_OK", "L", 1, 0})

For nX := 1 To Len(aMetaFld)
	aAdd(aStruDetail,{AllTrim(aMetaFld[nX]), TamSX3(aMetaFld[nX])[3], TamSX3(aMetaFld[nX])[1], TamSX3(aMetaFld[nX])[2]})
Next nX

oTblDetail := FWTemporaryTable():New()
oTblDetail:SetFields(aStruDetail)
oTblDetail:AddIndex("index1", {"G6L_CODAPU", "G6L_DTGERA", "G6M_SEGNEG", "G6M_TIPOAC", "G81_IDIFA"})
oTblDetail:Create()

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldCab

Adiciona os campos do cabe�alho.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldCab( oStruct, nOpc )

If ( nOpc == 1 )
	oStruct:AddField(STR0003, ; 					// [01] C Titulo do campo	//"Apura��o de"
					 STR0003, ;		 				// [02] C ToolTip do campo	//"Apura��o de"
					 "TMP_APUDE", ;		 			// [03] C identificador (ID) do Field
					 "C", ; 						// [04] C Tipo do campo
					 TamSx3("G6L_CODAPU")[1], ;		// [05] N Tamanho do campo
					 0, ; 							// [06] N Decimal do campo
					 {|| .T.}, ; 					// [07] B Code-block de valida��o do campo
					 NIL, ; 						// [08] B Code-block de valida��o When do campo
					 {}, ; 							// [09] A Lista de valores permitido do campo
					 .F., ; 						// [10] L Indica se o campo tem preenchimento obrigat�rio
					 Nil, ; 						// [11] B Code-block de inicializacao do campo
					 NIL, ; 						// [12] L Indica se trata de um campo chave
					 NIL, ; 						// [13] L Indica se o campo pode receber valor em uma opera��o de update.
					 .T.) 				   			// [14] L Indica se o campo � virtual	

	oStruct:AddField(STR0004, STR0004, "TMP_APUATE", "C", TamSx3("G6L_CODAPU")[1], 0, {|| .T.}, NIL, {}, .F., Nil, NIL, NIL, .T.)		// "Apura��o at�"
	oStruct:AddField(STR0005, STR0005, "TMP_DATADE", "D", TamSx3("G6L_DTGERA")[1], 0, {|| .T.}, NIL, {}, .F., Nil, NIL, NIL, .T.) 		// "Data de"  
	oStruct:AddField(STR0006, STR0006, "TMP_DATATE", "D", TamSx3("G6L_DTGERA")[1], 0, {|| .T.}, NIL, {}, .F., Nil, NIL, NIL, .T.)   	//"Data at�"
Else
	oStruct:AddField("TMP_APUDE", ; 		// [01] C Nome do Campo
					 "01", ; 				// [02] C Ordem
					 STR0003, ; 			// [03] C Titulo do campo		//"Apura��o de"
					 STR0003, ; 			// [04] C Descri��o do campo	//"Apura��o de"
					 {}, ; 					// [05] A Array com Help
					 "GET", ; 				// [06] C Tipo do campo
					 "@!", ; 				// [07] C Picture
					 NIL, ; 				// [08] B Bloco de Picture Var
					 "G6LMET", ;			// [09] C Consulta F3
					 .T., ; 				// [10] L Indica se o campo � edit�vel
					 NIL, ; 				// [11] C Pasta do campo
					 NIL, ; 				// [12] C Agrupamento do campo
					 {}, ; 					// [13] A Lista de valores permitido do campo (Combo)
					 NIL, ; 				// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL, ; 				// [15] C Inicializador de Browse
					 .T., ; 				// [16] L Indica se o campo � virtual
					 NIL ) 					// [17] C Picture Vari�vel

	oStruct:AddField("TMP_APUATE", "02", STR0004, STR0004, {}, "GET", "@!", Nil, "G6LMET", .T., Nil, Nil, {}, Nil, Nil, .T., Nil)		//"Apura��o ate"  
	oStruct:AddField("TMP_DATADE", "03", STR0005, STR0005, {}, "GET", ""  , Nil, ""      , .T., Nil, Nil, {}, Nil, Nil, .T., Nil)		//"Data de"		//"Data at�"
	oStruct:AddField("TMP_DATATE", "04", STR0006, STR0006, {}, "GET", ""  , Nil, ""      , .T., Nil, Nil, {}, Nil, Nil, .T., Nil)
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldDet

Adiciona as colunas da grid.

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldDet(oStructDet, nOpc)
	
Local aStruHis	:= (oTblDetail:GetAlias())->(DbStruct())
Local nX		:= 0
Local bVldLine	:= Nil

If FwIsInCallStack("Ta38GIt")
	bVldLine := Nil
Else
	bVldLine := {|oMdl, cCampo, xValueNew, nLine, xValueOld| vldMark(oMdl, cCampo, xValueNew, nLine, xValueOld)}
Endif 


//Adiciona o campo OK
If nOpc == 1
	oStructDet:AddField("", ; 			// [01] C Titulo do campo
						"", ; 			// [02] C ToolTip do campo
						"TMP_OK", ; 	// [03] C identificador (ID) do Field
						"L", ; 			// [04] C Tipo do campo
						1, ;   			// [05] N Tamanho do campo
						0, ;   			// [06] N Decimal do campo
						bVldLine, ; 	// [07] B Code-block de valida��o do campo
						NIL, ; 			// [08] B Code-block de valida��o When do campoz
						NIL, ; 			// [09] A Lista de valores permitido do campo
						NIL, ; 			// [10] L Indica se o campo tem preenchimento obrigat�rio
						NIL, ; 			// [11] B Code-block de inicializacao do campo
						NIL, ; 			// [12] L Indica se trata de um campo chave
						.T., ; 			// [13] L Indica se o campo pode receber valor em uma opera��o de update.
						.F.) 			// [14] L Indica se o campo � virtual
Else
	oStructDet:AddField("TMP_OK", ; 	// [01] C Nome do Campo
						"00", ; 		// [02] C Ordem
						"", ;   		// [03] C Titulo do campo
						"", ;   		// [04] C Descri��o do campo
						NIL, ;  		// [05] A Array com Help
						"CHECK", ;  	// [06] C Tipo do campo
						"@!", ; 		// [07] C Picture
						NIL, ;  		// [08] B Bloco de Picture Var
						"", ;   		// [09] C Consulta F3
						.T., ;  		// [10] L Indica se o campo � edit�vel
						NIL, ;  		// [11] C Pasta do campo
						NIL, ;  		// [12] C Agrupamento do campo
						NIL, ;  		// [13] A Lista de valores permitido do campo (Combo)
						NIL, ;  		// [14] N Tamanho M�ximo da maior op��o do combo
						NIL, ;  		// [15] C Inicializador de Browse
						.F., ;  		// [16] L Indica se o campo � virtual
						NIL) 			// [17] C Picture Vari�vel
EndIf


SX3->(DbSetOrder(2))	// X3_CAMPO
For nX := 1 To Len(aStruHis)
	If SX3->(DbSeek(aStruHis[nX][1]))
		If nOpc == 1
			oStructDet:AddField(GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), "", AllTrim(aStruHis[nX][1]), TamSX3(aDRFields[nX])[3], TamSX3(aDRFields[nX])[1], TamSX3(aDRFields[nX])[2], Nil, Nil, Nil, Nil, Nil, Nil, .F., .F.)
		ElseIf nOpc == 2
			oStructDet:AddField(AllTrim(aStruHis[nX][1]), StrZero(nX, 2), GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), GetSX3Cache(aStruHis[nX][1], "X3_DESCRIC"), Nil, TamSX3(aDRFields[nX])[3], X3Picture(aDRFields[nX][1]), Nil, "", .F., Nil, Nil, Nil, Nil, Nil, .F., Nil)
		EndIf
	EndIf
Next nX

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} VldMark

Permite a marca��o de apenas um item no mark.

@type Static Function
@author Enaldo Cardoso Junior
@since 14/03/2016
@version 12.1.7
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function VldMark(oMdlBase, cCampo, xValueNew, nLine, xValueOld)

Local nLinAtual := 0
Local lRet      := !Empty(oMdlBase:GetValue("G6L_CODAPU"))

If lRet .And. xValueNew
	nLinAtual := oMdlBase:GetLine()
	If nLinAtual # nLinMark
		If nLinMark # 0
			oMdlBase:GoLine(nLinMark)
			oMdlBase:SetValue("TMP_OK", .F.) 	// desmarca o item anterior
		EndIf
		oMdlBase:GoLine(nLinAtual)  			// retorna ao item posicionado antes
	EndIf
	nLinMark := nLinAtual
Else
	nLinMark := 0
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MCommit
Fun��o que efetua a associa��o da meta com o item da fatura a�rea.

@type static function

@param	oModelPesq: Objeto. Objeto da Classe FwFormModel	
@return .T.

@example
TA038MCommit(oModelPesq )

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038MCommit(oModelPesq)
	
Local oTur038Mdl     := Tur038Modelo()[1]
Local oMdlPesqResult := oModelPesq:GetModel("TMP_DETAIL")
Local oModelConc	 := Nil
Local oMdlG6I        := Nil
Local oMdlG6J        := Nil
Local nLine			 := 0
Local lRet       	 := .T.
		
BEGIN TRANSACTION
	oModelConc := FwLoadModel("TURA038D")
	oModelConc:SetOperation(MODEL_OPERATION_UPDATE)
	oModelConc:Activate()
	
	oMdlG6I := oModelConc:GetModel("G6I_ITENS")
	oMdlG6J := oModelConc:GetModel("G6J_ITENS")
	For nLine := 1 To oMdlPesqResult:Length()
		oMdlPesqResult:GoLine(nLine)
		If oMdlPesqResult:GetValue("TMP_OK") .And. !oMdlPesqResult:IsDeleted()
			//Pesquisa se j� existe uma concilia��o aberta para este posto ou cria uma nova
			If T38AddConc(oModelConc, "M", " ", " ", "2")
				If oMdlG6I:SeekLine({{"G6I_ITEM", oTur038Mdl:GetValue("G6IDETAIL", "G6I_ITEM")}})
	
					If (lRet := TA38MUpdItem(oModelConc, oMdlPesqResult:GetValue("G81_IDIFA")))		//Cria o vinculo da G6I com G3R
						If (lRet := TA38MUpdMeta(oModelConc, oMdlPesqResult:GetValue("G6L_FILIAL"), oMdlPesqResult:GetValue("G6L_CODAPU"), oMdlPesqResult:GetValue("G6M_SEGNEG")))
							If oMdlG6I:GetValue("G6I_META") == "1"						
								If oMdlG6J:SeekLine({{"G6J_CONCIL", oMdlG6I:GetValue("G6I_CONCIL")}})							
									lRet := oMdlG6J:SetValue("G6J_TIPO", "3")		//Metas
								EndIf	
							EndIf
						EndIf
					EndIf
				Else
					lRet := .F.
				EndIf
			Else
				lRet := .F.
			EndIf	
		EndIf
		
		If !lRet
			Exit
		EndIf	
	Next nLine
	
	If lRet .And. oModelConc:VldData()
		oModelConc:CommitData()	
	Else
		lRet := .F.
	EndIf

	If lRet
		FwMsgRun( , {|| lRet := T38AtuSt(oModelConc:GetValue("G6H_MASTER", "G6H_FATURA"))}, , STR0010)		//"Atualizando status"
	Endif
	
	oModelConc:DeActivate()
	oModelConc:Destroy()	

	If !lRet
		DisarmTransaction()
		Break
	EndIf	
END TRANSACTION

FwModelActive(oModelPesq)

oMdlG6I := Nil
oMdlG6J := Nil

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA38MUpdItem
Fun��o que atualiza o item da fatura a�rea, com as informa��es da concilia��o

@type static function

@param	oModel: Objeto. Objeto da Classe FwFormModel
		cIdIFA: Caractere. Identificador do Item Financeiro de Apura��o	
@return lRet:	L�gico. .T. se executou a atualiza��o com sucesso

@example
lRet := TA38MUpdItem(oModel,cIdIFA)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA38MUpdItem(oModel, cIdIFA)

Local oMdlG6I := oModel:GetModel("G6I_ITENS")
Local oMdlG6J := oModel:GetModel("G6J_ITENS")
Local lRet	  := .F.

lRet := oMdlG6I:SetValue("G6I_CONCIL", oMdlG6J:GetValue("G6J_CONCIL")) .And.;
		oMdlG6I:SetValue("G6I_ASSAUT", "2")	.And.;	//1-Sim;2-N�o
		oMdlG6I:SetValue("G6I_SITUAC", "2")	//2 - Associado
		oMdlG6I:SetValue("G6I_IDIFA", cIdIFA)

G81->(ConfirmSX8())

oMdlG6I := Nil
oMdlG6J := Nil

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA38MUpdMeta

Fun��o que atualiza as apura��es de metas que possuem a concilia��o e fatura definidas no modelo pelo item da fatura.

@type static function

@param	oModel: 	Objeto. Objeto da Classe FwFormModel
		cFilApu:	Caractere. Filial da Apura��o da metas
		cCodApu:	Caractere. C�digo da Apura��o da metas
		cSegNeg: 	Caractere. Segmento de Neg�cio da Apura��o de metas	
@return lRet:		L�gico. .T. se executou a atualiza��o com sucesso

@example
lRet := TA38MUpdMeta(oModel,cFilApu,cCodApu,cSegNeg)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA38MUpdMeta(oModel, cFilApu, cCodApu, cSegNeg)

Local oMdlG6I := oModel:GetModel("G6I_ITENS")
Local oMdlG6J := oModel:GetModel("G6J_ITENS")
Local cSts    := ""

cSts := "UPDATE " + RetSQLName("G6M") + chr(13)
cSts += "SET " + chr(13)
cSts += "	G6M_FILCON = '" + oMdlG6I:GetValue("G6I_FILIAL") + "', " + chr(13)
cSts += "	G6M_CONCIL = '" + oMdlG6J:GetValue("G6J_CONCIL") + "', " + chr(13)
cSts += "	G6M_FATMET = '" + oMdlG6I:GetValue("G6I_FATURA") + "', " + chr(13)
cSts += "	G6M_ITFAT = '"  + oMdlG6I:GetValue("G6I_ITEM")   + "'  " + chr(13)
cSts += "WHERE " + chr(13)
cSts += "	G6M_FILIAL = '" + cFilApu + "' AND " + chr(13)
cSts += "	G6M_CODAPU = '" + cCodApu + "' AND " + chr(13)
cSts += "	G6M_SEGNEG = '" + cSegNeg + "' AND " + chr(13)
cSts += IIF(TCSrvType() == "AS/400", "	@DELETED@ = ' '", " D_E_L_E_T_ = ' '")

Return TCSQLExec(cSts) >= 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA038MDestroy

Destroi os objetos.

@author Enaldo Cardoso Junior
@since 12/02/2015
@version 1.0
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function TA038MDestroy()

//� necess�rio verificar se a chamada veio do bot�o 'Salvar' da tela apresentada no bot�o cancelar
//fechando as tabelas tempor�rias somente na �ltima chamada
If !FwIsInCallStack('FwAlertExitPage')
	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType(oTblHeader) == "O"
		oTblHeader:Delete()
		FreeObj(oTblHeader)
	EndIf

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType(oTblDetail) == "O"
		oTblDetail:Delete()
		FreeObj(oTblDetail)
	EndIf
EndIf

Return .T.