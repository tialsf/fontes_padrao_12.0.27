#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039.CH" 

STATIC cIdItem := ""

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039
Concilia��o A�rea

@type Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TURA039()

Local oBrowse := Nil
Local aLegApr := {}
Local cTitulo := ""
	
oBrowse := FWMBrowse():New()
oBrowse:SetAlias("G6J")

If FWIsInCallStack("TURA039M")
	cTitulo := STR0114	//"Concilia��o de Metas"
	oBrowse:SetFilterDefault("G6J_TIPO == '3'")
Else
	cTitulo := STR0026	//"Concilia��o A�rea"	
	oBrowse:SetFilterDefault("G6J_TIPO <> '3'")

	SetKey (VK_F11, {|a, b| AcessaPerg("TURA041", .T.)})
EndIf

oBrowse:SetDescription(cTitulo)	//"Concilia��o A�rea"

oBrowse:AddLegend("G6J_DIVERG == '1'", "BR_VERMELHO", STR0044)	//"Divergente"
oBrowse:AddLegend("G6J_DIVERG == '2'", "BR_VERDE"   , STR0045)	//"N�o Divergente"

aLegApr := {"", {|| TurLApr()}, "C", "@BMP", 0, 1, 0, .F., {||.T.}, .T., {|| TurAprCor()}, , , , .F.}

oBrowse:AddColumn(aLegApr)
oBrowse:Activate()

Set Key VK_F11 To

Return Nil

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039
Op��es de menu

@type Static Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0001 ACTION "Tura39Conc(1)" 	OPERATION 2 ACCESS 0 //"Visualizar"
ADD OPTION aRotina TITLE STR0002 ACTION "Tura39Conc(2)"     OPERATION 4 ACCESS 0 //"Alterar"
ADD OPTION aRotina TITLE STR0003 ACTION "TA039MnuOpc(5)" 	OPERATION 5 ACCESS 0 //"Excluir"
ADD OPTION aRotina TITLE STR0033 ACTION "VIEWDEF.TURA039A" 	OPERATION 2 ACCESS 0 //"Vis�o Financeira"
ADD OPTION aRotina Title STR0084 ACTION "TA039Dem(1, .T.)"	OPERATION 4 ACCESS 0 //"Dem. Financ. Agencia"
ADD OPTION aRotina Title STR0085 ACTION "TA039Dem(2, .T.)"	OPERATION 4 ACCESS 0 //"Dem. Financ. Cliente"
	
Return aRotina

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039MnuOpc
Fun��o Auxiliar para chamar a execu��o da View. Esta fun��o � executada pelo menu da rotina

@type Function
@author Fernando Radu Muscalu
@since 28/07/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TA039MnuOpc(nOpc)

Local cOperacao	:= ""

Do Case
	Case nOpc == 1
		cOperacao := STR0001	//"Visualizar"
	Case nOpc == 5
		cOperacao := STR0003	//"Excluir"
End Case

If FwIsInCallStack("TURA039M")    //Concilia��o de Metas
	FWExecView(cOperacao, "TURA039M", nOpc, , {|| .T.})			//"Alterar" 
Else
	FWExecView(cOperacao, "TURA039MDL", nOpc, , {|| .T.})		//"Alterar"
EndIf

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039
Remove os bot�es desnecess�rio, tamb�m o bot�o confirmar pois as op��es no bot�o outras a��es realizam o commit.

@type Static Function
@author Enaldo Cardoso Junior
@since 17/03/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function Tur039Alt()

Local aButtons := {{.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.T., STR0004}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}}			//"Fechar"

If FwIsInCallStack("TURA039M") //Concilia��o de Metas
	FWExecView(STR0027, "TURA039M", MODEL_OPERATION_UPDATE, , {|| .T.}, , , aButtons)	//"Alterar"
Else	
	FWExecView(STR0027, "TURA039" , MODEL_OPERATION_UPDATE, , {|| .T.}, , , aButtons)	//"Alterar"
EndIf	

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur039Leg
Define as cores da legenda dos itens da fatura.

@author    Enaldo Cardoso Junior
@version   1.00
@since     15/03/2016
/*/
//------------------------------------------------------------------------------------------
Function Tur039Leg()

Local oLegenda := FwLegend():New()

oLegenda:Add("", "BR_VERMELHO", STR0018) 	//"N�o Associado"
oLegenda:Add("", "BR_AMARELO" , STR0019) 	//"Associado"
oLegenda:Add("", "BR_VERDE"   , STR0020) 	//"Aprovado"
oLegenda:Add("", "BR_LARANJA" , STR0164) 	//"Pendente Aval." 

oLegenda:View()
DelClassIntf()

Return .T.

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur039Des
Rotina de realiza a dessasocia��o chamada no bot�o dessasocia��o.

@author Enaldo Cardoso Junior
@since 12/02/2015
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function Tur039Des(lForce)

Local oModel	  := FwModelActive()
Local oModelG6I   := oModel:GetModel("G6I_ITENS")
Local aFilter	  := {}
Local lRet		  := .T.
Local lEstFOP	  := .F.
Local lAssocMetas := FwIsInCallStack("TURA039M")

Default lForce 	  := .F.

If !lForce
	If !oModelG6I:SeekLine({{"TMP_OK", .T.}})
		MsgInfo(STR0034)	//"Selecione algum item da fatura para realizar a dessasocia��o."
		Return .F.
	ElseIf !oModelG6I:SeekLine({{"TMP_OK", .F.}})
		If !lForce
			If oModelG6I:Length() == 1
				MsgInfo(STR0021)		//"Existe apenas um item da fatura associado ao documento de reserva, caso realmente deseje realizar a dessasocia��o dever� ser realizada a exclus�o do registro de concilia��o a�rea."
			Else
				MsgInfo(STR0022)		//"N�o � permetido dessasociar todos os itens de uma �nica vez."
			EndIf
			Return .F.
		EndIf	
	EndIf
EndIf

BEGIN TRANSACTION
	If lForce
		If T39LibIfEn(oModel, .T.)
			FwAlertHelp(I18N(STR0115, {oModelG6I:GetValue("G6I_ITEM")}), STR0116, "TUR039DES") //"Itens liberados do item #1[item fatura]# j� foram apurados ou finalizados."###"Estorne o faturamento ou apura��o antes de prosseguir."
		Else
			aAdd(aFilter, oModelG6I:GetValue("G6I_ITEM"))
		EndIf
	Else
		//Verifica se foi selecionado algum item da fatura para realizar dessasocia��o.
		While oModelG6I:SeekLine({{"TMP_OK", .T.}})
			If T39HasAltFOP(oModel) 
				If FwAlertYesNo(I18N(STR0142, {oModelG6I:GetValue("G6I_ITEM")}), STR0030) 	//"O item da fatura #1[item fatura]# possui acerto de FOP, deseja estornar o acerto?"###"Aten��o"
					FWMsgRun( , {|| lEstFOP := TA39JCAlFO(.T., .F. , .T.)}, , STR0101) 		//"Aguarde, marcando itens..."						
					If lEstFOP
						aAdd(aFilter, oModelG6I:GetValue("G6I_ITEM"))
					EndIf	
				EndIf

			ElseIf T39LibIfEn(oModel, .T.)
				lRet := .F.
				FwAlertHelp(I18N(STR0115, {oModelG6I:GetValue("G6I_ITEM")}), STR0116, "TUR039DES") //"Itens liberados do item #1[item fatura]# j� foram apurados ou finalizados."###"Estorne o faturamento ou apura��o antes de prosseguir."
			Else
				aAdd(aFilter, oModelG6I:GetValue("G6I_ITEM"))
			EndIf
			oModelG6I:LoadValue("TMP_OK", .F.)
		EndDo
	EndIf
	
	If lRet
		//Realiza a dessasocia��o.
		If len(aFilter) > 0
			//N�o � associa��o de metas
			If !lAssocMetas				
				If Tur39DelAss(aFilter)
					lRet := .T.
				Else
					MsgAlert(STR0156) //'Erro ao desassociar o item.'
					lRet := .F. 
				EndIf
			Else 
				lRet := TA39MDesMetas(oModel)		//efetua a desassocia��o de metas	
			EndIf
		
			If !lForce
				If lRet
					If !T039AprSt(oModel)
						MsgAlert(STR0155) //'Erro ao atualizar status.'
						lRet := .F.
					EndIf
				EndIf
			EndIf
		Else
			MsgInfo(STR0023)	//"Selecione algum item da fatura para realizar a dessasocia��o."
		EndIf
	Else
		DisarmTransaction()
		Break
	EndIf
END TRANSACTION

FwModelActive(oModel)
	
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur039Apr
Rotina de realiza a aprova��o do item da fatura.

@author Anderson Toledo
@since 07/08/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function Tur039Apr(lForce)

Local oModel	 := FwModelActive()
Local oModelG6I  := oModel:GetModel("G6I_ITENS")
Local oMdl039H	 := T39IniMdEr( oModelG6I:GetValue("G6I_CONCIL"))
Local aSaveLines := FwSaveRows()
Local aEstorna	 := {}
Local aAprova	 :=	{}
Local lAssocMetas:= FwIsInCallStack("TURA039M")
Local lMsgError	 := .F.
Local lRet		 := .T.
Local nX		 := 0
Local cError 	 := ""

Default lForce	:= .F.

FwModelActive(oModel)

//Valida��es para associa��o de metas
If lAssocMetas
	lRet := TA39MVldMetas(oModelG6I)
EndIf

If lRet
	BEGIN TRANSACTION
		While oModelG6I:SeekLine({{"TMP_OK", .T.}})		//Verifica se foi selecionado algum item da fatura para realizar dessasocia��o.
			lRet   := .T.
			cError := ""
			If oModelG6I:GetValue("G6I_SITUAC") == "2"	// Valida��o para aprova��o
				If !lForce .And. oModelG6I:GetValue("G6I_DIVFOP") <> "0"
					T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), I18N(STR0072 , {oModelG6I:GetValue("G6I_ITEM")}))	 //"Item da fatura #1[Num. da fatura]# est� com diverg�ncia de FOP, ajuste o Registro de venda antes de associar."
					lRet 	  := .F.		
					lMsgError := .T.
					oModelG6I:LoadValue("TMP_OK", .F.)
					Loop
			
				ElseIf oModelG6I:GetValue("G6I_SITUAC") == '2' .And. oModelG6I:GetValue("G6I_META") <> '1' .And. !T39VldNatur( oModel, @cError )
					T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), SubStr(STR0098 + ": " + cError, 1, 200))	 //"Informe as naturezas solicitadas."
					lRet 	  := .F.		
					lMsgError := .T.						
					oModelG6I:LoadValue("TMP_OK", .F.)
					Loop
				
				//N�o avalia para associa��o manual pois pode n�o ter gerado os acertos necess�rios.
				ElseIf !IsBlind() .And. oModelG6I:GetValue("G6I_META") <> "1" .And. IIF((oModelG6I:GetValue('G6I_TIPOIT') $ '3|5' .And. oModelG6I:GetValue("G6I_TPOPER") == '2') .Or. ;
				 																	   (!oModelG6I:GetValue('G6I_TIPOIT') $ '3|5' .And. oModelG6I:GetValue("G6I_TPOPER") == '1'), oModelG6I:GetValue("G6I_VLRLIQ"), oModelG6I:GetValue("G6I_VLRLIQ") * -1) <> T39GetVlrLiq()
					lRet 	  := .F.	
					lMsgError := .T.
					T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), STR0117)		//"Diverg�ncia de valor l�quido."
				EndIf
			EndIf	
		
			If oModelG6I:GetValue("G6I_SITUAC") == '3' 	//Estorno de aprova��o
				If SuperGetMV("MV_TURLIFA", , .T.)		//Realiza o estorno da libera��o dos itens do cliente			
					If T39LibIfEn(oModel, .T.)			//Verifica se algum item j� foi finalizado ou apurado	
						lRet      := .F.
						lMsgError := .T.
						T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), I18N(STR0115, {oModelG6I:GetValue("G6I_ITEM")}))   //"Itens liberados do item #1[item fatura]# j� foram apurados ou finalizados."
					Else
						If (lRet := T39LibIfCl(oModel, .F.))
							aAdd(aEstorna, oModelG6I:GetValue("G6I_ITEM"))
						Else
							T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), STR0157) 	//"Erro ao estornar a libera��o dos itens financeiros."
						EndIf
					EndIf	
				EndIf
			ElseIf lRet .And. oModelG6I:GetValue("G6I_SITUAC") == "2"		// Aprova��o de item financeiro 
				If SuperGetMV("MV_TURLIFA", , .T.)		//Realiza a libera��o dos itens do cliente			
					If !(lRet := T39LibIfCl(oModel, .T.))
						T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), TCSqlError())
					EndIf
				EndIf
					
				If lRet 
					aAdd(aAprova, oModelG6I:GetValue("G6I_ITEM"))
				EndIf
	
			ElseIf oModelG6I:GetValue("G6I_SITUAC") == "4" //Associa��o autom�tica pendente
				lRet      := .F.
				lMsgError := .T.
				T39AddError(oMdl039H, oModelG6I:GetValue("G6I_ITEM"), oModelG6I:GetValue("G6I_NUMID"), oModelG6I:GetValue("G6I_IDITEM"), oModelG6I:GetValue("G6I_NUMSEQ"), STR0165)	 	//"Item com erro durante associa��o. Favor desassociar o item e associar novamente."
			EndIf
			oModelG6I:LoadValue("TMP_OK", .F.)
		EndDo

		//Atualiza os status dos itens financeiros
		If lRet
			lRet := T39UpdItSts(oModel, aAprova, "3", "BR_VERDE") .And. T39UpdItSts(oModel, aEstorna, "2", "BR_AMARELO")
		EndIf
			
		//Valida campo G6J_APROVA
		If lRet
			If !T039AprSt(oModel)
				MsgAlert(STR0155) //'Erro ao atualizar status.'
				lRet := .F.
			EndIf	
		EndIf

		If !lRet
			DisarmTransaction()
			Break
		EndIf
	END TRANSACTION

	If lMsgError
		If Aviso(STR0113, STR0118, {STR0001, STR0119}, 2) == 1 //"Aten��o"###"Encontrada(s) diverg�ncia(s) durante o processo. Deseja visualizar a(s) diverg�ncia(s)?"###"Visualizar"###"Sair" 
			T39ShowErr(oMdl039H)
		EndIf
	EndIf
	oMdl039H:DeActivate()
	oMdl039H:Destroy()
	FwModelActive(oModel)
EndIf

FwRestRows(aSaveLines)

Tur39Refresh()

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039Efet
Gera��o da efetiva��o.

@author Enaldo Cardoso Junior
@since 29/03/2015
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA039Efet()

Local oModel 	 := Nil
Local oModelG8Y  := FwLoadModel("TURA042B")
Local oModelG6H  := Nil 
Local oModelG6I	 := Nil
Local oModelG3R	 := Nil
Local oModelG4CA := Nil
Local oModelG4CB := Nil
Local oModelG48A := Nil
Local oModelG48B := Nil
Local oModelG6M	 := Nil
Local oModelG81	 := Nil
Local oModelG6J	 := Nil
Local aDadosTit	 := {}
Local aDadosTitP := {}
Local aDadosTitR := {}
Local aItensNFS	 := {}
Local aValAbatP  := {}
Local aValAbatR  := {}
Local aTitAvulso := {}
Local aLogG8Y	 := {}
Local cNatureza  := ""
Local cErro 	 := ""
Local cNumNFS 	 := ""
Local cSerNFS 	 := ""
Local cFilAntBkp := cFilAnt
Local cAliasAux	 := GetNextAlias()
Local nX		 := 1
Local nY		 := 1
Local nZ		 := 0
Local nJ		 := 0
Local nPosSerie  := 0
Local nFilRef	 := 0
Local nPosNatur	 := 0
Local lContinua  := .T.
Local lOnLine	 := .T.
Local lMostrLanc := .T.
Local lAglutLanc := .F.

Pergunte("TURA038", .F.)

lOnLine 	:= IIF(mv_par01 == 1, .T., .F.)
lMostrLanc	:= IIF(mv_par02 == 1, .T., .F.)
lAglutLanc	:= IIF(mv_par03 == 1, .T., .F.)

//Obtem todos os fornecedores para faturamento
BeginSql Alias cAliasAux
	//Fornecedores Provenientes do Documento de Reserva
	SELECT DISTINCT G4C_CODIGO FORNECEDOR, G4C_LOJA LJ_FORNEC, 'N' META
	FROM %Table:G3R% G3R
	INNER JOIN %table:G4C% G4C ON G4C.G4C_FILIAL = G3R.G3R_FILIAL AND G4C.G4C_NUMID = G3R.G3R_NUMID AND G4C.G4C_IDITEM = G3R.G3R_IDITEM AND G4C_CLIFOR = '2' AND G4C_STATUS = '1' AND G4C.G4C_CARTUR = %Exp:Space(TamSX3("G4D_PROPRI")[1])% AND G4C.%notDel%
	INNER JOIN %table:G6I% G6I ON G3R.G3R_FILCON = G6I.G6I_FILIAL AND G6I_FATURA = %Exp:G6H->G6H_FATURA% AND G3R.G3R_NUMID = G6I.G6I_NUMID AND G3R.G3R_IDITEM = G6I.G6I_IDITEM AND G3R.G3R_CONCIL = G6I.G6I_CONCIL AND G6I.%notDel%
	WHERE G3R_FILCON = %Exp:G6H->G6H_FILIAL% AND G3R.%notDel%
	
	UNION 	
	
	//Fornecedores Provenientes das Metas Associadas
	SELECT DISTINCT G6L_FORNEC FORNECEDOR, G6L_LJFORN LJ_FORNEC, 'S' META	
	FROM %Table:G6L% G6L 
	INNER JOIN %Table:G6M% G6M ON G6M_FILIAL = G6L_FILIAL AND G6M_CODAPU = G6L_CODAPU AND G6M_FILCON = %Exp:G6H->G6H_FILIAL% AND G6M_FATMET = %Exp:G6H->G6H_FATURA% AND G6M_CONCIL <> '' AND G6M.%NotDel%	
	INNER JOIN %Table:G6I% G6I ON G6I_FILIAL = G6M_FILCON AND G6I_FATURA = G6M_FATMET AND G6I_ITEM = G6M_ITFAT AND G6I_CONCIL = G6M_CONCIL AND G6I_SITUAC = '3' AND G6I.%NotDel%	
	WHERE G6L_TPAPUR = '2' AND G6L.%NotDel%
EndSql

G6J->(DbSetOrder(2))	//G6J_FILIAL+G6J_FATURA+G6J_CPIATA
SED->(DbSetOrder(1))	//ED_FILIAL+ED_CODIGO
nValFat := TA039VlToF(G6H->G6H_FATURA, G6H->G6H_FORNEC, G6H->G6H_LOJA)	//Valida��o do total da Fatura � igual ao valor do cabe�alho

If nValFat # G6H->G6H_VALOR
	Help( , , "TA039EFET", , I18N(STR0035, {AllTrim(Transform(G6H->G6H_VALOR, PesqPict("G6H", "G6H_VALOR"))), AllTrim(Transform(nValFat, PesqPict("G6H", "G6H_VALOR")))}), 1, 0)  	//"O valor informado na fatura ( #1[Valor Fatura]# ) � divergente do calculado nos itens financeiros ( #2[Valor Fatura]# )."
	Return .F.
EndIf

oModel := FwLoadModel("TURA039F")
oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:Activate()
oModelG6H := oModel:GetModel("G6H_MASTER")

//Valida��o se a natureza est� preenchida
If !T39VldNatur(oModel, @cErro, "1")
	TurHelp(cErro, STR0081, "TA039EFET") //"Informe as naturezas dos itens listados na concilia��o."
	oModel:DeActivate()
	Return
EndIf

BEGIN TRANSACTION 
	lContinua := oModelG6H:LoadValue("G6H_EFETIV", "1")
	While (cAliasAux)->(!Eof()) .And. lContinua		//Realiza movimenta��es da efetiva��o da concilia��o
		aDadosTitP := {}
		aDadosTitR := {}
		aDadosTit  := {}
		aItensNFS  := {}
		aValAbatP  := {}
		aValAbatR  := {}
		aTitAvulso := {}
		aLogG8Y	   := {}
		cNatureza  := ""
		cErro	   := ""

		If TA042ItNFS((cAliasAux)->FORNECEDOR, (cAliasAux)->LJ_FORNEC, aItensNFS, @aValAbatR, @cErro, oModel)		//Verifica os itens para faturamento do fornecedor informado via parametro
			If ((cAliasAux)->META == "S")		//Verifica itens de meta para faturamento (gera��o da nota de sa�da) 
				lContinua := TA39MItNFS(aItensNFS, oModel,(cAliasAux)->FORNECEDOR, (cAliasAux)->LJ_FORNEC)
			EndIf	
			
			If (lContinua .And. Len(aItensNFS) > 0)
				For nFilRef := 1 To Len(aItensNFS) //Loop por filiais de refer�ncia
					If !lContinua
						Exit
					EndIf

					If !Empty(aItensNFS[nFilRef][1])
						cFilAnt := aItensNFS[nFilRef][1]
					EndIf

					For nPosSerie := 1 To Len(aItensNFS[nFilRef][2]) //Loop por s�rie das notas fiscais
						If !lContinua
							Exit
						EndIf

						For nPosNatur := 1 To Len(aItensNFS[nFilRef][2][nPosSerie][2]) //Loop por natureza
							If Empty(aItensNFS[nFilRef][2][nPosSerie][2])
								FwAlertHelp(STR0089, STR0090, "TA039EFET") 		//"Verifique o cadastro de 'Parametros Fiscais e Financeiros' se os c�digos de Naturezas est�o corretos." ### "Natureza em branco."
								lContinua := .F.
								Exit

							ElseIf !SED->(DbSeek(xFilial("SED") + aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1]))
								FwAlertHelp(I18N(STR0091, {aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1], cFilAnt}), STR0092, "TA039EFET") //"Natureza #1[codigo natureza]# n�o cadastrada na filial #2[C�digo filial]#." ### "Verifique o cadastro de Naturezas na filial informada."
								lContinua := .F.
								Exit
							EndIf

							cNumNFS := ""
							cSerNFS := aItensNFS[nFilRef][2][nPosSerie][1]
							TurSetNatur(aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1])

							//Gera��o da nota fiscal de sa�da
							aLogG8Y := {}
							If TA042EfNFS((cAliasAux)->FORNECEDOR, (cAliasAux)->LJ_FORNEC, @cNumNFS, @cSerNFS, aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][2], aLogG8Y)
								cFilAnt := cFilAntBkp
								If TA42AddLog(oModelG8Y, aLogG8Y, "", oModelG6H:GetValue("G6H_FATURA"), "1") 
									lContinua := .T.
								Else
									FwAlertHelp(STR0093, STR0094, "TA039EFET")  //"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
									lContinua := .F.
									Exit
								EndIf

							Else
								MostraErro()
								lContinua := .F.
								Exit
							EndIf
						Next nPosNatur
					Next nPosSerie
				Next nFilRef
			EndIf
		Else
			Help( , , "TA039EFET", , cErro, 1, 0)
			lContinua := .F.
		EndIf

		//Realiza as transfer�ncias dos t�tulos a receber para o fornecedor da fatura
		If lContinua .And. oModelG6H:GetValue("G6H_FORNEC") <> (cAliasAux)->FORNECEDOR .Or. oModelG6H:GetValue("G6H_LOJA") <> (cAliasAux)->LJ_FORNEC
			aTitulos := TA042GetTit(oModelG6H:GetValue("G6H_FATURA"), "1", (cAliasAux)->FORNECEDOR, (cAliasAux)->LJ_FORNEC , "1")

			SA2->(DbSetOrder(1))	// A2_FILIAL+A2_COD+A2_LOJA
			SA2->(DbSeek(xFilial("SA2") + oModelG6H:GetValue("G6H_FORNEC") + oModelG6H:GetValue("G6H_LOJA") ) )
			For nX := 1 To Len(aTitulos)
				aLogG8Y := {}
				If TA042TrfTi("", aTitulos[nX], SA2->A2_CLIENTE, SA2->A2_LOJCLI, SA2->A2_COD, SA2->A2_LOJA, aLogG8Y)
					For nY := 1 To Len(aLogG8Y)
						If TA42AddLog(oModelG8Y, aLogG8Y[nY], "", oModelG6H:GetValue("G6H_FATURA"), "1") 
							lContinua := .T.
						Else
							FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
							lContinua := .F.
							Exit
						EndIf
					Next nY
				Else
					lContinua := .F.
					Exit
				EndIf
			Next nX
		EndIf
		(cAliasAux)->(DbSkip())
	EndDo
	(cAliasAux)->(DbCloseArea())

	If lContinua
		TA042DaTit(oModel, aValAbatP, aDadosTitP, aValAbatR, aDadosTitR, .F.)		//Obtem os valores para os t�tulos a pagar e receber
		For nZ := 1 To Len(aDadosTitP)
			aLogG8Y := {}
			If TA042EfTiP("", "INT", oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aDadosTitP[nZ][2], aDadosTitP[nZ][3], oModelG6H:GetValue("G6H_EMISSA"), oModelG6H:GetValue("G6H_CONDPG"), aDadosTitP[nZ][4], oModelG6H:GetValue("G6H_VENCTO"), aLogG8Y)
				For nX := 1 To Len(aLogG8Y)
					If TA42AddLog(oModelG8Y, aLogG8Y[nX], "", oModelG6H:GetValue("G6H_FATURA"), "1")
						lContinua := .T.
					Else
						FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log."	### "Entre em contato com o administrador do sistema"
						lContinua := .F.
						Exit
					EndIf
				Next nX
			Else
				Help( , , "TA039EFET", , STR0095, 1, 0) 	//"Erro ao gerar o t�tulo a pagar, processo ser� estornado."
				lContinua := .F.
			EndIf
		Next nZ
	EndIf

	//Gera��o de t�tulo a receber de produtos sem cadastro nos parametros financeiros
	If lContinua .And. Len(aDadosTitR) > 0
		For nY := 1 To Len(aDadosTitR)
			cFilAnt := aDadosTitR[nY][1]
			aLogG8Y := {}
			If lContinua .And. TA042EfTiR(oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aDadosTitR[nY][2], aDadosTitR[nY][3], aLogG8Y)
				For nX := 1 To Len(aLogG8Y)
					cFilAnt := cFilAntBkp
					If TA42AddLog(oModelG8Y, aLogG8Y[nX], "", oModelG6H:GetValue("G6H_FATURA"), "1") 
						lContinua := .T.
					Else
						FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
						lContinua := .F.
						Exit
					EndIf
				Next nX
			Else
				FwAlertHelp(STR0096, STR0094, "TA039EFET") 		//"Erro ao gravar titulo receber." ### "Entre em contato com o administrador do sistema"
				lContinua := .F.
			EndIf
		Next nY
	EndIf

	If lContinua .And. Len(aValAbatP) > 0
		For nY := 1 To Len(aValAbatP)
			cFilAnt := aValAbatP[nY][1]
			aLogG8Y := {}
			If lContinua .And. TA042EfTiR(oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aValAbatP[nY][2], aValAbatP[nY][3], aLogG8Y)
				For nX := 1 To Len(aLogG8Y)
					cFilAnt := cFilAntBkp
					If TA42AddLog(oModelG8Y, aLogG8Y[nX], "", oModelG6H:GetValue("G6H_FATURA"), "1") 
						lContinua := .T.
					Else
						FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
						lContinua := .F.
						Exit
					EndIf
				Next nX
			Else
				FwAlertHelp(STR0096, STR0094, "TA039EFET") 		//"Erro ao gravar titulo receber." ### "Entre em contato com o administrador do sistema"
				lContinua := .F.
			EndIf
		Next nY
	EndIf

	If lContinua .And. Len(aValAbatR) > 0
		For nZ := 1 To Len(aValAbatR)
			aLogG8Y := {}
			If TA042EfTiP("", "INT", oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aValAbatR[nZ][2], aValAbatR[nZ][3], oModelG6H:GetValue("G6H_EMISSA"), oModelG6H:GetValue("G6H_CONDPG"), aValAbatR[nZ][4], oModelG6H:GetValue("G6H_VENCTO"), aLogG8Y)
				For nX := 1 To Len(aLogG8Y)
					If TA42AddLog(oModelG8Y, aLogG8Y[nX], "", oModelG6H:GetValue("G6H_FATURA"), "1")
						lContinua := .T.
					Else
						FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
						lContinua := .F.
						Exit
					EndIf
				Next nX
			Else
				Help( , , "TA039EFET", , STR0095, 1, 0) //"Erro ao gerar o t�tulo a pagar, processo ser� estornado."
				lContinua := .F.
			EndIf
		Next nZ
	EndIf

	//Retorna o Model Ativo para o modelo da concilia��o
	FwModelActive(oModel)

	//Compensa��o dos t�tulos
	If lContinua
		aLogG8Y := {}
		BEGIN TRANSACTION
			//Realiza a transferencia dos t�tulos a recebe para a filial corrente
			If TA042TrAllT(oModelG6H:GetValue("G6H_FATURA"), "1", oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aLogG8Y)
				For nY := 1 To Len(aLogG8Y)
					If TA42AddLog(oModelG8Y, aLogG8Y[nY], "", oModelG6H:GetValue("G6H_FATURA"), "1")
						lContinua := .T.
					Else
						DisarmTransaction()
						TurHelp(STR0173, "", "TA039EFET") 		//"Erro ao transferir t�tulo para a filial corrente" 
						lContinua := .F.
						Break
					EndIf
				Next
			Else
				DisarmTransaction()
				lContinua := .F.
				Break
			EndIf
		END TRANSACTION
			
		aLogG8Y := {}
		If (lContinua := TA042CompT(oModelG6H:GetValue("G6H_FATURA"), "1", oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), aLogG8Y))
			If Len(aLogG8Y) > 0
				If TA42AddLog(oModelG8Y, aLogG8Y, "", oModelG6H:GetValue("G6H_FATURA"), "1")
					lContinua := .T.
				Else
					FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
					lContinua := .F.
				EndIf
			EndIf
		Else
			Help( , , "TA039EFET", , STR0005, 1, 0)		// "Erro ao realizar a compensa��o entre carteiras." 
			lContinua := .F.
		EndIf
	EndIf
	
	If lContinua
		aTitulos := TA042TitFT(oModel, "1", oModelG6H:GetValue("G6H_FATURA"), oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"))
		If Len(aTitulos[4]) > 0
			aLogG8Y := {}
			If (lContinua := TA042GerFT(oModelG6H:GetValue("G6H_SERIE"), "FTF", oModelG6H:GetValue("G6H_FATURA"), oModelG6H:GetValue("G6H_NATUR"), oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), oModelG6H:GetValue("G6H_CONDPG"), aTitulos[1], aTitulos[2], aTitulos[3], aTitulos[4], aLogG8Y, , .T.))
				If TA42AddLog(oModelG8Y, aLogG8Y, "", oModelG6H:GetValue("G6H_FATURA"), "1")
					lContinua := .T.
				Else
					FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
					lContinua := .F.
				EndIf
			Else
				Help( , , "TA039EFET", , STR0097, 1, 0) //"Erro ao gerar o fatura a pagar, processo ser� estornado."
			EndIf
		EndIf
	EndIf
	
	//Compensa��o de recebimento antecipado
	If lContinua
		aLogG8Y := {}
		If (lContinua := TA042ComRA(oModelG6H:GetValue("G6H_FATURA"), , oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), "1", aLogG8Y))
			If TA42AddLog(oModelG8Y, aLogG8Y, "", oModelG6H:GetValue("G6H_FATURA"), "1")
				lContinua := .T.
			Else
				FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
				lContinua := .F.
			EndIf
		Else
			Help( , , "TA039EFET", , STR0006, 1, 0) //"Erro ao realizar a compensa��o com o Recebimento Antecipado (RA)."
		EndIf
	EndIf
	
	//Compensa��o de pagamento antecipado
	If lContinua
		aLogG8Y := {}
		If (lContinua := TA042ComPA(oModelG6H:GetValue("G6H_FATURA"), , oModelG6H:GetValue("G6H_FORNEC"), oModelG6H:GetValue("G6H_LOJA"), "1", aLogG8Y))
			If TA42AddLog(oModelG8Y, aLogG8Y, "", oModel:GetValue("G6H_FATURA"), "1")
				lContinua := .T.
			Else
				FwAlertHelp(STR0093, STR0094, "TA039EFET")		//"Erro ao gravar o log." ### "Entre em contato com o administrador do sistema"
				lContinua := .F.
			EndIf
		Else
			Help( , , "TA039EFET", , STR0006, 1, 0) //"Erro ao realizar a compensa��o com o Pagamento Antecipado (PA)."
		EndIf
	EndIf
	
	If lContinua
		oModelG6J  := oModel:GetModel("G6J_ITENS")
		oModelG3Q  := oModel:GetModel("G3Q_ITENS")
		oModelG3R  := oModel:GetModel("G3R_ITENS")
		oModelG4CA := oModel:GetModel("G4CA_ITENS")
		oModelG4CB := oModel:GetModel("G4CB_ITENS")
		oModelG48A := oModel:GetModel("G48A_ITENS")
		oModelG48B := oModel:GetModel("G48B_ITENS")
		oModelG6M  := oModel:GetModel("G6M_ITENS")
		oModelG81  := oModel:GetModel("G81_ITENS")

		For nX := 1 To oModelG3R:Length()
			If (!Empty(oModelG3R:GetValue("G3R_NUMID", nX)))
				oModelG3R:GoLine(nX)
				If oModelG3R:GetValue("G3R_STATUS") $ "2|4" //Particionado|Cancelado
					Loop
				EndIf

				For nY := 1 To oModelG4CA:Length()
					oModelG4CA:GoLine(nY)
					//Se item financeiro de repasse, n�o libera neste momento, ser� liberado juntamente com o acordo de origem no fornecedor
					If oModelG4CA:GetValue("G4C_STATUS") == "1" .And. Empty( oModelG4CA:GetValue("G4C_CODAPU")) .And. oModelG4CA:GetValue("G4C_CLASS") <> "C07"
						//Se IF tem origem em acordo e n�o est� vinculado ao item principal, passa para Ag. Apura��o
						If !Empty(oModelG4CA:GetValue("G4C_CLASS")) .And. Empty(oModelG4CA:GetValue("G4C_IFPRIN"))
							lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_STATUS", "2")
							lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_TPCONC", "1")
						Else
							lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_STATUS", "3")
							lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_TPCONC", "1")
							lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_DTLIB" , dDataBase)
						EndIf
					EndIf
				Next nX
				
				For nY := 1 To oModelG4CB:Length()
					oModelG4CB:GoLine(nY)
					If oModelG4CB:GetValue("G4C_STATUS") $ "1|3" .And. Empty(oModelG4CB:GetValue("G4C_CARTUR")) .And. Empty(oModelG4CB:GetValue("G4C_CODAPU"))
						lContinua := lContinua .And. oModelG4CB:LoadValue("G4C_STATUS", "4")
						lContinua := lContinua .And. oModelG4CB:LoadValue("G4C_TPCONC", "1")

						//Verifica se o acordo de fornecedor � base para acordo de repasse do cliente, neste caso, libera o acordo
						If !Empty(oModelG4CB:GetValue("G4C_NUMACD")) .And. oModelG48A:SeekLine({{ "G48_ACDBAS", oModelG4CB:GetValue("G4C_NUMACD")}})
							For nZ := 1 to oModelG4CA:Length()
								oModelG4CA:GoLine(nZ)
								If oModelG4CA:GetValue("G4C_NUMACD") ==  oModelG48A:GetValue("G48_CODACD") .And. Empty( oModelG4CA:GetValue("G4C_CODAPU") ) 
									lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_STATUS", TA39UpdRepas(oModelG4CA) )
									lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_TPCONC", "1")
									lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_DTLIB" , dDataBase )
								EndIf
							Next nZ
						EndIf

						//Finaliza o acordo que originou o item financeiro
						For nZ := 1 to oModelG48B:Length()
							oModelG48B:GoLine(nZ)
							If !oModelG48B:IsDeleted() .And. !Empty(oModelG48B:GetValue("G48_APLICA")) .And. oModelG48B:GetValue("G48_APLICA") == oModelG4CB:GetValue("G4C_APLICA")
								lContinua := lContinua .And. oModelG48B:LoadValue("G48_CODCON", oModelG3R:GetValue("G3R_CONCIL"))
								lContinua := lContinua .And. oModelG48B:LoadValue("G48_STATUS", "4")
							EndIf
						Next nZ
					EndIf
				Next nY
			EndIf
		Next nX			
		
		//Atualiza as Concilia��es - CAMPO de efetiva��o
		For nX := 1 To oModelG6J:Length()
			If oModelG6J:GetValue("G6J_APROVA", nX) == "3"				
				oModelG6J:GoLine(nX)					
				oModelG6J:LoadValue("G6J_EFETIV", "1")	
				oModelG6J:LoadValue("G6J_DTEFET", dDataBase)
			EndIf
		Next nX
		
		If !oModelG6M:IsEmpty()
			//Atualiza��o da Apura��o de Metas Liberada. 
			For nX := 1 to oModelG6M:Length()
				If !Empty(oModelG6M:GetValue("G6M_CODAPU", nX))
					oModelG6M:GoLine(nX)
					For nZ := 1 to oModelG81:Length() 			
						oModelG81:GoLine(nZ)
						oModelG81:LoadValue("G81_STATUS", "2")
						oModelG81:LoadValue("G81_FILFOR", oModelG6H:GetValue("G6H_FILIAL"))
						oModelG81:LoadValue("G81_FATFOR", oModelG6M:GetValue("G6M_FATMET"))
						oModelG81:LoadValue("G81_PRFFOR", oModelG6H:GetValue("G6H_SERIE"))
					Next nZ	
				EndIf
			Next nX	
		EndIf
	EndIf
	
	If lContinua
		If (lContinua := oModel:VldData())
			oModel:CommitData()
		Else
			JurShowErro( oModel:GetErrorMessage() )
		EndIf
	EndIf

	If lContinua
		For nX := 1 to oModelG3R:Length()
			oModelG3R:GoLine( nX )

			If oModelG3R:GetValue("G3R_STATUS") $ "2|4" //Particionado|Cancelado
				Loop
			EndIf
			TurAtuStRV(oModelG3R:GetValue("G3R_FILIAL"), oModelG3R:GetValue("G3R_NUMID"), oModelG3R:GetValue("G3R_IDITEM"), oModelG3R:GetValue("G3R_NUMSEQ"), "", "3", .T.)
		Next nX
	
		If lOnLine
			T039ACtInc(lOnLine, lMostrLanc, lAglutLanc, "TURA039")
		EndIf
		
		If !IsBlind()
			MsgInfo(STR0029) //"Incluido com sucesso! "
		EndIf
	Else
		DisarmTransaction()
		Break
	EndIf
END TRANSACTION 

cFilAnt := cFilAntBkp

oModel:DeActivate()
oModel:Destroy()

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039DEfe
Desfaz a efetiva��o.

@author Enaldo Cardoso Junior
@since 04/04/2015
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA039DEfe()
	
Local oModel 	 := Nil
Local oMldTura34 := Nil
Local oMldTura39 := Nil
Local oModelG8Y  := Nil
Local oModelG6H  := Nil
Local oModelG6I	 := Nil
Local oModelG3R	 := Nil
Local oModelG4CA := Nil
Local oModelG4CB := Nil
Local oModelG48A := Nil
Local oModelG48B := Nil
Local oModel39F	 := Nil 
Local nX		 := 0
Local nY		 := 0
Local nZ		 := 0
Local nJ		 := 0
Local cMsg		 := ""
Local lRet 	 	 := .T.	
Local lContinua  := .T.
Local lOnLine	 := .T.
Local lMostrLanc := .T.
Local lAglutLanc := .F.

Pergunte("TURA038", .F.)

lOnLine 	:= IIF(mv_par01 == 1,.T.,.F.)
lMostrLanc	:= IIF(mv_par02 == 1,.T.,.F.)
lAglutLanc	:= IIF(mv_par03 == 1,.T.,.F.)

If !T39VldG4C(G6H->G6H_FATURA, @cMsg)
	Help( , , "TA039DEFE", , STR0086 + CRLF + cMsg, 1, 0)	//"N�o ser� poss�vel desefetivar pelos seguintes motivos:"
	Return
EndIf

BEGIN TRANSACTION
	oModel := FwLoadModel("TURA038")
	oModel:SetOperation(MODEL_OPERATION_UPDATE)
	lContinua := oModel:Activate()
	
	oModel:LoadValue("G6HMASTER","G6H_EFETIV", "2")
	
	oMldTura34 := FwLoadModel("TURA034")
	oModelG3R  := oMldTura34:GetModel("G3R_ITENS")
	
	oModelG8Y := FwLoadModel("TURA042B")
	oModelG8Y:SetOperation( MODEL_OPERATION_DELETE )
	
	//Estorno das compensa��es de RA/PA
	If lContinua
		lContinua := TA042EsCom( G6H->G6H_FATURA, G6H->G6H_FORNEC, G6H->G6H_LOJA, "1")
	EndIf
	
	//Estorno da fatura a pagar
	If lContinua
		lContinua := TA042EsFat( G6H->G6H_FATURA, G6H->G6H_SERIE, G6H->G6H_FORNEC, G6H->G6H_LOJA )
	EndIf
	
	//Estorno de compensa��o entre carteiras
	If lContinua
		lContinua := TA042CmpCa(G6H->G6H_FATURA, G6H->G6H_FORNEC, G6H->G6H_LOJA, '1')
	EndIf
	
	//Estorno das transfer�ncias dos t�tulos
	If lContinua
		G8Y->( dbSetOrder(4) )	//G8Y_FILIAL+G8Y_TPFAT+G8Y_FATURA
		G8Y->( dbSeek( xFilial("G8Y") + "1" + G6H->G6H_FATURA ) )
	
		While G8Y->( !Eof() ) .And. G8Y->( G8Y_FILIAL+G8Y_TPFAT+G8Y_FATURA ) == xFilial("G8Y") + "1" + G6H->G6H_FATURA
			If G8Y->G8Y_TPDOC == "5" //T�tulo a receber transferido
				lContinua := TA042EstTr( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_PARCEL, G8Y->G8Y_TIPO, G8Y->G8Y_FILREF  )
	
				If !lContinua
					Exit
				EndIf
			EndIf
			G8Y->( dbSkip() )
		EndDo
	EndIf
	
	
	G8Y->( dbSeek( xFilial("G8Y") + "1" + G6H->G6H_FATURA ) )
	While lContinua .And. G8Y->( !Eof() ) .And. G8Y->G8Y_FILIAL+G8Y->G8Y_TPFAT+G8Y->G8Y_FATURA == xFilial("G8Y") + "1" + G6H->G6H_FATURA
	
		If G8Y->G8Y_TPDOC == "1" //Contas a Receber
	
			If TA042DeTiR( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_TIPO, G8Y->G8Y_PARCEL, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
			Else
				MostraErro()
				lContinua := .F.
			EndIf
	
		ElseIf G8Y->G8Y_TPDOC == "2" //Contas a pagar
			If TA042DeTiP( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_TIPO, G8Y->G8Y_PARCEL, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
	
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
			Else
				MostraErro()
				lContinua := .F.
			EndIf
	
	
		ElseIf G8Y->G8Y_TPDOC == "3" //Nota fiscal de entrada
			If TA042DeNFe( G8Y->G8Y_DOC, G8Y->G8Y_SERIE, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA )
	
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
	
			Else
				MostraErro()
				lContinua := .F.
			EndIf
	
		ElseIf G8Y->G8Y_TPDOC == "4" //Nota fiscal de saida
			If TA042DeNFS( G8Y->G8Y_DOC, G8Y->G8Y_SERIE, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
	
			Else
				MostraErro()
				lContinua := .F.
	
			EndIf
	
		Else
			oModelG8Y:Activate()
			If oModelG8Y:VldData()
				lContinua := .T.
				oModelG8Y:CommitData()
			EndIf
			oModelG8Y:DeActivate()
	
		EndIf
	
		(G8Y->( dbSkip() ))
	EndDo
	
	If ( lContinua )
		
		//Efetua a atualiza��o da libera��o da meta, para desfazer as atualiza��es 
		//que foram feitas na efetiva��o da concilia��o de meta		
		oModel39F := FwLoadModel("TURA039F")
		oModel39F:SetOperation(MODEL_OPERATION_UPDATE)
		oModel39F:GetModel("G81_ITENS"):SetLoadFilter({{"G81_STATUS","'2'"}}) //Filtra somente os itens que foram efetivados
		
		oModel39F:Activate()
		
		For nX := 1 to oModel39F:GetModel("G6M_ITENS"):Length()
			
			If ( !Empty(oModel39F:GetModel("G6M_ITENS"):GetValue("G6M_CODAPU",nX)) )
			
				oModel39F:GetModel("G6M_ITENS"):GoLine(nX)
				
				For nZ := 1 to oModel39F:GetModel("G81_ITENS"):Length() 			
					
					oModel39F:GetModel("G81_ITENS"):GoLine(nZ)
					
					oModel39F:GetModel("G81_ITENS"):LoadValue("G81_STATUS","1")
					oModel39F:GetModel("G81_ITENS"):LoadValue("G81_FILFOR","")
					oModel39F:GetModel("G81_ITENS"):LoadValue("G81_FATFOR","")
					oModel39F:GetModel("G81_ITENS"):LoadValue("G81_PRFFOR","")
					
				Next nZ	
			
			EndIf
			
		Next nx	
		
		If ( oModel39F:VldData() )
			oModel39F:CommitData()
			lContinua	:= .T.
		Else
			JurShowErro( oModel39F:GetErrorMessage() )
			lContinua := .F.
		EndIf
	
		oModel39F:DeActivate()
		
		If ( lContinua )
		
			If oModel:VldData()
				oModel:CommitData()
				lContinua	:= .T.
			Else
				lContinua	:= .F.
			EndIf
		
		EndIf
		
	EndIf
	
	If lContinua
	
		DbSelectArea("G6J")
		DbSetOrder(2)	//G6J_FILIAL+G6J_FATURA+G6J_CPIATA
	
		oMldTura39 := FwLoadModel("TURA039MDL")
		oMldTura39:SetOperation( MODEL_OPERATION_UPDATE )
	
		If G6J->(DbSeek(xFilial("G6H")+G6H->G6H_FATURA))
	
			While !Eof() .And. xFilial("G6H") == G6J->G6J_FILIAL .And. G6H->G6H_FATURA == G6J->G6J_FATURA
	
				If oMldTura39:Activate()
					T39OpenMdl( oMldTura39, 'G6J_MASTER', .T.)
	
					oModelG6I	:= oMldTura39:GetModel("G6I_ITENS")
					oModelG3R	:= oMldTura39:GetModel("G3R_ITENS")
					oModelG3Q	:= oMldTura39:GetModel("G3Q_ITENS")
					oModelG4CA	:= oMldTura39:GetModel("G4CA_ITENS")
					oModelG4CB	:= oMldTura39:GetModel("G4CB_ITENS")
					oModelG48A	:= oMldTura39:GetModel("G48A_ITENS")
					oModelG48B	:= oMldTura39:GetModel("G48B_ITENS")
	
	
					For nJ := 1 to oModelG6I:Length()
						oModelG6I:GoLine( nJ )
	
						For nX := 1 to oModelG3R:Length()
							oModelG3R:GoLine( nX )
	
							If !oModelG4CA:IsEmpty()
								For nY := 1 to oModelG4CA:Length()
									oModelG4CA:GoLine( nY )
	
									//Se item financeiro de repasse, ser� estornada a libera��o juntamente com o acordo de origem no fornecedor
									If oModelG4CA:GetValue("G4C_STATUS")$ "2|3" .And. oModelG4CA:GetValue("G4C_TPCONC") $ "1|3|" .And. oModelG4CA:GetValue("G4C_CLASS") <> 'C07'
										lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_STATUS", "1")
										lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_TPCONC", "")
										lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_DTLIB" , ctod(" / / ") )
									EndIf
	
								Next
	
							EndIf
							
							For nY := 1 to oModelG4CB:Length()
								oModelG4CB:GoLine( nY )
	
								If oModelG4CB:GetValue("G4C_STATUS") == "4" .And. oModelG4CB:GetValue("G4C_TPCONC") == "1"
									
									lContinua := lContinua .And. oModelG4CB:LoadValue("G4C_STATUS", "1")
									lContinua := lContinua .And. oModelG4CB:LoadValue("G4C_TPCONC", "")
									
	
									//Verifica se o acordo de fornecedor � base para acordo de repasse do cliente, neste caso, libera o acordo
									If !Empty(oModelG4CB:GetValue("G4C_NUMACD") ) .And. oModelG48A:SeekLine( { { "G48_ACDBAS", oModelG4CB:GetValue("G4C_NUMACD") } } )
										For nZ := 1 to oModelG4CA:Length()
											oModelG4CA:GoLine( nZ )
	
											If oModelG4CA:GetValue("G4C_NUMACD") ==  oModelG48A:GetValue("G48_CODACD")
												lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_STATUS", "1")
												lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_TPCONC", "")
												lContinua := lContinua .And. oModelG4CA:LoadValue("G4C_DTLIB" , ctod(" / / ") )
	
											EndIf
	
										Next
	
									EndIf
	
									//Finaliza o acordo que originou o item financeiro
									For nZ := 1 to oModelG48B:Length()
										oModelG48B:GoLine( nZ )
	
										If !oModelG48B:IsDeleted() .And. !Empty( oModelG48B:GetValue("G48_APLICA") ) .And. oModelG48B:GetValue("G48_APLICA") == oModelG4CB:GetValue("G4C_APLICA")
											lContinua := lContinua .And. oModelG48B:SetValue("G48_CODCON", "")
											lContinua := lContinua .And. oModelG48B:SetValue("G48_STATUS", "1")
										EndIf
									Next
	
								EndIf
	
							Next
	
						Next
	
					Next
	
					oMldTura39:SetValue("G6J_MASTER","G6J_EFETIV", "2")
					oMldTura39:SetValue("G6J_MASTER","G6J_DTEFET", STOD("") )
					If oMldTura39:VldData()
						oMldTura39:CommitData()
					Else
						JurShowErro(oMldTura39:GetModel():GetErrorMessage())
						lContinua := .F.
					EndIf
	
					For nJ := 1 to oModelG6I:Length()
						oModelG6I:GoLine( nJ )
					
						For nX := 1 to oModelG3R:Length()
							oModelG3R:GoLine( nX )
			
							If oModelG3R:GetValue("G3R_STATUS") $ '2|4' //Particionado|Cancelado
								Loop
							EndIf
		
							TurAtuStRV( oModelG3R:GetValue("G3R_FILIAL")	,;
										oModelG3R:GetValue("G3R_NUMID")	,; 
										oModelG3R:GetValue("G3R_IDITEM")	,; 
										oModelG3R:GetValue("G3R_NUMSEQ")	,; 
										''									,;
										'3'									,;
										.F. 								)
					
						Next
							
					Next
					
					oMldTura39:DeActivate()
	
				EndIf
				G6J->(DbSkip())
	
			EndDo
	
		EndIf
	
	EndIf
	
	If lContinua
		T039ACtExc(.T., lMostrLanc, lAglutLanc, "TURA039")
		If !IsBlind()
			MsgInfo(STR0028)	//"Estorno realizado com sucesso."
		EndIf
	Else
		DisarmTransaction()
		Break
	EndIf
END TRANSACTION

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039DelAcert
Fun��o que desfaz os acertos no momento da exclus�o.

@type function
@author Enaldo Cardoso Junior
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA039DelAcert(oModel, cNumId, cIdItem, cSeqAcerto, cConcil)

Local oMdlG3Q := oModel:GetModel("G3Q_ITENS")
Local oMdlG3R := oModel:GetModel("G3R_ITENS")
Local aLines := FwSaveRows(oModel)
Local lRet 	 := .F.
Local nX	 := 0

For nX := 1 To oMdlG3Q:Length()
	oMdlG3Q:GoLine(nX)
	If oMdlG3R:GetValue("G3R_NUMID") == cNumId .And. oMdlG3R:GetValue("G3R_IDITEM") == cIdItem .And. oMdlG3R:GetValue("G3R_SEQACR") == cSeqAcerto .And. oMdlG3R:GetValue("G3R_CONCIL") == cConcil
		lRet 	:= .T.
		oMdlG3Q:DeleteLine()
		oModel:GetModel("G3R_ITENS"):DeleteLine()
	EndIf
Next nX

FwRestRows(aLines)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039VlToF
Busca valor total da fatura.

@type function
@author Enaldo Cardoso Junior
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function TA039VlToF(cFatura, cCodFor, cLojFor)
	
Local aArea   := GetArea()
Local oModel  := FwLoadModel("TURA039A")
Local nAux	  := 0
Local nValFat := 0

oModel:SetOperation(MODEL_OPERATION_UPDATE)

G6J->(DbSetOrder(2))	//G6J_FILIAL+G6J_FATURA+G6J_CPIATA
G6H->(DbSetOrder(1))	//G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA
G6J->(DbSeek(xFilial("G6J") + cFatura))
While G6J->(!Eof()) .And. G6J->(G6J_FILIAL + G6J_FATURA) == xFilial("G6J") + cFatura
	If oModel:Activate()
		If (G6H->(DbSeek(xFilial("G6H") + G6J->G6J_FATURA)))
			nAux := 0
			TA039ADVal( , , @nAux, , G6H->G6H_FORNEC, G6H->G6H_LOJA)
			nValFat += nAux
			oModel:DeActivate()
		EndIf
	EndIf
	G6J->(DbSkip())
EndDo
G6J->(DbCloseArea())
G6H->(DbCloseArea())

//Checa se h� valor de Meta conciliada para compor o total da fatura
nValFat += TA39MTtlConcil(cFatura, cCodFor, cLojFor)

RestArea(aArea)

Return nValFat

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TurLApr
Valida as cores da legenda

@type Function
@author Enaldo Cardoso Junior
@since 16/05/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TurLApr()

Local cLegenda := ""

If G6J->G6J_APROVA == "1"
	cLegenda := "BR_VERMELHO"
ElseIf G6J->G6J_APROVA == "2"
	cLegenda := "BR_AMARELO"
ElseIf G6J->G6J_APROVA== "3"
	cLegenda := "BR_VERDE"
EndIf

Return cLegenda

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TurAprCor
Cores da legenda

@type Function
@author Enaldo Cardoso Junior
@since 16/05/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TurAprCor()

Local oLegenda :=  FWLegend():New()

oLegenda:Add("", "BR_VERDE"	  , STR0046)	//"Aprovado Total"
oLegenda:Add("", "BR_AMARELO" , STR0047)	//"Aprovado Parcial"
oLegenda:Add("", "BR_VERMELHO", STR0048)	//"N�o Aprovado"

oLegenda:View()
DelClassIntf()

Return .T.

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039TrRec
Fun��o que atualiza os status dos acordos 

@type Function
@author Enaldo Cardoso Junior
@since 16/05/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TA039TrRec(lMark)

Local aArea      := GetArea()
Local oModel 	 := FwModelActive()
Local oModelG3R	 := oModel:GetModel("G3R_ITENS")
Local oModelG4CB := oModel:GetModel("G4CB_ITENS")
Local oModelG48B := oModel:GetModel("G48B_ITENS")
Local aLines	 := FwSaveRows()
Local nX		 := 0
Local cAplicacao := ""
Local cStatus	 := ""
Local lCanDelG48 := oModelG48B:CanDeleteLine()

Default lMark	 := .F.

If oModelG48B:IsEmpty()
	Return
ElseIf Empty(oModelG48B:GetValue("G48_CODACD"))
	If !lCanDelG48
		oModelG48B:SetNoDeleteLine(.F.)
	EndIf
	oModelG48B:DeleteLine()
	If !lCanDelG48
		oModelG48B:SetNoDeleteLine(.T.)
	EndIf
	Return	
EndIf

cAplicacao := oModelG48B:GetValue("G48_APLICA")
cStatus	   := IIF(oModelG48B:GetValue("G48_RECCON"), "1", "2")

If lMark
	//Status 4 somente usado quando os itens s�o for�ados para ser marcados ou desmarcados
	cStatus := "4"
EndIf

//Se acordo cancelado, apenas desmarca
If oModelG48B:GetValue("G48_STATUS") == "3"
	oModelG48B:SetValue("G48_RECCON", .F.)
	Return

//Se o item j� est� em apura��o, ser� sempre desmarcado
ElseIf !Empty(oModelG48B:GetValue("G48_CODAPU"))
	oModelG48B:SetValue("G48_RECCON", .F.)
	cStatus := "2"

//Se acordo manual e acerto autom�tico ou classifica��o C11, for�a a ser sempre marcado
ElseIf (oModelG48B:GetValue("G48_INTERV") == "1" .And. oModelG3R:GetValue("G3R_ACRAUT") == "1") .Or. oModelG48B:GetValue("G48_CLASS") == "C11"
	oModelG48B:SetValue("G48_RECCON", .T.)
	cStatus := "1"
EndIf

If cStatus == "2"
	oModelG48B:SetValue("G48_COMSER", "2")
	oModelG48B:SetValue("G48_RECCON", .F.)
Else
	G4W->(DbSetOrder(1))		// G4W_FILIAL+G4W_CODACO+G4W_CODREV+G4W_MSBLQL
	If G4W->(DbSeek(xFilial("G4W") + oModelG48B:GetValue("G48_CODACD") + oModelG48B:GetValue("G48_CODREC")))
		If G4W->G4W_DESCFT == "1" //Na Fatura?
			oModelG48B:SetValue("G48_COMSER", "1")
			If lMark
				oModelG48B:SetValue("G48_RECCON", .T.)
				cStatus := "1"
			EndIf

			If oModelG3R:GetValue("G3R_ACERTO") == "1" .And. oModelG3R:GetValue("G3R_ACRAUT") == "1"
				If oModelG4CB:SeekLine({{"G4C_APLICA", oModelG48B:GetValue("G48_APLICA")}})
					oModelG4CB:SetValue("G4C_TPFOP", "1")
				EndIf
			EndIf
		Else
			oModelG48B:SetValue("G48_COMSER", "2")
			If lMark
				oModelG48B:SetValue("G48_RECCON", .F.)
				cStatus := "2"
			EndIf
		EndIf
	Else
		If Empty(oModelG48B:GetValue("G48_CODACD"))		//Se n�o � o valor do servi�o, coloca para ag. libera��o
			cStatus := "1"
		EndIf
	EndIf
EndIf

For nX := 1 To oModelG4CB:Length()
	oModelG4CB:GoLine(nX)
	If oModelG4CB:GetValue("G4C_APLICA") == cAplicacao .And. oModelG4CB:GetValue("G4C_STATUS") $ "1|2"
		oModelG4CB:SetValue("G4C_STATUS", cStatus)
	EndIf
Next

FwRestRows(aLines)

RestArea(aArea)

Return

//------------------------------------------------------------------------
/*/{Protheus.doc} T039ACtInc(lOnLine,lMostrLanc,lAglutLanc)
Contabiliza��o da rotina de inclus�o.

@sample 	T042ACtInc(oModel)
@return
@author  	Simone Mie Sato Kakinoana
@since   	21/04/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T039ACtInc(lOnLine,lMostrLanc,lAglutLanc,cRotinaOri)

Local aArea       := GetArea()
Local aFlagCTB 	  := {}
Local nPosReg	  := 0
Local dDtEfetiva  := dDataBase
Local cPadT30	  := "T30"
Local cPadT32     := "T32"
Local cPadT34     := "T34"
Local cPadT36     := "T36"
Local cLoteTur	  := LoteCont("TUR")
Local cAliasG4C   := ""
Local cFatura	  := G6I->G6I_FATURA
Local cOpera	  := ""
Local cFiltTipo	  := "% "
Local lPadT30	  := VerPadrao(cPadT30)
Local lPadT32	  := VerPadrao(cPadT32)
Local lPadT34	  := VerPadrao(cPadT34)
Local lPadT36	  := VerPadrao(cPadT36)
Local lUsaFlag	  := SuperGetMV("MV_CTBFLAG", .T. /*lHelp*/, .F. /*cPadrao*/)

Private cArquivo  := ""
Private nHdlPrv	  := 0
Private nTotal	  := 0
Private LanceiCtb := .F.

nHdlPrv	  := HeadProva(cLoteTur, cRotinaOri, Substr(cUsuario, 7, 6), @cArquivo)
cAliasG4C := GetNextAlias()

If G6I->G6I_TIPOIT == "3"
	cRotinaOri := "TURA039R"
EndIf

cFiltTipo += " AND (G3R_OPERAC = '1' OR G3R_OPERAC = '2' OR G3R_OPERAC ='3') "
cFiltTipo += " %"

BeginSql Alias cAliasG4C
	SELECT G4C.R_E_C_N_O_ RECNOG4C, G3R.R_E_C_N_O_ RECNOG3R, G6I.R_E_C_N_O_ RECNOG6I, G6J.R_E_C_N_O_ RECNOG6J, G3R_OPERAC, G4C.*, G6J_DTEFET, G6I_FATURA, G6I_ITEM 
	FROM %Table:G6H% G6H
	INNER JOIN %Table:G6I% G6I ON G6I_FILIAL = G6H_FILIAL AND G6I_FATURA = G6H_FATURA AND G6I.%notDel%
    INNER JOIN %Table:G6J% G6J ON G6J_FILIAL = G6I_FILIAL AND G6J_CONCIL = G6I_CONCIL AND G6J.%notDel%
	INNER JOIN %Table:G3R% G3R ON G3R_FILCON = G6I_FILIAL AND G3R_CONCIL = G6I_CONCIL AND G3R_CONINU = "" AND G3R.G3R_TPSEG = "1" AND G3R.%notDel% %Exp:cFiltTipo% 
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID  = G3R_NUMID  AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ AND G4C_CLIFOR = "2" AND G4C_CODAPU = "" AND G4C_LACONC <> "S" AND G4C.G4C_TPCONC IN ("1", "3") AND G4C_CONINU = "" AND G4C.%notDel%
    WHERE G6H_FILIAL = %xFilial:G6H% AND G6H_FATURA = %Exp:cFatura% AND G6H_EFETIV = "1" AND G6H.%notDel%
EndSql

While (cAliasG4C)->(!Eof())
	G3Q->(DbSetOrder(1))	// G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM+G3Q_NUMSEQ+G3Q_CONINU
	G3R->(DbSetOrder(1)) 	// G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ+G3R_CONINU
	G48->(DbSetOrder(1))	// G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD
	G4C->(DbSetOrder(2))	// G4C_FILIAL+G4C_IDIF+G4C_CONINU
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA

	G3R->(DbGoto((cAliasG4C)->RECNOG3R))
	G4C->(DbGoto((cAliasG4C)->RECNOG4C))
	G6I->(DbGoto((cAliasG4C)->RECNOG6I))
	G6J->(DbGoto((cAliasG4C)->RECNOG6J))
	If G3Q->(DbSeek(xFilial("G3Q") + (cAliasG4C)->G4C_NUMID + (cAliasG4C)->G4C_IDITEM + (cAliasG4C)->G4C_NUMSEQ))
		//Verifica se existe bloqueio por processo.
		If !CtbValiDt( , G6J->G6J_DTEFET, .F., , , {"TUR001"}, ) 	//VERIFICAR SE SER� G3Q_EMISS, G3Q_DTINC
			(cAliasG4C)->(DbSkip())
			Loop
		EndIf
	EndIf

	If (cAliasG4C)->G4C_TIPO == "1"	 //1=Venda
		If lPadT30
			If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
				aAdd(aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->(Recno()), 0, 0, 0})
			EndIf
			nTotal += DetProva(nHdlPrv, cPadT30, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "2/5" //2=Reembolso;5=Breakage
		If lPadT32
			If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
				aAdd(aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->(Recno()), 0, 0, 0})
			EndIf
			nTotal += DetProva(nHdlPrv, cPadT32, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "3" //3=Receita
		If G48->(DbSeek(xFilial("G48") + (cAliasG4C)->G4C_NUMID + (cAliasG4C)->G4C_IDITEM + (cAliasG4C)->G4C_NUMSEQ + (cAliasG4C)->G4C_APLICA + (cAliasG4C)->G4C_NUMACD))
			If lPadT34
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd(aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->(Recno()), 0, 0, 0})
				EndIf
				nTotal += DetProva(nHdlPrv, cPadT34, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
			EndIf
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "4" //4=Abat. Receita
		If G48->(DbSeek(xFilial("G48")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ+(cAliasG4C)->G4C_APLICA+(cAliasG4C)->G4C_NUMACD))
			If lPadT36
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd(aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->(Recno()), 0, 0, 0})
				EndIf
				nTotal += DetProva(nHdlPrv, cPadT36, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
			EndIf
		EndIf
	EndIf

	If LanceiCtb // Vem do DetProva
		If !lUsaFlag
			RecLock("G4C")
			G4C->G4C_LACONC := "S"
			G4C->(MsUnlock())
		EndIf
	ElseIf lUsaFlag
		If (nPosReg := aScan(aFlagCTB, {|x| x[4] == G4C->(Recno())})) > 0
			aFlagCTB := aDel(aFlagCTB, nPosReg)
			aFlagCTB := aSize(aFlagCTB, Len(aFlagCTB) - 1)
		EndIf
	EndIf
	(cAliasG4C)->(DbSkip())
EndDo
(cAliasG4C)->(DbCloseArea())

If nHdlPrv > 0 .And. nTotal > 0
	RodaProva(nHdlPrv,nTotal)
	cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
	cArquivo := ""
	nHdlPrv	 := 0
	nTotal	 := 0
EndIf

aFlagCTB := {}
RestArea(aArea)

Return

//------------------------------------------------------------------------
/*/{Protheus.doc} T042ACtExc(lOnLine,lMostrLanc,lAglutLanc)
Contabiliza��o da rotina de exclus�o.

@sample 	T042ACtExc(oModel)
@return
@author  	Simone Mie Sato Kakinoana
@since   	22/04/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T039ACtExc(lOnLine, lMostrLanc, lAglutLanc, cRotinaOri)

Local aArea      := GetArea()
Local aFlagCTB 	 := {}
Local nPosReg	 := 0
Local dDtEfetiva := dDataBase
Local cPadT31	 := "T31"
Local cPadT33	 := "T33"
Local cPadT35	 := "T35"
Local cPadT37	 := "T37"
Local cLoteTur	 := LoteCont("TUR")
Local cAliasG4C	 := ""
Local cOpera	 := ""
Local cFiltTipo	 := "% "
Local lPadT31	 := VerPadrao(cPadT31)
Local lPadT33	 := VerPadrao(cPadT33)
Local lPadT35	 := VerPadrao(cPadT35)
Local lPadT37	 := VerPadrao(cPadT37)
Local lUsaFlag	 := SuperGetMV("MV_CTBFLAG", .T. /*lHelp*/, .F. /*cPadrao*/)

Private cArquivo  := ""
Private nHdlPrv	  := 0
Private nTotal	  := 0
Private LanceiCtb := .F.

nHdlPrv	  := HeadProva(cLoteTur, cRotinaOri, Substr(cUsuario, 7, 6), @cArquivo)
cAliasG4C := GetNextAlias()

If G6I->G6I_TIPOIT == "3"
	cRotinaOri := "TURA039R"
EndIf

cFiltTipo += " AND (G3R_OPERAC ='1' OR G3R_OPERAC = '2' OR G3R_OPERAC = '3')"
cFiltTipo += " %"

BeginSql Alias cAliasG4C
	SELECT G4C.R_E_C_N_O_ RECNOG4C, G3R.R_E_C_N_O_ RECNOG3R, G6I.R_E_C_N_O_ RECNOG6I, G6J.R_E_C_N_O_ RECNOG6J, G3R_OPERAC, G4C.*
	FROM %Table:G6H% G6H
	INNER JOIN %Table:G6I% G6I ON G6I_FILIAL = G6H_FILIAL AND G6I_FATURA = G6H_FATURA AND G6I.%notDel%
	INNER JOIN %Table:G6J% G6J ON G6J_FILIAL = G6I_FILIAL AND G6J_CONCIL = G6I_CONCIL AND G6J.%notDel%
	INNER JOIN %Table:G3R% G3R ON G3R_FILCON = G6I_FILIAL AND G3R_CONCIL = G6I_CONCIL AND G3R_CONINU = "" AND G3R.G3R_TPSEG = "1" %Exp:cFiltTipo% AND G3R.%notDel%
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL AND 
	                              G4C_NUMID  = G3R_NUMID  AND 
	                              G4C_IDITEM = G3R_IDITEM AND 
	                              G4C_NUMSEQ = G3R_NUMSEQ AND	
	                              G4C_CLIFOR = "2" AND 
	                              G4C_CODAPU = ""  AND 
	                              G4C_LACONC = "S" AND 
	                              G4C_CONINU = ""  AND 
	                              G4C.%notDel% 
	WHERE G6H_FILIAL = %xFilial:G6H% AND G6H_FATURA = %Exp:G6I->G6I_FATURA% AND G6H_EFETIV = "2" AND G6H.%notDel%
EndSql

While (cAliasG4C)->(!Eof())
	G3Q->(DbSetOrder(1))	// G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM+G3Q_NUMSEQ+G3Q_CONINU
	G3R->(DbSetOrder(1)) 	// G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ+G3R_CONINU
	G48->(DbSetOrder(1))	// G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD
	G4C->(DbSetOrder(2))	// G4C_FILIAL+G4C_IDIF+G4C_CONINU
	G6I->(DbSetOrder(1))	// G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
	G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA

	G3R->(DbGoto((cAliasG4C)->RECNOG3R))
	G4C->(DbGoto((cAliasG4C)->RECNOG4C))
	G6I->(DbGoto((cAliasG4C)->RECNOG6I))
	G6J->(DbGoto((cAliasG4C)->RECNOG6J))
	If G3Q->(DbSeek(xFilial("G3Q")+ (cAliasG4C)->G4C_NUMID + (cAliasG4C)->G4C_IDITEM + (cAliasG4C)->G4C_NUMSEQ))
		//Verifica se existe bloqueio por processo.
		If !CtbValiDt( , G8C->G8C_DTEFET, , , , {"TUR001"}, )	//VERIFICAR SE SER� G3Q_EMISS, G3Q_DTINC
			(cAliasG4C)->(DbSkip())
			Loop
		EndIf
	EndIf

	If (cAliasG4C)->G4C_TIPO == "1"	 //1=Venda
		If lPadT31
			If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
				aAdd(aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->(Recno()), 0, 0, 0})
			EndIf
			nTotal += DetProva(nHdlPrv, cPadT31, cRotinaOri, cLoteTur, , , , , ,  , , @aFlagCTB)
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "2/5" //2=Reembolso;5=Breakage
		If lPadT33
			If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
				aAdd(aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->(Recno()), 0, 0, 0})
			EndIf
			nTotal += DetProva(nHdlPrv, cPadT33, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "3" //3=Receita
		If G48->(DbSeek(xFilial("G48") + (cAliasG4C)->G4C_NUMID + (cAliasG4C)->G4C_IDITEM + (cAliasG4C)->G4C_NUMSEQ + (cAliasG4C)->G4C_APLICA + (cAliasG4C)->G4C_NUMACD))
			If lPadT35
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd(aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0})
				EndIf
				nTotal += DetProva(nHdlPrv, cPadT35, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
			EndIf
		EndIf

	ElseIf (cAliasG4C)->G4C_TIPO $ "4" //4=Abat. Receita
		If G48->(DbSeek(xFilial("G48") + (cAliasG4C)->G4C_NUMID + (cAliasG4C)->G4C_IDITEM + (cAliasG4C)->G4C_NUMSEQ + (cAliasG4C)->G4C_APLICA + (cAliasG4C)->G4C_NUMACD))
			If lPadT37
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd(aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0})
				EndIf
				nTotal += DetProva(nHdlPrv, cPadT37, cRotinaOri, cLoteTur, , , , , , , , @aFlagCTB)
			EndIf
		EndIf
	EndIf

	If LanceiCtb // Vem do DetProva
		If !lUsaFlag
			RecLock("G4C")
			G4C->G4C_LACONC := " "
			G4C->(MsUnlock())
		EndIf
	ElseIf lUsaFlag
		If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G4C->(Recno()) }))>0
			aFlagCTB := Adel(aFlagCTB,nPosReg)
			aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
		EndIf
	EndIf

	(cAliasG4C)->(DbSkip())
EndDo
(cAliasG4C)->(DbCloseArea())

If nHdlPrv > 0 .And. nTotal > 0
	RodaProva(nHdlPrv, nTotal)
	cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc, , , , @aFlagCTB, , )
	cArquivo := ""
	nHdlPrv	 := 0
	nTotal	 := 0
EndIf

aFlagCTB := {}
RestArea(aArea)

Return

//------------------------------------------------------------------------
/*/{Protheus.doc} T39GetVlrLiq
Retorna o valor l�quido

@sample 	T39GetVlrLiq()
@return
@author  	Simone Mie Sato Kakinoana
@since   	22/04/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T39GetVlrLiq()
Local oModel := FwModelActive()
Return oModel:GetValue("VLRLIQ_G3R", "XXX_VLRLIQ")

//------------------------------------------------------------------------
/*/{Protheus.doc} T39VldG4C

@sample 	T39VldG4C(cFatura, cMsg)
@return
@author  	Simone Mie Sato Kakinoana
@since   	22/04/2016
@version  	12.1.8
/*/
//------------------------------------------------------------------------------------------
Function T39VldG4C(cFatura, cMsg)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias()
Local lRet		:= .T.

Default cMsg 	:= ""

BeginSql Alias cAliasAux
	SELECT G4C_CLIFOR, G4C_STATUS, G4C_CODAPU, G4C_IDIF
	FROM %Table:G6H% G6H
	INNER JOIN %Table:G3R% G3R ON G3R_FILCON = G6H_FILIAL AND G3R_FATURA = G6H_FATURA AND G3R.%notDel%
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID = G3R_NUMID AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ AND G4C_TPCONC = "1" AND G4C_CLIFOR = "1" AND G4C.%notDel%
	WHERE G6H_FILIAL = %xFilial:G6H% AND G6H_FATURA = %Exp:cFatura% AND G6H.%notDel%
EndSql

While (cAliasAux)->(!Eof())
	If (cAliasAux)->G4C_STATUS == "4"
		cMsg += I18N(STR0087 + CRLF, {(cAliasAux)->G4C_IDIF}) //"Item Financeiro #1[ Item financeiro ]# j� finalizado."
		lRet := .F.
	EndIf

	If !Empty((cAliasAux)->G4C_CODAPU)
		cMsg += I18N(STR0088 + CRLF, {(cAliasAux)->G4C_IDIF, (cAliasAux)->G4C_CODAPU}) //"Item Financeiro #1[ Item financeiro ]# faz parte da apura��o #2[ Num. Apura��o ]#."
		lRet := .F.
	EndIf
	(cAliasAux)->(DbSkip())
EndDo
(cAliasAux)->(DbCloseArea())

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39UpdRepas

Fun��o respons�vel por atualizar o status dos itens financeiros de repasse. Ser� analisado o item principal, caso
o item principal esteja com o status "4" (Finalizado), ent�o os itens filhos de repasse ser�o gerados com status
"2" (Ag. Apura��o). Do contr�rio, possuir�o o mesmo status do item pai.

@type function
@author Fernando Radu Muscalu
@since 13/10/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA39UpdRepas(oModelG4CA) 

Local cNewStat := ""
Local nZ	   := 0
Local nLine    := oModelG4CA:GetLine()

If !oModelG4CA:IsEmpty()
	If Empty(oModelG4CA:GetValue("G4C_IFPRIN"))		//Se n�o possui Item Principal, altera para aguardando apura��o
		cNewStat := "2"	//Ag. Apura��o
	ElseIf oModelG4CA:SeekLine({{"G4C_IFPRIN",Space(TamSx3("G4C_IFPRIN")[1])}})		//Busca o Item Principal de Repasse
		If oModelG4CA:GetValue("G4C_STATUS") == "4" //Se o item principal foi finalizado, o repasse ir� para:
			cNewStat := "2"	//Ag. Apura��o
		Else
			cNewStat := oModelG4CA:GetValue("G4C_STATUS") 	
		EndIf
	EndIf
EndIf

oModelG4CA:GoLine(nLine)

Return cNewStat

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039VMrAll
Fun��o para chamada da fun��o de marca todos com tela de processamento

@type function
@author Anderson Toledo
@since 26/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T039VMrAll()
Return FWMsgRun( , {|| Tur039MAll()}, , STR0101) //"Aguarde, marcando itens..."

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039VMrAll
Fun��o para marcar itens para aprova��o de acordo com status e crit�rio de sele��o

@type function
@author Anderson Toledo
@since 26/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function Tur039MAll()

Local oModel	:= FwModelActive()
Local oMdlG6I	:= oModel:GetModel("G6I_ITENS")	
Local oMdl039H	:= T39IniMdEr(oMdlG6I:GetValue("G6I_CONCIL"))
Local cMsg		:= ""
Local cError	:= ""
Local lDivFop 	:= .F.
Local lNoNatur	:= .F.
Local lDivVlLiq	:= .F. 
Local nOpc 		:= 0
Local nX		:= 0

FwModelActive(oModel)

cMsg := STR0102 + CRLF //"Op��es dispon�veis:" 
cMsg += STR0103 + CRLF //"1) Cancela a marca��o dos itens"
cMsg += STR0104 + CRLF //"2) Marca todos os itens em aberto, incluindo itens com acertos"
cMsg += STR0105 + CRLF //"3) Marca todos os itens em aberto que n�o possuem acertos gerados"

nOpc := AVISO(STR0106, cMsg, {STR0107, STR0108, STR0109}, 2) //"Marcar itens"###"1-Cancelar"###"3-Sem acerto"

If nOpc > 1
	For nX := 1 to oMdlG6I:Length()
		oMdlG6I:GoLine(nX)
		If  oMdlG6I:GetValue("G6I_SITUAC") == "2"			
			
			If nOpc == 3 .And. oMdlG6I:GetValue("G6I_ACERTO") == "1"
				Loop
			
			ElseIf oMdlG6I:GetValue("G6I_DIVFOP") > "0"
				lDivFop := .T.
				T39AddError(oMdl039H, oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ"), STR0120) //"Diverg�ncia de FOP/Moeda"

			ElseIf !T39VldNatur(oModel, @cError)	
				lNoNatur := .T.
				T39AddError(oMdl039H, oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ"), SubStr(STR0098 + ": " + cError, 1, 200))	 //"Informe as naturezas solicitadas."

			Else
				If IIF((oMdlG6I:GetValue("G6I_TIPOIT") $ "3|5" .And. oMdlG6I:GetValue("G6I_TPOPER") == "2") .Or. (!oMdlG6I:GetValue('G6I_TIPOIT') $ "3|5" .And. oMdlG6I:GetValue("G6I_TPOPER") == "1"), oMdlG6I:GetValue("G6I_VLRLIQ"), oMdlG6I:GetValue("G6I_VLRLIQ") * -1) <> T39GetVlrLiq()
					lDivVlLiq := .T.
					T39AddError(oMdl039H, oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ"), STR0117) //"Diverg�ncia de valor l�quido"
				EndIf
				oMdlG6I:SetValue("TMP_OK", .T.)
			EndIf
		EndIf
	Next nX
EndIf

If lDivFop .Or. lNoNatur .Or. lDivVlLiq 
	cMsg := ""
	
	If lDivFop
		cMsg += STR0110 + CRLF //" Alguns itens n�o foram marcados por estarem com diverg�ncia de FOP."
	EndIf
	
	If lNoNatur 	
		cMsg += STR0111 + CRLF //" Alguns itens n�o foram marcados por n�o conter todas as naturezas informadas."
	EndIf
	
	If lDivVlLiq
		cMsg += STR0112 + CRLF //" Foi detectado registros com diverg�ncia no valor l�quido."
	EndIf
	
	If Aviso(STR0113, cMsg + STR0123, {STR0001, STR0119}, 2) == 1 //"Aten��o"###"Deseja visualizar as diverg�ncias?"###"Visualizar"###"Sair"###"Sair" 
		T39ShowErr(oMdl039H)
	EndIf
EndIf

oMdl039H:DeActivate()
oMdl039H:Destroy()

FwModelActive(oModel)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39LibIfCl
Fun��o para liberar ou estornar a libera��o dos itens financeiros do cliente

@type function
@author Anderson Toledo
@since 28/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39LibIfCl(oModel, lLibera)

Local oModelG3R := oModel:GetModel("G3R_ITENS")
Local lContinua := .T.
Local nX		:= 0
Local nY		:= 0

If lLibera
	lContinua := T39UpdG4C(oModelG3R:GetValue("G3R_MSFIL"), oModelG3R:GetValue("G3R_NUMID"), oModelG3R:GetValue("G3R_IDITEM"), {{"G4C_STATUS", "3"}, {"G4C_TPCONC", "3"}, {"G4C_DTLIB", dDataBase}}, {"G4C_STATUS = '1'", "G4C_CLASS <> 'C07'", "G4C_CLIFOR = '1'"})
Else
	lContinua := T39UpdG4C(oModelG3R:GetValue("G3R_MSFIL"), oModelG3R:GetValue("G3R_NUMID"), oModelG3R:GetValue("G3R_IDITEM"), {{"G4C_STATUS", "1"}, {"G4C_TPCONC", "1"}, {"G4C_DTLIB", CToD("  /  /  ")}}, {"G4C_STATUS = '3'", "G4C_TPCONC = '3'", "G4C_CLASS <> 'C07'", "G4C_CLIFOR = '1'" })
EndIf	

Return lContinua

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39UpdLibIf

@type function
@author Anderson Toledo
@since 28/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39UpdLibIf(cMsFil, cNumId, cIdItem, aFldUpd, aFldCond)

Local cSts  := ""
Local nX    := 0

cSts := "UPDATE " + RetSqlName("G4C")
cSts += 	" SET "

For nX := 1 To Len(aFldUpd) 
	If nX > 1
		cSts += ", "
	EndIf
	cSts += aFldUpd[nX, 1] + " = " + aFldUpd[nX, 2] 
Next

cSts += " WHERE G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_IDIF "
cSts += 	" IN (SELECT G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_IDIF"
cSts += 		" FROM " + RetSqlName("G3R") + " G3R"
cSts += 		" INNER JOIN  " + RetSqlName("G4C") + " G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID = G3R_NUMID AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ AND " + IIF(TCSrvType() == "AS/400", " G4C.@DELETED@ = ' ' ", " G4C.D_E_L_E_T_ = ' ' ")

For nX := 1 To Len(aFldCond) 
	cSts += " AND " + aFldCond[nX] 
Next

cSts += 		" WHERE G3R_MSFIL = '" + cMsFil + "' AND G3R_NUMID = '" + cNumId + "' AND G3R_IDITEM = '" + cIdItem + "' AND " + IIF(TCSrvType() == "AS/400", " G3R.@DELETED@ = ' ')", " G3R.D_E_L_E_T_ = ' ')")	
	
Return TcSqlExec(cSts) >= 0

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39UpdG4C

@type function
@author Anderson Toledo
@since 28/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39UpdG4C(cMsFil, cNumId, cIdItem, aFldUpd, aFldCond)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias() 
Local cWhere    := "% "
Local nX        := 0

For nX := 1 To Len(aFldCond) 
	cWhere += " AND " + aFldCond[nX] 
Next
cWhere += " %"

BeginSql Alias cAliasAux
	SELECT G4C.R_E_C_N_O_ RECNO
	FROM %Table:G3R% G3R
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID = G3R_NUMID AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ AND G4C.%notDel%
	WHERE G3R_MSFIL = %Exp:cMsFil% AND G3R_NUMID = %Exp:cNumId% AND G3R_IDITEM = %Exp:cIdItem% AND G3R.%notDel% %Exp:cWhere%
EndSql

While (cAliasAux)->(!Eof())
	G4C->(DbGoTo((cAliasAux)->RECNO))
	RecLock("G4C", .F.)
	For nX := 1 To Len(aFldUpd) 
		G4C->&(aFldUpd[nX, 1]) := aFldUpd[nX, 2]
	Next nX	
	G4C->(MsUnLock())
	(cAliasAux)->(DbSkip())
EndDo
(cAliasAux)->(DbCloseArea())

Return .T.

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39LibIfEn
Fun��o para verificar se algum item liberado na aprova��o j� foi encerrado

@type function
@author Anderson Toledo
@since 28/12/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T39LibIfEn(oModel, lQuery)

Local aArea      := GetArea()
Local oModelG3R  := oModel:GetModel("G3R_ITENS")
Local oModelG4CA := Nil
Local cAliasAux	 := ""
Local lRet 		 := .F.
Local nY 		 := 0
Local nZ 		 := 0

Default lQuery   := .F.

If lQuery
	cAliasAux := GetNextAlias()
	BeginSql Alias cAliasAux
		SELECT COUNT(*) TOTAL
		FROM %Table:G3R% G3R
		INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL AND G4C_NUMID = G3R_NUMID AND G4C_IDITEM = G3R_IDITEM AND G4C_NUMSEQ = G3R_NUMSEQ AND G4C_TPCONC = '3' AND (G4C_STATUS = '4' OR G4C_CODAPU <> ' ') AND G4C_CLIFOR = '1' AND G4C.%notDel%
		WHERE G3R_MSFIL = %Exp:oModelG3R:GetValue("G3R_MSFIL")% AND G3R_NUMID = %Exp:oModelG3R:GetValue("G3R_NUMID")% AND G3R_IDITEM = %Exp:oModelG3R:GetValue("G3R_IDITEM")% AND G3R.%notDel%
	EndSql
	lRet := (cAliasAux)->TOTAL > 0
	(cAliasAux)->(DbCloseArea())
	
Else	
	oModelG4CA := oModel:GetModel("G4CA_ITENS")
	For nY := 1 To oModelG3R:Length()
		oModelG3R:GoLine(nY)
		For nZ := 1 To oModelG4CA:Length()
			If oModelG4CA:GetValue("G4C_TPCONC", nZ) == "3" .And. (oModelG4CA:GetValue("G4C_STATUS", nZ) == "4" .Or. !Empty(oModelG4CA:GetValue("G4C_CODAPU", nZ)))
				lRet := .T.
				Exit
			EndIf
		Next
							
		If lRet
			Exit
		EndIf
	Next
EndIf
	
RestArea(aArea)

Return lRet	  

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IniMdEr
Fun��o para inicializar o model de log das diverg�ncias

@type function
@author Anderson Toledo
@since 05/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39IniMdEr(cConcil)
	
Local oModel := FwLoadModel("TURA039H")

oModel:SetOperation(MODEL_OPERATION_INSERT)
oModel:Activate()

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.F.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.F.)
oModel:GetModel("TMP_DETAIL"):SetNoUpdateLine(.F.)
	
oModel:SetValue("TMP_MASTER", "TMP_CONCIL", cConcil)
	
Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39AddError
Fun��o para adicionar um item de diverg�ncia no log

@type function
@author Anderson Toledo
@since 05/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39AddError(oMdl039H, cItemFat, cNumId, cIdItem, cNumSeq, cMsg)

Local oMdlGrid := oMdl039H:GetModel("TMP_DETAIL")

If !oMdlGrid:IsEmpty()
	oMdlGrid:AddLine()
EndIf

oMdlGrid:LoadValue("G6I_ITEM"  , cItemFat) 
oMdlGrid:LoadValue("G6I_NUMID" , cNumId)
oMdlGrid:LoadValue("G6I_IDITEM", cIdItem) 
oMdlGrid:LoadValue("G6I_NUMSEQ", cNumSeq)
oMdlGrid:LoadValue("TMP_MSG"   , cMsg)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ShowErr
Fun��o para apresentar o log das diverg�ncias

@type function
@author Anderson Toledo
@since 05/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39ShowErr(oMdl039H)

Local aButtons := {{.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.T., STR0004}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}, {.F., Nil}}			//"Fechar"

oMdl039H:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oMdl039H:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)
oMdl039H:GetModel("TMP_DETAIL"):SetNoUpdateLine(.T.)

FWExecView(STR0008, "TURA039H", 1, , {|| .T.}, , , aButtons, , , , oMdl039H)	

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039AltRV
Fun��o para altera��o de registro de venda que est� em concilia��o

@type function
@author Anderson Toledo
@since 06/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T039AltRV()

Local aArea		:= GetArea()
Local aAreaG6J  := G6J->(GetArea())
Local oModel	:= FwModelActive()
Local oMdlG6J	:= oModel:GetModel("G6J_MASTER")
Local oMdlG6I	:= oModel:GetModel("G6I_ITENS")
Local oMdlG3R	:= oModel:GetModel("G3R_ITENS")
Local oMdlG3Q	:= oModel:GetModel("G3Q_ITENS")
Local oMdlT34	:= Nil
Local aRV		:= {}
Local aDadosUsr	:= {}
Local aManAss	:= {} 
Local aItemG6I	:= {}
local cBkpFil	:= cFilAnt
Local cNumFat 	:= oMdlG6I:GetValue("G6I_FATURA") 
Local cConcil	:= oMdlG6I:GetValue("G6I_CONCIL")
Local nX 		:= 1

If T38VlParAc() .And. FwAlertYesNo(STR0125, STR0113) //"Todas altera��es em registros de acertos ser�o perdidas. Deseja prosseguir com a altera��o do Registro de Vendas?"###"Aten��o"
	While oMdlG6I:SeekLine({{"TMP_OK", .T.}})
		If oMdlG6I:GetValue("G6I_SITUAC") $ "2|4" .And. oMdlG6I:GetValue("G6I_META") <> "1"
			If !T39HasAltFOP(oModel)
				aAdd(aRV, {oMdlG3R:GetValue("G3R_MSFIL"), oMdlG3R:GetValue("G3R_NUMID"), oMdlG3R:GetValue("G3R_IDITEM"), oMdlG6I:GetLine(), oMdlG3Q:GetValue("G3Q_DOCORI")})			
			EndIf
		EndIf 
		oMdlG6I:LoadValue("TMP_OK", .F.)
	EndDo
	
	If Len(aRV) > 0
		G3P->(DbSetOrder(1))	// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
		For nX := 1 To Len(aRV)		
			cFilAnt := aRV[nX][1]
			
			//realiza a chamada da rotina de altera��o do registro de venda, apresentando somente o item selecionado
			If G3P->(DbSeek(xFilial("G3P") + aRV[nX][2]))
				T34Segmento(Val(G3P->G3P_SEGNEG))
				TURA34HierAces(__cUserId, @aDadosUsr)
				
				If Empty(aDadosUsr[1])
					Help( , , "T039ALTRV", , STR0126, 1, 0) //"'Seu codigo de usuario nao esta relacionado com um vendedor ou agente. Nao sera possivel acessar essa rotina.'" 
				Else
					oMdlG6I:GoLine(aRV[nX][4])	//Desassocia o RV posicionado
					
					If oMdlG6I:GetValue('G6I_ASSAUT') == "2"
						//Armazena as informa��es necess�rias para a associa��o manual
						aAdd(aManAss, {oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_FILDR"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ")}) 	
					Else 
						//Armazena o Item do G6I que foi desassociado, para realizar a associa��o autom�tica somente destes itens
						aAdd(aItemG6I, oMdlG6I:GetValue("G6I_ITEM"))
					EndIf
					
					//Antes de chamar a fun��o Tur039Des devemos voltar a cFilAnt pra n�o dar problema
					//na hora de chamar os models que ir�o desassociar a concilia��o para permitir a edi��o do RV.
					If Tur039Des(.T.)
						T34DadosUsr(aDadosUsr)
						oMdlT34 := FwLoadModel("TURA034")
						oMdlT34:SetOperation(MODEL_OPERATION_UPDATE)
									
						If oMdlG6J:GetValue("G6J_TIPO") == "1"
							oMdlT34:GetModel("G3Q_ITENS"):SetLoadFilter({{"G3Q_IDITEM", "'" + aRV[nX][3] + "'", MVC_LOADFILTER_EQUAL}})
						Else
							oMdlT34:GetModel("G3Q_ITENS"):SetLoadFilter({{"G3Q_IDITEM", "'" + aRV[nX][3] + "'"}, {"G3Q_DOC", "'" + aRV[nX][5] + "'", , MVC_LOADFILTER_OR}})
						EndIf
	
						oMdlT34:GetModel("G3Q_ITENS"):SetNoInsertLine(.T.)

						If oMdlG6J:GetValue("G6J_TIPO") == "1"
							FWExecView(STR0127, "TURA034", 4, , {|| .T.}, , , , , , , oMdlT34)	//"Registro de Vendas"			
						Else
							T39SetIdItem(aRV[nX][3])	
							FWExecView(STR0127, "TURA034", 4, , {|| .T.}, , , , , , , oMdlT34)	//"Registro de Vendas"			
							T39SetIdItem("")
						EndIf
			
						oMdlT34:DeActivate()
						oMdlT34:Destroy()
						T34Segmento(0)
						T34DadosUsr({"", "", "", ""})
					EndIf
				EndIf	
			EndIf
		Next nX
		cFilAnt := cBkpFil 
		
		//Realiza a associa��o autom�tica do registro de venda
		G6H->(DbSetOrder(1))	//G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA
		If G6H->(DbSeek(xFilial("G6H") + cNumFat))
			//Caso algum item de associa��o manual tenha sido alterado, � refeita a associa��o
			If Len(aManAss) > 0
				T39IAssMan(cConcil, aManAss, .T.)
			EndIf 
			
			If Len(aItemG6I) > 0
				Tu038Asso(.T., aItemG6I)
			EndIf
		EndIf
	EndIf
EndIf

FwModelActive(oModel)
RestArea(aAreaG6J)
RestArea(aArea)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039VisRV
Fun��o para visualizar de registro de venda que est� em concilia��o

@type function
@author Anderson Toledo
@since 09/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T039VisRV()

Local aArea     := GetArea()
Local oModel	:= FwModelActive()
Local oMdlG6I	:= oModel:GetModel("G6I_ITENS")
Local oMdlG3R	:= oModel:GetModel("G3R_ITENS")
Local oMdlT34	:= Nil
Local aRV		:= {}
Local aDadosUsr	:= {}
local cBkpFil	:= cFilAnt
Local cConcil	:= ""

If FwAlertYesNo(I18N(STR0129, {oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM")}), STR0113) //"Deseja visualizar o registro de venda '#1[ Num. RV ]#' item '#2[ Id Item ]#'?"
	cFilAnt := oMdlG3R:GetValue("G3R_MSFIL")
	cConcil	:= oMdlG3R:GetValue("G3R_CONCIL") 	

	//realiza a chamada da rotina de altera��o do registro de venda, apresentando somente o item selecionado
	G3P->(DbSetOrder(1))	// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
	If G3P->(DbSeek(xFilial("G3P") + oMdlG6I:GetValue("G6I_NUMID")))
		T34Segmento(Val(G3P->G3P_SEGNEG))
		TURA34HierAces(__cUserId, @aDadosUsr)
				
		If Empty(aDadosUsr[1])
			Help( , , "T039VISRV", , STR0126, 1, 0) //"Seu codigo de usuario nao esta relacionado com um vendedor ou agente. Nao sera possivel acessar essa rotina." 
		Else
			T34DadosUsr(aDadosUsr)
			oMdlT34 := FwLoadModel("TURA034")
			oMdlT34:SetOperation(MODEL_OPERATION_VIEW)
			oMdlT34:GetModel("G3Q_ITENS"):SetLoadFilter({{"G3Q_IDITEM", "'" + oMdlG6I:GetValue("G6I_IDITEM") + "'", MVC_LOADFILTER_EQUAL}})
			oMdlT34:GetModel("G3Q_ITENS"):SetNoInsertLine(.T.)
									
			FWExecView(STR0127, "TURA034", MODEL_OPERATION_VIEW, , {|| .T.}, , , , , , , oMdlT34)	//"Registro de Vendas"			
		
			oMdlT34:DeActivate()
			oMdlT34:Destroy()
			T34Segmento(0)
			T34DadosUsr({"", "", "", ""})
		EndIf	
	EndIf
	cFilAnt := cBkpFil 
EndIf

FwModelActive(oModel)
RestArea(aArea)
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039AprSt

@type function
@author Anderson Toledo
@since 09/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T039AprSt(oModel)

Local oMdlG6J := oModel:GetModel("G6J_MASTER")
Local oMdlG6I := oModel:GetModel("G6I_ITENS")
Local cSts 	  := ""

cSts := "UPDATE " + RetSqlName("G6J")
cSts += 	" SET G6J_APROVA = (SELECT (CASE" 
cSts += 								" WHEN TOTAL = APROVADO	THEN '3'"
cSts += 								" WHEN APROVADO > 0		THEN '2'"
cSts += 								" ELSE '1'"
cSts += 								" END )"
cSts += 					  " FROM (SELECT SUM(1) TOTAL,"
cSts += 								   " SUM(CASE WHEN G6I_SITUAC = '2' THEN 1 ELSE 0 END) ASSOCIADO," 
cSts += 								   " SUM(CASE WHEN G6I_SITUAC = '3' THEN 1 ELSE 0 END) APROVADO"
cSts += 							" FROM " + RetSqlName("G6I") + " G6I"
cSts += 							" WHERE G6I_CONCIL = '" + oMdlG6I:GetValue("G6I_CONCIL") + "' AND G6I_FILIAL = '" + oMdlG6I:GetValue("G6I_FILIAL") + "' AND " + IIF(TCSrvType() == "AS/400", " G6I.@DELETED@ = ' ' ", " G6I.D_E_L_E_T_ = ' ' ")+ ") TRB)" 
cSts += 	" WHERE G6J_CONCIL = '" + oMdlG6J:GetValue("G6J_CONCIL") + "' AND G6J_FILIAL = '" + oMdlG6J:GetValue("G6J_FILIAL") + "' AND " + IIF(TCSrvType() == "AS/400", " @DELETED@ = ' ' ", " D_E_L_E_T_ = ' ' ")
	
Return TcSqlExec(cSts) >= 0 

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39AltFOP
Fun��o para valida��o e execu��o da rotina de altera��o de FOP
@type function
@author Anderson Toledo 
@since 19/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA39AltFOP()

Local oModel := FwModelActive()

If T38VlParAc()
	If oModel:GetValue("G6I_ITENS", "G6I_SITUAC") == "3"
		FwAlertWarning(I18N(STR0138, {oModel:GetValue("G6I_ITENS", "G6I_ITEM")}), STR0052)	//"Item da fatura #1[Num. da fatura]# j� aprovado, esta opera��o n�o � permitida."
		Return
	EndIf
	FWMsgRun( , {|| TA39IAlFOP()}, , STR0101) //"Aguarde, marcando itens..."	
EndIf
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39CAlFOP

@type function
@author Anderson Toledo
@since 09/01/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA39CAlFOP()

Local oModel := FwModelActive()

If T38VlParAc()
	If oModel:GetValue("G6I_ITENS", "G6I_SITUAC") == "3"
		FwAlertWarning(I18N(STR0138, {oModel:GetValue("G6I_ITENS", "G6I_ITEM")}), STR0052)	//"Item da fatura #1[Num. da fatura]# j� aprovado, esta opera��o n�o � permitida."
		Return
	EndIf
	FWMsgRun( , {|| TA39JCAlFO()}, , STR0101) //"Aguarde, marcando itens..."	
EndIf
	
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39SetIdItem
Fun��o para alterar o conteudo da vari�vel estatica cIdItem

@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T39SetIdItem(cId)	
	cIdItem := cId
Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39GetIdItem
Fun��o para obter o conteudo da vari�vel estatica cIdItem

@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T39GetIdItem()	
Return cIdItem

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39HasAltFOP
Fun��o para indicar se o item de venda posicionado possui alterea��o de FOP

@type function
@author Anderson Toledo
@since 13/06/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function T39HasAltFOP(oModel)

Local oModelG3Q	:= oModel:GetModel("G3Q_ITENS")
Local aLines	:= FwSaveRows(oModel)
Local lRet		:= .F.

lRet :=	oModelG3Q:SeekLine({{"G3Q_CANFOP", "1"}}) .Or. oModelG3Q:SeekLine({{"G3Q_CANFOP", "2"}}) .Or. oModelG3Q:SeekLine({{"G3Q_CANFOP", "3"}})

FwRestRows(aLines)
	
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39MdlConc
Fun��o que retorna o modelo de dados da concilia��o

@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39MdlConc(aFilter)

Local oModel := FwLoadModel("TURA039MDL")

Default aFilter	:= {}

If len(aFilter) > 0
	oModel:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_ITEM", T39Arr2Str(aFilter), MVC_LOADFILTER_IS_CONTAINED}})
EndIf

oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:SetVldActivate({|| .T.})
oModel:SetCommit( {|oModel| FwFormCommit(oModel)})
oModel:GetModel("G6J_MASTER"):SetOnlyQuery(.T.)
oModel:GetModel("G6I_ITENS"):SetOnlyQuery(.T.)
oModel:Activate()

T39OpenMdl(oModel, "G3R_ITENS", .T.)

Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39DelAcrAut
Fun��o que apaga os acertos gerados automaticamente

@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39DelAcrAut(aFilter)

Local oModel  := T39MdlConc(aFilter)
Local oMdlG6I := oModel:GetModel("G6I_ITENS")
Local oMdlG3R := oModel:GetModel("G3R_ITENS")
Local lRet	  := .T.
Local nX	  := 0
Local nY	  := 0

For nX := 1 To oMdlG6I:Length()
	oMdlG6I:GoLine( nX )
	For nY := 1 To oMdlG3R:Length() 	
		oMdlG3R:GoLine(nY)
		If oMdlG3R:GetValue("G3R_ACERTO") == "1" .And. oMdlG3R:GetValue("G3R_ACRAUT") == "1"
			oMdlG3R:DeleteLine()
		EndIf
	Next nY	
Next nX

If (lRet := oModel:VldData())
	oModel:CommitData()
EndIf

oModel:DeActivate()
oModel:Destroy()
	
Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39UpdItSts
Fun��o que atualiza a situa��o do item da fatura

@type function
@author Anderson Toledo
@since 12/05/2017
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39UpdItSts(oModel, aItens, cStatus, cLegenda)

Local aArea     := GetArea()
Local oModelG6I := oModel:GetModel("G6I_ITENS")
Local cSts		:= ""
Local lRet		:= .T.
Local nX		:= 0

If Len(aItens) == 0
	Return .T.
EndIf

cSts := "UPDATE " + RetSqlName("G6I")
cSts += 	" SET G6I_SITUAC = '" + cStatus +"' "
cSts +=		" WHERE G6I_ITEM IN " + T39Arr2Str(aItens, .T.) 
cSts +=			" AND G6I_CONCIL = '" + oModelG6I:GetValue("G6I_CONCIL") + "'"
cSts +=			" AND G6I_FATURA = '" + oModelG6I:GetValue("G6I_FATURA") + "'"
cSts +=			" AND G6I_FILIAL = '" + oModelG6I:GetValue("G6I_FILIAL") + "'"
cSts +=			IIF(TCSrvType() == "AS/400", " AND @DELETED@ = ' ' ", " AND D_E_L_E_T_ = ' ' ")

If (lRet := TCSqlExec(cSts) >= 0)
	For nX := 1 To Len(aItens)
		If oModelG6I:SeekLine({{"G6I_ITEM", aItens[nX]}})
			oModelG6I:LoadValue("G6I_LEGASS", cLegenda)
		EndIf
	Next nX
EndIf
	
RestArea(aArea)	

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39VldNatur
Fun��o que valida a natureza financeira dos documentos de reserva e seus respectivos acordos

@type function
@Return ${return}, ${return_description}
@param oModel, object, descricao
@param cErro, characters, descricao
@param cTipo, characters, descricao

@author osmar.junior
@since 07/11/2018
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function T39VldNatur(oModel, cErro, cTipo)

Local aArea     := GetArea()
Local oModelG3R := oModel:GetModel("G3R_ITENS")
Local aItens	:= {}
Local cAlias1	:= GetNextAlias()
Local cAlias2	:= ""
Local cSts		:= ""
Local lRet		:= .T.
Local nX		:= 0
Local nY		:= 0

Default cErro	:= ""
Default cTipo	:= ""

cError := STR0159 + CRLF //"Verifique as naturezas abaixo antes da efetiva��o:" 

If cTipo == "1"
	If Empty(oModel:GetValue("G6H_MASTER", "G6H_NATUR"))
		cErro := STR0160 + CRLF 	// "Natureza para gera��o da fatura a pagar"
		lRet  := .F.
	EndIf
EndIf

BeginSql Alias cAlias1
	SELECT G3R_MSFIL, G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ, G3R_NATURE	
	FROM %Table:G3R% G3R
	WHERE G3R_MSFIL = %Exp:oModelG3R:GetValue("G3R_MSFIL")% AND G3R_NUMID = %Exp:oModelG3R:GetValue("G3R_NUMID")% AND G3R_IDITEM = %Exp:oModelG3R:GetValue("G3R_IDITEM")% AND G3R_NUMSEQ <> '' AND G3R.%NotDel%
EndSql

While (cAlias1)->(!Eof())
	aAdd(aItens, {(cAlias1)->G3R_NUMID, (cAlias1)->G3R_IDITEM, (cAlias1)->G3R_NUMSEQ, .F., {}})
	
	If Empty((cAlias1)->G3R_NATURE)
		aTail(aItens)[4] := .T.
		aAdd(aTail(aItens)[5], STR0161) //"Documento de Reserva" 
	EndIf

	cAlias2	:= GetNextAlias()	
	BeginSql Alias cAlias2			
		SELECT G48_APLICA 
		FROM %Table:G3R% G3R 
		INNER JOIN %Table:G48% G48 ON G48_NUMID = G3R_NUMID AND G48_FILIAL = G3R_FILIAL AND G48_IDITEM = G3R_IDITEM AND G48_NUMSEQ = G3R_NUMSEQ AND G48_CLIFOR = '2' AND G48_STATUS = '1' AND G48_COMSER = '1' AND G48_NATURE = ' ' AND G48.%notDel%
		WHERE G3R_NUMID = %Exp:(cAlias1)->G3R_NUMID% AND G3R_IDITEM = %Exp:(cAlias1)->G3R_IDITEM% AND G3R_NUMSEQ = %Exp:(cAlias1)->G3R_NUMSEQ% AND G3R_MSFIL = %Exp:(cAlias1)->G3R_MSFIL% AND G3R.%notDel%
	EndSql
	
	While (cAlias2)->(!Eof())
		aTail(aItens)[4] := .T.
		aAdd(aTail(aItens)[5], STR0162 + (cAlias2)->G48_APLICA) //"Acordo fornecedor, aplica��o: " 
		(cAlias2)->(DbSkip())
	EndDo
	(cAlias2)->(DbCloseArea())
	(cAlias1)->(DbSkip())
EndDo
(cAlias1)->(DbCloseArea())

For nX := 1 To Len(aItens)
	If nX > 1
		cErro += CRLF
	EndIf

	If aItens[nX][4]	
		lRet  := .F.
		cErro += Space(4)
		cErro += I18N(STR0163 + CRLF, {aItens[nX][1], aItens[nX][2], aItens[nX][3]}) //"Doc. reserva: #1[Doc. Reserva]#, Item: #2[Item]#, Seq.: #3[Sequencia]#"

		For nY := 1 To Len(aItens[nX][5])
			cErro += CRLF
			cErro += Space(8) + " - " + aItens[nX][5][nY]
		Next nY
	EndIf
Next nX
	
RestArea(aArea)

Return lRet

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039Dem
Fun��o que chama a rotina que gera os demonstrativos financeiros

@type Function
@author Enaldo Cardoso Junior
@since 29/01/2016
@version 12.1.7
/*/
//------------------------------------------------------------------------------------------
Function TA039Dem(nTipo, lImpAuto)
	FWMsgRun( , {|| TA039DemFi(nTipo, lImpAuto)}, , STR0009) 		// "Aguarde enquanto o demonstrativo � gerado."
Return