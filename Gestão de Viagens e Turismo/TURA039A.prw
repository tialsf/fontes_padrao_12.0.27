#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039A.CH"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelos de dados para apresenta��o da vis�o financeira

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function ModelDef()
	
Local oModel := MPFormModel():New("TURA039A", /*bPreValidacao*/, /*bPosValidacao*/, {|| .T.}, /*bCancel*/)

oModel:AddFields("G6J_MASTER", /*cOwner*/, FWFormStruct(1, "G6J"))
oModel:GetModel("G6J_MASTER"):SetOnlyQuery ( .T. )

oModel:AddGrid("G3R_ITENS", "G6J_MASTER", FwFormStruct(1, "G3R", , .F.))
oModel:SetRelation("G3R_ITENS", {{"G3R_FILCON", "xFilial('G6I')"}, {"G3R_CONCIL", "G6J_CONCIL"}, {"G3R_STATUS", "'1'"}}, G3R->(IndexKey(1)))
oModel:GetModel("G3R_ITENS"):SetLoadFilter({{"G3R_TPSEG", "'1'", MVC_LOADFILTER_EQUAL}})

//Acordos Fornecedor
oModel:AddGrid("G48B_ITENS", "G3R_ITENS", FwFormStruct(1, "G48", , .F.))
oModel:SetRelation("G48B_ITENS", {{"G48_FILIAL", "G3R_FILIAL"}, {"G48_NUMID", "G3R_NUMID"}, {"G48_IDITEM", "G3R_IDITEM"}, {"G48_NUMSEQ", "G3R_NUMSEQ"}, {"G48_CLIFOR", "'2'"}, {"G48_CONINU", "''"}, {"G48_RECCON", "'T'" }}, G48->(IndexKey(1)))
oModel:GetModel("G48B_ITENS"):SetOptional(.T.)
oModel:GetModel("G48B_ITENS"):SetNoInsertLine(.T.)
oModel:GetModel("G48B_ITENS"):SetNoDeleteLine(.T.)

oModel:AddGrid("G4CI_ITENS", "G3R_ITENS", FwFormStruct(1, "G4C"))
oModel:SetRelation("G4CI_ITENS", {{"G4C_FILIAL", "G3R_FILIAL"}, {"G4C_NUMID", "G3R_NUMID"}, {"G4C_IDITEM", "G3R_IDITEM"}, {"G4C_NUMSEQ", "G3R_NUMSEQ"}, {"G4C_CONINU", "''"}}, G4C->(IndexKey(1)))
oModel:GetModel("G4CI_ITENS"):SetNoInsertLine(.T.)
oModel:GetModel("G4CI_ITENS"):SetNoDeleteLine(.T.)

oModel:AddGrid("G4CR_ITENS", "G3R_ITENS", FwFormStruct(1, "G4C"))
oModel:SetRelation("G4CR_ITENS", {{"G4C_FILIAL", "G3R_FILIAL"}, {"G4C_NUMID", "G3R_NUMID"}, {"G4C_IDITEM", "G3R_IDITEM"}, {"G4C_NUMSEQ", "G3R_NUMSEQ"}, {"G4C_CONINU", "''"}, {"G4C_CARTUR", "''"}}, G4C->(IndexKey(1))) 
oModel:GetModel("G4CR_ITENS"):SetNoInsertLine(.T.)
oModel:GetModel("G4CR_ITENS"):SetNoDeleteLine(.T.)

oModel:SetDescription(STR0001) //"Vis�o Financeira"

oModel:GetModel("G4CI_ITENS"):SetLoadFilter( , " G4C_TIPO IN ('1', '2', '5') AND G4C_CLIFOR = '2' ")		// ITENS DE VENDA
oModel:GetModel("G4CR_ITENS"):SetLoadFilter( , " G4C_TIPO IN ('3', '4') AND G4C_CLIFOR = '2' ")			// ITENS DE REEMBOLSO

oModel:SetVldActivate({|oModel| TA039AAct(oModel)})
	
Return oModel

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
ViewDef para apresenta��o da vis�o financeira

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView		:= FWFormView():New()
Local oStruG48B := FWFormStruct(2, "G48")

oView:SetModel(FWLoadModel("TURA039A"))

oStruG48B:RemoveField("G48_OK")
oStruG48B:RemoveField("G48_RECCON")

oView:AddField("VIEW_G6J", FWFormStruct(2, "G6J"), "G6J_MASTER")
oView:AddGrid("VIEW_G3R" , FWFormStruct(2, "G3R"), "G3R_ITENS" )
oView:AddGrid("VIEW_G48B", oStruG48B	         , "G48B_ITENS")
oView:AddGrid("VIEW_G4CI", FwFormStruct(2, "G4C"), "G4CI_ITENS")
oView:AddGrid("VIEW_G4CR", FwFormStruct(2, "G4C"), "G4CR_ITENS")

// Divis�o Horizontal
oView:CreateHorizontalBox("ALL", 100)
oView:CreateFolder("FOLDER_ALL"	,"ALL")

// Pastas
oView:AddSheet("FOLDER_ALL", "ABA_ALL_CABEC", STR0002) 	// "Concilia��o"
oView:AddSheet("FOLDER_ALL", "ABA_ALL_VISAO", STR0003) 	// "Vis�o Financeira"

oView:CreateHorizontalBox("CONCILIACAO_CONTENT", 100, , , "FOLDER_ALL", "ABA_ALL_CABEC")
oView:CreateHorizontalBox("LINHASUPERIOR"	   , 030, , , "FOLDER_ALL", "ABA_ALL_VISAO")
oView:CreateHorizontalBox("ACORDOS_CONTENT"	   , 030, , , "FOLDER_ALL", "ABA_ALL_VISAO")
oView:CreateHorizontalBox("FINANCEIRO_CONTENT" , 040, , , "FOLDER_ALL", "ABA_ALL_VISAO")

oView:CreateVerticalBox("DR_CONTENT"   , 080, "LINHASUPERIOR", , "FOLDER_ALL", "ABA_ALL_VISAO")
oView:CreateVerticalBox("TOTAL_CONTENT", 020, "LINHASUPERIOR", , "FOLDER_ALL", "ABA_ALL_VISAO")

oView:CreateFolder("FOLDER_FINANCEIRO", "FINANCEIRO_CONTENT")
oView:AddSheet("FOLDER_FINANCEIRO", "FOLDER_PAGAR" 	, STR0012) 	// "Intermedia��o"
oView:AddSheet("FOLDER_FINANCEIRO", "FOLDER_RECEBER", STR0011) 	// "Receitas"

oView:CreateHorizontalBox("oStruG4CI", 100, , , "FOLDER_FINANCEIRO", "FOLDER_PAGAR")
oView:CreateHorizontalBox("BOX_G4CR" , 100, , , "FOLDER_FINANCEIRO", "FOLDER_RECEBER")

oView:AddOtherObject("TOTAL_IF", {|oPanel| Tur042IFTot(oPanel)})

oView:SetOwnerView("VIEW_G6J" , "CONCILIACAO_CONTENT")
oView:SetOwnerView("VIEW_G3R" , "DR_CONTENT")
oView:SetOwnerView("VIEW_G48B", "ACORDOS_CONTENT")
oView:SetOwnerView("VIEW_G4CI", "oStruG4CI")
oView:SetOwnerView("VIEW_G4CR", "BOX_G4CR")
oView:SetOwnerView("TOTAL_IF" , "TOTAL_CONTENT")

Return oView

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur042IFTot
Apresentar Gets com totalizadores

@type function
@author anderson
@since 24/03/2016
@version 1.0
@param oDlg, objeto, objeto do OTHER_OBJECTS
/*/
//------------------------------------------------------------------------------------------
Static Function Tur042IFTot(oDlg)

Local aArea   := GetArea()
Local oModel  := FwModelActive()
Local oValRec := Nil
Local oValInt := Nil
Local oValLiq := Nil
Local nValRec := 0
Local nValInt := 0
Local nValLiq := 0

G6H->(DbSetOrder(1))	// G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA
G6H->(DbSeek(xFilial("G6H") + oModel:GetValue("G6J_MASTER", "G6J_FATURA")))

TA039ADVal(@nValRec, @nValInt, @nValLiq, .T., G6H->G6H_FORNEC, G6H->G6H_LOJA)

@ 005,003 SAY STR0006 OF oDlg PIXEL SIZE 120,9  //"TOTAIS"
@ 020,003 SAY STR0012 OF oDlg PIXEL SIZE 120,9  //"Intermedia��o"
@ 020,070 MSGET oValInt VAR nValInt OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth / 2 - 080, 010

@ 035,003 SAY STR0011 OF oDlg PIXEL SIZE 120,9  //"Receitas"
@ 035,070 MSGET oValRec VAR nValRec OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth / 2 - 080, 010

@ 050,003 SAY STR0013 OF oDlg PIXEL SIZE 120,9  //"Valor L�q."
@ 050,070 MSGET oValLiq VAR nValLiq OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth / 2  - 080,010

RestArea(aArea)

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039ADVal
Fun��o com calculo de valores de receita da vis�o financeira

@type function
@author anderson
@since 24/03/2016
@version 1.0
@param nValRec, num�rico, Variavel por refer�ncia para armazenar valor a receber
@param nValInt, num�rico, Variavel por refer�ncia para armazenar valor a pagar
@param nValLiq, num�rico, Variavel por refer�ncia para armazenar valor da fatura
@param lAllItF, l�gico  	, Indica que itens financeiros finalizados ir�o compor o valor do resultado final
@param cForRep, caractere	, C�digo do Fornecedor de Reporte
@param cLjRep, caractere	, Loja do Fornecedor de Reporte
/*/
//------------------------------------------------------------------------------------------
Function TA039ADVal(nValRec, nValInt, nValLiq, lAllItF, cForRep, cLjRep)

Local oModel	:= FwModelActive()
Local oModelG3R	:= oModel:GetModel("G3R_ITENS")
Local oModelInt	:= oModel:GetModel("G4CI_ITENS")
Local oModelRec	:= oModel:GetModel("G4CR_ITENS")
Local nItem		:= 0
Local nItemDr	:= 0
Local nVlrReceb	:= 0
Local nVlrPagar	:= 0	

Default lAllItF	:= .F.
Default nValRec	:= 0
Default nValInt	:= 0
Default nValLiq	:= 0
Default cForRep	:= oModelG3R:GetValue("G3R_FORREP")
Default cLjRep	:= oModelG3R:GetValue("G3R_LOJREP")

For nItemDr := 1 To oModelG3R:Length()
	oModelG3R:GoLine(nItemDr)
	
	For nItem := 1 To oModelInt:Length()
		oModelInt:GoLine(nItem)

		If lAllItF
			//Se n�o estiver em 1=Ag. Libera��o ou Finalizado e Tp Conc. = Terrestre, vai para o pr�ximo registro
			If !(oModelInt:GetValue("G4C_STATUS") $ "1|3" .Or. (oModelInt:GetValue("G4C_STATUS") == "4" .And. oModelInt:GetValue("G4C_TPCONC") $ "1|3"))
				Loop
			EndIf
		Else
			If !(oModelInt:GetValue("G4C_STATUS") $ "1|3")
				Loop
			EndIf
		EndIf

		//verifica se o fornecedor do item financeiro difere do fornecedor de reporte.
		//N�o considerar fornecedores diferentes
		If (oModelInt:GetValue("G4C_CODIGO") + oModelInt:GetValue("G4C_LOJA") <> cForRep + cLjRep)
			//Verifica se o pagamento foi realizado com cart�o
			If !Empty(oModelInt:GetValue("G4C_CARTUR"))
				Loop
			EndIf
		EndIf

		If !oModelInt:IsDeleted()
			TA39AAddVl(oModelInt, @nValInt, @nValRec)
		EndIf
	Next nItem

	For nItem := 1 To oModelRec:Length()
		oModelRec:GoLine(nItem)

		If lAllItF
			//Se n�o estiver em 1=Ag. Libera��o ou Finalizado e Tp Conc. = Terrestre, vai para o pr�ximo registro
			If !(oModelRec:GetValue("G4C_STATUS") $ "1|3" .Or. (oModelRec:GetValue("G4C_STATUS") == "4" .And. oModelRec:GetValue("G4C_TPCONC") $ "1|3"))
				Loop
			EndIf
		Else
			If !(oModelRec:GetValue("G4C_STATUS") $ "1|3")
				Loop
			EndIf
		EndIf

		If !oModelRec:IsDeleted()
			TA39AAddVl(oModelRec, @nValInt, @nValRec)
		EndIf
	Next nItem
Next nItemDr

//Se Intermedia��o > 0, valor a pagar, sen�o valor a receber
nVlrPagar += IIF(nValInt > 0, nValInt, nValInt * -1)

//Se Receitas > 0, valor a receber, sen�o valor a pagar
nVlrReceb += IIF(nValRec > 0, nValRec, nValRec * -1) 

nValLiq := nVlrPagar - nVlrReceb

Return

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39AAddVl
Fun��o para c�lculo do valor do item financeiro de acordo com o tipo e se a pagar/receber

@type function
@author Anderson Toledo
@since 19/07/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA39AAddVl(oModel, nValInter, nValRec)
	
If oModel:GetValue("G4C_TIPO") == "1" 					//Itens de Venda
	If oModel:GetValue("G4C_PAGREC") == "1"
		nValInter += oModel:GetValue("G4C_VALOR")
	Else
		nValInter -= oModel:GetValue("G4C_VALOR")
	EndIf
ElseIf oModel:GetValue("G4C_TIPO") $ "2|5" 				//Itens de Reembolso
	If oModel:GetValue("G4C_PAGREC") == "1"
		nValInter += oModel:GetValue("G4C_VALOR")
	Else
		nValInter -= oModel:GetValue("G4C_VALOR")	
	EndIf
ElseIf oModel:GetValue("G4C_TIPO") == "3" 				//Itens de Receita
	If oModel:GetValue("G4C_PAGREC") == "1"
		nValRec -= oModel:GetValue("G4C_VALOR")
	Else
		nValRec += oModel:GetValue("G4C_VALOR")
	EndIf
ElseIf oModel:GetValue("G4C_TIPO") == "4" 				//Itens de Abatimento de Receita
	If oModel:GetValue("G4C_PAGREC") == "1"
		nValRec -= oModel:GetValue("G4C_VALOR")
	Else
		nValRec += oModel:GetValue("G4C_VALOR")
	EndIf
EndIf

Return		

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AAct
Fun��o que valida abertura do modelo de dados

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//------------------------------------------------------------------------------------------
Function TA039AAct(oModel)

Local lRet := .T.
	
If G6J->G6J_EFETIV = "1" 
	Help( , , "TA039AAct", , STR0010, 1, 0) 	//"Concilia��o j� efetivada, itens da vis�o financeira j� encerrados."
	lRet := .F.		
EndIf
	
Return lRet