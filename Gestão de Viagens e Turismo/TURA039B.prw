#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados referete a pesquisa de Documentos de reserva para concilia��o - outros (terrestre)

@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
//+-----------------------------------------------------------------------------------------------------
Static Function ModelDef()
Return FWLoadModel("TURA039A")

//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Vis�o de dados referete a pesquisa de Documentos de reserva para concilia��o terrestre

@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
//+-----------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView  := FWLoadView("TURA039A")
Local oModel := oView:GetModel()

oModel:bCommit := {|oModel| TA039BCommit(oModel), TA039ADestroy()}

Return oView

//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039BCommit
Func�o de grava��o dos dados 

@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
//+-----------------------------------------------------------------------------------------------------
Static Function TA039BCommit(oModelPesq)

Local oModel 	 := TA042Model()
Local oMdlG3R	 := oModel:GetModel("G3R_ITENS")
Local oMdlG3P	 := oModel:GetModel("G3P_FIELDS")
Local oMdlG3Q	 := oModel:GetModel("G3Q_ITENS")
Local oMdlG3S    := oModel:GetModel("G3S_ITENS")
Local oMdlG3T	 := oModel:GetModel("G3T_ITENS")
Local oMdlG3U	 := oModel:GetModel("G3U_ITENS")
Local oMdlG3V	 := oModel:GetModel("G3V_ITENS")
Local oMdlG3W    := oModel:GetModel("G3W_ITENS")
Local oMdlG3Y    := oModel:GetModel("G3Y_ITENS")
Local oMdlG3X	 := oModel:GetModel("G3X_ITENS")
Local oMdlG42	 := oModel:GetModel("G42_ITENS")
Local oMdlG41	 := oModel:GetModel("G41_ITENS")
Local oMdlG40	 := oModel:GetModel("G40_ITENS")
Local oMdlG3Z	 := oModel:GetModel("G3Z_ITENS")
Local oMdlG43	 := oModel:GetModel("G43_ITENS")
Local oMdlG44    := oModel:GetModel("G44_ITENS")
Local oMdlG46	 := oModel:GetModel("G46_ITENS")
Local oMdlG47    := oModel:GetModel("G47_ITENS")
Local oMdlG48A	 := oModel:GetModel("G48A_ITENS")
Local oMdlG48B	 := oModel:GetModel("G48B_ITENS")
Local oMdlG49	 := oModel:GetModel("G49_ITENS")
Local oMdlG4CA   := oModel:GetModel("G4CA_ITENS")
Local oMdlG4CB	 := oModel:GetModel("G4CB_ITENS")
Local oMdlResult := oModelPesq:GetModel("TMP_DETAIL")
Local cNumId     := oMdlResult:GetValue("G3R_NUMID")
Local cIdItem    := oMdlResult:GetValue("G3R_IDITEM")
Local cNumSeq    := oMdlResult:GetValue("G3R_NUMSEQ")
Local cConcil    := oModel:GetValue("G8C_MASTER", "G8C_CONCIL")
Local nX	     := 0
Local nLine      := 0

G3R->(DbSetOrder(1))	// G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ+G3R_CONINU
For nLine := 1 to oMdlResult:Length()
	oMdlResult:GoLine(nLine)

	If oMdlResult:GetValue("TMP_OK") .And. !oMdlResult:IsDeleted()
		// TA039Tb2Md(oModel, cTable, nIndex, cFldKey, cKey, bCondNewLine, aSeek , bCondReg, aValues)

		// Registro de Venda
		If !TA039Tb2Md(oMdlG3P, "G3P", 1, "G3P_FILIAL+G3P_NUMID", xFilial("G3P") + cNumId, {|| .F. }, /*aSeek*/, /*bCondReg*/, /*aValues*/)
			Exit
		EndIf

		// Itens de Venda
		If !TA039Tb2Md(oMdlG3Q, "G3Q", 1, "G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM+G3Q_NUMSEQ", xFilial("G3Q") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3Q:GetValue("G3Q_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G3Q_CONORI", cConcil}})
			Exit
		EndIf

		// Documento de Reserva
		If !TA039Tb2Md(oMdlG3R, "G3R", 1, "G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ", xFilial("G3R") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3R:GetValue("G3R_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G3R_CONORI", cConcil}})
			Exit
		EndIf
		oMdlG3R:SetValue("G3R_CONCIL", cConcil)
		oMdlG3R:SetValue("G3R_STATUS", "2")

		// Passageiro
		If !TA039Tb2Md(oMdlG3S, "G3S", 1, "G3S_FILIAL+G3S_NUMID+G3S_IDITEM+G3S_NUMSEQ", xFilial("G4S") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3S:GetValue("G3S_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G3S_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento AEREO
		If !TA039Tb2Md(oMdlG3T, "G3T", 1, "G3T_FILIAL+G3T_NUMID+G3T_IDITEM+G3T_NUMSEQ", xFilial("G3T") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3T:GetValue("G3T_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3T->G3T_CODPAX}}}}, /*bCondReg*/, {{"G3T_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento HOTEL
		If !TA039Tb2Md(oMdlG3U, "G3U", 1, "G3U_FILIAL+G3U_NUMID+G3U_IDITEM+G3U_NUMSEQ", xFilial("G3U") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3U:GetValue("G3U_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3U->G3U_CODPAX}}}}, /*bCondReg*/, {{"G3U_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento CARRO
		If !TA039Tb2Md(oMdlG3V, "G3V", 1, "G3V_FILIAL+G3V_NUMID+G3V_IDITEM+G3V_NUMSEQ", xFilial("G3V") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3V:GetValue("G3V_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3V->G3V_CODPAX}}}}, /*bCondReg*/, {{"G3V_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento RODOVIARIO
		If !TA039Tb2Md(oMdlG3W, "G3W", 1, "G3W_FILIAL+G3W_NUMID+G3W_IDITEM+G3W_NUMSEQ", xFilial("G3W") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3W:GetValue("G3W_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3W->G3W_CODPAX}}}}, /*bCondReg*/, {{"G3W_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento TREM
		If !TA039Tb2Md(oMdlG3X, "G3X", 1, "G3X_FILIAL+G3X_NUMID+G3X_IDITEM+G3X_NUMSEQ", xFilial("G3X") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3X:GetValue("G3X_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3X->G3X_CODPAX}}}}, /*bCondReg*/, {{"G3X_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento CRUZEIRO
		If !TA039Tb2Md(oMdlG3Y, "G3Y", 1, "G3Y_FILIAL+G3Y_NUMID+G3Y_IDITEM+G3Y_NUMSEQ", xFilial("G3Y") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3Y:GetValue("G3Y_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G3Y->G3Y_CODPAX}}}}, /*bCondReg*/, {{"G3Y_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento PACOTE
		If !TA039Tb2Md(oMdlG3Z, "G3Z", 1, "G3Z_FILIAL+G3Z_NUMID+G3Z_IDITEM+G3Z_NUMSEQ", xFilial("G3Z") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG3Z:GetValue("G3Z_NUMID"))}, {oMdlG3Z, {{"G3S_CODPAX", {|| G3Z->G3Z_CODPAX}}}}, /*bCondReg*/, {{"G3Z_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento TOUR
		If !TA039Tb2Md(oMdlG40, "G40", 1, "G40_FILIAL+G40_NUMID+G40_IDITEM+G40_NUMSEQ", xFilial("G40") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG40:GetValue("G40_NUMID"))}, {oMdlG40, {{"G3S_CODPAX", {|| G40->G40_CODPAX}}}}, /*bCondReg*/, {{"G40_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento SEGURO
		If !TA039Tb2Md(oMdlG41, "G41", 1, "G41_FILIAL+G41_NUMID+G41_IDITEM+G41_NUMSEQ", xFilial("G41") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG41:GetValue("G41_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G41->G41_CODPAX}}}}, /*bCondReg*/, {{"G41_CONORI", cConcil}})
			Exit
		EndIf

 		// Segmento VISTO
		If !TA039Tb2Md(oMdlG42, "G42", 1, "G42_FILIAL+G42_NUMID+G42_IDITEM+G42_NUMSEQ", xFilial("G42") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG42:GetValue("G42_NUMID"))}, {oMdlG3S, {{"G3S_CODPAX", {|| G42->G42_CODPAX}}}}, /*bCondReg*/, {{"G42_CONORI", cConcil}})
			Exit
		EndIf

		// Segmento OUTROS
		If !TA039Tb2Md(oMdlG43, "G43", 1, "G43_FILIAL+G43_NUMID+G43_IDITEM+G43_NUMSEQ", xFilial("G43") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG43:GetValue("G43_NUMID"))}, {oMdlG43, {{"G3S_CODPAX", {|| G43->G43_CODPAX}}}}, /*bCondReg*/, {{"G43_CONORI", cConcil}})
			Exit
		EndIf

		// Tarifas
		If !TA039Tb2Md(oMdlG44, "G44", 1, "G44_FILIAL+G44_NUMID+G44_IDITEM+G44_NUMSEQ", xFilial("G44") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG44:GetValue("G44_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G44_CONORI", cConcil}})
			Exit
		EndIf

		// Taxas
		If !TA039Tb2Md(oMdlG46, "G46", 1, "G46_FILIAL+G46_NUMID+G46_IDITEM+G46_NUMSEQ", xFilial("G46") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG46:GetValue("G46_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G46_CONORI", cConcil}})
			Exit
		EndIf

		// Extras
		If !TA039Tb2Md(oMdlG47, "G47", 1, "G47_FILIAL+G47_NUMID+G47_IDITEM+G47_NUMSEQ", xFilial("G47") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG47:GetValue("G47_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G47_CONORI", cConcil}})
			Exit
		EndIf

		// Impostos
		If !TA039Tb2Md(oMdlG49, "G49", 1, "G49_FILIAL+G49_NUMID+G49_IDITEM+G49_NUMSEQ", xFilial("G49") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG49:GetValue("G49_NUMID"))}, /*aSeek*/, /*bCondReg*/, {{"G49_CONORI", cConcil}})
			Exit
		EndIf

		// Acordos Cliente
		If !TA039Tb2Md(oMdlG48A, "G48", 1, "G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ", xFilial("G48") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG48A:GetValue("G48_NUMID"))}, /*aSeek*/, {|| G48->G48_CLIFOR == "1"}, {{"G48_CONORI", cConcil}})
			Exit
		EndIf

		// Itens Financeiros Cliente
		If !TA039Tb2Md(oMdlG4CA, "G4C", 1, "G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ", xFilial("G4C") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG4CA:GetValue("G4C_NUMID"))}, /*aSeek*/, {|| G4C->G4C_CLIFOR == "1"}, {{"G4C_CONORI", cConcil}})
			Exit
		EndIf

		// Acordos Fornecedor
		If !TA039Tb2Md(oMdlG48B, "G48", 1, "G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ", xFilial("G48") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG48B:GetValue("G48_NUMID"))}, /*aSeek*/, {|| G48->G48_CLIFOR == "2"}, {{"G48_CONORI", cConcil}})
			Exit
		EndIf

		// Itens Financeiros Fornecedor
		If !TA039Tb2Md(oMdlG4CB, "G4C", 1, "G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ", xFilial("G4C") + cNumId + cIdItem + cNumSeq, {|| !Empty(oMdlG4CB:GetValue("G4C_NUMID"))}, /*aSeek*/, {|| G4C->G4C_CLIFOR == "2"}, {{"G4C_CONORI", cConcil}})
			Exit
		EndIf
	EndIf
Next nX

Return .T.

//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039Tb2Md
Fun��o que popula o modelo de dados com as informa��o da respectiva tabela  

@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
//+-----------------------------------------------------------------------------------------------------
Static Function TA039Tb2Md(oModel, cTable, nIndex, cFldKey, cKey, bCondNewLine, aSeek , bCondReg, aValues)

Local aArea      := GetArea()
Local oMdlActive := Nil
Local aErro	     := {}
Local aSeekModel := {}
Local cErro	     := ""
Local lRet 	     := .T.
Local nX		 := 0
Local nY		 := 0

DEFAULT bCondReg	 := {|| .T.}
DEFAULT bCondNewLine := {|| .T.}
DEFAULT aSeek		 := {}
DEFAULT aValues		 := {}

(cTable)->(DbSetOrder(nIndex))
(cTable)->(DbSeek(cKey))
While (cTable)->(!Eof()) .And. (cTable)->(&(cFldKey)) == cKey
	If !Eval(bCondReg)
		(cTable)->(DbSkip())
		Loop
	EndIf

	If Len(aSeek) > 0
		aSeekModel := {}
		For nY := 1 To Len(aSeek[2])
			aAdd(aSeekModel, {aSeek[2][nY][1], IIF(ValType(aSeek[2][nY][2]) == "B", Eval(aSeek[2][nY][2]), aSeek[2][nY][1])})
		Next

		If !aSeek[1]:SeekLine(aSeekModel)
			(cTable)->(DbSkip())
			Loop
		EndIf
	EndIf

	If Eval(bCondNewLine)
		If oModel:Length() == oModel:AddLine()
			oMdlActive := FwModelActive()
			TA039ShwError("TURA039BADDLINE",  oMdlActive:GetErrorMessage())
			Return .F.
		EndIf
	EndIf

	For nX := 1 To (cTable)->(FCount())
		If oModel:HasField(AllTrim((cTable)->(FieldName(nX))))
			//� realizado um LoadValue por se tratar de uma c�pia da linha original
			If !oModel:LoadValue(AllTrim((cTable)->(FieldName(nX))), (cTable)->(FieldGet(nX)))
				oMdlActive := FwModelActive()
				TA039ShwError("TURA039BSETVALUE",  oMdlActive:GetErrorMessage())
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next nX

	For nX := 1 To Len(aValues)
		If !oModel:LoadValue(aValues[nX][1], aValues[nX][2])
			oMdlActive := FwModelActive()
			TA039ShwError("TURA039BSETVALUE",  oMdlActive:GetErrorMessage())
			lRet := .F.
			Exit
		EndIf
	Next

	(cTable)->(DbSkip())
EndDo
(cTable)->(DbCloseArea())

RestArea(aArea)

Return lRet

//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039ShwError
Fun��o que mostra mensagem de erro do modelo de dados 

@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
//+-----------------------------------------------------------------------------------------------------
Static Function TA039ShwError( cIdHelp, aErro )

Local cErro := ""

cErro := "Id do formul�rio de origem:" + " [" + AllToChar(aErro[1]) + "]" + CRLF
cErro += "Id do campo de origem: " 	   + " [" + AllToChar(aErro[2]) + "]" + CRLF
cErro += "Id do formul�rio de erro: "  + " [" + AllToChar(aErro[3]) + "]" + CRLF
cErro += "Id do campo de erro: " 	   + " [" + AllToChar(aErro[4]) + "]" + CRLF
cErro += "Id do erro: " 			   + " [" + AllToChar(aErro[5]) + "]" + CRLF
cErro += "Mensagem do erro: " 		   + " [" + AllToChar(aErro[6]) + "]" + CRLF
cErro += "Mensagem da solu��o: " 	   + " [" + AllToChar(aErro[7]) + "]" + CRLF
cErro += "Valor atribu�do: "		   + " [" + AllToChar(aErro[8]) + "]" + CRLF
cErro += "Valor anterior: " 		   + " [" + AllToChar(aErro[9]) + "]" + CRLF

Help( , , cIdHelp, , cErro, 1, 0)

Return