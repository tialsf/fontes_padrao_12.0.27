#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039D.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039VerAc
Fun��o para verificar e gerar o acerto de um item caso necess�rio

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039VerAc(oModel)

Local oModelG6J  := Nil
Local oModelG6I  := Nil
Local oModelG3R  := Nil
Local oModelG48B := Nil
Local cBkpFilAnt := cFilAnt
Local cTipoIt	 := ""                                                                                                
Local cMsgErro	 := ""
Local nX		 := 0
Local nY		 := 0
Local nCont		 := 1 
Local lRet		 := .T.

Default oModel := FwModelActive()

oModelG6J  := oModel:GetModel("G6J_MASTER")
oModelG6I  := oModel:GetModel("G6I_ITENS")
oModelG3R  := oModel:GetModel("G3R_ITENS")
oModelG48B := oModel:GetModel("G48B_ITENS")

If !oModelG3R:IsEmpty() .And. !Empty(oModelG3R:GetValue("G3R_MSFIL"))
	cFilAnt := oModelG3R:GetValue("G3R_MSFIL") 
	cTipoIt := oModelG6I:GetValue("G6I_TIPOIT")
	
	bBlock := ErrorBlock({|e| TA039LOG(oModel:GetErrorMessage())})
	BEGIN SEQUENCE
		Do Case
			Case cTipoIt == "0"
				lRet := TA039AcTp0(oModel) //Cancelamento
			Case cTipoIt == "1"
				lRet := TA039AcTp1(oModel) //Venda Mista
			Case cTipoIt == "2"
				lRet := TA039AcTp2(oModel) //Venda de bilhete ( e-Ticket )
			Case cTipoIt == "3"
				lRet := TA039AcTp3(oModel) //Reembolso
			Case cTipoIt == "4"
				lRet := TA039AcTp4(oModel) //ADM
			Case cTipoIt == "5"
				lRet := TA039AcTp5(oModel) //ACM
			Case cTipoIt == "7"
				lRet := TA039AcTp7(oModel) //EMD
			Case cTipoIt == "9"
				lRet := TA039AcTp9(oModel) //BSP
		End Case		
	END SEQUENCE
	ErrorBlock(bBlock)
	
	cFilAnt := cBkpFilAnt
Else
	cMsgErro := STR0013 + CRLF	//"Bilhete/Loc com diverg�ncia ao gerar acerto."
	cMsgErro += STR0014 + oModelG6J:GetValue("G6J_FILIAL") + " - " + oModelG6J:GetValue("G6J_CONCIL") + CRLF		//"Concilia��o : "
	cMsgErro += STR0015 + oModelG6I:GetValue("G6I_FILIAL") + " - " + oModelG6I:GetValue("G6I_FATURA") + " - " + oModelG6I:GetValue("G6I_BILHET") + "/" + oModelG6I:GetValue("G6I_LOCALI") + CRLF				//"Item Fatura : "
	cMsgErro += STR0016 + oModelG3R:GetValue("G3R_FILIAL") + " - " + oModelG3R:GetValue("G3R_NUMID")  + "-"   + oModelG3R:GetValue("G3R_IDITEM") + "-" + oModelG3R:GetValue("G3R_NUMSEQ") + " | " + oModelG3R:GetValue("G3R_DOC") + CRLF		//"Item Reserva: "
	cMsgErro += CRLF
	While !Empty(ProcName(nCont))			
		cMsgErro += " > " + StrZero(nCont, 6) + " - " + ProcName(nCont) + " L: " + STR(ProcLine(nCont)) + CRLF
		nCont++
	EndDo		
	lRet := .F.		
	AutoGRLog("")
	MostraErro("x")
	AutoGRLog(cMsgErro)
	MostraErro()
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp1
Fun��o para gera��o do acerto do tipo 1 - Venda Mista

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp1(oModel) //Venda Mista
Return TA039AcTp2(oModel)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp2
Fun��o para gera��o do acerto do tipo 2 - Venda de bilhete ( e-Ticket) 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp2(oModel) //Venda de bilhete ( e-Ticket )

Local oModelG44   := oModel:GetModel("G44_ITENS")	// Tarifas
Local oModelG46   := oModel:GetModel("G46_ITENS")	// Taxas
Local oModelG47	  := oModel:GetModel("G47_ITENS")	// Extras
Local oModelG48A  := oModel:GetModel("G48A_ITENS")  // Acordos clientes
Local oModelG48B  := oModel:GetModel("G48B_ITENS")  // Acordos Fornecedor
Local oModelG6I   := oModel:GetModel("G6I_ITENS")	// Itens da Fatura
Local aCopyValues := {}
Local cSeqG3R	  := {}
Local cSeqOrig	  := oModel:GetValue("G3R_ITENS", "G3R_NUMSEQ")
Local cMoeda	  := ""
Local cTpFopG3R	  := ""
Local lRet 		  := .T.
Local nX		  := 0

cTpFopG3R := T39TpFop(oModel)
cMoeda	  := T39TpMoeda(oModel)
		
//Item da Fatura Tipo FOP: Intermedia��o e Documento de Reserva Tipo FOP: Cart�o Cliente	
If oModelG6I:GetValue("G6I_TPFOP") == "1" .And. cTpFopG3R == "2"
	//Neste caso � necess�rio gerar um item Faturado para pagamento ao fornecedor e cobran�a ao cliente. 
	//Por�m, como n�o � poss�vel saber o FOP de Intermedia��o que ser� utilizada o sistema apenas apontar� a diverg�ncia 
	//da FOP e n�o gerar� acerto automaticamente.
	lRet := oModelG6I:SetValue("G6I_DIVFOP", "1")
	
//Item da Fatura Tipo FOP: Intermedia��o e Documento de Reserva Tipo FOP: Cart�o Ag�ncia
ElseIf oModelG6I:GetValue("G6I_TPFOP") == "1" .And. cTpFopG3R == "3"
	//Neste caso � necess�rio gerar um item Faturado para pagamento ao fornecedor da fatura apenas, pois possivelmente 
	//o cliente j� tenha sido cobrado. Por�m, no item original o sistema gerou uma pend�ncia de pagamento � administradora 
	//do cart�o e esta pend�ncia deve ser estornada. Portanto, o sistema apontar� a diverg�ncia da FOP mas n�o ir� gerar acerto automaticamente.
	lRet := oModelG6I:SetValue("G6I_DIVFOP", "1")

//Item da Fatura Tipo FOP: Cart�o e Documento de Reserva Tipo FOP: Intermedia��o
ElseIf oModelG6I:GetValue("G6I_TPFOP") == "2" .And. cTpFopG3R == "1"
	//Neste caso � necess�rio gerar um item Faturado de valor inverso de Tarifa, Taxas e Extras para compensar com o original 
	//e igualar o valor liquido da Fatura do fornecedor. No entanto, o sistema n�o consegue identificar se foi uma compra no cart�o 
	//da ag�ncia e faturado ao cliente ou se foi cobrado diretamente no cart�o do cliente, por isso n�o � poss�vel gerar 
	//acerto ao cliente. Assim, o sistema apontar� a diverg�ncia da FOP mas n�o ir� gerar acerto automaticamente
	lRet := oModelG6I:SetValue("G6I_DIVFOP", "1")

//Item da Fatura com MOEDA diferente do Documento de Reserva
ElseIf oModelG6I:GetValue("G6I_MOEDA") <> cMoeda 
	// O sistema apontar� a diverg�ncia de MOEDA mas n�o ir� gerar acerto automaticamente
	lRet := oModelG6I:SetValue("G6I_DIVFOP", "2")
	
//FOP Desconhecida
ElseIf oModelG6I:GetValue("G6I_TPFOP") == "0"
	lRet := oModelG6I:SetValue("G6I_DIVFOP", "1")

Else
	lRet := T39GerAcer(oModel)
EndIf
		
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp3
Fun��o para gera��o do acerto do tipo 3 - Reembolso

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp3(oModel) //Reembolso
Return TA039AcTp2(oModel) 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp4
Fun��o para gera��o do acerto do tipo 4 - ADM

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp4(oModel) //ADM
Return TA039AcTp2(oModel) 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp5
Fun��o para gera��o do acerto do tipo  - ACM

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp5(oModel) //ACM
Return TA039AcTp2(oModel) 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp7
Fun��o para gera��o do acerto do tipo 7 - EMD 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp7(oModel) //EMD
Return TA039AcTp2(oModel) 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp9 
Fun��o para gera��o do acerto do tipo 9 BSP 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp9(oModel) //BSP
Return TA039AcTp2(oModel) 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcTp0
Fun��o para gera��o do acerto do tipo 0 - Cancelamento 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcTp0(oModel)

Local oModelG6I   := oModel:GetModel("G6I_ITENS")
Local oModelG3R   := oModel:GetModel("G3R_ITENS")	// Documento Reserva
Local oModelG3Q   := oModel:GetModel("G3Q_ITENS")	// Item de Venda
Local oModelG44   := oModel:GetModel("G44_ITENS")	// Tarifas
Local oModelG46   := oModel:GetModel("G46_ITENS")	// Taxas
Local oModelG47   := oModel:GetModel("G47_ITENS")	// Extras
Local oModelG48A  := oModel:GetModel("G48A_ITENS") 	// Acordos clientes
Local oModelG48B  := oModel:GetModel("G48B_ITENS") 	// Acordos Fornecedor
Local aCopyValues := {}
Local cSeqG3R	  := ""
Local cSeqOrig	  := oModelG3R:GetValue("G3R_NUMSEQ")
Local lRet 		  := .T.
Local nX		  := 0

If oModel:GetValue("G3R_ITENS", "G3R_STATUS") == "1" //DR Pendente
	T39OpenMdl(oModel, "G3R_ITENS", .T.)
	
	aCopyValues	:= TURxGetVls("G3R_ITENS", .T., {"G4CA_ITENS", "G4CB_ITENS"}, {}, oModel)
	If TURxSetVs(aCopyValues, {}, oModel)
		cSeqG3R := TA042MaxSq(oModel)
		TA042SetRec("G3R_ITENS", {{"_NUMSEQ", cSeqG3R}}, , .F.)
		
		oModelG6I:LoadValue("G6I_DIVERG", "1")	 	// Sim
		oModelG6I:LoadValue("G6I_ACERTO", "1") 	 	// Sim
		oModelG6I:LoadValue("G6I_CODDIV", STR0001) 	// "Status"		

		// tratamento realizado para quando ocorrer erro de gatilho aborte o restante do processo
		lRet := oModelG3R:SetValue("G3R_SEQACR", cSeqOrig)
		lRet := lRet .And. oModelG3Q:SetValue("G3Q_ATUIF" , "2") 		// Atualiza IF (1=Apenas Cliente;2=Cliente e Fornecedor)
		lRet := lRet .And. oModelG3Q:SetValue("G3Q_ACERTO", "1") 		// Sim	
		lRet := lRet .And. oModelG3R:SetValue("G3R_ACERTO", "1") 		// Sim
		lRet := lRet .And. oModelG3R:SetValue("G3R_ACRAUT", "1")  		// Sim
		lRet := lRet .And. oModelG3R:SetValue("G3R_STATUS", "1")		// Em Aberto	
		//Todos os valores ser�o recalculados
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLFIM" , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TOTREC", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TARIFA", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TAXA"  , 0)  
		lRet := lRet .And. oModelG3R:SetValue("G3R_EXTRAS", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLRIMP", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TXREE" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLRSER", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TAXADU", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TXFORN", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLCOMI", 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLINCE", 0)	
				
		If lRet
			//Tarifa, Taxa, Extras e impostos ter�o o valor invertido. 
			For nX := 1 To oModelG44:Length()
				oModelG44:GoLine(nX)	
				If oModelG44:GetValue("G44_TARBAS") <> 0                        	
				 	If !(lRet := oModelG44:SetValue("G44_TARBAS", oModelG44:GetValue("G44_TARBAS") * -1))
				 		Exit
				 	EndIf
				EndIf
			Next nX
			
			If lRet
				For nX := 1 To oModelG46:Length()
					oModelG46:GoLine(nX)	
					If oModelG46:GetValue("G46_VLBASE") <> 0                        	
					 	If !(lRet := oModelG46:SetValue("G46_VLBASE", oModelG46:GetValue("G46_VLBASE") * -1))
					 		Exit
					 	EndIf
					EndIf
				Next nX
		
				If lRet 
					For nX := 1 To oModelG47:Length()
						oModelG47:GoLine(nX)	
						If oModelG47:GetValue("G47_VLUNIT") <> 0                        	
						 	If !(lRet := oModelG47:SetValue("G47_VLUNIT", oModelG47:GetValue("G47_VLUNIT") * -1))
						 		Exit
						 	EndIf
						EndIf
					Next nX
			
					If lRet
						For nX := 1 To oModelG48A:Length()
							oModelG48A:GoLine(nX)	
							If oModelG48A:GetValue("G48_TPVLR") == "2" //Valor Fixo
								oModelG48A:LoadValue("G48_VLACD", oModelG48A:GetValue("G48_VLACD") * -1)
							EndIf
						Next nX
				
						For nX := 1 to oModelG48B:Length()
							oModelG48B:GoLine(nX)
							If oModelG48B:GetValue("G48_TPVLR") == "2" //Valor Fixo
								oModelG48B:LoadValue("G48_VLACD", oModelG48B:GetValue("G48_VLACD") * -1)
							EndIf
						Next nX
						
						T34CalcAco( , , "2", "0", .T.)
						T34CalcAco( , , "1", "0", .T.)
				
						//Refaz os itens financeiros
						oModelG48A:GoLine(1)
						oModelG48B:GoLine(1)
				
						//Atualiza Demonstrativo Financeiro
						T34AtuDmFi(oModel, .F.)
						Tur34ItFin(oModel, "3", .T., "3")
						TA039StG48()
					EndIf
				EndIf
			EndIf
		EndIf
	Else
		lRet := .F.
	EndIf
EndIf
	
If !lRet
	oModelG3R:DeleteLine() 
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039AcCan
Fun��o para gera��o do acerto de cancelamento 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039AcCan(oModel)

Local oModelG3R   := oModel:GetModel("G3R_ITENS")	// Documento Reserva
Local oModelG3Q   := oModel:GetModel("G3Q_ITENS")	// Item de Venda
Local oModelG46   := oModel:GetModel("G46_ITENS")	// Taxas
Local oModelG48B  := oModel:GetModel("G48B_ITENS")  // Acordos Fornecedor
Local aCopyValues := {}
Local cSeqG3R	  := ""
Local cSeqOrig	  := oModelG3R:GetValue("G3R_NUMSEQ")
Local lRet 		  := .T.
Local nX		  := 0
Local nTaxa		  := 0
Local nTaxaDU	  := 0
Local nComissao	  := 0
Local nIncentivo  := 0

T39CalVlDR(oModel, , @nTaxa, @nTaxaDU, @nComissao, @nIncentivo)
T39OpenMdl(oModel, "G3R_ITENS", .T.)
	
aCopyValues	:= TURxGetVls("G3R_ITENS", .T., {}, {}, oModel)
						
If (lRet := TURxSetVs(aCopyValues, {}, oModel))
	oModel:LoadValue("G6I_ITENS", "G6I_ACERTO", "1")

	cSeqG3R := TA042MaxSq(oModel)
	TA042SetRec("G3R_ITENS", {{"_NUMSEQ",cSeqG3R}}, , .F.)
		
	oModelG3R:LoadValue("G3R_SEQACR", cSeqOrig)
	oModelG3Q:LoadValue("G3Q_ATUIF" , "2") 		//Atualiza IF (1=Apenas Cliente;2=Cliente e Fornecedor)			
	oModelG3Q:LoadValue("G3Q_ACERTO", "1") 		//Sim	
	oModelG3Q:LoadValue("G3Q_EMISS" , dDataBase)
	oModelG3R:LoadValue("G3R_ACERTO", "1") 		//Sim
	oModelG3R:LoadValue("G3R_ACRAUT", "1")  		//Sim
	oModelG3R:LoadValue("G3R_STATUS", "1")		//Em Aberto	
	oModelG3R:LoadValue("G3R_EMISS" , dDataBase)
	oModelG3R:SetValue("G3R_VLFIM"  , 0) 
	oModelG3R:SetValue("G3R_TOTREC" , 0)
	oModelG3R:SetValue("G3R_TARIFA" , 0)
	oModelG3R:SetValue("G3R_TAXA"   , 0)  
	oModelG3R:SetValue("G3R_EXTRAS" , 0)
	oModelG3R:SetValue("G3R_VLRIMP" , 0)
	oModelG3R:SetValue("G3R_TXREE"  , 0) 
	oModelG3R:SetValue("G3R_VLRSER" , 0)
	oModelG3R:SetValue("G3R_TAXADU" , 0)
	oModelG3R:SetValue("G3R_TXFORN" , 0)
	oModelG3R:SetValue("G3R_VLCOMI" , 0)
	oModelG3R:SetValue("G3R_VLINCE" , 0)	
	oModelG46:DelAllLine()	

	If nTaxa > 0
		If oModelG46:Length() < oModelG46:AddLine()				
			oModelG46:LoadValue("G46_SEQTAX", StrZero(1, TamSX3("G46_SEQTAX")[1]))
			oModelG46:SetValue("G46_EMISS"  , dDataBase)
			oModelG46:SetValue("G46_CODTX"  , SuperGetMV("MV_TURACTX"))
			oModelG46:SetValue("G46_VLBASE" , nTaxa)
		Else
			FwAlertWarning(STR0002, STR0003) 	//Aten��o###Erro ao gerar acerto - TAXA 
			lRet := .F.	
		EndIf 
	EndIf	
	oModelG48B:DelAllLine()
	
	If lRet .And. nTaxaDU > 0 		
		If oModelG48B:Length() < oModelG48B:AddLine()	
			oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.), TamSX3("G48_APLICA")[1]))
			oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACDU"))
			oModelG48B:SetValue("G48_VLACD"  , nTaxaDU)
		Else
			FwAlertWarning(STR0002, STR0004) 	//Aten��o###Erro ao gerar acerto - TAXA DU
			lRet := .F.		
		EndIf	
	EndIf
	
	If lRet .And. nComissao > 0
		If oModelG48B:Length() < oModelG48B:AddLine()	
			oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.), TamSX3("G48_APLICA")[1]))
			oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACCM"))
			oModelG48B:SetValue("G48_VLACD"  , nComissao)
		Else
			FwAlertWarning(STR0002, STR0005) 	//Aten��o###Erro ao gerar acerto - COMISS�O
			lRet := .F.		
		EndIf	
	EndIf
	
	If lRet .And. nIncentivo > 0
		If oModelG48B:Length() < oModelG48B:AddLine()	
			oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.), TamSX3("G48_APLICA")[1]))
			oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACIN") )
			oModelG48B:SetValue("G48_VLACD"  , nIncentivo )
		Else
			FwAlertWarning(STR0002, STR0006) 	//Aten��o###Erro ao gerar acerto - INCENTIVO
			lRet := .F.		
		EndIf	
	EndIf
	
	If lRet
		oModelG48B:GoLine(1)
		//Atualiza Demonstrativo Financeiro
		T34AtuDmFi(oModel, .F.)
		Tur34ItFin(oModel, "3", .T., "3")
		TA039StG48()
	EndIf
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39OpenMdl
Fun��o para retirar restri��es do modelo quanto a inclus�o, altera��o e exclus�o de linhas

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39OpenMdl(oModel, cIdModel, lSubModels)

Local aDependency := oModel:GetDependency(cIdModel)
Local oModelAux	  := oModel:GetModel(cIdModel)
Local nX          := 0

If oModelAux:ClassName() == "FWFORMGRID"
	oModelAux:SetNoInsertLine(.F.)
	oModelAux:SetNoUpdateLine(.F.)
	oModelAux:SetNoDeleteLine(.F.)
EndIf

If lSubModels
	For nX := 1 To Len(aDependency)
		T39OpenMdl(oModel, aDependency[nX][2], lSubModels)
	Next nX
EndIf
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39CalVlDR
Fun��o para calculo do valor do documentos de reservas associados

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39CalVlDR(oModel, nTarifa, nTaxa, nTaxaDU, nComissao, nIncentivo, nMultaRee, nTaxForn)

Local oModelG3R	 := Nil
Local oModelG3Q	 := Nil
Local oModelG48B := Nil
Local oModelG48A := Nil
Local aLines	 := {}
Local nX 		 := 0
Local nY		 := 0

Default oModel 	   := FwModelActive()
Default nTarifa	   := 0
Default nTaxa	   := 0
Default nTaxaDU	   := 0
Default nComissao  := 0
Default nIncentivo := 0
Default nMultaRee  := 0
Default nTaxForn   := 0

aLines := FwSaveRows(oModel)

oModelG3R  := oModel:GetModel("G3R_ITENS")
oModelG3Q  := oModel:GetModel("G3Q_ITENS")
oModelG48B := oModel:GetModel("G48B_ITENS")
oModelG48A := oModel:GetModel("G48A_ITENS")

For nX := 1 To oModelG3R:Length()
	oModelG3R:GoLine(nX)

	If oModelG3Q:GetValue("G3Q_CANFOP") == "1" //IV Cancelado Altera��o FOP
		Loop
	ElseIf oModelG3R:GetValue("G3R_STATUS") = "4" //DR Cancelado	
		Loop
	EndIf

	If !oModelG3R:IsDeleted()
		nTarifa	  += oModelG3R:GetValue("G3R_TARIFA")
		nTaxa	  += oModelG3R:GetValue("G3R_TAXA") + oModelG3R:GetValue("G3R_EXTRAS")
		nMultaRee += oModelG3R:GetValue("G3R_TXREE")
		
		For nY := 1 To oModelG48B:Length()
			If !oModelG48B:IsDeleted(nY) .And. oModelG48B:GetValue("G48_STATUS", nY) == "1" .And. oModelG48B:GetValue("G48_COMSER", nY) == "1"
				Do Case 
					Case oModelG48B:GetValue("G48_CLASS", nY) == "F01"
						nComissao  += IIF(oModelG48B:GetValue("G48_TPACD", nY) == "1", oModelG48B:GetValue("G48_VLACD", nY), oModelG48B:GetValue("G48_VLACD", nY) * -1)
					Case oModelG48B:GetValue("G48_CLASS", nY) == "F02"
						nIncentivo += IIF(oModelG48B:GetValue("G48_TPACD", nY) == "1", oModelG48B:GetValue("G48_VLACD", nY), oModelG48B:GetValue("G48_VLACD", nY) * -1)
					Case oModelG48B:GetValue("G48_CLASS", nY) == "F03"
						nTaxForn   += IIf(oModelG48B:GetValue("G48_TPACD", nY) == "1", oModelG48B:GetValue("G48_VLACD", nY) * -1, oModelG48B:GetValue("G48_VLACD", nY))
				EndCase
			EndIf
		Next nY
		
		For nY := 1 To oModelG48A:Length()
			If !oModelG48A:IsDeleted(nY) .And. !oModelG48A:GetValue("G48_STATUS", nY) == "3" .And. oModelG48A:GetValue("G48_COMSER", nY) == "1"
				If oModelG48A:GetValue("G48_CLASS", nY) == "C11"
					nTaxaDU += IIF(oModelG48A:GetValue("G48_TPACD", nY) == "1", oModelG48A:GetValue("G48_VLACD", nY), oModelG48A:GetValue("G48_VLACD", nY) * -1)
				EndIf
			EndIf
		Next nY
	EndIf
Next nX

FwRestRows(aLines)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39TpFop
Fun��o para verificar o tipo de FOP do documento de reserva

@type function
@author Anderson Toledo
@since 10/08/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39TpFop(oModel)

Local oModelG3R  := oModel:GetModel("G3R_ITENS")
Local oModelG3Q	 := oModel:GetModel("G3Q_ITENS")
Local oModelG9KB := oModel:GetModel("G9KB_ITENS")
Local cTpFOP	 := "0"
Local nX		 := 0
Local nY		 := 0

//"0" - Indefinido
//"1" - Intermedia��o
//"2" - Cart�o Cliente
//"3" - Cart�o Ag�ncia 

//Como gera G9K para acordos e outro para o item da venda, precisa varrer a G9K e ver quem tem Percentual diferente de Zero, pois � assim � parcela da FOP
//se achar um como cart�o ag�ncia � Cart�o Agencia. Se achar como Faturado � Faturado, 
//Cart�o Cliente = G9K_CLIFOR = 2- Fornecedor, G9K_TIPOFOP = 3 - Pag. Direto, G9K_MODO = 5 Cart�o Cliente
//Cart�o Agencia =  G9K_CLIFOR = 2- Fornecedor, G9K_TIPOFOP = 1 - Intermedia��o, G9K_MODO = 4 Cart�o Agencia
//Faturado =  G9K_CLIFOR = 2- Fornecedor, G9K_TIPOFOP = 1 - Intermedia��o, G9K_MODO = 1 Fatura
For nY := 1 To oModelG3R:Length()
	oModelG3R:GoLine(nY) 

	If oModelG3Q:GetValue("G3Q_CANFOP") <> "1"
		For nX := 1 To oModelG9KB:Length()
			If !oModelG9KB:IsDeleted(nX) .And. oModelG9KB:GetValue("G9K_VLPERC", nX) <> 0 
				If oModelG9KB:GetValue("G9K_TPFOP", nX) == "1" .And. oModelG9KB:GetValue("G9K_MODO", nX) == "1"
					cTpFOP := "1" //Intermedia��o
					Exit
				ElseIf oModelG9KB:GetValue("G9K_TPFOP", nX) == "3" .And. oModelG9KB:GetValue("G9K_MODO", nX) == "5"
					cTpFOP := "2" //Cart�o Cliente
					Exit
				ElseIf oModelG9KB:GetValue("G9K_TPFOP", nX) == "1" .And. oModelG9KB:GetValue("G9K_MODO", nX) == "4"
					cTpFOP := "3" //Cart�o Ag�ncia
					Exit
				EndIf	
			EndIf
		Next nX
		Exit
	EndIf		
Next nY
	
Return cTpFOP

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39TpMoeda
Fun��o para verificar o tipo de MOEDA do documento de reserva

@type function
@author Anderson Toledo
@since 10/08/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39TpMoeda(oModel)

Local aLines  := FwSaveRows(oModel)
Local cMoeda  := ""
Local nX	  := 0
Local oMdlG3Q := oModel:GetModel("G3Q_ITENS")
Local oMdlG3R := oModel:GetModel("G3R_ITENS")

For nX := 1 To oMdlG3R:Length() 
	oMdlG3R:GoLine(nX)
	If oMdlG3Q:GetValue("G3Q_CANFOP") == "0" .Or. oMdlG3Q:GetValue("G3Q_CANFOP") == "1"
		cMoeda := oMdlG3R:GetValue("G3R_MOEDA")
	EndIf
Next nX

FwRestRows(aLines)
	
Return cMoeda

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39Diverg
Fun��o para verificar a diverg�ncia entre o item da fatura e os documentos de reservas associados

@type function
@author anderson.toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39Diverg(oModel, lDivergente, nTarifa, nTaxa, nTaxaDU, nComissao, nIncentivo, nMultaRee, nTaxForn)

Local oModelG6J  := oModel:GetModel("G6J_MASTER")
Local oModelG6I	 := oModel:GetModel("G6I_ITENS")	//Itens da fatura
Local oModelG3R  := oModel:GetModel("G3R_ITENS")
Local cCampos	 := ""
Local lReembolso := oModelG6I:GetValue("G6I_TIPOIT") == "3"
Local nDuFatura	 := 0

Default lDivergente := .F.		
Default nTarifa		:= 0
Default nTaxa		:= 0
Default nTaxaDU		:= 0
Default nComissao	:= 0
Default nIncentivo	:= 0
Default nMultaRee	:= 0
Default nTaxForn	:= 0

cCampos := AllTrim(oModelG6I:GetValue("G6I_CODDIV")) 

T39CalVlDR(oModel, @nTarifa, @nTaxa, @nTaxaDU, @nComissao, @nIncentivo, @nMultaRee, @nTaxForn)

// TARIFA
If oModelG6I:GetValue("G6I_TARIFA") <> nTarifa
	cCampos += IIF(!Empty(cCampos), "; G6I_TARIFA", "G6I_TARIFA")
	
	If nTarifa >= 0 .Or. lReembolso
		nTarifa := oModelG6I:GetValue("G6I_TARIFA") - nTarifa 
	Else
		nTarifa := nTarifa - oModelG6I:GetValue("G6I_TARIFA")
	EndIf
Else
	nTarifa := 0	
EndIf

// TAXA
If oModelG6I:GetValue("G6I_TAXAS") <> nTaxa
	cCampos += IIF(!Empty(cCampos), "; G6I_TAXAS", "G6I_TAXAS") 

	If nTaxa >= 0 .Or. lReembolso
		nTaxa := oModelG6I:GetValue("G6I_TAXAS") - nTaxa  
	Else
		nTaxa := nTaxa - oModelG6I:GetValue("G6I_TAXAS")		 
	EndIf
Else
	nTaxa := 0	
EndIf

// TAXA REEMBOLSO
If oModelG6I:GetValue("G6I_TXREE") <> nMultaRee
	cCampos += IIF(!Empty(cCampos), "; G6I_TXREE", "G6I_TXREE")

	If nMultaRee >= 0 .Or. lReembolso
		nMultaRee := oModelG6I:GetValue("G6I_TXREE") - nMultaRee
	Else
		nMultaRee := nMultaRee - oModelG6I:GetValue("G6I_TXREE")
	EndIf
Else
	nMultaRee := 0	
EndIf

//------------------------------------------------------------------------------------------------------
//Na G3R, quando � reembolso os campos G3R_TAXADU, G3R_TXFORN, G3R_VLCOMI, G3R_VLINCE s�o negativos
//ent�o � necess�rio converter a G6I antes da compara��o de diverg�ncia de valores
//------------------------------------------------------------------------------------------------------
nDuFatura := TUR34ROUND(oModelG6I:GetValue("G6I_TAXADU") * oModel:GetValue("G3R_ITENS", "G3R_TXCAMB") / oModel:GetValue("G3Q_ITENS", "G3Q_TXCAMB"), "G48_VLACD")
If (nDuFatura <> nTaxaDU)
	If nTaxaDU >= 0 .Or. lReembolso
		If lReembolso
			nTaxaDU := (nDuFatura * -1) - nTaxaDU
		Else
			nTaxaDU := nDuFatura - nTaxaDU
		EndIf	
	Else
		nTaxaDU := nTaxaDU - nDuFatura
	EndIf
	
	If nTaxaDU <> 0
		cCampos += IIF(!Empty(cCampos), "; G6I_TAXADU", "G6I_TAXADU")
	EndIf	
Else
	nTaxaDU := 0	
EndIf

// COMISSAO
If oModelG6I:GetValue("G6I_VLRCOM") <> nComissao
	If nComissao >= 0 .Or. lReembolso
		If lReembolso
			nComissao := (oModelG6I:GetValue("G6I_VLRCOM") * -1) - nComissao
		Else
			nComissao := oModelG6I:GetValue("G6I_VLRCOM") - nComissao
		EndIf	
	Else
		nComissao := nComissao - oModelG6I:GetValue("G6I_VLRCOM")
	EndIf
	 
	If nComissao <> 0
		cCampos += IIF(!Empty(cCampos), "; G6I_VLRCOM", "G6I_VLRCOM")
	EndIf
Else
	nComissao := 0	
EndIf

// INCENTIVO
If oModelG6I:GetValue("G6I_VLRINC") <> nIncentivo
	If nIncentivo >= 0 .Or. lReembolso 
		If lReembolso
			nIncentivo := (oModelG6I:GetValue("G6I_VLRINC") * -1) - nIncentivo
		Else
			nIncentivo := oModelG6I:GetValue("G6I_VLRINC") - nIncentivo
		EndIf	
	Else
		nIncentivo := nIncentivo - oModelG6I:GetValue("G6I_VLRINC")
	EndIf
	
	If nIncentivo <> 0
		cCampos += IIF(!Empty(cCampos), "; G6I_VLRINC", "G6I_VLRINC")
	EndIf
Else
	nIncentivo := 0	
EndIf

// TAXA CCDU
If oModelG6I:GetValue("G6I_TXCCDU") <> nTaxForn
	If nTaxForn >= 0 .Or. lReembolso
		If lReembolso
			nTaxForn := (oModelG6I:GetValue("G6I_TXCCDU") * -1) - nTaxForn
		Else
			nTaxForn := oModelG6I:GetValue("G6I_TXCCDU") - nTaxForn
		EndIf	
	Else
		nTaxForn := nTaxForn - oModelG6I:GetValue("G6I_TXCCDU")
	EndIf

	If nTaxForn <> 0
		cCampos += IIF(!Empty(cCampos), "; G6I_TXCCDU", "G6I_TXCCDU")
	EndIf
Else
	nTaxForn := 0
EndIf

If !Empty(cCampos)
	oModelG6I:LoadValue("G6I_DIVERG", "1")	//Sim
	oModelG6I:LoadValue("G6I_CODDIV", SubStr(cCampos, 1, TamSX3("G6I_CODDIV")[1]))		
	oModelG3R:LoadValue("G3R_DIVERG", "1")	
	oModelG6J:LoadValue("G6J_DIVERG", "1")		
	lDivergente := .T.		
Else
	oModelG6I:LoadValue("G6I_DIVERG", "2")	//N�o
	oModelG6I:LoadValue("G6I_CODDIV", "")
	oModelG3R:LoadValue("G3R_DIVERG", "2")
	oModelG6J:LoadValue("G6J_DIVERG", "2")	
	lDivergente := .F.
EndIf

oModelG6J := Nil
oModelG6I := Nil
oModelG3R := Nil

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39GerAcer
Fun��o para gera��o do item de acerto

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39GerAcer(oModel)

Local oModelG3R	  := oModel:GetModel("G3R_ITENS")		// Documento de reserva
Local oModelG3Q	  := oModel:GetModel("G3Q_ITENS")		// Item de venda
Local oModelG44   := oModel:GetModel("G44_ITENS")		// Tarifas
Local oModelG46   := oModel:GetModel("G46_ITENS")		// Taxas
Local oModelG47	  := oModel:GetModel("G47_ITENS")		// Extras
Local oModelG4E   := oModel:GetModel("G4E_ITENS")		// Reembolso
Local oModelG48A  := oModel:GetModel("G48A_ITENS") 		// Acordos Fornecedor
Local oModelG48B  := oModel:GetModel("G48B_ITENS") 		// Acordos Fornecedor
Local aCopyValues := {}
Local aValuesG44  := {}
Local aValuesG4E  := {}
Local cSeqG3R	  := {}
Local cSeqOrig	  := oModelG3R:GetValue("G3R_NUMSEQ")
Local cNatureza	  := ""
Local cG44BasTar  := ""
Local cG44TPTAR	  := ""
Local cG4EDTSOLI  := ""
Local cG4EBasTar  := ""
Local lRet 		  := .T.
Local lDivergente := .F.
Local lReembolso  := oModelG3R:GetValue("G3R_OPERAC") == "2"
Local lExistFunc  := Findfunction("U_TURNAT")
Local nX		  := 0
Local nTarifa	  := 0
Local nTaxa		  := 0
Local nTaxaDU	  := 0
Local nComissao	  := 0
Local nIncentivo  := 0
Local nTaxReemb	  := 0
Local nTaxForn	  := 0

T34Segmento(Val(oModel:GetValue("G3P_FIELDS", "G3P_SEGNEG")))

cG44BasTar := oModelG44:GetValue("G44_BASTAR")
cG44TPTAR  := oModelG44:GetValue("G44_TPTAR")
cG4EDTSOLI := oModelG4E:GetValue("G4E_DTSOLI")
cG4EBasTar := oModelG4E:GetValue("G4E_BASTAR")

T39Diverg(oModel, @lDivergente, @nTarifa, @nTaxa, , , , @nTaxReemb, )

If lDivergente
	T39OpenMdl(oModel, "G3R_ITENS", .T.)
	
	//Posiciona na primeira G3R v�lida
	For nX := 1 To oModelG3R:Length()
		oModelG3R:GoLine(nX)
		If oModelG3Q:GetValue("G3Q_CANFOP") == "0"
			Exit
		EndIf
	Next nX
	
	aCopyValues	:= TURxGetVls("G3R_ITENS", .T., {"G44_ITENS", "G46_ITENS", "G47_ITENS", "G4CA_ITENS", "G4CB_ITENS", "G4E_ITENS"}, {}, oModel)

	If nTarifa <> 0 .Or. lReembolso	
		If lReembolso
			aValuesG4E	:= TURxGetVls("G4E_ITENS", , , , oModel, {"G4E_ITENS"})
		Else
			aValuesG44	:= TURxGetVls("G44_ITENS", , , , oModel, {"G44_ITENS"})			
		EndIf
	EndIf	
	
	If TURxSetVs(aCopyValues, {}, oModel)
		oModel:LoadValue("G6I_ITENS", "G6I_ACERTO", "1")

		cSeqG3R := TA042MaxSq(oModel)
		TA042SetRec("G3R_ITENS", {{"_NUMSEQ", cSeqG3R}}, , .F.)
		
		lRet := oModelG3R:LoadValue("G3R_SEQACR", cSeqOrig)
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_ATUIF" , "2") 			// Atualiza IF (1=Apenas Cliente;2=Cliente e Fornecedor)			
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_STATUS", "1")  			// Sim
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_ACERTO", "1") 			// Sim	
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_DTINC" , dDataBase)
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_VLAFOP", 0)
		lRet := lRet .And. oModelG3Q:LoadValue("G3Q_ACRFOP", "2")			// N�o
		lRet := lRet .And. oModelG3R:LoadValue("G3R_ACERTO", "1") 			// Sim
		lRet := lRet .And. oModelG3R:LoadValue("G3R_ACRAUT", "1")  			// Sim
		lRet := lRet .And. oModelG3R:LoadValue("G3R_STATUS", "1")			// Em Aberto	
		lRet := lRet .And. oModelG3R:LoadValue("G3R_DTINC" , dDataBase)
		lRet := lRet .And. oModelG3R:LoadValue("G3R_DIVERG", "1")
		lRet := lRet .And. oModelG3Q:SetValue("G3Q_TAXADU" , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLFIM"  , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TOTREC" , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TARIFA" , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_TAXA"   , 0)
		lRet := lRet .And. oModelG3R:SetValue("G3R_EXTRAS" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLRIMP" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_TXREE"  , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLRSER" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_TXFORN" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLCOMI" , 0) 
		lRet := lRet .And. oModelG3R:SetValue("G3R_VLINCE" , 0)

		If lRet
			oModelG44:SetNoInsertLine(.F.)	
			oModelG44:SetNoDeleteLine(.F.)	
	
			oModelG46:SetNoInsertLine(.F.)	
			oModelG46:SetNoDeleteLine(.F.)	
	
			oModelG47:SetNoInsertLine(.F.)
			oModelG47:SetNoDeleteLine(.F.)
	
			oModelG48A:SetNoInsertLine(.F.)
			oModelG48A:SetNoDeleteLine(.F.)
	
			oModelG48B:SetNoInsertLine(.F.)
			oModelG48B:SetNoDeleteLine(.F.)
	
			oModelG4E:SetNoInsertLine(.F.)
			oModelG4E:SetNoDeleteLine(.F.)
			
			//Tamb�m � verificado a taxa de reembolso aqui, pois esta � preenchida na G4E tamb�m
			If nTarifa <> 0 .Or. lReembolso
				If lReembolso
					If oModelG4E:IsEmpty() .Or. oModelG4E:Length() < oModelG4E:AddLine()
						If (lRet := TURxSetVs(aValuesG4E, {"G4E_TARPAG", "G4E_TPGINT", "G4E_TARUTI", "G4E_TREEMB", "G4E_TXPG", "G4E_TXREEM", "G4E_EXREEM"}, oModel))
							TA042SetRec("G4E_ITENS", {{"_NUMSEQ", cSeqG3R}}, , .F.)
						
							lRet := oModelG4E:LoadValue("G4E_TARPAG", nTarifa) .And. lRet
							lRet := oModelG4E:LoadValue("G4E_TPGINT", nTarifa) .And. lRet
							lRet := oModelG4E:SetValue("G4E_TARUTI" , 0) 	   .And. lRet
							lRet := oModelG4E:SetValue("G4E_TREEMB" , nTarifa) .And. lRet
							
							If nTaxa <> 0
								lRet := oModelG4E:LoadValue("G4E_TXPG" , nTaxa) .And. lRet
								lRet := oModelG4E:SetValue("G4E_TXREEM", nTaxa) .And. lRet
							Else
								lRet := oModelG4E:LoadValue("G4E_TXPG" , 0) .And. lRet
								lRet := oModelG4E:SetValue("G4E_TXREEM", 0) .And. lRet					
							EndIf
						EndIf
					EndIf
									
					If !lRet
						IF IsInCallStack("Tu038Asso")
							AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "	
						Else
							JurShowErro(oModel:GetModel():GetErrorMessage())
						Endif
					EndIf	
				Else
					If oModelG44:IsEmpty() .Or. oModelG44:Length() < oModelG44:AddLine()
						If (lRet := TURxSetVs(aValuesG44, {"G44_MENOR", "G44_CHEIA", "G44_MAIOR", "G44_PUBLIC", "G44_SAVING", "G44_OPORTU", "G44_EARLY", "G44_LATE", "G44_PLALIM"}, oModel) .And. oModelG44:SetValue("G44_TARBAS", nTarifa / oModelG44:GetValue("G44_TXCAMB")))
							TA042SetRec("G44_ITENS", {{"_NUMSEQ", cSeqG3R}}, , .F.)
						EndIf
						
						If !lRet
							IF IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "	
							Else
								JurShowErro(oModel:GetModel():GetErrorMessage())
							Endif 
						EndIf
					Else
						IF IsInCallStack("Tu038Asso")
							AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0008 + " - " + STR0002) 			// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - TARIFA
						Else
							FwAlertWarning(STR0008, STR0002) 	//Aten��o###Erro ao gerar acerto - TARIFA
						Endif 
						lRet := .F.	
					EndIf 
				EndIf
			EndIf	
	
			If lRet .And. nTaxa <> 0 .And. !lReembolso
				If oModelG46:IsEmpty() .Or. oModelG46:Length() < oModelG46:AddLine()
					lRet := oModelG46:LoadValue("G46_SEQTAX", StrZero(oModelG46:Length(.T.) + 1, TamSX3("G46_SEQTAX")[1]))
					lRet := oModelG46:SetValue("G46_EMISS"	, dDataBase) .And. lRet
					lRet := oModelG46:SetValue("G46_CODTX"	, SuperGetMV("MV_TURACTX")) .And. lRet
					lRet := oModelG46:SetValue("G46_VLBASE" , nTaxa) .And. lRet
	
					If !lRet
						IF IsInCallStack("Tu038Asso")
							AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "		
						Else
							JurShowErro(oModel:GetModel():GetErrorMessage())
						Endif
					EndIf
				Else	
					IF IsInCallStack("Tu038Asso")
						AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0003 + " - " + STR0002)			// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - TAXA	
					Else
						FwAlertWarning(STR0003, STR0002) 	//Aten��o###Erro ao gerar acerto - TAXA
					Endif
					lRet := .F.	
				EndIf 
			EndIf	
	
			If lRet .And. nTaxReemb <> 0
				If oModelG46:IsEmpty() .Or. oModelG46:Length() < oModelG46:AddLine()
					lRet := oModelG46:LoadValue("G46_SEQTAX", StrZero(oModelG46:Length(.T.) + 1, TamSX3("G46_SEQTAX")[1]))
					lRet := oModelG46:SetValue("G46_EMISS"	, dDataBase) .And. lRet
					lRet := oModelG46:SetValue("G46_CODTX"	, SuperGetMV("MV_TURACTX")) .And. lRet
					lRet := oModelG46:SetValue("G46_VLBASE" , nTaxReemb) .And. lRet
	
					If !lRet
						IF IsInCallStack("Tu038Asso")
							AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "			
						Else
							JurShowErro(oModel:GetModel():GetErrorMessage())
						Endif
					EndIf
				Else
					IF IsInCallStack("Tu038Asso")
						AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0003 + " - " + STR0002)			// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - TAXA	
					Else
						FwAlertWarning(STR0003, STR0002) 	//Aten��o###Erro ao gerar acerto - TAXA
					Endif 
					lRet := .F.	
				EndIf 
			EndIf	
		
			If lRet
				//Recalcula os valores do DR
				TA042AtuAc(oModel)
		
				//Mantem apenas acordos de % abertos sem apura��o
				TA042DlAcF(oModel, .T., , .T.)
		
				//Recalcula acordos de fornecedor e cliente
				If !oModelG48B:IsEmpty()
					T34CalcAco( , , "2", "0", .T.)
				EndIf
		
				If !oModelG48A:IsEmpty()
					T34CalcAco( , , "1", "0", .T.)
				EndIf
		
				//Ap�s acerto de taxa ADe tarifa, verifica se os valores proveniente de acordos continuam com diverg�ncia
				T39Diverg(oModel, @lDivergente, , , @nTaxaDU, @nComissao, @nIncentivo, , @nTaxForn)
			
				If lDivergente
					If lRet .And. nTaxaDU <> 0 		
						If oModelG48A:IsEmpty() .Or. oModelG48A:Length() < oModelG48A:AddLine()
							lRet := oModelG48A:LoadValue("G48_APLICA", StrZero(oModelG48A:Length(.T.) + 1, TamSX3("G48_APLICA")[1]))
							lRet := oModelG48A:SetValue("G48_CODACD" , SuperGetMV("MV_TURACDU")) .And. lRet
							lRet := oModelG48A:SetValue("G48_VLACD"  , nTaxaDU) .And. lRet	
							
							If lRet .And. lExistFunc
								If !Empty((cNatureza := T39DGetNat(oModel, 1)))
									lRet := oModelG48A:LoadValue("G48_NATURE", cNatureza)
								EndIf
							EndIf
							
							If !lRet
								IF IsInCallStack("Tu038Asso")
									AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "	
								Else
								   JurShowErro(oModel:GetModel():GetErrorMessage())
								Endif
							Else
								//Se houve acerto de DU, pode ser que os valores de comiss�o, incentivo ou taxa fornecedor
								//tenham sido alterados por um acordo de fornecedor com base na DU
								T39ReScan(oModel, @nComissao, @nIncentivo, @nTaxForn)
							EndIf	
						Else
							IF IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0004 + " - " + STR0002)		// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - TAXA DU	
							Else
								FwAlertWarning(STR0004, STR0002) 	//Aten��o###Erro ao gerar acerto - TAXA DU
							Endif
							lRet := .F.		
						EndIf	
					EndIf
			
					If lRet .And. nComissao <> 0
						If oModelG48B:IsEmpty() .Or. oModelG48B:Length() < oModelG48B:AddLine()
							lRet := oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.) + 1, TamSX3("G48_APLICA")[1]))
							lRet := oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACCM")) .And. lRet
							lRet := oModelG48B:SetValue("G48_VLACD"  , nComissao) .And. lRet
					
							If lRet .And. lExistFunc
								If !Empty((cNatureza := T39DGetNat(oModel)))
									lRet := oModelG48B:LoadValue("G48_NATURE", cNatureza)
								EndIf
							EndIf
					
							If !lRet
								IF IsInCallStack("Tu038Asso")
									AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "		
								Else
									JurShowErro(oModel:GetModel():GetErrorMessage())
								Endif 
							EndIf
						Else
							IF IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0005 + " - " + STR0002)	// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - COMISSAO	
							Else
								FwAlertWarning(STR0005, STR0002) 	//Aten��o###Erro ao gerar acerto - COMISSAO
							Endif 
							lRet := .F.		
						EndIf	
					EndIf
			
					If lRet .And. nIncentivo <> 0
						If oModelG48B:IsEmpty() .Or. oModelG48B:Length() < oModelG48B:AddLine()
							lRet := oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.) + 1, TamSX3("G48_APLICA")[1]))
							lRet := oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACIN")) .And. lRet
							lRet := oModelG48B:SetValue("G48_VLACD"  , nIncentivo) .And. lRet
		
							If lRet .And. lExistFunc
								If !Empty((cNatureza := T39DGetNat(oModel)))
									lRet := oModelG48B:LoadValue("G48_NATURE", cNatureza)
								EndIf
							EndIf
					
							If !lRet
								IF IsInCallStack("Tu038Asso")
									AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])		// "RV: " // " ITEM: "  // " SEQ: "		
								Else
									JurShowErro(oModel:GetModel():GetErrorMessage())
								Endif 
							EndIf
						Else
							IF IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0006 + " - " + STR0002)		// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - INCENTIVO	
							Else
								FwAlertWarning(STR0006, STR0002) 	//Aten��o###Erro ao gerar acerto - INCENTIVO
							Endif 
							lRet := .F.		
						EndIf	
					EndIf
				
					If lRet .And. nTaxForn <> 0
						If oModelG48B:IsEmpty() .Or. oModelG48B:Length() < oModelG48B:AddLine()
							lRet := oModelG48B:LoadValue("G48_APLICA", StrZero(oModelG48B:Length(.T.) + 1, TamSX3("G48_APLICA")[1]))
							lRet := oModelG48B:SetValue("G48_CODACD" , SuperGetMV("MV_TURACTF")) .And. lRet
							lRet := oModelG48B:SetValue("G48_VLACD"  , nTaxForn) .And. lRet
		
							If lRet .And. lExistFunc
								If !Empty((cNatureza := T39DGetNat(oModel)))
									lRet := oModelG48B:LoadValue("G48_NATURE", cNatureza)
								EndIf
							EndIf
		
							If !lRet
								IF IsInCallStack("Tu038Asso")
									AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + oModel:GetErrorMessage()[6])	// "RV: " // " ITEM: "  // " SEQ: "		
								Else
									JurShowErro(oModel:GetModel():GetErrorMessage())
								Endif 
							EndIf
						Else
							IF IsInCallStack("Tu038Asso")
								AtuT38Msg(STR0010 + oModelG3R:GetValue("G3R_NUMID") + STR0011 + oModelG3R:GetValue("G3R_IDITEM") + STR0012 + oModelG3R:GetValue("G3R_NUMSEQ") + CRLF + STR0002 + " - " + STR0009)		// "RV: " // " ITEM: "  // " SEQ: "	//Aten��o###Erro ao gerar acerto - TAXA FORNECEDOR
							Else
								FwAlertWarning(STR0002, STR0009) 	//Aten��o###"Erro ao gerar acerto - TAXA FORNECEDOR"
							EndIf 
							lRet := .F.
						EndIf
					EndIf
		
					If lRet
						//Recalcula novamente os  acordos de fornecedor e cliente para atualizar os acordos de repasse corretamente
						If !oModelG48B:IsEmpty()
							T34CalcAco( , , "2", "0", .T.)
						EndIf
		
						If !oModelG48A:IsEmpty()
							T34CalcAco( , , "1", "0", .T.)
						EndIf
					EndIf
				EndIf
	
				If lRet
					T39OpenMdl(oModel, "G3R_ITENS", .T.) //Reabre para edi��o o modelo, o TURA034 pode ter bloqueado
					oModelG48B:GoLine(1)
		
					//Atualiza Demonstrativo Financeiro
					T34AtuDmFi(oModel, .F.)
	
					//Por se tratar do registro do acerto, sempre s�o refeitos os itens financeiros do cliente e fornecedor
					Tur34ItFin(oModel, "3", .T., "3")
		
					If !oModelG48B:IsEmpty()
						For nX := 1 To oModelG48B:Length()
							oModelG48B:GoLine(nX)
							TA039TrRec(.T.)
						Next nX
					EndIf
				EndIf
			EndIf	
	
			oModelG44:SetNoInsertLine(.T.)	
			oModelG44:SetNoDeleteLine(.T.)	
	
			oModelG46:SetNoInsertLine(.T.)	
			oModelG46:SetNoDeleteLine(.T.)	
	
			oModelG47:SetNoInsertLine(.T.)
			oModelG47:SetNoDeleteLine(.T.)
	
			oModelG48A:SetNoInsertLine(.T.)
			oModelG48A:SetNoDeleteLine(.T.)
	
			oModelG48B:SetNoInsertLine(.T.)
			oModelG48B:SetNoDeleteLine(.T.)
	
			oModelG4E:SetNoInsertLine(.T.)
			oModelG4E:SetNoDeleteLine(.T.)
		EndIf
	Else	
		lRet := .F.
	EndIf
EndIf

If !lRet
	oModelG3R:DeleteLine() 
EndIf

T34Segmento(0)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39DGetNat
Fun��o para retorno da Natura da opera��o

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39DGetNat(oModel, nTipo)

Local aArea      := GetArea()
Local oModelG3P  := oModel:GetModel("G3P_FIELDS")
Local oModelG3Q  := Nil
Local oModelG3R  := Nil
Local oModelAcd	 := Nil
Local cFilialAux := Nil
Local cNatureza  := ""
Local cTipo 	 := ""
Local cProduto	 := ""	
Local cCliFor	 := ""
Local cCodigo	 := ""
Local cLoja		 := ""
Local lExistFunc := Findfunction("U_TURNAT")

Default nTipo	 := 2

If lExistFunc
	If nTipo == 1
		oModelAcd  := oModel:GetModel("G48A_ITENS")
		oModelG3Q  := oModel:GetModel("G3Q_ITENS")
		cProduto   := oModelG3Q:GetValue("G3Q_PROD")	
		cCliFor	   := "1"
		cCodigo	   := oModelG3Q:GetValue("G3Q_CLIENT")
		cLoja	   := oModelG3Q:GetValue("G3Q_LOJA")
		cFilialAux := cFilAnt
	Else
		oModelAcd  := oModel:GetModel("G48B_ITENS")
		oModelG3R  := oModel:GetModel("G3R_ITENS")
		cProduto   := oModelG3R:GetValue("G3R_PROD")
		cCliFor	   := "2"
		cCodigo	   := oModelG3R:GetValue("G3R_FORREP")
		cLoja	   := oModelG3R:GetValue("G3R_LOJREP")
		cFilialAux := oModelG3R:GetValue("G3R_MSFIL")
	EndIf
	
	If Empty(oModelAcd:GetValue("G48_NATURE"))
		If oModelAcd:GetValue("G48_TPACD") == "1"
			cTipo := "3"
		ElseIf oModelAcd:GetValue("G48_TPACD") == "2"
			cTipo := "4"
		EndIf
	
		cNatureza := U_TURNAT(cFilialAux, cTipo, oModelAcd:GetValue("G48_CLASS"), oModelG3P:GetValue("G3P_SEGNEG"), cProduto, cCliFor, cCodigo, cLoja)
		SED->(DbSetOrder(1))	// ED_FILIAL+ED_CODIGO
		If !SED->(DbSeek(xFilial("SED") + cNatureza))
			cNatureza := ""
		EndIf
	EndIf
EndIf
	
RestArea(aArea)

Return cNatureza

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039StG48
Fun��o para 

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039StG48(lForce)

Local aLines	 := FwSaveRows()
Local oModel 	 := FwModelActive()
Local oModelG48B := oModel:GetModel("G48B_ITENS")
Local nOperation := oModel:GetOperation()
Local nX		 := 0

Default lForce := .F.

If nOperation <> MODEL_OPERATION_VIEW .And. nOperation <> MODEL_OPERATION_DELETE
	If !oModelG48B:IsEmpty()
		For nX := 1 To oModelG48B:Length()
			oModelG48B:GoLine(nX)
			If !oModelG48B:IsDeleted()
				TA039TrRec(lForce)
			EndIf
		Next
	EndIf
EndIf

FwRestRows(aLines)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ReScan
Verifica novamente se existem divergencias ap�s aplicar DU para verificar se mudou algum valor de acerto.

@author paulo.barbosa
@since 15/05/2018
@version 1.0
@type function
/*/                      
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ReScan(oModel, nComissao, nIncentivo, nTaxForn)

Local cCampos	 := ""
Local oModelG6I	 := oModel:GetModel("G6I_ITENS")
Local lReembolso := oModelG6I:GetValue("G6I_TIPOIT") == "3"

cCampos := AllTrim(oModelG6I:GetValue("G6I_CODDIV"))

nComissao  := 0
nIncentivo := 0
nTaxForn   := 0

T39CalVlDR(oModel, , , , @nComissao, @nIncentivo, , @nTaxForn)

//------------------------------------------------------------------------------------------------------
//Na G3R, quando � reembolso os campos G3R_TAXADU, G3R_TXFORN, G3R_VLCOMI, G3R_VLINCE s�o negativos
//ent�o � necess�rio converter a G6I antes da compara��o de diverg�ncia de valores
//------------------------------------------------------------------------------------------------------
If oModelG6I:GetValue("G6I_VLRCOM") <> nComissao
	If nComissao >= 0 .Or. lReembolso
		If lReembolso
			nComissao := (oModelG6I:GetValue("G6I_VLRCOM") * -1) - nComissao
		Else
			nComissao := oModelG6I:GetValue("G6I_VLRCOM") - nComissao
		EndIf	
	Else
		nComissao := nComissao - oModelG6I:GetValue("G6I_VLRCOM")
	EndIf
	 
	If nComissao <> 0 .And. !("G6I_VLRCOM" $ cCampos)
		cCampos += IIF(!Empty(cCampos), "; G6I_VLRCOM", "G6I_VLRCOM")
	EndIf
Else
	nComissao := 0
EndIf

If oModelG6I:GetValue("G6I_VLRINC") <> nIncentivo
	If nIncentivo >= 0 .Or. lReembolso 
		If lReembolso
			nIncentivo := (oModelG6I:GetValue("G6I_VLRINC") * -1) - nIncentivo
		Else
			nIncentivo := oModelG6I:GetValue("G6I_VLRINC") - nIncentivo
		EndIf	
	Else
		nIncentivo := nIncentivo - oModelG6I:GetValue("G6I_VLRINC")
	EndIf
	
	If nIncentivo <> 0 .And. !("G6I_VLRINC" $ cCampos)
		cCampos += IIF(!Empty(cCampos), "; G6I_VLRINC", "G6I_VLRINC")
	EndIf
Else
	nIncentivo := 0
EndIf

If oModelG6I:GetValue("G6I_TXCCDU") <> nTaxForn
	If nTaxForn >= 0 .Or. lReembolso
		If lReembolso
			nTaxForn := (oModelG6I:GetValue("G6I_TXCCDU") * -1) - nTaxForn
		Else
			nTaxForn := oModelG6I:GetValue("G6I_TXCCDU") - nTaxForn
		EndIf	
	Else
		nTaxForn := nTaxForn - oModelG6I:GetValue("G6I_TXCCDU")
	EndIf

	If nTaxForn <> 0 .And. !("G6I_TXCCDU" $ cCampos)
		cCampos += IIF(!Empty(cCampos), "; G6I_TXCCDU", "G6I_TXCCDU")
	EndIf
Else
	nTaxForn := 0
EndIf

If AllTrim(oModelG6I:GetValue("G6I_CODDIV")) != AllTrim(cCampos)
	oModelG6I:LoadValue("G6I_CODDIV", SubStr(cCampos, 1, TamSX3("G6I_CODDIV")[1]))
EndIf

Return .T.

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039LOG
Fun��o respons�vel recupera��o do erro ocorrido no modelo de dados.

@type 		Function
@author 	Thiago Tavares
@since 		05/11/2018
@version 	12.1.17
/*/
//+----------------------------------------------------------------------------------------
Static Function TA039LOG(oModel)

AutoGrLog("Id do formul�rio de origem:" + " [" + AllToChar(oModel:GetErrorMessage()[1]) + "]") // 
AutoGrLog("Id do campo de origem: "     + " [" + AllToChar(oModel:GetErrorMessage()[2]) + "]") // 
AutoGrLog("Id do formul�rio de erro: "  + " [" + AllToChar(oModel:GetErrorMessage()[3]) + "]") // 
AutoGrLog("Id do campo de erro: "       + " [" + AllToChar(oModel:GetErrorMessage()[4]) + "]") // 
AutoGrLog("Id do erro: "                + " [" + AllToChar(oModel:GetErrorMessage()[5]) + "]") // 
AutoGrLog("Mensagem do erro: "          + " [" + AllToChar(oModel:GetErrorMessage()[6]) + "]") // 
AutoGrLog("Mensagem da solu��o: "       + " [" + AllToChar(oModel:GetErrorMessage()[7]) + "]") // 
AutoGrLog("Valor atribu�do: "           + " [" + AllToChar(oModel:GetErrorMessage()[8]) + "]") // 
AutoGrLog("Valor anterior: "            + " [" + AllToChar(oModel:GetErrorMessage()[9]) + "]") // 
MostraErro()

Return