#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039E.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39DelAss
Fun��o para estorno da associa��o

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur39DelAss(aFilter)

Local aArea     := GetArea()
Local oModel 	:= FwLoadModel("TURA039MDL")
Local oModelG6J := oModel:GetModel("G6J_MASTER") 
Local oModelG6I := oModel:GetModel("G6I_ITENS") 
Local lRet		:= .T.
Local nX		:= 0

If len(aFilter) > 0
	oModelG6I:SetLoadFilter({{"G6I_ITEM", T39Arr2Str(aFilter), MVC_LOADFILTER_IS_CONTAINED}})
EndIf

oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:SetVldActivate({|| .T.})
oModel:SetCommit({|oModel| FwFormCommit(oModel)})
oModelG6J:SetOnlyQuery(.T.)
oModelG6I:SetOnlyQuery(.T.)
oModel:Activate()

T39OpenMdl(oModel, "G3R_ITENS", .T.)

If oModelG6I:Length() == Len(aFilter)
	For nX := 1 To oModelG6I:Length()
		oModelG6I:GoLine(nX)
		If !Tur39EsAss(oModel, .F.)
			lRet := .F.
			Exit				
		EndIf
	Next nX
Else
	lRet := .F.
EndIf

//Retira o valor dos campos utilizados no Relation, impedindo que o model reatribua os valores na G3R
oModelG6J:LoadValue("G6J_FILIAL", "")
oModelG6J:LoadValue("G6J_CONCIL", "")

If lRet
	If (lRet := oModel:VldData())
		oModel:CommitData()
	EndIf
EndIf

RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39EsAss
Fun��o para controle do estorno do item da fatura e documento de reserva da associa��o

@type function
@author anderson.toledo
@since 10/08/2016
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur39EsAss(oModel, lLoad)

Local lRet := .T.
	
Default oModel := FwModelActive()
	
If (lRet := Tur39EsG3R(oModel, lLoad))
	lRet := Tur39EsG6I(oModel)
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39EsG3R
Fun��o para estorno do documento de reserva da associa��o

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Tur39EsG3R(oModel, lLoad)

Local oMdlG3R := oModel:GetModel("G3R_ITENS")
Local cSts	  := ""
Local lRet	  := .T.
Local nX	  := 0

DEFAULT lLoad := .T.

For nX := 1 To oMdlG3R:Length()
	oMdlG3R:GoLine(nX)
	
	If !lRet
		Exit
	EndIf			
	
	If oMdlG3R:IsDeleted()
		Loop
	EndIf
	
	//Se acerto autom�tico, exclui item do RV 
	If oMdlG3R:GetValue("G3R_ACERTO") == "1" .And. oMdlG3R:GetValue("G3R_ACRAUT") == "1" 
		If lLoad
			lRet := Tur39ExG3R(oModel)
		Else
			lRet := lRet .And. oMdlG3R:DeleteLine()
		EndIf		
	Else
		If lLoad .OR. oMdlG3R:IsEmpty()
			cSts := "UPDATE " + RetSqlName("G3R")
			cSts += 	" SET G3R_CONCIL = '', G3R_FILCON = '', G3R_FATURA = '', G3R_DIVERG = '3'"   // 3 - N�o Associado 
			cSts += 	" WHERE G3R_FILIAL   = '" + oMdlG3R:GetValue("G3R_FILIAL") + "'" 
			cSts += 		" AND G3R_NUMID  = '" + oMdlG3R:GetValue("G3R_NUMID")  + "'"
			cSts += 		" AND G3R_IDITEM = '" + oMdlG3R:GetValue("G3R_IDITEM") + "'"
			cSts += 		" AND G3R_NUMSEQ = '" + oMdlG3R:GetValue("G3R_NUMSEQ") + "'"
			cSts += 	IIF(TCSrvType() == "AS/400", " AND @DELETED@ = ' '", " AND D_E_L_E_T_ = ' '")
		
			If TCSQLExec(cSts) < 0
				Help( , , "TUR39ESG3R", , STR0001 + TCSQLError(), 1, 0) //"Erro ao estornar associa��o DR: "
				lRet := .F.
				Exit
			EndIf			
		Else
			lRet := lRet .And. oMdlG3R:LoadValue("G3R_CONCIL", "")
			lRet := lRet .And. oMdlG3R:LoadValue("G3R_FILCON", "")
			lRet := lRet .And. oMdlG3R:LoadValue("G3R_FATURA", "")
			lRet := lRet .And. oMdlG3R:LoadValue("G3R_DIVERG", "3")
		EndIf	 
	EndIf	
Next nX

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39ExG3R
Fun��o para exclus�o do acerto autom�tico gerado na associa��o

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Tur39ExG3R(oModel)

Local aArea     := GetArea()
Local oModelT39 := Nil
Local oModelG6J := oModel:GetModel("G6J_MASTER")	
Local oModelG3R := oModel:GetModel("G3R_ITENS")
Local lRet		:= .T.

G6J->(DbSetOrder(1))	// G6J_FILIAL+G6J_CONCIL+G6J_NRIATA+G6J_CPIATA
If (lRet := G6J->(DbSeek(oModelG6J:GetValue("G6J_FILIAL") + oModelG6J:GetValue("G6J_CONCIL") + oModelG6J:GetValue("G6J_NRIATA") + oModelG6J:GetValue("G6J_CPIATA"))))
	oModelT39 := FwLoadModel("TURA039MDL")
	oModelT39:SetOperation(MODEL_OPERATION_UPDATE)
	oModelT39:GetModel("G3R_ITENS"):SetLoadFilter({{"G3R_NUMID" , oModelG3R:GetValue("G3R_NUMID") , MVC_LOADFILTER_EQUAL}, ;
		                                           {"G3R_IDITEM", oModelG3R:GetValue("G3R_IDITEM"), MVC_LOADFILTER_EQUAL}, ; 
		                                           {"G3R_NUMSEQ", oModelG3R:GetValue("G3R_NUMSEQ"), MVC_LOADFILTER_EQUAL}})		
	oModelT39:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_NUMID" , oModelG3R:GetValue("G3R_NUMID") , MVC_LOADFILTER_EQUAL}, ;
		                                           {"G6I_IDITEM", oModelG3R:GetValue("G3R_IDITEM"), MVC_LOADFILTER_EQUAL}, ; 
		                                           {"G6I_NUMSEQ", oModelG3R:GetValue("G3R_SEQACR"), MVC_LOADFILTER_EQUAL}, ; 
		                                           {"G6I_CONCIL", oModelG3R:GetValue("G3R_CONCIL"), MVC_LOADFILTER_EQUAL}, ; 
		                                           {"G6I_FILIAL", oModelG3R:GetValue("G3R_FILCON"), MVC_LOADFILTER_EQUAL}})		
	oModelT39:Activate()
	
	//Confirma que foi localizado o G3R na abertura do modelo
	If (lRet := oModelT39:GetModel("G3R_ITENS"):Length() > 0 .And. !oModelT39:GetModel("G3R_ITENS"):IsEmpty())
		T39OpenMdl(oModelT39, "G3R_ITENS", .T.)
		oModelT39:GetModel("G3R_ITENS"):DeleteLine()
		If (lRet := oModelT39:VldData())
			oModelT39:CommitData()
		EndIf
	EndIf
	oModelT39:DeActivate()
	oModelT39:Destroy()
EndIf	
	
RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tur39EsG6I
Fun��o para estorno do item da fatura na concilia��o

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tur39EsG6I(oModel)

Local aArea   := GetArea()
Local oMdlG6I := oModel:GetModel("G6I_ITENS")
Local cSts    := ""
Local lRet	  := .T.

cSts := "UPDATE " + RetSqlName("G6I")
cSts += 	" SET G6I_CONCIL = '' ,"
cSts += 		" G6I_FILDR  = '' ," 
cSts += 		" G6I_NUMID  = '' ,"
cSts += 		" G6I_IDITEM = '' ,"
cSts += 		" G6I_NUMSEQ = '' ,"
cSts += 		" G6I_SITUAC = '1',"	//N�o associado
cSts += 		" G6I_DIVERG = '2',"	//N�o
cSts += 		" G6I_DIVFOP = '0',"	//N�o
cSts += 		" G6I_ASSAUT = '2',"    //N�o
cSts += 		" G6I_ACERTO = '2',"
cSts += 		" G6I_CODDIV = '' ,"
cSts += 		" G6I_IDIFA  = ''  "
cSts += 	" WHERE G6I_FILIAL = '" + oMdlG6I:GetValue("G6I_FILIAL") + "'"  
cSts += 	  " AND G6I_FATURA = '" + oMdlG6I:GetValue("G6I_FATURA") + "'" 
cSts += 	  " AND G6I_ITEM   = '" + oMdlG6I:GetValue("G6I_ITEM")   + "'" 
cSts += 	IIF(TCSrvType() == "AS/400", " AND @DELETED@ = ' '" , " AND D_E_L_E_T_ = ' '")

If TCSQLExec(cSts) < 0
	lRet := .F. 
	Help( , , "TUR39ESG6I", , STR0002 + TCSQLError(), 1, 0) //"Erro ao estornar associa��o Item Fatura: "
EndIf

RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39Arr2Str
Serializa um vetor para utilizar como busca no load filter

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
@param aValues, array, vetor com os tipos que ser�o filtrados
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39Arr2Str(aFilter, lSql)

Local cFilter := ""
Local nX	  := 0

Default lSQL  := .F.

cFilter := IIF(lSQL, "(", "{")

For nX := 1 To Len(aFilter)
	If nX > 1
		cFilter += ", "
	EndIf
	cFilter += "'" + aFilter[nX] + "'"
Next

cFilter += IIF(lSQL, ")", "}")

Return cFilter