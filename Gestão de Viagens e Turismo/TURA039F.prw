#include "TURA039F.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

Static aFldObrig := {}

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados utilizado para efetiva��o da fatura a�rea, vinculando todos G3R diretamente na Fatura

@type function
@author Anderson Toledo
@since 26/08/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   := Nil
Local oStruG3R := FwFormStruct(1, "G3R", , .F.)		// Documento de reserva
Local oStruG6M := FwFormStruct(1, "G6M") 		    // Apura��o de Metas
Local oStruG81 := FwFormStruct(1, "G81") 		    // Apura��o de Metas Liberada
Local nI	   := 0
Local aFields  := {}	
Local aFldG3R  := {}

If !oStruG6M:HasField("G6M_CODAPU")
	oStruG6M:AddField(GetSX3Cache("G6M_CODAPU", "X3_TITULO"),; 																									// [01] C Titulo do campo	
					  GetSX3Cache("G6M_CODAPU", "X3_DESCRIC"),;																									// [02] C ToolTip do campo	
					  "G6M_CODAPU",; 																															// [03] C identificador (ID) do Field
					  TamSX3("G6M_CODAPU")[3],; 																												// [04] C Tipo do campo
					  TamSX3("G6M_CODAPU")[1],; 																												// [05] N Tamanho do campo
					  TamSX3("G6M_CODAPU")[2],; 																												// [06] N Decimal do campo
					  IIF(Empty(GetSX3Cache("G6M_CODAPU", "X3_VALID")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_VALID")) + "}")),; 		// [07] B Code-block de valida��o do campo
					  IIF(Empty(GetSX3Cache("G6M_CODAPU", "X3_WHEN")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_WHEN")) + "}")),;			// [08] B Code-block de valida��o When do campoz
					  Nil,; 																																	// [09] A Lista de valores permitido do campo
					  Nil,; 																																	// [10] L Indica se o campo tem preenchimento obrigat�rio
					  IIF(Empty(GetSX3Cache("G6M_CODAPU", "X3_RELACAO")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_RELACAO")) + "}")),; 	// [11] B Code-block de inicializacao do campo
					  Nil,; 																																	// [12] L Indica se trata de um campo chave
					  .T.,; 																																	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				      .F.) 																																		// [14] L Indica se o campo � virtual
EndIf

If !oStruG81:HasField("G81_CODAPU")
	oStruG81:AddField(GetSX3Cache("G81_CODAPU", "X3_TITULO"),; 																									// [01] C Titulo do campo	
					  GetSX3Cache("G81_CODAPU", "X3_DESCRIC"),;																									// [02] C ToolTip do campo	
					  "G81_CODAPU",; 																															// [03] C identificador (ID) do Field
					  TamSX3("G81_CODAPU")[3],; 																												// [04] C Tipo do campo
					  TamSX3("G81_CODAPU")[1],; 																												// [05] N Tamanho do campo
					  TamSX3("G81_CODAPU")[2],; 																												// [06] N Decimal do campo
					  IIF(Empty(GetSX3Cache("G81_CODAPU", "X3_VALID")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_CODAPU", "X3_VALID")) + "}")),; 		// [07] B Code-block de valida��o do campo
					  IIF(Empty(GetSX3Cache("G81_CODAPU", "X3_WHEN")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_CODAPU", "X3_WHEN")) + "}")),;			// [08] B Code-block de valida��o When do campoz
					  Nil,; 																																	// [09] A Lista de valores permitido do campo
					  Nil,; 																																	// [10] L Indica se o campo tem preenchimento obrigat�rio
					  IIF(Empty(GetSX3Cache("G81_CODAPU", "X3_RELACAO")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_CODAPU", "X3_RELACAO")) + "}")),; 	// [11] B Code-block de inicializacao do campo
					  Nil,; 																																	// [12] L Indica se trata de um campo chave
					  .T.,; 																																	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				      .F.) 																																		// [14] L Indica se o campo � virtual
EndIf

//Filial da Fatura do Fornecedor
If !oStruG81:HasField("G81_FILFOR")
	oStruG81:AddField(GetSX3Cache("G81_FILFOR", "X3_TITULO"),; 																									// [01] C Titulo do campo	
					  GetSX3Cache("G81_FILFOR", "X3_DESCRIC"),; 																								// [02] C ToolTip do campo	
					  "G81_FILFOR",; 																															// [03] C identificador (ID) do Field
					  TamSX3("G81_FILFOR")[3],; 																												// [04] C Tipo do campo
					  TamSX3("G81_FILFOR")[1],; 																												// [05] N Tamanho do campo
					  TamSX3("G81_FILFOR")[2],; 																												// [06] N Decimal do campo
					  IIF(Empty(GetSX3Cache("G81_FILFOR", "X3_VALID")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FILFOR", "X3_VALID")) + "}")),; 		// [07] B Code-block de valida��o do campo
					  IIF(Empty(GetSX3Cache("G81_FILFOR", "X3_WHEN")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FILFOR", "X3_WHEN")) + "}")),;			// [08] B Code-block de valida��o When do campoz
					  Nil,; 																																	// [09] A Lista de valores permitido do campo
					  Nil,; 																																	// [10] L Indica se o campo tem preenchimento obrigat�rio
					  IIF(Empty(GetSX3Cache("G81_FILFOR", "X3_RELACAO")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FILFOR", "X3_RELACAO")) + "}")),; 	// [11] B Code-block de inicializacao do campo
					  Nil,; 																																	// [12] L Indica se trata de um campo chave
					  .T.,; 																																	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				      .F.) 																																		// [14] L Indica se o campo � virtual
EndIf

//Nro da Fatura do Fornecedor
If !oStruG81:HasField("G81_FATFOR")
	oStruG81:AddField(GetSX3Cache("G81_FATFOR", "X3_TITULO"),; 																									// [01] C Titulo do campo	
					  GetSX3Cache("G81_FATFOR", "X3_DESCRIC"),;																									// [02] C ToolTip do campo	
					  "G81_FATFOR",; 																															// [03] C identificador (ID) do Field
					  TamSX3("G81_FATFOR")[3],; 																												// [04] C Tipo do campo
					  TamSX3("G81_FATFOR")[1],; 																												// [05] N Tamanho do campo
					  TamSX3("G81_FATFOR")[2],; 																												// [06] N Decimal do campo
					  IIF(Empty(GetSX3Cache("G81_FATFOR", "X3_VALID")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FATFOR", "X3_VALID")) + "}")),; 		// [07] B Code-block de valida��o do campo
					  IIF(Empty(GetSX3Cache("G81_FATFOR", "X3_WHEN")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FATFOR", "X3_WHEN")) + "}")),;			// [08] B Code-block de valida��o When do campoz
					  Nil,; 																																	// [09] A Lista de valores permitido do campo
					  Nil,; 																																	// [10] L Indica se o campo tem preenchimento obrigat�rio
					  IIF(Empty(GetSX3Cache("G81_FATFOR", "X3_RELACAO")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_FATFOR", "X3_RELACAO")) + "}")),; 	// [11] B Code-block de inicializacao do campo
					  Nil,; 																																	// [12] L Indica se trata de um campo chave
					  .T.,; 																																	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				      .F.) 																																		// [14] L Indica se o campo � virtual
EndIf

//Prefixo da Fatura do Fornecedor
If !oStruG81:HasField("G81_PRFFOR")
	oStruG81:AddField(GetSX3Cache("G81_PRFFOR", "X3_TITULO"),; 																									// [01] C Titulo do campo	
					  GetSX3Cache("G81_PRFFOR", "X3_DESCRIC"),;																									// [02] C ToolTip do campo	
					  "G81_PRFFOR",; 																															// [03] C identificador (ID) do Field
					  TamSX3("G81_PRFFOR")[3],; 																												// [04] C Tipo do campo
					  TamSX3("G81_PRFFOR")[1],; 																												// [05] N Tamanho do campo
					  TamSX3("G81_PRFFOR")[2],; 																												// [06] N Decimal do campo
					  IIF(Empty(GetSX3Cache("G81_PRFFOR", "X3_VALID")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_PRFFOR", "X3_VALID")) + "}")),; 		// [07] B Code-block de valida��o do campo
					  IIF(Empty(GetSX3Cache("G81_PRFFOR", "X3_WHEN")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_PRFFOR", "X3_WHEN")) + "}")),;			// [08] B Code-block de valida��o When do campoz
					  Nil,; 																																	// [09] A Lista de valores permitido do campo
					  Nil,; 																																	// [10] L Indica se o campo tem preenchimento obrigat�rio
					  IIF(Empty(GetSX3Cache("G81_PRFFOR", "X3_RELACAO")), Nil, MontaBlock("{|| " + Alltrim(GetSX3Cache("G81_PRFFOR", "X3_RELACAO")) + "}")),; 	// [11] B Code-block de inicializacao do campo
					  Nil,; 																																	// [12] L Indica se trata de um campo chave
					  .T.,; 																																	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
				      .F.) 																																		// [14] L Indica se o campo � virtual
EndIf

oStruG6M:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)
oStruG81:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)

aFldG3R := oStruG3R:GetFields()
For nI := 1 To Len(aFldG3R)
	If oStruG3R:GetProperty(aFldG3R[nI, 3], MODEL_FIELD_OBRIGAT)
		aAdd(aFields, aFldG3R[nI, 3]) 
	EndIf
Next nI

aAdd(aFldObrig, {"G3R_ITENS", aFields})
aFields := {}

oStruG3R:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)

oModel := MPFormModel():New("TURA039F", /*bPreValidacao*/, {|oMdl| TA39FVld(oMdl)} /*bPosValidacao*/, /*bCommit*/, /*bCancel*/)
oModel:AddFields("G6H_MASTER", /*cOwner*/, FWFormStruct(1, "G6H", , .F.))

oModel:AddGrid("G6J_ITENS", "G6H_MASTER", FWFormStruct(1, "G6J", , .F.))
oModel:SetRelation("G6J_ITENS", {{"G6J_FILIAL", "xFilial('G6H')"}, {"G6J_FATURA", "G6H_FATURA"}}, G6J->(IndexKey(1)))

oModel:AddGrid("G6I_ITENS", "G6J_ITENS", FWFormStruct(1, "G6I", , .F.))
oModel:SetRelation("G6I_ITENS", {{"G6I_FILIAL", "G6I_FILIAL"}, {"G6I_FATURA", "G6J_FATURA"}}, G6I->(IndexKey(1)))

oModel:AddGrid("G3R_ITENS", "G6H_MASTER", oStruG3R)
oModel:SetRelation("G3R_ITENS", {{"G3R_FILCON", "G6H_FILIAL"}, {"G3R_FATURA", "G6H_FATURA"}, {"G3R_CONINU", "''"}}, G3R->(IndexKey(1)))
oModel:GetModel("G3R_ITENS"):SetOptional(.T.) 

oModel:AddGrid("G3Q_ITENS", "G3R_ITENS", FwFormStruct(1, "G3Q", , .F.))
oModel:SetRelation("G3Q_ITENS", {{"G3Q_FILIAL", "G3R_FILIAL"}, {"G3Q_NUMID", "G3R_NUMID"}, {"G3Q_IDITEM", "G3R_IDITEM"}, {"G3Q_NUMSEQ", "G3R_NUMSEQ"}, {"G3Q_CONINU", "''"}}, G3Q->(IndexKey(1)))
oModel:GetModel("G3Q_ITENS"):SetOptional(.T.)

oModel:AddGrid("G48A_ITENS", "G3R_ITENS", FwFormStruct(1, "G48", , .F.))
oModel:SetRelation("G48A_ITENS", {{"G48_FILIAL", "G3R_FILIAL"}, {"G48_NUMID", "G3R_NUMID"}, {"G48_IDITEM", "G3R_IDITEM"}, {"G48_NUMSEQ", "G3R_NUMSEQ"}, {"G48_CLIFOR", "'1'"}, {"G48_CONINU", "''"}}, G48->(IndexKey(1)))
oModel:GetModel("G48A_ITENS"):SetOptional(.T.)

oModel:AddGrid("G48B_ITENS", "G3R_ITENS", FwFormStruct(1, "G48", , .F.))
oModel:SetRelation("G48B_ITENS", {{"G48_FILIAL", "G3R_FILIAL"}, {"G48_NUMID", "G3R_NUMID"}, {"G48_IDITEM", "G3R_IDITEM"}, {"G48_NUMSEQ", "G3R_NUMSEQ"}, {"G48_CLIFOR", "'2'"}, {"G48_CONINU", "''"}}, G48->(IndexKey(1)))
oModel:GetModel("G48B_ITENS"):SetOptional(.T.)

oModel:AddGrid("G4CA_ITENS", "G3R_ITENS", FwFormStruct(1, "G4C", , .F.))
oModel:SetRelation("G4CA_ITENS", {{"G4C_FILIAL", "G3R_FILIAL"}, {"G4C_NUMID", "G3R_NUMID"}, {"G4C_IDITEM", "G3R_IDITEM"}, {"G4C_NUMSEQ", "G3R_NUMSEQ"}, {"G4C_CLIFOR", "'1'"}, {"G4C_CONINU", "''"}}, G4C->(IndexKey(1)))
oModel:GetModel("G4CA_ITENS"):SetOptional(.T.)

oModel:AddGrid("G4CB_ITENS", "G3R_ITENS", FwFormStruct(1, "G4C", , .F.))
oModel:SetRelation("G4CB_ITENS", {{"G4C_FILIAL", "G3R_FILIAL"}, {"G4C_NUMID", "G3R_NUMID"}, {"G4C_IDITEM", "G3R_IDITEM"}, {"G4C_NUMSEQ", "G3R_NUMSEQ"}, {"G4C_CLIFOR", "'2'"}, {"G4C_CARTUR", "''"}, {"G4C_CONINU", "''"}}, G4C->(IndexKey(1)))
oModel:GetModel("G4CB_ITENS"):SetOptional(.T.)

oModel:AddGrid("G9KA_ITENS", "G3R_ITENS", FwFormStruct(1, "G9K", , .F.))
oModel:SetRelation("G9KA_ITENS", {{"G9K_FILIAL", "G3R_FILIAL"}, {"G9K_NUMID" , "G3R_NUMID"}, {"G9K_IDITEM", "G3R_IDITEM"}, {"G9K_NUMSEQ", "G3R_NUMSEQ"}, {"G9K_CLIFOR", "'1'"}, {"G9K_CONINU", "''"}}, G9K->(IndexKey(1)))
oModel:GetModel("G9KA_ITENS"):SetOptional(.T.)

oModel:AddGrid("G9KB_ITENS", "G3R_ITENS", FwFormStruct(1, "G9K", , .F.))
oModel:SetRelation("G9KB_ITENS", {{"G9K_FILIAL", "G3R_FILIAL"}, {"G9K_NUMID" , "G3R_NUMID"}, {"G9K_IDITEM", "G3R_IDITEM"}, {"G9K_NUMSEQ", "G3R_NUMSEQ"}, {"G9K_CLIFOR", "'2'"}, {"G9K_CONINU", "''"}}, G9K->(IndexKey(1)))
oModel:GetModel("G9KB_ITENS"):SetOptional(.T.)

oModel:AddFields("G3P_FIELDS", "G3R_ITENS", FwFormStruct(1, "G3P"))
oModel:SetRelation("G3P_FIELDS", {{"G3P_FILIAL", "G3R_FILIAL"}, {"G3P_NUMID", "G3R_NUMID"}}, G3P->(IndexKey(1)))
oModel:GetModel("G3P_FIELDS"):SetOnlyQuery(.T.)
oModel:GetModel("G3P_FIELDS"):SetOptional(.T.)

oModel:AddGrid("G6M_ITENS", "G6H_MASTER", oStruG6M)
oModel:SetRelation("G6M_ITENS", {{"G6M_FILCON", "G6H_FILIAL"}, {"G6M_FATMET", "G6H_FATURA"}}, G6M->(IndexKey(2)))
oModel:GetModel("G6M_ITENS"):SetOptional(.T.)

oModel:AddGrid("G81_ITENS", "G6M_ITENS", oStruG81)
oModel:SetRelation("G81_ITENS", {{"G81_FILIAL",	"G6M_FILIAL"}, {"G81_CODAPU", "G6M_CODAPU"}, {"G81_CLASS", "G6M_TIPOAC"}, {"G81_FILIAL", "G6M_FILCON"}}, G81->(IndexKey(1)))
oModel:GetModel("G81_ITENS"):SetOptional(.T.)
oModel:GetModel("G81_ITENS"):SetLoadFilter({{"G81_STATUS", "'1'"}})	//somente apura��es que n�o foram faturadas

oModel:SetDescription(STR0001)	// "Fatura A�rea"

Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39FVld
Fun��o que valida campos obrigat�rios

@type static function
@param	oModel:		Objeto. Objeto da Classe FwFormModel
@return lRet:	L�gico. .t. validado com sucesso
@example lRet := TA39FVld(oModel)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA39FVld(oModel)

Local nI     := 0
Local nX     := 0
Local nY     := 0
Local nZ     := 0
Local aObrig := TA39FObgFld() //Retorna o conte�do do array est�tico aFldObrig que possui os submodelos e seus campos obrigat�rios
Local lRet	 := .t.

If oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE
	For nZ := 1 To oModel:GetModel("G6I_ITENS"):Length()  
		If oModel:GetModel("G6I_ITENS"):GetValue("G6I_META", nZ) == "2"		//Sen�o for item de fatura de Metas, checa obrigatoriedades
			If Len(aObrig) > 0
				For nI := 1 To Len(aObrig)
					If oModel:GetModel(aObrig[nI, 1]):ClassName() == "FWFORMFIELDS"
						For nY := 1 To Len(aObrig[nI, 2])
							If oModel:GetModel(aObrig[nI, 1]):HasField(aObrig[nI, 2][nY])
								If Empty(oModel:GetModel(aObrig[nI, 1]):GetValue(aObrig[nI, 2][nY]))
									lRet := .F.
									Exit
								EndIf
							EndIf
						Next nY		
					ElseIf oModel:GetModel(aObrig[nI, 1]):ClassName() == "FWFORMGRID"
						For nX := 1 To oModel:GetModel(aObrig[nI,1]):Length()
							For nY := 1 To Len(aObrig[nI,2])
								If oModel:GetModel(aObrig[nI, 1]):HasField(aObrig[nI, 2][nY])
									If Empty(oModel:GetModel(aObrig[nI, 1]):GetValue(aObrig[nI, 2][nY], nX))
										lRet := .F.
										Exit
									EndIf
								EndIf
							Next nY		
							If !lRet
								Exit
							EndIf
						Next nX
					EndIf
					
					If !lRet
						Exit
					EndIf
				Next nI
			EndIf 
		EndIf
		
		If !lRet
			cIdModel := aObrig[nI, 1]
			cIdField := aObrig[nI, 2][nY]
			oModel:SetErrorMessage(cIdModel, cIdField, cIdModel, cIdField, "", STR0002, STR0003, "", "")	//"O campo � de preenchimento obrigat�rio!"#"Informe algum conte�do para o campo."
			Exit
		EndIf
	Next nZ
EndIf

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39FVld
Fun��o que retorna o array est�tico de TURA039F.PRW, aFldObrig.

@type function
@param
@return aFldObrig:	Array. Array est�tico que possui os submodelos e campos obrigat�rios do modelo
@example aFldObrig := TA39FObgFld()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA39FObgFld()
Return aFldObrig