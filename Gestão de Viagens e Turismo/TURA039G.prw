#Include "TOTVS.CH"
#Include "FWPRINTSETUP.CH"
#Include "FWMVCDEF.CH"
#INCLUDE "TURA039G.CH"

#DEFINE IMP_SPOOL	2
#DEFINE IMP_PDF		6 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039DemFi
Programa para gera��o do relat�rio demonstrativo financeiro da concilia��o terrestre

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039DemFi(nTipo, lImpAuto)	

Local oPrint 	 := Nil
Local oFont10 	 := TFont():New("Courier New", 9, 10, .T., .F., 5, .T., 5, .T., .F.)
Local oFont10n	 := TFont():New("Courier New", 9, 10, .T., .T., 5, .T., 5, .T., .F.)
Local oFont14n	 := TFont():New("Courier New", 9, 14, .T., .T., 5, .T., 5, .T., .F.)
Local oModel	 := Nil
Local oMdlG3Q    := Nil
Local oMdlG3R    := Nil
Local oMdlG44    := Nil
Local cCliente	 := ""
Local cLoja		 := ""	
Local cNome      := ""
Local cCaminho   := GetTempPath()
Local nLin		 := 40
Local nX		 := 0
Local nTarifaB	 := 0
Local nTafifaN	 := 0
Local nMarkup	 := 0
Local nTaxas	 := 0
Local nExtras	 := 0
Local nTaxasAg	 := 0
Local nFEE		 := 0
Local nTotRecCli := 0 
Local nTotRecFor := 0 
Local nVlrLiqCli := 0
Local nVlrLiqFor := 0
Local nVlrDU	 := 0
Local nAux		 := 0
Local nVlrDesc	 := 0
Local nVlrRepas	 := 0
Local nVlrInc	 := 0
Local lFirstPage := .T.
Local lNewPage	 := .F.

Default nTipo    := 1
Default lImpAuto := .F.

cNome := IIF(nTipo == 1, "DemFinAgen-AE.rel", "DemFinCli-AE.rel")

// Apaga arquivo ja existente
If File(cCaminho + cNome)
	fErase(cCaminho + cNome)
EndIf
 		
oPrint := FWMSPrinter():New( cNome, IMP_SPOOL, .F., , .T., , , , , , , .T.)
oPrint:SetResolution(72)
oPrint:SetPortrait()
oPrint:SetPaperSize(DMPAPER_A4) 
oPrint:SetMargin(60, 60, 60, 60) // nEsquerda, nSuperior, nDireita, nInferior 
oPrint:cPathPDF := GetTempPath()
oPrint:Setup()

oModel := FwLoadModel("TURA039G")			
oModel:SetOperation(MODEL_OPERATION_VIEW)
oModel:Activate()
oMdlG3Q := oModel:GetModel("G3Q_ITENS")
oMdlG3R := oModel:GetModel("G3R_ITENS")
oMdlG44 := oModel:GetModel("G44_ITENS")

For nX := 1 To oMdlG3Q:Length()
	oMdlG3Q:GoLine(nX)

	If cCliente <> oMdlG3Q:GetValue("G3Q_CLIENT") .Or. cLoja <> oMdlG3Q:GetValue("G3Q_LOJA") .Or. lNewPage
		If !Empty(cCliente) .And. !Empty(cLoja) .And. (cCliente <> oMdlG3Q:GetValue("G3Q_CLIENT") .Or. cLoja <> oMdlG3Q:GetValue("G3Q_LOJA"))
			nLin += 35
			oPrint:Say(nLin, 50 , STR0001, oFont10) //"Recebemos e aceitamos os itens nas condi��es expostas"
			nLin += 20
			oPrint:Say(nLin, 50 , STR0002, oFont10) //"Aceite em ____/____/________"
			oPrint:Say(nLin, 250, "___________________________________", oFont10)
		  	nLin += 10
		  	oPrint:Say(nLin, 300, STR0003, oFont10) //"Assinatura"
		EndIf		

		cCliente := oMdlG3Q:GetValue("G3Q_CLIENT")
		cLoja 	 := oMdlG3Q:GetValue("G3Q_LOJA") 
		lNewPage := .F.
		
		If !lFirstPage
			oPrint:EndPage()
		EndIf
		oPrint:StartPage()

		TA39GImpC(oPrint, @nLin, oModel, oFont10, oFont10n, oFont14n, nTipo)
	EndIf
	
	PrintSegm(oModel, oPrint, nTipo, @nLin, oFont10, oFont10n) 
	
	nTarifaB   := oMdlG44:GetValue("G44_TARBAS")
 	nTafifaN   := oMdlG44:GetValue("G44_APLICA")
 	nMarkup	   := oMdlG3Q:GetValue("G3Q_MARKUP")
 	nTaxas	   := oMdlG3Q:GetValue("G3Q_TAXA")
 	nExtras	   := oMdlG3Q:GetValue("G3Q_EXTRA")
 	nTaxasAg   := oMdlG3Q:GetValue("G3Q_TAXAAG")
 	nFEE	   := oMdlG3Q:GetValue("G3Q_FEE")
 	nTotRecCli := oMdlG3Q:GetValue("G3Q_TOTREC")
 	nTotRecFor := oMdlG3R:GetValue("G3R_TOTREC")
 	nVlrLiqCli := oMdlG3Q:GetValue("G3Q_VLFIM")
 	nVlrLiqFor := oMdlG3R:GetValue("G3R_VLFIM")
 	nVlrDU	   := oMdlG3Q:GetValue("G3Q_TAXADU")
 	nVlrCom	   := oMdlG3R:GetValue("G3R_VLCOMI")
 	nVlrDesc   := oMdlG3Q:GetValue("G3Q_DESC")
 	nVlrRepas  := oMdlG3Q:GetValue("G3Q_REPASS")
 	nVlrInc	   := oMdlG3R:GetValue("G3R_VLINCE")
 	
 	If nTipo == 1 	//Agencia
	 	nLin += 20 
		oPrint:Say(nLin, 50 , STR0004, oFont10n) //"Emiss�o:"
		oPrint:Say(nLin, 105, DtoC(oMdlG3R:GetValue("G3R_EMISS")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0005 + "(+)", oFont10n)   //"Tarifa Base:"
		oPrint:Say(nLin, 105, Transform(nTarifaB , "@E 999,999,999.99"), oFont10)   
		nLin += 10
		oPrint:Say(nLin, 50 , STR0006, oFont10n)    //"Cambio:"
		oPrint:Say(nLin, 105, Transform(oMdlG44:GetValue("G44_TXCAMB"), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0007 + "(+)", oFont10n)     //"Tarifa Nac.:"
		oPrint:Say(nLin, 105, Transform(nTafifaN, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0008 + "(+)", oFont10n)    //"Taxas:"
		oPrint:Say(nLin, 105, Transform(nTaxas, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0054 + "(+)", oFont10n)    //"Extras: "
		oPrint:Say(nLin, 105, Transform(nExtras, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0059 + "(-)", oFont10n)    //"Comiss�o: "
		oPrint:Say(nLin, 105, Transform(nVlrCom , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0067 + "(+)", oFont10n)    //"Incentivo: "
		oPrint:Say(nLin, 105, Transform(nVlrInc , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0009, oFont10n)    //"Total:"
		oPrint:Say(nLin, 105, Transform(nVlrLiqFor, "@E 999,999,999.99"), oFont10)
		
		PrintClien(oPrint, @nLin, oFont10, oFont10n)
			
		nLin += 20
		oPrint:Say(nLin, 50 , STR0055 + "(+)", oFont10n)    //"Taxa ADM: "
		oPrint:Say(nLin, 105, Transform(nTaxasAg, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0056 + "(+)", oFont10n)    //"FEE: "
		oPrint:Say(nLin, 105, Transform(nFEE , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0064 + "(-)", oFont10n)    //"Desconto: "
		oPrint:Say(nLin, 105, Transform(nVlrDesc , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0057 + "(+)", oFont10n)    //"MARKUP: "
		oPrint:Say(nLin, 105, Transform(nMarkup , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0065 + "(-)", oFont10n)    //"Repasse: "
		oPrint:Say(nLin, 105, Transform(nVlrRepas , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0058 + "(+)", oFont10n)    //"DU: "
		oPrint:Say(nLin, 105, Transform(nVlrDU , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0010, oFont10n)    //"Tot. Receita :"
		oPrint:Say(nLin, 105, Transform(nTotRecCli + nTotRecFor , "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0011, oFont10n)    //"Valor L�quido :"
		oPrint:Say(nLin, 105, Transform(nVlrLiqCli , "@E 999,999,999.99"), oFont10)
			
		PrinTitCli(oModel, oPrint, nTipo, @nLin, oFont10, oFont10n)
	Else //cliente
		nLin += 10
		oPrint:Say(nLin, 50 , STR0061, oFont10n) //"Status: "
		oPrint:Say(nLin, 105, IIF(oMdlG3Q:GetValue("G3Q_STATUS") == "1", STR0062, STR0063), oFont10)	
	 	nLin += 20 
		oPrint:Say(nLin, 50 , STR0004, oFont10n) //"Emiss�o:"
		oPrint:Say(nLin, 105, DtoC(oMdlG3R:GetValue("G3R_EMISS")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0005, oFont10n)   //"Tarifa Base:"
		oPrint:Say(nLin, 105, Transform(nTarifaB + nMarkup, "@E 999,999,999.99"), oFont10)  		
		nLin += 10
		oPrint:Say(nLin, 50 , STR0006, oFont10n)    //"Cambio:"
		oPrint:Say(nLin, 105, Transform(oMdlG44:GetValue("G44_TXCAMB"), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0007, oFont10n)     //"Tarifa Nac.:"
		oPrint:Say(nLin, 105, Transform(nTafifaN + nMarkup, "@E 999,999,999.99"), oFont10)  		
		nLin += 10
		oPrint:Say(nLin, 50 , STR0008, oFont10n)    //"Taxas:"
		oPrint:Say(nLin, 105, Transform(nTaxas + nVlrDU, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0054, oFont10n)    //"Extras: "
		oPrint:Say(nLin, 105, Transform(nExtras, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0055, oFont10n)    //"Taxa ADM: "
		oPrint:Say(nLin, 105, Transform(nTaxasAg, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0056, oFont10n)    //"FEE: "
		oPrint:Say(nLin, 105, Transform(nFEE, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0064, oFont10n)    //"Desconto: "
		oPrint:Say(nLin, 105, Transform(nVlrDesc, "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0009, oFont10n)    //"Total:"
		oPrint:Say(nLin, 105, Transform(nVlrLiqCli, "@E 999,999,999.99"), oFont10) 		
	EndIf	
	
	If nLin > (oPrint:nVertRes() / oPrint:nFactorVert) - 160
		lNewPage := .T.		//For�a a inclus�o de uma nova p�gina com cabe�alho  
	EndIf	 
	
	nLin += 10
	oPrint:Say(nLin, 50, STR0001, oFont10) //"Recebemos e aceitamos os itens nas condi��es expostas"
	nLin += 15
	oPrint:Say(nLin, 50 , STR0002, oFont10) //"Aceite em ____/____/________"
	oPrint:Say(nLin, 250, "___________________________________", oFont10)
  	nLin += 10
  	oPrint:Say(nLin, 300, STR0003, oFont10) //"Assinatura"
  	nLin += 20
  	oPrint:Say(nLin, 50 , Replicate("_", 100), oFont10)
  	nLin += 10
	oPrint:Say(nLin, 50	, STR0025 + DtoC(Date()), oFont10) 					//"Data de Emiss�o :"
  	oPrint:Say(nLin, 230, STR0026 + UsrFullName(RetCodUsr()), oFont10) 		//"Emissor :"
  	
  	If oModel:GetModel("G6J_MASTER"):HasField("G6J_USERGI")
  		nLin += QuebPag(10, @nLin, oPrint)
  		oPrint:Say(nLin, 50 , STR0096 + FWLeUserlg("G6J_USERGI", 2), oFont10) 	//"Data de Inclus�o :"
  		oPrint:Say(nLin, 230, STR0098 + FWLeUserlg("G6J_USERGI"), oFont10) 		//'Usr :'	  		
 	EndIf
  	If oModel:GetModel("G6J_MASTER"):HasField("G6J_USERGA")
  		nLin += QuebPag(10, @nLin, oPrint)
  		oPrint:Say(nLin, 50 , STR0097 + FWLeUserlg("G6J_USERGA", 2), oFont10) 	//"Data de Altera��o :"
  		oPrint:Say(nLin, 230, STR0098 + FWLeUserlg("G6J_USERGA"), oFont10) 		//'Usr :'		  		
  	EndIf 
	  	
  	lNewPage := .T.
Next nX
	
oPrint:Preview()

If !lImpAuto 
	If !IsBlind() .And. ApMsgYesNo(I18N(STR0099, {IIF(nTipo == 1, STR0027, STR0066)})) 	// "Deseja imprimir o #1?"	"Demonstrativo Ag�ncia"	"Demonstrativo Cliente"
		FwMsgRun( , {|| TA039DemFi(IIF(nTipo == 1, 2, 1), .T.)}, , I18N(STR0100, {IIF(nTipo == 1, STR0027, STR0066)})) 	// "Imprimindo o #1, aguarde..."  "Demonstrativo Ag�ncia"	"Demonstrativo Cliente"  nTipo : (1=Agencia | 2=Cliente) 
	EndIf
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39GImpC
Fun��o com impress�o do cabe�alho do relat�rio de demonstrativo financeiro

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA39GImpC(oPrint, nLin, oModel, oFont10, oFont10n, oFont14n, nTipo)

Local aArea     := GetArea()
Local nAux1	    := 0
Local nAux2	    := 0	
Local cDemonstr := IIF(nTipo == 1, STR0027, STR0066)	
Local cPosto	:= oModel:GetValue("G3Q_ITENS", "G3Q_POSTO") + " - " + AllTrim(Posicione("G3M", 1, xFilial("G3M") + oModel:GetValue("G3Q_ITENS", "G3Q_POSTO"), "G3M_DESCR"))

nLin := 40

oPrint:Say(nLin, ((oPrint:nHorzRes() - oPrint:GetTextWidth(cDemonstr , oFont14n)) / 2) / oPrint:nFactorHor, cDemonstr, oFont14n) //"Demonstrativo"###"Demonstrativo"

nAux1 := (oPrint:GetTextWidth(STR0028, oFont10n) + 10) / oPrint:nFactorHor //"Nome da Empresa: "

nLin += 20
oPrint:Say(nLin, 0    , STR0028, oFont10n) //"Nome da Empresa: "
oPrint:Say(nLin, nAux1, SM0->M0_NOMECOM, oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0029, oFont10n) //"Endere�o: "
oPrint:Say(nLin, nAux1, AllTrim(SM0->M0_ENDCOB) + IIF(Empty(SM0->M0_COMPCOB), "", " - ") + AllTrim(SM0->M0_COMPCOB), oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0030, oFont10n) //"Cidade: "
oPrint:Say(nLin, nAux1, AllTrim(SM0->M0_CIDCOB), oFont10)
nAux2 := (oPrint:GetTextWidth(AllTrim(SM0->M0_CIDCOB), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0031, oFont10n) //"Estado: "
nAux2 += (oPrint:GetTextWidth(STR0031, oFont10n) + 10) / oPrint:nFactorHor  //"Estado: "
oPrint:Say(nLin, nAux1 + nAux2, AllTrim(SM0->M0_ESTCOB), oFont10)
nAux2 += (oPrint:GetTextWidth(AllTrim(SM0->M0_ESTCOB), oFont10) + 10) / oPrint:nFactorHor
oPrint:Say(nLin, nAux1 + nAux2, STR0032, oFont10n)  //"CEP: "
nAux2 += (oPrint:GetTextWidth(STR0032, oFont10n) + 10) / oPrint:nFactorHor //"CEP: "
oPrint:Say(nLin, nAux1 + nAux2, SM0->M0_CEPCOB, oFont10) 
nLin += 10
oPrint:Say(nLin, 0    , STR0033, oFont10n) //"Fone: "
oPrint:Say(nLin, nAux1, Transform(SM0->M0_TEL, "@R (99)9999-9999"), oFont10) 
nAux2 := (oPrint:GetTextWidth(Transform(SM0->M0_TEL, "@R (99)9999-9999"), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0034, oFont10n) //"Fax: "
nAux2 += (oPrint:GetTextWidth(STR0034, oFont10) + 10) / oPrint:nFactorHor //"Fax: "
oPrint:Say(nLin, nAux1 + nAux2, Transform(SM0->M0_FAX, "@R (99)9999-9999"), oFont10) 
nLin += 10
oPrint:Say(nLin, 0    , STR0035    , oFont10n) //"CNPJ: "
oPrint:Say(nLin, nAux1, SM0->M0_CGC, oFont10) 
nLin += 20
oPrint:Say(nLin, 0, STR0036, oFont10n) //"Demonstrativo:"

SA1->(DbSetOrder(1))	// A1_FILIAL+A1_COD+A1_LOJA
SA1->(DbSeek(xFilial("SA1") + oModel:GetValue("G3Q_ITENS", "G3Q_CLIENT") + oModel:GetValue("G3Q_ITENS", "G3Q_LOJA")))

nLin += 20
oPrint:Say(nLin, 0    , STR0039, oFont10n) //"Sacado:"
nLin += 10
oPrint:Say(nLin, 0    , STR0040, oFont10n) //"Nome do Sacado:"
oPrint:Say(nLin, nAux1, SA1->A1_NOME, oFont10)
nAux2 := (oPrint:GetTextWidth(AllTrim(SA1->A1_NOME), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0041, oFont10n) //"C�digo do Cliente:"
nAux2 += (oPrint:GetTextWidth(STR0041, oFont10n) + 10) / oPrint:nFactorHor  //"C�digo do Cliente:"
oPrint:Say(nLin, nAux1 + nAux2, SA1->A1_COD, oFont10)
nAux2 += (oPrint:GetTextWidth(SA1->A1_COD, oFont10) + 10) / oPrint:nFactorHor  //"Loja do Cliente:"
oPrint:Say(nLin, nAux1 + nAux2, STR0087, oFont10n) //"Loja do Cliente:"
nAux2 += (oPrint:GetTextWidth(STR0087, oFont10) + 10) / oPrint:nFactorHor
oPrint:Say(nLin, nAux1 + nAux2, SA1->A1_LOJA, oFont10)		
nLin += 10
oPrint:Say(nLin, 0    , STR0042, oFont10n) //"Endere�o:"
oPrint:Say(nLin, nAux1, SA1->A1_END, oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0043, oFont10n) //"Complemento:"
oPrint:Say(nLin, nAux1, SA1->A1_COMPLEM, oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0044, oFont10n) //"CEP:"
oPrint:Say(nLin, nAux1, SA1->A1_CEP, oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0045, oFont10n) //"Cidade:"
oPrint:Say(nLin, nAux1, AllTrim(SA1->A1_MUN) + "/" + SA1->A1_EST, oFont10)
nAux2 := (oPrint:GetTextWidth(AllTrim(SA1->A1_MUN) + "/" + SA1->A1_EST, oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0046, oFont10n) //"Bairro:"
nAux2 += (oPrint:GetTextWidth(STR0046, oFont10n) + 10) / oPrint:nFactorHor  //"Bairro:"
oPrint:Say(nLin, nAux1 + nAux2, SA1->A1_BAIRROC, oFont10)	
nLin += 10
oPrint:Say(nLin, 0    , STR0047, oFont10n) //"Conta cont�bil:"
oPrint:Say(nLin, nAux1, SA1->A1_CONTA, oFont10)
nAux2 := (oPrint:GetTextWidth(AllTrim(SA1->A1_CONTA), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0033, oFont10n) //"Fone:"
nAux2 += (oPrint:GetTextWidth(STR0033, oFont10n) + 10) / oPrint:nFactorHor  //"Fone:"
oPrint:Say(nLin, nAux1 + nAux2, AllTrim(Transform(SA2->A2_TEL, PesqPict("SA2", "A2_TEL"))), oFont10)
nAux2 += (oPrint:GetTextWidth(AllTrim(Transform(SA2->A2_TEL, PesqPict("SA2", "A2_TEL"))), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0034, oFont10n) //"Fax:"
nAux2 += (oPrint:GetTextWidth(STR0034, oFont10n) + 10) / oPrint:nFactorHor  //"Fax:"
oPrint:Say(nLin, nAux1 + nAux2, AllTrim(Transform(SA2->A2_FAX, PesqPict("SA2", "A2_FAX"))), oFont10)
nLin += 10
oPrint:Say(nLin, 0    , STR0048, oFont10n) //"CGC/CPF:"
oPrint:Say(nLin, nAux1, SA1->A1_CGC, oFont10)
nAux2 := (oPrint:GetTextWidth(SA1->A1_CGC, oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0049, oFont10n) //"Inscr. Estadual:"
nAux2 += (oPrint:GetTextWidth(STR0049, oFont10n) + 10) / oPrint:nFactorHor  //"Inscr. Estadual:"
oPrint:Say(nLin, nAux1 + nAux2, SA1->A1_INSCR, oFont10)		
nLin += 10
oPrint:Say(nLin, 0    , STR0050, oFont10n) //"End. de Cobran�a:"
oPrint:Say(nLin, nAux1, SA1->A1_ENDCOB, oFont10n)	
nLin += 10
oPrint:Say(nLin, 0    , STR0044, oFont10n) //"Cep: "
oPrint:Say(nLin, nAux1, SA1->A1_CEPC, oFont10)
nAux2 := (oPrint:GetTextWidth(AllTrim(SA1->A1_CEPC), oFont10) + 10) / oPrint:nFactorHor 
oPrint:Say(nLin, nAux1 + nAux2, STR0051, oFont10n) //"Cidade/Estado:"
nAux2 += (oPrint:GetTextWidth(STR0051, oFont10) + 10) / oPrint:nFactorHor 		 //"Cidade/Estado:"
oPrint:Say(nLin, nAux1 + nAux2, AllTrim(SA1->A1_MUNC) + "/" + SA1->A1_ESTC, oFont10n)
nLin += 15
oPrint:Say(nLin, 0, STR0052, oFont10) //"Nesta data, por vossa solicita��o, prestamos os servi�os relativos aos itens abaixo:"
nLin += 15

RestArea(aArea)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados j� com a ordena��o para gera��o do relat�rio de demonstrativo financeiro

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel := MPFormModel():New("TURA039G", /*bPreValidacao*/, /*bPosValidacao*/,{|| .T.}, {|| .T.} )

oModel:AddFields("G6J_MASTER", /*cOwner*/, FWFormStruct(1, "G6J"))
oModel:GetModel("G6J_MASTER"):SetOnlyQuery(.T.)
			
//Documentos de reserva
oModel:AddGrid("G3R_ITENS", "G6J_MASTER", FwFormStruct(1, "G3R", , .F.))
oModel:SetRelation("G3R_ITENS", {{"G3R_FILCON", "G6J_FILIAL" }, {"G3R_TPSEG", "1"}, {"G3R_CONCIL", "G6J_CONCIL"}}, "G3R_FILIAL+G3R_TPSEG+G3R_CONCIL")
oModel:GetModel("G3R_ITENS"):SetOnlyQuery(.T.)

//Item de Venda
oModel:AddGrid("G3Q_ITENS", "G3R_ITENS", FwFormStruct(1, "G3Q", , .F.))
oModel:SetRelation("G3Q_ITENS", {{"G3Q_FILIAL", "G3R_FILIAL"}, {"G3Q_NUMID", "G3R_NUMID"}, {"G3Q_IDITEM", "G3R_IDITEM"}, {"G3Q_NUMSEQ", "G3R_NUMSEQ"}}, G3Q->(IndexKey(1)))
oModel:GetModel("G3Q_ITENS"):SetOnlyQuery(.T.)

//Registro de Venda
oModel:AddFields("G3P_FIELDS", "G3Q_ITENS", FwFormStruct(1, "G3P", , .F.))
oModel:SetRelation("G3P_FIELDS", {{"G3P_FILIAL", "G3Q_FILIAL"}, {"G3P_NUMID", "G3Q_NUMID"}}, G3P->(IndexKey(1)))
oModel:GetModel("G3P_FIELDS"):SetOnlyQuery(.T.)

//Passageiros da reserva
oModel:AddGrid("G3S_ITENS", "G3R_ITENS", FwFormStruct(1, "G3S", , .F.))
oModel:SetRelation("G3S_ITENS", {{"G3S_FILIAL", "G3R_FILIAL"}, {"G3S_NUMID", "G3R_NUMID"}, {"G3S_IDITEM", "G3R_IDITEM"}, {"G3S_NUMSEQ", "G3R_NUMSEQ"}}, G3S->(IndexKey(1)))
oModel:GetModel("G3S_ITENS"):SetOnlyQuery(.T.)

//Entidades Adicionais
oModel:AddGrid("G4B_ITENS", "G3S_ITENS", FWFormStruct(1, "G4B", , .F.))
oModel:SetRelation("G4B_ITENS", {{"G4B_FILIAL", "G3S_FILIAL"}, {"G4B_NUMID", "G3S_NUMID"}, {"G4B_IDITEM", "G3S_IDITEM"}, {"G4B_NUMSEQ", "G3S_NUMSEQ"}, {"G4B_CLIENT", "G3Q_CLIENT"}, {"G4B_CODPAX", "G3S_CODPAX"}}, G4B->(IndexKey(1)))
oModel:GetModel("G4B_ITENS"):SetOnlyQuery(.T.)	

//1 - A�reo(G3T)
oModel:AddGrid("G3T_ITENS", "G3R_ITENS", FwFormStruct(1, "G3T", , .F.))
oModel:SetRelation("G3T_ITENS", {{"G3T_FILIAL", "G3R_FILIAL"}, {"G3T_NUMID", "G3R_NUMID"}, {"G3T_IDITEM", "G3R_IDITEM"}, {"G3T_NUMSEQ", "G3R_NUMSEQ"}}, G3T->(IndexKey(1)))
oModel:GetModel("G3T_ITENS"):SetOnlyQuery(.T.)

//Tarifas
oModel:AddGrid("G44_ITENS", "G3Q_ITENS", FwFormStruct(1, "G44", , .F.))
oModel:SetRelation("G44_ITENS", {{"G44_FILIAL", "G3Q_FILIAL"}, {"G44_NUMID", "G3Q_NUMID"}, {"G44_IDITEM", "G3Q_IDITEM"}, {"G44_NUMSEQ", "G3Q_NUMSEQ"}}, G44->(IndexKey(1)))
oModel:GetModel("G44_ITENS"):SetOnlyQuery(.T.)

//Taxas
oModel:AddGrid("G46_ITENS", "G3Q_ITENS", FwFormStruct(1, "G46", , .F.))
oModel:SetRelation("G46_ITENS", {{"G46_FILIAL", "G3Q_FILIAL"}, {"G46_NUMID", "G3P_NUMID"}, {"G46_IDITEM", "G3Q_IDITEM"}, {"G46_NUMSEQ", "G3Q_NUMSEQ"}, {"G46_CONINU", "''"}}, G46->(IndexKey(1)))
oModel:GetModel("G3T_ITENS"):SetOnlyQuery(.T.)

//Extras
oModel:AddGrid("G47_ITENS", "G3Q_ITENS", FwFormStruct(1, "G47", , .F.))
oModel:SetRelation("G47_ITENS", {{"G47_FILIAL", "G3Q_FILIAL"}, {"G47_NUMID", "G3P_NUMID"}, {"G47_IDITEM", "G3Q_IDITEM"}, {"G47_NUMSEQ", "G3Q_NUMSEQ"}, {"G47_CONINU", "''"}}, G47->(IndexKey(1)))
oModel:GetModel("G3T_ITENS"):SetOnlyQuery(.T.)

//Itens Financeiros (Pastas Abaixo)
oModel:AddGrid("G4CA_ITENS", "G3Q_ITENS", FwFormStruct(1, "G4C", , .F.))
oModel:SetRelation("G4CA_ITENS", {{"G4C_FILIAL", "G3Q_FILIAL"}, {"G4C_NUMID", "G3Q_NUMID"}, {"G4C_IDITEM", "G3Q_IDITEM"}, {"G4C_NUMSEQ", "G3Q_NUMSEQ"}, {"G4C_CLIFOR", "'1'"}, {"G4C_TIPO", "'1'"}}, G4C->(IndexKey(1)))
oModel:GetModel("G4CA_ITENS"):SetOnlyQuery(.T.)	

//G85
oModel:AddGrid("G85_ITENS", "G4CA_ITENS", FwFormStruct(1, "G85", , .F.))
oModel:SetRelation("G85_ITENS", {{"G85_FILIAL", "G4C_FILIAL"}, {"G85_IDIF", "G4C_IDIF"}, {"G85_PREFIX", "G4C_PREFIX"}, {"G85_NUMFAT", "G4C_NUMFAT"}}, G85->(IndexKey(1)))
oModel:GetModel("G85_ITENS"):SetOnlyQuery(.T.)

//G8E
oModel:AddGrid("G8E_ITENS", "G85_ITENS", FwFormStruct(1, "G8E", , .F.))
oModel:SetRelation("G8E_ITENS", {{"G8E_FILIAL", "G85_FILIAL"}, {"G8E_PREFIX", "G85_PREFIX"}, {"G8E_NUMFAT", "G85_NUMFAT"}, {"G8E_TIPO", "'3'"}}, G8E->(IndexKey(1)))
oModel:GetModel("G8E_ITENS"):SetOnlyQuery(.T.)

//Reembolso
oModel:AddGrid("G4E_ITENS", "G3R_ITENS", FWFormStruct(1, "G4E", , .F.))
oModel:SetRelation("G4E_ITENS", {{"G4E_FILIAL", "G3R_FILIAL"}, {"G4E_NUMID", "G3R_NUMID"}, {"G4E_IDITEM", "G3R_IDITEM"}, {"G4E_NUMSEQ", "G3R_NUMSEQ"}}, G4E->(IndexKey(1)))
oModel:GetModel("G4E_ITENS"):SetOnlyQuery(.T.)
	
Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados j� com a ordena��o para gera��o do relat�rio de demonstrativo financeiro

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TipoDocumento(cTpDoc)

Local cDoc := ""

Do Case
	Case AllTrim(cTpDoc) == "2"       				
		cDoc := STR0015 // Bilhete 
	Case AllTrim(cTpDoc) == "3"				    
	    cDoc := STR0016 //Localizador
	Case AllTrim(cTpDoc) == "5"				    
	    cDoc := "ADM: " 
	Case AllTrim(cTpDoc) == "6"				    
	    cDoc := "ACM: "    
	Case AllTrim(cTpDoc) == "7"				    
	    cDoc := "EMD: " 
	Case AllTrim(cTpDoc) == "8"				    
	    cDoc := "MCO: " 
EndCase

Return cDoc

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PrintSegm
Modelo de dados j� com a ordena��o para gera��o do relat�rio de demonstrativo financeiro

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function PrintSegm(oModel, oPrint, nTipo, nLin, oFont10, oFont10n)

Local oMdlG3R := oModel:GetModel("G3R_ITENS")
Local oMdlG3Q := oModel:GetModel("G3Q_ITENS")
Local oMdlG3T := oModel:GetModel("G3T_ITENS")
Local oMdlG4E := oModel:GetModel("G4E_ITENS")
Local cFilG3Q := oMdlG3Q:GetValue("G3Q_FILIAL")
Local cNumID  := oMdlG3Q:GetValue("G3Q_NUMID")
Local cIDItem := oMdlG3Q:GetValue("G3Q_IDITEM")
Local cNumSeq := oMdlG3Q:GetValue("G3Q_NUMSEQ")
Local cPosto  := oMdlG3Q:GetValue("G3Q_POSTO") + " - " + AllTrim(Posicione("G3M", 1, xFilial("G3M") + oMdlG3Q:GetValue("G3Q_POSTO"), "G3M_DESCR"))
Local nX	  := 0	
Local nAux1	  := 0
	
//EMPRESA - FILIAL - POSTO DE ATENDIMENTO	
nLin += 10
oPrint:Say(nLin, 50 , STR0068 + AllTrim(SA1->A1_NOME) +  STR0070 + AllTrim(cPosto), oFont10n)
nLin += 10
oPrint:Say(nLin, 50 , STR0069 + AllTrim(cFilG3Q) + " " + STR0088 + cNumID + " " + STR0089 + cIDItem + " " + STR0090 + cNumSeq, oFont10n)
	
If oMdlG3Q:GetValue("G3Q_OPERAC") <> "2"  
	nLin += 10
	oPrint:Say(nLin, 50 , STR0012, oFont10n) 	//"Descri��o:"
	oPrint:Say(nLin, 105, STR0013, oFont10) 	//"Aereo"

	For nX := 1 To oMdlG3T:Length()
		nLin += 20
		oPrint:Say(nLin, 50 , STR0014, oFont10n) 			//"Passageiro: "
		oPrint:Say(nLin, 105, oMdlG3T:GetValue("G3T_CODPAX", nX) + "-" + oMdlG3T:GetValue("G3T_NOMPAX", nX), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , TipoDocumento(oMdlG3Q:GetValue("G3Q_TPDOC")), oFont10n) 
		oPrint:Say(nLin, 105, oMdlG3R:GetValue("G3R_DOC"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0017, oFont10n) 			//"Fornecedor: "
		oPrint:Say(nLin, 105, AllTrim(oMdlG3T:GetValue("G3T_NOMFOR", nX)), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0018, oFont10n) 			//"CNPJ: "
		oPrint:Say(nLin, 105, Transform(Posicione("SA2", 1, xFilial("SA2") + oMdlG3R:GetValue("G3R_FORNEC") + oMdlG3R:GetValue("G3R_LOJA"), "A2_CGC"), PesqPict("SA2", "A2_CGC")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0024, oFont10n) 			//"Produto:"
		oPrint:Say(nLin, 105, AllTrim(oMdlG3R:GetValue("G3R_PROD")) + " / " + AllTrim(oMdlG3R:GetValue("G3R_DESPRD")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0019, oFont10n) 			//Data Saida    
		oPrint:Say(nLin, 105, DtoC(oMdlG3T:GetValue("G3T_DTSAID", nX)), oFont10)
		oPrint:Say(nLin, 155, STR0020, oFont10n) 			//Data chegada 
		oPrint:Say(nLin, 215, DtoC(oMdlG3T:GetValue("G3T_DTCHEG", nX)), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0021, oFont10n) 			//Aerop. Orig.    
		oPrint:Say(nLin, 115, AllTrim(oMdlG3T:GetValue("G3T_TERORI")), oFont10)
		oPrint:Say(nLin, 155  ,STR0022, oFont10n) 			//Aerop. Dest.
		oPrint:Say(nLin, 215 , AllTrim(oMdlG3T:GetValue("G3T_TERDST", nX)), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0023, oFont10n) 			//Classe da Reserva
		oPrint:Say(nLin, 145, AllTrim(oMdlG3T:GetValue("G3T_CLASRV", nX)), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0053, oFont10n) 			//"Solicitante:"
		oPrint:Say(nLin, 105, oMdlG3Q:GetValue("G3Q_NOMESO"), oFont10)	
		
		ImprimEnt(@nLin, oModel, oPrint, oMdlG3T:GetValue("G3T_CODPAX", nX), oFont10, oFont10n)
	Next nX
Else //reembolso
	nLin += 10
	oPrint:Say(nLin, 50 , STR0012, oFont10n) 		//"Descri��o:"
	oPrint:Say(nLin, 105, STR0013, oFont10) 		//"Aereo"

	For nX := 1 To oMdlG4E:Length()
		nLin += 20
		oPrint:Say(nLin, 50 , TipoDocumento(oMdlG3Q:GetValue("G3Q_TPDOC")), oFont10n) 
		oPrint:Say(nLin, 105, oMdlG3R:GetValue("G3R_DOC"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0017, oFont10n) 		//"Fornecedor: "
		oPrint:Say(nLin, 105, AllTrim(Posicione("SA2", 1, xFilial("SA2") + oMdlG3R:GetValue("G3R_FORNEC") + oMdlG3R:GetValue("G3R_LOJA"), "A2_NOME")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0018, oFont10n) 		//"CNPJ: "
		oPrint:Say(nLin, 105, Transform(Posicione("SA2", 1, xFilial("SA2") + oMdlG3R:GetValue("G3R_FORNEC") + oMdlG3R:GetValue("G3R_LOJA"), "A2_CGC"), PesqPict("SA2", "A2_CGC")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0024, oFont10n) 		//"Produto:"
		oPrint:Say(nLin, 105, AllTrim(oMdlG3R:GetValue("G3R_PROD")) + " / " + AllTrim(oMdlG3R:GetValue("G3R_DESPRD")), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0053, oFont10n) 		//"Solicitante:"
		oPrint:Say(nLin, 105, oMdlG3Q:GetValue("G3Q_NOMESO"), oFont10)	
		nLin += 10
		oPrint:Say(nLin, 50 , STR0071, oFont10n) 		//"Doc. Solic.: "
		oPrint:Say(nLin, 105, oMdlG4E:GetValue("G4E_DTSOLI", nX), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0072, oFont10n) 		//"Mot. Reembolso: "
		oPrint:Say(nLin, 120, Posicione("G8P", 1 , xFilial("G8P") + oMdlG4E:GetValue("G4E_MOTREE", nX), "G8P_DESCR"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0073, oFont10n) 		//"Tipo Ree. :"
		oPrint:Say(nLin, 105, IIF(oMdlG4E:GetValue("G4E_TPREE", nX) == "1",	STR0083, STR0084), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0074, oFont10n) 		//"Dest. Fin.: "
		oPrint:Say(nLin, 105, IIF(oMdlG4E:GetValue("G4E_DESTFN", nX) == "1", STR0085, STR0086), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0075, oFont10n) 		//"Data Prev.: "
		oPrint:Say(nLin, 105, DtoC(oMdlG4E:GetValue("G4E_DTPREV", nX)), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0076, oFont10n) 		//"Data Cred.: "
		oPrint:Say(nLin, 105, DtoC(oMdlG4E:GetValue("G4E_DTCRED", nX)), oFont10)
		nLin += 10 
		oPrint:Say(nLin, 50 , STR0077, oFont10n)    	//"Tarifa Paga: "
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_TARPAG", nX), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0078, oFont10n)    	//"Tar. Reemb.: "
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_TREEMB", nX), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0079, oFont10n)    	//"Taxas Pagas.: "
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_TXPG", nX), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0080, oFont10n)    	//"Taxas Reemb.:"
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_TXREEM", nX), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0081, oFont10n)    	//"Extras Pagos: "
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_EXTPG", nX), "@E 999,999,999.99"), oFont10)
		nLin += 10
		oPrint:Say(nLin, 50 , STR0082, oFont10n)    	//"Extras reemb.: "
		oPrint:Say(nLin, 105, Transform(oMdlG4E:GetValue("G4E_EXREEM", nX), "@E 999,999,999.99"), oFont10)
		
		ImprimEnt(@nLin, oModel, oPrint, oMdlG3T:GetValue("G3T_CODPAX"), oFont10, oFont10n)
	Next nX	
EndIf	

Return
	
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados j� com a ordena��o para gera��o do relat�rio de demonstrativo financeiro

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ImprimEnt(nLin, oModel, oPrint, cPax, oFont10, oFont10n)

Local aArea      := GetArea()
Local cAliasEntA := GetNextAlias()
Local nAux       := 0
	
BeginSQL Alias cAliasEntA
	SELECT G4B.G4B_TITUL, G4B.G4B_ITEM, G4B.G4B_DESENT
	FROM %Table:G4B% G4B
	WHERE G4B.G4B_FILIAL = %xFilial:G4B% AND 
	      G4B.G4B_NUMID  = %Exp:oModel:GetValue("G3R_ITENS", "G3R_NUMID")% AND
          G4B.G4B_IDITEM = %Exp:oModel:GetValue("G3R_ITENS", "G3R_IDITEM")% AND
          G4B.G4B_NUMSEQ = %Exp:oModel:GetValue("G3R_ITENS", "G3R_NUMSEQ")% AND 
          G4B.G4B_CODPAX = %Exp:cPax% AND
          G4B.G4B_CONINU = '' AND
          G4B.%NotDel%
EndSQL

(cAliasEntA)->(DbGoTop())
If (cAliasEntA)->(!Eof())
	While (cAliasEntA)->(!Eof())
		nLin += 10
		oPrint:Say(nLin, 50 , STR0060, oFont10n) //"Entidade: "
		oPrint:Say(nLin, 105, (cAliasEntA)->G4B_TITUL + " | " + (cAliasEntA)->G4B_DESENT, oFont10)	
		(cAliasEntA)->(DbSkip())
	EndDo
EndIf		

RestArea(aArea)

Return 	

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PrinTitCli
(long_description)

@type function
@param oModel, objeto, (Descri��o do par�metro)
@param oPrint, objeto, (Descri��o do par�metro)
@param nTipo, num�rico, (Descri��o do par�metro)
@param nLin, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)

@author osmar.junior
@since 17/10/2016
@version 1.0
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function PrinTitCli(oModel, oPrint, nTipo, nLin, oFont10, oFont10n)

Local aArea   := GetArea()
Local oMdlG8E := oModel:GetModel("G8E_ITENS")
Local nX      := 0

If oModel:GetValue("G4CA_ITENS", "G4C_TPFOP") == "1" //Intermedi��o
	SE1->(DbsetOrder(1)) 	// E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
	nLin += 20
	oPrint:Say(nLin, 50, STR0094, oFont10n) 	//T�tulos cliente:  N�mero      Prefixo Parcela Vencimento     Valor    Status
	
	For nX := 1 To oMdlG8E:Length()
		oMdlG8E:GoLine(nX)
		If SE1->(DbSeek(xFilial("SE1") + oMdlG8E:GetValue("G8E_PREFIX") + oMdlG8E:GetValue("G8E_NUMFAT") + oMdlG8E:GetValue("G8E_PARCEL") + oMdlG8E:GetValue("G8E_TIPOTI")))
			nLin += 10
			oPrint:Say(nLin, 135, SE1->E1_NUM + "    " + SE1->E1_PREFIXO + "       " + SE1->E1_PARCELA + "    " + AllTrim(DtoC(SE1->E1_VENCREA)) + "     " + AllTrim(Str(SE1->E1_VALOR)) + "      " + StsTitulo(SE1->E1_SALDO, SE1->E1_VALOR), oFont10)	
		EndIf	
	Next nX
EndIf
	
RestArea(aArea)

Return	

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} StsTitulo
(long_description)

@type function
@param nSaldo, num�rico, (Descri��o do par�metro)
@param nValor, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)

@author osmar.junior
@since 17/10/2016
@version 1.0
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function StsTitulo(nSaldo, nValor)

Local cRet:= ""

If nSaldo == 0
	cret := STR0091	//'Baixado'
ElseIf nSaldo == 	nValor
	cret := STR0092	//'Aberto'
ElseIf nSaldo >= 0.01
	cRet := STR0093	//'Baixado parcialmente'
EndIf	
 
Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} PrintClien
(long_description)

@type function
@param oPrint, objeto, (Descri��o do par�metro)
@param nLin, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)

@author osmar.junior
@since 17/10/2016
@version 1.0
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function PrintClien(oPrint, nLin, oFont10, oFont10n)

Local aArea    := GetArea()
Local cNome	   := AllTrim(SA1->A1_NOME) 
Local cCodCli  := SA1->A1_COD
Local cLojaCli := SA1->A1_LOJA	
Local nAux1	   := 50
Local nAux2	   := 0

If nLin > (oPrint:nVertRes() / oPrint:nFactorVert) - 160
	oPrint:StartPage()
	nLin := 20			 
EndIf

nLin += 20
oPrint:Say(nLin, nAux1, cNome, oFont10)

nAux1 += (oPrint:GetTextWidth(cNome, oFont10) + 10) / oPrint:nFactorHor	
nAux2 += (oPrint:GetTextWidth(cNome, oFont10) + 10) / oPrint:nFactorHor  	
oPrint:Say(nLin, nAux1 + nAux2, cCodCli, oFont10)	
 	
nAux2 += (oPrint:GetTextWidth(cCodCli, oFont10) + 10) / oPrint:nFactorHor
oPrint:Say(nLin, nAux1 + nAux2, STR0087, oFont10n) 		//"Loja do Cliente:"

nAux2 += (oPrint:GetTextWidth(STR0087, oFont10) + 10) / oPrint:nFactorHor
oPrint:Say(nLin, nAux1 + nAux2, cLojaCli, oFont10)	

RestArea(aArea)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039DemFi
Fun��o que valida a quebra de p�gina na impress�o 

@type function
@author Osmar Cioni
@since 19/09/2016
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function QuebPag(nLi, nLin, oPrint)

Local nVLi	:= nLi + 20

If nLin > (oPrint:nVertRes() / oPrint:nFactorVert) - nVLi 
	oPrint:EndPage()
	oPrint:StartPage()
	nLin := 40
EndIf

Return nLi