#Include "TOTVS.CH"
#Include "FWMVCDEF.CH"
#Include "FWEDITPANEL.CH"
#Include "TURA039H.CH"

Static oTblHeader := Nil //Tabela tempor�ria com os campos referente a pesquisa
Static oTblDetail := Nil //Tabela tempor�ria com os campos referente aos DR's encontrados

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
ModelDef para pesquisa de documentos de Reserva

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel	 := MPFormModel():New("TURA039H", /*bPreValidacao*/, /*bPosValidacao*/, {|| .T.}, /*bCancel*/)
Local oStructCab := FWFormModelStruct():New()
Local oStructDet := FWFormModelStruct():New()

TA039HCrTb()

oStructCab:AddTable(oTblHeader:GetAlias(), , STR0001, {|| oTblHeader:GetRealName()}) //"Inconsist�ncia" 
oStructDet:AddTable(oTblDetail:GetAlias(), , STR0002, {|| oTblDetail:GetRealName()}) //"Mensagens" 

AddFldCab(oStructCab, 1)
AddFldDet(oStructDet, 1)

oModel:AddFields("TMP_MASTER", /*cOwner*/, oStructCab)

oModel:AddGrid("TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, /*bLoad*/)
oModel:SetRelation("TMP_DETAIL", {}, (oTblDetail:GetAlias())->(IndexKey(1)))

oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

oModel:SetDeActivate({|| TA039HDestroy()})
oModel:SetPrimaryKey({})

Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
ViewDef para pesquisa de documentos de Reserva

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView		 := FWFormView():New()
Local oStructDet := FWFormViewStruct():New()

oView:SetModel(FWLoadModel("TURA039H"))

AddFldDet(oStructDet, 2)

oView:AddGrid("VIEW_TMPD", oStructDet, "TMP_DETAIL")

oView:CreateHorizontalBox("INFERIOR", 100)

oView:SetOwnerView("VIEW_TMPD", "INFERIOR")

oView:showInsertMsg(.F.)

Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039HDestroy
Fun��o para desalocar objetos instanciados

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039HDestroy()

//� necess�rio verificar se a chamada veio do bot�o 'Salvar' da tela apresentada no bot�o cancelar
//fechando as tabelas tempor�rias somente na �ltima chamada
If !FwIsInCallStack("FwAlertExitPage")
	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType(oTblHeader) == "O"
		oTblHeader:Delete()
		FreeObj(oTblHeader)
	EndIf

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType(oTblDetail) == "O"
		oTblDetail:Delete()
		FreeObj(oTblDetail)
	EndIf
EndIf

Return .T.

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA042ECrTb
Cria��o das tabelas tempor�rias

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039HCrTb()

Local aStruHeader := {}
Local aStruDetail := {}
Local aDetFields  := {"G6I_ITEM", "G6I_NUMID", "G6I_IDITEM", "G6I_NUMSEQ"}
Local nX	      := 0

aAdd(aStruHeader, {"TMP_CONCIL", "C", TamSX3("G6J_CONCIL")[1], TamSX3("G6J_CONCIL")[2]})

oTblHeader := FWTemporaryTable():New()
oTblHeader:SetFields(aStruHeader)
oTblHeader:AddIndex("index1", {"TMP_CONCIL"})
oTblHeader:Create()

For nX := 1 To Len(aDetFields)
	aAdd(aStruDetail, {AllTrim(aDetFields[nX]), TamSX3(aDetFields[nX])[3], TamSX3(aDetFields[nX])[1], TamSX3(aDetFields[nX])[2]})
Next

aAdd(aStruDetail, {"TMP_MSG", "C", 200, 0})

oTblDetail := FWTemporaryTable():New()
oTblDetail:SetFields(aStruDetail)
oTblDetail:AddIndex("index1", {"G6I_ITEM"})
oTblDetail:Create()

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldCab
Cria��o dos campos referente ao cabe�alho

@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldCab(oStruct, nOpc)

If nOpc == 1
	oStruct:AddField(STR0003, ; 	 				// [01] C Titulo do campo //"Concilia��o"
					 STR0003, ; 	 				// [02] C ToolTip do campo //"Concilia��o"
					 "TMP_CONCIL", ; 				// [03] C identificador (ID) do Field
					 "C", ;          				// [04] C Tipo do campo
					 TamSX3("G6J_CONCIL")[1], ; 	// [05] N Tamanho do campo
					 TamSX3("G6J_CONCIL")[2], ; 	// [06] N Decimal do campo
					 {|| .T.}, ; 					// [07] B Code-block de valida��o do campo
					 Nil, ; 	 					// [08] B Code-block de valida��o When do campo
					 Nil, ; 	 					// [09] A Lista de valores permitido do campo //'1=Aereo'###'2=Terrestre'
					 .F., ; 	 					// [10] L Indica se o campo tem preenchimento obrigat�rio
					 Nil, ; 	 					// [11] B Code-block de inicializacao do campo
					 Nil, ; 	 					// [12] L Indica se trata de um campo chave
					 Nil, ; 	 					// [13] L Indica se o campo pode receber valor em uma opera��o de update.
					 .T.) 				 	  		// [14] L Indica se o campo � virtual
Else
	oStruct:AddField("TMP_XGRPPR",; 	// [01] C Nome do Campo
					 "01",;    			// [02] C Ordem
					 STR0003,; 			// [03] C Titulo do campo //"Concilia��o"
					 STR0003,; 			// [04] C Descri��o do campo //"Concilia��o"
					 {},;      			// [05] A Array com Help
					 "C",;     			// [06] C Tipo do campo
					 "@!",;    			// [07] C Picture
					 Nil,;     			// [08] B Bloco de Picture Var
					 "",;      			// [09] C Consulta F3
					 .F.,;     			// [10] L Indica se o campo � edit�vel
					 Nil,;     			// [11] C Pasta do campo
					 Nil,;     			// [12] C Agrupamento do campo
					 Nil,;     			// [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
					 Nil,;     			// [14] N Tamanho M�ximo da maior op��o do combo
					 Nil,;     			// [15] C Inicializador de Browse
					 .T.,;     			// [16] L Indica se o campo � virtual
					 Nil) 				// [17] C Picture Vari�vel
EndIf

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} AddFldDet
Adiciona os campos referenes a detalhes no cabe�alho no model/view

@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function AddFldDet( oStructDet, nOpc )

Local aStruHis := (oTblDetail:GetAlias())->(DbStruct())
Local nX	   := 0

SX3->(DbSetOrder(2))	// X3_CAMPO
For nX := 1 To Len(aStruHis)
	If SX3->(DbSeek(aStruHis[nX][1])) 
		If nOpc == 1
			oStructDet:AddField(GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), ; 		// [01] C Titulo do campo
								"", ; 										   		// [02] C ToolTip do campo
								AllTrim(aStruHis[nX][1]), ; 				   		// [03] C identificador (ID) do Field
								TamSX3(aStruHis[nX][1])[3], ; 			   			// [04] C Tipo do campo
								TamSX3(aStruHis[nX][1])[1], ; 			   			// [05] N Tamanho do campo
								TamSX3(aStruHis[nX][1])[2], ; 		   				// [06] N Decimal do campo
								Nil, ; 										   		// [07] B Code-block de valida��o do campo
								Nil, ; 										   		// [08] B Code-block de valida��o When do campoz
								Nil, ; 										   		// [09] A Lista de valores permitido do campo
								Nil, ; 										   		// [10] L Indica se o campo tem preenchimento obrigat�rio
								Nil, ; 										   		// [11] B Code-block de inicializacao do campo
								Nil, ; 										   		// [12] L Indica se trata de um campo chave
								.F., ; 										   		// [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F.)   										   		// [14] L Indica se o campo � virtual
	
		ElseIf nOpc == 2 
			If !Empty(X3CBox())
				aCBox := Separa(X3CBox(), ";")
			Else
				aCBox := Nil
			EndIf
	
			oStructDet:AddField(AllTrim(aStruHis[nX][1]), ; 						// [01] C Nome do Campo
								StrZero(nX, 2), ;         							// [02] C Ordem
								GetSX3Cache(aStruHis[nX][1], "X3_TITULO"), ; 		// [03] C Titulo do campo
								GetSX3Cache(aStruHis[nX][1], "X3_DESCRIC"), ; 		// [04] C Descri��o do campo
								Nil, ; 												// [05] A Array com Help
								TamSX3(aStruHis[nX][1])[3], ; 						// [06] C Tipo do campo
								GetSX3Cache(aStruHis[nX][1], "X3_PICTURE"), ; 		// [07] C Picture
								Nil, ; 												// [08] B Bloco de Picture Var
								"", ;  												// [09] C Consulta F3
								.F., ; 												// [10] L Indica se o campo � edit�vel
								Nil, ; 												// [11] C Pasta do campo
								Nil, ; 												// [12] C Agrupamento do campo
								aCBox, ;  											// [13] A Lista de valores permitido do campo (Combo)
								Nil, ; 												// [14] N Tamanho M�ximo da maior op��o do combo
								Nil, ; 												// [15] C Inicializador de Browse
								.F., ; 												// [16] L Indica se o campo � virtual
								Nil) 	 											// [17] C Picture Vari�vel
		EndIf
	EndIf
Next nX 

//Adiciona o campo OK
If nOpc == 1
	oStructDet:AddField(STR0004, ;   	// [01] C Titulo do campo //"Mensagem"
						"", ;        	// [02] C ToolTip do campo
						"TMP_MSG", ; 	// [03] C identificador (ID) do Field
						"C", ;       	// [04] C Tipo do campo
						200, ;       	// [05] N Tamanho do campo
						0,;          	// [06] N Decimal do campo
						Nil, ;       	// [07] B Code-block de valida��o do campo
						Nil, ;       	// [08] B Code-block de valida��o When do campoz
						Nil, ;       	// [09] A Lista de valores permitido do campo
						Nil, ;       	// [10] L Indica se o campo tem preenchimento obrigat�rio
						Nil, ;       	// [11] B Code-block de inicializacao do campo
						Nil, ;       	// [12] L Indica se trata de um campo chave
						.T., ;       	// [13] L Indica se o campo pode receber valor em uma opera��o de update.
						.T.)         	// [14] L Indica se o campo � virtual
Else
	oStructDet:AddField("TMP_MSG", ;      		// [01] C Nome do Campo
						StrZero(nX, 2), ; 		// [02] C Ordem
						STR0004, ;        		// [03] C Titulo do campo//"Mensagem"
						STR0004, ;        		// [04] C Descri��o do campo //"Mensagem"
						Nil, ;            		// [05] A Array com Help
						"C", ;            		// [06] C Tipo do campo
						Nil, ;            		// [07] C Picture
						Nil, ;            		// [08] B Bloco de Picture Var
						"", ;             		// [09] C Consulta F3
						.T., ;            		// [10] L Indica se o campo � edit�vel
						Nil, ;            		// [11] C Pasta do campo
						Nil, ;            		// [12] C Agrupamento do campo
						Nil, ;            		// [13] A Lista de valores permitido do campo (Combo)
						Nil, ;            		// [14] N Tamanho M�ximo da maior op��o do combo
						Nil, ;            		// [15] C Inicializador de Browse
						.T., ;            		// [16] L Indica se o campo � virtual
						Nil)              		// [17] C Picture Vari�vel
EndIf

Return