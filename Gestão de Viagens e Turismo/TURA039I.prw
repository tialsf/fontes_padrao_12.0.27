#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#Include "TURA039I.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39AltFOP
Fun��o para altera��o da FOP do registro de venda que est� em concilia��o

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA39IAlFOP()

Local aArea		:= GetArea()
Local aAreaG6J  := G6J->(GetArea())
Local oModel	:= FwModelActive()
Local oMdlG6I	:= oModel:GetModel("G6I_ITENS")
Local oMdlG3R	:= oModel:GetModel("G3R_ITENS")
Local oMdlG3Q	:= oModel:GetModel("G3Q_ITENS")
Local oMdlT34	:= Nil
Local oView34   := Nil
Local aLines	:= FwSaveRows()
Local aRV		:= {}
Local aDadosUsr	:= {}
Local aItemG6I	:= {}
Local aManAss	:= {}
local cBkpFil	:= cFilAnt
Local cNumFat 	:= "" 
Local cConcil	:= ""
Local cMsgSolu	:= ""
Local nX 		:= 0

cNumFat := oMdlG6I:GetValue("G6I_FATURA")
cConcil	:= oMdlG6I:GetValue("G6I_CONCIL")

If !T39IVlPaAc()
	Return
EndIf	

If FwAlertYesNo(STR0001, STR0002) //"Todas altera��es em registros de acertos ser�o perdidas. Deseja prosseguir com a altera��o da FOP no Registro de Vendas?"###"Aten��o"
	For nX := 1 To oMdlG6I:Length()
		oMdlG6I:GoLine(nX)
		If oMdlG6I:GetValue("TMP_OK")		//Armazena Filial, Num. RV e Id Item de todos os RV's que ser�o alterados
		
			If oMdlG6I:GetValue("G6I_SITUAC") == "2" .And. oMdlG6I:GetValue("G6I_META") <> "1" 
				If !T39HasAltFOP(oModel)	 
					aAdd(aRV, {oMdlG3R:GetValue("G3R_MSFIL"), oMdlG3R:GetValue("G3R_NUMID"), oMdlG3R:GetValue("G3R_IDITEM"), oMdlG6I:GetLine()})			
				Else
					cMsgSolu := STR0014 //"Estorne a mudan�a anterior de FOP."
				EndIf
			Else
				cMsgSolu := STR0015 // 'Item j� aprovado.'
			EndIf 
		EndIf
	Next nX
	
	If Len(aRV) > 0
		G3P->(DbSetOrder(1))		// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
		For nX := 1 To Len(aRV)		
			cFilAnt := aRV[nX][1]
			
			//realiza a chamada da rotina de altera��o do registro de venda, apresentando somente o item selecionado
			If G3P->(DbSeek(xFilial("G3P") + aRV[nX][2]))
				T34Segmento(Val(G3P->G3P_SEGNEG))
				TURA34HierAces(__cUserId, @aDadosUsr)
				
				If Empty(aDadosUsr[1])
					Help( , , "TA39IAlFOP", , STR0003, 1, 0) //"Seu c�digo de usu�rio n�o esta relacionado com um vendedor ou agente. Nao ser� poss�vel acessar essa rotina." 
				Else
					FwModelActive(oModel)
					oMdlG6I:GoLine(aRV[nX][4])					
					cStatus := TA39VldSts(oModel)
					 										
					If cStatus == "1"			
						FwAlertHelp(I18N(STR0004, {oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM")}), STR0005, "TA39IAlFOP") //"O Registro de venda #1[registro de venda]# no item de venda #2[item venda]# n�o possui itens financeiros finalizados."###"Utilize a rotina 'Atualizar RV' localizada em 'Outras A��es'."
					ElseIf cStatus == "2"
						FwAlertHelp(I18N(STR0006, {oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM")}), STR0007, "TA39IAlFOP") //"O Registro de venda #1[registro de venda]# no item de venda #2[item venda]# possui itens financeiros de fornecedor finalizados."###"Estorne os processos que finalizaram os itens financeiros."
					Else	
						If oMdlG6I:GetValue("G6I_ASSAUT") == "2"
							//Armazena as informa��es necess�rias para a associa��o manual
							aAdd(aManAss, {oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_FILDR"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ")}) 	
						Else 
							//Armazena o Item do G6I que foi desassociado, para realizar a associa��o autom�tica somente destes itens
							aAdd(aItemG6I, oMdlG6I:GetValue("G6I_ITEM"))
						EndIf
						
						//Desassocia todos os RV's selecionados
						If Tur039Des(.T.)
							T34DadosUsr(aDadosUsr)
							oMdlT34 := FwLoadModel("TURA034")
							oMdlT34:SetOperation(MODEL_OPERATION_UPDATE)
							oMdlT34:GetModel("G3Q_ITENS"):SetLoadFilter({{"G3Q_IDITEM", "'" + aRV[nX][3] + "'", MVC_LOADFILTER_EQUAL}})
							oMdlT34:GetModel("G3Q_ITENS"):SetNoInsertLine(.T.)
							oMdlT34:GetModel("G3Q_ITENS"):SetPost({|| TA039VlAlt()}, .T.) 
							T39IAdField(oMdlT34)
							T39IAdTrig(oMdlT34)
							T39ISetPre(oMdlT34, "G3P_FIELDS")				
							oMdlT34:Activate()				
							T34GerRat(oMdlT34)			//Verifica a necessidade e gera se necess�rio o rateio de entidade adicional
							If TA39IGerAc(oMdlT34)		//Cria a c�pia do registro atual
								T39INoDelL(oMdlT34, "G3Q_ITENS", {"G4D_ITENS"})						//N�o permite apagar linhas
								FWExecView(STR0008, "TURA034", 4, , {|| .T.}, , , , , , , oMdlT34)	//"Registro de Vendas"			
							Else
								cMsgSolu := oMdlT34:GetErrorMessage()[6]
								TurHelp(cMsgSolu, STR0016, "TA39INoAltFOP") //"Realize os ajustes no registro de vendas para alterar a FOP."
							EndIf
							oMdlT34:DeActivate()
							oMdlT34:Destroy()
							T34Segmento(0)
							T34DadosUsr({"", "", "", ""})
						EndIf
					EndIf
				EndIf	
			EndIf
		Next nX
		cFilAnt := cBkpFil 
		
		//Realiza a associa��o autom�tica do registro de venda
		G6H->(DbSetOrder(1))		// G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA
		If G6H->(DbSeek(xFilial("G6H") + cNumFat))
			//Caso algum item de associa��o manual tenha sido alterado, � refeita a associa��o
			If Len(aManAss) > 0
				T39IAssMan(cConcil, aManAss, .T.)
			EndIf 
			
			If Len(aItemG6I) > 0
				Tu038Asso(.T., aItemG6I)
			EndIf
		EndIf
	Else	
		Help( , ,"TA39IAlFOP", , STR0013, 1, 0, , , , , , {cMsgSolu})		//'Altera��o de FOP n�o permitida.' 
	EndIf
EndIf

FwModelActive(oModel)
FwRestRows(aLines)
RestArea(aAreaG6J)
RestArea(aArea)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39VldSts
Fun��o para valida��o dos status dos itens financeiros de cliente e fornecedores, indicando quais est�o finalizados

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA39VldSts(oModelT39)

Local cStatus  := ""
Local oMdlG6I  := oModelT39:GetModel("G6I_ITENS")
Local lOpenCli := TA039IStIf(oMdlG6I:GetValue("G6I_FILIAL"), oMdlG6I:GetValue("G6I_CONCIL"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), "1") == 0
Local lOpenFor := TA039IStIf(oMdlG6I:GetValue("G6I_FILIAL"), oMdlG6I:GetValue("G6I_CONCIL"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), "2") == 0
Local nX	   := 0

If lOpenCli .And. lOpenFor		//Itens financeiros de cliente e fornecedor em aberto 
	cStatus := "1"
ElseIf !lOpenFor				//Pelo menos um item do fornecedor finalizado
	cStatus := "2"
ElseIf !lOpenCli				//Pelo menos um item de cliente finalizado e itens de fornecedor em aberto 
	cStatus := "3"
EndIf
	
Return cStatus

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039IStIf
Fun��o para verificar quantos itens financeiros est�o finalizados de um determinado item de venda

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039IStIf(cFilCon, cConcil, cNumId, cIdItem, cCliFor)

Local aArea     := GetArea()
Local cAliasAux := GetNextAlias()
Local nTotal    := 0

BeginSql Alias cAliasAux
	SELECT COUNT(*) TOTAL 
	FROM %Table:G4C% G4C
	INNER JOIN %Table:G3R% G3R ON G3R_FILIAL = G4C_FILIAL AND 
								  G3R_NUMID  = G4C_NUMID  AND 
	                              G3R_IDITEM = G4C_IDITEM AND 
	                              G3R_NUMSEQ = G4C_NUMSEQ AND 
	                              G3R_CONINU = %Exp:Space(TamSX3("G3R_CONINU")[1])% AND 
	                              G3R_CONCIL = %Exp:cConcil% AND 
	                              G3R_FILCON = %Exp:cFilCon% AND 
	                              G3R.%notDel%
	WHERE G4C_NUMID  = %Exp:cNumId% AND 
	      G4C_IDITEM = %Exp:cIdItem% AND 
	      G4C_CLIFOR = %Exp:cCliFor% AND 
	      G4C_STATUS > '3' AND 
	      G4C_CODAPU = ' ' AND 
	      G4C.%notDel%
EndSql

nTotal := (cAliasAux)->TOTAL
(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return nTotal

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39IGerAc
Fun��o para gera��o do acerto do item utilizado para altera��o da FOP

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA39IGerAc(oModel)

Local oModelG3Q	  := oModel:GetModel("G3Q_ITENS")
Local oModelG3R	  := oModel:GetModel("G3R_ITENS")
Local oModelG44	  := oModel:GetModel("G44_ITENS")	
Local oModelG46	  := oModel:GetModel("G46_ITENS")	
Local oModelG47	  := oModel:GetModel("G47_ITENS")
Local oModelG48A  := oModel:GetModel("G48A_ITENS")
Local oModelG48B  := oModel:GetModel("G48B_ITENS")
Local oModelG4E   := oModel:GetModel("G4E_ITENS")
Local aLines      := FwSaveRows()
Local aCopyValues := {}
Local cSeqG3R	  := ""
Local cSeqOrig	  := oModelG3Q:GetValue("G3Q_NUMSEQ")
Local nX		  := 0
Local nVlrPg	  := 0
Local lRet		  := .F.

T39OpenMdl(oModel, "G3Q_ITENS", .T.)

For nX := 1 To oModelG3Q:Length() 
	oModelG3Q:GoLine(nX)
		
	If oModelG3Q:GetValue("G3Q_ACRFOP") <> "1"
		T39ICanAc(oModel, "1")			//Cancela acordos em aberto de clientes
		T39ICanAc(oModel, "2")			//Cancela acordos em aberto de fornecedores
		T39ICIFFin(oModel, @nVlrPg)		//Faz a somat�ria dos itens j� finalizados do cliente
		oModelG3Q:LoadValue("G3Q_ATUIF"	, "3")
		oModelG3Q:LoadValue("G3Q_ATUIFA", "3")
	EndIf
Next nX
		
aCopyValues	:= TURxGetVls("G3Q_ITENS", .T., {"G4CA_ITENS", "G4CB_ITENS"}, {}, oModel, /*aNoAddLine*/, {{"G48A_ITENS", {{"G48_CANFOP", {|x| x <> "1"}}}}, {"G48B_ITENS", {{"G48_CANFOP", {|x| x <> "1"}}}}})

//Seta como n�o houve altera��o de FOP ou Moeda
oModelG3Q:LoadValue("G3Q_CANFOP", "0")			
									
//Finaliza o IV e DR atual
oModelG3Q:LoadValue("G3Q_STATUS", "2") //Finalizado
oModelG3R:LoadValue("G3R_STATUS", "4") //Cancelado
																
If TURxSetVs(aCopyValues, {}, oModel)
	lRet := .T.

	cSeqG3R := TA042MaxSq(oModel)
	TA042SetRec("G3Q_ITENS", {{"_NUMSEQ", cSeqG3R}}, , .F.)
		
	oModelG3R:LoadValue("G3R_SEQACR", cSeqOrig)		
	oModelG3Q:LoadValue("G3Q_ACERTO", "1") 		//Sim
	oModelG3Q:LoadValue("G3Q_ACRFOP", "1") 		//Sim	
	oModelG3Q:LoadValue("G3Q_ACORDO", "1")
	oModelG3R:LoadValue("G3R_ACRAUT", "1")  	//Sim
	oModelG3Q:LoadValue("G3Q_DTINC" , dDataBase)
	oModelG3R:LoadValue("G3R_ACERTO", "1") 		//Sim
	oModelG3R:LoadValue("G3R_STATUS", "1")		//Em Aberto	
	oModelG3Q:LoadValue("G3Q_STATUS", "1") 		//Aberto
	oModelG3R:LoadValue("G3R_STATUS", "1") 		//Aberto
	oModelG3Q:SetValue("G3Q_VLAFOP" , nVlrPg)	//Informa o valor j� pago pelo cliente no item original
	oModelG3R:SetValue("G3R_VLFIM"  , 0)		//Todos os valores ser�o recalculados 
	oModelG48A:SetValue("G48_CODAPU", "")		//N�o traz para o registro copiado o n�mero da apura��o
	oModelG48B:SetValue("G48_CODAPU", "")		//N�o traz para o registro copiado o n�mero da apura��o

	oModelG44:SetNoInsertLine(.F.)	
	oModelG44:SetNoDeleteLine(.F.)	
	oModelG46:SetNoInsertLine(.F.)	
	oModelG46:SetNoDeleteLine(.F.)	
	oModelG47:SetNoInsertLine(.F.)
	oModelG47:SetNoDeleteLine(.F.)
	oModelG48B:SetNoInsertLine(.F.)
	oModelG48B:SetNoDeleteLine(.F.)
	oModelG4E:SetNoInsertLine(.F.)
	oModelG4E:SetNoDeleteLine(.F.)

	//Atualiza os acordos finalizados do cliente, invertendo o tipo de cobran�a
	T39IAltAcd(oModel)
	
	//Recalcula acordos de fornecedor e cliente
	If !oModelG48B:IsEmpty()
		T34CalcAco( , , "2", "0", .T.)
	EndIf

	If !oModelG48A:IsEmpty()
		T34CalcAco( , , "1", "0", .T.)
	EndIf

	T34AtuDmFi(oModel, .F.)		//Atualiza Demonstrativo Financeiro
EndIf

oModelG44:SetNoInsertLine(.T.)	
oModelG44:SetNoDeleteLine(.T.)	
oModelG46:SetNoInsertLine(.T.)	
oModelG46:SetNoDeleteLine(.T.)	
oModelG47:SetNoInsertLine(.T.)
oModelG47:SetNoDeleteLine(.T.)
oModelG48B:SetNoInsertLine(.T.)
oModelG48B:SetNoDeleteLine(.T.)

FwRestRows(aLines)	

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ISetPre
Fun��o utilizada para incluir pr�-valida��o nos modelos

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ISetPre(oModel, cIdModel)

Local oModelAux	  := oModel:GetModel(cIdModel)
Local aDependency := oModel:GetDependency(cIdModel)
Local nX		  := 0

If oModelAux:ClassName() == "FWFORMGRID"	
	oModelAux:SetLPre({|oModelGrid, nLine, cAction, cField| TA039ILPre(oModelGrid, nLine, cAction, cField)})
	oModelAux:SetNoInsertLine(.T.)
Else
	oModelAux:SetOnlyView(.T.)
EndIf

For nX := 1 to len(aDependency)
	T39ISetPre(oModel, aDependency[nX][2])
Next

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039ILPre
Fun��o utilizada para pr�-valida��o dos modelos

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039ILPre(oModelGrid, nLine, cAction, cField)

Local oModel 	:= oModelGrid:GetModel()
Local aAcdClass	:= {}
Local lRet		:= .T.	

If cAction == "CANSETVALUE"
	//Se o modelo n�o estiver marcado como altera��o de FOP, n�o permite altera��o
	If oModel:GetValue("G3Q_ITENS", "G3Q_ACRFOP") <> "1"
		lRet := .F.

	//Permite cancelar acordos de cliente C08, C09 e C11 de acordo com a FOP
	ElseIf oModelGrid:GetId() == "G48A_ITENS" .And. cField == "G48_STATUS"
		If T34VldAcd(oModel, aAcdClass)
			lRet := .F.
		Else
			//Se o acordo alterado est� entre os acordos inv�lidos, permite altera��o 
			lRet := aScan(aAcdClass, {|x| x == oModelGrid:GetValue("G48_CLASS")}) > 0
		EndIf
	
	//Somente � permitido altera��o no campo Forma de pagamento e moedas e taxas fornecedor e tabela de Entidade Adicional	
	ElseIf !(AllTrim(cField) $ "G3Q_FORMPG|G4B_ITEM|G3R_MOEDA|G3R_TAXA|G44_MOEDFO|G44_TXCAMB|G46_MOEDFO|G46_TXCAMB|G47_MOEDFO|G47_TXCAMB|") .And. oModelGrid:GetId() <> "G4D_ITENS"
		lRet := .F. 

	//Para os campos edit�veis na rotina de altera��o de FOP/Moeda, chama a rotina padr�o do TURA034
	Else
		lRet := Tur034EdtVld(oModelGrid, nLine, cAction, cField)
	EndIf
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IAdTrig
Fun��o utilizada para adicionar gatilho nos modelos

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39IAdTrig(oModel)

Local oStruG3Q := oModel:GetModel("G3Q_ITENS"):GetStruct()
Local oStruG3R := oModel:GetModel("G3R_ITENS"):GetStruct()
Local oStruG44 := oModel:GetModel("G44_ITENS"):GetStruct()
Local oStruG46 := oModel:GetModel("G46_ITENS"):GetStruct()
Local oStruG47 := oModel:GetModel("G47_ITENS"):GetStruct()
Local aAux	   := {}

aAux := FwStruTrigger("G3Q_FORMPG", "G3Q_FORMPG", "T039ITrFOP()", .F., "")
oStruG3Q:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G3Q_FORMPG", "G3Q_FORMPG", "T039ITrFOP()", .F., "")
oStruG3Q:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("XXX_ALTFOP", "G3Q_CANFOP", "T039ITrAlt()", .F., "")
oStruG3Q:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("XXX_ALTMOE", "G3Q_CANFOP", "T039ITrAlt()", .F., "")
oStruG3Q:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G3R_MOEDA", "G3R_MOEDA", "T039ITrMoe()", .F., "")
oStruG3R:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G3R_TAXA", "G3R_TAXA", "T039ITrMoe()", .F., "")
oStruG3R:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G44_MOEDFO", "G44_MOEDFO", "T039ITrMoe()", .F., "")
oStruG44:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])

aAux := FwStruTrigger("G44_TXCAMB", "G44_TXCAMB", "T039ITrMoe()", .F., "")
oStruG44:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])			

aAux := FwStruTrigger("G46_MOEDFO", "G46_MOEDFO", "T039ITrMoe()", .F., "")
oStruG46:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])			

aAux := FwStruTrigger("G46_TXCAMB", "G46_TXCAMB", "T039ITrMoe()", .F., "")
oStruG46:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])			

aAux := FwStruTrigger("G47_MOEDFO", "G47_MOEDFO", "T039ITrMoe()", .F., "")
oStruG47:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])			

aAux := FwStruTrigger("G47_TXCAMB", "G47_TXCAMB", "T039ITrMoe()", .F., "")
oStruG47:AddTrigger(aAux[1], aAux[2], aAux[3], aAux[4])			
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039ITrFOP
Fun��o utilizada para tratamento no IV original, de acordo com a nova FOP informada

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T039ITrFOP()

Local oModel 	:= FwModelActive()
Local oModelG3Q := oModel:GetModel("G3Q_ITENS")
Local cFopOrig	:= ""
Local cFopDest	:= ""
Local nX		:= 0
Local nLine		:= 0
Local nVlrPg	:= 0

nLine := oModelG3Q:GetLine()

oModelG3Q:GoLine(1)
cFopOrig := T039GetFOP(oModel)

oModelG3Q:GoLine(nLine)
cFopDest := T039GetFOP(oModel)

If cFopOrig <> cFopDest
	oModelG3Q:GoLine(1)
	oModelG3Q:SetValue("XXX_ALTFOP", "1")
	
	oModelG3Q:GoLine(nLine) 
	oModelG3Q:SetValue("G3Q_ACORDO" , "1")
	oModelG3Q:LoadValue("G3Q_ATUIF" , "3")
	oModelG3Q:LoadValue("G3Q_ATUIFA", "3")
	
	T034DmFin(oModel)
	T34AtuDmFi(oModel)
Else
	oModelG3Q:GoLine(1)
	oModelG3Q:SetValue("XXX_ALTFOP", "0")
	oModelG3Q:GoLine( nLine )	
EndIf
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039ITrMoe

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T039ITrMoe(cField, cModel)

Local oModel  := FwModelActive()
Local oMdlG3Q := oModel:GetModel("G3Q_ITENS")
Local oMdlG3R := oModel:GetModel("G3R_ITENS")
Local oMdlG44 := oModel:GetModel("G44_ITENS")
Local aLines  := FwSaveRows()
Local aFldG3R := {{"G3R_MOEDA" , ""}, {"G3R_TAXA" , 0}}
Local aFldG44 := {{"G44_MOEDFO", ""}, {"G44_MOEDA", ""}, {"G44_TXCAMB", 0}}
Local nX	  := 0
Local cStatus := "0"

For nX := 1 To Len(aFldG3R)
	aFldG3R[nX][2] := oMdlG3R:GetValue(aFldG3R[nX][1])
Next nX

For nX := 1 To Len(aFldG44)
	aFldG44[nX][2] := oMdlG44:GetValue(aFldG44[nX][1])
Next nX

oMdlG3Q:GoLine(1)
For nX := 1 To Len(aFldG3R)
	If aFldG3R[nX][2] <> oMdlG3R:GetValue(aFldG3R[nX][1])
		cStatus	:= '1'
		Exit
	EndIf	
Next nX

For nX := 1 To Len(aFldG44)
	If aFldG44[nX][2] <> oMdlG44:GetValue(aFldG44[nX][1])
		cStatus	:= '1'
		Exit
	EndIf	
Next nX

oMdlG3Q:SetValue("XXX_ALTMOE", cStatus)

FwRestRows(aLines)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039ITrAlt

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T039ITrAlt()

Local oModel  := FwModelActive()
Local oMdlG3Q := oModel:GetModel("G3Q_ITENS")
Local cBin	  := ""

//N�o pode alterar a posi��o dos campos, pois a ordem bin�ria em decimal indica a posi��o correta do combo
//Novos campos dever�o ser adicionados em primeiro, para formar o bin�rio da direita para a esquerda
cBin += oMdlG3Q:GetValue("XXX_ALTMOE")
cBin += oMdlG3Q:GetValue("XXX_ALTFOP")
	
Return cValToChar(CToN(cBin, 2))
 
//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T039GetFOP
Fun��o utilizada para obter o tipo da FOP

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T039GetFOP(oModel)

Local aArea := GetArea()
Local cRet  := ""

//G3O_TIPO 	-> 1=Intermediacao;2=Direto Agencia;3=Pag. Direto;4=Cartao Cliente
//G3O_ORICP -> 1=Fornecedor;2=Fixa;3=Venda;4=Cartao Proprio                                                                
G3O->(DbSetOrder(1))		// G3O_FILIAL+G3O_CODIGO+G3O_ITEM
G3O->(DbSeek(xFilial("G3O") + oModel:GetValue("G3Q_ITENS", "G3Q_FORMPG")))

Do Case
	Case G3O->G3O_TIPO == "1" .Or. G3O->G3O_TIPO == "2"                                                                                    
		If G3O->G3O_ORICP == "1"
			cRet := "1" //Intermedia��o
		ElseIf G3O->G3O_ORICP == "4"
			cRet := "2" //Cart�o Ag�ncia
		EndIf
		
	Case G3O->G3O_TIPO = "3" .Or. G3O->G3O_TIPO = "4"
		cRet := "3" //Cart�o Cliente
End Case

RestArea(aArea)

Return cRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ICanAc
Fun��o utilizada para cancelamento dos acordos aplicados em aberto

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ICanAc( oModel, cTipo )

Local oModelG48 := IIF(cTipo == "1", oModel:GetModel("G48A_ITENS"), oModel:GetModel("G48B_ITENS"))
Local nX        := 0

For nX := 1 to oModelG48:Length() 
	oModelG48:GoLine(nX)
	If oModelG48:GetValue("G48_STATUS") == "1" 		// 1-Aberto
		oModelG48:LoadValue("G48_STATUS", "3") 		// 3-Cancelado
		oModelG48:SetValue("G48_CANFOP" , "1") 	// 1-Sim
	EndIf
Next nX
 
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39ICIFFin
Fun��o calculo do valor j� pago pelo cliente em um item de venda

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39ICIFFin(oModel, nVlrPg)

Local oModelG4CA := oModel:GetModel("G4CA_ITENS")
Local nX 		 := 0

For nX := 1 to oModelG4CA:Length() 
	oModelG4CA:GoLine(nX)
	If oModelG4CA:GetValue("G4C_STATUS") == "4" .And. Empty(oModelG4CA:GetValue("G4C_CLASS"))	// 4-Finalizado
		If oModelG4CA:GetValue("G4C_PAGREC") == "1"
			nVlrPg += oModelG4CA:GetValue("G4C_VALOR")		//Se o valor era a pagar, ent�o � positivo para somar no valor final e cobrar este valor do cliente 
		ElseIf oModelG4CA:GetValue("G4C_PAGREC") == "2"
			nVlrPg -= oModelG4CA:GetValue("G4C_VALOR")		//Se o valor era a receber, ent�o � negativo para abater do valor final e devolver este valor ao cliente
		EndIf
	EndIf
Next nX

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039VlAlt
Fun��o para valida��o das altera��es realizadas no modelo s�o v�lidas

@type function
@author Anderson Toledo
@since 03/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039VlAlt()

Local oModel	:= FwModelActive()
Local oModelG3Q	:= oModel:GetModel("G3Q_ITENS")
Local aLines	:= FwSaveRows()
Local cFOPOrig	:= ""
Local lRet		:= .T.
Local nX		:= 0

oModelG3Q:GoLine(1)
If oModelG3Q:GetValue("G3Q_CANFOP") == "0"
	TurHelp(STR0017, STR0018, "TA039VLALT") //"N�o houve altera��o de FOP e/ou Moeda"###"Realize a altera��o de FOP, moeda ou taxa de c�mbio para confirmar a opera��o."
	lRet := .F.
EndIf

//Apenas verifico a FOP se houve apenas altera��o de FOP
If lRet .And. oModelG3Q:GetValue("G3Q_CANFOP") == "1" 
	For nX := 1 To oModelG3Q:Length()
		oModelG3Q:GoLine(nX)
		If nX == 1
			cFOPOrig := AllTrim(oModelG3Q:GetValue("G3Q_FORMPG", nX))
		ElseIf cFOPOrig == AllTrim(oModelG3Q:GetValue("G3Q_FORMPG", nX))
			lRet := .F.
			Exit
		EndIf
	Next nX

	If !lRet
		TurHelp(STR0009, STR0010, "TA039VLALT") //"C�digo da FOP Destino n�o pode ser o mesmo c�digo da FOP Origem."###"Informe outra FOP no campo Forma de Pagamento"
	EndIf
EndIf

FwRestRows(aLines)
		
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IAssMan
Fun��o para recriar a associa��o manual desfeita na altera��o da FOP

@type function
@author Anderson Toledo
@since 22/03/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39IAssMan(cConcil, aManAss, lGerAcerto)

Local oModelConc := Nil
Local oModelG6I	 := Nil
Local aItensG6I  := {}
Local lRet	     := .F.
Local nX	     := 0
Local nPos	     := 0

Default lGerAcerto := .F.

aEval(aManAss, {|x| aAdd(aItensG6I, x[1])})

oModelConc := FwLoadModel("TURA038D")
oModelConc:SetOperation( MODEL_OPERATION_UPDATE)
oModelConc:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_ITEM"  , T39Arr2Str(aItensG6I), MVC_LOADFILTER_IS_CONTAINED}})
oModelConc:GetModel("G6J_ITENS"):SetLoadFilter({{"G6J_CONCIL", cConcil				, MVC_LOADFILTER_EQUAL 		 }})
oModelConc:Activate()

//Realiza a Associa��o Manual
lRet := T39IRunAsM(aManAss, oModelConc)
		
If lRet .And. oModelConc:VldData()
	oModelConc:CommitData()	
	lRet := T38AtuNat({cConcil})
Else
	lRet := .F.
EndIf

oModelConc:DeActivate()
oModelConc:Destroy()	

If lGerAcerto
	lRet := T38AutAcr(G6H->G6H_FATURA)
EndIf

If lRet	
	If lRet
		lRet := T38AtuSt(G6H->G6H_FATURA)
	EndIf
EndIf
		
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IRunAsM
Fun��o que realiza a associa��o manual

@type function
@author Anderson Toledo
@since 22/03/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39IRunAsM(aManAss, oModelConc, nLine)

Local oModelG6I := oModelConc:GetModel("G6I_ITENS")
Local nX		:= 0
Local nTamMdl	:= oModelG6I:Length()
Local lRet		:= .T.

Default nLine 	:= 0

//Se nLine diferente de zero, s� avalia a linha informada, sen�o avalia todas
If nLine == 0 
	nLine := 1
Else
	nTamMdl := nLine
EndIf 	

For nX := nLine To nTamMdl
	If !lRet
		Exit
	EndIf
	
	oModelG6I:GoLine(nX)
	If (nPos := aScan(aManAss, {|x| x[1] == oModelG6I:GetValue("G6I_ITEM")})) > 0
		BEGIN TRANSACTION
			//Cria o vinculo da G6I com G3R				
			lRet := T38G6IxG3R(oModelConc, aManAss[nPos][2], aManAss[nPos][3], aManAss[nPos][4], aManAss[nPos][5], .F.) 
			
			If !(lRet .And. T38UpdG3R(oModelConc, aManAss[nPos][3], aManAss[nPos][4], aManAss[nPos][5], FwXFilial("G3R", aManAss[nPos][2])))
				lRet := .F.
			EndIf
			
			If !lRet
				DisarmTransaction()
				Break
			EndIf
		END TRANSACTION
	Else
		FwAlertHelp(STR0011, I18N(STR0012, {oModelG6I:GetValue("G6I_ITEM")}), "T39IRUNASM") //"Item da fatura n�o encontrado"###"N�o foi poss�vel realizar a associa��o manual do item da fatura #1 [item fatura]#"
	EndIf
Next nX

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39INoDelL
Fun��o utilizada apagar todas as informa��es de um modelo e submodelo

@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39INoDelL(oModel, cIdModel, aCanDel)

Local oModelAux	:= oModel:GetModel(cIdModel)
Local aIdGrids 	:= oModel:GetDependency(cIdModel)
Local nX 		:= 0

Default aCanDel	:= {}

If oModelAux:ClassName() == "FWFORMGRID"
	If aScan(aCanDel, {|x| x == cIdModel}) == 0
		oModelAux:SetNoDeleteLine(.T.)
	EndIf	
Else
	oModelAux:SetOnlyView(.T.)	
EndIf

For nX := 1 To Len(aIdGrids)
	T39INoDelL(oModel, aIdGrids[nX][2], aCanDel)	
Next nX

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IAltAcd

@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39IAltAcd(oModel)

Local oMdlG48A 	:= oModel:GetModel("G48A_ITENS")
Local aLines	:= FwSaveRows(oModel)
Local cNatureza	:= ""
Local lRet		:= .T.
Local nX		:= 0
Local nValor	:= 0

For nX := 1 To oMdlG48A:Length()
	//Se finalizado, inverte opera��o do acordo, passa para manual e deixa como aberto
	If oMdlG48A:GetValue("G48_STATUS", nX) == "4"
		If oMdlG48A:GetValue("G48_TPACD", nX) == "1" //1-Receita
			nValor += oMdlG48A:GetValue("G48_VLACD", nX)
		ElseIf oMdlG48A:GetValue("G48_TPACD", nX) == "2" //2-Abat. de receita
			nValor -= oMdlG48A:GetValue("G48_VLACD", nX)
		EndIf	
	EndIf
Next Nx

If nValor <> 0
	If oMdlG48A:IsEmpty() .Or. oMdlG48A:Length() < oMdlG48A:AddLine()
		lRet := oMdlG48A:LoadValue("G48_APLICA", StrZero(T034NxtApl(oMdlG48A, "1", oMdlG48A:Length(.T.)), TamSx3("G48_APLICA")[1]))			 
		lRet := oMdlG48A:SetValue("G48_CODACD", SuperGetMV("MV_TURACCR",,"")) .And. lRet
		
		If nValor > 0 //Receita vira abatimento de receita
			lRet := oMdlG48A:SetValue("G48_VLACD", nValor) .And. lRet
			lRet := oMdlG48A:LoadValue("G48_TPACD", "2") .And. lRet
		Else
			lRet := oMdlG48A:SetValue("G48_VLACD", nValor * -1) .And. lRet
			lRet := oMdlG48A:LoadValue("G48_TPACD", "1") .And. lRet
		EndIf
		
		If Findfunction("U_TURNAT")
			cNatureza := T39DGetNat( oModel, 2 ) 
			If !Empty( cNatureza )
				oMdlG48A:LoadValue("G48_NATURE", cNatureza)
			EndIf
		EndIf
	EndIf
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IAltAcd

@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39IVlPaAc()

Local lRet 	 := .T.
Local cError := STR0019 + CRLF //"Solicite ao administrador do sistema o verificar os itens abaixo: " 

lRet := T38VldPrm("MV_TURACCR", "C12", @cError)
	
If !lRet
	Help( , , STR0002, , cError, 1, 0) //"Aten��o"	
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39IAltAcd

@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function T39IAdField(oModel)

Local oStruG3Q := oModel:GetModel("G3Q_ITENS"):GetStruct()

oStruG3Q:AddField(STR0020, ; 	 									// [01] C Titulo do campo   //"Altera��o FOP"
				  STR0020, ; 	 									// [02] C ToolTip do campo  //"Altera��o FOP"	
				  "XXX_ALTFOP", ; 									// [03] C identificador (ID) do Field
				  "C", ; 											// [04] C Tipo do campo
				  1, ;   											// [05] N Tamanho do campo
				  0, ;   											// [06] N Decimal do campo
				  {|| .T.}, ; 										// [07] B Code-block de valida��o do campo
				  Nil, ; 	 										// [08] B Code-block de valida��o When do campo
				  {STR0022, STR0023}, ;								// [09] A Lista de valores permitido do campo //'0=N�o'###'1=Sim'
				  .T., ;               								// [10] L Indica se o campo tem preenchimento obrigat�rio
				  FwBuildFeature(STRUCT_FEATURE_INIPAD, "0"), ; 	// [11] B Code-block de inicializacao do campo
				  Nil, ;                                            // [12] L Indica se trata de um campo chave
				  .F., ;                                            // [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
				  .T.)                                              // [14] L Indica se o campo � virtual

oStruG3Q:AddField(STR0021, ; 	 									// [01] C Titulo do campo //"Altera��o Moeda"
				  STR0021, ; 	 									// [02] C ToolTip do campo    //"Altera��o Moeda"
				  "XXX_ALTMOE", ; 									// [03] C identificador (ID) do Field
				  "C", ;      										// [04] C Tipo do campo
				  1, ;        										// [05] N Tamanho do campo
				  0, ;        										// [06] N Decimal do campo
				  {|| .T.}, ; 										// [07] B Code-block de valida��o do campo
				  Nil, ;      										// [08] B Code-block de valida��o When do campo
				  {STR0022, STR0023}, ; 							// [09] A Lista de valores permitido do campo //'0=N�o'###'1=Sim'
				  .T., ; 											// [10] L Indica se o campo tem preenchimento obrigat�rio
				  FwBuildFeature(STRUCT_FEATURE_INIPAD, "0"),; 		// [11] B Code-block de inicializacao do campo
				  Nil, ; 									   		// [12] L Indica se trata de um campo chave
				  .F., ; 									   		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
				  .T.)  									   		// [14] L Indica se o campo � virtual
Return