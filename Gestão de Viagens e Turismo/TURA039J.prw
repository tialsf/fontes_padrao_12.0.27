#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039J.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39JCAlFO
Fun��o para cancelamento do acerto da FOP

@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA39JCAlFO(lForce, lAssocia, lDelRecurs)

Local aArea		:= GetArea()
Local aAreaG6J  := G6J->(GetArea())
Local oModel	:= FwModelActive()
Local oMdlG6I	:= oModel:GetModel("G6I_ITENS")
Local oMdlG3R	:= oModel:GetModel("G3R_ITENS")
Local oMdlT34	:= Nil
Local oMdlG3Q	:= Nil
Local aRV		:= {}
Local aDadosUsr	:= {}
Local aAtuSts 	:= {}
Local aItemG6I	:= {}
Local aManAss	:= {}
local cBkpFil	:= cFilAnt
Local cNumFat 	:= oMdlG6I:GetValue("G6I_FATURA") 
Local cConcil	:= oMdlG6I:GetValue("G6I_CONCIL")
Local lRet		:= .T.
Local nX 		:= 1
Local nY		:= 1

Default lForce	   := .F.
Default lAssocia   := .T.
Default lDelRecurs := .F.
	
If lForce .Or. FwAlertYesNo(STR0001, STR0002) 	//"Todas altera��es em registros de acertos ser�o perdidas. Deseja prosseguir com o cancelamento da altera��o da FOP no Registro de Vendas?"###"Aten��o"
	If lForce
		aAdd(aRV, {oMdlG3R:GetValue("G3R_MSFIL"), oMdlG3R:GetValue("G3R_NUMID"), oMdlG3R:GetValue("G3R_IDITEM"), oMdlG6I:GetLine()})			
	Else
		For nX := 1 to oMdlG6I:Length()
			oMdlG6I:GoLine(nX)
	
			//Armazena Filial, Num. RV e Id Item de todos os RV's que ser�o alterados
			If oMdlG6I:GetValue("TMP_OK") .And. oMdlG6I:GetValue("G6I_SITUAC") == "2" .And. oMdlG6I:GetValue("G6I_META") <> "1" 
				If T39HasAltFOP(oModel)
					aAdd(aRV, {oMdlG3R:GetValue("G3R_MSFIL"), oMdlG3R:GetValue("G3R_NUMID"), oMdlG3R:GetValue("G3R_IDITEM"), oMdlG6I:GetLine()})			
				EndIf	
			EndIf 
		Next nX
	EndIf
	
	If Len(aRV) > 0
		G3P->(DbSetOrder(1))	// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
		For nX := 1 To Len(aRV)		
			cFilAnt := aRV[nX][1]
			
			//realiza a chamada da rotina de altera��o do registro de venda, apresentando somente o item selecionado
			If G3P->(DbSeek(xFilial("G3P") + aRV[nX][2]))
				T34Segmento(Val(G3P->G3P_SEGNEG))
				TURA34HierAces(__cUserId, @aDadosUsr)
				
				If Empty(aDadosUsr[1])
					Help( , , "Tura39SemAcesso", , STR0003, 1, 0) //"Seu c�digo de usu�rio n�o esta relacionado com um vendedor ou agente. Nao ser� poss�vel acessar essa rotina." 
				
				Else
					BEGIN TRANSACTION
							
						FwModelActive(oModel)
						oMdlG6I:GoLine(aRV[nX][4])					
							
						If TA39JVldAl(oMdlG6I:GetValue("G6I_FILIAL"), oMdlG6I:GetValue("G6I_CONCIL"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"))
							//Indica se ir� realizar a associa��o dos itens ap�s o estorno da altera��o da FOP  
							If lAssocia		
								If oMdlG6I:GetValue("G6I_ASSAUT") == "2"
									//Armazena as informa��es necess�rias para a associa��o manual
									aAdd(aManAss, {oMdlG6I:GetValue("G6I_ITEM"), oMdlG6I:GetValue("G6I_FILDR"), oMdlG6I:GetValue("G6I_NUMID"), oMdlG6I:GetValue("G6I_IDITEM"), oMdlG6I:GetValue("G6I_NUMSEQ")})		 	
								Else 
									// Armazena o Item do G6I que foi desassociado, para realizar a associa��o autom�tica somente destes itens
									aAdd(aItemG6I, oMdlG6I:GetValue("G6I_ITEM"))	
								EndIf
							EndIf
										 			 			 	
						 	//Desassocia todos os RV's selecionados
							If !lDelRecurs //N�o desassocia porque 
								Tur039Des(.T.)
							EndIf
							
							T34DadosUsr(aDadosUsr)
						
							oMdlT34 := FwLoadModel("TURA034")
							oMdlT34:SetOperation(MODEL_OPERATION_UPDATE)
							oMdlT34:GetModel("G3Q_ITENS"):SetLoadFilter({{"G3Q_IDITEM", "'" + aRV[nX][3] + "'", MVC_LOADFILTER_EQUAL}})
							oMdlT34:GetModel("G3Q_ITENS"):SetNoInsertLine( .T. )
							T39JSetPre(oMdlT34, "G3P_FIELDS")							
							If (lRet := oMdlT34:Activate())	
								oMdlG3Q := oMdlT34:GetModel("G3Q_ITENS")
								T39AtuIV(oMdlT34)	
								aAtuSts := {}
							
								For nY := 1 To oMdlG3Q:Length() 	
									oMdlG3Q:GoLine(nY)
									aAdd(aAtuSts, {oMdlG3Q:GetValue("G3Q_FILIAL"), oMdlG3Q:GetValue("G3Q_NUMID"), oMdlG3Q:GetValue("G3Q_IDITEM"), oMdlG3Q:GetValue("G3Q_NUMSEQ")})
								Next
							
								oMdlG3Q:GoLine(1)
										
								//N�o permite apagar linhas
								T39INoDelL(oMdlT34, "G3Q_ITENS")
												
								If lForce	
									If (lRet := oMdlT34:VldData())
										oMdlT34:CommitData()
									Else
										DisarmTransaction()
										Break
									EndIf
										
								ElseIf FWExecView(STR0004, "TURA034", 4, , {|| .T.}, , , , , , , oMdlT34) <> 0	//"Registro de Vendas" 			
									lRet := .F.
									DisarmTransaction()
									Break
								EndIf
									
								If lRet
									For nY := 1 To Len(aAtuSts)
										TurAtuStRV(aAtuSts[nY][1], aAtuSts[nY][2], aAtuSts[nY][3], aAtuSts[nY][4], "", "3", .T.)
									Next nY				
												
								EndIf
								oMdlT34:DeActivate()
								oMdlT34:Destroy()
							Else
								DisarmTransaction()
								Break
							EndIf							
						EndIf
		
					END TRANSACTION
					T34Segmento(0)
					T34DadosUsr({"", "", "", ""})
				EndIf	
			EndIf
		Next nX
		cFilAnt := cBkpFil 
		
		//Indica se ir� realizar a associa��o dos itens ap�s o estorno da altera��o da FOP  
		If lAssocia		
			//Realiza a associa��o autom�tica do registro de venda
			G6H->(DbSetOrder(1))		// G6H_FILIAL+G6H_FATURA+DTOS(G6H_EMISSA)+G6H_FORNEC+G6H_LOJA
			If G6H->(DbSeek(xFilial("G6H") + cNumFat))
				//Caso algum item de associa��o manual tenha sido alterado, � refeita a associa��o
				If Len(aManAss) > 0
					T39IAssMan(cConcil, aManAss)
				EndIf 
			
				If Len(aItemG6I) > 0
					Tu038Asso(.T., aItemG6I)
				EndIf				
			EndIf
		EndIf
	EndIf
EndIf

FwModelActive(oModel)
RestArea(aAreaG6J)
RestArea(aArea)
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39JVldAl
Fun��o para valida��o do status da G3Q

@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA39JVldAl(cFilCon, cConcil, cNumId, cIdItem)

Local aArea     := GetArea() 
Local cAliasAux := GetNextAlias()
Local nTotal	:= 0	

BeginSql Alias cAliasAux
	SELECT COUNT(*) TOTAL 
	FROM %Table:G3R% G3R
	INNER JOIN %Table:G3Q% G3Q ON G3Q_FILIAL = G3R_FILIAL AND G3Q_NUMID = G3R_NUMID AND G3Q_NUMID = G3R_NUMID AND G3Q_NUMSEQ = G3R_NUMSEQ AND G3Q_ACRFOP = '1' AND G3Q.%notDel%
	WHERE G3R_NUMID = %Exp:cNumId% AND G3R_IDITEM =  %Exp:cIdItem% AND G3R_CONCIL = %Exp:cConcil% AND G3R_FILCON = %Exp:cFilCon% AND G3R.%notDel%       
EndSql

nTotal := (cAliasAux)->TOTAL
(cAliasAux)->(DbCloseArea())

RestArea(aArea)

Return nTotal > 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39JSetPre
Fun��o para adicionar a rotina de pr�-valida��o nos modelos
@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39JSetPre(oModel, cIdModel)

Local aDependency := oModel:GetDependency(cIdModel)
Local oModelAux	  := oModel:GetModel(cIdModel)
Local nX		  := 0

If oModelAux:ClassName() == "FWFORMGRID"	
	oModelAux:SetLPre({|oModelGrid, nLine, cAction, cField| TA039JLPre(oModelGrid, nLine, cAction, cField)})
	oModelAux:SetNoInsertLine(.T.)
Else
	oModelAux:SetOnlyView(.T.)
EndIf

For nX := 1 To Len(aDependency)
	T39JSetPre( oModel, aDependency[nX][2] )
Next nX

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39JSetPre
Fun��o para pr�-valida��o nos modelos

@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function TA039JLPre(oModelGrid, nLine, cAction, cField)

Local oModel := oModelGrid:GetModel()
Local lRet	 := .T.	

If cAction == "CANSETVALUE"
	lRet := .F.
EndIf
	
Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39AtuIV
Fun��o para atualiza��o dos itens de venda

@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39AtuIV(oModel)

Local oModelG3Q	:= oModel:GetModel("G3Q_ITENS")
Local oModelG3R	:= oModel:GetModel("G3R_ITENS")
Local nX		:= 0

oModelG3Q:SetNoDeleteLine(.F.)

For nX := 1 to oModelG3Q:Length()
	If Val(oModelG3Q:GetValue("G3Q_CANFOP")) > 0
		T39JEsCaAc(oModel, "1")
		T39JEsCaAc(oModel, "2")
	
		oModelG3Q:SetValue("G3Q_VLAFOP" , 0) 
		oModelG3Q:SetValue("G3Q_ACRFOP" , "2")
		oModelG3Q:SetValue("G3Q_CANFOP" , "0")
		oModelG3Q:LoadValue("G3Q_ATUIF" , "3")
		oModelG3Q:LoadValue("G3Q_ATUIFA", "3")
		oModelG3Q:LoadValue("G3Q_STATUS", "1")
		oModelG3R:LoadValue("G3R_STATUS", "1")
	
	ElseIf oModelG3Q:GetValue("G3Q_ACRFOP") == "1"
		oModelG3Q:DeleteLine()
	EndIf
	
	//Recalcula acordos de fornecedor e cliente
	If !oModel:GetModel("G48B_ITENS"):IsEmpty()
		T34CalcAco( , , "2", "0", .T.)
	EndIf

	If !oModel:GetModel("G48A_ITENS"):IsEmpty()
		T34CalcAco( , , "1", "0", .T.)
	EndIf
Next nX
oModelG3Q:SetNoDeleteLine(.F.)
	
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39JEsCaAc
Fun��o para estornar o cancelamento dos acordos aplicados

@type function
@author Anderson toledo
@since 16/02/2017
@version 1.0
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T39JEsCaAc(oModel, cTipo)

Local oModelG48 := IIF(cTipo == "1", oModel:GetModel("G48A_ITENS"), oModel:GetModel("G48B_ITENS"))
Local nX        := 0

For nX := 1 To oModelG48:Length() 
	oModelG48:GoLine(nX)
	If oModelG48:GetValue("G48_CANFOP") == "1" 	// 1-Aberto
		oModelG48:LoadValue("G48_STATUS", "1") 	// 3-Cancelado
		oModelG48:SetValue("G48_CANFOP" , "") 	// 1-Sim
	EndIf
Next nX

Return