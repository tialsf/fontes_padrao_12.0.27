#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039K.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039K
Fun��o filtra os itens marcados na concilia��o a�rea e chama a altera��o de Naturezas

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA039K()

Local oModel	:= FwModelActive()
Local oModelG6I	:= oModel:GetModel("G6I_ITENS")
Local aFilter	:= {}

While oModelG6I:SeekLine({{"TMP_OK", .T.}})
	If oModelG6I:GetValue("G6I_SITUAC") == "2"
		aAdd(aFilter, oModelG6I:GetValue("G6I_ITEM"))
	EndIf
	oModelG6I:SetValue("TMP_OK", .F.) 
End

If !Empty(aFilter)
	TURA039NAT(aFilter)
Else
	Help( , , "TURA039K", , STR0001, 1, 0) //"Nenhum item marcado, ou itens j� aprovados."
EndIf

FwModelActive(oModel)

Return 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039NAT
Fun��o que ativa a altera��o de Naturezas na concilia��o a�rea

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@param aFilter, array, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TURA039NAT(aFilter)

Local oModel := FwLoadModel("TURA039K")

If len(aFilter) > 0
	oModel:GetModel("G6I_ITENS"):SetLoadFilter({{"G6I_ITEM", T39Arr2Str(aFilter), MVC_LOADFILTER_IS_CONTAINED}})
EndIf

oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:SetVldActivate({|| .T. })
oModel:SetCommit({|oModel| FwFormCommit(oModel)})
oModel:GetModel("G6J_MASTER"):SetOnlyQuery(.T.)
oModel:GetModel("G6I_ITENS"):SetOnlyQuery(.T.)
oModel:Activate()	

FWExecView(STR0002, "TURA039K", 4, , {|| .T.}, , , , , , , oModel)

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo para edi��o de Naturezas na concilia��o a�rea

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   	:= MPFormModel():New("TURA039K", /*bPreValidacao*/, /*bPosValid*/, /*bCommit*/, /*bCancel*/ )
Local oStruG48B	:= FWFormStruct(1, "G48", /*bAvalCampo*/, .F.) 
Local bLoad     := {|oModel| LOAD(oModel)}
Local bLinePre	:= {|oModelGrid, nLine, cAction, cField| TA039KLPre(oModelGrid, nLine, cAction, cField)}
Local xAux		:= {}

// Adiciona a descricao do Modelo de Dados
oModel:SetDescription(STR0002) //"Naturezas"

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
oModel:AddFields("G6J_MASTER", /*cOwner*/, FWFormStruct(1, "G6J", /*bAvalCampo*/, .F.), /*bPreValidacao*/, /*bPosValidacao*/, bLoad)
oModel:GetModel("G6J_MASTER"):SetDescription( STR0002 ) //"Naturezas" 

oModel:AddGrid("G6I_ITENS", "G6J_MASTER", FWFormStruct(1, "G6I", /*bAvalCampo*/, .F.), /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G6I_ITENS", {{"G6I_FILIAL", "G6J_FILIAL"}, {"G6I_CONCIL", "G6J_CONCIL"}}, G6I->(IndexKey(7)))
oModel:GetModel("G6I_ITENS"):SetNoInsertLine(.T.)	
oModel:GetModel("G6I_ITENS"):SetNoUpdateLine(.T.)	
oModel:GetModel("G6I_ITENS"):SetNoDeleteLine(.T.)
							
oModel:AddGrid("G3R_ITENS", "G6I_ITENS", FWFormStruct(1, "G3R", /*bAvalCampo*/, .F.), /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G3R_ITENS", {{"G3R_FILCON", "G6I_FILIAL"}, {"G3R_CONCIL", "G6I_CONCIL"}, {"G3R_MSFIL", "G6I_FILDR"}, {"G3R_NUMID", "G6I_NUMID"}, {"G3R_IDITEM", "G6I_IDITEM"}}, G3R->(IndexKey(2)))
oModel:GetModel("G3R_ITENS"):SetDescription( STR0003) //"Documentos de Reserva"
oModel:GetModel("G3R_ITENS"):SetNoInsertLine(.T.)		
oModel:GetModel("G3R_ITENS"):SetNoDeleteLine(.T.)

oModel:AddFields("G3P_FIELDS", "G3R_ITENS", FWFormStruct(1, "G3P", /*bAvalCampo*/, .F.))
oModel:SetRelation("G3P_FIELDS", {{"G3P_FILIAL", "G3R_FILIAL"}, {"G3P_NUMID", "G3R_NUMID"}}, G3P->(IndexKey(1)))
oModel:GetModel("G3P_FIELDS"):SetOnlyQuery(.T.)
oModel:GetModel("G3P_FIELDS"):SetOptional(.T.)

oModel:AddGrid("G48B_ITENS", "G3R_ITENS", oStruG48B, bLinePre, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*bLoad*/)
oModel:SetRelation("G48B_ITENS", {{"G48_FILIAL", "G3R_FILIAL"}, {"G48_NUMID", "G3R_NUMID"}, {"G48_IDITEM", "G3R_IDITEM"}, {"G48_NUMSEQ", "G3R_NUMSEQ"}, {"G48_STATUS", "'1'"}, {"G48_CLIFOR", "'2'"}, {"G48_CODAPU", "''"}}, G48->(IndexKey(1)))
oModel:GetModel("G48B_ITENS"):SetDescription( STR0004 ) //"Acordos de Fornecedor"									
oModel:GetModel("G48B_ITENS"):SetOptional(.T.)
oModel:GetModel("G48B_ITENS"):SetNoInsertLine(.T.) 
	
oModel:AddGrid("G4CB_ITENS", "G48B_ITENS", FWFormStruct(1, "G4C", /*bAvalCampo*/, .F.))
oModel:SetRelation("G4CB_ITENS", {{"G4C_FILIAL", "G48_FILIAL"}, {"G4C_NUMID", "G48_NUMID"}, {"G4C_IDITEM", "G48_IDITEM"}, {"G4C_NUMSEQ", "G48_NUMSEQ"}, {"G4C_APLICA", "G48_APLICA"}, {"G4C_CLIFOR", "'2'"}}, G4C->(IndexKey(1)))							
oModel:GetModel("G4CB_ITENS"):SetOptional(.T.)

//Gatilhos
xAux := FwStruTrigger("G48_NATURE", "G48_NATURE", "Tura39KTrg('G48_NATURE')", .F.)
oStruG48B:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])

xAux := FwStruTrigger("G48_RECCON", "G48_RECCON", "Tura39KTrg('G48_RECCON')", .F.)
oStruG48B:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])
		
Return oModel

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de altera��o de Naturezas na concilia��o a�rea

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView	   := FWFormView():New()
Local oStruG6I := FWFormStruct(2, "G6I")
Local oStruG3R := FWFormStruct(2, "G3R")
Local oStruG48 := FWFormStruct(2, "G48")

oStruG6I:SetProperty("*"		 , MVC_VIEW_CANCHANGE, .F.)
oStruG3R:SetProperty("G3R_NATURE", MVC_VIEW_ORDEM	 , "00")	
oStruG3R:SetProperty("*"		 , MVC_VIEW_CANCHANGE, .F.)
oStruG3R:SetProperty("G3R_NATURE", MVC_VIEW_CANCHANGE, .T.)
oStruG48:SetProperty("G48_RECCON", MVC_VIEW_ORDEM	 , "00")	
oStruG48:SetProperty("G48_NATURE", MVC_VIEW_ORDEM	 , "01")			
oStruG48:SetProperty("*"		 , MVC_VIEW_CANCHANGE, .F.)
oStruG48:SetProperty("G48_NATURE", MVC_VIEW_CANCHANGE, .T.)
oStruG48:SetProperty("G48_RECCON", MVC_VIEW_CANCHANGE, .T.)

oStruG48:RemoveField("G48_OK")

// Define qual o Modelo de dados ser� utilizado
oView:SetModel(FWLoadModel("TURA039K"))

oView:AddGrid("VIEW_G6I", oStruG6I, "G6I_ITENS")
oView:EnableTitleView("VIEW_G6I", STR0005) 			//"Itens da fatura"

oView:AddGrid("VIEW_G3R", oStruG3R, "G3R_ITENS")
oView:EnableTitleView("VIEW_G3R", STR0003)			//"Documentos de Reserva"

oView:AddGrid("VIEW_G48", oStruG48, "G48B_ITENS")	
oView:EnableTitleView("VIEW_G48", STR0004)			//"Acordos de Fornecedor"
	
oView:CreateHorizontalBox("BOX_G6I", 30)
oView:CreateHorizontalBox("BOX_G3R", 30)
oView:CreateHorizontalBox("BOX_G48", 40)

oView:SetOwnerView("VIEW_G6I", "BOX_G6I")
oView:SetOwnerView("VIEW_G3R", "BOX_G3R")
oView:SetOwnerView("VIEW_G48", "BOX_G48")
	
oView:AddUserButton(STR0009, STR0010, {|| T039KAltNat()}, , , {MODEL_OPERATION_INSERT, MODEL_OPERATION_UPDATE}) 	//"Natureza autom�tica"###"Atualiza��o autom�tica de natureza"
		
Return oView

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
View de altera��o de Naturezas na concilia��o a�rea

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function T039KAltNat()

Local oModel 	:= FwModelActive()	
Local oModelG6I	:= oModel:GetModel("G6I_ITENS")
Local oModelG3R	:= oModel:GetModel("G3R_ITENS")
Local aLines	:= FwSaveRows()
Local nX		:= 0
Local nY		:= 0
	
If !Findfunction("U_TURNAT")
	TurHelp(STR0011, STR0012, "T39KNOFUNC") 	//"Ponto de entrada U_TURNAT n�o consta no RPO."###"Entre em contato com o administrador do ERP."
	Return
Else
	If FwAlertYesNo(STR0013, STR0014) 	//"Confirma a atualiza��o das naturezas n�o informadas?"###"Atualiza��o de naturezas"
		For nX := 1 To oModelG6I:Length()
			oModelG6I:GoLine(nX)
			For nY := 1 To oModelG3R:Length()
				oModelG3R:GoLine(nY)
				TA038AtNat(oModel, oModelG3R:GetValue("G3R_MSFIL"))
			Next nY
		Next nX
	EndIf
EndIf
	
FwRestRows(aLines)	
		
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Load
Fun��o para carga inicial do modelo

@type function
@author osmar.junior
@since 09/08/2017
@version 1.0
@param oMdl, objeto, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Static Function Load(oMdl)

Local aArea	   	 := GetArea()
Local oStructG6J := oMdl:GetStruct()
Local nOperation := oMdl:GetOperation()
Local nX		 := 0	
Local aCampos    := oStructG6J:GetFields()
Local aLoad		 := {}
Local aDados	 := {}
Local uValor 	 := Nil

If nOperation <> MODEL_OPERATION_INSERT
	For nX := 1 To Len(aCampos)
		If !(aCampos[nX][MODEL_FIELD_VIRTUAL])
			uValor := G6J->&(aCampos[nX][MODEL_FIELD_IDFIELD])
			aAdd(aLoad, uValor)
		Else
			uValor := CriaVar(aCampos[nX][MODEL_FIELD_IDFIELD], .F.)
			aAdd(aLoad, uValor)
		EndIf
	Next
	aDados := {aLoad, G6J->(Recno())}
EndIf

RestArea(aArea)
	
Return aDados

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} Tura39KTrg
Fun��o que centraliza os gatilhos

@type function
@author osmar.junior
@since 10/08/2017
@version 1.0
@param cCampo, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function Tura39KTrg(cCampo)

Local nItens   := 0
Local oModel   := FwModelActive()
Local oMdlG48B := oModel:GetModel("G48B_ITENS")
Local oMdlG4CB := oModel:GetModel("G4CB_ITENS")

If cCampo == "G48_NATURE"
	For nItens := 1 To oMdlG4CB:Length()
		oMdlG4CB:GoLine(nItens)
		If !oMdlG4CB:IsDeleted()
			oMdlG4CB:LoadValue("G4C_NATUR", oMdlG48B:GetValue("G48_NATURE") )
		EndIf
	Next nItens
ElseIf cCampo == "G48_RECCON"
	TA039TrRec(oMdlG48B:GetValue("G48_RECCON"))
EndIf

oMdlG48B := Nil
oMdlG4CB := Nil

Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA039KLPre
(long_description)

@type function
@author osmar.junior
@since 10/08/2017
@version 1.0
@param oModelGrid, objeto, (Descri��o do par�metro)
@param nLine, num�rico, (Descri��o do par�metro)
@param cAction, character, (Descri��o do par�metro)
@param cField, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example (examples)
@see (links_or_references)
/*/
//--------------------------------------------------------------------------------------------------------------------
Function TA039KLPre(oModelGrid, nLine, cAction, cField)

Local cModelId := oModelGrid:GetId()
Local lRet 	   := .T.

If cModelId == "G48B_ITENS"
	If cAction == "CANSETVALUE"
		If Empty(oModelGrid:GetValue("G48_CODACD"))
			FwAlertWarning(STR0008, STR0007) //"Nenhum acordo aplicado."###"A��o n�o permitida."
			lRet := .F.
		ElseIf cField == "G48_RECCON" .And. oModelGrid:GetValue("G48_CLASS") == "C11" .And. oModelGrid:GetValue("G48_RECCON")
			FwAlertWarning(STR0006, STR0007) //"N�o � permitido desmarcar acordos do tipo C11 - TAXA DU."###"A��o n�o permitida."
			lRet := .F.
		EndIf
	EndIf
EndIf
		
Return lRet