#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA039M.CH"

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA039M
Concilia��o de Metas

@type function
@param	
@return nil
@example  TURA039M()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references)
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TURA039M()
TURA039()
Return

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Define o modelo de dados TURA039M, utilizado na pesquisa de metas

@type static function
@param	
@return oModel:	Objeto. Objeto da Classe MPFormModel
@example oModel := ModelDef()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel   := FwLoadModel("TURA039")
Local oStruG6M := FWFormStruct(1, "G6M")

If !oStruG6M:HasField("G6M_CODAPU")
	oStruG6M:AddField(Alltrim(GetSX3Cache("G6M_CODAPU", "X3_TITULO")), ; 							 		// [01] C Titulo do campo	//#"C�d. Fornec"
					  GetSX3Cache("G6M_CODAPU", "X3_DESCRIC"), ; 						 					// [02] C ToolTip do campo	//#"C�d. Fornec"
					  "G6M_CODAPU", ; 							 											// [03] C identificador (ID) do Field
					  TamSX3("G6M_CODAPU")[3], ; 										 					// [04] C Tipo do campo
					  TamSX3("G6M_CODAPU")[1], ; 									 						// [05] N Tamanho do campo
					  TamSX3("G6M_CODAPU")[2], ; 									 						// [06] N Decimal do campo
					  MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_VALID")) + "}"), ;   		// [07] B Code-block de valida��o do campo
					  MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_WHEN")) + "}"), ;	 		// [08] B Code-block de valida��o When do campoz
					  NIL, ; 												 								// [09] A Lista de valores permitido do campo
					  NIL, ; 												 								// [10] L Indica se o campo tem preenchimento obrigat�rio
					  MontaBlock("{|| " + Alltrim(GetSX3Cache("G6M_CODAPU", "X3_RELACAO")) + "}"), ; 		// [11] B Code-block de inicializacao do campo
					  NIL, ; 												 								// [12] L Indica se trata de um campo chave
					  .T., ; 												 								// [13] L Indica se o campo pode receber valor em uma opera��o de update.
					  .F. ) 												 								// [14] L Indica se o campo � virtual
EndIf

oStruG6M:SetProperty("G6M_SEGNEG", MODEL_FIELD_OBRIGAT, .F.)

//Acrescentado submodelo de Metas
oModel:AddGrid("G6M_ITENS", "G6I_ITENS", oStruG6M)
oModel:SetRelation("G6M_ITENS", {{"G6M_FILCON", "G6I_FILIAL"}, {"G6M_FATMET", "G6I_FATURA"}, {"G6M_ITFAT", "G6I_ITEM"}, {"G6M_CONCIL", "G6I_CONCIL"}}, G6M->(IndexKey(2)))
oModel:GetModel("G6M_ITENS"):SetDescription(STR0008)		// "Metas"
oModel:GetModel("G6M_ITENS"):SetOptional(.T.)
oModel:GetModel("G6M_ITENS"):SetOnlyView(.T.)

oModel:GetModel("G3R_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G3R_ITENS"):SetOptional(.T.)
	
oModel:GetModel("G3P_FIELDS"):SetOnlyQuery(.T.)
oModel:GetModel("G3P_FIELDS"):SetOptional( .T. )

oModel:GetModel("G3Q_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G3Q_ITENS"):SetOptional( .T. )

oModel:GetModel("G3S_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G3S_ITENS"):SetOptional(.T.)

oModel:GetModel("G3T_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G3T_ITENS"):SetOptional(.T.)

oModel:GetModel("G44_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G44_ITENS"):SetOptional(.T.)

oModel:GetModel("G45_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G45_ITENS"):SetOptional(.T.)

oModel:GetModel("G46_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G46_ITENS"):SetOptional(.T.)

oModel:GetModel("G47_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G47_ITENS"):SetOptional(.T.)

oModel:GetModel("G49_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G49_ITENS"):SetOptional(.T.)

oModel:GetModel("G4B_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4B_ITENS"):SetOptional(.T.)

oModel:GetModel("G4A_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4A_ITENS"):SetOptional(.T.)

oModel:GetModel("G48A_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G48A_ITENS"):SetOptional(.T.)

oModel:GetModel("G48B_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G48B_ITENS"):SetOptional(.T.)

oModel:GetModel("G4E_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4E_ITENS"):SetOptional(.T.)

oModel:GetModel("G4CA_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4CA_ITENS"):SetOptional(.T.)

oModel:GetModel("G4CB_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4CB_ITENS"):SetOptional(.T.)

oModel:GetModel("G4D_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G4D_ITENS"):SetOptional(.T.)
	
oModel:GetModel("G9KA_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G9KA_ITENS"):SetOptional(.T.)

oModel:GetModel("G9KB_ITENS"):SetOnlyQuery(.T.)
oModel:GetModel("G9KB_ITENS"):SetOptional(.T.)

oModel:GetModel("ENTIDADE"):SetOnlyQuery(.T.)
oModel:GetModel("ENTIDADE"):SetOptional(.T.)									
									
Return(oModel)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Define a view TURA039M, utilizado na pesquisa de metas

@type static function
@param	
@return oView:	Objeto. Objeto da Classe FwFormView
@example oView := ViewDef()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oView		:= FWFormView():New()
Local oModel	:= FwLoadModel("TURA039M")
Local oStruG6J  := FWFormStruct(2, "G6J")
Local oStruG6I  := FWFormStruct(2, "G6I")
Local oStruG6M  := FWFormStruct(2, "G6M")

T39MStrItFat(oStruG6I, "V")

oView:SetModel(oModel)

oView:AddField("VIEW_G6J", oStruG6J, "G6J_MASTER")
oView:AddGrid("VIEW_G6I" , oStruG6I, "G6I_ITENS")
oView:AddGrid("VIEW_G6M" , oStruG6M, "G6M_ITENS")

oView:CreateHorizontalBox("SUPERIOR", 20)
oView:CreateHorizontalBox("MEIO"	, 30)
oView:CreateHorizontalBox("INFERIOR", 50)

oView:SetOwnerView("VIEW_G6J", "SUPERIOR")
oView:SetOwnerView("VIEW_G6I", "MEIO")
oView:SetOwnerView("VIEW_G6M", "INFERIOR")

oView:EnableTitleView("VIEW_G6J")
oView:EnableTitleView("VIEW_G6I", STR0001)	//"Itens da Fatura(Bilhetes)"
oView:EnableTitleView("VIEW_G6M", STR0002)	//"Apura��es de Metas"

oView:AddUserButton(STR0003, STR0003, {|oModel| Tur039Des()})	//"Desassociar"#"Desassociar"
oView:AddUserButton(STR0004, STR0004, {|oModel| Tur039Apr()})	//"Aprovar/Desaprovar"

Return(oView)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Define o menu de op��es baseado na rotina TURA039 concilia��o a�rea

@type static function
@param	
@return aRotina:	Array. Array aRotina
@example aRotina := MenuDef()

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function MenuDef()
Return FWLoadMenuDef("TURA039")

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} T39MStrItFat
Fun��o para cria��o dos campos virtuais referente ao Model

@type function
@author Anderson Toledo
@since 10/08/2016
@version 1.0
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Static Function T39MStrItFat(oStruG6I,cTipo)
	
If cTipo == "M"	
	oStruG6I:AddField("", ; 		// [01] C Titulo do campo
					  "", ; 		// [02] C ToolTip do campo
					  "TMP_OK", ;   // [03] C identificador (ID) do Field
					  "L", ; 		// [04] C Tipo do campo
					  1, ;   		// [05] N Tamanho do campo
					  0, ;   		// [06] N Decimal do campo
					  NIL, ; 		// [07] B Code-block de valida��o do campo
					  NIL, ; 		// [08] B Code-block de valida��o When do campoz
					  NIL, ; 		// [09] A Lista de valores permitido do campo
					  NIL, ; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
					  NIL, ; 		// [11] B Code-block de inicializacao do campo
					  .F., ; 		// [12] L Indica se trata de um campo chave
					  .F., ; 		// [13] L Indica se o campo pode receber valor em uma opera��o de update.
					  .F.)   		// [14] L Indica se o campo � virtual

	oStruG6I:AddField("", "", "G6I_LEGASS", "BT", 1, 0, {||Tur039Leg()}, NIL, NIL, .F., {||Tur039Sts()}, NIL, .T., .T.)
Else
	oStruG6I:AddField("TMP_OK", ; 		// [01] C Nome do Campo
					  "00", ;     		// [02] C Ordem
					  "", ;       		// [03] C Titulo do campo
					  "", ;       		// [04] C Descri��o do campo
					  NIL, ;      		// [05] A Array com Help
					  "L", ;      		// [06] C Tipo do campo
					  NIL, ;      		// [07] C Picture
					  NIL, ;      		// [08] B Bloco de Picture Var
					  "", ;       		// [09] C Consulta F3
					  .T., ;      		// [10] L Indica se o campo � edit�vel
					  NIL, ;      		// [11] C Pasta do campo
					  NIL, ;      		// [12] C Agrupamento do campo
					  NIL, ;      		// [13] A Lista de valores permitido do campo (Combo)
					  NIL, ;      		// [14] N Tamanho M�ximo da maior op��o do combo
					  NIL, ;      		// [15] C Inicializador de Browse
					  .T., ;      		// [16] L Indica se o campo � virtual
					  NIL) 	      		// [17] C Picture Vari�vel
	
	oStruG6I:AddField("G6I_LEGASS", "01", "", "", {}, "BT", "", NIL, NIL, .F., "", NIL, NIL, NIL, NIL, .T., NIL)
EndIf

Return 

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39MEsAsMts
Fun��o que efetua a desassocia��o das apura��es de metas - atualizando em massa a tabela G6M - no momento em 
que a concilia��o � exclu�da. Op��o do MenuDef. A��es Relacionadas > Excluir

@type function
@param	oMdlG6I:	Objeto. Objeto da Classe FwFormGridModel (G6I_ITENS) 
		nLine:		Num�rico. Nro da Linha posicionada no grid do objeto oMdlG6I
@return lRet:	L�gico. .t. atualiza��o efetuada com sucesso.
@example lRet := TA39MEsAsMts(oMdlG6I,nLine)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TA39MEsAsMts(oMdlG6I, nLine)

Local cSts := ""

cSts := "UPDATE " + RetSQLName("G6M") 
cSts += "SET G6M_FILCON = '', G6M_CONCIL = '', G6M_FATMET = '', G6M_ITFAT  = '' "
cSts += "WHERE G6M_FILCON = '" + oMdlG6I:GetValue("G6I_FILIAL", nLine) + "' AND " 
cSts += "      G6M_FATMET = '" + oMdlG6I:GetValue("G6I_FATURA", nLine) + "' AND "
cSts += "	   G6M_ITFAT  = '" + oMdlG6I:GetValue("G6I_ITEM"  , nLine) + "' AND "
cSts += "	   G6M_CONCIL = '" + oMdlG6I:GetValue("G6I_CONCIL", nLine) + "' AND "
cSts += IIF(TCSrvType() == "AS/400", "@DELETED@ = ' '", "D_E_L_E_T_ = ' '")

Return TCSQLExec(cSts) >= 0

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39MEsAsMts
Fun��o que efetua a desassocia��o das apura��es de metas atrav�s da op��o de formul�rio (tela de Concilia��o) em Outras A��es > Desassociar

@type function
@param	oModel:	Objeto. Objeto da Classe FwFormModel
@return lRet:	L�gico. .t. atualiza��o efetuada com sucesso.
@example TA39MDesMetas(oModel)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TA39MDesMetas(oModel)

Local oModelG6I	:= oModel:GetModel("G6I_ITENS")
Local oModelG6M	:= oModel:GetModel("G6M_ITENS")
Local lRet		:= .T.
Local nI		:= 0

For nI := 1 To oModelG6I:Length()
	If oModelG6I:GetValue("TMP_OK", nI) .And. oModelG6I:GetValue("G6I_META", nI) == "1"
		oModelG6I:GoLine(nI)

		//Estorno da Associa��o de Meta. Atualiza os registros de G6M  
		lRet := TA39MEsAsMts(oModelG6I, nI)

		//Estorno da Concilia��o de Meta. Atualiza os registros de G6I
		lRet := Tur39EsG6I(oModel)
	EndIf
Next nI

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39MVldMetas
Fun��o que efetua algumas valida��es antes de aprovar a meta associada ao item da fatura. As valida��es s�o:
	- O Valor da apura��o de meta n�o pode ser diferente do valor l�quido do item da fatura
	- A apura��o deve estar liberada para efetuar a aprova��o
	- A apura��o de meta n�o pode ter uma concilia��o que fora efetivada previamente.  

@type function
@param	oModelG6I:	Objeto. Objeto da Classe FwFormGridModel (G6I_ITENS)
@return lRet:	L�gico. .t. validado com sucesso.
@example lRet := TA39MVldMetas(oModelG6I)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TA39MVldMetas(oModelG6I)

Local aArea    := GetArea()
Local lRet	   := .T.
Local cChave   := ""
Local cMsg	   := ""
Local cMsgSolu := ""
Local cRet	   := ""
Local nI	   := 0	

G6M->(DbSetOrder(2))	//G6M_FILIAL+G6M_FATMET+G6M_ITFAT+G6M_CONCIL
For nI := 1 To oModelG6I:Length()
	If oModelG6I:GetValue("TMP_OK", nI)
		cChave := PadR(oModelG6I:GetValue("G6I_FILIAL", nI), TamSx3("G6M_FILCON")[1])
		cChave += PadR(oModelG6I:GetValue("G6I_FATURA", nI), TamSx3("G6M_FATMET")[1])
		cChave += PadR(oModelG6I:GetValue("G6I_ITEM"  , nI), TamSx3("G6M_ITFAT")[1])
		cChave += PadR(oModelG6I:GetValue("G6I_CONCIL", nI), TamSx3("G6M_CONCIL")[1])
		
		If G6M->(DbSeek(cChave))
			If G6M->G6M_TOTAL <> oModelG6I:GetValue("G6I_VLRLIQ", nI)
				lRet     := .F.
				cMsg     := I18N(STR0014, {Alltrim(G6M->G6M_CODAPU), Alltrim(oModelG6I:GetValue("G6I_FATURA", nI)),Alltrim(oModelG6I:GetValue("G6I_ITEM", nI))})			// "A apura��o de metas #1 n�o possui o mesmo valor que o item da fatura #2/#3." 
				cMsgSolu := I18N(STR0007, {Chr(13), Transform(oModelG6I:GetValue("G6I_VLRLIQ", nI), PesqPict("G6I", "G6I_VLRLIQ")), Chr(13)})								// "Ajuste o valor da apura��o. #1O Valor do item da fatura � #2.#3"
				cMsgSolu += I18N(STR0009, {Transform(G6M->G6M_TOTAL, PesqPict("G6I", "G6I_VLRLIQ"))}) 																		// "O Valor da apura��o de metas � #1."
				FwAlertHelp(cMsg, cMsgSolu, STR0010)																														// "Aten��o"
				Exit
			EndIf
			
			If lRet
				If Empty((cRet := TA43SeekG81("G81_STATUS", G6M->G6M_FILIAL, G6M->G6M_CODAPU, G6M->G6M_SEGNEG)))
					lRet     := .F.
					cMsg     := I18N(STR0011, {Alltrim(G6M->G6M_CODAPU), Alltrim(oModelG6I:GetValue("G6I_FATURA", nI)), Alltrim(oModelG6I:GetValue("G6I_ITEM", nI))})	// "A apura��o de metas #1 que foi conciliada com o item da fatura #2/#3, n�o foi liberada."   
					cMsgSolu := STR0013 + Chr(13)					// "Somente os itens de fatura conciliados com Apura��es Liberadas poder�o ser aprovadas."
					cMsgSolu += STR0015								// "Na rotina de Apura��o de Receitas de Metas, libere a apura��o mencionada." 
					FwAlertHelp(cMsg, cMsgSolu, STR0010)			// "Aten��o"
					Exit

				ElseIf cRet == "2"
					lRet := .F.
					cMsg := I18N(STR0005, {Alltrim(G6M->G6M_CODAPU), Alltrim(oModelG6I:GetValue("G6I_FATURA", nI)), Alltrim(oModelG6I:GetValue("G6I_ITEM", nI))})	// "A apura��o de metas #1 que foi conciliada com o item da fatura #2/#3, foi efetivada. "   
					FwAlertHelp(cMsg, STR0017, STR0010)				// "A fatura a�rea/item n�o pode estar efetivado para desaprovar." ### "Aten��o"
					Exit
				Endif
			EndIf
		EndIf
	EndIf
Next nI

RestArea(aArea)

Return lRet

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39MTtlConcil
Fun��o que retorna o total das apura��es de metas de uma determinada fatura e fornecedor

@type function
@param	cFatura:	Caractere. Nro da Fatura A�rea
		cCodFor:	Caractere. C�digo do Fornecedor da Fatura A�rea e da Meta
		cLojFor:	Caractere. Loja do Fornecedor da Fatura A�rea e da Meta
		cFilCon:	Caractere. Filial da Concilia��o de metas	
@return nRet:	Num�rico. Valor total das metas que foram conciliadas para um determinado fornecedor, de acordo com a fatura informada
@example nRet := TA39MTtlConcil(cFatura,cCodFor,cLojFor,cFilCon)

@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0
@see (links_or_references) 
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TA39MTtlConcil(cFatura, cCodFor, cLojFor, cFilCon)

Local aArea   := GetArea()
Local cNxtTab := GetNextAlias() 
Local nRet	  := 0

Default cFilCon	:= xFilial("G6J")

BeginSQL Alias cNxtTab
	SELECT SUM(G6M_TOTAL) TOTAL
	FROM %Table:G6M% G6M
	INNER JOIN %Table:G6L% G6L ON G6L_FILIAL = G6M_FILIAL AND G6L_CODAPU = G6M_CODAPU AND G6L_TPAPUR = '2' AND G6L_FORNEC = %Exp:cCodFor% AND G6L_LJFORN = %Exp:cLojFor% AND G6L.%NotDel%
	INNER JOIN %Table:G6I% G6I ON G6I_FILIAL = G6M_FILCON AND G6I_FATURA = G6M_FATMET AND G6I_ITEM = G6M_ITFAT AND G6I_CONCIL = G6M_CONCIL AND G6I_SITUAC = '3' AND G6I.%NotDel%
	WHERE G6M_FILCON = %Exp:cFilCon% AND G6M_FATMET = %Exp:cFatura% AND G6M_CONCIL <> '' AND G6M.%NotDel%
EndSQL

nRet := (cNxtTab)->TOTAL
(cNxtTab)->(DbCloseArea())

RestArea(aArea)

Return(nRet)

//--------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA39MItNFS
Fun��o que valida par�metros fiscais e tamb�m monta array dos Itens da Nota Fiscal de sa�da

@type function
@param	aItensNFS:	Array. Array passado por refer�ncia para montagem futura da nota fiscal de sa�da. 
		oModel:		Objeto. Objeto da Classe FwFormModel
@return lRet:	L�gico. .t. Se o Par�metro fiscal do produto da meta e classifica��o do acordo de meta foi validado com sucesso
@example lRet := TA39MItNFS(aItensNFS, oModel)
@author Fernando Radu Muscalu
@since 28/12/2016
@version 1.0

@see (links_or_references)
/*/ 
//--------------------------------------------------------------------------------------------------------------------
Function TA39MItNFS(aItensNFS, oModel, cFornec, cLoja)

Local oModelG6M	:= oModel:GetModel("G6M_ITENS")
Local oModelG81	:= oModel:GetModel("G81_ITENS")
Local cMsg		:= ""
Local nI		:= 0
Local nX		:= 0
Local aParFis	:= {}
Local aFldMsg	:= {}
Local lRet		:= .T.

For nI := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nI)
	For nX := 1 To oModelG81:Length()
		aParFis := T35PesqPFF(cFilAnt, oModelG6M:GetValue("G6M_TIPOAC"), "", oModelG81:GetValue("G81_CODPRD",nX), "2", cFornec, cLoja)

		//Verifica se o produto possui c�digo de produto cadastrado nos parametros fiscais e financeiros
		If Len(aParFis) > 0 .And. !Empty(aParFis[4])
			TA42AddItNf(aItensNFS, oModelG6M:GetValue("G6M_FILREF"), aParFis[3], oModelG6M:GetValue("G6M_NATURE"), aParFis[4], aParFis[2], oModelG81:GetValue("G81_VALOR", nX))
		Else
			aFldMsg := {oModelG6M:GetValue("G6M_TIPOAC"), oModelG6M:GetValue("G6M_SEGNEG"), oModelG81:GetValue("G81_CODPRD", nX)}
			cMsg	:= STR0018	//"N�o foi localizado o parametro fiscal e financeiro para a "
			cMsg	+= STR0019	//"Classifica��o #1[Class]#, Seg. Neg. #2[Segmento]# e Produto #3[Produto]#"
			FwAlertHelp(I18N(cMsg, aFldMsg), STR0020, STR0010)	//"Cadastre o parametro fiscal e financeiro." ### "Aten��o"
			lRet := .F.
		EndIf
		
		If (!lRet)
			Exit
		EndIf	
	Next nX
	
	If (!lRet)
		Exit
	EndIf
Next nI	

Return lRet