#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TURA040.CH"
#INCLUDE "FWEDITPANEL.CH"
#INCLUDE 'FILEIO.CH'

#DEFINE ARR_DADOS		1
#DEFINE ARR_FILHOS	2
#DEFINE GRIDMAXLIN	99999

Static aApur     := {}
Static cCliCod   := ''
Static cCliLoja  := ''
Static cCliMoeda := ''
Static cCliRazao := ''
Static cSegmento := ''
Static lDelAcd   := .F.
Static aG48Dif   := {}

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TURA040
Fun��o chamada pelo menu respons�vel pela manuten��o da receita de clientes.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TURA040()

Local oBrowse := Nil 

SetKey (VK_F12,{|a,b| AcessaPerg("TURA040C",.T.)})

oBrowse := FwMBrowse():New()

oBrowse:SetAlias('G6L')
oBrowse:SetDescription(STR0097)		// "Apura��o de Receita de Cliente"

oBrowse:AddLegend( "G6L_STATUS == '1'", 'GREEN', STR0001 ) // "Apura��o em Aberto"
oBrowse:AddLegend( "G6L_STATUS == '2'", 'RED'  , STR0002 ) // "Apura��o Liberada"

oBrowse:SetFilterDefault("G6L_TPAPUR == '1'")

oBrowse:bIniWindow := {|| TurApuraBrw(oBrowse)}

oBrowse:Activate()

Set Key VK_F12 To

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Fun��o respons�vel pela cria��o do modelo de dados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ModelDef()

Local oModel    := Nil
Local oStruG6L  := FWFormStruct(1, 'G6L', /*bAvalCampo*/,    /*lViewUsado*/)	// APURACAO
Local oStruG6M  := FWFormStruct(1, 'G6M', /*bAvalCampo*/, .F./*lViewUsado*/)	// SEGMENTOS APURACAO
Local oStruG6O  := FWFormStruct(1, 'G6O', /*bAvalCampo*/, .F./*lViewUsado*/)	// RESUMO APURACAO
Local oStruG6P  := FWFormStruct(1, 'G6P', /*bAvalCampo*/, .F./*lViewUsado*/)	// MANAGEMENT
Local oStruG6N  := FWFormStruct(1, 'G6N', /*bAvalCampo*/, .F./*lViewUsado*/)	// FLAT / SUCCESS
Local oStruG48A := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	// ACORDOS APLICADOS
Local oStruG48B := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	// ACORDOS APLICADOS
Local oStruG82  := FWFormStruct(1, 'G82', /*bAvalCampo*/, .F./*lViewUsado*/)	// SERVI�OS PROPRIOS
Local oStruG80  := FWFormStruct(1, 'G80', /*bAvalCampo*/, .F./*lViewUsado*/)	// ITENS ENTIDADES ADICIONAIS
Local oStruG81  := FWFormStruct(1, 'G81', /*bAvalCampo*/, .F./*lViewUsado*/)	// ITENS FINANCEIROS DE APURA��O
Local xAux      := Nil

oModel := MPFormModel():New('TURA040', /*bPreValidacao*/, {|oModel| TA040PVal( oModel )}/*bPosValidacao*/, { |oModel| TA040Grv( oModel ) }/*bCommit*/, /*bCancel*/)

oModel:AddFields('G6L_MASTER', /*cOwner*/, oStruG6L )

oModel:AddGrid('G6M_DETAIL', 'G6L_MASTER', oStruG6M, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G6M_DETAIL', {{'G6M_FILIAL', 'xFilial( "G6M" )' }, { 'G6M_CODAPU', 'G6L_CODAPU' } }, G6M->(IndexKey(1)))

oModel:AddGrid('G82_DETAIL', 'G6L_MASTER', oStruG82, {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA040G82LP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}/*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G82_DETAIL', {{'G82_FILIAL', 'xFilial( "G82" )' }, { 'G82_CODAPU', 'G6L_CODAPU' } }, G82->(IndexKey(1)))

oModel:AddGrid('G6O_DETAIL', 'G6M_DETAIL', oStruG6O, {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA040G6OLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}/*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G6O_DETAIL', {{'G6O_FILIAL', 'xFilial( "G6O" )' }, { 'G6O_CODAPU', 'G6L_CODAPU' }, { 'G6O_SEGNEG', 'G6M_SEGNEG' }, { 'G6O_TIPOAC', 'G6M_TIPOAC' }}, G6O->(IndexKey(1)))

oModel:AddGrid('G6P_DETAIL', 'G6O_DETAIL', oStruG6P, {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA040G6PLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}/*bLinePre*/,/*bLinePost*/, /*bPreVal*/, {|oMdl,nLinha| TA040G6PLO(oMdl,nLinha)}/*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G6P_DETAIL', {{'G6P_FILIAL', 'xFilial( "G6P" )' }, { 'G6P_CODAPU', 'G6L_CODAPU' } , { 'G6P_SEGNEG', 'G6M_SEGNEG' }, { 'G6P_CODACD', 'G6O_CODACD' }}, G6P->(IndexKey(1)))

oModel:AddGrid('G6N_DETAIL', 'G6O_DETAIL', oStruG6N, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G6N_DETAIL', {{'G6N_FILIAL', 'xFilial( "G6N" )' }, { 'G6N_CODAPU', 'G6L_CODAPU' } , { 'G6N_SEGNEG', 'G6M_SEGNEG' }, { 'G6N_TIPOAC', 'G6M_TIPOAC' }, { 'G6N_CODACD', 'G6O_CODACD' }}, G6N->(IndexKey(1)))

oModel:AddGrid('G48A_DETAIL', 'G6O_DETAIL', oStruG48A , {|oMdl, nLine, cAction, cCampo, xVlBefore, xVlCurrent| TA040VLDG48(oMdl, nLine, cAction, cCampo, xVlBefore, xVlCurrent)}/*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G48A_DETAIL', {{'G48_FILAPU', 'G6L_FILIAL' }, { 'G48_CODAPU', 'G6L_CODAPU' }, { 'G48_SEGNEG', 'G6O_SEGNEG' }, { 'G48_CLASS', 'G6M_TIPOAC' } , { 'G48_CODACD', 'G6O_CODACD' }, { 'G48_CODREC', 'G6O_CODREV' }} , G48->( IndexKey( 5 ) ) )
oModel:GetModel('G48A_DETAIL'):SetLoadFilter({{'G48_STATUS', "'3'", MVC_LOADFILTER_NOT_EQUAL}})

oModel:AddGrid('G48B_DETAIL', 'G82_DETAIL', oStruG48B , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G48B_DETAIL', {{'G48_FILAPU', 'G6L_FILIAL' }, { 'G48_CODAPU', 'G6L_CODAPU' }, { 'G48_SEGNEG', 'G82_SEGNEG' }, { 'G48_CODPRD', 'G82_CODPRD' }, { 'G48_CLASS', "'V01'" }} , G48->(IndexKey(3)))

oModel:AddGrid('G80A_DETAIL', 'G6M_DETAIL', oStruG80 , {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA040G80LP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G80A_DETAIL', {{'G80_FILIAL', 'xFilial( "G80" )' }, { 'G80_CODAPU', 'G6L_CODAPU' }, { 'G80_SEGNEG', 'G6M_SEGNEG' }, { 'G80_TIPOAC', 'G6M_TIPOAC' }} , G80->(IndexKey(1)))

oModel:AddGrid('G80B_DETAIL', 'G82_DETAIL', oStruG80 , {|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA040G80LP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G80B_DETAIL', {{'G80_FILIAL', 'xFilial( "G80" )' }, { 'G80_CODAPU', 'G6L_CODAPU' }, { 'G80_SEGNEG', 'G82_SEGNEG' }, { 'G80_CODPRD', 'G82_CODPRD' }} , G80->(IndexKey(1)))

oModel:AddGrid('G81A_DETAIL', 'G6M_DETAIL', oStruG81 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G81A_DETAIL', {{'G81_FILIAL', 'xFilial( "G81" )' }, { 'G81_CODAPU', 'G6L_CODAPU' }, { 'G81_SEGNEG', 'G6M_SEGNEG' }, { 'G81_CLASS', 'G6M_TIPOAC' }} , G81->(IndexKey(1)))

oModel:AddGrid('G81B_DETAIL', 'G82_DETAIL', oStruG81 , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModel:SetRelation('G81B_DETAIL', {{'G81_FILIAL', 'xFilial( "G81" )' }, { 'G81_CODAPU', 'G6L_CODAPU' }, { 'G81_SEGNEG', 'G82_SEGNEG' }, { 'G81_CODPRD', 'G82_CODPRD' }} , G81->(IndexKey(1)))

oStruG6P:SetProperty('G6P_FORMUL', MODEL_FIELD_WHEN, {|| FwFldGet('G6P_TPCALC') == '2'})  //1=Valor;2=Formula
oStruG82:SetProperty('G82_PERDES', MODEL_FIELD_WHEN, {|| !Empty(FwFldGet('G82_CODPRD'))})  
oStruG82:SetProperty('G82_PERCTX', MODEL_FIELD_WHEN, {|| !Empty(FwFldGet('G82_CODPRD'))})  

oStruG6N:SetProperty('G6N_VALOR' , MODEL_FIELD_OBRIGAT, .F.)
oStruG81:SetProperty('G81_CONDPG', MODEL_FIELD_OBRIGAT, .F.)

// vendas
oStruG48A:SetProperty('G48_STATUS', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48A:SetProperty('G48_CONTAC', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48A:SetProperty('G48_CCUSTO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48A:SetProperty('G48_ITEMCO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48A:SetProperty('G48_CLASVL', MODEL_FIELD_WHEN, {|| .F.})
oStruG48A:SetProperty('G48_NATURE', MODEL_FIELD_WHEN, {|| .F.})      

xAux := FwStruTrigger('G48_PERACD', 'G48_PERACD', "TA040XGAT('G48_PERACD', )", .F. )
oStruG48A:AddTrigger(xAux[1], xAux[2], xAux[3], xAux[4])   

// servi�o pr�prio
oStruG48B:SetProperty('G48_PERACD', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_VLACD' , MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_STATUS', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_CONTAC', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_CCUSTO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_ITEMCO', MODEL_FIELD_WHEN, {|| .F.})  
oStruG48B:SetProperty('G48_CLASVL', MODEL_FIELD_WHEN, {|| .F.})
oStruG48B:SetProperty('G48_NATURE', MODEL_FIELD_WHEN, {|| .F.})      

oModel:AddCalc('G6P_CALC' , 'G6O_DETAIL', 'G6P_DETAIL',  'G6P_TOTAL' , 'G6O__CALCD', 'SUM', {|| .T.}, , STR0152, , 14, 2)	// "Tot. Custo"
oModel:AddCalc('G6P_CALC' , 'G6O_DETAIL', 'G6O_DETAIL',  'G6O_VALOR' , 'G6O__CALCL', 'SUM', {|| .T.}, , STR0153, , 14, 2)	// "Lucro"
oModel:AddCalc('G6P_CALC' , 'G6O_DETAIL', 'G6O_DETAIL',  'G6O_VAPLIC', 'G6O__CALCR', 'SUM', {|| .T.}, , STR0154, , 14, 2)	// "Tot. Receita"

oModel:AddCalc('G80_CALCA', 'G6M_DETAIL', 'G80A_DETAIL', 'G80_PERC'  , 'G68__CALCP', 'FORMULA', {|| .T.}, , STR0155, {|oModel, nTotalAtual, xValor, lSomando| TA040CalcV(oModel, nTotalAtual, xValor, lSomando, '1')}, 14, 2)	// "Total %"
oModel:AddCalc('G80_CALCA', 'G6M_DETAIL', 'G80A_DETAIL', 'G80_VLRLIQ', 'G68__CALCV', 'FORMULA', {|| .T.}, , STR0156, {|oModel, nTotalAtual, xValor, lSomando| TA040CalcV(oModel, nTotalAtual, xValor, lSomando, '1')}, 14, 2)	// "Total Valor"

oModel:AddCalc('G80_CALCB', 'G82_DETAIL', 'G80B_DETAIL', 'G80_PERC'  , 'G80__CALCC', 'FORMULA', {|| .T.}, , STR0155, {|oModel, nTotalAtual, xValor, lSomando| TA040CalcV(oModel, nTotalAtual, xValor, lSomando, '1')}, 14, 2)	// "Total %"
oModel:AddCalc('G80_CALCB', 'G82_DETAIL', 'G80B_DETAIL', 'G80_VLRLIQ', 'G80__CALCD', 'FORMULA', {|| .T.}, , STR0156, {|oModel, nTotalAtual, xValor, lSomando| TA040CalcV(oModel, nTotalAtual, xValor, lSomando, '1')}, 14, 2)	// "'Total Valor"

oModel:GetModel('G6M_DETAIL' ):SetUniqueLine({'G6M_FILIAL', 'G6M_CODAPU', 'G6M_SEGNEG', 'G6M_TIPOAC'})
oModel:GetModel('G82_DETAIL' ):SetUniqueLine({'G82_FILIAL', 'G82_CODAPU', 'G82_SEGNEG', 'G82_CODPRD'})
oModel:GetModel('G6O_DETAIL' ):SetUniqueLine({'G6O_FILIAL', 'G6O_CODAPU', 'G6O_SEGNEG', 'G6O_CODACD', 'G6O_CODREV'})
oModel:GetModel('G6P_DETAIL' ):SetUniqueLine({'G6P_FILIAL', 'G6P_CODAPU', 'G6P_SEGNEG', 'G6P_CODACD', 'G6P_ITEM'})
oModel:GetModel('G6N_DETAIL' ):SetUniqueLine({'G6N_FILIAL', 'G6N_CODAPU', 'G6N_SEGNEG', 'G6N_CODACD', 'G6N_ITEM'})
oModel:GetModel('G48A_DETAIL'):SetUniqueLine({'G48_FILIAL', 'G48_NUMID' , 'G48_IDITEM', 'G48_NUMSEQ', 'G48_APLICA', 'G48_CODACD'})
oModel:GetModel('G48B_DETAIL'):SetUniqueLine({'G48_FILIAL', 'G48_NUMID' , 'G48_IDITEM', 'G48_NUMSEQ', 'G48_APLICA', 'G48_CODACD'})
oModel:GetModel('G80B_DETAIL'):SetUniqueLine({'G80_FILIAL', 'G80_CODAPU', 'G80_SEGNEG', 'G80_TIPOAC', 'G80_CODPRD', 'G80_SEQUEN'})
oModel:GetModel('G80A_DETAIL'):SetUniqueLine({'G80_FILIAL', 'G80_CODAPU', 'G80_SEGNEG', 'G80_TIPOAC', 'G80_CODPRD', 'G80_SEQUEN'})
oModel:GetModel('G81A_DETAIL'):SetUniqueLine({'G81_FILIAL', 'G81_IDIFA'})
oModel:GetModel('G81B_DETAIL'):SetUniqueLine({'G81_FILIAL', 'G81_IDIFA'})

oModel:GetModel('G6M_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G6O_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G6P_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G6N_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G82_DETAIL' ):SetOptional(.T.)
oModel:GetModel('G48A_DETAIL'):SetOptional(.T.)
oModel:GetModel('G48B_DETAIL'):SetOptional(.T.)
oModel:GetModel('G80B_DETAIL'):SetOptional(.T.)
oModel:GetModel('G80A_DETAIL'):SetOptional(.T.)
oModel:GetModel('G81B_DETAIL'):SetOptional(.T.)
oModel:GetModel('G81A_DETAIL'):SetOptional(.T.)

oModel:GetModel('G6M_DETAIL' ):SetNoDeleteLine(.T.)
oModel:GetModel('G6N_DETAIL' ):SetNoDeleteLine(.T.)
oModel:GetModel('G48A_DETAIL'):SetNoDeleteLine(.T.)
oModel:GetModel('G48B_DETAIL'):SetNoDeleteLine(.T.)
oModel:GetModel('G81A_DETAIL'):SetNoDeleteLine(.T.)
oModel:GetModel('G81B_DETAIL'):SetNoDeleteLine(.T.)

oModel:GetModel('G6O_DETAIL' ):SetNoInsertLine(.T.)
oModel:GetModel('G6M_DETAIL' ):SetNoInsertLine(.T.)
oModel:GetModel('G6N_DETAIL' ):SetNoInsertLine(.T.)
oModel:GetModel('G48A_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G48B_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G81A_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G81B_DETAIL'):SetNoInsertLine(.T.)

oModel:GetModel('G48A_DETAIL'):SetOnlyQuery(.F.)
oModel:GetModel('G48B_DETAIL'):SetOnlyQuery(.T.)

// Define o n�mero m�ximo de linhas que o model poder� receber, de acordo com a define GRIDMAXLIN.
oModel:GetModel('G48A_DETAIL'):SetMaxLine(GRIDMAXLIN)
oModel:GetModel('G48B_DETAIL'):SetMaxLine(GRIDMAXLIN)
oModel:GetModel('G6N_DETAIL' ):SetMaxLine(GRIDMAXLIN)
oModel:GetModel('G6P_DETAIL' ):SetMaxLine(GRIDMAXLIN)

oModel:SetActivate()
oModel:SetVldActivate( { |oModel| TA040VLDAC( oModel ) } )

Return oModel

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Fun��o respons�vel pela montagem do menu

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE STR0003 ACTION 'VIEWDEF.TURA040' OPERATION 2	ACCESS 0 // "Visualizar"
ADD OPTION aRotina TITLE STR0004 ACTION 'TA040Gerar'      OPERATION 3	ACCESS 0 // "Gerar"
ADD OPTION aRotina TITLE STR0005 ACTION 'TA040Alterar'    OPERATION 4	ACCESS 0 // "Alterar"
ADD OPTION aRotina TITLE STR0006 ACTION 'VIEWDEF.TURA040' OPERATION 5	ACCESS 0 // "Excluir"
ADD OPTION aRotina TITLE STR0007 ACTION 'TA040SelApu(1)'  OPERATION 3	ACCESS 0 // "Liberar"
ADD OPTION aRotina TITLE STR0102 ACTION 'TA040SelApu(2)'  OPERATION 3	ACCESS 0 // "Estornar" 
ADD OPTION aRotina TITLE STR0109 ACTION 'TA040RPT'        OPERATION 6	ACCESS 0 // "Demonstrativo"

// Ponto de entrada utilizado para inserir novas opcoes no array aRotina
If ExistBlock('TA040MENU')
	aRotina := ExecBlock('TA040MENU', .F., .F., {aRotina})
EndIf

Return aRotina

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Gerar
Fun��o respons�vel pela tela de par�metros e tipos de acordos para gera��o da apura��o.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040Gerar()

Local aTpAcordo := {}
Local aTpTpAcd  := {}
Local cPerg     := 'TUR040'
Local lContinue := .F.
Local nX        := 0

//+----------------------------------------------------
//|	Apresenta tela de par�metros e sele��o de Tipos de Acordo
//+----------------------------------------------------
If (lContinue := Pergunte(cPerg, .T., .F.))
	If Empty(MV_PAR10) .Or. Empty(MV_PAR09)
		Help( , , "TA040Gerar", , STR0134, 1, 0)		// "Os campos Dt. Inclus�o De ? e Dt. Inclus�o At� ? devem ser preenchidos."
		lContinue := .F.
	EndIf

	If lContinue .And. MV_PAR11 == 1
		lContinue := TA040TpAco(@aTpAcordo,)
	ElseIf lContinue
		G8B->(DbGoTop())
		While G8B->(!EOF())
			If (G8B->G8B_CODIGO <> 'V01' .Or. !(G8B->G8B_CODIGO $ 'C08|C09|C11')) .And. G8B->G8B_CLIFOR == '1' 
				aAdd(aTpAcordo, G8B->G8B_CODIGO)
				aAdd(aTpTpAcd,{G8B->G8B_CODIGO, G8B->G8B_TIPO})
			EndIf
			G8B->(DbSkip())
		EndDo
	EndIf
EndIf

If Len(aTpTpAcd) == 0
	G8B->(DbSetOrder(1))
	For nX := 1  To Len(aTpAcordo)
		If G8B->(DbSeek(xFilial("G8B") + aTpAcordo[nX] ))		
			aAdd(aTpTpAcd,{G8B->G8B_CODIGO, G8B->G8B_TIPO})
		EndIf
	Next
EndIf

If lContinue .And. Len(aTpAcordo) > 0 .And. Len(aTpTpAcd) > 0 									
	TA040Proc(@aTpAcordo,@aTpTpAcd)
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Proc
Fun��o respons�vel pela aplica��o e apura��o da receita de clientes.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Proc(aTpAcordo,aTpTpAcd)

Local nSegmento    := 0
Local nCompl       := 0
Local nCliente     := 0
Local aFecha       := {}
Local aSA1Clientes := {}
Local lApura       := .F.
Local lContinue    := .T.
Local lAcTran      := .F.
Local lApurou      := .F.
Local lServProp    := MV_PAR12 == 1 //	Considera Servi�o Pr�prio
Local cDiretorio   := GetMv('MV_DIRAPU', , 'C:\TOTVS\')
Local cApur        := ''
Local aParamSeg    := {MV_PAR06, MV_PAR07, MV_PAR08}  // Par�metros dos Segmentos 1=Corporativo / 2=Eventos / 3=Lazer
Local aApura       := {}       

cCliCod   := ''
cCliLoja  := ''
cCliMoeda := ''
cCliRazao := ''
cSegmento := ''
aApur     := Array(2)

aSA1Clientes := TA040SeSA1(MV_PAR09, MV_PAR10)	// Retorna Clientes para o intervalo informado no pergunte

If !(lContinue := (Len(aSA1Clientes) > 0) )
	Help( , , "TA040Proc", , STR0010, 1, 0) // "N�o foram encontrados Clientes para os par�metros informados."
EndIf

If lContinue

	//+---------------------------
	//|	Percorre CLIENTE / MOEDA
	//+---------------------------
	For nCliente := 1 To Len(aSA1Clientes)
		cCliCod   := aSA1Clientes[nCliente][AScan(aSA1Clientes[nCliente], {|x, y| x[1] == 'G3Q_CLIENT'})][2]
		cCliLoja  := aSA1Clientes[nCliente][AScan(aSA1Clientes[nCliente], {|x, y| x[1] == 'G3Q_LOJA'  })][2]
		cCliMoeda := aSA1Clientes[nCliente][AScan(aSA1Clientes[nCliente], {|x, y| x[1] == 'G3Q_MOEDCL'})][2]
		cCliRazao := Posicione('SA1', 1, xFilial('SA1') + cCliCod + cCliLoja, 'A1_NOME') 

		aFecha  := TURAX01(cCliCod, cCliLoja, MV_PAR05 == 1, '2', dDatabase, MV_PAR06 == 1, MV_PAR07 == 1, MV_PAR08 == 1)
		aApur   := Array(2)
		lApura  := .F.	// limpa vari�vel
		lAcTran := .F.

		If Empty(aFecha); Loop; EndIf

		aApur[ARR_DADOS]	:= TA040ArG6L()
		aApur[ARR_FILHOS]	:= {}

		//+---------------------------------
		//|	PERCORRE OS COMPLEMENTOS DO CLIENTE
		//+---------------------------------
		For nCompl := 1 To Len(aFecha)

			//+---------------------------------
			//|	PERCORRE OS SEGMENTOS DO CLIENTE
			//+---------------------------------
			For nSegmento := 1 To Len(aFecha[nCompl][4])
				cSegmento	:= SubStr(aFecha[nCompl][4], nSegmento, 1)	// Segmento

				//+-----------------------------
				//|	Validando se o Segmento ser� apurado
				//+-----------------------------
				If aParamSeg[Val(cSegmento)] == 1

					//+-----------------------------
					//|	Aplica Acordos
					//+-----------------------------
					TA040Aplica(aFecha[nCompl], aTpAcordo, @lAcTran)
	
					//+-----------------------------
					//|	Apura Acordos
					//+-----------------------------
					If !lApura
						Processa({|| TA040Apura(aFecha[nCompl], @lApura, aTpAcordo, lAcTran, aTpTpAcd)}, STR0008, STR0009 + " " + cCliRazao)  // "Aguarde..."###"Gerando apura��o..."
					Else
						Processa({|| TA040Apura(aFecha[nCompl], , aTpAcordo, lAcTran, aTpTpAcd)}, STR0008, STR0009 + " " + cCliRazao)
					EndIf
	
					//+-----------------------------
					//|	Vendas de Servi�o Pr�prio
					//+-----------------------------
					If lServProp
						If !lApura
							Processa({|| TA040SerPr(aFecha[nCompl], @lApura)}, STR0008, STR0157)	// "Aguarde"	// "Gerando apura��o de Servi�os Pr�prios..."
						Else
							Processa({|| TA040SerPr(aFecha[nCompl], )}, STR0008, STR0157)	// "Aguarde"	// "Gerando apura��o de Servi�os Pr�prios..."
						EndIf
					EndIf
				EndIf
			Next nSegmento
		Next nCompl

		If lApura
			Processa({|| lApura := TA040ExApu(@cApur)}, STR0008, STR0158)	// "Aguarde"    // "Finalizando a grava��o da apura��o gerada..."
		EndIf

		If lApura .And. MV_PAR15 == 1 // Envia Relat�rio Autom�tico
			lApurou := .T.
			aAdd(aApura, {cApur, dDataBase, cCliCod, cCliLoja, .T.})
		ElseIf lApura
			lApurou := .T.
		EndIf
	Next nCliente

	If !lApurou
		Help( , , "TA040PROC", , STR0108, 1, 0)	// "N�o h� receita a ser apurada para os par�metros informados." 
	ElseIf Len(aApura) > 0 .And. MV_PAR15 == 1	//  Relat�rio Autom�tico
		Processa({|| TURR001(aApura, MV_PAR17 == 1, MV_PAR19, MV_PAR18, MV_PAR06 == 1, MV_PAR07 == 1, MV_PAR08 == 1, cDiretorio)}, STR0008, STR0105)		// "Aguarde"    "Gerando relat�rio..."
	EndIf 
EndIf

//+-----------------------------
// Limpa vari�veis static
//+-----------------------------
aApur     := {}
cCliCod   := ''
cCliLoja  := ''
cCliMoeda := ''
cSegmento := ''

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SerPr
Fun��o respons�vel pela apura��o de servi�o pr�rpio

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SerPr(aFechaSeg, lApura)

Local lLast      := .F.
Local aDadosApur := {}
Local aProdutos  := {}
Local nLen       := 0
Local nProduto   := 0
Local cProd      := ''
Local aAux       := {}

aProdutos := Ta040SeG48('V01', '% G48_CODPRD %') // Servi�o Proprio

For nProduto := 1 to Len(aProdutos)
	If cProd <> aProdutos[nProduto][AScan(aProdutos[nProduto],{|x, y| x[1] == 'G48_CODPRD'})][2]
		aDadosApur             := Array(2)
		aDadosApur[ARR_DADOS]  := TA040ArG82('V01', aProdutos[nProduto])	// Servi�o Proprio
		aDadosApur[ARR_FILHOS] := {}
	EndIf

	cProd := aProdutos[nProduto][AScan(aProdutos[nProduto],{|x, y| x[1] == 'G48_CODPRD'})][2]
	aAux  := TA040ArG48(aProdutos[nProduto])	// Servi�o Proprio

	aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })

	lLast := nProduto == Len(aProdutos)

	If lLast .Or. (cProd <> aProdutos[nProduto + 1][AScan(aProdutos[nProduto + 1],{|x, y| x[1] == 'G48_CODPRD'})][2])
		aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
		nLen := Len( aApur[ARR_FILHOS] )
		aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
		lApura := .T.
		lLast  := .F.
	EndIf
Next nProduto

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ExG48
Fun��o respons�vel pela verifica��o do acordo j� aplicado.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ExG48(cNumId, cIdItem, cNumSeq, cClass)

Local lRet	:= .F.

G48->(DbGoTop())
G48->(DbSetOrder(1))	// G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD

If G48->(DbSeek( xFilial('G48') + cNumId + cIdItem + cNumSeq))
	While G48->(!EOF()) .And. G48->(G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ) == xFilial('G48') + cNumId + cIdItem + cNumSeq
		If (lRet := G48->G48_CLASS $ cClass)
			Exit
		EndIf
		G48->(DbSkip())
	EndDo
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Aplica
Fun��o respons�vel pela aplica��o dos acordos.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Aplica(aFechaSeg, aTpAcordo, lAcTran)
	
Local lCombo       := (MV_PAR11 <> 1) .Or. AScan(aTpAcordo, 'C05') > 0
Local lTransaction := (MV_PAR11 <> 1) .Or. AScan(aTpAcordo, 'C01') > 0 .Or. lCombo

//+------------------------------------------------------------
//|	Aplica acordos do tipo C05 - Combo
//+------------------------------------------------------------
If lCombo
	lAcTran := TA040AplCo(aFechaSeg)
EndIf

//+------------------------------------------------------------
//|	Aplica acordos do tipo C01 - Transaction
//+------------------------------------------------------------
If lTransaction .And. lAcTran
	TA040AplTr(aFechaSeg)
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Apura
Fun��o respons�vel pela apura��o de acordos.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Apura(aFechaSeg, lApura, aTpAcordo, lAcTran, aTpTpAcd)

Local lFlat        :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C02') > 0
Local lManagement  :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C03') > 0
Local lSuccess     :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C04') > 0
Local lCombo       := ((MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C05') > 0) .And. lAcTran
Local lTaxaAgen    :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C06') > 0
Local lTaxaC12     :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C12') > 0
Local lRepasse     :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C07') > 0
Local lTaxaReem    :=  (MV_PAR11 <> 1) .Or. AScan(aTpAcordo,'C10') > 0
Local lTransaction := AScan(aTpAcordo, 'C01') > 0 .Or. lCombo
Local aDadosApur   := Array(2)
Default lApura     := .F.

ProcRegua(9)

//+------------------------------------------------------------
//|	Apura acordos do tipo C02 - Flat
//|		Tipo:	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lFlat
	TA040Flat(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C04 - Success
//|		Tipo:	Valor Fixo
//+------------------------------------------------------------
If lSuccess
	TA040Succe(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C03 - Management
//|		Tipo:	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lManagement
	TA040Manag(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C01 - Transaction
//|		Tipo: 	Porcentagem
//+------------------------------------------------------------
If lTransaction
	TA040Trans(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C05 - Combo
//|		Tipo: 	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lAcTran .And. lCombo
	TA040Combo(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C07 - Repasse
//|		Tipo:	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lRepasse
	TA040Repas(aFechaSeg, @aApur, @lApura, aTpTpAcd)
EndIf
IncProc()

//+------------------------------------------------------------
//|	Apura acordos do tipo C06 - Taxa Agencia
//|	Apura acordos do tipo C10 - Taxa Reembolso
//|		Tipo:	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lTaxaAgen
	TA040TaxaA(aFechaSeg, @aApur, @lApura, aTpTpAcd, 'C06')
EndIf
IncProc()

//+------------------------------------------------------------
//|	//|	Apura acordos do tipo C12 -  Cr�dito N�o Tribut�vel
//|		Tipo:	Valor Fixo
//|				Porcentagem
//+------------------------------------------------------------
If lTaxaC12
	TA040TaxaA(aFechaSeg, @aApur, @lApura, aTpTpAcd, 'C12')
EndIf
IncProc()
	
If lTaxaReem
	TA040TaxaA(aFechaSeg, @aApur, @lApura, aTpTpAcd, 'C10')
EndIf
IncProc()

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040AplTr
Fun��o respons�vel pela aplica��o do transaction.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040AplTr(aFechaSeg)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local aAcordos  := {}
Local aAcoItem  := {}
Local aIV       := {}
Local nAcordo   := 0
Local nAcoItem  := 0
Local nIV       := 0
Local lAplica   := .F.

aAcordos := Ta040SeG5V(aFechaSeg[1], 'C01', cSegmento) // Transaction
aIV      := TA040SeG3Q(cCliMoeda, MV_PAR09, MV_PAR10, cCliCod, cCliLoja, cSegmento) // Transaction

For nAcordo := 1 To Len(aAcordos)

	For nIV := 1 To Len(aIV)
		aAcoItem := TA040SeG5X(aAcordos[nAcordo])

		//+------------------------------------------------------
		//|	Verifica se IV j� possui acordo C01 aplicado
		//+------------------------------------------------------
		lAplica := !TA040ExG48(aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_NUMID'})][2], ;
		                       aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_IDITEM'})][2], ; 
		                       aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2], ;
		                       'C01|C05')

		//+------------------------------------------------------
		//|	Valida regra de aplica��o do IV (rotina em TURA034.PRW)
		//+------------------------------------------------------
		If lAplica
			lAplica := Tura34RegApl(aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G5V_CODACO'})][2]/*cAcordo*/, ;
			                        aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G5V_CODREV'})][2]/*cRevisao*/, ;
			                        '1'/*cTipoAcordo*/, ;	// 1-Cliente
			                        aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_NUMID'})][2]/*cRegVenda*/, ;
			                        aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_IDITEM'})][2]/*cItemVenda*/, ;
			                        aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]/*cNumSeq*/, ;
			                        /*[oModel]*/)
		EndIf

		//+------------------------------------------------------
		//|	Valida IV com cadastro dos Itens do Acordo
		//+------------------------------------------------------
		If lAplica
			For nAcoItem := 1 To Len(aAcoItem)
				If TA040TraVl(aAcoItem[nAcoItem], aIV[nIV])
					G3P->(DbGoTo(aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3P_RECNO'})][2]))
					G3Q->(DbGoTo(aIV[nIV][AScan(aIV[nIV],{|x, y| x[1] == 'G3Q_RECNO'})][2]))

					//+------------------------------------------------------
					//|	Rotina que instancia o modelo do RV para inclus�o do acordo do cliente na G48
					//+------------------------------------------------------
					TA040G48TI(G3Q->G3Q_NUMID, G3Q->G3Q_IDITEM, G3Q->G3Q_NUMSEQ, G3Q->G3Q_EMISS, aAcordos[nAcordo], aAcoItem[nAcoItem])
					Exit // Aplica somente um item do acordo
				EndIf
			Next
		EndIf
	Next nIV

Next nAcordo

RestArea(aArea)

FwRestRows(aSaveRows)
Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G48TI
Fun��o respons�vel pela cria��o do transaction atrav�s do registro de venda (TURA034).

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G48TI(cNumId, cIdItem, cNumSeq, cDtEmiss, aAcordo, aAcorItem)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local aDados    := {}
Local aAux      := {}
Local nX, nY    := 0
Local nQtde     := 0
Local nQtdeAco  := 0
Local nPos      := 0
Local oModel034 := FwLoadModel("TURA034")
Local oModelG3P := oModel034:GetModel('G3P_FIELDS')
Local oModelG3Q := oModel034:GetModel('G3Q_ITENS')
Local oModelG48 := oModel034:GetModel('G48A_ITENS')
Local oStruG3Q  := oModelG3Q:GetStruct()
Local aStruG3Q  := oStruG3Q:GetFields()
Local oStruG48  := oModelG48:GetStruct()
Local aStruG48  := oStruG48:GetFields()
Local oStruG4C  := oModel034:GetModel('G4CA_ITENS'):GetStruct()	
Local lRet      := .T.
Local nItem     := 0
Local nAplica   := 0
Local nValor    := 0
Local lFeeCanc  := .F.
Local cCodAcd   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRec   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]
Local cTpVal    := aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_TPVAL'})][2]
Local cBaseCl   := aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_BASCAL'})][2]
Local nPerAcd   := IIF(cTpVal == '1', aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_VALOR'})][2], 0)
Local nVlBase   := 0

TA034StruM( , oStruG3Q, , , , , , , , , , , , , , , , , , , , oStruG48, , , , oStruG4C)

oModel034:SetOperation(MODEL_OPERATION_UPDATE)
oModel034:Activate()

//+------------------------------------------------------
//|	Posiciona no IV correspondente para inclus�o do Acordo do Cliente
//+------------------------------------------------------
For nItem := 1 To oModelG3Q:Length()
	oModelG3Q:GoLine(nItem)

	If Alltrim(cIdItem) + Alltrim(cNumSeq) == Alltrim(oModelG3Q:GetValue('G3Q_IDITEM')) + Alltrim(oModelG3Q:GetValue('G3Q_NUMSEQ'))
		
		nQtdeAco := 0
		nAplica  := 1
		If !oModelG48:IsEmpty()
			nQtdeAco := oModelG48:Length()
			nAplica  := Val(oModelG48:GetValue("G48_APLICA", oModelG48:Length()))
		EndIf
		oModelG48:GoLine(nAplica)

		If !oModelG48:IsInserted()
			If !oModelG48:CanInsertLine()
				oModelG48:SetNoInsertLine(.F.)
			EndIf
			oModelG48:AddLine()
			nAplica++
		EndIf

		aDados  := {}
		nVlBase := T34ValBase('1', nPerAcd, cBaseCl, .T.)[1]
		
		If oModelG3Q:GetValue('G3Q_OPERAC') $ '1|3|4'										// 1=Emiss�o;3=Reemiss�o;4=Venda Servi�os Pr�prios
			If cTpVal == '1'		// 1-Porcentagem
				nValor := nVlBase * (aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_VALOR'})][2] / 100)
			Else
				nValor := aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_VALOR'})][2]
			EndIf	
		ElseIf oModelG3Q:GetValue('G3Q_OPERAC') == '2'  									// 2=Reembolso;
			If cTpVal == '1'		// 1-Porcentagem
				nValor := nVlBase * (aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_REEMBO'})][2] / 100)
			Else
				nValor := aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_REEMBO'})][2]
			EndIf
		EndIf 		
		
		aAdd(aDados, {'G48_OK'    , .T.})
		aAdd(aDados, {'G48_APLICA', StrZero(nAplica, 3)})
		aAdd(aDados, {'G48_DTINCL', cDtEmiss})
		aAdd(aDados, {'G48_CLIFOR', '1'})
		aAdd(aDados, {'G48_CLASS' , 'C01'})
		aAdd(aDados, {'G48_DESCR' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_DESCRI'})][2]})
		aAdd(aDados, {'G48_NIVEL' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_NIVEL'})][2]})
		aAdd(aDados, {'G48_TPVLR' , cTpVal})
		aAdd(aDados, {'G48_CODPRD', oModelG3Q:GetValue('G3Q_PROD')})
		aAdd(aDados, {'G48_ACDBAS', aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_ACOORI'})][2]})
		aAdd(aDados, {'G48_BASECL', cBaseCl})
		aAdd(aDados, {'G48_VLBASE', nVlBase})
		aAdd(aDados, {'G48_PERACD', nPerAcd})
		aAdd(aDados, {'G48_INTERV', '3'})	//3=Inclu�do Automaticamente
		aAdd(aDados, {'G48_APUGER', '1'})	//1=Sim
		aAdd(aDados, {'G48_CODACD', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODACO'})][2]})
		aAdd(aDados, {'G48_CODREC', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODREV'})][2]})
		aAdd(aDados, {'G48_VLACD' , nValor})
		aAdd(aDados, {'G48_STATUS', '1'})
		aAdd(aDados, {'G48_OPERA' , '1'})	// inclus�o
		aAdd(aDados, {'G48_TPACD' , '1'})	// receita
		aAdd(aDados, {'G48_SEGNEG', cSegmento})
		aAdd(aDados, {'G48_COMSER', '2'})
		aAdd(aAux, aClone(aDados))

		//+------------------------------------------------------
		//|	Aplicando Acordos de Transaction caso tenha altera��es no IV
		//+------------------------------------------------------
		If oModelG3Q:GetValue('G3Q_QTDALT') >= 1
			For nQtde := 1 To oModelG3Q:GetValue('G3Q_QTDALT')
				If !oModelG48:CanInsertLine()
					oModelG48:SetNoInsertLine(.F.)
				EndIf
				oModelG48:AddLine()
				nAplica++
				aAdd(aDados, {'G48_OK'    , .T.})
				aAdd(aDados, {'G48_APLICA', StrZero(nAplica, 3)})
				aAdd(aDados, {'G48_DTINCL', cDtEmiss})
				aAdd(aDados, {'G48_CLIFOR', '1'})
				aAdd(aDados, {'G48_CLASS' , 'C01'})
				aAdd(aDados, {'G48_DESCR' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_DESCRI'})][2]})
				aAdd(aDados, {'G48_NIVEL' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_NIVEL'})][2]})
				aAdd(aDados, {'G48_TPVLR' , cTpVal})
				aAdd(aDados, {'G48_CODPRD', oModelG3Q:GetValue('G3Q_PROD')})
				aAdd(aDados, {'G48_ACDBAS', aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_ACOORI'})][2]})
				aAdd(aDados, {'G48_BASECL', cBaseCl})
				aAdd(aDados, {'G48_VLBASE', nVlBase})
				aAdd(aDados, {'G48_PERACD', nPerAcd})
				aAdd(aDados, {'G48_INTERV', '3'})	//3=Inclu�do Automaticamente
				aAdd(aDados, {'G48_APUGER', '1'})	//1=Sim
				aAdd(aDados, {'G48_CODACD', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODACO'})][2]})
				aAdd(aDados, {'G48_CODREC', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODREV'})][2]})
				aAdd(aDados, {'G48_VLACD' , aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_REMARC'})][2]})
				aAdd(aDados, {'G48_STATUS', '1'})
				aAdd(aDados, {'G48_OPERA' , '2'})	// altera��o
				aAdd(aDados, {'G48_TPACD' , '1'})	// receita
				aAdd(aDados, {'G48_SEGNEG', cSegmento})
				aAdd(aDados, {'G48_COMSER', '2'})
				aAdd(aAux, aClone(aDados))
			Next nQtde
		EndIf
		
		//+------------------------------------------------------
		//|	Validando se o IV foi cancelado 
		//|	Caso afirmativo, � preciso verificar algumas informa��es da tabela G5U
		//+------------------------------------------------------
		If lRet .And. oModelG3Q:GetValue('G3Q_STATUS') == '3' 	// 3=Cancelado
			dbSelectArea("G5U")
			G5U->(dbSetOrder(1))
			If G5U->(dbSeek(xFilial("G5U") + oModelG3Q:GetValue('G3Q_MOTCAN')))
				If lFeeCanc := G5U->G5U_ACOFEE == "1"
					If !oModelG48:CanInsertLine()
						oModelG48:SetNoInsertLine(.F.)
					EndIf
					oModelG48:AddLine()
					nAplica++
					aAdd(aDados, {'G48_OK'    , .T.})
					aAdd(aDados, {'G48_APLICA', StrZero(nAplica, 3)})
					aAdd(aDados, {'G48_DTINCL', cDtEmiss})
					aAdd(aDados, {'G48_CLIFOR', '1'})
					aAdd(aDados, {'G48_CLASS' , 'C01'})
					aAdd(aDados, {'G48_DESCR' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_DESCRI'})][2]})
					aAdd(aDados, {'G48_NIVEL' , aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_NIVEL'})][2]})
					aAdd(aDados, {'G48_TPVLR' , cTpVal})
					aAdd(aDados, {'G48_CODPRD', oModelG3Q:GetValue('G3Q_PROD')})
					aAdd(aDados, {'G48_ACDBAS', aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_ACOORI'})][2]})
					aAdd(aDados, {'G48_BASECL', cBaseCl})
					aAdd(aDados, {'G48_VLBASE', nVlBase})
					aAdd(aDados, {'G48_PERACD', nPerAcd})
					aAdd(aDados, {'G48_INTERV', '3'})	//3=Inclu�do Automaticamente
					aAdd(aDados, {'G48_APUGER', '1'})	//1=Sim
					aAdd(aDados, {'G48_CODACD', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODACO'})][2]})
					aAdd(aDados, {'G48_CODREC', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODREV'})][2]})
					aAdd(aDados, {'G48_VLACD' , aAcorItem[AScan(aAcorItem, {|x, y| x[1] == 'G5X_CANCEL'})][2]})
					aAdd(aDados, {'G48_STATUS', '1'})
					aAdd(aDados, {'G48_OPERA' , '3'})	// cancelamento
					aAdd(aDados, {'G48_TPACD' , '1'})	// receita
					aAdd(aDados, {'G48_SEGNEG', cSegmento})
					aAdd(aDados, {'G48_COMSER', '2'})
					aAdd(aAux, aClone(aDados))
				EndIf 

				If G5U->G5U_CANFEE == "1" 
					For nX := 1 To Len(aAux)
						If aAux[nX][AScan(aDados, {|x, y| x[1] == 'G48_OPERA'})][2] $ '1|2'
							aAux[nX][AScan(aDados, {|x, y| x[1] == 'G48_STATUS'})][2] := '3'
						EndIf
					Next nX
				EndIf 
			EndIf
			G5U->(DbCloseArea())
		EndIf

		For nY := 1 To Len(aAux)
			oModelG48:GoLine(nQtdeAco + nY)
			For nX := 1 To Len(aAux[nY])
				If (nPos := aScan(aStruG48, {|x| AllTrim(x[3])== AllTrim(aAux[nY][nX][1])})) > 0
					If !(lRet := IIF(AllTrim(aAux[nY][nX][1]) != 'G48_OK', ;
									   oModel034:SetValue( 'G48A_ITENS', aStruG48[nPos][3], aAux[nY][nX][2]), ;
									   oModel034:LoadValue('G48A_ITENS', aStruG48[nPos][3], aAux[nY][nX][2])))
						Exit
					EndIf
				EndIf
			Next nX
		Next nY
		Exit
	EndIf
Next nItem

T34CalcAco("", "", "1", aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_NIVEL'})][2], .T.)
T34AtuDmFi(oModel034)
Tur34ItFin(oModel034, "", .T., "1")

If (lRet := oModel034:VldData())
	oModel034:CommitData()
Else
	TA040GetEr(oModel034)
EndIf

oModel034:DeActivate()
oModel034:Destroy()

RestArea(aArea)
FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040TraVl
Fun��o respons�vel pela aplica��o do transaction.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040TraVl(aAcorItem, aIV)

Local lRet      := .T.
Local nIV       := 0
Local nRegra    := 0
Local aRegras   := {}
Local xTodos    := Nil
Local xValorG5X := Nil
Local xValorG3Q := Nil

aAdd(aRegras, {{'ACORDO', 'G5X_GRPROD'}, {'IV', 'G3Q_GRPPRD'}, {'TODOS', ''  }})
aAdd(aRegras, {{'ACORDO', 'G5X_DESTIN'}, {'IV', 'G3Q_DESTIN'}, {'TODOS', '3' }})
aAdd(aRegras, {{'ACORDO', 'G5X_ASSIST'}, {'IV', 'G3Q_TPEMIS'}, {'TODOS', '99'}})

For nRegra := 1 To Len(aRegras)
	xTodos		:= aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'TODOS'})][2]
	xValorG5X	:= aAcorItem[AScan(aAcorItem,{|x, y| x[1] == aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'ACORDO'})][2]})][2]
	xValorG3Q	:= aIV[AScan(aIV, {|x, y| x[1] == aRegras[nRegra][AScan(aRegras[nRegra], {|x, y| x[1] == 'IV'})][2]})][2]

	//+------------------------------------------------------
	//| Se valor igual a TODOS, valida e passa para pr�ximo item do acordo
	//+------------------------------------------------------
	If (lRet := (!Empty(xTodos) .And. xTodos == xValorG5X) )
		Loop
	Else
		If !(lRet := xValorG5X == xValorG3Q)
			Exit
		EndIf
	EndIf
Next nRegra

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040AplCo
Fun��o respons�vel pela aplica��o do combo.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040AplCo(aFechaSeg)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local aAcordos  := {}
Local aAcoItem  := {}
Local nAcordo   := 0
Local nItem     := 0
Local nQuant    := 0
Local nAcoItem  := 0
Local lOk       := (MV_PAR16 == 1)	// Gera Apura��o Sem Valor?
Local lCombo    := .T.
Local lValPass  := .F.
Local cBaseApu  := ''
Local aRecno    := {}
Local aAcoItem1 := {}
Local nRecno    := 0
Local nAcoItem1 := 0
Local lAcordo   := .F.
Local cCampo    := ''
Local cWhere    := ''
Local lPrinci   := .F.
Local cChave    := ''

//+-------------------------------------------------------
//| Seleciona IV
//+-------------------------------------------------------
aAcordos := Ta040SeG5V(aFechaSeg[1], 'C05', cSegmento) // Combo

For nAcordo := 1 To Len(aAcordos)
	lAcordo  := .T.
	cBaseApu := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G5V_BASAPU'})][2]
	cCampo   := IIF(cBaseApu == '4', 'G3Q_ORDER', 'G3Q_NUMID')
	lValPass := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G5V_PASSAG'})][2] == '1'
	aAcoItem := TA040SeG61(aAcordos[nAcordo])  // G61 - Itens do Combo

	//+-------------------------------------------------------
	// Procura itens do Combo por RV/OS
	//+-------------------------------------------------------
	aRecno := {}

	//+-------------------------------------------------------
	// Valida Itens do Acordo
	//+-------------------------------------------------------
	If Len(aAcoItem) > 0
		 lAcoItem := TA040ComVl(aAcoItem[1], '1', @aAcoItem1)
	EndIf

	For nAcoItem1 := 1 To Len(aAcoItem1)
		aRecno	:= {}
		cWhere := cCampo + " = '" + aAcoItem1[nAcoItem1][AScan(aAcoItem1[nAcoItem1],{|x, y| x[1] == cCampo})][2] + "' "

		For nAcoItem := 1 To Len(aAcoItem)
			lAcoItem := TA040ComVl(aAcoItem[nAcoItem], '2', {}, @aRecno, cWhere)
			If !lAcoItem; Exit; EndIf // Se n�o encontrou algum item do acordo, passa p/ pr�ximo RV
		Next nAcoItem

		If lAcoItem

			If lValPass
				lAcoItem := TA040VlPas(aRecno)
			EndIf

			//+-------------------------------------------------------
			//|	Cria Acordo Aplicado
			//+-------------------------------------------------------
			If lAcoItem
				lOk		 := .T.
				lPrinci := .F.

				//+-------------------------------------------------------
				// Gera Chave do Combo
				//+-------------------------------------------------------
				cChave := TA040CChav(aRecno, cBaseApu)

				For nRecno := 1 To Len(aRecno)
					If lPrinci
						aRecno[nRecno][AScan(aRecno[nRecno],{|x, y| x[1] == 'G61_PRINCI'})][2] :=  '2'
					Else
						lPrinci := aRecno[nRecno][AScan(aRecno[nRecno],{|x, y| x[1] == 'G61_PRINCI'})][2] == '1'
					EndIf

					G3P->(DbGoTo(aRecno[nRecno][AScan(aRecno[nRecno],{|x, y| x[1] == 'G3P_RECNO'})][2]))
					G3Q->(DbGoTo(aRecno[nRecno][AScan(aRecno[nRecno],{|x, y| x[1] == 'G3Q_RECNO'})][2]))

					//+------------------------------------------------------
					//|	Rotina que instancia o modelo do RV para inclus�o do acordo do cliente na G48
					//+------------------------------------------------------
					TA040G48CI(G3Q->G3Q_IDITEM, G3Q->G3Q_IDITEM, G3Q->G3Q_NUMSEQ, G3Q->G3Q_EMISS, aAcordos[nAcordo], aRecno[nRecno][AScan(aRecno[nRecno],{|x, y| x[1] == 'G61_PRINCI'})][2] == '1', cChave)
				Next nItem
			EndIf
		EndIf
	Next nAcoItem1
Next nAcordo

RestArea(aArea)
FwRestRows(aSaveRows)

Return lAcordo

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040CChav
Fun��o respons�vel pelo retorno da chave do combo.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040CChav(aRecno, cBaseApu)

Local nX			:= 0
Local cChave		:= ''
Default cBaseApu	:= ''

For nX := 1 To Len(aRecno)
	If aRecno[nX][AScan(aRecno[nX],{|x, y| x[1] == 'G61_PRINCI'})][2] == '1'
		If cBaseApu == '4'
			cChave	:= aRecno[nX][AScan(aRecno[nX],{|x, y| x[1] == 'G3Q_ORDER'})][2]
		Else
			cChave	:= aRecno[nX][AScan(aRecno[nX],{|x, y| x[1] == 'G3Q_NUMID' })][2] + ;
			          aRecno[nX][AScan(aRecno[nX],{|x, y| x[1] == 'G3Q_IDITEM'})][2] + ;
			          aRecno[nX][AScan(aRecno[nX],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]
		EndIf

		Exit
	EndIf
Next

Return cChave

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G48Up
Fun��o respons�vel pela altera��o dos acordos aplicados atrav�s do registro de venda (TURA034).

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G48Up(aDetalhes, cCodApu)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()

G48->(DbGoTo(aDetalhes[AScan(aDetalhes,{|x| x[1] == 'G48_RECNO'})][2]))

If G48->(!EOF())
	RecLock('G48', .F.)
	G48->G48_OK     := .T.
	G48->G48_FILAPU := xFilial("G6L")
	G48->G48_CODAPU := cCodApu
	G48->(MsUnLock())

	G4C->( dbSetOrder(1) )
	If G4C->( dbSeek( G48->( G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD ) ) )
		While G4C->(!Eof()) .And. G4C->(G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_APLICA+G4C_NUMACD) == G48->(G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD)
			If Empty(G4C->G4C_CONINU) .And. G4C->G4C_CLIFOR == '1'
				RecLock('G4C', .F.)
				G4C->G4C_CODAPU := cCodApu
				G4C->( MsUnlock() )
			EndIf
			G4C->(dbSkip())
		EndDo
	EndIf

EndIf

RestArea(aArea)
FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G48CI
Fun��o respons�vel pela cria��o do combo atrav�s do registro de venda (TURA034).

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G48CI(cNumId, cIdItem, cNumSeq, cDtEmiss, aAcordo, lPrincipal, cChave)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local aDados    := {}
Local nX        := 0
Local nPos      := 0
Local oModel034 := FwLoadModel("TURA034")
Local oModelG3P := oModel034:GetModel('G3P_FIELDS')
Local oModelG3Q := oModel034:GetModel('G3Q_ITENS')
Local oModelG48 := oModel034:GetModel('G48A_ITENS')
Local oStruG3Q  := oModelG3Q:GetStruct()
Local aStruG3Q  := oStruG3Q:GetFields()
Local oStruG48  := oModelG48:GetStruct()
Local aStruG48  := oStruG48:GetFields()
Local oStruG4C  := oModel034:GetModel('G4CA_ITENS'):GetStruct()
Local lRet      := .T.
Local nItem     := 0
Local nAplica   := 0
Local cBaseApu  := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_BASAPU'})][2]
Local cCodAcd   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRec   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]
Local nValor    := IIF(lPrincipal, aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_VALOR'})][2], 0)
Local cTpCombo  := IIF(cBaseApu == '4', '2', '1')
Local cTpVal    := aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_TPVAL' })][2]
Local cBaseCl   := aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_BASCAL'})][2]
Local nPerAcd   := IIF(cTpVal == '1', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_VALOR'})][2], 0)
Local nVlBase   := 0

TA034StruM( , oStruG3Q, , , , , , , , , , , , , , , , , , , , oStruG48, , , , oStruG4C)

oModel034:SetOperation(MODEL_OPERATION_UPDATE)
oModel034:Activate()

//+------------------------------------------------------
//|	Posiciona no IV correspondente para inclus�o do Acordo do Cliente
//+------------------------------------------------------
For nItem := 1 To oModelG3Q:Length()
	oModelG3Q:GoLine(nItem)

	If Alltrim(cIdItem) + Alltrim(cNumSeq) == Alltrim(oModelG3Q:GetValue('G3Q_IDITEM')) + Alltrim(oModelG3Q:GetValue('G3Q_NUMSEQ'))
		
		nAplica := 1
		If !oModelG48:IsEmpty()
			nAplica := Val(oModelG48:GetValue('G48_APLICA', oModelG48:Length()))
		EndIf
		oModelG48:GoLine(nAplica)

		If !oModelG48:IsInserted()
			If !oModelG48:CanInsertLine()
				oModelG48:SetNoInsertLine(.F.)
			EndIf
			oModelG48:AddLine()
			nAplica++
		EndIf
		
		nVlBase := T34ValBase('1', nPerAcd, cBaseCl, .T.)[1]
		
		If cTpVal == '1'		// 1-Porcentagem
			nValor := nVlBase * (aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_VALOR'})][2] / 100)
		EndIf 
		
		aDados := {}
		aAdd(aDados, {'G48_OK'    , .T.})
		aAdd(aDados, {'G48_APLICA', StrZero(nAplica, 3)})
		aAdd(aDados, {'G48_DTINCL', cDtEmiss})
		aAdd(aDados, {'G48_CLIFOR', '1'})
		aAdd(aDados, {'G48_CLASS' , 'C05'})
		aAdd(aDados, {'G48_DESCR' , aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_DESCRI'})][2]})
		aAdd(aDados, {'G48_NIVEL' , aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_NIVEL'})][2]})
		aAdd(aDados, {'G48_TPVLR' , cTpVal})
		aAdd(aDados, {'G48_CODPRD', oModelG3Q:GetValue('G3Q_PROD')})
		aAdd(aDados, {'G48_ACDBAS', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_ACOORI'})][2]})
		aAdd(aDados, {'G48_BASECL', cBaseCl})
		aAdd(aDados, {'G48_VLBASE', nVlBase})
		aAdd(aDados, {'G48_PERACD', nPerAcd})
		aAdd(aDados, {'G48_INTERV', '3'})	//3=Inclu�do Automaticamente
		aAdd(aDados, {'G48_APUGER', '1'})	// 1=Sim
		aAdd(aDados, {'G48_CODACD', cCodAcd})
		aAdd(aDados, {'G48_CODREC', cCodRec})
		aAdd(aDados, {'G48_VLACD' , nValor})
		aAdd(aDados, {'G48_STATUS', '1'})	// 1=Em Aberto
		aAdd(aDados, {'G48_OPERA' , '1'})	// 1=inclus�o
		aAdd(aDados, {'G48_TPACD' , '1'})	// 1=receita
		aAdd(aDados, {'G48_SEGNEG', cSegmento})
		aAdd(aDados, {'G48_CHAVEC', cChave})
		aAdd(aDados, {'G48_TPCOMB', cTpCombo})
		aAdd(aDados, {'G48_PRICMB', IIF(lPrincipal, '1', '2')})
		aAdd(aDados, {'G48_COMSER', '2'})

		For nX := 1 To Len( aDados )
			//+------------------------------------------------------
			//|	Verifica se os campos passados existem na estrutura do modelo
			//+------------------------------------------------------
			If (nPos := aScan(aStruG48, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
				If !(lRet := IIF(AllTrim(aDados[nX][1]) != 'G48_OK', ;
								   oModel034:SetValue( 'G48A_ITENS', aStruG48[nPos][3], aDados[nX][2]), ;
								   oModel034:LoadValue('G48A_ITENS', aStruG48[nPos][3], aDados[nX][2])))
					Exit
				EndIf
			EndIf
		Next nX

		Exit
	EndIf
Next nItem

T34CalcAco("", "", "1", aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_NIVEL'})][2], .T.)
T34AtuDmFi(oModel034)
Tur34ItFin(oModel034, "", .T., "1")

If (lRet := oModel034:VldData())
	oModel034:CommitData()
Else
	TA040GetEr(oModel034)
EndIf

oModel034:DeActivate()
oModel034:Destroy()

RestArea(aArea)
FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040VlPas
Fun��o respons�vel pela valida��o do passageiro.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040VlPas(aRecno)

Local lRet		:= .F.
Local cNumId	:= ''
Local cIdItem	:= ''
Local nItem1	:= 0
Local nItem2	:= 0
Local nPass1	:= 0
Local aPass1	:= {}
Local nPass2	:= 0
Local aPass2	:= {}

For nItem1 := 1 To Len(aRecno)

	cNumId  := aRecno[nItem1][AScan(aRecno[nItem1],{|x, y| x[1] == 'G3Q_NUMID'})][2]
	cIdItem := aRecno[nItem1][AScan(aRecno[nItem1],{|x, y| x[1] == 'G3Q_IDITEM'})][2]
	aPass1  := TA040SeG3S(cNumId,cIdItem)

	For nItem2 := 1 To Len(aRecno)
		If nItem1 == nItem2; Loop; EndIf 	// n�o compara passageiros do mesmo IV

		cNumId  := aRecno[nItem2][AScan(aRecno[nItem2],{|x, y| x[1] == 'G3Q_NUMID'})][2]
		cIdItem := aRecno[nItem2][AScan(aRecno[nItem2],{|x, y| x[1] == 'G3Q_IDITEM'})][2]
		aPass2  := TA040SeG3S(cNumId,cIdItem)

		For nPass1 := 1 To Len(aPass1)
			For nPass2 := 1 To Len(aPass2)
				If aPass1[nPass1][AScan(aPass1[nPass1],{|x, y| x[1] == 'G3S_CONTAT'})][2] == aPass2[nPass2][AScan(aPass2[nPass2],{|x, y| x[1] == 'G3S_CONTAT'})][2]
					lRet := .T.
					Exit
				EndIf
			Next nPass2
			
			If lRet; Exit; EndIf // Se encontrou um passageiro em comum, retorna .T.
		Next nPass
		
		If lRet; Exit; EndIf // Se encontrou um passageiro em comum, retorna .T.
	Next nItem2
	
	If lRet; Exit; EndIf // Se encontrou um passageiro em comum, retorna .T.
Next nItem1

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Succe
Fun��o respons�vel pela apura��o de acordo do tipo success.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Succe(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := (MV_PAR16 == 1)	// Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAcorItem  := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nX         := 0
Local nItem      := 0

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C04', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG5V(aFechaSeg[1], 'C04', cSegmento) // Success

For nX := 1 to Len(aAcordos)

	lAplica := .F.
	aAux    := TA040ArG6O(aAcordos[nX])	// Success
	nItem   := 0

	aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
	nLen := Len(aDadosApur[ARR_FILHOS])

	//+------------------------------------------------------
	// Seleciona IV
	//+------------------------------------------------------
	aDetalhes	:= TA040SeG3Q(cCliMoeda, MV_PAR09, MV_PAR10, cCliCod, cCliLoja, cSegmento) // Success

	//+------------------------------------------------------
	// Seleciona Itens do Acordo
	//+------------------------------------------------------
	aAcorItem	:= Ta040SeG60(aAcordos[nX])

	//+------------------------------------------------------
	// Valida IV X Itens Acordo Success
	//+------------------------------------------------------
	ProcRegua(Len(aDetalhes))
	For nDetalhe := 1 To Len(aDetalhes)
		IncProc()
		If TA040SucVl(aAcorItem, aDetalhes[nDetalhe])
			aAux := TA040ArG6N(aDetalhes[nDetalhe], StrZero(++nItem, 5))	// Success
			aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
			lOk := .T.
		EndIf
	Next nDetalhe
Next

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

TURXNIL(aDadosApur)
TURXNIL(aAcordos)
TURXNIL(aAcorItem)
TURXNIL(aAux)
TURXNIL(aDetalhes)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SucVl
Fun��o respons�vel pela valida��o do success.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SucVl(aAcorItem, aDetalhe)

Local aRegras   := {}
Local lRet      := .F.
Local xTodos    := Nil
Local xValorG60 := Nil
Local xValorG3Q := Nil
Local nAcorItem := 0
Local nRegra    := 0

//+------------------------------------------------------
//|	Array com De->Para das regras do acordo e valor que considera todos, se houver
//+------------------------------------------------------
aAdd(aRegras, {{'ACORDO'	, 'G60_GRPROD'}, {'IV', 'G3Q_GRPPRD'}, {'TODOS', ''  }})
aAdd(aRegras, {{'ACORDO'	, 'G60_DESTIN'}, {'IV', 'G3Q_DESTIN'}, {'TODOS', '3' }})
aAdd(aRegras, {{'ACORDO'	, 'G60_ASSIST'}, {'IV', 'G3Q_TPEMIS'}, {'TODOS', '99'}})

//+------------------------------------------------------
// Valida se G44_CHEIA n�o est� zerado
//+------------------------------------------------------
lRet := TA040SucTa(aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_NUMID'})][2], aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_IDITEM'})][2], aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]) > 0

//+------------------------------------------------------
// Valida itens do acordo
//+------------------------------------------------------
If lRet
	For nAcorItem := 1 To Len(aAcorItem)

		For nRegra := 1 To Len(aRegras)
			xTodos		:= aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'TODOS'})][2]
			xValorG60	:= aAcorItem[nAcorItem][AScan(aAcorItem[nAcorItem],{|x, y| x[1] == aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'ACORDO'})][2]})][2]
			xValorG3Q	:= aDetalhe[AScan(aDetalhe,{|x, y| x[1] == aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'IV'})][2] })][2]

			//+------------------------------------------------------
			//| Se valor igual a TODOS, valida e passa para pr�ximo item do acordo
			//+------------------------------------------------------
			If !Empty(xTodos) .And. xTodos == xValorG60
				Loop
			Else
				If !(lRet := ((xValorG60 == xValorG3Q) .Or. (Empty(xValorG60) .And. aRegras[nRegra][AScan(aRegras[nRegra],{|x, y| x[1] == 'ACORDO'})][2] == 'G60_GRPROD')))
					Exit
				EndIf
			EndIf
		Next nRegra

		If !lRet
			Exit
		EndIf
	Next nAcorItem
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ComVl
Fun��o respons�vel pela valida��o do combo.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ComVl(aAcorItem, cItem, aAcoItem1, aRecno, cCampo)

Local cWhere    := '% '
Local cGrpPrd   := aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_GRPROD'})][2]
Local cDestin   := aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_DESTIN'})][2]
Local cTpEmis   := aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_ASSIST'})][2]
Local nQuantAco := aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_QTDADE'})][2]
Local lPrinci   := aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_PRINCI'})][2] == '1'
Local nQuant    := 0
Local lRet      := .F.
Local aItens    := {}
Local aAux      := {}
Local nAux      := 0
Local nX        := 0
Local lAplica   := .F.

Default aRecno  := {}
Default cCampo  := ''

If cItem = '1'
	aAcoItem1 := {}
EndIf

//+------------------------------------------------------
//|	Valida atributos dos itens do acordo
//+------------------------------------------------------
cWhere += " AND G3Q_GRPPRD = '" + cGrpPrd + "' "
cWhere += IIF(cDestin == '3' , '', " AND G3Q_DESTIN = '" + cDestin + "' ")
cWhere += IIF(cTpEmis == '99', '', " AND G3Q_TPEMIS = '" + cTpEmis + "' ")
cWhere += IIF(Empty(cCampo), '', " AND " + cCampo)

cWhere += ' %'

aItens := TA040SeG3Q(cCliMoeda, MV_PAR09, MV_PAR10, cCliCod, cCliLoja, cSegmento, cWhere) // Combo

//+------------------------------------------------------
//|	Verifica se j� possui acordos dos tipos C01 e C05 aplicados no IV
//+------------------------------------------------------
For nX := 1 To Len(aItens)

	If !TA040ExG48(aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMID'})][2], ;
	               aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_IDITEM'})][2], ;
	               aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2], ;
	               '|C05|C01|')

		//+------------------------------------------------------
		//|	Valida regra de aplica��o do IV (rotina em TURA034.PRW)
		//+------------------------------------------------------
		lAplica := Tura34RegApl(aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_CODACO'})][2]/*cAcordo*/, ;
		                        aAcorItem[AScan(aAcorItem,{|x, y| x[1] == 'G61_CODREV'})][2]/*cRevisao*/, ;
		                        '1'/*cTipoAcordo*/, ;		// 1-Cliente
		                        aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMID'})][2]/*cRegVenda*/, ;
		                        aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_IDITEM'})][2]/*cItemVenda*/, ;
		                        aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]/*cNumSeq*/, ;
		                        /*[oModel]*/)

		//+------------------------------------------------------
		//|	Indica apenas um registro como principal
		//+------------------------------------------------------
		If lAplica
			If lPrinci
				aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G61_PRINCI'})][2] := '1'
			Else
				aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G61_PRINCI'})][2] := '2'
			EndIf
			nQuant++
			aAdd(aAux, AClone(aItens[nX]))
			If (lRet := nQuant >= nQuantAco) .And. cItem == '2'
				Exit
			EndIf
		EndIf
	EndIf
Next

For nAux := 1 To Len(aAux)
	If cItem == '1'
		aAdd(aAcoItem1, AClone(aAux[nAux]))
	Else
		aAdd(aRecno, AClone(aAux[nAux]))
	EndIf
Next

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SucTa
Fun��o respons�vel pelo retorno da taxa cheia

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SucTa(cRegVen, cItVend, cSeqIV)

Local aArea    := GetArea()
Local nRet	   := 0

DbSelectArea('G44')
G44->(DbSetOrder(1))	// G44_FILIAL+G44_NUMID+G44_IDITEM+G44_NUMSEQ
G44->(DbGoTop())

If G44->(DbSeek( xFilial('G44') + cRegVen + cItVend + cSeqIV))
	nRet := G44->G44_CHEIA
EndIf
G44->(DbCloseArea())

RestArea(aArea)

Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Flat
Fun��o respons�vel pela apura��o de acordo do tipo flat.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Flat(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := .F.
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nX         := 0

lOk := (MV_PAR16) == 1	// Gera Apura��o Sem Valor?

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C02', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG5V(aFechaSeg[1], 'C02', cSegmento) // Flat

For nX := 1 to Len(aAcordos)
	aAux := TA040ArG6O(aAcordos[nX])	// Flat
	aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })

	nLen		:= Len(aDadosApur[ARR_FILHOS])
	aDetalhes	:= TA040SeG3Q(cCliMoeda, MV_PAR09, MV_PAR10, cCliCod, cCliLoja, cSegmento, "% AND G3Q_VLRSER <> 0 %") // Flat

	For nDetalhe := 1 To Len(aDetalhes)
		aAux := TA040ArG6N(aDetalhes[nDetalhe], StrZero(nDetalhe, 5))	// Flat
		aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
		lOk := .T.
	Next
Next

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Manag
Fun��o respons�vel pela apura��o de acordo do tipo management.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Manag(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := (MV_PAR16) == 1	// Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nX         := 0

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C03', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG5V(aFechaSeg[1], 'C03', cSegmento) // Management

For nX := 1 to Len(aAcordos)
	aAux := TA040ArG6O(aAcordos[nX]) // Management
	aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
	nLen      := Len(aDadosApur[ARR_FILHOS])
	aDetalhes := TA040SeG5Z(aAcordos[nX])

	For nDetalhe := 1 To Len(aDetalhes)
		aAux := TA040ArG6P(aDetalhes[nDetalhe], StrZero(nDetalhe, 5))
		aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux),{} })
		lOk := .T.
	Next
Next

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Combo
Fun��o respons�vel pela apura��o de acordo do tipo combo.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Combo(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := (MV_PAR16) == 1 // Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nAcordo    := 0
Local cAcordo    := ''

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C05', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG48('C05') // Combo

For nAcordo := 1 to Len(aAcordos)

	If cAcordo <> aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
					aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]
		aAux := TA040ArG6O(aAcordos[nAcordo]) // Combo
		aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
		nLen := Len(aDadosApur[ARR_FILHOS])
	EndIf

	cAcordo := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
	           aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

	aAux := TA040ArG48(aAcordos[nAcordo])
	aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
	lOk := .T.

Next nAcordo

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Repas
Fun��o respons�vel pela apura��o de acordo do tipo repasse.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Repas(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := (MV_PAR16) == 1 // Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nAcordo    := 0
Local cAcordo    := ''

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C07', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG48('C07') // Repasse

For nAcordo := 1 to Len(aAcordos)

	If cAcordo <> aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
					aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

		aAux := TA040ArG6O(aAcordos[nAcordo]) // Repasse
		aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
		nLen := Len(aDadosApur[ARR_FILHOS])
	EndIf

	cAcordo := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
	           aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

	aAux := TA040ArG48(aAcordos[nAcordo]) // Repasse
	aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
	lOk := .T.

Next nAcordo

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040TaxaA
Fun��o respons�vel pela apura��o de acordo do tipo taxa agencia.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040TaxaA(aFechaSeg, aApur, lApura, aTpTpAcd, cClass)

Local lOk        := (MV_PAR16) == 1 // Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nAcordo    := 0
Local cAcordo    := ''

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M(cClass, aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG48(cClass)

For nAcordo := 1 to Len(aAcordos)

	If cAcordo <> aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
					aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

		aAux := TA040ArG6O(aAcordos[nAcordo]) // Taxas Agencia
		aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
		nLen := Len(aDadosApur[ARR_FILHOS])
	EndIf

	cAcordo := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
	           aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

	aAux := TA040ArG48(aAcordos[nAcordo])
	aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
	lOk := .T.

Next nAcordo

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Trans
Fun��o respons�vel pela apura��o de acordo do tipo transaction.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Trans(aFechaSeg, aApur, lApura, aTpTpAcd)

Local lOk        := (MV_PAR16) == 1 // Gera Apura��o Sem Valor?
Local aDadosApur := Array(2)
Local aAcordos   := {}
Local aAux       := {}
Local aDetalhes  := {}
Local nDetalhe   := 0
Local nLen       := 0
Local nAcordo    := 0
Local cAcordo    := ''

aDadosApur             := Array(2)
aDadosApur[ARR_DADOS]  := TA040ArG6M('C01', aTpTpAcd)
aDadosApur[ARR_FILHOS] := {}

aAcordos := Ta040SeG48('C01') // Transaction

For nAcordo := 1 to Len(aAcordos)

	If cAcordo <> aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
				  aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

		aAux := TA040ArG6O(aAcordos[nAcordo]) // Transaction
		aAdd(aDadosApur[ARR_FILHOS], { AClone(aAux), {} })
		nLen := Len(aDadosApur[ARR_FILHOS])
	EndIf

	cAcordo := aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODACD'})][2] + ;
	           aAcordos[nAcordo][AScan(aAcordos[nAcordo],{|x, y| x[1] == 'G48_CODREC'})][2]

	aAux := TA040ArG48(aAcordos[nAcordo])
	aAdd(aDadosApur[ARR_FILHOS][nLen][ARR_FILHOS], { AClone(aAux), {} })
	lOk := .T.
Next nAcordo

If lOk
	aAdd(aApur[ARR_FILHOS], {{}, Array(2) })
	nLen := Len( aApur[ARR_FILHOS] )
	aApur[ARR_FILHOS][nLen] := AClone(aDadosApur)
	lApura := .T.
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ExApu
Fun��o respons�vel pela cria��o da apura��o atrav�s do modelo de dados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ExApu(cApur)

Local oModel040  := FWLoadModel('TURA040')
Local lRet       := .T.
Local nX         := 0
Local nY         := 0
Local nDetalhe   := 0
Local nLinhaG82  := 0
Local nLinhaG6M  := 0
Local aG6LDados  := AClone(aApur[ARR_DADOS])
Local aG6LFilhos := AClone(aApur[ARR_FILHOS])
Local aRecDados  := {}
Local aRecFilhos := {}
Local aG6ODados  := {}
Local aG6OFilhos := {}
Local aDetalhes  := {}
Local nReceita   := 0
Local nG6O       := 0
Local nG82       := 0
Local cTipoAc    := ''
Local cBasApu    := ''

oModel040:GetModel('G6M_DETAIL'):SetNoDeleteLine(.F.)
oModel040:SetOperation(MODEL_OPERATION_INSERT)
oModel040:Activate()

//+------------------------------------------------------
//|	Apura��o de Receita Cliente - G6L
//+------------------------------------------------------
TA040SetVl(@oModel040, 'G6L_MASTER', aG6LDados)	// G6L

//+------------------------------------------------------
//|	Tipos de Apura��o - G6M
//+------------------------------------------------------
For nReceita := 1 to Len(aG6LFilhos)
	aRecDados 	:= AClone(aG6LFilhos[nReceita][ARR_DADOS])
	aRecFilhos	:= AClone(aG6LFilhos[nReceita][ARR_FILHOS])

	If AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'}) > 0
		cTipoAc := aRecDados[AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'})][2]
		nLinhaG6M++
	Else
		cTipoAc := 'V01'
		nLinhaG82++
	EndIf

	Do Case
		Case cTipoAc == 'V01'
			TA040SetVl(@oModel040, 'G82_DETAIL', aRecDados, nLinhaG82 > 1) 	// G82

			ProcRegua(Len(aRecFilhos))
			For nDetalhe := 1 To Len(aRecFilhos)
				IncProc()
				aDetalhes 	:= AClone(aRecFilhos[nDetalhe][ARR_DADOS])
				TA040G48Up(aDetalhes, oModel040:GetValue('G6L_MASTER', 'G6L_CODAPU'))	// Servi�o Pr�prio
				TA040XCP28(oModel040, aDetalhes)											//	Atualiza G82_CODPRD
			Next nDetalhe

		Case cTipoAc $ 'C01|C02|C03|C04|C05|C06|C12|C07|C10'
			TA040SetVl(@oModel040, 'G6M_DETAIL', aRecDados, nLinhaG6M > 1) 	// G6M

			//+------------------------------------------------------
			//|	Acordos da Apura��o - G6O
			//+------------------------------------------------------
			For nG6O := 1 To Len(aRecFilhos)
				aG6ODados  := AClone(aRecFilhos[nG6O][ARR_DADOS])
				aG6OFilhos := AClone(aRecFilhos[nG6O][ARR_FILHOS])
				cTipoAc    := aRecDados[AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'})][2]

				TA040SetVl(@oModel040, 'G6O_DETAIL', aG6ODados, nG6O > 1)	// G6O

				ProcRegua(Len(aG6OFilhos))
				For nDetalhe := 1 To Len(aG6OFilhos)
					IncProc()
					aDetalhes := AClone(aG6OFilhos[nDetalhe][ARR_DADOS])
					cBasApu   := aG6ODados[AScan(aG6ODados,{|x, y| x[1] == 'G6O_BASAPU'})][2]

					Do Case
						Case cTipoAc == 'C02'	// Flat
							TA040SetVl(oModel040, 'G6N_DETAIL', aDetalhes, nDetalhe > 1)		// G6N

						Case cTipoAc == 'C03'	// Management
							TA040SetVl(oModel040, 'G6P_DETAIL', aDetalhes, nDetalhe > 1)		// G6P

						Case cTipoAc == 'C04'	// Success
							TA040SetVl(oModel040, 'G6N_DETAIL', aDetalhes, nDetalhe > 1)		// G6N

						Case cTipoAc == 'C05'	// Combo
							TA040XCP27(oModel040, aDetalhes)
							TA040XCP33(oModel040, aDetalhes)				//|	Atualiza G6O_VLBASE C05
							TA040XCP46(oModel040, aDetalhes)				//|	Atualiza G6O_VALOR  C05

						Case cTipoAc $ 'C01|C06|C12|C07|C10'	// Transaction | Taxa Agencia | Repasse | Taxa Reembolso
							TA040XCP32(oModel040, aDetalhes)				//|	Atualiza G6O_VAPLIC C01|C06|C12|C07|C10
							TA040XCP33(oModel040, aDetalhes)				//|	Atualiza G6O_VLBASE C01|C06|C12|C07|C10
					EndCase
				Next nDetalhe
			Next nG6O
		EndCase
Next nReceita

If !TA040ApZer(oModel040)
	cApur := oModel040:GetValue('G6L_MASTER', 'G6L_CODAPU')
	
	If (lRet := oModel040:VldData())
		If (lRet := oModel040:CommitData())
			For nReceita := 1 to Len(aG6LFilhos)
				aRecDados 	:= AClone(aG6LFilhos[nReceita][ARR_DADOS])
				aRecFilhos	:= AClone(aG6LFilhos[nReceita][ARR_FILHOS])
				
				If AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'}) > 0
					If aRecDados[AScan(aRecDados,{|x, y| x[1] == 'G6M_TIPOAC'})][2] $ 'C01|C05|C06|C12|C07|C10'
						ProcRegua(Len(aRecFilhos))
						For nG6O := 1 To Len(aRecFilhos)
							IncProc()
							aG6ODados  := AClone(aRecFilhos[nG6O][ARR_DADOS])
							aG6OFilhos := AClone(aRecFilhos[nG6O][ARR_FILHOS])
	
							For nDetalhe := 1 To Len(aG6OFilhos)
								aDetalhes := AClone(aG6OFilhos[nDetalhe][ARR_DADOS])
								TA040G48Up(aDetalhes, oModel040:GetValue('G6L_MASTER', 'G6L_CODAPU'))
							Next nDetalhe
						Next nG6O
					EndIf
				EndIf
			Next nReceita
		EndIf		
	Else
		TA040GetEr(oModel040)
		RollBackSX8()
	EndIf
Else 
	lRet := .F.
EndIf

oModel040:DeActivate()
oModel040:Destroy()

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SetVl
Fun��o respons�vel pela atualiza��o dos valores do model.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SetVl(oModel040, cIdModel, aDados, lLinha)

Local lRet     := .T.
Local nX       := 0
Local nY       := 0
Local nPos     := 0
Local oMdl     := oModel040:GetModel(cIdModel)
Local oStru    := oMdl:GetStruct()
Local aStru    := oStru:GetFields()
Local cClasse  := ''

Default lLinha := .F.

If (cClasse := oMdl:ClassName()) == 'FWFORMGRID'
	oMdl:SetNoInsertLine(.F.)
EndIf

If lLinha
	oMdl:AddLine()
EndIf

For nX := 1 To Len( aDados )
	//+------------------------------------------------------
	//|	Verifica se os campos passados existem na estrutura do modelo
	//+------------------------------------------------------
	If (nPos := aScan(aStru, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
		If !(lRet := oModel040:SetValue(cIdModel, aStru[nPos][3], aDados[nX][2]))
			MsgAlert(STR0011 + aStru[nPos][3]) // "Erro ao atribuir valor "
			Exit
		EndIf
	EndIf
Next nX

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG6L
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G6L.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG6L()

Local aRet  := {}
Local cNome := Posicione('SA1', 1, xFilial('SA1') + cCliCod + cCliLoja, 'A1_NOME')

aAdd(aRet, {'G6L_TPAPUR', '1'		})		// 1=Clientes;2=Fornecedores
aAdd(aRet, {'G6L_CLIENT', cCliCod	})
aAdd(aRet, {'G6L_LOJA'  , cCliLoja	})
aAdd(aRet, {'G6L_NOME'  , cNome		})
aAdd(aRet, {'G6L_DTGERA', dDataBase})
aAdd(aRet, {'G6L_DTINI' , MV_PAR09	})
aAdd(aRet, {'G6L_DTFIM' , MV_PAR10	})
aAdd(aRet, {'G6L_STATUS', '1'		})		// 1=Em aberto;2=Liberado
aAdd(aRet, {'G6L_MOEDA' , cCliMoeda})		// Moeda do Item de Venda

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG6M
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G6M.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG6M(cTipoAC, aTpTpAcd)

Local aRet			:= {}
Local lExtTurNat	:= FindFunction('U_TURNAT')
Local cTpTpAC		:= aTpTpAcd[AScan(aTpTpAcd,{|x, y| x[1] == cTipoAC})][2]
Local cNat			:= ""

aAdd(aRet, {'G6M_SEGNEG', cSegmento})	// 1=Corporativo;2= Eventos;3=Lazer
aAdd(aRet, {'G6M_TIPOAC', cTipoAC  })	// G8B
aAdd(aRet, {'G6M_RATPAD', ' '      })	// 1=Sim;2=N�o

cNat := IIF(lExtTurNat, U_TURNAT(cFilAnt,IIF(cTpTpAC == '1','3','4'), cTipoAC, cSegmento, " ", '1', cCliCod, cCliLoja ), "")
If !Empty(cNat) .And. TurVldNat(cNat, .F., )
	aAdd(aRet, {'G6M_NATURE', cNat })	
EndIf

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG82
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G82.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG82(cTipoAC, aProduto)

Local aRet 		:= {}
Local cProduto	:= aProduto[AScan(aProduto,{|x, y| x[1] == 'G48_CODPRD'})][2]
Local lExtTurNat	:= FindFunction('U_TURNAT')

aAdd(aRet, {'G82_SEGNEG', cSegmento})		// 1=Corporativo;2= Eventos;3=Lazer
aAdd(aRet, {'G82_TIPOAC', cTipoAC})		// G8B
aAdd(aRet, {'G82_CODPRD', cProduto})
aAdd(aRet, {'G82_RATPAD', ' '})
aAdd(aRet, {'G82_NATURE', IIF(lExtTurNat, U_TURNAT(cFilAnt, '3', cTipoAC, cSegmento, cProduto, '1', cCliCod, cCliLoja ), "")})

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG80
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G80.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG80(nSequen, cCli, cLoja, cTpEnt, cItEnt, nValorIV, nTotalIV, nVlDesc, nVlTxAd, cTipoAc, nVlAcPr)

Local oModel  := FwModelActive()
Local aRet    := {}
Local nX      := 0
Local cDesEnt := Posicione('G3E', 1, xFilial('G3E') + cTpEnt, 'G3E_DESCR')
Local cDescr  := Posicione('G3G', 1, xFilial('G3G') + cCli + cLoja + cTpEnt + cItEnt, 'G3G_DESCR')	// G3G_FILIAL+G3G_CLIENT+G3G_LOJA+G3G_TIPO+G3G_ITEM
Local nPerc   := (nValorIV / nTotalIV) * 100
Local nValor  := (nPerc * nVlAcPr) / 100
Local nTaxa   := (nVlTxAd * nPerc) / 100
Local nDesc   := (nVlDesc * nPerc) / 100
Local nTotal	:= nValor + nTaxa - nDesc

Default cTipoAc	:= ''

//+----------------------------------------------------
//|	Aplica descontos e taxas adicionais
//+----------------------------------------------------
nTotal := (nValor - nDesc) + nTaxa

aAdd(aRet, {'G80_SEQUEN', StrZero(nSequen, TamSx3('G80_SEQUEN')[1])})
aAdd(aRet, {'G80_LOJA'  , cLoja  })
aAdd(aRet, {'G80_TPENT' , cTpEnt })
aAdd(aRet, {'G80_DESENT', cDesEnt})
aAdd(aRet, {'G80_ITENT' , cItEnt })
aAdd(aRet, {'G80_DESCR' , cDescr })
aAdd(aRet, {'G80_PERC'  , nPerc  })
aAdd(aRet, {'G80_VALOR' , nValor })
aAdd(aRet, {'G80_PERDES', nDesc  })
aAdd(aRet, {'G80_PERCTX', nTaxa  })	
aAdd(aRet, {'G80_VLRLIQ', nTotal })	

If !Empty(cTipoAc)
	aAdd(aRet, {'G80_TIPOAC', cTipoAc})
EndIf

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG6P
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G6P.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG6P(aDetalhe,cItem)

Local aRet := {}

aAdd(aRet, {'G6P_ITEM'  , cItem})
aAdd(aRet, {'G6P_DESCRI', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_DESCRI'})][2]})
aAdd(aRet, {'G6P_TPCALC', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_TPCALC'})][2]})
aAdd(aRet, {'G6P_QTDADE', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_QTDADE'})][2]})

If aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_TPCALC'})][2] == '1' // 1=Valor; 2=Formula
	aAdd(aRet, {'G6P_VALOR',  aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_VALOR' })][2]})
Else
	aAdd(aRet, {'G6P_FORMUL', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G5Z_FORMUL'})][2]})
EndIf

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG48
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G48.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG48(aDetalhe)

Local aRet := {}

aAdd(aRet, {'G48_NUMID' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_NUMID' })][2]})
aAdd(aRet, {'G48_IDITEM', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_IDITEM'})][2]})
aAdd(aRet, {'G48_NUMSEQ', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_NUMSEQ'})][2]})
aAdd(aRet, {'G48_APLICA', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_APLICA'})][2]})
aAdd(aRet, {'G48_CODACD', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_CODACD'})][2]})
aAdd(aRet, {'G48_CODREC', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_CODREC'})][2]})
aAdd(aRet, {'G48_CLASS' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_CLASS' })][2]})
aAdd(aRet, {'G48_VLBASE', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_VLBASE'})][2]})
aAdd(aRet, {'G48_SEGNEG', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3P_SEGNEG'})][2]})
aAdd(aRet, {'G48_OK'    , .T.})
aAdd(aRet, {'G3P_RECNO' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3P_RECNO' })][2]})
aAdd(aRet, {'G3Q_RECNO' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_RECNO' })][2]})
aAdd(aRet, {'G48_RECNO' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_RECNO' })][2]})
aAdd(aRet, {'G48_VLACD' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_VLACD' })][2]})
aAdd(aRet, {'G3Q_VLRSER', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_VLRSER'})][2]})
aAdd(aRet, {'G48_TPACD' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G48_TPACD' })][2]})

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG6N
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G6N.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG6N(aDetalhe, cItem, lOk, dDtIncl)

Local aRet := {}

Default lOk     := .T. 
Default dDtIncl := dDataBase

aAdd(aRet, {'G6N_ITEM'  , cItem})
aAdd(aRet, {'G6N_OK'    , lOk})
aAdd(aRet, {'G6N_FILREF', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_FILIAL'})][2]})
aAdd(aRet, {'G6N_REGVEN', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_NUMID' })][2]})
aAdd(aRet, {'G6N_ITVEND', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_IDITEM'})][2]})
aAdd(aRet, {'G6N_SEQIV' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]})
aAdd(aRet, {'G6N_OPERAC', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_OPERAC'})][2]})
aAdd(aRet, {'G6N_TPDOC' , aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_TPDOC' })][2]})
aAdd(aRet, {'G6N_DOCFOR', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_DOC'   })][2]})
aAdd(aRet, {'G6N_GRPROD', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_GRPPRD'})][2]})
aAdd(aRet, {'G6N_DESGRP', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_DESGRP'})][2]})
aAdd(aRet, {'G6N_EMISSA', aDetalhe[AScan(aDetalhe,{|x, y| x[1] == 'G3Q_EMISS' })][2]})
aAdd(aRet, {'G6N_DTINCL', dDtIncl})

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG6O
Fun��o respons�vel pela montagem do array com os dados a serem gravados na G6O.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG6O(aAcordo)

Local aRet := {}

aAdd(aRet, {'G6O_CODACD', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODACO'})][2]})
aAdd(aRet, {'G6O_CODREV', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_CODREV'})][2]})
aAdd(aRet, {'G6O_DCACD' , aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_DESCRI'})][2]})
aAdd(aRet, {'G6O_TIPOVL', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_TPVAL' })][2]})	// 1=Porcentagem;2=Valor Fixo
aAdd(aRet, {'G6O_BASAPU', aAcordo[AScan(aAcordo, {|x, y| x[1] == 'G5V_BASAPU'})][2]})	// 1=Volume;2=Quantidade;3=Custo

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040GetEr
Fun��o respons�vel recupera��o do erro ocorrido no modelo de dados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040GetEr(oModel)

Local aErro := oModel:GetErrorMessage()

AutoGrLog(STR0012 + ' [' + AllToChar( aErro[1] ) + ']') // "Id do formul�rio de origem:"
AutoGrLog(STR0013 + ' [' + AllToChar( aErro[2] ) + ']') // "Id do campo de origem: "
AutoGrLog(STR0014 + ' [' + AllToChar( aErro[3] ) + ']') // "Id do formul�rio de erro: "
AutoGrLog(STR0015 + ' [' + AllToChar( aErro[4] ) + ']') // "Id do campo de erro: "
AutoGrLog(STR0016 + ' [' + AllToChar( aErro[5] ) + ']') // "Id do erro: "
AutoGrLog(STR0017 + ' [' + AllToChar( aErro[6] ) + ']') // "Mensagem do erro: "
AutoGrLog(STR0018 + ' [' + AllToChar( aErro[7] ) + ']') // "Mensagem da solu��o: "
AutoGrLog(STR0019 + ' [' + AllToChar( aErro[8] ) + ']') // "Valor atribu�do: "
AutoGrLog(STR0020 + ' [' + AllToChar( aErro[9] ) + ']') // "Valor anterior: "
MostraErro()

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040TpAco
Fun��o respons�vel pela tela de tipos de acordos.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040TpAco(aTpAcordo)

Local oDlg     := Nil
Local oFWLayer := FWLayer():New()
Local aAcordos := {}
Local oMark    := FWMarkBrowse():New()
Local oPanel   := Nil
Local oButton  := FwButtonBar():New()
Local lRet     := .F.

Define MsDialog oDlg Title STR0021 From 0, 0 To 360, 560 Pixel // "Sele��o de Tipos de Acordo"

oFWLayer:Init(oDlg, .F., .T.)
oFWLayer:AddLine('ALLL', 100, .F.)
oFWLayer:AddCollumn('ALLC' , 100, .T., 'ALLL')
oPanel := oFWLayer:GetColPanel('ALLC' , 'ALLL')

oMark:SetAlias('G8B')
oMark:SetFieldMark('G8B_OK')
oMark:SetAllMark({|| oMark:AllMark()})
oMark:AddFilter(STR0133, "SUBSTR(G8B_CODIGO, 1, 1) == 'C' .AND. !(G8B_CODIGO $ 'C08|C09|C11')", .T., .T.)		// "Filtro Padr�o"
oMark:SetOwner(oPanel)

oMark:SetMenuDef('')
oMark:SetProfileID('1')
oMark:ForceQuitButton()
oMark:Activate()

oButton:Init(oDlg, 015, 015, CONTROL_ALIGN_BOTTOM, .T.)
oButton:AddBtnText(STR0022, STR0023, {||oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) // "Sair"###"Cancela Opera��o"
oButton:AddBtnText(STR0024, STR0024, {||lRet := TA040OkAc(oMark, aTpAcordo), oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) // "OK"###"OK"

Activate MsDialog oDlg Center

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeSA1
Fun��o respons�vel sele��o dos dados do cliente.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeSA1(dDtDe, dDtAte)

Local aArea     := GetArea() 
Local cAliasSA1 := GetNextAlias()
Local aRet      := {}
Local aAux      := {}
Local nX        := 0
Local cFilG3Q   := AllTrim(xFilial("G3Q", xFilial("G6L"))) + '%'
Local cFilSA1   := TA040ExpFil("SA1", "SA1", "G3Q") 

Default dDtDe   := CToD('  /  /  ')
Default dDtAte  := CToD('  /  /  ')

BeginSql Alias cAliasSA1
	SELECT DISTINCT G3Q_CLIENT, G3Q_LOJA, G3Q_MOEDCL
	FROM %Table:G3Q% G3Q
	INNER JOIN %Table:SA1% SA1 ON %Exp:cFilSA1% 
	                              A1_COD  = G3Q_CLIENT AND
	                              A1_LOJA = G3Q_LOJA   AND
							      SA1.%NotDel%
	WHERE G3Q_FILIAL LIKE %Exp:cFilG3Q% AND  
	      G3Q_DTINC  BETWEEN %Exp:dDtDe%    AND %Exp:dDtAte%   AND
	      G3Q_CLIENT BETWEEN %Exp:MV_PAR01% AND %Exp:MV_PAR03% AND
	      G3Q_LOJA   BETWEEN %Exp:MV_PAR02% AND %Exp:MV_PAR04% AND
		  A1_GRPVEN  BETWEEN %Exp:MV_PAR20% AND %Exp:MV_PAR21% AND 
		  G3Q_CONINU = %Exp:Space(TamSx3('G3Q_CONINU')[1])%    AND
	      G3Q.%NotDel%
	ORDER BY G3Q_CLIENT, G3Q_LOJA, G3Q_MOEDCL
EndSql

While (cAliasSA1)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasSA1)->(FCount())
		aAdd(aAux, {(cAliasSA1)->(FieldName(nX)), (cAliasSA1)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasSA1)->(DbSkip())
EndDo

(cAliasSA1)->(DbCloseArea())
RestArea(aArea)

Return aRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040OKAc
Fun��o respons�vel sele��o dos tipos de acordo marcados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040OKAc(oMark, aTpAcordo)

Local cMark := oMark:cMark
Local lRet  := .F.

oMark:Mark(cMark)
G8B->(DbGoTop())

While G8B->(!EOF())
	If oMark:IsMark(cMark)
		aAdd(aTpAcordo, G8B->G8B_CODIGO)
		lRet := .T.
	EndIf
	G8B->(DbSkip())
EndDo

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta040SeG5V
Fun��o respons�vel pela sele��o dos acordos vinculados ao cliente.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function Ta040SeG5V(cCodCliCpl, cTipo, cSegmento)

Local aArea     := GetArea()
Local cAliasG5V := GetNextAlias()
Local cWhere    := ''
Local aRet      := {}
Local aAux      := {}
Local nX        := 0

Do Case
	Case cSegmento == '1'
		cWhere := "% G5V_CORP = 'T' %"
	Case cSegmento == '2'
		cWhere := "% G5V_EVENTO = 'T' %"
	Case cSegmento == '3'
		cWhere := "% G5V_LAZER = 'T' %"
EndCase

BeginSql Alias cAliasG5V
	Column G5V_VIGFIM as Date
	SELECT G5V_CODACO, G5V_CODREV, G5V_BASAPU, G5V_VALOR, G5V_PASSAG, G5V_VIGFIM, 
			G5V_DESCRI, G5V_ACOORI, G5V_BASAPU, G5V_BASCAL, G5V_TPVAL AS G5V_TPVAL, G5V_NIVEL
		FROM %Table:G5V% G5V
		INNER JOIN %Table:G69% G69 ON G69_CODACO = G5V_CODACO AND G69_FILIAL = %xFilial:G69% AND G69.%NotDel%
		WHERE G69_CODIGO = %Exp:cCodCliCpl% AND 
		      G5V_TPFEE = %Exp:cTipo% AND 
		      G5V_FILIAL = %xFilial:G5V% AND 
		      G5V_MSBLQL <> '1' AND 
		      G5V_FORAPL = '2' AND
		      G5V_FTSERV = '2' AND
		      G5V.%NotDel% AND 
		      %Exp:cWhere%
		ORDER BY G5V_VALOR DESC
EndSql

While (cAliasG5V)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG5V)->(FCount())
		aAdd(aAux, {(cAliasG5V)->(FieldName(nX)), (cAliasG5V)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG5V)->(DbSkip())
EndDo

(cAliasG5V)->(DbCloseArea())

RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG5Z
Fun��o respons�vel pela sele��o dos itens do acordo Management.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG5Z(aAcordo)

Local aArea     := GetArea()
Local cAliasG5Z := GetNextAlias()
Local cWhere    := ''
Local aRet      := {}
Local aAux      := {}
Local cCodAco   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRev   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]
Local nX        := 0

BeginSql Alias cAliasG5Z
	SELECT G5Z_CODACO, G5Z_CODREV, G5Z_ITEM, G5Z_TPCALC, G5Z_QTDADE, G5Z_FORMUL, G5Z_VALOR, G5Z_TOTAL, G5Z_DESCRI
		FROM %Table:G5Z% G5Z
		WHERE G5Z_FILIAL = %xFilial:G5Z% AND 
		      G5Z_CODACO = %Exp:cCodAco% AND 
		      G5Z_CODREV = %Exp:cCodRev% AND 
		      G5Z.%NotDel%
EndSql

While (cAliasG5Z)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG5Z)->(FCount())
		aAdd(aAux, {(cAliasG5Z)->(FieldName(nX)), (cAliasG5Z)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG5Z)->(DbSkip())
EndDo

(cAliasG5Z)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG48
Fun��o respons�vel pela sele��o dos acordos aplicados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG48(cClass, cOrderBy)

Local aArea     := GetArea()
Local cAliasG48 := GetNextAlias()
Local aRet      := {}
Local aAux      := {}
Local nX        := 0
Local cProdDe   := MV_PAR13
Local cProdAte  := IIF(Empty(MV_PAR14), Replicate('Z', Len(MV_PAR14)), MV_PAR14)
Local cJoin     := IIF(cClass == 'C05', "%LEFT%", "%INNER%")

Default cOrderBy	:= '% G48_CODACD, G48_CODREC %'

BeginSql Alias cAliasG48
	Column G48_OK as Logical
	SELECT G48_FILIAL, G48_CODACD, G48_NUMSEQ, G48_NUMID, G48_IDITEM, G48_DTINT, G48_VLACD, G48_CODREC, G48_APLICA, G48_CLASS, G48_VLBASE, 
		   G48_CODPRD, G48_TPACD, G3Q_PRCBAS, G3Q_VLRSER, G48_CHAVEC, G48_OK, G5V_CODACO, G5V_CODREV, G5V_DESCRI, 
		   CASE WHEN G48_CLASS = 'C01' THEN G5X_TPVAL ELSE G5V_TPVAL END G5V_TPVAL, G5V_BASAPU, G5V_NIVEL, G3P_SEGNEG, 
		   G3P.R_E_C_N_O_ as G3P_RECNO, G3Q.R_E_C_N_O_ AS G3Q_RECNO, G48.R_E_C_N_O_ AS G48_RECNO
		FROM %Table:G3P% G3P
		INNER JOIN %Table:G3Q% G3Q ON G3Q_FILIAL = %xFilial:G3Q% AND 
		                              G3Q_NUMID  = G3P_NUMID AND 
		                              G3Q_MOEDCL = %Exp:cCliMoeda% AND
		                              G3Q_CONINU = '' AND
		                              G3Q.%NotDel%
		INNER JOIN %Table:G48% G48 ON G48_FILIAL = %xFilial:G48% AND 
		                              G48_NUMID  = G3Q_NUMID AND 
		                              G48_IDITEM = G3Q_IDITEM AND 
		                              G48_NUMSEQ = G3Q_NUMSEQ AND 
		                              G48_CLIFOR = '1' AND 
		                              G48_STATUS = '1' AND
		                              G48_COMSER = '2' AND
		                              G48_CLASS  = %Exp:cClass% AND 
		                              G48_CODAPU = '' AND 
		                              G48_DTINCL BETWEEN %Exp:MV_PAR09% AND %Exp:MV_PAR10% AND
		                              G48_CONINU = '' AND
		                              G48.%NotDel% 
		%Exp:cJoin%  JOIN %Table:G4C% G4C ON G4C_FILIAL = %xFilial:G4C% AND 
		                              G4C_NUMID  = G48_NUMID     AND 
		                              G4C_IDITEM = G48_IDITEM    AND 
		                              G4C_NUMSEQ = G48_NUMSEQ    AND 
		                              G4C_CLASS  = G48_CLASS     AND 
		                              G4C_APLICA = G48_APLICA    AND 
		                              G4C_STATUS = '2' AND
		                              G4C_CONINU = '' AND 
		                              G4C.%NotDel%
		LEFT JOIN %Table:G5V% G5V ON  G5V_FILIAL = %xFilial:G5V% AND 
		                              G5V_CODACO = G48_CODACD AND 
		                              G5V_CODREV = G48_CODREC AND
		                              G5V_TPFEE = G48_CLASS AND
		                              G5V.%NotDel%  
		LEFT JOIN %Table:G5X% G5X ON  G5X_FILIAL = G5V_FILIAL AND 
		                              G5X_CODACO = G5V_CODACO AND 
		                              G5X_CODREV = G5V_CODREV AND
		                              G5X_GRPROD = G3Q_GRPPRD AND
		                              G5X.%NotDel%  
		WHERE G3P_CLIENT = %Exp:cCliCod% AND 
		      G3P_LOJA = %Exp:cCliLoja% AND 
		      G3P_SEGNEG = %Exp:cSegmento% AND 
		      G3P_FILIAL = %xFilial:G3P% AND 
		      G3Q_PROD BETWEEN %Exp:cProdDe% AND %Exp:cProdAte% AND 
		      G3P.%NotDel%
		GROUP BY G48_FILIAL, G48_CODACD, G48_NUMSEQ, G48_NUMID, G48_IDITEM, G48_DTINT, G48_VLACD, G48_CODREC, G48_APLICA, G48_CLASS, G48_VLBASE, 
				  G48_CODPRD, G48_TPACD, G3Q_PRCBAS, G3Q_VLRSER, G48_CHAVEC, G48_OK, G5V_CODACO, G5V_CODREV, G5V_DESCRI, G5V_TPVAL, G5X_TPVAL, 
				  G5V_BASAPU, G5V_NIVEL, G3P_SEGNEG, G3P.R_E_C_N_O_, G3Q.R_E_C_N_O_, G48.R_E_C_N_O_ 
		ORDER BY %Exp:cOrderBy%
EndSql

While (cAliasG48)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG48)->(FCount())
		aAdd(aAux , {(cAliasG48)->(FieldName(nX)), (cAliasG48)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG48)->(DbSkip())
EndDo

(cAliasG48)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG3Q
Fun��o respons�vel pela pela sele��o dos IVs.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG3Q(cMoeda, dDtDe, dDtAte, cCliCod, cCliLoja, cSegmento, cWhere)

Local aArea     := GetArea()
Local aRet      := {}
Local aAux      := {}
Local cAliasG3Q := GetNextAlias()
Local nX        := 0
Local cProdDe   := MV_PAR13
Local cProdAte  := IIF(Empty(MV_PAR14), Replicate('Z', TamSx3('B1_COD')[1]), MV_PAR14)

Default cMoeda    := ''
Default cCliCod   := ''
Default cCliLoja  := ''
Default cSegmento := ''
Default dDtDe     := CToD('  /  /  ')
Default dDtAte    := CToD('  /  /  ')
Default cWhere    := '% %'

If Empty(MV_PAR13) .And. Empty(MV_PAR14)
	cProdDe   := Replicate(' ', TamSx3("B1_COD")[1])
	cProdAte  := Replicate('Z', TamSx3("B1_COD")[1])
EndIf

BeginSql Alias cAliasG3Q
	Column G3Q_EMISS As Date
	SELECT G3Q_FILIAL, G3Q_NUMID, G3Q_ORDER, G3Q_IDITEM, G3Q_NUMSEQ, G3Q_OPERAC, G3Q_TPDOC, G3Q_DOC, 
	       G3Q_GRPPRD, G3Q_DESGRP, G3Q_EMISS, G3Q_PRCBAS, G3Q_VLRSER, G3Q_DESTIN, G3Q_TPEMIS, 
	       '' AS G61_PRINCI, G3Q.R_E_C_N_O_ AS G3Q_RECNO, G3P.R_E_C_N_O_ AS G3P_RECNO
		FROM %Table:G3P% G3P
		INNER JOIN %Table:G3Q% G3Q ON G3Q_FILIAL = %xFilial:G3Q% AND 
		                              G3Q_NUMID  = G3P_NUMID AND 
		                              G3Q_MOEDCL = %Exp:cMoeda% AND 
		                              G3Q_OPERAC <> '4' AND 
		                              G3Q_DTINC BETWEEN %Exp:dDtDe% AND %Exp:dDtAte% AND
		                              G3Q_CONINU = '' AND 
		                              G3Q.%NotDel%  
		WHERE G3P_CLIENT = %Exp:cCliCod% AND 
		      G3P_LOJA   = %Exp:cCliLoja% AND 
		      G3P_SEGNEG = %Exp:cSegmento% AND 
		      G3P_FILIAL = %xFilial:G3P% AND 
		      G3Q_PROD BETWEEN %Exp:cProdDe% AND %Exp:cProdAte% AND 
		      G3P.%NotDel% 
		      %Exp:cWhere%
		ORDER BY G3Q_NUMID, G3Q_EMISS
EndSql

While (cAliasG3Q)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG3Q)->(FCount())
		aAdd(aAux, {(cAliasG3Q)->(FieldName(nX)), (cAliasG3Q)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG3Q)->(DbSkip())
EndDo

(cAliasG3Q)->(DbCloseArea())

TURXNIL(aAux)
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG60
Fun��o respons�vel pela sele�ao da G60.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG60(aAcordo)

Local aArea     := GetArea()
Local aRet      := {}
Local aAux      := {}
Local cAliasG60 := GetNextAlias()
Local cCodAco   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRev   := aAcordo[AScan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]
Local nX        := 0

BeginSql Alias cAliasG60
	SELECT G60_ITEM, G60_GRPROD, G60_DESTIN, G60_ASSIST, G60_PERCDE, G60_PERCAT, G60_VALOR
	FROM %Table:G60% G60
	WHERE G60_FILIAL = %xFilial:G60% AND 
	      G60_CODACO = %Exp:cCodAco% AND 
	      G60_CODREV = %Exp:cCodRev% AND
	      G60.%NotDel%
EndSql

While (cAliasG60)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG60)->(FCount())
		aAdd(aAux, {(cAliasG60)->(FieldName(nX)), (cAliasG60)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG60)->(DbSkip())
EndDo

(cAliasG60)->(DbCloseArea())

TURXNIL(aAux)
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG61
Fun��o respons�vel pela sele��o da G61.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG61(aAcordo)

Local aArea     := GetArea()
Local aAux      := {}
Local aRet      := {}
Local cAliasG61 := GetNextAlias()
Local nX        := 0
Local cCodAco   := aAcordo[Ascan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRev   := aAcordo[Ascan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]

BeginSql Alias cAliasG61
	SELECT G61_GRPROD, G61_DESTIN, G61_ASSIST, G61_QTDADE, G61_PRINCI, G61_CODACO, G61_ITEM, G61_CODREV
		FROM %Table:G61% G61
		WHERE G61_FILIAL = %xFilial:G61% AND 
		      G61_CODACO = %Exp:cCodAco% AND 
		      G61_CODREV = %Exp:cCodRev% AND
		      G61.%NotDel%
EndSql

While (cAliasG61)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG61)->(FCount())
		aAdd(aAux, {(cAliasG61)->(FieldName(nX)), (cAliasG61)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG61)->(DbSkip())
EndDo

(cAliasG61)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG5X
Fun��o respons�vel pela sele��o da G5X.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG5X(aAcordo)

Local aArea     := GetArea()
Local aAux      := {}
Local aRet      := {}
Local cAliasG5X := GetNextAlias()
Local nX        := 0
Local cCodAco   := aAcordo[Ascan(aAcordo,{|x, y| x[1] == 'G5V_CODACO'})][2]
Local cCodRev   := aAcordo[Ascan(aAcordo,{|x, y| x[1] == 'G5V_CODREV'})][2]

BeginSql Alias cAliasG5X
	SELECT G5X_CODACO, G5X_CODREV, G5X_ITEM, G5X_GRPROD, G5X_DESTIN, G5X_ASSIST, 
	       G5X_TPVAL, G5X_VALOR, G5X_REEMBO, G5X_REMARC, G5X_CANCEL, G5X_BASCAL
		FROM %Table:G5X% G5X
		WHERE G5X_FILIAL = %xFilial:G5X% AND
		      G5X_CODACO = %Exp:cCodAco% AND 
		      G5X_CODREV = %Exp:cCodRev% AND
		      G5X.%NotDel%
EndSql

While (cAliasG5X)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG5X)->(FCount())
		aAdd(aAux, {(cAliasG5X)->(FieldName(nX)) , (cAliasG5X)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG5X)->(DbSkip())
EndDo

(cAliasG5X)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG3G
Fun��o respons�vel pela sele��o das entidades do cliente/loja informados.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG3G(cEntiCli,cEntiLoja)

Local aArea     := GetArea()
Local aAux      := {}
Local aRet      := {}
Local cAliasG3G := GetNextAlias()
Local nX        := 0

BeginSql Alias cAliasG3G
	SELECT G3G_CLIENT, G3G_LOJA, G3G_TIPO, G3G_ITEM, G3G_DESCR
		FROM %Table:G3G% G3G
		WHERE G3G_FILIAL = %xFilial:G3G%  AND 
		      G3G_CLIENT = %Exp:cEntiCli% AND 
		      G3G_LOJA = %Exp:cEntiLoja%  AND 
		      G3G_MSBLQL <> '1' AND 
		      G3G.%NotDel%
EndSql

While (cAliasG3G)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG3G)->(FCount())
		aAdd(aAux, {(cAliasG3G)->(FieldName(nX)), (cAliasG3G)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG3G)->(DbSkip())
EndDo

(cAliasG3G)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG4A
Fun��o respons�vel pela sele�ao de IV x Rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG4A(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg)

Local aArea     := GetArea() 
Local aSaveRows := FwSaveRows()
Local aAux      := {}
Local aRet      := {}
Local cAliasG4A := GetNextAlias()
Local nX        := 0
Local nG6N      := 0
Local nG6O      := 0
Local oModel    := FwModelActive()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local lOk       := .F.

BeginSql Alias cAliasG4A
	SELECT	G6N_REGVEN, G6N_ITVEND, G6N_SEQIV, G6N_VALOR, G6N_CODACD, G6N_ITEM, G4A_PERRAT, G4A_ITEM, 
	       G4A_TPENT, G4A_LOJA, G6N.R_E_C_N_O_ AS G6N_RECNO, G4A.R_E_C_N_O_ AS G4A_RECNO
	FROM %Table:G6N% G6N
	INNER JOIN %Table:G4A% G4A ON G4A_FILIAL = %xFilial:G4A% AND 
	                              G4A_NUMID  = G6N_REGVEN    AND 
	                              G4A_IDITEM = G6N_ITVEND    AND 
	                              G4A_NUMSEQ = G6N_SEQIV     AND 
	                              G4A_TPENT  = %Exp:cTpEnt%  AND 
	                              G4A_ITEM   = %Exp:cItem%   AND 
	                              G4A.%NotDel%
	WHERE G6N_FILIAL = %xFilial:G6N% AND 
	      G6N_CODAPU = %Exp:cCodApu% AND 
	      G6N_TIPOAC = %Exp:cTipoAc% AND 
	      G6N_SEGNEG = %Exp:cSegNeg% AND 
	      G6N.%NotDel%
	ORDER BY G6N_CODACD, G6N_REGVEN, G6N_ITVEND
EndSql

While (cAliasG4A)->(!EOF())
	lOk := .F.

	//+----------------------------------------------------
	// Percorre todos os acordos do tipo/Segmento
	//+----------------------------------------------------
	For nG6O := 1 To oModelG6O:Length()
		oModelG6O:GoLine(nG6O)
		If !oModelG6O:IsDeleted() .And. oModelG6O:GetValue('G6O_CODACD')  == (cAliasG4A)->G6N_CODACD
			For nG6N := 1 To oModelG6N:Length()
				oModelG6N:GoLine(nG6N)
				If (lOk := (!oModelG6N:IsDeleted() .And. oModelG6N:GetValue('G6N_CODACD')  == (cAliasG4A)->G6N_CODACD .And. oModelG6N:GetValue('G6N_REGVEN')  == (cAliasG4A)->G6N_REGVEN .And. oModelG6N:GetValue('G6N_ITVEND')  == (cAliasG4A)->G6N_ITVEND))
					aAux := {}
					For nX := 1 To (cAliasG4A)->(FCount())
						aAdd(aAux, {(cAliasG4A)->(FieldName(nX)), (cAliasG4A)->(FieldGet(nX))})
					Next
					aAdd(aRet, AClone(aAux))
					Exit
				EndIf
			Next nG6N
		EndIf
		If lOk; Exit; EndIf
	Next nG6O
	(cAliasG4A)->(DbSkip())
EndDo

(cAliasG4A)->(DbCloseArea())
FwRestRows(aSaveRows)
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG4C
Fun��o respons�vel pela sele��o de IV x Rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG4C(cTipoAc, cTpEnt, cItem, cCodApu)

Local aArea      := GetArea() 
Local aSaveRows  := FwSaveRows()
Local aAux       := {}
Local aRet       := {}
Local cAliasG4C  := GetNextAlias()
Local nX         := 0
Local oModel     := FwModelActive()
Local oModelG48  := oModel:GetModel('G48A_DETAIL')
Local oModelG48B := oModel:GetModel('G48B_DETAIL')
Local oModelG6O  := oModel:GetModel('G6O_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local nG48       := 0
Local nG6O       := 0
Local nG82       := 0
Local lOk        := .F.
Local cOrderBy   := ''
Local cWhere     := '% '

If cTipoAc == 'V01'
	cOrderBy := '% G48_CODPRD %'
	cWhere   += " AND G48_CODPRD = '" + oModelG82:GetValue('G82_CODPRD') + "' %"
Else
	cOrderBy := '% G48_CODACD, G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA %'
	cWhere   += ' %'
EndIf

BeginSql Alias cAliasG4C
	SELECT	G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA, G48_VLACD, G48_CODPRD, G4C_VALOR,
	       G4C_ENTAD, G4C_ITRAT, G48_CODACD, G48.R_E_C_N_O_ AS G48_RECNO, G4C.R_E_C_N_O_ AS G4C_RECNO
	FROM %Table:G48% G48
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G48_FILIAL    AND 
	                              G4C_NUMID  = G48_NUMID     AND 
	                              G4C_IDITEM = G48_IDITEM    AND 
	                              G4C_NUMSEQ = G48_NUMSEQ    AND 
	                              G4C_CLASS  = G48_CLASS     AND 
	                              G4C_ENTAD  = %Exp:cTpEnt%  AND 
	                              G4C_ITRAT  = %Exp:cItem%   AND
	                              G4C_APLICA = G48_APLICA    AND 
	                              G4C_STATUS = '2' AND
	                              G4C_CONINU = '' AND 
	                              G4C.%NotDel%
	WHERE G48_FILAPU = %xFilial:G6L% AND 
	      G48_CODAPU = %Exp:cCodApu% AND 
	      G48_CLASS  = %Exp:cTipoAc% AND 
	      G48_CONINU = '' AND
	      G48.%NotDel% 
	      %Exp:cWhere%
	ORDER BY %Exp:cOrderBy%
EndSql

If cTipoAc $ 'C01|C05|C06|C12|C07|C10'
	While (cAliasG4C)->(!EOF())
		aAux 	:= {}
		lOk		:= .F.

		//+----------------------------------------------------
		//|	Somente considera itens marcados n�o deletados
		//+----------------------------------------------------
		For nG6O := 1 To oModelG6O:Length()
			oModelG6O:GoLine(nG6O)
			If !oModelG6O:IsDeleted() .And. oModelG6O:GetValue('G6O_CODACD') == (cAliasG4C)->G48_CODACD
				For nG48 := 1 To oModelG48:Length()
					oModelG48:GoLine(nG48)
					If (lOk := (oModelG48:GetValue('G48_NUMID') == (cAliasG4C)->G48_NUMID .And. oModelG48:GetValue('G48_IDITEM') == (cAliasG4C)->G48_IDITEM .And. oModelG48:GetValue('G48_NUMSEQ') == (cAliasG4C)->G48_NUMSEQ .And. oModelG48:GetValue('G48_APLICA') == (cAliasG4C)->G48_APLICA))
						If oModelG48:GetValue('G48_OK') .And. !oModelG48:IsDeleted()
							For nX := 1 To (cAliasG4C)->(FCount())
								aAdd(aAux, {(cAliasG4C)->(FieldName(nX)), (cAliasG4C)->(FieldGet(nX))})
							Next
							aAdd(aRet, AClone(aAux))
						EndIf
						Exit
					EndIf
				Next nG48
			EndIf
			If lOk; Exit; EndIf
		Next nG6O
		(cAliasG4C)->(DbSkip())
	EndDo
EndIf

If cTipoAc == 'V01'
	While (cAliasG4C)->(!EOF())
		lOk  := .F.
		aAux := {}
		For nG48 := 1 To oModelG48B:Length()
			oModelG48B:GoLine(nG48)
			If (lOk := (oModelG48B:GetValue('G48_NUMID') == (cAliasG4C)->G48_NUMID .And. oModelG48B:GetValue('G48_IDITEM') == (cAliasG4C)->G48_IDITEM .And. oModelG48B:GetValue('G48_NUMSEQ') == (cAliasG4C)->G48_NUMSEQ .And. oModelG48B:GetValue('G48_APLICA') == (cAliasG4C)->G48_APLICA))
				If oModelG48B:GetValue('G48_OK') .And. !oModelG48B:IsDeleted()
					For nX := 1 To (cAliasG4C)->(FCount())
						aAdd(aAux, {(cAliasG4C)->(FieldName(nX)), (cAliasG4C)->(FieldGet(nX))})
					Next
					aAdd(aRet, AClone(aAux))
				EndIf
				Exit
			EndIf
		Next nG48
		(cAliasG4C)->(DbSkip())
	EndDo
EndIf

(cAliasG4C)->(DbCloseArea())
FwRestRows(aSaveRows)
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040GetTR
Fun��o respons�vel pela sele��o do IV x rateio

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040GetTR(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg, cCodPrd)

Local nRet      := 0
Local cAliasTot := GetNextAlias()
Local nX        := 0
Local lOk       := .F.
Default cCodPrd := ''

Do Case
	Case cTipoAc $ 'C02|C04'
		BeginSql Alias cAliasTot
			SELECT G6N_VALOR AS VALOR
			FROM %Table:G6N% G6N
			WHERE G6N_FILIAL = %xFilial:G6N% AND 
			      G6N_CODAPU = %Exp:cCodApu% AND 
			      G6N_TIPOAC = %Exp:cTipoAc% AND 
			      G6N_SEGNEG = %Exp:cSegNeg% AND 
			      G6N.%NotDel%
		EndSql
		lOk := .T.

	Case cTipoAc $ 'C01|C05|C06|C12|C07|C10'
		BeginSql Alias cAliasTot
			SELECT G48_VLACD AS VALOR
			FROM %Table:G48% G48
			WHERE G48_FILAPU = %xFilial:G6L% AND 
			      G48_CODAPU = %Exp:cCodApu% AND 
			      G48_CLASS  = %Exp:cTipoAc% AND 
			      G48_SEGNEG = %Exp:cSegNeg% AND
			      G48_CONINU = '' AND
			      G48.%NotDel%
		EndSql
		lOk	:= .T.

	Case cTipoAc == 'V01'
		BeginSql Alias cAliasTot
			SELECT G48_VLACD AS VALOR
			FROM %Table:G48% G48
			WHERE G48_FILAPU = %xFilial:G6L% AND 
			      G48_CODAPU = %Exp:cCodApu% AND 
			      G48_CLASS  = %Exp:cTipoAc% AND 
			      G48_CODPRD = %Exp:cCodPrd% AND 
			      G48_SEGNEG = %Exp:cSegNeg% AND
			      G48_CONINU = '' AND 
			      G48.%NotDel%
		EndSql
		lOk	:= .T.
EndCase

If lOk
	While (cAliasTot)->(!EOF())
		nRet += (cAliasTot)->VALOR
		(cAliasTot)->(DbSkip())
	EndDo

	(cAliasTot)->(DbCloseArea())
EndIf

Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040C02IV
Fun��o respons�vel pelo c�lculo do valor base de acordo Flat.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040C02IV(cClicod, cCliLoja, dDtIni, dDtFim, cMoeda, cSegmento, nTipo, oModel, cTipoAc)

Local nRet    := 0
Local nVolume := 0
Local nX      := 1
Local aItens  := {}
Local cNumId  := ''
Local cIdItem := ''
Local cNumSeq := ''
Local nQtde   := 0

Default nTipo     := 1
Default cCliCod   := ''
Default cCliLoja  := ''
Default cMoeda    := ''
Default cSegmento := ''
Default dDtIni    := CToD('  /  /  ')
Default dDtFim    := CToD('  /  /  ')
Default oModel    := Nil
Default cTipoAc   := ''

aItens := TA040SeG3Q(cMoeda, dDtIni, dDtFim, cCliCod, cCliLoja, cSegmento) // Flat / Management

For nX := 1 To Len(aItens)

	//+------------------------------------------------------
	//| Validando G6N_OK 
	//+------------------------------------------------------
	If !Empty(cTipoAc) .And. cTipoAc == 'C02'
		cNumId  := aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMID' })][2]
		cIdItem := aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_IDITEM'})][2]
		cNumSeq := aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_NUMSEQ'})][2]
		If oModel:SeekLine({{'G6N_REGVEN', cNumId}, {'G6N_ITVEND', cIdItem}, {'G6N_SEQIV', cNumSeq}})
			If !oModel:GetValue('G6N_OK') 
				Loop
			EndIf
		EndIf
	EndIf 		 		

	//+------------------------------------------------------
	//| Soma: 			1=Emissao;3=Reemissao
	//| Subtrai:		2=Reembolso;
	//| Desconsidera:	6=Cancelamento;5=Reg.Alteracao;
	//|					4=Venda Servi�os Proprios
	//+------------------------------------------------------
	nQtde++
	Do Case
		Case aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_OPERAC'})][2] $ '13'
			nVolume += aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_VLRSER'})][2]
		Case aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_OPERAC'})][2] == '2'
			nVolume -= aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_VLRSER'})][2]
	EndCase
Next

If nTipo == 1 		// Volume
	nRet := nVolume
ElseIf nTipo = 2		// Quantidade
	nRet := nQtde
EndIf

Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040C03IV
Fun��o respons�vel pelo c�lculo do valor base de acordo com o tipo de retorno:	1=Volume;2=Quantidade.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040C03IV(cClicod, cCliLoja, dDtIni, dDtFim, cMoeda, cSegmento, nTipo)

Local nRet    := 0
Local nVolume := 0
Local nX      := 1
Local aItens  := {}

Default nTipo     := 1
Default cCliCod   := ''
Default cCliLoja  := ''
Default cMoeda    := ''
Default cSegmento := ''
Default dDtIni    := CToD('  /  /  ')
Default dDtFim    := CToD('  /  /  ')

aItens := TA040SeG3Q(cMoeda, dDtIni ,dDtFim ,cCliCod ,cCliLoja ,cSegmento) // Management

For nX := 1 To Len(aItens)

	//+------------------------------------------------------
	//| Soma: 			1=Emissao;3=Reemissao
	//| Subtrai:		2=Reembolso;
	//| Desconsidera:	6=Cancelamento;5=Reg.Alteracao;
	//|					4=Venda Servi�os Proprios
	//+------------------------------------------------------
	Do Case
		Case aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_OPERAC'})][2] $ '13'
			nVolume += aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_VLRSER'})][2]
		Case aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_OPERAC'})][2] == '2'
			nVolume -= aItens[nX][AScan(aItens[nX],{|x, y| x[1] == 'G3Q_VLRSER'})][2]
	EndCase
Next

If nTipo == 1		// Volume
	nRet := nVolume
ElseIf nTipo = 2	// Quantidade
	nRet := Len(aItens)
EndIf

Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XGAT
Fun��o chamada atrav�s de gatilho de campos.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040XGAT(cCampo, nFolder)

Local aSaveRows := FwSaveRows()
Local oModel    := FwModelActive()
Local cTipoAc   := oModel:GetValue('G6M_DETAIL', 'G6M_TIPOAC')

Default nFolder := 1

If ( FWIsInCallStack("TURA040") ) 

	If nFolder == 2 // Receita de Vendas
		cTipoAc := 'V01'
	EndIf
	
	Do Case
		Case cCampo == 'G80_PERC'
			TA040XCP05(oModel)
			TA040XCP40(oModel)
	
		Case cCampo == 'G80_TPENT'
			TA040XCP41(oModel)
	
		Case cCampo == 'G80_ITENT'
			TA040XCP42(oModel)
	
		Case cCampo == 'G48_OK'
			If cTipoAc $ 'C01|C06|C12|C07|C10'		// Transaction | Taxa Agencia | Repasse | Taxa Reembolso
				TA040XCP32(oModel)
				TA040XCP33(oModel)
			ElseIf cTipoAc == 'C05'				// Combo
				TA040XCP27(oModel)
				TA040XCP46(oModel)
				
				If nFolder == 1
					TA040XCP48(oModel)
				EndIf
			Else
				TA040XCP28(oModel)
			EndIf
	
		Case cCampo == 'G6N_OK'
			If cTipoAc == 'C02'  			// Flat 
				TA040XCP44(oModel, cCampo)
			ElseIf cTipoAc == 'C04'			// Success
				TA040XCP45(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6N_VALMAX'
			If cTipoAc == 'C04'  			// Success 
				TA040XCP20(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6N_VALOR'
			If cTipoAc == 'C04'				// Success
				TA040XCP19(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6N_SEQIV'
			If cTipoAc == 'C02'				// Flat
				TA040XCP17(oModel, cCampo)
			ElseIf cTipoAc == 'C04'			// Success
				TA040XCP25(oModel, cCampo)
				TA040XCP18(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6P_FORMUL'
			TA040XCP16(oModel, cCampo)
	
		Case cCampo == 'G6P_VALOR'
			TA040XCP16(oModel, cCampo)
			TA040XCP06(oModel, cCampo)
	
		Case cCampo == 'G6P_QTDADE'
			TA040XCP06(oModel, cCampo)
	
		Case cCampo == 'G6P_TOTAL'
			If cTipoAc == 'C03' 				// Management
				TA040XCP30(oModel, cCampo)
				TA040XCP12(oModel, cCampo)
				TA040XCP31(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6O_VAPLIC'
			If cTipoAc $ 'C01|C05|C06|C12|C07|C10'		// Transaction | Combo Taxa Agencia | Repasse | Taxa Reembolso
				TA040XCP29(oModel)
			EndIf
			TA040XCP07(oModel, cCampo)
	
		Case cCampo == 'G6O_BASAPU'
			If cTipoAc == 'C02'					// Flat
				TA040XCP04(oModel, cCampo)
			ElseIf cTipoAc == 'C03' 			// Management
				TA040XCP26(oModel, cCampo)
				TA040XCP12(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6O_CODREV'
			TA040XCP03(oModel, cCampo)
	
		Case cCampo == 'G6O_VLBASE'
			If cTipoAc == 'C02'				// Flat
				TA040XCP13(oModel, cCampo)
				TA040XCP02(oModel, cCampo)
			ElseIf cTipoAc == 'C04'			// Success
				TA040XCP22(oModel, cCampo)
			ElseIf cTipoAc $ 'C01|C05|C06|C12|C07|C10'		// Transaction | Combo Taxa Agencia | Repasse | Taxa Reembolso
				TA040XCP29(oModel)
			EndIf
	
		Case cCampo == 'G6O_VALOR'
			If cTipoAc == 'C02'				// Flat
				TA040XCP02(oModel, cCampo)
			ElseIf cTipoAc == 'C03'			// Management
				TA040XCP24(oModel, cCampo)
			ElseIf cTipoAc == 'C04'			// Success
				TA040XCP23(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6O_TARMAX'
			If cTipoAc == 'C04'				// Success
				TA040XCP21(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6O_TARAPL'
			If cTipoAc == 'C04'				// Success
				TA040XCP21(oModel, cCampo)
				TA040XCP23(oModel, cCampo)
			EndIf
	
		Case cCampo == 'G6M_PERDES'
			TA040XCP11(oModel)
	
		Case cCampo == 'G6M_VLDESC'
			TA040XCP10(oModel)
			TA040XCP01(oModel)
	
		Case cCampo == 'G6M_PERCTX'
			TA040XCP09(oModel)
	
		Case cCampo == 'G6M_VLTXAD'
			TA040XCP08(oModel)
			TA040XCP01(oModel)
	
		Case cCampo == 'G6M_VLACD'
			If ( oModel:GetValue('G6M_DETAIL', 'G6M_PERDES') > 0 )
				IIF(!FwIsInCallStack('TA50MarkAll') .And. !FwIsInCallStack('TA040RaDel'), MsgInfo(STR0025, STR0146), ) 	// "O Vlr. Descont. ser� recalculado com base no % Desconto."###"Rec�lculo Desconto"
				TA040XCP11(oModel)
			EndIf
			If ( oModel:GetValue('G6M_DETAIL', 'G6M_PERCTX') > 0 )
				IIF(!FwIsInCallStack('TA50MarkAll') .And. !FwIsInCallStack('TA040RaDel'), MsgInfo(STR0026, STR0147), ) 	// "O Vlr. Tx. Ad. ser� recalculado com base no % Tx. Adic.."###"Rec�lculo Taxa Adicional"
				TA040XCP09(oModel)
			EndIf
			TA040XCP01(oModel)
			
			// atualiza o rateio
			If oModel:GetOperation() == MODEL_OPERATION_UPDATE
				TA040XCP47(oModel)
			EndIf
	
		Case cCampo == 'G82_TOTAL'
			TA040XCP34(oModel)
	
		Case cCampo == 'G82_PERDES'
			TA040XCP35(oModel)
	
		Case cCampo == 'G82_VLDESC'
			TA040XCP37(oModel)
			TA040XCP38(oModel)
	
		Case cCampo == 'G82_PERCTX'
			TA040XCP36(oModel)
	
		Case cCampo == 'G82_VLTXAD'
			TA040XCP39(oModel)
			TA040XCP38(oModel)
	
		Case cCampo == 'G82_VLPRD'
			If ( oModel:GetValue('G82_DETAIL', 'G82_PERDES') > 0 )
				MsgInfo(STR0025) // "O Vlr. Descont. ser� recalculado com base no % Desconto."
				TA040XCP35(oModel)
			EndIf
			If ( oModel:GetValue('G82_DETAIL', 'G82_PERCTX') > 0 )
				MsgInfo(STR0026) // "O Vlr. Tx. Ad. ser� recalculado com base no % Tx. Adic.."
				TA040XCP36(oModel)
			EndIf
			TA040XCP38(oModel)
	
		Case cCampo == 'G6M_TOTAL'
			TA040XCP14(oModel)
			TA040XCP43(oModel)
	
			// atualiza o rateio
			If oModel:GetOperation() == MODEL_OPERATION_UPDATE
				TA040XCP47(oModel)
			EndIf

		Case cCampo == 'G6L_TOTACD'
			TA040XCP15(oModel)
	
		Case cCampo == 'G6L_TOTPRD'
			TA040XCP15(oModel)

		Case cCampo == 'G48_PERACD'
			TA040XCP49(oModel)

	EndCase

ElseIf ( FWIsInCallStack("TURA050") ) 

	TA050XGAT(oModel,cCampo)

EndIf
FwRestRows(aSaveRows)

Return Nil

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP50
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC

@type 		Function
@author 	Thiago Tavares
@since 		11/01/2017
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP50(oModel, nVlrNew, nVlrOld)

Local oMdlG48  := oModel:GetModel('G48A_DETAIL')
Local nVlrApl  := oModel:GetModel('G6O_DETAIL'):GetValue('G6O_VAPLIC') 

If oMdlG48:GetValue('G48_OK')
	If oMdlG48:GetValue('G48_TPACD') == '1'
		oModel:GetModel('G6O_DETAIL' ):SetValue('G6O_VAPLIC', nVlrApl + (nVlrNew - nVlrOld))
	ElseIf oMdlG48:GetValue('G48_TPACD') == '2'
		oModel:GetModel('G6O_DETAIL' ):SetValue('G6O_VAPLIC', nVlrApl - (nVlrNew - nVlrOld))
	EndIf
	
	oMdlG48:LoadValue('G48_ATUIF', '1')
	
	aAdd(aG48Dif, {oMdlG48:GetValue('G48_NUMID'), oMdlG48:GetValue('G48_IDITEM'), oMdlG48:GetValue('G48_NUMSEQ'), nVlrOld, nVlrNew - nVlrOld, oMdlG48:GetValue('G48_CLASS'), oMdlG48:GetValue('G48_FILIAL')})
EndIf

Return 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP49
Fun��o respons�vel pela atualiza��o do campo G48_VLACD e G6O_VAPLIC

@type 		Function
@author 	Thiago Tavares
@since 		11/01/2017
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP49(oModel)

Local oMdlG48  := oModel:GetModel('G48A_DETAIL')
Local nVlrApl  := oModel:GetModel('G6O_DETAIL'):GetValue('G6O_VAPLIC') 
Local nVlrOld  := oMdlG48:GetValue('G48_VLACD')
Local nVlrNew  := (oMdlG48:GetValue('G48_PERACD') * oMdlG48:GetValue('G48_VLBASE')) / 100

If oMdlG48:GetValue('G48_OK')
	If oMdlG48:GetValue('G48_TPACD') == '1'
		oModel:GetModel('G6O_DETAIL' ):SetValue('G6O_VAPLIC', nVlrApl + (nVlrNew - nVlrOld))
	ElseIf oMdlG48:GetValue('G48_TPACD') == '2'
		oModel:GetModel('G6O_DETAIL' ):SetValue('G6O_VAPLIC', nVlrApl - (nVlrNew - nVlrOld))
	EndIf
	
	If !oMdlG48:CanSetValue('G48_VLACD')
		oModel:GetModel('G48A_DETAIL'):GetStruct():SetProperty('G48_VLACD', MODEL_FIELD_WHEN, {|| .T.})
	EndIf
	
	oMdlG48:SetValue('G48_VLACD', nVlrNew)
	oMdlG48:LoadValue('G48_ATUIF', '1')
	
	aAdd(aG48Dif, {oMdlG48:GetValue('G48_NUMID'), oMdlG48:GetValue('G48_IDITEM'), oMdlG48:GetValue('G48_NUMSEQ'), nVlrOld, nVlrNew - nVlrOld, oMdlG48:GetValue('G48_CLASS'), oMdlG48:GetValue('G48_FILIAL')})
EndIf

Return 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP48
Fun��o respons�vel pela marcar/desmarcar o campo G48_OK que for acordo de COMBO (C05).

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP48(oModel)

Local oMdlG48 := oModel:GetModel('G48A_DETAIL')
Local cNumId  := oMdlG48:GetValue('G48_NUMID') 
Local lOk     := oMdlG48:GetValue('G48_OK')
Local nX      := 0

For nX := 1 To oMdlG48:Length()
	oMdlG48:GoLine(nX)
	If oMdlG48:GetValue('G48_NUMID') == cNumId
		oMdlG48:SetValue('G48_OK', lOk)
	EndIf	
Next nX

Return 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP47
Fun��o respons�vel pela atualiza��o do campo G80_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP47(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := Nil
Local oModelG80 := Nil
Local oModelG82 := Nil
Local nTotal    := 0
Local nPerc     := 0
Local nValor    := 0
Local nPerDes   := 0
Local nPercTx   := 0
Local nX        := 0
Local lOk       := .T.
Local oView     := FwViewActive()
Local nFolder   := oView:GetFolderActive('F_ALL',2)[1]

If nFolder == 1 // Receita de ACordos
	oModelG6M := oModel:GetModel('G6M_DETAIL')
	oModelG80 := oModel:GetModel('G80A_DETAIL')
ElseIf nFolder == 2 // Receita de Vendas
	oModelG82 := oModel:GetModel('G82_DETAIL')
	oModelG80 := oModel:GetModel('G80B_DETAIL')
EndIf
	
If !oModelG80:IsEmpty()
	For nX := 1 To oModelG80:Length()
		oModelG80:GoLine(nX)
		
		If nFolder == 1 // Receita de ACordos
			nTotal    := oModelG6M:GetValue('G6M_VLACD')
			nPerDes   := oModelG6M:GetValue('G6M_PERDES')
			nPercTx   := oModelG6M:GetValue('G6M_PERCTX')
		ElseIf nFolder == 2 // Receita de Vendas
			nTotal    := oModelG82:GetValue('G82_VLPRD')
			nPerDes   := oModelG82:GetValue('G82_PERDES')
			nPercTx   := oModelG82:GetValue('G82_PERCTX')
		EndIf
		
		nPerc  := oModelG80:GetValue('G80_PERC')
		nValor := nTotal * (nPerc/100)
		nTotal := nValor - (nValor * nPerDes / 100) + (nValor * nPercTx / 100)
	
		If !(lOk := oModelG80:SetValue('G80_VALOR', nValor))
			Help( , , "TA040XCP47", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
		EndIf
		
		If !(lOk := oModelG80:SetValue('G80_PERDES', nPerDes))
			Help( , , "TA040XCP47", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
		EndIf
		
		If !(lOk := oModelG80:SetValue('G80_PERCTX', nPercTx))
			Help( , , "TA040XCP47", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
		EndIf
		
		If !(lOk := oModelG80:SetValue('G80_VLRLIQ', nTotal))
			Help( , , "TA040XCP47", , STR0056, 1, 0) // "Erro ao calcular Total do Rateio."
		EndIf
	Next nX
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP46
Fun��o respons�vel pela atualiza��o do campo G6O_VALOR.

@type 		Function
@author 	Thiago Tavares
@since 		11/04/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP46(oModel, aDetalhes)

Local aSaveRows := FwSaveRows()
Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local cChave    := oModelG48:GetValue('G48_CHAVEC')
Local lOk       := oModelG48:GetValue('G48_OK')
Local nVlrAtu   := oModelG6O:GetValue('G6O_VALOR')
Local cTipoVl   := oModelG6O:GetValue('G6O_TIPOVL')
Local nX        := 0
Local nValor    := 0

Default aDetalhes	:= {}

If cTipoVl == '1' // Porcentagem
	If Len(aDetalhes) > 0
		nValor := aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2]
	Else
		If oModelG48:GetValue('G48_CHAVEC') == cChave
			If lOk
				nValor += oModelG48:GetValue('G48_VLACD')
			Else
				nValor -= oModelG48:GetValue('G48_VLACD')
			EndIf
		EndIf
	EndIf
	
	If !(lOk := oModelG6O:SetValue('G6O_VALOR', nVlrAtu + nValor))
		Help( , , "TA040XCP46", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
	EndIf
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP45
Fun��o respons�vel pela atualiza��o dos campos G6O_TARAPL e G6O_TARMAX.

@type 		Function
@author 	Thiago Tavares
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP45(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local nValor    := oModelG6O:GetValue('G6O_TARAPL')
Local nValorMax := oModelG6O:GetValue('G6O_TARMAX')
Local nX        := 0
Local lOk       := .T.

If oModelG6N:GetValue('G6N_OK')
	nValor    += oModelG6N:GetValue('G6N_VALOR')
	nValorMax += oModelG6N:GetValue('G6N_VALMAX')
Else
	nValor    -= oModelG6N:GetValue('G6N_VALOR')
	nValorMax -= oModelG6N:GetValue('G6N_VALMAX')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_TARAPL', nValor))
	Help( , , "TA040XCP45", , STR0047, 1, 0) // "Erro ao atualizar Tarifa Aplicada."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_TARMAX', nValorMax))
	Help( , , "TA040XCP45", , STR0046, 1, 0) // "Erro ao atualizar Tarifa Cheia."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP44
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP44(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_BASAPU') $ '12'		//	1=Volume;2=Quantidade
	xValor := TA040C02IV(oModelG6L:GetValue('G6L_CLIENT'), ;
	                     oModelG6L:GetValue('G6L_LOJA'), ;
	                     oModelG6L:GetValue('G6L_DTINI'), ;
	                     oModelG6L:GetValue('G6L_DTFIM'), ;
	                     oModelG6L:GetValue('G6L_MOEDA'), ;
	                     oModel:GetValue('G6M_DETAIL', 'G6M_SEGNEG'), ;
	                     Val(oModelG6O:GetValue('G6O_BASAPU')), ;
	                     oModelG6N, 'C02')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VLBASE', xValor))
	Help( , , "TA040XCP44", , STR0038, 1, 0) // "Erro ao calcular Valor Base do Item."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP43
Fun��o respons�vel pelo aviso o usu�rio caso o rateio precise ser alterado.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP43(oModel)

Local aSaveRows := FwSaveRows()
Local lRateio   := .F.
Local oView     := Nil

If oModel:GetValue('G6M_DETAIL', 'G6M_RATPAD') == '1'
	lRateio := MsgYesNo(STR0027) // "Valor total do acordo alterado. Deseja recalcular rateio padr�o? Recomendado o rec�lculo do rateio ap�s a altera��o do �ltimo item."
EndIf

If lRateio
	oView := FwViewActive()
	TA040Rat(oView)
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP42
Fun��o respons�vel pela atualiza��o do campo G80_PERC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP42(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG80 := Nil
Local lOk       := .T.
Local oView     := FwViewActive()
Local nFolder   := oView:GetFolderActive('F_ALL', 2)[1]
Local cDescr    := ''

If nFolder == 1 // Receita de Acordos
	oModelG80	:= oModel:GetModel('G80A_DETAIL')
ElseIf nFolder == 2 // Receita de Vendas
	oModelG80	:= oModel:GetModel('G80B_DETAIL')
EndIf

cDescr	:= Posicione('G3G',1, xFilial('G3G') + oModel:GetValue('G6L_MASTER', 'G6L_CLIENT') + ; 
								                  oModel:GetValue('G6L_MASTER', 'G6L_LOJA') + ;
								                  oModelG80:GetValue('G80_TPENT') + ;
								                  oModelG80:GetValue('G80_ITENT'), ;
													'G3G_DESCR')	// G3G_FILIAL+G3G_CLIENT+G3G_LOJA+G3G_TIPO+G3G_ITEM


If !(lOk := oModelG80:SetValue('G80_DESCR', cDescr))
	Help( , , "TA040XCP42", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

If !(lOk := oModelG80:SetValue('G80_PERC', 0))
	Help( , , "TA040XCP42", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP41
Fun��o respons�vel pela atualiza��o do campo G80_ITENT.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP41(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG80 := Nil
Local lOk       := .T.
Local cDescr    := ''
Local oView     := FwViewActive()
Local nFolder   := oView:GetFolderActive('F_ALL',2)[1]

If nFolder == 1 // Receita de Acordos
	oModelG80	:= oModel:GetModel('G80A_DETAIL')
ElseIf nFolder == 2	// Receita de Vendas
	oModelG80	:= oModel:GetModel('G80B_DETAIL')
EndIf

cDescr := Posicione('G3E', 1, xFilial('G3E') + oModelG80:GetValue('G80_TPENT'), 'G3E_DESCR')

If !(lOk := oModelG80:SetValue('G80_DESENT', cDescr))
	Help( , , "TA040XCP41", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

If !(lOk := oModelG80:SetValue('G80_ITENT', Space(TamSx3('G80_ITENT')[1])))
	Help( , , "TA040XCP41", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP40
Fun��o respons�vel pela atualiza��o do campo G6M_RATPAD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP40(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local lOk       := .T.
Local oView     := FwViewActive()
Local nFolder   := oView:GetFolderActive('F_ALL',2)[1]

If nFolder == 1 // Receita de Acordos
	If !(lOk := oModelG6M:SetValue('G6M_RATPAD', '2'))
		Help( , , "TA040XCP40", , STR0029, 1, 0) // "Erro ao atualizar controle Rateio Padr�o"
	EndIf
ElseIf nFolder == 2 // Receita de Vendas
	If !(lOk := oModelG82:SetValue('G82_RATPAD', '2'))
		Help( , , "TA040XCP40", , STR0029, 1, 0) // "Erro ao atualizar controle Rateio Padr�o"
	EndIf
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP39
Fun��o respons�vel pela atualiza��o do campo G82_PERCTX.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP39(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local xValor    := (100 * oModelG82:GetValue('G82_VLTXAD')) / oModelG82:GetValue('G82_VLPRD')
Local lOk       := .T.

If !(lOk := oModelG82:SetValue('G82_PERCTX', xValor))
	Help( , , "TA040XCP39", , STR0030, 1, 0) // "Erro ao calcular Percentual do Desconto do Produto."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP38
Fun��o respons�vel pela atualiza��o do campo G82_TOTAL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP38(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local xValor    := oModelG82:GetValue('G82_VLPRD') - oModelG82:GetValue('G82_VLDESC') + oModelG82:GetValue('G82_VLTXAD')
Local lOk       := .T.

If !(lOk := oModelG82:SetValue('G82_TOTAL', xValor))
	Help( , , "TA040XCP38", , STR0031, 1, 0) // "Erro ao atualizar G82_TOTAL."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP37
Fun��o respons�vel pela atualiza��o do campo G82_PERDES.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP37(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local xValor    := (100 * oModelG82:GetValue('G82_VLDESC')) / oModelG82:GetValue('G82_VLPRD')
Local lOk       := .T.

If !(lOk := oModelG82:SetValue('G82_PERDES', xValor))
	Help( , , "TA040XCP37", , STR0030, 1, 0) // "Erro ao calcular Percentual do Desconto do Produto."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP36
Fun��o respons�vel pela atualiza��o do campo G82_VLTXAD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP36(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local xValor    := (oModelG82:GetValue('G82_VLPRD') * oModelG82:GetValue('G82_PERCTX')) / 100
Local lOk       := .T.

If !(lOk := oModelG82:SetValue('G82_VLTXAD', xValor))
	Help( , , "TA040XCP36", , STR0032, 1, 0) // "Erro ao calcular Valor da Taxa Adicional do Produto."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP35
Fun��o respons�vel pela atualiza��o do campo G82_VLDESC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP35(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local xValor    := (oModelG82:GetValue('G82_VLPRD') * oModelG82:GetValue('G82_PERDES')) / 100
Local lOk       := .T.

If !(lOk := oModelG82:SetValue('G82_VLDESC', xValor))
	Help( , , "TA040XCP35", , STR0033, 1, 0) // "Erro ao calcular Valor do Desconto do Produto."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP34
Fun��o respons�vel pela atualiza��o do campo G6L_TOTPRD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP34(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local nValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG82:Length()
	oModelG82:GoLine(nX)
	If ! (oModelG82:IsDeleted())
		nValor += oModelG82:GetValue('G82_TOTAL')
	EndIf
Next

If !(lOk := oModelG6L:SetValue('G6L_TOTPRD', nValor))
	Help( , , "TA040XCP34", , STR0034, 1, 0) // "Erro ao totalizar Valor Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP33
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP33(oModel,aDetalhes)

Local aSaveRows := FwSaveRows()
Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local nValor    := 0
Local lOk       := oModelG48:GetValue('G48_OK')
Local nVlBase   := oModelG6O:GetValue('G6O_VLBASE')
Local cTipoVl   := oModelG6O:GetValue('G6O_TIPOVL')
Local nX        := 0

Default aDetalhes := {}

//+-----------------------------------------------------
// Se Tipo for Valor fixo, n�o atualiza campo G6O_VLBASE
//+-----------------------------------------------------
If cTipoVl == '1' // Porcentagem
	If Len(aDetalhes) > 0
		G48->(DbGoTo(aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_RECNO'}))][2]))
		If G48->(!EOF())
			nValor := G48->G48_VLBASE
		EndIf
	Else
		If lOk
			nValor += oModelG48:GetValue('G48_VLBASE')
		Else
			nValor -= oModelG48:GetValue('G48_VLBASE')
		EndIf
	EndIf

	If !(lOk := oModelG6O:SetValue('G6O_VLBASE', nVlBase + nValor))
		Help( , ,"TA040XCP33", , STR0035, 1, 0) // "Erro ao atualizar Valor Base do Acordo."
	EndIf
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP32
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP32(oModel, aDetalhes)

Local aSaveRows := FwSaveRows()
Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local nValor    := 0
Local lOk       := oModelG48:GetValue('G48_OK')
Local cTpAcd    := oModelG48:GetValue('G48_TPACD')
Local nVAplic   := oModelG6O:GetValue('G6O_VAPLIC')
Local nX        := 0

Default aDetalhes	:= {}

If !oModelG6O:IsDeleted(oModelG6O:GetLine())
	If Len(aDetalhes) > 0
		If aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_TPACD'}))][2] == '1' 			// receita
			nValor := aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2]
		ElseIf aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_TPACD'}))][2] == '2' 		// abatimento de receita
			nValor := -(aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2])
		EndIf
	Else
		If lOk
			If cTpAcd == '1'				// receita 
				nValor += oModelG48:GetValue('G48_VLACD')
			ElseIf cTpAcd == '2'			// abatimento de receita
				nValor -= oModelG48:GetValue('G48_VLACD')
			EndIf
		Else
			If cTpAcd == '1'				// receita 
				nValor -= oModelG48:GetValue('G48_VLACD')
			ElseIf cTpAcd == '2'			// abatimento de receita
				nValor += oModelG48:GetValue('G48_VLACD')
			EndIf
		EndIf
	EndIf
	
	If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', nVAplic + nValor))
		Help( , , "TA040XCP32", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
	EndIf
EndIf 

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP31
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP31(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local oModelG6P := oModel:GetModel('G6P_DETAIL')
Local xRet      := oModelG6P:GetValue(cCampo)
Local nX        := 0
Local nVAplic   := 0
Local xValor    := 0
Local lOk       := .T.

For nX := 1 To oModelG6P:Length()
	oModelG6P:GoLine(nX)

	If ! (oModelG6P:IsDeleted())
		xValor += oModelG6P:GetValue('G6P_TOTAL')
	EndIf
Next

nVAplic := xValor + oModelG6O:GetValue('G6O_VALOR')

If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', nVAplic))
	Help( , , "TA040XCP31", , STR0037, 1, 0) // "Erro ao atualizar Vlr Final."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP30
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP30(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local oModelG6P := oModel:GetModel('G6P_DETAIL')
Local xRet      := oModelG6P:GetValue(cCampo)
Local xValor    := 0
Local nX        := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_BASAPU') == '3' 	// Custo
	For nX := 1 To oModelG6P:Length()
		oModelG6P:GoLine(nX)

		If ! (oModelG6P:IsDeleted())
			xValor += oModelG6P:GetValue('G6P_TOTAL')
		EndIf
	Next

ElseIf oModelG6O:GetValue('G6O_BASAPU') == '1' 	// Volume
	xValor := TA040C02IV(oModelG6L:GetValue('G6L_CLIENT'), ;
	                     oModelG6L:GetValue('G6L_LOJA'), ;
	                     oModelG6L:GetValue('G6L_DTINI'), ;
	                     oModelG6L:GetValue('G6L_DTFIM'), ;
	                     oModelG6L:GetValue('G6L_MOEDA'), ;
	                     oModel:GetValue('G6M_DETAIL', 'G6M_SEGNEG'), ;
	                     Val(oModelG6O:GetValue('G6O_BASAPU')))
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VLBASE', xValor))
	Help( , , "TA040XCP30", , STR0038, 1, 0) 		//	"Erro ao calcular Valor Base do Item."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP29
Fun��o respons�vel pela atualiza��o do campo G6O_VALOR G6O_PERCEN.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP29(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local cTpVal    := oModelG6O:GetValue('G6O_TIPOVL')
Local nValor    := 0
Local lOk       := .T.

If cTpVal == '2' // Valor Fixo
	nValor := (oModelG6O:GetValue('G6O_VAPLIC'))
	If !(lOk := oModelG6O:SetValue('G6O_VALOR', nValor))
		Help( , , "TA040XCP29", , STR0039 + oModelG6O:GetValue('G6O_CODACD'), 1, 0) // "Erro ao atualizar Valor do Acordo. "
	EndIf
ElseIf cTpVal == '1' .And. !FwIsInCallStack('TA040RaDel') 	// Porcentagem
	nValor := (oModelG6O:GetValue('G6O_VAPLIC') / oModelG6O:GetValue('G6O_VLBASE')) * 100
	If !(lOk := oModelG6O:SetValue('G6O_PERCEN', nValor * IIF(nValor < 0, -1, 1)))
		Help( , , "TA040XCP29", , STR0039 + oModelG6O:GetValue('G6O_CODACD'), 1, 0) // "Erro ao atualizar Valor do Acordo. "
	EndIf
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP28
Fun��o respons�vel pela atualiza��o do campo G82_VLPRD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP28(oModel,aDetalhes)

Local aSaveRows  := FwSaveRows()
Local oModelG48B := oModel:GetModel('G48B_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local nValor     := 0
Local lOk        := oModelG48B:GetValue('G48_OK')
Local cTpAcd     := oModelG48B:GetValue('G48_TPACD')
Local nVlPrd     := oModelG82:GetValue('G82_VLPRD ')
Local nX         := 0

Default aDetalhes	:= {}

If !oModelG82:IsDeleted(oModelG82:GetLine())
	If Len(aDetalhes) > 0
		If aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_TPACD'}))][2] == '1' 			// receita
			nValor := aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2]
		ElseIf aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_TPACD'}))][2] == '2' 		// abatimento de receita
			nValor := -(aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2])
		EndIf
	Else
		If lOk
			If cTpAcd == '1'				// receita 
				nValor += oModelG48B:GetValue('G48_VLACD')
			ElseIf cTpAcd == '2'			// abatimento de receita
				nValor -= oModelG48B:GetValue('G48_VLACD')
			EndIf
		Else
			If cTpAcd == '1'				// receita 
				nValor -= oModelG48B:GetValue('G48_VLACD')
			ElseIf cTpAcd == '2'			// abatimento de receita
				nValor += oModelG48B:GetValue('G48_VLACD')
			EndIf
		EndIf
	EndIf
	
	If !(lOk := oModelG82:SetValue('G82_VLPRD', nVlPrd + nValor))
		Help( , , "TA040XCP28", , STR0040, 1, 0) // "Erro ao atualizar Valor Total do produto."
	EndIf
EndIf
	
FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP27
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP27(oModel, aDetalhes)

Local aSaveRows := FwSaveRows()
Local oModelG48 := oModel:GetModel('G48A_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local nValor    := 0
Local cChave    := oModelG48:GetValue('G48_CHAVEC')
Local lOk       := oModelG48:GetValue('G48_OK')
Local nVAplic   := oModelG6O:GetValue('G6O_VAPLIC')
Local nX        := 0

Default aDetalhes	:= {}

If Len(aDetalhes) > 0
	nValor := aDetalhes[(AScan(aDetalhes,{|x, y| x[1] == 'G48_VLACD'}))][2]
Else
	If oModelG48:GetValue('G48_CHAVEC') == cChave
		If lOk
			nValor += oModelG48:GetValue('G48_VLACD')
		Else
			nValor -= oModelG48:GetValue('G48_VLACD')
		EndIf
	EndIf
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', nVAplic + nValor))
	Help( , , "TA040XCP27", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP26
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP26(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_BASAPU') $ '12'		//	1=Volume;2=Quantidade
	xValor := TA040C03IV(oModelG6L:GetValue('G6L_CLIENT'), ; 
	                     oModelG6L:GetValue('G6L_LOJA'), ;
	                     oModelG6L:GetValue('G6L_DTINI'), ;
	                     oModelG6L:GetValue('G6L_DTFIM'), ;
	                     oModelG6L:GetValue('G6L_MOEDA'), ;
	                     oModel:GetValue('G6M_DETAIL', 'G6M_SEGNEG'), ; 
	                     Val(oModelG6O:GetValue('G6O_BASAPU')))
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VLBASE', xValor))
	Help( , , "TA040XCP26", , STR0038, 1, 0) // "Erro ao calcular Valor Base do Item."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP25
Fun��o respons�vel pela atualiza��o do campo G6N_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP25(oModel, cCampo)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

G3Q->(DbSetOrder(1))	// G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM+G3Q_NUMSEQ
G3Q->(DbGoTop())

If G3Q->(DbSeek( xFilial('G3Q') + oModelG6N:GetValue('G6N_REGVEN') + oModelG6N:GetValue('G6N_ITVEND') + oModelG6N:GetValue('G6N_SEQIV')))
	xValor	:= G3Q->G3Q_TARIFA + G3Q->G3Q_MARKUP
EndIf

If !(lOk := oModelG6N:SetValue('G6N_VALOR', xValor))
	Help( , , "TA040XCP25", , STR0041, 1, 0) // "Erro ao calcular Valor do acordo aplicado ao Item."
EndIf

RestArea(aArea)
FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP24
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP24(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local nVAplic   := 0
Local lOk       := .T.

nVAplic := oModelG6O:GetValue('G6O_VALOR')

If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', nVAplic))
	Help( , , "TA040XCP24", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP23
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP23(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_TIPOVL') == '1' 		// Porcentagem
	xValor := oModelG6O:GetValue('G6O_TARAPL') * (oModelG6O:GetValue('G6O_VALOR') / 100)
ElseIf oModelG6O:GetValue('G6O_TIPOVL') == '2'	// Fixo
	xValor := oModelG6O:GetValue('G6O_VALOR')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', xValor))
	Help( , , "TA040XCP23", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP22
Fun��o respons�vel pela atualiza��o do campo G6O_VALOR - G5Y_ITEM - G5Y_FXINI - G5Y_FXFIM.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP22(oModel, cCampo)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local cFaixa    := ''
Local nFxDe     := 0
Local nFxAte    := 0
Local nValor    := 0
Local nVlBase   := 0
Local cSeekG60  := ''
Local lOk       := .T.

cSeekG60 := xFilial('G60') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV')
nVlBase  := oModelG6O:GetValue('G6O_VLBASE')

G60->(DbSetOrder(1))
G60->(DbGoTop())

If G60->(DbSeek( cSeekG60 ))
	While G60->(!EOF()) .And. G60->(G60_FILIAL + G60_CODACO + G60_CODREV ) == cSeekG60
		If nVlBase >= G60->G60_PERCDE .And. nVlBase <= G60->G60_PERCAT
			nValor := G60->G60_VALOR
			cFaixa := G60->G60_ITEM
			nFxDe  := G60->G60_PERCDE
			nFxAte := G60->G60_PERCAT
			Exit
		EndIf
		G60->(DbSkip())
	EndDo
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VALOR', nValor))
	Help( , , "TA040XCP22", , STR0039 + oModelG6O:GetValue('G6O_CODACD'), 1, 0) // "Erro ao atualizar Valor do Acordo. "
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FAIXA', cFaixa))
	Help( , , "TA040XCP22", , STR0042, 1, 0) // "Erro ao atualizar Faixa do Acordo."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FXDE', nFxDe))
	Help( , , "TA040XCP22", , STR0043, 1, 0) // "Erro ao atualizar Faixa De do Acordo."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FXATE', nFxAte))
	Help( , , "TA040XCP22", , STR0044, 1, 0) // "Erro ao atualizar Faixa At� do Acordo."
EndIf

RestArea(aArea)
FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP21
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP21(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local nValor    := 0
Local lOk       := .T.

nValor := ( (oModelG6O:GetValue('G6O_TARMAX') - oModelG6O:GetValue('G6O_TARAPL') ) / oModelG6O:GetValue('G6O_TARMAX')) * 100

If !(lOk := oModelG6O:SetValue('G6O_VLBASE', nValor))
	Help( , ,"TA040XCP21", , STR0045, 1, 0) // "Erro ao atualizar Valor Base."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP20
Fun��o respons�vel pela atualiza��o do campo G6O_TARMAX.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP20(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local nValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG6N:Length()
	oModelG6N:GoLine(nX)
	If ! (oModelG6N:IsDeleted())
		nValor += oModelG6N:GetValue('G6N_VALMAX')
	EndIf
Next

If !(lOk := oModelG6O:SetValue('G6O_TARMAX', nValor))
	Help( , , "TA040XCP20", , STR0046, 1, 0) // "Erro ao atualizar Tarifa Cheia."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP19
Fun��o respons�vel pela atualiza��o do campo G6O_TARAPL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP19(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local nValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG6N:Length()
	oModelG6N:GoLine(nX)
	If ! (oModelG6N:IsDeleted())
		nValor += oModelG6N:GetValue('G6N_VALOR')
	EndIf
Next

If !(lOk := oModelG6O:SetValue('G6O_TARAPL', nValor))
	Help( , , "TA040XCP19", , STR0047, 1, 0) // "Erro ao atualizar Tarifa Aplicada."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP18
Fun��o respons�vel pela atualiza��o do campo G6N_VALMAX.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP18(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

xValor := TA040SucTa(oModelG6N:GetValue('G6N_REGVEN'), oModelG6N:GetValue('G6N_ITVEND'), oModelG6N:GetValue('G6N_SEQIV'))

If !(lOk := oModelG6N:SetValue('G6N_VALMAX', xValor))
	Help( , , "TA040XCP18", , STR0048, 1, 0) // "Erro ao calcular Valor Total da Apura��o."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP17
Fun��o respons�vel pela atualiza��o do campo G6N_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP17(oModel, cCampo)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local xRet      := oModelG6N:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

G3Q->(DbSetOrder(1))	// G3Q_FILIAL+G3Q_NUMID+G3Q_IDITEM+G3Q_NUMSEQ
G3Q->(DbGoTop())

If G3Q->(DbSeek( xFilial('G3Q') + oModelG6N:GetValue('G6N_REGVEN') + oModelG6N:GetValue('G6N_ITVEND') + oModelG6N:GetValue('G6N_SEQIV')))
	xValor	:= G3Q->G3Q_VLRSER
EndIf

If !(lOk := oModelG6N:SetValue('G6N_VALOR', xValor))
	Help( , , "TA040XCP17", , STR0041, 1, 0) // "Erro ao calcular Valor do acordo aplicado ao Item."
EndIf

FwRestRows(aSaveRows)
RestArea(aArea)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP16
Fun��o respons�vel pela atualiza��o do campo G6P_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP16(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6P := oModel:GetModel('G6P_DETAIL')
Local xRet      := oModelG6P:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6P:GetValue('G6P_TPCALC') == '2'
	xValor := TA040G6PFo(oModelG6P:GetValue('G6P_FORMUL'))

	If ValType(xValor) == 'N'
		If !(lOk := oModelG6P:SetValue('G6P_VALOR', xValor))
			Help( , ,"TA040XCP16", , STR0049, 1, 0) // "Erro ao atualizar Valor do Item."
		EndIf
	EndIf
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP15
Fun��o respons�vel pela atualiza��o do campo G6L_TOTAL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP15(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local nValor    := 0
Local lOk       := .T.

nValor	:= oModelG6L:GetValue('G6L_TOTACD') + oModelG6L:GetValue('G6L_TOTPRD')

If !(lOk := oModelG6L:SetValue('G6L_TOTAL', nValor))
	Help( , , "TA040XCP15", , STR0048, 1, 0) // "Erro ao calcular Valor Total da Apura��o."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP14
Fun��o respons�vel pela atualiza��o do campo G6L_TOTACD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP14(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local nValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nX)
	If ! (oModelG6M:IsDeleted())
		nValor += oModelG6M:GetValue('G6M_TOTAL')
	EndIf
Next

If !(lOk := oModelG6L:SetValue('G6L_TOTACD', nValor))
	Help( , , "TA040XCP14", , STR0034, 1, 0) // "Erro ao totalizar Valor Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP13
Fun��o respons�vel pela atualiza��o do campo G6O_VALOR - G5Y_ITEM - G5Y_FXINI - G5Y_FXFIM.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP13(oModel, cCampo)

Local aArea     := GetArea()
Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local nValor    := 0
Local cFaixa    := ''
Local nFxDe     := 0
Local nFxAte    := 0
Local nAux      := 0
Local cSeekG5Y  := ''
Local lOk       := .T.

cSeekG5Y := xFilial('G5Y') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV')
nAux     := oModelG6O:GetValue('G6O_VLBASE')

G5Y->(DbSetOrder(1))		// G5Y_FILIAL+G5Y_CODACO+G5Y_CODREV+G5Y_ITEM
G5Y->(DbGoTop())

If G5Y->(DbSeek( cSeekG5Y ))
	While G5Y->(!EOF()) .And. G5Y->(G5Y_FILIAL + G5Y_CODACO + G5Y_CODREV ) == cSeekG5Y
		If nAux >= G5Y->G5Y_FXINI .And. nAux <= G5Y->G5Y_FXFIM
			nValor := G5Y->G5Y_VALOR
			cFaixa := G5Y->G5Y_ITEM
			nFxDe  := G5Y->G5Y_FXINI
			nFxAte := G5Y->G5Y_FXFIM
			Exit
		EndIf
		G5Y->(DbSkip())
	EndDo
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VALOR', nValor))
	Help( , , "TA040XCP13", , STR0039 + oModelG6O:GetValue('G6O_CODACD'), 1, 0) // "Erro ao atualizar Valor do Acordo. "
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FAIXA', cFaixa))
	Help( , , "TA040XCP13", , STR0042, 1, 0) // "Erro ao atualizar Faixa do Acordo."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FXDE', nFxDe))
	Help( , , "TA040XCP13", , STR0043, 1, 0) // "Erro ao atualizar Faixa De do Acordo."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_FXATE', nFxAte))
	Help( , , "TA040XCP13", , STR0044, 1, 0) // "Erro ao atualizar Faixa At� do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP12
Fun��o respons�vel pela atualiza��o do campo G6O_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP12(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local nValor    := 0
Local nPercen   := 0
Local lOk       := .T.
Local cTipo     := ''

cTipoVl := Posicione('G5V', 1, xFilial('G5V') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV'), 'G5V_TPVAL')

If cTipoVl == '1' 		// Porcentagem
	nPercen := Posicione('G5V', 1, xFilial('G5V') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV'), 'G5V_VALOR')
	nValor  := (oModelG6O:GetValue('G6O_VLBASE') * nPercen) / 100
ElseIf cTipoVl == '2'	// Fixo
	nValor  := Posicione('G5V', 1, xFilial('G5V') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV'), 'G5V_VALOR')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_PERCEN',nPercen))
	Help(, , "TA040XCP12", , STR0050, 1, 0) 		// "Erro ao atualizar Valor do Acordo."
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VALOR',nValor))
	Help( , , "TA040XCP12", , STR0050, 1, 0) 		// "Erro ao atualizar Valor do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP11
Fun��o respons�vel pela atualiza��o do campo G6M_VLDESC.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP11(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (oModelG6M:GetValue('G6M_VLACD') * oModelG6M:GetValue('G6M_PERDES')) / 100
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_VLDESC', xValor))
	Help( , , "TA040XCP11", , STR0051, 1, 0) // "Erro ao calcular Valor do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP10
Fun��o respons�vel pela atualiza��o do campo G6M_PERDES.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP10(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (100 * oModelG6M:GetValue('G6M_VLDESC')) / oModelG6M:GetValue('G6M_VLACD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_PERDES', xValor))
	Help( , , "TA040XCP10", , STR0052, 1, 0) // "Erro ao calcular Percentual do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP09
Fun��o respons�vel pela atualiza��o do campo G6M_VLTXAD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP09(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (oModelG6M:GetValue('G6M_VLACD') * oModelG6M:GetValue('G6M_PERCTX')) / 100
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_VLTXAD', xValor))
	Help( , , "TA040XCP09", , STR0053, 1, 0) // "Erro ao calcular Valor da Taxa Adicional do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP08
Fun��o respons�vel pela atualiza��o do campo G6M_PERCTX.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP08(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := (100 * oModelG6M:GetValue('G6M_VLTXAD')) / oModelG6M:GetValue('G6M_VLACD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_PERCTX', xValor))
	Help( , , "TA040XCP08", , STR0052, 1, 0) // "Erro ao calcular Percentual do Desconto do Acordo."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP07
Fun��o respons�vel pela atualiza��o do campo G6M_VLACD.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP07(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local nX        := 0
Local lOk       := .T.

For nX := 1 To oModelG6O:Length()
	oModelG6O:GoLine(nX)
	If !(oModelG6O:IsDeleted())
		xValor += oModelG6O:GetValue('G6O_VAPLIC')
	EndIf
Next

If !(lOk := oModelG6M:SetValue('G6M_VLACD', xValor))
	Help( , , "TA040XCP07", , STR0054, 1, 0) // "Erro ao calcular Valor Apurado do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP06
Fun��o respons�vel pela atualiza��o do campo G6P_TOTAL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP06(oModel, cCampo)
	
Local aSaveRows := FwSaveRows()
Local oModelG6P := oModel:GetModel('G6P_DETAIL')
Local xRet      := oModelG6P:GetValue(cCampo)
Local xValor    := oModelG6P:GetValue('G6P_QTDADE') * oModelG6P:GetValue('G6P_VALOR')
Local lOk       := .T.

If !(lOk := oModelG6P:SetValue('G6P_TOTAL', xValor))
	Help( , , "TA040XCP06", , STR0055, 1, 0) // "Erro ao calcular Valor Total do Item."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP05
Fun��o respons�vel pela atualiza��o do campo G80_VALOR.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP05(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := Nil
Local oModelG80 := Nil
Local oModelG82 := Nil
Local nTotal    := 0
Local nPerc     := 0
Local nValor    := 0
Local nPerDes   := 0
Local nPercTx   := 0
Local lOk       := .T.
Local oView     := FwViewActive()
Local nFolder   := oView:GetFolderActive('F_ALL',2)[1]

If nFolder == 1 // Receita de ACordos
	oModelG6M := oModel:GetModel('G6M_DETAIL')
	oModelG80 := oModel:GetModel('G80A_DETAIL')

	nTotal    := oModelG6M:GetValue('G6M_VLACD')
	nPerDes   := oModelG6M:GetValue('G6M_PERDES')
	nPercTx   := oModelG6M:GetValue('G6M_PERCTX')

ElseIf nFolder == 2 // Receita de Vendas
	oModelG82 := oModel:GetModel('G82_DETAIL')
	oModelG80 := oModel:GetModel('G80B_DETAIL')

	nTotal    := oModelG82:GetValue('G82_VLPRD')
	nPerDes   := oModelG82:GetValue('G82_PERDES')
	nPercTx   := oModelG82:GetValue('G82_PERCTX')
EndIf

nPerc	:= oModelG80:GetValue('G80_PERC')
nValor  := nTotal * (nPerc/100)
nTotal	:= nValor - (nValor * nPerDes / 100) + (nValor * nPercTx / 100)

If !(lOk := oModelG80:SetValue('G80_VALOR', nValor))
	Help( , , "TA040XCP05", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

If !(lOk := oModelG80:SetValue('G80_PERDES', nPerDes))
	Help( , , "TA040XCP05", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

If !(lOk := oModelG80:SetValue('G80_PERCTX', nPercTx))
	Help( , , "TA040XCP05", , STR0028, 1, 0) // "Erro ao calcular Valor do Rateio."
EndIf

If !(lOk := oModelG80:SetValue('G80_VLRLIQ', nTotal))
	Help( , , "TA040XCP05", , STR0056, 1, 0) // "Erro ao calcular Total do Rateio."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP04
Fun��o respons�vel pela atualiza��o do campo G6O_VLBASE.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP04(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6L := oModel:GetModel('G6L_MASTER')
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local oModelG6N := oModel:GetModel('G6N_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_BASAPU') $ '12'		//	1=Volume;2=Quantidade
	xValor := TA040C02IV(oModelG6L:GetValue('G6L_CLIENT'), ;
	                     oModelG6L:GetValue('G6L_LOJA'), ;
	                     oModelG6L:GetValue('G6L_DTINI'), ;
	                     oModelG6L:GetValue('G6L_DTFIM'), ;
	                     oModelG6L:GetValue('G6L_MOEDA'), ;
	                     oModel:GetValue('G6M_DETAIL', 'G6M_SEGNEG'), ;
	                     Val(oModelG6O:GetValue('G6O_BASAPU')), ;
	                     oModelG6N, 'C02')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VLBASE', xValor))
	Help( , , "TA040XCP04", , STR0038, 1, 0) // "Erro ao calcular Valor Base do Item."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP03
Fun��o respons�vel pela atualiza��o do campo G6M_TOTAL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP03(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := ''
Local xAux      := Posicione('G5V', 1, xFilial('G5V') + oModelG6O:GetValue('G6O_CODACD') + oModelG6O:GetValue('G6O_CODREV'), 'G5V_VIGFIM')
Local lOk       := .T.

If Empty(xAux) .Or. xAux >= dDataBase
	xValor	:= '1'
Else
	xValor	:= '3'
EndIf

If !(lOk := oModelG6O:SetValue('G6O_STATUS', xValor))
	Help( , , "TA040XCP03", , STR0057, 1, 0) // "Erro ao atualizar Status do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP02
Fun��o respons�vel pela atualiza��o do campo G6O_VAPLIC.
@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP02(oModel, cCampo)

Local aSaveRows := FwSaveRows()
Local oModelG6O := oModel:GetModel('G6O_DETAIL')
Local xRet      := oModelG6O:GetValue(cCampo)
Local xValor    := 0
Local lOk       := .T.

If oModelG6O:GetValue('G6O_TIPOVL') == '1' 		// Porcentagem
	xValor := oModelG6O:GetValue('G6O_VLBASE') * (oModelG6O:GetValue('G6O_VALOR') / 100)
ElseIf oModelG6O:GetValue('G6O_TIPOVL') == '2'	// Fixo
	xValor := oModelG6O:GetValue('G6O_VALOR')
EndIf

If !(lOk := oModelG6O:SetValue('G6O_VAPLIC', xValor))
	Help( , , "TA040XCP02", , STR0036, 1, 0) // "Erro ao atualizar Valor Total do Acordo."
EndIf

FwRestRows(aSaveRows)

Return xRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040XCP01
Fun��o respons�vel pela atualiza��o do campo G6M_TOTAL.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040XCP01(oModel)

Local aSaveRows := FwSaveRows()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local xValor    := oModelG6M:GetValue('G6M_VLACD') - oModelG6M:GetValue('G6M_VLDESC') + oModelG6M:GetValue('G6M_VLTXAD')
Local lOk       := .T.

If !(lOk := oModelG6M:SetValue('G6M_TOTAL', xValor))
	Help( , , "TA040XCP01", , STR0058, 1, 0) // "Erro ao atualizar G6M_TOTAL."
EndIf

FwRestRows(aSaveRows)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G6MCh
Fun��o respons�vel pela sele��o das pastas de acordo com a classifica��o do acordo posicionado.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G6MCh(oView, cViewID)

Local oModel   := FwModelActive()
Local cTipoAc  := oModel:GetValue('G6M_DETAIL', 'G6M_TIPOAC')
Local cBaseApu := oModel:GetValue('G6O_DETAIL', 'G6O_BASAPU')

oView:HideFolder(  'F_DETALHES', 1, 2)
oView:HideFolder(  'F_DETALHES', 2, 2)
oView:SelectFolder('F_DETALHES', 3, 2)

Do Case
	Case cTipoAc $ 'C02|C04' 						// Flat | Success
		oView:HideFolder('F_DETALHES'  , 1, 2)		// Acordo x Itens de Venda G48
		oView:SelectFolder('F_DETALHES', 2, 2)		// Acordo x Itens de Venda G6N
		oView:HideFolder('F_DETALHES'  , 3, 2)		// Planilha de Custo G6P

	Case cTipoAc == 'C03' 							// Management
		oView:HideFolder('F_DETALHES'  , 1, 2)		// Acordo x Itens de Venda G48
		oView:HideFolder('F_DETALHES'  , 2, 2)		// Acordo x Itens de Venda G6N
		oView:SelectFolder('F_DETALHES', 3, 2)		// Planilha de Custo G6P

	Case cTipoAc $ 'C01|C05|C06|C12|C07|C10'			// Transaction | Combo | Taxa Agencia | Repasse | Taxa Reembolso
		oView:SelectFolder('F_DETALHES', 1, 2)		// Acordo x Itens de Venda G48
		oView:HideFolder('F_DETALHES'  , 2, 2)		// Acordo x Itens de Venda G6N
		oView:HideFolder('F_DETALHES'  , 3, 2)		// Planilha de Custo G6P
EndCase

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G6PFo
Fun��o respons�vel pelo retorno da f�rmula informada.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G6PFo(cFormula)

Local nRet	:= 0
nRet := &( AllTrim(Posicione('SM4', 1, xFilial('SM4') + cFormula, 'M4_FORMULA')))

Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G6PLO
Fun��o respons�vel pela valida��o da G6P.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G6PLO(oMdlG6P,nLinha)

Local lRet      := .T.
Local aSaveRows := FwSaveRows()
Local oModel    := FwModelActive()
Local nX        := 0
Local nTotal    := 0

For nX := 1 To oMdlG6P:Length()
	oMdlG6P:GoLine(nX)

	//+------------------------------------------------------
	//|	Valida G6P_FORMUL
	//+------------------------------------------------------
	If lRet
		Do Case
			Case oMdlG6P:GetValue('G6P_TPCALC') == '2'
				nFormul := TA040G6PFo(oMdlG6P:GetValue('G6P_FORMUL'))

				//+-----------------------------------------------------
				//|	Valida G6P_FORMUL
				//+-----------------------------------------------------
				If !(lRet := ValType(nFormul) == 'N')
					Help( , , "TA040G6PLO", , STR0059, 1, 0) // "Valor informado para o campo F�rmula � inv�lido."
				EndIf

			Case oMdlG6P:GetValue('G6P_TPCALC') == '1'
				//+-----------------------------------------------------
				//|	Se tipo do c�lculo for F�rmula, G6P_VALOR deve ser 0
				//+-----------------------------------------------------
				If lRet
					If !(lRet := (oMdlG6P:GetValue('G6P_VALOR') > 0 ))
						Help( , , "TA040G6PLO", , STR0060, 1, 0) // "Campo Valor � obrigat�rio."
					EndIf
				EndIf
		EndCase
	EndIf
Next

FwRestRows(aSaveRows)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040VLDAC
Fun��o respons�vel pela valida��o antes da exclus�o da apura��o.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040VLDAC(oModel)

Local lRet       := .T.
Local nOperation := oModel:GetOperation()

//+-----------------------------------------------------
// N�o permite excluir apura��es liberadas
//+-----------------------------------------------------
Do Case
	Case nOperation == MODEL_OPERATION_DELETE
		If !(lRet := !(G6L->G6L_STATUS == '2')) 		// 1=Em aberto;2=Liberado
			Help( , , "TA040VLDAC", , STR0061, 1, 0) 	// "Apura��o Liberada n�o pode ser exclu�da."
		EndIf
	Case nOperation == MODEL_OPERATION_UPDATE .And. !FwIsInCallStack("TA040SelApu") .And. !FwIsInCallStack("TURXEstApura") .And. !IsInCallStack('TA50DemApura')
		If !(lRet := !(G6L->G6L_STATUS == '2'))		// 1=Em aberto;2=Liberado
			Help( , , "TA040VLDAC", , STR0062, 1, 0) 	// "Apura��o Liberada n�o pode ser alterada."
		EndIf
EndCase

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G82LP
Fun��o respons�vel pela valida��o da G82.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040G82LP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)

Local lRet         := .T.
Local oModelMaster := FwModelActive()
Local oModelG80    := oModelMaster:GetModel('G80B_DETAIL')
Local nValor       := 0
Local nG80         := 0

Do Case
	Case cAction == 'DELETE'
		If !(lDelAcd := lRet := MsgYesNo(I18N(STR0063 + CRLF + ; // "'a exclus�o do Produto #1 ?"
                                              STR0064 + CRLF + ; // "Ap�s salvar a apura��o, este produto n�o ser� mais "
                                              STR0065, ; 			// "considerado nesta apura��o."
                                              {oGridModel:GetValue('G82_CODPRD')}), ;
                                         STR0066)) 			// "Exclus�o de Produtos"

			FwAlertHelp("TA040G82LP", STR0068) 		// "A��o cancelada pelo usu�rio."
		EndIf

	Case cAction == 'UNDELETE'
		lDelAcd := .T.
EndCase

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G6OLP
Fun��o respons�vel pela valida��o da G6O.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040G6OLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)

Local lRet         := .T.
Local oModelMaster := FwModelActive()
Local oMdlG48      := oModelMaster:GetModel('G48A_DETAIL')
Local nG48         := 0
Local nValor       := 0

Do Case
	Case cAction == 'DELETE'
		cTpAcordo := AllTrim(Posicione('G8B', 1, xFilial('G8B') + oModelMaster:GetValue('G6M_DETAIL', 'G6M_TIPOAC'), 'G8B_DESCRI'))
		If !(lDelAcd := lRet := MsgYesNo(I18N(STR0070 + CRLF + ; // "Confirma a exclus�o do Acordo #1 - #2 ?"
                                              STR0071 + CRLF + ; // "Ap�s salvar a apura��o, este acordo n�o ser� mais "
                                              STR0065, ; 			// "considerado nesta apura��o."
                                              {oGridModel:GetValue('G6O_CODACD'), cTpAcordo}), ;
                                         STR0072)) 			// "Exclus�o de Acordos"

			FwAlertHelp("TA040G6OLP", STR0068) 		// "A��o cancelada pelo usu�rio."
		EndIf

	Case cAction == 'UNDELETE'
		lDelAcd := .T.
EndCase

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G6PLP
Fun��o respons�vel pela valida��o da G6P.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040G6PLP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)

Local lRet         := .T.
Local oModelMaster := FwModelActive()
Local nVlBase      := 0

Do Case

//+------------------------------------------------------
// Valida Tipo de C�lculo, Valor e F�rmula
//+------------------------------------------------------
Case cAction == 'SETVALUE'
	If lRet .And. cIDField == 'G6P_FORMUL'
		//+------------------------------------------------------
		//|	Se tipo do c�lculo for Valor, G6P_FORMUL deve ser vazio
		//+------------------------------------------------------
		If !(lRet := !(oGridModel:GetValue('G6P_TPCALC') == '1' .And. !Empty(xValue)) )
			If xValue <> TA040G6PFo(oGridModel:GetValue('G6P_FORMUL'))
				Help( , , "TA040G6PLP", , STR0075, 1, 0) // "Valor informado � diferente do valor calculado da F�rmula."
			EndIf
		EndIf
	EndIf

//+------------------------------------------------------
// Atualiza G6O - Vlr Base
//+------------------------------------------------------
Case cAction == 'DELETE'
	If oModelMaster:GetValue('G6O_DETAIL', 'G6O_BASAPU') == '3' 	// Custo
		nVlBase := oModelMaster:GetValue('G6O_DETAIL', 'G6O_VLBASE') - oGridModel:GetValue('G6P_TOTAL')
		If !(lRet := oModelMaster:SetValue('G6O_DETAIL', 'G6O_VLBASE', nVlBase))
			Help( , , "TA040G6PLP", , STR0076, 1, 0) // "Erro ao deletar linha."
		EndIf
	Else
		oModelMaster:SetValue('G6O_DETAIL', 'G6O_VAPLIC', oModelMaster:GetValue('G6O_DETAIL', 'G6O_VAPLIC') - oGridModel:GetValue('G6P_TOTAL'))
	EndIf

Case cAction == 'UNDELETE'
 	If oModelMaster:GetValue('G6O_DETAIL', 'G6O_BASAPU') == '3' // Custo
		nVlBase := oModelMaster:GetValue('G6O_DETAIL', 'G6O_VLBASE') + oGridModel:GetValue('G6P_TOTAL')
		If !(lRet := oModelMaster:SetValue('G6O_DETAIL', 'G6O_VLBASE', nVlBase))
			Help( , ,"TA040G6PLP", , STR0077, 1, 0) // "Erro ao recuperar linha."
		EndIf
	Else
		oModelMaster:SetValue('G6O_DETAIL', 'G6O_VAPLIC', oModelMaster:GetValue('G6O_DETAIL', 'G6O_VAPLIC') + oGridModel:GetValue('G6P_TOTAL'))
	EndIf
EndCase

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G80LP
Fun��o respons�vel pela valida��o da G80.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040G80LP(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)

Local lRet    := .T.
Local nX      := 0
Local nPerc   := 0
Local oModel  := oGridModel:GetModel()
Local lRatPad := IIF(oGridModel:GetId() == 'G80A_DETAIL', oModel:GetValue("G6M_DETAIL", "G6M_RATPAD"), oModel:GetValue("G82_DETAIL", "G82_RATPAD"))

If cAction == 'UNDELETE' .And. lRatPad == "1"
	lRet := .F.
	oModel:SetErrorMessage(oGridModel:GetId(), , oGridModel:GetId(), , STR0116, STR0150, STR0151)	// "Aten��o"###"N�o � poss�vel recuperar uma linha de Rateio Padr�o."###"Adicione uma nova linha ou refa�a o Rateio Padr�o."			
ElseIf cAction == 'SETVALUE' .Or. cAction == 'UNDELETE' 
	If lRet .And. ((cIDField == 'G80_PERC' .And. xValue > 0) .Or. cAction == 'UNDELETE') 
		For nX := 1 To oGridModel:Length()
			oGridModel:GoLine(nX)
			If nX == nLine .And. (!oGridModel:IsDeleted() .Or. cAction == 'UNDELETE')
				If cAction == 'UNDELETE'
					nPerc += oGridModel:GetValue('G80_PERC')
				Else
					nPerc += xValue
				EndIf 
			ElseIf !oGridModel:IsDeleted()
				nPerc += oGridModel:GetValue('G80_PERC')
			EndIf
		Next nX
		oGridModel:GoLine(nLine)

		If nPerc > 100
			lRet := .F.
			Help( , , "TA040G80LP", , STR0078, 1, 0) // "Percentual informado ultrapassa 100%."
		EndIf
	EndIf
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ApZer
Fun��o respons�vel pela verifica��o e exclus�o de acordos sem valor.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ApZer(oModel)

Local lZerada    := MV_PAR16 == 2
Local oModelG6M  := oModel:GetModel('G6M_DETAIL' )
Local oModelG6O  := oModel:GetModel('G6O_DETAIL' )
Local oModelG48A := oModel:GetModel('G48A_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL' )
Local oModelG48B := oModel:GetModel('G48B_DETAIL')
Local nG6M       := 0
Local nG6O       := 0
Local nG82       := 0

If lZerada
	
	// Vendas
	For nG6M := 1 To oModelG6M:Length()
		oModelG6M:GoLine(nG6M)
		If !(oModelG6M:IsDeleted()) .And. oModelG6M:GetValue('G6M_TIPOAC') $ 'C01|C05|C06|C12|C07|C10'
			For nG6O := 1 To oModelG6O:Length()
				oModelG6O:GoLine(nG6O)
				If !(oModelG6O:IsDeleted())
					If oModelG6M:GetValue('G6M_TOTAL') == 0 .And. oModelG6O:GetValue('G6O_VAPLIC') == 0 .And. oModelG48A:Length() == 0
						oModelG6M:DeleteLine()
					Else
						lZerada := .F.
					EndIf
				EndIf
			Next nG6O
		EndIf
	Next nG6M

	// Servi�o Proprio
	If MV_PAR12 == 1
		For nG82 := 1 To oModelG82:Length()
			oModelG82:GoLine(nG82)
			If !(oModelG82:IsDeleted()) 
				If oModelG82:GetValue('G82_TOTAL') == 0 .And. oModelG48B:Length() == 0
					oModelG82:DeleteLine()
				Else
					lZerada := .F.
				EndIf
			EndIf
		Next nG82	
	EndIf
EndIf

Return lZerada

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG3S
Fun��o respons�vel pela sele��o da G3S.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG3S(cNumId,cIdItem)

Local aArea     := GetArea()
Local aRet      := {}
Local aAux      := {}
Local nX        := 0
Local cAliasG3S := GetNextAlias()

BeginSql Alias cAliasG3S
	SELECT G3S_NUMID, G3S_IDITEM, G3S_NUMSEQ, G3S_CONTAT
	FROM %Table:G3S% G3S
	WHERE G3S_FILIAL = %xFilial:G3S% AND 
	      G3S_NUMID  = %Exp:cNumId%  AND 
	      G3S_IDITEM = %Exp:cIdItem% AND
	      G3S_CONINU = '' AND
	      G3S.%NotDel%
EndSql

While (cAliasG3S)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG3S)->(FCount())
		aAdd(aAux, {(cAliasG3S)->(FieldName(nX)), (cAliasG3S)->(FieldGet(nX))})
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG3S)->(DbSkip())
EndDo

(cAliasG3S)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040PVal
Fun��o respons�vel pela valida��o da apura��o.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040PVal( oModel )

Local aSaveRows := FwSaveRows()
Local aArea     := GetArea()
Local lRet      := .T.

lRet := TA040RaVlA('2')		// validando o rateio manual de Receita de Acordo 

If lRet 
	lRet := TA040RaVlA('1')	// validando o rateio padr�o de Receita de Acordo
EndIf 

If lRet
	lRet := TA040RaVlV('2')	// validando o rateio manual de Receita de Servi�o Pr�prio
EndIf

If lRet
	lRet := TA040RaVlV('1')	// validando o rateio padr�o de Receita de Servi�o Pr�prio
EndIf

If !lRet
	Help( , , "TA040PVal", , STR0095, 1, 0)	// "Verifique a porcentagem informada."
EndIf

FWRestRows(aSaveRows)
RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Grv
Fun��o respons�vel pela grava��o da apura��o.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Grv(oModel)

Local aSaveRows  := FwSaveRows()
Local aArea      := GetArea()
Local nOperation := oModel:GetOperation()
Local oModelG48  := oModel:GetModel('G48A_DETAIL')
Local oModelG48B := oModel:GetModel('G48B_DETAIL')
Local oModelG6O  := oModel:GetModel('G6O_DETAIL')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local cCodApu    := oModel:GetModel('G6L_MASTER'):GetValue('G6L_CODAPU') 
Local cChave     := ''
Local nItem      := 0
Local nAcordo    := 0
Local nApu       := 0
Local nPerc      := 0
Local lRet       := .T.
Local lOk        := .T.
Local aG48Del    := {}
Local aAux       := {}
Local aG48ID	 := {}

If nOperation == MODEL_OPERATION_UPDATE
	//+------------------------------------------------------
	//| Verifica apura��o de acordos
	//+------------------------------------------------------
	For nApu := 1 to oModelG6M:Length()
		oModelG6M:GoLine(nApu)
		
		For nAcordo := 1 to oModelG6O:Length()
			oModelG6O:GoLine(nAcordo)
			If oModelG6O:GetValue('G6O_TIPOAC') $ 'C01|C05|C06|C12|C07|C10'
				If oModelG6O:IsDeleted()
					For nItem := 1 to oModelG48:Length()
						oModelG48:GoLine(nItem)
						aAux := {}
						aAdd(aAux, {'SEGNEG'    , oModelG6M:GetValue('G6M_SEGNEG')})
						aAdd(aAux, {'G48_FILAPU', oModelG48:GetValue('G48_FILAPU')})
						aAdd(aAux, {'G48_FILIAL', oModelG48:GetValue('G48_FILIAL')})
						aAdd(aAux, {'G48_NUMID' , oModelG48:GetValue('G48_NUMID' )})
						aAdd(aAux, {'G48_IDITEM', oModelG48:GetValue('G48_IDITEM')})
						aAdd(aAux, {'G48_NUMSEQ', oModelG48:GetValue('G48_NUMSEQ')})
						aAdd(aAux, {'G48_APLICA', oModelG48:GetValue('G48_APLICA')})
						aAdd(aAux, {'G48_CODACD', oModelG48:GetValue('G48_CODACD')})
						aAdd(aAux, {'G48_CODREC', oModelG48:GetValue('G48_CODREC')})
						aAdd(aAux, {'G48_CODAPU', '' })
						aAdd(aAux, {'G48_OK'    , .F.})
						aAdd(aG48Del, AClone(aAux))
						TA040G48Id(oModelG48, aG48ID)
					Next nItem
				Else
					For nItem := 1 to oModelG48:Length()
						oModelG48:GoLine(nItem)
						If oModelG48:IsUpdated()
							aAux := {}
							aAdd(aAux, {'SEGNEG'    , oModelG6M:GetValue('G6M_SEGNEG')})
							aAdd(aAux, {'G48_FILAPU', oModelG48:GetValue('G48_FILAPU')})
							aAdd(aAux, {'G48_FILIAL', oModelG48:GetValue('G48_FILIAL')})
							aAdd(aAux, {'G48_NUMID' , oModelG48:GetValue('G48_NUMID' )})
							aAdd(aAux, {'G48_IDITEM', oModelG48:GetValue('G48_IDITEM')})
							aAdd(aAux, {'G48_NUMSEQ', oModelG48:GetValue('G48_NUMSEQ')})
							aAdd(aAux, {'G48_APLICA', oModelG48:GetValue('G48_APLICA')})
							aAdd(aAux, {'G48_CODACD', oModelG48:GetValue('G48_CODACD')})
							aAdd(aAux, {'G48_CODREC', oModelG48:GetValue('G48_CODREC')})
							aAdd(aAux, {'G48_CODAPU', IIF(oModelG48:GetValue('G48_OK'), oModelG48:GetValue('G48_CODAPU'), '')})
							aAdd(aAux, {'G48_OK'    , oModelG48:GetValue('G48_OK'    )})
							aAdd(aG48Del, AClone(aAux))
							If !oModelG48B:GetValue('G48_OK')
								TA040G48Id(oModelG48, aG48ID)
							EndIf
						EndIf
					Next nItem
				EndIf
			EndIf
		Next nItem
	Next nApu

	//+------------------------------------------------------
	//| Verifica apura��o de servi�o pr�prio
	//+------------------------------------------------------
	For nApu := 1 to oModelG82:Length()
		oModelG82:GoLine(nApu)
		If oModelG82:IsDeleted()
			For nItem := 1 to oModelG48B:Length()
				oModelG48B:GoLine(nItem)
				aAux := {}
				aAdd(aAux, {'SEGNEG'    , oModelG82:GetValue ('G82_SEGNEG')})
				aAdd(aAux, {'G48_FILAPU', oModelG48B:GetValue('G48_FILAPU')})
				aAdd(aAux, {'G48_FILIAL', oModelG48B:GetValue('G48_FILIAL')})
				aAdd(aAux, {'G48_NUMID' , oModelG48B:GetValue('G48_NUMID' )})
				aAdd(aAux, {'G48_IDITEM', oModelG48B:GetValue('G48_IDITEM')})
				aAdd(aAux, {'G48_NUMSEQ', oModelG48B:GetValue('G48_NUMSEQ')})
				aAdd(aAux, {'G48_APLICA', oModelG48B:GetValue('G48_APLICA')})
				aAdd(aAux, {'G48_CODACD', oModelG48B:GetValue('G48_CODACD')})
				aAdd(aAux, {'G48_CODREC', oModelG48B:GetValue('G48_CODREC')})
				aAdd(aAux, {'G48_CODAPU', ''})
				aAdd(aAux, {'G48_OK'    , .F.})
				aAdd(aG48Del, AClone(aAux))
				TA040G48Id(oModelG48B, aG48ID)
			Next nItem
		Else
			For nItem := 1 to oModelG48B:Length()
				oModelG48B:GoLine(nItem)
				If oModelG48B:IsUpdated()
					aAux := {}
					aAdd(aAux, {'SEGNEG'    , oModelG82:GetValue ('G82_SEGNEG')})
					aAdd(aAux, {'G48_FILAPU', oModelG48B:GetValue('G48_FILAPU')})
					aAdd(aAux, {'G48_FILIAL', oModelG48B:GetValue('G48_FILIAL')})
					aAdd(aAux, {'G48_NUMID' , oModelG48B:GetValue('G48_NUMID' )})
					aAdd(aAux, {'G48_IDITEM', oModelG48B:GetValue('G48_IDITEM')})
					aAdd(aAux, {'G48_NUMSEQ', oModelG48B:GetValue('G48_NUMSEQ')})
					aAdd(aAux, {'G48_APLICA', oModelG48B:GetValue('G48_APLICA')})
					aAdd(aAux, {'G48_CODACD', oModelG48B:GetValue('G48_CODACD')})
					aAdd(aAux, {'G48_CODREC', oModelG48B:GetValue('G48_CODREC')})
					aAdd(aAux, {'G48_CODAPU', IIF(oModelG48B:GetValue('G48_OK'), oModelG48B:GetValue('G48_CODAPU'), '')})
					aAdd(aAux, {'G48_OK'    , oModelG48B:GetValue('G48_OK'    )})
					aAdd(aG48Del, AClone(aAux))
					If !oModelG48B:GetValue('G48_OK')
						TA040G48Id(oModelG48B, aG48ID)
					EndIf
				EndIf
			Next nItem
		EndIf
	Next nApu	
EndIf

If nOperation == MODEL_OPERATION_DELETE
	oModel:GetModel('G48A_DETAIL'):SetOnlyQuery(.T.)
	lRet := TurClearApu(oModel:GetModel('G6L_MASTER'):GetValue('G6L_FILIAL'), cCodApu, 'TURA040')	
	If lRet
		aG48ID := TurGetIdAC(oModel)
		DelAcApu(aG48ID, cCodApu)	
	EndIf
EndIf

If lRet .And. oModel:VldData()
	FWFormCommit(oModel)

	If nOperation == MODEL_OPERATION_UPDATE
		Processa({|| TA040Upd(cCodApu, aG48Del, /*lLiber*/, /*lEstor*/), DelAcApu(aG48ID, cCodApu)})
	Endif
EndIf

FWRestRows(aSaveRows)
RestArea( aArea )

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Upd
Fun��o respons�vel pela altera��o no RV atrav�s da TURA034.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040Upd(cCodApu, aG48Del, lLiber, lEstor)

Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local nX         := 0
Local nPos       := 0
Local oModelG48  := Nil
Local oModelG4C  := Nil
Local oStruG48   := FWFormStruct(1, 'G48', /*bAvalCampo*/, .F./*lViewUsado*/)	 
Local oStruG4C   := FWFormStruct(1, 'G4C', /*bAvalCampo*/, .F./*lViewUsado*/)	 
Local aStruG48   := oStruG48:GetFields()
Local lRet       := .T.
Local nG48       := 0
Local nG4C       := 0
Local nItem      := 0
Local nAcordo    := 0
Local nValor     := 0
Local cFilApu    := ''
Local cG48Fil    := ''    
Local cNumId     := ''
Local cSegNeg    := ''
Local cIdItem    := ''
Local cNumSeq    := ''
Local cAplica    := ''
Local cCodAcd    := ''
Local cCodRec    := ''
Local cConinu    := Space(TamSx3("G48_CONINU")[1])
Local aRV        := {}
Local lOk        := .F.

Default lLiber   := .F.
Default lEstor   := .F.

oModelG48 := MPFormModel():New('G48_FIELDS', /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/)
oModelG48:AddFields('G48_FIELDS', /*cOwner*/, oStruG48 )

oStruG48:SetProperty('G48_CODAPU', MODEL_FIELD_OBRIGAT, .F.)
oStruG48:SetProperty('G48_CODACD', MODEL_FIELD_OBRIGAT, .F.)
oStruG48:SetProperty('G48_CODREC', MODEL_FIELD_OBRIGAT, .F.)

oModelG48:AddGrid('G4C_DETAIL', 'G48_FIELDS', oStruG4C, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/)
oModelG48:SetRelation('G4C_DETAIL', {{'G4C_FILIAL', 'G48_FILIAL'}, {'G4C_NUMID', 'G48_NUMID'}, {'G4C_IDITEM', 'G48_IDITEM'}, {'G4C_NUMSEQ', 'G48_NUMSEQ'}, {'G4C_APLICA', 'G48_APLICA'}, {'G4C_NUMACD', 'G48_CODACD'}, {'G4C_CODAPU', 'G48_CODAPU'}, {'G4C_CONINU', 'G48_CONINU'}} , G4C->(IndexKey(1)))
oModelG48:GetModel('G4C_DETAIL'):SetOptional(.T.)

Begin Transaction
	//+------------------------------------------------------------
	//| Posiciona no RV
	//+------------------------------------------------------------
	ProcRegua(Len(aG48Del))
	For nG48 := 1 To Len(aG48Del)
		IncProc()
		cFilApu := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_FILAPU'})][2], TamSx3('G48_FILAPU')[1])
		cG48Fil := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_FILIAL'})][2], TamSx3('G48_FILIAL')[1])
		cNumId  := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_NUMID' })][2], TamSx3('G48_NUMID' )[1])
		cIdItem := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_IDITEM'})][2], TamSx3('G48_IDITEM')[1])
		cNumSeq := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_NUMSEQ'})][2], TamSx3('G48_NUMSEQ')[1])
		cAplica := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_APLICA'})][2], TamSx3('G48_APLICA')[1])
		cCodAcd := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_CODACD'})][2], TamSx3('G48_CODACD')[1])
		cCodRec := PadR(aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'G48_CODREC'})][2], TamSx3('G48_CODREC')[1])
		cSegNeg := aG48Del[nG48][AScan(aG48Del[nG48],{|x, y| x[1] == 'SEGNEG'})][2]
	
		If (nPos := Ascan(aRv,{|x| x[1] + x[2] + x[3] + x[4] == cG48Fil + cNumId + cIdItem + cNumSeq})) <= 0 
			aAdd(aRv, {cG48Fil, cNumId, cIdItem, cNumSeq})
		EndIf
	
		If TURSearch("G48", 5, cFilApu + cCodApu + cG48Fil + cNumId + cIdItem + cNumSeq + cAplica + cCodAcd + cCodRec)
			If G48->G48_SEGNEG = cSegNeg   
				oModelG48:SetOperation(MODEL_OPERATION_UPDATE)
				oModelG48:Activate()
			
				For nX := 1 To Len(aG48Del[nG48])
					If (nPos := aScan(aStruG48, {|x| AllTrim(x[3]) == AllTrim(aG48Del[nG48][nX][1])})) > 0
						If !(lRet := IIF(AllTrim(aG48Del[nG48][nX][1]) != 'G48_OK', ;
										   oModelG48:SetValue( 'G48_FIELDS', aStruG48[nPos][3], aG48Del[nG48][nX][2]), ;
										   oModelG48:LoadValue('G48_FIELDS', aStruG48[nPos][3], aG48Del[nG48][nX][2])))
							Help( , , "TA040Upd", , I18N(STR0106, {aStruG48[nPos][3]}), 1, 0)	// "Erro ao atribuir valor #1" 
							Exit
						EndIf
					EndIf
				Next nX
	
				//+------------------------------------------------------------
				//|	Se for libera��o, finaliza acordo aplicado
				//+------------------------------------------------------------
				If lRet .And. (lLiber .Or. lEstor)
					oModelG4C := oModelG48:GetModel('G4C_DETAIL')
					For nG4C := 1 To oModelG4C:Length()
						oModelG4C:GoLine(nG4C)
						If !oModelG4C:IsDeleted() .And. !Empty(oModelG4C:GetValue('G4C_IDIF')) 
							oModelG4C:SetValue('G4C_STATUS', IIF(lLiber, '4', '2'))
						EndIf
					Next nG4C
				EndIf
				
				If (lRet := oModelG48:VldData())
					oModelG48:CommitData()
				Else
					TA040GetEr(oModelG48)
					DisarmTransaction()
					Break
				EndIf
				oModelG48:DeActivate()
			EndIf
		EndIf
	Next nG48
	
	If lRet
		//Atualiza Status do IV
		For nX := 1 To Len(aRv)
			TurAtuStRV(aRv[nX,1], aRv[nX,2], aRv[nX,3], aRv[nX,4], cConinu, '1', IIF(lLiber, .T., .F.))
		Next
	EndIf

	If lRet .And. !lLiber .And. !lEstor
		TA040UPDRV()
	EndIf

End Transaction

RestArea(aArea)
FwRestRows(aSaveRows)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ChFld
Fun��o respons�vel pela valida��o do rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ChFld(cPasta, nOrigem, nDestino)

Local aSaveRows := FwSaveRows()
Local oView     := FwViewActive()
Local cViewID   := ''
Local lRet      := .T.

If cPasta == 'F_ALL'
	//+-----------------------------------
	//| Valida % do Rateio manual
	//+-----------------------------------
	If nOrigem == 1 // Receita de Acordos
		lRet := TA040RaVlA('2')	// validando o rateio manual de Receita de Acordo 
		If lRet
			lRet := TA040RaVlA('1')	// validando o rateio padr�o de Receita de Acordo
		EndIf
	ElseIf nOrigem == 2	// Receita de Vendas
		lRet := TA040RaVlV('2')	// validando o rateio manual de Receita de Servi�o Pr�prio
		If lRet
			lRet := TA040RaVlV('1')	// validando o rateio padr�o de Receita de Servi�o Pr�prio
		EndIf
	EndIf
EndIf

FwRestRows(aSaveRows)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Fun��o respons�vel pela defini��o da vis�o da Apura��o de Receita de Clientes.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function ViewDef()

Local oModel    := FWLoadModel('TURA040')
Local oStruG6L  := FWFormStruct(2, 'G6L')
Local oStruG6M  := FWFormStruct(2, 'G6M')
Local oStruG82  := FWFormStruct(2, 'G82')
Local oStruG6O  := FWFormStruct(2, 'G6O')
Local oStruG6P  := FWFormStruct(2, 'G6P')
Local oStruG6N  := FWFormStruct(2, 'G6N')
Local oStruG48  := FWFormStruct(2, 'G48')
Local oStruG80  := FWFormStruct(2, 'G80')
Local oStruG81  := FWFormStruct(2, 'G81')
Local oView     := FWFormView():New()
Local oStruCal1 := FWCalcStruct(oModel:GetModel('G6P_CALC' ))
Local oStruCalA := FWCalcStruct(oModel:GetModel('G80_CALCA'))
Local oStruCalB := FWCalcStruct(oModel:GetModel('G80_CALCB'))
Local nI		:= 0

/*-------------------------------------------------------------------
	Mapa de aOrdem
	
	aOrdem - Array
		aOrdem[n]	- Array
			aOrderm[n,1] - Caractere, Nome do Campo que Antecede
			aOrderm[n,2] - Caractere, Nome do Campo que Procede
--------------------------------------------------------------------*/
Local aOrdem	:= {}
Local aNoFldG6L := {	"G6L_GRPPRO",;
						"G6L_FORNEC",;
						"G6L_LJFORN",;
						"G6L_NOMFOR" }
Local aNoFldG6M := {	"G6M_FILREF" }
Local aNoFldG6O := {	"G6O_FILREF" }						
Local aNoFldG48 := {	'G48_MARK',;
						'G48_RECCON',;
						'G48_ATUIF' ,;
						'G49_CLIFOR',;
						'G48_PRICMB',;
						'G48_FILAPU',;
						'G48_DSFAPU',;
						'G48_CODAPU',;
						'G48_CODACD',;
						'G48_CODREC',;
						'G48_SEGNEG',;
						'G48_FILREF',;
						'G48_CONTAC',;
						'G48_CCUSTO',;
						'G48_ITEMCO',;
						'G48_CLASVL',;
						'G48_DSCMTI',;
						'G48_DSCCT1',;
						'G48_DSCCTT',;
						'G48_DSCCTD',;
						'G48_DSCCTH' }

//Remove campos que n�o ser�o apresentados no Field, Representado pela tabela G6L
For nI := 1 to Len(aNoFldG6L)
	If oStruG6L:HasField(aNoFldG6L[nI])
		oStruG6L:RemoveField(aNoFldG6L[nI])
	Endif
Next nI

//Remove campos que n�o ser�o apresentados no Grid Receita Acordos, representado pela tabela G6M
For nI := 1 to Len(aNoFldG6M)
	If oStruG6M:HasField(aNoFldG6M[nI])
		oStruG6M:RemoveField(aNoFldG6M[nI])
	Endif
Next nI

//Remove campos que n�o ser�o apresentados no Grid Acordos, representado pela tabela G6O
For nI := 1 to Len(aNoFldG6O)
	If oStruG6O:HasField(aNoFldG6O[nI])
		oStruG6O:RemoveField(aNoFldG6O[nI])
	Endif
Next nI

//Remove campos que seriam apresentados no Grid Acordo x Item de Venda - Tabela G48
For nI := 1 to Len(aNoFldG48)
	If oStruG48:HasField(aNoFldG48[nI])
		oStruG48:RemoveField(aNoFldG48[nI])
	Endif
Next nI

//Altera ordem de campos da Estrutura de G48 - Acordos Aplicados
aOrdem := {{"G48_ACDBAS","G48_DACDBA"}}

TurOrdViewStruct("G48",oStruG48,aOrdem)

//Altera ordem de campos da Estrutura de G80 - Rateio da Apuracao Cliente
aOrdem := {	{"G80_CODPRD","G80_DSCPRD"}}

TurOrdViewStruct("G80",oStruG80,aOrdem)

//Altera ordem de campos da Estrutura de G81 - Item Financeiro de Apuracao
TurOrdViewStruct("G81",oStruG81)

//Altera ordem de campos da Estrutura de G82 - Apuracao de Cliente � Vendas
aOrdem := {	{"G82_CODPRD","G82_DSCPRD"},;
			{"G82_NATURE","G82_DSCNAT"}}

TurOrdViewStruct("G82",oStruG82,aOrdem)

oView:SetModel( oModel )

oStruG48:SetProperty('G48_OK', MVC_VIEW_TITULO, '')
oStruG48:SetProperty('G48_OK', MVC_VIEW_ORDEM , '01')

oView:AddField('VIEW_CALC_1', oStruCal1, 'G6P_CALC' )
oView:AddField('VIEW_CALC_A', oStruCalA, 'G80_CALCA')
oView:AddField('VIEW_CALC_B', oStruCalB, 'G80_CALCB')

oView:AddField('VIEW_G6L', oStruG6L, 'G6L_MASTER')

oView:AddGrid('VIEW_G6M' , oStruG6M, 'G6M_DETAIL' )
oView:AddGrid('VIEW_G82' , oStruG82, 'G82_DETAIL' )
oView:AddGrid('VIEW_G6O' , oStruG6O, 'G6O_DETAIL' )
oView:AddGrid('VIEW_G6P' , oStruG6P, 'G6P_DETAIL' )
oView:AddGrid('VIEW_G6N' , oStruG6N, 'G6N_DETAIL' )
oView:AddGrid('VIEW_G48A', oStruG48, 'G48A_DETAIL')
oView:AddGrid('VIEW_G48B', oStruG48, 'G48B_DETAIL')
oView:AddGrid('VIEW_G80B', oStruG80, 'G80B_DETAIL')
oView:AddGrid('VIEW_G80A', oStruG80, 'G80A_DETAIL')
oView:AddGrid('VIEW_G81B', oStruG81, 'G81B_DETAIL')
oView:AddGrid('VIEW_G81A', oStruG81, 'G81A_DETAIL')

oView:SetViewProperty('VIEW_G6L', 'SETLAYOUT',{ FF_LAYOUT_HORZ_DESCR_TOP , 2 } )

oView:AddIncrementField('VIEW_G6P' , 'G6P_ITEM'  )
oView:AddIncrementField('VIEW_G6N' , 'G6N_ITEM'  )
oView:AddIncrementField('VIEW_G48A', 'G48_APLICA')
oView:AddIncrementField('VIEW_G48B', 'G48_APLICA')
oView:AddIncrementField('VIEW_G80A', 'G80_SEQUEN')
oView:AddIncrementField('VIEW_G80B', 'G80_SEQUEN')

//+-----------------------------------------------------------------------
//| TELA ESQUERDA
//+-----------------------------------------------------------------------
oView:CreateVerticallBox('TELA_ESQ', 29)	// Apura��o

//+-----------------------------------------------------------------------
//| TELA DIREITA
//+-----------------------------------------------------------------------
oView:CreateVerticallBox('TELA_DIR', 71)
oView:CreateHorizontalBox('BOX_DIR', 100, 'TELA_DIR')
oView:CreateFolder('F_ALL', 'BOX_DIR')

//+-----------------------------------------------------------------------
//| TELA DIREITA - Receita de Acordo
//+-----------------------------------------------------------------------
oView:AddSheet('F_ALL', 'S_REC_ACORDO', STR0079) // "Receita de Acordos"
oView:CreateHorizontalBox('BOX_AC_SUP', 23, , , 'F_ALL', 'S_REC_ACORDO')

//+-------------------------------------------------------------------
//|	ACORDOS
//+-------------------------------------------------------------------
oView:CreateHorizontalBox('BOX_AC_MEI', 77, , , 'F_ALL', 'S_REC_ACORDO')
oView:CreateFolder('F_ACORDOS', 'BOX_AC_MEI')

//+-----------------------------------------------------------
//|	Acordos
//+-----------------------------------------------------------
oView:AddSheet('F_ACORDOS', 'S_ACORDOS', STR0080) // "Acordos"
oView:CreateHorizontalBox('BOX_S_ACORDOS_UP', 35, , , 'F_ACORDOS', 'S_ACORDOS')

//+-------------------------------------------------------------------
//|	DETALHES ACORDOS
//+-------------------------------------------------------------------
oView:CreateHorizontalBox('BOX_S_ACORDOS_DOW', 65, , ,'F_ACORDOS', 'S_ACORDOS')
oView:CreateFolder('F_DETALHES', 'BOX_S_ACORDOS_DOW')

//+-----------------------------------------------------------
//|	Acordo x Itens de Venda G48
//+-----------------------------------------------------------
oView:AddSheet('F_DETALHES', 'S_ITENS_G48', STR0096) // // "Acordo x Itens de Venda"
oView:CreateHorizontalBox('BOX_S_ITENS_G48', 100, , , 'F_DETALHES', 'S_ITENS_G48')

//+-----------------------------------------------------------
//|	Acordo x Itens de Venda G6N
//+-----------------------------------------------------------
oView:AddSheet('F_DETALHES', 'S_ITENS_G6N', STR0096) // "Acordo x Itens de Venda"
oView:CreateHorizontalBox('BOX_S_ITENS_G6N', 100, , , 'F_DETALHES', 'S_ITENS_G6N')

//+-----------------------------------------------------------
//|	Planilha Custos G6P
//+-----------------------------------------------------------
oView:AddSheet('F_DETALHES', 'S_ITENS_G6P', STR0082) // "Planilha Custos"
oView:CreateHorizontalBox('BOX_ITENS_G6P_1', 70, , , 'F_DETALHES', 'S_ITENS_G6P')
oView:CreateHorizontalBox('BOX_ITENS_G6P_2', 30, , , 'F_DETALHES', 'S_ITENS_G6P')

//+-----------------------------------------------------------
//|	Rateio Clientes
//+-----------------------------------------------------------
oView:AddSheet('F_ACORDOS', 'S_RATEIO', STR0083) // "Rateio Clientes"
oView:CreateHorizontalBox('BOX_S_RATEIO_ACO_1', 80, , , 'F_ACORDOS', 'S_RATEIO')
oView:CreateHorizontalBox('BOX_S_RATEIO_ACO_2', 20, , , 'F_ACORDOS', 'S_RATEIO')

//+-----------------------------------------------------------
//|	Itens Financeiro
//+-----------------------------------------------------------
oView:AddSheet('F_ACORDOS', 'S_ITENSF', STR0081) // "Itens Financeiros"
oView:CreateHorizontalBox('BOX_S_ITENSF', 100, , , 'F_ACORDOS', 'S_ITENSF')

//+-----------------------------------------------------------------------
//| TELA DIREITA - Receita de Vendas
//+-----------------------------------------------------------------------
oView:AddSheet('F_ALL', 'S_REC_VENDA', STR0084) // "Receita de Vendas"
oView:CreateHorizontalBox('BOX_RECEITA_SUP', 34, , , 'F_ALL', 'S_REC_VENDA')

//+-------------------------------------------------------------------
//|	Receita de Vendas - Detalhes
//+-------------------------------------------------------------------
oView:CreateHorizontalBox('BOX_VENDAS', 66, , , 'F_ALL', 'S_REC_VENDA')
oView:CreateFolder('F_VENDAS', 'BOX_VENDAS')

//+-----------------------------------------------------------
//|	Acordos
//+-----------------------------------------------------------
oView:AddSheet('F_VENDAS', 'S_VENDAS', STR0085) // "Vendas"
oView:CreateHorizontalBox('BOX_S_VENDAS', 100, , , 'F_VENDAS', 'S_VENDAS')

//+-----------------------------------------------------------
//|	Rateio Cliente
//+-----------------------------------------------------------
oView:AddSheet('F_VENDAS', 'S_RATEIO_VEN', STR0086) // "Rateio Cliente"
oView:CreateHorizontalBox('BOX_S_RATEIO_VEN_1', 80, , , 'F_VENDAS', 'S_RATEIO_VEN')
oView:CreateHorizontalBox('BOX_S_RATEIO_VEN_2', 20, , , 'F_VENDAS', 'S_RATEIO_VEN')

//+-----------------------------------------------------------
//|	Itens Financeiros - Vendas
//+-----------------------------------------------------------
oView:AddSheet('F_VENDAS', 'S_IT_VENDAS', STR0081) // "Itens Financeiros"
oView:CreateHorizontalBox('BOX_S_IT_VENDAS', 100, , , 'F_VENDAS', 'S_IT_VENDAS')

oView:SetOwnerView('VIEW_G6L'   , 'TELA_ESQ'          )
oView:SetOwnerView('VIEW_G6M'   , 'BOX_AC_SUP'        )
oView:SetOwnerView('VIEW_G6O'   , 'BOX_S_ACORDOS_UP'  )
oView:SetOwnerView('VIEW_G80A'  , 'BOX_S_RATEIO_ACO_1')
oView:SetOwnerView('VIEW_CALC_A', 'BOX_S_RATEIO_ACO_2')

oView:SetOwnerView('VIEW_G48B'  , 'BOX_S_VENDAS'      )
oView:SetOwnerView('VIEW_G80B'  , 'BOX_S_RATEIO_VEN_1')
oView:SetOwnerView('VIEW_CALC_B', 'BOX_S_RATEIO_VEN_2')

oView:SetOwnerView('VIEW_G81A'  , 'BOX_S_ITENSF'   )
oView:SetOwnerView('VIEW_G48A'  , 'BOX_S_ITENS_G48')
oView:SetOwnerView('VIEW_G6N'   , 'BOX_S_ITENS_G6N')
oView:SetOwnerView('VIEW_G6P'   , 'BOX_ITENS_G6P_1')
oView:SetOwnerView('VIEW_CALC_1', 'BOX_ITENS_G6P_2')
oView:SetOwnerView('VIEW_G82'   , 'BOX_RECEITA_SUP')
oView:SetOwnerView('VIEW_G81B'  , 'BOX_S_IT_VENDAS')

oView:bVldFolder := {|cPasta, nOrigem, nDestino| TA040ChFld(cPasta, nOrigem, nDestino)}

If IsInCallStack('TA040Alterar')
	oView:AddUserButton(STR0087, 'CLIPS', {|oView| TA040Rat(oView)}) // "Rateio Padr�o"
EndIf

oView:SetViewProperty('G6M_DETAIL', 'CHANGELINE',{{|oView, cViewID| TA040G6MCh(oView, cViewID)}})
oView:SetAfterViewActivate({|oView, cViewID| TA040G6MCh(oView, cViewID)})

oView:SetViewAction('DELETELINE'  , {|oView, cIdView, nNumLine| TA040RaDel(oView, cIdView, nNumLine)})
oView:SetViewAction('UNDELETELINE', {|oView, cIdView, nNumLine| TA040RaDel(oView, cIdView, nNumLine)})

//Adi��o de novos botoes
oView:AddUserButton(STR0113, 'CLIPS', {|oVw| TA50MarkAll(oVw,.t.)  },,VK_F11,{MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE} ) // "Marcar/Desmarcar Todos"

Return oView

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Rat
Fun��o respons�vel pela chamada pr�pria para cada tipo de rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040Rat(oView)

Local oModel  := FwModelActive()
Local nFolder := oView:GetFolderActive('F_ALL',2)[1]
Local cTipoAc := ''

Do Case
	Case nFolder == 1	// "Receita de Acordos"
		cTipoAc := oModel:GetValue('G6M_DETAIL', 'G6M_TIPOAC')

		Do Case
			Case cTipoAc $ 'C02|C04' // Flat | Success
				Processa({|| TA040RaG6N()}, STR0008, STR0088) // "Aguarde..."###"Processando Rateio Padr�o..."

			Case cTipoAc $ 'C01|C05|C06|C12|C07|C10'	// Transaction | Combo | Taxa Agencia | Repasse | Taxa Reembolso
				Processa({|| TA040RaG48()}, STR0008, STR0088) // "Aguarde..."###"Processando Rateio Padr�o..."
		EndCase

	Case nFolder == 2	// "Receita de Vendas"
		Processa({|| TA040RaVen()}, STR0008, STR0088) // "Aguarde..."###"Processando Rateio Padr�o..."
EndCase

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaG6N
Fun��o respons�vel pela montagem do array utilizado na inclus�o da G6N.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaG6N()

Local aEntidades := {}
Local nEnt       := 0
Local cTpEnt     := ''
Local cItem      := ''
Local aG4A       := {}
Local nG4A       := {}
Local nTotal     := 0
Local aG80       := {}
Local aAux       := {}
Local nPerc      := 0
Local nG80       := 0
Local oModel     := FwModelActive()
Local cTipoAc    := oModel:GetValue('G6M_DETAIL', 'G6M_TIPOAC')
Local cCodApu    := oModel:GetValue('G6L_MASTER', 'G6L_CODAPU')
Local cSegNeg    := FwFldGet('G6M_SEGNEG')
Local oModelG80  := oModel:GetModel('G80A_DETAIL')
Local nValor     := 0
Local lAddLine   := .F.

aEntidades	:= TA040SeG3G(FwFldGet('G6L_CLIENT'), FwFldGet('G6L_LOJA'))
ProcRegua(Len(aEntidades))

For nEnt := 1 To Len(aEntidades)
	IncProc()
	cTpEnt	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_TIPO'})][2]
	cItem	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_ITEM'})][2]
	nValor	:= 0
	aG4A 	:= TA040SeG4A(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg)
	nTotal	:= TA040GetTR(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg)	// G6N

	For nG4A := 1 To Len(aG4A)
		nValor += aG4A[nG4A][AScan(aG4A[nG4A],{|x, y| x[1] == 'G6N_VALOR'})][2] * (aG4A[nG4A][AScan(aG4A[nG4A],{|x, y| x[1] == 'G4A_PERRAT'})][2] / 100)
	Next nG4A

	If nValor > 0
		aAux := TA040ArG80(nEnt, ;
		                   FwFldGet('G6L_CLIENT'), ;
		                   FwFldGet('G6L_LOJA'), ;
		                   cTpEnt, ;
		                   cItem, ;
		                   nValor, ;
		                   nTotal, ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLDESC'), ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLTXAD'), ;
		                   '', ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLACD'))
		aAdd(aG80, AClone(aAux))
	EndIf

Next nEnt

For nG80 := 1 To oModelG80:Length()
	oModelG80:GoLine(nG80)
	If !Empty(FwFldGet('G80_TPENT'))
		oModelG80:DeleteLine()
		lAddLine := .T.
	EndIf
Next nG80

If Len(aG80) > 0
	If TA040RatEx('G80A_DETAIL', oModel, aG80, lAddLine)
		oModel:SetValue('G6M_DETAIL', 'G6M_RATPAD', '1') // 1=Sim;2=N�o
	EndIf
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaVen
Fun��o respons�vel pelo rateio das vendas de servi�os pr�prios.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaVen()

Local aEntidades := {}
Local aAux       := {}
Local aG80       := {}
Local nEnt       := 0
Local nG4C       := 0
Local nValor     := 0
Local nG80       := 0
Local cTpEnt     := ''
Local cItem      := ''
Local oModel     := FwModelActive()
Local oModelG80  := oModel:GetModel('G80B_DETAIL')
Local cCodApu    := oModel:GetValue('G6L_MASTER', 'G6L_CODAPU')
Local cTipoAc    := 'V01'
Local lAddLine   := .F.
Local cCodPrd    := oModel:GetValue('G82_DETAIL', 'G82_CODPRD')
Local cSegNeg    := FwFldGet('G82_SEGNEG')

aEntidades	:= TA040SeG3G(FwFldGet('G6L_CLIENT'), FwFldGet('G6L_LOJA'))
ProcRegua(Len(aEntidades))

For nEnt := 1 To Len(aEntidades)
	IncProc()
	cTpEnt	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_TIPO'})][2]
	cItem	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_ITEM'})][2]
	nValor	:= 0
	aG4C 	:= TA040SeG4C(cTipoAc, cTpEnt, cItem, cCodApu) 							// Servi�os
	nTotal	:= TA040GetTR(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg, cCodPrd)		// Servi�os

	For nG4C := 1 To Len(aG4C)
		nValor += aG4C[nG4C][AScan(aG4C[nG4C],{|x, y| x[1] == 'G4C_VALOR'})][2]
	Next nG4C

	If nValor > 0
		aAux := TA040ArG80(nEnt, ;
		                   FwFldGet('G6L_CLIENT'), ;
		                   FwFldGet('G6L_LOJA'), ;
		                   cTpEnt, ;
		                   cItem, ;
		                   nValor, ;
		                   nTotal, ;
		                   oModel:GetValue('G82_DETAIL', 'G82_VLDESC'), ;
		                   oModel:GetValue('G82_DETAIL', 'G82_VLTXAD'), ;
		                   'V01', ;
		                   oModel:GetValue('G82_DETAIL', 'G82_VLPRD'))
		aAdd(aG80, AClone(aAux))
	EndIf
Next nEnt

For nG80 := 1 To oModelG80:Length()
	oModelG80:GoLine(nG80)
	If !Empty(oModelG80:GetValue('G80_TPENT'))
		oModelG80:DeleteLine()
		lAddLine := .T.
	EndIf
Next nG80

If Len(aG80) > 0
	If TA040RatEx('G80B_DETAIL', oModel, aG80, lAddLine)
		oModel:SetValue('G82_DETAIL', 'G82_RATPAD', '1') // 1=Sim;2=N�o
	EndIf
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaG48
Fun��o respons�vel pelo rateio da apura��o (G48).

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaG48()

Local aEntidades := {}
Local nEnt       := 0
Local aG4C       := {}
Local nG4C       := 0
Local cTpEnt     := ''
Local cItem      := ''
Local oModel     := FwModelActive()
Local cTipoAc    := oModel:GetValue('G6M_DETAIL', 'G6M_TIPOAC')
Local cSegNeg    := FwFldGet('G6M_SEGNEG')
Local cCodApu    := oModel:GetValue('G6L_MASTER', 'G6L_CODAPU')
Local oModelG80  := oModel:GetModel('G80A_DETAIL')
Local nValor     := 0
Local nG80       := 0
Local nTotal     := 0
Local aG80       := {}
Local aAux       := {}
Local lAddLine   := .F.

aEntidades	:= TA040SeG3G(FwFldGet('G6L_CLIENT'), FwFldGet('G6L_LOJA'))
ProcRegua(Len(aEntidades))

For nEnt := 1 To Len(aEntidades)
	IncProc()
	cTpEnt	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_TIPO'})][2]
	cItem	:= aEntidades[nEnt][AScan(aEntidades[nEnt], {|x, y| x[1] == 'G3G_ITEM'})][2]
	nValor	:= 0
	aG4C 	:= TA040SeG4C(cTipoAc, cTpEnt, cItem, cCodApu)
	nTotal	:= TA040GetTR(cTipoAc, cTpEnt, cItem, cCodApu, cSegNeg)		// G48

	For nG4C := 1 To Len(aG4C)
		nValor += aG4C[nG4C][AScan(aG4C[nG4C],{|x, y| x[1] == 'G4C_VALOR'})][2]
	Next nG4C

	If nValor > 0
		aAux := TA040ArG80(nEnt, ;
		                   FwFldGet('G6L_CLIENT'), ;
		                   FwFldGet('G6L_LOJA'), ;
		                   cTpEnt, ;
		                   cItem, ;
		                   nValor, ;
		                   nTotal, ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLDESC'), ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLTXAD'), ;
		                   '', ;
		                   oModel:GetValue('G6M_DETAIL', 'G6M_VLACD'))
		aAdd(aG80, AClone(aAux))
	EndIf
Next nEnt

For nG80 := 1 To oModelG80:Length()
	oModelG80:GoLine(nG80)
	If !Empty(FwFldGet('G80_TPENT'))
		oModelG80:DeleteLine()
		lAddLine := .T.
	EndIf
Next nG80

If Len(aG80) > 0
	If TA040RatEx('G80A_DETAIL', oModel, aG80, lAddLine)
		oModel:SetValue('G6M_DETAIL', 'G6M_RATPAD', '1') // 1=Sim;2=N�o
	EndIf
EndIf

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RatEx
Fun��o respons�vel pela execu��o do rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RatEx(cIdModel,oModel,aG80,lAddLine)

Local lRet := .T.
Local nG80 := 0

Default cIdModel	:= ''

For nG80 := 1 To Len(aG80)
	lAddLine := lAddLine .Or. nG80 > 1
	TA040SetVl(@oModel, cIdModel, aG80[nG80], lAddLine) // G80A
Next nG80

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040CalcV
Fun��o retorna o valor do totalizador

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040CalcV(oModel, nTotAtu, xValor, lSomando, cTipo)

Local oMdlG80 := Nil
Local nRet    := 0

oMdlG80 := IIF(cTipo $ '1|2', oModel:GetModel('G80A_DETAIL'), oModel:GetModel('G80B_DETAIL'))
If !oMdlG80:IsEmpty()
	nRet := IIF(lSomando, nTotAtu + xValor, nTotAtu + (xValor * -1))
EndIf
			
Return nRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaVlA
Fun��o respons�vel pela valida��o do rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaVlA(cRatPad)

Local lRet      := .T.
Local oModel    := FwModelActive()
Local oModelG6M := oModel:GetModel('G6M_DETAIL')
Local oModelG80 := oModel:GetModel('G80A_DETAIL')
Local nG80      := 0
Local nG6M      := 0
Local nPerc     := 0
Local cTipoAc   := ''
Local cSegNeg   := ''
Default cRatPad := '2'

For nG6M := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nG6M)
	nPerc   := 0
	cTipoAc := oModelG6M:GetValue('G6M_TIPOAC')
	IIF(oModelG6M:GetValue('G6M_SEGNEG') == '1', cNegSeg := STR0141, IIF(oModelG6M:GetValue('G6M_SEGNEG') == '2', cNegSeg := STR0142, cSegNeg := STR0143))		// Corporativo  // "Evento"  // "Lazer"

	If oModelG6M:GetValue('G6M_RATPAD') == cRatPad 
		For nG80 := 1 To oModelG80:Length()
			oModelG80:GoLine(nG80)
			If  !oModelG80:IsDeleted()
				nPerc += oModelG80:GetValue('G80_PERC')
			EndIf
		Next nG80

		If nPerc != 0
			If !(lRet := Round(nPerc, 2) == 100)
				MsgInfo(I18N(STR0089 + ; // "Rateio deve corresponder a 100% do valor total. "
				             STR0090, {AllTrim(Posicione("G8B", 1 , xFilial("G8B") + cTipoAc, "G8B_DESCRI")), nPerc, cSegNeg})) // "Verifique o tipo de acordo #1 ( #2 %) do Segmento #3 ."
				Exit
			EndIf
		EndIf
	EndIf
Next nG6M

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaVlV
Fun��o respons�vel pela valida��o do rateio.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaVlV(cRatPad)

Local lRet      := .T.
Local oModel    := FwModelActive()
Local oModelG82 := oModel:GetModel('G82_DETAIL')
Local oModelG80 := oModel:GetModel('G80B_DETAIL')
Local nG80      := 0
Local nG82      := 0
Local nPerc     := 0
Local cProduto  := ''
Default cRatPad := '2'

For nG82 := 1 To oModelG82:Length()
	oModelG82:GoLine(nG82)
	nPerc    := 0
	cProduto := oModelG82:GetValue('G82_CODPRD')

	If oModelG82:GetValue('G82_RATPAD') == cRatPad
		For nG80 := 1 To oModelG80:Length()
			oModelG80:GoLine(nG80)
			If !oModelG80:IsDeleted()
				nPerc += oModelG80:GetValue('G80_PERC')
			EndIf
		Next nG80

		If nPerc != 0
			If !(lRet := Round(nPerc, 2) == 100)
				MsgInfo(I18N(STR0089 + ; // "Rateio deve corresponder a 100% do valor total. "
				             STR0091, {RTrim(cProduto), nPerc})) // "Verifique o produto #1 ( #2 %)."
				Exit
			EndIf
		EndIf
	EndIf
Next nG82

Return lRet


//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040LibP
Fun��o respons�vel pela libera��o da apura��o.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040LibP(cCliente, cLoja)

Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local nG6LRecno  := G6L->(Recno())
Local oModel     := FWLoadModel('TURA040')
Local oModelG6L  := oModel:GetModel('G6L_MASTER')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local oModelG81A := oModel:GetModel('G81A_DETAIL')
Local oModelG81B := oModel:GetModel('G81B_DETAIL')
Local oStruG81A  := oModelG81A:GetStruct()
Local aStruG81A  := oStruG81A:GetFields()
Local oStruG81B  := oModelG81B:GetStruct()
Local aStruG81B  := oStruG81B:GetFields()
Local aDadosG6M  := {}
Local aDadosG82  := {}
Local aDadosG81A := {}
Local aDadosG81B := {}
Local aAux       := {}
Local nG81       := 0
Local nG6M       := 0
Local nG82       := 0
Local nX         := 0
Local nPos       := 0
Local lRet       := .F.
Local cCodApu    := G6L->G6L_CODAPU
Local aCtbG81A   := {}
Local aCtbG81B   := {}
Local lOnLine    := .F.
Local lMostrLanc := .F.
Local lAglutLanc := .F.

Pergunte("TURA040C",.F.)

lOnLine 	:= IIF(mv_par01 == 1,.T.,.F.)
lMostrLanc	:= IIF(mv_par02 == 1,.T.,.F.)
lAglutLanc	:= IIF(mv_par03 == 1,.T.,.F.)

oModel:GetModel('G81A_DETAIL'):SetNoInsertLine(.F.)
oModel:GetModel('G81B_DETAIL'):SetNoInsertLine(.F.)

//+-------------------------------------------------
//| Localiza Receita de Acordos
//+-------------------------------------------------
aDadosG6M := TA040SeG80('G6M', cCodApu)
For nG6M := 1 To Len(aDadosG6M)
	aAux := TA040ArG81(aDadosG6M[nG6M], cCliente, cLoja)
	If aDadosG6M[nG6M][AScan(aDadosG6M[nG6M],{|x, y| x[1] == 'TOTAL'})][2] <> 0
		aAdd(aDadosG81A, AClone(aAux))
	EndIf
Next nG6M

//+-------------------------------------------------
//|	Localiza Receita de Vendas
//+-------------------------------------------------
aDadosG82 := TA040SeG80('G82', cCodApu)
For nG82 := 1 To Len(aDadosG82)
	aAux := TA040ArG81(aDadosG82[nG82], cCliente, cLoja)
	If aDadosG82[nG82][AScan(aDadosG82[nG82],{|x, y| x[1] == 'TOTAL'})][2] <> 0
		aAdd(aDadosG81B, AClone(aAux))
	EndIf
Next nG82

//+-------------------------------------------------
//| Inclui IF da Apura��o
//+-------------------------------------------------
oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:Activate()

For nG6M := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nG6M)
	For nG81 := 1 To Len(aDadosG81A)
		If oModelG6M:GetValue('G6M_TIPOAC') == aDadosG81A[nG81][AScan(aDadosG81A[nG81], {|x, y| x[1] == 'G81_CLASS'})][2] .And. oModelG6M:GetValue('G6M_SEGNEG') == aDadosG81A[nG81][AScan(aDadosG81A[nG81], {|x, y| x[1] == 'G81_SEGNEG'})][2]
			If !(oModelG81A:Length() == 1 .And. Empty(oModelG81A:GetValue('G81_CLIENT')))
				oModelG81A:AddLine()
			EndIf

			//+-------------------------------------------
			//| Inclui dados na G81
			//+-------------------------------------------
			aDados := AClone(aDadosG81A[nG81])
			For nX := 1 To Len( aDados )
				If (nPos := aScan(aStruG81A, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
					If !(lRet := oModel:SetValue('G81A_DETAIL', aStruG81A[nPos][3], aDados[nX][2]))
						MsgAlert(STR0011 + aStruG81A[nPos][3]) // "Erro ao atribuir valor "
						Exit
					EndIf
				EndIf
			Next nX

			AADD(aCtbG81A,oModelG81A:GetValue('G81_IDIFA'))
						
		EndIf
	Next nG81
Next nG6M

//+-------------------------------------------
//| Inclui IF da Apura��o - Venda de Servi�os
//+-------------------------------------------
For nG82 := 1 To oModelG82:Length()
	oModelG82:GoLine(nG82)
	For nG81 := 1 To Len(aDadosG81B)
		If oModelG82:GetValue('G82_CODPRD') == aDadosG81B[nG81][AScan(aDadosG81B[nG81], {|x, y| x[1] == 'G81_CODPRD'})][2] .And. oModelG82:GetValue('G82_SEGNEG') == aDadosG81B[nG81][AScan(aDadosG81B[nG81], {|x, y| x[1] == 'G81_SEGNEG'})][2]
			If !(oModelG81B:Length() == 1 .And. Empty(oModelG81B:GetValue('G81_CLIENT')))
				oModelG81B:AddLine()
			EndIf

			//+-------------------------------------------
			//| Inclui dados na G81
			//+-------------------------------------------
			aDados := AClone(aDadosG81B[nG81])
			For nX := 1 To Len( aDados )
				If (nPos := aScan(aStruG81B, {|x| AllTrim(x[3])== AllTrim(aDados[nX][1])})) > 0
					If !(lRet := oModel:SetValue('G81B_DETAIL', aStruG81B[nPos][3], aDados[nX][2]))
						MsgAlert(STR0011 + aStruG81B[nPos][3]) // "Erro ao atribuir valor "
						Exit
					EndIf
				EndIf
			Next nX
			
			AADD(aCtbG81B,oModelG81B:GetValue('G81_IDIFA'))
			
		EndIf
	Next nG81
Next nG82

//+-------------------------------------------
//| Atualiza G4C e G48
//+-------------------------------------------
If (lRet := TA040LibG(nG6LRecno))
	//+-------------------------------------------
	//|	Atualiza status da apura��o
	//+-------------------------------------------
	oModelG6L:SetValue('G6L_STATUS', '2')
	oModelG6L:SetValue('G6L_CLIFAT', cCliente)
	oModelG6L:SetValue('G6L_LOJFAT', cLoja)
	
	If (lRet := oModel:VldData())
		BEGIN TRANSACTION
			If (lRet := oModel:CommitData())
				If lOnLine
					TurCtOnApu(lOnline,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA040")
				EndIf
			Else
				DisarmTransaction()
				Break
			EndIf
		END TRANSACTION
	Else
		TA040GetEr(oModel)
	EndIf
EndIf

oModel:GetModel('G81A_DETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G81B_DETAIL'):SetNoInsertLine(.T.)
oModel:DeActivate()
oModel:Destroy()
	
FwRestRows(aSaveRows)
RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ArG81
Fun��o respons�vel pela montagem do array da G81.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ArG81(aDados, cCliente, cLoja)

Local aRet    := {}
Local cTipoAc := aDados[AScan(aDados,{|x, y| x[1] == 'TIPOAC'})][2]
Local cSegNeg	:= aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2]
Local cTipo   := aDados[AScan(aDados,{|x, y| x[1] == 'TPTPAC'})][2]
Local nValor  := aDados[AScan(aDados,{|x, y| x[1] == 'TOTAL'})][2]

If aDados[AScan(aDados,{|x, y| x[1] == 'TOTAL'})][2] < 0
	nValor := aDados[AScan(aDados,{|x, y| x[1] == 'TOTAL'})][2] * -1 
EndIf

aAdd(aRet, {'G81_IDIFA' , GetSXENum('G81', 'G81_IDIFA')}) 
ConfirmSx8()

aAdd(aRet, {'G81_TIPO'  , cTipo})
aAdd(aRet, {'G81_SEGNEG', aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2]})
If cTipoAc == 'V01'
	aAdd(aRet, {'G81_CODPRD', aDados[AScan(aDados,{|x, y| x[1] == 'G82_CODPRD'})][2]})
EndIf
aAdd(aRet, {'G81_CLASS' , aDados[AScan(aDados,{|x, y| x[1] == 'TIPOAC'})][2]})
aAdd(aRet, {'G81_CLIENT', IIF(!Empty(cCliente), cCliente, G6L->G6L_CLIENT)})
aAdd(aRet, {'G81_LOJA'  , IIF(!Empty(cLoja), cLoja, G6L->G6L_LOJA)})
aAdd(aRet, {'G81_EMISSA', dDataBase})
aAdd(aRet, {'G81_PAGREC', IIF(cTipoAc == 'C07' .Or. aDados[AScan(aDados,{|x, y| x[1] == 'TOTAL'})][2] < 0, '1', '2')}) // 1=Pagar;2=Receber
aAdd(aRet, {'G81_MOEDA' , G6L->G6L_MOEDA})
aAdd(aRet, {'G81_VALOR' , nValor})
aAdd(aRet, {'G81_TPENT' , aDados[AScan(aDados,{|x, y| x[1] == 'G80_TPENT'})][2]})
aAdd(aRet, {'G81_ITENT' , aDados[AScan(aDados,{|x, y| x[1] == 'G80_ITENT'})][2]})
aAdd(aRet, {'G81_CONDPG', TURXCndPag(G6L->G6L_CLIENT, G6L->G6L_LOJA, aDados[AScan(aDados,{|x, y| x[1] == 'SEGNEG'})][2], /*cGrpCod*/, '2')})	
aAdd(aRet, {'G81_STATUS', '1'})	// Liberado
aAdd(aRet, {'G81_DTLIB' , dDataBase})	// Liberado
aAdd(aRet, {'G81_FILREF', cFilAnt})	// Filial de referencia
aAdd(aRet, {'G81_NATURE', aDados[AScan(aDados,{|x, y| x[1] == 'NATURE'})][2]})	// Natureza

Return AClone(aRet)



//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SeG80
Fun��o respons�vel pela sele��o da G80.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040SeG80(cTipo, cCodApu)

Local aArea     := GetArea()
Local aAux      := {}
Local aRet      := {}
Local cAliasG80 := GetNextAlias()
Local nX        := 0

Do Case
	Case cTipo == 'G6M'
		BeginSql Alias cAliasG80
			SELECT G6M_CODAPU CODAPU,
			       G6M_SEGNEG SEGNEG,
			       G6M_TIPOAC TIPOAC,
			       G6M_NATURE NATURE,
			       G8B_TIPO TPTPAC,
			       G8B_DESCRI ACDESC,		
			       G80_VLRLIQ,
			       G80_TPENT,
			       G80_ITENT,
			       SUM(G6M_TOTAL) TOTAL
			FROM %Table:G6M% G6M
			INNER JOIN %Table:G8B% G8B ON G8B_FILIAL = %xFilial:G8B% AND
			 								  G8B_CODIGO = G6M_TIPOAC AND
			 								  G8B.%NotDel%
			LEFT JOIN %Table:G80% G80 ON G80_FILIAL = %xFilial:G80% AND 
			                             G80_CODAPU = G6M_CODAPU AND 
			                             G80_SEGNEG = G6M_SEGNEG AND 
			                             G80_TIPOAC = G6M_TIPOAC AND
			                             G80.%NotDel%
			WHERE G6M_FILIAL = %xFilial:G6M% AND 
			      G6M_CODAPU = %Exp:cCodApu% AND
			      G6M.%NotDel% 
			GROUP BY G6M_CODAPU, G6M_SEGNEG, G6M_TIPOAC, G6M_NATURE, G8B_TIPO, G8B_DESCRI, G80_TPENT, G80_ITENT, G80_VLRLIQ
			ORDER BY G6M_TIPOAC, G80_TPENT, G80_ITENT
		EndSql

	Case cTipo == 'G82'
		BeginSql Alias cAliasG80
			SELECT G82_CODAPU CODAPU,
			       G82_SEGNEG SEGNEG,
			       G82_NATURE NATURE,
			       'V01' TIPOAC,
			       G8B_TIPO TPTPAC,
			       G8B_DESCRI ACDESC,
			       G80_VLRLIQ,
			       G80_TPENT,
			       G80_ITENT,
			       G82_CODPRD,
			       B1_TS,
			       B1_GRUPO,
			       SUM(G82_TOTAL) TOTAL
			FROM %Table:G82% G82
			INNER JOIN %Table:G8B% G8B ON G8B_FILIAL = %xFilial:G8B% AND
			 								  G8B_CODIGO = 'V01' AND
			 								  G8B.%NotDel%
			INNER JOIN %Table:SB1% SB1 ON B1_FILIAL = %xFilial:SB1% AND
			 								  B1_COD = G82_CODPRD AND
			 								  SB1.%NotDel%
			LEFT JOIN %Table:G80% G80 ON G80_FILIAL = %xFilial:G80% AND 
			                             G80_CODAPU = G82_CODAPU AND 
			                             G80_SEGNEG = G82_SEGNEG AND 
			                             G80_TIPOAC = 'V01' AND 
			                             G80_CODPRD = G82_CODPRD AND 
			                             G80.%NotDel%
			WHERE G82_FILIAL = %xFilial:G82% AND 
			      G82_CODAPU = %Exp:cCodApu% AND
			      G82.%NotDel% 
			GROUP BY G82_CODAPU, G82_SEGNEG, G82_NATURE, G82_CODPRD, B1_TS, B1_GRUPO, G80_TPENT, G80_ITENT, G80_VLRLIQ, G8B_TIPO, G8B_DESCRI 
			ORDER BY G82_CODPRD, G80_TPENT, G80_ITENT
		EndSql
EndCase

While (cAliasG80)->(!EOF())
	aAux := {}
	For nX := 1 To (cAliasG80)->(FCount())
		If (cAliasG80)->(FieldName(nX)) != 'TOTAL'
			aAdd(aAux, {(cAliasG80)->(FieldName(nX)), (cAliasG80)->(FieldGet(nX))})
		ElseIf (cAliasG80)->G80_VLRLIQ > 0 .And. !Empty((cAliasG80)->G80_TPENT) .And. !Empty((cAliasG80)->G80_ITENT)
			aAdd(aAux, {(cAliasG80)->(FieldName(nX)), (cAliasG80)->G80_VLRLIQ})
		Else  
			aAdd(aAux, {(cAliasG80)->(FieldName(nX)), (cAliasG80)->(FieldGet(nX))})
		EndIf
	Next
	aAdd(aRet, AClone(aAux))
	(cAliasG80)->(DbSkip())
EndDo

(cAliasG80)->(DbCloseArea())
RestArea(aArea)

Return AClone(aRet)

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040LibG
Fun��o respons�vel pela chamada da TURA034. 
Atrav�s dela ser�o atualizados os status da G48 e G4C.

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040LibG(nG6LRecno, lEstorno)

Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local cAliasG48  := GetNextAlias()
Local cCodApu    := ''
Local aG48       := {}
Local lRet       := .T.

Default lEstorno := .F.

//+------------------------------------------------
//| Posiciona na Apura��o
//+------------------------------------------------
G6L->(DbGoTo(nG6LRecno))
cCodApu := G6L->G6L_CODAPU

//+------------------------------------------------
// Seleciona Itens do Acordo
//+------------------------------------------------
BeginSql Alias cAliasG48
	Column G48_OK as Logical
	SELECT	G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA, G48_CODACD, G48_CODREC, G48_FILIAL, 
			G48_FILAPU, G48_CODAPU, G48_OK, G48_CODPRD, G48_CLASS, G48_VLACD, G6M_SEGNEG SEGNEG
	FROM %Table:G48% G48
	INNER JOIN %Table:G6M% G6M ON G6M_FILIAL = G48_FILAPU AND 
	                              G6M_CODAPU = G48_CODAPU AND 
	                              G6M_TIPOAC = G48_CLASS AND 
	                              G6M_SEGNEG = G48_SEGNEG AND 
	                              G6M.%NotDel%
	WHERE G48_FILAPU = %xFilial:G6M% AND
	      G48_CODAPU = %Exp:cCodApu% AND 
	      G48.%NotDel%
	ORDER BY G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_CODACD, G48_APLICA
EndSql

While (cAliasG48)->(!EOF())
	// Liberar apenas o G4C do Combo principal
	If ( (cAliasG48)->G48_VLACD == 0 )
		(cAliasG48)->(DbSkip())
		Loop
	EndIf 

	aAux := {}
	aAdd(aAux, {'SEGNEG'    , (cAliasG48)->SEGNEG    })
	aAdd(aAux, {'G48_FILIAL', (cAliasG48)->G48_FILIAL})
	aAdd(aAux, {'G48_NUMID' , (cAliasG48)->G48_NUMID	})
	aAdd(aAux, {'G48_IDITEM', (cAliasG48)->G48_IDITEM})
	aAdd(aAux, {'G48_NUMSEQ', (cAliasG48)->G48_NUMSEQ})
	aAdd(aAux, {'G48_APLICA', (cAliasG48)->G48_APLICA})
	aAdd(aAux, {'G48_CODACD', (cAliasG48)->G48_CODACD})
	aAdd(aAux, {'G48_CODREC', (cAliasG48)->G48_CODREC})
	aAdd(aAux, {'G48_CODPRD', (cAliasG48)->G48_CODPRD})
	aAdd(aAux, {'G48_CLASS' , (cAliasG48)->G48_CLASS })
	aAdd(aAux, {'G48_OK'    , (cAliasG48)->G48_OK    })

	If (cAliasG48)->G48_OK
		aAdd(aAux, {'G48_FILAPU', (cAliasG48)->G48_FILAPU})
		aAdd(aAux, {'G48_CODAPU', (cAliasG48)->G48_CODAPU})
		aAdd(aAux, {'G48_STATUS', IIF(!lEstorno, '4', '1')})
	Else
		aAdd(aAux, {'G48_FILAPU', ''})
		aAdd(aAux, {'G48_CODAPU', ''})
		aAdd(aAux, {'G48_STATUS', ''})
	EndIf
	aAdd(aG48, AClone(aAux))
	(cAliasG48)->(DbSkip())
EndDo

(cAliasG48)->(DbCloseArea())

//+------------------------------------------------
// Seleciona Itens de Venda
//+------------------------------------------------
cAliasG48 := GetNextAlias()

BeginSql Alias cAliasG48
	Column G48_OK as Logical
	SELECT	G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA, G48_CODACD, G48_CODREC, G48_FILIAL, 
	       G48_FILAPU, G48_CODAPU, G48_OK, G48_CODPRD, G48_CLASS, G82_SEGNEG SEGNEG
	FROM %Table:G48% G48
	INNER JOIN %Table:G82% G82 ON G82_FILIAL = G48_FILAPU AND 
	                              G82_CODAPU = G48_CODAPU AND 
	                              G82_CODPRD = G48_CODPRD AND 
	                              G82_SEGNEG = G48_SEGNEG AND 
	                              G82.%NotDel%
	WHERE G48_FILAPU = %xFilial:G82% AND
	      G48_CODAPU = %Exp:cCodApu% AND 
	      G48_CLASS  = 'V01' AND
	      G48.%NotDel%
	ORDER BY G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_CODPRD, G48_APLICA
EndSql

While (cAliasG48)->(!EOF())
	aAux := {}
	aAdd(aAux, {'SEGNEG'    , (cAliasG48)->SEGNEG    })
	aAdd(aAux, {'G48_FILIAL', (cAliasG48)->G48_FILIAL})
	aAdd(aAux, {'G48_NUMID' , (cAliasG48)->G48_NUMID	})
	aAdd(aAux, {'G48_IDITEM', (cAliasG48)->G48_IDITEM})
	aAdd(aAux, {'G48_NUMSEQ', (cAliasG48)->G48_NUMSEQ})
	aAdd(aAux, {'G48_APLICA', (cAliasG48)->G48_APLICA})
	aAdd(aAux, {'G48_CODACD', (cAliasG48)->G48_CODACD})
	aAdd(aAux, {'G48_CODREC', (cAliasG48)->G48_CODREC})
	aAdd(aAux, {'G48_CODPRD', (cAliasG48)->G48_CODPRD})
	aAdd(aAux, {'G48_CLASS' , (cAliasG48)->G48_CLASS })
	aAdd(aAux, {'G48_OK'    , (cAliasG48)->G48_OK    })

	If (cAliasG48)->G48_OK
		aAdd(aAux, {'G48_FILAPU', (cAliasG48)->G48_FILAPU})
		aAdd(aAux, {'G48_CODAPU', (cAliasG48)->G48_CODAPU})
		aAdd(aAux, {'G48_STATUS', IIF(!lEstorno, '4', '1')})
	Else
		aAdd(aAux, {'G48_FILAPU', ''})
		aAdd(aAux, {'G48_CODAPU', ''})
	EndIf
	aAdd(aG48, AClone(aAux))
	(cAliasG48)->(DbSkip())
EndDo

(cAliasG48)->(DbCloseArea())

lRet := TA040Upd(cCodApu, aG48, IIF(!lEstorno, .T., .F.), lEstorno)

FwRestRows(aSaveRows)
RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RaDel
Fun��o respons�vel pelo rec�lculo do rateio padr�o ao deletar uma linha

@type 		Function
@author 	Veronica de Almeida
@since 		30/11/2015
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040RaDel(oView, cIdView, nNumLine)

Local oModel   := FwModelActive()
Local oMdlG6M  := oModel:GetModel('G6M_DETAIL')
Local oMdlG6O  := oModel:GetModel('G6O_DETAIL')
Local cTpVal   := oMdlG6O:GetValue('G6O_TIPOVL')
Local nValor   := 0
Local nPercen  := 0
Local lRet     := .F.
Local lDeleted := .F.
Local nX       := 0
Local oMdlDet  := Nil

If !lDelAcd
	lRet := .T.
Else
	Do Case
		Case cIdView == 'G6O_DETAIL'
			If oMdlG6O:Length() > 1
				If oModel:GetValue("G6M_DETAIL", "G6M_PERDES") > 0
					MsgInfo(STR0148, STR0146)	// "O Valor de Desconto ser� recalculado com base no % Desconto."###"Rec�lculo Desconto"
				EndIf
				If oModel:GetValue("G6M_DETAIL", "G6M_PERCTX") > 0
					MsgInfo(STR0149, STR0147)	// "O Valor da Taxa Adicional ser� recalculado com base no % Tx. Adic.."###"Rec�lculo Taxa Adicional"
				EndIf
			EndIf

			lDeleted := oMdlG6O:IsDeleted(nNumLine)
			nValor   := oMdlG6M:GetValue('G6M_VLACD') - oMdlG6O:GetValue('G6O_VAPLIC')
			If !lDeleted
				nValor := oMdlG6M:GetValue('G6M_VLACD') + oMdlG6O:GetValue('G6O_VAPLIC')
			EndIf
	
			If !(lRet := oModel:SetValue('G6M_DETAIL', 'G6M_VLACD', nValor))
				Help( , ,"TA040RaDel", , STR0073, 1, 0) // "Erro ao excluir acordo."
			Else
				// Atualizando os itens de acordo com o tipo de acordo
				If oMdlG6O:GetValue('G6O_TIPOAC') $ 'C01|C05|C06|C12|C07|C10' 
					nValor  := 0
					oMdlDet := oModel:GetModel('G48A_DETAIL')

					For nX := 1 To oMdlDet:Length()
						oMdlDet:GoLine(nX)
						
						// Caso a linha G6O esteja exclu�da desmarcar os acordos, do contr�rio deve marca-los
						oMdlDet:LoadValue('G48_OK', IIF(lDeleted, .F., .T.))

						If oMdlDet:GetValue('G48_OK')
							nValor += oMdlDet:GetValue(IIF(cTpVal == '1', 'G48_VLBASE', 'G48_VLACD'))
						EndIf
					Next nX
					If lDeleted
						oMdlG6O:LoadValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'), 0)
						oMdlG6O:LoadValue('G6O_PERCEN', 0)
						oMdlG6O:LoadValue('G6O_VAPLIC', 0)
					Else
						oMdlG6O:SetValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'), nValor)
						If cTpVal == '1'
							nPercen := Posicione('G5V', 1, xFilial('G5V') + oMdlG6O:GetValue('G6O_CODACD') + oMdlG6O:GetValue('G6O_CODREV'), 'G5V_VALOR')
							oMdlG6O:SetValue('G6O_PERCEN', nPercen)
						EndIf
						oMdlG6O:SetValue('G6O_VAPLIC', IIF(cTpVal == '1', oMdlG6O:GetValue('G6O_VLBASE') * (oMdlG6O:GetValue('G6O_PERCEN')/100), oMdlG6O:GetValue(IIF(cTpVal == '1', 'G6O_VLBASE', 'G6O_VALOR'))) * IIF(oMdlDet:GetValue('G48_TPACD') == '1', 1, -1))
					EndIf					
				
				ElseIf oMdlG6O:GetValue('G6O_TIPOAC') $ 'C02|C04'
					oMdlDet := oModel:GetModel('G6N_DETAIL')
					oMdlDet:SetNoUpdateLine(.F.)

					For nX := 1 To oMdlDet:Length()
						oMdlDet:GoLine(nX)
						
						If lDeleted
							oMdlDet:SetValue('G6N_OK', .F.)
						Else
							oMdlDet:SetValue('G6N_OK', .T.)
						EndIf
					Next nX

					oMdlDet:SetNoUpdateLine(lDeleted)
				EndIf 
			EndIf
			oView:Refresh()
			
		Case cIdView == 'G82_DETAIL'
			nValor := oModel:GetValue('G6L_MASTER', 'G6L_TOTPRD') - oModel:GetValue('G82_DETAIL', 'G82_TOTAL')
			If !(oModel:GetModel('G82_DETAIL'):IsDeleted(nNumLine))
				nValor := oModel:GetValue('G6L_MASTER', 'G6L_TOTPRD') + oModel:GetValue('G82_DETAIL', 'G82_TOTAL')
			EndIf
	
			If !(lRet := oModel:SetValue('G6L_MASTER', 'G6L_TOTPRD', nValor))
				Help( , , "TA040RaDel", , STR0067, 1, 0) // "Erro ao excluir produto."
			Else
				// Atualizando os itens de acordo com o tipo de acordo
				oMdlDet := oModel:GetModel('G48B_DETAIL')
				For nX := 1 To oMdlDet:Length()
					oMdlDet:GoLine(nX)
					If oModel:GetModel('G82_DETAIL'):IsDeleted(nNumLine)
						oMdlDet:LoadValue('G48_OK', .F.)
					Else
						oMdlDet:LoadValue('G48_OK', .T.)
					EndIf
				Next nX
			EndIf
			oView:Refresh()
	
		Case cIdView == 'G80A_DETAIL'
			If !(lRet := oModel:SetValue('G6M_DETAIL', 'G6M_RATPAD', '2'))
				Help( , , "TA040RaDel", , STR0029, 1, 0) // "Erro ao atualizar controle Rateio Padr�o"
			EndIf
			oView:Refresh()
	
		Case cIdView == 'G80B_DETAIL'
			If !(lRet := oModel:SetValue('G82_DETAIL', 'G82_RATPAD', '2'))
				Help( , ,"TA040RaDel", , STR0029, 1, 0) // "Erro ao atualizar controle Rateio Padr�o"
			EndIf
			oView:Refresh()
	EndCase
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040EstL
Fun��o respons�vel pelo estorno da libera��o da apura��o.

@type 		Function
@author 	Thiago Tavares
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040EstL()

Local aArea      := GetArea()
Local aSaveRows  := FwSaveRows()
Local nG6LRecno  := G6L->(Recno())
Local oModel     := FWLoadModel('TURA040')
Local oModelG6L  := oModel:GetModel('G6L_MASTER')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local oModelG81A := oModel:GetModel('G81A_DETAIL')
Local oModelG81B := oModel:GetModel('G81B_DETAIL')
Local nG6M       := 0
Local nG82       := 0
Local cCodApu    := G6L->G6L_CODAPU
Local aCtbG81A	 := {}
Local aCtbG81B	 := {}
Local lOnLine	 := .F.
Local lMostrLanc := .F.
Local lAglutLanc := .F.
Local lRet       := .F.

Pergunte("TURA040C",.F.)

lOnLine 	:= .T.	//Sempre ser� on-line no estorno
lMostrLanc	:= IIF(mv_par02 == 1,.T.,.F.)
lAglutLanc	:= IIF(mv_par03 == 1,.T.,.F.)

oModel:GetModel('G81A_DETAIL'):SetNoDeleteLine(.F.)
oModel:GetModel('G81B_DETAIL'):SetNoDeleteLine(.F.)

//+-------------------------------------------------
//| Exclui IF da Apura��o
//+-------------------------------------------------
oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:Activate()

For nG6M := 1 To oModelG6M:Length()
	oModelG6M:GoLine(nG6M)
	AADD(aCtbG81A,oModelG81A:GetValue('G81_IDIFA'))
	oModelG81A:DelAllLine()
Next nG6M

//+-------------------------------------------
//| Exclui IF da Apura��o - Venda de Servi�os
//+-------------------------------------------
For nG82 := 1 To oModelG82:Length()
	oModelG82:GoLine(nG82)
	AADD(aCtbG81B,oModelG81B:GetValue('G81_IDIFA'))
	oModelG81B:DelAllLine()
Next nG82

//+-------------------------------------------
//| Atualiza G4C e G48
//+-------------------------------------------
If (lRet := TA040LibG(nG6LRecno, .T.))
	//+-------------------------------------------
	//|	Atualiza status da apura��o
	//+-------------------------------------------
	oModelG6L:SetValue('G6L_STATUS', '1')
	oModelG6L:SetValue('G6L_CLIFAT', '')
	oModelG6L:SetValue('G6L_LOJFAT', '')
	
	If (lRet := oModel:VldData())
		BEGIN TRANSACTION
			TurCtOnEAp(oModel,lOnLine,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA040")
			If !(lRet := oModel:CommitData())
				DisarmTransaction()
				Break
			EndIf
		END TRANSACTION
	Else
		TA040GetEr(oModel)
	EndIf
EndIf 

oModel:GetModel('G81A_DETAIL'):SetNoDeleteLine(.T.)
oModel:GetModel('G81B_DETAIL'):SetNoDeleteLine(.T.)
oModel:DeActivate()

FwRestRows(aSaveRows)
RestArea(aArea)

Return lRet 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040Alterar
Fun��o respons�vel pela montagem da View sem o bot�o "Salvar e Criar Novo"

@type 		Function
@author 	Thiago Tavares
@since 		29/02/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040Alterar()

Local oModel 	  := Nil
Local oExecView := Nil
Local aButtons  := {{.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}, {.F., Nil}, ;
                    {.F., Nil}, {.T., STR0103}, {.T., STR0104}, {.F., Nil}, {.F., Nil}, ;		// "Confirmar		"Cancelar"
                    {.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}}

oModel:= FWLoadModel("TURA040")
oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:Activate()

oView := FWLoadView("TURA040")
oView:SetModel(oModel)

oExecView := FWViewExec():New()
oExecView:SetTitle(Upper(STR0005))		// "Alterar"
oExecView:SetView(oView)
oExecView:SetModal(.F.)               
oExecView:SetButtons(aButtons)
oExecView:SetOperation(MODEL_OPERATION_UPDATE)
oExecView:OpenView(.F.)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040PERGT
Fun��o utilizada para preenchimento automatico dos Pergunte

@type 		Function
@author 	Thiago Tavares
@since 		31/08/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040PERGT()

Local lRet := .T.

Do Case
	Case Upper(Alltrim(ReadVar())) == 'MV_PAR01'		// CLIENTE DE
		If !Empty(MV_PAR01) .And. Alltrim(Upper(MV_PAR01)) <> Replicate("Z", TamSx3("G3Q_CLIENT")[1])
			MV_PAR03 := MV_PAR01
		ElseIf Empty(MV_PAR01)
			MV_PAR03 := Replicate("Z", TamSx3("G3Q_CLIENT")[1])
		EndIf
			
	Case Upper(Alltrim(ReadVar())) == 'MV_PAR02'		// LOJA DE
		If !Empty(MV_PAR02) .And. Alltrim(Upper(MV_PAR02)) <> Replicate("Z", TamSx3("G3Q_LOJA")[1])
			MV_PAR04 := MV_PAR02
		ElseIf Empty(MV_PAR02)
			MV_PAR04 := Replicate("Z", TamSx3("G3Q_LOJA")[1])
		EndIf

	Case Upper(Alltrim(ReadVar())) $ 'MV_PAR10|MV_PAR09'		// DATA INCLUSAO ATE
		If !Empty(MV_PAR09) .And. !Empty(MV_PAR10) .And. MV_PAR10 < MV_PAR09
			Help( , , "TA040PERGT", , STR0107, 1, 0)		// "O campo Dt. Inclus�o At�? n�o pode ser menor que o campo Dt. Inclus�o De?"
			lRet := .F. 
		EndIf
	
	Case Upper(Alltrim(ReadVar())) == 'MV_PAR13'			// PRODUTO DE
		If !Empty(MV_PAR13) .And. Alltrim(Upper(MV_PAR13)) <> Replicate("Z", TamSx3("B1_COD")[1])
			MV_PAR14 := MV_PAR13
		ElseIf Empty(MV_PAR13)
			MV_PAR14 := Replicate("Z", TamSx3("B1_COD")[1])
		EndIf
End Case

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040RPT
Fun��o chamada pelo menu para chamada da impress�o do demonstrativo de cobran�a

@type 		Function
@author 	Thiago Tavares
@since 		31/08/2016
@version 	12.1.7
/*/
//+----------------------------------------------------------------------------------------
Function TA040RPT(nTipo)

If FindFunction("TURR001")
	TURR001()
Endif

Return()

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040VLDG48()
Fun��o que valida se o acordo pode ser editado

@type 		Function
@author 	Thiago Tavares
@since 		11/01/2017
@version 	12.1.14
/*/
//---------------------------------------------------------------------------------
Static Function TA040VLDG48(oMdl, nLine, cAction, cCampo, xVlBefore, xVlCurrent)

Local lRet    := .F.
Local lG48IFs := TA040G48IF(oMdl)
Local oView   := IIF(!IsBlind(), FwViewActive(), Nil)
Local oMdlG6O := oMdl:GetModel():GetModel('G6O_DETAIL')

If !oMdlG6O:IsDeleted()
	If oMdl:GetModel():GetOperation() == MODEL_OPERATION_UPDATE
		If cAction == 'CANSETVALUE'
			If cCampo == 'G48_OK'
				lRet := .T.
			ElseIf cCampo == 'G48_PERACD' .And. oMdl:GetValue('G48_OK') .And. oMdl:GetValue('G48_TPVLR') == '1' .And. (oMdl:GetValue('G48_CLASS') $ 'C01|C06|C12|C07|C10' .Or. (oMdl:GetValue('G48_CLASS') == 'C05' .And. oMdl:GetValue('G48_PRICMB') == '1'))
				lRet := .T.
			ElseIf cCampo == 'G48_VLACD' .And. oMdl:GetValue('G48_OK') .And. oMdl:GetValue('G48_TPVLR') == '2' .And. (oMdl:GetValue('G48_CLASS') $ 'C01|C06|C12|C07|C10' .Or. (oMdl:GetValue('G48_CLASS') == 'C05' .And. oMdl:GetValue('G48_PRICMB') == '1'))
				lRet := .T. 
			EndIf
	
			If lRet .And. lG48IFs
				If IsBlind() .Or. (FwAlertYesNo(STR0115, '') .And. Tura34Inte(oMdl, .T.))	// "Deseja realizar a altera��o manualmente?" 
					lRet := .T.
				Else
					lRet := .F.
				EndIf	
			EndIf 
		ElseIf cAction == 'SETVALUE'
			If cCampo $ 'G48_OK|G48_PERACD|G48_VLACD'
				If cCampo != 'G48_OK' .And. xVlBefore < 0
					lRet := .F.
					oMdl:GetModel():SetErrorMessage(oMdl:GetModel():GetId(), , oMdl:GetModel():GetId(), , STR0116, STR0117, STR0118)	// "Aten��o"	"O Acordo n�o aceita valor apurado negativo."		"Sempre informar valores positivos."	
				Else
					lRet := .T. 
				Endif	
			Else
				lRet := .F.
			EndIf		
		EndIf
	EndIf
EndIf

If lRet .And. cCampo $ 'G48_VLACD' .And. cAction == 'SETVALUE' .And. !IsInCallStack('TA040XCP49')
	TA040XCP50(oMdl:GetModel(), xVlBefore, xVlCurrent)
EndIf

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G48IF
Fun��o se o acordo possui IF finalizado

@type 		Function
@author 	Thiago Tavares
@since 		11/01/2017
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Function TA040G48IF(oModel) 

Local aArea     := GetArea()
Local cAliasG48 := GetNextAlias()
Local cNumId    := oModel:GetValue('G48_NUMID')
Local cIdItem   := oModel:GetValue('G48_IDITEM')
Local cNumSeq   := oModel:GetValue('G48_NUMSEQ')
Local cCodAcd   := oModel:GetValue('G48_CODACD')
Local cRevAcd   := oModel:GetValue('G48_CODREC')
Local lRet      := .F.

BeginSQL Alias cAliasG48
	SELECT G48.R_E_C_N_O_
 	FROM %Table:G48% G48
	INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G48_FILIAL AND 
	                              G4C_NUMID  = G48_NUMID  AND 
	                              G4C_IDITEM = G48_IDITEM AND 
	                              G4C_NUMSEQ = G48_NUMSEQ AND 
	                              G4C_CLASS  = G48_CLASS  AND 
	                              G4C_APLICA = G48_APLICA AND 
	                              G4C_STATUS = '4' AND 
	                              G4C_CONINU = ''  AND 
	                              G4C.%NotDel%
 	WHERE G48_FILAPU = %xFilial:G6L% AND 
 	      G48_NUMID  = %Exp:cNumId%  AND 
 	      G48_IDITEM = %Exp:cIdItem% AND 
 	      G48_NUMSEQ = %Exp:cNumSeq% AND 
 	      G48_CODACD = %Exp:cCodAcd% AND 
 	      G48_CODREC = %Exp:cRevAcd% AND 
 	      G48_CLIFOR = '1' AND 
 	      G48_CONINU = ''  AND 
 	      G48.%NotDel% 	
EndSQL

If (cAliasG48)->(!EOF())
	lRet := .T.	
EndIf
(cAliasG48)->(DbCloseArea())

RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040UPDRV
Fun��o respons�vel pela atualizado do Item de Venda

@type 		Function
@author 	Thiago Tavares
@since 		11/01/2017
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040UPDRV()

Local aArea      := GetArea()
Local cFilialAnt := cFilAnt  
Local oModelRV   := Nil
Local nItemG3Q   := 0
Local nIndex     := 0
Local cCampo     := ''

For nIndex := 1 To Len(aG48Dif)
	If aG48Dif[nIndex][5] <> 0 
		DbSelectArea('G3P')
		G3P->(DbSetOrder(1))		// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG
		If G3P->(DbSeek(aG48Dif[nIndex][7] + aG48Dif[nIndex][1]))
			cFilAnt  := aG48Dif[nIndex][7]
			oModelRV := FWLoadModel('TURA034')
			oModelRV:SetOperation(MODEL_OPERATION_UPDATE)
			
			If oModelRV:Activate()
				For nItemG3Q := 1 To oModelRV:GetModel("G3Q_ITENS"):Length()
					oModelRV:GetModel("G3Q_ITENS"):GoLine(nItemG3Q)
					If oModelRV:GetModel("G3Q_ITENS"):GetValue("G3Q_IDITEM") == aG48Dif[nIndex][2] .And. oModelRV:GetModel("G3Q_ITENS"):GetValue("G3Q_NUMSEQ") == aG48Dif[nIndex][3] 
						cCampo:= AllTrim(Posicione('G8B', 1, xFilial('G8B') + aG48Dif[nIndex][6], 'G8B_DESTIN')) 
						oModelRV:GetModel("G3Q_ITENS"):SetValue(cCampo, oModelRV:GetModel("G3Q_ITENS"):GetValue(cCampo) + aG48Dif[nIndex][5])					
						Tur34ItFin(oModelRV, ' ', .F., '1' , .T.)
						T34AtuDmFi(oModelRV) 										
					EndIf
				Next nItemG3Q
				
				If oModelRV:VldData() 
					oModelRV:CommitData()
				EndIf
				oModelRV:DeActivate()
			EndIf
			oModelRV:Destroy()
			cFilAnt := cFilialAnt
		EndIf
		G3P->(DbCloseArea())
	EndIf
Next nIndex

aG48Dif := {} 
RestArea(aArea)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040SelApu
Fun��o respons�vel por selecionar apura��es atrav�s de um markbrowser para liberar/estornar

@type 		Function
@author 	Thiago Tavares
@since 		30/01/2017
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Function TA040SelApu(nTipo)

Local aArea     := GetArea()
Local aVetor	:= {}
Local oDlg      := Nil
Local oOk       := LoadBitmap( GetResources(), "LBOK" ) //CHECKED    //LBOK  //LBTIK
Local oNo       := LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO
Local oLbx 	    := Nil
Local oChk      := Nil
Local lMark     := .F.
Local lChk		:= .F.
Local lLibera   := nTipo == 1
Local lMarcado  := .F.
Local lRet      := .T.
Local lCliApu   := .F.
Local lContinua := .F.
Local nX        := 0
Local nPos      := 0
Local cStatus   := IIF(lLibera, cStatus := '1', '2')		// 1=Em Aberto / 2=Liberado
Local cVar      := ''
Local cAliasG6L := GetNextAlias() 
Local cArqLog   := GetTempPath() + IIF(lLibera, STR0120, STR0121) + FwTimeStamp() + '.log'		// "Liberacao_"		"Estorno_"
Local cMsgLog   := ''
Local cCliente  := Space(TamSx3('G3Q_CLIENT')[1])
Local cLoja     := Space(TamSx3('G3Q_LOJA')[1])
Local cCodApu   := Space(TamSx3('G6L_CODAPU')[1])

BeginSql Alias cAliasG6L
	COLUMN G6L_DTGERA as Date
	
	SELECT G6L_CODAPU, G6L_CLIENT, G6L_LOJA, G6L_DTGERA 
	FROM %Table:G6L% G6L
	WHERE G6L_FILIAL = %xFilial:G6L% AND 
	      G6L_TPAPUR = '1' AND
	      G6L_STATUS = %Exp:cStatus% AND 
	      G6L.%NotDel%
	ORDER BY G6L_CODAPU
EndSql

If (cAliasG6L)->(!Eof())
	While (cAliasG6L)->(!Eof())
		aAdd(aVetor, {lMark, ;
			          (cAliasG6L)->G6L_CODAPU, ;
			          (cAliasG6L)->G6L_DTGERA, ;
			          (cAliasG6L)->G6L_CLIENT, ;
			          (cAliasG6L)->G6L_LOJA, ;
			          Posicione('SA1', 1, xFilial('SA1') + (cAliasG6L)->(G6L_CLIENT + G6L_LOJA), 'A1_NOME')})  
		(cAliasG6L)->(DbSkip())
	EndDo
Else
	aAdd(aVetor, {lMark, '', CtoD('//'), '', '', ''})  
EndIf
(cAliasG6L)->(DbCloseArea())

DEFINE MSDIALOG oDlg TITLE STR0122 FROM 0,0 TO 340,700 PIXEL	// "Sele��o de Apura��es"

@ 010,010 SAY OemToAnsi(STR0159) Of oDlg PIXEL SIZE 40,9		// "C�d. Apura��o:" 
@ 010,050 MSGET cCodApu Picture "@!" OF oDlg PIXEL SIZE 80,9
@ 010,130 BUTTON STR0160 SIZE 50, 12 ACTION (IIF((nPos := aScan(aVetor, {|x| x[2] == cCodApu})) > 0, oLbx:nAt := nPos, Alert(STR0161))) OF oDlg PIXEL		// "Procurar"	"Apura��o n�o encontrada."   

@ 30,10 LISTBOX oLbx VAR cVar FIELDS HEADER " ", TURX3Title("G6L_CODAPU"), TURX3Title("G6L_DTGERA"), TURX3Title("G6L_CLIENT"), TURX3Title("G6L_LOJA"), TURX3Title("A1_NOME") ;
   SIZE 335,120 OF oDlg PIXEL ON dblClick(aVetor[oLbx:nAt, 1] := !aVetor[oLbx:nAt, 1], oLbx:Refresh())

oLbx:SetArray(aVetor)
oLbx:bLine := {|| {Iif(aVetor[oLbx:nAt, 1], oOk, oNo),;
                       aVetor[oLbx:nAt, 2],;
                       aVetor[oLbx:nAt, 3],;
                       aVetor[oLbx:nAt, 4],;
                       aVetor[oLbx:nAt, 5],;
                       aVetor[oLbx:nAt, 6]}}
	 
//+----------------------------------------------------------------
//| Para marcar e desmarcar todos existem duas op�oes, acompanhe...
//+----------------------------------------------------------------
//| Chamando uma funcao pr�pria
//+----------------------------------------------------------------
@ 155,10 CHECKBOX oChk VAR lChk PROMPT STR0113 SIZE 75,007 PIXEL OF oDlg;		// "Marcar/Desmarcar Todos" 
          ON CLICK(Iif(lChk, TLbxMarca(@oLbx, @aVetor, lChk), TLbxMarca(@oLbx, @aVetor, lChk)))

If lLibera
	@ 155,194 SAY OemToAnsi(STR0135) Of oDlg PIXEL SIZE 80,9 // "Cliente:"
	@ 153,215 MSGET cCliente Picture "@!" OF oDlg PIXEL WHEN lCliApu F3 "SA1" SIZE 40,9
	@ 155,265 SAY OemToAnsi(STR0136) Of oDlg PIXEL SIZE 80,9 // "Loja:"
	@ 153,280 MSGET cLoja Picture "@!" OF oDlg PIXEL WHEN lCliApu SIZE 20,9
	
	@ 155,115 CHECKBOX oChk VAR lCliApu PROMPT STR0137 SIZE 75,007 PIXEL OF oDlg;		// "Alterar cliente da fatura" 
          ON CLICK(cCliente := Space(TamSx3('G3Q_CLIENT')[1]), cLoja := Space(TamSx3('G3Q_LOJA')[1]), oLbx:SetFocus())
          
EndIf

DEFINE SBUTTON FROM 155,317 TYPE 1 ACTION (IIF(!lCliApu .Or. Ta40CliFat(cCliente, cLoja, aVetor), (lContinua := .T., oDlg:End()), .F.)) ENABLE OF oDlg

ACTIVATE MSDIALOG oDlg CENTER

If lContinua
	// verificando se alguma apura��o foi marcada
	For nX := 1 to Len(aVetor)
		If aVetor[nX, 1] .And. !Empty(aVetor[nX, 2]) 
			lMarcado := .T.
		EndIf
	Next nX
	
	If !lMarcado
		FwAlertHelp('TA040SelApu', STR0123)	
	Else
		If FwAlertYesNo(I18N(STR0124, {IIF(lLibera, STR0125, STR0126)}))	// "Confirma #1 da(s) apura��o(�es) selecionada(s)?"    "a libera��o"     "o estorno"  
			For nX := 1 To Len(aVetor)
				If aVetor[nX, 1] 
					G6L->(DbSeek(xFilial('G6L') + aVetor[nX, 2]))
				
					If lLibera
						If !(lRet := TurChkItApur('TURA040'))
							cMsgLog += I18N(STR0127, {aVetor[nX, 2]}) + Chr(13) + Chr(10)		// "#1: N�o foi poss�vel liberar porque a apura��o n�o possui itens de acordos aplicados para serem liberados."
						EndIf
					Else
						If !(lRet := TURXVlIF(G6L->G6L_CODAPU))
							cMsgLog += I18N(STR0128, {aVetor[nX, 2]}) + Chr(13) + Chr(10)		// "#1: N�o foi poss�vel estornar porque a apura��o possui itens financeiros que foram finalizados."
						EndIf
					EndIf
					
					If lRet .And. CtbValiDt( , aVetor[nX, 3], , , , {"TUR001"}, )
						If SoftLock("G6L")
							// mesmo sendo poss�vel bloquear o registro pode ter acabado de acontecer 
							// uma libera��o/estorno, por isso, verifico o status ao liberar/estornar
							If (lLibera .And. G6L->G6L_STATUS == "1") .Or. (!lLibera .And. G6L->G6L_STATUS == "2")
								FwMsgRun( , IIF(lLibera, {|| lRet := StaticCall(TURA040,TA040LibP,cCliente,cLoja)}, {|| lRet := (StaticCall(TURA040, TA040EstL))}), , I18N(IIF(lLibera, STR0094, STR0145), {aVetor[nX, 2]}))	// "Liberando a Apura��o: #1"		"Estornando a Apura��o: #1"
								If lRet 
									cMsgLog += I18N(STR0129, {aVetor[nX, 2], IIF(lLibera, STR0130, STR0131)}) + Chr(13) + Chr(10)		// "#1: Apura��o #2 com sucesso."		"liberada"		"estornada"
								Else
									cMsgLog += I18N(STR0144, {aVetor[nX, 2], IIF(lLibera, STR0125, STR0126)}) + Chr(13) + Chr(10)		// "#1: Houve uma falha ao realizar #2. Por favor, tente novamente."		"a libera��o"     "o estorno"
								EndIf
							ElseIf lLibera .And. G6L->G6L_STATUS == "2" 
								cMsgLog += aVetor[nX, 2] + ": " + STR0002 + Chr(13) + Chr(10)		// "Apura��o Liberada"
							ElseIf !lLibera .And. G6L->G6L_STATUS == "2" 
								cMsgLog += aVetor[nX, 2] + ": " + STR0001		// "Apura��o em Aberto" 
							EndIf
							G6L->(MsUnLock())
						Else
							cMsgLog += I18N(STR0144, {aVetor[nX, 2], IIF(lLibera, STR0125, STR0126)}) + Chr(13) + Chr(10)		// "#1: Houve uma falha ao realizar #2. Por favor, tente novamente."		"a libera��o"     "o estorno"
						EndIf
					EndIf	
				EndIf
			Next nX
			
			// gravando o arquivo com o log do processamento
			nHandle := FCreate(cArqLog)		
			FSeek(nHandle, 0, FS_END)         
			FWrite(nHandle, cMsgLog, Len(cMsgLog)) 
			FClose(nHandle)
			If FwAlertYesNo(STR0132) 
				ShellExecute('Open', cArqLog, '', GetTempPath(), 1)
			EndIf                   
		EndIf
	EndIf 
EndIf

G6L->(DbCloseArea())
RestArea(aArea)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} Ta40CliFat
Valida cliente e loja digitados na tela de sele��o de apura��es

@type 		Function
@author 	paulo.barbosa
@since 		07/03/2018
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Static Function Ta40CliFat(cCliente, cLoja, aVetor)

Local aArea      := GetArea()
Local lRet       := .T.
Local cQry       := "% AND G6M.G6M_CODAPU IN('"
Local nX         := 0
Local cAliasQry  := ""
Local aComplemen := {}

If Empty(cCliente) .Or. Empty(cLoja)
	TurHelp(STR0138, STR0139, 'Ta40CliFat') //"Cliente ou loja n�o preenchidos" ### "Favor preencher cliente e loja"
	lRet := .F.
ElseIf !SA1->(ExistCpo('SA1', cCliente + cLoja))
	lRet := .F.
Else
	For nX := 1 to Len(aVetor)
		If aVetor[nX,1]
			cQry += aVetor[nX, 2] + IIF(nX + 1 <= Len(aVetor), "', '", "")
		EndIf
	Next nX
	
	cQry += "') %"
	
	cAliasQry := GetNextAlias()
	BeginSql Alias cAliasQry
		SELECT DISTINCT G6M_SEGNEG
		FROM %Table:G6M% G6M
		WHERE G6M.G6M_FILIAL = %xFilial:G6L% AND G6M.%NotDel% %Exp:cQry% 
	EndSql
	
	While (cAliasQry)->(!EOF())
		aComplemen := Tur18RtCmp(cCliente, cLoja, Val((cAliasQry)->G6M_SEGNEG))
		If aComplemen[2] .Or. Empty(aComplemen[1])
			TurHelp( i18N(STR0140, {IIF((cAliasQry)->G6M_SEGNEG == "1", STR0141, IIF((cAliasQry)->G6M_SEGNEG == "2", STR0142, STR0143))}) , "Favor cadastrar o complemento do cliente para o segmento informado.", 'Ta40CliFat' ) //"Cliente sem nenhum Complemento cadastrado para o segmento #1" ### "Corporativo" ### "Evento" ### "Lazer"
			lRet := .F.
			Exit
		EndIf
		(cAliasQry)->(DbSkip())
	EndDo
	(cAliasQry)->(DbCloseArea())
EndIf

RestArea(aArea)

Return lRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040ExpFil
Retorna express�o para utiliza��o na query que seleciona os clientes para apura��o

@type 		Function
@author 	paulo.barbosa
@since 		07/03/2018
@version 	12.1.14
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040ExpFil(cTabela, cAliasTab, cTabBase)

Local cCompE := FWModeAccess(cTabela, 1)
Local cCompU := FWModeAccess(cTabela, 2)
Local cCompF := FWModeAccess(cTabela, 3)
Local nTam	 := FWSizeFilial()
Local cRet	 := '%%'

If cCompE == 'C' //Empresa Compartilhada
	nTam -= Len(FWSM0Layout( , 1))                                                                                                          
EndIf

If cCompU == 'C' //Unidade Compartilhada
	nTam -= Len(FWSM0Layout( , 2))
EndIf

If cCompF == 'C' //Filial Compartilhada
	nTam -= Len(FWSM0Layout( , 3))
EndIf

If nTam > 0
	cRet := "%SUBSTRING(" + cAliasTab + "." + PrefixoCpo(cTabela) + "_FILIAL, 1, " + ALLTRIM(STR(nTam))+ " ) = SUBSTRING( " + cTabBase + "_FILIAL, 1, " + ALLTRIM(STR(nTam)) + ") AND %"
EndIf

Return cRet

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TurGetIdAC
@author osmar.junior
@since 26/11/2018
@version 1.0
@return ${return}, ${return_description}
@param cFilialApu, characters, descricao
@param cCodApu, characters, descricao
@param cCodCli, characters, descricao
@param cLojCli, characters, descricao
@type function
/*/
//+----------------------------------------------------------------------------------------
Static Function TurGetIdAC(oModel)

Local oModelG48  := oModel:GetModel('G48A_DETAIL')
Local oModelG48B := oModel:GetModel('G48B_DETAIL')
Local oModelG6O  := oModel:GetModel('G6O_DETAIL')
Local oModelG6M  := oModel:GetModel('G6M_DETAIL')
Local oModelG82  := oModel:GetModel('G82_DETAIL')
Local nApu		 := 0
Local nID		 := 0
Local aRet		 := {}
Local nAcordo	 := 0
Local nItem		 := 0

For nApu := 1 to oModelG6M:Length()
	oModelG6M:GoLine(nApu)
	
	For nAcordo := 1 to oModelG6O:Length()
		oModelG6O:GoLine(nAcordo)
		
		If !oModelG6O:IsDeleted()
			For nItem := 1 to oModelG48:Length()
				oModelG48:GoLine(nItem)	
				nID := oModelG48:GetDataId(nItem)
				If !Empty(nID)
					AADD( aRet, nID )
				EndIf
			Next nItem									
		EndIf				
	
	Next nAcordo
	
Next nApu

//+------------------------------------------------------
//| Verifica apura��o de servi�o pr�prio
//+------------------------------------------------------
For nApu := 1 to oModelG82:Length()
	oModelG82:GoLine(nApu)
	
	If !oModelG82:IsDeleted()
		For nItem := 1 to oModelG48B:Length()
			oModelG48B:GoLine(nItem)
			nID := oModelG48B:GetDataId(nItem) 
			If !Empty(nID)
				AADD( aRet, nID )
			EndIf
		Next nItem		
	EndIf	
		
Next nApu

Return aRet 

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} DelAcApu
@author osmar.junior
@since 26/11/2018
@version 1.0
@return ${return}, ${return_description}
@param cFilialApu, characters, descricao
@param cCodApu, characters, descricao
@param cCodCli, characters, descricao
@param cLojCli, characters, descricao
@type function
/*/
//+----------------------------------------------------------------------------------------
Static Function DelAcApu( aG48ID, cCodApu )

Local aArea := GetArea()
Local nX	:= 0

G4C->(DbSetOrder(1))
G3R->(DbSetOrder(1))

For nX := 1 To Len(aG48ID)
	G48->(DbGoto(aG48ID[nX]))
    If G3R->(!DbSeek(G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ))) 
        RecLock("G48", .F.)
		G48->(DbDelete())
		G48->(MsUnlock()) 

		If G4C->(DbSeek(G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA)))
			If G4C->G4C_ACERTO == '1' //DELETAR O ACORDO SE FOR DE ACERTO E N�O POSSUIR MAIS O ACERTO
				While (G4C->(!EoF()) .And. G4C->(G4C_FILIAL + G4C_NUMID + G4C_IDITEM + G4C_NUMSEQ + G4C_APLICA + G4C_NUMACD) == G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD))
					If G4C->G4C_CLIFOR == '1'
						RecLock("G4C", .F.)
						G4C->(DbDelete())
						G4C->(MsUnlock()) 				
					EndIf
					G4C->(DbSkip())
				EndDo
			EndIf
		EndIf
	EndIf
Next nX

RestArea(aArea)

Return

//+----------------------------------------------------------------------------------------
/*/{Protheus.doc} TA040G48Id
Fun��o que retorna o RECNO do registro da tabela G48

@author osmar.junior
@since 29/11/2018
@version 1.0
@return ${return}, ${return_description}
@param oModelAux, object, descricao
@param aRet, array, descricao
@type function
/*/
//+----------------------------------------------------------------------------------------
Static Function TA040G48Id(oModelAux, aRet)

If !Empty(oModelAux:GetDataId())
	aAdd(aRet, oModelAux:GetDataId())
EndIf
	
Return