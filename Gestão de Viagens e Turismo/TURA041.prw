#include "TURA041.CH"
#Include 'PROTHEUS.CH'
#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

Static Ta041Inclui 		:= .f. 

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TURA041
Programa respons�vel por efetuar cadastro dos layouts de Submodelos de MVC. Originalmente,
foi idealizado para efetuar o cadastro de Layout para a Concilia��o (tabela G6I) 	

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TURA041()

Local oBrowse := FWMBrowse():New()

oBrowse:SetAlias("G8A")
oBrowse:SetDescription(STR0001)//"Cadastro de Layouts"
oBrowse:bIniWindow := {|| TA41InitBrw(oBrowse), G8A->(DbGotop()), oBrowse:GoTop(.t.)}
oBrowse:Activate()

Return()

//------------------------------------------------------------------------------------------------------
/*{Protheus.doc} ModelDef

Defini��o do Modelo do MVC

@return: 
	oModel:	Object. Objeto da classe MPFormModel

@sample: oModel := ModelDef()

@author Fernando Radu Muscalu

@since 08/06/2016
@version 1.0
*/
//------------------------------------------------------------------------------------------------------

Static Function ModelDef()

Local oModel
Local oStrCab	:= FWFormStruct(1, "G8A")//FWFormModelStruct():New()
Local oStrGrd	:= FWFormStruct(1, "G9M")

Local aRelation	:= {}
Local aField	:= aClone(oStrGrd:GetFields())

Local nI		:= 0

Local bInitPad
Local bCpoChange

//Inlus�o de novos campos na estrutura - Begin 

bInitPad := FwBuildFeature( STRUCT_FEATURE_INIPAD,'"PMSSETAUP"')

oStrGrd:AddField("",;		//Descri��o (Label) do campo
				 STR0002,;			//Descri��o Tooltip do campo//"Acima"
				 "G9M_UP",;			//Identificador do campo
				 "C",;				//Tipo de dado
				 15,;				//Tamanho
				 0,;				//Decimal
				 nil,;				//Valid do campo
				 nil,;				//When do campo
				 {},;				//Lista de Op��es (Combo)
				 .f.,;				//Indica se campo � obrigat�rio
				 bInitPad,;				//inicializador Padr�o
				 .f.,;				//Indica se o campo � chave
				 .f.,;				//Indica se o campo pode receber um valor em uma opera��o update
				 .T.)				//Indica se o campo � virtual

bInitPad := FwBuildFeature( STRUCT_FEATURE_INIPAD,'"PMSSETADOWN"')
oStrGrd:AddField("",;		//Descri��o (Label) do campo
				 STR0003,;			//Descri��o Tooltip do campo//"Abaixo"
				 "G9M_DOWN",;		//Identificador do campo
				 "C",;				//Tipo de dado
				 15,;				//Tamanho
				 0,;				//Decimal
				 nil,;				//Valid do campo
				 nil,;				//When do campo
				 {},;				//Lista de Op��es (Combo)
				 .f.,;				//Indica se campo � obrigat�rio
				 bInitPad,;				//inicializador Padr�o
				 .f.,;				//Indica se o campo � chave
				 .f.,;				//Indica se o campo pode receber um valor em uma opera��o update
				 .T.)				//Indica se o campo � virtual


//End

oModel := MPFormModel():New("TURA041")

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
bCpoChange := {|oSMdl,cAct,cCpo,cCon,x|	x:= TA41Valid(oSMdl,cAct,cCpo,cCon),;
				Iif(x,TA41LoadGrd(oSMdl,cAct,cCpo,cCon),x)}

oModel:AddFields("G8AMASTER", , oStrCab, bCpoChange)

// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
oModel:AddGrid("G9MDETAIL", "G8AMASTER", oStrGrd)

//Define as descri��es dos submodelos
oModel:SetDescription(STR0004)//"Cadastro de Layout"
oModel:GetModel("G8AMASTER"):SetDescription(STR0005)//"Layout - Modelo de Dados"
oModel:GetModel("G9MDETAIL"):SetDescription("Campos")

//Bloqueia inser��o e exclus�o de linhas
oModel:GetModel('G9MDETAIL'):SetNoInsertLine(.T.)
oModel:GetModel('G9MDETAIL'):SetNoDeleteLine(.T.)

//Regras
oModel:AddRules("G8AMASTER","G8A_SUBMDL","G8AMASTER","G8A_MODEL",3)

//Relacionamentos
aRelation := {	{ "G9M_FILIAL", "XFilial('G9M')" },; 
				{ "G9M_CODIGO", "G8A_CODIGO" }}
				
oModel:SetRelation( "G9MDETAIL", aRelation, G9M->(IndexKey(1)) )

Return(oModel)

//------------------------------------------------------------------------------------------------------
/*{Protheus.doc} ViewDef

Defini��o da View do MVC

@return: 
	oView:	Object. Objeto da classe FWFormView

@sample: oView := ViewDef()

@author Fernando Radu Muscalu

@since 18/08/2015
@version 1.0
*/
//------------------------------------------------------------------------------------------------------
Static Function ViewDef()

Local oStrCab	:= FWFormStruct(2, "G8A")//FwFormViewStruct():New()
Local oStrGrd	:= FWFormStruct(2, "G9M")
Local oModel	:= ModelDef()
Local oView		:= Nil

Local aField	:= aClone(oStrGrd:GetFields())

Local nI		:= 0

//Inlus�o de novos campos na estrutura - Begin 
oStrGrd:AddField(	"G9M_UP",;			// [01] C Nome do Campo
					"01",;				// [02] C Ordem
					" ",; 				// [03] C Titulo do campo
					"",; 			// [04] C Descri��o do campo//"Marcar Item"
					{""} ,;		// [05] A Array com Help//"Marcar Item"
					"Get",; 			// [06] C Tipo do campo - GET, COMBO OU CHECK
					"@BMP",;				// [07] C Picture
					NIL,; 				// [08] B Bloco de Picture Var
					"",; 				// [09] C Consulta F3
					.F.,; 				// [10] L Indica se o campo � edit�vel
					NIL, ; 				// [11] C Pasta do campo
					NIL,; 				// [12] C Agrupamento do campo
					{},; 				// [13] A Lista de valores permitido do campo (Combo)
					NIL,; 				// [14] N Tamanho Maximo da maior op��o do combo
					NIL,;				// [15] C Inicializador de Browse
					.t.)				// [16] L Indica se o campo � virtual

oStrGrd:AddField(	"G9M_DOWN",;		// [01] C Nome do Campo
					"02",;				// [02] C Ordem
					" ",; 				// [03] C Titulo do campo
					"",; 			// [04] C Descri��o do campo//"Marcar Item"
					{""} ,;		// [05] A Array com Help//"Marcar Item"
					"Get",; 			// [06] C Tipo do campo - GET, COMBO OU CHECK
					"@BMP",;				// [07] C Picture
					NIL,; 				// [08] B Bloco de Picture Var
					"",; 				// [09] C Consulta F3
					.F.,; 				// [10] L Indica se o campo � edit�vel
					NIL, ; 				// [11] C Pasta do campo
					NIL,; 				// [12] C Agrupamento do campo
					{},; 				// [13] A Lista de valores permitido do campo (Combo)
					NIL,; 				// [14] N Tamanho Maximo da maior op��o do combo
					NIL,;				// [15] C Inicializador de Browse
					.t.)				// [16] L Indica se o campo � virtual
					
//End

//Ajustes nas estruturas - Begin
oStrCab:SetProperty("G8A_MODEL", MVC_VIEW_CANCHANGE, INCLUI)
oStrCab:SetProperty("G8A_SUBMDL", MVC_VIEW_CANCHANGE, INCLUI)
oStrGrd:RemoveField("G9M_CODIGO")
//End

oView := FWFormView():New()

// Define qual o Modelo de dados ser� utilizado
oView:SetModel(oModel)

//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
oView:AddField('VW_G8AMASTER', oStrCab, 'G8AMASTER')
oView:AddGrid('VW_G9MDETAIL', oStrGrd, 'G9MDETAIL')

// Criar "box" horizontal para receber algum elemento da view
oView:CreateHorizontalBox('CABEC', 25)
oView:CreateHorizontalBox('CORPO', 75)

// Relaciona o ID da View com o "box" para exibicao
oView:SetOwnerView('VW_G8AMASTER', 'CABEC')
oView:SetOwnerView('VW_G9MDETAIL', 'CORPO')

//Habitila os t�tulos dos modelos para serem apresentados na tela
oView:EnableTitleView('VW_G8AMASTER')
oView:EnableTitleView('VW_G9MDETAIL')

oView:GetViewObj("G9MDETAIL")[3]:SetDoubleClick({|oVw,cCpo,nLn,nLn2| TA41DblClick(oVw,cCpo,nLn,nLn2) })
oView:SetAfterViewActivate({|oVw,cId| TA41DescCpo(oVw, cId) })

Return(oView)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} MenuDef
Fun��o que define o menu do programa

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina Title STR0006 	Action "TA041Mnt(1)"	OPERATION 2 ACCESS 0 //STR0006//"Visualizar"
ADD OPTION aRotina Title STR0007		Action "TA041Mnt(3)"	OPERATION 3 ACCESS 0 //"Gerar" //"Incluir"
ADD OPTION aRotina Title STR0008 		Action "TA041Mnt(4)"	OPERATION 4 ACCESS 0 //STR0008//"Alterar"
ADD OPTION aRotina Title STR0009 		Action "TA041Mnt(5)" 	OPERATION 5 ACCESS 0 //STR0009//"Excluir"
	
Return(aRotina)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41InitBrw
Fun��o respons�vel por efetuar uma pr�-carga na inicializa��o 	

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA41InitBrw(oBrowse)

Local nX		:= 0
Local nI		:= 0

Local aInit		:= {}
Local aField	:= {}
Local aMVC		:= {}

Local oModel	:= FwLoadModel("TURA041")

Local lOk		:= .f.

aAdd(aInit, {"TURA038","G6IDETAIL",STR0010})//"Fatura A�rea"
aAdd(aInit, {"TURA039","G6I_ITENS",STR0011})//"Concilia��o A�rea"

For nI := 1 to Len(aInit)
	
	G8A->(DbSetOrder(2)) //G8A_FILIAL + G8A_MODEL + G8A_SUBMDL + G8A_NMLAY 

	If ( !G8A->(DbSeek(XFilial("G8A") + Padr(aInit[nI,1],TamSx3("G8A_MODEL")[1]) + Padr(aInit[nI,2],TamSx3("G8A_SUBMDL")[1]) + Padr(aInit[nI,3],TamSx3("G8A_NMLAY")[1]) )) )
				
		oModel:SetOperation(MODEL_OPERATION_INSERT)
		
		oModel:Activate()
							
		lOk :=	oModel:GetModel("G8AMASTER"):SetValue("G8A_NMLAY", aInit[nI,3]) .And.;
				oModel:GetModel("G8AMASTER"):SetValue("G8A_MODEL", aInit[nI,1]) .And.;
				oModel:GetModel("G8AMASTER"):SetValue("G8A_SUBMDL", aInit[nI,2])
		
		If ( lOk )
			
			If ( oModel:VldData() )
				lOk := oModel:CommitData()				
			Else
				lOk := .f.
			Endif
			
		Endif 
		
		oModel:DeActivate()
					
	Endif
			
Next nI

G8A->(DbGoTop())

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41DblClick
Fun��o respons�vel por a troca de dados entre duas linhas do grid G9MDETAIL. H� duas imagens
neste grid que funcionam como esp�cies de bot�es. Estas imagens s�o representadas pos duas 
setas, uma que aponta para cima e outra para baixo. Se a a��o do bot�o foi a seta para baixo,
ent�o o registro desta linha ir� trocar com o registro de linha logo abaixo. Se a seta que
foi acionada � a que aponta para cima, ent�o ser� trocado pelo registro antecessor a este. 	

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA41DblClick(oViewGrd,cCampo,nLine,nLine2)

Local aBkpLine	:= {}
Local aActualLn	:= {}
Local aField 	:= oViewGrd:GetModel("G9MDETAIL"):GetStruct():GetFields()

Local lUp		:= .f.
Local lDown		:= .f.

Local nI		:= 0
Local nDif		:= 0

Local oView		:= FwViewActive()

If ( Alltrim(cCampo) == "G9M_UP" )

	If (nLine > 1 )		
		lUp := .t.
	Endif

ElseIf ( Alltrim(cCampo) == "G9M_DOWN" )
	
	If (nLine < oViewGrd:GetModel("G9MDETAIL"):Length() )		
		lDown := .t.	
	Endif
	
Endif

If ( lUp .Or. lDown )
	
	If ( lUp )
		nDif := -1
	ElseIf ( lDown )
		nDif := 1
	Endif		

	For nI := 1 to Len(aField)
		
		If ( aField[nI,3] == "G9M_ORDEM" )
			aAdd(aActualLn, {aField[nI,3], oViewGrd:GetModel("G9MDETAIL"):GetValue(aField[nI,3],nLine+nDif)})
		Else
			aAdd(aActualLn, {aField[nI,3], oViewGrd:GetModel("G9MDETAIL"):GetValue(aField[nI,3],nLine)})
		Endif
			
	Next nI
	
	For nI := 1 to Len(aField)
		
		If ( aField[nI,3] == "G9M_ORDEM" )
			aAdd(aBkpLine, {aField[nI,3], oViewGrd:GetModel("G9MDETAIL"):GetValue(aField[nI,3],nLine)})
		Else
			aAdd(aBkpLine, {aField[nI,3], oViewGrd:GetModel("G9MDETAIL"):GetValue(aField[nI,3],nLine+nDif)})
		Endif
						
	Next nI
	
	oViewGrd:GetModel("G9MDETAIL"):GoLine(nLine+nDif)
	
	For nI := 1 to Len(aField)
		oViewGrd:GetModel("G9MDETAIL"):LoadValue(aField[nI,3], aActualLn[nI,2] )
	Next nI
	
	oViewGrd:GetModel("G9MDETAIL"):GoLine(nLine)
	
	For nI := 1 to Len(aField)
		oViewGrd:GetModel("G9MDETAIL"):LoadValue(aField[nI,3], aBkpLine[nI,2] )
	Next nI
	
	oViewGrd:GetModel("G9MDETAIL"):GoLine(nLine+nDif)
	oView:Refresh("VW_G9MDETAIL")
	oViewGrd:oBrowse:GoTo(nLine+nDif)
		
Endif

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41Valid
Fun��o respons�vel por efetuar as valida��es do SubModelo G8AMASTER 	

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Static Function TA41Valid(oSubMdl,cAction,cCampo,cConteudo,aMVC)

Local cModel	:= ""
Local cSubMdl	:= ""
Local cNxtAlias	:= ""

Local lRet := .T.

If (cAction == "SETVALUE" .AND. !Empty(cConteudo) )
	
	If ( oSubMdl:GetModel():GetOperation() == MODEL_OPERATION_INSERT )
		 
		 If ( cCampo == "G8A_MODEL" )
		 	
		 	cModel	:= cConteudo
		 	cSubMdl	:= oSubMdl:GetValue("G8A_SUBMDL") 
		 	
		 	If ( !Empty(cSubMdl) .And. !TA41ChkView(Alltrim(cModel),Alltrim(cSubMdl))[1] )
		 		lRet := .f.
		 		FwAlertError(STR0012 + Alltrim(cModel) + " n�o cont�m o submodelo " + Alltrim(cSubMdl) + ".",STR0013)//"O Modelo de Dados "//"Dados Incorreto"
		 	ElseIf ( Empty(cSubMdl) .And. !TA41ChkView(Alltrim(cModel))[1] )
		 		lRet := .f.	
		 		FwAlertError(STR0012 + Alltrim(cModel) + STR0015,STR0013)//"O Modelo de Dados "//" n�o existe."//"Dados Incorreto"
		 	Endif
		 		
		 ElseIf ( cCampo == "G8A_SUBMDL" )
		 	
		 	cModel	:= oSubMdl:GetValue("G8A_MODEL")
		 	cSubMdl	:= cConteudo
		 	
		 	If ( !TA41ChkView(Alltrim(cModel), Alltrim(cSubMdl))[1] )
		 		lRet := .f.
		 		FwAlertError(STR0017 + Alltrim(cSubMdl) + STR0018 + Alltrim(cModel) + ".",STR0013)//"O Submodelo de Dados "//" n�o existe dentro do Modelo de dados "//"Dados Incorreto"
		 	Endif
		 	
		 Endif
		
	Endif

Endif

Return(lRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41GetInfoCpo
Fun��o respons�vel pela descri��o/t�tulo do campo. Utilizado no dicion�rio SX3 - campo
X3_RELACAO (tabela G8A) 	

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TA41GetInfoCpo(cField, cConteudo)

Local cRet		:= ""

Local aMVC		:= {}
Local aField	:= {}

Local oModel	:= FwModelActive() 

Default cConteudo := IIf(Len(oModel:GetModel("G9MDETAIL"):aDataModel) > 0, oModel:GetModel("G9MDETAIL"):GetValue("G9M_CAMPO"), "") 

If ( Len(oModel:GetModel("G8AMASTER"):aDataModel) > 0 )
	
	aMVC := TA41ChkView(oModel:GetModel("G8AMASTER"):GetValue("G8A_MODEL"),oModel:GetModel("G8AMASTER"):GetValue("G8A_SUBMDL"))
	
	If ( aMVC[1] )
		
		aField := aMVC[3]:GetFields()
		
		nP := aScan(aField, {|x| Alltrim(x[1]) == Alltrim(cConteudo)})
		
		If ( nP > 0 )
			If ( cField == "G9M_TITULO" )
				cRet := Alltrim(aField[nP,3])
			ElseIf ( cField == "G9M_DESCRI" )
				
				If ( !Empty(aField[nP,4]) )
					cRet := Alltrim(aField[nP,4])
				Else
					cRet := STR0020//"Cpo. Sem Identifica��o"
				Endif
					
			Endif
		Endif
		
	Endif
	
Endif	 

Return(cRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41LoadGrd
Fun��o que efetua a carga do Grid G9MDETAIL. Esta fun��o � chamada no valid dos campos 
G8A_MODEL e G8A_SUBMDL, ap�s a valida��o de fato de cada campo.

A fun��o busca o submodelo da view para carregar a estrutura deste, listando desta forma
todos os campos deste submodelo. 

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Static Function TA41LoadGrd(oSubMdl,cAction,cCampo,cConteudo,aMVC)

Local cModel	:= ""
Local cSubModel	:= ""
Local cDescricao:= ""
	
Local nX 		:= 0 
Local nLength	:= 0

Local lLoad	:= .F.
Local lOk	:= .T.

If ( cAction == "SETVALUE" )
	
	If ( oSubMdl:GetModel():GetOperation() == MODEL_OPERATION_INSERT .And.; 
		 cCampo $ "G8A_MODEL|G8A_SUBMDL" .And. !Empty(cConteudo) )
		
		If ( cCampo == "G8A_MODEL" )
				
			If ( !Empty(oSubMdl:GetValue("G8A_SUBMDL")) )
			
				cModel 		:= Alltrim(cConteudo)
				cSubModel	:= Alltrim(oSubMdl:GetValue("G8A_SUBMDL"))
				
				lLoad := .T.	
			
			Endif
						 
		ElseIf 	( cCampo == "G8A_SUBMDL"  )   
			
			If ( !Empty(oSubMdl:GetValue("G8A_MODEL")) )
				
				cModel 		:= Alltrim(oSubMdl:GetValue("G8A_MODEL"))
				cSubModel	:= Alltrim(cConteudo)
			
				lLoad := .T.
				
			Endif
			
		Endif
			  
	Endif
			 
	If ( lLoad )
		
		oSubMdl:GetModel():GetModel("G9MDETAIL"):SetNoInsertLine(.F.)
		
		If !( oSubMdl:GetModel():GetModel("G9MDETAIL"):Length() == 1 .And.; 
			!Empty(oSubMdl:GetModel():GetModel("G9MDETAIL"):GetValue("G9M_CAMPO")) )
			
			oSubMdl:GetModel():GetModel("G9MDETAIL"):ClearData()
			
		Endif
		
		aMVC := TA41ChkView(Alltrim(cModel), Alltrim(cSubModel))
			
		aField := aMVC[3]:GetFields()
		
		aSort(aField,,,{|x,y| x[2] < y[2]})
		
		For nX := 1 to Len(aField)
			
			If (nX > 1 )
				
				nLength := oSubMdl:GetModel():GetModel("G9MDETAIL"):Length() 
				
				oSubMdl:GetModel():GetModel("G9MDETAIL"):AddLine()
				
				If (nLength == oSubMdl:GetModel():GetModel("G9MDETAIL"):Length())
					Exit
				Endif
			
			Else
				
				lOk :=	oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_UP", "PMSSETAUP") .And.;
						oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_DOWN", "PMSSETADOWN")
						
				If ( !lOk )
					Exit
				Endif
							
			Endif	 
			
			If ( !Empty(aField[nX,4]) )
				cDescricao := Alltrim(aField[nX,4])
			Else
				cDescricao := STR0020
			Endif
			
			lOk :=	oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_CODIGO", oSubMdl:GetValue("G8A_CODIGO")) .And.;
					oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_ORDEM", StrZero(nX,TamSx3("G9M_ORDEM")[1])) .And.;
					oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_CAMPO", aField[nX,1])  .And.;
					oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_TITULO", aField[nX,3]) .And.;
					oSubMdl:GetModel():GetModel("G9MDETAIL"):SetValue("G9M_DESCRI", cDescricao)
			
			If ( !lOK )
				Exit
			Endif
					
		Next nX
			
		oSubMdl:GetModel():GetModel("G9MDETAIL"):GoLine(1)
		oSubMdl:GetModel():GetModel("G9MDETAIL"):SetNoInsertLine(.T.)
		
	Endif
			
Endif

Return(lOk)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41DescCpo
Fun��o que carrega T�tulo e Descri��o dos Campos (G9M_TITULO e G9M_DESCRI) no Grid.  

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Static Function TA41DescCpo(oVw, cId)

Local nI	:= 0

If ( oVw:GetModel():GetOperation() <> MODEL_OPERATION_INSERT )

	For nI := 1 to oVw:GetModel("G9MDETAIL"):Length()
		oVw:GetModel("G9MDETAIL"):GoLine(nI)
		oVw:GetModel("G9MDETAIL"):ForceValue("G9M_TITULO",TA41GetInfoCpo("G9M_TITULO",oVw:GetModel("G9MDETAIL"):GetValue("G9M_CAMPO")))
		oVw:GetModel("G9MDETAIL"):ForceValue("G9M_DESCRI",TA41GetInfoCpo("G9M_DESCRI",oVw:GetModel("G9MDETAIL"):GetValue("G9M_CAMPO")))
	Next nI
	
	oVw:GetModel("G9MDETAIL"):GoLine(1)
	oVw:Refresh()
	
EndIf

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41ChkView
Fun��o que checa se Existe o MVC e o submodelo. Existindo, � retornado os objetos da View 
e da Estrutura da View de acordo com o submodelo passado    

@type 		Function
@author 	Fernando Radu Muscalu
@since 		08/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TA41ChkView(cId,cSubId) 
	
Local oModel
Local oView
Local oStruct

Local lRet	:= .t.

Local aRet	:= {}

Local bError := ErrorBlock({|e|  lRet := .F.,  MsgAlert(STR0023 +chr(10)+ e:Description)})

Default cSubId := ""

Begin Sequence

If ( !Empty(cId) )

	oView := FwLoadView(Alltrim(cId))
	
	If ( ValType(oView) == "O" )
		
		If ( !Empty(cSubId) )
			
			If ( Valtype(oView:GetModel(Alltrim(cSubId))) == "O" )
			
				oStruct := oView:GetViewObj(Alltrim(cSubId))[3]:GetStruct()
			
				If ( ValType(oStruct) <> "O" )
					lRet := .f.
				Endif
				
			Else
				lRet := .f.
			Endif
		
		EndIf
		
	Else
		lRet := .f.	
	Endif

Endif

End Sequence

ErrorBlock(bError)

aRet := {lRet,oView,oStruct}

Return(aRet)

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA041Mnt
Fun��o que � chamada pelo menudef e � respons�vel pela manuten��o do cadastro. 
Inclus�o, Altera��o e Exclus�o    

@type 		Function
@author 	Fernando Radu Muscalu
@since 		13/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Function TA041Mnt(nOpc)

Local oModel	:= FwLoadModel("TURA041")

Local nOpcA		:= 0

If ( nOpc == MODEL_OPERATION_INSERT )
	Ta041Inclui := .t.
Else
	Ta041Inclui := .f.
Endif

oModel:SetOperation(nOpc)
oModel:Activate()

nOpcA := FWExecView(STR0022,"TURA041",nOpc,,{|| .T.},,,,,,,oModel)//"Layout"

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41ViewOrder
Fun��o que � executada na montagem da View dos programas TURA038 - Fatura A�rea e TURA039 -
Concilia��o A�rea. 

Esta fun��o poder�, eventualmente, ser chamada em outros ModelDef() de diversos programas

@type 		Function
@author 	Fernando Radu Muscalu
@since 		13/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------

Function TA41ViewOrder(oStrView,cModel,cSubModel,cCodLay)

Local aLayout := {}

Default cModel 		:= Alltrim(FunName())
Default cSubModel	:= ""
Default cCodLay		:= ""

If ( !FwIsInCallStack("TURA041") )

	If ( Len(aLayout := TA41ExistLayout(cModel,cSubModel,cCodLay)) > 0 )
		
		Pergunte("TURA041",.f.)
			
		If (!Empty(cCodLay) .and. aScan(aLayout, {|x| Alltrim(x[1]) == Alltrim(cCodLay) }) > 0 ) .Or. ;
		   (!Empty(mv_par01) .and. aScan(aLayout, {|x| Alltrim(x[1]) == Alltrim(mv_par01) }) > 0 )
		
			G9M->(DbSetOrder(1))//G9M_FILIAL + G9M_CODIGO + G9M_ORDEM + G9M_CAMPO
			
			If ( G9M->(DbSeek(XFilial("G9M") + Padr(IIF(Empty(cCodLay), mv_par01, cCodLay), TamSx3("G9M_CODIGO")[1]))) )
			
				While ( G9M->(!EoF()) .And. Alltrim(IIF(Empty(cCodLay), mv_par01, cCodLay)) == Alltrim(G9M->G9M_CODIGO) )
					
					If ( oStrView:HasField(Alltrim(G9M->G9M_CAMPO)) )
						oStrView:SetProperty(Alltrim(G9M->G9M_CAMPO), MVC_VIEW_ORDEM, Alltrim(G9M->G9M_ORDEM) )
					Endif
				
					G9M->(DbSkip())
				EndDo
						
			Endif
			
		Endif
		
	Endif

Endif

Return()

//+----------------------------------------------------------------------------------------
/*{Protheus.doc} TA41ExistLayout
Fun��o que efetua a busca do Layout de acordo com o Model (ou programa) passado via par�metro
e o submodelo de dados. esta fun��o � auxiliar a fun��o TA41ViewOrder()

@type 		Function
@author 	Fernando Radu Muscalu
@since 		15/06/2016
@version 	12.1.7
*/
//+----------------------------------------------------------------------------------------
Static Function TA41ExistLayout(cModel,cSubModel,cCodLay)

Local cWhere	:= ""
Local cNxtAlias	:= GetNextAlias()

Local aRet		:= {}

If ( !Empty(cCodLay) )
	cWhere := "%G8A_CODIGO = '" + cCodLay + "' "
Else
	
	cWhere := "%G8A_MODEL = '" + cModel + "' "
	
	If ( !Empty(cSubModel) )
		cWhere += " AND G8A_SUBMDL = '" + cSubModel + "' "
	Endif
	
Endif	

cWhere += "%"

BeginSQL Alias cNxtAlias

	SELECT
		DISTINCT
		G8A_CODIGO,
		G8A_MODEL,
		G8A_SUBMDL
	FROM
		%Table:G8A% G8A
	WHERE
		G8A.%NotDel% AND
		G8A_FILIAL = %XFilial:G8A% AND
		%Exp:cWhere%
			
EndSQL

While ( !(cNxtAlias)->(EoF()) )
	
	aAdd(aRet, (cNxtAlias)->({G8A_CODIGO,G8A_MODEL,G8A_SUBMDL}))
	
	(cNxtAlias)->(DbSkip())
	
EndDo

(cNxtAlias)->(DbCloseArea())

Return(aRet)
