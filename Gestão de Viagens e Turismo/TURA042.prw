#INCLUDE "TURA042.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

Static oCacheCalc   := Nil
Static lAltTarifa 			//Indica se houve altera��o manual na tabela G44 - Tarifas
Static lAltTaxas			//Indica se houve altera��o manual na tabela G46 - Taxas
Static lAltExtras			//Indica se houve altera��o manual na tabela G47 - Extras
Static lAltImpostos			//Indica se houve altera��o manual na tabela G49 - Impostos
Static lAplAcordFin			//Indica que dever� ser aplicado os acordos financeiros antes da grava��o dos dados
Static lAltSeg				//Indica se houve altera��o no per�odo de reserva do segmento
Static lAllDelete	 		// Flag para excluir sem valida��o
 
/*/{Protheus.doc} TuraEfetiv
Fun��o para chamada da efetiva��o da concilia��o
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param cOpc, character, Op��o do tipo da opera��o [1] = Gerar; [2] = Estornar
@param cTipo, character, Tipo da concilia��o: [1] = Terrestre; [2]  = A�reo; [3] = Reembolso
/*/
Function TuraEfetiv( cOpc, cTipo )

Local lRet := .T.

	If cOpc == "1" //Gerar
		If cTipo == "1" .AND. !IsInCallStack("TA042AEfet") .AND. !IsInCallStack("TA042ADEfe") //Terrestre
			If G8C->G8C_STATUS == "1"
				Help(,,"TA042EFET",,STR0001,1,0) //"Concilia��o j� efetivada."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0002, {G8C->G8C_CONCIL})) // "Confirma a efetiva��o da concilia��o: #1 ?"
				FwMsgRun( , {|| TA042AEfet()}, , I18N(STR0062, {G8C->G8C_CONCIL}))		// "Efetivando a concilia��o #1, aguarde..."	
			EndIf

		ElseIf cTipo == "2" //Aereo
			If G6H->G6H_EFETIV == "1"
				Help(,,"TA042EFET",,STR0061,1,0) //	"Fatura A�rea j� conciliada."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0060, {G6H->G6H_FATURA})) //	"Confirma a efetiva��o da fatura a�rea: #1 ?"
				FwMsgRun( , {|| TA039Efet()}, , I18N(STR0063, {G6H->G6H_FATURA}))		// "Efetivando a fatura a�rea #1, aguarde..."	
			EndIf
		
		ElseIf cTipo == "3" .AND. !IsInCallStack("TA042AEfet") .AND. !IsInCallStack("TA042ADEfe") //Reembolso
			If G8C->G8C_STATUS == "1"
				Help(,,"TA042EFET",,STR0001,1,0) //"Concilia��o j� efetivada."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0002, {G8C->G8C_CONCIL})) //	"Confirma a efetiva��o da concilia��o: #1 ?"
				FwMsgRun( , {|| TA042REfet( cOpc )}, , I18N(STR0062, {G8C->G8C_CONCIL}))		// "Efetivando a concilia��o #1, aguarde..."	
			EndIf
		EndIf

	ElseIf cOpc == "2" //Estornar

		If G8C->G8C_DTEFET > dDataBase
			FwAlertHelp(I18N(STR0022, {Day2Str(G8C->G8C_DTEFET) + '/' + Month2Str(G8C->G8C_DTEFET) + '/' + Year2Str(G8C->G8C_DTEFET), dDataBase}), ;	// "N�o � poss�vel desfazer uma efetiva��o realizada posteriormente (#1) a data base do sistema (#2)." 
						STR0023, ;		// "Ajuste a data base do sistema e tente desfazer a efetiva��o da concilia��o novamente." 
						'TA042AIFCLI' )
			lRet := .F.
		
		ElseIf cTipo == "1" .AND. !IsInCallStack("TURA042R") // Terrestre
			If G8C->G8C_STATUS == "2"
				Help(,,"TA042EFET",,STR0003,1,0) //"Concilia��o n�o efetivada."
				lRet := .F.
			ElseIf G8C->G8C_FILEFE <> cFilAnt
				Help(,,"TA042EFFIL",,i18N( STR0021,{ G8C->G8C_FILEFE } ),1,0) //"A concilia��o foi efetiva pela filial #1[ Filial ]# e somente pode ser estornada por ela."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0004, {G8C->G8C_CONCIL})) // "Confirma o estorno da efetiva��o da concilia��o: #1 ?"
				FwMsgRun( , {|| TA042ADEfe()}, , I18N(STR0065, {G8C->G8C_CONCIL}))		// "Estornando a efetiva��o da concilia��o #1, aguarde..."
			EndIf
		
		ElseIf cTipo == "2" //Aereo
			If G6H->G6H_EFETIV == "2"
				Help(,,"TA042EFET",,STR0003,1,0) //"Concilia��o n�o efetivada."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0064, {G6H->G6H_FATURA}))	// "Confirma o estorno da efetiva��o fatura a�rea: #1 ?"
				FwMsgRun( , {|| TA039DEfe()}, , I18N(STR0066, {G6H->G6H_FATURA}))		// "Estornando a efetiva��o da fatura a�rea #1, aguarde..."	
			EndIf	

		ElseIf cTipo == "3" .AND. !IsInCallStack("TURA042A")//Reembolso
			If G8C->G8C_STATUS == "2"
				Help(,,"TA042EFET",,STR0003,1,0) //"Concilia��o n�o efetivada."
				lRet := .F.
			ElseIf G8C->G8C_FILEFE <> cFilAnt
				Help(,,"TA042EFFIL",,i18N( STR0021,{ G8C->G8C_FILEFE } ),1,0) //"A concilia��o foi efetiva pela filial #1[ Filial ]# e somente pode ser estornada por ela."
				lRet := .F.
			ElseIf IsBlind() .Or. ApMsgYesNo( I18N(STR0004, {G8C->G8C_CONCIL})) //	"Confirma o estorno da efetiva��o da concilia��o: #1 ?"
				FwMsgRun( , {|| TA042REfet( cOpc )}, , I18N(STR0065, {G8C->G8C_CONCIL}))		// "Estornando a efetiva��o da concilia��o #1, aguarde..."
			EndIf
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} TA042GetVls
Fun��o que serializa todos os dados de um modelo e seus submodelos em um vetor
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param cIdCopy, character, Id do modelo que ser� serializado
@param lAllLines, booleano, Se for um grid, indica que dever� ser copiado todas as linhas
@param aNoCopy, array, Vetor com ID's que n�o ser�o copiados
@return array, Vetor com todos os dados do modelo e submodelos serializados
/*/
Function TA042GetVls( cIdCopy, lAllLines, aNoCopy )
	Local aRetValues	:= {cIdCopy,{}}
	Local aModelStruct 	:= {}
	Local aModelValues	:= {}
	Local aDependency	:= {}
	Local aValuesDep	:= {}
	Local nX			:= 0
	Local nY			:= 0
	Local nJ			:= 0
	Local oModel 		:= FwModelActive()
	Local oModelAux		:= oModel:GetModel( cIdCopy )

	DEFAULT lAllLines 	:= .F.
	DEFAULT aNoCopy		:= {}	

	If aScan( aNoCopy, {|x| x == cIdCopy} ) > 0
		Return aRetValues
	EndIf 

	aDependency  := oModel:GetDependency( cIdCopy )
	aModelStruct := oModelAux:GetStruct():GetFields()

	If lAllLines .And. oModelAux:ClassName() == "FWFORMGRID"
		For nX := 1 to oModelAux:Length()
			oModelAux:GoLine( nX )

			aModelValues := {}
			aValuesDep	 := {}

			If  ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
				For nY := 1 to len( aModelStruct )
					aAdd( aModelValues, { AllTrim( aModelStruct[nY][3] ), oModelAux:GetValue( aModelStruct[nY][3] ) } )
				Next

				For nJ := 1 to len( aDependency )
					aAdd( aValuesDep, TA042GetVls( aDependency[nJ][2], .T., aNoCopy)  )
				Next
			EndIf
			aAdd( aRetValues[2], {aModelValues, aValuesDep} )
		Next

	Else
		If ( oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty() ) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
			For nX := 1 to len( aModelStruct )
				aAdd( aModelValues, { AllTrim( aModelStruct[nX][3] ), oModelAux:GetValue( aModelStruct[nX][3] ) } )
			Next
		EndIf

		For nJ := 1 to len( aDependency )
			aAdd( aValuesDep, TA042GetVls( aDependency[nJ][2], .T., aNoCopy)  )
		Next

		aAdd( aRetValues[2], {aModelValues, aValuesDep} )
	EndIf

Return aRetValues

/*/{Protheus.doc} TA042SetVs
Fun��o para apendar ao modelo os dados seriados pela fun��o TA042GetVls
@type function
@author Anderson Toledo
@since 16/03/2016
@version 1.0
@param aCopyValues, array, vetor com valores serializados pela fun��o TA042GetVls
/*/
Function TA042SetVs( aCopyValues )
	Local oModel 	:= FwModelActive()
	Local nX 		:= 0
	Local nY 		:= 0
	Local lRet		:= .T.

	For nX := 1 to len( aCopyValues[2] )
		If len( aCopyValues[2][nX][1] ) > 0
			If oModel:GetModel( aCopyValues[1] ):ClassName() == "FWFORMGRID"
				lRet := TA042LoadV(oModel,aCopyValues[1],aCopyValues[2][nX][1], !oModel:GetModel( aCopyValues[1] ):IsEmpty() )
			Else
				lRet := TA042LoadV(oModel,aCopyValues[1],aCopyValues[2][nX][1], .F. )
			EndIf

			If lRet
				For nY := 1 to len( aCopyValues[2][nX][2] )
					lRet := TA042SetVs( aCopyValues[2][nX][2][nY] )
				Next
			EndIf
		EndIf
	Next

Return lRet

Static Function ModelDef()
	Local oModel	:= Nil
	Local oStruG8C 	:= FWFormStruct( 1, "G8C", /*bAvalCampo*/, /*lViewUsado*/ )	//Cabe�alho Conc. Fatura Terrestre
	Local oStruG3P	:= FwFormStruct( 1, "G3P",,.F. )				//Registro de venda
	Local oStruG3R	:= FwFormStruct( 1, "G3R",,.F. )				//Documento de reserva
	Local oStruG3Q	:= FwFormStruct( 1, "G3Q",,.F. )				//Item de Venda
	Local oStruG48A	:= FwFormStruct( 1, "G48",,.F. )				//Acordos Cliente (Pastas Abaixo)
	Local oStruG48AF:= FwFormStruct( 1, "G48",,.F. )				//Acordos Cliente Finalizados antes da concilia��o
	Local oStruG4CA	:= FwFormStruct( 1, "G4C",,.F. )				//Itens Financeiros (Pastas Abaixo)
	Local oStruG4CAF:= FwFormStruct( 1, "G4C",,.F. )				//Itens Financeiros Cliente Finalizados antes da concilia��o
	Local oStruG9KA	:= FwFormStruct( 1, "G9K",,.F. )				//Demonstrativo Financeiro - Cliente
	Local oStruG9KB	:= FwFormStruct( 1, "G9K",,.F. )				//Demonstrativo Financeiro - Fornecedor
	Local oStruG4D	:= FwFormStruct( 1, "G4D",,.F. ) 				//Dados cart�o
	Local oStruG3S	:= FwFormStruct( 1, "G3S",,.F. ) 				//Passageiros
	Local oStruG4B	:= FwFormStruct( 1, "G4B",,.F. ) 				//Informa��es Adicionais
	Local oStruG3T	:= FwFormStruct( 1, "G3T",,.F. ) 				//1 - A�reo(G3T)
	Local oStruG3U	:= FwFormStruct( 1, "G3U",,.F. ) 				//2 - Hotel(G3U)
	Local oStruG3V	:= FwFormStruct( 1, "G3V",,.F. ) 				//3 - Carro (G3V)
	Local oStruG3W	:= FwFormStruct( 1, "G3W",,.F. ) 				//4 - Rodovi�rio (G3W)
	Local oStruG3Y	:= FwFormStruct( 1, "G3Y",,.F. ) 				//5 - Cruzeiro (G3Y)
	Local oStruG3X	:= FwFormStruct( 1, "G3X",,.F. ) 				//6 - Trem (G3X)
	Local oStruG42	:= FwFormStruct( 1, "G42",,.F. ) 				//7 - Visto (G42)
	Local oStruG41	:= FwFormStruct( 1, "G41",,.F. ) 				//8 - Seguro (G41)
	Local oStruG40	:= FwFormStruct( 1, "G40",,.F. ) 				//9 - Tour (G40)
	Local oStruG3Z	:= FwFormStruct( 1, "G3Z",,.F. ) 				//10 - Pacote (G3Z)
	Local oStruG43	:= FwFormStruct( 1, "G43",,.F. ) 				//11 - Outros (G43)
	Local oStruG44	:= FwFormStruct( 1, "G44",,.F. ) 				//Tarifas
	Local oStruG46	:= FwFormStruct( 1, "G46",,.F. ) 				//Taxas
	Local oStruG47	:= FwFormStruct( 1, "G47",,.F. ) 				//Extras
	Local oStruG48B	:= FwFormStruct( 1, "G48",,.F. ) 				//Acordos Fornecedores
	Local oStruG49	:= FwFormStruct( 1, "G49",,.F. ) 				//Impostos
	Local oStruG4CB	:= FwFormStruct( 1, "G4C",,.F. ) 				//Itens Financeiros - Fornecedor
	Local oStruG4A	:= FwFormStruct( 1, "G4A",,.F. ) 				//Rateio
	Local oStruG4E	:= FwFormStruct( 1, "G4E",,.F. ) 				//Reembolso
	Local oStruSRC	:= FWFormModelStruct():New()					// Pesquisa
	Local oStrEntid := FWFormModelStruct():New()
	Local oStrTOTDR := FWFormModelStruct():New() 
	Local aStruSRC  := {{}, 0}
	Local bLoad 	:= {|oFieldModel, lCopy| Tur34Ent(oFieldModel, lCopy)}
	Local bLoadTDR	:= {|oGridModel, lCopy| Tu42CDR(oGridModel, lCopy)}

	oStruG8C:AddField("Moeda"			      ,;	// 	[01]  C   Titulo do campo
				 	   "Moeda da Concilia��o" ,;	// 	[02]  C   ToolTip do campo
				 	   "G8C_MOEDA"	          ,;	// 	[03]  C   Id do Field
				 	   "C"					  ,;	// 	[04]  C   Tipo do campo
				 	   TamSx3("G3R_MOEDA")[1] ,;	// 	[05]  N   Tamanho do campo
				 	   0					  ,;	// 	[06]  N   Decimal do campo
				 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
				 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
				 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
				 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
				 	   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
				 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
				 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
				 	   .T.)							// 	[14]  L   Indica se o campo � virtual


	If !IsInCallStack("TuraEfetiv")
		aAdd(aStruSRC[1], Space(TamSX3("G3R_NUMID" )[1]))
		aAdd(aStruSRC[1], Space(TamSX3("G3Q_ORDER" )[1]))
		aAdd(aStruSRC[1], Space(TamSX3("G3Q_ITOS"  )[1]))
		aAdd(aStruSRC[1], Space(TamSX3("G3R_LOCGDS")[1]))
		aAdd(aStruSRC[1], Space(TamSX3("G3R_IDCONF")[1]))

		// estrututa da pesquisa
		TA042AddFlds(oStruSRC, 1)
	Endif 

	If FwIsInCallStack( "TURA042A" )
		TA042VtFld( oStruG44, oStruG46, oStruG47, oStruG49, oStruG3R )
		TA042Trig( oStruG44, oStruG46, oStruG47, oStruG49, oStruG3R, oStruG48B  )
		TA042VtPar( { oStruG3R, oStruG3Q, oStruG48A, oStruG4CA, oStruG9KA, oStruG9KB, oStruG4D, oStruG3S, oStruG4B, oStruG3T, oStruG3U, oStruG3V, oStruG3W, oStruG3Y,;
 					  oStruG3X, oStruG42, oStruG41, oStruG40, oStruG3Z, oStruG43, oStruG44, oStruG46, oStruG47, oStruG48B, oStruG49, oStruG4CB, oStruG4A, oStruG4E } )
	ElseIf FwIsInCallStack( "TURA042R" )
		TA042Trig( /*oStruG44*/, /*oStruG46*/, /*oStruG47*/, /*oStruG49*/, /*oStruG3R*/, oStruG48B, oStruG4E  )

	EndIf

	//Altera a estrutura obtida atrav�s do FwFormStruct
	TA034StruM(	oStruG3P,oStruG3Q,oStruG3R,oStruG3S,oStruG3T,oStruG3U,oStruG3V,oStruG3W,oStruG3X,oStruG3Y,/*oStrG3Z*/,oStruG40,oStruG41,oStruG42,oStruG43,;
				oStruG44,/*oStruG45*/,oStruG46,oStruG47,oStruG4A,oStruG4B,oStruG48A,oStruG48B,oStruG49,oStruG4E,oStruG4CA,oStruG4CB, oStrEntid, oStruG4D, oStruG9KA, oStruG9KB )

	// cria a estrutura para totalizadores da DR
	TU42MTRB( oStrTOTDR, 'M' )
	
	oStruG3P:SetProperty("*"		  ,	MODEL_FIELD_OBRIGAT, .F.)
	oStruG44:SetProperty("G44_MOEDA"  ,	MODEL_FIELD_OBRIGAT, .F.)
	oStruG46:SetProperty("G46_MOEDA"  ,	MODEL_FIELD_OBRIGAT, .T.)
	oStruG46:SetProperty("G46_TXCAMB" ,	MODEL_FIELD_OBRIGAT, .T.)
	oStruG47:SetProperty("G47_MOEDA"  ,	MODEL_FIELD_OBRIGAT, .T.)
	oStruG47:SetProperty("G47_TXCAMB" ,	MODEL_FIELD_OBRIGAT, .T.)
	oStruG3Q:SetProperty("G3Q_PRCBAS" ,	MODEL_FIELD_OBRIGAT, .F.) 			//Retirada a obrigatoriedade para o calculo do No Show
	oStruG3Q:SetProperty("G3Q_PRECAG" ,	MODEL_FIELD_OBRIGAT, .F.) 			//Retirada a obrigatoriedade para o calculo do No Show
	oStruG48A:SetProperty("G48_RECCON", MODEL_FIELD_OBRIGAT, .F.)
	oStruG48B:SetProperty("G48_RECCON", MODEL_FIELD_OBRIGAT, .F.)

	oStruG3R:SetProperty('G3R_TARIFA'	,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3R:SetProperty('G3R_TAXA'		,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3R:SetProperty('G3R_EXTRAS'	,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3R:SetProperty('G3R_VLRIMP'	,	MODEL_FIELD_VALID 	, {|| .T. } )

	oStruG3Q:SetProperty('G3Q_TARIFA'	,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3Q:SetProperty('G3Q_TAXA'		,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3Q:SetProperty('G3Q_EXTRA'	,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3Q:SetProperty('G3Q_VLRIMP'	,	MODEL_FIELD_VALID 	, {|| .T. } )
	oStruG3Q:SetProperty('G3Q_PRECAG'	,	MODEL_FIELD_VALID 	, {|| .T. } )

	oStruG44:SetProperty('G44_VLRDIA'	,	MODEL_FIELD_VALID 	, {|| .T. } )

	oModel := MPFormModel():New( "TURA042", /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )

	oModel:AddFields( "G8C_MASTER", /*cOwner*/, oStruG8C )

	// Pesquisa
	If !IsInCallStack("TuraEfetiv")
		oModel:AddFields("TMP_SEARCH", "G8C_MASTER", oStruSRC, /*bPre*/, /*bPos*/, {|| aStruSRC}/*bLoad*/ )
		oModel:GetModel ("TMP_SEARCH"):SetDescription(STR0057) 	// "Pesquisa" 
		oModel:GetModel ("TMP_SEARCH"):SetOnlyQuery(.T.)
		oModel:GetModel ("TMP_SEARCH"):SetOptional(.T.)
	EndIf
	
	//Documentos de reserva
	oModel:AddGrid( "G3R_ITENS", "G8C_MASTER", oStruG3R )
	oModel:SetRelation( "G3R_ITENS", { { "G3R_CONCIL", "G8C_CONCIL" }, { "G3R_FILCON", "xFilial('G8C')" } }, G3R->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3R_ITENS" ):SetLoadFilter( { { "G3R_TPSEG", "'1'", MVC_LOADFILTER_NOT_EQUAL } } )
	oModel:GetModel( "G3R_ITENS" ):SetDescription( STR0005 ) //"Documentos de Reserva"
	oModel:GetModel( "G3R_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3R_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Registro de Venda
	oModel:AddFields( "G3P_FIELDS", "G3R_ITENS", oStruG3P )
	oModel:SetRelation( "G3P_FIELDS", { { "G3P_FILIAL", "G3R_FILIAL" }, { "G3P_NUMID", "G3R_NUMID" } }, G3P->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3P_FIELDS" ):SetOnlyQuery(.T.)
	oModel:GetModel( "G3P_FIELDS" ):SetOptional( .T. )
	
	//Item de Venda
	oModel:AddGrid( "G3Q_ITENS", "G3R_ITENS", oStruG3Q )
	oModel:SetRelation( "G3Q_ITENS", { { "G3Q_FILIAL", "G3R_FILIAL" }, { "G3Q_NUMID", "G3R_NUMID" }, { "G3Q_IDITEM", "G3R_IDITEM" }, { "G3Q_NUMSEQ", "G3R_NUMSEQ" }, { "G3Q_CONORI", "G8C_CONCIL" } }, G3Q->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Q_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3Q_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Rateio
	oModel:AddGrid("G4A_ITENS","G3Q_ITENS",oStruG4A,,,/*bPreVal*/,/*bPosVal*/,)
	oModel:SetRelation("G4A_ITENS",{{"G4A_FILIAL","G3Q_FILIAL"},{"G4A_NUMID" ,"G3Q_NUMID"},{"G4A_IDITEM","G3Q_IDITEM"},{"G4A_NUMSEQ","G3Q_NUMSEQ"},{"G4A_CLIENT","G3Q_CLIENT"}, { "G4A_CONORI", "G8C_CONCIL" }},G4A->(IndexKey(1)))
	oModel:GetModel("G4A_ITENS"):SetOptional(.T.)
	oModel:GetModel( "G4A_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Cliente (Pastas Abaixo)
	oModel:AddGrid( "G48A_ITENS", "G3Q_ITENS", oStruG48A )
	oModel:SetRelation( "G48A_ITENS", { { "G48_FILIAL", "G3Q_FILIAL" }, { "G48_NUMID", "G3Q_NUMID" }, { "G48_IDITEM", "G3Q_IDITEM" }, { "G48_NUMSEQ", "G3Q_NUMSEQ" }, { "G48_CLIFOR", "'1'" }, { "G48_CONORI", "G8C_CONCIL" }, { 'G48_CODAPU', "''" }  }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48A_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G48A_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Cliente Finalizados antes da concilia��o
	oModel:AddGrid( "G48AF_ITENS", "G3Q_ITENS", oStruG48AF )
	oModel:SetRelation( "G48AF_ITENS", { { "G48_FILIAL", "G3Q_FILIAL" }, { "G48_NUMID", "G3Q_NUMID" }, { "G48_IDITEM", "G3Q_IDITEM" }, { "G48_NUMSEQ", "G3Q_NUMSEQ" }, { "G48_CLIFOR", "'1'" }, { "G48_CONORI", "''" }, { 'G48_CONINU', "''" }  }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48AF_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G48AF_ITENS" ):SetOnlyQuery(.T.)
	
	//Itens Financeiros (Pastas Abaixo)
	oModel:AddGrid( "G4CA_ITENS", "G3Q_ITENS", oStruG4CA )
	oModel:SetRelation( "G4CA_ITENS", { { "G4C_FILIAL", "G3Q_FILIAL" }, { "G4C_NUMID", "G3Q_NUMID" }, { "G4C_IDITEM", "G3Q_IDITEM" }, { "G4C_NUMSEQ", "G3Q_NUMSEQ" }, { "G4C_CLIFOR", "'1'" }, { "G4C_CONORI", "G8C_CONCIL" }, { 'G4C_CODAPU', "''" }  }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CA_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4CA_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Itens Financeiros Cliente Finalizados antes da concilia��o
	oModel:AddGrid( "G4CAF_ITENS", "G3Q_ITENS", oStruG4CAF )
	oModel:SetRelation( "G4CAF_ITENS", { { "G4C_FILIAL", "G3Q_FILIAL" }, { "G4C_NUMID", "G3Q_NUMID" }, { "G4C_IDITEM", "G3Q_IDITEM" }, { "G4C_NUMSEQ", "G3Q_NUMSEQ" }, { "G4C_CLIFOR", "'1'" }, { "G4C_CONORI", "''" }, { 'G4C_CONINU', "''" }  }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CAF_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4CAF_ITENS" ):SetOnlyQuery(.T.)

	//Reembolso
	oModel:AddGrid( 'G4E_ITENS', 'G3R_ITENS', oStruG4E )
	oModel:SetRelation( 'G4E_ITENS', { { 'G4E_FILIAL', 'G3R_FILIAL' }, { 'G4E_NUMID', 'G3R_NUMID' }, { 'G4E_IDITEM', 'G3R_IDITEM' }, { 'G4E_NUMSEQ', 'G3R_NUMSEQ' }, { 'G4E_CONORI', 'G8C_CONCIL' } }, G4E->( IndexKey( 1 ) ) )
	oModel:GetModel( 'G4E_ITENS' ):SetOptional( .T. )
	oModel:GetModel( "G4E_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Passageiros
	//Passageiros da reserva
	oModel:AddGrid( "G3S_ITENS", "G3R_ITENS", oStruG3S )
	oModel:SetRelation( "G3S_ITENS", { { "G3S_FILIAL", "G3R_FILIAL" }, { "G3S_NUMID", "G3R_NUMID" }, { "G3S_IDITEM", "G3R_IDITEM" }, { "G3S_NUMSEQ", "G3R_NUMSEQ" }, { "G3S_CONORI", "G8C_CONCIL" } }, G3S->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3S_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3S_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Dados Adicionais
	oModel:AddGrid("G4B_ITENS","G3S_ITENS",oStruG4B )
	oModel:SetRelation("G4B_ITENS",{{"G4B_FILIAL","G3S_FILIAL"},{"G4B_NUMID" ,"G3S_NUMID"},{"G4B_IDITEM","G3S_IDITEM"},{"G4B_NUMSEQ","G3S_NUMSEQ"},{"G4B_CLIENT","G3Q_CLIENT"},{"G4B_CODPAX","G3S_CODPAX"},{"G4B_CONORI","G8C_CONCIL"}},G4B->(IndexKey(1)))
	oModel:GetModel( "G4B_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4B_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//1 - A�reo(G3T)
	oModel:AddGrid( "G3T_ITENS", "G3S_ITENS", oStruG3T )
	oModel:SetRelation( "G3T_ITENS", { { "G3T_FILIAL", "G3R_FILIAL" }, { "G3T_NUMID", "G3S_NUMID" }, { "G3T_IDITEM", "G3S_IDITEM" }, { "G3T_NUMSEQ", "G3S_NUMSEQ" }, { "G3T_CODPAX", "G3S_CODPAX" }, { "G3T_CONORI", "G8C_CONCIL" } }, G3T->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3T_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3T_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//2 - Hotel(G3U)
	oModel:AddGrid( "G3U_ITENS", "G3Q_ITENS", oStruG3U )
	oModel:SetRelation( "G3U_ITENS", { { "G3U_FILIAL", "G3Q_FILIAL" }, { "G3U_NUMID", "G3Q_NUMID" }, { "G3U_IDITEM", "G3Q_IDITEM" }, { "G3U_NUMSEQ", "G3Q_NUMSEQ" }, { "G3U_CONORI", "G8C_CONCIL" } }, G3U->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3U_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3U_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//3 - Carro (G3V)
	oModel:AddGrid( "G3V_ITENS", "G3Q_ITENS", oStruG3V )
	oModel:SetRelation( "G3V_ITENS", { { "G3V_FILIAL", "G3Q_FILIAL" }, { "G3V_NUMID", "G3Q_NUMID" }, { "G3V_IDITEM", "G3Q_IDITEM" }, { "G3V_NUMSEQ", "G3Q_NUMSEQ" }, { "G3V_CONORI", "G8C_CONCIL" } }, G3V->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3V_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3V_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//4 - Rodovi�rio (G3W)
	oModel:AddGrid( "G3W_ITENS", "G3Q_ITENS", oStruG3W )
	oModel:SetRelation( "G3W_ITENS", { { "G3W_FILIAL", "G3Q_FILIAL" }, { "G3W_NUMID", "G3Q_NUMID" }, { "G3W_IDITEM", "G3Q_IDITEM" }, { "G3W_NUMSEQ", "G3Q_NUMSEQ" }, { "G3W_CONORI", "G8C_CONCIL" } }, G3W->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3W_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3W_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//5 - Cruzeiro (G3Y)
	oModel:AddGrid( "G3Y_ITENS", "G3Q_ITENS", oStruG3Y  )
	oModel:SetRelation( "G3Y_ITENS", { { "G3Y_FILIAL", "G3Q_FILIAL" }, { "G3Y_NUMID", "G3Q_NUMID" }, { "G3Y_IDITEM", "G3Q_IDITEM" }, { "G3Y_NUMSEQ", "G3Q_NUMSEQ" }, { "G3Y_CONORI", "G8C_CONCIL" } }, G3Y->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Y_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3Y_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//6 - Trem (G3X)
	oModel:AddGrid( "G3X_ITENS", "G3Q_ITENS", oStruG3X )
	oModel:SetRelation( "G3X_ITENS", { { "G3X_FILIAL", "G3Q_FILIAL" }, { "G3X_NUMID", "G3Q_NUMID" }, { "G3X_IDITEM", "G3Q_IDITEM" }, { "G3X_NUMSEQ", "G3Q_NUMSEQ" }, { "G3X_CONORI", "G8C_CONCIL" } }, G3X->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3X_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3X_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//7 - Visto (G42)
	oModel:AddGrid( "G42_ITENS", "G3Q_ITENS", oStruG42 )
	oModel:SetRelation( "G42_ITENS", { { "G42_FILIAL", "G3Q_FILIAL" }, { "G42_NUMID", "G3Q_NUMID" }, { "G42_IDITEM", "G3Q_IDITEM" }, { "G42_NUMSEQ", "G3Q_NUMSEQ" }, { "G42_CONORI", "G8C_CONCIL" } }, G42->( IndexKey( 1 ) ) )
	oModel:GetModel( "G42_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G42_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//8 - Seguro (G41)
	oModel:AddGrid( "G41_ITENS", "G3Q_ITENS", oStruG41 )
	oModel:SetRelation( "G41_ITENS", { { "G41_FILIAL", "G3Q_FILIAL" }, { "G41_NUMID", "G3Q_NUMID" }, { "G41_IDITEM", "G3Q_IDITEM" }, { "G41_NUMSEQ", "G3Q_NUMSEQ" }, { "G41_CONORI", "G8C_CONCIL" } }, G41->( IndexKey( 1 ) ) )
	oModel:GetModel( "G41_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G41_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//9 - Tour (G40)
	oModel:AddGrid( "G40_ITENS", "G3Q_ITENS", oStruG40 )
	oModel:SetRelation( "G40_ITENS", { { "G40_FILIAL", "G3Q_FILIAL" }, { "G40_NUMID", "G3Q_NUMID" }, { "G40_IDITEM", "G3Q_IDITEM" }, { "G40_NUMSEQ", "G3Q_NUMSEQ" }, { "G40_CONORI", "G8C_CONCIL" } }, G40->( IndexKey( 1 ) ) )
	oModel:GetModel( "G40_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G40_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//10 - Pacote (G3Z)
	oModel:AddGrid( "G3Z_ITENS", "G3Q_ITENS", oStruG3Z )
	oModel:SetRelation( "G3Z_ITENS", { { "G3Z_FILIAL", "G3Q_FILIAL" }, { "G3Z_NUMID", "G3Q_NUMID" }, { "G3Z_IDITEM", "G3Q_IDITEM" }, { "G3Z_NUMSEQ", "G3Q_NUMSEQ" }, { "G3Z_CONORI", "G8C_CONCIL" } }, G3Z->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Z_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3Z_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//11 - Outros (G43)
	oModel:AddGrid( "G43_ITENS", "G3Q_ITENS", oStruG43 )
	oModel:SetRelation( "G43_ITENS", { { "G43_FILIAL", "G3Q_FILIAL" }, { "G43_NUMID", "G3Q_NUMID" }, { "G43_IDITEM", "G3Q_IDITEM" }, { "G43_NUMSEQ", "G3Q_NUMSEQ" }, { "G43_CONORI", "G8C_CONCIL" } }, G43->( IndexKey( 1 ) ) )
	oModel:GetModel( "G43_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G43_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Tarifas
	oModel:AddGrid( "G44_ITENS", "G3R_ITENS", oStruG44 )
	oModel:SetRelation( "G44_ITENS", { { "G44_FILIAL", "G3R_FILIAL" }, { "G44_NUMID", "G3R_NUMID" }, { "G44_IDITEM", "G3R_IDITEM" }, { "G44_NUMSEQ", "G3R_NUMSEQ" }, { "G44_CONORI", "G8C_CONCIL" } }, G44->( IndexKey( 1 ) ) )
	oModel:GetModel( "G44_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G44_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Taxas
	oModel:AddGrid( "G46_ITENS", "G3R_ITENS", oStruG46 )
	oModel:SetRelation( "G46_ITENS", { { "G46_FILIAL", "G3R_FILIAL" }, { "G46_NUMID", "G3R_NUMID" }, { "G46_IDITEM", "G3R_IDITEM" }, { "G46_NUMSEQ", "G3R_NUMSEQ" }, { "G46_CONORI", "G8C_CONCIL" } }, G46->( IndexKey( 1 ) ) )
	oModel:GetModel( "G46_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G46_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Extras
	oModel:AddGrid( "G47_ITENS", "G3R_ITENS", oStruG47 )
	oModel:SetRelation( "G47_ITENS", { { "G47_FILIAL", "G3R_FILIAL" }, { "G47_NUMID", "G3R_NUMID" }, { "G47_IDITEM", "G3R_IDITEM" }, { "G47_NUMSEQ", "G3R_NUMSEQ" }, { "G47_CONORI", "G8C_CONCIL" } }, G47->( IndexKey( 1 ) ) )
	oModel:GetModel( "G47_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G47_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Fornecedor
	oModel:AddGrid( "G48B_ITENS", "G3R_ITENS", oStruG48B )
	oModel:SetRelation( "G48B_ITENS", { { "G48_FILIAL", "G3R_FILIAL" }, { "G48_NUMID", "G3R_NUMID" }, { "G48_IDITEM", "G3R_IDITEM" }, { "G48_NUMSEQ", "G3R_NUMSEQ" }, { "G48_CLIFOR", "'2'" }, { "G48_CONORI", "G8C_CONCIL" }, { "G48_CODAPU", "''" }  }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48B_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G48B_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Fornecedor Finalizados antes da concilia��o
	oModel:AddGrid( "G48BF_ITENS", "G3R_ITENS", oStruG48AF )
	oModel:SetRelation( "G48BF_ITENS", { { "G48_FILIAL", "G3R_FILIAL" }, { "G48_NUMID", "G3R_NUMID" }, { "G48_IDITEM", "G3R_IDITEM" }, { "G48_NUMSEQ", "G3R_NUMSEQ" }, { "G48_CLIFOR", "'2'" }, { "G48_CONORI", "''" }, { 'G48_CONINU', "''" }  }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48BF_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G48BF_ITENS" ):SetOnlyQuery(.T.)

	//Impostos
	oModel:AddGrid( "G49_ITENS", "G3R_ITENS", oStruG49 )
	oModel:SetRelation( "G49_ITENS", { { "G49_FILIAL", "G3R_FILIAL" }, { "G49_NUMID", "G3R_NUMID" }, { "G49_IDITEM", "G3R_IDITEM" }, { "G49_NUMSEQ", "G3R_NUMSEQ" }, { "G49_CONORI", "G8C_CONCIL" } }, G49->( IndexKey( 1 ) ) )
	oModel:GetModel( "G49_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G49_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Itens Financeiros - Fornecedor
	oModel:AddGrid( "G4CB_ITENS", "G3R_ITENS", oStruG4CB )
	oModel:SetRelation( "G4CB_ITENS", { { "G4C_FILIAL", "G3R_FILIAL" }, { "G4C_NUMID", "G3R_NUMID" }, { "G4C_IDITEM", "G3R_IDITEM" }, { "G4C_NUMSEQ", "G3R_NUMSEQ" }, { "G4C_CLIFOR", "'2'" }, { "G4C_CONORI", "G8C_CONCIL"}, { "G4C_CODAPU", "''" } }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CB_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4CB_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Itens Financeiros Fornecedor Finalizados antes da concilia��o
	oModel:AddGrid( "G4CBF_ITENS", "G3R_ITENS", oStruG4CAF )
	oModel:SetRelation( "G4CBF_ITENS", { { "G4C_FILIAL", "G3R_FILIAL" }, { "G4C_NUMID", "G3R_NUMID" }, { "G4C_IDITEM", "G3R_IDITEM" }, { "G4C_NUMSEQ", "G3R_NUMSEQ" }, { "G4C_CLIFOR", "'2'" }, { "G4C_CONORI", "''" }, { 'G4C_CONINU', "''" }  }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CBF_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4CBF_ITENS" ):SetOnlyQuery(.T.)

	oModel:AddGrid("G4D_ITENS","G3R_ITENS",oStruG4D,,,/*bPreVal*/,/*bPosVal*/,)
	oModel:SetRelation("G4D_ITENS",{{"G4D_FILIAL","G3R_FILIAL"},{"G4D_NUMID" ,"G3R_NUMID"},{"G4D_IDITEM","G3R_IDITEM"},{"G4D_NUMSEQ","G3R_NUMSEQ"}, { "G4D_CONORI", "G8C_CONCIL" }},G4D->(IndexKey(1)))
	oModel:GetModel( "G4D_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G4D_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	oModel:AddGrid("G9KA_ITENS","G3Q_ITENS",oStruG9KA,,,/*bPreVal*/,/*bPosVal*/,)
	oModel:SetRelation("G9KA_ITENS",{{"G9K_FILIAL","G3Q_FILIAL"},{"G9K_NUMID" ,"G3Q_NUMID"},{"G9K_IDITEM","G3Q_IDITEM"},{"G9K_NUMSEQ","G3Q_NUMSEQ"},{"G9K_CLIFOR","'1'"},{ "G9K_CONORI", "G8C_CONCIL" }},G9K->(IndexKey(1)))
	oModel:GetModel( "G9KA_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G9KA_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	oModel:AddGrid("G9KB_ITENS","G3R_ITENS",oStruG9KB,,,/*bPreVal*/,/*bPosVal*/,)
	oModel:SetRelation("G9KB_ITENS",{{"G9K_FILIAL","G3R_FILIAL"},{"G9K_NUMID" ,"G3R_NUMID"},{"G9K_IDITEM","G3R_IDITEM"},{"G9K_NUMSEQ","G3R_NUMSEQ"},{"G9K_CLIFOR","'2'"},{ "G9K_CONORI", "G8C_CONCIL" }},G9K->(IndexKey(1)))
	oModel:GetModel( "G9KB_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G9KB_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	oModel:AddFields('ENTIDADE',"G3Q_ITENS", oStrEntid,{|oMdl,cAcao,cCampo| Tur034EdtVld(oMdl,,cAcao,cCampo) }, /*bPosValidacao*/, bLoad /*bCarga*/ )
	oModel:GetModel('ENTIDADE'):SetDescription( STR0079 )	// "Entidade"
	oModel:SetRelation('ENTIDADE',{{"G3Q_FILIAL","G3Q_FILIAL"},{"G3Q_NUMID","G3Q_NUMID"},{"G3Q_NUMSEQ","G3Q_NUMSEQ"}},G3Q->(IndexKey(1)))
	oModel:GetModel('ENTIDADE'):SetOptional(.T.)
	oModel:GetModel('ENTIDADE'):SetOnlyQuery(.T.)

	// Adiciona a estrutura dos totalizadores de DR	
	oModel:addGrid('TOTDR', "G3R_ITENS", oStrTOTDR , , , , , bLoadTDR)
	oModel:SetRelation('TOTDR',{{"FILIAL","G3R_FILIAL"},{"NUMID","G3R_NUMID"},{"IDITEM","G3R_IDITEM"},{"NUMSEQ","G3R_NUMSEQ"}},G3R->(IndexKey(1)))
	oModel:GetModel('TOTDR'):SetDescription( STR0080 )		// "Totalizadores do Documento de Reserva"
	oModel:GetModel('TOTDR'):SetOptional(.T.)
	oModel:GetModel('TOTDR'):SetOnlyQuery(.T.)

	oModel:AddRules( "G8C_MASTER", "G8C_FATURA", "G8C_MASTER", "G8C_LOJA", 3 )

	oModel:SetOnDemand( .T. )
	oModel:SetDeActivate({|| TA042Destroy()})

	TURXNIL(aStruSRC)
Return oModel

Static Function ViewDef()
	Local oView		:= nil
	Local oModel   	:= FWLoadModel( "TURA042" )
	Local oStruG8C 	:= FWFormStruct( 2, "G8C" )
	Local oStruG3R 	:= FWFormStruct( 2, "G3R" )
	Local oStruG3P 	:= FWFormStruct( 2, "G3P" )
	Local oStruG3Q 	:= FWFormStruct( 2, "G3Q" )
	Local oStruG48A	:= FWFormStruct( 2, "G48" )
	Local oStruG4CA	:= FWFormStruct( 2, "G4C" )
	Local oStruG3S	:= FwFormStruct( 2, "G3S" ) //Passageiros
	Local oStruG46	:= FwFormStruct( 2, "G46" ) //Taxas
	Local oStruG48B	:= FwFormStruct( 2, "G48" ) //Acordos Fornecedores
	Local oStruG49	:= FwFormStruct( 2, "G49" ) //Impostos
	Local oStruG4CB := FWFormStruct( 2, "G4C" ) //Itens financeiros - Fornecedor
	Local oStruG4A	:= FwFormStruct( 2, "G4A" )
	Local oStruG9KA	:= FwFormStruct( 2, "G9K" )								//Demonstrativo Financeiro - Cliente
	Local oStruG9KB	:= FwFormStruct( 2, "G9K" )								//Demonstrativo Financeiro - Fornecedor
	Local oStruSRC 	:= FWFormViewStruct():New()
	Local oStrTOTDR := FWFormViewStruct():New()

	// estrutura da pesquisa
	If !IsInCallStack("TuraEfetiv") 
		TA042AddFlds(oStruSRC, 2)
	EndIf
	oView := FWFormView():New()

	//retira da view os campos que s�o apresentados por campos virtuais, para o caso de desmarcar o acordo
	oStruG3R:RemoveField("G3R_VLCOMI")
	oStruG3R:RemoveField("G3R_VLINCE")
	oStruG3R:RemoveField("G3R_TAXADU")
	oStruG3R:RemoveField("G3R_TXFORN")
	oStruG3R:RemoveField("G3R_TOTREC")
	oStruG3R:RemoveField("G3R_VLFIM")
	oStruG3R:RemoveField("G3R_DIVERG")

	//Os campos G3R_TAXADU, G3R_TXFORN, G3R_VLCOMI, G3R_VLINCE, da tabela G3R - Doc. de Reserva, n�o poder�o ser manipulados diretamente porque eles s�o preenchidos atrav�s dos acordos aplicados;
	//Os campos G3R_TARIFA, G3R_TAXA, G3R_EXTRAS, G3R_VLRIMP, da tabela G3R - Doc. de Reserva, tamb�m n�o poder�o ser manipulados diretamente porque eles s�o preenchidos atrav�s dos respectivos relacionamentos com as tabelas G44 �Tarifas, G46 � Taxas, G47 � Extras e G49 � Impostos;
	oStruG48B:SetProperty( "G48_RECCON"	, MVC_VIEW_ORDEM	  	, "00")
	oStruG3S:SetProperty( "*"			, MVC_VIEW_CANCHANGE	, .F.)

	oStruG3R:SetProperty( "G3R_TARIFA", MVC_VIEW_CANCHANGE, .F.)
	oStruG3R:SetProperty( "G3R_TAXA"  , MVC_VIEW_CANCHANGE, .F.)
	oStruG3R:SetProperty( "G3R_EXTRAS", MVC_VIEW_CANCHANGE, .F.)
	oStruG3R:SetProperty( "G3R_VLRIMP", MVC_VIEW_CANCHANGE, .F.)

	TA034StruV( oStruG3Q, oStruG4CA, /*oStruG4CB*/, /*oStruG3T*/, /*oStruG3U*/, /*oStruG3V*/, /*oStruG3W*/, /*oStruG3Y*/, /*oStruG3Z*/,/*oStruG40*/, /*oStruG41*/, /*oStruG42*/, /*oStruG43*/, oStruG48A, oStruG48B, /*oStruG4B*/, /*oStruG3X*/  )
	
	TU42MTRB( oStrTOTDR, 'V' )

	oView:SetModel( oModel )

	oView:AddField("VIEW_G8C"	, oStruG8C		, "G8C_MASTER" )
	IIF(!IsInCallStack("TuraEfetiv"), oView:AddField("VIEW_SRC"	, oStruSRC, "TMP_SEARCH"), )
	oView:AddGrid( "VIEW_G3R"	, oStruG3R		, "G3R_ITENS"  )
	oView:AddField( "VIEW_G3P"	, oStruG3P		, "G3P_FIELDS" )
	oView:AddGrid( "VIEW_G3Q"	, oStruG3Q		, "G3Q_ITENS"  )
	oView:AddGrid( "VIEW_G48A"	, oStruG48A		, "G48A_ITENS" )
	oView:AddGrid( "VIEW_G4CA"	, oStruG4CA		, "G4CA_ITENS" )
	oView:AddGrid( "VIEW_G3S"	, oStruG3S		, "G3S_ITENS"  )
	oView:AddGrid( "VIEW_G46"	, oStruG46		, "G46_ITENS"  )
	oView:AddGrid( "VIEW_G48B"	, oStruG48B 	, "G48B_ITENS" )
	oView:AddGrid( "VIEW_G49"	, oStruG49		, "G49_ITENS"  )
	oView:AddGrid( "VIEW_G4CB"	, oStruG4CB		, "G4CB_ITENS" )
	oView:AddGrid( "VIEW_G4A"	, oStruG4A		, "G4A_ITENS"  )
	oView:AddGrid( "VIEW_G9KA"	, oStruG9KA		, "G9KA_ITENS" )
	oView:AddGrid( "VIEW_G9KB"	, oStruG9KB		, "G9KB_ITENS" )	
	oView:AddGrid( "VIEW_TOTDR" , oStrTOTDR		, "TOTDR"      )

	oView:AddIncrementField("G46_ITENS","G46_SEQTAX")	//Taxas
	oView:AddIncrementField("G49_ITENS","G49_SEQ") 		//Impostos
	oView:AddIncrementField("G48B_ITENS","G48_APLICA")	//Acordos Fornecedor

	//T�tulos
	oView:EnableTitleView( "VIEW_G3R" )
	IIF(!IsInCallStack("TuraEfetiv"), oView:EnableTitleView( "VIEW_SRC", STR0057), )	//"Pesquisa"
	oView:EnableTitleView("G9KA_ITENS",STR0016) //"Resumo Cliente"
	oView:EnableTitleView("G9KB_ITENS",STR0017) //"Resumo Fornecedor"

	// Divis�o Horizontal
	oView:CreateHorizontalBox( "ALL",100 )
	oView:CreateFolder("FOLDER_ALL"	,"ALL")

	// Pastas
	oView:AddSheet("FOLDER_ALL","ABA_ALL_CABEC"		,	STR0019	) //"Concilia��o"
	oView:AddSheet("FOLDER_ALL","ABA_ALL_ITENS"		,	STR0006	) //"Itens Concilia��o"

	oView:CreateHorizontalBox( "BOX_G8C"		, 100, , , "FOLDER_ALL","ABA_ALL_CABEC"		)
	oView:CreateHorizontalBox( "BOX_G3R"		, 040, , , "FOLDER_ALL","ABA_ALL_ITENS"		)
 	oView:CreateHorizontalBox( "ITENS_DR"		, 045, , , "FOLDER_ALL","ABA_ALL_ITENS"		)
	oView:CreateHorizontalBox( "TOTAL_DR"		, 015, , , "FOLDER_ALL","ABA_ALL_ITENS"		)
	If !IsInCallStack("TuraEfetiv")
		oView:CreateVerticalBox('BOX_G3R_LEFT' , 25, "BOX_G3R", , "FOLDER_ALL", "ABA_ALL_ITENS")
		oView:CreateVerticalBox('BOX_G3R_RIGHT', 75, "BOX_G3R", , "FOLDER_ALL", "ABA_ALL_ITENS")
	EndIf 

	oView:CreateFolder("FOLDER_IT_DR","ITENS_DR")

	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G3P", STR0007	) //"Registro de Venda"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G3Q", STR0008		) //"Item de Venda"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G3S", STR0009	) //"Dados da reserva"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G46", STR0010		) //"Taxas"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G49", STR0011	) //"Impostos"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G48B", STR0012 ) //"Acordos Fornecedor"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G4CB", STR0013 ) //"Itens Financeiros"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G9K", STR0018) //"Demonstrativo" 
	
	oView:CreateHorizontalBox( "BOX_G3P"		, 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G3P") // Registro de venda
	oView:CreateHorizontalBox( "BOX_G3Q"		, 040, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G3Q") // Item de Venda
	oView:CreateHorizontalBox( "DADOSCLIENTE"	, 060, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G3Q" )

	oView:CreateFolder("FOLDER_DADOSCLI","DADOSCLIENTE")
	oView:AddSheet( "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G48A", STR0014	) //"Acordos de clientes"
	oView:AddSheet( "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G4CA", STR0013	) //"Itens Financeiros"
	oView:AddSheet( "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G4A" , STR0015				) //"Rateio"

	oView:CreateHorizontalBox( "BOX_G48A"		, 100, , , "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G48A")
	oView:CreateHorizontalBox( "BOX_G4CA"		, 100, , , "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G4CA")
	oView:CreateHorizontalBox( "BOX_G4A"		, 100, , , "FOLDER_DADOSCLI", "FOLDER_DADOSCLI_G4A" )

	oView:CreateHorizontalBox( "BOX_G3S" 		, 040, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G3S" ) // Passageiros
	oView:CreateHorizontalBox( "SEGMENTOS"		, 060, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G3S" ) // Segmentos

	oView:CreateFolder("FOLDER_SEG","SEGMENTOS")

	oView:CreateHorizontalBox( "BOX_G46"		, 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G46")
	oView:CreateHorizontalBox( "BOX_G49"		, 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G49")
	oView:CreateHorizontalBox( "BOX_G48B"		, 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G48B")
	oView:CreateHorizontalBox( "BOX_G4CB"		, 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G4CB")

	oView:CreateVerticalBox( "BOX_G9KA"	, 050, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G9K" ) 
	oView:CreateVerticalBox( "BOX_G9KB"	, 050, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G9K" )
	
	oView:SetViewAction('DELETELINE'  , {|oView, cIdView, nNumLine| TA042Dlin(oView, cIdView, nNumLine)})
	oView:SetViewAction('UNDELETELINE', {|oView, cIdView, nNumLine| TA042Dlin(oView, cIdView, nNumLine)})
	
	oView:SetOwnerView( "VIEW_G8C" 	, "BOX_G8C" )

	If !IsInCallStack("TuraEfetiv") 
		oView:SetOwnerView( "VIEW_SRC" 	, "BOX_G3R_LEFT" )
		oView:SetOwnerView( "VIEW_G3R" 	, "BOX_G3R_RIGHT" )
	Else
		oView:SetOwnerView( "VIEW_G3R" 	, "BOX_G3R" )
	EndIf

	oView:SetOwnerView( "VIEW_G3P" 	, "BOX_G3P" )
	oView:SetOwnerView( "VIEW_G3Q" 	, "BOX_G3Q")
	oView:SetOwnerView( "VIEW_G3S"	, "BOX_G3S" )
	oView:SetOwnerView( "VIEW_G46"	, "BOX_G46" )
	oView:SetOwnerView( "VIEW_G49"	, "BOX_G49" )
	oView:SetOwnerView( "VIEW_G48A"	, "BOX_G48A" )
	oView:SetOwnerView( "VIEW_G4CA"	, "BOX_G4CA" )
	oView:SetOwnerView( "VIEW_G4A"	, "BOX_G4A"  )
	oView:SetOwnerView( "VIEW_G48B"	, "BOX_G48B" )
	oView:SetOwnerView( "VIEW_G4CB"	, "BOX_G4CB" )
	oView:SetOwnerView( "VIEW_G9KA"	, "BOX_G9KA" )
	oView:SetOwnerView( "VIEW_G9KB"	, "BOX_G9KB" )
	oView:SetOwnerView( "VIEW_TOTDR", "TOTAL_DR" )
	
	//desabilitar views
	oView:SetOnlyView("VIEW_G3P")
	oView:SetOnlyView("VIEW_G48A")
	oView:SetOnlyView("VIEW_G4CA")
	oView:SetOnlyView("VIEW_G4CB")

	If !IsInCallStack("TuraEfetiv") .And. !IsInCallStack("TA042AEfet") 
		SetKey(VK_F5, {|| FwMsgRun( , {|| TA042SrcG3R()}, , STR0047)}) //"Pesquisando o Documento de Reserva..."
	EndIf
	oView:SetProgressBar(.T.)

Return oView

/*/{Protheus.doc} TA042Model
Fun��o para retorno do objeto oModel (vari�vel est�tica) do fonte atual
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042Model()
	Local oAuxMdl := nil

	If FwIsInCallStack( "TA042APesq" )
		oAuxMdl := TA042AModel()
	ElseIf FwIsInCallStack( "TA042RPesq" )
		oAuxMdl := TA042RModel()
	EndIF
Return oAuxMdl

/*/{Protheus.doc} TA042LoadV
Realiza o LoadValue no modelo de dados informado com as informa��es passadas via parametro
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA042LoadV(oMdlMaster,cIdModel,aDados,lLinha)
	Local lRet	 := .T.
	Local nX	 := 0
	Local nY	 := 0
	Local nPos	 := 0
	Local oModel := oMdlMaster:GetModel(cIdModel)
	Local oStru	 := oModel:GetStruct()

	Default lLinha	:= .F.

	If lLinha
		If oModel:CanInsertLine()
			If oModel:Length() == oModel:AddLine()
				Return .F.
			EndIf
		Else
			MsgInfo(STR0020 + oModel:GetId() ) //'Modelo n�o permite incluir Linhas: '
			Return .F.
		EndIf
	EndIf

	For nX := 1 To Len( aDados )
		// Verifica se os campos passados existem na estrutura do modelo
		If oStru:HasField( aDados[nX][1] )
			If !( lRet := oMdlMaster:LoadValue(cIdModel, aDados[nX][1], aDados[nX][2] ) )
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next

Return lRet

/*/{Protheus.doc} TA042SetVl
Realiza o SetValue no modelo de dados informado com as informa��es passadas via parametro
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA042SetVl(oMdlMaster,cIdModel,aDados,lLinha)
	Local lRet	 := .T.
	Local nX	 := 0
	Local nY	 := 0
	Local nPos	 := 0
	Local oModel := oMdlMaster:GetModel(cIdModel)
	Local oStru	 := oModel:GetStruct()

	Default lLinha	:= .F.

	If lLinha
		If oModel:Length() == oModel:AddLine()
			Return .F.
		EndIf
	EndIf

	For nX := 1 To Len( aDados )
		// Verifica se os campos passados existem na estrutura do modelo
		If oStru:HasField( aDados[nX][1] )
			If !( lRet := oMdlMaster:SetValue(cIdModel, aDados[nX][1], aDados[nX][2] ) )
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next

Return lRet

/*/{Protheus.doc} TA042AtuAc
Realiza o recalculo dos campos G3R_TARIFA,G3R_TAXA,G3R_VLRIMP,G3R_EXTRAS com base nos itens das tabelas
@type function
@author Anderson Toledo
@since 26/04/2016
@version 12.1.7
/*/
Function TA042AtuAc( oModel )
	Local aSaveLines	:= FwSaveRows( oModel )
	Local aValues		:= {}
	Local nVlrTarifa	:= 0
	Local nVlrTaxa		:= 0
	Local nVlrExtra		:= 0
	Local nVlrImp		:= 0
	Local nVlrTxRee		:= 0
	Local nX			:= 0
	Local lRet 			:= .T.
	Local oMdlG3R   	:= oModel:GetModel( 'G3R_ITENS' )
	Local oMdlG4E		:= oModel:GetModel( 'G4E_ITENS' )
	Local oMdlG44		:= oModel:GetModel( 'G44_ITENS' )
	Local oMdlG46		:= oModel:GetModel( 'G46_ITENS' )
	Local oMdlG47		:= oModel:GetModel( 'G47_ITENS' )
	Local oMdlG49		:= oModel:GetModel( 'G49_ITENS' )
	
	//Calculo para reembolso
	For nX := 1 To oMdlG4E:Length()
		oMdlG4E:GoLine( nX )
		
		If !oMdlG4E:IsDeleted() 
			nVlrTarifa	+= oMdlG4E:GetValue( 'G4E_TREEMB' )
			nVlrTaxa	+= oMdlG4E:GetValue( 'G4E_TXREEM' )
			nVlrExtra	+= oMdlG4E:GetValue( 'G4E_EXREEM' )			
		EndIf
	Next
	
	//Tarifas
	For nX := 1 To oMdlG44:Length()
		oMdlG44:GoLine( nX )
	
		If !oMdlG44:IsDeleted() 
			nVlrTarifa	+=  oMdlG44:GetValue( 'G44_APLICA' ) + oMdlG44:GetValue( 'G44_EARLY' ) + oMdlG44:GetValue( 'G44_LATE' );
								 + ( oMdlG44:GetValue( 'G44_PLALIM' ) * oMdlG3R:GetValue( ' G3R_DIARIA' ) )
		EndIf
	Next
	
	//Taxas
	For nX := 1 To oMdlG46:Length()
		oMdlG46:GoLine( nX )
	
		If !oMdlG46:IsDeleted() 
			If oMdlG3R:GetValue( 'G3R_OPERAC' ) == '2'
				nVlrTxRee += oMdlG46:GetValue( 'G46_VLTAXA' )
			Else
				nVlrTaxa += oMdlG46:GetValue( 'G46_VLTAXA' ) 
			EndIf
		EndIf
	Next

	//Extras
	For nX := 1 To oMdlG47:Length()
		oMdlG47:GoLine( nX )
	
		If !oMdlG47:IsDeleted() 
			nVlrExtra += oMdlG47:GetValue( 'G47_TOTAL' ) 
		EndIf
	Next

	//Impostos
	For nX := 1 To oMdlG49:Length()
		oMdlG49:GoLine( nX )
	
		If !oMdlG49:IsDeleted() 
			nVlrImp += oMdlG49:GetValue( 'G49_VALOR' ) 
		EndIf
	Next	
	
	aAdd(aValues, {'G3R_TARIFA'	, nVlrTarifa})
	aAdd(aValues, {'G3R_TAXA'	, nVlrTaxa	})
	aAdd(aValues, {'G3R_EXTRAS'	, nVlrExtra	})
	aAdd(aValues, {'G3R_VLRIMP'	, nVlrImp	})
	aAdd(aValues, {'G3R_TXREE'	, nVlrTxRee	})

	lRet := TA042SetVl(oModel,'G3R_ITENS',aValues )
	
	FwRestRows( aSaveLines )
	
	TURXNIL(aSaveLines)
	TURXNIL(aValues)
Return lRet

/*/{Protheus.doc} TA042ExiAd
Verifica se j� existe antecipa��o para a concilia��o
@type function
@author Anderson Toledo
@since 24/06/2016
@version 1.0
@return caractere, '1' - Antecipa��o j� cadastrada, '2' - N�o existe antecipa��o
/*/
Function TA042ExiAd()
	Local cRet 	 := '2'
	Local oModel := FwModelActive()
			
	cFatura	 := oModel:GetValue( 'G8C_MASTER', 'G8C_FATURA' )
	cFornece := oModel:GetValue( 'G8C_MASTER', 'G8C_FORNEC' )
	cLoja  	 := oModel:GetValue( 'G8C_MASTER', 'G8C_LOJA' )
	
	G8X->( dbSetOrder( 1 ) )
	If !Empty( cFatura ) .And. !Empty( cFornece ) .And. !Empty( cLoja ) 
		If G8X->( DbSeek( xFilial('G8X') + '2' + cFatura + cFornece + cLoja ) )
			cRet := '1'
		EndIf
	EndIf
	
Return cRet

/*/{Protheus.doc} TA042LPre
Fun��o gen�rica para bloquear a edi��o de modelos, na especializa��o do model, os modelos que permitem atualiza��o deve sobrescrever a LinePre
@type function
@author Anderson Toledo
@since 24/06/2016
@version 1.0
/*/
Function TA042LPre(oModelGrid, nLine, cAction, cField)
	Local lRet := .T.

	If cAction == 'CANSETVALUE'
		lRet := .F.
	EndIf

Return lRet

/*/{Protheus.doc} TA042VtPar
Fun��o para verificar quais estruturas podem ter parcialidade e adicionar os campos e gatilhos espec�ficos para a parcialide
@type function
@author Anderson Toledo
@since 18/07/2016
@version 1.0
@param aStructs, array, Vetor com as estruturas que ir�o compor o model
@return 
/*/
Function TA042VtPar( aStructs )
	Local nX 		  := 0
	Local cTable	  := ""
	Local cIdTable	  := ""
	Local cFldDtIni	  := ""
	Local cFldDtFim	  := ""
	Local cFldPeriodo := ""
	Local aAux        := {}
	
	For nX := 1 to len( aStructs )
		cTable := aStructs[nX]:GetTable()[1]	

		If TA042SegPr( cTable, '2' )
			TA042FdSeg( cTable, @cIdTable, @cFldDtIni, @cFldDtFim, @cFldPeriodo  )
			
			aStructs[nX]:AddField('Bkp ' + cTable + cFldDtIni  ,; 		// [01] C Titulo do campo
								  'Bkp ' + cTable + cFldDtIni  ,; 		// [02] C ToolTip do campo
								  'XXX' + cFldDtIni 		   ,; 		// [03] C identificador (ID) do Field
								  'D' 						   ,; 		// [04] C Tipo do campo
								  TamSX3(cTable + cFldDtIni)[1],; 		// [05] N Tamanho do campo
								  TamSX3(cTable + cFldDtIni)[2],; 		// [06] N Decimal do campo
								  NIL						   ,; 		// [07] B Code-block de valida��o do campo
								  NIL 						   ,; 		// [08] B Code-block de valida��o When do campo
								  NIL 						   ,; 		// [09] A Lista de valores permitido do campo
								  .F. 						   ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
								  NIL 						   ,; 		// [11] B Code-block de inicializacao do campo
								  NIL 						   ,; 		// [12] L Indica se trata de um campo chave
								  .F. 						   ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
								  .T.) 									// [14] L Indica se o campo � virtual

			aStructs[nX]:AddField('Bkp ' + cTable + cFldDtFim  ,; 		// [01] C Titulo do campo
								  'Bkp ' + cTable + cFldDtFim  ,; 		// [02] C ToolTip do campo
								  'XXX' + cFldDtFim 		   ,; 		// [03] C identificador (ID) do Field
								  'D' 						   ,; 		// [04] C Tipo do campo
								  TamSX3(cTable + cFldDtFim)[1],; 		// [05] N Tamanho do campo
								  TamSX3(cTable + cFldDtFim)[2],; 		// [06] N Decimal do campo
								  NIL						   ,; 		// [07] B Code-block de valida��o do campo
								  NIL 						   ,; 		// [08] B Code-block de valida��o When do campo
								  NIL 						   ,; 		// [09] A Lista de valores permitido do campo
								  .F. 						   ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
								  NIL 						   ,; 		// [11] B Code-block de inicializacao do campo
								  NIL 						   ,; 		// [12] L Indica se trata de um campo chave
								  .F. 						   ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
								  .T.) 									// [14] L Indica se o campo � virtual
	
			//Apenas adiciona o campo de backup de per�odo caso o segmento possua o campo
			If !Empty( cFldPeriodo )
				aStructs[nX]:AddField('Bkp ' + cTable + cFldPeriodo  ,; 		// [01] C Titulo do campo
									  'Bkp ' + cTable + cFldPeriodo	 ,;	 		// [02] C ToolTip do campo
									  'XXX' + cFldPeriodo 			 ,; 		// [03] C identificador (ID) do Field
									  'N' 							 ,; 		// [04] C Tipo do campo
									  TamSX3(cTable + cFldPeriodo)[1],; 		// [05] N Tamanho do campo
									  TamSX3(cTable + cFldPeriodo)[2],; 		// [06] N Decimal do campo
									  NIL							 ,; 		// [07] B Code-block de valida��o do campo
									  NIL 							 ,; 		// [08] B Code-block de valida��o When do campo
									  NIL 							 ,; 		// [09] A Lista de valores permitido do campo
									  .F. 							 ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
									  NIL 							 ,; 		// [11] B Code-block de inicializacao do campo
									  NIL 							 ,; 		// [12] L Indica se trata de um campo chave
									  .F. 							 ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
									  .T.) 										// [14] L Indica se o campo � virtual
				
			EndIf

			aAux := FwStruTrigger(cTable + cFldDtIni, "XXX" + cFldDtIni, "TA042TrSeg('" + cTable + "_ITENS', '" + cTable + cFldDtIni + "', 'XXX" + cFldDtIni + "')")
			aStructs[nX]:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )

			aAux := FwStruTrigger(cTable + cFldDtFim, "XXX" + cFldDtFim, "TA042TrSeg('" + cTable + "_ITENS', '" + cTable + cFldDtFim + "', 'XXX" + cFldDtFim + "')")
			aStructs[nX]:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
			
			//Apenas adiciona o gatilho de per�odo caso o segmento possua o campo
			If !Empty( cFldPeriodo )
				aAux := FwStruTrigger(cTable + cFldPeriodo, "XXX" + cFldPeriodo, "TA042TrSeg('" + cTable + "_ITENS', '" + cTable + cFldPeriodo + "', 'XXX" + cFldPeriodo + "')")
				aStructs[nX]:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
			EndIf

		EndIf
	Next
Return

/*/{Protheus.doc} TA042SegPr
Fun��o para retornar se determinado segmento possui parcialidade
@type function
@author Anderson
@since 18/07/2016
@version 1.0
@param cSegmento, character, c�digo ou tabela do segmento para pesquisa
@param cTipo, character, Tipo do segmento para pesquisa 1 - C�digo do segmento, 2 - Tabela do segmento
@return l�gico, informa se no segmento � permitido a parcialidade 
/*/
Function TA042SegPr( cSegmento, cTipo )
	Local lRet := .F.
		
	If cTipo == '1'
		lRet := AllTrim( Upper(cSegmento) ) $ '2|3|B'
	Else
		lRet := AllTrim( Upper(cSegmento) ) $ 'G3U|G3V|G43'
	EndIf
	 
Return lRet 

/*/{Protheus.doc} TA042FdSeg
Fun��o para retornar tabela e campos referentes a datas e per�odos
@type function
@author Anderson Toledo
@since 18/07/2016
@version 1.0
@param cSegmento, character, c�digo ou tabela do segmento
@param cIdTable, character, parametro via refer�ncia, ser� informado a tabela do segmento
@param cFldDtIni, character, parametro via refer�ncia, ser� informado o campo da data inicial
@param cFldDtFim, character, parametro via refer�ncia, ser� informado o campo da data final
@param cFldPeriodo, character, parametro via refer�ncia, ser� informado o campo do per�odo caso este exista
/*/
Function TA042FdSeg( cSegmento, cIdTable, cFldDtIni, cFldDtFim, cFldPeriodo  )
	
	DEFAULT cFldDtIni		:= ""
	DEFAULT cFldDtFim		:= ""
	DEFAULT cFldPeriodo	:= ""
	
	Do Case
		Case cSegmento == '2' .Or. cSegmento == 'G3U' //Hotel
			cIdTable	:= "G3U"
			cFldDtIni	:= "_DTINI"
			cFldDtFim	:= "_DTFIM"
			cFldPeriodo := "_QTDDIA"
		Case cSegmento == '3' .Or. cSegmento == 'G3V' //Carro
			cIdTable	:= "G3V"
			cFldDtIni	:= "_DTINI"
			cFldDtFim	:= "_DTFIM"
			cFldPeriodo := "_QTDDIA"
		Case Upper( cSegmento ) == 'B' .Or. cSegmento == 'G43' //Outros
			cIdTable	:= "G43"
			cFldDtIni	:= "_DTINI"
			cFldDtFim	:= "_DTFIM"
			cFldPeriodo := ""	
		OtherWise
			cIdModel	:= ""
			cFldDtIni	:= ""
			cFldDtFim	:= ""
	EndCase	
		
Return


/*/{Protheus.doc} TA042NoSeg
Fun��o utilizada na serializa��o do model na parcialidade para n�o copiar os demais segmentos 
@type function
@author Anderson Toledo
@since 18/07/2016
@version 1.0
@param cSeg, character, C�digo do segmento que ser� parcionalizado
/*/
Function TA042NoSeg( cSeg )
	Local aSegmentos := {}
	Local aAux 		 := {{'1', 'G3T_ITENS' },; //Aereo
	                     {'2', 'G3U_ITENS' },; //Hotel 
	                     {'3', 'G3V_ITENS' },; //Carro 
	                     {'4', 'G3W_ITENS' },; //Rodovi�rio 
	                     {'5', 'G3X_ITENS' },; //Cruzeiro 
	                     {'6', 'G3Y_ITENS' },; //Trem 
	                     {'7', 'G3Z_ITENS' },; //Visto 
	                     {'8', 'G40_ITENS' },; //Seguro 
	                     {'9', 'G41_ITENS' },; //Tour 
	                     {'A', 'G42_ITENS' },; //Pacote 
	                     {'B', 'G43_ITENS' } } //Outros	 

	Local nPos	:= 0

	nPos := aScan( aAux, { |x| x[1] == Upper( cSeg )  }  )  
	If nPos > 0
		ADel( aAux, nPos )
		ASize( aAux, len(aAux) - 1 )
		aEval( aAux, {|x| aAdd(aSegmentos, [2]) } )
	
		aAdd( aSegmentos, 'G4CA_ITENS' )
		aAdd( aSegmentos, 'G4CB_ITENS' )
		
	EndIf

	TURXNIL(aAux)
Return aSegmentos

/*/{Protheus.doc} TA42DelSons
Fun��o utilizada apagar todas as informa��es de um modelo e submodelo
@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/
Function TA42DelSons( oModel, cIdModel, lAll, lUnDelete )
	Local aIdGrids 	:= oModel:GetDependency( cIdModel )
	Local nX 		:= 0
	Local nY 		:= 0
	
	DEFAULT	lUnDelete := .F.

	If lUnDelete
		If !("G4C" $ cIdModel) .And. oModel:GetModel( cIdModel ):ClassName() == "FWFORMGRID"
			If lAll
				For nY := 1 to oModel:GetModel( cIdModel ):Length()
					oModel:GetModel( cIdModel ):GoLine( nY )
					If !oModel:GetModel( cIdModel ):isEmpty()
						oModel:GetModel( cIdModel ):UnDeleteLine( )
					EndIf
				Next
			Else
				If !oModel:GetModel( cIdModel ):isEmpty()
					oModel:GetModel( cIdModel ):UnDeleteLine( )
				Endif
			EndIf
		EndIf
	Else
		If oModel:GetModel( cIdModel ):ClassName() == "FWFORMGRID"
			If lAll
				If !oModel:GetModel( cIdModel ):isEmpty()
					oModel:GetModel( cIdModel ):DelAllLine( )
				EndIf
			Else
				If !oModel:GetModel( cIdModel ):isEmpty()
					oModel:GetModel( cIdModel ):DeleteLine( )
				EndIf
			EndIf
		EndIf
	EndIf

	For nX := 1 to len( aIdGrids )
		TA42DelSons( oModel, aIdGrids[nX][2], .T., lUnDelete )
	Next
Return

/*/{Protheus.doc} TA42IfCliF
Fun��o utilizada para verificar se existem itens financeiros de clientes liberados pela concilia��o finalizados
@type function
@author Anderson Toledo
@since 18/10/2016
@version 1.0
/*/

Function TA42IfCliF( cFilCon, cConcil )
	Local cAlias 	:= GetNextAlias()
	Local lRet 	:= .T.

	BeginSql Alias cAlias
		SELECT COUNT(*) FINALIZADO
			FROM %Table:G3R% G3R
			INNER JOIN %Table:G4C% G4C
				ON G4C_FILIAL = G3R_FILIAL
					AND G4C_NUMID = G3R_NUMID
					AND G4C_IDITEM = G3R_IDITEM
					AND G4C_NUMSEQ = G3R_NUMSEQ
					AND G4C_CLIFOR = '1'
					AND G4C_TPCONC = '2'
					AND G4C_STATUS = '4'
					AND G4C.%notDel%
			WHERE G3R_CONCIL = %Exp:cConcil%
				AND G3R_FILCON = %Exp:cFilCon%
				AND G3R.%notDel%
	EndSql

	lRet := (cAlias)->FINALIZADO > 0

	(cAlias)->( dbCloseArea() )

Return lRet


/*/{Protheus.doc} TA42MTRB
Fun��o utilizada para criar campos para estrutura nova para totalizadores da DR
@type function
@author Fernando Amorim(Cafu)
@since 03/05/2017
/*/
Static Function TU42MTRB( oStrTOTDR,cTipStr )

Local aCombo := TURGetComb('G3R_STATUS',1)

If cTipStr == 'M'
	If ValType( oStrTOTDR ) == "O"
		oStrTOTDR:AddTable("   ",{" "}," ")		
		oStrTOTDR:AddField(STR0024			      ,;	// 	[01]  C   Titulo do campo
					 	   STR0024			      ,;	// 	[02]  C   ToolTip do campo
					 	   "FILIAL"		          ,;	// 	[03]  C   Id do Field
					 	   "C"					  ,;	// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_FILIAL")[1],;	// 	[05]  N   Tamanho do campo
					 	   0					  ,;	// 	[06]  N   Decimal do campo
					 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
					 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   Nil					  ,;	//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual

		oStrTOTDR:AddField(STR0025	             ,;		// 	[01]  C   Titulo do campo
					 	   STR0025			     ,;		// 	[02]  C   ToolTip do campo
					 	   "NUMID"		         ,;		// 	[03]  C   Id do Field
					 	   "C"					 ,;		// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_NUMID")[1],;		// 	[05]  N   Tamanho do campo
					 	   0					 ,;		// 	[06]  N   Decimal do campo
					 	   NIL					 ,;		// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				     ,;		// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					 ,;		//	[09]  A   Lista de valores permitido do campo
					 	   .F.					 ,;		//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					 ,;		//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				     ,;		//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				     ,;		//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual
		 	
		oStrTOTDR:AddField(STR0026	              ,;	// 	[01]  C   Titulo do campo
					 	   STR0026				  ,;	// 	[02]  C   ToolTip do campo
					 	   "IDITEM"		          ,;	// 	[03]  C   Id do Field
					 	   "C"					  ,;	// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_IDITEM")[1],;	// 	[05]  N   Tamanho do campo
					  	   0					  ,;	// 	[06]  N   Decimal do campo
					 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
					 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual

		oStrTOTDR:AddField(STR0027	              ,;	// 	[01]  C   Titulo do campo
					 	   STR0027				  ,;	// 	[02]  C   ToolTip do campo
					 	   "NUMSEQ"		          ,;	// 	[03]  C   Id do Field
					  	   "C"					  ,;	// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_NUMSEQ")[1],;	// 	[05]  N   Tamanho do campo
					 	   0					  ,;	// 	[06]  N   Decimal do campo
					 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
					 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual
		
		oStrTOTDR:AddField(STR0028	             ,;		//  [01]  C   Titulo do campo
					 	   STR0028			     ,;		// 	[02]  C   ToolTip do campo
					 	   "DTINI"		         ,;		// 	[03]  C   Id do Field
					 	   "D"					 ,;		// 	[04]  C   Tipo do campo
					 	   TamSx3("G3U_DTINI")[1],;		// 	[05]  N   Tamanho do campo
					 	   0					 ,;		// 	[06]  N   Decimal do campo
					 	   NIL					 ,;		// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				     ,;		// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					 ,;		//	[09]  A   Lista de valores permitido do campo
					 	   .F.					 ,;		//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL				     ,;		//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				     ,;		//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				     ,;		//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual

		oStrTOTDR:AddField(STR0029 	             ,;		// 	[01]  C   Titulo do campo
					 	   STR0029			     ,;		// 	[02]  C   ToolTip do campo
					 	   "DTFIM"		         ,;		// 	[03]  C   Id do Field
					 	   "D"					 ,;		// 	[04]  C   Tipo do campo
					 	   TamSx3("G3U_DTFIM")[1],;		// 	[05]  N   Tamanho do campo
					 	   0					 ,;		// 	[06]  N   Decimal do campo
					 	   NIL					 ,;		// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				     ,;		// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					 ,;		//	[09]  A   Lista de valores permitido do campo
					 	   .F.					 ,;		//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					 ,;		//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				     ,;		//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				     ,;		//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual
			 	
		oStrTOTDR:AddField(STR0030	              ,;	// 	[01]  C   Titulo do campo
					 	   STR0030				  ,;	// 	[02]  C   ToolTip do campo
					 	   "TARIFAS"		      ,;	// 	[03]  C   Id do Field
					 	   "N"					  ,;	// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_TARIFA")[1],;	// 	[05]  N   Tamanho do campo
					 	   TamSx3("G3R_TARIFA")[2],;	// 	[06]  N   Decimal do campo
					 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
					 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual
			 
		oStrTOTDR:AddField(STR0031	            ,;		// 	[01]  C   Titulo do campo
					 	   STR0031				,;		// 	[02]  C   ToolTip do campo
					 	   "TAXAS"		        ,;		// 	[03]  C   Id do Field
					 	   "N"					,;		// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_TAXA")[1],;		// 	[05]  N   Tamanho do campo
					 	   TamSx3("G3R_TAXA")[2],;		// 	[06]  N   Decimal do campo
					 	   NIL					,;		// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				    ,;		// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					,;		//	[09]  A   Lista de valores permitido do campo
					 	   .F.					,;		//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					,;		//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				    ,;		//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				    ,;		//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual	
		 	
		oStrTOTDR:AddField(STR0032	              ,;	// 	[01]  C   Titulo do campo
					 	   STR0032				  ,;	// 	[02]  C   ToolTip do campo
					 	   "EXTRAS"		          ,;	// 	[03]  C   Id do Field
					 	   "N"					  ,;	// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_EXTRAS")[1],;	// 	[05]  N   Tamanho do campo
					 	   TamSx3("G3R_EXTRAS")[2],;	// 	[06]  N   Decimal do campo
					 	   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					  ,;	//	[09]  A   Lista de valores permitido do campo
					 	   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual	
	
		oStrTOTDR:AddField(STR0033	            ,;		// 	[01]  C   Titulo do campo
					 	   STR0033				,;		// 	[02]  C   ToolTip do campo
					 	   "ACORDOS"		    ,;		// 	[03]  C   Id do Field
					 	   "N"					,;		// 	[04]  C   Tipo do campo
					 	   TamSx3("G3R_TAXA")[1],;		// 	[05]  N   Tamanho do campo
					 	   TamSx3("G3R_TAXA")[2],;		// 	[06]  N   Decimal do campo
					 	   NIL					,;		// 	[07]  B   Code-block de valida��o do campo
					 	   NIL 				    ,;		// 	[08]  B   Code-block de valida��o When do campo
					 	   NIL					,;		//	[09]  A   Lista de valores permitido do campo
					 	   .F.					,;		//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
					 	   NIL					,;		//	[11]  B   Code-block de inicializacao do campo
					 	   .F.				    ,;		//	[12]  L   Indica se trata-se de um campo chave
					 	   .F.				    ,;		//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
					 	   .T.)							// 	[14]  L   Indica se o campo � virtual	
	
		oStrTOTDR:AddField(STR0070	              ,;	// 	[01]  C   Titulo do campo		// "Status"
						   STR0070				  ,;	// 	[02]  C   ToolTip do campo
						   "STATUS"		          ,;	// 	[03]  C   Id do Field
						   "C"					  ,;	// 	[04]  C   Tipo do campo
						   TamSx3("G3R_STATUS")[1],;	// 	[05]  N   Tamanho do campo
						   TamSx3("G3R_STATUS")[2],;	// 	[06]  N   Decimal do campo
						   NIL					  ,;	// 	[07]  B   Code-block de valida��o do campo
						   NIL 				      ,;	// 	[08]  B   Code-block de valida��o When do campo
						   aCombo				  ,;	//	[09]  A   Lista de valores permitido do campo
						   .F.					  ,;	//	[10]  L   Indica se o campo tem preenchimento obrigat�rio
						   NIL					  ,;	//	[11]  B   Code-block de inicializacao do campo
						   .F.				      ,;	//	[12]  L   Indica se trata-se de um campo chave
						   .F.				      ,;	//	[13]  L   Indica se o campo N�O pode receber valor em uma opera��o de update.
						   .T.)							// 	[14]  L   Indica se o campo � virtual	
	Endif
Else
	oStrTOTDR:AddField('DTINI',;		// [01] Campo
					   '01'   ,;        // [02] Ordem
					   STR0028,;        // [03] Titulo
					   STR0028,;        // [04] Descricao
					          ,;        // [05] Help
					   'GET'  ,;        // [06] Tipo do campo   COMBO, Get ou CHECK
					   "@!"   ,;        // [07] Picture
					   NIl    ,;        // [08] PictVar
					   ' '    ,;    	// [09] F3   
					   .F.    ,; 		// [10] L Indica se o campo � edit�vel
					   NIL    ,; 		// [11] C Pasta do campo
					   NIL    ,; 		// [12] C Agrupamento do campo
					   {}     ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL    ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL    ,;	 	// [15] C Inicializador de Browse
					   .T.) 			// [16] L Indica se o campo � virtual         
	
	oStrTOTDR:AddField('DTFIM',;        // [01] Campo
		               '02'   ,;        // [02] Ordem
		               STR0029,;        // [03] Titulo
		               STR0029,;        // [04] Descricao
		                      ,;        // [05] Help
		               'GET'  ,;        // [06] Tipo do campo   COMBO, Get ou CHECK
		               "@!"   ,;        // [07] Picture
		               NIL    ,;        // [08] PictVar
		               ' '    ,;    	// [09] F3   
	                   .F.    ,; 		// [10] L Indica se o campo � edit�vel
					   NIL    ,; 		// [11] C Pasta do campo
					   NIL    ,; 		// [12] C Agrupamento do campo
					   {}     ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL    ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL    ,;	 	// [15] C Inicializador de Browse
					   .T.) 			// [16] L Indica se o campo � virtual   
					
	oStrTOTDR:AddField('TARIFAS'                    ,;		// [01] Campo
		               '03'                         ,;      // [02] Ordem
		               STR0030                      ,;      // [03] Titulo
		               STR0030                      ,;      // [04] Descricao
		                                            ,;      // [05] Help
		               'GET'                        ,;      // [06] Tipo do campo   COMBO, Get ou CHECK
		               PesqPict("G3R", "G3R_TARIFA"),;      // [07] Picture
		               NIL                          ,;      // [08] PictVar
		               ' '                          ,;    	// [09] F3   
	                   .F.                          ,; 		// [10] L Indica se o campo � edit�vel
					   NIL                          ,; 		// [11] C Pasta do campo
					   NIL                          ,; 		// [12] C Agrupamento do campo
					   {}                           ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL                          ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL                          ,;	 	// [15] C Inicializador de Browse
					   .T.) 								// [16] L Indica se o campo � virtual   
		
	oStrTOTDR:AddField('TAXAS'                    ,;		// [01] Campo
					   '04'                       ,;        // [02] Ordem
					   STR0031                    ,;        // [03] Titulo
					   STR0031                    ,;        // [04] Descricao
					                              ,;        // [05] Help
					   'GET'                      ,;        // [06] Tipo do campo   COMBO, Get ou CHECK
					   PesqPict("G3R", "G3R_TAXA"),;        // [07] Picture
					   NIL                        ,;        // [08] PictVar
					   ' '                        ,;    	// [09] F3   
					   .F.                        ,; 		// [10] L Indica se o campo � edit�vel
					   NIL                        ,; 		// [11] C Pasta do campo
					   NIL                        ,; 		// [12] C Agrupamento do campo
					   {}                         ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL                        ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL                        ,;	 	// [15] C Inicializador de Browse
					   .T.) 								// [16] L Indica se o campo � virtual   
		
	oStrTOTDR:AddField('EXTRAS'                     ,;		// [01] Campo
					   '05'                         ,;      // [02] Ordem
					   STR0032                      ,;      // [03] Titulo
					   STR0032                      ,;      // [04] Descricao
					                                ,;      // [05] Help
					   'GET'                        ,;      // [06] Tipo do campo   COMBO, Get ou CHECK
					   PesqPict("G3R", "G3R_EXTRAS"),;      // [07] Picture
					   NIL                          ,;      // [08] PictVar
					   ' '                          ,;    	// [09] F3   
					   .F.                          ,; 		// [10] L Indica se o campo � edit�vel
					   NIL                          ,; 		// [11] C Pasta do campo
					   NIL                          ,; 		// [12] C Agrupamento do campo
					   {}                           ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL                          ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL                          ,;	 	// [15] C Inicializador de Browse
					   .T.) 								// [16] L Indica se o campo � virtual   
		
	oStrTOTDR:AddField('ACORDOS'                    ,;		// [01] Campo
					   '06'                         ,;      // [02] Ordem
					   STR0033                      ,;      // [03] Titulo
					   STR0033                      ,;      // [04] Descricao
					                                ,;      // [05] Help
					   'GET'                        ,;      // [06] Tipo do campo   COMBO, Get ou CHECK
					   PesqPict("G3R", "G3R_TARIFA"),;      // [07] Picture
					   NIL                          ,;      // [08] PictVar
					   ' '                          ,;    	// [09] F3   
				       .F.                          ,; 		// [10] L Indica se o campo � edit�vel
					   NIL                          ,; 		// [11] C Pasta do campo
					   NIL                          ,; 		// [12] C Agrupamento do campo
					   {}                           ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   NIL                          ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL                          ,;	 	// [15] C Inicializador de Browse
					   .T.) 							    // [16] L Indica se o campo � virtual   
		
	oStrTOTDR:AddField('STATUS',;		// [01] Campo
					   '07'    ,;       // [02] Ordem
					   STR0070 ,;       // [03] Titulo
					   STR0070 ,;       // [04] Descricao
					           ,;       // [05] Help
					   'COMBO' ,;       // [06] Tipo do campo   COMBO, Get ou CHECK
					   "@!"    ,;       // [07] Picture
					   NIL     ,;       // [08] PictVar
					   ' '     ,;    	// [09] F3   
				       .F.     ,; 		// [10] L Indica se o campo � edit�vel
					   NIL     ,; 		// [11] C Pasta do campo
					   NIL     ,; 		// [12] C Agrupamento do campo
					   aCombo  ,; 		// [13] A Lista de valores permitido do campo (Combo)
					   Nil     ,; 		// [14] N Tamanho Maximo da maior op��o do combo
					   NIL     ,;	 	// [15] C Inicializador de Browse
					   .T.) 			// [16] L Indica se o campo � virtual   
Endif

Return oStrTOTDR

Static Function Tu42CDR(oGridModel,lCopy)
Local aLoad 	:= {}
Local aArea		:= GetArea()
Local aAreaG3R	:= G3R->(GetArea())

aAdd(aLoad,{0,{G3R->G3R_FILIAL, G3R->G3R_NUMID, G3R->G3R_IDITEM, G3R->G3R_NUMSEQ,cTOD("  / /  "),cTOD("  / /  "),0,0,0,0,''}})
RestArea(aAreaG3R)
RestArea(aArea)

TURXNIL(aArea)
TURXNIL(aAreaG3R)

Return aLoad

/*/{Protheus.doc} TA042AddFlds
Adiciona os campos do model e da view de pesquisa
@type Static Function
@author Thiago Tavares
@since 04/05/2017
@version 12.1.14
/*/
Static Function TA042AddFlds(oStruct, nOpc)

If nOpc == 1
	oStruct:AddField(STR0034                , ; 	// [01] C Titulo do campo			// "N�m. Ident."
					 STR0035                , ; 	// [02] C ToolTip do campo			// "N�mero Identifica��o Reserva"
					 "SRC_NUMID"            , ; 	// [03] C identificador (ID) do Field
					 "C"                    , ; 	// [04] C Tipo do campo
					 TamSX3("G3R_NUMID")[1] , ; 	// [05] N Tamanho do campo
					 0 					    , ;		// [06] N Decimal do campo
					 {|| .T.}	            , ;		// [07] B Code-block de valida��o do campo
					 {|| .T.}			    , ;		// [08] B Code-block de valida��o When do campo
					 NIL                    , ;		// [09] A Lista de valores permitido do campo
					 .F.                    , ;		// [10] L Indica se o campo tem preenchimento obrigat�rio
					 NIL                    , ;		// [11] B Code-block de inicializacao do campo
					 .F. 				    , ;		// [12] L Indica se trata de um campo chave
					 .F. 				    , ;		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
					 .T.) 							// [14] L Indica se o campo � virtual

	oStruct:AddField(STR0036                , ; 	// [01] C Titulo do campo			// "Ordem Serv."
					 STR0037                , ; 	// [02] C ToolTip do campo			// "N�mero da Ordem de Servi�o"
					 "SRC_ORDER"            , ; 	// [03] C identificador (ID) do Field
					 "C"                    , ; 	// [04] C Tipo do campo
					 TamSX3("G3Q_ORDER")[1] , ; 	// [05] N Tamanho do campo
					 0 					    , ; 	// [06] N Decimal do campo
					 {|| .T.}		        , ; 	// [07] B Code-block de valida��o do campo
					 {|| .T.}			    , ; 	// [08] B Code-block de valida��o When do campo
					 NIL                    , ; 	// [09] A Lista de valores permitido do campo
					 .F.                    , ; 	// [10] L Indica se o campo tem preenchimento obrigat�rio
					 NIL                    , ; 	// [11] B Code-block de inicializacao do campo
					 .F. 				    , ; 	// [12] L Indica se trata de um campo chave
					 .F. 				    , ; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
					 .T.) 							// [14] L Indica se o campo � virtual

	oStruct:AddField(STR0038                , ;		// [01] C Titulo do campo			// "Item O.S."
					 STR0039                , ;		// [02] C ToolTip do campo			// "Item da Ordem de Servi�o"
					 "SRC_ITOS"             , ;		// [03] C identificador (ID) do Field
					 "C"                    , ;		// [04] C Tipo do campo
					 TamSX3("G3Q_ITOS")[1]  , ;		// [05] N Tamanho do campo
					 0 					    , ;		// [06] N Decimal do campo
					 {|| .T.}	            , ;		// [07] B Code-block de valida��o do campo
					 {|| .T.}			    , ;		// [08] B Code-block de valida��o When do campo
					 NIL                    , ;		// [09] A Lista de valores permitido do campo
					 .F.                    , ;		// [10] L Indica se o campo tem preenchimento obrigat�rio
					 NIL                    , ;		// [11] B Code-block de inicializacao do campo
					 .F.				    , ; 	// [12] L Indica se trata de um campo chave
					 .F. 				    , ; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
					 .T.) 							// [14] L Indica se o campo � virtual

	oStruct:AddField(STR0040                , ;		// [01] C Titulo do campo			// "Loc. GDS"
					 STR0041                , ;		// [02] C ToolTip do campo			// "Localizador GDS"
					 "SRC_LOCGDS"           , ;		// [03] C identificador (ID) do Field
					 "C"                    , ;		// [04] C Tipo do campo
					 TamSX3("G3R_LOCGDS")[1], ;		// [05] N Tamanho do campo
					 0 					    , ;		// [06] N Decimal do campo
					 {|| .T.}	            , ;		// [07] B Code-block de valida��o do campo
					 {|| .T.}			    , ;		// [08] B Code-block de valida��o When do campo
					 NIL                    , ;		// [09] A Lista de valores permitido do campo
					 .F.                    , ;		// [10] L Indica se o campo tem preenchimento obrigat�rio
					 NIL                    , ;		// [11] B Code-block de inicializacao do campo
					 .F. 				    , ;		// [12] L Indica se trata de um campo chave
					 .F. 				    , ;		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
					 .T.) 							// [14] L Indica se o campo � virtual

	oStruct:AddField(STR0071                , ;		// [01] C Titulo do campo			// "Id. Conf."
					 STR0072                , ;		// [02] C ToolTip do campo			// "Id. Confirma��o"
					 "SRC_IDCONF"           , ;		// [03] C identificador (ID) do Field
					 "C"                    , ;		// [04] C Tipo do campo
					 TamSX3("G3R_IDCONF")[1], ;		// [05] N Tamanho do campo
					 0 					    , ;		// [06] N Decimal do campo
					 {|| .T.}	            , ;		// [07] B Code-block de valida��o do campo
					 {|| .T.}			    , ;		// [08] B Code-block de valida��o When do campo
					 NIL                    , ;		// [09] A Lista de valores permitido do campo
					 .F.                    , ;		// [10] L Indica se o campo tem preenchimento obrigat�rio
					 NIL                    , ;		// [11] B Code-block de inicializacao do campo
					 .F. 				    , ;		// [12] L Indica se trata de um campo chave
					 .F. 				    , ;		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
					 .T.) 							// [14] L Indica se o campo � virtual

Else
	oStruct:AddField("SRC_NUMID" , ; 	// [01] C Nome do Campo
					 "01"		 , ; 	// [02] C Ordem
					 STR0034 	 , ; 	// [03] C Titulo do campo			// "N�m. Ident."
					 STR0035     , ; 	// [04] C Descri��o do campo		// "N�mero Identifica��o Reserva"
					 {} 		 , ; 	// [05] A Array com Help
					 "GET"		 , ; 	// [06] C Tipo do campo GET, COMBO OU CHECK
					 "@!"        , ; 	// [07] C Picture
					 NIL 		 , ; 	// [08] B Bloco de Picture Var
					 "" 		 , ; 	// [09] C Consulta F3
					 .T. 		 , ; 	// [10] L Indica se o campo � edit�vel
					 NIL 		 , ; 	// [11] C Pasta do campo
					 NIL 		 , ; 	// [12] C Agrupamento do campo
					 NIL         , ; 	// [13] A Lista de valores permitido do campo (Combo)
					 NIL 		 , ; 	// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL 		 , ; 	// [15] C Inicializador de Browse
					 .T.         , ; 	// [16] L Indica se o campo � virtual
					 NIL) 				// [17] C Picture Vari�vel

	oStruct:AddField("SRC_ORDER" , ; 	// [01] C Nome do Campo
					 "02"		 , ; 	// [02] C Ordem
					 STR0036 	 , ; 	// [03] C Titulo do campo			// "Ordem Serv."
					 STR0037     , ; 	// [04] C Descri��o do campo		// "N�mero da Ordem de Servi�o"
					 {} 		 , ; 	// [05] A Array com Help
					 "GET"		 , ; 	// [06] C Tipo do campo GET, COMBO OU CHECK
					 "@!"        , ; 	// [07] C Picture
					 NIL 		 , ; 	// [08] B Bloco de Picture Var
					 "" 		 , ; 	// [09] C Consulta F3
					 .T. 		 , ; 	// [10] L Indica se o campo � edit�vel
					 NIL 		 , ; 	// [11] C Pasta do campo
					 NIL 		 , ; 	// [12] C Agrupamento do campo
					 NIL         , ; 	// [13] A Lista de valores permitido do campo (Combo)
					 NIL 		 , ; 	// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL 		 , ; 	// [15] C Inicializador de Browse
					 .T.         , ; 	// [16] L Indica se o campo � virtual
					 NIL) 				// [17] C Picture Vari�vel

	oStruct:AddField("SRC_ITOS"  , ; 	// [01] C Nome do Campo
					 "03"		 , ; 	// [02] C Ordem
					 STR0038 	 , ; 	// [03] C Titulo do campo			// "Item O.S."
					 STR0039     , ; 	// [04] C Descri��o do campo		// "Item da Ordem de Servi�o"
					 {} 		 , ; 	// [05] A Array com Help
					 "GET"		 , ; 	// [06] C Tipo do campo GET, COMBO OU CHECK
					 "@!"        , ; 	// [07] C Picture
					 NIL 		 , ; 	// [08] B Bloco de Picture Var
					 "" 		 , ; 	// [09] C Consulta F3
					 .T. 		 , ; 	// [10] L Indica se o campo � edit�vel
					 NIL 		 , ; 	// [11] C Pasta do campo
					 NIL 		 , ; 	// [12] C Agrupamento do campo
					 NIL         , ; 	// [13] A Lista de valores permitido do campo (Combo)
					 NIL 		 , ; 	// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL 		 , ; 	// [15] C Inicializador de Browse
					 .T.         , ; 	// [16] L Indica se o campo � virtual
					 NIL) 				// [17] C Picture Vari�vel

	oStruct:AddField("SRC_LOCGDS", ; 	// [01] C Nome do Campo
					 "04"		 , ;	// [02] C Ordem
					 STR0040 	 , ; 	// [03] C Titulo do campo			// "Loc. GDS"
					 STR0041     , ; 	// [04] C Descri��o do campo		// "Localizador GDS"
					 {} 		 , ;	// [05] A Array com Help
					 "GET"		 , ;	// [06] C Tipo do campo GET, COMBO OU CHECK
					 "@!"        , ;	// [07] C Picture
					 NIL 		 , ; 	// [08] B Bloco de Picture Var
					 "" 		 , ; 	// [09] C Consulta F3
					 .T. 		 , ; 	// [10] L Indica se o campo � edit�vel
					 NIL 		 , ; 	// [11] C Pasta do campo
					 NIL 		 , ; 	// [12] C Agrupamento do campo
					 NIL         , ; 	// [13] A Lista de valores permitido do campo (Combo)
					 NIL 		 , ; 	// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL 		 , ; 	// [15] C Inicializador de Browse
					 .T.         , ; 	// [16] L Indica se o campo � virtual
					 NIL) 		        // [17] C Picture Vari�vel

	oStruct:AddField("SRC_IDCONF", ; 	// [01] C Nome do Campo
					 "05"		 , ;	// [02] C Ordem
					 STR0071 	 , ; 	// [03] C Titulo do campo			// "Id. Conf."
					 STR0072     , ; 	// [04] C Descri��o do campo		// "Id. Confirma��o"
					 {} 		 , ;	// [05] A Array com Help
					 "GET"		 , ;	// [06] C Tipo do campo GET, COMBO OU CHECK
					 "@!"        , ;	// [07] C Picture
					 NIL 		 , ; 	// [08] B Bloco de Picture Var
					 "" 		 , ; 	// [09] C Consulta F3
					 .T. 		 , ; 	// [10] L Indica se o campo � edit�vel
					 NIL 		 , ; 	// [11] C Pasta do campo
					 NIL 		 , ; 	// [12] C Agrupamento do campo
					 NIL         , ; 	// [13] A Lista de valores permitido do campo (Combo)
					 NIL 		 , ; 	// [14] N Tamanho M�ximo da maior op��o do combo
					 NIL 		 , ; 	// [15] C Inicializador de Browse
					 .T.         , ; 	// [16] L Indica se o campo � virtual
					 NIL) 		        // [17] C Picture Vari�vel

EndIf

Return

/*/{Protheus.doc} TA042SrcG3R
Fun��o que realiza a pesquisa atrav�s do modelo tempor�rio
@type Static Function
@author Thiago Tavares
@since 05/05/2017
@version 12.1.14
/*/
Static Function TA042SrcG3R()

Local aArea     := GetArea()
Local cAliasG3R := ""
Local cAliasMrk := ""
Local oModel    := FwModelActive()
Local oMdlSRC   := oModel:GetModel('TMP_SEARCH')
Local oView     := FwViewActive()
Local oDlg      := Nil
Local oFWLayer  := Nil
Local oPainel   := Nil
Local oMarkG3R  := Nil
Local oButton   := Nil
Local nLine     := 0
Local nTotReg   := 0
Local cQuery    := ""
Local cWhere    := ""
Local aColunas  := {}

If Empty(oMdlSRC:GetValue('SRC_NUMID'))  .And. ;
   Empty(oMdlSRC:GetValue('SRC_ORDER'))  .And. ;
   Empty(oMdlSRC:GetValue('SRC_ITOS'))   .And. ;
   Empty(oMdlSRC:GetValue('SRC_LOCGDS')) .And. ;
   Empty(oMdlSRC:GetValue('SRC_IDCONF'))
	Help( , , "TA042SrcG3R", , STR0058, 1, 0)		// "� obrigat�rio o preenchimento de pelo menos um dos campos para pesquisa."		 	
Else  
	SetKey(VK_F5, {||})
	
	If !Empty(oMdlSRC:GetValue('SRC_NUMID'))
		cWhere += " G3R_NUMID = '"  + StrTran(StrTran(oMdlSRC:GetValue('SRC_NUMID'), '"', ''), "'", "")  + "' AND "
	EndIf
	 
	If !Empty(oMdlSRC:GetValue('SRC_ORDER'))
		cWhere += " G3Q_ORDER = '"  + StrTran(StrTran(oMdlSRC:GetValue('SRC_ORDER'), '"', ''), "'", "")  + "' AND " 
	EndIf
	 
	If !Empty(oMdlSRC:GetValue('SRC_ITOS'))
		cWhere += " G3Q_ITOS = '"   + StrTran(StrTran(oMdlSRC:GetValue('SRC_ITOS'), '"', ''), "'", "")   + "' AND " 
	EndIf
	 
	If !Empty(oMdlSRC:GetValue('SRC_LOCGDS'))
		cWhere += " G3R_LOCGDS = '" + StrTran(StrTran(oMdlSRC:GetValue('SRC_LOCGDS'), '"', ''), "'", "") + "' AND " 
	EndIf
	 
	If !Empty(oMdlSRC:GetValue('SRC_IDCONF'))
		cWhere += " G3R_IDCONF = '" + StrTran(StrTran(oMdlSRC:GetValue('SRC_IDCONF'), '"', ''), "'", "") + "' AND " 
	EndIf
	
	cQuery := "SELECT "
	cQuery += "		'  ' AS G3R_MARK, "
	cQuery += "		G3R_FILIAL, "
	cQuery += "		G3R_NUMID, "
	cQuery += "		G3R_IDITEM, "
	cQuery += "		G3R_NUMSEQ, "
	cQuery += "		(CASE "
	cQuery += "			WHEN G3Q_OPERAC = '1' THEN '" + STR0042 + "' "		// "Emiss�o" 
	cQuery += "			WHEN G3Q_OPERAC = '2' THEN '" + STR0055 + "' "		// "Reembolso"		
	cQuery += "			WHEN G3Q_OPERAC = '3' THEN '" + STR0043 + "' "		// "Reemiss�o" 
	cQuery += "		END) G3Q_OPERAC, "
	cQuery += "		(CASE "
	cQuery += "			WHEN G3Q_TPDOC = '1' THEN '" + STR0044 + "' "		// "Voucher" 
	cQuery += "			WHEN G3Q_TPDOC = '4' THEN '" + STR0054 + "' "		// "Doc. Adicional" 
	cQuery += "		END) G3Q_TPDOC, "
	cQuery += "		G3Q_DOC, "
	cQuery += "		G3Q_EMISS, "
	cQuery += "		G3R_FORNEC, "
	cQuery += "		G3R_LOJA, "
	cQuery += "		A2_NOME, "
	cQuery += "		G3R_ACERTO, "
	cQuery += "		G3R_TARIFA, "
	cQuery += "		G3R_TAXA, "
	cQuery += "		G3R_EXTRAS, "
	cQuery += "		G3R_VLCOMI, "
	cQuery += "		G3R_TAXADU, "
	cQuery += "		G3R_VLINCE, "
	cQuery += "		G3R_FORREP, "
	cQuery += "		G3R_LOJREP, "
	cQuery += "		G3R_NOMREP, "
	cQuery += "		G3R_TXFORN, "
	cQuery += "		G3R_TOTREC, "
	cQuery += "		G3Q_POSTO, "
	cQuery += "		G3M_DESCR, "
	cQuery += "		G3Q_CLIENT, "
	cQuery += "		G3Q_LOJA, "
	cQuery += "		A1_NOME, "
	cQuery += "		G3Q_FORMPG, " 
	cQuery += "		G3Q_DESCFP, "
	cQuery += "		G3Q_ORDER, "
	cQuery += "		G3Q_ITOS, "
	cQuery += "		G3R_LOCGDS, "
	cQuery += "		G3R_CONCIL, "
	cQuery += "		G3R_STATUS, "
	cQuery += "		G3R_SEQNSH, "
	cQuery += "		G3R_SEQPRC, "
	cQuery += "		G3R_MOEDA, "
	cQuery += "		G3R_MSFIL "
	cQuery += "	FROM " + RetSQLName("G3R") + " G3R "
	cQuery += "	INNER JOIN " + RetSQLName("G3Q") + " G3Q ON G3Q_FILIAL = G3R_FILIAL AND "
	cQuery += "	                                     G3Q_NUMID  = G3R_NUMID  AND "
	cQuery += "	                                     G3Q_IDITEM = G3R_IDITEM AND "
	cQuery += "	                                     G3Q_NUMSEQ = G3R_NUMSEQ AND "
	cQuery += "	                                     G3Q_CONINU = ''         AND "
	cQuery += "	                                     G3Q.D_E_L_E_T_ = '' "
	cQuery += "	INNER JOIN " + RetSQLName("G3M") + " G3M ON G3M_FILIAL = G3Q_FILPST AND "
	cQuery += "	                                     G3M_CODIGO = G3Q_POSTO  AND " 
	cQuery += "	                                     G3M.D_E_L_E_T_ = '' "
	cQuery += "	INNER JOIN " + RetSQLName("SA1") + " SA1 ON A1_FILIAL = '" + xFilial('SA1') + "' AND "
	cQuery += "	                              A1_COD    = G3Q_CLIENT    AND "
	cQuery += "	                              A1_LOJA   = G3Q_LOJA      AND "
	cQuery += "	                              SA1.D_E_L_E_T_ = '' "
	cQuery += "	INNER JOIN " + RetSQLName("SA2") + " SA2 ON A2_FILIAL = '" + xFilial('SA2') + "' AND "
	cQuery += "	                              A2_COD    = G3R_FORNEC    AND "
	cQuery += "	                              A2_LOJA   = G3R_LOJA      AND "
	cQuery += "	                              SA2.D_E_L_E_T_ = '' "

	If !Empty(xFilial('G8C')) .Or. !Empty(xFilial('SA1'))
		cQuery += "	WHERE G3R_FILIAL = '" + xFilial('G3R') + "' AND "
	Else
		cQuery += "	WHERE "
	EndIf
	
	If FwIsInCallStack('TURA042A')
		cQuery += "	      G3Q_OPERAC IN ('1', '3') AND "		// 1=Emiss�o e 3=Reemiss�o
	ElseIf FwIsInCallStack('TURA042R')
		cQuery += "	      G3Q_OPERAC = '2' AND "				// 2=Reembolso
	EndIf
	
	cQuery += "	      G3Q_TPDOC  IN ('1', '4') AND "			// 1=Voucher e 4=Doc Adicional
	cQuery += "		 (G3R_ACERTO = '2' OR (G3R_ACERTO = '1' AND G3R_CONALT = '')) AND "		// mostra acerto manual
	cQuery += "		  G3R_TPSEG <> '1' AND "
	cQuery += "		  G3R_CONINU = ''  AND " + cWhere
	cQuery += "		  G3R.D_E_L_E_T_ = '' "
	
	cQuery    := ChangeQuery(cQuery)
	cAliasG3R := GetNextAlias()
	DBUseArea(.T., "TOPCONN", TCGenQry( , , cQuery), cAliasG3R, .F., .T.)
	DBSelectArea(cAliasG3R)
		
	Count To nTotReg
	
	(cAliasG3R)->(DbGoTop())
		
	If (cAliasG3R)->(EOF())	
		Help( , , "TA042SrcG3R", , STR0045, 1, 0)		// "Documento de Reserva n�o encontrado."	
	Else	
		If nTotReg == 1 .Or. (nTotReg == 2 .And. TA042TemAc((cAliasG3R)))
			If TA042VldDR(oModel, (cAliasG3R)->G3R_MARK  , ;
	                              (cAliasG3R)->G3R_FILIAL, ;
	                              (cAliasG3R)->G3R_NUMID , ;
	                              (cAliasG3R)->G3R_IDITEM, ;
	                              (cAliasG3R)->G3R_NUMSEQ, ;
	                              (cAliasG3R)->G3R_FORREP, ;
	                              (cAliasG3R)->G3R_LOJREP, ;
	                              (cAliasG3R)->G3R_CONCIL, ;
	                              (cAliasG3R)->G3R_STATUS, ;
	                              (cAliasG3R)->G3R_SEQNSH, ;
	                              (cAliasG3R)->G3R_SEQPRC, ;
	                              (cAliasG3R)->G3R_MOEDA)

				FwMsgRun( , {|| TA042FillG3R(oModel, (cAliasG3R), 1)}, , STR0046)		// "Adicionando o(s) Documento(s) de Reserva..."
			
			EndIf	
		
		Else
			cAliasMrk := GetNextAlias()
			
			aAdd(aColunas, {TitSX3("G3R_FILIAL")[1], "G3R_FILIAL", "C", TAMSX3("G3R_FILIAL")[1], TAMSX3("G3R_FILIAL")[2], GetSx3Cache("G3R_FILIAL", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_NUMID" )[1], "G3R_NUMID" , "C", TAMSX3("G3R_NUMID" )[1], TAMSX3("G3R_NUMID" )[2], GetSx3Cache("G3R_NUMID" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_IDITEM")[1], "G3R_IDITEM", "C", TAMSX3("G3R_IDITEM")[1], TAMSX3("G3R_IDITEM")[2], GetSx3Cache("G3R_IDITEM", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_NUMSEQ")[1], "G3R_NUMSEQ", "C", TAMSX3("G3R_NUMSEQ")[1], TAMSX3("G3R_NUMSEQ")[2], GetSx3Cache("G3R_NUMSEQ", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_FORNEC")[1], "G3R_FORNEC", "C", TAMSX3("G3R_FORNEC")[1], TAMSX3("G3R_FORNEC")[2], GetSx3Cache("G3R_FORNEC", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_LOJA"  )[1], "G3R_LOJA"  , "C", TAMSX3("G3R_LOJA"  )[1], TAMSX3("G3R_LOJA"  )[2], GetSx3Cache("G3R_LOJA"  , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("A2_NOME"   )[1], "A2_NOME"   , "C", TAMSX3("A2_NOME"   )[1], TAMSX3("A2_NOME"   )[2], GetSx3Cache("A2_NOME"   , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_FORREP")[1], "G3R_FORREP", "C", TAMSX3("G3R_FORREP")[1], TAMSX3("G3R_FORREP")[2], GetSx3Cache("G3R_FORREP", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_LOJREP")[1], "G3R_LOJREP", "C", TAMSX3("G3R_LOJREP")[1], TAMSX3("G3R_LOJREP")[2], GetSx3Cache("G3R_LOJREP", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_NOMREP")[1], "G3R_NOMREP", "C", TAMSX3("G3R_NOMREP")[1], TAMSX3("G3R_NOMREP")[2], GetSx3Cache("G3R_NOMREP", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_FORMPG")[1], "G3Q_FORMPG", "C", TAMSX3("G3Q_FORMPG")[1], TAMSX3("G3Q_FORMPG")[2], GetSx3Cache("G3Q_FORMPG", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_DESCFP")[1], "G3Q_DESCFP", "C", TAMSX3("G3Q_DESCFP")[1], TAMSX3("G3Q_DESCFP")[2], GetSx3Cache("G3Q_DESCFP", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_TARIFA")[1], "G3R_TARIFA", "N", TAMSX3("G3R_TARIFA")[1], TAMSX3("G3R_TARIFA")[2], GetSx3Cache("G3R_TARIFA", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_TAXA"  )[1], "G3R_TAXA"  , "N", TAMSX3("G3R_TAXA"  )[1], TAMSX3("G3R_TAXA"  )[2], GetSx3Cache("G3R_TAXA"  , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_EXTRAS")[1], "G3R_EXTRAS", "N", TAMSX3("G3R_EXTRAS")[1], TAMSX3("G3R_EXTRAS")[2], GetSx3Cache("G3R_EXTRAS", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_VLCOMI")[1], "G3R_VLCOMI", "N", TAMSX3("G3R_VLCOMI")[1], TAMSX3("G3R_VLCOMI")[2], GetSx3Cache("G3R_VLCOMI", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_TAXADU")[1], "G3R_TAXADU", "N", TAMSX3("G3R_TAXADU")[1], TAMSX3("G3R_TAXADU")[2], GetSx3Cache("G3R_TAXADU", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_VLINCE")[1], "G3R_VLINCE", "N", TAMSX3("G3R_VLINCE")[1], TAMSX3("G3R_VLINCE")[2], GetSx3Cache("G3R_VLINCE", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_TXFORN")[1], "G3R_TXFORN", "N", TAMSX3("G3R_TXFORN")[1], TAMSX3("G3R_TXFORN")[2], GetSx3Cache("G3R_TXFORN", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_TOTREC")[1], "G3R_TOTREC", "N", TAMSX3("G3R_TOTREC")[1], TAMSX3("G3R_TOTREC")[2], GetSx3Cache("G3R_TOTREC", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_OPERAC")[1], "G3Q_OPERAC", "C",                      30, TAMSX3("G3Q_OPERAC")[2], GetSx3Cache("G3Q_OPERAC", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_TPDOC" )[1], "G3Q_TPDOC" , "C",                      20, TAMSX3("G3Q_TPDOC" )[2], GetSx3Cache("G3Q_TPDOC" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_DOC"   )[1], "G3Q_DOC"   , "C", TAMSX3("G3Q_DOC"   )[1], TAMSX3("G3Q_DOC"   )[2], GetSx3Cache("G3Q_DOC"   , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_EMISS" )[1], "G3Q_EMISS" , "D", TAMSX3("G3Q_EMISS" )[1], TAMSX3("G3Q_EMISS" )[2], GetSx3Cache("G3Q_EMISS" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_POSTO" )[1], "G3Q_POSTO" , "C", TAMSX3("G3Q_POSTO" )[1], TAMSX3("G3Q_POSTO" )[2], GetSx3Cache("G3Q_POSTO" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3M_DESCR" )[1], "G3M_DESCR" , "C", TAMSX3("G3M_DESCR" )[1], TAMSX3("G3M_DESCR" )[2], GetSx3Cache("G3M_DESCR" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_CLIENT")[1], "G3Q_CLIENT", "C", TAMSX3("G3Q_CLIENT")[1], TAMSX3("G3Q_CLIENT")[2], GetSx3Cache("G3Q_CLIENT", "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_LOJA"  )[1], "G3Q_LOJA"  , "C", TAMSX3("G3Q_LOJA"  )[1], TAMSX3("G3Q_LOJA"  )[2], GetSx3Cache("G3Q_LOJA"  , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("A1_NOME"   )[1], "A1_NOME"   , "C", TAMSX3("A1_NOME"   )[1], TAMSX3("A1_NOME"   )[2], GetSx3Cache("A1_NOME"   , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_ORDER" )[1], "G3Q_ORDER" , "C", TAMSX3("G3Q_ORDER" )[1], TAMSX3("G3Q_ORDER" )[2], GetSx3Cache("G3Q_ORDER" , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3Q_ITOS"  )[1], "G3Q_ITOS"  , "C", TAMSX3("G3Q_ITOS"  )[1], TAMSX3("G3Q_ITOS"  )[2], GetSx3Cache("G3Q_ITOS"  , "X3_PICTURE")})
			aAdd(aColunas, {TitSX3("G3R_LOCGDS")[1], "G3R_LOCGDS", "C", TAMSX3("G3R_LOCGDS")[1], TAMSX3("G3R_LOCGDS")[2], GetSx3Cache("G3R_LOCGDS", "X3_PICTURE")})
		
			Define MsDialog oDlg Title STR0048 From 0, 0 To 360, 960 Pixel // "Selecionar Documento de Reserva" 
			
				oFWLayer := FWLayer():New()
				oFWLayer:Init(oDlg, .F., .T.)
				oFWLayer:AddLine('ALLL', 100, .T.)
				oFWLayer:AddCollumn('ALLC' , 100, .T., 'ALLL')
				oPanel := oFWLayer:GetColPanel('ALLC' , 'ALLL')
				
				oMarkG3R := FWMarkBrowse():New()
				oMarkG3R:SetOwner(oPanel)
				oMarkG3R:SetDataQuery(.T.)
				oMarkG3R:SetQuery(cQuery)
				oMarkG3R:SetAlias(cAliasMrk)
				oMarkG3R:SetFieldMark('G3R_MARK')
				oMarkG3R:Mark('OK')
				oMarkG3R:SetFields(aColunas)
				oMarkG3R:SetValid({|| TA042VldDR(oModel, (cAliasMrk)->G3R_MARK  , ;
					                                     (cAliasMrk)->G3R_FILIAL, ;
					                                     (cAliasMrk)->G3R_NUMID , ;
					                                     (cAliasMrk)->G3R_IDITEM, ;
					                                     (cAliasMrk)->G3R_NUMSEQ, ;
					                                     (cAliasMrk)->G3R_FORREP, ;
					                                     (cAliasMrk)->G3R_LOJREP, ;
					                                     (cAliasMrk)->G3R_CONCIL, ;
					                                     (cAliasMrk)->G3R_STATUS, ;
					                                     (cAliasMrk)->G3R_SEQNSH, ;
					                                     (cAliasMrk)->G3R_SEQPRC, ;
					                                     (cAliasMrk)->G3R_MOEDA)})
				oMarkG3R:DisableReport()
				oMarkG3R:DisableSaveConfig()
				oMarkG3R:SetMenuDef('')
				oMarkG3R:Activate()
				
				oButton := FwButtonBar():New()
				oButton:Init(oPanel, 015, 015, CONTROL_ALIGN_BOTTOM, .T.)
				oButton:AddBtnText(STR0049, STR0050, {|| oMarkG3R:DeActivate(), oPanel:Destroy(), oFWLayer:Destroy(), oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) // "Sair" "Cancela a pesquisa"
				oButton:AddBtnText("Ok", "Ok", {|| FwMsgRun( , {|| TA042FillG3R(oModel, (cAliasMrk), 2)}, , STR0046), oMarkG3R:DeActivate(), oPanel:Destroy(), oFWLayer:Destroy(), oDlg:End()}, , , CONTROL_ALIGN_RIGHT, .T.) // "Adicionando o(s) Documento(s) de Reserva..."
		
			Activate MsDialog oDlg Center
		EndIf
	EndIf
	(cAliasG3R)->(DbCloseArea())
			
	oMdlSRC:ClearField('SRC_NUMID')
	oMdlSRC:ClearField('SRC_ORDER')
	oMdlSRC:ClearField('SRC_ITOS')
	oMdlSRC:ClearField('SRC_LOCGDS')
	oMdlSRC:ClearField('SRC_IDCONF')

	RestArea(aArea)
	
	//Atualiza o grid de totalizadores
	oView:Refresh()
	
	SetKey(VK_F5, {|| FwMsgRun( , {|| TA042SrcG3R()}, , STR0047)})		// "Pesquisando o Documento de Reserva..." 
EndIf

TURXNIL(aArea)
TURXNIL(aColunas)

Return .T.

/*/{Protheus.doc} TA042Destroy
Fun��o que destroi as tabelas temporarias
@type Static Function
@author Thiago Tavares
@since 05/05/2017
@version 12.1.14
/*/
Static Function TA042Destroy()

Local aListArrays := {}
Local lRet := .F.
Local nX := 0

If (lRet := oCacheCalc:List(aListArrays))
	TURXNIL(aListArrays)
EndIf
	
oCacheCalc:Clean()
DelClassIntF()

SetKey(VK_F5, {||})
SetKey(VK_F8, {||})

Return .T.

/*/{Protheus.doc} TA042VldDR

Fun��o para valida��o do DR pesquisado/selecionado 

@type Static Function
@author Thiago Tavares
@since 10/05/2017
@version 12.1.14
/*/
Static Function TA042VldDR(oModel, cMarcado, cFillG3R, cNumId, cIdItem, cNumSeq, cFornRep, cLojaRep, cConcil, cStatus, cSeqNsh, cSeqPrc, cMoeda)

Local lRet := .T.

If Empty(AllTrim(cMarcado))		// se vier vazio � pq est� sendo marcado
	If !Empty(AllTrim(cFillG3R + cNumId + cIdItem + cNumSeq))
		If oModel:GetModel("G3R_ITENS"):SeekLine({{"G3R_FILIAL", cFillG3R}, ;
		                                          {"G3R_NUMID" , cNumId }, ;
		                                          {"G3R_IDITEM", cIdItem}, ;
		                                          {"G3R_NUMSEQ", cNumSeq}}, .T.)
		    If oModel:GetValue("G3R_ITENS", "G3R_DIVERG") != '1'                                       
				TurHelp(STR0053, IIF(!oModel:GetModel("G3R_ITENS"):IsDeleted(), '', STR0081), "TA042VldDR")		// "Recupere a linha exclu�da." 		                                          
				lRet := .F.
			Else
				TurHelp(STR0087, STR0084, "TA042VldDR")		// "Esse Documento de Reserva apresentou problemas ao atualizar os Itens Financeiros e n�o poder� ser inclu�do nessa concilia��o."###"Concilie o Documento de Reserva em outra concilia��o." 		                                          
				lRet := .F.
			EndIf
		ElseIf oModel:GetModel("G3R_ITENS"):SeekLine({{"G3R_FILIAL", cFillG3R}, ;
		                                              {"G3R_NUMID" , cNumId }, ;
		                                              {"G3R_IDITEM", cIdItem}, ;
		                                              {"G3R_NUMSEQ", cSeqNsh}}, .T.)
		    If oModel:GetValue("G3R_ITENS", "G3R_DIVERG") != '1'
				TurHelp(STR0082, STR0084, "TA042VldDR")		// "Um Documento de Reserva de No Show n�o pode ser conciliado atrav�s da concilia��o que o gerou."###"Concilie o Documento de Reserva em outra concilia��o."
				lRet := .F.
			EndIf
		ElseIf oModel:GetModel("G3R_ITENS"):SeekLine({{"G3R_FILIAL", cFillG3R}, ;
		                                              {"G3R_NUMID" , cNumId }, ;
		                                              {"G3R_IDITEM", cIdItem}, ;
		                                              {"G3R_NUMSEQ", cSeqPrc}}, .T.)
		    If oModel:GetValue("G3R_ITENS", "G3R_DIVERG") != '1'
				TurHelp(STR0083, STR0084, "TA042VldDR")		// "Um Documento de Reserva de Parcialidade n�o pode ser conciliado atrav�s da concilia��o que o gerou."###"Concilie o Documento de Reserva em outra concilia��o."	                                          
				lRet := .F.
			EndIf
		ElseIf !Empty(cConcil) 
			Help( , , "TA042VldDR", , I18N(STR0051, {cConcil}), 1, 0)	// "Documento de Reserva foi encontrado na concilia��o #1."
			lRet := .F.
		ElseIf cStatus $ '3|4' 
			Help( , , "TA042VldDR", , IIF(cStatus == '4', STR0056, STR0088), 1, 0)	// "Documento de Reserva cancelado."###"Documento de Reserva finalizado."
			lRet := .F.
		ElseIf !Empty(oModel:GetValue("G8C_MASTER", "G8C_FORNEC")) .And. !Empty(oModel:GetValue("G8C_MASTER", "G8C_LOJA"))
			If (!Empty(cFornRep) .And. oModel:GetValue("G8C_MASTER", "G8C_FORNEC") <> cFornRep) .Or. ;
			   (!Empty(cLojaRep) .And. oModel:GetValue("G8C_MASTER", "G8C_LOJA"  ) <> cLojaRep)
				Help( , , "TA042VldDR", , STR0052, 1, 0)	// "Documento de Reserva com Fornecedor de Reporte diferente do fornecedor da concilia��o."
				lRet := .F.
			EndIf
		EndIf	
	EndIf
EndIf 

Return lRet

/*/{Protheus.doc} TA042FillG3R

Fun��o para preencher a linha do modelo G3R_ITENS com o DR pesquisado/selecionado

@type Static Function
@author Thiago Tavares
@since 10/05/2017
@version 12.1.14
/*/
Static Function TA042FillG3R(oModel, cAliasG3R, nTipo)

Local cBkpFil	  := cFilAnt
Local aArea       := GetArea()
Local oModelT34   := Nil
Local oModelG3P   := Nil
Local oModelG3R   := Nil
Local oModelG48B  := Nil
Local aCopyValues := {}
Local cNatureza   := ""
Local cTipo       := ""
Local cFornRep    := ""
Local cLojaRep    := ""
Local cFornecedor := ""
Local cLoja 	  := ""
Local cFilRV      := ""
Local cNumId      := ""
Local cIdItem     := ""
Local lExistFunc  := Findfunction('U_TURNAT')
Local nX          := 0
Local lContinua   := .T.
Local lRet        := .T.
Local lMoedaDif   := .F.
Local lMark       := .F.

DbSelectArea('G3R')
G3R->(DbSetOrder(1))	// G3R_FILIAL+G3R_NUMID+G3R_IDITEM+G3R_NUMSEQ+G3R_CONINU

(cAliasG3R)->(DbGoTop())

While (cAliasG3R)->(!Eof())
	cFilAnt := (cAliasG3R)->G3R_MSFIL
	If nTipo == 1 .Or. (nTipo == 2 .And. !Empty(AllTrim((cAliasG3R)->G3R_MARK)))
		G3R->(DbSeek((cAliasG3R)->G3R_FILIAL + (cAliasG3R)->G3R_NUMID + (cAliasG3R)->G3R_IDITEM + (cAliasG3R)->G3R_NUMSEQ))
		cFornRep := (cAliasG3R)->G3R_FORREP
		cLojaRep := (cAliasG3R)->G3R_LOJREP
		(cAliasG3R)->(DbSkip())
		
		If (cAliasG3R)->(!Eof()) .And. !Empty(AllTrim((cAliasG3R)->G3R_MARK)) .And. (cFornRep != (cAliasG3R)->G3R_FORREP .Or. cLojaRep != (cAliasG3R)->G3R_LOJREP)
			lContinua := .F.
			Exit
		EndIf
	EndIf
	If (cAliasG3R)->(!Eof())
		(cAliasG3R)->(DbSkip())
	EndIf
EndDo

If !lContinua
	Help( , , "TA042FillG3R", , STR0059, 1, 0)		// "N�o � poss�vel adicionar Documentos de Reserva com Fornecedores diferentes." 
Else 
	(cAliasG3R)->(DbGoTop())
	While (cAliasG3R)->(!Eof())
		If nTipo == 1 .Or. (nTipo == 2 .And. (!Empty(AllTrim((cAliasG3R)->G3R_MARK)) .Or. ;
						                      (cFilRV == (cAliasG3R)->G3R_FILIAL .And. cNumId == (cAliasG3R)->G3R_NUMID .And. cIdItem == (cAliasG3R)->G3R_IDITEM .And. (cAliasG3R)->G3R_ACERTO == '1' .And. lMark)))

			If G3R->(DbSeek((cAliasG3R)->G3R_FILIAL + (cAliasG3R)->G3R_NUMID + (cAliasG3R)->G3R_IDITEM + (cAliasG3R)->G3R_NUMSEQ))
				If !Empty(oModel:GetValue("G8C_MASTER", "G8C_MOEDA")) .And. oModel:GetValue("G8C_MASTER", "G8C_MOEDA") != (cAliasG3R)->G3R_MOEDA
					lMoedaDif := .T.
				Else
					DbSelectArea('G3P')
					G3P->(DbSetOrder(1))
					G3P->(DbSeek((cAliasG3R)->G3R_FILIAL + (cAliasG3R)->G3R_NUMID))
					If SoftLock("G3P")
						oModelT34 := FwLoadModel( 'TURA042H' )
						oModelT34:SetOperation( MODEL_OPERATION_VIEW  )
						oModelT34:GetModel('G48A_ITENS' ):SetLoadFilter({{'G48_CODAPU', "''" , MVC_LOADFILTER_EQUAL     }, {'G48_CONINU', "''", MVC_LOADFILTER_EQUAL}})
						oModelT34:GetModel('G4CA_ITENS' ):SetLoadFilter({{'G4C_STATUS', "'3'", MVC_LOADFILTER_LESS_EQUAL}})
						oModelT34:GetModel('G4CAF_ITENS'):SetLoadFilter({{'G4C_STATUS', "'3'", MVC_LOADFILTER_GREATER   }})
						oModelT34:GetModel('G48B_ITENS' ):SetLoadFilter({{'G48_CODAPU', "''" , MVC_LOADFILTER_EQUAL     }, {'G48_CONINU', "''", MVC_LOADFILTER_EQUAL}})
					
						If oModelT34:Activate()
							TA042AcGrd(oModel, .T.)
							
							If Empty(oModel:GetValue("G8C_MASTER", "G8C_FORNEC")) .Or. Empty(oModel:GetValue("G8C_MASTER", "G8C_LOJA")) 
								oModel:SetValue("G8C_MASTER", "G8C_FORNEC", G3R->G3R_FORREP)
								oModel:SetValue("G8C_MASTER", "G8C_LOJA"  , G3R->G3R_LOJREP)
							EndIf					
					
							aCopyValues := TURxGetVls('G3R_ITENS', .T., {'G4CB_ITENS'}, {}, oModelT34, {}, {{'G48A_ITENS', {{'G48_STATUS', {|x| x != '4'}}}}, {'G48B_ITENS', {{'G48_STATUS', {|x| x != '4'}}}}})
							If TURxSetVs(aCopyValues, {}, oModel)
								TA042SetRec("G3R_ITENS", {{"_CONORI", oModel:GetValue("G8C_MASTER", "G8C_CONCIL" )}}, , .F.)
								TA042SetRec("G3R_ITENS", {{"_CONMAN", "2"}}, , .T.)
					
								oModelG3P  := oModel:GetModel('G3P_FIELDS')
								oModelG3R  := oModel:GetModel('G3R_ITENS')
								oModelG48B := oModel:GetModel('G48B_ITENS')
					
								oModelG3R:SetValue('G3R_FILCON', xFilial('G8C'))
								oModel:GetModel("G8C_MASTER"):SetValue("G8C_MOEDA", oModelG3R:GetValue("G3R_MOEDA"))
					
								If lExistFunc
									If oModelG3R:GetValue('G3R_STATUS') == '1' .And. Empty(oModelG3R:GetValue("G3R_NATURE"))
										cNatureza := U_TURNAT(oModelG3R:GetValue('G3R_MSFIL') , ;
					                                          '1'                             , ; 
					                                          ''                              , ; 
					                                          oModelG3P:GetValue('G3P_SEGNEG'), ; 
					                                          oModelG3R:GetValue('G3R_PROD')  , ;
					                                          '2'                             , ; 
					                                          oModelG3R:GetValue('G3R_FORREP'), ; 
					                                          oModelG3R:GetValue('G3R_LOJREP'))
	
										If !Empty(cNatureza) .And. TurVldNat(cNatureza, .T.)
											oModelG3R:LoadValue("G3R_NATURE", cNatureza)
										EndIf
									EndIf
								EndIf
					
								If !oModelG48B:IsEmpty()
									For nX := 1 to oModelG48B:Length()
										oModelG48B:GoLine(nX)
										oModelG48B:LoadValue("G48_RECCON", IIF(oModelG48B:GetValue("G48_COMSER") == "1", .T., .F.))
					
										If lExistFunc
											If Empty(oModelG48B:GetValue('G48_NATURE'))
												If oModelG48B:GetValue('G48_TPACD')  == '1'
													cTipo := '3'
												ElseIf oModelG48B:GetValue( 'G48_TPACD' ) == '2'
													cTipo := '4'
												EndIf
					
												G4W->(dbSetOrder(1))
												G4W->(dbSeek(xFilial("G4W") + oModelG48B:GetValue("G48_CODACD") + oModelG48B:GetValue("G48_CODREC")))
												If G4W->G4W_TPENT == "2" //Fornecedor de Reporte
													cFornecedor := oModelG3R:GetValue("G3R_FORREP")
													cLoja 		:= oModelG3R:GetValue("G3R_LOJREP")
					
												Else //Fornecedor de Produto
													cFornecedor	:= oModelG3R:GetValue("G3R_FORNEC")
													cLoja		:= oModelG3R:GetValue("G3R_LOJA"  )
					
												EndIf
					
												cNatureza := U_TURNAT(oModelG3R:GetValue('G3R_MSFIL') , ; 
												                      cTipo                           , ;
					                                                  oModelG48B:GetValue('G48_CLASS'), ; 
					                                                  oModelG3P:GetValue('G3P_SEGNEG'), ; 
					                                                  oModelG3R:GetValue('G3R_PROD')  , ; 
					                                                  '2'                             , ; 
					                                                  cFornecedor                     , ; 
					                                                  cLoja)
					
												If !Empty(cNatureza) .And. TurVldNat(cNatureza, .T.)
													oModelG48B:LoadValue('G48_NATURE', cNatureza)
												EndIf
											EndIf
										EndIf
									Next nX
								EndIf
								
								//Atualiza Demonstrativo Financeiro
								T34AtuDmFi(oModel)
					
								//Gera novos itens financeiros
								If !(lRet := Tur34ItFin(oModel, '3', .T., '3', , .T.))
									oModelG3R:SetValue('G3R_DIVERG', '1')
									TA042AcGrd(	oModel, .T. )
									SetAllDelete(.T.)	
									TA42DelSons( oModel, 'G3R_ITENS', .F. )
									SetAllDelete(.F.)
									TA042AcGrd(	oModel, .F. )
									Help( , , "TA042FillG3R", , I18N(STR0078, {oModelG3R:GetValue('G3R_NUMID'), oModelG3R:GetValue('G3R_IDITEM'), oModelG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele n�o ser� inserido na concilia��o."
								Else
									//Atualiza o grid de totalizadores e o cabe�alho
									CALCVLG8C(oModelG3R, .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "ADDLIN"/*cMomento*/)
								EndIf
							Else
								Help( , , "TA042FillG3R", , STR0059 + " (" + TurGetErrorMsg(oModel) + ") ", 1, 0)		// "N�o � poss�vel adicionar Documentos de Reserva com Fornecedores diferentes." 
							EndIf
					
							oModelT34:DeActivate()
						EndIf
						oModelT34:Destroy()
					EndIf
					G3P->(DbCloseArea())
				EndIf
			EndIf
		EndIf
		(cAliasG3R)->(DbSkip())
		cFilRV   := (cAliasG3R)->G3R_FILIAL
		cNumId   := (cAliasG3R)->G3R_NUMID
		cIdItem  := (cAliasG3R)->G3R_IDITEM
		lMark    := !Empty(AllTrim((cAliasG3R)->G3R_MARK))
	EndDo
	TA042AcGrd(oModel, .F.)
EndIf

If lMoedaDif
	Help( , , "TA042FillG3R", , STR0086, 1, 0)	// "N�o foi poss�vel incluir um ou mais Documentos de Reserva por estarem com a moeda diferente dos demais DRs desta concilia��o."
EndIf

G3R->(DbCloseArea())
RestArea(aArea)

cFilAnt := cBkpFil

TURXNIL(aArea)
TURXNIL(aCopyValues)

Return

/*/{Protheus.doc} TA042Concil

Fun��o para chamar a respectiva rotina, a efetiva��o e os demonstrativos

@type Static Function
@author Thiago Tavares
@since 10/05/2017
@version 12.1.14
/*/
Function TA042Concil(nOpc, cTipo)

Local cFunc := IIF( cTipo == '1', 'TURA042A', 'TURA042R' )

If FWExecView( ,'VIEWDEF.' + cFunc, nOpc, /*oDlg*/, {|| .T.}/*bCloseOk*/, /*bOk*/,/*nPercRed*/,/*aButtons*/, /*bCancel*/ ) == 0
	TuraEfetiv('1', cTipo)
EndIf

Return

/*/{Protheus.doc} TA042Dlin

Fun��o para ajustar valor na linha quando deletada 

@type Static Function
@author Thiago Tavares
@since 10/05/2017
@version 12.1.14
/*/
Function TA042Dlin(oView, cIdView, nNumLine)

Local oModel   := FwModelActive()
Local oMdlG3R  := oModel:GetModel('G3R_ITENS')
Local oMdlG48  := oModel:GetModel('G48B_ITENS')
Local nLinG3R  := oMdlG3R:GetLine()
Local nLinG48  := oMdlG48:GetLine()
Local aLines   := FWSaveRows()

If cIdView == 'G48B_ITENS'
	CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
	SetAplAcordFin(.T.)
	oMdlG48:Goline(nLinG48)
	
ElseIf cIdView == 'G3R_ITENS'
	CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
	oView:Refresh('VIEW_TOTDR')		
	oMdlG3R:Goline(nLinG3R)
Endif

oView:Refresh('VIEW_G3R')
FwRestRows( aLines )

TURXNIL(aLines)

Return

/*/{Protheus.doc} CalcTotDr
Realiza o recalculo dos campos G3R_TARIFA,G3R_TAXA,G3R_VLRIMP,G3R_EXTRAS com base nos itens das tabelas
@type function
@author Fernando Amorim(Cafu)
@since 08/05/2017
@version 12.1.7
/*/
 Function CalcTotDr( cMomento, cChaveTDR )
	Local aSaveLines := FwSaveRows()
 	Local oModel     := FwModelActive()
	Local oMdlG8C 	 := oModel:GetModel( 'G8C_MASTER' )
	Local oMdlG3R 	 := oModel:GetModel( 'G3R_ITENS' )
	Local oModelG48B := oModel:GetModel( 'G48B_ITENS')
	Local oModelTDR	 := oModel:GetModel("TOTDR")
	Local oView      := FWViewActive()
	Local aValues    := {}
	Local nVlrTarifa := 0
	Local nVlrTaxa   := 0
	Local nVlrExtra  := 0
	Local nVlrAcord  := 0
	Local nG4CVenda  := 0
	Local nX         := 0
	Local nSeekLin   := 0
	Local cFilRV	 := oMdlG3R:GetValue("G3R_FILIAL")  
	Local cNumId	 := oMdlG3R:GetValue("G3R_NUMID")  
	Local cIdItem	 := oMdlG3R:GetValue("G3R_IDITEM")
	Local cNumSeq	 := oMdlG3R:GetValue("G3R_NUMSEQ")
	Local cSeqACR	 := ''
	Local cTpSeg	 := oMdlG3R:GetValue("G3R_TPSEG")
	Local cStatus    := oMdlG3R:GetValue("G3R_STATUS")
	Local lAcerto    := oMdlG3R:GetValue("G3R_ACERTO") == '1'
	Local nLinSave	 := oMdlG3R:GetLine()
	Local lDeleted   := oMdlG3R:IsDeleted()
	Local nSignal 	 := IIF( oMdlG8C:GetValue('G8C_TIPO') == '1', 1, -1 )
	Local lRet       := .T.
			
	Default cChaveTDR := oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID") + oMdlG3R:GetValue("G3R_IDITEM")+oMdlG3R:GetValue("G3R_NUMSEQ")

	If oModel:GetOperation() != MODEL_OPERATION_DELETE .And. !(FWIsInCallStack("TA042AEFET") .Or. FWIsInCallStack("TA042ADEFE"))
		// apagar os valores do DR caso a chave seja encontrada 
		If oCacheCalc:Get(cChaveTDR)
			oCacheCalc:Del(cChaveTDR)
		EndIf
	
		If !lDeleted .And. oCacheCalc:Get(cChaveTDR, aValues)
			If Len(aValues) > 0
				aAdd(aValues, {"DTINI"   , aValues[1][2]})
				aAdd(aValues, {"DTFIM"   , aValues[2][2]})
				aAdd(aValues, {'TARIFAS' , aValues[3][2]})
				aAdd(aValues, {'TAXAS'	 , aValues[4][2]})
				aAdd(aValues, {'EXTRAS'	 , aValues[5][2]})
				aAdd(aValues, {'ACORDOS' , aValues[6][2]})	
				aAdd(aValues, {'STATUS'  , aValues[7][2]})
				aAdd(aValues, {'G4CVENDA', aValues[8][2]})
			EndIf
		Else 
			// Se a linha do DR estiver deletada, os campos de data dever�o ficar vazios
			If lDeleted .Or. lAcerto
				aAdd(aValues, {"DTINI"  ,  cTod('  /  /  ')	})
				aAdd(aValues, {"DTFIM"  ,  cTod('  /  /  ') })
				cStatus := ''	
			Else
				If cTpSeg == '2'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3U_ITENS'):GetValue('G3U_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3U_ITENS'):GetValue('G3U_DTFIM')})
						
				Elseif cTpSeg == '3'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3V_ITENS'):GetValue('G3V_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3V_ITENS'):GetValue('G3V_DTFIM')})
						
				ElseIf cTpSeg == '4'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3W_ITENS'):GetValue('G3W_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3W_ITENS'):GetValue('G3W_DTFIM')})
						
				ElseIf cTpSeg == '5'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3Y_ITENS'):GetValue('G3Y_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3Y_ITENS'):GetValue('G3Y_DTFIM')})
						
				ElseIf cTpSeg == '6'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3X_ITENS'):GetValue('G3X_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3X_ITENS'):GetValue('G3X_DTFIM')})
					
				ElseIf cTpSeg == '7'
					aAdd(aValues, {"DTINI", cTod('  /  /  ')})
					aAdd(aValues, {"DTFIM", cTod('  /  /  ')})
						
				ElseIf cTpSeg == '8'
					aAdd(aValues, {"DTINI", oModel:GetModel('G41_ITENS'):GetValue('G41_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G41_ITENS'):GetValue('G41_DTFIM')})
						
				ElseIf cTpSeg == '9'
					aAdd(aValues, {"DTINI", oModel:GetModel('G40_ITENS'):GetValue('G40_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G40_ITENS'):GetValue('G40_DTFIM')})
						
				ElseIf cTpSeg == 'A'
					aAdd(aValues, {"DTINI", oModel:GetModel('G3Z_ITENS'):GetValue('G3Z_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G3Z_ITENS'):GetValue('G3Z_DTFIM')})
						
				ElseIf cTpSeg == 'B'
					aAdd(aValues, {"DTINI", oModel:GetModel('G43_ITENS'):GetValue('G43_DTINI')})
					aAdd(aValues, {"DTFIM", oModel:GetModel('G43_ITENS'):GetValue('G43_DTFIM')})
						
				Endif
			EndIf
			
			// zerando as valores do DR no cache caso a linha esteja deletada ou o status do DR seja 2=particionado ou 4=cancelado
			If oMdlG3R:GetValue("G3R_STATUS") $ '2|4' .Or. lDeleted .Or. lAcerto
				nVlrTarifa := 0
				nVlrTaxa   := 0 
				nVlrExtra  := 0
				nVlrAcord  := 0
			Else
				//Acordos
				For nX := 1 to oModelG48B:Length()
					oModelG48B:GoLine(nX)
			
					If oModelG48B:GetValue("G48_STATUS" ) == "3" .Or. oModelG48B:IsDeleted()
						Loop
					EndIf
			
					If oModelG48B:GetValue("G48_RECCON") .And. !Empty(oModelG48B:GetValue("G48_CODACD"))
						If oModelG48B:GetValue("G48_TPACD") == '1' 
							nVlrAcord += oModelG48B:GetValue(  "G48_VLACD" )
						Else
							nVlrAcord -= oModelG48B:GetValue( "G48_VLACD" )
						EndIf
					EndIf
				Next nX
	
				nVlrTarifa	:= oMdlG3R:GetValue("G3R_TARIFA")
				nVlrTaxa	:= oMdlG3R:GetValue("G3R_TAXA")
				nVlrExtra	:= oMdlG3R:GetValue("G3R_EXTRAS")
				nG4CVenda   := TA042G4CVlr(oModel)
					
				// caso tenha acerto, � preciso somar os valores
				cSeqACR	:= Soma1(cNumSeq) 
				If oMdlG3R:SeekLine({{"G3R_FILIAL", cFilRV}, {"G3R_NUMID", cNumId}, {"G3R_IDITEM", cIdItem}, {"G3R_NUMSEQ", cSeqACR}})
					nSeekLin := oMdlG3R:GetLine()
					While oMdlG3R:GetValue("G3R_FILIAL") == cFilRV .AND. oMdlG3R:GetValue("G3R_NUMID") == cNumId .AND. oMdlG3R:GetValue("G3R_IDITEM") == cIdItem
						If !oMdlG3R:IsDeleted() .And. oMdlG3R:GetValue('G3R_ACERTO') == '1' .And. !oMdlG3R:GetValue('G3R_STATUS') $ '2|4' .And. oMdlG3R:GetValue("G3R_NUMSEQ") == cSeqACR
							//Acordos
							For nX := 1 to oModelG48B:Length()
								oModelG48B:GoLine(nX)
						
								If oModelG48B:GetValue("G48_STATUS" ) == "3" .Or. oModelG48B:IsDeleted()
									Loop
								EndIf
			
								If oModelG48B:GetValue("G48_RECCON") .And. !Empty(oModelG48B:GetValue("G48_CODACD"))
									If oModelG48B:GetValue("G48_TPACD") == '1' 
										nVlrAcord += oModelG48B:GetValue(  "G48_VLACD" )
									Else
										nVlrAcord -= oModelG48B:GetValue( "G48_VLACD" )
									EndIf
								EndIf
							Next nX

							nVlrTarifa	+= oMdlG3R:GetValue("G3R_TARIFA")
							nVlrTaxa	+= oMdlG3R:GetValue("G3R_TAXA")
							nVlrExtra	+= oMdlG3R:GetValue("G3R_EXTRAS")
							nG4CVenda   += TA042G4CVlr(oModel)
						EndIf		
						
						If ++nSeekLin > oMdlG3R:Length()
							Exit
						EndIf
						oMdlG3R:GoLine(nSeekLin)
						cSeqACR	:= oMdlG3R:GetValue("G3R_NUMSEQ")
					EndDo
				EndIf
			EndIf

			aAdd(aValues, {'TARIFAS' , (nVlrTarifa := nVlrTarifa * nSignal)})
			aAdd(aValues, {'TAXAS'	 , (nVlrTaxa   := nVlrTaxa   * nSignal)})
			aAdd(aValues, {'EXTRAS'	 , (nVlrExtra  := nVlrExtra  * nSignal)})
			aAdd(aValues, {'ACORDOS' , nVlrAcord  		   })	//Nos acordos n�o � invertido o sinal no reembolso pois � calculado sobre os tipos receita e abat. de receita
			aAdd(aValues, {'STATUS'  , cStatus			   })
			aAdd(aValues, {'G4CVENDA', nG4CVenda})
					
		EndIf

		oMdlG3R:Goline(nLinSave)
		For nX := 1 to Len(aValues) - 1
			oModelTDR:ForceValue(aValues[nX][1], aValues[nX][2])
		Next nX

		TRDChaceSet(cChaveTDR, aValues)
		
		If (cMomento == "CHANGE" .Or. cMomento == "ADDLIN" .Or. cMomento == "UPDLIN") .And. !FwIsInCallStack("TA042APESQ") .And. !FwIsInCallStack("TA042RPESQ")
			oView:Refresh( 'VIEW_TOTDR'	)		
			oView:Refresh( 'VIEW_G3R'	)
		EndIf
	EndIf
	
	FwRestRows(aSaveLines)
	
	TURXNIL(aSaveLines)
Return lRet

/*/{Protheus.doc} CALCVLG8C
Realiza o trtamento de quais linhas podem ser atualizadas
@type function
@author Fernando Amorim(Cafu)
@since 08/05/2017
@version 12.1.7
/*/
Function CALCVLG8C(oModelGrid, lRecalc, lAllLines, cChaveTDR, cMomento)

Local aValues 	 := {}
Local oModel	 := FwModelActive()
Local oMdlG8C 	 := oModel:GetModel('G8C_MASTER')
Local nSignal 	 := IIF(oMdlG8C:GetValue('G8C_TIPO') == '1', 1, -1)
Local nSomaAnt	 := 0
Local nSomaAtu	 := 0
Local nVlAcdAnt  := 0
Local nVlAcdAtu  := 0
Local nX, nY	 := 0

Default cChaveTDR := oModelGrid:GetValue("G3R_FILIAL") + oModelGrid:GetValue("G3R_NUMID") + oModelGrid:GetValue("G3R_IDITEM")+oModelGrid:GetValue("G3R_NUMSEQ")
Default lRecalc   := .F.
Default lAllLines := .F.

If oModel:GetOperation() != MODEL_OPERATION_DELETE .And. !(FWIsInCallStack("TA042AEFET") .Or. ; 
                                                           FWIsInCallStack("TA042ADEFE") .Or. ;
                                                           FWIsInCallStack("TA042GERAC"))

	If !lRecalc .And. !lAllLines
		If !FwIsInCallStack('TA042ChLin')		
			// qdo for edi�ao e visualizacao apenas carrega o primeiro DR no array do cache
			oModelGrid:GoLine(1) 
		EndIf
		CalcTotDr(cMomento)
	
	ElseIf lRecalc .And. lAllLines
		// nesse trecho recalcula todos os DRs
		oMdlG8C:LoadValue("G8C_VLRBRU", 0)
		oMdlG8C:LoadValue("G8C_VLRACD", 0)
		
		For nY := 1 To oModelGrid:Length()
			oModelGrid:GoLine(nY) 
			
			If oModelGrid:IsDeleted() .Or. Empty(oModelGrid:GetValue('G3R_NUMID'))
				Loop
			EndIf
			
			cChaveTDR := oModelGrid:GetValue("G3R_FILIAL") + oModelGrid:GetValue("G3R_NUMID") + oModelGrid:GetValue("G3R_IDITEM")+oModelGrid:GetValue("G3R_NUMSEQ")
			oCacheCalc:Del(cChaveTDR)
			CalcTotDr(cMomento, cChaveTDR)
			
			If oCacheCalc:Get(cChaveTDR, aValues) .And. oModelGrid:GetValue("G3R_STATUS") == '1'
				If Len(aValues) > 4
					nSomaAtu  += aValues[aScan(aValues, {|x| x[1] == 'G4CVENDA'})][2]
					nVlAcdAtu += aValues[aScan(aValues, {|x| x[1] == 'ACORDOS' })][2]
				Endif
			EndIf					
		Next nY

		oMdlG8C:LoadValue("G8C_VLRBRU", (oMdlG8C:GETValue("G8C_VLRBRU") + nSomaAtu))
		oMdlG8C:LoadValue("G8C_VLRACD", (oMdlG8C:GETValue("G8C_VLRACD") + nVlAcdAtu))
		oMdlG8C:LoadValue("G8C_VLRFAT", (oMdlG8C:GETValue("G8C_VLRBRU") - (oMdlG8C:GETValue("G8C_VLRACD") * nSignal)))
	
	ElseIf lRecalc .And. !lAllLines
		// nesse trecho recalcula apenas o DR posicionado e suas respectivas sequencias
		// esse trecho ser� chamado qdo for criado NoShow/Parcialidade, realizado Atualiza DR ou adicionado/deletado DR  
		
		If oModelGrid:GetValue("G3R_STATUS") $ '1|2' .And. (lAltTarifa .Or. lAltTaxas .Or. lAltExtras .Or. lAplAcordFin .Or. lRecalc)
			If oCacheCalc:Get(cChaveTDR, aValues)
				If Len(aValues) > 4
					nSomaAnt  := aValues[aScan(aValues, {|x| x[1] == 'G4CVENDA'})][2]
					nVlAcdAnt := aValues[aScan(aValues, {|x| x[1] == 'ACORDOS' })][2]
				Endif
			Endif
	
			oCacheCalc:Del(cChaveTDR)
			CalcTotDr(cMomento, cChaveTDR)
	
			If oCacheCalc:Get(cChaveTDR, aValues)
				If Len(aValues) > 4
					nSomaAtu  := aValues[aScan(aValues, {|x| x[1] == 'G4CVENDA'})][2]
					nVlAcdAtu := aValues[aScan(aValues, {|x| x[1] == 'ACORDOS' })][2]
				Endif 
			Endif
			If nSomaAnt <> nSomaAtu
				oMdlG8C:LoadValue("G8C_VLRBRU",(oMdlG8C:GETValue("G8C_VLRBRU") + (nSomaAtu - nSomaAnt)))
			Endif

			If nVlAcdAnt <> nVlAcdAtu
				oMdlG8C:LoadValue("G8C_VLRACD",(oMdlG8C:GETValue("G8C_VLRACD") + (nVlAcdAtu - nVlAcdAnt)))
			Endif

			oMdlG8C:LoadValue("G8C_VLRFAT",(oMdlG8C:GETValue("G8C_VLRBRU") - (oMdlG8C:GETValue("G8C_VLRACD") * nSignal)))
		Endif
	EndIf
EndIf

Return .T.

Function TRDChaceNew()
	oCacheCalc := tHashMap():New()
Return 

Function TRDChaceSet(cChave, aValues)
	oCacheCalc:Set(cChave, aValues)
Return 

Function TRDChaceGet(cChave)

Local aValues := {}
oCacheCalc:Get(cChave, aValues)

Return aValues

Function TDRCacheClean()
	oCacheCalc:Clean()
Return

Function TDRCacheDel(cChave)
	oCacheCalc:Del(cChave)
Return

Function SetAltTarifa(lValor)
	lAltTarifa := lValor
Return

Function GetAltTarifa()
Return lAltTarifa

Function SetAltTaxas(lValor)
	lAltTaxas := lValor
Return

Function GetAltTaxas()
Return lAltTaxas

Function SetAltExtras(lValor)
	lAltExtras := lValor
Return

Function GetAltExtras()
Return lAltExtras

Function SetAltImpostos(lValor)
	lAltImpostos := lValor
Return

Function GetAltImpostos()
Return lAltImpostos

Function SetAplAcordFin(lValor)
	lAplAcordFin := lValor
Return

Function GetAplAcordFin()
Return lAplAcordFin

Function SetAltSeg(lValor)
	lAltSeg := lValor
Return

Function GetAltSeg()
Return lAltSeg

Function SetAllDelete(lValor)
	lAllDelete := lValor
Return

Function GetAllDelete()
Return lAllDelete

/*/{Protheus.doc} TA042G4CVlr
Retorna a somat�ria dos IFs do DR posicionado
@type function
@author Thiago Tavares
@since 15/08/2017
@version 12.1.7
/*/
Static Function TA042G4CVlr(oModel)

Local oMdlG4C  := oModel:GetModel('G4CB_ITENS')
Local cFornPRD := oModel:GetValue('G3R_ITENS', 'G3R_FORNEC')
Local cLojaPRD := oModel:GetValue('G3R_ITENS', 'G3R_LOJA')
Local cFornRPT := oModel:GetValue('G3R_ITENS', 'G3R_FORREP')
Local cLojaRPT := oModel:GetValue('G3R_ITENS', 'G3R_LOJREP')
Local nValor   := 0
Local nX       := 0

For nX := 1 To oMdlG4C:Length()
	oMdlG4C:GoLine(nX)
	If !oMdlG4C:IsDeleted()
		If (oMdlG4C:GetValue('G4C_CODIGO') + oMdlG4C:GetValue('G4C_LOJA') == cFornPRD + cLojaPRD .Or. ;
		    oMdlG4C:GetValue('G4C_CODIGO') + oMdlG4C:GetValue('G4C_LOJA') == cFornRPT + cLojaRPT) .And. ;
		    oMdlG4C:GetValue('G4C_STATUS') $ '1|3' .And. Empty(oMdlG4C:GetValue('G4C_APLICA'))  
			nValor := oMdlG4C:GetValue('G4C_VALOR')
		EndIf
	EndIf
Next nX

Return nValor

/*/{Protheus.doc} TA42VldCPg
Valida��o do tipo da condi��o de pagamento
@type function
@author Anderson Toledo
@since 22/08/2017
@version 12.1.7
/*/

Function TA42VldCPg( cCondPgto )
	Local lRet := .T.
	
	DEFAULT cCondPgto := &( ReadVar() )
	
	SE4->( dbSetOrder(1) )
	If SE4->(DbSeek(xFilial('SE4') + cCondPgto))
		If SE4->E4_TIPO <> Alltrim(GetMv('MV_TURTPCD', , 'C')) 		//Par�metro do Tipo de Condi��o de Pagamento
			lRet := .F.
			TurHelp( i18N(STR0073,{cCondPgto, SE4->E4_TIPO, Alltrim(GetMv('MV_TURTPCD', , 'C'))}),; //"Condi��o de pagamento '#1[Condi��o pagamento]#' � do tipo '#2[Tipo condi��o]#' e o valor esperado � '#3[Tipo condi��o]#'."  
					 STR0074,; //"Selecione outra condi��o de pagamento ou ajuste o par�mentro 'MV_TURTPCD'."
					 "TA042VldCondPgto" )
		EndIf
		
	EndIf
		
Return lRet  

/*/{Protheus.doc} TA42VldCPg
Valida��o do tipo da condi��o de pagamento
@type function
@author Anderson Toledo
@since 16/11/2017
@version 12.1.7
/*/

Function TA42VlCPNF( cCondPgto, cOrig )
	Local lRet := .T.
	
	DEFAULT cOrig	:= ''

	If Empty( cCondPgto )
		lRet := .F.
		TurHelp( i18N(STR0075,{cOrig}),; //"Condi��o de pagamento n�o informada no par�metro '#1[ Par�metro ]#'."   
				 i18N(STR0076,{cOrig}),; //"Solicite ao administrador do sistema a revis�o do par�metro '#1[ Par�metro ]#'."
				 "TA042VldCPNF1" )		
	Else
		SE4->( dbSetOrder(1) )
		If !( SE4->(DbSeek(xFilial('SE4') + cCondPgto) ) )
			lRet := .F.
			TurHelp( i18N(STR0077,{cCondPgto, cOrig}),; //"Condi��o de pagamento '#1[Condi��o pagamento]#' informada no par�metro '#2[ Par�metro ]#' n�o existe."   
					 i18N(STR0076,{cOrig}),; //"Solicite ao administrador do sistema a revis�o do par�metro '#1[ Par�metro ]#'."
					 "TA042VldCPNF2" )		
		EndIf
	
	EndIf

Return lRet

/*/{Protheus.doc} TA042TemAc()
Retorna se o DR encontrado tem Acerto

@type function
@author Thiago Tavares
@since 25/09/2017
@version 12.1.7
/*/
Static Function TA042TemAc(cAliasG3R)

Local lRet := .F.

While (cAliasG3R)->(!Eof())
	If (cAliasG3R)->G3R_ACERTO == '1'
		lRet := .T.
		Exit
	EndIf
	(cAliasG3R)->(DbSkip())
EndDo

(cAliasG3R)->(DbGoTop())

Return lRet

/*/{Protheus.doc} TA042Coninu

Fun��o para inutiliza��o de documentos de reserva e submodelos "clonados"

@type 		function
@author 	Thiago Tavares
@param		oMdlG8C - objeto, modelo de dados da concilia��o
@since 		31/01/2018
@version 	1.0
/*/
Function TA042Coninu(oMdlG8C)

Local oModelT34 := Nil
Local oMdlG3R   := Nil
Local nX        := 0
Local lRet      := .T.
Local lInclui   := .F.
Local lAltera   := .F.
Local lExclui   := .F.
Local lCOntinua := .F.
Local cSRCInu   := ""	// vari�vel utilizada para informar o valor que ser� pesquisado em G3R_CONINU
Local cG3RInu   := ""	// vari�vel utilizada para informar o valor que ser� gravado em G3R_CONINU
Local cTmpAlias := ""

Default oMdlG8C	:= FwModelActive()

oMdlG3R := oMdlG8C:GetModel("G3R_ITENS")
lInclui := oMdlG8C:GetOperation() == 3
lAltera := oMdlG8C:GetOperation() == 4
lExclui := oMdlG8C:GetOperation() == 5

DbSelectArea("G3P")
G3P->(DbSetOrder(1))	// G3P_FILIAL+G3P_NUMID+G3P_SEGNEG

For nX := 1 To oMdlG3R:Length()
	oMdlG3R:GoLine(nX)
	lContinua := .F.

	If G3P->(DbSeek(oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID")))
		If SoftLock("G3P")
			oModelT34 := FwLoadModel("TURA042J")
			oModelT34:SetOperation( MODEL_OPERATION_UPDATE )
			oModelT34:GetModel('G3R_ITENS'):SetLoadFilter({{"G3R_FILIAL", "'" + oMdlG3R:GetValue("G3R_FILIAL") + "'", MVC_LOADFILTER_EQUAL}, ;
			                                               {"G3R_NUMID" , "'" + oMdlG3R:GetValue("G3R_NUMID")  + "'", MVC_LOADFILTER_EQUAL}, ;
			                                               {"G3R_IDITEM", "'" + oMdlG3R:GetValue("G3R_IDITEM") + "'", MVC_LOADFILTER_EQUAL}, ;
			                                               {"G3R_NUMSEQ", "'" + oMdlG3R:GetValue("G3R_NUMSEQ") + "'", MVC_LOADFILTER_EQUAL}})
			
			If oModelT34:Activate()
				// validando se � vazio no caso das sequencias geradas pelo NoShow/Parcialidade que n�o precisam ser inutilizadas/liberadas 
				If !oModelT34:GetModel("G3R_ITENS"):IsEmpty()
					
					// inutilizando o DR -> PREENCHENDO O CONINU
					// se a linha no modelo da concilia��o n�o estiver deleteda e for inclus�o ou alteracao e o DR estiver sendo incluido na conciliacao
					// no modelo do RV procurar pelo DR com G3R_CONINU vazio e gravar o c�digo da conciliacao
					If (lInclui .Or. (lAltera .And. Empty(oModelT34:GetValue("G3R_ITENS", "G3R_CONINU")))) .And. !oMdlG3R:IsDeleted()
						lContinua := .T.
						cSRCInu   := Space(TamSX3("G3R_CONINU")[1])
						cG3RInu   := oMdlG8C:GetValue("G8C_MASTER", "G8C_CONCIL")
						
					// liberando o DR -> LIMPANDO O CONINU
					// se for exclusao ou for alteracao e o DR estiver sendo excluido da conciliacao
					// no modelo do RV procurar pelo DR com G3R_CONINU igual ao da conciliacao e limpar o c�digo da conciliacao
					ElseIf lExclui .Or. (lAltera .And. !Empty(oModelT34:GetValue("G3R_ITENS", "G3R_CONINU")) .And. oMdlG3R:IsDeleted())	
						lContinua := .T.
						cSRCInu   := oMdlG8C:GetValue("G8C_MASTER", "G8C_CONCIL")
						
						If lExclui .Or. (lAltera .And. oMdlG3R:IsDeleted())
							cG3RInu := Space(TamSX3("G3R_CONINU")[1])
						ElseIf lAltera .And. !Empty(oModelT34:GetValue("G3R_ITENS", "G3R_CONINU"))
							cG3RInu := oMdlG8C:GetValue("G8C_MASTER", "G8C_CONCIL")
						EndIf
					EndIf
	
					If lContinua
						// rotina que grava as informa��es nos submodelos do RV
						TA042SetInu("G3P_FIELDS", {{"_CONINU", cG3RInu}}, , .F., cSRCInu)
						If (lRet := oModelT34:VldData())
							If (lRet := oModelT34:CommitData())
	
								// DR inutilizado por uma concilia��o e, depois, um acordo G48 � apurado
								// Caso a concilia��o seja excluida ou o DR seja deletado da concilia��o 
								// � preciso restaurar o registro original, do contr�rio, o registro ficaria no "limbo" 
								If lExclui .Or. (lAltera .And. oMdlG3R:IsDeleted())
									cTmpAlias := GetNextAlias()
							
									BeginSql Alias cTmpAlias
										SELECT G48_FILIAL, G48_NUMID, G48_IDITEM, G48_NUMSEQ, G48_APLICA, G48_CLIFOR, G48_CODACD, G48_CONORI
										FROM %Table:G48% G48
										INNER JOIN %Table:G6L% G6L ON G6L_FILIAL = G48_FILAPU AND G6L_CODAPU = G48_CODAPU AND G6L.%NotDel%
										WHERE G48_FILIAL =  %Exp:oModelT34:GetValue("G3R_ITENS", "G3R_FILIAL")% AND
											  G48_NUMID	 =  %Exp:oModelT34:GetValue("G3R_ITENS", "G3R_NUMID" )% AND 
											  G48_IDITEM =  %Exp:oModelT34:GetValue("G3R_ITENS", "G3R_IDITEM")% AND 
											  G48_CODAPU <> %Exp:Space(TamSX3("G48_CODAPU")[1])% AND 
											  G48_CONORI =  %Exp:oMdlG8C:GetValue("G8C_MASTER", "G8C_CONCIL")% AND
											  G48.%NotDel%
									EndSql
							
									G48->(DbSetOrder(1))	// G48_FILIAL+G48_NUMID+G48_IDITEM+G48_NUMSEQ+G48_APLICA+G48_CODACD
									G4C->(DbSetOrder(1))	// G4C_FILIAL+G4C_NUMID+G4C_IDITEM+G4C_NUMSEQ+G4C_APLICA+G4C_NUMACD
									While lRet .And. (cTmpAlias)->(!EOF())
										If G48->(DbSeek((cTmpAlias)->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD)))
											While G48->(!EOF()) .And. G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD) == (cTmpAlias)->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD)
												If G48->G48_CONORI != (cTmpAlias)->G48_CONORI
													If G4C->(DbSeek(G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD)))
														While G4C->(!EOF()) .And. G4C->(G4C_FILIAL + G4C_NUMID + G4C_IDITEM + G4C_NUMSEQ + G4C_APLICA + G4C_NUMACD) == G48->(G48_FILIAL + G48_NUMID + G48_IDITEM + G48_NUMSEQ + G48_APLICA + G48_CODACD)
															If G4C->G4C_CONORI != (cTmpAlias)->G48_CONORI
																If (lRet := RecLock('G4C', .F.))
																	G4C->(DbDelete())
																	G4C->(MsUnlock())
																EndIf
															Else
																If (lRet := RecLock('G4C',.F.))
																	G4C->G4C_CONORI := ""
																	G4C->(MsUnlock())
																EndIf
															EndIf
															G4C->(DbSkip())
														EndDo
							
														If lRet .And. (lRet := RecLock('G48',.F.))
															G48->(DbDelete())
															G48->(MsUnlock())
														EndIf
													EndIf
												Else
													If (lRet := RecLock('G48',.F.))
														G48->G48_CONORI := ""
														G48->(MsUnlock())
													EndIf
												EndIf
												G48->(DbSkip())
											EndDo
										EndIf
										(cTmpAlias)->(DbSkip())
									EndDo
									(cTmpAlias)->(DbCloseArea())
								EndIf
							Else
								lRet := .F. 
								Exit	
							EndIf
						Else
							lRet := .F.
							Exit
						EndIf
					EndIf
				EndIf
				oModelT34:DeActivate()
				oModelT34:Destroy()
			Else
				lRet := .F.
				Exit
			EndIf
		EndIf
	EndIf
Next nX

If ValType(oModelT34) == "O" .And. oModelT34:IsActive()
	oModelT34:DeActivate()
	oModelT34:Destroy()
EndIf

G3P->(DbCloseArea())

FwModelActive( oMdlG8C )

Return lRet

/*/{Protheus.doc} TA042SetInu
Fun��o para alterar todas as ocorrencias do campo _CONINU de forma recursiva

@type 		function
@author 	Thiago Tavares
@since 		16/02/2018
@version 	1.0
@param 		cIdModel, character, Id do modelo de onde ser� realizada a recurs�o em seus submodelos
@param 		aLoadValues, array, vetor com informa��es para o loadvalue
@param 		aSetValues, array, vetor com informa��es para o setvalue
@param 		lAllLines, booleano, Se for grid, indica que dever� ser percorrida todas as linhas
/*/
Function TA042SetInu(cIdModel, aLoadValues, aSetValues, lAllLines, cConcil)
	
Local aDependency 	:= {}
Local cTable		:= ""
Local oModel		:= FwModelActive()
Local oModelAux		:= oModel:GetModel(cIdModel)
Local nX, nY		:= 0

DEFAULT aLoadValues	:= {}
DEFAULT aSetValues	:= {}
DEFAULT lAllLines	:= .F.
DEFAULT cConcil     := ""

aDependency := oModel:GetDependency( cIdModel )
cTable		:= oModelAux:GetStruct():GetTable()[1]

If lAllLines .And. oModelAux:ClassName() == "FWFORMGRID"
	For nX := 1 to oModelAux:Length()
		oModelAux:GoLine(nX)

		If cIdModel != "G3R_ITENS" .Or. (cIdModel == "G3R_ITENS" .And. oModelAux:GetValue("G3R_CONINU") == cConcil)
			For nY := 1 To Len(aSetValues)
				If oModelAux:HasField(cTable + aSetValues[nY][1])
					If (oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty()) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
						oModelAux:SetValue(cTable + aLoadValues[nY][1], aSetValues[nY][2])
					EndIf
				EndIf
			Next
	
			For nY := 1 To Len(aLoadValues)
				If oModelAux:HasField(cTable + aLoadValues[nY][1])
					If (oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty()) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
						oModelAux:LoadValue(cTable + aLoadValues[nY][1], aLoadValues[nY][2])
					EndIf
				EndIf
			Next
	
			For nY := 1 To Len(aDependency)
				TA042SetInu(aDependency[nY][2], aLoadValues, aSetValues, .T., cConcil)
			Next
		EndIF
	Next
Else
	If cIdModel != "G3R_ITENS" .Or. (cIdModel == "G3R_ITENS" .And. oModelAux:GetValue("G3R_CONINU") == cConcil)
		For nY := 1 To Len(aSetValues)
			If oModelAux:HasField(cTable + aSetValues[nY][1])
				If (oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty()) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
					oModelAux:SetValue(cTable + aLoadValues[nY][1], aSetValues[nY][2])
				EndIf
			EndIf
		Next
	
		For nY := 1 To Len(aLoadValues)
			If oModelAux:HasField(cTable + aLoadValues[nY][1])
				If (oModelAux:ClassName() == "FWFORMGRID" .And. !oModelAux:IsEmpty()) .Or. oModelAux:ClassName() == "FWFORMFIELDS"
					oModelAux:LoadValue(cTable + aLoadValues[nY][1], aLoadValues[nY][2])
				EndIf
			EndIf
		Next
	
		For nY := 1 To Len(aDependency)
			TA042SetInu(aDependency[nY][2], aLoadValues, aSetValues, .T., cConcil)
		Next
	EndIf
EndIf

Return .T.