#INCLUDE "TURA042A.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch" 

Static oTA042Model

Static lInputExtras	 // Indica que mesmo com G3Q_INFFAT diferente de 2 ou 3 o usu�rio concordou em lan�ar extras
Static lShowObsIV	 // Apresenta a observa��o do IV ao incluir um extra
Static aCmpAtuIf	 := T34CmpAtIf()
Static oMdlAtivo     := Nil

/*/{Protheus.doc} TURA042A
Concilia��o Outros (Terrestre)
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Function TURA042A()
	Local oBrowse	:= nil
	
	SetKey (VK_F12,{|a,b| AcessaPerg("TURA042A",.T.)})

	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias("G8C")
	oBrowse:SetMenuDef( "TURA042A" )

	oBrowse:SetDescription(STR0001) //"Concilia��o Outros (Terrestre)"

	oBrowse:AddLegend( "G8C_STATUS == '1'", "GREEN" , STR0106 )	//Efetivada
	oBrowse:AddLegend( "G8C_STATUS == '2'", "RED" 	, STR0107 )	//N�o Efetivada
	oBrowse:AddLegend( "G8C_STATUS == '3'", "YELLOW", STR0178 ) //"Erro efetiva��o"	

	oBrowse:SetFilterDefault( 'G8C->G8C_TIPO == "1"' )

	oBrowse:Activate()

	Set Key VK_F12 To

Return nil

/*/{Protheus.doc} ModelDef
Modelo de dados referete a concilia��o outros (terrestre)
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function ModelDef(oModel)
	Local oStruG4B	:= FwFormStruct( 1, "G4B",,.F. ) //Informa��es Adicionais
	Local oStruG3T	:= FwFormStruct( 1, "G3T",,.F. ) //1 - A�reo(G3T)
	Local oStruG3U	:= FwFormStruct( 1, "G3U",,.F. ) //2 - Hotel(G3U)
	Local oStruG3V	:= FwFormStruct( 1, "G3V",,.F. ) //3 - Carro (G3V)
	Local oStruG3W	:= FwFormStruct( 1, "G3W",,.F. ) //4 - Rodovi�rio (G3W)
	Local oStruG3Y	:= FwFormStruct( 1, "G3Y",,.F. ) //5 - Cruzeiro (G3Y)
	Local oStruG3X	:= FwFormStruct( 1, "G3X",,.F. ) //6 - Trem (G3X)
	Local oStruG42	:= FwFormStruct( 1, "G42",,.F. ) //7 - Visto (G42)
	Local oStruG41	:= FwFormStruct( 1, "G41",,.F. ) //8 - Seguro (G41)
	Local oStruG40	:= FwFormStruct( 1, "G40",,.F. ) //9 - Tour (G40)
	Local oStruG3Z	:= FwFormStruct( 1, "G3Z",,.F. ) //10 - Pacote (G3Z)
	Local oStruG43	:= FwFormStruct( 1, "G43",,.F. ) //11 - Outros (G43)
	Local oStruG8C 	:= Nil	//Cabe�alho Conc. Fatura Terrestre
	Local oStruG3R	:= Nil 	//Documento de reserva
	Local oStruG46	:= Nil 	//Taxas
	Local oStruG47	:= Nil 	//Extras
	Local oStruG49	:= Nil 	//Impostos
	Local oStruG4C  := Nil 	//Itens Financeiros Fornecedor
	Local oStruG48A := Nil 	//Acordos de Cliente

	Default oModel	:= FwLoadModel( "TURA042" )

	oStruG8C  := oModel:GetModel("G8C_MASTER"):GetStruct()
	oStruG3R  := oModel:GetModel("G3R_ITENS" ):GetStruct() 
	oStruG46  := oModel:GetModel("G46_ITENS" ):GetStruct() 
	oStruG47  := oModel:GetModel("G47_ITENS" ):GetStruct() 
	oStruG49  := oModel:GetModel("G49_ITENS" ):GetStruct() 
	oStruG4C  := oModel:GetModel("G4CB_ITENS"):GetStruct() 
	oStruG48A := oModel:GetModel("G48A_ITENS"):GetStruct() 

	oStruG3R:SetProperty( "G3R_STATUS", MODEL_FIELD_INIT , {|| "" })  		//Status iniciado como branco, para n�o permitir alterar os valores dos modelos dependentes
	oStruG3R:SetProperty( "G3R_TES"	  , MODEL_FIELD_WHEN , {|| .T.})  		//TES para nota fiscal de entrada
	oStruG8C:SetProperty( "G8C_TIPO"  , MODEL_FIELD_INIT , {|| "1"})		//Tipo venda
	oStruG46:SetProperty( "G46_CONMAN", MODEL_FIELD_INIT , {|| "1"}) 		//Todos as taxas incluidas ser�o marcadas como inseridas manualmente
	oStruG47:SetProperty( "G47_CONMAN", MODEL_FIELD_INIT , {|| "1"}) 		//Todos os extras incluidos ser�o marcadas como inseridas manualmente
	oStruG49:SetProperty( "G49_CONMAN", MODEL_FIELD_INIT , {|| "1"})  		//Todos os impostos incluidos ser�o marcadas como inseridas manualmente
	oStruG4C:SetProperty( "G4C_STATUS", MODEL_FIELD_NOUPD, .F.     )  		
	oStruG48A:SetProperty("G48_CANCON", MODEL_FIELD_NOUPD, .F.     )  		
	oStruG48A:SetProperty("G48_RECCON", MODEL_FIELD_NOUPD, .F.     )  		
	
	oModel:SetPost( {|oModel| TA042ATOK(oModel) } )
	oModel:SetCommit( {|oModel|  TA042ACommit(oModel) } )

	//Documentos de reserva
	oModel:GetModel( "G3R_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Itens de venda
	oModel:GetModel( "G3Q_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Cliente (Pastas Abaixo)
	oModel:GetModel( "G48A_ITENS" ):SetLPre(  { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Itens Financeiros (Pastas Abaixo)
	oModel:GetModel( "G4CA_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Passageiros da reserva
	oModel:GetModel( "G3S_ITENS" ):SetLPre(  { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Dados Adicionais
	oModel:GetModel( "G4B_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//1 - A�reo(G3T)
	oModel:GetModel( "G3T_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//2 - Hotel(G3U)
	oModel:GetModel( "G3U_ITENS" ):SetLPre(  { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//3 - Carro (G3V)
	oModel:GetModel( "G3V_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//4 - Rodovi�rio (G3W)
	oModel:GetModel( "G3W_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//5 - Cruzeiro (G3Y)
	oModel:GetModel( "G3Y_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//6 - Trem (G3X)
	oModel:GetModel( "G3X_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//7 - Visto (G42)
	oModel:GetModel( "G42_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//8 - Seguro (G41)
	oModel:GetModel( "G41_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//9 - Tour (G40)
	oModel:GetModel( "G40_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//10 - Pacote (G3Z)
	oModel:GetModel( "G3Z_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//11 - Outros (G43)
	oModel:GetModel( "G43_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Tarifas
	oModel:GetModel( "G44_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Taxas
	oModel:GetModel( "G46_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Extras
	oModel:GetModel( "G47_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Acordos Fornecedor
	oModel:GetModel( "G48B_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Impostos
	oModel:GetModel( "G49_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Rateio
	oModel:GetModel( "G4A_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Demonstrativo - Cliente
	oModel:GetModel( "G9KA_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	//Demonstrativo - Fornecedor
	oModel:GetModel( "G9KB_ITENS" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042LPre(oModelGrid, nLine, cAction, cField) } )

	oModel:SetVldActivate( { |oModel| TA042AVlAct( oModel ) } )
	oModel:SetActivate( {||  TA042Init( oModel ) } )

Return oModel

/*/{Protheus.doc} ViewDef
Vis�o referete a concilia��o outros (terrestre)
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ViewDef()
	Local oView		:= FwLoadView( "TURA042" )
	Local oModel   	:= oView:GetModel()
	Local oStruG4B	:= FwFormStruct( 2, "G4B" ) //Informa��es Adicionais
	Local oStruG3T	:= FwFormStruct( 2, "G3T" ) //1 - A�reo(G3T)
	Local oStruG3U	:= FwFormStruct( 2, "G3U" ) //2 - Hotel(G3S)
	Local oStruG3V	:= FwFormStruct( 2, "G3V" ) //3 - Carro (G3V)
	Local oStruG3W	:= FwFormStruct( 2, "G3W" ) //4 - Rodovi�rio (G3W)
	Local oStruG3Y	:= FwFormStruct( 2, "G3Y" ) //5 - Cruzeiro (G3Y)
	Local oStruG3X	:= FwFormStruct( 2, "G3X" ) //6 - Trem (G3X)
	Local oStruG42	:= FwFormStruct( 2, "G42" ) //7 - Visto (G42)
	Local oStruG41	:= FwFormStruct( 2, "G41" ) //8 - Seguro (G41)
	Local oStruG40	:= FwFormStruct( 2, "G40" ) //9 - Tour (G40)
	Local oStruG3Z	:= FwFormStruct( 2, "G3Z" ) //10 - Pacote (G3Z)
	Local oStruG43	:= FwFormStruct( 2, "G43" ) //11 - Outros (G43)
	Local oStruG44	:= FwFormStruct( 2, "G44" ) //Tarifas
	Local oStruG47	:= FwFormStruct( 2, "G47" )	//Extras

	TA034StruV( /*oStruG3Q*/, /*oStruG4CA*/, /*oStruG4CB*/, oStruG3T, oStruG3U, oStruG3V, oStruG3W, oStruG3Y, /*oStruG3Z*/,oStruG40, oStruG41, oStruG42, oStruG43, /*oStruG48A*/, /*oStruG48B*/, oStruG4B, oStruG3X )

	oStruG44:SetProperty( "*"		  , MVC_VIEW_CANCHANGE, .F.)
	oStruG44:SetProperty( "G44_TARBAS", MVC_VIEW_CANCHANGE, .T.)
	oStruG44:SetProperty( "G44_EARLY" , MVC_VIEW_CANCHANGE, .T.)
	oStruG44:SetProperty( "G44_LATE"  , MVC_VIEW_CANCHANGE, .T.)
	oStruG44:SetProperty( "G44_PLALIM", MVC_VIEW_CANCHANGE, .T.)

	ModelDef(oModel)

	oView:AddGrid( "VIEW_G4B", oStruG4B, "G4B_ITENS" )
	oView:AddGrid( "VIEW_G3T", oStruG3T, "G3T_ITENS" )
	oView:AddGrid( "VIEW_G3U", oStruG3U, "G3U_ITENS" )
	oView:AddGrid( "VIEW_G3V", oStruG3V, "G3V_ITENS" )
	oView:AddGrid( "VIEW_G3W", oStruG3W, "G3W_ITENS" )
	oView:AddGrid( "VIEW_G3Y", oStruG3Y, "G3Y_ITENS" )
	oView:AddGrid( "VIEW_G3X", oStruG3X, "G3X_ITENS" )
	oView:AddGrid( "VIEW_G42", oStruG42, "G42_ITENS" )
	oView:AddGrid( "VIEW_G41", oStruG41, "G41_ITENS" )
	oView:AddGrid( "VIEW_G40", oStruG40, "G40_ITENS" )
	oView:AddGrid( "VIEW_G3Z", oStruG3Z, "G3Z_ITENS" )
	oView:AddGrid( "VIEW_G43", oStruG43, "G43_ITENS" )
	oView:AddGrid( "VIEW_G44", oStruG44, "G44_ITENS" )
	oView:AddGrid( "VIEW_G47", oStruG47, "G47_ITENS" )

	oView:AddIncrementField("G47_ITENS","G47_SEQ")		//Extras

	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G44", STR0019	) //"Tarifas"
	oView:AddSheet( "FOLDER_IT_DR", "FOLDER_IT_DR_G47", STR0021	) //"Extras"

	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3T", STR0027 ) //"A�reo"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3U", STR0028	) //"Hotel"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3V", STR0029	) //"Carro"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3W", STR0030	) //"Rodovi�rio"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3Y", STR0031	) //"Cruzeiro"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3X", STR0032	) //"Trem"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G42", STR0033	) //"Visto"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G41", STR0034	) //"Seguro"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G40", STR0035	) //"Tour"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G3Z", STR0036	) //"Pacote"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G43", STR0037	) //"Outros"
	oView:AddSheet( "FOLDER_SEG", "FOLDER_SEG_G4B", STR0038	) //"Informa��es Adicionais"

	oView:CreateHorizontalBox( "BOX_G44", 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G44" )
	oView:CreateHorizontalBox( "BOX_G47", 100, , , "FOLDER_IT_DR", "FOLDER_IT_DR_G47" )
	oView:CreateHorizontalBox( "BOX_G3T", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3T"   )
	oView:CreateHorizontalBox( "BOX_G3U", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3U"   )
	oView:CreateHorizontalBox( "BOX_G3V", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3V"   )
	oView:CreateHorizontalBox( "BOX_G3W", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3W"   )
	oView:CreateHorizontalBox( "BOX_G3Y", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3Y"   )
	oView:CreateHorizontalBox( "BOX_G3X", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3X"   )
	oView:CreateHorizontalBox( "BOX_G42", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G42"   )
	oView:CreateHorizontalBox( "BOX_G41", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G41"   )
	oView:CreateHorizontalBox( "BOX_G40", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G40"   )
	oView:CreateHorizontalBox( "BOX_G3Z", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G3Z"   )
	oView:CreateHorizontalBox( "BOX_G43", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G43"   )
	oView:CreateHorizontalBox( "BOX_G4B", 100, , , "FOLDER_SEG"  , "FOLDER_SEG_G4B"   ) 

	oView:SetOwnerView( "VIEW_G44", "BOX_G44" )
	oView:SetOwnerView( "VIEW_G47", "BOX_G47" )
	oView:SetOwnerView( "VIEW_G3T", "BOX_G3T" )
	oView:SetOwnerView( "VIEW_G3U", "BOX_G3U" )
	oView:SetOwnerView( "VIEW_G3V", "BOX_G3V" )
	oView:SetOwnerView( "VIEW_G3W", "BOX_G3W" )
	oView:SetOwnerView( "VIEW_G3Y", "BOX_G3Y" )
	oView:SetOwnerView( "VIEW_G3X", "BOX_G3X" )
	oView:SetOwnerView( "VIEW_G42", "BOX_G42" )
	oView:SetOwnerView( "VIEW_G41", "BOX_G41" )
	oView:SetOwnerView( "VIEW_G40", "BOX_G40" )
	oView:SetOwnerView( "VIEW_G3Z", "BOX_G3Z" )
	oView:SetOwnerView( "VIEW_G43", "BOX_G43" )
	oView:SetOwnerView( "VIEW_G4B", "BOX_G4B" )

 	//Bot�es
	oView:AddUserButton( STR0003, STR0004, {|| TA042APesq() }, , , {MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE}) //"Pesquisar DR"###"Pesquisa documentos de reserva"
	oView:AddUserButton( STR0005, STR0006, {|| TA042VAtuDr()}, , , {MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE}) //"Atualizar DRs"###"Atualiza informa��es do DR devido altera��es"
	oView:AddUserButton( STR0008, STR0009, {|| TA042VNShow()}, , , {MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE}) //"No Show"###"Realizar a opera��o de No Show"
	oView:AddUserButton( STR0011, STR0012, {|| TA042VDesfz()}, , , {MODEL_OPERATION_INSERT ,MODEL_OPERATION_UPDATE}) //"Desfaz Alt."###"Desfazer No Show, Acerto e Parc."

	//Desabilitar Edi��o da View
	oView:SetOnlyView( "VIEW_G4B")
	oView:SetAfterViewActivate({|oView| TA042HiFol( oView ) })
	oView:SetViewProperty("G3R_ITENS","CHANGELINE",{{|oView, cViewID| TA042ChLin( oView, cViewID ) }})
	oView:SetVldFolder({|cFolderID, nOldSheet, nSelSheet| TA042VlFol(cFolderID, nOldSheet, nSelSheet)})
	
	If !IsInCallStack("TuraEfetiv") .And. !IsInCallStack("TA042AEfet") 
		SetKey(VK_F8, {|| FwMsgRun( , {|| TA042AtuDR(.T.)}, , STR0007)}) // "Aplicando altera��es no DR..."
	EndIf

Return oView

/*/{Protheus.doc} TA042Desfz
Fun��o respons�vel por desfazer a efetiva��o da concilia��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042Desfz()
	Local nX		:= 0
	Local oModel 	:= FwModelActive()
	Local oMdlG3Q	:= oModel:GetModel( "G3Q_ITENS" )
	Local oMdlG3R	:= oModel:GetModel( "G3R_ITENS" )
	Local oMdlG48A  := oModel:GetModel( "G48A_ITENS" )
	Local oMdlG48B  := oModel:GetModel( "G48B_ITENS" )
	Local cSeg		:= oMdlG3R:GetValue( "G3R_TPSEG" )
	Local cTabSeg	:= ""
	Local cFldDtIni	:= ""
	Local cFldDtFim	:= ""
	Local cPeriodo	:= ""
	Local cMessage	:= ""
	Local nLine		:= oMdlG3R:GetLine()
	Local cChaveTDR := oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID") + oMdlG3R:GetValue("G3R_IDITEM") + oMdlG3R:GetValue("G3R_NUMSEQ")
	Local lRet      := .T.
	Local aLines 	:= FWSaveRows()

	If oModel:GetOperation() <> MODEL_OPERATION_INSERT .And. oModel:GetOperation() <> MODEL_OPERATION_UPDATE
		Help(,,"TA042Desfz",,STR0095,1,0) //"Op��o dispon�vel somente para INCLUS�O ou ALTERA��O."
		lRet := .F.
	EndIf
	
	If lRet .And. oModel:GetValue("G8C_MASTER", "G8C_STATUS") == "1"
		Help(,,"TA042Desfz",,STR0039,1,0)	 //"Concilia��o j� efetivada, estorne a efetiva��o para prosseguir com o cancelameno do NO SHOW."
		lRet := .F.
	EndIf

	If lRet .And. oMdlG3R:IsDeleted()
		Help(,,"TA042Desfz",,STR0103,1,0)	 //"N�o � poss�vel gerar um No Show para um DR deletado."
		lRet := .F.
	EndIf

	If lRet
		//Registro particionado
		If oMdlG3R:GetValue("G3R_STATUS ") == "2"
			//Verifica se todos os itens gerados da parcialidade est�o presentes no mesmo RV
			If !T042ExItPr( oMdlG3R:GetValue( 'G3R_NUMID' 	),;
			 				oMdlG3R:GetValue( 'G3R_IDITEM' 	),;
			 				oMdlG3R:GetValue( 'G3R_NUMSEQ' 	),;
			 				oMdlG3R:GetValue( 'G3R_CONCIL'	),;
			 				oMdlG3R:GetValue( 'G3R_FILCON'	),;
			 				@cMessage 									)
				FwAlertWarning( STR0153 + CRLF + cMessage, STR0155 ) //"Aten��o"
				lRet := .F.
			EndIf
		Else
			If !Empty(oMdlG3R:GetValue("G3R_SEQPRC")) .And. oMdlG3R:GetValue("G3R_ACERTO") <> '1' .And. oMdlG3R:GetValue("G3R_ACRAUT") <> '1'
				Help(,,"TA042Desfz",,STR0048,1,0)	 //"Doc. de Reserva posicionado n�o possui parcionalidade para ser estornada."
				lRet := .F.
			ElseIf !Empty(oMdlG3R:GetValue("G3R_SEQNSH")) .And. oMdlG3R:GetValue("G3R_NOSHOW") <> "1"
				Help(,,"TA042Desfz",,STR0170,1,0)	 // "Apenas Documentos de Reserva com status 'PARTICIONADO' podem ter o NO SHOW desfeito."
				lRet := .F.
			EndIf
		EndIf
	
		If lRet .And. (IsBlind() .Or. ApMsgYesNo(STR0040)) //"Deseja estornar as altera��es de No Show, Acertos e Parcialidade desde DR?"
			SetAllDelete(.T.)
	
			If oMdlG3R:GetValue("G3R_NOSHOW") == "1" .And. !TA042NShow("2")
				lRet := .F.	
			EndIf

			If oMdlG3R:GetValue("G3R_STATUS") == "2" .And. !TA042Parc( "2" )
				lRet := .F.
			ElseIf TA042SegPr( cSeg, '1' )
				//Verifica se houve altera��o das datas de inicial ou final do registro posicionado
				//Quando n�o h� necessidade de gerar parcialidade do registro (nenhum valor foi cobrado do cliente ainda) as datas s�o alteradas diretamente no registro
	
				TA042AcGrd( oModel, .T.  )
	
				TA042FdSeg( cSeg, @cTabSeg, @cFldDtIni, @cFldDtFim, @cPeriodo )
				cIdSeg		:= cTabSeg + "_ITENS"
	
				//Restaura as datas originais
				If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtIni )  )
					oModel:SetValue(cIdSeg, cTabSeg + cFldDtIni, oModel:GetValue(cIdSeg, "XXX" + cFldDtIni ) )
				EndIf
	
				If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtFim )  )
					oModel:SetValue(cIdSeg, cTabSeg + cFldDtFim, oModel:GetValue(cIdSeg, "XXX" + cFldDtFim ) )
				EndIf
	
				If !Empty(cPeriodo) .And. !Empty( oModel:GetValue(cIdSeg, "XXX" + cPeriodo )  )
					oModel:SetValue(cIdSeg, cTabSeg + cPeriodo, oModel:GetValue(cIdSeg, "XXX" + cPeriodo ) )
				EndIf
	
				TA042AcGrd( oModel, .F.  )
			EndIf
		
			If lRet	
				TA042Acert( oModel, "2" )
		
				//Reaplica os acordos de fornecedores
				oMdlG3R:GoLine(nLine)
				cFilAnt := oMdlG3R:GetValue("G3R_MSFIL")
		
				T42AECanAC( oModel, '1' )
				T42AECanAC( oModel, '2' )
		
				If !oMdlG3R:IsDeleted()
		
					If !oMdlG48B:IsEmpty()
						T34CalcAco( , , "2", "0", .T.)
					EndIf
		
					If !oMdlG48A:IsEmpty()
						T34CalcAco( , , "1", "0", .T.)
					EndIf
		
				EndIf
		
				//Refaz os itens financeiros
				oMdlG48A:GoLine(1)
				oMdlG48B:GoLine(1)
		
				//Atualiza Demonstrativo Financeiro
				T34AtuDmFi(oModel)
		
				If !Tur34ItFin(oModel, oMdlG3Q:GetValue("G3Q_ATUIF"), .T., '3', , .T.)
					oMdlG3R:GoLine(nLine)	
					oMdlG3R:SetValue('G3R_DIVERG', '1')
					TA042AcGrd(	oModel, .T. )
					SetAllDelete(.T.)	
					TA42DelSons( oModel, 'G3R_ITENS', .F. )
					SetAllDelete(.F.)	
					TA042AcGrd( oModel, .F. )
					Help( , , "TA042Desfz", , I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
				Else
					If !IsBlind()
						oView := FwViewActive()
						oView:GetViewObj("G4CA_ITENS")[3]:Refresh(.T.,.F.)
						oView:GetViewObj("G4CB_ITENS")[3]:Refresh(.T.,.F.)
					EndIf
					TA042StsIF( oModel )
				EndIf
			EndIf
		EndIf
	EndIf
	
	SetAllDelete(.F.)
	If lRet 
		CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .T./*lAllLines*/, cChaveTDR, "UPDLIN"/*cMomento*/)
	EndIf
	oMdlG3R:GoLine(nLine)
	FWRestRows( aLines )

	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042NShow
Fun��o para tratamento de No Show
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cOpc, character, Indica a opera��o a ser executada "1" - Gera NoShow; "2" - Desfaz NoShow
/*/
Function TA042NShow( cOpc )
	Local aCopyValues	:= {}
	Local aValuesG46	:= {}
	Local aLines		:= FWSaveRows()
	Local aAux			:= {}
	Local cSeqNoShow	:= ""
	Local cSeqG3Q		:= ""
	Local cSeqPend      := ""
	Local cIdSeg		:= ""
	Local cTabSeg		:= ""
	Local cFldPeriodo	:= ""
	Local cChaveTDR     := ""
	Local nX			:= 0
	Local nY			:= 0
	Local nPeriodo		:= 0
	Local oModel 		:= FwModelActive()
	Local oMdlG3Q       := oModel:GetModel( "G3Q_ITENS" )
	Local oMdlG3R       := oModel:GetModel( "G3R_ITENS" )
	Local cFilRV        := oMdlG3R:GetValue('G3R_FILIAL')
	Local cNumId        := oMdlG3R:GetValue('G3R_NUMID')
	Local cIdItem       := oMdlG3R:GetValue('G3R_IDITEM')
	Local cNumSeq       := oMdlG3R:GetValue('G3R_NUMSEQ')
	Local nLinSave	    := oMdlG3R:GetLine()
	Local lRet          := .T.

	TA042AcGrd( oModel, .T.  )

	If oMdlG3R:GetValue("G3R_TPSEG") == "2"
		cIdSeg		:= "G3U_ITENS"
		cTabSeg	    := "G3U"
		cFldPeriodo := "_QTDDIA"
	Else
		cIdSeg		:= "G3V_ITENS"
		cTabSeg	    := "G3V"
		cFldPeriodo := "_QTDDIA"
	EndIf

	If (oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE) .And. ;
	   (GetAltTarifa() .Or. GetAltTaxas() .Or. GetAltExtras() .Or. GetAltImpostos() .Or. GetAltSeg() .Or. GetAplAcordFin())
		TurHelp(STR0202, STR0203, "TA042NOSHOW")	// "Foram realizadas altera��es em outros Documentos de Reserva da concilia��o."	"Utilize a rotina 'Atualizar DRs' para atualiza��o e depois refa�a o No Show."
		lRet := .F.
	Else
		If cOpc == "1" //C�lculo de No Show
			If oModel:GetOperation() <> 3 .And. oModel:GetOperation() <> 4
				Help(,,"TA042NOSHOW",,STR0041,1,0)	 //"Rotina dispon�vel somente em opera��es de INCLUS�O ou ALTERA��O."
				lRet := .F.
			ElseIf oMdlG3R:IsDeleted()
				Help(,,"TA042NOSHOW",,STR0103,1,0)	 //"N�o � poss�vel gerar um No Show para um DR deletado."
				lRet := .F.
			ElseIf oMdlG3R:GetValue("G3R_TPSEG") <> "2" .And. oMdlG3R:GetValue("G3R_TPSEG") <> "3" //Somente segmentos "2-Hotel" e "3-Carro" possuem No Show
				Help(,,"TA042NOSHOW",,STR0042,1,0)	 //"Apenas segmentos Hotel e Carro podem ser aplicados no Show."
				lRet := .F.
			ElseIf ( oMdlG3R:GetValue("G3R_STATUS" ) ) <> "1"
				Help(,,"TA042NOSHOW",,STR0044,1,0)	 //"Apenas Documentos de Reserva com status 'PENDENTE' podem ter NO SHOW."
				lRet := .F.
			ElseIf ( !Empty( oMdlG3R:GetValue("G3R_SEQNSH" ) ) )
				Help(,,"TA042NOSHOW",,STR0102,1,0)	 //Registro com origem em No Show, n�o � poss�vel aplicar um novo No Show."
				lRet := .F.
			ElseIf ( oMdlG3R:GetValue("G3R_ACERTO" ) == '1' )
				Help(,,"TA042NSACERTO",,STR0131,1,0)	 //"N�o � permitido aplicar No Show em item de acerto."
				lRet := .F.
			ElseIf Empty(SuperGetMV("MV_TURTNS", , "")) .Or. !ExistCpo("G3A", SuperGetMV("MV_TURTNS", , ""))
				Help(,,"TA042NOSHOW",,STR0082,1,0)	//"Solicite ao administrador do sistema cadastrar o param�tro 'MV_TURTNS' com o c�digo da taxa de NO SHOW."
				lRet := .F.
			ElseIf !IsBlind() .And. !ApMsgYesNo(STR0043) //"Deseja gerar o No Show do registro posicionado?"
				lRet := .F.
			EndIf
	
			If lRet
				aCopyValues	:= TA042GetVls( "G3R_ITENS", .F., {"G4CA_ITENS", "G4CB_ITENS"} )
				cSeqNoShow  := oMdlG3R:GetValue("G3R_NUMSEQ")
		
				//Obtem o periodo original
				nPeriodo := oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo )
		
				aAdd( aValuesG46, { "G46_FILIAL", xFilial("G46") 								} )
				aAdd( aValuesG46, { "G46_NUMID"	, oMdlG3R:GetValue("G3R_NUMID" )				} )
				aAdd( aValuesG46, { "G46_IDITEM", StrZero(1, TamSX3("G46_IDITEM")[1])			} )
				aAdd( aValuesG46, { "G46_NUMSEQ", oMdlG3R:GetValue("G3R_NUMSEQ" )				} )
				aAdd( aValuesG46, { "G46_SEQTAX", StrZero(1, TamSX3("G46_SEQTAX")[1])			} )
				aAdd( aValuesG46, { "G46_EMISS"	, dDataBase										} )
				aAdd( aValuesG46, { "G46_CODTX" , SuperGetMV( "MV_TURTNS",,"") 					} )
				aAdd( aValuesG46, { "G46_VLBASE", oModel:GetValue( "G44_ITENS", "G44_TARBAS" ) / nPeriodo	} )
				aAdd( aValuesG46, { "G46_MOEDFO", oModel:GetValue( "G44_ITENS", "G44_MOEDFO" )	} )
				aAdd( aValuesG46, { "G46_TXCAMB", oModel:GetValue( "G44_ITENS", "G44_TXCAMB" )	} )
				aAdd( aValuesG46, { "G46_MOEDA" , oModel:GetValue( "G44_ITENS", "G44_MOEDA" )	} )
				aAdd( aValuesG46, { "G46_CONMAN", "2"											} )
				aAdd( aValuesG46, { "G46_OBS" 	, STR0045 										} ) //"Aplicada taxa de NO SHOW"
		
				//Altera o status dos registros originais na G3Q e G3R
				oMdlG3Q:SetValue("G3Q_STATUS", "4")
				oMdlG3R:SetValue("G3R_STATUS", "2")
				oMdlG3R:SetValue("G3R_NOSHOW", "1")
	
				//Exclui acordos se abertos e n�o est�o em apura��o
				T42ACanACO( oModel, '1' )	
				T42ACanACO( oModel, '2' )
			
				//Cria��o do primeiro par de registros
				If TA042SetVs( aCopyValues )
					cSeqG3Q := cSeqPend := TA042MaxSq( oModel )
					TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
		
					oMdlG3R:LoadValue("G3R_SEQNSH", cSeqNoShow)
					oMdlG3Q:SetValue ("G3Q_STATUS", "1")
		
					oModel:SetValue( cIdSeg, cTabSeg + "_DTFIM" , oModel:GetValue( cIdSeg, cTabSeg + "_DTINI" ) )
					oModel:SetValue( "G44_ITENS", "G44_TARBAS", 0 )
		
					//Gera a taxa do No Show
					TA042SetVl(oModel,"G46_ITENS",aValuesG46,.F.)
		
					//Cria��o do segundo par de registros
					If nPeriodo > 1 .AND.  TA042SetVs( aCopyValues )
						cSeqG3Q := TA042MaxSq( oModel )
						TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
		
						oMdlG3R:LoadValue("G3R_SEQNSH", cSeqNoShow)
						oMdlG3R:SetValue ("G3R_STATUS", "4")
						oMdlG3Q:SetValue ("G3Q_STATUS", "3")
						oModel:SetValue  (cIdSeg, cTabSeg + "_DTINI" , oModel:GetValue( cIdSeg, cTabSeg + "_DTINI" ) + 1 )
					EndIf
				EndIf
	
				If oMdlG3R:SeekLine({{"G3R_NUMSEQ", cSeqPend}})
					T34AtuDmFi(oModel)
					If !(lRet := Tur34ItFin(oModel, oMdlG3Q:GetValue("G3Q_ATUIF"), .T., '3', , .T.))
						oMdlG3R:GoLine(nLinSave)	
						oMdlG3R:SetValue('G3R_DIVERG', '1')	
						TA042AcGrd(	oModel, .T. )
						SetAllDelete(.T.)
						TA42DelSons( oModel, 'G3R_ITENS', .F. )
	
						While oMdlG3R:SeekLine({{"G3R_NUMSEQ", cSeqPend}}) .And. !oMdlG3R:IsDeleted()
							oMdlG3R:SetValue('G3R_DIVERG', '1')	
							TA42DelSons( oModel, 'G3R_ITENS', .F. )
						EndDo
	
						SetAllDelete(.F.)
						TA042AcGrd(	oModel, .F. )	
						Help( , , "TA042NOSHOW", , I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
					Else
						If !IsBlind()
							oView := FwViewActive()
							oView:GetViewObj("G4CA_ITENS")[3]:Refresh(.T.,.F.)
							oView:GetViewObj("G4CB_ITENS")[3]:Refresh(.T.,.F.)
						EndIf
						TA042StsIF(oModel)
					EndIf
				EndIf
			EndIf
		ElseIf cOpc == "2" // Desfazer No Show
	
			If oModel:GetValue("G8C_MASTER", "G8C_STATUS") == "1"
				Help(,,"TA042DNOSHOW",,STR0039,1,0)	 //"Concilia��o j� efetivada, estorne a efetiva��o para prosseguir com o cancelameno do NO SHOW."
				lRet := .F.
			EndIf
	
			If lRet .And. oMdlG3R:GetValue("G3R_NOSHOW") <> "1"
				Help(,,"TA042DNOSHOW",,STR0046,1,0)				 //"Doc. de Reserva posicionado n�o possui No Show para ser estornado."
				lRet := .F.
			EndIf
	
			If lRet
				//Obtem a sequ�ncia que ir� originar o No Show
				cSeqNoShow := oMdlG3R:GetValue("G3R_NUMSEQ" )
		
				//Altera o status dos registros originais na G3Q e G3R
				oMdlG3Q:SetValue("G3Q_STATUS", "1" )
				oMdlG3R:SetValue("G3R_NOSHOW", "2" )
			
				T42AECanAC( oModel, '1' )
				T42AECanAC( oModel, '2' )
	
				For nX := 1 To oMdlG3R:Length()
					oMdlG3R:GoLine( nX )
		
					If oMdlG3R:GetValue("G3R_SEQNSH" ) == cSeqNoShow
						SetAllDelete(.T.)
		
						oModel:GetModel( "G46_ITENS" ):DelAllLine()
						oModel:GetModel( "G44_ITENS" ):DelAllLine()
						oModel:GetModel( "G3V_ITENS" ):DelAllLine()
						oModel:GetModel( "G3U_ITENS" ):DelAllLine()
						oModel:GetModel( "G3S_ITENS" ):DelAllLine()
						oMdlG3Q:DeleteLine()
						oMdlG3R:DeleteLine()
						TDRCacheDel(oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID") + oMdlG3R:GetValue("G3R_IDITEM") + oMdlG3R:GetValue("G3R_NUMSEQ"))
		
						SetAllDelete(.F.)
					EndIf
				Next
		
				If oMdlG3R:SeekLine({{"G3R_NUMSEQ", oMdlG3R:GetValue("G3R_SEQNSH")}})
					T34AtuDmFi(oModel)
					If !(lRet := Tur34ItFin(oModel, oMdlG3Q:GetValue("G3Q_ATUIF"), .T., '3', , .T.))
						oMdlG3R:GoLine(nLinSave)	
						oMdlG3R:SetValue('G3R_DIVERG', '1')	
						TA042AcGrd(	oModel, .T. )
						SetAllDelete(.T.)	
						TA42DelSons( oModel, 'G3R_ITENS', .F. )
	
						While oMdlG3R:SeekLine({{"G3R_NUMSEQ", oMdlG3R:GetValue("G3R_SEQNSH")}}) .And. !oMdlG3R:IsDeleted()
							oMdlG3R:SetValue('G3R_DIVERG', '1')	
							TA42DelSons( oModel, 'G3R_ITENS', .F. )
						EndDo
	
						SetAllDelete(.F.)	
						TA042AcGrd(	oModel, .F. )
						Help( , , "TA042DNOSHOW", , I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
					Else
						If !IsBlind()
							oView := FwViewActive()
							oView:GetViewObj("G4CA_ITENS")[3]:Refresh(.T.,.F.)
							oView:GetViewObj("G4CB_ITENS")[3]:Refresh(.T.,.F.)
						EndIf
						TA042StsIF(oModel)
					EndIf
				EndIf
			EndIf
		EndIf
	EndIf
	
	TA042AcGrd(oModel, .F.)
	If lRet .And. !FwIsInCallStack('TA042Desfz')
		nX := nLinSave
		oMdlG3R:GoLine(nLinSave)
		While oMdlG3R:GetValue('G3R_FILIAL') == cFilRV .And. oMdlG3R:GetValue('G3R_NUMID') == cNumId .And. oMdlG3R:GetValue('G3R_IDITEM') == cIdItem
			cChaveTDR := oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID") + oMdlG3R:GetValue("G3R_IDITEM") + oMdlG3R:GetValue("G3R_NUMSEQ")
			CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .F./*lAllLines*/, cChaveTDR, "UPDLIN"/*cMomento*/)

			If ++nX > oMdlG3R:Length()
				Exit
			EndIf
			
			cNumSeq := Soma1(oMdlG3R:GetValue('G3R_NUMSEQ'))
			If oMdlG3R:SeekLine({{"G3R_FILIAL", cFilRV}, {"G3R_NUMID", cNumId}, {"G3R_IDITEM", cIdItem}, {"G3R_NUMSEQ", cNumSeq}})
				nX := oMdlG3R:GetLine()
			EndIf
			
			oMdlG3R:GoLine(nX)
		EndDo

		SetAltTarifa(.F.)
		SetAltTaxas(.F.)
		SetAltExtras(.F.)
		SetAltImpostos(.F.)
		SetAltSeg(.F.)
		SetAplAcordFin(.F.)
	EndIf

	oMdlG3R:GoLine(nLinSave)
	FwRestRows(aLines)
	
	TURXNIL(aCopyValues)
	TURXNIL(aValuesG46)
	TURXNIL(aLines)
	TURXNIL(aAux)
Return lRet

/*/{Protheus.doc} TA042Parc
Fun��o para tratamento de parcialidade
@type function
@author Anderson Toledo
@since 15/03/2016
@param cOpc, character, indica o tipo de opera��o a ser executado: "1" - Gera parcionalide; "2" - Desfaz parcionalidade
/*/
Static Function TA042Parc( cOpc, lAll )
	Local aLines		:= FWSaveRows()
	Local aCopyValues	:= {}
	Local cSeg			:= ""
	Local cIdSeg		:= ""
	Local cTabSeg		:= ""
	Local cFldDtIni	    := ""
	Local cFldDtFim	    := ""
	Local cFldPeriodo	:= ""
	Local cSeqParc	    := ""
	Local cChaveTDR     := ""
	Local nTotPer		:= 0
	Local nNewPer		:= 0
	Local nX			:= 0
	Local dParcIni	    := CtoD( " /  / " )
	Local dParcFim	    := CtoD( " /  / " )
	Local dFirstDate	:= CtoD( " /  / " )
	Local dLastDate	    := CtoD( " /  / " )		
	Local lBkpAllDel	:= GetAllDelete()
	Local oModel 		:= FwModelActive()
	Local oModelG3Q	    := oModel:GetModel("G3Q_ITENS")
	Local oModelG3R	    := oModel:GetModel("G3R_ITENS")
	Local cStsG3R       := oModelG3R:GetValue('G3R_STATUS')
	Local cStsG3Q       := oModelG3Q:GetValue('G3Q_STATUS')
	Local nLinSave	    := oModelG3R:GetLine()
	Local lRet          := .T.

	DEFAULT	lAll := .F.

	SetAllDelete(.T.)

	//Se origem foi do NoShow, n�o faz nada
	If !Empty( oModelG3R:GetValue( "G3R_SEQNSH" ) )
		lRet := .F.
	Else
		cSeg := oModelG3R:GetValue( "G3R_TPSEG" )
	
		//Apenas os segmentos listados abaixo possuem parcialidade
		If !TA042SegPr( cSeg, '1' )
			lRet := .F.
		Else
			TA042AcGrd( oModel, .T.  )
		
			If cOpc == "1"
				aCopyValues	:= TA042GetVls( "G3R_ITENS", .F., TA042NoSeg( cSeg ) )
				
				//Obtem o ID do modelo e os campos de acordo com o segmento
				TA042FdSeg( cSeg, @cTabSeg, @cFldDtIni, @cFldDtFim, @cFldPeriodo  )
		
				cIdSeg := cTabSeg + "_ITENS"
		
				//Obtem a sequ�ncia que ir� originar o No Show
				cSeqParc := oModelG3R:GetValue("G3R_NUMSEQ" )
		
				//Verifica se a data inicial alterada � maior que a data inicial original
				If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtIni ) ) .And. oModel:GetValue(cIdSeg, cTabSeg + cFldDtIni ) > oModel:GetValue(cIdSeg, "XXX" + cFldDtIni )
					dParcIni := oModel:GetValue(cIdSeg, cTabSeg + cFldDtIni )
				EndIf
		
				//Verifica se a data inicial alterada � maior que a data inicial original
				If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtFim ) ) .And. oModel:GetValue(cIdSeg, cTabSeg + cFldDtFim ) < oModel:GetValue(cIdSeg, "XXX" + cFldDtFim )
					dParcFim := oModel:GetValue(cIdSeg, cTabSeg + cFldDtFim )
				EndIf
		
				If ( !Empty( dParcIni ) .Or. !Empty( dParcFim ) ) .And. ( TA042VlAPrc( oModel, cIdSeg, cTabSeg, cFldDtIni, cFldDtFim, cFldPeriodo, lAll ) )
		
					//Altera os status do registro original
					oModelG3Q:SetValue("G3Q_STATUS", "4" )
					oModelG3R:SetValue("G3R_STATUS", "2" )
		
					//Exclui acordos se abertos e n�o est�o em apura��o
					T42ACanACO( oModel, '1' )	
					T42ACanACO( oModel, '2' )
		
					//Libera novamente os modelos, algumas fun��es do TURA034 alteram a propriedade SetNoInsertLine
					TA042AcGrd( oModel, .T.  )
		
					//Restaura as datas originais
					If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtIni )  )
						oModel:SetValue(cIdSeg, cTabSeg + cFldDtIni, oModel:GetValue(cIdSeg, "XXX" + cFldDtIni ) )
					EndIf
					
					If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtFim )  )
						oModel:SetValue(cIdSeg, cTabSeg + cFldDtFim, oModel:GetValue(cIdSeg, "XXX" + cFldDtFim ) )
					EndIf
		
					dFirstDate	:= oModel:GetValue(cIdSeg, cTabSeg + cFldDtIni )
					dLastDate	:= oModel:GetValue(cIdSeg, cTabSeg + cFldDtFim )		
		
					//Obtem o total do periodo original
					If !Empty( cFldPeriodo )
						nTotPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo )
					Else
						nTotPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) -	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )
					EndIf
		
					//Gera registro com o periodo inicial cancelado
					If !Empty( dParcIni )
						If TA042SetVs( aCopyValues )
							cSeqG3Q := TA042MaxSq( oModel )
							TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
		
							oModelG3R:LoadValue("G3R_SEQPRC"	, cSeqParc )
							oModelG3R:SetValue ("G3R_DTINC"		, dDataBase 	)
							oModelG3R:SetValue ("G3R_STATUS", "4" )
							oModelG3Q:SetValue ("G3Q_STATUS", "3" )
		
							oModel:SetValue(cIdSeg, cTabSeg + cFldDtIni, oModel:GetValue(cIdSeg, "XXX" + cFldDtIni ) )
							oModel:SetValue( cIdSeg, cTabSeg + cFldDtFim, dParcIni ) //Altera a data final da reserva cancelada
		
							//Zera os valores backup
							oModel:LoadValue( cIdSeg, "XXX" + cFldDtIni, ctod( " / / " ) )
							oModel:LoadValue( cIdSeg, "XXX" + cFldDtFim, ctod( " / / " ) )
		
							If !Empty( cFldPeriodo )
								nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo )
							Else
								nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) - 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )
							EndIf
		
							//Utiliza regra de tr�s para a propor��o do valor da tarifa no novo registro
							TA042ProTr( oModel, nTotPer, nNewPer )
		
							//Apenas as taxas e extras com a emiss�o entre o per�odo inicio e fim ser�o mantidos no DR
							TA042PrcTx( oModel,	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )	,;
												 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) 	,;
												 	dFirstDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni ) ,;
												 	dLastDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) )
		
							//Exclui acordos abertos e em apura��o
							TA042DlAcF( oModel, .F., .T. )
		
							//Libera novamente os modelos, algumas fun��es do TURA034 alteram a propriedade SetNoInsertLine
							TA042AcGrd( oModel, .T.  )
		
							//Recalcula os valores do DR
							TA042AtuAc( oModel )
		
						EndIf
					EndIf
		
					//Gera o registro com as datas da concilia��o
					If TA042SetVs( aCopyValues )
						cSeqG3Q := TA042MaxSq( oModel )
						TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
		
						oModelG3R:LoadValue("G3R_SEQPRC"	, cSeqParc )
						oModelG3R:SetValue ("G3R_DTINC", dDataBase )
						oModelG3R:SetValue ("G3R_STATUS", "1" )
						oModelG3Q:SetValue ("G3Q_STATUS", "1" )
		
						//Zera os valores backup
						oModel:LoadValue( cIdSeg, "XXX" + cFldDtIni, ctod( " / / " ) )
						oModel:LoadValue( cIdSeg, "XXX" + cFldDtFim, ctod( " / / " ) )
		
						If !Empty( cFldPeriodo )
							nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo )
						Else
							nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) - 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )
						EndIf
		
						//Utiliza regra dde tr�s para a propor��o do valor da tarifa no novo registro
						TA042ProTr( oModel, nTotPer, nNewPer )
		
						//Apenas as taxas e extras com a emiss�o entre o per�odo inicio e fim ser�o mantidos no DR
						TA042PrcTx( oModel, 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )	,; 
												oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) 	,;
												dFirstDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni ) ,;
												 dLastDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) )
		
						//Exclui acordos encerrados e que est�o em apura��o
						TA042DlAcF( oModel, .T. )
		
						//Libera novamente os modelos, algumas fun��es do TURA034 alteram a propriedade SetNoInsertLine
						TA042AcGrd( oModel, .T.  )
		
						//Recalcula os valores do DR()
						TA042AtuAc( oModel )
		
					EndIf
		
					//Gera registro com o periodo final cancelado
					If !Empty( dParcFim ) 
						If TA042SetVs( aCopyValues )
							cSeqG3Q := TA042MaxSq( oModel )
							TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )
			
							oModelG3R:LoadValue("G3R_SEQPRC"	, cSeqParc )
							oModelG3R:SetValue ("G3R_DTINC", dDataBase )
							oModelG3R:SetValue ("G3R_STATUS", "4" )
			
							oModelG3Q:SetValue("G3Q_STATUS", "3" )
							oModel:SetValue( cIdSeg, cTabSeg + cFldDtFim, oModel:GetValue( cIdSeg, "XXX" + cFldDtFim ) )
							oModel:SetValue( cIdSeg, cTabSeg + cFldDtIni, dParcFim )
			
							//Zera os valores backup
							oModel:LoadValue( cIdSeg, "XXX" + cFldDtIni, ctod( " / / " ) )
							oModel:LoadValue( cIdSeg, "XXX" + cFldDtFim, ctod( " / / " ) )
			
							If !Empty( cFldPeriodo )
								nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo )
							Else
								nNewPer	:= 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) - 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )
							EndIf
			
							//Utiliza regra dde tr�s para a propor��o do valor da tarifa no novo registro
							TA042ProTr( oModel, nTotPer, nNewPer )
			
							//Apenas as taxas e extras com a emiss�o entre o per�odo inicio e fim ser�o mantidos no DR
							TA042PrcTx( oModel, 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni )	,;
												 	oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim )	,;
												 	dFirstDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtIni ) ,;
													 dLastDate == oModel:GetValue( cIdSeg, cTabSeg + cFldDtFim ) )
			
							//Exclui acordos se abertos e n�o est�o em apura��o
							TA042DlAcF( oModel, .F., .T. )
			
							//Libera novamente os modelos, algumas fun��es do TURA034 alteram a propriedade SetNoInsertLine
							TA042AcGrd( oModel, .F.  )
			
							//Recalcula os valores do DR()
							TA042AtuAc( oModel )
						EndIf
					EndIf
		
				EndIf
	
			ElseIf cOpc == "2"
		
				If oModel:GetValue("G8C_MASTER", "G8C_STATUS") == "1"
					Help(,,"TA042PARC",,STR0047,1,0)	 //"Concilia��o j� efetivada, estorne a efetiva��o para prosseguir com o cancelameno da parcionalidade."
					lRet := .F.
				EndIf
		
				If lRet
					//Obtem a sequ�ncia que ir� desfazer a Parcialidade
					cSeqParc := oModelG3R:GetValue("G3R_NUMSEQ")
			
					//Altera o status dos registros originais na G3Q e G3R
					oModelG3Q:SetValue("G3Q_STATUS", "1" )
					oModelG3R:SetValue("G3R_STATUS", "1" )
			
					// verificando se o DR parcial tambem foi parcializado 
					If oModelG3R:SeekLine({{"G3R_SEQPRC", cSeqParc}})
						oModelG3R:GoLine(oModelG3R:GetLine())		
						If oModelG3R:GetValue("G3R_STATUS") == '2'  
							FwAlertHelp(I18N(STR0171, {oModelG3R:GetValue("G3R_NUMSEQ")}) , I18N(STR0172, {oModelG3R:GetValue("G3R_NUMSEQ")}))				 
			
							// voltando o status dos registros originais na G3Q e G3R
							oModelG3Q:SetValue("G3Q_STATUS", cStsG3Q)
							oModelG3R:SetValue("G3R_STATUS", cStsG3R)
			
							lRet := .F.
						EndIf
					EndIf
					
					If lRet 
						For nX := 1 To oModelG3R:Length()
							oModelG3R:GoLine( nX )
							If oModelG3R:GetValue("G3R_SEQPRC") == cSeqParc 
								oModelG3R:DeleteLine()
								TDRCacheDel(oModelG3R:GetValue("G3R_FILIAL") + oModelG3R:GetValue("G3R_NUMID") + oModelG3R:GetValue("G3R_IDITEM") + oModelG3R:GetValue("G3R_NUMSEQ"))
							EndIf
						Next
				
					EndIf
				EndIf
			EndIF

			TA042AcGrd(oModel, .F.)
		EndIf
	EndIf

	oModelG3R:GoLine(nLinSave)
	SetAllDelete(lBkpAllDel)
	FwRestRows(aLines)

	TURXNIL(aLines)
	TURXNIL(aCopyValues)
Return lRet

/*/{Protheus.doc} TA042ProTr
Altera o valor da taxa com o valor proporcional do novo per�odo
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042ProTr( oModel, nTotPer, nNewPer )
	Local aLines := FwSaveRows( oModel )
	Local nX		:= 0

	For nX := 1 to oModel:GetModel( "G44_ITENS" ):Length()
		oModel:GetModel( "G44_ITENS" ):GoLine(nX)

		If oModel:GetValue( "G44_ITENS", "G44_VLRDIA" ) > 0
			oModel:SetValue( "G44_ITENS", "G44_TARBAS", Round( oModel:GetValue( "G44_ITENS", "G44_VLRDIA" ) * nNewPer, TamSX3( "G44_TARBAS")[2]) )
		Else
			oModel:SetValue( "G44_ITENS", "G44_TARBAS", Round( oModel:GetValue( "G44_ITENS", "G44_TARBAS" ) * nNewPer / nTotPer, TamSX3( "G44_TARBAS")[2]) )
		EndIf
		
		oModel:SetValue( "G44_ITENS", "G44_APLICA", FwFldGet("G44_TARBAS") * FwFldGet("G44_TXCAMB") )
	Next

	FwRestRows( aLines )
	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042PrcTx
Fun��o para excluir taxas e extras fora do periodo inicial e final gerado pela parcialidade
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042PrcTx( oModel, dDtIni, dDtFim, lFirstDate, lLastDate )
	Local aLines 	:= FwSaveRows()
	Local nX		:= 0
	Local oModelG46	:= oModel:GetModel("G46_ITENS")
	Local oModelG47	:= oModel:GetModel("G47_ITENS")
	Local lBkpAllDel:= GetAllDelete() //Armazena o valor da variavel, a chamada j� pode estar como .T.

	SetAllDelete(.T.)

	For nX := 1 To oModelG46:Length()
		oModelG46:GoLine( nX )

		//Vefica se a taxa est� fora do per�odo da reserva, caso positivo, exclui a taxa ou se est� al�m dos extremos das datas
		If ( oModelG46:GetValue("G46_EMISS") < dDtIni .And. !lFirstDate) .Or. ( oModelG46:GetValue("G46_EMISS") > dDtFim .And. !lLastDate)
			oModelG46:DeleteLine()
		EndIf
	Next

	For nX := 1 To oModelG47:Length()
		oModelG47:GoLine( nX )

		//Vefica se o extra est� fora do per�odo da reserva, caso positivo, exclui a taxa ou se est� al�m dos extremos das datas
		If ( oModelG47:GetValue("G47_EMISS") < dDtIni .And. !lFirstDate ) .Or. ( oModelG47:GetValue("G47_EMISS") > dDtFim .And. !lLastDate )
			oModelG47:DeleteLine()
		EndIf
	Next

	SetAllDelete(lBkpAllDel)

	FwRestRows( aLines )
	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042DlAcF
Exclui acordos com valores fixos e cancela os demais
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042DlAcF( oModel, lAtivo, lDeleteAll, lOnlyPerc )
	Local nX := 0

	DEFAULT lAtivo		:= .F.
	DEFAULT lDeleteAll	:= .F.
	DEFAULT lOnlyPerc		:= .F.

	//Apaga todos os acordos
	If lDeleteAll
		oModel:GetModel("G48A_ITENS"):DelAllLine()
		oModel:GetModel("G48B_ITENS"):DelAllLine()

	//lAtivo - Mantem apenas os acordos em aberto e que n�o estejam em apura��o
	ElseIf lAtivo
		If !oModel:GetModel("G48A_ITENS"):IsEmpty()
			For nX := 1 To oModel:GetModel("G48A_ITENS"):Length()
				oModel:GetModel("G48A_ITENS"):GoLine(nX)

				If oModel:GetValue("G48A_ITENS", "G48_STATUS" ) == "3"
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				ElseIf oModel:GetValue("G48A_ITENS", "G48_STATUS" ) == "4" .Or. !Empty( oModel:GetValue("G48A_ITENS", "G48_CODAPU" ) )
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				ElseIf lOnlyPerc .And. oModel:GetValue("G48A_ITENS", "G48_TPVLR" ) == "2"
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				ElseIf Empty( oModel:GetValue("G48A_ITENS", "G48_APLICA" ) )
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				EndIf

			Next

		EndIf

		If !oModel:GetModel("G48B_ITENS"):IsEmpty()
			For nX := 1 To oModel:GetModel("G48B_ITENS"):Length()
				oModel:GetModel("G48B_ITENS"):GoLine(nX)

				If oModel:GetValue("G48B_ITENS", "G48_STATUS" ) == "3"
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				ElseIf oModel:GetValue("G48B_ITENS", "G48_STATUS" ) == "4" .Or. !Empty( oModel:GetValue("G48B_ITENS", "G48_CODAPU" ) )
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				ElseIf lOnlyPerc .And. oModel:GetValue("G48B_ITENS", "G48_TPVLR" ) == "2"
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				ElseIf Empty( oModel:GetValue("G48B_ITENS", "G48_APLICA" ) )
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				ElseIf oModel:GetValue("G48B_ITENS", "G48_CLASS" ) == 'C11'
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				EndIf

			Next

		EndIf

		If lOnlyPerc .and. !oModel:GetModel("G48A_ITENS"):IsEmpty() 
			For nX := 1 To oModel:GetModel("G48A_ITENS"):Length()
				oModel:GetModel("G48A_ITENS"):GoLine(nX)
				If !oModel:GetModel("G48A_ITENS"):IsDeleted()
					If !( oModel:GetValue("G48A_ITENS", "G48_CLASS" ) == 'C07' .AND. ;
						oModel:GetModel("G48B_ITENS"):SeekLine({{"G48_CODACD",oModel:GetValue("G48A_ITENS", "G48_ACDBAS" )}}) ) 
						oModel:GetModel("G48A_ITENS"):DeleteLine()			
					Endif
				Endif
			Next
		Endif
	Else

		If !oModel:GetModel("G48A_ITENS"):IsEmpty()
			For nX := 1 To oModel:GetModel("G48A_ITENS"):Length()
				oModel:GetModel("G48A_ITENS"):GoLine(nX)

				If oModel:GetValue("G48A_ITENS", "G48_STATUS" ) <> "4" .And. Empty( oModel:GetValue("G48A_ITENS", "G48_CODAPU" ) )
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				ElseIf Empty( oModel:GetValue("G48A_ITENS", "G48_APLICA" ) )
					oModel:GetModel("G48A_ITENS"):DeleteLine()
				EndIf

			Next

		EndIf

		If !oModel:GetModel("G48B_ITENS"):IsEmpty()
			For nX := 1 To oModel:GetModel("G48B_ITENS"):Length()
				oModel:GetModel("G48B_ITENS"):GoLine(nX)

				If oModel:GetValue("G48B_ITENS", "G48_STATUS" ) <> "4" .And. Empty( oModel:GetValue("G48B_ITENS", "G48_CODAPU" ) )
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				ElseIf Empty( oModel:GetValue("G48B_ITENS", "G48_APLICA" ) )
					oModel:GetModel("G48B_ITENS"):DeleteLine()
				EndIf

			Next
		EndIf
	EndIf

Return

/*/{Protheus.doc} TA042VlAPrc
Fun��o para validade se deve ser aplicada a parcioalidade
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042VlAPrc( oModel, cIdSeg, cTabSeg, cFldDtIni, cFldDtFim, cFldPeriodo, lAll  )
	Local dParcIni := ctod( " / / " )
	Local dParcFim := ctod( " / / " )
	Local lRet		 := .T.
	Local nX		 := 0

	//Verifica se a altera��o de datas possui o mesmo per�odo anterior
	If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtIni ) )
		dParcIni := oModel:GetValue(cIdSeg, "XXX" + cFldDtIni )
	Else
		dParcIni := oModel:GetValue(cIdSeg, cTabSeg + cFldDtIni )
	EndIf

	If !Empty( oModel:GetValue(cIdSeg, "XXX" + cFldDtFim ) )
		dParcFim := oModel:GetValue(cIdSeg, "XXX" + cFldDtFim )
	Else
		dParcFim  := oModel:GetValue(cIdSeg, cTabSeg + cFldDtFim )
	EndIf

	If !Empty( cFldPeriodo )
		lRet := oModel:GetValue( cIdSeg, cTabSeg + cFldPeriodo ) <> ( dParcFim - dParcIni ) + 1
	EndIf

	If lRet
		lRet := .F.

		//Verifica se o cliente j� foi cobrado, neste caso n�o gera a parcialidade e mantem as datas informadas
		For nX := 1 To oModel:GetModel("G4CA_ITENS"):Length()
			oModel:GetModel("G4CA_ITENS"):GoLine( nX )

			If lAll .Or. oModel:GetValue("G4CA_ITENS", "G4C_STATUS" ) == '4' .Or. oModel:GetValue("G4CAF_ITENS", "G4C_STATUS" ) == '4' //1-Ag. Libera��o; 2-Ag. Apura��o; 3-Liberado; 4-Finalizado
				lRet := .T.
				Exit 
			EndIf
		Next
	EndIf

Return lRet

/*/{Protheus.doc} TA042Acert
Fun��o para tratamento de acertos no documento de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, objeto instanciado do ModelDef
@param cOpc, character, Indica a opera��o a ser executada: "1" - Gera os acertos; "2" - Desfaz os acertos
/*/
//Posi��o dos vetores que armazenam os valores alterados
//[1]		Num�rico	- Numero da linha alterada
//[2] 		L�gico 	- Indica se a linha estava deletada ou n�o
//[3][1]	Caractere	- Nome do campo que teve altera��o
//[3][2]	Num�rico	- Valor original
//[3][3]	Num�rico 	- Valor do acerto
Function TA042Acert( oModel, cOpc, lAll )
	Local aLines		:= FwSaveRows()
	Local aCopyValues	:= {}
	Local aG44Acerto	:= {}
	Local aG46Acerto	:= {}
	Local aG47Acerto	:= {}
	Local aG49Acerto	:= {}
	Local aFieldsG44	:= { { "G44_TARBAS", "XXX_TARBAS", "XAL_TARBAS" }, { "G44_EARLY", "XXX_EARLY", "XAL_EARLY" }, { "G44_LATE", "XXX_LATE", "XAL_LATE"  }, { "G44_PLALIM", "XXX_PLALIM", "XAL_PLALIM" } }
	Local aFieldsG46	:= { { "G46_VLBASE", "XXX_VLBASE", "XAL_VLBASE" } }
	Local aFieldsG47	:= { { "G47_VLUNIT", "XXX_VLUNIT", "XAL_VLUNIT" } }
	Local aFieldsG49	:= { { "G49_BASE"  , "XXX_BASE", "XAL_BASE" }, { "G49_ALIQ", "XXX_ALIQ", "XAL_ALIQ" } }
	Local cBkpFilAnt	:= cFilAnt
	Local cNumRV		:= ""
	Local cIdRV			:= ""
	Local cSeqG3Q		:= ""
	Local cSeqAcerto	:= ""
	Local nX, nY, nJ    := 0
	Local oMdlG3R       := oModel:GetModel( "G3R_ITENS" )
	Local cFilRV        := oMdlG3R:GetValue('G3R_FILIAL')
	Local cNumId        := oMdlG3R:GetValue('G3R_NUMID')
	Local cIdItem       := oMdlG3R:GetValue('G3R_IDITEM')
	Local cNumSeq       := oMdlG3R:GetValue('G3R_NUMSEQ')
	Local cChaveORI     := oMdlG3R:GetValue("G3R_FILIAL") + oMdlG3R:GetValue("G3R_NUMID") + oMdlG3R:GetValue("G3R_IDITEM") + oMdlG3R:GetValue("G3R_NUMSEQ") 
	Local nLinSave	    := oMdlG3R:GetLine()

	DEFAULT lAll	:= .F.

	TA042AcGrd( oModel, .T.  )

	If cOpc == "1"
		If 	GetAltTarifa() .Or. GetAltTaxas() .Or. GetAltExtras()	.OR. GetAltImpostos()

			For nX := 1 To oModel:GetModel("G3R_ITENS"):Length()
				oModel:GetModel("G3R_ITENS"):GoLine( nX )
				cFilAnt := oModel:GetValue("G3R_ITENS","G3R_MSFIL")
				lAcerto := .F.

				aG44Acerto	:= {}
				aG46Acerto	:= {}
				aG47Acerto	:= {}
				aG49Acerto	:= {}

				If oModel:GetModel("G3R_ITENS"):IsDeleted()
					Loop
				EndIf

				If oModel:GetValue("G3R_ITENS","G3R_STATUS") <> "1"
					Loop
				EndIf

				//Se j� � um acerto autom�tico, n�o gera um novo acerto
				If oModel:GetValue("G3R_ITENS","G3R_ACERTO") == '1' .And. oModel:GetValue("G3R_ITENS","G3R_ACRAUT") == '1'
					Loop
				EndIf

				//Verifica se o cliente j� foi cobrado, somente neste caso ser� gerado o acerto
				For nJ := 1 To oModel:GetModel("G4CA_ITENS"):Length()
					oModel:GetModel("G4CA_ITENS"):GoLine( nJ )

					If  oModel:GetValue("G4CA_ITENS", "G4C_STATUS" ) == '4'	//1-Ag. Libera��o; 2-Ag. Apura��o; 3-Liberado; 4-Finalizado
						lAcerto := .T.
						Exit
					EndIf
					
				Next

				//Verifica se o cliente j� foi cobrado em processos encerrados fora da concilia��o
				For nJ := 1 To oModel:GetModel("G4CAF_ITENS"):Length()
					oModel:GetModel("G4CAF_ITENS"):GoLine( nJ )

					If  oModel:GetValue("G4CAF_ITENS", "G4C_STATUS" ) == '4' .And. Empty(oModel:GetValue("G4CAF_ITENS", "G4C_CODAPU" ))	//1-Ag. Libera��o; 2-Ag. Apura��o; 3-Liberado; 4-Finalizado
						lAcerto := .T.
						Exit
					EndIf
					
				Next

				//Para gerar acerto:
				// [1] - � necess�rio (Itens financeiros com status '3|4')
				// [2] - For�ando a gerar acerto de todos os itens (lAll == .T.)
				// [3] - Para for�ar a gera��o do acerto, o DR n�o pode ser originado pela parcialidade
				If !lAcerto
					//N�o gera acerto para um item com origem em parcialidade sem a devida necessidade.
					If lAll
						If !Empty( oModel:GetValue( "G3R_ITENS", "G3R_SEQPRC" ) )
							Loop
						EndIf
					Else
						Loop
					EndIf
				EndIf

				//Se houve altera��o nos itens, mas os valores permaneceram os mesmos, n�o � necess�rio gerar registros de acordo
				If lAcerto .And. oModel:GetValue( "G3R_ITENS", "G3R_VLFIM" ) <> oModel:GetValue( "G3R_ITENS", "XBK_VLFIM" )
					If GetAltTarifa()
						TA042AltAc( oModel, "G44_ITENS", aG44Acerto, aFieldsG44 )
					Endif

					If GetAltTaxas()
						TA042AltAc( oModel, "G46_ITENS", aG46Acerto, aFieldsG46 )
					EndIf

					If GetAltExtras()
						TA042AltAc( oModel, "G47_ITENS", aG47Acerto, aFieldsG47 )
					EndIf

					If GetAltImpostos()
						TA042AltAc( oModel, "G49_ITENS", aG49Acerto, aFieldsG49 )
					EndIf

					If len( aG44Acerto ) + len( aG46Acerto ) + len( aG47Acerto ) + len( aG49Acerto ) > 0
						aCopyValues	:=	TA042GetVls( "G3R_ITENS", .F., {"G4CA_ITENS","G4CAF_ITENS","G4CB_ITENS"} ) //Realiza a c�pia de todos os registros

			 			//Obtem a sequ�ncia que ir� originar o Acerto
						cSeqAcerto := oModel:GetValue( "G3R_ITENS", "G3R_NUMSEQ" )

			 			//Atualiza os valores originais
						TA042AcOri( oModel, "G44_ITENS", aG44Acerto )
						TA042AcOri( oModel, "G46_ITENS", aG46Acerto )
						TA042AcOri( oModel, "G47_ITENS", aG47Acerto )
						TA042AcOri( oModel, "G49_ITENS", aG49Acerto )

			 			//Altera o Documento de reserva para n�o permitir novas altera��es
						oModel:SetValue( "G3R_ITENS", "G3R_CONALT", "X" )

						If TA042SetVs( aCopyValues )

							cSeqG3Q := TA042MaxSq( oModel )
							TA042SetRec( "G3R_ITENS", {{"_NUMSEQ",cSeqG3Q}}, , .F.  )

			 				//Altera o status do documento de reserva e item de venda
							oModel:SetValue( "G3R_ITENS", "G3R_ACERTO", "1" )
							oModel:SetValue( "G3R_ITENS", "G3R_STATUS", "1" )
							oModel:SetValue( "G3R_ITENS", "G3R_ACRAUT", "1" )
							oModel:SetValue( "G3Q_ITENS", "G3Q_STATUS", "1" )
							oModel:SetValue( "G3Q_ITENS", "G3Q_ACERTO", "1" )

							oModel:SetValue( "G3Q_ITENS", "G3Q_IVORIG", oModel:GetValue( "G3Q_ITENS", "G3Q_IDITEM" ) )
							oModel:SetValue( "G3Q_ITENS", "G3Q_SEQORI", cSeqG3Q )

							oModel:SetValue( "G3R_ITENS", "G3R_SEQACR", cSeqAcerto )
							oModel:SetValue( "G3R_ITENS", "G3R_DTINC", dDataBase )

			 				//Aplica os valores dos acertos e exclui linhas que n�o sofreram altera��es
							TA042GerAc( oModel, "G44_ITENS", aG44Acerto, aFieldsG44 )
							TA042GerAc( oModel, "G46_ITENS", aG46Acerto, aFieldsG46 )
							TA042GerAc( oModel, "G47_ITENS", aG47Acerto, aFieldsG47 )
							TA042GerAc( oModel, "G49_ITENS", aG49Acerto, aFieldsG49 )

							//Atualiza os valores do DR
							Ta34VlrDia(,'G44_TARBAS',)
							TA042AtuAc( oModel )

							//Mantem apenas acordos de % abertos sem apura��o
							TA042DlAcF( oModel, .T., ,.T.)

							//Recalcula os acordos de fornecedores
							If !oModel:GetModel("G48B_ITENS"):IsEmpty()
								T34CalcAco( , , "2", "0", .T.)
							EndIf

							If !oModel:GetModel("G48A_ITENS"):IsEmpty()
								T34CalcAco( , , "1", "0", .T.)
							EndIf

							//Por se tratar do registro do acerto, sempre s�o refeitos os itens financeiros do cliente e fornecedor
							T34AtuDmFi(oModel)
							If !Tur34ItFin(oModel, '3', .T., '3', , .T.)
								oMdlG3R:GoLine(nLinSave)
								oMdlG3R:SetValue('G3R_DIVERG', '1')	
								TA042AcGrd(	oModel, .T. )
								SetAllDelete(.T.)	
								TA42DelSons( oModel, 'G3R_ITENS', .F. )
								SetAllDelete(.F.)	
								TA042AcGrd(	oModel, .F. )
								Help( , , "TA042ACERTO", , I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
							Else
								If !IsBlind()
									oView := FwViewActive()
									oView:GetViewObj("G4CA_ITENS")[3]:Refresh(.T.,.F.)
									oView:GetViewObj("G4CB_ITENS")[3]:Refresh(.T.,.F.)
								EndIf
								
								If !( SuperGetMV( 'MV_TURALAC',,.F.) )
									oModel:SetValue( "G3R_ITENS", "G3R_CONALT", "X" ) //Altera o Documento de reserva para n�o permitir novas altera��es
								EndIf
							EndIf
						EndIf
					EndIf
				EndIf
			Next
		EndIf
	Else

		//Obtem a sequ�ncia que ir� originar o Acerto
		cNumRV	   := oModel:GetValue( "G3R_ITENS", "G3R_NUMID" )
		cIdRV	   := oModel:GetValue( "G3R_ITENS", "G3R_IDITEM" )
		cSeqAcerto := oModel:GetValue( "G3R_ITENS", "G3R_NUMSEQ" )

		oModel:SetValue( "G3R_ITENS", "G3R_CONALT", "" )

		For nX := 1 To oModel:GetModel( "G3R_ITENS" ):Length()
			oModel:GetModel( "G3R_ITENS" ):GoLine( nX )

			If oMdlG3R:GetValue("G3R_NUMID" ) == cNumRV .And. ;
			   oMdlG3R:GetValue("G3R_IDITEM" ) == cIdRV .And. ;
			   oMdlG3R:GetValue("G3R_SEQACR" ) == cSeqAcerto

				oMdlG3R:DeleteLine()
			EndIf
		Next

	EndIf

	cFilAnt := cBkpFilAnt

	TA042AcGrd( oModel, .F.  )
	oMdlG3R:GoLine(nLinSave)
	FwRestRows( aLines )

	TURXNIL(aLines)
	TURXNIL(aCopyValues)
	TURXNIL(aG44Acerto)
	TURXNIL(aG46Acerto)
	TURXNIL(aG47Acerto)
	TURXNIL(aG49Acerto)
	TURXNIL(aFieldsG44)
	TURXNIL(aFieldsG46)
	TURXNIL(aFieldsG47)
	TURXNIL(aFieldsG49)
Return

/*/{Protheus.doc} TA042AltAc
Fun��o para verificar quais registros tiveram altera��es e retornar vetor com campos alterados
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado do ModelDef
@param cIdModel, character, Id do modelo a ser aplicado os acertos
@param aAcerto, array, Vetor com que ser� preenchido com os valores alterados
@param aFieldComp, array, Vetor com campos para compara��o dos valores originais com valores alterados
/*/
Static Function TA042AltAc( oModel, cIdModel, aAcerto, aFieldComp )
	Local nX			:= 0
	Local nY 			:= 0
	Local nPos			:= 0
	Local oModelAlt 	:= oModel:GetModel(cIdModel)

	If !oModelAlt:IsEmpty()
		For nX := 1 to oModelAlt:Length()
			oModelAlt:GoLine( nX )
	
			If !oModelAlt:IsDeleted()
				For nY := 1 to Len( aFieldComp )
					If oModelAlt:GetValue( aFieldComp[nY][1] ) <> oModelAlt:GetValue( aFieldComp[nY][2] )  .And. oModelAlt:GetValue( aFieldComp[nY][3] )
						nPos := aScan( aAcerto, {|x| x[1] == oModelAlt:GetLine() } )
	
						If nPos == 0
							aAdd( aAcerto, {oModelAlt:GetLine(), .F., { } } )
							nPos := len( aAcerto )
						EndIf
	
						aAdd( aAcerto[nPos][3],{aFieldComp[nY][1], oModelAlt:GetValue( aFieldComp[nY][2] ), oModelAlt:GetValue( aFieldComp[nY][1] ) - oModelAlt:GetValue( aFieldComp[nY][2] ) } )
					EndIf
				Next
			EndIf
		Next
	EndIf
Return

/*/{Protheus.doc} TA042AcOri
Fun��o respons�vel por retornar os valores originais da linha que foi realizado o acerto
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado da ModelDef
@param cIdModel, character, Id do modelo que ir� ser restaurado
@param aValues, array, Vetor com valores originais a serem restaurados (obtido na rotina TA042Acert)
/*/
Static Function TA042AcOri( oModel, cIdModel, aValues )
	Local nY := 0
	Local nJ := 0

	//Retorna os valores originais do registro alterado
	For nY := 1 To len( aValues )
		If !( aValues[nY][2] ) .And. len( aValues[nY][3] ) > 0
			oModel:GetModel(cIdModel):GoLine( aValues[nY][1] )

			For nJ := 1 to len(aValues[nY][3])
				oModel:SetValue( cIdModel, aValues[nY][3][nJ][1], aValues[nY][3][nJ][2] )
			Next
		EndIf
	Next

Return

/*/{Protheus.doc} TA042GerAc
Fun��o para aplicar os valores de acertos pertinentes e excluir submodelos que n�o sofreram altera��es
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado do ModelDef
@param cIdModel, character, Id do modelo a ser aplicado os acertos
@param aAcerto, array, Vetor com valores de acertos a ser aplicado (obtido na rotina TA042Acert)
@param aFieldComp, array, Vetor com campos para compara��o dos valores originais com valores alterados
/*/
Static Function TA042GerAc( oModel, cIdModel, aValues, aFields )
	Local nY 		:= 0
	Local nJ 		:= 0
	Local nPosLine	:= 0

	For nY := 1 To oModel:GetModel(cIdModel):Length()
		oModel:GetModel(cIdModel):GoLine( nY )
		nPosLine := aScan( aValues, {|x| x[1] == nY }  )

		If nPosLine == 0 //Linha sem altera��o, ent�o deleta
			oModel:GetModel(cIdModel):DeleteLine()
			If cIdModel == 'G3R_ITENS'
				TDRCacheDel(oModel:GetValue('G3R_ITENS', "G3R_FILIAL") + oModel:GetValue('G3R_ITENS', "G3R_NUMID") + oModel:GetValue('G3R_ITENS', "G3R_IDITEM") + oModel:GetValue('G3R_ITENS', "G3R_NUMSEQ"))
			EndIf
		Else

			For nJ := 1 to len( aValues[nPosLine][3] )
				oModel:SetValue( cIdModel, aValues[nPosLine][3][nJ][1], 0 )
			Next

			For nJ := 1 to len(aValues[nPosLine][3])
				oModel:SetValue( cIdModel, aValues[nPosLine][3][nJ][1], aValues[nPosLine][3][nJ][3] )
			Next

		EndIf

	Next

Return

/*/{Protheus.doc} TA042AcDel
Fun��o respons�vel por deletar os acordos aplicados dos registros de acerto, mantendo apenas os acordos de repasse
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado do ModelDef
/*/
Static Function TA042AcDel( oModel )
	Local aAcordoRp	:= {}
	Local cCodAco		:= ""
	Local nX			:= 0

	SetAllDelete(.T.)

	For nX := 1 To oModel:GetModel( "G48A_ITENS" ):Length()
		oModel:GetModel( "G48A_ITENS" ):GoLine( nX )

		If Empty( oModel:GetValue( "G48A_ITENS", "G48_ACDBAS" ) )
			oModel:GetModel( "G48A_ITENS" ):DeleteLine()
		Else
			aAdd( aAcordoRp, oModel:GetValue( "G48A_ITENS", "G48_ACDBAS" ) )
		EndIf

	Next

	For nX := 1 To oModel:GetModel( "G48B_ITENS" ):Length()
		oModel:GetModel( "G48B_ITENS" ):GoLine( nX )

		cCodAco := oModel:GetValue( "G48B_ITENS", "G48_CODACD" )
		If aScan( aAcordoRp, {|x| x == cCodAco } ) == 0
			oModel:GetModel( "G48B_ITENS" ):DeleteLine()
		EndIf

	Next

	SetAllDelete(.F.)
	TURXNIL(aAcordoRp)
Return

/*/{Protheus.doc} MenuDef
Menu Funcional
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function MenuDef()
	Local aRotina 	:= {}

	ADD OPTION aRotina Title STR0049 	Action "VIEWDEF.TURA042A" 	 OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina Title STR0050	Action "TA042Concil(3,'1')"	 OPERATION 3 ACCESS 0 //"Incluir"
	ADD OPTION aRotina Title STR0051 	Action "TA042Concil(4,'1')"	 OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina Title STR0052 	Action "VIEWDEF.TURA042A" 	 OPERATION 5 ACCESS 0 //"Excluir"
	ADD OPTION aRotina Title STR0053	Action "TuraEfetiv('1','1')" OPERATION 4 ACCESS 0 //"Efetivar" 
	ADD OPTION aRotina Title STR0054	Action "TuraEfetiv('2','1')" OPERATION 4 ACCESS 0 //"Desfaz Efetivar" 
	ADD OPTION aRotina Title STR0055	Action "TURA058"			 OPERATION 4 ACCESS 0 //"Antecipa��o"
	ADD OPTION aRotina Title STR0056	Action "FwMsgRun( , {|| TA042DemFi(1)}, , '" + STR0164 + "')"		 OPERATION 4 ACCESS 0 //"Dem. Financ. Agencia"	"Imprimindo o Demonstrativo Ag�ncia, aguarde..."
	ADD OPTION aRotina Title STR0132	Action "FwMsgRun( , {|| TA042DemFi(2)}, , '" + STR0163 + "')"		 OPERATION 4 ACCESS 0 //"Dem. Financ. Cliente"	"Imprimindo o Demonstrativo Cliente, aguarde..."
	ADD OPTION aRotina Title STR0057 	Action "TA042VisFi"			 OPERATION 4 ACCESS 0 //"Vis�o Finan."
	ADD OPTION aRotina Title STR0098 	Action "VIEWDEF.TURA042G" 	 OPERATION 2 ACCESS 0 //"Log Efetiva��o"

Return aRotina

/*/{Protheus.doc} TA042APesq
Fun��o de chamada da tela de pesquisa documentos de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042APesq()
	Local oView	   := FwViewActive()
	Local aButtons := {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0058},{.T.,STR0059},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Confirmar"###"Cancelar"

	oTA042Model	:= FwModelActive()

	If 	oTA042Model:GetOperation() <> MODEL_OPERATION_INSERT .And. oTA042Model:GetOperation() <> MODEL_OPERATION_UPDATE
		Help(,,"TA042Pesq",,STR0095,1,0) //"Op��o dispon�vel somente para INCLUS�O ou ALTERA��O."
		Return
	EndIf

	TA042AcGrd(	oTA042Model, .T. )

	If FWExecView(STR0060,"VIEWDEF.TURA042F",3,,{|| .T.},,,aButtons) == 0 //"Pesquisa de Documentos de Reserva"
		TA042StsIF( oTA042Model )
	EndIf

	FwModelActive( oTA042Model )
	oTA042Model:GetModel("G3R_ITENS"):GoLine(1)
	oView:Refresh()

	TA042AcGrd(	oTA042Model, .F. )

	TURXNIL(aButtons)
Return

/*/{Protheus.doc} TA042AModel
Fun��o para retorno do objeto oModel (vari�vel est�tica) do fonte atual
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042AModel()
Return oTA042Model

/*/{Protheus.doc} TA042ChLin
Fun��o executada a cada troca de linha da view referente a G3R
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oView, objeto, Objeto instanciado da ViewDef
@param cViewID, character, Id da View que originou a chamada
/*/
Static Function TA042ChLin( oView, cViewID )

	//Caso a pasta selecionada seja a passageiros, atualiza a pasta referente a tipo de passageiros
	If oView:GetFolderActive( "FOLDER_IT_DR", 2 )[1] == 3
		TA042VlFol( "FOLDER_IT_DR", , 3 )
	EndIf

	// fun��o para atualizar os campos totalizadores de DR
	CalcTotDr( 'CHANGE' )
Return

/*/{Protheus.doc} TA042AtuDR
Fun��o para atualizar documentos de reserva, se necess�rio gera parcialidade, acertos e recalculo dos acordos aplicados
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042AtuDR( lAll )
	Local aLines     := {}
	Local cBkpFilAnt := cFilAnt
	Local nX         := 0
	local nY         := 0
	Local oModel     := FwModelActive()
	Local oMdlG3R    := oModel:GetModel('G3R_ITENS')
	Local nLine      := oMdlG3R:GetLine()
	Local lParc      := .F.

	aLines	:= FwSaveRows( oModel )

	DEFAULT lAll		:= .F.

	SetAllDelete(.T.)

	If (oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE) .And. ;
	   (GetAltTarifa() .Or. GetAltTaxas() .Or. GetAltExtras() .Or. GetAltImpostos() .Or. GetAltSeg() .Or. GetAplAcordFin())
		If IsBlind() .Or. ApMsgYesNo( STR0061 ) //"Deseja atualizar os DR's e gerar acertos e parcialidades pendentes?"

			TA042AcGrd( oModel, .T.  )

			If lAll
				//Verifica se houve parcionalidade dos segmentos
				lParc := TA042VlPrc()

				//Verifica se houve altera��o
				Ta042DvVlr()
			EndIf

			//Reaplica os acordos de fornecedores
			For nX := 1 To oMdlG3R:Length()
				oMdlG3R:GoLine(nX)
				cFilAnt :=  oMdlG3R:GetValue("G3R_MSFIL")

				If !oMdlG3R:IsDeleted()

					If !oModel:GetModel("G48B_ITENS"):IsEmpty()
						T34CalcAco( , , "2", "0", .T.)
					EndIf

					If !oModel:GetModel("G48A_ITENS"):IsEmpty()
						T34CalcAco( , , "1", "0", .T.)
					EndIf

				EndIf

				//Refaz os itens financeiros
				oModel:GetModel("G48A_ITENS"):GoLine(1)
				oModel:GetModel("G48B_ITENS"):GoLine(1)

				If oMdlG3R:GetValue('G3R_STATUS') == '1'
					//Atualiza Demonstrativo Financeiro
					T34AtuDmFi(oModel)
					If !Tur34ItFin(oModel, '3', .T., IIF(lParc, '3', oModel:GetModel("G3Q_ITENS"):GetValue("G3Q_ATUIFA")), , .T.)
						oMdlG3R:GoLine(nLine)
						oMdlG3R:SetValue('G3R_DIVERG', '1')
						TA042AcGrd(	oModel, .T. )	
						SetAllDelete(.T.)	
						TA42DelSons( oModel, 'G3R_ITENS', .F. )
						SetAllDelete(.F.)	
						TA042AcGrd(	oModel, .F. )
						Help( , , "TA042ACERTO", , I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
					Else
						If !IsBlind()
							oView := FwViewActive()
							oView:GetViewObj("G4CA_ITENS")[3]:Refresh(.T.,.F.)
							oView:GetViewObj("G4CB_ITENS")[3]:Refresh(.T.,.F.)
						EndIf
						TA042StsIF( oModel )
					EndIf
				EndIf
			Next

			oModel:SetValue( 'G8C_MASTER', 'G8C_FATURA', oModel:GetValue( 'G8C_MASTER', 'G8C_FATURA' ) )
			TA042AcGrd( oModel, .F.  )
			oMdlG3R:GoLine(nLine)
			CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .T./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)

			SetAltTarifa(.F.)
			SetAltTaxas(.F.)
			SetAltExtras(.F.)
			SetAltImpostos(.F.)
			SetAltSeg(.F.)
			SetAplAcordFin(.F.)
		EndIf
	EndIf

	SetAllDelete(.F.)
	cFilAnt := cBkpFilAnt
	FwRestRows( aLines )

	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042TrRec
Fun��o para atualizar o status dos itens financeiros do fornecedor, se ele est� marcado para ser conciliado ou n�o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042TrRec()
	Local aLines	 := FwSaveRows()
	Local nX		 := 0
	Local oModel 	 := FwModelActive()
	Local oModelG3R  := oModel:GetModel("G3R_ITENS")
	Local oModelG4CB := oModel:GetModel("G4CB_ITENS")
	Local oModelG48B := oModel:GetModel("G48B_ITENS")
	Local cAplicacao := oModelG48B:GetValue("G48_APLICA")
	Local cStatus	 := IIF( oModelG48B:GetValue("G48_RECCON"), "1", "2")

	TA042AcGrd(oModel, .T.)

	//Se o item j� est� em apura��o, ser� sempre desmarcado
	If !Empty( oModelG48B:GetValue('G48_CODAPU'))
		oModelG48B:SetValue("G48_RECCON", .F.)
		cStatus := '2'

	//Se acordo manual e acerto autom�tico, for�a a ser sempre marcado
	ElseIf oModelG48B:GetValue('G48_INTERV') == '1' .And. oModelG3R:GetValue('G3R_ACRAUT') == '1'
		oModelG48B:SetValue("G48_RECCON", .T.)
		cStatus := '1'
	EndIf

	oModelG48B:SetValue( "G48_COMSER", cStatus )
	If oModel:GetValue( 'G3R_ITENS', 'G3R_ACERTO' ) == '1' .And. oModel:GetValue('G3R_ITENS', 'G3R_ACRAUT') == '1'
		If oModelG4CB:SeekLine({{'G4C_APLICA', oModelG48B:GetValue("G48_APLICA")}})
			oModelG4CB:SetValue("G4C_TPFOP", IIF(cStatus == '1', '1', '2'))
		EndIf
	EndIf

	If oModelG4CB:SeekLine({{"G4C_APLICA", cAplicacao}})
		If oModelG4CB:GetValue( "G4C_STATUS" ) $ '1|2'
			oModelG4CB:SetValue( "G4C_STATUS", cStatus )
		EndIf
	EndIf

	TA042AcGrd(oModel, .F.)

	FwRestRows(aLines)

	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042StsIF
Refaz os status dos itens financeiros de acordo se este est� selecionado para conciliar ou n�o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042StsIF( oModel, cNumSeq )
	Local aLines 	 := FwSaveRows()
	Local nX		 := 1
	Local nY		 := 1
	Local oModelG3R  := oModel:GetModel("G3R_ITENS")
	Local oModelG48B := oModel:GetModel("G48B_ITENS")
	
	Default cNumSeq := ""
	
	For nX := 1 to oModelG3R:Length()
		oModelG3R:GoLine( nX )
	
		If !oModelG48B:IsEmpty() .And. ((Empty(cNumSeq) .Or. oModelG3R:GetValue('G3R_NUMSEQ') == cNumSeq .Or. oModelG3R:GetValue('G3R_SEQPRC') == cNumSeq) .And. oModelG3R:GetValue('G3R_STATUS') == '1')
			For nY := 1 to oModelG48B:Length()
				oModelG48B:GoLine(nY)
				If !oModelG48B:IsDeleted() .And. !Empty(oModelG48B:GetValue("G48_CODACD"))
					TA042TrRec()
				EndIf
			Next
		EndIf
	Next
	
	FwRestRows( aLines )

	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042AVlAct
Valida��es para opera��es na concilia��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado da ModelDef
/*/
Static Function TA042AVlAct( oModel )
	Local cMessage	:= ""
	Local lRet := .T.

	If oModel:GetOperation() == MODEL_OPERATION_UPDATE .Or. oModel:GetOperation() == MODEL_OPERATION_DELETE
		If G8C->G8C_STATUS == "1" .And. !( FWIsInCallStack( "TA042AEFET" ) .Or. FWIsInCallStack( "TA042ADEFE" ) )
			lRet := .F.
			Help(,,"TA042UPD",,STR0062,1,0)	 //"Concilia��o j� efetivada, estorne a efetiva��o para alterar ou excluir."
		EndIf
		
		If oModel:GetOperation() == MODEL_OPERATION_DELETE
			If !T042VlItPr(oModel, @cMessage) 
				FwAlertWarning( STR0153 + CRLF + cMessage, STR0155 ) //"Aten��o"
				lRet := .F.
			EndIf	
		EndIf	
		
	EndIf

Return lRet

/*/{Protheus.doc} TA042VlFat
Fun��o para validar o n�mero da fatura informada.
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042VlFat()
	Local aArea		:= GetArea()
	Local aAreaG8C	:= G8C->( GetArea() )
	Local lRet 		:= .T.
	Local oModel    := FwModelActive()

	If !FwIsInCallStack('TA042FillG3R') .And. oModel:GetOperation() == MODEL_OPERATION_INSERT
		G8C->( dbSetOrder(2) )
		If G8C->( dbSeek(xFilial("G8C") + FWFldGet("G8C_FORNEC") + FWFldGet("G8C_LOJA") + FWFldGet("G8C_FATURA")))
			lRet := .F.
	
			If IsBlind()
				Help(,,"TA042VLFAT",,STR0063,1,0)		 //"Fatura j� cadastrada para este fornecedor."
			Else
				If ApMsgYesNo( STR0064 ) //"Fatura j� cadastrada para este fornecedor. Deseja acessar a Vis�o Financeira?"
					FWExecView(STR0060, "VIEWDEF.TURA042D", 4, , {|| .T.}) //"Pesquisa de Documentos de Reserva"
				EndIf
			EndIf
		EndIf
	EndIf
	
	RestArea(aAreaG8C)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaG8C)
Return lRet

/*/{Protheus.doc} TA042VlFor
Valida��o se � poss�vel alterar o fornecedor da fatura
@type function
@author Anderson Toledo
@since 05/04/2016
@version 1.0
@param cOpc, character, Tipo de valida��o '1' - Fornecedor; '2' - Loja
/*/
Function TA042VlFor( cOpc )
	Local aArea     := GetArea()
	Local aLines	:= {}
	Local lRet 		:= .T.
	Local lAltFor	:= .F.
	Local nX		:= 0
	Local oModel	:= FwModelActive()

	aLines := FwSaveRows( oModel )

	//Verifica se existem registros ativos na G3R
	If oModel:GetModel( "G3R_ITENS" ):Length( .T. ) > 0 .And. (oModel:GetModel( "G3R_ITENS" ):Length() == 1 .And. !oModel:GetModel( "G3R_ITENS" ):IsEmpty())
		If cOpc == '1'
			For nX := 1 to oModel:GetModel( "G3R_ITENS" ):Length( )
				oModel:GetModel( "G3R_ITENS" ):GoLine( nX )

				If !oModel:GetModel( "G3R_ITENS" ):IsDeleted( ) .And. !Empty( oModel:GetValue( "G3R_ITENS", "G3R_FORREP" ) ) .And. oModel:GetValue( "G3R_ITENS", "G3R_FORREP" ) <> oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" )
					lAltFor := .T.
					Exit
				EndIf
			Next
		Else
			For nX := 1 to oModel:GetModel( "G3R_ITENS" ):Length( )
				oModel:GetModel( "G3R_ITENS" ):GoLine( nX )

				If !oModel:GetModel( "G3R_ITENS" ):IsDeleted( ) .And. !Empty( oModel:GetValue( "G3R_ITENS", "G3R_LOJREP" ) ) .And. oModel:GetValue( "G3R_ITENS", "G3R_LOJREP" ) <> oModel:GetValue( "G8C_MASTER", "G8C_LOJA" )
					lAltFor := .T.
					Exit
				EndIf
			Next
		EndIf

		If lAltFor
			If ( IsBlind() .Or. FwAlertYesNo( STR0096 ) ) //"Na troca de fornecedor todos os Documentos de Reserva ser�o exclu�dos da concilia��o, deseja prosseguir?"
				TA042AcGrd( oModel, .T. )
				SetAllDelete(.T.)
				TA42DelSons( oModel, 'G3R_ITENS', .F. )
				SetAllDelete(.F.)
				TA042AcGrd( oModel, .F. )
				CALCVLG8C(oModel:GetModel( "G3R_ITENS" ), .T./*lRecalc*/, .T./*lAllLines*/, /*ChaveTDR*/, "UPDLIN"/*cMomento*/)
			Else
				lRet := .F.
			EndIf
		EndIf
	ElseIf Empty(FwFldGet("G8C_FORNEC"))
		lRet := .T.
	
	ElseIf Empty(FwFldGet("G8C_LOJA")) 
		DbSelectArea("SA2")
		SA2->(DbSetOrder(1))	// A2_FILIAL+A2_CO D+A2_LOJA
		If SA2->(DbSeek(xFilial("SA2") + FwFldGet("G8C_FORNEC")))
			lRet := .T.
		Else
			lRet := .F.
			oModel:SetErrorMessage("G8C_MASTER", "G8C_FORNEC", "G8C_MASTER", "G8C_FORNEC", "TA042VlFor", STR0191)		// "Fornecedor inv�lido."
		EndIf
		SA2->(DbCloseArea())

	Else
		DbSelectArea("SA2")
		SA2->(DbSetOrder(1))	// A2_FILIAL+A2_COD+A2_LOJA
		If SA2->(DbSeek(xFilial("SA2") + FwFldGet("G8C_FORNEC") + FwFldGet("G8C_LOJA")))
			If FieldPos("A2_MSBLQL") > 0
				If SA2->A2_MSBLQL == "1"
					oModel:SetErrorMessage("G8C_MASTER", "G8C_FORNEC", "G8C_MASTER", "G8C_FORNEC", "TA042VlFor", STR0192)		// "Fornecedor bloqueado para uso."
					lRet := .F.
				EndIf
			EndIf
		Else
			lRet := .F.
			oModel:SetErrorMessage("G8C_MASTER", "G8C_LOJA", "G8C_MASTER", "G8C_LOJA", "TA042VlFor", STR0193)		// "Loja inv�lida."
		EndIf
		SA2->(DbCloseArea())
	EndIf

	FwRestRows( aLines )
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aLines)
Return lRet

/*/{Protheus.doc} TA042VlPrc
Fun��o para validar a necessidade de gerar parcialidade
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042VlPrc()
	Local aLines	:= FWSaveRows()
	Local aLinesParc:= {}				//Armazena as linhas que tiveram altera��o nas datas de reserva para gerar a parcialidade
	Local cSegmento	:= ""
	Local cIdTable	:= ""
	Local cFldDtIni	:= ""
	Local cFldDtFim	:= ""
	Local nX		:= 0
	Local lAll		:= .F.
	Local lParc 	:= .F.
	Local oModel	:= FwModelActive()
	Local oModelG3R	:= oModel:GetModel("G3R_ITENS")

	//Indica se houve altera��o no per�odo de reserva
	If	GetAltSeg()

		If !IsBlind() .And. ( Empty( SuperGetMV( 'MV_TURPRNF',,'0') ) .Or. SuperGetMV( 'MV_TURPRNF',,'0') == '0' )
			lAll := Aviso(STR0114, STR0115, { STR0116, STR0117 }, 1) == 1 //"Gera��o de parcialidade"###"Deseja gerar parcialidade para todas altera��es, ou somente para DR's com itens finalizados?"###"Todos"###"Finalizados"
		ElseIf SuperGetMV( 'MV_TURPRNF',,'0' ) == '1'
			lAll := .F.
		Else	
		 	lAll := .T.
		EndIf

		For  nX := 1 to oModelG3R:Length()
			oModelG3R:GoLine( nX )

			If oModelG3R:IsDeleted() .Or. !Empty( oModelG3R:GetValue( "G3R_SEQNSH" ) )
				Loop
			EndIf

			cSegmento := oModelG3R:GetValue( "G3R_TPSEG" )

			//Apenas os segmentos listados abaixo possuem parcialidade
			If !TA042SegPr( cSegmento, '1' )
				Loop
			EndIf

			//Obtem o ID do modelo e os campos de acordo com o segmento
			TA042FdSeg( cSegmento, @cIdTable, @cFldDtIni, @cFldDtFim  )

			If oModelG3R:GetValue( "G3R_STATUS" ) <> "2" ;
					.And. ( !Empty(oModel:GetValue( cIdTable + "_ITENS", "XXX" + cFldDtIni ) ) .OR. !Empty(oModel:GetValue(cIdTable + "_ITENS", "XXX" + cFldDtFim) ) ) ;
					.And. ( oModel:GetValue(cIdTable + "_ITENS",cIdTable + cFldDtIni) <> oModel:GetValue(cIdTable + "_ITENS", "XXX" + cFldDtIni) ;
					.OR. oModel:GetValue(cIdTable + "_ITENS", cIdTable + cFldDtFim) <> oModel:GetValue(cIdTable + "_ITENS", "XXX" + cFldDtFim ) )

				aAdd( aLinesParc, nX )

			EndIf

		Next

		For nX := 1 to len(aLinesParc)
			oModelG3R:GoLine( aLinesParc[nX] )
			TA042Parc( "1", lAll )
			lParc := .T.
		Next

	EndIf

	SetAltSeg(.F.)
	FwRestRows( aLines )

	TURXNIL(aLines)
	TURXNIL(aLinesParc)
Return lParc

/*/{Protheus.doc} Ta042DvVlr
Fun��o para validar a necessidade de gera��o de acerto
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function Ta042DvVlr()
	Local lRet := .F.
	Local lAll := .F.

	If 	GetAltTarifa() .Or. GetAltTaxas() .Or. GetAltExtras() .Or. GetAltImpostos()
		If !IsBlind() .And. SuperGetMV( 'MV_TURACNF',,.F. )
			lAll := Aviso(STR0118,STR0119, { STR0116, STR0117 }, 1) == 1 //"Gera��o de acerto"###"Deseja gerar acerto para todas altera��es, ou somente para DR's com itens finalizados?"###"Todos"###"Finalizados"
		EndIf

		lRet := TA042Acert( FwModelActive(), "1", lAll )
	EndIf

Return lRet

/*/{Protheus.doc} TA042VlFol
Fun��o para na view apresentar somente a aba do segmento selecionado na G3R
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042VlFol( cFolderID, nOldSheet, nSelSheet )
	Local cTpPass	:= ""
	Local oView 	:= FwViewActive()
	Local oModel 	:= FwModelActive()

	If AllTrim(cFolderID) == "FOLDER_IT_DR"
		TA042HiFol( oView )

		If nSelSheet == 3
			cTpPass := AllTrim( oModel:GetValue("G3R_ITENS","G3R_TPSEG") )

			If Val(cTpPass) > 0
				oView:SelectFolder("FOLDER_SEG", Val(cTpPass), 2)
			Else
				//Verifica posi��o da folder pelo c�digo ASCII - 55, resultando em A = 10, B = 11, C = 12...
				oView:SelectFolder("FOLDER_SEG", Asc( Upper( cTpPass ) ) - 55, 2)
			EndIf
		EndIf

	EndIf

Return

/*/{Protheus.doc} TA042HiFol
Fun��o para esconder todas as abas de segmentos
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042HiFol( oView )
	oView:HideFolder("FOLDER_SEG", 1, 2) //A�reo
	oView:HideFolder("FOLDER_SEG", 2, 2) //Hotel
	oView:HideFolder("FOLDER_SEG", 3, 2) //Carro
	oView:HideFolder("FOLDER_SEG", 4, 2) //Rodovi�rio
	oView:HideFolder("FOLDER_SEG", 5, 2) //Cruzeiro
	oView:HideFolder("FOLDER_SEG", 6, 2) //Trem
	oView:HideFolder("FOLDER_SEG", 7, 2) //Visto
	oView:HideFolder("FOLDER_SEG", 8, 2) //Seguro
	oView:HideFolder("FOLDER_SEG", 9, 2) //Tour
	oView:HideFolder("FOLDER_SEG", 10, 2) //Pacote
	oView:HideFolder("FOLDER_SEG", 11, 2) //Outros
Return

/*/{Protheus.doc} TA042AcGrd
Fun��o para determinar os acessos de inserir/deletar linhas nos grids, a atualiza��o � realizada  atrav�s de pre-valida��o CANSETVALUE
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, objeto do ModelDef instaciado
@param bAcesso, booleano, Indica que o modelo e todos submodelos ser�o liberado para inclus�o, exclus�o e altera��o para rotinas de importa��o de dados
/*/
Function TA042AcGrd( oModel, lAcesso )
	Local aIdGrids 	 := oModel:GetModelIds()
	Local nX 		 := 0
	Local nOperation := oModel:GetOperation()

	If lAcesso
		For nX := 1 To len(aIdGrids)
			If oModel:GetModel(aIdGrids[nX]):ClassName() == "FWFORMGRID"
				oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .F. )
				oModel:GetModel( aIdGrids[nX] ):SetNoUpdateLine( .F. )
				oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .F. )
			EndIf
		Next

	Else
		If nOperation == 3 .Or. nOperation == 4

			For nX := 1 To len(aIdGrids)
				If oModel:GetModel(aIdGrids[nX]):ClassName() == "FWFORMGRID"

					If AllTrim( aIdGrids[nX] ) == "G3R_ITENS"
						oModel:GetModel( "G3R_ITENS" ):SetNoDeleteLine( .F. )
						oModel:GetModel( "G3R_ITENS" ):SetNoInsertLine( .T. )

					ElseIf AllTrim( aIdGrids[nX] ) == "G44_ITENS"
						oModel:GetModel( "G44_ITENS" ):SetNoDeleteLine( .T. )
						oModel:GetModel( "G44_ITENS" ):SetNoInsertLine( .T. )

					ElseIf AllTrim( aIdGrids[nX] ) == "G46_ITENS"
						oModel:GetModel( "G46_ITENS" ):SetNoInsertLine( .F. )

					ElseIf AllTrim( aIdGrids[nX] ) == "G47_ITENS"
						oModel:GetModel( "G47_ITENS" ):SetNoInsertLine( .F. )

					ElseIf AllTrim( aIdGrids[nX] ) = "G49_ITENS"
						oModel:GetModel( "G49_ITENS" ):SetNoInsertLine( .F. )

					ElseIf AllTrim( aIdGrids[nX] ) = "G48B_ITENS"
						oModel:GetModel( "G48B_ITENS" ):SetNoInsertLine( .F. )

					Else
						oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .T. )
						oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .T. )
					
					EndIf
				EndIf
			Next
		Else
			For nX := 1 To len(aIdGrids)
				If oModel:GetModel(aIdGrids[nX]):ClassName() == "FWFORMGRID"
					oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .T. )
					oModel:GetModel( aIdGrids[nX] ):SetNoUpdateLine( .T. )
					oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .T. )
				EndIf
			Next
		EndIf
	EndIf
	
	TURXNIL(aIdGrids)
Return

/*/{Protheus.doc} TA042ACommit
Fun��o para tratamento da grava��o dos dados
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto do ModelDef instanciado
/*/
Static Function TA042ACommit( oModel )

Local lRet 	   := .T.
Local cMsgErro := ""
Local cMsgSolu := ""
Local nX 	   := 0
Local cBkpFil  := cFilAnt

If !FwIsInCallStack("TuraEfetiv")
	If oModel:GetOperation() != MODEL_OPERATION_DELETE
		// validar se tem algum DR no modelo pois deve haver pelo menos UM para ser conciliado

		If !(oModel:GetModel("G3R_ITENS"):Length(.T.) .And. !oModel:GetModel("G3R_ITENS"):IsEmpty())
			lRet := .F.
			cMsgErro := I18N(STR0194, {IIF(oModel:GetOperation() == 3, STR0050, IIF(oModel:GetOperation() == 4, STR0051, IIF(oModel:GetOperation() == 5, STR0052, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
			cMsgSolu := STR0196 	// "A concilia��o precisa ter ao menos um Documento de Reserva."	
		EndIf
	EndIf
		
	// Validando se houve altera��o manual nas tabelas G44, G46, G47, G49 ou 
	// se o per�odo de reserva foi alterado ou 
	// se foi realizada alguma altera��o em acordos
	If lRet .And. (oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE)	
		If GetAltTarifa() .Or. GetAltTaxas() .Or. GetAltExtras() .Or. GetAltImpostos() .Or.	GetAltSeg() .Or. GetAplAcordFin()
			lRet := .F.
			cMsgErro := STR0197	 	// "Foram realizadas altera��es nos Documentos de Reserva."
			cMsgSolu := STR0198		// "Utilize a rotina 'Atualizar DRs' para atualiza��o dos registros."
		EndIf
	EndIf

	If lRet
		BEGIN TRANSACTION
			If (lRet := TA042Coninu(oModel))
				If (lRet := oModel:VldData())
					If !(lRet := FwFormCommit(oModel))
						cMsgErro := I18N(STR0194, {IIF(oModel:GetOperation() == 3, STR0050, IIF(oModel:GetOperation() == 4, STR0051, IIF(oModel:GetOperation() == 5, STR0052, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"	
						cMsgSolu := STR0195		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."
						DisarmTransaction()
						Break
					EndIf
				Else
					cMsgErro := I18N(STR0194, {IIF(oModel:GetOperation() == 3, STR0050, IIF(oModel:GetOperation() == 4, STR0051, IIF(oModel:GetOperation() == 5, STR0052, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"	
					cMsgSolu := STR0195		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."
					DisarmTransaction()
					Break
				EndIf
			Else
				cMsgErro := I18N(STR0194, {IIF(oModel:GetOperation() == 3, STR0050, IIF(oModel:GetOperation() == 4, STR0051, IIF(oModel:GetOperation() == 5, STR0052, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
				cMsgSolu := STR0195		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."
				DisarmTransaction()
				Break
			EndIf
		END TRANSACTION
	EndIf
Else
	If !(lRet := FwFormCommit(oModel))
		cMsgErro := I18N(STR0199, {IIF(FwIsInCallStack("TA042AEfet"), STR0200, STR0201)})		// "Ocorreu um erro ao #1 Concilia��o."		"efetivar a"	"estornar a efetiva��o da" 	
		cMsgSolu := STR0195		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."
	EndIf
EndIf

If !lRet
	oModel:SetErrorMessage(oModel:GetId(), "", oModel:GetId(), "G3R_CONALT", "TA042ACommit", cMsgErro, cMsgSolu)		
EndIf

cFilAnt := cBkpFil

Return lRet

/*/{Protheus.doc} TA042LPre
Fun��o utilizada para pr�-valida��o da edi��o de grids
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042LPre(oModelGrid, nLine, cAction, cField)
	Local aSaveLines 	:= {}
	Local aIdGrids		:= {}	//{ "G3Q_ITENS","G48A_ITENS","G4CA_ITENS","G3S_ITENS","G3T_ITENS","G3U_ITENS","G3V_ITENS","G3W_ITENS","G3Y_ITENS","G3X_ITENS","G42_ITENS","G41_ITENS","G40_ITENS","G3Z_ITENS","G43_ITENS","G44_ITENS","G46_ITENS","G47_ITENS","G48B_ITENS","G49_ITENS","G4CB_ITENS"}
	Local cAux			:= ""
	Local cModelId		:= oModelGrid:GetId()
	Local cTabSeg		:= ""
	Local cFldDtIni		:= ""
	Local cFldDtFim		:= ""
	Local cFldPeriodo	:= ""
	Local cOpcao		:= ""
	Local lRet			:= .T.
	Local nX			:= 0
	Local oModel 		:= oModelGrid:GetModel()
	Local oView         := Nil
	Local oModelG3Q		:= Nil
	Local lLimpa 		:= .T.

	//Se a variavel est�tica lAllDelete, deleta sem valida��o individual
	If GetAllDelete() .And. ( cAction == "DELETE" .Or. cAction == "UNDELETE" )
		Return .T.
	EndIf

	//N�o permite altera��es e exclus�es de DR gerados via concilia��o
	If cAction $ "CANSETVALUE|SETVALUE|DELETE|UNDELETE"

		Do Case
			Case cModelId == "G3R_ITENS"
				If cAction == "SETVALUE"
					If cField == "G3R_VLFIM" .And. Empty( FwFldGet( "XBK_VLFIM" ) )
						FwFldPut( "XBK_VLFIM", FwFldGet( "G3R_VLFIM" ) )
					EndIf

				ElseIf cAction == "CANSETVALUE" 
					If oModel:GetValue( "G3R_ITENS", "G3R_CONALT" ) == "X"
						oModel:SetErrorMessage(cModelId, "G3R_CONALT", cModelId, "G3R_CONALT", "TA042NOALT", STR0124)  //"N�o � permitido altera��o neste documento de reserva, bloqueado por j� possuir acerto."
						Return .F.
					ElseIf oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
						oModel:SetErrorMessage(cModelId, "G3R_STATUS", cModelId, "G3R_STATUS", "TA042NOALT", STR0125)  //"N�o � permitido altera��o neste documento de reserva, Status � diferente de 'Pendente'."
						Return .F.
					ElseIf cModelId == "G44_ITENS" .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1" .And. !Empty(oModel:GetValue( "G3R_ITENS", "G3R_SEQNSH" ))
						oModel:SetErrorMessage("G44_ITENS", "G44_APLICA", "G44_ITENS", "G44_APLICA", "TA042NOALT", STR0175)  // "N�o � permitido altera��o de tarifa de Item de Venda de NoShow."
						Return .F.
					EndIf
					
					If cField == "G3R_VLFIM" .And. Empty( FwFldGet( "XBK_VLFIM" ) )
						FwFldPut( "XBK_VLFIM", FwFldGet( "G3R_VLFIM" ) )
					EndIf
			
					If cField == "G3R_NATURE"
						SetAplAcordFin(.T.)
					Else
						Return .F.
					EndIf
			
				ElseIf cAction == "DELETE"  
					If oModelGrid:GetValue("G3R_CONCIL") == oModel:GetValue("G8C_MASTER", "G8C_CONCIL") .Or. oModel:GetOperation() == MODEL_OPERATION_INSERT
						// no show
						If oModelGrid:GetValue("G3R_NOSHOW") == "1" .Or. oModelGrid:SeekLine({{"G3R_FILIAL", oModelGrid:GetValue('G3R_FILIAL')}, ;
						                        											  {"G3R_NUMID" , oModelGrid:GetValue('G3R_NUMID' )}, ; 
						                        											  {"G3R_IDITEM", oModelGrid:GetValue('G3R_IDITEM')}, ; 
						                        											  {"G3R_NUMSEQ", oModelGrid:GetValue("G3R_SEQNSH")}}) 
							oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0166)  //"N�o � poss�vel excluir itens de NoShow ou gerados atrav�s deste."
							Return .F.
						EndIf

						// parcialidade
						If oModelGrid:SeekLine({{"G3R_FILIAL", oModelGrid:GetValue('G3R_FILIAL')}, ;
						                        {"G3R_NUMID" , oModelGrid:GetValue('G3R_NUMID' )}, ;
						                        {"G3R_IDITEM", oModelGrid:GetValue('G3R_IDITEM')}, ;
						                        {"G3R_NUMSEQ", oModelGrid:GetValue("G3R_SEQPRC")}}) .Or. oModelGrid:GetValue("G3R_STATUS") $ "2|4"
							oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0167)  //"N�o � poss�vel excluir itens gerados atrav�s de parcialidade."
							Return .F.
						EndIf			
					EndIf 
					
					If !Empty( oModelGrid:GetValue("G3R_SEQACR") ) .And. oModelGrid:GetValue("G3R_ACRAUT") == '1'
						oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0168)  //"N�o � poss�vel excluir itens gerados atrav�s de acerto."
						Return .F.						
		
					ElseIf !GetAllDelete() .And. (oModelGrid:GetValue("G3R_NOSHOW") == "1" .Or. !Empty(oModelGrid:GetValue("G3R_SEQNSH"))) .And. !FWIsInCallStack("TA042NSHOW")
						oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0066)  //"Documento de reserva com NO SHOW, realize o estorno para apagar este registro."
						Return .F.
			
					Else
						aSaveLines := FWSaveRows()
						TA042AcGrd(	oModel, .T. )
						SetAllDelete(.T.)	
			
						//� necess�rio marcar todos os filhos como deletados, somente assim, todos os registros ser�o
						//processados no Commit e a fun��o TA042EstIn executada, liberando o flag dos registros originais
						TA42DelSons( oModel, 'G3R_ITENS', .F. )
						SetAllDelete(.F.)	
						TA042AcGrd( oModel, .F. )
						FWRestRows( aSaveLines )
					EndIf
		
					// se todos DR estiverem exclu�dos limpar os dados do fornecedor da capa da concilia��o
					For nX := 1 To oModel:GetModel("G3R_ITENS"):Length()
						oModel:GetModel("G3R_ITENS"):GoLine(nX)
						If nX != nLine .And. !oModel:GetModel("G3R_ITENS"):IsDeleted()
							lLimpa := .F.
						EndIf
					Next nX
					
					oModel:GetModel("G3R_ITENS"):GoLine(nLine)
					If lLimpa
						oModel:LoadValue("G8C_MASTER", "G8C_FORNEC", '')
						oModel:LoadValue("G8C_MASTER", "G8C_LOJA"  , '')
					EndIf
				
				ElseIf cAction == 'UNDELETE' 
					If oModelGrid:SeekLine({{"G3R_FILIAL", oModelGrid:GetValue('G3R_FILIAL')}, ;
					                        {"G3R_NUMID" , oModelGrid:GetValue('G3R_NUMID' )}, ;
					                        {"G3R_IDITEM", oModelGrid:GetValue('G3R_IDITEM')}, ;
					                        {"G3R_NUMSEQ", oModelGrid:GetValue('G3R_NUMSEQ')}})
						oModel:SetErrorMessage(cModelId, "G3R_NUMID", cModelId, "G3R_NUMID", "TA042NOUNDEL", STR0165)  // "Documento de Reserva j� selecionado nesta concilia��o."
						Return .F.
					
					ElseIf oModelGrid:GetValue("G3R_DIVERG") == '1'
						oModelGrid:GetModel():SetErrorMessage(cModelId, "G3R_DIVERG", cModelId, "G3R_DIVERG", "TA042NOUNDEL", STR0190) 	// "O Documento de Reserva apresentou falha ao atualizar os Itens Financeiros e n�o pode ser recuperado." 
						Return .F.
		
					ElseIf !Empty(oModelGrid:GetValue("G3R_SEQNSH"))
						oModel:SetErrorMessage(cModelId, "G3R_SEQNSH", cModelId, "G3R_SEQNSH", "TA042NOUNDEL", STR0176)  //"N�o � poss�vel recuperar itens de NoShow ou gerados atrav�s deste."
						Return .F.
		
					ElseIf !Empty( oModelGrid:GetValue("G3R_SEQPRC") )
						oModel:SetErrorMessage(cModelId, "G3R_SEQPRC", cModelId, "G3R_SEQPRC", "TA042NOUNDEL", STR0177)  //"N�o � poss�vel recuperar itens gerados atrav�s de parcialidade."
						Return .F.
									
					ElseIf !Empty(oModel:GetValue("G8C_MASTER", "G8C_FORNEC")) .Or. !Empty(oModel:GetValue("G8C_MASTER", "G8C_LOJA"))
		
						If (!Empty(oModel:GetValue("G8C_MASTER", "G8C_FORNEC")) .And. oModel:GetValue("G8C_MASTER", "G8C_FORNEC") <> oModelGrid:GetValue('G3R_FORREP')) .Or. ;
						   (!Empty(oModel:GetValue("G8C_MASTER", "G8C_LOJA"  )) .And. oModel:GetValue("G8C_MASTER", "G8C_LOJA"  ) <> oModelGrid:GetValue('G3R_LOJREP'))
							oModel:SetErrorMessage("G8C_MASTER", "G8C_FORNEC", "G8C_MASTER", "G8C_FORNEC", "TA042NOUNDEL", STR0169)  // "Documento de Reserva com Fornecedor de Reporte diferente do fornecedor da concilia��o."
							Return .F.
						Else
							oView := FwViewActive()
							oView:GetViewObj("G8C_MASTER")[3]:Refresh(.T.,.F.)
						EndIf
					EndIf
					
					aSaveLines := FWSaveRows()
					TA042AcGrd(	oModel, .T. )
					SetAllDelete(.T.)	
	
					//� necess�rio marcar todos os filhos como deletados, somente assim, todos os registros ser�o
					//processados no Commit e a fun��o TA042EstIn executada, liberando o flag dos registros originais
					TA42DelSons( oModel, 'G3R_ITENS', .F., .T. )

					If !Tur34ItFin(oModel, '3', .T., '3', , .T.)
						oMdlG3R:GoLine(nLine)
						oMdlG3R:SetValue('G3R_DIVERG', '1')
						TA042AcGrd(	oModel, .T. )	
						SetAllDelete(.T.)	
						TA42DelSons( oModel, 'G3R_ITENS', .F. )
						oModel:SetErrorMessage(cModelId, "G3R_NUMID", cModelId, "G3R_NUMID", "TA042NOUNDEL", I18N(STR0189, {oMdlG3R:GetValue('G3R_NUMID'), oMdlG3R:GetValue('G3R_IDITEM'), oMdlG3R:GetValue('G3R_NUMSEQ')}))		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele ser� exclu�do dessa concilia��o."
					Else
						TA042StsIF( oModel )
					EndIf

					SetAllDelete(.F.)
					TA042AcGrd( oModel, .F. )
					FWRestRows( aSaveLines )

					oView := FwViewActive()
					oView:Refresh()
				EndIf
		
			Case cModelId == "G3Q_ITENS"
				If cAction == "CANSETVALUE"
					If cField == "G3Q_NATURE"
						SetAplAcordFin(.T.)
					ElseIf cField != "G3Q_SOLIC"
						Return .F.
					EndIf
				EndIf
		
			Case cModelId == "G44_ITENS"
				If cAction == "CANSETVALUE"
					If !( cField $ "G44_TARBAS|G44_EARLY|G44_LATE|G44_PLALIM" ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
						Return .F.
					ElseIf oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrTar( cField, "XXX" + SubStr(cField,4), .F. )
					EndIf
				ElseIf (cAction $ "DELETE|UNDELETE") .And. !GetAllDelete() .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					Return .F.
				EndIf
		
			Case cModelId == "G46_ITENS"
				If cAction == "CANSETVALUE"
					If ( oModelGrid:GetValue( "G46_CONMAN" ) == "2"  .And. cField <> "G46_VLBASE" ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
						SX3->( dbSetOrder(2) )
						SX3->( dbSeek( "G46_VLBASE" ) )
						oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042CANSETVALUE", STR0067 + AllTrim(X3Titulo()) + STR0068)  //"Para taxas j� cadastradas, apenas o campo "###" � alter�vel."
						Return .F.
								
					ElseIf cField == "G46_VLBASE" .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrTax( cField, "XXX" + SubStr(cField,4), .F. )
					EndIf
		
				ElseIf cAction == "DELETE" 
					If !GetAllDelete() .And. oModelGrid:GetValue( "G46_CONMAN" ) == "2" .And. !oModel:GetModel( "G3R_ITENS" ):IsDeleted()
						oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0069)  //"Somente taxas inseridas manualmente podem ser exclu�das."
						Return .F.
					EndIf
					If !FwIsInCallStack('TA042Desfz')
						SetAltExtras(.T.)
					EndIf
					oModel:GetModel('G3R_ITENS'):SetValue('G3R_TAXA', oModel:GetModel('G3R_ITENS'):GetValue('G3R_TAXA') - oModelGrid:GetValue('G46_VLTAXA'))
		
				ElseIf cAction == "UNDELETE" 
					If !FwIsInCallStack('TA042Desfz')
						SetAltTaxas(.T.)
					EndIf
					oModel:GetModel('G3R_ITENS'):SetValue('G3R_TAXA', oModel:GetModel('G3R_ITENS'):GetValue('G3R_TAXA') + oModelGrid:GetValue('G46_VLTAXA'))	
				EndIf
		
				If cAction $ "DELETE|UNDELETE"		
					CALCVLG8C(oModel:GetModel('G3R_ITENS'), .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
				EndIf
		
			Case cModelId == "G47_ITENS"
				If cAction == "CANSETVALUE"
					If ( oModelGrid:GetValue( "G47_CONMAN" ) == "2"  .And. cField <> "G47_VLUNIT" ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
						SX3->( dbSetOrder(2) )
						SX3->( dbSeek( "G47_VLUNIT" ) )
						oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042CANSETVALUE", STR0070 + AllTrim(X3Titulo()) + STR0068)  //"Para extras j� cadastrados, apenas o campo "###" � alter�vel."
						Return .F.
		
					ElseIf cField == "G47_VLUNIT" .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrExt( 'G47_VLUNIT', 'XXX_VLUNIT', .F. )
		
					ElseIf Empty( oModel:GetValue( "G3Q_ITENS", "G3Q_INFFAT" ) ) .OR. oModel:GetValue( "G3Q_ITENS", "G3Q_INFFAT" )  == "1"
						If IsBlind() .Or. lInputExtras .Or. ApMsgYesNo( STR0071 ) //"No item de venda, � informado que este item n�o possui extras. Deseja prosseguir?"
							lInputExtras := .T.
						Else
							Return .F.
						EndIf

					ElseIf lShowObsIV .And. ( oModel:GetValue( "G3Q_ITENS", "G3Q_INFFAT" ) == "2" .Or. oModel:GetValue( "G3Q_ITENS", "G3Q_INFFAT" ) == "3" ) .And. !Empty( oModel:GetValue( "G3Q_ITENS", "G3Q_OBSFAT" ) )
						oModel:SetErrorMessage(cModelId, "G47_VLUNIT", cModelId, "G47_VLUNIT", "TA042CANSETVALUE", oModel:GetValue( "G3Q_ITENS", "G3Q_OBSFAT" ))  // AQUI SER� MONSTRADO A INFORMACAO GRAVADO NO CAMPO G3Q_OBSFAT
						lShowObsIV := .F.
					EndIf
		
				ElseIf cAction == "DELETE"
					If !GetAllDelete() .And. oModelGrid:GetValue( "G47_CONMAN" ) == "2" .And. !oModel:GetModel( "G3R_ITENS" ):IsDeleted()
						oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0072)  //"Somente extras inseridos manualmente podem ser exclu�das."
						Return .F.
					EndIf
					If !FwIsInCallStack('TA042Desfz')
						SetAltExtras(.T.)
					EndIf
					oModel:GetModel('G3R_ITENS'):SetValue('G3R_EXTRAS', oModel:GetModel('G3R_ITENS'):GetValue('G3R_EXTRAS') - oModelGrid:GetValue('G47_TOTAL'))
					
				ElseIf cAction == "UNDELETE" 
					If !FwIsInCallStack('TA042Desfz')
						SetAltExtras(.T.)
					EndIf
					oModel:GetModel('G3R_ITENS'):SetValue('G3R_EXTRAS', oModel:GetModel('G3R_ITENS'):GetValue('G3R_EXTRAS') + oModelGrid:GetValue('G47_TOTAL'))
				EndIf
		
				If cAction $ "DELETE|UNDELETE"		
					CALCVLG8C(oModel:GetModel('G3R_ITENS'), .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
				EndIf
				
			Case cModelId $ "G48A_ITENS|G48B_ITENS"
				If cModelId == "G48A_ITENS"
					Return .F.
				Else
					If cAction == "CANSETVALUE"
						If oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
							Return .F.
						
						ElseIf cField == "G48_RECCON"
							If oModelGrid:GetValue( "G48_STATUS" ) <> "1"
								oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042CANSETVALUE", STR0093)  //"Apenas acordos abertos podem sem marcados para conciliar."
								Return .F.
							EndIf
	
						ElseIf cField $ "G48_NATURE|G48_STATUS|G48_CODACD|G48_PERACD|G48_VLACD"
							SetAplAcordFin(.T.)
						EndIf
								
					ElseIf cAction == "DELETE"
						If ( !GetAllDelete() .And. ( !Empty( oModelGrid:GetValue( "G48_INTERV" ) ) .And. oModelGrid:GetValue( "G48_INTERV" ) == "3" ) )
							oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0073)  //"Somente acordos inseridos manualmente podem ser exclu�dos."
							Return .F.
						Else
							SetAplAcordFin(.T.)
						EndIf
					EndIf
				EndIf
		
			Case cModelId == "G49_ITENS"
				If cAction == "CANSETVALUE"
					If ( oModelGrid:GetValue( "G49_CONMAN " ) == "2"  .And. !( cField $ "G49_BASE|G49_ALIQ") ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
						SX3->( dbSetOrder(2) )
						SX3->( dbSeek( "G49_BASE" ) )
						cAux := AllTrim( X3Titulo() ) + ", "
		
						SX3->( dbSetOrder(2) )
						SX3->( dbSeek( "G49_ALIQ" ) )
						cAux += AllTrim( X3Titulo() )
		
						oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042CANSETVALUE", STR0074 + cAux + STR0075)  //"Para extras j� cadastrados, apenas os campos "###" s�o alter�veis."
						Return .F.
								
					ElseIf cField $ "G49_BASE|G49_ALIQ" .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrImp( cField, "XXX" + SubStr(cField,4), .F. )
					EndIf
		
				ElseIf cAction == "DELETE" 
					If !GetAllDelete() .And. oModelGrid:GetValue( "G49_CONMAN" ) == "2" .And. !oModel:GetModel( "G3R_ITENS" ):IsDeleted()
						oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0076)  //"Somente impostos inseridos manualmente podem ser exclu�das."
						lRet := .F.
					EndIf
				EndIf
		
			Case cModelId == "G4B_ITENS"
				If cAction == "CANSETVALUE"
					lRet := .F.
				EndIf
		
			Case cModelId == "G4A_ITENS"
				If cAction == "CANSETVALUE" .Or. cAction == "DELETE" .Or. cAction == "UNDELETE"
					lRet := .F.
				EndIf
		
			Case cModelId == "G9KA_ITENS" .Or. cModelId == "G9KB_ITENS"
				If cAction == "CANSETVALUE"
					lRet := .F.
		
				ElseIf !GetAllDelete() .And. ( cAction == "DELETE" .Or. cAction == "UNDELETE" )
					lRet := .F.
		
				EndIf
		
				//Campos para altera��o de parcialidade
			Case TA042SegPr( SubStr( cModelId, 1, 3 ), '2' )
		
				If cAction == "CANSETVALUE"
					TA042FdSeg( oModel:GetValue( "G3R_ITENS", "G3R_TPSEG" ), @cTabSeg, @cFldDtIni, @cFldDtFim, @cFldPeriodo  )
		
					If cField $ cTabSeg+cFldDtIni+"|"+cTabSeg+cFldDtFim .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrSeg( cModelId, cField, "XXX" + SubStr(cField,4), .F. )
						lRet := .T.
		
						//Verifica se a altera��o � na quantidade de dias do per�odo
					ElseIf !Empty( cFldPeriodo ) .And. cField $ cTabSeg+cFldPeriodo .And. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) == "1"
						TA042TrSeg( cModelId, cField, "XXX" + SubStr(cField,4), .F. )
						lRet := .T.
		
					Else
						lRet := .F.
		
					EndIf
				EndIf
		
		EndCase
	EndIf
	
	//Verifica a necessidade de atualiza��o do item financeiro
	If lRet .And. cAction $ "SETVALUE|DELETE|UNDELETE|LOADVALUE" .And. Ascan(aCmpAtuIf,{|x| x[1] == cField}) > 0
		If (SubStr(cField, 1, 3) $ "G3R|G4D|G4A" .Or. cField == "G3Q_FORMPG" .Or. (SubStr(cField, 1, 3) == 'G9K' .And. oModelGrid:GetId() == "G9KB_ITENS" ))
			cOpcao := "2"	//fornecedor
		Else
			cOpcao := "1"	//cliente
		EndIf

		oModelG3Q := oModel:GetModel( 'G3Q_ITENS' )
		If Empty(oModelG3Q:GetValue("G3Q_ATUIF"))
			oModelG3Q:LoadValue("G3Q_ATUIF",cOpcao)
		ElseIf cOpcao <> oModelG3Q:GetValue("G3Q_ATUIF")
			oModelG3Q:LoadValue("G3Q_ATUIF",'3')
		EndIf
	EndIf

	TURXNIL(aSaveLines)
	TURXNIL(aIdGrids)
Return lRet

/*/{Protheus.doc} TA042Init
Fun��o para inicializar as variaveis est�ticas e campos virtuais a cada ativa��o do modelo de dados
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042Init( oModel )
	Local aLines := FwSaveRows( oModel )
	Local nX	 := 0

	SetAltTarifa(.F.)
	SetAltTaxas(.F.)
	SetAltExtras(.F.)
	SetAltImpostos(.F.)
	SetAplAcordFin(.F.)
	SetAltSeg(.F.)
	lInputExtras := .F.
	lShowObsIV := .T.
	SetAllDelete(.F.)
	TurSetNatur( "" )

	TA042AcGrd( oModel, .T. )
	TA042AcGrd( oModel, .F. )

	FwRestRows( aLines )

	// fun��o para atualizar os campos totalizadores de DR
	TRDChaceNew()
	TDRCacheClean()
	
	If !(FWIsInCallStack("TA042AEFET") .Or. FWIsInCallStack("TA042ADEFE")) .And. oModel:GetOperation() != MODEL_OPERATION_INSERT
		CALCVLG8C(oModel:GetModel('G3R_ITENS'), .F./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "INIT"/*cMomento*/)
	EndIf

	If oModel:GetOperation() == MODEL_OPERATION_UPDATE
		oModel:GetModel("G8C_MASTER"):SetValue("G8C_MOEDA", oModel:GetModel("G3R_ITENS"):GetValue("G3R_MOEDA"))
	EndIf
	
	TURXNIL(aLines)
Return .T.

/*/{Protheus.doc} TA042VtFld
Fun��o para cria��o dos campos virtuais
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042VtFld( oStruG44, oStruG46, oStruG47, oStruG49, oStruG3R )

	If ValType(oStruG44) == 'O'
		oStruG44:AddField('Bkp G44_TARBAS'		 ,;		// [01] C Titulo do campo
						  'Bkp G44_TARBAS'		 ,; 	// [02] C ToolTip do campo
						  'XXX_TARBAS' 			 ,; 	// [03] C identificador (ID) do Field
						  'N' 					 ,; 	// [04] C Tipo do campo
						  TamSX3("G44_TARBAS")[1],; 	// [05] N Tamanho do campo
						  TamSX3("G44_TARBAS")[2],; 	// [06] N Decimal do campo
						  NIL					 ,; 	// [07] B Code-block de valida��o do campo
						  NIL 					 ,; 	// [08] B Code-block de valida��o When do campo
						  NIL 					 ,; 	// [09] A Lista de valores permitido do campo
						  NIL 					 ,; 	// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					 ,; 	// [11] B Code-block de inicializacao do campo
						  NIL 					 ,; 	// [12] L Indica se trata de um campo chave
						  .F. 					 ,; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual

		oStruG44:AddField('Alt G44_TARBAS', ; 		// [01] C Titulo do campo
						  'Alt G44_TARBAS', ; 		// [02] C ToolTip do campo
						  'XAL_TARBAS' 	  , ; 		// [03] C identificador (ID) do Field
						  'L' 			  , ; 		// [04] C Tipo do campo
						  1				  , ; 		// [05] N Tamanho do campo
						  0				  , ; 		// [06] N Decimal do campo
						  NIL			  , ; 		// [07] B Code-block de valida��o do campo
						  NIL 			  , ; 		// [08] B Code-block de valida��o When do campo
						  NIL 			  , ; 		// [09] A Lista de valores permitido do campo
						  NIL 			  , ; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			  , ; 		// [11] B Code-block de inicializacao do campo
						  NIL 			  , ; 		// [12] L Indica se trata de um campo chave
						  .F. 			  , ; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 						// [14] L Indica se o campo � virtual

		oStruG44:AddField('Bkp G44_EARLY'		,; 		// [01] C Titulo do campo
						  'Bkp G44_EARLY'		,; 		// [02] C ToolTip do campo
						  'XXX_EARLY' 			,; 		// [03] C identificador (ID) do Field
						  'N' 					,; 		// [04] C Tipo do campo
						  TamSX3("G44_EARLY")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G44_EARLY")[2],; 		// [06] N Decimal do campo
						  NIL					,; 		// [07] B Code-block de valida��o do campo
						  NIL 					,; 		// [08] B Code-block de valida��o When do campo
						  NIL 					,; 		// [09] A Lista de valores permitido do campo
						  .F. 					,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					,; 		// [11] B Code-block de inicializacao do campo
						  NIL 					,; 		// [12] L Indica se trata de um campo chave
						  .F. 					,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual

		oStruG44:AddField('Alt G44_EARLY',; 	// [01] C Titulo do campo
						  'Alt G44_EARLY',; 	// [02] C ToolTip do campo
						  'XAL_EARLY' 	 ,; 	// [03] C identificador (ID) do Field
						  'L' 			 ,; 	// [04] C Tipo do campo
						  1				 ,; 	// [05] N Tamanho do campo
						  0				 ,; 	// [06] N Decimal do campo
						  NIL			 ,; 	// [07] B Code-block de valida��o do campo
						  NIL 			 ,; 	// [08] B Code-block de valida��o When do campo
						  NIL 			 ,; 	// [09] A Lista de valores permitido do campo
						  .F. 			 ,; 	// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			 ,; 	// [11] B Code-block de inicializacao do campo
						  NIL 			 ,; 	// [12] L Indica se trata de um campo chave
						  .F. 			 ,; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 					// [14] L Indica se o campo � virtual

		oStruG44:AddField('Bkp G44_LATE'	   ,; 		// [01] C Titulo do campo
						  'Bkp G44_LATE'	   ,; 		// [02] C ToolTip do campo
						  'XXX_LATE' 		   ,; 		// [03] C identificador (ID) do Field
						  'N' 				   ,; 		// [04] C Tipo do campo
						  TamSX3("G44_LATE")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G44_LATE")[2],; 		// [06] N Decimal do campo
						  NIL				   ,; 		// [07] B Code-block de valida��o do campo
						  NIL 				   ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 				   ,; 		// [09] A Lista de valores permitido do campo
						  .F. 				   ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 				   ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 				   ,; 		// [12] L Indica se trata de um campo chave
						  .F. 				   ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual

		oStruG44:AddField('Alt G44_LATE',; 		// [01] C Titulo do campo
						  'Alt G44_LATE',; 		// [02] C ToolTip do campo
						  'XAL_LATE' 	,; 		// [03] C identificador (ID) do Field
						  'L' 			,; 		// [04] C Tipo do campo
						  1				,; 		// [05] N Tamanho do campo
						  0				,; 		// [06] N Decimal do campo
						  NIL			,; 		// [07] B Code-block de valida��o do campo
						  NIL 			,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			,; 		// [09] A Lista de valores permitido do campo
						  .F. 			,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			,; 		// [11] B Code-block de inicializacao do campo
						  NIL 			,; 		// [12] L Indica se trata de um campo chave
						  .F. 			,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 					// [14] L Indica se o campo � virtual


		oStruG44:AddField('Bkp G44_PLALIM'		 ,; 	// [01] C Titulo do campo
						  'Bkp G44_PLALIM'		 ,; 	// [02] C ToolTip do campo
						  'XXX_PLALIM' 			 ,; 	// [03] C identificador (ID) do Field
						  'N' 					 ,; 	// [04] C Tipo do campo
						  TamSX3("G44_PLALIM")[1],; 	// [05] N Tamanho do campo
						  TamSX3("G44_PLALIM")[2],; 	// [06] N Decimal do campo
						  NIL					 ,; 	// [07] B Code-block de valida��o do campo
						  NIL 					 ,; 	// [08] B Code-block de valida��o When do campo
						  NIL 					 ,; 	// [09] A Lista de valores permitido do campo
						  .F. 					 ,; 	// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					 ,; 	// [11] B Code-block de inicializacao do campo
						  NIL 					 ,; 	// [12] L Indica se trata de um campo chave
						  .F. 					 ,; 	// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual


		oStruG44:AddField('Alt G44_PLALIM',; 		// [01] C Titulo do campo
						  'Alt G44_PLALIM',; 		// [02] C ToolTip do campo
						  'XAL_PLALIM' 	  ,; 		// [03] C identificador (ID) do Field
						  'L' 			  ,;		// [04] C Tipo do campo
					 	  1				  ,; 		// [05] N Tamanho do campo
						  0				  ,; 		// [06] N Decimal do campo
						  NIL			  ,; 		// [07] B Code-block de valida��o do campo
						  NIL 			  ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			  ,; 		// [09] A Lista de valores permitido do campo
						  .F. 			  ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			  ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 			  ,; 		// [12] L Indica se trata de um campo chave
						  .F. 			  ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 						// [14] L Indica se o campo � virtual
	EndIf

	If ValType( oStruG46 ) == 'O'
		oStruG46:AddField('Bkp G46_VLBASE'		 ,; 		// [01] C Titulo do campo
						  'Bkp G46_VLBASE'		 ,; 		// [02] C ToolTip do campo
						  'XXX_VLBASE' 			 ,; 		// [03] C identificador (ID) do Field
						  'N' 					 ,; 		// [04] C Tipo do campo
						  TamSX3("G46_VLBASE")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G46_VLBASE")[2],; 		// [06] N Decimal do campo
						  NIL					 ,; 		// [07] B Code-block de valida��o do campo
						  NIL 					 ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 					 ,; 		// [09] A Lista de valores permitido do campo
						  .F. 					 ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					 ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 					 ,; 		// [12] L Indica se trata de um campo chave
						  .F. 					 ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 								// [14] L Indica se o campo � virtual

		oStruG46:AddField('Alt G46_VLBASE',; 		// [01] C Titulo do campo
						  'Alt G46_VLBASE',; 		// [02] C ToolTip do campo
						  'XAL_VLBASE' 	  ,; 		// [03] C identificador (ID) do Field
						  'L' 			  ,; 		// [04] C Tipo do campo
					  	  1				  ,; 		// [05] N Tamanho do campo
						  0				  ,; 		// [06] N Decimal do campo
						  NIL			  ,; 		// [07] B Code-block de valida��o do campo
						  NIL 			  ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			  ,; 		// [09] A Lista de valores permitido do campo
						  .F. 			  ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			  ,; 		// [11] B Code-block de inicializacao do campo
						  .T. 			  ,; 		// [12] L Indica se trata de um campo chave
						  .F. 			  ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 						// [14] L Indica se o campo � virtual
	EndIf

	If ValType( oStruG47 ) == 'O'
		oStruG47:AddField('Bkp G47_VLUNIT'       ,; 		// [01] C Titulo do campo
						  'Bkp G47_VLUNIT'		 ,; 		// [02] C ToolTip do campo
						  'XXX_VLUNIT' 			 ,; 		// [03] C identificador (ID) do Field
						  'N' 					 ,; 		// [04] C Tipo do campo
						  TamSX3("G47_VLUNIT")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G47_VLUNIT")[2],; 		// [06] N Decimal do campo
						  NIL					 ,; 		// [07] B Code-block de valida��o do campo
						  NIL 					 ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 					 ,; 		// [09] A Lista de valores permitido do campo
						  .F. 					 ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					 ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 					 ,; 		// [12] L Indica se trata de um campo chave
						  .F. 					 ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 								// [14] L Indica se o campo � virtual

		oStruG47:AddField('Alt G47_VLUNIT',; 		// [01] C Titulo do campo
						  'Alt G47_VLUNIT',; 		// [02] C ToolTip do campo
						  'XAL_VLUNIT' 	  ,; 		// [03] C identificador (ID) do Field
						  'L' 			  ,; 		// [04] C Tipo do campo
						  1				  ,; 		// [05] N Tamanho do campo
						  0				  ,; 		// [06] N Decimal do campo
						  NIL			  ,; 		// [07] B Code-block de valida��o do campo
						  NIL 			  ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			  ,; 		// [09] A Lista de valores permitido do campo
						  .F. 			  ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			  ,;	 	// [11] B Code-block de inicializacao do campo
						  NIL 			  ,; 		// [12] L Indica se trata de um campo chave
						  .F. 			  ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 						// [14] L Indica se o campo � virtual
	EndIf

	If ValType( oStruG49 ) == 'O'
		oStruG49:AddField('Bkp G49_BASE'	   ,; 		// [01] C Titulo do campo
					  	  'Bkp G49_BASE'	   ,; 		// [02] C ToolTip do campo
						  'XXX_BASE' 		   ,; 		// [03] C identificador (ID) do Field
						  'N' 				   ,; 		// [04] C Tipo do campo
						  TamSX3("G49_BASE")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G49_BASE")[2],; 		// [06] N Decimal do campo
						  NIL				   ,; 		// [07] B Code-block de valida��o do campo
						  NIL 				   ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 				   ,; 		// [09] A Lista de valores permitido do campo
						  .F. 				   ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 				   ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 				   ,; 		// [12] L Indica se trata de um campo chave
						  .F. 				   ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual

		oStruG49:AddField('Alt G49_BASE',; 		// [01] C Titulo do campo
						  'Alt G49_BASE',; 		// [02] C ToolTip do campo
						  'XAL_BASE' 	,; 		// [03] C identificador (ID) do Field
						  'L' 			,; 		// [04] C Tipo do campo
					 	  1				,; 		// [05] N Tamanho do campo
						  0				,; 		// [06] N Decimal do campo
						  NIL			,; 		// [07] B Code-block de valida��o do campo
						  NIL 			,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			,; 		// [09] A Lista de valores permitido do campo
						  .F. 			,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			,; 		// [11] B Code-block de inicializacao do campo
						  NIL 			,; 		// [12] L Indica se trata de um campo chave
						  .F. 			,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 					// [14] L Indica se o campo � virtual

		oStruG49:AddField('Bkp G49_ALIQ'	   ,; 		// [01] C Titulo do campo
						  'Bkp G49_ALIQ'	   ,; 		// [02] C ToolTip do campo
						  'XXX_ALIQ' 		   ,; 		// [03] C identificador (ID) do Field
						  'N' 				   ,; 		// [04] C Tipo do campo
						  TamSX3("G49_ALIQ")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G49_ALIQ")[2],; 		// [06] N Decimal do campo
						  NIL				   ,; 		// [07] B Code-block de valida��o do campo
						  NIL 				   ,; 		// [08] B Code-block de valida��o When do campo
						  NIL 				   ,; 		// [09] A Lista de valores permitido do campo
						  .F. 				   ,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 				   ,; 		// [11] B Code-block de inicializacao do campo
						  NIL 				   ,; 		// [12] L Indica se trata de um campo chave
						  .F. 				   ,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual

		oStruG49:AddField('Alt G49_ALIQ',; 		// [01] C Titulo do campo
						  'Alt G49_ALIQ',; 		// [02] C ToolTip do campo
						  'XAL_ALIQ' 	,; 		// [03] C identificador (ID) do Field
						  'L' 			,; 		// [04] C Tipo do campo
						  1				,; 		// [05] N Tamanho do campo
						  0				,; 		// [06] N Decimal do campo
						  NIL			,; 		// [07] B Code-block de valida��o do campo
						  NIL 			,; 		// [08] B Code-block de valida��o When do campo
						  NIL 			,; 		// [09] A Lista de valores permitido do campo
						  .F. 			,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 			,; 		// [11] B Code-block de inicializacao do campo
						  NIL 			,; 		// [12] L Indica se trata de um campo chave
						  .F. 			,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 					// [14] L Indica se o campo � virtual
	EndIf

	If ValType( oStruG3R ) == 'O'
		oStruG3R:AddField("Bkp Vlr Fim"			,; 		// [01] C Titulo do campo
						  "Bkp Vlr Fim"	 		,; 		// [02] C ToolTip do campo
						  "XBK_VLFIM" 			,; 		// [03] C identificador (ID) do Field
						  'N' 					,; 		// [04] C Tipo do campo
						  TamSX3("G3R_VLFIM")[1],; 		// [05] N Tamanho do campo
						  TamSX3("G3R_VLFIM")[2],; 		// [06] N Decimal do campo
						  NIL					,; 		// [07] B Code-block de valida��o do campo
						  NIL 					,; 		// [08] B Code-block de valida��o When do campo
						  NIL 					,; 		// [09] A Lista de valores permitido do campo
						  NIL 					,; 		// [10] L Indica se o campo tem preenchimento obrigat�rio
						  NIL 					,; 		// [11] B Code-block de inicializacao do campo
						  NIL 					,; 		// [12] L Indica se trata de um campo chave
						  .F. 					,; 		// [13] L Indica se o campo N�O pode receber valor em uma opera��o de update.
						  .T.) 							// [14] L Indica se o campo � virtual
	EndIf
Return

/*/{Protheus.doc} TA042Trig
Fun��o para cria��o de gatilhos dos campos virtuais
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042Trig( oStruG44, oStruG46, oStruG47, oStruG49, oStruG3R, oStruG48B, oStruG4E )
	Local aAux := {}

	If ValType( oStruG44 ) == 'O'
		aAux := FwStruTrigger( 	"G44_TARBAS", "XXX_TARBAS", "TA042TrTar( 'G44_TARBAS', 'XXX_TARBAS' )"	)
		oStruG44:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G44_EARLY", "XXX_EARLY", "TA042TrTar( 'G44_EARLY', 'XXX_EARLY' )"		)
		oStruG44:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G44_LATE", "XXX_LATE", "TA042TrTar( 'G44_LATE', 'XXX_LATE' )"		)
		oStruG44:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G44_PLALIM", "XXX_PLALIM", "TA042TrTar( 'G44_PLALIM', 'XXX_PLALIM' )"	)
		oStruG44:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	EndIf

	If ValType( oStruG46 ) == 'O'
		aAux := FwStruTrigger( 	"G46_VLBASE", "XXX_VLBASE", "TA042TrTax( 'G46_VLBASE', 'XXX_VLBASE' )"	)
		oStruG46:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	EndIf

	If ValType( oStruG47 ) == 'O'
		aAux := FwStruTrigger( 	"G47_VLUNIT", "XXX_VLUNIT", "TA042TrExt( 'G47_VLUNIT', 'XXX_VLUNIT' )"	)
		oStruG47:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	EndIf

	If ValType( oStruG49 ) == 'O'
		aAux := FwStruTrigger( 	"G49_BASE", "XXX_BASE", "TA042TrImp( 'G49_BASE', 'XXX_BASE' )"	)
		oStruG49:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )

		aAux := FwStruTrigger( 	"G49_ALIQ", "XXX_ALIQ", "TA042TrImp( 'G49_ALIQ', 'XXX_ALIQ' )"	)
		oStruG49:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	EndIf

	If ValType( oStruG48B ) == 'O'
		//Sempre que alterar o status do acordo, � desmarcado o campo RECCON, se ativo, obriga o usu�rio a marcar novamente
		xAux := FwStruTrigger( 'G48_STATUS', 'G48_RECCON', '.F.', .F. )
		oStruG48B:AddTrigger( xAux[1], xAux[2], xAux[3], xAux[4])
	
		aAux := FwStruTrigger( 	"G48_RECCON", "G48_RECCON", "TA042TrRec()"	)
		oStruG48B:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G48_RECCON", "G48_RECCON", "TA042TrG48( 'G48_RECCON', 'G48_RECCON' )"	)
		oStruG48B:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G48_VLACD", "G48_VLACD", "TA042TrG48( 'G48_VLACD', 'G48_VLACD' )" )
		oStruG48B:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G48_CODACD", "G48_CODACD", "TA042TrG48( 'G48_CODACD', 'G48_CODACD' )" )
		oStruG48B:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G48_STATUS", "G48_STATUS", "TA042TrG48( 'G48_STATUS', 'G48_STATUS' )" )
		oStruG48B:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	EndIf

	If ValType( oStruG4E ) == 'O'
		//Sempre que alterar valores de reembolso
		aAux := FwStruTrigger( 	"G4E_TARPAG", "G4E_TARPAG", "SetAltTarifa(.T.)" )
		oStruG4E:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G4E_TPGINT", "G4E_TPGINT", "SetAltTarifa(.T.)" )
		oStruG4E:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G4E_TREEMB", "G4E_TREEMB", "SetAltTarifa(.T.)" )
		oStruG4E:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G4E_TXREEM", "G4E_TXREEM", "SetAltTaxas(.T.)" )
		oStruG4E:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
		aAux := FwStruTrigger( 	"G4E_EXREEM", "G4E_EXREEM", "SetAltExtras(.T.)" )
		oStruG4E:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4] )
	
	EndIf

	TURXNIL(aAux)
Return

/*/{Protheus.doc} TA042TrTar
Fun��o para gatilhos referente a campos virtuais de tarifas
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrTar( cDom, cCDom, lGatilho )
	Local cRet	:= FwFldGet( cDom )
	DEFAULT lGatilho	:= .T.

	//Atualiza vari�vel static, indicando altera��o manual de tarifa
	If FWIsInCallStack( "TA042PARC" )
		cRet	:= FwFldGet( cCDom )
	ElseIf lGatilho
		SetAltTarifa(.T.)

		//Apenas a primeira altera��o ser� realizada a c�pia do valor, mantendo a c�pia do valor original do registro
		If FwFldGet( "XAL" + SubStr(cCDom,4))
			cRet := FwFldGet(cCDom)
		EndIf

	ElseIf !FwFldGet( "XAL" + SubStr(cCDom,4))
		FwFldPut( "XAL" + SubStr(cCDom,4), .T.)
		FwFldPut(cCDom,cRet)
	EndIf

Return cRet

/*/{Protheus.doc} TA042TrTax
Fun��o para gatilhos referente a campos virtuais de taxas
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrTax( cDom, cCDom, lGatilho )
	Local cRet	:= FwFldGet( cDom )
	DEFAULT lGatilho := .T.

	//Atualiza vari�vel static, indicando altera��o manual de tarifa
	If FWIsInCallStack( "TA042PARC" )
		cRet	:= FwFldGet( cCDom )
	ElseIf lGatilho
		SetAltTaxas(.T.)

		//Apenas a primeira altera��o ser� realizada a c�pia do valor, mantendo a c�pia do valor original do registro
		If FwFldGet( "XAL" + SubStr(cCDom,4))
			cRet := FwFldGet(cCDom)
		EndIf

	ElseIf !FwFldGet( "XAL" + SubStr(cCDom,4))
		FwFldPut( "XAL" + SubStr(cCDom,4), .T.)
		FwFldPut(cCDom,cRet)
	EndIf

Return cRet

/*/{Protheus.doc} TA042TrExt
Fun��o para gatilhos referente a campos virtuais de extras
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrExt( cDom, cCDom, lGatilho )
	Local cRet	:= FwFldGet( cDom )
	DEFAULT lGatilho := .T.

	//Atualiza vari�vel static, indicando altera��o manual de tarifa
	If FWIsInCallStack( "TA042PARC" )
		cRet	:= FwFldGet( cCDom )
	ElseIf lGatilho
		SetAltExtras(.T.)

		//Apenas a primeira altera��o ser� realizada a c�pia do valor, mantendo a c�pia do valor original do registro
		If FwFldGet( "XAL" + SubStr(cCDom,4))
			cRet := FwFldGet(cCDom)
		EndIf

	ElseIf !FwFldGet( "XAL" + SubStr(cCDom,4))
		FwFldPut( "XAL" + SubStr(cCDom,4), .T.)
		FwFldPut(cCDom,cRet)
	EndIf

Return cRet


/*/{Protheus.doc} TA042TrImp
Fun��o para gatilhos referente a campos virtuais de impostos
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrImp( cDom, cCDom, lGatilho )
	Local cRet	:= FwFldGet( cDom )
	DEFAULT lGatilho := .T.

	//Atualiza vari�vel static, indicando altera��o manual de tarifa
	If FWIsInCallStack( "TA042PARC" )
		cRet	:= FwFldGet( cCDom )
	ElseIf lGatilho
		SetAltImpostos(.T.)

		//Apenas a primeira altera��o ser� realizada a c�pia do valor, mantendo a c�pia do valor original do registro
		If FwFldGet( "XAL" + SubStr(cCDom,4))
			cRet := FwFldGet(cCDom)
		EndIf

	ElseIf !FwFldGet( "XAL" + SubStr(cCDom,4))
		FwFldPut( "XAL" + SubStr(cCDom,4), .T.)
		FwFldPut(cCDom,cRet)
	EndIf

Return cRet

/*/{Protheus.doc} TA042TrSeg
Fun��o para gatilhos referente a campos virtuais de segmentos
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrSeg( cIdModel, cDom, cCDom, lGatilho )
	Local oModel	:= FwModelActive()
	Local cRet		:= oModel:GetValue( cIdModel, cDom )
	DEFAULT lGatilho := .T.

	//Atualiza vari�vel static, indicando altera��o per�odo segmento
	If lGatilho
		SetAltSeg(.T.)

		//Apenas a primeira altera��o ser� realizada a c�pia do valor, mantendo a c�pia do valor original do registro
		If !Empty( oModel:GetValue( cIdModel, cCDom )  )
			cRet := oModel:GetValue( cIdModel, cCDom )
		EndIf

	ElseIf Empty( oModel:GetValue( cIdModel, cCDom ) )
		oModel:SetValue( cIdModel, cCDom, cRet )
	Else
		cRet := oModel:GetValue( cIdModel, cCDom )
	EndIf

Return cRet

/*/{Protheus.doc} TA042TrG3R
Fun��o para gatilhos referente a campos virtuais de documentos de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrG3R( cDom, cCDom )
	Local xRetorno := FwFldGet(cDom)

	If cCDom == "XXX_VLFIM"
		If FwFldGet("G3Q_OPERAC") == "2" //Reembolso
			xRetorno := IIf(FwFldGet("G3R_STATUS") == "4" , 0 ,FwFldGet("G3R_VLRSER") + FwFldGet("XXX_TAXADU")) + FwFldGet("XXX_TOTREC")
		Else
			xRetorno := IIf(FwFldGet("G3R_STATUS") == "4" , 0 ,FwFldGet("G3R_VLRSER") - FwFldGet("XXX_TAXADU")) - FwFldGet("XXX_TOTREC")
		EndIf

	ElseIf cCDom == "XXX_TOTREC"
		xRetorno := FwFldGet("XXX_VLCOMI") + FwFldGet("XXX_VLINCE") - FwFldGet("XXX_TXFORN")
	EndIf

Return xRetorno

/*/{Protheus.doc} TA042TrG48
Fun��o para gatilhos referente a campos virtuais de acordos aplicados
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cDom, character, campo dominio do gatilho
@param cCDom, character, contra dominio do gatilho
@param lGatilho, booleano, indica chamada da fun��o via gatilho
/*/
Function TA042TrG48( cDom, cCDom )
	Local aLines  := FwSaveRows()
	Local oModel  := FwModelActive()
	Local oMdlG3R := oModel:GetModel( "G3R_ITENS" )

	CALCVLG8C(oMdlG3R, .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
	FwRestRows(aLines)
	
	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042ATOK
Fun��o para valida��o do Post (TudoOK)
@type function
@author Anderson Toledo
@since 13/04/2016
@version 1.0
/*/
Function TA042ATOK(oModel)
	Local lRet := .T.

	If oModel:GetOperation() == 3 .Or. oModel:GetOperation() == 4

		If GetAltTarifa()			; 	//Indica se houve altera��o manual na tabela G44 - Tarifas
			.Or. GetAltTaxas()	;	//Indica se houve altera��o manual na tabela G46 - Taxas
			.Or. GetAltExtras()	;	//Indica se houve altera��o manual na tabela G47 - Extras
			.Or. GetAltImpostos()	;	//Indica se houve altera��o manual na tabela G49 - Impostos
			.Or. GetAltSeg()		;	//Indica se houve altera��o no per�odo de reserva
			.Or. GetAplAcordFin()		//Indica alguma altera��o que for�a a atualiza��o dos DR's

			Help(,,"TA042COMMIT",,STR0065,1,0)							 //"Foram realizadas altera��es nos Documentos de Reserva, utilize a rotina 'Atualizar DRs' para atualiza��o dos registros."
			lRet := .F.

		EndIf

	EndIf

Return lRet

/*/{Protheus.doc} TA042VAtuDr
Fun��o com tratamentos para a View na chamada da rotina de atualiza��o de DR's
@type function
@author Anderson Toledo
@since 03/05/2016
/*/
Static Function TA042VAtuDr()
	Local oView := FwViewActive()

	FwMsgRun(,{|| TA042AtuDR( .T. ) },,STR0007 ) //"Aplicando altera��es no DR..."

	oView:Refresh()
Return

/*/{Protheus.doc} TA042VNShow
Fun��o com tratamentos para a View na chamada da rotina de No Show
@type function
@author Anderson Toledo
@since 03/05/2016
/*/
Static Function TA042VNShow()
	Local oView := FwViewActive()

	FwMsgRun(,{|| TA042NShow("1")},,STR0010) //"No Show"###"Realizar a opera��o de No Show"###"Aplicando no Show..."

	oView:Refresh()
Return

/*/{Protheus.doc} TA042VDesfz
Fun��o com tratamentos para a View na chamada da Desfaz a efetiva��o
@type function
@author Anderson Toledo
@since 03/05/2016
/*/
Static Function TA042VDesfz()

	FwMsgRun(,{|| TA042Desfz()},,STR0013) //"Desfazendo altera��es..."

Return

//------------------------------------------------------------------------
/*{Protheus.doc} T042ACtInc(lOnLine,lMostrLanc,lAglutLanc)

 Contabiliza��o da rotina de inclus�o.


@sample 	T042ACtInc(oModel)
@return
@author  	Simone Mie Sato Kakinoana
@since   	21/04/2016
@version  	12.1.8
*/
//------------------------------------------------------------------------------------------
Function T042ACtInc(lOnLine,lMostrLanc,lAglutLanc,cRotinaOri)

	Local aArea     := GetArea()
	Local aAreaG3Q  := G3Q->(GetArea())
	Local aAreaG3R  := G3R->(GetArea())
	Local aAreaG48  := G48->(GetArea())
	Local aFlagCTB 	:= {}

	Local nPosReg			:= 0

	Local cPadT30	 	:= "T30"
	Local cPadT32    	:= "T32"
	Local cPadT34    	:= "T34"
	Local cPadT36   	:= "T36"
	Local cLoteTur	 	:= LoteCont("TUR")
	Local cAliasG4C  	:= ""
	Local cNumId		:= ""
	Local cConcil		:= G8C->G8C_CONCIL
	Local cFiltTipo	:= "% "

	Local lPadT30		:= VerPadrao(cPadT30)
	Local lPadT32		:= VerPadrao(cPadT32)
	Local lPadT34		:= VerPadrao(cPadT34)
	Local lPadT36		:= VerPadrao(cPadT36)
	Local lUsaFlag		:= SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)

	Private cArquivo  	:= ""
	Private nHdlPrv		:= 0
	Private nTotal	 		:= 0

	Private LanceiCtb		:= .F.

	nHdlPrv := HeadProva(cLoteTur,cRotinaOri,Substr(cUsuario,7,6),@cArquivo)

	cAliasG4C 	:= GetNextAlias()

	If cRotinaOri == "TURA042A"
		cFiltTipo += " AND ( G3R_OPERAC ='1' OR G3R_OPERAC ='3') "
	ElseIf cRotinaOri == "TURA042R"
		cFiltTipo += " AND G3R_OPERAC ='2' "
	EndIf
	cFiltTipo += " %"

	If Select(cAliasG4C) > 0
		dbSelectArea(cAliasG4C)
		dbCloseArea()
	EndIf

	BeginSql Alias cAliasG4C

		SELECT G4C.R_E_C_N_O_ RECNOG4C, G3R.R_E_C_N_O_ RECNOG3R, G4C.*, G3R_OPERAC
		FROM %Table:G4C% G4C
		INNER JOIN %Table:G8C% G8C ON G8C_FILIAL = %xFilial:G8C% AND G8C.%notDel%
		AND G8C.G8C_STATUS = '1'
		AND G4C.G4C_CONORI = G8C.G8C_CONCIL
		INNER JOIN %Table:G3R% G3R ON G3R_FILIAL = %xFilial:G3R% AND G3R.%notDel%
		AND G3R.G3R_NUMID = G4C.G4C_NUMID AND G3R.G3R_IDITEM = G4C.G4C_IDITEM AND G3R.G3R_NUMSEQ = G4C.G4C_NUMSEQ
		AND G3R.G3R_CONORI <> ''
		AND G3R.G3R_CONORI = G4C.G4C_CONORI
		%Exp:cFiltTipo%
		WHERE G4C_FILIAL = %xFilial:G4C%
		AND G4C.%notDel%
		AND G4C_CONORI = %Exp:cConcil%
		AND G4C_CLIFOR = '2'
		AND G4C_CODAPU = ''
		AND G4C_LACONC <> 'S'
	EndSql

//AND G4C_STATUS = '4'

	While (cAliasG4C)->( !EOF() )

		DbSelectArea("G4C")
		G4C->(dbSetOrder(2))
		G4C->(dbGoto((cAliasG4C)->RECNOG4C))

		G3Q->(DbSetOrder(1))
		If G3Q->(DbSeek(xFilial("G3Q")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ))

		//Verifica se existe bloqueio por processo.
			If !CtbValiDt(,G8C->G8C_DTEFET,.F.,,,{"TUR001"},)//VERIFICAR SE SER� G3Q_EMISS, G3Q_DTINC
				(cAliasG4C)->( DbSkip() )
				Loop
			EndIf

		EndIf

		G3R->(DbSetOrder(1))
		G3R->(dbGoto((cAliasG4C)->RECNOG3R))

		If (cAliasG4C)->G4C_TIPO == "1"	 //1=Venda

			If lPadT30
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->( Recno() ), 0, 0, 0} )
				Endif

				nTotal += DetProva(nHdlPrv,cPadT30,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "2/5" //2=Reembolso;5=Breakage

			If lPadT32
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->( Recno() ), 0, 0, 0} )
				Endif

				nTotal += DetProva(nHdlPrv,cPadT32,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "3" //3=Receita

			G48->(DbSetOrder(1))
			If G48->(DbSeek(xFilial("G48")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ+(cAliasG4C)->G4C_APLICA+(cAliasG4C)->G4C_NUMACD))
				If lPadT34
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
						aAdd( aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->( Recno() ), 0, 0, 0} )
					Endif

					nTotal += DetProva(nHdlPrv,cPadT34,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
				EndIf
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "4" //4=Abat. Receita

			G48->(DbSetOrder(1))
			If G48->(DbSeek(xFilial("G48")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ+(cAliasG4C)->G4C_APLICA+(cAliasG4C)->G4C_NUMACD))
				If lPadT36
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
						aAdd( aFlagCTB, {"G4C_LACONC", "S", "G4C", G4C->( Recno() ), 0, 0, 0} )
					Endif

					nTotal += DetProva(nHdlPrv,cPadT36,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
				EndIf
			EndIf

		EndIf

		If LanceiCtb // Vem do DetProva
			If !lUsaFlag
				RecLock("G4C")
				G4C->G4C_LACONC := "S"
				G4C->(MsUnlock())
			EndIf
		ElseIf lUsaFlag
			If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G4C->(Recno()) }))>0
				aFlagCTB := Adel(aFlagCTB,nPosReg)
				aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
			Endif
		EndIf

		G4C->(DbCloseArea())
		(cAliasG4C)->( DbSkip() )
	End
	(cAliasG4C)->( DbCloseArea() )

	If nHdlPrv > 0 .And. nTotal > 0
		RodaProva(nHdlPrv,nTotal)
		cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
		cArquivo	:= ""
		nHdlPrv	:= 0
		nTotal		:= 0
	Endif

	RestArea(aAreaG48)
	RestArea(aAreaG3R)
	RestArea(aAreaG3Q)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaG3Q)
	TURXNIL(aAreaG3R)
	TURXNIL(aAreaG48)
	TURXNIL(aFlagCTB)
Return

//------------------------------------------------------------------------
/*{Protheus.doc} T042ACtExc(lOnLine,lMostrLanc,lAglutLanc)

 Contabiliza��o da rotina de exclus�o.

@sample 	T042ACtExc(oModel)
@return
@author  	Simone Mie Sato Kakinoana
@since   	22/04/2016
@version  	12.1.8
*/
//------------------------------------------------------------------------------------------
Function T042ACtExc(lOnLine,lMostrLanc,lAglutLanc,cRotinaOri)

	Local aArea     := GetArea()
	Local aAreaG3Q  := G3Q->(GetArea())
	Local aAreaG3R  := G3R->(GetArea())
	Local aAreaG48  := G48->(GetArea())
	Local aFlagCTB 	:= {}

	Local nPosReg			:= 0

	Local cPadT31	 	:= "T31"
	Local cPadT33    	:= "T33"
	Local cPadT35    	:= "T35"
	Local cPadT37    	:= "T37"
	Local cLoteTur	   	:= LoteCont("TUR")
	Local cAliasG4C 	:= ""
	Local cConcil		:= G8C->G8C_CONCIL
	Local cFiltTipo	 := "% "

	Local lPadT31    := VerPadrao(cPadT31)
	Local lPadT33    := VerPadrao(cPadT33)
	Local lPadT35    := VerPadrao(cPadT35)
	Local lPadT37    := VerPadrao(cPadT37)
	Local lUsaFlag	 := SuperGetMV( "MV_CTBFLAG" , .T. /*lHelp*/, .F. /*cPadrao*/)

	Private cArquivo  := ""
	Private nHdlPrv	  := 0
	Private nTotal	  := 0
	Private LanceiCtb := .F.

	nHdlPrv := HeadProva(cLoteTur,cRotinaOri,Substr(cUsuario,7,6),@cArquivo)

	cAliasG4C 	:= GetNextAlias()

	If cRotinaOri == "TURA042A"
		cFiltTipo += " AND ( G3R_OPERAC ='1' OR G3R_OPERAC ='3') "
	ElseIf cRotinaOri == "TURA042R"
		cFiltTipo += " AND G3R_OPERAC ='2' "
	EndIf
	cFiltTipo += " %"

	If Select(cAliasG4C) > 0
		dbSelectArea(cAliasG4C)
		dbCloseArea()
	EndIf

	BeginSql Alias cAliasG4C

		SELECT G4C.R_E_C_N_O_ RECNOG4C, G3R.R_E_C_N_O_ RECNOG3R, G4C.*, G3R_OPERAC
		FROM %Table:G4C% G4C
		INNER JOIN %Table:G8C% G8C ON G8C_FILIAL = %xFilial:G8C% AND G8C.%notDel%
		AND G4C.G4C_CONORI = G8C.G8C_CONCIL
		INNER JOIN %Table:G3R% G3R ON G3R_FILIAL = %xFilial:G3R% AND G3R.%notDel%
		AND G3R.G3R_NUMID = G4C.G4C_NUMID AND G3R.G3R_IDITEM = G4C.G4C_IDITEM AND G3R.G3R_NUMSEQ = G4C.G4C_NUMSEQ
		AND G3R.G3R_CONORI <> ''
		AND G3R.G3R_CONORI = G4C.G4C_CONORI
		%Exp:cFiltTipo%
		WHERE G4C_FILIAL = %xFilial:G4C%
		AND G4C.%notDel%
		AND G4C_CONORI = %Exp:cConcil%
		AND G4C_CLIFOR = '2'
		AND G4C_CODAPU = ''
		AND G4C_LACONC = 'S'
	EndSql

//AND G4C_STATUS = '4'
	While (cAliasG4C)->( !EOF() )

		DbSelectArea("G4C")
		G4C->(dbSetOrder(2))
		G4C->(dbGoto((cAliasG4C)->RECNOG4C))

		G3Q->(DbSetOrder(1))
		If G3Q->(DbSeek(xFilial("G3Q")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ))
		//Verifica se existe bloqueio por processo.
			If !CtbValiDt(,G8C->G8C_DTEFET,,,,{"TUR001"},)//VERIFICAR SE SER� G3Q_EMISS, G3Q_DTINC
				(cAliasG4C)->( DbSkip() )
				Loop
			EndIf
		EndIf

		G3R->(DbSetOrder(1))
		G3R->(dbGoto((cAliasG4C)->RECNOG3R))

		If (cAliasG4C)->G4C_TIPO == "1"	 //1=Venda

			If lPadT31
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0} )
				Endif

				nTotal += DetProva(nHdlPrv,cPadT31,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "2/5" //2=Reembolso;5=Breakage
			If lPadT33
				If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
					aAdd( aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0} )
				Endif

				nTotal += DetProva(nHdlPrv,cPadT33,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "3" //3=Receita

			G48->(DbSetOrder(1))
			If G48->(DbSeek(xFilial("G48")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ+(cAliasG4C)->G4C_APLICA+(cAliasG4C)->G4C_NUMACD))
				If lPadT35
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
						aAdd( aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0} )
					Endif

					nTotal += DetProva(nHdlPrv,cPadT35,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
				EndIf
			EndIf

		ElseIf (cAliasG4C)->G4C_TIPO $ "4" //4=Abat. Receita

			G48->(DbSetOrder(1))
			If G48->(DbSeek(xFilial("G48")+ (cAliasG4C)->G4C_NUMID+(cAliasG4C)->G4C_IDITEM+(cAliasG4C)->G4C_NUMSEQ+(cAliasG4C)->G4C_APLICA+(cAliasG4C)->G4C_NUMACD))
				If lPadT37
					If lUsaFlag  // Armazena em aFlagCTB para atualizar no modulo Contabil
						aAdd( aFlagCTB, {"G4C_LACONC", " ", "G4C", G4C->( Recno() ), 0, 0, 0} )
					Endif

					nTotal += DetProva(nHdlPrv,cPadT37,cRotinaOri,cLoteTur,,,,,,,,@aFlagCTB)
				EndIf
			EndIf

		EndIf

		If LanceiCtb // Vem do DetProva
			If !lUsaFlag
				RecLock("G4C")
				G4C->G4C_LACONC := " "
				G4C->(MsUnlock())
			EndIf
		ElseIf lUsaFlag
			If (nPosReg  := aScan(aFlagCTB,{ |x| x[4] == G4C->(Recno()) }))>0
				aFlagCTB := Adel(aFlagCTB,nPosReg)
				aFlagCTB := aSize(aFlagCTB,Len(aFlagCTB)-1)
			Endif
		EndIf

		G4C->(DbCloseArea())
		(cAliasG4C)->( DbSkip() )
	End
	(cAliasG4C)->( DbCloseArea() )

	If nHdlPrv > 0 .And. nTotal > 0
		RodaProva(nHdlPrv,nTotal)
		cA100Incl(cArquivo, nHdlPrv, 3, cLoteTur, lMostrLanc, lAglutLanc,,,,@aFlagCTB,,)
		cArquivo	:= ""
		nHdlPrv	:= 0
		nTotal		:= 0
	Endif

	RestArea(aAreaG48)
	RestArea(aAreaG3R)
	RestArea(aAreaG3Q)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaG3Q)
	TURXNIL(aAreaG3R)
	TURXNIL(aAreaG48)
	TURXNIL(aFlagCTB)
Return

/*/{Protheus.doc} TA042VisFi
Fun��o para chamar a vis�o financeira sem apresentar os bot�es padr�o.
@type function
@author Anderson Toledo
@since 04/07/2016
@version 1.0
/*/
Function TA042VisFi()
	Local aButtons 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,"Sair"},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Confirmar"###"Cancelar"

	FWExecView(STR0057,'VIEWDEF.TURA042D',4,,{|| .T.},,,aButtons)
	
	TURXNIL(aButtons)
Return

/*/{Protheus.doc} TA042AVlNat
Fun��o para valida��o das naturezas
@type function
@author Anderson Toledo
@since 14/09/2016
@version 1.0
/*/
Function TA042AVlNat( oModel, cError, cTipo )
	
	Local aErros		:= {}
	Local aSaveLines	:= FwSaveRows( oModel )
	
	Local lRet			:= .T.
	Local lAssocMetas	:= FwIsInCallStack("TURA039M")
	
	Local nX			:= 0
	Local nY 			:= 0
	Local nZ			:= 0
	Local nA 			:= 0
	Local cString		:= ""
	
	Local oModelG3R	:= oModel:GetModel( 'G3R_ITENS' )
	Local oModelG3Q	:= oModel:GetModel( 'G3Q_ITENS' )
	Local oModelG48B	:= oModel:GetModel( 'G48B_ITENS' )
	Local oModelG6I		:= oModel:GetModel( 'G6I_ITENS' )
	Local oModelG6M		:= oModel:GetModel( 'G6M_ITENS' )

	DEFAULT cTipo		:= ''
	DEFAULT cError 	:= ''

	If ValType( oModelG6I ) == 'O'
	
		//Varrer o G6I para identificar se � uma meta ou um DR,
		//Se for DR, posiciona o submodelo de G6H e faz o bloco assinalado de DR
		For nZ := 1 to oModelG6I:Length()
		
			If ( oModelG6I:GetValue("G6I_META") <> "1" ) //Sen�o � meta
				
				//Bloco assinalado de DR
				For nX := 1 to oModelG3R:Length()
					
					oModelG3R:GoLine( nX )
	
					aAdd( aErros, { oModelG3R:GetValue( 'G3R_NUMID' ), oModelG3R:GetValue( 'G3R_IDITEM' ), oModelG3R:GetValue( 'G3R_NUMSEQ' ), .F., {}  } )
	
					//Se cancelado por FOP, n�o verifica as naturezas	
					If oModelG3Q:GetValue( 'G3Q_CANFOP' ) == '1'		
						Loop
					EndIf
	
					If Empty( oModelG3R:GetValue( 'G3R_NATURE' ) )
						aErros[nX][4] := .T.
						aAdd( aErros[nX][5], STR0126 ) //"Documento de Reserva"
					EndIf
	
					If !oModelG48B:IsEmpty()
						
						For nY := 1 to oModelG48B:Length()
						
							oModelG48B:GoLine( nY )
	
							If oModelG48B:GetValue( 'G48_STATUS' ) == '3'
								Loop
	
							ElseIf oModelG48B:GetValue( 'G48_RECCON' ) .And. Empty( oModelG48B:GetValue( 'G48_NATURE' ) )
								aErros[nX][4] := .T.
								aAdd( aErros[nX][5], STR0127 + oModelG48B:GetValue( 'G48_APLICA' ) ) //"Acordo fornecedor, aplica��o: "
							EndIf
	
						Next nY
			
					EndIf
			
				Next nX
				
			ElseIf ( lAssocMetas )	//Metas
				
				oModelG6I:GoLine(nZ)
				
				For nX := 1 to oModelG6M:Length()
				
					aAdd(aErros, {	oModelG6M:GetValue("G6M_CODAPU",nX),; 
									oModelG6M:GetValue("G6M_FATMET",nX),; 
									oModelG6M:GetValue("G6M_ITFAT",nX),; 
									.F.,;
									{} } )
					
					If ( Empty(oModelG6M:GetValue("G6M_NATURE",nX)) )
						aErros[nX][4] := .T.
						aAdd( aErros[nX][5], "Apura��o de Metas" ) //"Documento de Reserva"
					EndIf
					
				Next nX
				
			EndIf
	
		Next nZ

	Else
		nA := 0
		For nX := 1 to oModelG3R:Length()
			oModelG3R:GoLine( nX )
			If !oModelG3R:GetValue( 'G3R_STATUS' ) $ '2|4'
				nA++
			aAdd( aErros, { oModelG3R:GetValue( 'G3R_NUMID' ), oModelG3R:GetValue( 'G3R_IDITEM' ), oModelG3R:GetValue( 'G3R_NUMSEQ' ), .F., {}  } )
	
			If Empty( oModelG3R:GetValue( 'G3R_NATURE' ) ) .And. oModelG3R:GetValue( 'G3R_STATUS' ) == '1'
					aErros[nA][4] := .T.
					aAdd( aErros[nA][5], STR0126 ) //"Documento de Reserva"
			EndIf
	
			If !oModelG48B:IsEmpty()
				For nY := 1 to oModelG48B:Length()
					oModelG48B:GoLine( nY )
	
					If oModelG48B:GetValue( 'G48_STATUS' ) == '3'
						Loop
	
					ElseIf oModelG48B:GetValue( 'G48_RECCON' ) .And. Empty( oModelG48B:GetValue( 'G48_NATURE' ) )
							aErros[nA][4] := .T.
							aAdd( aErros[nA][5], STR0127 + oModelG48B:GetValue( 'G48_APLICA' ) ) //"Acordo fornecedor, aplica��o: "
					EndIf
	
				Next
	
			EndIf
			Endif
		Next
	
	EndIf
	
	cError := STR0128 + CRLF //"Verifique as naturezas abaixo antes da efetiva��o:"

	//Somente executa se espec�ficar o tipo do cabe�alho
	If cTipo == '1'
	
		If Empty( oModel:GetValue( "G6H_MASTER","G6H_NATUR" )  )
			cError := STR0151 + CRLF	//"Natureza para gera��o da fatura a pagar"
			lRet	:= .F.
		EndIf

	ElseIf cTipo == '2'
	
		If oModel:GetValue( "G8C_MASTER","G8C_DOCENT" ) <> '1'
		
			If Empty( oModel:GetValue( "G8C_MASTER","G8C_NATUR" )  )
				cError := STR0151 + CRLF	//"Natureza para gera��o da fatura a pagar"
				lRet	:= .F.
			EndIf
		
		EndIf
		
	EndIf

	For nX := 1 to len(aErros)
	
		If nX > 1
			cError += CRLF
		EndIf

		If aErros[nX][4]
			
			lRet	:= .F.
			
			If ( aScan(aErros[nX,5],{|x| "APURA" $ Alltrim(Upper(x))}) > 0 )
				cString := STR0152	//"Apura��o: #1[Apuracao]#, Fatura A�rea: #2[Fatura Aerea]# e Item da Fatura: #3[Item da Fatura]#"
			Else
				cString := STR0129	//"Doc. reserva: #1[Doc. Reserva]#, Item: #2[Item]#, Seq.: #3[Sequencia]#"
			EndIf	
				 
			cError += Space(4)
			cError += i18N(cString , { aErros[nX][1], aErros[nX][2], aErros[nX][3] } )

			For nY := 1 to len( aErros[nX][5] )
				cError += CRLF
				cError += Space(8) + " - " + aErros[nX][5][nY]
			Next
			
		EndIf

	Next

	FwRestRows( aSaveLines )

	TURXNIL(aErros)
	TURXNIL(aSaveLines)
Return lRet

/*/{Protheus.doc} T42FilRef
Fun��o para obter a filial de refer�ncia
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function T42FilRef( oModel )
	Local cFilRef := ""

	If SuperGetMV( 'MV_CONCMFI',,.F.)
		cFilRef := cFilAnt
	Else
		cFilRef := oModel:GetValue( 'G3R_ITENS', 'G3R_MSFIL' )
	EndIf

Return cFilRef

/*/{Protheus.doc} TA42CaAbNF
Fun��o para realizar os abatimentos de acordo com filial e natureza para t�tulos
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA42CalcAb( aAbat, aDadosTit )
	Local nX			:= 0
	Local nPosFRef	:= 0
	Local nPos 		:= 0

	If Len(aAbat) > 0 .And. Len(aDadosTit) > 0

		If Len(aAbat) >= Len(aDadosTit)
			For nX := 1 To Len(aAbat)
				nPosFRef := AScan( aDadosTit, {|x|, x[1] == aAbat[nX][1] .And. x[2] == aAbat[nX][2]  } )

				If nPosFRef > 0
					If aDadosTit[nPosFRef][3] >= aAbat[nX][3]
						aDadosTit[nPosFRef][3] -= aAbat[nX][3]
						aAbat[nX][3] := 0
					Else
						aAbat[nX][3] -= aDadosTit[nPosFRef][3]
						aDadosTit[nPosFRef][3] := 0
					EndIf
				EndIf
			Next

		Else
			For nX := 1 To Len(aDadosTit)
				nPosFRef := AScan( aAbat, {|x|, x[1] == aDadosTit[nX][1] .And. x[2] == aDadosTit[nX][2]  } )

				If nPosFRef > 0
					If aAbat[nPosFRef][3] >= aDadosTit[nX][3]
						aAbat[nPosFRef][3] -= aDadosTit[nX][3]
						aDadosTit[nX][3] := 0
					Else
						aDadosTit[nX][3] -= aAbat[nPosFRef][3]
						aAbat[nPosFRef][3] := 0
					EndIf
				EndIf
			Next
		EndIf
	EndIf

	//Retira itens zerados dos vetores de abatimento e t�tulos avulsos
	nPos := aScan( aDadosTit, {|x| x[3] == 0} )
	While nPos > 0 .And. len(aDadosTit) > 0
		aDel(aDadosTit, nPos)
		aSize(aDadosTit, len(aDadosTit) - 1 )

		nPos := aScan( aDadosTit, {|x| x[3] == 0} )

	EndDo

	nPos := aScan( aAbat, {|x| x[3] == 0} )
	While nPos > 0 .And. len(aAbat) > 0
		aDel(aAbat, nPos)
		aSize(aAbat, len(aAbat) - 1 )

		nPos := aScan( aAbat, {|x| x[3] == 0} )

	EndDo

Return

/*/{Protheus.doc} TA42CaAbNF
Fun��o para realizar os abatimentos de acordo com filial e natureza para nota fiscal
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA42CaAbNF( aAbat, aDadosTit )
	Local aAux			:= 0
	Local nX			:= 0
	Local nPosFRef	:= 0
	Local nPos 		:= 0

	If Len(aAbat) > 0 .And. Len(aDadosTit) > 0

		If Len(aAbat) >= Len(aDadosTit)
			For nX := 1 To Len(aAbat)
				nPosFRef := AScan( aDadosTit, {|x|, x[1] == aAbat[nX][1] .And. x[2] == aAbat[nX][2]  } )

				If nPosFRef > 0

					aAux := aDadosTit[nPosFRef][5]
					aSort( aAux,,,{ |x,y| x > y } )

					While aAbat[nX][3] > 0 .And. len( aAux ) > 0
						If aAbat[nX][3] >= aAux[1][3]
							aAbat[nX][3] -= aAux[1][3]

							aDel( aAux, 1 )
							aSort( aAux, len(aAux) - 1 )

						Else
							aAux[1][3] -= aAbat[nX][3]
							aAbat[nX][3] := 0
						EndIf
					EndDo

				EndIf

			Next

		Else
			For nX := 1 To Len(aDadosTit)

				nPosFRef := AScan( aAbat, {|x|, x[1] == aDadosTit[nX][1] .And. x[2] == aDadosTit[nX][2]  } )

				If nPosFRef > 0

					aAux := aDadosTit[nX][5]
					aSort( aAux,,,{ |x,y| x > y } )

					While aAbat[nPosFRef][3] > 0 .And. len( aAux ) > 0
						If aAbat[nPosFRef][3] >= aAux[1][3]
							aAbat[nPosFRef][3] -= aAux[1][3]

							aDel( aAux, 1 )
							aSort( aAux, len(aAux) - 1 )

						Else
							aAux[1][3] -= aAbat[nPosFRef][3]
							aAbat[nPosFRef][3] := 0
						EndIf
					EndDo


				EndIf

			Next
		EndIf
	EndIf

	//Retira itens zerados dos vetores de abatimento e t�tulos avulsos
	nX 	:= 1
	nPos := len( aDadosTit )
	While nX < nPos .And. len( aDadosTit ) > 0
		If len( aDadosTit[nX][5] ) == 0
	 		aDel( aDadosTit, nX )
	 		nPos := len( aDadosTit ) - 1
	 		aSize( aDadosTit, nPos )
		Else
			nX++
		EndIf
	EndDo

	nPos := aScan( aAbat, {|x| x[3] == 0} )
	While nPos > 0 .And. len(aAbat) > 0
		aDel(aAbat, nPos)
		aSize(aAbat, len(aAbat) - 1 )

		nPos := aScan( aAbat, {|x| x[3] == 0} )

	EndDo

	TURXNIL(aAux)
Return

/*/{Protheus.doc} TA042TitFT
Fun��o para obter os t�tulos que ir�o compor da fatura a pagar
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA042TitFT( oModel, cTipo, cFatura, cFornece, cLoja )
	Local aArea     := GetArea()
	Local aAreaSE2	:= SE2->( GetArea() )
	Local aAux		:= {}
	Local aTit		:= {}
	Local aRet		:= {}
	Local dDataIni	:= dDataBase
	Local dDataFim	:= dDataBase
	Local nX		:= 0
	Local nMoeda	:= 0

	aAux := TA042GetTit(cFatura, cTipo, cFornece, cLoja, '2', .T. )

	SE2->( dbSetOrder(1) )
	For nX := 1 to len(aAux)

		If SE2->( dbSeek( aAux[nX]  ) )
			nMoeda := SE2->E2_MOEDA

			If SE2->E2_EMISSAO < dDataIni
				dDataIni := SE2->E2_EMISSAO
			EndIf

			If SE2->E2_EMISSAO > dDataFim
				dDataFim := SE2->E2_EMISSAO
			EndIf

			aAdd( aTit,	{SE2->E2_PREFIXO, SE2->E2_NUM, SE2->E2_PARCELA, SE2->E2_TIPO, .F.})
		EndIf
	Next

	aRet := { nMoeda, dDataIni, dDataFim, aTit }

	RestArea(aAreaSE2)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSE2)
	TURXNIL(aAux)
Return aRet

/*/{Protheus.doc} TA042GerFT
Fun��o para gera��o da fatura a pagar
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA042GerFT( cPrefixo, cTipo, cNumTit, cNatureza, cFornece, cLoja, cCondicao, nMoeda, dDataIni, dDataFim, aTitulos, aLog, oModel, lAereo )
	Local aArea     := GetArea()
	Local aAreaSE2	:= SE2->( GetArea() )
	Local aArray	:= {}
	Local cAlias	:= ""
	Local lRet		:= .T.
	
	Default lAereo := .F.
	Private lMsErroAuto := .F.

	aArray := { cPrefixo, cTipo, cNumTit, cNatureza, dDataIni, dDataFim, cFornece, cLoja, "", "", cCondicao, nMoeda, aTitulos }

	TA042ASetMdl(oModel)
	MsExecAuto( { |x,y| FINA290(x,y)},3,aArray,)

	If lMsErroAuto
		MostraErro()
		lRet := .F.
	ElseIf lAereo
		cAlias := GetNextAlias()
		BeginSql Alias cAlias
			SELECT E2_FILIAL,
					E2_PREFIXO,
					E2_NUM,
					E2_PARCELA,
					E2_TIPO,
					E2_FORNECE,
					E2_LOJA
				FROM %Table:SE2%
				WHERE E2_FILIAL = %xFilial:SE2%
					AND E2_PREFIXO = %Exp:cPrefixo%
					AND E2_NUM	= %Exp:cNumTit%
					AND E2_TIPO = 'FTF'
					AND E2_FORNECE = %Exp:cFornece%
					AND E2_LOJA = %Exp:cLoja%
					AND %notDel%
		EndSql

		SE2->( dbSetOrder(1) )
		While (cAlias)->( !EOF() )
			If SE2->( dbSeek( (cAlias)->( E2_FILIAL + E2_PREFIXO + E2_NUM + E2_PARCELA + E2_TIPO + E2_FORNECE + E2_LOJA ) ) )
				//Criar o vetor com o log da opera��o
				aAdd( aLog, { "G8Y_FILREF"	, xFilial( 'SE2' )	} 	)
				aAdd( aLog, { "G8Y_TPDOC"	, "8"				}	)
				aAdd( aLog, { "G8Y_DOC"		, SE2->E2_NUM		}	)
				aAdd( aLog, { "G8Y_NUM"		, SE2->E2_NUM		}	)
				aAdd( aLog, { "G8Y_SERIE"	, SE2->E2_PREFIXO 	}	)
				aAdd( aLog, { "G8Y_PREFIX"	, SE2->E2_PREFIXO	}	)
				aAdd( aLog, { "G8Y_PARCEL"	, SE2->E2_PARCELA	}	)
				aAdd( aLog, { "G8Y_FORNEC"	, SE2->E2_FORNECE	}	)
				aAdd( aLog, { "G8Y_LOJA"	, SE2->E2_LOJA		}	)
				aAdd( aLog, { "G8Y_TIPO"	, "FTF"				}	)
				aAdd( aLog, { "G8Y_IDDOC"	, FINGRVFK7('SE2',&(TurKeyTitulo('SE2'))) 	}	)
				aAdd( aLog, { "G8Y_VALOR"	, SE2->E2_VALOR		}	)
			Else
				lRet := .F.	
			EndIf
			(cAlias)->( dbSkip() )
		EndDo
		(cAlias)->( dbCloseArea() )
	EndIf

	RestArea(aAreaSE2)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSE2)
	TURXNIL(aArray)
Return lRet

/*/{Protheus.doc} TA042EstFT
Fun��o para verificar se existe uma fatura a pagar e solicitar o estorno da mesma
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA042EsFat( cFatura, cSerie, cFornec, cLoja )
	Local aArea     := GetArea()
	Local aAreaSE2	:= SE2->( GetArea() )
	Local cAlias	:= GetNextAlias()
	Local lRet		:= .T.

	BeginSql Alias cAlias
		SELECT E2_FILIAL,
				E2_PREFIXO,
				E2_NUM,
				E2_PARCELA,
				E2_TIPO,
				E2_FORNECE,
				E2_LOJA
			FROM %Table:SE2% SE2
			WHERE E2_NUM = %Exp:cFatura%
				AND E2_PREFIXO = %Exp:cSerie%
				AND E2_FORNECE  = %Exp:cFornec%
				AND E2_LOJA  = %Exp:cLoja%
				AND E2_FILIAL = %xFilial:SE2%
				AND E2_TIPO = 'FTF'
				AND %notDel%
	EndSql

	If ( cAlias )->( !EOF() )

		TA042EstFT( 	( cAlias )->E2_FILIAL,;
						( cAlias )->E2_PREFIXO,;
						( cAlias )->E2_NUM,;
						( cAlias )->E2_PARCELA,;
						( cAlias )->E2_TIPO,;
						( cAlias )->E2_FORNECE,;
						( cAlias )->E2_LOJA )

	EndIf

	( cAlias )->( dbCloseArea() )

	RestArea(aAreaSE2)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSE2)
Return lRet

/*/{Protheus.doc} TA042EstFT
Fun��o para estorno da fatura a pagar
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA042EstFT( cFilTit, cPrefixo, cNum, cParcela, cTipo, cFornece, cLoja )
	Local aArea     := GetArea()
	Local aAreaSE2	:= SE2->( GetArea() )
	Local aTitulo	:={}
	Local lRet		:= .T.

	Private lMsErroAuto := .F.

	SE2->( dbSetOrder(1) )
	If SE2->( dbSeek( cFilTit + cPrefixo + cNum + cParcela + cTipo + cFornece + cLoja ) )
		aAdd( aTitulo,{	SE2->E2_PREFIXO	,;
							SE2->E2_NUM		,;
							SE2->E2_PARCELA	,;
							SE2->E2_TIPO 		,;
							.F. 				} )
	EndIf

	MsExecAuto( { |x,y| FINA290(x,y)},4,aTitulo)

	If lMsErroAuto
		MostraErro()
		lRet := .F.
	Endif

	RestArea(aAreaSE2)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSE2)
	TURXNIL(aTitulo)
Return lRet

/*/{Protheus.doc} TA042Vencto
Fun��o para obter a data de vencimento da fatura da concilia��o
@type function
@author Thiago Tavares
@since 02/02/2017
@version 1.0
/*/
Function TA042Vencto(nValor, cCondicao)

Local aArea   := GetArea()
Local oModel  := TA042AGetMdl()
Local aRet    := {}
Local dVencto := SToD('')

dVencto := oModel:GetValue('G8C_MASTER', 'G8C_VENCIM')

DbSelectArea('SE4')
SE4->(DbSetOrder(1))		// E4_FILIAL+E4_CODIGO
If SE4->(DbSeek(xFilial('SE4') + cCondicao))
	If SE4->E4_TIPO == Alltrim(GetMv('MV_TURTPCD', , 'C')) 		//Par�metro do Tipo de Condi��o de Pagamento
		aRet := TURxCond(nValor, cCondicao, dVencto)
	EndIf
EndIf
SE4->(DbCloseArea())

RestArea(aArea)
TURXNIL(aArea)

Return aRet 

/*/{Protheus.doc} TA042ASetMdl
Utilizada para guardar o model ativo
@type function
@author Thiago Tavares
@since 07/02/2017
@version 1.0
/*/
Function TA042ASetMdl(oModel)
	oMdlAtivo := oModel
Return 

/*/{Protheus.doc} TA042AGetMdl
Utilizada para retornar o model ativo
@type function
@author Thiago Tavares
@since 07/02/2017
@version 1.0
/*/
Function TA042AGetMdl()
Return	oMdlAtivo

Static Function T042VlItPr( oModel, cMessage )
	Local aArea     := GetArea()
	Local cAlias	:= GetNextAlias()
	Local lRet 		:= .T. 
	
	DEFAULT	cMessage	:= ""	
	
	BeginSql Alias cAlias		
		SELECT	G3R_NUMID,
				G3R_IDITEM,
				G3R_NUMSEQ,
				G3R_CONCIL,
				G3R_FILCON
			FROM %Table:G3R%
			WHERE G3R_CONCIL = %Exp:G8C->G8C_CONCIL%
				AND G3R_FILCON = %xFilial:G8C%
				AND G3R_STATUS = '2'
				AND %notDel%
	EndSql

	While ( cAlias )->( !EOF() )
		
		If !T042ExItPr( ( cAlias )->G3R_NUMID	,;
						( cAlias )->G3R_IDITEM	,;
						( cAlias )->G3R_NUMSEQ	,;
						( cAlias )->G3R_CONCIL	,;
						( cAlias )->G3R_FILCON 	,;
						@cMessage				)
			lRet := .F.
			Exit
		EndIf

		( cAlias )->( dbSkip() )
	EndDo
	( cAlias )->( DbCloseArea() )
	
	RestArea(aArea)
	TURXNIL(aArea)
Return lRet
 
Static Function T042ExItPr( cNumId, cIdItem, cSeq, cConcil, cFilCon, cMessage )
	Local aArea     := GetArea()
	Local cAlias	:= GetNextAlias()
	Local lItNoParc	:= .T.
	
	DEFAULT cMessage	:= ""
	
	BeginSql Alias cAlias
		
		SELECT G3R_NUMID, G3R_IDITEM, G3R_NUMSEQ
			FROM %Table:G3R%
			WHERE	G3R_NUMID	= %Exp:cNumId%
				AND	G3R_IDITEM	= %Exp:cIdItem%
				AND (G3R_SEQPRC = %Exp:cSeq% OR G3R_SEQNSH = %Exp:cSeq%)
				AND G3R_CONCIL <> %Exp:cConcil%
				AND G3R_FILCON =  %Exp:cFilCon%
				AND G3R_CONINU = ' '
				AND %notDel%
			 	
	EndSql

	While (cAlias)->( !EOF() )
		lItNoParc := .F.
	
		cMessage += i18n( STR0156 + CRLF,; //"DR: #1[Documento de reserva]# IV: #2[Item de Venda]# Seq.: #3[Sequ�ncia]#"
		 					{ (cAlias)->G3R_NUMID, (cAlias)->G3R_IDITEM, (cAlias)->G3R_NUMSEQ }  )
	
		(cAlias)->( dbSkip() )
	EndDo
	
	( cAlias )->( dbCloseArea() )

	RestArea(aArea)
	TURXNIL(aArea)
Return lItNoParc

Static Function T42ACanACO( oModel, cTipo )
	Local nX := 0
	Local oModelG48  := IIF( cTipo == '1', oModel:GetModel( 'G48A_ITENS'  ), oModel:GetModel( 'G48B_ITENS'  ) )
	Local oModelG4C  := IIF( cTipo == '1', oModel:GetModel( 'G4CA_ITENS'  ), oModel:GetModel( 'G4CB_ITENS'  ) )
	
	If !oModelG48:IsEmpty()
		For nX := 1 to oModelG48:Length() 
			oModelG48:GoLine( nX )
			
			If !Empty( oModelG48:GetValue( "G48_CODAPU" ) )
				Loop
			EndIf
	
			If oModelG48:GetValue( 'G48_STATUS' ) < '3' 		// 1-Aberto
				oModelG48:LoadValue( 'G48_STATUS'	, '3' ) 	// 3-Cancelado
				oModelG48:SetValue( 'G48_CANCON'	, '1' ) 	// 1-Sim
			EndIf
		Next
	EndIf
	
	For nX := 1 to oModelG4C:Length() 
		oModelG4C:GoLine( nX )
		
		If oModelG4C:IsEmpty() .Or. oModelG4C:IsDeleted() .Or. !Empty(oModelG4C:GetValue("G4C_CODAPU")) .Or. oModelG4C:GetValue("G4C_STATUS") == '4' 
			Loop
		EndIf
		
		oModelG4C:DeleteLine()
	Next

Return

Static Function T42AECanAC( oModel, cTipo )
	Local nX := 0
	Local oModelG48 := IIF( cTipo == '1', oModel:GetModel( 'G48A_ITENS' ), oModel:GetModel( 'G48B_ITENS' ) )

	If !oModelG48:IsEmpty()
		For nX := 1 to oModelG48:Length() 
			oModelG48:GoLine( nX )
	
			If oModelG48:GetValue( 'G48_CANCON' ) == '1' 	// 1-Aberto
				oModelG48:LoadValue( 'G48_STATUS'	, '1' ) 	// 1-Aberto
				oModelG48:SetValue( 'G48_CANCON'	, '2' ) 	// 1-Sim
				
			EndIf
			
		Next
	EndIf
	
Return