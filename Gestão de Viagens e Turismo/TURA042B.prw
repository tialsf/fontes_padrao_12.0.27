#Include "Totvs.ch"
#Include "FWMVCDef.ch"

/*/{Protheus.doc} ModelDef
Modelo de dados para grava��o do log da efetiva��o da concilia��o terrestre (outros)
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel   := Nil
	Local oStruG8Y := FWFormStruct(1, "G8Y", /*bAvalCampo*/, /*lViewUsado*/)	//T�tulos Faturas
	
	oStruG8Y:SetProperty("*", MODEL_FIELD_OBRIGAT, .F.)
	
	oModel := MPFormModel():New("TURA042B", /*bPreValidacao*/, /*bPosValidacao*/,/*bCommit*/, /*bCancel*/)
	oModel:AddFields("G8Y_MASTER", /*cOwner*/, oStruG8Y)
Return oModel