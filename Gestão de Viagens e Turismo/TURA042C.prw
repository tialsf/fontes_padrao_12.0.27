#INCLUDE "TURA042C.ch"
#Include "Totvs.ch"
#Include "FWPrintSetup.ch"
#Include "FWMVCDef.ch"

#DEFINE IMP_SPOOL	2
#DEFINE IMP_PDF		6 

/*/{Protheus.doc} TA042DemFi
Programa para gera��o do relat�rio demonstrativo financeiro da concilia��o terrestre
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Function TA042DemFi(nTipo, lImpAuto)	
	Local cCliente	 := ""
	Local cLoja		 := ""
	Local nLin		 := 40
	Local nCol1	     := 50
	Local nCol2      := 150
	Local nX		 := 0
	Local lFirstPage := .T.
	Local lNewPage	 := .F.
	Local oPrint 	 := nil
	Local oFont10 	 := TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
	Local oFont10n	 := TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
	Local oFont14n	 := TFont():New("Courier New",9,14,.T.,.T.,5,.T.,5,.T.,.F.)
	Local oModel	 := nil
	Local cNome      := ''
	Local nTarifaB	 := 0
	Local nTafifaN	 := 0
	Local nMarkup	 := 0
	Local nTaxas	 := 0
	Local nExtras	 := 0
	Local nTaxasAg	 := 0
	Local nFEE		 := 0
	Local nFEECli	 := 0
	Local nTotRecCli := 0
	Local nTotRecClA := 0 
	Local nTotRecFor := 0 
	Local nVlrLiqCli := 0
	Local nVlrLiqFor := 0
	Local nVlrDU	 := 0
	Local nAux		 := 0 
	Local nVlrDesc	 := 0
	Local nVlrRepas	 := 0
	Local nVlrInc	 := 0
	Local aValAcerto := {}
	Local nVlrTxFor	 := 0
	Local cTPFOP	 := ''
	Local nTipoImp	 := 1
	Local cCaminho   := Iif(isBlind(), '\spool\' ,GetTempPath())
	 
	Default nTipo    := 1
	Default lImpAuto := .F.
	
	If FwIsInCallStack('TURA042A')
		Pergunte('TURA042A', .F.)
	ElseIf FwIsInCallStack('TURA042R')
		Pergunte('TURA042R', .F.)
	EndIf
	
	nTipoImp := IIF(!Empty(MV_PAR04), MV_PAR04, 1)
	cNome    := IIF(nTipo == 1, "DemFinAgen-Ter.rel", "DemFinCli-Ter.rel")
	
	// Apaga arquivo ja existente
	If File(cCaminho+cNome)
		FErase(cCaminho+cNome)
	EndIf

	if IsBlind()
		nTipoImp := 3
	Endif		
	
	oPrint := FWMSPrinter():New(cNome, IIF(nTipoImp == 3, IMP_PDF, IMP_SPOOL)	, .F., cCaminho, IsBlind() .or. nTipoImp == 2	,.f.,	,	,isBlind()	, 		,	,!IsBlind() .and. nTipoImp == 3)
	oPrint:SetResolution(72)
	oPrint:SetPortrait()
	oPrint:SetPaperSize(DMPAPER_A4) 
	oPrint:SetMargin(60,60,60,60) // nEsquerda, nSuperior, nDireita, nInferior 
	oPrint:cPathPDF := cCaminho
	If IsBlind()
		oPrint:lserver	:=.T.
		oPrint:linjob	:=.T.
	Endif	
	If oPrint:nModalResult == PD_CANCEL
 		Return
	EndIf

	oModel := FwLoadModel( "TURA042C" )			
	oModel:SetOperation( MODEL_OPERATION_VIEW )
	oModel:Activate()
			
	For nX := 1 to oModel:GetModel( "G3R_ITENS" ):Length()
		oModel:GetModel( "G3R_ITENS" ):GoLine( nX )
		
		// N�o imprime quando status cancelado ou particionado, ou quando � acerto pois os valores ja s�o somados ao item principal.
		If oModel:GetValue("G3R_ITENS", "G3R_STATUS") $ '2|4' .Or. oModel:GetValue("G3R_ITENS", "G3R_ACERTO") = '1'
			Loop
		EndIf
	
		If cCliente <> oModel:GetValue("G3Q_ITENS", "G3Q_CLIENT") .Or. cLoja <> oModel:GetValue("G3Q_ITENS", "G3Q_LOJA") .Or. lNewPage
			cCliente := oModel:GetValue("G3Q_ITENS", "G3Q_CLIENT")
			cLoja 	 := oModel:GetValue("G3Q_ITENS", "G3Q_LOJA") 
			lNewPage := .F.
			
			If !lFirstPage
				oPrint:EndPage()
			EndIf
			
			If lFirstPage .Or. nTipo == 2
				oPrint:StartPage()
				TA042CImpC( oPrint, @nLin, oModel, oFont10, oFont10n, oFont14n, nTipo )

			ElseIf nTipo == 1
				SA1->( dbSetOrder(1) )
				SA1->( dbSeek( xFilial("SA1") +  cCliente +  cLoja ) )
	
			EndIf
			lFirstPage := .F.
		EndIf
		
		PrintSegm(oModel,oPrint,nTipo,@nLin) 
		
		If nTipo == 2
			nLin += 10
			oPrint:Say(nLin, nCol1 ,STR0085	 , oFont10n) //"Status: "
			oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, 145, 165) ,IIF(oModel:GetValue("G3Q_ITENS","G3Q_STATUS")=='1',STR0086,STR0087) , oFont10)	
		EndIf
		
		aValAcerto  := Tur42cVal(oModel)
		nTarifaB	:= oModel:GetValue( "G44_ITENS","G44_TARBAS")	+ aValAcerto[1]
	 	nTafifaN	:= oModel:GetValue( "G44_ITENS","G44_APLICA")	+ aValAcerto[2]
	 	nMarkup		:= oModel:GetValue( "G3Q_ITENS","G3Q_MARKUP")	+ aValAcerto[3]
	 	nTaxas		:= oModel:GetValue( "G3Q_ITENS","G3Q_TAXA")		+ aValAcerto[4] 	
	 	nExtras		:= oModel:GetValue( "G3Q_ITENS","G3Q_EXTRA")	+ aValAcerto[5]
	 	nTaxasAg	:= oModel:GetValue( "G3Q_ITENS","G3Q_TAXAAG")	+ aValAcerto[6]
	 	nFEE		:= oModel:GetValue( "G3Q_ITENS","G3Q_FEE")		+ aValAcerto[7]
	 	nFEECli		:= AcoFee(oModel,'1')							+ aValAcerto[7]
	 	nTotRecCli	:= (oModel:GetValue( "G3Q_ITENS","G3Q_TOTREC")	+ aValAcerto[8]) -  AcoFee(oModel,'2')
	 	nTotRecClA  := oModel:GetValue( "G3Q_ITENS","G3Q_TOTREC")	+ aValAcerto[8]
	 	nTotRecFor	:= oModel:GetValue( "G3R_ITENS","G3R_TOTREC")	+ aValAcerto[9]
	 	nVlrLiqCli	:= oModel:GetValue( "G3Q_ITENS","G3Q_VLFIM")	+ aValAcerto[10]
	 	nVlrLiqFor	:= oModel:GetValue( "G3R_ITENS","G3R_VLFIM")	+ aValAcerto[11]
	 	nVlrDU		:= oModel:GetValue( "G3Q_ITENS","G3Q_TAXADU")	+ aValAcerto[12]
	 	nVlrCom		:= oModel:GetValue( "G3R_ITENS","G3R_VLCOMI")	+ aValAcerto[13]
	 	nVlrDesc	:= oModel:GetValue( "G3Q_ITENS","G3Q_DESC")		+ aValAcerto[14]
	 	nVlrRepas	:= oModel:GetValue( "G3Q_ITENS","G3Q_REPASS")	+ aValAcerto[15]
	 	nVlrInc		:= oModel:GetValue( "G3R_ITENS","G3R_VLINCE")	+ aValAcerto[16]
	 	nVlrTxFor	:= oModel:GetValue( "G3R_ITENS","G3R_TXFORN")	+ aValAcerto[17]
	 	cTPFOP		:= Posicione("G3O",1,xFilial("G3O")+oModel:GetValue( "G3Q_ITENS","G3Q_FORMPG" ),"G3O->G3O_TIPO")
	
	 	If nTipo == 1 	//Agencia
			nLin += QuebPag(20,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0004, oFont10n) //"Emiss�o:"
			oPrint:Say(nLin, nCol2 + 22, AllTrim(DtoC( oModel:GetValue( "G3R_ITENS", "G3R_EMISS" ) ))   , oFont10)
				
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0005+'(+)', oFont10n)   //"Tarifa Base:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTarifaB , "@E 999,999,999.99") , oFont10)   

			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0006, oFont10n)    //"Cambio:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( oModel:GetValue( "G44_ITENS","G44_TXCAMB"), "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0007+'(+)', oFont10n)     //"Tarifa Nac.:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTafifaN, "@E 999,999,999.99"), oFont10)

			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0008+'(+)', oFont10n)    //"Taxas:"

			If cTPFOP <> '1'
				oPrint:Say(nLin, nCol2 ,TRANSFORM( nTaxas , "@E 999,999,999.99"), oFont10)
			Else
				oPrint:Say(nLin, nCol2 ,TRANSFORM( nTaxas  + nVlrDU, "@E 999,999,999.99"), oFont10)
			Endif 
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0078 +'(+)', oFont10n)    //"Extras:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nExtras, "@E 999,999,999.99"), oFont10)
									
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0082+'(-)', oFont10n)    //"Comiss�o: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrCom , "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0094+'(-)', oFont10n)    //"Incentivo: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrInc , "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0126+'(+)', oFont10n)    //"Tx. Fornec.: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrTxFor , "@E 999,999,999.99"), oFont10)
			
			If cTPFOP <> '1'
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0081+'(+)', oFont10n)    //"DU: "
				oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrDU , "@E 999,999,999.99"), oFont10)
			EndIf
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0009, oFont10n)    //"Total:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrLiqFor, "@E 999,999,999.99"), oFont10)
			
			PrintClien(oPrint,@nLin)
			
			nLin += QuebPag(20,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0079+'(+)', oFont10n)    //"Taxa ADM: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTaxasAg , "@E 999,999,999.99"), oFont10) 
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0080+'(+)', oFont10n)    //"FEE: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nFEE , "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0088+'(-)', oFont10n)    //"Desconto: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrDesc , "@E 999,999,999.99"), oFont10)
	
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0084+'(+)', oFont10n)    //"MARKUP: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nMarkup , "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0089+'(-)', oFont10n)    //"Repasse: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrRepas , "@E 999,999,999.99"), oFont10)			
			
			If cTPFOP == '1'
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0081+'(+)', oFont10n)    //"DU: "
				oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrDU , "@E 999,999,999.99"), oFont10)
			EndIf

			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0058 , oFont10n)    //"Tot. Receita :"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTotRecClA + nTotRecFor, "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0059 , oFont10n)    //"Valor L�quido :"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrLiqCli, "@E 999,999,999.99"), oFont10)
	  	
			PrinTitCli(oModel,oPrint,nTipo,@nLin)
	
		Else //Cliente (nTipo == 2)
			nLin += QuebPag(20,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0004, oFont10n) //"Emiss�o:"
			oPrint:Say(nLin, nCol2 + 22, AllTrim(DtoC( oModel:GetValue( "G3R_ITENS", "G3R_EMISS" ) ))   , oFont10)
				
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0005, oFont10n)   //"Tarifa Base:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTarifaB + nMarkup, "@E 999,999,999.99") , oFont10)  		
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0006, oFont10n)    //"Cambio:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( oModel:GetValue( "G44_ITENS","G44_TXCAMB"), "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0007, oFont10n)     //"Tarifa Nac.:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTafifaN + nMarkup , "@E 999,999,999.99"), oFont10)  		
				
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0008, oFont10n)    //"Taxas:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTaxas + nVlrDU, "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0078, oFont10n)    //"Extras:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nExtras, "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0079, oFont10n)    //"Taxa ADM: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nTaxasAg , "@E 999,999,999.99"), oFont10) 
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0080, oFont10n)    //"FEE: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nFEECli , "@E 999,999,999.99"), oFont10)
			
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0088, oFont10n)    //"Desconto: "
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrDesc , "@E 999,999,999.99"), oFont10)
		
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0009, oFont10n)    //"Total:"
			oPrint:Say(nLin, nCol2 ,TRANSFORM( nVlrLiqCli -  AcoFee(oModel,'2'), "@E 999,999,999.99"), oFont10) 				
		
			//For�a a inclus�o de uma nova p�gina com cabe�alho
			If nLin > ( oPrint:nVertRes() / oPrint:nFactorVert ) - 120 
				lNewPage := .T.  
			EndIf	 
		
			If lNewPage
				oPrint:EndPage()
				oPrint:StartPage()
				nLin := 40
			Endif
			
			nLin += 35
			oPrint:Say(nLin, 0,STR0001, oFont10) //"Recebemos e aceitamos os itens nas condi��es expostas"
			
			nLin += 15
			oPrint:Say(nLin, 0 ,STR0002, oFont10) //"Aceite em ____/____/________"
			oPrint:Say(nLin, 250  ,"___________________________________", oFont10)
		  	
		  	nLin += 10
		  	oPrint:Say(nLin, 300  ,STR0003 , oFont10) //"Assinatura"
		EndIf
		  	
	  	nLin += 20
	  	oPrint:Say(nLin, 0 ,REPLICATE("_",100), oFont10)
	  	
	  	nLin += 15
		oPrint:Say(nLin, 0 ,STR0076 + DTOC(DATE()), oFont10) //"Data de Emiss�o :"
	  	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, 200, 230)		,STR0077 + UsrFullName( RetCodUsr() ), oFont10) //"Emissor :"
	  	
	  	If oModel:GetModel('G8C_MASTER'):HasField('G8C_USERGI')
	  		nLin += QuebPag(10,@nLin,oPrint)
	  		oPrint:Say(nLin, 0 ,STR0128 + FWLeUserlg("G8C_USERGI",2), oFont10) //"Data de Inclus�o :"
	  		oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, 200, 230)		,STR0130 + FWLeUserlg("G8C_USERGI"), oFont10) //'Usr :'	  		
	  	EndIf
	  	
	  	If oModel:GetModel('G8C_MASTER'):HasField('G8C_USERGA')
	  		nLin += QuebPag(10,@nLin,oPrint)
	  		oPrint:Say(nLin, 0 ,STR0129 + FWLeUserlg("G8C_USERGA",2), oFont10) //"Data de Altera��o :"
	  		oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, 200, 230)		,STR0130 + FWLeUserlg("G8C_USERGA"), oFont10) //'Usr :'		  		
	  	EndIf 
		
		lNewPage := .T.
	Next
	
	If nTipoImp == 3
		If File(cCaminho+StrTran(cNome,"rel","PDF"))
			FErase(cCaminho+StrTran(cNome,"rel","PDF"))
		EndIf
		oPrint:Preview()		
		if !IsBlind()
			ShellExecute("Print", StrTran(cNome,"rel","PDF"), " ", cCaminho, 0) 
		Endif	
	Else
		oPrint:Preview()
	EndIf

	If !lImpAuto 
		If !IsBlind() .AND. ApMsgYesNo(I18N(STR0127, {IIF(nTipo == 1, STR0090, STR0030)})) 	// "Deseja imprimir o #1?"
			FwMsgRun( , {|| TA042DemFi(IIF(nTipo == 1, 2, 1), .T.)}, , I18N(STR0131, {IIF(nTipo == 1, STR0090, STR0030)})) 	// "Imprimindo o #1, aguarde..."	nTipo : (1=Agencia | 2=Cliente) 
		EndIf
	EndIf
	
	TURXNIL(aValAcerto)
Return

/*/{Protheus.doc} TA042CImpC
Fun��o com impress�o do cabe�alho do relat�rio de demonstrativo financeiro
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function TA042CImpC( oPrint, nLin, oModel, oFont10, oFont10n, oFont14n, nTipo )
	Local aArea     := GetArea()
	Local aAreaSA1  := SA1->(GetArea())
	Local aAreaSA2  := SA2->(GetArea())
	Local nAux1	    := 0
	Local nAux2	    := 0	
	Local nAux3	    := 20	
	Local cDemonstr := IIF(nTipo == 1, STR0030, STR0090) //"Demonstrativo"###"Demonstrativo"
	
	nLin := 40
	
	oPrint:Say(nLin, ( ( oPrint:nHorzRes() - oPrint:GetTextWidth( cDemonstr , oFont14n ) ) / 2 ) / oPrint:nFactorHor  ,cDemonstr, oFont14n) //"Demonstrativo"###"Demonstrativo"
	
	nAux1 := ( oPrint:GetTextWidth( STR0031 , oFont10n ) + 10 ) / oPrint:nFactorHor //"Nome da Empresa: "
	nAux1 := IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 + nAux3)
	
	nLin += 20
	oPrint:Say(nLin, 0  ,STR0031	, oFont10n) //"Nome da Empresa: "
	oPrint:Say(nLin, nAux1, SM0->M0_NOMECOM   , oFont10)
	
	nLin += 10
	oPrint:Say(nLin, 0  			,STR0032 	                       ,oFont10n) //"Endere�o: "
	oPrint:Say(nLin, nAux1  	, AllTrim( SM0->M0_ENDCOB ) + IIF( Empty( SM0->M0_COMPCOB ), "", " - " ) + AllTrim( SM0->M0_COMPCOB )   ,oFont10)
	
	nLin += 10
	oPrint:Say(nLin, 0  ,STR0033 	                       ,oFont10n) //"Cidade: "
	oPrint:Say(nLin, nAux1  , AllTrim( SM0->M0_CIDCOB)	      ,oFont10)
	
	nAux2 := ( oPrint:GetTextWidth( STR0033+AllTrim( SM0->M0_CIDCOB ), oFont10 ) + 10 ) / oPrint:nFactorHor
	oPrint:Say(nLin, nAux1 + nAux2, STR0034	      ,   oFont10n) //"Estado: "
	
	nAux2 += ( oPrint:GetTextWidth( STR0034, oFont10n ) + 10 ) / oPrint:nFactorHor  //"Estado: "
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3), AllTrim( SM0->M0_ESTCOB )	      ,   oFont10)
	
	nAux2 += ( oPrint:GetTextWidth( AllTrim( SM0->M0_ESTCOB ), oFont10 ) + 10 ) / oPrint:nFactorHor
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3), STR0035	      ,   oFont10n)  //"CEP: "
	
	nAux2 += ( oPrint:GetTextWidth( STR0035, oFont10n ) + 10 ) / oPrint:nFactorHor //"CEP: "
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5), SM0->M0_CEPCOB	      ,   oFont10) 
	
	nLin += 10
	oPrint:Say(nLin, 0  ,STR0036 	               , oFont10n) //"Fone: "
	oPrint:Say(nLin, nAux1, TransForm(SM0->M0_TEL, "@R (99)9999-9999" ) , oFont10) 
	
	nAux2 := ( oPrint:GetTextWidth( TransForm(SM0->M0_TEL, "@R (99)9999-9999" ), oFont10 ) + 20 ) / oPrint:nFactorHor 
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5)  ,STR0037 	,oFont10n) //"Fax: "
	
	nAux2 += ( oPrint:GetTextWidth( STR0037 , oFont10 ) + 10 ) / oPrint:nFactorHor //"Fax: "
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 10), TransForm(SM0->M0_FAX, "@R (99)9999-9999" ),   oFont10) 

	nLin += 10
	oPrint:Say(nLin, 0  ,STR0038 	           	, oFont10n) //"CNPJ: "
	oPrint:Say(nLin, nAux1  , TransForm(SM0->M0_CGC,"@R 99.999.999/9999-99") , oFont10) 
	
	nLin += 20
	oPrint:Say(nLin, 0  ,Upper(STR0039)	               , oFont10n) //"Demonstrativo"
	
	nLin += 10
	oPrint:Say(nLin, 0  ,STR0115	 , oFont10n) //"Concilia��o: "
	nAux2 := ( oPrint:GetTextWidth( STR0115, oFont10n ) + 10 ) / oPrint:nFactorHor 
	oPrint:Say(nLin, nAux1  ,G8C->G8C_CONCIL	 , oFont10) //G8C->G8C_CONCIL
		
	nLin += 10
	oPrint:Say(nLin, 0  ,STR0004	        	, oFont10n) //"Emiss�o:"
	oPrint:Say(nLin, nAux1  , dToC( G8C->G8C_EMISSA ), oFont10 )
	
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3),STR0041 	        	, oFont10n) //"Vencimento:"
	nAux2 += ( oPrint:GetTextWidth( STR0041, oFont10n ) + 10 ) / oPrint:nFactorHor  //"Vencimento:"
	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + 30), dToC( G8C->G8C_VENCIM ), oFont10 )
	
	SA1->(dbSetOrder(1))
	SA1->(dbSeek(xFilial("SA1") + oModel:GetValue("G3Q_ITENS", "G3Q_CLIENT") + oModel:GetValue("G3Q_ITENS", "G3Q_LOJA")))

	If nTipo == 2 //Cliente
		nLin += 20
		oPrint:Say(nLin, 0  		, Upper(STR0042)	        	, oFont10n) //"Sacado:"
		
		nLin += 10
		oPrint:Say(nLin, 0  		,STR0043   	, oFont10n) //"Nome do Sacado:"
	 	oPrint:Say(nLin, nAux1   ,AllTrim(SA1->A1_NOME)  		 	, oFont10)
	 	
		nLin += 10
	 	oPrint:Say(nLin, 0	,STR0044   	, oFont10n) //"C�digo do Cliente:"
	 	oPrint:Say(nLin, nAux1  ,SA1->A1_COD , oFont10)	
	 	
	 	nAux2 += ( oPrint:GetTextWidth( SA1->A1_COD , oFont10 ) + 10 ) / oPrint:nFactorHor  //"Loja do Cliente:"
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3),STR0111  	, oFont10n) //"Loja do Cliente:"
	 	
	 	nAux2 += ( oPrint:GetTextWidth( STR0111 , oFont10 ) + 10 ) / oPrint:nFactorHor
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5),SA1->A1_LOJA	 , oFont10)	
	
	 	nLin += 10
		oPrint:Say(nLin, 0  ,STR0045   	, oFont10n) //"Endere�o:"
		oPrint:Say(nLin, nAux1  ,SA1->A1_END   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0046   	, oFont10n) //"Complemento:"
		oPrint:Say(nLin, nAux1  ,SA1->A1_COMPLEM   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0047   	, oFont10n) //"CEP:"
		oPrint:Say(nLin, nAux1  ,SA1->A1_CEP   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0056   	, oFont10n) //"Cidade/Estado:"
		oPrint:Say(nLin, nAux1  ,AllTrim(SA1->A1_MUN) + "/" + SA1->A1_EST  , oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( AllTrim(SA1->A1_MUN) + "/" + SA1->A1_EST  , oFont10 ) + 20 ) / oPrint:nFactorHor 
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3),STR0049   	, oFont10n) //"Bairro:"
	 	
	 	nAux2 += ( oPrint:GetTextWidth( STR0049 , oFont10n ) + 10 ) / oPrint:nFactorHor  //"Bairro:"
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5), SA1->A1_BAIRROC , oFont10)	
	 
		nLin += 10
		oPrint:Say(nLin, 0	,STR0051   	, oFont10n) //"Fone:"
		oPrint:Say(nLin, nAux1	,ALLTRIM(TRANSFORM(SA1->A1_DDD+SA1->A1_TEL,"@R (99)9999-9999"))   	, oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( ALLTRIM(TRANSFORM(SA1->A1_DDD+SA1->A1_TEL,"@R (99)9999-9999"))     , oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 +  IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5),STR0052   	, oFont10n) //"Fax:"
		
		nAux2 += ( oPrint:GetTextWidth( STR0052    , oFont10n ) + 10 ) / oPrint:nFactorHor  //"Fax:"
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 10),ALLTRIM(TRANSFORM(SA1->A1_DDD+SA1->A1_FAX,"@R (99)9999-9999"))   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0053   	, oFont10n) //"CGC/CPF:"
		oPrint:Say(nLin, nAux1  ,ALLTRIM(TRANSFORM(SA1->A1_CGC,PesqPict('SA1','A1_CGC')))   	, oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( ALLTRIM(TRANSFORM(SA1->A1_CGC,PesqPict('SA1','A1_CGC')))     , oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 40), STR0054  	, oFont10n) //"Inscr. Estadual:"
		
		nAux2 += ( oPrint:GetTextWidth( STR0054     , oFont10n ) + 10 ) / oPrint:nFactorHor  //"Inscr. Estadual:"
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 50), SA1->A1_INSCR  	, oFont10)		
	 
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0050   	, oFont10n) //"Conta cont�bil:"
		oPrint:Say(nLin, nAux1  ,SA1->A1_CONTA  , oFont10)
		
		nLin += 20
		oPrint:Say(nLin, 0  ,STR0055 + ":"   	, oFont10n) //"End. de Cobran�a:"
		oPrint:Say(nLin, nAux1,SA1->A1_ENDCOB  	, oFont10)	
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0035   	, oFont10n) //"Cep: "
		oPrint:Say(nLin, nAux1,SA1->A1_CEPC  	, oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( SA1->A1_CEPC      , oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3) ,STR0056  	, oFont10n) //"Cidade/Estado:"
			
		nAux2 += ( oPrint:GetTextWidth( STR0056     , oFont10 ) + 10 ) / oPrint:nFactorHor 		 //"Cidade/Estado:"
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + 30),AllTrim( SA1->A1_MUNC) + "/" + SA1->A1_ESTC  	, oFont10)
		
		nLin += 15
		oPrint:Say(nLin, 0  ,STR0057, oFont10) //"Nesta data, por vossa solicita��o, prestamos os servi�os relativos aos itens abaixo:"
	
	Else
		SA2->(dbSetOrder(1))
		SA2->(dbSeek(xFilial("SA2") + oModel:GetValue("G3R_ITENS", "G3R_FORREP") + oModel:GetValue("G3R_ITENS", "G3R_LOJREP")))
			
		nLin += 10
		oPrint:Say(nLin, 0  		,STR0014 	, oFont10n) //Fornecedor: 
	 	oPrint:Say(nLin, nAux1   ,SA2->A2_NOME  		 	, oFont10)
	 	
	 	nLin += 10
	 	oPrint:Say(nLin, 0  		,STR0122   	, oFont10n) //"C�d. Fornec.:"
	 	
	 	nAux2 := ( oPrint:GetTextWidth( STR0122 , oFont10n ) + 10 ) / oPrint:nFactorHor  //"C�d. Fornec.:" 
	 	oPrint:Say(nLin, nAux1 ,SA2->A2_COD , oFont10)	
	 	
	 	nAux2 += ( oPrint:GetTextWidth( STR0122+SA2->A2_COD , oFont10 ) + 10 ) / oPrint:nFactorHor  //"Loja Fornec.:"
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3) ,STR0123   	, oFont10n) //"Loja Fornec.:"
	 	
	 	nAux2 += ( oPrint:GetTextWidth( STR0123, oFont10 ) + 10 ) / oPrint:nFactorHor
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5) ,SA2->A2_LOJA	 , oFont10)	
	 	
	 	nLin += 10
		oPrint:Say(nLin, 0  ,STR0045   	, oFont10n) //"Endere�o:"
		oPrint:Say(nLin, nAux1  ,SA2->A2_END   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0047   	, oFont10n) //"CEP:"
		oPrint:Say(nLin, nAux1  ,SA2->A2_CEP   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0048   	, oFont10n) //"Cidade:"
		oPrint:Say(nLin, nAux1  ,AllTrim(SA2->A2_MUN) + "/" + AllTrim(SA2->A2_EST)  , oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( AllTrim(SA2->A2_MUN) + "/" + AllTrim(SA2->A2_EST)  , oFont10 ) + 10 ) / oPrint:nFactorHor 
	 	oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 10),STR0049   	, oFont10n) //"Bairro:"
	 			
		nAux2 += ( oPrint:GetTextWidth( STR0049   , oFont10n ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 10),SA2->A2_BAIRRO   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0	,STR0051   	, oFont10n) //"Fone:"
		oPrint:Say(nLin, nAux1	,ALLTRIM(TRANSFORM(SA2->A2_DDD+SA2->A2_TEL,"@R (99)9999-9999"))   	, oFont10) //"Fone:"
		
		nAux2 += ( oPrint:GetTextWidth( ALLTRIM(TRANSFORM(SA2->A2_DDD+SA2->A2_TEL,"@R (99)9999-9999"))      , oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3),STR0052   	, oFont10n) //"Fax:"
		
		nAux2 += ( oPrint:GetTextWidth( STR0052    , oFont10n ) + 10 ) / oPrint:nFactorHor  //"Fax:"
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 5),ALLTRIM(TRANSFORM(SA2->A2_DDD+SA2->A2_FAX,"@R (99)9999-9999"))   	, oFont10)
		
		nLin += 10
		oPrint:Say(nLin, 0  ,STR0053   	, oFont10n) //"CGC/CPF:"
		oPrint:Say(nLin, nAux1, TransForm(SA2->A2_CGC,"@R 99.999.999/9999-99") , oFont10)
		
		nAux2 := ( oPrint:GetTextWidth( TransForm(SA2->A2_CGC,"@R 99.999.999/9999-99")     , oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 40),STR0054  	, oFont10n) //"Inscr. Estadual:"
		
		nAux2 += ( oPrint:GetTextWidth( STR0054     , oFont10n ) + 10 ) / oPrint:nFactorHor  //"Inscr. Estadual:"
		oPrint:Say(nLin, nAux1 + IIF(oPrint:nDevice != IMP_SPOOL, nAux2, nAux2 + nAux3 + 50),SA2->A2_INSCR  	, oFont10)		

		nLin += 15
		PrinTitFor(oModel,oPrint,nTipo,@nLin)
	EndIf
	nLin += 15

	RestArea(aAreaSA2)
	RestArea(aAreaSA1)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSA1)
	TURXNIL(aAreaSA2)
Return

/*/{Protheus.doc} ModelDef
Modelo de dados j� com a ordena��o para gera��o do relat�rio de demonstrativo financeiro
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function ModelDef()
	Local oModel	:= {}
	Local oStruG8C 	:= FWFormStruct(1, "G8C", ,    )		//Cabe�alho Conc. Fatura Terrestre
	Local oStruG3P	:= FwFormStruct(1, "G3P", ,.F. )		//Registro de venda
	Local oStruG3R	:= FwFormStruct(1, "G3R", ,.F. )		//Documento de reserva
	Local oStruG3Q	:= FwFormStruct(1, "G3Q", ,.F. )		//Item de Venda
	Local oStruG48A	:= FwFormStruct(1, "G48", ,.F. )		//Acordos Cliente (Pastas Abaixo)
	Local oStruG4CA	:= FwFormStruct(1, "G4C", ,.F. )		//Itens Financeiros (Pastas Abaixo)
	Local oStruG8Y	:= FwFormStruct(1, "G8Y", ,.F. )		//T�tulos faturas
	Local oStruG85	:= FwFormStruct(1, "G85", ,.F. )
	Local oStruG8E	:= FwFormStruct(1, "G8E", ,.F. )
	Local oStruG3S	:= FwFormStruct(1, "G3S", ,.F. ) 		//Passageiros
	Local oStruG3U	:= FwFormStruct(1, "G3U", ,.F. ) 		//2 - Hotel(G3U)
	Local oStruG3V	:= FwFormStruct(1, "G3V", ,.F. ) 		//3 - Carro (G3V)
	Local oStruG3W	:= FwFormStruct(1, "G3W", ,.F. ) 		//4 - Rodovi�rio (G3W)
	Local oStruG3Y	:= FwFormStruct(1, "G3Y", ,.F. ) 		//5 - Cruzeiro (G3Y)
	Local oStruG3X	:= FwFormStruct(1, "G3X", ,.F. ) 		//6 - Trem (G3X)
	Local oStruG42	:= FwFormStruct(1, "G42", ,.F. ) 		//7 - Visto (G42)
	Local oStruG41	:= FwFormStruct(1, "G41", ,.F. ) 		//8 - Seguro (G41)
	Local oStruG40	:= FwFormStruct(1, "G40", ,.F. ) 		//9 - Tour (G40)
	Local oStruG3Z	:= FwFormStruct(1, "G3Z", ,.F. ) 		//10 - Pacote (G3Z)
	Local oStruG43	:= FwFormStruct(1, "G43", ,.F. ) 		//11 - Outros (G43)
	Local oStruG44	:= FwFormStruct(1, "G44", ,.F. ) 		//Tarifas
	Local oStruG46	:= FwFormStruct(1, "G46", ,.F. ) 		//Taxas
	Local oStruG47	:= FwFormStruct(1, "G47", ,.F. ) 		//Extras
	Local oStruG48B	:= FwFormStruct(1, "G48", ,.F. ) 		//Acordos Fornecedores
	Local oStruG49	:= FwFormStruct(1, "G49", ,.F. ) 		//Impostos
	Local oStruG4CB	:= FwFormStruct(1, "G4C", ,.F. ) 		//Itens Financeiros - Fornecedor
	Local oStruG4B	:= FWFormStruct(1, 'G4B', ,.F. ) 		//Entidades Adiicionais
	Local oStruG4E	:= FWFormStruct(1, 'G4E', ,.F. ) 		//Reembolso

	oModel := MPFormModel():New( "TURA042C", /*bPreValidacao*/, /*bPosValidacao*/,{|| .T.}, {|| .T.} )

	oModel:AddFields( "G8C_MASTER", /*cOwner*/, oStruG8C )
	oModel:GetModel( "G8C_MASTER" ):SetOnlyQuery(.T.)
	
	//T�tulos faturas
	oModel:AddGrid( "G8Y_ITENS", "G8C_MASTER", oStruG8Y )
	oModel:SetRelation( "G8Y_ITENS", { { "G8Y_FILIAL", "xFilial('G8Y')" }, { "G8Y_CONCIL", "G8C_CONCIL" }  }, G8Y->( IndexKey( 1 ) ) ) 
	oModel:GetModel( "G8Y_ITENS" ):SetOnlyQuery(.T.)
			
	//Documentos de reserva
	oModel:AddGrid( "G3R_ITENS", "G8C_MASTER", oStruG3R )
	oModel:SetRelation( "G3R_ITENS", { { "G3R_CONCIL", "G8C_CONCIL" }, { "G3R_FILCON", "xFilial('G8C')" } }, G3R->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3R_ITENS" ):SetOnlyQuery(.T.)
	
	//Item de Venda
	oModel:AddGrid( "G3Q_ITENS", "G3R_ITENS", oStruG3Q )
	oModel:SetRelation( "G3Q_ITENS", { { "G3Q_FILIAL", "G3R_FILIAL" }, { "G3Q_NUMID", "G3R_NUMID" }, { "G3Q_IDITEM", "G3R_IDITEM" }, { "G3Q_NUMSEQ", "G3R_NUMSEQ" }, { "G3Q_CONORI", "G8C_CONCIL" } }, G3Q->( IndexKey( 1 ) ) )
	oModel:GetModel( 'G3Q_ITENS' ):SetLoadFilter( { { "G3Q_STATUS", "{'1','2'}", MVC_LOADFILTER_IS_CONTAINED } } )
	oModel:GetModel( "G3Q_ITENS" ):SetOnlyQuery(.T.)
		
	//Acordos de Cliente	
	oModel:AddGrid( "G48A_ITENS", "G3Q_ITENS", oStruG48A )
	oModel:SetRelation("G48A_ITENS",{{"G48_FILIAL","G3Q_FILIAL"},{"G48_NUMID" ,"G3Q_NUMID"},{"G48_IDITEM","G3Q_IDITEM"},{"G48_NUMSEQ","G3Q_NUMSEQ"},{"G48_CLIFOR","'1'"},{"G48_CONINU","''"}},G48->(IndexKey(1)))
	oModel:GetModel( "G48A_ITENS" ):SetOnlyQuery(.T.)

	//Registro de Venda
	oModel:AddFields( "G3P_FIELDS", "G3Q_ITENS", oStruG3P )
	oModel:SetRelation( "G3P_FIELDS", { { "G3Q_FILIAL", "G3Q_FILIAL" }, { "G3P_NUMID", "G3Q_NUMID" } }, G3P->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3P_FIELDS" ):SetOnlyQuery(.T.)

	//Tarifas
	oModel:AddGrid( "G44_ITENS", "G3Q_ITENS", oStruG44 )
	oModel:SetRelation( "G44_ITENS", { { "G44_FILIAL", "G3Q_FILIAL" }, { "G44_NUMID", "G3Q_NUMID" }, { "G44_IDITEM", "G3Q_IDITEM" }, { "G44_NUMSEQ", "G3Q_NUMSEQ" }, { "G44_CONORI", "G8C_CONCIL" } }, G44->( IndexKey( 1 ) ) )
	oModel:GetModel( "G44_ITENS" ):SetOnlyQuery( .T. )

	//Itens Financeiros (Pastas Abaixo)
	oModel:AddGrid( "G4CA_ITENS", "G3Q_ITENS", oStruG4CA )
	oModel:SetRelation( "G4CA_ITENS", { { "G4C_FILIAL", "G3Q_FILIAL" }, { "G4C_NUMID", "G3Q_NUMID" }, { "G4C_IDITEM", "G3Q_IDITEM" }, { "G4C_NUMSEQ", "G3Q_NUMSEQ" }, { "G4C_CLIFOR", "'1'" }, { "G4C_TIPO", "'1'" },{ "G4C_CONORI", "G8C_CONCIL" } }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CA_ITENS" ):SetOnlyQuery(.T.)	

	//G85
	oModel:AddGrid( "G85_ITENS", "G4CA_ITENS", oStruG85 )
	oModel:SetRelation( "G85_ITENS", { { "G85_FILIAL", "G4C_FILIAL" }, { "G85_IDIF", "G4C_IDIF" }, { "G85_PREFIX", "G4C_PREFIX" },{ "G85_NUMFAT", "G4C_NUMFAT" }} , G85->( IndexKey( 1 ) ) )
	oModel:GetModel( "G85_ITENS" ):SetOnlyQuery(.T.)
	
	//G8E
	oModel:AddGrid( "G8E_ITENS", "G85_ITENS", oStruG8E )
	oModel:SetRelation( "G8E_ITENS", { { "G8E_FILIAL", "G85_FILIAL" }, { "G8E_PREFIX", "G85_PREFIX" },{ "G8E_NUMFAT", "G85_NUMFAT" },{"G8E_TIPO", "'3'"}} , G8E->( IndexKey( 1 ) ) )
	oModel:GetModel( "G8E_ITENS" ):SetOnlyQuery(.T.)
	
	//Passageiros da reserva
	oModel:AddGrid( "G3S_ITENS", "G3R_ITENS", oStruG3S )
	oModel:SetRelation( "G3S_ITENS", { { "G3S_FILIAL", "G3R_FILIAL" }, { "G3S_NUMID", "G3R_NUMID" }, { "G3S_IDITEM", "G3R_IDITEM" }, { "G3S_NUMSEQ", "G3R_NUMSEQ" }, { "G3S_CONORI", "G8C_CONCIL" } }, G3S->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3S_ITENS" ):SetOnlyQuery( .T. )

	//Entidades Adicionais
	oModel:AddGrid( "G4B_ITENS", "G3S_ITENS", oStruG4B )
	oModel:SetRelation("G4B_ITENS",{{"G4B_FILIAL","G3S_FILIAL"},{"G4B_NUMID" ,"G3S_NUMID"},{"G4B_IDITEM","G3S_IDITEM"},{"G4B_NUMSEQ","G3S_NUMSEQ"},{"G4B_CLIENT","G3Q_CLIENT"},{"G4B_CODPAX","G3S_CODPAX"}},G4B->(IndexKey(1)))
	oModel:GetModel("G4B_ITENS"):SetOnlyQuery(.T.)	
	
	//2 - Hotel(G3U)
	oModel:AddGrid( "G3U_ITENS", "G3Q_ITENS", oStruG3U  )
	oModel:SetRelation( "G3U_ITENS", { { "G3U_FILIAL", "G3Q_FILIAL" }, { "G3U_NUMID", "G3Q_NUMID" }, { "G3U_IDITEM", "G3Q_IDITEM" }, { "G3U_NUMSEQ", "G3Q_NUMSEQ" }, { "G3U_CONORI", "G8C_CONCIL" } }, G3U->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3U_ITENS" ):SetOnlyQuery( .T. )

	//3 - Carro (G3V)
	oModel:AddGrid( "G3V_ITENS", "G3Q_ITENS", oStruG3V )
	oModel:SetRelation( "G3V_ITENS", { { "G3V_FILIAL", "G3Q_FILIAL" }, { "G3V_NUMID", "G3Q_NUMID" }, { "G3V_IDITEM", "G3Q_IDITEM" }, { "G3V_NUMSEQ", "G3Q_NUMSEQ" }, { "G3V_CONORI", "G8C_CONCIL" } }, G3V->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3V_ITENS" ):SetOnlyQuery( .T. )

	//4 - Rodovi�rio (G3W)
	oModel:AddGrid( "G3W_ITENS", "G3Q_ITENS", oStruG3W )
	oModel:SetRelation( "G3W_ITENS", { { "G3W_FILIAL", "G3Q_FILIAL" }, { "G3W_NUMID", "G3Q_NUMID" }, { "G3W_IDITEM", "G3Q_IDITEM" }, { "G3W_NUMSEQ", "G3Q_NUMSEQ" }, { "G3W_CONORI", "G8C_CONCIL" } }, G3W->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3W_ITENS" ):SetOnlyQuery( .T. )

	//5 - Cruzeiro (G3Y)
	oModel:AddGrid( "G3Y_ITENS", "G3Q_ITENS", oStruG3Y )
	oModel:SetRelation( "G3Y_ITENS", { { "G3Y_FILIAL", "G3Q_FILIAL" }, { "G3Y_NUMID", "G3Q_NUMID" }, { "G3Y_IDITEM", "G3Q_IDITEM" }, { "G3Y_NUMSEQ", "G3Q_NUMSEQ" },  { "G3Y_CONORI", "G8C_CONCIL" } }, G3Y->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Y_ITENS" ):SetOnlyQuery( .T. )

	//6 - Trem (G3X)
	oModel:AddGrid( "G3X_ITENS", "G3Q_ITENS", oStruG3X )
	oModel:SetRelation( "G3X_ITENS", { { "G3X_FILIAL", "G3Q_FILIAL" }, { "G3X_NUMID", "G3Q_NUMID" }, { "G3X_IDITEM", "G3Q_IDITEM" }, { "G3X_NUMSEQ", "G3Q_NUMSEQ" },  { "G3X_CONORI", "G8C_CONCIL" } }, G3X->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3X_ITENS" ):SetOnlyQuery( .T. )

	//7 - Visto (G42)
	oModel:AddGrid( "G42_ITENS", "G3Q_ITENS", oStruG42 )
	oModel:SetRelation( "G42_ITENS", { { "G42_FILIAL", "G3Q_FILIAL" }, { "G42_NUMID", "G3Q_NUMID" }, { "G42_IDITEM", "G3Q_IDITEM" }, { "G42_NUMSEQ", "G3Q_NUMSEQ" },  { "G42_CONORI", "G8C_CONCIL" } }, G42->( IndexKey( 1 ) ) )
	oModel:GetModel( "G42_ITENS" ):SetOnlyQuery( .T. )

	//8 - Seguro (G41)
	oModel:AddGrid( "G41_ITENS", "G3Q_ITENS", oStruG41 )
	oModel:SetRelation( "G41_ITENS", { { "G41_FILIAL", "G3Q_FILIAL" }, { "G41_NUMID", "G3Q_NUMID" }, { "G41_IDITEM", "G3Q_IDITEM" }, { "G41_NUMSEQ", "G3Q_NUMSEQ" },  { "G41_CONORI", "G8C_CONCIL" } }, G41->( IndexKey( 1 ) ) )
	oModel:GetModel( "G41_ITENS" ):SetOnlyQuery( .T. )

	//9 - Tour (G40)
	oModel:AddGrid( "G40_ITENS", "G3Q_ITENS", oStruG40 )
	oModel:SetRelation( "G40_ITENS", { { "G40_FILIAL", "G3Q_FILIAL" }, { "G40_NUMID", "G3Q_NUMID" }, { "G40_IDITEM", "G3Q_IDITEM" }, { "G40_NUMSEQ", "G3Q_NUMSEQ" }, { "G40_CONORI", "G8C_CONCIL" } }, G40->( IndexKey( 1 ) ) )
	oModel:GetModel( "G40_ITENS" ):SetOnlyQuery( .T. )

	//10 - Pacote (G3Z)
	oModel:AddGrid( "G3Z_ITENS", "G3Q_ITENS", oStruG3Z )
	oModel:SetRelation( "G3Z_ITENS", { { "G3Z_FILIAL", "G3Q_FILIAL" }, { "G3Z_NUMID", "G3Q_NUMID" }, { "G3Z_IDITEM", "G3Q_IDITEM" }, { "G3Z_NUMSEQ", "G3Q_NUMSEQ" },  { "G3Z_CONORI", "G8C_CONCIL" } }, G3Z->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Z_ITENS" ):SetOnlyQuery( .T. )

	//11 - Outros (G43)
	oModel:AddGrid( "G43_ITENS", "G3Q_ITENS", oStruG43 )
	oModel:SetRelation( "G43_ITENS", { { "G43_FILIAL", "G3Q_FILIAL" }, { "G43_NUMID", "G3Q_NUMID" }, { "G43_IDITEM", "G3Q_IDITEM" }, { "G43_NUMSEQ", "G3Q_NUMSEQ" }, { "G43_CONORI", "G8C_CONCIL" } }, G43->( IndexKey( 1 ) ) )
	oModel:GetModel( "G43_ITENS" ):SetOnlyQuery( .T. )
	
	//Reembolso
	oModel:AddGrid( "G4E_ITENS", "G3R_ITENS", oStruG4E )
	oModel:SetRelation( "G4E_ITENS", { { "G4E_FILIAL", "G3R_FILIAL" }, { "G4E_NUMID", "G3R_NUMID" }, { "G4E_IDITEM", "G3R_IDITEM" }, { "G4E_NUMSEQ", "G3R_NUMSEQ" } }, G4E->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4E_ITENS" ):SetOnlyQuery( .T. )
	
	oModel:SetOnDemand( .T. )
	
Return oModel

/*/{Protheus.doc} PrintSegm
(long_description)
@type function
@author osmar.junior
@since 14/10/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@param oPrint, objeto, (Descri��o do par�metro)
@param nTipo, num�rico, (Descri��o do par�metro)
@param nLin, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function PrintSegm(oModel,oPrint,nTipo,nLin)
	Local oFont10 	:= TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
	Local oFont10n	:= TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
	Local cTpSeg	:= oModel:GetValue( "G3R_ITENS", "G3R_TPSEG" )
	Local nAux1		:= 0
	Local cPosto	:= oModel:GetValue( "G3Q_ITENS","G3Q_POSTO" ) + " - " +Alltrim(POSICIONE('G3M',1,xFilial("G3M")+oModel:GetValue("G3Q_ITENS","G3Q_POSTO"),'G3M_DESCR' ))
	Local cDescFOp 	:= oModel:GetValue( "G3Q_ITENS","G3Q_FORMPG" )+' - '+Posicione("G3N",1,xFilial("G3N")+oModel:GetValue( "G3Q_ITENS","G3Q_FORMPG" ),"G3N->G3N_DESCR")
	Local nX        := 0
	Local cFilG3Q   := oModel:GetValue( "G3Q_ITENS","G3Q_FILIAL" )
	Local cNumID    := oModel:GetValue( "G3Q_ITENS","G3Q_NUMID" )
	Local cIDItem   := oModel:GetValue( "G3Q_ITENS","G3Q_IDITEM" )
	Local cNumSeq   := oModel:GetValue( "G3Q_ITENS","G3Q_NUMSEQ" )
	Local nCol1     := 50   // coluna dos textos fixos
	Local nCol2     := IIF(oPrint:nDevice != IMP_SPOOL, 145, 165)  // coluna dos dados do banco
	Local nColR     := IIF(oPrint:nDevice != IMP_SPOOL, 120, 140)
	Local aAreaSA1  := SA1->(GetArea())

	SA1->(dbSetOrder(1))
	SA1->(dbSeek(xFilial("SA1") + oModel:GetValue("G3Q_ITENS", "G3Q_CLIENT") + oModel:GetValue("G3Q_ITENS", "G3Q_LOJA")))

	//EMPRESA - FILIAL - POSTO DE ATENDIMENTO
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, nCol1  ,STR0091+Alltrim(SA1->A1_NOME) , oFont10n)
	
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, nCol1  , STR0093+ '    ' + Alltrim(cPosto) , oFont10n)
	
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, nCol1  ,STR0092 + Alltrim(cFilG3Q) +" "+ STR0112+cNumID +" "+ STR0113+cIDItem +" "+ STR0114+cNumSeq, oFont10n)
	
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, nCol1  ,STR0124 + Alltrim(cDescFOp) , oFont10n) //'Formas de Pagamnento: '
		
	If oModel:GetValue("G3Q_ITENS", "G3Q_OPERAC") <> '2'
		If cTpSeg == "2"
			nLin += QuebPag(20,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0011)	 	, oFont10) //"Hotel"
			
			For nX := 1 to oModel:GetModel( "G3U_ITENS" ):Length()
				oModel:GetModel( "G3U_ITENS" ):GoLine( nX )
			
				nLin += QuebPag(20,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0012	 , oFont10n) //"Hospede:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3U_ITENS","G3U_CODPAX") + "-" + oModel:GetValue("G3U_ITENS","G3U_NOMPAX")	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013 , oFont10n) //"Voucher:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) + " - " + oModel:GetValue("G3U_ITENS","G3U_CIDHOT")	 , oFont10)
				
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
						
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0015	 , oFont10n) //"IN:"
				oPrint:Say(nLin, nCol2 , AllTrim(DToC( oModel:GetValue("G3U_ITENS","G3U_DTINI") ))	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0016	 , oFont10n) //"OUT:"
				oPrint:Say(nLin, nCol2 + 120 , DToC( oModel:GetValue("G3U_ITENS","G3U_DTFIM") )	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0017	 , oFont10n) //"N�m. Noites:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G3U_ITENS","G3U_QTDDIA"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3U_ITENS","G3U_TPPROD") )  , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0019	 , oFont10n) //"Quantidade:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G3U_ITENS","G3U_QTDPRD"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3U_ITENS","G3U_CODPAX"))
				
			Next nX 
			
		ElseIf cTpSeg == "3"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0021)	 	, oFont10) //"Ve�culo"
			
			For nX := 1 to oModel:GetModel( "G3V_ITENS" ):Length()
				oModel:GetModel( "G3V_ITENS" ):GoLine( nX )
				
				nLin += QuebPag(20,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3V_ITENS","G3V_CODPAX") + "-" + oModel:GetValue("G3V_ITENS","G3V_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Voucher:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	 
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
				
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:"
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G3V_ITENS","G3V_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70  ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G3V_ITENS","G3V_DTFIM") )	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0063	 , oFont10n) //"Diarias:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G3V_ITENS","G3V_QTDDIA"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") )   , oFont10)
											
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0064	 , oFont10n) //"Categoria:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G3V_ITENS","G3V_CATAUT"))	 , oFont10)
					
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0019	 , oFont10n) //"Quantidade:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G3V_ITENS","G3V_QTDPRD"))	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3V_ITENS","G3V_CODPAX"))
									
			Next nX
			
		ElseIf cTpSeg == "4"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0022)	 , oFont10) //"Rodovi�rio"
			
			For nX := 1 to oModel:GetModel( "G3W_ITENS" ):Length()
				oModel:GetModel( "G3W_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3W_ITENS","G3W_CODPAX") + "-" + oModel:GetValue("G3W_ITENS","G3W_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
				
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
							
				nLin += QuebPag(10,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G3W_ITENS","G3W_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G3W_ITENS","G3W_DTFIM") )	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065	 , oFont10n) //"Cidade de Embarque:""
				oPrint:Say(nLin, nCol2 , AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3W_ITENS","G3W_CIDEMB"), "G5S_CIDADE"))	 , oFont10)
	
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0066	 , oFont10n) //"Cidade de Desmbarque:"
				oPrint:Say(nLin, nCol2 , AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3W_ITENS","G3W_CIDDES"), "G5S_CIDADE"))	 , oFont10)			
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3W_ITENS","G3W_CATBUS") )  , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3W_ITENS","G3W_CODPAX"))
									
			Next nX
											
		ElseIf cTpSeg == "5"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0023)	 , oFont10) //"Cruzeiro"
		
			For nX := 1 to oModel:GetModel( "G3Y_ITENS" ):Length()
				oModel:GetModel( "G3Y_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Y_ITENS","G3Y_CODPAX") + "-" + oModel:GetValue("G3Y_ITENS","G3Y_NOMPAX")	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
			
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G3Y_ITENS","G3Y_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G3Y_ITENS","G3Y_DTFIM") )	 , oFont10)
				
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3Y_ITENS","G3Y_CIDPRI") )  , oFont10)
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0067	 , oFont10n) //"Navio:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Y_ITENS","G3Y_NOMNAV")	 , oFont10)								
					
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065 , oFont10n) //"Cidade de Embarque:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3Y_ITENS","G3Y_CIDEMB"), "G5S_CIDADE"))	 , oFont10)	
											
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3Y_ITENS","G3Y_CODPAX"))
									
			Next nX							
										
		ElseIf cTpSeg == "6"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0024)	 , oFont10) //"Trem"
			
			For nX := 1 to oModel:GetModel( "G3X_ITENS" ):Length()
				oModel:GetModel( "G3X_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3X_ITENS","G3X_CODPAX") + "-" + oModel:GetValue("G3X_ITENS","G3X_NOMPAX")	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
			
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf	
								
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G3X_ITENS","G3X_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G3X_ITENS","G3X_DTFIM") )	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3X_ITENS","G3X_DESFER") )  , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0064 , oFont10n) //"Categoria:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3X_ITENS","G3X_CTTREM")	 , oFont10)								
					
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0068, oFont10n) //"N�mero:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3X_ITENS","G3X_NRTREM")	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065	 , oFont10n) //"Cidade embarque:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3X_ITENS","G3X_CIDEMB"), "G5S_CIDADE"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0069	 , oFont10n) //"Cidade de Destino:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3X_ITENS","G3X_CIDDES"), "G5S_CIDADE"))	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3X_ITENS","G3X_CODPAX"))
									
			Next nX
									
		ElseIf cTpSeg == "7"
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0025)	 , oFont10) //"Visto"
		
			For nX := 1 to oModel:GetModel( "G42_ITENS" ):Length()
				oModel:GetModel( "G42_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G42_ITENS","G42_CODPAX") + "-" + oModel:GetValue("G42_ITENS","G42_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
			
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf					
						
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0070	 , oFont10n) //"Pais de visto:"
				oPrint:Say(nLin, nCol2 , AllTrim(Posicione('SYA', 1, xFilial('SYA') + oModel:GetValue("G42_ITENS","G42_PAIS"), 'YA_DESCR'))	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0071	 , oFont10n) //"Tipo de Visto:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G42_ITENS","G42_TPVIST")	 , oFont10)	
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G42_ITENS","G42_CODPAX"))
				
			Next nX						
												
		ElseIf cTpSeg == "8"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0026)	 , oFont10) //"Seguro"
		
			For nX := 1 to oModel:GetModel( "G41_ITENS" ):Length()
				oModel:GetModel( "G41_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G41_ITENS","G41_CODPAX") + "-" + oModel:GetValue("G41_ITENS","G41_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
				
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
				
				nLin += QuebPag(10,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G41_ITENS","G41_DTINI") )	 , oFont10)
				nLin += QuebPag(10,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G41_ITENS","G41_DTFIM") )	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0072	 , oFont10n) //"Nome do Plano:"
				oPrint:Say(nLin, nCol2 ,AllTrim( Posicione("G4G",1,xFilial("G4G")+oModel:GetValue("G41_ITENS","G41_PLSEGU"),"G4G->G4G_NOME"))	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0073	 , oFont10n) //"Benefici�rio:"
				oPrint:Say(nLin, nCol2 ,AllTrim(oModel:GetValue("G41_ITENS","G41_APOLIC"))	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0074	 , oFont10n) //"Ap�lice:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G41_ITENS","G41_NUMAPO")	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G41_ITENS","G41_CODPAX"))
									
			Next nX
										
		ElseIf cTpSeg == "9"
			nLin += QuebPag(10,@nLin,oPrint)
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0027)	 , oFont10) //"Tour"
		
			For nX := 1 to oModel:GetModel( "G40_ITENS" ):Length()
				oModel:GetModel( "G40_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G40_ITENS","G40_CODPAX") + "-" + oModel:GetValue("G40_ITENS","G40_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
					
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
				
				nLin += QuebPag(10,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G40_ITENS","G40_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G40_ITENS","G40_DTFIM") )	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) + " / " + ;
											AllTrim( oModel:GetValue("G40_ITENS","G40_NMTOUR") )  , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0075 , oFont10n) //"Cidade:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G40_ITENS","G40_CIDPRI"), "G5S_CIDADE"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065	 , oFont10n) //"Cidade de embarque:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G40_ITENS","G40_CIDEMB"), "G5S_CIDADE"))	 , oFont10)
					
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G40_ITENS","G40_CODPAX"))
										
			Next nX
									
		ElseIf cTpSeg == "A"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0028)	 , oFont10) //"Pacote"
		
			For nX := 1 to oModel:GetModel( "G3Z_ITENS" ):Length()
				oModel:GetModel( "G3Z_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Z_ITENS","G3Z_CODPAX") + "-" + oModel:GetValue("G3Z_ITENS","G3Z_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
				
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
					
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G3Z_ITENS","G3Z_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G3Z_ITENS","G3Z_DTFIM") )	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0075	 , oFont10n) //"Cidade principal:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3Z_ITENS","G3Z_CIDPRI"), "G5S_CIDADE")) , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065	 , oFont10n) //"Cidade de embarque:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G3Z_ITENS","G3Z_CIDEMB"), "G5S_CIDADE"))	 , oFont10)	
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G3Z_ITENS","G3Z_CODPAX"))
									
			Next nX
										
		ElseIf cTpSeg == "B"
			nLin += QuebPag(10,@nLin,oPrint) 
			oPrint:Say(nLin, nCol1 ,STR0010	 , oFont10n) //"Descri��o:"
			oPrint:Say(nLin, nCol2 ,Upper(STR0029)	 	, oFont10) //"Outros"
		
			For nX := 1 to oModel:GetModel( "G43_ITENS" ):Length()
				oModel:GetModel( "G43_ITENS" ):GoLine( nX )
					
				nLin += QuebPag(20,@nLin,oPrint) 
				oPrint:Say(nLin, nCol1 ,STR0060	 , oFont10n) //"Passageiro:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G43_ITENS","G43_CODPAX") + "-" + oModel:GetValue("G43_ITENS","G43_NOMPAX")	 , oFont10)
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0013	 , oFont10n) //"Documento:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
			
				If nTipo == 2	
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor:"
					oPrint:Say(nLin, nCol2 , AllTrim( Posicione('SA2', 1, xFilial('SA2') + oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")), 'A2_NOME') ) , oFont10)
					
					nLin += QuebPag(10,@nLin,oPrint)
					oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ:"
					oPrint:Say(nLin, nCol2 , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
				EndIf
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0061	 , oFont10n) //"Data Inicial:""
				oPrint:Say(nLin, nCol2 , DToC( oModel:GetValue("G43_ITENS","G43_DTINI") )	 , oFont10)
				oPrint:Say(nLin, nCol2 +  70 ,STR0062	 , oFont10n) //"Data Final  :"
				oPrint:Say(nLin, nCol2 + 150 , DToC( oModel:GetValue("G43_ITENS","G43_DTFIM") )	 , oFont10)		
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0063	 , oFont10n) //"Diarias:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G43_ITENS","G43_QTDDIA"))	 , oFont10)
							
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
				oPrint:Say(nLin, nCol2 ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
											AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) , oFont10)
											
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0019	 , oFont10n) //"Quantidade:"
				oPrint:Say(nLin, nCol2 ,cValToChar(oModel:GetValue("G43_ITENS","G43_QTDPRD"))	 , oFont10)
			
			
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0065	 , oFont10n) //"Cidade de partida:"
				oPrint:Say(nLin, nCol2 ,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G43_ITENS","G43_CIDEMB"), "G5S_CIDADE"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0069	 , oFont10n) //"Cidade de destino:"
				oPrint:Say(nLin, nCol2,AllTrim(Posicione("G5S", 1, xFilial("G5S") + oModel:GetValue("G43_ITENS","G43_CIDDES"), "G5S_CIDADE"))	 , oFont10)
				
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
				oPrint:Say(nLin, nCol2 ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
				
				ImprimEnt(@nLin,oModel,oPrint,oModel:GetValue("G43_ITENS","G43_CODPAX"))
									
			Next nX
									
		EndIf
		
	Else //Reembolso

		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0013 , oFont10n) 
		oPrint:Say(nLin, nColR ,oModel:GetValue("G3R_ITENS","G3R_DOC")	 , oFont10)
	
		nLin += QuebPag(20,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0014	 , oFont10n) //"Fornecedor: "
		oPrint:Say(nLin, nColR , AllTrim( POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_NOME') ) , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0038	 , oFont10n) //"CNPJ: "
		oPrint:Say(nLin, nColR , TRANSFORM(POSICIONE("SA2",1, xFilial("SA2")+oModel:GetValue("G3R_ITENS","G3R_FOR"+IIf(nTipo==1,"REP","NEC"))+oModel:GetValue("G3R_ITENS","G3R_LOJ"+IIf(nTipo==1,"REP","A")),'A2_CGC'), PesqPict('SA2','A2_CGC') ) , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0018	 , oFont10n) //"Produto:"
		oPrint:Say(nLin, nColR ,	AllTrim( oModel:GetValue("G3R_ITENS","G3R_PROD") ) + " / " + ;
									AllTrim( oModel:GetValue("G3R_ITENS","G3R_DESPRD") ) , oFont10)
	
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0020	 , oFont10n) //"Solicitante:"
		oPrint:Say(nLin, nColR ,oModel:GetValue("G3Q_ITENS","G3Q_NOMESO")	 , oFont10)	
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0095	 , oFont10n) //"Doc. Solic.: "
		oPrint:Say(nLin, nColR ,oModel:GetValue("G4E_ITENS","G4E_DTSOLI")	 , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0096	 , oFont10n) //"Mot. Reembolso: "
		oPrint:Say(nLin, nColR ,POSICIONE("G8P",1, xFilial("G8P")+oModel:GetValue("G4E_ITENS","G4E_MOTREE"),'G8P_DESCR')	 , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0097	 , oFont10n) //"Tipo Ree. :"
		oPrint:Say(nLin, nColR ,IIF(oModel:GetValue("G4E_ITENS","G4E_TPREE")=='1',STR0107,STR0108)	 , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0098	 , oFont10n) //"Dest. Fin.: "
		oPrint:Say(nLin, nColR ,IIF(oModel:GetValue("G4E_ITENS","G4E_DESTFN")=='1',STR0109,STR0110)	 , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0099	 , oFont10n) //"Data Prev.: "
		oPrint:Say(nLin, nColR ,DToC(oModel:GetValue("G4E_ITENS","G4E_DTPREV"))	 , oFont10)
		
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0100	 , oFont10n) //"Data Cred.: "
		oPrint:Say(nLin, nColR ,DToC(oModel:GetValue("G4E_ITENS","G4E_DTCRED"))	 , oFont10)
			
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0101	, oFont10n)    //'Tarifa Paga: '
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_TARPAG") , "@E 999,999,999.99"), oFont10)
				
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0102	, oFont10n)    //'Tar. Reemb.: '
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_TREEMB") , "@E 999,999,999.99"), oFont10)
					
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0103	, oFont10n)    //'Taxas Pagas.: '
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_TXPG") , "@E 999,999,999.99"), oFont10)
						
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0104	, oFont10n)    //'Taxas Reemb.:'
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_TXREEM") , "@E 999,999,999.99"), oFont10)
							
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,STR0105	, oFont10n)    //'Extras Pagos: '
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_EXTPG") , "@E 999,999,999.99"), oFont10)
				
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol1 ,'Extras reemb.: ', oFont10n)    //'Extras reemb.: '
		oPrint:Say(nLin, nColR  ,TRANSFORM( oModel:GetValue("G4E_ITENS","G4E_EXREEM") , "@E 999,999,999.99"), oFont10)

	EndIf
	
	RestArea(aAreaSA1)
Return

/*/{Protheus.doc} ImprimEnt
(long_description)
@type function
@author osmar.junior
@since 14/10/2016
@version 1.0
@param nLin, num�rico, (Descri��o do par�metro)
@param oModel, objeto, (Descri��o do par�metro)
@param oPrint, objeto, (Descri��o do par�metro)
@param cPax, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function ImprimEnt(nLin,oModel,oPrint,cPax)
Local aArea      := GetArea()
Local oFont10 	 := TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
Local oFont10n	 := TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
Local cAliasEntA := GetNextAlias()
Local nCol       := 50
Local nAux1      := 0
	
BeginSQL Alias cAliasEntA

	SELECT	G4B.G4B_TITUL, G4B.G4B_ITEM, G4B.G4B_DESENT
	FROM	%Table:G4B% G4B
	WHERE	G4B.G4B_NUMID 	= %Exp:oModel:GetValue('G3R_ITENS','G3R_NUMID')%
		AND	G4B.G4B_IDITEM 	= %Exp:oModel:GetValue('G3R_ITENS','G3R_IDITEM')%
		AND	G4B.G4B_NUMSEQ 	= %Exp:oModel:GetValue('G3R_ITENS','G3R_NUMSEQ')%
		AND	G4B.G4B_CODPAX 	= %Exp:cPax%
		AND	G4B.G4B_FILIAL 	= %xFilial:G4B%
		AND	G4B.G4B_CONINU 	= ''
		AND	G4B.%NotDel%

EndSQL

(cAliasEntA)->(dbGoTop())
If (cAliasEntA)->(!Eof())
	While (cAliasEntA)->(!Eof())
		nLin += QuebPag(10,@nLin,oPrint)
		oPrint:Say(nLin, nCol,AllTrim((cAliasEntA)->G4B_TITUL) + ":  "	 , oFont10n) //"Entidade: "

		nAux1 := ( oPrint:GetTextWidth( AllTrim((cAliasEntA)->G4B_TITUL) + ":  ", oFont10 ) + 10 ) / oPrint:nFactorHor 
		oPrint:Say(nLin, nCol + IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 + 30) + (Len(AllTrim((cAliasEntA)->G4B_TITUL))*5) + 10 ,AllTrim((cAliasEntA)->G4B_ITEM) + " - " + AllTrim((cAliasEntA)->G4B_DESENT), oFont10)	

		(cAliasEntA)->(DbSkip())
	EndDo
EndIf		
(cAliasEntA)->( DbCloseArea() )

If nLin > ( oPrint:nVertRes() / oPrint:nFactorVert ) - 160
	oPrint:StartPage()
	nLin := 20			 
EndIf

RestArea(aArea)

TURXNIL(aArea)

Return 	

/*/{Protheus.doc} PrinTitFor
(long_description)
@type function
@author osmar.junior
@since 14/10/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@param oPrint, objeto, (Descri��o do par�metro)
@param nTipo, num�rico, (Descri��o do par�metro)
@param nLin, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function PrinTitFor(oModel,oPrint,nTipo,nLin)
	Local aArea     := GetArea()
	Local oFont10 	:= TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
	Local oFont10n	:= TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
	Local nX        := 0
	Local nTotFat	:= 0
	Local nPos      := 0
	Local aTitulos	:= {}
	Local nAux1     := IIF(oPrint:nDevice != IMP_SPOOL, 85, 95)

	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, 0  ,STR0116	 , oFont10n) //"Fatura: "
	oPrint:Say(nLin, nAux1 ,oModel:GetValue( "G8C_MASTER","G8C_FATURA" ) 	 , oFont10)	
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, 0  ,STR0117	 , oFont10n) //T�tulos Fornec.:  N�mero      Prefixo Parcela Vencimento     Valor    Status     Saldo

	DbSelectArea("SE2")
	SE2->(DbsetOrder(1))
	For nX := 1 to oModel:GetModel( "G8Y_ITENS" ):Length()
		oModel:GetModel( "G8Y_ITENS" ):GoLine( nX )
		
		If SE2->(DbSeek(oModel:GetValue( "G8Y_ITENS","G8Y_FILREF" )+oModel:GetValue( "G8Y_ITENS","G8Y_SERIE" )+oModel:GetValue( "G8Y_ITENS","G8Y_DOC" )+oModel:GetValue( "G8Y_ITENS","G8Y_PARCEL" )+oModel:GetValue( "G8Y_ITENS","G8Y_TIPO" )+oModel:GetValue( "G8Y_ITENS","G8Y_FORNEC" )+oModel:GetValue( "G8Y_ITENS","G8Y_LOJA" )))
			AADD(aTitulos,{oModel:GetValue( "G8Y_ITENS","G8Y_TPDOC" ),SE2->E2_NUM,SE2->E2_PREFIXO,SE2->E2_PARCELA,SE2->E2_VENCREA,SE2->E2_VALOR,StsTitulo(  SE2->E2_SALDO,SE2->E2_VALOR ),SE2->E2_SALDO })			
		Endif	
		
	Next nX
	SE2->(DbCloseArea())
	
	aTitulos := ASORT( aTitulos, , ,{ | X,Y| X[1] > Y[1] } )
		
	If (nPos := aScan(aTitulos,{|x| x[1] == '8'})) > 0
		nLin += QuebPag(10,@nLin,oPrint)	
		oPrint:Say(nLin, nAux1 , aTitulos[nPos][2]+'    '+ aTitulos[nPos][3]+'       '+aTitulos[nPos][4]+'    '+ Alltrim(DTOC(aTitulos[nPos][5]))+'     '+ Alltrim(TRANSFORM(aTitulos[nPos][6],"@E 999,999,999.99"))+'    '+ aTitulos[nPos][7]+'       '+Alltrim(TRANSFORM(aTitulos[nPos][8],"@E 999,999,999.99"))  , oFont10)
		nTotFat += aTitulos[nPos][8]
	ElseIf (Ascan(aTitulos,{|x| x[1] == '2'}) > 0)
		For nX := 1 to Len(aTitulos)
			If aTitulos[nX][1] == '2' .AND. aTitulos[nx][8] > 0 //Saldo
				nLin += QuebPag(10,@nLin,oPrint)	
				oPrint:Say(nLin, nAux1 , aTitulos[nx][2]+'    '+ aTitulos[nX][3]+'       '+aTitulos[nX][4]+'    '+ Alltrim(DTOC(aTitulos[nX][5]))+'     '+ Alltrim(TRANSFORM(aTitulos[nX][6],"@E 999,999,999.99"))+'    '+ aTitulos[nX][7]+'       '+Alltrim(TRANSFORM(aTitulos[nX][8],"@E 999,999,999.99"))  , oFont10)
				nTotFat += aTitulos[nx][8]
			EndIf
		Next nX
	EndIf

	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, 0  ,STR0125	 , oFont10n) 	//"Total: "
	oPrint:Say(nLin, nAux1  ,Alltrim(TRANSFORM(nTotFat,"@E 999,999,999.99"))	 , oFont10)
	
	nLin += QuebPag(10,@nLin,oPrint)
	
	RestArea(aArea)
	
	TURXNIL(aArea)
Return	

/*/{Protheus.doc} StsTitulo
(long_description)
@type function
@author osmar.junior
@since 14/10/2016
@version 1.0
@param nSaldo, num�rico, (Descri��o do par�metro)
@param nValor, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function StsTitulo( nSaldo,nValor )
Local cRet:= ''

If nSaldo == 0
	cret := STR0118	//'Baixado'
ElseIf nSaldo == 	nValor
	cret := STR0119	//'Aberto'
ElseIf nSaldo >= 0.01
	cRet := STR0120	//'Baixado parcialmente'
EndIf	
 
Return cRet

/*/{Protheus.doc} PrinTitCli
(long_description)
@type function
@author osmar.junior
@since 14/10/2016
@version 1.0
@param ${param}, ${param_type}, ${param_descr}
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function PrinTitCli(oModel,oPrint,nTipo,nLin)
	Local aArea    := GetArea()
	Local oFont10  := TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
	Local oFont10n := TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
	Local nX       := 0
	Local nTotFat  := 0
	Local nAux1    := IIF(oPrint:nDevice != IMP_SPOOL, 85, 95)

	nLin += QuebPag(20,@nLin,oPrint)
	oPrint:Say(nLin, 0  ,STR0121	 , oFont10n) //T�tulos cliente:  N�mero      Prefixo Parcela Vencimento     Valor    Status

	If oModel:GetValue( "G4CA_ITENS", "G4C_TPFOP" ) == '1' //Intermedi��o
		DbSelectArea("SE1")
		SE1->(DbsetOrder(1))
		For nX := 1 to oModel:GetModel( "G8E_ITENS" ):Length()
			oModel:GetModel( "G8E_ITENS" ):GoLine( nX )
			
			If SE1->(DbSeek(oModel:GetValue( "G8E_ITENS","G8E_FILREF" )+oModel:GetValue( "G8E_ITENS","G8E_PREFIX" )+oModel:GetValue( "G8E_ITENS","G8E_NUMFAT" )+oModel:GetValue( "G8E_ITENS","G8E_PARCEL" )+oModel:GetValue( "G8E_ITENS","G8E_TIPOTI" )))
				nLin += QuebPag(10,@nLin,oPrint)
				oPrint:Say(nLin, nAux1 , SE1->E1_NUM +'    '+ SE1->E1_PREFIXO+'       '+SE1->E1_PARCELA+'    '+ Alltrim(DTOC(SE1->E1_VENCREA))+'     '+ Alltrim(TRANSFORM(SE1->E1_VALOR,"@E 999,999,999.99"))+'      '+ StsTitulo(  SE1->E1_SALDO,SE1->E1_VALOR )+'       '+ Alltrim(TRANSFORM(SE1->E1_SALDO,"@E 999,999,999.99")) , oFont10)	
				nTotFat += SE1->E1_VALOR
			Endif	
				
		Next nX		
		SE1->(DbCloseArea())
	EndIf
	
	nLin += QuebPag(10,@nLin,oPrint)
	oPrint:Say(nLin, 0  ,STR0125	 , oFont10n) 	//"Total: "
	oPrint:Say(nLin, nAux1  ,Alltrim(TRANSFORM(nTotFat,"@E 999,999,999.99"))	 , oFont10)
	
	nLin += QuebPag(20,@nLin,oPrint)
	
	RestArea(aArea)
	
	TURXNIL(aArea)
Return	

/*/{Protheus.doc} PrintClien
(long_description)
@type function
@author osmar.junior
@since 17/10/2016
@version 1.0
@param oPrint, objeto, (Descri��o do par�metro)
@param nLin, num�rico, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function PrintClien(oPrint,nLin)
	Local oFont10 	:= TFont():New("Courier New",9,10,.T.,.F.,5,.T.,5,.T.,.F.)
	Local oFont10n	:= TFont():New("Courier New",9,10,.T.,.T.,5,.T.,5,.T.,.F.)
	Local nAux1		:= 50
	Local nAux2		:= 30
	Local cNome		:= AllTrim(SA1->A1_NOME) 
	Local cCodCli	:= SA1->A1_COD
	Local cLojaCli	:= SA1->A1_LOJA	

	nLin += QuebPag(20,@nLin,oPrint)
	
	oPrint:Say(nLin, nAux1   ,STR0109+":"  		 	, oFont10n)
	
	nAux1 += ( oPrint:GetTextWidth(STR0109, oFont10n ) + 10 ) / oPrint:nFactorHor
	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 += 15) ,Alltrim(cNome)  		 	, oFont10)
	
	nAux1 += ( oPrint:GetTextWidth(Alltrim(cNome), oFont10 ) + 10 ) / oPrint:nFactorHor	 		
 	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 + nAux2) ,STR0040 , oFont10n)
 	
 	nAux1 += ( oPrint:GetTextWidth(STR0040, oFont10 ) + 10 ) / oPrint:nFactorHor	 		
 	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 += nAux2) ,Alltrim(cCodCli) , oFont10)	
 	 	
 	nAux1 += ( oPrint:GetTextWidth( cCodCli , oFont10 ) + 10 ) / oPrint:nFactorHor
 	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 + nAux2) ,STR0111   	, oFont10n) //"Loja do Cliente:"
 
 	nAux1 += ( oPrint:GetTextWidth( STR0111 , oFont10 ) + 10 ) / oPrint:nFactorHor
 	oPrint:Say(nLin, IIF(oPrint:nDevice != IMP_SPOOL, nAux1, nAux1 + nAux2) ,cLojaCli	 , oFont10)	
Return

/*/{Protheus.doc} Tur42cVal
(long_description)
@type function
@author osmar.junior
@since 28/10/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@return ${aValores}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function Tur42cVal(oModel)
Local aValores   := {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
Local cAliasAcer := GetNextAlias()
Local nTarifaB	 := 0
Local nTafifaN	 := 0
Local nMarkup	 := 0
Local nTaxas	 := 0
Local nExtras	 := 0
Local nTaxasAg	 := 0
Local nFEE		 := 0
Local nTotRecCli := 0
Local nTotRecFor := 0
Local nVlrLiqCli := 0
Local nVlrLiqFor := 0
Local nVlrDU	 := 0
Local nVlrCom	 := 0
Local nVlrDesc	 := 0
Local nVlrRepas	 := 0
Local nVlrInc	 := 0
Local nVlrTxFor	 := 0
	
BeginSQL Alias cAliasAcer

	SELECT		 G44.G44_TARBAS,
				 G44.G44_APLICA
				,G3Q.G3Q_MARKUP
				,G3Q.G3Q_TAXA
				,G3Q.G3Q_EXTRA
				,G3Q.G3Q_TAXAAG
				,G3Q.G3Q_FEE
				,G3Q.G3Q_TOTREC
				,G3Q.G3Q_VLFIM
				,G3Q.G3Q_TAXADU
				,G3Q.G3Q_DESC
				,G3Q.G3Q_REPASS
				,G3R.G3R_VLINCE
				,G3R.G3R_VLCOMI
				,G3R.G3R_TOTREC
				,G3R.G3R_VLFIM
				,G3R.G3R_TXFORN
	FROM		%Table:G3Q% G3Q 
				INNER JOIN %Table:G3R% G3R ON 
					G3R_FILIAL = %xFilial:G3R% 	AND 									 
					G3R.G3R_NUMID  = G3Q.G3Q_NUMID	AND 
					G3R.G3R_IDITEM = G3Q.G3Q_IDITEM AND 
					G3R.G3R_NUMSEQ = G3Q.G3Q_NUMSEQ	AND
					G3R.G3R_CONINU = '' AND
					G3R.G3R_ACERTO = '1' AND
					G3R.G3R_ACRAUT = '1' AND
					G3R.%notDel%
				LEFT JOIN %Table:G44% G44 ON 
					G44_FILIAL = %xFilial:G44% 	AND 									 
					G3R.G3R_NUMID  = G44.G44_NUMID	AND 
					G3R.G3R_IDITEM = G44.G44_IDITEM AND 
					G3R.G3R_NUMSEQ = G44.G44_NUMSEQ	AND
					G44.G44_CONINU = '' AND
					G44.%NotDel%
	WHERE		G3Q.G3Q_NUMID 	= %Exp:oModel:GetValue('G3Q_ITENS','G3Q_NUMID')%
	AND			G3Q.G3Q_IDITEM 	= %Exp:oModel:GetValue('G3Q_ITENS','G3Q_IDITEM')%	
	AND			G3Q.G3Q_FILIAL 	= %xFilial:G3Q%
	AND			G3Q.G3Q_CONINU = '' 
	AND			G3Q.G3Q_ACERTO 	= '1'	
	AND			G3Q.%NotDel%

EndSQL

(cAliasAcer)->(dbGoTop())
If (cAliasAcer)->(!Eof())
	While (cAliasAcer)->(!Eof())
		nTarifaB   += (cAliasAcer)->G44_TARBAS
	 	nTafifaN   += (cAliasAcer)->G44_APLICA
	 	nMarkup	   += (cAliasAcer)->G3Q_MARKUP
	 	nTaxas	   += (cAliasAcer)->G3Q_TAXA
	 	nExtras	   += (cAliasAcer)->G3Q_EXTRA
	 	nTaxasAg   += (cAliasAcer)->G3Q_TAXAAG
	 	nFEE	   += (cAliasAcer)->G3Q_FEE
	 	nTotRecCli += (cAliasAcer)->G3Q_TOTREC
	 	nTotRecFor += (cAliasAcer)->G3R_TOTREC
	 	nVlrLiqCli += (cAliasAcer)->G3Q_VLFIM
	 	nVlrLiqFor += (cAliasAcer)->G3R_VLFIM
	 	nVlrDU	   += (cAliasAcer)->G3Q_TAXADU
	 	nVlrCom	   += (cAliasAcer)->G3R_VLCOMI
	 	nVlrDesc   += (cAliasAcer)->G3Q_DESC
	 	nVlrRepas  += (cAliasAcer)->G3Q_REPASS
	 	nVlrInc	   += (cAliasAcer)->G3R_VLINCE
		nVlrTxFor  += (cAliasAcer)->G3R_TXFORN
			
		(cAliasAcer)->(DbSkip())
	EndDo
	(cAliasAcer)->( dbCloseArea() )
		
	aValores := {nTarifaB, nTafifaN, nMarkup, nTaxas, nExtras, nTaxasAg, nFEE, nTotRecCli, nTotRecFor, nVlrLiqCli, nVlrLiqFor, nVlrDU, nVlrCom, nVlrDesc, nVlrRepas, nVlrInc, nVlrTxFor}
EndIf		

Return aValores

/*/{Protheus.doc} AcoFee
(long_description)
@type function
@author osmar.junior
@since 13/12/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@param cComSer, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AcoFee(oModel,cComSer)
Local nFEE			:= 0
Local nItemAcordo	:= 0

For nItemAcordo := 1 To oModel:GetModel('G48A_ITENS'):Length()
	oModel:GetModel('G48A_ITENS'):GoLine(nItemAcordo)

	If !oModel:GetModel('G48A_ITENS'):isDeleted() .AND. oModel:GetModel('G48A_ITENS'):GetValue("G48_COMSER") == cComSer .AND. oModel:GetModel('G48A_ITENS'):GetValue("G48_CLASS") $ "C01|C05" .AND. oModel:GetModel('G48A_ITENS'):GetValue("G48_STATUS") != '3' 			
		nFEE += oModel:GetModel('G48A_ITENS'):GetValue("G48_VLACD")
	EndIf
Next nItemAcordo

Return nFEE

/*/{Protheus.doc} AcoFee
(long_description)
@type function
@author osmar.junior
@since 13/12/2016
@version 1.0
@param oModel, objeto, (Descri��o do par�metro)
@param cComSer, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function QuebPag(nLi,nLin,oPrint)
Local nVLi := nLi + 20

If nLin > ( oPrint:nVertRes() / oPrint:nFactorVert ) - nVLi 
	oPrint:EndPage()
	oPrint:StartPage()
	nLin := 40
Endif

Return nLi