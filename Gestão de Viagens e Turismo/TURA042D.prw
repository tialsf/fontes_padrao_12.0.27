#INCLUDE "TURA042D.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

/*/{Protheus.doc} ModelDef
ModelDef para apresenta��o da vis�o financeira
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel	:= Nil
	Local oStruG8C 	:= FWFormStruct(1, "G8C", , .F.)	//Cabe�alho Conc. Fatura Terrestre
	Local oStruG3R	:= FwFormStruct(1, "G3R", , .F.)	//Documento de reserva
	Local oStruG3Q	:= FwFormStruct(1, "G3Q", , .F.)	//Item de venda
	Local oStruG48	:= FwFormStruct(1, "G48", , .F.)	//Acordos Aplicados
	Local oStruG4C	:= FwFormStruct(1, "G4C", , .F.) 	//Itens Financeiros
	Local oStruG9K	:= FwFormStruct(1, "G9K", , .F.)	//Demonstrativo Financeiro - Fornecedor

	TA042VtFld( , , , , , , oStruG3R )
	TA034StruM( /*oStruG3P*/, /*oStruG3Q*/, oStruG3R )
	
	oModel := MPFormModel():New( "TURA042D", /*bPreValidacao*/, /*bPosValidacao*/,{|| .T.}, /*bCancel*/ )

	oModel:AddFields( "G8C_MASTER", /*cOwner*/, oStruG8C )
	oModel:GetModel( "G8C_MASTER" ):SetOnlyQuery ( .T. )

	oModel:AddGrid( "G3R_ITENS", "G8C_MASTER", oStruG3R )
	oModel:SetRelation( "G3R_ITENS", { { "G3R_CONCIL", "G8C_CONCIL" }, { "G3R_FILCON", "xFilial('G8C')" }, {"G3R_STATUS",'"1"'} }, G3R->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3R_ITENS" ):SetLoadFilter( { { "G3R_TPSEG", "'1'", MVC_LOADFILTER_NOT_EQUAL } } )
	oModel:GetModel( "G3R_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3R_ITENS" ):SetOnlyQuery ( .T. )
	oModel:GetModel( 'G3R_ITENS' ):SetNoInsertLine(.T.)
	oModel:GetModel( 'G3R_ITENS' ):SetNoDeleteLine(.T.)
	
	//Item de Venda
	oModel:AddGrid( "G3Q_ITENS", "G3R_ITENS", oStruG3Q )
	oModel:SetRelation( "G3Q_ITENS", { { "G3Q_FILIAL", "G3R_FILIAL" }, { "G3Q_NUMID", "G3R_NUMID" }, { "G3Q_IDITEM", "G3R_IDITEM" }, { "G3Q_NUMSEQ", "G3R_NUMSEQ" }, { "G3Q_CONORI", "G8C_CONCIL" } }, G3Q->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Q_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G3Q_ITENS" ):SetOnlyQuery ( .T. )
	oModel:GetModel( "G3Q_ITENS" ):SetDescription( 'xxx' )
	oModel:GetModel( 'G3Q_ITENS' ):SetNoInsertLine(.T.)
	oModel:GetModel( 'G3Q_ITENS' ):SetNoDeleteLine(.T.)
	
	//Acordos Fornecedor
	oModel:AddGrid( "G48B_ITENS", "G3R_ITENS", oStruG48 )
	oModel:SetRelation( "G48B_ITENS", { { "G48_FILIAL", "G3R_FILIAL" }, { "G48_NUMID", "G3R_NUMID" }, { "G48_IDITEM", "G3R_IDITEM" }, { "G48_NUMSEQ", "G3R_NUMSEQ" }, { "G48_CLIFOR", "'2'" }, { "G48_CONORI", "G8C_CONCIL" }, { "G48_RECCON", '"T"' } }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48B_ITENS" ):SetOptional( .T. )
	oModel:GetModel( "G48B_ITENS" ):SetOnlyQuery ( .T. )
	oModel:GetModel( 'G48B_ITENS' ):SetNoInsertLine(.T.)
	oModel:GetModel( 'G48B_ITENS' ):SetNoDeleteLine(.T.)

	oModel:AddGrid( "G4CI_ITENS", "G3R_ITENS", oStruG4C )
	oModel:SetRelation("G4CI_ITENS",{{"G4C_FILIAL","G3R_FILIAL"}, { "G4C_NUMID", "G3R_NUMID" }, { "G4C_IDITEM", "G3R_IDITEM" }, { "G4C_NUMSEQ", "G3R_NUMSEQ" } ,{ "G4C_CONORI", "G8C_CONCIL" },{"G4C_CLIFOR","'2'"},{"G4C_CARTUR","''"},{"G4C_CONINU","''"}},G4C->(IndexKey(1)))
	oModel:GetModel( 'G4CI_ITENS' ):SetOnlyQuery ( .T. )
	oModel:GetModel( 'G4CI_ITENS' ):SetOptional( .T. )
	oModel:GetModel('G4CI_ITENS'):SetNoInsertLine(.T.)
	oModel:GetModel('G4CI_ITENS'):SetNoDeleteLine(.T.)

	oModel:AddGrid( "G4CR_ITENS", "G3R_ITENS", oStruG4C )
	oModel:SetRelation("G4CR_ITENS",{{"G4C_FILIAL","G3R_FILIAL"}, { "G4C_NUMID", "G3R_NUMID" }, { "G4C_IDITEM", "G3R_IDITEM" }, { "G4C_NUMSEQ", "G3R_NUMSEQ" } ,{ "G4C_CONORI", "G8C_CONCIL" },{"G4C_CLIFOR","'2'"},{"G4C_CARTUR","''"},{"G4C_CONINU","''"}},G4C->(IndexKey(1)))
	oModel:GetModel( 'G4CR_ITENS' ):SetOnlyQuery ( .T. )
	oModel:GetModel( 'G4CR_ITENS' ):SetOptional( .T. )
	oModel:GetModel('G4CR_ITENS'):SetNoInsertLine(.T.)
	oModel:GetModel('G4CR_ITENS'):SetNoDeleteLine(.T.)
	
	oModel:GetModel( 'G4CI_ITENS' ):SetLoadFilter( { { 'G4C_TIPO', "{'1','2','5'}", MVC_LOADFILTER_IS_CONTAINED } } )
	oModel:GetModel( 'G4CR_ITENS' ):SetLoadFilter( { { 'G4C_TIPO', "{'3','4'}"	  , MVC_LOADFILTER_IS_CONTAINED } } )

	oModel:AddGrid( "G9KB_ITENS", "G3R_ITENS", oStruG9K )
	oModel:SetRelation("G9KB_ITENS",{{"G9K_FILIAL","G3R_FILIAL"},{"G9K_NUMID" ,"G3R_NUMID"},{"G9K_IDITEM","G3R_IDITEM"},{"G9K_NUMSEQ","G3R_NUMSEQ"},{"G9K_CLIFOR","'2'"},{ "G9K_CONORI", "G8C_CONCIL" }},G9K->(IndexKey(1)))
	oModel:GetModel('G9KB_ITENS'):SetOnlyQuery(.T.)
	oModel:GetModel('G9KB_ITENS'):SetOptional(.T.)
	oModel:GetModel('G9KB_ITENS'):SetNoInsertLine(.T.)
	oModel:GetModel('G9KB_ITENS'):SetNoDeleteLine(.T.)

	oModel:SetVldActivate({|oModel| TA042DVlAct(oModel)})
	oModel:SetActivate({|| TA042DAtVF()})

Return oModel

/*/{Protheus.doc} ViewDef
ViewDef para apresenta��o da vis�o financeira
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ViewDef()
	Local oView	   := FWFormView():New()
	Local oModel   := FWLoadModel( "TURA042D")
	Local oStruG8C := FWFormStruct( 2, "G8C" )
	Local oStruG3R := FWFormStruct( 2, "G3R" )
	Local oStruG48 := FWFormStruct( 2, "G48" )
	Local oStruG4C := FwFormStruct( 2, "G4C" )

	oView:SetModel( oModel )

	oStruG48:RemoveField("G48_OK")
	oStruG48:RemoveField("G48_RECCON")

	oStruG8C:SetProperty('*', MVC_VIEW_CANCHANGE, .F.) 
	oStruG3R:SetProperty('*', MVC_VIEW_CANCHANGE, .F.) 
	oStruG48:SetProperty('*', MVC_VIEW_CANCHANGE, .F.) 
	oStruG4C:SetProperty('*', MVC_VIEW_CANCHANGE, .F.) 

	oView:AddField("VIEW_G8C" , oStruG8C, "G8C_MASTER")
	oView:AddGrid( "VIEW_G3R" , oStruG3R, "G3R_ITENS" )
	oView:AddGrid( "VIEW_G48B", oStruG48, "G48B_ITENS")
	oView:AddGrid( "VIEW_G4CI", oStruG4C, "G4CI_ITENS")
	oView:AddGrid( "VIEW_G4CR", oStruG4C, "G4CR_ITENS")

	// Divis�o Horizontal
	oView:CreateHorizontalBox( "ALL",100 )
	oView:CreateFolder("FOLDER_ALL"	,"ALL")

	// Pastas
	oView:AddSheet("FOLDER_ALL","ABA_ALL_CABEC", STR0009) //"Concilia��o"
	oView:AddSheet("FOLDER_ALL","ABA_ALL_VISAO", STR0010) //"Vis�o Financeira"
	
	oView:CreateHorizontalBox( "CONCILIACAO_CONTENT", 100, , , "FOLDER_ALL","ABA_ALL_CABEC"	)
	oView:CreateHorizontalBox( "LINHASUPERIOR"		, 030, , , "FOLDER_ALL","ABA_ALL_VISAO"	)
	oView:CreateHorizontalBox( "ACORDOS_CONTENT"	, 030, , , "FOLDER_ALL","ABA_ALL_VISAO"	)
	oView:CreateHorizontalBox( "FINANCEIRO_CONTENT"	, 040, , , "FOLDER_ALL","ABA_ALL_VISAO"	)
	
	oView:CreateVerticalBox( "DR_CONTENT"	, 080, "LINHASUPERIOR", , "FOLDER_ALL","ABA_ALL_VISAO" )
	oView:CreateVerticalBox( "TOTAL_CONTENT", 020, "LINHASUPERIOR", , "FOLDER_ALL","ABA_ALL_VISAO" )

	oView:CreateFolder("FOLDER_FINANCEIRO", "FINANCEIRO_CONTENT")
	oView:AddSheet( "FOLDER_FINANCEIRO", "FOLDER_PAGAR" 	, STR0011 	) //"Intermedia��o"
	oView:AddSheet( "FOLDER_FINANCEIRO", "FOLDER_RECEBER"	, STR0012 	) //"Receitas"

	oView:CreateHorizontalBox( "BOX_G4CI", 100, , , "FOLDER_FINANCEIRO", "FOLDER_PAGAR")
	oView:CreateHorizontalBox( "BOX_G4CR", 100, , , "FOLDER_FINANCEIRO", "FOLDER_RECEBER")

	oView:AddOtherObject("TOTAL_IF", {|oPanel| Tur042IFTot(oPanel) })

	oView:SetOwnerView( "VIEW_G8C"	, "CONCILIACAO_CONTENT")
	oView:SetOwnerView( "VIEW_G3R"	, "DR_CONTENT" )
	oView:SetOwnerView( "VIEW_G48B"	, "ACORDOS_CONTENT" )
	oView:SetOwnerView( "VIEW_G4CI"	, "BOX_G4CI" )
	oView:SetOwnerView( "VIEW_G4CR"	, "BOX_G4CR" )
	oView:SetOwnerView( "TOTAL_IF"	, "TOTAL_CONTENT")
	
Return oView

/*/{Protheus.doc} Tur042IFTot
Apresentar Gets com totalizadores
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param oDlg, objeto, objeto do OTHER_OBJECTS
/*/
Static Function Tur042IFTot(oDlg)
	Local nValRec	:= 0
	Local nValInt	:= 0
	Local nValLiq	:= 0
	Local oValRec 	:= nil
	Local oValInt  	:= nil
	Local oValLiq 	:= nil

	TA042DVal( @nValRec, @nValInt, @nValLiq, .T. )
	
	@ 005,003 SAY STR0003 OF oDlg PIXEL SIZE 120,9  //'TOTAIS'
	@ 020,003 SAY STR0011 OF oDlg PIXEL SIZE 120,9  //"Intermedia��o"
	@ 020,070 MSGET oValInt VAR nValInt OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth/2-080,010 HASBUTTON

	@ 035,003 SAY STR0012 OF oDlg PIXEL SIZE 120,9  //"Receitas"
	@ 035,070 MSGET oValRec VAR nValRec OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth/2-080,010 HASBUTTON

	@ 050,003 SAY STR0013 OF oDlg PIXEL SIZE 120,9  //"Valor L�q."
	@ 050,070 MSGET oValLiq VAR nValLiq OF oDlg PICTURE "@E 999,999,999.99" WHEN .F. PIXEL SIZE oDlg:nClientWidth/2-080,010 HASBUTTON

Return

/*/{Protheus.doc} TA042DVal
Fun��o com calculo de valores de receita da vis�o financeira
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param nValRec, num�rico, Variavel por refer�ncia para armazenar valor a receber
@param nValPag, num�rico, Variavel por refer�ncia para armazenar valor a pagar
@param nValFat, num�rico, Variavel por refer�ncia para armazenar valor da fatura
@param lAllItF, l�gico  	, Indica que itens financeiros finalizados ir�o compor o valor do resultado final
/*/
Function TA042DVal( nValRec, nValInt, nValLiq, lAllItF )
	Local nX			:= 0
	Local nItem			:= 0
	Local nItemDr		:= 0
	Local nVlrReceber	:= 0
	Local nVlrPagar		:= 0	
	Local oModel		:= FwModelActive()
	Local oModelG3R		:= oModel:GetModel( 'G3R_ITENS' )
	Local oModelInt		:= oModel:GetModel( 'G4CI_ITENS' )
	Local oModelRec		:= oModel:GetModel( 'G4CR_ITENS' )

	DEFAULT lAllItF	:= .F.
	DEFAULT nValRec	:= 0
	DEFAULT nValInt	:= 0
	DEFAULT nValLiq	:= 0

	For nItemDr := 1 To oModelG3R:Length()
		oModelG3R:GoLine( nItemDr ) 
		
		For nItem := 1 To oModelInt:Length()
			oModelInt:GoLine(nItem)
	
			If lAllItF
				//Se n�o estiver em 1=Ag. Libera��o ou Finalizado e Tp Conc. = Terrestre, vai para o pr�ximo registro
				If !( oModelInt:GetValue("G4C_STATUS") == "1" .Or. ( oModelInt:GetValue("G4C_STATUS") == "4" .And. oModelInt:GetValue("G4C_TPCONC") == "2" ) )
					Loop
				EndIf			
			Else
				If oModelInt:GetValue("G4C_STATUS") <> "1"
					Loop
				EndIf
			EndIf
	
			If !oModelInt:isDeleted()
				TA42DAddVl( oModelInt, @nValInt, @nValRec )
			EndIf
		Next nItem
	
		For nItem := 1 To oModelRec:Length()
			oModelRec:GoLine(nItem)
	
			If lAllItF
				//Se n�o estiver em 1=Ag. Libera��o ou Finalizado e Tp Conc. = Terrestre, vai para o pr�ximo registro
				If !( oModelRec:GetValue("G4C_STATUS") == "1" .Or. ( oModelRec:GetValue("G4C_STATUS") == "4" .And. oModelRec:GetValue("G4C_TPCONC") == "2" ) )
					Loop
				EndIf			
			Else
				If oModelRec:GetValue("G4C_STATUS") <> "1"
					Loop
				EndIf
			EndIf

			If !oModelRec:isDeleted()
				TA42DAddVl( oModelRec, @nValInt, @nValRec )
			EndIf
					
		Next nItem
	
	Next nItemDr


	//Se Intermedia��o > 0, valor a pagar, sen�o valor a receber
	If nValInt > 0	
		nVlrPagar += nValInt
	Else	
		nVlrReceber	+= nValInt * -1
	EndIf
	
	//Se Receitas > 0, valor a receber, sen�o valor a pagar 
	If nValRec > 0
		nVlrReceber	+= nValRec
	Else
		nVlrPagar += nValRec * -1
	EndIf
	
	nValLiq := nVlrPagar - nVlrReceber

Return

/*/{Protheus.doc} TA042DAtVF
Fun��o para atualizar os campos virtuais de valores do modelo G3R_ITENS
@type function
@author Anderson Toledo
@since 04/07/2016
@version 1.0
/*/
Function TA042DAtVF()
	Local aArea      := GetArea()
	Local aAreaG8B   := G8B->(GetArea())     
	Local aValues	 := { { "G3R_TXFORN", 0 }, { "G3R_VLCOMI", 0 }, { "G3R_VLINCE", 0 } }
	Local oModel 	 := FwModelActive()
	Local oModelG48B := oModel:GetModel( "G48B_ITENS" )
	Local cClass	 := ""
	Local cField     := ""
	Local nPos		 := 0
	Local nX		 := 0

	G8B->( dbSetOrder(1) )
	For nX := 1 to oModelG48B:Length()
		oModelG48B:GoLine(nx)

		cClass	:= oModel:GetValue( "G48B_ITENS", "G48_CLASS" )

		If G8B->( dbSeek( xFilial( "G8B" ) + cClass ) ) .And. G8B->G8B_CLIFOR == "2"
			cField := G8B->G8B_DESTIN
		
			If !Empty( cField ) .And. oModel:GetModel("G3R_ITENS"):HasField( cField  )
				If oModel:GetValue( "G48B_ITENS", "G48_RECCON" )
					If (nPos := aScan( aValues, {|x| x[1] == cField } )) > 0
						aValues[nPos][2] += oModel:GetValue( "G48B_ITENS", "G48_VLACD" )
					EndIf	
				EndIf
			EndIf
		EndIf
	Next

	For nX := 1 to len(aValues)
		oModel:SetValue( "G3R_ITENS", aValues[nX][1], aValues[nX][2] )
	Next

	oModel:lModify := .F.
	
	RestArea(aAreaG8B)
	RestArea(aArea)
	
	TURXNIL(aArea)
	TURXNIL(aAreaG8B)
	TURXNIL(aValues)
Return

/*/{Protheus.doc} TA42DAddVl
Fun��o para c�lculo do valor do item financeiro de acordo com o tipo e se a pagar/receber
@type function
@author Anderson Toledo
@since 19/07/2016
@version 1.0
/*/
Function TA42DAddVl( oModel, nValInter, nValRec )
	
	If oModel:GetValue('G4C_TIPO') == '1' //Itens de Venda
		If oModel:GetValue('G4C_PAGREC') == '1'
			nValInter += oModel:GetValue("G4C_VALOR")
		Else
			nValInter -= oModel:GetValue("G4C_VALOR")
		EndIf
	ElseIf oModel:GetValue('G4C_TIPO') $ '2|5' //Itens de Reembolso
		If oModel:GetValue('G4C_PAGREC') == '1'
			nValInter += oModel:GetValue("G4C_VALOR")
		Else
			nValInter -= oModel:GetValue("G4C_VALOR")	
		EndIf
	ElseIf oModel:GetValue('G4C_TIPO') == '3' //Itens de Receita
		If oModel:GetValue('G4C_PAGREC') == '1'
			nValRec -= oModel:GetValue("G4C_VALOR")
		Else
			nValRec += oModel:GetValue("G4C_VALOR")
		EndIf
	ElseIf oModel:GetValue('G4C_TIPO') == '4' //Itens de Abatimento de Receita
		If oModel:GetValue('G4C_PAGREC') == '1'
			nValRec -= oModel:GetValue("G4C_VALOR")
		Else
			nValRec += oModel:GetValue("G4C_VALOR")
		EndIf
	EndIf
Return


/*/{Protheus.doc} TA042DVlAct
Valida��es para opera��es na visao financeira
@type 		function
@author 	Thiago Tavares
@since 		05/12/2017
@version	1.0
@param 		oModel, objeto, Objeto instanciado da ModelDef
/*/
Static Function TA042DVlAct( oModel )
	Local lRet := .T.

	If G8C->G8C_STATUS != "2" 
		lRet := .F.
		oModel:SetErrorMessage("G8C_MASTER", "G8C_STATUS", "G8C_MASTER", "G8C_STATUS", "TA042DAVlAct", STR0014, STR0015)		// "Concilia��o j� efetivada ou efetivada parcialmente."		"Para detalhes financeiros visualize o Log de Efetiva��o."	  
	EndIf
		
Return lRet