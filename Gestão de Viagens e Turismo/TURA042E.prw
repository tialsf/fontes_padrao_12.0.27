#INCLUDE "TURA042E.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"
#Include "FWEditPanel.ch"

Static oTblHeader	:= nil //Tabela tempor�ria com os campos referente a pesquisa
Static oTblDetail	:= nil //Tabela tempor�ria com os campos referente aos DR's encontrados

/*/{Protheus.doc} ModelDef
ModelDef para pesquisa de documentos de Reserva
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel	 := MPFormModel():New( "TURA042E", /*bPreValidacao*/, /*bPosValidacao*/,{|oModel| TA042ECommit(oModel) }, {|oModel| FWFormCancel(oModel) } )
	Local oStructCab := FWFormModelStruct():New()
	Local oStructDet := FWFormModelStruct():New()

	TA042ECrTb()

	oStructCab:AddTable(oTblHeader:GetAlias(), , STR0001, {|| oTblHeader:GetRealName()}) //'Pesquisa'
	oStructDet:AddTable(oTblDetail:GetAlias(), , STR0002, {|| oTblDetail:GetRealName()}) //'Documentos de reserva'

	AddFldCab( oStructCab, 1 )
	AddFldDet( oStructDet, 1 )

	AddGatilhos( oStructCab, oStructDet )

	oModel:AddFields( "TMP_MASTER", /*cOwner*/, oStructCab )

	oModel:AddGrid(	"TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, /*bLoad*/)
	oModel:SetRelation( "TMP_DETAIL", { }, ( oTblDetail:GetAlias() )->( IndexKey( 1 ) ) )

	oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)
	
	oModel:SetDeActivate( {|| TA042EDestroy() } )
	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
ViewDef para pesquisa de documentos de Reserva
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function ViewDef()
	Local oView		 := FWFormView():New()
	Local oModel   	 := FWLoadModel( "TURA042E" )
	Local oStructCab := FWFormViewStruct():New()
	Local oStructDet := FWFormViewStruct():New()

	oView:SetModel( oModel )

	AddFldCab( oStructCab, 2 )
	AddFldDet( oStructDet, 2 )

	AddGroups( oStructCab )

	oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER" )
	oView:AddGrid( "VIEW_TMPD", oStructDet, "TMP_DETAIL" )

	oView:AddUserButton( STR0003+"-F5", "CLIPS", {|| FwMsgRun(,{|| TA042EQry() },,STR0004 ) },,VK_F5 ) //'Pesquisar'###"Pesquisando documentos..."

	oView:CreateHorizontalBox( "SUPERIOR", 50 )
	oView:CreateHorizontalBox( "INFERIOR", 50 )

	oView:SetOwnerView( "VIEW_TMPM", "SUPERIOR" )
	oView:SetOwnerView( "VIEW_TMPD", "INFERIOR" )

	oView:showInsertMsg( .F. )

Return oView

/*/{Protheus.doc} TA042EQry
Consulta os documentos de reserva com base nos parametros informados
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA042EQry()
	Local aArea         := GetArea()
	Local cAlias 		:= ""
	Local cFilterSA1	:= "% "
	Local cFilterSA2	:= "% "
	Local cFilterG3Q	:= "% "
	Local cFilterG3R	:= "% "
	Local lNewLine		:= .F.
	Local nX			:= 0
	Local oModel		:= FwModelActive()
	Local oModelMaster	:= oModel:GetModel("TMP_MASTER")
	Local oModelDetail	:= oModel:GetModel("TMP_DETAIL")

	//Verifica se foi indicado o n�mero do RV ou a data inicial e final antes de executar a pesquisa
	If ( Empty( oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") ) .And. Empty( oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") ) ) .And. ( Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE") ) .And. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT") ) .And. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XDOC")) .And. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XLOGDS")) .And. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XDORDE")) .And. Empty(oModel:GetModel("TMP_MASTER"):GetValue("TMP_XAORDE")) )
 		Help(,,"Tura039aQuery",,STR0005,1,0) //'Informe o n�mero do Localizador/Bilhete/Voucher ou o n�mero do RV ou Data Inicial e Final para realizar a consulta de documentos de reserva.'
		Return
	EndIf

	oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.F.)
	oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.F.)

	oModelDetail:ClearData()

	//Para a concilia��o ser multi-filial, � necess�rio as tabelas G8C e SA1 estarem completamente compartilhadas
	If !Empty( xFilial('G8C') ) .Or. !Empty( xFilial('SA1') )
		cFilterG3R += " AND G3R_FILIAL = '" + xFilial('G3R') + "'" 
	EndIf

	//Filtros Where da tabela G3R
	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") )
		cFilterG3R += " AND G3R_NUMID BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") )
		cFilterG3R += " AND G3R_NUMID = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") )
		cFilterG3R += " AND G3R_NUMID = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XNURVA") + "'"
	EndIf

	If  !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") )
		cFilterG3R += " AND G3R_IDITEM BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") )
		cFilterG3R += " AND G3R_IDITEM = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") )
		cFilterG3R += " AND G3R_IDITEM = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XIDRVA") + "'"
	EndIf

	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") )
		cFilterG3R += " AND G3R_NUMSEQ BETWEEN '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") + "' AND '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") )
		cFilterG3R += " AND G3R_NUMSEQ = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVD") + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") )
		cFilterG3R += " AND G3R_NUMSEQ = '" + oModel:GetModel("TMP_MASTER"):GetValue("TMP_XSERVA") + "'"
	EndIf

	//Sempre segmento diferente de aereo
	cFilterG3R += " AND G3R_TPSEG <> '1'"

	If !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE") ) .And. !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT") )
		cFilterG3R += " AND G3R_EMISS BETWEEN '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "' AND '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "' "
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIDE") )
		cFilterG3R += " AND G3R_EMISS >= '" + DToS(oModelMaster:GetValue("TMP_XEMIDE")) + "'"
	ElseIf !Empty(  oModel:GetModel("TMP_MASTER"):GetValue("TMP_XEMIAT") )
		cFilterG3R += " AND G3R_EMISS >= '" + DToS(oModelMaster:GetValue("TMP_XEMIAT")) + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XDOC"))
		cFilterG3R += " AND G3R_DOC LIKE '%" + AllTrim(oModelMaster:GetValue("TMP_XDOC")) + "' "
	EndIf

	If !oModelMaster:GetValue("TMP_XCANC")
		cFilterG3R += " AND G3R_STATUS <> '4' "
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_XLOGDS"))
		cFilterG3R += " AND G3R_LOCGDS = '"+oModelMaster:GetValue("TMP_XLOGDS")+"' "
	EndIf

	//Filtros Where da tabela SA2
	If !Empty(oModelMaster:GetValue("TMP_XFORND"))
		cFilterSA2 +=  " AND A2_COD >= '" + oModelMaster:GetValue("TMP_XFORND") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XLOJAD"))
		cFilterSA2 += " AND A2_LOJA >= '" + oModelMaster:GetValue("TMP_XLOJAD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XFORNA"))
		cFilterSA2 += " AND A2_COD <= '" + oModelMaster:GetValue("TMP_XFORNA") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XLOJAA"))
		cFilterSA2 += " AND A2_LOJA <= '" + oModelMaster:GetValue("TMP_XLOJAA") + "'"
	EndIf

	//Filtros Where da tabala G3Q
	If oModelMaster:GetValue("TMP_XGRPPR") == "1" //Reembolso terrestre
		cFilterG3Q += " AND G3Q_OPERAC = '2'"
	Else
		cFilterG3Q += " AND G3Q_OPERAC <> '2'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XPOSTD"))
		cFilterG3Q += " AND G3Q_POSTO >= '" + oModelMaster:GetValue("TMP_XPOSTD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XPOSTA"))
		cFilterG3Q += " AND G3Q_POSTO <= '" + oModelMaster:GetValue("TMP_XPOSTA") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XFOP"))
		cFilterG3Q += " AND G3Q_FORMPG = '" + oModelMaster:GetValue("TMP_XFOP") + "'"
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_XDORDE"))
		cFilterG3Q += " AND G3Q_ORDER >= '" + oModelMaster:GetValue("TMP_XDORDE") + "'"
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_XAORDE"))
		cFilterG3Q += " AND G3Q_ORDER <= '" + oModelMaster:GetValue("TMP_XAORDE") + "'"
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_XDITOS"))
		cFilterG3Q += " AND G3Q_ITOS >= '" + oModelMaster:GetValue("TMP_XDITOS") + "'"
	EndIf
	
	If !Empty(oModelMaster:GetValue("TMP_XAITOS"))
		cFilterG3Q += " AND G3Q_ITOS <= '" + oModelMaster:GetValue("TMP_XAITOS") + "'"
	EndIf
	
	//Filtros Where da tabela SA1
	If !Empty(oModelMaster:GetValue("TMP_XCLIDE"))
		cFilterSA1 += " AND A1_COD >= '" + oModelMaster:GetValue("TMP_XCLIDE") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLLJD"))
		cFilterSA1 += " AND A1_LOJA >= '" + oModelMaster:GetValue("TMP_XCLLJD") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLIAT"))
		cFilterSA1 += " AND A1_COD <= '" + oModelMaster:GetValue("TMP_XCLIAT") + "'"
	EndIf

	If !Empty(oModelMaster:GetValue("TMP_XCLLJA"))
		cFilterSA1 += " AND A1_LOJA <= '" + oModelMaster:GetValue("TMP_XCLLJA") + "'"
	EndIf

	cAlias 		:= GetNextAlias()
	cFilterSA1	+= " %"
	cFilterSA2	+= " %"
	cFilterG3Q	+= " %"
	cFilterG3R	+= " %"

	BeginSql Alias cAlias
		Column G3R_EMISS as date

		SELECT DISTINCT
				G3R_FILIAL,
				G3R_NUMID,
				G3R_IDITEM,
				G3R_NUMSEQ,
				G3R_DOC,
				G3R_CONJUG,
				G3R_EMISS,
				G3R_TARIFA,
				G3R_TAXA,
				G3R_EXTRAS,
				G3R_VLCOMI,
				G3R_TAXADU,
				G3R_VLINCE,
				G3R_FORREP,
				G3R_LOJREP,
				G3R_NOMREP,
				G3R_TXFORN,
				G3R_TOTREC,
				G3R_TPSEG,
				G3R_FORNEC,
				G3R_LOJA,
				G3Q_POSTO,
				G3M_DESCR,
				G3Q_CLIENT,
				G3Q_LOJA,
				A1_NOME,
				G3Q_FORMPG,
				G3Q_DESCFP,
				G3Q_ORDER,
				G3Q_ITOS,
				G3R_LOCGDS,
				G3R_MSFIL,
				G3R_SEQPRC,
				G3R_SEQNSH,
				G3R_MOEDA
			FROM %Table:G3R% G3R
			INNER JOIN %Table:G3Q% G3Q
				ON G3Q.G3Q_NUMID = G3R.G3R_NUMID
					AND G3Q.G3Q_IDITEM = G3R.G3R_IDITEM
					AND G3Q.G3Q_NUMSEQ = G3R.G3R_NUMSEQ
					AND G3Q_CONINU = ' '
					AND G3Q_FILIAL = G3R_FILIAL
					AND G3Q.%notDel%
					%Exp:cFilterG3Q%
			INNER JOIN %Table:G3M% G3M
				ON G3M.G3M_CODIGO = G3Q.G3Q_POSTO
					AND G3M_FILIAL = G3Q.G3Q_FILPST
					AND G3M.%notDel%
			INNER JOIN %Table:SA1% SA1
				ON SA1.A1_COD = G3Q.G3Q_CLIENT
					AND SA1.A1_LOJA = G3Q.G3Q_LOJA
					AND SA1.A1_FILIAL = %xFilial:SA1%
					AND SA1.%notDel%
					%Exp:cFilterSA1%
			INNER JOIN %Table:SA2% SA2
				ON ( SA2.A2_COD = G3R.G3R_FORNEC OR SA2.A2_COD = G3R.G3R_FORREP )
					AND ( SA2.A2_LOJA = G3R.G3R_LOJA OR SA2.A2_LOJA = G3R.G3R_LOJREP )
					AND SA2.A2_FILIAL = %xFilial:SA2%
					AND SA2.%notDel%
					%Exp:cFilterSA2%
			WHERE G3R_CONCIL = ' '
				AND G3R_CONINU = ' '
				AND G3R.%notDel%
				%Exp:cFilterG3R%
	EndSql

	While (cAlias)->( !EOF() )
		If lNewLine
			oModelDetail:AddLine()
		EndIf

		For nX := 1 to (cAlias)->( FCount() )
			oModelDetail:SetValue( (cAlias)->( FieldName(nX) ), (cAlias)->( FieldGet(nX) )  )
		Next

		lNewLine := .T.
		(cAlias)->( dbSkip() )
	EndDo

	oModelDetail:GoLine(1)
	(cAlias)->(dbCloseArea())

	oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

	RestArea(aArea)
	
	TURXNIL(aArea)
Return

/*/{Protheus.doc} TA042ECommit
Fun��o de pseudo-commit, n�o realiza nenhum tratamento
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param oModel, objeto, Objeto do ModelDef instanciado
/*/
Function TA042ECommit(oModel)
Return .T.

/*/{Protheus.doc} TA042EDestroy
Fun��o para desalocar objetos instanciados
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Function TA042EDestroy()

	//� necess�rio verificar se a chamada veio do bot�o 'Salvar' da tela apresentada no bot�o cancelar
	//fechando as tabelas tempor�rias somente na �ltima chamada
	If !FwIsInCallStack( 'FwAlertExitPage' )
		If ValType( oTblHeader ) == "O"		//Verifica se o objeto FwTemporaryTable est� instanciado
			oTblHeader:Delete()
			FreeObj( oTblHeader )
		EndIf
	
		If ValType( oTblDetail ) == "O"		//Verifica se o objeto FwTemporaryTable est� instanciado
			oTblDetail:Delete()
			FreeObj( oTblDetail )
		EndIf
	EndIf

Return .T.

/*/{Protheus.doc} TA042ECrTb
Cria��o das tabelas tempor�rias
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA042ECrTb()
	Local aArea         := GetArea()
	Local aAreaSX3      := SX3->(GetArea())
	Local aStruHeader	:= {}
	Local aStruDetail	:= {}
	Local nX			:= 0
	Local aDRFields	    := {"G3R_FILIAL","G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC", "G3R_FORREP", "G3R_LOJREP",;
							"G3R_NOMREP", "G3R_CONJUG", "G3R_EMISS", "G3R_TARIFA", "G3R_TAXA", "G3R_EXTRAS", "G3R_VLCOMI",;
							"G3R_TAXADU", "G3R_VLINCE", "G3R_TXFORN", "G3R_TOTREC", "G3R_TPSEG","G3R_FORNEC","G3R_LOJA",;
							"A2_NOME","G3Q_POSTO","G3M_DESCR","G3Q_CLIENT","G3Q_LOJA","A1_NOME","G3Q_FORMPG","G3Q_DESCFP",;
							"G3Q_ORDER","G3Q_ITOS","G3R_LOCGDS","G3R_MSFIL","G3R_SEQPRC","G3R_SEQNSH","G3R_MOEDA"}

	AAdd(aStruHeader,{"TMP_XGRPPR", "C", 10						, 0})
	AAdd(aStruHeader,{"TMP_XNURVD", "C", TamSX3("G3R_NUMID")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XIDRVD", "C", TamSX3("G3R_IDITEM")[1], 0})
	AAdd(aStruHeader,{"TMP_XSERVD", "C", TamSX3("G3R_NUMSEQ")[1], 0})
	AAdd(aStruHeader,{"TMP_XNURVA", "C", TamSX3("G3R_NUMID")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XIDRVA", "C", TamSX3("G3R_IDITEM")[1], 0})
	AAdd(aStruHeader,{"TMP_XSERVA", "C", TamSX3("G3R_NUMSEQ")[1], 0})
	AAdd(aStruHeader,{"TMP_XEMIDE", "D", 08						, 0})
	AAdd(aStruHeader,{"TMP_XEMIAT", "D", 08						, 0})
	AAdd(aStruHeader,{"TMP_XFORND", "C", TamSX3("A2_COD")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XLOJAD", "C", TamSX3("A2_LOJA")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XNOMFO", "C", TamSX3("A2_NOME")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XFORNA", "C", TamSX3("A2_COD")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XLOJAA", "C", TamSX3("A2_LOJA")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XNOMFA", "C", TamSX3("A2_NOME")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XPOSTD", "C", TamSX3("G3P_POSTO")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XNOMPD", "C", TamSX3("G3P_DESPST")[1], 0})
	AAdd(aStruHeader,{"TMP_XPOSTA", "C", TamSX3("G3P_POSTO")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XNOMPA", "C", TamSX3("G3P_DESPST")[1], 0})
	AAdd(aStruHeader,{"TMP_XCLIDE", "C", TamSX3("A1_COD")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCLLJD", "C", TamSX3("A1_LOJA")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCLNDE", "C", TamSX3("A1_NOME")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCLIAT", "C", TamSX3("A1_COD")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCLLJA", "C", TamSX3("A1_LOJA")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCLINA", "C", TamSX3("A1_NOME")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XCANC" , "L", 1						, 0})
	AAdd(aStruHeader,{"TMP_XDOC"  , "C", TamSX3("G3Q_DOC")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XFOP"  , "C", TamSX3("G3Q_FORMPG")[1], 0})
	AAdd(aStruHeader,{"TMP_XDFOP" , "C", TamSX3("G3Q_DESCFP")[1], 0})
	AAdd(aStruHeader,{"TMP_XDORDE", "C", TamSX3("G3Q_ORDER")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XDITOS", "C", TamSX3("G3Q_ITOS")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XAORDE", "C", TamSX3("G3Q_ORDER")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XAITOS", "C", TamSX3("G3Q_ITOS")[1]	, 0})
	AAdd(aStruHeader,{"TMP_XLOGDS", "C", TamSX3("G3R_LOCGDS")[1], 0})
	
	oTblHeader := FWTemporaryTable():New()

	oTblHeader:SetFields( aStruHeader )
	oTblHeader:AddIndex( "index1", {"TMP_XGRPPR"} )
	oTblHeader:Create()

	AAdd(aStruDetail,{"TMP_OK",	"L", 1, 0})

	SX3->( dbSetOrder(2) )
	For nX := 1 to len(aDRFields)
		If SX3->( dbSeek( aDRFields[nX] ) )
			AAdd(aStruDetail,{AllTrim(SX3->X3_CAMPO),SX3->X3_TIPO,SX3->X3_TAMANHO,SX3->X3_DECIMAL})
		EndIf
	Next

	oTblDetail := FWTemporaryTable():New()

	oTblDetail:SetFields( aStruDetail )
	oTblDetail:AddIndex( "index1", {"G3R_NUMID", "G3R_IDITEM", "G3R_NUMSEQ", "G3R_DOC"} )
	oTblDetail:Create()

	RestArea(aAreaSX3)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSX3)
	TURXNIL(aStruHeader)
	TURXNIL(aStruDetail)
	TURXNIL(aDRFields)
Return

/*/{Protheus.doc} AddFldCab
Cria��o dos campos referente ao cabe�alho
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function AddFldCab( oStruct, nOpc )
	If nOpc == 1
		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0006	,; // [01] C Titulo do campo //'Tipo concilia��o'
							STR0006 	,; // [02] C ToolTip do campo //'Tipo concilia��o'
							"TMP_XGRPPR" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							1 					,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							NIL 				,; // [08] B Code-block de valida��o When do campo
							{STR0007,STR0008} , ; // [09] A Lista de valores permitido do campo //'1=Aereo'###'2=Terrestre'
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							FwBuildFeature( STRUCT_FEATURE_INIPAD, IIf(FWIsInCallStack("TURA042A"),"2","1") ) , ; // [11] B Code-block de inicializacao do campo
							NIL 				,; // [12] L Indica se trata de um campo chave
							NIL 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0009		,; // [01] C Titulo do campo //'Num RV De'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XNURVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMID")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0010			,; // [01] C Titulo do campo //'ID RV De'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XIDRVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_IDITEM")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0011		,; // [01] C Titulo do campo //'Seq RV De'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XSERVD" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMSEQ")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0012		,; // [01] C Titulo do campo //'Num RV At�'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XNURVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMID")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0013		,; // [01] C Titulo do campo //'ID RV At�'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XIDRVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_IDITEM")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0014		,; // [01] C Titulo do campo //'Seq RV At�'
							"" 					,; // [02] C ToolTip do campo
							"TMP_XSERVA" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_NUMSEQ")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual


		oStruct:AddField(	STR0015,; //'Emiss�o De'
							"",;
							"TMP_XEMIDE",;
							"D",;
							08,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0016,; //'Emiss�o At�'
							"",;
							"TMP_XEMIAT",;
							"D",;
							08,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0017,; //'Fornecedor De'
							"",;
							"TMP_XFORND",;
							"C",;
							TamSX3("A2_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0018,; //'Loja De'
							"",;
							"TMP_XLOJAD",;
							"C",;
							TamSX3("A2_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0019,; //'Nome Fornecedor De'
							"",;
							"TMP_XNOMFO",;
							"C",;
							TamSX3("A2_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T. },;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0020,; //'Fornecedor At�'
							"",;
							"TMP_XFORNA",;
							"C",;
							TamSX3("A2_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0021,; //'Loja At�'
							"",;
							"TMP_XLOJAA",;
							"C",;
							TamSX3("A2_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0022,; //'Nome Fornecedor At�'
							"",;
							"TMP_XNOMFA",;
							"C",;
							TamSX3("A2_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T. },;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0023,; //'Posto De'
							"",;
							"TMP_XPOSTD",;
							"C",;
							TamSX3("G3P_POSTO")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )


		oStruct:AddField(	STR0024,; //'Nome Posto At�'
							"",;
							"TMP_XNOMPD",;
							"C",;
							TamSX3("G3P_DESPST")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0025,; //'Posto At�'
							"",;
							"TMP_XPOSTA",;
							"C",;
							TamSX3("G3P_POSTO")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0024,; //'Nome Posto At�'
							"",;
							"TMP_XNOMPA",;
							"C",;
							TamSX3("G3P_DESPST")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0026,; //'Cliente De'
							"",;
							"TMP_XCLIDE",;
							"C",;
							TamSX3("A1_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0018,; //'Loja De'
							"",;
							"TMP_XCLLJD",;
							"C",;
							TamSX3("A1_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0027,; //'Nome Cliente De'
							"",;
							"TMP_XCLNDE",;
							"C",;
							TamSX3("A1_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0028,; //'Cliente At�'
							"",;
							"TMP_XCLIAT",;
							"C",;
							TamSX3("A1_COD")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0021,; //'Loja At�'
							"",;
							"TMP_XCLLJA",;
							"C",;
							TamSX3("A1_LOJA")[1],;
							0,;
							{|| .T.},;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0029,; //'Nome Cliente At�'
							"",;
							"TMP_XCLINA",;
							"C",;
							TamSX3("A1_NOME")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0030,; //'Listar Cancelados'
							"",;
							"TMP_XCANC",;
							"L",;
							1,;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	STR0031,; //'Localizador/Bilhete/Voucher' 
							"",;
							"TMP_XDOC"	,;
							"C",;
							TamSX3("G3Q_DOC")[1],;
							0,;
							{|| .T. },;
							{|| .T.},;
							Nil,;
							.F.,;
							Nil,;
							.F.,;
							.F.,;
							.T. )

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0045				,; // [01] C Titulo do campo //"Forma Pagto."
							"" 					,; // [02] C ToolTip do campo
							"TMP_XFOP" 			,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_FORMPG")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual
							
		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0046				,; // [01] C Titulo do campo //"Desc. Forma Pagto."
							"" 					,; // [02] C ToolTip do campo
							"TMP_XDFOP" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_DESCFP")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual
		
		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0047				,; // [01] C Titulo do campo //"De O.S."
							"" 					,; // [02] C ToolTip do campo
							"TMP_XDORDE" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_ORDER")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0048		,; // [01] C Titulo do campo //"De Item OS"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XDITOS" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_ITOS")[1] ,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0049				,; // [01] C Titulo do campo //"At� O.S."
							"" 					,; // [02] C ToolTip do campo
							"TMP_XAORDE" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_ORDER")[1] 		,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual

		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0050		,; // [01] C Titulo do campo //"At� Item OS"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XAITOS" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3Q_ITOS")[1] ,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual
		
		oStruct:AddField(	; 						// Ord. Tipo Desc.
							STR0056		,; // [01] C Titulo do campo //"Localizador GDS"
							"" 					,; // [02] C ToolTip do campo
							"TMP_XLOGDS" 		,; // [03] C identificador (ID) do Field
							"C"					,; // [04] C Tipo do campo
							TamSX3("G3R_LOCGDS")[1] ,; // [05] N Tamanho do campo
							0 					,; // [06] N Decimal do campo
							{|| .T.}			,; // [07] B Code-block de valida��o do campo
							{|| .T.}			,; // [08] B Code-block de valida��o When do campo
							Nil 				,; // [09] A Lista de valores permitido do campo
							.F. 				,; // [10] L Indica se o campo tem preenchimento obrigat�rio
							Nil					,; // [11] B Code-block de inicializacao do campo
							.F. 				,; // [12] L Indica se trata de um campo chave
							.F. 				,; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. ) 				   // [14] L Indica se o campo � virtual
		
	Else

		oStruct:AddField( ; // Ord. Tipo Desc.
							"TMP_XGRPPR" 		,; // [01] C Nome do Campo
							"01"				,; // [02] C Ordem
							STR0006 	,; // [03] C Titulo do campo //'Tipo concilia��o'
							STR0006 	,; // [04] C Descri��o do campo //'Tipo concilia��o'
							{} 					,; // [05] A Array com Help
							"C"			 		,; // [06] C Tipo do campo
							"@!" 				,; // [07] C Picture
							NIL 				,; // [08] B Bloco de Picture Var
							"" 					,; // [09] C Consulta F3
							.F. 				,; // [10] L Indica se o campo � edit�vel
							NIL 				,; // [11] C Pasta do campo
							NIL 				,; // [12] C Agrupamento do campo
							{STR0007,STR0008} ,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 				,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 				,; // [15] C Inicializador de Browse
							.T. 				,; // [16] L Indica se o campo � virtual
							NIL ) 				// [17] C Picture Vari�vel


		
		oStruct:AddField(	'TMP_XDOC',;
							'02',;
							STR0031,; //'Localizador/Bilhete/Voucher'
							STR0031,; //'Localizador/Bilhete/Voucher'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCANC',;
							'03',;
							STR0030,; //'Listar Cancelados'
							STR0030,; //'Listar Cancelados'
							{},;
							'L',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)




		oStruct:AddField(	"TMP_XNURVD",;
							'04',;
							STR0032,; //'N�mero RV De'
							STR0032,; //'N�mero RV De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XIDRVD",;
							'05',;
							STR0010,; //'ID RV De'
							STR0010,; //'ID RV De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XSERVD",;
							'06',;
							STR0033,; //'Seq. RV De'
							STR0033,; //'Seq. RV De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil )

		oStruct:AddField(	"TMP_XNURVA",;
							'07',;
							STR0034,; //'N�mero RV At�'
							STR0034,; //'N�mero RV At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XIDRVA",;
							'08',;
							STR0013,; //'ID RV At�'
							STR0013,; //'ID RV At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	"TMP_XSERVA",;
							'09',;
							STR0035,; //'Seq. RV At�'
							STR0035,; //'Seq. RV At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil )


		oStruct:AddField(	STR0036,; //'TMP_XEMIDE'
							'10',;
							STR0015,; //'Emiss�o De'
							STR0015,; //'Emiss�o De'
							{},;
							'D',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XEMIAT',;
							'11',;
							STR0016,; //'Emiss�o At�'
							STR0016,; //'Emiss�o At�'
							{},;
							'D',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)
														

		oStruct:AddField(	'TMP_XFORND',;
							'12',;
							STR0017,; //'Fornecedor De'
							STR0017,; //'Fornecedor De'
							{},;
							'C',;
							'',;
							Nil,;
							'SA2',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XLOJAD',;
							'13',;
							STR0018,; //'Loja De'
							STR0018,; //'Loja De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMFO',;
							'14',;
							STR0019,; //'Nome Fornecedor De'
							STR0019,; //'Nome Fornecedor De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XFORNA',;
							'15',;
							STR0020,; //'Fornecedor At�'
							STR0020,; //'Fornecedor At�'
							{},;
							'C',;
							'',;
							Nil,;
							'SA2',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XLOJAA',;
							'16',;
							STR0021,; //'Loja At�'
							STR0021,; //'Loja At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMFA',;
							'17',;
							STR0022,; //'Nome Fornecedor At�'
							STR0022,; //'Nome Fornecedor At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XPOSTD',;
							'18',;
							STR0023,; //'Posto De'
							STR0023,; //'Posto De'
							{},;
							'C',;
							'',;
							Nil,;
							'G3M',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMPD',;
							'19',;
							STR0037,; //'Nome Posto De'
							STR0037,; //'Nome Posto De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XPOSTA',;
							'20',;
							STR0025,; //'Posto At�'
							STR0025,; //'Posto At�'
							{},;
							'C',;
							'',;
							Nil,;
							'G3M',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XNOMPA',;
							'21',;
							STR0024,; //'Nome Posto At�'
							STR0024,; //'Nome Posto At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLIDE',;
							'22',;
							STR0026,; //'Cliente De'
							STR0026,; //'Cliente De'
							{},;
							'C',;
							'',;
							Nil,;
							'SA1',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLLJD',;
							'23',;
							STR0018,; //'Loja De'
							STR0018,; //'Loja De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLNDE',;
							'24',;
							STR0027,; //'Nome Cliente De'
							STR0027,; //'Nome Cliente De'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLIAT',;
							'25',;
							STR0028,; //'Cliente At�'
							STR0028,; //'Cliente At�'
							{},;
							'C',;
							'',;
							Nil,;
							'SA1',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLLJA',;
							'26',;
							STR0021,; //'Loja At�'
							STR0021,; //'Loja At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.T.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)

		oStruct:AddField(	'TMP_XCLINA',;
							'27',;
							STR0029,; //'Nome Cliente At�'
							STR0029,; //'Nome Cliente At�'
							{},;
							'C',;
							'',;
							Nil,;
							'',;
							.F.,;
							Nil,;
							Nil,;
							{},;
							Nil,;
							Nil,;
							.T.,;
							Nil)
							
		
		oStruct:AddField( 	"TMP_XFOP" 		,; // [01] C Nome do Campo
							"28"			,; // [02] C Ordem
							STR0045 		,; // [03] C Titulo do campo //"Forma Pagto."
							STR0045 	  	,; // [04] C Descri��o do campo //"Forma Pagto."
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							"G3N"			,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel
							
		oStruct:AddField( 	"TMP_XDFOP" 	,; // [01] C Nome do Campo
							"29"			,; // [02] C Ordem
							STR0046 		,; // [03] C Titulo do campo //"Desc. Forma Pagto."
							STR0046 		,; // [04] C Descri��o do campo //"Desc. Forma Pagto."
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.F. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

		oStruct:AddField( "TMP_XDORDE" 	,; // [01] C Nome do Campo
							"30"			,; // [02] C Ordem
							STR0047		,; // [03] C Titulo do campo //"De O.S."
							STR0047		,; // [04] C Descri��o do campo //"De O.S."
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

		oStruct:AddField( "TMP_XDITOS" 	,; // [01] C Nome do Campo
							"31"			,; // [02] C Ordem
							STR0048	,; // [03] C Titulo do campo //"De Item OS"
							STR0048	,; // [04] C Descri��o do campo //"De Item OS"
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

		oStruct:AddField( "TMP_XAORDE" 	,; // [01] C Nome do Campo
							"32"			,; // [02] C Ordem
							STR0049		,; // [03] C Titulo do campo //"At� O.S."
							STR0049		,; // [04] C Descri��o do campo //"At� O.S."
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

		oStruct:AddField( "TMP_XAITOS" 	,; // [01] C Nome do Campo
							"33"			,; // [02] C Ordem
							STR0050	,; // [03] C Titulo do campo //"At� Item OS"
							STR0050	,; // [04] C Descri��o do campo //"At� Item OS"
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] 'C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

		oStruct:AddField( "TMP_XLOGDS" 	,; // [01] C Nome do Campo
							"34"			,; // [02] C Ordem
							STR0056	,; // [03] C Titulo do campo //"Localizador GDS"
							STR0056	,; // [04] C Descri��o do campo //"Localizador GDS"
							{} 				,; // [05] A Array com Help
							"C"			 	,; // [06] C Tipo do campo
							"@!" 			,; // [07] 'C Picture
							NIL 			,; // [08] B Bloco de Picture Var
							""				,; // [09] C Consulta F3
							.T. 			,; // [10] L Indica se o campo � edit�vel
							NIL 			,; // [11] C Pasta do campo
							NIL 			,; // [12] C Agrupamento do campo
							{} 				,; // [13] A Lista de valores permitido do campo (Combo) //'1=Aereo'###'2=Terrestre'
							NIL 			,; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			,; // [15] C Inicializador de Browse
							.T. 			,; // [16] L Indica se o campo � virtual
							NIL 			)  // [17] C Picture Vari�vel

							
	EndIf

Return


/*/{Protheus.doc} AddFldDet
Adiciona os campos referenes a detalhes no cabe�alho no model/view
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function AddFldDet( oStructDet, nOpc )
	Local aStruHis	:= ( oTblDetail:GetAlias() )->( dbStruct() )
	Local nX		:= 0

	SX3->( dbSetOrder(2) )

	//Adiciona o campo OK
	If nOpc == 1
		oStructDet:AddField( ; // Ord. Tipo Desc.
								""			, ; // [01] C Titulo do campo
								"" 			, ; // [02] C ToolTip do campo
								"TMP_OK"	, ; // [03] C identificador (ID) do Field
								"L"			, ; // [04] C Tipo do campo
								1			, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 		) // [14] L Indica se o campo � virtual

		oStructDet:AddField( ; // Ord. Tipo Desc.
								STR0051			, ; // [01] C Titulo do campo //"N Fantasia Rep"
								"" 			, ; // [02] C ToolTip do campo
								"TMP_XNFREP"	, ; // [03] C identificador (ID) do Field
								"C"			, ; // [04] C Tipo do campo
								TamSX3("A2_NREDUZ")[1]	, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.T. 		) // [14] L Indica se o campo � virtual
		
		oStructDet:AddField( ; // Ord. Tipo Desc.
								STR0053			, ; // [01] C Titulo do campo//"N Fantasia Prd"
								""	 			, ; // [02] C ToolTip do campo
								"TMP_XNFORN"	, ; // [03] C identificador (ID) do Field
								"C"			, ; // [04] C Tipo do campo
								TamSX3("A2_NREDUZ")[1]	, ; // [05] N Tamanho do campo
								0			, ; // [06] N Decimal do campo
								NIL			, ; // [07] B Code-block de valida��o do campo
								NIL 		, ; // [08] B Code-block de valida��o When do campoz
								NIL 		, ; // [09] A Lista de valores permitido do campo
								NIL 		, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL			, ; // [11] B Code-block de inicializacao do campo
								NIL 		, ; // [12] L Indica se trata de um campo chave
								.T. 		, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.T. 		) // [14] L Indica se o campo � virtual
		

	Else
		oStructDet:AddField( ; // Ord. Tipo Desc.
								"TMP_OK" 	, ; // [01] C Nome do Campo
								"00"		, ; // [02] C Ordem
								""			, ; // [03] C Titulo do campo
								""	, 		; // [04] C Descri��o do campo
								NIL			, ; // [05] A Array com Help
								"L" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.T. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.F. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel

		oStructDet:AddField( ; // Ord. Tipo Desc.
								"TMP_XNFREP" 	, ; // [01] C Nome do Campo
								"09"		, ; // [02] C Ordem
								STR0051	, ; // [03] C Titulo do campo//"N Fantasia Rep"
								STR0052	, ; // [04] C Descri��o do campo //"Nome Fantasia Reporte"
								NIL			, ; // [05] A Array com Help
								"C" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.T. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.T. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel
	
		oStructDet:AddField( ; // Ord. Tipo Desc.
								"TMP_XNFORN" 	, ; // [01] C Nome do Campo
								"23"		, ; // [02] C Ordem
								STR0053	, ; // [03] C Titulo do campo//"N Fantasia Prd"
								STR0054	, ; // [04] C Descri��o do campo//"Nome Fantasia Produto"
								NIL			, ; // [05] A Array com Help
								"C" 		, ; // [06] C Tipo do campo
								NIL			, ; // [07] C Picture
								NIL 		, ; // [08] B Bloco de Picture Var
								"" 			, ; // [09] C Consulta F3
								.T. 		, ; // [10] L Indica se o campo � edit�vel
								NIL 		, ; // [11] C Pasta do campo
								NIL 		, ; // [12] C Agrupamento do campo
								NIL 		, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 		, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 		, ; // [15] C Inicializador de Browse
								.T. 		, ; // [16] L Indica se o campo � virtual
								NIL 		) 	// [17] C Picture Vari�vel
	
	EndIf

	//Adiciona os demais campos da tabela
	For nX := 1 to len( aStruHis )
		If SX3->( dbSeek( aStruHis[nX][1] ) )

			If nOpc == 1
				oStructDet:AddField( ; // Ord. Tipo Desc.
										X3Titulo() , ; // [01] C Titulo do campo
										"" 				, ; // [02] C ToolTip do campo
										AllTrim(SX3->X3_CAMPO) , ; // [03] C identificador (ID) do Field
										SX3->X3_TIPO 	, ; // [04] C Tipo do campo
										SX3->X3_TAMANHO 	, ; // [05] N Tamanho do campo
										SX3->X3_DECIMAL 	, ; // [06] N Decimal do campo
										NIL				, ; // [07] B Code-block de valida��o do campo
										NIL 			, ; // [08] B Code-block de valida��o When do campoz
										NIL 			, ; // [09] A Lista de valores permitido do campo
										NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
										NIL				, ; // [11] B Code-block de inicializacao do campo
										NIL 			, ; // [12] L Indica se trata de um campo chave
										.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
										.F. 			) // [14] L Indica se o campo � virtual


			ElseIf nOpc == 2
			If !Empty(X3CBOX())
					aCBox := Separa(X3CBOX(),";")
				Else
					aCBox := nil
				Endif
				
				oStructDet:AddField( ; // Ord. Tipo Desc.
										AllTrim(SX3->X3_CAMPO) 	, ; // [01] C Nome do Campo
										StrZero(nX, 2)			, ; // [02] C Ordem
										X3Titulo()	, ; // [03] C Titulo do campo
										X3Titulo()	, ; // [04] C Descri��o do campo
										NIL			, ; // [05] A Array com Help
										SX3->X3_TIPO 		, ; // [06] C Tipo do campo
										SX3->X3_PICTURE	, ; // [07] C Picture
										NIL 			, ; // [08] B Bloco de Picture Var
										"" 				, ; // [09] C Consulta F3
										.F. 			, ; // [10] L Indica se o campo � edit�vel
										NIL 			, ; // [11] C Pasta do campo
										NIL 			, ; // [12] C Agrupamento do campo
										aCBox			, ; // [13] A Lista de valores permitido do campo (Combo)
										NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
										NIL 			, ; // [15] C Inicializador de Browse
										.F. 			, ; // [16] L Indica se o campo � virtual
										NIL 			) 	// [17] C Picture Vari�vel


			EndIf
		EndIf
	Next

	TURXNIL(aStruHis)
Return

/*/{Protheus.doc} AddGroups
Cria��o do agrupamento de campos
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function AddGroups( oStruct )

	oStruct:AddGroup( "GRUPO01", STR0038, "", 1 ) //"Grupo"
	oStruct:AddGroup( "GRUPO07", STR0044, "", 1 ) //"Bilhete/Cancelados"
	oStruct:AddGroup( "GRUPO02", STR0039, "", 1 ) //"Registro de Venda"
	oStruct:AddGroup( "GRUPO08", STR0045, "", 1 ) //"Forma Pgto."
	oStruct:AddGroup( "GRUPO03", STR0040, "", 1 ) //"Emiss�o"
	oStruct:AddGroup( "GRUPO04", STR0041, "", 1 ) //"Fornecedor"
	oStruct:AddGroup( "GRUPO05", STR0042, "", 1 ) //"Posto"
	oStruct:AddGroup( "GRUPO06", STR0043, "", 1 ) //"Cliente"
	oStruct:AddGroup( "GRUPO09", STR0055, "", 1 ) //"Ordem de Servi�o"
	
	oStruct:SetProperty( "TMP_XGRPPR" , MVC_VIEW_GROUP_NUMBER, "GRUPO01" )

	oStruct:SetProperty( "TMP_XNURVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XIDRVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XSERVD" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XNURVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XIDRVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )
	oStruct:SetProperty( "TMP_XSERVA" , MVC_VIEW_GROUP_NUMBER, "GRUPO02" )

	oStruct:SetProperty( "TMP_XFOP"   , MVC_VIEW_GROUP_NUMBER, "GRUPO08" )
	oStruct:SetProperty( "TMP_XDFOP"  , MVC_VIEW_GROUP_NUMBER, "GRUPO08" )

	oStruct:SetProperty( "TMP_XEMIDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO03" )
	oStruct:SetProperty( "TMP_XEMIAT" , MVC_VIEW_GROUP_NUMBER, "GRUPO03" )
	oStruct:SetProperty( "TMP_XFORND" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XLOJAD" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XNOMFO" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XFORNA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XLOJAA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XNOMFA" , MVC_VIEW_GROUP_NUMBER, "GRUPO04" )
	oStruct:SetProperty( "TMP_XPOSTD" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XNOMPD" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XPOSTA" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XNOMPA" , MVC_VIEW_GROUP_NUMBER, "GRUPO05" )
	oStruct:SetProperty( "TMP_XCLIDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLLJD" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLNDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLIAT" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLLJA" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCLINA" , MVC_VIEW_GROUP_NUMBER, "GRUPO06" )
	oStruct:SetProperty( "TMP_XCANC"  , MVC_VIEW_GROUP_NUMBER, "GRUPO07" )
	oStruct:SetProperty( "TMP_XDOC"   , MVC_VIEW_GROUP_NUMBER, "GRUPO07" )
	oStruct:SetProperty( "TMP_XLOGDS" , MVC_VIEW_GROUP_NUMBER, "GRUPO07" )
	
	oStruct:SetProperty( "TMP_XDORDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO09" )
	oStruct:SetProperty( "TMP_XDITOS" , MVC_VIEW_GROUP_NUMBER, "GRUPO09" )
	oStruct:SetProperty( "TMP_XAORDE" , MVC_VIEW_GROUP_NUMBER, "GRUPO09" )
	oStruct:SetProperty( "TMP_XAITOS" , MVC_VIEW_GROUP_NUMBER, "GRUPO09" )
	
	oStruct:SetProperty( "TMP_XSERVD" , MVC_VIEW_INSERTLINE, .T. )
	oStruct:SetProperty( "TMP_XNOMFO" , MVC_VIEW_INSERTLINE, .T. )
	oStruct:SetProperty( "TMP_XNOMPD" , MVC_VIEW_INSERTLINE, .T. )
	oStruct:SetProperty( "TMP_XCLNDE" , MVC_VIEW_INSERTLINE, .T. )
Return

/*/{Protheus.doc} AddGatilhos
Cria��o dos gatilhos
@type function
@author anderson
@since 24/03/2016
@version 1.0
/*/
Static Function AddGatilhos( oStruct, oStructDet )
	Local aAux := {}

	aAux := FwStruTrigger( 'TMP_XFORND', 'TMP_XLOJAD', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORND', 'TMP_XLOJAD', 'SA2->A2_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XLOJAD', 'TMP_XNOMFO', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_XFORND")+FwFldGet("TMP_XLOJAD"),"SA2->A2_NOME","")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORNA', 'TMP_XLOJAA', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XFORNA', 'TMP_XLOJAA', 'SA2->A2_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XLOJAA', 'TMP_XNOMFA', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("TMP_XFORNA")+FwFldGet("TMP_XLOJAA"),"SA2->A2_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XPOSTD', 'TMP_XNOMPD', 'Posicione("G3M",1,xFilial("G3M")+FwFldGet("TMP_XPOSTD"),"G3M->G3M_DESCR")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XPOSTA', 'TMP_XNOMPA', 'Posicione("G3M",1,xFilial("G3M")+FwFldGet("TMP_XPOSTA"),"G3M->G3M_DESCR")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIDE', 'TMP_XCLLJD', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIDE', 'TMP_XCLLJD', 'SA1->A1_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLLJD', 'TMP_XCLNDE', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_XCLIDE")+FwFldGet("TMP_XCLLJD"),"SA1->A1_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIAT', 'TMP_XCLLJA', '"  "', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLIAT', 'TMP_XCLLJA', 'SA1->A1_LOJA', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'TMP_XCLLJA', 'TMP_XCLINA', 'Posicione("SA1",1,xFilial("SA1")+FwFldGet("TMP_XCLIAT")+FwFldGet("TMP_XCLLJA"),"SA1->A1_NOME")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])

	aAux := FwStruTrigger( 'G3R_LOJA', 'A2_NOME', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("G3R_FORNEC")+FwFldGet("G3R_LOJA"),"SA2->A2_NOME","")', .F. )
	oStructDet:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger( 'G3R_LOJREP', 'TMP_XNFREP', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("G3R_FORREP")+FwFldGet("G3R_LOJREP"),"SA2->A2_NREDUZ","")', .F. )
	oStructDet:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger( 'G3R_LOJA', 'TMP_XNFORN', 'Posicione("SA2",1,xFilial("SA2")+FwFldGet("G3R_FORNEC")+FwFldGet("G3R_LOJA"),"SA2->A2_NREDUZ","")', .F. )
	oStructDet:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger( 'TMP_XFOP', 'TMP_XDFOP', 'Posicione("G3N",1,xFilial("G3N")+FwFldGet("TMP_XFOP"),"G3N->G3N_DESCR","")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])
	
	aAux := FwStruTrigger( 'TMP_XDORDE', 'TMP_XAORDE', 'FwFldGet("TMP_XDORDE")', .F. )
	oStruct:AddTrigger( aAux[1], aAux[2], aAux[3], aAux[4])
	
	TURXNIL(aAux)
Return