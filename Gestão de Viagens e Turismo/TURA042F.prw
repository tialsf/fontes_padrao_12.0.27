#INCLUDE "TURA042F.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

Static cForConc := ""
Static cFLjConc := ""

/*/{Protheus.doc} ModelDef
Modelo de dados referete a pesquisa de Documentos de reserva para concilia��o - outros (terrestre)
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function ModelDef()
Return oModel := FWLoadModel( "TURA042E" )

/*/{Protheus.doc} ViewDef
Vis�o de dados referete a pesquisa de Documentos de reserva para concilia��o terrestre
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ViewDef()
	Local oView  := FWLoadView( "TURA042E" )
	Local oModel := oView:GetModel()

	oModel:GetModel( "TMP_DETAIL" ):GetStruct():SetProperty("TMP_OK", MODEL_FIELD_VALID, {|oModel, cField, xValue, nLine, xOldValue| TA042FVAL(oModel, cField, xValue, nLine, xOldValue)})
	oModel:GetModel( "TMP_DETAIL" ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042FLPre(oModelGrid, nLine, cAction, cField) } )

	oModel:bCommit := {|oModel| TA042FCommit(oModel), TA042EDestroy() }

Return oView

/*/{Protheus.doc} TA042FLPre
Pr�-valida��o para verificar se o registro � apto para importa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA042FLPre(oModelGrid, nLine, cAction, cField)
	Local oModelConc := Nil
	Local lRet 	     := .T.

	Default cForConc := ""
	Default cFLjConc := ""

	If cField == "TMP_OK"
		If cAction == "CANSETVALUE"
			oModelConc := TA042Model()
			
			If !Empty(oModelConc:GetValue("G8C_MASTER","G8C_FORNEC")) .And. !Empty(oModelConc:GetValue("G8C_MASTER","G8C_LOJA"))
				If oModelConc:GetValue("G8C_MASTER","G8C_FORNEC") <> oModelGrid:GetValue( "G3R_FORREP" ) .OR. oModelConc:GetValue("G8C_MASTER","G8C_LOJA") <> oModelGrid:GetValue( "G3R_LOJREP" )
					Help( , , "TA042FLPRE", , STR0010, 1, 0) //"Apenas documentos de reserva com fornecedor de report igual ao fornecedor da concilia��o podem ser importados."
					lRet := .F.
				EndIf
			Endif
			
			If lRet .And. oModelConc:GetModel( "G3R_ITENS" ):SeekLine({	{"G3R_NUMID" , oModelGrid:GetValue("G3R_NUMID") } ,;
																		{"G3R_IDITEM", oModelGrid:GetValue("G3R_IDITEM")} ,;
																		{"G3R_NUMSEQ", oModelGrid:GetValue("G3R_NUMSEQ")}})
				Help(,,"TA042FLPRE",,STR0011,1,0) //"Documento de reserva j� incluido nesta concilia��o."
				lRet := .F.
			ElseIf lRet .And. oModelConc:GetModel("G3R_ITENS"):SeekLine({{"G3R_FILIAL", oModelGrid:GetValue("G3R_FILIAL")}, ;
		                                          {"G3R_NUMID" , oModelGrid:GetValue("G3R_NUMID") }, ;
		                                          {"G3R_IDITEM", oModelGrid:GetValue("G3R_IDITEM")}, ;
		                                          {"G3R_NUMSEQ", oModelGrid:GetValue("G3R_SEQPRC")}}, .T.)
				TurHelp(STR0013, STR0015, "TA042FLPre")		// "Um Documento de Reserva de Parcialidade n�o pode ser conciliado atrav�s da concilia��o que o gerou."###"Concilie o Documento de Reserva em outra concilia��o."
				lRet := .F.
			ElseIf lRet .And. oModelConc:GetModel("G3R_ITENS"):SeekLine({{"G3R_FILIAL", oModelGrid:GetValue("G3R_FILIAL")}, ;
		                                          {"G3R_NUMID" , oModelGrid:GetValue("G3R_NUMID") }, ;
		                                          {"G3R_IDITEM", oModelGrid:GetValue("G3R_IDITEM")}, ;
		                                          {"G3R_NUMSEQ", oModelGrid:GetValue("G3R_SEQNSH")}}, .T.)
				TurHelp(STR0014, STR0015, "TA042FLPre")		// "Um Documento de Reserva de No Show n�o pode ser conciliado atrav�s da concilia��o que o gerou."###"Concilie o Documento de Reserva em outra concilia��o." 		                                          
				lRet := .F.
			EndIf
		EndIf
	EndIf
Return lRet

/*/{Protheus.doc} TA042FCommit
Fun��o de commit do modelo de dados
@type function
@author anderson
@since 24/03/2016
@version 1.0
/*/
Static Function TA042FCommit(oModelPesq)
	Local aArea      := GetArea()
	Local aAreaG3R   := G3R->(GetArea())
	Local cFilBkp	 := cFilAnt
	Local cNatureza	 := ""
	Local cTipo		 := ""
	Local cFornecedor:= ''
	Local cLoja 	 := ''
	Local lExistFunc := Findfunction('U_TURNAT')
	Local nX		 := 0
	Local nLine		 := 0
	Local oModel 	 := TA042Model()
	Local oMdlResult := oModelPesq:GetModel("TMP_DETAIL")
	Local oModelT34  := nil
	Local oModelT34F := nil
	Local oStruModel := nil
	Local oModelG3P  := nil
	Local oModelG3R  := nil
	Local oModelG48B := nil
	Local lRet       := .T.
	Local lMoedaDif  := .F.

	G3R->( dbSetOrder(1) )
	For nLine := 1 to oMdlResult:Length()
		oMdlResult:GoLine( nLine )

		If oMdlResult:GetValue("TMP_OK") .And. !oMdlResult:IsDeleted()
			cFilAnt := oMdlResult:GetValue("G3R_MSFIL")
			
			If !Empty(oModel:GetValue("G8C_MASTER", "G8C_MOEDA")) .And. oModel:GetValue("G8C_MASTER", "G8C_MOEDA") != oMdlResult:GetValue("G3R_MOEDA")
				lMoedaDif := .T.
			Else
				G3R->( dbSetOrder(1) )
				If G3R->( dbSeek( oMdlResult:GetValue("G3R_FILIAL") + oMdlResult:GetValue("G3R_NUMID") + oMdlResult:GetValue("G3R_IDITEM") + oMdlResult:GetValue("G3R_NUMSEQ") ) )
					DbSelectArea('G3P')
					G3P->(DbSetOrder(1))
					G3P->(DbSeek(oMdlResult:GetValue("G3R_FILIAL") + oMdlResult:GetValue("G3R_NUMID")))
					If SoftLock("G3P")
						oModelT34 := FwLoadModel( 'TURA042H' )
						oModelT34:SetOperation( MODEL_OPERATION_VIEW  )
		
						oModelT34:GetModel( 'G48B_ITENS'  ):SetLoadFilter({{ 'G48_CODAPU', "''" , MVC_LOADFILTER_EQUAL }, { 'G48_CONINU', "''", MVC_LOADFILTER_EQUAL }})
						oModelT34:GetModel( 'G48A_ITENS'  ):SetLoadFilter({{ 'G48_CODAPU', "''" , MVC_LOADFILTER_EQUAL }, { 'G48_CONINU', "''", MVC_LOADFILTER_EQUAL }})
						oModelT34:GetModel( 'G4CA_ITENS'  ):SetLoadFilter({{ 'G4C_STATUS', "'2'", MVC_LOADFILTER_LESS_EQUAL }})
						oModelT34:GetModel( 'G4CAF_ITENS' ):SetLoadFilter({{ 'G4C_STATUS', "'2'", MVC_LOADFILTER_GREATER }})
		
						If oModelT34:Activate()
							//Retorna como model ativo, o modelo da concilia��o
							FwModelActive( oModel )
							
							oModel:GetModel("G8C_MASTER"):SetValue("G8C_MOEDA", oMdlResult:GetValue("G3R_MOEDA"))
							
							TA042AcGrd( oModel, .T.  )
							If Empty(oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" )) .OR. Empty(oModel:GetValue( "G8C_MASTER", "G8C_LOJA" )) 
								oModel:SetValue( "G8C_MASTER", "G8C_FORNEC", G3R->G3R_FORREP )
								oModel:SetValue( "G8C_MASTER", "G8C_LOJA", G3R->G3R_LOJREP )
							Endif					
		
							aCopyValues := TURxGetVls('G3R_ITENS', .T., {'G4CB_ITENS'}, {}, oModelT34, {}, {{'G48A_ITENS', {{'G48_STATUS', {|x| x != '4'}}}}, {'G48B_ITENS', {{'G48_STATUS', {|x| x != '4'}}}}})
							If TURxSetVs( aCopyValues, {}, oModel )
								TA042SetRec("G3R_ITENS", {{"_CONORI", oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" )}}, , .F.)
								TA042SetRec("G3R_ITENS", {{"_CONMAN", "2"}}, , .T.)
		
								oModelG3P 	:= oModel:GetModel( 'G3P_FIELDS' )
								oModelG3R 	:= oModel:GetModel( 'G3R_ITENS' )
								oModelG48B	:= oModel:GetModel( 'G48B_ITENS' )
		
								oModelG3R:SetValue( 'G3R_FILCON', xFilial('G8C') )
		
								If lExistFunc
									If oModelG3R:GetValue( 'G3R_STATUS' ) == '1' .And. Empty( oModelG3R:GetValue( "G3R_NATURE"  ) )
		
										cNatureza := U_TURNAT(oModelG3R:GetValue('G3R_MSFIL') ,;
															  '1'							  ,;
															  ''							  ,;
															  oModelG3P:GetValue('G3P_SEGNEG'),;
															  oModelG3R:GetValue('G3R_PROD')  ,;
															  '2'							  ,;
															  oModelG3R:GetValue('G3R_FORREP'),;
															  oModelG3R:GetValue('G3R_LOJREP'))
										If !Empty(cNatureza) .And. TurVldNat(cNatureza, .T.)
											oModelG3R:LoadValue("G3R_NATURE", cNatureza)
										EndIf
									EndIf
								EndIf
		
								If !oModelG48B:IsEmpty()
									DbSelectArea('SED')
									SED->( dbSetOrder(1) )
		
									For nX := 1 to oModelG48B:Length()
										oModelG48B:GoLine( nX )
		
										If  oModelG48B:GetValue( "G48_COMSER" ) == "1"
											oModelG48B:LoadValue( "G48_RECCON", .T. )
										Else
											oModelG48B:LoadValue( "G48_RECCON", .F. )
										EndIf
		
										If lExistFunc
		
											If Empty( oModelG48B:GetValue( 'G48_NATURE' ) )
												If oModelG48B:GetValue( 'G48_TPACD' )  == '1'
													cTipo := '3'
												ElseIf oModelG48B:GetValue( 'G48_TPACD' ) == '2'
													cTipo := '4'
												EndIf
		
												G4W->(dbSetOrder(1))
												G4W->(dbSeek(xFilial("G4W")+oModelG48B:GetValue("G48_CODACD")+oModelG48B:GetValue("G48_CODREC")))
												If G4W->G4W_TPENT == "2" //Fornecedor de Reporte
													cFornecedor := oModelG3R:GetValue("G3R_FORREP")
													cLoja 		:= oModelG3R:GetValue("G3R_LOJREP")
					
												Else //Fornecedor de Produto
													cFornecedor	:= oModelG3R:GetValue("G3R_FORNEC")
													cLoja		:= oModelG3R:GetValue("G3R_LOJA"  )
					
												EndIf
					
												cNatureza := U_TURNAT(oModelG3R:GetValue('G3R_MSFIL') ,;
																	  cTipo							  ,;
																	  oModelG48B:GetValue('G48_CLASS'),;
																	  oModelG3P:GetValue('G3P_SEGNEG'),;
																	  oModelG3R:GetValue('G3R_PROD')  ,;
																      '2'							  ,;
																	  cFornecedor					  ,;
																	  cLoja)
		
		
												If !Empty(cNatureza) .And. TurVldNat(cNatureza, .T.)
													oModelG48B:LoadValue('G48_NATURE', cNatureza)
												EndIf
											EndIf
										EndIf
									Next
									SED->(DbCloseArea())
								EndIf
								
								//Atualiza Demonstrativo Financeiro
								T34AtuDmFi( oModel, .F. )
		
								//Gera novos itens financeiros
								If !(lRet := Tur34ItFin(oModel, '3', .T., '3', , .T.))
									oModelG3R:SetValue('G3R_DIVERG', '1')	
									IIF(FwIsInCallStack("TURA042A"), TA042AcGrd( oModel, .T. ), TA042RAcGr( oModel, .T. ))
									SetAllDelete(.T.)
									TA42DelSons( oModel, 'G3R_ITENS', .F. )
									SetAllDelete(.F.)
									IIF(FwIsInCallStack("TURA042A"), TA042AcGrd( oModel, .F. ), TA042RAcGr( oModel, .F. ))
									Help( , , "TA042FCOMMIT", , I18N(STR0012, {oModelG3R:GetValue('G3R_NUMID'), oModelG3R:GetValue('G3R_IDITEM'), oModelG3R:GetValue('G3R_NUMSEQ')}) + CRLF + CRLF + TurGetErrorMsg(oModel), 1, 0)		// "Falha ao atualizar os Itens Financeiros do Documento de Reserva #1/#2/#3. Ele n�o ser� inserido na concilia��o."
								Else
									//Atualiza o grid de totalizadores e o cabe�alho
									CALCVLG8C(oModelG3R, .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "ADDLIN"/*cMomento*/)
								EndIf
							Else
								lContinua := .F.
								TurGetErrorMsg( oModel )
							EndIf
		
							oModelT34:DeActivate()
						EndIf
		
						oModelT34:Destroy()
					EndIf
					G3P->(DbCloseArea())
				EndIf
			EndIf
		EndIf
	Next
	
	If lMoedaDif
		Help( , , "TA042FCommit", , STR0016, 1, 0)	// "N�o foi poss�vel incluir um ou mais Documentos de Reserva por estarem com a moeda diferente dos demais DRs desta concilia��o."
	EndIf
	
	TA042AcGrd( oModel, .F.  )
	cForConc := ""
	cFLjConc := ""
	cFilAnt	 := cFilBkp 

	RestArea(aAreaG3R)
	RestArea(aArea)
	
	TURXNIL(aArea)
	TURXNIL(aAreaG3R)
Return .T.

/*/{Protheus.doc} TA039Tb2Md
Fun��o de importa��o dos dados da tabela para o modelo de dados
@type function
@author anderson
@since 24/03/2016
@version 1.0
/*/
Static Function TA039Tb2Md(oModel, cTable, nIndex, cFldKey, cKey, bCondNewLine, aSeek , bCondReg, aValues)
	Local aErro	     := {}
	Local aSeekModel := {}
	Local cErro	     := ""
	Local lLoop	     := .F.
	Local lRet 	     := .T.
	Local nX		 := 0
	Local nY		 := 0
	Local oMdlActive := Nil
	Local xValue	 := Nil

	DEFAULT bCondReg	 := {|| .T. }
	DEFAULT bCondNewLine := {|| .T. }
	DEFAULT aSeek		 := {}
	DEFAULT aValues		 := {}

	(cTable)->( dbSetOrder(nIndex) )
	(cTable)->( dbSeek( cKey ) )

	While (cTable)->( !EOF() ) .And. (cTable)->( &(cFldKey) ) == cKey
		If !Eval( bCondReg )
			(cTable)->( dbSkip() )
			Loop
		EndIf

 		If len(aSeek) > 0
 			aSeekModel := {}
 			For nY := 1 to len(aSeek[2])
 				aAdd( aSeekModel, { aSeek[2][nY][1], IIF( ValType(aSeek[2][nY][2]) == "B", Eval(aSeek[2][nY][2]), aSeek[2][nY][1]) } )
 			Next

 			If !aSeek[1]:SeekLine( aSeekModel )
				(cTable)->( dbSkip() )
				Loop
			EndIf
		EndIf

		If Eval( bCondNewLine )
			If oModel:Length() == oModel:AddLine()
				oMdlActive := FwModelActive()
				TA039ShwError( "TURA039BADDLINE",  oMdlActive:GetErrorMessage() )
				Return .F.
			EndIf
		EndIf

 		For nX := 1 To (cTable)->( FCount() )
			If oModel:HasField( AllTrim((cTable)->( FieldName(nX) ) ) )

				//� realizado um LoadValue por se tratar de uma c�pia da linha original
				If !oModel:LoadValue( AllTrim((cTable)->( FieldName(nX) ) ), (cTable)->( FieldGet(nX) ) )
					oMdlActive := FwModelActive()
					TA039ShwError( "TURA039BSETVALUE",  oMdlActive:GetErrorMessage() )
					lRet := .F.
					Exit
				EndIf
			EndIf
		Next

		For nX := 1 To len( aValues )

			xValue := aValues[nX][2]
			If ValType( aValues[nX][2] ) == "C" .And. SubStr(aValues[nX][2],1,1) == "&"
				xValue := &( SubStr(aValues[nX][2],2) )
			EndIf

			If !oModel:LoadValue( aValues[nX][1], xValue )
				oMdlActive := FwModelActive()
				TA039ShwError( "TURA039BSETVALUE",  oMdlActive:GetErrorMessage() )
				lRet := .F.
				Exit
			EndIf
		Next

 		(cTable)->( dbSkip() )
	EndDo

	TURXNIL(aErro)
	TURXNIL(aSeekModel)
Return lRet

/*/{Protheus.doc} TA039ShwError
Apresenta mensagem de erro
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
/*/
Static Function TA039ShwError( cIdHelp, aErro )
	Local cErro := ""

	cErro := STR0001 + " [" + AllToChar( aErro[1] ) + "]" + CRLF //'Id do formul�rio de origem:'
	cErro += STR0002 + " [" + AllToChar( aErro[2] ) + "]" + CRLF //'Id do campo de origem: '
	cErro += STR0003 + " [" + AllToChar( aErro[3] ) + "]" + CRLF //'Id do formul�rio de erro: '
	cErro += STR0004 + " [" + AllToChar( aErro[4] ) + "]" + CRLF //'Id do campo de erro: '
	cErro += STR0005 + " [" + AllToChar( aErro[5] ) + "]" + CRLF //'Id do erro: '
	cErro += STR0006 + " [" + AllToChar( aErro[6] ) + "]" + CRLF //'Mensagem do erro: '
	cErro += STR0007 + " [" + AllToChar( aErro[7] ) + "]" + CRLF //'Mensagem da solu��o: '
	cErro += STR0008 + " [" + AllToChar( aErro[8] ) + "]" + CRLF //'Valor atribu�do: '
	cErro += STR0009 + " [" + AllToChar( aErro[9] ) + "]" + CRLF //'Valor anterior: '

	Help( , , cIdHelp, , cErro, 1, 0)
Return

Static Function TA042FVAL(oModel, cField, xValue, nLine, xOldValue)
Local lRet := .T.

Default cForConc := ""
Default cFLjConc := ""

If !oModel:GetValue('TMP_OK')
	If !oModel:SeekLine({{'TMP_OK',.T.}})
		cForConc := "" 
		cFLjConc := ""
	Endif
Else
	cForConc := oModel:GetValue('G3R_FORREP') 
	cFLjConc := oModel:GetValue('G3R_LOJREP')
EndIf

oModel:GoLine(nLine)

Return lRet