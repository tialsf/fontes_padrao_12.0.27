#Include 'Totvs.ch'
#Include 'FWMVCDef.ch'
#Include 'TURA042G.ch'

/*/{Protheus.doc} ModelDef
Modelo de dados para apresenta��o do log de efetiva��o
@type function
@author Anderson Toledo
@since 06/04/2016
@version 1.0
/*/
Static Function ModelDef()
	Local oModel   := Nil
	Local oStruG8C := FwFormStruct(1, 'G8C') //Cabe�alho Concilia��o Terrestre
	Local oStruG8Y := FwFormStruct(1, 'G8Y') //T�tulos x Concilia��o

	oModel := MPFormModel():New('TURA042G', /*bPreValidacao*/, /*bPosValidacao*/, {|| .T.}, /*bCancel*/)

	oModel:AddFields('G8C_MASTER', /*cOwner*/, oStruG8C)

	//Documentos de reserva
	oModel:AddGrid('G8Y_ITENS', 'G8C_MASTER', oStruG8Y)
	oModel:SetRelation('G8Y_ITENS', {{'G8Y_FILIAL', 'G8C_FILIAL'}, {'G8Y_CONCIL', 'G8C_CONCIL'}}, G8Y->(IndexKey(1)))
	oModel:GetModel('G8Y_ITENS'):SetDescription( STR0001 ) //'Documentos de Reserva'
	oModel:GetModel('G8Y_ITENS'):SetNoInsertLine( .F. )
	oModel:GetModel('G8Y_ITENS'):SetNoUpdateLine( .F. )
	oModel:GetModel('G8Y_ITENS'):SetNoDeleteLine( .F. )
Return oModel

/*/{Protheus.doc} ViewDef
Vis�o para apresenta��o do log de efetiva��o
@type function
@author Anderson Toledo
@since 06/04/2016
@version 1.0
/*/
Static Function ViewDef()
	Local oView	   := FWFormView():New()
	Local oModel   := FWLoadModel('TURA042G')
	Local oStruG8Y := FwFormStruct(2, 'G8Y') //T�tulos x Concilia��o

	oView:SetModel(oModel)

	oView:AddGrid('VIEW_G8Y', oStruG8Y, 'G8Y_ITENS')
	oView:EnableTitleView('VIEW_G8Y')
	oView:CreateHorizontalBox('BOX_ALL', 100)
	oView:SetOwnerView('VIEW_G8Y', 'BOX_ALL')
Return oView