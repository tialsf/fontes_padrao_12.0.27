#INCLUDE "TURA042A.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

/*/{Protheus.doc} TA042AEfet
Fun��o para efetiva��o da concilia��o terrestre
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cOpc, character, Indica o tipo de opera��o a ser execudo: "1" - Efetiva��o da concilia��o; "2" - Estorno da efetiva��o
/*/
Function TA042AEfet( cModelEfet )
	Local aArea         := GetArea()
	Local aAreaSF1      := SF1->(GetArea())
	Local aAreaSE2      := SE2->(GetArea())
	Local aAreaSED      := SED->(GetArea())
	Local aAreaSA2      := SA2->(GetArea())
	Local aValues		:= {}
	Local aFornPrd		:= {}
	Local aItensNFS 	:= {}
	Local aDadosTit		:= {}
	Local aDadosTitP	:= {}
	Local aDadosTitR	:= {}
	Local aValAbatP		:= {}
	Local aValAbatR		:= {}
	Local aTitulos		:= {}
	Local aLogG8Y		:= {}
	Local lContinua		:= .T.
	Local lConfirmNFS 	:= .F.
	Local nValFatura	:= 0
	Local nTitAvulso	:= 0
	Local oModel		:= nil
	Local oModelG3R		:= nil
	Local oModelG8Y		:= nil
	Local oModelG48A	:= nil
	Local oModelG48B	:= nil
	Local oModelG4CA	:= nil
	Local oModelG4CB	:= nil
	Local cAlias		:= ""
	Local cErro			:= ""
	Local aDados		:= {}
	Local aParc			:= {}
	Local cNumNFS		:= ""
	Local cSerNFS		:= ""
	Local cNatTit		:= ""
	Local nX			:= 0
	Local nY			:= 0
	Local nZ			:= 0
	Local nFilRef		:= 0
	Local nPosSerie		:= 0
	Local nPosNatur		:= 0
	Local cPerg			:= ""

	Local lOnLine		:= .T.
	Local lMostrLanc	:= .T.
	Local lAglutLanc	:= .F.

	Local cFilAntBkp	:= cFilAnt
	Local cChave		:= ""
	Local lMostraErro 	:= .F.
	Local aStack 	  	:= {}
	Local cStackMsg   	:= ""
	Local nIt 		  	:= 0
	Local cFileG8C    	:= "CONCIL-"+ALLTRIM(G8C->G8C_CONCIL)+"-"+Dtos(Date())+StrTran(Time(),":","")+".log"
	Local _nHandTXT		:= Nil

	Private cNumero		:= ""

	DEFAULT cModelEfet 	:= "TURA042A"

	If cModelEfet == "TURA042A"
		cPerg := "TURA042A"
	ElseIf 	cModelEfet == "TURA042R"
		cPerg := "TURA042R"
	EndIf

	Pergunte(cPerg,.F.)

	lOnLine 	:= Iif(mv_par01 == 1,.T.,.F.)
	lMostrLanc	:= Iif(mv_par02 == 1,.T.,.F.)
	lAglutLanc	:= Iif(mv_par03 == 1,.T.,.F.)

	If !LockByName( xFilial('G8C') + G8C->G8C_CONCIL, .T., .T. )
		TurHelp(STR0187, STR0188, 'TA42INOLOCK')	//"N�o foi poss�vel bloquear o registro para efetiva��o."	//"Registro em edi��o ou efetiva��o por outra esta��o, aguarde alguns instantes e tente novamente."
		Return .F.
	EndIf

	//Valida��o de efetiva��o parcial
	If G8C->G8C_STATUS == '3'
		If ApMsgYesNo( STR0179 ) //"O processo de efetiva��o n�o foi concluido com sucesso, deseja estornar a efetiva��o atual?"
			If !TA042ADEfe( cModelEfet )
				TurHelp( STR0180, STR0181, 'TA42IERREST' ) //'Erro ao estornar a efetiva��o'###'Entre em contato com o administrador do sistema'
				Return .F.
			EndIf
		Else
			Return .F.
		EndIf
	EndIf

	//Calcula o total de receita dos itens financeiros selecionados
	nValFatura := TA042CaVrF(cModelEfet)

	If nValFatura <> G8C->G8C_VLRFAT
		Help(,,"TA042AEFEVALF",,i18N(STR0104, {	AllTrim( Transform(G8C->G8C_VLRFAT	,PesqPict("G8C","G8C_VLRFAT") ) ), AllTrim( Transform(nValFatura ,PesqPict("G8C","G8C_VLRFAT") ) ) 	}),1,0)  //"O valor informado na fatura ( #1[Valor Fatura]# ) � divergente do calculado nos itens financeiros ( #2[Valor Fatura]# )."
		lContinua := .F.
		Return
	EndIf

	If Empty( G8C->G8C_NUMDOC ) .Or. Empty( G8C->G8C_SERIE ) .Or. Empty( G8C->G8C_CONDPG  )
		Help(,,"TA042EFETIVA",,STR0079,1,0)			 //"Informe o n� do documento de entrada e sua s�rie para efetiva��o da concilia��o"
		lContinua := .F.
		Return
	EndIf

	If !TA42VldCPg( G8C->G8C_CONDPG ) .Or. !TA42VlCPNF( PADR(SuperGetMV("MV_TURCCP", , ""), TamSX3("G8C_CONDPG")[1]), "MV_TURCCP" )
		lContinua := .F.
		Return
	EndIf

	//Verifica se o n�mero da fatura/t�tulo j� existe como t�tulo a pagar
	If G8C->G8C_DOCENT == '1'
		SF1->( dbSetOrder(1) )
		If SF1->( dbSeek( xFilial('SF1') + G8C->( G8C_NUMDOC+G8C_SERIE+G8C_FORNEC+G8C_LOJA ) ) )
			Help(,,"TA042EFEDOC1",,i18N(STR0109,{AllTrim(G8C->G8C_NUMDOC), AllTrim(G8C->G8C_SERIE)}),1,0) //"J� existe uma NFE com o n�mero #1[N�mero T�tulo]#, prefixo #2[Prefixo T�tulo]# para este fornecedor."
			lContinua := .F.
			Return
		EndIf
	EndIf

	SE2->( dbSetOrder(6) )
	If SE2->( dbSeek( xFilial('SE2') + G8C->( G8C_FORNEC+G8C_LOJA+G8C_SERIE+G8C_NUMDOC ) ) )
		Help(,,"TA042EFEDOC2",,i18N(STR0110,{AllTrim(G8C->G8C_NUMDOC), AllTrim(G8C->G8C_SERIE)}),1,0)			 //"J� existe um t�tulo a pagar com o n�mero #1[N�mero T�tulo]#, prefixo #2[Prefixo T�tulo]# para este fornecedor."
		lContinua := .F.
		Return
	EndIf

	//Valida��o p�s ativa��o do modelo
	oModel := FwLoadModel( cModelEfet )
	oModel:SetOperation( MODEL_OPERATION_UPDATE )
	oModel:Activate()

	//Valida��o se a natureza est� preenchida
	If !TA042AVlNat( oModel, @cErro, '2' )
		FwAlertHelp(cErro,STR0130,"TA042NATUR") //"Informe as naturezas dos itens listados na concilia��o."
		oModel:DeActivate()
		lContinua := .F.
		Return
	EndIf

	If lContinua
		cAlias := GetNextAlias()
	
		LogConcilT(@_nHandTXT,cFileG8C,1,"")	
	
		//Obter todos os fornecedores que ser�o efetivados
		BeginSql Alias cAlias
			SELECT DISTINCT G4C_CODIGO, G4C_LOJA
			FROM %Table:G3R% G3R
			INNER JOIN %Table:G4C% G4C ON G4C_FILIAL = G3R_FILIAL
				AND G4C_NUMID = G3R_NUMID
				AND G4C_IDITEM = G3R_IDITEM
				AND G4C_NUMSEQ = G3R_NUMSEQ
				AND G4C_CLIFOR = '2'
				AND G4C_CARTUR = ' '
				AND G4C_CONORI = G3R_CONCIL
				AND G4C.%notDel%
			WHERE G3R_CONCIL = %Exp:oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" )%
				AND G3R_FILCON = %Exp:oModel:GetValue( "G8C_MASTER", "G8C_FILIAL" )%
				AND G3R.%notDel%
		EndSql

		oModelG8Y   := FwLoadModel( "TURA042B" )
		oModelG3R	:= oModel:GetModel( "G3R_ITENS" )
		oModelG4CA	:= oModel:GetModel( "G4CA_ITENS" )
		oModelG4CB	:= oModel:GetModel( "G4CB_ITENS" )
		oModelG48A	:= oModel:GetModel( "G48A_ITENS" )
		oModelG48B	:= oModel:GetModel( "G48B_ITENS" )

		aAdd(aValues, {"G8C_STATUS", "3"		})
		aAdd(aValues, {"G8C_DTEFET", dDataBase	})
		aAdd(aValues, {"G8C_FILEFE", cFilAnt	})

		lContinua := TA042SetVl(oModel,"G8C_MASTER",aValues )

		If lContinua .And. oModel:VldData()
			oModel:CommitData()
		Else
			TurHelp( STR0182, oModel:GetErrorMessage()[6], 'TA42IEFETERR' ) //'N�o foi poss�vel efetivar a concilia��o.'
			lContinua := .F.
		EndIf

		While lContinua .And. (cAlias)->(!EOF()) 
			aDadosTitP := {}
			aDadosTit  := {}
			aItensNFS  := {}
			aValAbatP  := {}
			aValAbatR  := {}
			nTitAvulso := 0
			cErro      := ""
			TurSetNatur( "" )
			FwModelActive(oModel)

			//Verifica os itens para faturamento do fornecedor informado via parametro
			If TA042ItNFS( (cAlias)->G4C_CODIGO, (cAlias)->G4C_LOJA, aItensNFS, @aValAbatR, @cErro, oModel )

				If len( aItensNFS ) > 0
					For nFilRef := 1 To Len( aItensNFS ) //Loop por filiais de refer�ncia
						If !lContinua
							Exit
						EndIf

						If !Empty( aItensNFS[nFilRef][1] )
							cFilAnt := aItensNFS[nFilRef][1]
						EndIf

						For nPosSerie := 1 To Len( aItensNFS[nFilRef][2] ) //Loop por s�rie das notas fiscais
							If !lContinua
								Exit
							EndIf

							For nPosNatur := 1 To Len( aItensNFS[nFilRef][2][nPosSerie][2]) //Loop por natureza
								If !lContinua
									Exit
								EndIf

								If Empty( aItensNFS[nFilRef][2][nPosSerie][2] )
									TurHelp(STR0133, STR0134, 'TA042AEFNAT')	//"Verifique o cadastro de 'Parametros Fiscais e Financeiros' se os c�digos de Naturezas est�o corretos."	//"Natureza em branco."
									lContinua := .F.
									Exit
								ElseIf !SED->( dbSeek( xFilial('SED') + aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1] ) )
									TurHelp(I18N(STR0135,{aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1],cFilAnt}), STR0136, 'TA042AEFNAT2')		//"Natureza #1[codigo natureza]# n�o cadastrada na filial #2[C�digo filial]#."		//"Verifique o cadastro de Naturezas na filial informada."
									lContinua := .F.
									Exit
								EndIf

								cNumNFS := ''
								cSerNFS := aItensNFS[nFilRef][2][nPosSerie][1]
								TurSetNatur( aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][1] )

								//Gera��o da nota fiscal de sa�da
								BEGIN TRANSACTION
									aLogG8Y  := {}
									lErroLog := .F.
									LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de NF (TA042EfNFS)"+CRLF)	
									If TA042EfNFS((cAlias)->G4C_CODIGO, (cAlias)->G4C_LOJA, @cNumNFS, @cSerNFS, aItensNFS[nFilRef][2][nPosSerie][2][nPosNatur][2], aLogG8Y)
										LogConcilT(_nHandTXT,cFileG8C,2, "NF GERADA "+AsString( aLogG8Y )+CRLF)
										cFilAnt := cFilAntBkp
										If TA42AddLog( oModelG8Y, aLogG8Y, oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
											lContinua := .T.
										Else
											DisarmTransaction()
											lErroLog := .T.
											lContinua := .F.
										EndIf
									Else
										LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de NF (TA042EfNFS)"+CRLF)
										DisarmTransaction()
										lContinua := .F.
									EndIf
								END TRANSACTION
							Next

							If !lContinua
								If lErroLog
									TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
								Else
									Help(,,"TA042NFE",,STR0077,1,0)	 //"Erro ao gerar a nota fiscal de entrada."
									MostraErro()
								EndIf
							EndIf	
						Next
					Next
				EndIf
			Else
				Help(,,"TA042EFETCLI",,cErro,1,0)
				lContinua := .F.
			EndIf

			//Realiza as transfer�ncias dos t�tulos a receber para o fornecedor da fatura
			If lContinua .And. (oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" ) <> (cAlias)->G4C_CODIGO .Or. oModel:GetValue( "G8C_MASTER", "G8C_LOJA" ) <> (cAlias)->G4C_LOJA)
				LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Transf. T�tulos (TA042GetTit)"+CRLF)
				aTitulos := TA042GetTit( oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), '2', (cAlias)->G4C_CODIGO, (cAlias)->G4C_LOJA , '1' )
				LogConcilT(_nHandTXT,cFileG8C,2, "NF GERADA "+AsString( aTitulos )+CRLF)
				SA2->( dbSetOrder(1) )
				SA2->( dbSeek( xFilial("SA2") + oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" ) + oModel:GetValue( "G8C_MASTER", "G8C_LOJA" ) ) )
				For nX := 1 to len( aTitulos )
					If !lContinua
						Exit
					EndIf	
					
					aLogG8Y := {}
					
					BEGIN TRANSACTION
						LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Transf. T�tulos (TA042TrfTi)"+CRLF)
						If TA042TrfTi( '', aTitulos[nX], SA2->A2_CLIENTE, SA2->A2_LOJCLI, SA2->A2_COD, SA2->A2_LOJA, aLogG8Y )
							LogConcilT(_nHandTXT,cFileG8C,2, "NF GERADA "+AsString( aLogG8Y )+CRLF)
							For nY := 1 to len( aLogG8Y )
								If TA42AddLog( oModelG8Y, aLogG8Y[nY], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
									lContinua := .T.
								Else
									DisarmTransaction()
									lContinua := .F.
									Exit
								EndIf
							Next
						Else
							LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Transf. T�tulos (TA042TrfTi)"+CRLF)
							DisarmTransaction()
							lContinua := .F.
						EndIf
					END TRANSACTION
				Next
				
				If !lContinua
					TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
				EndIf				
			EndIf
			(cAlias)->( dbSkip() )
		EndDo
		(cAlias)->( dbCloseArea() )

		If lContinua
			//Obtem os valores para os t�tulos a pagar e receber
			LogConcilT(_nHandTXT,cFileG8C,2, "Dados dos T�tulos a Rec.(TA042DaTit)"+CRLF)
			TA042DaTit( oModel, aValAbatP, aDadosTitP, aValAbatR, aDadosTitR, .F., oModel:GetValue( "G8C_MASTER","G8C_DOCENT" ) == "1" )

			If lContinua .And. len(aDadosTitP) > 0
				LogConcilT(_nHandTXT,cFileG8C,2, "Dados dos T�tulos a Rec. aDadosTitP "+AsString( aDadosTitP )+CRLF)		
				If oModel:GetValue( "G8C_MASTER","G8C_DOCENT" ) == '1'
					// Gera��o da nota fiscal de entrada
					If  len(aDadosTitP) + len(aValAbatR) > 1
						Help(,,"TA042NATNFE",,STR0149,1,0) //"Existe mais de uma natureza cadastrada no demonstrativo financeiro do fornecedor para o tipo intermedia��o, verifique o Demonstrativo na concilia��o."
						lContinua := .F.
					Else
						BEGIN TRANSACTION
							aLogG8Y  := {}
							lErroLog := .F.
							LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de NFE (TA042EfNFE)"+CRLF)
							If TA042EfNFE( oModel, aDadosTitP[1], aLogG8Y )
								LogConcilT(_nHandTXT,cFileG8C,2, "NFe GERADA "+AsString( aLogG8Y )+CRLF)
								If len( aLogG8Y ) > 0
									If TA42AddLog( oModelG8Y, aLogG8Y, oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
										lContinua := .T.
									Else
										DisarmTransaction()
										lErroLog := .T.
										lContinua := .F.
									EndIf
								EndIf
							Else
								LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de NFE (TA042EfNFE)"+CRLF)
								DisarmTransaction()
								lContinua := .F.
							EndIf
						END TRANSACTION

						If !lContinua
							If lErroLog
								TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
							Else
								Help(,,"TA042NFE",,STR0077,1,0)	 //"Erro ao gerar a nota fiscal de entrada."
								MostraErro()
							EndIf
						EndIf	
					EndIf
				Else
					// Gera��o do t�tulo a pagar - Quando n�o gera nota de entrada
					For nZ := 1 to len(aDadosTitP)
						If !lContinua
							Exit
						EndIf

						aLogG8Y  := {}
						lErroLog := .F.	
						TA042ASetMdl(oModel)
						BEGIN TRANSACTION
							LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de T�tulos Pag. (TA042EfTiP)"+CRLF)
							If TA042EfTiP(''											,;
										  oModel:GetValue( "G8C_MASTER", "G8C_SERIE"  )	,;
										  oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" )	,;
										  oModel:GetValue( "G8C_MASTER", "G8C_LOJA"   )	,;
										  aDadosTitP[nZ][2]								,;
										  aDadosTitP[nZ][3]								,;
										  oModel:GetValue( "G8C_MASTER", "G8C_EMISSA" )	,;
										  oModel:GetValue( "G8C_MASTER", "G8C_CONDPG" )	,;
										  aDadosTitP[nZ][4]								,;
										  oModel:GetValue( "G8C_MASTER", "G8C_VENCIM" )	,;
										  aLogG8Y)
								LogConcilT(_nHandTXT,cFileG8C,2, "Titulos a a Pagar "+AsString( aLogG8Y )+CRLF)
								For nX := 1 to len( aLogG8Y )
									If TA42AddLog( oModelG8Y, aLogG8Y[nX], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
										lContinua := .T.
									Else
										DisarmTransaction()
										lErroLog  := .F.
										lContinua := .F.
										Exit
									EndIf
								Next
							Else
								LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de T�tulos Pag. (TA042EfTiP)"+CRLF)
								DisarmTransaction()
								lContinua := .F.
							EndIf
						END TRANSACTION

						If !lContinua
							If lErroLog
								TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
							Else
								Help(,,"TA042ATITP",,STR0139,1,0) //"Erro ao gerar o t�tulo a pagar, processo ser� estornado."
							EndIf
						EndIf	
					Next
				EndIf
			EndIf

			//Gera��o de t�tulo a receber de produtos sem cadastro nos parametros financeiros
			If lContinua .And. Len( aDadosTitR ) > 0
				LogConcilT(_nHandTXT,cFileG8C,2, "Dados dos T�tulos a Rec. aDadosTitR "+AsString( aDadosTitR )+CRLF)
				For nY := 1 to len(aDadosTitR)
					If !lContinua
						Exit
					EndIf
					
					aLogG8Y := {}
					cFilAnt := aDadosTitR[nY][1]
					BEGIN TRANSACTION 
						LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Tit.Rec. (TA042EfTiR)-aDadosTitR"+CRLF)
						If TA042EfTiR(oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" )	,;
									  oModel:GetValue( "G8C_MASTER", "G8C_LOJA"   )	,;
									  aDadosTitR[nY][2],;
									  aDadosTitR[nY][3],;
									  aLogG8Y )
							LogConcilT(_nHandTXT,cFileG8C,2, "Gerado de Tit.Rec. (TA042EfTiR) - "+AsString( aLogG8Y )+CRLF)
							For nX := 1 to len( aLogG8Y )
								cFilAnt := cFilAntBkp
	
								If TA42AddLog( oModelG8Y, aLogG8Y[nX], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
									lContinua := .T.
								Else
									DisarmTransaction()
									lErroLog  := .T.
									lContinua := .F.
									Exit
								EndIf
							Next
						Else
							LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Tit.Rec. NF (TA042EfTiR)"+CRLF)
							DisarmTransaction()
							lContinua := .F.						
						EndIf
					END TRANSACTION
					
					If !lContinua
						If lErroLog
							TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
						Else
							Help(,,"TA042ATITP",,STR0139,1,0) //"Erro ao gerar o t�tulo a pagar, processo ser� estornado."
						EndIf
					EndIf	
				Next
			EndIf

			If lContinua .And. len( aValAbatP ) > 0
				LogConcilT(_nHandTXT,cFileG8C,2, "Dados dos T�tulos a Rec. aValAbatP "+AsString( aValAbatP )+CRLF)
				For nY := 1 to len(aValAbatP)
					If !lContinua
						Exit
					EndIf

					aLogG8Y := {}
					cFilAnt := aValAbatP[nY][1]
					BEGIN TRANSACTION
						LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Tit.Rec.(TA042EfTiR)-aValAbatP"+CRLF)
						If TA042EfTiR(oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" ),;
									  oModel:GetValue( "G8C_MASTER", "G8C_LOJA"   ),;
									  aValAbatP[nY][2],;
									  aValAbatP[nY][3],;
									  aLogG8Y)
							LogConcilT(_nHandTXT,cFileG8C,2, "Gerado de Tit.Rec.(TA042EfTiR) "+AsString( aLogG8Y )+CRLF)
							For nX := 1 to len( aLogG8Y )
								cFilAnt := cFilAntBkp
								If TA42AddLog( oModelG8Y, aLogG8Y[nX], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
									lContinua := .T.
								Else
									lErroLog  := .T.	
									lContinua := .F.
									Exit
								EndIf
							Next
						Else
							LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Tit.Rec.(TA042EfTiR)-aValAbatP"+CRLF)
							DisarmTransaction()
							lContinua := .F.
						EndIf
					END TRANSACTION
				
					If !lContinua
						If lErroLog
							TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
						Else
							TurHelp(STR0140, STR0138, 'TA42AG8YTITR')	//"Erro ao gravar titulo receber."	//"Entre em contato com o administrador do sistema"
						EndIf
					EndIf	
				Next
			EndIf

			If lContinua .And. len( aValAbatR ) > 0
				LogConcilT(_nHandTXT,cFileG8C,2, "Dados dos T�tulos a Rec. aValAbatR "+AsString( aValAbatR )+CRLF)
				For nZ := 1 to len(aValAbatR)
					If !lContinua
						Exit
					EndIf

					aLogG8Y := {}
					TA042ASetMdl(oModel)
					BEGIN TRANSACTION 
						LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Tit.Pag. (TA042EfTiP)"+CRLF)
						If TA042EfTiP(''											,;
									  oModel:GetValue( "G8C_MASTER", "G8C_SERIE"  )	,;
									  oModel:GetValue( "G8C_MASTER", "G8C_FORNEC" )	,;
									  oModel:GetValue( "G8C_MASTER", "G8C_LOJA"   )	,;
									  aValAbatR[nZ][2]								,;
									  aValAbatR[nZ][3]								,;
									  oModel:GetValue( "G8C_MASTER", "G8C_EMISSA" )	,;
									  oModel:GetValue( "G8C_MASTER", "G8C_CONDPG" )	,;
									  aValAbatR[nZ][4]								,;
									  oModel:GetValue( "G8C_MASTER", "G8C_VENCIM" )	,;
									  aLogG8Y )
							LogConcilT(_nHandTXT,cFileG8C,2, "Gerado de Tit.Pag. (TA042EfTiP): "+AsString( aLogG8Y )+CRLF)
							For nX := 1 to len( aLogG8Y )
								If !lContinua
									Exit
								EndIf	
								
								If TA42AddLog( oModelG8Y, aLogG8Y[nX], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
									lContinua := .T.
								Else
									DisarmTransaction()
									lErroLog  := .T.
									lContinua := .F.
									Exit
								EndIf
							Next
						Else
							LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Tit.Pag. (TA042EfTiP)"+CRLF)
							DisarmTransaction()
							lContinua := .F.
						EndIf
					END TRANSACTION

					If !lContinua
						If lErroLog
							TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
						Else
							TurHelp(STR0139,"","TA042ATITP") //"Erro ao gerar o t�tulo a pagar, processo ser� estornado."	
						EndIf
					EndIf	
				Next
			EndIf
		EndIf

		//Retorna o Model Ativo para o modelo da concilia��o
		FwModelActive( oModel )

		//Compensa��o dos t�tulos
		If lContinua
			aLogG8Y := {}
			BEGIN TRANSACTION
				//Realiza a transferencia dos t�tulos a recebe para a filial corrente
				LogConcilT(_nHandTXT,cFileG8C,2, "Transferencia dos t�tulos a recebe (TA042TrAllT)"+CRLF)
				If TA042TrAllT( oModel:GetValue('G8C_MASTER','G8C_CONCIL'), '2', oModel:GetValue('G8C_MASTER','G8C_FORNEC'), oModel:GetValue('G8C_MASTER','G8C_LOJA'), aLogG8Y )
					LogConcilT(_nHandTXT,cFileG8C,2, "Transferencia de Tit.Realizada: "+AsString( aLogG8Y )+CRLF)
					For nY := 1 to len( aLogG8Y )
						If TA42AddLog( oModelG8Y, aLogG8Y[nY], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
							lContinua := .T.
						Else
							DisarmTransaction()
							lErroLog  := .T.
							lContinua := .F.
							Exit
						EndIf
					Next
				Else
					LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Transferencia dos t�tulos a recebe (TA042TrAllT)"+CRLF)
					DisarmTransaction()
					lContinua := .F.
				EndIf
			END TRANSACTION
			
			If !lContinua
				If lErroLog
					TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
				Else
					TurHelp(STR0183,"","TA042ATRFT") //"Erro ao transferir t�tulo para a filial corrente" 	
				EndIf
			EndIf	
			
			aLogG8Y := {}
			If lContinua 
				BEGIN TRANSACTION
					LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Comppensa��o (TA042CompT)"+CRLF)
					If TA042CompT( oModel:GetValue('G8C_MASTER','G8C_CONCIL'), '2', oModel:GetValue('G8C_MASTER','G8C_FORNEC'), oModel:GetValue('G8C_MASTER','G8C_LOJA'), aLogG8Y )
						LogConcilT(_nHandTXT,cFileG8C,2, "Compensa��o realizada: "+AsString( aLogG8Y )+CRLF)
						If len( aLogG8Y ) > 0
							If TA42AddLog( oModelG8Y, aLogG8Y, oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
								lContinua := .T.
							Else
								DisarmTransaction()
								lErroLog  := .T.	
								lContinua := .F.
							EndIf
						EndIf
					Else
						LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Comppensa��o (TA042CompT)"+CRLF)
						DisarmTransaction()
						lContinua := .F.
					EndIf
				END TRANSACTION

				If !lContinua
					If lErroLog
						TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
					Else
						TurHelp(STR0184,"","TA042ACOMPT") //"Erro ao realizar compensa��o de t�tulos" 	
					EndIf
				EndIf	
			EndIf
		EndIf

		// Gera��o da fatura a pagar com os t�tulos gerados, somente se Gera NFE == 2-N�o
		If oModel:GetValue( "G8C_MASTER","G8C_DOCENT" ) <> '1'
			LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Titulos (TA042TitFT)"+CRLF)
			aTitulos := TA042TitFT( oModel, '2', oModel:GetValue('G8C_MASTER','G8C_CONCIL'),oModel:GetValue('G8C_MASTER','G8C_FORNEC'), oModel:GetValue('G8C_MASTER','G8C_LOJA') )
			LogConcilT(_nHandTXT,cFileG8C,2, "T�tulos Gerados - TA042TitFT "+AsString( aTitulos )+CRLF)
			If len(aTitulos[4]) > 0
				aLogG8Y := {}
				BEGIN TRANSACTION
					LogConcilT(_nHandTXT,cFileG8C,2, "Gera��o de Fatura (TA042GerFT)"+CRLF)
					If  TA042GerFT(oModel:GetValue( "G8C_MASTER", "G8C_SERIE" ) ,;
								   'FTF'										,;
								   AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")) ,;
								   oModel:GetValue( "G8C_MASTER", "G8C_NATUR" ) ,;
								   oModel:GetValue( "G8C_MASTER", "G8C_FORNEC") ,;
								   oModel:GetValue( "G8C_MASTER", "G8C_LOJA"  ) ,;
								   oModel:GetValue( "G8C_MASTER", "G8C_CONDPG") ,;
								   aTitulos[1]									,;
								   aTitulos[2]									,;
								   aTitulos[3]									,;
								   aTitulos[4] 									,;
								   aLogG8Y										,;
								   oModel)	
					
					Else
						DisarmTransaction()
						lContinua := .F.
					EndIf
				END TRANSACTION
				
				//Log FTF - Fora da transa��o, pois somente ap�s o t�tulo � apresentado na SE2
				If lContinua
					LogConcilT(_nHandTXT,cFileG8C,2, "Fatura - GetTitFTF "+CRLF)
					If GetTitFTF(oModel:GetValue( "G8C_MASTER", "G8C_SERIE" ),'FTF',AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")),oModel:GetValue( "G8C_MASTER", "G8C_FORNEC"),oModel:GetValue( "G8C_MASTER", "G8C_LOJA"  ),aLogG8Y)
						LogConcilT(_nHandTXT,cFileG8C,2, "Fatura GERADA "+AsString( aLogG8Y )+CRLF)
						For nY := 1 To Len(aLogG8Y)
							If !TA42AddLog( oModelG8Y, aLogG8Y[nY], oModel:GetValue( "G8C_MASTER", "G8C_CONCIL" ), AllTrim(oModel:GetValue( "G8C_MASTER", "G8C_FATURA")), '2' )
								lContinua	:= .F.
								lErroLog	:= .F.
								Exit
							EndIF
						Next nY
					Else
						LogConcilT(_nHandTXT,cFileG8C,2, "Erro - Gera��o de Fatura (TA042GerFT)"+CRLF)
						lContinua	:= .F.
						TurHelp(STR0150,"","GetTitFTF") //"Erro ao gerar o fatura a pagar, processo ser� estornado."	
					EndIf	
				EndIf
				
				If !lContinua
					If lErroLog
						TurHelp(STR0137, STR0138, 'TA42AG8YNFS')	//"Erro ao gravar o log."	//"Entre em contato com o administrador do sistema"
					Else
						TurHelp(STR0150,"","TA042FAT") //"Erro ao gerar o fatura a pagar, processo ser� estornado."
					EndIf
				EndIf	
			EndIf
		EndIf

		//Compensa��o de recebimento antecipado
		If lContinua
			LogConcilT(_nHandTXT,cFileG8C,2, "Compensa��o de recebimento antecipado (TA042ComRA)"+CRLF)
			lContinua := TA042ComRA( AllTrim(oModel:GetValue('G8C_MASTER','G8C_FATURA')), oModel:GetValue('G8C_MASTER','G8C_CONCIL'), oModel:GetValue('G8C_MASTER','G8C_FORNEC'), oModel:GetValue('G8C_MASTER','G8C_LOJA'), '2' )
		EndIf

		//Compensa��o de pagamento antecipado
		If lContinua
			LogConcilT(_nHandTXT,cFileG8C,2, "Compensa��o de pagamento antecipado (TA042ComPA)"+CRLF)
			lContinua := TA042ComPA( AllTrim(oModel:GetValue('G8C_MASTER','G8C_FATURA')), oModel:GetValue('G8C_MASTER','G8C_CONCIL'), oModel:GetValue('G8C_MASTER','G8C_FORNEC'), oModel:GetValue('G8C_MASTER','G8C_LOJA'), '2' )
		EndIf

		If lContinua
			For nX := 1 to oModelG3R:Length()
				If !lContinua
					Exit
				EndIf
					
				oModelG3R:GoLine( nX )
				
				If oModelG3R:GetValue( 'G3R_STATUS' ) $ '2|4' //Particionado|Cancelado
					Loop
				EndIf

				For nY := 1 to oModelG4CA:Length()
					oModelG4CA:GoLine( nY )

					//Se item financeiro de repasse, n�o libera neste momento, ser� liberado juntamente com o acordo de origem no fornecedor
					If oModelG4CA:GetValue( "G4C_STATUS") == "1" .And. oModelG4CA:GetValue( "G4C_CLASS" ) <> 'C07'
						//Se IF tem origem em acordo e n�o est� vinculado ao item principal, passa para Ag. Apura��o
						If !Empty( oModelG4CA:GetValue( "G4C_CLASS" ) ) .And. Empty( oModelG4CA:GetValue( "G4C_IFPRIN") )
							lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_STATUS", "2" )
							lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_TPCONC", "2" )
						Else
							lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_STATUS", "3" )
							lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_TPCONC", "2" )
							lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_DTLIB" , dDataBase )
							LogConcilT(_nHandTXT,cFileG8C,2, "Libera��o Ref oModelG4CA "+AsString( lContinua )+CRLF)
						EndIf
					EndIf
				Next

				For nY := 1 to oModelG4CB:Length()
					If !lContinua
						Exit
					EndIf	
					
					oModelG4CB:GoLine( nY )

					If oModelG4CB:GetValue( "G4C_STATUS") == "1" .And. Empty( oModelG4CB:GetValue( "G4C_CARTUR" ) )
						lContinua := lContinua .And. oModelG4CB:LoadValue( "G4C_STATUS", "4" )
						lContinua := lContinua .And. oModelG4CB:LoadValue( "G4C_TPCONC", "2" )

						//Verifica se o acordo de fornecedor � base para acordo de repasse do cliente, neste caso, libera o acordo
						If !Empty(oModelG4CB:GetValue( "G4C_NUMACD") ) .And. oModelG48A:SeekLine( { { "G48_ACDBAS", oModelG4CB:GetValue( "G4C_NUMACD") } } )
							For nZ := 1 to oModelG4CA:Length()
								oModelG4CA:GoLine( nZ )
								If oModelG4CA:GetValue( "G4C_NUMACD" ) ==  oModelG48A:GetValue( "G48_CODACD" )
									lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_STATUS", TA39UpdRepas(oModelG4CA) )
									lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_TPCONC", "2" )
									lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_DTLIB" , dDataBase )
									LogConcilT(_nHandTXT,cFileG8C,2, "Libera��o Ref oModelG4CB "+AsString( lContinua )+CRLF)
								EndIf
							Next
						EndIf

						//Finaliza o acordo que originou o item financeiro
						If ( !oModelG48B:IsEmpty() )
							For nZ := 1 to oModelG48B:Length()
								oModelG48B:GoLine( nZ )
								If !oModelG48B:IsDeleted() .And. !Empty(oModelG48B:GetValue( 'G48_APLICA' )) .And. oModelG48B:GetValue( 'G48_APLICA' ) == oModelG4CB:GetValue( "G4C_APLICA")
									lContinua := lContinua .And. oModelG48B:LoadValue( "G48_CODCON", oModel:GetValue('G8C_MASTER','G8C_CONCIL') )
									lContinua := lContinua .And. oModelG48B:LoadValue( "G48_STATUS", "4" )
								EndIf
							Next
						EndIf
					EndIf
				Next
			Next
		EndIf

		If lContinua
			LogConcilT(_nHandTXT,cFileG8C,2, "Atualiza��o de Status"+CRLF)
			//Altera o status da concilia��o para efetivado
			aValues := {}
			aAdd(aValues, {"G8C_STATUS", "1"})

			lContinua := TA042SetVl(oModel,"G8C_MASTER",aValues )
			If lContinua .And. oModel:VldData()
				If (lRet := oModel:CommitData())
					For nX := 1 to oModelG3R:Length()
						TurAtuStRV( oModelG3R:GetValue('G3R_FILIAL', nX), oModelG3R:GetValue('G3R_NUMID', nX), oModelG3R:GetValue('G3R_IDITEM', nX), oModelG3R:GetValue('G3R_NUMSEQ', nX), oModelG3R:GetValue('G3R_CONINU', nX), "3", .T. )
					Next nX
					LogConcilT(_nHandTXT,cFileG8C,2, "Finaliza��o da Concilia��o"+CRLF)
				Else
					LogConcilT(_nHandTXT,cFileG8C,2, "Erro no CommitData()"+ AsString(oModel:GetErrorMessage()) +CRLF)	
				EndIf
			Else
				JurShowErro( oModel:GetErrorMessage() )
				lContinua	:= .F.
			EndIf
		EndIf

		If lContinua
			If lOnLine
				LogConcilT(_nHandTXT,cFileG8C,2, "Inicio da Contabiliza��o -  T042ACtInc:"+Dtos(Date())+Time()+CRLF)
				T042ACtInc(lOnLine,lMostrLanc,lAglutLanc,cModelEfet)
				LogConcilT(_nHandTXT,cFileG8C,2, "Fim da Contabiliza��o -  T042ACtInc:"+Dtos(Date())+Time()+CRLF)
			EndIf

			If IsBlind() .Or. ApMsgYesNo(STR0160)		// "Deseja imprimir os Demonstrativos de Cliente e Ag�ncia?"
				FwMsgRun( , {|| TA042DemFi(2, .T.)}, , STR0163) // "Imprimindo o Demonstrativo Cliente, aguarde..."		nTipo : (1=Agencia | 2=Cliente)
				FwMsgRun( , {|| TA042DemFi(1, .T.)}, , STR0164) // "Imprimindo o Demonstrativo Ag�ncia, aguarde..."		nTipo : (1=Agencia | 2=Cliente)
			EndIf
		Else
			If !TA042ADEfe( cModelEfet )
				TurHelp( STR0185, STR0186, 'TA42IERREST' ) //'Erro ao estornar a efetiva��o'###'Entre em contato com o administrador do sistema'
			EndIf
		EndIf

		LogConcilT( _nHandTXT, cFileG8C, 3 )

		//Destrava a concilia��o
		UnLockByName( xFilial('G8C') + G8C->G8C_CONCIL, .T., .T. ) 
	
		oModel:DeActivate()
		oModel:Destroy()

		While InTransact()

			msgalert("Transa��o Presa... " + G8C->G8C_CONCIL)

			aStack := GetTTSInUse()
			If !Empty(aStack)
				cStackMsg += ' Stack:'
			EndIf
			For nIt:=1 To Len(aStack)
				cStackMsg += aStack[nIt]+'() '+CRLF
			Next nIt

			Final("Transa��o Presa... " + G8C->G8C_CONCIL +" - "+cStackMsg)
		EndDo

	Endif

	RestArea(aAreaSA2)
	RestArea(aAreaSED)
	RestArea(aAreaSE2)
	RestArea(aAreaSF1)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSF1)
	TURXNIL(aAreaSE2)
	TURXNIL(aAreaSED)
	TURXNIL(aAreaSA2)
	TURXNIL(aValues)
	TURXNIL(aFornPrd)
	TURXNIL(aItensNFS)
	TURXNIL(aDadosTit)
	TURXNIL(aDadosTitP)
	TURXNIL(aDadosTitR)
	TURXNIL(aValAbatP)
	TURXNIL(aValAbatR)
	TURXNIL(aTitulos)
	TURXNIL(aLogG8Y)
	TURXNIL(aDados)
	TURXNIL(aParc)
	TURXNIL(aStack)
Return lContinua

/*/{Protheus.doc} TA042CaVrF
Fun��o para calculo do total da fatura com base nos itens financeiros
@type function
@author Anderson Toledo
@since 03/05/2016
/*/
Static Function TA042CaVrF(cModelEfet)
	Local nValFatura  := 0
	Local oModel	  := FwLoadModel( 'TURA042D' )
	Local nSignal  	  := IIF( cModelEfet == 'TURA042A', 1, -1 )

	oModel:SetOperation( MODEL_OPERATION_UPDATE  )
	If oModel:Activate()
		TA042DVal( , , @nValFatura )
		nValFatura := nValFatura * nSignal
		oModel:DeActivate()
	EndIf
	
Return nValFatura

/*/{Protheus.doc} TA042ADEfe
Fun��o para desfazer a efetiva��o da concilia��o terrestre
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param cOpc, character, Indica o tipo de opera��o a ser execudo: "1" - Efetiva��o da concilia��o; "2" - Estorno da efetiva��o
/*/
Function TA042ADEfe( cModelEfet )
	Local aArea         := GetArea()
	Local aAreaG8Y      := G8Y->(GetArea())
	Local aValues		:= {}
	Local aFornPrd		:= {}
	Local aItensNFS 	:= {}
	Local aDadosTit		:= {}
	Local aDadosTitP	:= {}
	Local aDadosTitR	:= {}
	Local aValAbatP		:= {}
	Local aValAbatR		:= {}
	Local aTitulos		:= {}
	Local aLogG8Y		:= {}
	Local lContinua		:= .T.
	Local lConfirmNFS 	:= .F.
	Local nValFatura	:= 0
	Local nTitAvulso	:= 0
	Local oModel		:= nil
	Local oModelG3R		:= nil
	Local oModelG8Y		:= nil
	Local oModelG48A	:= nil
	Local oModelG48B	:= nil
	Local oModelG4CA	:= nil
	Local oModelG4CB	:= nil
	Local cErro			:= ""
	Local aDados		:= {}
	Local aParc			:= {}
	Local cNumNFS		:= ""
	Local cSerNFS		:= ""
	Local cNatTit		:= ""
	Local nX			:= 0
	Local nY			:= 0
	Local nZ			:= 0
	Local nIt			:= 0
	Local nFilRef		:= 0
	Local nPosSerie		:= 0
	Local nPosNatur		:= 0
	Local cPerg			:= ""

	Local lOnLine		:= .T.
	Local lMostrLanc	:= .T.
	Local lAglutLanc	:= .F.

	Local cFilAntBkp	:= cFilAnt
	Local cChave		:= ""

	Private cNumero		:= ""

	DEFAULT cModelEfet 	:= "TURA042A"

	If cModelEfet == "TURA042A"
		cPerg	:= "TURA042A"
	ElseIf 	cModelEfet == "TURA042R"
		cPerg	:= "TURA042R"
	EndIf

	Pergunte(cPerg,.F.)

	lOnLine 	:= Iif(mv_par01 == 1,.T.,.F.)
	lMostrLanc	:= Iif(mv_par02 == 1,.T.,.F.)
	lAglutLanc	:= Iif(mv_par03 == 1,.T.,.F.)

	If 	TA42IfCliF( G8C->G8C_FILIAL, G8C->G8C_CONCIL )
		FwAlertHelp(STR0147, STR0148, 'TA042AIFCLI')	//"Existem itens financeiros de cliente liberados pela concilia��o com status de finalizado."	//"Estorne o faturamento/apura��o de clientes para efetuar esta opera��o."
		Return .F.
	EndIf

	oModel := FwLoadModel( cModelEfet )
	oModel:SetOperation( MODEL_OPERATION_UPDATE )
	oModel:Activate()

	oModelG3R	:= oModel:GetModel( "G3R_ITENS" )
	oModelG4CA	:= oModel:GetModel( "G4CA_ITENS" )
	oModelG4CB	:= oModel:GetModel( "G4CB_ITENS" )
	oModelG48A	:= oModel:GetModel( "G48A_ITENS" )
	oModelG48B	:= oModel:GetModel( "G48B_ITENS" )

	oModelG8Y := FwLoadModel( "TURA042B" )
	oModelG8Y:SetOperation( MODEL_OPERATION_DELETE )

	//Estorno da fatura a pagar
	If lContinua
		lContinua := TA042EsFat(AllTrim(G8C->G8C_FATURA), G8C->G8C_SERIE, G8C->G8C_FORNEC, G8C->G8C_LOJA )
	EndIf

	//Estorno das compensa��es de RA/PA
	If lContinua
		lContinua := TA042EsCom(AllTrim(G8C->G8C_FATURA), G8C->G8C_FORNEC, G8C->G8C_LOJA, '2' )
	EndIf

	If lContinua
		lContinua := TA042CmpCa( G8C->G8C_CONCIL, G8C->G8C_FORNEC, G8C->G8C_LOJA, '2'  )
	EndIf

	//Estorno das transfer�ncias dos t�tulos
	If lContinua
		G8Y->( dbSetOrder(3) )
		G8Y->( dbSeek( xFilial("G8Y") + "2" + G8C->G8C_CONCIL ) )

		While G8Y->( !EOF() ) .And. G8Y->( G8Y_FILIAL+G8Y_TPFAT+G8Y_CONCIL ) == xFilial("G8Y") + "2" + G8C->G8C_CONCIL
			If G8Y->G8Y_TPDOC == "5" //T�tulo a receber transferido
				lContinua := TA042EstTr( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_PARCEL, G8Y->G8Y_TIPO, G8Y->G8Y_FILREF  )

				If !lContinua
					Exit
				EndIf
			EndIf
			G8Y->( dbSkip() )
		EndDo
	EndIf

	G8Y->( dbSeek( xFilial("G8Y") + "2" + G8C->G8C_CONCIL ) )
	While lContinua .And. G8Y->(!EOF()) .And.  G8Y->( G8Y_FILIAL+G8Y_TPFAT+G8Y_CONCIL ) == xFilial("G8Y") + "2" + G8C->G8C_CONCIL

		If G8Y->G8Y_TPDOC == "1" //Contas a Receber
			If TA042DeTiR( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_TIPO, G8Y->G8Y_PARCEL, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
			Else
				MostraErro()
				lContinua := .F.
			EndIf

		ElseIf G8Y->G8Y_TPDOC == "2" //Contas a pagar
			If TA042DeTiP( G8Y->G8Y_PREFIX, G8Y->G8Y_NUM, G8Y->G8Y_TIPO, G8Y->G8Y_PARCEL, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
			Else
				MostraErro()
				lContinua := .F.
			EndIf

		ElseIf G8Y->G8Y_TPDOC == "3" //Nota fiscal de entrada
			If TA042DeNFe( G8Y->G8Y_DOC, G8Y->G8Y_SERIE, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()

			Else
				MostraErro()
				lContinua := .F.
			EndIf

		ElseIf G8Y->G8Y_TPDOC == "4" //Nota fiscal de saida
			If TA042DeNFS( G8Y->G8Y_DOC, G8Y->G8Y_SERIE, G8Y->G8Y_FORNEC, G8Y->G8Y_LOJA, G8Y->G8Y_FILREF )
				oModelG8Y:Activate()
				If oModelG8Y:VldData()
					lContinua := .T.
					oModelG8Y:CommitData()
				EndIf
				oModelG8Y:DeActivate()
			Else
				lContinua := .F.
			EndIf
		
		Else
			oModelG8Y:Activate()
			If oModelG8Y:VldData()
				lContinua := .T.
				oModelG8Y:CommitData()
			EndIf
			oModelG8Y:DeActivate()
		EndIf

		(G8Y->( dbSkip() ))
	EndDo

	//Retorna o Model Ativo para o modelo da concilia��o
	FwModelActive( oModel )

	If lContinua
		For nX := 1 to oModelG3R:Length()
			oModelG3R:GoLine( nX )

			If oModelG3R:GetValue( 'G3R_STATUS' ) $ '2|4' //Particionado|Cancelado
				Loop
			EndIf

			oModelG3R:LoadValue("G3R_STATUS","1")	//Pendente

			For nY := 1 to oModelG4CA:Length()
				oModelG4CA:GoLine( nY )

				//Se item financeiro de repasse, ser� estornada a libera��o juntamente com o acordo de origem no fornecedor
				If oModelG4CA:GetValue( "G4C_STATUS")$ "2|3" .And. oModelG4CA:GetValue( "G4C_TPCONC" ) == "2" .And. oModelG4CA:GetValue( "G4C_CLASS" ) <> 'C07'
					lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_STATUS", "1" )
					lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_TPCONC", "" )
					lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_DTLIB" , ctod( " / / " ) )
				EndIf
			Next

			For nY := 1 to oModelG4CB:Length()
				oModelG4CB:GoLine( nY )
				If oModelG4CB:GetValue( "G4C_STATUS" ) == "4" .And. oModelG4CB:GetValue( "G4C_TPCONC" ) == "2"
					lContinua := lContinua .And. oModelG4CB:LoadValue( "G4C_STATUS", "1")
					lContinua := lContinua .And. oModelG4CB:LoadValue( "G4C_TPCONC", "" )

					//Verifica se o acordo de fornecedor � base para acordo de repasse do cliente, neste caso, libera o acordo
					If !Empty(oModelG4CB:GetValue( "G4C_NUMACD") ) .And. oModelG48A:SeekLine( { { "G48_ACDBAS", oModelG4CB:GetValue( "G4C_NUMACD") } } )
						For nZ := 1 to oModelG4CA:Length()
							oModelG4CA:GoLine( nZ )
							If oModelG4CA:GetValue( "G4C_NUMACD" ) ==  oModelG48A:GetValue( "G48_CODACD" )
								lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_STATUS", "1" )
								lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_TPCONC", "" )
								lContinua := lContinua .And. oModelG4CA:LoadValue( "G4C_DTLIB" , ctod( " / / " ) )
							EndIf
						Next
					EndIf

					If ( !oModelG48B:IsEmpty() )

						//Finaliza o acordo que originou o item financeiro
						For nZ := 1 to oModelG48B:Length()
							oModelG48B:GoLine( nZ )
							If !oModelG48B:IsDeleted() .And. oModelG48B:GetValue( 'G48_APLICA' ) == oModelG4CB:GetValue( "G4C_APLICA")
								lContinua := lContinua .And. oModelG48B:LoadValue( "G48_CODCON", "" )
								lContinua := lContinua .And. oModelG48B:LoadValue( "G48_STATUS", "1" )
							EndIf
						Next
					Endif
				EndIf
			Next
		Next
	EndIf

	If lContinua
		aAdd(aValues, {"G8C_STATUS", "2"		})
		aAdd(aValues, {"G8C_DTEFET", ctod(" / / ")	})
		aAdd(aValues, {"G8C_FILEFE", ""	})

		lContinua := TA042SetVl(oModel,"G8C_MASTER",aValues )

		If lContinua .And. oModel:VldData()
			oModel:CommitData()
			lContinua	:= .T.
		Else
			lContinua	:= .F.
		EndIf
	EndIf

	If lContinua
		T042ACtExc(.T.,lMostrLanc,lAglutLanc,cModelEfet)
	EndIf
	oModel:DeActivate()

Return lContinua

/*/{Protheus.doc} TA042EfNFE
Fun��o para gera��o da nota fiscal de entrada
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto do ModelDef instanciado
@param nValAbat, numerico, residuo do valor de abatimento de receita que ser� adicionado ao valor da nota fiscal
/*/
Function TA042EfNFE( oModel, aItensNF, aLog )
	Local aArea     := GetArea()
	Local aAreaSF1  := SF1->(GetArea())
	Local aLines	:= FwSaveRows()
	Local aCabec 	:= {}
	Local aLinha 	:= {}
	Local aItens 	:= {}
	Local lRet 		:= .T.
	Local nX		:= 0
	Local nY		:= 0

	Local oModelG3R		:= oModel:GetModel( "G3R_ITENS" )
	Local oModelG4CB	:= oModel:GetModel( "G4CB_ITENS" )
	Local oModelG48B	:= oModel:GetModel( "G48B_ITENS" )

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.

	If Empty( oModel:GetValue("G8C_MASTER","G8C_NUMDOC" ) ) .Or. Empty( oModel:GetValue("G8C_MASTER","G8C_SERIE" ) ) .Or. Empty( oModel:GetValue("G8C_MASTER","G8C_CONDPG" ) )
		lRet := .F.
		Help(,,"TA042EFETIVA",,STR0079,1,0)			 //"Informe o n� do documento de entrada e sua s�rie para efetiva��o da concilia��o"
	Else
		aAdd( aCabec, {"F1_FILIAL" 	, xFilial( "SF1" )							 , Nil 	} )
		aAdd( aCabec, {"F1_TIPO"   	, "N"										 , Nil 	} )
		aAdd( aCabec, {"F1_FORMUL" 	, "N"										 , Nil 	} )
		aAdd( aCabec, {"F1_DOC"    	, oModel:GetValue("G8C_MASTER", "G8C_NUMDOC"), Nil 	} )
		aAdd( aCabec, {"F1_SERIE"  	, oModel:GetValue("G8C_MASTER", "G8C_SERIE") , Nil 	} )
		aAdd( aCabec, {"F1_EMISSAO"	, oModel:GetValue("G8C_MASTER", "G8C_EMISSA"), Nil 	} )
		aAdd( aCabec, {"F1_FORNECE"	, oModel:GetValue("G8C_MASTER", "G8C_FORNEC"), Nil 	} )
		aAdd( aCabec, {"F1_LOJA"   	, oModel:GetValue("G8C_MASTER", "G8C_LOJA")  , Nil 	} )
		aAdd( aCabec, {"F1_EST"	 	, Posicione( "SA2", 1, xFilial("SA2") + oModel:GetValue("G8C_MASTER", "G8C_FORNEC") + oModel:GetValue("G8C_MASTER", "G8C_LOJA"), "A2_EST"  ) , Nil 	} )
		aAdd( aCabec, {"F1_ESPECIE"	, "NFE"										 , Nil 	} )
		aAdd( aCabec, {"F1_COND"	, oModel:GetValue("G8C_MASTER", "G8C_CONDPG"), Nil 	} )
		aAdd( aCabec, {"F1_MOEDA"	, aItensNF[4], Nil 	} )
		aAdd( aCabec, {"E2_NATUREZ"	, aItensNF[2], Nil 	} )

		For nX	:= 1 to Len( aItensNF[5] )
			aLinha := {}
			aAdd(aLinha,{"D1_COD"  	, aItensNF[5][nX][1], Nil })
			aAdd(aLinha,{"D1_QUANT"	, 1					, Nil })
			aAdd(aLinha,{"D1_VUNIT"	, aItensNF[5][nX][3], Nil })
			aAdd(aLinha,{"D1_TOTAL"	, aItensNF[5][nX][3], Nil })
			aAdd(aLinha,{"D1_TES"	, aItensNF[5][nX][2], Nil })
			aAdd(aItens,aLinha)
		Next

		MSExecAuto({|x,y,z| mata103(x,y,z)},aCabec,aItens,3)
	EndIf

	If lMsErroAuto
		MostraErro()
		lRet := .F.
	Else
		SF1->(dbSetOrder(1))
		If SF1->( dbSeek( xFilial('SF1') + oModel:GetValue( "G8C_MASTER", "G8C_NUMDOC") + oModel:GetValue( "G8C_MASTER", "G8C_SERIE" ) + oModel:GetValue( "G8C_MASTER", "G8C_FORNEC") + oModel:GetValue( "G8C_MASTER", "G8C_LOJA") ) )
			aAdd( aLog, { "G8Y_TPDOC"	, "3"											} )
			aAdd( aLog, { "G8Y_DOC"		, oModel:GetValue( "G8C_MASTER", "G8C_NUMDOC")	} )
			aAdd( aLog, { "G8Y_SERIE"	, oModel:GetValue( "G8C_MASTER", "G8C_SERIE" )	} )
			aAdd( aLog, { "G8Y_FORNEC"	, oModel:GetValue( "G8C_MASTER", "G8C_FORNEC")	} )
			aAdd( aLog, { "G8Y_LOJA"	, oModel:GetValue( "G8C_MASTER", "G8C_LOJA")	} )
			aAdd( aLog, { "G8Y_TIPO"	, "NF"											} )
			aAdd( aLog, { "G8Y_IDDOC"	, "" /*FINGRVFK7()*/ 							} )
			aAdd( aLog, { "G8Y_VALOR"	, SF1->F1_VALBRUT 								} )
		Else
			Help(,,"TA042ATITP",,i18N(STR0122,{ oModel:GetValue( "G8C_MASTER", "G8C_NUMDOC"), oModel:GetValue( "G8C_MASTER", "G8C_SERIE" ) }),1,0) //"Nota Fiscal de entrada #1[ Numero Doc ]# S�rie #2[ Serie ]# n�o localizado."
			lRet := .F.
		EndIf
	EndIf

	FwRestRows(aLines)
	RestArea(aAreaSF1)
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAreaSF1)
	TURXNIL(aLines)
	TURXNIL(aCabec)
	TURXNIL(aLinha)
	TURXNIL(aItens)
Return	lRet

/*/{Protheus.doc} TA042DeNFe
Fun��o para estorno do documento de entrada gerado na efetiva��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042DeNFe( cDoc, cSerie, cFornece, cLoja )
	Local aCabec 	:= {}
	Local lRet		:= .T.

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.

	aAdd( aCabec,	{"F1_FILIAL" 	, xFilial( "SF1" ), Nil 	} )
	aAdd( aCabec,	{"F1_TIPO"   	, "N"			  , Nil 	} )
	aAdd( aCabec,	{"F1_FORMUL" 	, "N"			  , Nil 	} )
	aAdd( aCabec,	{"F1_DOC"    	, cDoc			  , Nil 	} )
	aAdd( aCabec,	{"F1_SERIE"  	, cSerie 		  , Nil 	} )
	aAdd( aCabec,	{"F1_FORNECE"	, cFornece		  , Nil 	} )
	aAdd( aCabec,	{"F1_LOJA"   	, cLoja  		  , Nil 	} )
	aAdd( aCabec,	{"F1_EST"	 	, Posicione( "SA2", 1, xFilial("SA2") + cFornece + cLoja, "A2_EST"  ) , Nil 	} )
	aAdd( aCabec,	{"F1_ESPECIE"	, "NFE"			  , Nil 	} )

	MSExecAuto({|x,y,z| mata103(x,y,z)},aCabec,{},5)
	If lMsErroAuto
		MostraErro()
		lRet := .F.
	EndIf

	TURXNIL(aCabec)
Return	lRet

/*/{Protheus.doc} TA042EfTiP
Fun��o para gera��o de t�tulos a pagar na efetiva��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042EfTiP( cNumDoc, cSerie, cFornece, cLoja, cNatTit, nValor , dEmissa, cCondPgto, nMoeda, dVencto, aLog )
	Local aArea		:= GetArea()
	Local aCond		:= {}
	Local aArray	:= {}
	Local lRet		:= .T.
	Local nY		:= 0
	Local nTamParc	:= TamSX3('E2_PARCELA')[1] 
	Local cParc		:= PADL(Alltrim(SuperGetMv("MV_1DUP")),nTamParc,"0")
	Local nI		:= 0
	Local nDia		:= Day(dVencto)
	Local nNovoDia	:= 0
	Local nTamLog	:= 0
	Local dDtValid	:= StoD("")

	Private lMsErroAuto := .F.

	DEFAULT cNumDoc := ""

	If Empty( cNumDoc )
		cNumDoc := TA042NumTi( 'SE2', cSerie )
	EndIf

	SE4->( DbSetOrder( 1 ) )
	SE4->( DbSeek( xFilial('SE4')+ cCondPgto ) )

	If SE4->E4_TIPO == '9'
		G66->( dbSetOrder(1) )
		If G66->( dbSeek( xFilial( 'G66' ) + cCondPgto ) )
			aCond := TA009Cond( cCondPgto, nValor )
		Else
			Help(,,"TA042EFTIPCP",,STR0105,1,0)			 //"Condi��o de pagamento do Tipo 9 sem o cadastro de complemento."
			Return .F.
		EndIf

	ElseIf ( SE4->E4_TIPO == Alltrim(GetMv("MV_TURTPCD",,"C")) ) //Par�metro do Tipo de Condi��o de Pagamento

		If FWIsInCallStack("TA039EFET")
			aCond := Condicao( nValor, cCondPgto  ) // Concilia��o Aerea
		Else
			aCond := TA042Vencto(nValor, cCondPgto) // Concilia�ao Terrestre
		EndIf

		For nI := 1 to Len(aCond)

			dDtValid := SToD(cValToChar(Year(aCond[nI,1])) + StrZero(Month(aCond[nI,1]),2) + StrZero(nDia,2))
			//Checa se dia existe no m�s que est� sendo lido. Se n�o existir, ser� pego o �ltimo dia existente do m�s.
			If ( Empty(dDtValid) .Or. DataValida(dDtValid) == dDataBase )

				If ( nDia == 31 .And. nDia <> Day(LastDay(aCond[nI,1])) .Or. ( nDia >= 29 .And. Month(aCond[nI,1]) == 2 ) )
					nNovoDia :=  Day(LastDay(aCond[nI,1]))
					dDtValid := SToD(cValToChar(Year(aCond[nI,1])) + StrZero(Month(aCond[nI,1]),2) + StrZero(nNovoDia,2))
				Endif

			Endif

			aCond[nI,1] := dDtValid
		Next nI
	Else
		aCond := Condicao( nValor, cCondPgto  )
	EndIf

	For nY := 1 to len( aCond )
		aArray := {}
		aAdd(aArray,{ "E2_PREFIXO" 	, cSerie  	, NIL } )
		aAdd(aArray,{ "E2_NUM" 		, cNumDoc 	, NIL } )
		aAdd(aArray,{ "E2_TIPO" 	, "NF" 		, NIL } )

		If Len( aCond ) > 1
			cParc := Soma1( cParc, nTamParc, .T. )
			aAdd( aArray, { "E2_PARCELA" 	, cParc	, NIL } )
		Else
			cParc := Space(nTamParc)
		EndIf

		If !Empty( cNatTit )
			aAdd(aArray,{ "E2_NATUREZ" , cNatTit	, NIL } )
		EndIf

		aAdd( aArray, {"E2_FORNECE"	, cFornece					, Nil	} )
		aAdd( aArray, {"E2_LOJA"   	, cLoja  					, Nil 	} )
		aAdd( aArray, {"E2_EMISSAO"	, dEmissa					, Nil 	} )
		aAdd( aArray, {"E2_VENCTO" 	, aCond[nY][1]				, NIL 	} )
		aAdd( aArray, {"E2_VENCREA" , DataValida(aCond[nY][1])	, NIL 	} )
		aAdd( aArray, {"E2_MOEDA" 	, nMoeda					, NIL 	} )
		aAdd( aArray, {"E2_VALOR" 	, aCond[nY][2]				, NIL 	} )
		aAdd( aArray, {"E2_ORIGEM" 	, 'TURA042A'				, NIL 	} )

		MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aArray,, 3)
		If lMsErroAuto
			lRet := .F.
			MostraErro()
			Exit
		Else
			SE2->( dbSetOrder(1) )
			If SE2->( dbSeek( xFilial('SE2') + cSerie + cNumDoc + cParc + "NF " + cFornece + cLoja ) )
				//Criar o vetor com o log da opera��o
				aAdd( aLog, {} )
				nTamLog := len( aLog )

				aAdd( aLog[nTamLog], { "G8Y_FILREF"	, xFilial( 'SE2' )	} )
				aAdd( aLog[nTamLog], { "G8Y_TPDOC"	, "2"			}	)
				aAdd( aLog[nTamLog], { "G8Y_DOC"	, cNumDoc		}	)
				aAdd( aLog[nTamLog], { "G8Y_NUM"	, cNumDoc		}	)
				aAdd( aLog[nTamLog], { "G8Y_SERIE"	, cSerie 		}	)
				aAdd( aLog[nTamLog], { "G8Y_PREFIX"	, cSerie		}	)
				aAdd( aLog[nTamLog], { "G8Y_PARCEL"	, cParc			}	)
				aAdd( aLog[nTamLog], { "G8Y_FORNEC"	, cFornece		}	)
				aAdd( aLog[nTamLog], { "G8Y_LOJA"	, cLoja			}	)
				aAdd( aLog[nTamLog], { "G8Y_TIPO"	, "NF "			}	)
				aAdd( aLog[nTamLog], { "G8Y_IDDOC"	, FINGRVFK7('SE2',&(TurKeyTitulo('SE2'))) 		}	)
				aAdd( aLog[nTamLog], { "G8Y_VALOR"	, SE2->E2_VALOR									}	)
			Else
				Help(,,"TA042ATITP",,i18N(STR0141,{ cNumDoc, cSerie }),1,0) //"T�tulo a pagar #1[ Numero T�tulo ]# Prefixo #2[ Prefixo ]# n�o localizado."
				lRet := .F.
				Exit
			EndIf
		EndIf
	Next

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aCond)
	TURXNIL(aArray)
Return lRet

/*/{Protheus.doc} TA042DeTiP
Fun��o para estorno de t�tulos a pagar gerados na efetiva��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042DeTiP( cPrefixo, cNum, cTipo, cParc, cFornece, cLoja, cFilRef )
	Local aArray  := {}
	Local cBkpFil := cFilAnt

	DEFAULT cFilRef := cFilAnt

	cFilAnt := cFilRef

	Private lMsErroAuto := .F.

	aAdd(aArray,{ "E2_PREFIXO" 	, cPrefixo  , NIL } )
	aAdd(aArray,{ "E2_NUM" 		, cNum 		, NIL } )
	aAdd(aArray,{ "E2_TIPO" 	, cTipo 	, NIL } )
	aAdd(aArray,{ "E2_PARCELA" 	, cParc		, NIL } )
	aAdd(aArray,{ "E2_FORNECE"	, cFornece	, NIL } )
	aAdd(aArray,{ "E2_LOJA"   	, cLoja  	, NIL } )

	MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aArray,, 5)

	cFilAnt := cBkpFil
	TURXNIL(aArray)
Return !lMsErroAuto

Function TA042EfTiR( cFornRep, cLojaRep, cNatTit, nVlrTit, aLog )
	Local aArea     := GetArea()
	Local aArray 	:= {}
	Local aCond 	:= {}
	Local aDadosTit	:= {}
	Local cNum		:= ""
	Local cCond		:= SuperGetMV("MV_TURCCP",,"")
	Local nTamParc	:= TamSX3('E1_PARCELA')[1] 
	Local cParc		:= PADL(Alltrim(SuperGetMv("MV_1DUP")),nTamParc,"0")
	Local lRet		:= .T.
	Local nX		:= 0
	Local nY 		:= 0

	Private lMsErroAuto := .F.

	SA2->( dbSetOrder(1) )
	If !SA2->( dbSeek( xFilial("SA2") + cFornRep + cLojaRep ) ) .And. !Empty( SA2->A2_CLIENTE ) .And. !Empty( SA2->A2_LOJCLI )
		Help(,,"TA042EFETIVA",,STR0142,1,0) //"C�digo de cliente n�o informado no cadastro do fornecedor"
		Return .F.
	EndIf

	If Empty( cCond )
		Help(,,"TA042EFETIVA2",,STR0080,1,0)			 //"Solicite ao administrador do sistema o cadastro do par�metro 'MV_TURCCP', com a condi��o de pagamento para a nota fiscal de sa�da."
		Return .F.
	EndIf

	cNum	:= GetSXENum("SE1","E1_NUM","E1_TUR_CR")

	SE1->( dbSetOrder(1) )
	If SE1->( dbSeek( xFilial("SE1") + PADR( "NF", TamSX3("E1_PREFIXO")[1]  ) + cNum ) )
		cNum	:= GetSXENum("SE1","E1_NUM","E1_TUR_CR")
	EndIf

	SE4->( DbSetOrder( 1 ) )
	SE4->( DbSeek( xFilial('SE4')+ cCond ) )
	If SE4->E4_TIPO == '9'
		G66->( dbSetOrder(1) )
		If G66->( dbSeek( xFilial( 'G66' ) + cCond ) )
			aCond := TA009Cond( cCond, nVlrTit )
		Else
			Help(,,"TA042EFTIPCP",,STR0105,1,0)			 //"Condi��o de pagamento do Tipo 9 sem o cadastro de complemento."
			Return .F.
		EndIf
	Else
		aCond := Condicao( nVlrTit, cCond  )
	EndIf

	For nY := 1 to len( aCond )
		aArray := {}
		aAdd( aArray, { "E1_PREFIXO", "NF" 			 , NIL } )
		aAdd( aArray, { "E1_NUM" 	, cNum			 , NIL } )
		aAdd( aArray, { "E1_TIPO" 	, "NF" 			 , NIL } )
		aAdd( aArray, { "E1_NATUREZ", cNatTit 		 , NIL } )
		aAdd( aArray, { "E1_CLIENTE", SA2->A2_CLIENTE, NIL } )
		aAdd( aArray, { "E1_LOJA"	, SA2->A2_LOJCLI , NIL } )

		If len( aCond ) > 1
			cParc := Soma1( cParc, nTamParc, .T. )
			aAdd( aDadosTit, { cNum, cParc, "NF" } )
			aAdd( aArray, { "E1_PARCELA" , cParc 	, NIL } )
		Else
			aAdd( aDadosTit, { cNum,Space(nTamParc), "NF " } )
		EndIf

		aAdd( aArray, { "E1_EMISSAO", dDataBase		, NIL } )
		aAdd( aArray, { "E1_VENCTO"	, aCond[nY][1]	, NIL } )
		aAdd( aArray, { "E1_VENCREA", aCond[nY][1]	, NIL } )
		aAdd( aArray, { "E1_VALOR" 	, aCond[nY][2]	, NIL } )
		aAdd( aArray, { "E1_ORIGEM"	, 'TURA042A' 	, NIL } )

		MsExecAuto( { |x,y| FINA040(x,y)} , aArray, 3) // 3-Inclusao,4-Altera��o,5-Exclus�o
		If lMsErroAuto
			lRet := .F.
			RollBackSX8()
			MostraErro()
			Exit
		Else
			ConfirmSX8()
		EndIf
	Next

	//Verifica se todos os t�tulos foram gerados corretamente e cria array para grava��o do log
	If lRet
		SE1->( dbSetOrder(1) )
		For nX := 1 to len(aDadosTit)
			If SE1->( dbSeek( xFilial('SE1') + aDadosTit[nX][3] + aDadosTit[nX][1] + aDadosTit[nX][2] + aDadosTit[nX][3] ) )
				aAdd( aLog, {} )
				nTamLog := Len( aLog )

				aAdd( aLog[nTamLog], { "G8Y_FILREF"	, xFilial( 'SE1' )	} )
				aAdd( aLog[nTamLog], { "G8Y_TPDOC"	, "1"				} )
				aAdd( aLog[nTamLog], { "G8Y_DOC"	, SE1->E1_NUM		} )
				aAdd( aLog[nTamLog], { "G8Y_NUM"	, SE1->E1_NUM		} )
				aAdd( aLog[nTamLog], { "G8Y_SERIE"	, ""				} )
				aAdd( aLog[nTamLog], { "G8Y_PREFIX"	, SE1->E1_PREFIXO	} )
				aAdd( aLog[nTamLog], { "G8Y_PARCEL"	, SE1->E1_PARCELA	} )
				aAdd( aLog[nTamLog], { "G8Y_FORNEC"	, cFornRep			} )
				aAdd( aLog[nTamLog], { "G8Y_LOJA"	, cLojaRep 			} )
				aAdd( aLog[nTamLog], { "G8Y_TIPO"	, SE1->E1_TIPO		} )
				aAdd( aLog[nTamLog], { "G8Y_VALOR"	, SE1->E1_VALOR		} )
				aAdd( aLog[nTamLog], { "G8Y_IDDOC"	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1'))) } )
			Else
				Help(,,"TA039TITR",,i18N(STR0143,{ aDadosTit[nX][1], aDadosTit[nX][3] }),1,0) //"T�tulo a receber #1[ Numero T�tulo ]# Prefixo #2[ Prefixo ]# n�o localizado."
				lRet := .F.
				Exit
			EndIf
		Next
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aArray)
	TURXNIL(aCond)
	TURXNIL(aDadosTit)
Return lRet

Function TA042DeTiR( cPrefixo, cNum, cTipo, cParc, cCliente, cLoja, cFilRef )
	Local aArea     := GetArea()
	Local aArray 	:= {}
	Local cBkpFil	:= cFilAnt

	DEFAULT cFilRef := cFilAnt

	Private lMsErroAuto := .F.

	cFilAnt := cFilRef

	//Exclus�o deve ter o registro SE1 posicionado
	SE1->( DbSetOrder(1) )
	If SE1->( DbSeek(xFilial("SE1") + PADR(cPrefixo,TamSX3("E1_PREFIXO")[1]) + PADR(cNum,TamSX3("E1_NUM")[1]) + PADR(cParc,TamSX3("E1_PARCELA")[1]) + PADR(cTipo,TamSX3("E1_TIPO")[1]) ) )

		aAdd(aArray,{ "E1_PREFIXO" 	, cPrefixo  , NIL } )
		aAdd(aArray,{ "E1_NUM" 		, cNum 		, NIL } )
		aAdd(aArray,{ "E1_TIPO" 	, cTipo 	, NIL } )
		aAdd(aArray,{ "E1_PARCELA" 	, cParc		, NIL } )

		SA2->( dbSetOrder(1) )
		If SA2->( dbSeek( xFilial("SA2") + cCliente + cLoja ) )
			aAdd(aArray,{ "E1_CLIENTE"	, SA2->A2_CLIENTE		, NIL } )
			aAdd(aArray,{ "E1_LOJA"   	, SA2->A2_LOJCLI 		, NIL } )
		EndIf

		MsExecAuto( { |x,y| FINA040(x,y)}, aArray, 5)
	Else
		lMsErroAuto := .F.
	EndIf

	cFilRef := cFilAnt

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aArray)
Return !lMsErroAuto

/*/{Protheus.doc} TA042EfNFS
Fun��o para gera��o de nota fiscal de sa�da na efetiva��o da concilia��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042EfNFS( cForRep, cLojaRep, cNumPar, cSeriePar, aItensNF, aLog )
	Local aArea		:= GetArea()
	Local aCabs     := {}
	Local aItens    := {}
	Local aStruSF2  := {}
	Local aStruSD2  := {}
	Local aDocOri	:= {}
	Local cNumero	:= cNumPar
	Local cSerie    := cSeriePar
	Local cTpCli	:= ""
	Local cTpRV		:= ""
	Local cItem 	:= "00"
	Local lRet		:= .T.
	Local nX        := 1
	Local nJ 		:= 0
	Local cCond		:= SuperGetMV("MV_TURCCP",,"")

	Local nF2FILIAL   := 0
	Local nF2TIPO     := 0
	Local nF2DOC      := 0
	Local nF2SERIE    := 0
	Local nF2EMISSAO  := 0
	Local nF2CLIENTE  := 0
	Local nF2LOJA     := 0
	Local nF2ESPECIE  := 0
	Local nF2COND     := 0
	Local nF2DTDIGIT  := 0
	Local nF2EST      := 0
	Local nF2VALMERC  := 0
	Local nF2TIPOCLI  := 0
	Local nF2MOEDA	  := 0

	Local nD2FILIAL  	:= 0
	Local nD2DOC     	:= 0
	Local nD2SERIE   	:= 0
	Local nD2CLIENTE 	:= 0
	Local nD2LOJA    	:= 0
	Local nD2EMISSAO 	:= 0
	Local nD2TIPO    	:= 0
	Local nD2ITEM    	:= 0
	Local nD2CF      	:= 0
	Local nD2COD     	:= 0
	Local nD2UM      	:= 0
	Local nD2QUANT   	:= 0
	Local nD2PRCVEN  	:= 0
	Local nD2TOTAL   	:= 0
	Local nD2LOCAL   	:= 0
	Local nD2TES     	:= 0
	Local nD2BASEIPI 	:= 0
	Local nD2ALIQIPI 	:= 0
	Local nD2VALIPI  	:= 0
	Local nD2BASEICM 	:= 0
	Local nD2ALIQICM 	:= 0
	Local nD2VALICM  	:= 0
	Local nD2TP			:= 0
	Local nD2CODISS  	:= 0
	Local nD2ESTOQUE 	:= 0
	Local nD2CONTA		:= 0

	Private lMsErroAuto := .F.

	DEFAULT aLog		:= {}

	If Empty( cCond )
		Help(,,"TA042EFETIVA",,STR0080,1,0)			 //"Solicite ao administrador do sistema o cadastro do par�metro 'MV_TURCCP', com a condi��o de pagamento para a nota fiscal de sa�da."
		Return .F.
	EndIf

	SA2->( dbSetOrder(1) )
	If !SA2->( dbSeek( xFilial("SA2") + cForRep + cLojaRep ) )
		Help(,,"TA042NFS",,STR0086,1,0) //"Fornecedor informado n�o cadastrado."
		Return .F.
	EndIf

	If Empty( SA2->A2_CLIENTE ) .Or. Empty( SA2->A2_LOJCLI )
		Help(,,"TA042NFS",,STR0087 + cForRep + "/" + cLojaRep + STR0088,1,0) //"Fornecedor "###" n�o possui c�digo de cliente e loja cadastrados, n�o ser� poss�vel efetivar a concilia��o."
		Return .F.
	EndIf

	SA1->( dbSetOrder(1) )
	If !SA1->( dbSeek( xFilial( "SA1" ) + SA2->A2_CLIENTE + SA2->A2_LOJCLI ) )
		Help(,,"TA042NFS",,STR0089 + SA2->A2_CLIENTE + "/" + SA2->A2_LOJCLI + STR0090,1,0) //"Cliente "###" n�o localizado no cadastro de cliente."
		Return .F.
	EndIf

	//Estrutura do dicionario utilizado pela rotina automatica
	aStruSF2    :=  SF2->(dbStruct())

	//Montagem da capa do documento fiscal
	nF2FILIAL   := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_FILIAL"})
	nF2TIPO     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_TIPO"   })
	nF2DOC      := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_DOC"    })
	nF2SERIE    := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_SERIE"  })
	nF2EMISSAO  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_EMISSAO"})
	nF2CLIENTE  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_CLIENTE"})
	nF2LOJA     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_LOJA"   })
	nF2ESPECIE  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_ESPECIE"})
	nF2COND     := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_COND"   })
	nF2DTDIGIT  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_DTDIGIT"})
	nF2EST      := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_EST"    })
	nF2VALMERC  := Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_VALMERC"})
	nF2TIPOCLI	:= Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_TIPOCLI"})
	nF2MOEDA	:= Ascan(aStruSF2,{|x| AllTrim(x[1]) == "F2_MOEDA"  })

	For nX := 1 To Len(aStruSF2)
		If aStruSF2[nX][2] $ "C/M"
			Aadd(aCabs,"")
		ElseIf aStruSF2[nX][2] == "N"
			Aadd(aCabs,0)
		ElseIf aStruSF2[nX][2] == "D"
			Aadd(aCabs,CtoD("  /  /  "))
		ElseIf aStruSF2[nX][2] == "L"
			Aadd(aCabs,.F.)
		EndIf
	Next

	aCabs[nF2FILIAL]    := xFilial("SF2")
	aCabs[nF2TIPO]      := "N"
	aCabs[nF2DOC]       := cNumero
	aCabs[nF2SERIE]     := cSerie
	aCabs[nF2EMISSAO]   := dDataBase
	aCabs[nF2CLIENTE]   := SA1->A1_COD
	aCabs[nF2LOJA]      := SA1->A1_LOJA
	aCabs[nF2ESPECIE]   := "NF"
	aCabs[nF2COND]      := cCond
	aCabs[nF2DTDIGIT]   := dDataBase
	aCabs[nF2EST]      	:= SA1->A1_EST
	aCabs[nF2TIPOCLI]	:= SA1->A1_TIPO
	aCabs[nF2MOEDA]		:= CriaVar( 'F2_MOEDA' )

	//Estrutura do dicionario utilizado pela rotina automatica
	aStruSD2    :=  SD2->(dbStruct())

	//Montagem dos itens do documento fiscal
	nD2FILIAL   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_FILIAL"  })
	nD2DOC      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_DOC"	 })
	nD2SERIE    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_SERIE"	 })
	nD2CLIENTE  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CLIENTE" })
	nD2LOJA     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_LOJA"	 })
	nD2EMISSAO  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_EMISSAO" })
	nD2TIPO     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_TIPO"	 })
	nD2ITEM     := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_ITEM"	 })
	nD2CF       := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CF"		 })
	nD2COD      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_COD"	 })
	nD2UM       := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_UM"		 })
	nD2QUANT    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_QUANT"	 })
	nD2PRUNIT	:= Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_PRUNIT"  })
	nD2PRCVEN   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_PRCVEN"  })
	nD2TOTAL    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_TOTAL"	 })
	nD2LOCAL    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_LOCAL"	 })
	nD2TES      := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_TES"	 })
	nD2BASEIPI  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_BASEIPI" })
	nD2ALIQIPI  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_IPI"	 })
	nD2VALIPI   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_VALIPI"  })
	nD2BASEICM  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_BASEICM" })
	nD2ALIQICM  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_PICM"	 })
	nD2VALICM   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_VALICM"  })
	nD2TP		:= Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_TP"      })
	nD2CODISS   := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CODISS"  })
	nD2ESTOQUE  := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_ESTOQUE" })
	nD2CONTA    := Ascan(aStruSD2,{|x| AllTrim(x[1]) == "D2_CONTA"   })

	SF4->( dbSetOrder(1) )
	SB1->( dbSetOrder(1) )
	SBZ->( dbSetOrder(1) )

	For nX := 1 to len( aItensNF )
		aCabs[nF2VALMERC] += aItensNF[nX][3]

		aAdd(aItens, {})
		AADD(aDocOri,0)

		nPos := Len(aItens)

		For nJ := 1 To Len(aStruSD2)
			If  aStruSD2[nJ][2]$"C/M"
				aAdd(aItens[nPos],"")
			ElseIf aStruSD2[nJ][2]=="D"
				aAdd(aItens[nPos],CToD(""))
			ElseIf aStruSD2[nJ][2]=="N"
				aAdd(aItens[nPos],0)
			ElseIf aStruSD2[nJ][2]=="L"
				aAdd(aItens[nPos],.T.)
			EndIf
		Next

		cItem := Soma1( cItem )
		aItens[Len(aItens),nD2FILIAL]  	:=  xFilial("SF2")
		aItens[Len(aItens),nD2ITEM]    	:=  cItem
		aItens[Len(aItens),nD2DOC]     	:=  cNumero
		aItens[Len(aItens),nD2SERIE]   	:=  cSerie
		aItens[Len(aItens),nD2CLIENTE]	:=  SA1->A1_COD
		aItens[Len(aItens),nD2LOJA]    	:=  SA1->A1_LOJA
		aItens[Len(aItens),nD2EMISSAO] 	:=  dDatabase
		aItens[Len(aItens),nD2TIPO]    	:=  "N"
		aItens[Len(aItens),nD2UM]     	:=  "UN"
		aItens[Len(aItens),nD2QUANT]  	:=  1
		aItens[Len(aItens),nD2PRUNIT] 	:=  aItensNF[nX][3]
		aItens[Len(aItens),nD2PRCVEN] 	:=  aItensNF[nX][3]
		aItens[Len(aItens),nD2TOTAL]  	:=  aItensNF[nX][3]

		SB1->( dbSeek( xFilial("SB1") + aItensNF[nX][1] ) )

		aItens[Len(aItens),nD2LOCAL]  	:= SB1->B1_LOCPAD
		aItens[Len(aItens),nD2COD]     	:= SB1->B1_COD
		aItens[Len(aItens),nD2TP]     	:= SB1->B1_TIPO
		aItens[Len(aItens),nD2CONTA]    := SB1->B1_CONTA

		If !Empty( SB1->B1_CODISS )
			aItens[Len(aItens),ND2CODISS]	:= SB1->B1_CODISS
		ElseIf SBZ->( dbSeek( xFilial("SBZ") + aItensNF[nX][1] ) ) .And. !Empty( SBZ->BZ_CODISS )
			aItens[Len(aItens),ND2CODISS]	:= SBZ->BZ_CODISS
		EndIf

		If Empty( aItensNF[nX][2] ) .OR. !(SF4->( DbSeek( xFilial( "SF4" ) + aItensNF[nX][2] ) ) .And. SF4->F4_DUPLIC == "S")
			If Empty(SB1->B1_TS) .Or. !(SF4->( dbSeek( xFilial("SF4") + SB1->B1_TS) ) .And. SF4->F4_DUPLIC == "S")
				Help(,,"TA042EFETES",,i18N(STR0108,{SB1->B1_COD}),1,0)			 //"TES n�o cadastrada ou TES informada n�o gera duplicata para o produto #1[ codigo produto ]#."
				Return .F.
			EndIf
		EndIf

		aItens[Len(aItens),nD2TES]    	:= SF4->F4_CODIGO
		aItens[Len(aItens),nD2CF]		:= SF4->F4_CF
		aItens[Len(aItens),nD2ESTOQUE]	:= SF4->F4_ESTOQUE

	Next

	cNumero := MaNfs2Nfs("",; 		//Serie do Documento de Origem 
	                     "",; 		//Numero do Documento de Origem 
	                     "",; 		//Cliente/Fornecedor do documento do origem 
	                     "",; 		//Loja do Documento de origem 
	                     cSerie,; 	//Serie do Documento a ser gerado 
	                     ,;			//Mostra Lct.Contabil (OPC) 
	                     ,;			//Aglutina Lct.Contabil (OPC) 
	                     ,;			//Contabiliza On-Line (OPC) 
	                     ,;			//Contabiliza Custo On-Line (OPC) 
	                     ,;			//Reajuste de preco na nota fiscal (OPC) 
	                     ,;			//Tipo de Acrescimo Financeiro (OPC) 
	                     ,;			//Tipo de Arredondamento (OPC) 
	                     ,;			//Atualiza Amarracao Cliente x Produto (OPC) 
	                     ,;			//Cupom Fiscal (OPC) 
	                     ,;			//CodeBlock de Selecao do SD2 (OPC) 
	                     ,;			//CodeBlock a ser executado para o SD2 (OPC) 
	                     ,;			//CodeBlock a ser executado para o SF2 (OPC) 
	                     ,;			//CodeBlock a ser executado no final da transacao (OPC) 
	                     aDocOri,;	//Array com os Recnos do SF2 (OPC) 
	                     aItens,;	//Array com os itens do SD2 (OPC) 
	                     aCabs,;	//Array com os dados do SF2 (OPC) 
	                     .F.,;		//Calculo Fiscal - Desabilita o calculo fiscal pois as informacoes ja foram passadas nos campos do SD2 e SF2 (OPC) 
	                     ,;			//code block para tratamento do fiscal - SF2 (OPC) 
	                     ,;			//code block para tratamento do fiscal - SD2 (OPC) 
	                     ,;			//code block para tratamento do fiscal - SE1 (OPC) 
	                     )			//Numero do documento fiscal (OPC)

	If lMsErroAuto
		lRet := .F.
	Else
		cNumPar	  := PadR(cNumero, TamSX3('F2_DOC')[1])
		cSeriePar := PadR(cSerie , TamSX3('F2_SERIE')[1])

		SF2->( dbSetOrder(1) )
		If SF2->( dbSeek( xFilial('SF2') + cNumPar + cSeriePar ) )
			//Cria o vetor de Log que ser� retornado a chamada de oritem
			aAdd( aLog,{ "G8Y_FILREF"	, xFilial( 'SF2' )	} )
			aAdd( aLog,{ 'G8Y_TPDOC'	, '4' 				} )
			aAdd( aLog,{ 'G8Y_DOC'		, cNumPar 			} )
			aAdd( aLog,{ 'G8Y_SERIE'	, cSeriePar  		} )
			aAdd( aLog,{ 'G8Y_PREFIX'	, cSeriePar			} )
			aAdd( aLog,{ 'G8Y_NUM'		, cNumPar			} )
			aAdd( aLog,{ 'G8Y_FORNEC' 	, cForRep			} )
			aAdd( aLog,{ 'G8Y_LOJA' 	, cLojaRep			} )
			aAdd( aLog,{ 'G8Y_TIPO'		, 'NF'				} )
			aAdd( aLog,{ 'G8Y_VALOR'	, aCabs[nF2VALMERC] } )
			aAdd( aLog,{ "G8Y_IDDOC"	, "" 				} )
		Else
			FwAlertHelp(I18N(STR0144,{ cNumNFS, cSerNFS }), "",  "TA42ASEMNFS")		//"Nota fiscal de sa�da #1[ Numero nota ]# S�rie #2[ S�rie ]# n�o localizado."
			lRet := .F.
		EndIf
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aCabs)
	TURXNIL(aItens)
	TURXNIL(aStruSF2)
	TURXNIL(aStruSD2)
	TURXNIL(aDocOri)
Return lRet

/*/{Protheus.doc} TA042DeNFS
Fun��o para estorno de notas fiscais de sa�da gerada na efetiva��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042DeNFS( cDoc, cSerie, cFornece, cLoja, cFilRef )
	Local aArea     := GetArea()
	Local aRegSD2	:= {}
	Local aRegSE1	:= {}
	Local aRegSE2	:= {}
	Local cMarca	:= ""
	Local cBkpFil	:= cFilAnt
	Local lRet		:= .T.

	DEFAULT cFilRef := cFilAnt

	cFilAnt := cFilRef

	SF2->( dbSetOrder(1) )
	If SF2->( dbSeek( xFilial("SF2") + cDoc + cSerie ) )
		cMarca := GetMark(,"SF2","F2_OK")
		RecLock("SF2",.F.)
		SF2->F2_OK := cMarca
		SF2->( MsUnlock() )

		If MaCanDelF2("SF2",SF2->(RecNo()),@aRegSD2,@aRegSE1,@aRegSE2)
			SF2->( MaDelNFS(aRegSD2,aRegSE1,aRegSE2))
		Else
			lRet := .F.
		EndIf
	EndIf

	cFilRef := cFilAnt
	
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aRegSD2)
	TURXNIL(aRegSE1)
	TURXNIL(aRegSE2)
Return	lRet

Function TA042TrAllT( cConcil, cTipoCon, cFornPar, cLojaPar, aLog )
	Local aArea     := GetArea()
	Local aAux	:= {}
	Local nX	:= 0

	aAux := TA042GetTit( cConcil, cTipoCon, cFornPar, cLojaPar, '1', ,.T. )

	//Realiza a transfer�ncia dos t�tulos para a filial corrente
	SE1->( dbSetOrder(1) )
	For nX := 1 to len(aAux)
		If SE1->( dbSeek( aAux[nX] ) )
			TA042TrfTi( cFilAnt, aAux[nX], SE1->E1_CLIENTE, SE1->E1_LOJA, SE1->E1_CLIENTE, SE1->E1_LOJA, aLog )
		EndIf
	Next
	
	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAux)
Return .T.

/*/{Protheus.doc} TA042CompT
Fun��o para compensa��o de t�tulos entre carteiras
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042CompT( cConcil, cTipoCon, cFornPar, cLojaPar, aLog )
	Local aArea     := GetArea()
	Local aAux		:=  {}
	Local aTitSE1	:= 	{}
	Local aTitSE2	:=	{}
	Local cCliente	:= ""
	Local cLojaCli	:= ""
	Local cFornece	:= ""
	Local cLojaFor	:= ""
	Local dDtIni	:= cTod(" / / ")
	Local dDtFim	:= cTod(" / / ")
	Local lRet		:= .T.
	Local nX		:= 0
	Local nValSE1	:= 0
	Local nValSE2	:= 0
	Local nPos		:= 0
	Local nPosSerie	:= 0

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.

	//Realiza a transfer�ncia dos t�tulos para a filial corrente
	aAux := TA042GetTit( cConcil, cTipoCon, cFornPar, cLojaPar, '1' )
	SE1->( dbSetOrder(1) )
	For nX := 1 to len(aAux)
		If SE1->( dbSeek( aAux[nX] ) ) .And. SE1->E1_SALDO > 0
			cCliente	:= SE1->E1_CLIENTE
			cLojaCli	:= SE1->E1_LOJA
			nValSE1 	+= SE1->E1_SALDO

			If SE1->E1_VENCREA < dDtIni .Or. Empty( dDtIni )
				dDtIni	:= SE1->E1_VENCREA
			EndIf

			If SE1->E1_VENCREA > dDtFim .Or. Empty( dDtFim )
				dDtFim := SE1->E1_VENCREA
			EndIf

			AAdd(aTitSE1, {SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO})
		EndIf
	Next

	aAux := TA042GetTit( cConcil, cTipoCon, cFornPar, cLojaPar, '2' )
	SE2->( dbSetOrder(1) )
	For nX := 1 to len(aAux)

		If SE2->( dbSeek( aAux[nX]  ) ) .And. SE2->E2_SALDO > 0
			cFornece	:= SE2->E2_FORNECE
			cLojaFor	:= SE2->E2_LOJA
			nValSE2 	+= SE2->E2_SALDO

			If SE2->E2_VENCREA < dDtIni .Or. Empty( dDtIni )
				dDtIni	:= SE2->E2_VENCREA
			EndIf

			If SE2->E2_VENCREA > dDtFim .Or. Empty( dDtFim )
				dDtFim := SE2->E2_VENCREA
			EndIf

			AAdd(aTitSE2, {SE2->E2_FILIAL + SE2->E2_PREFIXO + SE2->E2_NUM + SE2->E2_PARCELA	+ SE2->E2_TIPO + SE2->E2_FORNECE + SE2->E2_LOJA})
		EndIf
	Next

	If Len(aTitSE1) > 0 .And. Len(aTitSE2) > 0
		aAutoCab := {{"AUTDVENINI450", dDtIni				  , nil},; 
			         {"AUTDVENFIM450", dDtFim				  , nil},; 
			         {"AUTNLIM450"   , Max( nValSE1, nValSE2 ), nil},; 
			         {"AUTCCLI450"   , cCliente				  , nil},; 
			         {"AUTCLJCLI"    , cLojaCli				  , nil},; 
			         {"AUTCFOR450"   , cFornece				  , nil},; 
			         {"AUTCLJFOR"    , cLojaFor				  , nil},; 
			         {"AUTCMOEDA450" , "01"					  , nil},; 
			         {"AUTNDEBCRED"  , 1					  , nil},; 
			         {"AUTLTITFUTURO", .F.					  , nil},; 
			         {"AUTARECCHAVE" , aTitSE1				  , nil},; 
			         {"AUTAPAGCHAVE" , aTitSE2				  , nil}}

		MSExecAuto({|x,y,z| Fina450(x,y,z)}, nil , aAutoCab , 3 )
		If !lMsErroAuto
			confirmsx8()

			If len( aTitSE1 ) > 0 .And. len( aTitSE2 ) > 0
				SE1->( dbSetOrder(1) )
				SE2->( dbSetOrder(1) )

				nValSE1	:= 0
				nValSE2	:= 0

				For nX := 1 to len( aTitSE1 )
					SE1->( dbSeek( aTitSE1[nX][1]  ) )
					nValSE1 += SE1->E1_SALDO
				Next

				For nX := 1 to len( aTitSE2 )
					SE2->( dbSeek( aTitSE2[nX][1] ) )
					nValSE2 += SE2->E2_SALDO
				Next

				aAdd( aLog,{ 'G8Y_FILREF'	, ''	 	} )
				aAdd( aLog,{ 'G8Y_TPDOC'	, '7' 		} )
				aAdd( aLog,{ 'G8Y_DOC'		, '' 		} )
				aAdd( aLog,{ 'G8Y_SERIE'	, ''  		} )
				aAdd( aLog,{ 'G8Y_PREFIX'	, ''		} )
				aAdd( aLog,{ 'G8Y_NUM'		, ''		} )
				aAdd( aLog,{ 'G8Y_FORNEC' 	, cFornPar	} )
				aAdd( aLog,{ 'G8Y_LOJA' 	, cLojaPar	} )
				aAdd( aLog,{ 'G8Y_TIPO'		, ''		} )
				aAdd( aLog,{ 'G8Y_VALOR'	, nValSE2 - nValSE1 } )
				aAdd( aLog,{ "G8Y_IDDOC"	, ''  		} )
			EndIf
		Else
			lRet := .F.
			msgalert(STR0081) //"Erro na inclusao!"
			rollbacksx8()
			MostraErro()
		EndIf
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAux)
	TURXNIL(aTitSE1)
	TURXNIL(aTitSE2)
Return lRet

/*/{Protheus.doc} TA042CmpCa
Fun��o para estorno da compensa��o entre carteiras
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function  TA042CmpCa( cConcil, cFornece, cLoja, cTpFat  )
	Local aArea     := GetArea()
	Local aAuxSE2	:= {}
	Local aComp		:= {}
	Local aAutoCab 	:= {}
	Local lRet		:= .T.
	Local nX		:= 0

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.

	If cTpFat == '1'
		aAuxSE2 := TA042GetTit( cConcil, '1', cFornece, cLoja, '2' )
	Else
		aAuxSE2 := TA042GetTit( cConcil, '2', cFornece, cLoja, '2' )
	EndIf

	SE2->( dbSetOrder(1) )
	For nX := 1 to len(aAuxSE2)
		If SE2->( dbSeek(aAuxSE2[nX]) ) .And. !Empty(SE2->E2_IDENTEE)
			If aScan( aComp, {|x| x[1] == SE2->E2_IDENTEE } ) == 0
				aAdd( aComp, { SE2->E2_IDENTEE, SE2->( RecNo() ) } )
			EndIf
		EndIf

	Next

	For nX := 1 to len(aComp)
		aAutoCab := {{"CFILCAN", cFilAnt, nil}, {"CCOMPCAN", aComp[nX][1], nil}}

		SE2->( dbGoTo( aComp[nX][2] ) )
		MSExecAuto({|x,y,z| Fina450(x,y,z)}, nil , aAutoCab , 4 )
		If lMsErroAuto
			MostraErro()
			lRet := .F.
		Endif
	Next

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAuxSE2)
	TURXNIL(aComp)
	TURXNIL(aAutoCab)
Return lRet

/*/{Protheus.doc} TA042ItNFS
Retorna os itens aptos a serem faturados na nota fiscao de sa�da
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param cForRep, character, C�digo do fornecedor de report
@param cLojaRep, character, Loja do fornecedor de report
@param aItensNF, array, Vetor em qual ser� retornado os itens aptos a faturar
@param nValAbat, num�rico, Valor de abatimento a ser incorporado na NFE
@param nTitAvulso, num�rico, Valor de t�tulos avulsos a ser gerados por produto n�o possuir cadastro de parametros fiscais
@param cErro, character, Mensagem de erro que impossibilitou a gera��o dos itens
/*/
Function TA042ItNFS( cForRep, cLojaRep, aItensNF, aAbat, cErro , oModel)
	Local aArea 	:= GetArea()
	Local aParFis	:= {}
	Local cTpCli	:= ""
	Local cTpRV		:= ""
	Local cNatureza := ""
	Local cFilRef	:= ""
	Local nPosFRef 	:= 0
	Local nX		:= 0
	Local nY		:= 0
	Local aIt		:= {}
	Local nTamVl	:= TamSx3("G4C_VALOR")[1]
	Local nDecVl	:= TamSx3("G4C_VALOR")[2]
	Local nP		:= 0
	Local nFil		:= 0
	Local nSer		:= 0
	Local nNat		:= 0
	Local nIte		:= 0
	Local nItFilial	:= 0
	Local oModelG3R	:= nil
	Local oModelG48B:= nil

	DEFAULT cErro	:= ""
	DEFAULT oModel	:= FwModelActive()

	Private lMsErroAuto := .F.

	SF4->( dbSetOrder(1) )
	SA1->( dbSetOrder(1) )

	SA2->( dbSetOrder(1) )
	If !SA2->( dbSeek( xFilial("SA2") + cForRep + cLojaRep ) )
		cErro := STR0086 //"Fornecedor informado n�o cadastrado."
		Return .F.
	EndIf

	If Empty( SA2->A2_CLIENTE ) .Or. Empty( SA2->A2_LOJCLI )
		cErro := STR0087 + cForRep + "/" + cLojaRep + STR0088 //"Fornecedor "###" n�o possui c�digo de cliente e loja cadastrados, n�o ser� poss�vel efetivar a concilia��o."
		Return .F.
	EndIf

	oModelG3R := oModel:GetModel( "G3R_ITENS" )
	For nX := 1 to oModelG3R:Length()
		oModelG3R:GoLine( nX )

		cFilRef := T42FilRef( oModel )

		//Se particionado ou cancelado, pula o DR
		If oModelG3R:GetValue("G3R_STATUS") $ "2|4"
			Loop
		EndIf

		oModelG4CB	:= oModel:GetModel( "G4CB_ITENS" )
		oModelG48B 	:= oModel:GetModel( "G48B_ITENS" )

		For nY := 1 to oModelG4CB:Length()
			oModelG4CB:GoLine( nY )

			//Somente verifica os itens com o fornecedor de repasse igual ao informado por parametro
			//Rotina ser� utilizada pela quebra de fornecedores na concilia��o a�rea
			If oModelG4CB:GetValue('G4C_CODIGO') <> cForRep .Or. oModelG4CB:GetValue('G4C_LOJA') <> cLojaRep
				Loop
			EndIf

			If  oModelG4CB:GetValue( "G4C_TIPO" ) $ '3|4' .And. oModelG4CB:GetValue( "G4C_STATUS" ) == "1" .And. Empty(oModelG4CB:GetValue( "G4C_CARTUR" ))

				// Tipo 3 - Receitas
				If oModelG4CB:GetValue( "G4C_TIPO" ) == '3'

					//Receitas a pagar ( Acertos )
					If oModelG4CB:GetValue("G4C_PAGREC") == "1"

						If Empty(  oModelG4CB:GetValue('G4C_APLICA')  )
							cNatureza := oModelG3R:GetValue('G3R_NATURE')
						Else
							oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
							cNatureza := oModelG48B:GetValue('G48_NATURE')
						EndIf

						nPosFRef := aScan( aAbat, {|x|, x[1] == oModelG3R:GetValue('G3R_MSFIL') .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
						If nPosFRef == 0
							AAdd(aAbat, { oModelG3R:GetValue('G3R_MSFIL'), cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
							nPosFRef := Len(aAbat)
						EndIf

						aAbat[nPosFRef][3] += oModelG4CB:GetValue( "G4C_VALOR" )

					//Receitas a receber
					Else

						aParFis := T35PesqPFF(oModelG3R:GetValue('G3R_MSFIL'),; 
						                      oModelG4CB:GetValue('G4C_CLASS'),; 
						                      oModel:GetValue('G3P_FIELDS','G3P_SEGNEG'),; 
						                      oModelG4CB:GetValue('G4C_CODPRO'),; 
						                      '2',; 
						                      cForRep,; 
						                      cLojaRep)

						//Verifica se o produto possui c�digo de produto cadastrado nos parametros fiscais e financeiros
						If !Empty( aParFis[4] ) .And. oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
							TA42AddItNf( aItensNF, oModelG3R:GetValue('G3R_MSFIL'), aParFis[3], oModelG48B:GetValue( 'G48_NATURE' ), aParFis[4], aParFis[2], oModelG4CB:GetValue( "G4C_VALOR" )  )

						Else
							FwAlertHelp(I18N(STR0145,{ oModelG4CB:GetValue('G4C_CLASS'), oModel:GetValue('G3P_FIELDS','G3P_SEGNEG'), oModelG4CB:GetValue('G4C_CODPRO') }), STR0146, 'TA042PFF')	//"N�o foi localizado o parametro fiscal e financeiro para a Classifica��o #1[Class]#, Seg. Neg. #2[Segmento]# e Produto #3[Produto]#"	//"Cadastre o parametro fiscal e financeiro."
							Return .F.
						EndIf
					EndIf

				// Tipo 4 - Abat. Receitas
				Else

					If Empty(  oModelG4CB:GetValue('G4C_APLICA')  )
						cNatureza := oModelG3R:GetValue('G3R_NATURE')
					Else
						oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
						cNatureza := oModelG48B:GetValue('G48_NATURE')
					EndIf

					nPosFRef := aScan( aAbat, {|x|, x[1] == oModelG3R:GetValue('G3R_MSFIL') .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
					If nPosFRef == 0
						AAdd(aAbat, { oModelG3R:GetValue('G3R_MSFIL'), cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
						nPosFRef := Len(aAbat)
					EndIf

					//Abat. de receitas a pagar
					If oModelG4CB:GetValue("G4C_PAGREC") == "1"
						aAbat[nPosFRef][3] += oModelG4CB:GetValue( "G4C_VALOR" )

						//Abat. de receitas a receber ( Acertos )
					Else
						aAbat[nPosFRef][3] -= oModelG4CB:GetValue( "G4C_VALOR" )

					EndIf
				EndIf
			EndIf
		Next
	Next

	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o tratamento de abatimento dos Itens da NF. Positivo(aItensNF) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------
	//Cria array auxiliar ordenado para abatimento por ordem do menor valor por filial
	If Len(aAbat) > 0 .And. Len(aItensNF) > 0
		For nFil := 1 To Len(aItensNF) //Filiais
			For nSer := 1 To Len(aItensNF[nFil,2]) //S�ries
				For nNat := 1 To Len(aItensNF[nFil,2,nSer,2]) //Natureza
					For nIte := 1 To Len(aItensNF[nFil,2,nSer,2,nNat,2]) //Itens
						AAdd(aIt,{aItensNF[nFil,1],nFil,nSer,nNat,nIte,aItensNF[nFil,2,nSer,2,nNat,2,nIte,3]})
					Next
				Next
			Next
		Next

		aIt := ASort(aIt,,,{|x,y| x[1]+STR(x[6],nTamVl,nDecVl) < y[1]+STR(y[6],nTamVl,nDecVl) })

		For nX := 1 To Len( aAbat )
			nP := AScan( aIt, {|x|, x[1] == aAbat[nX][1] } )

			If nP > 0
				nItFilial := 1

				//Abate enquanto existir itens da NF para Filial e valor de abatimento para a filial
				While nItFilial <= Len( aItensNF[aIt[nP,2]][2] ) .And. aAbat[nX][3] > 0
					nItFilial++

					If aIt[nP,6] > aAbat[nX][3]
						//Se o valor do item � maior que o abatimento, diminui o Item e zera o abatimento
						aIt[nP,6] -= aAbat[nX][3]
						aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2][aIt[nP,5]][3] = aIt[nP,6]
						aAbat[nX][3] 	:= 0

					Else
						//Se o valor do item � menor ou igual que o abatimento, diminui o abatimento e apaga o item
						aAbat[nX][3] -= aIt[nP,6]

						//Apaga o Item (Produto e TES)
						ADel( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2], aIt[nP,5])
						ASize(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2], Len(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2]) - 1)

						If Len(aItensNF[aIt[nP,2]][2][aIt[nP,3]][2][aIt[nP,4]][2]) == 0

							//Apagar a Natureza
							ADel(  aItensNF[aIt[nP,2]][2][aIt[nP,3]][2], aIt[nP,4] )
							ASize( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2], Len( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2] ) - 1 )

							If Len( aItensNF[aIt[nP,2]][2][aIt[nP,3]][2] ) == 0

								//Apagar a S�rie
								ADel(  aItensNF[aIt[nP,2]][2],aIt[nP,3] )
								ASize( aItensNF[aIt[nP,2]][2], Len( aItensNF[aIt[nP,2]][2] ) - 1 )

							EndIf
						EndIf

						//Apaga Array auxiliar
						ADel(aIt, nP)
						ASize(aIt, Len(aIt)-1)
						Exit
					EndIf
				EndDo
			EndIf
		Next
	EndIf

	If Len( aItensNF ) == 0
		aItensNF := {}
	EndIf

	nP := aScan( aAbat, {|x| x[3] == 0} )
	While nP > 0 .And. len(aAbat) > 0
		aDel(aAbat, nP)
		aSize(aAbat, len(aAbat) - 1 )
		nP := aScan( aAbat, {|x| x[3] == 0} )
	EndDo

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aIt)
Return .T.

/*/{Protheus.doc} TA42AddItNf
Fun��o auxiliar para obter os itens da NF
@type function
@author Anderson Toledo
@since 27/05/2016
@version 1.0
/*/
Function TA42AddItNf( aItensNF, cFilRef, cSerieNF, cNaturNF, cCodPrd, cTES, nValor )
	Local nPosFRef 	  := 0
	Local nPosSerie	  := 0
	Local nPosNaturez := 0
	Local nPosItem	  := 0

	//Verifica se j� existe filial de refer�ncia
	nPosFRef := aScan( aItensNF, {|x|, x[1] == cFilRef } )
	If nPosFRef == 0
		AAdd(aItensNF, { cFilRef, {} } )
		nPosFRef := Len(aItensNF)
	EndIf

	//Verifica a posi��o da s�rie da nota fical
	nPosSerie	:= aScan( aItensNF[nPosFRef][2], {|x| x[1] == cSerieNF }  )
	If nPosSerie == 0
		AAdd( aItensNF[nPosFRef][2], { cSerieNF, {} } )
		nPosSerie := Len(aItensNF[nPosFRef][2])
	EndIf

	//Verifica a posi��o da natureza
	nPosNaturez := aScan( aItensNF[nPosFRef][2][nPosSerie][2], {|x| x[1] == cNaturNF }  )
	If nPosNaturez == 0
		AAdd( aItensNF[nPosFRef][2][nPosSerie][2], { cNaturNF, {} } )
		nPosNaturez := Len(aItensNF[nPosFRef][2][nPosSerie][2])
	EndIf

	//Verifica a posi��o do item na NF
	nPosItem := aScan( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2], {|x|  x[1] == cCodPrd .And. x[2] == cTES  } )
	If nPosItem == 0
		AAdd( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2], { cCodPrd, cTES, 0 } )
		nPosItem := Len( aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2] )
	EndIf

	aItensNF[nPosFRef][2][nPosSerie][2][nPosNaturez][2][nPosItem][3] += nValor

Return

/*/{Protheus.doc} TA042ComRA
Fun��o para compensa��o de PA geradas atrav�s da antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param cFatura, character, C�digo da fatura
@param cFornece, character, C�digo do fornecedor
@param cLoja, character, Loja da fatura
@param cTpFat, character, tipo da fatura [1] - A�rea, [2] - Terrestre
/*/
Function TA042ComRA( cFatura, cConcil, cFornece, cLoja, cTpFat, aLog )
	Local aArea		:= GetArea()
	Local aAux		:= {}
	Local aRecSE1	:= {}
	Local aRecRA	:= {}
	Local lRet 		:= .T.
	Local nX		:= 0

	Local lContabiliza 	:= .F.
	Local lAglutina   	:= .F.
	Local lDigita   	:= .F.

	Default aLog := {}
	
	//Verifica se existe recebimento antecipado para esta fatura
	G8X->(dbSetOrder(1))
	If G8X->( dbSeek( xFilial('G8X') + cTpFat + cFatura + cFornece + cLoja  ) )

		//Obtem os dados do RA
		SE1->( dbSetOrder(1) )
		If SE1->(dbSeek( xFilial('SE1') + G8X->(G8X_PREFIX + G8X_NUM + G8X_PARCEL + G8X_TIPO))) .And. SE1->E1_SALDO > 0
			aAdd( aRecRA, SE1->( RecNo() ) )
		EndIf

		//Obtem o Recno de t�tulos a receber
		If cTpFat == '1'
			aAux := TA042GetTit( cFatura, '1', cFornece, cLoja, '1' )
		Else
			aAux := TA042GetTit( cConcil, '2', cFornece, cLoja, '1' )
		EndIf

		SE1->( dbSetOrder( 1 ) )
		For nX := 1 to len(aAux)
			If SE1->( dbSeek( aAux[nX] ) ) .And. SE1->E1_SALDO > 0
				aAdd( aRecSE1, SE1->( RecNo() ) )
			EndIf
		Next

		If len( aRecRA ) > 0 .And. len( aRecSE1 ) > 0

			PERGUNTE("FIN330",.F.)
			lContabiliza  	:= MV_PAR09 == 1
			lAglutina    	:= .F. //N�o tem esse pergunte no FIN330
			lDigita   		:= MV_PAR07 == 1
			
			If !MaIntBxCR(3,aRecSE1,,aRecRA,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.},,,,,dDatabase )
				Help("TA042COMRA",1,"HELP","TA042COMRA",STR0091+CRLF+STR0092,1,0) //"N�o foi poss�vel a compensa��o"###" do titulo do adiantamento"
				lRet := .F.
			Else
				For nX := 1 to len(aRecRA)
					SE1->(DbGoTo(aRecRA[nX]))
					
					aAdd( aLog, { "G8Y_FILREF"	, xFilial( 'SE1' )	} 	)
					aAdd( aLog, { "G8Y_TPDOC"	, "A"				}	)
					aAdd( aLog, { "G8Y_DOC"		, SE1->E1_NUM		}	)
					aAdd( aLog, { "G8Y_NUM"		, SE1->E1_NUM		}	)
					aAdd( aLog, { "G8Y_SERIE"	, SE1->E1_PREFIXO 	}	)
					aAdd( aLog, { "G8Y_PREFIX"	, SE1->E1_PREFIXO	}	)
					aAdd( aLog, { "G8Y_PARCEL"	, SE1->E1_PARCELA	}	)
					aAdd( aLog, { "G8Y_FORNEC"	, cFornece			}	)
					aAdd( aLog, { "G8Y_LOJA"	, cLoja				}	)
					aAdd( aLog, { "G8Y_TIPO"	, "RA"				}	)
					aAdd( aLog, { "G8Y_IDDOC"	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1'))) 	}	)
					aAdd( aLog, { "G8Y_VALOR"	, SE1->E1_VALOR		}	)
				Next nX
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAux)
	TURXNIL(aRecSE1)
	TURXNIL(aRecRA)
Return lRet

/*/{Protheus.doc} TA042ComPA
Fun��o para compensa��o de t�tulos a pagar gerados via antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param cFatura, character, C�digo da fatura
@param cFornece, character, C�digo do fornecedor
@param cLoja, character, C�digo da loja
@param cTpFat, character, tipo da fatura [1] - A�rea, [2] - Terrestre
/*/
Function TA042ComPA( cFatura, cConcil, cFornece, cLoja, cTpFat, aLog )
	Local aArea		:= GetArea()
	Local aAux		:= {}
	Local aRecSE2	:= {}
	Local aRecPA	:= {}
	Local lRet 		:= .T.
	Local nX		:= 0

	Local lContabiliza 	:= .F.
	Local lAglutina   	:= .F.
	Local lDigita   	:= .F.
	
	Default aLog := {}

	//Verifica se existe recebimento antecipado para esta fatura
	G8X->(dbSetOrder(1))
	If G8X->( dbSeek( xFilial('G8X') + cTpFat + cFatura + cFornece + cLoja ) )

		//Obtem os dados do PA
		SE2->( dbSetOrder(1) )
		If SE2->( dbSeek(xFilial('SE2') + G8X->(G8X_PREFIX + G8X_NUM + G8X_PARCEL + G8X_TIPO))) .And. SE2->E2_SALDO > 0
			aAdd( aRecPA, SE2->( RecNo() ) )
		EndIf

		//Obtem o Recno de t�tulos a receber
		If cTpFat == '1'
			aAux := TA042GetTit( cFatura, '1', cFornece, cLoja, '2' )
		Else
			aAux := TA042GetTit( cConcil, '2', cFornece, cLoja, '2' )
		EndIf

		SE2->( dbSetOrder( 1 ) )
		For nX := 1 to len(aAux)
			If SE2->( dbSeek( aAux[nX] ) ) .And. SE2->E2_SALDO > 0
				aAdd( aRecSE2, SE2->( RecNo() ) )
			EndIf
		Next

		If len( aRecPA ) > 0 .And. len( aRecSE2 ) > 0
			PERGUNTE("AFI340",.F.)
			lContabiliza := MV_PAR11 == 1
			lAglutina    := MV_PAR08 == 1
			lDigita   	 := MV_PAR09 == 1

			If !MaIntBxCP(2,aRecSE2,,aRecPA,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.},,,,,dDatabase)
				Help("TA042COMPA",1,"HELP","TA042COMPA",STR0091+CRLF+STR0092,1,0) //"N�o foi poss�vel a compensa��o"###" do titulo do adiantamento"
				lRet := .F.
			Else
				For nX := 1 to len(aRecPA)
					SE2->(DbGoTo(aRecPA[nX]))
					
					aAdd( aLog, { "G8Y_FILREF"	, xFilial( 'SE2' )	} 	)
					aAdd( aLog, { "G8Y_TPDOC"	, "9"				}	)
					aAdd( aLog, { "G8Y_DOC"		, SE2->E2_NUM		}	)
					aAdd( aLog, { "G8Y_NUM"		, SE2->E2_NUM		}	)
					aAdd( aLog, { "G8Y_SERIE"	, SE2->E2_PREFIXO 	}	)
					aAdd( aLog, { "G8Y_PREFIX"	, SE2->E2_PREFIXO	}	)
					aAdd( aLog, { "G8Y_PARCEL"	, SE2->E2_PARCELA	}	)
					aAdd( aLog, { "G8Y_FORNEC"	, cFornece			}	)
					aAdd( aLog, { "G8Y_LOJA"	, cLoja				}	)
					aAdd( aLog, { "G8Y_TIPO"	, "PA"				}	)
					aAdd( aLog, { "G8Y_IDDOC"	, FINGRVFK7('SE2',&(TurKeyTitulo('SE2'))) 	}	)
					aAdd( aLog, { "G8Y_VALOR"	, SE2->E2_VALOR		}	)
				Next nX
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
	TURXNIL(aAux)
	TURXNIL(aRecSE2)
	TURXNIL(aRecPA)
Return lRet

/*/{Protheus.doc} TA042EsCom
Fun��o para estorno da compensa��o de t�tulos a pagar gerados via antecipa��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param cFatura, character, C�digo da fatura
@param cFornece, character, C�digo do fornecedor
@param cLoja, character, C�digo da loja
@param cTpFat, character, tipo da fatura [1] - A�rea, [2] - Terrestre
/*/
Function TA042EsCom( cFatura, cFornece, cLoja, cTpFat )
	Local aArea			:= GetArea()
	Local lRet 			:= .T.
	Local lContabiliza 	:= .F.
	Local lAglutina   	:= .F.
	Local lDigita   	:= .F.

	Private lMsErroAuto	:= .F.

	Pergunte('AFI340',.F.)

	lContabiliza := MV_PAR11 == 1
	lAglutina    := MV_PAR08 == 1
	lDigita   	 := MV_PAR09 == 1

	//Verifica se existe recebimento antecipado para esta fatura
	G8X->(dbSetOrder(1))
	If G8X->( dbSeek( xFilial('G8X') + cTpFat + cFatura + cFornece + cLoja ) )
		If G8X->G8X_TIPOTI == '1' // PA

			If SE2->(dbSeek(xFilial('SE1') + G8X->G8X_PREFIX + G8X->G8X_NUM + G8X->G8X_PARCEL + G8X->G8X_TIPO + G8X->G8X_FORNEC + G8X->G8X_LOJA))
				Fina340(5,.T.)
			Else
				lRet := .F.
			EndIf

		//Estorno RA
		Else
			If SE1->(dbSeek(xFilial('SE1') + G8X->G8X_PREFIX + G8X->G8X_NUM + G8X->G8X_PARCEL + G8X->G8X_TIPO))
				lRet := Fina330(5,.T.)
			Else
				lRet := .F.
			EndIf
		EndIf
	EndIf

	RestArea(aArea)

	TURXNIL(aArea)
Return lRet

/*/{Protheus.doc} TA042GetTit
Fun��o para obter os t�tulos a pagar/receber gerados na efetiva��o
@type function
@author Anderson Toledo
@since 24/03/2016
@version 1.0
@param cCodigo, character, C�digo da fatura para cOrigem == '1' ou c�digo da concilia��o para cOrigem == '2'
@param cOrigem, characeter, Origem da G8Y: [1] - A�reo, [2] - Terrestre
@param cFornece, character, Fornecedor da fatura
@param cLoja, character, Loja da fatura
@param cTipo, character, Tipo de t�tulos 1 - T�tulos a receber; 2 - T�tulos a pagar
/*/
Function TA042GetTit( cCodigo, cOrigem, cFornece, cLoja, cTipo, lSaldo, lOtherFil )
	Local aArea		:= GetArea()
	Local cAlias 	:= GetNextAlias()
	Local cExpCod	:= ""
	Local cFilBkp	:= cFilAnt
	Local aRet		:= {}

	DEFAULT lSaldo 		:= .F.
	DEFAULT lOtherFil	:= .F.

	If cOrigem == '1'
		cExpCod = "% G8Y_FATURA = '"+ AllTrim( cCodigo ) +"' %"
	Else
		cExpCod = "% G8Y_CONCIL = '"+ AllTrim( cCodigo ) +"' %"
	EndIf

	If cTipo == '1' //Titulos a receber

		BeginSql Alias cAlias
			SELECT	G8Y_PREFIX,
					G8Y_NUM,
					G8Y_PARCEL,
					G8Y_TIPO,
					G8Y_TPDOC,
					G8Y_FILREF
			FROM %Table:G8Y%
			WHERE %Exp:cExpCod%
				AND G8Y_FORNEC = %Exp:cFornece%
				AND G8Y_LOJA = %Exp:cLoja%
				AND ( G8Y_TPDOC = '1' OR G8Y_TPDOC = '4' OR G8Y_TPDOC = '6')
				AND G8Y_TPFAT = %Exp:cOrigem%
				AND G8Y_FILIAL = %xFilial:G8Y%
				AND %notDel%
		EndSql

		While (cAlias)->( !EOF() )
			If lOtherFil
				cFilAnt := (cAlias)->G8Y_FILREF
			EndIf

			If (cAlias)->G8Y_TPDOC == "1" .OR. (cAlias)->G8Y_TPDOC == "6" //Contas a Receber e transferencias de t�tulos
				SE1->( dbSetOrder(1) )
				If SE1->( dbSeek( xFilial("SE1") + (cAlias)->( G8Y_PREFIX + G8Y_NUM + G8Y_PARCEL + G8Y_TIPO ) ) )
					If (!lSaldo .Or. SE1->E1_SALDO > 0) .And. ( !lOtherFil .Or. xFilial( 'SE1', SE1->E1_FILIAL ) <> xFilial( 'SE1', cFilBkp ) )
						AAdd(aRet, SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO)
					EndIf
				EndIf

			Else //T�tulos gerados com nota fiscal de saida
				SE1->( dbSetOrder(1) )
				SE1->( dbSeek( xFilial("SE1") + (cAlias)->( G8Y_PREFIX + G8Y_NUM  ) ) )
				While SE1->( !EOF() ) .And. SE1->( E1_FILIAL + E1_PREFIXO + E1_NUM) == xFilial("SE1") + (cAlias)->( G8Y_PREFIX + G8Y_NUM  )
					If ( !lSaldo .Or. SE1->E1_SALDO > 0 ) .And. ;
					   !( SE1->E1_TIPO $ MVIRABT+"/"+MVCSABT+"/"+MVCFABT+"/"+MVPIABT+"/"+MVABATIM+"/"+MVRECANT ) .And.;
					   ( !lOtherFil .Or. xFilial( 'SE1', SE1->E1_FILIAL ) <> xFilial( 'SE1', cFilBkp ) )
						AAdd(aRet, SE1->E1_FILIAL + SE1->E1_PREFIXO + SE1->E1_NUM + SE1->E1_PARCELA + SE1->E1_TIPO)
					EndIf
					SE1->( dbSkip() )
				EndDo
			EndIf
			
			(cAlias)->( dbSkip() )
		EndDo
		
	Else //T�tulos a Pagar

		BeginSql Alias cAlias
			SELECT	G8Y_PREFIX,
					G8Y_SERIE,
					G8Y_NUM,
					G8Y_DOC,
					G8Y_PARCEL,
					G8Y_TIPO,
					G8Y_FORNEC,
					G8Y_LOJA,
					G8Y_TPDOC,
					G8Y_FILREF
			FROM %Table:G8Y%
			WHERE %Exp:cExpCod%
				AND G8Y_FORNEC = %Exp:cFornece%
				AND G8Y_LOJA = %Exp:cLoja%
				AND ( G8Y_TPDOC = '2' OR G8Y_TPDOC = '3' OR G8Y_TPDOC = '8')
				AND G8Y_TPFAT = %Exp:cOrigem%
				AND G8Y_FILIAL = %xFilial:G8Y%
				AND %notDel%
		EndSql

		While (cAlias)->( !EOF() )
			
			//Pesquisa por t�tulos gerados atrav�s de nota fiscal de entrada
			If  (cAlias)->G8Y_TPDOC == "3"
				SE2->( dbSetOrder(6) )
				SE2->( dbSeek( xFilial("SE2", (cAlias)->G8Y_FILREF ) + (cAlias)->( G8Y_FORNEC + G8Y_LOJA  + G8Y_SERIE + G8Y_DOC  ) ) )
				While SE2->(!EOF()) .And. SE2->(E2_FILIAL + E2_FORNECE + E2_LOJA + E2_PREFIXO + E2_NUM) == xFilial("SE2") + (cAlias)->(G8Y_FORNEC + G8Y_LOJA + G8Y_SERIE + G8Y_DOC)
					If !lSaldo .Or. SE2->E2_SALDO > 0
						AAdd(aRet, SE2->E2_FILIAL + SE2->E2_PREFIXO + SE2->E2_NUM + SE2->E2_PARCELA + SE2->E2_TIPO	+ SE2->E2_FORNECE + SE2->E2_LOJA)
					EndIf
					SE2->( dbSkip() )
				EndDo
			Else
				SE2->( dbSetOrder(1) )
				If SE2->( dbSeek( xFilial("SE2", (cAlias)->G8Y_FILREF ) + (cAlias)->( G8Y_PREFIX + G8Y_NUM + G8Y_PARCEL + G8Y_TIPO + G8Y_FORNEC + G8Y_LOJA ) ) )
					If !lSaldo .Or. SE2->E2_SALDO > 0
						AAdd(aRet, SE2->E2_FILIAL + SE2->E2_PREFIXO	+ SE2->E2_NUM + SE2->E2_PARCELA + SE2->E2_TIPO + SE2->E2_FORNECE + SE2->E2_LOJA)
					EndIf
				EndIf
			EndIf
			
			(cAlias)->( dbSkip() )
		EndDo
	EndIf
	(cAlias)->( dbCloseArea() )

	cFilAnt := cFilBkp

	RestArea(aArea)
	
	TURXNIL(aArea)
Return aRet

/*/{Protheus.doc} TA042TrfTi
Fun��o para transfer�ncia de t�tulos a receber
@type function
@author Anderson Toledo
@since 01/04/2016
@version 1.0
@param cChaveSE1, character, Chave do t�tulo no indice 1 da tabela SE1
@param cCliente, character, C�digo do cliente para qual o t�tulo ser� transferido
@param cLoja, character, Loja do cliente para qual o t�tulo ser� transferido
/*/
Function TA042TrfTi( cFilDeb, cChaveSE1, cCliente, cLoja, cFornRep, cLojaRep, aLog )
	Local aArea		:= GetArea()
	Local aVetor	:= {}
	Local cTitPai	:= ''
	Local cTipo		:= ''
	Local cFilPai	:= ''
	Local cNumSol	:= ''
	Local lRet		:= .T.
	Local nTamLog	:= 0

	//Variaveis que indicam se haver� troca de prefixo, conforme FINA630
	Local lExcSX5	:= FWModeAccess("SX5",3) == "E"

	DEFAULT cFilDeb	:= ''

	Private lMsErroAuto 	:= .F.
	Private INCLUI		:= .T.

	SE1->( dbSetOrder(1) )
	If SE1->( dbSeek( cChaveSE1 ) )
		If Empty( cFilDeb )
			cFilDeb := SE1->E1_FILIAL
		EndIf

		aVetor :={{"E6_FILDEB" , cFilDeb , Nil},;
				  {"E6_CLIENTE", cCliente, Nil},;
				  {"E6_LOJA"   , cLoja   , Nil},;
				  {"AUTHISTDEB", STR0094 , Nil}	}

		MSExecAuto({|x,y| Fina620(x,y)},aVetor,3) //Inclusao de Solicita��o de transferencia
		If lMsErroAuto
			MostraErro()
			lRet := .F.
		Else
			cTitPai := AllTrim( SE1->(E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_CLIENTE+E1_LOJA) )
			cTipo	:= SE1->E1_TIPO
			cNumSol	:= SE1->E1_NUMSOL
			aVetor :={{"E6_NUMSOL"	,SE1->E1_NUMSOL,Nil}}

			MSExecAuto({|x,y| Fina630(x,y)},aVetor,3) //Aprova��o Automatica da transferencia
			If lMsErroAuto
				MostraErro()
				lRet := .F.
			EndIf
		EndIf
	EndIf

	//Cria o vetor com o log dos t�tulos gerados
	If lRet
		SE1->( dbSetOrder(1) )
		If SE1->( dbSeek( cChaveSE1 ) )
			aAdd( aLog, {} )
			nTamLog := len( aLog )

			aAdd( aLog[nTamLog], { "G8Y_FILREF"		, SE1->E1_FILIAL		} )
			aAdd( aLog[nTamLog], { "G8Y_TPDOC"		, "5"					} )
			aAdd( aLog[nTamLog], { "G8Y_DOC"		, SE1->E1_NUM			} )
			aAdd( aLog[nTamLog], { "G8Y_NUM"		, SE1->E1_NUM			} )
			aAdd( aLog[nTamLog], { "G8Y_SERIE"		, SE1->E1_PREFIXO		} )
			aAdd( aLog[nTamLog], { "G8Y_PREFIX"		, SE1->E1_PREFIXO		} )
			aAdd( aLog[nTamLog], { "G8Y_PARCEL"		, SE1->E1_PARCELA		} )
			aAdd( aLog[nTamLog], { "G8Y_FORNEC"		, cFornRep				} )
			aAdd( aLog[nTamLog], { "G8Y_LOJA"		, cLojaRep				} )
			aAdd( aLog[nTamLog], { "G8Y_TIPO"		, SE1->E1_TIPO			} )
			aAdd( aLog[nTamLog], { "G8Y_IDDOC"		, FINGRVFK7('SE1',&(TurKeyTitulo('SE1'))) } )

			SE6->( dbSetOrder(3) )
			If SE6->(DbSeek(xFilial( 'SE6' ) + cNumSol ))
				SE1->( dbSetOrder(1) )
				If SE1->( dbSeek( xFilial('SE1', SE6->E6_FILDEB ) + SE6->( E6_PREFIXO + E6_NUM + E6_PARCDES + E6_TIPO ) ) )
					aAdd( aLog, {} )
					nTamLog := len( aLog )

					aAdd( aLog[nTamLog], { "G8Y_FILREF"	, xFilial( 'SE1' )		} )
					aAdd( aLog[nTamLog], { "G8Y_TPDOC"	, "6" 					} )
					aAdd( aLog[nTamLog], { "G8Y_DOC"	, SE1->E1_NUM			} )
					aAdd( aLog[nTamLog], { "G8Y_NUM"	, SE1->E1_NUM			} )
					aAdd( aLog[nTamLog], { "G8Y_SERIE"	, SE1->E1_PREFIXO		} )
					aAdd( aLog[nTamLog], { "G8Y_PREFIX"	, SE1->E1_PREFIXO		} )
					aAdd( aLog[nTamLog], { "G8Y_PARCEL"	, SE1->E1_PARCELA		} )
					aAdd( aLog[nTamLog], { "G8Y_FORNEC"	, cFornRep				} )
					aAdd( aLog[nTamLog], { "G8Y_LOJA"	, cLojaRep				} )
					aAdd( aLog[nTamLog], { "G8Y_TIPO"	, SE1->E1_TIPO			} )
					aAdd( aLog[nTamLog], { "G8Y_IDDOC"	, FINGRVFK7('SE1',&(TurKeyTitulo('SE1'))) } )
				Else
					lRet := .F.
				EndIf
			EndIf
		EndIf
	EndIf

	RestArea(aArea)
	
	TURXNIL(aArea)
	TURXNIL(aVetor)
Return lRet

/*/{Protheus.doc} TA042EstTr
Fun��o para estorno da transfer�ncia do cliente do t�tulo
@type function
@author Anderson Toledo
@since 01/04/2016
@version 1.0
@param cPrefixo, character, Prefixo do t�tulo a receber original
@param cNum, character, N�mero do t�tulo a receber original
@param cParcel, character, Parcela do t�tulo a receber original
@param cTipo, character, Tipo do t�tulo a receber original
/*/
Function TA042EstTr( cPrefixo, cNum, cParcel, cTipo, cFilRef )
	Local aVetor	:= {}
	Local cBkpFil	:= cFilAnt
	Local lRet 	    := .T.

	Private lMsErroAuto	:= .F.

	DEFAULT cFilRef := cFilAnt

	//Posiciona na filial de solicita��o de inclus�o apenas para obter o numero da solicita��o
	cFilAnt := cFilRef

	SE1->( dbSetOrder(1) )
	If SE1->( dbSeek( xFilial( "SE1" ) + cPrefixo + cNum + cParcel + cTipo ) ) .And. !Empty(SE1->E1_NUMSOL)

		aVetor :={{"E6_NUMSOL",SE1->E1_NUMSOL,Nil}}

		//Retorna para a filial da efetiva��o, precisa estar nessa filial para a rotina encontrar a refer�ncia na SE6
		cFilAnt := cBkpFil
		
		MSExecAuto({|x,y| Fina630(x,y)},aVetor,6) //Aprova��o Automatica da transferencia
		If lMsErroAuto
			lRet 	    := .F.
			MostraErro()
		EndIf
	EndIf

	cFilAnt := cBkpFil
	TURXNIL(aVetor)
Return lRet

/*/{Protheus.doc} TA042DaTit
Fun��o para obter os itens para t�tulos a pagar e receber
@type function
@author Anderson Toledo
@since 03/05/2016
@version 1.0
/*/
Function TA042DaTit( oModel, aAbatP, aDadosTitP, aAbatR, aDadosTitR, lIfOperCar, lGeraNFE )
	Local aLines		:= FwSaveRows( oModel )
	Local cFilRef		:= ""
	Local nX			:= 0
	Local nY			:= 0
	Local nPos			:= 0
	Local nPosPrd		:= 0
	Local oModelG3R		:= oModel:GetModel( "G3R_ITENS" )
	Local oModelG48B	:= oModel:GetModel( "G48B_ITENS" )
	Local oModelG4CB	:= oModel:GetModel( "G4CB_ITENS" )

	DEFAULT aAbatP		:= {}
	DEFAULT aDadosTitP	:= {}
	DEFAULT aAbatR		:= {}
	DEFAULT aDadosTitR	:= {}
	DEFAULT lIfOperCar	:= .F.
	DEFAULT lGeraNFE	:= .F.

	For nX	:= 1 to oModelG3R:Length()
		oModelG3R:GoLine( nX )

		cFilRef := T42FilRef( oModel )

		If oModelG3R:GetValue("G3R_STATUS") $ "2|4"
			Loop
		EndIf

		For nY := 1 to oModelG4CB:Length()
			oModelG4CB:GoLine( nY )

			//Verifica se itens para a operadora de cart�o ser�o considerados no valor total
			If !Empty( oModelG4CB:GetValue("G4C_CARTUR") ) .And. !lIfOperCar
				Loop
			EndIf

			If  oModelG4CB:GetValue( "G4C_TIPO" ) $ '1|2' .And. oModelG4CB:GetValue( "G4C_STATUS" ) == "1"
				//Venda
				If oModelG4CB:GetValue( "G4C_TIPO" ) == '1'
					//Venda a pagar
					If oModelG4CB:GetValue( "G4C_PAGREC" ) == '1'
						If Empty(  oModelG4CB:GetValue('G4C_APLICA')  )
							cNatureza := oModelG3R:GetValue('G3R_NATURE')
						Else
							oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
							cNatureza := oModelG48B:GetValue('G48_NATURE')
						EndIf

						If lGeraNFE
							nPos := aScan( aDadosTitP, {|x| x[1] == cFilRef .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )

							If nPos == 0
								aAdd( aDadosTitP, { cFilRef, cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ), {} } )
								nPos := len( aDadosTitP)
							EndIf

							nPosPrd := aScan( aDadosTitP[nPos][5], {|x| x[1] == oModelG4CB:GetValue( "G4C_CODPRO" ) .And. x[2] == oModelG3R:GetValue( "G3R_TES" ) } )
							If nPosPrd == 0
								aAdd( aDadosTitP[nPos][5],{oModelG4CB:GetValue( "G4C_CODPRO" ), oModelG3R:GetValue( "G3R_TES" ), 0} )
								nPosPrd := len( aDadosTitP[nPos][5] )
							EndIf

							aDadosTitP[nPos][5][nPosPrd][3] += oModelG4CB:GetValue( "G4C_VALOR" )

						Else
							nPos := aScan( aDadosTitP, {|x| x[1] == cFilRef .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) )  } )
							If nPos == 0
								aAdd( aDadosTitP, { cFilRef, cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
								nPos := len( aDadosTitP)
							EndIf

							aDadosTitP[nPos][3] += oModelG4CB:GetValue( "G4C_VALOR" )
						EndIf

						//Se � venda a receber ent�o a origem � de acerto, deve abater o valor do item a pagar
					Else
						If Empty(  oModelG4CB:GetValue('G4C_APLICA')  )
							cNatureza := oModelG3R:GetValue('G3R_NATURE')
						Else
							oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
							cNatureza := oModelG48B:GetValue('G48_NATURE')
						EndIf

						nPos := aScan( aAbatP, {|x|, x[1] == oModelG3R:GetValue('G3R_MSFIL') .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
						If nPos== 0
							AAdd(aAbatP, { oModelG3R:GetValue('G3R_MSFIL'), cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
							nPos:= Len(aAbatP)
						EndIf

						aAbatP[nPos][3] += oModelG4CB:GetValue( "G4C_VALOR" )

					EndIf

					//Reembolso
				Else

					If Empty(  oModelG4CB:GetValue('G4C_APLICA')  )
						cNatureza := oModelG3R:GetValue('G3R_NATURE')
					Else
						oModelG48B:SeekLine( { {'G48_APLICA', oModelG4CB:GetValue( "G4C_APLICA" ) } } )
						cNatureza := oModelG48B:GetValue('G48_NATURE')
					EndIf

					nPos := aScan( aDadosTitR, {|x|, x[1] == oModelG3R:GetValue('G3R_MSFIL') .And. x[2] == cNatureza .And. x[4] == Val( oModelG4CB:GetValue( "G4C_MOEDA" ) )  } )
					If nPos == 0
						AAdd(aDadosTitR, { oModelG3R:GetValue('G3R_MSFIL'), cNatureza, 0, Val( oModelG4CB:GetValue( "G4C_MOEDA" ) ) } )
						nPos := Len(aDadosTitR)
					EndIf

					//Abat. de receitas a pagar
					If oModelG4CB:GetValue("G4C_PAGREC") == "1"
						aDadosTitR[nPos][3] -= oModelG4CB:GetValue( "G4C_VALOR" )

						//Abat. de receitas a receber ( Acertos )
					Else
						aDadosTitR[nPos][3] += oModelG4CB:GetValue( "G4C_VALOR" )

					EndIf

				EndIf

			EndIf

		Next

	Next

	//+-------------------------------------------------------------------------------------------
	//|Abaixo � realizado o abatimento dos t�tulos avulso. Positivo (aTitAvulso) x Negativo(aAbat)
	//+-------------------------------------------------------------------------------------------
	If lGeraNFE
		TA42CaAbNF( aAbatP, aDadosTitP )
	Else
		TA42CalcAb( aAbatP, aDadosTitP )
	EndIf

	TA42CalcAb( aAbatR, aDadosTitR )
	FwRestRows( aLines )

	TURXNIL(aLines)
Return

/*/{Protheus.doc} TA042NumTi
Fun��o para obter o n�mero do t�tulo a ser gerado, de acordo com a tabela e prefixo
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Static Function TA042NumTi( cTabela, cPrefix )
	Local aArea		:= (cTabela)->( GetArea() )
	Local cNumTit	:= ""
	Local cField	:= IIF( cTabela == 'SE1', 'E1_NUM', 'E2_NUM' )

	cNumTit := GetSxeNum(cTabela, cField, cPrefix)

	( cTabela )->( dbSetOrder(1) )
	While ( cTabela )->( dbSeek( xFilial( cTabela ) + cPrefix + cNumTit ) )
		ConfirmSX8()
		cNumTit := GetSxeNum(cTabela, cField, cPrefix)
	EndDo
	
	RestArea(aArea)

	TURXNIL(aArea)
Return cNumTit

/*/{Protheus.doc} TA42AddLog
Fun��o para adicionar o log ao modelo da tabela G8Y
@type function
@author Anderson Toledo
@since 10/11/2016
@version 1.0
/*/
Function TA42AddLog(oMdlG8Y, aDados, cConcil, cFatura, cTipo)
	Local cIdModel	:= 'G8Y_MASTER'
	Local lRet		:= .T.
	Local nX		:= 0
	Local nY		:= 0
	Local nPos		:= 0
	Local oMdlBkp	:= Nil
	Local oStru		:= Nil

	If Len(aDados) > 0
	
		oMdlBkp := FwModelActive()
		
		oMdlG8Y:SetOperation( MODEL_OPERATION_INSERT )
		oMdlG8Y:Activate()
	
		oStru := oMdlG8Y:GetModel(cIdModel):GetStruct()
	
		oMdlG8Y:SetValue( cIdModel, "G8Y_FATURA"	, cFatura	)
		oMdlG8Y:SetValue( cIdModel, "G8Y_TPFAT"	, cTipo	)
		oMdlG8Y:SetValue( cIdModel, "G8Y_CONCIL"	, cConcil	)
	
		For nX := 1 To Len( aDados )
			// Verifica se os campos passados existem na estrutura do modelo
			If oStru:HasField( aDados[nX][1] )
				If !( lRet := oMdlG8Y:SetValue(cIdModel, aDados[nX][1], aDados[nX][2] ) )
					lRet := .F.
					Exit
				EndIf
			EndIf
		Next
	
		If oMdlG8Y:VldData()
			oMdlG8Y:CommitData()
		Else
			TurGetErrorMsg( oMdlG8Y )
			lRet := .F.
		EndIf
	
		oMdlG8Y:DeActivate()
	
		FwModelActive( oMdlBkp )
	EndIf

Return lRet

/*/{Protheus.doc} GetTitFTF
Adiciona ao array informa��es do t�tulo
@author osmar.junior
@since 12/04/2018
@version 1.0
@return ${return}, Retorna verdadeiro caso encontre o(s) t�tulos
@param cPrefixo, characters, Prefixo do T�tulo
@param cTipo, characters, Tipo do T�tulo (Ex:FTF)
@param cNumTit, characters, N�mero do T�tulo
@param cFornece, characters, C�digo do fornecedor
@param cLoja, characters, Loja do fornecedor
@param aLog, array, Array que armazenar� o(s) t�tulo(s), caso encontrad(s)
@type function
/*/
Function GetTitFTF(cPrefixo, cTipo, cNumTit, cFornece, cLoja, aLog)
	Local aArea     := GetArea()
	Local aAreaSE2	:= SE2->( GetArea() )
	Local cAliasAux	:= ""
	Local lRet		:= .F.
	Local nTamLog	:= 0

	cAliasAux := GetNextAlias()
		BeginSql Alias cAliasAux
			SELECT E2_FILIAL,
					E2_PREFIXO,
					E2_NUM,
					E2_PARCELA,
					E2_TIPO,
					E2_FORNECE,
					E2_LOJA
				FROM %Table:SE2%
				WHERE E2_FILIAL = %xFilial:SE2%
					AND E2_PREFIXO = %Exp:cPrefixo%
					AND E2_NUM	= %Exp:cNumTit%
					AND E2_TIPO = %Exp:cTipo%
					AND E2_FORNECE = %Exp:cFornece%
					AND E2_LOJA = %Exp:cLoja%
					AND %notDel%
		EndSql

		SE2->( dbSetOrder(1) )
		While (cAliasAux)->( !EOF() )
			If SE2->( dbSeek( (cAliasAux)->( E2_FILIAL + E2_PREFIXO + E2_NUM + E2_PARCELA + E2_TIPO + E2_FORNECE + E2_LOJA ) ) )
				lRet := .T.
				//Criar o vetor com o log da opera��o
				aAdd( aLog, {} )
				nTamLog := Len( aLog )
				
				aAdd( aLog[nTamLog], { "G8Y_FILREF"	, xFilial( 'SE2' )	} 	)
				aAdd( aLog[nTamLog], { "G8Y_TPDOC"	, "8"				}	)
				aAdd( aLog[nTamLog], { "G8Y_DOC"		, SE2->E2_NUM		}	)
				aAdd( aLog[nTamLog], { "G8Y_NUM"		, SE2->E2_NUM		}	)
				aAdd( aLog[nTamLog], { "G8Y_SERIE"	, SE2->E2_PREFIXO 	}	)
				aAdd( aLog[nTamLog], { "G8Y_PREFIX"	, SE2->E2_PREFIXO	}	)
				aAdd( aLog[nTamLog], { "G8Y_PARCEL"	, SE2->E2_PARCELA	}	)
				aAdd( aLog[nTamLog], { "G8Y_FORNEC"	, SE2->E2_FORNECE	}	)
				aAdd( aLog[nTamLog], { "G8Y_LOJA"	, SE2->E2_LOJA		}	)
				aAdd( aLog[nTamLog], { "G8Y_TIPO"	, "FTF"				}	)
				aAdd( aLog[nTamLog], { "G8Y_IDDOC"	, FINGRVFK7('SE2',&(TurKeyTitulo('SE2'))) 	}	)
				aAdd( aLog[nTamLog], { "G8Y_VALOR"	, SE2->E2_VALOR		}	)
			Else
				lRet := .F.	
			EndIf
			(cAliasAux)->( dbSkip() )
		EndDo
		(cAliasAux)->( dbCloseArea() )
		
		RestArea(aAreaSE2)
		RestArea(aArea)

Return lRet


/*/{Protheus.doc} LogConcilT
(long_description)
@type function
@author osmar.junior
@since 07/05/2018
@version 1.0
@param _nHandTXT, ${param_type}, (Handle de Arquivo de Log)
@param cFileG8C, character, (Nome do arquivo de Log)
@param nTipo, num�rico, (Tipo :1=cria��o do arquivo;2=Atribui��o de texto no log;3=Fecha arquivo de log)
@param cTexto, character, (Texto para atribui��o no arquivo de log)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/Static Function LogConcilT( _nHandTXT, cFileG8C, nTipo, cTexto )
Local lLogConcT := SuperGetMV("MV_TURLGT", , .F.)
Default cTexto	:= ''

	If lLogConcT
	
		If nTipo == 1 			//Cria��o do Arquivo de Log
		
			_nHandTXT := FCreate(cFileG8C)	
			If _nHandTXT == -1
				MsgStop("Erro na criacao do arquivo de log. Contate o administrador do sistema")
			Else
				FWrite(_nHandTXT, 'Cria��o do arquivo: '+Dtos( Date() ) +" - "+ Time() +CRLF )
			EndIf		
		
		ElseIf	nTipo == 2	//Adiciona fluxo de execu��o mop arquivo de log
		
			FWrite(_nHandTXT, Time()+" - "+cTexto)
			
		ElseIf	nTipo == 3	//fecha Arquivo
		
			FWrite(_nHandTXT, Time()+" - " + "Fecha arquivo.")
			FClose(_nHandTXT)
			
		EndIf
		
	EndIf
		
Return 