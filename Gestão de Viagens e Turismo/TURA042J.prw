#INCLUDE "TURA042.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"

Static Function ModelDef()
	Local oModel	 := Nil
	Local oStruG3P	 := FwFormStruct(1, "G3P", , .F.)			//Registro de venda
	Local oStruG3R	 := FwFormStruct(1, "G3R", , .F.)			//Documento de reserva
	Local oStruG3Q	 := FwFormStruct(1, "G3Q", , .F.)			//Item de Venda
	Local oStruG48A	 := FwFormStruct(1, "G48", , .F.)			//Acordos Cliente 
	Local oStruG4CA	 := FwFormStruct(1, "G4C", , .F.)			//Itens Financeiros 
	Local oStruG9KA	 := FwFormStruct(1, "G9K", , .F.)			//Demonstrativo Financeiro - Cliente
	Local oStruG9KB	 := FwFormStruct(1, "G9K", , .F.)			//Demonstrativo Financeiro - Fornecedor
	Local oStruG4D	 := FwFormStruct(1, "G4D", , .F.)			//Dados cart�o
	Local oStruG3S	 := FwFormStruct(1, "G3S", , .F.) 			//Passageiros
	Local oStruG4B	 := FwFormStruct(1, "G4B", , .F.) 			//Informa��es Adicionais
	Local oStruG3U	 := FwFormStruct(1, "G3U", , .F.) 			//2 - Hotel(G3U)
	Local oStruG3V	 := FwFormStruct(1, "G3V", , .F.) 			//3 - Carro (G3V)
	Local oStruG3W	 := FwFormStruct(1, "G3W", , .F.) 			//4 - Rodovi�rio (G3W)
	Local oStruG3Y	 := FwFormStruct(1, "G3Y", , .F.) 			//5 - Cruzeiro (G3Y)
	Local oStruG3X	 := FwFormStruct(1, "G3X", , .F.) 			//6 - Trem (G3X)
	Local oStruG42	 := FwFormStruct(1, "G42", , .F.) 			//7 - Visto (G42)
	Local oStruG41	 := FwFormStruct(1, "G41", , .F.) 			//8 - Seguro (G41)
	Local oStruG40	 := FwFormStruct(1, "G40", , .F.) 			//9 - Tour (G40)
	Local oStruG3Z	 := FwFormStruct(1, "G3Z", , .F.) 			//10 - Pacote (G3Z)
	Local oStruG43	 := FwFormStruct(1, "G43", , .F.) 			//11 - Outros (G43)
	Local oStruG44	 := FwFormStruct(1, "G44", , .F.) 			//Tarifas
	Local oStruG46	 := FwFormStruct(1, "G46", , .F.) 			//Taxas
	Local oStruG47	 := FwFormStruct(1, "G47", , .F.) 			//Extras
	Local oStruG48B	 := FwFormStruct(1, "G48", , .F.) 			//Acordos Fornecedores
	Local oStruG49	 := FwFormStruct(1, "G49", , .F.) 			//Impostos
	Local oStruG4CB	 := FwFormStruct(1, "G4C", , .F.) 			//Itens Financeiros - Fornecedor
	Local oStruG4A	 := FwFormStruct(1, "G4A", , .F.) 			//Rateio
	Local oStruG4E	 := FwFormStruct(1, "G4E", , .F.) 			//Reembolso

	//Altera a estrutura obtida atrav�s do FwFormStruct
	TA034StruM(	oStruG3P,oStruG3Q,oStruG3R,oStruG3S,/*oStruG3T*/,oStruG3U,oStruG3V,oStruG3W,oStruG3X,oStruG3Y,/*oStrG3Z*/,oStruG40,oStruG41,oStruG42,oStruG43,;
				oStruG44,/*oStruG45*/,oStruG46,oStruG47,oStruG4A,oStruG4B,oStruG48A,oStruG48B,oStruG49,oStruG4E,oStruG4CA,oStruG4CB, /*oStrEntid*/, oStruG4D )

	oModel := MPFormModel():New( "TURA042J", /*bPreValidacao*/, /*bPosValidacao*/, /*bCommit*/, /*bCancel*/ )

	//Registro de Venda
	oModel:AddFields( "G3P_FIELDS", /*cOwner*/, oStruG3P )

	//Registro de Venda
	oModel:AddGrid( "G3R_ITENS", "G3P_FIELDS", oStruG3R )
	oModel:SetRelation( "G3R_ITENS", { { "G3R_FILIAL", "G3P_FILIAL" }, { "G3R_NUMID", "G3P_NUMID" }, { "G3R_CONORI", "Space(TamSX3('G3R_CONORI')[1])" } }, G3R->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3R_ITENS" ):SetOptional( .T. )

	//Item de Venda
	oModel:AddGrid( "G3Q_ITENS", "G3R_ITENS", oStruG3Q )
	oModel:SetRelation( "G3Q_ITENS", { { "G3Q_FILIAL", "G3R_FILIAL" }, { "G3Q_NUMID", "G3R_NUMID" }, { "G3Q_IDITEM", "G3R_IDITEM" }, { "G3Q_NUMSEQ", "G3R_NUMSEQ" }, { "G3Q_CONORI", "G3R_CONORI" }, { "G3Q_CONINU", "G3R_CONINU" } }, G3Q->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Q_ITENS" ):SetOptional( .T. )

	//Rateio
	oModel:AddGrid( "G4A_ITENS", "G3Q_ITENS", oStruG4A )
	oModel:SetRelation( "G4A_ITENS", { { "G4A_FILIAL", "G3Q_FILIAL" }, { "G4A_NUMID", "G3Q_NUMID" }, { "G4A_IDITEM", "G3Q_IDITEM" }, { "G4A_NUMSEQ", "G3Q_NUMSEQ" }, { "G4A_CLIENT", "G3Q_CLIENT" }, { "G4A_CONORI", "G3Q_CONORI" }, { "G4A_CONINU", "G3Q_CONINU" } }, G4A->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4A_ITENS" ):SetOptional(.T.)

	//Acordos Cliente 
	oModel:AddGrid( "G48A_ITENS", "G3Q_ITENS", oStruG48A )
	oModel:SetRelation( "G48A_ITENS", { { "G48_FILIAL", "G3Q_FILIAL" }, { "G48_NUMID", "G3Q_NUMID" }, { "G48_IDITEM", "G3Q_IDITEM" }, { "G48_NUMSEQ", "G3Q_NUMSEQ" }, { "G48_CONORI", "G3Q_CONORI" }, { "G48_CONINU", "G3Q_CONINU" }, { "G48_CLIFOR", "'1'" }, { 'G48_CODAPU', "''" } }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48A_ITENS" ):SetOptional( .T. )

	//Itens Financeiros 
	oModel:AddGrid( "G4CA_ITENS", "G3Q_ITENS", oStruG4CA )
	oModel:SetRelation( "G4CA_ITENS", { { "G4C_FILIAL", "G3Q_FILIAL" }, { "G4C_NUMID", "G3Q_NUMID" }, { "G4C_IDITEM", "G3Q_IDITEM" }, { "G4C_NUMSEQ", "G3Q_NUMSEQ" }, { "G4C_CONORI", "G3Q_CONORI" }, { "G4C_CONINU", "G3Q_CONINU" }, { "G4C_CLIFOR", "'1'" }, { 'G4C_CODAPU', "''" } }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CA_ITENS" ):SetOptional( .T. )

	//Reembolso
	oModel:AddGrid( 'G4E_ITENS', 'G3R_ITENS', oStruG4E )
	oModel:SetRelation( 'G4E_ITENS', { { 'G4E_FILIAL', 'G3R_FILIAL' }, { 'G4E_NUMID', 'G3R_NUMID' }, { 'G4E_IDITEM', 'G3R_IDITEM' }, { 'G4E_NUMSEQ', 'G3R_NUMSEQ' }, { 'G4E_CONORI', 'G3R_CONORI' }, { 'G4E_CONINU', 'G3R_CONINU' } }, G4E->( IndexKey( 1 ) ) )
	oModel:GetModel( 'G4E_ITENS' ):SetOptional( .T. )

	//Passageiros da reserva
	oModel:AddGrid( "G3S_ITENS", "G3R_ITENS", oStruG3S )
	oModel:SetRelation( "G3S_ITENS", { { "G3S_FILIAL", "G3R_FILIAL" }, { "G3S_NUMID", "G3R_NUMID" }, { "G3S_IDITEM", "G3R_IDITEM" }, { "G3S_NUMSEQ", "G3R_NUMSEQ" }, { "G3S_CONORI", "G3R_CONORI" }, { "G3S_CONINU", "G3R_CONINU" } }, G3S->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3S_ITENS" ):SetOptional( .T. )

	//Dados Adicionais
	oModel:AddGrid("G4B_ITENS", "G3S_ITENS", oStruG4B )
	oModel:SetRelation( "G4B_ITENS", { { "G4B_FILIAL", "G3S_FILIAL" }, { "G4B_NUMID", "G3S_NUMID" }, { "G4B_IDITEM", "G3S_IDITEM" }, { "G4B_NUMSEQ", "G3S_NUMSEQ" }, { "G4B_CLIENT", "G3Q_CLIENT" }, { "G4B_CODPAX", "G3S_CODPAX" }, { "G4B_CONORI", "G3S_CONORI" }, { "G4B_CONINU", "G3S_CONINU" } }, G4B->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4B_ITENS" ):SetOptional( .T. )

	//2 - Hotel(G3U)
	oModel:AddGrid( "G3U_ITENS", "G3Q_ITENS", oStruG3U )
	oModel:SetRelation( "G3U_ITENS", { { "G3U_FILIAL", "G3Q_FILIAL" }, { "G3U_NUMID", "G3Q_NUMID" }, { "G3U_IDITEM", "G3Q_IDITEM" }, { "G3U_NUMSEQ", "G3Q_NUMSEQ" }, { "G3U_CONORI", "G3Q_CONORI" }, { "G3U_CONINU", "G3Q_CONINU" } }, G3U->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3U_ITENS" ):SetOptional( .T. )

	//3 - Carro (G3V)
	oModel:AddGrid( "G3V_ITENS", "G3Q_ITENS", oStruG3V )
	oModel:SetRelation( "G3V_ITENS", { { "G3V_FILIAL", "G3Q_FILIAL" }, { "G3V_NUMID", "G3Q_NUMID" }, { "G3V_IDITEM", "G3Q_IDITEM" }, { "G3V_NUMSEQ", "G3Q_NUMSEQ" }, { "G3V_CONORI", "G3Q_CONORI" }, { "G3V_CONINU", "G3Q_CONINU" } }, G3V->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3V_ITENS" ):SetOptional( .T. )

	//4 - Rodovi�rio (G3W)
	oModel:AddGrid( "G3W_ITENS", "G3Q_ITENS", oStruG3W )
	oModel:SetRelation( "G3W_ITENS", { { "G3W_FILIAL", "G3Q_FILIAL" }, { "G3W_NUMID", "G3Q_NUMID" }, { "G3W_IDITEM", "G3Q_IDITEM" }, { "G3W_NUMSEQ", "G3Q_NUMSEQ" }, { "G3W_CONORI", "G3Q_CONORI" }, { "G3W_CONINU", "G3Q_CONINU" } }, G3W->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3W_ITENS" ):SetOptional( .T. )

	//5 - Cruzeiro (G3Y)
	oModel:AddGrid( "G3Y_ITENS", "G3Q_ITENS", oStruG3Y  )
	oModel:SetRelation( "G3Y_ITENS", { { "G3Y_FILIAL", "G3Q_FILIAL" }, { "G3Y_NUMID", "G3Q_NUMID" }, { "G3Y_IDITEM", "G3Q_IDITEM" }, { "G3Y_NUMSEQ", "G3Q_NUMSEQ" }, { "G3Y_CONORI", "G3Q_CONORI" }, { "G3Y_CONINU", "G3Q_CONINU" } }, G3Y->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Y_ITENS" ):SetOptional( .T. )

	//6 - Trem (G3X)
	oModel:AddGrid( "G3X_ITENS", "G3Q_ITENS", oStruG3X )
	oModel:SetRelation( "G3X_ITENS", { { "G3X_FILIAL", "G3Q_FILIAL" }, { "G3X_NUMID", "G3Q_NUMID" }, { "G3X_IDITEM", "G3Q_IDITEM" }, { "G3X_NUMSEQ", "G3Q_NUMSEQ" }, { "G3X_CONORI", "G3Q_CONORI" }, { "G3X_CONINU", "G3Q_CONINU" } }, G3X->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3X_ITENS" ):SetOptional( .T. )

	//7 - Visto (G42)
	oModel:AddGrid( "G42_ITENS", "G3Q_ITENS", oStruG42 )
	oModel:SetRelation( "G42_ITENS", { { "G42_FILIAL", "G3Q_FILIAL" }, { "G42_NUMID", "G3Q_NUMID" }, { "G42_IDITEM", "G3Q_IDITEM" }, { "G42_NUMSEQ", "G3Q_NUMSEQ" }, { "G42_CONORI", "G3Q_CONORI" }, { "G42_CONINU", "G3Q_CONINU" } }, G42->( IndexKey( 1 ) ) )
	oModel:GetModel( "G42_ITENS" ):SetOptional( .T. )

	//8 - Seguro (G41)
	oModel:AddGrid( "G41_ITENS", "G3Q_ITENS", oStruG41 )
	oModel:SetRelation( "G41_ITENS", { { "G41_FILIAL", "G3Q_FILIAL" }, { "G41_NUMID", "G3Q_NUMID" }, { "G41_IDITEM", "G3Q_IDITEM" }, { "G41_NUMSEQ", "G3Q_NUMSEQ" }, { "G41_CONORI", "G3Q_CONORI" }, { "G41_CONINU", "G3Q_CONINU" } }, G41->( IndexKey( 1 ) ) )
	oModel:GetModel( "G41_ITENS" ):SetOptional( .T. )

	//9 - Tour (G40)
	oModel:AddGrid( "G40_ITENS", "G3Q_ITENS", oStruG40 )
	oModel:SetRelation( "G40_ITENS", { { "G40_FILIAL", "G3Q_FILIAL" }, { "G40_NUMID", "G3Q_NUMID" }, { "G40_IDITEM", "G3Q_IDITEM" }, { "G40_NUMSEQ", "G3Q_NUMSEQ" }, { "G40_CONORI", "G3Q_CONORI" }, { "G40_CONINU", "G3Q_CONINU" } }, G40->( IndexKey( 1 ) ) )
	oModel:GetModel( "G40_ITENS" ):SetOptional( .T. )

	//10 - Pacote (G3Z)
	oModel:AddGrid( "G3Z_ITENS", "G3Q_ITENS", oStruG3Z )
	oModel:SetRelation( "G3Z_ITENS", { { "G3Z_FILIAL", "G3Q_FILIAL" }, { "G3Z_NUMID", "G3Q_NUMID" }, { "G3Z_IDITEM", "G3Q_IDITEM" }, { "G3Z_NUMSEQ", "G3Q_NUMSEQ" }, { "G3Z_CONORI", "G3Q_CONORI" }, { "G3Z_CONINU", "G3Q_CONINU" } }, G3Z->( IndexKey( 1 ) ) )
	oModel:GetModel( "G3Z_ITENS" ):SetOptional( .T. )

	//11 - Outros (G43)
	oModel:AddGrid( "G43_ITENS", "G3Q_ITENS", oStruG43 )
	oModel:SetRelation( "G43_ITENS", { { "G43_FILIAL", "G3Q_FILIAL" }, { "G43_NUMID", "G3Q_NUMID" }, { "G43_IDITEM", "G3Q_IDITEM" }, { "G43_NUMSEQ", "G3Q_NUMSEQ" }, { "G43_CONORI", "G3Q_CONORI" }, { "G43_CONINU", "G3Q_CONINU" } }, G43->( IndexKey( 1 ) ) )
	oModel:GetModel( "G43_ITENS" ):SetOptional( .T. )

	//Tarifas
	oModel:AddGrid( "G44_ITENS", "G3R_ITENS", oStruG44 )
	oModel:SetRelation( "G44_ITENS", { { "G44_FILIAL", "G3R_FILIAL" }, { "G44_NUMID", "G3R_NUMID" }, { "G44_IDITEM", "G3R_IDITEM" }, { "G44_NUMSEQ", "G3R_NUMSEQ" }, { "G44_CONORI", "G3R_CONORI" }, { "G44_CONINU", "G3R_CONINU" } }, G44->( IndexKey( 1 ) ) )
	oModel:GetModel( "G44_ITENS" ):SetOptional( .T. )

	//Taxas
	oModel:AddGrid( "G46_ITENS", "G3R_ITENS", oStruG46 )
	oModel:SetRelation( "G46_ITENS", { { "G46_FILIAL", "G3R_FILIAL" }, { "G46_NUMID", "G3R_NUMID" }, { "G46_IDITEM", "G3R_IDITEM" }, { "G46_NUMSEQ", "G3R_NUMSEQ" }, { "G46_CONORI", "G3R_CONORI" }, { "G46_CONINU", "G3R_CONINU" } }, G46->( IndexKey( 1 ) ) )
	oModel:GetModel( "G46_ITENS" ):SetOptional( .T. )

	//Extras
	oModel:AddGrid( "G47_ITENS", "G3R_ITENS", oStruG47 )
	oModel:SetRelation( "G47_ITENS", { { "G47_FILIAL", "G3R_FILIAL" }, { "G47_NUMID", "G3R_NUMID" }, { "G47_IDITEM", "G3R_IDITEM" }, { "G47_NUMSEQ", "G3R_NUMSEQ" }, { "G47_CONORI", "G3R_CONORI" }, { "G47_CONINU", "G3R_CONINU" } }, G47->( IndexKey( 1 ) ) )
	oModel:GetModel( "G47_ITENS" ):SetOptional( .T. )

	//Impostos
	oModel:AddGrid( "G49_ITENS", "G3R_ITENS", oStruG49 )
	oModel:SetRelation( "G49_ITENS", { { "G49_FILIAL", "G3R_FILIAL" }, { "G49_NUMID", "G3R_NUMID" }, { "G49_IDITEM", "G3R_IDITEM" }, { "G49_NUMSEQ", "G3R_NUMSEQ" }, { "G49_CONORI", "G3R_CONORI" }, { "G49_CONINU", "G3R_CONINU" } }, G49->( IndexKey( 1 ) ) )
	oModel:GetModel( "G49_ITENS" ):SetOptional( .T. )

	//Acordos Fornecedor
	oModel:AddGrid( "G48B_ITENS", "G3R_ITENS", oStruG48B )
//	oModel:SetRelation( "G48B_ITENS", { { "G48_FILIAL", "G3R_FILIAL" }, { "G48_NUMID", "G3R_NUMID" }, { "G48_IDITEM", "G3R_IDITEM" }, { "G48_NUMSEQ", "G3R_NUMSEQ" }, { "G48_CONORI", "G3R_CONORI" }, { "G48_CONINU", "G3R_CONINU" }, { "G48_CLIFOR", "'2'" }, { "G48_CODAPU", "''" }, { 'G48_RECCON', "'.T.'" } }, G48->( IndexKey( 1 ) ) )
	oModel:SetRelation( "G48B_ITENS", { { "G48_FILIAL", "G3R_FILIAL" }, { "G48_NUMID", "G3R_NUMID" }, { "G48_IDITEM", "G3R_IDITEM" }, { "G48_NUMSEQ", "G3R_NUMSEQ" }, { "G48_CONORI", "G3R_CONORI" }, { "G48_CONINU", "G3R_CONINU" }, { "G48_CLIFOR", "'2'" }, { "G48_CODAPU", "''" } }, G48->( IndexKey( 1 ) ) )
	oModel:GetModel( "G48B_ITENS" ):SetOptional( .T. )

	//Itens Financeiros - Fornecedor
	oModel:AddGrid( "G4CB_ITENS", "G3R_ITENS", oStruG4CB )
	oModel:SetRelation( "G4CB_ITENS", { { "G4C_FILIAL", "G3R_FILIAL" }, { "G4C_NUMID", "G3R_NUMID" }, { "G4C_IDITEM", "G3R_IDITEM" }, { "G4C_NUMSEQ", "G3R_NUMSEQ" }, { "G4C_CONORI", "G3R_CONORI" }, { "G4C_CONINU", "G3R_CONINU" }, { "G4C_CLIFOR", "'2'" }, { "G4C_CODAPU", "''" } }, G4C->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4CB_ITENS" ):SetOptional( .T. )

	oModel:AddGrid( "G4D_ITENS", "G3R_ITENS", oStruG4D )
	oModel:SetRelation( "G4D_ITENS", { { "G4D_FILIAL", "G3R_FILIAL" }, { "G4D_NUMID", "G3R_NUMID" }, { "G4D_IDITEM", "G3R_IDITEM" }, { "G4D_NUMSEQ", "G3R_NUMSEQ" }, { "G4D_CONORI", "G3R_CONORI" }, { "G4D_CONINU", "G3R_CONINU" } }, G4D->( IndexKey( 1 ) ) )
	oModel:GetModel( "G4D_ITENS" ):SetOptional( .T. )

	oModel:AddGrid( "G9KA_ITENS", "G3Q_ITENS", oStruG9KA )
	oModel:SetRelation( "G9KA_ITENS", { { "G9K_FILIAL", "G3R_FILIAL" }, { "G9K_NUMID", "G3Q_NUMID" }, { "G9K_IDITEM", "G3Q_IDITEM" }, { "G9K_NUMSEQ", "G3Q_NUMSEQ" }, { "G9K_CONORI", "G3Q_CONORI" }, { "G9K_CONINU", "G3Q_CONINU" }, { "G9K_CLIFOR", "'1'" } }, G9K->( IndexKey( 1 ) ) )
	oModel:GetModel( "G9KA_ITENS" ):SetOptional( .T. )

	oModel:AddGrid( "G9KB_ITENS", "G3R_ITENS", oStruG9KB )
	oModel:SetRelation( "G9KB_ITENS", { { "G9K_FILIAL", "G3R_FILIAL" }, { "G9K_NUMID", "G3R_NUMID" }, { "G9K_IDITEM", "G3R_IDITEM" }, { "G9K_NUMSEQ", "G3R_NUMSEQ" }, { "G9K_CONORI", "G3R_CONORI" }, { "G9K_CONINU", "G3R_CONINU" }, { "G9K_CLIFOR", "'2'"} }, G9K->( IndexKey( 1 ) ) )
	oModel:GetModel( "G9KB_ITENS" ):SetOptional( .T. )

Return oModel