#INCLUDE "TURA042R.ch"
#Include 'Totvs.ch' 
#Include 'FWMVCDef.ch'

Static oTA042Model
Static aCmpAtuIf := T34CmpAtIf()

/*/{Protheus.doc} TURA042R
Concilia��o Reembolso (Terrestre)
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Function TURA042R()
	Local oBrowse	:= nil

	SetKey (VK_F12,{|a,b| AcessaPerg("TURA042R",.T.)})

	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('G8C')
	oBrowse:SetMenuDef( 'TURA042R' )

	oBrowse:SetDescription(STR0001) //"Reembolso Terrestre"

	oBrowse:AddLegend( 'G8C_STATUS == "1"', 'GREEN' , STR0002 ) //"Efetivada"
	oBrowse:AddLegend( 'G8C_STATUS == "2"', 'RED' 	, STR0003  ) //"N�o Efetivada"
	oBrowse:AddLegend( "G8C_STATUS == '3'", "YELLOW", STR0040 ) //"Erro efetiva��o"

	oBrowse:SetFilterDefault( 'G8C->G8C_TIPO == "2"' )

	oBrowse:Activate()

	Set Key VK_F12 To
Return nil

/*/{Protheus.doc} ModelDef
Modelo de dados referete a concilia��o outros (terrestre)
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function ModelDef(oModel)
	Local oStruG8C 	:= Nil	//Cabe�alho Conc. Fatura Terrestre
	Local oStruG46	:= Nil 	//Taxas
	Local oStruG49	:= Nil 	//Impostos
	Local oStruG4C  := Nil 	//Itens Financeiros Fornecedor

	Default oModel	:= FwLoadModel( 'TURA042' )		//Reembolso

	oStruG8C := oModel:GetModel("G8C_MASTER"):GetStruct()	
	oStruG46 := oModel:GetModel("G46_ITENS" ):GetStruct() 
	oStruG49 := oModel:GetModel("G49_ITENS" ):GetStruct() 
	oStruG4C := oModel:GetModel("G4CB_ITENS"):GetStruct() 

	oStruG8C:SetProperty('G8C_TIPO'	 , MODEL_FIELD_INIT, {|| '2' } )
	oStruG8C:SetProperty('G8C_DOCENT', MODEL_FIELD_INIT, {|| '2' } )
	oStruG8C:SetProperty('G8C_DOCENT', MODEL_FIELD_WHEN, {|| .F. } )
	oStruG46:SetProperty("G46_CONMAN", MODEL_FIELD_INIT, {|| "1" } ) 		//Todos as taxas incluidas ser�o marcadas como inseridas manualmente
	oStruG49:SetProperty("G49_CONMAN", MODEL_FIELD_INIT, {|| "1" } )  		//Todos os impostos incluidos ser�o marcadas como inseridas manualmente
	oStruG4C:SetProperty("G4C_STATUS", MODEL_FIELD_NOUPD, .F.     )  		//Todos os impostos incluidos ser�o marcadas como inseridas manualmente

	//Documentos de reserva
	oModel:GetModel( 'G3R_ITENS'  ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G48A_ITENS' ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G48B_ITENS' ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G9KA_ITENS' ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G9KA_ITENS' ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G4E_ITENS'  ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G46_ITENS'  ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )
	oModel:GetModel( 'G49_ITENS'  ):SetLPre( { |oModelGrid, nLine, cAction, cField| TA042RLPre(oModelGrid, nLine, cAction, cField) } )

	oModel:SetPost( {|oModel| TA042RTOK(oModel) } )
	oModel:SetCommit( {|oModel| TA042RCommit(oModel) } )
	oModel:SetVldActivate( { |oModel| TA042RVlAct( oModel ) } )
	oModel:SetActivate( {|| TA042RInit( oModel ) } )

Return oModel

/*/{Protheus.doc} ViewDef
Vis�o referete a concilia��o outros (terrestre)
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ViewDef()
	Local oView		:= FwLoadView( 'TURA042' )
	Local oModel   	:= oView:GetModel()
 	Local oStruG4E	:= FwFormStruct( 2, 'G4E' )	//Reembolso
 	Local oStruG4B	:= FwFormStruct( 2, 'G4B' ) //Informa��es Adicionais

 	ModelDef( oModel )

 	oView:AddGrid( 'VIEW_G4B', oStruG4B, 'G4B_ITENS' )
 	oView:AddGrid( 'VIEW_G4E', oStruG4E, 'G4E_ITENS' )

 	//Bot�es
 	oView:AddUserButton( STR0004, STR0005, {|| TA042RPesq() } ) //"Pesquisar DR"###"Pesquisa documentos de reserva"
	oView:AddUserButton( STR0023, STR0024, {|| FwMsgRun(,{|| TA042AtuDR( .F. ), SetAplAcordFin(.F.) },,STR0025) }) //"Atualizar DRs"###"Atualiza informa��es do DR devido altera��es"###"Aplicando altera��es no DR..."

	oView:AddSheet( 'FOLDER_IT_DR', 'FOLDER_IT_DR_G4E', STR0006) //"Reembolso"
	oView:AddSheet( 'FOLDER_SEG', 'FOLDER_SEG_G4B', STR0007	) //"Informa��es Adicionais"

	oView:CreateHorizontalBox( 'BOX_G4E', 100, , , 'FOLDER_IT_DR', 'FOLDER_IT_DR_G4E' )
	oView:CreateHorizontalBox( 'BOX_G4B', 100, , , 'FOLDER_SEG'  , 'FOLDER_SEG_G4B'   )

	oView:SetOwnerView('VIEW_G4E', 'BOX_G4E')
	oView:SetOwnerView('VIEW_G4B', 'BOX_G4B')

	oView:SetViewProperty("G3R_ITENS","CHANGELINE",{{|oView, cViewID| TA042RChLin( oView, cViewID ) }})
	//oView:SetAfterViewActivate({|oView| T42RViewAc( oView ) })
	
	oView:SetViewAction('DELETELINE'  , {|oView, cIdView, nNumLine| TA042Dlin(oView, cIdView, nNumLine)})
	oView:SetViewAction('UNDELETELINE', {|oView, cIdView, nNumLine| TA042Dlin(oView, cIdView, nNumLine)})

	If !IsInCallStack("TuraEfetiv") .And. !IsInCallStack("TA042AEfet") 
		SetKey(VK_F8, {|| FwMsgRun( , {|| TA042AtuDR(.F.)}, , STR0025)}) // "Aplicando altera��es no DR..."
	EndIf
	
Return oView

/*/{Protheus.doc} TA042RVlAct
Valida��es para opera��es na concilia��o
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, Objeto instanciado da ModelDef
/*/
Static Function TA042RVlAct( oModel )
	Local lRet := .T.

	If oModel:GetOperation() == MODEL_OPERATION_UPDATE .Or. oModel:GetOperation() == MODEL_OPERATION_DELETE
		If G8C->G8C_STATUS == '1' .And. !FWIsInCallStack( 'TA042REFET' )
			lRet := .F.
			Help(,,'TA042RUPD',,STR0008,1,0) //"Concilia��o j� efetivada, estorne a efetiva��o para alterar ou excluir."
		EndIf
	EndIf
Return lRet

/*/{Protheus.doc} TA042RTOK
Fun��o para valida��o do Post (TudoOK)
@type function
@author Anderson Toledo
@since 13/04/2016
@version 1.0
/*/
Function TA042RTOK( oModel )
	Local lRet := .T.

	If oModel:GetOperation() == 3 .Or. oModel:GetOperation() == 4
		If GetAplAcordFin()		//Indica alguma altera��o que for�a a atualiza��o dos DR's
			Help(,,"TA042RCOMMIT",,STR0029,1,0) //"Foram realizadas altera��es nos Documentos de Reserva, utilize a rotina 'Atualizar DRs' para atualiza��o dos registros."
			lRet := .F.
		EndIf
	EndIf

Return lRet

/*/{Protheus.doc} TA042RLPre
Pr� valida��o de modelos
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042RLPre(oModelGrid, nLine, cAction, cField)
	Local aSaveLines := {}
	Local aIdGrids	 := { "G3Q_ITENS","G48A_ITENS","G4CA_ITENS","G3S_ITENS","G3T_ITENS","G3U_ITENS","G3V_ITENS","G3W_ITENS","G3Y_ITENS","G3X_ITENS","G42_ITENS","G41_ITENS","G40_ITENS","G3Z_ITENS","G43_ITENS","G44_ITENS","G46_ITENS","G47_ITENS","G48B_ITENS","G49_ITENS","G4CB_ITENS","G4E_ITENS","TOTDR"}
	Local cModelId	 := oModelGrid:GetId()
	Local lRet		 := .T.
	Local nX		 := 0
	Local nLineGrid	 := 0
	Local oModel 	 := FwModelActive()

	//Se a variavel est�tica lAllDelete, deleta sem valida��o individual
	If GetAllDelete() .And. cAction $ "DELETE|UNDELETE"
		Return .T.
	EndIf

	Do Case
		Case cModelId == "G46_ITENS"
			If cAction == "CANSETVALUE"
				If ( oModelGrid:GetValue( "G46_CONMAN" ) == "2"  .And. cField <> "G46_VLBASE" ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					lRet := .F.
					SX3->( dbSetOrder(2) )
					SX3->( dbSeek( "G46_VLBASE" ) )
					oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042RCANSETVALUE", I18N(STR0036, {AllTrim(X3Titulo())}))  // "Para taxas j� cadastradas, apenas o campo #1 � alter�vel."
					Return .F.
				EndIf
				If !FwIsInCallStack('TA042Desfz')
					SetAplAcordFin(.T.)
				EndIf

			ElseIf cAction == "DELETE" 
				If !GetAllDelete() .And. oModelGrid:GetValue( "G46_CONMAN" ) == "2" .And. !oModel:GetModel( "G3R_ITENS" ):IsDeleted()
					oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0057)	// "Somente taxas inseridas manualmente podem ser exclu�das."
					Return .F.
				EndIf
				If !FwIsInCallStack('TA042Desfz')
					SetAplAcordFin(.T.)
				EndIf
				oModel:GetModel('G3R_ITENS'):SetValue('G3R_TAXA', oModel:GetModel('G3R_ITENS'):GetValue('G3R_TAXA') - oModelGrid:GetValue('G46_VLTAXA'))
	
			ElseIf cAction == "UNDELETE" 
				If !FwIsInCallStack('TA042Desfz')
					SetAplAcordFin(.T.)
				EndIf
				oModel:GetModel('G3R_ITENS'):SetValue('G3R_TAXA', oModel:GetModel('G3R_ITENS'):GetValue('G3R_TAXA') + oModelGrid:GetValue('G46_VLTAXA'))	
			EndIf
	
			If cAction $ "DELETE|UNDELETE"		
				CALCVLG8C(oModel:GetModel('G3R_ITENS'), .T./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "UPDLIN"/*cMomento*/)
			EndIf
	
		Case cModelId == "G49_ITENS"
			If cAction == "CANSETVALUE"
				If ( oModelGrid:GetValue( "G49_CONMAN " ) == "2"  .And. !( cField $ "G49_BASE|G49_ALIQ") ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					lRet := .F.
	
					SX3->( dbSetOrder(2) )
					SX3->( dbSeek( "G49_BASE" ) )
					cAux := AllTrim( X3Titulo() ) + ", "
	
					SX3->( dbSetOrder(2) )
					SX3->( dbSeek( "G49_ALIQ" ) )
					cAux += AllTrim( X3Titulo() )
	
					oModel:SetErrorMessage(cModelId, cField, cModelId, cField, "TA042RCANSETVALUE", I18N(STR0037, {cAux}))  // "Para impostos j� cadastrados, apenas os campos #1 s�o alter�veis."
				EndIf
		
			ElseIf cAction == "DELETE" 
				If !GetAllDelete() .And. oModelGrid:GetValue( "G49_CONMAN" ) == "2" .And. !oModel:GetModel( "G3R_ITENS" ):IsDeleted()
					oModel:SetErrorMessage(cModelId, "", cModelId, "", "TA042NODEL", STR0056) 	// "Somente impostos inseridos manualmente podem ser exclu�dos."
					lRet := .F.
				EndIf
			EndIf
			
		Case cModelId == "G4E_ITENS"
			If cAction == "CANSETVALUE"
				If !( cField $ "G4E_TARPAG|G4E_TARUTI|G4E_TXREEM|G4E_EXREEM" ) .Or. oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					lRet := .F.
				EndIf
			ElseIf  (cAction == "DELETE" .Or. cAction == "UNDELETE") .And. !GetAllDelete()  .And.  oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
				lRet := .F.
			EndIf
			
			If lRet .And. !FwIsInCallStack('TA042Desfz')
				SetAplAcordFin(.T.)
			EndIf

		Case cModelId == "G3R_ITENS"
			If cAction == "CANSETVALUE"
				If oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					oModel:SetErrorMessage(cModelId, "G3R_STATUS", cModelId, "G3R_STATUS", "TA042RNOALT", STR0047)  //"N�o � permitido altera��o neste documento de reserva, Status � diferente de 'Pendente'."
					Return .F.
				EndIf
				
				If cField == "G3R_NATURE"
					SetAplAcordFin(.T.)
				Else
					Return .F.
				EndIf

			ElseIf cAction == "DELETE"
				aSaveLines := FWSaveRows()
				TA042RAcGr( oModel, .T. )
				SetAllDelete(.T.)

				//� necess�rio marcar todos os filhos como deletados, somente assim, todos os registros ser�o
				//processados no Commit e a fun��o TA042EstIn executada, liberando o flag dos registros originais
				TA42DelSons( oModel, 'G3R_ITENS', .F. )
				SetAllDelete(.F.)
				TA042RAcGr( oModel, .F. )
				FWRestRows( aSaveLines )

			ElseIf cAction == "UNDELETE"
				If oModelGrid:SeekLine({{"G3R_FILIAL", oModelGrid:GetValue('G3R_FILIAL')}, ;
				                        {"G3R_NUMID" , oModelGrid:GetValue('G3R_NUMID' )}, ;
				                        {"G3R_IDITEM", oModelGrid:GetValue('G3R_IDITEM')}, ;
				                        {"G3R_NUMSEQ", oModelGrid:GetValue('G3R_NUMSEQ')}})
					oModel:SetErrorMessage(cModelId, "G3R_NUMID", cModelId, "G3R_NUMID", "TA042RNOUNDEL", STR0045)  // "Documento de Reserva j� selecionado nesta concilia��o."
					Return .F.
					
				ElseIf oModelGrid:GetValue("G3R_DIVERG") == '1'
					oModelGrid:GetModel():SetErrorMessage(cModelId, "G3R_DIVERG", cModelId, "G3R_DIVERG", "TA042RNOUNDEL", STR0046) 	// "O Documento de Reserva apresentou falha ao atualizar os Itens Financeiros e n�o pode ser recuperado." 
					Return .F.
				EndIf
				
				aSaveLines := FWSaveRows()
				TA042RAcGr(	oModel, .T. )
				SetAllDelete(.T.)	
				oModelGrid:GoLine( nLine )
				TA42DelSons( oModel, 'G3R_ITENS', .F., .T. )
				SetAllDelete(.F.)	
				TA042RAcGr( oModel, .F. )
				FWRestRows( aSaveLines )
			EndIf

		Case cModelId == "G3Q_ITENS"
			If cAction == "CANSETVALUE"
				If cField == "G3Q_NATURE"
					lRet := .T.
					SetAplAcordFin(.T.)
				Else
					lRet := .F.
				EndIf
			EndIf

		Case cModelId $ "G48A_ITENS|G48B_ITENS"
			If cAction == "CANSETVALUE"
				If oModel:GetValue( "G3R_ITENS", "G3R_STATUS" ) <> "1"
					lRet := .F.

				ElseIf cField == "G48_RECCON"
					If oModelGrid:GetValue( "G48_STATUS" ) <> "1"
						oModelGrid:GetModel():SetErrorMessage(cModelId, cField, cModelId, cField, "TA042G48OK", STR0026) 	//"Apenas acordos abertos podem sem marcados para conciliar."
						lRet := .F.
					EndIf

				ElseIf cField $ "G48_NATURE|G48_STATUS|G48_CODACD|G48_PERACD|G48_VLACD"
					SetAplAcordFin(.T.)
				EndIf

			ElseIf cAction == "DELETE"
				If cModelId == "G48A_ITENS"
					lRet := .F.
				Else
					If ( !GetAllDelete() .And. ( !Empty( oModelGrid:GetValue( "G48_INTERV" ) ) .And. oModelGrid:GetValue( "G48_INTERV" ) == "3" ) )
						oModelGrid:GetModel():SetErrorMessage(cModelId, "G48_INTERV", cModelId, "G48_INTERV", "TA042RACDEL", STR0031) 	//"Somente acordos inseridos manualmente podem ser exclu�dos."
						lRet := .F.
					Else
						SetAplAcordFin(.T.)
					EndIf
				EndIf

			ElseIf cAction == "UNDELETE"
				If cModelId == "G48A_ITENS"
					lRet := .F.
				EndIf
			EndIf

		Case cModelId == "G9KA_ITENS" .Or. cModelId == "G9KB_ITENS"
 			If cAction == "CANSETVALUE"
 				lRet := .F.
 			ElseIf cAction == "DELETE" .Or. cAction == "UNDELETE"
 				lRet := .F.
 			EndIf

	End Case

	//Verifica a necessidade de atualiza��o do item financeiro
	If lRet .And. cAction $ "SETVALUE|DELETE|UNDELETE|LOADVALUE" .And. Ascan(aCmpAtuIf,{|x| x[1] == cField}) > 0

		//Condi��o para atualizar fornecedor
		If	( SubStr(cField,1,3) $ "G3R|G4D|G4A" .Or. cField == "G3Q_FORMPG" .Or.;
			( SubStr(cField,1,3) == 'G9K' .And. oModelGrid:GetId() == "G9KB_ITENS" ))

			cOpcao := "2"

		//Condi��o para atualizar cliente
		Else
			cOpcao := "1"
		EndIf

		oModelG3Q := oModel:GetModel( 'G3Q_ITENS' )
		If Empty(oModelG3Q:GetValue("G3Q_ATUIF"))
			oModelG3Q:LoadValue("G3Q_ATUIF",cOpcao)
		ElseIf cOpcao <> oModelG3Q:GetValue("G3Q_ATUIF")
			oModelG3Q:LoadValue("G3Q_ATUIF",'3')
		EndIf

	EndIf

	TURXNIL(aSaveLines)
	TURXNIL(aIdGrids)
Return lRet

/*/{Protheus.doc} TA042RCommit
Fun��o para persist�ncia/exclus�o das informa��es
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042RCommit( oModel )

Local lRet 	   := .T.
Local cMsgErro := ""
Local cMsgSolu := ""
Local nX       := 0
Local cBkpFil  := cFilAnt

If !FwIsInCallStack("TuraEfetiv")
	If oModel:GetOperation() != MODEL_OPERATION_DELETE
		// validar se tem algum DR no modelo pois deve haver pelo menos UM para ser conciliado
		If !(oModel:GetModel("G3R_ITENS"):Length(.T.) .And. !oModel:GetModel("G3R_ITENS"):IsEmpty())
			lRet := .F.
			cMsgErro := I18N(STR0048, {IIF(oModel:GetOperation() == 3, STR0050, IIF(oModel:GetOperation() == 4, STR0051, IIF(oModel:GetOperation() == 5, STR0052, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
			cMsgSolu := STR0050		// "A concilia��o precisa ter ao menos um Documento de Reserva."	
		EndIf
	EndIf

	// Validando se houve alguma altera��o que for�a a atualiza��o dos DR's 
	If lRet .And. (oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE)	
		If GetAplAcordFin()		
			lRet := .F.
			cMsgErro := STR0051		// "Foram realizadas altera��es nos Documentos de Reserva."
			cMsgSolu := STR0052 	// "Utilize a rotina 'Atualizar DRs' para atualiza��o dos registros."
		EndIf
	EndIf

	If lRet
		BEGIN TRANSACTION
			If (lRet := TA042Coninu(oModel))
				If (lRet := oModel:VldData())
					If !(lRet := FwFormCommit(oModel))
						cMsgErro := I18N(STR0048, {IIF(oModel:GetOperation() == 3, STR0010, IIF(oModel:GetOperation() == 4, STR0011, IIF(oModel:GetOperation() == 5, STR0012, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
						cMsgSolu := STR0049		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."	
						DisarmTransaction()
						Break
					EndIf
				Else
					cMsgErro := I18N(STR0048, {IIF(oModel:GetOperation() == 3, STR0010, IIF(oModel:GetOperation() == 4, STR0011, IIF(oModel:GetOperation() == 5, STR0012, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
					cMsgSolu := STR0049		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."	
					DisarmTransaction()
					Break
				EndIf
			Else
				cMsgErro := I18N(STR0048, {IIF(oModel:GetOperation() == 3, STR0010, IIF(oModel:GetOperation() == 4, STR0011, IIF(oModel:GetOperation() == 5, STR0012, "")))}) 	// "Ocorreu um problema ao #1 a Concilia��o" 	"Incluir" 	"Alterar"		"Excluir"
				cMsgSolu := STR0049		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."	
				DisarmTransaction()
				Break
			EndIf
		END TRANSACTION
	EndIf
Else
	lRet := FwFormCommit(oModel)
	cMsgErro := I18N(STR0053, {IIF(FwIsInCallStack("TA042AEfet"), STR0054, STR0055)})		// "Ocorreu um erro ao #1 Concilia��o."	 	"efetivar a"	"estornar a efetiva��o da"	
	cMsgSolu := STR0049		// "Por favor, tente novamente. Caso o problema persista, entrar em contato com o admistrador do sistema."
EndIf

If !lRet
	oModel:SetErrorMessage(oModel:GetId(), "", oModel:GetId(), "G3R_CONALT", "TA042RCOMMIT", cMsgErro, cMsgSolu)		
EndIf

cFilAnt := cBkpFil
	
Return lRet

/*/{Protheus.doc} TA042REfet
Rotina para efetiva��o da concilia��o de reembolso
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042REfet( cOpc )
	//Possui o mesmo tratamento que a concilia��o terrestre
	If cOpc == '1'
		TA042AEfet( 'TURA042R' )
	Else
		TA042ADEfe( 'TURA042R' )
	EndIf

Return

/*/{Protheus.doc} MenuDef
Menu Funcional
@type function
@author Anderson Toledo
@since 08/01/2016
@version 12.1.7
/*/
Static Function MenuDef()
	Local aRotina := {}

	ADD OPTION aRotina Title STR0009 	Action 'VIEWDEF.TURA042R' 	 OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina Title STR0010	Action "TA042Concil(3,'3')"	 OPERATION 3 ACCESS 0 //"Incluir"
	ADD OPTION aRotina Title STR0011 	Action "TA042Concil(4,'3')"	 OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina Title STR0012 	Action 'VIEWDEF.TURA042R' 	 OPERATION 5 ACCESS 0 //"Excluir"
	ADD OPTION aRotina Title STR0013	Action 'TuraEfetiv("1","3")' OPERATION 4 ACCESS 0 //"Efetivar"
	ADD OPTION aRotina Title STR0015	Action 'TuraEfetiv("2","3")' OPERATION 4 ACCESS 0 //"Desfaz Efetivar"
	ADD OPTION aRotina Title STR0017 	Action "TA042RVisFi" 		 OPERATION 4 ACCESS 0 //"Vis�o Finan."
	ADD OPTION aRotina Title STR0030 	Action "VIEWDEF.TURA042G" 	 OPERATION 2 ACCESS 0 //"Log Efetiva��o"
	ADD OPTION aRotina Title STR0032	Action "FwMsgRun( , {|| TA042DemFi(1)}, , '" + STR0034 + "')"		 OPERATION 4 ACCESS 0 //"Dem. Financ. Agencia"	"Imprimindo o Demonstrativo Ag�ncia, aguarde..."
	ADD OPTION aRotina Title STR0033	Action "FwMsgRun( , {|| TA042DemFi(2)}, , '" + STR0035 + "')"		 OPERATION 4 ACCESS 0 //"Dem. Financ. Cliente"	"Imprimindo o Demonstrativo Cliente, aguarde..."

Return aRotina

/*/{Protheus.doc} TA042APesq
Fun��o de chamada da tela de pesquisa documentos de reserva
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042RPesq()
	Local oView	:= FwViewActive()
	Local aButtons := {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,STR0018},{.T.,STR0019},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Confirmar"###"Cancelar"

	oTA042Model	:= FwModelActive()

	TA042RAcGr(	oTA042Model, .T. )

	FWExecView(STR0020,"VIEWDEF.TURA042F",3,,{|| .T.},,,aButtons) //"Pesquisa de Documentos de Reserva"

	FwModelActive( oTA042Model )
	oView:Refresh()

	oTA042Model:GetModel('G3R_ITENS'):GoLine(1)

	TA042RAcGr(	oTA042Model, .F. )
	TURXNIL(aButtons)
Return

/*/{Protheus.doc} TA042RAcGr
Fun��o para determinar os acessos de inserir/deletar linhas nos grids, a atualiza��o � realizada  atrav�s de pre-valida��o CANSETVALUE
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oModel, objeto, objeto do ModelDef instaciado
@param bAcesso, booleano, Indica que o modelo e todos submodelos ser�o liberado para inclus�o, exclus�o e altera��o para rotinas de importa��o de dados
/*/
Static Function TA042RAcGr( oModel, lAcesso )
	Local aIdGrids 	 := oModel:GetModelIds()
	Local nX 		 := 0
	Local nOperation := oModel:GetOperation()

	If lAcesso
		For nX := 1 To len(aIdGrids)
			If oModel:GetModel(aIdGrids[nX]):ClassName() == 'FWFORMGRID'
				oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .F. )
				oModel:GetModel( aIdGrids[nX] ):SetNoUpdateLine( .F. )
				oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .F. )
			EndIf
		Next

	Else
		If nOperation == 3 .Or. nOperation == 4
			For nX := 1 To len(aIdGrids)
				If oModel:GetModel(aIdGrids[nX]):ClassName() == 'FWFORMGRID'

					If AllTrim( aIdGrids[nX] ) == 'G3R_ITENS'
						oModel:GetModel( 'G3R_ITENS' ):SetNoDeleteLine( .F. )
						oModel:GetModel( 'G3R_ITENS' ):SetNoInsertLine( .T. )
					ElseIf AllTrim( aIdGrids[nX] ) = 'G48B_ITENS'
						oModel:GetModel( 'G48B_ITENS' ):SetNoInsertLine( .F. )
						oModel:GetModel( 'G48B_ITENS' ):SetNoDeleteLine( .F. )
					ElseIf AllTrim( aIdGrids[nX] ) == "G4E_ITENS"
						oModel:GetModel( "G4E_ITENS" ):SetNoDeleteLine( .T. )
						oModel:GetModel( "G4E_ITENS" ):SetNoInsertLine( .T. )
					ElseIf AllTrim( aIdGrids[nX] ) == "G46_ITENS"
						oModel:GetModel( "G46_ITENS" ):SetNoInsertLine( .F. )
					ElseIf AllTrim( aIdGrids[nX] ) = "G49_ITENS"
						oModel:GetModel( "G49_ITENS" ):SetNoInsertLine( .F. )
					Else
						oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .T. )
						oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .T. )
					EndIf
				EndIf
			Next

		Else

			For nX := 1 To len(aIdGrids)
				If oModel:GetModel(aIdGrids[nX]):ClassName() == 'FWFORMGRID'
					oModel:GetModel( aIdGrids[nX] ):SetNoInsertLine( .T. )
					oModel:GetModel( aIdGrids[nX] ):SetNoUpdateLine( .T. )
					oModel:GetModel( aIdGrids[nX] ):SetNoDeleteLine( .T. )
				EndIf
			Next

		EndIf

	EndIf

	TURXNIL(aIdGrids)
Return

/*/{Protheus.doc} TA042RInit
Fun��o para executar as condi��es na inicializa��o do model
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Static Function TA042RInit( oModel )
	SetAplAcordFin(.F.)
	SetAllDelete(.F.)
	TA042RAcGr( oModel, .F. )

	// fun��o para atualizar os campos totalizadores de DR
	TRDChaceNew()
	TDRCacheClean()
	
	If !(FWIsInCallStack("TA042AEFET") .Or. FWIsInCallStack("TA042ADEFE")) .And. oModel:GetOperation() != MODEL_OPERATION_INSERT 
		CALCVLG8C(oModel:GetModel('G3R_ITENS'), .F./*lRecalc*/, .F./*lAllLines*/, /*cChaveTDR*/, "INIT"/*cMomento*/)
	EndIf
	
	If oModel:GetOperation() == MODEL_OPERATION_UPDATE
		oModel:GetModel("G8C_MASTER"):SetValue("G8C_MOEDA", oModel:GetModel("G3R_ITENS"):GetValue("G3R_MOEDA"))
	EndIf
Return .T.

/*/{Protheus.doc} TA042RModel
Fun��o para retorno do objeto oModel (vari�vel est�tica) do fonte atual
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
/*/
Function TA042RModel()
Return oTA042Model

/*/{Protheus.doc} TA042RVisFi
Fun��o para chamar a vis�o financeira sem apresentar os bot�es padr�o.
@type function
@author Anderson Toledo
@since 04/07/2016
@version 1.0
/*/
Function TA042RVisFi()
	Local aButtons 	:= {{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.T.,"Sair"},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil},{.F.,Nil}} //"Confirmar"###"Cancelar"

	FWExecView(STR0017,'VIEWDEF.TURA042D',4,,{|| .T.},,,aButtons)
	
	TURXNIL(aButtons)
Return

/*/{Protheus.doc} TA042ChLin
Fun��o executada a cada troca de linha da view referente a G3R
@type function
@author Anderson Toledo
@since 15/03/2016
@version 1.0
@param oView, objeto, Objeto instanciado da ViewDef
@param cViewID, character, Id da View que originou a chamada
/*/
Static Function TA042RChLin( oView, cViewID )

	// fun��o para atualizar os campos totalizadores de DR
	CalcTotDr( 'CHANGE' )

Return

Static Function T42RViewAc( oView )
	Local oModel := FwModelActive()
	
	// fun��o para atualizar os campos totalizadores de DR
	oModel:GetModel( 'G3R_ITENS' ):GoLine( 1 )
	oView:Refresh()

Return