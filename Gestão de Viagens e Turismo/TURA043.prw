#INCLUDE "TURA043.ch"
#Include "Totvs.ch"
#Include "FWMVCDef.ch"
#Include "FWEditPanel.ch"

#DEFINE AP_VOLUME_VENDAS 	"M01"
#DEFINE AP_TRANSACAO		"M02"
#DEFINE AP_MARKET_SHARE		"M03"
#DEFINE AP_CRESCIMENTO		"M04"
#DEFINE GRIDMAXLIN 99999

Static aFaixas	:= {}
Static lShowRun	:= .T.
Static oTblHisSin	:= nil //Tabela tempor�ria com a somat�ria do per�odo por acordo
Static oTblHisAna	:= nil //Tabela tempor�ria com os documentos de reserva que compoem o hist�rico
Static oChart		:= nil //Objeto FwChart utilizado na view da apura��o
Static oPnlChart	:= nil //Container utilizado para o objeto FwChart
Static nTotMs		:= 0
Static nTA43Opc		:= 0
//+-----------------------------------------------------------------------------------------------------
//|Browse
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TURA043
Apura��o de receitas via metas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TURA043()
Local oBrowse
Local bExec

SetKey (VK_F12,{|a,b| AcessaPerg("TURA043C",.T.)})

bExec := {||	nTA43Opc := 3,; 
				TA043Gera(),;
				G6L->(DbGoTop()),;
				oBrowse:ExecuteFilter(),;
				nTA43Opc := 0}

oBrowse := FWMBrowse():New()
oBrowse:SetAlias("G6L")

oBrowse:SetDescription(STR0001) //"Apura��o de Metas"

oBrowse:AddLegend( "G6L_STATUS == '1'", "GREEN" , STR0002 ) //"Em Aberto"
oBrowse:AddLegend( "G6L_STATUS == '2'", "RED"  , STR0003  ) //"Liberada"

oBrowse:SetFilterDefault("G6L_TPAPUR == '2'")

oBrowse:bIniWindow := {|| TurApuraBrw(oBrowse)}

oBrowse:AddButton(STR0038, bExec,,3,,,3)//"Gerar"

oBrowse:Activate()

Set Key VK_F12 To

Return nil

//+-----------------------------------------------------------------------------------------------------
//|MVC
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} ModelDef
Modelo de dados referete a apura��o de metas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ModelDef()
	
	Local oModel	:= Nil
	Local oStruG6L 	:= FWFormStruct( 1, "G6L", /*bAvalCampo*/, /*lViewUsado*/ )	// APURACAO
	Local oStruG6M 	:= FWFormStruct( 1, "G6M", /*bAvalCampo*/, /*lViewUsado*/ )	// SEGMENTOS APURACAO
	Local oStruG6O 	:= FWFormStruct( 1, "G6O", /*bAvalCampo*/, /*lViewUsado*/ )	// RESUMO APURACAO
	Local oStruG6N 	:= FWFormStruct( 1, "G6N", /*bAvalCampo*/, /*lViewUsado*/ )	// ACORDOS X DOCUMENTOS DE RESERVA
	Local oStruG81	:= FWFormStruct( 1, "G81", /*bAvalCampo*/, /*lViewUsado*/ )	// ITEM FINANCEIRO
	
	Local aBaseApu	:= Tur43Cbox("G6D_BASEAP") // { '1=Volume Vendas','2=Transa��o','3=Market Share','4=Crescimento' }
	Local aRelation	:= {}
	
	oModel := MPFormModel():New( "TURA043", /*bPreValidacao*/, /*bPosValidacao*/,{|oModel| FWFormCommit(oModel), TA043Destroy() }, {|oModel| FWFormCancel(oModel), TA043Destroy()} )
	oModel:setActivate( {| oModel | TA043LdHis( oModel ) } )

	// Retira a obrigatoriedado dos campos cliente e loja para gerar a apura��o caso o fornecedor n�o tenho seu c�digo de cliente cadastrado
	// Ao liberar a apura��o foi criada uma valida��o para verificar o preenchimento desses campos
	oStruG6L:SetProperty('G6L_CLIENT', MODEL_FIELD_OBRIGAT, .F.)
	oStruG6L:SetProperty('G6L_LOJA',   MODEL_FIELD_OBRIGAT, .F.)

	//Retira a obrigatoriedade de campos utilizados somente na apura��o de receitas de clientes
	oStruG6M:SetProperty("G6M_SEGNEG"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG81:SetProperty("G81_SEGNEG"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG81:SetProperty("G81_CONDPG"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG81:SetProperty("G81_TPENT"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG81:SetProperty("G81_ITENT"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG81:SetProperty("*"			, MODEL_FIELD_WHEN	 ,{ ||.F. } )

	//Retira a obrigatoriedade dos campos referentes a valores, ser�o exibidas todas as metas de acordo com o parametro
	oStruG6O:SetProperty("G6O_VALOR"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG6O:SetProperty("G6O_VAPLIC"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG6N:SetProperty("G6N_VALMAX"	, MODEL_FIELD_OBRIGAT,.F.)
	oStruG6N:SetProperty("G6N_DESGRP"	, MODEL_FIELD_INIT	, {|| SBM->( GetAdvFVal("SBM","BM_DESC",xFilial("SBM")+G6N->G6N_GRPROD,1,"") )  } )
	
	oStruG6N:AddField(	STR0061 		, ; // [01] C Titulo do campo	//#"C�d. Fornec"
						STR0061 		, ; // [02] C ToolTip do campo	//#"C�d. Fornec"
						"G3R_FORNEC" 	, ; // [03] C identificador (ID) do Field
						"C" 			, ; // [04] C Tipo do campo
						TamSX3("G3R_FORNEC")[1], ; // [05] N Tamanho do campo
						0 				, ; // [06] N Decimal do campo
						NIL				, ; // [07] B Code-block de valida��o do campo
						NIL 			, ; // [08] B Code-block de valida��o When do campoz
						NIL 			, ; // [09] A Lista de valores permitido do campo
						NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
						NIL  			, ; // [11] B Code-block de inicializacao do campo
						NIL 			, ; // [12] L Indica se trata de um campo chave
						.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
						.T. ) 				// [14] L Indica se o campo � virtual
	
	oStruG6N:AddField(	STR0063 				, ; // [01] C Titulo do campo	//#"Loja Forn."
						STR0063 				, ; // [02] C ToolTip do campo	//#"Loja Forn."
						"G3R_LOJA" 	, ; // [03] C identificador (ID) do Field
						"C" 			, ; // [04] C Tipo do campo
						TamSX3("G3R_LOJA")[1], ; // [05] N Tamanho do campo
						0 				, ; // [06] N Decimal do campo
						NIL				, ; // [07] B Code-block de valida��o do campo
						NIL 			, ; // [08] B Code-block de valida��o When do campoz
						NIL 			, ; // [09] A Lista de valores permitido do campo
						NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
						NIL   			, ; // [11] B Code-block de inicializacao do campo
						NIL 			, ; // [12] L Indica se trata de um campo chave
						.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
						.T. ) 				// [14] L Indica se o campo � virtual
	
	oStruG6N:AddField(	STR0065 		, ; // [01] C Titulo do campo	//#"Fornecedor"
						STR0065 		, ; // [02] C ToolTip do campo	//#"Fornecedor"
						"G3R_NOME" 		, ; // [03] C identificador (ID) do Field
						"C" 			, ; // [04] C Tipo do campo
						TamSX3("G3R_NOME")[1], ; // [05] N Tamanho do campo
						0 				, ; // [06] N Decimal do campo
						NIL				, ; // [07] B Code-block de valida��o do campo
						NIL 			, ; // [08] B Code-block de valida��o When do campoz
						NIL 			, ; // [09] A Lista de valores permitido do campo
						NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
						NIL   			, ; // [11] B Code-block de inicializacao do campo
						NIL 			, ; // [12] L Indica se trata de um campo chave
						.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
						.T. ) 				// [14] L Indica se o campo � virtual						
	
	//Acerta inicializadores padr�o divergentes da apura��o de clienets
	oStruG6O:SetProperty("G6O_DCACD"	, MODEL_FIELD_INIT	, {|| Posicione("G6D",1,xFilial("G6D")+ G6O->( G6O_CODACD + G6O_CODREV ), "G6D_DESCRI") } )
	
	//Campo virtual para apresentar legenda dos acordos, sendo
	// VERDE - Acordo ativo
	// CINZA - Acordo fora de vig�ncia
	oStruG6O:AddField( ;
							"" 				, ; // [01] C Titulo do campo
							"" 				, ; // [02] C ToolTip do campo
							"G6O___LEG" 	, ; // [03] C identificador (ID) do Field
							"C" 			, ; // [04] C Tipo do campo
							8 				, ; // [05] N Tamanho do campo
							0 				, ; // [06] N Decimal do campo
							NIL				, ; // [07] B Code-block de valida��o do campo
							NIL 			, ; // [08] B Code-block de valida��o When do campoz
							NIL 			, ; // [09] A Lista de valores permitido do campo
							NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
							FwBuildFeature( STRUCT_FEATURE_INIPAD,'IIF(G6O->G6O_STATUS == "1","BR_VERDE","BR_CINZA")')   , ; // [11] B Code-block de inicializacao do campo
							NIL 			, ; // [12] L Indica se trata de um campo chave
							.T. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
							.T. 			) // [14] L Indica se o campo � virtual

		
	//- Ajuste do combo do campo G6O_BASAPU pois a tabela � compartilhada para apura��o de receita de clientes e apura��o de acordo de metas
	oStruG6O:SetProperty('G6O_BASAPU',MODEL_FIELD_VALUES, aBaseApu )

	oModel:AddFields( "G6L_MASTER" , /*cOwner*/, oStruG6L )
	
	oModel:AddGrid( "G6M_DETAIL", "G6L_MASTER", oStruG6M, {|oMdl,nLn,cAct,cFld,xVlBef,xVlAft| Ta043Vld(oMdl,nLn,cAct,cFld,xVlBef,xVlAft) }/*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	aRelation := {	{ "G6M_FILIAL", "XFilial('G6L')" },; 
					{ "G6M_CODAPU", "G6L_CODAPU" } } 
	
	oModel:SetRelation( "G6M_DETAIL", aRelation, G6M->( IndexKey( 1 ) ) )

	oModel:AddGrid( 	"G6O_DETAIL"	,;
						"G6M_DETAIL"	,;
						oStruG6O	 	,;
						{|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA043V2G6O(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)}	,; //bLinePre
						/*bLinePost*/	,;
						{|oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue| TA043V1G6O(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)} /*bPreVal*/	,;
						/*bPosVal*/	,;
						/*BLoad*/ 		)
	
	aRelation := {	{ "G6O_FILIAL", "XFilial('G6L')" },; 
					{ "G6O_CODAPU", "G6L_CODAPU" },; 
					{ "G6O_SEGNEG", "G6M_SEGNEG" },; 
					{ "G6O_TIPOAC", "G6M_TIPOAC" }}
					
	oModel:SetRelation( "G6O_DETAIL", aRelation, G6O->( IndexKey( 1 ) ) )

	oModel:AddGrid( "G6N_DETAIL", "G6O_DETAIL", oStruG6N, , /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )

	aRelation := {	{ "G6N_FILIAL", "XFilial('G6L')" },; 
					{ "G6N_CODAPU", "G6L_CODAPU" },; 
					{ "G6N_SEGNEG", "G6M_SEGNEG" },; 
					{ "G6N_CODACD", "G6O_CODACD" } }
	
	oModel:SetRelation( "G6N_DETAIL", aRelation,  "G6N_FILREF+G6N_REGVEN+G6N_ITVEND+G6N_SEQIV") //G6N->( IndexKey( 1 )

	oModel:AddGrid( "G81_DETAIL", "G6M_DETAIL", oStruG81, /*bLinePre*/, /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )

	aRelation := { 	{ "G81_FILIAL", "XFilial('G6L')" },; 
					{ "G81_CODAPU", "G6L_CODAPU" },; 
					{ "G81_SEGNEG", "G6M_SEGNEG" } }
	
	oModel:SetRelation( "G81_DETAIL", aRelation, G81->( IndexKey( 1 ) ) )

	oModel:GetModel( "G6M_DETAIL" ):SetUniqueLine( { "G6M_FILIAL","G6M_SEGNEG","G6M_TIPOAC" } )
	oModel:GetModel( "G6O_DETAIL" ):SetUniqueLine( { "G6O_FILIAL","G6O_CODACD" } )
	oModel:GetModel( "G6N_DETAIL" ):SetUniqueLine( { "G6N_FILIAL","G6N_ITEM" } )

	oModel:GetModel("G6M_DETAIL"):SetNoInsertLine(nTA43Opc <> 3)

	oModel:GetModel('G6O_DETAIL'):SetNoInsertLine(nTA43Opc <> 3)

	oModel:GetModel("G6N_DETAIL"):SetDescription( STR0004 )  // "Acordo x Documento de Reserva"
	oModel:GetModel("G6N_DETAIL"):SetOptional(.T.)
	oModel:GetModel("G6N_DETAIL"):SetNoInsertLine(nTA43Opc <> 3)
	oModel:GetModel("G6N_DETAIL"):SetNoDeleteLine(.T.)

	oModel:GetModel("G81_DETAIL"):SetOptional(.T.)
	oModel:GetModel("G81_DETAIL"):SetNoInsertLine(.F.)
	oModel:GetModel("G81_DETAIL"):SetNoUpdateLine(.F.)
	oModel:GetModel("G81_DETAIL"):SetNoDeleteLine(.F.)

	// Define o n�mero m�ximo de linhas que o model poder� receber, de acordo com a define GRIDMAXLIN.
	oModel:GetModel( 'G6N_DETAIL' ):SetMaxLine(GRIDMAXLIN)
		
	oModel:SetVldActivate( {|oModel| TA043VlAct(oModel) })
	oModel:SetActivate({|oModel| TA43DefInit(oModel)})

Return oModel


/*/{Protheus.doc} ViewDef
Vis�o referente a apura��o de metas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ViewDef()
	Local oModel   	:= FWLoadModel( "TURA043" )
	Local oStruG6L 	:= FWFormStruct( 2, "G6L" )
	Local oStruG6M 	:= FWFormStruct( 2, "G6M" )
	Local oStruG6O 	:= FWFormStruct( 2, "G6O" )
	Local oStruG6N 	:= FWFormStruct( 2, "G6N" )
	Local oStruG81 	:= FWFormStruct( 2, "G81" )
	Local oView		:= FWFormView():New()
	Local oChart	:= nil
	
	Local nI		:= 0
	
	Local aBaseApu	:= Tur43Cbox("G6D_BASEAP") // { '1=Volume Vendas','2=Transa��o','3=Market Share','4=Crescimento' }
	
	/*-------------------------------------------------------------------
	Mapa de aOrdem
	
	aOrdem - Array
		aOrdem[n]	- Array
			aOrderm[n,1] - Caractere, Nome do Campo que Antecede
			aOrderm[n,2] - Caractere, Nome do Campo que Procede
	--------------------------------------------------------------------*/
	Local aOrdem	:= {}
	Local aNoFldG6L	:= {	"G6L_TOTPRD",;
							"G6L_TOTACD"}
	Local aNoFldG6M	:= {	"G6M_RATPAD",;
							"G6M_SEGNEG",;
							"G6M_FILREF"}
	Local aNoFldG6O := {	"G6O_VAPLIC",;
							"G6O_FILREF"}
	Local aNoFldG6N	:= {	"G6N_SEGNEG",;
							"G6N_DSCACD",;
							"G6N_TIPOAC",;
							"G6N_DSCLAS"}
	Local aNoFldG81	:= { 	"G81_SEGNEG",;
							'G81_CODPRD',;
							'G81_DSCPRD'}
	
	//Ir� apresentar as mensagens de processamento
	lShowRun	:= .T.

	oView:SetModel( oModel )
	
	//Altera a edi��o do campo
	oStruG6M:SetProperty("G6M_FATMET" ,MVC_VIEW_CANCHANGE, .t.)
	oStruG6M:SetProperty("G6M_ITFAT" ,MVC_VIEW_CANCHANGE, .t.)
	oStruG6M:SetProperty("G6M_VLDESC" ,MVC_VIEW_CANCHANGE, .t.)
	oStruG6M:SetProperty("G6M_VLTXAD" ,MVC_VIEW_CANCHANGE, .t.)
	
	oStruG6N:SetProperty( "G6N_FILREF",MVC_VIEW_CANCHANGE, .f.)
	
	oStruG6N:SetProperty( "G6N_OK" , MVC_VIEW_TITULO, "")
	
	//Altera��o das ordens dos campos
	oStruG6N:SetProperty( "G6N_OK" , MVC_VIEW_ORDEM, "01")
	oStruG6N:SetProperty( "G6N_FILREF" , MVC_VIEW_ORDEM, "02")
	oStruG6N:SetProperty( "G6N_REGVEN" , MVC_VIEW_ORDEM, "06")
	oStruG6N:SetProperty( "G6N_ITVEND" , MVC_VIEW_ORDEM, "07")
	
	//Campo virtual para apresentar legenda dos acordos, sendo
	// VERDE - Acordo ativo
	// CINZA - Acordo fora de vig�ncia
	oStruG6O:AddField( ;
							"G6O___LEG" 	, ; // [01] C Nome do Campo
							"00" 			, ; // [02] C Ordem
							"" 				, ; // [03] C Titulo do campo
							""				, ; // [04] C Descri��o do campo
							NIL				, ; // [05] A Array com Help
							"C" 			, ; // [06] C Tipo do campo
							"@BMP" 		, ; // [07] C Picture
							NIL 			, ; // [08] B Bloco de Picture Var
							"" 				, ; // [09] C Consulta F3
							.T. 			, ; // [10] L Indica se o campo � edit�vel
							NIL 			, ; // [11] C Pasta do campo
							NIL 			, ; // [12] C Agrupamento do campo
							NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
							NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
							NIL 			, ; // [15] C Inicializador de Browse
							.T. 			, ; // [16] L Indica se o campo � virtual
							NIL 			) 	// [17] C Picture Vari�vel

		

	//- Ajuste do combo do campo G6O_BASAPU pois a tabela � compartilhada para apura��o de receita de clientes e apura��o de acordo de metas
	oStruG6O:SetProperty('G6O_BASAPU',MVC_VIEW_COMBOBOX, aBaseApu )

	//---------------------------------------------------------------------------------------------
	//Campos utilizados somente na apura��o de receita de clientes, s�o retirados da estrutura
	//---------------------------------------------------------------------------------------------
	//Remove campos que n�o ser�o apresentados no Field, Representado pela tabela G6L
	For nI := 1 to Len(aNoFldG6L)
		
		If ( oStruG6L:HasField(aNoFldG6L[nI]) )
			oStruG6L:RemoveField(aNoFldG6L[nI])
		Endif
		
	Next nI
	
	//Remove campos que n�o ser�o apresentados no Grid Receita Acordos, representado pela tabela G6M
	For nI := 1 to Len(aNoFldG6M)
		
		If ( oStruG6M:HasField(aNoFldG6M[nI]) )
			oStruG6M:RemoveField(aNoFldG6M[nI])
		Endif
		
	Next nI
	
	//Remove campos que n�o ser�o apresentados no Grid Acordos, representado pela tabela G6O
	For nI := 1 to Len(aNoFldG6O)
		
		If ( oStruG6O:HasField(aNoFldG6O[nI]) )
			oStruG6O:RemoveField(aNoFldG6O[nI])
		Endif
		
	Next nI
	
	//Remove campos que n�o ser�o apresentados no Grid Acordos, representado pela tabela G6O
	For nI := 1 to Len(aNoFldG6N)
		
		If ( oStruG6N:HasField(aNoFldG6N[nI]) )
			oStruG6N:RemoveField(aNoFldG6N[nI])
		Endif
	
	Next nI
	
	//Remove campos que seriam apresentados no Grid Itens Financeiros (Aba de mesmo nome) - Tabela G81
	For nI := 1 to Len(aNoFldG81)
		
		If ( oStruG81:HasField(aNoFldG81[nI]) )
			oStruG81:RemoveField(aNoFldG81[nI])
		Endif
	
	Next nI
	
	//Altera ordem de campos da Estrutura de G81 - Item Financeiro de Apuracao
	oStruG81:SetProperty( "G81_FILREF" , MVC_VIEW_ORDEM, "01")
	
	aOrdem := {	{"G81_FILREF","G81_CODPRD"},;
				{"G81_CODPRD","G81_DSCPRD"},;
				{"G81_CLASS","G81_DSCLAS"},;
				{"G81_LOJA","G81_DSCCLI"},;
				{"G81_MOEDA","G81_DSCMOE"},;
				{"G81_CONDPG","G81_DCNDPG"},;
				{"G81_ITENT","G81_FILFAT"},;
				{"G81_FILFAT","G81_NUMFAT"},;
				{"G81_NATURE","G81_DSCNAT"}} 
	
	TurOrdViewStruct("G81",oStruG81,aOrdem)

	oView:AddField("VIEW_G6L", oStruG6L, "G6L_MASTER" )
	oView:AddGrid( "VIEW_G6M", oStruG6M, "G6M_DETAIL" )
	oView:AddGrid( "VIEW_G6O", oStruG6O, "G6O_DETAIL" )
	oView:AddGrid( "VIEW_G6N", oStruG6N, "G6N_DETAIL" )
	oView:AddGrid( "VIEW_G81", oStruG81, "G81_DETAIL" )

	oView:AddIncrementField("VIEW_G6N", "G6N_ITEM" )

	oView:AddOtherObject("VIEW_GRAF", {|oPanel| TA043CreGr(oPanel) } )

	oView:SetViewProperty("VIEW_G6L","SETLAYOUT" ,{ FF_LAYOUT_HORZ_DESCR_TOP , 2 } )
	oView:SetViewProperty("VIEW_G6O","CHANGELINE",{ {|| FWMsgRun(oPnlChart, {|| TA043AtuGr() } )} } )

	oView:EnableTitleView( "VIEW_G6N" )

	//Bot�es
	oView:AddUserButton( STR0005, "", {|| TA043HtDoc()} ) //"Consulta Hist�rico"


	// Divis�o Vertical
	oView:CreateVerticalBox("TELA_ESQ"		, 30 )	// Apura��o
	oView:CreateVerticalBox("TELA_DIR"		, 70 )

	// Divis�o Horizontal
	oView:CreateHorizontalBox( "SUPERIOR"	, 30	,"TELA_DIR" )
	oView:CreateHorizontalBox( "MEIO"		, 30	,"TELA_DIR" )
	oView:CreateHorizontalBox( "INFERIOR"	, 40	,"TELA_DIR" )

	// Divis�o Inferior
	oView:CreateVerticalBox( "INFERIOR_ESQ"	, 60	,"INFERIOR" )
	oView:CreateVerticalBox( "INFERIOR_DIR"	, 40	,"INFERIOR" )

	// Pastas
	oView:CreateFolder("PASTA_01","SUPERIOR")
	oView:CreateFolder("PASTA_02","MEIO")

	// Abas
	oView:AddSheet( "PASTA_01", "P1_ABA01", STR0006   ) //"Receita de Acordos"

	oView:AddSheet( "PASTA_02", "P2_ABA01", STR0007   ) //"Acordos"
	oView:AddSheet( "PASTA_02", "P2_ABA02", STR0008  ) //"Itens Financeiro"

	oView:CreateHorizontalBox( "B_P1_ABA01", 100, , , "PASTA_01", "P1_ABA01" )	// Receita de Acordos

	oView:CreateHorizontalBox( "B_P2_ABA01", 100, , , "PASTA_02", "P2_ABA01" )	// Acordos
	oView:CreateHorizontalBox( "B_P2_ABA02", 100, , , "PASTA_02", "P2_ABA02" )	// Itens Financeiros

	oView:SetOwnerView( "VIEW_G6L", "TELA_ESQ" 	)
	oView:SetOwnerView( "VIEW_G6M", "B_P1_ABA01"	)
	oView:SetOwnerView( "VIEW_G6O", "B_P2_ABA01" 	)
	oView:SetOwnerView( "VIEW_G81", "B_P2_ABA02" 	)

	oView:SetOwnerView( "VIEW_G6N"	, "INFERIOR_ESQ" )
	oView:SetOwnerView( "VIEW_GRAF"	, "INFERIOR_DIR" )


	oView:SetViewProperty("VIEW_G6N", "ENABLENEWGRID")
	oView:SetViewProperty("VIEW_G6N", "GRIDFILTER", {.T.})
	oView:SetViewProperty("VIEW_G6N", "GRIDSEEK", {.T.})

	oView:SetDescription(STR0001)

Return oView

//+-----------------------------------------------------------------------------------------------------
//|Processamento/Gera��o das apura��es
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043Gera
Fun��o para param�tros e tela de processamento da gera��o das metas de acordos
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043Gera()
	Local aArea		:= GetArea()
	Local aAreaSA2	:= SA2->( GetArea() )
	Local cPerg 		:= "TURA043"
	Local lBkpShowRun := lShowRun

	//Desativa as mensagens do FwMsgRun
	lShowRun	:= .F.

	If IsBlind()

		Pergunte( cPerg, .F. )
		TA043Proc( )

	ElseIf Pergunte( cPerg )

		If ( TurMultFiliais() )
		
			oProcess := MsNewProcess():New( {|| TA043Proc( oProcess ) }, STR0009,STR0010,.T.) //"Apura��o de Receita de Metas"###"Gerando apura��o..."
			oProcess:Activate()

		Endif
		
	EndIf

	//Ativa as mensagens do FwMsgRun, caso j� estivesse ativa
	lShowRun	:= lBkpShowRun


	RestArea( aAreaSA2 )
	RestArea( aArea )
Return


/*/{Protheus.doc} TA043Proc
Realiza o calculo das metas com base nos fornecedores informados e metas selecionadas via pergunte
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043Proc( oProcess )
	
	Local aFields		:= TA43GetFieldsRV()
	Local aForn			:= {}
	Local aAcordos		:= {}
	Local aDadosApur	:= {}
	Local aDadosAcTipos	:= {}
	Local aDadosMeta	:= {}
	Local aDocReserv	:= {}
	Local dDataIni 		:= CToD( " /  / " )
	Local dDataFim 		:= CToD( " /  / " )
	Local nFornece		:= 0
	Local nAcordo		:= 0
	Local nDocReserv	:= 0
	Local oModel		:= nil
	Local lShowProc	:= !IsBlind() .And. ValType( oProcess ) == "O"
	Local cTpTpAC		:= ""

	If IsBlind() .Or. !lShowRun
		aForn	:= TA043SeFor()
	Else
		FwMsgRun(,{|| aForn	:= TA043SeFor() },,STR0011 ) //"Selecionando fornecedores..."
	EndIf

	If MV_PAR01 == AP_CRESCIMENTO
		If MV_PAR03 <= 0 .Or. MV_PAR03 > 12
			Help(,,"TA043PROC",,STR0028,1,0) //"O campo IN�CIO PER�ODO deve conter o valor entre 1 e 12."
			Return
		Else
			//Ajusta variaveis de data inicio e fim para o periodo selecionado
			TA043PerCr(MV_PAR02, MV_PAR03, @MV_PAR06, @MV_PAR07)
		EndIf
	EndIf

	If lShowProc
		oProcess:SetRegua1( len(aForn) )
	EndIf

	G8B->(DbSetOrder(1))
	If G8B->(DbSeek(xFilial("G8B") + MV_PAR01 ))		
		 cTpTpAC := G8B->G8B_TIPO
	EndIf

	SA2->( dbSetOrder(1) )
	For nFornece := 1 to len(aForn)
		If lShowProc
			oProcess:IncRegua1( STR0012 + aForn[nFornece,1] + "-" + aForn[nFornece,2] ) //"Processando fornecedor: "
		EndIf

		nTotMs	:= 0

		If SA2->( dbSeek( xFilial("SA2") + aForn[nFornece,1] + aForn[nFornece,2] ) )
			lShowRun := .F.

			oModel := FwLoadModel( "TURA043" )
			oModel:SetOperation( MODEL_OPERATION_INSERT )
			oModel:Activate()

			If MV_PAR01 == AP_MARKET_SHARE .Or. MV_PAR01 == AP_CRESCIMENTO
				//Cria��o da tabela tempor�ria para obter o valor total de periodo
				TA043CrTbl( aFields )
			EndIf

			//Adiciona os dados referente ao Cabe�alho da Apura��o, com base no fornecedor posicionado
			aDadosApur := TA043SeG6L( MV_PAR06, MV_PAR07 )
			TA043SetVl(oModel,"G6L_MASTER",aDadosApur )

			//Adiciona os dados referente aos tipos de acordo
			aDadosAcTipos := TA043SeG6M( MV_PAR01, cTpTpAC, SA2->A2_COD, SA2->A2_LOJA )
			TA043SetVl(oModel,"G6M_DETAIL",aDadosAcTipos )

			//Busca fornecedores e acordos vig�ntes conforme parametros
			aAcordos := TA043GetAcordos( aForn[nFornece,3] )
			 
			If lShowProc
				oProcess:SetRegua2( len(aAcordos) )
			EndIf

			G6D->( dbSetOrder(1) )
			For nAcordo := 1 to len( aAcordos )

				If lShowProc
					oProcess:IncRegua2( I18N(  STR0013, { AllTrim( aAcordos[nAcordo,1] ), AllTrim( aAcordos[nAcordo,2] ) } ) ) //"Meta: #1[codigo meta]# Revis�o: #2[revis�o meta]#."
				EndIf

				G6D->( dbSeek( xFilial("G6D") + aAcordos[nAcordo,1] + aAcordos[nAcordo,2] ) )

				If MV_PAR01 == AP_MARKET_SHARE
					//Atualiza��o das tabelas tempor�rias com base nos acordos
					TA043InTMS( aFields, G6D->G6D_CODACO, G6D->G6D_CODREV, MV_PAR06, MV_PAR07, G6D->G6D_REEMBO == "1" )

				ElseIf MV_PAR01 == AP_CRESCIMENTO
					dDataIni := stod( cValToChar( Year(MV_PAR06) - 1 ) + StrZero( Month(MV_PAR06),2 ) + StrZero( Day(MV_PAR06),2 )  )
					dDataFim := LastDay(Stod( cValToChar( Year(MV_PAR07) - 1 ) + StrZero( Month(MV_PAR07),2 ) + "01" ) )
					TA043InTCr( aFields, aForn[nFornece,1], aForn[nFornece,2], G6D->G6D_CODACO, G6D->G6D_CODREV, dDataIni, dDataFim, G6D->G6D_REEMBO == "1" )
				EndIf

				//Busca os dados para montagem da apura��o de Metas, com base registro posicionado da G6D
				aDadosMeta := TA043SeG6O( )
				TA043SetVl(oModel,"G6O_DETAIL",aDadosMeta, nAcordo > 1 )

				//Busca aos dados referente a Documentos de reserva com a meta selecionada
				aDocReserv := TA043DocReserv( MV_PAR01, aForn[nFornece,3][nAcordo][3] , G6D->G6D_CODACO, G6D->G6D_CODREV, MV_PAR06, MV_PAR07  )
				For nDocReserv := 1 to len(aDocReserv)
					TA043SetVl(oModel,"G6N_DETAIL",aDocReserv[nDocReserv], nDocReserv > 1 )
					oModel:SetValue("G6N_DETAIL","G6N_OK",.T.)
				Next

			Next

			If oModel:VldData()
				oModel:CommitData()
			Else
				aErro := oModel:GetErrorMessage()

				AutoGrLog( STR0014 	+ " [" + AllToChar( aErro[1] ) + "]" ) //"Id do formul�rio de origem:"
				AutoGrLog( STR0015 		+ " [" + AllToChar( aErro[2] ) + "]" ) //"Id do campo de origem: "
				AutoGrLog( STR0016 	+ " [" + AllToChar( aErro[3] ) + "]" ) //"Id do formul�rio de erro: "
				AutoGrLog( STR0017 		+ " [" + AllToChar( aErro[4] ) + "]" ) //"Id do campo de erro: "
				AutoGrLog( STR0018 					+ " [" + AllToChar( aErro[5] ) + "]" ) //"Id do erro: "
				AutoGrLog( STR0019 			+ " [" + AllToChar( aErro[6] ) + "]" ) //"Mensagem do erro: "
				AutoGrLog( STR0020 		+ " [" + AllToChar( aErro[7] ) + "]" ) //"Mensagem da solu��o: "
				AutoGrLog( STR0021			 	+ " [" + AllToChar( aErro[8] ) + "]" ) //"Valor atribu�do: "
				AutoGrLog( STR0022 				+ " [" + AllToChar( aErro[9] ) + "]" ) //"Valor anterior: "

				MostraErro()

			EndIf

			oModel:DeActivate()
			oModel:Destroy()

		EndIf

	Next

Return


/*/{Protheus.doc} TA043SeFor
Verifica todos os fornecedores que ter�o as metas calculadas com base nos parametros informados
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SeFor()
Local aForn		:= {}
Local aGrupo 	:= {}

Local cAlias 	:= GetNextAlias()
Local cWherePer	:= "% %"
Local cGrupo 	:= ""
Local cFornece  := ""
Local cFilSA1	:= "%" + TurFilFilter("SA1",.T.) + "%"
Local cFilSA2	:= "%" + TurFilFilter("SA2",.T.) + "%"


Local nPosGru 	:= 0

//Se Base de apura��o CRESCIMENTO verifica o per�odo cadastrado no acordo
If MV_PAR01 == AP_CRESCIMENTO
	cWherePer := "% AND G6D_PERIOD = '" + cValToChar(MV_PAR02) + "' %"
EndIf

BeginSql Alias cAlias
	SELECT 
		G6D_CODACO, 
		G6D_CODREV, 
		G6D_CODFOR AS FORNECE, 
		G6D_LOJA AS LOJA, 
		'1' AS SEQ
	FROM 
		%Table:G6D% G6D
	INNER JOIN 
		%Table:SA2% SA2 
	ON 
		%Exp:cFilSA2% AND
		SA2.%notDel% AND 
		A2_COD = G6D_CODFOR AND 
		A2_LOJA = G6D_LOJA 
	WHERE 
		G6D_FILIAL = %xFilial:G6D% AND
		G6D_CLASS = %Exp:MV_PAR01% AND
		G6D_STATUS <> '1' AND
		G6D_CODACO BETWEEN %Exp:MV_PAR04% AND %Exp:MV_PAR05% AND
		G6D_VIGINI <= %Exp:dDataBase% AND
		G6D_VIGFIM >= %Exp:dDataBase% AND
		G6D_FILIAL = %xFilial:G6D% AND
		G6D_CODFOR + G6D_LOJA BETWEEN %Exp:MV_PAR08% + %Exp:MV_PAR09% AND %Exp:MV_PAR11% + %Exp:MV_PAR12% AND
		G6D_CODREV = 
		( 
			SELECT 
				MAX( G6D_CODREV ) 
			FROM 
				%Table:G6D% G6DTMP
			WHERE 
				G6DTMP.G6D_CODACO = G6D.G6D_CODACO AND
				G6DTMP.G6D_FILIAL = G6D.G6D_FILIAL AND
				G6DTMP.%notDel%
		)
		%Exp:cWherePer% AND
		G6D.%notDel%
	UNION
	SELECT 
		G6F_CODACO, 
		G6F_CODREV, 
		G6F_CODFOR AS FORNECE, 
		G6F_LOJA AS LOJA, 
		'2' AS SEQ 
	FROM 
		%Table:G6F% G6F 
	INNER JOIN 
		%Table:G6D% G6D 
	ON 
			G6D_FILIAL = %xFilial:G6D% AND
			G6D_CODACO = G6F.G6F_CODACO AND
			G6F_CODREV = G6D_CODREV AND
			G6D_CLASS = %Exp:MV_PAR01% AND
			G6D_STATUS <> '1' AND
			G6D_CODACO BETWEEN %Exp:MV_PAR04% AND %Exp:MV_PAR05% AND
			G6D_VIGINI <= %Exp:dDataBase% AND
			G6D_VIGFIM >= %Exp:dDataBase% AND
			G6D_CODFOR + G6D_LOJA BETWEEN %Exp:MV_PAR08% + %Exp:MV_PAR09% AND %Exp:MV_PAR11% + %Exp:MV_PAR12% AND
			G6D_CODREV = 
			( 
				SELECT 
					MAX( G6D_CODREV ) 
				FROM 
					%Table:G6D% G6DTMP
				WHERE 
					G6DTMP.G6D_CODACO = G6D.G6D_CODACO AND
					G6DTMP.G6D_FILIAL = G6D.G6D_FILIAL AND
					G6DTMP.%notDel%
			)
	WHERE 
		G6F_FILIAL = %xFilial:G6F% AND 
		G6F.%notDel%
	ORDER BY 1, 5, 3, 4
	  
EndSQL 

While (cAlias)->( !EOF() )
	If (cAlias)->SEQ == "1"
		If (cAlias)->FORNECE + (cAlias)->LOJA <> cFornece
			aAdd( aForn, { (cAlias)->FORNECE , (cAlias)->LOJA , {} } ) 
		 	cFornece := (cAlias)->FORNECE + (cAlias)->LOJA
		 	cGrupo := "'" + (cAlias)->FORNECE + (cAlias)->LOJA + "'"
		 	aAdd( aForn[Len(aForn)][3] , { (cAlias)->G6D_CODACO , (cAlias)->G6D_CODREV , cGrupo } )
		Else
		 	cGrupo := "'" + (cAlias)->FORNECE + (cAlias)->LOJA + "'"
		 	aGrupo := aForn[Len(aForn)][3]
			aAdd( aGrupo , {} )
			nPosGru := Len(aGrupo)
			aGrupo[nPosGru] := { (cAlias)->G6D_CODACO , (cAlias)->G6D_CODREV , cGrupo }
			aForn[Len(aForn)][3] := aGrupo
		Endif			
	Else
	 	aGrupo := aForn[Len(aForn)][3]
		nPosGru := Len(aGrupo)
		cGrupo := aGrupo[nPosGru][3]
		cGrupo += ",'" + (cAlias)->FORNECE + (cAlias)->LOJA + "'"
		aGrupo[nPosGru][3] 	 := cGrupo
		aForn[Len(aForn)][3] := aGrupo
	Endif
	//aAdd(aForn, { (cAlias)->A2_COD, (cAlias)->A2_LOJA} )
	(cAlias)->(dbSkip())
EndDo

(cAlias)->( dbCloseArea() )

Return aForn

/*/{Protheus.doc} TA043SeG6M
Retorna o vetor para cria��o do total da apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SeG6M( cTipoClass, cTpTpAC, cForCod, cForLoja )
	Local aAux := {}
	Local lExtTurNat	:= FindFunction('U_TURNAT')
	Local cNat			:= ""	

	aAdd( aAux,{ "G6M_TIPOAC"	, cTipoClass		} )
	aAdd( aAux,{ "G6M_VLACD"		, 0					} )
	aAdd( aAux,{ "G6M_PERDES"	, 0					} )
	aAdd( aAux,{ "G6M_VLDESC" 	, 0					} )
	aAdd( aAux,{ "G6M_PERCTX" 	, 0					} )
	aAdd( aAux,{ "G6M_VLTXAD" 	, 0					} )
	aAdd( aAux,{ "G6M_TOTAL"		, 0					} )
	
	cNat := IIf(lExtTurNat, U_TURNAT(cFilAnt,IIf(cTpTpAC == '1','3','4'), cTipoClass, "1", " ", '2', cForCod, cForLoja ), "")
	If !Empty(cNat) .And. TurVldNat(cNat, .F., )
		aAdd( aAux,{ "G6M_NATURE"	, cNat })
	EndIf
	
Return aAux



/*/{Protheus.doc} TA043SeG6O
Retorna vetor para cria��o da apura��o posicionada (G6D)
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SeG6O( )
	Local aArea		:= GetArea()
	Local aAreaG6E	:= G6E->( GetArea() )
	Local aAux := {}

	aAdd( aAux, { "G6O_TIPOAC"	, G6D->G6D_CLASS	} )
	aAdd( aAux, { "G6O_CODACD"	, G6D->G6D_CODACO	} )
	aAdd( aAux, { "G6O_CODREV"	, G6D->G6D_CODREV	} )
	aAdd( aAux, { "G6O_DCACD" 	, G6D->G6D_DESCRI	} )
	aAdd( aAux, { "G6O_BASAPU"	, G6D->G6D_BASEAP	} )
	aAdd( aAux, { "G6O_VLBASE"	, 0 				} )
	aAdd( aAux, { "G6O_TIPOVL"	, G6D->G6D_TPVLR	} )
	aAdd( aAux, { "G6O_VALOR"	, 0					} )
	aAdd( aAux, { "G6O_VAPLIC"	, 0					} )

	If G6D->G6D_VIGINI <= dDataBase .And. dDataBase <= G6D->G6D_VIGFIM
		aAdd( aAux, { "G6O_STATUS"	, "1"	} ) // Acordo vigente
	Else
		aAdd( aAux, { "G6O_STATUS"	, "3"	} ) // Acordo fora de vig�ncia
	EndIf

	//Inicializa os valores desejados na meta, demais valores ser�o inicializados com 0. Estes ser�o atualizados conforme lan�amento dos documentos de reserva
	G6E->(dbSetOrder(1))
	If G6E->( dbSeek( xFilial("G6E") + G6D->G6D_CODACO + G6D->G6D_CODREV + G6D->G6D_FAIXA ) )
		aAdd( aAux, { "G6O_FAIXA"	, G6E->G6E_FAIXA 	} )
		aAdd( aAux, { "G6O_TOTMET"	, G6E->G6E_DE	} )
	Else
		aAdd( aAux, { "G6O_FAIXA"	, "" 	} )
		aAdd( aAux, { "G6O_TOTMET"	, 0		} )
	EndIf

	aAdd( aAux, { "G6O_TOTREE"	, 0					} )
	aAdd( aAux, { "G6O_FXATU"	, "" 				} )
	aAdd( aAux, { "G6O_FXDE"	, 0					} )
	aAdd( aAux, { "G6O_FXATE"	, 0					} )
	aAdd( aAux, { "G6O_TOTNEC"	, 0					} )
	aAdd( aAux, { "G6O_PERNEC"	, 0					} )

	RestArea( aAreaG6E )
	RestArea( aArea )

Return aAux



/*/{Protheus.doc} TA043SeG6L
Retorna o cabe�alho para cria��o da apura��o com base no fornecedor posicionado e parametros informados
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SeG6L( dDataIni, dDataFim )
	Local aAux := {}

	aAdd( aAux,{ "G6L_CLIENT"	, SA2->A2_CLIENTE	} )
	aAdd( aAux,{ "G6L_LOJA"		, SA2->A2_LOJCLI	} )
	aAdd( aAux,{ "G6L_FORNEC"	, SA2->A2_COD	} )
	aAdd( aAux,{ "G6L_LJFORN"	, SA2->A2_LOJA	} )
	aAdd( aAux,{ "G6L_NOMFOR"	, SA2->A2_NOME 	} )
	aAdd( aAux,{ "G6L_TPAPUR"	, "2"			} )
	aAdd( aAux,{ "G6L_DTGERA" 	, dDataBase		} )
	aAdd( aAux,{ "G6L_DTINI"	, dDataIni		} )
	aAdd( aAux,{ "G6L_DTFIM"	, dDataFim		} )
	aAdd( aAux,{ "G6L_STATUS"	, "1"			} )
	aAdd( aAux,{ "G6L_MOEDA"	, "01"			} )
	aAdd( aAux,{ "G6L_TOTACD" 	, 0				} )
	aAdd( aAux,{ "G6L_TOTAL"	, 0				} )

Return aAux



/*/{Protheus.doc} TA043GetAcordos
Retorna os c�digos de acordo e revis�o de acordo com parametros informados
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043GetAcordos( aGrupo )
Local aAcordos	:= {}
Local nX := 0 

For nX := 1 To Len(aGrupo)
	aAdd(aAcordos, { aGrupo[nX][1], aGrupo[nX][2] } )
Next

Return aAcordos

//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043ProcLib
Processamento da libera��o da apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043ProcLib()
	Local aErro		:= {}
	Local aValues		:= {}
	Local cTpForn		:= ""
	Local lContinua	:= .T.
	Local nX			:= 0
	Local oModel 		:= Nil
	Local oStruG81		:= Nil 
	
	Local aCtbG81A		:= {}
	Local aCtbG81B		:= {}
	Local lOnLIne		:= .F.
	Local lMostrLanc	:= .F.
	Local lAglutLanc	:= .F.
	Local lExiste		:= .T.
	
	Local cFilWhere		:= ""
	Local cChave		:= ""
	Local cIdIFA		:= ""
	
	lExiste := TURExistX1("TURA043C")
	
	If ( lExiste )
		Pergunte("TURA043C",.F.)
	Endif	

	lOnLine 	:= Iif(lExiste .And. mv_par01 == 1,.T.,.F.)
	lMostrLanc	:= Iif(lExiste .And. mv_par02 == 1,.T.,.F.)
	lAglutLanc	:= Iif(lExiste .And. mv_par03 == 1,.T.,.F.)	

	oModel := FwLoadModel( "TURA043" )
	oModel:SetOperation( MODEL_OPERATION_UPDATE )
	oModel:Activate()

	aAdd(aValues, {"G6L_STATUS", "2"})
	lContinua := TA043SetVl(oModel,"G6L_MASTER",aValues )

	If lContinua
		
		//monta array est�tico das filiais do grupo de empresas corrente
		TurSelFil(.T.,.F.)
		
		oStruG81 := oModel:GetModel("G81_DETAIL"):GetStruct()

		cTpForn := IIF(Posicione("SA1", 1, xFilial("SA1") + oModel:GetValue( "G6L_MASTER", "G6L_CLIENT" ) + oModel:GetValue( "G6L_MASTER", "G6L_LOJA" ), "A1_EST") == "EX", "2", "1")
		cFilWhere 	:= "% " + TurFilFilter("G6A") + " %"	//monta a estrutura de filtro das filiais do grupo de empresas para uso na query a seguir  
		
		//zera array est�tico das filiais do grupo de empresas corrente
		TurArrMultFil(.T.) 

			oStruG81:SetProperty("*", MODEL_FIELD_WHEN	 ,{ || .T. } )
			
			For nX := 1 to oModel:GetModel( "G6M_DETAIL" ):Length()
				
				aValues := {}
	
				oModel:GetModel( "G6M_DETAIL" ):GoLine( nX )
	
				cIdIFA := GetSXENum( "G81", "G81_IDIFA" )
				
				aAdd(aValues, { "G81_IDIFA"		, cIdIFA						 				} 	)	
				aAdd(aValues, { "G81_CODAPU"	, oModel:GetValue( "G6L_MASTER", "G6L_CODAPU" ) }	)
				aAdd(aValues, { "G81_CLASS"		, oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC" )	}	)
				aAdd(aValues, { "G81_CLIENT" 	, oModel:GetValue( "G6L_MASTER", "G6L_CLIENT" )	}	)
				aAdd(aValues, { "G81_LOJA"		, oModel:GetValue( "G6L_MASTER", "G6L_LOJA"	)	}	)
				aAdd(aValues, { "G81_EMISSA"	, dDataBase										}	)
	
				If oModel:GetValue( "G6M_DETAIL", "G6M_TOTAL" ) >= 0
					aAdd(aValues, { "G81_VALOR"	, oModel:GetValue( "G6M_DETAIL", "G6M_TOTAL" )	}	)
					aAdd(aValues, { "G81_PAGREC"	, "2"										}	)
				Else
					aAdd(aValues, { "G81_VALOR"	, oModel:GetValue( "G6M_DETAIL", "G6M_TOTAL" ) * -1	}	)
					aAdd(aValues, { "G81_PAGREC"	, "1"										}	)
				EndIf
	
				aAdd(aValues, { "G81_MOEDA"	, oModel:GetValue( "G6L_MASTER", "G6L_MOEDA" )	}	)
				aAdd(aValues, { "G81_TIPO"	, POSICIONE( "G8B", 1, xFilial("G8B")+oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC" ), "G8B_TIPO" )	}	)
		 		aAdd(aValues, {'G81_CONDPG',  TURXCndPag(oModel:GetValue( "G6L_MASTER", "G6L_CLIENT" ), oModel:GetValue( "G6L_MASTER", "G6L_LOJA" ), '1', /*cGrpCod*/, '2')})	
				aAdd(aValues, { "G81_STATUS", "1" } )

				aAdd(aValues, { "G81_DTLIB" , dDataBase	} )
				aAdd(aValues, { "G81_FILREF", cFilAnt		} )
				aAdd(aValues, { "G81_NATURE", oModel:GetValue( "G6M_DETAIL", "G6M_NATURE" )	} )
	
				If TA043SetVl(oModel,"G81_DETAIL",aValues )
					ConfirmSX8()
					AADD(aCtbG81A,oModel:GetModel( "G81_DETAIL" ):GetValue("G81_IDIFA"))
				Else
					lContinua := .F.
					Exit
				EndIf
	
			Next

			oStruG81:SetProperty("*", MODEL_FIELD_WHEN	 ,{ || .F. } )

			If lContinua
				
				If oModel:VldData()
				
					BEGIN TRANSACTION
					
					//Pesquisa o Item da Fatura aerea para atualizar o IDIFA
					G6I->(DbSetOrder(1))	//G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
					
					cChave := Padr(oModel:GetValue("G6M_DETAIL","G6M_FILCON"),TamSx3("G6I_FILIAL")[1])
					cChave += Padr(oModel:GetValue("G6M_DETAIL","G6M_FATMET"),TamSx3("G6I_FATURA")[1])
					cChave += Padr(oModel:GetValue("G6M_DETAIL","G6M_ITFAT"),TamSx3("G6I_ITEM")[1])
					cChave += Padr(oModel:GetValue("G6M_DETAIL","G6M_CONCIL"),TamSx3("G6I_CONCIL")[1])
					
					If ( G6I->(DbSeek(cChave)) )
						
						RecLock("G6I",.F.)
							G6I->G6I_IDIFA := cIdIFA
						G6I->(MsUnlock())
							
					EndIf
					
					oModel:CommitData()
					
					If lOnLine
						TurCtOnApu(lOnline,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA043")						
					EndIf
					
					END TRANSACTION
				
				Else
					lContinua := .F.
				EndIf
			EndIf
		
			If !lContinua
		
				aErro := oModel:GetErrorMessage()
		
				AutoGrLog( STR0014 	+ " [" + AllToChar( aErro[1] ) + "]" ) //"Id do formul�rio de origem:"
				AutoGrLog( STR0015 	+ " [" + AllToChar( aErro[2] ) + "]" ) //"Id do campo de origem: "
				AutoGrLog( STR0016 	+ " [" + AllToChar( aErro[3] ) + "]" ) //"Id do formul�rio de erro: "
				AutoGrLog( STR0017 	+ " [" + AllToChar( aErro[4] ) + "]" ) //"Id do campo de erro: "
				AutoGrLog( STR0018 	+ " [" + AllToChar( aErro[5] ) + "]" ) //"Id do erro: "
				AutoGrLog( STR0019 	+ " [" + AllToChar( aErro[6] ) + "]" ) //"Mensagem do erro: "
				AutoGrLog( STR0020 	+ " [" + AllToChar( aErro[7] ) + "]" ) //"Mensagem da solu��o: "
				AutoGrLog( STR0021	+ " [" + AllToChar( aErro[8] ) + "]" ) //"Valor atribu�do: "
				AutoGrLog( STR0022 	+ " [" + AllToChar( aErro[9] ) + "]" ) //"Valor anterior: "
		
				MostraErro()
		
			EndIf

	EndIf

	oModel:DeActivate()

Return

/*/{Protheus.doc} TA043ProcEst
Estorno da libera��o da apura��o
@type function
@author Rogerio Melonio
@since 17/03/2016
@version 12.1.7
/*/
Static Function TA043ProcEst()
Local aErro			:= {}
Local lContinua		:= .T.
Local nX			:= 0
Local oModel 		:= nil

Local aCtbG81A		:= {}
Local aCtbG81B		:= {}
Local lOnLine		:= .F.
Local lMostrLanc	:= .F.
Local lAglutLanc	:= .F.
Local lExiste		:= .T.

lExiste := TURExistX1("TURA043C")

If ( lExiste )
	Pergunte("TURA043C",.F.)
Endif	

lOnLine 	:= .T.	//Sempre ser� on-line no estorno
lMostrLanc	:= Iif(lExiste .And. mv_par02 == 1,.T.,.F.)
lAglutLanc	:= Iif(lExiste .And. mv_par03 == 1,.T.,.F.)

oModel := FwLoadModel( "TURA043" )
oModel:SetOperation( MODEL_OPERATION_UPDATE )
oModel:Activate()

lContinua := oModel:GetModel("G6L_MASTER"):SetValue("G6L_STATUS", "1" )

If lContinua

	For nX := 1 to oModel:GetModel( "G81_DETAIL" ):Length()
		oModel:GetModel( "G81_DETAIL" ):GoLine( nX )
		AADD(aCtbG81A,oModel:GetModel( "G81_DETAIL" ):GetValue('G81_IDIFA'))
		oModel:GetModel( "G81_DETAIL" ):DeleteLine()
	Next

	If lContinua
		If oModel:VldData()
			BEGIN TRANSACTION
			TurCtOnEAp(oModel,lOnLine,lMostrLanc,lAglutLanc,aCtbG81A,aCtbG81B,"TURA043")
			oModel:CommitData()
			END TRANSACTION
		Else
			lContinua := .F.
		EndIf
	EndIf
		
	If !lContinua
		
		aErro := oModel:GetErrorMessage()
	
		AutoGrLog( STR0014 	+ " [" + AllToChar( aErro[1] ) + "]" ) //"Id do formul�rio de origem:"
		AutoGrLog( STR0015 	+ " [" + AllToChar( aErro[2] ) + "]" ) //"Id do campo de origem: "
		AutoGrLog( STR0016 	+ " [" + AllToChar( aErro[3] ) + "]" ) //"Id do formul�rio de erro: "
		AutoGrLog( STR0017 	+ " [" + AllToChar( aErro[4] ) + "]" ) //"Id do campo de erro: "
		AutoGrLog( STR0018 	+ " [" + AllToChar( aErro[5] ) + "]" ) //"Id do erro: "
		AutoGrLog( STR0019 	+ " [" + AllToChar( aErro[6] ) + "]" ) //"Mensagem do erro: "
		AutoGrLog( STR0020 	+ " [" + AllToChar( aErro[7] ) + "]" ) //"Mensagem da solu��o: "
		AutoGrLog( STR0021	+ " [" + AllToChar( aErro[8] ) + "]" ) //"Valor atribu�do: "
		AutoGrLog( STR0022 	+ " [" + AllToChar( aErro[9] ) + "]" ) //"Valor anterior: "
	
		MostraErro()
		
	EndIf

EndIf

oModel:DeActivate()

Return

//+-----------------------------------------------------------------------------------------------------
//|Documentos de reserva
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043DocReserv
Retorna os documentos de reserva de acordo com o tipo de apura��o e parametros informados e o tipo de apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DocReserv( cTpApur, cGrupo, cMeta, cRev, dDataDe, dDataAte )
Local aDocReserv 	:= {}

	If cTpApur == AP_VOLUME_VENDAS
		aDocReserv := TA043DoRVV( cGrupo, cMeta, cRev, dDataDe, dDataAte  )

	ElseIf cTpApur == AP_TRANSACAO
		aDocReserv	:= TA043DoRTr( cGrupo, cMeta, cRev, dDataDe, dDataAte  )

	ElseIf cTpApur == AP_MARKET_SHARE
		aDocReserv := TA043DoRMS( cGrupo, cMeta, cRev, dDataDe, dDataAte  )
	ElseIf cTpApur == AP_CRESCIMENTO
		aDocReserv := TA043DoRCR( cGrupo, cMeta, cRev, dDataDe, dDataAte  )
	EndIf

Return	aDocReserv

/*/{Protheus.doc} TA043DoRVV
Obtem os documentos de reserva para apura��o por volume de vendas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DoRVV(cGrupo, cMeta, cRev, dDataDe, dDataAte )
	
Local aAux			:= {}
Local aDocReserv	:= {}
Local aFields		:= TA43GetFieldsRV() 
Local aWhere		:= {}

Local cAlias		:= GetNextAlias()
Local cBkpFil		:= ""
	
Local nX			:= 0

aAdd(aWhere,"G3R_EMISS BETWEEN '" + dtos( dDataDe ) + "' AND '" + dtos( dDataAte ) + "'")
aAdd(aWhere,"G3R_FORNEC + G3R_LOJA IN (" + cGrupo + ")" )
aAdd(aWhere,"G3R_STATUS <> '4'")

//Verifica se os reembolsos s�o considerados ou n�o
G6D->( dbSetOrder(1) )
If G6D->( dbSeek( xFilial("G6D") + cMeta + cRev ) ) .And. G6D->G6D_REEMBO <> "1"
	aAdd(aWhere,"G3R_OPERAC <> '2'")
EndIf

If TURegApl( cAlias, "G3R", aFields, aWhere, "3", cMeta, cRev )
	While (cAlias)->( !EOF() )

		cBkpFil := cFilAnt
		
		cFilAnt := TurFormFil((cAlias)->G3R_FILIAL)
		
		//Provisoriamente � realizada a regra de aplica��o registro a registro, futuramente a TURegApl j� far� a consulta com a regra de aplica��o contemplada
		If !Tura34RegApl(	cMeta /*cAcordo*/;
							,cRev /*cRevisao*/;
							,"3" /*cTipoAcordo*/;
							,(cAlias)->G3R_NUMID /*cRegVenda*/;
							,(cAlias)->G3R_IDITEM /*cItemVenda*/;
							,(cAlias)->G3R_NUMSEQ /*cNumSeq*/;
							,/*[oModel]*/;
							)
			
			(cAlias)->( dbSkip() )
			
			cFilAnt := cBkpFil
			 
			Loop
			
		EndIf

		cFilAnt := cBkpFil
		
		aAux := {}

		aAdd(aAux, { "G6N_FILREF"	, (cAlias)->G3R_FILIAL } )
		aAdd(aAux, { "G6N_ITEM"		, StrZero( len(aDocReserv)+1, TamSX3("G6N_ITEM")[1] ) } )
		aAdd(aAux, { "G6N_DTINCL"	, dDataBase				} )
		aAdd(aAux, { "G6N_REGVEN"	, (cAlias)->G3R_NUMID 	} )
		aAdd(aAux, { "G6N_ITVEND"	, (cAlias)->G3R_IDITEM  } )
		aAdd(aAux, { "G6N_SEQIV"	, (cAlias)->G3R_NUMSEQ 	} )
		aAdd(aAux, { "G6N_OPERAC"	, (cAlias)->G3R_OPERAC 	} )
		aAdd(aAux, { "G6N_TPDOC"	, (cAlias)->G3R_TPDOC 	} )
		aAdd(aAux, { "G6N_DOCFOR"	, (cAlias)->G3R_DOC 	} )
		aAdd(aAux, { "G6N_GRPROD"	, (cAlias)->G3R_GRPPRD 	} )
		aAdd(aAux, { "G6N_EMISSA"	, (cAlias)->G3R_EMISS 	} )
		aAdd(aAux, { "G6N_VALOR"	, (cAlias)->G3R_VLRSER * (cAlias)->G3R_TXCAMB } )
		aAdd(aAux, { "G3R_FORNEC"	, (cAlias)->G3R_FORNEC } )
		aAdd(aAux, { "G3R_LOJA"		, (cAlias)->G3R_LOJA } )
		aAdd(aAux, { "G3R_NOME"		, Posicione("SA2",1,XFilial("SA2") + (cAlias)->(G3R_FORNEC+G3R_LOJA),"A2_NOME") } )

		aAdd(aDocReserv, aAux)

		(cAlias)->( dbSkip() )

	EndDo

EndIf

If Select( cAlias ) > 0
	(cAlias)->( dbCloseArea() )
EndIf

Return aDocReserv


/*/{Protheus.doc} TA043DoRTr
Obtem os documentos de reserva para apura��o por transa��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DoRTr(cGrupo, cMeta, cRev, dDataDe, dDataAte)
	//Utiliza a consulta aos documentos de reserva que volume de vendas, est� em rotina separada para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	Local aDocReserv := TA043DoRVV(cGrupo, cMeta, cRev, dDataDe, dDataAte)
Return aDocReserv


/*/{Protheus.doc} TA043DoRMS
Obtem os documentos de reserva para apura��o por market share
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DoRMS(cGrupo, cMeta, cRev, dDataDe, dDataAte)
	//Utiliza a consulta aos documentos de reserva que volume de vendas, est� em rotina separada para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	Local aDocReserv := TA043DoRVV(cGrupo, cMeta, cRev, dDataDe, dDataAte)
Return aDocReserv


/*/{Protheus.doc} TA043DoRCr
Obtem os documentos de reserva para apura��o por crescimento
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DoRCr(cGrupo, cMeta, cRev, dDataDe, dDataAte)
	//Utiliza a consulta aos documentos de reserva que volume de vendas, est� em rotina separada para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	Local aDocReserv := TA043DoRVV(cGrupo, cMeta, cRev, dDataDe, dDataAte)
Return aDocReserv




//+-----------------------------------------------------------------------------------------------------
//|Marcar/Desmarcar de documento de reservas (Atualiza��o de valores por documento)
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043Mark
Fun��o para marcar/desmarcar documentos de reserva, chamada via gatilho do campo G6N_OK
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043Mark()
	Local oModel 	:= FwModelActive()
	Local cBaseApur	:= oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC")

	Do Case
		Case cBaseApur == AP_VOLUME_VENDAS
			TA043MrkVV( oModel )
		Case cBaseApur == AP_TRANSACAO
			TA043MrkTr( oModel )
		Case cBaseApur == AP_MARKET_SHARE
			TA043MrkMS( oModel )
		Case cBaseApur == AP_CRESCIMENTO
			TA043MrkCr( oModel )
	EndCase

Return


/*/{Protheus.doc} TA043MrkVV
Executa a atualiza��o de valores de um documento de reserva marcado/desmarcado na apura��o de volume de vendas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043MrkVV( oModel )
	Local cOperac		:= ""
	Local cFldVlr		:= ""
	Local lMarca		:= .F.
	Local nVlrDoc		:= 0
	Local nVlrField	:= 0

	DEFAULT oModel	:= FwModelActive()

	cOperac	:= oModel:GetValue( "G6N_DETAIL","G6N_OPERAC" 	)
	lMarca	:= oModel:GetValue( "G6N_DETAIL","G6N_OK" 		)
	nVlrDoc	:= oModel:GetValue( "G6N_DETAIL","G6N_VALOR" 	)

	If cOperac == "2" //Reembolso
		cFldVlr	:= "G6O_TOTREE"
		nVlrField	:= oModel:GetValue( "G6O_DETAIL","G6O_TOTREE" )
	Else
		cFldVlr	:= "G6O_VLBASE"
		nVlrField	:= oModel:GetValue( "G6O_DETAIL","G6O_VLBASE" )
	EndIf

	If lMarca
		nVlrField += nVlrDoc
	Else
		nVlrField -= nVlrDoc
	EndIf

	oModel:SetValue( "G6O_DETAIL",cFldVlr, nVlrField)

Return


/*/{Protheus.doc} TA043MrkTr
Executa a atualiza��o de valores de um documento de reserva marcado/desmarcado na apura��o de transa��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043MrkTr( oModel )
	Local cOperac		:= ""
	Local cFldVlr		:= ""
	Local lMarca		:= .F.
	Local nVlrField	:= 0

	DEFAULT oModel	:= FwModelActive()

	cOperac	:= oModel:GetValue( "G6N_DETAIL","G6N_OPERAC" 	)
	lMarca		:= oModel:GetValue( "G6N_DETAIL","G6N_OK" 	)


	If cOperac == "2" //Reembolso
		cFldVlr	:= "G6O_TOTREE"
		nVlrField	:= oModel:GetValue( "G6O_DETAIL","G6O_TOTREE" )
	Else
		cFldVlr	:= "G6O_VLBASE"
		nVlrField	:= oModel:GetValue( "G6O_DETAIL","G6O_VLBASE" )
	EndIf

	If lMarca
		nVlrField += 1
	Else
		nVlrField -= 1
	EndIf

	oModel:SetValue( "G6O_DETAIL",cFldVlr, nVlrField)
Return


/*/{Protheus.doc} TA043MrkTr
Executa a atualiza��o de valores de um documento de reserva marcado/desmarcado na apura��o de market share
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043MrkMS( oModel )
	Local aLines		:= {}
	Local cOperac		:= ""
	Local cAliasTMP		:= oTblHisSin:GetAlias()
	Local lMarca		:= .F.
	Local oModelG6N		:= Nil
	Local nX			:= 0
	Local nValor		:= 0

	DEFAULT oModel	:= FwModelActive()

	oModel		:= FwModelActive()
	oModelG6N	:= oModel:GetModel( "G6N_DETAIL" )
	aSaveRows	:= FwSaveRows( oModel )

	For nX := 1 to oModelG6N:Length()
		oModelG6N:GoLine( nX )

		If oModelG6N:GetValue( "G6N_OK" )
			If oModelG6N:GetValue( "G6N_OPERAC" ) == "1"
				nValor += oModelG6N:GetValue( "G6N_VALOR" )
			Else
				nValor -= oModelG6N:GetValue( "G6N_VALOR" )
			EndIf
		EndIf
	Next

	If nValor < 0
		nValor := 0
	Endif

	FwRestRows(aSaveRows)

	(cAliasTMP)->(dbSetOrder(1))
	(cAliasTMP)->( dbSeek( oModel:GetValue( "G6O_DETAIL","G6O_CODACD") + oModel:GetValue( "G6O_DETAIL","G6O_CODREV") ) )

	nTotMs := (cAliasTMP)->TMP_TOTAL
	oModel:SetValue( "G6O_DETAIL","G6O_VLBASE", nValor )

Return

/*/{Protheus.doc} TA043MrkCr
Executa a atualiza��o de valores de um documento de reserva marcado/desmarcado na apura��o de crescimento
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043MrkCr( oModel )
	Local aLines		:= {}
	Local cOperac		:= ""
	Local cAliasTMP		:= oTblHisSin:GetAlias()
	Local lMarca		:= .F.
	Local oModelG6N		:= Nil
	Local nX			:= 0
	Local nValor		:= 0

	DEFAULT oModel	:= FwModelActive()

	oModelG6N	:= oModel:GetModel( "G6N_DETAIL" )
	aSaveRows	:= FwSaveRows( oModel )

	For nX := 1 to oModelG6N:Length()
		oModelG6N:GoLine( nX )

		If oModelG6N:GetValue( "G6N_OK" )
			If oModelG6N:GetValue( "G6N_OPERAC" ) == "1"
				nValor += oModelG6N:GetValue( "G6N_VALOR" )
			Else
				nValor -= oModelG6N:GetValue( "G6N_VALOR" )
			EndIf
		EndIf
	Next

	FwRestRows(aSaveRows)

	(cAliasTMP)->(dbSetOrder(1))
	(cAliasTMP)->( dbSeek( oModel:GetValue( "G6O_DETAIL","G6O_CODACD") + oModel:GetValue( "G6O_DETAIL","G6O_CODREV") ) )

	oModel:SetValue( "G6O_DETAIL","G6O_VLBASE", ( ( ( nValor  - (cAliasTMP)->TMP_TOTAL ) / (cAliasTMP)->TMP_TOTAL )  * 100 ) )
Return

//+-----------------------------------------------------------------------------------------------------
//|Atualiza��o de metas
//+-----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA043AtMt
Fun��o para atualiza��o de metas conforme o tipo de apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043AtMt( )
	Local oModel 		:= FwModelActive()
	Local cBaseApur	:= oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC")

	Do Case
		Case cBaseApur == AP_VOLUME_VENDAS
			If lShowRun
				FwMsgRun(,{|| TA043AtMtV(), TA043ApVV() },,STR0026 ) //"Atualizando Metas..."
			Else
				TA043AtMtV()
				TA043ApVV()
			EndIf

		Case cBaseApur == AP_TRANSACAO
			If lShowRun
				FwMsgRun(,{|| TA043AtMtT(), TA043ApTr() },,STR0026 ) //"Atualizando Metas..."
			Else
				TA043AtMtT()
				TA043ApTr()
			EndIf

		Case cBaseApur == AP_MARKET_SHARE
			If lShowRun
				FwMsgRun(,{|| TA043AtMtM(), TA043ApMS() },,STR0026 ) //"Atualizando Metas..."
			Else
				TA043AtMtM()
				TA043ApMS()
			EndIf

		Case cBaseApur == AP_CRESCIMENTO
			If lShowRun
				FwMsgRun(,{|| TA043AtMtC(), TA043ApCr() },,STR0026 ) //"Atualizando Metas..."
			Else
				TA043AtMtC()
				TA043ApCr()
			EndIf
	EndCase

	//Verifica se o gr�fico da view deve ser atualizado
	If lShowRun
		FWMsgRun( oPnlChart, {|| TA043AtuGr() } )
	EndIf

Return

/*/{Protheus.doc} TA043AtMtV
Fun��o respons�vel por atualizar faixas de meta de acordo com valor atual em apura��es de volume de vendas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043AtMtV( oModel )
Local aArea 	:= GetArea()
Local cAlias	:= GetNextAlias()
Local cMeta	:= ""
Local cRev		:= ""
Local nVlrBas	:= 0
Local nReemb	:= 0
Local nValor	:= 0
Local nVlrFaixa := 0
DEFAULT oModel	:= FwModelActive()

cMeta		:= oModel:GetValue("G6O_DETAIL","G6O_CODACD")
cRev		:= oModel:GetValue("G6O_DETAIL","G6O_CODREV")
nVlrBas		:= oModel:GetValue("G6O_DETAIL","G6O_VLBASE")
nReemb		:= oModel:GetValue("G6O_DETAIL","G6O_TOTREE")

If oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC") == AP_MARKET_SHARE

	nValor		:= nVlrBas - nReemb
	nVlrFaixa	:= nValor / nTotMs * 100 

	If nVlrFaixa < 0
		nVlrFaixa := 0
	Endif

	BeginSql Alias cAlias
		
		SELECT G6E_FAIXA, G6E_DE, G6E_ATE, G6E_VALOR
			FROM %Table:G6E%
			WHERE G6E_CODACO = %Exp:cMeta%
				AND G6E_CODREV = %Exp:cRev%
				AND G6E_DE <= %Exp:nVlrFaixa%
				AND G6E_ATE >= %Exp:nVlrFaixa%
				AND G6E_FILIAL = %xFilial:G6E%
				AND %notDel%

	EndSql

Else

	nValor := nVlrBas - nReemb

	If nValor < 0
		nValor := 0
	Endif

	BeginSql Alias cAlias
	
		SELECT G6E_FAIXA, G6E_DE, G6E_ATE, G6E_VALOR
			FROM %Table:G6E%
			WHERE G6E_CODACO = %Exp:cMeta%
				AND G6E_CODREV = %Exp:cRev%
				AND G6E_DE <= %Exp:nValor%
				AND G6E_ATE >= %Exp:nValor%
				AND G6E_FILIAL = %xFilial:G6E%
				AND %notDel%

	EndSql

Endif

oModel:SetValue("G6O_DETAIL","G6O_FXATU"	, (cAlias)->G6E_FAIXA	)
oModel:SetValue("G6O_DETAIL","G6O_FXDE"		, (cAlias)->G6E_DE		)
oModel:SetValue("G6O_DETAIL","G6O_FXATE"	, (cAlias)->G6E_ATE 		)
oModel:SetValue("G6O_DETAIL","G6O_FXVLR"	, (cAlias)->G6E_VALOR 	)
	
(cAlias)->( dbCloseArea() )
	
Return

/*/{Protheus.doc} TA043AtMtT
Fun��o respons�vel por atualizar faixas de meta de acordo com valor atual em apura��es de transa��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043AtMtT( oModel )
	//Utiliza a mesma forma de faixas de metas que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043AtMtV( oModel )
Return


/*/{Protheus.doc} TA043AtMtM
Fun��o respons�vel por atualizar faixas de meta de acordo com valor atual em apura��es de market share
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043AtMtM( oModel )
	//Utiliza a mesma forma de faixas de metas que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043AtMtV( oModel )
Return

/*/{Protheus.doc} TA043AtMtC
Fun��o respons�vel por atualizar faixas de meta de acordo com valor atual em apura��es de crescimento
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043AtMtC( oModel )
	//Utiliza a mesma forma de faixas de metas que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043AtMtV( oModel )
Return



//+-----------------------------------------------------------------------------------------------------
//|Atualiza��o de totalizadores por acordo e por apura��o
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043AtRA
Fun��o respons�vel por atualizar o acordo da meta com os documentos de reserva selecionados
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043AtRA( cAction, nLine )
	Local aSaveLines 	:= FWSaveRows()
	Local nX			:= 0
	Local nVlrApur		:= 0
	Local oModel		:= FwModelActive()
	Local oModelG6O		:= oModel:GetModel( "G6O_DETAIL" )

	DEFAULT cAction	:= ""
	DEFAULT nLine		:= 0

	For nX := 1 to oModelG6O:Length()

		oModelG6O:GoLine( nX )

		//Tratamento para chamadas atrav�s da exclus�o/restaura��o de acordos
		If nLine == nX
			If cAction == "DELETE"
				Loop
			ElseIf	cAction == "UNDELETE"
				nVlrApur += oModelG6O:GetValue( "G6O_VALOR" )
			EndIf

		//Acordo n�o deletado
		ElseIf !( oModelG6O:IsDeleted() )
			nVlrApur += oModelG6O:GetValue( "G6O_VALOR" )

		EndIf

	Next

	oModel:SetValue( "G6M_DETAIL", "G6M_VLACD", nVlrApur )


	FwRestRows( aSaveLines )
Return


/*/{Protheus.doc} TA043AtTA
Fun��o respons�vel por atualizar o total da apura��o com base nos valores atualizados dos acordos
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043AtTA()
	Local aSaveLines 	:= FWSaveRows()
	Local nX			:= 0
	Local nTotal		:= 0
	Local oModel		:= FwModelActive()
	Local oModelG6M 	:= oModel:GetModel( "G6M_DETAIL" )

	For nX := 1 To oModelG6M:Length()

		oModelG6M:GoLine( nX )
		If !( oModelG6M:IsDeleted() )
			nTotal	+= oModelG6M:GetValue( "G6M_TOTAL" )
		EndIf

	Next

	oModel:SetValue( "G6L_MASTER", "G6L_TOTAL", nTotal )

	FwRestRows( aSaveLines )

Return





//+-----------------------------------------------------------------------------------------------------
//|Apura��es
//+-----------------------------------------------------------------------------------------------------


/*/{Protheus.doc} TA043ApVV
Fun��o para apura��o de metas por volume de vendas, para c�lculo das faixas de valores verifique TA045AtMt
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
@see TA045AtMt
/*/
Static Function TA043ApVV( oModel )
	Local cTpVlr	:= ""
	Local nVlrApu := 0
	local nVlrBas	:= 0
	Local nVlrRee	:= 0
	Local nFxVlr	:= 0

	DEFAULT oModel 	:= FwModelActive()

	cTpVlr		:= oModel:GetValue("G6O_DETAIL","G6O_TIPOVL") 

	nVlrBas		:= oModel:GetValue("G6O_DETAIL", "G6O_VLBASE"	)
	nVlrRee		:= oModel:GetValue("G6O_DETAIL", "G6O_TOTREE"	)
	nFxVlr		:= oModel:GetValue("G6O_DETAIL", "G6O_FXVLR"	)

	If cTpVlr == "1"
		nVlrApu := (nVlrBas - nVlrRee ) * ( nFxVlr / 100 )
	ElseIf cTpVlr == "2"
		nVlrApu := nFxVlr
	EndIf

	oModel:SetValue("G6O_DETAIL","G6O_VALOR",nVlrApu)

Return

/*/{Protheus.doc} TA043ApTr
Fun��o para apura��o de metas por transa��es, para c�lculo das faixas de valores verifique TA045AtMt
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
@see TA045AtMt
/*/
Static Function TA043ApTr( oModel )
	//Utiliza a mesma forma de calculo que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043ApVV( oModel )
Return


/*/{Protheus.doc} TA043ApMS
Fun��o para apura��o de metas por Market Share, para c�lculo das faixas de valores verifique TA045AtMt
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
@see TA045AtMt
/*/
Static Function TA043ApMS( oModel )
	//Utiliza a mesma forma de calculo que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043ApVV( oModel )
Return


/*/{Protheus.doc} TA043ApCr
Fun��o para apura��o de metas por Crescimento, para c�lculo das faixas de valores verifique TA045AtMt
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
@see TA045AtMt
/*/
Static Function TA043ApCr( oModel )
	//Utiliza a mesma forma de calculo que volume de vendas, utiliza esta rotina a parte para facilitar o desenvolvimento de futuras altera��es na regra de neg�cio
	TA043ApVV( oModel )
Return





//+-----------------------------------------------------------------------------------------------------
//|Dados para compara��o - Market Share/Crescimento
//+-----------------------------------------------------------------------------------------------------


/*/{Protheus.doc} TA043LdHis
Fun��o gera��o de tabelas tempor�ricas e carregamento das informa��es de dados para compara��o, utilizada para apura��o de Market Share e Crescimento
@type function
@author Anderson Toledo
@since 26/10/2015
@version 12.1.7
/*/
Static Function TA043LdHis( oModel )
	Local aFields		:= TA43GetFieldsRV()
	Local aSaveLines 	:= FWSaveRows()
	Local cBaseApur		:= oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC")
	Local dDataIni		:= ctod(" / / ")
	Local dDataFim		:= ctod(" / / ")
	Local lBkpShwRun	:= .F.
	Local oModelG6O		:= oModel:GetModel("G6O_DETAIL")
	Local nOperation 	:= oModel:GetOperation()
	Local nX			:= 0

	lBkpShwRun	:= lShowRun
	lShowRun 	:= .F.

	If nOperation <> MODEL_OPERATION_INSERT 
		TA043CrTbl( aFields )

		If cBaseApur == AP_MARKET_SHARE
			G6D->( dbSetOrder(1) )
			For nX := 1 To oModelG6O:Length()
				oModelG6O:GoLine(nX)

				G6D->( dbSeek( xFilial("G6D") + oModelG6O:GetValue("G6O_CODACD") + oModelG6O:GetValue("G6O_CODREV") ) )
				TA043InTMS( aFields, G6D->G6D_CODACO, G6D->G6D_CODREV, oModel:GetValue("G6L_MASTER","G6L_DTINI"), oModel:GetValue("G6L_MASTER","G6L_DTFIM"), G6D->G6D_REEMBO == "1" )

				//Refaz os calculos, a base de hist�rico pode ter sofrido altera��es ap�s a gera��o da apura��o
				If nOperation == MODEL_OPERATION_UPDATE
					TA043MrkMS( oModel )
					TA043AtMtM( oModel )
					TA043ApMS( oModel )
				Endif
			Next

		ElseIf cBaseApur == AP_CRESCIMENTO
			dDataIni := oModel:GetValue("G6L_MASTER","G6L_DTINI")
			dDataFim := oModel:GetValue("G6L_MASTER","G6L_DTFIM")

			dDataIni := stod( cValToChar( Year(dDataIni) - 1 ) + StrZero( Month(dDataIni),2 ) + StrZero( Day(dDataIni),2 )  )
			dDataFim := LastDay(Stod( cValToChar( Year(dDataFim) - 1 ) + StrZero( Month(dDataFim),2 ) + "01" ) )

			G6D->( dbSetOrder(1) )
			For nX := 1 To oModelG6O:Length()
				oModelG6O:GoLine(nX)

				G6D->( dbSeek( xFilial("G6D") + oModelG6O:GetValue("G6O_CODACD") + oModelG6O:GetValue("G6O_CODREV") ) )
				TA043InTCr( aFields, oModel:GetValue("G6L_MASTER","G6L_FORNEC"), oModel:GetValue("G6L_MASTER","G6L_LJFORN") ,G6D->G6D_CODACO, G6D->G6D_CODREV, dDataIni, dDataFim, G6D->G6D_REEMBO == "1" )

				//Refaz os calculos, a base de hist�rico pode ter sofrido altera��es ap�s a gera��o da apura��o
				If nOperation == MODEL_OPERATION_UPDATE
					TA043MrkCr( oModel )
					TA043AtMtC( oModel )
					TA043ApCr( oModel )
				Endif
			Next


		EndIf

	EndIf

	FwRestRows( aSaveLines )

	lShowRun := lBkpShwRun
	
Return


/*/{Protheus.doc} TA043CrTbl
Fun��o para cria��o das tabelas tempor�rias utilizadas para Market Share e Crescimento
@type function
@author Anderson Toledo
@since 26/10/2015
@version 12.1.7
/*/
Static Function TA043CrTbl( aFields )
	Local aStruHis	:= {}
	Local aStruTot	:= {}
	Local nX			:= 0

	AAdd( aStruTot, {"TMP_ACORDO"	, "C", TamSX3("G6D_CODACO")[1]	, 0	} )
	AAdd( aStruTot, {"TMP_REV"		, "C", TamSX3("G6D_CODREV")[1]	, 0	} )
	AAdd( aStruTot, {"TMP_TOTAL"	, "N", 16						, 2	} )
	AAdd( aStruTot, {"TMP_TOTSEL"	, "N", 16						, 2	} )
	AAdd( aStruTot, {"TMP_TOTAPU"	, "N", 16						, 2	} )


	oTblHisSin := FWTemporaryTable():New()

	oTblHisSin:SetFields( aStruTot )
	oTblHisSin:AddIndex( "index1", {"TMP_ACORDO","TMP_REV"} )
	oTblHisSin:Create()

	AAdd( aStruHis, {"TMP_ACORDO"	, "C", TamSX3("G6D_CODACO")[1]	, 0	} )
	AAdd( aStruHis, {"TMP_REV"		, "C", TamSX3("G6D_CODREV")[1]	, 0	} )

	SX3->(dbSetOrder(2))
	For nX := 1 to len( aFields )
		If SX3->( dbSeek( aFields[nX] ) )
			AAdd( aStruHis, {SX3->X3_CAMPO, SX3->X3_TIPO, SX3->X3_TAMANHO, SX3->X3_DECIMAL } )
		EndIf
	Next

	oTblHisAna := FWTemporaryTable():New()
	oTblHisAna:SetFields( aStruHis )
	oTblHisAna:AddIndex( "index1", {"TMP_ACORDO","TMP_REV"} )
	oTblHisAna:Create()

Return

/*/{Protheus.doc} TA043InTMS
Fun��o para inserir os dados referentes a apura��o de Market Share nas tabelas tempor�rias
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
/*/
Static Function TA043InTMS( aFields, cCodAco, cRev, dDataDe, dDataAte, lReemb )

Local aWhere		:= {}
Local cAlias 		:= GetNextAlias()
Local cAliasTMP		:= oTblHisAna:GetAlias()
Local cAliasSin		:= oTblHisSin:GetAlias()
Local cMeta			:= ''
Local cBkpFil		:= ''

Local nTotal		:= 0
Local nX			:= 0

Default oModel	:= FwModelActive()

cMeta := cCodAco

aAdd(aWhere,"G3R_EMISS BETWEEN '" + dtos( dDataDe ) + "' AND '" + dtos( dDataAte ) + "'")
aAdd(aWhere,"G3R_STATUS <> '4'")

//Verifica se os reembolsos s�o considerados ou n�o
If lReemb
	aAdd(aWhere,"G3R_OPERAC <> '2'")
EndIf

If TURegApl( cAlias, "G3R", aFields, aWhere, "3", cCodAco, cRev )
	While (cAlias)->( !EOF() )

		cBkpFil := cFilAnt
	
		cFilAnt := TurFormFil((cAlias)->G3R_FILIAL)
		
		//Provisoriamente � realizada a regra de aplica��o registro a registro, futuramente a TURegApl j� far� a consulta com a regra de aplica��o contemplada
		If !Tura34RegApl(	cMeta /*cAcordo*/;
							,cRev /*cRevisao*/;
							,"3" /*cTipoAcordo*/;
							,(cAlias)->G3R_NUMID /*cRegVenda*/;
							,(cAlias)->G3R_IDITEM /*cItemVenda*/;
							,(cAlias)->G3R_NUMSEQ /*cNumSeq*/;
							,/*[oModel]*/;
							)
			(cAlias)->( dbSkip() )
			
			cFilAnt := cBkpFil
					
			Loop
			
		EndIf

		cFilAnt := cBkpFil

		RecLock(cAliasTMP, .T.)
			For nX := 1 to len(aFields)
				(cAliasTMP)->( FieldPut( FieldPos(aFields[nX]), (cAlias)->( FieldGet( nX ) ) ) )
			Next

			(cAliasTMP)->TMP_ACORDO := cCodAco
			(cAliasTMP)->TMP_REV	:= cRev

			If aScan(aFields, {|x| x == "G3R_MOEDA"}) > 0 .AND. aScan(aFields, {|x| x == "G3R_VLRSER"}) > 0
				(cAliasTMP)->G3R_VLRSER := (cAlias)->G3R_VLRSER * (cAlias)->G3R_TXCAMB
			EndIf


		(cAliasTMP)->( MsUnlock() )


		If (cAliasTMP)->G3R_OPERAC == "1"
			nTotal += (cAliasTMP)->G3R_VLRSER
		Else
			nTotal -= (cAliasTMP)->G3R_VLRSER
		EndIf

		(cAlias)->( dbSkip() )
	EndDo

EndIf

RecLock(cAliasSin,.T.)
	(cAliasSin)->TMP_ACORDO := cCodAco
	(cAliasSin)->TMP_REV	:= cRev
	(cAliasSin)->TMP_TOTAL	:= nTotal
(cAliasSin)->( MsUnlock() )

If Select( cAlias ) > 0
	(cAlias)->( dbCloseArea() )
EndIf

oModel := Nil

Return


/*/{Protheus.doc} TA043InTCr
Fun��o para inserir os dados referentes a apura��o de Crescimento nas tabelas tempor�rias
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
/*/
Static Function TA043InTCr( aFields, cFornece, cLoja, cCodAco, cRev, dDataDe, dDataAte, lReemb )

Local aWhere		:= {}

Local cAlias 		:= GetNextAlias()
Local cAliasTMP		:= oTblHisAna:GetAlias()
Local cAliasSin		:= oTblHisSin:GetAlias()
Local cMeta			:= ''
Local cBkpFil		:= ''

Local nTotal		:= 0
Local nX			:= 0

Default oModel	:= FwModelActive()

If !Empty(oModel:GetValue("G6O_DETAIL","G6O_CODACD"))
	cMeta := oModel:GetValue("G6O_DETAIL","G6O_CODACD")
Else
	cMeta := cCodAco
Endif

aAdd(aWhere,"G3R_EMISS BETWEEN '" + dtos( dDataDe ) + "' AND '" + dtos( dDataAte ) + "'")
aAdd(aWhere,"G3R_FORNEC = '" + cFornece + "'")
aAdd(aWhere,"G3R_LOJA	= '" + cLoja + "'")
aAdd(aWhere,"G3R_STATUS <> '4'")

//Verifica se os reembolsos s�o considerados ou n�o
If lReemb
	aAdd(aWhere,"G3R_OPERAC <> '2'")
EndIf

If TURegApl( cAlias, "G3R", aFields, aWhere, "3", cCodAco, cRev )
	
	While (cAlias)->( !EOF() )

		cBkpFil := cFilAnt

		cFilAnt := TurFormFil((cAlias)->G3R_FILIAL)
		
		//Provisoriamente � realizada a regra de aplica��o registro a registro, futuramente a TURegApl j� far� a consulta com a regra de aplica��o contemplada
		If !Tura34RegApl(	cMeta /*cAcordo*/;
							,cRev /*cRevisao*/;
							,"3" /*cTipoAcordo*/;
							,(cAlias)->G3R_NUMID /*cRegVenda*/;
							,(cAlias)->G3R_IDITEM /*cItemVenda*/;
							,(cAlias)->G3R_NUMSEQ /*cNumSeq*/;
							,/*[oModel]*/;
							)
							
			(cAlias)->( dbSkip() )
			
			cFilAnt := cBkpFil 
			
			Loop
		EndIf

		cFilAnt := cBkpFil
		
		RecLock(cAliasTMP, .T.)
			For nX := 1 to len(aFields)
				(cAliasTMP)->( FieldPut( FieldPos(aFields[nX]), (cAlias)->( FieldGet( nX ) ) ) )
			Next

			(cAliasTMP)->TMP_ACORDO := cCodAco
			(cAliasTMP)->TMP_REV	:= cRev

			If aScan(aFields, {|x| x == "G3R_MOEDA"}) > 0 .AND. aScan(aFields, {|x| x == "G3R_VLRSER"}) > 0
				If (cAlias)->G3R_MOEDA == "01"
					(cAliasTMP)->G3R_VLRSER := (cAlias)->G3R_VLRSER
				Else
					(cAliasTMP)->G3R_VLRSER := (cAlias)->G3R_VLRSER * (cAlias)->G3R_TXCAMB
				EndIf
			EndIf


		(cAliasTMP)->( MsUnlock() )


		If (cAliasTMP)->G3R_OPERAC == "1"
			nTotal += (cAliasTMP)->G3R_VLRSER
		Else
			nTotal -= (cAliasTMP)->G3R_VLRSER
		EndIf

		(cAlias)->( dbSkip() )
	EndDo

EndIf

RecLock(cAliasSin,.T.)
	(cAliasSin)->TMP_ACORDO	:= cCodAco
	(cAliasSin)->TMP_REV	:= cRev
	(cAliasSin)->TMP_TOTAL	:= nTotal
(cAliasSin)->( MsUnlock() )

If Select( cAlias ) > 0
	(cAlias)->( dbCloseArea() )
EndIf

oModel := Nil

Return


/*/{Protheus.doc} TA043HtDoc
Apresentar dados dos documentos de reserva utilizados como compara��o nas apura��es de Market Share e Crescimento
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
/*/
Static Function TA043HtDoc()

Local aSaveRows		:= FwSaveRows( )
Local cAliasTMP 	:= Nil
Local cTitulo		:= ""
Local nX			:= 0
Local nTotSel		:= 0
Local oModel		:= FwModelActive()
Local oModelG6N		:= oModel:GetModel( "G6N_DETAIL" )

If (oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC") == AP_CRESCIMENTO) .Or. (oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC") == AP_MARKET_SHARE)
	
	If ValType(oTblHisSin) == "U"
		TA043LdHis(oModel)		
	EndIf
	cAliasTMP 	:= oTblHisSin:GetAlias()

	If oModel:GetValue( "G6M_DETAIL", "G6M_TIPOAC") == AP_MARKET_SHARE
		cTitulo := STR0051 // "Market Share"
	Else
		cTitulo := STR0052 // "Crescimento"
	EndIf

	For nX := 1 To oModelG6N:Length()
		oModelG6N:GoLine( nX )

		If oModelG6N:GetValue( "G6N_OK" )
			If oModelG6N:GetValue( "G6N_OPERAC" ) == "1"
				nTotSel += oModelG6N:GetValue( "G6N_VALOR" )
			Else
				nTotSel -= oModelG6N:GetValue( "G6N_VALOR" )
			EndIf
		EndIf
	Next

	RecLock( cAliasTMP, .F. )
		( cAliasTMP )->TMP_TOTSEL	:= nTotSel
		( cAliasTMP )->TMP_TOTAPU	:= oModel:GetValue( "G6O_DETAIL","G6O_VLBASE" )
	( cAliasTMP )->( MsUnlock() )

	FWExecView(cTitulo,'TURA043A', MODEL_OPERATION_VIEW )

	FwRestRows(aSaveRows)
Else

	Help(,,"TA043VLDHIS",,STR0053,1,0) // "Hist�rico dispon�vel apenas para apura��es de Market Share e Crescimento"

Endif

Return

//+-----------------------------------------------------------------------------------------------------
//|	Gr�fico - FwChart
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043CreGr
Fun��o respons�vel pela cria��o do gr�fico utilizado na OTHER_OBJECTS
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043CreGr( oPanel )
	Local cTitle	:= ""
	Local oModel 	:= FwModelActive()

	oPnlChart	:= oPanel

	cTitle := "META - " + oModel:GetValue("G6O_DETAIL","G6O_CODACD")

	oChart := FWChartLine():New()
	oChart:setTitle( cTitle, CONTROL_ALIGN_CENTER )
	oChart:init( oPanel, .T. )
	oChart:setLegend( 0 )

	If TA043SerGr(); oChart:Build(); EndIf
Return

/*/{Protheus.doc} TA043AtuGr
Fun��o respons�vel pelo atualiza��o do gr�fico utilizado na OTHER_OBJECTS
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043AtuGr()
	Local cTitle	:= ""
	Local oModel 	:= FwModelActive()

	cTitle := "META - " + oModel:GetValue("G6O_DETAIL","G6O_CODACD")

	oChart:Reset()
	oChart:init( oPnlChart, .T. )
	oChart:setTitle( cTitle, CONTROL_ALIGN_CENTER )
	oChart:setLegend( 0 )

	If TA043SerGr(); oChart:Build(); EndIf

Return


/*/{Protheus.doc} TA043SerGr
Fun��o respons�vel pela gera��o das s�ries apresentadas no gr�fico da OTHER_OBJECTS
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SerGr()
	Local aArea		:= GetArea()
	Local aAux		:= {}
	Local cAlias	:= GetNextAlias()
	Local cMeta	:= ""
	Local cRev		:= ""
	Local cPict		:= ""
	Local nValor	:= 0
	Local nPos		:= 0
	Local nX		:= 0
	Local oModel 	:= nil
	Local lRet    := .T.

	oModel 	:= FwModelActive()
	cMeta		:= oModel:GetValue("G6O_DETAIL","G6O_CODACD"	)
	cRev		:= oModel:GetValue("G6O_DETAIL","G6O_CODREV"	)
	nValor		:= oModel:GetValue("G6O_DETAIL", "G6O_VLBASE"	) - oModel:GetValue("G6O_DETAIL", "G6O_TOTREE"	)

	//Por quest�es de performance, as faixas de metas s�o armazenadas em um vetor est�tico,
	//evitando uma consulta ao banco de dados a cada atualiza��o do gr�fico
	nPos := aScan( aFaixas, { |x| x[1] == cMeta + cRev  } )

	If nPos == 0

		BeginSql Alias cAlias

			SELECT	G6E_CODACO,
					G6E_CODREV,
					G6E_DE,
					G6E_ATE
				FROM %Table:G6E%
				WHERE G6E_CODACO = %Exp:cMeta%
					AND G6E_CODREV = %Exp:cRev%
					AND G6E_FILIAL = %xFilial:G6E%
					AND %notDel%
				ORDER BY G6E_FILIAL, G6E_CODACO, G6E_CODREV, G6E_FAIXA

		EndSql

		cPict	:= PesqPict("G6E","G6E_DE")

		aAdd( aFaixas, { cMeta+cRev, {} } )
		nPos 	:= len(aFaixas)

		If (cAlias)->( !EOF() )
			While (cAlias)->( !EOF() )
				aAdd( aFaixas[nPos][2], { 	AllTrim( Transform((cAlias)->G6E_DE, cPict) ),;
												(cAlias)->G6E_DE } )
				(cAlias)->( dbSkip() )
			EndDo
		Else
			lRet := .F.
		EndIf
		(cAlias)->( dbCloseArea() )

	EndIf

	If lRet
		For nX := 1 to len( aFaixas[nPos][2] )
			aAdd(aAux, {aFaixas[nPos][2][nX][1], nValor })
		Next

		oChart:addSerie( "Metas"	, aFaixas[nPos][2] )
		oChart:addSerie( "Apurado"	, aAux )
	EndIf
	RestArea( aArea )

Return lRet

//+-----------------------------------------------------------------------------------------------------
//|Valida��es
//+----------------------------------------------------------------------------------------------------
/*/{Protheus.doc} TA043VldPerg
Fun��o com valida��es do pergunte TURA043
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043VldPerg()
	Local cVar		:= ReadVar()
	Local lRet 	:= .T.

	Do Case
		Case cVar == "MV_PAR03"
			If Empty( MV_PAR03 ) .And. MV_PAR01 == AP_CRESCIMENTO
				Help(,,"TA043PERG",,STR0027,1,0) //"O campo IN�CIO PER�ODO � de preenchimento obrigat�rio para Base de Apura��o do tipo Crescimento."
				lRet := .F.
			ElseIf !Empty( MV_PAR03 ) .And. ( MV_PAR03 < 0 .Or. MV_PAR03 > 12 )
				Help(,,"TA043PERG",,STR0028,1,0) //"O campo IN�CIO PER�ODO deve conter o valor entre 1 e 12."
				lRet := .F.
			EndIf

		Case cVar == "MV_PAR05"
			If Empty(MV_PAR05)
				Help(,,"TA043PERG",,STR0029,1,0) //"O campo META AT� � de preenchimento obrigat�rio."
				lRet := .F.
			ElseIf MV_PAR04 > MV_PAR05
				Help(,,"TA043PERG",,STR0030,1,0) //"O campo META AT� deve ser maior que o campo META DE."
				lRet := .F.
			EndIf

		Case cVar == "MV_PAR07"
			If Empty(MV_PAR07)
				Help(,,"TA043PERG",,STR0031,1,0) //"O campo DATA AT� � de preenchimento obrigat�rio."
				lRet := .F.
			ElseIf MV_PAR06 > MV_PAR07
				Help(,,"TA043PERG",,STR0032,1,0) //"O campo DATA AT� deve ser maior que o campo DATA DE."
				lRet := .F.
			EndIf

		Case cVar == "MV_PAR09"
			MV_PAR10 := POSICIONE( "SA2",1,xFilial("SA2")+MV_PAR08+MV_PAR09, "A2_NOME")

		Case cVar == "MV_PAR11"
			If Empty( MV_PAR11 )
				Help(,,"TA043PERG",,STR0033,1,0) //"O campo FORNECEDOR AT� � de preenchimento obrigat�rio."
				lRet := .F.
			EndIf

		Case cVar == "MV_PAR12"
			If !Empty( MV_PAR11 ) .And. Empty( MV_PAR12 )
				Help(,,"TA043PERG",,STR0034,1,0) //"O campo LOJA AT� � de preenchimento obrigat�rio."
				lRet := .F.
			Else
				MV_PAR13 := POSICIONE( "SA2",1,xFilial("SA2")+MV_PAR11+MV_PAR12, "A2_NOME")
			EndIf

	EndCase

Return lRet



/*/{Protheus.doc} TA043VlAct
Valida��o para ativa��o do Model
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043VlAct( oModel )
	Local lRet 		:= .T.
	Local nOperation 	:= oModel:GetOperation()

	Do Case
		Case nOperation == MODEL_OPERATION_UPDATE
			If G6L->G6L_STATUS <> "1" .And. !FwIsInCallStack("TURXEstApura")
				Help(,,"TA043VLDACT",,STR0035,1,0) //"Apenas apura��es com status EM ABERTO podem ser alteradas."
				lRet := .F.
			EndIf

		Case nOperation == MODEL_OPERATION_DELETE
			If G6L->G6L_STATUS <> "1"
				Help(,,"TA043VLDACT",,STR0036,1,0) //"Apenas apura��es com status EM ABERTO podem ser exclu�das."
				lRet := .F.
			ElseIf TA43ConMtApv(G6L->G6L_CODAPU,G6L->G6L_FILIAL)
				Help(,,"TA043VLDACT",,STR0084,1,0)//"Apenas apura��es sem concilia��o podem ser exclu�das"
				lRet := .F.
			EndIf
	End Case

Return lRet

//+-----------------------------------------------------------------------------------------------------
//|Gen�ricas
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} MenuDef
Menu Funcional
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function MenuDef()
	Local aRotina := {}

	ADD OPTION aRotina Title STR0037 	Action "VIEWDEF.TURA043" 	OPERATION 2 ACCESS 0 //"Visualizar"
	ADD OPTION aRotina Title STR0039 	Action "TA043Alterar" 		OPERATION 4 ACCESS 0 //"Alterar"
	ADD OPTION aRotina Title STR0040 	Action "VIEWDEF.TURA043" 	OPERATION 5 ACCESS 0 //"Excluir"
	ADD OPTION aRotina Title STR0041 	Action "TURXLibApura"		OPERATION 4 ACCESS 0 //"Liberar Apura��o"
	ADD OPTION aRotina Title STR0057 	Action "TURXEstApura"		OPERATION 4 ACCESS 0 //"Estornar Libera��o"

Return aRotina

/*/{Protheus.doc} TA043Alterar
Rotina de Altera��o de Apura��o
@author Rogerio Melonio
@since 01/03/2016
@version 12.1.7
/*/
Function TA043Alterar
Local oModel	:= Nil
Local oExecView	:= Nil
Local aButtons	:= {{.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}, {.F., Nil}, ;
                    {.F., Nil}, {.T., STR0055}, {.T., STR0056}, {.F., Nil}, {.F., Nil}, ;	// "Confirmar           "Cancelar"
                    {.F., Nil}, {.F., Nil}    , {.F., Nil}    , {.F., Nil}}

oModel:= FWLoadModel("TURA043")
oModel:SetOperation(MODEL_OPERATION_UPDATE)
oModel:Activate()

oView := FWLoadView("TURA043")
oView:SetModel(oModel)

oExecView := FWViewExec():New()
oExecView:SetTitle(STR0039)		// "Alterar"
oExecView:SetView(oView)
oExecView:SetModal(.F.)               
oExecView:SetButtons(aButtons)
oExecView:SetOperation(MODEL_OPERATION_UPDATE)
oExecView:OpenView(.F.)

Return

/*/{Protheus.doc} TA043PerCr
Calculo dos periodos a serem considerados na apura��o por crescimento
@type function
@author Anderson Toledo
@since 26/10/2015
@version 1.0
/*/
Static Function TA043PerCr(nPeriodo, nIniPer, dDataIni, dDataFim)
	Local nSomaPeriodo	:= 0
	Local nAnoIni			:= Year(dDataIni)
	Local nMesFim			:= 0
	Local nAnoFim			:= 0

	Do Case
		Case nPeriodo == 1 //Mensal
			nSomaPeriodo := 0
		Case nPeriodo == 2 //Trimestral
			nSomaPeriodo := 2
		Case nPeriodo == 3 //Semestral
			nSomaPeriodo := 5
		Case nPeriodo == 4 //Anual
			nSomaPeriodo := 11
	End Case

	nMesFim	:= nIniPer + nSomaPeriodo
	If nMesFim > 12
		nMesFim := nMesFim % 12
		nAnoFim :=	Year( dDataIni ) + 1
	Else
		nAnoFim := Year( dDataIni )
	EndIf

	dDataIni := stod( cValToChar( nAnoIni ) + StrZero( nIniPer, 2 ) + "01" )
	dDataFim := LastDay( stod( cValToChar(nAnoFim) + StrZero(nMesFim,2) + "01"  ) )

Return



/*/{Protheus.doc} TA043SetVl
Realiza o setValue no modelo de dados informado com as informa��es passadas via parametro
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043SetVl(oMdlMaster,cIdModel,aDados,lLinha)
Local lRet		:= .T.
Local nX		:= 0
Local nY		:= 0
Local nPos		:= 0

Local oModel 	:= oMdlMaster:GetModel(cIdModel)
Local oStru	:= oModel:GetStruct()

Local aAreaG6D := G6D->(GetArea())

Default lLinha	:= .F.

If lLinha

	If oModel:Length() == oModel:AddLine()
		lRet := .F.
	EndIf

EndIf

For nX := 1 To Len( aDados )
	// Verifica se os campos passados existem na estrutura do modelo
	If oStru:HasField( aDados[nX][1] )
		If !( lRet := oMdlMaster:SetValue(cIdModel, aDados[nX][1], aDados[nX][2] ) )
			lRet := .F.
			Exit
		EndIf
	EndIf
Next

G6D->(RestArea(aAreaG6D))

Return lRet

/*/{Protheus.doc} TA043V1G6O
Fun��o para valida��o para exclus�o de metas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043V1G6O(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)
	Local cMsg			:= ""
	Local lRet 		:= .T.

	If cAction == "DELETE"
		If !IsBlind()
			cMsg	:= STR0042 + CRLF + STR0043 //"Confirma a exclus�o da Meta #1[codigo meta]# - #2[descri��o meta]#?"###"Ap�s salvar a apura��o, esta meta n�o ser� mais considerada nesta apura��o."
			If !( ApMsgYesNo(   I18N( cMsg, { AllTrim( oGridModel:GetValue("G6O_CODACD" ) ), AllTrim( oGridModel:GetValue("G6O_DCACD" ) ) } ) ) )
				lRet := .F.
				Help(,,"TURA043DELMETA",,STR0044,1,0) //"Exclus�o da meta cancelada pelo usu�rio."
			EndIf

		EndIf

	EndIf

Return lRet


/*/{Protheus.doc} TA043V2G6O
Fun��o de controle para apagar/restaurar dados filhos da tabela de apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043V2G6O(oGridModel, nLine, cAction, cIDField, xValue, xCurrentValue)
	Local lRet := .T.

	If cAction == "DELETE"
		If TA043DlG6N( .F. )
			TA043AtRA( cAction, nLine )
		Else
			lRet := .F.
		EndIf

	ElseIf cAction == "UNDELETE"
		If TA043DlG6N( .T. )
			TA043AtRA( cAction, nLine )
		Else
			lRet := .F.
		EndIf

	EndIf

Return lRet


/*/{Protheus.doc} TA043DlG6N
Fun��o para apagar/restaurar dados filhos da tabela de apura��o
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043DlG6N( lUnDelete )
	Local aSaveRows	:= FwSaveRows()
	Local nX			:= 0
	Local oModel 		:= FwModelActive()
	Local oModelG6N	:= oModel:GetModel("G6N_DETAIL")

	DEFAULT lUnDelete := .F.

	oModel:GetModel("G6N_DETAIL"):SetNoDeleteLine(.F.)

	For nX := 1 to oModelG6N:Length()
		oModelG6N:GoLine( nX )

		If lUnDelete
			oModelG6N:UnDeleteLine()
		Else
			oModelG6N:DeleteLine()
		EndIf
	Next

	oModel:GetModel("G6N_DETAIL"):SetNoDeleteLine(.T.)

	FwRestRows( aSaveRows )

Return .T.


/*/{Protheus.doc} TA043Destroy
Desalocar mem�ria utilizada pelas vari�veis static
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043Destroy()
	aFaixas	:= {}
	lShowRun	:= .T.
	nSumHist	:= 0
	oChart		:= nil
	oPnlChart	:= nil

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType( oTblHisSin ) == "O"
		oTblHisSin:Delete()
		FreeObj( oTblHisSin )
	EndIf

	//Verifica se o objeto FwTemporaryTable est� instanciado
	If ValType( oTblHisAna ) == "O"
		oTblHisAna:Delete()
		FreeObj( oTblHisAna )
	EndIf

Return .T.


/*/{Protheus.doc} TA043TbSin
Retornar Alias e Nome Real da tabela tempor�ria Sint�tica de documentos de reserva
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043TbSin()
	Local aRet := {}

	If ValType(oTblHisSin) == "O"
		aAdd(aRet,oTblHisSin:GetAlias())
		aAdd(aRet,oTblHisSin:GetRealName())
	EndIf

Return aRet


/*/{Protheus.doc} TA043TbAna
Retornar Alias e Nome Real da tabela tempor�ria analitica de documentos de reserva
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043TbAna()
	Local aRet := {}

	If ValType(oTblHisAna) == "O"
		aAdd(aRet,oTblHisAna:GetAlias())
		aAdd(aRet,oTblHisAna:GetRealName())
	EndIf

Return aRet





//+-----------------------------------------------------------------------------------------------------
//|	Gatilhos
//+-----------------------------------------------------------------------------------------------------

/*/{Protheus.doc} TA043XGAT
Fun��o gen�rica para chamada de gatilhos, recebendo por parametro o campo que disparou o gatilho e executando as opera��es relacionadas
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Function TA043XGAT(cCampo)
	Local aSaveRows	:= FwSaveRows()
	Local oModel		:= FwModelActive()
	Local xRet

	Do Case
		Case cCampo == "G6M_VLACD"
			TA043XCP02(oModel)	// Atualiza G6M_VLDESC
			TA043XCP04(oModel)	// Atualiza G6M_VLTXAD
			TA043XCP01(oModel)	// Atualiza G6M_TOTAL
		Case cCampo == "G6M_PERDES"
			TA043XCP02(oModel)	// Atualiza G6M_VLDESC
			TA043XCP01(oModel)	// Atualiza G6M_TOTAL
		Case cCampo == "G6M_VLDESC"
			TA043XCP03(oModel)	// Atualiza G6M_PERDES
			TA043XCP01(oModel)	//	Atualiza G6M_TOTAL
		Case cCampo == "G6M_PERCTX"
			TA043XCP04(oModel)	// Atualiza G6M_VLTXAD
			TA043XCP01(oModel)	// Atualiza G6M_TOTALAa
		Case cCampo == "G6M_VLTXAD"
			TA043XCP05(oModel)	// Atualiza G6M_PERCTX
			TA043XCP01(oModel)	// Atualiza G6M_TOTAL
		Case cCampo == "G6O_TOTREE"
			TA043XCP06(oModel)	//Atualiza G6O_TOTNEC
		Case cCampo == "G6O_TOTMET"
			TA043XCP06(oModel)	//Atualiza G6O_TOTNEC
		Case cCampo == "G6O_VLBASE"
			TA043XCP06(oModel)	//Atualiza G6O_TOTNEC
		Case cCampo == "G6O_TOTNEC"
			TA043XCP07(oModel)	//Atualiza G6O_PERNEC
		EndCase

	FwRestRows(aSaveRows)

Return


/*/{Protheus.doc} TA043XCP01
Atualiza o total do acordo
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP01(oModel)
	Local oModelG6M	:= oModel:GetModel("G6M_DETAIL")
	Local xValor		:= oModelG6M:GetValue("G6M_VLACD") - oModelG6M:GetValue("G6M_VLDESC") + oModelG6M:GetValue("G6M_VLTXAD")
	
	If oModelG6M:SetValue("G6M_TOTAL",xValor)
		TA043AtTA()
	Else
		Help(,,"TURA043",,STR0045,1,0) //"Erro ao atualizar o campo 'Total'"
	EndIf
	
	
Return


/*/{Protheus.doc} TA043XCP02
Atualiza o valor de desconto do acordo
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP02(oModel)
	Local oModelG6M	:= oModel:GetModel("G6M_DETAIL")
	Local xValor	:= ( oModelG6M:GetValue("G6M_VLACD") * oModelG6M:GetValue("G6M_PERDES") ) / 100

	If !( oModelG6M:SetValue("G6M_VLDESC",xValor) )
		Help(,,"TURA043",,STR0046,1,0) //"Erro ao calcular Valor do Desconto do Acordo."
	EndIf
Return


/*/{Protheus.doc} TA043XCP03
Atualiza o percentual de desconto do acordo
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP03(oModel)
	Local oModelG6M	:= oModel:GetModel("G6M_DETAIL")
	Local xValor	:= ( 100 * oModelG6M:GetValue("G6M_VLDESC") ) / oModelG6M:GetValue("G6M_VLACD")

	If !( oModelG6M:SetValue("G6M_PERDES",xValor) )
		Help(,,"TURA040",,STR0047,1,0) //"Erro ao calcular Percentual do Desconto da Meta."
	EndIf
Return


/*/{Protheus.doc} TA043XCP04
Atualiza o valor adicional do acordo
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP04(oModel)
	Local oModelG6M	:= oModel:GetModel("G6M_DETAIL")
	Local xValor	:= ( oModelG6M:GetValue("G6M_VLACD") * oModelG6M:GetValue("G6M_PERCTX") ) / 100

	If !( oModelG6M:SetValue("G6M_VLTXAD",xValor) )
		Help(,,"TURA043",,STR0048,1,0) //"Erro ao calcular Valor da Taxa Adicional da Meta."
	EndIf
Return

/*/{Protheus.doc} TA043XCP05
Atualiza o percentual adicional do acordo
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP05(oModel)
	Local oModelG6M	:= oModel:GetModel("G6M_DETAIL")
	Local xValor	:= ( 100 * oModelG6M:GetValue("G6M_VLTXAD") ) / oModelG6M:GetValue("G6M_VLACD")

	If !( oModelG6M:SetValue("G6M_PERCTX",xValor) )
		Help(,,"TURA043",,STR0047,1,0) //"Erro ao calcular Percentual do Desconto da Meta."
	EndIf
Return


/*/{Protheus.doc} TA043XCP06
Atualiza o total necess�rio para atingir a meta
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP06(oModel)
	Local oModelG6O	:= oModel:GetModel("G6O_DETAIL")
	Local xValor		:= oModelG6O:GetValue("G6O_TOTMET") - ( oModelG6O:GetValue("G6O_VLBASE") - oModelG6O:GetValue("G6O_TOTREE") )

	If xValor < 0
		lOk := oModelG6O:SetValue("G6O_TOTNEC",0)
	Else
		lOk := oModelG6O:SetValue("G6O_TOTNEC",xValor)
	EndIf

	If !( lOk )
		Help(,,"TURA043",,STR0049,1,0) //"Erro ao atualizar o campo 'Total Necess�rio'."
	EndIf

Return


/*/{Protheus.doc} TA043XCP07
Atualiza o percentual necess�rio para atingir a meta
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TA043XCP07(oModel)
	Local oModelG6O	:= oModel:GetModel("G6O_DETAIL")
	Local xValor		:= ( oModelG6O:GetValue("G6O_TOTNEC") / oModelG6O:GetValue("G6O_TOTMET") ) * 100

	If xValor <= 0
		lOk := oModelG6O:SetValue("G6O_PERNEC", 0)
	Else
		lOk := oModelG6O:SetValue("G6O_PERNEC", xValor)
	EndIf

	If !( lOk )
		Help(,,"TURA043",,STR0050,1,0) //"Erro ao atualizar o campo '% Necess�rio'."
	EndIf

Return


/*/{Protheus.doc} TURegApl
Fun��o provis�ria para buscar registros da G3R, esta fun��o dever� estar na TURAXFUN e j� executar a consulta com os filtros aplicados
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function TURegApl( cAlias, cTableBase, aRetFields, aWhere, cTpAcordo, cAcordo, cRev )
	Local aDtFields	:= {}
	Local aEntQry		:= {}
	Local cQuery 	 	:= ""
	Local cFilDel 	:= ""
	Local lFirst		:= .T.
	Local lRet 	:= .T.
	Local nX 			:= 0


	If Select( cAlias ) > 0
		( cAlias )->( dbCloseArea() )
	EndIf

	aAdd(aEntQry, cTableBase)

	//
	// Montagem da Query
	//

	//Adiciona os campos utilizados na Query
	SX3->(dbSetOrder(2))
	For nX := 1 to len(aRetFields)
		If SX3->( dbSeek( aRetFields[nX] ) )

			If SX3->X3_TIPO == "D"
				aAdd( aDtFields, aRetFields[nX] )
			EndIf

			If !lFirst
				cQuery += ","
			EndIf

			cQuery += aRetFields[nX]

			lFirst := .F.
		EndIf
	Next

	//Adiciona as tabelas envolvidas
	cQuery += " FROM "

	For nX := 1 to len( aEntQry )
		If nX > 1
			cQuery  += ", "
			cFilDel += " AND "
		EndIf

		cQuery 	+= RetSqlName( aEntQry[nX] ) + " " + aEntQry[nX]
		cFilDel	+= TurFilFilter(aEntQry[nX]) + " AND " + aEntQry[nX] + ".D_E_L_E_T_ = ' '"
		
		//N�o ser�o considerados registros inutilizados do RV
		If ( Len(TamSx3(aEntQry[nX]+"_CONINU")) > 0 )
			aAdd(aWhere, aEntQry[nX]+"_CONINU = '' ")
		Endif
		
	Next

	//Adiciona Clausulas Where
	cQuery += " WHERE "

	For nX := 1 to len( aWhere )
		If nX > 1
			cQuery += " AND "
		EndIf

		cQuery += aWhere[nX]

	Next

	//Adiciona condi��es FILIAL e DELETE para as entidades envolvidas
	If len(aWhere) > 1
		cQuery += " AND "
	EndIf
	cQuery += cFilDel

	cQuery := "% " + cQuery +" %"

	BeginSql Alias cAlias
		SELECT %Exp:cQuery%
	EndSql

	For nX := 1 to len(aDtFields)
		TCSetField( cAlias, aDtFields[nX], "D")
	Next

Return lRet

/*/{Protheus.doc} Tur43Cbox
Fun��o que retorna array com lista de op��es da base de apura��o
@author Rogerio Melonio
@since 23/02/2016
@version 12.1.7
/*/
Static function Tur43Cbox(cCampo) 
// 
Local nX	:= 0
Local aBase := {}
Local cCBOX := ""
Local aOrdemSX3 := SX3->(GetArea())
Local nCont := 0

SX3->(DBSETORDER(2))
If SX3->(DBSEEK(cCampo))
   cCBOX := X3CBOX()
EndIf

Do While !Empty(cCBOX)
   nCONT := AT(';',cCBOX)
   If nCont == 0
      AADD(aBase,AllTrim(cCBOX))
      Exit
   Else
      AADD(aBase,AllTrim(SUBSTR(cCBOX,1,nCONT-1)))
   EndIf 
   cCBOX := SUBSTR(cCBOX,nCONT+1)
EndDo

If Empty(aBase)
	aBase := {STR0067,STR0068,STR0069,STR0070}//"1=Volume Vendas"#"2=Transa��o"#"3=Market Share"#"4=Crescimento"
Endif

SX3->(RestArea(aOrdemSX3))

Return(aBase)

/*/{Protheus.doc} Tur043When
Fun��o que retorna retorna modo de edi��o do campo G6M_NUMFAT
@author Rogerio Melonio
@since 24/02/2016
@version 12.1.7
/*/
Function Tur043When()
Local lRet 		:= .F.
Local oModel 	:= FWModelActive()
Local oModelG6O	:= oModel:GetModel("G6O_DETAIL")
Local cAcordo	:= "" 
Local cAereo	:= "" 
Local nX		:= 0
Local nLinG6O	:= oModelG6O:GetLine()	
Local aSaveRows	:= FwSaveRows( oModel )

For nX := 1 To oModelG6O:Length()
	oModelG6O:GoLine(nX)
	If oModelG6O:GetValue("G6O_STATUS") == "1"
		cAcordo := oModelG6O:GetValue("G6O_CODACD") + oModelG6O:GetValue("G6O_CODREV")
		cAereo	:= Posicione("G6D",1,xFilial("G6D")+cAcordo,"G6D_AEREO") 
		If cAereo == "1"
			lRet := .T.
			Exit
		Endif
	Endif
Next

oModelG6O:GoLine(nX)

FwRestRows(aSaveRows)
 
Return lRet


/*{Protheus.doc} TA43DefInit
Fun��o que efetua a inicializa��o dos campos virtuais de Fornecedor na Grid 
Metas vs Documento de Reserva
@author Fernando Radu Muscalu
@since 17/05/2016
@version 12.1.7
*/
Static Function TA43DefInit(oModel)

Local nI	:= 0
Local nX	:= 0
Local nY	:= 0

Local aAreaSA2	:= {}
Local aSaveRows	:= {}

If ( oModel:GetOperation() <> MODEL_OPERATION_INSERT )
	
	aAreaSA2 := SA2->(GetArea())	
	
	aSaveRows	:= FwSaveRows( oModel )
	
	For nY := 1 to oModel:GetModel("G6M_DETAIL"):Length()
		
		oModel:GetModel("G6M_DETAIL"):GoLine(nY)
		
		For nI := 1 to oModel:GetModel("G6O_DETAIL"):Length()
			
			oModel:GetModel("G6O_DETAIL"):GoLine(nI)
			
			For nX := 1 to oModel:GetModel("G6N_DETAIL"):Length()
				
				aDadoFor := G3R->(GetAdvFVal("G3R", {"G3R_FORNEC","G3R_LOJA"}, oModel:GetModel("G6N_DETAIL"):GetValue("G6N_FILREF", nX) + oModel:GetModel("G6N_DETAIL"):GetValue("G6N_DOCFOR",nX), 3, {}))
				
				If ( Len(aDadoFor) > 0) 
					
					oModel:GetModel("G6N_DETAIL"):GoLine(nX)
					oModel:GetModel("G6N_DETAIL"):ForceValue("G3R_FORNEC",aDadoFor[1])
					oModel:GetModel("G6N_DETAIL"):ForceValue("G3R_LOJA",aDadoFor[2])
					oModel:GetModel("G6N_DETAIL"):ForceValue("G3R_NOME",Posicione("SA2",1,XFilial("SA2") + aDadoFor[1] + aDadoFor[2], "A2_NOME"))
				
				Endif
				
			Next nX8
			
		Next nI
	
	Next nY
	
	FwRestRows(aSaveRows)
	RestArea(aAreaSA2)
	
Endif

Return()

/*{Protheus.doc} TA43GetFieldsRV
Retorna os campos da tabela G3R que ser� utilizado em diversas fun��es

@author Fernando Radu Muscalu
@since 17/05/2016
@version 12.1.7
*/
Static Function TA43GetFieldsRV()

Local aRet	:= {"G3R_FILIAL",;
				"G3R_NUMID",;
				"G3R_IDITEM",;
				"G3R_NUMSEQ",;
				"G3R_OPERAC",;
				"G3R_TPDOC",;
				"G3R_DOC",;
				"G3R_GRPPRD",;
				"G3R_EMISS",;
				"G3R_MOEDA",;
				"G3R_TXCAMB",;
				"G3R_VLRSER",;
				"G3R_FORNEC",;
				"G3R_LOJA"}
Return(aRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA43NameAcd(cCodAcd,cFrom)

Fun��o que verifica se existe uma determinada revis�o solicitada para o Acordo
especificado no par�metro

@Params:	cCodAcd, Caractere, C�digo do Acordo requisitado
			cFrom, Caractere, Qual � o acordo (C - Cliente; F - Fornecedor)
			
@return:	cRet, Caractere, descri��o do acordo

@sample	cDescAcd := TA43NameAcd(cCodAcd,cFrom)
			 		
@author    	Fernando Radu Muscalu
@since     	16/11/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------

Function TA43NameAcd(cCodAcd,cFrom)

Local cRet	:= ""

Default cFrom := Iif(FwIsInCallStack("TURA040"), "C","F")

If ( cFrom == "C" )
	
	cChave := xFilial("G5V")
	cChave += Padr(cCodAcd,TamSx3("G5V_CODACO")[1])
	cChave += TurRetRevAcd(cCodAcd,cFrom)

	cRet := G5V->(GetAdvFVal("G5V", "G5V_DESCRI",cChave,1,""))
	
Else

	cChave := xFilial("G4W")
	cChave += Padr(cCodAcd,TamSx3("G4W_CODACO")[1])
	cChave += TurRetRevAcd(cCodAcd,cFrom)

	cRet := G4W->(GetAdvFVal("G4W", "G4W_DESCRI",cChave,1,""))

Endif

Return(cRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA43SeekG81

Fun��o que retorna um dado da G81, de acordo com a apura��o e segmento de neg�cio

@Params:	cCampo:		Caractere. Campo que se deseja buscar o dado
			cFilApu:	Caractere. C�digo da filial da apura��o 
			cCodApu:	Caractere. C�digo da Apura��o. 
			cSegNeg:	Caractere. Segmento de Neg�cio que foi apurado 

@return:	xRet: 		Undefined. Conte�do referente ao campo do par�metro cCampo 

@sample	xRet := TA43SeekG81(cCampo,cFilApu,cCodApu,cSegNeg)
			 		
@author    	Fernando Radu Muscalu
@since     	22/12/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------

Function TA43SeekG81(cCampo,cFilApu,cCodApu,cSegNeg)

Local cNxtAlias := GetNextAlias() 
Local cField := "%" + cCampo + "%"

Local xRet

If ( G81->(FieldPos(cCampo)) > 0 )

	BeginSQL Alias cNxtAlias
	
		SELECT
			DISTINCT
			%Exp:cField%
		FROM
			%Table:G81% G81
		WHERE
			G81_FILIAL = %Exp:cFilApu%
			AND G81_CODAPU = %Exp:cCodApu%
			AND G81_SEGNEG = %Exp:cSegNeg%
			AND G81.%NotDel%	
	EndSQL
	
	xRet := (cNxtAlias)->(FieldGet(FieldPos(cCampo)))
	
	(cNxtAlias)->(DbCloseArea())
	
EndIf

Return(xRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA43ConMtApv

Fun��o que busca a exist�ncia de uma apura��o de metas que foi conciliada e 
aprovada

@Params:	
	cCodApu:	Caractere. C�digo da Apura��o.
	cFilApu:	Caractere. C�digo da filial da apura��o 

@return: lRet:	L�gico. .t. Existe ao menos uma meta que foi conciliada e aprovada 

@sample	lRet := TA43ConMtApv(cCodApu,cFilApu)
			 		
@author    	Fernando Radu Muscalu
@since     	22/12/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------
Function TA43ConMtApv(cCodApu,cFilApu)

Local cChave	:= ""
Local cNxtAlias	:= GetNextAlias()

Local lRet		:= .T.

Default cFilApu	:= xFilial("G6L")

//Query para buscar a exist�ncia de concilia��o de metas aprovadas
BeginSQL Alias cNxtAlias

	SELECT
		DISTINCT
		1 EXISTE
	FROM
		%Table:G6M% G6M
	INNER JOIN
		%Table:G6L% G6L
	ON
		G6L_FILIAL = G6M_FILIAL
		AND G6L_CODAPU = G6M_CODAPU
		AND G6L_TPAPUR = '2'
		AND G6L.%NotDel%		
	INNER JOIN
		%Table:G6I% G6I
	ON
		G6I_FILIAL = G6M_FILCON
		AND G6I_FATURA = G6M_FATMET
		AND G6I_ITEM = G6M_ITFAT
		AND G6I_CONCIL = G6M_CONCIL
		AND G6I.%NotDel%	
	WHERE
		G6M_FILIAL = %Exp:cFilApu%
		AND G6M_CODAPU = %Exp:cCodApu%
		AND G6M_FATMET <> ''
		AND G6M_CONCIL <> ''
		AND G6M.%NotDel%		

EndSQL

lRet := (cNxtAlias)->EXISTE > 0

(cNxtAlias)->(DbCloseArea())
				
Return(lRet)

//------------------------------------------------------------------------------
/*/{Protheus.doc} TA43VldPrdMta

Fun��o que valida se h� um produto de servi�o cadastrado para apura��o de metas.

@Params:	
	lDisplay:	L�gico. .t. - Mostra mensagens de Help
	
@return: lRet:	L�gico. .t. Valida��o efetuada com sucesso 

@sample	lRet := TA43VldPrdMta(lDisplay)
			 		
@author    	Fernando Radu Muscalu
@since     	26/12/2016
@version	12.1.13
/*/
//------------------------------------------------------------------------------

Function TA43VldPrdMta(lDisplay)

Local lRet		:= .T.

Local cCodProd 	:= Padr(GetMV("MV_TURPRMT"),TamSx3("B1_COD")[1])
Local cMsgProb	:= ""
Local cMsgSolu	:= ""

Local aAreaSB1	:= {}

Default lDisplay := .T.

If ( !Empty(cCodProd) )
	
	aAreaSB1	:= SB1->(GetArea())
	
	SB1->(DbSetOrder(1))	//B1_FILIAL+B1_COD
	
	If ( SB1->(DbSeek(xFilial("SB1") + cCodProd)) )
		
		If ( lRet )
			
			If ( Alltrim(SB1->B1_TIPO) <> "SV" )	//Verifica se � produto de servi�o
			
				lRet := .f.
				
				cMsgProb := STR0071 + Alltrim(SB1->B1_COD) + space(1) + Alltrim(SB1->B1_DESC)	//"Produto "
				cMsgProb += STR0072																//" n�o � de servi�os."
			
				cMsgSolu := STR0073 //"Verifique o campo Tipo do produto em quest�o, ele dever� ser igual a 'SV'."
				
			EndIf
			
		EndIf
		
	Else
		
		lRet := .f.
		
		cMsgProb := STR0074 + cCodProd + STR0075 	//"Produto de c�digo "#" informado no par�metro MV_TURPRMT"
		cMsgProb += STR0076							//" n�o est� cadastrado."
			
		cMsgSolu := STR0077							//"No cadastro de produtos do sistema, cadastre o produto em quest�o."
		
	EndIf
	
	RestArea(aAreaSB1)
	
Else
	
	lRet := .f.
	
	cMsgProb := STR0078	//"N�o h� informa��o de c�digo de produto no par�metro MV_TURPRMT"
		
	cMsgSolu := STR0079	//"No cadastro de par�metros do sistema, informe um c�digo "
	cMsgSolu += STR0080	//"de produto de servi�os para o par�metro."	
	
EndIf

If ( !lRet )
		
	If ( lDisplay )
		FwAlertHelp(cMsgProb,cMsgSolu,STR0081)	//"Aten��o"
	EndIf
	
EndIf

Return(lRet)

//---------------------------------------------------------------------------------
/*/{Protheus.doc} Ta050VldMrk()
Efetua a valida��o da marca��o do item

@sample		Ta050VldMrk(oSubMdl,nLine,cAction,cCampo)
@param		
	oSubMdl:	Tipo -> FormGridView; Submodelo da Grid G48A_DETAIL
	nLine:		Tipo -> Num�rico; Linha atual que est� em valida��o
	cAction:	Tipo -> Caractere; Tipo da A��o que est� em execu��o (SETVALUE, CANSETVALUE,DELETE...)
	cCampo:		Tipo -> Caractere; Campo que est� sendo validado 		
@return		

@author 	Fernando Radu Muscalu
@since 		29/08/2016
@version 	P12
/*/
//---------------------------------------------------------------------------------

Static Function Ta043Vld(oSubMdl,nLine,cAction,cCampo,xVlCurrent,xVlBefore)//a=oModel

Local cChave	:= ""
Local cFatura 	:= ""
Local cItFatu 	:= ""

Local lRet		:= .T.

If ( oSubMdl:GetModel():GetOperation() == MODEL_OPERATION_UPDATE )
	
	If ( cAction == "CANSETVALUE" )
		
		If ( cCampo $ "G6M_FATMET|G6M_ITFAT" )
			
			//Concilia��o preenchida, n�o permite editar o campo
			If ( !Empty(oSubMdl:GetValue("G6M_CONCIL")) )
				lRet := .f.
			Endif			
			
		EndIf 
	
	ElseIf ( cAction == "SETVALUE" )
		
		If ( cCampo $ "G6M_FATMET|G6M_ITFAT" )
			
			If ( cCampo == "G6M_FATMET" )
				cFatura := xVlCurrent
				cItFatu	:= oSubMdl:GetValue("G6M_ITFAT")
			ElseIf ( cCampo == "G6M_ITFAT" )	
				cFatura := oSubMdl:GetValue("G6M_FATMET")
				cItFatu := xVlCurrent
			EndIf
				
			G6I->(DbSetOrder(1))	//G6I_FILIAL+G6I_FATURA+G6I_ITEM+G6I_CONCIL
					
			cChave := Padr(oSubMdl:GetValue("G6M_FILCON"),TamSx3("G6I_FILIAL")[1])
			cChave += Padr(cFatura,TamSx3("G6I_FATURA")[1])
			cChave += Padr(cItFatu,TamSx3("G6I_ITEM")[1])
			
			If ( G6I->(DbSeek(cChave)) )
				
				//Se a fatura existe, mas o item n�o est� mais pendente de associa��o, n�o permite editar
				If ( G6I->G6I_SITUAC <> "1" )
					lRet := .f.
					FwAlertHelp(STR0082,STR0083,STR0081)	//"Fatura/item a�reo escolhido j� possui associa��o."#"Escolha outra fatura/item a�reo."#"Aten��o"
				EndIf
				
			EndIf 
		
		EndIf
		
	EndIf
	
Endif

Return(lRet)
