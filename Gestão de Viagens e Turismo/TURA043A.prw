#Include "Totvs.ch"
#Include "FWMVCDef.ch"
#INCLUDE "TURA043a.ch"

/*/{Protheus.doc} ModelDef
Modelo de dados referete a visualiza��o de dados para compara��o na apura��o de Market Share e Crescimento
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ModelDef()
	Local bLoad 		:= {|oFieldModel, lCopy| loadField(oFieldModel, lCopy)}
	Local oModel		:= MPFormModel():New( "TURA043A" )
	Local oStructCab 	:= FWFormModelStruct():New()
	Local oStructDet 	:= FWFormModelStruct():New()

	oStructCab:AddTable( TA043TbSin()[1], , STR0001, {|| TA043TbSin()[2] })		//"Dados compara��o"
	oStructDet:AddTable( TA043TbAna()[1], , STR0002, {|| TA043TbAna()[2] })		// "Documentos de reserva"

	AddFldCab( oStructCab, 1 )
	AddFldDet( oStructDet, 1 )


	oModel:AddFields( "TMP_MASTER", /*cOwner*/, oStructCab )

	oModel:AddGrid( 	"TMP_DETAIL", "TMP_MASTER", oStructDet, /*bLinePre*/, /*bLinePost*/, /*bPre*/, /*bLinePost*/, bLoad)
	oModel:SetRelation( "TMP_DETAIL", { { "TMP_ACORDO", "TMP_ACORDO" }, { "TMP_REV", "TMP_REV" }}, ( TA043TbAna()[1] )->( IndexKey( 1 ) ) )

	oModel:GetModel("TMP_DETAIL"):SetNoInsertLine(.T.)
	oModel:GetModel("TMP_DETAIL"):SetNoUpdateLine(.T.)
	oModel:GetModel("TMP_DETAIL"):SetNoDeleteLine(.T.)

	oModel:SetPrimaryKey({})

Return oModel

/*/{Protheus.doc} ViewDef
Vis�o referete a visualiza��o de dados para compara��o na apura��o de Market Share e Crescimento
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function ViewDef()
	Local oModel   	:= FWLoadModel( "TURA043A" )
	Local oView		:= FWFormView():New()
	Local oStructCab	:= FWFormViewStruct():New()
	Local oStructDet 	:= FWFormViewStruct():New()

	oView:SetModel( oModel )

	AddFldCab( oStructCab, 2 )
	AddFldDet( oStructDet, 2 )

	oView:AddField("VIEW_TMPM", oStructCab, "TMP_MASTER" )
	oView:AddGrid( "VIEW_TMPD", oStructDet, "TMP_DETAIL" )

	oView:CreateHorizontalBox( "SUPERIOR"	, 20 )
	oView:CreateHorizontalBox( "INFERIOR"	, 80 )

	oView:SetOwnerView( "VIEW_TMPM"	, "SUPERIOR" )
	oView:SetOwnerView( "VIEW_TMPD"	, "INFERIOR" )

Return oView

/*/{Protheus.doc} AddFldCab
Adiciona os campos referenes a estrutura no cabe�alho no model/view
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function AddFldCab( oStructCab, nOpc )

	If nOpc == 1
		oStructCab:AddField( ; // Ord. Tipo Desc.
								STR0003       , ; // [01] C Titulo do campo		// "C�d. Acordo"
								"" 				, ; // [02] C ToolTip do campo
								"TMP_ACORDO" 	, ; // [03] C identificador (ID) do Field
								"C" 			, ; // [04] C Tipo do campo
								TamSX3("G6D_CODACO")[1] , ; // [05] N Tamanho do campo
								0 				, ; // [06] N Decimal do campo
								NIL				, ; // [07] B Code-block de valida��o do campo
								NIL 			, ; // [08] B Code-block de valida��o When do campoz
								NIL 			, ; // [09] A Lista de valores permitido do campo
								NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL				, ; // [11] B Code-block de inicializacao do campo
								NIL 			, ; // [12] L Indica se trata de um campo chave
								.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 			) // [14] L Indica se o campo � virtual


		oStructCab:AddField( ; // Ord. Tipo Desc.
								STR0004       , ; // [01] C Titulo do campo		// "Rev. Acordo"
								"" 				, ; // [02] C ToolTip do campo
								"TMP_REV" 		, ; // [03] C identificador (ID) do Field
								"C" 			, ; // [04] C Tipo do campo
								TamSX3("G6D_CODREV")[1] , ; // [05] N Tamanho do campo
								0 				, ; // [06] N Decimal do campo
								NIL				, ; // [07] B Code-block de valida��o do campo
								NIL 			, ; // [08] B Code-block de valida��o When do campoz
								NIL 			, ; // [09] A Lista de valores permitido do campo
								NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL				, ; // [11] B Code-block de inicializacao do campo
								NIL 			, ; // [12] L Indica se trata de um campo chave
								.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 			) // [14] L Indica se o campo � virtual

		oStructCab:AddField( ; // Ord. Tipo Desc.
								STR0005       , ; // [01] C Titulo do campo		// "Total Hist�rico"
								"" 				, ; // [02] C ToolTip do campo
								"TMP_TOTAL" 	, ; // [03] C identificador (ID) do Field
								"N" 			, ; // [04] C Tipo do campo
								16				, ; // [05] N Tamanho do campo
								2 				, ; // [06] N Decimal do campo
								NIL				, ; // [07] B Code-block de valida��o do campo
								NIL 			, ; // [08] B Code-block de valida��o When do campoz
								NIL 			, ; // [09] A Lista de valores permitido do campo
								NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL				, ; // [11] B Code-block de inicializacao do campo
								NIL 			, ; // [12] L Indica se trata de um campo chave
								.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 			) // [14] L Indica se o campo � virtual


		oStructCab:AddField( ; // Ord. Tipo Desc.
								STR0006       , ; // [01] C Titulo do campo			// "Total Selecionado"
								"" 				, ; // [02] C ToolTip do campo
								"TMP_TOTSEL" 	, ; // [03] C identificador (ID) do Field
								"N" 			, ; // [04] C Tipo do campo
								16				, ; // [05] N Tamanho do campo
								2 				, ; // [06] N Decimal do campo
								NIL				, ; // [07] B Code-block de valida��o do campo
								NIL 			, ; // [08] B Code-block de valida��o When do campoz
								NIL 			, ; // [09] A Lista de valores permitido do campo
								NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL				, ; // [11] B Code-block de inicializacao do campo
								NIL 			, ; // [12] L Indica se trata de um campo chave
								.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 			) // [14] L Indica se o campo � virtual

		oStructCab:AddField( ; // Ord. Tipo Desc.
								STR0007		, ; // [01] C Titulo do campo		// "Valor Apurado"
								"" 				, ; // [02] C ToolTip do campo
								"TMP_TOTAPU" 	, ; // [03] C identificador (ID) do Field
								"N" 			, ; // [04] C Tipo do campo
								16				, ; // [05] N Tamanho do campo
								2 				, ; // [06] N Decimal do campo
								NIL				, ; // [07] B Code-block de valida��o do campo
								NIL 			, ; // [08] B Code-block de valida��o When do campoz
								NIL 			, ; // [09] A Lista de valores permitido do campo
								NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
								NIL				, ; // [11] B Code-block de inicializacao do campo
								NIL 			, ; // [12] L Indica se trata de um campo chave
								.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
								.F. 			) // [14] L Indica se o campo � virtual

	ElseIf nOpc == 2
		oStructCab:AddField( ; // Ord. Tipo Desc.
								"TMP_ACORDO" 	, ; // [01] C Nome do Campo
								"01" 			, ; // [02] C Ordem
								STR0003			, ; // [03] C Titulo do campo			// "C�d. Acordo"
								STR0003			, ; // [04] C Descri��o do campo		// "C�d. Acordo"
								NIL				, ; // [05] A Array com Help
								"C" 			, ; // [06] C Tipo do campo
								"@!"	 		, ; // [07] C Picture
								NIL 			, ; // [08] B Bloco de Picture Var
								"" 				, ; // [09] C Consulta F3
								.F. 			, ; // [10] L Indica se o campo � edit�vel
								NIL 			, ; // [11] C Pasta do campo
								NIL 			, ; // [12] C Agrupamento do campo
								NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 			, ; // [15] C Inicializador de Browse
								.F. 			, ; // [16] L Indica se o campo � virtual
								NIL 			) 	// [17] C Picture Vari�vel

		oStructCab:AddField( ; // Ord. Tipo Desc.
								"TMP_REV" 		, ; // [01] C Nome do Campo
								"02"			, ; // [02] C Ordem
								STR0004			, ; // [03] C Titulo do campo			// "Rev. Acordo"
								STR0004			, ; // [04] C Descri��o do campo		// "Rev. Acordo"
								NIL				, ; // [05] A Array com Help
								"C" 			, ; // [06] C Tipo do campo
								"@!"	 		, ; // [07] C Picture
								NIL 			, ; // [08] B Bloco de Picture Var
								"" 				, ; // [09] C Consulta F3
								.F. 			, ; // [10] L Indica se o campo � edit�vel
								NIL 			, ; // [11] C Pasta do campo
								NIL 			, ; // [12] C Agrupamento do campo
								NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 			, ; // [15] C Inicializador de Browse
								.F. 			, ; // [16] L Indica se o campo � virtual
								NIL 			) 	// [17] C Picture Vari�vel

		oStructCab:AddField( ; // Ord. Tipo Desc.
								"TMP_TOTAL" 	, ; // [01] C Nome do Campo
								"03" 			, ; // [02] C Ordem
								STR0005			, ; // [03] C Titulo do campo				// "Total Hist�rico"
								STR0005			, ; // [04] C Descri��o do campo			// "Total Hist�rico"
								NIL				, ; // [05] A Array com Help
								"N" 			, ; // [06] C Tipo do campo
								"@E 999,999,999.99"	, ; // [07] C Picture
								NIL 			, ; // [08] B Bloco de Picture Var
								"" 				, ; // [09] C Consulta F3
								.F. 			, ; // [10] L Indica se o campo � edit�vel
								NIL 			, ; // [11] C Pasta do campo
								NIL 			, ; // [12] C Agrupamento do campo
								NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 			, ; // [15] C Inicializador de Browse
								.F. 			, ; // [16] L Indica se o campo � virtual
								NIL 			) 	// [17] C Picture Vari�vel

		oStructCab:AddField( ; // Ord. Tipo Desc.
								"TMP_TOTSEL" 	, ; // [01] C Nome do Campo
								"04"			, ; // [02] C Ordem
								STR0006			, ; // [03] C Titulo do campo				// "Total Selecionado"
								STR0006			, ; // [04] C Descri��o do campo			// "Total Selecionado"
								NIL				, ; // [05] A Array com Help
								"N" 			, ; // [06] C Tipo do campo
								"@E 999,999,999.99"	, ; // [07] C Picture
								NIL 			, ; // [08] B Bloco de Picture Var
								"" 				, ; // [09] C Consulta F3
								.F. 			, ; // [10] L Indica se o campo � edit�vel
								NIL 			, ; // [11] C Pasta do campo
								NIL 			, ; // [12] C Agrupamento do campo
								NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 			, ; // [15] C Inicializador de Browse
								.F. 			, ; // [16] L Indica se o campo � virtual
								NIL 			) 	// [17] C Picture Vari�vel

		oStructCab:AddField( ; // Ord. Tipo Desc.
								"TMP_TOTAPU" 	, ; // [01] C Nome do Campo
								"05" 			, ; // [02] C Ordem
								STR0007			, ; // [03] C Titulo do campo		// "Valor Apurado"
								STR0007			, ; // [04] C Descri��o do campo	// "Valor Apurado"
								NIL				, ; // [05] A Array com Help
								"N" 			, ; // [06] C Tipo do campo
								"@E 999,999,999.99"	, ; // [07] C Picture
								NIL 			, ; // [08] B Bloco de Picture Var
								"" 				, ; // [09] C Consulta F3
								.F. 			, ; // [10] L Indica se o campo � edit�vel
								NIL 			, ; // [11] C Pasta do campo
								NIL 			, ; // [12] C Agrupamento do campo
								NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
								NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
								NIL 			, ; // [15] C Inicializador de Browse
								.F. 			, ; // [16] L Indica se o campo � virtual
								NIL 			) 	// [17] C Picture Vari�vel

	EndIf

Return


/*/{Protheus.doc} AddFldDet
Adiciona os campos referenes a detalhes no cabe�alho no model/view
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function AddFldDet( oStructDet, nOpc )
	Local aStruHis	:= ( TA043TbAna()[1] )->( dbStruct() )
	Local aFields		:= {}
	Local nX			:= 0

	SX3->( dbSetOrder(2) )

	For nX := 1 to len( aStruHis )
		If SX3->( dbSeek( aStruHis[nX][1] ) )

			If nOpc == 1
				oStructDet:AddField( ; // Ord. Tipo Desc.
										X3Titulo() , ; // [01] C Titulo do campo
										"" 				, ; // [02] C ToolTip do campo
										SX3->X3_CAMPO , ; // [03] C identificador (ID)adm do Field
										SX3->X3_TIPO 	, ; // [04] C Tipo do campo
										SX3->X3_TAMANHO 	, ; // [05] N Tamanho do campo
										SX3->X3_DECIMAL 	, ; // [06] N Decimal do campo
										NIL				, ; // [07] B Code-block de valida��o do campo
										NIL 			, ; // [08] B Code-block de valida��o When do campoz
										NIL 			, ; // [09] A Lista de valores permitido do campo
										NIL 			, ; // [10] L Indica se o campo tem preenchimento obrigat�rio
										NIL				, ; // [11] B Code-block de inicializacao do campo
										NIL 			, ; // [12] L Indica se trata de um campo chave
										.F. 			, ; // [13] L Indica se o campo pode receber valor em uma opera��o de update.
										.F. 			) // [14] L Indica se o campo � virtual


			ElseIf nOpc == 2
				oStructDet:AddField( ; // Ord. Tipo Desc.
										SX3->X3_CAMPO 	, ; // [01] C Nome do Campo
										StrZero(nX, 2)			, ; // [02] C Ordem
										X3Titulo()	, ; // [03] C Titulo do campo
										X3Titulo()	, ; // [04] C Descri��o do campo
										NIL			, ; // [05] A Array com Help
										SX3->X3_TIPO 		, ; // [06] C Tipo do campo
										SX3->X3_PICTURE	, ; // [07] C Picture
										NIL 			, ; // [08] B Bloco de Picture Var
										"" 				, ; // [09] C Consulta F3
										.F. 			, ; // [10] L Indica se o campo � edit�vel
										NIL 			, ; // [11] C Pasta do campo
										NIL 			, ; // [12] C Agrupamento do campo
										NIL 			, ; // [13] A Lista de valores permitido do campo (Combo)
										NIL 			, ; // [14] N Tamanho M�ximo da maior op��o do combo
										NIL 			, ; // [15] C Inicializador de Browse
										.F. 			, ; // [16] L Indica se o campo � virtual
										NIL 			) 	// [17] C Picture Vari�vel


			EndIf
		EndIf
	Next

Return


/*/{Protheus.doc} loadField
Fun��o para carga dos dados na grid
@type function
@author Anderson Toledo
@since 09/11/2015
@version 12.1.7
/*/
Static Function loadField(oFieldModel, lCopy)
	Local aAux		:= {}
	Local aLoad 	:= {}
	Local aFields	:= oFieldModel:GetStruct():GetFields()
	Local cAlias	:= TA043TbAna()[1]
	Local nX		:= 0

	(cAlias)->(dbGoTop())
	While (cAlias)->( !EOF() )
		aAux := {}
		For nX := 1 to len(aFields)
			aAdd(aAux, &( cAlias+"->"+aFields[nX,3] ) )
		Next

		aAdd( aLoad, {(cAlias)->(RecNo()),aAux} )
		(cAlias)->( dbSkip() )
	EndDo

Return aLoad